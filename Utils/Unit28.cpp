//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit28.h"
#include "XMLContainer.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm28 * Form28;
//---------------------------------------------------------------------------
__fastcall TForm28::TForm28(TComponent * Owner)
    : TForm(Owner)
 {
  ConvDoc = NULL;
  try
   {
    try
     {
      ConvDoc = LoadLibrary(L"icdosconv.dll");
     }
    catch (...)
     {
     }
    if (ConvDoc)
     {
      FOpenProc    = (TOpenConvertProc )GetProcAddress(ConvDoc, "OpenConvert");
      FCloseProc   = (TCloseConvertProc )GetProcAddress(ConvDoc, "CloseConvert");
      FConvertProc = (TConvertDocProc )GetProcAddress(ConvDoc, "ConvertDoc");
     }
    else
     {
     }
   }
  catch (Exception & E)
   {
    MessageBox(NULL, E.Message.c_str(), L"icdosconv.dll", 0);
   }
 }
//---------------------------------------------------------------------------
void __fastcall TForm28::FormDestroy(TObject * Sender)
 {
  if (ConvDoc)
   {
    FreeLibrary(ConvDoc);
    ConvDoc = NULL;
   }
 }
//---------------------------------------------------------------------------
const UnicodeString DOSConvertErrMsg[] =
{
  "������� \"%s\" �� ������.",
  "���������� ������� �������� windows-������.",
  "���������� ������� ������������ ��������� windows-������.",
  "���������� ������� ������������ windows-������.",
  "����������� �������� ������ ��������� windows-������, ���������� ������� � �������.",
  "�� ������ �������������� �������� windows-������.",
  "�� ������ ������������ ��������������� ��������� windows-������.",
  "�� ������ ������������ windows-������ ��������������� ��������� DOS-������.",
  "��� ���������� ������������� windows-������ �� ��������� � ��� ������������� ���������� ��������� windows-������.",
  "������� ������������ ������������� ���������: \"%s\".",
  "�������� DOS-������ � ����� %d �� ������.",
  "�������� DOS-������ ������� �����.",
  "�������� DOS-������ ������� ���.",
  "%s - ����� ��������� DOS-������ ������ � ������������ �������.",
  "� ������������� windows-������ ��������� �������.",
  "� ������������� windows-������ ��������� �������.",
  "� ������������� windows-������ ������ ������� �������.",
  "� ������������� windows-������ ��������� ������� �������.",
  "� ��������� windows-������ ��������� �������.",
  "� ��������� windows-������ ��������� �������.",
  "����� \"%s\" �� ����������.",
  " ",
  "�������� \"%s\" \n�� ����� ���� �������� � DOS-�������.",
  "�������� \"%s\" \n�� ����� ���� �������� � DOS-�������.\n������ �� ������������� ���������� � �������� ���������.\n�������� ����������� �� \"%s\", ��������� ������ \"%s\"."
};
void __fastcall TForm28::Button1Click(TObject * Sender)
 {
/*
extern "C" bool __declspec(dllexport) __stdcall OpenConvert(char * AOrgCode)
extern "C" int __declspec(dllexport) __stdcall CloseConvert(char * AFileName)
extern "C" bool __declspec(dllexport) __stdcall ConvertDoc(char * ADoc, char * ASpec)
*/
      TTagNode *spec = new TTagNode;
      TTagNode *doc = new TTagNode;
      int RC;
      if (FOpenProc("812.2741381.000"))
       {
         spec->LoadFromZIPXMLFile("D:\\work\\imm\\icv7\\Client\\test\\spec.zxml");
         spec->Encoding = "windows-1251";
         doc->LoadFromZIPXMLFile("D:\\work\\imm\\icv7\\Client\\test\\doc.zxml");
         doc->Encoding = "windows-1251";
         RC = FConvertProc(AnsiString(doc->AsXML).c_str(), AnsiString(spec->AsXML).c_str());
         if (RC)
          ShowMessage(DOSConvertErrMsg[RC]);
         FCloseProc("D:\\work\\imm\\icv7\\Client\\test\\Win32\\Debug\\mail.let");
       }
 }
//---------------------------------------------------------------------------
