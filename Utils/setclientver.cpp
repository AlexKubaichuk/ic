// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include "XMLContainer.h"
#include "ExtUtils.h"
// ---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
 {
  try
   {
    TTagNode * verXML = new TTagNode;
    TTagNode * ProdVerXML = new TTagNode;
    TTagNode * Modules, *Module;
    try
     {
      verXML->LoadFromXMLFile(ParamStr(1));
      verXML->AV["main"]        = icsGetFileVersion(ParamStr(2));
      ProdVerXML->Name          = "versioninfo";
      Modules                   = ProdVerXML->AddChild("modulesinfo");
      Module                    = Modules->AddChild("moduleinfo");
      Module->AV["description"] = "������";
      Module->AV["version"]     = icsGetFileVersion(ParamStr(2));
      Module->AV["comments"]    = "���������� ������ ��-���";
      Module                    = Modules->AddChild("moduleinfo");
      Module->AV["description"] = "������ ��";
      Module->AV["version"]     = icsGetFileVersion(ParamStr(3));
      Module->AV["comments"]    = "��������� ������ ��-���";
      Module                    = Modules->AddChild("moduleinfo");
      Module->AV["description"] = "������ ����������������";
      Module->AV["version"]     = icsGetFileVersion(ParamStr(4));
      Module->AV["comments"]    = "������ ���������������� ��-���";
      Module                    = Modules->AddChild("moduleinfo");
      Module->AV["description"] = "�������� ����������";
      Module->AV["version"]     = icsGetFileVersion(ParamStr(5));
      Module->AV["comments"]    = "��������� ��������� ����������";
      Module                    = ProdVerXML->AddChild("companyinfo");
      Module->AV["name"]        = "��� '���������������� ����������� �������'";
      Module->AV["contact"]     = "�����-���������, ��. ��������� 10, ���./���� (812) 667-83-88, 667-86-88, 667-86-76";
      Module->AV["http"]        = "www.inprosys.ru";
      Module->AV["email"]       = "support@inprosys.ru";
     }
    __finally
     {
      verXML->SaveToXMLFile(ParamStr(1));
      delete verXML;
      ProdVerXML->SaveToZIPXMLFile(ParamStr(6));
      delete ProdVerXML;
     }
   }
  catch (Exception & exception)
   {
    Application->ShowException(& exception);
   }
  catch (...)
   {
    try
     {
      throw Exception("");
     }
    catch (Exception & exception)
     {
      Application->ShowException(& exception);
     }
   }
  return 0;
 }
// ---------------------------------------------------------------------------
