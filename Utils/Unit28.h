//---------------------------------------------------------------------------

#ifndef Unit28H
#define Unit28H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
typedef bool __stdcall(*TOpenConvertProc)(char * AOrgCode);
typedef int __stdcall(*TCloseConvertProc)(char * AFileName);
typedef bool __stdcall(*TConvertDocProc)(char * ADoc, char * ASpec);
class TForm28 : public TForm
{
__published:	// IDE-managed Components
 TButton *Button1;
 void __fastcall FormDestroy(TObject *Sender);
 void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
  HINSTANCE ConvDoc;
 TOpenConvertProc  FOpenProc;
 TCloseConvertProc FCloseProc;
 TConvertDocProc FConvertProc;
public:		// User declarations
 __fastcall TForm28(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm28 *Form28;
//---------------------------------------------------------------------------
#endif
