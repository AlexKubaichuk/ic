﻿// ----------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include <stdio.h>
#include <memory>
#include <Datasnap.DSHTTP.hpp>
// ---------------------------------------------------------------------------
#include "appbaseunit.h"
#include "icsLog.h"
#include "srvsetting.h"
#include <WebReq.hpp>
#pragma link "Web.WebReq"
#ifdef USEPACKAGES
#pragma link "IndySystem.bpi"
#pragma link "IndyCore.bpi"
#pragma link "IndyProtocols.bpi"
#else
#pragma comment(lib, "IndySystem")
#pragma comment(lib, "IndyCore")
#pragma comment(lib, "IndyProtocols")
#endif
#pragma link "IdHTTPWebBrokerBridge"
// ---------------------------------------------------------------------------
// #include "dsRegUnitsUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma resource "*.dfm"
// ---------------------------------------------------------------------------
TDSServer * FDSServer;
TDSAuthenticationManager * FDSAuthenticationManager;
// ---------------------------------------------------------------------------
extern PACKAGE TComponentClass WebModuleClass;
// ---------------------------------------------------------------------------
TAppBase * __fastcall TAppBase::AppServer()
 {
//  LogMessage("Before new TAppBase()", EVENTLOG_INFORMATION_TYPE);
  static TAppBase * RC = new TAppBase();
//  LogMessage("After new TAppBase()", EVENTLOG_INFORMATION_TYPE);
  return RC;
 }
// ---------------------------------------------------------------------------
__fastcall TAppBase::TAppBase() : TDataModule(NULL)
 {
  CreateLogs();
  FAdmUser   = "";
  FAdmPasswd = "";
  FCPath     = icsPrePath(ExtractFilePath(ParamStr(0)));
  if (WebRequestHandler() != NULL)
   {
    WebRequestHandler()->WebModuleClass = WebModuleClass;
   }
  FServer = new TIdHTTPWebBrokerBridge(NULL);
  // FServer->Active = false;
  // FServer->RegisterWebModuleClass(WebModuleClass);
  FPort = _SERVER_PORT_;
  FAdminClass    = NULL;
  MKB            = new TdsMKB;
 }
// ---------------------------------------------------------------------------
__fastcall TAppBase::~TAppBase(void)
 {
  delete FAdminClass;
  FAdminClass = NULL;
  Stop();
  Disconnect();
  FDSServer                = NULL;
  FDSAuthenticationManager = NULL;
  DeleteLogs();
  delete FServer;
  FServer = NULL;
  delete MKB;
 }
// ----------------------------------------------------------------------------
TdsAdminClass * __fastcall TAppBase::AdmClass()
 {
  return ((TdsAdminClass *)FAdminClass);
 }
// ----------------------------------------------------------------------------
void __fastcall TAppBase::DSAuthenticationUserAuthenticate(TObject * Sender, const UnicodeString Protocol,
  const UnicodeString Context, const UnicodeString User, const UnicodeString Password, bool & valid,
  TStrings * UserRoles)
 {
  bool FValid = false;
  UnicodeString FUserName = User;
  valid = (FAdmUser == User) && (FAdmPasswd == Password);
  if (valid)
   FUserName = "admin";
  else
   {
    valid = AdmClass()->CheckUser(User, Password, FValid);
    if (valid)
     {
      valid = (valid == FValid);
     }
   }
  if (valid)
   {
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes)
     {
      UserRoles->Add("UPS=" + Password);
      dsLogAuth("[UserAuthenticate] успех: {" + IntToStr(FSes->Id) + "}" + Protocol + "/" + Context + " '" +
        FUserName + "'");
     }
    else
     dsLogAuth("[UserAuthenticate] успех: " + Protocol + "/" + Context + " '" + FUserName + "'");
   }
  else
   {
    if ((User == "check") && (Password == "check"))
     valid = true;
    else
     dsLogAuth("[UserAuthenticate] отказ: " + Protocol + "/" + Context + " '" + FUserName + "'");
   }
  if (valid)
   AdmClass()->LoginUser("");
 }
// ----------------------------------------------------------------------------
void __fastcall TAppBase::DSAuthenticationUserAuthorize(TObject * Sender,
  TDSAuthorizeEventObject * AuthorizeEventObject, bool & valid)
 {
  valid = false;
  TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
  if (FSes)
   {
    valid = (AuthorizeEventObject->MethodAlias == "DSAdmin.GetServerMethodParameters") ||
      (AuthorizeEventObject->MethodAlias == "DSAdmin.GetPlatformName") ||
      (AuthorizeEventObject->MethodAlias == "DSAdmin.DescribeMethod");
    if (!valid)
     {
      // dsLogMessage(FSes->UserRoles->Text,__FUNC__);
      // if (FSes->UserRoles->Count < 2)
      // {
      valid = (AuthorizeEventObject->MethodAlias == "TdsAdminClass.LoginUser");
      if (!valid)
       valid = AdmClass()->CheckRT(FSes->UserRoles->Text, AuthorizeEventObject->MethodAlias);
      // }
      // else
      // valid = AdmClass()->CheckRT(FSes->UserRoles->Text, AuthorizeEventObject->MethodAlias);
     }
    if (!valid)
     {
      UnicodeString FUserName = AuthorizeEventObject->UserName;
      if (FUserName == FAdmUser)
       FUserName = "admin";
      dsLogAuth("отказ в доступе: {" + IntToStr(FSes->Id) + "." + AuthorizeEventObject->MethodAlias + "} '" +
        FUserName + "'");
     }
    // else
    // dsLogAuth("доступ : {" + IntToStr(FSes->Id) + "." + AuthorizeEventObject->MethodAlias + "} '" + AuthorizeEventObject->UserName + "'");
   }
  // valid = true;
 }
// ---------------------------------------------------------------------------
TDSServer * DSServer(void)
 {
  return FDSServer;
 }
// ---------------------------------------------------------------------------
TDSAuthenticationManager * DSAuthenticationManager(void)
 {
  return FDSAuthenticationManager;
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::Disconnect()
 {
  TClassUnitMap::iterator i;
  // TDocMap FDocMap;
  for (i = FDocMap.begin(); i != FDocMap.end(); i++)
   delete i->second;
  FDocMap.clear();
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::ICServerError(TDSErrorEventObject * DSErrorEventObject)
 {
  dsLogError(DSErrorEventObject->Error->Message, __FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::CheckBaseCount(int ACount)
 {
  TStringList * OpenSesList = new TStringList;
  TStringList * OpenBaseList = new TStringList;
  try
   {
    if (!((int)(ACount / 100)))
     throw EBaseAppException(Now().FormatString(" --- dd.mm.yyyy hh.nn.ss"));
    TDSSession * FSes;
    int idx;
    TDSSessionManager::Instance->GetOpenSessionKeys(OpenSesList);
    if ((int)(OpenSesList->Count / ((int)(ACount / 100))))
     throw EBaseAppException(Now().FormatString(" -- dd.mm.yyyy hh.nn.ss"));
    for (int i = 0; i < OpenSesList->Count; i++)
     {
      FSes = TDSSessionManager::Instance->Session[OpenSesList->Strings[i]];
      if (FSes->Status == Active)
       {
        idx = OpenBaseList->IndexOf(FSes->UserRoles->Values["ICDB"].UpperCase());
        if (idx == -1)
         OpenBaseList->Add(FSes->UserRoles->Values["ICDB"].UpperCase());
       }
     }
    if (OpenBaseList->Count / (ACount - (ACount / 100) * 100))
     throw EBaseAppException(Now().FormatString(" - dd.mm.yyyy hh.nn.ss"));
   }
  __finally
   {
    delete OpenSesList;
    delete OpenBaseList;
   }
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                            TdsDocClass                                     #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsDocClassGetClass(TDSServerClass * DSServerClass, TPersistentClass & PersistentClass)
 {
  dsLogBegin(__FUNC__);
  PersistentClass = __classid(TdsDocClass);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
TDataModule * __fastcall TAppBase::CreateDocClass()
 {
  dsLogBegin(__FUNC__);
  dsLogEnd(__FUNC__);
  return new TdsDocClass(NULL, MKB, FXMLList);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsDocClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassCreateInstance(DSCreateInstanceEventObject, FDocMap, CreateDocClass, "dsDocClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsDocClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassDestroyInstance(DSDestroyInstanceEventObject, FDocMap, "dsDocClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                            TdsAdminClass                                  #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
void __fastcall TAppBase::DSAdminClassGetClass(TDSServerClass * DSServerClass, TPersistentClass & PersistentClass)
 {
  dsLogBegin(__FUNC__);
  PersistentClass = __classid(TdsAdminClass);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::DSAdminClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  try
   {
    DSCreateInstanceEventObject->ServerClassInstance = FAdminClass;
   }
  catch (Sysutils::Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::DSAdminClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  try
   {
    DSDestroyInstanceEventObject->ServerClassInstance = NULL;
   }
  catch (Sysutils::Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
////******************************************************************************
void __fastcall TAppBase::ClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject,
  TClassUnitMap & AUnitMap, TCreateModule ACreateNewInstance, const UnicodeString AClassName)
 {
  dsLogBegin(__FUNC__);
  try
   {
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes && (FSes->Status != Terminated))
     {
      FSes->LifeDuration = 28800000; // 8h //86400000; //24h
      UnicodeString FAuthUser = DSCreateInstanceEventObject->ServerConnectionHandler->ConProperties->Values
        ["DSAuthenticationUser"];
      if (FAuthUser.Length())
       {
        if (FSes->UserName.UpperCase() == FAuthUser.UpperCase())
         {
          UnicodeString FDB = FSes->UserRoles->Values["ICDB"];
          TClassUnitMap::iterator FRC = AUnitMap.find(FSes->SessionName /* Id */);
          TDataModule * tmpIC = NULL;
          if (FRC != AUnitMap.end())
           tmpIC = FRC->second;
          else
           {
            try
             {
              tmpIC = ACreateNewInstance();
              dsLogMessage("create " + AClassName + ": " + FSes->Id, __FUNC__, 5);
             }
            catch (Sysutils::Exception & E)
             {
              dsLogError(" in new : " + E.Message, __FUNC__, 5);
             }
           }
          if (tmpIC)
           {
            AUnitMap[FSes->SessionName /* Id */] = tmpIC;
            DSCreateInstanceEventObject->ServerClassInstance = tmpIC;
           }
         }
        else
         dsLogError("Имя пользователя отличается от имени пользователя сессии.", __FUNC__);
       }
      else
       dsLogError("Ошибка определения имени пользователя [" +
        DSCreateInstanceEventObject->ServerConnectionHandler->ConProperties->Properties->Text + "]", __FUNC__);
     }
   }
  catch (Sysutils::Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes)
     FClearSes(FSes->SessionName);
    DSCreateInstanceEventObject->ServerClassInstance = NULL;
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::ClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject,
  TClassUnitMap & AUnitMap, const UnicodeString AClassName)
 {
  dsLogBegin(__FUNC__);
  try
   {
    DSDestroyInstanceEventObject->ServerClassInstance = NULL;
    /* for (TClassUnitMap::iterator i = AUnitMap.begin(); i != AUnitMap.end(); i++)
     {
     TDSSession * FSes = TDSSessionManager::Instance->Session[i->first];
     if (FSes)
     {
     dsLogMessage("delete " + AClassName + ": " + i->first, __FUNC__, 5);
     delete i->second;
     AUnitMap.erase(i->first);
     }
     } */
   }
  catch (Sysutils::Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void TerminateThreads(void)
 {
  if (TDSSessionManager::Instance != NULL)
   {
    TDSSessionManager::Instance->TerminateAllSessions();
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TAppBase::FGetActive()
 {
  bool RC = false;
  try
   {
    if (FServer)
     RC = FServer->Active;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TAppBase::LoadOpt()
 {
  bool RC = false;
  try
   {
    try
     {
      UnicodeString FXMLPath = icsPrePath(ExtractFilePath(ParamStr(0)));
      UnicodeString FFN = icsPrePath(FXMLPath + "\\conf\\" + UnicodeString(_OPT_FILE_NAME_));
      if (!FileExists(FFN))
       {
        ForceDirectories(FXMLPath);
        TTagNode * tmp = new TTagNode;
        try
         {
          tmp->Encoding = "utf-8";
          tmp->Name     = "g";
          tmp->SaveToXMLFile(FFN, "");
         }
        __finally
         {
          delete tmp;
         }
       }
      SrvOptXMLData->FileName = FFN;
      SrvOpt->LoadXML();
      RC = true;
     }
    catch (Sysutils::Exception & E)
     {
//      ShowMessage("LLLL >>> "+E.Message);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::Start()
 {
//      ShowMessage("1 >>> ");
  if (!FServer->Active)
   {
//      ShowMessage("2 >>> ");
    if (!FXMLList)
     FXMLList = new TAxeXMLContainer;
//      ShowMessage("3 >>> ");
    FXMLList->OnExtGetXML = FLoadXML;
//      ShowMessage("4 >>> ");
    if (LoadOpt())
     {
//      ShowMessage("5 >>> ");
      FPort = SrvOpt->Vals[_PORTNAME_].AsIntDef(_SERVER_PORT_);
//      ShowMessage("6 >>> ");
      if (SrvOpt->Vals["usessl"].AsBool)
       {
        if (SrvOpt->Vals["RootCertFile"].AsString.Trim().Length())
         SSLHandle->SSLOptions->RootCertFile = icsPrePath(FCPath + "\\" + SrvOpt->Vals["RootCertFile"]);
        SSLHandle->SSLOptions->CertFile = icsPrePath(FCPath + "\\" + SrvOpt->Vals["CertFile"]);
        SSLHandle->SSLOptions->KeyFile  = icsPrePath(FCPath + "\\" + SrvOpt->Vals["KeyFile"]);
        SSLHandle->OnGetPassword        = SSLHandleGetPassword;
        FServer->IOHandler              = SSLHandle;
       }
//      ShowMessage("7 >>> ");
      MsgLog->LogLevel = SrvOpt->Vals["logLevel"].AsIntDef(1);
//      ShowMessage("8 >>> ");
//      PlanerLog->LogLevel  = SrvOpt->Vals["logLevel"].AsIntDef(1);
//      ShowMessage("9 >>> ");
//      RequestLog->LogLevel = SrvOpt->Vals["logLevel"].AsIntDef(1);
//      ShowMessage("10 >>> ");
     }
//      ShowMessage("11 >>> ");
    FServer->Bindings->Clear();
//      ShowMessage("12 >>> ");
    FServer->DefaultPort = FPort;
//      ShowMessage("13 >>> ");
/*
    TTagNode * pid = new TTagNode;
    pid->Name = "pid";
    try
     {
      UnicodeString pp;
      FAdmUser   = icsNewGUID().UpperCase();
      FAdmPasswd = icsNewGUID().UpperCase();
      pp.printf(L"%04X-", FPort);
      pid->AddChild("i")->AV["PCDATA"] = pp + FAdmUser;
      pp.printf(L"%04X-", 65535 - FPort);
      pid->AddChild("i")->AV["PCDATA"] = pp + FAdmPasswd;
      pid->SaveToZIPXMLFile(icsPrePath(FCPath + "\\pid"), "");
     }
    __finally
     {
      delete pid;
     }
     */
    ICServer->HideDSAdmin = true;
    FServer->Active = true;
    // ClearTimer->Enabled = true;
//      ShowMessage("14 >>> ");
    try
     {
      FDSServer                = ICServer;
//      ShowMessage("15 >>> ");
      FDSAuthenticationManager = DSAuthentication;
      FXMLList->GetXML("001D3500-00005882-DC28");
      FXMLList->GetXML("0E291426-00005882-2493");
      FXMLList->GetXML("12063611-00008cd7-cd89");
      FXMLList->GetXML("1232303C-0000A6C8-46DC");
      FXMLList->GetXML("4031DA5E-8AC52FB3-7D03");
      FXMLList->GetXML("40381E23-92155860-4448");
      FXMLList->GetXML("454A13CD-BE856CB1-E252");
      FXMLList->GetXML("549F0EBE-1B9B98A7-BD64");
      FXMLList->GetXML("569E1EB2-D0CFEF0F-BE9A");
      FXMLList->GetXML("57E3D087-D0CFEF0F-1A84");
      FXMLList->GetXML("5BCAE819-D0CFEF0F-0B87");
      FXMLList->GetXML("ICS_APP_OPT");
//      ShowMessage("16 >>> ");
      ICServer->Start();
//      ShowMessage("16 >>> ");
      if (!FAdminClass)
       FAdminClass = new TdsAdminClass(NULL, FXMLList, FPort);
//      ShowMessage("17 >>> ");
      AdmClass()->User = FAdmUser;
//      ShowMessage("18 >>> ");
      AdmClass()->Pwd = FAdmPasswd;
//      ShowMessage("19 >>> ");
      AdmClass()->OnSesClose = FClearSes;
//      ShowMessage("20 >>> ");
     }
    catch (Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
    dsLogMessage("Server started", __FUNC__);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::Stop()
 {
  dsLogMessage("Server stoped", __FUNC__);
  // ClearTimer->Enabled = false;
  if (TDSSessionManager::Instance != NULL)
   {
    TDSSessionManager::Instance->TerminateAllSessions();
   }
  FServer->Active = false;
  FServer->Bindings->Clear();
  ICServer->Stop();
  delete  FXMLList;
  FXMLList = NULL;
  SSLHandle->OnGetPassword = NULL;
  FServer->IOHandler       = NULL;
 }
// ---------------------------------------------------------------------------
bool __fastcall TAppBase::FLoadXML(TTagNode * ItTag, UnicodeString & Src)
 {
  dsLogBegin(__FUNC__);
  dsLogMessage(Src, __FUNC__, 1);
  bool RC = false;
  try
   {
    UnicodeString xmlPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\" + Src + ".zxml");
    if (FileExists(xmlPath))
     {
      ItTag->LoadFromZIPXMLFile(xmlPath);
      RC = true;
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::SrvOptLoadXML(TTagNode * AOptNode)
 {
  UnicodeString FFN = icsPrePath(FCPath + "\\" + UnicodeString(_OPT_DEF_FILE_NAME_));
  if (FileExists(FFN))
   {
    AOptNode->LoadFromZIPXMLFile(FFN);
   }
  else
   dsLogError("Ошибка загрузки описания настроек '" + UnicodeString(_OPT_DEF_FILE_NAME_) + "'", __FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::SSLHandleGetPassword(UnicodeString & Password)
 {
  Password = SrvOpt->Vals["SSLKeyPass"].AsStringDef("").Trim();
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::FClearSes(UnicodeString ASesId)
 {
  TStringList * FSesList = new TStringList;
  dsLogBegin(__FUNC__);
  try
   {
    TClassUnitMap::iterator FRC;
    dsLogMessage(ASesId, "");
    FRC = FDocMap.find(ASesId);
    if (FRC != FDocMap.end())
     {
      delete FRC->second;
      FDocMap.erase(ASesId);
      dsLogMessage(ASesId, "FDocMap");
     }
    if (TDSSessionManager::Instance->Session[ASesId])
     {
      dsLogMessage(ASesId, "Instance->Session[ASesId]");
      TDSSessionManager::Instance->Session[ASesId]->Terminate();
      TDSSessionManager::Instance->RemoveSession(ASesId);
     }
   }
  __finally
   {
    delete FSesList;
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::ClearTimerTimer(TObject * Sender)
 { // очистка сессий
  TStringList * FSesList = new TStringList;
  dsLogBegin(__FUNC__);
  try
   {
    TDSSessionManager::Instance->GetOpenSessionKeys(FSesList);
    FSesList->Text = FSesList->Text.UpperCase();
    TClassUnitMap::iterator i;
    for (i = FDocMap.begin(); i != FDocMap.end(); i++)
     {
      if (FSesList->IndexOf(i->first.UpperCase()) == -1)
       {
        dsLogMessage(i->first, "FDocMap");
        delete i->second;
       }
     }
   }
  __finally
   {
    delete FSesList;
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsSyncLogMessage(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsSyncLogError(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  dsSyncLogSQL(ASQL, AFuncName, AExecTime, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogBegin(UnicodeString AFuncName)
 {
  dsLogBegin(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogEnd(UnicodeString AFuncName)
 {
  dsLogEnd(AFuncName);
 }
// ---------------------------------------------------------------------------
 void __fastcall TAppBase::CreateLogs()
  {
   // err log
   if (ErrLog)
    delete ErrLog;
   ErrLog = new TLog("docerr");
   // msg log
   if (MsgLog)
    delete MsgLog;
   MsgLog           = new TLog("docmsg");
   MsgLog->LogLevel = 9;
   // auth log
   if (AuthLog)
    delete AuthLog;
   AuthLog = new TLog("docauth");
   // Planer Log
   // RequestLog
   if (RequestLog)
    delete RequestLog;
   RequestLog           = new TLog("docrequest");
   RequestLog->LogLevel = 9;
   // CostructLog
   if (CostructLog)
    delete CostructLog;
   CostructLog           = new TLog("doccostruct");
   CostructLog->LogLevel = 9;
  }
 // ---------------------------------------------------------------------------
 void __fastcall TAppBase::DeleteLogs()
  {
   // err log
   if (ErrLog)
    delete ErrLog;
   ErrLog = NULL;
   // msg log
   if (MsgLog)
    delete MsgLog;
   MsgLog = NULL;
   // auth log
   if (AuthLog)
    delete AuthLog;
   AuthLog = NULL;
   // RequestLog
   if (RequestLog)
    delete RequestLog;
   RequestLog = NULL;
   // CostructLog
   if (CostructLog)
    delete CostructLog;
   CostructLog = NULL;
  }
 // ---------------------------------------------------------------------------

