//---------------------------------------------------------------------------
#ifndef MainSrvcH
#define MainSrvcH
//---------------------------------------------------------------------------
#include <System.SysUtils.hpp>
#include <Classes.hpp>
#include <SvcMgr.hpp>
#include <vcl.h>
//---------------------------------------------------------------------------
class TAppBase;
class TICDocServer : public TService
{
__published:    // IDE-managed Components
 void __fastcall ServiceStart(TService *Sender, bool &Started);
 void __fastcall ServiceDestroy(TObject *Sender);
 void __fastcall ServiceAfterInstall(TService *Sender);

private:        // User declarations

protected:
  TAppBase* FServer;

  bool __fastcall DoStop(void);
  bool __fastcall DoPause(void);
  bool __fastcall DoContinue(void);
  void __fastcall DoInterrogate(void);

public:         // User declarations
  __fastcall TICDocServer(TComponent* Owner);
  TServiceController __fastcall GetServiceController(void);

  friend void __stdcall ServiceController(unsigned CtrlCode);
};
//---------------------------------------------------------------------------
extern PACKAGE TICDocServer *ICDocServer;
//---------------------------------------------------------------------------
#endif

