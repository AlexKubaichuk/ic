#ifndef srvsettingH
#define srvsettingH
//---------------------------------------------------------------------------
#define _CONFSETTTING

#define REG_USE_KLADR
#define REG_USE_ORG
#define REG_USE_DOC

#define REG_USE_IMM_NOM

#define _SERVER_PORT_ 5110
#define _PORTNAME_ "port"
#define _OPT_FILE_NAME_ "docsrv_opt.xml"
#define _OPT_DEF_FILE_NAME_ "defxml\\srvoptdef.zxml"
//---------------------------------------------------------------------------
#endif
