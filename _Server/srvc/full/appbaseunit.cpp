﻿// ----------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include <stdio.h>
#include <memory>
#include <Datasnap.DSHTTP.hpp>
// ---------------------------------------------------------------------------
#include "appbaseunit.h"
#include "icsLog.h"
#include "srvsetting.h"
#include <WebReq.hpp>
#pragma link "Web.WebReq"
#ifdef USEPACKAGES
#pragma link "IndySystem.bpi"
#pragma link "IndyCore.bpi"
#pragma link "IndyProtocols.bpi"
#else
#pragma comment(lib, "IndySystem")
#pragma comment(lib, "IndyCore")
#pragma comment(lib, "IndyProtocols")
#endif
#pragma link "IdHTTPWebBrokerBridge"
// ---------------------------------------------------------------------------
// #include "dsRegUnitsUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma resource "*.dfm"
// ---------------------------------------------------------------------------
TDSServer * FDSServer;
TDSAuthenticationManager * FDSAuthenticationManager;
// ---------------------------------------------------------------------------
extern PACKAGE TComponentClass WebModuleClass;
// ---------------------------------------------------------------------------
TAppBase * __fastcall TAppBase::AppServer()
 {
//  LogMessage("Before new TAppBase()", EVENTLOG_INFORMATION_TYPE);
  static TAppBase * RC = new TAppBase();
//  LogMessage("After new TAppBase()", EVENTLOG_INFORMATION_TYPE);
  return RC;
 }
// ---------------------------------------------------------------------------
__fastcall TAppBase::TAppBase() : TDataModule(NULL)
 {
  CreateLogs();
  FAdmUser   = "";
  FAdmPasswd = "";
  FCPath     = icsPrePath(ExtractFilePath(ParamStr(0)));
  if (WebRequestHandler() != NULL)
   {
    WebRequestHandler()->WebModuleClass = WebModuleClass;
   }
  FServer = new TIdHTTPWebBrokerBridge(NULL);
  // FServer->Active = false;
  // FServer->RegisterWebModuleClass(WebModuleClass);
  FPort = _SERVER_PORT_;
  FAdminClass    = NULL;
  FMISSyncModule = NULL;
  MKB            = new TdsMKB;
 }
// ---------------------------------------------------------------------------
__fastcall TAppBase::~TAppBase(void)
 {
  delete FAdminClass;
  FAdminClass = NULL;
  delete FMISSyncModule;
  FMISSyncModule = NULL;
  Stop();
  Disconnect();
  FDSServer                = NULL;
  FDSAuthenticationManager = NULL;
  DeleteLogs();
  delete FServer;
  FServer = NULL;
  delete MKB;
 }
// ----------------------------------------------------------------------------
TdsAdminClass * __fastcall TAppBase::AdmClass()
 {
  return ((TdsAdminClass *)FAdminClass);
 }
// ----------------------------------------------------------------------------
void __fastcall TAppBase::DSAuthenticationUserAuthenticate(TObject * Sender, const UnicodeString Protocol,
  const UnicodeString Context, const UnicodeString User, const UnicodeString Password, bool & valid,
  TStrings * UserRoles)
 {
  bool FValid = false;
  UnicodeString FUserName = User;
  valid = (FAdmUser == User) && (FAdmPasswd == Password);
  if (valid)
   FUserName = "admin";
  else
   {
    valid = AdmClass()->CheckUser(User, Password, FValid);
    if (valid)
     {
      valid = (valid == FValid);
     }
   }
  if (valid)
   {
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes)
     {
      UserRoles->Add("UPS=" + Password);
      dsLogAuth("[UserAuthenticate] успех: {" + IntToStr(FSes->Id) + "}" + Protocol + "/" + Context + " '" +
        FUserName + "'");
     }
    else
     dsLogAuth("[UserAuthenticate] успех: " + Protocol + "/" + Context + " '" + FUserName + "'");
   }
  else
   {
    if ((User == "check") && (Password == "check"))
     valid = true;
    else
     dsLogAuth("[UserAuthenticate] отказ: " + Protocol + "/" + Context + " '" + FUserName + "'");
   }
  if (valid)
   AdmClass()->LoginUser("");
 }
// ----------------------------------------------------------------------------
void __fastcall TAppBase::DSAuthenticationUserAuthorize(TObject * Sender,
  TDSAuthorizeEventObject * AuthorizeEventObject, bool & valid)
 {
  valid = false;
  TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
  if (FSes)
   {
    valid = (AuthorizeEventObject->MethodAlias == "DSAdmin.GetServerMethodParameters") ||
      (AuthorizeEventObject->MethodAlias == "DSAdmin.GetPlatformName") ||
      (AuthorizeEventObject->MethodAlias == "DSAdmin.DescribeMethod");
    if (!valid)
     {
      // dsLogMessage(FSes->UserRoles->Text,__FUNC__);
      // if (FSes->UserRoles->Count < 2)
      // {
      valid = (AuthorizeEventObject->MethodAlias == "TdsAdminClass.LoginUser");
      if (!valid)
       valid = AdmClass()->CheckRT(FSes->UserRoles->Text, AuthorizeEventObject->MethodAlias);
      // }
      // else
      // valid = AdmClass()->CheckRT(FSes->UserRoles->Text, AuthorizeEventObject->MethodAlias);
     }
    if (!valid)
     {
      UnicodeString FUserName = AuthorizeEventObject->UserName;
      if (FUserName == FAdmUser)
       FUserName = "admin";
      dsLogAuth("отказ в доступе: {" + IntToStr(FSes->Id) + "." + AuthorizeEventObject->MethodAlias + "} '" +
        FUserName + "'");
     }
    // else
    // dsLogAuth("доступ : {" + IntToStr(FSes->Id) + "." + AuthorizeEventObject->MethodAlias + "} '" + AuthorizeEventObject->UserName + "'");
   }
  // valid = true;
 }
// ---------------------------------------------------------------------------
TDSServer * DSServer(void)
 {
  return FDSServer;
 }
// ---------------------------------------------------------------------------
TDSAuthenticationManager * DSAuthenticationManager(void)
 {
  return FDSAuthenticationManager;
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::Disconnect()
 {
  // TICMap FIcMap;
  TClassUnitMap::iterator i;
  for (i = FIcMap.begin(); i != FIcMap.end(); i++)
   delete i->second;
  FIcMap.clear();
  // TDocMap FDocMap;
  for (i = FDocMap.begin(); i != FDocMap.end(); i++)
   delete i->second;
  FDocMap.clear();
  // TVSMap FVSMap;
  for (i = FVSMap.begin(); i != FVSMap.end(); i++)
   delete i->second;
  FVSMap.clear();
  // TKLADRMap FKLADRMap;
  for (i = FKLADRMap.begin(); i != FKLADRMap.end(); i++)
   delete i->second;
  FKLADRMap.clear();
  // TOrgMap FOrgMap;
  for (i = FOrgMap.begin(); i != FOrgMap.end(); i++)
   delete i->second;
  FOrgMap.clear();
  // TOrgMap FEIDataMap;
  for (i = FEIDataMap.begin(); i != FEIDataMap.end(); i++)
   delete i->second;
  FEIDataMap.clear();
  // TOrgMap FMISAPIMap
  for (i = FMISAPIMap.begin(); i != FMISAPIMap.end(); i++)
   delete i->second;
  FMISAPIMap.clear();
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::ICServerConnect(TDSConnectEventObject * DSConnectEventObject)
 {
  // TStrings * FProp = DSConnectEventObject->ConnectProperties->Properties;
  // dsLogMessage(" >>> connect " + FProp->Values["HostName"] + ":" + FProp->Values["Port"] + " user: " +
  // FProp->Values["DSAuthenticationUser"] + " DN: " + FProp->Values["DriverName"], __FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::ICServerDisconnect(TDSConnectEventObject * DSConnectEventObject)
 {
  // TStrings * FProp = DSConnectEventObject->ConnectProperties->Properties;
  // dsLogMessage(" <<< disconnect " + FProp->Values["HostName"] + ":" + FProp->Values["Port"] + " user: " +
  // FProp->Values["DSAuthenticationUser"] + " DN: " + FProp->Values["DriverName"], __FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::ICServerError(TDSErrorEventObject * DSErrorEventObject)
 {
  dsLogError(DSErrorEventObject->Error->Message, __FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::ICServerPrepare(TDSPrepareEventObject * DSPrepareEventObject)
 {
  // dsLogMessage("ICServerPrepare: "+DSPrepareEventObject->UserName);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                            TdsICClass                                      #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsICClassClassGetClass(TDSServerClass * DSServerClass, TPersistentClass & PersistentClass)
 {
  dsLogBegin(__FUNC__);
  PersistentClass = __classid(TdsICClass);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::CheckBaseCount(int ACount)
 {
  TStringList * OpenSesList = new TStringList;
  TStringList * OpenBaseList = new TStringList;
  try
   {
    if (!((int)(ACount / 100)))
     throw EBaseAppException(Now().FormatString(" --- dd.mm.yyyy hh.nn.ss"));
    TDSSession * FSes;
    int idx;
    TDSSessionManager::Instance->GetOpenSessionKeys(OpenSesList);
    if ((int)(OpenSesList->Count / ((int)(ACount / 100))))
     throw EBaseAppException(Now().FormatString(" -- dd.mm.yyyy hh.nn.ss"));
    for (int i = 0; i < OpenSesList->Count; i++)
     {
      FSes = TDSSessionManager::Instance->Session[OpenSesList->Strings[i]];
      if (FSes->Status == Active)
       {
        idx = OpenBaseList->IndexOf(FSes->UserRoles->Values["ICDB"].UpperCase());
        if (idx == -1)
         OpenBaseList->Add(FSes->UserRoles->Values["ICDB"].UpperCase());
       }
     }
    if (OpenBaseList->Count / (ACount - (ACount / 100) * 100))
     throw EBaseAppException(Now().FormatString(" - dd.mm.yyyy hh.nn.ss"));
   }
  __finally
   {
    delete OpenSesList;
    delete OpenBaseList;
   }
 }
// ---------------------------------------------------------------------------
TDataModule * __fastcall TAppBase::CreateICClass()
 {
  dsLogBegin(__FUNC__);
  TDataModule * RC = NULL;
  try
   {
    RC = new TdsICClass(NULL, FPort, MKB, FXMLList);
    CheckBaseCount(((TdsICClass *)RC)->CCode.ToIntDef(0));
   }
  catch (EBaseAppException & E)
   {
    delete RC;
    RC = NULL;
    dsLogError("Нарушение ограничения лицензии.", __FUNC__);
    throw;
   }
  catch (Sysutils::Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
    throw;
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsICClassClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassCreateInstance(DSCreateInstanceEventObject, FIcMap, CreateICClass, "dsICClassClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsICClassClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassDestroyInstance(DSDestroyInstanceEventObject, FIcMap, "dsICClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                            TdsDocClass                                     #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsDocClassGetClass(TDSServerClass * DSServerClass, TPersistentClass & PersistentClass)
 {
  dsLogBegin(__FUNC__);
  PersistentClass = __classid(TdsDocClass);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
TDataModule * __fastcall TAppBase::CreateDocClass()
 {
  dsLogBegin(__FUNC__);
  dsLogEnd(__FUNC__);
  return new TdsDocClass(NULL, MKB, FXMLList);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsDocClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassCreateInstance(DSCreateInstanceEventObject, FDocMap, CreateDocClass, "dsDocClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsDocClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassDestroyInstance(DSDestroyInstanceEventObject, FDocMap, "dsDocClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                            TdsVacStoreClass                                #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsVacStoreClassGetClass(TDSServerClass * DSServerClass, TPersistentClass & PersistentClass)
 {
  dsLogBegin(__FUNC__);
  PersistentClass = __classid(TdsVacStoreClass);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
TDataModule * __fastcall TAppBase::CreateVacStoreClass()
 {
  dsLogBegin(__FUNC__);
  dsLogEnd(__FUNC__);
  return new TdsVacStoreClass(NULL, FXMLList);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsVacStoreClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassCreateInstance(DSCreateInstanceEventObject, FVSMap, CreateVacStoreClass, "dsVacStoreClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsVacStoreClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassDestroyInstance(DSDestroyInstanceEventObject, FVSMap, "dsVacStoreClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                            TdsKLADRClass                                   #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsKLADRClassGetClass(TDSServerClass * DSServerClass, TPersistentClass & PersistentClass)
 {
  dsLogBegin(__FUNC__);
  PersistentClass = __classid(TdsKLADRClass);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
TDataModule * __fastcall TAppBase::CreateKLADRClass()
 {
  dsLogBegin(__FUNC__);
  dsLogEnd(__FUNC__);
  return new TdsKLADRClass(NULL, NULL);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsKLADRClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassCreateInstance(DSCreateInstanceEventObject, FKLADRMap, CreateKLADRClass, "dsKLADRClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsKLADRClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassDestroyInstance(DSDestroyInstanceEventObject, FKLADRMap, "dsKLADRClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                            TdsOrgClass                                     #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsOrgClassGetClass(TDSServerClass * DSServerClass, TPersistentClass & PersistentClass)
 {
  dsLogBegin(__FUNC__);
  PersistentClass = __classid(TdsOrgClass);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
TDataModule * __fastcall TAppBase::CreateOrgClass()
 {
  dsLogBegin(__FUNC__);
  dsLogEnd(__FUNC__);
  return new TdsOrgClass(NULL, FXMLList->GetXML("5BCAE819-D0CFEF0F-0B87"));
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsOrgClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassCreateInstance(DSCreateInstanceEventObject, FOrgMap, CreateOrgClass, "dsOrgClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsOrgClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassDestroyInstance(DSDestroyInstanceEventObject, FOrgMap, "dsOrgClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                            TdsEIDataClass                                  #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
TDataModule * __fastcall TAppBase::CreateEIDataClass()
 {
  dsLogBegin(__FUNC__);
  dsLogEnd(__FUNC__);
  return new TdsEIDataClass(NULL, FPort, FXMLList);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsEIDataClassGetClass(TDSServerClass * DSServerClass, TPersistentClass & PersistentClass)
 {
  dsLogBegin(__FUNC__);
  PersistentClass = __classid(TdsEIDataClass);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsEIDataClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassCreateInstance(DSCreateInstanceEventObject, FEIDataMap, CreateEIDataClass, "dsEIDataClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsEIDataClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassDestroyInstance(DSDestroyInstanceEventObject, FEIDataMap, "dsEIDataClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                            TdsAdminClass                                  #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
void __fastcall TAppBase::DSAdminClassGetClass(TDSServerClass * DSServerClass, TPersistentClass & PersistentClass)
 {
  dsLogBegin(__FUNC__);
  PersistentClass = __classid(TdsAdminClass);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::DSAdminClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  try
   {
    DSCreateInstanceEventObject->ServerClassInstance = FAdminClass;
   }
  catch (Sysutils::Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::DSAdminClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  try
   {
    DSDestroyInstanceEventObject->ServerClassInstance = NULL;
   }
  catch (Sysutils::Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                            TMISAPI                                         #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
TDataModule * __fastcall TAppBase::CreateMISAPIClass()
 {
  dsLogBegin(__FUNC__);
  dsLogEnd(__FUNC__);
  return new TMISAPI(NULL, MKB, FXMLList);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISAPIClassGetClass(TDSServerClass * DSServerClass, TPersistentClass & PersistentClass)
 {
  dsLogBegin(__FUNC__);
  PersistentClass = __classid(TMISAPI);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISAPIClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassCreateInstance(DSCreateInstanceEventObject, FMISAPIMap, CreateMISAPIClass, "MISAPIClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISAPIClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassDestroyInstance(DSDestroyInstanceEventObject, FMISAPIMap, "MISAPIClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
////******************************************************************************
void __fastcall TAppBase::ClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject,
  TClassUnitMap & AUnitMap, TCreateModule ACreateNewInstance, const UnicodeString AClassName)
 {
  dsLogBegin(__FUNC__);
  try
   {
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes && (FSes->Status != Terminated))
     {
      FSes->LifeDuration = 28800000; // 8h //86400000; //24h
      UnicodeString FAuthUser = DSCreateInstanceEventObject->ServerConnectionHandler->ConProperties->Values
        ["DSAuthenticationUser"];
      if (FAuthUser.Length())
       {
        if (FSes->UserName.UpperCase() == FAuthUser.UpperCase())
         {
          UnicodeString FDB = FSes->UserRoles->Values["ICDB"];
          TClassUnitMap::iterator FRC = AUnitMap.find(FSes->SessionName /* Id */);
          TDataModule * tmpIC = NULL;
          if (FRC != AUnitMap.end())
           tmpIC = FRC->second;
          else
           {
            try
             {
              tmpIC = ACreateNewInstance();
              dsLogMessage("create " + AClassName + ": " + FSes->Id, __FUNC__, 5);
             }
            catch (Sysutils::Exception & E)
             {
              dsLogError(" in new : " + E.Message, __FUNC__, 5);
             }
           }
          if (tmpIC)
           {
            AUnitMap[FSes->SessionName /* Id */] = tmpIC;
            DSCreateInstanceEventObject->ServerClassInstance = tmpIC;
           }
         }
        else
         dsLogError("Имя пользователя отличается от имени пользователя сессии.", __FUNC__);
       }
      else
       dsLogError("Ошибка определения имени пользователя [" +
        DSCreateInstanceEventObject->ServerConnectionHandler->ConProperties->Properties->Text + "]", __FUNC__);
     }
   }
  catch (Sysutils::Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes)
     FClearSes(FSes->SessionName);
    DSCreateInstanceEventObject->ServerClassInstance = NULL;
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::ClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject,
  TClassUnitMap & AUnitMap, const UnicodeString AClassName)
 {
  dsLogBegin(__FUNC__);
  try
   {
    DSDestroyInstanceEventObject->ServerClassInstance = NULL;
    /* for (TClassUnitMap::iterator i = AUnitMap.begin(); i != AUnitMap.end(); i++)
     {
     TDSSession * FSes = TDSSessionManager::Instance->Session[i->first];
     if (FSes)
     {
     dsLogMessage("delete " + AClassName + ": " + i->first, __FUNC__, 5);
     delete i->second;
     AUnitMap.erase(i->first);
     }
     } */
   }
  catch (Sysutils::Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void TerminateThreads(void)
 {
  if (TDSSessionManager::Instance != NULL)
   {
    TDSSessionManager::Instance->TerminateAllSessions();
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TAppBase::FGetActive()
 {
  bool RC = false;
  try
   {
    if (FServer)
     RC = FServer->Active;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TAppBase::LoadOpt()
 {
  bool RC = false;
  try
   {
    try
     {
      UnicodeString FXMLPath = icsPrePath(ExtractFilePath(ParamStr(0)));
      UnicodeString FFN = icsPrePath(FXMLPath + "\\conf\\" + UnicodeString(_OPT_FILE_NAME_));
      if (!FileExists(FFN))
       {
        ForceDirectories(FXMLPath);
        TTagNode * tmp = new TTagNode;
        try
         {
          tmp->Encoding = "utf-8";
          tmp->Name     = "g";
          tmp->SaveToXMLFile(FFN, "");
         }
        __finally
         {
          delete tmp;
         }
       }
      SrvOptXMLData->FileName = FFN;
      SrvOpt->LoadXML();
      RC = true;
     }
    catch (Sysutils::Exception & E)
     {
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::Start()
 {
  if (!FServer->Active)
   {
    if (!FXMLList)
     FXMLList = new TAxeXMLContainer;
    FXMLList->OnExtGetXML = FLoadXML;
    if (LoadOpt())
     {
      FPort = SrvOpt->Vals[_PORTNAME_].AsIntDef(5100);
      if (SrvOpt->Vals["usessl"].AsBool)
       {
        if (SrvOpt->Vals["RootCertFile"].AsString.Trim().Length())
         SSLHandle->SSLOptions->RootCertFile = icsPrePath(FCPath + "\\" + SrvOpt->Vals["RootCertFile"]);
        SSLHandle->SSLOptions->CertFile = icsPrePath(FCPath + "\\" + SrvOpt->Vals["CertFile"]);
        SSLHandle->SSLOptions->KeyFile  = icsPrePath(FCPath + "\\" + SrvOpt->Vals["KeyFile"]);
        SSLHandle->OnGetPassword        = SSLHandleGetPassword;
        FServer->IOHandler              = SSLHandle;
       }
      MsgLog->LogLevel = SrvOpt->Vals["logLevel"].AsIntDef(1);
      PlanerLog->LogLevel  = SrvOpt->Vals["logLevel"].AsIntDef(1);
      RequestLog->LogLevel = SrvOpt->Vals["logLevel"].AsIntDef(1);
     }
    FServer->Bindings->Clear();
    FServer->DefaultPort = FPort;
    TTagNode * pid = new TTagNode;
    pid->Name = "pid";
    try
     {
      UnicodeString pp;
      FAdmUser   = icsNewGUID().UpperCase();
      FAdmPasswd = icsNewGUID().UpperCase();
      pp.printf(L"%04X-", FPort);
      pid->AddChild("i")->AV["PCDATA"] = pp + FAdmUser;
      pp.printf(L"%04X-", 65535 - FPort);
      pid->AddChild("i")->AV["PCDATA"] = pp + FAdmPasswd;
      pid->SaveToZIPXMLFile(icsPrePath(FCPath + "\\pid"), "");
     }
    __finally
     {
      delete pid;
     }
    ICServer->HideDSAdmin = true;
    FServer->Active = true;
    // ClearTimer->Enabled = true;
    try
     {
      FDSServer                = ICServer;
      FDSAuthenticationManager = DSAuthentication;
      FXMLList->GetXML("001D3500-00005882-DC28");
      FXMLList->GetXML("0E291426-00005882-2493");
      FXMLList->GetXML("12063611-00008cd7-cd89");
      FXMLList->GetXML("1232303C-0000A6C8-46DC");
      FXMLList->GetXML("4031DA5E-8AC52FB3-7D03");
      FXMLList->GetXML("40381E23-92155860-4448");
      FXMLList->GetXML("454A13CD-BE856CB1-E252");
      FXMLList->GetXML("549F0EBE-1B9B98A7-BD64");
      FXMLList->GetXML("569E1EB2-D0CFEF0F-BE9A");
      FXMLList->GetXML("57E3D087-D0CFEF0F-1A84");
      FXMLList->GetXML("5BCAE819-D0CFEF0F-0B87");
      FXMLList->GetXML("ICS_APP_OPT");
      ICServer->Start();
      if (!FAdminClass)
       FAdminClass = new TdsAdminClass(NULL, FXMLList, FPort);
      AdmClass()->User = FAdmUser;
      AdmClass()->Pwd = FAdmPasswd;
      AdmClass()->OnSesClose = FClearSes;
      if (!FMISSyncModule)
       {
        FMISSyncModule               = new TMISSyncManager(FXMLList);
        FMISSyncModule->OnLogMessage = MISOnLogMessage;
        FMISSyncModule->OnLogError   = MISOnLogError;
        FMISSyncModule->OnLogSQL     = MISOnLogSQL;
        FMISSyncModule->OnLogBegin   = MISOnLogBegin;
        FMISSyncModule->OnLogEnd     = MISOnLogEnd;
       }
      if (SrvOpt->Vals["syncBegTime"].AsString.Length())
       FMISSyncModule->SyncStartTime = TTime(SrvOpt->Vals["syncBegTime"].AsString);
      if (SrvOpt->Vals["syncEndTime"].AsString.Length())
       FMISSyncModule->SyncStopTime = TTime(SrvOpt->Vals["syncEndTime"].AsString);
      FMISSyncModule->StartSync();
     }
    catch (Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
    dsLogMessage("Server started", __FUNC__);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::Stop()
 {
  dsLogMessage("Server stoped", __FUNC__);
  // ClearTimer->Enabled = false;
  if (FMISSyncModule)
   {
    FMISSyncModule->StopSync();
    FMISSyncModule->OnLogMessage = NULL;
    FMISSyncModule->OnLogError   = NULL;
    FMISSyncModule->OnLogSQL     = NULL;
    FMISSyncModule->OnLogBegin   = NULL;
    FMISSyncModule->OnLogEnd     = NULL;
   }
  if (TDSSessionManager::Instance != NULL)
   {
    TDSSessionManager::Instance->TerminateAllSessions();
   }
  delete FMISSyncModule;
  FMISSyncModule  = NULL;
  FServer->Active = false;
  FServer->Bindings->Clear();
  ICServer->Stop();
  delete  FXMLList;
  FXMLList = NULL;
  SSLHandle->OnGetPassword = NULL;
  FServer->IOHandler       = NULL;
 }
// ---------------------------------------------------------------------------
bool __fastcall TAppBase::FLoadXML(TTagNode * ItTag, UnicodeString & Src)
 {
  dsLogBegin(__FUNC__);
  dsLogMessage(Src, __FUNC__, 1);
  bool RC = false;
  try
   {
    UnicodeString xmlPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\" + Src + ".zxml");
    if (FileExists(xmlPath))
     {
      ItTag->LoadFromZIPXMLFile(xmlPath);
      RC = true;
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::SrvOptLoadXML(TTagNode * AOptNode)
 {
  UnicodeString FFN = icsPrePath(FCPath + "\\" + UnicodeString(_OPT_DEF_FILE_NAME_));
  if (FileExists(FFN))
   {
    AOptNode->LoadFromZIPXMLFile(FFN);
   }
  else
   dsLogError("Ошибка загрузки описания настроек '" + UnicodeString(_OPT_DEF_FILE_NAME_) + "'", __FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::SSLHandleGetPassword(UnicodeString & Password)
 {
  Password = SrvOpt->Vals["SSLKeyPass"].AsStringDef("").Trim();
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::FClearSes(UnicodeString ASesId)
 {
  TStringList * FSesList = new TStringList;
  dsLogBegin(__FUNC__);
  try
   {
    TClassUnitMap::iterator FRC;
    dsLogMessage(ASesId, "");
    FRC = FIcMap.find(ASesId);
    if (FRC != FIcMap.end())
     {
      delete FRC->second;
      FIcMap.erase(ASesId);
      dsLogMessage(ASesId, "FIcMap");
     }
    FRC = FDocMap.find(ASesId);
    if (FRC != FDocMap.end())
     {
      delete FRC->second;
      FDocMap.erase(ASesId);
      dsLogMessage(ASesId, "FDocMap");
     }
    FRC = FVSMap.find(ASesId);
    if (FRC != FVSMap.end())
     {
      delete FRC->second;
      FVSMap.erase(ASesId);
      dsLogMessage(ASesId, "FVSMap");
     }
    FRC = FKLADRMap.find(ASesId);
    if (FRC != FKLADRMap.end())
     {
      delete FRC->second;
      FKLADRMap.erase(ASesId);
      dsLogMessage(ASesId, "FKLADRMap");
     }
    FRC = FOrgMap.find(ASesId);
    if (FRC != FOrgMap.end())
     {
      delete FRC->second;
      FOrgMap.erase(ASesId);
      dsLogMessage(ASesId, "FOrgMap");
     }
    FRC = FEIDataMap.find(ASesId);
    if (FRC != FEIDataMap.end())
     {
      delete FRC->second;
      FEIDataMap.erase(ASesId);
      dsLogMessage(ASesId, "FEIDataMap");
     }
    FRC = FMISAPIMap.find(ASesId);
    if (FRC != FMISAPIMap.end())
     {
      delete FRC->second;
      FMISAPIMap.erase(ASesId);
      dsLogMessage(ASesId, "FMISAPIMap");
     }
    if (TDSSessionManager::Instance->Session[ASesId])
     {
      dsLogMessage(ASesId, "Instance->Session[ASesId]");
      TDSSessionManager::Instance->Session[ASesId]->Terminate();
      TDSSessionManager::Instance->RemoveSession(ASesId);
     }
   }
  __finally
   {
    delete FSesList;
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::ClearTimerTimer(TObject * Sender)
 { // очистка сессий
  TStringList * FSesList = new TStringList;
  dsLogBegin(__FUNC__);
  try
   {
    TDSSessionManager::Instance->GetOpenSessionKeys(FSesList);
    FSesList->Text = FSesList->Text.UpperCase();
    TClassUnitMap::iterator i;
    for (i = FIcMap.begin(); i != FIcMap.end(); i++)
     {
      if (FSesList->IndexOf(i->first.UpperCase()) == -1)
       {
        dsLogMessage(i->first, "FIcMap");
        delete i->second;
       }
     }
    for (i = FDocMap.begin(); i != FDocMap.end(); i++)
     {
      if (FSesList->IndexOf(i->first.UpperCase()) == -1)
       {
        dsLogMessage(i->first, "FDocMap");
        delete i->second;
       }
     }
    for (i = FVSMap.begin(); i != FVSMap.end(); i++)
     {
      if (FSesList->IndexOf(i->first.UpperCase()) == -1)
       {
        dsLogMessage(i->first, "FVSMap");
        delete i->second;
       }
     }
    for (i = FKLADRMap.begin(); i != FKLADRMap.end(); i++)
     {
      if (FSesList->IndexOf(i->first.UpperCase()) == -1)
       {
        dsLogMessage(i->first, "FKLADRMap");
        delete i->second;
       }
     }
    for (i = FOrgMap.begin(); i != FOrgMap.end(); i++)
     {
      if (FSesList->IndexOf(i->first.UpperCase()) == -1)
       {
        dsLogMessage(i->first, "FOrgMap");
        delete i->second;
       }
     }
    for (i = FEIDataMap.begin(); i != FEIDataMap.end(); i++)
     {
      if (FSesList->IndexOf(i->first.UpperCase()) == -1)
       {
        dsLogMessage(i->first, "FEIDataMap");
        delete i->second;
       }
     }
    for (i = FMISAPIMap.begin(); i != FMISAPIMap.end(); i++)
     {
      if (FSesList->IndexOf(i->first.UpperCase()) == -1)
       {
        dsLogMessage(i->first, "FMISAPIMap");
        delete i->second;
       }
     }
   }
  __finally
   {
    delete FSesList;
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::DSTCPServerTransportConnect(TDSTCPConnectEventObject & Event)
 {
  dsLogMessage(">>>", __FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::DSTCPServerTransportDisconnect(TDSTCPDisconnectEventObject Event)
 {
  dsLogMessage("<<<", __FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsSyncLogMessage(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsSyncLogError(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  dsSyncLogSQL(ASQL, AFuncName, AExecTime, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogBegin(UnicodeString AFuncName)
 {
  dsLogBegin(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogEnd(UnicodeString AFuncName)
 {
  dsLogEnd(AFuncName);
 }
// ---------------------------------------------------------------------------
 void __fastcall TAppBase::CreateLogs()
  {
   // err log
   if (ErrLog)
    delete ErrLog;
   ErrLog = new TLog("err");
   // msg log
   if (MsgLog)
    delete MsgLog;
   MsgLog           = new TLog("msg");
   MsgLog->LogLevel = 9;
   // auth log
   if (AuthLog)
    delete AuthLog;
   AuthLog = new TLog("auth");
   // Planer Log
   if (PlanerLog)
    delete PlanerLog;
   PlanerLog           = new TLog("planer");
   PlanerLog->LogLevel = 9;
   // RequestLog
   if (RequestLog)
    delete RequestLog;
   RequestLog           = new TLog("request");
   RequestLog->LogLevel = 9;
   // SyncLog
   if (SyncLog)
    delete SyncLog;
   SyncLog           = new TLog("sync");
   SyncLog->LogLevel = 9;
   // CostructLog
   if (CostructLog)
    delete CostructLog;
   CostructLog           = new TLog("costruct");
   CostructLog->LogLevel = 9;
  }
 // ---------------------------------------------------------------------------
 void __fastcall TAppBase::DeleteLogs()
  {
   // err log
   if (ErrLog)
    delete ErrLog;
   ErrLog = NULL;
   // msg log
   if (MsgLog)
    delete MsgLog;
   MsgLog = NULL;
   // auth log
   if (AuthLog)
    delete AuthLog;
   AuthLog = NULL;
   // Planer Log
   if (PlanerLog)
    delete PlanerLog;
   PlanerLog = NULL;
   // RequestLog
   if (RequestLog)
    delete RequestLog;
   RequestLog = NULL;
   // SyncLog
   if (SyncLog)
    delete SyncLog;
   SyncLog = NULL;
   // CostructLog
   if (CostructLog)
    delete CostructLog;
   CostructLog = NULL;
  }
 // ---------------------------------------------------------------------------

