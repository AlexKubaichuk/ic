//---------------------------------------------------------------------------
#ifndef icsLogH
#define icsLogH

#include <Classes.hpp>
#include "kabBaseLog.h"
#include <Datasnap.DSSession.hpp>
#include "ExtUtils.h"
#include "XMLContainer.h"

//---------------------------------------------------------------------------
namespace kabLog
{
extern PACKAGE TLog *ErrLog;
extern PACKAGE TLog *MsgLog;
extern PACKAGE TLog *AuthLog;
extern PACKAGE TLog *PlanerLog;
extern PACKAGE TLog *RequestLog;
extern PACKAGE TLog *CostructLog;
extern PACKAGE TLog *SyncLog;

extern PACKAGE void __fastcall dsLogMessage(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
extern PACKAGE void __fastcall dsLogWarning(UnicodeString AMessage, UnicodeString  AFuncName = "", int ALvl = 1);
extern PACKAGE void __fastcall dsLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl = 1);
extern PACKAGE void __fastcall dsLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl = 1);

extern PACKAGE void __fastcall dsLogBegin(UnicodeString AFuncName, int ALvl = 5);
extern PACKAGE void __fastcall dsLogEnd(UnicodeString AFuncName, int ALvl = 5);
extern PACKAGE void __fastcall dsLogAuth(UnicodeString AMessage, int ALvl = 1);

extern PACKAGE void __fastcall dsPlanLogMessage(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 5);
extern PACKAGE void __fastcall dsPlanLogWarning(UnicodeString AMessage, UnicodeString  AFuncName = "", int ALvl = 1);
extern PACKAGE void __fastcall dsPlanLogSQL(UnicodeString AMessage, UnicodeString AFuncName, TTime AExecTime, int ALvl = 1);
extern PACKAGE void __fastcall dsPlanLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl = 1);

extern PACKAGE void __fastcall dsRequestLogMessage(UnicodeString AMessage, UnicodeString AFuncName);

extern PACKAGE void __fastcall dsSyncLogMessage(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl = 1);
extern PACKAGE void __fastcall dsSyncLogSQL(UnicodeString  ASQL, UnicodeString  AFuncName, TTime AExecTime, int ALvl = 1);
extern PACKAGE void __fastcall dsSyncLogError(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl = 1);


extern PACKAGE void __fastcall dsLogC(UnicodeString AFuncName, bool AEnd = false);
extern PACKAGE void __fastcall dsLogD(UnicodeString AFuncName, bool AEnd = false);

}; // end of namespace kabLog
using namespace kabLog;
//---------------------------------------------------------------------------

#endif
