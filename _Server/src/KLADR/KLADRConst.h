//---------------------------------------------------------------------------

#ifndef KLADRConstH
#define KLADRConstH
//---------------------------------------------------------------------------
#include <Classes.hpp>
//---------------------------------------------------------------------------
extern PACKAGE const UnicodeString kladrFormatVer;
extern PACKAGE const UnicodeString kcHouseEditMask;
extern PACKAGE const UnicodeString kcVldEditMask;
extern PACKAGE const UnicodeString kcKorpEditMask;
extern PACKAGE const UnicodeString kcBuildEditMask;
extern PACKAGE const UnicodeString kcFlatEditMask;
extern PACKAGE const int MSC_ID;
extern PACKAGE const int SPB_ID;

extern PACKAGE const UnicodeString apVld;
extern PACKAGE const UnicodeString apKorp;
extern PACKAGE const UnicodeString apBuild;

#endif
