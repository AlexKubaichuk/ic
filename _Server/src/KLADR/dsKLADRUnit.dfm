object dsKLADRClass: TdsKLADRClass
  OldCreateOrder = False
  Height = 248
  Width = 375
  object KLADRConnection: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'CharacterSet=UTF8'
      'User_Name=sysdba'
      'Port=3050'
      'Server=localhost'
      'Password=masterkey'
      'Pooled=False')
    FormatOptions.AssignedValues = [fvFmtDisplayDateTime, fvFmtDisplayDate, fvFmtDisplayTime]
    FormatOptions.FmtDisplayDateTime = 'dd.mm.yyyy hh:nn:ss'
    FormatOptions.FmtDisplayDate = 'dd.mm.yyyy'
    FormatOptions.FmtDisplayTime = 'hh:nn:ss'
    TxOptions.AutoStart = False
    TxOptions.AutoStop = False
    TxOptions.DisconnectAction = xdRollback
    LoginPrompt = False
    Left = 34
    Top = 20
  end
end
