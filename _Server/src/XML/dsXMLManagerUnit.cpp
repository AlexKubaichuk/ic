//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsXMLManagerUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


//---------------------------------------------------------------------------
__fastcall TXMLManager::TXMLManager()
  : TAxeXMLContainer()
{
  dsLogBegin(__FUNC__);
  OnExtGetXML = FLoadXML;
  dsLogEnd(__FUNC__);
}
//---------------------------------------------------------------------------
__fastcall TXMLManager::~TXMLManager()
{
  dsLogBegin(__FUNC__);

  dsLogEnd(__FUNC__);
}
//---------------------------------------------------------------------------
bool __fastcall TXMLManager::FLoadXML(TTagNode *ItTag, UnicodeString &Src)
{
  dsLogBegin(__FUNC__);
  bool RC = false;
  try
   {
     UnicodeString xmlPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\"+Src+".zxml");
     if (FileExists(xmlPath))
      {
        ItTag->LoadFromZIPXMLFile(xmlPath);
        RC = true;
      }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
}
//---------------------------------------------------------------------------

