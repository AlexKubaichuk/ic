//---------------------------------------------------------------------------

#ifndef OrgParsersH
#define OrgParsersH
//---------------------------------------------------------------------------
#include <System.SysUtils.hpp>
#include <Classes.hpp>
//#include <cxLabel.hpp>
//#include <cxControls.hpp>
//#include <cxStyles.hpp>
//#include "cxDropDownEdit.hpp"
//#include <cxTL.hpp>
//#include "cxButtons.hpp"
#include <cxHint.hpp>

//#define _DEBUG_MESSAGE

#include "kabCustomParsersTypeDef.h"

#include "AxeUtil.h"
#include "OrgConst.h"
//---------------------------------------------------------------------------
using namespace std;

//---------------------------------------------------------------------------
enum class TOrgTypeValue {Base = 0, NF = 1, Main = 2, Control = 3};
struct PACKAGE orgCode
{
    int           FormatVer;  // ������ �������
    TOrgTypeValue Type;       // 0 - �������������, 1 - ���������������
    __int64       OrgID;      // ��� �����������
    int           OKVED;      // ��� �����
    UnicodeString L1Val;      // |
    UnicodeString L2Val;      // | -> �������� �������
    UnicodeString L3Val;      // |
    UnicodeString Format;      // ���������
    __int64       Period;     // ������� ������

    __fastcall orgCode ();
    void __fastcall Clear ();
    UnicodeString __fastcall AsShortString();
    UnicodeString __fastcall AsString ();
    UnicodeString __fastcall LevelValStr();
    void operator = (orgCode AVal);
    bool operator != (orgCode AVal);
};
//---------------------------------------------------------------------------
struct PACKAGE orgFormats
{
    UnicodeString id;      // ���
    UnicodeString L1Val;      // |
    UnicodeString L2Val;      // | -> �������� �������
    UnicodeString L3Val;      // |
    orgFormats() {id = L1Val = L2Val = L3Val = "";};
};
//---------------------------------------------------------------------------
class PACKAGE EOrgError : public Exception
{
public:
    __fastcall EOrgError(const UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE TOrgCacheRecord : public TObject
{
private:
  TTagNode *FFormatDef;
  void __fastcall AddLevelFormats(TTagNode * AFormatDef, TStringList *AFormatList);
  void __fastcall GetOrgStrFormatByOKVED(TTagNode* AFormatDef, int AOKVED, UnicodeString AFormatCode, TStringList *AFormatList);
public:
    int Status;
    UnicodeString Values[6];

    __fastcall TOrgCacheRecord(TTagNode *AFormatDef);
    UnicodeString __fastcall GetText(UnicodeString Params = "11111111", TTagNode *AFormatDef = NULL);
};
//---------------------------------------------------------------------------
class PACKAGE TNFOrgWords : public TStringList
{
private:
    UnicodeString FText;
    StringIntMap *FResWords;
//    StrStrMap    FReplasedObjects;

    void __fastcall setText(UnicodeString ASrc);
    UnicodeString __fastcall getWord(int AIdx);
    void __fastcall setWord(int AIdx, UnicodeString AVal);
    UnicodeString __fastcall getPreparedText();
    UnicodeString __fastcall getLast();
    UnicodeString __fastcall getFirst();

    bool __fastcall IsNotBr(wchar_t ASrc);
    bool __fastcall IsLBr(wchar_t ASrc);
    bool __fastcall IsRBr(wchar_t ASrc);
    bool __fastcall Cmp(UnicodeString ASrc);
    bool __fastcall CanClear(UnicodeString ASrc);

    ParseWordType __fastcall getType(int AIdx);
    ParseWordType __fastcall getFirstType();
    ParseWordType __fastcall getLastType();

public:
    __property UnicodeString Word[int AIdx] = {read = getWord, write = setWord};
    __property UnicodeString Last           = {read = getLast};
    __property UnicodeString First          = {read = getFirst};

    __property ParseWordType WordType[int AIdx] = {read = getType};
    __property ParseWordType FirstType          = {read = getFirstType};
    __property ParseWordType LastType           = {read = getLastType};

    __property UnicodeString Text           = {read = FText, write=setText};
    __property UnicodeString PreparedText   = {read = getPreparedText};

    __fastcall TNFOrgWords(UnicodeString ASrc);
    __fastcall ~TNFOrgWords();
};

//---------------------------------------------------------------------------
class PACKAGE TNFOrgItems : public TStringList
{
private:
    UnicodeString FText;
    wchar_t    FSeparator;
    wchar_t    FDopSeparator;
    bool       FExtended;
    void __fastcall setText(UnicodeString ASrc);

    TNFOrgWords* __fastcall getItem(int AIdx);
    TNFOrgWords* __fastcall getLast();
    TNFOrgWords* __fastcall getFirst();
public:
    __property UnicodeString Text              = {read = FText, write=setText};

    __property TNFOrgWords* Item[int AIdx] = {read = getItem};

    __property TNFOrgWords* Last           = {read = getLast};
    __property TNFOrgWords* First          = {read = getFirst};

    __fastcall TNFOrgItems(UnicodeString ASrc, wchar_t ASeparator = ',', bool AExtended = false);
    __fastcall ~TNFOrgItems();
};
//---------------------------------------------------------------------------
UnicodeString __fastcall DivideLevelVal(UnicodeString ASrc, UnicodeString &AVal);
extern PACKAGE int __fastcall GetLvlType(UnicodeString AVal);
extern PACKAGE bool __fastcall IsLvlCorrectType(int AType, UnicodeString AVal);
//extern PACKAGE void __fastcall ShowDebug(UnicodeString AText);
extern PACKAGE orgCode __fastcall ParseOrgCodeStr (UnicodeString AOrgStr, bool ATemplateMode = false);
extern PACKAGE bool __fastcall GetOrgLvlStruct(UnicodeString ASrcStr, int &AOKVED, UnicodeString &AL1, UnicodeString &AL2, UnicodeString &AL3, UnicodeString &AFormat, __int64 &APeriod);
//extern PACKAGE void __fastcall GetOrgStrFormatByOKVED(TTagNode* AFormatDef, int AOKVED, UnicodeString AFormatCode, TStringList *AFormatList);
extern PACKAGE void __fastcall GetOrgFormatTypeByOKVED(TTagNode* AFormatDef, int AOKVED, int & ALvl1, int & ALvl2, int & ALvl3);
extern PACKAGE orgCode __fastcall ParseOrgLvlStruct(TTagNode* AFormatDef, TNFOrgItems *AValues, int ALvlIdx, orgCode AOrg);
//---------------------------------------------------------------------------
#endif
