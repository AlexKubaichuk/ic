//---------------------------------------------------------------------------

#pragma hdrstop

#include "TimeChecker.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall TTimeChecker::TTimeChecker()
{
  FBeg = FCur = Time();
  FValStr = "";
}
//---------------------------------------------------------------------------
__fastcall TTimeChecker::~TTimeChecker()
{
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TTimeChecker::GetStr(UnicodeString AMessage)
{
  return AMessage+" ["+(Time()-FBeg).FormatString("ss.zzz")+"] "+FValStr;
}
//---------------------------------------------------------------------------
void __fastcall TTimeChecker::Check(UnicodeString AMessage)
{
  if (AMessage.Length())
   FValStr += AMessage+" ["+(Time()-FCur).FormatString("ss.zzz")+"]";
  FCur = Time();
}
//---------------------------------------------------------------------------
