//---------------------------------------------------------------------------
#ifndef dsBackGroundExecutorH
#define dsBackGroundExecutorH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
//#include "DocCreatorDM.h"
//#include "DocExDFM.h"

#include "dsDocDMUnit.h"
#include "dsDocSQLCreator.h"
//#include "doctypes.h"
//---------------------------------------------------------------------------

typedef  map<int,int> TCodeList;
typedef  map<const UnicodeString, TCodeList*> TStrTreeMap;

//ctNone - ���,
//ctEmpty - ������,
//ctBase - �� ����,
//ctDoc - �� ���������,
//ctBaseDoc  - �� ���� � �� ���������};

//---------------------------------------------------------------------------
class PACKAGE TdsExecuter : public TObject
{
private:
  typedef void __fastcall (__closure *TdcSyncMethod)(void);

  TTagNode *FErrMsgNode;

  TReplaceFlags fdRepl;

  UnicodeString FLogMsg, FLogFuncName;
  int FLogALvl;

  TTime         FLogExecTime;

  TkabProgress FOnProgress;

  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;
  TOnLogSQLEvent      FOnLogSQL;

  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;


  TkabProgressType FPrType;
  int              FPrVal;
  UnicodeString    FPrMsg;


  void __fastcall SyncCall(TdcSyncMethod AMethod);
  void __fastcall SyncCallOnProgress();

  void __fastcall LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl = 1);
  void __fastcall SyncCallLogMsg();
  void __fastcall LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl = 1);
  void __fastcall SyncCallLogErr();
  void __fastcall LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime = 0, int ALvl = 1);
  void __fastcall SyncCallLogSQL();
  void __fastcall LogBegin(UnicodeString AFuncName);
  void __fastcall SyncCallLogBegin();
  void __fastcall LogEnd(UnicodeString AFuncName);
  void __fastcall SyncCallLogEnd();

  virtual bool __fastcall ExecuteProc();

  UnicodeString __fastcall DateFS(TDateTime ADate);
  void          __fastcall FSetProgress(TkabProgressType AType = TkabProgressType::StageInc, int AVal = 0, UnicodeString AMsg = "");

protected:
  class TBackgroundExecuter;
  TBackgroundExecuter * FBkgCreator;
public:		// User declarations
  __fastcall TdsExecuter();
  __fastcall ~TdsExecuter();
  UnicodeString __fastcall StartExecute();

  __property TkabProgress      OnProgress = {read=FOnProgress, write=FOnProgress};

  __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
  __property TOnLogMsgErrEvent OnLogError   = {read = FOnLogError, write = FOnLogError};
  __property TOnLogSQLEvent    OnLogSQL     = {read = FOnLogSQL, write = FOnLogSQL};

  __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
  __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};
};
//---------------------------------------------------------------------------
#endif
//---------------------------------------------------------------------------

