//---------------------------------------------------------------------------

#ifndef kabCustomParsersTypeDefH
#define kabCustomParsersTypeDefH
//---------------------------------------------------------------------------
#include <System.SysUtils.hpp>
#include <System.Classes.hpp>

#include <map>
//---------------------------------------------------------------------------
using namespace std;
//---------------------------------------------------------------------------
typedef enum {wtLBR, wtRBR, wtWord, wtNone} ParseWordType;
typedef map <UnicodeString, int > StringIntMap;
typedef map <UnicodeString, UnicodeString> StrStrMap;

#endif
