
//---------------------------------------------------------------------------
#ifndef WMDefH
#define WMDefH
//---------------------------------------------------------------------------
#include <System.SysUtils.hpp>
#include <System.Classes.hpp>
#include <Web.HTTPApp.hpp>
#include <Data.DBXCommon.hpp>
#include <DataSnap.DSCommonServer.hpp>
#include <DataSnap.DSHTTPCommon.hpp>
#include <DataSnap.DSHTTPWebBroker.hpp>
#include <DataSnap.DSServer.hpp>
#include <Web.HTTPProd.hpp>
#include <DataSnap.DSAuth.hpp>
#include <Datasnap.DSClientMetadata.hpp>
#include <Datasnap.DSHTTP.hpp>
#include <Datasnap.DSMetadata.hpp>
#include <Datasnap.DSProxyDispatcher.hpp>
#include <Datasnap.DSProxyJavaScript.hpp>
#include <Datasnap.DSServerMetadata.hpp>
#include <IPPeerServer.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <IPPeerClient.hpp>
#include <DSProxyCppRest.hpp>
#include <System.JSON.hpp>
//---------------------------------------------------------------------------
class TWMDefDM : public TWebModule
{
__published:	// IDE-managed Components
  TDSRESTWebDispatcher *DSRESTWebDispatcher;
	TPageProducer *ServerFunctionInvoker;
  TWebFileDispatcher *WebFileDispatcher;
  TDSProxyGenerator *DSProxyGenerator;
  TDSServerMetaDataProvider *DSServerMetaData;
  TDSProxyDispatcher *DSProxyDispatcher;
 TPageProducer *Index;
	void __fastcall ServerFunctionInvokerHTMLTag(TObject *Sender, TTag Tag, const UnicodeString TagString,
		  TStrings *TagParams, UnicodeString &ReplaceText);
	void __fastcall WebModuleDefaultAction(TObject *Sender, TWebRequest *Request, TWebResponse *Response,
		  bool &Handled);
	void __fastcall WebModuleBeforeDispatch(TObject *Sender, TWebRequest *Request, TWebResponse *Response,
		  bool &Handled);
	void __fastcall WebFileDispatcherBeforeDispatch(TObject *Sender, const UnicodeString AFileName,
		  TWebRequest *Request, TWebResponse *Response, bool &Handled);
	void __fastcall WebModuleCreate(TObject *Sender);
 void __fastcall DSRESTWebDispatcherHTTPTrace(TObject *Sender, TDSHTTPContext *AContext,
          TDSHTTPRequest *ARequest, TDSHTTPResponse *AResponse);
 void __fastcall WebModuleException(TObject *Sender, Sysutils::Exception *E, bool &Handled);
 void __fastcall WMDefDMAURAImportPatientAction(TObject *Sender, TWebRequest *Request,
          TWebResponse *Response, bool &Handled);
 void __fastcall WMDefDMMISCommonImportPatientAction(TObject *Sender, TWebRequest *Request,
          TWebResponse *Response, bool &Handled);
 void __fastcall WMDefDMMISCommonPatientF63Action(TObject *Sender, TWebRequest *Request,
          TWebResponse *Response, bool &Handled);
 void __fastcall IndexHTMLTag(TObject *Sender, TTag Tag, const UnicodeString TagString,
          TStrings *TagParams, UnicodeString &ReplaceText);






private:	// User declarations
	TWebActionItem* FServerFunctionInvokerAction;
	bool __fastcall AllowServerFunctionInvoker(void);
  UnicodeString __fastcall DSHTTPRequestToString(TDSHTTPRequest *ARequest);
  UnicodeString __fastcall DSHTTPResponseToString(TDSHTTPResponse *AResponse);

  UnicodeString __fastcall WebRequestToShortString(TWebRequest *Request);
  UnicodeString __fastcall WebRequestToString(TWebRequest *Request);
public:		// User declarations
	__fastcall TWMDefDM(TComponent* Owner);
};
//---------------------------------------------------------------------------
#endif

