//---------------------------------------------------------------------------

#ifndef KabCustomDSRowH
#define KabCustomDSRowH

//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Data.DB.hpp>
//#include "cxGridTableView.hpp"
//#include "cxGridCardView.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
//#include "kabCustomDSTypeDef.h"
//#include "Variant.hpp"
//#include "icsVariant.h"
//---------------------------------------------------------------------------
class PACKAGE EkabCustomDataSetRowError : public System::Sysutils::Exception
{
public:
	 __fastcall EkabCustomDataSetRowError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE TkabCustomDataSetRow : public TObject
{
private:
  typedef map<UnicodeString, Variant> TkabCustomDataSetFieldsMap;
  TkabCustomDataSetFieldsMap *FDataFields;
  TkabCustomDataSetFieldsMap *FStrDataFields;
  bool FFetched;
  void __fastcall NewRec(TTagNode *AFlDef);

  void       __fastcall FSetValue(UnicodeString AName, const Variant& AVal);
  Variant    __fastcall FGetValue(UnicodeString AName);

  void       __fastcall FSetStrValue(UnicodeString AName, const Variant &AVal);
  Variant    __fastcall FGetStrValue(UnicodeString AName);

public:
  __fastcall TkabCustomDataSetRow(TTagNode *AFlDef);
  __fastcall TkabCustomDataSetRow(TTagNode *AFlDef, TkabCustomDataSetRow *ASrc);
  __fastcall ~TkabCustomDataSetRow();

  __property bool Fetched = {read=FFetched, write=FFetched};
  __property Variant Value[UnicodeString AName]    = {read=FGetValue, write=FSetValue};
  __property Variant StrValue[UnicodeString AName] = {read=FGetStrValue, write=FSetStrValue};
};
//---------------------------------------------------------------------------
#endif

