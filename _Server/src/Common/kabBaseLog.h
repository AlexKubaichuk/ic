//---------------------------------------------------------------------------

#ifndef kabBaseLogH
#define kabBaseLogH

#include <Classes.hpp>
#include <Datasnap.DSSession.hpp>
#include "ExtUtils.h"
#include "XMLContainer.h"

//---------------------------------------------------------------------------
namespace kabLog
{
typedef void __fastcall (__closure *TOnLogMsgErrEvent)(UnicodeString AMessage, UnicodeString AFuncName, int ALvl);
typedef void __fastcall (__closure *TOnLogBegEndEvent)  (UnicodeString AFuncName);
typedef void __fastcall (__closure *TOnLogSQLEvent)  (UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl);
//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                                 TLog                                  ##
//##                                                                       ##
//###########################################################################

// ��������� ������
enum TLogCompleteResult
{
  crSucceeded,
  crTerminated,
  crFatalError
};

//---------------------------------------------------------------------------

// ������ (� �������������� ������� ������� :) )

class TLog : public TObject
{
private:
    bool FActive;
    bool FCalcWECount;
    UnicodeString FFileName;
    UnicodeString FLogName, FLogPath;
    TDate      FCreateDate;
    TStream*   FStream;         // ������ ������� (� ������ ��� � �����)
    int        FWarningCount;
    int        FErrorCount;
    int        FLogLevel;
    class TBackgroundZIP;
    TBackgroundZIP * FBackgroundZIP;

    inline void __fastcall FreeData();
    void __fastcall ZIPLogFile(UnicodeString AFileName);


protected:
    void __fastcall CheckActive();
    UnicodeString __fastcall CurrentTime() const;
    UnicodeString __fastcall CurrentDate() const;
    TStream* __fastcall CreateFileStream(UnicodeString AFileName) const;
    void __fastcall Write(UnicodeString AStr);
    void __fastcall WriteLn(UnicodeString AStr);
    void __fastcall WriteLn();


public:
    __fastcall TLog(UnicodeString ALogName);
    virtual __fastcall ~TLog();

    // ������ ������� �� ������ � ����
//    void __fastcall Flush();
    void __fastcall LogMessage(UnicodeString AMessage, int ALvl = 1);
    void __fastcall LogWarning(UnicodeString AMessage, int ALvl = 1);
    void __fastcall LogError(UnicodeString AMessage, int ALvl = 1);

    // ������� ������������� �������� ���������� ������ � ��������������
    __property bool CalcWECount = {read = FCalcWECount, write = FCalcWECount};
    // ������� ���������� �������
//    __property bool Active = {read = FActive, write = SetActive};
    // ������� ������� ����
    __property int  LogLevel = {read = FLogLevel, write = FLogLevel};
    // ������ ��� ����� �������
    __property UnicodeString FileName = {read = FFileName, write = FFileName};
    // ���������� ������� � ���������������
    __property int  WarningCount = {read = FWarningCount};
    // ���������� ������� �� �������
    __property int  ErrorCount = {read = FErrorCount};
};
//---------------------------------------------------------------------------
class PACKAGE TkabLogHelper : public TObject
{
private:

  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;
  TOnLogSQLEvent      FOnLogSQL;

  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;

public:
  __fastcall TkabLogHelper();
  __fastcall ~TkabLogHelper();

  void __fastcall LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl = 1);
  void __fastcall LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl = 1);
  void __fastcall LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl = 1);
  void __fastcall LogBegin(UnicodeString AFuncName = 5);
  void __fastcall LogEnd(UnicodeString AFuncName = 5);

  __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
  __property TOnLogMsgErrEvent OnLogError   = {read = FOnLogError, write = FOnLogError};
  __property TOnLogSQLEvent    OnLogSQL     = {read = FOnLogSQL, write = FOnLogSQL};

  __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
  __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};
};
//---------------------------------------------------------------------------
extern PACKAGE UnicodeString __fastcall LogUser(TDSSession * ASes);
extern PACKAGE UnicodeString __fastcall PrintProcessMemoryInfo(DWORD AProcessID = 0);
//---------------------------------------------------------------------------
}; // end of namespace kabLog
using namespace kabLog;
//---------------------------------------------------------------------------

#endif
