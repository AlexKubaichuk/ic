// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsDBSupportDMUnit.h"
// #include "dsIC67Sync.h"
// #include "ICSDATEUTIL.hpp"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TdsDBSupportDM * dsDBSupportDM;
// ---------------------------------------------------------------------------
__fastcall TdsDBSupportDM::TdsDBSupportDM(TComponent * Owner) : TDataModule(Owner)
 {
  FTerminated = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  if (FOnLogMessage)
   FOnLogMessage(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  if (FOnLogError)
   FOnLogError(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  if (FOnLogSQL)
   FOnLogSQL(ASQL, AFuncName, AExecTime, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::LogBegin(UnicodeString AFuncName)
 {
  if (FOnLogBegin)
   FOnLogBegin(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::LogEnd(UnicodeString AFuncName)
 {
  if (FOnLogEnd)
   FOnLogEnd(AFuncName);
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsDBSupportDM::CreateTempQuery()
 {
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = IcConnection;
    FQ->Connection                       = IcConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  return FQ;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDBSupportDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDBSupportDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
  UnicodeString AParam1Name, Variant AParam1Val, UnicodeString ALogComment, UnicodeString AParam2Name,
  Variant AParam2Val, UnicodeString AParam3Name, Variant AParam3Val, UnicodeString AParam4Name, Variant AParam4Val)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
      if (AParam2Name.Length())
       AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
      if (AParam3Name.Length())
       AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
      if (AParam4Name.Length())
       AQuery->ParamByName(AParam4Name)->Value = AParam4Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      UnicodeString FParams = "; Params: ";
      if (AParam1Name.Length())
       FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
      if (AParam2Name.Length())
       FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
      if (AParam3Name.Length())
       FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
      if (AParam4Name.Length())
       FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "");
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::UpdateCardGenCode()
 {
  if (!FTerminated)
   FUpdateCardGenCode("3003");
  if (!FTerminated)
   FUpdateCardGenCode("1003");
  if (!FTerminated)
   FUpdateCardGenCode("102F");
  if (!FTerminated)
   FUpdateCardGenCode("1100");
  if (!FTerminated)
   FUpdateCardGenCode("3030");
  if (!FTerminated)
   FUpdateCardGenCode("1030");
  if (!FTerminated)
   FUpdateCardGenCode("1112");
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::FUpdateCardGenCode(UnicodeString AGenId)
 {
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    UnicodeString GenNum = "1";
    quExec(FQ, false, "SELECT Max(code) as MCODE FROM CLASS_" + AGenId);
    if (FQ->RecordCount)
     {
      if (!FQ->FieldByName("MCODE")->IsNull)
       GenNum = FQ->FieldByName("MCODE")->AsString;
     }
    quExec(FQ, false, "ALTER SEQUENCE GEN_" + AGenId + "_ID RESTART WITH " + GenNum);
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
 }
// ---------------------------------------------------------------------------
