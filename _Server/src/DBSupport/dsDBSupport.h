//---------------------------------------------------------------------------

#ifndef dsDBSupportH
#define dsDBSupportH

//#include "DKCfg.h"      // ��������� �������� ����������

#include <System.hpp>
#include <Classes.hpp>
//#include <DateUtils.hpp>

//#include "DKClasses.h"
//#include "XMLContainer.h"
//#include "ICSDATEUTIL.hpp"
//#include "DKUtils.h"

#include "dsDBSupportDMUnit.h"
//#include "dsMISSyncDMUnit.h"
//#include "PlanerDebug.h"

//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                              TdsDBSupport                                 ##
//##                                                                       ##
//###########################################################################

//---------------------------------------------------------------------------
class PACKAGE TdsDBSupport : public TObject
{
typedef void __fastcall (__closure *TdsSyncMethod)(void);
private:
    // �������� �� ��������
//    typedef list<__int64> TPlanedUnitList;
//    typedef list<TTagNode*> TTagNodeList;
//    typedef map<int,int> TIntMap;

  class TBackgroundDBSupport;
  TBackgroundDBSupport * FBkgSyncModule;
  bool FTerminated;
  bool FInSync;
  void __fastcall SyncCall(TdsSyncMethod AMethod);


  int             FMaxProgress;        // Max �������� ��� ProgressBar'�
  TNotifyEvent    FOnIncProgress;      // �������, ��� ��������� �������� ���������� �������
                                         // ��������� ������� ProgressBar'�
  UnicodeString FLogMsg, FLogFuncName;
  int FLogALvl;
  TTime         FLogExecTime;

  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;
  TOnLogSQLEvent      FOnLogSQL;

  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;


  inline void __fastcall CallOnIncProgress();
  void __fastcall SyncCallOnIncProgress();

  UnicodeString FStage;

  int FCurrProgress ;

  TdsDBSupportDM* FDM;


  UnicodeString __fastcall FGetDBId();
  void __fastcall FSetDBId(UnicodeString AValue);

protected:
  void __fastcall SyncCallLogMsg();
  void __fastcall SyncCallLogErr();
  void __fastcall SyncCallLogSQL();
  void __fastcall SyncCallLogBegin();
  void __fastcall SyncCallLogEnd();

  void __fastcall CallLogMsg(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogErr(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl = 1);
  void __fastcall CallLogBegin(UnicodeString AFuncName);
  void __fastcall CallLogEnd(UnicodeString AFuncName);

public:
    System::UnicodeString __fastcall StartSync();
    System::UnicodeString __fastcall StopSync();
    __property bool Terminated = {read = FTerminated};
    __property bool InSync = {read = FInSync};
    int __fastcall GetMaxProgress();
    int __fastcall GetCurProgress();
    UnicodeString __fastcall GetCurStage() const;

    __fastcall TdsDBSupport();
    virtual __fastcall ~TdsDBSupport();

    __property int          MaxProgress   = { read = FMaxProgress };
    __property TNotifyEvent OnIncProgress = { read = FOnIncProgress, write = FOnIncProgress };

    __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
    __property TOnLogMsgErrEvent OnLogError   = {read = FOnLogError, write = FOnLogError};
    __property TOnLogSQLEvent    OnLogSQL     = {read = FOnLogSQL, write = FOnLogSQL};

    __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
    __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};

  __property UnicodeString DBId = {read=FGetDBId, write=FSetDBId};

  System::UnicodeString __fastcall Sync();
};
//---------------------------------------------------------------------------
#endif
