object dsAdminClass: TdsAdminClass
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Height = 262
  Width = 495
  object IcConnection: TFDConnection
    Params.Strings = (
      'ConnectionDef=IC')
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    ResourceOptions.AssignedValues = [rvDirectExecute, rvAutoConnect, rvSilentMode]
    ResourceOptions.DirectExecute = True
    ResourceOptions.SilentMode = True
    ResourceOptions.AutoConnect = False
    TxOptions.AutoStart = False
    TxOptions.AutoStop = False
    LoginPrompt = False
    Transaction = FDTransaction1
    Left = 29
    Top = 15
  end
  object FDTransaction1: TFDTransaction
    Connection = IcConnection
    Left = 35
    Top = 82
  end
  object DocConnnect: TDSRestConnection
    Host = 'localhost'
    Port = 5100
    UserName = 'localhost'
    Password = 'localhost'
    LoginPrompt = False
    Left = 135
    Top = 27
    UniqueId = '{2561E080-5D09-4396-9D34-969204DAB404}'
  end
end
