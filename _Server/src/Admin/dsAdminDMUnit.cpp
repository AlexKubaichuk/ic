// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsAdminDMUnit.h"
// #include "ICSDATEUTIL.hpp"
// #include "PrePlaner.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TdsAdminDM * dsAdminDM;
// ---------------------------------------------------------------------------
__fastcall TdsAdminDM::TdsAdminDM(TComponent * Owner, TFDConnection * AConnection, TAxeXMLContainer * AXMLList)
  : TDataModule(Owner)
 {
  FConnection           = AConnection;
  FAdmUser              = "";
  FAdmUserPass          = "";
  FBaseXMLList          = AXMLList;
  FXMLList              = new TAxeXMLContainer;
  FXMLList->OnExtGetXML = FLoadXML;
 }
// ---------------------------------------------------------------------------
__fastcall TdsAdminDM::~TdsAdminDM()
 {
  // for (TdsSrvClassifDataMap::iterator i = FClassifData.begin(); i != FClassifData.end(); i++)
  // delete i->second;
  // FClassifData.clear();
  // delete FKLADR;
  delete FXMLList;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsAdminDM::PreloadData()
 {
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    TdsLPU tmpLpu;
    TdsUser tmpUser;
    TdsDB tmpDB;
    TdsRight tmpRT;
    FLPUMap.clear();
    FUserMap.clear();
    FSysUserMap.clear();
    FNameUserMap.clear();
    FDBMap.clear();
    FRightMap.clear();
    quExec(FQ, false, "Select * from class_00C7");
    while (!FQ->Eof)
     {
      tmpLpu.Code     = FQ->FieldByName("R011E")->AsString; // <text name='���' uid='011E'
      tmpLpu.Name     = FQ->FieldByName("R00CA")->AsString; // <text name='������������' uid='00CA'
      tmpLpu.FullName = FQ->FieldByName("R00CB")->AsString; // <text name='������ ������������' uid='00CB'
      tmpLpu.Addr     = FQ->FieldByName("R00CC")->AsString; // <extedit name='�����' uid='00CC'
      tmpLpu.OwnerFIO = FQ->FieldByName("R00CD")->AsString; // <text name='�.�.�. ������������' uid='00CD'
      tmpLpu.Phone    = FQ->FieldByName("R00CE")->AsString; // <text name='�������' uid='00CE'
      tmpLpu.EMail    = FQ->FieldByName("R00CF")->AsString; // <text name='��. �����' uid='00CF'
      FLPUMap[FQ->FieldByName("code")->AsInteger] = tmpLpu;
      FQ->Next();
     }
    quExec(FQ, false, "Select * from class_0002");
    while (!FQ->Eof)
     {
      tmpUser.Code                            = FQ->FieldByName("code")->AsInteger;
      tmpUser.Login                           = FQ->FieldByName("R0016")->AsString; // <text name='�����' uid='0016'
      tmpUser.Pass                            = FQ->FieldByName("R0017")->AsString; // <text name='������' uid='0017'
      tmpUser.Fam                             = FQ->FieldByName("R0013")->AsString; // <text name='�������' uid='0013'
      tmpUser.Name                            = FQ->FieldByName("R0014")->AsString; // <text name='���' uid='0014'
      tmpUser.SName                           = FQ->FieldByName("R0015")->AsString; // <text name='��������' uid='0015'
      FNameUserMap[tmpUser.Login.UpperCase()] = tmpUser;
      FUserMap[FQ->FieldByName("code")->AsInteger] = tmpUser;
      FQ->Next();
     }
    // dsLogMessage("1", __FUNC__, 1);
    quExec(FQ, false, "Select * from class_0008");
    // dsLogMessage("2", __FUNC__, 1);
    int itUserCode = -10;
    while (!FQ->Eof)
     {
      // dsLogMessage("3", __FUNC__, 1);
      tmpDB.LPUCode   = FQ->FieldByName("R0120")->AsInteger; // <choiceDB name='���' uid='0120'
      tmpDB.ICDBPath  = FQ->FieldByName("R0011")->AsString; // <extedit name='���� � �� ��-���' uid='0011'
      tmpDB.DocDBPath = FQ->FieldByName("R001D")->AsString;
      // dsLogMessage("31", __FUNC__, 1);
      // <extedit name='���� � �� ��-���-���������������' uid='001D'
      tmpDB.Comment = FQ->FieldByName("R000E")->AsString; // <text name='��������' uid='000E'
      FDBMap[FQ->FieldByName("code")->AsInteger] = tmpDB;
      // ******************************************************************************
      // *                                                                            *
      // *          ���������� ��������� �������������, ��� ���������� � ���          *
      // *                                                                            *
      // ******************************************************************************
      tmpUser.Code  = itUserCode;
      tmpUser.Login = FQ->FieldByName("R0125")->AsString; // <text name='�����' uid='0016'
      tmpUser.Pass  = FQ->FieldByName("R0126")->AsString; // <text name='������' uid='0017'
      tmpUser.Fam   = "system"; // <text name='�������' uid='0013'
      tmpUser.Name  = "system"; // <text name='���' uid='0014'
      tmpUser.SName = "system"; // <text name='��������' uid='0015'
      // dsLogMessage("32", __FUNC__, 1);
      FNameUserMap[tmpUser.Login.UpperCase()]      = tmpUser;
      FUserMap[FQ->FieldByName("code")->AsInteger] = tmpUser;
      FSysUserMap[tmpUser.Login.UpperCase()]       = tmpUser;
      // dsLogMessage("33", __FUNC__, 1);
      tmpRT.DBCode   = FQ->FieldByName("code")->AsInteger; // <���� ������' uid='0019'
      tmpRT.UserCode = itUserCode-- ; // <������������' uid='001A'
      // dsLogMessage("34", __FUNC__, 1);
      // tmpRT.Admin          = FQ->FieldByName("R001E")->AsInteger; // <�����������������' uid='001E' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'/>
      // tmpRT.AdminLPU       = FQ->FieldByName("R011F")->AsInteger; // <������� ������ ���' uid='011F' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'/>
      tmpRT.IC                                           = true; // <������ � ��-���' uid='003B'
      tmpRT.ICReg                                        = true; // <������������' uid='002A'
      tmpRT.ICRegUList                                   = true; // <������ � ������ ���������' uid='0033'
      tmpRT.ICRegUListEdit                               = true; // <��������������' uid='0036'
      tmpRT.ICRegCList                                   = true; // <������ � ������������' uid='0037'
      tmpRT.ICRegCListEdit                               = true; // <��������������' uid='0038'
      tmpRT.ICCard                                       = true; // <������ � ���. �����' uid='002B'
      tmpRT.ICCardEdit                                   = true; // <��������������' uid='0039'
      tmpRT.ICPlan                                       = true; // <������ � ������ ������' uid='002C'
      tmpRT.ICPlanPlaning                                = true; // <���������� ������������' uid='003A'
      tmpRT.Doc                                          = true; // <������ � "��-���������������"' uid='003C'
      tmpRT.DocSpec                                      = true; // <������ � ������ "�������� ����������" uid='002E'
      tmpRT.DocSpecEdit                                  = true; // <��������������' uid='003D'
      tmpRT.DocDoc                                       = true; // <������ � ������ ���������' uid='002D'
      tmpRT.DocDocEdit                                   = true; // <��������������' uid='003E'
      tmpRT.DocFilter                                    = true; // <������ � ������ �������' uid='002F'
      tmpRT.DocFilterEdit                                = true; // <��������������' uid='003F'
      tmpRT.VS                                           = true; // <����� ����' uid='0029'
      FRightMap[-1 * FQ->FieldByName("code")->AsInteger] = tmpRT;
      // dsLogMessage("35: "+IntToStr(tmpRT.UserCode), __FUNC__, 1);
      FQ->Next();
     }
    quExec(FQ, false, "Select * from class_0018");
    while (!FQ->Eof)
     {
      tmpRT.DBCode   = FQ->FieldByName("R0019")->AsInteger; // <���� ������' uid='0019'
      tmpRT.UserCode = FQ->FieldByName("R001A")->AsInteger; // <������������' uid='001A'
      // tmpRT.Admin          = FQ->FieldByName("R001E")->AsInteger; // <�����������������' uid='001E'
      // tmpRT.AdminLPU       = FQ->FieldByName("R011F")->AsInteger; // <������� ������ ���' uid='011F'
      tmpRT.IC             = FQ->FieldByName("R003B")->AsInteger; // <������ � ��-���' uid='003B'
      tmpRT.ICReg          = FQ->FieldByName("R002A")->AsInteger; // <������������' uid='002A'
      tmpRT.ICRegUList     = FQ->FieldByName("R0033")->AsInteger; // <������ � ������ ���������' uid='0033'
      tmpRT.ICRegUListEdit = FQ->FieldByName("R0036")->AsInteger; // <��������������' uid='0036'
      tmpRT.ICRegCList     = FQ->FieldByName("R0037")->AsInteger; // <������ � ������������' uid='0037'
      tmpRT.ICRegCListEdit = FQ->FieldByName("R0038")->AsInteger; // <��������������' uid='0038'
      tmpRT.ICCard         = FQ->FieldByName("R002B")->AsInteger; // <������ � ���. �����' uid='002B'
      tmpRT.ICCardEdit     = FQ->FieldByName("R0039")->AsInteger; // <��������������' uid='0039'
      tmpRT.ICPlan         = FQ->FieldByName("R002C")->AsInteger; // <������ � ������ ������' uid='002C'
      tmpRT.ICPlanPlaning  = FQ->FieldByName("R003A")->AsInteger; // <���������� ������������' uid='003A'
      tmpRT.Doc            = FQ->FieldByName("R003C")->AsInteger; // <������ � "��-���������������" uid='003C'
      tmpRT.DocSpec        = FQ->FieldByName("R002E")->AsInteger; // <������ � ������ "�������� ����������" uid='002E'
      tmpRT.DocSpecEdit    = FQ->FieldByName("R003D")->AsInteger; // <��������������' uid='003D'
      tmpRT.DocDoc         = FQ->FieldByName("R002D")->AsInteger; // <������ � ������ ���������' uid='002D'
      tmpRT.DocDocEdit     = FQ->FieldByName("R003E")->AsInteger; // <��������������' uid='003E'
      tmpRT.DocFilter      = FQ->FieldByName("R002F")->AsInteger; // <������ � ������ �������' uid='002F'
      tmpRT.DocFilterEdit  = FQ->FieldByName("R003F")->AsInteger; // <��������������' uid='003F'
      tmpRT.VS             = FQ->FieldByName("R0029")->AsInteger; // <����� ����' uid='0029'
      FRightMap[FQ->FieldByName("code")->AsInteger] = tmpRT;
      FQ->Next();
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsAdminDM::FLoadXML(TTagNode * ItTag, UnicodeString & Src)
 {
  bool RC = false;
  try
   {
    /*
     UnicodeString xmlPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\" + Src + ".zxml");
     if (FileExists(xmlPath))
     {
     ItTag->LoadFromZIPXMLFile(xmlPath);
     RC = true;
     }
     */
    ItTag->AsXML = FBaseXMLList->GetXML(Src)->AsXML;
    RC           = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsAdminDM::CreateDBCheckQuery()
 {
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = DBCheckConnection;
    FQ->Connection                       = DBCheckConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  return FQ;
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsAdminDM::CreateTempQuery()
 {
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = FConnection;
    FQ->Connection                       = FConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  return FQ;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsAdminDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsAdminDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsAdminDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString AParam1Name,
  Variant AParam1Val, UnicodeString ALogComment, UnicodeString AParam2Name, Variant AParam2Val,
  UnicodeString AParam3Name, Variant AParam3Val, UnicodeString AParam4Name, Variant AParam4Val,
  UnicodeString AParam5Name, Variant AParam5Val)
 {
  bool RC = false;
  TTime FBeg = Time();
  UnicodeString FParams = "; Params: ";
  if (AParam1Name.Length())
   FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
  if (AParam2Name.Length())
   FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
  if (AParam3Name.Length())
   FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
  if (AParam4Name.Length())
   FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "") + "; ";
  if (AParam5Name.Length())
   FParams += AParam5Name + "=" + VarToStrDef(AParam5Val, "");
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
      if (AParam2Name.Length())
       AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
      if (AParam3Name.Length())
       AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
      if (AParam4Name.Length())
       AQuery->ParamByName(AParam4Name)->Value = AParam4Val;
      if (AParam5Name.Length())
       AQuery->ParamByName(AParam5Name)->Value = AParam5Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL(((ALogComment.Length()) ? ALogComment : ASQL) + FParams, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TdsAdminDM::GetNewCode(UnicodeString TabId)
 {
  __int64 RC = -1;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    FQ->Command->CommandKind = skStoredProc;
    FQ->SQL->Text            = "MCODE_" + TabId;
    FQ->Prepare();
    FQ->OpenOrExecute();
    RC = FQ->FieldByName("MCODE")->AsInteger;
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsAdminDM::NewGUID()
 {
  return icsNewGUID().UpperCase();
 }
// ---------------------------------------------------------------------------
int __fastcall TdsAdminDM::FGetUC()
 {
  return FNameUserMap.size();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsAdminDM::FindUser(UnicodeString AUser, UnicodeString APass)
 {
  UnicodeString RC = "nouser";
  try
   {
    if (!FGetUC())
     {
      if (APass.Length())
       RC = APass;
      else
       RC = "";
     }
    else
     {
      TStrUserMap::iterator FRC;
      if (APass.Length())
       { // ������ ������
        FRC = FNameUserMap.find(AUser.UpperCase());
        if (FRC != FNameUserMap.end())
         {
          if (FRC->second.Pass == APass)
           RC = FRC->second.Pass;
          else
           RC = "nopass";
         }
       }
      else
       {
        FRC = FNameUserMap.find(AUser.UpperCase());
        if (FRC != FNameUserMap.end())
         RC = "";
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsAdminDM::FindSysUser(UnicodeString AUser, UnicodeString APass)
 {
  UnicodeString RC = "error";
  try
   {
    if (FSysUserMap.size())
     {
      TStrUserMap::iterator FRC;
      if (APass.Length())
       { // ������ ������
        FRC = FSysUserMap.find(AUser.UpperCase());
        if (FRC != FSysUserMap.end())
         {
          if (FRC->second.Pass == APass)
           RC = "";
          else
           RC = "error";
         }
       }
      else
       RC = "error";
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsAdminDM::AddRt(TTagNode * ARoot, bool AVal, UnicodeString AFlName)
 {
  TTagNode * rt = ARoot->AddChild("r");
  rt->AV["uid"] = AFlName;
  rt->AV["v"]   = IntToStr((int)AVal);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsAdminDM::GetUserRight(TDSSession * ASes, UnicodeString ADBId, UnicodeString & AICDB,
  UnicodeString & ADocDB, UnicodeString & AVSDB)
 {
  // dsLogBegin(__FUNC__);
  UnicodeString RC = "<nouser/>";
  TFDQuery * FQ = CreateTempQuery();
  TTagNode * RT = new TTagNode;
  RT->Name = "rt";
  try
   {
    // dsLogMessage("1 "+ASes->UserName, __FUNC__, 1);
    TStrUserMap::iterator FRC = FNameUserMap.find(ASes->UserName.UpperCase());
    // dsLogMessage("2", __FUNC__, 1);
    if (FRC != FNameUserMap.end())
     {
      // dsLogMessage("3", __FUNC__, 1);
      TdsUser tmpUser = FRC->second;
      int Count = 0;
      for (TIntRightMap::iterator iRt = FRightMap.begin(); iRt != FRightMap.end(); iRt++)
       {
        // dsLogMessage("4 "+IntToStr(iRt->second.UserCode)+" -> "+IntToStr(tmpUser.Code), __FUNC__, 1);
        if (iRt->second.UserCode == tmpUser.Code)
         Count++ ;
       }
      if (!Count)
       RC = "<access_denied/>";
      else
       {
        // ShowMessage(IntToStr(Count));
        TIntDBMap::iterator iDB;
        TIntLPUMap::iterator iLPU;
        TTagNode * itNode;
        bool FCont;
        if ((Count == 1) || ADBId.Length())
         {
          FCont = true;
          for (TIntRightMap::iterator iRt = FRightMap.begin(); (iRt != FRightMap.end()) && FCont; iRt++)
           {
            if (iRt->second.UserCode == tmpUser.Code)
             {
              // ShowMessage("iRt->second.UserCode == tmpUser.Code");
              iDB = FDBMap.find(iRt->second.DBCode);
              if (iDB != FDBMap.end())
               { // �������� �� ����������
                // ShowMessage("�������� �� ����������");
                iLPU = FLPUMap.find(iDB->second.LPUCode);
                if (iLPU != FLPUMap.end())
                 { // �������� ��� �������
                  // ShowMessage("�������� ��� �������");
                  if (ADBId.Length())
                   {
                    if (ADBId == "AdMin")
                     FCont = false; // !(iRt->second.Admin || iRt->second.AdminLPU);
                    else
                     FCont = (ADBId.UpperCase() != iLPU->second.Code.UpperCase());
                   }
                  else
                   FCont = false;
                  if (!FCont)
                   {
                    // ShowMessage(iDB->second.ICDBPath);
                    AICDB              = iDB->second.ICDBPath;
                    ADocDB             = iDB->second.DocDBPath;
                    AVSDB              = iDB->second.ICDBPath;
                    itNode             = RT->AddChild("db");
                    itNode->AV["id"]   = iLPU->second.Code;
                    itNode->AV["name"] = iLPU->second.Name;
                    // AddRt(itNode, iRt->second.Admin, "001E");          //�����������������' uid='001E'/>
                    // AddRt(itNode, iRt->second.AdminLPU, "011F");       //������� ������ ���' uid='011F'/>
                    AddRt(itNode, iRt->second.IC, "003B"); // ������ � ��-���' uid='003B'/>
                    AddRt(itNode, iRt->second.ICReg, "002A"); // ������������' uid='002A'>
                    AddRt(itNode, iRt->second.ICRegUList, "0033"); // ������ � ������ ���������' uid='0033'>
                    AddRt(itNode, iRt->second.ICRegUListEdit, "0036"); // ��������������' uid='0036'>
                    AddRt(itNode, iRt->second.ICRegCList, "0037"); // ������ � ������������' uid='0037'>
                    AddRt(itNode, iRt->second.ICRegCListEdit, "0038"); // ��������������' uid='0038'>
                    AddRt(itNode, iRt->second.ICCard, "002B"); // ������ � ���. �����' uid='002B'>
                    AddRt(itNode, iRt->second.ICCardEdit, "0039"); // ��������������' uid='0039'>
                    AddRt(itNode, iRt->second.ICPlan, "002C"); // ������ � ������ ������' uid='002C'>
                    AddRt(itNode, iRt->second.ICPlanPlaning, "003A"); // ���������� ������������' uid='003A'>
                    AddRt(itNode, iRt->second.Doc, "003C"); // ������ � ��-���������������' uid='003C'/>
                    AddRt(itNode, iRt->second.DocSpec, "002E"); // ������ � ������ �������� ����������' uid='002E'>
                    AddRt(itNode, iRt->second.DocSpecEdit, "003D"); // ��������������' uid='003D'>
                    AddRt(itNode, iRt->second.DocDoc, "002D"); // ������ � ������ ���������' uid='002D'>
                    AddRt(itNode, iRt->second.DocDocEdit, "003E"); // ��������������' uid='003E'>
                    AddRt(itNode, iRt->second.DocFilter, "002F"); // ������ � ������ �������' uid='002F'>
                    AddRt(itNode, iRt->second.DocFilterEdit, "003F"); // ��������������' uid='003F'>
                    AddRt(itNode, iRt->second.VS, "0029"); // ����� ����' uid='0029'/>
                   }
                 }
               }
             }
           }
         }
        else
         {
          for (TIntRightMap::iterator iRt = FRightMap.begin(); iRt != FRightMap.end(); iRt++)
           {
            if (iRt->second.UserCode == tmpUser.Code)
             { // ������ ������������
              iDB = FDBMap.find(iRt->second.DBCode);
              if (iDB != FDBMap.end())
               { // �������� �� ����������
                iLPU = FLPUMap.find(iDB->second.LPUCode);
                if (iLPU != FLPUMap.end())
                 { // �������� ��� �������
                  itNode             = RT->AddChild("db");
                  itNode->AV["id"]   = iLPU->second.Code;
                  itNode->AV["name"] = iLPU->second.Name;
                 }
               }
             }
           }
         }
        RC = RT->AsXML;
       }
     }
    else
     {
      // dsLogMessage("_22", __FUNC__, 1);
      TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
      if (FSes)
       {
        if ((FSes->UserRoles->Values["UPS"] == FAdmUserPass) && (FSes->UserName == FAdmUser))
         RC = RT->AsXML;
       }
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
    delete RT;
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsAdminDM::ConnectToDB(UnicodeString ADBName)
 {
  bool RC = false;
  try
   {
    ConnectDB(DBCheckConnection, ADBName, __FUNC__);
    RC = true;
   }
  catch (Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsAdminDM::UpdateLPUList(UnicodeString ACode)
 {
  // dsLogBegin(__FUNC__);
  TFDQuery * FQ = CreateTempQuery();
  TFDQuery * FExtQ = CreateDBCheckQuery();
  try
   {
    quExecParam(FQ, false, "Select * from class_0008 where code=:pCode", "pCode", ACode);
    dsLogMessage("���: " + IntToStr(FQ->RecordCount), __FUNC__);
    if (FQ->RecordCount)
     {
      UnicodeString LPUCode, ICDB, DocDB;
      LPUCode = FQ->FieldByName("R0120")->AsInteger;
      // <choiceDB name='���' uid='0120' comment='' ref='00C7' required='1' inlist='l' depend='' isedit='1' is3D='1' fl='' minleft='' default='' linecount=''/>
      ICDB = FQ->FieldByName("R0011")->AsString;
      // <extedit name='���� � �� ��-���' uid='0011' comment='' ref='' required='0' inlist='l' depend='' isedit='1' is3D='1' minleft='50' linecount='2' showtype='0' btncount='1' fl='' fieldtype='varchar' length='255' default=''/>
      DocDB = FQ->FieldByName("R001D")->AsString;
      // <extedit name='���� � �� ��-���-���������������' uid='001D' comment='' ref='' required='0' inlist='l' depend='' isedit='1' is3D='1' minleft='50' linecount='2' showtype='0' btncount='1' fl='' fieldtype='varchar' length='255' default=''/>
      if (ConnectToDB(ICDB) && ConnectToDB(DocDB))
       {
        dsLogMessage("��: " + ICDB + ", ���: " + DocDB, __FUNC__);
        // �� ��
        ConnectToDB(ICDB);
        try
         {
          dsLogMessage("ConnectToDB ��: " + ICDB, __FUNC__);
          for (int i = 0; i < 11; i++)
           quExec(FExtQ, false, "ALTER TRIGGER CLASS_00C7_BD" + IntToStr(i) + " INACTIVE");
          quExec(FExtQ, false, "Delete from CLASS_00C7");
          quExec(FQ, false, "Select * from class_00C7");
          while (!FQ->Eof)
           {
            quExecParam(FExtQ, false,
              "Update or Insert into class_00C7 (code, R011E, R00CA, R00CB, R00CC) values (:pcode, :pR011E, :pR00CA, :pR00CB, :pR00CC) MATCHING (CODE)",
              "pcode", FQ->FieldByName("code")->AsString, "", "pR011E", FQ->FieldByName("R011E")->AsString, "pR00CA",
              FQ->FieldByName("R00CA")->AsString, "pR00CB", FQ->FieldByName("R00CB")->AsString, "pR00CC",
              FQ->FieldByName("R00CC")->AsString);
            quExecParam(FExtQ, false,
              "Update or Insert into class_00C7 (code, R00CD, R00CE, R00CF) values (:pcode, :pR00CD, :pR00CE, :pR00CF) MATCHING (CODE)",
              "pcode", FQ->FieldByName("code")->AsString, "", "pR00CD", FQ->FieldByName("R00CD")->AsString, "pR00CE",
              FQ->FieldByName("R00CE")->AsString, "pR00CF", FQ->FieldByName("R00CF")->AsString);
            FQ->Next();
           }
         }
        __finally
         {
          for (int i = 0; i < 11; i++)
           quExec(FExtQ, false, "ALTER TRIGGER CLASS_00C7_BD" + IntToStr(i) + " ACTIVE");
          if (FExtQ->Transaction->Active)
           FExtQ->Transaction->Commit();
          DisconnectDB(DBCheckConnection, __FUNC__);
         }
        // �� ���������������
        ConnectToDB(DocDB);
        dsLogMessage("ConnectToDB ���: " + DocDB, __FUNC__);
        try
         {
          quExecParam(FQ, false, "Select * from class_00C7 where code=:pCode", "pCode", LPUCode);
          quExec(FExtQ, false, "Delete from SUBORDORG where SUBORD=2");
          while (!FQ->Eof)
           {
            quExecParam(FExtQ, false,
              "UPDATE OR INSERT INTO SUBORDORG (CODE, NAME, ADDR, SUBORD, EMAIL, PHONE) VALUES (:pCODE, :pNAME, :pADDR, 2, :pEMAIL, :pPHONE) MATCHING (CODE)",
              "pCODE", FQ->FieldByName("R011E")->AsString, "", "pNAME", FQ->FieldByName("R00CA")->AsString, "pADDR",
              FQ->FieldByName("R00CC")->AsString, "pEMAIL", FQ->FieldByName("R00CF")->AsString, "pPHONE",
              FQ->FieldByName("R00CE")->AsString);
            quExecParam(FExtQ, false,
              "UPDATE OR INSERT INTO SUBORDORG (CODE, FULLNAME, MODULE_TYPE, OWNERFIO) VALUES (:pCODE, :pFULLNAME, :pMODULE_TYPE, :pOWNERFIO) MATCHING (CODE)",
              "pCODE", FQ->FieldByName("R011E")->AsString, "", "pFULLNAME", FQ->FieldByName("R00CB")->AsString,
              "pMODULE_TYPE", "IC", "pOWNERFIO", FQ->FieldByName("R00CD")->AsString);
            FQ->Next();
           }
          quExecParam(FQ, false, "Select * from class_00C7 where code<>:pCode", "pCode", LPUCode);
          quExec(FExtQ, false, "Delete from SUBORDORG where SUBORD=0");
          while (!FQ->Eof)
           {
            quExecParam(FExtQ, false,
              "UPDATE OR INSERT INTO SUBORDORG (CODE, NAME, ADDR, SUBORD, EMAIL, PHONE) VALUES (:pCODE, :pNAME, :pADDR, 0, :pEMAIL, :pPHONE) MATCHING (CODE)",
              "pCODE", FQ->FieldByName("R011E")->AsString, "", "pNAME", FQ->FieldByName("R00CA")->AsString, "pADDR",
              FQ->FieldByName("R00CC")->AsString, "pEMAIL", FQ->FieldByName("R00CF")->AsString, "pPHONE",
              FQ->FieldByName("R00CE")->AsString);
            quExecParam(FExtQ, false,
              "UPDATE OR INSERT INTO SUBORDORG (CODE, FULLNAME, MODULE_TYPE, OWNERFIO) VALUES (:pCODE, :pFULLNAME, :pMODULE_TYPE, :pOWNERFIO) MATCHING (CODE)",
              "pCODE", FQ->FieldByName("R011E")->AsString, "", "pFULLNAME", FQ->FieldByName("R00CB")->AsString,
              "pMODULE_TYPE", "IC", "pOWNERFIO", FQ->FieldByName("R00CD")->AsString);
            FQ->Next();
           }
         }
        __finally
         {
          if (FExtQ->Transaction->Active)
           FExtQ->Transaction->Commit();
          DisconnectDB(DBCheckConnection, __FUNC__);
         }
       }
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
    DeleteTempQuery(FExtQ);
   }
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsAdminDM::CheckUpdate()
 {
  /*
   //  dsLogBegin(__FUNC__);
   TFDQuery * FQ = CreateTempQuery();
   TFDQuery * FExtQ = CreateDBCheckQuery();
   try
   {
   quExecParam(FQ, false, "Select * from class_0008 where code=:pCode", "pCode", ACode);
   dsLogMessage("���: " + IntToStr(FQ->RecordCount), __FUNC__);
   if (FQ->RecordCount)
   {
   UnicodeString LPUCode, ICDB, DocDB;
   LPUCode = FQ->FieldByName("R0120")->AsInteger;
   //<choiceDB name='���' uid='0120' comment='' ref='00C7' required='1' inlist='l' depend='' isedit='1' is3D='1' fl='' minleft='' default='' linecount=''/>
   ICDB = FQ->FieldByName("R0011")->AsString;
   //<extedit name='���� � �� ��-���' uid='0011' comment='' ref='' required='0' inlist='l' depend='' isedit='1' is3D='1' minleft='50' linecount='2' showtype='0' btncount='1' fl='' fieldtype='varchar' length='255' default=''/>
   DocDB = FQ->FieldByName("R001D")->AsString;
   //<extedit name='���� � �� ��-���-���������������' uid='001D' comment='' ref='' required='0' inlist='l' depend='' isedit='1' is3D='1' minleft='50' linecount='2' showtype='0' btncount='1' fl='' fieldtype='varchar' length='255' default=''/>
   if (ConnectToDB(ICDB) && ConnectToDB(DocDB))
   {
   dsLogMessage("��: " + ICDB + ", ���: " + DocDB, __FUNC__);
   //�� ��
   ConnectToDB(ICDB);
   try
   {
   dsLogMessage("ConnectToDB ��: " + ICDB, __FUNC__);
   for (int i = 0; i < 11; i++)
   quExec(FExtQ, false, "ALTER TRIGGER CLASS_00C7_BD" + IntToStr(i) + " INACTIVE");
   quExec(FExtQ, false, "Delete from CLASS_00C7");
   quExec(FQ, false, "Select * from class_00C7");
   while (!FQ->Eof)
   {
   quExecParam(FExtQ, false,
   "Update or Insert into class_00C7 (code, R011E, R00CA, R00CB, R00CC) values (:pcode, :pR011E, :pR00CA, :pR00CB, :pR00CC) MATCHING (CODE)",
   "pcode", FQ->FieldByName("code")->AsString, "", "pR011E", FQ->FieldByName("R011E")->AsString, "pR00CA",
   FQ->FieldByName("R00CA")->AsString, "pR00CB", FQ->FieldByName("R00CB")->AsString, "pR00CC",
   FQ->FieldByName("R00CC")->AsString);
   quExecParam(FExtQ, false,
   "Update or Insert into class_00C7 (code, R00CD, R00CE, R00CF) values (:pcode, :pR00CD, :pR00CE, :pR00CF) MATCHING (CODE)",
   "pcode", FQ->FieldByName("code")->AsString, "", "pR00CD", FQ->FieldByName("R00CD")->AsString, "pR00CE",
   FQ->FieldByName("R00CE")->AsString, "pR00CF", FQ->FieldByName("R00CF")->AsString);
   FQ->Next();
   }
   }
   __finally
   {
   for (int i = 0; i < 11; i++)
   quExec(FExtQ, false, "ALTER TRIGGER CLASS_00C7_BD" + IntToStr(i) + " ACTIVE");
   if (FExtQ->Transaction->Active)
   FExtQ->Transaction->Commit();

   DisconnectDB(DBCheckConnection, __FUNC__);
   }
   //�� ���������������
   ConnectToDB(DocDB);
   dsLogMessage("ConnectToDB ���: " + DocDB, __FUNC__);
   try
   {
   quExecParam(FQ, false, "Select * from class_00C7 where code=:pCode", "pCode", LPUCode);
   quExec(FExtQ, false, "Delete from SUBORDORG where SUBORD=2");
   while (!FQ->Eof)
   {
   quExecParam(FExtQ, false,
   "UPDATE OR INSERT INTO SUBORDORG (CODE, NAME, ADDR, SUBORD, EMAIL, PHONE) VALUES (:pCODE, :pNAME, :pADDR, 2, :pEMAIL, :pPHONE) MATCHING (CODE)",
   "pCODE", FQ->FieldByName("R011E")->AsString, "", "pNAME", FQ->FieldByName("R00CA")->AsString, "pADDR",
   FQ->FieldByName("R00CC")->AsString, "pEMAIL", FQ->FieldByName("R00CF")->AsString, "pPHONE",
   FQ->FieldByName("R00CE")->AsString);
   quExecParam(FExtQ, false,
   "UPDATE OR INSERT INTO SUBORDORG (CODE, FULLNAME, MODULE_TYPE, OWNERFIO) VALUES (:pCODE, :pFULLNAME, :pMODULE_TYPE, :pOWNERFIO) MATCHING (CODE)",
   "pCODE", FQ->FieldByName("R011E")->AsString, "", "pFULLNAME", FQ->FieldByName("R00CB")->AsString,
   "pMODULE_TYPE", "IC", "pOWNERFIO", FQ->FieldByName("R00CD")->AsString);
   FQ->Next();
   }
   }
   __finally
   {
   if (FExtQ->Transaction->Active)
   FExtQ->Transaction->Commit();
   DisconnectDB(DBCheckConnection, __FUNC__);
   }
   }
   }
   }
   __finally
   {
   DeleteTempQuery(FQ);
   DeleteTempQuery(FExtQ);
   }
   //  dsLogEnd(__FUNC__);
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsAdminDM::DBList(TStrings * AList)
 {
  TFDQuery * FQ = CreateTempQuery();
  TFDQuery * FQl = CreateTempQuery();
  TBase64Encoding * B64Encode = new TBase64Encoding;
  AList->Clear();
  try
   {
    UnicodeString FLPU;
    quExec(FQ, false, "Select * from class_0008", ">>dblist");
    while (!FQ->Eof)
     {
      quExecParam(FQl, false, "Select * from class_00C7 where code=:pCode", "pCode",
        FQ->FieldByName("R0120")->AsInteger, "lpu");
      FLPU = FQl->FieldByName("R00CA")->AsString + "@@";
      FLPU += FQl->FieldByName("R00CB")->AsString + "@@";
      FLPU += FQl->FieldByName("R00CC")->AsString + "@@";
      FLPU += FQl->FieldByName("R00CD")->AsString + "@@";
      FLPU += FQl->FieldByName("R00CE")->AsString + "@@";
      FLPU += FQl->FieldByName("R00CF")->AsString;
      FLPU = B64Encode->Encode(FLPU);
      AList->Add(FQ->FieldByName("R0011")->AsString + "@@" + FLPU);
      FQ->Next();
     }
   }
  __finally
   {
    delete B64Encode;
    DeleteTempQuery(FQ);
    DeleteTempQuery(FQl);
   }
 }
// ---------------------------------------------------------------------------
