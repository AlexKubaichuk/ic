//---------------------------------------------------------------------------

#ifndef dsMKBH
#define dsMKBH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "XMLContainer.h"
#include <System.JSON.hpp>
#include "ExtUtils.h"
#include "icsLog.h"
//---------------------------------------------------------------------------
class TdsMKB : public TObject
{
private:	// User declarations
  TAnsiStrMap *FMKBData;
  TAnsiStrMap *FMKBLoData;
  TAnsiStrMap *FMKBExtData;

  bool __fastcall FLoadMKB(TTagNode* ANode);

public:		// User declarations
  __fastcall TdsMKB();
  __fastcall ~TdsMKB();

  TJSONObject* __fastcall Find(UnicodeString AStr);
  UnicodeString __fastcall CodeToText(UnicodeString ACode);
};
//---------------------------------------------------------------------------
#endif
