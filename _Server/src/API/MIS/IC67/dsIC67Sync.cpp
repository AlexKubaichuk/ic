// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsIC67Sync.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                              TIC67Sync                                 ##
// ##                                                                       ##
// ###########################################################################
__fastcall TIC67Sync::TIC67Sync(TFDConnection * AConnection)
 {
  FIC67Connection = AConnection;
  InfMap["1"]     = "���.";
  InfMap["2"]     = "���.";
  InfMap["3"]     = "���.";
  InfMap["4"]     = "����.";
  InfMap["5"]     = "�����.";
  InfMap["6"]     = "����";
  InfMap["7"]     = "�����.";
  InfMap["8"]     = "�����.";
  InfMap["9"]     = "�����";
  InfMap["10"]    = "����.";
  InfMap["11"]    = "��-���.";
  InfMap["12"]    = "������.";
  InfMap["13"]    = "�����.";
  InfMap["14"]    = "���.�";
  InfMap["15"]    = "���.";
  InfMap["16"]    = "�����.";
  InfMap["17"]    = "�����.";
  InfMap["18"]    = "�����.";
  InfMap["19"]    = "����.";
  InfMap["20"]    = "���.��.";
  InfMap["21"]    = "����.";
  InfMap["22"]    = "��.���";
  InfMap["23"]    = "����";
  InfMap["24"]    = "������.";
  InfMap["25"]    = "�.����";
  InfMap["26"]    = "���.�";
  InfMap["30"]    = "���.";
  InfMap["31"]    = "���.�����";
  InfMap["32"]    = "�����.";
  InfMap["34"]    = "����.���.";
  InfMap["35"]    = "���";
 }
// ---------------------------------------------------------------------------
__fastcall TIC67Sync::~TIC67Sync()
 {
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TIC67Sync::Convert(TJSONObject * ASrc)
 {
  return ASrc;
 }
// ----------------------------------------------------------------------------
TJSONObject * __fastcall TIC67Sync::NewObjPair2(TFDQuery * AQ, UnicodeString AFlName)
 {
  if (AQ->FieldByName(AFlName)->IsNull)
   return (TJSONObject *)(new TJSONNull);
  else
   return NewObjPair(AsStr(AQ, AFlName), AsStr(AQ, AFlName));
 }
// ----------------------------------------------------------------------------
void __fastcall TIC67Sync::SetClData(TJSONObject * ACls, UnicodeString AId, UnicodeString APref, UnicodeString ACode,
  UnicodeString ALPU, UnicodeString AOtdel)
 {
  LogBegin(__FUNC__);
  if (AId.Trim().Length() && ACode.Trim().Length() && ACode.ToIntDef(0))
   {
    TFDQuery * FQ = CreateTempQuery();
    try
     {
      try
       {
        quExec(FQ, false, "Select * From class_" + APref + AId + " where code=" + ACode);
        if (FQ->RecordCount)
         { // ����� ������ � �����������
          TJSONObject * FCls = (TJSONObject *)GetJSONValue(ACls, AId);
          if (!FCls) // ���� ��� ����������� AId, �� ������ ���
           {
            FCls = new TJSONObject;
            ACls->AddPair(AId.UpperCase(), FCls);
           }
          if (FCls)
           { // ������ ���, ���������� ����������
            if (!GetJSONValue((TJSONObject *)FCls, ACode))
             { // ������ ����������� ��� �� ���������
              UnicodeString FClId = AId.UpperCase();
              TJSONObject * FClRec = new TJSONObject;
              TFDQuery * FQ02 = CreateTempQuery();
              try
               {
                FClRec->AddPair("code", NewObjPair2(FQ, "code"));
                // ---------------------------------------------------------------------------
                // <class name='�����������' uid='0001' ref='no' required='yes' inlist='list'>
                // ---------------------------------------------------------------------------
                if (FClId == "0001")
                 {
                  // FClRec->AddPair("32CC", NewObjPair(icsNewGUID(), "guid"));
                  if (FQ->FieldByName("r00AF")->AsInteger)
                   { // ���������� ������� "�������������"
                    FClRec->AddPair("3196", NewObjPair(ALPU, ALPU));
                    // <choiceDB name='���' uid='3196' ref='00C7' default='Main'/>
                   }
                  // <text name='������������' uid='0008' ref='no' required='yes' inlist='list' depend='no'  length='20'/>
                  UnicodeString FOrgName = PrepareStr(AsStr(FQ, "r0008"));
                  // <text name='������������' uid='0003'
                  if (FQ->FieldByName("r0002")->AsInteger)
                   {
                    quExec(FQ02, false, "Select * From class_" + APref + "0002 where code=" + AsStr(FQ, "r0002"));
                    // <class name='���� �����������' uid='0002'
                    if (FQ02->RecordCount)
                     FOrgName = PrepareStr(AsStr(FQ02, "r0003")) + " " + FOrgName;
                    // <���� ����������� text name='������������' uid='0003'
                   }
                  FClRec->AddPair("0008", FOrgName.Trim());
                  FClRec->AddPair("00A3", NewObjPair2(FQ, "R00A3"));
                  // <choiceTree name='��� ������������ �� �����' uid='00A3'
                  FClRec->AddPair("00AF", NewObjPair2(FQ, "R00AF")); // <binary name='�������������' uid='00AF'
                  FClRec->AddPair("00B1", NewObjPair2(FQ, "R00B1")); // <binary name='��������������' uid='00B1'
                  FClRec->AddPair("0009", PrepareStr(AsStr(FQ, "r0009")));
                  // <text name='�������' uid='0009'
                  FClRec->AddPair("0107", new TJSONNull); // -        <text name='�����' uid='0107'    length='255'/>
                  FClRec->AddPair("011A", new TJSONNull); // -        <choiceDB name='������' uid='011A' ref='0117' />
                  FClRec->AddPair("010A", new TJSONNull);
                  // -        <choiceDB name='�������������' uid='010A' ref='0109' />
                 }
                // ---------------------------------------------------------------------------
                // <class name='���������' uid='000C' ref='no' required='yes' inlist='list'>
                // ---------------------------------------------------------------------------
                else if (FClId == "000C")
                 {
                  // FClRec->AddPair("32C5", NewObjPair(icsNewGUID(), "guid"));
                  FClRec->AddPair("00DB", NewObjPair(ALPU, ALPU)); // <choiceDB name='���' uid='00DB'
                  FClRec->AddPair("000D", PrepareStr(AsStr(FQ, "r000D"))); // <text name='������������' uid='000D'
                 }
                // ---------------------------------------------------------------------------
                // <class name='�������' uid='000E' ref='no' required='yes' inlist='list'>
                // ---------------------------------------------------------------------------
                else if (FClId == "000E")
                 {
                  // FClRec->AddPair("32C8", NewObjPair(icsNewGUID(), "guid"));
                  FClRec->AddPair("00F6", NewObjPair(ALPU, ALPU)); // <choiceDB name='���' uid='00F6'
                  if (AOtdel.ToIntDef(0))
                   FClRec->AddPair("00F7", NewObjPair(AOtdel, AOtdel)); // <choiceDB name='���������' uid='00F7'
                  FClRec->AddPair("000F", PrepareStr(AsStr(FQ, "r000F"))); // <text name='������������' uid='000F'
                  FClRec->AddPair("00F8", new TJSONNull); // -        <choiceDB name='���������� ����' uid='00F8'
                 }
                // ---------------------------------------------------------------------------
                // <class name='��������� ��������' uid='0095'
                // ---------------------------------------------------------------------------
                else if (FClId == "0095")
                 {
                  // FClRec->AddPair("32CE", NewObjPair(icsNewGUID(), "guid"));
                  FClRec->AddPair("0098", PrepareStr(AsStr(FQ, "r0098"))); // <text name='������������' uid='0098'
                  FClRec->AddPair("0096", PrepareStr(AsStr(FQ, "r0096"))); // <text name='�������' uid='0096'
                  FClRec->AddPair("0097", PrepareStr(AsStr(FQ, "r0097"))); // <text name='�����' uid='0097'
                 }
                // ---------------------------------------------------------------------------
                // <class name='������ ������' uid='00AA'
                // ---------------------------------------------------------------------------
                else if (FClId == "00AA")
                 {
                  // FClRec->AddPair("32C7", NewObjPair(icsNewGUID(), "guid"));
                  FClRec->AddPair("0120", NewObjPair(ALPU, ALPU)); // <choiceDB name='���' uid='0120'
                  FClRec->AddPair("00AD", PrepareStr(AsStr(FQ, "r00AD"))); // <text name='�������' uid='00AD'
                  FClRec->AddPair("00AC", PrepareStr(AsStr(FQ, "r00AC"))); // <text name='���' uid='00AC'
                  FClRec->AddPair("00AE", PrepareStr(AsStr(FQ, "r00AE"))); // <text name='��������' uid='00AE'
                  FClRec->AddPair("00DF", new TJSONNull); // -        <choiceDB name='���������' uid='00DF'
                  FClRec->AddPair("00E0", new TJSONNull); // -        <text name='�������' uid='00E0'
                  FClRec->AddPair("00E1", new TJSONNull); // -        <text name='��. �����' uid='00E1'
                 }
                // ---------------------------------------------------------------------------
                // <class name='������ ���������� ��������������' uid='00AB' inlist='list'>
                // ---------------------------------------------------------------------------
                else if (FClId == "00AB")
                 {
                  // FClRec->AddPair("32CF", NewObjPair(icsNewGUID(), "guid"));
                  FClRec->AddPair("00B0", PrepareStr(AsStr(FQ, "r00B0"))); // <text name='������������' uid='00B0'
                 }
                // ---------------------------------------------------------------------------
                // <class name='���. ���������� 1' uid='00BD' inlist='list' isedit='no'>
                // ---------------------------------------------------------------------------
                else if (FClId == "00BD")
                 {
                  // FClRec->AddPair("32D0", NewObjPair(icsNewGUID(), "guid"));
                  FClRec->AddPair("00C0", PrepareStr(AsStr(FQ, "r00C0"))); // <text name='������������' uid='00C0'
                 }
                // ---------------------------------------------------------------------------
                // <class name='���. ���������� 2' uid='00BE' inlist='list' isedit='no'>
                // ---------------------------------------------------------------------------
                else if (FClId == "00BE")
                 {
                  // FClRec->AddPair("32D1", NewObjPair(icsNewGUID(), "guid"));
                  FClRec->AddPair("00C2", PrepareStr(AsStr(FQ, "r00C2"))); // <text name='������������' uid='00C2'
                 }
                FCls->AddPair(ACode.UpperCase(), TJSONObject::ParseJSONValue(FClRec->ToString()));
               }
              __finally
               {
                DeleteTempQuery(FQ02);
                delete FClRec;
               }
             }
           }
         }
       }
      catch (System::Sysutils::Exception & E)
       {
        dsLogError(E.Message, __FUNC__);
       }
     }
    __finally
     {
      delete FQ;
     }
   }
  LogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
bool __fastcall TIC67Sync::IsFullSync()
 {
  LogBegin(__FUNC__);
  bool RC = false;
  try
   {
    TFDQuery * FQ = CreateTempQuery();
    try
     {
      quExec(FQ, false, "Select Count(*) as RCOUNT From casebook1 ");
      if (!FQ->FieldByName("RCOUNT")->AsInteger)
       {
        quExec(FQ, false, "Select Count(*) as RCOUNT From acasebook1 ");
        RC = !FQ->FieldByName("RCOUNT")->AsInteger;
       }
     }
    __finally
     {
      if (FQ->Transaction->Active)
       FQ->Transaction->Commit();
      DeleteTempQuery(FQ);
     }
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONArray * __fastcall TIC67Sync::GetPatIdList()
 {
  LogBegin(__FUNC__);
  TJSONArray * RC = new TJSONArray;
  try
   {
    TFDQuery * FQ = CreateTempQuery();
    try
     {
      quExec(FQ, false, "Select * From acasebook1 ");
      while (!FQ->Eof)
       {
        RC->Add("a" + AsStr(FQ, "code"));
        FQ->Next();
       }
      quExec(FQ, false, "Select * From casebook1 ");
      while (!FQ->Eof)
       {
        RC->Add(AsStr(FQ, "code"));
        FQ->Next();
       }
     }
    __finally
     {
      if (FQ->Transaction->Active)
       FQ->Transaction->Commit();
      DeleteTempQuery(FQ);
     }
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
// TJSONObject * __fastcall TIC67Sync::GetPatDataById(UnicodeString AId)
UnicodeString __fastcall TIC67Sync::GetPatDataById(UnicodeString AId)
 {
  LogBegin(__FUNC__);
  LogMessage("RecId: " + AId, __FUNC__);
  UnicodeString RC = "";
  TJSONObject * ObjRC = new TJSONObject;
  TJSONObject * ClsRC = new TJSONObject;
  try
   {
    try
     {
      UnicodeString FPref = "";
      UnicodeString FId = AId;
      if (AId[1] == 'a')
       {
        FPref = "a";
        FId   = FId.Delete(1, 1);
       }
      TDate OtvDate = TDate(0);
      TDate FBirthday = TDate(0);
      ObjRC->AddPair("pt", FGetPatRegDataById(FId, FPref, FBirthday, OtvDate, ClsRC));
      if (CheckCard(FId, FPref, OtvDate))
       { // ������������ ��������, �����, ������
        TJSONObject * cdData = new TJSONObject;
        ObjRC->AddPair("cd", cdData);
        Convert1003(cdData, FId, FPref, FBirthday, ClsRC);
        Convert102f(cdData, FId, FPref, FBirthday, ClsRC);
        Convert1030(cdData, FId, FPref, FBirthday, OtvDate, ClsRC);
       }
      ObjRC->AddPair("cl", ClsRC);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    RC = ObjRC->ToString();
    delete ObjRC;
   }
  LogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TIC67Sync::FGetPatRegDataById(UnicodeString ARecId, UnicodeString APref, TDate & ABirthday,
  TDate & AOtvToDate, TJSONObject * ACls)
 {
  LogBegin(__FUNC__);
  dsLogMessage("RecId: " + ARecId, __FUNC__);
  TJSONObject * RC = new TJSONObject;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      quExec(FQ, false, "Select * From " + APref + "casebook1 where code=" + ARecId);
      if (FQ->RecordCount)
       {
        ABirthday  = 0;
        AOtvToDate = 0;
        UnicodeString FVal, FVal2;
        int SupportType = 6; // <choice name='������ ������������' uid='32B3'   >
        // ������������� �� ������� - 1
        // ������������� �� ����������� - 2
        // ������������� �� ������ - 3
        // �������������� - 4
        // ����� - 5
        // �� ������������� - 6
        ABirthday = FQ->FieldByName("R0031")->AsDateTime;
        UnicodeString FLPUCode = FLPUId;
        RC->AddPair("CODE", NewObjPair2(FQ, "code"));
        // RC->AddPair("32D2", NewObjPair(icsNewGUID(), "guid"));
        RC->AddPair("001F", PrepareStr(AsStr(FQ, "r001F"))); // -> <text name='�������' uid='001F' length='30'/>
        RC->AddPair("0020", PrepareStr(AsStr(FQ, "r0020"))); // -> <text name='���' uid='0020' length='20'/>
        RC->AddPair("002F", PrepareStr(AsStr(FQ, "r002F"))); // -> <text name='��������' uid='002F' length='30'/>
        // RC->AddPair("", FQ->FieldByName("r")->AsString); //-> <text name='� �����' uid='0122'    minleft='90' length='30'/>
        RC->AddPair("0030", AsDTF(FQ, "r0030")); // -> <date name='���� �����������' uid='0030'
        RC->AddPair("0031", AsDTF(FQ, "r0031")); // -> <date name='���� ��������' uid='0031' />
        // -> <extedit name='�������' uid='00B2'   depend='0031' minleft='90' length='10'/><extedit name='�������' uid='00B2' depend='0031'  />
        RC->AddPair("0021", NewObjPair2(FQ, "R0021")); // -> <choice name='���' uid='0021'  default='2'>
        // <extedit name='��������� �����' uid='0032' depend='0136.0136' length='11'/>
        // <extedit name='�����' uid='00FF' depend='0136.0136' length='4'/>
        // <extedit name='���' uid='0100' depend='0136.0136' length='50'/>
        // PrepareValue(FUnitData, "0033"); //-> <extedit name='��������' len='15'/> -> <text len='5'>
        // -> <extedit name='�����' uid='0136'      length='40'/><choiceBD name='�����' uid='0032' ref='0017'   >
        // <choiceDB name='���' uid='00F1' ref='00C7'   default='Main' />
        RC->AddPair("00F1", NewObjPair(FLPUCode, FLPUCode));
        // ������������ ������
        UnicodeString AddrStr = "";
        UnicodeString ChAddrStr = "";
        UnicodeString ChHouse = "";
        UnicodeString ChBuild = "";
        UnicodeString ChFlat = "";
        if (FQ->FieldByName("R0022")->AsInteger && FQ->FieldByName("R0032")->AsInteger)
         { // ���� ����� � �� ������������
          TFDQuery * FQ17 = CreateTempQuery();
          TFDQuery * FQ18 = CreateTempQuery();
          TFDQuery * FQ19 = CreateTempQuery();
          TFDQuery * FQ10 = CreateTempQuery();
          try
           {
            quExec(FQ17, false, "Select * From class_" + APref + "0017 where code=" + AsStr(FQ, "R0032"));
            if (FQ17->RecordCount)
             { // ����� ������ � �����������
              // if (APref.UpperCase() == "A")
              if (FQ17->FieldByName("R001E")->AsInteger && (APref.UpperCase() != "A")) // ��� �������
               {
                RC->AddPair("00B3", NewObjPair2(FQ17, "R001E")); // choiceDB name='�������' uid='00B3'
                SupportType = 1; // ������������� �� ������� - 1
                quExec(FQ10, false, "Select * From class_" + APref + "0010 where r0012=" + AsStr(FQ17, "R001E"));
                FVal2 = "";
                if (FQ10->RecordCount)
                 {
                  if (FQ10->FieldByName("R0011")->AsInteger)
                   { // ���������� ���������
                    RC->AddPair("00F9", NewObjPair2(FQ10, "R0011")); // <choiceDB name='���������' uid='00F9' ref='000C'
                    SetClData(ACls, "000C", APref, AsStr(FQ10, "R0011"), FLPUCode, ""); // ���������
                    FVal2 = AsStr(FQ10, "R0011");
                   }
                 }
                SetClData(ACls, "000E", APref, AsStr(FQ17, "R001E"), FLPUCode, FVal2); // �������
               }
              ChHouse = AsStr(FQ17, "R001C"); // <text name='� ����' uid='001C' ;
              ChBuild = AsStr(FQ17, "R001D"); // <text name='������' uid='001D';
              AddrStr = ChHouse;
              FVal    = ChBuild;
              // FVal2 = FQ17->FieldByName("R0018")->AsString; //<class name='�����' uid='0018' ref='no' required='yes' inlist='list'>
              if (FVal.Length())
               {
                if (AddrStr.Length())
                 AddrStr += ", ����. " + FVal;
                else
                 AddrStr = FVal;
               }
              ChFlat = AsStr(FQ, "R0033").Trim();
              if (ChFlat.Length())
               AddrStr += ", ��. " + ChFlat;
              quExec(FQ18, false, "Select * From class_" + APref + "0018 where code=" + AsStr(FQ17, "R0018"));
              if (FQ18->RecordCount)
               { // ����� ������� � �����������
                FVal = AsStr(FQ18, "R001B"); // <text name='������������' uid='001B'
                if (FVal.Length())
                 {
                  if (AddrStr.Length())
                   AddrStr = FVal + ", " + AddrStr;
                  else
                   AddrStr = FVal;
                  ChAddrStr = FVal;
                 }
                quExec(FQ19, false, "Select * From class_" + APref + "0019 where code=" + AsStr(FQ18, "R0019"));
                if (FQ19->RecordCount)
                 { // ���������� ����� ������ � �����������
                  FVal = AsStr(FQ19, "R001A"); // <<text name='������������' uid='001A'
                  if (FVal.Length())
                   {
                    if (AddrStr.Length())
                     {
                      AddrStr   = FVal + ", " + AddrStr;
                      ChAddrStr = FVal + ", " + ChAddrStr;
                     }
                    else
                     {
                      AddrStr   = FVal;
                      ChAddrStr = FVal;
                     }
                   }
                 }
               }
             }
           }
          __finally
           {
            DeleteTempQuery(FQ17);
            DeleteTempQuery(FQ18);
            DeleteTempQuery(FQ19);
            DeleteTempQuery(FQ10);
           }
         }
        else
         AddrStr = AsStr(FQ, "R0024").Trim(); // <text name='����������������� �����' uid='0024'
        if (AddrStr.Length())
         {
          UnicodeString FCompAddrCode = "";
          if (FCheckAddr)
           {
            FCompAddrCode = FCheckAddr(ChAddrStr, ChHouse, ChBuild, ChFlat);
           }
          if (FCompAddrCode.Length())
           RC->AddPair("0136", NewObjPair(FCompAddrCode, PrepareStr(AddrStr)));
          else
           RC->AddPair("0136", NewObjPair("-1", PrepareStr(AddrStr)));
         }
        RC->AddPair("0022", NewObjPair2(FQ, "R0022")); // -> <binary name='����� ������������� ������������' uid='0022'
        RC->AddPair("0148", PrepareStr(AsStr(FQ, "R0034")));
        // <text '���������' uid='0148' - '�������' uid='0034'
        // <extedit name='���������/���������' uid='0111' depend='0025.0025' length='50'/>
        // <extedit name='������ ��������' uid='0112' ref='0108' depend='0025.0025'  />
        // <extedit name='������ �����������' uid='011B' ref='0117' depend='0025.0025'    >
        bool FOrgNotExists = true;
        if (FQ->FieldByName("R0023")->AsInteger && FQ->FieldByName("R0025")->AsInteger && (APref.UpperCase() != "A"))
         {
          TFDQuery * FQ01 = CreateTempQuery();
          try
           {
            quExec(FQ01, false, "Select * From class_" + APref + "0001 where code=" + AsStr(FQ, "R0025"));
            if (FQ01->RecordCount)
             {
              if (FQ01->FieldByName("R00AF")->AsInteger)
               {
                SupportType = 2; // ������������� �� ����������� - 2
                RC->AddPair("00F0", NewObjPair("1", "1"));
                // binary name='������������� ����������� (����)' uid='00F0'/>
               }
              else if (FQ01->FieldByName("R00B1")->AsInteger)
               SupportType = 4; // �������������� - 4
              RC->AddPair("0025", NewObjPair2(FQ, "R0025"));
              // -> <extedit name='�����������' uid='0025'  1'   ><choiceBD name='�����������' uid='0025' 0'>
              if (!FQ->FieldByName("R0026")->IsNull && FQ->FieldByName("R0026")->AsString.Trim().Length())
               RC->AddPair("0026", NewObjPair2(FQ, "R0026"));
              // FQ->FieldByName("R0026")->AsString.Trim()-> <extedit name='��1' uid='0026' 002'  length='10' >
              else
               RC->AddPair("0026", new TJSONString("")); // -> <extedit name='��1' uid='0026' 002'  length='10' >
              if (!FQ->FieldByName("R0027")->IsNull && FQ->FieldByName("R0027")->AsString.Trim().Length())
               RC->AddPair("0027", NewObjPair2(FQ, "R0027"));
              // FQ->FieldByName("R0027")->AsString.Trim()-> <extedit name='��2' uid='0027' gth='10' >
              else
               RC->AddPair("0027", new TJSONString("")); // -> <extedit name='��2' uid='0027' gth='10' >
              if (!FQ->FieldByName("R0028")->IsNull && FQ->FieldByName("R0028")->AsString.Trim().Length())
               RC->AddPair("0028", NewObjPair2(FQ, "R0028"));
              // FQ->FieldByName("R0028")->AsString.Trim()-> <extedit name='��3' uid='0028'   length='10' >
              else
               RC->AddPair("0028", new TJSONString("")); // -> <extedit name='��3' uid='0028'   length='10' >
              SetClData(ACls, "0001", APref, FQ->FieldByName("R0025")->AsString, FLPUCode, ""); // �����������
              FOrgNotExists = false;
             }
           }
          __finally
           {
            DeleteTempQuery(FQ01);
           }
         }
        if (FOrgNotExists)
         {
          RC->AddPair("0025", new TJSONNull);
          // -> <extedit name='�����������' uid='0025'  1'   ><choiceBD name='�����������' uid='0025' 0'>
          RC->AddPair("0026", new TJSONNull);
          // -> <extedit name='��1' uid='0026' depend='0025.0025' length='20'/><text name='������� 1' uid='0026' ref='0005'  depend='0025.0002'  length='10' >
          RC->AddPair("0027", new TJSONNull);
          // -> <extedit name='��2' uid='0027' depend='0025.0025' length='20'/><text name='������� 2' uid='0027' ref='0006'  depend='0025.0002'  length='10' >
          RC->AddPair("0028", new TJSONNull);
          // -> <extedit name='��3' uid='0028' depend='0025.0025' length='20'/><text name='������� 3' uid='0028' ref='0007'  depend='0025.0002'  length='10' >
         }
        if (APref.UpperCase() == "A")
         SupportType = 5;
        RC->AddPair("32B3", NewObjPair(SupportType, SupportType)); // <choice name='������ ������������' uid='32B3'   >
        RC->AddPair("00A5", PrepareStr(FQ->FieldByName("R00A5")->AsString));
        // <text name='�����' uid='00A5' length='14'/>
        bool FClear = true;
        if (FQ->FieldByName("R0092")->AsInteger)
         { // ���� ��������� �����
          if (FQ->FieldByName("R0093")->AsInteger && FQ->FieldByName("R009A")->AsString.Trim().Length())
           {
            SetClData(ACls, "0095", APref, FQ->FieldByName("R0093")->AsString, 0, ""); // �����. ��������
            RC->AddPair("0093", NewObjPair2(FQ, "R0093"));
            // -> <choiceBD name='�����. ��������' uid='0093' ref='0095'   >
            if (FQ->FieldByName("R0099")->AsString.Trim().Length())
             RC->AddPair("0099", PrepareStr(FQ->FieldByName("R0099")->AsString.Trim()));
            // -> <text name='�����' uid='0099'   length='10' >
            else
             RC->AddPair("0099", new TJSONNull);
            RC->AddPair("009A", PrepareStr(FQ->FieldByName("R009A")->AsString.Trim()));
            // -> <text name='�����' uid='009A'   length='15' >
            FClear = false;
           }
         }
        if (FClear)
         {
          RC->AddPair("0093", new TJSONNull); // -> <choiceBD name='�����. ��������' uid='0093' ref='0095'   >
          RC->AddPair("0099", new TJSONNull); // -> <text name='�����' uid='0099'   length='10' >
          RC->AddPair("009A", new TJSONNull); // -> <text name='�����' uid='009A'   length='15' >
         }
        FClear = true;
        UnicodeString ULSer = PrepareStr(FQ->FieldByName("R009D")->AsString.Trim());
        UnicodeString ULNum = PrepareStr(FQ->FieldByName("R009E")->AsString.Trim());
        if (FQ->FieldByName("R009B")->AsInteger && ULSer.Length() && ULNum.Length())
         { // ���� ������������� ��������
          int ULType = FQ->FieldByName("R009C")->AsInteger; // -> <choice name='��. ��������' uid='009C'  default='-1'>
          // {0 -> 2}{1 -> 1}{2 -> 3}
          if ((ULType >= 0) && (ULType <= 2))
           {
            if (!ULType)
             RC->AddPair("009C", (new TJSONObject)->AddPair(2, 2));
            else if (ULType == 1)
             RC->AddPair("009C", (new TJSONObject)->AddPair(1, 1));
            else if (ULType == 2)
             RC->AddPair("009C", (new TJSONObject)->AddPair(3, 3));
            RC->AddPair("009D", ULSer); // -> <text name='�����' uid='009D'   length='5' >
            RC->AddPair("009E", ULNum); // -> <text name='�����' uid='009E'   length='15' >
           }
         }
        if (FClear)
         {
          RC->AddPair("009C", new TJSONNull); // -> <choice name='��. ��������' uid='009C'  default='-1'>
          RC->AddPair("009D", new TJSONNull); // -> <text name='�����' uid='009D'   length='5' >
          RC->AddPair("009E", new TJSONNull); // -> <text name='�����' uid='009E'   length='15' >
         }
        // <choice name='������ �����' uid='0138' default='-1'>
        if (FQ->FieldByName("R0085")->AsInteger)
          // -> new <text name='������������� �������' uid='0085' length='255'/> old <binary name='
           RC->AddPair("0085", "����");
        // <text name='������������� ���������������' uid='0139' length='255'/>
        if (FQ->FieldByName("R0087")->AsInteger)
          // -> new <text name='��������� ������� �� ��������' uid='0087' length='255'/> old <binary
           RC->AddPair("0087", "����");
        RC->AddPair("0084", NewObjPair2(FQ, "R0084"));
        // -> <binary name='(1) ��������� ��������� ���' uid='0084' invname='(1) ����������� ��������� ��������� ���'/>
        RC->AddPair("0086", NewObjPair2(FQ, "R0086"));
        // -> <binary name='(3) ����� ��������' uid='0086' invname='(3) �� ����� ��������'/>
        RC->AddPair("0088", NewObjPair2(FQ, "R0088"));
        // -> <binary name='(�) ���� �� ����������' uid='0088' invname='(�) ����������� ���� �� ����������'/>
        RC->AddPair("005A", NewObjPair2(FQ, "R005A")); // -> <binary name='(�) ���� ���' uid='005A'/>
        RC->AddPair("00A7", NewObjPair2(FQ, "R00A7")); // -> <binary name='(�) ���������������� ���������' uid='00A7'/>
        RC->AddPair("005C", NewObjPair2(FQ, "R005C")); // -> <binary name='(�) ������� ������������' uid='005C'/>
        // RC->AddPair("", NewObjPair2(FQ, "R")); //-> <extedit name='�����������' uid='0025'  1'   ><choiceBD name='�����������' uid='0025' 0'>
        RC->AddPair("0131", NewObjPair2(FQ, "R00A8"));
        // -> 0131 <- 00A8 // <binary name='(�) ��������� ������������' uid='00A8'/>
        // <date name='���� ����������' uid='0132'  >
        // <text name='����� �������������' uid='0133' length='30' >
        RC->AddPair("00C4", NewObjPair2(FQ, "R00C4")); // -> <binary name='������ ���������' uid='00C4'/>
        if (FQ->FieldByName("R00C4")->AsInteger)
         RC->AddPair("00C5", FQ->FieldByName("R00C5")->AsString.Trim()); // -> <date name='��' uid='00C5'  >
        else
         RC->AddPair("00C5", new TJSONNull);
        // <choiceDB name='���' uid='00F3' ref='00C7'/>
        if (FQ->FieldByName("R00BF")->AsInteger)
         {
          SetClData(ACls, "00BD", APref, FQ->FieldByName("R00BF")->AsString.Trim(), 0, "");
          // -> 00BF <- 00BF // <choiceBD name='���. ���������� 1' uid='00BF' ref='00BD'/>
          RC->AddPair("00BF", NewObjPair2(FQ, "R00BF"));
         }
        else
         RC->AddPair("00BF", new TJSONNull);
        RC->AddPair("00C1", PrepareStr(FQ->FieldByName("R00C1")->AsString.Trim()));
        // -> 00C1 <- 00C1 // <text name='����������' uid='00C1' length='150'/>
        RC->AddPair("3195", new TJSONNull); // <date name='���. ���� �����' uid='3195' depend='3195.3195'/>
        if (FQ->FieldByName("R0083")->AsInteger) // []<binary name='�������� �����' uid='0083'/> //�����
         {
          FVal = FQ->FieldByName("R00BC")->AsString; // []<date name='�������� ����� ��' uid='00BC'  >
          if (TryStrToDate(FVal, AOtvToDate))
           if (AOtvToDate <= Date())
            AOtvToDate = 0;
         }
        RC->AddPair("330E", NewObjPair("2", "�� 6.� (�������������)"));
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
  LogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
bool __fastcall TIC67Sync::CheckCard(UnicodeString AUCode, UnicodeString APref, TDate AOtvDate)
 {
  bool RC = false;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    RC = (int)AOtvDate;
    if (!RC)
     {
      quExec(FQ, false, "Select Count(*) as RCOUNT From card_" + APref + "1003 where ucode=" + AUCode);
      RC = FQ->FieldByName("RCOUNT")->AsInteger;
      if (!RC)
       {
        quExec(FQ, false, "Select Count(*) as RCOUNT From card_" + APref + "102F where ucode=" + AUCode);
        RC = FQ->FieldByName("RCOUNT")->AsInteger;
        if (!RC)
         {
          quExec(FQ, false, "Select Count(*) as RCOUNT From card_" + APref + "1030 where ucode=" + AUCode);
          RC = FQ->FieldByName("RCOUNT")->AsInteger;
         }
       }
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
bool __fastcall TIC67Sync::Convert1003(TJSONObject * ACardRoot, UnicodeString AUCode, UnicodeString APref,
  TDate ABirthday, TJSONObject * ACls)
 {
  bool RC = false;
  TJSONMap * FPrivMap = new TJSONMap;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    // TJSONMap * CardMap;
    TJSONMap::iterator itCardMap;
    TJSONObject * FCardData;
    quExec(FQ, false, "Select * From card_" + APref + "1003 where ucode=" + AUCode);
    // CardMap = GetOldUnitCard("1003", AUCode);
    if (FQ->RecordCount)
     {
      TJSONObject * privData = new TJSONObject;
      FPrivMap->clear();
      UnicodeString Priv_Code, Priv_Val;
      TJSONMap::iterator FPrivFRC;
      while (!FQ->Eof)
       {
        Priv_Code = (FQ->FieldByName("R1024")->AsString + "_" + FQ->FieldByName("R1020")->AsString).UpperCase();
        Priv_Val  = FQ->FieldByName("R1021")->AsString + ":" + FQ->FieldByName("R1075")->AsString + "^" +
          FQ->FieldByName("R1092")->AsString;
        FPrivFRC = FPrivMap->find(Priv_Code);
        if (FPrivFRC == FPrivMap->end())
         {
          FCardData = new TJSONObject;
          FCardData->AddPair("CODE", FQ->FieldByName("code")->AsString);
          // FCardData->AddPair("32D6", NewObjPair(icsNewGUID(), "guid"));
          FCardData->AddPair("317A", NewObjPair2(FQ, "UCODE"));
          // <extedit  name='��� ��������'            uid='317A' ref='1000' depend='317A.317A' showtype='0' btncount='' fieldtype='integer' length='' default=''/>
          // <extedit     name='��� ��������'            uid='3183' ref='3003' depend='3183.3183' showtype='0' btncount='' fieldtype='integer' length='' default=''/>
          // <extedit     name='��� �����'               uid='32AD' ref='3084' depend='32AD.32AD' showtype='0' btncount='' fieldtype='integer' length='' default=''/>
          // <extedit     name='�������'                 uid='30B4' comment='calc'  depend='3024' showtype='0' btncount='' fieldtype='varchar' length='9' default=''/>
          FCardData->AddPair("3024", FQ->FieldByName("R1024")->AsDateTime.FormatString("dd.mm.yyyy"));
          // <date     name='���� ����������'         uid='3024'
          if ((AsInt(FQ, "R1020") == 159) || (AsInt(FQ, "R1020") == 160))
           FCardData->AddPair("3020", NewObjPair(IntToStr(AsInt(FQ, "R1020") + 1),
            IntToStr(AsInt(FQ, "R1020") + 1))); // <choiceDB name='����' uid='3020' ref='0035'
          else
           FCardData->AddPair("3020", NewObjPair2(FQ, "R1020")); // <choiceDB name='����' uid='3020' ref='0035'
          FCardData->AddPair("3021", FQ->FieldByName("R1021")->AsString); // <text     name='���' uid='3021'
          FCardData->AddPair("3185", Priv_Val); // <text name='��� �� ���������'        uid='3185'
          // <choiceDB name='��������'                uid='1075' ref='003A'
          FCardData->AddPair("3022", FQ->FieldByName("R1022")->AsString);
          // <digit    name='����'                    uid='3022' />  //1022//->          <digit name='����' uid='1022'
          FCardData->AddPair("3023", PrepareStr(FQ->FieldByName("R1023")->AsString));
          // <text     name='�����'                   uid='3023' >  //1023//->          <text name='�����' uid='1023'
          FCardData->AddPair("3025", NewObjPair2(FQ, "R1025"));
          // <binary   name='������� ���������'       uid='3025'  default='uncheck'/>
          FCardData->AddPair("3026", NewObjPair2(FQ, "R1026"));
          // <binary   name='���� ����� �������'      uid='3026'  default='uncheck'>
          FCardData->AddPair("3028", NewObjPair2(FQ, "R1028"));
          // <binary   name='���� ������� �������'    uid='3028'  default='uncheck'>
          FCardData->AddPair("30A8", NewObjPair2(FQ, "R10A8"));
          // <choiceDB name='������ �������'          uid='30A8' comment='empty' ref='0038'  depend='3020.0039' linecount=''>
          FCardData->AddPair("3027", FQ->FieldByName("R1027")->AsString);
          // <digit    name='�������� �������'        uid='3027' > //1027//-> <digit name='�������� �������' uid='1027'
          FCardData->AddPair("30AE", NewObjPair2(FQ, "R10AE"));
          // <binary   name='������� ��������'        uid='30AE'  default='uncheck'/>
          // DeleteJSONPair(FCardData, "1092");           //<extedit  name='�����'                   uid='1092'    //->          <extedit name='�����' uid='1092'
          FCardData->AddPair("30AF", NewObjPair2(FQ, "R10AF"));
          // <choice   name='��� ������'              uid='30AF' default='0'>
          if (FQ->FieldByName("R101F")->AsInteger)
           FCardData->AddPair("317F", NewObjPair(FLPUId, FLPUId));
          // <choiceDB name='���' uid='317F' ref='00C7'  linecount='2'/>
          FCardData->AddPair("301F", NewObjPair2(FQ, "R101F"));
          // <binary   name='��������� � ������ ���'  uid='301F'  invname='��������� � ������ �����������' default='check'/>
          FCardData->AddPair("30B0", NewObjPair2(FQ, "R10B0"));
          // <choiceDB name='����'                    uid='30B0' ref='00AA' depend='317F.00AA;3180.00AA' />
          // <choiceDB    name='���. ������'             uid='3181' ref='00AA' depend='317F.00AA;3180.00AA' />
          FCardData->AddPair("30B1", NewObjPair2(FQ, "R10B1"));
          // <choiceDB name='�������� ��������������' uid='30B1' ref='00AB' />
          FCardData->AddPair("3182", NewObjPair2(FQ, "R2002"));
          // <choiceDB name='���. ����������'         uid='3182' ref='00BE' />
          FCardData->AddPair("3155", ABirthday);
          // <date     name='�� ��������'             uid='3155' /> <date name='�� ��������' uid='1155' isedit='0' default='patbd'/>
          (*FPrivMap)[Priv_Code] = FCardData;
         }
        else
         {
          Priv_Val = JSONToString(FPrivFRC->second, "3185") + ";" + Priv_Val;
          DeleteJSONPair(FPrivFRC->second, "3185");
          FPrivFRC->second->AddPair("3185", Priv_Val);
         }
        FQ->Next();
       }
      for (itCardMap = FPrivMap->begin(); itCardMap != FPrivMap->end(); itCardMap++)
       {
        if (itCardMap->second)
         {
          privData->AddPair(JSONToString(itCardMap->second, "CODE"),
            TJSONObject::ParseJSONValue(itCardMap->second->ToString()));
          delete itCardMap->second;
         }
        itCardMap->second = NULL;
       }
      ACardRoot->AddPair("3003", privData);
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
    FPrivMap->clear();
    delete FPrivMap;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TIC67Sync::Convert102f(TJSONObject * ACardRoot, UnicodeString AUCode, UnicodeString APref,
  TDate ABirthday, TJSONObject * ACls)
 {
  bool RC = false;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    TJSONObject * FCardData;
    quExec(FQ, false, "Select * From card_" + APref + "102F where ucode=" + AUCode);
    if (FQ->RecordCount)
     {
      TJSONObject * testData = new TJSONObject;
      while (!FQ->Eof)
       {
        FCardData = new TJSONObject;
        try
         {
          FCardData->AddPair("CODE", FQ->FieldByName("code")->AsString);
          // FCardData->AddPair("32D8", NewObjPair(icsNewGUID(), "guid"));
          FCardData->AddPair("017B", NewObjPair2(FQ, "UCODE"));
          // <extedit name='��� ��������' uid='017B' ref='1000' depend='017B.017B' isedit='0' fieldtype='integer'/>
          // <extedit name='��� �����' uid='32AF' ref='3084' depend='32AF.32AF' isedit='0' fieldtype='integer'/>
          FCardData->AddPair("1031", FQ->FieldByName("R1031")->AsDateTime.FormatString("dd.mm.yyyy"));
          // -> <date name='���� ����������' uid='1031'
          // <extedit name='�������' uid='10B5'
          // <choiceDB name='��������' uid='1200' ref='003A'/>
          switch (FQ->FieldByName("R1032")->AsInteger)
           {
           case 1:
             {
              FCardData->AddPair("1200", NewObjPair("1", "1"));
              break;
             }
           case 2:
             {
              FCardData->AddPair("1200", NewObjPair("3", "3"));
              break;
             }
           case 3:
             {
              FCardData->AddPair("1200", NewObjPair("5", "5"));
              break;
             }
           case 4:
             {
              FCardData->AddPair("1200", NewObjPair("4", "4"));
              break;
             }
           case 5:
             {
              FCardData->AddPair("1200", NewObjPair("6", "6"));
              break;
             }
           case 6:
             {
              FCardData->AddPair("1200", NewObjPair("2", "2"));
              break;
             }
           case 7:
             {
              FCardData->AddPair("1200", NewObjPair("7", "7"));
              break;
             }
           case 8:
             {
              FCardData->AddPair("1200", NewObjPair("13", "13"));
              break;
             }
           case 9:
             {
              FCardData->AddPair("1200", NewObjPair("14", "14"));
              break;
             }
           case 10:
             {
              FCardData->AddPair("1200", NewObjPair("1", "1"));
              break;
             }
           case 11:
             {
              FCardData->AddPair("1200", NewObjPair("26", "26"));
              break;
             }
           case 12:
             {
              FCardData->AddPair("1200", NewObjPair("6", "6"));
              break;
             }
           case 13:
             {
              FCardData->AddPair("1200", NewObjPair("13", "13"));
              break;
             }
           case 14:
             {
              FCardData->AddPair("1200", NewObjPair("7", "7"));
              break;
             }
           case 15:
             {
              FCardData->AddPair("1200", NewObjPair("5", "5"));
              break;
             }
           case 16:
             {
              FCardData->AddPair("1200", NewObjPair("3", "3"));
              break;
             }
           case 17:
             {
              FCardData->AddPair("1200", NewObjPair("1", "1"));
              break;
             }
           case 18:
             {
              FCardData->AddPair("1200", NewObjPair("1", "1"));
              break;
             }
           case 19:
             {
              FCardData->AddPair("1200", NewObjPair("1", "1"));
              break;
             }
           case 20:
             {
              FCardData->AddPair("1200", NewObjPair("18", "18"));
              break;
             }
           case 21:
             {
              FCardData->AddPair("1200", NewObjPair("1", "1"));
              break;
             }
           case 22:
             {
              FCardData->AddPair("1200", NewObjPair("1", "1"));
              break;
             }
           }
          FCardData->AddPair("1032", NewObjPair2(FQ, "R1032")); // -> <choiceDB name='�����' uid='1032' ref='002A'  />
          FCardData->AddPair("1033", PrepareStr(FQ->FieldByName("R1033")->AsString));
          // -> <text name='����� ���������' uid='1033'   length='20'/>
          FCardData->AddPair("1034", NewObjPair2(FQ, "R1034")); // -> <choice name='�����' uid='1034'   default='0'>
          FCardData->AddPair("103B", NewObjPair2(FQ, "R103B"));
          // -> <binary name='������� ���������' uid='103B'   default='check'/>
          // []<binary name='���� �������' uid='1041' ref='' required='no' inlist='list_ext' depend='' invname='������� �������������' isedit='yes' is3D='yes' default='uncheck'>
          if (!FQ->FieldByName("R1041")->AsInteger)
           FCardData->AddPair("10A7", NewObjPair("27", "27"));
          // -> <choiceDB name='������ �������' uid='10A7' comment='empty' ref='0038'   depend='1032.002B'>
          else
           FCardData->AddPair("10A7", NewObjPair2(FQ, "R10A7"));
          // -> <choiceDB name='������ �������' uid='10A7' comment='empty' ref='0038'   depend='1032.002B'>
          FCardData->AddPair("103F", FQ->FieldByName("R103F")->AsString);
          // -> <digit name='��������' uid='103F'   digits='13' decimal='6' min='0' max='999999'>
          // <digit name='�������� 2' uid='0184'   digits='13' decimal='6' min='0' max='999999'>
          // <digit name='�������� 3' uid='0186'   digits='13' decimal='6' min='0' max='999999'>
          // <digit name='�������� 4' uid='0188'   digits='13' decimal='6' min='0' max='999999'>
          // <digit name='�������� 5' uid='018A'   digits='13' decimal='6' min='0' max='999999'>
          FCardData->AddPair("1093", NewObjPair2(FQ, "R1093"));
          // -> <extedit name='�����' uid='1093'   isedit='0' length='9'/>
          if (FQ->FieldByName("R10A5")->AsInteger)
           FCardData->AddPair("018B", NewObjPair(FLPUId, FLPUId));
          // <choiceDB name='���' uid='018B' ref='00C7'  linecount='2'/>
          FCardData->AddPair("10A5", NewObjPair2(FQ, "R10A5")); // -> <binary name='��������� � ������ ���' uid='10A5'
          FCardData->AddPair("10B2", NewObjPair2(FQ, "R10B2"));
          // -> <choiceDB name='����' uid='10B2' comment='empty' ref='00AA'  depend='018B.00AA;018C.00AA' linecount='2'/>
          // <choiceDB name='���. ������' uid='018D' comment='empty'
          FCardData->AddPair("10B3", NewObjPair2(FQ, "R10B3"));
          // -> <choiceDB name='�������� ��������������' uid='10B3' ref='00AB'  linecount='2'/>
          FCardData->AddPair("018E", NewObjPair2(FQ, "R2003"));
          // <choiceDB name='���. ����������' uid='018E' ref='00BE'  /><choiceBD name='���. ����������' uid='2003'
          // -> <digit name='���������� �������� �������' uid='10C1' digits='2' min='0' max='99' isedit='0'/>
          // -> <digit name='���������� ������ �������' uid='10C2' digits='2' min='1' max='99' isedit='0'/>
          FCardData->AddPair("1156", ABirthday); // <date name='�� ��������' uid='1156' isedit='0' default='patbd'/>
          testData->AddPair(FQ->FieldByName("code")->AsString, TJSONObject::ParseJSONValue(FCardData->ToString()));
         }
        __finally
         {
          delete FCardData;
         }
        FQ->Next();
       }
      ACardRoot->AddPair("102F", testData);
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TIC67Sync::Convert1030(TJSONObject * ACardRoot, UnicodeString AUCode, UnicodeString APref,
  TDate ABirthday, TDate ADayTo, TJSONObject * ACls)
 {
  bool RC = false;
  TStringList * FInfList = new TStringList;
  TFDQuery * FQ = CreateTempQuery();
  TJSONMap * FOtvMap = new TJSONMap;
  try
   {
    // TJSONMap * CardMap;
    TJSONMap::iterator itCardMap;
    TJSONObject * FCardData;
    UnicodeString FVal;
    quExec(FQ, false, "Select * From card_" + APref + "1030 where ucode=" + AUCode);
    if (FQ->RecordCount || (int)ADayTo)
     {
      TJSONObject * otvData = new TJSONObject;
      if ((int)ADayTo)
       { // ��������� ����� �� ��������� "�������� �����"
        FCardData = new TJSONObject;
        try
         {
          // FCardData->AddPair("1032", NewObjPair2(FQ, "R1032")); //-> <choiceDB name='�����' uid='1032' ref='002A'  />
          // FCardData->AddPair("1033", FQ->FieldByName("R1033")->AsString); //-> <text name='����� ���������' uid='1033'   length='20'/>
          FCardData->AddPair("CODE", AUCode);
          // FCardData->AddPair("32DA", NewObjPair(icsNewGUID(), "guid"));
          FCardData->AddPair("317C", NewObjPair(AUCode, AUCode));
          // <extedit name='��� ��������'       uid='317C' ref='1000' depend='317C.317C' isedit='0' fieldtype='integer'/>
          FCardData->AddPair("305F", Date().FormatString("dd.mm.yyyy"));
          // <date    name='������ ��������'    uid='305F'  /><date name='������ ��������' uid='105F'
          FCardData->AddPair("3065", ADayTo.FormatString("dd.mm.yyyy"));
          // <date    name='��������� ��������' uid='3065' /><date name='��������� ��������' uid='1065'
          FCardData->AddPair("3043", NewObjPair("0", "���������"));
          // <choice  name='�����'              uid='3043'   default='0'><choice name='�����' uid='1043'
          FCardData->AddPair("3083", "all:��� ��������");
          // <text    name='������ ��������'    uid='3083'   length='255'/><choiceBD name='���������� ��' uid='1083' ref='003A'
          FCardData->AddPair("3066", NewObjPair("5", "�������� �����"));
          // <choice  name='�������' uid='3066'   default='0'><choice name='�������' uid='1066'
          FCardData->AddPair("3082", new TJSONNull); // <extedit name='�����������'        uid='3082'      length='255'>
          FCardData->AddPair("3110", NewObjPair("1", "+"));
          // <binary  name='����� �� ��������'  uid='3110'  default='check'/>
          FCardData->AddPair("3111", NewObjPair("0", "-")); // <binary  name='����� �� �����'     uid='3111' />
          otvData->AddPair(AUCode, FCardData->ToString());
         }
        __finally
         {
          delete FCardData;
         }
       }
      if (FQ->RecordCount)
       {
        // ----------------------
        FOtvMap->clear();
        UnicodeString Otv_Code, Otv_Val;
        TJSONMap::iterator FOtvFRC;
        while (!FQ->Eof)
         {
          FCardData = new TJSONObject;
          try
           {
            Otv_Code = (FQ->FieldByName("R1043")->AsString + "_" + FQ->FieldByName("R105F")->AsString + "_" +
              FQ->FieldByName("R1065")->AsString + "_" + FQ->FieldByName("R1066")->AsString).UpperCase();
            Otv_Val = "";
            FVal    = FQ->FieldByName("R1083")->AsString;
            if (FVal.ToIntDef(0) > 0)
             {
              TAnsiStrMap::iterator FRC = InfMap.find(FVal);
              if (FRC != InfMap.end())
               Otv_Val = FVal + ":" + FRC->second;
             }
            if (Otv_Val.Length())
             {
              FOtvFRC = FOtvMap->find(Otv_Code);
              if (FOtvFRC == FOtvMap->end())
               {
                FCardData->AddPair("CODE", FQ->FieldByName("code")->AsString);
                FCardData->AddPair("317C", NewObjPair2(FQ, "ucode"));
                // <extedit name='��� ��������' uid='317C' ref='1000' depend='317C.317C' isedit='0' fieldtype='integer'/>
                FCardData->AddPair("305F", FQ->FieldByName("R105F")->AsDateTime.FormatString("dd.mm.yyyy"));
                // <date name='������ ��������' uid='305F' />< uid='105F'
                if (!FQ->FieldByName("R1043")->AsInteger)
                 FCardData->AddPair("3065", FQ->FieldByName("R1065")->AsDateTime.FormatString("dd.mm.yyyy"));
                // <date name='��������� ��������' uid='3065' /><date uid='1065'
                else
                 FCardData->AddPair("3065", new TJSONNull);
                // <date name='��������� ��������' uid='3065' /><date uid='1065'
                FCardData->AddPair("3043", NewObjPair2(FQ, "R1043"));
                // <choice name='�����' uid='3043'   default='0'><choice name='�����' uid='1043'
                FCardData->AddPair("3083", Otv_Val);
                FCardData->AddPair("3066", NewObjPair2(FQ, "R1066"));
                // <choice name='�������' uid='3066'   default='0'><choice name='�������' uid='1066' default='0'>
                // []<binary name='������������������ �� ���-10' uid='10A0'
                // <choiceTree name='�����������' uid='1082' ref='2000'
                // []<text name='�������� �����������' uid='10A1'
                // <extedit name='�����������' uid='3082'      length='255'>
                if (FQ->FieldByName("R10A0")->AsInteger) // ������������������ �� ���-10
                 {
                  if (FQ->FieldByName("R1082")->AsInteger)
                   {
                    FVal = "##" + FQ->FieldByName("R1082")->AsString;
                    // <extedit name='�����������' uid='3082'      length='255'>
                    FCardData->AddPair("3082", NewObjPair(FVal, FVal));
                   }
                 }
                else if (FQ->FieldByName("R10A1")->AsString.Trim().Length())
                 {
                  FVal = FQ->FieldByName("R10A1")->AsString.Trim();
                  // <extedit name='�����������' uid='3082'      length='255'>
                  FCardData->AddPair("3082", NewObjPair("-1", PrepareStr(FVal)));
                 }
                FCardData->AddPair("3110", NewObjPair("1", "+"));
                // binary name='����� �� ��������' uid='3110'  default='check'/>
                FCardData->AddPair("3111", NewObjPair("0", "-")); // binary name='����� �� �����' uid='3111' />
                (*FOtvMap)[Otv_Code] = FCardData;
               }
              else
               {
                Otv_Val = JSONToString(FOtvFRC->second, "3083") + "," + Otv_Val;
                DeleteJSONPair(FOtvFRC->second, "3083");
                FOtvFRC->second->AddPair("3083", Otv_Val);
               }
             }
           }
          __finally
           {
            // delete FCardData;
           }
          FQ->Next();
         }
        for (itCardMap = FOtvMap->begin(); itCardMap != FOtvMap->end(); itCardMap++)
         {
          if (itCardMap->second)
           {
            FInfList->Clear();
            FInfList->Delimiter     = ',';
            FInfList->DelimitedText = JSONToString(itCardMap->second, "3083");
            for (int i = 0; i < FInfList->Count; i++)
             FInfList->Strings[i] = GetLPartB(FInfList->Strings[i], ':');
            bool All = true;
            for (TAnsiStrMap::iterator i = InfMap.begin(); (i != InfMap.end()) && All; i++)
             All &= (FInfList->IndexOf(i->first) != -1);
            if (All)
             {
              DeleteJSONPair(itCardMap->second, "3083");
              itCardMap->second->AddPair("3083", "all:��� ��������");
             }
            otvData->AddPair(JSONToString(itCardMap->second, "CODE"),
              TJSONObject::ParseJSONValue(itCardMap->second->ToString()));
            delete itCardMap->second;
            itCardMap->second = NULL;
           }
         }
        // ----------------------
       }
      ACardRoot->AddPair("3030", otvData);
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
    delete FInfList;
    delete FOtvMap;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TIC67Sync::DeletePat(UnicodeString AId)
 {
  LogBegin(__FUNC__);
  try
   {
    if (AId.Length())
     {
      UnicodeString FPref = "";
      UnicodeString FId = AId;
      if (AId[1] == 'a')
       {
        FPref = "a";
        FId   = FId.Delete(1, 1);
       }
      TFDQuery * FQ = CreateTempQuery();
      try
       {
        quExec(FQ, true, "Delete From " + FPref + "casebook1 where code=" + FId);
       }
      __finally
       {
        if (FQ->Transaction->Active)
         FQ->Transaction->Commit();
        DeleteTempQuery(FQ);
       }
     }
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TIC67Sync::CreateTempQuery()
 {
  TFDQuery * FQ = new TFDQuery(NULL);
  FQ->Transaction = new TFDTransaction(NULL);
  try
   {
    FQ->Transaction->Connection          = FIC67Connection;
    FQ->Connection                       = FIC67Connection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  return FQ;
 }
// ---------------------------------------------------------------------------
void __fastcall TIC67Sync::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
// ---------------------------------------------------------------------------
bool __fastcall TIC67Sync::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      LogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    LogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TIC67Sync::PrepareStr(UnicodeString AText)
 {
  UnicodeString RC = AText;
  try
   {
    if (RC.Pos("\\"))
     RC = StringReplace(RC, "\\", "/", TReplaceFlags() << rfReplaceAll);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TIC67Sync::AsDTF(TFDQuery * AQ, UnicodeString AFN)
 {
  return AQ->FieldByName(AFN)->AsDateTime.FormatString("dd.mm.yyyy");
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TIC67Sync::AsStr(TFDQuery * AQ, UnicodeString AFN)
 {
  return AQ->FieldByName(AFN)->AsString;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TIC67Sync::AsInt(TFDQuery * AQ, UnicodeString AFN)
 {
  return AQ->FieldByName(AFN)->AsInteger;
 }
// ---------------------------------------------------------------------------
