/*
 ����        - Planer.cpp
 ������      - ���������� ������������.
 ��������� "���������� ������������"
 ��������    - �����������.
 ���� ����������
 ����������� - ������� �.�.
 ����        - 09.05.2004
 */
// ---------------------------------------------------------------------------
#include <string.h>
#pragma hdrstop
#include <algorithm>
#include <vcl.h>
// #include "DKSTL.h"
#include "dsMISManager.h"
// #include "dsSrvRegTemplate.h"
// #include "JSONUtils.h"
// #include "OrgParsers.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ###########################################################################
// ##                                                                       ##
// ##                     TBackgroundMISSync                                ##
// ##                                                                       ##
// ###########################################################################
class TMISSyncManager::TBackgroundMISSync : public TThread
 {
 private:
  TMISSyncManager * FSyncModule;
 protected:
  virtual void __fastcall Execute();
 public:
  __fastcall TBackgroundMISSync(bool CreateSuspended, TMISSyncManager * ASyncModule);
  __fastcall ~TBackgroundMISSync();
  void __fastcall ThreadSafeCall(TdsSyncMethod Method);
 };
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                     TMISSyncManager::TBackgroundMISSync                ##
// ##                                                                       ##
// ###########################################################################
__fastcall TMISSyncManager::TBackgroundMISSync::TBackgroundMISSync(bool CreateSuspended, TMISSyncManager * ASyncModule)
  : TThread(CreateSuspended), FSyncModule(ASyncModule)
 {
 }
// ---------------------------------------------------------------------------
__fastcall TMISSyncManager::TBackgroundMISSync::~TBackgroundMISSync()
 {
  FSyncModule = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::TBackgroundMISSync::Execute()
 {
  CoInitialize(NULL);
  FSyncModule->Sync();
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::TBackgroundMISSync::ThreadSafeCall(TdsSyncMethod Method)
 {
  Synchronize(Method);
 }
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                              TMISSyncManager                           ##
// ##                                                                       ##
// ###########################################################################
// ---------------------------------------------------------------------------
__fastcall TMISSyncManager::TMISSyncManager(TAxeXMLContainer * AXMLList)
 {
  FDM               = new TdsMISManagerDM(NULL, AXMLList);
  FDM->OnLogMessage = CallLogMsg;
  FDM->OnLogError   = CallLogErr;
  FDM->OnLogSQL     = CallLogSQL;
  FBTime            = 0;
  FETime            = 0;
  FBkgSyncModule    = NULL;
  FStage            = "";
  FMaxProgress      = 10;
  FOnIncProgress    = NULL;
  FDBSupportDate    = NULL;
 }
// ---------------------------------------------------------------------------
__fastcall TMISSyncManager::~TMISSyncManager()
 {
  StopSync();
  if (FBkgSyncModule)
   delete FBkgSyncModule;
  FBkgSyncModule = NULL;
  for (TDBSupportMap::iterator i = FDBSupportMap.begin(); i != FDBSupportMap.end(); i++)
   {
    i->second->OnLogMessage = NULL; // LogMessage;
    i->second->OnLogError   = NULL; // LogError;
    i->second->OnLogSQL     = NULL; // LogSQL;
    i->second->OnLogBegin   = NULL; // LogBegin;
    i->second->OnLogEnd     = NULL; // LogEnd;
    delete i->second;
   }
  FDBSupportMap.clear();
  for (TMISMap::iterator i = FMISList.begin(); i != FMISList.end(); i++)
   {
    i->second->OnLogMessage = NULL; // LogMessage;
    i->second->OnLogError   = NULL; // LogError;
    i->second->OnLogSQL     = NULL; // LogSQL;
    i->second->OnLogBegin   = NULL; // LogBegin;
    i->second->OnLogEnd     = NULL; // LogEnd;
    delete i->second;
   }
  FMISList.clear();
  delete FDM;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::CallOnIncProgress()
 {
  SyncCall(& SyncCallOnIncProgress);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::SyncCallOnIncProgress()
 {
  if (FOnIncProgress)
   FOnIncProgress(this);
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TMISSyncManager::StartSync()
 {
  CallLogBegin(__FUNC__);
  UnicodeString RC = "error";
  try
   {
    if (InSync)
     CallLogErr("����������� �������������.", __FUNC__);
    else
     {
      if (FBkgSyncModule)
       {
        FBkgSyncModule->Terminate();
        Sleep(50);
        delete FBkgSyncModule;
        FBkgSyncModule = NULL;
       }
      FBkgSyncModule = new TBackgroundMISSync(false, this);
      RC = "ok";
     }
   }
  __finally
   {
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TMISSyncManager::StopSync()
 {
  UnicodeString RC = "error";
  CallLogBegin(__FUNC__);
  try
   {
    FTerminated = true;
    if (FBkgSyncModule && InSync)
     {
      for (TDBSupportMap::iterator i = FDBSupportMap.begin(); i != FDBSupportMap.end(); i++)
       i->second->StopSync();
      for (TMISMap::iterator i = FMISList.begin(); i != FMISList.end(); i++)
       i->second->StopSync();
      FInSync     = false;
      delete FBkgSyncModule;
      FBkgSyncModule = NULL;
     }
    RC = "ok";
   }
  __finally
   {
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TMISSyncManager::SyncCall(TdsSyncMethod AMethod)
 {
  if (FBkgSyncModule)
   FBkgSyncModule->ThreadSafeCall(AMethod);
  else
   AMethod();
 }
// ---------------------------------------------------------------------------
int __fastcall TMISSyncManager::GetMaxProgress()
 {
  return FMaxProgress;
 }
// ---------------------------------------------------------------------------
int __fastcall TMISSyncManager::GetCurProgress()
 {
  return FCurrProgress;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TMISSyncManager::GetCurStage() const
 {
  return FStage;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsLogMessage(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsLogError(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::LogBegin(UnicodeString AFuncName)
 {
  dsLogBegin(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::LogEnd(UnicodeString AFuncName)
 {
  dsLogEnd(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::CallLogMsg(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogMsg);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::CallLogErr(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogErr);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::CallLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  FLogMsg      = ASQL;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  FLogExecTime = AExecTime;
  SyncCall(& SyncCallLogSQL);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::CallLogBegin(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogBegin);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::CallLogEnd(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogEnd);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::SyncCallLogMsg()
 {
  if (FOnLogMessage)
   FOnLogMessage(FLogMsg, FLogFuncName, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::SyncCallLogSQL()
 {
  if (FOnLogSQL)
   FOnLogSQL(FLogMsg, FLogFuncName, FLogExecTime, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::SyncCallLogErr()
 {
  if (FOnLogError)
   FOnLogError(FLogMsg, FLogFuncName, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::SyncCallLogBegin()
 {
  if (FOnLogBegin)
   FOnLogBegin(FLogFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSyncManager::SyncCallLogEnd()
 {
  if (FOnLogEnd)
   FOnLogEnd(FLogFuncName);
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TMISSyncManager::Sync()
 {
  CallLogBegin(__FUNC__);
  UnicodeString RC = "������";
  FTerminated   = false;
  FInSync       = true;
  FCurrProgress = 0;
  FMaxProgress  = 0;
  FStage        = "�������������";
  for (TMISMap::iterator i = FMISList.begin(); i != FMISList.end(); i++)
   {
    i->second->StopSync();
    delete i->second;
   }
  FMISList.clear();
  try
   {
    while (!FTerminated)
     {
      if (CheckShedule())
       {
        try
         {
          // ******* Body begin ***************************
          FDM->ConnectUsr();
          TFDQuery * FQ = FDM->CreateTempQuery();
          try
           {
            FDM->quExec(FQ, false, "Select * From class_0008 ");
            TMISSync * FSyncModule;
            TMISMap::iterator FRC;
            TDBSupportMap::iterator DBRC;
            TdsDBSupport * FDBSupport;
            UnicodeString FDBId;
            UnicodeString FMISUrl;
            UnicodeString DB67, AddrComp67;
            int FMisType;
            bool FAutoSync;
            UnicodeString Path67;
            while (!FQ->Eof)
             {
              FDBId = GetDBId(FQ->FieldByName("r0120")->AsString).UpperCase();
              DBRC  = FDBSupportMap.find(FQ->FieldByName("r0011")->AsString.UpperCase());
              if (DBRC == FDBSupportMap.end())
               {
                FDBSupport               = new TdsDBSupport;
                FDBSupport->OnLogMessage = CallLogMsg; // LogMessage;
                FDBSupport->OnLogError   = CallLogErr; // LogError;
                FDBSupport->OnLogBegin   = CallLogBegin; // LogBegin;
                FDBSupport->OnLogEnd     = CallLogEnd; // LogEnd;
                FDBSupport->DBId         = FQ->FieldByName("r0011")->AsString;
                FDBSupportMap[FQ->FieldByName("r0011")->AsString.UpperCase()] = FDBSupport;
               }
              else
               FDBSupport = DBRC->second;
              if (FDBSupport)
               {
                if (FDBSupportDateMap[FDBSupport->DBId.UpperCase()] != Date())
                 {
                  if (!FDBSupport->InSync)
                   {
                    FDBSupportDateMap[FDBSupport->DBId.UpperCase()] = Date();
                    FDBSupport->StartSync();
                   }
                 }
                else if (!FDBSupport->InSync)
                 FDBSupport->StopSync();
               }
              Path67 = ExtractFilePath(FQ->FieldByName("r0011")->AsString) + "\\ic67\\";
              DB67       = icsPrePath(Path67 + "ic.gdb");
              AddrComp67 = icsPrePath(Path67 + "addrdef.adrdat");
              if (FileExists(DB67))
               {
                FRC = FMISList.find("IC67" + FDBId);
                if (FRC == FMISList.end())
                 {
                  FSyncModule               = new TMISSync;
                  FSyncModule->OnLogMessage = CallLogMsg; // LogMessage;
                  FSyncModule->OnLogError   = CallLogErr; // LogError;
                  FSyncModule->OnLogSQL     = CallLogSQL; // LogSQL;
                  FSyncModule->OnLogBegin   = CallLogBegin; // LogBegin;
                  FSyncModule->OnLogEnd     = CallLogEnd; // LogEnd;
                  // FSyncModule->BaseURI      = DB67;
                  FSyncModule->DBId        = FDBId;
                  FSyncModule->LPUId       = FQ->FieldByName("r0120")->AsString;
                  FSyncModule->IdListFunc  = DB67;
                  FSyncModule->PatDataFunc = "null";
                  FSyncModule->MIS         = "IC67";
                  if (FileExists(AddrComp67))
                   FSyncModule->AddrCompData = AddrComp67;
                  FSyncModule->User        = FQ->FieldByName("r0125")->AsString;
                  FSyncModule->Password    = FQ->FieldByName("r0126")->AsString;
                  FSyncModule->SyncType    = TMISSyncType::Full;
                  FMISList["IC67" + FDBId] = FSyncModule;
                 }
                else
                 FSyncModule = FRC->second;
               }
              else
               {
                FMISUrl     = "";
                FMisType    = 0;
                FAutoSync   = false;
                FSyncModule = NULL;
                // uid='0122' '������ / ����� �������'
                if (!FQ->FieldByName("r0122")->IsNull)
                 FMISUrl = FQ->FieldByName("r0122")->AsString.Trim();
                // uid='0121' '���'
                if (!FQ->FieldByName("r0121")->IsNull)
                 FMisType = FQ->FieldByName("r0121")->AsInteger;
                // uid='0127' '�������������'  �������������� = 1; ������ = 2
                if (!FQ->FieldByName("r0127")->IsNull)
                 FAutoSync = (FQ->FieldByName("r0127")->AsInteger == 1) || (FQ->FieldByName("r0127")->AsInteger == 3);
                FRC = FMISList.find(FDBId);
                if (FRC != FMISList.end())
                 { // ������ ������������� ����
                  FSyncModule = FRC->second;
                  if (FAutoSync && FMisType && FMISUrl.Length())
                   { // ������� ��� ��� ������������� � ����� ����� � �������������� �������������
                    if (!((FSyncModule->BaseURI == FMISUrl) && (FSyncModule->User == FQ->FieldByName("r0125")->AsString)
                      && (FSyncModule->Password == FQ->FieldByName("r0126")->AsString) &&
                      (FSyncModule->IdListFunc == MISIdListFunc(FMisType)) &&
                      (FSyncModule->PatDataFunc == MISPatDataFunc(FMisType)) && (FSyncModule->MIS == MISName
                      (FMisType))))
                     { // ��������� ������������� �� ���������
                      FSyncModule->OnLogMessage = NULL; // LogMessage;
                      FSyncModule->OnLogError   = NULL; // LogError;
                      FSyncModule->OnLogBegin   = NULL; // LogBegin;
                      FSyncModule->OnLogEnd     = NULL; // LogEnd;
                      delete FSyncModule;
                      Sleep(100);
                      FSyncModule               = new TMISSync;
                      FSyncModule->OnLogMessage = CallLogMsg; // LogMessage;
                      FSyncModule->OnLogError   = CallLogErr; // LogError;
                      FSyncModule->OnLogBegin   = CallLogBegin; // LogBegin;
                      FSyncModule->OnLogEnd     = CallLogEnd; // LogEnd;
                      FSyncModule->BaseURI      = FMISUrl;
                      FSyncModule->DBId         = FDBId;
                      FSyncModule->LPUId        = FQ->FieldByName("r0120")->AsString;
                      FSyncModule->IdListFunc   = MISIdListFunc(FMisType);
                      FSyncModule->PatDataFunc  = MISPatDataFunc(FMisType);
                      FSyncModule->MIS          = MISName(FMisType);
                      if (FQ->FieldByName("r0127")->AsInteger == 1)
                       FSyncModule->SyncType = TMISSyncType::Full;
                      else if (FQ->FieldByName("r0127")->AsInteger == 3)
                       FSyncModule->SyncType = TMISSyncType::Code;
                      else
                       FSyncModule->SyncType = TMISSyncType::None;
                      FSyncModule->User     = FQ->FieldByName("r0125")->AsString;
                      FSyncModule->Password = FQ->FieldByName("r0126")->AsString;
                      FMISList[FDBId]       = FSyncModule;
                     }
                   }
                  else
                   {
                    FSyncModule = NULL;
                    delete FRC->second;
                    FMISList.erase(FDBId);
                   }
                 }
                else
                 {
                  if (FAutoSync && FMisType && FMISUrl.Length())
                   { // ������� ��� ��� ������������� � ����� ����� � �������������� �������������
                    FSyncModule               = new TMISSync;
                    FSyncModule->OnLogMessage = CallLogMsg; // LogMessage;
                    FSyncModule->OnLogError   = CallLogErr; // LogError;
                    FSyncModule->OnLogBegin   = CallLogBegin; // LogBegin;
                    FSyncModule->OnLogEnd     = CallLogEnd; // LogEnd;
                    FSyncModule->BaseURI      = FMISUrl;
                    FSyncModule->DBId         = FDBId;
                    FSyncModule->LPUId        = FQ->FieldByName("r0120")->AsString;
                    FSyncModule->IdListFunc   = MISIdListFunc(FMisType);
                    FSyncModule->PatDataFunc  = MISPatDataFunc(FMisType);
                    FSyncModule->MIS          = MISName(FMisType);
                    if (FQ->FieldByName("r0127")->AsInteger == 1)
                     FSyncModule->SyncType = TMISSyncType::Full;
                    else if (FQ->FieldByName("r0127")->AsInteger == 3)
                     FSyncModule->SyncType = TMISSyncType::Code;
                    else
                     FSyncModule->SyncType = TMISSyncType::None;
                    FSyncModule->User     = FQ->FieldByName("r0125")->AsString;
                    FSyncModule->Password = FQ->FieldByName("r0126")->AsString;
                    FMISList[FDBId]       = FSyncModule;
                   }
                 }
               }
              if (FSyncModule)
               {
                if (CheckShedule())
                 {
                  if (!FSyncModule->InSync)
                   FSyncModule->StartSync();
                 }
                else if (FSyncModule->InSync)
                 FSyncModule->StopSync();
               }
              FQ->Next();
             }
           }
          __finally
           {
            FDM->DeleteTempQuery(FQ);
            FDM->DisconnectUsr();
           }
          // ******* Body end *****************************
          RC = "ok";
         }
        catch (System::Sysutils::Exception & E)
         {
          CallLogErr(E.Message, __FUNC__);
         }
       }
      else
       {
        TMISSync * FSyncModule;
        for (TMISMap::iterator i = FMISList.begin(); i != FMISList.end(); i++)
         {
          FSyncModule = i->second;
          if (FSyncModule->InSync)
           FSyncModule->StopSync();
          FSyncModule->OnLogMessage = NULL; // LogMessage;
          FSyncModule->OnLogError   = NULL; // LogError;
          FSyncModule->OnLogSQL     = NULL; // LogSQL;
          FSyncModule->OnLogBegin   = NULL; // LogBegin;
          FSyncModule->OnLogEnd     = NULL; // LogEnd;
          Sleep(100);
          delete i->second;
         }
        FMISList.clear();
       }
      for (int i = 0; (i < 10000) && !FTerminated; i++)
       {
        Application->ProcessMessages();
        Sleep(10);
       }
     }
   }
  __finally
   {
    FInSync = false;
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TMISSyncManager::MISName(int AMisType)
 {
  UnicodeString RC = "";
  try
   {
    switch (AMisType)
     {
     case 1:
       { // ����
        RC = "AURA";
        break;
       }
     case 2:
       { // ����� ���������
        RC = "MISCOMMON";
        break;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TMISSyncManager::MISIdListFunc(int AMisType)
 {
  UnicodeString RC = "";
  try
   {
    switch (AMisType)
     {
     case 1:
       { // ����
        RC = "patientIds";
        break;
       }
     case 2:
       { // ����� ���������
        RC = "patientIds";
        break;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TMISSyncManager::MISPatDataFunc(int AMisType)
 {
  UnicodeString RC = "";
  try
   {
    switch (AMisType)
     {
     case 1:
       { // ����
        RC = "patient";
        break;
       }
     case 2:
       { // ����� ���������
        RC = "patient";
        break;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TMISSyncManager::GetDBId(UnicodeString ACode)
 {
  UnicodeString RC = "error";
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    FDM->quExecParam(FQ, false, "Select R011E as DBId From class_00C7 where code=:pCode", "pCode", ACode);
    if (FQ->RecordCount)
     RC = FQ->FieldByName("DBId")->AsString;
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    FDM->DeleteTempQuery(FQ);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
bool __fastcall TMISSyncManager::CheckShedule()
 {
  bool RC = false;
  try
   {
    if ((double)FBTime && (double)FETime)
     {
      Word Hour, Min, Sec, MSec;
      DecodeTime(Time(), Hour, Min, Sec, MSec);
      TTime Cur = TTime(Hour, Min, Sec, 0);
      if (FBTime < FETime)
       {
        RC = (Cur >= FBTime) && (Cur < FETime);
       }
      else
       {
        RC = (Cur >= FBTime) && (Cur <= TTime("23:59:59")) || (Cur >= TTime("00:00:01")) && (Cur < FETime);
       }
     }
    else
     RC = true;
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
bool __fastcall TMISSyncManager::CheckDBSupportShedule()
 {
  bool RC = false;
  try
   {
    TTime FDBSBTime = TTime("23:00:00");
    TTime FDBSETime = TTime("23:30:00");
    if ((double)FDBSBTime && (double)FDBSETime)
     {
      Word Hour, Min, Sec, MSec;
      DecodeTime(Time(), Hour, Min, Sec, MSec);
      TTime Cur = TTime(Hour, Min, Sec, 0);
      if (FDBSBTime < FDBSETime)
       {
        RC = (Cur >= FDBSBTime) && (Cur < FDBSETime);
       }
      else
       {
        RC = (Cur >= FDBSBTime) && (Cur <= TTime("23:59:59")) || (Cur >= TTime("00:00:01")) && (Cur < FDBSETime);
       }
     }
    else
     RC = true;
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
