//---------------------------------------------------------------------------
#ifndef dsMISCommonH
#define dsMISCommonH

#include <System.hpp>
#include <Classes.hpp>
//#include <DateUtils.hpp>

//#include "pFIBDatabase.hpp"
//#include "DKClasses.h"
//#include "XMLContainer.h"
//#include "ICSDATEUTIL.hpp"
//#include "DKUtils.h"

//---------------------------------------------------------------------------
#include "dsMISAPIDMUnit.h"
#include "dsMISSyncModule.h"
//---------------------------------------------------------------------------
class PACKAGE TMISSyncCommon : public TObject
{
typedef void __fastcall (__closure *TdsSyncMethod)(void);
private:
  class TBackgroundMISSync;
  TBackgroundMISSync * FBkgSyncModule;
  bool FTerminated;
  bool FInSync;

  int             FCurrProgress;
  int             FMaxProgress;        // Max �������� ��� ProgressBar'�
  TNotifyEvent    FOnIncProgress;      // �������, ��� ��������� �������� ���������� ������� ��������� ������� ProgressBar'�

  UnicodeString FStage;

  UnicodeString FLogMsg, FLogFuncName;
  int FLogALvl;

  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;

  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;


  void __fastcall SyncCall(TdsSyncMethod AMethod);

  inline void __fastcall CallOnIncProgress();
  void __fastcall SyncCallOnIncProgress();


//  TdsMISSyncDM* FDM;

  typedef map<UnicodeString, TMISSync*> TMISMap;
  TdsMISAPIDM *FDM;
  TMISMap FMISList;

  TTime FBTime, FETime;

  void __fastcall LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl);
  void __fastcall LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl);
  void __fastcall LogBegin(UnicodeString AFuncName);
  void __fastcall LogEnd(UnicodeString AFuncName);

  UnicodeString __fastcall GetDBId(UnicodeString  ACode);


protected:
  void __fastcall SyncCallLogMsg();
  void __fastcall SyncCallLogErr();
  void __fastcall SyncCallLogBegin();
  void __fastcall SyncCallLogEnd();

  void __fastcall CallLogMsg(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogErr(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogBegin(UnicodeString AFuncName);
  void __fastcall CallLogEnd(UnicodeString AFuncName);

  UnicodeString __fastcall Sync();
public:
  __fastcall TMISSyncCommon();
  virtual __fastcall ~TMISSyncCommon();

    UnicodeString __fastcall StartSync();
    UnicodeString __fastcall StopSync();

    __property bool Terminated = {read = FTerminated};
    __property bool InSync = {read = FInSync};

    int __fastcall GetMaxProgress();
    int __fastcall GetCurProgress();
    UnicodeString __fastcall GetCurStage() const;


    __property int          MaxProgress   = { read = FMaxProgress };
    __property TNotifyEvent OnIncProgress = { read = FOnIncProgress, write = FOnIncProgress };

    __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
    __property TOnLogMsgErrEvent OnLogError = {read = FOnLogError, write = FOnLogError};

    __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
    __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};

  __property  TTime SyncStartTime = {read=FBTime, write=FBTime};
  __property  TTime SyncStopTime = {read=FETime, write=FETime};

  System::UnicodeString __fastcall CreatePlan();
};
//---------------------------------------------------------------------------
#endif
