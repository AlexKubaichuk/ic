/*
 ����        - Planer.cpp
 ������      - ���������� ������������.
 ��������� "���������� ������������"
 ��������    - �����������.
 ���� ����������
 ����������� - ������� �.�.
 ����        - 09.05.2004
 */
//---------------------------------------------------------------------------
#include <string.h>
#pragma hdrstop
#include <algorithm>
#include <vcl.h>
//#include "DKSTL.h"
#include "dsMISCommon.h"
//#include "dsSrvRegTemplate.h"
//#include "JSONUtils.h"
//#include "OrgParsers.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//###########################################################################
//##                                                                       ##
//##                     TBackgroundMISSync                                ##
//##                                                                       ##
//###########################################################################
class TMISSyncCommon::TBackgroundMISSync : public TThread
 {
 private:
  TMISSyncCommon * FSyncModule;
 protected:
  virtual void __fastcall Execute();
 public:
  __fastcall TBackgroundMISSync(bool CreateSuspended, TMISSyncCommon * ASyncModule);
  __fastcall ~TBackgroundMISSync();
  void __fastcall ThreadSafeCall(TdsSyncMethod Method);
 };
//---------------------------------------------------------------------------
//###########################################################################
//##                                                                       ##
//##                     TMISSyncCommon::TBackgroundMISSync                ##
//##                                                                       ##
//###########################################################################
__fastcall TMISSyncCommon::TBackgroundMISSync::TBackgroundMISSync(bool CreateSuspended, TMISSyncCommon * ASyncModule)
    : TThread(CreateSuspended), FSyncModule(ASyncModule)
 {
 }
//---------------------------------------------------------------------------
__fastcall TMISSyncCommon::TBackgroundMISSync::~TBackgroundMISSync()
 {
  FSyncModule = NULL;
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::TBackgroundMISSync::Execute()
 {
  CoInitialize(NULL);
  FSyncModule->Sync();
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::TBackgroundMISSync::ThreadSafeCall(TdsSyncMethod Method)
 {
  Synchronize(Method);
 }
//---------------------------------------------------------------------------
//###########################################################################
//##                                                                       ##
//##                              TMISSyncCommon                           ##
//##                                                                       ##
//###########################################################################
//---------------------------------------------------------------------------
__fastcall TMISSyncCommon::TMISSyncCommon()
 {
  /*
   FDM = new TdsMISSyncDM(NULL);

   FDM->OnLogMessage = CallLogMsg;
   FDM->OnLogError   = CallLogErr;

   FDM->OnLogBegin = CallLogBegin;
   FDM->OnLogEnd   = CallLogEnd;

   */
  //dsLogC(__FUNC__);
  FDM    = new TdsMISAPIDM(NULL);
  FBTime = 0;
  FETime = 0;

  FBkgSyncModule = NULL;
  FStage         = "";
  FMaxProgress   = 10;
  FOnIncProgress = NULL;

  //dsLogC(__FUNC__, true);
 }
//---------------------------------------------------------------------------
__fastcall TMISSyncCommon::~TMISSyncCommon()
 {
  StopSync();
  if (FBkgSyncModule)
   delete FBkgSyncModule;
  FBkgSyncModule = NULL;

  for (TMISMap::iterator i = FMISList.begin(); i != FMISList.end(); i++)
   delete i->second;
  FMISList.clear();
  delete FDM;
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::CallOnIncProgress()
 {
  SyncCall(& SyncCallOnIncProgress);
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::SyncCallOnIncProgress()
 {
  if (FOnIncProgress)
   FOnIncProgress(this);
 }
//---------------------------------------------------------------------------
System::UnicodeString __fastcall TMISSyncCommon::StartSync()
 {
  CallLogBegin(__FUNC__);
  UnicodeString RC = "error";
  try
   {
    if (InSync)
     CallLogErr("����������� �������������.", __FUNC__);
    else
     {
      if (FBkgSyncModule)
       {
        FBkgSyncModule->Terminate();
        Sleep(50);
        delete FBkgSyncModule;
        FBkgSyncModule = NULL;
       }
      FBkgSyncModule = new TBackgroundMISSync(false, this);
      RC = "ok";
     }
   }
  __finally
   {
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TMISSyncCommon::StopSync()
 {
  UnicodeString RC = "error";
  CallLogBegin(__FUNC__);
  try
   {
    if (FBkgSyncModule && InSync)
     {
      for (TMISMap::iterator i = FMISList.begin(); i != FMISList.end(); i++)
       i->second->StopSync();

      FTerminated = true;
      FInSync     = false;
      delete FBkgSyncModule;
      FBkgSyncModule = NULL;
     }
    RC = "ok";
   }
  __finally
   {
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
void __fastcall TMISSyncCommon::SyncCall(TdsSyncMethod AMethod)
 {
  if (FBkgSyncModule)
   FBkgSyncModule->ThreadSafeCall(AMethod);
  else
   AMethod();
 }
//---------------------------------------------------------------------------
int __fastcall TMISSyncCommon::GetMaxProgress()
 {
  return FMaxProgress;
 }
//---------------------------------------------------------------------------
int __fastcall TMISSyncCommon::GetCurProgress()
 {
  return FCurrProgress;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TMISSyncCommon::GetCurStage() const
 {
  return FStage;
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::CallLogMsg(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogMsg);
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::CallLogErr(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogErr);
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::CallLogBegin(UnicodeString  AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogBegin);
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::CallLogEnd(UnicodeString  AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogEnd);
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::SyncCallLogMsg()
 {
  if (FOnLogMessage)
   FOnLogMessage(FLogMsg, FLogFuncName, FLogALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::SyncCallLogErr()
 {
  if (FOnLogError)
   FOnLogError(FLogMsg, FLogFuncName, FLogALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::SyncCallLogBegin()
 {
  if (FOnLogBegin)
   FOnLogBegin(FLogFuncName);
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::SyncCallLogEnd()
 {
  if (FOnLogEnd)
   FOnLogEnd(FLogFuncName);
 }
//---------------------------------------------------------------------------
System::UnicodeString __fastcall TMISSyncCommon::Sync()
 {
  CallLogBegin(__FUNC__);
  UnicodeString RC = "������";
  FTerminated   = false;
  FInSync       = true;
  FCurrProgress = 0;
  FMaxProgress  = 0;
  FStage        = "�������������";
  try
   {
    while (!FTerminated)
     {
      try
       {
        // ******* Body begin ***************************
        /*
         for (TMISMap::iterator i = FMISList.begin(); i != FMISList.end(); i++)
         {
         i->second->StopSync();
         delete i->second;
         }
         FMISList.clear();
         FDM->ConnectUsr();
         TFDQuery * FQ = FDM->CreateUsrTempQuery();
         try
         {
         FDM->quExec(FQ, false, "Select * From class_0008 ");
         TMISSync * FSyncModule;
         TMISMap::iterator FRC;
         UnicodeString FDBId;
         UnicodeString FMISUrl;
         int FMisType;
         bool FAutoSync;
         while (!FQ->Eof)
         {
         FDBId = GetDBId(FQ->FieldByName("r0120")->AsString).UpperCase();
         FRC   = FMISList.find(FDBId);
         //uid='0122' '������ / ����� �������'
         FMISUrl = "";
         if (!FQ->FieldByName("r0122")->IsNull)
         FMISUrl = FQ->FieldByName("r0122")->AsString.Trim();

         //uid='0121' '���'
         FMisType = 0;
         if (!FQ->FieldByName("r0121")->IsNull)
         FMisType = FQ->FieldByName("r0121")->AsInteger;

         //uid='0127' '�������������'  �������������� = 1; ������ = 2
         FAutoSync = false;
         if (!FQ->FieldByName("r0127")->IsNull)
         FAutoSync = (FQ->FieldByName("r0127")->AsInteger == 1);

         if (FAutoSync && FMisType && FMISUrl.Length() && FRC == FMISList.end())
         {//������� ��� ��� ������������� � ����� ����� � �������������� �������������
         FSyncModule               = new TMISSync;
         FSyncModule->OnLogMessage = LogMessage;
         FSyncModule->OnLogError   = LogError;
         FSyncModule->OnLogBegin   = LogBegin;
         FSyncModule->OnLogEnd     = LogEnd;
         FSyncModule->BaseURI      = FMISUrl;
         FSyncModule->DBId         = FDBId;
         FSyncModule->LPUId        = FQ->FieldByName("r0120")->AsString;
         switch (FMisType)
         {
         case 1:
         {//����
         FSyncModule->IdListFunc  = "patientIds";
         FSyncModule->PatDataFunc = "patient";
         FSyncModule->MIS         = "AURA";
         break;
         }
         case 2:
         {//����� ���������
         FSyncModule->IdListFunc  = "patientIds";
         FSyncModule->PatDataFunc = "patient";
         FSyncModule->MIS         = "MISCOMMON";
         break;
         }
         }
         FSyncModule->User = FQ->FieldByName("r0125")->AsString;
         FSyncModule->Password = FQ->FieldByName("r0126")->AsString;
         FMISList[FDBId]       = FSyncModule;
         FSyncModule->StartSync();
         }
         FQ->Next();
         }
         }
         __finally
         {
         if (FQ->Transaction->Active)
         FQ->Transaction->Commit();
         FDM->DeleteTempQuery(FQ);
         }
         */
        // ******* Body end *****************************
        RC = "ok";
       }
      catch (System::Sysutils::Exception & E)
       {
        CallLogErr(E.Message, __FUNC__);
       }
       Sleep(10000);
     }
   }
  __finally
   {
    FInSync = false;
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------

void __fastcall TMISSyncCommon::LogMessage(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl)
 {
  dsLogMessage(AMessage, AFuncName, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::LogError(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl)
 {
  dsLogError(AMessage, AFuncName, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::LogBegin(UnicodeString  AFuncName)
 {
  dsLogBegin(AFuncName);
 }
//---------------------------------------------------------------------------
void __fastcall TMISSyncCommon::LogEnd(UnicodeString  AFuncName)
 {
  dsLogEnd(AFuncName);
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TMISSyncCommon::GetDBId(UnicodeString  ACode)
 {
  UnicodeString RC = "error";
  TFDQuery * FQ = FDM->CreateUsrTempQuery();
  try
   {
    FDM->quExecParam(FQ, false, "Select R011E as DBId From class_00C7 where code=:pCode", "pCode", ACode);
    if (FQ->RecordCount)
     RC = FQ->FieldByName("DBId")->AsString;
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    FDM->DeleteTempQuery(FQ);
   }
  return RC;
 }
//----------------------------------------------------------------------------
