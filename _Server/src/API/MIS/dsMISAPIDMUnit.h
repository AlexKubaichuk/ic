//---------------------------------------------------------------------------

#ifndef dsMISAPIDMUnitH
#define dsMISAPIDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
//---------------------------------------------------------------------------
//#include "ICUtils.h"
//#include "IcsXMLDoc.h"
#include "dsKLADRUnit.h"
#include <System.JSON.hpp>
#include "XMLContainer.h"
#include <FireDAC.Phys.FB.hpp>
//#include "dsSrvClassifUnit.h"
#include "OrgParsers.h"
#include <FireDAC.Phys.FBDef.hpp>
#include "icsLog.h"
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.VCLUI.Wait.hpp>
#include <Datasnap.DSClientRest.hpp>
#include "kabQueryUtils.h"
//#include <IPPeerClient.hpp>
//---------------------------------------------------------------------------
class TdsMISAPIDM : public TDataModule
{
__published:	// IDE-managed Components
 TFDConnection *ICConnection;
private:	// User declarations
  TTagNode *FDefXML;
  UnicodeString __fastcall NewGUID();
  TAxeXMLContainer *FXMLList;
  TAxeXMLContainer *FBaseXMLList;
  bool __fastcall FLoadXML(TTagNode *ItTag, UnicodeString &Src);

public:		// User declarations
  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AParam1Name,      Variant AParam1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AParam2Name = "", Variant AParam2Val = 0,
                               UnicodeString AParam3Name = "", Variant AParam3Val = 0,
                               UnicodeString AParam4Name = "", Variant AParam4Val = 0);

  __fastcall TdsMISAPIDM(TComponent* Owner, TAxeXMLContainer *AXMLList);
  __fastcall ~TdsMISAPIDM();

  __property TTagNode* DefXML = {read=FDefXML, write=FDefXML};
  __property TAxeXMLContainer *XMLList = {read=FXMLList};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsMISAPIDM *dsMISAPIDM;
//---------------------------------------------------------------------------
#endif
