//---------------------------------------------------------------------------
#ifndef dsMISAPIUnitH
#define dsMISAPIUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <DataSnap.DSServer.hpp>
//#include <Datasnap.DSProviderDataModuleAdapter.hpp>
//---------------------------------------------------------------------------
#include <Data.DB.hpp>
//#include <IPPeerClient.hpp>
#include "dsKLADRUnit.h"
#include "dsOrgUnit.h"
#include "dsMISAPIDMUnit.h"
#include "dsSrvRegUnit.h"
#include "SVRClientClassesUnit.h"
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <IPPeerClient.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
//---------------------------------------------------------------------------
#include "dsMKB.h"
//---------------------------------------------------------------------------
class DECLSPEC_DRTTI TMISAPI : public TDataModule
 {
 __published:

 private: //User declarations
  TTagNode *    FDefXML;
  TdsMISAPIDM * FDM;
  TdsMKB *      FMKB;
  /*
   TdsKLADRClass *   FKLADRComp;
   TdsOrgClass *     FOrgComp;
   UnicodeString     FUploadData;
   UnicodeString     FLPUID;
   */
  TFDQuery *        FClassDataQuery;
  TdsRegClassData * FClassData;

  TJSONValue * __fastcall AsDate(TFDQuery * AQ, UnicodeString  AId);
  TJSONValue * __fastcall AsBinary(TFDQuery * AQ, UnicodeString  AId);
  TJSONValue * __fastcall AsJSONValue(TFDQuery * AQ, UnicodeString  AId);
  TJSONValue * __fastcall AsClassObj(TFDQuery * AQ, UnicodeString  AId);
  TJSONValue * __fastcall AsChoiceObj(TFDQuery * AQ, UnicodeString  AId);
  UnicodeString __fastcall AsIntStr(TFDQuery * AQ, UnicodeString  AFlName);

  TJSONArray * __fastcall VacInfList(TFDQuery * AQ, UnicodeString  AId);

  int __fastcall ReacCountByFormat(TFDQuery * AQ, UnicodeString  AId);
  TJSONValue * __fastcall TestReacValue(TFDQuery * AQ);

  TJSONValue * __fastcall AsMKBCode(TFDQuery * AQ, UnicodeString  AId);

  TJSONArray * __fastcall GetPrivData(__int64 APatId);
  TJSONArray * __fastcall GetTestData(__int64 APatId);
  TJSONArray * __fastcall GetCancelData(__int64 APatId);
  void __fastcall LogMessage(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl);
  void __fastcall LogError(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl);
  void __fastcall LogBegin(UnicodeString  AFuncName);
  void __fastcall LogEnd(UnicodeString  AFuncName);
public: //User declarations
  __fastcall TMISAPI(TComponent * Owner, TdsMKB * AMKB, TAxeXMLContainer *AXMLList);
  __fastcall ~TMISAPI();

  TJSONObject * PatientF63(UnicodeString APtId);
  UnicodeString ImportPatient(UnicodeString AMIS, TJSONObject * ASrc);
 };
//---------------------------------------------------------------------------
//extern PACKAGE TdsICClass *dsICClass;
//---------------------------------------------------------------------------
#endif
