//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsMISAPIDMUnit.h"
#include "ICSDATEUTIL.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TdsMISAPIDM * dsMISAPIDM;
//---------------------------------------------------------------------------
__fastcall TdsMISAPIDM::TdsMISAPIDM(TComponent * Owner, TAxeXMLContainer *AXMLList) : TDataModule(Owner)
 {
  FBaseXMLList          = AXMLList;
  FXMLList              = new TAxeXMLContainer;
  FXMLList->OnExtGetXML = FLoadXML;
 }
//---------------------------------------------------------------------------
__fastcall TdsMISAPIDM::~TdsMISAPIDM()
 {
  //for (TdsSrvClassifDataMap::iterator i = FClassifData.begin(); i != FClassifData.end(); i++)
  //delete i->second;
  //FClassifData.clear();
  delete FXMLList;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsMISAPIDM::FLoadXML(TTagNode * ItTag, UnicodeString & Src)
 {
  bool RC = false;
  try
   {
/*
    UnicodeString xmlPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\" + Src + ".zxml");
    if (FileExists(xmlPath))
     {
      ItTag->LoadFromZIPXMLFile(xmlPath);
      RC = true;
     }
*/
      ItTag->AsXML = FBaseXMLList->GetXML(Src)->AsXML;
      RC = true;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
TFDQuery * __fastcall TdsMISAPIDM::CreateTempQuery()
 {
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = ICConnection;
    FQ->Connection                       = ICConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  return FQ;
 }
//---------------------------------------------------------------------------
void __fastcall TdsMISAPIDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsMISAPIDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString  ASQL,
    UnicodeString  ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsMISAPIDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString  ASQL,
    UnicodeString  AParam1Name, Variant AParam1Val, UnicodeString  ALogComment,
    UnicodeString  AParam2Name, Variant AParam2Val, UnicodeString  AParam3Name, Variant AParam3Val,
    UnicodeString  AParam4Name, Variant AParam4Val)
 {
  bool RC = false;
  TTime FBeg = Time();
  UnicodeString FParams = "; Params: ";
  if (AParam1Name.Length())
   FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
  if (AParam2Name.Length())
   FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
  if (AParam3Name.Length())
   FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
  if (AParam4Name.Length())
   FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "");
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
      if (AParam2Name.Length())
       AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
      if (AParam3Name.Length())
       AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
      if (AParam4Name.Length())
       AQuery->ParamByName(AParam4Name)->Value = AParam4Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL(((ALogComment.Length()) ? ALogComment : ASQL) + FParams, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsMISAPIDM::NewGUID()
 {
  return icsNewGUID().UpperCase();
 }
//---------------------------------------------------------------------------

