//---------------------------------------------------------------------------
#ifndef dsMISCommonSyncH
#define dsMISCommonSyncH
//---------------------------------------------------------------------------
#include <System.hpp>
#include <Classes.hpp>
#include "dsMISBaseSync.h"
//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                              TMISCommonSync                           ##
//##                                                                       ##
//###########################################################################

//---------------------------------------------------------------------------
class PACKAGE TMISCommonSync : public TdsMISBaseSync
{
private:

  bool __fastcall AddPatOrg(TJSONObject * APtRoot, TJSONObject * AClsRoot, TJSONObject * ASrc);
  bool __fastcall AddPatUch(TJSONObject * APtRoot, TJSONObject * AClsRoot, TJSONObject * ASrc);
  bool __fastcall AddPatExitState(TJSONObject * APtRoot, TJSONObject * AClsRoot, TJSONObject * ASrc);
  void __fastcall AddPatInsComp(TJSONObject * APtRoot, TJSONObject * AClsRoot, TJSONObject * ASrc);
  void __fastcall AddPatDocument(TJSONObject * APtRoot, TJSONObject * ASrc);

public:

  __fastcall TMISCommonSync();
  __fastcall ~TMISCommonSync();
  TJSONObject* __fastcall Convert(TJSONObject* ASrc);
};
//---------------------------------------------------------------------------
#endif
