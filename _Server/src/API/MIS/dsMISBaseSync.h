//---------------------------------------------------------------------------
#ifndef dsMISBaseSyncH
#define dsMISBaseSyncH
//---------------------------------------------------------------------------
#include <System.hpp>
#include <Classes.hpp>

//---------------------------------------------------------------------------
#include "JSONUtils.h"
#include "icsLog.h"
//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                              TdsMISBaseSync                           ##
//##                                                                       ##
//###########################################################################

//---------------------------------------------------------------------------
class PACKAGE TdsMISBaseSync : public TObject
{
private:

  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;
  TOnLogSQLEvent      FOnLogSQL;

  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;

protected:
  UnicodeString FLPUId;

  void __fastcall LogMessage(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall LogError(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall LogSQL(UnicodeString ASQL, UnicodeString AFuncName = "", TTime AExecTime = 0, int ALvl = 1);
  void __fastcall LogBegin(UnicodeString AFuncName);
  void __fastcall LogEnd(UnicodeString AFuncName);

/*  TJSONValue * __fastcall AsObj(TJSONObject * ASrc, UnicodeString  AId);
  TJSONValue * __fastcall AsNewJSONValue(TJSONObject * ASrc, UnicodeString  AId);
  TJSONObject * __fastcall NewObjPair(UnicodeString  AId, UnicodeString  AVal);
  TJSONObject * __fastcall NewObjPair(UnicodeString  AId, TJSONValue *AVal);
  bool __fastcall IsNull(TJSONObject * ASrc, UnicodeString  AId);
*/
  void __fastcall AddClData(TJSONObject * ACls, UnicodeString  AId, UnicodeString ACode, TJSONObject * AClData);

  void __fastcall AddPatState(TJSONObject * APtRoot, bool AUch, bool AOrg, bool AExit);

public:
  __fastcall TdsMISBaseSync();
  virtual __fastcall ~TdsMISBaseSync();

  virtual TJSONObject* __fastcall Convert(TJSONObject* ASrc);

  __property UnicodeString LPUId = {read=FLPUId, write=FLPUId};

  __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
  __property TOnLogMsgErrEvent OnLogError   = {read = FOnLogError, write = FOnLogError};
  __property TOnLogSQLEvent    OnLogSQL     = {read = FOnLogSQL, write = FOnLogSQL};

  __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
  __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};
};
//---------------------------------------------------------------------------
#endif
