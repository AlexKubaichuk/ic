// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsMISSyncDMUnit.h"
#include "dsIC67Sync.h"
// #include "ICSDATEUTIL.hpp"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TdsMISSyncDM * dsMISSyncDM;
// ---------------------------------------------------------------------------
__fastcall TdsMISSyncDM::TdsMISSyncDM(TComponent * Owner) : TDataModule(Owner)
 {
  FAddrCompData = "";
  FAddrComp     = new TAnsiStrMap;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsMISSyncDM::FGetBaseURI()
 {
  return MISClient->BaseURL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsMISSyncDM::FSetBaseURI(UnicodeString AValue)
 {
  MISClient->BaseURL = AValue;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsMISSyncDM::FGetUser()
 {
  return ICImportConnect->UserName;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsMISSyncDM::FSetUser(UnicodeString AValue)
 {
  ICImportConnect->UserName = AValue;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsMISSyncDM::FGetPassword()
 {
  return ICImportConnect->Password;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsMISSyncDM::FSetPassword(UnicodeString AValue)
 {
  ICImportConnect->Password = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsMISSyncDM::LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  if (FOnLogMessage)
   FOnLogMessage(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsMISSyncDM::LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  if (FOnLogError)
   FOnLogError(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsMISSyncDM::LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  if (FOnLogSQL)
   FOnLogSQL(ASQL, AFuncName, AExecTime, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsMISSyncDM::LogBegin(UnicodeString AFuncName)
 {
  if (FOnLogBegin)
   FOnLogBegin(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsMISSyncDM::LogEnd(UnicodeString AFuncName)
 {
  if (FOnLogEnd)
   FOnLogEnd(AFuncName);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsMISSyncDM::IsFullSync()
 {
  LogBegin(__FUNC__);
  bool RC = false;
  try
   {
    TIC67Sync * F67SyncModule = new TIC67Sync(IC67Connection);
    F67SyncModule->LPUId        = LPUId;
    F67SyncModule->OnLogMessage = LogMessage;
    F67SyncModule->OnLogError   = LogError;
    F67SyncModule->OnLogSQL     = LogSQL;
    F67SyncModule->OnLogBegin   = LogBegin;
    F67SyncModule->OnLogEnd     = LogEnd;

    try
     {
      RC = F67SyncModule->IsFullSync();
     }
    __finally
     {
      delete F67SyncModule;
     }
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONArray * __fastcall TdsMISSyncDM::GetPatIdList(bool AIsIC67)
 {
  LogBegin(__FUNC__);
  TJSONArray * RC = NULL;
  try
   {
    if (AIsIC67)
     {
      TIC67Sync * F67SyncModule = new TIC67Sync(IC67Connection);
      F67SyncModule->LPUId        = LPUId;
      F67SyncModule->OnLogMessage = LogMessage;
      F67SyncModule->OnLogError   = LogError;
      F67SyncModule->OnLogSQL     = LogSQL;
      F67SyncModule->OnLogBegin   = LogBegin;
      F67SyncModule->OnLogEnd     = LogEnd;

      try
       {
        RC = (TJSONArray *) TJSONObject::ParseJSONValue(F67SyncModule->GetPatIdList()->ToString());
       }
      __finally
       {
        delete F67SyncModule;
       }
     }
    else
     {
      if (FPatDataFunc.Trim().Length())
       {
        try
         {
          MISRequest->Resource = FIdListFunc;
          MISRequest->Execute();
          RC = (TJSONArray *) TJSONObject::ParseJSONValue(MISRequest->Response->JSONValue->ToString());
         }
        catch (System::Sysutils::Exception & E)
         {
          LogError(E.Message, __FUNC__);
         }
       }
      else
       LogError("Get patient ID list function is not defined.", __FUNC__);
     }
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsMISSyncDM::GetPatData(UnicodeString AId, bool AIsIC67)
 {
  LogBegin(__FUNC__);
  TJSONObject * RC = NULL;
  try
   {
    if (AIsIC67)
     {
      if (AId.Length())
       {
        TIC67Sync * F67SyncModule = new TIC67Sync(IC67Connection);
        F67SyncModule->LPUId        = LPUId;
        F67SyncModule->OnLogMessage = LogMessage;
        F67SyncModule->OnLogError   = LogError;
        F67SyncModule->OnLogSQL     = LogSQL;
        F67SyncModule->OnLogBegin   = LogBegin;
        F67SyncModule->OnLogEnd     = LogEnd;
        F67SyncModule->OnCheckAddr  = CheckAddr67;
        try
         {
          // RC = (TJSONObject *) TJSONObject::ParseJSONValue(F67SyncModule->GetPatDataById(AId)->ToString());
          UnicodeString DDD = F67SyncModule->GetPatDataById(AId);
          dsLogMessage(DDD, __FUNC__, 1);
          RC = (TJSONObject *) TJSONObject::ParseJSONValue(DDD);
         }
        __finally
         {
          delete F67SyncModule;
         }
       }
     }
    else
     {
      if (FPatDataFunc.Trim().Length())
       {
        try
         {
          MISRequest->Resource = FPatDataFunc + "/" + URIEncode(AId);
          MISRequest->Execute();
          RC = (TJSONObject *) TJSONObject::ParseJSONValue(MISRequest->Response->JSONValue->ToString());
         }
        catch (System::Sysutils::Exception & E)
         {
          LogError(E.Message, __FUNC__);
         }
       }
      else
       LogError("Get patient data function is not defined.", __FUNC__);
     }
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsMISSyncDM::DeletePat(UnicodeString AId)
 {
  LogBegin(__FUNC__);
  try
   {
    if (AId.Length())
     {
      TIC67Sync * F67SyncModule = new TIC67Sync(IC67Connection);
      F67SyncModule->LPUId        = LPUId;
      F67SyncModule->OnLogMessage = LogMessage;
      F67SyncModule->OnLogError   = LogError;
      F67SyncModule->OnLogSQL     = LogSQL;
      F67SyncModule->OnLogBegin   = LogBegin;
      F67SyncModule->OnLogEnd     = LogEnd;
      try
       {
        F67SyncModule->DeletePat(AId);
       }
      __finally
       {
        delete F67SyncModule;
       }
     }
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsMISSyncDM::ImportPatData(TJSONObject * AData, UnicodeString & AResMessage)
 {
  LogBegin(__FUNC__);
  AResMessage = "";
  bool RC = false;
  try
   {
    UnicodeString FRecId = "";
    if (FSyncType == TMISSyncType::Code)
     AData->AddPair("synctype", "code");
    else
     AData->AddPair("synctype", "full");

    UnicodeString ImpRC = FEIClient->ImportData("1000", AData, FRecId, AResMessage);
    if (SameText(ImpRC, "ok"))
     RC = true;
    else
     LogError(ImpRC, __FUNC__);
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsMISSyncDM::CreateConnectionClients()
 {
  LogBegin(__FUNC__);
  try
   {
    FEIClient    = new TdsEIDataClassClient(ICImportConnect);
    FAdminClient = new TdsAdminClassClient(ICImportConnect);
    if (FAdminClient)
     FAdminClient->LoginUser("");
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsMISSyncDM::DeleteConnectionClients()
 {
  LogBegin(__FUNC__);
  try
   {
    if (FAdminClient)
     {
      FAdminClient->Logout();
      delete FAdminClient;
      FAdminClient = NULL;
     }
    if (FEIClient)
     delete FEIClient;
    FEIClient = NULL;
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsMISSyncDM::CheckAddr67(UnicodeString AAddr, UnicodeString AHouse, UnicodeString ABuilding,
 UnicodeString AFlat)
 {
  LogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    if (FAddrComp->size())
     {
      TAnsiStrMap::iterator FRC = FAddrComp->find(AAddr.UpperCase());
      if (FRC != FAddrComp->end())
       {
        RC = FRC->second + HouseFlatCodeStr(AHouse, ABuilding, AFlat);
       }
     }
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsMISSyncDM::HouseFlatCodeStr(UnicodeString AHouse, UnicodeString ABuilding,
 UnicodeString AFlat)
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString temp = AHouse.Trim();
    if (ABuilding.Trim().Length())
     temp += "#K" + ABuilding.Trim();

    if (temp.Length())
     {
      RC += temp;
      if (AFlat.Trim().Length())
       RC += "##" + AFlat.Trim();
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsMISSyncDM::PrepareAddr67Comp()
 {
  FAddrComp->clear();
  System::Sysutils::TReplaceFlags flRep;
  flRep << rfReplaceAll << rfIgnoreCase;
  if (FAddrCompData.Length())
   {
    TTagNode * FAddrCompDataDef = new TTagNode;
    try
     {
      FAddrCompDataDef->LoadFromZIPXMLFile(FAddrCompData);
      TTagNode * itNode = FAddrCompDataDef->GetFirstChild();
      while (itNode)
       {
        (*FAddrComp)[itNode->AV["s"].UpperCase()] = StringReplace(itNode->AV["c"], "@02110##", "@02111##", flRep);
        itNode = itNode->GetNext();
       }
     }
    __finally
     {
      delete FAddrCompDataDef;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsMISSyncDM::ClearAddr67Comp()
 {
  FAddrComp->clear();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsMISSyncDM::DataModuleDestroy(TObject * Sender)
 {
  delete FAddrComp;
 }
// ---------------------------------------------------------------------------
