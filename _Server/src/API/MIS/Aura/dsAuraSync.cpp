//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsAuraSync.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//###########################################################################
//##                                                                       ##
//##                              TAuraSync                                 ##
//##                                                                       ##
//###########################################################################
__fastcall TAuraSync::TAuraSync()
 {
 }
//---------------------------------------------------------------------------
__fastcall TAuraSync::~TAuraSync()
 {
 }
//---------------------------------------------------------------------------
bool __fastcall TAuraSync::AddPatOrg(TJSONObject * APtRoot, TJSONObject * AClsRoot, TJSONObject * ASrc)
 {
  //IN :
  //�����������: "org":
  //{
  //"torg":"1-�������� | 2-����� | 3-�������",
  //"orgname":{"id":"<���>","name":"<����.>"},
  //"orgaddr":"<����.>",
  //"orgclass":"<����.>,
  //"orgcllit":"<����.>"
  //}

  //OUT:
  //<'������ �����������' uid = '0117'   < extedit 'cguid' uid = '32CA'   < text  '������������' uid = '0119'

  //'�����������' uid = '0001'
  //.     {} 'cguid' uid = '32CC'
  //.     "" '������������' uid = '0008'
  //.     {} '��� ������������ �� �����' uid = '00A3'
  //.     {} '���' uid = '3196'
  //.     "" '�����' uid = '0107'
  //.     "" '�������' uid = '0009'
  //.     {} '������' uid = '011A'

  //<extedit '��1' uid='0026'
  //<extedit '��2' uid='0027'
  //<extedit '��3' uid='0028'
  //<extedit '������ �����������' uid='011B' ref='0117' depend='0025.0025' isedit='0'  showtype='1' fieldtype='integer'/>

  //<extedit '�����������' uid='0025' comment='search' ref='0001'  depend='011B.0001' linecount='3' showtype='1' fieldtype='integer'/>
  //<binary '������������� ����������� (����)' uid='00F0' required='1'>

  bool RC = false;
  try
   {
    TJSONObject * Org = (TJSONObject *)GetJSONValue(ASrc, "org");
    if (Org)
     {
      if (!Org->Null)
       {
        TJSONObject * OrgNameObj = (TJSONObject *)GetJSONValue(Org, "orgname");
        if (!OrgNameObj->Null)
         {
          UnicodeString OrgName = JSONToString(OrgNameObj, "name", "").Trim();
          UnicodeString OrgCode = JSONToString(OrgNameObj, "id", "").Trim();
          if (OrgName.Length() && OrgCode.Length())
           {//����������� �������
            int OrgType = JSONToString(Org, "torg", "0").ToIntDef(0);

            TJSONObject * OrgClData = new TJSONObject;
            //OrgClData->AddPair("32CC", icsNewGUID().UpperCase());
            OrgClData->AddPair("0008", OrgName); //<text name='������������
            OrgClData->AddPair("0107", JSONToString(Org, "orgaddr", "")); //<text name='�����' uid='0107
            if (OrgType == 2)
             {
              AddClData(AClsRoot, "0117", "2", NewObjPair("0119", "�����"));
              OrgClData->AddPair("00A3", NewObjPair("1773", "�������� ����� � ������� (������) ����� �����������")); //<  '��� ������������ �� �����' uid = '00A3'
              OrgClData->AddPair("011A", NewObjPair("2", "�����")); //<choiceDB name='������' uid='011A'
              APtRoot->AddPair("011B", NewObjPair("2", "�����")); //<choiceDB name='������' uid='011B'
             }
            else if (OrgType == 3)
             {
              AddClData(AClsRoot, "0117", "3", NewObjPair("0119", "������� ���"));
              OrgClData->AddPair("00A3", NewObjPair("1767", "���������� � ��������� ����� �����������")); //<  '��� ������������ �� �����' uid = '00A3'
              OrgClData->AddPair("011A", NewObjPair("3", "������� ���")); //<choiceDB name='������' uid='011A'
              APtRoot->AddPair("011B", NewObjPair("3", "������� ���")); //<choiceDB name='������' uid='011B'
             }

            AddClData(AClsRoot, "0001", OrgCode, OrgClData);
            APtRoot->AddPair("0025", NewObjPair(OrgCode, OrgCode));
            if (OrgType == 2)
             {
              UnicodeString FClass = JSONToString(OrgNameObj, "orgclass", "").Trim();
              UnicodeString FLit = JSONToString(OrgNameObj, "orgcllit", "").Trim();
              if (FClass.Length())
               {
                APtRoot->AddPair("0026", NewObjPair(FClass, FClass));
                if (FLit.Length())
                 APtRoot->AddPair("0027", NewObjPair(FLit, FLit));
               }
             }
            RC = true;
           }
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
bool __fastcall TAuraSync::AddPatUch(TJSONObject * APtRoot, TJSONObject * AClsRoot, TJSONObject * ASrc)
 {
  //IN :
  //�������: "uch":"<����.>"

  //OUT:
  //<'�������' uid='000E'
  //.      {} '���' uid='00F6'
  //.      - <choiceDB name='���������' uid='00F7'
  //.      "" '������������' uid='000F'

  bool RC = false;
  try
   {
    TJSONObject * Uch = (TJSONObject *)GetJSONValue(ASrc, "uch");
    if (Uch)
     {
      if (!Uch->Null)
       {
        TJSONObject * UchClData = new TJSONObject;
        UnicodeString UchName = JSONToString(ASrc, "uch");
        UchClData->AddPair("00F6", NewObjPair(LPUId, LPUId));
        UchClData->AddPair("000F", UchName);

        AddClData(AClsRoot, "000E", "_" + JSONToString(ASrc, "uch").Trim().UpperCase(), UchClData);
        APtRoot->AddPair("00B3", NewObjPair("_" + UchName, UchName)); //�������: "uch":"<����.>" <choiceDB  uid='00B3'
        RC = true;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
bool __fastcall TAuraSync::AddPatExitState(TJSONObject * APtRoot, TJSONObject * AClsRoot, TJSONObject * ASrc)
 {
  //IN :
  //������� �������: "exit":
  //.{
  //. "isexit":"False | True",
  //. "exit":
  //. {
  //.   "id":"<���>",
  //.   "name":"<����.>"
  //. }
  //.}

  //OUT:
  //< '���. ���������� (������������)' uid='00BD'
  //.      "" '������������' uid='00C0'
  //
  //{}< '���. ����������' uid = '00BF'

  bool RC = false;
  try
   {
    TJSONObject * FExit = (TJSONObject *)GetJSONValue(ASrc, "exit");
    if (FExit)
     {
      if (!FExit->Null)
       {
        TJSONValue * IsExit = GetJSONValue(FExit, "isexit");
        TJSONObject * FExitVal = (TJSONObject *)GetJSONValue(FExit, "exit");
        UnicodeString ExitValID = JSONToString(FExitVal, "id").Trim().UpperCase();
        UnicodeString ExitValName = JSONToString(FExitVal, "name").Trim();

        if (ExitValID.Length() && ExitValName.Length() && (IsExit->Value().LowerCase() == "true"))
         {
          AddClData(AClsRoot, "00BD", ExitValID, NewObjPair("00C0", ExitValName));
          APtRoot->AddPair("00BF", NewObjPair(ExitValID, ExitValName));
          RC = true;
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
void __fastcall TAuraSync::AddPatInsComp(TJSONObject * APtRoot, TJSONObject * AClsRoot, TJSONObject * ASrc)
 {
  //IN :
  //��������� �����: "sp":
  //{
  //"inscomp":{"id":"<���>","name":"<����.>"},
  //"ser":"<����.>",
  //"num":"<����.>"
  //}

  //OUT:
  //<'��������� ��������' uid = '0095'  <extedit 'cguid' uid='32CE' <text '������������' uid='0098' <text '�������' uid='0096' <text '�����' uid='0097'
  //.    {}'�����. ��������' uid='0093'
  //.    "" '�����' uid='0099'
  //.    ""'�����' uid='009A'  cast='up' length='16'/>

  TJSONObject * SP = (TJSONObject *)GetJSONValue(ASrc, "sp");
  if (SP)
   {
    if (!SP->Null)
     {
      TJSONObject * InsComp = (TJSONObject *)GetJSONValue(SP, "inscomp");
      UnicodeString InsCompID = JSONToString(InsComp, "id").Trim().UpperCase();
      UnicodeString InsCompName = JSONToString(InsComp, "name").Trim();
      UnicodeString SPSer = JSONToString(SP, "ser");
      UnicodeString SPNum = JSONToString(SP, "num");
      if (InsCompID.Length() && InsCompName.Length() && SPNum.Length())
       {
        AddClData(AClsRoot, "0095", InsCompID, NewObjPair("0098", InsCompName));
        APtRoot->AddPair("0093", NewObjPair(JSONToString(InsComp, "id"), JSONToString(InsComp, "name")));
        APtRoot->AddPair("0099", JSONCopy(SP, "ser", true));
        APtRoot->AddPair("009A", JSONCopy(SP, "num", true));
       }
     }
   }
 }
//----------------------------------------------------------------------------
void __fastcall TAuraSync::AddPatDocument(TJSONObject * APtRoot, TJSONObject * ASrc)
 {
  //IN :
  //������������� ��������: "doc":
  //{
  //"docum":{"id":"<����.>","name":"<����.>"},
  //"ser":"<����.>",
  //"num":"<����.>"
  //}

  //OUT:
  //.   {} '��. ��������' uid='009C'  default='-1'><chval '���' value='0'/><chval '������������� � ��������' value='1'/><chval '�������' value='2'/><chval '������ ��./�' value='3'/></choice>
  //.   "" '�����' uid='009D'
  //.   "" '�����' uid='009E'

  TJSONObject * DOC = (TJSONObject *)GetJSONValue(ASrc, "doc");
  if (DOC)
   {
    TJSONObject * DOCUM = (TJSONObject *)GetJSONValue(DOC, "docum");
    UnicodeString DocId = JSONToString(DOCUM, "id").Trim().UpperCase();
    if (DocId.ToIntDef(0))
     {
      if (DocId == "12")
       APtRoot->AddPair("009C", NewObjPair("1", "������������� � ��������"));
      else if (DocId == "14")
       APtRoot->AddPair("009C", NewObjPair("2", "�������"));
      else
       APtRoot->AddPair("009C", NewObjPair("3", "������ ��./�"));
      APtRoot->AddPair("009D", JSONCopy(DOC, "ser", true));
      APtRoot->AddPair("009E", JSONCopy(DOC, "num", true));
     }
   }
 }
//----------------------------------------------------------------------------
TJSONObject * __fastcall TAuraSync::Convert(TJSONObject * ASrc)
 {
  LogBegin(__FUNC__);
  TJSONObject * RC = NULL;
  UnicodeString tmp;
  try
   {
    if (!(IsNull(ASrc, "id") && IsNull(ASrc, "fam") && IsNull(ASrc, "name") && IsNull(ASrc, "sname")))
     {
      RC = new TJSONObject;
      bool FIsUch = false;
      bool FIsOrg = false;
      bool FIsExit = false;
      TJSONObject * ptRoot = new TJSONObject;
      RC->AddPair("pt", ptRoot);
      TJSONObject * clsRoot = new TJSONObject;
      RC->AddPair("cl", clsRoot);
      ptRoot->AddPair("CODE", new TJSONNull);              // * ��� ��������: "id":"<����.>" <extedit 'cguid' uid='32D2'
      ptRoot->AddPair("32D2", AsObj(ASrc, "id"));          // * ��� ��������: "id":"<����.>" <extedit 'cguid' uid='32D2'
      ptRoot->AddPair("001F", JSONToString(ASrc, "fam"));  // * �������: "fam":"<����.>" <text uid='001F'
      ptRoot->AddPair("0020", JSONToString(ASrc, "name")); // * ���: "name":"<����.>" <text uid='0020'
      ptRoot->AddPair("002F", JSONToString(ASrc, "sname")); // * ��������: "sname":"<����.>" <text uid='002F'
      ptRoot->AddPair("0122", JSONCopy(ASrc, "cardid", true)); //� �����: "cardid":"<����.>" <text  uid='0122'
      ptRoot->AddPair("0030", JSONCopy(ASrc, "rdate", true)); // * ���� �����������: "rdate":"<����>"  ���� ����������� � ��� <date uid='0030'
      ptRoot->AddPair("0031", JSONCopy(ASrc, "bdate", true)); // * ���� ��������: "bdate":"<����>" <date uid='0031'

      // * ���: "sex":"<����.>"  � | �  <choice uid='0021' <'�������' = '1' < �������' = '0'
      if ((JSONToString(ASrc, "sex").UpperCase() == "M")||(JSONToString(ASrc, "sex").UpperCase() == "�"))
       ptRoot->AddPair("0021", NewObjPair("0", "�������"));
      else
       ptRoot->AddPair("0021", NewObjPair("1", "�������"));

      ptRoot->AddPair("0136", NewObjPair("-1", JSONCopy(ASrc, "addr", true))); //�����: "addr":"<����.>" <extedit  uid='0136'

      //<binary '����� �������������' uid='0022'

      ptRoot->AddPair("0148", JSONCopy(ASrc, "mphone", true)); //��������� �������: "mphone":"<����.>"  <text uid='0148'
      ptRoot->AddPair("014A", JSONCopy(ASrc, "hphone", true)); //�������� �������: "hphone":"<����.>" <text uid='014A'
      ptRoot->AddPair("014C", JSONCopy(ASrc, "wphone", true)); //������� �������: "wphone":"<����.>" <text uid='014C'

      FIsOrg = AddPatOrg(ptRoot, clsRoot, ASrc); //�����������: "org":{<torg>,<orgname>,<orgAddress>,<orgclass>,<orgcllit>} <extedit uid='0025'

      ptRoot->AddPair("0023", new TJSONNull);
      //<choice '���������� ������' uid='0023' default='5'><chval '�����������' value='0'/><chval '��������' value='1'/><chval '����������' value='2'/><chval '���������' value='3'/></choice>

      ptRoot->AddPair("00F1", NewObjPair(LPUId, LPUId)); //<choiceDB '���' uid='00F1' ref='00C7' required='1'  isedit='0' default='Main'/>
      FIsUch = AddPatUch(ptRoot, clsRoot, ASrc);         //�������: "uch":"<����.>" <choiceDB  uid='00B3'

      FIsExit = AddPatExitState(ptRoot, clsRoot, ASrc); //������� �������: "exit":{"isexit":"False | True","exit":{"id":"<���>","name":"<����.>"}}

      //<choice '������ ������������' uid='32B3' ><'������������� �� �������' ='1'/><'������������� �� �����������' ='2'/><'������������� �� ������' value='3'/><chval '��������������' value='4'/><chval '�����' value='5'/><chval '�� �������������' value='6'/></choice>
      AddPatState(ptRoot, FIsUch, FIsOrg, FIsExit);

      ptRoot->AddPair("00A5", JSONCopy(ASrc, "snils", true)); //�����: "snils":"<����.>" <text uid='00A5'
      ptRoot->AddPair("330E", NewObjPair("1", "��� (�������������)"));

      AddPatInsComp(ptRoot, clsRoot, ASrc); //��������� �����: "sp":{<inscomp><ser>,<num>}
      AddPatDocument(ptRoot, ASrc);         //������������� ��������: "docum":{"id":"<���>","name":"<����.>"}

      //<choice '������ �����' uid='0138' default='-1'><chval '0(I)Rh-' value='1'/><chval '0(I)Rh+' value='2'/><chval 'A(II)Rh-' value='3'/><chval 'A(II)Rh+' value='4'/><chval 'B(III)Rh-' value='5'/><chval 'B(III)Rh+' value='6'/><chval 'AB(IV)Rh-' value='7'/><chval 'AB(IV)Rh+' value='8'/></choice>
      //<text '������������� �������' uid='0085'  length='255'/>
      //<text '������������� ���������������' uid='0139'  length='255'/>
      //<text '��������� ������� �� ��������' uid='0087'  length='255'/>
      //<binary '��������� ��������� ���' uid='0084'  />
      //<binary '����� ��������' uid='0086'  />
      //<binary '���� �� ����������' uid='0088'  />
      //<binary '���� ���' uid='005A' />
      //<binary '���������������� ���������' uid='00A7' />
      //<binary '������� ������������' uid='005C' />
      //<choice '������������' uid='0131' required='1'  default='0'><chval '���' value='0'/><chval '� ��������' value='1'/><chval '������������' value='2'/></choice>
      //<date '���� ����������' uid='0132' >
      //<text '����� �������������' uid='0133'   length='30'>
      //<binary '������ ���������' uid='00C4' />
      //<date '���� ������' uid='00C5' required='1' >
      //<choiceDB '���' uid='00F3' ref='00C7' />
      //<choiceDB '���. ����������' uid='00BF' ref='00BD'  />
      //<text '����������' uid='00C1'    length='150'/>
      //<date '���. ���� �����' uid='3195' depend='3195.3195' isedit='0'/>
     }
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
