//---------------------------------------------------------------------------
#ifndef dsAuraSyncH
#define dsAuraSyncH
//---------------------------------------------------------------------------
#include <System.hpp>
#include <Classes.hpp>
#include "dsMISBaseSync.h"
//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                              TAuraSync                                 ##
//##                                                                       ##
//###########################################################################

//---------------------------------------------------------------------------
class PACKAGE TAuraSync : public TdsMISBaseSync
{
private:

  bool __fastcall AddPatOrg(TJSONObject * APtRoot, TJSONObject * AClsRoot, TJSONObject * ASrc);
  bool __fastcall AddPatUch(TJSONObject * APtRoot, TJSONObject * AClsRoot, TJSONObject * ASrc);
  bool __fastcall AddPatExitState(TJSONObject * APtRoot, TJSONObject * AClsRoot, TJSONObject * ASrc);
  void __fastcall AddPatInsComp(TJSONObject * APtRoot, TJSONObject * AClsRoot, TJSONObject * ASrc);
  void __fastcall AddPatDocument(TJSONObject * APtRoot, TJSONObject * ASrc);

public:

  __fastcall TAuraSync();
  __fastcall ~TAuraSync();
  TJSONObject* __fastcall Convert(TJSONObject* ASrc);
};
//---------------------------------------------------------------------------
#endif
