//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsMISManagerDMUnit.h"
#include "ICSDATEUTIL.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TdsMISManagerDM * dsMISManagerDM;
//---------------------------------------------------------------------------
__fastcall TdsMISManagerDM::TdsMISManagerDM(TComponent * Owner, TAxeXMLContainer *AXMLList) : TDataModule(Owner)
 {
  FBaseXMLList          = AXMLList;
  FXMLList              = new TAxeXMLContainer;
  FXMLList->OnExtGetXML = FLoadXML;
 }
//---------------------------------------------------------------------------
__fastcall TdsMISManagerDM::~TdsMISManagerDM()
 {
  delete FXMLList;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsMISManagerDM::FLoadXML(TTagNode * ItTag, UnicodeString & Src)
 {
  bool RC = false;
  try
   {
/*
    UnicodeString xmlPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\" + Src + ".zxml");
    if (FileExists(xmlPath))
     {
      ItTag->LoadFromZIPXMLFile(xmlPath);
      RC = true;
     }
*/
      ItTag->AsXML = FBaseXMLList->GetXML(Src)->AsXML;
      RC = true;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
TFDQuery * __fastcall TdsMISManagerDM::CreateTempQuery()
 {
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = UsrConnection;
    FQ->Connection                       = UsrConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  return FQ;
 }
//---------------------------------------------------------------------------
void __fastcall TdsMISManagerDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsMISManagerDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString  ASQL,
    UnicodeString  ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      LogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    LogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsMISManagerDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString  ASQL,
    UnicodeString  AParam1Name, Variant AParam1Val, UnicodeString  ALogComment,
    UnicodeString  AParam2Name, Variant AParam2Val, UnicodeString  AParam3Name, Variant AParam3Val,
    UnicodeString  AParam4Name, Variant AParam4Val)
 {
  bool RC = false;
  TTime FBeg = Time();
  UnicodeString FParams = "; Params: ";
  if (AParam1Name.Length())
   FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
  if (AParam2Name.Length())
   FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
  if (AParam3Name.Length())
   FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
  if (AParam4Name.Length())
   FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "");
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
      if (AParam2Name.Length())
       AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
      if (AParam3Name.Length())
       AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
      if (AParam4Name.Length())
       AQuery->ParamByName(AParam4Name)->Value = AParam4Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      LogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    LogSQL(((ALogComment.Length()) ? ALogComment : ASQL) + FParams, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsMISManagerDM::NewGUID()
 {
  return icsNewGUID().UpperCase();
 }
//---------------------------------------------------------------------------
void __fastcall TdsMISManagerDM::ConnectUsr()
 {
/*
  ConnectDB(Connection, ICDB, __FUNC__);
  DisconnectDB(Connection, __FUNC__);
*/
  ConnectDB(UsrConnection, ExtractFilePath(ParamStr(0)) + "\\USR.FDB", __FUNC__, LogError, LogBegin, LogEnd);
 }
//---------------------------------------------------------------------------
void __fastcall TdsMISManagerDM::DisconnectUsr()
 {
  DisconnectDB(UsrConnection, __FUNC__, LogBegin, LogEnd);
//  if (UsrConnection->Connected)
 }
//---------------------------------------------------------------------------
void __fastcall TdsMISManagerDM::LogMessage(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl)
 {
  if (FOnLogMessage)
   FOnLogMessage(AMessage, AFuncName, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TdsMISManagerDM::LogError(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl)
 {
  if (FOnLogError)
   FOnLogError(AMessage, AFuncName, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TdsMISManagerDM::LogEnd(UnicodeString  AFuncName)
 {
//  if (FOnLogError)
//   FOnLogError(AMessage, AFuncName, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TdsMISManagerDM::LogBegin(UnicodeString  AFuncName)
 {
//  if (FOnLogError)
//   FOnLogError(AMessage, AFuncName, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TdsMISManagerDM::LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  if (FOnLogSQL)
   FOnLogSQL(ASQL, AFuncName, AExecTime, ALvl);
 }
//---------------------------------------------------------------------------

