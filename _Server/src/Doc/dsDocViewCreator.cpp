/*
 ����        - dsDocViewCreator.cpp
 ������      - ��������������� (ICS)
 ��������    - ����������� ���������.
 ���� ����������
 ����������� - ������� �.�.
 ����        - 18.10.2004
 */
//---------------------------------------------------------------------------

#include <basepch.h>

#pragma hdrstop

#include "dsDocViewCreator.h"
#include "dsDocSrvConstDef.h"
#include "dsDocXMLUtils.h"
#include "icsLog.h"

#include <math.h>
//#include "DKFileUtils.h"
//#include "DKMBUtils.h"
//#include "DKRusMB.h"
//#include "DKAxeUtils.h"
//#include "ICSWord.h"

//#include "HTMLPreviewForm.h"

#pragma package(smart_init)

//###########################################################################
//##                                                                       ##
//##                               TdsDocViewCreator                       ##
//##                                                                       ##
//###########################################################################

// ***************************************************************************
// ********************** ��������/�������� ���������� ***********************
// ***************************************************************************

__fastcall TdsDocViewCreator::TdsDocViewCreator(TComponent * Owner)
    : TComponent(Owner),
    FNumTCellAlign("center"),
    FNumTCellFontName("Times New Roman"),
    FNumTCellFontSize(8),
    FNumTCellLTitle(cFMT(& _dsDocViewerNumTCellLTitle))
 {
  dsLogBegin(__FUNC__);
  FDocFormat                 = dfHTML;
  FMode                      = vmSaveToDisk;
  FUseCSS                    = true;
  FUseWordCSS                = true;
  FUseExcelCSS               = true;
  FSpec                      = NULL;
  FDoc                       = NULL;
  FTmpPath                   = GetTempPath();
  FDefSavePath               = "";
  FHTML                      = "";
  FndHTML                    = NULL;
  FXMLContainer              = NULL;
  FPreviewType               = ptUnknown;
  FState                     = csNone;
  FDocAssign                 = false;
  FListZeroValCheckCountOnly = true;
  FDocXMLTemplate            = NULL;
  //FICSWordTemplateFN = "ICS_Word.dot";

  __BorderWidth = "";
  __RowNumbers  = true;
  __ColNumbers  = true;
  __RowNum      = 1;
  __ColNum      = 1;
  __DopInfo     = new TStringList;
  __PBMax       = 0;
  __PBPosition  = 0;
  __URL         = "";
  //__DocName =  "";
  __OnFly              = false;
  __CanShowPreviewDlg  = false;
  FOnChangeCreateStage = NULL;
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

__fastcall TdsDocViewCreator::~TdsDocViewCreator()
 {
  dsLogBegin(__FUNC__);
  if (FDocXMLTemplate)
   {
    delete FDocXMLTemplate;
    FDocXMLTemplate = NULL;
   }
  delete FndHTML;
  FndHTML = NULL;
  delete __DopInfo;
  //�������� ��������� ������
  for (list<UnicodeString>::iterator i = FTmpFileNames.begin(); i != FTmpFileNames.end(); i++)
   DeleteFile((* i));
  UnicodeString OldTmpFileName = icsPrePath(GetTempPath() + "\\ics_doc_viewer.htm");
  //�������� �������� ����������� ���������� �����, ���������� ����������� ������ < 1.1.0
  if (FileExists(OldTmpFileName))
   DeleteFile(OldTmpFileName);
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

//������������� ���� FndHTML

TTagNode * __fastcall TdsDocViewCreator::InitNDHTML(bool bCreateBodyTag)
 {
  dsLogBegin(__FUNC__);
  delete FndHTML;
  FndHTML           = NULL;
  FndHTML           = new TTagNode;
  FndHTML->Encoding = "utf-8";
  if (bCreateBodyTag)
   {
    FndHTML->Name = "body";
    return (FUseWordCSS) ? FndHTML->AddChild("div", "class=Section1") : FndHTML;
   }
  dsLogEnd(__FUNC__);
  return NULL;
 }

//---------------------------------------------------------------------------

//����������� ���� � �������� ��� ������������ ���������� HTML-����� � ������ vmSaveToDisk �� ���������

UnicodeString __fastcall TdsDocViewCreator::GetTempPath() const
 {
  dsLogBegin(__FUNC__);
  wchar_t szTempPath[MAX_PATH];
  szTempPath[0] = '\0';
  ::GetTempPath(MAX_PATH, szTempPath);
  dsLogEnd(__FUNC__);
  return static_cast<UnicodeString>(szTempPath);
 }

// ***************************************************************************
// ********************** ��������������� ������ *****************************
// ***************************************************************************

void __fastcall TdsDocViewCreator::InternalInitPB()
 {
  //if (FPB)
  //{
  //FPB->Min = 0;
  //FPB->Position = 0;
  //}
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::InternalSetPBMax()
 {
  //if (FPB)
  //FPB->Max = __PBMax;
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::InternalIncPB()
 {
  //if (FPB)
  //FPB->Position = FPB->Position + 1;
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::InternalSetPBPosition()
 {
  //if (FPB)
  //FPB->Position = __PBPosition;
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::InitPB()
 {
  InternalInitPB();
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::SetPBMax(const int nMax)
 {
  __PBMax = nMax;
  InternalSetPBMax();
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::IncPB()
 {
  InternalIncPB();
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::SetPBPosition(const int nPosition)
 {
  __PBPosition = nPosition;
  InternalSetPBPosition();
 }

//---------------------------------------------------------------------------
void __fastcall TdsDocViewCreator::InternalGenerateOnChangeCreateStageEvent()
 {
  if (FOnChangeCreateStage)
   FOnChangeCreateStage(this, __CreateStage);
 }

//---------------------------------------------------------------------------
void __fastcall TdsDocViewCreator::GenerateOnChangeCreateStageEvent(TICSDocViewerCreateStage ACreateStage)
 {
  __CreateStage = ACreateStage;
  InternalGenerateOnChangeCreateStageEvent();
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::ChangeState(TICSDocViewerCreateStage AState, bool bGenerateOnChangeCreateStageEvent)
 {
  FState = AState;
  if (bGenerateOnChangeCreateStageEvent)
   GenerateOnChangeCreateStageEvent(AState);
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::SetTmpPath(UnicodeString AValue)
 {
  dsLogBegin(__FUNC__);
  if (FTmpPath != AValue)
   FTmpPath = AValue;
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::SetDefSavePath(UnicodeString AValue)
 {
  dsLogBegin(__FUNC__);
  if (FDefSavePath != AValue)
   FDefSavePath = AValue;
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::SetXMLContainer(const TAxeXMLContainer * AValue)
 {
  dsLogBegin(__FUNC__);
  if (FXMLContainer != AValue)
   FXMLContainer = const_cast<TAxeXMLContainer *>(AValue);
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::SetMode(const TICSDocViewerMode AValue)
 {
  dsLogBegin(__FUNC__);
  if (FMode != AValue)
   FMode = AValue;
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::SetUseCSS(const bool AValue)
 {
  dsLogBegin(__FUNC__);
  if (FUseCSS != AValue)
   FUseCSS = AValue;
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::SetUseWordCSS(const bool AValue)
 {
  if (FUseWordCSS != AValue)
   FUseWordCSS = AValue;
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::SetUseExcelCSS(const bool AValue)
 {
  if (FUseExcelCSS != AValue)
   FUseExcelCSS = AValue;
 }

//---------------------------------------------------------------------------
void __fastcall TdsDocViewCreator::SetOnChangeCreateStage(const TOnChangeCreateStageEvent AValue)
 {
  if (FOnChangeCreateStage != AValue)
   FOnChangeCreateStage = AValue;
 }

//---------------------------------------------------------------------------
void __fastcall TdsDocViewCreator::SetDocFormat(const TICSDocViewerDocFormat AValue)
 {
  if (FDocFormat != AValue)
   {
    if (AValue == dfMSWord)
     throw System::Sysutils::Exception(UnicodeString(__FUNC__) + "AValue :" + FMT(& _dsDocViewerErrorSetDocFormat));
    FDocFormat = AValue;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocViewCreator::SetSpecificator(const TTagNode * AValue)
 {
  //if (AValue)
  //CheckSpec(const_cast<TTagNode*>(AValue));

  if (FSpec)
   delete FSpec;
  FSpec = NULL;
  if (AValue)
   {
    FSpec = new TTagNode;
    FSpec->Assign(const_cast<TTagNode *>(AValue), true);
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocViewCreator::SetDocument(const TTagNode * AValue)
 {
  if (FDoc != AValue)
   {
    if (AValue && (AValue->Name != "docum"))
     throw System::Sysutils::Exception(UnicodeString(__FUNC__) + " AValue: " + FMT(& _dsDocErrorDocNotDefined)
         );
    if (FDocAssign)
     FDoc->Assign(const_cast<TTagNode *>(AValue), true);
    else
     FDoc = const_cast<TTagNode *>(AValue);
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocViewCreator::SetDocAssign(const bool AValue)
 {
  FDocAssign = AValue;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocViewCreator::SetListZeroValCheckCountOnly(const bool AValue)
 {
  FListZeroValCheckCountOnly = AValue;
 }
//---------------------------------------------------------------------------
TAxeXMLContainer * __fastcall TdsDocViewCreator::GetXMLContainer() const
 {
  if (!FXMLContainer)
   throw System::Sysutils::Exception(UnicodeString(__FUNC__) + ": " + FMT(& _dsDocErrorXMLContainerNotDefined));
  return FXMLContainer;
 }
//---------------------------------------------------------------------------
//����������� ���'� ����� ��� �������� ������� ndInsText ( xml-��� "instext" ������������� )
UnicodeString __fastcall TdsDocViewCreator::GetInsTextStyle(TTagNode * ndInsText)
 {
  dsLogBegin(__FUNC__);
  UnicodeString Result;
  if ((Result = ndInsText->AV["style"]) == "")
   if ((Result = ndInsText->GetParent("inscription")->AV["style"]) == "")
    Result = ndInsText->GetRoot()->GetChildByName("styles", true)->AV["defaultbstyle"];
  dsLogEnd(__FUNC__);
  return Result;
 }
//---------------------------------------------------------------------------
//����������� ������������ ��� �������� ������� ndInsText ( xml-��� "instext" ������������� )

UnicodeString __fastcall TdsDocViewCreator::GetInsTextAlign(TTagNode * ndInsText)
 {
  dsLogBegin(__FUNC__);
  UnicodeString StyleUID = GetInsTextStyle(ndInsText);
  TTagNode * ndStyles = ndInsText->GetRoot()->GetChildByName("styles", true);
  dsLogEnd(__FUNC__);
  return ndStyles->GetChildByAV("style", "uid", StyleUID)->AV["align"];
 }
//---------------------------------------------------------------------------
/*
 * ����������� ���'� ����� ������������� ��� ������ ������� ( xml-��� "table" ������������� ) ndCellTagNode
 *
 *
 * ���������:
 *   [in]  AStyleAttrName   - ��� ��������, ������������� ������������ ����� ( "hstyle" ��� "bstyle" )
 *   [in]  ndCellTagNode    - ������ ������� ( xml-��� "trow" ��� "tcol" ������������� )
 *   [in]  ndAltCellTagNode - �������������� ������ ������� ( xml-��� "trow" ��� "tcol" ������������� )
 *                            [default = NULL]
 *
 * ������������ ��������:
 *   ��� ����� ������������� ��� ������ ������� ( xml-��� "table" ������������� ) ndCellTagNode
 *
 */
UnicodeString __fastcall TdsDocViewCreator::GetTableCellStyle(UnicodeString AStyleAttrName, TTagNode * ndCellTagNode, TTagNode * ndAltCellTagNode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString Result;
  if ((Result = ndCellTagNode->AV[AStyleAttrName]) == "")
   if (!ndAltCellTagNode || (ndAltCellTagNode && ((Result = ndAltCellTagNode->AV[AStyleAttrName]) == "")))
    if ((Result = ndCellTagNode->GetParent("table")->AV[AStyleAttrName]) == "")
     Result = ndCellTagNode->GetRoot()->GetChildByName("styles", true)->AV[(AStyleAttrName == "hstyle")
        ? "defaulthstyle"
        : "defaultbstyle"
         ];
  dsLogEnd(__FUNC__);
  return Result;
 }
//---------------------------------------------------------------------------
/*
 * ����������� ���'� ����� ������������� ��� ������ ( xml-��� "list" ������������� ) ndList
 *
 *
 * ���������:
 *   [in]  AStyleAttrName - ��� ��������, ������������� ������������ ����� ( "hstyle" ��� "bstyle" )
 *   [in]  ndList         - ������ ( xml-��� "list" ������������� )
 *
 * ������������ ��������:
 *   ��� ����� ������������� ��� ������ ( xml-��� "list" ������������� ) ndList
 *
 */
UnicodeString __fastcall TdsDocViewCreator::GetListStyle(UnicodeString AStyleAttrName, TTagNode * ndList)
 {
  dsLogBegin(__FUNC__);
  UnicodeString Result;
  if ((Result = ndList->AV[AStyleAttrName]) == "")
   Result = ndList->GetRoot()->GetChildByName("styles", true)->AV[(AStyleAttrName == "hstyle")
      ? "defaulthstyle"
      : "defaultbstyle"
       ];
  dsLogEnd(__FUNC__);
  return Result;
 }
//---------------------------------------------------------------------------
/*
 * ����������� ���'� ����� ������������� ��� ������ ������ ( xml-��� "list" ������������� ) ndCellTagNode
 *
 *
 * ���������:
 *   [in]  AStyleAttrName   - ��� ��������, ������������� ������������ ����� ( "hstyle" ��� "bstyle" )
 *   [in]  ndCellTagNode    - ������ ������ ( xml-��� "list" ������������� )
 *
 * ������������ ��������:
 *   ��� ����� ������������� ��� ������ ������ ( xml-��� "list" ������������� ) ndCellTagNode
 *
 */
UnicodeString __fastcall TdsDocViewCreator::GetListCellStyle(UnicodeString AStyleAttrName, TTagNode * ndCellTagNode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString Result = "";
  if (dsDocSpec::IsListColSingle1(ndCellTagNode))
   Result = ndCellTagNode->AV[AStyleAttrName];
  else if (dsDocSpec::IsListColSingle2(ndCellTagNode))
   Result = ndCellTagNode->GetParent("listcol")->AV[AStyleAttrName];
  else if (dsDocSpec::IsListColGroup(ndCellTagNode))
   if (ndCellTagNode->CmpName("listcolgroup"))
    {
     if (AStyleAttrName == "hstyle")
      Result = ndCellTagNode->AV["style"];
    }
   else //listcol
        Result = ndCellTagNode->AV[AStyleAttrName];
  if (Result == "")
   Result = GetListStyle(AStyleAttrName, ndCellTagNode->GetParent("list"));
  dsLogEnd(__FUNC__);
  return Result;
 }
//---------------------------------------------------------------------------
//������������ �������������� �������� � ������ �������� ����������� ������� ��������
//������������ ������������, ��� 0.0 ����� ������������ ��� 0
void __fastcall TdsDocViewCreator::NormalizeNumericalValue(UnicodeString & ACellText, const bool fShowZVal)
 {
  dsLogBegin(__FUNC__);
  try
   {
    const double cdblEps = 1E-8;
    double dblValue = icsStrToFloat(ACellText);

    if (fabs(dblValue - 0.0) < cdblEps)
     ACellText = (fShowZVal) ? "0" : "";
    dsLogEnd(__FUNC__);
   }
  catch (EConvertError &)
   {;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocViewCreator::HiMetricToPixels(long * pnWidth, long * pnHeight)
 {
  dsLogBegin(__FUNC__);
  //��������� ��� ��������������
  const float nHiMetricPerInch = 25.4;

  HDC hDC = GetDC(0);

  //���������� ���������� �������� � ����������
  //����� �� ����������� � �� ���������
  int dpiX = GetDeviceCaps(hDC, LOGPIXELSX);
  int dpiY = GetDeviceCaps(hDC, LOGPIXELSY);

  //��������� ��������������
  if (pnWidth)
   * pnWidth = *pnWidth * dpiX / nHiMetricPerInch;

  if (pnHeight)
   * pnHeight = *pnHeight * dpiY / nHiMetricPerInch;

  ReleaseDC(0, hDC);
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------
//������������ ������ ��� ��������� ������� ������ ( xml-��� "list" ������������� )
UnicodeString __fastcall TdsDocViewCreator::GetListColHeaderText(TTagNode * ndListCol)
 {
  dsLogBegin(__FUNC__);
  UnicodeString HeaderText = "";
  if (dsDocSpec::IsListCol(ndListCol))
   {
    if (dsDocSpec::IsListColSingle2(ndListCol))
     {
      if (ndListCol->AV["ref"] != "")
       HeaderText = GetXMLContainer()->GetRefer(ndListCol->AV["ref"])->AV["name"];
     }
    else
     HeaderText = ndListCol->AV["name"];
    if ((HeaderText == "") && (FDocFormat == dfHTML))
     HeaderText = "&nbsp;";
   }
  dsLogEnd(__FUNC__);
  return HeaderText;
 }
//---------------------------------------------------------------------------
//������������ ������ ��� ��������� ������/������� ������� ( xml-��� "table" ������������� )
UnicodeString __fastcall TdsDocViewCreator::GetTableHeaderCellText(TTagNode * ndHeaderCell)
 {
  dsLogBegin(__FUNC__);
  UnicodeString HeaderText = ndHeaderCell->AV["name"];
  if ((HeaderText == "") && (FDocFormat == dfHTML))
   HeaderText = "&nbsp;";
  dsLogEnd(__FUNC__);
  return HeaderText;
 }
//---------------------------------------------------------------------------
/*
 * ������������ ������ ��� ������ � ������� ������ ������ ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in/out]  ndV       - ������� xml-��� "iv" ���������
 *   [in]      ndListCol - xml-��� ������������� ( "listcol", "listcolgroup" ��� "is_attr" ),
 *                         ��������������� �������� ������� ����������� ������
 *                         ( xml-��� "list" ������������� )
 *   [in]      bShowZVal - ������� "���������� ������� ��������"
 *
 * ������������ ��������:
 *   ����� ��� ������ � ������� ������/������� ������� ( xml-��� "table" ������������� )
 *
 */
UnicodeString __fastcall TdsDocViewCreator::GetDocListCellText(TTagNode *& ndV, const TTagNode * ndListCol, const bool bShowZVal)
 {
  dsLogBegin(__FUNC__);
  UnicodeString CellText = "";
  try
   {
    try
     {
      if (dsDocSpec::IsListColSingle1(ndListCol) || dsDocSpec::IsListColSingle2(ndListCol))
       CellText = ndV->AV["val"];
      else //������ �������� � ��������� "���������� �������"
       {
        int nChildColCount = (const_cast<TTagNode *>(ndListCol)->CmpName("listcolgroup"))
            //��� listcolgroup
            ? ndListCol->Count
            //��� listcol( link_is( is_attr+ ) )
            //: GetTagNodeChildrenCount( CC_TNode(ndListCol)->GetFirstChild(), "is_attr", false );
            : const_cast<TTagNode *>(ndListCol)->GetFirstChild()->GetCount(false, "is_attr");
        for (int i = 0; i < nChildColCount; i++)
         {
          if (CellText.Length())
           CellText += " ";
          CellText += ndV->AV["val"];
          if (i < nChildColCount - 1)
           ndV = ndV->GetNext();
         }
       }
      //������ ����������� ������� ��������
      if (!FListZeroValCheckCountOnly ||
          (FListZeroValCheckCountOnly &&
          const_cast<TTagNode *>(ndListCol)->CmpName("listcol") &&
          const_cast<TTagNode *>(ndListCol)->GetChildByName("link_is") &&
          const_cast<TTagNode *>(ndListCol)->GetChildByName("link_is")->GetChildByName("count"))
          )
       NormalizeNumericalValue(CellText, bShowZVal);
     }
    catch (Exception & E)
     {
      CellText = E.Message;
     }
   }
  __finally
   {
    dsLogEnd(__FUNC__);
   }
  return CellText;
 }
//---------------------------------------------------------------------------
/*
 * ����������� ������������� �������� ������ �����, ��������������� �������� ������
 * ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in]  ANode  - ������ xml-���������, � ������� �������������� ����� �������������
 *                  �������� ������
 *
 * ������������ ��������:
 *   ������������ �������� ������
 *
 */

__int64 __fastcall TdsDocViewCreator::GetListColsMaxLevel(TTagNode * ANode) const
 {
  dsLogBegin(__FUNC__);
  __int64 nLevel = 0;
  if (dsDocSpec::IsListColSingle(ANode))
   {
    int nNodeLevel = ANode->Level;
    if (dsDocSpec::IsListColSingle2(ANode))
     nNodeLevel-- ;
    if (nNodeLevel > nLevel)
     nLevel = nNodeLevel;
   }
  TTagNode * ndChild = const_cast<TTagNode *>(ANode)->GetFirstChild();
  while (ndChild)
   {
    long nChildNodesMaxLevel = GetListColsMaxLevel(ndChild);
    if (nChildNodesMaxLevel > nLevel)
     nLevel = nChildNodesMaxLevel;
    ndChild = ndChild->GetNext();
   }
  dsLogEnd(__FUNC__);
  return nLevel;
 }

//---------------------------------------------------------------------------

//����������� ������������� �������� ��� "����������" ��� ������������ �������������

int __fastcall TdsDocViewCreator::GetMaxPBForSpec()
 {
  TTagNode * ndDocContent = FSpec->GetChildByName("doccontent");
  int nPBMax = ndDocContent->Count;
  TTagNode * ndDocElement = ndDocContent->GetFirstChild();
  while (ndDocElement)
   {
    if (ndDocElement->CmpName("inscription"))
     nPBMax += ndDocElement->Count;
    else if (ndDocElement->CmpName("list"))
     nPBMax += ndDocElement->GetCount(dsDocSpec::IsListColSingle);
    else if (ndDocElement->CmpName("table"))
     {
      nPBMax += ndDocElement->GetChildByName("columns")->GetCount(true, "tcol");
      nPBMax += ndDocElement->GetChildByName("rows")->GetCount(true, "trow");
     }
    ndDocElement = ndDocElement->GetNext();
   }
  return nPBMax;
 }

//---------------------------------------------------------------------------

//����������� ������������� �������� ��� "����������" ��� ������������ ���������

int __fastcall TdsDocViewCreator::GetMaxPBForDoc()
 {
  int nPBMax = GetMaxPBForSpec();
  TTagNode * ndObj = FDoc->GetChildByName("contens")->GetFirstChild();
  while (ndObj)
   {
    TTagNode * ndObjRef = FSpec->GetTagByUID(ndObj->AV["uidmain"]);
    if (ndObjRef->CmpName("list,table"))
     nPBMax += ndObj->Count;
    ndObj = ndObj->GetNext();
   }
  return nPBMax;
 }

//---------------------------------------------------------------------------

//����������� ������������� �������� ��� "����������" ��� ������������ ���������
//�� ������� MS Word

int __fastcall TdsDocViewCreator::GetMaxPBForDocByMSWordTemplate()
 {
  TTagNode * ndDocContent = FSpec->GetChildByName("doccontent");
  int nPBMax = ndDocContent->Count;
  TTagNode * ndDocElement = ndDocContent->GetFirstChild();
  while (ndDocElement)
   {
    if (ndDocElement->CmpName("inscription"))
     nPBMax += ndDocElement->Count;
    ndDocElement = ndDocElement->GetNext();
   }
  TTagNode * ndObj = FDoc->GetChildByName("contens")->GetFirstChild();
  while (ndObj)
   {
    TTagNode * ndObjRef = FSpec->GetTagByUID(ndObj->AV["uidmain"]);
    if (ndObjRef->CmpName("list,table"))
     nPBMax += ndObj->Count;
    ndObj = ndObj->GetNext();
   }
  return nPBMax;
 }

//---------------------------------------------------------------------------

//������������ ����������� ���������/������������� (���������� �����)

void __fastcall TdsDocViewCreator::InternalPreviewCreate()
 {
  dsLogBegin(__FUNC__);
  //�������������
  ChangeState(csCreateInit);
  InitPB();
  //������������ �����������
  if (!FSpec)
   throw System::Sysutils::Exception(UnicodeString(__FUNC__) + " " + FMT(& _dsDocViewerErrorNoSpecDef));
  switch (FPreviewType)
   {
   case ptSpecificator:
     {
      if (FDocXMLTemplate)
       CreateSpecByMSWordTemplate();
      else
       switch (FDocFormat)
        {
        case dfHTML:
          {
           CreateSpecHTML();
           InitHTMLPreview();
           break;
          }
        case dfMSWord:
          {
           CreateSpecMSWord();
           InitMSWordPreview();
           break;
          }
        }
      break;
     }
   case ptDocument:
     {
      if (!FDoc)
       throw System::Sysutils::Exception(UnicodeString(__FUNC__) + " " + FMT(& _dsDocViewerErrorNoDocDef));
      if (FDoc->GetChildByName("passport")->AV["GUIspecificator"].UpperCase() !=
          FSpec->GetChildByName("passport")->AV["GUI"].UpperCase()
          )
       throw System::Sysutils::Exception(UnicodeString(__FUNC__) + " " + FMT(& _dsDocErrorGUINotValid));
      if (FDocXMLTemplate)
       CreateDocByMSWordTemplate();
      else
       switch (FDocFormat)
        {
        case dfHTML:
         CreateDocHTML();
         InitHTMLPreview();
         break;
        case dfMSWord:
         CreateDocMSWord();
         InitMSWordPreview();
         break;
        }
      break;
     }
   }
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

// ***************************************************************************
// ********************** ��������������� ������ *****************************
// ********************** ��� ������� HTML ***********************************
// ***************************************************************************

//���������� � htmlParent ��������� html-���� "table"
//( ������������ ��������� ���� __BorderWidth ��� �������� ������� ����� � __Width ��� �������� ������ )

TTagNode * __fastcall TdsDocViewCreator::AddHTMLTableTag(TTagNode * htmlParent)
 {
  dsLogBegin(__FUNC__);
  TTagNode * htmlTableTag = htmlParent->AddChild("table");
  SetHTMLBoxWidthValues(htmlTableTag, __BorderWidth, __Width);
  htmlTableTag->AV["bordercolor"] = "black";
  htmlTableTag->AV["cellspacing"] = "0";
  htmlTableTag->AV["cellpadding"] = "3";
  dsLogEnd(__FUNC__);
  return htmlTableTag;
 }

//---------------------------------------------------------------------------

//������������ ����� � �������� �������� ��� ��������� ������������ ��� html-���� "div"

void __fastcall TdsDocViewCreator::GetHTML_DIVAlignAttrVal(UnicodeString AAlign, UnicodeString & AAttrName, UnicodeString & AAttrVal)
 {
  dsLogBegin(__FUNC__);
  if (FUseCSS)
   {
    AAttrName = "style";
    AAttrVal  = "vertical-align:top; text-align : " + AAlign; /*change kab 20.06.07 "vertical-align:top;"*/
   }
  else
   {
    AAttrName = "align";
    AAttrVal  = AAlign;
   }
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

/*
 * ����������� ������������ ����� �������� ������ � ������� � �������� ������ �
 * ���������� ��������, � ������� ����������� ������ ������ ��� html-���� "font".
 * ������� ������������ ��������� �� ������ ������� html-������, �����������
 * ���������� MS Word v11 ( MS Office 2003 ).
 *
 *
 * ���������:
 *   [in]  nFontSize - ������ ������ � �������
 *
 * ������������ ��������:
 *   ������ ������ � ���������� ��������
 *
 */

int __fastcall TdsDocViewCreator::GetHTMLFontSizeMSOLike(const int nFontSize)
 {
  int RC = 0;
  dsLogBegin(__FUNC__);
  try
   {
    if (nFontSize <= 9)
     return 1;
    else if (nFontSize <= 11)
     return 2;
    else if (nFontSize == 12)
     return 3;
    else if (nFontSize <= 15)
     return 4;
    else if (nFontSize <= 20)
     return 5;
    else if (nFontSize <= 29)
     return 6;
    else
     return 7;
   }
  __finally
   {
    dsLogEnd(__FUNC__);
   }
  return RC;
 }

//---------------------------------------------------------------------------

/*
 * ������������ html-����� ��� �������������� ������ � ������������ � ���������
 * ������
 *
 *
 * ���������:
 *   [in]  htmlTag   - html-��� � ������� ( �������� <div>, <span>, <th>, <td> )
 *   [in]  AStyleUID - ��� ����� ������������� ��� ��������������
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������������� �����
 *
 *
 */

TTagNode * __fastcall TdsDocViewCreator::CreateHTMLTextFormat(TTagNode * htmlTag, UnicodeString AStyleUID)
 {
  dsLogBegin(__FUNC__);
  TTagNode * ndResult = htmlTag;
  TTagNode * ndStyles = FSpec->GetChildByName("styles", true);
  TTagNode * ndStyle = ndStyles->GetChildByAV("style", "uid", AStyleUID);
  if (FUseCSS)
   {
    htmlTag->AV["class"] = AStyleUID;
    if (ndStyle->CmpAV("wordwrap", "0"))
     if (htmlTag->CmpName("th,td"))
      htmlTag->AV["nowrap"] = "";
     else
      ndResult = htmlTag->AddChild("nobr");
   }
  else
   {
    if (htmlTag->CmpName("th,td"))
     htmlTag->AV["align"] = ndStyle->AV["align"];
    ndResult             = htmlTag->AddChild("font");
    ndResult->AV["face"] = ndStyle->AV["fontname"];
    ndResult->AV["size"] = IntToStr((int) GetHTMLFontSizeMSOLike(StrToInt(ndStyle->AV["fontsize"])));
    if (ndStyle->CmpAV("wordwrap", "0"))
     ndResult = ndResult->AddChild("nobr");
    if (ndStyle->CmpAV("strike", "1"))
     ndResult = ndResult->AddChild("strike");
    if (ndStyle->CmpAV("underline", "1"))
     ndResult = ndResult->AddChild("u");
    if (ndStyle->CmpAV("bold", "1"))
     ndResult = ndResult->AddChild("b");
    if (ndStyle->CmpAV("italic", "1"))
     ndResult = ndResult->AddChild("i");
   }
  dsLogEnd(__FUNC__);
  return ndResult;
 }

//---------------------------------------------------------------------------

/*
 * ��������� ������ ����� ��� ������ html-������� ( ������ ��� �������������� CSS )
 *
 *
 * ���������:
 *   [in]  htmlTableCell - html-���, ��������������� ������ html-�������
 *   [in]  ABorderWidth  - ������ �����
 *
 * ������������ ��������:
 *   true  - �������� ���������
 *   false - ��������� ����������� �� ����, ����� ���������� ������������� CSS
 *
 */

bool __fastcall TdsDocViewCreator::SetHTMLTableCellBorderWidth(TTagNode * htmlTableCell, UnicodeString ABorderWidth)
 {
  if (FUseCSS)
   {
    //htmlTableCell->AV["style"] = "border-width:" + ABorderWidth + "px";
    return true;
   }
  else
   return false;
 }

//---------------------------------------------------------------------------

bool __fastcall TdsDocViewCreator::SetHTMLBoxWidthValues(TTagNode * htmlTag, UnicodeString ABorderWidth, UnicodeString AWidth)
 {
  dsLogBegin(__FUNC__);
  UnicodeString AWidthHTMLVal = "";
  if (AWidth != "")
   {
    if (AWidth.Pos("%"))
     AWidthHTMLVal = AWidth;
    else
     {
      long nWidthPX = AWidth.ToInt();
      HiMetricToPixels(& nWidthPX, NULL);
      AWidthHTMLVal = IntToStr((int) nWidthPX) + "px";
     }
   }

  if (FUseCSS)
   {
    if (AWidthHTMLVal != "")
     htmlTag->AV["style"] = "width:" + AWidthHTMLVal;
   }
  else
   {
    if (AWidthHTMLVal != "")
     htmlTag->AV["width"] = AWidthHTMLVal;
    if (htmlTag->CmpName("table"))
     htmlTag->AV["border"] = ABorderWidth;
   }
  dsLogEnd(__FUNC__);
  return true;
 }

//---------------------------------------------------------------------------

/*
 * ������������ html-����� ��� �������������� ������ ������ html-�������,
 * ������������ ��� ��������� �����/��������
 *
 *
 * ���������:
 *   [in]  htmlNumTableCell - html-���, ��������������� ������ html-�������,
 *                            ������������ ��� ��������� �����/�������� ( html-��� "th" )
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������������� �����
 *
 *
 */

TTagNode * __fastcall TdsDocViewCreator::CreateHTMLTableNumCellTextFormat(TTagNode * htmlNumTableCell)
 {
  dsLogBegin(__FUNC__);
  TTagNode * ndResult = htmlNumTableCell;
  if (FUseCSS)
   htmlNumTableCell->AV["class"] = "num";
  else
   {
    htmlNumTableCell->AV["align"] = FNumTCellAlign;
    ndResult                      = htmlNumTableCell->AddChild("font");
    ndResult->AV["face"]          = FNumTCellFontName;
    ndResult->AV["size"]          = IntToStr((int) GetHTMLFontSizeMSOLike(StrToInt(FNumTCellFontSize)));
   }
  dsLogEnd(__FUNC__);
  return ndResult;
 }

//---------------------------------------------------------------------------

/*
 * ������������ �������� HTML-�����
 *
 *
 * ���������:
 *   [in]  AStyleClass          - ������������ ������ HTML-�����
 *   [in]  AStyleClassStrPrefix - ������� ����� ������������� ������ HTML-�����
 *   [in]  AStylePropStrPrefix  - ������� ����� ��������
 *   [in]  ndStyle              - xml-��� "style" �������������
 *
 * ������������ ��������:
 *   �������� HTML-����� � ���� ������
 *
 *
 */

UnicodeString __fastcall TdsDocViewCreator::GetHTMLStyleDefinition(UnicodeString AStyleClass,
    UnicodeString AStyleClassStrPrefix,
    UnicodeString AStylePropStrPrefix,
    TTagNode * ndStyle)
 {
  dsLogBegin(__FUNC__);
  UnicodeString STYLE = AStyleClassStrPrefix + "." + AStyleClass + "\n";
  STYLE += AStyleClassStrPrefix + "{\n";
  STYLE += AStylePropStrPrefix + "text-align     : " + ndStyle->AV["align"] + ";\n";
  STYLE += AStylePropStrPrefix + "vertical-align:top;\n"; /*change kab 20.06.07 ������������ ������������*/
  STYLE += AStylePropStrPrefix + "mso-number-format:\"\@\";"; /*change kab 20.06.07 ��� excel-� ��� ������ "�����"*/
  STYLE += AStylePropStrPrefix + "font-family    : \"" + ndStyle->AV["fontname"] + "\";\n";
  STYLE += AStylePropStrPrefix + "font-size      : " + ndStyle->AV["fontsize"] + "pt;\n";
  STYLE += AStylePropStrPrefix + "text-decoration:";
  if (ndStyle->CmpAV("strike", "1") ||
      ndStyle->CmpAV("underline", "1")
      )
   {
    if (ndStyle->CmpAV("strike", "1"))
     STYLE += " line-through";
    if (ndStyle->CmpAV("underline", "1"))
     STYLE += " underline";
   }
  else
   STYLE += " none";
  STYLE += ";\n";
  STYLE += AStylePropStrPrefix + "font-weight    :";
  STYLE += (ndStyle->CmpAV("bold", "1")) ? " bold" : " normal";
  STYLE += ";\n";
  STYLE += AStylePropStrPrefix + "font-style     :";
  STYLE += (ndStyle->CmpAV("italic", "1")) ? " italic" : " normal";
  STYLE += ";\n";
  STYLE += AStyleClassStrPrefix + "}\n";
  dsLogEnd(__FUNC__);
  return STYLE;
 }

//---------------------------------------------------------------------------

/*
 * ������������ ������� HTML-������
 *
 *
 * ���������:
 *   [in]  AStyleClassStrPrefix - ������� ����� ������������� ������ HTML-�����
 *   [in]  AStylePropStrPrefix  - ������� ����� ��������
 *   [in]  ndStyle              - xml-��� "styles" �������������
 *
 * ������������ ��������:
 *   �������� ������� HTML-������ � ���� ������
 *
 *
 */

UnicodeString __fastcall TdsDocViewCreator::GetHTMLSTYLESDefinition(UnicodeString AStyleClassStrPrefix,
    UnicodeString AStylePropStrPrefix,
    TTagNode * ndStyles)
 {
  dsLogBegin(__FUNC__);
  UnicodeString STYLES = "<!-- Document style sheet -->\n"
      "<style type='text/css'>\n"
      "<!--\n";
  TTagNode * ndStyle = ndStyles->GetFirstChild();
  while (ndStyle)
   {
    STYLES += GetHTMLStyleDefinition(ndStyle->AV["uid"], AStyleClassStrPrefix, AStylePropStrPrefix, ndStyle);
    ndStyle = ndStyle->GetNext();
   }
  STYLES += "-->\n</style>";
  dsLogEnd(__FUNC__);
  return STYLES;
 }

//---------------------------------------------------------------------------

/*
 * ������������ ������� HTML-������ ��� ������������� ���������� �������� ������������ HTML-��������� �
 * MS Word 2000/XP/2003
 *
 *
 * ���������:
 *   [in]  AStyleClassStrPrefix - ������� ����� ������������� ������ HTML-�����
 *   [in]  AStylePropStrPrefix  - ������� ����� ��������
 *   [in]  ndStyle              - xml-��� "pagesetup" �������������
 *
 * ������������ ��������:
 *   �������� ������� HTML-������ � ���� ������
 *
 *
 */

UnicodeString __fastcall TdsDocViewCreator::GetMSOWordHTMLSTYLESDefinition(
    UnicodeString AStyleClassStrPrefix,
    UnicodeString AStylePropStrPrefix,
    TTagNode * ndPageSetup
    )
 {
  dsLogBegin(__FUNC__);
  UnicodeString STYLES = "<!-- Style sheet for Microsoft Word v9 (MS Office 2000), v10 (MS Office XP ), v11 (MS Office 2003) -->\n"
      "<!--[if gte mso 9]>\n"
      "<style>\n";
  STYLES += AStyleClassStrPrefix + "@page Section1\n";
  STYLES += AStyleClassStrPrefix + "{\n";
  STYLES += AStylePropStrPrefix + "size                : ";
  UnicodeString MSOWidth, MSOHeight;
  if (ndPageSetup->CmpAV("orientation", "0"))//������� ����������
   {
    MSOWidth  = FloatToStrF(StrToFloat(ndPageSetup->AV["width"]) / 100, ffFixed, 7, 2);
    MSOHeight = FloatToStrF(StrToFloat(ndPageSetup->AV["height"]) / 100, ffFixed, 7, 2);
   }
  else //��������� ����������
   {
    MSOWidth  = FloatToStrF(StrToFloat(ndPageSetup->AV["height"]) / 100, ffFixed, 7, 2);
    MSOHeight = FloatToStrF(StrToFloat(ndPageSetup->AV["width"]) / 100, ffFixed, 7, 2);
   }
  STYLES += MSOWidth + "mm " + MSOHeight + "mm;\n";
  STYLES += AStylePropStrPrefix + "mso-page-orientation: ";
  STYLES += ndPageSetup->CmpAV("orientation", "0") ? "portrait" : "landscape";
  STYLES += ";\n";
  UnicodeString MSOMarginLeft, MSOMarginRight, MSOMarginTop, MSOMarginBottom;
  MSOMarginLeft   = FloatToStrF(StrToFloat(ndPageSetup->AV["leftfield"]) / 100, ffFixed, 7, 2);
  MSOMarginRight  = FloatToStrF(StrToFloat(ndPageSetup->AV["rightfield"]) / 100, ffFixed, 7, 2);
  MSOMarginTop    = FloatToStrF(StrToFloat(ndPageSetup->AV["topfield"]) / 100, ffFixed, 7, 2);
  MSOMarginBottom = FloatToStrF(StrToFloat(ndPageSetup->AV["bottomfield"]) / 100, ffFixed, 7, 2);
  STYLES += AStylePropStrPrefix + "margin              : " +
      MSOMarginTop + "mm " +
      MSOMarginRight + "mm " +
      MSOMarginBottom + "mm " +
      MSOMarginLeft + "mm;\n";
  STYLES += AStyleClassStrPrefix + "}\n";
  STYLES += AStyleClassStrPrefix + "div.Section1\n" +
      AStyleClassStrPrefix + "{\n" +
      AStylePropStrPrefix + "page: Section1;\n" +
      AStyleClassStrPrefix + "}\n";
  STYLES += "</style>\n"
      "<![endif]-->";
  dsLogEnd(__FUNC__);
  return STYLES;
 }

//---------------------------------------------------------------------------

/*
 * ������������ ������� HTML-������ ��� ������������� ���������� �������� ������������ HTML-��������� �
 * MS Excel 2000/XP/2003
 *
 *
 * ���������:
 *   [in]  AStyleClassStrPrefix - ������� ����� ������������� ������ HTML-�����
 *   [in]  AStylePropStrPrefix  - ������� ����� ��������
 *   [in]  ndStyle              - xml-��� "pagesetup" �������������
 *
 * ������������ ��������:
 *   �������� ������� HTML-������ � ���� ������
 *
 *
 */

UnicodeString __fastcall TdsDocViewCreator::GetMSOExcelHTMLSTYLESDefinition(
    UnicodeString AStyleClassStrPrefix,
    UnicodeString AStylePropStrPrefix,
    TTagNode * ndPageSetup
    )
 {
  dsLogBegin(__FUNC__);
  //Style sheet
  UnicodeString STYLES =
      "<!-- Style sheet for Microsoft Excel v9 (MS Office 2000), v10 (MS Office XP ), v11 (MS Office 2003) -->\n"
      "<!--[if gte mso 9]>\n"
      "<style>\n";
  STYLES += AStyleClassStrPrefix + "@page \n";
  STYLES += AStyleClassStrPrefix + "{\n";
  STYLES += AStylePropStrPrefix + "mso-page-orientation: ";
  STYLES += ndPageSetup->CmpAV("orientation", "0") ? "portrait" : "landscape";
  STYLES += ";\n";
  UnicodeString MSOMarginLeft, MSOMarginRight, MSOMarginTop, MSOMarginBottom;
  MSOMarginLeft   = FloatToStrF(StrToFloat(ndPageSetup->AV["leftfield"]) / 100 / 25.4, ffFixed, 7, 2);
  MSOMarginRight  = FloatToStrF(StrToFloat(ndPageSetup->AV["rightfield"]) / 100 / 25.4, ffFixed, 7, 2);
  MSOMarginTop    = FloatToStrF(StrToFloat(ndPageSetup->AV["topfield"]) / 100 / 25.4, ffFixed, 7, 2);
  MSOMarginBottom = FloatToStrF(StrToFloat(ndPageSetup->AV["bottomfield"]) / 100 / 25.4, ffFixed, 7, 2);
  STYLES += AStylePropStrPrefix + "margin              : " +
      MSOMarginTop + "in " +
      MSOMarginRight + "in " +
      MSOMarginBottom + "in " +
      MSOMarginLeft + "in;\n";
  STYLES += AStyleClassStrPrefix + "}\n";
  STYLES +=
      "</style>\n"
      "<![endif]-->\n";

  //Spread sheet
  STYLES +=
      "<!-- Spread sheet for Microsoft Excel v9 (MS Office 2000), v10 (MS Office XP ), v11 (MS Office 2003) -->\n"
      "<!--[if gte mso 9]>\n"
      "<xml>\n"
      "  <x:ExcelWorkbook>\n"
      "    <x:ExcelWorksheets>\n"
      "      <x:ExcelWorksheet>\n"
      "        <x:WorksheetOptions>\n"
      "          <x:Print>\n"
      "            <x:ValidPrinterInfo/>\n"
      "            <x:PaperSizeIndex>" + IntToStr((int)DMPAPER_A4) + "</x:PaperSizeIndex>\n"
      "          </x:Print>\n"
      "        </x:WorksheetOptions>\n"
      "      </x:ExcelWorksheet>\n"
      "    </x:ExcelWorksheets>\n"
      "  </x:ExcelWorkbook>\n"
      "</xml>\n"
      "<![endif]-->";
  dsLogEnd(__FUNC__);
  return STYLES;
 }

//---------------------------------------------------------------------------

/*
 * ������������ ������� HTML-������ ��� �������������� ������ HTML-���������
 *
 *
 * ���������:
 *   [in]  AStyleClassStrPrefix - ������� ����� ������������� ������ HTML-�����
 *   [in]  AStylePropStrPrefix  - ������� ����� ��������
 *
 * ������������ ��������:
 *   �������� ������� HTML-������ � ���� ������
 *
 *
 */

UnicodeString __fastcall TdsDocViewCreator::GetTableHTMLSTYLESDefinition(UnicodeString AStyleClassStrPrefix,
    UnicodeString AStylePropStrPrefix)
 {
  dsLogBegin(__FUNC__);
  UnicodeString STYLES = "<!-- Style sheet for tables -->\n"
      "<style type='text/css'>\n"
      "<!--\n";
  STYLES += AStyleClassStrPrefix + "table\n";
  STYLES += AStyleClassStrPrefix + "{\n";
  STYLES += AStylePropStrPrefix + "border-width  : 1px;\n";
  STYLES += AStylePropStrPrefix + "border-collapse: collapse;\n";
  STYLES += AStylePropStrPrefix + "border-style   : solid;\n";
  STYLES += AStylePropStrPrefix + "border-color   : black;\n";
  STYLES += AStyleClassStrPrefix + "}\n";
  STYLES += AStyleClassStrPrefix + "th, td\n";
  STYLES += AStyleClassStrPrefix + "{\n";
  STYLES += AStylePropStrPrefix + "border-width  : 1px;\n";
  STYLES += AStylePropStrPrefix + "border-collapse: collapse;\n";
  STYLES += AStylePropStrPrefix + "border-style   : solid;\n";
  STYLES += AStylePropStrPrefix + "border-color   : black;\n";
  STYLES += AStyleClassStrPrefix + "}\n";
  STYLES += AStyleClassStrPrefix + "th.num, td.num\n";
  STYLES += AStyleClassStrPrefix + "{\n";
  STYLES += AStylePropStrPrefix + "border-collapse: collapse;\n";
  STYLES += AStylePropStrPrefix + "border-style   : solid;\n";
  STYLES += AStylePropStrPrefix + "border-color   : black;\n";
  STYLES += AStylePropStrPrefix + "text-align     : " + FNumTCellAlign + ";\n";
  STYLES += AStylePropStrPrefix + "mso-number-format:\"\@\";"; /*change kab 20.06.07  ��� excel-� ��� ������ "�����"*/
  STYLES += AStylePropStrPrefix + "vertical-align:top;\n"; /*change kab 20.06.07 ������������ ������������*/
  STYLES += AStylePropStrPrefix + "font-family    : \"" + FNumTCellFontName + "\";\n";
  STYLES += AStylePropStrPrefix + "font-size      : " + IntToStr((int) FNumTCellFontSize) + "pt;\n";
  STYLES += AStylePropStrPrefix + "text-decoration: none;\n";
  STYLES += AStylePropStrPrefix + "font-weight    : normal;\n";
  STYLES += AStylePropStrPrefix + "font-style     : normal;\n";
  STYLES += AStyleClassStrPrefix + "}\n";
  STYLES += "-->\n</style>";
  dsLogEnd(__FUNC__);
  return STYLES;
 }

//---------------------------------------------------------------------------

//������������ HTML-����� � ���� ������

void __fastcall TdsDocViewCreator::CreateHTML(TICSDocViewerPreviewType APreviewType)
 {
  dsLogBegin(__FUNC__);
  ChangeState(csCreateHTML);
  UnicodeString TITLE = "";
  UnicodeString META = "<meta name='Generator' content='" + ClassName() + " V2.0'>\n"
      "<meta name='Description' content='";
  switch (APreviewType)
   {
   case ptSpecificator:
    TITLE = FSpec->GetChildByName("passport")->AV["mainname"];
    META += "specificator view";
    break;

   case ptDocument:
     {
      TTagNode * FPass = FDoc->GetChildByName("passport");
      TITLE = FPass->AV["mainname"] +
          " {" + GetDocPeriodValStr(FPass->AV["perioddef"], GetLPartB(FPass->AV["periodval"], '-'), GetRPartB(FPass->AV["periodval"], '-')) + "}";
      META += "document view";
      break;
     }
   }
  META += "'>";
  UnicodeString STYLES = "";
  if (FUseCSS || FUseWordCSS || FUseExcelCSS)
   META += "\n";
  if (FUseCSS)
   {
    STYLES +=
        GetHTMLSTYLESDefinition("\t", "\t  ", FSpec->GetChildByName("styles", true)) +
        "\n" +
        GetTableHTMLSTYLESDefinition("\t", "\t  ");
    if (FUseWordCSS || FUseExcelCSS)
     STYLES += "\n";
   }
  if (FUseWordCSS)
   {
    STYLES += GetMSOWordHTMLSTYLESDefinition("\t", "\t  ", FSpec->GetChildByName("pagesetup", true));
    if (FUseExcelCSS)
     STYLES += "\n";
   }
  if (FUseExcelCSS)
   STYLES += GetMSOExcelHTMLSTYLESDefinition("\t", "\t  ", FSpec->GetChildByName("pagesetup", true));
  try
   {
    FndHTML->OnProgressInit     = CreateHTMLProgressInit;
    FndHTML->OnProgress         = CreateHTMLProgressChange;
    FndHTML->OnProgressComplite = CreateHTMLProgressComplite;

    FHTML = FndHTML->AsHTML(TITLE, META + STYLES, false, 2, false);
    if (FUseExcelCSS)
     FHTML = StringReplace(
        FHTML,
        "<html>",
        "<html\n"
        "  xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n"
        "  xmlns:x=\"urn:schemas-microsoft-com:office:excel\"\n"
        "  xmlns=\"http://www.w3.org/TR/REC-html40\"\n"
        ">",
        TReplaceFlags() << rfIgnoreCase
         );
   }
  __finally
   {
    FndHTML->OnProgressInit     = NULL;
    FndHTML->OnProgress         = NULL;
    FndHTML->OnProgressComplite = NULL;
   }
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::CreateHTMLProgressInit(int AMax, UnicodeString AMsg)
 {
  InitPB();
  SetPBMax(AMax);
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::CreateHTMLProgressChange(int APercent, UnicodeString AMsg)
 {
  SetPBPosition(APercent);
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocViewCreator::CreateHTMLProgressComplite(UnicodeString AMsg)
 {
 }

//---------------------------------------------------------------------------

/*
 * ������������ ����� ��� �������� ������ ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in]      ndListEl - xml-��� "listcont" �������������
 *   [in\out]  htmlList - ������ ( html-��� "table", ��������������� xml-���� "list" ������������� )
 *   [int]     htmlRow  - ������� ������ html-������� ( html-��� "tr" )
 *                        ������������ ��� ������ ������� ����� ������������ ��������, ��� ������
 *                        ������� ������ ���� NULL
 *                        [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *
 */

void __fastcall TdsDocViewCreator::CreateHTMLListHeaderCell(TTagNode * ndListEl, TTagNode * htmlList, TTagNode * htmlRow)
 {
  dsLogBegin(__FUNC__);
  if (CheckIfTerminated())
   return;
  bool FCreateColumns = true;
  if (dsDocSpec::IsListCol(ndListEl))
   {
    bool attUnion = dsDocSpec::GetListColGroupUnion(ndListEl);

    //�������� ������-���������
    TTagNode * htmlTH = htmlRow->AddChild("th");
    //��������� ������ ����� � ������ �������
    if (dsDocSpec::IsListColSingle1(ndListEl) || dsDocSpec::IsListColSingle2(ndListEl))
     SetHTMLBoxWidthValues(htmlTH, __BorderWidth, dsDocSpec::GetSizeAttrVal(ndListEl, "width"));
    else if (attUnion)
     {
      int nWidth = 0;
      bool bNeedWidth = false;
      if (dsDocSpec::IsListColLinkISGroup(ndListEl))
       {
        UnicodeString AWidth = ndListEl->AV["width"];
        if (AWidth != "")
         {
          TStringList * sl = NULL;
          try
           {
            sl       = new TStringList;
            sl->Text = StringReplace(AWidth, ";", "\n", TReplaceFlags() << rfReplaceAll);
            for (int i = 0; i < sl->Count; i++)
             if (sl->Strings[i] != "")
              {
               nWidth += sl->Strings[i].ToInt();
               bNeedWidth = true;
              }
           }
          __finally
           {
            if (sl)
             delete sl;
           }
         }
       }
      else
       {
        TTagNode * ndLCol = ndListEl->GetFirstChild();
        while (ndLCol)
         {
          UnicodeString AWidth = ndLCol->AV["width"];
          if (AWidth != "")
           {
            nWidth += AWidth.ToInt();
            bNeedWidth = true;
           }
          ndLCol = ndLCol->GetNext();
         }
       }

      if (bNeedWidth)
       SetHTMLBoxWidthValues(htmlTH, __BorderWidth, IntToStr((int)nWidth));
      else
       SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
     }
    else
     SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
    TTagNode * htmlTHData = CreateHTMLTextFormat(htmlTH, GetListCellStyle("hstyle", ndListEl));
    TAttr * attPCDATA = htmlTHData->AddAttr("PCDATA", GetListColHeaderText(ndListEl));
    attPCDATA->Ref = ndListEl;

    //����������� "rowspan" ��� ��������� ������
    if (dsDocSpec::IsListColSingle(ndListEl))
     {
      int nListElLevel = ndListEl->Level;
      if (dsDocSpec::IsListColSingle2(ndListEl))
       nListElLevel-- ;
      int nListColMaxLevel = GetListColsMaxLevel(ndListEl->GetParent("listcont"));
      int nMaxDepth = nListColMaxLevel - nListElLevel + 1;
      if (nMaxDepth > 1)
       htmlTH->AV["rowspan"] = IntToStr((int) nMaxDepth);
      IncPB();
     }
    //����������� "colspan" ��� ��������� ������
    else if (!attUnion)
     htmlTH->AV["colspan"] = IntToStr(ndListEl->GetCount(dsDocSpec::IsListColSingle));
    if (attUnion)
     FCreateColumns = false;
   }
  if (FCreateColumns)
   {
    TTagNode * ndChild = ndListEl->GetFirstChild();
    while (ndChild)
     {
      //���������� htmlRow ��� ������ ������� ������ ��� ����� ������ �������
      if (dsDocSpec::IsListFirstCol(ndChild))
       {
        int nRowInd = ndChild->Level - ndChild->GetParent("listcont")->Level;
        if (dsDocSpec::IsListColSingle2(ndChild))
         nRowInd-- ;
        int i = 0;
        TTagNode * htmlTR = htmlList->GetFirstChild();
        while (htmlTR && (++i < nRowInd))
         htmlTR = htmlTR->GetNext();
        //����������� ����� ������ ��� ������������ ������������
        htmlRow = (htmlTR) ? htmlTR : htmlList->AddChild("tr");
       }
      CreateHTMLListHeaderCell(ndChild, htmlList, htmlRow);
      ndChild = ndChild->GetNext();
     }
   }
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

/*
 * ������������ ����� ��� �������� ������ ( xml-��� "list" ������������� )
 * � ����������� ������ "��������" ��������
 *
 *
 * ���������:
 *   [in]      ndListCont      - xml-��� "listcont" �������������
 *   [in\out]  htmlList        - ������ ( html-��� "table", ��������������� xml-���� "list" ������������� )
 *   [out]     AListSingleCols - ������ "��������" ��������
 *                               [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *     __RowNumbers
 *     __BorderWidth
 *
 */

void __fastcall TdsDocViewCreator::CreateHTMLListHeader(TTagNode * ndListCont, TTagNode * htmlList, TTagNodeList * AListSingleCols)
 {
  dsLogBegin(__FUNC__);
  if (CheckIfTerminated())
   return;
  CreateHTMLListHeaderCell(ndListCont, htmlList);
  //���������� ��������� ��� ������� � �������� ����� ������
  if (__RowNumbers)
   {
    TTagNode * htmlFirstRow = htmlList->GetFirstChild();
    TTagNode * htmlTH = htmlFirstRow->InsertChild(htmlFirstRow->GetFirstChild(), "th", true);
    SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
    TTagNode * htmlTHData = CreateHTMLTextFormat(htmlTH, GetListStyle("hstyle", ndListCont->GetParent("list")));
    htmlTHData->AV["PCDATA"] = FNumTCellLTitle;
    //����������� "rowspan" ��� ��������� ������
    int nMaxDepth = GetListColsMaxLevel(ndListCont) - ndListCont->Level;
    if (nMaxDepth > 1)
     htmlTH->AV["rowspan"] = IntToStr((int) nMaxDepth);
   }
  TTagNodeList ListSingleCols;
  if (AListSingleCols)
   {
    dsDocSpec::GetListSingleColsList(ndListCont, & ListSingleCols);
    *AListSingleCols = ListSingleCols;
   }
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

/*
 * ������������ ������ ��� ������ ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in]      AListSingleCols - ������ "��������" ��������
 *   [in\out]  htmlList        - ������ ( html-��� "table", ��������������� xml-���� "list" ������������� )
 *   [out]     ADataCells      - ������ html-����� "td" html-������� - ������ � �������
 *                               [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *
 */

void __fastcall TdsDocViewCreator::CreateHTMLListBodyRow(const TTagNodeList * AListSingleCols, TTagNode * htmlList, TTagNodeList * ADataCells)
 {
  dsLogBegin(__FUNC__);
  if (ADataCells)
   ADataCells->clear();
  TTagNode * htmlRow = htmlList->AddChild("tr");
  //���������� ������ ��� ������ ������ ������
  if (__RowNumbers)
   {
    TTagNode * htmlTD = htmlRow->AddChild("td");
    SetHTMLTableCellBorderWidth(htmlTD, __BorderWidth);
    TTagNode * htmlTDData = CreateHTMLTableNumCellTextFormat(htmlTD);
    TAttr * pcdata = htmlTDData->AddAttr("PCDATA", "&nbsp;");
    pcdata->Ref = NULL;
    if (ADataCells)
     ADataCells->push_back(htmlTDData);
   }
  for (TTagNodeList::const_iterator i = AListSingleCols->begin(); i != AListSingleCols->end(); i++)
   {
    if (CheckIfTerminated())
     return;
    TTagNode * htmlTD = htmlRow->AddChild("td");
    SetHTMLTableCellBorderWidth(htmlTD, __BorderWidth);
    TTagNode * htmlTDData = CreateHTMLTextFormat(htmlTD, GetListCellStyle("bstyle", *i));
    TAttr * pcdata = htmlTDData->AddAttr("PCDATA", "&nbsp;");
    pcdata->Ref = *i;
    if (ADataCells)
     ADataCells->push_back(htmlTDData);
   }
  IncPB();
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

/*
 * ������������ ������-��������� ��� ������� ( xml-��� "table" ������������� )
 * ( ������������� ����������� ��������� ����� html-�������, ���� ������� ���������� )
 *
 *
 * ���������:
 *   [in]  ACellTagNodeName - ��� �������������� xml-����� ( "tcol" ��� "trow" )
 *   [in]  ndCellTagNode    - xml-���, ��������������� ������-��������� �������
 *   [int] htmlRow          - ������� ������ html-������� ( html-��� "tr" )
 *
 * ������������ ��������:
 *   ������-��������� ( html-��� "th" ) �������
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *      __RowNum
 *
 */

TTagNode * __fastcall TdsDocViewCreator::CreateHTMLTableHeaderCell(UnicodeString ACellTagNodeName,
    TTagNode * ndCellTagNode,
    TTagNode * htmlRow)
 {
  dsLogBegin(__FUNC__);
  if (CheckIfTerminated())
   return NULL;
  TTagNode * htmlTH = NULL;
  if (ndCellTagNode->CmpName(ACellTagNodeName))
   {
    //���������� ������ ��������� �������/������
    htmlTH = htmlRow->AddChild("th");

    //��������� ������ ����� � ������ �������
    if ((ACellTagNodeName == "tcol") && !ndCellTagNode->GetChildByName("tcol"))
     SetHTMLBoxWidthValues(htmlTH, __BorderWidth, dsDocSpec::GetSizeAttrVal(ndCellTagNode, "width"));
    else
     SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
    TTagNode * htmlTHData = CreateHTMLTextFormat(htmlTH, GetTableCellStyle("hstyle", ndCellTagNode));
    TAttr * attPCDATA = htmlTHData->AddAttr("PCDATA", GetTableHeaderCellText(ndCellTagNode));
    attPCDATA->Ref = ndCellTagNode;
    //������������ ��� ���������� ����� ��� ����� ������� � ������� HTML
    ndCellTagNode->Ctrl = reinterpret_cast<TWinControl *>(htmlRow);
    //������ �������/�����
    if (ndCellTagNode->GetChildByName(ACellTagNodeName))
     {
      int nTableSingleElCount;
      UnicodeString SpanAttrName;
      if (ACellTagNodeName == "tcol")
       {
        nTableSingleElCount = ndCellTagNode->GetCount(dsDocSpec::IsTableColSingle);
        SpanAttrName        = "colspan";
        //nTableSingleElCount = 4;
       }
      else //trow
       {
        nTableSingleElCount = ndCellTagNode->GetCount(dsDocSpec::IsTableRowSingle);
        SpanAttrName        = "rowspan";
       }
      htmlTH->AV[SpanAttrName] = IntToStr((int) nTableSingleElCount);
     }
    //�������/������
    else
     {
      UnicodeString ElTopParentName, SpanAttrName;
      if (ACellTagNodeName == "tcol")
       {
        ElTopParentName = "columns";
        SpanAttrName    = "rowspan";
       }
      else
       {
        ElTopParentName = "rows";
        SpanAttrName    = "colspan";
       }
      int nTableElMaxLevel = dsDocSpec::GetTagNodesMaxLevel(ndCellTagNode->GetParent(ElTopParentName), ACellTagNodeName);
      int nMaxDepth = nTableElMaxLevel - ndCellTagNode->Level + 1;
      if (nMaxDepth > 1)
       htmlTH->AV[SpanAttrName] = IntToStr((int) nMaxDepth);
      //���������� ������ � ������� ������
      if ((ACellTagNodeName == "trow") && __RowNumbers)
       {
        TTagNode * htmlTH = htmlRow->AddChild("th");
        SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
        TTagNode * htmlTHData = CreateHTMLTableNumCellTextFormat(htmlTH);
        UnicodeString S;
        int nSingleRowCount = ndCellTagNode->GetParent("rows")->GetCount(dsDocSpec::IsTableRowSingle);
        S.printf(("%0" + IntToStr((int) IntToStr((int) nSingleRowCount).Length()) + "u").c_str(),
            __RowNum++
            );
        htmlTHData->AV["PCDATA"] = S;
       }
     }
    IncPB();
   }
  dsLogEnd(__FUNC__);
  return htmlTH;
 }

//---------------------------------------------------------------------------

/*
 * ������������ ����� ��� �������� ������� ( xml-��� "table" ������������� )
 *
 *
 * ���������:
 *   [in]      ndCol     - xml-��� "columns" �������������
 *   [in\out]  htmlTable - ������� ( html-��� "table", ��������������� xml-���� "table" ������������� )
 *   [int]     htmlRow   - ������� ������ html-������� ( html-��� "tr" )
 *                         ������������ ��� ������ ������� ����� ������������ ��������, ��� ������
 *                         ������� ������ ���� NULL
 *                         [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *      __ColNumbers
 *
 */

void __fastcall TdsDocViewCreator::CreateHTMLTableHeaderCols(TTagNode * ndCol, TTagNode * htmlTable, TTagNode * htmlRow)
 {
  dsLogBegin(__FUNC__);
  if (CheckIfTerminated())
   return;
  CreateHTMLTableHeaderCell("tcol", ndCol, htmlRow);
  TTagNode * ndChild = ndCol->GetChildByName("tcol");
  while (ndChild && ndChild->CmpName("tcol"))
   {
    //��� ������ ������� � ������ ( ��� ��� ����� ������ ������� )
    if (dsDocSpec::IsTableFirstCol(ndChild))
     {
      //��� ����� ������ �������
      if (ndChild->GetParent()->CmpName("columns"))
       {
        //���������� ����� ������ ��� HTML-�������
        htmlRow = htmlTable->AddChild("tr");
        //���������� ����� ����� ������� ������ ����� ������� ( ��� ������ �������� ������ )
        TTagNode * htmlTH = htmlRow->AddChild("th");
        SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
        TTagNode * ndTableSection = ndChild->GetParent("table")->GetChildByName("columns");
        int nTableElMaxLevel = dsDocSpec::GetTagNodesMaxLevel(ndTableSection, "tcol");
        htmlTH->AV["rowspan"] = nTableElMaxLevel - ndTableSection->Level +static_cast<int>(__ColNumbers);
        ndTableSection        = ndChild->GetParent("table")->GetChildByName("rows");
        nTableElMaxLevel      = dsDocSpec::GetTagNodesMaxLevel(ndTableSection, "trow");
        htmlTH->AV["colspan"] = nTableElMaxLevel - ndTableSection->Level +static_cast<int>(__RowNumbers);
        htmlTH->AV["PCDATA"]  = "&nbsp;";
       }
      //���������� htmlRow
      else
       {
        int nRowInd = ndChild->Level - ndChild->GetParent("columns")->Level;
        int i = 0;
        TTagNode * htmlTR = htmlTable->GetFirstChild();
        while (htmlTR && (++i < nRowInd))
         htmlTR = htmlTR->GetNext();
        htmlRow = (htmlTR) ? htmlTR : htmlTable->AddChild("tr");
       }
     }
    CreateHTMLTableHeaderCols(ndChild, htmlTable, htmlRow);
    ndChild = ndChild->GetNext();
   }
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

/*
 * ������������ ����� ��� �������� ������� ( xml-��� "table" ������������� ) � ���������� ��������
 *
 *
 * ���������:
 *   [in]      ndColumns         - xml-��� "columns" �������������
 *   [in\out]  htmlTable         - ������� ( html-��� "table", ��������������� xml-���� "table" ������������� )
 *   [out]     ATableSingleCols  - ������ "��������"-����� ����� ��� ��������
 *                                 ������� ( xml-��� "table" ������������� ), �.�. ����� �����,
 *                                 � ������� ��� ����������� ��� �������� �����-����������
 *                                 [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __ColNumbers
 *      __ColNum
 *
 */

void __fastcall TdsDocViewCreator::CreateHTMLTableCols(TTagNode * ndColumns, TTagNode * htmlTable, TTagNodeList * ATableSingleCols)
 {
  dsLogBegin(__FUNC__);
  if (CheckIfTerminated())
   return;
  CreateHTMLTableHeaderCols(ndColumns, htmlTable);
  TTagNodeList TableSingleCols;
  dsDocSpec::GetTableSingleColsList(ndColumns, & TableSingleCols);
  //���������� ����� � �������� ��������
  if (__ColNumbers)
   {
    TTagNode * htmlRow = htmlTable->AddChild("tr");
    for (TTagNodeList::const_iterator i = TableSingleCols.begin(); i != TableSingleCols.end(); i++)
     {
      TTagNode * htmlTH = htmlRow->AddChild("th");
      SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
      TTagNode * htmlTHData = CreateHTMLTableNumCellTextFormat(htmlTH);
      UnicodeString S;
      S.printf(("%0" + IntToStr(IntToStr((int) TableSingleCols.size()).Length()) + "u").c_str(),
          __ColNum++
          );
      htmlTHData->AV["PCDATA"] = S;
     }
   }
  if (ATableSingleCols)
   * ATableSingleCols = TableSingleCols;
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

/*
 * ������������ ����� ��� ����� ������� ( xml-��� "table" ������������� )
 * � ����� ������ � ���� �������
 *
 *
 * ���������:
 *   [in]     ndRow            - xml-��� "rows" �������������
 *   [in\out] htmlTable        - ������� ( html-��� "table", ��������������� xml-���� "table" ������������� )
 *   [in]     ATableSingleCols - ������ "��������"-����� ����� ��� ��������
 *                               ������� ( xml-��� "table" ������������� ), �.�. ����� �����,
 *                               � ������� ��� ����������� ��� �������� �����-����������
 *   [out]    DataCells        - ������ ������� ( ���� 2-������� ������� ) ���������� �� ������
 *                               ������ �������
 *   [int]    htmlRow          - ������� ������ html-������� ( html-��� "tr" )
 *                               ������������ ��� ������ ������� ����� ������������ ��������, ��� ������
 *                               ������� ������ ���� NULL
 *                               [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *
 */

void __fastcall TdsDocViewCreator::CreateHTMLTableRows(TTagNode * ndRow,
    TTagNode * htmlTable,
    const TTagNodeList & ATableSingleCols,
    TTagNodeList2 * DataCells,
    TTagNode * htmlRow)
 {
  dsLogBegin(__FUNC__);
  if (CheckIfTerminated())
   return;
  if (CreateHTMLTableHeaderCell("trow", ndRow, htmlRow) &&
      !ndRow->GetChildByName("trow")
      )
   {
    //���������� ����� ��� ������
    TTagNodeList DataCellStr;
    for (TTagNodeList::const_iterator i = ATableSingleCols.begin(); i != ATableSingleCols.end(); i++)
     {
      TTagNode * htmlTD = htmlRow->AddChild("td");
      SetHTMLTableCellBorderWidth(htmlTD, __BorderWidth);
      TTagNode * htmlTDData = CreateHTMLTextFormat(htmlTD, GetTableCellStyle("bstyle", *i, ndRow));
      htmlTDData->AV["PCDATA"] = "&nbsp;";
      if (DataCells)
       DataCellStr.push_back(htmlTDData);
     }
    if (DataCells)
     DataCells->push_back(DataCellStr);
   }
  TTagNode * ndChild = ndRow->GetChildByName("trow");
  while (ndChild && ndChild->CmpName("trow"))
   {
    //���������� htmlRow
    if (!htmlRow || !dsDocSpec::IsTableFirstRow(ndChild))
     htmlRow = htmlTable->AddChild("tr");
    else if (dsDocSpec::IsTableFirstRow(ndChild))
     htmlRow = reinterpret_cast<TTagNode *>(ndChild->GetParent()->Ctrl);
    CreateHTMLTableRows(ndChild, htmlTable, ATableSingleCols, DataCells, htmlRow);
    ndChild = ndChild->GetNext();
   }
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

/*
 * ������������ ����������� ������ ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in]  ndList     - xml-��� "list" �������������
 *   [in]  htmlParent - html-��� - ������ ��� ��������������� html-������
 *   [in]  nRowCount  - ���������� ����� � ����������� ������
 *                      [default = 1]
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������ ( xml-��� "list" ������������� )
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __Width
 *      __RowNumbers
 *
 */

TTagNode * __fastcall TdsDocViewCreator::AddHTMLList(TTagNode * ndList, TTagNode * htmlParent, TTagNodeList * AListSingleCols, int nRowCount)
 {
  dsLogBegin(__FUNC__);
  __BorderWidth = ndList->AV["border"];
  __Width       = ndList->AV["width"];
  __RowNumbers  = ndList->CmpAV("rownumbers", 1);
  TTagNode * htmlList = AddHTMLTableTag(htmlParent);
  TTagNode * ndListCont = ndList->GetChildByName("listcont");
  TTagNodeList ListSingleCols;
  CreateHTMLListHeader(ndListCont, htmlList, & ListSingleCols);
  for (int i = 0; i < nRowCount; i++)
   {
    if (CheckIfTerminated())
     break;
    CreateHTMLListBodyRow(& ListSingleCols, htmlList);
   }
  if (AListSingleCols)
   * AListSingleCols = ListSingleCols;
  dsLogEnd(__FUNC__);
  return htmlList;
 }

//---------------------------------------------------------------------------

/*
 * ������������ ����������� ������� ( xml-��� "table" ������������� )
 *
 *
 * ���������:
 *   [in]  ndTable    - xml-��� "table" �������������
 *   [in]  htmlParent - html-��� - ������ ��� �������������� html-�������
 *   [out] DataCells  - ������ ������� ( ���� 2-������� ������� ) ���������� �� ������
 *                      ������ �������
 *                      [default = NULL]
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������� ( xml-��� "table" ������������� )
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *      __ColNumbers
 *      __RowNum
 *      __ColNum
 *
 */

TTagNode * __fastcall TdsDocViewCreator::AddHTMLTable(TTagNode * ndTable, TTagNode * htmlParent, TTagNodeList2 * DataCells)
 {
  dsLogBegin(__FUNC__);
  __BorderWidth = ndTable->AV["border"];
  __Width       = ndTable->AV["width"];
  __RowNumbers  = ndTable->CmpAV("rownumbers", 1);
  __ColNumbers  = ndTable->CmpAV("colnumbers", 1);
  __RowNum      = 1;
  __ColNum      = 1;
  TTagNode * htmlTable = AddHTMLTableTag(htmlParent);
  TTagNodeList TableSingleCols;
  CreateHTMLTableCols(ndTable->GetChildByName("columns"), htmlTable, & TableSingleCols);
  if (DataCells)
   DataCells->clear();
  CreateHTMLTableRows(ndTable->GetChildByName("rows"), htmlTable, TableSingleCols, DataCells);
  dsLogEnd(__FUNC__);
  return htmlTable;
 }

//---------------------------------------------------------------------------

/*
 * ������������ ����������� ������� ( xml-��� "inscription" ������������� )
 *
 *
 * ���������:
 *   [in]  ndInscription - xml-��� "inscription" �������������
 *   [in]  htmlParent    - html-��� - ������ ��� ��������������� �����������
 *
 * ������������ ��������:
 *   html-���, ��������������� ����������� ������� ( xml-��� "inscription" ������������� )
 *
 */

TTagNode * __fastcall TdsDocViewCreator::AddHTMLInscription(TTagNode * ndInscription, TTagNode * htmlParent)
 {
  dsLogBegin(__FUNC__);
  if (ndInscription->CmpAV("newline", "1"))
   htmlParent->AddChild("br");
  dsLogEnd(__FUNC__);
  return htmlParent->AddChild("div");
 }

//---------------------------------------------------------------------------

/*
 * ������������ ����������� �������� ������� ( xml-��� "instext" ������������� )
 *
 *
 * ���������:
 *   [in]     ndInsText  - xml-��� "instext" �������������
 *   [in\out] htmlParent - html-��� - ������ ��� ��������������� �����������
 *
 * ������������ ��������:
 *   html-���, ��������������� ����������� �������� ������� ( xml-��� "instext" ������������� )
 *
 */

TTagNode * __fastcall TdsDocViewCreator::AddHTMLInsText(TTagNode * ndInsText, TTagNode ** htmlParent)
 {
  dsLogBegin(__FUNC__);
  if (ndInsText->CmpAV("newline", "1"))
   * htmlParent = (*htmlParent)->AddChild("div");

  UnicodeString AlignAttrName, AlignAttrVal;
  GetHTML_DIVAlignAttrVal(GetInsTextAlign(ndInsText), AlignAttrName, AlignAttrVal);
  (*htmlParent)->AV[AlignAttrName] = AlignAttrVal;

  TTagNode * htmlInsText = (*htmlParent)->AddChild("span");
  dsLogEnd(__FUNC__);
  return CreateHTMLTextFormat(htmlInsText, GetInsTextStyle(ndInsText));
 }

//---------------------------------------------------------------------------

//������������ ����������� ������������� � ������� HTML

void __fastcall TdsDocViewCreator::CreateSpecHTML()
 {
  dsLogBegin(__FUNC__);
  ChangeState(csCreateHTMLTags);
  SetPBMax(GetMaxPBForSpec());
  TTagNode * htmlBodyRoot = InitNDHTML();
  TTagNode * ndDocElement = FSpec->GetChildByName("doccontent")->GetFirstChild();
  while (ndDocElement)
   {
    if (CheckIfTerminated())
     return;
    if (ndDocElement->CmpName("inscription"))//�������
     {
      TTagNode * htmlInsTextCurParent = AddHTMLInscription(ndDocElement, htmlBodyRoot);
      TTagNode * ndInsText = ndDocElement->GetFirstChild();
      while (ndInsText)//������� �������
       {
        if (CheckIfTerminated())
         return;
        TTagNode * htmlInsText = AddHTMLInsText(ndInsText, &htmlInsTextCurParent);
        UnicodeString InsViewText = dsDocSpec::GetInsTagViewText(ndInsText, GetXMLContainer());
        if (InsViewText != csEmptyInsText)
         htmlInsText->AV["PCDATA"] = InsViewText;
        ndInsText = ndInsText->GetNext();
        IncPB();
       }
     }
    else if (ndDocElement->CmpName("list"))//������
         AddHTMLList(ndDocElement, htmlBodyRoot);
    else if (ndDocElement->CmpName("table"))//�������
         AddHTMLTable(ndDocElement, htmlBodyRoot);
    htmlBodyRoot->AddChild("br");
    ndDocElement = ndDocElement->GetNext();
    IncPB();
   }
  CreateHTML(ptSpecificator);
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

//������������ ����������� ��������� � ������� HTML

void __fastcall TdsDocViewCreator::CreateDocHTML()
 {
  dsLogBegin(__FUNC__);
  ChangeState(csCreateHTMLTags);
  SetPBMax(GetMaxPBForDoc());
  TTagNode * htmlBodyRoot = InitNDHTML();
  TTagNode * ndObj = FDoc->GetChildByName("contens")->GetFirstChild();
  TTagNode * ndErrList = FDoc->GetChildByName("passport")->GetFirstChild();
  if (ndErrList)
   {
     TTagNode *itErr = ndErrList->GetFirstChild();
     while(itErr)
     {
       htmlBodyRoot->AddChild("hr");
       htmlBodyRoot->AddChild("b")->AV["PCDATA"] = itErr->AV["PCDATA"];
       htmlBodyRoot->AddChild("br");
       htmlBodyRoot->AddChild("hr");
       itErr = itErr->GetNext();
     }
   }

  while (ndObj)
   {
    if (CheckIfTerminated())
     return;
    //��������� ����� "lv" ������� ������ - ��������� ���������
    TTagNode * ndObjRef = FSpec->GetTagByUID(ndObj->AV["uidmain"]);
    if (ndObjRef->CmpName("inscription"))//�������
     {
      TTagNode * htmlInsTextCurParent = AddHTMLInscription(ndObjRef, htmlBodyRoot);
      TTagNode * ndInsElement = ndObj->GetFirstChild();
      while (ndInsElement)
       {
        if (CheckIfTerminated())
         return;
        //��������� ����� "lv" ������� ������ - ��������� �������
        TTagNode * ndInsElementRef = FSpec->GetTagByUID(ndInsElement->AV["uidmain"]);
        TTagNode * htmlInsText = AddHTMLInsText(ndInsElementRef, &htmlInsTextCurParent);
        UnicodeString InsViewText = ndInsElementRef->AV["value"];
        UnicodeString RefText = ndInsElement->GetFirstChild()->AV["val"];
        UnicodeString CntClcRlsText = ndInsElement->GetFirstChild()->GetNext()->AV["val"];
        if (RefText != "")
         {
          if (InsViewText != "")
           InsViewText += " ";
          InsViewText += RefText;
         }
        if (CntClcRlsText != "")
         {
          if (InsViewText != "")
           InsViewText += " ";
          InsViewText += CntClcRlsText;
         }
        if (InsViewText != "")
         htmlInsText->AV["PCDATA"] = InsViewText;
        ndInsElement = ndInsElement->GetNext();
        IncPB();
       }
     }
    else if (ndObjRef->CmpName("list"))//������
     {
      TTagNodeList ListSingleCols;
      TTagNode * htmlList = AddHTMLList(ndObjRef, htmlBodyRoot, &ListSingleCols, 0);
      TTagNode * ndListRow = ndObj->GetFirstChild();
      int nRowNo = 1;
      while (ndListRow)
       {
        if (CheckIfTerminated())
         return;
        //��������� ����� "lv" ������� ������ - ����� ������
        TTagNodeList DataCells;
        CreateHTMLListBodyRow(& ListSingleCols, htmlList, & DataCells);
        TTagNodeList::iterator itListCell = DataCells.begin();
        //���������� ������ ������� ������
        if (__RowNumbers)
         {
          UnicodeString RowNo = IntToStr((int) nRowNo);
          RowNo = UnicodeString::StringOfChar('0', IntToStr((int) ndObj->Count).Length() - RowNo.Length()) + RowNo;
          (*itListCell)->AV["PCDATA"] = RowNo;
          itListCell++ ;
         }
        TTagNode * ndListData = ndListRow->GetFirstChild();
        while (ndListData)
         {
          if (CheckIfTerminated())
           return;
          //��������� ����� "lv", "iv" �������� ������ - ����� ������� ������ ������
          if (ndListData->CmpName("iv"))//������� �������� �������� ��� ������� ����������
           {                            //��� ��������� ��������
            TTagNode * ndListCol = reinterpret_cast<TTagNode *>((*itListCell)->GetAttrByName("PCDATA")->Ref);
            UnicodeString CellText = GetDocListCellText(ndListData, ndListCol, ndObjRef->CmpAV("showzval", "1"));
            if (CellText != "")
             (*itListCell)->AV["PCDATA"] = CellText;
           }
          else //�������� ��������� ��������
           {
            try
             {
              TTagNodeList::iterator itListCellInit = itListCell;
              TTagNode * ndListLinkISRows = ndListData->GetFirstChild();
              if (ndListLinkISRows)
               {
                while (ndListLinkISRows)
                 {
                  if (CheckIfTerminated())
                   return;
                  //��������� ����� "lv" ���������� ������ - ������ ��������� ��������� ��������
                  itListCell = itListCellInit;
                  TTagNode * ndListLinkISData = ndListLinkISRows->GetFirstChild();
                  while (ndListLinkISData)
                   {
                    if (CheckIfTerminated())
                     return;
                    //��������� ����� "iv" ������ ������ - ������ ����� ��������� ��������� ��������
                    TAttr * attPCDATA = (*itListCell)->GetAttrByName("PCDATA");
                    TTagNode * ndListCol = reinterpret_cast<TTagNode *>(attPCDATA->Ref);
                    UnicodeString CellText = GetDocListCellText(ndListLinkISData, ndListCol, ndObjRef->CmpAV("showzval", "1"));
                    //if ( CellText != "" )
                    //{ /*BUGFIX kab 20.06.07 - ���������� ������ �������� � ������*/
                    attPCDATA->Value = "";
                    if ((* itListCell)->Count)
                     (* itListCell)->AddChild("br");
                    (*itListCell)->AddChild("span")->AV["PCDATA"] = CellText;
                    //}
                    itListCell++ ;
                    ndListLinkISData = ndListLinkISData->GetNext();
                   }
                  ndListLinkISRows = ndListLinkISRows->GetNext();
                 }
               }
              else
               {
                TTagNode * ndListLinkISRef = FSpec->GetTagByUID(ndListData->AV["uidmain"]);
                if (ndListLinkISRef)
                 {
                  if (CheckIfTerminated())
                   return;
                  //��������� ����� "lv" ���������� ������ - ������ ��������� ��������� ��������
                  itListCell = itListCellInit;
                  TTagNode * ndListISAttrRef = ndListLinkISRef->GetFirstChild();
                  if (ndListISAttrRef)
                   ndListISAttrRef = ndListISAttrRef->GetFirstChild();
                  while (ndListISAttrRef)
                   {
                    if (CheckIfTerminated())
                     return;
                    if (ndListISAttrRef->CmpName("is_attr"))
                     {
                      //��������� ����� "iv" ������ ������ - ������ ����� ��������� ��������� ��������
                      TAttr * attPCDATA = (*itListCell)->GetAttrByName("PCDATA");
                      attPCDATA->Value = "";
                      (* itListCell)->AddChild("span", "PCDATA=");
                      itListCell++ ;
                     }
                    ndListISAttrRef = ndListISAttrRef->GetNext();
                   }
                 }
               }
              if (itListCell != itListCellInit)
               itListCell-- ;
             }
            catch (Exception & E)
             {
              dsLogError(E.Message, __FUNC__);
             }
           }
          itListCell++ ;
          ndListData = ndListData->GetNext();
         }
        ndListRow = ndListRow->GetNext();
        nRowNo++ ;
       }
     }
    else if (ndObjRef->CmpName("table"))//�������
     {
      TTagNodeList2 DataCells;
      AddHTMLTable(ndObjRef, htmlBodyRoot, & DataCells);
      TTagNode * ndTableRow = ndObj->GetFirstChild();
      TTagNodeList2::iterator itDataCells = DataCells.begin();
      while (ndTableRow)
       {
        if (CheckIfTerminated())
         return;
        //��������� ����� "lv" ������� ������ - ����� �������
        TTagNode * ndTableData = ndTableRow->GetFirstChild();
        TTagNodeList::iterator itDataCellStr = (*itDataCells).begin();
        while (ndTableData)
         {
          if (CheckIfTerminated())
           return;
          //��������� ����� "iv" - ����� ������ �������
          UnicodeString CellText = ndTableData->AV["val"];
          //������ ����������� ������� ��������
          NormalizeNumericalValue(CellText, ndObjRef->CmpAV("showzval", "1"));
          if (CellText != "")
           (*itDataCellStr)->AV["PCDATA"] = CellText;
          ndTableData = ndTableData->GetNext();
          itDataCellStr++ ;
         }
        ndTableRow = ndTableRow->GetNext();
        itDataCells++ ;
        IncPB();
       }
     }
    htmlBodyRoot->AddChild("br");
    ndObj = ndObj->GetNext();
    IncPB();
   }
  CreateHTML(ptDocument);
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

//������������� ����� �������� ������� ���������������� ��������� ��� ��������������� ��������� � ������� HTML

void __fastcall TdsDocViewCreator::InitHTMLPreview()
 {
  dsLogBegin(__FUNC__);
  //TfmHTMLPreview *dlg;
  WideString URL;
  bool bOnFly;
  __CanShowPreviewDlg = false;
  switch (FMode)
   {
   case vmOnFly:
    bOnFly = true;
    URL = FHTML;
    break;

   case vmSaveToDisk:
    bOnFly = false;
    wchar_t szTmpFileName[MAX_PATH];
    UnicodeString HTMLTmpFileName = icsPrePath(FTmpPath + "\\idv" + TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss") + ".htm");
    FTmpFileNames.push_back(HTMLTmpFileName);
    TFileStream * fs = NULL;
    try
     {
      fs = new TFileStream(HTMLTmpFileName, fmCreate | fmShareExclusive);
      fs->Seek(0, soFromBeginning);
      AnsiStringT<1251>tmpFLR = AnsiStringT<1251>(FHTML);
      fs->Write(tmpFLR.c_str(), tmpFLR.Length());
     }
    __finally
     {
      if (fs)
       delete fs;
     }
    URL = HTMLTmpFileName;
    break;
   }
  __CanShowPreviewDlg = true;
  __URL   = URL;
  __OnFly = bOnFly;
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

// ***************************************************************************
// ********************** ��������������� ������ *****************************
// ********************** ��� ������� MSWord *********************************
// ***************************************************************************

//������������ ����������� ��������� � ������� MSWord

void __fastcall TdsDocViewCreator::CreateDocMSWord()
 {
  throw System::Sysutils::Exception(UnicodeString(__FUNC__) + " " + FMT(& _dsDocViewerErrorCreateMSWord));
 }

//---------------------------------------------------------------------------
void __fastcall TdsDocViewCreator::FSetDocXMLTemplate(TTagNode * ATmpl)
 {
  dsLogBegin(__FUNC__);
  if (ATmpl)
   {
    if (!FDocXMLTemplate)
     FDocXMLTemplate = new TTagNode(NULL);
    FDocXMLTemplate->Assign(ATmpl, true);
   }
  else
   {
    if (FDocXMLTemplate)
     {
      delete FDocXMLTemplate;
      FDocXMLTemplate = NULL;
     }
   }
  dsLogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------

//������������ ����������� ������������� � ������� MS Word

void __fastcall TdsDocViewCreator::CreateSpecMSWord()
 {
  throw System::Sysutils::Exception(UnicodeString(__FUNC__) + " " + FMT(& _dsDocViewerErrorCreateMSWord));
 }

//---------------------------------------------------------------------------

//������������� ����� �������� ������� ���������������� ��������� ��� ��������������� ��������� � ������� MS Word

void __fastcall TdsDocViewCreator::InitMSWordPreview()
 {
  throw System::Sysutils::Exception(UnicodeString(__FUNC__) + " " + FMT(& _dsDocViewerErrorCreateMSWord));
 }

//---------------------------------------------------------------------------

// ***************************************************************************
// ********************** ��������������� ������ *****************************
// ********************** ��� ������������ ������ ****************************
// ********************** �� ������� MSWord **********************************
// ***************************************************************************

bool __fastcall TdsDocViewCreator::CreateMSWordDocByTemplate()
 {
  /*
   wchar_t szTmpFileName[MAX_PATH];
   UnicodeString MSWordTmpFileName = AppendFileName(
   FTmpPath,
   "idv" + TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss")+".doc"
   );
   FTmpFileNames.push_back(MSWordTmpFileName);
   TFileStream *fs = NULL;
   try
   {
   fs = new TFileStream( MSWordTmpFileName, fmCreate | fmShareExclusive );
   fs->Seek( 0, soFromBeginning );
   fs->CopyFrom( FDocXMLTemplate, FDocXMLTemplate->Size);
   }
   __finally
   {
   if ( fs ) delete fs;
   }
   __DocName = MSWordTmpFileName;
   */
  return true;
 }

//---------------------------------------------------------------------------

//������������ ����������� ������������� �� ������� MSWord

void __fastcall TdsDocViewCreator::CreateSpecByMSWordTemplate()
 {
  dsLogBegin(__FUNC__);
  __CanShowPreviewDlg = false; //��� ����
  __CanShowPreviewDlg = CreateMSWordDocByTemplate();
  dsLogEnd(__FUNC__);
 }

//---------------------------------------------------------------------------

//������������ ����������� ��������� �� ������� MSWord

void __fastcall TdsDocViewCreator::CreateDocByMSWordTemplate()
 {
  /*__CanShowPreviewDlg = false; //��� ����
   #pragma option push -w-pia
   if ( __CanShowPreviewDlg = CreateMSWordDocByTemplate() )
   #pragma option pop
   {
   TXMLDoc* WordDoc = NULL;
   TXMLTab* CurTab  = NULL;
   int nCurTabInd = 0;
   try
   {
   WordDoc = new TXMLDoc();
   CoInitialize(NULL);       //������-�� ����� ����� ���� ???
   WordDoc->Load(FDocXMLTemplate);

   ChangeState( csFillMSWordTemplate );
   SetPBMax(GetMaxPBForDocByMSWordTemplate());
   TTagNode* ndObj = FDoc->GetChildByName( "contens" )->GetFirstChild();
   while (ndObj)
   {
   if (CheckIfTerminated()) return;
   //��������� ����� "lv" ������� ������ - ��������� ���������
   TTagNode *ndObjRef = FSpec->GetTagByUID(ndObj->AV["uidmain"]);
   //��������� ������ �� ������� �� ������� ��� ����������
   CurTab = WordDoc->GetTab( ++nCurTabInd );
   //##########################################################################
   //#                              �������                                   #
   //##########################################################################
   if ( ndObjRef->CmpName("inscription") )
   {
   TTagNode *ndInsElement = ndObj->GetFirstChild();
   int nRowInd = 1;
   int nColInd = 0;
   //��� ������� �������� ������� ���� ������
   while (ndInsElement)
   {
   if (CheckIfTerminated()) return;
   //��������� ����� "lv" ������� ������ - ��������� �������
   nColInd++;
   TTagNode *ndInsElementRef = FSpec->GetTagByUID(ndInsElement->AV["uidmain"]);
   if ( ndInsElementRef->GetPrev() && ndInsElementRef->CmpAV("newline","1") )
   {
   nRowInd++;
   nColInd = 1;
   }
   //����������� �����
   UnicodeString InsViewText = ndInsElementRef->AV["value"];
   //����� �� ������
   UnicodeString RefText = ndInsElement->GetFirstChild()->AV["val"];
   //����� �� �������� ���-�� ��� ��������� ����������
   UnicodeString CntClcRlsText = ndInsElement->GetFirstChild()->GetNext()->AV["val"];
   if ( RefText != "" )
   {
   if ( InsViewText != "" )
   InsViewText += " ";
   InsViewText += RefText;
   }
   if ( CntClcRlsText != "" )
   {
   if ( InsViewText != "" )
   InsViewText += " ";
   InsViewText += CntClcRlsText;
   }
   if ( InsViewText != "" )
   CurTab->SetText( nRowInd, nColInd, InsViewText );
   ndInsElement = ndInsElement->GetNext();
   IncPB();
   }
   }
   //##########################################################################
   //#                              ������                                    #
   //##########################################################################
   else if ( ndObjRef->CmpName("list") )
   {
   TTagNodeList ListSingleCols;
   dsDocSpec::GetListSingleColsList( ndObjRef, &ListSingleCols );
   TTagNodeList::iterator itListSingleCols;
   //������ ������ ������
   int nRowInd = GetListColsMaxLevel( ndObjRef ) - ndObjRef->Level;
   //������ ������� ������
   int nColInd;
   int nRowNo = 1;
   TTagNode *ndListRow = ndObj->GetFirstChild();
   CurTab->AppendRow( ndObj->Count - 1 );
   while (ndListRow)
   {
   if ( CheckIfTerminated() ) return;
   //��������� ����� "lv" ������� ������ - ����� ������
   nColInd = 1;
   itListSingleCols = ListSingleCols.begin();
   //���������� ������ ������� ������
   if ( ndObjRef->CmpAV( "rownumbers", 1 ) )
   {
   UnicodeString RowNo = IntToStr((int) nRowNo );
   RowNo = UnicodeString::StringOfChar( '0', IntToStr((int) ndObj->Count ).Length() - RowNo.Length() ) + RowNo;
   CurTab->SetText( nRowInd, nColInd, RowNo );
   nColInd++;
   }
   TTagNode *ndListData = ndListRow->GetFirstChild();
   while (ndListData)
   {
   if (CheckIfTerminated()) return;
   //��������� ����� "lv", "iv" �������� ������ - ����� ������� ������ ������
   if (ndListData->CmpName("iv"))   //������� �������� �������� ��� ������� ����������
   {                         //��� ��������� ��������
   UnicodeString CellText = GetDocListCellText( ndListData, *itListSingleCols, ndObjRef->CmpAV( "showzval", "1" ) );
   CurTab->SetText( nRowInd, nColInd, CellText );
   nColInd++;
   }
   else  //�������� ��������� ��������
   {
   TTagNodeList::iterator itListSingleColsInit = itListSingleCols;
   int nSubColInd = nColInd;
   TTagNode *ndListLinkISRows = ndListData->GetFirstChild();
   while (ndListLinkISRows)
   {
   if (CheckIfTerminated()) return;
   //��������� ����� "lv" ���������� ������ - ������ ��������� ��������� ��������
   itListSingleCols = itListSingleColsInit;
   nSubColInd = nColInd;
   TTagNode *ndListLinkISData = ndListLinkISRows->GetFirstChild();
   while (ndListLinkISData)
   {
   if (CheckIfTerminated()) return;
   //��������� ����� "iv" ������ ������ - ������ ����� ��������� ��������� ��������
   UnicodeString CellText = GetDocListCellText( ndListLinkISData, *itListSingleCols, ndObjRef->CmpAV( "showzval", "1" ) );
   if ( ndListLinkISRows->GetPrev() )
   CurTab->SetText( nRowInd, nSubColInd, "\n" );
   CurTab->SetText( nRowInd, nSubColInd, CellText );
   itListSingleCols++;
   nSubColInd++;
   ndListLinkISData = ndListLinkISData->GetNext();
   }
   ndListLinkISRows = ndListLinkISRows->GetNext();
   }
   nColInd = nSubColInd;
   if ( itListSingleCols != itListSingleColsInit )
   itListSingleCols--;
   }
   itListSingleCols++;
   ndListData = ndListData->GetNext();
   }
   ndListRow = ndListRow->GetNext();
   nRowNo++;
   nRowInd++;
   IncPB();
   }
   }
   //##########################################################################
   //#                              �������                                   #
   //##########################################################################
   else if ( ndObjRef->CmpName("table") )
   {
   TTagNodeList TableSingleRows;
   TICSDocSpec::GetTableSingleRowsList( ndObjRef->GetChildByName("rows"), &TableSingleRows );
   TTagNodeList::iterator itTableSingleRows = TableSingleRows.begin();
   int nRowInd;  //������ ������ ������
   int nColInd;  //������ ������� ������
   nRowInd = dsDocSpec::GetTagNodesMaxLevel( ndObjRef, "tcol" ) - ndObjRef->Level;
   if ( ndObjRef->CmpAV( "colnumbers", "1" ) )
   nRowInd++;
   TTagNode *ndTableRow = ndObj->GetFirstChild();
   while (ndTableRow)
   {
   if (CheckIfTerminated()) return;
   //��������� ����� "lv" ������� ������ - ����� �������
   TTagNode *ndTableData = ndTableRow->GetFirstChild();
   nColInd = (*itTableSingleRows)->Level - ndObjRef->Level;
   if ( ndObjRef->CmpAV( "rownumbers", "1" ) )
   nColInd++;
   while(ndTableData)
   {
   if (CheckIfTerminated()) return;
   //��������� ����� "iv" - ����� ������ �������
   UnicodeString CellText = ndTableData->AV["val"];
   //������ ����������� ������� ��������
   NormalizeNumericalValue(CellText, ndObjRef->CmpAV("showzval", "1"));
   CurTab->SetText( nRowInd, nColInd, CellText );
   nColInd++;
   ndTableData = ndTableData->GetNext();
   }
   ndTableRow = ndTableRow->GetNext();
   itTableSingleRows++;
   nRowInd++;
   IncPB();
   }
   }
   ndObj = ndObj->GetNext();
   IncPB();
   }
   //      WordDoc->Save();
   //      WordDoc->Close();
   FDocXMLTemplate  = WordDoc->AsTagNode();
   }
   __finally
   {
   if (WordDoc) delete WordDoc;
   }
   }
   */ //!!!
 }

//---------------------------------------------------------------------------
// ***************************************************************************
// ********************** �������� ������ ************************************
// ***************************************************************************

UnicodeString __fastcall TdsDocViewCreator::CreatePreview(TICSDocViewerPreviewType APreviewType, TAxeXMLContainer * AXMLList, TTagNode * ASpec, TTagNode * ADoc)
 {
  dsLogBegin(__FUNC__);
  Mode         = vmOnFly;
  XMLContainer = AXMLList;
  Specificator = ASpec;
  Document     = ADoc;
  UnicodeString RC = "error";
  try
   {
    if (APreviewType == ptUnknown)
     throw System::Sysutils::Exception(UnicodeString(__FUNC__) + " APreviewType: " + FMT(& _dsDocViewerErrorPreviewTypeVal));
    //�������������
    FPreviewType = APreviewType;

    InternalPreviewCreate();
    ChangeState(csNone, false);
    RC = __URL;
   }
  __finally
   {
    XMLContainer = NULL;
    Specificator = NULL;
    Document     = NULL;
   }
  dsLogEnd(__FUNC__);
  return RC;
 }

//---------------------------------------------------------------------------
bool __fastcall TdsDocViewCreator::CheckIfTerminated()
 {
  return false;
 }
//---------------------------------------------------------------------------
