unit dsDocSrvConst;

interface

resourcestring
// Common
  dsDocCaption = '������';
  dsDocErrorTagEq = '�������� ������ ��������������� ���� "%s".';
  dsDocErrorXMLContainerNotDefined = '�� ������ xml-���������.';
  dsDocErrorDocNotDefined = '��������� ������� ��������.';
  dsDocErrorGUINotValid = '��� ������������� ���������� ��������� �� ��������� � ��� ���������� �������������';
  dsDocErrorCommonDop = '��� ������ - %s,'+#13#10+'��������� - "%s".';
  dsDocErrorCreateOwnerNotValid   = '�������� ������ ���� �������� TdsDocSpec.';
  dsDocErrorInputErrorCaption  = '������ �����';
  dsDocErrorReqFieldCaption  = '���� "%s" ������ ���� ���������.';
  dsDocExExtFuncErrorCreateDocView  = '������ ������������ ��������� (����������� �������� ���������)';
  dsDocDocSpecListCopyCaption = '�����';

//SQLCreator
  dsDocSQLCreatorErrorCondStructure  = '������ � ��������� ������� ������';
  dsDocSQLCreatorErrorFuncMissing  = '����� ������� "%s"; ��������� ���������: %s';

// BaseEdit
  dsDocBaseEditCommonTBCaption = '�����';
  dsDocBaseEditEditTBCaption = '������';
  dsDocBaseEditErrorSetDomainNum = '����� ������ ������ ���������� � %s.';
  dsDocBaseEditErrorGetNom = '�� ������ ����������� (GUI = %s).';
  dsDocBaseEditErrorGetDomain = '����� � ������� "%s" � ������������ �� ������.';
  dsDocBaseEditErrorNoXML = '����������� xml.';
  dsDocBaseEditErrorGenUID = '��������� uid ����������. �� ����������� �� ���� �� ��������� �������: ParentXML, XML.';

// Globals
  dsDocGlobals = '';

//Filter
  dsDocFilterCaption = '�������� �������';
  dsDocFilterFilterTBCaption = '������';

  dsDocFilterPassportCaption       = '&���������';
  dsDocFilterPassportHint          = '���������';
  dsDocFilterCustomLoadSaveCaption = '&��������� / ���������';
  dsDocFilterCustomLoadSaveHint    = '��������� / ���������';
  dsDocFilterCustomLoadCaption     = '���&������';
  dsDocFilterCustomLoadHint        = '���������';
  dsDocFilterORCaption             = '�&��';
  dsDocFilterORHint                = '�������� ������ ������� ������������� �� {���}';
  dsDocFilterANDCaption            = '&�';
  dsDocFilterANDHint               = '�������� ������ ������� ������������� �� {�}';
  dsDocFilterLogicNOTCaption       = '���&/�';
  dsDocFilterLogicNOTHint          = '������� ��� ������ �������';
  dsDocFilterNOTCaption            = '&��';
  dsDocFilterNOTHint               = '��������� �������� �������';
  dsDocFilterAddCondCaption        = '&�������� �������';
  dsDocFilterAddCondHint           = '�������� �������';
  dsDocFilterDeleteCaption         = '&�������';
  dsDocFilterDeleteHint            = '�������';
  dsDocFilterKonkrCaption          = '&���������� �������� ...';
  dsDocFilterKonkrHint             = '���������� �������� ...';
  dsDocFilterDeKonkrCaption        = '������� � �&� ������������';
  dsDocFilterDeKonkrHint           = '������� � �� ������������';
  dsDocFilterCommentCaption        = '&�����������';
  dsDocFilterCommentHint           = '�����������';
  dsDocFilterCheckCaption          = '���&������';
  dsDocFilterCheckHint             = '���������';

  dsDocFilterXMLTitle = '������';
  dsDocFilterErrorSetIsFilter = '������ �������� ������� ������� ��� ���������� �������.';
  dsDocFilterErrorLoadNomDLL = '������ �������� "nom_dll.dll". ��� ������ - %s, ��������� - "%s".';
  dsDocFilterErrorFreeNomDLL = '������ �������� "nom_dll.dll". ��� ������ - %s, ��������� - "%s".';
  dsDocFilterErrorCallProc = '%s - ������ ������ "%s" �� "nom_dll.dll".';
  dsDocFilterErrorCaption = '������';
  dsDocFilterConformCaption = '���������������';
  dsDocFilterCheckOkCaption = '������ ���������';
  dsDocFilterCheckEmptyCaption = '������ ������';
  dsDocFilterCheckNoBodyCaption = '������ �����������';

  dsDocFilterNewCaption = '����� ������';
  dsDocFilterNewAuthorCaption = '����������';
  dsDocFilterErrorEmptyGroup = '������� ������ ����� ������� �����������.';
  dsDocFilterErrorEmptyValue = 'dsDocFilterErrorEmptyGroup';
  dsDocFilterErrorIS = '�������� �������������� �������� � UID''��  "%s" �� �������.';
  dsDocFilterErrorISDomain = '�������� �������������� �������� � UID''�� "%s" �� ����������� ������ � ������� "%s".';

//  FilterEdit
  dsDocFilterEditCaption = '������������ �������';
  dsDocFilterEditOkBtnCaption     = '���������';
  dsDocFilterEditCancelBtnCaption = '������';
  dsDocFilterEditHelpBtnCaption   = '&������';

// FilterUtils
  dsDocFilterUtilsFilterVersion = '������ ������� "%s" �� ��������������. ��������� ������ "%s" ��� ����.';

// Options
  dsDocOptions = '';

// Parsers
  dsDocParsersLexTypes01 = '���';
  dsDocParsersLexTypes02 = '��������� ���������';
  dsDocParsersLexTypes03 = '���������� ����� ���������';
  dsDocParsersLexTypes04 = '���������� ������������ ���������';
  dsDocParsersLexTypes05 = '����� ������ �������';
  dsDocParsersLexTypes06 = '����� ������';
  dsDocParsersLexTypes07 = '�������� ��������';
  dsDocParsersLexTypes08 = '�������� ���������';
  dsDocParsersLexTypes09 = '�������� ���������';
  dsDocParsersLexTypes10 = '�������� �������';
  dsDocParsersLexTypes11 = '��������� �����';
  dsDocParsersLexTypes12 = '��������� �� �����';
  dsDocParsersLexTypes13 = '��������� ������';
  dsDocParsersLexTypes14 = '��������� ������';
  dsDocParsersLexTypes15 = '��������� ������ ��� �����';
  dsDocParsersLexTypes16 = '��������� ������ ��� �����';
  dsDocParsersLexTypes17 = '����������� ������';
  dsDocParsersLexTypes18 = '����������� ������';
  dsDocParsersLexTypes19 = '�������� ��';
  dsDocParsersLexTypes20 = '�������� �';
  dsDocParsersLexTypes21 = '�������� ���';

  dsDocParsersLexClasses1 = '��� ������������ ����';
  dsDocParsersLexClasses2 = '��� ��� ��������� ��������� ����, ����� ������/������� ��� ����� ������';
  dsDocParsersLexClasses3 = '��� ��� ��������� ���������� ����';
  dsDocParsersLexClasses4 = '������� ��������';
  dsDocParsersLexClasses5 = '����������������� ��������';
  dsDocParsersLexClasses6 = '���������� ��������';
  dsDocParsersLexClasses7 = '�������� ���������';
  dsDocParsersLexClasses8 = '����������� ������';
  dsDocParsersLexClasses9 = '����������� ������';

  dsDocParsersLexErrors1 = '�������� ������.';
  dsDocParsersLexErrors2 = '������ ���������.';
  dsDocParsersLexErrors3 = '�������� ������.';
  dsDocParsersLexErrors4 = '����������� ����� �������.';
  dsDocParsersLexErrors5 = '��������� ������-�����������.';
  dsDocParsersLexErrors6 = '��������� ������-�����.';
  dsDocParsersLexErrors7 = '��������� ������-����� ��� ������-�����������.';
  dsDocParsersLexErrors8 = '��������� ������-�����, ������-����������� ��� ������ ''.''.';
  dsDocParsersLexErrors9 = '��������� ������-�����, ������-����������� ��� ������ '':''.';

  dsDocParsersSyntErrors01 = '�������� ������.';
  dsDocParsersSyntErrors02 = '��������� ������.';
  dsDocParsersSyntErrors03 = '������ ���������.';
  dsDocParsersSyntErrors04 = '��������� �� �������� ��������.';
  dsDocParsersSyntErrors05 = '�������� �� ������ � ������ ���������.';
  dsDocParsersSyntErrors06 = '��� �������� �� ������� ��������.';
  dsDocParsersSyntErrors07 = '��� �������� ������ ���� ������ ���� � ������ ���� �������.';
  dsDocParsersSyntErrors08 = '��� �������� ������ ���� ������� ��� ��������.';
  dsDocParsersSyntErrors09 = '��� ���������� �������� ������ ���� ������� ���������� ��������.';
  dsDocParsersSyntErrors10 = '��� �������������� �������� ������ ���� ������� �������������� ��������.';
  dsDocParsersSyntErrors11 = '��� �������� ��������� � �������� ��������� ����� ��������� ���, ���������, �������������� ��������� ��� ����� ������ �������.';
  dsDocParsersSyntErrors12 = '������� ���������� ������ ���� ������� � ���� ��������������� ��������� ��� ��������������� ��������.';
  dsDocParsersSyntErrors13 = '������� �������� ������ ���� ������� � ���� ����������� ���������.';

  dsDocParsersSemErrors01 = '�������� ������.';
  dsDocParsersSemErrors02 = '�������� ��������� ������� ������.';
  dsDocParsersSemErrors03 = '����� ������� �����.';
  dsDocParsersSemErrors04 = '��������� ���������� � "1".';
  dsDocParsersSemErrors05 = '����� ������ ������� �����.';
  dsDocParsersSemErrors06 = '��������� ����� ���������� � "1".';
  dsDocParsersSemErrors07 = '����� ������� ������� �����.';
  dsDocParsersSemErrors08 = '��������� �������� ���������� � "1".';
  dsDocParsersSemErrors09 = '������� �� ����.';
  dsDocParsersSemErrors10 = '��� �������������� �������� ������ ���� ������� �������������� ��������.';
  dsDocParsersSemErrors11 = '��� �������� ��������� � �������� ��������� ����� ��������� ���, ���������, �������������� ��������� ��� ����� ������ �������.';

  dsDocParsersErrorPos = ' ������� %s.';

  dsDocParsersErrorExprLex = '����������� ����������� ����������.';
  dsDocParsersErrorExprSynt = '����������� �������������� ����������.';
  dsDocParsersErrorExprSem = '����������� ������������� ����������.';

  dsDocParsersOk = '�������� ������.';

  dsDocParsersLexError = '����������� ������: ';
  dsDocParsersSyntError = '�������������� ������: ';
  dsDocParsersSemError = '������������� ������: ';

  dsDocParsersNotConst = '��';
  dsDocParsersAndConst = '�';
  dsDocParsersOrConst = '���';
  dsDocParsersErrorPolandStr = '�� ����������� �������� PolandStr.';

// Spec
  dsDocSpecEmptyInsCaption = '� � � � � � �  � � � � � � �  -  � � � � � � � �  � �  � � � � � � � � � � �';
  dsDocSpecGetCountCaption = '������� ���������� ��� "%s"';
{*  dsDocSpecCaption = '�������� �������� ���������';
  dsDocSpecTabPrtyColCaption = '������';
  dsDocSpecTabPrtyRowCaption = '�������';
  dsDocSpecEmptyColCaption = '� � � � � �  � � � � � � �';
  dsDocSpecCountViewText = ' (������� ����������)';
  dsDocSpecEmptyEllipsiseEditor = '(�� �����������)';
  dsDocSpecNotEmptyEllipsiseEditor = '(�����������)';
  dsDocSpecInscViewText = '�������';
  dsDocSpecListViewText = '������ -> ������ �������� �� "%s"';
  dsDocSpecTableViewText = '������� -> ������ �������� �� "%s"';

  dsDocSpecMainMenuCaption = '����';
  dsDocSpecPropPanelCaption = '��������';

  dsDocSpecParametersCaption = '&���������';
  dsDocSpecParametersHint = '���������';
  dsDocSpecParametersShortCut = '';

  dsDocSpecPreViewCaption = '��������������� ����&����';
  dsDocSpecPreViewHint = '��������������� ��������';
  dsDocSpecPreViewShortCut = '';

  dsDocSpecCutCaption = '&��������';
  dsDocSpecCutHint = '��������';
  dsDocSpecCutShortCut = '';

  dsDocSpecCopyCaption = '&����������';
  dsDocSpecCopyHint = '����������';
  dsDocSpecCopyShortCut = '';

  dsDocSpecPasteCaption = '���&�����';
  dsDocSpecPasteHint = '��������';
  dsDocSpecPasteShortCut = '';

  dsDocSpecDeleteCaption = '&�������';
  dsDocSpecDeleteHint = '�������';
  dsDocSpecDeleteShortCut = 'Del';

  dsDocSpecAddInscCaption = '&�������';
  dsDocSpecAddInscHint = '�������';
  dsDocSpecAddInscShortCut = 'Ctrl+I';

  dsDocSpecAddInsTextCaption = '&������� �������';
  dsDocSpecAddInsTextHint = '������� �������';
  dsDocSpecAddInsTextShortCut = 'Ctrl+Alt+I';

  dsDocSpecAddListCaption = '&������';
  dsDocSpecAddListHint = '������';
  dsDocSpecAddListShortCut = 'Ctrl+L';

  dsDocSpecAddLColCaption = '�����&�� ������';
  dsDocSpecAddLColHint = '������� ������';
  dsDocSpecAddLColShortCut = 'Ctrl+Alt+L';

  dsDocSpecAddTableCaption = '&�������';
  dsDocSpecAddTableHint = '�������';
  dsDocSpecAddTableShortCut = 'Ctrl+T';

  dsDocSpecAddTColCaption = '��&����� �������';
  dsDocSpecAddTColHint = '������� �������';
  dsDocSpecAddTColShortCut = 'Ctrl+Alt+T';

  dsDocSpecAddTRowCaption = '��&���� �������';
  dsDocSpecAddTRowHint = '������ �������';
  dsDocSpecAddTRowShortCut = 'Shift+Ctrl+T';

  dsDocSpecMoveUpCaption = '����������� ����&�';
  dsDocSpecMoveUpHint = '����������� �����';
  dsDocSpecMoveUpShortCut = 'Ctrl+Up';

  dsDocSpecMoveDownCaption = '����������� ���&�';
  dsDocSpecMoveDownHint = '����������� ����';
  dsDocSpecMoveDownShortCut = 'Ctrl+Down';

  dsDocSpecCommonItemCaption = '�&����';
  dsDocSpecEditItemCaption = '&������';
  dsDocSpecObjectItemCaption = '&������';

  dsDocSpecXMLTitle = '�������� ���������';
  dsDocSpecErrorCreateXML = '�� ������ xml.';

  dsDocSpecLineName0Col  = '���������';
  dsDocSpecLineName00Col = '����';
  dsDocSpecLineName1Col  = '�������';
  dsDocSpecLineName2Col  = '�������';
  dsDocSpecLineName0Row  = '���������';
  dsDocSpecLineName00Row = '��';
  dsDocSpecLineName1Row  = '������';
  dsDocSpecLineName2Row  = '������';

  dsDocSpecCanDelColRowDopInfo = '�� %s �%s ��������� ��������� %s';
  dsDocSpecErrorCanDelColRow = '%s %s ������ �������, �.�. �� %s ���� ������.';
  dsDocSpecMsgCanDeleteDocRulesRefObj = '�� ��������� ������� � �������� ������������ ��������� �� ��������� ���� ������. ����������?';
  dsDocSpecGetXMLNodeViewTextRow = '������';
  dsDocSpecGetXMLNodeViewTextCol = '�������';

  dsDocSpecGetSizeViewTextAuto = '����';
  dsDocSpecGetSizeViewTextFullWidth = '�� ���� ������';
  dsDocSpecGetSizeViewTextFullHeight = '�� ���� ������';
  dsDocSpecErrorApplyStyle = '���������� ������� ����� �� ������.';
  dsDocSpecErrorApplyUnion = '��� ����� �������� ���������� ������ ������ �������� ������ ������� ������� "���������� �������".';
  dsDocSpecErrorApplyText = '���������� ����������� ������� ��������.';
  dsDocSpecErrorApplyInt = '���������� ������� ����� ������������� ��������.';
  dsDocSpecCalcCountRefConf = '�������� "������� ����������" � "������" �����������������. �������� �������� "������� ����������" ����� ��������. ����������?';
  dsDocSpecSetRefDlgCaption = '� ������� ���������';
  dsDocSpecSetRefDlTreegCaption = '������� ��� �����������';
  dsDocSpecSetRefRefObjCaption = '������� ��� ������';
  dsDocSpecSetRefDocParamCaption = '��������� ���������';
  dsDocSpecSetRefTechInfoCaption = '��������������� ��������';
  dsDocSpecErrorSetRefDopInfo = '������� ������� "%s" ��������� �� "%s"';
  dsDocSpecErrorSetRefMsg = '������� �� ����� ���� ������, �.�. �� ���� ������� ������.';
  dsDocSpecErrorOperation = '������� ������ ��������� �� ������������ ������ ��������.';
  dsDocSpecAddTableRowNum = '������ %s';
  dsDocSpecAddTableColNum = '������� %s';
  dsDocSpecAddTableNewCol = '����� �������';
  dsDocSpecAddTableNewRow = '����� ������';
  dsDocSpecWidthCaption = '������';
  dsDocSpecRulesCaption = '������� ������';
  dsDocSpecCalcRulesCaption = '������� ����������';
  dsDocSpecCheckRulesCaption = '������� ��������';
  dsDocSpecDataStyleCaption = '����� ������';
  dsDocSpecHeaderStyleCaption = '����� ���������';
  dsDocSpecDefaultHeaderStyleCaption = '����� ��������� �� ���������';
  dsDocSpecDefaultDataStyleCaption = '����� ������ �� ���������';
  dsDocSpecHeaderCaption = '���������';
  dsDocSpecStyleCaption = '�����';
  dsDocSpecDefaultStyleCaption = '����� �� ���������';
  dsDocSpecNewRowCaption = '� ����� ������';
  dsDocSpecStaticTextCaption = '����������� �����';
  dsDocSpecRefCaption = '������';
  dsDocSpecCalcCountCaption = '������� ����������';
  dsDocSpecColUnionCaption = '���������� �������';
  dsDocSpecShowingCaption = '����������';
  dsDocSpecSelectDataFromCaption = '������ �������� ��';
  dsDocSpecOrderCaption = '������� ����������';
  dsDocSpecShowZeroValsCaption = '���������� ������� ��������';
  dsDocSpecThickRectCaption = '������� �����';
  dsDocSpecRowNumCaption = '��������� �����';
  dsDocSpecColNumCaption = '��������� ��������';
  dsDocSpecPriorityCaption = '��������������';

  dsDocSpecONE_VAVCaption = '��';
  dsDocSpecZERO_VAVCaption = '���';
  dsDocSpecTRUE_VAVCaption = '��';
  dsDocSpecFALSE_VAVCaption = '���';      *}

// SpecUtils
  dsDocSpecUtilsReqVersion = '������ �������� ��������� "%s" �� ��������������. ��������� ������ "%s" ��� ����.';
  dsDocViewerNumTCellLTitle = '� �/�';
  dsDocViewerErrorSetDocFormat = '������ ������ ���������� �� ������������ ��������� �����.';
  dsDocViewerErrorNoSpecDef = '������ ���� ������ ������������.';
  dsDocViewerErrorNoDocDef = '������ ���� ������ ��������.';
  dsDocViewerErrorCreateMSWord = '������ ������ ���������� �� ������������ ������������ ���������� � ������� MS Word.';
  dsDocViewerErrorPreviewTypeVal = '�������� "ptUnknown" ��������� APreviewType ������������ ������ ��� ���������� �����.';

// Viewer
{*
  dsDocViewerIEHeader = '&b���. &p �� &P';
  dsDocViewerCaption = '�������� ���������';
  dsDocViewerErrorObjectLocked = '������ ������������';
  dsDocViewerErrorOpenTmpFile = '������ �������� ���������� �����.';
  dsDocViewerErrorApp = '����������� ����������';
  dsDocViewerErrorClass = '����������� �����';
  dsDocViewerErrorOnFly = '��� ������ "�� ����" �������� � ������� %s �� ��������.';
  dsDocViewerErrorMsgCaption = '��������������� ��������';
  dsDocViewerErrorRepCreate1 = '���������� - %s;'+#13#10+'���        - %s;'+#13#10+'���������: %s';
  dsDocViewerErrorRepCreate2 = '��� ������������ ������ ��������� ������.';
  dsDocViewerErrorRepCreate3 = '������ ������������ ������.';
  dsDocViewerErrorIllegalOperation = '��������� ��������� ������������ ��������.';

// InclToNameEdit
  dsDocInclToNameEditCaption = '��������� ������, ����������� � ������������ �������';
  dsDocInclToNameEditOkBtnCaption     = '���������';
  dsDocInclToNameEditCancelBtnCaption = '������';
  dsDocInclToNameEditHelpBtnCaption   = '&������';

// ISection
  dsDocISectionErrorCreate1 = '������ ������������ ��� ������������� ������ ������ IOR � IMOR.';
  dsDocISectionErrorCreate2 = '��������� xml-�������� �� �������� �������������.';
  dsDocISectionCaption1 = '����� ���������� ���������� �� ...';
  dsDocISectionCaption2 = '����� ������ ��������� �� ...';
  dsDocISectionErrorCaption = '������!!!';

  dsDocISectionLeaveAsIsBtnCaption     = '�������� �������� ��������������';
  dsDocISectionOkBtnCaption     = '���������';
  dsDocISectionCancelBtnCaption = '������';
  dsDocISectionHelpBtnCaption   = '&������';

  dsDocISectionSelAllCaption = '������� &���';
  dsDocISectionSelAllHint = '������� ���';
  dsDocISectionClearSelCaption = '&�����';
  dsDocISectionClearSelHint = '�����';
  dsDocISectionInvSelCaption = '&��������';
  dsDocISectionInvSelHint = '��������';

// ListSortOrder
  dsDocListSortOrderCaption = '����� �������� ��� ����������';
  dsDocListSortOrderColLabCaption = '�������:';
  dsDocListSortOrderSortOrderLabCaption = '����������� ��:';
  dsDocListSortOrderAddColsCaption = '>';
  dsDocListSortOrderAddColsHint = '�������� ������� ��� ���������� (Ins)';
  dsDocListSortOrderDeleteCaption = 'X';
  dsDocListSortOrderDeleteHint = '������� (Del)';
  dsDocListSortOrderClearCaption = '��������';
  dsDocListSortOrderClearHint = '�������� (Ctrl+Del)';
  dsDocListSortOrderMoveUpCaption = '�����';
  dsDocListSortOrderMoveUpHint = '����������� ����� (Ctrl+�����)';
  dsDocListSortOrderMoveDownCaption = '����';
  dsDocListSortOrderMoveDownHint = '����������� ���� (Ctrl+����)';
  dsDocListSortOrderAscSortCaption = '���������� �� &�����������';
  dsDocListSortOrderAscSortHint = '���������� �� �����������';
  dsDocListSortOrderDescSortCaption = '���������� �� &��������';
  dsDocListSortOrderDescSortHint = '���������� �� ��������';
  dsDocListSortOrderOkBtnCaption     = '���������';
  dsDocListSortOrderCancelBtnCaption = '������';
  dsDocListSortOrderHelpBtnCaption   = '&������';

// ObjRulesAssocEdit

  dsDocObjRulesAssocEditTitleCaptionNormal = '������������ ����� ��������� (������)';
  dsDocObjRulesAssocEditTitleCaptionCross = '������������ ����� ��������� (���������)';
  dsDocObjRulesAssocEditTableElementNameCol = '������ �������';
  dsDocObjRulesAssocEditTableElementNameRow = '����� ������';
  dsDocObjRulesAssocEditSetAssocConf = '��� �������� "%s" ��� ����������� ������������. ����������?';
  dsDocObjRulesAssocEditCaption = '��������� ������������';
  dsDocObjRulesAssocEditThisStructTitle = '������:';
  dsDocObjRulesAssocEditRefStructTitle = '������������� ������:';
  dsDocObjRulesAssocEditAutoExpand = '�����������';
  dsDocObjRulesAssocEditSetAssocCaption = '&����������';
  dsDocObjRulesAssocEditSetAssocHint = '����������';
  dsDocObjRulesAssocEditOkCaption = '���������';
  dsDocObjRulesAssocEditOkHint = '���������';
  dsDocObjRulesAssocEditClearAssocCaption = '&��������';
  dsDocObjRulesAssocEditClearAssocHint = '��������';
  dsDocObjRulesAssocEditCancelBtnCaption = '������';
  dsDocObjRulesAssocEditHelpBtnCaption   = '&������';

// ObjRulesEdit
  dsDocObjRulesEditCaption = '��������� ������������';
  dsDocObjRulesEditObjTitleCaption = '  ������';
  dsDocObjRulesEditThisObjTitle = '������:';
  dsDocObjRulesEditRefObjTitle = '������������� ������:';
  dsDocObjRulesEditPropHead = '  ���������';
  dsDocObjRulesEditAssocType = '��� ������������:';
  dsDocObjRulesEditCrossTypeN = '������';
  dsDocObjRulesEditCrossTypeC = '���������';

  dsDocObjRulesEditOkCaption = '���������';
  dsDocObjRulesEditOkHint = '���������';
  dsDocObjRulesEditCancelBtnCaption = '������';
  dsDocObjRulesEditHelpBtnCaption   = '&������';

// ParametersEdit
  dsDocParametersEditSpecName = '������������';
  dsDocParametersEditOrientation1 = '�������';
  dsDocParametersEditOrientation2 = '���������';
  dsDocParametersEditStyleExists = '����� � ������������� "%s" ��� ����.';

  dsDocParametersEditCDERR_DIALOGFAILURE    = '������ �������� ����������� ����.';
  dsDocParametersEditCDERR_FINDRESFAILURE   = '�� ������ ��������� ������.';
  dsDocParametersEditCDERR_INITIALIZATION   = '������ ������������� ������� ����������� ����, �������� ��-�� �������� ������.';
  dsDocParametersEditCDERR_LOADRESFAILURE   = '������ �������� ���������� �������.';
  dsDocParametersEditCDERR_LOADSTRFAILURE   = '������ �������� ��������� ������.';
  dsDocParametersEditCDERR_LOCKRESFAILURE   = '������ ���������� ���������� �������.';
  dsDocParametersEditCDERR_MEMALLOCFAILURE  = '������ ��������� ������ ��� ���������� ��������.';
  dsDocParametersEditCDERR_MEMLOCKFAILURE   = '������ ���������� ������, ��������� � ������������.';
  dsDocParametersEditCDERR_NOHINSTANCE      = '�� ������ ��������������� ���������� ���������� ���������� ��� ������ ���������� �������.';
  dsDocParametersEditCDERR_NOHOOK           = '�� ������ ��������������� ��������� �� hook-���������.';
  dsDocParametersEditCDERR_NOTEMPLATE       = '�� ������ ������ ����������� ����.';
  dsDocParametersEditCDERR_REGISTERMSGFAIL  = '������ ����������� ������ ����������� ����.';
  dsDocParametersEditCDERR_STRUCTSIZE       = '������ ��� �������� ������� ����������������� ���������.';
  dsDocParametersEditPDERR_CREATEICFAILURE  = '������ �������� ��������������� ���������.';
  dsDocParametersEditPDERR_DEFAULTDIFFERENT = '������� �� ��������� �� ������������� ����������.';
  dsDocParametersEditPDERR_DNDMMISMATCH     = '�� ������������ ��������� ��������� �� ���������� ����������������� ����������.';
  dsDocParametersEditPDERR_GETDEVMODEFAIL   = '������ ������������� ���������� ���������.';
  dsDocParametersEditPDERR_INITFAILURE      = '������ �������������';
  dsDocParametersEditPDERR_LOADDRVFAILURE   = '������ �������� �������� ���������� ��������.';
  dsDocParametersEditPDERR_NODEFAULTPRN     = '����������� ������� �� ���������.';
  dsDocParametersEditPDERR_NODEVICES        = '�� ������� �������� ��������.';
  dsDocParametersEditPDERR_PARSEFAILURE     = '������ ������� ����� � ����������������� ���������.';
  dsDocParametersEditPDERR_PRINTERNOTFOUND  = '������ [devices] ����� WIN.INI �� �������� ������ ��� ���������� ��������.';
  dsDocParametersEditPDERR_RETDEFFAILURE    = '�������������� � ����������������� ���������.';
  dsDocParametersEditPDERR_SETUPFAILURE     = '������ �������� ���������� �������.';
  dsDocParametersEditErrorCreateDlg = '������ �������� ����������� ����.';
  dsDocParametersEditNewStyle = '����� ����� %s';
  dsDocParametersEditDeleteStyleConf = '����� "%s" ������������ ��������� ���������. �� ������� � ���, ��� ������ ������� ���?';

  dsDocParametersEditfmParametersEditCaption = '���������';
  dsDocParametersEditbApplyBtnCaption = '���������';
  dsDocParametersEditbCancelBtnCaption = '������';
  dsDocParametersEditbHelpBtnCaption = '&������';
  dsDocParametersEdittsCommonTSCaption = '&�����';
  dsDocParametersEditSpecNameLabLabCaption = '������������:';
  dsDocParametersEditLabel3LabCaption = '�����:';
  dsDocParametersEditLabel4LabCaption = '������:';
  dsDocParametersEditLabel5LabCaption = '��������:';
  dsDocParametersEditLabel6LabCaption = '���� � ����� ��������:';
  dsDocParametersEditlbTimeStampLabCaption = 'lbTimeStamp';
  dsDocParametersEditlbAutorLabCaption = 'lbAutor';
  dsDocParametersEditCondRulesLbLabCaption = '������� ������:' ;
  dsDocParametersEditDocRulesLbLabCaption = '������� ������������ ��������� �� ���������:';
  dsDocParametersEditGroupBox1GBCaption = '������� ���������� ���������';
  dsDocParametersEditchbPersonalCHBCaption = '������������';
  dsDocParametersEditchbGroupCHBCaption = '���������';
  dsDocParametersEditPeriodDefChBxCHBCaption = '�������������';
  dsDocParametersEditSaveBDChBxCHBCaption = '��������� � ����� ����������';
  dsDocParametersEdittsPageSetupTSCaption = '&��������� ��������';
  dsDocParametersEditLabel8LabCaption = '������';
  dsDocParametersEditLabel9LabCaption = '����������';
  dsDocParametersEditLabel10LabCaption = '���� (��)';
  dsDocParametersEditLabel11LabCaption = '������:';
  dsDocParametersEditlbOrientationLabCaption = 'lbOrientation';
  dsDocParametersEditLabel13LabCaption = '�����:';
  dsDocParametersEditLabel14LabCaption = '�������:';
  dsDocParametersEditLabel15LabCaption = '������:';
  dsDocParametersEditLabel16LabCaption = '������:';
  dsDocParametersEditlbPageSizeLabCaption = 'lbPageSize';
  dsDocParametersEditlbLeftMarginLabCaption = 'lbLeftMargin';
  dsDocParametersEditlbTopMarginLabCaption = 'lbTopMargin';
  dsDocParametersEditlbRightMarginLabCaption = 'lbRightMargin';
  dsDocParametersEditlbBottomMarginLabCaption = 'lbBottomMargin';
  dsDocParametersEditbChangePageSetupBtnCaption = '��������';
  dsDocParametersEdittsStylesTSCaption = '&�����';
  dsDocParametersEditLabel1LabCaption = '�����';
  dsDocParametersEditLabel22LabCaption = '��������������';
  dsDocParametersEditlbFontInfoLabCaption = 'lbFontInfo';
  dsDocParametersEditchbUseBDefCHBCaption = '������������ �� ��������� ��� ������';
  dsDocParametersEditchbUseHDefCHBCaption = '������������ �� ��������� ��� ����������';
  dsDocParametersEditbAddStyleBtnCaption = '��������';
  dsDocParametersEditbDeleteStyleBtnCaption = '�������';
  dsDocParametersEditchbWordWrapCHBCaption = '���������� �� ������';
  dsDocParametersEditbChangeFontBtnCaption = '�������� �����';
  dsDocParametersEditButton1BtnCaption = '�������� ������������';
  dsDocParametersEditactAddStyleActCaption = '&��������';
  dsDocParametersEditactEditStyleActCaption = '&�������� ������������';
  dsDocParametersEditactDeleteStyleActCaption = '&�������';
//  dsDocParametersEdit = '';

// PassportEdit
  dsDocPassportEditErrorOwnreType = '�������� ������ ���� �������� TdsDocFilter.';

  dsDocPassportEditCaption = '���������';
  dsDocPassportEditLabel1LabCaption = '������������:';
  dsDocPassportEditLabel2LabCaption = '�����:';
  dsDocPassportEditLabel3LabCaption = '������:';
  dsDocPassportEditLabel4LabCaption = '��������:';
  dsDocPassportEditLabel5LabCaption = '���� � ����� ��������:';
  dsDocPassportEditlbTimeStampLabCaption = 'lbTimeStamp';
  dsDocPassportEditbNameAdvancedBtnCaption = '������������� ...';
  dsDocPassportEditbApplyBtnCaption = '���������';
  dsDocPassportEditbCancelBtnCaption = '������';
  dsDocPassportEditbHelpBtnCaption = '������';

// PeriodEdit
  dsDocPeriodEditGetPeriodDefXMLStr = '������� "%s" ������������ TPeriodDef �� ��������������.';
  dsDocPeriodEditSetPeriodDefXMLStr = '"%s" - ������������ ��������.';
  dsDocPeriodEditGetPeriodValueXMLStr = '��������� ������ ���������.';

  dsDocPeriodEditGetPeriodValueXMLStrHalfYear = '�������� "%s" ��� ��������� �� ��������������.';
  dsDocPeriodEditGetPeriodValueXMLStrQuart = '�������� "%s" ��� �������� �� ��������������.';

  dsDocPeriodEditSetPeriodValueXMLStrHalfYear = '"%s" - ������������ �������� ������� ��� ���������.';
  dsDocPeriodEditSetPeriodValueXMLStrQuart = '"%s" - ������������ �������� ������� ��� ��������.';
  dsDocPeriodEditSetPeriodValueXMLStrMonth = '"%s" - ������������ �������� ������� ��� ������.';


  dsDocPeriodEditValidateInputsBegin = '���������� ������� ������ �������.';
  dsDocPeriodEditValidateInputsEnd = '���������� ������� ��������� �������.';
  dsDocPeriodEditValidateInputsNotEq = '������ ������� �� ������ ��������� ��� ���������.';
  dsDocPeriodEditDefRBtnClick = '�� ������ ������� ������� FPeriodDefMap.';

  dsDocPeriodEditCaption = '������';
  dsDocPeriodEditOkBtnBtnCaption = '���������';
  dsDocPeriodEditCancelBtnBtnCaption = '������';
  dsDocPeriodEditClientGBxGBCaption = '�������� �������';
  dsDocPeriodEditHalfYearYearLbLabCaption = '���';
  dsDocPeriodEditQuoterYearLbLabCaption = '���';
  dsDocPeriodEditMonthYearLbLabCaption = '���';
  dsDocPeriodEditUniqueSepLbLabCaption = '-';
  dsDocPeriodEditYearRBtnRBCaption = '���';
  dsDocPeriodEditHalfYearRBtnRBCaption = '���������';
  dsDocPeriodEditQuoterRBtnRBCaption = '�������';
  dsDocPeriodEditMonthRBtnRBCaption = '�����';
  dsDocPeriodEditUniqueRBtnRBCaption = '����������';
  dsDocPeriodEditUnDefRBtnRBCaption = '�� ���������';

  dsDocPeriodEditHalfYearCBxItems1 = '������';
  dsDocPeriodEditHalfYearCBxItems2 = '������';

  dsDocPeriodEditQuoterCBxItems1 = '������';
  dsDocPeriodEditQuoterCBxItems2 = '������';
  dsDocPeriodEditQuoterCBxItems3 = '������';
  dsDocPeriodEditQuoterCBxItems4 = '���������';     *}
{*
// RulesExprEdit

  dsDocRulesExprEditCaption = '�������';
  dsDocRulesExprEditlbTitleLabCaption = '�������';
  dsDocRulesExprEditbOkBtnCaption = '���������';
  dsDocRulesExprEditbCancelBtnCaption = '������';
  dsDocRulesExprEditbHelpBtnCaption = '&������';

  dsDocRulesExprEditrtUnknown = '???';
  dsDocRulesExprEditrtCalcCaption = '������� ����������';
  dsDocRulesExprEditrtCalclbTitleCaption =  '������� ���������� ���';
  dsDocRulesExprEditrtHandCaption = '������� �������� ������� �����';
  dsDocRulesExprEditrtHandlbTitleCaption =  '������� �������� ������� ����� ���';

// SetRef
  dsDocSetRefGetObjTreeTitle = '������ �������� ��� ������ �� ���������.';
  dsDocSetRefSetObjTreeTitle = '������ �������� ��� ������ �� ���������.';
  dsDocSetRefSetRef = '�� ������ ������ ������ � ���''�� "%s".';

  dsDocSetRefCaption = '���������� ������';
  dsDocSetRefbOkBtnCaption = '���������';
  dsDocSetRefbCancelBtnCaption = '������';
  dsDocSetRefbHelpBtnCaption = '&������';
  dsDocSetRefbClearBtnCaption = '��������';

// StyleEdit
  dsDocStyleEditStyleNameCaption   = '������������ �����';
  dsDocStyleEditfumADDCaption = '���������� �����';
  dsDocStyleEditfumEDITCaption = '�������������� ������������ �����';

  dsDocStyleEditbOkBtnCaption = '���������';
  dsDocStyleEditbCancelBtnCaption = '������';
  dsDocStyleEditbHelpBtnCaption = '&������';
  dsDocStyleEditStyleNameLabLabCaption = '������������ �����:';

// AddList
  dsDocAddListErrorInvalidArgument = '���������� ������ ���� �������� �������������.';
  dsDocAddListErrorSetMainIS = '��������� �������� �� ����������� ������, ������������ � cbISs.';
  dsDocAddListErrorAddCol =  '"��� ������ �������� � ��������� "���������� �������" ����������� ���������� ����� ��������. ������ �������� �� ����� ���������."';
  dsDocAddListErrorAddCols = '"��� ������ �������� � ��������� "���������� �������" ����������� ���������� ����� ��������."';
  dsDocAddListCaption = '����� ������� ������';
  dsDocAddListColGroupCaption = '� � � � � �  � � � � � � � �';

  dsDocAddListISListTitle = '������ �������� ��:';
  dsDocAddListISTreeTitle = '�������:';
  dsDocAddListColsTitle = '�������:';

  dsDocAddListAddColsCaption = '>';
  dsDocAddListAddColsHint = '�������� �������';

  dsDocAddListAddGColsCaption = '+>';
  dsDocAddListAddGColsHint = '�������� ������ ��������';

  dsDocAddListDeleteCaption = 'X';
  dsDocAddListDeleteHint = '�������';

  dsDocAddListClearCaption = '��������';
  dsDocAddListClearHint = '��������';

  dsDocAddListUpCaption = '�����';
  dsDocAddListUpHint = '����������� �����';

  dsDocAddListDownCaption = '����';
  dsDocAddListDownHint = '����������� ����';

  dsDocAddListOkBtnCaption     = '���������';
  dsDocAddListCancelBtnCaption = '������';
  dsDocAddListHelpBtnCaption   = '&������';

// AddTable
  dsDocAddTableListSrcLabCaption = '������ �������� ��:';
  dsDocAddTableColCountLabCaption = '����� ��������:';
  dsDocAddTableRowCountLabCaption = '����� �����:';
  dsDocAddTableOkBtnCaption     = '���������';
  dsDocAddTableCancelBtnCaption = '������';
  dsDocAddTableHelpBtnCaption   = '&������';

// CommentEdit
  dsDocCommentEditCommentLabCaption = '�����������:';
  dsDocCommentEditOkBtnCaption      = '���������';
  dsDocCommentEditCancelBtnCaption  = '������';
  dsDocCommentEditHelpBtnCaption    = '&������';

// Count
  dsDocCountEvtCaption = '������� ���������� ��� "%s"';
  dsDocCountCountCaption = '������� ���������� ��� :';
  dsDocCountFilterRulesCaption = '������� ������:';
  dsDocCountOkBtnCaption      = '���������';
  dsDocCountCancelBtnCaption  = '������';
  dsDocCountHelpBtnCaption    = '&������';
  dsDocCountClearBtnCaption    = '��������';

// ElSizeEdit
  dsDocElSizeEditAreInputsCorrectMsg = '���������� ������� ��������.';
  dsDocElSizeEditValHeightCaption = '��������� �������� ������';
  dsDocElSizeEditHeightCaption = '������';
  dsDocElSizeEditFullHeightCaption = '�� ���� ������';
  dsDocElSizeEditValWidthCaption = '��������� �������� ������';
  dsDocElSizeEditWidthCaption = '������';
  dsDocElSizeEditFullWidthCaption = '�� ���� ������';

  dsDocElSizeEditErrorSizeType = '"%s" - ������������ �������� ��� ���� �������.';
  dsDocElSizeEditErrorUnitsType = '"%s" - ������������ �������� ��� ������� ���������.';

  dsDocElSizeEditErrorSizeVal = '��������� ������� ������ ���� ��������������� �����.';
  dsDocElSizeEditErrorMaxSizeVal = '������������ �������� ������� ������ ���� ��������������� ������.';
  dsDocElSizeEditErrorSizeValRange = '"%s" - �������� �� ������ ��������� "%s".';
  dsDocElSizeEditErrorValValidate = '"%s" - ������������ ��������.';

  dsDocElSizeEditAutoSizeCaption  = '���������������';
  dsDocElSizeEditValueCaption     = '��������';
  dsDocElSizeEditUnitsCaption     = '�������:';
  dsDocElSizeEditOkBtnCaption     = '���������';
  dsDocElSizeEditCancelBtnCaption = '������';
  dsDocElSizeEditHelpBtnCaption   = '&������';

// HandEdit
  dsDocMatrixErrorParam = '�������� ������ ����';
  dsDocMatrixErrorIndex = '������ %s ������� �� ������� �������.';
  dsDocHandEditErrorRow = '������';
  dsDocHandEditErrorCol = '�������';
  dsDocHandEditCheckRulesMsg = '������ ������ (� ������ - %s; � ������� - %s)'+#13#10+'�� ������������� �������� �������� ��� %s � %s.'+#13#10+'������� ��������: %s';
  dsDocHandEditCheckRulesCpt = '��������� ������ �� ������������� �������� ��������.';

  dsDocHandEditStructHeaderCaption = '  ��������� ���������';
  dsDocHandEditmiDocCaption      = '��������';
  dsDocHandEditmiViewCaption     = '���';
  dsDocHandEditmiJumpCaption     = '�������';

  dsDocHandEditSaveCaption = '&���������';
  dsDocHandEditSaveHint = '���������';
  dsDocHandEditReCalcCaption = '&�������� ����������� ��������';
  dsDocHandEditReCalcHint = '�������� ����������� ��������';
  dsDocHandEditFirstDocElementCaption = '�&����� ������� ���������';
  dsDocHandEditFirstDocElementHint = '������ ������� ���������';
  dsDocHandEditPrevDocElementCaption = '�&��������� ������� ���������';
  dsDocHandEditPrevDocElementHint = '���������� ������� ���������';
  dsDocHandEditNextDocElementCaption = '�&�������� ������� ���������';
  dsDocHandEditNextDocElementHint = '��������� ������� ���������';
  dsDocHandEditLastDocElementCaption = '�&�������� ������� ���������';
  dsDocHandEditLastDocElementHint = '��������� ������� ���������';
  dsDocHandEditViewStructCaption = '&��������� ���������';
  dsDocHandEditViewStructHint = '��������� ���������';
  dsDocHandEditViewDocElementInscCaption = '&������� ��� ��������� ���������';
  dsDocHandEditViewDocElementInscHint = '������� ��� ��������� ���������';
  dsDocHandEditExitCaption = '&�������';
  dsDocHandEditExitHint = '�������';

// RulesEdit
   dsDocRulesEditVersionLimit  = '������ ������ ������������ ��������� ������ ������������ ������ ��� ������.';
  dsDocRulesEditAccessDeny    = '� ������� ��������.';
  dsDocRulesEditErrorEmptyDoc = '���������� ������� ���� �� ���� ������ ������������ ���������.';
  dsDocRulesEditConfCol   = '������ �������';
  dsDocRulesEditConfRow   = '����� ������';
  dsDocRulesEditErrorConf = '���������� ������� ������������ ���� �� ��� %s �������.';
  dsDocRulesEditErrorIntDefDoc = '���������� ������� �������� ������������� ���������� �� ������.';
  dsDocRulesEditErrorPeriodDef = '���������� ������� �������� �������.';
  dsDocRulesEditErrorOrgDef = '���������� ������� ��� ����������� �� ������.';
  dsDocRulesEditDefChangeConf = '����� �������� ������������� ���������� �������� � ������� ������ ��������. ����������?';

  dsDocRulesEditCaption     = '������� ������������ ��������� �� ���������';
  dsDocRulesEditCreateStep  = '  ����� ������������';
  dsDocRulesEditStepParam   = '     ���������';
  dsDocRulesEditStepObjects = '     �������';
  dsDocRulesEditIntDocCaption = '������������� ���������';
  dsDocRulesEditIntDocDef = '��������:';
  dsDocRulesEditIntDocPerType = '��� �������:';
  dsDocRulesEditIntDocPeriod = '������:';
  dsDocRulesEditIntDocPeriodisity = '�������������:';
  dsDocRulesEditOrg = '�����������';
  dsDocRulesEditOrgType = '���:';
  dsDocRulesEditOrgName = '������������:';

  dsDocRulesEditAddStepCaption = '�������� &����';
  dsDocRulesEditAddStepHint = '�������� ����';
  dsDocRulesEditMoveStepUpCaption = '����������� ����&�';
  dsDocRulesEditMoveStepUpHint = '����������� �����';
  dsDocRulesEditMoveStepDownCaption = '����������� ���&�';
  dsDocRulesEditMoveStepDownHint = '����������� ����';
  dsDocRulesEditDeleteCaption = '&�������';
  dsDocRulesEditDeleteHint = '�������';
  dsDocRulesEditAddObjRulesCaption = '�������� ������� ������������ &�������';
  dsDocRulesEditAddObjRulesHint = '�������� ������� ������������ �������';
  dsDocRulesEditSetAssocCaption = '���������� ����&��������';
  dsDocRulesEditSetAssocHint = '���������� ������������';
  dsDocRulesEditEditPropCaption = '&��������';
  dsDocRulesEditEditPropHint = '��������';

  dsDocRulesEditOkBtnCaption     = '���������';
  dsDocRulesEditCancelBtnCaption = '������';
  dsDocRulesEditHelpBtnCaption   = '&������';


//dsDocPageSetup

  dsDocPagePrintKoeff = '����������� ������: ';
  dsDocPageSetupCaption = '��������� ��������';
  dsDocPageSetupOrientationGBCaption = ' ���������� ';
  dsDocPageSetupportRBCaption = '�������';
  dsDocPageSetuplandRBCaption = '���������';
  dsDocPageSetupFieldsGBCaption = ' ���� (��) ';
  dsDocPageSetupOkBtnCaption = '���������';
  dsDocPageSetupCancelBtnCaption = '������';
*}
//DocCreator
  dsDocDocCreatorCaption      = '������������ ���������';
  dsDocDocCreatorStageByCommCaption  = '���� : %s �� %s';

  dsDocDocCreatorDocLoadConf  = '������ �������� �� ��������� ������ ��� ����������!!!'#13#10' ��������� ����������� ��������?';
  dsDocDocCreatorDocLoadConfCaption  = '������������� �������� ���������';
  dsDocDocCreatorInitCaption  = '�������������...';
  dsDocDocCreatorInscCreateCaption  = '������������ ������ �������';
  dsDocDocCreatorListCreateCaption  = '������������ ������ (����� �����: "%s")';
  dsDocDocCreatorErrorISLink  = '����������� ����� ����� ����������';
  dsDocDocCreatorTableCreateGetCount  = '������������ ������� (������ ���������� �����/��������)';
  dsDocDocCreatorTableCreateRows  = '������������ ������� (������������ �����)';
  dsDocDocCreatorTableCreateRow  = '������';
  dsDocDocCreatorTableCreateCols  = '������������ ������� (������������ �������)';
  dsDocDocCreatorTableCreateCol  = '�������';
  dsDocDocCreatorTableCreateCeills  = '������������ ������� (������������ �����)';
  dsDocDocCreatorTableCreateCeill  = '������������ �������, ������(%s,%s)';
  dsDocDocCreatorTableCreateCeillMsg  = '������������ ������� (%s � %s)';
  dsDocDocCreatorErrorMissingColRowDef  = '����������� �������� ����� ��� �������';
  dsDocDocCreatorErrorCeilCalc  = '������ ��� ���������� ������.'#13#10'������ - %s ������� - %s; (%s,%s)';

  dsDocDocCreatorPeroidFromCaption  = '�';
  dsDocDocCreatorPeroidTo1Caption  = '��';
  dsDocDocCreatorPeroidTo2Caption  = '��';
  dsDocDocCreatorUndefPeroidCaption  = '�� ������';

  dsDocDocCreatorPeriodicityEqual  = '��������� � �������������� ������������ ���������';
  dsDocDocCreatorPeriodicityYear  = '���';
  dsDocDocCreatorPeriodicityHalfYear  = '���������';
  dsDocDocCreatorPeriodicityQuarter  = '�������';
  dsDocDocCreatorPeriodicityMonth  = '�����';
  dsDocDocCreatorPeriodicityUnique  = '����������';
  dsDocDocCreatorPeriodicityNo  = '�����������';
  dsDocDocCreatorPeriodicityAny  = '�����';

  dsDocDocCreatorAuthorDefined  = '';
  dsDocDocCreatorAuthorThis  = '����';
  dsDocDocCreatorAuthorAllChild  = '��� �����������';

  dsDocDocCreatorErrorNoIntDocs  = '���� %s'#13#10'����������� ��������� ��� ����������.'#13#10#13#10'������������� ���������:'#13#10'��������:'#09'"%s";'#13#10'������:'#09#09'"%s";'#13#10'�������������:'#09'"%s";'#13#10'�����������:'#09'"%s".'#13#10;

  dsDocDocCreatorErrorCreateRowColNamesNotValid  = '������������ ������ �����/�������� �� ������������� ''rows,columns''.';
  dsDocDocCreatorErrorRowColReferNotValid  = '������� ������������ ��������� �� ���������,'#13#10' ������� ������ �� ������������� %s, ���=''%s''.';

  dsDocDocCreatorSpecPrepare  = '���������� �������� ���������';

//ExtFunc

  dsDocExtFuncDocPeriodLocStr1  = '���';
  dsDocExtFuncDocPeriodLocStr2  = '���������';
  dsDocExtFuncDocPeriodLocStr3  = '�������';
  dsDocExtFuncDocPeriodLocStr4  = '�����';
  dsDocExtFuncDocPeriodLocStr5  = '����������';
  dsDocExtFuncDocPeriodLocStr6  = '�����������';

  dsDocExtFuncDocPeriodValStrUniq1  = '%s.%s.%s �. - %s.%s.%s �.';
  dsDocExtFuncDocPeriodValStrUniq2  = '� %s.%s.%s �.';
  dsDocExtFuncDocPeriodValStrUniq3  = '�� %s.%s.%s �.';

  dsDocExtFuncDocPeriodValStrMonth  = '%s %s �.';

  dsDocExtFuncDocPeriodValStrQuart1  = 'I ������� %s �.';
  dsDocExtFuncDocPeriodValStrQuart2  = 'II ������� %s �.';
  dsDocExtFuncDocPeriodValStrQuart3  = 'III ������� %s �.';
  dsDocExtFuncDocPeriodValStrQuart4  = 'IV ������� %s �.';

  dsDocExtFuncDocPeriodValStrHalfYear1  = 'I ���������  %s �.';
  dsDocExtFuncDocPeriodValStrHalfYear2  = 'II ��������� %s �.';

  dsDocExtFuncDocPeriodValStrYear  = '%s �.';

  dsDocExtFuncErrorReadSettingCaption  = '������ ������ ��������';
  dsDocExtFuncErrorReadSettingMsg  = '� ���������� ��������� �� ������ ��������� ���';

  dsDocExtFuncCreateDocViewMsg  = '������������ ����������� ���������';
  dsDocExtFuncErrorCreateDocView  = '������ ������������ ��������� (����������� �������� ���������)';

  dsDocExtFuncReplaceDocMsg  = '�������� - "%s"'#13#10' �� ������: %s'#13#10' ��� ����������.'#13#10#13#10'�������� ������������ ��������?';
  dsDocExtFuncReplaceDocCaption  = '������������� ������ ���������';
  dsDocExtFuncMissingDocDef  = '����������� �������� ���������, ��� �������� "%s"';
  dsDocExtFuncNoDocDef  = '�������� �����������';
  dsDocExtFuncFileVersionString  = 'StringFileInfo\041904E3\FileVersion';

  dsDocExtFuncRecCount1   = '������';
  dsDocExtFuncRecCount234 = '������';
  dsDocExtFuncRecCount    = '�������';

implementation

end.
