//---------------------------------------------------------------------------
#include <vcl.h>

#pragma hdrstop

#include "dsDocSQLCreator.h"
#include "dsDocSrvConstDef.h"
#include "dsDocXMLUtils.h"
//#include "msgdef.h"
#include "icsLog.h"
//#include "ExtFunc.h"
//#include "ICSDocExDataDef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
//###########################################################################
//#                     TdsDocSQLCreator                                         #
//###########################################################################
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
__fastcall TdsDocSQLCreator::TdsDocSQLCreator(TAxeXMLContainer * AXMLS, UnicodeString  APref)
 {
  FXMLS      = AXMLS;
  FTablePref = APref;
  FPerType   = -1;
  PerFr      = 0;
  PerTo      = 0;
 }
//---------------------------------------------------------------------------
__fastcall TdsDocSQLCreator::~TdsDocSQLCreator()
 {
  //FExtProc = NULL;
  FXMLS = NULL;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocSQLCreator::SetPeriod(int APerType, TDateTime AFr, TDateTime ATo)
 {
  FPerType = APerType;
  PerFr    = AFr;
  PerTo    = ATo;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::CreateSQL(UnicodeString ABaseISRef, TTagNode * ACond, TStringList * AFieldList, UnicodeString AKeyVal, UnicodeString & ACountSQL, TStringList * AOrderList,
    TStringList * ATabList)
 {
  FFBaseISRef = ABaseISRef;
  TStringList * FTabs;
  UnicodeString FSQL = "";
  UnicodeString __tmp;
  try
   {
    bool FIsDist = false;
    bool FIsAddKey = false;
    UnicodeString FSelect, FFrom, FWhere, FISTabKey;
    FTabs     = new TStringList;
    FISTabKey = GetISTabKey(ABaseISRef);
    FWhere    = "";
    // ********** Begin fix 03.05.11
    if (ACond)
     {
      bool RC = true;
      FWhere = CreateWhere(ACond, FTabs, RC);
      if (!RC)
       return "";
      if (ATabList)
       {
        int tIdx;
        for (int t = 0; t < ATabList->Count; t++)
         {
          tIdx = FTabs->IndexOf(ATabList->Strings[t]);
          if (tIdx == -1)
           {
            FTabs->Add(ATabList->Strings[t]);
           }
         }
        FFrom = CreateFrom(ABaseISRef, FTabs, &FIsDist, AKeyVal, &FIsAddKey);
       }
      else
       FFrom = CreateFrom(ABaseISRef, FTabs, &FIsDist, AKeyVal, &FIsAddKey);
     }
    else
     {
      if (ATabList)
       FFrom = CreateFrom(ABaseISRef, ATabList, &FIsDist, AKeyVal, &FIsAddKey);
      else
       FFrom = GetTabName(ABaseISRef) + " " + GetTabAlias(ABaseISRef);
     }
    // ********** end fix 03.05.11
    if (FFrom == "")
     return "";
    FSQL      = "Select " + UnicodeString((FIsDist) ? "Distinct " : "") + " ";
    ACountSQL = "Select Count(" + UnicodeString((FIsDist) ? "Distinct " : "") + " " + FISTabKey + ") as RCOUNT";
    if (AFieldList)
     {
      for (int i = 0; i < AFieldList->Count; i++)
       FSQL += GetPart2(AFieldList->Strings[i], '=') + ", ";
     }
    FSQL += FISTabKey;
    FSQL += " From " + FFrom;
    ACountSQL += " From " + FFrom;
    //__tmp = GetPeriodVals(ABaseISRef,PerFr,PerTo);
    if ((__tmp != "") || (FWhere != "") || (!FIsAddKey && (AKeyVal != "")))
     {
      bool IsFirst = true;
      FSQL += " Where ";
      ACountSQL += " Where ";
      if (FWhere != "")
       {
        FSQL += "(" + FWhere + ")";
        ACountSQL += "(" + FWhere + ")";
        IsFirst = false;
       }
      if (!FIsAddKey && (AKeyVal != ""))
       {
        if (IsFirst)
         {
          FSQL += AKeyVal;
          ACountSQL += AKeyVal;
         }
        else
         {
          FSQL += " and " + AKeyVal;
          ACountSQL += " and " + AKeyVal;
         }
        IsFirst = false;
       }
      if (__tmp != "")
       {
        if (IsFirst)
         {
          FSQL += __tmp;
          ACountSQL += __tmp;
         }
        else
         {
          FSQL += " and " + __tmp;
          ACountSQL += " and " + __tmp;
         }
       }
     }
    if (AOrderList)
     {
      UnicodeString tmpOrd = "";
      if (AOrderList->Count)
       {
        tmpOrd = " Order By " + AOrderList->Strings[0];
        for (int i = 1; i < AOrderList->Count; i++)
         {
          tmpOrd += ", " + AOrderList->Strings[i];
         }
        FSQL += tmpOrd;
       }
     }
   }
  __finally
   {
    if (FTabs)
     delete FTabs;
   }
  return FSQL;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::GetPeriodVals(UnicodeString ABaseISRef, TDateTime AFr, TDateTime ATo)
 {
  TTagNode * FIS = FXMLS->GetRefer(ABaseISRef);
  TTagNode * perIS, *perISFl, *tmp;
  UnicodeString RC = "";
  UnicodeString TabAlias = GetTabAlias(ABaseISRef);
  if (FIS && ((int)AFr != 0) && ((int)ATo != 0))
   {
    TTagNode * PerFl = FIS->GetRoot()->GetChildByName("periodfields", true);
    if (PerFl)
     {
      perIS = PerFl->GetFirstChild();
      while (perIS)
       {
        if (FIS == FIS->GetTagByUID(perIS->AV["ref"]))
         {//���� ��� �������� ����������� ������ ����������� �������� ABaseISRef
          perISFl = perIS->GetFirstChild();
          while (perISFl)
           {
            tmp = FIS->GetTagByUID(perISFl->AV["ref"]);
            if (RC == "")
             RC = TabAlias + "." + tmp->AV["fields"] + ">='" + DateFS(AFr) + "' and " + TabAlias + "." + tmp->AV["fields"] + "<='" + DateFS(ATo) + "'";
            else
             RC += " and " + TabAlias + "." + tmp->AV["fields"] + ">='" + DateFS(AFr) + "' and " + TabAlias + "." + tmp->AV["fields"] + "<='" + DateFS(ATo) + "'";
            perISFl = perISFl->GetNext();
           }
         }
        perIS = perIS->GetNext();
       }
     }
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDocSQLCreator::CreateWhere(TTagNode * ANode, TStringList * ATabList, UnicodeString & AWhereList, UnicodeString ABaseISRef)
 {
  bool RC = true;
  AWhereList = CreateWhere(ANode, ATabList, RC, ABaseISRef);
  return RC;
 }
//---------------------------------------------------------------------------
#define _NOT_ ANode->CmpAV("not","1")
UnicodeString __fastcall TdsDocSQLCreator::CreateWhere(TTagNode * ANode, TStringList * ATabList, bool & ARC, UnicodeString ABaseISRef)
 {
  //FAlPr = 0;
  TTagNode * __tmp;
  if (ABaseISRef != "")
   FFBaseISRef = ABaseISRef;
  UnicodeString FWhere = "";
  bool IsBracket = false;
  if (ANode->CmpName("condrules,rand,ror"))
   {
    if (_NOT_)
     {
      FWhere += " NOT (";
      IsBracket = true;
     }
    if (ANode->GetChildByName("IERef") || ((ANode->Count > 1) && ANode->CmpName("rand,ror")))
     {
      if (!IsBracket)
       {
        FWhere += " (";
        IsBracket = true;
       }
     }
    FWhere += CreateWhereGroup(ANode, ATabList, ARC, FFBaseISRef);
    if (!ARC)
     return "";
    if (IsBracket)
     FWhere += ") ";
   }
  else
   {
    dsLogError(cFMT(& _dsDocSQLCreatorErrorCondStructure), __FUNC__);
    ARC = false;
   }
  return FWhere;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::CreateWhereGroup(TTagNode * ANode, TStringList * ATabList, bool & ARC, UnicodeString ABaseISRef)
 {
  TTagNode * iNode, *tmp, *IS;
  UnicodeString FWhere = "";
  UnicodeString tmpWhere;
  UnicodeString FOper;
  TReplaceFlags rFlag;
  rFlag << rfReplaceAll;

  if (ANode->CmpName("ror"))
   FOper = " OR ";
  else if (ANode->CmpName("rand"))
   FOper = " AND ";
  else
   {
    if (ANode->CmpAV("uniontype", "and"))
     FOper = " AND ";
    else
     FOper = " OR ";
   }
  TTagNode * sGroup = NULL;
  try
   {
    //����������� � ���������� ����������� ���������
    sGroup = new TTagNode(NULL);
    iNode  = ANode->GetFirstChild();
    UnicodeString BaseISTabKey = GetISTabKey(ABaseISRef);
    while (iNode)
     {
      if (iNode->CmpName("rand,ror"))
       {//������
        tmp = sGroup->GetChildByName("groups");
        if (!tmp)
         tmp = sGroup->AddChild("groups");
        tmp->AddChild("tmp")->Assign(iNode, true);
       }
      else if (iNode->CmpName("IERef"))
       {//�������������� �������

        IS = FXMLS->GetRefer(iNode->AV["ref"]);
        if (!IS)
         {
          if (iNode->GetParent("trow"))
           throw Exception("������ �� ������������� �������, ���='" + iNode->AV["ref"] + "', ������ - '" + iNode->GetParent("trow")->AV["name"] + "'");
          else if (iNode->GetParent("tcol"))
           throw Exception("������ �� ������������� �������, ���='" + iNode->AV["ref"] + "', ������� - '" + iNode->GetParent("tcol")->AV["name"] + "'");
          else if (iNode->GetParent("listcol"))
           throw Exception("������ �� ������������� �������, ���='" + iNode->AV["ref"] + "', ������� - '" + iNode->GetParent("listcol")->AV["name"] + "'");
          else
           throw Exception("������ �� ������������� �������, ���='" + iNode->AV["ref"] + "'");
         }
        IS = IS->GetParent("IS");
        tmp = sGroup->GetChildByAV("ISRef", "ref", IS->AV["uid"]);
        if (!tmp)
         {
          tmp                    = sGroup->AddChild("ISRef");
          tmp->AV["ref"]         = IS->AV["uid"];
          tmp->AV["rel_type"]    = IntToStr(GetISRelationsType(ABaseISRef, iNode->AV["ref"]));
          tmp->AV["tabalias"]    = GetTabAlias(IS); //+IntToStr(FAlPr); FAlPr++;
          tmp->AV["tabname"]     = GetTabName(IS);
          tmp->AV["relationkey"] = GetRelationKey(ABaseISRef, iNode->AV["ref"]);
         }
        bool isAddTab = !tmp->CmpAV("rel_type", "2,4"); //��� relation - 1:M, M:M �� ���������
        //������� � From ��������� �������
        tmp = tmp->AddChild("tmp");
        tmp->Assign(iNode, true);
        tmp->AV["where"] = CreateSQLParams(iNode, ATabList, isAddTab);
       }
      iNode = iNode->GetNext();
     }
    //���������� SQL-�
    iNode = sGroup->GetFirstChild();
    while (iNode)
     {
      tmpWhere = "";
      if (iNode->GetPrev())
       FWhere += FOper;
      if (iNode->CmpName("ISRef"))
       {//��������
        tmp = iNode->GetFirstChild();
        while (tmp)
         {
          if (tmp->GetPrev())
           tmpWhere += FOper;
          if (tmp->CmpAV("not", "1"))
           tmpWhere += " NOT (" + tmp->AV["where"] + ") ";
          else
           tmpWhere += tmp->AV["where"];
          tmp = tmp->GetNext();
         }
        if (iNode->CmpAV("rel_type", "2,4"))//relation type - 1:M, M:M
         {
          tmpWhere = StringReplace(tmpWhere, iNode->AV["tabalias"], "REF1_M", rFlag);
          FWhere += BaseISTabKey + " in (Select REF1_M." + iNode->AV["relationkey"];
          FWhere += " From " + iNode->AV["tabname"] + " REF1_M Where ";
          FWhere += tmpWhere + " and REF1_M." + iNode->AV["relationkey"];
          FWhere += "=" + BaseISTabKey + ")";
         }
        else //relation type - 1:1, M:1
             FWhere += tmpWhere;
       }
      else if (iNode->CmpName("groups"))
       {//������ �����
        tmp = iNode->GetFirstChild();
        while (tmp)
         {
          if (tmp->GetPrev())
           FWhere += FOper;
          FWhere += CreateWhere(tmp, ATabList, ARC, ABaseISRef);
          tmp = tmp->GetNext();
         }
       }
      iNode = iNode->GetNext();
     }
   }
  __finally
   {
    if (sGroup)
     delete sGroup;
   }
  return FWhere;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::CreateSQLParams(TTagNode * AIERef, TStringList * ATabList, bool IsAddTab)
 {
  //������� - ��������� ��� ���� (�����)
  //"tabalias"."field1"="��������" and(or) ... "tabalias"."fieldN"="��������"
  //���� ����� > 1  ��������� ������ � ������
  //������ - (CB.NAME='����' and CB.FAM='�������')
  //CB.NAME='�����'
  //
  //� ATabList - ���������� ������ �� �������� ������� ����������� �������
  //� ������ � "join"  - ���� ������� ��������� (�������� �����)
  //������ - GUI.UID:join
  //������   40381E23-92155860-4448.0000:
  //40381E23-92155860-4448.0028:join address addr on (addr.code=cb.addr)
  //40381E23-92155860-4448.0000:join org on (org.code=cb.org) join typeorg torg
  //on (torg.code=org.torg)
  UnicodeString RC = "";
  try
   {
    if (FOnGetSVal)
     {
      UnicodeString FDocPerDate = ";";
      if (((int)PerFr) && ((int)PerTo))
       FDocPerDate = PerFr.FormatString("dd.mm.yyyy") + ";" + PerTo.FormatString("dd.mm.yyyy");
      else if (((int)PerFr))
       FDocPerDate = PerFr.FormatString("dd.mm.yyyy") + ";";
      else if (((int)PerTo))
       FDocPerDate = ";" + PerTo.FormatString("dd.mm.yyyy");
      FDocPerDate += "#" + IntToStr(FPerType);
      TTagNode * FIE = FXMLS->GetRefer(AIERef->AV["ref"]);
      if (FIE)
       {
        UnicodeString _Proc_ = "SQL" + FIE->AV["procname"];
        UnicodeString fRC = "";
        UnicodeString fTab = "";
        try
         {
          try
           {
            fRC = FDocPerDate;
            dsLogMessage(_Proc_,__FUNC__,1);
            FOnGetSVal(_Proc_, AIERef, fTab, fRC);
            RC = fRC;
            dsLogMessage(RC,__FUNC__,1);
            if (IsAddTab)
             {
              if (ATabList->IndexOf(fTab) == -1)
               ATabList->Add(fTab);
             }
           }
          catch (Exception & E)
           {
            dsLogError(cFMT2(& _dsDocSQLCreatorErrorFuncMissing, _Proc_, E.Message), __FUNC__);
           }
         }
        __finally
         {
         }
       }
      else
       {
        dsLogError(cFMT2(& _dsDocSQLCreatorErrorFuncMissing, "�� �������", "����������� �������� IE, ������:\"" + AIERef->AV["ref"] + "\""), __FUNC__);
       }
     }
    else
     dsLogError("�� ����� ���������� OnGetSVal", __FUNC__);
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
TRelationsType __fastcall TdsDocSQLCreator::GetISRelationsType(UnicodeString ABaseISref, UnicodeString AIERef)
 {
  //������� - ��� ����� ����� ���������� ABaseISref -> AIERef
  //Rel_None = 0, Rel_1_1 = 1,  Rel_1_M = 2,  Rel_M_1 = 3, Rel_M_M = 4
  TRelationsType RC = Rel_None;
  TTagNode * FIE = FXMLS->GetRefer(AIERef);
  if (FIE)
   {
    TTagNode * FBaseIS = FXMLS->GetRefer(ABaseISref);
    TTagNode * FRefIS = FIE->GetParent("IS");
    if (FBaseIS && FRefIS)
     {//��� �������� �������
      TTagNode * FBaseISLinks = FBaseIS->GetChildByName("refers");
      if (FBaseISLinks)
       {
        FBaseISLinks = FBaseISLinks->GetFirstChild();
        UnicodeString tmpRef = GetRefByType(_GUI(ABaseISref), GetSpecGUI(FRefIS) + "." + FRefIS->AV["uid"]);
        while (FBaseISLinks)
         {
          if (FBaseISLinks->CmpAV("ref", tmpRef))
           {
            if (FBaseISLinks->CmpAV("typerelation", "11"))
             RC = Rel_1_1;
            else if (FBaseISLinks->CmpAV("typerelation", "1M"))
             RC = Rel_1_M;
            else if (FBaseISLinks->CmpAV("typerelation", "M1"))
             RC = Rel_M_1;
            else if (FBaseISLinks->CmpAV("typerelation", "MM"))
             RC = Rel_M_M;
            //MM - � DTD �����������
            //����� � ���� ����������������
            //�������� �������� ������ ����� ��������� ���������
            //� �� ������ �������������
            break;
           }
          FBaseISLinks = FBaseISLinks->GetNext();
         }
       }

     }
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::GetRelationKey(UnicodeString ABaseISref, UnicodeString AIERef)
 {
  UnicodeString RC = "";
  TTagNode * FIE = FXMLS->GetRefer(AIERef);
  if (FIE)
   {
    TTagNode * FBaseIS = FXMLS->GetRefer(ABaseISref);
    TTagNode * FRefIS = FIE->GetParent("IS");
    if (FBaseIS && FRefIS)
     {//��� �������� �������
      TTagNode * FBaseISLinks = FBaseIS->GetChildByName("refers");
      if (FBaseISLinks)
       {
        FBaseISLinks = FBaseISLinks->GetFirstChild();
        UnicodeString tmpRef = GetRefByType(_GUI(ABaseISref), GetSpecGUI(FRefIS) + "." + FRefIS->AV["uid"]);
        while (FBaseISLinks)
         {
          if (FBaseISLinks->CmpAV("ref", tmpRef))
           {
            RC = FBaseISLinks->GetFirstChild()->AV["fieldout"];
            break;
           }
          FBaseISLinks = FBaseISLinks->GetNext();
         }
       }

     }
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::CreateFrom(UnicodeString ABaseISRef, TStringList * ATabList, bool * IsDist, UnicodeString AKeyVal, bool * IsAddKey)
 {
  //������� - ��������� ��� From
  TStringList * FTabList = NULL;
  TStringList * FUsedIS = NULL; //������ ������������ ���������
  UnicodeString FFrom;
  try
   {
    FUsedIS  = new TStringList;
    FTabList = new TStringList;
    UnicodeString FISRef, tmp, tmpJoin, tmpRef, FTabName, FTabAlias;
    UnicodeString FBaseTabAlias = GetTabAlias(ABaseISRef);
    int ind;
    TTagNode * itLink, *itKey, *tmpLink;
    TTagNode * FBaseIS = FXMLS->GetRefer(ABaseISRef);
    TTagNode * FISLinks = FBaseIS->GetChildByName("refers");
    FFrom = GetTabName(ABaseISRef) + " " + FBaseTabAlias;
    //������ ������ ������ �� ��������, ��������� "join"
    for (int i = 0; i < ATabList->Count; i++)
     {
      FISRef = GetPart1(ATabList->Strings[i], ':');
      if (_wcscmpi(ABaseISRef.c_str(), FISRef.c_str()) == 0)
       {
        if (GetPart2(ATabList->Strings[i], ':') != "")
         FFrom += " " + GetPart2(ATabList->Strings[i], ':');
       }
      else
       {
        ind = FTabList->IndexOfName(FISRef);
        if (ind != -1)
         FTabList->Values[FISRef] = FTabList->Values[FISRef] + " " + GetPart2(ATabList->Strings[i], ':');
        else
         FTabList->Add((FISRef + "=" + GetPart2(ATabList->Strings[i], ':')));
       }
     }
    //FUsedIS->Assign(FTabList);
    //������ ��������� ��� "From"
    for (int i = 0; i < FTabList->Count; i++)
     {
      FISRef    = FTabList->Names[i];
      FTabName  = GetTabName(FISRef);
      FTabAlias = GetTabAlias(FISRef);
      tmp       = GetRefByType(_GUI(ABaseISRef), FISRef);
      //���������� ������� ����� ����� ����������
      itLink    = FISLinks->GetChildByAV("refer", "ref", tmp);
      tmpJoin   = FTabList->Values[FTabList->Names[i]].Trim();
      *IsAddKey = true;
      if (itLink)
       {//���������� ���������������� ����� ����� ����������
        FFrom += AddLinkKey(itLink, FBaseTabAlias, FTabName, FTabAlias, tmpJoin, IsDist, true, AKeyVal, (FTabList->Count < 2));
        if (FUsedIS->IndexOfName(FISRef) == -1)
         FUsedIS->Add(FISRef + "=");
       }
      else
       {//���������������� ����� ����� ���������� �����������
        TStringList * FLinkPath = new TStringList;
        GetISLinkPath(FISLinks, FISRef, FLinkPath);
        for (int j = FLinkPath->Count - 1; j >= 0; j--)
         {
          tmpLink   = (TTagNode *)FLinkPath->Objects[j];
          tmpRef    = PrepareRef(tmpLink, tmpLink->AV["ref"]);
          FTabName  = GetTabName(tmpRef);
          FTabAlias = GetTabAlias(tmpRef);
          tmp       = tmpLink->GetParent("is")->AV["tabalias"];
          if (FUsedIS->IndexOfName(tmpRef) != -1)
           continue;
          FFrom += AddLinkKey(tmpLink, tmp, FTabName, FTabAlias, tmpJoin, IsDist, (i == 0), AKeyVal, (i == FTabList->Count - 1));
          FUsedIS->Add(tmpRef + "=");
         }
        delete FLinkPath;
       }
     }
   }
  __finally
   {
    if (FTabList)
     delete FTabList;
    if (FUsedIS)
     delete FUsedIS;
   }
  return FFrom;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::AddLinkKey(TTagNode * ALink, UnicodeString ABaseTabName, UnicodeString ATabName, UnicodeString ATabAlias, UnicodeString AJoin, bool * AIsDist,
    bool AIsAddJoin, UnicodeString AKeyVal, bool AIsAddKey)
 {
  UnicodeString FFrom = "";
  UnicodeString lBr, rBr, cOper;
  lBr   = "";
  rBr   = "";
  cOper = " and ";
  TTagNode * itKey;
  //��������� ������� ������ ����� ����� 1:�
  *AIsDist |= (ALink->AV["typerelation"].UpperCase() == "1M");
  if ((AJoin != "") && AIsAddJoin)
   {//�������������� "join"�  ������������
    FFrom += " join (" + ATabName + " " + ATabAlias + " " + AJoin + ")";
   }
  else
   {//�������������� "join"�  �����������
    FFrom += " join " + ATabName + " " + ATabAlias;
   }
  //����������� ����� ���������
  FFrom += " on (";
  if (ALink->AV["keylink"].LowerCase() == "or")
   {
    lBr   = "(";
    rBr   = ")";
    cOper = " or ";
   }
  itKey = ALink->GetFirstChild();
  FFrom += lBr + ATabAlias + "." + itKey->AV["fieldout"] + "=";
  FFrom += ABaseTabName + "." + itKey->AV["fieldin"];
  itKey = itKey->GetNext();
  while (itKey)
   {
    FFrom += cOper + ATabAlias + "." + itKey->AV["fieldout"] + "=";
    FFrom += ABaseTabName + "." + itKey->AV["fieldin"];
    itKey = itKey->GetNext();
   }
  FFrom += rBr;
  if (AIsAddKey && (AKeyVal != ""))
   FFrom += " AND " + AKeyVal;
  FFrom += ")";
  return FFrom;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocSQLCreator::GetISLinkPath(TTagNode * ALinks, UnicodeString AISRef, TStringList * ATabList)
 {
  TTagNode * itNode = ALinks->GetFirstChild();
  TStringList * FSearchPath;
  try
   {
    FSearchPath = new TStringList;
    //��������� ������ �� �������� � ������ ����������� ����
    FSearchPath->Add(GetSpecGUI(ALinks) + "." + ALinks->GetParent("is")->AV["uid"]);
    while (itNode)
     {
      //�������� � ��� �������� �������� � �����
      if (FGetISLinkPath(itNode->GetRefer("ref")->GetChildByName("refers"), AISRef, FSearchPath, ATabList))
       {
        ATabList->AddObject("", (TObject *)itNode);
        return;
       }
      itNode = itNode->GetNext();
     }
   }
  __finally
   {
    if (FSearchPath)
     delete FSearchPath;
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDocSQLCreator::FGetISLinkPath(TTagNode * ALinks, UnicodeString AISRef, TStringList * ASearchPath, TStringList * ATabList)
 {
  if (!ALinks)
   return false; //�������� � ������ ��������� � �� ��������
  TTagNode * itNode = ALinks->GetFirstChild();
  //��������� ������ �� �������� � ������ ����������� ����
  ASearchPath->Add(GetSpecGUI(ALinks) + "." + ALinks->GetParent("is")->AV["uid"]);

  UnicodeString tmp = GetRefByType(GetSpecGUI(itNode), AISRef);
  while (itNode)
   {
    //��������������� ���������� ������� ������ ����� ����������
    if (itNode->CmpAV("ref", tmp))
     {//��� !!! �����
      ATabList->AddObject("", (TObject *)itNode);
      return true;
     }
    if (ASearchPath->IndexOf(PrepareRef(itNode, itNode->AV["ref"])) == -1)
     {//���� ��� ��� �� ��������, ������ ��������� :)
      if (FGetISLinkPath(itNode->GetRefer("ref")->GetChildByName("refers"), AISRef, ASearchPath, ATabList))
       {
        ATabList->AddObject("", (TObject *)itNode);
        return true;
       }
     }
    itNode = itNode->GetNext();
   }
  return false;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::PrepareRef(TTagNode * ANode, UnicodeString AISRef)
 {//��������� GUI  � ������ ����� �� �����������
  if (AISRef.Pos("."))
   return AISRef;
  else
   return GetSpecGUI(ANode) + "." + AISRef;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::ReplaceKey(UnicodeString ASQL, UnicodeString AKey)
 {//�������� � SQL-��������� �������� _KEYVALUE_ �� �������� �����
  TReplaceFlags rFlag;
  rFlag << rfReplaceAll;
  return StringReplace(ASQL, "_KEYVALUE_", AKey, rFlag);
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::GetRefByType(UnicodeString ABaseGUI, UnicodeString AISRef)
 {
  if (_wcscmpi(ABaseGUI.c_str(), _GUI(AISRef).c_str()) == 0)
   {//�������� � ����� ������������, ���� �� ������� ������
    return _UID(AISRef);
   }
  else
   {//�������� � ������ �������������, ���� �� ������ ������
    return AISRef;
   }
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::GetTabAlias(TTagNode * Aattr)
 {
  TTagNode * FIS = Aattr;
  if (FIS)
   {
    if (!FIS->CmpName("IS"))
     FIS = FIS->GetParent("is");
    if (FIS)
     return FIS->AV["tabalias"];
    else
     return "";
   }
  else
   return "";
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::GetTabAlias(UnicodeString AISRef)
 {
  return GetTabAlias(FXMLS->GetRefer(AISRef));
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::GetTabName(TTagNode * Aattr)
 {
  UnicodeString RC = "";
  try
   {
    TTagNode * FIS = Aattr;
    if (FIS)
     {
      if (!FIS->CmpName("IS"))
       FIS = FIS->GetParent("is");
      if (FIS)
       {
        RC = FIS->AV["maintable"];
        if (RC.UpperCase().Pos("CLASS_"))
         RC = "CLASS_" + FTablePref + RC.SubString(7, RC.Length() - 6);
        else if (RC.UpperCase().Pos("CARD_"))
         RC = "CARD_" + FTablePref + RC.SubString(6, RC.Length() - 5);
        else
         RC = FTablePref + RC;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::GetTabName(UnicodeString AISRef)
 {
  return GetTabName(FXMLS->GetRefer(AISRef));
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::GetISTabKey(TTagNode * AIS, bool AAddAlias)
 {
  TTagNode * FIS = AIS;
  if (FIS)
   {
    if (!FIS->CmpName("IS"))
     FIS = FIS->GetParent("is");
    if (AAddAlias)
     return FIS->AV["tabalias"] + "." + FIS->AV["keyfield"];
    else
     return FIS->AV["keyfield"];
   }
  else
   return "";
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::GetISTabKey(UnicodeString AISRef, bool AAddAlias)
 {
  return GetISTabKey(FXMLS->GetRefer(AISRef), AAddAlias);
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocSQLCreator::DateFS(TDateTime ADate)
 {
  UnicodeString RC = ADate.FormatString("dd.mm.yyyy");
  try
   {
    if (FOnGetDateStringFormat)
     {
      FOnGetDateStringFormat(RC);
      RC = ADate.FormatString(RC);
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
