/*
  ���������:

  29.06.2004 - ������� �.�.
        ��������� �������� ������������ ���� Wordconst

        ������ ������� � ����� MICSComp

        ��������� ����������
                  #ifndef WordConstH
                  #define WordConsrH
                  ...
                  #endif

        � ���� ���������� ��������
                #pragma hdrstop

                #include "WordConst.h"

                #pragma package(smart_init)

*/

//---------------------------------------------------------------------------

#ifndef IcsXMLDocConstH
#define IcsXMLDocConstH

namespace Icsxmldocconst
{

// Color index
#define wAuto 0
#define wBlack 1
#define wBlue 2
#define wTurquoise 3
#define wBrightGreen 4
#define wPink 5
#define wRed 6
#define wYellow 7
#define wWhite 8
#define wDarkBlue 9
#define wTeal 10
#define wGreen 11
#define wViolet 12
#define wDarkRed 13
#define wDarkYellow 14
#define wGray50 15
#define wGray25 16
#define wByAuthor 0xFFFFFFFF
#define wNoHighlight 0

// Horizontal align
#define halLeft 0
#define halCenter 1
#define halRight 2
#define halJustify 3

// Vertical align
#define valTop 0
#define valCenter 1
#define valJustify 2
#define valBottom 3

//Border ArtStyle
#define  baApples 1
#define  baMapleMuffins 2
#define  baCakeSlice 3
#define  baCandyCorn 4
#define  baIceCreamCones 5
#define  baChampagneBottle 6
#define  baPartyGlass 7
#define  baChristmasTree 8
#define  baTrees 9
#define  baPalmsColor 10
#define  baBalloons3Colors 11
#define  baBalloonsHotAir 12
#define  baPartyFavor 13
#define  baConfettiStreamers 14
#define  baHearts 15
#define  baHeartBalloon 16
#define  baStars3D 17
#define  baStarsShadowed 18
#define  baStars 19
#define  baSun 20
#define  baEarth2 21
#define  baEarth1 22
#define  baPeopleHats 23
#define  baSombrero 24
#define  baPencils 25
#define  baPackages 26
#define  baClocks 27
#define  baFirecrackers 28
#define  baRings 29
#define  baMapPins 30
#define  baConfetti 31
#define  baCreaturesButterfly 32
#define  baCreaturesLadyBug 33
#define  baCreaturesFish 34
#define  baBirdsFlight 35
#define  baScaredCat 36
#define  baBats 37
#define  baFlowersRoses 38
#define  baFlowersRedRose 39
#define  baPoinsettias 40
#define  baHolly 41
#define  baFlowersTiny 42
#define  baFlowersPansy 43
#define  baFlowersModern2 44
#define  baFlowersModern1 45
#define  baWhiteFlowers 46
#define  baVine 47
#define  baFlowersDaisies 48
#define  baFlowersBlockPrint 49
#define  baDecoArchColor 50
#define  baFans 51
#define  baFilm 52
#define  baLightning1 53
#define  baCompass 54
#define  baDoubleD 55
#define  baClassicalWave 56
#define  baShadowedSquares 57
#define  baTwistedLines1 58
#define  baWaveline 59
#define  baQuadrants 60
#define  baCheckedBarColor 61
#define  baSwirligig 62
#define  baPushPinNote1 63
#define  baPushPinNote2 64
#define  baPumpkin1 65
#define  baEggsBlack 66
#define  baCup 67
#define  baHeartGray 68
#define  baGingerbreadMan 69
#define  baBabyPacifier 70
#define  baBabyRattle 71
#define  baCabins 72
#define  baHouseFunky 73
#define  baStarsBlack 74
#define  baSnowflakes 75
#define  baSnowflakeFancy 76
#define  baSkyrocket 77
#define  baSeattle 78
#define  baMusicNotes 79
#define  baPalmsBlack 80
#define  baMapleLeaf 81
#define  baPaperClips 82
#define  baShorebirdTracks 83
#define  baPeople 84
#define  baPeopleWaving 85
#define  baEclipsingSquares2 86
#define  baHypnotic 87
#define  baDiamondsGray 88
#define  baDecoArch 89
#define  baDecoBlocks 90
#define  baCirclesLines 91
#define  baPapyrus 92
#define  baWoodwork 93
#define  baWeavingBraid 94
#define  baWeavingRibbon 95
#define  baWeavingAngles 96
#define  baArchedScallops 97
#define  baSafari 98
#define  baCelticKnotwork 99
#define  baCrazyMaze 100
#define  baEclipsingSquares1 101
#define  baBirds 102
#define  baFlowersTeacup 103
#define  baNorthwest 104
#define  baSouthwest 105
#define  baTribal6 106
#define  baTribal4 107
#define  baTribal3 108
#define  baTribal2 109
#define  baTribal5 110
#define  baXIllusions 111
#define  baZanyTriangles 112
#define  baPyramids 113
#define  baPyramidsAbove 114
#define  baConfettiGrays 115
#define  baConfettiOutline 116
#define  baConfettiWhite 117
#define  baMosaic 118
#define  baLightning2 119
#define  baHeebieJeebies 120
#define  baLightBulb 121
#define  baGradient 122
#define  baTriangleParty 123
#define  baTwistedLines2 124
#define  baMoons 125
#define  baOvals 126
#define  baDoubleDiamonds 127
#define  baChainLink 128
#define  baTriangles 129
#define  baTribal1 130
#define  baMarqueeToothed 131
#define  baSharksTeeth 132
#define  baSawtooth 133
#define  baSawtoothGray 134
#define  baPostageStamp 135
#define  baWeavingStrips 136
#define  baZigZag 137
#define  baCrossStitch 138
#define  baGems 139
#define  baCirclesRectangles 140
#define  baCornerTriangles 141
#define  baCreaturesInsects 142
#define  baZigZagStitch 143
#define  baCheckered 144
#define  baCheckedBarBlack 145
#define  baMarquee 146
#define  baBasicWhiteDots 147
#define  baBasicWideMidline 148
#define  baBasicWideOutline 149
#define  baBasicWideInline 150
#define  baBasicThinLines 151
#define  baBasicWhiteDashes 152
#define  baBasicWhiteSquares 153
#define  baBasicBlackSquares 154
#define  baBasicBlackDashes 155
#define  baBasicBlackDots 156
#define  baStarsTop 157
#define  baCertificateBanner 158
#define  baHandmade1 159
#define  baHandmade2 160
#define  baTornPaper 161
#define  baTornPaperBlack 162
#define  baCouponCutoutDashes 163
#define  baCouponCutoutDots 164

// border LineStyle
#define  lsNone                  0
#define  lsSingle                1
#define  lsDot                   2
#define  lsDashSmallGap          3
#define  lsDashLargeGap          4
#define  lsDashDot               5
#define  lsDashDotDot            6
#define  lsDouble                7
#define  lsTriple                8
#define  lsThinThickSmallGap     9
#define  lsThickThinSmallGap     10
#define  lsThinThickThinSmallGap 11
#define  lsThinThickMedGap       12
#define  lsThickThinMedGap       13
#define  lsThinThickThinMedGap   14
#define  lsThinThickLargeGap     15
#define  lsThickThinLargeGap     16
#define  lsThinThickThinLargeGap 17
#define  lsSingleWavy            18
#define  lsDoubleWavy            19
#define  lsDashDotStroked        20
#define  lsEmboss3D              21
#define  lsEngrave3D             22

// border line width
#define  lw025pt 2
#define  lw050pt 4
#define  lw075pt 6
#define  lw100pt 8
#define  lw150pt 12
#define  lw225pt 18
#define  lw300pt 24
#define  lw450pt 36
#define  lw600pt 48

} //end of namespace Wordconst
using namespace Icsxmldocconst;

#endif
