
//---------------------------------------------------------------------------
#include <vcl.h>
//#include <stdlib.h>
//#include <WINPERF.H>
//#include <windows.h>
//#include <stdio.h>
//#include <string.h>
#include <ClipBrd.hpp>

#pragma hdrstop

#include "IcsXMLDoc.h"
#include "ExtUtils.h"
#include "IcsXMLDocConst.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
__fastcall EdsXMLError::EdsXMLError(UnicodeString AMsg):Exception(AMsg)
{
}
//---------------------------------------------------------------------------
__fastcall TXMLDoc::TXMLDoc()
     : TObject()
{
  FDoc = NULL;
  FIsOpen = false;
  FDocName = "temp";
}
//---------------------------------------------------------------------------
__fastcall TXMLDoc::~TXMLDoc()
{/*if (!FDoc.IsEmpty()&&FIsOpen) Close();*/
}
//---------------------------------------------------------------------------
bool __fastcall TXMLDoc::Open(UnicodeString fName)
{
  bool RC = false;
  try
   {
     try
      {
        if (!FileExists(fName))
         {
           throw EdsXMLError("������ �������� �������. ����: \""+fName+"\" ����������� !!!");
         }
        else
         {
           Application->ProcessMessages();
           if (!FDoc) FDoc = new TTagNode(NULL);
           FDoc->LoadFromXMLFile(fName);
           FHead = FDoc->GetChildByName("head");
           FBody = FDoc->GetChildByName("body");
           FPageSetup = FHead->GetChildByName("pagesetup");
           if (!(FHead&&FBody))
            {
              if (FDoc) delete FDoc; FDoc = NULL;
              throw EdsXMLError("������ �������� �������. ������������ ��������� �������");
            }
           else
            {
              Application->ProcessMessages();
              FDocName = fName;
              FIsOpen = true;
              RC = true;
            }
         }
      }
     catch (Exception &E)
      {
        throw EdsXMLError("������ �������� ���������. ����: \""+fName+"\".\n��������� ���������:\n"+E.Message);
      }
     catch (...)
      {
        throw EdsXMLError("������ �������� ���������. ����: \""+fName+"\"");
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TXMLDoc::SetTitle(UnicodeString ATitle)
{
  TTagNode *FTitleNode = FDoc->GetChildByName("title",true);
  if (!FTitleNode)
   {
     FTitleNode = FDoc->GetChildByName("head");
     if (FTitleNode) FTitleNode = FTitleNode->AddChild("title");
     else
      {
        FTitleNode = FDoc->GetChildByName("body");
        if (FTitleNode)
         {
           FTitleNode = FTitleNode->Insert("head");
           FTitleNode = FTitleNode->AddChild("title");
         }
      }
   }
  if (FTitleNode)
   FTitleNode->AV["PCDATA"] = ATitle;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TXMLDoc::GetTitle()
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FTitleNode = FDoc->GetChildByName("title",true);
     if (FTitleNode)
      RC = FTitleNode->AV["PCDATA"];
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TXMLDoc::Load(TTagNode *ASrc)
{
  bool RC = false;
  try
   {
     try
      {
        if (!ASrc)
         {
           throw EdsXMLError("������ �������� �������. ����������� �������� ������ !!!");
         }
        else
         {
           Application->ProcessMessages();
           if (!FDoc) FDoc = new TTagNode(NULL);
           FDoc->Assign(ASrc, true);
           FHead = FDoc->GetChildByName("head");
           FBody = FDoc->GetChildByName("body");
           FPageSetup = FHead->GetChildByName("pagesetup");
           if (!(FHead&&FBody))
            {
              if (FDoc) delete FDoc; FDoc = NULL;
              throw EdsXMLError("������ �������� �������. ������������ ��������� �������.");
            }
           else
            {
              Application->ProcessMessages();
              FDocName = "temp";
              FIsOpen = true;
              RC = true;
            }
         }
      }
     catch (Exception &E)
      {
        throw EdsXMLError("������ �������� ���������.\n��������� ���������:\n"+E.Message);
      }
     catch (...)
      {
        throw EdsXMLError("������ �������� ���������.");
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
int __fastcall TXMLDoc::GetTM()
{
  if (FPageSetup) return (FPageSetup->AV["topfield"].ToIntDef(0)/100.0);
  else            return 0;
}
//---------------------------------------------------------------------------
int __fastcall TXMLDoc::GetLM()
{
  if (FPageSetup) return (FPageSetup->AV["leftfield"].ToIntDef(0)/100.0);
  else            return 0;
}
//---------------------------------------------------------------------------
int __fastcall TXMLDoc::GetRM()
{
  if (FPageSetup) return (FPageSetup->AV["rightfield"].ToIntDef(0)/100.0);
  else            return 0;
}
//---------------------------------------------------------------------------
int __fastcall TXMLDoc::GetBM()
{
  if (FPageSetup) return (FPageSetup->AV["bottomfield"].ToIntDef(0)/100.0);
  else            return 0;
}
//---------------------------------------------------------------------------
int __fastcall TXMLDoc::GetPO()
{
  if (FPageSetup) return (FPageSetup->AV["orientation"].ToIntDef(0));
  else            return 0;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall GetChildByIDX(TTagNode *Src, UnicodeString ANodeName, int AInd)
{
  TTagNode *tmpTab = NULL;
  TTagNode *itNode = Src->GetChildByName(ANodeName);
  int idx = 0;
  while (itNode)
   {
     if (itNode->CmpName(ANodeName)) idx++;
     if (idx == AInd)
      {
        tmpTab = itNode;
        break;
      }
     itNode = itNode->GetNext();
   }
  return (TXMLTab*)tmpTab;
}
//---------------------------------------------------------------------------
TXMLTab* __fastcall TXMLDoc::GetTab(int AInd)
{
  TTagNode *tmpTab = GetChildByIDX(FBody,"table",AInd);
  return (TXMLTab*)tmpTab;
}
//---------------------------------------------------------------------------
void __fastcall TXMLDoc::DeleteTab(int AInd)
{
  try
   {
     TTagNode *tmpTab = GetChildByIDX(FBody,"table",AInd);
     if (tmpTab)  delete tmpTab;
   }
  catch (Exception &E)
   {
     throw EdsXMLError("������ �������� �������. ������ = "+IntToStr((int)AInd)+"\n��������� ���������:\n"+E.Message);
   }
  catch (...)
   {
     throw EdsXMLError("������ �������� �������. ������ = "+IntToStr((int)AInd));
   }
}
//---------------------------------------------------------------------------
void __fastcall TXMLDoc::InsertFile(UnicodeString AFileName)
{
/*
  try
   {
     if (!FileExists(AFileName))
      {
        MessageBox(NULL,("���� \""+AFileName+"\" ����������� !!!").c_str(),"������ ������� �����",0);
        return;
      }
     PropertyGet  GetSents("Sentences");
     PropertyGet  GetLastSent("Last");
     Function     FInsertFile("InsertFile");
     FInsertFile << OleStr(AFileName);
     FDoc.Exec(GetSents).Exec(GetLastSent).Exec(FInsertFile);
     Application->ProcessMessages();
   }
  catchAll("���� \""+AFileName+"\"","������ ������� �����")
*/
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TXMLDoc::GetAPILastErrorFormatMessage( int &nErrorCode )
{
  UnicodeString ErrMsg;
  LPVOID lpMsgBuf;
  nErrorCode = GetLastError();
  FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER |
                 FORMAT_MESSAGE_FROM_SYSTEM |
                 FORMAT_MESSAGE_IGNORE_INSERTS,
                 NULL,
                 nErrorCode,
                 MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                 (LPTSTR) &lpMsgBuf,
                 0,
                 NULL
  );
  ErrMsg = static_cast<LPCTSTR>(lpMsgBuf);
  LocalFree( lpMsgBuf );
  return ErrMsg;
}
//---------------------------------------------------------------------------
void __fastcall TXMLDoc::ExtPreview(UnicodeString fName)
{
}
//---------------------------------------------------------------------------
void __fastcall TXMLDoc::Preview(UnicodeString fName, UnicodeString ACaption, bool AOnSaveAs, int AHelpContext, bool AShowExt)
{
}
//---------------------------------------------------------------------------
__fastcall TXMLTab::TXMLTab(TTagNode *ASrc): TTagNode(ASrc)
{
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::Delete()
{
  try
   {
     delete this;
   }
  catch (Exception &E)
   {
     throw EdsXMLError("������ �������� �������.\n��������� ���������:\n"+E.Message);
   }
  catch (...)
   {
     throw EdsXMLError("������ �������� �������.");
   }
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::Paste(int ARow, int ACol, UnicodeString APref)
{
  try
   {
     if (Clipboard()->HasFormat(CF_TEXT))
      {
        this->SetText(ARow, ACol, Clipboard()->AsText);
      }
     else if (Clipboard()->HasFormat(CF_BITMAP) || Clipboard()->HasFormat(CF_METAFILEPICT) || Clipboard()->HasFormat(CF_PICTURE))
      {
        TClipboard *pCB = Clipboard();
        TPicture *tmpPict = NULL;
        UnicodeString tmpPath = "";
        bool FSet = false;
        double picWidth = 0;
        double picHeight = 0;
        double ppI = Screen->PixelsPerInch;
        try
         {
           TPicture *tmpPict = new TPicture;
           tmpPict->LoadFromClipboardFormat(CF_BITMAP, pCB->GetAsHandle(CF_BITMAP), NULL);
           wchar_t szTempPath[ MAX_PATH ];
           szTempPath[0] = '\0';
           ::GetTempPath( MAX_PATH, szTempPath );
           tmpPath = UnicodeString(szTempPath);
           tmpPath = icsPrePath(tmpPath+"\\img_"+APref+TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss")+".bmp");
           tmpPict->SaveToFile(tmpPath);
           picWidth  = tmpPict->Width;
           picHeight = tmpPict->Height;
           TTagNode *PageNode = this->GetRoot()->GetChildByName("pagesetup",true);
           if (PageNode)
            {
              picWidth  = (picWidth/ppI)*25.4;
              picHeight = (picHeight/ppI)*25.4;
              double tmpLF     = PageNode->AV["leftfield"].ToIntDef(0)/100;
              double tmpRF     = PageNode->AV["rightfield"].ToIntDef(0)/100;
              double tmpTF     = PageNode->AV["topfield"].ToIntDef(0)/100;
              double tmpBF     = PageNode->AV["bottomfield"].ToIntDef(0)/100;
              double tmpWidth  = PageNode->AV["width"].ToIntDef(0)/100;
              double tmpHeight = PageNode->AV["height"].ToIntDef(0)/100;

              tmpWidth  = tmpWidth  - tmpLF - tmpRF;
              tmpHeight = tmpHeight - tmpTF - tmpBF;
              if (picWidth > tmpWidth)   picWidth = tmpWidth;
              if (picHeight > tmpHeight) picHeight = tmpHeight;

              picWidth =  (picWidth/25.4)*ppI;
              picHeight = (picHeight/25.4)*ppI;
            }
           FSet = true;
         }
        __finally
         {
           delete tmpPict;
         }
        if (FSet)
         this->SetText(ARow, ACol, "$lt$img src=\""+tmpPath+"\" width=\""+IntToStr((int)picWidth)+"\"$gt$");
      }
   }
  catch (Exception &E)
   {
     throw EdsXMLError("������ ������ ������� �������. ������ = "+IntToStr((int)ARow)+"  ������� = "+IntToStr((int)ACol)+"\n��������� ���������:\n"+E.Message);
   }
  catch (...)
   {
     throw EdsXMLError("������ ������ ������� �������. ������ = "+IntToStr((int)ARow)+"  ������� = "+IntToStr((int)ACol));
   }
}
//---------------------------------------------------------------------------
long __fastcall TXMLTab::Width(int ARow, int ACol)
{
  long RC = 0;
  try
   {
     TTagNode *tmpRow = GetChildByIDX(this,"tr",ARow);
     if (tmpRow)
      {
        TTagNode *tmpCol = GetChildByIDX(tmpRow,"td",ACol);
        if (tmpCol) RC = tmpCol->AV["width"].ToIntDef(0);
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
//��������� ������ ( � point'�� ) ������ ��������� �������
//fWidth      - ������ ������ � point'��
//ARulerStyle - ���������� ����� ��������� ������ ������ ( �� ��������� wdAdjustNone )
void __fastcall TXMLTab::SetWidth( int ARow, int ACol, float fWidth, TXMLRulerStyle ARulerStyle )
{
  try
   {
     TTagNode *tmpRow = GetChildByIDX(this,"tr",ARow);
     if (tmpRow)
      {
        TTagNode *tmpCol = GetChildByIDX(tmpRow,"td",ACol);
        if (tmpCol) tmpCol->AV["width"] = IntToStr((int)fWidth);
      }
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TXMLTab::GetCell(int ARow, int ACol)
{
  TTagNode *RC = NULL;
  try
   {
     TTagNode *tmpRow = GetChildByIDX(this,"tr",ARow);
     if (tmpRow)
      {
        TTagNode *tmpCol = GetChildByIDX(tmpRow,"td",ACol);
        if (tmpCol)
         {
           while (tmpCol->GetFirstChild())
            tmpCol = tmpCol->GetFirstChild();
           if (tmpCol) RC = tmpCol;
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::SetText(int ARow, int ACol, UnicodeString AText)
{
  TTagNode *Cell = GetCell(ARow, ACol);
  try
   {
     if (Cell) Cell->AV["pcdata"] = AText;
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::SetLeftIndent(int ARow, int ACol, float fCentimeters )
{
/*
  try
   {
     float fPoints;

     fPoints = fCentimeters * 28.35; //(1 cm = 28.35 points) - �� ������� �� Visual Basic
                                     //�� ���� ������� ���������� ����� CentimetersToPoints
                                     //������� Application
     PropertySet  SetLI("LeftIndent");
     PropertyGet  ParForm("ParagraphFormat");
     GetCell.ClearArgs();
     FTab.Exec(GetCell << Variant(ARow) << Variant(ACol)).Exec(GetRange).Exec(ParForm).Exec(SetLI << fPoints);
     Application->ProcessMessages();
   }
  catchAll("������ = "+IntToStr((int)ARow)+"  ������� = "+IntToStr((int)ACol)+" �������� "+FloatToStr(fCentimeters)+" ��","������ ��������� �������� ������")
*/
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::SetTexture(int ARow, int ACol, int APercent)
{
  try
   {
     TTagNode *FCell = GetChildByIDX(this,"tr",ARow);
     if (FCell)
      {
        FCell = GetChildByIDX(FCell,"td",ACol);
        if (FCell)
         {
           UnicodeString BgColor = "0";
           BgColor.printf(L"%01X",(int)((100-APercent)*0.15));
           FCell->AV["bgcolor"] = "#"+UnicodeString::StringOfChar(BgColor[1],6);
         }
      }
   }
  catch (Exception &E)
   {
     throw EdsXMLError("������ ��������� �������� �������. ������ "+IntToStr((int)ARow)+" ������� "+IntToStr((int)ACol)+"\n��������� ���������:\n"+E.Message);
   }
  catch (...)
   {
     throw EdsXMLError("������ ��������� �������� �������. ������ "+IntToStr((int)ARow)+" ������� "+IntToStr((int)ACol));
   }
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::SetFontColor(int ARow, int ACol, int AColor)
{
/*
  try
   {
     GetCell.ClearArgs();
     PropertyGet GetFont("Font");
     PropertySet SetColorInd("ColorIndex");
     FTab.Exec(GetCell << Variant(ARow) << Variant(ACol)).Exec(GetRange).Exec(GetFont).Exec(SetColorInd << Variant((int)AColor));
   }
  catchAll("������ = "+IntToStr((int)ARow)+"  ������� = "+IntToStr((int)ACol)+" ���� - "+IntToStr((int)AColor),"������ ��������� ����� ������")
*/
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::SetBold(int ARow, int ACol, bool ABold)
{
  try
   {
     TTagNode *Cell = GetCell(ARow, ACol);
     if (Cell)
      {
        if (ABold)
         {
           if (!Cell->CmpName("b"))
            {
              Cell->AddChild("b")->AV["pcdata"] = Cell->AV["pcdata"];
              Cell->AV["pcdata"] = "";
            }
         }
        else
         {
           if (Cell->CmpName("b"))
            {
              if (Cell->GetParent()->Count > 1)
               {
                 Cell->Name = "span";
               }
              else
               {
                 Cell->GetParent()->AV["pcdata"] = Cell->AV["pcdata"];
                 Cell = Cell->GetParent();
                 Cell->DeleteChild();
               }
            }
         }
      }
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::AddRow(int ABeforeRow, int ACount/*, TProgressBar *prBar*/)
{
  TTagNode *FRow = GetChildByIDX(this,"tr",ABeforeRow);
  for (int i = 0; i < ACount; i++)
    InsertChild(FRow,"tr",true)->Assign(FRow,true);
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::AppendRow(int ACount)
{
  TTagNode *FRow = this->GetLastChild();
  for (int i = 0; i < ACount; i++)
   InsertChild(FRow,"tr",true)->Assign(FRow,true);
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::AddCol(int ABeforeCol, int ACount)
{
/*
  try
   {
     PropertyGet GetCols("Columns");
     Procedure   SetSel("Select");
     Function    FAddCol("Add");
     GetItem.ClearArgs();
     Variant FCols = FTab.Exec(GetCols);
     FCols.Exec(GetItem << Variant(ABeforeCol)).Exec(SetSel);
     for (int i = 0; i < ACount; i++)
      FCols.Exec(FAddCol);
   }
  catchAll("������� = "+IntToStr((int)ABeforeCol)+"  ���������� = "+IntToStr((int)ACount),"������ ���������� ������� ")
*/
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::DeleteRow(int ARow, int ACount)
{
  try
   {
     int FCount = ACount;
     TTagNode *FRow = GetChildByIDX(this,"tr",ARow);
     TTagNode *FDelRow;
     while (FRow && FCount)
      {
        FDelRow = FRow;
        FRow = FRow->GetNext();
        if (FDelRow)
         {
           delete FDelRow;
           FCount--;
         }
      }
   }
  catch (Exception &E)
   {
     throw EdsXMLError("������ �������� �����. ������ = "+IntToStr((int)ARow)+"  ���������� = "+IntToStr((int)ACount)+"\n��������� ���������:\n"+E.Message);
   }
  catch (...)
   {
     throw EdsXMLError("������ �������� �����. ������ = "+IntToStr((int)ARow)+"  ���������� = "+IntToStr((int)ACount));
   }
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::DeleteCol(int ACol, int ACount)
{
/*
  try
   {
     PropertyGet GetCols("Columns");
     Function    FDel("Delete");
     GetItem.ClearArgs();
     Variant FCols = FTab.Exec(GetCols);
     GetItem << Variant(ACol);
     for (int i = 0; i < ACount; i++)
      FCols.Exec(GetItem).Exec(FDel);
   }
  catchAll("������� = "+IntToStr((int)ACol)+"  ���������� = "+IntToStr((int)ACount),"������ �������� �������")
*/
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::Merge(int AFrRow, int AFrCol, int AToRow, int AToCol)
{
/*
  try
   {
     GetCell.ClearArgs();
     Variant ACell = FTab.Exec(GetCell << Variant(AToRow) << Variant(AToCol));
     GetCell.ClearArgs();
     FTab.Exec(GetCell << Variant(AFrRow) << Variant(AFrCol)).Exec(MergeCell << ACell);
   }
  catchAll("� "+IntToStr((int)AFrRow)+":"+IntToStr((int)AFrCol)+" �� "+IntToStr((int)AToRow)+":"+IntToStr((int)AToCol),"������ ����������� �����")
*/
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::MergeCol(int ARow, int AFrCol, int AToCol)
{
  try
   {
     int FColspan = AToCol - AFrCol +1;
     TTagNode *FCell = GetChildByIDX(this,"tr",ARow);
     if (FCell)
      {
        FCell = GetChildByIDX(FCell,"td",AFrCol);
        if (FCell)
         {
           FCell->AV["colspan"] = IntToStr((int)FCell->AV["colspan"].ToIntDef(0)+FColspan);
           TTagNode *itNode = FCell;
           bool AllPCData = true;
           for (int i = 1; i < FColspan; i++)
            {
              if (itNode)
               {
                 if (itNode->Count)
                  {
                    AllPCData = false;
                    break;
                  }
               }
              else break;
              itNode = itNode->GetNext();
            }
           itNode = FCell->GetNext();
           if (!AllPCData && !FCell->Count)
            {
              if (FCell->AV["pcdata"].Length())
               FCell->AddChild("span")->AV["pcdata"] = FCell->AV["pcdata"];
            }
           TTagNode *FDelNode;
           TTagNode *itChildNode;
           for (int i = 1; i < FColspan; i++)
            {
              if (itNode)
               {
                 if (AllPCData)
                  {
                    FCell->AV["pcdata"] = (FCell->AV["pcdata"]+" "+itNode->AV["pcdata"]).Trim();
                  }
                 else
                  {
                    if (itNode->Count)
                     {
                       itChildNode = itNode->GetFirstChild();
                       while (itChildNode)
                        {
                          FCell->AddChild("span")->Assign(itChildNode,true);
                          itChildNode = itChildNode->GetNext();
                        }
                     }
                    else
                     {
                       if (itNode->AV["pcdata"].Length())
                        FCell->AddChild("span")->AV["pcdata"] = itNode->AV["pcdata"];
                     }
                  }
               }
              else
               break;
              FDelNode = itNode;
              itNode = itNode->GetNext();
              delete FDelNode;
            }
         }
      }
   }
  catch (Exception &E)
   {
     throw EdsXMLError("������ ����������� �������. ������ "+IntToStr((int)ARow)+" ������� � "+IntToStr((int)AFrCol)+" �� "+IntToStr((int)AToCol)+"\n��������� ���������:\n"+E.Message);
   }
  catch (...)
   {
     throw EdsXMLError("������ ����������� �������. ������ "+IntToStr((int)ARow)+" ������� � "+IntToStr((int)AFrCol)+" �� "+IntToStr((int)AToCol));
   }
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::MergeRow(int ACol, int AFrRow, int AToRow)
{
/*
  try
   {
     GetCell.ClearArgs();
     Variant ACell = FTab.Exec(GetCell << Variant(AToRow) << Variant(ACol));
     GetCell.ClearArgs();
     MergeCell.ClearArgs();
     FTab.Exec(GetCell << Variant(AFrRow) << Variant(ACol)).Exec(MergeCell << ACell);
   }
  catchAll("������� "+IntToStr((int)ACol)+" ������ � "+IntToStr((int)AFrRow)+" �� "+IntToStr((int)AToRow),"������ ����������� �����")
*/
}
//---------------------------------------------------------------------------
int __fastcall TXMLTab::RowCount()
{
  return this->GetCount(false,"tr");
}
//---------------------------------------------------------------------------
int __fastcall TXMLTab::ColCount()
{
  int RC = 0;
  try
   {
     TTagNode *FRow = this->GetLastChild();
     RC = FRow->GetCount(false,"td");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::SetHAlign(int ARow, int ACol, int AAlign)
{
/*
  try
   {
     PropertySet  SetAAl("Alignment");
     PropertyGet  ParForm("ParagraphFormat");
     GetCell.ClearArgs();
     FTab.Exec(GetCell << Variant(ARow) << Variant(ACol)).Exec(GetRange).Exec(ParForm).Exec(SetAAl << Variant(AAlign));
   }
  catchAll("������ = "+IntToStr((int)ARow)+"  ������� = "+IntToStr((int)ACol),"������ ��������� �������� ��������������� ������������ ������")
*/
}
//---------------------------------------------------------------------------
void __fastcall TXMLTab::SetVAlign(int ARow, int ACol, int AAlign)
{
/*
  try
   {
     PropertySet  SetAAl("VerticalAlignment");
     GetCell.ClearArgs();
     FTab.Exec(GetCell << Variant(ARow) << Variant(ACol)).Exec(SetAAl << Variant(AAlign));
   }
  catchAll("������ = "+IntToStr((int)ARow)+"  ������� = "+IntToStr((int)ACol),"������ ��������� �������� ������������� ������������ ������")
*/
}
//---------------------------------------------------------------------------
/*
TXMLBorder* __fastcall TXMLTab::FBorder(int ARow, int ACol, int brdType)
{
return NULL;
  try
   {
     TXMLBorder* RC = new TXMLBorder;
     GetCell.ClearArgs();
     GetItem.ClearArgs();
     PropertyGet GerBorders("Borders");
     Variant tmp = FTab.Exec(GetCell << Variant(ARow) << Variant(ACol)).Exec(GetRange).Exec(GerBorders).Exec(GetItem << Variant(brdType));
     RC->SetBorder(tmp);
     return RC;
   }
  catchAllRC("������ = "+IntToStr((int)ARow)+"  ������� = "+IntToStr((int)ACol),"������ ����������� ������ ������",NULL)
#define catchAllRC(a,b,c) catch (...) { MessageBox(NULL, UnicodeString(a).c_str(),b,MB_ICONSTOP); return c;}
}
*/
//---------------------------------------------------------------------------
//!!!TXMLBorder* __fastcall TXMLTab::TopBorder(int ARow, int ACol)
//!!!{
//!!!return NULL;
/*
  return FBorder(ARow, ACol, brdTop);
*/
//!!!}
//---------------------------------------------------------------------------
//!!!TXMLBorder* __fastcall TXMLTab::LeftBorder(int ARow, int ACol)
//!!!{
//!!!return NULL;
/*
  return FBorder(ARow, ACol, brdLeft);
*/
//!!!}
//---------------------------------------------------------------------------
//!!!
/*
TXMLBorder* __fastcall TXMLTab::BottomBorder(int ARow, int ACol)
{
return NULL;
  return FBorder(ARow, ACol, brdBottom);
}
*/
//---------------------------------------------------------------------------
//!!!
/*
TXMLBorder* __fastcall TXMLTab::RightBorder(int ARow, int ACol)
{
return NULL;
  return FBorder(ARow, ACol, brdRight);
}
*/
//---------------------------------------------------------------------------
//!!!
/*
TXMLBorder* __fastcall TXMLTab::HorizontalBorder(int ARow, int ACol)
{
return NULL;
  return FBorder(ARow, ACol, brdHorizontal);
}
*/
//---------------------------------------------------------------------------
//!!!
/*
TXMLBorder* __fastcall TXMLTab::VerticalBorder(int ARow, int ACol)
{
return NULL;
  return FBorder(ARow, ACol, brdVertical);
}
*/
//---------------------------------------------------------------------------
//!!!
/*
__fastcall TXMLBorder::TXMLBorder()
{
  FBorder.ChangeType(9);
}
//---------------------------------------------------------------------------
void __fastcall TXMLBorder::SetBorder(Variant ABorder)
{
  FBorder = ABorder;
}
*/
//---------------------------------------------------------------------------
/*
void __fastcall TXMLBorder::SetArtStyle(int AStyle)
{
  try
   {
     PropertySet  SetProp("ArtStyle");
     FBorder.Exec(SetProp << Variant(AStyle));
   }
  catchAll("������ ��������� ����� ������ ������","������ ��������� ����� ������ ������ (��� -1)")
}
*/
//---------------------------------------------------------------------------
/*
void __fastcall TXMLBorder::SetArtWidth(int AWidth)
{
  try
   {
     PropertySet  SetProp("ArtWidth");
     FBorder.Exec(SetProp << Variant(AWidth));
   }
  catchAll("������ ��������� ������ ������ ������","������ ��������� ������ ������ ������ (��� -1)")
}
*/
//---------------------------------------------------------------------------
/*
void __fastcall TXMLBorder::SetColor(int AColor)
{
  try
   {
     PropertySet  SetProp("Color");
     FBorder.Exec(SetProp << Variant(AColor));
   }
  catchAll("������ ��������� ����� ������ ������","������ ��������� ����� ������ ������ (��� -1)")
}
*/
//---------------------------------------------------------------------------
/*
void __fastcall TXMLBorder::SetColorIndex(int AColor)
{
  try
   {
     PropertySet  SetProp("ColorIndex");
     FBorder.Exec(SetProp << Variant(AColor));
   }
  catchAll("������ ��������� ����� ������ ������","������ ��������� ����� ������ ������ (��� -2)")
}
*/
//---------------------------------------------------------------------------
/*
void __fastcall TXMLBorder::SetLineStyle(int ALineStyle)
{
  try
   {
     PropertySet  SetProp("LineStyle");
     FBorder.Exec(SetProp << Variant(ALineStyle));
   }
  catchAll("������ ��������� ����� ������ ������","������ ��������� ����� ������ ������ (��� -2)")
}
*/
//---------------------------------------------------------------------------
/*
void __fastcall TXMLBorder::SetLineWidth(int AWidth)
{
  try
   {
     PropertySet  SetProp("LineWidth");
     FBorder.Exec(SetProp << Variant(AWidth));
   }
  catchAll("������ ��������� ������ ������ ������","������ ��������� ������ ������ ������ (��� -2)")
}
*/
//---------------------------------------------------------------------------
/*
void __fastcall TXMLBorder::SetVisible(bool AVis)
{
  try
   {
     PropertySet  SetProp("Visible");
     FBorder.Exec(SetProp << Variant(AVis));
   }
  catchAll("������ ��������� ��������� ������ ������","������ ��������� ��������� ������ ������")
}
*/
//---------------------------------------------------------------------------
void __fastcall TXMLDoc::AsStrings(TStringList *ADest)
{
   if (!this) throw ETagNodeError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   tmpList = ADest;
   tmpList->Add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
   FCreateHTML(FDoc);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TXMLDoc::FGetAsString()
{
  UnicodeString RC = "";
  tmpList = new TStringList;
  try
   {
     tmpList->Add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
     FCreateHTML(FDoc);
     RC = tmpList->Text;
   }
  __finally
   {
     delete tmpList;
   }
  return RC;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TXMLDoc::AsTagNode()
{
  return FDoc;
}
//---------------------------------------------------------------------------
void __fastcall TXMLDoc::SetDoc(TTagNode* ADoc)
{
  FDoc->Assign(ADoc,true);
}
//---------------------------------------------------------------------------
void __fastcall TXMLDoc::FCreateHTML(TTagNode *ANode)
{
   UnicodeString ASrc = "";
   if (!FDoc) throw ETagNodeError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   UnicodeString _pcdata, xSpace;
   xSpace = UnicodeString::StringOfChar(' ', ANode->Level);
   ASrc += xSpace+"<"+ANode->Name;
   _pcdata = "";
   TAttr* FAttr;
   for (int i = 0; i < ANode->AttrCount; i++)
    {
      FAttr = ANode->Attrs(i);
      if (_wcscmpi(FAttr->Name.c_str(),L"PCDATA") == 0)
       _pcdata = RepNonXML(FAttr->Value,1,false);
      else
       ASrc += " "+FAttr->Name+"='"+RepNonXML(FAttr->Value,1,false)+"'";
    }
   ASrc += ">";
   tmpList->Add(ASrc);
   if (_pcdata.Length()) tmpList->Add(_pcdata);
   if (ANode->Count != 0)
    {
//      if (!ANode->GetFirstChild()->CmpName("b,i,u,s,strong"))
//       ASrc += "\n";
      TTagNode *ItNode = ANode->GetFirstChild();
      while (ItNode)
       {
         FCreateHTML(ItNode);
         ItNode = ItNode->GetNext();
       }
    }
   if (!ANode->CmpName("br,hr,basefont,meta"))
    {
      if (ANode->Count == 0) tmpList->Add(("</"+ANode->Name+">").c_str());
      else                   tmpList->Add((xSpace+"</"+ANode->Name+">").c_str());
    }
//   if (!ANode->CmpName("b,i,u,s,strong"))
//    ASrc += "\n";
//   tmpList->Add(ASrc);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TXMLDoc::RepNonXML(UnicodeString ASrc, bool ARepSpace, bool ARepFormatTags)
{
  UnicodeString tmp = ASrc;
  TReplaceFlags Repflg;
  Repflg << rfReplaceAll;
//  if (strstr(tmp.c_str(),"&nbsp;")) tmp = StringReplace(tmp, "&nbsp;",  "$nbsp;$",  Repflg);
/*
  if (!ARepFormatTags)
   {
     if (strstr(tmp.c_str(),"<b>") ) tmp = StringReplace(tmp, "<b>",  "$b$",  Repflg);
     if (strstr(tmp.c_str(),"</b>")) tmp = StringReplace(tmp, "</b>", "$/b$",  Repflg);
     if (strstr(tmp.c_str(),"<s>") ) tmp = StringReplace(tmp, "<s>",  "$s$",  Repflg);
     if (strstr(tmp.c_str(),"</s>")) tmp = StringReplace(tmp, "</s>", "$/s$",  Repflg);
     if (strstr(tmp.c_str(),"<i>") ) tmp = StringReplace(tmp, "<i>",  "$i$",  Repflg);
     if (strstr(tmp.c_str(),"</i>")) tmp = StringReplace(tmp, "</i>", "$/i$",  Repflg);
     if (strstr(tmp.c_str(),"<u>") ) tmp = StringReplace(tmp, "<u>",  "$u$",  Repflg);
     if (strstr(tmp.c_str(),"</u>")) tmp = StringReplace(tmp, "</u>", "$/u$",  Repflg);
     if (strstr(tmp.c_str(),"<br>")) tmp = StringReplace(tmp, "<br>", "$br$",  Repflg);
     if (strstr(tmp.c_str(),"<hr>")) tmp = StringReplace(tmp, "<hr>", "$hr$",  Repflg);
   }
  if (strchr(tmp.c_str(),'&')  ) tmp = StringReplace(tmp, "&",  "&amp;",  Repflg);
  if (strchr(tmp.c_str(),'<')  ) tmp = StringReplace(tmp, "<",  "&lt;",   Repflg);
  if (strchr(tmp.c_str(),'>')  ) tmp = StringReplace(tmp, ">",  "&gt;",   Repflg);
  if (strchr(tmp.c_str(),'\"') ) tmp = StringReplace(tmp, "\"", "&quot;", Repflg);
  if (strchr(tmp.c_str(),'\'') ) tmp = StringReplace(tmp, "'",  "&apos;", Repflg);
  if (ARepSpace)
   tmp = StringReplace(tmp, " ", "&nbsp;", Repflg);
*/
  if (!ARepFormatTags)
   {
     if (wcsstr(tmp.c_str(),L"$lt$") ) tmp = StringReplace(tmp,L"$lt$",  L"<",   Repflg);
     if (wcsstr(tmp.c_str(),L"$gt$") ) tmp = StringReplace(tmp,L"$gt$",  L">",   Repflg);
     if (wcsstr(tmp.c_str(),L"$b$") )  tmp = StringReplace(tmp,L"$b$",  L"<b>",   Repflg);
     if (wcsstr(tmp.c_str(),L"$/b$"))  tmp = StringReplace(tmp,L"$/b$", L"</b>",   Repflg);
     if (wcsstr(tmp.c_str(),L"$s$") )  tmp = StringReplace(tmp,L"$s$",  L"<s>",   Repflg);
     if (wcsstr(tmp.c_str(),L"$/s$"))  tmp = StringReplace(tmp,L"$/s$", L"</s>",   Repflg);
     if (wcsstr(tmp.c_str(),L"$i$") )  tmp = StringReplace(tmp,L"$i$",  L"<i>",   Repflg);
     if (wcsstr(tmp.c_str(),L"$/i$"))  tmp = StringReplace(tmp,L"$/i$", L"</i>",   Repflg);
     if (wcsstr(tmp.c_str(),L"$u$") )  tmp = StringReplace(tmp,L"$u$",  L"<u>",   Repflg);
     if (wcsstr(tmp.c_str(),L"$/u$"))  tmp = StringReplace(tmp,L"$/u$", L"</u>",   Repflg);
     if (wcsstr(tmp.c_str(),L"$br$"))  tmp = StringReplace(tmp,L"$br$", L"<br>",   Repflg);
     if (wcsstr(tmp.c_str(),L"$hr$"))  tmp = StringReplace(tmp,L"$hr$", L"<hr>",   Repflg);
   }
  return tmp;
}
//---------------------------------------------------------------------------

