﻿// ---------------------------------------------------------------------------
// #include <vcl.h>
#pragma hdrstop
#include "dsDocUnit.h"
#include <Datasnap.DSHTTP.hpp>
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "System.Classes.TPersistent"
#pragma resource "*.dfm"
// TRegClassDM *RegClassDM;
// ---------------------------------------------------------------------------
__fastcall TdsDocClass::TdsDocClass(TComponent * Owner, TdsMKB * AMKB, TAxeXMLContainer * AXMLList) : TDataModule(Owner)
 {
  dsLogC(__FUNC__);
  try
   {
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes /* AICDBPath.Length() && ADocDBPath.Length() */)
     {
      ConnectDB(IcConnection, FSes->UserRoles->Values["ICDB"], __FUNC__);
      ConnectDB(DocConnection, FSes->UserRoles->Values["DOCDB"], __FUNC__);
      FDM      = new TdsDocDM(this, DocConnection, IcConnection, AMKB, AXMLList);
      FRegComp = new TdsSrvRegistry(this, DocConnection, FDM->XMLList, "549F0EBE-1B9B98A7-BD64", "", false);
      FRegComp->OnExtGetCode = FGetNewTabCode;
      FRegComp->OnDataConv = FDataConv;
      FDefXML              = FDM->XMLList->GetXML("549F0EBE-1B9B98A7-BD64");
      FRegComp->SetRegDef(FDefXML);
      FRegComp->OnGetAddrStr        = FGetAddrStr;
      FRegComp->OnGetOrgStr         = FGetOrgStr;
      FRegComp->OnGetFilterData     = FGetFilterData;
      FDM->ImmNom->OnGetAddrStr     = FGetAddrStr;
      FDM->ImmNom->OnGetOrgStr      = FGetOrgStr;
      FDM->ImmNom->OnGetOrgFieldStr = FGetOrgFieldStr;
      FDM->DocNom->OnGetAddrStr     = FGetAddrStr;
      FDM->DocNom->OnGetOrgStr      = FGetOrgStr;
      FDM->OnExtGetAVal             = FGetExtAVal;
      FKLADRComp                    = new TdsKLADRClass(this, IcConnection);
      FOrgComp                      = new TdsOrgClass(NULL, FDM->XMLList->GetXML("5BCAE819-D0CFEF0F-0B87"));
     }
   }
  catch (Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogC(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
__fastcall TdsDocClass::~TdsDocClass()
 {
  dsLogD(__FUNC__);
  DisconnectDB(DocConnection, __FUNC__);
  DisconnectDB(IcConnection, __FUNC__);
  delete FRegComp;
  delete FDM;
  delete FKLADRComp;
  delete FOrgComp;
  dsLogD(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
void TdsDocClass::Disconnect()
 {
  // FRegComp->Disconnect();
 }
// ---------------------------------------------------------------------------
TDocModuleType TdsDocClass::FGetModuleType(System::UnicodeString AId)
 {
  TDocModuleType RC = dmtNone;
  try
   {
    UnicodeString FMod = GetLPartB(AId).Trim().UpperCase();
    if (FMod == "DOC")
     RC = dmtDoc;
    else if (FMod == "FLT")
     RC = dmtFlt;
    else if (FMod == "SORG")
     RC = dmtSOrg;
    else if (FMod == "SPEC")
     RC = dmtSpec;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 TdsDocClass::GetCount(UnicodeString AId, UnicodeString AFilterParam)
 {
  if (AFilterParam.Length())
   dsLogMessage("GetCount: ID: " + AId + ", filter: " + AFilterParam);
  else
   dsLogMessage("GetCount: ID: " + AId);
  __int64 RC = 0;
  try
   {
    try
     {
      RC = FRegComp->GetCount(GetRPartE(AId).Trim().UpperCase(), AFilterParam);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsDocClass::GetIdList(UnicodeString AId, int AMax, UnicodeString AFilterParam)
 {
  if (AFilterParam.Length())
   dsLogMessage("GetIdList: ID: " + AId + ", Max: " + IntToStr(AMax) + ", filter: " + AFilterParam, "", 5);
  else
   dsLogMessage("GetIdList: ID: " + AId + ", Max: " + IntToStr(AMax), "", 5);
  TJSONObject * RC;
  try
   {
    try
     {
      RC = FRegComp->GetIdList(GetRPartE(AId).Trim().UpperCase(), AMax, AFilterParam);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsDocClass::GetValById(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogMessage("GetValById: ID: " + AId + ", RecId: " + ARecId);
  TJSONObject * RC;
  try
   {
    try
     {
      RC = FRegComp->GetValById(GetRPartB(AId).Trim(), ARecId);
      /*
       switch (FGetModuleType(AId))
       {
       case imtReg:
       {
       RC = FRegComp->GetValById(GetRPartB(AId).Trim().UpperCase(), ARecId);
       break;
       }
       case imtCard:
       {
       break;
       }
       case imtPlan:
       {
       break;
       }
       }
       */
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsDocClass::GetValById10(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogMessage("GetValById: ID: " + AId + ", RecId: " + ARecId);
  TJSONObject * RC;
  try
   {
    try
     {
      RC = FRegComp->GetValById10(GetRPartB(AId).Trim(), ARecId);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsDocClass::Find(UnicodeString AId, UnicodeString AFindData)
 {
  dsLogMessage("Find: ID: " + AId + ", FindParam: " + AFindData);
  TJSONObject * RC;
  try
   {
    try
     {
      RC = FRegComp->Find(GetRPartE(AId).Trim().UpperCase(), AFindData);
      /*
       switch (FGetModuleType(AId))
       {
       case imtReg:
       {
       RC = FRegComp->Find(GetRPartE(AId).Trim().UpperCase(), AFindParam);
       break;
       }
       case imtCard:
       {
       break;
       }
       case imtPlan:
       {
       break;
       }
       }
       */
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsDocClass::InsertData(UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecId)
 {
  dsLogMessage("InsertData: ID: " + AId + ", Value: " + AValue->ToString());
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FRegComp->InsertData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId);
      // RC = FDM->InsertData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId);
      /*
       switch (FGetModuleType(AId))
       {
       case imtReg:
       {
       break;
       }
       case imtCard:
       {
       break;
       }
       case imtPlan:
       {
       break;
       }
       }
       */
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsDocClass::EditData(UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecId)
 {
  dsLogMessage("EditData: ID: " + AId + ", Value: " + AValue->ToString());
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FRegComp->EditData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId);
      // RC = FDM->EditData(GetRPartE(AId).Trim().UpperCase(), AValue);
      /*
       switch (FGetModuleType(AId))
       {
       case imtReg:
       {
       break;
       }
       case imtCard:
       {
       break;
       }
       case imtPlan:
       {
       break;
       }
       }
       */
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsDocClass::DeleteData(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogMessage("DeleteData: ID: " + AId + ", RecId: " + ARecId);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FRegComp->DeleteData(GetRPartE(AId).Trim().UpperCase(), ARecId);
      /*
       switch (FGetModuleType(AId))
       {
       case imtReg:
       {
       break;
       }
       case imtCard:
       {
       break;
       }
       case imtPlan:
       {
       break;
       }
       }
       */
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString TdsDocClass::GetTextData(UnicodeString AId, UnicodeString AFlId, UnicodeString ARecId,
  UnicodeString & AMsg)
 {
  dsLogMessage("DeleteData: ID: " + AId + ", RecId: " + ARecId);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FRegComp->GetTextData(GetRPartE(AId).Trim().UpperCase(), AFlId, ARecId, AMsg);
      /*
       switch (FGetModuleType(AId))
       {
       case imtReg:
       {
       break;
       }
       case imtCard:
       {
       break;
       }
       case imtPlan:
       {
       break;
       }
       }
       */
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsDocClass::SetTextData(UnicodeString AId, UnicodeString AFlId, UnicodeString ARecId,
  UnicodeString AVal)
 {
  dsLogMessage("DeleteData: ID: " + AId + ", RecId: " + ARecId);
  UnicodeString RC = "";
  try
   {
    try
     {
      UnicodeString FState = GetLPartB(AId, '#').Trim().UpperCase();
      if (!FState.Length())
       {
        RC = FRegComp->SetTextData(GetRPartE(AId).Trim().UpperCase(), AFlId, ARecId, AVal);
       }
      else if (FState == "BEGIN")
       {
        FTextData = AVal;
       }
      else if (FState == "PROGRESS")
       {
        FTextData += AVal;
       }
      else if (FState == "END")
       {
        FTextData += AVal;
        RC        = FRegComp->SetTextData(GetRPartE(GetRPartB(AId, '#')).Trim().UpperCase(), AFlId, ARecId, FTextData);
        FTextData = "";
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsDocClass::GetDefXML()
 {
  dsLogMessage("GetDefXML");
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDefXML->AsXML;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsDocClass::GetClassXML(UnicodeString ARef)
 {
  dsLogMessage("GetClassXML: ID: " + ARef);
  UnicodeString RC = "";
  TTagNode * def = new TTagNode;
  try
   {
    try
     {
      def->Encoding = "utf-8";
      def->LoadFromXMLFile(icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\" + ARef + ".xml"));
      RC = def->AsXML;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete def;
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsDocClass::GetClassZIPXML(UnicodeString ARef)
 {
  dsLogMessage("GetClassZIPXML: ID: " + ARef);
  UnicodeString RC = "";
  TTagNode * def = new TTagNode;
  try
   {
    try
     {
      /*
       def->Encoding = "utf-8";
       def->LoadFromZIPXMLFile(icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\" + ARef + ".zxml"));
       RC = def->AsXML;
       */
      RC = FDM->XMLList->GetXML(ARef)->AsXML;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete def;
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsDocClass::GetSpecById(UnicodeString AId, int & ASpecType)
 {
  dsLogMessage("GetSpec: ID: " + AId);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->GetSpecTextById(AId, ASpecType);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString TdsDocClass::GetDocById(UnicodeString AId)
 {
  dsLogMessage("GetSpec: ID: " + AId);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->GetDocTextById(AId);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString TdsDocClass::CreateDocPreview(UnicodeString AId, int & Al, int & At, int & Ar, int & Ab,
  double & Aps, int & Ao)
 {
  dsLogMessage("GetDoc: ID: " + AId);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->CreateDocPreview(AId, Al, At, Ar, Ab, Aps, Ao);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void TdsDocClass::SaveDoc(UnicodeString AId)
 {
  dsLogMessage("SaveDoc: ID: " + AId);
  try
   {
    try
     {
      // FDM->SaveDoc(AId);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
System::UnicodeString TdsDocClass::CheckByPeriod(bool FCreateByType, UnicodeString ACode, UnicodeString ASpecGUI)
 {
  dsLogMessage("CheckByPeriod: ByType=" + IntToStr((int)FCreateByType) + ", Code=" + ACode + ", SpecGUI=" + ASpecGUI);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->CheckByPeriod(FCreateByType, ACode, ASpecGUI);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString TdsDocClass::DocListClearByDate(TDate ADate)
 {
  dsLogMessage("DocListClearByDate:  date: " + ADate.FormatString("dd.mm.yyyy"));
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->DocListClearByDate(ADate);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int TdsDocClass::GetDocCountBySpec(UnicodeString ASpecOWNER, UnicodeString ASpecGUI, UnicodeString & ARCMsg)
 {
  int RC = 0;
  dsLogMessage("GetDocCountBySpec:  SpecOWNER: " + ASpecOWNER + ", SpecGUI:" + ASpecGUI);
  try
   {
    try
     {
      RC = FDM->GetDocCountBySpec(ASpecOWNER, ASpecGUI, ARCMsg);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void TdsDocClass::SetMainLPU(__int64 ALPUCode)
 {
  try
   {
    try
     {
      FDM->SetMainLPU(ALPUCode);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString TdsDocClass::GetMainLPUParam()
 {
  UnicodeString RC = "<error/>";
  try
   {
    try
     {
      RC = FDM->GetMainLPUParam();
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsDocClass::MainOwnerCode()
 {
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->MainOwnerCode;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool TdsDocClass::IsMainOwnerCode(UnicodeString AOwnerCode)
 {
  dsLogMessage("IsMainOwnerCode:  SpecOwner: " + AOwnerCode);
  bool RC = false;
  try
   {
    try
     {
      RC = FDM->IsMainOwnerCode(AOwnerCode);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString TdsDocClass::CopySpec(UnicodeString AName, UnicodeString & ANewGUI)
 {
  dsLogMessage("CopySpec:  Name: " + AName);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->CopySpec(AName, ANewGUI);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int TdsDocClass::CheckSpecExists(UnicodeString AName, UnicodeString & AGUI, bool AIsEdit)
 {
  dsLogMessage("CheckSpecExists:  Name: " + AName);
  int RC = 0;
  try
   {
    try
     {
      RC = FDM->CheckSpecExists(AName, AGUI, AIsEdit);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool TdsDocClass::CheckSpecExistsByCode(UnicodeString AGUI)
 {
  dsLogMessage("CheckSpecExistsByCode:  AGUI: " + AGUI);
  int RC = 0;
  try
   {
    try
     {
      RC = FDM->CheckSpecExistsByCode(AGUI);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool TdsDocClass::CheckDocCreateType(UnicodeString ASpecGUI, int & ACreateType, int ADefCreateType,
  int & AGetCreateType, int & AGetPeriod)
 {
  dsLogMessage("CheckDocCreate:  '" + ASpecGUI + "', ACreateType: " + IntToStr(ACreateType) + ", ADefCreateType: " +
    IntToStr(ADefCreateType));
  bool RC = false;
  try
   {
    try
     {
      RC = FDM->CheckDocCreateType(ASpecGUI, ACreateType, ADefCreateType, AGetCreateType, AGetPeriod);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsDocClass::DocCreateCheckExist(UnicodeString ASpecGUI, UnicodeString AAuthor, int APerType,
  TDate APeriodDateFr, TDate APeriodDateTo)
 {
  dsLogMessage("DocCreateCheckExist:  '" + ASpecGUI + "', APerType: " + IntToStr(APerType) + "', APerType: " +
    APeriodDateFr.FormatString("dd.mm.yyyy") + "', APerType: " + APeriodDateTo.FormatString("dd.mm.yyyy"));
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->DocCreateCheckExist(ASpecGUI, AAuthor, APerType, APeriodDateFr, APeriodDateTo);
      dsLogMessage("DocCreateCheckExist  RC:  '" + RC + "'");
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsDocClass::CheckFilter(UnicodeString AFilterGUI)
 {
  dsLogMessage("CheckFilter:  '" + AFilterGUI + "'");
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->CheckFilter(AFilterGUI);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsDocClass::DocCreateCheckFilter(UnicodeString ASpecGUI, UnicodeString AFilterCode,
  UnicodeString & AFilterName, UnicodeString & AFilterGUI)
 {
  dsLogMessage("DocCreateCheckFilter:  '" + ASpecGUI + "', AFilterCode: " + AFilterCode);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->DocCreateCheckFilter(ASpecGUI, AFilterCode, AFilterName, AFilterGUI);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsDocClass::DocCreate(UnicodeString ASpecGUI, UnicodeString AAuthor, UnicodeString AFilter, bool AConcr,
  int ACreateType, int APerType, TDate APeriodDateFr, TDate APeriodDateTo)
 {
  dsLogMessage("DocCreate:  '" + ASpecGUI + "', ACreateType: " + IntToStr(ACreateType) + ", APerType: " +
    IntToStr(APerType));
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->DocCreate(ASpecGUI, AAuthor, AFilter, AConcr, ACreateType, APerType, APeriodDateFr, APeriodDateTo);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsDocClass::StartDocCreate(UnicodeString ASpecGUI, UnicodeString AAuthor, UnicodeString AFilter,
  UnicodeString AFilterNameGUI, bool AConcr, int ACreateType, int APerType, TDate APeriodDateFr, TDate APeriodDateTo,
  bool ANotSave)
 {
  dsLogMessage("DocCreate:  '" + ASpecGUI + "', ACreateType: " + IntToStr(ACreateType) + ", APerType: " +
    IntToStr(APerType));
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->StartDocCreate(ASpecGUI, AAuthor, AFilter, AFilterNameGUI, AConcr, ACreateType, APerType, APeriodDateFr,
        APeriodDateTo, ANotSave);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString TdsDocClass::DocCreateProgress(System::UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "error";
  ARecId = "";
  try
   {
    RC = FDM->DocCreateProgress(ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
bool TdsDocClass::FGetNewTabCode(UnicodeString ATab, UnicodeString & ARet)
 {
  bool RC = false;
  try
   {
    UnicodeString FTab = ATab.UpperCase();
    if (FTab == "DOCUMENTS")
     {
      ARet = NewGUI();
     }
    else if (FTab == "FILTERS")
     {
      ARet = FDM->GetNewTabCode("FILTERS", "CODE");
     }
    else if (FTab == "SPECIFIKATORS")
     {
      if (ARet.UpperCase() == "-777")
       ARet = NewGUI();
     }
    else if (FTab == "SUBORDORG")
     {
      ARet = FDM->NewGUID();
     }
    RC = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsDocClass::FDataConv(TdsRegTextDataConvType AConvType, UnicodeString ATabId, UnicodeString AFlId,
  UnicodeString AData)
 {
  UnicodeString RC = AData;
  try
   {
    if (AConvType == cdtSet)
     {
      if (ATabId == "0002")
       {
        TStringStream * FZipDoc = new TStringStream;
        TTagNode * FDoc = new TTagNode;
        try
         {
          FDoc->AsXML = RC;
          FDoc->GetZIPXML(FZipDoc);
          RC = FZipDoc->DataString;
         }
        __finally
         {
          delete FDoc;
          delete FZipDoc;
         }
       }
     }
    else
     { // cdtGet
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsDocClass::FGetAddrStr(UnicodeString ACode, unsigned int AParams)
 {
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FKLADRComp->AddrCodeToText(ACode, AParams /* "11111111" */);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsDocClass::FGetOrgStr(UnicodeString ACode, unsigned int AParams)
 {
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FOrgComp->OrgCodeToText(ACode);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsDocClass::FGetOrgFieldStr(__int64 ACode, UnicodeString AFieldName)
 {
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FOrgComp->GetOrgField(ACode, AFieldName);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocClass::FGetExtAVal(TTagNode * ADefNode, TStringList * AFields)
 {
  UnicodeString RC = "GetExtAVal: ОШИБКА [" + ADefNode->AV["uid"] + "]";
  try
   {
    try
     {
      if (ADefNode->CmpAV("uid", "0136,0032,00FF"))
       { // адрес полностью
        RC = "";
        if (AFields->Values["R0136"].Length())
         {
          adresCode FAddrCode = ParseAddrStr(AFields->Values["R0136"]);
          if (ADefNode->CmpAV("uid", "0136"))
           { // адрес полностью
            if (AFields->Values["R0100"].Length())
             ParseHouseStruct(AFields->Values["R0100"], FAddrCode.House, FAddrCode.Vld, FAddrCode.Korp,
            FAddrCode.Build);
            if (AFields->Values["R0033"].Length())
             FAddrCode.Flat = AFields->Values["R0033"];
            RC = FKLADRComp->AddrCodeToText(FAddrCode.AsString(), 0x0FF);
           }
          else if (ADefNode->CmpAV("uid", "0032"))
           { // Населённый пункт
            RC = FKLADRComp->AddrCodeToText(FAddrCode.AsString(), 0x070 /* "0111 0000" */);
           }
          else if (ADefNode->CmpAV("uid", "00FF"))
           { // Улица
            RC = FKLADRComp->AddrCodeToText(FAddrCode.AsString(), 0x008 /* "00001000" */);
           }
         }
       }
      else if (ADefNode->CmpAV("uid", "0100"))
       { // Дом
        RC = "";
        if (AFields->Values["R0100"].Length())
         {
          adresCode FAddrCode = ParseAddrStr("000000000000000000000@02110");
          if (AFields->Values["R0100"].Length())
           ParseHouseStruct(AFields->Values["R0100"], FAddrCode.House, FAddrCode.Vld, FAddrCode.Korp, FAddrCode.Build);
          RC = FKLADRComp->AddrCodeToText(FAddrCode.AsString(), 0x004 /* "00000100" */);
         }
       }
      else if (ADefNode->CmpAV("uid", "0033"))
       { // Квартира
        RC = "";
        if (AFields->Values["R0033"].Length())
         {
          adresCode FAddrCode = ParseAddrStr("000000000000000000000@02110");
          if (AFields->Values["R0033"].Length())
           {
            FAddrCode.Flat        = AFields->Values["R0033"];
            FAddrCode.FlatObjType = fotFlat;
           }
          RC = FKLADRComp->AddrCodeToText(FAddrCode.AsString(), 0x002 /* "00000010" */);
         }
       }
      else if (ADefNode->CmpAV("uid", "0025"))
       {
        if (AFields->Values["R0025"].Length())
         {
          orgCode FOrgCode;
          FOrgCode.OrgID = AFields->Values["R0025"].ToIntDef(0);
          if (AFields->Values["R0026"].Length())
           FOrgCode.L1Val = AFields->Values["R0026"];
          if (AFields->Values["R0027"].Length())
           FOrgCode.L2Val = AFields->Values["R0027"];
          if (AFields->Values["R0028"].Length())
           FOrgCode.L3Val = AFields->Values["R0028"];
          // if (AFields->Values["R0111"].Length())
          // FOrgCode.State = AFields->Values["R0111"];
          if (AFields->Values["R0112"].Length())
           FOrgCode.Format = UIDStr(AFields->Values["R0112"].ToIntDef(0));
          RC = FOrgComp->OrgCodeToText(FOrgCode.AsString());
         }
        else
         {
          RC = "неорганизован";
          int noval = AFields->Values["R003E"].Trim().ToIntDef(-1);
          switch (noval)
           {
           case 0:
             {
              RC += " по состоянию здоровья";
              break;
             }
           case 1:
             {
              RC += " по социальным причинам";
              break;
             }
           case 2:
             {
              RC += " по другим причинам";
              break;
             }
           }
         }
       }
      else if (ADefNode->CmpAV("uid", "011B"))
       { // Группа организации
        RC = "Группа организации";
       }
      else if (ADefNode->CmpAV("uid", "0026"))
       {
        if (AFields->Values["R0025"].Length())
         {
          orgCode FOrgCode;
          FOrgCode.OrgID = AFields->Values["R0025"].ToIntDef(0);
          if (AFields->Values["R0026"].Length())
           FOrgCode.L1Val = AFields->Values["R0026"];
          if (AFields->Values["R0027"].Length())
           FOrgCode.L2Val = AFields->Values["R0027"];
          if (AFields->Values["R0028"].Length())
           FOrgCode.L3Val = AFields->Values["R0028"];
          RC = FOrgComp->OrgCodeToText(FOrgCode.AsString(), "01110000");
         }
        else
         RC = "";
       }
      else if (ADefNode->CmpAV("uid", "1082"))
       { // заболевание по МКБ-10
        RC = FDM->MKB->CodeToText(AFields->Values["R1082"]);
       }
      else if (ADefNode->CmpAV("uid", "3082"))
       { // заболевание по МКБ-10
        RC = FDM->MKB->CodeToText(AFields->Values["R3082"]);
       }
      else if (ADefNode->CmpAV("uid", "3304"))
       { // заболевание по МКБ-10
        RC = FDM->MKB->CodeToText(AFields->Values["R3304"]);
       }
      else if (ADefNode->CmpAV("uid", "32FF"))
       { // заболевание по МКБ-10
        RC = FDM->MKB->CodeToText(AFields->Values["R32FF"]);
       }
      else if (ADefNode->CmpAV("uid", "1089"))
       { // МИБП
        RC = FDM->GetMIBPName(AFields->Values["R1086"].ToIntDef(0), AFields->Values["R1089"]);
       }
      else if (ADefNode->CmpAV("uid", "3089"))
       { // МИБП
        RC = FDM->GetMIBPName(AFields->Values["R3086"].ToIntDef(0), AFields->Values["R3089"]);
       }
      else if (ADefNode->CmpAV("uid", "32AA"))
       { // МИБП
        RC = FDM->GetMIBPName(AFields->Values["R3086"].ToIntDef(0), AFields->Values["R32AA"]);
       }
      else if (ADefNode->CmpAV("uid", "108B"))
       { // 1084 <extedit name='Вид' uid='108B' required='1' inlist='e' length='255'/>
        RC = GetRPartB(AFields->Values["R108B"], '.');
       }
      else if (ADefNode->CmpAV("uid", "308B"))
       { // 3084 <extedit name='Вид' uid='308B' required='1' inlist='e' length='255'/>
        RC = FDM->GetVacType(AFields->Values["R308B"].Trim());
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsDocClass::LoadDocPreviewProgress(System::UnicodeString & AMsg)
 {
  UnicodeString RC = "error";
  try
   {
    try
     {
      RC = FDM->LoadDocPreviewProgress(AMsg);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * TdsDocClass::GetQList(UnicodeString AId)
 {
  TJSONObject * RC = NULL;
  try
   {
    RC = FDM->GetQList(AId);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void TdsDocClass::SetQList(UnicodeString AId, TJSONObject * AList)
 {
  try
   {
    FDM->SetQList(AId, AList);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString TdsDocClass::GetFilterByGUI(System::UnicodeString AGUI)
 {
  return FDM->GetFilterByGUI(AGUI);
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsDocClass::ImportData(UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecId)
 {
  dsLogMessage("ImportData: ID: " + AId + ", Value: " + AValue->ToString());
  UnicodeString RC = "";
  try
   {
    try
     {
      UnicodeString FResMessage = "";
      RC = FRegComp->ImportData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId, FResMessage);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString TdsDocClass::FGetFilterData(System::UnicodeString AGUI, unsigned int AParams)
 {
  UnicodeString RC = FDM->GetFilterByGUI(AGUI);
  return RC;
 }
// ---------------------------------------------------------------------------
bool TdsDocClass::CanHandEdit(UnicodeString ASpecGUI)
 {
  return FDM->CanHandEdit(ASpecGUI);
 }
// ---------------------------------------------------------------------------
