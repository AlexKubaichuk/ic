/*
  ����        - dsDocViewCreator.h
  ������      - ��������������� (ICS)
  ��������    - ����������� ���������.
                ������������ ����
  ����������� - �������� �.�.
  ����        - 01.02.2015
*/
//---------------------------------------------------------------------------

#ifndef dsDocViewCreatorH
#define dsDocViewCreatorH

//---------------------------------------------------------------------------

#include <System.SysUtils.hpp>
#include <System.Classes.hpp>
//#include <Registry.hpp>
//#include "DKClasses.h"
#include "XMLContainer.h"
#include "ExtUtils.h"
#include <list>
//#include "IcsXMLDoc.h"
//#include "Preview.h"

//#include "ICSDocGlobals.h"
#include "dsDocSpec.h"

namespace dsDocViewCreator
{

//###########################################################################
//##                                                                       ##
//##                                 Globals                               ##
//##                                                                       ##
//###########################################################################

// ������ ������������ ���������
enum TICSDocViewerDocFormat
{
  dfHTML,       // ������ HTML
  dfMSWord      // ������ MS Word
};

//---------------------------------------------------------------------------

// ��� ������� ��� �����������
enum TICSDocViewerPreviewType
{
  ptUnknown,            // �� ������ ����, ���� �� ���� ����� :)
  ptSpecificator,       // ���������� ������������
  ptDocument            // ���������� ��������
};

//---------------------------------------------------------------------------

// ����� ������ ������������
enum TICSDocViewerMode
{
  vmOnFly,              // ������������ ��������� "�� ����"
  vmSaveToDisk          // ������������ ��������� � ��������������� ����������� �� ����
};

//---------------------------------------------------------------------------

// ���� ������������ ����������� �������������/���������
enum TICSDocViewerCreateStage
{
  csNone,               // ����������� ��������� � ��������� ���������� �
                        // ������������ ����������� �������������/���������
  csCreateInit,         // ������������� ����� ������������� ����������� �������������/���������
  csCreateHTMLTags,     // ������������ HTML-�����
  csCreateHTML,         // �������������� HTML-����� � ������
  csFillMSWordTemplate, // ���������� ������� MS Word
  csPreviewDlgStart     // ������ ������� ���������������� ���������
};

//---------------------------------------------------------------------------

typedef void __fastcall (__closure *TOnChangeCreateStageEvent)( TObject* Sender, const TICSDocViewerCreateStage ACreateStage );
//typedef void __fastcall (__closure *TOnPreviewCreationFinishedEvent)( TObject* Sender, const bool bNormallyTerminated );

//###########################################################################
//##                                                                       ##
//##                               TdsDocViewCreator                           ##
//##                                                                       ##
//###########################################################################

// ����������� ���������

class PACKAGE TdsDocViewCreator : public TComponent/*, IDKComponentVersion, IDKComponentComment*/
{
    typedef TComponent inherited;
private:
    TICSDocViewerDocFormat FDocFormat;      //������ ������������ ���������
    TICSDocViewerMode      FMode;           //����� ������ ������������
    bool                   FUseCSS;         //���������� ������������ ��� ������������ HTML-��������� CSS
    bool                   FUseWordCSS;     //���������� ������������� ��� ������������ HTML-��������� CSS
                                            //��� ������������� ���������� �������� �
                                            //MS Word 2000/XP/2003
    bool                   FUseExcelCSS;    //���������� ������������� ��� ������������ HTML-��������� CSS
                                            //��� ������������� ���������� �������� �
                                            //MS Excel 2000/XP/2003
    TICSDocViewerCreateStage FState;        //��������� ������������ - ���� ������������ �����������
                                            //�������������/���������
    TTagNode*              FSpec;           //������������ ( xml-������ � ������ "specificator" )
    TTagNode*              FDoc;            //�������� ( xml-������ � ������ "docum" )
    UnicodeString             FTmpPath;        //���� � �������� ��� ������������ ��������� HTML-������
                                            //� ������ vmSaveToDisk
    list<UnicodeString>       FTmpFileNames;   //����� ��������� ������ ��� ������ vmSaveToDisk
    UnicodeString             FDefSavePath;    //���� � �������� �� ��������� ��� ����������
                                            //���������������� HTML-�����
    UnicodeString             FHTML;           //HTML-���� � ���� ������
    TTagNode*              FndHTML;         //������ html-�����
    TAxeXMLContainer*      FXMLContainer;   //XML-��������� ( ��. AxeUtil.h ) � �������������
    TICSDocViewerPreviewType FPreviewType;  //��� ������� ��� �����������
    bool                   FDocAssign;      //������� ������������� assign'� ���
                                            //���������
    bool                   FListZeroValCheckCountOnly; //������� ���������� �������
                                                       //����������� ������� ��������
                                                       //��� ������ ������ ���
                                                       //��������, ��������������� count'�
    TTagNode*              FDocXMLTemplate; //������ MS Word, �� ������ �������� �������� �����������
    void                   __fastcall FSetDocXMLTemplate(TTagNode *ATmpl);

    //�������
    TOnChangeCreateStageEvent FOnChangeCreateStage; //��������� ��� ����� ����� ������������
                                                    //����������� �������������/���������



    //���� ��� ���������� ���� ( ������������ ��� �������� � ������ ������� ����� ������������ �������� )
    UnicodeString    __BorderWidth;    //������ ����� ������� ( xml-��� "table" ������������� )
                                    //��� ������ ( xml-��� "list" ������������� )
    UnicodeString    __Width;
    bool          __RowNumbers;     //������� ������������� ��������� ����� ������� ( xml-��� "table" ������������� )
                                    //��� ����� ������ ( xml-��� "list" ������������� )
    bool          __ColNumbers;     //������� ������������� ��������� �������� ������� ( xml-��� "table" ������������� )
    int           __RowNum;         //������� ����� ������ ������� ( xml-��� "table" ������������� )
    int           __ColNum;         //������� ����� ������� ������� ( xml-��� "table" ������������� )
    //���� ��� ���������� ����
    TStringList*  __DopInfo;
    int        __PBMax;             //������������ ��� �������� ������ � ����� InternalSetPBMax()
    int        __PBPosition;        //������������ ��� �������� ������ � ����� InternalSetPBPosition()
    WideString __URL;               //������������ ��� �������� ������ � ����� HTMLPreview()
    bool       __OnFly;
    bool       __CanShowPreviewDlg;
    TICSDocViewerCreateStage __CreateStage; //������������ ��� �������� ������ � ����� GenerateOnChangeCreateStageEvent()

    //*****************************

    //�������� ������ � ������� ������/������� ����������� ������� ( xml-��� "table" ������������� )
    //��� ������ � ������� ������ ����������� ������ ( xml-��� "list" ������������� )
    const UnicodeString FNumTCellAlign;        //������������ ( left | center | right | justify )
    const UnicodeString FNumTCellFontName;     //������������ ������
    const int        FNumTCellFontSize;     //������ ������ ( � ������� )
    const UnicodeString FNumTCellLTitle;       //��������� ������ � ������� ������ ����������� ������

    //*****************************

    /*�������������*/
    TTagNode* __fastcall InitNDHTML( bool bCreateBodyTag = true );
    UnicodeString __fastcall GetTempPath() const;
    /*��������������� ������*/
    void __fastcall CreateHTMLProgressInit(int AMax, UnicodeString AMsg);
    void __fastcall CreateHTMLProgressChange(int APercent, UnicodeString AMsg);
    void __fastcall CreateHTMLProgressComplite(UnicodeString AMsg);

    void __fastcall InternalInitPB();
    void __fastcall InternalSetPBMax();
    void __fastcall InternalIncPB();
    void __fastcall InternalSetPBPosition();
    void __fastcall InitPB();
    void __fastcall SetPBMax(const int nMax);
    void __fastcall IncPB();
    void __fastcall SetPBPosition(const int nPosition);
    void __fastcall InternalGenerateOnChangeCreateStageEvent();
    void __fastcall GenerateOnChangeCreateStageEvent( TICSDocViewerCreateStage ACreateStage );
    void __fastcall ChangeState( TICSDocViewerCreateStage AState, bool bGenerateOnChangeCreateStageEvent = true );

    UnicodeString __fastcall GetTableCellStyle( UnicodeString AStyleAttrName, TTagNode *ndCellTagNode, TTagNode *ndAltCellTagNode = NULL );
    UnicodeString __fastcall GetListStyle( UnicodeString AStyleAttrName, TTagNode *ndList );
    UnicodeString __fastcall GetListCellStyle( UnicodeString AStyleAttrName, TTagNode *ndCellTagNode );
    UnicodeString __fastcall GetInsTextStyle( TTagNode *ndInsText );
    UnicodeString __fastcall GetInsTextAlign( TTagNode *ndInsText );
    void __fastcall NormalizeNumericalValue(UnicodeString &ACellText, const bool fShowZVal);
    void __fastcall HiMetricToPixels(long *pnWidth, long *pnHeight);
    /*��������������� ������ ��� ������� HTML*/
    TTagNode* __fastcall AddHTMLTableTag( TTagNode *htmlParent );
    void __fastcall GetHTML_DIVAlignAttrVal( UnicodeString AAlign, UnicodeString &AAttrName, UnicodeString &AAttrVal );
    int __fastcall GetHTMLFontSizeMSOLike( const int nFontSize );
    TTagNode* __fastcall CreateHTMLTextFormat( TTagNode *htmlTag, UnicodeString AStyleUID );
    bool __fastcall SetHTMLTableCellBorderWidth( TTagNode *htmlTableCell, UnicodeString ABorderWidth );
    bool __fastcall SetHTMLBoxWidthValues( TTagNode *htmlTableCell, UnicodeString ABorderWidth, UnicodeString AWidth );
    TTagNode* __fastcall CreateHTMLTableNumCellTextFormat( TTagNode *htmlNumTableCell );
protected:
    /*TdsDocViewCreator*/
    /*��������������� ������*/
    void __fastcall SetTmpPath( UnicodeString AValue );
    void __fastcall SetDefSavePath( UnicodeString AValue );
    void __fastcall SetXMLContainer( const TAxeXMLContainer *AValue );
    void __fastcall SetMode( const TICSDocViewerMode AValue );
    void __fastcall SetUseCSS( const bool AValue );
    void __fastcall SetUseWordCSS( const bool AValue );
    void __fastcall SetUseExcelCSS( const bool AValue );
    void __fastcall SetOnChangeCreateStage( const TOnChangeCreateStageEvent AValue );
    void __fastcall SetDocFormat( const TICSDocViewerDocFormat AValue );
    void __fastcall SetSpecificator( const TTagNode *AValue );
    void __fastcall SetDocument( const TTagNode *AValue );
    void __fastcall SetDocAssign( const bool AValue );
    void __fastcall SetListZeroValCheckCountOnly( const bool AValue );

    TAxeXMLContainer* __fastcall GetXMLContainer() const;
    //*****************************
    bool __fastcall CheckIfTerminated();
    //*****************************
    UnicodeString __fastcall GetListColHeaderText( TTagNode *ndListCol );
    UnicodeString __fastcall GetTableHeaderCellText( TTagNode *ndHeaderCell );
    UnicodeString __fastcall GetDocListCellText( TTagNode* &ndV, const TTagNode *ndListCol, const bool bShowZVal );
    __int64 __fastcall GetListColsMaxLevel(TTagNode *ANode ) const;
    int __fastcall GetMaxPBForSpec();
    int __fastcall GetMaxPBForDoc();
    int __fastcall GetMaxPBForDocByMSWordTemplate();
    void __fastcall InternalPreviewCreate();
    /*��������������� ������ ��� ������� HTML*/

    UnicodeString __fastcall GetHTMLStyleDefinition( UnicodeString  AStyleClass,
                                                  UnicodeString  AStyleClassStrPrefix,
                                                  UnicodeString  AStylePropStrPrefix,
                                                  TTagNode*         ndStyle );
    UnicodeString __fastcall GetHTMLSTYLESDefinition( UnicodeString  AStyleClassStrPrefix,
                                                   UnicodeString  AStylePropStrPrefix,
                                                   TTagNode*         ndStyles );
    UnicodeString __fastcall GetMSOWordHTMLSTYLESDefinition(
      UnicodeString  AStyleClassStrPrefix,
      UnicodeString  AStylePropStrPrefix,
      TTagNode*         ndPageSetup
    );
    UnicodeString __fastcall GetMSOExcelHTMLSTYLESDefinition(
      UnicodeString  AStyleClassStrPrefix,
      UnicodeString  AStylePropStrPrefix,
      TTagNode*         ndPageSetup
    );
    UnicodeString __fastcall GetTableHTMLSTYLESDefinition( UnicodeString  AStyleClassStrPrefix,
                                                        UnicodeString  AStylePropStrPrefix );
    void __fastcall CreateHTML( TICSDocViewerPreviewType APreviewType );
    void __fastcall CreateHTMLListHeaderCell( TTagNode *ndListEl, TTagNode *htmlList, TTagNode *htmlRow = NULL );
    void __fastcall CreateHTMLListHeader( TTagNode *ndListEl, TTagNode *htmlList, TTagNodeList *AListSingleCols = NULL );
    void __fastcall CreateHTMLListBodyRow( const TTagNodeList *AListSingleCols, TTagNode *htmlList, TTagNodeList *ADataCells = NULL );
    TTagNode* __fastcall CreateHTMLTableHeaderCell( UnicodeString  ACellTagNodeName,
                                                    TTagNode*         ndCellTagNode,
                                                    TTagNode*         htmlRow );
    void __fastcall CreateHTMLTableHeaderCols( TTagNode *ndCol, TTagNode *htmlTable, TTagNode *htmlRow = NULL );
    void __fastcall CreateHTMLTableCols( TTagNode *ndColumns, TTagNode *htmlTable, TTagNodeList *ATableSingleCols = NULL );
    void __fastcall CreateHTMLTableRows( TTagNode *ndRow, TTagNode *htmlTable, const TTagNodeList &ATableSingleCols, TTagNodeList2 *DataCells, TTagNode *htmlRow = NULL );
    TTagNode* __fastcall AddHTMLInscription( TTagNode *ndInscription, TTagNode *htmlParent );
    TTagNode* __fastcall AddHTMLInsText( TTagNode *ndInsText, TTagNode **htmlParent );
    TTagNode* __fastcall AddHTMLList( TTagNode *ndList, TTagNode *htmlParent, TTagNodeList *AListSingleCols = NULL, int nRowCount = 1 );
    TTagNode* __fastcall AddHTMLTable( TTagNode *ndTable, TTagNode *htmlParent, TTagNodeList2 *DataCells = NULL );
    void __fastcall CreateSpecHTML();
    void __fastcall CreateDocHTML();
    void __fastcall InitHTMLPreview();
    /*��������������� ������ ��� ������� MSWord*/
    void __fastcall CreateSpecMSWord();
    void __fastcall CreateDocMSWord();
    void __fastcall InitMSWordPreview();
    /*��������������� ������ ��� ��� ������������ ������ �� ������� MSWord*/
    bool __fastcall CreateMSWordDocByTemplate();
    void __fastcall CreateSpecByMSWordTemplate();
    void __fastcall CreateDocByMSWordTemplate();


public:
    __fastcall TdsDocViewCreator(TComponent* Owner);
    virtual __fastcall ~TdsDocViewCreator();


    //������������ ( xml-������ � ������ "specificator" ) - ��� ��������� �������� �������� Assign
    __property TTagNode*  Specificator = { read = FSpec,        write = SetSpecificator };
    //�������� ( xml-������ � ������ "docum" ) - ��� ��������� �������� � ����������� ��
    //�������� DocAssign �������� ��� �� �������� Assign
    __property TTagNode*  Document     = { read = FDoc,         write = SetDocument     };
    //���� � �������� ��� ������������ ��������� ������ � ������ vmSaveToDisk (�� ��������� ���� �
    //���������� ���������� ��������)
    __property UnicodeString TmpPath      = { read = FTmpPath,     write = SetTmpPath      };
    //���� � �������� �� ��������� ��� ���������� ���������������� HTML-�����
    __property UnicodeString DefSavePath  = { read = FDefSavePath, write = SetDefSavePath  };
    //XML-��������� ( ��. AxeUtil.h ) � ������������� !!! SetXMLContainer �� ������ Assign !!!
    __property TAxeXMLContainer*  XMLContainer = { read = FXMLContainer, write = SetXMLContainer };
    //������ MS Word, �� ������ �������� �������� ����������� - ��� ��������� �������� ��������
    //�� �������� Assign
    __property TTagNode* XMLTemplate = { read = FDocXMLTemplate, write = FSetDocXMLTemplate };
    //������ ��� ����� ICS_Word.dot
//    __property UnicodeString  ICSWordTemplateFN = { read = FICSWordTemplateFN, write = FICSWordTemplateFN };

    //*****************************

    //������������ ����������� ���������/������������� � ������ �������
    //���������������� ���������
    UnicodeString __fastcall CreatePreview( TICSDocViewerPreviewType APreviewType, TAxeXMLContainer* AXMLList = NULL, TTagNode* ASpec = NULL, TTagNode* ADoc = NULL);


__published:
    /*��������*/
    //������ ������������ ���������
    __property TICSDocViewerDocFormat DocFormat  = { read = FDocFormat,   write = SetDocFormat,   default = dfHTML       };
    //����� ������ ������������
    __property TICSDocViewerMode      Mode       = { read = FMode,        write = SetMode,        default = vmSaveToDisk };
    //���������� ������������� ��� ������������ HTML-��������� CSS
    __property bool                   UseCSS     = { read = FUseCSS,      write = SetUseCSS,      default = true         };
    //���������� ������������� ��� ������������ HTML-��������� CSS ��� ������������� ���������� �������� �
    //MS Word 2000/XP/2003
    __property bool                   UseWordCSS = { read = FUseWordCSS,  write = SetUseWordCSS,  default = true         };
    //���������� ������������� ��� ������������ HTML-��������� CSS ��� ������������� ���������� �������� �
    //MS Excel 2000/XP/2003
    __property bool                   UseExcelCSS = { read = FUseExcelCSS,  write = SetUseExcelCSS,  default = true         };
    //��������� ������������ - ���� ������������ ����������� �������������/���������
    __property TICSDocViewerCreateStage State = { read = FState };
    //������� ������������� assign'� ��� ��������� ( �������� Document )
    __property bool                   DocAssign = { read = FDocAssign, write = SetDocAssign, default = false };
    //������� ���������� ������� ����������� ������� �������� ��� ������ ������ ���
    //��������, ��������������� count'�
    __property bool                   ListZeroValCheckCountOnly = { read = FListZeroValCheckCountOnly, write = SetListZeroValCheckCountOnly, default = true };

    /*�������*/
    //��������� ��� ����� ����� ������������ ����������� �������������/���������
    __property TOnChangeCreateStageEvent OnChangeCreateStage = { read = FOnChangeCreateStage, write = SetOnChangeCreateStage };

};

//---------------------------------------------------------------------------

} // end of namespace dsDocViewCreator
using namespace dsDocViewCreator;

#endif
