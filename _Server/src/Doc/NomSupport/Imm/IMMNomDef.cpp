/*
 ����������               ���.
 �����������              �����.
 ��������                 ����.
 ������                   ����.
 ��������                 �����.
 ����                     ����
 �������                  �����.
 ���������                �����.
 �����                    �����
 �������� ���������       ����.
 ��-���������             ��-���.
 ��������������� �������� ������.
 ��������                 �����.
 ������� �                ���.�
 ���-��������             ���.
 �������������� ��������  �����.
 ������������             �����.
 ���������                �����.
 �����������              �������.
 ��������� ����           ���.��.
 ���������                ����.
 ������� ���              ��.���
 ����                     ����
 ���������                ���������
 �������� ����            ����.����
 ������� �                ���.�
 ������                   ������
 ���������� �����         ���.�����
 ������������ ��������    �������.
 ������ ���������         ����.���.
 ����� ��������� �������� ���
 */
#include "IMMNomDef.h"
#include <System.DateUtils.hpp>
//#include <Registry.hpp>
//#include "RegPPConkretiz.h"
//#include "SchConkretiz.h"
//#include "ProbReak.h"
#define flVal(a) AFields->Values[AFields->Names[a]]
#define quFNStr(a) FDM->Query->FieldByName(a)->AsString
#define quFNInt(a) FDM->Query->FieldByName(a)->AsInteger
#define quFNFloat(a) FDM->Query->FieldByName(a)->AsFloat
#define quFNDate(a) FDM->Query->FieldByName(a)->AsDateTime
//---------------------------------------------------------------------------
__fastcall TICSIMMNomDef::TICSIMMNomDef(TFDQuery * AQuery, TAxeXMLContainer * AXMLList, UnicodeString ARegGUI,
    TdsCommClassData * AClsData) : TICSBaseNomDef(AQuery, AXMLList, ARegGUI)
 {
  dsLogC(__FUNC__);
  FQuery   = AQuery;
  FClsData = AClsData;
  dsLogC(__FUNC__, true);
 }
//---------------------------------------------------------------------------
__fastcall TICSIMMNomDef::~TICSIMMNomDef()
 {
  dsLogD(__FUNC__);
  dsLogD(__FUNC__, true);
 }
//---------------------------------------------------------------------------
TGetAttrFunc __fastcall TICSIMMNomDef::GetAttrMethodAddr(UnicodeString AFuncName)
 {
  TGetAttrFunc RC = NULL;
  if (AFuncName == "APlanVac")
   RC = APlanVac;
  else if (AFuncName == "APlanItemStr")
   RC = APlanItemStr;
  else if (AFuncName == "APlanItemVacType")
   RC = APlanItemVacType;
  else if (AFuncName == "APlanExecMIBP")
   RC = APlanExecMIBP;
  else if (AFuncName == "APlanPeriod")
   RC = APlanPeriod;
  else if (AFuncName == "APlanInfNotExec")
   RC = APlanInfNotExec;
  else if (AFuncName == "APlanMIBPNotExec")
   RC = APlanMIBPNotExec;
  else if (AFuncName == "AOrgLevels")
   RC = AOrgLevels;
  //else if (AFuncName == "AAddrValue")       RC = AAddrValue;
  //else if (AFuncName == "AFullAddrValue")   RC = AFullAddrValue;
  else if (AFuncName == "APatPrivAge")
   RC = APatPrivAge;
  else if (AFuncName == "ASchName")
   RC = ASchName;
  //else if (AFuncName == "ASetOrgCode")      RC = ASetOrgCode;
  //else if (AFuncName == "ASetLPUName")      RC = ASetLPUName;
  //else if (AFuncName == "ASetLPUFullName")  RC = ASetLPUFullName;
  //else if (AFuncName == "ASetAddr")         RC = ASetAddr;
  //else if (AFuncName == "ASetPhone")        RC = ASetPhone;
  //else if (AFuncName == "ASetEmail")        RC = ASetEmail;
  if (!RC)
   RC = TICSBaseNomDef::GetAttrMethodAddr(AFuncName);
  return RC;
 }
//---------------------------------------------------------------------------
TGetDefFunc __fastcall TICSIMMNomDef::GetDefMethodAddr(UnicodeString AFuncName)
 {
  TGetDefFunc RC = NULL;
  if (AFuncName == "DefDatePeriodLast")
   RC = DefDatePeriodLast;
  else if (AFuncName == "DefOtvodDatePeriodLast")
   RC = DefOtvodDatePeriodLast;
  else if (AFuncName == "DefInqTreeValue")
   RC = DefInqTreeValue;
  else if (AFuncName == "DefPlanPrivReak")
   RC = DefPlanPrivReak;
  else if (AFuncName == "DefPlanProbReak")
   RC = DefPlanProbReak;
  else if (AFuncName == "DefPlanPres")
   RC = DefPlanPres;
  else if (AFuncName == "DefPlanItem")
   RC = DefPlanItem;
  else if (AFuncName == "DefPlanItemExec")
   RC = DefPlanItemExec;
  else if (AFuncName == "DefExtEditValue")
   RC = DefExtEditValue;
  else if (AFuncName == "DefVacSch")
   RC = DefVacSch;
  else if (AFuncName == "DefTestSch")
   RC = DefTestSch;
  if (!RC)
   RC = TICSBaseNomDef::GetDefMethodAddr(AFuncName);
  return RC;
 }
//---------------------------------------------------------------------------
TGetSQLFunc __fastcall TICSIMMNomDef::GetSQLMethodAddr(UnicodeString AFuncName)
 {
  TGetSQLFunc RC = NULL;
  if (AFuncName == "SQLDatePeriodLast")
   RC = SQLDatePeriodLast;
  else if (AFuncName == "SQLOtvodDatePeriodLast")
   RC = SQLOtvodDatePeriodLast;
  else if (AFuncName == "SQLInqTreeValue")
   RC = SQLInqTreeValue;
  else if (AFuncName == "SQLPlanPrivReak")
   RC = SQLPlanPrivReak;
  else if (AFuncName == "SQLPlanProbReak")
   RC = SQLPlanProbReak;
  else if (AFuncName == "SQLPlanPres")
   RC = SQLPlanPres;
  else if (AFuncName == "SQLPlanItem")
   RC = SQLPlanItem;
  else if (AFuncName == "SQLPlanItemExec")
   RC = SQLPlanItemExec;
  else if (AFuncName == "SQLExtEditValue")
   RC = SQLExtEditValue;
  else if (AFuncName == "SQLVacSch")
   RC = SQLVacSch;
  else if (AFuncName == "SQLTestSch")
   RC = SQLTestSch;
  if (!RC)
   RC = TICSBaseNomDef::GetSQLMethodAddr(AFuncName);
  return RC;
 }
//---------------------------------------------------------------------------
int __fastcall TICSIMMNomDef::CorrectDay(Word Year, Word Month, Word Day)
 {
  Word DaysCount = DaysInAMonth(Year, Month);
  if (Day > DaysCount)
   Day = DaysCount;
  return Day;
 }
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::CalcPlanPeriod(TDate * ADateBP, TDate * ADateEP, TDate * APlanMonth)
 {//����������� ������� ������������ � ������ ������������
  //return: (������� ����� < ���� ������ ������������)
  TDate CurDate;
  Word DayNP, CurYear, CurMonth, CurDay, DayBP, MonthBP, YearBP, DayEP, MonthEP, YearEP;
  if ((int)(*APlanMonth))
   CurDate = (*APlanMonth);
  else
   CurDate = Date();
  DecodeDate(CurDate, CurYear, CurMonth, CurDay);
  //TRegIniFile *IMMIni = new TRegIniFile ("IMM");
  //IMMIni->RootKey = HKEY_LOCAL_MACHINE;
  //IMMIni->OpenKey(FDM->RegKey,false);
  DayNP = 25; //IMMIni->ReadInteger("LPUParam","DAY_PLAN",25);
  //IMMIni->CloseKey();
  //delete IMMIni;
  if (CurDay < DayNP)
   {
    MonthEP = CurMonth;
    YearEP  = CurYear;
    MonthBP = Dateutils::MonthOf(Sysutils::IncMonth(CurDate, -1));
    YearBP  = (MonthEP == 1) ? YearEP - 1 : YearEP;
   }
  else
   {
    MonthBP = CurMonth;
    YearBP  = CurYear;
    MonthEP = Dateutils::MonthOf(Sysutils::IncMonth(CurDate));
    YearEP  = (MonthBP == 12) ? CurYear + 1 : CurYear;
   }
  DayBP = CorrectDay(YearBP, MonthBP, DayNP);
  DayEP = CorrectDay(YearEP, MonthEP, 99);
  (*ADateBP) = EncodeDate(YearBP, MonthBP, DayBP);
  (*ADateEP) = EncodeDate(YearEP, MonthEP, DayEP);
  if ((* ADateEP) > CurDate)
   (* ADateEP) = CurDate;
  (*APlanMonth) = EncodeDate(YearEP, MonthEP, 1);
  return (Date().FormatString("dd").ToIntDef(1) < DayNP);
 }
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#                   ��������� ��������� �������� ���������                   #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
/*
 void __fastcall TICSIMMNomDef::ASetOrgCode(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
 {
 FGetOrgParam(ARet, "CODE");
 }
 //---------------------------------------------------------------------------
 void __fastcall TICSIMMNomDef::ASetLPUName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet, UnicodeString  AAttrRef)
 {
 FGetOrgParam(ARet, "NAME");
 }
 //---------------------------------------------------------------------------
 void __fastcall TICSIMMNomDef::ASetLPUFullName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
 {
 FGetOrgParam(ARet, "FULLNAME");
 }
 //---------------------------------------------------------------------------
 void __fastcall TICSIMMNomDef::ASetAddr(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
 {
 FGetOrgParam(ARet, "ADDR");
 }
 //---------------------------------------------------------------------------
 void __fastcall TICSIMMNomDef::ASetPhone(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
 {
 FGetOrgParam(ARet, "PHONE");
 }
 //---------------------------------------------------------------------------
 void __fastcall TICSIMMNomDef::ASetEmail(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
 {
 FGetOrgParam(ARet, "EMAIL");
 }
 //---------------------------------------------------------------------------
 */
void __fastcall TICSIMMNomDef::APlanVac(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
    UnicodeString AAttrRef)
 {
  UnicodeString DDD = "";
  if (flVal(1) == "0")
   {//����
    TReplaceFlags rFlag;
    rFlag << rfReplaceAll;
    TStringList * tmp = new TStringList;
    tmp->Text = StringReplace(flVal(4), "#", "\n", rFlag);
    DDD += FClsData->VacList[flVal(2)]; //������������ �������
    if (tmp->Count)
     {
      tmp->Delete(0);
      if (tmp->Count == 1)
       {
        DDD += " " + _UID(tmp->Strings[0]);
       }
      else
       {
        //���������, �� ������ ���������� ����� ����������
        bool isAll = true;
        for (int i = 1; i < tmp->Count; i++)
         if (_UID(tmp->Strings[i]) != _UID(tmp->Strings[0]))
          isAll = false;
        if (isAll)
         {//���� ���������� ���������
          if (_UID(tmp->Strings[0]).Trim().UpperCase() == "D")
           DDD += " ���.";
          else
           DDD += " " + _UID(tmp->Strings[0]);
         }
        else
         {//���� ���������� ������
          DDD += " (";
          for (int i = 0; i < tmp->Count; i++)
           if (tmp->Strings[i].Length())
            {
             if (_UID(tmp->Strings[i]).Trim().UpperCase() == "D")
              DDD += FClsData->ShortInfList[_GUI(tmp->Strings[i])] + " ���.;";
             else
              DDD += FClsData->ShortInfList[_GUI(tmp->Strings[i])] + " " + _UID(tmp->Strings[i]) + ";";
            }
          DDD += ")";
         }
       }
     }
    else
     DDD += " (���.)";
    delete tmp;
   }
  else
   {//�����
    DDD += FClsData->TestList[flVal(3)];
   }
  ARet = DDD;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSIMMNomDef::GetVacType(UnicodeString ASrc)
 {
  //ASrc - 15.V1;5.V2;4.V2;3.V2;2.V1;
  UnicodeString RC = "������ : " + ASrc;
  try
   {
    UnicodeString FType, FLastType;
    UnicodeString FTypeList = ASrc;
    UnicodeString FInfType = GetLPartB(FTypeList, ';');
    FLastType = GetRPartB(FInfType, '.');
    bool FAllEqu = true;
    while (FInfType.Length() && FAllEqu)
     {
      FType     = GetRPartB(FInfType, '.');
      FAllEqu   = (FLastType == FType);
      FTypeList = GetRPartB(FTypeList, ';');
      FInfType  = GetLPartB(FTypeList, ';');
     }
    if (FAllEqu)
     RC = FLastType;
    else
     {
      RC        = "";
      FTypeList = ASrc;
      FInfType  = GetLPartB(FTypeList, ';');
      while (FInfType.Length())
       {
        RC += GetRPartB(FInfType, '.') + "-" + FClsData->ShortInfList[GetLPartB(FInfType, '.')] + "; ";
        FTypeList = GetRPartB(FTypeList, ';');
        FInfType  = GetLPartB(FTypeList, ';');
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::APlanItemStr(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
    UnicodeString AAttrRef)
 {
  UnicodeString RC = "������, �� �������� ��� ��������.";
  //�������� R32A6,
  //��� R1086,   1/2/3
  //���� R1089,
  //��� R108B
  int FType = AFields->Values["R1086"].ToIntDef(0);
  if (FType)
   {//������ ���� R1086 - ���� �� ���������
    if (FType == 1)
     {                               //����
      RC = AFields->Values["R108B"]; //���
      RC += " " + FClsData->VacList[AFields->Values["R1089"]]; //������������ �������
     }
    else if (FType == 2)
     {//�����
      RC = FClsData->TestList[AFields->Values["R1089"]];
     }
    else if (FType == 3)
     {//��������
      RC = FClsData->CheckList[AFields->Values["R1089"]];
     }
   }
  else
   {//�� ������ ���� R1086 - ���� �� ��������
    //�������� R32A6,
    //��� R3086,   1/2/3
    //���� R3089,
    //��� R308B
    //R3086, R3089, R308B'
    int FType = AFields->Values["R3086"].ToIntDef(0);
    if (FType == 1)
     {                                           //����
      RC = GetVacType(AFields->Values["R308B"]); //���
      RC += " " + FClsData->VacList[AFields->Values["R3089"]]; //������������ �������
     }
    else if (FType == 2)
     {//�����
      RC = FClsData->TestList[AFields->Values["R3089"]];
     }
    else if (FType == 3)
     {//��������
      RC = FClsData->CheckList[AFields->Values["R3089"]];
     }
   }
  ARet = RC;
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::APlanItemVacType(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
    UnicodeString AAttrRef)
 {
  ARet = GetVacType(AFields->Values["R308B"]); //���
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::APlanExecMIBP(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
    UnicodeString AAttrRef)
 {
  UnicodeString RC = "������, �� �������� ��� ��������.";
  //�������� R32A6,      R1086, R32AA
  //��� R1086,   1/2/3
  //���� R1089,
  //��� R108B
  int FType = AFields->Values["R1086"].ToIntDef(0);
  if (FType)
   {//������ ���� R1086 - ���� �� ���������
    if (FType == 1)
     {                                                  //����
      RC = FClsData->VacList[AFields->Values["R32AA"]]; //������������ �������
     }
    else if (FType == 2)
     {//�����
      RC = FClsData->TestList[AFields->Values["R32AA"]];
     }
    else if (FType == 3)
     {//��������
      RC = FClsData->CheckList[AFields->Values["R32AA"]];
     }
   }
  ARet = RC;
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::APlanInfNotExec(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet, UnicodeString AAttrRef)
 {
  //PlanInfNotExec  R1098,R3303
  int FExec = AFields->Values["R1098"].ToIntDef(0);
  if (!FExec)
   {//�� ���������
    TTagNode * FChoice = FDM->XMLList->GetRefer(ARef);
    if (FChoice->CmpName("choice"))
     {
      FChoice = FChoice->GetChildByAV("choicevalue", "value", AFields->Values["R3303"]);
      if (FChoice)
       ARet = FChoice->AV["name"];
     }
   }
  else
   ARet = "";
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::APlanMIBPNotExec(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet, UnicodeString AAttrRef)
 {
  //PlanMIBPNotExec R3098,R32FC
  int FExec = AFields->Values["R3098"].ToIntDef(0);
  if (FExec != 1)
   {//�� ��������� ��� ��������� ��������
    TTagNode * FChoice = FDM->XMLList->GetRefer(ARef);
    if (FChoice->CmpName("choice"))
     {
      FChoice = FChoice->GetChildByAV("choicevalue", "value", AFields->Values["R32FC"]);
      if (FChoice)
       ARet = FChoice->AV["name"];
     }
   }
  else
   ARet = "";
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::APlanPeriod(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
    UnicodeString AAttrRef)
 {
  UnicodeString RC = flVal(0);
  TDate FDate;
  if (TryStrToDate(RC, FDate))
   {
    RC = FDate.FormatString("mmmm yyyy");
   }
  ARet = RC;
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::AOrgLevels(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
    UnicodeString AAttrRef)
 {
  ARet = StringReplace(flVal(0), "$", "", TReplaceFlags() << rfReplaceAll).Trim();
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::APatPrivAge(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
    UnicodeString AAttrRef)
 {
  try
   {
    ARet = AgeStr(StrToDate(AFields->Values[AFields->Names[1]]), StrToDate(AFields->Values[AFields->Names[0]]));
   }
  catch (EConvertError & E)
   {
    ARet = "";
   }
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::ASchName(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
    UnicodeString AAttrRef)
 {
  if (AFields->Count)
   {
    UnicodeString RC = "";
    UnicodeString flName = GetPart1(AFields->Strings[0], '=').UpperCase();
    TTagNode * SchList;
    UnicodeString xName, xTab;
    if ((flName == "R1092") || (flName == "R1078"))
     {//��������
      SchList = FDM->XMLList->GetXML("12063611-00008cd7-cd89");
      SchList = SchList->GetTagByUID(_GUI(GetPart2(AFields->Strings[0], '=').UpperCase()));
      xName   = "R0040";
      xTab    = "CLASS_0035";
     }
    else if ((flName == "R1093") || (flName == "R1079"))
     {//�����
      SchList = FDM->XMLList->GetXML("001D3500-00005882-DC28");
      SchList = SchList->GetTagByUID(_GUI(GetPart2(AFields->Strings[0], '=').UpperCase()));
      xName   = "R002C";
      xTab    = "CLASS_002A";
     }
    if (SchList)
     {
      FDM->qtExec("Select " + xName + " From " + xTab + " Where code=" + SchList->AV["ref"]);
      if (FDM->Query->RecordCount)
       RC = FDM->Query->FieldByName(xName)->AsString + " (������� " + SchList->AV["name"] + ")";
     }
    ARet = RC;
   }
  else
   ARet = "";
 }
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#            ��������� ��������� �������� �������������� ��������� (Def...)  #
//#                                                                            #
//#            ��������� ������������ ������ SQL-�������             (SQL...)  #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefDatePeriodLast(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  //AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
  //����� �� ���, �� �� �� �� ����� �����
  //��� ���� "����"
  //{
  //1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
  //1,2 ��� �������; 0 - �������� ���
  //1 - ������ �� ������� ����
  //2 - ������ �� ����
  //2-� - ���� ��;   ��� ���� "�������� ���"
  //��� ����� "������ �� ..." �������� �������
  //3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
  //��� ���� "������ �� ������� ����"  - �� �������������;
  //4-� - �������� - ��� ���� "�������� ���"  - �� �������������
  //�������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
  if (!FDM)
   return false;
  bool iRC = false;
  UnicodeString tmp = "";
  TTagNode * __tmp;
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  if (AIE->GetFirstChild())
   {
    if (AIE->Count)
     {
      tmp   = "";
      __tmp = AIE->GetFirstChild();
      int DType = AIE->GetFirstChild()->GetAVDef("value", "0").ToInt();
      bool FLast = (bool)(DType & 1);
      DType = DType >> 1;
      if (FLast)
       {
        TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
        if (!FISAttr->AV["add_ref"].Length())
         tmp = "������ ��������� ������������";
        else
         tmp = FISAttr->AV["add_ref"];
       }
      TTagNode * DateType = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam1 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam2 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam3 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam4 = __tmp;
      if (DType == 0)
       {//�������� ���
        if ((DateParam1 && (DateParam1->AV["value"].Length())) || (DateParam2 && (DateParam2->AV["value"].Length())))
         {
          tmp += " " + DateType->AV["name"];
          if (DateParam1->AV["value"].Length())
           {
            tmp += " " + DateParam1->AV["name"];
            tmp += "='" + DateParam1->AV["value"] + "'";
           }
          if (DateParam2->AV["value"].Length())
           {
            tmp += " " + DateParam2->AV["name"];
            tmp += "='" + DateParam2->AV["value"] + "'";
           }
         }
       }
      else if (DType == 3)
       {//������ ������������ ���������
        tmp += " " + DateType->AV["name"];
        if (DateParam3->AV["value"].Length())
         {
          tmp += " " + DateParam3->AV["name"];
          tmp += "='" + DateParam3->AV["value"] + "'";
         }
       }
      else
       {//������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
        tmp += " " + DateType->AV["name"];
        if (DateParam1->AV["value"].Length())
         {
          tmp += " " + DateParam1->AV["name"];
          tmp += "='" + DateParam1->AV["value"] + "'";
         }
        if (DateParam2->AV["value"].Length())
         {
          tmp += " " + DateParam2->AV["name"];
          tmp += "='" + DateParam2->AV["value"] + "'";
         }
        if (DateParam3->AV["value"].Length())
         {
          tmp += " " + DateParam3->AV["name"];
          tmp += "='" + DateParam3->AV["value"] + "'";
         }
       }
      if (DateParam4)
       {
        tmp += " " + DateParam4->AV["name"];
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLDatePeriodLast(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode * FIS = FISAttr->GetParent("is");
  TTagNode * DateOffs, *__tmp;
  bool Otvod = false;
  ATab = FGetGUI(FISCondRules) + "." + FIS->AV["uid"] + ":";
  UnicodeString xFr, xTo, flName, xPer, xOffs, xTabName, xMaxSQL, xMaxTabName, MaxflName, FUnitRefFN;
  FUnitRefFN = "error";
  TTagNode * FRef = FIS->GetChildByAV("field_refer", "fieldout", "CODE", true);
  if (FRef)
   FUnitRefFN = FRef->AV["fieldin"];
  xFr      = "";
  xTo      = "";
  xTabName = FIS->AV["maintable"];
  flName   = FIS->AV["tabalias"] + "." + FISAttr->AV["fields"];
  int DType = AIE->GetFirstChild()->GetAVDef("value", "0").ToInt();
  bool FLast = (bool)(DType & 1);
  DType = DType >> 1;
  TTagNode * TagFr, *TagTo, *NotDop;
  __tmp       = AIE->GetFirstChild()->GetNext();
  TagFr       = __tmp;
  __tmp       = __tmp->GetNext();
  TagTo       = __tmp;
  __tmp       = __tmp->GetNext();
  DateOffs    = __tmp;
  __tmp       = __tmp->GetNext();
  NotDop      = __tmp;
  xMaxTabName = "DTT" + IntToStr((int)FLast);
  xMaxSQL     = flName + " = (Select Max(" + xMaxTabName + "." + FISAttr->AV["fields"] + ") From " + xTabName + " " +
      xMaxTabName;
  xMaxSQL += " Where " + xMaxTabName + "." + FUnitRefFN + "=" + FIS->AV["tabalias"] + "." + FUnitRefFN;
  MaxflName = xMaxTabName + "." + FISAttr->AV["fields"];
  if (DType == 0)
   {//�������� ���
    if (TagFr && (TagFr->AV["value"].Length()))
     xFr = ">='" + _CorrectDate(TagFr->AV["value"]) + "'";
    if (TagTo && (TagTo->AV["value"].Length()))
     xTo = "<='" + _CorrectDate(TagTo->AV["value"]) + "'";
   }
  else if (DType == 1)
   {//������ �� ������� ����
    xPer  = TagFr->AV["value"];
    xOffs = DateOffs->AV["value"];
    if (!xOffs.Length())
     {
      xFr = ">='" + FIncPeriod(Date().FormatString("dd.mm.yyyy"), xPer, 1) + "'";
      xTo = "<='" + Date().FormatString("dd.mm.yyyy") + "'";
     }
    else
     {
      xFr = ">='" + FIncPeriod(Date().FormatString("dd.mm.yyyy"), AddPeriod(xPer, xOffs), 1) + "'";
      xTo = "<='" + FIncPeriod(Date().FormatString("dd.mm.yyyy"), xOffs, 1) + "'";
     }
   }
  else if (DType == 2)
   {//������ �� ���� ��������� ������� ������������ ���������
    UnicodeString FDocPer = GetPart1(UnicodeString(ARet), '#');
    if (FDocPer.Length())
     {
      if (GetPart2(FDocPer, ';').Length())
       FDocPer = GetPart2(FDocPer, ';');
      else
       FDocPer = Date().FormatString("dd.mm.yyyy");
     }
    else
     FDocPer = Date().FormatString("dd.mm.yyyy");
    xPer  = TagFr->AV["value"];
    xOffs = DateOffs->AV["value"];
    if (!xOffs.Length())
     {
      xFr = ">='" + FIncPeriod(FDocPer, xPer, 1) + "'";
      xTo = "<='" + FDocPer + "'";
     }
    else
     {
      xFr = ">='" + FIncPeriod(FDocPer, AddPeriod(xPer, xOffs), 1) + "'";
      xTo = "<='" + FIncPeriod(FDocPer, xOffs, 1) + "'";
     }
   }
  else if (DType == 3)
   {//������ ������������ ���������
    UnicodeString FPer = GetPart1(UnicodeString(ARet), '#');
    if (FPer.Length())
     {
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
        if (GetPart1(FPer, ';').Length())
         xFr = ">='" + TDate(GetPart1(FPer, ';')).FormatString("dd.mm.yyyy") + "'";
        if (GetPart2(FPer, ';').Length())
         xTo = "<='" + TDate(GetPart2(FPer, ';')).FormatString("dd.mm.yyyy") + "'";
       }
      else
       {
        if (GetPart1(FPer, ';').Length())
         xFr = ">='" + FIncPeriod(GetPart1(FPer, ';'), xOffs, 1) + "'";
        if (GetPart2(FPer, ';').Length())
         xTo = "<='" + FIncPeriod(GetPart2(FPer, ';'), xOffs, 1) + "'";
       }
     }
   }
  if (xFr.Length())
   {
    xMaxSQL += " and " + MaxflName + xFr;
    xFr = flName + xFr;
   }
  if (xTo.Length())
   {
    xMaxSQL += " and " + MaxflName + xTo;
    xTo = flName + xTo;
   }
  if (FLast)
   {
    TTagNode * tmp;
    //��������
    tmp = AIE->GetParent();
    tmp = tmp->GetChildByAV("IERef", "ref", "0E291426-00005882-2493." + FGetRefValue(FISAttr, "0070", "0086"));
    if (tmp)
     {
      if (tmp->Count > 1)//���������� �������� ?
       {
        TTagNode * itNode = tmp->GetFirstChild();
        while (itNode)
         {
          if (itNode->CmpAV("ref", "1"))//���������� �������� �������� !!!
               xMaxSQL += " and " + xMaxTabName + ".R1075=" + itNode->AV["value"].Trim();
          else if (itNode->CmpAV("ref", "2"))//���������� �������� ������� !!!
               xMaxSQL += " and " + xMaxTabName + ".R1020=" + itNode->AV["value"].Trim();
          //else if (itNode->CmpAV("ref","3"))  // ���������� �������� ����� !!!
          //xMaxSQL += " and "+FDM->UpperFunc+"("+xMaxTabName+".R1023)='"+itNode->AV["value"].Trim().UpperCase()+"'";
          //else if (itNode->CmpAV("ref","4"))  // ���������� �������� ���� ���������� !!!
          //xMaxSQL += " and "+FDM->UpperFunc+"("+xMaxTabName+".R1021)='"+itNode->AV["value"].Trim().UpperCase()+"'";
          itNode = itNode->GetNext();
         }
       }
      if (NotDop)
       {
        if (NotDop->CmpAV("value", "1"))
         xMaxSQL += " and Upper(" + xMaxTabName + ".R1021)<>'���.'";
       }
     }
    else
     {
      //�����
      tmp = AIE->GetParent();
      tmp = tmp->GetChildByAV("IERef", "ref", "0E291426-00005882-2493." + FGetRefValue(FISAttr, "0044", "00B0"));
      if (tmp)
       {
        tmp = tmp->GetChildByAV("text", "ref", "0");
        if (tmp)//���������� �������� ����� ?
         {
          xMaxSQL += " and " + xMaxTabName + ".R1032=" + tmp->AV["value"];
         }
       }
     }
    xMaxSQL += ")";
    ARet = xMaxSQL;
   }
  if (xFr.Length() && xTo.Length())
   {
    if (FLast)
     ARet = "(" + xFr + " and " + xTo + " and " + xMaxSQL + ")";
    else
     ARet = "(" + xFr + " and " + xTo + ")";
   }
  else if (xFr.Length())
   {
    if (FLast)
     ARet = "(" + xFr + " and " + xMaxSQL + ")";
    else
     ARet = xFr;
   }
  else if (xTo.Length())
   {
    if (FLast)
     ARet = "(" + xTo + " and " + xMaxSQL + ")";
    else
     ARet = xTo;
   }
  else
   {
    if (FLast)
     ARet = xMaxSQL;
    else
     ARet = "";
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefOtvodDatePeriodLast(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  //AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
  //����� �� ���, �� �� �� �� ����� �����
  //��� ���� "����"
  //{
  //1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
  //1,2 ��� �������; 0 - �������� ���
  //1 - ������ �� ������� ����
  //2 - ������ �� ����
  //2-� - ���� ��;   ��� ���� "�������� ���"
  //��� ����� "������ �� ..." �������� �������
  //3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
  //��� ���� "������ �� ������� ����"  - �� �������������;
  //4-� - �������� - ��� ���� "�������� ���"  - �� �������������
  //�������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
  if (!FDM)
   return false;
  bool iRC = false;
  UnicodeString tmp = "";
  TTagNode * __tmp;
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  if (AIE->GetFirstChild())
   {
    if (AIE->Count)
     {
      tmp   = "";
      __tmp = AIE->GetFirstChild();
      int DType = AIE->GetFirstChild()->GetAVDef("value", "0").ToInt();
      bool FLast = (bool)(DType & 1);
      DType = DType >> 1;
      if (FLast)
       {
        TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
        if (!FISAttr->AV["add_ref"].Length())
         tmp = "������ ��������� ������������";
        else
         tmp = FISAttr->AV["add_ref"];
       }
      TTagNode * DateType = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam1 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam2 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam3 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam4 = __tmp;
      if (DType == 0)
       {//�������� ���
        if ((DateParam1 && (DateParam1->AV["value"].Length())) || (DateParam2 && (DateParam2->AV["value"].Length())))
         {
          tmp += " " + DateType->AV["name"];
          if (DateParam1->AV["value"].Length())
           {
            tmp += " " + DateParam1->AV["name"];
            tmp += "='" + DateParam1->AV["value"] + "'";
           }
          if (DateParam2->AV["value"].Length())
           {
            tmp += " " + DateParam2->AV["name"];
            tmp += "='" + DateParam2->AV["value"] + "'";
           }
         }
       }
      else if (DType == 3)
       {//������ ������������ ���������
        tmp += " " + DateType->AV["name"];
        if (DateParam3->AV["value"].Length())
         {
          tmp += " " + DateParam3->AV["name"];
          tmp += "='" + DateParam3->AV["value"] + "'";
         }
       }
      else
       {//������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
        tmp += " " + DateType->AV["name"];
        if (DateParam1->AV["value"].Length())
         {
          tmp += " " + DateParam1->AV["name"];
          tmp += "='" + DateParam1->AV["value"] + "'";
         }
        if (DateParam2->AV["value"].Length())
         {
          tmp += " " + DateParam2->AV["name"];
          tmp += "='" + DateParam2->AV["value"] + "'";
         }
        if (DateParam3->AV["value"].Length())
         {
          tmp += " " + DateParam3->AV["name"];
          tmp += "='" + DateParam3->AV["value"] + "'";
         }
       }
      if (DateParam4)
       {
        tmp += " " + DateParam4->AV["name"];
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLOtvodDatePeriodLast(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode * FIS = FISAttr->GetParent("is");
  TTagNode * DateOffs, *__tmp;
  bool Otvod = false;
  Otvod = FISCondRules->CmpAV("uid", "007B,007C");
  ATab  = FGetGUI(FISCondRules) + "." + FIS->AV["uid"] + ":";
  UnicodeString xFr, xTo, flName, xPer, xOffs, xTabName, xTabAlias, xMaxSQL, xMaxTabName, MaxflName, FUnitRefFN; ;
  FUnitRefFN = "error";
  TTagNode * FRef = FIS->GetChildByAV("field_refer", "fieldout", "CODE", true);
  if (FRef)
   FUnitRefFN = FRef->AV["fieldin"];
  xFr       = "";
  xTo       = "";
  xTabName  = FIS->AV["maintable"];
  xTabAlias = xTabName;
  if (FIS->AV["tabalias"].Length())
   xTabAlias = FIS->AV["tabalias"];
  flName = xTabAlias + "." + FISAttr->AV["fields"];
  int DType = AIE->GetFirstChild()->GetAVDef("value", "0").ToInt();
  bool FLast = (bool)(DType & 1);
  DType = DType >> 1;
  TTagNode * TagFr, *TagTo, *NotDop;
  __tmp       = AIE->GetFirstChild()->GetNext();
  TagFr       = __tmp;
  __tmp       = __tmp->GetNext();
  TagTo       = __tmp;
  __tmp       = __tmp->GetNext();
  DateOffs    = __tmp;
  __tmp       = __tmp->GetNext();
  NotDop      = __tmp;
  xMaxTabName = "DTT" + IntToStr((int)FLast);
  xMaxSQL     = flName + " = (Select Max(" + xMaxTabName + "." + FISAttr->AV["fields"] + ") From " + xTabName + " " +
      xMaxTabName;
  xMaxSQL += " Where " + xMaxTabName + "." + FUnitRefFN + "=" + xTabAlias + "." + FUnitRefFN;
  MaxflName = xMaxTabName + "." + FISAttr->AV["fields"];
  if (DType == 0)
   {//�������� ���
    if (TagFr && (TagFr->AV["value"].Length()))
     xFr = "'" + _CorrectDate(TagFr->AV["value"]) + "'";
    if (TagTo && (TagTo->AV["value"].Length()))
     xTo = "'" + _CorrectDate(TagTo->AV["value"]) + "'";
   }
  else if (DType == 1)
   {//������ �� ������� ����
    xPer  = TagFr->AV["value"];
    xOffs = DateOffs->AV["value"];
    if (!xOffs.Length())
     {
      xFr = "'" + FIncPeriod(Date().FormatString("dd.mm.yyyy"), xPer, 1) + "'";
      xTo = "'" + Date().FormatString("dd.mm.yyyy") + "'";
     }
    else
     {
      xFr = "'" + FIncPeriod(Date().FormatString("dd.mm.yyyy"), AddPeriod(xPer, xOffs), 1) + "'";
      xTo = "'" + FIncPeriod(Date().FormatString("dd.mm.yyyy"), xOffs, 1) + "'";
     }
   }
  else if (DType == 2)
   {//������ �� ���� ��������� ������� ������������ ���������
    UnicodeString FDocPer = GetPart1(UnicodeString(ARet), '#');
    if (FDocPer.Length())
     {
      if (GetPart2(FDocPer, ';').Length())
       FDocPer = GetPart2(FDocPer, ';');
      else
       FDocPer = Date().FormatString("dd.mm.yyyy");
     }
    else
     FDocPer = Date().FormatString("dd.mm.yyyy");
    xPer  = TagFr->AV["value"];
    xOffs = DateOffs->AV["value"];
    if (!xOffs.Length())
     {
      xFr = "'" + FIncPeriod(FDocPer, xPer, 1) + "'";
      xTo = "'" + FDocPer + "'";
     }
    else
     {
      xFr = "'" + FIncPeriod(FDocPer, AddPeriod(xPer, xOffs), 1) + "'";
      xTo = "'" + FIncPeriod(FDocPer, xOffs, 1) + "'";
     }
   }
  else if (DType == 3)
   {//������ ������������ ���������
    UnicodeString FPer = GetPart1(UnicodeString(ARet), '#');
    if (FPer.Length())
     {
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
        if (GetPart1(FPer, ';').Length())
         xFr = "'" + TDate(GetPart1(FPer, ';')).FormatString("dd.mm.yyyy") + "'";
        if (GetPart2(FPer, ';').Length())
         xTo = "'" + TDate(GetPart2(FPer, ';')).FormatString("dd.mm.yyyy") + "'";
       }
      else
       {
        if (GetPart1(FPer, ';').Length())
         xFr = "'" + FIncPeriod(GetPart1(FPer, ';'), xOffs, 1) + "'";
        if (GetPart2(FPer, ';').Length())
         xTo = "'" + FIncPeriod(GetPart2(FPer, ';'), xOffs, 1) + "'";
       }
     }
   }
  UnicodeString frFlName, toFlName, frMaxFlName, toMaxFlName, CommSQL;
  frFlName    = xTabAlias + ".R105F";
  toFlName    = xTabAlias + ".R1065";
  frMaxFlName = xMaxTabName + ".R105F";
  toMaxFlName = xMaxTabName + ".R1065";
  CommSQL     = "";
  if (xFr.Length() && xTo.Length())
   {
    xMaxSQL += " and " + frMaxFlName + " <= " + xTo + " and (" + toMaxFlName + ">=" + xFr + " or " + toMaxFlName +
        " is NULL)";
    CommSQL = frFlName + " <= " + xTo + " and (" + toFlName + ">=" + xFr + " or " + toFlName + " is NULL)";
   }
  else if (xFr.Length())
   {
    xMaxSQL += " and (" + toMaxFlName + ">=" + xFr + " or " + toMaxFlName + " is NULL)";
    CommSQL = "(" + toFlName + ">=" + xFr + " or " + toFlName + " is NULL)";
   }
  else if (xTo.Length())
   {
    xMaxSQL += " and " + frMaxFlName + " <= " + xTo;
    CommSQL = frFlName + " <= " + xTo;
   }
  if (FLast)
   {
    TTagNode * tmp;
    tmp = AIE->GetParent();
    tmp = tmp->GetChildByAV("IERef", "ref", "0E291426-00005882-2493." + FGetRefValue(FISAttr, "004F", "00BB"));
    if (tmp)
     {
      tmp = tmp->GetChildByAV("text", "ref", "0");
      if (tmp)//���������� �������� �������� ?
       {
        xMaxSQL += " and " + xMaxTabName + ".R1083=" + tmp->AV["value"];
       }
     }
    tmp = AIE->GetParent();
    tmp = tmp->GetChildByAV("IERef", "ref", "0E291426-00005882-2493." + FGetRefValue(FISAttr, "004E", "00BA"));
    if (tmp)
     {
      tmp = tmp->GetChildByAV("text", "ref", "0");
      if (tmp)//�������� ��� ������ ?
       {
        xMaxSQL += " and " + xMaxTabName + ".R1043=" + tmp->AV["value"];
       }
     }
    xMaxSQL += ")";
   }
  if (CommSQL.Length())
   {
    if (FLast)
     ARet = "(" + CommSQL + " and " + xMaxSQL + ")";
    else
     ARet = "(" + CommSQL + ")";
   }
  else
   {
    if (FLast)
     ARet = xMaxSQL;
    else
     ARet = "";
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefInqTreeValue(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  bool iRC = false;
  UnicodeString tmp = "";
  TTagNode * __tmp;
  if (AIE->GetFirstChild())
   {
    tmp = "";
    if (AIE->Count > 1)
     {
      if (AIE->GetFirstChild()->GetNext())
       {
        if (AIE->GetFirstChild()->GetNext()->AV["value"] != "0")
         tmp += "{��������� ���������}";
       }
      tmp += AIE->GetFirstChild()->AV["name"];
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLInqTreeValue(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode * FIS = FISAttr->GetParent("is");
  UnicodeString FTabName = FIS->AV["tabalias"];
  UnicodeString FColName = FTabName + "." + FISAttr->AV["fields"];
  UnicodeString ISTab = FGetGUI(FISCondRules) + "." + FIS->AV["uid"] + ":";
  ATab = ISTab;
  if (!AIE->Count)
   {
    ARet = "";
   }
  else
   {
    TTagNode * FMCode = AIE->GetChildByAV("text", "ref", "1");
    if (FMCode)
     {
      if (FMCode->CmpAV("value", "1"))
       ARet = "(" + FColName + " >= " + FMCode->GetNext()->AV["value"] + " and " + FColName + " <= " +
           FMCode->GetNext()->GetNext()->AV["value"] + ")";
      else
       ARet = " " + FColName + " = " + FMCode->GetNext()->AV["value"];
     }
    else
     ARet = "";
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefPlanPrivReak(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  UnicodeString tmp = "";
  TTagNode * __tmp;
  return FDM->DefRegValue(AIE, AText, AXMLIE, tmp, "", false, false);
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLPlanPrivReak(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  GetSQLValue(AIE, ATab, ARet);
  if (!ARet.Trim().Length())
   {
    UnicodeString FTmpl = "";
    TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
    if (FISCondRules)
     GetSQLDefValue(AIE, FISCondRules->GetChildByName("root")->AsXML);
    GetSQLValue(AIE, ATab, ARet);
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefPlanProbReak(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  UnicodeString tmp;
  TTagNode * __tmp;
  bool iRC = false;
  if (AIE->GetFirstChild())
   {
    if (AIE->Count > 1)
     {
      __tmp = AIE->GetFirstChild();
      tmp   = "";
      while (__tmp->GetNext())
       {
        tmp += " " + __tmp->AV["name"];
        __tmp = __tmp->GetNext();
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLPlanProbReak(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  try
   {
    TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
    TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
    TTagNode * FIS = FISAttr->GetParent("is");
    ATab = FGetGUI(FISCondRules) + "." + FIS->AV["uid"] + ":";
    UnicodeString FSQL = "";
    TTagNode * _tmp;
    /*
     }
     Result->AddChild("text","name=����������� �������� ������:"+FromED->Text+",value="+FromED->Text+",ref=5");
     if (ReakTypeCB->ItemIndex == 0)
     {
     Result->AddChild("text","name=����������:"+ToED->Text+",value="+ToED->Text+",ref=6");
     if (ToED2->Text.Length())
     Result->AddChild("text","name=���������� �� ���������:"+ToED2->Text+",value="+ToED2->Text+",ref=7");
     }
     */
    int ChDif = GetChildContAVDef(AIE, "4", "-1").ToIntDef(-1); //������ ���������
    int FFixVal, FDelta, FDelta2;
    UnicodeString FTmpl = "";
    if (FISCondRules)
     GetSQLDefValue(AIE, FISCondRules->GetChildByName("root")->AsXML);
    GetSQLValue(AIE, ATab, ARet);
    if (ChDif != -1)
     {
      if (ARet.Trim().Length())
       ARet += " and ";
      if (!ChDif)
       {//������ ����������
        FFixVal = GetChildContAVDef(AIE, "5", "0").ToIntDef(0); //����������� �������� ������
        FDelta  = GetChildContAVDef(AIE, "6", "0").ToIntDef(0); //����������
        FDelta2 = GetChildContAVDef(AIE, "7", "0").ToIntDef(0); //���������� �� ���������
        if (!(FDelta2 > 0))
         FDelta2 = FFixVal;
        ARet += "PROB.R10A7=2 and ( ";
        ARet += "(PROB.R103F >= " + IntToStr(FFixVal) + " and PROB.R10C2=2 and ";
        ARet += "(PROB.R103F - PROB.R10C1) >= " + IntToStr(FDelta) + ") or ";
        ARet += "(PROB.R103F >= " + IntToStr(FDelta2) + " and PROB.R10C2=7))";
       }
      else
       {//������ ������
        FFixVal = GetChildContAVDef(AIE, "5", "0").ToIntDef(0); //����������� �������� ������
        ARet += "PROB.R103F > " + IntToStr(FFixVal) + " and PROB.R10A7=2 and (PROB.R10C1<2 or PROB.R10C1 is NULL)";
       }
     }
   }
  catch (Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefPlanPres(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  bool iRC = false;
  UnicodeString tmp;
  if (AIE->GetFirstChild())
   {
    if (AIE->Count > 1)
     {
      TTagNode * __tmp = AIE->GetFirstChild();
      tmp = "";
      while (__tmp)
       {
        if (!__tmp->CmpAV("name", "sql"))
         tmp += " " + __tmp->AV["name"];
        __tmp = __tmp->GetNext();
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefPlanItem(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  bool iRC = false;
  UnicodeString tmp;
  if (AIE->GetFirstChild())
   {
    if (AIE->Count > 1)
     {
      TTagNode * __tmp = AIE->GetFirstChild();
      tmp = "";
      while (__tmp)
       {
        if (!__tmp->CmpAV("name", "sql"))
         tmp += " " + __tmp->AV["name"];
        __tmp = __tmp->GetNext();
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefPlanItemExec(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  bool iRC = false;
  UnicodeString tmp;
  if (AIE->GetFirstChild())
   {
    if (AIE->Count > 1)
     {
      TTagNode * __tmp = AIE->GetFirstChild();
      tmp = "";
      while (__tmp)
       {
        if (!__tmp->CmpAV("name", "sql"))
         tmp += " " + __tmp->AV["name"];
        __tmp = __tmp->GetNext();
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLPlanPres(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode * FIS = FISAttr->GetParent("is");
  ATab = FGetGUI(FISCondRules) + "." + FIS->AV["uid"] + ":";
  TTagNode * _tmp = AIE->GetChildByAV("text", "name", "sql");
  if (_tmp)
   {
    TDate FDateBP;
    TDate FDateEP;
    TDate FPlanMonth;
    CalcPlanPeriod(& FDateBP, & FDateEP, & FPlanMonth);
    UnicodeString cdPlan, pdPlan;
    cdPlan = FPlanMonth.FormatString("01.mm.yyyy");
    pdPlan = Sysutils::IncMonth(FPlanMonth, -1).FormatString("01.mm.yyyy");
    TReplaceFlags rFlag;
    rFlag << rfReplaceAll;
    ARet = Sysutils::StringReplace(Sysutils::StringReplace(_tmp->AV["value"], "_PREV_", pdPlan, rFlag), "_CURR_",
        cdPlan, rFlag);
   }
  else
   ARet = "";
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLPlanItem(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode * FIS = FISAttr->GetParent("is");
  ATab = FGetGUI(FISCondRules) + "." + FIS->AV["uid"] + ":";
  UnicodeString Alias = FIS->AV["tabalias"];
  int FItemType = GetChildContAVDef(AIE, "type", "-1").ToIntDef(-1); //��� ��������
  int FItemRef = GetChildContAVDef(AIE, "objref", "-1").ToIntDef(-1); //��� ��������
  bool PlanByInf = FIS->CmpAV("ref", "40381E23-92155860-4448.1084");
  if ((FItemType != -1) && (FItemRef != -1))
   {
    UnicodeString TypeFL = "";
    UnicodeString ValFL = "";
    if (PlanByInf)
     {
      TypeFL = "R1086";
      ValFL  = "R1089";
     }
    else
     {
      TypeFL = "R3086";
      ValFL  = "R3089";
     }
    ARet = Alias + "." + TypeFL + "=" + IntToStr(FItemType + 1) + " and " + Alias + "." + ValFL + "=" + IntToStr(FItemRef);
   }
  else
   ARet = "";
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLPlanItemExec(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode * FIS = FISAttr->GetParent("is");
  ATab = FGetGUI(FISCondRules) + "." + FIS->AV["uid"] + ":";
  UnicodeString Alias = FIS->AV["tabalias"];
  int FItemType = GetChildContAVDef(AIE, "type", "-1").ToIntDef(-1); //��� ��������
  int FItemRef = GetChildContAVDef(AIE, "objref", "-1").ToIntDef(-1); //��� ��������
  bool PlanByInf = FIS->CmpAV("ref", "40381E23-92155860-4448.1084");
  if ((FItemType != -1) && (FItemRef != -1) && PlanByInf)
   {//���� ���������� ������������ ������ � ����� �� ���������
    UnicodeString TypeFL = "";
    UnicodeString ValFL = "";
    if (PlanByInf)
     {
      TypeFL = "R1086";
      ValFL  = "R32AA";
     }
    ARet = Alias + "." + TypeFL + "=" + IntToStr(FItemType + 1) + " and " + Alias + "." + ValFL + "=" + IntToStr(FItemRef);
   }
  else
   ARet = "";
 }
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefExtEditValue(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  UnicodeString tmp;
  TTagNode * __tmp;
  bool iRC = false;
  if (AIE->GetFirstChild())
   {
    if (AIE->Count > 1)
     {
      __tmp = AIE->GetFirstChild();
      tmp   = "";
      while (__tmp->GetNext())
       {
        tmp += " " + __tmp->AV["name"];
        __tmp = __tmp->GetNext();
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLExtEditValue(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode * FIS = FISAttr->GetParent("is");
  ATab = FGetGUI(FISCondRules) + "." + FIS->AV["uid"] + ":";
  TTagNode * _tmp = AIE->GetChildByAV("text", "name", "sql");
  if (_tmp)
   ARet = _tmp->AV["value"];
  else
   ARet = "";
 }
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefVacSch(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  bool iRC = false;
  if (AIE->Count)
   {
    AText = AIE->GetFirstChild()->AV["name"].Trim();
    iRC   = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLVacSch(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode * FIS = FISAttr->GetParent("is");
  ATab = FGetGUI(FISCondRules) + "." + FIS->AV["uid"] + ":";
  TTagNode * _tmp = AIE->GetChildByAV("text", "ref", "0");
  if (_tmp)
   ARet = "Upper(" + FIS->AV["tabalias"] + "." + FISAttr->AV["fields"] + ")='" + _tmp->AV["value"].UpperCase() + "'";
  else
   ARet = "";
 }
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefTestSch(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  return DefVacSch(AIE, AText, AXMLIE);
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLTestSch(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  SQLVacSch(AIE, ATab, ARet);
 }
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::DoGetOrgParam(UnicodeString & ARet, UnicodeString AFlName)
 {
dsLogMessage("TICSIMMNomDef::DoGetOrgParam >>> "+FDM->MainLPUCode);
  UnicodeString FFlName = AFlName;
  FDM->qtExecParam("Select Count(*) as RCount From class_00C7 Where Upper(R011E)= :pGUI", "pGUI",
      FDM->MainLPUCode.UpperCase());
  if (FDM->Query->FieldByName("RCount")->AsInteger)
   {
    if (FFlName.UpperCase() == "CODE")
     FFlName = "R011E";
    else if (FFlName.UpperCase() == "NAME")
     FFlName = "R00CA";
    else if (FFlName.UpperCase() == "FULLNAME")
     FFlName = "R00CB";
    else if (FFlName.UpperCase() == "ADDR")
     FFlName = "R00CC";
    else if (FFlName.UpperCase() == "PHONE")
     FFlName = "R00CE";
    else if (FFlName.UpperCase() == "EMAIL")
     FFlName = "R00CF";
    else if (FFlName.UpperCase() == "OWNERFIO")
     FFlName = "R00CD";
    FDM->qtExecParam("Select " + FFlName + " From class_00C7 Where Upper(R011E)= :pGUI", "pGUI",
        FDM->MainLPUCode.UpperCase());
    if (FDM->Query->RecordCount)
     {
      if (FFlName.UpperCase() == "R00CC")//�����
           ARet = FDM->GetAddrStrByCode(FDM->Query->FieldByName(FFlName)->AsString);
      else
       ARet = FDM->Query->FieldByName(FFlName)->AsString;
     }
    else
     ARet = "";
   }
  else
   {
    ARet = "������, ��� �����������: " + FDM->MainLPUCode;
   }
 }
//---------------------------------------------------------------------------
