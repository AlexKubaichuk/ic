//---------------------------------------------------------------------------

#ifndef IMMNomDefH
#define IMMNomDefH
//---------------------------------------------------------------------------
//#include <vcl.h>
#include "BaseNomDef.h"
#include "dsCommClassData.h"
//---------------------------------------------------------------------------
class TICSIMMNomDef : public TICSBaseNomDef
{
private:
    TFDQuery *FQuery;

    TdsCommClassData *FClsData;

    void __fastcall DoGetOrgParam(UnicodeString &ARet, UnicodeString AFlName);

    int __fastcall CorrectDay( Word Year, Word Month, Word Day );
    bool __fastcall CalcPlanPeriod(TDate *ADateBP, TDate *ADateEP, TDate *APlanMonth);

    void __fastcall APlanVac(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall APlanItemStr(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall APlanItemVacType(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall APlanExecMIBP(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall APlanInfNotExec(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet, UnicodeString AAttrRef);
    void __fastcall APlanMIBPNotExec(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet, UnicodeString AAttrRef);

    void __fastcall APlanPeriod(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AOrgLevels(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall APatPrivAge(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASchName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);

    bool __fastcall DefDatePeriodLast(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    bool __fastcall DefOtvodDatePeriodLast(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    bool __fastcall DefInqTreeValue(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    bool __fastcall DefPlanPrivReak(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    bool __fastcall DefPlanProbReak(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    bool __fastcall DefPlanPres(TTagNode *AIE, UnicodeString& AText,  UnicodeString& AXMLIE);
    bool __fastcall DefPlanItem(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE);
    bool __fastcall DefPlanItemExec(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE);
    bool __fastcall DefExtEditValue(TTagNode *AIE, UnicodeString& AText,UnicodeString& AXMLIE);
    bool __fastcall DefVacSch(TTagNode *AIE, UnicodeString& AText,UnicodeString& AXMLIE);
    bool __fastcall DefTestSch(TTagNode *AIE, UnicodeString& AText,UnicodeString& AXMLIE);

    void __fastcall SQLDatePeriodLast(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    void __fastcall SQLOtvodDatePeriodLast(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    void __fastcall SQLInqTreeValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    void __fastcall SQLPlanPrivReak(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    void __fastcall SQLPlanProbReak(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    void __fastcall SQLPlanPres(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    void __fastcall SQLPlanItem(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet);
    void __fastcall SQLPlanItemExec(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet);
    void __fastcall SQLExtEditValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    void __fastcall SQLVacSch(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    void __fastcall SQLTestSch(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
  UnicodeString __fastcall GetVacType(UnicodeString  ASrc);

protected:
    virtual TGetAttrFunc __fastcall GetAttrMethodAddr(UnicodeString AFuncName);
    virtual TGetDefFunc __fastcall GetDefMethodAddr(UnicodeString AFuncName);
    virtual TGetSQLFunc __fastcall GetSQLMethodAddr(UnicodeString AFuncName);
public:

    __fastcall TICSIMMNomDef(TFDQuery *AQuery, TAxeXMLContainer *AXMLList, UnicodeString ARegGUI, TdsCommClassData *AClsData);
    __fastcall ~TICSIMMNomDef();
};
#endif
