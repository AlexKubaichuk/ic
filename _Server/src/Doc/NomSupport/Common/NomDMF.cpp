//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

//#include <math.h>
#include "NomDMF.h"

#define quFNStr(a) FQuery->FieldByName(a)->AsString
#define quFNInt(a) FQuery->FieldByName(a)->AsInteger
#define quFNFloat(a) FQuery->FieldByName(a)->AsFloat
#define quFNDate(a) FQuery->FieldByName(a)->AsDateTime
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocTreeMapNode::FGetAsExtCodeName()
{
  return FItemExtCode+" "+FItemName;
}
//---------------------------------------------------------------------------
__fastcall TNomDM::TNomDM(TFDQuery *AQuery, TAxeXMLContainer *AXMLList, UnicodeString ARegGUI)
{
  FRegGUI  = ARegGUI;
  FQuery   = AQuery;
  FXMLList = AXMLList;
  FReg = FXMLList->GetXML(ARegGUI);

/*
  FKLADR = new TKLADRFORM(NULL);
  FKLADR->Database = AUTOFUNC_DB;
  FKLADR->RegionEnabled = true;
  FKLADR->NasPunktEnabled = true;
  FKLADR->StreetEnabled = true;
  FKLADR->StreetRequired = true;
  FKLADR->StreetVisible = true;
  FKLADR->RequiredColor = clInfoBk;  //!!!
*/

//  LPUCode = 0;
  FClassData = new TdsRegClassData(FReg, FQuery);
}
//---------------------------------------------------------------------------
__fastcall TNomDM::~TNomDM()
{
  delete FClassData;
//  XMLList = NULL;
//!!!  delete FKLADR;
}
//---------------------------------------------------------------------------
void __fastcall TNomDM::DecodePeriod(UnicodeString AScr, int &AYear, int &AMonth, int &ADay)
{
  AYear  = GetLPartB(AScr,'/').ToInt();
  AMonth = GetLPartB(GetRPartB(AScr,'/'),'/').ToInt();
  ADay   = GetRPartB(GetRPartB(AScr,'/'),'/').ToInt();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TNomDM::IncPeriod(UnicodeString AFrDate, UnicodeString APer, int IsSub)
{
  TDateTime RC = TDate(0);
  try
   {
     int FYears, FMonths, FDays;
     DecodePeriod(APer, FYears, FMonths, FDays);
     if (IsSub)
      {
       FYears  *= -1;
       FMonths *= -1;
       FDays   *= -1;
      }
     RC = Dateutils::IncYear
     (
       Sysutils::IncMonth
       (
         Dateutils::IncDay(StrToDate(AFrDate),FDays),
         FMonths
       ),
       FYears
     );
   }
  __finally
   {
   }
  return RC.FormatString("dd.mm.yyyy");
}
//---------------------------------------------------------------------------
//#include "NomTreeClass.h"
void __fastcall TNomDM::qtExec(UnicodeString ASQL)
{
  try
   {
     FQuery->Close();
     if (FQuery->Transaction->Active) FQuery->Transaction->Commit();
     FQuery->Transaction->StartTransaction();
     FQuery->SQL->Text = ASQL;
     FQuery->OpenOrExecute();
   }
  catch(System::Sysutils::Exception &E)
   {
     dsLogError(E.Message+"; SQL = "+ASQL, __FUNC__);
   }
}
//---------------------------------------------------------------------------
void __fastcall TNomDM::qtExecParam(UnicodeString ASQL, UnicodeString AParamName, Variant AValue)
{
  try
   {
     FQuery->Close();
     if (FQuery->Transaction->Active) FQuery->Transaction->Commit();
     FQuery->Transaction->StartTransaction();
     FQuery->SQL->Text = ASQL;
     FQuery->Prepare();
     FQuery->ParamByName(AParamName)->Value = AValue;
     FQuery->OpenOrExecute();
   }
  catch(System::Sysutils::Exception &E)
   {
     dsLogError(E.Message+"; SQL = "+ASQL, __FUNC__);
   }
}
//---------------------------------------------------------------------------
void __fastcall GetTreeValues(TTagNode *ANode, TStringList *AList, int *Count)
{
  TTagNode *itNode = ANode->GetFirstChild();
  if (ANode->CmpName("treegroup"))
   AList->AddObject(ANode->AV["name"],(TObject*)(*Count));
  else
   {
    if (ANode->GetParent()->CmpName("treegroup"))
     AList->AddObject(ANode->GetParent()->AV["name"]+":"+ANode->AV["name"],(TObject*)(*Count));
    else
     AList->AddObject(ANode->AV["name"],(TObject*)(*Count));
   }
  (*Count)++;
  while (itNode)
   {
     GetTreeValues(itNode, AList,Count);
     itNode = itNode->GetNext();
   }
}
//---------------------------------------------------------------------------
//#include "Unit1.h"
bool __fastcall TNomDM::DefRegValue(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE, UnicodeString ATemplate, UnicodeString AGUI, bool AisAll, bool AIsSubSel)
{
   bool iRC = false;
   UnicodeString tmp;
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         TTagNode *__tmp = AIE->GetFirstChild();
         tmp = "";
         TReplaceFlags rFlag;
         rFlag << rfReplaceAll;
         while (__tmp)
          {
            if (!__tmp->CmpAV("name","sql"))
             {
               if (__tmp->AV["show"].ToIntDef(1))
                tmp += " "+StringReplace(__tmp->AV["name"], "\n", " ", rFlag);
             }
            __tmp = __tmp->GetNext();
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
bool __fastcall TNomDM::DefChVal(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE, TStringList *AValues, TStringList *ACodeValues)
{
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         __tmp = AIE->GetFirstChild();
         tmp = "";
         while (__tmp->GetNext())
          {
            tmp += " "+__tmp->AV["name"];
            __tmp = __tmp->GetNext();
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
/*void __fastcall TNomDM::ExecProc(UnicodeString AProcName, TpFIBQuery *AQuery, UnicodeString AParamName, Variant AParam)
{
   Variant DDD[1];
   DDD[0] = AParam;
   if (AQuery->Transaction->Active) AQuery->Transaction->Rollback();
   AQuery->Close();
   AQuery->SQL->Clear();
   AQuery->Transaction->StartTransaction();
   AQuery->ExecProcedure(AProcName.c_str(),DDD,0);
}    */ //!!!
//---------------------------------------------------------------------------
UnicodeString __fastcall TNomDM::GetClassData(long ACode, UnicodeString AName)
{
  UnicodeString RC = "";
  try
   {
     RC = FClassData->GetData(AName, ACode);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TNomDM::ClearHash()
{
  FClassData->ClearHash("");
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TNomDM::GetAddrStrByCode(UnicodeString ACode, unsigned int AParams)
{
  UnicodeString RC = "";
  try
   {
     if (FOnGetAddrStr)
      RC = FOnGetAddrStr(ACode, AParams);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TNomDM::GetOrgStrByCode(UnicodeString ACode)
{
  UnicodeString RC = "";
  try
   {
     if (FOnGetOrgStr)
      RC = FOnGetOrgStr(ACode, 0);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TNomDM::GetOrgFieldByCode(UnicodeString ACode, UnicodeString AFlName)
{
  UnicodeString RC = "";
  try
   {
     if (FOnGetOrgFieldStr)
      RC = FOnGetOrgFieldStr(ACode.ToIntDef(-1), AFlName);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
inline int __fastcall SpecPeriodInt(UnicodeString APeriod)
{
  if (APeriod.ToIntDef(-1) != -1)
   {
     if (APeriod.ToIntDef(-1))    return 1;
     else                         return 0;
   }
  else                            return 0;
}
//---------------------------------------------------------------------------
/*
UnicodeString __fastcall GetDocPeriodValStr(UnicodeString APerType, UnicodeString AValFrom, UnicodeString AValTo)
{
   UnicodeString RC = "";
   UnicodeString FFrDay,FFrMonth,FFrYear,FToDay,FToMonth,FToYear,FFrMonthFull,FToMonthFull;
   FFrDay = "";FFrMonth = "";FFrYear = "";FToDay = "";FToMonth = "";FToYear = "";FFrMonthFull = "";FToMonthFull = "";
   TDateTime tmpDate;
   if (TryStrToDate(AValFrom,tmpDate))
    {
      FFrDay       = StrToDate(AValFrom).FormatString("dd");
      FFrMonth     = StrToDate(AValFrom).FormatString("mm");
      FFrMonthFull = StrToDate(AValFrom).FormatString("mmmm");
      FFrYear      = StrToDate(AValFrom).FormatString("yyyy");
    }
   if (TryStrToDate(AValTo,tmpDate))
    {
      FToDay       = StrToDate(AValTo).FormatString("dd");
      FToMonth     = StrToDate(AValTo).FormatString("mm");
      FToMonthFull = StrToDate(AValTo).FormatString("mmmm");
      FToYear      = StrToDate(AValTo).FormatString("yyyy");
    }

   int FPerType;
   if (APerType.ToIntDef(-1) == -1)
    FPerType = SpecPeriodInt(APerType);
   else
    FPerType = APerType.ToInt();
   if (FPerType == 0)
    {//"�����������"
      return "";
    }
   else if (FPerType == 1)
    {//"����������"
      if (AValFrom.Length() && AValTo.Length())
       {
         RC =  Format("%s.%s.%s �. - %s.%s.%s �.",OPENARRAY( TVarRec, (FFrDay,FFrMonth,FFrYear,FToDay,FToMonth,FToYear) ) );
       }
      else if (AValFrom.Length())
       RC =  Format("� %s.%s.%s �.",OPENARRAY( TVarRec, (FFrDay,FFrMonth,FFrYear) ) );
      else if (AValTo.Length())
       RC =  Format("�� %s.%s.%s �.",OPENARRAY( TVarRec, (FToDay,FToMonth,FToYear) ) );
    }
   else if (FPerType == 2)
    {//"�����"
      RC =  Format("%s %s �.",OPENARRAY( TVarRec, (FFrMonthFull,FFrYear) ) );
    }
   else if (FPerType == 3)
    {//"�������"
      int quarter = StrToDate(AValFrom).FormatString("mm").ToInt();
      if ((quarter >=1)&&(quarter<=3))
       RC =  Format("I ������� %s �.",OPENARRAY( TVarRec, (FFrYear) ) );
      else if ((quarter >=4)&&(quarter<=6))
       RC =  Format("II ������� %s �.",OPENARRAY( TVarRec, (FFrYear) ) );
      else if ((quarter >=7)&&(quarter<=9))
       RC =  Format("III ������� %s �.",OPENARRAY( TVarRec, (FFrYear) ) );
      else if ((quarter >=10)&&(quarter<=12))
       RC =  Format("IV ������� %s �.",OPENARRAY( TVarRec, (FFrYear) ) );
    }
   else if (FPerType == 4)
    {//"���������"
      int quarter = StrToDate(AValFrom).FormatString("mm").ToInt();
      if ((quarter >=1)&&(quarter<=6))
       RC =  Format("I ���������  %s �.",OPENARRAY( TVarRec, (FFrYear) ) );
      else if ((quarter >=7)&&(quarter<=12))
       RC =  Format("II ��������� %s �.",OPENARRAY( TVarRec, (FFrYear) ) );
    }
   else if (FPerType == 5)
    {//"���"
      RC = Format("%s �.",OPENARRAY( TVarRec, (FFrYear) ) );
    }
   else
    RC = "";
   return RC;
}         */
//---------------------------------------------------------------------------

