//---------------------------------------------------------------------------
#ifndef NomDMFH
#define NomDMFH

#include <System.Classes.hpp>
//#include <FireDAC.Comp.Client.hpp>
//#include "XMLContainer.h"
#include "dsRegClassData.h"
#include <System.DateUtils.hpp>
#include "ICSDATEUTIL.hpp"
#include "icsLog.h"
//#include "NomTreeClass.h"
//!!!#include "KLADR.h"

#define _scError    0
#define _scDigit    1
#define _scChoice   2
#define _scDiapazon 3
#define _scDate     4
#define _scRDate    5

//#include "datadef.h"

#define AsTextDate AsDateTime.FormatString("dd.mm.yyyy")
//!!!#include "ICSClassData.h"
//---------------------------------------------------------------------------
typedef UnicodeString __fastcall (__closure *TNomGetExtValuesEvent)(TTagNode *ADefNode, TStringList *AFields);
typedef UnicodeString  (__closure *TNomGetStrValByCodeEvent)(UnicodeString ACode, unsigned int AParams);
typedef UnicodeString  (__closure *TNomGetFieldValByCodeEvent)(__int64 ACode, UnicodeString AFieldName);
//---------------------------------------------------------------------------
class PACKAGE TdsDocTreeMapNode : public TObject
{
private:
    UnicodeString FItemExtCode, FItemName;
    UnicodeString __fastcall FGetAsExtCodeName();
public:
    __fastcall TdsDocTreeMapNode(UnicodeString AExtCode, UnicodeString AName);
    __fastcall ~TdsDocTreeMapNode();

    __property UnicodeString AsName = {read=FItemName};
    __property UnicodeString AsExtCodeName = {read=FGetAsExtCodeName};
    __property UnicodeString AsExtCode = {read=FItemExtCode};
};
//---------------------------------------------------------------------------
typedef map<UnicodeString,TdsDocTreeMapNode*> TdsTreeItemMap;
typedef map<UnicodeString, TdsTreeItemMap*> TdsTreeClassMap;
//---------------------------------------------------------------------------
class PACKAGE TNomDM : public TObject
{
private:	// User declarations
//       bool __fastcall GetTreeCh(void *uiTag, UnicodeString &UID);
    TdsRegClassData *FClassData;
    UnicodeString FRegGUI;
  TNomGetExtValuesEvent FOnExtGetAVal;
  TNomGetStrValByCodeEvent  FOnGetAddrStr;
  TNomGetStrValByCodeEvent  FOnGetOrgStr;
  TNomGetFieldValByCodeEvent FOnGetOrgFieldStr;

    UnicodeString FMainLPUCode;
    TFDQuery *FQuery;
    TAxeXMLContainer *FXMLList;
    TTagNode *FReg;
//!!!    TKLADRFORM *FKLADR;
//    TClassValuesMap   FClassValues;
//    TLongMap* __fastcall GetClassHash(UnicodeString AClassUID);
public:		// User declarations
    __fastcall TNomDM(TFDQuery *AQuery, TAxeXMLContainer *AXMLList, UnicodeString ARegGUI);
    __fastcall ~TNomDM();

//    int LPUCode;

    TdsTreeClassMap FTreeClassMap;

  void __fastcall DecodePeriod(UnicodeString AScr, int &AYear, int &AMonth, int &ADay);
  UnicodeString __fastcall IncPeriod(UnicodeString AFrDate, UnicodeString APer, int IsSub);

    void __fastcall qtExec(UnicodeString ASQL);
    void __fastcall qtExecParam(UnicodeString ASQL, UnicodeString AParamName, Variant AValue);
    void       __fastcall ClearHash();
//    void       __fastcall ExecProc(UnicodeString AProcName, TpFIBQuery *AQuery, UnicodeString AParamName, Variant AParam);
    UnicodeString __fastcall GetClassData(long ACode, UnicodeString AName);

    bool __fastcall DefRegValue(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE, UnicodeString ATemplate, UnicodeString AGUI,bool AisAll, bool AIsSubSel);
    bool __fastcall DefChVal(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE, TStringList *AValues, TStringList *ACodeValues = NULL);

  UnicodeString __fastcall GetAddrStrByCode(UnicodeString ACode, unsigned int AParams = 0x0FF);
  UnicodeString __fastcall GetOrgStrByCode(UnicodeString ACode);
  UnicodeString __fastcall GetOrgFieldByCode(UnicodeString ACode, UnicodeString AFlName);


    __property UnicodeString RegGUI    = {read=FRegGUI};
    __property TFDQuery *Query    = {read=FQuery};
    __property UnicodeString MainLPUCode = {read=FMainLPUCode, write=FMainLPUCode};
    __property TTagNode *Reg = {read=FReg};

  __property TNomGetExtValuesEvent OnExtGetAVal = {read=FOnExtGetAVal, write=FOnExtGetAVal};

  __property TNomGetStrValByCodeEvent  OnGetAddrStr = {read = FOnGetAddrStr, write=FOnGetAddrStr};
  __property TNomGetStrValByCodeEvent  OnGetOrgStr = {read = FOnGetOrgStr, write=FOnGetOrgStr};
  __property TNomGetFieldValByCodeEvent  OnGetOrgFieldStr = {read = FOnGetOrgFieldStr, write=FOnGetOrgFieldStr};

    __property TAxeXMLContainer* XMLList = {read=FXMLList};
//!!!    __property TKLADRFORM *KLADR    = {read=FKLADR};
};
//---------------------------------------------------------------------------
/*
extern void __fastcall GetTreeValues(TTagNode *ANode, TStringList *AList, int *Count);
*/

//extern PACKAGE TNomDM *AutoFunc_DM;
//---------------------------------------------------------------------------
#endif

