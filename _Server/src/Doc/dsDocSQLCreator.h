//---------------------------------------------------------------------------
#ifndef dsDocSQLCreatorH
#define dsDocSQLCreatorH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Controls.hpp>
//#include <StdCtrls.hpp>
//#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "dsDocSrvTypeDef.h"
//---------------------------------------------------------------------------
enum TRelationsType
{
 Rel_None = 0 ,
 Rel_1_1 =  1 ,
 Rel_1_M =  2 ,
 Rel_M_1 =  3 ,
 Rel_M_M =  4
};
//---------------------------------------------------------------------------
//typedef void (__stdcall *TSQLExtFunc)(TTagNode *AIE, char* ATab, char* ARet);
//---------------------------------------------------------------------------
class PACKAGE TdsDocSQLCreator : public TObject
{
__published:	// IDE-managed Components
private:	// User declarations
//        inline UnicodeString __fastcall GetGUI(TTagNode *ANode)
//         {return ANode->GetRoot()->GetChildByName("passport")->AV["gui"];};
        TdsDocGetDateStringFormat FOnGetDateStringFormat;

  TdsDocGetSVal FOnGetSVal;

        UnicodeString __fastcall CreateSQLParams(TTagNode *AIERef, TStringList *ATabList, bool IsAddTab = true);
        UnicodeString __fastcall AddLinkKey(TTagNode* ALink, UnicodeString ABaseTabName, UnicodeString ATabName, UnicodeString ATabAlias, UnicodeString AJoin, bool *AIsDist, bool AIsAddJoin, UnicodeString AKeyVal, bool AIsAddKey);
        bool       __fastcall FGetISLinkPath(TTagNode *ALinks, UnicodeString AISRef, TStringList *ASearchPath, TStringList *ATabList);
        UnicodeString __fastcall GetRefByType(UnicodeString ABaseGUI, UnicodeString AISRef);
        UnicodeString __fastcall GetPeriodVals(UnicodeString ABaseISRef, TDateTime AFr, TDateTime ATo);
        UnicodeString __fastcall CreateWhereGroup(TTagNode *ANode,TStringList *ATabList, bool &ARC, UnicodeString ABaseISRef);
        UnicodeString __fastcall DateFS(TDateTime ADate);
        int        FPerType;
        TDateTime  PerFr,PerTo;
        UnicodeString  FTablePref;
//        int FAlPr;
        UnicodeString  FFBaseISRef;
public:		// User declarations
        TAxeXMLContainer *FXMLS;
//        HINSTANCE FExtProc;
        __fastcall TdsDocSQLCreator(TAxeXMLContainer *AXMLS, UnicodeString APref = "");
        virtual __fastcall ~TdsDocSQLCreator();
        UnicodeString     __fastcall ReplaceKey(UnicodeString ASQL, UnicodeString AKey);
        void           __fastcall SetPeriod(int APerType, TDateTime AFr,TDateTime ATo);
        void           __fastcall GetISLinkPath(TTagNode *ALinks, UnicodeString AISRef, TStringList *ATabList);
        UnicodeString     __fastcall PrepareRef(TTagNode *ANode, UnicodeString AISRef);
        UnicodeString     __fastcall CreateSQL(UnicodeString ABaseISRef, TTagNode *ACond, TStringList *AFieldList, UnicodeString AKeyVal, UnicodeString &ACountSQL ,TStringList *AOrderList,TStringList *ATabList = NULL);
        UnicodeString     __fastcall CreateWhere(TTagNode *ANode,TStringList *ATabList, bool &ARC, UnicodeString ABaseISRef = "");
        bool           __fastcall CreateWhere(TTagNode *ANode,TStringList *ATabList,UnicodeString &AWhereList, UnicodeString ABaseISRef = "");
        UnicodeString     __fastcall CreateFrom(UnicodeString ABaseISRef, TStringList *ATabList, bool *IsDist, UnicodeString AKeyVal, bool *IsAddKey);
        TRelationsType __fastcall GetISRelationsType(UnicodeString ABaseISref, UnicodeString AIERef);
        UnicodeString     __fastcall GetTabAlias(TTagNode *Aattr);
        UnicodeString     __fastcall GetTabAlias(UnicodeString AISRef);
        UnicodeString     __fastcall GetTabName(TTagNode *Aattr);
        UnicodeString     __fastcall GetTabName(UnicodeString AISRef);
        UnicodeString     __fastcall GetISTabKey(UnicodeString AISRef, bool AAddAlias = true);
        UnicodeString     __fastcall GetISTabKey(TTagNode *AIS, bool AAddAlias = true);
        UnicodeString     __fastcall GetRelationKey(UnicodeString ABaseISref, UnicodeString AIERef);
  __property TdsDocGetDateStringFormat OnGetDateStringFormat = {read=FOnGetDateStringFormat, write=FOnGetDateStringFormat};
  __property TdsDocGetSVal OnGetSVal = {read=FOnGetSVal, write=FOnGetSVal};
};
//---------------------------------------------------------------------------
#endif
