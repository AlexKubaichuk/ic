object dsICClass: TdsICClass
  OldCreateOrder = False
  Height = 262
  Width = 495
  object IcConnection: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'User_Name=sysdba'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'Server=localhost'
      'Port=3050'
      'Pooled=False')
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    FormatOptions.AssignedValues = [fvFmtDisplayDateTime, fvFmtDisplayDate, fvFmtDisplayTime]
    FormatOptions.FmtDisplayDateTime = 'dd#mm#yyyy hh:nn:ss'
    FormatOptions.FmtDisplayDate = 'dd#mm#yyyy'
    FormatOptions.FmtDisplayTime = 'hh:nn:ss'
    ResourceOptions.AssignedValues = [rvDirectExecute, rvAutoConnect, rvSilentMode]
    ResourceOptions.DirectExecute = True
    ResourceOptions.SilentMode = True
    ResourceOptions.AutoConnect = False
    TxOptions.AutoStart = False
    TxOptions.AutoStop = False
    TxOptions.DisconnectAction = xdRollback
    LoginPrompt = False
    Left = 29
    Top = 15
  end
  object SRVConnect: TDSRestConnection
    Host = 'localhost'
    Port = 5100
    Left = 227
    Top = 10
    UniqueId = '{2561E080-5D09-4396-9D34-969204DAB404}'
  end
  object AppOpt: TAppOptions
    OnLoadXML = AppOptLoadXML
    DataProvider = AppOptXMLData
    Left = 132
    Top = 87
  end
  object AppOptXMLData: TAppOptXMLProv
    Left = 232
    Top = 84
  end
  object DocConnect: TDSRestConnection
    Host = 'localhost'
    Port = 5100
    UserName = '1'
    Password = '1'
    LoginPrompt = False
    Left = 132
    Top = 11
    UniqueId = '{2561E080-5D09-4396-9D34-969204DAB404}'
  end
end
