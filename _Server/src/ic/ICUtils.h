//---------------------------------------------------------------------------

#ifndef ICUtilsH
#define ICUtilsH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
//extern PACKAGE UnicodeString __fastcall GetWordForNumber(int nRecordsNum, const UnicodeString Words[]);
extern PACKAGE void __fastcall StrToPeriod(UnicodeString AVal, int &AYear, int &AMonth, int &ADay);
extern PACKAGE int __fastcall CmpPeriod(int AY1, int AM1, int AD1, int AY2, int AM2, int AD2);
extern PACKAGE int __fastcall GetTens(int nVal);
extern PACKAGE int __fastcall GetOnes(int nVal);
extern PACKAGE UnicodeString __fastcall DurationToStr(const int AYears, const int AMonths, const int ADays);
extern PACKAGE UnicodeString __fastcall DateDurationToStr(TDate ADate1, TDate ADate2);
extern PACKAGE UnicodeString __fastcall DateDiffToPeriodStr(TDateTime ACurDate, TDateTime ABirthDay);
extern PACKAGE bool          __fastcall PeriodInRange(UnicodeString AMin, UnicodeString AMax, UnicodeString AVal, bool AMinEqu = true, bool AMaxEqu = false);
//---------------------------------------------------------------------------
#endif
