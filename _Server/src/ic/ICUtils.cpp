//---------------------------------------------------------------------------


#pragma hdrstop

#include <stdio.h>
#include "ICUtils.h"
#include "ICSDATEUTIL.hpp"
#include "DKUtils.h"
//#include "AxeUtil.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
UnicodeString __fastcall DurationToStr(const int AYears, const int AMonths, const int ADays)
{
  UnicodeString sStr;

  if (AYears)
  {
    if (sStr.Length())
      sStr += " ";
    sStr += IntToStr(AYears) + GetWordForNumber(AYears, WFNYear)[1] + ".";
  }
  if (AMonths)
  {
    if (sStr.Length())
      sStr += " ";
    sStr += IntToStr(AMonths) + "�.";
  }
  if (ADays)
  {
    if (sStr.Length())
      sStr += " ";
    sStr += IntToStr(ADays) + "�.";
  }

  if (!sStr.Length())
    sStr = "0�.";
  return sStr;
}

//---------------------------------------------------------------------------
UnicodeString __fastcall DateDurationToStr(TDate ADate1, TDate ADate2)
{
  Word wYears, wMonths, wDays;
  DateDiff(ADate1, ADate2, wDays, wMonths, wYears);
  return DurationToStr(wYears, wMonths, wDays);
}
//---------------------------------------------------------------------------
int __fastcall GetTens(int nVal)
{
  int nResult         = 0;
  UnicodeString asVal = IntToStr(abs(nVal));
  if (asVal.Length() > 1)
   {
    try
     {
      nResult = StrToInt(asVal[asVal.Length() - 1]);
     }
    catch (EConvertError&)
     {
      nResult = 0;
     }
   }
  return nResult;
}
//---------------------------------------------------------------------------
int __fastcall GetOnes(int nVal)
 {
  int nResult         = 0;
  UnicodeString asVal = IntToStr(abs(nVal));
  if (asVal.Length())
   {
    try
     {
      nResult = StrToInt(asVal[asVal.Length()]);
     }
    catch (EConvertError&)
     {
      nResult = 0;
     }
   }
  return nResult;
 }

// ---------------------------------------------------------------------------
UnicodeString __fastcall DateDiffToPeriodStr(TDateTime ACurDate, TDateTime ABirthDay)
{
  UnicodeString RC = "";
  try
   {
     Word FYear,FMonth,FDay;
     DateDiff(ACurDate, ABirthDay, FDay, FMonth, FYear);
     RC = IntToStr(FYear)+"/"+IntToStr(FMonth)+"/"+IntToStr(FDay);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall StrToPeriod(UnicodeString AVal, int &AYear, int &AMonth, int &ADay)
{
  AYear = 0;
  AMonth = 0;
  ADay = 0;
  UnicodeString FCurVal;
  bool FScan;
  int i;
  // ���
  FCurVal = "";
  FScan = true;
  for (i = 1; (i <= AVal.Length()) && FScan; i++)
   {
     if (AVal[i] != '/')
      FCurVal += AVal[i];
     else
      {
        i++;
        FScan = false;
      }
   }
  AYear = FCurVal.ToIntDef(0);
  // �������
  FCurVal = "";
  FScan = true;
  for (i; (i <= AVal.Length()) && FScan; i++)
   {
     if (AVal[i] != '/')
      FCurVal += AVal[i];
     else
      {
        i++;
        FScan = false;
      }
   }
  AMonth = FCurVal.ToIntDef(0);
  //����
  FCurVal = "";
  FScan = true;
  for (i; (i <= AVal.Length()) && FScan; i++)
   {
     if (AVal[i] != '/')
      FCurVal += AVal[i];
     else
      {
        i++;
        FScan = false;
      }
   }
  ADay = FCurVal.ToIntDef(0);
}
//---------------------------------------------------------------------------
bool __fastcall PeriodInRange(UnicodeString AMin, UnicodeString AMax, UnicodeString AVal, bool AMinEqu, bool AMaxEqu)
{
  bool RC = false;
  try
   {
     int FYMin, FMMin, FDMin, FYMax, FMMax, FDMax, FYVal, FMVal, FDVal;

     swscanf(AMin.c_str(),L"%3d/%2d/%2d", &FYMin, &FMMin, &FDMin);
     swscanf(AMax.c_str(),L"%3d/%2d/%2d", &FYMax, &FMMax, &FDMax);
     swscanf(AVal.c_str(),L"%3d/%2d/%2d", &FYVal, &FMVal, &FDVal);

     int FCompMin = CmpPeriod(FYMin, FMMin, FDMin, FYVal, FMVal, FDVal);
     int FCompMax = CmpPeriod(FYVal, FMVal, FDVal, FYMax, FMMax, FDMax);

     if (AMinEqu)
      {
        if (AMaxEqu)
         RC = (FCompMin != 1) && (FCompMax != 1);  // min <= val <= max
        else
         RC = (FCompMin != 1) && (FCompMax == -1); // min <= val < max
      }
     else
      {
        if (AMaxEqu)
         RC = (FCompMin == -1) && (FCompMax != 1);  // min < val <= max
        else
         RC = (FCompMin == -1) && (FCompMax == -1); // min < val < max
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
int __fastcall CmpPeriod(int AY1, int AM1, int AD1, int AY2, int AM2, int AD2)
{
  //  1  - 1 > 2
  //  0  - 1 = 2
  // -1  - 1 < 2
  int RC = -1;
  try
   {
     if (AY1 == AY2)      // Y1 == Y2
      {
        if (AM1 == AM2)      // M1 == M2
         {
           if (AD1 == AD2)      // D1 == D2
             RC = 0;
           else if (AD1 > AD2) // D1 > D2
             RC = 1;
         }
        else if (AM1 > AM2) // M1 > M2
         {
           RC = 1;
         }
      }
     else if (AY1 > AY2) // Y1 > Y2
       RC = 1;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

