﻿// ---------------------------------------------------------------------------
// #include <vcl.h>
#pragma hdrstop

#include "dsICUnit.h"
#include "DataMF.h"
#include <Datasnap.DSHTTP.hpp>
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "System.Classes.TPersistent"
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma resource "*.dfm"

// TRegClassDM *RegClassDM;
// ---------------------------------------------------------------------------
__fastcall TdsICClass::TdsICClass(TComponent * Owner, __int64 APort, TdsMKB * AMKB, TAxeXMLContainer *AXMLList) : TDataModule(Owner)
 {
  dsLogC(__FUNC__);
  try
   {
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes)
     {
      // dsLogMessage(FSes->UserRoles->Text, __FUNC__);
      ConnectDB(IcConnection, FSes->UserRoles->Values["ICDB"], __FUNC__);

      FDM               = new TdsICDM(this, IcConnection, AMKB, AXMLList);
      FDM->OnGetAddrStr = FGetAddrStr;
      FDM->OnGetOrgStr  = FGetOrgStr;

      FGDMRC               = "";
      FGDMRCCode           = "";
      DocConnect->UserName = FSes->UserName;
      DocConnect->Password = FSes->UserRoles->Values["UPS"];
      SRVConnect->UserName = FSes->UserName;
      SRVConnect->Password = FSes->UserRoles->Values["UPS"];
      FGDMECode            = TdsKeyStatus::ksOk;
      FGDM                 = new TDataM(this);

      FGDMRC     = FGDM->RC;
      FGDMRCCode = FGDM->RCCode;
      FGDMCCode = FGDM->CCode;
      FGDMECode  = FGDM->ECode;

      FLPUId   = FSes->UserRoles->Values["ID"];
      FLPUCode = FDM->GetMainLPUCode(FLPUId);

      UnicodeString FXMLPath = icsPrePath(ExtractFilePath(ParamStr(0)));
      UnicodeString FFN = icsPrePath(FXMLPath + "\\ic_app_opt.xml");
      if (!FileExists(FFN))
       {
        ForceDirectories(FXMLPath);
        TTagNode * tmp = new TTagNode;
        try
         {
          tmp->Encoding = "utf-8";
          tmp->Name     = "g";
          tmp->SaveToXMLFile(FFN, "");
         }
        __finally
         {
          delete tmp;
         }
       }
      AppOptXMLData->FileName = FFN;
      AppOpt->LoadXML();

      FRegComp = new TdsSrvRegistry(this, IcConnection, FDM->XMLList, "40381E23-92155860-4448", "", true,
       FSes->UserRoles->Values["ICDB"].UpperCase(), FSes->SessionName);
      FPlanComp   = new TdsSrvPlaner(this, IcConnection, FDM->XMLList, "40381E23-92155860-4448", "");
      FDM->DefXML = FDM->XMLList->GetXML("40381E23-92155860-4448");

      FDefXML = FDM->DefXML;
      FRegComp->SetRegDef(FDefXML);
      FPlanComp->SetRegDef(FDefXML);
      FPlanComp->OnGetValById      = FOnGetValById;
      FPlanComp->OnGetValById10    = FOnGetValById10;
      FPlanComp->OnPlanInsertData  = FOnInsertData;
      FPlanComp->OnGetMIBPList     = FOnGetMIBPList;
      FPlanComp->OnGetOrgStr       = FGetOrgStr;
      FPlanComp->OnCheckSpecExists = FCheckSpecExists;

      FKLADRComp                  = new TdsKLADRClass(this, IcConnection);
      FOrgComp                    = new TdsOrgClass(NULL, FDM->XMLList->GetXML("5BCAE819-D0CFEF0F-0B87"));
      FRegComp->OnGetAddrStr      = FGetAddrStr;
      FRegComp->OnGetOrgStr       = FGetOrgStr;
      FRegComp->OnUnitDataChanged = FOnUnitDataChanged;

      FRegComp->OnGetFilterData = FGetFilterData;
      FRegComp->OnCardEdited = FCardEdited;
      SRVConnect->Port          = APort;
      DocConnect->Port          = APort;
      FDocClient                = new TdsDocClassClient(DocConnect);
      FDocAdmin                 = new TdsAdminClassClient(DocConnect);

      FVSClient = new TdsVacStoreClassClient(SRVConnect);
      FVSAdmin  = new TdsAdminClassClient(SRVConnect);
      DocLogged = false;
      VSLogged  = false;
     }
   }
  catch (Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
    throw;
   }
  dsLogC(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
__fastcall TdsICClass::~TdsICClass()
 {
  dsLogD(__FUNC__);
  DisconnectDB(IcConnection, __FUNC__);
  delete FDocClient;
  delete FVSClient;
  delete FRegComp;
  delete FPlanComp;
  delete FDM;
  delete FGDM;
  delete FKLADRComp;
  delete FOrgComp;
  delete FDocAdmin;
  delete FVSAdmin;
  dsLogD(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
void TdsICClass::FOnGetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARetData)
 {
  ARetData = FRegComp->GetValById(AId, ARecId);
 }
// ---------------------------------------------------------------------------
void TdsICClass::FOnGetValById10(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARetData)
 {
  ARetData = FRegComp->GetValById10(AId, ARecId);
 }
// ---------------------------------------------------------------------------
void TdsICClass::FOnGetMIBPList(System::UnicodeString AId, TJSONObject *& ARetData)
 {
  if (!VSLogged)
   {
    FVSAdmin->LoginUser(FLPUId);
    VSLogged = true;
   }
  ARetData = FVSClient->GetMIBPList(AId);
 }
// ---------------------------------------------------------------------------
System::UnicodeString TdsICClass::FOnInsertData(System::UnicodeString AId, TJSONObject * AValue,
 System::UnicodeString & ARecId)
 {
  return FRegComp->InsertData(AId, AValue, ARecId);
 }
// ---------------------------------------------------------------------------
void TdsICClass::FCheckDocLogin()
 {
  if (!DocLogged)
   {
    FDocAdmin->LoginUser(FLPUId);
    DocLogged = true;
   }
 }
// ---------------------------------------------------------------------------
bool TdsICClass::FCheckSpecExists(System::UnicodeString AId)
 {
  FCheckDocLogin();
  return FDocClient->CheckSpecExistsByCode(AId);
 }
// ---------------------------------------------------------------------------
UnicodeString TdsICClass::FGetFilterData(System::UnicodeString AGUI, unsigned int AParams)
 {
  FCheckDocLogin();
  UnicodeString RC = FDocClient->GetFilterByGUI(AGUI);
  return RC;
 }
// ---------------------------------------------------------------------------
void TdsICClass::Disconnect()
 {
  // FRegComp->Disconnect();
 }
// ---------------------------------------------------------------------------
TICModuleType TdsICClass::FGetModuleType(System::UnicodeString AId)
 {
  TICModuleType RC = imtNone;
  try
   {
    UnicodeString FMod = GetLPartB(AId).Trim().UpperCase();
    if (FMod == "REG")
     RC = imtReg;
    else if (FMod == "CARD")
     RC = imtCard;
    else if (FMod == "PLAN")
     RC = imtPlan;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 TdsICClass::GetCount(UnicodeString AId, UnicodeString AFilterParam, UnicodeString & AExtCount)
 {
  if (AExtCount.Length())
   dsLogMessage("GetCount: ID: " + AId + ", " + AExtCount);
  else if (AFilterParam.Length())
   dsLogMessage("GetCount: ID: " + AId + ", filter: " + AFilterParam);
  else
   dsLogMessage("GetCount: ID: " + AId);
  __int64 RC = 0;
  try
   {
    try
     {
      switch (FGetModuleType(AId))
       {
       case imtReg:
       case imtCard:
       case imtPlan:
         {
          if (AExtCount.Length())
           FDM->GetExtCount(AExtCount);
          else
           RC = FRegComp->GetCount(GetRPartE(AId).Trim().UpperCase(), AFilterParam);
          break;
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsICClass::GetIdList(UnicodeString AId, int AMax, UnicodeString AFilterParam)
 {
  if (AFilterParam.Length())
   dsLogMessage("GetIdList: ID: " + AId + ", Max: " + IntToStr(AMax) + ", filter: " + AFilterParam, "", 5);
  else
   dsLogMessage("GetIdList: ID: " + AId + ", Max: " + IntToStr(AMax), "", 5);
  TJSONObject * RC;
  try
   {
    try
     {
      switch (FGetModuleType(AId))
       {
       case imtReg:
       case imtCard:
       case imtPlan:
         {
          RC = FRegComp->GetIdList(GetRPartE(AId).Trim().UpperCase(), AMax, AFilterParam);
          break;
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsICClass::GetValById(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogMessage("GetValById: ID: " + AId + ", RecId: " + ARecId);
  TJSONObject * RC;
  try
   {
    try
     {
      switch (FGetModuleType(AId))
       {
       case imtReg:
       case imtCard:
       case imtPlan:
         {
          RC = FRegComp->GetValById(GetRPartB(AId).Trim(), ARecId);
          break;
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsICClass::GetValById10(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogMessage("GetValById10: ID: " + AId + ", RecId: " + ARecId);
  TJSONObject * RC;
  try
   {
    try
     {
      switch (FGetModuleType(AId))
       {
       case imtReg:
       case imtCard:
       case imtPlan:
         {
          RC = FRegComp->GetValById10(GetRPartB(AId).Trim(), ARecId);
          break;
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsICClass::Find(UnicodeString AId, UnicodeString AFindData)
 {
  dsLogMessage("Find: ID: " + AId + ", FindParam: " + AFindData);
  TJSONObject * RC;
  try
   {
    try
     {
      switch (FGetModuleType(AId))
       {
       case imtReg:
       case imtCard:
         {
          RC = FRegComp->Find(GetRPartE(AId).Trim().UpperCase(), AFindData);
          break;
         }
       case imtPlan:
         {
          break;
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsICClass::FindEx(UnicodeString AId, UnicodeString AFindData)
 {
  dsLogMessage("FindEx: ID: " + AId + ", FindData: " + AFindData);
  TJSONObject * RC;
  try
   {
    try
     {
      RC = FDM->FindEx(AId, AFindData);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::InsertData(UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecId)
 {
  dsLogMessage("InsertData: ID: " + AId + ", Value: " + AValue->ToString());
  UnicodeString RC = "";
  try
   {
    try
     {
      switch (FGetModuleType(AId))
       {
       case imtReg:
         {
          RC = FRegComp->InsertData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId);
          break;
         }
       case imtCard:
         {
          RC = FRegComp->InsertCardData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId);
          break;
         }
       case imtPlan:
         {
          break;
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::EditData(UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecId)
 {
  dsLogMessage("EditData: ID: " + AId + ", Value: " + AValue->ToString());
  UnicodeString RC = "";
  try
   {
    try
     {
      switch (FGetModuleType(AId))
       {
       case imtReg:
         {
          RC = FRegComp->EditData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId);
          break;
         }
       case imtCard:
         {
          RC = FRegComp->EditCardData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId);
          break;
         }
       case imtPlan:
         {
          break;
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::DeleteData(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogMessage("DeleteData: ID: " + AId + ", RecId: " + ARecId);
  UnicodeString RC = "";
  try
   {
    try
     {
      switch (FGetModuleType(AId))
       {
       case imtReg:
       case imtPlan:
         {
          RC = FRegComp->DeleteData(GetRPartE(AId).Trim().UpperCase(), ARecId);
          break;
         }
       case imtCard:
         {
          RC = FRegComp->DeleteCardData(GetRPartE(AId).Trim().UpperCase(), ARecId);
          break;
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::GetDefXML()
 {
  dsLogMessage("GetDefXML");
  UnicodeString RC = "";
  try
   {
    try
     {
      // FGDM->chkey();
      // FGDMRC     = FGDM->RC;
      // FGDMRCCode = FGDM->RCCode;
      // FGDMECode  = FGDM->ECode;

      if (FGDMECode == TdsKeyStatus::ksOk)
       RC = FDefXML->AsXML;
      else
       RC = FGDMRC;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::GetClassXML(UnicodeString ARef)
 {
  dsLogMessage("GetClassXML: ID: " + ARef);
  UnicodeString RC = "";
  TTagNode * def = new TTagNode;
  try
   {
    try
     {
      def->Encoding = "utf-8";
      UnicodeString xmlPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\" + ARef + ".xml");
      def->LoadFromXMLFile(xmlPath);
      RC = def->AsXML;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete def;
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::GetClassZIPXML(UnicodeString ARef)
 {
  dsLogMessage("GetClassZIPXML: ID: " + ARef);
  UnicodeString RC = "";
  TTagNode * def = new TTagNode;
  try
   {
    try
     {
     /*
      def->Encoding = "utf-8";
      UnicodeString xmlPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\" + ARef + ".zxml");
      def->LoadFromZIPXMLFile(xmlPath);
      RC = def->AsXML;
      */
      RC = FDM->XMLList->GetXML(ARef)->AsXML;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete def;
   }
  return RC;
 }
// ----------------------------------------------------------------------------
bool TdsICClass::UploadData(UnicodeString ADataPart, int AType)
 {
  bool RC = false;
  try
   {
    try
     {
      if (!AType)
       FUploadData = "";
      else if (AType)
       FUploadData += ADataPart;
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
bool TdsICClass::SetClassZIPXML(UnicodeString ARef)
 {
  dsLogMessage("SetClassZIPXML: ID: " + ARef);
  bool RC = false;
  TTagNode * def = new TTagNode;
  try
   {
    try
     {
      def->Encoding = "utf-8";
      UnicodeString xmlPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\" + ARef + ".zxml");
      def->AsXML = FUploadData;
      def->SaveToZIPXMLFile(xmlPath);
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete def;
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::GetAppOptData()
 {
  dsLogMessage(__FUNC__);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = AppOptXMLData->ValuesNode->AsXML;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
void TdsICClass::SetAppOptData(UnicodeString ARef)
 {
  dsLogMessage(__FUNC__);
  TTagNode * def = new TTagNode;
  try
   {
    try
     {
      def->Encoding = "utf-8";
      def->AsXML    = ARef;
      def->SaveToXMLFile(icsPrePath(AppOptXMLData->FileName), "");
      AppOpt->LoadXML();
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete def;
   }
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::GetCliOptData()
 {
  dsLogMessage(__FUNC__);
  UnicodeString RC = "<no_opt/>";
  TTagNode * FCliOpt = new TTagNode;
  try
   {
    try
     {
      TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
      if (FSes)
       {
        UnicodeString FOptPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\options");
        UnicodeString FFN = icsPrePath(FOptPath + "\\" + FSes->UserName + "_opt.xml");
        if (!FileExists(FFN))
         {
          ForceDirectories(FOptPath);
          FCliOpt->Encoding = "utf-8";
          FCliOpt->AsXML    =
           "<?xml version='1.0' encoding='utf-8'?>" "<g>" " <i n='0007' v='0'/>" " <i n='000A' v='1'/>"
           " <i n='000B' v='1'/>" " <i n='000C' v='1'/>" " <i n='000D' v='0'/>" " <i n='000E' v='0'/>"
           " <i n='0018' v='find'/>" " <i n='0019' v='find'/>" " <i n='0016' v='lfStandard'/>" " <i n='0014' v=''/>"
           " <i n='0015' v='1'/>" " <i n='001B' v='не проверена'/>" " <i n='001C' v='есть'/>" " <i n='001D' v='нет'/>"
           " <i n='001F' v=''/>" " <i n='0020' v='+'/>" " <i n='0021' v='-'/>" " <i n='0023' v=''/>" "</g>";

          FCliOpt->SaveToXMLFile(FFN, "");
         }

        FCliOpt->LoadFromXMLFile(FFN);
        RC = FCliOpt->AsXML;
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete FCliOpt;
   }
  return RC;
 }
// ----------------------------------------------------------------------------
void TdsICClass::SetCliOptData(UnicodeString ARef)
 {
  dsLogMessage(__FUNC__);
  TTagNode * FCliOpt = new TTagNode;
  try
   {
    try
     {
      TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
      if (FSes)
       {
        UnicodeString FOptPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\options");
        UnicodeString FFN = icsPrePath(FOptPath + "\\" + FSes->UserName + "_opt.xml");
        ForceDirectories(FOptPath);
        FCliOpt->Encoding = "utf-8";
        FCliOpt->AsXML    = ARef;
        FCliOpt->SaveToXMLFile(FFN, "");
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete FCliOpt;
   }
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::GetF63(UnicodeString ARef, UnicodeString APrintParams)
 {
  dsLogMessage("GetF63: ID: " + ARef);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->GetF63(ARef, APrintParams);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::GetVacDefDoze(TDate APrivDate, TDate APatBirthday, System::UnicodeString AVacCode)
 {
  dsLogMessage("GetVacDefDoze: ID: " + AVacCode);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->GetVacDefDoze(APrivDate, APatBirthday, AVacCode);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::CreateUnitPlan(__int64 APtId)
 {
  dsLogMessage("UnitPlan");
  UnicodeString RC = "";
  try
   {
    try
     {
      FPlanComp->CreateUnitPlan(APtId);
      RC = "ok";
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
/*
 System::UnicodeString TdsICClass::CreatePlan(TJSONObject * AVal, System::UnicodeString & ARecId)
 {
 dsLogMessage("Plan");
 UnicodeString RC = "";
 try
 {
 try
 {
 RC = FPlanComp->CreatePlan(AVal, ARecId);
 }
 catch (System::Sysutils::Exception & E)
 {
 dsLogError(E.Message, __FUNC__);
 }
 }
 __finally
 {
 }
 return RC;
 }
 */
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::DeletePlan(System::UnicodeString ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FPlanComp->DeletePlan(ARecId);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString TdsICClass::DeletePatientPlan(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "Ошибка";
  try
   {
    RC = FPlanComp->DeletePatientPlan(AId, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::StartCreatePlan(TJSONObject * AVal)
 {
  dsLogMessage("StartCreatePlan");
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FPlanComp->StartCreatePlan(AVal);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::StopCreatePlan()
 {
  dsLogMessage("StopCreatePlan");
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FPlanComp->StopCreatePlan();
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::CheckCreatePlanProgress(System::UnicodeString & ARecId)
 {
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FPlanComp->CheckCreatePlanProgress(ARecId);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::GetPatPlanOptions(__int64 APatCode)
 {
  dsLogMessage("TdsICClass::GetPatPlanOptions: PtId: " + IntToStr(APatCode), "", 5);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FPlanComp->GetPatPlanOptions(APatCode);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
void TdsICClass::SetPatPlanOptions(__int64 APatCode, System::UnicodeString AData)
 {
  dsLogBegin(__FUNC__);
  dsLogMessage(UnicodeString(__FUNC__)+" PtId: " + IntToStr(APatCode), "", 5);
  try
   {
    try
     {
      FPlanComp->SetPatPlanOptions(APatCode, AData);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
 }
// ----------------------------------------------------------------------------
UnicodeString TdsICClass::GetPrivVar(__int64 APrivCode)
 {
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FRegComp->GetPrivVar(APrivCode);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsICClass::GetOtvodInf(__int64 AOtvodCode)
 {
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FRegComp->GetOtvodInf(AOtvodCode);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsICClass::FGetAddrStr(UnicodeString ACode, unsigned int AParams)
 {
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FKLADRComp->AddrCodeToText(ACode, AParams /* "11111111" */);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsICClass::FGetOrgStr(UnicodeString ACode, unsigned int AParams)
 {
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FOrgComp->OrgCodeToText(ACode);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * TdsICClass::GetPlanPrintFormats()
 {
  TJSONObject * RC;
  try
   {
    try
     {
      RC = FPlanComp->GetPlanPrintFormats();
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsICClass::PrintPlan(__int64 ACode)
 {
  UnicodeString RC = "";
  FCheckDocLogin();
  try
   {
    try
     {
      UnicodeString FSpecGUI = "";
      TTagNode * FFilter = new TTagNode;
      TDate FrDate, ToDate;
      FDM->GetPlanPrintData(ACode, FSpecGUI, FFilter, FrDate, ToDate); /* 2 - dctBase */

      try
       {
        UnicodeString FFltName = "";
        TTagNode * FPassport = FFilter->GetChildByName("passport");
        if (FPassport)
         {
          FFltName = FPassport->AV["mainname"] + ":" + FPassport->AV["gui"];
         }
        UnicodeString StartRC = FDocClient->StartDocCreate(FSpecGUI, "", FFilter->AsXML, FFltName, false, 2, 2, FrDate,
         ToDate, false); /* месяц */
        if (SameText(StartRC, "ok"))
         {
          while (!RC.Length())
           {
            StartRC = FDocClient->DocCreateProgress(RC);
            if (SameText(StartRC, "ok"))
             {
              if (!RC.Length())
               RC = "error";
             }
            else if (SameText(StartRC, "error"))
             {
              RC = "error";
             }
            Sleep(50);
           }
         }
        else if (!SameText(StartRC, "error"))
         RC = StartRC;
       }
      __finally
       {
        delete FFilter;
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      RC = "<body>О Ш И Б К А<br/>" + E.Message + "</body>";
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void TdsICClass::SetMainLPU(__int64 ALPUCode)
 {
  try
   {
    try
     {
      FDM->SetMainLPU(ALPUCode);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICClass::AppOptLoadXML(TTagNode * AOptNode)
 {
/*
  UnicodeString FXMLPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml");
  UnicodeString FFN = icsPrePath(FXMLPath + "\\ICS_APP_OPT.zxml");
  if (FileExists(FFN))
   {
    AOptNode->LoadFromZIPXMLFile(FFN);
   }
  else
   dsLogError("Ошибка загрузки описания настроек 'defxml\\ICS_APP_OPT.zxml'", __FUNC__);
   */
      AOptNode->AsXML = FDM->XMLList->GetXML("ICS_APP_OPT")->AsXML;
 }
// ---------------------------------------------------------------------------
TJSONObject * TdsICClass::FindMKB(UnicodeString AStr)
 {
  dsLogMessage("Find: str: " + AStr);
  return FDM->MKB->Find(AStr);
 }
// ----------------------------------------------------------------------------
UnicodeString TdsICClass::MKBCodeToText(UnicodeString ACode)
 {
  dsLogMessage("Code: " + ACode);
  return FDM->MKB->CodeToText(ACode);
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsICClass::GetPlanTemplateDef(int AType)
 {
  UnicodeString RC = "error";
  try
   {
    RC = FPlanComp->GetPlanTemplateDef(AType);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString TdsICClass::GetPlanOpt()
 {
  UnicodeString RC = "error";
  try
   {
    RC = FPlanComp->GetPlanOpt();
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void TdsICClass::SetPlanOpt(UnicodeString AOpt)
 {
  try
   {
    FPlanComp->SetPlanOpt(AOpt);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
__int64 TdsICClass::LPUCode()
 {
  return FLPUCode;
 }
// ---------------------------------------------------------------------------
void TdsICClass::SetUpdateSearch(bool AVal)
 {
  if (AVal)
   {
    FDM->UpdateSearch();
   }
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsICClass::FOnUnitDataChanged(__int64 APtId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    RC = FPlanComp->CreateUnitPlan(APtId);
    // отправить изменения выше.
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
bool TdsICClass::PatGrChData(__int64 APtId, UnicodeString AData)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  try
   {
    if (AData.Trim().Length())
     RC = FRegComp->PatGrChData1(IntToStr(APtId), (TJSONObject *)TJSONObject::ParseJSONValue(AData));
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsICClass::FindAddr(UnicodeString AStr, UnicodeString ADefAddr, int AParams)
 {
  TJSONObject * RC = NULL;
  try
   {
    try
     {
      RC = FKLADRComp->Find(AStr, ADefAddr, AParams);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString TdsICClass::AddrCodeToText(UnicodeString ACode, int AParams)
 {
  return FGetAddrStr(ACode, AParams);
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsICClass::FindOrg(UnicodeString AStr)
 {
  TJSONObject * RC = NULL;
  try
   {
    try
     {
      RC = FOrgComp->Find(AStr);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString TdsICClass::OrgCodeToText(UnicodeString ACode, UnicodeString ASetting)
 {
  return FGetOrgStr(ACode);
 }
// ----------------------------------------------------------------------------
void TdsICClass::FCardEdited(__int64 AUCode, int AType, int AInfCode)
 {
   FPlanComp->ClearHandSch(AUCode, AType, AInfCode);
 }
// ----------------------------------------------------------------------------

