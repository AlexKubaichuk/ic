//---------------------------------------------------------------------------

#ifndef dsSearchDMUnitH
#define dsSearchDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
//---------------------------------------------------------------------------
//#include "ICUtils.h"
//#include "IcsXMLDoc.h"
//#include "dsKLADRUnit.h"
#include <System.JSON.hpp>
#include "XMLContainer.h"
#include <FireDAC.Phys.FB.hpp>
//#include "dsSrvClassifUnit.h"
//#include "OrgParsers.h"
#include <FireDAC.Phys.FBDef.hpp>
#include "icsLog.h"
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.VCLUI.Wait.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "kabQueryUtils.h"
//#include <IPPeerClient.hpp>
//---------------------------------------------------------------------------
class TdsSearchDM : public TDataModule
{
__published:	// IDE-managed Components
 TFDConnection *SearchCon;
private:	// User declarations
//  typedef TAnsiStrMap TSearchCodeList;
  TAnsiStrMap FCurFindCodes;
  TAnsiStrMap FFindDataCode;
  TAnsiStrMap FEraseDataCode;
  TTagNode * FRegDef;
//  __int64 __fastcall GetNewCode(UnicodeString TabId);

  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AParam1Name,      Variant AParam1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AParam2Name = "", Variant AParam2Val = 0,
                               UnicodeString AParam3Name = "", Variant AParam3Val = 0,
                               UnicodeString AParam4Name = "", Variant AParam4Val = 0);


public:		// User declarations
  __fastcall TdsSearchDM(TComponent* Owner, const UnicodeString ADBPath);
  __fastcall ~TdsSearchDM();
  void __fastcall TdsSearchDM::Update(UnicodeString ACode, UnicodeString AStr);
  TJSONObject* __fastcall TdsSearchDM::Find(UnicodeString AId, UnicodeString AFindData);
  __property TTagNode * RegDef = {read=FRegDef, write=FRegDef};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsSearchDM *dsSearchDM;
//---------------------------------------------------------------------------
#endif
