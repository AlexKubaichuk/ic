﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsSrvRegUnit.h"
#include "JSONUtils.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
__fastcall TdsSrvRegistry::TdsSrvRegistry(TComponent * Owner, TFDConnection * AConnection, TAxeXMLContainer * AXMLList,
  const UnicodeString ARegGUI, const UnicodeString AImpGUI, bool APreload, UnicodeString AUser, UnicodeString APass)
  : TComponent(Owner)
 {
  dsLogC(__FUNC__);
  FDM = new TdsRegDM(this, AConnection, AXMLList, ARegGUI, APreload, AUser, APass);
  dsLogMessage("new TdsRegDM", "", 5);
  FDataImporter = new TDataImporter(AConnection, ARegGUI, AImpGUI, FDM->XMLList);
  dsLogMessage("new TDataImporter", "", 5);
  FCardEditors = new TdsRegCardEditors(FDM);
  dsLogMessage("new TdsRegCardEditors", "", 5);
  dsLogC(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
__fastcall TdsSrvRegistry::~TdsSrvRegistry()
 {
  dsLogD(__FUNC__);
  delete FCardEditors;
  delete FDataImporter;
  delete FDM;
  dsLogD(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvRegistry::SetRegDef(TTagNode * AVal)
 {
  FDM->RegDef = AVal;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TdsSrvRegistry::GetCount(System::UnicodeString AId, System::UnicodeString AFilterParam)
 {
  dsLogBegin(__FUNC__);
  __int64 RC = 0;
  try
   {
    RC = FDM->GetCount(AId, AFilterParam);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvRegistry::GetIdList(System::UnicodeString AId, int AMax,
  System::UnicodeString AFilterParam)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC;
  try
   {
    RC = FDM->GetIdList(AId, AMax, AFilterParam);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvRegistry::GetImportValById(System::UnicodeString AId, System::UnicodeString ARecId,
  TJSONObject * ACls)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC;
  try
   {
    RC = FDM->GetImportValById(AId, ARecId, ACls);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvRegistry::GetValById(System::UnicodeString AId, System::UnicodeString ARecId)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC;
  try
   {
    RC = FDM->GetValById(AId, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvRegistry::GetValById10(System::UnicodeString AId, System::UnicodeString ARecId)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC;
  try
   {
    RC = FDM->GetValById10(AId, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvRegistry::Find(System::UnicodeString AId, UnicodeString AFindData)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC;
  try
   {
    RC = FDM->Find(AId, AFindData);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvRegistry::ImportData(System::UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId, UnicodeString & AResMessage)
 {
  dsLogBegin(__FUNC__);
  AResMessage = "";
  UnicodeString RC = "";
  try
   {
    FDataImporter->OnImportCard = ImportCardData;
    RC                          = FDataImporter->ImportData(AId, AValue, ARecId, AResMessage);
    if ((AId == "1000") && SameText(RC, "ok"))
     {
      TJSONObject * PtData = GetImportValById("1000", ARecId, NULL);
      try
       {
        FDM->UpdateSearchData(AId, PtData, ARecId, true);
        UnitDataChanged(ARecId.ToIntDef(0));
       }
      __finally
       {
        delete PtData;
       }
     }
   }
  __finally
   {
    dsLogMessage("8", __FUNC__, 1);
    FDataImporter->OnImportCard = NULL;
    dsRequestLogMessage(AValue->ToString(), "Import data class" + AId.Trim().UpperCase());
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvRegistry::InsertData(System::UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    RC = FDataImporter->InsertOrUpdateData(AId, AValue, ARecId);
    if ((AId == "1000") && SameText(RC, "ok"))
     {
      FDM->UpdateSearchData(AId, AValue, ARecId);
      UnitDataChanged(ARecId.ToIntDef(0));
     }
   }
  __finally
   {
    dsRequestLogMessage(AValue->ToString(), "Insert data class" + AId.Trim().UpperCase());
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvRegistry::EditData(System::UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    RC = FDataImporter->InsertOrUpdateData(AId, AValue, ARecId);
    if ((AId == "1000") && SameText(RC, "ok"))
     {
      FDM->UpdateSearchData(AId, AValue, ARecId);
      UnitDataChanged(ARecId.ToIntDef(0));
     }
   }
  __finally
   {
    dsRequestLogMessage(AValue->ToString(), "Edit data class" + AId.Trim().UpperCase());
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvRegistry::DeleteData(System::UnicodeString AId, System::UnicodeString ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    RC = FDM->DeleteData(AId, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvRegistry::ImportCardData(System::UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    __int64 FUnitCode;
    RC = FCardEditors->ImportData(AId, AValue, ARecId, FUnitCode);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvRegistry::InsertCardData(System::UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    __int64 FUnitCode;
    RC = FCardEditors->InsertData(AId, AValue, ARecId, FUnitCode);
    if (SameText(RC, "ok"))
     UnitDataChanged(FUnitCode);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvRegistry::EditCardData(System::UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    __int64 FUnitCode;
    RC = FCardEditors->EditData(AId, AValue, ARecId, FUnitCode);
    if (SameText(RC, "ok"))
     UnitDataChanged(FUnitCode);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvRegistry::DeleteCardData(System::UnicodeString AId, System::UnicodeString ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    __int64 FUnitCode;
    RC = FCardEditors->DeleteData(AId, ARecId, FUnitCode);
    if (SameText(RC, "ok"))
     UnitDataChanged(FUnitCode);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvRegistry::GetTextData(System::UnicodeString AId, System::UnicodeString AFlId,
  System::UnicodeString ARecId, UnicodeString & AMsg)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    if (FDataConv)
     RC = FDataConv(cdtGet, AId, AFlId, FDM->GetTextData(AId, AFlId, ARecId, AMsg));
    else
     RC = FDM->GetTextData(AId, AFlId, ARecId, AMsg);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvRegistry::SetTextData(System::UnicodeString AId, System::UnicodeString AFlId,
  System::UnicodeString ARecId, System::UnicodeString AVal)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    if (FDataConv)
     RC = FDM->SetTextData(AId, AFlId, ARecId, FDataConv(cdtSet, AId, AFlId, AVal));
    else
     RC = FDM->SetTextData(AId, AFlId, ARecId, AVal);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TdsDIOnExtGetCodeEvent __fastcall TdsSrvRegistry::FGetOnExtGetCode()
 {
  if (FDataImporter)
   return FDataImporter->OnExtGetCode;
  else
   return NULL;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvRegistry::FSetOnExtGetCode(TdsDIOnExtGetCodeEvent AVal)
 {
  if (FDataImporter)
   FDataImporter->OnExtGetCode = AVal;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvRegistry::GetPrivVar(__int64 APrivCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    RC = FCardEditors->GetPrivVar(APrivCode);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvRegistry::GetOtvodInf(__int64 AOtvodCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    RC = FCardEditors->GetOtvodInf(AOtvodCode);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TdsRegGetStrValByCodeEvent __fastcall TdsSrvRegistry::FGetOnGetAddrStr()
 {
  if (FDM)
   return FDM->OnGetAddrStr;
  else
   return NULL;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvRegistry::FSetOnGetAddrStr(TdsRegGetStrValByCodeEvent AVal)
 {
  if (FDM)
   FDM->OnGetAddrStr = AVal;
 }
// ----------------------------------------------------------------------------
TdsRegGetStrValByCodeEvent __fastcall TdsSrvRegistry::FGetOnGetOrgStr()
 {
  if (FDM)
   return FDM->OnGetOrgStr;
  else
   return NULL;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvRegistry::FSetOnGetOrgStr(TdsRegGetStrValByCodeEvent AVal)
 {
  if (FDM)
   FDM->OnGetOrgStr = AVal;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvRegistry::UnitDataChanged(__int64 ACode)
 {
  if (FOnUnitDataChanged)
   FOnUnitDataChanged(ACode);
 }
// ----------------------------------------------------------------------------
TdsRegGetStrValByCodeEvent __fastcall TdsSrvRegistry::FGetOnGetFilterData()
 {
  if (FDM)
   return FDM->OnGetFilterData;
  else
   return NULL;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvRegistry::FSetOnGetFilterData(TdsRegGetStrValByCodeEvent AVal)
 {
  if (FDM)
   FDM->OnGetFilterData = AVal;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvRegistry::GetMIBPName(int AGUIType, UnicodeString ACode)
 {
  UnicodeString RC = "error " + IntToStr(AGUIType) + ":" + ACode;
  try
   {
    if (FDM)
     RC = FDM->GetMIBPName(AGUIType, ACode);
    else
     RC = "!FDM";
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TdsKLADRClass * __fastcall TdsSrvRegistry::FGetKLADR()
 {
  TdsKLADRClass * RC = NULL;
  try
   {
    if (FDataImporter)
     RC = FDataImporter->KLADR;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvRegistry::FSetKLADR(TdsKLADRClass * AValue)
 {
  if (FDataImporter)
   FDataImporter->KLADR = AValue;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSrvRegistry::PatGrChData1(System::UnicodeString ARecId, TJSONObject * AValue)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  try
   {
    RC = FDM->PatGrChData1(ARecId, AValue);
    if (RC)
     {
      TJSONObject * PtData = GetImportValById("1000", ARecId, NULL);
      try
       {
        FDM->UpdateSearchData("1000", PtData, ARecId, true);
        UnitDataChanged(ARecId.ToIntDef(0));
       }
      __finally
       {
        delete PtData;
       }
     }
   }
  __finally
   {
    dsRequestLogMessage(AValue->ToString(), "PatGrChData1 class_1000");
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TdsRegCardEditedEvent __fastcall TdsSrvRegistry::FGetOnEdited()
 {
  return FCardEditors->OnEdited;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvRegistry::FSetOnEdited(TdsRegCardEditedEvent AValue)
 {
  FCardEditors->OnEdited = AValue;
 }
// ----------------------------------------------------------------------------
