//---------------------------------------------------------------------------

#ifndef dsRegDMUnitH
#define dsRegDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
//---------------------------------------------------------------------------
#include <System.JSON.hpp>
#include "XMLContainer.h"

#include "srvsetting.h"
#include <Vcl.ExtCtrls.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <IPPeerClient.hpp>
#ifndef _CONFSETTTING
 #error _CONFSETTTING using are not defined
#endif
#ifdef REG_USE_IMM_NOM
 #include "IMMNomDef.h"
#endif
#include <FireDAC.Phys.FB.hpp>
#include "dsSrvClassifUnit.h"
#include <FireDAC.Phys.FBDef.hpp>
#include "icsLog.h"
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.VCLUI.Wait.hpp>
#include "dsRegClassData.h"
#include "dsRegXMLClassData.h"
#include "dsSearchDMUnit.h"
//#include "SVRClientClassesUnit.h"
//---------------------------------------------------------------------------
typedef UnicodeString  (__closure *TdsRegGetStrValByCodeEvent)(UnicodeString ACode, unsigned int AParams);
//typedef void (__closure *TdsSearchAddEvent)(UnicodeString ACode, UnicodeString AStr);

//---------------------------------------------------------------------------
class TdsRegDM : public TDataModule
{
__published:	// IDE-managed Components
private:	// User declarations
  void __fastcall CreateXMLs(UnicodeString ATabName);
//  typedef map <UnicodeString, TStringList*> TListMap;
  TFDConnection *FConnection;

  TTagNode *FRegDef;
  TAxeXMLContainer *FXMLList;
  TdsCommClassData * FClsData;

#ifdef REG_USE_IMM_NOM
  TICSIMMNomDef      *FImmNom;
  TFDQuery* FImmNomQuery;
#endif

  TFDQuery* FClassDataQuery;
  TdsRegClassData *FClassData;
  TdsRegXMLClassData *FXMLClassData;

  UnicodeString FRegGUI;
  UnicodeString __fastcall GetClassData(UnicodeString ACode, UnicodeString AName);
  void __fastcall ClearHash();
  void __fastcall FSetRegDef(TTagNode *AVal);
  TdsSrvClassifDataMap  FClassifData;
  TdsRegGetStrValByCodeEvent  FOnGetAddrStr;
  TdsRegGetStrValByCodeEvent  FOnGetOrgStr;
  TdsRegGetStrValByCodeEvent  FOnGetFilterData;
  TdsSearchDM  *FSearch;

//  TAnsiStrMap   FSelectSQLMap;
//  TAnsiStrMap   FClassSQLMap;
//  TAnsiStrMap   FSelectRecSQLMap;
//  TAnsiStrMap   FCountSQLMap;
//  TAnsiStrMap   FDeleteSQLMap;
//  TAnsiStrMap   FKeyFlMap;
//  TListMap      FFields;
//  TAnsiStrMap   FSelectSQLMap;
//  TAnsiStrMap   FSelectSQLMap;



  UnicodeString __fastcall GetPlanVacType(UnicodeString  AVType);
  UnicodeString __fastcall FGetDepVal(UnicodeString AVals, UnicodeString AUID );
  bool __fastcall FDepValIsNull(UnicodeString AVals);
  UnicodeString __fastcall GetDependValSQL(UnicodeString ARef, bool AEmptyIfNoDepend);
  void __fastcall GetDependClassCode(UnicodeString ADepClName, UnicodeString ADepFlVal, UnicodeString ARetFlName, UnicodeString &ARetVals);

  void __fastcall GetSQLs(UnicodeString AId);
  void __fastcall CheckSQLs(UnicodeString AId);
  UnicodeString __fastcall GetKeyFl(UnicodeString AId);
  UnicodeString __fastcall GetFlNameById(UnicodeString ATabId, UnicodeString AFlId);
  UnicodeString __fastcall GetTabName(UnicodeString AId);
  UnicodeString __fastcall GetCountSQL(UnicodeString AId);
  UnicodeString __fastcall GetDeleteSQL(UnicodeString AId);
  UnicodeString __fastcall GetClassSQL(UnicodeString AId);
  UnicodeString __fastcall GetSelectSQL(UnicodeString AId);
  UnicodeString __fastcall GetOrderSQL(UnicodeString AId);
  UnicodeString __fastcall GetSelectRecSQL(UnicodeString AId);
  UnicodeString __fastcall GetSelectRec10SQL(UnicodeString AId);
  bool          __fastcall IsQuotedKey(UnicodeString AId);
  TStringList* __fastcall GetFieldsList(UnicodeString AId);
  TJSONObject* __fastcall GetExtEditVal(TTagNode *AFlDef, UnicodeString ARecId, UnicodeString ACode, TFDQuery *AFQ, UnicodeString &AValue, TJSONObject * AClsRC = NULL);
  UnicodeString __fastcall SetDay(int AYear, int AMonth, int ADay );
  UnicodeString __fastcall AddrStrByCode(UnicodeString ACode, unsigned int AParams = 0x0FF);
  UnicodeString __fastcall OrgStrByCode(UnicodeString ACode);
  void          __fastcall AddImportClsData(TJSONObject* ACls, System::UnicodeString AId, System::UnicodeString ACode);
  TJSONObject*  __fastcall GetRowValue(bool AExport, TStringList* AFlList, TFDQuery *AQ, System::UnicodeString ARecId, TJSONObject*  ARowRC, TJSONObject*  AClsRC = NULL);

  void __fastcall FRegGetSVal(UnicodeString AFuncName, TTagNode *ANode, UnicodeString &ATab, UnicodeString &ARet);
  UnicodeString GetWhereFromFilter (TTagNode *AFlt, UnicodeString AMainISUID);
  TStringList* __fastcall GetListFl(UnicodeString AId);

  TTagNode* __fastcall FOnGetXML(UnicodeString AId);
  UnicodeString __fastcall FOnGetDocExtSQL(UnicodeString  AId);
  UnicodeString __fastcall GetExtEditSearchVal(TTagNode *AFlDef, UnicodeString ACode, TJSONObject *AValue, bool AImport);
  UnicodeString __fastcall GetJSONSearchData(TJSONObject *AValue, const UnicodeString AId, bool AImport);
//  TdsSearchClassClient * FSearchClient;

public:		// User declarations
  __fastcall TdsRegDM(TComponent* Owner, TFDConnection *AConnection, TAxeXMLContainer *AXMLList, UnicodeString ARegGUI, bool APreload = true, UnicodeString AUser = "", UnicodeString APass = "");
  __fastcall ~TdsRegDM();


//  void __fastcall LoadFullData(TdsSearchAddEvent AAdd = NULL);
  UnicodeString __fastcall GetSchName(int AGUIType, UnicodeString  ARef);
  UnicodeString __fastcall GetMIBPName(int AGUIType, UnicodeString  ACode);
  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  UnicodeString __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AP1Name,      Variant AP1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AP2Name = "", Variant AP2Val = 0,
                               UnicodeString AP3Name = "", Variant AP3Val = 0,
                               UnicodeString AP4Name = "", Variant AP4Val = 0,
                               UnicodeString AP5Name = "", Variant AP5Val = 0);

  UnicodeString __fastcall AgeStr(TDateTime ACurDate, TDateTime ABirthDay);
  UnicodeString __fastcall AgeStrY(TDateTime ACurDate, TDateTime ABirthDay);

  __int64       __fastcall GetCount(UnicodeString AId, System::UnicodeString AFilterParam);
  TJSONObject*  __fastcall GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam);
  TJSONObject*  __fastcall GetImportValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject * ACls);
  TJSONObject*  __fastcall GetValById(System::UnicodeString AId, System::UnicodeString ARecId);
  TJSONObject*  __fastcall GetValById10(System::UnicodeString AId, System::UnicodeString ARecId);
  TJSONObject*  __fastcall Find(System::UnicodeString AId, UnicodeString AFindData);
  UnicodeString __fastcall DeleteData(System::UnicodeString AId, System::UnicodeString ARecId);
  UnicodeString __fastcall SetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString AVal);
  UnicodeString __fastcall GetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, UnicodeString &AMsg);
  void          __fastcall UpdateSearchData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString ARecId, bool AImport = false);
  bool          __fastcall PatGrChData1(System::UnicodeString  ARecId, TJSONObject * AValue);

  __property TTagNode *RegDef = {read = FRegDef, write=FSetRegDef};

  __property TAxeXMLContainer *XMLList = {read = FXMLList};

  __property TdsRegGetStrValByCodeEvent  OnGetAddrStr = {read = FOnGetAddrStr, write=FOnGetAddrStr};
  __property TdsRegGetStrValByCodeEvent  OnGetOrgStr = {read = FOnGetOrgStr, write=FOnGetOrgStr};
  __property TdsRegGetStrValByCodeEvent  OnGetFilterData = {read = FOnGetFilterData, write=FOnGetFilterData};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsRegDM *dsRegDM;
//---------------------------------------------------------------------------
#endif
