// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsRegDMUnit.h"
#ifndef _CONFSETTTING
#error _CONFSETTTING using are not defined
#endif
#ifdef REG_USE_DOC
#include "dsDocXMLUtils.h"
#endif
#include "dsDocSQLCreator.h"
#include "ICSDATEUTIL.hpp"
#ifdef REG_USE_KLADR
#include "KLADRParsers.h"
#endif
#ifdef REG_USE_ORG
#include "OrgParsers.h"
#endif
#include "JSONUtils.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TdsRegDM * dsRegDM;
// ---------------------------------------------------------------------------
const UnicodeString ICErrMsg[] =
 {
  " ",
  "\"�������� -> ����\"",
  "\"�����\"",
  "\"��������\"",
  "\"�������� -> �����\"",
  "\"�������� -> ��������\"",
  "\"������������� ����\"",
  "\"����\"",
  "\"���� ����\"",
  "\"�������� -> ����\"",
  "\"�����\"",
  "\"������� ������� �� ����\"",
  "\"������� ������� �� �����\"",
  "\"������� ������� �� ��������\"",
  "\"������� ������� �� ����\"",
  "\"��������\"",
  "\"�������� (�� ���������)\"",
  "\"�����\"",
  "\"������� ������� �� �����\"",
  "\"�������� -> �����\"",
  "\"�����\"",
  "\"������� ������� �� ��������\"",
  "\"�����������\"",
  "\"�����������\"",
  "\"������ ���. ���������\"",
  "\"���������\"",
  "\"������ ���. ���������\"",
  "\"�������\"",
  "\"������� �� �������\"",
  "\"�����������\"",
  "\"������ ���������\"",
  "\"������ ���������\"",
  "\"����� �������\"",
  "\"��������\"",
  "\"�������� (�� ���������)\"",
  "\"�������\"",
  "\"������� �� �������\"",
  "\"����� �������\"",
  "\"������ ���������\"",
  "\"������ ���������\"",
  "\"����� �������\"",
  "\"��������\"",
  "\"�����\"",
  "\"��������\"",
  "\"�������\"",
  "\"��������\"",
  "\"��������\"",
  "\"�������� (�� ���������)\"",
  "\"�������� (�� ���������)\"",
  "\"�����\"",
  "\"�����\"",
  "\"������� �� �������\"",
  "\"������ ���������\"",
  "\"����� �������\"",
  "\"������ ���������\"",
  "\"����� �������������\"",
  "\"��������\"",
  "\"�������� (�� ���������)\"",
  "\"�����\"",
  "\"������ ���������\"",
  "\"��������\"",
  "\"�������� (�� ���������)\"",
  "\"�����\"",
  "\"������ �������\"",
  "\"��������\"",
  "\"�������� (�� ���������)\"",
  "\"������ �������\"",
  "\"������ �������\"",
  "\"���� (�� �����)\"",
  "\"�����\"",
  "\"������ �������\"",
  "\"���� (�� �����)\"",
  "\"������ �������\"",
  "\"���� (�� �����)\"",
  "\"������������� �����������\"",
  "\"�������������� �����������\"",
  "\"������ ������\"",
  "\"������ ��������\"",
  "\"���� (�����)\"",
  "\"��������\"",
  "\"����\"",
  "\"�������� (�� ���������)\"",
  "\"�����\"",
  "\"������ (�� ���������)\"",
  "\"��������������� ����\"",
  "\"���� �� ���������\"",
  "\"��������\"",
  "\"��������\"",
  "\"��������\"",
  "\"��������\"",
  "\"��������\"",
  "\"������ �������\"",
  "\"�������� -> ��������\"",
  "\"��������\"",
  "\"����\"",
  "\"���� �� ���������\"",
  "\"����\"",
  "\"�������� t�c\"",
  "\"�������� t�c\"",
  " ",
  "\"������ ���������\""
 };
// ---------------------------------------------------------------------------
__fastcall TdsRegDM::TdsRegDM(TComponent * Owner, TFDConnection * AConnection, TAxeXMLContainer * AXMLList,
  UnicodeString ARegGUI, bool APreload, UnicodeString AUser, UnicodeString APass) : TDataModule(Owner)
 {
  FConnection = AConnection;
  FRegDef     = new TTagNode;
  FRegGUI     = ARegGUI;
  FXMLList    = AXMLList;
  // SearchConnnect->UserName = AUser;
  // SearchConnnect->Password = APass;
#ifdef REG_USE_IMM_NOM
  FImmNom  = NULL;
  FClsData = NULL;
  if (ARegGUI.UpperCase() == "40381E23-92155860-4448")
   {
    FImmNomQuery = CreateTempQuery();
    FClsData     = new TdsCommClassData(AConnection);
    FImmNom      = new TICSIMMNomDef(FImmNomQuery, FXMLList, "40381E23-92155860-4448", FClsData);
   }
#endif
  FClassDataQuery = CreateTempQuery();
  dsLogMessage("new TdsRegClassData", __FUNC__);
  FClassData = new TdsRegClassData(FXMLList->GetXML(FRegGUI), FClassDataQuery);
  dsLogMessage("TdsRegClassData", __FUNC__);
  FXMLClassData           = new TdsRegXMLClassData;
  FXMLClassData->OnGetXML = FOnGetXML;
  dsLogMessage(FRegGUI, __FUNC__);
  if (FRegGUI.UpperCase() == "549F0EBE-1B9B98A7-BD64")
   FClassData->OnGetExtSQL = FOnGetDocExtSQL;
  dsLogMessage(FRegGUI, "end");
  FSearch         = new TdsSearchDM(this, AConnection->Params->Database);
  FSearch->RegDef = FXMLList->GetXML(FRegGUI);
  // FSearchClient = new TdsSearchClassClient(SearchConnnect);
 }
// ---------------------------------------------------------------------------
__fastcall TdsRegDM::~TdsRegDM()
 {
  delete FRegDef;
  delete FSearch;
  for (TdsSrvClassifDataMap::iterator i = FClassifData.begin(); i != FClassifData.end(); i++)
   delete i->second;
  FClassifData.clear();
#ifdef REG_USE_IMM_NOM
  if (FImmNom)
   {
    delete FImmNom;
    DeleteTempQuery(FImmNomQuery);
   }
#endif
  delete FClassData;
  DeleteTempQuery(FClassDataQuery);
  delete FXMLClassData;
  if (FClsData)
   delete FClsData;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsRegDM::FOnGetXML(UnicodeString AId)
 {
  return FXMLList->GetXML(AId);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetClassData(UnicodeString ACode, UnicodeString AName)
 {
  UnicodeString RC = "";
  try
   {
    RC = FClassData->GetData(AName, ACode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegDM::ClearHash()
 {
  FClassData->ClearHash("");
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegDM::FSetRegDef(TTagNode * AVal)
 {
  FRegDef->AsXML = AVal->AsXML;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegDM::GetSQLs(UnicodeString AId)
 {
  TdsSrvClassifSQLData * tmpData = new TdsSrvClassifSQLData;
  try
   {
    GetClassifSQLData(FRegDef, AId, tmpData);
    FClassifData[AId] = tmpData;
   }
  catch (Exception & E)
   {
    dsLogError(E.Message + "; ID = " + AId, __FUNC__);
    throw E;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegDM::CheckSQLs(UnicodeString AId)
 {
  try
   {
    TdsSrvClassifDataMap::iterator FRC = FClassifData.find(AId);
    if (FRC == FClassifData.end())
     GetSQLs(AId);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetKeyFl(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->KeyFl;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TStringList * __fastcall TdsRegDM::GetListFl(UnicodeString AId)
 {
  TStringList * RC = NULL;
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->ListFields;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetFlNameById(UnicodeString ATabId, UnicodeString AFlId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(ATabId);
    RC = FClassifData[ATabId]->FieldNameById(AFlId);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetTabName(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->TabName;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetCountSQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->CountSQL;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetDeleteSQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->DeleteSQL;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetClassSQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->ClassSQL;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetSelectSQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->SelectSQL;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetOrderSQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->OrderSQL;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetSelectRecSQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->SelectRecSQL;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetSelectRec10SQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->SelectRec10SQL;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegDM::IsQuotedKey(UnicodeString AId)
 {
  bool RC = false;
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->QuotedKey;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TStringList * __fastcall TdsRegDM::GetFieldsList(UnicodeString AId)
 {
  TStringList * RC = NULL;
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->Fields;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TdsRegDM::GetCount(UnicodeString AId, System::UnicodeString AFilterParam)
 {
  __int64 RC = 0;
  TFDQuery * FQ = CreateTempQuery();
  TJSONObject * tmpFlData = NULL;
  TTagNode * tmpFlt = NULL;
  try
   {
    bool FFiltered = false;
    if (AFilterParam.Length())
     {
      if (AFilterParam[1] == '{')
       {
        tmpFlData = (TJSONObject *)TJSONObject::ParseJSONValue(AFilterParam);
        if (!tmpFlData->Null && FOnGetFilterData)
         {
          if (tmpFlData->Pairs[0]->JsonString->Value().UpperCase() == "GUI")
           {
            tmpFlt        = new TTagNode;
            tmpFlt->AsXML = FOnGetFilterData(((TJSONString *)tmpFlData->Pairs[0]->JsonValue)->Value(), 0);
           }
         }
        FFiltered = true;
       }
      else if (AFilterParam.Pos("<?xml"))
       {
        tmpFlt        = new TTagNode;
        tmpFlt->AsXML = AFilterParam;
        FFiltered     = true;
       }
     }
    if (FFiltered)
     {
      if (tmpFlt)
       {
        if (quExec(FQ, false, GetCountSQL(AId) + " where " + GetWhereFromFilter(tmpFlt, "0E291426-00005882-2493.0000")))
         RC = FQ->FieldByName("RCount")->AsInteger;
       }
      else
       {
        UnicodeString FFilterWhere = "";
        TJSONPairEnumerator * itPair = tmpFlData->GetEnumerator();
        while (itPair->MoveNext())
         {
          if (FFilterWhere.Length())
           FFilterWhere += " and ";
          // FFilterWhere += " r"+((TJSONString*)itPair->Current->JsonString)->Value()+"="+((TJSONString*)itPair->Current->JsonValue)->Value();
          // FFilterWhere += " r"+JSONEnumPairCode(itPair)+"="+JSONEnumPairValue(itPair);
          FFilterWhere += " " + GetFlNameById(AId, JSONEnumPairCode(itPair)) + "=" + JSONEnumPairValue(itPair);
         }
        if (quExec(FQ, false, GetCountSQL(AId) + " where " + FFilterWhere))
         RC = FQ->FieldByName("RCount")->AsInteger;
       }
     }
    else
     {
      if (quExec(FQ, false, GetCountSQL(AId)))
       RC = FQ->FieldByName("RCount")->AsInteger;
     }
   }
  __finally
   {
    if (tmpFlt)
     delete tmpFlt;
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsRegDM::GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam)
 {
  TJSONArray * RC = new TJSONArray;
  TFDQuery * FQ = CreateTempQuery();
  TJSONObject * tmpFlData = NULL;
  TTagNode * tmpFlt = NULL;
  try
   {
    bool FFiltered = false;
    if (AFilterParam.Length())
     {
      if (AFilterParam[1] == '{')
       {
        tmpFlData = (TJSONObject *)TJSONObject::ParseJSONValue(AFilterParam);
        if (!tmpFlData->Null && FOnGetFilterData)
         {
          if (tmpFlData->Pairs[0]->JsonString->Value().UpperCase() == "GUI")
           {
            tmpFlt        = new TTagNode;
            tmpFlt->AsXML = FOnGetFilterData(((TJSONString *)tmpFlData->Pairs[0]->JsonValue)->Value(), 0);
           }
         }
        FFiltered = true;
       }
      else if (AFilterParam.Pos("<?xml"))
       {
        tmpFlt        = new TTagNode;
        tmpFlt->AsXML = AFilterParam;
        FFiltered     = true;
       }
     }
    UnicodeString FSQL = "";
    if (FFiltered)
     {
      if (tmpFlt)
       FSQL = GetSelectSQL(AId) + " where " + GetWhereFromFilter(tmpFlt, "0E291426-00005882-2493.0000");
      else
       {
        UnicodeString FFilterWhere = "";
        TJSONPairEnumerator * itPair = tmpFlData->GetEnumerator();
        while (itPair->MoveNext())
         {
          if (FFilterWhere.Length())
           FFilterWhere += " and ";
          // FFilterWhere += " r"+((TJSONString*)itPair->Current->JsonString)->Value()+"="+((TJSONString*)itPair->Current->JsonValue)->Value();
          // FFilterWhere += " r"+JSONEnumPairCode(itPair)+"="+JSONEnumPairValue(itPair);
          FFilterWhere += " " + GetFlNameById(AId, JSONEnumPairCode(itPair)) + "=" + JSONEnumPairValue(itPair);
         }
        FSQL = GetSelectSQL(AId) + " where " + FFilterWhere;
       }
     }
    else
     FSQL = GetSelectSQL(AId);
    if (GetOrderSQL(AId).Length())
     FSQL += " Order by " + GetOrderSQL(AId); /* sort order */
    if (quExec(FQ, false, FSQL))
     {
      TStringList * FFields = GetListFl(AId);
      while (!FQ->Eof)
       {
        tmpFlData = new TJSONObject;
        tmpFlData->AddPair("code", FQ->FieldByName(GetKeyFl(AId))->AsString);
        if (FFields)
         GetRowValue(false, FFields, FQ, FQ->FieldByName(GetKeyFl(AId))->AsString, tmpFlData);
        RC->Add(tmpFlData);
        FQ->Next();
       }
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
    if (tmpFlt)
     delete tmpFlt;
   }
  return (TJSONObject *)RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsRegDM::GetImportValById(System::UnicodeString AId, System::UnicodeString ARecId,
  TJSONObject * ACls)
 {
  TJSONObject * RC = NULL; // new TJSONObject;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    if (!quExecParam(FQ, false, GetSelectRecSQL(AId), "p" + GetKeyFl(AId), ARecId).Length())
     {
      if (FQ->RecordCount)
       {
        TStringList * FFlList = GetFieldsList(AId);
        RC = GetRowValue(true, FFlList, FQ, ARecId, NULL, ACls);
        if (RC)
         RC->AddPair(GetKeyFl(AId), ARecId);
       }
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsRegDM::GetValById(System::UnicodeString AId, System::UnicodeString ARecId)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC = new TJSONObject;
  TFDQuery * FQ = CreateTempQuery();
  bool FUpdateSearch = AId.Pos("updatesearch");
  UnicodeString FId = AId;
  if (FUpdateSearch)
   FId = GetLPartB(FId, ':');
  try
   {
    try
     {
      if (!FId.Pos("."))
       { // �������� ��� ������
        if (!quExecParam(FQ, false, GetSelectRecSQL(FId), "p" + GetKeyFl(FId), ARecId).Length())
         {
          if (FQ->RecordCount)
           {
            TStringList * FFlList = GetFieldsList(FId);
            RC->AddPair(GetKeyFl(FId), ARecId);
            GetRowValue(false, FFlList, FQ, ARecId, RC);
            if (FUpdateSearch && (FId == "1000"))
             UpdateSearchData(FId, RC, ARecId, true);
           }
         }
       }
      else
       { // �������� ����
        // uid.uid
        UnicodeString FCmd = GetLPartB(FId).LowerCase();
        UnicodeString FRefId = GetRPartB(FId);
        if (FCmd == "f")
         { // �������� �������� ���� Id2, �������������� Id1 � �����  ARecId
          TTagNode * flDef;
          UnicodeString Id1, Id2, FTab, FFN;
          Id1   = GetLPartB(FRefId).LowerCase();
          Id2   = GetRPartB(FRefId).LowerCase();
          flDef = FRegDef->GetTagByUID(Id1);
          if (flDef)
           {
            FTab = "class_" + flDef->AV["uid"];
            if (flDef->AV["tab"].Length())
             FTab = flDef->AV["tab"];
            flDef = FRegDef->GetTagByUID(Id2);
            if (flDef)
             {
              FFN = "R" + flDef->AV["uid"];
              if (flDef->AV["fl"].Length())
               FFN = flDef->AV["fl"];
              if (!quExecParam(FQ, false, "Select (" + FFN + ") as FlVal From " + FTab + " Where " + GetKeyFl(Id1) +
                "=:FCode", "FCode", ARecId).Length())
               {
                if (FQ->FieldByName("FlVal")->IsNull)
                 RC->AddPair(Id2, new TJSONNull);
                else
                 RC->AddPair(Id2, FQ->FieldByName("FlVal")->AsVariant);
               }
             }
           }
         }
        else if (FCmd == "l")
         { // ������ ���� ���.������
          if (quExec(FQ, false, GetClassSQL(FRefId) + " Order by name"))
           {
            while (!FQ->Eof)
             {
              RC->AddPair(FQ->FieldByName(GetKeyFl(FRefId))->AsString.Trim(), FQ->FieldByName("name")->AsString.Trim());
              FQ->Next();
             }
           }
         }
        else if (FCmd == "d")
         { // ������ ���� ���.������ � �������������
          TTagNode * FClDef = FRegDef->GetTagByUID(GetLPartB(FRefId, ':'));
          if (FClDef)
           {
            UnicodeString FSQL = GetDependValSQL(FRefId, FClDef->CmpAV("comment", "empty"));
            if (FSQL.Length())
             FSQL += " Order by name";
            if (FSQL.Length() && FClDef)
             {
              if (quExec(FQ, false, FSQL))
               {
                while (!FQ->Eof)
                 {
                  RC->AddPair(FQ->FieldByName(GetKeyFl(FClDef->AV["ref"]))->AsString.Trim(),
                    FQ->FieldByName("name")->AsString.Trim());
                  FQ->Next();
                 }
               }
             }
           }
         }
        else if (FCmd == "s2")
         { // ������ ���� ���.������ � �������������
          UnicodeString FSQL = FRefId;
          if (FSQL.Length())
           {
            if (quExec(FQ, false, FSQL))
             {
              while (!FQ->Eof)
               {
                RC->AddPair(FQ->FieldByName("ps1")->AsString, FQ->FieldByName("ps2")->AsString);
                FQ->Next();
               }
             }
           }
         }
        else if (FCmd == "s3")
         { // ������ ���� ���.������ � �������������
          UnicodeString FSQL = FRefId;
          if (FSQL.Length())
           {
            if (quExec(FQ, false, FSQL))
             {
              while (!FQ->Eof)
               {
                RC->AddPair(FQ->FieldByName("ps1")->AsString, FQ->FieldByName("ps2")->AsString + ":" +
                  FQ->FieldByName("ps3")->AsString);
                FQ->Next();
               }
             }
           }
         }
        else if (FCmd == "s4")
         { // ������ ���� ���.������ � �������������
          UnicodeString FSQL = FRefId;
          if (FSQL.Length())
           {
            if (quExec(FQ, false, FSQL))
             {
              while (!FQ->Eof)
               {
                RC->AddPair(FQ->FieldByName("ps1")->AsString, FQ->FieldByName("ps2")->AsString + ":" +
                  FQ->FieldByName("ps3")->AsString + "#" + FQ->FieldByName("ps4")->AsString);
                FQ->Next();
               }
             }
           }
         }
       }
     }
    catch (Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
      throw;
     }
    catch (...)
     {
      dsLogError("��", __FUNC__);
      throw;
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Rollback();
    DeleteTempQuery(FQ);
    dsLogMessage(IntToStr((int)RC), __FUNC__);
    dsLogEnd(__FUNC__);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsRegDM::GetValById10(System::UnicodeString AId, System::UnicodeString ARecId)
 {
  TJSONObject * RC = new TJSONObject;
  TJSONObject * RowRC;
  UnicodeString FRecId = ARecId;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    if (!AId.Pos("."))
     { // �������� ��� ������
      System::Sysutils::TReplaceFlags flRep;
      flRep << rfReplaceAll << rfIgnoreCase;
      if (IsQuotedKey(AId))
       {
        TStringList * QL = new TStringList;
        QL->Delimiter     = ',';
        QL->DelimitedText = FRecId;
        FRecId            = "";
        try
         {
          for (int i = 0; i < QL->Count; i++)
           {
            if (QL->Strings[i].Trim().Length())
             {
              if (FRecId.Length())
               FRecId += ",";
              FRecId += "'" + QL->Strings[i].Trim() + "'";
             }
           }
         }
        __finally
         {
          delete QL;
         }
       }
      if (quExec(FQ, false, StringReplace(GetSelectRec10SQL(AId), "_KEY_REP_VAL_", FRecId, flRep)))
       {
        TStringList * FFlList = GetFieldsList(AId);
        while (!FQ->Eof)
         {
          RowRC = new TJSONObject;
          RowRC->AddPair("code", FQ->FieldByName(GetKeyFl(AId))->AsString);
          GetRowValue(false, FFlList, FQ, ARecId, RowRC);
          RC->AddPair(GetKeyFl(AId), RowRC);
          FQ->Next();
         }
       }
     }
    else
     { // �������� ����
      dsLogError("error ID: " + AId, __FUNC__);
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsRegDM::GetRowValue(bool AExport, TStringList * AFlList, TFDQuery * AQ,
  System::UnicodeString ARecId, TJSONObject * ARowRC, TJSONObject * AClsRC)
 {
  TJSONObject * RC = ARowRC;
  try
   {
    TTagNode * flDef;
    UnicodeString FId, FFN;
    UnicodeString FExtVal = "";
    if (!RC)
     RC = new TJSONObject;
    for (int i = 0; i < AFlList->Count; i++)
     {
      FId   = AFlList->Strings[i].UpperCase();
      flDef = FRegDef->GetTagByUID(FId);
      FFN   = "R" + FId;
      if (flDef->AV["fl"].Length())
       FFN = flDef->AV["fl"];
      if (AQ->FieldByName(FFN)->IsNull && !flDef->CmpAV("comment", "calc"))
       RC->AddPair(FId, new TJSONNull);
      else
       {
        if (flDef->CmpName("choice, choiceDB"))
         {
          if (AQ->FieldByName(FFN + "Str")->IsNull)
           RC->AddPair(FId, new TJSONNull);
          else if (AQ->FieldByName(FFN + "Str")->AsString.Length())
           RC->AddPair(FId, NewObjPair(AQ->FieldByName(FFN)->AsString, AQ->FieldByName(FFN + "Str")->AsString));
          else
           RC->AddPair(FId, NewObjPair(AQ->FieldByName(FFN)->AsString, new TJSONString("")));
          if (AClsRC && flDef->CmpName("choiceDB"))
           AddImportClsData(AClsRC, flDef->AV["ref"], AQ->FieldByName(FFN)->AsString);
         }
        else if (flDef->CmpName("binary"))
         RC->AddPair(FId, NewObjPair(AQ->FieldByName(FFN)->AsString, UnicodeString((AQ->FieldByName(FFN)->AsInteger) ?
          "+" : "-")));
        else if (flDef->CmpName("choiceTree, extedit"))
         {
          if (!(flDef->CmpName("extedit") && flDef->CmpAV("fieldtype",
            "integer") && (flDef->AV["length"].ToIntDef(0) > 64)))
           RC->AddPair(FId, GetExtEditVal(flDef, ARecId, AQ->FieldByName(FFN)->AsString, AQ, FExtVal, AClsRC));
          else if (!(flDef->CmpName("extedit") && flDef->CmpAV("fieldtype",
            "integer") && (flDef->AV["length"].ToIntDef(0) > 512)))
           GetExtEditVal(flDef, ARecId, AQ->FieldByName(FFN)->AsString, AQ, FExtVal, AClsRC);
         }
        else
         {
          UnicodeString FlVal = AQ->FieldByName(FFN)->AsString;
          if ((FFN.UpperCase() == "R3083") && AExport)
           {
            if (FlVal.UpperCase().Pos("��������"))
             FlVal = "all:��� ��������";
            else
             {
              TFDQuery * FQ = CreateTempQuery();
              try
               {
                if (!quExecParam(FQ, false,
                  "Select ot.R1083, inf.r006e from class_1030 ot left join class_003A inf on (inf.code = ot.R1083) where ot.r3184=:pCode",
                  "pCode", AQ->FieldByName("code")->AsInteger).Length())
                 {
                  FlVal = "";
                  while (!FQ->Eof)
                   {
                    if (FlVal.Length())
                     FlVal += ",";
                    FlVal += FQ->FieldByName("R1083")->AsString + ":" + FQ->FieldByName("r006e")->AsString;
                    FQ->Next();
                   }
                 }
               }
              __finally
               {
                DeleteTempQuery(FQ);
               }
             }
           }
          RC->AddPair(FId, FlVal);
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegDM::AddImportClsData(TJSONObject * ACls, System::UnicodeString AId, System::UnicodeString ACode)
 {
  if (ACode.Trim().Length() && (ACode != "0"))
   {
    TJSONValue * FCls = ACls->Values[AId.UpperCase()];
    if (!FCls)
     {
      ACls->AddPair(AId.UpperCase(), NewObjPair(ACode, GetImportValById(AId.UpperCase(), ACode.UpperCase(), ACls)));
     }
    else
     {
      FCls = ((TJSONObject *)FCls)->Values[ACode.UpperCase()];
      if (!FCls)
       ((TJSONObject *)ACls->Values[AId.UpperCase()])->AddPair(ACode.UpperCase(),
        GetImportValById(AId.UpperCase(), ACode.UpperCase(), ACls));
     }
   }
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsRegDM::Find(System::UnicodeString AId, UnicodeString AFindData)
 {
  return FSearch->Find(AId, AFindData);
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsRegDM::CreateTempQuery()
 {
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = FConnection;
    FQ->Connection                       = FConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  return FQ;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
  UnicodeString AP1Name, Variant AP1Val, UnicodeString ALogComment, UnicodeString AP2Name, Variant AP2Val,
  UnicodeString AP3Name, Variant AP3Val, UnicodeString AP4Name, Variant AP4Val, UnicodeString AP5Name, Variant AP5Val)
 {
  UnicodeString RC = "error";
  TTime FBeg = Time();
  UnicodeString FParams = "; Params: ";
  if (AP1Name.Length())
   FParams += AP1Name + "=" + VarToStrDef(AP1Val, "") + "; ";
  if (AP2Name.Length())
   FParams += AP2Name + "=" + VarToStrDef(AP2Val, "") + "; ";
  if (AP3Name.Length())
   FParams += AP3Name + "=" + VarToStrDef(AP3Val, "") + "; ";
  if (AP4Name.Length())
   FParams += AP4Name + "=" + VarToStrDef(AP4Val, "") + "; ";
  if (AP5Name.Length())
   FParams += AP5Name + "=" + VarToStrDef(AP5Val, "");
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AP1Name)->Value = AP1Val;
      if (AP2Name.Length())
       AQuery->ParamByName(AP2Name)->Value = AP2Val;
      if (AP3Name.Length())
       AQuery->ParamByName(AP3Name)->Value = AP3Val;
      if (AP4Name.Length())
       AQuery->ParamByName(AP4Name)->Value = AP4Val;
      if (AP5Name.Length())
       AQuery->ParamByName(AP5Name)->Value = AP5Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = "";
     }
    catch (EIBNativeException & E)
     {
      if ((E.ErrorCode == 335544517) && E.ErrorCount)
       { // ����������
        RC = "�������� ����������.\n�������� ������������ � ������� " + ICErrMsg[E.Errors[0]->ExceptionCode];
       }
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    if (ALogComment.Length())
     dsLogSQL(ALogComment + FParams, __FUNC__, FBeg - Time(), 5);
    else
     dsLogSQL(ASQL + FParams, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegDM::GetDependClassCode(UnicodeString ADepClName, UnicodeString ADepFlVal,
  UnicodeString ARetFlName, UnicodeString & ARetVals)
 {
  // TFDQuery *FQ = CreateTempQuery();
  // DeleteTempQuery(FQ);
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    if (quExec(FQ, false, "Select " + ARetFlName + " From " + ADepClName + " Where " + ADepFlVal))
     {
      UnicodeString FRetVals = "";
      int FCount = 0;
      while (!FQ->Eof && (FCount < 200))
       {
        FRetVals += FQ->FieldByName(ARetFlName)->AsString + ",";
        FCount++ ;
        FQ->Next();
       }
      if (FCount >= 199)
       {
        FRetVals = "Select " + ARetFlName + " From " + ADepClName + " Where " + ADepFlVal;
       }
      else
       {
        if (!FRetVals.Length())
         FRetVals = "-1";
        else
         FRetVals[FRetVals.Length()] = ' ';
       }
      ARetVals = FRetVals;
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::FGetDepVal(UnicodeString AVals, UnicodeString AUID)
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString FDep, FDepList;
    FDepList = AVals;
    if (FDepList.Length() >= 6)
     {
      while (FDepList.Length() && !RC.Length())
       {
        FDep = GetLPartB(FDepList, ';').Trim();
        if (!FDep.Length())
         FDep = FDepList;
        if (GetLPartB(FDep, '=').Trim().UpperCase() == AUID.UpperCase())
         RC = GetRPartB(FDep, '=').Trim();
        FDepList = GetRPartB(FDepList, ';').Trim();
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegDM::FDepValIsNull(UnicodeString AVals)
 {
  bool RC = true;
  try
   {
    UnicodeString FDep, FDepList;
    FDepList = AVals;
    if (FDepList.Length() >= 6)
     {
      while (FDepList.Length() && RC)
       {
        FDep = GetLPartB(FDepList, ';').Trim();
        if (!FDep.Length())
         FDep = FDepList;
        RC &= !GetRPartB(FDep, '=').Trim().Length();
        FDepList = GetRPartB(FDepList, ';').Trim();
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetDependValSQL(UnicodeString ARef, bool AEmptyIfNoDepend)
 {
  UnicodeString RC = "";
  try
   {
    // FCtrlOwner->FillClass("d."+FNode->AV["ref"]+":"+FNode->AV["depend"]+":"+ADepClCode+":"+ADepUID, (TStringList*)ED->Properties->Items, ACode);
    // UnicodeString FRef = ARef;
    UnicodeString FNodeUid = GetLPartB(ARef, ':'); // uid choiceDB ������� �����������
    UnicodeString FDepVals = GetRPartB(ARef, ':'); // �������� ��������� �� ������� ������� � ������� "UID=��������;"
    TTagNode * FDef = FRegDef->GetTagByUID(FNodeUid);
    if (FDef) // ���� choiceDB
     {
      UnicodeString FTabPref = "";
      UnicodeString FNodeRef = FDef->AV["ref"]; // ������� �� �������������, �� �������� ���� ������
      UnicodeString FNodeDepend = FDef->AV["depend"] + ";";
      // ������ ������������ � ������� uid-��������.id-�������������� � ������� ��������� �����������
      // FRef = GetRPartB(FRef,':');
      // UnicodeString ADepClCode = GetLPartB(FRef,':').UpperCase();
      // UnicodeString ADepUID = GetRPartB(FRef,':').UpperCase();
      UnicodeString FDep, FDepList;
      FDepList = FNodeDepend;
      if (FDepList.Length() >= 9)
       {
        UnicodeString FDepTab, FDepWhereVal, FRetFlName, FRetVals;
        if (!FDepValIsNull(FDepVals))
         {
          UnicodeString FDepUID;
          TTagNode * FDepClDef, *FDepNode, *_tmp;
          FDepTab   = ("CLASS_" + FTabPref + FNodeRef).UpperCase();
          FDepClDef = FRegDef->GetTagByUID(FNodeRef);
          bool IsEquDepend = true;
          while (FDepList.Length() && IsEquDepend)
           {
            FDep = GetLPartB(FDepList, ';').UpperCase();
            IsEquDepend &= (FDepTab == "CLASS_" + FTabPref + _UID(FDep));
            FDepList = GetRPartB(FDepList, ';').Trim();
           }
          FDepList = FNodeDepend;
          UnicodeString FCtrlUID, FDepCode, FDepClCode, FFlName;
          if (IsEquDepend)
           { // ��� ����������� � ����� �������, ����� ������� ����� where
            FRetVals     = "";
            FDepWhereVal = "";
            while (FDepList.Length())
             {
              FDep       = GetLPartB(FDepList, ';').Trim();
              FCtrlUID   = _GUI(FDep);
              _tmp       = FRegDef->GetTagByUID(FCtrlUID);
              FDepClCode = _tmp->AV["ref"];
              FDepCode   = FGetDepVal(FDepVals, FCtrlUID);
              FDepNode   = FDepClDef->GetChildByAV("", "ref", FDepClCode);
              if (FDepNode)
               {
                FFlName = "Cl0" + FNodeRef + ".R" + FDepNode->AV["uid"];
                if (FDepCode.Length() && (FDepCode.ToIntDef(-1) != -2))
                 {
                  if (FDepWhereVal.Length())
                   FDepWhereVal += " and ";
                  FDepWhereVal += FFlName + "=" + FDepCode;
                 }
                else
                 {
                  if (FDepWhereVal.Length())
                   FDepWhereVal += " and ";
                  FDepWhereVal += " (" + FFlName + "=0 or " + FFlName + " is null)";
                 }
               }
              FDepList = GetRPartB(FDepList, ';').Trim();
             }
           }
          else
           {
            FDepList = FNodeDepend;
            while (FDepList.Length())
             {
              FDep = GetPart1(FDepList, ';').Trim();
              if (!FDep.Length())
               FDep = FDepList;
              // FDep � ������� uid-��������.id-�������������� � ������� ��������� �����������
              FCtrlUID = _GUI(FDep); // FCtrlUID - uid ������� �� �������� ������� ��������
              FDepCode = FGetDepVal(FDepVals, FCtrlUID);
              if (FDepCode.Length())
               {
                FDepUID    = _UID(FDep); // FDepUID uid �������������� � ������� ��������� �����
                FDepTab    = "CLASS_" + FTabPref + FDepUID;
                FDepClDef  = FRegDef->GetTagByUID(FDepUID); // ���� �������������� � ������� ��������� �����
                FDepClCode = FRegDef->GetTagByUID(FCtrlUID)->AV["ref"]; // � ������ ����� �������������� FDepClDef
                FDepNode   = FDepClDef->GetChildByAV("", "ref", FDepClCode /* ADepClCode */);
                // ���������� ���� � ref= ������_��_��������_�������_��������->AV["ref"]
                // ���� � ���, ��� ������������ ����� ������������ �� ���������� ������
                FRetVals = "";
                if (FDepCode.ToIntDef(-1) != -2) // ���������, ������ ����� ???
                 {
                  if (FDepCode.ToIntDef(-1) == -1)
                   {
                    FDepWhereVal = "(R" + FDepNode->AV["uid"] + " is NULL or R" + FDepNode->AV["uid"] + "=0)";
                    FRetFlName   = "CODE";
                   }
                  else
                   {
                    if (FDepUID == FDepClCode /* ADepClCode */)
                      // ��� �������� �������� ����������, ��� ����� ���� ���� ��������� ���������������
                     {
                      FDepWhereVal = "CODE=" + FDepCode;
                     }
                    else if (FDepUID == FNodeRef)
                      // ����������� ��������� � �������������� �������� �� �������� ������� ��������
                     {
                      FDepWhereVal = "R" + FDepNode->AV["uid"] + "=" + FDepCode;
                      FRetFlName   = "CODE";
                     }
                    else // ����������� ��������� � ��������� ��������������
                     {
                      FDepWhereVal = "R" + FDepNode->AV["uid"] + "=" + FDepCode;
                      FRetFlName   = "R" + FDepClDef->GetChildByAV("", "ref", FNodeRef)->AV["uid"];
                     }
                   }
                  GetDependClassCode(FDepTab, FDepWhereVal, FRetFlName, FRetVals);
                  FDepWhereVal = "code in (" + FRetVals + ")";
                 }
               }
              FDepList = GetRPartB(FDepList, ';').Trim();
             }
           }
         }
        if (FDepWhereVal.Length())
         RC = GetClassSQL(FNodeRef) + " Where " + FDepWhereVal;
        // InvadeClassCB(cBox(0),0, false, cNode(0),quFree,FTabPref,FRetVals,FCtrlOwner->DefXML); //+++
        else
         {
          if (!AEmptyIfNoDepend)
           RC = GetClassSQL(FNodeRef);
          // InvadeClassCB(cBox(0),0, false, cNode(0),quFree,FTabPref,FRetVals,FCtrlOwner->DefXML); //+++
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegDM::DeleteData(System::UnicodeString AId, System::UnicodeString ARecId)
 {
  UnicodeString RC = "error";
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      RC = quExecParam(FQ, true, GetDeleteSQL(AId), "p" + GetKeyFl(AId), ARecId);
      if (!RC.Length())
       RC = "ok";
     }
    catch (Exception & E)
     {
      RC = E.Message;
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegDM::GetTextData(System::UnicodeString AId, System::UnicodeString AFlId,
  System::UnicodeString ARecId, UnicodeString & AMsg)
 {
  UnicodeString RC = "";
  AMsg = "error";
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      if (!quExecParam(FQ, false, "Select " + GetFlNameById(AId, AFlId) + " From " + GetTabName(AId) + " where " +
        GetKeyFl(AId) + "=:pKey", "pKey", ARecId).Length())
       {
        RC   = FQ->FieldByName(GetFlNameById(AId, AFlId))->AsString;
        AMsg = "ok";
       }
     }
    catch (Exception & E)
     {
      RC = E.Message;
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegDM::SetTextData(System::UnicodeString AId, System::UnicodeString AFlId,
  System::UnicodeString ARecId, System::UnicodeString AVal)
 {
  UnicodeString RC = "error";
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      if (!quExecParam(FQ, true, "Update " + GetTabName(AId) + " set " + GetFlNameById(AId, AFlId) + "=:pFl where " +
        GetKeyFl(AId) + "=:pKey", "pFl", AVal, "", "pKey", ARecId).Length())
       RC = "ok";
     }
    catch (Exception & E)
     {
      RC = E.Message;
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * __fastcall TdsRegDM::GetExtEditVal(TTagNode * AFlDef, UnicodeString ARecId, UnicodeString ACode,
  TFDQuery * AFQ, UnicodeString & AValue, TJSONObject * AClsRC)
 {
  TJSONObject * RC = new TJSONObject;
  UnicodeString FCode = ACode.Trim();
  UnicodeString FValue = "";
  if (FCode.Length())
   FValue = ACode;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    // ������������ -----------------------------------------------------------
    if (FRegGUI.UpperCase() == "40381E23-92155860-4448")
     {
      if (AFlDef->CmpAV("uid", "00B2")) // �������, ���. �����
         FValue = AgeStr(Date(), AFQ->FieldByName("r0031")->AsDateTime);
      else if (AFlDef->CmpAV("uid", "10B4")) // �������, �������� // "30B4->3155
         FValue = AgeStr(AFQ->FieldByName("R1024")->AsDateTime, AFQ->FieldByName("R1155")->AsDateTime);
      else if (AFlDef->CmpAV("uid", "30B4")) // �������, �������� // "30B4->3155
         FValue = AgeStr(AFQ->FieldByName("R3024")->AsDateTime, AFQ->FieldByName("R3155")->AsDateTime);
      else if (AFlDef->CmpAV("uid", "10B5")) // �������, �����
         FValue = AgeStr(AFQ->FieldByName("R1031")->AsDateTime, AFQ->FieldByName("R1156")->AsDateTime);
      else if (AFlDef->CmpAV("uid", "1105")) // �������, ��������
         FValue = AgeStr(AFQ->FieldByName("R1104")->AsDateTime, AFQ->FieldByName("R1160")->AsDateTime);
      else if (AFlDef->CmpAV("uid", "32A9")) // �������, ���� �� ���������
         FValue = AgeStr(AFQ->FieldByName("R1088")->AsDateTime, AFQ->FieldByName("R31A5")->AsDateTime);
      else if (AFlDef->CmpAV("uid", "32A8")) // �������, ����
         FValue = AgeStr(AFQ->FieldByName("R3088")->AsDateTime, AFQ->FieldByName("R32A5")->AsDateTime);
      else if (AFlDef->CmpAV("uid", "019D, 00CC, 00E4, 0136, 015A, 0102")) // �����
       {
        if (ACode.Length())
         {
#ifdef REG_USE_KLADR
          adresCode FAddrCode = ParseAddrStr(FCode);
          if (AFlDef->CmpAV("uid", "0136")) // ����� �������
           {
            if (!AFQ->FieldByName("R0100")->IsNull && AFQ->FieldByName("R0100")->AsString.Length())
             ParseHouseStruct(AFQ->FieldByName("R0100")->AsString, FAddrCode.House, FAddrCode.Vld, FAddrCode.Korp,
              FAddrCode.Build);
            if (!AFQ->FieldByName("R0033")->IsNull && AFQ->FieldByName("R0033")->AsString.Length())
             FAddrCode.Flat = AFQ->FieldByName("R0033")->AsString;
            FCode  = FAddrCode.AsString();
            FValue = AddrStrByCode(FCode);
           }
          else if (AFlDef->CmpAV("uid", "015A")) // ����� ����� ������� [not used]
           {
            // if (!AFQ->FieldByName("R0100")->IsNull && AFQ->FieldByName("R0100")->AsString.Length())
            // ParseHouseStruct(AFQ->FieldByName("R0100")->AsString, FAddrCode.House, FAddrCode.Vld, FAddrCode.Korp, FAddrCode.Build);
            // if (!AFQ->FieldByName("R0033")->IsNull && AFQ->FieldByName("R0033")->AsString.Length())
            // FAddrCode.Flat = AFQ->FieldByName("R0033")->AsString;
            FCode  = FAddrCode.AsString();
            FValue = AddrStrByCode(FCode);
           }
          else if (AFlDef->CmpAV("uid", "0102"))
           { // ������� �� �������
            FCode  = FAddrCode.AsString();
            FValue = AddrStrByCode(FCode) + ", " + AFQ->FieldByName("R00FC")->AsString;
           }
          else if (AFlDef->CmpAV("uid", "019D, 00CC, 00E4")) // ����� ������������� ����,���,������������� ���
           {
            FCode  = FAddrCode.AsString();
            FValue = AddrStrByCode(FCode);
           }
          else
           FValue = "����� �� �������";
#else
          FValue = FCode;
#endif
         }
        else
         FValue = "����� �� �������";
       }
      else if (AFlDef->CmpAV("uid", "0025")) // �����������
       {
        if (ACode.Length())
         {
#ifdef REG_USE_ORG
          orgCode FOrgCode;
          if (FCode.ToIntDef(0))
           FOrgCode.OrgID = FCode.ToIntDef(0);
          else
           FOrgCode = ParseOrgCodeStr(FCode);
          dsLogMessage("Code: >> "+FCode);
          if (!AFQ->FieldByName("R0026")->IsNull && AFQ->FieldByName("R0026")->AsString.Length())
           FOrgCode.L1Val = AFQ->FieldByName("R0026")->AsString;
          if (!AFQ->FieldByName("R0027")->IsNull && AFQ->FieldByName("R0027")->AsString.Length())
           FOrgCode.L2Val = AFQ->FieldByName("R0027")->AsString;
          if (!AFQ->FieldByName("R0028")->IsNull && AFQ->FieldByName("R0028")->AsString.Length())
           FOrgCode.L3Val = AFQ->FieldByName("R0028")->AsString;
//          if (!AFQ->FieldByName("R0111")->IsNull && AFQ->FieldByName("R0111")->AsString.Length())
//           FOrgCode.State = AFQ->FieldByName("R0111")->AsString;
          if (!AFQ->FieldByName("R0112")->IsNull && AFQ->FieldByName("R0112")->AsInteger)
           FOrgCode.Format = UIDStr(AFQ->FieldByName("R0112")->AsInteger);
          FOrgCode.Type = TOrgTypeValue::Base;
          quExecParam(FQ, false, "SELECT code from class_32F2 where r32F4 = :pOrgCode", "pOrgCode",
            IntToStr(FOrgCode.OrgID));
          if (FQ->RecordCount)
           {
            FOrgCode.Type = TOrgTypeValue::Main;
           }
          else
           {
            // <class name='�������������� �����������' uid='32F3' inlist='l'>
            quExecParam(FQ, false, "SELECT code from class_32F3 where r32F5 = :pOrgCode", "pOrgCode",
              IntToStr(FOrgCode.OrgID));
            if (FQ->RecordCount)
             {
              FOrgCode.Type = TOrgTypeValue::Control;
             }
           }
          FCode = FOrgCode.AsString();
          FValue = OrgStrByCode(FCode);
          if (AClsRC && FOrgCode.OrgID)
           {
            AddImportClsData(AClsRC, AFlDef->AV["ref"], IntToStr(FOrgCode.OrgID));
           }
#else
          FValue = FCode;
#endif
         }
        else
         FValue = "�������������";
       }
      else if (AFlDef->CmpAV("uid", "1118")) // ��������������� ����::����
       {
        if (ACode.Length())
         {
          if (!AFQ->FieldByName("R1116")->IsNull)
           {
            __int64 MIBP = FCode.ToIntDef(0);
            if (AFQ->FieldByName("R1116")->AsInteger == 1)
             { // R1116 = �������� - 1,  ����� - 2,  �������� - 3
              __int64 MIBPType = (int)(MIBP / 10000);
              MIBP -= 10000 * MIBPType;
             }
            FValue = GetMIBPName(AFQ->FieldByName("R1116")->AsInteger, IntToStr(MIBP));
           }
         }
        else
         FValue = "������";
       }
      else if (AFlDef->CmpAV("uid", "1089")) // ����::����
       {
        if (ACode.Length())
         {
          if (!AFQ->FieldByName("R1086")->IsNull)
           {
            FValue = GetMIBPName(AFQ->FieldByName("R1086")->AsInteger, FCode);
           }
         }
        else
         FValue = "������";
       }
      else if (AFlDef->CmpAV("uid", "32AA")) // ����::���� ����������
       {
        if (ACode.Length())
         {
          if (!AFQ->FieldByName("R1086")->IsNull)
           {
            FValue = GetMIBPName(AFQ->FieldByName("R1086")->AsInteger, FCode);
           }
         }
        else
         FValue = "������";
       }
      else if (AFlDef->CmpAV("uid", "3089")) // ����::����
       {
        if (ACode.Length())
         {
          if (!AFQ->FieldByName("R3086")->IsNull)
           {
            FValue = GetMIBPName(AFQ->FieldByName("R3086")->AsInteger, FCode);
           }
         }
        else
         FValue = "������";
       }
      else if (AFlDef->CmpAV("uid", "308B")) // ����::��� ����������
       {
        FValue = "������";
        if (ACode.Length())
         FValue = GetPlanVacType(FCode);
       }
      else if (AFlDef->CmpAV("uid", "3082")) // �����::�����������
       {
        if (ACode.Length())
         {
          if (ACode.Pos("##"))
           FValue = FXMLClassData->GetData("mkb", FCode);
          else
           FValue = ACode.Trim();
         }
        else
         FValue = "������";
       }
      else if (AFlDef->CmpAV("uid", "31A2")) // ��������������� ����::����� �����
       {
        if (ACode.Length())
         {
          if (!AFQ->FieldByName("R31A4")->IsNull)
           {
            TDate tmp = AFQ->FieldByName("R31A4")->AsDateTime;
            FValue = tmp.FormatString("{yy.mm} mmmm yyyy");
            FValue += " [ " + AgeStr(tmp, AFQ->FieldByName("R31A3")->AsDateTime) + " ]";
           }
         }
        else
         FValue = "";
       }
     }
    // ��������������� --------------------------------------------------------
    else if (FRegGUI.UpperCase() == "549F0EBE-1B9B98A7-BD64")
     {
      if (AFlDef->CmpAV("uid", "0007, 0015, 002E")) // ��������:�����, ���������:�����, ������:�����
       {
        if (FCode.Length())
         FValue = GetClassData(FCode, "0004");
        else
         FValue = "������";
       }
      else if (AFlDef->CmpAV("uid", "0017")) // ��������:������
       {
#ifdef REG_USE_DOC
        if (FCode.Length())
         FValue = GetDocPeriodValStr(FCode, AFQ->FieldByName("F_FROMDATE")->AsDateTime,
          AFQ->FieldByName("F_TODATE")->AsDateTime);
        else
         FValue = "������";
#endif
       }
      else if (AFlDef->CmpAV("uid", "0023")) // �����������, �����
       {
        if (ACode.Length())
         {
#ifdef REG_USE_KLADR
          adresCode FAddrCode = ParseAddrStr(FCode);
          FCode  = FAddrCode.AsString();
          FValue = AddrStrByCode(FCode);
          if (!FValue.Trim().Length())
           FValue = ACode;
#else
          FValue = ACode;
#endif
         }
        else
         FValue = "����� �� �������";
       }
     }
    // ������� --------------------------------------------------------
    else if (FRegGUI.UpperCase() == "57E3D087-D0CFEF0F-1A84")
     {
      if (AFlDef->CmpAV("uid", "00CC")) // �����
       {
        if (ACode.Length())
         {
#ifdef REG_USE_KLADR
          adresCode FAddrCode = ParseAddrStr(FCode);
          FCode  = FAddrCode.AsString();
          FValue = AddrStrByCode(FCode);
#else
          FValue = FCode;
#endif
         }
        else
         FValue = "����� �� �������";
       }
     }
    if (!FCode.Length() && FValue.Length())
     FCode = FValue;
    RC->AddPair(FCode, FValue); // "{\""+FCode+"\":\""+FValue+"\"}";
    AValue = FValue;
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC; // "{\""+FCode+"\":\""+FValue+"\"}";
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetExtEditSearchVal(TTagNode * AFlDef, UnicodeString ACode, TJSONObject * AValue,
  bool AImport)
 {
  UnicodeString RC = "";
  // RC->AddPair(ACode, ACode);
  // UnicodeString RC =  "{\""+ACode+"\":\""+ACode+"\"}";
  UnicodeString FCode = ACode.Trim();
  UnicodeString tmpVal;
  UnicodeString FValue = "";
  try
   {
    // ������������ -----------------------------------------------------------
    if (FRegGUI.UpperCase() == "40381E23-92155860-4448")
     {
      if (AFlDef->CmpAV("uid", "0136")) // �����
       {
        if (ACode.Length())
         {
#ifdef REG_USE_KLADR
          adresCode FAddrCode = ParseAddrStr(FCode);
          if (AFlDef->CmpAV("uid", "0136")) // ����� �������
           {
            tmpVal = GetJSONSearchData(AValue, "0100", AImport);
            if (tmpVal.Length())
             ParseHouseStruct(tmpVal, FAddrCode.House, FAddrCode.Vld, FAddrCode.Korp, FAddrCode.Build);
            tmpVal = GetJSONSearchData(AValue, "0033", AImport);
            if (tmpVal.Length())
             FAddrCode.Flat = tmpVal;
            FValue = AddrStrByCode(FAddrCode.AsString(), 0x17E);
           }
          else
           FValue = "����� �� �������";
#endif
         }
        else
         FValue = "����� �� �������";
       }
      else if (AFlDef->CmpAV("uid", "0025")) // �����������
       {
        if (ACode.Length())
         {
#ifdef REG_USE_ORG
          orgCode FOrgCode;
          if (FCode.ToIntDef(0))
           FOrgCode.OrgID = FCode.ToIntDef(0);
          else
           FOrgCode = ParseOrgCodeStr(FCode);
          tmpVal = GetJSONSearchData(AValue, "0026", AImport);
          if (tmpVal.Length())
           FOrgCode.L1Val = tmpVal;
          tmpVal = GetJSONSearchData(AValue, "0027", AImport);
          if (tmpVal.Length())
           FOrgCode.L2Val = tmpVal;
          tmpVal = GetJSONSearchData(AValue, "0028", AImport);
          if (tmpVal.Length())
           FOrgCode.L3Val = tmpVal;
//          tmpVal = GetJSONSearchData(AValue, "0111", AImport);
//          if (tmpVal.Length())
//           FOrgCode.State = tmpVal;
          tmpVal = GetJSONSearchData(AValue, "0112", AImport);
          if (tmpVal.Length())
           FOrgCode.Format = UIDStr(tmpVal.ToIntDef(0));
          FValue = OrgStrByCode(FOrgCode.AsString());
#endif
         }
        else
         FValue = "�������������";
       }
     }
    RC = FValue;
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::SetDay(int AYear, int AMonth, int ADay)
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString sm = IntToStr(AYear);
    sm = sm.SubString(sm.Length(), 1);
    if (((sm == "1") || (sm == "2") || (sm == "3") || (sm == "4")) && !((AYear < 15) && (AYear > 10)))
     sm = "�. ";
    else
     sm = "�. ";
    if (AYear)
     RC += IntToStr(AYear) + sm;
    if (AMonth)
     RC += IntToStr(AMonth) + "�. ";
    if (ADay)
     RC += IntToStr(ADay) + "�.";
    if (!(AYear + AMonth + ADay))
     RC += "0�.";
   }
  __finally
   {
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::AgeStr(TDateTime ACurDate, TDateTime ABirthDay)
 {
  UnicodeString RC = "";
  try
   {
    if (ACurDate < ABirthDay)
     {
      RC = "-";
     }
    else
     {
      Word FYear, FMonth, FDay;
      DateDiff(ACurDate, ABirthDay, FDay, FMonth, FYear);
      RC = SetDay(FYear, FMonth, FDay);
     }
   }
  __finally
   {
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::AgeStrY(TDateTime ACurDate, TDateTime ABirthDay)
 {
  UnicodeString RC = "";
  try
   {
    if (ACurDate < ABirthDay)
     {
      RC = "-";
     }
    else
     {
      float DDD = (int)ACurDate - ((int)ABirthDay) - 2;
      unsigned short YY = 0, MM = 0, DD = 0;
      (ACurDate - ABirthDay + 1).TDateTime::DecodeDate(& YY, & MM, & DD);
      MM-- ;
      RC = FloatToStrF((DDD / 365.25), ffFixed, 16, 2) + " " + " �.";
     }
   }
  __finally
   {
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::AddrStrByCode(UnicodeString ACode, unsigned int AParams)
 {
  UnicodeString RC = "������";
  try
   {
    if (FOnGetAddrStr)
     RC = FOnGetAddrStr(ACode, AParams);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::OrgStrByCode(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetOrgStr)
     RC = FOnGetOrgStr(ACode, 0);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsRegDM::GetWhereFromFilter(TTagNode * AFlt, UnicodeString AMainISUID)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  UnicodeString FCountSQL = "";
  bool FIsDist = false;
  bool FIsAddKey = false;
  TStringList * TabList = new TStringList;
  TdsDocSQLCreator * FSQLCreator = new TdsDocSQLCreator(FXMLList);
  FSQLCreator->OnGetSVal = FRegGetSVal;
  try
   {
    TTagNode * FDocCondRules;
    if (AFlt->CmpName("condrules"))
     FDocCondRules = AFlt;
    else
     FDocCondRules = AFlt->GetChildByName("condrules", true);
    if (FDocCondRules)
     {
      try
       {
        if (!FSQLCreator->CreateWhere(FDocCondRules, TabList, RC, AMainISUID))
         RC = "";
        // else
        // ATabList = FSQLCreator->CreateFrom(AMainISUID, TabList, &FIsDist,"",&FIsAddKey);
       }
      catch (Exception & E)
       {
        dsLogError("FSQLCreator->CreateWhere : " + E.Message + "; CondRules = " + FDocCondRules->AsXML, __FUNC__, 5);
       }
     }
    else
     dsLogMessage("������������ condrules", __FUNC__, 5);
    dsLogMessage(RC, __FUNC__, 5);
   }
  __finally
   {
    delete TabList;
    delete FSQLCreator;
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegDM::FRegGetSVal(UnicodeString AFuncName, TTagNode * ANode, UnicodeString & ATab,
  UnicodeString & ARet)
 {
#ifdef REG_USE_IMM_NOM
  dsLogMessage(AFuncName, __FUNC__, 5);
  if (FImmNom)
   {
    FImmNom->GetSVal(AFuncName, ANode, ATab, ARet);
    dsLogMessage(ARet, __FUNC__, 5);
   }
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegDM::CreateXMLs(UnicodeString ATabName)
 {
  /* quExec("Select * from "+ATabName+" Order by CODE");

   //  TTagNode *itNode = (TTagNode*)ANode;
   UnicodeString iCode,iExtCode,iLev,iName;
   iCode    = "CODE";
   iExtCode = "EXTCODE";
   iLev     = "LEV";
   iName    = "NAME";

   // ������������ ��������

   TTagNode *clRoot;
   TList *tmpNodes = new TList;
   int Level, tmpLevel;
   Level = 0;
   TTagNode *tmpNode1;
   TTagNode *tmpNode2;
   clRoot = new TTagNode;
   tmpNodes->Add(clRoot);
   int i = 1;
   __int64 subL = 0;
   __int64 sCode = -1;
   int sLev = 0;
   try
   {
   while (!quFree->Eof)
   {
   tmpLevel = quFree->FieldByName(iLev)->AsInteger;
   tmpLevel -= subL;
   if (tmpLevel > (Level+1))
   {
   sCode = (__int64)quFree->FieldByName(iCode)->AsInteger;
   sLev = tmpLevel;
   subL = tmpLevel - 1;
   tmpLevel -= subL;
   }
   if (tmpLevel < Level)
   {
   if ((sLev >= quFree->FieldByName(iLev)->AsInteger)||((sCode+1)!=(__int64)quFree->FieldByName(iCode)->AsInteger))
   {
   tmpLevel = 1;
   sLev = quFree->FieldByName(iLev)->AsInteger;
   subL = quFree->FieldByName(iLev)->AsInteger - 1;
   }
   }
   else
   {
   if ((sCode+1)!=(__int64)quFree->FieldByName(iCode)->AsInteger)
   {
   tmpLevel = 1;
   sLev = quFree->FieldByName(iLev)->AsInteger;
   subL = quFree->FieldByName(iLev)->AsInteger - 1;
   }
   }
   sCode = (__int64)quFree->FieldByName(iCode)->AsInteger;
   tmpNode2 = (TTagNode*)tmpNodes->Items[tmpLevel - 1];

   //        tmpNode1 = ClassTree->Items->AddChildObject(tmpNode2,tmpName,(void*)((__int64)quFree->FieldByName(iCode)->AsInteger));
   tmpNode1 = tmpNode2->AddChild("i");
   tmpNode1->AV["c"] = quFree->FieldByName(iCode)->AsString;
   tmpNode1->AV["e"] = quFree->FieldByName(iExtCode)->AsString;
   tmpNode1->AV["n"] = quFree->FieldByName(iName)->AsString;
   tmpNode1->AV["l"] = quFree->FieldByName(iLev)->AsString;
   tmpNode1->AV["i"] = quFree->FieldByName("is_id")->AsString;

   i++;
   if (Level != tmpLevel)
   {
   if (tmpLevel == tmpNodes->Count) tmpNodes->Add(tmpNode1);
   else tmpNodes->Items[tmpLevel] = tmpNode1;
   }
   else
   tmpNodes->Items[tmpLevel] = tmpNode1;
   Level = tmpLevel;
   quFree->Next();
   }
   clRoot->SaveToXMLFile(ATabName+".xml","");
   delete clRoot;
   }
   catch (Exception &E)
   {
   //     MessageBox(NULL,"","",MB_ICONSTOP);
   }
   if (tmpNodes) delete tmpNodes;
   tmpNodes = NULL; */
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetSchName(int AGUIType, UnicodeString ARef)
 {
  UnicodeString RC = "error";
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      UnicodeString FGUI = "";
      TTagNode * SchList, *SchLine;
      // UnicodeString xName,xTab;
      switch (AGUIType)
       {
       case 1:
         {
          FGUI = "12063611-00008cd7-cd89"; /* xName = "R0040"; xTab = "CLASS_0035"; */
          break;
         } // �������
       case 2:
         {
          FGUI = "001D3500-00005882-DC28"; /* xName = "R002C"; xTab = "CLASS_002A"; */
          break;
         } // �����
       case 3:
         {
          FGUI = "XXXXXXXX-XXXXXXXX-XXXX"; /* xName = "R013F"; xTab = "CLASS_013E"; */
          break;
         } // ��������
       }
      SchList = FXMLList->GetXML(FGUI);
      if (SchList)
       {
        SchLine = SchList->GetTagByUID(_UID(ARef));
        SchList = SchList->GetTagByUID(_GUI(ARef));
       }
      if (SchLine && SchList)
       {
        // quExec(FQ, "Select "+xName+" From "+xTab+" Where code="+SchList->AV["ref"]);
        // if (FQ->RecordCount)
        RC = SchLine->AV["name"] + "." + GetMIBPName(AGUIType, SchList->AV["ref"]) + " (������� " +
          SchList->AV["name"] + ")";
       }
     }
    catch (Exception & E)
     {
      RC = E.Message;
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetMIBPName(int AGUIType, UnicodeString ACode)
 {
  UnicodeString RC = "error";
  try
   {
    try
     {
      switch (AGUIType)
       {
       case 1:
         {
          RC = GetClassData(ACode, "0035"); /* xName = "R0040"; xTab = "CLASS_0035"; */
          break;
         } // �������
       case 2:
         {
          RC = GetClassData(ACode, "002A"); /* xName = "R002C"; xTab = "CLASS_002A"; */
          break;
         } // �����
       case 3:
         {
          RC = GetClassData(ACode, "013E"); /* xName = "R013F"; xTab = "CLASS_013E"; */
          break;
         } // ��������
       }
     }
    catch (Exception & E)
     {
      RC = E.Message;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetPlanVacType(UnicodeString AVType)
 {
  UnicodeString RC = AVType;
  UnicodeString LastVacType, CurVacType, FullVType;
  UnicodeString VacTypeList = AVType;
  try
   {
    LastVacType = "";
    FullVType   = "";
    while (VacTypeList.Length())
     {
      CurVacType = GetLPartB(VacTypeList, ';'); // inf.vac_type
      if (!LastVacType.Length())
       LastVacType = GetRPartB(CurVacType, '.').UpperCase();
      else if (LastVacType != GetRPartB(CurVacType, '.').UpperCase())
       LastVacType = "-1";
      FullVType += GetClassData(GetLPartB(CurVacType, '.'), "003A") + "." + GetRPartB(CurVacType, '.') + "; ";
      // ��������_srt.���_����������;
      VacTypeList = GetRPartB(VacTypeList, ';').Trim();
     }
    if (LastVacType.Length() && (LastVacType != "-1"))
     RC = LastVacType;
    else
     RC = FullVType.Trim();
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::FOnGetDocExtSQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString FId = AId.UpperCase();
    if (FId == "0001") // <class name='������ �������� ����������' uid='0001' tab='SPECIFIKATORS'>
     {
      RC = "Select NAME as STR_CLASS From SPECIFIKATORS Where Upper(gui)=Upper(:CL_CODE)";
     }
    else if (FId == "0002") // <class name='������ ����������' uid='0002' tab='DOCUMENTS'>
     {
      RC = "Select NAME as STR_CLASS From DOCUMENTS Where Upper(gui)=Upper(:CL_CODE)";
     }
    else if (FId == "0003") // <class name='������ ��������' uid='0003' tab='FILTERS'>
     {
      RC = "Select NAME as STR_CLASS From FILTERS Where Upper(gui)=Upper(:CL_CODE)";
     }
    else if (FId == "0004") // <class name='������ �����������' uid='0004' tab='SUBORDORG'>
     {
      RC = "Select NAME as STR_CLASS From SUBORDORG Where Upper(CODE)=Upper(:CL_CODE)";
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegDM::UpdateSearchData(System::UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString ARecId, bool AImport)
 {
  TAnsiStrMap FSearchField = TAnsiStrMap();
  FSearchField["0023"] = "1";
  FSearchField["0093"] = "1";
  FSearchField["00BF"] = "1";
  FSearchField["0136"] = "1";
  FSearchField["0025"] = "1";
  dsLogBegin(__FUNC__, 6);
  if (AValue && ARecId.Length())
   {
    TReplaceFlags FRepFl;
    FRepFl << rfReplaceAll;
    TFDQuery * FQ = CreateTempQuery();
    bool commit = false;
    bool FirstQ = true;
    try
     {
      try
       {
        TJSONObject * FRecValue = (TJSONObject *)GetJSONValue(AValue, "pt");
        if (!FRecValue)
         FRecValue = AValue;
        UnicodeString FVal;
        TJSONObject * FValue;
        TTagNode * FFlDefNode;
        UnicodeString FFam = GetJSONSearchData(FRecValue /* AValue */ , "001F", AImport).Trim();
        FFam += UnicodeString::StringOfChar(' ', 30 - FFam.Length());
        UnicodeString FName = GetJSONSearchData(FRecValue /* AValue */ , "0020", AImport).Trim();
        FName += UnicodeString::StringOfChar(' ', 20 - FName.Length());
        UnicodeString FSName = GetJSONSearchData(FRecValue /* AValue */ , "002F", AImport).Trim();
        FSName += UnicodeString::StringOfChar(' ', 30 - FSName.Length());
        UnicodeString StrIdx = FFam + FName + FSName;
        for (TAnsiStrMap::iterator iFl = FSearchField.begin(); iFl != FSearchField.end(); iFl++)
         {
          FFlDefNode = FRegDef->GetTagByUID(iFl->first);
          FValue     = (TJSONObject *)GetJSONValue(FRecValue, iFl->first);
          if (FValue)
           {
            if (!FValue->Null && FValue->Count)
             {
              if (FValue->ClassType()->ClassNameIs("TJSONObject"))
               {
                FVal = ((TJSONString *)FValue->Pairs[0]->JsonValue)->Value().Trim();
               }
              else
               {
                FVal = ((TJSONString *)FValue)->Value().Trim();
               }
              if (AImport)
               {
                if (FFlDefNode->CmpName("binary,choice,choiceDB,choiceTree,extedit"))
                 {
                  if (FValue->ClassType()->ClassNameIs("TJSONObject"))
                   FVal = ((TJSONString *)FValue->Pairs[0]->JsonString)->Value();
                 }
               }
              if (FVal.Length())
               {
                if (FFlDefNode->CmpName("extedit"))
                 FVal = GetExtEditSearchVal(FFlDefNode, FVal, FRecValue, AImport);
                else if (FFlDefNode->CmpName("binary"))
                 FVal = FVal;
                else if (FFlDefNode->CmpName("choice"))
                 {
                  if (FFlDefNode->GetChildByAV("choicevalue", "value", FVal))
                   FVal = FFlDefNode->GetChildByAV("choicevalue", "value", FVal)->AV["name"];
                 }
                else if (FFlDefNode->CmpName("choiceDB"))
                 {
                  if (FVal.ToIntDef(0))
                   FVal = GetClassData(FVal, FFlDefNode->AV["ref"]);
                  else
                   FVal = "";
                 }
                else
                 FVal = "";
                if (FVal.Length())
                 {
                  if (FFlDefNode->CmpAV("uid", "0136"))
                   {
                    TStringList * WR = new TStringList;
                    try
                     {
                      FVal     = StringReplace(FVal.UpperCase(), ",", " ", FRepFl);
                      WR->Text = StringReplace(FVal, " ", "\n", FRepFl);
                      for (int i = 0; i < WR->Count; i++)
                       {
                        if (WR->Strings[i].Trim().Length())
                         {
                          if (FirstQ)
                           commit =
                             !quExecParam(FQ, false,
                            "update or insert into search_addr_data (SEARCH_ID, CODE, STRVAL, FLNAME, SORTIDX) values (1000, :pCODE, :pval, :pfl, :pStrIdx) matching(SEARCH_ID, CODE, FLNAME)",
                            "pCODE", ARecId, "", "pval", WR->Strings[i], "pfl",
                            FFlDefNode->AV["uid"].UpperCase() + "_" + IntToStr(i), "pStrIdx", StrIdx).Length();
                          else
                           commit &=
                             !quExecParam(FQ, false,
                            "update or insert into search_addr_data (SEARCH_ID, CODE, STRVAL, FLNAME, SORTIDX) values (1000, :pCODE, :pval, :pfl, :pStrIdx) matching(SEARCH_ID, CODE, FLNAME)",
                            "pCODE", ARecId, "", "pval", WR->Strings[i], "pfl",
                            FFlDefNode->AV["uid"].UpperCase() + "_" + IntToStr(i), "pStrIdx", StrIdx).Length();
                         }
                        FirstQ = false;
                       }
                     }
                    __finally
                     {
                      delete WR;
                     }
                   }
                  else if (FFlDefNode->CmpAV("uid", "0025"))
                   {
                    TStringList * WR = new TStringList;
                    try
                     {
                      WR->Text = StringReplace(FVal.UpperCase(), " ", "\n", FRepFl);
                      for (int i = 0; i < WR->Count; i++)
                       {
                        if (WR->Strings[i].Trim().Length())
                         {
                          if (FirstQ)
                           commit =
                             !quExecParam(FQ, false,
                            "update or insert into search_org_data (SEARCH_ID, CODE, STRVAL, FLNAME, SORTIDX) values (1000, :pCODE, :pval, :pfl, :pStrIdx) matching(SEARCH_ID, CODE, FLNAME)",
                            "pCODE", ARecId, "", "pval", WR->Strings[i], "pfl",
                            FFlDefNode->AV["uid"].UpperCase() + "_" + IntToStr(i), "pStrIdx", StrIdx).Length();
                          else
                           commit &=
                             !quExecParam(FQ, false,
                            "update or insert into search_org_data (SEARCH_ID, CODE, STRVAL, FLNAME, SORTIDX) values (1000, :pCODE, :pval, :pfl, :pStrIdx) matching(SEARCH_ID, CODE, FLNAME)",
                            "pCODE", ARecId, "", "pval", WR->Strings[i], "pfl",
                            FFlDefNode->AV["uid"].UpperCase() + "_" + IntToStr(i), "pStrIdx", StrIdx).Length();
                          FirstQ = false;
                         }
                       }
                     }
                    __finally
                     {
                      delete WR;
                     }
                   }
                  else
                   {
                    if (FirstQ)
                     commit =
                       !quExecParam(FQ, false,
                      "update or insert into search_data (SEARCH_ID, CODE, STRVAL, FLNAME, SORTIDX) values (1000, :pCODE, :pval, :pfl, :pStrIdx) matching(SEARCH_ID, CODE, FLNAME)",
                      "pCODE", ARecId, "", "pval", FVal.UpperCase(), "pfl", FFlDefNode->AV["uid"].UpperCase(),
                      "pStrIdx", StrIdx).Length();
                    else
                     commit &=
                       !quExecParam(FQ, false,
                      "update or insert into search_data (SEARCH_ID, CODE, STRVAL, FLNAME, SORTIDX) values (1000, :pCODE, :pval, :pfl, :pStrIdx) matching(SEARCH_ID, CODE, FLNAME)",
                      "pCODE", ARecId, "", "pval", FVal.UpperCase(), "pfl", FFlDefNode->AV["uid"].UpperCase(),
                      "pStrIdx", StrIdx).Length();
                    FirstQ = false;
                   }
                 }
               }
             }
           }
          // *************************
         }
       }
      catch (Exception & E)
       {
        dsLogError(E.Message, __FUNC__);
        throw;
       }
      catch (...)
       {
        dsLogError("��", __FUNC__);
        throw;
       }
     }
    __finally
     {
      if (commit)
       {
        FQ->Transaction->Commit();
       }
      DeleteTempQuery(FQ);
     }
   }
  dsLogEnd(__FUNC__, 6);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegDM::GetJSONSearchData(TJSONObject * AValue, const UnicodeString AId, bool AImport)
 {
  dsLogBegin(__FUNC__, 6);
  UnicodeString RC = "";
  try
   {
    if (AValue)
     {
      UnicodeString StrVal;
      TJSONObject * FValue = (TJSONObject *)GetJSONValue(AValue, AId);
      TTagNode * FFlDefNode = FRegDef->GetTagByUID(AId);
      if (FValue && FFlDefNode)
       {
        if (!FValue->Null)
         {
          StrVal = ((TJSONString *)FValue)->Value();
          if (AImport)
           {
            if (FFlDefNode->CmpName("binary,choice,choiceDB,choiceTree,extedit"))
             {
              if (FValue->Count)
               RC = ((TJSONString *)FValue->Pairs[0]->JsonString)->Value().Trim();
             }
            else
             RC = StrVal.Trim();
           }
          else
           RC = StrVal;
         }
       }
     }
   }
  __finally
   {
    dsLogEnd(__FUNC__, 6);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegDM::PatGrChData1(System::UnicodeString ARecId, TJSONObject * AValue)
 {
  dsLogBegin(__FUNC__, 6);
  bool RC = false;
  if (AValue && ARecId.Length())
   {
    TFDQuery * FQ = CreateTempQuery();
    try
     {
      Sleep(1000);
      // {"Value1":{"0021":"1","0088":"1","00B3":"74","00C4":"1","00C5":"28.04.2017","00F1":"3","32B3":"3"},"Value2":{}}
      TJSONObject * FRecValue = (TJSONObject *)GetJSONValue(AValue, "Value1");
      if (FRecValue)
       {
        UnicodeString FSQL = "";
        try
         {
          UnicodeString FId, FValue;
          TJSONPairEnumerator * itPair = FRecValue->GetEnumerator();
          while (itPair->MoveNext())
           {
            FId = JSONEnumPairCode(itPair).UpperCase();
            if (FSQL.Length())
             FSQL += ", ";
            FSQL += " r" + FId + "=:pr" + FId;
            if (FId == "0136") // �����
             {
              FSQL += ", R0100=:pR0100";
              FSQL += ", R0033=:pR0033";
             }
            else if (FId == "0025") // �����������
             {
              FSQL += ", R0026=:pR0026";
              FSQL += ", R0027=:pR0027";
              FSQL += ", R0028=:pR0028";
             }
           }
          FSQL += " Where code=:pCode";
          FSQL = "Update class_1000 set " + FSQL;
          FQ->Close();
          if (!FQ->Transaction->Active)
           FQ->Transaction->StartTransaction();
          FQ->SQL->Text = FSQL;
          FQ->Prepare();
          itPair = FRecValue->GetEnumerator();
          while (itPair->MoveNext())
           {
            FId    = JSONEnumPairCode(itPair).UpperCase();
            FValue = JSONEnumPairValue(itPair);
            if (FId == "0136") // �����
             {
              adresCode FAddrCode = ParseAddrStr(FValue);
              FQ->ParamByName("pr" + FId)->Value = FAddrCode.AsString();
              FQ->ParamByName("pR0100")->Value = FAddrCode.HouseCodeStr();
              FQ->ParamByName("pR0033")->Value = FAddrCode.Flat;
             }
            else if (FId == "0025") // �����������
             {
              orgCode FOrgCode = ParseOrgCodeStr(FValue);
              FQ->ParamByName("pr" + FId)->Value = FOrgCode.OrgID;
              if (FOrgCode.L1Val.Length())
               FQ->ParamByName("pR0026")->Value = FOrgCode.L1Val;
              if (FOrgCode.L2Val.Length())
               FQ->ParamByName("pR0027")->Value = FOrgCode.L2Val;
              if (FOrgCode.L3Val.Length())
               FQ->ParamByName("pR0028")->Value = FOrgCode.L3Val;
             }
            else
             FQ->ParamByName("pr" + FId)->Value = FValue;
           }
          FQ->ParamByName("pCode")->Value = ARecId;
          FQ->OpenOrExecute();
          FQ->Transaction->Commit();
         }
        catch (System::Sysutils::Exception & E)
         {
          dsLogError(E.Message + "; SQL = " + FSQL + "; Data: " + AValue->ToString(), __FUNC__);
         }
       }
     }
    __finally
     {
      DeleteTempQuery(FQ);
     }
   }
  dsLogEnd(__FUNC__, 6);
  return RC;
 }
// ---------------------------------------------------------------------------
