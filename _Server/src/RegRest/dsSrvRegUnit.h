//---------------------------------------------------------------------------

#ifndef dsSrvRegUnitH
#define dsSrvRegUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
#include "dsRegDMUnit.h"
#include "dsRegCardEditors.h"
#include "DataExchange.h"
//#include "diDataTypeDef.h"
//---------------------------------------------------------------------------
typedef enum {cdtGet, cdtSet} TdsRegTextDataConvType;
typedef UnicodeString  (__closure *TdsRegTextDataConvEvent)(TdsRegTextDataConvType AConvType, UnicodeString ATabId, UnicodeString AFlId, UnicodeString AData);
typedef UnicodeString  __fastcall(__closure *TdsRegUnitDataChangedEvent)(__int64 ACode);
//---------------------------------------------------------------------------
class PACKAGE TdsSrvRegistry : public TComponent
{
__published:
private:
  TDataImporter *FDataImporter;
  TdsRegCardEditors *FCardEditors;
  TdsRegDM      *FDM;

  TdsRegUnitDataChangedEvent FOnUnitDataChanged;

  TdsDIOnExtGetCodeEvent __fastcall FGetOnExtGetCode();
  void __fastcall FSetOnExtGetCode(TdsDIOnExtGetCodeEvent AVal);

  TdsRegGetStrValByCodeEvent __fastcall FGetOnGetAddrStr();
  void __fastcall FSetOnGetAddrStr(TdsRegGetStrValByCodeEvent AVal);

  TdsRegGetStrValByCodeEvent __fastcall FGetOnGetOrgStr();
  void __fastcall FSetOnGetOrgStr(TdsRegGetStrValByCodeEvent AVal);

  TdsRegGetStrValByCodeEvent  __fastcall FGetOnGetFilterData();
  void __fastcall FSetOnGetFilterData(TdsRegGetStrValByCodeEvent  AVal);

  TdsKLADRClass* __fastcall FGetKLADR();
  void __fastcall FSetKLADR(TdsKLADRClass *AValue);

  TdsRegTextDataConvEvent FDataConv;

  void __fastcall UnitDataChanged(__int64 ACode);

  TdsRegCardEditedEvent __fastcall FGetOnEdited();
  void __fastcall FSetOnEdited(TdsRegCardEditedEvent AValue);

public:
  __fastcall TdsSrvRegistry(TComponent* Owner, TFDConnection *AConnection, TAxeXMLContainer *AXMLList, const UnicodeString ARegGUI, const UnicodeString AImpGUI, bool APreload = true, UnicodeString AUser = "", UnicodeString APass = "");
  __fastcall ~TdsSrvRegistry();
  void __fastcall SetRegDef(TTagNode *AVal);

  __int64                __fastcall GetCount(System::UnicodeString AId, System::UnicodeString AFilterParam);
  TJSONObject*           __fastcall GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam);
  TJSONObject*           __fastcall GetImportValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject * ACls);
  TJSONObject*           __fastcall GetValById(System::UnicodeString AId, System::UnicodeString ARecId);
  TJSONObject*           __fastcall GetValById10(System::UnicodeString AId, System::UnicodeString ARecId);
  TJSONObject*           __fastcall Find(System::UnicodeString AId, UnicodeString AFindData);

  System::UnicodeString  __fastcall ImportData(System::UnicodeString AId, TJSONObject *AValue, UnicodeString &ARecId, UnicodeString & AResMessage);

  System::UnicodeString  __fastcall InsertData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString  __fastcall EditData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString  __fastcall DeleteData(System::UnicodeString AId, System::UnicodeString ARecId);
  System::UnicodeString  __fastcall SetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString AVal);
  System::UnicodeString  __fastcall GetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, UnicodeString &AMsg);

  System::UnicodeString __fastcall ImportCardData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString __fastcall InsertCardData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString __fastcall EditCardData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString __fastcall DeleteCardData(System::UnicodeString AId, System::UnicodeString ARecId);
  UnicodeString         __fastcall GetPrivVar(__int64 APrivCode);
  UnicodeString         __fastcall GetOtvodInf(__int64 AOtvodCode);
  UnicodeString         __fastcall GetMIBPName(int AGUIType, UnicodeString  ACode);
  bool                  __fastcall PatGrChData1(System::UnicodeString  ARecId, TJSONObject * AValue);

  __property TdsDIOnExtGetCodeEvent OnExtGetCode = {read = FGetOnExtGetCode, write = FSetOnExtGetCode};
  __property TdsRegTextDataConvEvent OnDataConv = {read = FDataConv, write=FDataConv};
  __property TdsRegGetStrValByCodeEvent  OnGetAddrStr = {read = FGetOnGetAddrStr, write=FSetOnGetAddrStr};
  __property TdsRegGetStrValByCodeEvent  OnGetOrgStr = {read = FGetOnGetOrgStr, write=FSetOnGetOrgStr};
  __property TdsRegUnitDataChangedEvent OnUnitDataChanged = {read = FOnUnitDataChanged, write=FOnUnitDataChanged};
  __property TdsRegGetStrValByCodeEvent  OnGetFilterData = {read = FGetOnGetFilterData, write=FSetOnGetFilterData};
  __property TdsKLADRClass*  KLADR = {read = FGetKLADR, write = FSetKLADR};
  __property TdsRegCardEditedEvent OnCardEdited = {read=FGetOnEdited, write=FSetOnEdited};
};
//---------------------------------------------------------------------------
#endif
