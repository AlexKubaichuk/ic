//---------------------------------------------------------------------------

#ifndef dsRegCardEditorsH
#define dsRegCardEditorsH
//---------------------------------------------------------------------------
#include "dsRegDMUnit.h"
//#include "dsCommClassData.h"
//---------------------------------------------------------------------------
typedef void  (__closure *TdsRegCardEditedEvent)(__int64 AUCode, int AType, int AInfCode);
//---------------------------------------------------------------------------
typedef enum {citImport, citInsert, citEdit} TCardImportType;
typedef struct tagCheckCardItem {

  __int64       Id;      // ���
  __int64       MIBP;    // ��� ����
  __int64       PlanId;  // ��� �����, ���� ����
  int           Inf;     // ��� ��������
  UnicodeString VacType; // V / RV
  TDate         Date;    // ���� ����������/�����
  __int64       ExecMIBP; // ��� ���� ����������
  TDate         ExecDate; // ���� ���������� � �����
  int           InThisLPU; // ��������� � ������ ���
  int           NoMIBP; // ����������� ����
  int           NoExecType; // ������� ������������

  tagCheckCardItem() : Id(0),
                  MIBP(0),
                  PlanId(0),
                  Inf(0),
                  VacType(""),
                  Date(0),
                  InThisLPU(0),
                  NoMIBP(0),
                  NoExecType(1),
                  ExecMIBP(0),
                  ExecDate(0){};
  tagCheckCardItem(TFDQuery *AFQ, int AType)
  { // AType 1 - priv, 2 - privbyinf, 3 - plan, 4 - planbyinf, 5 - test;
    tagCheckCardItem();
    switch (AType)
    {
      case 1:
      {// AType 1 - priv
        Id      = AFQ->FieldByName("code")->AsInteger;        // ���
        MIBP    = AFQ->FieldByName("R3020")->AsInteger;       // ��� ����
        PlanId  = AFQ->FieldByName("R32AD")->AsInteger;       // ��� �����, ���� ����
        Inf     = 0;                                          // ��� ��������
        VacType = AFQ->FieldByName("R3021")->AsString.Trim(); // V / RV
        Date    = AFQ->FieldByName("R3024")->AsDateTime;      // ���� ����������
        InThisLPU  = AFQ->FieldByName("R301F")->AsInteger;     // ��������� � ������ ���
        NoMIBP     = 0;     // ����������� ����
        NoExecType = 1;     // ������� ������������
        break;
      }
      case 2:
      {// 2 - privbyinf
        Id      = AFQ->FieldByName("code")->AsInteger;        // ���
        MIBP    = AFQ->FieldByName("R1020")->AsInteger;       // ��� ����
        PlanId  = AFQ->FieldByName("R32AE")->AsInteger;       // ��� �����, ���� ����
        Inf     = AFQ->FieldByName("R1075")->AsInteger;       // ��� ��������
        VacType = AFQ->FieldByName("R1021")->AsString.Trim(); // V / RV
        Date    = AFQ->FieldByName("R1024")->AsDateTime;      // ���� ����������
        InThisLPU = AFQ->FieldByName("R101F")->AsInteger;   // ��������� � ������ ���
        NoMIBP     = 0;     // ����������� ����
        NoExecType = 1;     // ������� ������������
        break;
      }
      case 3:
      {//3 - plan
        Id         = AFQ->FieldByName("code")->AsInteger;        // ���
        MIBP       = AFQ->FieldByName("R3089")->AsInteger;       // ��� ����
        PlanId     = AFQ->FieldByName("R328F")->AsInteger;       // ��� �����, ���� ����
        Inf        = AFQ->FieldByName("R32AB")->AsInteger;       // ��� ��������
        VacType    = AFQ->FieldByName("R308B")->AsString.Trim(); // V / RV
        Date       = AFQ->FieldByName("R3088")->AsDateTime;      // ���� �����
        InThisLPU  = AFQ->FieldByName("R32FD")->AsInteger;   // ��������� � ������ ���
        NoMIBP     = AFQ->FieldByName("R3308")->AsInteger;     // ����������� ����
        NoExecType = AFQ->FieldByName("R32FC")->AsInteger;     // ������� ������������
        break;
      }
      case 4:
      {// 4 - planbyinf
        Id         = AFQ->FieldByName("code")->AsInteger;        // ���
        MIBP       = AFQ->FieldByName("R1089")->AsInteger;       // ��� ����
        PlanId     = AFQ->FieldByName("R318F")->AsInteger;       // ��� �����, ���� ����
        Inf        = AFQ->FieldByName("R32A6")->AsInteger;       // ��� ��������
        VacType    = AFQ->FieldByName("R108B")->AsString.Trim(); // V / RV
        Date       = AFQ->FieldByName("R1088")->AsDateTime;      // ���� �����
        InThisLPU  = AFQ->FieldByName("R3302")->AsInteger;       // ��������� � ������ ���
        NoMIBP     = AFQ->FieldByName("R3303")->AsInteger;       // ����������� ����
        NoExecType = AFQ->FieldByName("R3307")->AsInteger;       // ������� ������������
        break;
      }
      case 5:
      {//  5 - test;
        Id      = AFQ->FieldByName("code")->AsInteger;        // ���
        MIBP    = AFQ->FieldByName("R1032")->AsInteger;       // ��� ����
        PlanId  = AFQ->FieldByName("R32AF")->AsInteger;       // ��� �����, ���� ����
        Inf     = AFQ->FieldByName("R1200")->AsInteger;       // ��� ��������
//        VacType = AFQ->FieldByName("R108B")->AsString.Trim(); // V / RV
        Date    = AFQ->FieldByName("R1031")->AsDateTime;      // ���� �����
        InThisLPU = AFQ->FieldByName("R10A5")->AsInteger;   // ��������� � ������ ���
        NoMIBP     = 0;     // ����������� ����
        NoExecType = 1;     // ������� ������������
        break;
      }
    }
  };
  UnicodeString ToString()
  {
    return
    "Id:"+IntToStr(Id)+
    "; MIBP:"+IntToStr(MIBP)+
    "; PlanId:"+IntToStr(PlanId)+
    "; Inf:"+IntToStr(Inf)+
    "; VacType:"+VacType+
    "; Date:"+DateToStr(Date)+
    "; InThisLPU:"+IntToStr(InThisLPU)+
    "; NoMIBP:"+IntToStr(NoMIBP)+
    "; NoExecType:"+IntToStr(NoExecType)+
    "; ExecMIBP:"+IntToStr(ExecMIBP)+
    "; ExecDate:"+DateToStr(ExecDate);
  };
} TCheckCardItem;
//---------------------------------------------------------------------------
typedef map<int, TCheckCardItem> TCheckCardItemMap;
//---------------------------------------------------------------------------
class PACKAGE TdsRegCardEditors : public TObject
{
private:
  TdsRegDM      *FDM;
  TList         *FSchList;
  TTagNode      *VacSch;
  TTagNode      *TestSch;
  TdsRegCardEditedEvent FOnEdited;

//  TdsCommClassData  *FCommClsData;

  System::UnicodeString __fastcall ImportVac(TCardImportType AImpType, TJSONObject *AValue, System::UnicodeString &ARecId, __int64 &AUnitCode);
  System::UnicodeString __fastcall DeleteVac(System::UnicodeString ARecId, __int64 &AUnitCode);

//  bool                  __fastcall CheckVacInPlan(__int64 AUCode, TDate AVacDate, __int64 APlanCode);
  bool                  __fastcall CheckVacInPlan(__int64 AUCode, TDate AVacDate, bool AFromOtvod);

  System::UnicodeString __fastcall ImportTest(TCardImportType AImpType, TJSONObject *AValue, System::UnicodeString &ARecId, __int64 &AUnitCode);
  System::UnicodeString __fastcall DeleteTest(System::UnicodeString ARecId, __int64 &AUnitCode);
  bool                  __fastcall CheckTestInPlan(__int64 AUCode, TDate ATestDate, __int64 APlanCode, bool AFromOtvod = false);

  System::UnicodeString __fastcall ImportCheck(TCardImportType AImpType, TJSONObject *AValue, System::UnicodeString &ARecId, __int64 &AUnitCode);
  System::UnicodeString __fastcall DeleteCheck(System::UnicodeString ARecId, __int64 &AUnitCode);
  System::UnicodeString __fastcall CheckCheckInPlan();

  System::UnicodeString __fastcall ImportOtvod(TCardImportType AImpType, TJSONObject *AValue, System::UnicodeString &ARecId, __int64 &AUnitCode);
  System::UnicodeString __fastcall DeleteOtvod(System::UnicodeString ARecId, __int64 &AUnitCode);
//  bool                  __fastcall CheckOtvodInPlan(__int64 AUCode, TDate AVacDate, __int64 APlanCode);

  __int64 __fastcall GetNewCode(UnicodeString TabId);
  UnicodeString __fastcall NewGUID();

  void __fastcall CheckVacSchCorrect(UnicodeString &AVac, UnicodeString &ASch, UnicodeString &AVacType);
  bool __fastcall VacSchCorrect(UnicodeString AVac, UnicodeString ASch, UnicodeString AVacType);
  UnicodeString __fastcall GetNewSchByType(UnicodeString AMIBP, UnicodeString AVacType);
  bool __fastcall FGetSchList(TTagNode *ANode, UnicodeString &AMIBPCode);

public:
  __fastcall TdsRegCardEditors(TdsRegDM* ADM);
  __fastcall ~TdsRegCardEditors();

  System::UnicodeString __fastcall ImportData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId, __int64 &AUnitCode);
  System::UnicodeString __fastcall InsertData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId, __int64 &AUnitCode);
  System::UnicodeString __fastcall EditData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId, __int64 &AUnitCode);
  System::UnicodeString __fastcall DeleteData(System::UnicodeString AId, System::UnicodeString ARecId, __int64 &AUnitCode);
  UnicodeString __fastcall GetPrivVar(__int64 APrivCode);
  UnicodeString __fastcall GetOtvodInf(__int64 AOtvodCode);
  __property TdsRegCardEditedEvent OnEdited = {read=FOnEdited, write=FOnEdited};
};
//---------------------------------------------------------------------------
#endif
