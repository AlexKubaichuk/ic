//<choicevalue name='����������' value='0'/>
//<choicevalue name='������ ����������' value='1'/>
//<choicevalue name='����������� �� ���������' value='2'/>
//<choicevalue name='����������� �� �� ���������' value='3'/>
//<choicevalue name='����������� ��� ���������������' value='4'/>
//<choicevalue name='����������� �� ����' value='5'/>
//<choicevalue name='����������� �� ���� ��� ���������������' value='6'/>
//---------------------------------------------------------------------------
/*
 ����        - Planer.h
 ������      - ���������� ������������.
 ��������� "���������� ������������"
 ��������    - �����������.
 ������������ ����
 ����������� - ������� �.�.
 ����        - 09.05.2004
 */
//---------------------------------------------------------------------------
/*
 ������� ���������:

 2016
 �������������� �������
 */
//---------------------------------------------------------------------------

#ifndef PlanLinkerH
#define PlanLinkerH

#include "DKCfg.h"      // ��������� �������� ����������

#include <System.hpp>
#include <Classes.hpp>
#include <DateUtils.hpp>
//#include "pFIBDatabase.hpp"
#include "DKClasses.h"
#include "XMLContainer.h"
#include "ICSDATEUTIL.hpp"
#include "DKUtils.h"

#include "PlanLinkerDMUnit.h"
//#include "PlanerDebug.h"

//---------------------------------------------------------------------------
typedef void __fastcall(__closure * TdsSyncMethod)(void);
//---------------------------------------------------------------------------


//###########################################################################
//##                                                                       ##
//##                           EPlanLinkerError                             ##
//##                                                                       ##
//###########################################################################

//��������� ��� ������������ ������ � ������� TPlanLinker
class EPlanLinkerError : public DKClasses::EMethodError
 {
  typedef DKClasses::EMethodError inherited;

 public:
#pragma warn -inl
  inline __fastcall EPlanLinkerError(
      const UnicodeString MethodName,
      const UnicodeString Description
      ) : DKClasses::EMethodError(MethodName, Description)
   {
   }
  inline __fastcall virtual ~EPlanLinkerError(void)
   {
   }
#pragma warn .inl
 };
//###########################################################################
//##                                                                       ##
//##                              TPlanLinker                               ##
//##                                                                       ##
//###########################################################################

class TPlanerDM;

//���� ������������
enum class TPlanLinkerStage
 {
  Initialization,  //�������������
      PlanDop,     //������������ ����� ��� �������������� �������� ��� �����
      UpdatePlans, //���������� ������ ( �������� ����.����������� � ��������
  //���������, ���� ������������ �������������� �� ������ ����� )
      PlanPriviv,                //���� �� ��������� ��� ������������ ��������
      PlanProb,                  //���� �� ��������� ��� ������������ �����
      AppendDopPrivivByPrevProb, //��������� � ���� �������������� �������� �� ���������� ����� ����������� ����
      UnionPriviv,               //����������� ����������� ������ ��������� ��������, ������������ ����� � ��� �� ����
      CorrectPolyVak,            //������������� ��������, ����������� �������������
      UnionPrivivByPriority,     //����������� �� ���� �������� ������������ ���� � �������� �� ����� ��������,
  //����������� ���� �������������� � ����� ������������� ����
      AppendPrepareProb,     //��������� � ���� ���������� (��������������� ����)
      CorrectPrivivPlanDate, //������������� ��� ������������ ���������� �������� � ������������ � �������
  //����� ������� � ����������
      ApplyPlan//��������� ��������������� ����� � ��
     };

typedef void __fastcall(__closure * TOnPlanLinkerStageChange)(TObject * Sender, TPlanLinkerStage NewPlanerStage);
//---------------------------------------------------------------------------
class PACKAGE TPlanLinker : public TObject
 {
 private:
  //�������� �� ��������
  typedef list<__int64>   TPlanedUnitList;
  typedef list<TTagNode *>TTagNodeList;
  //�������

  //��� ����������� �������������

  TDate           FDatePB;    //���� ������ ������� ������������
  TDate           FDatePE;    //���� ��������� ������� ������������
  TDate           FPlanMonth; //1-� ����� ������ ������������
  TPlanedUnitList FPlUnitList;
  TIntStrMap         FDopPlUnitList;
  TIntMap     FTempPlanMIBPList;

  class TBackgroundPlanLinker;
  TBackgroundPlanLinker * FBkgPlanLinker;
  bool                    FTerminated;
  bool                    FPlaning;
  void __fastcall SyncCall(TdsSyncMethod AMethod);

  TPlanLinkerDM *   FDM;
  TJSONObject * FPCValue;
  UnicodeString FPCRecId;
  UnicodeString FPCommStage, FPCStage;
  int           FCurrPlan, FCommPlanCount;

  TUnitPrePlan * FUnitPrePlan;

  int          FMaxProgress;   //Max �������� ��� ProgressBar'�
  TNotifyEvent FOnIncProgress; //�������, ��� ��������� �������� ���������� �������
  //��������� ������� ProgressBar'�
  TOnPlanLinkerStageChange FOnPlanerStageChange; //��������� ��� ��������� ����� ������������

  UnicodeString     FLogMsg, FLogFuncName;
  int               FLogALvl;
  TOnLogMsgErrEvent FOnLogMessage;
  TOnLogMsgErrEvent FOnLogError;

  TOnLogBegEndEvent       FOnLogBegin;
  TOnLogBegEndEvent       FOnLogEnd;

  //��� ���������� ������
  TPlanLinkerStage FPlanerStage;

  inline void __fastcall CallOnIncProgress();
  void __fastcall SyncCallOnIncProgress();

  inline void __fastcall CallOnPlanerStageChange(TPlanLinkerStage NewPlanerStage);
  void __fastcall SyncCallOnPlanerStageChange();

  //TkabCustomDataSetRow *FUnitData;
  TdsPlanGetStrValByCodeEvent __fastcall FGetOnGetOrgStr();
  void __fastcall FSetOnGetOrgStr(TdsPlanGetStrValByCodeEvent AVal);

  TdsOnPlanGetMIBPListEvent  __fastcall FGetOnGetMIBPList();
  void __fastcall FSetOnGetMIBPList(TdsOnPlanGetMIBPListEvent AVal);

  void __fastcall UnionPriviv(TUnitPrePlan * ASrc, TUnitPrePlan * ADest, int APlanType);
  int __fastcall AddUnitPlan(TFDQuery *AQ, __int64 APlanCode, TUnitPrePlan *AUnitPrePlan, int APlanType);
  bool __fastcall CheckMIBPPlanCompat(__int64 AMIBPCode);
  bool __fastcall InfInPlanVac(TUnitPriorPrePlan *APlanVac, int AInfCode, int &AInfPrior, bool &AAlreadyInPlan);
  bool __fastcall CheckCompatible(__int64 AMIBP1, __int64 AMIBP2);
  void __fastcall GetPlanContType(int APlanType, int APlanSupportType, int &AType1, int &AType2);
  void __fastcall ClearTmpPrePlan(TUnitPrePlan * APrePlan);
protected :
      void __fastcall SyncCallLogMsg();
  void __fastcall SyncCallLogErr();
  void __fastcall SyncCallLogBegin();
  void __fastcall SyncCallLogEnd();

  void __fastcall CallLogMsg(UnicodeString  AMessage, UnicodeString  AFuncName = "", int ALvl = 1);
  void __fastcall CallLogErr(UnicodeString  AMessage, UnicodeString  AFuncName = "", int ALvl = 1);
  void __fastcall CallLogBegin(UnicodeString  AFuncName);
  void __fastcall CallLogEnd(UnicodeString  AFuncName);
  void __fastcall CallSaveDebugLog(TObject * Sender);
  void __fastcall SyncCallSaveDebugLog();

 public:
  System::UnicodeString __fastcall StartCreatePlan(TJSONObject * AVal, TdsOnPlanCreateUnitPlanEvent AOnCreateUnitPlan);
  System::UnicodeString __fastcall StopCreatePlan();
  __property bool Terminated =   {read = FTerminated};
  __property bool Planing =   {read = FPlaning};
  int __fastcall GetMaxProgress();
  int __fastcall GetCurProgress();
  UnicodeString __fastcall GetCurStage() const ;

  __fastcall TPlanLinker(TFDConnection * AConnection, TAxeXMLContainer * AXMLList, TdsCommClassData *AClsData);
  virtual __fastcall ~TPlanLinker();

  //���� ������ ������� ������������
  __property TDate DatePB =   {read = FDatePB};
  //���� ��������� ������� ������������
  __property TDate DatePE =   {read = FDatePE};
  //1-� ����� ������ ������������
  __property TDate PlanMonth =   {read = FPlanMonth};

  __property int MaxProgress =   {read = FMaxProgress};
  __property TNotifyEvent OnIncProgress =   {read = FOnIncProgress, write = FOnIncProgress};
  //��������� ��� ��������� ����� ������������
  __property TOnPlanLinkerStageChange OnPlanerStageChange =   {read = FOnPlanerStageChange, write = FOnPlanerStageChange};

  //��������� ��� ������ ���-�
  __property TOnLogMsgErrEvent OnLogMessage =   {read = FOnLogMessage, write = FOnLogMessage};
  __property TOnLogMsgErrEvent OnLogError =   {read = FOnLogError, write = FOnLogError};

  __property TOnLogBegEndEvent OnLogBegin =   {read = FOnLogBegin, write = FOnLogBegin};
  __property TOnLogBegEndEvent OnLogEnd =   {read = FOnLogEnd, write = FOnLogEnd};

  __property TdsPlanGetStrValByCodeEvent  OnGetOrgStr = {read = FGetOnGetOrgStr, write=FSetOnGetOrgStr};
  __property TdsOnPlanGetMIBPListEvent OnGetMIBPList    = {read=FGetOnGetMIBPList,    write=FSetOnGetMIBPList};

  __property UnicodeString PCRecId =   {read = FPCRecId};
  System::UnicodeString __fastcall CreatePlan();

  void __fastcall Init(TDate ABegDate);

 };

//---------------------------------------------------------------------------
#endif
