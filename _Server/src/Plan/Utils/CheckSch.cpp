// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "CheckSch.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
__fastcall TCheckSch::TCheckSch(TdsSrvPlanDM * ADM)
 {
  FDM = ADM;
 }
// ---------------------------------------------------------------------------
__fastcall TCheckSch::~TCheckSch()
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TCheckSch::CheckAndUpdate(__int64 AUCode)
 {
  FUCode = AUCode;
  SUCode = IntToStr(AUCode);
  dsLogMessage("PatId=" + SUCode, __FUNC__);
  UnicodeString FNewSch;
  TStringList * UpdateSQLs = new TStringList;
  TFDQuery * FQ = FDM->CreateTempQuery();
  TFDQuery * FUpdQ = FDM->CreateTempQuery();
  TTagNode * FUnitSch = new TTagNode;
  TTagNode * FSchRoot, *itSch;
  bool Updated = false;
  try
   {
    FUnitSch->AsXML = FDM->GetDefUnitSch(FUCode);
    // FUnitSch->AsXML = FDM->GetUnitSch(FUCode);
    // ##############################################################################
    // #                   �������� ���� ��������                                   #
    // #                   ������������� ���� ������                                #
    // ##############################################################################
    TTagNode * Sch, *SchLine, *tmpSch;
    UnicodeString SchUID;
    bool schCorrect;
    try
     {
      // ���� �� ���������
      dsLogMessage("2", __FUNC__);
      FDM->quExecParam(FQ, false,
        "Select CODE, R1020, R1021, R1075, R1092 From class_1003 Where R017A = :pPtId order by R1024", "pPtId", FUCode);
      while (!FQ->Eof)
       {
        SchUID     = "_NULL_";
        schCorrect = false;
        // �������� ������������ �����
        if (FNS(FQ, "R1021").UpperCase().Pos("���"))
         { // �������������� ��������
          if (!((FNS(FQ, "R1021") == "���.") && IsNullSch(FNS(FQ, "R1092"))))
           FDM->quExecParam(FUpdQ, false, "Update CLASS_1003 Set R1092='_NULL_', R1021 = '���.' Where CODE=:pCode",
            "pCode", FQ->FieldByName("CODE")->AsInteger);
          schCorrect = true;
         }
        else
         {
          schCorrect = VacSchCorrect(FQ, "R1020", "R1092", "R1021");
         }
        if (!schCorrect)
         { // ����� ���������� �������� �� ����������
          FNewSch = GetNewSchByType(FNS(FQ, "R1020"), FNS(FQ, "R1021"));
          if (FNewSch.Length())
           { // ������ � ����� ���������� �������
            FDM->quExecParam(FUpdQ, false, "Update CLASS_1003 Set R1092=:pR1092 Where CODE=:pCode", "pCode",
              FQ->FieldByName("CODE")->AsInteger, "", "pR1092", FNewSch);
           }
          else
           { // ������ � ����� ���������� �� �������
            FDM->quExecParam(FUpdQ, false, "Update CLASS_1003 Set R1092='_NULL_', R1021 = '���.' Where CODE=:pCode",
              "pCode", FQ->FieldByName("CODE")->AsInteger);
            dsLogError("������ ���� ����������! PatId=" + SUCode + " : ��� ����������: '" + FNS(FQ, "R1021") +
              "'; �������: (" + FDM->ClsData->VacName[FNS(FQ, "R1020")] + ", ��� = " + FNS(FQ, "R1020") + ")",
              __FUNC__);
           }
         }
        FQ->Next();
       }
      if (FUpdQ->Transaction->Active)
       FUpdQ->Transaction->Commit();
     }
    catch (Exception & E)
     {
      if (FUpdQ->Transaction->Active)
       FUpdQ->Transaction->Rollback();
      dsLogError(E.Message + "; PatId=" + SUCode, __FUNC__);
     }
    // ##############################################################################
    // #                   �������� ���� ����                                       #
    // #                   ������������� ���� ����                                  #
    // ##############################################################################
    UpdateSQLs->Clear();
    SchUID = "_NULL_";
    try
     {
      FDM->quExecParam(FQ, false, "Select  CODE, R1032, R1093 From class_102f Where R017B = :pPtId", "pPtId", FUCode);
      while (!FQ->Eof)
       {
        SchUID     = "_NULL_";
        schCorrect = false;
        // �������� ������������ �����
        if (IsNullSch(FNS(FQ, "R1093"))) // �������������� �����
           schCorrect = true;
        else
         {
          SchLine = FDM->TestSchXML->GetTagByUID(_UID(FNS(FQ, "R1093")));
          if (SchLine)
           schCorrect = SchLine->CmpName("line"); // ������ ����� �������
         }
        if (!schCorrect)
         { // ����� ���������� ����� �� ����������
          Sch = FDM->TestSchXML->GetChildByAV("schema", "ref", FNS(FQ, "R1032"), true);
          if (Sch)
           { // ����� ���������� ����� �������
            SchUID  = Sch->AV["uid"];
            SchLine = Sch->GetFirstChild();
            if (SchLine)
             { // ������ � ����� ���������� �������
              if (SchLine->CmpName("line"))
               {
                SchUID += "." + SchLine->AV["uid"];
                FDM->quExecParam(FUpdQ, false, "Update CLASS_102f Set R1093=:pR1093 Where CODE=:pCode", "pCode",
                  FQ->FieldByName("CODE")->AsInteger, "", "pR1093", SchUID);
               }
              else
               {
                FDM->quExecParam(FUpdQ, false, "Update CLASS_102f Set R1093='_NULL_' Where CODE=:pCode", "pCode",
                  FQ->FieldByName("CODE")->AsInteger);
               }
             }
            else
             { // ������ � ����� ���������� �� �������
              FDM->quExecParam(FUpdQ, false, "Update CLASS_102f Set R1093='_NULL_' Where CODE=:pCode", "pCode",
                FQ->FieldByName("CODE")->AsInteger);
             }
           }
          else
           { // ���� ������
            if (!FDM->ClsData->TestName[FNS(FQ, "R1032")].Length())
             dsLogError("PatId=" + SUCode + ", ������ ���� �����! ��� = " + FNS(FQ, "R1032"), __FUNC__);
            else
             dsLogError("PatId=" + SUCode + ", ����������� �����! �����: (" + FDM->ClsData->TestName[FNS(FQ, "R1032")] +
              ", ��� = " + FNS(FQ, "R1032") + ")", __FUNC__);
           }
         }
        FQ->Next();
       }
      if (FUpdQ->Transaction->Active)
       FUpdQ->Transaction->Commit();
     }
    catch (Exception & E)
     {
      if (FUpdQ->Transaction->Active)
       FUpdQ->Transaction->Rollback();
      dsLogError(E.Message + "; PatId=" + SUCode, __FUNC__);
     }
    // ##############################################################################
    // #                   ����������� ���� ����������                              #
    // ##############################################################################
    FSchRoot = FUnitSch->GetChildByName("sch", true);
    UnicodeString schInf, schSch, exschSch;
    try
     {
      FDM->quExecParam(FQ, false,
        "Select  PRI.R1075, PRI.R1092 From class_1003 PRI Where PRI.R017A = :pPtId and PRI.R1024 = (Select Max(PRI1.R1024) From class_1003 PRI1 Where PRI1.R017A = PRI.R017A and PRI1.R1075 = PRI.R1075 and PRI1.R1092 <> '_NULL_' and PRI1.R10AE=0) and PRI.R10AE=0",
        "pPtId", FUCode);
      UpdateSQLs->Clear();
      while (!FQ->Eof)
       {
        schSch   = FNS(FQ, "R1092").Trim();
        schInf   = FNS(FQ, "R1075").Trim();
        exschSch = "";
        itSch    = FSchRoot->GetChildByAV("spr", "infref", schInf);
        if (itSch)
         {
          exschSch = itSch->AV["vac"];
          if (exschSch.Length())
           {
            if (exschSch.UpperCase() != schSch.UpperCase())
             {
              if (CheckLine(schInf, schSch, exschSch))
               {
                // if (!itSch->AV["chvac"].ToIntDef(0))
                // {
                Updated          = true;
                itSch->AV["vac"] = schSch;
                // }
               }
             }
           }
         }
        FQ->Next();
       }
     }
    catch (Exception & E)
     {
      dsLogError(E.Message + "; PatId=" + SUCode, __FUNC__);
     }
    try
     {
      FDM->quExecParam(FQ, false,
        "Select pr.R1200, pr.R1093 From class_102f pr Where pr.R017B = :pPtId and pr.R1031 = (Select Max(R1031) From class_102f Where R017B = :pPtId and R1200 = pr.R1200 and Upper(pr.R1093) <> '_NULL_' )",
        "pPtId", FUCode);
      UpdateSQLs->Clear();
      while (!FQ->Eof)
       {
        schSch   = FNS(FQ, "R1093").Trim();
        schInf   = FNS(FQ, "R1200").Trim();
        exschSch = "";
        itSch    = FSchRoot->GetChildByAV("spr", "infref", schInf);
        if (itSch)
         {
          exschSch = itSch->AV["test"];
          if (exschSch.Length())
           {
            if (exschSch.UpperCase() != schSch.UpperCase())
             {
              if (CheckLine(schInf, schSch, exschSch))
               {
                // if (!itSch->AV["chtest"].ToIntDef(0))
                // {
                Updated           = true;
                itSch->AV["test"] = schSch;
                // }
               }
             }
           }
         }
        FQ->Next();
       }
     }
    catch (Exception & E)
     {
      dsLogError(E.Message + "; PatId=" + SUCode, __FUNC__);
     }
   }
  __finally
   {
    if (Updated)
     FDM->SetUnitSch(FUCode, FUnitSch->AsXML);
    delete FUnitSch;
    FDM->DeleteTempQuery(FQ);
    FDM->DeleteTempQuery(FUpdQ);
    delete UpdateSQLs;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TCheckSch::VacSchCorrect(TFDQuery * AQuery, UnicodeString AVac, UnicodeString ASch,
  UnicodeString AVacType)
 {
  // schCorrect = VacSchCorrect(FQ, "R1020", "R1092", "R1021");
  bool RC = false;
  try
   {
    TTagNode * Line = FDM->VacSchXML->GetTagByUID(_UID(FNS(AQuery, ASch)));
    if (Line)
     {
      if (Line->CmpName("line"))
       { // ������ ����� �������  // �������� �� ���������� �������, ���� ����������, uid �����
        TTagNode * Sch = Line->GetParent();
        if (Sch)
         {
          RC = Sch->CmpAV("ref", FNS(AQuery, AVac)) && Sch->CmpAV("uid", _GUI(FNS(AQuery, ASch))) && Line->CmpAV("name",
            FNS(AQuery, AVacType));
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TCheckSch::CheckLine(UnicodeString AInf, UnicodeString APrivSch, UnicodeString AOptSch)
 {
  bool RC = true;
  try
   {
    UnicodeString tv, tvEx;
    if (!IsNullSch(APrivSch) && !IsNullSch(AOptSch))
     {
      if (_GUI(APrivSch).UpperCase() != _GUI(AOptSch).UpperCase())
       { // ��������� �� ������ ������
        TTagNode * tmpSchNode = FDM->VacSchXML->GetTagByUID(_UID(APrivSch));
        TTagNode * tmpSchNodeEx = FDM->VacSchXML->GetTagByUID(_UID(AOptSch));
        if (tmpSchNode && tmpSchNodeEx)
         {
          TTagNode * tmpSchp = tmpSchNode->GetParent("schema");
          TTagNode * tmpSchpEx = tmpSchNodeEx->GetParent("schema");
          if (tmpSchp && tmpSchpEx)
           {
            if (FDM->ClsData->InfInVac(AInf, tmpSchpEx->AV["ref"]))
             { // �������� ������ ������� �������
              if (tmpSchNode->CmpName("line") && tmpSchNodeEx->CmpName("line"))
               {
                tv   = tmpSchNode->AV["name"].UpperCase();
                tvEx = tmpSchNodeEx->AV["name"].UpperCase();
                if (tv[1] == 'V')
                 { // v
                  if (tvEx[1] == 'V')
                   { // v
                    tv[1]   = ' ';
                    tv      = tv.Trim();
                    tvEx[1] = ' ';
                    tvEx    = tvEx.Trim();
                    RC      = (tvEx.ToIntDef(1) < tv.ToIntDef(1));
                   }
                  else // rv
                     RC = false;
                 }
                else
                 { // rv
                  if (tvEx[1] != 'V') // v
                   { // rv
                    tv[1]   = ' ';
                    tv[2]   = ' ';
                    tv      = tv.Trim();
                    tvEx[1] = ' ';
                    tvEx[2] = ' ';
                    tvEx    = tvEx.Trim();
                    if (GetLPartB(tv, '/').ToIntDef(1) == GetLPartB(tvEx, '/').ToIntDef(1))
                     RC = (GetRPartE(tvEx, '/').ToIntDef(1) < GetRPartE(tv, '/').ToIntDef(1));
                    else
                     RC = (GetLPartB(tvEx, '/').ToIntDef(1) < GetLPartB(tv, '/').ToIntDef(1));
                   }
                 }
               }
             }
           }
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TCheckSch::GetNewSchByType(UnicodeString AMIBP, UnicodeString AVacType)
 {
  UnicodeString RC = "";
  FSchList = new TList;
  try
   {
    UnicodeString SchUID = AMIBP;
    FDM->VacSchXML->Iterate(FGetSchList, SchUID);
    SchUID = "";
    TTagNode * SchLine, *FDefSch, *FSch;
    FDefSch = NULL;
    for (int i = 0; (i < FSchList->Count) && !FDefSch; i++)
     {
      if (((TTagNode *)FSchList->Items[i])->CmpAV("def", "1"))
       FDefSch = (TTagNode *)FSchList->Items[i];
     }
    SchLine = NULL;
    if (FDefSch) // ����� �� ��������� �������
       SchLine = FDefSch->GetChildByAV("line", "name", AVacType.Trim());
    if (SchLine) // ������ � ����� ���������� �������
       RC = FDefSch->AV["uid"] + "." + SchLine->AV["uid"];
    else
     {
      SchLine = NULL;
      for (int i = 0; (i < FSchList->Count) && !SchLine; i++)
       {
        FDefSch = ((TTagNode *)FSchList->Items[i]);
        SchLine = FDefSch->GetChildByAV("line", "name", AVacType.Trim());
       }
      if (SchLine) // ������ � ����� ���������� �������
         RC = FDefSch->AV["uid"] + "." + SchLine->AV["uid"];
     }
   }
  __finally
   {
    delete FSchList;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TCheckSch::FGetSchList(TTagNode * ANode, UnicodeString & AMIBPCode)
 {
  if (ANode->CmpName("schema"))
   {
    if (ANode->CmpAV("ref", AMIBPCode))
     {
      FSchList->Add((void *)ANode);
     }
   }
  return false;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TCheckSch::FNS(TFDQuery * AQuery, UnicodeString a)
 {
  return AQuery->FieldByName(a)->AsString;
 }
// ---------------------------------------------------------------------------
void __fastcall TCheckSch::FSetUCode(__int64 ACode)
 {
  FUCode = ACode;
  SUCode = IntToStr(ACode);
 }
// ---------------------------------------------------------------------------
bool __fastcall TCheckSch::IsNullSch(UnicodeString ASch)
 {
  return (ASch.UpperCase().Trim() == "_NULL_");
 }
// ---------------------------------------------------------------------------
