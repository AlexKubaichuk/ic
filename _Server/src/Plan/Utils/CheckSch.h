//---------------------------------------------------------------------------
#ifndef CheckSchH
#define CheckSchH

#include <System.Classes.hpp>
#include "dsSrvPlanDMUnit.h"

//---------------------------------------------------------------------------
class TCheckSch : public TObject
{
private:
  TdsSrvPlanDM   *FDM;
  __int64 FUCode;
  UnicodeString SUCode;
  TList   *FSchList;
  void __fastcall FSetUCode(__int64 ACode);
  bool __fastcall VacSchCorrect(TFDQuery *AQuery, UnicodeString AVac, UnicodeString ASch, UnicodeString AVacType);
  bool __fastcall CheckLine(UnicodeString AInf, UnicodeString APrivSch, UnicodeString AOptSch);

  bool __fastcall IsNullSch(UnicodeString ASch);
  UnicodeString __fastcall GetNewSchByType(UnicodeString AMIBP, UnicodeString AVacType);
  bool __fastcall FGetSchList(TTagNode *ANode, UnicodeString &AMIBPCode);

  UnicodeString __fastcall FNS(TFDQuery *AQuery, UnicodeString a);

public:
    __fastcall TCheckSch(TdsSrvPlanDM *ADM);
    __fastcall ~TCheckSch();
//  __property __int64 UCode = {read = FUCode, write=FSetUCode};
  void __fastcall CheckAndUpdate(__int64 AUCode);
};
//---------------------------------------------------------------------------
#endif

