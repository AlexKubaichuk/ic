//---------------------------------------------------------------------------

#pragma hdrstop

#include "dsPlaner.h"
#include "icsLog.h"
#include "DKUtils.h"
#include "DKClasses.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall TdsPlaner::TdsPlaner(TdsPlanDM * ADM)
 {
 }
//---------------------------------------------------------------------------
__fastcall TdsPlaner::~TdsPlaner()
 {
 }
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#       ���� ��� ��������                                                    #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
void __fastcall TdsPlaner::CreateUnitPlan(__int64 APtId)
 {
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::FCreateUnitPlan(__int64 APtId, TPrePlaner * APrePlaner, TDate ABegDate)
 {//������������ ��� ��������
  try
   {
    dsPlanLogMessage("������������", __FUNC__, 5);
    FPlanningStatus = psInProgress;
    try
     {
      APrePlaner->CreateUnitPlan(ABegDate, APtId);
     }
    catch (DKCommonExceptionEx & E)
     {
      dsPlanLogError("��� ������ - " + IntToStr(E.Id) + "; ���������  - " + E.Message + " ������ ���������:" + E.DetMsg->Text, __FUNC__);
     }
    catch (Exception & E)
     {
      dsPlanLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    FPlanningStatus = psNone;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::FOnCreateUnitPlan(__int64 APtId, TDate ABegDate)
 {
  TTime FBeg = Time();
  try
   {
    FCreateUnitPlan(APtId, FIMMPlaner, ABegDate);
   }
  __finally
   {
    dsPlanLogMessage("������������ ������������ ��� " + IntToStr(APtId) + ": " + (Time() - FBeg).FormatString("nn.ss.zzz"));
   }
 }
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#       ���� �� �������/�����������                                          #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
UnicodeString __fastcall TdsPlaner::StartCreatePlan(TJSONObject * AVal)
 {
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsPlaner::StopCreatePlan()
 {
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsPlaner::GetPlanProgress(System::UnicodeString & ARecCode)
 {
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::PlanCreateEnd()
 {
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::FLogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsPlanLogMessage(AMessage, AFuncName, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::FLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsPlanLogError(AMessage, AFuncName, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::FLogBegin(UnicodeString AFuncName)
 {
  dsLogBegin(AFuncName);
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::FLogEnd(UnicodeString AFuncName)
 {
  dsLogEnd(AFuncName);
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::FSaveDebugLog(UnicodeString AXML)
 {
  dsLogBegin(__FUNC__);
  if (AXML.Length())
   {
    dsPlanLogMessage("==============================================================================");
    dsPlanLogMessage(AXML);
    dsPlanLogMessage("==============================================================================");
   }
  dsLogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::PlanLinkerStageChange(TObject * Sender, TPlanLinkerStage NewPlanerStage)
 {
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::PlanerStageChange(TObject * Sender, TPlanerStage NewPlanerStage)
 {
  if (/*(*/FPlanningStatus == psInProgress/*) && FPlanningProgress*/)
   {
    switch (NewPlanerStage)
     {
     case psInitialization:
      FLogMessage("\"psInitialization\"", __FUNC__, 5);
      break;
     case psUpdatePlans:
      FLogMessage("\"psUpdatePlans\"", __FUNC__, 5);
      break;
     case psPlanDop:
      FLogMessage("\"psPlanDop\"", __FUNC__, 5);
      break;
     case psPlanPriviv:
      FLogMessage("\"psPlanPriviv\"", __FUNC__, 5);
      break;
     case psPlanProb:
      FLogMessage("\"psPlanProb\"", __FUNC__, 5);
      break;
     case psAppendDopPrivivByPrevProb:
      FLogMessage("\"psAppendDopPrivivByPrevProb\"", __FUNC__, 5);
      break;
     case psCorrectPolyVak:
      FLogMessage("\"psCorrectPolyVak\"", __FUNC__, 5);
      break;
     case psCorrectPrivivPlanDate:
      FLogMessage("\"psCorrectPrivivPlanDate\"", __FUNC__, 5);
      break;
     case psAppendPrepareProb:
      FLogMessage("\"psAppendPrepareProb\"", __FUNC__, 5);
      break;
     case psApplyPlan:
      FLogMessage("\"psApplyPlan\"", __FUNC__, 5);
      break;
     }
    Application->ProcessMessages();
   }
 }

//---------------------------------------------------------------------------

