#ifndef PlanerdebugH
#define PlanerdebugH

#include <System.Classes.hpp>

#include "DKCfg.h"      // ��������� �������� ����������

//#include <System.hpp>
//#include "PlanerDef.h"
//#include "XMLContainer.h"
#include "dsPlanDMUnit.h"

//---------------------------------------------------------------------------
class TPrePlanerDebug : public TObject
{
private:
   TTagNode *FLog;
   TTagNode *FCurPat;
   TTagNode *FCurInfLine;
   TTagNode *FCurVacList;
   TTagNode *FCurTestList;
   TTagNode *FtmpCurLine;

 TNotifyEvent FOnSaveLog;

   TTagNode *Fp1;
   TTagNode *Fp2;
   TTagNode *Fp3;
   TTagNode *Fp4;
   TTagNode *Fp5;
   TTagNode *Fp6;


//   TKontType       FDbgKontType;
//   TDopIDKind      FDbgDopIDKind;
//   TFltPlanKind    FDbgFltPlanKind;
//   int             FDbgDopID;
//   TDate           FDbgDopPlanDate;
//   TPlanPeriodType FDbgPPP;

   TPlanItemList* FPrivPlan;
   TPlanItemList* FDopPrivivPlan;
   TPlanItemList* FProbPlan;
   TPlanItemList* FDopProbPlan;
   TdsPlanDM *FDM;


   void __fastcall DFW(TTagNode *ANode, UnicodeString AComm, UnicodeString AVal);
   UnicodeString __fastcall DateStr(TDateTime ADate);
  void __fastcall SaveLog();

   UnicodeString __fastcall GetInfName(int ACode);
   UnicodeString __fastcall GetVacName(int ACode);
   UnicodeString __fastcall GetTestName(int ACode);
   UnicodeString __fastcall GetCheckName(int ACode);

protected:

public:
    __fastcall TPrePlanerDebug(TdsPlanDM *ADM);
    __fastcall ~TPrePlanerDebug();

  void __fastcall InitLog();

    void __fastcall LogPlanPeriod(TDate ADatePB, TDate ADatePE, TDate APlanMonth);
    void __fastcall LogPatData(int APtId, UnicodeString APtFIO,TDate APtBirthDay, UnicodeString APtAge, UnicodeString UnitSch);

    void __fastcall StartSch(TTagNode *ABegSchemeLine);

    void __fastcall Set_CVL_CTL();
    void __fastcall SetCIL(TPlanItemKind APlanItemKind, UnicodeString AInfId, bool ARoundMode, bool ALPlanItemExists, TDate ALPlanItemDate);

    void __fastcall CIL_ExceptSch(bool AResult);
    void __fastcall CIL_Data(TTagNode *ACurSchemeLine, TStringList *AJumpTrace);
    void __fastcall CIL_ItemPlanDate(TDate APlanItemPlanDate);
    void __fastcall CIL_Season(bool ASeasonOk);
    void __fastcall CIL_PlanByPrevTest(bool AResult);
    void __fastcall CIL_RetToPlan();
    void __fastcall CIL_PlanBeginDate(TPlanItemKind APlanItemKind, TDate ANPlanItemBegDate);
    void __fastcall CIL_InPlanPeriod();

    void __fastcall ItemListBegin(TPlanItemKind APlanItemKind);
    void __fastcall SetItemList(int AInfId, int AInfType);
    void __fastcall ItemListEnd();

    void __fastcall DebugPrintListPlans(int  ARootIdx);

   __property TTagNode *Log = {read = FLog};

   __property TPlanItemList* PrivPlan  = {read = FPrivPlan, write=FPrivPlan};
   __property TPlanItemList* DopPrivivPlan  = {read = FDopPrivivPlan, write=FDopPrivivPlan};
   __property TPlanItemList* ProbPlan  = {read = FProbPlan, write=FProbPlan};
   __property TPlanItemList* DopProbPlan  = {read = FDopProbPlan, write=FDopProbPlan};


//   __property TKontType       KontType = {read = FDbgKontType, write=FDbgKontType};
//   __property TDopIDKind      DopIDKind = {read = FDbgDopIDKind, write=FDbgDopIDKind};
//   __property TFltPlanKind    FltPlanKind = {read = FDbgFltPlanKind, write=FDbgFltPlanKind};
//   __property int             DopID = {read = FDbgDopID, write=FDbgDopID};
//   __property TDate           DopPlanDate = {read = FDbgDopPlanDate, write=FDbgDopPlanDate};
//   __property TPlanPeriodType PPP = {read = FDbgPPP, write=FDbgPPP};

   __property TNotifyEvent OnSaveLog = {read = FOnSaveLog, write=FOnSaveLog};

};

//---------------------------------------------------------------------------
#endif
