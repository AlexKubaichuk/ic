/*
 ����        - Planer.cpp
 ������      - ���������� ������������.
 ��������� "���������� ������������"
 ��������    - �����������.
 ���� ����������
 ����������� - ������� �.�.
 ����        - 09.05.2004
 */
// ---------------------------------------------------------------------------
#include <string.h>
#pragma hdrstop
#include <algorithm>
#include <vcl.h>
#include "DKSTL.h"
#include "PlanLinker.h"
#include "dsSrvRegTemplate.h"
#include "JSONUtils.h"
#include "OrgParsers.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                     TExecutor::TBackgroundPlanLinker                  ##
// ##                                                                       ##
// ###########################################################################
class TPlanLinker::TBackgroundPlanLinker : public TThread
 {
 private:
  TPlanLinker * FPlaner;
 protected:
  virtual void __fastcall Execute();
 public:
  __fastcall TBackgroundPlanLinker(bool CreateSuspended, TPlanLinker * APlaner);
  __fastcall ~TBackgroundPlanLinker();
  void __fastcall ThreadSafeCall(TdsSyncMethod Method);
 };
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                      EExecutionError                                  ##
// ##                                                                       ##
// ###########################################################################
__fastcall EExecutionError::EExecutionError(const UnicodeString Msg) : Exception(Msg)
 {
 }
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                     TExecutor::BackgroundExecutor                     ##
// ##                                                                       ##
// ###########################################################################
__fastcall TPlanLinker::TBackgroundPlanLinker::TBackgroundPlanLinker(bool CreateSuspended, TPlanLinker * APlaner)
  : TThread(CreateSuspended), FPlaner(APlaner)
 {
 }
// ---------------------------------------------------------------------------
__fastcall TPlanLinker::TBackgroundPlanLinker::~TBackgroundPlanLinker()
 {
  FPlaner = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::TBackgroundPlanLinker::Execute()
 {
  // CallLogBegin();
  CoInitialize(NULL);
  FPlaner->CreatePlan();
  // CallLogEnd();
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::TBackgroundPlanLinker::ThreadSafeCall(TdsSyncMethod Method)
 {
  Synchronize(Method);
 }
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                              TPlanLinker                               ##
// ##                                                                       ##
// ###########################################################################
// ADB                  - ������� ���� ������
// FDebugList           - ���� ��� ���������� ������
// [default = ""]
__fastcall TPlanLinker::TPlanLinker(TFDConnection * AConnection, TAxeXMLContainer * AXMLList,
  TdsCommClassData * AClsData)
 {
  FDM                  = new TPlanLinkerDM(NULL, AConnection, AXMLList, AClsData);
  FBkgPlanLinker       = NULL;
  FPCStage             = "";
  FPCommStage          = "";
  FMaxProgress         = 10;
  FOnIncProgress       = NULL;
  FOnPlanerStageChange = NULL;
 }
// ---------------------------------------------------------------------------
__fastcall TPlanLinker::~TPlanLinker()
 {
  // delete FDM;
  if (FBkgPlanLinker)
   delete FBkgPlanLinker;
  FBkgPlanLinker = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::CallOnIncProgress()
 {
  SyncCall(& SyncCallOnIncProgress);
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::SyncCallOnIncProgress()
 {
  if (FOnIncProgress)
   FOnIncProgress(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::CallOnPlanerStageChange(TPlanLinkerStage NewPlanerStage)
 {
  FPlanerStage = NewPlanerStage;
  SyncCall(& SyncCallOnPlanerStageChange);
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::SyncCallOnPlanerStageChange()
 {
  if (FOnPlanerStageChange)
   OnPlanerStageChange(this, FPlanerStage);
 }
// ---------------------------------------------------------------------------
// ������������� ( ����������� ������� ������������ )
void __fastcall TPlanLinker::Init(TDate ABegDate)
 {
  FPlanMonth = ::CalculatePlanPeriod(ABegDate, FDatePB, FDatePE);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #       ���������� ����� ��� �����������                                     #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TPlanLinker::CreatePlan()
 {
  CallLogBegin(__FUNC__);
  CallLogMsg("AValue = " + FPCValue->ToString());
  UnicodeString RC = "������";
  TFDQuery * FQ = FDM->CreateTempQuery();
  TFDQuery * InsQ = FDM->CreateTempQuery();
  FTerminated    = false;
  FPlaning       = true;
  FCurrPlan      = 0;
  FCommPlanCount = 0;
  FPCommStage    = "���������� ������������ ... ";
  FPCStage       = FPCommStage;
  TUnitPrePlan::iterator plFRC;
  try
   {
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    try
     {
      Application->ProcessMessages();
      FDM->GetMIBPFromStore();
      Application->ProcessMessages();
      TJSONPairEnumerator * itPair;
      UnicodeString FPlanName, FUnitFlName;
      __int64 FPlanUnitCode = -1;
      __int64 FPlanCode;
      UnicodeString FPlanGUID;
      int FPlanType = JSONToInt(FPCValue, "type", 0);
      int FPlanSupportType = JSONToInt(FPCValue, "supporttype", 0);
      int FPlanPeriod = JSONToInt(FPCValue, "planperiod", 1);
      UnicodeString FPlanedPlanMonth = JSONToString(FPCValue, "planmonth", "");
      UnicodeString FPlanPrintFormat = JSONToString(FPCValue, "planprintformat", "");
      UnicodeString __fastcall(__closure * GetUnitName)(TJSONObject * AValue);
      __int64 tmpCode1, tmpCode2;
      int Type1, Type2;
      // __int64 FPlanUnitCode = JSONToInt(FPCValue, "");
      FPlUnitList.clear();
      if (FPlanType == 0)
       { // �������
        GetUnitName = FDM->GetUchName;
        FUnitFlName = "00B3";
        tmpCode1    = JSONToInt(FPCValue, "00B3"); // �������
        if (tmpCode1)
         FPlUnitList.push_back(tmpCode1);
        else
         {
          tmpCode1 = JSONToInt(FPCValue, "00F1"); // ���
          // tmpCode2 = JSONToInt(FPCValue, "00F9"); //�������������
          if (tmpCode1)
           {
            // ��������� ������ �������� ����� ���
            FDM->quExecParam(FQ, false,
              "Select uch.CODE, otd.R000D From CLASS_000E uch join CLASS_000C otd on (otd.code=uch.R00F7) Where uch.R00F6=:pCode1",
              "pCode1", tmpCode1);
            CallLogMsg("Plan ������ �������� FQ.Count : " + IntToStr(FQ->RecordCount));
            while (!FQ->Eof)
             {
              FPlUnitList.push_back(FQ->FieldByName("CODE")->AsInteger);
              FQ->Next();
             }
           }
         }
       }
      else if ((FPlanType == 1) || (FPlanType == 2) || (FPlanType == 3) || (FPlanType == 4))
       { // �����������
        GetUnitName = FDM->GetOrgName;
        FUnitFlName = "0025";
        orgCode FOrgCode = ParseOrgCodeStr(JSONToString(FPCValue, "0025", "0.0@1##0")); // �����������
        tmpCode2 = FOrgCode.OrgID; // �����������
        tmpCode1 = JSONToInt(FPCValue, "0023"); // ���������� ������
        if (tmpCode2)
         FPlUnitList.push_back(tmpCode2);
        else
         {
          if (tmpCode1)
           {
            FDM->quExecParam(FQ, false,
              "Select distinct r0025 From CLASS_1000 Where R0023=:pCode1 and r0025 is not null and r0025 <> 0",
              "pCode1", tmpCode1);
            while (!FQ->Eof)
             {
              FPlUnitList.push_back(FQ->FieldByName("r0025")->AsInteger);
              FQ->Next();
             }
           }
         }
       }
      else if (FPlanType == 5)
       { // ������
        GetUnitName = FDM->GetFltName;
       }
      TJSONObject * FPlanPCValue;
      UnicodeString FFieldsDef = FDM->GetPlanTemplateDef(FPlanType);
      CallLogMsg("Plan FPlUnitList : " + IntToStr((int)FPlUnitList.size()));
      FPCRecId = "";
      TIntStrMap::iterator FDopFRC;
      // ���� �� �������� / ������������
      for (TPlanedUnitList::iterator i = FPlUnitList.begin(); i != FPlUnitList.end(); i++)
       {
        FPlanPCValue = (TJSONObject *)FPCValue->Clone();
        IntToJSON(FPlanPCValue, FUnitFlName, (* i));
        CallLogMsg("Plan FPCValue : " + FPlanPCValue->ToString());
        FPlanName   = GetUnitName(FPlanPCValue);
        FPCommStage = "���� ���: " + FPlanName;
        FDM->quExecParam(FQ, false,
          "Select CODE, CGUID From CLASS_3188 Where R318A=:pPeriod and Upper(R318B)=:pName and R3192=:pType", "pPeriod",
          FPlanMonth, "", "pName", FPlanName.UpperCase(), "pType", FPlanType + 1);
        if (FQ->RecordCount)
         {
          FPlanCode = FQ->FieldByName("CODE")->AsInteger;
          FPlanGUID = FQ->FieldByName("CGUID")->AsString;
         }
        else
         {
          FPlanCode = FDM->GetNewCode("3188");
          FPlanGUID = FDM->NewGUID();
         }
        DeleteJSONPair(FPlanPCValue, "type");
        DeleteJSONPair(FPlanPCValue, "supporttype");
        DeleteJSONPair(FPlanPCValue, "planmonth");
        DeleteJSONPair(FPlanPCValue, "planperiod");
        DeleteJSONPair(FPlanPCValue, "planprintformat");
        TdsRegTemplate * FTmpl = new TdsRegTemplate(FFieldsDef, FDM->XMLList->GetXML("40381E23-92155860-4448"),
          FPlanPCValue);
        try
         {
          CallLogMsg("Plan where def: " + FFieldsDef);
          CallLogMsg("Plan FPCValue before GetWhere : " + FPlanPCValue->ToString());
          UnicodeString FWhere = FTmpl->GetWhere("CB");
          CallLogMsg("Plan Where : " + FWhere);
          if (FQ->Transaction->Active)
           FQ->Transaction->Commit();
          // ������ ���� ����� ����������
          FPCStage = FPCommStage + " ������� ����� ... ";
          FDM->quExecParam(FQ, false, "Delete from class_1084 where R318F=:pPlanCode and r1098<>1", "pPlanCode",
            FPlanCode);
          FDM->quExecParam(FQ, false, "Delete from class_3084 where R328F=:pPlanCode and r3098<>1", "pPlanCode",
            FPlanCode);
          if (FDM->OnCreateUnitPlan)
           { // ��������� �� ������������� ����������������
            FQ->SQL->Text =
              "Select Count(distinct cb.code) as RCount from class_1000 cb where (cb.R3195 is null or cb.R3195 <= :pBegPlanDate) and (cb.r32B3 = :pSType1 or cb.r32B3 = :pSType2) and " +
              FWhere;
            FQ->Prepare();
            itPair = FPlanPCValue->GetEnumerator();
            while (itPair->MoveNext())
             {
              UnicodeString V1 = JSONEnumPairCode(itPair);
              UnicodeString V2 = JSONEnumPairValue(itPair);
              FQ->ParamByName("p" + V1)->Value = V2;
             }
            FQ->ParamByName("pBegPlanDate")->Value = Date(); // FDatePB;
            GetPlanContType(FPlanType, FPlanSupportType, Type1, Type2);
            FQ->ParamByName("pSType1")->Value = Type1;
            FQ->ParamByName("pSType2")->Value = Type2;
            FQ->OpenOrExecute();
            FCommPlanCount = FQ->FieldByName("RCount")->AsInteger;
            Application->ProcessMessages();
            CallLogMsg("������������ �����������, ����� = " + FQ->FieldByName("RCount")->AsString, "", 5);
            FQ->SQL->Text =
              "Select distinct cb.code as UCODE, cb.r001f, cb.r0020, cb.r002f, cb.r0031 from class_1000 cb where (cb.R3195 is null or cb.R3195 <= :pBegPlanDate) and (cb.r32B3 = :pSType1 or cb.r32B3 = :pSType2) and " +
              FWhere;
            FQ->Prepare();
            itPair = FPlanPCValue->GetEnumerator();
            while (itPair->MoveNext())
             FQ->ParamByName("p" + JSONEnumPairCode(itPair))->Value = JSONEnumPairValue(itPair);
            FQ->ParamByName("pBegPlanDate")->Value = Date(); // FDatePB;
            GetPlanContType(FPlanType, FPlanSupportType, Type1, Type2);
            FQ->ParamByName("pSType1")->Value = Type1;
            FQ->ParamByName("pSType2")->Value = Type2;
            FQ->OpenOrExecute();
            FPCStage  = FPCommStage + " ��������������� ������������ ... ";
            FCurrPlan = 0;
            try
             {
              while (!FQ->Eof && !FTerminated)
               {
                FDM->OnCreateUnitPlan(FQ->FieldByName("UCODE")->AsInteger, FDatePB,
                  FQ->FieldByName("r0031")->AsDateTime, FQ->FieldByName("r001f")->AsString + " " +
                  FQ->FieldByName("r0020")->AsString + " " + FQ->FieldByName("r002f")->AsString);
                FCurrPlan++ ;
                Application->ProcessMessages();
                FQ->Next();
               }
             }
            __finally
             {
             }
           }
          if (FQ->Transaction->Active)
           FQ->Transaction->Commit();
          FQ->Transaction->StartTransaction();
          UnicodeString FSQL, FCountSQL;
          FSQL = "Select distinct cb.code as UCODE, cb.R001f, cb.R0020, cb.R002f, cb.R32B3, cb.R0031, cb.R0026, cb.R0027, cb.R0028, ppl.* from class_1000 cb right join class_1112 ppl on (ppl.r1113 = cb.code) where cb.code is not null and ppl.r1115 >= :pBegPlanDate and ppl.r1115 <= :pEndPlanDate  and (cb.r32B3 = :pSType1 or cb.r32B3 = :pSType2) and ";
          FCountSQL =
            "Select Count(distinct cb.code) as RCount from class_1000 cb right join class_1112 ppl on (ppl.r1113 = cb.code) where cb.code is not null and ppl.r1115 >= :pBegPlanDate and ppl.r1115 <= :pEndPlanDate and (cb.r32B3 = :pSType1 or cb.r32B3 = :pSType2) and ";
          switch (FPlanType)
           {
           case 0:
             { // [1] �������:            ������ ���� (������ ��� �������� � �����)
              FSQL += FWhere + " order by cb.code, ppl.code";
              FCountSQL += FWhere;
              break;
             }
           case 1:
             { // [2] �����������:   ����������� ���� (��� �������� � ���� ����������� ��������)
              // FSQL += "R3193<>1 and " + FWhere + " order by cb.code, ppl.code";
              // FCountSQL += "R3193<>1 and " + FWhere;
              FSQL += FWhere + " order by cb.code, ppl.code";
              FCountSQL += FWhere;
              break;
             }
           case 2:
             { // [3] �����������:   ������ ���� (������ ��� �������� � �����)
              FSQL += FWhere + " order by cb.code, ppl.code";
              FCountSQL += FWhere;
              break;
             }
           case 3:
             { // [4] �����������:   ���� ��� ���������� ���������� (�����)
              // FSQL += "R3193=1 and " + FWhere + " order by ppl.r3187, cb.code, ppl.code";
              // FCountSQL += "R3193=1 and " + FWhere;
              FSQL += FWhere + " order by ppl.r3187, cb.code, ppl.code";
              FCountSQL += FWhere;
              if (FPlanPeriod > 1)
               { // �������� �� ���� �������
                FDatePE = Sysutils::IncMonth(FDatePB, FPlanPeriod - 1);
                FDatePE = TDate(IntToStr(DaysInMonth(FDatePE)) + FDatePE.FormatString(".mm.yyyy"));
               }
              break;
             }
           case 4:
             { // [5] �����������:   ���� ��� ���������� ���������� (�������� ��� ������ ��������)
              // FSQL += "R3193=1 and " + FWhere + " order by ppl.r3187, cb.code, ppl.code";
              // FCountSQL += "R3193=1 and " + FWhere;
              FSQL += FWhere + " order by ppl.r3187, cb.code, ppl.code";
              FCountSQL += FWhere;
              break;
             }
           case 5:
             { // [6] ����������� ������������ (������������ �� �������)
              break;
             }
           }
          // CallLogMsg("Check PlanType::after");
          FQ->SQL->Text = FCountSQL;
          FQ->Prepare();
          CallLogMsg("������������ �����, SQL = " + FCountSQL, "", 5);
          itPair = FPlanPCValue->GetEnumerator();
          while (itPair->MoveNext())
           {
            FQ->ParamByName("p" + JSONEnumPairCode(itPair))->Value = JSONEnumPairValue(itPair);
            CallLogMsg("������������ �����, p" + JSONEnumPairCode(itPair) + " = " + JSONEnumPairValue(itPair), "", 5);
           }
          CallLogMsg("������������ �����, FDatePB = " + FDatePB.FormatString("dd.mm.yyyy") + ", FDatePE = " +
            FDatePE.FormatString("dd.mm.yyyy"), "", 5);
          FQ->ParamByName("pBegPlanDate")->Value = FDatePB;
          FQ->ParamByName("pEndPlanDate")->Value = FDatePE;
          GetPlanContType(FPlanType, FPlanSupportType, Type1, Type2);
          FQ->ParamByName("pSType1")->Value = Type1;
          FQ->ParamByName("pSType2")->Value = Type2;
          CallLogMsg("������������ �����, SQL = " + FCountSQL, "", 5);
          FQ->OpenOrExecute();
          FPCStage       = FPCommStage + " ������������ ����� ... ";
          FCommPlanCount = FQ->FieldByName("RCount")->AsInteger;
          Application->ProcessMessages();
          FCurrPlan = 0;
          CallLogMsg("������������ �����, ����� = " + FQ->FieldByName("RCount")->AsString, "", 5);
          // ��������/���������� ����� -- begin -------------------------
          if (!InsQ->Transaction->Active)
           InsQ->Transaction->StartTransaction();
          InsQ->Transaction->StartTransaction();
          InsQ->SQL->Text =
            "UPDATE OR INSERT INTO class_3188 (code, cguid, R318A, R318B, R318D, R318E, R3192, R318C, R3194) values (:pcode, :pcguid, :pR318A, :pR318B, :pR318D, :pR318E, :pR3192, :pR318C, :pR3194) MATCHING (CODE)";
          InsQ->Prepare();
          InsQ->ParamByName("pCode")->Value = FPlanCode;
          InsQ->ParamByName("pcguid")->Value = FPlanGUID;
          InsQ->ParamByName("pR318A")->Value = FPlanMonth; // <date name='������' uid='318A'/>
          InsQ->ParamByName("pR318B")->Value = FPlanName;
          // <text name='������������' uid='318B' cast='no' length='255'/>
          InsQ->ParamByName("pR318D")->Value = FQ->FieldByName("RCount")->AsInteger;
          if (FQ->FieldByName("RCount")->AsInteger)
            // <digit name='������� �������' uid='318D' required='1' digits='6' min='0' max='999999'/>
             InsQ->ParamByName("pR318E")->Value = 2;
          else
           InsQ->ParamByName("pR318E")->Value = 1;
          InsQ->ParamByName("pR3192")->Value = FPlanType + 1; // <choice name='���' uid='3192' default='0'>
          FPlanPCValue->AddPair("type", FPlanType);
          FPlanPCValue->AddPair("supporttype", FPlanSupportType);
          FPlanPCValue->AddPair("planperiod", FPlanPeriod);
          FPlanPCValue->AddPair("planmonth", FPlanedPlanMonth);
          FPlanPCValue->AddPair("planprintformat", FPlanPrintFormat);
          InsQ->ParamByName("pR318C")->Value = FPlanPCValue->ToString();
          // <extedit name='��������� ������' uid='318C'/>
          DeleteJSONPair(FPlanPCValue, "type");
          DeleteJSONPair(FPlanPCValue, "supporttype");
          DeleteJSONPair(FPlanPCValue, "planperiod");
          DeleteJSONPair(FPlanPCValue, "planmonth");
          DeleteJSONPair(FPlanPCValue, "planprintformat");
          InsQ->ParamByName("pR3194")->Value = (*i); // <choiceDB name='��� �������/�����������' uid='3194' ref='000E'/>
          InsQ->Prepare();
          InsQ->OpenOrExecute();
          if (InsQ->Transaction->Active)
           InsQ->Transaction->Commit();
          // ��������/���������� ����� -- end -------------------------
          CallLogMsg("������������ �����, ������, SQL = " + FSQL, "", 5);
          FQ->SQL->Text = FSQL;
          FQ->Prepare();
          itPair = FPlanPCValue->GetEnumerator();
          while (itPair->MoveNext())
           FQ->ParamByName("p" + JSONEnumPairCode(itPair))->Value = JSONEnumPairValue(itPair);
          FQ->ParamByName("pBegPlanDate")->Value = FDatePB;
          FQ->ParamByName("pEndPlanDate")->Value = FDatePE;
          GetPlanContType(FPlanType, FPlanSupportType, Type1, Type2);
          FQ->ParamByName("pSType1")->Value = Type1;
          FQ->ParamByName("pSType2")->Value = Type2;
          FQ->OpenOrExecute();
          __int64 FPlUCode = -1;
          FUnitPrePlan = new TUnitPrePlan;
          UnicodeString FPlItemCode, FUnitFIO;
          FUnitFIO = "";
          int FPlanUnitCount = 0;
          try
           {
            FUnitPrePlan->clear();
            // ******  begin ���� �� ������� ���������������� �����   **************
            while (!FQ->Eof && !FTerminated)
             { // ���� �� ������� ���������������� �����
              // �������� ������ ��� ������ �������� �
              // ��������� �� ��������� ���� AddUnitPlan(...)
              FPlItemCode = FQ->FieldByName("R1116")->AsString + "." + FQ->FieldByName("R3187")->AsString;
              if (FQ->FieldByName("R1113")->AsInteger != FPlUCode)
               { // ����� �������  - �������� ������������� � ����
                if (FPlUCode != -1)
                 {
                  // ***  Debug output ***********************************************
                  CallLogMsg("1. " + FUnitFIO + "----------------------------------------", "", 5);
                  for (TUnitPrePlan::iterator i = FUnitPrePlan->begin(); i != FUnitPrePlan->end(); i++)
                   CallLogMsg(FDM->ClsData->InfName[IntToStr(i->second->Inf)] + " id: " + IntToStr(i->second->Inf) +
                    " " + i->second->VacType, "", 5);
                  CallLogMsg("----------------------------------------------------------------------------", "", 5);
                  // ***  Debug output ***********************************************
                  FPlanUnitCount += AddUnitPlan(InsQ, FPlanCode, FUnitPrePlan, FPlanType);
                  FCurrPlan++ ;
                  Application->ProcessMessages();
                 }
                ClearTmpPrePlan(FUnitPrePlan);
                (*FUnitPrePlan)[FPlItemCode] = new TPrePlanRec(FQ);
                FPlUCode = FQ->FieldByName("R1113")->AsInteger;
                FUnitFIO = IntToStr(FPlUCode) + " " + FQ->FieldByName("R001f")->AsString + " " +
                  FQ->FieldByName("R0020")->AsString + " " + FQ->FieldByName("R002F")->AsString;
               }
              else
               { // ���������� �������� ������ ��� ��������
                plFRC = FUnitPrePlan->find(FPlItemCode);
                if (plFRC == FUnitPrePlan->end())
                 (*FUnitPrePlan)[FPlItemCode] = new TPrePlanRec(FQ);
                else
                 {
                  if (plFRC->second->PlanItemType == FQ->FieldByName("R1116")->AsInteger)
                   CallLogErr("��������� ��������/���� �� ����� ��������. Data: " +
                    (* FUnitPrePlan)[FPlItemCode]->ToString(), __FUNC__);
                 }
               }
              FQ->Next();
              Application->ProcessMessages();
             }
            // ******  end ���� �� ������� ���������������� �����   **************
            if (FUnitPrePlan->size() && (FPlUCode != -1))
             {
              // ***  Debug output ***********************************************
              CallLogMsg("2. " + FUnitFIO + "----------------------------------------", "", 5);
              for (TUnitPrePlan::iterator i = FUnitPrePlan->begin(); i != FUnitPrePlan->end(); i++)
               CallLogMsg(FDM->ClsData->InfName[IntToStr(i->second->Inf)] + " id: " + IntToStr(i->second->Inf) + " " +
                i->second->VacType, "", 5);
              CallLogMsg("----------------------------------------------------------------------------", "", 5);
              // ***  Debug output ***********************************************
              FPlanUnitCount += AddUnitPlan(InsQ, FPlanCode, FUnitPrePlan, FPlanType);
              ClearTmpPrePlan(FUnitPrePlan);
              FCurrPlan++ ;
              Application->ProcessMessages();
             }
            if (!InsQ->Transaction->Active)
             InsQ->Transaction->StartTransaction();
            CallLogMsg("PlanUnitCount = " + IntToStr(FPlanUnitCount), __FUNC__, 5);
            InsQ->SQL->Text = "UPDATE  class_3188 set R318D = :pR318D where code=:pcode";
            InsQ->Prepare();
            InsQ->ParamByName("pCode")->Value = FPlanCode;
            InsQ->ParamByName("pR318D")->Value = FPlanUnitCount;
            InsQ->Prepare();
            InsQ->OpenOrExecute();
            if (InsQ->Transaction->Active)
             InsQ->Transaction->Commit();
           }
          __finally
           {
            ClearTmpPrePlan(FUnitPrePlan);
            delete FUnitPrePlan;
           }
         }
        __finally
         {
          delete FTmpl;
          if (FQ->Transaction->Active)
           FQ->Transaction->Commit();
         }
        FPCStage = "";
        if (!FTerminated)
         {
          if (!FPCRecId.Length())
           FPCRecId = IntToStr(FPlanCode);
         }
       }
      if (!FTerminated)
       RC = "ok";
     }
    catch (Exception & E)
     {
      if (FQ->Transaction->Active)
       FQ->Transaction->Rollback();
      CallLogErr(E.Message, __FUNC__);
     }
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
    FDM->DeleteTempQuery(InsQ);
    CallSaveDebugLog(NULL);
    FPlaning = false;
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TPlanLinker::ClearTmpPrePlan(TUnitPrePlan * APrePlan)
 {
  for (TUnitPrePlan::iterator i = APrePlan->begin(); i != APrePlan->end(); i++)
   {
    delete i->second;
    i->second = NULL;
   }
  APrePlan->clear();
 }
// ----------------------------------------------------------------------------
void __fastcall TPlanLinker::GetPlanContType(int APlanType, int APlanSupportType, int & AType1, int & AType2)
 {
  // <choicevalue name='������������� �� �������' value='1'/>
  // <choicevalue name='������������� �� �����������' value='2'/>
  // <choicevalue name='������������� �� ������' value='3'/>
  // <choicevalue name='��������������' value='4'/>
  // <choicevalue name='�����' value='5'/>
  // <choicevalue name='�� �������������' value='6'/>
  AType1 = 1; // "������������� �� �������
  AType2 = 1; // "������������� �� �������
  if (APlanType == 0)
   { // �������
    switch (APlanSupportType)
     {
     case 0: // "������������� �� ������� + �� ������"
       {
        AType1 = 1;
        AType2 = 3;
        break;
       }
     case 1: // "������������� �� �������"
       {
        AType1 = 1;
        AType2 = 1;
        break;
       }
     case 2: // "������������� �� ������"
       {
        AType1 = 3;
        AType2 = 3;
        break;
       }
     }
   }
  else if ((APlanType == 1) || (APlanType == 2) || (APlanType == 3) || (APlanType == 4))
   { // �����������
    switch (APlanSupportType)
     {
     case 0: // "������������� �� �����������"
       {
        AType1 = 2;
        AType2 = 2;
        break;
       }
     case 1: // "������������� �� ����������� + ��������������"
       {
        AType1 = 2;
        AType2 = 4;
        break;
       }
     case 2: // "��������������"
       {
        AType1 = 4;
        AType2 = 4;
        break;
       }
     }
   }
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TPlanLinker::StartCreatePlan(TJSONObject * AVal,
  TdsOnPlanCreateUnitPlanEvent AOnCreateUnitPlan)
 {
  CallLogBegin(__FUNC__);
  FDM->OnCreateUnitPlan = AOnCreateUnitPlan;
  UnicodeString RC = "error";
  try
   {
    FPCValue = (TJSONObject *)AVal->Clone();
    // (TJSONObject*)TJSONObject::ParseJSONValue(AVal->ToString());
    UnicodeString FPlanedPlanMonth = JSONToString(FPCValue, "planmonth", "");
    if (FPlanedPlanMonth.Length())
     Init(TDate(FPlanedPlanMonth));
    else
     Init(Date());
    if (Planing)
     throw EExecutionError("���������� ����������, ����������� ����.");
    if (FBkgPlanLinker)
     {
      FBkgPlanLinker->Terminate();
      Sleep(50);
      delete FBkgPlanLinker;
      FBkgPlanLinker = NULL;
     }
    FBkgPlanLinker = new TBackgroundPlanLinker(false, this);
    RC = "ok";
   }
  __finally
   {
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TPlanLinker::StopCreatePlan()
 {
  UnicodeString RC = "error";
  CallLogBegin(__FUNC__);
  try
   {
    if (FBkgPlanLinker && Planing)
     {
      CallSaveDebugLog(NULL);
      FTerminated = true;
      FPlaning    = false;
      delete FBkgPlanLinker;
      FBkgPlanLinker = NULL;
     }
    RC = "ok";
   }
  __finally
   {
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TPlanLinker::SyncCall(TdsSyncMethod AMethod)
 {
  if (FBkgPlanLinker)
   FBkgPlanLinker->ThreadSafeCall(AMethod);
  else
   AMethod();
 }
// ---------------------------------------------------------------------------
int __fastcall TPlanLinker::GetMaxProgress()
 {
  return FCommPlanCount;
 }
// ---------------------------------------------------------------------------
int __fastcall TPlanLinker::GetCurProgress()
 {
  return FCurrPlan;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TPlanLinker::GetCurStage() const
 {
  return FPCStage;
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::CallLogMsg(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogMsg);
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::SyncCallLogMsg()
 {
  if (FOnLogMessage)
   FOnLogMessage(FLogMsg, FLogFuncName, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::CallLogErr(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogErr);
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::SyncCallLogErr()
 {
  if (FOnLogError)
   FOnLogError(FLogMsg, FLogFuncName, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::CallLogBegin(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogBegin);
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::SyncCallLogBegin()
 {
  if (FOnLogBegin)
   FOnLogBegin(FLogFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::CallLogEnd(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogEnd);
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::SyncCallLogEnd()
 {
  if (FOnLogEnd)
   FOnLogEnd(FLogFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::CallSaveDebugLog(TObject * Sender)
 {
  SyncCall(& SyncCallSaveDebugLog);
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::SyncCallSaveDebugLog()
 {
  CallLogBegin(__FUNC__);
  // if (FOnSaveDebugLog)
  // FOnSaveDebugLog(FDebugLog->Log->AsXML);
  CallLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinker::UnionPriviv(TUnitPrePlan * ASrc, TUnitPrePlan * ADest, int APlanType)
 {
  // ASrc  - key : type.infcode type: 1 - priv, 2 - test, 3 - check
  // ADest - key : type.infcode type: 1 - priv, 2 - test, 3 - check, 4 - privbyinf[, 5 - testbyinf , 6 - checkbyinf] ?
  TUnitPriorPrePlan * tmpPrePlanPrivList = new TUnitPriorPrePlan;
  TUnitPriorPrePlan * FPrePlanPrivList = new TUnitPriorPrePlan;
  TUnitPrePlan * FPrePlanTestList = new TUnitPrePlan;
  TUnitPrePlan * FPrePlanCheckList = new TUnitPrePlan;
  TIntMap * FSortedMIBPList = new TIntMap;
  try
   {
    bool ContPlan, InStore, curMIBPPlanCompat, BrigExec, BrigPrior, FEmptyStor, FullNotCompat, CanAdd;
    TPrePlanRec * PrePlanPriv;
    TPrePlanRec * PrePlanTest;
    TPrePlanRec * PrePlanCheck;
    TIntListMap::iterator FRC, FInfRC;
    TIntMap * FMIBPList, *FMIBPInfList;
    bool FAllInf, FAlreadyInPlan;
    __int64 FMIBPCode, FPlanMIBPCode, FInfCode;
    UnicodeString StrMIBPCode;
    bool PrePlanMIBPMonoPres;
    int FMaxMIBPInfCount, FMaxVacPrior, FInfPrior, FVacPrior;
    UnicodeString FReason;
    BrigExec = false;
    switch (APlanType)
     {
     case 0: // [1] �������:       ������ ���� (������ ��� �������� � �����)
     case 1: // [2] �����������:   ����������� ���� (��� �������� � ���� ����������� ��������)
     case 2: // [3] �����������:   ������ ���� (������ ��� �������� � �����)
     case 5: // [6] ����������� ������������ (������������ �� �������)
      break;
     case 3: // [4] �����������:   ���� ��� ���������� ���������� (�����)
     case 4: // [5] �����������:   ���� ��� ���������� ���������� (�������� ��� ������ ��������)
       {
        BrigExec = true;
        break;
       }
     }
    // ������������ ��������������� ���� �� ������� �� ��������� ���� : ��������, �����, ��������
    // � ������ ������ ���������� ����������
    for (TUnitPrePlan::iterator i = ASrc->begin(); i != ASrc->end(); i++)
     {
      if (!BrigExec || BrigExec && i->second->Brig)
       { // ��������� ��� ��� ��� ������ ���������  -----------------------------
        if (i->second->PlanItemType == 1 /* �������� */)
         (*tmpPrePlanPrivList)[i->second->Prior * 100 + i->second->Inf] = i->second; // ��������� �� ����������
        else if (i->second->PlanItemType == 2 /* ����� */)
         (*FPrePlanTestList)[i->first] = i->second;
        else if (i->second->PlanItemType == 3 /* �������� */)
         (*FPrePlanCheckList)[i->first] = i->second;
       } // ---------------------------------------------------------------------
     }
    // �������� �� ���������������� ����� ��������
    // ��������������� ������ ������ �� ���������� ��������
    // �.�. :
    // .     ���� ������ ���������, �� �������� ������ ���������
    // .     ���� ������ �� ���������, �� �������� ������ �� ���������
    if (tmpPrePlanPrivList->size())
     BrigPrior = tmpPrePlanPrivList->begin()->second->Brig;
    for (TUnitPriorPrePlan::iterator i = tmpPrePlanPrivList->begin(); i != tmpPrePlanPrivList->end(); i++)
     {
      if (APlanType == 1)
       { // �����������:   ����������� ���� (���� � ���� � �� ����, �������� �� ����)
        if (!i->second->Brig)
         (*FPrePlanPrivList)[i->first] = i->second;
       }
      else
       {
        if (i->second->Brig == BrigPrior)
         (*FPrePlanPrivList)[i->first] = i->second;
       }
     }
    tmpPrePlanPrivList->clear();
    FTempPlanMIBPList.clear();
    // ��������, ������ -------------------------------------------------------
    dsPlanLogMessage("���� �� ���������(���������), �����: " + IntToStr((int)FPrePlanPrivList->size()));
    for (TUnitPriorPrePlan::iterator itPriv = FPrePlanPrivList->begin(); itPriv != FPrePlanPrivList->end(); itPriv++)
     { // ���� �� ���������(���������) ������
      PrePlanPriv = itPriv->second;
      FReason     = "";
      dsPlanLogMessage("�������� ��� " + FDM->ClsData->InfNameStr(PrePlanPriv->Inf) +
        "  --- ������ ----------------------------");
      ContPlan = !PrePlanPriv->InPlan;
      BrigExec = true;
      switch (APlanType)
       {
       case 0: // [1] �������:       ������ ���� (������ ��� �������� � �����)
       case 2: // [3] �����������:   ������ ���� (������ ��� �������� � �����)
       case 5: // [6] ����������� ������������ (������������ �� �������)
        break;
       case 1: // [2] �����������:   ����������� ���� (��� �������� � ���� ����������� ��������)
         {
          BrigExec = !PrePlanPriv->Brig;
          break;
         }
       case 3: // [4] �����������:   ���� ��� ���������� ���������� (�����)
       case 4: // [5] �����������:   ���� ��� ���������� ���������� (�������� ��� ������ ��������)
         {
          BrigExec = PrePlanPriv->Brig;
          break;
         }
       }
      if (ContPlan && BrigExec)
       {
        if (FDM->ClsData->GetInfMIBPList(PrePlanPriv->Inf, FMIBPList))
         { // �������� ������ ������ �� �������� <PrePlanPriv->Inf>
          FSortedMIBPList->clear();
          FDM->SortByCountAndPrior(FMIBPList, FSortedMIBPList);
          FPlanMIBPCode       = -1;
          FMaxMIBPInfCount    = 0;
          FMaxVacPrior        = 9999;
          PrePlanMIBPMonoPres = false;
          if (PrePlanPriv->MIBP)
           {
            if (FDM->ClsData->GetMIBPInfCount(PrePlanPriv->MIBP) == 1)
             {
              if (PrePlanPriv->MIBPType == 2)
               { // ���������� ������ ������������ �� �����
                PrePlanMIBPMonoPres = true;
               }
              else
               {
                PrePlanMIBPMonoPres = FDM->MIBPInStore(PrePlanPriv->MIBP) && CheckMIBPPlanCompat(PrePlanPriv->MIBP);
               }
             }
           }
          FEmptyStor = true;
          FullNotCompat = true;
          for (TIntMap::iterator itMIBP = FSortedMIBPList->begin(); itMIBP != FSortedMIBPList->end(); itMIBP++)
           { // ���� �� ���� ��� �������� <PrePlanPriv->Inf>
            // MIBPInDefStore(__int64 AMIBPCode);
            FReason           = "";
            FMIBPCode         = itMIBP->second;
            curMIBPPlanCompat = false;
            ContPlan          = false;
            if (FDM->CheckMIBPByAge(PrePlanPriv->PatBirthDay, PrePlanPriv->PrePlanDate, FMIBPCode))
             {
              if (FDM->MIBPInStore(FMIBPCode))
               {
                FEmptyStor        = false;
                curMIBPPlanCompat = CheckMIBPPlanCompat(FMIBPCode);
                FullNotCompat &= !curMIBPPlanCompat;
                if (!curMIBPPlanCompat)
                 FReason = FDM->ClsData->VacNameStr(FMIBPCode) + " ������������ � ���������� �����.";
                else
                 ContPlan = true;
               }
              else
               FReason = FDM->ClsData->VacNameStr(FMIBPCode) + " ����������� � �������.";
             }
            else
             FReason = FDM->ClsData->VacNameStr(FMIBPCode) + " ���������� ����������� �� �������� ����������.";
            if (ContPlan)
             { // ������� ���������� � ���� � �������
              FVacPrior = 9999;
              if (FDM->ClsData->GetMIBPInfList(FMIBPCode, FMIBPInfList))
               { // �������� ������ �������� ������� <itMIBP->second>
                FAllInf        = true;
                FAlreadyInPlan = false;
                switch (FMIBPInfList->size())
                 {
                 case 0: // ������, ��� �������� ��� �������
                   {
                    dsPlanLogError("   ������� " + FDM->ClsData->VacNameStr(FMIBPCode) +
                      ", ����������� ������ ��������.", __FUNC__);
                    break;
                   }
                 case 1: // �����������
                   {
                    // **********   �����������  begin ************************************
                    // �����������, ������� ��������� � ���� ����:
                    // ����� ������ �� ����������� �
                    // (
                    // .  �� ������� ������� � ��������������� �����
                    // .    ���
                    // .  (
                    // .    ������� == ������� ���������������� ����� � ��� �����������
                    // .      ���
                    // .    ������� ���������������� ����� - �����������
                    // .  )
                    // )
                    if (!FMaxMIBPInfCount)
                     { // ����� ������ �� �����������
                      CanAdd = !PrePlanMIBPMonoPres;
                      if (!CanAdd)
                       { // � ��������������� ����� ����������� � � ���� ������ ���� ������ ���
                        CanAdd = (PrePlanPriv->MIBP == FMIBPCode);
                       }
                      if (CanAdd)
                       {
                        FMaxMIBPInfCount = 1;
                        FMaxVacPrior     = PrePlanPriv->Prior;
                        FPlanMIBPCode    = FMIBPCode;
                        dsPlanLogMessage("   " + FDM->ClsData->InfNameStr(PrePlanPriv->Inf) + " ���������� " +
                          FDM->ClsData->VacNameStr(FPlanMIBPCode));
                       }
                     }
                    // **********   �����������  end   ************************************
                    break;
                   }
                 default: // �����������
                   {
                    // **********   �����������  begin ************************************
                    dsPlanLogMessage("  ���� �� ��������� " + FDM->ClsData->VacNameStr(FMIBPCode));
                    // ��������� ��� ���� �������� ������� :
                    // 1. ������� � ��������������� �����
                    // 2. ���������� � ����������� �����
                    // ���������� ��������� ������� = max (��������� �������� �������)
                    for (TIntMap::iterator itInf = FMIBPInfList->begin();
                    (itInf != FMIBPInfList->end()) && FAllInf && !FAlreadyInPlan; itInf++)
                     { // ���� �� ��������� ����
                      FAllInf &= InfInPlanVac(FPrePlanPrivList, itInf->first, FInfPrior, FAlreadyInPlan);
                      if (FAllInf && !FAlreadyInPlan && (FInfPrior < FVacPrior) && (itInf->first != PrePlanPriv->Inf))
                       FVacPrior = FInfPrior;
                     }
                    if (FAllInf && !FAlreadyInPlan && ((unsigned)FMaxMIBPInfCount < FMIBPInfList->size()))
                     { // ��� �������� ���� � ��������������� �����
                      // � ��� � ����������� �����
                      FMaxMIBPInfCount = FMIBPInfList->size();
                      if (FMaxVacPrior == PrePlanPriv->Prior)
                       { // �� ������ ������� ���� �����������, �������������� ��������� �� ��������� ���������
                        FMaxVacPrior  = FVacPrior;
                        FPlanMIBPCode = FMIBPCode;
                       }
                      else if (FVacPrior < FMaxVacPrior)
                       {
                        FMaxVacPrior  = FVacPrior;
                        FPlanMIBPCode = FMIBPCode;
                        dsPlanLogMessage("   " + FDM->ClsData->InfNameStr(PrePlanPriv->Inf) + " ���������� " +
                          FDM->ClsData->VacNameStr(FPlanMIBPCode));
                       }
                     }
                    else
                     {
                      if (!FAllInf)
                       dsPlanLogWarning("   ������� " + FDM->ClsData->VacNameStr(FMIBPCode) +
                        " ��������� �� �����, �������: �� ��� �������� ������� ������������ � �����.", __FUNC__);
                      else if (FAlreadyInPlan)
                       dsPlanLogWarning("   ������� " + FDM->ClsData->VacNameStr(FMIBPCode) +
                        " ��������� �� �����, �������: �������� ������� ��������� ������������, ��� �������������� � �����.",
                        __FUNC__);
                     }
                    // **********   �����������  end   ************************************
                   }
                 }
               }
             }
            else
             { // ������� ������������ �/��� ����������� � �������
              dsPlanLogWarning("   " + FReason, __FUNC__);
             }
           } // ���� �� ���� ��� �������� <PrePlanPriv->Inf> �����
          if (FPlanMIBPCode != -1)
           { // ��������� ������� �� ��������� ������, �������� ������� ������� �� ������  FPrivList
            FTempPlanMIBPList[PrePlanPriv->Prior * 100 + PrePlanPriv->Inf] = FPlanMIBPCode;
            dsPlanLogMessage("  �������� �� ��������� ������ " + FDM->ClsData->InfNameStr(PrePlanPriv->Inf) +
              " ���������� " + FDM->ClsData->VacNameStr(FPlanMIBPCode));
            if (FDM->ClsData->GetMIBPInfList(FPlanMIBPCode, FMIBPInfList))
             {
              // dsPlanLogMessage("  FDM->GetMIBPInfList(FPlanMIBPCode, FMIBPInfList) "+IntToStr((int)FMIBPInfList->size()));
              for (TUnitPriorPrePlan::iterator eraseItem = FPrePlanPrivList->begin();
              eraseItem != FPrePlanPrivList->end(); eraseItem++)
               {
                if (FMIBPInfList->find(eraseItem->second->Inf) != FMIBPInfList->end())
                 {
                  eraseItem->second->InPlan = true;
                  // dsPlanLogMessage("  �������� �������� "+InfList[IntToStr(eraseItem->second->Inf)]+" ��� ����������� � ���� �� "+FDM->ClsData->VacNameStr(FPlanMIBPCode));
                 }
               }
             }
           }
          else
           { // ��� ������ � ������� �� ��������� ��������
            if (FEmptyStor)
             { // �� ������ ����������� ������� �� ��������, �������� ���������� �� �����������
              FTempPlanMIBPList[PrePlanPriv->Prior * 100 + PrePlanPriv->Inf] = -1 * PrePlanPriv->Inf;
              dsPlanLogMessage("  �������� �� ��������� ������ " + FDM->ClsData->InfNameStr(PrePlanPriv->Inf) +
                " ��� �������� �������");
              PrePlanPriv->InPlan = true;
              if (FMIBPList->size())
               dsPlanLogWarning("  �������� �� �������� " + FDM->ClsData->InfNameStr(PrePlanPriv->Inf) +
                " ��������� � ���� ��� �������� �������, ������� ��. ����.", __FUNC__);
              else
               dsPlanLogWarning("  �� ������ ����������� ������� ��� �������� " + FDM->ClsData->InfNameStr
                (PrePlanPriv->Inf) + ", �������� ��������� � ���� ��� �������� �������.", __FUNC__);
             }
            else
             {
              if (FullNotCompat)
               {
                if (FMIBPList->size())
                 dsPlanLogWarning("  �������� �� �������� " + FDM->ClsData->InfNameStr(PrePlanPriv->Inf) +
                  " ��������� �� �����, ������� ��. ����.", __FUNC__);
                else
                 dsPlanLogWarning("  �� ������ ����������� ������� ��� �������� " + FDM->ClsData->InfNameStr
                  (PrePlanPriv->Inf) + ", �������� ��������� �� �����.", __FUNC__);
               }
              else
               {
                FTempPlanMIBPList[PrePlanPriv->Prior * 100 + PrePlanPriv->Inf] = -1 * PrePlanPriv->Inf;
                dsPlanLogMessage("  �������� �� ��������� ������ " + FDM->ClsData->InfNameStr(PrePlanPriv->Inf) +
                  " ��� �������� �������");
                PrePlanPriv->InPlan = true;
                if (FMIBPList->size())
                 dsPlanLogWarning("  �������� �� �������� " + FDM->ClsData->InfNameStr(PrePlanPriv->Inf) +
                  " ��������� � ���� ��� �������� �������, ������� ��. ����.", __FUNC__);
                else
                 dsPlanLogWarning("  �� ������ ����������� ������� ��� �������� " + FDM->ClsData->InfNameStr
                  (PrePlanPriv->Inf) + ", �������� ��������� � ���� ��� �������� �������.", __FUNC__);
               }
             }
           }
         }
        else
         dsPlanLogError("  �� ������� ������� ��� �������� " + FDM->ClsData->InfNameStr(PrePlanPriv->Inf), __FUNC__);
       }
      else
       {
        if (ContPlan)
         dsPlanLogWarning("  �������� " + FDM->ClsData->InfNameStr(PrePlanPriv->Inf) +
          " ������������ � ������������ � ����������� �����, ��� ����� - " + IntToStr(APlanType) + ".");
        else
         dsPlanLogWarning("  �������� " + FDM->ClsData->InfNameStr(PrePlanPriv->Inf) +
          " ������������. �������: ���������� ������������.");
       }
      dsPlanLogMessage("�������� ��� " + FDM->ClsData->InfNameStr(PrePlanPriv->Inf) +
        "  --- ��������� ----------------------------");
     } // ���� �� ���������(���������) �����
    if (FTempPlanMIBPList.size())
     { // � ���� ���� ������
      TUnitPrePlan::iterator FPPItem, FDestItem;
      for (TIntMap::iterator p = FTempPlanMIBPList.begin(); p != FTempPlanMIBPList.end(); p++)
       {
        FMIBPCode   = p->second;
        StrMIBPCode = IntToStr(FMIBPCode);
        if (FMIBPCode > 0)
         { // ���� ����
          if (FDM->ClsData->GetMIBPInfList(FMIBPCode, FMIBPInfList))
           {
            for (TIntMap::iterator itInf = FMIBPInfList->begin(); itInf != FMIBPInfList->end(); itInf++)
             {
              FPPItem = ASrc->find("1." + IntToStr(itInf->first));
              if (FPPItem != ASrc->end())
               {
                FDestItem = ADest->find("1." + StrMIBPCode);
                if (FDestItem == ADest->end())
                 {
                  (*ADest)["1." + StrMIBPCode] = new TPrePlanRec(FPPItem->second);
                  (* ADest)["1." + StrMIBPCode]->MIBP = FMIBPCode;
                  (* ADest)["1." + StrMIBPCode]->Inf = 0;
                  (*ADest)["1." + StrMIBPCode]->TypeList[FPPItem->second->Inf] = FPPItem->second->VacType;
                 }
                else
                 {
                  FDestItem->second->AllVacType = FDestItem->second->AllVacType + FPPItem->second->InfVacType();
                  FDestItem->second->TypeList[FPPItem->second->Inf] = FPPItem->second->VacType;
                 }
               }
             }
           }
          dsPlanLogMessage("�������� " + FDM->ClsData->VacNameStr(FMIBPCode));
         }
        else
         { // ���� ���
          // ��������� �������� ��� ����
          FInfCode = FMIBPCode;
          UnicodeString FtmpInfCode = IntToStr(-1 * FInfCode);
          FPPItem = ASrc->find("1." + FtmpInfCode);
          if (FPPItem != ASrc->end())
           {
            FDestItem = ADest->find("4." + FtmpInfCode);
            if (FDestItem == ADest->end())
             {
              (*ADest)["4." + FtmpInfCode] = new TPrePlanRec(FPPItem->second);
              (* ADest)["4." + FtmpInfCode]->MIBP = 0;
              dsPlanLogMessage("�������� �������� �� " + FDM->ClsData->InfNameStr(-1 * FInfCode));
             }
            else
             dsPlanLogMessage("�������� �� �������� " + FDM->ClsData->InfNameStr(-1 * FInfCode) +
              " ��� ������������ � �����.");
           }
         }
       }
     }
    // ��������, ����� --------------------------------------------------------
    // ����� ------------------------------------------------------------------
    for (TUnitPrePlan::iterator itTest = FPrePlanTestList->begin(); itTest != FPrePlanTestList->end(); itTest++)
     {
      PrePlanTest = itTest->second;
      dsPlanLogMessage("�������� ��� " + FDM->ClsData->TestName[PrePlanTest->MIBP] +
        "  --- ������ ----------------------------");
      ContPlan = ADest->find(itTest->first) == ADest->end();
      BrigExec = true;
      switch (APlanType)
       {
       case 0: // [1] �������:       ������ ���� (������ ��� �������� � �����)
       case 2: // [3] �����������:   ������ ���� (������ ��� �������� � �����)
       case 5: // [6] ����������� ������������ (������������ �� �������)
        break;
       case 1: // [2] �����������:   ����������� ���� (��� �������� � ���� ����������� ��������)
         {
          BrigExec = !PrePlanTest->Brig;
          break;
         }
       case 3: // [4] �����������:   ���� ��� ���������� ���������� (�����)
       case 4: // [5] �����������:   ���� ��� ���������� ���������� (�������� ��� ������ ��������)
         {
          BrigExec = PrePlanTest->Brig;
          break;
         }
       }
      if (ContPlan && BrigExec)
       {
        (*ADest)[itTest->first] = new TPrePlanRec(PrePlanTest);
        (*ADest)[itTest->first]->Inf = PrePlanTest->Inf; // ���� 0
       }
      else
       {
        if (ContPlan)
         dsPlanLogMessage("  ����� " + FDM->ClsData->TestName[PrePlanTest->MIBP] +
          " ������������ � ������������ � ����������� �����, ��� ����� - " + IntToStr(APlanType) + ".");
       }
      dsPlanLogMessage("�������� ��� " + FDM->ClsData->TestName[PrePlanTest->MIBP] +
        "  --- ��������� ----------------------------");
      /*
       if (ADest->find(itTest->first) == ADest->end())
       {
       (*ADest)[itTest->first] = new TPrePlanRec(PrePlanTest);
       (* ADest)[itTest->first]->Inf = 0;
       }
       */
     }
    // �������� ------------------------------------------------------------------
    for (TUnitPrePlan::iterator itCheck = FPrePlanCheckList->begin(); itCheck != FPrePlanCheckList->end(); itCheck++)
     {
      // TPrePlanRec * PrePlanCheck;
      if (ADest->find(itCheck->first) == ADest->end())
       {
        (*ADest)[itCheck->first] = new TPrePlanRec(itCheck->second);
        (* ADest)[itCheck->first]->Inf = 0;
       }
     }
   }
  __finally
   {
    delete FSortedMIBPList;
    delete tmpPrePlanPrivList;
    delete FPrePlanPrivList;
    delete FPrePlanTestList;
    delete FPrePlanCheckList;
   }
 }
// ----------------------------------------------------------------------------
int __fastcall TPlanLinker::AddUnitPlan(TFDQuery * AQ, __int64 APlanCode, TUnitPrePlan * AUnitPrePlan, int APlanType)
 {
  int RC = 0;
  TIntMap * FMIBPInfList;
  TUnitPrePlan * FUnitPlanPriv = new TUnitPrePlan;
  try
   {
    UnionPriviv(AUnitPrePlan, FUnitPlanPriv, APlanType);
    if (FUnitPlanPriv->size())
     RC = 1;
    try
     {
      if (!AQ->Transaction->Active)
       AQ->Transaction->StartTransaction();
      // ########################################################################
      // #                                                                      #
      // #     ����                                                             #
      // #                                                                      #
      // ########################################################################
      AQ->SQL->Text =
        "UPDATE OR INSERT INTO class_3084 (code, cguid, R328F,   R32B2,   R317D,   R32FB,   R3298,   R32FE,   R3299,   R329A,   R3087,   R32FA,   R3088,   R3086,   R3089,   R32AB,   R308B,   R3291,   R308C,   R3098,   R308D, R32A7,   R32FD,   R32FC,   R32FF, R3308, R32A5) " "values (:pcode, :pcguid, :pR328F, :pR32B2, :pR317D, :pR32FB, :pR3298, :pR32FE, :pR3299, :pR329A, :pR3087, :pR32FA, :pR3088, :pR3086, :pR3089, :pR32AB, :pR308B, :pR3291, :pR308C, :pR3098, :pR308D, :pR32A7, :pR32FD, :pR32FC, :pR32FF, :pR3308, :pR32A5) MATCHING (CODE)";
      AQ->Prepare();
      AQ->ParamByName("pR328F")->Value = APlanCode; // '��� �����' uid='328F'
      AQ->ParamByName("pR32B2")->Value = APlanType + 1;
      for (TUnitPrePlan::iterator i = FUnitPlanPriv->begin(); i != FUnitPlanPriv->end(); i++)
       {
        AQ->ParamByName("pCode")->Value = FDM->GetNewCode("3084");
        AQ->ParamByName("pcguid")->Value = FDM->NewGUID();
        // �������
        AQ->ParamByName("pR317D")->Value = i->second->UCode;
        // extedit name='��� ��������' uid='017D' ref='1000' depend='017D.017D' isedit='0' fieldtype='integer'/>
        AQ->ParamByName("pR32FB")->Value = i->second->SupportState;
        // choice name='������ ������������' uid='32FB' required='1
        AQ->ParamByName("pR3298")->Value = i->second->OrgLev;
        // text name='������ �����������' uid='3298' inlist='l' cast='no' length='30'/>
        AQ->ParamByName("pR32FE")->Value = i->second->Fam; // text name='�������' uid='32FE' inlist='l' length='30'/>
        AQ->ParamByName("pR3299")->Value = i->second->Name; // text name='���' uid='3299' inlist='l' length='20'/>
        AQ->ParamByName("pR329A")->Value = i->second->SName; // text name='��������' uid='329A' inlist='l' length='30'/>
        // ����
        AQ->ParamByName("pR3087")->Value = i->second->PlanMonth;
        // date name='���� ��' uid='1087' required='1' inlist='e'/>
        AQ->ParamByName("pR32FA")->Value = i->second->Src;
        // choice name='�������� �����' uid='32FA' inlist='l' default='1'
        AQ->ParamByName("pR3088")->Value = i->second->PrePlanDate;
        // date name='���� �����' uid='1088' required='1' inlist='l'/>
        AQ->ParamByName("pR3086")->Value = i->second->PlanItemType;
        // binary name='����/�����/��������?' uid='1086' required='1'/>
        AQ->ParamByName("pR3089")->Value = i->second->MIBP;
        // _UID(i->first).ToIntDef(-1); // choiceDB name='����' uid='1089' ref='0035' required='1' inlist='l'>
        AQ->ParamByName("pR32AB")->Value = i->second->Inf; // choiceDB name='��������' uid='32AB' ref='003A' inlist='l
        AQ->ParamByName("pR308B")->Value = i->second->AllVacType;
        // extedit name='���/��������' uid='108B' required='1' inlist='e' length='255'/>
        AQ->ParamByName("pR3291")->Value = i->second->Brig; // binary name='��������� ����������' uid='3191'/>
        AQ->ParamByName("pR308C")->Value = i->second->Prior;
        // digit name='���������' uid='108C' required='1' digits='3' min='0' max='999'/>
        // ����������
        AQ->ParamByName("pR3098")->Value = 0;
        // binary name='��������/�����/�������� ���������' uid='3098' required='1' inlist='e'/>
        AQ->ParamByName("pR308D")->Value = Variant::Empty();
        // <date name='���� ����������' uid='308D' inlist='e' default='CurrentDate'>
        AQ->ParamByName("pR32A7")->Value = Variant::Empty();
        // binary name='��������� ������ ��������' uid='32A7' required='1' inlist='l'>
        AQ->ParamByName("pR32FD")->Value = Variant::Empty();
        // binary name='��������� � ������ ���' uid='32FD' required='1' inlist='l'>
        AQ->ParamByName("pR32FC")->Value = 1; // choice name='������� ������������' uid='32FC' inlist='l' default='1'>
        AQ->ParamByName("pR32FF")->Value = Variant::Empty();
        // <extedit name='�����������' uid='32FF' comment='search' required='1' inlist='l' linecount='3' showtype='1' length='255'>
        AQ->ParamByName("pR3308")->Value = 0; // <binary name='����������� ����' uid='3308'/>
        AQ->ParamByName("pR32A5")->Value = i->second->PatBirthDay;
        // <date name='�� ��������' uid='32A5' isedit='0' default='patbd'/>
        AQ->OpenOrExecute();
       }
      // ########################################################################
      // #                                                                      #
      // #     ���� �� ���������                                                #
      // #                                                                      #
      // ########################################################################
      AQ->SQL->Text =
        "UPDATE OR INSERT INTO class_1084 (code, cguid, R318F,   R32B1,   R017D,   R3301,   R3306,   R3198,   R3199,   R319A,   R3300,   R1087,   R1088,   R32A6,   R1086,   R1089,  R32AC,   R108B,   R3191,   R108C,   R1098,   R108D,   R3305,   R32AA,   R3302,   R3303,   R3304, R3307, R31A5) " "values (:pcode, :pcguid, :pR318F, :pR32B1, :pR017D, :pR3301, :pR3306, :pR3198, :pR3199, :pR319A, :pR3300, :pR1087, :pR1088, :pR32A6, :pR1086, :pR1089, :pR32AC, :pR108B, :pR3191, :pR108C, :pR1098, :pR108D, :pR3305, :pR32AA, :pR3302, :pR3303, :pR3304, :pR3307, :pR31A5) MATCHING (CODE)";
      AQ->Prepare();
      AQ->ParamByName("pR318F")->Value = APlanCode; // '��� �����' uid='318F'
      AQ->ParamByName("pR32B1")->Value = APlanType + 1;
      // __int64 FVacCode;
      for (TUnitPrePlan::iterator i = FUnitPlanPriv->begin(); i != FUnitPlanPriv->end(); i++)
       {
        // �������
        AQ->ParamByName("pR017D")->Value = i->second->UCode;
        // extedit name='��� ��������' uid='017D' ref='1000' depend='017D.017D' isedit='0' fieldtype='integer'/>
        AQ->ParamByName("pR3301")->Value = i->second->SupportState;
        // choice name='������ ������������' uid='3301' required='1' inlist='l' default='0'>
        AQ->ParamByName("pR3306")->Value = i->second->OrgLev;
        // text name='������ �����������' uid='3306' inlist='l' cast='no' length='30'/>
        AQ->ParamByName("pR3198")->Value = i->second->Fam; // text name='�������' uid='3198' inlist='l' length='30'/>
        AQ->ParamByName("pR3199")->Value = i->second->Name; // text name='���' uid='3199' inlist='l' length='20'/>
        AQ->ParamByName("pR319A")->Value = i->second->SName; // text name='��������' uid='319A' inlist='l' length='30'/>
        // ����
        AQ->ParamByName("pR3300")->Value = i->second->Src;
        // choice name='�������� �����' uid='3300' inlist='l' default='1'>
        AQ->ParamByName("pR1087")->Value = i->second->PlanMonth;
        // date name='�������� ������' uid='1087' required='1' inlist='e'/>
        AQ->ParamByName("pR1088")->Value = i->second->PrePlanDate;
        // date name='���� �����' uid='1088' required='1' inlist='l'/>
        // AQ->ParamByName("pR32A6")->Value =
        AQ->ParamByName("pR1086")->Value = i->second->PlanItemType;
        // binary name='����/�����/��������?' uid='1086' required='1'/>
        AQ->ParamByName("pR1089")->Value = i->second->MIBP;
        // FVacCode;                // choiceDB name='����' uid='1089' ref='0035' required='1' inlist='l'>
        AQ->ParamByName("pR32AC")->Value = !i->second->MIBP; // binary name='����������� �������' uid='32AC'/>
        // AQ->ParamByName("pR108B")->Value = i->second->VacType;    // extedit name='���/��������' uid='108B' required='1' inlist='e' length='255'/>
        AQ->ParamByName("pR3191")->Value = i->second->Brig; // binary name='��������� ����������' uid='3191'/>
        AQ->ParamByName("pR108C")->Value = i->second->Prior;
        // digit name='���������' uid='108C' required='1' digits='3' min='0' max='999'/>
        // ����������
        AQ->ParamByName("pR1098")->Value = 0;
        // binary name='��������/�����/�������� ���������' uid='1098' required='1' inlist='e'/>
        AQ->ParamByName("pR108D")->Value = Variant::Empty();
        // date name='���� ����������' uid='108D' inlist='e' default='CurrentDate'>
        AQ->ParamByName("pR3305")->Value = Variant::Empty();
        // binary name='��������� ������ ��������' uid='3305' required='1' inlist='l'>
        AQ->ParamByName("pR32AA")->Value = Variant::Empty();
        // extedit name='���� ����������' uid='32AA' ref='0035' inlist='l' fieldtype='integer'>
        AQ->ParamByName("pR3302")->Value = Variant::Empty();
        // binary name='��������� � ������ ���' uid='3302' required='1' inlist='l'>
        AQ->ParamByName("pR3303")->Value = 1; // choice name='������� ������������' uid='3303' inlist='l' default='1'>
        AQ->ParamByName("pR3304")->Value = Variant::Empty();
        // extedit name='�����������' uid='3304' comment='search' required='1' inlist='l' linecount='3' showtype='1' length='255'>
        AQ->ParamByName("pR3307")->Value = 0; // <binary name='����������� ����' uid='3307' inlist='l'/>
        AQ->ParamByName("pR31A5")->Value = i->second->PatBirthDay;
        // date name='�� ��������' uid='31A5' isedit='0' default='patbd'/>
        if (i->second->MIBP && (i->second->PlanItemType == 1) /* �������� */)
         { // ���� ����
          if (FDM->ClsData->GetMIBPInfList(i->second->MIBP, FMIBPInfList))
           {
            // i->second->AllVacType
            for (TIntMap::iterator itInf = FMIBPInfList->begin(); itInf != FMIBPInfList->end(); itInf++)
             { // ���� �� ��������� ����
              AQ->ParamByName("pCode")->Value = FDM->GetNewCode("1084");
              AQ->ParamByName("pcguid")->Value = FDM->NewGUID();
              AQ->ParamByName("pR32A6")->Value = itInf->first;
              AQ->ParamByName("pR108B")->Value = i->second->TypeList[itInf->first];
              // extedit name='���/��������' uid='108B' required='1' inlist='e' length='255'/>
              AQ->OpenOrExecute();
             }
           }
         }
        else
         { // �� ���� ����
          AQ->ParamByName("pCode")->Value = FDM->GetNewCode("1084");
          AQ->ParamByName("pcguid")->Value = FDM->NewGUID();
          AQ->ParamByName("pR32A6")->Value = i->second->Inf;
          AQ->ParamByName("pR108B")->Value = i->second->VacType;
          // extedit name='���/��������' uid='108B' required='1' inlist='e' length='255'/>
          AQ->OpenOrExecute();
         }
        // AQ->ParamByName("pR1085")->Value = i->second->PrePlanDate;  // choice name='������' uid='1085' required='1' inlist='l' default='0'>
       }
      if (AQ->Transaction->Active)
       AQ->Transaction->Commit();
     }
    catch (Exception & E)
     {
      dsPlanLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    for (TUnitPrePlan::iterator i = AUnitPrePlan->begin(); i != AUnitPrePlan->end(); i++)
     delete i->second;
    AUnitPrePlan->clear();
    delete FUnitPlanPriv;
   }
  return RC;
 }
// ----------------------------------------------------------------------------
bool __fastcall TPlanLinker::CheckMIBPPlanCompat(__int64 AMIBPCode)
 { // �������� ������������� ���� � ���� � �����
  bool RC = false;
  try
   {
    bool Comp = true;
    for (TIntMap::iterator itVac = FTempPlanMIBPList.begin(); (itVac != FTempPlanMIBPList.end()) && Comp; itVac++)
     {
      Comp &= CheckCompatible(itVac->second, AMIBPCode);
     }
    RC = Comp;
    if (!RC)
     dsPlanLogMessage(FDM->ClsData->VacNameStr(AMIBPCode) + " ������������ c ��������� ���������� �����.");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TPlanLinker::InfInPlanVac(TUnitPriorPrePlan * APlanVac, int AInfCode, int & AInfPrior,
  bool & AAlreadyInPlan)
 { // ������� �������� � ��������������� ����� ��������
  bool RC = false;
  AInfPrior = -2;
  try
   {
    // dsPlanLogMessage("InfInPlanVac: "+IntToStr((int)APlanVac->size())+" : "+InfNameStr(AInfCode));
    TUnitPriorPrePlan::iterator FPItem;
    for (FPItem = APlanVac->begin(); (FPItem != APlanVac->end()) && !RC; FPItem++)
     {
      // dsPlanLogMessage("InfInPlanVac: "+InfNameStr(FPItem->second->Inf));
      AAlreadyInPlan = FPItem->second->InPlan;
      RC             = (FPItem->second->Inf == AInfCode);
      if (RC)
       {
        if (FPItem->second->MIBPType)
         {
          RC = false;
          break;
         }
        else
         AInfPrior = FPItem->second->Prior;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TPlanLinker::CheckCompatible(__int64 AMIBP1, __int64 AMIBP2)
 {
  // �������� �� ������������� 2-� ������
  bool RC = true;
  try
   {
    UnicodeString FCode1 = IntToStr(AMIBP1);
    UnicodeString FCode2 = IntToStr(AMIBP2);
    dsPlanLogMessage("�������� �� ������������� " + FDM->ClsData->VacNameStr(AMIBP1) + " -> " + FDM->ClsData->VacNameStr
      (AMIBP2));
    TTagNode * FIncompNode = FDM->PlanOpt->GetChildByName("incompMIBP", true);
    if (FIncompNode)
     {
      // dsPlanLogMessage(" ����� FIncompNode");
      TTagNode * itNode = FIncompNode->GetFirstChild();
      while (itNode)
       {
        // dsPlanLogMessage(" ����� �� FIncompNode");
        if (itNode->CmpAV("objref1", FCode1))
         {
          if (itNode->CmpAV("objref2", "-1," + FCode2))
           RC = false;
         }
        else if (itNode->CmpAV("objref1", FCode2))
         {
          if (itNode->CmpAV("objref2", "-1," + FCode1))
           RC = false;
         }
        itNode = itNode->GetNext();
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TdsPlanGetStrValByCodeEvent __fastcall TPlanLinker::FGetOnGetOrgStr()
 {
  TdsPlanGetStrValByCodeEvent RC = NULL;
  if (FDM)
   RC = FDM->OnGetOrgStr;
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TPlanLinker::FSetOnGetOrgStr(TdsPlanGetStrValByCodeEvent AVal)
 {
  if (FDM)
   FDM->OnGetOrgStr = AVal;
 }
// ----------------------------------------------------------------------------
TdsOnPlanGetMIBPListEvent __fastcall TPlanLinker::FGetOnGetMIBPList()
 {
  TdsOnPlanGetMIBPListEvent RC = NULL;
  if (FDM)
   RC = FDM->OnGetMIBPList;
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TPlanLinker::FSetOnGetMIBPList(TdsOnPlanGetMIBPListEvent AVal)
 {
  if (FDM)
   FDM->OnGetMIBPList = AVal;
 }
// ----------------------------------------------------------------------------
