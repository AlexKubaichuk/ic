// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <System.DateUtils.hpp>
#include "dsSrvPlanDMUnit.h"
#include "dsSrvRegTemplate.h"
#include "OrgParsers.h"
#include "KLADRParsers.h"
#include "System.zip.hpp"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TdsSrvPlanDM * dsSrvPlanDM;
// const UnicodeString csXMLDateFmtStr = "dd.mm.yyyy";
// const wchar_t cchXMLDateSeparator = '.';
// const wchar_t cchXMLPeriodSeparator = '-';
// const wchar_t cchXMLDurationSeparator = '/';
// ---------------------------------------------------------------------------
__fastcall TdsSrvPlanDM::TdsSrvPlanDM(TComponent * Owner, TFDConnection * AConnection, TAxeXMLContainer * AXMLList,
  TdsCommClassData * AClsData) : TDataModule(Owner)
 {
  FConnection        = AConnection;
  FClsData           = AClsData;
  FRegDef            = new TTagNode;
  FUnitDataDef       = new TTagNode;
  FUnitDataDef->Name = "fld";
  FXMLList           = AXMLList;
  LoadPlanSetting();
  FPlanOptDefSch         = FPlanOpt->GetChildByName("infdef", true);
  FVacSchXML             = FXMLList->GetXML("12063611-00008cd7-cd89");
  FTestSchXML            = FXMLList->GetXML("001D3500-00005882-DC28");
  FCheckSchXML           = FXMLList->GetXML("569E1EB2-D0CFEF0F-BE9A");
  FMIBPAgeDef            = FVacSchXML->GetChildByName("mibpparams", true);
  FSQLCreator            = new TdsDocSQLCreator(FXMLList); /* FDM->ICSNom */
  FSQLCreator->OnGetSVal = FDocGetSVal;
  FImmNomQuery           = CreateTempQuery();
  FImmNom                = new TICSIMMNomDef(FImmNomQuery, FXMLList, "40381E23-92155860-4448", FClsData);
 }
// ---------------------------------------------------------------------------
__fastcall TdsSrvPlanDM::~TdsSrvPlanDM()
 {
  delete FImmNom;
  DeleteTempQuery(FImmNomQuery);
  delete FSQLCreator;
  delete FRegDef;
  delete FUnitDataDef;
  for (TdsSrvClassifDataMap::iterator i = FClassifData.begin(); i != FClassifData.end(); i++)
   delete i->second;
  FClassifData.clear();
 }
// ---------------------------------------------------------------------------
void _fastcall TdsSrvPlanDM::LoadPlanSetting()
 {
  TFDQuery * FQ = CreateTempQuery();
  TStringStream * FZipSch = new TStringStream;
  TTagNode * tmpPLS = new TTagNode;
  try
   {
    quExecParam(FQ, true, "Select FLD_VALUE from BINARY_DATA where Upper(NAME) = Upper(:pName)", "pName",
    "planSetting");
    if (FQ->RecordCount)
     {
      FZipSch->WriteData(FQ->FieldByName("FLD_VALUE")->AsBytes, FQ->FieldByName("FLD_VALUE")->AsBytes.Length);
      tmpPLS->Encoding = "utf-8";
      tmpPLS->SetZIPXML(FZipSch);
      FXMLList->Add(tmpPLS, "planSetting");
     }
    FPlanOpt = FXMLList->GetXML("planSetting");
   }
  __finally
   {
    delete tmpPLS;
    delete FZipSch;
    DeleteTempQuery(FQ);
   }
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlanDM::GetPlanTemplateDef(int AType)
 {
  UnicodeString RC = "<root/>";
  try
   {
    UnicodeString FFN = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\template\\pl" + IntToStr(AType) + ".xml");
    if (FileExists(FFN))
     {
      TTagNode * tmplNode = new TTagNode;
      try
       {
        tmplNode->LoadFromXMLFile(FFN);
        RC = tmplNode->AsXML;
       }
      __finally
       {
        delete tmplNode;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlanDM::FSetRegDef(TTagNode * AVal)
 {
  FRegDef->AsXML = AVal->AsXML;
  TTagNode * FUnitDef = FRegDef->GetTagByUID("1000");
  if (FUnitDef)
   {
    FUnitDataDef->Name = "fld";
    UnicodeString _EMPTYSTR_;
    FUnitDataDef->AddChild("code");
    FUnitDataDef->AddChild("data");
    FUnitDef->Iterate(FSetFieldsDef, _EMPTYSTR_);
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSrvPlanDM::FSetFieldsDef(TTagNode * ANode, UnicodeString & ATmp)
 {
  if (ANode->CmpName("binary,choice,choiceDB,choiceTree,text,date,extedit,digit"))
   FUnitDataDef->AddChild()->Assign(ANode, true);
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlanDM::GetSQLs(UnicodeString AId)
 {
  TdsSrvClassifSQLData * tmpData = new TdsSrvClassifSQLData;
  try
   {
    GetClassifSQLData(FRegDef, AId, tmpData);
    FClassifData[AId] = tmpData;
   }
  catch (Exception & E)
   {
    dsPlanLogError(E.Message + "; ID = " + AId, __FUNC__);
    throw E;
   }
  /*
   TdsSrvClassif *FSQLs = new TdsSrvClassif( "", "");
   try
   {
   FSelectSQLMap[AId]    = FSQLs->SQLs->SelectSQL;
   FClassSQLMap[AId]     = FSQLs->SQLs->ClassSQL;
   FCountSQLMap[AId]     = FSQLs->SQLs->CountSQL;
   FDeleteSQLMap[AId]    = FSQLs->SQLs->DeleteSQL;
   FSelectRecSQLMap[AId] = FSQLs->SQLs->SelectRecSQL;
   FKeyFlMap[AId]        = FSQLs->SQLs->KeyFl;
   FFields[AId] = new TStringList;
   FFields[AId]->Text = FSQLs->SQLs->Fields->Text;
   }
   __finally
   {
   delete FSQLs;
   }
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlanDM::CheckSQLs(UnicodeString AId)
 {
  try
   {
    TdsSrvClassifDataMap::iterator FRC = FClassifData.find(AId);
    if (FRC == FClassifData.end())
     GetSQLs(AId);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetKeyFl(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->KeyFl;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetFlNameById(UnicodeString ATabId, UnicodeString AFlId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(ATabId);
    RC = FClassifData[ATabId]->FieldNameById(AFlId);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetTabName(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->TabName;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetCountSQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->CountSQL;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetDeleteSQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->DeleteSQL;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetClassSQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->ClassSQL;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetSelectSQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->SelectSQL;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetOrderSQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->OrderSQL;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetSelectRecSQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->SelectRecSQL;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetSelectRec10SQL(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->SelectRec10SQL;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSrvPlanDM::IsQuotedKey(UnicodeString AId)
 {
  bool RC = false;
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->QuotedKey;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TStringList * __fastcall TdsSrvPlanDM::GetFieldsList(UnicodeString AId)
 {
  TStringList * RC = NULL;
  try
   {
    CheckSQLs(AId);
    RC = FClassifData[AId]->Fields;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TdsSrvPlanDM::GetCount(UnicodeString AId, System::UnicodeString AFilterParam)
 {
  __int64 RC = 0;
  TFDQuery * FQ = CreateTempQuery();
  TJSONObject * tmpFlData = NULL;
  TTagNode * tmpFlt = NULL;
  try
   {
    bool FFiltered = false;
    if (AFilterParam.Length())
     {
      if (AFilterParam[1] == '{')
       {
        tmpFlData = (TJSONObject *)TJSONObject::ParseJSONValue(AFilterParam);
        FFiltered = true;
       }
      else if (AFilterParam.Pos("<?xml"))
       {
        tmpFlt        = new TTagNode;
        tmpFlt->AsXML = AFilterParam;
        FFiltered     = true;
       }
     }
    if (FFiltered)
     {
      if (tmpFlt)
       {
        if (quExec(FQ, false, GetCountSQL(AId) + " where " + GetWhereFromFilter(tmpFlt, "0E291426-00005882-2493.0000")))
         RC = FQ->FieldByName("RCount")->AsInteger;
       }
      else
       {
        UnicodeString FFilterWhere = "";
        TJSONPairEnumerator * itPair = tmpFlData->GetEnumerator();
        while (itPair->MoveNext())
         {
          if (FFilterWhere.Length())
           FFilterWhere += " and ";
          // FFilterWhere += " r"+((TJSONString*)itPair->Current->JsonString)->Value()+"="+((TJSONString*)itPair->Current->JsonValue)->Value();
          FFilterWhere += " r" + JSONEnumPairCode(itPair) + "=" + JSONEnumPairValue(itPair);
         }
        if (quExec(FQ, false, GetCountSQL(AId) + " where " + FFilterWhere))
         RC = FQ->FieldByName("RCount")->AsInteger;
       }
     }
    else
     {
      if (quExec(FQ, false, GetCountSQL(AId)))
       RC = FQ->FieldByName("RCount")->AsInteger;
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvPlanDM::GetIdList(System::UnicodeString AId, int AMax,
  System::UnicodeString AFilterParam)
 {
  TJSONArray * RC = new TJSONArray;
  TFDQuery * FQ = CreateTempQuery();
  TJSONObject * tmpFlData = NULL;
  TTagNode * tmpFlt = NULL;
  try
   {
    bool FFiltered = false;
    if (AFilterParam.Length())
     {
      if (AFilterParam[1] == '{')
       {
        tmpFlData = (TJSONObject *)TJSONObject::ParseJSONValue(AFilterParam);
        FFiltered = true;
       }
      else if (AFilterParam.Pos("<?xml"))
       {
        tmpFlt        = new TTagNode;
        tmpFlt->AsXML = AFilterParam;
        FFiltered     = true;
       }
     }
    UnicodeString FSQL = "";
    if (FFiltered)
     {
      if (tmpFlt)
       FSQL = GetSelectSQL(AId) + " where " + GetWhereFromFilter(tmpFlt, "0E291426-00005882-2493.0000");
      else
       {
        UnicodeString FFilterWhere = "";
        TJSONPairEnumerator * itPair = tmpFlData->GetEnumerator();
        while (itPair->MoveNext())
         {
          if (FFilterWhere.Length())
           FFilterWhere += " and ";
          // FFilterWhere += " r"+((TJSONString*)itPair->Current->JsonString)->Value()+"="+((TJSONString*)itPair->Current->JsonValue)->Value();
          FFilterWhere += " r" + JSONEnumPairCode(itPair) + "=" + JSONEnumPairValue(itPair);
         }
        FSQL = GetSelectSQL(AId) + " where " + FFilterWhere;
       }
     }
    else
     FSQL = GetSelectSQL(AId);
    if (GetOrderSQL(AId).Length())
     FSQL += " Order by " + GetOrderSQL(AId); /* sort order */
    if (quExec(FQ, false, FSQL))
     {
      while (!FQ->Eof)
       {
        RC->Add(FQ->FieldByName(GetKeyFl(AId))->AsString);
        FQ->Next();
       }
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
    if (tmpFlt)
     delete tmpFlt;
   }
  return (TJSONObject *)RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvPlanDM::GetValById(System::UnicodeString AId, System::UnicodeString ARecId)
 {
  TJSONObject * RC = new TJSONObject;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    if (!AId.Pos("."))
     { // �������� ��� ������
      if (quExecParam(FQ, false, GetSelectRecSQL(AId), "p" + GetKeyFl(AId), ARecId))
       {
        if (FQ->RecordCount)
         {
          TTagNode * flDef;
          UnicodeString FId, FFN;
          TStringList * FFlList = GetFieldsList(AId);
          RC->AddPair(GetKeyFl(AId), ARecId);
          for (int i = 0; i < FFlList->Count; i++)
           {
            FId   = FFlList->Strings[i];
            flDef = FRegDef->GetTagByUID(FId);
            FFN   = "R" + FId;
            if (flDef->AV["fl"].Length())
             FFN = flDef->AV["fl"];
            if (FQ->FieldByName(FFN)->IsNull)
             {
              RC->AddPair(FId, new TJSONNull);
             }
            else
             {
              if (flDef->CmpName("choice, choiceDB"))
               {
                RC->AddPair(FId, TJSONObject::ParseJSONValue("{\"" + FQ->FieldByName(FFN)->AsString + "\":\"" +
                  FQ->FieldByName(FFN + "Str")->AsString + "\"}"));
               }
              else if (flDef->CmpName("binary"))
               {
                RC->AddPair(FId, TJSONObject::ParseJSONValue("{\"" + FQ->FieldByName(FFN)->AsString + "\":\"" +
                  UnicodeString((FQ->FieldByName(FFN)->AsInteger) ? "+" : "") + "\"}"));
               }
              else if (flDef->CmpName("choiceTree, extedit"))
               {
                if (!(flDef->CmpName("extedit") && flDef->CmpAV("fieldtype",
                  "integer") && (flDef->AV["length"].ToIntDef(0) > 64)))
                 RC->AddPair(FId, TJSONObject::ParseJSONValue("{\"" + FQ->FieldByName(FFN)->AsString + "\":\"" +
                  FQ->FieldByName(FFN)->AsString + "\"}"));
               }
              else
               RC->AddPair(FId, FQ->FieldByName(FFN)->AsString);
             }
           }
         }
       }
     }
    else
     { // �������� ����
      // uid.uid
      UnicodeString FCmd = GetLPartB(AId).LowerCase();
      UnicodeString FRefId = GetRPartB(AId);
      if (FCmd == "f")
       { // �������� �������� ���� Id2, �������������� Id1 � �����  ARecId
        TTagNode * flDef;
        UnicodeString Id1, Id2, FTab, FFN;
        Id1   = GetLPartB(FRefId).LowerCase();
        Id2   = GetRPartB(FRefId).LowerCase();
        flDef = FRegDef->GetTagByUID(Id1);
        if (flDef)
         {
          FTab = "class_" + flDef->AV["uid"];
          if (flDef->AV["tab"].Length())
           FTab = flDef->AV["tab"];
          flDef = FRegDef->GetTagByUID(Id2);
          if (flDef)
           {
            FFN = "R" + flDef->AV["uid"];
            if (flDef->AV["fl"].Length())
             FFN = flDef->AV["fl"];
            if (quExecParam(FQ, false, "Select (" + FFN + ") as FlVal From " + FTab + " Where " + GetKeyFl(Id1) +
              "=:FCode", "FCode", ARecId))
             {
              if (FQ->FieldByName("FlVal")->IsNull)
               RC->AddPair(Id2, new TJSONNull);
              else
               RC->AddPair(Id2, FQ->FieldByName("FlVal")->AsVariant);
             }
           }
         }
       }
      else if (FCmd == "l")
       { // ������ ���� ���.������
        if (quExec(FQ, false, GetClassSQL(FRefId) + " Order by name"))
         {
          while (!FQ->Eof)
           {
            RC->AddPair(FQ->FieldByName(GetKeyFl(FRefId))->AsString, FQ->FieldByName("name")->AsString);
            FQ->Next();
           }
         }
       }
      else if (FCmd == "d")
       { // ������ ���� ���.������ � �������������
        TTagNode * FClDef = FRegDef->GetTagByUID(GetLPartB(FRefId, ':'));
        if (FClDef)
         {
          UnicodeString FSQL = GetDependValSQL(FRefId, FClDef->CmpAV("comment", "empty"));
          if (FSQL.Length())
           FSQL += " Order by name";
          if (FSQL.Length() && FClDef)
           {
            if (quExec(FQ, false, FSQL))
             {
              while (!FQ->Eof)
               {
                RC->AddPair(FQ->FieldByName(GetKeyFl(FClDef->AV["ref"]))->AsString, FQ->FieldByName("name")->AsString);
                FQ->Next();
               }
             }
           }
         }
       }
      else if (FCmd == "s2")
       { // ������ ���� ���.������ � �������������
        UnicodeString FSQL = FRefId;
        if (FSQL.Length())
         {
          if (quExec(FQ, false, FSQL))
           {
            while (!FQ->Eof)
             {
              RC->AddPair(FQ->FieldByName("ps1")->AsString, FQ->FieldByName("ps2")->AsString);
              FQ->Next();
             }
           }
         }
       }
      else if (FCmd == "s3")
       { // ������ ���� ���.������ � �������������
        UnicodeString FSQL = FRefId;
        if (FSQL.Length())
         {
          if (quExec(FQ, false, FSQL))
           {
            while (!FQ->Eof)
             {
              RC->AddPair(FQ->FieldByName("ps1")->AsString, FQ->FieldByName("ps2")->AsString + ":" +
                FQ->FieldByName("ps3")->AsString);
              FQ->Next();
             }
           }
         }
       }
      else if (FCmd == "s4")
       { // ������ ���� ���.������ � �������������
        UnicodeString FSQL = FRefId;
        if (FSQL.Length())
         {
          if (quExec(FQ, false, FSQL))
           {
            while (!FQ->Eof)
             {
              RC->AddPair(FQ->FieldByName("ps1")->AsString, FQ->FieldByName("ps2")->AsString + ":" +
                FQ->FieldByName("ps3")->AsString + "#" + FQ->FieldByName("ps4")->AsString);
              FQ->Next();
             }
           }
         }
       }
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvPlanDM::GetValById10(System::UnicodeString AId, System::UnicodeString ARecId)
 {
  TJSONObject * RC = new TJSONObject;
  TJSONObject * RowRC;
  UnicodeString FRecId = ARecId;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    if (!AId.Pos("."))
     { // �������� ��� ������
      // *******************
      System::Sysutils::TReplaceFlags flRep;
      flRep << rfReplaceAll << rfIgnoreCase;
      if (IsQuotedKey(AId))
       {
        TStringList * QL = new TStringList;
        QL->Delimiter     = ',';
        QL->DelimitedText = FRecId;
        FRecId            = "";
        try
         {
          for (int i = 0; i < QL->Count; i++)
           {
            if (FRecId.Length())
             FRecId += ",";
            FRecId += "'" + QL->Strings[i].Trim() + "'";
           }
         }
        __finally
         {
          delete QL;
         }
       }
      // *******************
      if (quExec(FQ, false, StringReplace(GetSelectRec10SQL(AId), "_KEY_REP_VAL_", FRecId, flRep)))
       {
        TTagNode * flDef;
        UnicodeString FId, FFN;
        TStringList * FFlList = GetFieldsList(AId);
        while (!FQ->Eof)
         {
          RowRC = new TJSONObject;
          RowRC->AddPair("code", FQ->FieldByName(GetKeyFl(AId))->AsString);
          for (int i = 0; i < FFlList->Count; i++)
           {
            FId   = FFlList->Strings[i];
            flDef = FRegDef->GetTagByUID(FId);
            FFN   = "R" + FId;
            if (flDef->AV["fl"].Length())
             FFN = flDef->AV["fl"];
            if (FQ->FieldByName(FFN)->IsNull)
             {
              RowRC->AddPair(FId, new TJSONNull);
             }
            else
             {
              if (flDef->CmpName("choice, choiceDB"))
               {
                RowRC->AddPair(FId, TJSONObject::ParseJSONValue("{\"" + FQ->FieldByName(FFN)->AsString + "\":\"" +
                  FQ->FieldByName(FFN + "Str")->AsString + "\"}"));
               }
              else if (flDef->CmpName("binary"))
               {
                RowRC->AddPair(FId, TJSONObject::ParseJSONValue("{\"" + FQ->FieldByName(FFN)->AsString + "\":\"" +
                  UnicodeString((FQ->FieldByName(FFN)->AsInteger) ? "+" : "") + "\"}"));
               }
              else if (flDef->CmpName("choiceTree, extedit"))
               {
                if (!(flDef->CmpName("extedit") && flDef->CmpAV("fieldtype",
                  "integer") && (flDef->AV["length"].ToIntDef(0) > 64)))
                 RowRC->AddPair(FId, TJSONObject::ParseJSONValue("{\"" + FQ->FieldByName(FFN)->AsString + "\":\"" +
                  FQ->FieldByName(FFN)->AsString + "\"}"));
                // RowRC->AddPair(FId,/*TJSONObject::ParseJSONValue(*/GetExtEditVal(flDef, ARecId, FQ->FieldByName(FFN)->AsString, FQ)/*)*/);
               }
              else
               RowRC->AddPair(FId, FQ->FieldByName(FFN)->AsString);
             }
           }
          RC->AddPair(GetKeyFl(AId), RowRC);
          FQ->Next();
         }
       }
     }
    else
     { // �������� ����
      dsPlanLogError("error ID: " + AId, __FUNC__);
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvPlanDM::Find(System::UnicodeString AId, TJSONObject * AFindParam)
 {
  TJSONObject * RC;
  // TdsSrvClassif *FSQLs = new TdsSrvClassif(FRegDef, AId, "", "");
  try
   {
    // RC = (TJSONObject*)TJSONObject::ParseJSONValue(UnicodeString("[1,2,3,4,5]"));
    // quExec(GetCountSQL(AId));
    // RC = (TJSONObject*)TJSONObject::ParseJSONValue("{\"code\":"+ARecId+",\"003D\":{\"5\":\"����\"},\"002C\":\"����\",\"0062\":{\"3\":\"�������\"},\"0063\":0.5,\"0069\":1,\"0067\":{\"3\":\"�������\"},\"0068\":100,\"006B\":1,\"00C6\":1}");
    // RC = quFree->FieldByName("RCount")->AsInteger;
    // FSQLs->SQLs;
    // RC = GetCount(AId);
   }
  __finally
   {
    // delete FSQLs;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsSrvPlanDM::CreateTempQuery()
 {
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = FConnection;
    FQ->Connection                       = FConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  return FQ;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlanDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSrvPlanDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsPlanLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSrvPlanDM::quExecParamStm(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
  UnicodeString AParam1Name, UnicodeString AParam1Val, UnicodeString ALogComment, UnicodeString AParam2Name,
  TStream * AParam2Val)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
      if (AParam2Name.Length())
       AQuery->ParamByName(AParam2Name)->LoadFromStream(AParam2Val, ftBlob);
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      UnicodeString FParams = "; Params: ";
      if (AParam1Name.Length())
       FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
      if (AParam2Name.Length())
       FParams += AParam2Name + "=stm; ";
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    UnicodeString FParams = "; Params: ";
    if (AParam1Name.Length())
     FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
    if (AParam2Name.Length())
     FParams += AParam2Name + "=stm; ";
    dsLogSQL(((ALogComment.Length()) ? ALogComment : ASQL) + FParams, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSrvPlanDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
  UnicodeString AParam1Name, UnicodeString AParam1Val, UnicodeString ALogComment, UnicodeString AParam2Name,
  Variant AParam2Val, UnicodeString AParam3Name, Variant AParam3Val, UnicodeString AParam4Name, Variant AParam4Val)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
      if (AParam2Name.Length())
       AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
      if (AParam3Name.Length())
       AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
      if (AParam4Name.Length())
       AQuery->ParamByName(AParam4Name)->Value = AParam4Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      UnicodeString FParams = "; Params: ";
      if (AParam1Name.Length())
       FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
      if (AParam2Name.Length())
       FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
      if (AParam3Name.Length())
       FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
      if (AParam4Name.Length())
       FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "");
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    UnicodeString FParams = "; Params: ";
    if (AParam1Name.Length())
     FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
    if (AParam2Name.Length())
     FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
    if (AParam3Name.Length())
     FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
    if (AParam4Name.Length())
     FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "");
    dsLogSQL(((ALogComment.Length()) ? ALogComment : ASQL) + FParams, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlanDM::GetDependClassCode(UnicodeString ADepClName, UnicodeString ADepFlVal,
  UnicodeString ARetFlName, UnicodeString & ARetVals)
 {
  // TFDQuery *FQ = CreateTempQuery();
  // DeleteTempQuery(FQ);
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    if (quExec(FQ, false, "Select " + ARetFlName + " From " + ADepClName + " Where " + ADepFlVal))
     {
      UnicodeString FRetVals = "";
      int FCount = 0;
      while (!FQ->Eof && (FCount < 200))
       {
        FRetVals += FQ->FieldByName(ARetFlName)->AsString + ",";
        FCount++ ;
        FQ->Next();
       }
      if (FCount >= 199)
       {
        FRetVals = "Select " + ARetFlName + " From " + ADepClName + " Where " + ADepFlVal;
       }
      else
       {
        if (!FRetVals.Length())
         FRetVals = "-1";
        else
         FRetVals[FRetVals.Length()] = ' ';
       }
      ARetVals = FRetVals;
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::FGetDepVal(UnicodeString AVals, UnicodeString AUID)
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString FDep, FDepList;
    FDepList = AVals;
    if (FDepList.Length() >= 6)
     {
      while (FDepList.Length() && !RC.Length())
       {
        FDep = GetLPartB(FDepList, ';').Trim();
        if (!FDep.Length())
         FDep = FDepList;
        if (GetLPartB(FDep, '=').Trim().UpperCase() == AUID.UpperCase())
         RC = GetRPartB(FDep, '=').Trim();
        FDepList = GetRPartB(FDepList, ';').Trim();
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSrvPlanDM::FDepValIsNull(UnicodeString AVals)
 {
  bool RC = true;
  try
   {
    UnicodeString FDep, FDepList;
    FDepList = AVals;
    if (FDepList.Length() >= 6)
     {
      while (FDepList.Length() && RC)
       {
        FDep = GetLPartB(FDepList, ';').Trim();
        if (!FDep.Length())
         FDep = FDepList;
        RC &= !GetRPartB(FDep, '=').Trim().Length();
        FDepList = GetRPartB(FDepList, ';').Trim();
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetDependValSQL(UnicodeString ARef, bool AEmptyIfNoDepend)
 {
  UnicodeString RC = "";
  try
   {
    // FCtrlOwner->FillClass("d."+FNode->AV["ref"]+":"+FNode->AV["depend"]+":"+ADepClCode+":"+ADepUID, (TStringList*)ED->Properties->Items, ACode);
    // UnicodeString FRef = ARef;
    UnicodeString FNodeUid = GetLPartB(ARef, ':'); // uid choiceDB ������� �����������
    UnicodeString FDepVals = GetRPartB(ARef, ':'); // �������� ��������� �� ������� ������� � ������� "UID=��������;"
    TTagNode * FDef = FRegDef->GetTagByUID(FNodeUid);
    if (FDef) // ���� choiceDB
     {
      UnicodeString FTabPref = "";
      UnicodeString FNodeRef = FDef->AV["ref"]; // ������� �� �������������, �� �������� ���� ������
      UnicodeString FNodeDepend = FDef->AV["depend"] + ";";
      // ������ ������������ � ������� uid-��������.id-�������������� � ������� ��������� �����������
      // FRef = GetRPartB(FRef,':');
      // UnicodeString ADepClCode = GetLPartB(FRef,':').UpperCase();
      // UnicodeString ADepUID = GetRPartB(FRef,':').UpperCase();
      UnicodeString FDep, FDepList;
      FDepList = FNodeDepend;
      if (FDepList.Length() >= 9)
       {
        UnicodeString FDepTab, FDepWhereVal, FRetFlName, FRetVals;
        if (!FDepValIsNull(FDepVals))
         {
          UnicodeString FDepUID;
          TTagNode * FDepClDef, *FDepNode, *_tmp;
          FDepTab   = ("CLASS_" + FTabPref + FNodeRef).UpperCase();
          FDepClDef = FRegDef->GetTagByUID(FNodeRef);
          bool IsEquDepend = true;
          while (FDepList.Length() && IsEquDepend)
           {
            FDep = GetLPartB(FDepList, ';').UpperCase();
            IsEquDepend &= (FDepTab == "CLASS_" + FTabPref + _UID(FDep));
            FDepList = GetRPartB(FDepList, ';').Trim();
           }
          FDepList = FNodeDepend;
          UnicodeString FCtrlUID, FDepCode, FDepClCode, FFlName;
          if (IsEquDepend)
           { // ��� ����������� � ����� �������, ����� ������� ����� where
            FRetVals     = "";
            FDepWhereVal = "";
            while (FDepList.Length())
             {
              FDep       = GetLPartB(FDepList, ';').Trim();
              FCtrlUID   = _GUI(FDep);
              _tmp       = FRegDef->GetTagByUID(FCtrlUID);
              FDepClCode = _tmp->AV["ref"];
              FDepCode   = FGetDepVal(FDepVals, FCtrlUID);
              FDepNode   = FDepClDef->GetChildByAV("", "ref", FDepClCode);
              if (FDepNode)
               {
                FFlName = "Cl0" + FNodeRef + ".R" + FDepNode->AV["uid"];
                if (FDepCode.Length() && (FDepCode.ToIntDef(-1) != -2))
                 {
                  if (FDepWhereVal.Length())
                   FDepWhereVal += " and ";
                  FDepWhereVal += FFlName + "=" + FDepCode;
                 }
                else
                 {
                  if (FDepWhereVal.Length())
                   FDepWhereVal += " and ";
                  FDepWhereVal += " (" + FFlName + "=0 or " + FFlName + " is null)";
                 }
               }
              FDepList = GetRPartB(FDepList, ';').Trim();
             }
           }
          else
           {
            FDepList = FNodeDepend;
            while (FDepList.Length())
             {
              FDep = GetPart1(FDepList, ';').Trim();
              if (!FDep.Length())
               FDep = FDepList;
              // FDep � ������� uid-��������.id-�������������� � ������� ��������� �����������
              FCtrlUID = _GUI(FDep); // FCtrlUID - uid ������� �� �������� ������� ��������
              FDepCode = FGetDepVal(FDepVals, FCtrlUID);
              if (FDepCode.Length())
               {
                FDepUID    = _UID(FDep); // FDepUID uid �������������� � ������� ��������� �����
                FDepTab    = "CLASS_" + FTabPref + FDepUID;
                FDepClDef  = FRegDef->GetTagByUID(FDepUID); // ���� �������������� � ������� ��������� �����
                FDepClCode = FRegDef->GetTagByUID(FCtrlUID)->AV["ref"]; // � ������ ����� �������������� FDepClDef
                FDepNode   = FDepClDef->GetChildByAV("", "ref", FDepClCode /* ADepClCode */);
                // ���������� ���� � ref= ������_��_��������_�������_��������->AV["ref"]
                // ���� � ���, ��� ������������ ����� ������������ �� ���������� ������
                FRetVals = "";
                if (FDepCode.ToIntDef(-1) != -2) // ���������, ������ ����� ???
                 {
                  if (FDepCode.ToIntDef(-1) == -1)
                   {
                    FDepWhereVal = "(R" + FDepNode->AV["uid"] + " is NULL or R" + FDepNode->AV["uid"] + "=0)";
                    FRetFlName   = "CODE";
                   }
                  else
                   {
                    if (FDepUID == FDepClCode /* ADepClCode */)
                      // ��� �������� �������� ����������, ��� ����� ���� ���� ��������� ���������������
                     {
                      FDepWhereVal = "CODE=" + FDepCode;
                     }
                    else if (FDepUID == FNodeRef)
                      // ����������� ��������� � �������������� �������� �� �������� ������� ��������
                     {
                      FDepWhereVal = "R" + FDepNode->AV["uid"] + "=" + FDepCode;
                      FRetFlName   = "CODE";
                     }
                    else // ����������� ��������� � ��������� ��������������
                     {
                      FDepWhereVal = "R" + FDepNode->AV["uid"] + "=" + FDepCode;
                      FRetFlName   = "R" + FDepClDef->GetChildByAV("", "ref", FNodeRef)->AV["uid"];
                     }
                   }
                  GetDependClassCode(FDepTab, FDepWhereVal, FRetFlName, FRetVals);
                  FDepWhereVal = "code in (" + FRetVals + ")";
                 }
               }
              FDepList = GetRPartB(FDepList, ';').Trim();
             }
           }
         }
        if (FDepWhereVal.Length())
         RC = GetClassSQL(FNodeRef) + " Where " + FDepWhereVal;
        // InvadeClassCB(cBox(0),0, false, cNode(0),quFree,FTabPref,FRetVals,FCtrlOwner->DefXML); //+++
        else
         {
          if (!AEmptyIfNoDepend)
           RC = GetClassSQL(FNodeRef);
          // InvadeClassCB(cBox(0),0, false, cNode(0),quFree,FTabPref,FRetVals,FCtrlOwner->DefXML); //+++
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlanDM::DeleteData(System::UnicodeString AId, System::UnicodeString ARecId)
 {
  UnicodeString RC = "error";
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      if (quExecParam(FQ, true, GetDeleteSQL(AId), "p" + GetKeyFl(AId), ARecId))
       RC = "ok";
     }
    catch (Exception & E)
     {
      RC = E.Message;
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::DeletePatientPlan(System::UnicodeString AId, System::UnicodeString ARecId)
 {
  UnicodeString RC = "������";
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    UnicodeString FId = _UID(AId);
    UnicodeString FPlanId = _GUI(AId);
    if (FId.LowerCase() == "all")
     {
      if (quExecParam(FQ, false, "Delete from class_3084 where R328F=:pCode and R317D=:pUCode", "pCode", FPlanId, "",
        "pUCode", ARecId))
       {
        if (quExecParam(FQ, true, "Delete from class_1084 where R318F=:pCode and R017D=:pUCode", "pCode", FPlanId, "",
          "pUCode", ARecId))
         RC = "ok";
       }
     }
    else if (FId.LowerCase() == "item")
     {
      if (quExecParam(FQ, false, "Select R317D, R3086, R3089, R32AB from class_3084 where code=:pCode", "pCode",
      ARecId))
       {
        if (FQ->RecordCount)
         {
          UnicodeString FSQL = "Delete from class_1084 where R318F=:pCode and R017D=:pUCode and R1086=:pIType and ";
          __int64 FItemCode = 0;
          if (FQ->FieldByName("R3089")->AsInteger)
           {
            FSQL += "R1089=:pItemCode";
            FItemCode = FQ->FieldByName("R3089")->AsInteger;
           }
          else
           {
            FSQL += "R32A6=:pItemCode";
            FItemCode = FQ->FieldByName("R32AB")->AsInteger;
           }
          if (quExecParam(FQ, false, FSQL, "pCode", FPlanId, "", "pUCode", FQ->FieldByName("R317D")->AsInteger,
            "pIType", FQ->FieldByName("R3086")->AsInteger, "pItemCode", FItemCode))
           {
            if (quExecParam(FQ, true, "Delete from class_3084 where code=:pCode", "pCode", ARecId))
             RC = "ok";
           }
         }
       }
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlanDM::DeletePlan(System::UnicodeString ARecId)
 {
  return DeleteData("3188", ARecId);
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlanDM::GetTextData(System::UnicodeString AId, System::UnicodeString AFlId,
  System::UnicodeString ARecId, UnicodeString & AMsg)
 {
  UnicodeString RC = "";
  AMsg = "error";
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      if (quExecParam(FQ, false, "Select " + GetFlNameById(AId, AFlId) + " From " + GetTabName(AId) + " where " +
        GetKeyFl(AId) + "=:pKey", "pKey", ARecId))
       {
        RC   = FQ->FieldByName(GetFlNameById(AId, AFlId))->AsString;
        AMsg = "ok";
       }
     }
    catch (Exception & E)
     {
      RC = E.Message;
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlanDM::SetTextData(System::UnicodeString AId, System::UnicodeString AFlId,
  System::UnicodeString ARecId, System::UnicodeString AVal)
 {
  UnicodeString RC = "error";
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      if (quExecParam(FQ, true, "Update " + GetTabName(AId) + " set " + GetFlNameById(AId, AFlId) + "=:pFl where " +
        GetKeyFl(AId) + "=:pKey", "pFl", AVal, "", "pKey", ARecId))
       RC = "ok";
     }
    catch (Exception & E)
     {
      RC = E.Message;
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetPlanOpt()
 {
  return FPlanOpt->AsXML;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlanDM::SetPlanOpt(UnicodeString AOpts)
 {
  // � ������ ���� ��������� ��������� ���������� ���
  TStringStream * FZipSch = new TStringStream;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    FPlanOpt->AsXML = AOpts;
    FPlanOpt->GetZIPXML(FZipSch);
    quExecParam(FQ, true,
      "UPDATE OR INSERT INTO BINARY_DATA (NAME, FLD_VALUE) values(:pNAME, :pFLD_VALUE) MATCHING (NAME)", "pNAME",
      "planSetting", "", "pFLD_VALUE", FZipSch->DataString);
   }
  __finally
   {
    delete FZipSch;
    DeleteTempQuery(FQ);
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetUnitSch(__int64 AUCode)
 {
  UnicodeString RC = "";
  TTagNode * tmpSch = new TTagNode;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      dsPlanLogMessage("PtId : " + IntToStr(AUCode), __FUNC__, 5);
      if (quExecParam(FQ, false, "Select data From UNIT_CARD_OPT where code=:pCode", "pCode", AUCode,
        "������ �������� ������������ ��� ��������"))
       {
        TTagNode * FSchRoot, *itSch, *itDefSch;
        TTagNode * FSchDefRoot = FPlanOptDefSch;
        if (!FSchDefRoot)
         throw Exception("����������� ��������� ���� �� ���������");
        bool FCreateNew = false;
        if (FQ->FieldByName("data")->IsNull) // ��� ����
           FCreateNew = true;
        else
         {
          try
           {
            try
             {
              tmpSch->Encoding = "utf-8";
              tmpSch->AsXML    = FQ->FieldByName("data")->AsString;
             }
            catch (Exception & E)
             {
              TStringStream * FZipSch = new TStringStream;
              try
               {
                FZipSch->WriteData(FQ->FieldByName("data")->AsBytes, FQ->FieldByName("data")->AsBytes.Length);
                tmpSch->Encoding = "utf-8";
                tmpSch->SetZIPXML(FZipSch);
               }
              __finally
               {
                delete FZipSch;
               }
             }
            FSchRoot = tmpSch->GetChildByName("sch", true);
            if (!FSchRoot)
             FCreateNew = true;
           }
          catch (Exception & E)
           {
            FCreateNew = true;
           }
         }
        if (FCreateNew)
         {
          tmpSch->Name     = "ppls";
          tmpSch->Encoding = "utf-8";
          tmpSch->AddChild("passport")->AV["gui"] = NewGUI();
          FSchRoot = tmpSch->AddChild("content")->AddChild("sch");
          itDefSch = FSchDefRoot->GetFirstChild();
          while (itDefSch)
           {
            itSch               = FSchRoot->AddChild("spr");
            itSch->AV["infref"] = itDefSch->AV["infref"];
            itSch->AV["vac"]    = itDefSch->AV["vschdef"];
            itSch->AV["test"]   = itDefSch->AV["tschdef"];
            itSch->AV["tvac"]   = "_NULL_";
            itSch->AV["ttest"]  = "_NULL_";
            itDefSch            = itDefSch->GetNext();
           }
         }
        else
         {
          itDefSch = FSchDefRoot->GetFirstChild();
          while (itDefSch)
           {
            itSch = FSchRoot->GetChildByAV("spr", "infref", itDefSch->AV["infref"]);
            if (!itSch)
             {
              itSch               = FSchRoot->AddChild("spr");
              itSch->AV["infref"] = itDefSch->AV["infref"];
              itSch->AV["vac"]    = itDefSch->AV["vschdef"];
              itSch->AV["test"]   = itDefSch->AV["tschdef"];
              itSch->AV["tvac"]   = "_NULL_";
              itSch->AV["ttest"]  = "_NULL_";
             }
            itDefSch = itDefSch->GetNext();
           }
         }
        RC = tmpSch->AsXML;
       }
     }
    catch (Exception & E)
     {
      RC = E.Message;
     }
   }
  __finally
   {
    delete tmpSch;
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlanDM::GetUnitDopPlanCodes(__int64 AUCode, UnicodeString & AAddrCode, UnicodeString & AOrgCode,
  UnicodeString & AUchCode, UnicodeString & AProfCode)
 {
  AAddrCode = "";
  AOrgCode  = "";
  AUchCode  = "";
  AProfCode = "";
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      dsPlanLogMessage("PtId : " + IntToStr(AUCode), __FUNC__, 5);
      // �����' uid = '0136'
      // �����������' uid = '0025'
      // ���' uid = '00F1'
      // �������' uid = '00B3'
      if (quExecParam(FQ, false, "Select R0136, R0025, R00F1, R00B3 From class_1000 where code=:pCode", "pCode",
        AUCode))
       {
        if (!FQ->FieldByName("R0136")->IsNull)
         AAddrCode = FQ->FieldByName("R0136")->AsString;
        if (!FQ->FieldByName("R0025")->IsNull)
         AOrgCode = FQ->FieldByName("R0025")->AsString;
        if (!FQ->FieldByName("R00B3")->IsNull)
         AUchCode = FQ->FieldByName("R00B3")->AsString;
        // if (!FQ->FieldByName("R")->IsNull)
        // AProfCode = FQ->FieldByName("R")->AsString;
       }
     }
    catch (Exception & E)
     {
      dsPlanLogError("PtId : " + IntToStr(AUCode) + ", ��������� ���������: " + E.Message, __FUNC__, 5);
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlanDM::GetPatPlanOptions(__int64 APatCode)
 {
  dsLogBegin(UnicodeString(__FUNC__) + "PtId :" + IntToStr(APatCode));
  UnicodeString RC = "";
  bool AddInf;
  TTagNode * tmpSch = new TTagNode;
  try
   {
    UnicodeString FProfCode, FAddrCode, FOrgCode, FUchCode;
    // !!!+ �������� ��������� ����� ��� ��������
    tmpSch->AsXML = GetUnitSch(APatCode);
    GetUnitDopPlanCodes(APatCode, FAddrCode, FOrgCode, FUchCode, FProfCode);
    TTagNode * FPatCommPlan;
    TTagNode * FPatPlan = tmpSch->GetChildByName("vacplan", true);
    if (!FPatPlan)
     FPatPlan = tmpSch->GetChildByName("content", true)->AddChild("vacplan");
    FPatCommPlan = FPatPlan->GetChildByName("comm");
    if (!FPatCommPlan)
     FPatCommPlan = FPatPlan->AddChild("comm");
    else
     FPatCommPlan->DeleteChild();
    TTagNode * itPlan = PlanOpt->GetChildByName("vacplan", true)->GetFirstChild();
    while (itPlan)
     {
      if (itPlan->CmpAV("objtype", "l"))
       FPatCommPlan->AddChild()->Assign(itPlan, true);
      else if (itPlan->CmpAV("objtype", "u") && FUchCode.Length() && itPlan->CmpAV("objref", FUchCode))
       FPatCommPlan->AddChild()->Assign(itPlan, true);
      else if (itPlan->CmpAV("objtype", "p") && FProfCode.Length() && itPlan->CmpAV("objref", FProfCode))
       FPatCommPlan->AddChild()->Assign(itPlan, true);
      else if (itPlan->CmpAV("objtype", "o") && FOrgCode.Length())
       {
        if (GetLPartB(itPlan->AV["objref"], '.') == FOrgCode)
         FPatCommPlan->AddChild()->Assign(itPlan, true);
       }
      else if (itPlan->CmpAV("objtype", "a") && FAddrCode.Length())
       {
        if (AddrInAddr(FAddrCode, itPlan->AV["objref"]))
         FPatCommPlan->AddChild()->Assign(itPlan, true);
       }
      itPlan = itPlan->GetNext();
     }
    RC = tmpSch->AsXML;
   }
  __finally
   {
    delete tmpSch;
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetDefUnitSch(__int64 AUCode /* , UnicodeString &ADefUnitSCh */)
  // void __fastcall TdsSrvPlanDM::GetDefUnitSch(__int64 AUCode, UnicodeString & ADefUnitSCh)
 {
  UnicodeString RC = "";
  TTagNode * tmpSch = new TTagNode;
  TTagNode * FSaveSch = new TTagNode;
  try
   {
    tmpSch->AsXML = GetUnitSch(AUCode);
    TTagNode * FSchRoot, *itSch, *itDefSch, *itSaveSch;
    TTagNode * FSchDefRoot = FPlanOptDefSch;
    FSchRoot        = tmpSch->GetChildByName("sch", true);
    FSaveSch->AsXML = FSchRoot->AsXML;
    FSchRoot->DeleteChild();
    itDefSch = FSchDefRoot->GetFirstChild();
    while (itDefSch)
     {
      itSch               = FSchRoot->AddChild("spr");
      itSch->AV["infref"] = itDefSch->AV["infref"];
      itSch->AV["vac"]    = itDefSch->AV["vschdef"];
      itSch->AV["test"]   = itDefSch->AV["tschdef"];
      itSch->AV["tvac"]   = "_NULL_";
      itSch->AV["ttest"]  = "_NULL_";
      itSaveSch           = FSaveSch->GetChildByAV("spr", "infref", itSch->AV["infref"]);
      if (itSaveSch)
       {
        if (itSaveSch->AV["chvac"].ToIntDef(0))
         {
          itSch->AV["vac"]   = itSaveSch->AV["vac"];
          itSch->AV["chvac"] = "1";
         }
        if (itSaveSch->AV["chtest"].ToIntDef(0))
         {
          itSch->AV["test"]   = itSaveSch->AV["test"];
          itSch->AV["chtest"] = "1";
         }
       }
      itDefSch = itDefSch->GetNext();
     }
    RC = tmpSch->AsXML;
   }
  __finally
   {
    delete tmpSch;
    delete FSaveSch;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlanDM::SetUnitSch(__int64 AUCode, UnicodeString ASch)
 {
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    quExecParam(FQ, true, "UPDATE OR INSERT INTO UNIT_CARD_OPT (code, data) values(:pCode, :pData)", "pCode", AUCode,
      "���������� �������� ������������ ��� ��������", "pData", ASch);
   }
  __finally
   {
    DeleteTempQuery(FQ);
    Application->ProcessMessages();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlanDM::FDocGetSVal(UnicodeString AFuncName, TTagNode * ANode, UnicodeString & ATab,
  UnicodeString & ARet)
 {
  FImmNom->GetSVal(AFuncName, ANode, ATab, ARet);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::NewGUID()
 {
  return icsNewGUID().UpperCase();
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlanDM::GetPlanProgress()
 {
  UnicodeString RC = "progress";
  try
   {
    Application->ProcessMessages();
    RC = IntToStr(FCurrPlan) + "/" + IntToStr(FCommPlanCount);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetWhereFromFilter(TTagNode * AFlt, UnicodeString AMainISUID)
 {
  UnicodeString RC = "";
  UnicodeString FCountSQL = "";
  bool FIsDist = false;
  bool FIsAddKey = false;
  TStringList * TabList = new TStringList;
  TdsDocSQLCreator * FSQLCreator = new TdsDocSQLCreator(FXMLList);
  FSQLCreator->OnGetSVal = FRegGetSVal;
  try
   {
    TTagNode * FDocCondRules;
    if (AFlt->CmpName("condrules"))
     FDocCondRules = AFlt;
    else
     FDocCondRules = AFlt->GetChildByName("condrules", true);
    if (FDocCondRules)
     {
      try
       {
        if (!FSQLCreator->CreateWhere(FDocCondRules, TabList, RC, AMainISUID))
         RC = "";
        // else
        // ATabList = FSQLCreator->CreateFrom(AMainISUID, TabList, &FIsDist,"",&FIsAddKey);
       }
      catch (Exception & E)
       {
        dsPlanLogError("FSQLCreator->CreateWhere : " + E.Message + "; CondRules = " + FDocCondRules->AsXML,
        __FUNC__, 5);
       }
     }
   }
  __finally
   {
    delete TabList;
    delete FSQLCreator;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlanDM::FRegGetSVal(UnicodeString AFuncName, TTagNode * ANode, UnicodeString & ATab,
  UnicodeString & ARet)
 {
  FImmNom->GetSVal(AFuncName, ANode, ATab, ARet);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlanDM::GetUnitData(UnicodeString AUCode, TDate & ABithDay)
 {
  UnicodeString RC = "������";
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      if (quExecParam(FQ, false, "Select R001f, R0020, R002f, R0031 From class_1000 where code=:pCode", "pCode",
      AUCode))
       {
        RC = FQ->FieldByName("R001f")->AsString + " ";
        RC += FQ->FieldByName("R0020")->AsString + " ";
        RC += FQ->FieldByName("R002f")->AsString;
        ABithDay = FQ->FieldByName("R0031")->AsDateTime;
       }
     }
    catch (Exception & E)
     {
      dsPlanLogError("PtId : " + AUCode + ", ��������� ���������: " + E.Message, __FUNC__, 5);
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
