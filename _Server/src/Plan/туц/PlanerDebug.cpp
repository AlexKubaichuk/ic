

#include <vcl.h>
#pragma hdrstop

#include "PlanerDebug.h"
//#include "DMF.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall TPrePlanerDebug::TPrePlanerDebug(TdsPlanDM *ADM)
{
  dsPlanLogMessage("",__FUNC__,5);
  FDM = ADM;
  FLog = NULL;
  FCurPat = NULL;
  FCurInfLine = NULL;
  FCurVacList = NULL;
  FCurTestList = NULL;
}
//---------------------------------------------------------------------------
__fastcall TPrePlanerDebug::~TPrePlanerDebug()
{
  dsPlanLogMessage("",__FUNC__,5);
  if (FLog)
   {
     SaveLog();
     delete FLog;
   }
  FLog = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::InitLog()
{
  dsPlanLogMessage("",__FUNC__,5);
  if (FLog)
   {
//     SaveLog();
     delete FLog;
   }
  FLog = new TTagNode;
  FCurPat      = NULL;
  FCurInfLine  = NULL;
  FCurVacList  = NULL;
  FCurTestList = NULL;
  FtmpCurLine  = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::LogPlanPeriod(TDate ADatePB, TDate ADatePE, TDate APlanMonth)
{
//  dsPlanLogMessage("",__FUNC__,5);
  if (FLog)
  {
    TTagNode *tmpNode = FLog->AddChild("PlanParams");
//    DFW( "******************** ����������� IMMPlaner ************************\n" );
    DFW(tmpNode, "����, �����:",DateToStr(Date())+"  "+TimeToStr(Time()));
    DFW(tmpNode, "���� ������ ������� ������������",DateStr(ADatePB));
    DFW(tmpNode, "���� ��������� ������� ������������",DateStr(ADatePE));
    DFW(tmpNode, "1-� ����� ������ ������������",DateStr(APlanMonth));
  }
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::DFW(TTagNode *ANode, UnicodeString AComm, UnicodeString AVal)
{
  dsPlanLogMessage(AComm+": "+AVal,__FUNC__,5);
  if (ANode)
   {
     TTagNode *tmp = ANode->AddChild("it");
     tmp->AV["val"] = AVal;
     tmp->AV["comm"] = AComm;
   }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TPrePlanerDebug::DateStr(TDateTime ADate)
{
//  dsPlanLogMessage("",__FUNC__,5);
  return ((int)ADate == 0)? UnicodeString(""):UnicodeString(DateToStr(ADate));
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::StartSch(TTagNode *ABegSchemeLine)
{
//  dsPlanLogMessage("",__FUNC__,5);
  if (FCurInfLine)
   {
     if (ABegSchemeLine)
      DFW(FCurInfLine,"UID ��������� ������ �����",ABegSchemeLine->AV["uid"]);
     else
      DFW(FCurInfLine,"UID ��������� ������ �����","��� ��������� �����");
   }
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::CIL_Data(TTagNode *ACurSchemeLine, TStringList *AJumpTrace)
{
//  dsPlanLogMessage("",__FUNC__,5);
  if (FCurInfLine)
  {
   if ( ACurSchemeLine )
   {
     DFW(FCurInfLine,"UID ����������� ������ �����",ACurSchemeLine->AV["uid"]);
     DFW(FCurInfLine,"���. ����� ����������� ������ �����",ACurSchemeLine->AV["min_pause"]);
     DFW(FCurInfLine,"���. ������� ����������� ������ �����",ACurSchemeLine->AV["min_age"]);
   }
   TTagNode *tmp = FCurInfLine->AddChild("trace","comm=������ ���������:");
   for ( int i = 0; i < AJumpTrace->Count; i++ )
     DFW(tmp,GetPart1(AJumpTrace->Strings[i],'#'),GetPart2(AJumpTrace->Strings[i],'#'));
  }
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::CIL_ItemPlanDate(TDate APlanItemPlanDate)
{
//  dsPlanLogMessage("",__FUNC__,5);
  DFW(FCurInfLine,"���� ������������ ����������",DateStr(APlanItemPlanDate));
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::CIL_Season(bool ASeasonOk)
{
//  dsPlanLogMessage("",__FUNC__,5);
  if (ASeasonOk)
   DFW(FCurInfLine,"� ����� ���������� �����","");
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::CIL_PlanByPrevTest(bool AResult)
{
//  dsPlanLogMessage("",__FUNC__,5);
  if (AResult)
   DFW(FCurInfLine,"�� ����������� �������������� ����� ����������� �� ����","");
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::CIL_ExceptSch(bool AResult)
{
//  dsPlanLogMessage("",__FUNC__,5);
  if (FCurInfLine)
   {
     if (AResult) DFW(FCurInfLine, "EXCEPTION","���� � ������� ������");
     else         DFW(FCurInfLine, "EXCEPTION","���� �� ��������� ������");
   }
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::CIL_RetToPlan()
{
//  dsPlanLogMessage("",__FUNC__,5);
  DFW(FCurInfLine,"������� � ���� �� ����","");
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::CIL_InPlanPeriod()
{
//  dsPlanLogMessage("",__FUNC__,5);
  DFW(FCurInfLine,"� ������ ������������ �����","");
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::ItemListBegin(TPlanItemKind APlanItemKind)
{
  dsPlanLogMessage("",__FUNC__,5);
  FtmpCurLine = (APlanItemKind == TPlanItemKind::pkVakchina)? FCurVacList : FCurTestList;
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::SetItemList(int AInfId, int AInfType)
{
  if (FtmpCurLine)
   {
     TTagNode *tmp;

     tmp = FtmpCurLine->AddChild("inf");
     tmp->AV["id"]   = IntToStr(AInfId);
     tmp->AV["name"] = GetInfName(AInfId);
     if (AInfType)  tmp->AV["type"] = "(�������)";
     else           tmp->AV["type"] = "(��������)";
     dsPlanLogMessage(tmp->AsXML,__FUNC__,5);
   }
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::ItemListEnd()
{
  dsPlanLogMessage("",__FUNC__,5);
  FtmpCurLine = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::SetCIL(TPlanItemKind APlanItemKind, UnicodeString AInfId, bool ARoundMode, bool ALPlanItemExists, TDate ALPlanItemDate)
{
//  dsPlanLogMessage("",__FUNC__,5);
  TTagNode *FCurLine = (APlanItemKind == TPlanItemKind::pkVakchina)? FCurVacList : FCurTestList;
  if (FCurLine)
   {
     FCurInfLine = FCurLine->GetChildByAV("inf", "id", AInfId);
     if (FCurInfLine)
     {
       DFW(FCurInfLine," ------ ��������: ", GetInfName(AInfId.ToIntDef(0)));
       DFW(FCurInfLine,"���", (ARoundMode) ? UnicodeString("�� ����") : UnicodeString("�� �����"));
       DFW(FCurInfLine,UnicodeString("���� ���������� ��������� ")+
            UnicodeString((APlanItemKind == TPlanItemKind::pkVakchina)? "��������" : "�����"),
                 (ALPlanItemExists)
                                ? DateStr(ALPlanItemDate)
                                 : UnicodeString("---")
           );
     }
   }
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::CIL_PlanBeginDate(TPlanItemKind APlanItemKind, TDate ANPlanItemBegDate)
{
//  dsPlanLogMessage("",__FUNC__,5);
  DFW(FCurInfLine, UnicodeString("����, ���. � ���-� �. ���� ��������. ����. ") +
                   UnicodeString((APlanItemKind == TPlanItemKind::pkVakchina)? "��������" : "�����"),
                   DateStr(ANPlanItemBegDate)
     );
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::LogPatData(int APtId, UnicodeString APtFIO,TDate APtBirthDay, UnicodeString APtAge, UnicodeString UnitSch)
{
  dsPlanLogMessage("",__FUNC__,5);
  if (FLog)
  {
    TTagNode *tmpNode;
    TTagNode *patNode = FLog;
    if (patNode)
     {
       patNode = patNode->AddChild("Patient");
       patNode->AV["id"]   = IntToStr(APtId);
       patNode->AV["name"] = APtFIO;
       patNode->AV["bd"]   = DateStr(APtBirthDay);
       patNode->AV["age"]  = APtAge;

       patNode->AddChild("SchList")->AddChild()->AsXML = UnitSch;

       patNode->AddChild("InfList","comm=����������� ��������");
       tmpNode = patNode->AddChild("VacTestList","comm=������������ ��������/����");
       Fp1 = tmpNode->AddChild("p1","comm=�� ����������");
       Fp2 = tmpNode->AddChild("p2","comm=����� ������������� ��� ������������ ���������� ��������"); //����� �����������
       Fp3 = tmpNode->AddChild("p3","comm=�� ���������� � ����� ���� ������"); //����� ������������� ��������
       Fp4 = tmpNode->AddChild("p4","comm=����� ���������� � ����� ���� ������");  //����� ����� � ������������
       Fp5 = tmpNode->AddChild("p5","comm=�� ���������� ����������"); //����� ������������� ��� ������������ ���������� ��������
       Fp6 = tmpNode->AddChild("p6","comm=����� ���������� ����������");
     }
  }
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::Set_CVL_CTL()
{
  dsPlanLogMessage("",__FUNC__,5);
  if (FLog)
   {
     FCurPat = FLog->GetChildByName("Patient");
     TTagNode *tmp = FCurPat->GetChildByName("InfList");
     FCurVacList = tmp->AddChild("VacList","comm=��������");
     FCurTestList = tmp->AddChild("TestList","comm=�����");
   }
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::DebugPrintListPlans(int  ARootIdx)
{
  dsPlanLogMessage("",__FUNC__,5);
  if (ARootIdx && FLog)
   {
     TTagNode *FRoot;
     switch(ARootIdx)
     {
       case 1:{ FRoot = Fp1; break;}
       case 2:{ FRoot = Fp2; break;}
       case 3:{ FRoot = Fp3; break;}
       case 4:{ FRoot = Fp4; break;}
       case 5:{ FRoot = Fp5; break;}
       case 6:{ FRoot = Fp6; break;}
     }

     TTagNode *tmp = FRoot->AddChild("VacList","comm=����������� ��������");
     TTagNode *tmp2;
     for ( TPlanItemListIt i = FPrivPlan->begin(); i != FPrivPlan->end(); i++ )
     {
       tmp2 = tmp->AddChild("vac");
       tmp2->AV["id"] = IntToStr(i->OwnerId);
       tmp2->AV["name"] = i->OwnerName;
       tmp2->AV["type"] = IntToStr(i->InfekId);
       tmp2->AV["pldate"] = DateStr(i->PlanDate);           //DFW(tmp2,"���� ������������ ����������",DateStr(i->PlanDate));
       tmp2->AV["line_id"] = i->SchemeLineUID;              //DFW(tmp2,"UID ������ �����",i->SchemeLineUID);
       tmp2->AV["vactype"] = i->Type_Prep;                  //DFW(tmp2,"��� ����������",);
       tmp2->AV["prior"] = IntToStr(i->Priority);           //DFW(tmp2,"��������� ����������",);
       tmp2->AV["priorinfdef"] = IntToStr(i->V_RV_Priority);//DFW(tmp2,"��������� ���������� �� �������� �������� ",);
       tmp2->AV["rdate"] =  DateStr(i->VypDate);            //DFW(tmp2,"���� ��������� ���������� ",);
     }
     for ( TPlanItemListIt i = FDopPrivivPlan->begin(); i != FDopPrivivPlan->end(); i++ )
     {
       tmp2 = tmp->AddChild("vac");
       tmp2->AV["id"] = IntToStr(i->OwnerId);
       tmp2->AV["name"] = i->OwnerName;
       tmp2->AV["pldate"] = DateStr(i->PlanDate);           //DFW(tmp2,"���� ������������ ����������",DateStr(i->PlanDate));
       tmp2->AV["line_id"] = i->SchemeLineUID;              //DFW(tmp2,"UID ������ �����",i->SchemeLineUID);
       tmp2->AV["vactype"] = i->Type_Prep;                  //DFW(tmp2,"��� ����������",);
       tmp2->AV["prior"] = IntToStr(i->Priority);           //DFW(tmp2,"��������� ����������",);
       tmp2->AV["priorinfdef"] = IntToStr(i->V_RV_Priority);//DFW(tmp2,"��������� ���������� �� �������� �������� ",);
       tmp2->AV["rdate"] =  DateStr(i->VypDate);            //DFW(tmp2,"���� ��������� ���������� ",);
     }
     tmp = FRoot->AddChild("TestList","comm=����������� �����");
     for ( TPlanItemListIt i = FProbPlan->begin(); i != FProbPlan->end(); i++ )
     {
       tmp2 = tmp->AddChild("test");
       tmp2->AV["id"] = IntToStr(i->OwnerId);
       tmp2->AV["name"] = i->OwnerName;
       tmp2->AV["type"] = IntToStr(i->InfekId);
       tmp2->AV["pldate"] = DateStr(i->PlanDate);           //DFW(tmp2,"���� ������������ ����������",DateStr(i->PlanDate));
       tmp2->AV["line_id"] = i->SchemeLineUID;              //DFW(tmp2,"UID ������ �����",i->SchemeLineUID);
       tmp2->AV["vactype"] = i->Type_Prep;                  //DFW(tmp2,"��� ����������",);
       tmp2->AV["prior"] = IntToStr(i->Priority);           //DFW(tmp2,"��������� ����������",);
       tmp2->AV["priorinfdef"] = IntToStr(i->V_RV_Priority);//DFW(tmp2,"��������� ���������� �� �������� �������� ",);
       tmp2->AV["rdate"] =  DateStr(i->VypDate);            //DFW(tmp2,"���� ��������� ���������� ",);
     }
     for ( TPlanItemListIt i = FDopProbPlan->begin(); i != FDopProbPlan->end(); i++ )
     {
       tmp2 = tmp->AddChild("test");
       tmp2->AV["id"] = IntToStr(i->OwnerId);
       tmp2->AV["name"] = i->OwnerName;
       tmp2->AV["pldate"] = DateStr(i->PlanDate);           //DFW(tmp2,"���� ������������ ����������",DateStr(i->PlanDate));
       tmp2->AV["line_id"] = i->SchemeLineUID;              //DFW(tmp2,"UID ������ �����",i->SchemeLineUID);
       tmp2->AV["vactype"] = i->Type_Prep;                  //DFW(tmp2,"��� ����������",);
       tmp2->AV["prior"] = IntToStr(i->Priority);           //DFW(tmp2,"��������� ����������",);
       tmp2->AV["priorinfdef"] = IntToStr(i->V_RV_Priority);//DFW(tmp2,"��������� ���������� �� �������� �������� ",);
       tmp2->AV["rdate"] =  DateStr(i->VypDate);            //DFW(tmp2,"���� ��������� ���������� ",);
     }
   }
}

//---------------------------------------------------------------------------
UnicodeString __fastcall TPrePlanerDebug::GetInfName(int ACode)
{
  UnicodeString RC = "������, ��� �������� = "+IntToStr(ACode);
  try
   {
     if (FDM->InfName[IntToStr(ACode)].Length())
      RC = FDM->InfName[IntToStr(ACode)];
   }
  __finally
   {
   }
  dsPlanLogMessage(RC,__FUNC__,5);
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TPrePlanerDebug::GetVacName(int ACode)
{
  UnicodeString RC = "������, ��� ������� = "+IntToStr(ACode);
  try
   {
     if (FDM->VacName[IntToStr(ACode)].Length())
      RC = FDM->VacName[IntToStr(ACode)];
   }
  __finally
   {
   }
  dsPlanLogMessage(RC,__FUNC__,5);
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TPrePlanerDebug::GetTestName(int ACode)
{
  UnicodeString RC = "������, ��� ����� = "+IntToStr(ACode);
  try
   {
     if (FDM->TestName[IntToStr(ACode)].Length())
      RC = FDM->TestName[IntToStr(ACode)];
   }
  __finally
   {
   }
  dsPlanLogMessage(RC,__FUNC__,5);
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TPrePlanerDebug::GetCheckName(int ACode)
{
  UnicodeString RC = "������, ��� �������� = "+IntToStr(ACode);
  try
   {
     if (FDM->CheckName[IntToStr(ACode)].Length())
      RC = FDM->CheckName[IntToStr(ACode)];
   }
  __finally
   {
   }
  dsPlanLogMessage(RC,__FUNC__,5);
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TPrePlanerDebug::SaveLog()
{
  dsPlanLogMessage("",__FUNC__,5);
  if (FOnSaveLog)
   FOnSaveLog(this);
}
//---------------------------------------------------------------------------

