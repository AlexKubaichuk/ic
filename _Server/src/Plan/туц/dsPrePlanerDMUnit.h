//---------------------------------------------------------------------------

#ifndef dsPrePlanerDMUnitH
#define dsPrePlanerDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.VCLUI.Wait.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
#include "dsRegClassData.h"
#include <System.JSON.hpp>
#include "XMLContainer.h"
#include "dsSrvClassifUnit.h"
#include "icsLog.h"
#include <list>
#include "dsDocSQLCreator.h"
#include "IMMNomDef.h"
#include "JSONUtils.h"
//---------------------------------------------------------------------------
//���� ������ ������������
#define ERR_NO_BEG_SCHEME       1               //����������� ��������� ����� ��� ��������
#define ERR_BAD_BEG_SCHEME      2               //������� �������������� ��������� �����
#define ERR_SCHEMES_LOOP        3               //������������ �� ������� �������������� � ������� ����
//---------------------------------------------------------------------------
typedef void          (__closure *TdsOnPlanGetValByIdEvent)(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARetData);
typedef void          (__closure *TdsOnPlanGetValById10Event)(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARetData);
typedef UnicodeString (__closure *TdsOnPlanInsertDataEvent)(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
typedef void          (__closure *TdsOnPlanGetMIBPListEvent)(System::UnicodeString AId, TJSONObject *& ARetData);
typedef void          __fastcall (__closure *TdsOnPlanCreateUnitPlanEvent)(__int64 APtId, TDate ABegDate);
typedef UnicodeString  (__closure *TdsPlanGetStrValByCodeEvent)(UnicodeString ACode, unsigned int AParams);
typedef void          __fastcall (__closure *TdsOnPlanSaveDebugEvent)(UnicodeString AXML);
//---------------------------------------------------------------------------
// ��� ��������� �� ������ ������������
enum TPlanErrMsg
{
  pmWarning = 1,        // ��������������
  pmError = 2           // ������
};
//---------------------------------------------------------------------------
/*// ��� �����������
enum TKontType
{
  ktNone = 0,
  ktUch  = 1,        // �� �������
  ktOrg  = 2,        // �� �����������
  ktFlt  = 3         // �� �������
};  */
//---------------------------------------------------------------------------
// ��� ������ ���� TfmPlan::FDopID
/*enum TDopIDKind
{
  dkUnknown = 0,
  dkUch     = 1,        // ���� TfmPlan::FDopID �������� ��� �������
  dkOrg     = 2,        // ���� TfmPlan::FDopID �������� ��� �����������
  dkFltVak  = 3,        // ���� TfmPlan::FDopID �������� ��� �������
  dkFltProb = 4         // ���� TfmPlan::FDopID �������� ��� �����
};
//---------------------------------------------------------------------------
// ��� ������������ �� �������
enum TFltPlanKind
{
  fpNone,               //������������ ������������� �� �� �������
  fpDop,                //�������������
  fpCal                 //�� ���������
};
*/
//---------------------------------------------------------------------------
// ��� ������� ������������
/*enum class TPlanPeriodType
{
  ptNone     = -1,
  ptCur      = 0,       // �������
  ptPrev     = 1,       // ����������
  ptPrevPrev = 2,       // ����. ����������
  ptNext                // ��������� ( �������� ��� ����������� ������� ������������ )
};
*/
//---------------------------------------------------------------------------
// ��� �������� ������������
enum class TPlanItemKind
{
  pkVakchina = 0,       // �������
  pkTest     = 1,       // �����
  pkCheck    = 2,       // ��������
  pkUnknown
};
//---------------------------------------------------------------------------
// ��� �������� ����������� �����
enum class TCardItemKind
{
  Vac     = 0,       // ��������
  Test    = 1,       // �����
  Check   = 2,       // ��������
  Cancel  = 3,       // �����
  Unknown
};
//---------------------------------------------------------------------------
// ��� ��������
enum TJumpKind
{
  jkUnknown,    // �����������
  jkSrok,       // �� �����
  jkAge,        // �� ��������
  jkCond,       // �� �������
  jkEnd         // �� ���������
};
//---------------------------------------------------------------------------
// ��� ������������ ��������������� �����
enum class TPrePlanKind
{
  None,   // �� �����������
  PreVac, // ����������� ����������������
  PostVac // ����������� ����������������
};
//---------------------------------------------------------------------------
struct TJump{
  TJumpKind Kind;
  UnicodeString Reason;

  TJump(): Kind(jkUnknown), Reason(""){};
  TJump(TJumpKind AKind, UnicodeString AReason): Kind(AKind), Reason(AReason){};
};
// ��� "���������", �� �������� �������� ������ � ������� "����" (CARD_1084)
enum TPlanItemSource
{
  pisUnknown        = -1,
  pisOffPlan        = 0,        // ����������
  pisManual         = 1,        // ������ ����������
  pisPlanerClndr    = 2,        // ����������� �� ���������
  pisPlanerOffClndr = 3,        // ����������� �� �� ���������
  pisPlanerPrev     = 4,        // ����������� ��� ���������������
  pisPlanerRnd      = 5,        // ����������� �� ����
  pisPlanerRndPrev  = 6         // ����������� �� ���� ��� ���������������
};
//---------------------------------------------------------------------------
//������� ������ ����������� ��������/����
typedef struct tagPLAN_ITEM {
  TPlanItemKind PlanItemKind;           // ��� �������� ( ��������/����� )
  int           InfekId;                // ��� �������� (-1 ��� �������������� ��������/����� ��� ���
                                        // ������������ ��������)
  int           OwnerId;                // ��� ����/�����
  UnicodeString OwnerName;              // ������������ ����/�����
  TDate         PlanDate;               // ���� ������������ ���������� ��������/�����
  TDate         PlanMonth;              // ������ ���� ������ ���� ������������ ����������
  TDate         PlanDayByMonth;         // ����������� ���� ������
  UnicodeString SchemeLineUID;          // InfekId.<UID ������ �����, �� ������� ������������� ��������/�����>
                                        //  ( "" ��� �������������� ��������/����� )
                                        // ��� ������������ �������� ������ uid'�� ����������� ";"
  UnicodeString Type_Prep;              // ��� ���������� ( Vi,RVi/j,D ) / ����� ����� (��.i,D),
                                        // ��� ������������ �������� ������ ����������� ";"
  int           Priority;               // ��������� ���������� ( -1 ��� ����� )
  int           V_RV_Priority;          // ��������� ����������/������������ (�� �������� ����������� ��������)
                                        // ��� -1 ��� �����
  TDate         VypDate;                // ���� ��������� ���������� ��������/�����
  TPlanItemSource Source;               // ��� "���������", �� �������� �������� ������ � ������� "����" (CARD_1084)
  tagPLAN_ITEM() : PlanItemKind(TPlanItemKind::pkUnknown),
                   InfekId(-1),
                   OwnerId(-1),
                   OwnerName(""),
                   PlanDate(0),
                   PlanDayByMonth(0),
                   PlanMonth(0),
                   SchemeLineUID(""),
                   Type_Prep(""),
                   Priority(-1),
                   V_RV_Priority(-1),
                   VypDate(0),
                   Source(pisUnknown) {};
}PLAN_ITEM, *PPLAN_ITEM;
//---------------------------------------------------------------------------
typedef struct tagCardItem {
  TCardItemKind Kind;    // ��� �������� ( ��������/�����/��������/����� )
  __int64       Id;      // ��� ����/�����/��������/������� ������
  int           Inf;     // ��� ��������
  bool          Tur;     // ������� ���� (������ ��� ��������) (���������/����������) ��� �������
  UnicodeString VacType; // V / RV
  TDate         Date;    // ���� ����������
  TDate         EndDate; // ���� ��������� ��� �������
  UnicodeString Sch;     // �����
  bool          Vac;     // ����� ��������
  bool          Test;    // ����� �����

  tagCardItem() : Kind(TCardItemKind::Unknown),
                  Id(-1),
                  Inf(-1),
                  Tur(false),
                  Vac(false),
                  Test(false),
                  VacType(""),
                  Date(0),
                  EndDate(0),
                  Sch(""){};
} TCardItem;

//---------------------------------------------------------------------------
typedef struct tagPreTestItem {
  int           Inf;     // ��� �������� <choiceDB name='��������' uid='006E' ref='003A' required='1' inlist='l'/>
  __int64       Test;    // ��� ����� <choiceDB name='�����' uid='006D' ref='002A' required='1' inlist='l'/>
  int /*double*/ Reak;    //  <digit name='�������� �������' uid='006F' required='1' inlist='e' digits='2' min='0' max='99'/>
  int           Actuals; // <digit name='������������ ��������' uid='0070' required='1' inlist='e' digits='3' min='0' max='999' units='��.'/>
  TPrePlanKind  Kind;    // <choice name='�����������' uid='0076' required='1' inlist='e' default='0'>
                         //     <choicevalue name='�� �����������' value='0'/>
                         //     <choicevalue name='����������� ���������������� �����' value='1'/>
                         //     <choicevalue name='����������� ���������������� �����' value='2'/>
  int           Pause;    //<digit name='�����' uid='0072' required='1' inlist='e' digits='2' min='0' max='30' units='��.'>
  UnicodeString MinAge, MaxAge;

  tagPreTestItem() : Inf(-1),
                     Test(-1),
                     Reak(0),
                     Actuals(0),
                     Kind(TPrePlanKind::None),
                     Pause(0),
                     MinAge(""),
                     MaxAge("")
                     {};
} TPreTestItem;

//---------------------------------------------------------------------------
typedef list<TPreTestItem>      TPreTestItemList;          //������ ��������� ��������/����/��������/�������
typedef list<TCardItem>         TCardItemList;          //������ ��������� ��������/����/��������/�������
typedef list<PLAN_ITEM>         TPlanItemList;          //������ ����������� ��������/����
typedef map<int, TDate>         TPlanItemDateList;          //������ ����������� ��������/����
typedef map<int, UnicodeString> TPlanVacTypeList;          //������ ����������� ��������/����
typedef TPlanItemList::iterator TPlanItemListIt;        //�������� ��� ������ ����������� ��������/����
//---------------------------------------------------------------------------
//������� ������-������ ��� ��������� ����� �������� ����
typedef struct tagTRACE_ITEM {
  UnicodeString    SchemeUID;              //UID �����
  UnicodeString    LineUID;                //UID ������
  UnicodeString    Name;                   //������ ����������� ������ ����� ( <������������ �����>.<������������ ������> )
  int           RefOwner;               //��� ��������������� ���� ��� �����
  TJumpKind     JumpKind;               //��� ��������, � ���������� �������� ������ ������� ����� � ������
  UnicodeString    JumpReason;             //������� ���������� ��������, � ���������� �������� ������ ������� ����� � ������

  tagTRACE_ITEM() : SchemeUID(""),
                    LineUID(""),
                    Name(""),
                    RefOwner(-1),
                    JumpKind(jkUnknown),
                    JumpReason("") {};
}TRACE_ITEM, *PTRACE_ITEM;

//---------------------------------------------------------------------------
typedef list<TRACE_ITEM>         TJumpTraceList;        //������-������ ��� ��������� ����� �������� ����
typedef TJumpTraceList::iterator TJumpTraceListIt;      //�������� ��� ������ ��� ��������� ����� �������� ����
//---------------------------------------------------------------------------
//������� ������ ���������� ��������/����/��������/�������
typedef struct tagCard_ITEM {
  int             ItemId;           // ��� ����/�����/��������/��� ������
  UnicodeString   ItemType;         // ��� ��������/�����
  UnicodeString   ItemSch;          // ����� ��������/�����/��������
  int             InfId;            // ��� ��������
  UnicodeString   ItemName;         // ������������ �������/�����/��������/���� ������
  UnicodeString   InfName;          // ������������ ��������
  TDate           Date;             // ���� ���������� �������/�����/��������, ���� ������ ������
  TDate           EndDate;          // ���� ��������� ��� ���������� ������
  int             Type;             // ������� ���� ���  ��������/�����/
  tagCard_ITEM(): ItemId(-1),
                  ItemType(""),
                  ItemSch(""),
                  InfId(-1),
                  ItemName(""),
                  InfName(""),
                  Date(0),
                  EndDate(0),
                  Type(-1){};
}CARD_ITEM, *PCARD_ITEM;
//---------------------------------------------------------------------------
typedef map<int, CARD_ITEM>   TCardItemMap;       //������ ���������� ��������/����/��������/�������
//---------------------------------------------------------------------------
//������� ������ ����������� ��������
typedef struct tagINF_ITEM {
  int             Id;                // ��� ��������
  int             Type;              // ��� 0 - ����, 1- ���
  UnicodeString   TurSch;            // ����� ��� ����
  tagINF_ITEM() : Id(-1),
                  Type(-1),
                  TurSch("") {};
  tagINF_ITEM(int AId, int AType, UnicodeString ATurSch) : Id(AId),
                                                           Type(AType),
                                                           TurSch(ATurSch) {};
}INF_ITEM;
//---------------------------------------------------------------------------
typedef list<INF_ITEM>         TInfItemList;          //������ ����������� ��������
//---------------------------------------------------------------------------
extern const UnicodeString PatLName;           // �������� ���� "�������"
extern const UnicodeString PatName;            // �������� ���� "���"
extern const UnicodeString PatSName;           // �������� ���� "��������"
extern const UnicodeString PatBitrhDay;        // �������� ���� "���� ��������"
//---------------------------------------------------------------------------
typedef struct tagPrePlanRec
{
  __int64       UCode;        // ��� ��������' uid='1113' ref='1000' depend='1113.1113' isedit='0' fieldtype='integer'/>
  TDate         PatBirthDay;     // ���� �����' uid='111B'/>
  short         Brig;         // ��������� ����������' uid='3193'/>
  TDate         PrePlanDate;  // ���� ���������������� �����' uid='1115' inlist='l'/>
  short         PlanItemType; // ���' uid='1116' default='1'>          <choicevalue name='��������' value='1'/>          <choicevalue name='�����' value='2'/>          <choicevalue name='��������' value='3'/>        </choice>
  UnicodeString Fam,Name,SName;      // FIO
  int           SupportState;          // choice name='&amp;������ ������������' uid='32B3' required='1' inlist='l' default='0'>
  UnicodeString OrgLev;      // ������ �����������
  UnicodeString VacType;      // ���/��������' uid='1117' inlist='l' cast='no' length='10'/>
  UnicodeString AllVacType;   // inf1.VacType;inf2.VacType; ...
  int           Inf;          // ��������' uid='3187' ref='003A' inlist='l'/>
  __int64       MIBP;         // ����' uid='1118' ref='0035' inlist='l'/>
  int           MIBPType;     //  0  - ����� �������, 1 - ����� �����������  2 - ������ ���������
  int           Prior;        // ���������' uid='1119' inlist='l' digits='4' min='-999' max='9999'/>
  TPlanVacTypeList TypeList;
                              // � �����' uid='111A'/>
  TDate         PlanDate;     // ���� �����' uid='111B'/>
  TDate         PlanMonth;
  int           Src;          // ��������' uid='111C' inlist='l' default='0'>          <choicevalue name='����������' value='0'/>          <choicevalue name='������ ����������' value='1'/>          <choicevalue name='����������� �� ���������' value='2'/>          <choicevalue name='����������� �� �� ���������' value='3'/>          <choicevalue name='����������� ��� ���������������' value='4'/>          <choicevalue name='����������� �� ����' value='5'/>          <choicevalue name='����������� �� ���� ��� ���������������' value='6'/>
  bool          InPlan;
  tagPrePlanRec() {};
  UnicodeString InfVacType()
  {
    return IntToStr(Inf)+"."+VacType+";";
  };
  tagPrePlanRec(TFDQuery *AQ)
  {
    UCode        = AQ->FieldByName("R1113")->AsInteger;
    Fam          = AQ->FieldByName("R001f")->AsString;
    Name         = AQ->FieldByName("R0020")->AsString;
    SName        = AQ->FieldByName("R002f")->AsString;
    SupportState = AQ->FieldByName("R32B3")->AsInteger;
    OrgLev       = AQ->FieldByName("R0026")->AsString+
    "$"+AQ->FieldByName("R0027")->AsString+
    "$"+AQ->FieldByName("R0028")->AsString;
    PatBirthDay  = AQ->FieldByName("R0031")->AsDateTime;
    Brig         = AQ->FieldByName("R3193")->AsInteger;
    PrePlanDate  = AQ->FieldByName("R1115")->AsDateTime;
    PlanMonth    = AQ->FieldByName("R31A2")->AsDateTime;
    PlanItemType = AQ->FieldByName("R1116")->AsInteger;
    VacType      = AQ->FieldByName("R1117")->AsString;
    Inf          = AQ->FieldByName("R3187")->AsInteger;
    MIBP         = AQ->FieldByName("R1118")->AsInteger;

    if (PlanItemType == 1)
     {//R1116 = �������� - 1,  ����� - 2,  �������� - 3
       MIBPType = (int)(MIBP/10000);
       MIBP     -= 10000*MIBPType;
     }
    else
     MIBPType = 0;

    Prior        = AQ->FieldByName("R1119")->AsInteger;
                              // � �����' uid='111A'/>
    PlanDate     = AQ->FieldByName("R111B")->AsDateTime;
    Src          = AQ->FieldByName("R111C")->AsInteger;
    InPlan       = false;
    AllVacType   = InfVacType();
  };
  tagPrePlanRec(tagPrePlanRec *ASrc)
  {
    UCode        = ASrc->UCode;
    Fam          = ASrc->Fam;
    Name         = ASrc->Name;
    SName        = ASrc->SName;
    OrgLev       = ASrc->OrgLev;
    SupportState = ASrc->SupportState;
    PatBirthDay  = ASrc->PatBirthDay;
    Brig         = ASrc->Brig;
    PlanMonth    = ASrc->PlanMonth;
    PrePlanDate  = ASrc->PrePlanDate;
    PlanItemType = ASrc->PlanItemType;
    VacType      = ASrc->VacType;
    Inf          = ASrc->Inf;
    MIBP         = ASrc->MIBP;
    MIBPType     = ASrc->MIBPType;
    Prior        = ASrc->Prior;
//    TypeList = TPlanVacTypeList(ASrc->TypeList);
                              // � �����' uid='111A'/>
    PlanDate     = ASrc->PlanDate;
    Src          = ASrc->Src;
    InPlan       = ASrc->InPlan;
    AllVacType   = ASrc->AllVacType;
  };
  UnicodeString ToString()
  {
    return ", UCode:"+IntToStr(UCode)+
    ", FIO:"+Fam+
    " "+Name+
    " "+SName+
    " "+OrgLev+
    ", SupState:"+IntToStr(SupportState)+
    ", Brig:"+IntToStr(Brig)+
    ", PrePlanDate:"+PrePlanDate.FormatString("dd.mm.yyyy")+
    ", PlanItemType:"+IntToStr(PlanItemType)+
    ", VacType:"+VacType +
    ", Inf:"+IntToStr(Inf)+
    ", MIBP:"+IntToStr(MIBP)+
    ", Prior:"+IntToStr(Prior)+
    ", PlanDate:"+PlanDate.FormatString("dd.mm.yyyy")+
    ", Src:"+IntToStr(Src);
  };
}
TPrePlanRec;
typedef map<UnicodeString, TPrePlanRec*> TUnitPrePlan;
typedef map<int, TPrePlanRec*> TUnitPriorPrePlan;
//typedef list<int> TMIBPInfList;
//typedef map <int, TMIBPInfList> TMIBPList;
//---------------------------------------------------------------------------
struct gtint
{
  bool operator()(int  v1, int v2) const
  {
    return (v1 > v2);
  }
};
//---------------------------------------------------------------------------
typedef map<int,int, gtint> TIntMap;
typedef map<int, TIntMap*> TIntListMap;
//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                              Globals                                  ##
//##                                                                       ##
//###########################################################################


//�������������� ������� ��� ���������� ������ ��������/����/��������/�������
struct CardItemGtDate : public binary_function<TCardItem, TCardItem, bool>
{
  bool operator() (TCardItem x, TCardItem y);
};
struct CardItemLtDate : public binary_function<TCardItem, TCardItem, bool>
{
  bool operator() (TCardItem x, TCardItem y);
};
struct CardItemGtEndDate : public binary_function<TCardItem, TCardItem, bool>
{
  bool operator() (TCardItem x, TCardItem y);
};
struct CardItemLtEndDate : public binary_function<TCardItem, TCardItem, bool>
{
  bool operator() (TCardItem x, TCardItem y);
};
//�������������� ������� ��� ���������� ������ ����������� ��������/����
/*struct PlanItemListLessOwner : public binary_function<PLAN_ITEM, PLAN_ITEM, bool>
{
  bool operator() (PLAN_ITEM x, PLAN_ITEM y);
};
   */
struct PlanItemListGreaterPriority : public binary_function<PLAN_ITEM, PLAN_ITEM, bool>
{
  bool operator() (PLAN_ITEM x, PLAN_ITEM y);
};

struct PlanItemListLessPlanDate : public binary_function<PLAN_ITEM, PLAN_ITEM, bool>
{
  bool operator() (PLAN_ITEM x, PLAN_ITEM y);
};

struct PlanItemListGreaterPlanDate : public binary_function<PLAN_ITEM, PLAN_ITEM, bool>
{
  bool operator() (PLAN_ITEM x, PLAN_ITEM y);
};


//---------------------------------------------------------------------------
class TdsPrePlanerDM : public TDataModule
{
__published:	// IDE-managed Components
private:	// User declarations
/*
typedef map <int, TMIBPInfList> TMIBPList;
*/
//  typedef list<int,gtint> TIntList;

//  typedef map <UnicodeString, TStringList*> TListMap;
  TFDConnection *FConnection;
//  TDate StartCreatePlanBegDate, StartCreatePlanEndDate, StartCreatePlanPlanMonth;
//  TJSONObject *StartCreatePlanValue;


  TTagNode *FRegDef;
  void __fastcall FSetRegDef(TTagNode *AVal);

  TTagNode *FUnitDataDef;
  bool __fastcall FSetFieldsDef(TTagNode *ANode, UnicodeString &ATmp);

  TAxeXMLContainer *FXMLList;

  TdsDocSQLCreator *FSQLCreator;

  TTagNode *FVacSchXML;
  TTagNode *FTestSchXML;
  TTagNode *FCheckSchXML;
  TTagNode *FMIBPAgeDef;


  TdsRegClassData *FClassData;
  TICSIMMNomDef      *FImmNom;
  TFDQuery* FImmNomQuery;
  TFDQuery* FClassDataQuery;
  UnicodeString FPlanRecCode;
  int FCommPlanCount, FCurrPlan;
  TdsSrvClassifDataMap  FClassifData;
  TdsOnPlanGetValByIdEvent  FOnPlanGetValById;
  TdsOnPlanGetValById10Event  FOnPlanGetValById10;
  TdsOnPlanInsertDataEvent  FOnPlanInsertData;
  TdsOnPlanGetMIBPListEvent FOnGetMIBPList;
  TdsOnPlanCreateUnitPlanEvent FOnCreateUnitPlan;
  TdsPlanGetStrValByCodeEvent  FOnGetOrgStr;
//  TAnsiStrMap   FSelectSQLMap;
//  TAnsiStrMap   FClassSQLMap;
//  TAnsiStrMap   FSelectRecSQLMap;
//  TAnsiStrMap   FCountSQLMap;
//  TAnsiStrMap   FDeleteSQLMap;
//  TAnsiStrMap   FKeyFlMap;
//  TListMap      FFields;
//  TAnsiStrMap   FSelectSQLMap;
//  TAnsiStrMap   FSelectSQLMap;
  TUnitPrePlan   *FUnitPrePlan;

  TIntMap         *FStoreMIBPList;
  TIntMap         *FDefStoreMIBPList;
  TIntMap         *FSortedStoreMIBPList;
  TIntMap         *FDefSortedStoreMIBPList;
  TIntMap         *FStoreTestList;
  TIntMap         *FDefStoreTestList;
//  void          __fastcall UnionPriviv(TUnitPrePlan *ASrc, TUnitPrePlan *ADest, int APlanType);
  UnicodeString __fastcall FGetDepVal(UnicodeString AVals, UnicodeString AUID );
  bool __fastcall FDepValIsNull(UnicodeString AVals);
  UnicodeString __fastcall GetDependValSQL(UnicodeString ARef, bool AEmptyIfNoDepend);
  void __fastcall GetDependClassCode(UnicodeString ADepClName, UnicodeString ADepFlVal, UnicodeString ARetFlName, UnicodeString &ARetVals);

  void __fastcall CheckSQLs(UnicodeString AId);
  void __fastcall GetSQLs(UnicodeString AId);
  UnicodeString __fastcall GetKeyFl(UnicodeString AId);
  UnicodeString __fastcall GetFlNameById(UnicodeString ATabId, UnicodeString AFlId);
  UnicodeString __fastcall GetTabName(UnicodeString AId);
  UnicodeString __fastcall GetCountSQL(UnicodeString AId);
  UnicodeString __fastcall GetDeleteSQL(UnicodeString AId);
  UnicodeString __fastcall GetClassSQL(UnicodeString AId);
  UnicodeString __fastcall GetSelectSQL(UnicodeString AId);
  UnicodeString __fastcall GetSelectRecSQL(UnicodeString AId);
  UnicodeString __fastcall GetSelectRec10SQL(UnicodeString AId);
  UnicodeString __fastcall GetOrderSQL(UnicodeString AId);
  TStringList*  __fastcall GetFieldsList(UnicodeString AId);
  bool          __fastcall IsQuotedKey(UnicodeString AId);
  UnicodeString __fastcall GetWhereFromFilter (TTagNode *AFlt, UnicodeString AMainISUID);
  void          __fastcall FRegGetSVal(UnicodeString AFuncName, TTagNode *ANode, UnicodeString &ATab, UnicodeString &ARet);

  UnicodeString __fastcall FGetInfName(UnicodeString ACode);
  UnicodeString __fastcall FGetVacName(UnicodeString ACode);
  UnicodeString __fastcall FGetTestName(UnicodeString ACode);
  UnicodeString __fastcall FGetCheckName(UnicodeString ACode);

  int __fastcall FGetVPrior(int ACode);
  int __fastcall FGetRVPrior(int ACode);
  void __fastcall FDocGetSVal(UnicodeString AFuncName, TTagNode *ANode, UnicodeString &ATab, UnicodeString &ARet);
  void _fastcall LoadPlanSetting();

  void __fastcall ClearHash();
public:		// User declarations
  __fastcall TdsPrePlanerDM(TComponent* Owner, TFDConnection *AConnection, TAxeXMLContainer *AXMLList);
  __fastcall ~TdsPlanDM();
 UnicodeString __fastcall VacNameStr(__int64 ACode);
 UnicodeString __fastcall InfNameStr(__int64 ACode);

  TPreTestItemList PreTestList;
//  void          __fastcall AddUnitPlan(TFDQuery *AQ, __int64 APlanCode, TUnitPrePlan *AUnitPrePlan, int APlanType);
  UnicodeString __fastcall GetOrgName(TJSONObject *AValue);
//  UnicodeString __fastcall GetOrgFullCode(__int64 ACode);
  UnicodeString __fastcall GetUchName(TJSONObject *AValue);
  UnicodeString __fastcall GetFltName(TJSONObject *AValue);
  __int64 __fastcall GetNewCode(UnicodeString TabId);
  UnicodeString __fastcall NewGUID();
  int __fastcall GetBrigExec(int APlanItemType, __int64 AInfId);
  UnicodeString __fastcall GetDefSch(__int64 AInf, int AType);

//  Variant __fastcall JSONToVar(TJSONObject *AValue, UnicodeString AId, Variant ADef = Variant::Empty());
//  __int64 __fastcall JSONToInt(TJSONObject *AValue, UnicodeString AId, __int64 ADef = 0);
//  System::UnicodeString __fastcall JSONToString(TJSONObject *AValue, UnicodeString AId, UnicodeString ADef = "");

  UnicodeString __fastcall GetClassData(long ACode, UnicodeString AName);
  void          __fastcall GetMIBPFromStore();
  TJSONObject* __fastcall GetDefMIBP(int AType);
  bool __fastcall MIBPInStore(__int64 AMIBPCode);
  bool __fastcall MIBPInDefStore(__int64 AMIBPCode);
//  bool __fastcall CheckMIBPPlanCompat(__int64 AMIBPCode);
//  bool __fastcall CheckCompatible(__int64 AMIBP1, __int64 AMIBP2);
  TAnsiStrMap VacList;
  TIntMap     FVPrior;
  TIntMap     FRVPrior;
  TTagNode    *FPlanOpt;
  TTagNode    *FPlanOptDefSch;
  TAnsiStrMap TestList;
  TAnsiStrMap InfList;
  TAnsiStrMap CheckList;

  bool __fastcall CondCheck(TTagNode *nFltId, int APtCode);

  TIntListMap InfVacList;
  TIntListMap InfProbList;
//    TAnsiStrMap ProfList;
  TIntListMap VacInfList;
  TIntListMap ProbInfList;

  bool __fastcall Connect();
  bool __fastcall Disconnect();

  UnicodeString __fastcall GetPlanTemplateDef(int AType);
  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AParam1Name, UnicodeString AParam1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AParam2Name = "", Variant AParam2Val = 0,
                               UnicodeString AParam3Name = "", Variant AParam3Val = 0,
                               UnicodeString AParam4Name = "", Variant AParam4Val = 0);


  __int64       __fastcall GetCount(UnicodeString AId, System::UnicodeString AFilterParam);
  TJSONObject*  __fastcall GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam);
  TJSONObject*  __fastcall GetValById(System::UnicodeString AId, System::UnicodeString ARecId);
  TJSONObject*  __fastcall GetValById10(System::UnicodeString AId, System::UnicodeString ARecId);
  TJSONObject*  __fastcall Find(System::UnicodeString AId, TJSONObject *AFindParam);
  UnicodeString __fastcall DeleteData(System::UnicodeString AId, System::UnicodeString ARecId);
  UnicodeString __fastcall DeletePlan(System::UnicodeString ARecId);
  UnicodeString __fastcall SetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString AVal);
  UnicodeString __fastcall GetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, UnicodeString &AMsg);
  void          __fastcall GetUnitDopPlanCodes(__int64 AUCode, UnicodeString &AAddrCode, UnicodeString &AOrgCode, UnicodeString &AUchCode, UnicodeString &AProfCode);
  UnicodeString __fastcall GetPatPlanOptions(__int64 APatCode);
  UnicodeString __fastcall GetUnitSch(__int64 AUCode);
  UnicodeString __fastcall GetDefUnitSch(__int64 AUCode);
  void __fastcall SetUnitSch(__int64 AUCode, UnicodeString ASch);
  bool __fastcall InfInVac(UnicodeString AInf, UnicodeString AVac);
  bool __fastcall InfInTest(UnicodeString AInf, UnicodeString ATest);
  bool __fastcall GetMIBPInfList(int AMIBPCode, TIntMap *& ARC);
  int __fastcall  GetMIBPInfCount(int AMIBPCode);
  bool __fastcall GetInfMIBPList(int AInfCode, TIntMap *& ARC);
//  bool __fastcall InfInPlanVac(TUnitPriorPrePlan *APlanVac, int AInfCode, int &AInfPrior, bool &AAlreadyInPlan);
//  System::UnicodeString __fastcall CreatePlan(TDate ABegDate, TDate AEndDate, TDate APlanMonth, TJSONObject *AValue, System::UnicodeString &ARecId);
//  System::UnicodeString __fastcall StartCreatePlan(TDate ABegDate, TDate AEndDate, TDate APlanMonth, TJSONObject *AValue);
  System::UnicodeString __fastcall GetPlanProgress();
  System::UnicodeString __fastcall GetPlanOpt();
  void __fastcall SetPlanOpt(UnicodeString AOpts);
  bool __fastcall CheckMIBPByAge(TDate APatBirthDay, TDate FPrePlanDate, __int64 FMIBPCode);

  __property TTagNode *RegDef = {read = FRegDef, write=FSetRegDef};
  __property UnicodeString InfName[UnicodeString ACode] = {read=FGetInfName};
  __property UnicodeString VacName[UnicodeString ACode] = {read=FGetVacName};
  __property UnicodeString TestName[UnicodeString ACode] = {read=FGetTestName};
  __property UnicodeString CheckName[UnicodeString ACode] = {read=FGetCheckName};
  __property TTagNode *PlanOpt = {read=FPlanOpt};
  __property TTagNode *UnitDataDef = {read=FUnitDataDef};

  __property TTagNode *VacSchXML = {read=FVacSchXML};
  __property TTagNode *TestSchXML = {read=FTestSchXML};
  __property TTagNode *CheckSchXML = {read=FCheckSchXML};

  __property int VPrior[int ACode] = {read=FGetVPrior};
  __property int RVPrior[int ACode] = {read=FGetRVPrior};

  __property TAxeXMLContainer* XMLList = {read=FXMLList};

  __property TdsOnPlanInsertDataEvent  OnPlanInsertData = {read=FOnPlanInsertData, write=FOnPlanInsertData};
  __property TdsOnPlanGetValByIdEvent  OnGetValById     = {read=FOnPlanGetValById, write=FOnPlanGetValById};
  __property TdsOnPlanGetValById10Event  OnGetValById10     = {read=FOnPlanGetValById10, write=FOnPlanGetValById10};
  __property TdsOnPlanGetMIBPListEvent OnGetMIBPList    = {read=FOnGetMIBPList, write=FOnGetMIBPList};
  __property TdsOnPlanCreateUnitPlanEvent OnCreateUnitPlan    = {read=FOnCreateUnitPlan, write=FOnCreateUnitPlan};
  __property TdsPlanGetStrValByCodeEvent  OnGetOrgStr = {read = FOnGetOrgStr, write=FOnGetOrgStr};
  __property UnicodeString PlanRecCode = {read = FPlanRecCode};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsPrePlanerDM *dsPrePlanerDM;
//---------------------------------------------------------------------------
extern PACKAGE void __fastcall XMLStrToDuration(UnicodeString AValue, int *AYears, int *AMonths, int *ADays);
extern PACKAGE TDate __fastcall IncDate(TDate ABase, UnicodeString APeriod);
//extern bool CondCheck(TTagNode *nFltId, int APtCode);
extern PACKAGE bool __fastcall IsNullSch(UnicodeString ASch);

extern PACKAGE const UnicodeString csReqFldNULL;

extern PACKAGE const int nMaxVPriority;
extern PACKAGE const int nMinVPriority;
extern PACKAGE const int nMaxRVPriority;
extern PACKAGE const int nMinRVPriority;
extern PACKAGE TDate __fastcall CalculatePlanPeriod(TDate ADate, TDate &ADateBP, TDate &ADateEP);
//---------------------------------------------------------------------------
#endif
