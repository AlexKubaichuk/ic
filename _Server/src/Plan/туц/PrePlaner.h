//          <choicevalue name='����������' value='0'/>
//          <choicevalue name='������ ����������' value='1'/>
//          <choicevalue name='����������� �� ���������' value='2'/>
//          <choicevalue name='����������� �� �� ���������' value='3'/>
//          <choicevalue name='����������� ��� ���������������' value='4'/>
//          <choicevalue name='����������� �� ����' value='5'/>
//          <choicevalue name='����������� �� ���� ��� ���������������' value='6'/>
//---------------------------------------------------------------------------
/*
  ����        - Planer.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - �����������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 09.05.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  16.05.2007 - 17.05.2007
    [+] ������������� ��������, ����������� �������������, �� ������� ���������
        ������������ ���� �� �� ����� �������� ��������������� �����������
    [+] ������ ��������, ����������� �������������, �� ������� �������� ����
        �� ���������

  13.03.2007
    [!] ��� ������� � �������� ���� ����� AsBoolean

  09.03.2007
    [!] ��� ������������ ����������� �������� �� ������� - ��� ������ �������
        ������������ ������������� ��������������� �����
    [!] ��� ��������� ������������ ��������� ���� VACTYPE ��� ��������������
        �������� � ����
    [+] ��������� ������������ �������� ��� ��������

  26.02.2007
    [*] kab

  01.06.2006
    [!] ��� ������������� ��������, ����������� ������������� �� ������� �������
        ��������� ���� ���� �� ��� ����� �������� ��������������� �����������
        ������� ��������

  18.05.2006
    [+] ��������� ������� �������� ��� ��������� �������� �� ����
    [+] �������� ���� ��� �������� � �������� �� �����������
        ������������
    [+] ������������� ��������, ����������� ������������� �� ������� �������
        ��������� ���� ���� �� ��� ����� �������� ��������������� �����������
        ������� ��������
    [!] � ������� CorrectPolyVak() � CorrectPrivivPlanDate() ��� �������� ��������

  03.05.2006 - 05.05.2006
    [+] ��������� ������� ��������
    [!] ��� ��������� � ���� ���������� (��������������� ����)

  25.04.2006 - 27.04.2006
    [!] ��� ��������� �������� "min_age" ���� "line" ��� ����

  19.04.2006
    [+] ��������� �������� ������������

  24.03.2006
    [!] ��� ��������� �������� �� ��������� ������������

  23.03.2006
    [-] ��������� � ���� �������������� �������� �� ���������� ����� ����������� ����
    [+] ��������� ���� F_PLAN ������� CARD_1084

  18.03.2006
    [+] ��������� �������� �� ��������� ������������
    [*] ����� ������ ������������ xml-����� ����������� TTagNode �
        ��������������� xml-����������
    [-] �������� VacSchXMLFileName
    [-] �������� ProbSchXMLFileName
    [*] �����������


  05.11.2005
    [+] ���� ���������� ����� "������������� ��� ������������ ����������
        ��������"

  11.11.2005
    [*] ��������� � ������ ������������ �������������� �� �������� �� �����
        �������� �� ����� ���������� ����� "������������� ��������,
        ����������� �������������" - ��� ����������� ������ ���� ��������� ��
        �����������

  11.11.2005 -  kab
    [+] ���� ��������� ������ ������������  TPlanErrMsg

  08.11.2005 - 10.11.2005
    [!] ���������� ������ ��� ���������� ���������������� �����
    [+] C������� TPrePlaner::VacSchXML  - ����������������� � VacSchXMLFileName
    [+] ������� TPrePlaner::OnPlanerStageChange
    [+] ���� ���������� ����� "������������� ��������, ����������� �������������"
    [+] ������ ������ ������������ - �������� TPrePlaner::ErrLog
    [*] ��� ������������ ������ � ������� TPrePlaner ������ DKException
        ��������� EIMMPlanerError
    [*] ����������� + �����������

  14.07.2005
    1. ��������� ����, ��������� � ������������ ��������� ����, �����������
       � ���������������� � ������ TPrePlaner::AppendPrepareProb()

  21.02.2005
    1. ��� ��������� �������� �� ����� (����� TPrePlaner::GetCurSchemeLine)
       ������ �� ����������� ������������

  22.12.2004
    1. ��������� ��������� ����� ����� ( R1098 � VAKTYPE ������� CARD_1084 - "����" )

  30.11.2004
    1. �������� ����� "������������" ������������

  26.11.2004
    1. � ������������ ������� ��������� ����������, ���� �������� ADB == NULL.
       ��� ������� ��� ����, ����� ����� ���� ������������ �����������
       ��� ����������� �������� ������� ������������

  09.05.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef PrePlanerH
#define PrePlanerH

#include "DKCfg.h"      // ��������� �������� ����������

#include <System.hpp>
#include <Classes.hpp>
#include <DateUtils.hpp>
//#include "pFIBDatabase.hpp"
#include "DKClasses.h"
#include "XMLContainer.h"
#include "ICSDATEUTIL.hpp"
#include "DKUtils.h"

#include "dsPlanDMUnit.h"
#include "PlanerDebug.h"

#include "KabCustomDSRow.h"
/*//������� (������)
typedef struct tagDKAGE {
  Word Years;     //���������� ���
  Word Months;      //���������� �������
  Word Days;      //���������� ����
}DKAGE, *PDKAGE;
  */

//---------------------------------------------------------------------------
typedef void __fastcall (__closure *TdsSyncMethod)(void);
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
/*
//�������������� ������� ��� ������ � ������ ����������� ��������/����
struct SelectPlanItemOwnerId : public unary_function<PLAN_ITEM, int>
{
  const int& operator()(const PLAN_ITEM &x) const { return x.OwnerId; }
};

struct SelectPlanItemInfekId : public unary_function<PLAN_ITEM, int>
{
  const int& operator()(const PLAN_ITEM &x) const { return x.InfekId; }
};
*/


//�������������� ������� ��� ������ � ������ ��� ��������� ����� �������� ����
struct SelectTraceItemLineUID : public unary_function<TRACE_ITEM, UnicodeString>
{
  UnicodeString  operator()(const TRACE_ITEM &x) const { return x.LineUID; }
};


//###########################################################################
//##                                                                       ##
//##                           ������� �������                             ##
//##                                                                       ##
//###########################################################################

/*
 * �������� ������� �� �������
 *
 *
 * ���������:
 *   [in]  nFltId - ������
 *   [in]  APtId  - ��� ��������
 *
 * ������������ ��������:
 *   true  - ������� �����������
 *   false - ������� �� �����������
 *
 */

//extern bool CondCheck(TTagNode *nFltId, int APtId);

//###########################################################################
//##                                                                       ##
//##                            TIMMJumpTrace                              ##
//##                                                                       ##
//###########################################################################

// ������ ��������� ����� �������� ����

class TIMMJumpTrace : public TObject
{
private:
  TdsPlanDM *FDM;
public:
    __fastcall TIMMJumpTrace(TdsPlanDM *ADM);
    virtual __fastcall ~TIMMJumpTrace();

    TPlanItemKind   TraceKind;
    TJumpTraceList  JumpTraceList;

    inline void __fastcall Clear();
    void __fastcall Append( TTagNode *ASchemeLine, TJumpKind AJumpKind, UnicodeString AJumpReason  );
    bool __fastcall FindByLineUID( UnicodeString ALineUID, TJumpTraceListIt *AJumpTraceListIt = NULL );
    int __fastcall GetTraceStrings( /*TpFIBQuery *fq, */TStringList *TraceStrs );
};

//###########################################################################
//##                                                                       ##
//##                           EIMMPlanerError                             ##
//##                                                                       ##
//###########################################################################

// ��������� ��� ������������ ������ � ������� TPrePlaner
class EIMMPlanerError : public DKClasses::EMethodError
{
    typedef DKClasses::EMethodError inherited;

public:
    #pragma warn -inl
    inline __fastcall EIMMPlanerError(
      const UnicodeString MethodName,
      const UnicodeString Description
    ) : DKClasses::EMethodError(MethodName, Description) {}
    inline __fastcall virtual ~EIMMPlanerError(void) {}
    #pragma warn .inl
};
//###########################################################################
//##                                                                       ##
//##                              TPrePlaner                               ##
//##                                                                       ##
//###########################################################################

//class TPlanerDM;

// ���� ������������
enum TPlanerStage
{
  psInitialization,             // �������������
  psPlanDop,                    // ������������ ����� ��� �������������� �������� ��� �����
  psUpdatePlans,                // ���������� ������ ( �������� ����.����������� � ��������
                                // ���������, ���� ������������ �������������� �� ������ ����� )
  psPlanPriviv,                 // ���� �� ��������� ��� ������������ ��������
  psPlanProb,                   // ���� �� ��������� ��� ������������ �����
  psAppendDopPrivivByPrevProb,  // ��������� � ���� �������������� �������� �� ���������� ����� ����������� ����
  psCorrectPolyVak,             // ������������� ��������, ����������� �������������
                                // ����������� ���� �������������� � ����� ������������� ����
  psAppendPrepareProb,          // ��������� � ���� ���������� (��������������� ����)
  psCorrectPrivivPlanDate,      // ������������� ��� ������������ ���������� �������� � ������������ � �������
                                // ����� ������� � ����������
  psApplyPlan                   // ��������� ��������������� ����� � ��
};

typedef void __fastcall (__closure *TOnPlanerStageChange)( TObject* Sender, TPlanerStage NewPlanerStage );

//---------------------------------------------------------------------------
/*
class PACKAGE TUnitPrePlanerData : public TObject
{
private:
    __int64         FPtId;               // ��� ��������

    DKAGE           FPtAge;              // ������� �������� ��� �������� �������������� ������������
    TDate           FPtBirthDay;         // ���� �������� ����� ��������
    UnicodeString   FPtFIO;              // ��� ����� ��������

    TCardItemList   FCardPrivList;       // ������ ��������� ��������
    TCardItemList   FCardTestList;       // ������ ��������� ����
    TCardItemList   FCardCheckList;      // ������ ��������� ��������
    TCardItemList   FCardCancelList;     // ������ ��������� �������

    TPlanItemDateList FVacDateByInfComm;
    TPlanItemDateList FVacDateByInf;
    TPlanItemDateList FVacDateByTurInf;
    TPlanItemDateList FTestDateByTest;
    TPlanItemDateList FTestDateByInf;
    TPlanItemDateList FCheckDateByInf;

    TPlanItemDateList FVacOtvodDateByInf;
    TPlanItemDateList FTestOtvodDateByInf;
    TPlanItemDateList FConstVacOtvodDateByInf;
    TPlanItemDateList FConstTestOtvodDateByInf;


  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;

  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;

    void __fastcall GetUnitCardData();

    bool __fastcall GetPtAge(Word &Years, Word &Months, Word &Days);

    bool __fastcall FGetVacDateByType(int AInfId, bool ARoundMode, TPlanItemKind APlanItemKind, TDate &ADate, UnicodeString AVacType);

    void __fastcall AppendPrepareProb();
    void __fastcall ClearPlan();
    void __fastcall ApplyPlan(TPlanItemKind PlanItemKind, bool IsMain);
  TJSONObject* __fastcall FGetValues(PLAN_ITEM AItem);
  TdsPlanDM   *FDM;

  TkabCustomDataSetRow *FUnitData;
  TTagNode *FUnitPlanSetting;

  bool __fastcall GetPauseValue(int &APause, UnicodeString AType1, UnicodeString ARef1, UnicodeString AType2, UnicodeString ARef2);
  bool __fastcall GetInfInfPause(int &APause, UnicodeString ARef1, UnicodeString ARef2);
  bool __fastcall GetTestInfPause(int &APause, UnicodeString ARef1, UnicodeString ARef2);
  bool __fastcall GetTestTestPause(int &APause, UnicodeString ARef1, UnicodeString ARef2);
  bool __fastcall GetInfTestPause(int &APause, UnicodeString ARef1, UnicodeString ARef2);
  int __fastcall GetMaxTestPause(int ATestCode);
  int __fastcall GetMaxPauseByMonth(TDate ADate);

  TDate __fastcall GetPlanItemEarliestDate(int AInfId, TPlanItemKind APlanItemKind);
  void __fastcall CorrectPlanDateByMonth();
protected:
  void __fastcall SyncCallLogMsg();
  void __fastcall SyncCallLogErr();
  void __fastcall SyncCallLogBegin();
  void __fastcall SyncCallLogEnd();

  void __fastcall CallLogMsg(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogErr(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogBegin(UnicodeString AFuncName);
  void __fastcall CallLogEnd(UnicodeString AFuncName);
  void __fastcall CallSaveDebugLog(TObject *Sender);
  void __fastcall SyncCallSaveDebugLog();

    void __fastcall SetVacSchXML(TTagNode* AValue);
    void __fastcall SetTestSchXML(TTagNode* AValue);
    void __fastcall SetCheckSchXML(TTagNode* AValue);
    void __fastcall SetPtId(__int64 Value);
    void __fastcall FGetPatData();
//    void __fastcall SetDopIDKind(TDopIDKind AValue);
//    void __fastcall SetFltPlanKind(TFltPlanKind AValue);


public:



    System::UnicodeString __fastcall StartCreateUnitPlan(TDate ABegDate, __int64 APtCode);
    System::UnicodeString __fastcall StopCreatePlan();
    __property bool Terminated = {read = FTerminated};
    __property bool Planing = {read = FPlaning};
    int __fastcall GetMaxProgress();
    int __fastcall GetCurProgress();
    UnicodeString __fastcall GetCurStage() const;

    __fastcall TUnitPrePlanerData(TdsPlanDM   *ADM);
    virtual __fastcall ~TUnitPrePlanerData();

    __property __int64      PtId = { read = FPtId, write = SetPtId };

    // ������� ��������, ��� �������� �������������� ������������
    __property DKAGE        PtAge = { read = FPtAge };
    //���� �������� ��������, ��� �������� �������������� ������������
    __property TDate        PtBirthDay = { read = FPtBirthDay };

    // ��������� ��� ������ ���-�
    __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
    __property TOnLogMsgErrEvent OnLogError = {read = FOnLogError, write = FOnLogError};

    __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
    __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};

    __property UnicodeString PCRecId = { read = FPCRecId };
};

//---------------------------------------------------------------------------
*/
class PACKAGE TPrePlaner : public TObject
{
private:
    // ������������� ��������
/*    struct TInfIncpbltItem
    {
      int Id1;  // ID 1-� ��������
      int Id2;  // ID 1-� ��������
      TInfIncpbltItem() : Id1(0), Id2(0) {}
      TInfIncpbltItem(int AId1, int AId2) : Id1(AId1), Id2(AId2) {}
    };
    // ��������
    // InfIncpbltItemEqualTo(Item)(TestItem)
    // ����������, �������� �� �������� TestItem �������������� �� ���������
    // �������� Item
    struct InfIncpbltItemEqualTo : public unary_function<const TInfIncpbltItem&, bool>
    {
    private:
      TInfIncpbltItem TestItem;
    public:
      InfIncpbltItemEqualTo(const TInfIncpbltItem &ATestItem) : TestItem(ATestItem) {}
      bool operator() (const TInfIncpbltItem &AItem)
      {
        return (
          ((TestItem.Id1 == AItem.Id1) && (TestItem.Id2 == AItem.Id2)) ||
          ((TestItem.Id1 == AItem.Id2) && (TestItem.Id2 == AItem.Id1))
        );
      }
    };
                                */
    // �������� �� ��������
//    struct TInfItem
//    {
//      int Id;           // ID
//      UnicodeString Name;  // ������������
//      TInfItem() : Id(0) {}
//    };
//    typedef list<TInfItem> TInfList;
//    typedef list<__int64> TPlanedUnitList;
    typedef list<TTagNode*> TTagNodeList;
    // ��������� ������ ��� PLAN_ITEM::SchemeLineUID
    // first  - Id ��������
    // second - UID ������ �����, �� ������� ������������� ��������/�����
    typedef map<const int, UnicodeString> TSchemeLineIds;
    typedef map<UnicodeString, TDate> TItemDateMap;

    //�������
    TTagNode*       FVacSchXML;          // xml-�������� �� ������� ���������� (��� imm-s)
    TTagNode*       FTestSchXML;         // xml-�������� �� ������� ���� (��� imm-s)
    TTagNode*       FCheckSchXML;         // xml-�������� �� ������� ���� (��� imm-s)
    __int64         FPtId;               // ��� ��������

                                         // ������ ������ �� ��������� ������ ������������
    //��� ����������� �������������
    TTagNode*       FXML;                // xml-��������
//    TPlanerDM*      FDM;                 // ������ ������

//    TItemDateMap    FLastItemDateMap;
//    TPlanPeriodType FPPP;                // ��� ������� ������������
    TDate           FBeginDate;
    TDate           FDatePB;             // ���� ������ ������� ������������
    TDate           FDatePE;             // ���� ��������� ������� ������������
//    TDate           FDatePFB;            // ���� ������ ������� ������������ �����
//    TDate           FDatePFE;            // ���� ��������� ������� ������������ �����
    TDate           FPlanMonth;          // 1-� ����� ������ ������������
//    TPlanedUnitList FPlUnitList;

    DKAGE           FPtAge;              // ������� �������� ��� �������� �������������� ������������
    TDate           FPtBirthDay;         // ���� �������� ����� ��������
    UnicodeString   FPtFIO;              // ��� ����� ��������
//    TCardItemMap    FPrivList;
//    TCardItemMap    FTestList;
//    TCardItemMap    FCheckList;
//    TCardItemMap    FOtvodList;

    TIMMJumpTrace*  FJumpTrace;          // ������ ��������� ����� �������� ����

    TPlanItemList   FPrivPlan;           // ������ ����������� ��������/����
    TPlanItemList   FTestPlan;
    TPlanItemList   FDopPrivivPlan;
    TPlanItemList   FDopTestPlan;

    TCardItemList   FCardPrivList;       // ������ ��������� ��������
    TCardItemList   FCardTestList;       // ������ ��������� ����
    TCardItemList   FCardCheckList;      // ������ ��������� ��������
    TCardItemList   FCardCancelList;     // ������ ��������� �������

    TPlanItemDateList FVacDateByInfComm;
    TPlanItemDateList FVacDateByInf;
    TPlanItemDateList FVacDateByTurInf;
    TPlanItemDateList FTestDateByTest;
    TPlanItemDateList FTestDateByInf;
    TPlanItemDateList FCheckDateByInf;

    TPlanItemDateList FVacOtvodDateByInf;
    TPlanItemDateList FTestOtvodDateByInf;
    TPlanItemDateList FConstVacOtvodDateByInf;
    TPlanItemDateList FConstTestOtvodDateByInf;


  class TBackgroundPlaner;
  TBackgroundPlaner * FBkgPlaner;
  bool FTerminated;
  bool FPlaning;
  void __fastcall SyncCall(TdsSyncMethod AMethod);

//    TUnitPrePlan   *FUnitPrePlan;

    int             FMaxProgress;        // Max �������� ��� ProgressBar'�
    TNotifyEvent    FOnIncProgress;      // �������, ��� ��������� �������� ���������� �������
                                         // ��������� ������� ProgressBar'�
    TOnPlanerStageChange FOnPlanerStageChange; // ��������� ��� ��������� ����� ������������

  UnicodeString FLogMsg, FLogFuncName;
  int FLogALvl;
  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;

  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;
  TdsOnPlanSaveDebugEvent FOnSaveDebugLog;

    TStringList* FErrLog;                // ������ ������ ������������

    TStringList*    FTempSL;
    // ��� ���������� ������
    TPrePlanerDebug*    FDebugLog;
    TPlanerStage FPlanerStage;
    TTagNode* __fastcall FGetDebugLog();
    void __fastcall FSetDebugLog();

    int __fastcall VacTypeToInt(UnicodeString AVacType);
    void __fastcall GetUnitCardData();
    void __fastcall CheckData(UnicodeString  AMethodName);
    inline void __fastcall CallOnIncProgress();
  void __fastcall SyncCallOnIncProgress();

    inline void __fastcall CallOnPlanerStageChange(TPlanerStage NewPlanerStage);
  void __fastcall SyncCallOnPlanerStageChange();

    bool __fastcall GetPtAge(Word &Years, Word &Months, Word &Days);
  TTagNode* __fastcall GetSchLineByLastItem(TPlanItemKind APlanItemKind, int AInf, bool fRoundMode, TTagNode *APlanOptSch);
  TTagNode* __fastcall GetSchemeLineByVacType(__int64 AMIBId, UnicodeString AVacType);
    TTagNode* __fastcall GetBegSchemeLine(TPlanItemKind PlanItemKind, int InfekId, bool fRoundMode);
    bool __fastcall GetCurSchemeLine(TPlanItemKind APlanItemKind, int AInfekId,
                                     bool ALastPlanItemExists, TDate ALastPlanItemDate,
                                     TDate APlanItemDate,
                                     TTagNode * ABegSchemeLine, TTagNode *& ACurSchemeLine,
                                     TDate &ASrokPlanDate,  TTagNode *& ASrokPlanSchemeLine
    );
    TDate __fastcall GetPlanItemPlanDate(int AInfId, bool ARoundMode, TPlanItemKind APlanItemKind, TDate APlanDate, bool bLPlanItemExists, TDate ALastPlanItemDate,TTagNode *ACurrLine);
    bool __fastcall FGetVacDateByType(int AInfId, bool ARoundMode, TPlanItemKind APlanItemKind, TDate &ADate, UnicodeString AVacType);
    bool __fastcall CheckNeedForPrivivPlanByPrevProba(int nInfekId, TDate APrivivPlanDate);
    bool __fastcall FindInfekPlanItemInPlan(TPlanItemKind PlanItemKind, int nInfekId, TPlanItemListIt *PlanItemListIt = NULL);
    int __fastcall DecodeSchemeLineIds(TPlanItemListIt itItem, TSchemeLineIds &AIds);

    void __fastcall GetPlanInfList( TPlanItemKind PlanItemKind, TInfItemList &FPlanInfList);
    bool __fastcall GetLastPlanItemDate(int AInfId, bool ARoundMode, TPlanItemKind APlanItemKind, TDate &ADate);
    int __fastcall PlanItemPlan(TPlanItemKind PlanItemKind);
    void __fastcall CheckByConstCancel();
    void __fastcall CorrectPrivivPlanDate();
    void __fastcall AppendPrepareProb();
    void __fastcall ClearPlan();
    void __fastcall ApplyPlan(TPlanItemKind PlanItemKind, bool IsMain);
  TJSONObject* __fastcall FGetValues(PLAN_ITEM AItem);
  TdsPlanDM   *FDM;

  TkabCustomDataSetRow *FUnitData;

//  TJSONObject *FPCValue;
//  UnicodeString FPCRecId;
  UnicodeString FPCommStage, FPCStage;
  int FCurrPlan, FCommPlanCount;

  TTagNode *FUnitPlanSetting;
  bool __fastcall GetPauseValue(int &APause, UnicodeString AType1, UnicodeString ARef1, UnicodeString AType2, UnicodeString ARef2);
  bool __fastcall GetInfInfPause(int &APause, UnicodeString ARef1, UnicodeString ARef2);
  bool __fastcall GetTestInfPause(int &APause, UnicodeString ARef1, UnicodeString ARef2);
  bool __fastcall GetTestTestPause(int &APause, UnicodeString ARef1, UnicodeString ARef2);
  bool __fastcall GetInfTestPause(int &APause, UnicodeString ARef1, UnicodeString ARef2);
  int __fastcall GetMaxTestPause(int ATestCode);
  int __fastcall GetMaxPauseByMonth(TDate ADate);

  TDate __fastcall GetPlanItemEarliestDate(int AInfId, TPlanItemKind APlanItemKind);
  void __fastcall CorrectPlanDateByMonth();
protected:
  void __fastcall SyncCallLogMsg();
  void __fastcall SyncCallLogErr();
  void __fastcall SyncCallLogBegin();
  void __fastcall SyncCallLogEnd();

  void __fastcall CallLogMsg(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogErr(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogBegin(UnicodeString AFuncName);
  void __fastcall CallLogEnd(UnicodeString AFuncName);
  void __fastcall CallSaveDebugLog(TObject *Sender);
  void __fastcall SyncCallSaveDebugLog();

    void __fastcall SetVacSchXML(TTagNode* AValue);
    void __fastcall SetTestSchXML(TTagNode* AValue);
    void __fastcall SetCheckSchXML(TTagNode* AValue);
    void __fastcall SetPtId(__int64 Value);
    void __fastcall FGetPatData();
    void __fastcall CalcPlanPeriod(TDate ABegDate);
//    void __fastcall SetDopIDKind(TDopIDKind AValue);
//    void __fastcall SetFltPlanKind(TFltPlanKind AValue);


public:



    System::UnicodeString __fastcall StartCreateUnitPlan(TDate ABegDate, __int64 APtCode);
    System::UnicodeString __fastcall StopCreatePlan();
    __property bool Terminated = {read = FTerminated};
    __property bool Planing = {read = FPlaning};
    int __fastcall GetMaxProgress();
    int __fastcall GetCurProgress();
    UnicodeString __fastcall GetCurStage() const;

    __fastcall TPrePlaner(TdsPlanDM   *ADM);
    virtual __fastcall ~TPrePlaner();

    // xml-�������� �� ������� ���������� (��� imm-s)
    // (��� ������������ Assign �� ��������)
    __property TTagNode*    VacSchXML = { read = FVacSchXML, write = SetVacSchXML };
    // xml-�������� �� ������� ���� (��� imm-s)
    // (��� ������������ Assign �� ��������)
    __property TTagNode*    TestSchXML = { read = FTestSchXML, write = SetTestSchXML };
    // xml-�������� �� ������� ���� (��� imm-s)
    // (��� ������������ Assign �� ��������)
    __property TTagNode*    CheckSchXML = { read = FCheckSchXML, write = SetCheckSchXML };


    // ��� �����������
//    __property TKontType    KontType = { read = FKontType, write = FKontType };
    // ��� �������, �����������, ���� ��� �����
//    __property int          DopID = { read = FDopID, write = FDopID };
    // ����������, ��� ���� ������ DopID
//    __property TDopIDKind   DopIDKind = { read = FDopIDKind, write = SetDopIDKind };
    // ��� ������������ �� �������
//    __property TFltPlanKind FltPlanKind = { read = FFltPlanKind, write = SetFltPlanKind };
    // ���� ������������ �������������� �������� ��� �����
//    __property TDate        DopPlanDate = { read = FDopPlanDate, write = FDopPlanDate };
    // ��� ��������
    __property __int64      PtId = { read = FPtId, write = SetPtId };

    // ������� ��������, ��� �������� �������������� ������������
    __property DKAGE        PtAge = { read = FPtAge };
    //���� �������� ��������, ��� �������� �������������� ������������
    __property TDate        PtBirthDay = { read = FPtBirthDay };

    // ��� ������� ������������
//    __property TPlanPeriodType PPP = { read = FPPP };
    // ���� ������ ������� ������������
    __property TDate           DatePB = { read = FDatePB };
    // ���� ��������� ������� ������������
    __property TDate           DatePE = { read = FDatePE };
    // ���� ������ ������� ������������ �����
//    __property TDate           DatePFB = { read = FDatePFB };
    // ���� ��������� ������� ������������ �����
//    __property TDate           DatePFE = { read = FDatePFE };
    // 1-� ����� ������ ������������
    __property TDate           PlanMonth = { read = FPlanMonth };

    __property int          MaxProgress   = { read = FMaxProgress };
    __property TNotifyEvent OnIncProgress = { read = FOnIncProgress, write = FOnIncProgress };
    // ��������� ��� ��������� ����� ������������
    __property TOnPlanerStageChange OnPlanerStageChange = { read = FOnPlanerStageChange, write = FOnPlanerStageChange };

    // ��������� ��� ������ ���-�
    __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
    __property TOnLogMsgErrEvent OnLogError = {read = FOnLogError, write = FOnLogError};

    __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
    __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};

   __property TdsOnPlanSaveDebugEvent OnSaveDebugLog = {read = FOnSaveDebugLog, write=FOnSaveDebugLog};

    // ������ ������ ������������
    __property TStringList* ErrLog = { read = FErrLog, write = FErrLog };
    __property TTagNode* DebugLog = { read = FGetDebugLog};

//    __property TJSONObject *PCValue = { read = FPCValue, write = FPCValue };
//    __property UnicodeString PCRecId = { read = FPCRecId };
    System::UnicodeString __fastcall CreatePlan();

//    int  __fastcall GetNewPlanId();
    TDate __fastcall CalculatePlanPeriod( TDate &ADateBP, TDate &ADateEP);
//    void __fastcall PlanDop();

    bool __fastcall CreateUnitPlan(TDate ABegDate, __int64 APtCode);

    // ��� ���������� ������
    bool __fastcall IsDebugPrint();
};

//---------------------------------------------------------------------------

#endif
