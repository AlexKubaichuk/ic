//---------------------------------------------------------------------------

#ifndef dsVacStoreUnitH
#define dsVacStoreUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
//---------------------------------------------------------------------------
#include "dsSrvRegUnit.h"
#include "dsVacStoreDMUnit.h"
#include "dsVacStoreEditors.h"
//---------------------------------------------------------------------------
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
//---------------------------------------------------------------------------
enum class TVacStoreModuleType {None, Income, Outcome, Cancel };
class DECLSPEC_DRTTI TdsVacStoreClass : public TDataModule
{
__published:	// IDE-managed Components
 TFDConnection *IcConnection;
private:	// User declarations
   TdsSrvRegistry  *FRegComp;
   TTagNode        *FDefXML;
   TdsVacStoreDM        *FDM;
   TdsVacStoreEditors *FVSEditors;
   bool FGetNewTabCode(UnicodeString ATab, UnicodeString &ARet);

public:		// User declarations
  __fastcall TdsVacStoreClass(TComponent* Owner, TAxeXMLContainer *AXMLList);
  __fastcall ~TdsVacStoreClass();
  void Disconnect();

  UnicodeString GetDefXML();
  UnicodeString GetClassXML(UnicodeString ARef);
  UnicodeString GetClassZIPXML(UnicodeString ARef);
  __int64       GetCount(UnicodeString AId, UnicodeString AFilterParam);
  TJSONObject*  GetIdList(UnicodeString AId, int AMax, UnicodeString AFilterParam);
  TJSONObject*  GetValById(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  GetValById10(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  Find(UnicodeString AId, UnicodeString AFindData);
  UnicodeString InsertData(UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  UnicodeString EditData(UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  UnicodeString DeleteData(UnicodeString AId, UnicodeString ARecId);
  UnicodeString SetTextData(UnicodeString AId, UnicodeString AFlId, UnicodeString ARecId, UnicodeString AVal);
  UnicodeString GetTextData(UnicodeString AId, UnicodeString AFlId, UnicodeString ARecId, UnicodeString &AMsg);


  void          SetMainLPU(__int64 ALPUCode);
  UnicodeString MainOwnerCode();
  bool          IsMainOwnerCode(UnicodeString AOwnerCode);
  UnicodeString GetMIBPForm();

  TJSONObject*  GetMIBPList(UnicodeString AId);
};
//---------------------------------------------------------------------------
#endif
