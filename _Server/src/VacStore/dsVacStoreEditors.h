//---------------------------------------------------------------------------

#ifndef dsVacStoreEditorsH
#define dsVacStoreEditorsH
//---------------------------------------------------------------------------
#include "dsVacStoreDMUnit.h"
//---------------------------------------------------------------------------
class PACKAGE TdsVacStoreEditors : public TObject
{
private:
  TdsVacStoreDM      *FDM;

  System::UnicodeString __fastcall ImportIncome(bool AIsEdit, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString __fastcall DeleteIncome(System::UnicodeString ARecId);

  System::UnicodeString __fastcall ImportOutcome(bool AIsEdit, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString __fastcall DeleteOutcome(System::UnicodeString ARecId);

  System::UnicodeString __fastcall ImportCancel(bool AIsEdit, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString __fastcall DeleteCancel(System::UnicodeString ARecId);

  __int64 __fastcall GetNewCode(UnicodeString TabId);
  UnicodeString __fastcall NewGUID();
//  System::UnicodeString __fastcall JSONToString(TJSONValue *AValue, UnicodeString ADef = "");
//  TDate                 __fastcall JSONToDate(TJSONValue *AValue, TDate ADef = TDate(0));
//  __int64               __fastcall JSONToInt(TJSONValue *AValue, __int64 ADef = 0);
//  double                __fastcall JSONToDouble(TJSONValue *AValue, double ADef = 0);
//  Variant               __fastcall JSONToVar(TJSONValue *AValue, Variant ADef = Variant::Empty());

public:
  __fastcall TdsVacStoreEditors(TdsVacStoreDM* ADM);
  __fastcall ~TdsVacStoreEditors();

  System::UnicodeString __fastcall InsertData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString __fastcall EditData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString __fastcall DeleteData(System::UnicodeString AId, System::UnicodeString ARecId);
};
//---------------------------------------------------------------------------
#endif
