//---------------------------------------------------------------------------

#pragma hdrstop

#include "dsVacStoreEditors.h"
#include "JSONUtils.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
__fastcall TdsVacStoreEditors::TdsVacStoreEditors(TdsVacStoreDM* ADM)
{
  FDM = ADM;
}
//---------------------------------------------------------------------------
__fastcall TdsVacStoreEditors::~TdsVacStoreEditors()
{
}
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#                      �����                                                 #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsVacStoreEditors::InsertData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId)
{
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  try
   {
/*
           if ((_UID_ == "111F") ||
            { // ������ �������
            }
           else if (_UID_ == "1120")
            { // ������ �������
            }
           else if (_UID_ == "1124")
            { //������ ��������
            }
*/
     if (AId.UpperCase() == "111F")           // ������ �������
      RC = ImportIncome(false, AValue, ARecId);
     else if (AId.UpperCase() == "1120")      // ������ �������
      RC = ImportOutcome(false, AValue, ARecId);
     else if (AId.UpperCase() == "1124")      //������ ��������
      RC = ImportCancel(false, AValue, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
}
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsVacStoreEditors::EditData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId)
{
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������, �������������� �� ��������������.";
  try
   {
//     if (AId.UpperCase() == "111F")
//      RC = ImportIncome(true, AValue, ARecId);
//     else if (AId.UpperCase() == "102F")
//      RC = ImportOutcome(true, AValue, ARecId);
//     else if (AId.UpperCase() == "1100")
//      RC = ImportCancel(true, AValue, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
}
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsVacStoreEditors::DeleteData(System::UnicodeString AId, System::UnicodeString ARecId)
{
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
     if (AId.UpperCase() == "111F")
      RC = DeleteIncome(ARecId);
     else if (AId.UpperCase() == "1120")
      RC = DeleteOutcome(ARecId);
     else if (AId.UpperCase() == "1124")
      RC = DeleteCancel(ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
}
//---------------------------------------------------------------------------
/*System::UnicodeString __fastcall TdsVacStoreEditors::JSONToString(TJSONValue *AValue, UnicodeString ADef)
{
  UnicodeString RC = ADef;
  try
   {
     if (AValue)
      {
        if (!AValue->Null)
         RC = ((TJSONString*)AValue)->Value();
      }
     else
      dsLogError("TdsVacStoreEditors::JSONToString AValue = NULL");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
*/
/*
TDate __fastcall TdsVacStoreEditors::JSONToDate(TJSONValue *AValue, TDate ADef)
{
  TDate RC = ADef;
  try
   {
     if (AValue)
      {
        if (!AValue->Null)
         RC = TDate(((TJSONString*)AValue)->Value());
      }
     else
      dsLogError("TdsVacStoreEditors::JSONToDate AValue = NULL");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
*/
/*
__int64 __fastcall TdsVacStoreEditors::JSONToInt(TJSONValue *AValue, __int64 ADef)
{
  __int64 RC = ADef;
  try
   {
     if (AValue)
      {
        if (!AValue->Null)
         RC = ((TJSONNumber*)AValue)->AsInt64;
      }
     else
      dsLogError("TdsVacStoreEditors::JSONToInt AValue = NULL");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
*/
/*
double __fastcall TdsVacStoreEditors::JSONToDouble(TJSONValue *AValue, double ADef)
{
  double RC = ADef;
  try
   {
     if (AValue)
      {
        if (!AValue->Null)
         RC = ((TJSONNumber*)AValue)->AsDouble;
      }
     else
      dsLogError("TdsVacStoreEditors::JSONToDouble AValue = NULL");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
*/
/*
Variant __fastcall TdsVacStoreEditors::JSONToVar(TJSONValue *AValue, Variant ADef)
{
  Variant RC = ADef;
  try
   {
     if (AValue)
      {
        if (!AValue->Null)
          RC = Variant(JSONToString(AValue));
      }
     else
      dsLogError("TdsVacStoreEditors::JSONToVar AValue = NULL");
   }
  __finally
   {
   }
  return RC;
}
*/
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#                      ������ �������                                        #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsVacStoreEditors::ImportIncome(bool AIsEdit, TJSONObject *AValue, System::UnicodeString &ARecId)
{
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  TFDQuery *FQ = FDM->CreateTempQuery();
  TFDQuery *InsQ = FDM->CreateTempQuery();
  TFDTransaction *SaveTR = (TFDTransaction*)FQ->Transaction;
  FQ->Transaction = InsQ->Transaction;
  try
   {
     if (!FQ->Transaction->Active) FQ->Transaction->StartTransaction();
     try
      {
        __int64 FCode        = -1;//JSONToInt(AValue->Values["3183"], -1);
        __int64 FVacCode     = JSONToInt(AValue, "112F");
        __int64 FVacCount    = JSONToInt(AValue, "112C");
        UnicodeString FSeria = JSONToString(AValue, "112E");

        bool FErr = false;
/*
        FDM->quExecParam(FQ, "Select CODE From CLASS_3003 Where R317A=:pUCODE and R3024=:pDate and R3020=:pVacCode","pUCODE", FUCode, "pDate", FVacDate, "pVacCode", FVacCode);
        if (FQ->RecordCount)
         {
           FErr = !AIsEdit || (AIsEdit && FQ->FieldByName("CODE")->AsInteger != FCode);
           if (FErr)
             RC = "�������� ����������, ������� ��������� � ��������� ������.";
         }
*/
        if (!FErr)
         {
           if (InsQ->Transaction->Active) InsQ->Transaction->Commit();
           InsQ->Transaction->StartTransaction();
           InsQ->SQL->Text = "INSERT INTO class_111F (code, cguid, R112B, R1125, R112C, R112D, R112F, R112E, R1130, R1133, R1134, R1135) values (:pcode, :pcguid, :pR112B, :pR1125, :pR112C, :pR112D, :pR112F, :pR112E, :pR1130, :pR1133, :pR1134, :pR1135)";
           InsQ->Prepare();
           if (!AIsEdit || (FCode == -1))
            FCode = GetNewCode("111F");

           try
            {
              InsQ->ParamByName("pCode")->Value = FCode;
              ARecId = IntToStr(FCode);

              if (!AIsEdit)
               InsQ->ParamByName("pcguid")->AsString = NewGUID();

              __int64 FCommCount, FOldCount, FSerCount;
              __int64 FCommCode, FSerCode;
              FCommCount = FSerCount = 0;
              FCommCode = FSerCode = -1;

              FDM->quExecParam(FQ, false, "Select CODE, R1141 From class_1121 Where R113F=:pR113F and Upper(R1140)=:pR1140","pR113F", FVacCode, "", "pR1140", FSeria.UpperCase());
              if (FQ->RecordCount)
               {
                 FSerCount = FQ->FieldByName("R1141")->AsInteger;
                 FSerCode  = FQ->FieldByName("CODE")->AsInteger;
               }
              FSerCount += FVacCount;
              FDM->quExecParam(FQ, false, "Select CODE, R1148 From class_1122 Where R1147=:pR1147","pR1147", FVacCode);
              FOldCount = 0;
              if (FQ->RecordCount)
               {
                 FCommCount = FQ->FieldByName("R1148")->AsInteger;
                 FOldCount = FCommCount;
                 FCommCode  = FQ->FieldByName("CODE")->AsInteger;
               }
              FCommCount += FVacCount;

              // ��������� � ������ �������
              InsQ->ParamByName("pR112B")->Value = FOldCount;                         // <digit name='���������� �������' uid='112B' digits='10' min='0' max='9999999999'/>
              InsQ->ParamByName("pR1125")->Value = JSONToVar(AValue, "1125"); // <date name='���� �����������' uid='1125' required='1' inlist='l'/>
              InsQ->ParamByName("pR112C")->Value = JSONToVar(AValue, "112C"); // <digit name='���������� (����)' uid='112C' required='1' inlist='l' digits='9' min='0' max='999999999'/>
              InsQ->ParamByName("pR112D")->Value = 0;                                 // <digit name='���������� (��������)' uid='112D' digits='9' min='0' max='999999999'/>
              InsQ->ParamByName("pR112F")->Value = JSONToVar(AValue, "112F"); // <choiceDB name='����' uid='112F' ref='0035' required='1' inlist='l'/>
              InsQ->ParamByName("pR112E")->Value = JSONToVar(AValue, "112E"); // <text name='�����' uid='112E' required='1' inlist='l' cast='no' length='20'/>
              InsQ->ParamByName("pR1130")->Value = JSONToVar(AValue, "1130"); // <date name='����� ��' uid='1130' required='1' inlist='l'/>
              InsQ->ParamByName("pR1133")->Value = JSONToVar(AValue, "1133"); // <choiceDB name='���������' uid='1133' ref='1131'/>
              InsQ->ParamByName("pR1134")->Value = JSONToVar(AValue, "1134"); // <choiceDB name='�������������' uid='1134' ref='019A'/>
              InsQ->ParamByName("pR1135")->Value = JSONToVar(AValue, "1135"); // <text name='������� �������� ( t�c)' uid='1135' cast='no' length='255'/>

              InsQ->OpenOrExecute();
              // ��������� (���������) ������ ������� ���� (�����)
              InsQ->SQL->Text = "UPDATE OR INSERT INTO class_1121 (code, cguid, R1127, R113F, R1140, R1141, R1142, R1143, R1144, R1145, R1146) values (:pcode, :pcguid, :pR1127, :pR113F, :pR1140, :pR1141, :pR1142, :pR1143, :pR1144, :pR1145, :pR1146) MATCHING (CODE)";
              InsQ->Prepare();

              if (FSerCode == -1)
               {
                 FSerCode = GetNewCode("1121");
                 InsQ->ParamByName("pcguid")->AsString = NewGUID();
               }
              InsQ->ParamByName("pCode")->Value = FSerCode;

              InsQ->ParamByName("pR1127")->Value = JSONToVar(AValue, "1125");  // <date name='���� �����������' uid='1127' required='1' inlist='l'/>
              InsQ->ParamByName("pR113F")->Value = JSONToVar(AValue, "112F");  // <choiceDB name='����' uid='113F' ref='0035' required='1' inlist='l'/>
              InsQ->ParamByName("pR1140")->Value = JSONToVar(AValue, "112E");  // <text name='�����' uid='1140' required='1' inlist='l' cast='no' length='20'/>
              InsQ->ParamByName("pR1141")->Value = FSerCount;                          // <digit name='���������� (����)' uid='1141' required='1' inlist='l' digits='9' min='0' max='99999999'/>
              InsQ->ParamByName("pR1142")->Value = 0;                                  // <digit name='���������� (��������)' uid='1142' digits='9' min='0' max='99999999'/>
              InsQ->ParamByName("pR1143")->Value = JSONToVar(AValue, "1130");  // <date name='����� ��' uid='1143' required='1' inlist='l'/>
              InsQ->ParamByName("pR1144")->Value = JSONToVar(AValue, "1133");  // <choiceDB name='���������' uid='1144' ref='1131'/>
              InsQ->ParamByName("pR1145")->Value = JSONToVar(AValue, "1134");  // <choiceDB name='�������������' uid='1145' ref='019A'/>
              InsQ->ParamByName("pR1146")->Value = JSONToVar(AValue, "1135");  // <text name='������� ���������������' uid='1146' cast='no' length='255'/>
              InsQ->OpenOrExecute();

              // ��������� (���������) ������ ������� ���� (�����)
              InsQ->SQL->Text = "UPDATE OR INSERT INTO class_1122 (code, cguid, R1128, R1147, R1148, R1149, R114A, R114B, R114C, R114F, R114E) values (:pcode, :pcguid, :pR1128, :pR1147, :pR1148, :pR1149, :pR114A, :pR114B, :pR114C, :pR114F, :pR114E) MATCHING (CODE)";
              InsQ->Prepare();

              if (FCommCode == -1)
               {
                 FCommCode = GetNewCode("1122");
                 InsQ->ParamByName("pcguid")->AsString = NewGUID();
               }

              InsQ->ParamByName("pCode")->Value  = FCommCode;

              FDM->quExecParam(FQ, false, "Select Min(R1143) as MDate From class_1121 Where R113F=:pR113F","pR113F", FVacCode);
              if (FQ->RecordCount)
               InsQ->ParamByName("pR114A")->Value = FQ->FieldByName("MDate")->AsDateTime;      // <date name='����� �� (����������� ����)' uid='114A' required='1' inlist='l'/>
              else
               InsQ->ParamByName("pR114A")->Value = JSONToVar(AValue, "1130");      // <date name='����� �� (����������� ����)' uid='114A' required='1' inlist='l'/>

              FDM->quExecParam(FQ, false, "Select Max(R1143) as MDate From class_1121 Where R113F=:pR113F","pR113F", FVacCode);
              if (FQ->RecordCount)
                InsQ->ParamByName("pR114B")->Value = FQ->FieldByName("MDate")->AsDateTime;      // <date name='����� �� (������������ ����)' uid='114B' required='1' inlist='l'/>
              else
                InsQ->ParamByName("pR114B")->Value = JSONToVar(AValue, "1130");      // <date name='����� �� (������������ ����)' uid='114B' required='1' inlist='l'/>

              InsQ->ParamByName("pR1128")->Value = JSONToVar(AValue, "1125");  // <date name='���� ���������� �����������' uid='1128'/>
              InsQ->ParamByName("pR1147")->Value = JSONToVar(AValue, "112F");  // <choiceDB name='����' uid='1147' ref='0035' required='1' inlist='l'/>
              InsQ->ParamByName("pR1148")->Value = FCommCount;                         // <digit name='���������� (����)' uid='1148' required='1' inlist='l' digits='9' min='0' max='999999999'/>
              InsQ->ParamByName("pR1149")->Value = 0;                                  // <digit name='���������� (��������)' uid='1149' digits='9' min='0' max='99999999'/>
              InsQ->ParamByName("pR114C")->Value = 2;                                  // <digit name='Min. ���������� ���' uid='114C' digits='5' min='0' max='99999'/>
              InsQ->ParamByName("pR114F")->Value = 2;                                  // <digit name='Max. ���������� ���' uid='114F' digits='5' min='0' max='9999'/>
              InsQ->ParamByName("pR114E")->Value = 2;                                  // <digit name='Avg. ���������� ���' uid='114E' digits='5' min='0' max='99999'/>
              InsQ->OpenOrExecute();


              if (InsQ->Transaction->Active) InsQ->Transaction->Commit();
              if (FQ->Transaction->Active)   FQ->Transaction->Commit();

              RC = "ok";
            }
           __finally
            {
            }
         }
      }
     catch(...)
      {
        if (FQ->Transaction->Active) FQ->Transaction->Rollback();
        if (InsQ->Transaction->Active) InsQ->Transaction->Rollback();
      }
   }
  __finally
   {
     FQ->Transaction = SaveTR;
     FDM->DeleteTempQuery(FQ);
     FDM->DeleteTempQuery(InsQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
}
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsVacStoreEditors::DeleteIncome(System::UnicodeString ARecId)
{
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  TFDQuery *FQ = FDM->CreateTempQuery();
  try
   {
     if (!FQ->Transaction->Active) FQ->Transaction->StartTransaction();
     try
      {
        FDM->quExecParam(FQ, false, "Select R112F, R112E, R112C From CLASS_111F Where code=:pCode", "pCode", ARecId);
        if (FQ->RecordCount)
         {
           __int64 FVacCode     = FQ->FieldByName("R112F")->AsInteger;
           UnicodeString FSeria = FQ->FieldByName("R112E")->AsString;
           __int64 FVacCount    = FQ->FieldByName("R112C")->AsInteger;

           FDM->quExecParam(FQ, false, "Delete  From CLASS_111F Where code=:pCode", "pCode", ARecId);
           FDM->quExecParam(FQ, false, "Update CLASS_1121 Set R1141 = R1141-:pCount Where R113F=:pVacCode and Upper(R1140) = :pSeria", "pCount", FVacCount, "", "pVacCode", FVacCode, "pSeria", FSeria.UpperCase());
           FDM->quExecParam(FQ, false, "Update CLASS_1122 Set R1148 = R1148-:pCount Where R1147=:pVacCode ", "pCount", FVacCount, "", "pVacCode", FVacCode);
           if (FQ->Transaction->Active) FQ->Transaction->Commit();
           RC = "ok";
         }
      }
     catch(Exception &E)
      {
        if (FQ->Transaction->Active) FQ->Transaction->Rollback();
        RC = E.Message;
      }
   }
  __finally
   {
     FDM->DeleteTempQuery(FQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
}
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#                      ������ �������                                        #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsVacStoreEditors::ImportOutcome(bool AIsEdit, TJSONObject *AValue, System::UnicodeString &ARecId)
{
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  TFDQuery *FQ = FDM->CreateTempQuery();
  TFDQuery *InsQ = FDM->CreateTempQuery();
  TFDTransaction *SaveTR = (TFDTransaction*)FQ->Transaction;
  FQ->Transaction = InsQ->Transaction;
  try
   {
     if (!FQ->Transaction->Active) FQ->Transaction->StartTransaction();
     try
      {
        __int64 FCode        = -1;//JSONToInt(AValue->Values["3183"], -1);
        __int64 FVacCode     = JSONToInt(AValue, "113A");
        __int64 FVacCount    = JSONToInt(AValue, "1138");
        UnicodeString FSeria = JSONToString(AValue, "113B");

        FDM->quExecParam(FQ, false, "Select R1141 From class_1121 Where R113F=:pR113F and Upper(R1140)=:pR1140", "pR113F", FVacCode, "", "pR1140", FSeria.UpperCase());
        if (FQ->RecordCount)
         {
           if (FVacCount <= FQ->FieldByName("R1141")->AsInteger)
            {
              if (InsQ->Transaction->Active) InsQ->Transaction->Commit();
              InsQ->Transaction->StartTransaction();
              InsQ->SQL->Text = "INSERT INTO class_1120 (code, cguid, R1126, R1136, R1138, R1139, R113A, R113B, R113C, R113D, R113E) values (:pcode, :pcguid, :pR1126, :pR1136, :pR1138, :pR1139, :pR113A, :pR113B, :pR113C, :pR113D, :pR113E)";
              InsQ->Prepare();
              if (!AIsEdit || (FCode == -1))
               FCode = GetNewCode("1120");

              try
               {
                 FDM->quExecParam(FQ, false, "Update CLASS_1121 Set R1141 = R1141-:pCount Where R113F=:pVacCode and Upper(R1140) = :pSeria", "pCount", FVacCount, "", "pVacCode", FVacCode, "pSeria", FSeria.UpperCase());
                 FDM->quExecParam(FQ, false, "Update CLASS_1122 Set R1148 = R1148-:pCount Where R1147=:pVacCode ", "pCount", FVacCount, "", "pVacCode", FVacCode);

                 InsQ->ParamByName("pCode")->Value = FCode;
                 ARecId = IntToStr(FCode);
                 if (!AIsEdit)
                  InsQ->ParamByName("pcguid")->AsString = NewGUID();

                 __int64 FCommCount = 0;
                 __int64 FSerCount = 0;

                 FDM->quExecParam(FQ, false, "Select R1148 From class_1122 Where R1147=:pR1147","pR1147", FVacCode);
                 if (FQ->RecordCount)
                  FCommCount = FQ->FieldByName("R1148")->AsInteger;

                 FDM->quExecParam(FQ, false, "Select R1141 From class_1121 Where R113F=:pR113F and Upper(R1140)=:pR1140","pR113F", FVacCode, "", "pR1140", FSeria.UpperCase());
                 if (FQ->RecordCount)
                   FSerCount = FQ->FieldByName("R1141")->AsInteger;

                 // ��������� � ������ �������
                 InsQ->ParamByName("pR1126")->Value = JSONToVar(AValue, "1126"); // <date name='���� ������' uid='1126' required='1' inlist='l'/>
                 InsQ->ParamByName("pR1136")->Value = JSONToVar(AValue, "1136"); // <choiceDB name='���' uid='1136' ref='00C7' required='1' inlist='l'/>
                 InsQ->ParamByName("pR1138")->Value = JSONToVar(AValue, "1138"); // <digit name='���������� (����)' uid='1138' required='1' inlist='l' digits='9' min='0' max='999999999'/>
                 InsQ->ParamByName("pR1139")->Value = 0; // <digit name='���������� (��������)' uid='1139' digits='9' min='0' max='999999999'/>
                 InsQ->ParamByName("pR113A")->Value = JSONToVar(AValue, "113A"); // <choiceDB name='����' uid='113A' ref='0035' required='1' inlist='l'/>
                 InsQ->ParamByName("pR113B")->Value = JSONToVar(AValue, "113B"); // <text name='�����' uid='113B' required='1' inlist='l' cast='no' length='20'/>
                 InsQ->ParamByName("pR113C")->Value = FSerCount; // <digit name='������� �����' uid='113C' inlist='l' digits='10' min='0' max='9999999999'/>
                 InsQ->ParamByName("pR113D")->Value = JSONToVar(AValue, "113D"); // <digit name='t�c ������������' uid='113D' digits='5' decimal='2' min='-100' max='100'/>
                 InsQ->ParamByName("pR113E")->Value = JSONToVar(AValue, "113E"); // <digit name='t�c ��������������� ���' uid='113E' digits='5' decimal='2' min='-100' max='100'/>

                 InsQ->OpenOrExecute();

                 if (InsQ->Transaction->Active) InsQ->Transaction->Commit();
                 if (FQ->Transaction->Active)   FQ->Transaction->Commit();

                 RC = "ok";
               }
              __finally
               {
               }
            }
           else
            RC = "���������� ��� ������("+IntToStr(FVacCount)+") ��������� ���������� ��� � �������("+IntToStr(FQ->FieldByName("R1141")->AsInteger)+").";
         }
        else
         RC = "����������� ������� ��������� �����.";
      }
     catch(...)
      {
        if (FQ->Transaction->Active) FQ->Transaction->Rollback();
        if (InsQ->Transaction->Active) InsQ->Transaction->Rollback();
      }
   }
  __finally
   {
     FQ->Transaction = SaveTR;
     FDM->DeleteTempQuery(FQ);
     FDM->DeleteTempQuery(InsQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
}
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsVacStoreEditors::DeleteOutcome(System::UnicodeString ARecId)
{
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  TFDQuery *FQ = FDM->CreateTempQuery();
  try
   {
     if (!FQ->Transaction->Active) FQ->Transaction->StartTransaction();
     try
      {
        FDM->quExecParam(FQ, false, "Select R113A, R113B, R1138 From CLASS_1120 Where code=:pCode", "pCode", ARecId);
        if (FQ->RecordCount)
         {
           __int64 FVacCode     = FQ->FieldByName("R113A")->AsInteger;
           UnicodeString FSeria = FQ->FieldByName("R113B")->AsString;
           __int64 FVacCount    = FQ->FieldByName("R1138")->AsInteger;

           FDM->quExecParam(FQ, false, "Delete  From CLASS_1120 Where code=:pCode", "pCode", ARecId);
           FDM->quExecParam(FQ, false, "Update CLASS_1121 Set R1141 = R1141+:pCount Where R113F=:pVacCode and Upper(R1140) = :pSeria", "pCount", FVacCount, "", "pVacCode", FVacCode, "pSeria", FSeria.UpperCase());
           FDM->quExecParam(FQ, false, "Update CLASS_1122 Set R1148 = R1148+:pCount Where R1147=:pVacCode ", "pCount", FVacCount, "", "pVacCode", FVacCode);
           if (FQ->Transaction->Active) FQ->Transaction->Commit();
           RC = "ok";
         }
      }
     catch(Exception &E)
      {
        if (FQ->Transaction->Active) FQ->Transaction->Rollback();
        RC = E.Message;
      }
   }
  __finally
   {
     FDM->DeleteTempQuery(FQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
}
//----------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#                      ������ ��������                                       #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsVacStoreEditors::ImportCancel(bool AIsEdit, TJSONObject *AValue, System::UnicodeString &ARecId)
{
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  TFDQuery *FQ = FDM->CreateTempQuery();
  TFDQuery *InsQ = FDM->CreateTempQuery();
  TFDTransaction *SaveTR = (TFDTransaction*)FQ->Transaction;
  FQ->Transaction = InsQ->Transaction;
  try
   {
     if (!FQ->Transaction->Active) FQ->Transaction->StartTransaction();
     try
      {
        __int64 FCode        = -1;//JSONToInt(AValue->Values["3183"], -1);
        __int64 FVacCode     = JSONToInt(AValue, "1151");
        __int64 FVacCount    = JSONToInt(AValue, "1153");
        UnicodeString FSeria = JSONToString(AValue, "1152");

        FDM->quExecParam(FQ, false, "Select R1141 From class_1121 Where R113F=:pR113F and Upper(R1140)=:pR1140", "pR113F", FVacCode, "", "pR1140", FSeria.UpperCase());
        if (FQ->RecordCount)
         {
           if (FVacCount <= FQ->FieldByName("R1141")->AsInteger)
            {
              if (InsQ->Transaction->Active) InsQ->Transaction->Commit();
              InsQ->Transaction->StartTransaction();
              InsQ->SQL->Text = "INSERT INTO class_1124 (code, cguid, R112A, R1151, R1152, R1153, R1154) values (:pcode, :pcguid, :pR112A, :pR1151, :pR1152, :pR1153, :pR1154)";
              InsQ->Prepare();
              if (!AIsEdit || (FCode == -1))
               FCode = GetNewCode("1120");

              try
               {
                 FDM->quExecParam(FQ, false, "Update CLASS_1121 Set R1141 = R1141-:pCount Where R113F=:pVacCode and Upper(R1140) = :pSeria", "pCount", FVacCount, "", "pVacCode", FVacCode, "pSeria", FSeria.UpperCase());
                 FDM->quExecParam(FQ, false, "Update CLASS_1122 Set R1148 = R1148-:pCount Where R1147=:pVacCode ", "pCount", FVacCount, "", "pVacCode", FVacCode);

                 InsQ->ParamByName("pCode")->Value = FCode;
                 ARecId = IntToStr(FCode);

                 if (!AIsEdit)
                  InsQ->ParamByName("pcguid")->AsString = NewGUID();

                 __int64 FCommCount = 0;
                 __int64 FSerCount = 0;

                 FDM->quExecParam(FQ, false, "Select R1148 From class_1122 Where R1147=:pR1147","pR1147", FVacCode);
                 if (FQ->RecordCount)
                  FCommCount = FQ->FieldByName("R1148")->AsInteger;

                 FDM->quExecParam(FQ, false, "Select R1141 From class_1121 Where R113F=:pR113F and Upper(R1140)=:pR1140","pR113F", FVacCode, "", "pR1140", FSeria.UpperCase());
                 if (FQ->RecordCount)
                   FSerCount = FQ->FieldByName("R1141")->AsInteger;

                 // ��������� � ������ ��������
                 InsQ->ParamByName("pR112A")->Value = JSONToVar(AValue, "112A"); // <date name='���� ��������' uid='112A' required='1' inlist='l'/>
                 InsQ->ParamByName("pR1151")->Value = JSONToVar(AValue, "1151"); // <choiceDB name='����' uid='1151' ref='0035' required='1' inlist='l'/>
                 InsQ->ParamByName("pR1152")->Value = JSONToVar(AValue, "1152"); // <text name='�����' uid='1152' required='1' inlist='l' cast='no' length='20'/>
                 InsQ->ParamByName("pR1153")->Value = JSONToVar(AValue, "1153"); // <digit name='���������� (����)' uid='1153' required='1' inlist='l' digits='9' min='0' max='99999999'/>
                 InsQ->ParamByName("pR1154")->Value = JSONToVar(AValue, "1154"); // <digit name='���������� (��������)' uid='1154' digits='9' min='0' max='99999999'/>

                 InsQ->OpenOrExecute();

                 if (InsQ->Transaction->Active) InsQ->Transaction->Commit();
                 if (FQ->Transaction->Active)   FQ->Transaction->Commit();

                 RC = "ok";
               }
              __finally
               {
               }
            }
           else
            RC = "���������� ��� ��������("+IntToStr(FVacCount)+") ��������� ���������� ��� � �������("+IntToStr(FQ->FieldByName("R1141")->AsInteger)+").";
         }
        else
         RC = "����������� ������� ��������� �����.";
      }
     catch(...)
      {
        if (FQ->Transaction->Active) FQ->Transaction->Rollback();
        if (InsQ->Transaction->Active) InsQ->Transaction->Rollback();
      }
   }
  __finally
   {
     FQ->Transaction = SaveTR;
     FDM->DeleteTempQuery(FQ);
     FDM->DeleteTempQuery(InsQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
}
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsVacStoreEditors::DeleteCancel(System::UnicodeString ARecId)
{
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  TFDQuery *FQ = FDM->CreateTempQuery();
  try
   {
     if (!FQ->Transaction->Active) FQ->Transaction->StartTransaction();
     try
      {
        FDM->quExecParam(FQ, false, "Select R1151, R1152, R1153 From CLASS_1124 Where code=:pCode", "pCode", ARecId);
        if (FQ->RecordCount)
         {
           __int64 FVacCode     = FQ->FieldByName("R1151")->AsInteger;
           UnicodeString FSeria = FQ->FieldByName("R1152")->AsString;
           __int64 FVacCount    = FQ->FieldByName("R1153")->AsInteger;

           FDM->quExecParam(FQ, false, "Delete  From CLASS_1124 Where code=:pCode", "pCode", ARecId);
           FDM->quExecParam(FQ, false, "Update CLASS_1121 Set R1141 = R1141+:pCount Where R113F=:pVacCode and Upper(R1140) = :pSeria", "pCount", FVacCount, "", "pVacCode", FVacCode, "pSeria", FSeria.UpperCase());
           FDM->quExecParam(FQ, false, "Update CLASS_1122 Set R1148 = R1148+:pCount Where R1147=:pVacCode ", "pCount", FVacCount, "", "pVacCode", FVacCode);
           if (FQ->Transaction->Active) FQ->Transaction->Commit();
           RC = "ok";
         }
      }
     catch(Exception &E)
      {
        if (FQ->Transaction->Active) FQ->Transaction->Rollback();
        RC = E.Message;
      }
   }
  __finally
   {
     FDM->DeleteTempQuery(FQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
}
//----------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#                      ������                                                #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
__int64 __fastcall TdsVacStoreEditors::GetNewCode(UnicodeString TabId)
{
  __int64 RC = -1;
  TFDQuery *FQ = FDM->CreateTempQuery();
  try
   {
     if (!FQ->Transaction->Active)   FQ->Transaction->StartTransaction();
     FQ->Command->CommandKind = skStoredProc;
     FQ->SQL->Text = "MCODE_"+TabId;
     FQ->Prepare();
     FQ->OpenOrExecute();
     RC = FQ->FieldByName("MCODE")->AsInteger;
   }
  __finally
   {
     FDM->DeleteTempQuery(FQ);
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsVacStoreEditors::NewGUID()
{
  return icsNewGUID().UpperCase();
}
//---------------------------------------------------------------------------

