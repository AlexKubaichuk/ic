//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
int __fastcall ServiceStatusEx(SC_HANDLE AHSvc, SERVICE_STATUS_PROCESS &AStatus)
{
  int RC = 0;
  try
   {
     SERVICE_STATUS_PROCESS ssStatus;
     DWORD dwBytesNeeded;
     bool FSrvcRC = QueryServiceStatusEx(
                 AHSvc,                          // handle to service
                 SC_STATUS_PROCESS_INFO,         // information level
                 (LPBYTE) &ssStatus,             // address of structure
                 sizeof(SERVICE_STATUS_PROCESS), // size of structure
                 &dwBytesNeeded );               // size needed if buffer is too small
     if (!FSrvcRC)
      RC = GetLastError();
     else
      AStatus = ssStatus;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
 {
  try
   {
    Application->Initialize();
    //ShowMessage("������.");
    //ShowMessage(IntToStr(ParamCount()));
    //ShowMessage(ParamStr(0));
    if (ParamCount() > 0)
     {
      UnicodeString ConvSrvcName = ParamStr(1);
      int RC = 0;
      SC_HANDLE h_manager = NULL;
      SC_HANDLE h_svc = NULL;

      SERVICE_STATUS_PROCESS ssStatus;

      DWORD dwStartTickCount;
      DWORD dwTimeout = 30000; //30-second time-out

      try
       {
        h_manager = OpenSCManager(L"", NULL, SC_MANAGER_CONNECT + SC_STATUS_PROCESS_INFO);
        if (h_manager)
         {
          h_svc = OpenService(h_manager, ConvSrvcName.c_str(), SERVICE_ALL_ACCESS);
          if (h_svc)
           {
            //ShowMessage("������ "+ConvSrvcName+" ������.");
            RC = ServiceStatusEx(h_svc, ssStatus);
            if (!RC)
             {
              if (ssStatus.dwCurrentState == SERVICE_STOPPED)
               {
                RC = 0;
               }
              else if (ssStatus.dwCurrentState == SERVICE_RUNNING)
               {
                if (!ControlService(h_svc, SERVICE_CONTROL_STOP, (LPSERVICE_STATUS) & ssStatus))
                 RC = GetLastError();
                else
                 {
                  //Wait for the service to stop.
                  dwStartTickCount = GetTickCount();
                  while (ssStatus.dwCurrentState != SERVICE_STOPPED)
                   {
                    Sleep(ssStatus.dwWaitHint);
                    RC = ServiceStatusEx(h_svc, ssStatus);
                    if (RC)
                     break;
                    if (ssStatus.dwCurrentState == SERVICE_STOPPED)
                     {
                      RC = 0;
                      break;
                     }
                    if (GetTickCount() - dwStartTickCount > dwTimeout)
                     {
                      RC = 1;
                      break;
                     }
                   }
                 }
               }
             }
            if (!RC)
             {
              DeleteService(h_svc);
              //ShowMessage("������ "+ConvSrvcName+" ���������������.");
             }
           }
          else
           RC = GetLastError();
         }
       }
      __finally
       {
        if (h_svc)
         CloseServiceHandle(h_svc);
        if (h_manager)
         CloseServiceHandle(h_manager);
       }
     }
   }
  catch (Exception & exception)
   {
    Application->ShowException(& exception);
   }
  catch (...)
   {
    try
     {
      throw Exception("");
     }
    catch (Exception & exception)
     {
      Application->ShowException(& exception);
     }
   }
  return 0;
 }
//---------------------------------------------------------------------------
