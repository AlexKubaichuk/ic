// ---------------------------------------------------------------------------
#include "ICGrdMain.h"
// ---------------------------------------------------------------------------
#include "SrvcUtil.h"
#include "DKUtils.h"
#include "ExtUtils.h"
#include "srvsetting.h"
#include "msgdef.h"
#include "Winapi.TlHelp32.hpp"
#pragma package(smart_init)
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma resource "*.dfm"
TICGuardant * ICGuardant;
const UnicodeString GrdSrvcName = "ICBaseServer";
// ---------------------------------------------------------------------------
__fastcall TICGuardant::TICGuardant(TComponent * Owner) : TService(Owner)
 {
 }
void __stdcall ServiceController(unsigned CtrlCode)
 {
  ICGuardant->Controller(CtrlCode);
 }
TServiceController __fastcall TICGuardant::GetServiceController(void)
 {
  return (TServiceController) ServiceController;
 }
// ---------------------------------------------------------------------------
void __fastcall TICGuardant::ServiceAfterInstall(TService * Sender)
 {
  SC_HANDLE schSCManager;
  SC_HANDLE schService;
  SERVICE_DESCRIPTION sd;
  LPTSTR szDesc = TEXT("������ �������� �� \"��-���\"");
  // Get a handle to the SCM database.
  schSCManager = OpenSCManager(NULL, // local computer
    NULL, // ServicesActive database
    SC_MANAGER_ALL_ACCESS); // full access rights
  if (schSCManager)
   {
    // Get a handle to the service.
    schService = OpenService(schSCManager, // SCM database
      Name.c_str(), // name of service
      SERVICE_CHANGE_CONFIG); // need change config access
    if (schService)
     {
      // Change the service description.
      sd.lpDescription = szDesc;
      if (!ChangeServiceConfig2(schService, // handle to service
        SERVICE_CONFIG_DESCRIPTION, // change: description
        &sd)) // new description
       {
        LogMessage("ChangeServiceConfig2 failed", EVENTLOG_ERROR_TYPE);
       }
      CloseServiceHandle(schService);
      CloseServiceHandle(schSCManager);
     }
    else
     {
      LogMessage("OpenService failed", EVENTLOG_ERROR_TYPE);
      CloseServiceHandle(schSCManager);
     }
   }
  else
   {
    LogMessage("OpenSCManager failed", EVENTLOG_ERROR_TYPE);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICGuardant::FServiceStart(TService * Sender, bool & Started)
 {
  try
   {
    CurDate         = Date();
    FRestartAttampt = 3;
    FChechAttampt   = 10;
    FNightRestart   = false;
    if (LoadOpt())
     {
      SrvConnect->Port = SrvOpt->Vals[_PORTNAME_].AsIntDef(5100);
      FNightRestart    = SrvOpt->Vals["SrvRestart"].AsIntDef(0);
      FCheckProcessed  = false;
      SrvcRestart();
      Sleep(100);
      Started = true;
     }
   }
  catch (Exception & E)
   {
    LogMessage(E.Message, EVENTLOG_ERROR_TYPE);
   }
  if (Started)
   LogMessage("������ ���������", EVENTLOG_INFORMATION_TYPE);
  else
   LogMessage("������ ������ �������.", EVENTLOG_ERROR_TYPE);
 }
// ---------------------------------------------------------------------------
void __fastcall TICGuardant::ServiceExecute(TService * Sender)
 {
  try
   {
    int i = 0;
    while (!Terminated)
     {
      if (i++ > 6000)
       {
        FServiceExecute();
        i = 0;
       }
      Sleep(100);
      ServiceThread->ProcessRequests(false);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICGuardant::FServiceStop(TService * Sender, bool & Stopped)
 {
  // if (FAppOptions) delete FAppOptions;
  // FAppOptions = NULL;
  Stopped = true;
  LogMessage("������ ����������", EVENTLOG_INFORMATION_TYPE);
 }
// ---------------------------------------------------------------------------
void __fastcall TICGuardant::FServiceExecute()
 {
  bool FValid = CheckSrvc();
  if (!FValid)
   FChechAttampt-- ;
  if (!FValid && (FChechAttampt <= 0))
   {
    if ((FRestartAttampt > 0))
     {
      SrvcRestart();
      Sleep(100);
      FRestartAttampt-- ;
     }
   }
  if (FNightRestart)
   {
    if ((CurDate < Date()) && (TTime::CurrentTime() > TTime("02:00")))
     {
      CurDate = Date();
      SrvcRestart();
      Sleep(10000);
     }
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TICGuardant::CheckSrvc()
 {
  bool RC = FCheckProcessed;
  if (!RC)
   {
    FCheckProcessed = true;
    TDSAdminRestClient * CheckClient = NULL;
    try
     {
      DWORD FStatus;
      int SrcvRC = ServiceStatus(GrdSrvcName, FStatus);
      if (!SrcvRC)
       {
        if (FStatus == SERVICE_RUNNING)
         {
          CheckClient = new TDSAdminRestClient(SrvConnect);
          try
           {
            RC = CheckClient->GetPlatformName().Length();
            if (RC)
             {
              FRestartAttampt = 3;
              FChechAttampt   = 10;
             }
           }
          catch (Exception & E)
           {
           }
         }
       }
      else
       {
        LogMessage("������ ����������� ������� ������� ��-���\n��������� ���������:\n\"" +
          Dkutils::GetAPILastErrorFormatMessage(SrcvRC) + "\"", EVENTLOG_ERROR_TYPE);
       }
     }
    __finally
     {
      FCheckProcessed = false;
      if (CheckClient)
       delete CheckClient;
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TICGuardant::SrvcRestart()
 {
  DWORD FStatus;
  int RC = ServiceStatus(GrdSrvcName, FStatus);
  if (!RC)
   {
    bool CanStartSrvc = true;
    if (FStatus == SERVICE_RUNNING)
     {
      RC = KillService(GrdSrvcName); // ServiceStop(GrdSrvcName);
      if (RC)
       {
        CanStartSrvc = false;
        LogMessage("������ ��������� ������� ��-���\n��������� ���������:\n\"" + Dkutils::GetAPILastErrorFormatMessage
          (RC) + "\"", EVENTLOG_ERROR_TYPE);
       }
     }
    else if (FStatus == SERVICE_STOPPED)
     CanStartSrvc = true;
    if (CanStartSrvc)
     {
      RC = ServiceStart(GrdSrvcName);
      if (RC)
       LogMessage("������ ������� ������� ��-���\n��������� ���������:\n\"" + Dkutils::GetAPILastErrorFormatMessage(RC)
        + "\"", EVENTLOG_ERROR_TYPE);
     }
   }
  else
   LogMessage("������ ����������� ������� ������� ��-���\n��������� ���������:\n\"" +
    Dkutils::GetAPILastErrorFormatMessage(RC) + "\"", EVENTLOG_ERROR_TYPE);
 }
// ---------------------------------------------------------------------------
void __fastcall TICGuardant::SrvOptLoadXML(TTagNode * AOptNode)
 {
  UnicodeString FFN = icsPrePath(icsPrePath(ExtractFilePath(ParamStr(0))) + "\\" + UnicodeString(_OPT_DEF_FILE_NAME_));
  if (FileExists(FFN))
   {
    AOptNode->LoadFromZIPXMLFile(FFN);
   }
  else
   LogMessage("������ �������� �������� �������� '" + UnicodeString(_OPT_DEF_FILE_NAME_) + "'", EVENTLOG_ERROR_TYPE);
 }
// ---------------------------------------------------------------------------
bool __fastcall TICGuardant::LoadOpt()
 {
  bool RC = false;
  try
   {
    try
     {
      UnicodeString FXMLPath = icsPrePath(ExtractFilePath(ParamStr(0)));
      UnicodeString FFN = icsPrePath(FXMLPath + "\\" + UnicodeString(_OPT_FILE_NAME_));
      if (!FileExists(FFN))
       {
        ForceDirectories(FXMLPath);
        TTagNode * tmp = new TTagNode;
        try
         {
          tmp->Encoding = "utf-8";
          tmp->Name     = "g";
          tmp->SaveToXMLFile(FFN, "");
         }
        __finally
         {
          delete tmp;
         }
       }
      SrvOptXMLData->FileName = FFN;
      SrvOpt->LoadXML();
      RC = true;
     }
    catch (Exception & E)
     {
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall GetAPILastErrorFormatMessage()
 {
  UnicodeString ErrMsg;
  LPVOID lpMsgBuf;
  int nErrorCode = GetLastError();
  FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL,
    nErrorCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
    (LPTSTR) & lpMsgBuf, 0, NULL);
  ErrMsg = static_cast<LPCTSTR>(lpMsgBuf);
  LocalFree(lpMsgBuf);
  return ErrMsg;
 }
// ---------------------------------------------------------------------------
HANDLE GetProcessHandle(wchar_t * szExeName)
 {
  Winapi::Tlhelp32::PROCESSENTRY32 Pc =
   {
    sizeof(Winapi::Tlhelp32::PROCESSENTRY32)
   };
  HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);
  if (Process32First(hSnapshot, &Pc))
   {
    do
     {
      if (!wcscmp(Pc.szExeFile, szExeName))
       {
        return OpenProcess(PROCESS_ALL_ACCESS, TRUE, Pc.th32ProcessID);
       }
     }
    while (Process32Next(hSnapshot, &Pc));
   }
  return NULL;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICGuardant::KillService(UnicodeString ASrvcName)
 {
  bool RC = true;
  try
   {
    try
     {
      int x;
      HANDLE kill = GetProcessHandle(ASrvcName.c_str());
      DWORD fdwExit = 0;
      GetExitCodeProcess(kill, & fdwExit);
      TerminateProcess(kill, fdwExit);
      x  = CloseHandle(kill);
      RC = false;
     }
    catch (System::Sysutils::Exception & E)
     {
      MessageBox(Forms::Application->Handle, E.Message.c_str(), L"������ 21", MB_ICONINFORMATION);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
