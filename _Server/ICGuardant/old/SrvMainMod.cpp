//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SrvMainMod.h"
#include "srvsetting.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSrvMain *SrvMain;
//---------------------------------------------------------------------------
__fastcall TSrvMain::TSrvMain(TComponent* Owner, TConvertVPOptions* AAppOptions)
        : TDataModule(Owner)
{
  FAppOptions = AAppOptions;
  cmdServer->Port = FAppOptions->Port;
}
//---------------------------------------------------------------------------
void __fastcall TSrvMain::DataModuleCreate(TObject *Sender)
{
  FReadParamsMode = false;
  FCurCmd = "";
  FCmdWithPapams = "";
  FCmdParams = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TSrvMain::DataModuleDestroy(TObject *Sender)
{
  if (FCmdParams) delete FCmdParams;
  FCmdParams = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TSrvMain::cmdServerClientConnect(TObject *Sender,
      TCustomWinSocket *Socket)
{
  LogMessage("ClientConnect", EVENTLOG_INFORMATION_TYPE);
}
//---------------------------------------------------------------------------
void __fastcall TSrvMain::cmdServerClientDisconnect(TObject *Sender,
      TCustomWinSocket *Socket)
{
  LogMessage("ClientDisconnect", EVENTLOG_INFORMATION_TYPE);
}
//---------------------------------------------------------------------------
void __fastcall TSrvMain::cmdServerClientError(TObject *Sender,
      TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int &ErrorCode)
{
  LogMessage("Error, code="+IntToStr(ErrorCode), EVENTLOG_ERROR_TYPE);
  ErrorCode = 0;
}
//---------------------------------------------------------------------------
void __fastcall TSrvMain::cmdServerClientRead(TObject *Sender,
      TCustomWinSocket *Socket)
{
  AnsiString Command = Socket->ReceiveText().Trim().LowerCase();
  AnsiString RetData = "ok";
  _DEBUG_LOG_("cmdServerClientRead. Command: "+Command);
  try
   {
     if (StrInList(FCmdWithPapams, Command))
      {
        if (FCmdParams) delete FCmdParams;
        FCmdParams = new TStringList;
        FReadParamsMode = true;
        FCurCmd = Command;
      }
     else if (FReadParamsMode)
      {
        if (Command == "params_end")
         {
           FReadParamsMode = false;
           FSrvCommand(FCurCmd, FCmdParams);
           delete FCmdParams;
           FCmdParams = NULL;
           FCurCmd = "";
         }
        else
         {
           FCmdParams->Add(Command);
         }
      }
     else
      {
        FReadParamsMode = false;
        FCurCmd = "";
        RetData = FSrvCommand(Command, NULL);
      }
   }
  __finally
   {
     RetData = RetData.Trim();
     Socket->SendText(RetData);
   }
}
//---------------------------------------------------------------------------
void __fastcall TSrvMain::cmdServerClientWrite(TObject *Sender,
      TCustomWinSocket *Socket)
{
  Socket->SendText(NewGUI());
}
//---------------------------------------------------------------------------
void __fastcall TSrvMain::LogMessage(const AnsiString &AValue, unsigned EventType)
{
  if (FLogger)
   FLogger(AValue, EventType);
}
//---------------------------------------------------------------------------
void __fastcall TSrvMain::LogMsg(const AnsiString &AValue)
{
  if (FLogger)
   FLogger(AValue, -10);
}
//---------------------------------------------------------------------------
void __fastcall TSrvMain::_DEBUG_LOG_(const AnsiString &AValue)
{
  if (FLogger)
   FLogger(AValue, -5);
}
//---------------------------------------------------------------------------
void __fastcall TSrvMain::FProcessRequests()
{
  if (FProcessRequestsEvt)
   FProcessRequestsEvt(this);
}
//---------------------------------------------------------------------------
bool __fastcall TSrvMain::FGetProgress()
{
  bool RC = false;
  try
   {
     if (FGetProgressEvent)
       RC = FGetProgressEvent();
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TSrvMain::FGetSrvActive()
{
  return cmdServer->Active;
}
//---------------------------------------------------------------------------
void __fastcall TSrvMain::FSetSrvActive(bool AValue)
{
  cmdServer->Active = AValue;
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSrvMain::FSrvCommand(const AnsiString &ACmd, TStringList *ACmdParams)
{
  AnsiString RC = "";
  try
   {
     if (FOnCommand)
      RC = FOnCommand(ACmd, ACmdParams);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

