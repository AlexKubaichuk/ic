//---------------------------------------------------------------------------

#ifndef TransportModH
#define TransportModH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "Globals.h"
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
typedef void  (__stdcall *TLogCallBack)(char* AMessage);
typedef void  (__stdcall *TransportInitProc)(int AIFACode, char* ADBName, char *AOptions, TLogCallBack ALog);
typedef void  (__stdcall *TransportDeinitProc)(int AIFACode);
typedef char* (__stdcall *TransportGetNameProc)(void);
typedef bool  (__stdcall *TransportSendDataProc)(char* AUserID, char* AUserAlias, char* AUserName, char* ASendSubject, char* ASendDataList);
typedef int   (__stdcall *TransportGetDataProc)(char* AUserID, char* AUserAlias, char* AUserName, char* ARecivedDataList);
typedef bool  (__stdcall *TransportGetSettingProc)(char *ASrcData, char *ARetData);
typedef void  (__stdcall *TransportDelPackageProc)(char *APkgId);
typedef void  (__stdcall *TransportGetParentOrgDataProc)(char* AUserID, char* AUserAlias, char* AUserName);
//---------------------------------------------------------------------------
class TTransportClient : public TDataModule
{
__published:	// IDE-managed Components
        TTimer *ReceiveTimer;
        void __fastcall ReceiveTimerTimer(TObject *Sender);
private:	// User declarations
        TConvertVPOptions* FAppOptions;
        TServiceLogEvent FLogger;
        TNotifyEvent FProcessRequestsEvt;
        TServiceGetProgressEvent FGetProgressEvent;
        TServiceSetProgressEvent FSetProgressEvent;
        TServiceCmdExecEvent     FCmdExec;
        AnsiString               FTmpOutFolderName;

        HINSTANCE    FTransportModule;

        TransportInitProc             FTransportDllInit;
        TransportDeinitProc           FTransportDllDeinit;
        TransportGetNameProc          FTransportDllGetName;
        TransportSendDataProc         FTransportDllSendData;
        TransportGetDataProc          FTransportDllGetData;
        TransportGetSettingProc       FTransportDllGetSetting;
        TransportDelPackageProc       FTransportDelPackage;
        TransportGetParentOrgDataProc FTransportDllGetParentOrgData;

        bool __fastcall FGetReceiveActive();
        void __fastcall FSetReceiveActive(bool AVal);
        void __fastcall LogMessage(const AnsiString &AValue, unsigned EventType);
        void __fastcall LogMsg(const AnsiString &AValue);
        void __fastcall LogErr(const AnsiString &AValue);
        void __fastcall _DEBUG_LOG_(const AnsiString &AValue);
        void __fastcall FProcessRequests();
        bool __fastcall FGetProgress();
        void __fastcall FSetProgress(bool AVal);
        void __fastcall FReceiveData();

        void __fastcall FExecCommand(const AnsiString &APkgName, const AnsiString &AUserID, const AnsiString &AUserAlias, const AnsiString &AUserName, TTagNode *AAttachNode, TTagNode *ACmd);
        AnsiString __fastcall GetFileNameFromAttach(TTagNode *AAttachNode, AnsiString AFN);
        bool __fastcall TTransportClient::SendDataTo(TTransportPackData *APkg, const AnsiString &AExportFN);
public:		// User declarations
        __fastcall TTransportClient(TComponent* Owner, TConvertVPOptions* AAppOptions);


        bool __fastcall SendDataToParent(const AnsiString &AExportFN);
        bool __fastcall SendDataToOwner(TTransportPackData *APkg, const AnsiString &AExportFN);
        bool __fastcall SendChGUINToOwner(TTransportPackData *APkg, const AnsiString &AOldGUID, const AnsiString &ANewGUID);
        void __fastcall DeletePackage(const AnsiString &APkgID);

        void __fastcall SendTicket(TTransportPackData *APkg);


        __property TServiceLogEvent OnLogMessage = {read=FLogger, write=FLogger};
        __property TNotifyEvent OnProcessRequestsRequired = {read=FProcessRequestsEvt, write=FProcessRequestsEvt};
        __property TServiceGetProgressEvent OnGetProgress = {read=FGetProgressEvent, write=FGetProgressEvent};
        __property TServiceSetProgressEvent OnSetProgress = {read=FSetProgressEvent, write=FSetProgressEvent};
        __property TServiceCmdExecEvent     OnCmdExec     = {read=FCmdExec, write=FCmdExec};

        __property AnsiString TmpOutFolderName            = {read=FTmpOutFolderName, write=FTmpOutFolderName};

        __property bool ReceiveActive = {read=FGetReceiveActive, write=FSetReceiveActive};
};
//---------------------------------------------------------------------------
extern PACKAGE TTransportClient *TransportClient;
extern void __stdcall TransportDllLog(char* AMessage);
//---------------------------------------------------------------------------
#endif
