//---------------------------------------------------------------------------
#include "MainICTransportSrv.h"
//#include "Options.h"
#include <DateUtils.hpp>
#include "icsLog.h"
#include "SrvcUtil.h"
#include "srvsetting.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "DKVCLVerInfo"
#pragma resource "*.dfm"

TICTransportSrv *ICTransportSrv;
//---------------------------------------------------------------------------
void __stdcall TransportDllLog(char* AMessage)
{
  ICTransportSrv->Log->LogMessage(AMessage);
}
//---------------------------------------------------------------------------
__fastcall TICTransportSrv::TICTransportSrv(TComponent* Owner)
	: TService(Owner)
{
  FSyncClient = NULL;
  FMainServer = NULL;
  FTransportClient = NULL;
}
//---------------------------------------------------------------------------
TServiceController __fastcall TICTransportSrv::GetServiceController(void)
{
	return (TServiceController) ServiceController;
}
//---------------------------------------------------------------------------
void __stdcall ServiceController(unsigned CtrlCode)
{
	ICTransportSrv->Controller(CtrlCode);
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::ServiceCreate(TObject *Sender)
{
  SetCurrentDir(icsPrePath(ExtractFilePath(ParamStr(0))));
  TmpOutFolderName = icsPrePath(ExtractFilePath(ParamStr(0))+"\\tmp");
  if (!DirectoryExists(TmpOutFolderName))
   ForceDirectories(TmpOutFolderName);
  FAppOptions     = NULL;
  FLog = new TLog;
  AnsiString sDirName = icsPrePath(ExtractFileDir(ParamStr(0)) + "\\"+AnsiString(_LOGDIR_));
  if (!DirectoryExists(sDirName))
    ForceDirectories(sDirName);
  FLog->FileName = sDirName + "\\ictransport_" + Date().FormatString("yyyymmdd") + ".log";
  FLog->Active = true;

  LastConvStateCaption = "";
//  CurConvStateStr = "";

  FSrvProgress = false;
  _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] ServiceCreate.");
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::ServiceDestroy(TObject *Sender)
{
  if (FSyncClient) delete FSyncClient;
  FSyncClient = NULL;
  if (FMainServer) delete FMainServer;
  FMainServer = NULL;
  if (FTransportClient) delete FTransportClient;
  FTransportClient = NULL;

}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::LoadOptions()
{
  if (FAppOptions) delete FAppOptions;
  FAppOptions = new TConvertVPOptions;
  try
   {
     FAppOptions->Load();
   }
  catch(EOptLoadErr &E)
   {
     FLog->LogError(E.Message);
   }
  InitProgressCtrls();

  ServiceThread->ProcessRequests(false);

  LogMessage("�������� ��������", EVENTLOG_INFORMATION_TYPE);
  _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] LoadOptions: SRV: localhost:"+FAppOptions->Port+", SyncSrv: "+FAppOptions->EIHost+":"+FAppOptions->EIPort);
  ServiceThread->ProcessRequests(false);
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::ServiceStart(TService *Sender, bool &Started)
{
  LoadOptions();
  FSyncClient = new TSyncClient(this, FAppOptions);
  FSyncClient->OnLogMessage              = SyncClientLogErrMessage;
  FSyncClient->OnProgress                = SyncClientProgress;
  FSyncClient->OnProcessRequestsRequired = FOnProcessRequests;
  FSyncClient->OnGetProgress             = FOnGetInProgress;
  FSyncClient->CurConvStateStr = "";

  FMainServer = new TSrvMain(this, FAppOptions);
  FMainServer->OnLogMessage              = SyncClientLogErrMessage;
  FMainServer->OnProcessRequestsRequired = FOnProcessRequests;
  FMainServer->OnCommand                 = FOnServiceCommand;
  FMainServer->CmdWithPapams             = "start";
  FMainServer->OnGetProgress             = FOnGetInProgress;

  FTransportClient = new TTransportClient(this, FAppOptions);
  FTransportClient->OnLogMessage              = SyncClientLogErrMessage;
  FTransportClient->OnProcessRequestsRequired = FOnProcessRequests;
  FTransportClient->OnGetProgress             = FOnGetInProgress;
  FTransportClient->OnSetProgress             = FOnSetInProgress;
  FTransportClient->OnCmdExec                 = FExecCommand;
  FTransportClient->TmpOutFolderName          = TmpOutFolderName;
  FTransportClient->ReceiveActive             = true;

  _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] ServiceStart.");
  FSrvProgress = false;
  _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] Transport dll loaded.");
  FMainServer->SrvActive = true;
  Started = true;
  if (Started)
   LogMessage("������ ���������", EVENTLOG_INFORMATION_TYPE);
  else
   LogMessage("������ ������ �������.", EVENTLOG_INFORMATION_TYPE);
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::ServiceStop(TService *Sender, bool &Stopped)
{
  FTransportClient->ReceiveActive = false;

  if (FSyncClient) delete FSyncClient;
  FSyncClient = NULL;
  FMainServer->SrvActive = false;
  if (FMainServer) delete FMainServer;
  FMainServer = NULL;

  if (FAppOptions) delete FAppOptions;
  FAppOptions = NULL;
//!!!  cmdServer->Active = false;
//!!!  cmdSyncClient->Active = false;
  Stopped = true;
  LogMessage("������ ����������", EVENTLOG_INFORMATION_TYPE);
  _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] ServiceStop.");
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::ServiceExecute(TService *Sender)
{
  try
   {
     while (!Terminated)
      {
        FServiceExecute();
        ServiceThread->ProcessRequests(false);
      }
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::FServiceExecute()
{
  if (!FSrvProgress)
   {
      TTransportPackData *FReceiveCmd;
      if (FCmdBuffer.size())
       { // ��������� ������ �� �������
         FSrvProgress = true;
         try
          {
            FReceiveCmd = FCmdBuffer.front();
            if (FReceiveCmd->UserID == "start")
             {
               _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] UnitUploadStart.");
               AnsiString ExportFN = TmpOutFolderName+"\\unit_e.zxml";
               if (FSyncClient->UnitUploadStart(FReceiveCmd->UserAlias.Trim(), ExportFN))
                {
                  _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] Service 'ICSyncSrv' execute complite.");
                  FTransportClient->SendDataToParent(ExportFN);
                }
             }
          }
         __finally
          {
            delete FReceiveCmd;
            FCmdBuffer.pop_front();
            FSrvProgress = false;
          }
       }
      else if (FReceiveCmdBuffer.size())
       { // ��������� ������ ������������ �������
         FSrvProgress = true;
         try
          {
            FReceiveCmd = FReceiveCmdBuffer.front();
            TTagNode *itCmd = FReceiveCmd->Cmd->GetFirstChild();
            FTransportClient->SendTicket(FReceiveCmd);
            AnsiString FAttFileName;
            while (itCmd)
             {
               FAttFileName = "";
               if (itCmd->AV["fn"].Trim().Length())
                FAttFileName = GetFileNameFromAttach(FReceiveCmd->AttachNode, itCmd->AV["fn"]);

               if (itCmd->CmpName("import_cls"))
                { // ������������� ��������������
                  FSyncClient->ClsDownloadStart(FAttFileName);
                }
               else if (itCmd->CmpName("import_unit"))
                { // ������������� ������ ���������
                  FSyncClient->UnitDownloadStart(FAttFileName);
                }
               else if (itCmd->CmpName("change_guin"))
                { // �������� guin ��������
                  FSyncClient->UnitChangeGUINStart(itCmd->AV["clid"],itCmd->AV["old"],itCmd->AV["new"]);
                }
               else if (itCmd->CmpName("export_cls"))
                { // �������������� �������������� // �� �����
//                  FSyncClient->ClsUploadStart(ACmd->AV["gui"], AUserID, AUserAlias, AUserName, false, FAttFileName);
                }
               else if (itCmd->CmpName("export_unit"))
                { // �������������� ������ ��������� // �� �����
//                  FSyncClient->UnitUploadStart(ACmd->AV["gui"], AUserID, AUserAlias, AUserName, false, FAttFileName);
                }
               else if (itCmd->CmpName("delete_cls"))
                { // ������� ������ ��������������(��)
//                  FSyncClient->ClsDeleteStart(FAttFileName);
                }
               else if (itCmd->CmpName("delete_unit"))
                { // ������� ������ ��������(��)
//                  FSyncClient->UnitDeleteStart(FAttFileName);
                }
               itCmd = itCmd->GetNext();
             }
          }
         __finally
          {
            FTransportClient->DeletePackage(FReceiveCmd->PkgName);
            delete FReceiveCmd;
            FReceiveCmdBuffer.pop_front();
            FSrvProgress = false;
          }
       }
   }
  Sleep(10);
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::FReceiveData()
{
}
//---------------------------------------------------------------------------

void __fastcall TICTransportSrv::FExecCommand(const AnsiString &APkgName, const AnsiString &AUserID, const AnsiString &AUserAlias, const AnsiString &AUserName, TTagNode *AAttachNode, TTagNode *ACmd)
{
  FReceiveCmdBuffer.push_back(new TTransportPackData(APkgName, AUserID, AUserAlias, AUserName, AAttachNode, ACmd));
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::ConvertStop()
{
  LogMessage("������������� �� �����������", EVENTLOG_INFORMATION_TYPE);
  FSyncClient->CurConvStateStr = "";
  FSrvProgress = false;
  InitProgressCtrls();
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::ExecutionBegin(TObject *Sender)
{
  InitProgressCtrls();
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::InitProgressCtrls()
{
  CurProgressCaption = "";
  CurPBPosition = 0;
  TotalProgressCaption = "";
  TotalPBPosition = 0;
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::ServiceAfterInstall(TService *Sender)
{
  AnsiString FDescDef = "������ ���������� �� \"���������� ������������\"";
  SC_HANDLE h_manager, h_svc;
  SERVICE_DESCRIPTION FDesc;
  FDesc.lpDescription = new char[FDescDef.Length()+1];
  memset(FDesc.lpDescription,0,FDescDef.Length()+1);
  strcpy(FDescDef.c_str(),FDesc.lpDescription);
  try
   {
     h_manager = OpenSCManager("" ,NULL, SC_MANAGER_CONNECT);
     if (h_manager)
      {
        h_svc = OpenService(h_manager, Name.c_str(), SERVICE_CHANGE_CONFIG);
        if (h_svc)
         {
           ChangeServiceConfig2(h_svc, SERVICE_CONFIG_DESCRIPTION, &FDesc);
           CloseServiceHandle(h_svc);
         }
        CloseServiceHandle(h_manager);
      }
   }
  __finally
   {
     delete FDesc.lpDescription;
   }
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::_DEBUG_LOG_(const AnsiString &AMsg)
{
#ifdef _DEBUG_
  FLog->LogMessage(AMsg);
#endif
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::SyncClientProgress(const AnsiString &AValue, unsigned int EventType)
{
  TTagNode *PrData = new TTagNode(NULL);
  TTagNode *tmp;
  try
   {
     try
      {
        PrData->AsXML = AValue;
        tmp = PrData->GetFirstChild();
        if (tmp->AV["PCDATA"].Trim().Length())
         CurProgressCaption = tmp->AV["PCDATA"];
        CurPBPosition       = tmp->AV["p"].ToIntDef(0);
        CurPBPropertiesMax  = tmp->AV["m"].ToIntDef(0);

        tmp = tmp->GetNext();
        if (tmp->AV["PCDATA"].Trim().Length())
         TotalProgressCaption = tmp->AV["PCDATA"];
        TotalPBPosition      = tmp->AV["p"].ToIntDef(0);
        TotalPBPropertiesMax = tmp->AV["m"].ToIntDef(0);
      }
     catch (...)
      {
      }
   }
  __finally
   {
     delete PrData;
   }
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::SyncClientLogErrMessage(const AnsiString &AValue, unsigned int EventType)
{
  if (EventType == -5)
   _DEBUG_LOG_(AValue);
  else if (EventType == -10)
   FLog->LogMessage(AValue);
  else if (EventType == -15)
   FLog->LogError(AValue);
  else
   LogMessage(AValue.c_str(), EventType);
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::FOnProcessRequests(TObject* Sender)
{
  ServiceThread->ProcessRequests(false);
}
//---------------------------------------------------------------------------
bool __fastcall TICTransportSrv::FOnGetInProgress()
{
  return FSrvProgress;
}
//---------------------------------------------------------------------------
void __fastcall TICTransportSrv::FOnSetInProgress(bool AVal)
{
  FSrvProgress = AVal;
}
//---------------------------------------------------------------------------
AnsiString __fastcall TICTransportSrv::FOnServiceCommand(const AnsiString &ACmd, TStringList *ACmdParams)
{
  AnsiString RC = "";
  try
   {
     RC = "ok";
     _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] cmdServerClientRead. Command: "+ACmd);
     if (ACmd == "get state")
      {
        _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] cmdServerClientRead. Progress="+IntToStr((int)FSrvProgress));
        if (FSrvProgress)
         RC = "progress";
        else
         RC = "waiting";
      }
     else if (ACmd == "get state str")
      {
        RC = FSyncClient->CurConvStateStr;
      }
     else if (ACmd == "get last state")
      {
        _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] cmdServerClientRead. LastConvStateCaption="+LastConvStateCaption);
        if (LastConvStateCaption.Length())
         {
           RC = LastConvStateCaption;
           LastConvStateCaption = "";
         }
      }
     else if (ACmd == "get progress")
      {
        _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] cmdServerClientRead.");
        if (FSrvProgress)
         RC = "<pr><c p='"+IntToStr(CurPBPosition)+"' m='"+IntToStr(CurPBPropertiesMax)+"'>"+CurProgressCaption+"</c><t p='"+IntToStr(TotalPBPosition)+"' m='"+IntToStr(TotalPBPropertiesMax)+"'>"+TotalProgressCaption+"</t></pr>";
      }

     else if (ACmd == "start")
      {
        _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] TCPSrvAccept.");
        if (FSrvProgress)
         RC = "progress";
        else
         {
           FCmdBuffer.push_back(new TTransportPackData("", ACmd, ACmdParams->Text, "", NULL, NULL));
         }
      }
     else if (ACmd == "stop")
      {
        _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] cmdServerClientRead.");
        if (FSrvProgress)
         ConvertStop();
      }
     else
      {
        RC = "error command";
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
/*
void __fastcall TICTransportSrv::ClsUploadStart(const AnsiString &ATicketID, const AnsiString &AUserID, const AnsiString &AUserAlias, const AnsiString &AUserName, bool AFromSchedule, const AnsiString &AClsCodesFN)
{
  FSrvProgress = true;
  CurConvStateStr = "�������� ������������";
  if (AFromSchedule)
   CurConvStateStr += " �� ����������";
  LogMessage(CurConvStateStr.c_str(), EVENTLOG_INFORMATION_TYPE);
  try
   {
     _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] ClsUploadStart.");
     int RC = ::ServiceStart(ConvSrvcName);
     AnsiString ExportFN = TmpOutFolderName+"\\cls_e.zxml";
     AnsiString CmdFN = TmpOutFolderName+"\\cmd.zxml";
     if (!RC || (RC == 1))
      {
        _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] Service 'ICSyncSrv' started.");
        _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] SendStartCommand(\"start /e /c /fn \""+ExportFN+"\" ok\")");
        SendStartCommand("/e\n/c\n/fn\n\""+ExportFN+"\"\n/lu\n"+FAppOptions->LastClsSync.FormatString("dd.mm.yyyy"));
        if (CheckSyncProgress())
         {
           _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] Service 'ICSyncSrv' execute complite.");
           if (FileExists(ExportFN))
            {
              _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] File \""+ExportFN+"\" exists.");
              char FUserID[256];
              char FUserAlias[256];
              char FUserName[1024];
              memset(FUserID,0,256);
              memset(FUserAlias,0,256);
              memset(FUserName,0,1024);
              TTagNode *FCommandNode = new TTagNode(NULL);
              TTagNode *FAttNode = new TTagNode(NULL);
              try
               {
                 FCommandNode->Name = "cmd";
                 FCommandNode->AV["gui"] = NewGUI();
                 if (ATicketID.Length())
                  FCommandNode->AV["ref_gui"] = ATicketID;
                 FAttNode->Name = "attlist";

                 FCommandNode->AddChild("import_cls")->AV["fn"] = ExtractFileName(ExportFN);
                 FCommandNode->SaveToZIPXMLFile(CmdFN,"");

                 FAttNode->AddChild("i")->AV["att"] = CmdFN;
                 FAttNode->AddChild("i")->AV["att"] = ExportFN;
                 try
                  {
                    if (!(AUserID+AUserAlias+AUserName).Length())
                     FTransportDllGetParentOrgData(FUserID, FUserAlias, FUserName);
                    else
                     {
                       strncpy(FUserID,    AUserID.c_str(),    AUserID.Length());
                       strncpy(FUserAlias, AUserAlias.c_str(), AUserAlias.Length());
                       strncpy(FUserName,  AUserName.c_str(),  AUserName.Length());
                     }
                  }
                 catch (Exception &E)
                  {
                    FLog->LogError("Pr["+IntToStr((int)FSrvProgress)+"] TransportSendData. "+E.Message);
                  }
                 _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] TransportGetParentOrgData: UserID: "+AnsiString(FUserID)+"  UserAlias: "+AnsiString(FUserAlias)+"  UserName: "+AnsiString(FUserName));
                 try
                  {
                    FTransportDllSendData(FUserID, FUserAlias, FUserName, "export", FAttNode->AsXML.c_str());
                    FAppOptions->LastClsSync = Date();
                    FAppOptions->Save();
                  }
                 catch (Exception &E)
                  {
                    FLog->LogError("Pr["+IntToStr((int)FSrvProgress)+"] TransportSendData. "+E.Message);
                  }
                 _DEBUG_LOG_("Pr["+IntToStr((int)FSrvProgress)+"] TransportSendData. Command: "+FAttNode->AsXML);
               }
              __finally
               {
                 delete FCommandNode;
                 delete FAttNode;
               }
            }
         }
      }
   }
  __finally
   {
     FSrvProgress = false;
   }
}
//---------------------------------------------------------------------------
*/

