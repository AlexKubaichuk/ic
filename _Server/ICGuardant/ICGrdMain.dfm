object ICGuardant: TICGuardant
  OldCreateOrder = False
  AllowPause = False
  DisplayName = 'IC server guard'
  AfterInstall = ServiceAfterInstall
  OnExecute = ServiceExecute
  OnStart = FServiceStart
  OnStop = FServiceStop
  Height = 338
  Width = 368
  object SrvOpt: TAppOptions
    OnLoadXML = SrvOptLoadXML
    DataProvider = SrvOptXMLData
    Left = 47
    Top = 23
  end
  object SrvOptXMLData: TAppOptXMLProv
    Left = 48
    Top = 86
  end
  object SrvConnect: TDSRestConnection
    Host = 'localhost'
    Port = 5100
    UserName = 'check'
    Password = 'check'
    LoginPrompt = False
    Left = 138
    Top = 23
    UniqueId = '{2561E080-5D09-4396-9D34-969204DAB404}'
  end
end
