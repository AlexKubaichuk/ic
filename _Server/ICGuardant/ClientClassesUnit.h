#ifndef ClientClassesUnitH
#define ClientClassesUnitH

#include "Data.DBXCommon.hpp"
#include "System.Classes.hpp"
#include "System.SysUtils.hpp"
#include "Data.DB.hpp"
#include "Data.SqlExpr.hpp"
#include "Data.DBXDBReaders.hpp"
#include "Data.DBXCDSReaders.hpp"
#include "DataSnap.DSProxyRest.hpp"

  class TdsICClassClient : public TDSAdminRestClient
  {
  private:
    TDSRestCommand *FAppOptLoadXMLCommand;
    TDSRestCommand *FDisconnectCommand;
    TDSRestCommand *FGetDefXMLCommand;
    TDSRestCommand *FGetClassXMLCommand;
    TDSRestCommand *FGetClassZIPXMLCommand;
    TDSRestCommand *FSetClassZIPXMLCommand;
    TDSRestCommand *FGetCountCommand;
    TDSRestCommand *FGetIdListCommand;
    TDSRestCommand *FGetIdListCommand_Cache;
    TDSRestCommand *FGetValByIdCommand;
    TDSRestCommand *FGetValByIdCommand_Cache;
    TDSRestCommand *FGetValById10Command;
    TDSRestCommand *FGetValById10Command_Cache;
    TDSRestCommand *FFindCommand;
    TDSRestCommand *FFindCommand_Cache;
    TDSRestCommand *FFindExCommand;
    TDSRestCommand *FFindExCommand_Cache;
    TDSRestCommand *FInsertDataCommand;
    TDSRestCommand *FEditDataCommand;
    TDSRestCommand *FDeleteDataCommand;
    TDSRestCommand *FGetF63Command;
    TDSRestCommand *FGetVacDefDozeCommand;
    TDSRestCommand *FCreateUnitPlanCommand;
    TDSRestCommand *FDeletePlanCommand;
    TDSRestCommand *FDeletePatientPlanCommand;
    TDSRestCommand *FStartCreatePlanCommand;
    TDSRestCommand *FStopCreatePlanCommand;
    TDSRestCommand *FCheckCreatePlanProgressCommand;
    TDSRestCommand *FGetPatPlanOptionsCommand;
    TDSRestCommand *FGetPrivVarCommand;
    TDSRestCommand *FGetOtvodInfCommand;
    TDSRestCommand *FSetPatPlanOptionsCommand;
    TDSRestCommand *FPrintPlanCommand;
    TDSRestCommand *FGetPlanPrintFormatsCommand;
    TDSRestCommand *FGetPlanPrintFormatsCommand_Cache;
    TDSRestCommand *FGetAppOptDataCommand;
    TDSRestCommand *FSetAppOptDataCommand;
    TDSRestCommand *FSetMainLPUCommand;
    TDSRestCommand *FFindMKBCommand;
    TDSRestCommand *FFindMKBCommand_Cache;
    TDSRestCommand *FMKBCodeToTextCommand;
    TDSRestCommand *FGetPlanTemplateDefCommand;
    TDSRestCommand *FGetPlanOptCommand;
    TDSRestCommand *FSetPlanOptCommand;
    TDSRestCommand *FUploadDataCommand;
    TDSRestCommand *FGetCliOptDataCommand;
    TDSRestCommand *FSetCliOptDataCommand;
    TDSRestCommand *FSetUpdateSearchCommand;
    TDSRestCommand *FPatGrChDataCommand;
    TDSRestCommand *FFindAddrCommand;
    TDSRestCommand *FFindAddrCommand_Cache;
    TDSRestCommand *FAddrCodeToTextCommand;
    TDSRestCommand *FLPUCodeCommand;
    TDSRestCommand *FFindOrgCommand;
    TDSRestCommand *FFindOrgCommand_Cache;
    TDSRestCommand *FOrgCodeToTextCommand;
  public:
    __fastcall TdsICClassClient(TDSRestConnection *ARestConnection);
    __fastcall TdsICClassClient(TDSRestConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TdsICClassClient();
    void __fastcall AppOptLoadXML(TJSONObject* AOptNode);
    void __fastcall Disconnect();
    System::UnicodeString __fastcall GetDefXML(const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetClassXML(System::UnicodeString ARef, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetClassZIPXML(System::UnicodeString ARef, const String& ARequestFilter = String());
    bool __fastcall SetClassZIPXML(System::UnicodeString ARef, const String& ARequestFilter = String());
    __int64 __fastcall GetCount(System::UnicodeString AId, System::UnicodeString AFilterParam, System::UnicodeString  &AExtCount, const String& ARequestFilter = String());
    TJSONObject* __fastcall GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetIdList_Cache(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter = String());
    TJSONObject* __fastcall GetValById(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetValById_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    TJSONObject* __fastcall GetValById10(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetValById10_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    TJSONObject* __fastcall Find(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall Find_Cache(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter = String());
    TJSONObject* __fastcall FindEx(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall FindEx_Cache(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter = String());
    System::UnicodeString __fastcall InsertData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall EditData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall DeleteData(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetF63(System::UnicodeString ARef, System::UnicodeString APrintParams, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetVacDefDoze(System::TDateTime APrivDate, System::TDateTime APatBirthday, System::UnicodeString AVacCode, const String& ARequestFilter = String());
    System::UnicodeString __fastcall CreateUnitPlan(__int64 APtId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall DeletePlan(System::UnicodeString ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall DeletePatientPlan(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall StartCreatePlan(TJSONObject* AVal, const String& ARequestFilter = String());
    System::UnicodeString __fastcall StopCreatePlan(const String& ARequestFilter = String());
    System::UnicodeString __fastcall CheckCreatePlanProgress(System::UnicodeString  &ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetPatPlanOptions(__int64 APatCode, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetPrivVar(__int64 APrivCode, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetOtvodInf(__int64 AOtvodCode, const String& ARequestFilter = String());
    void __fastcall SetPatPlanOptions(__int64 APatCode, System::UnicodeString AData);
    System::UnicodeString __fastcall PrintPlan(__int64 ACode, const String& ARequestFilter = String());
    TJSONObject* __fastcall GetPlanPrintFormats(const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetPlanPrintFormats_Cache(const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetAppOptData(const String& ARequestFilter = String());
    void __fastcall SetAppOptData(System::UnicodeString ARef);
    void __fastcall SetMainLPU(__int64 ALPUCode);
    TJSONObject* __fastcall FindMKB(System::UnicodeString AStr, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall FindMKB_Cache(System::UnicodeString AStr, const String& ARequestFilter = String());
    System::UnicodeString __fastcall MKBCodeToText(System::UnicodeString ACode, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetPlanTemplateDef(int AType, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetPlanOpt(const String& ARequestFilter = String());
    void __fastcall SetPlanOpt(System::UnicodeString AOpt);
    bool __fastcall UploadData(System::UnicodeString ADataPart, int AType, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetCliOptData(const String& ARequestFilter = String());
    void __fastcall SetCliOptData(System::UnicodeString ARef);
    void __fastcall SetUpdateSearch(bool AVal);
    bool __fastcall PatGrChData(__int64 APtId, System::UnicodeString AData, const String& ARequestFilter = String());
    TJSONObject* __fastcall FindAddr(System::UnicodeString AStr, System::UnicodeString ADefAddr, int AParams, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall FindAddr_Cache(System::UnicodeString AStr, System::UnicodeString ADefAddr, int AParams, const String& ARequestFilter = String());
    System::UnicodeString __fastcall AddrCodeToText(System::UnicodeString ACode, int AParams, const String& ARequestFilter = String());
    __int64 __fastcall LPUCode(const String& ARequestFilter = String());
    TJSONObject* __fastcall FindOrg(System::UnicodeString AStr, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall FindOrg_Cache(System::UnicodeString AStr, const String& ARequestFilter = String());
    System::UnicodeString __fastcall OrgCodeToText(System::UnicodeString ACode, System::UnicodeString ASetting, const String& ARequestFilter = String());
  };

  class TdsDocClassClient : public TDSAdminRestClient
  {
  private:
    TDSRestCommand *FDisconnectCommand;
    TDSRestCommand *FGetDefXMLCommand;
    TDSRestCommand *FGetClassXMLCommand;
    TDSRestCommand *FGetClassZIPXMLCommand;
    TDSRestCommand *FGetCountCommand;
    TDSRestCommand *FGetIdListCommand;
    TDSRestCommand *FGetIdListCommand_Cache;
    TDSRestCommand *FGetValByIdCommand;
    TDSRestCommand *FGetValByIdCommand_Cache;
    TDSRestCommand *FGetValById10Command;
    TDSRestCommand *FGetValById10Command_Cache;
    TDSRestCommand *FFindCommand;
    TDSRestCommand *FFindCommand_Cache;
    TDSRestCommand *FImportDataCommand;
    TDSRestCommand *FInsertDataCommand;
    TDSRestCommand *FEditDataCommand;
    TDSRestCommand *FDeleteDataCommand;
    TDSRestCommand *FSetTextDataCommand;
    TDSRestCommand *FGetTextDataCommand;
    TDSRestCommand *FGetSpecByIdCommand;
    TDSRestCommand *FGetDocByIdCommand;
    TDSRestCommand *FCreateDocPreviewCommand;
    TDSRestCommand *FSaveDocCommand;
    TDSRestCommand *FCheckByPeriodCommand;
    TDSRestCommand *FDocListClearByDateCommand;
    TDSRestCommand *FGetDocCountBySpecCommand;
    TDSRestCommand *FSetMainLPUCommand;
    TDSRestCommand *FGetMainLPUParamCommand;
    TDSRestCommand *FMainOwnerCodeCommand;
    TDSRestCommand *FIsMainOwnerCodeCommand;
    TDSRestCommand *FCopySpecCommand;
    TDSRestCommand *FCheckSpecExistsCommand;
    TDSRestCommand *FCheckSpecExistsByCodeCommand;
    TDSRestCommand *FCheckDocCreateTypeCommand;
    TDSRestCommand *FDocCreateCheckExistCommand;
    TDSRestCommand *FCheckFilterCommand;
    TDSRestCommand *FDocCreateCheckFilterCommand;
    TDSRestCommand *FDocCreateCommand;
    TDSRestCommand *FStartDocCreateCommand;
    TDSRestCommand *FDocCreateProgressCommand;
    TDSRestCommand *FLoadDocPreviewProgressCommand;
    TDSRestCommand *FGetQListCommand;
    TDSRestCommand *FGetQListCommand_Cache;
    TDSRestCommand *FSetQListCommand;
    TDSRestCommand *FGetFilterByGUICommand;
  public:
    __fastcall TdsDocClassClient(TDSRestConnection *ARestConnection);
    __fastcall TdsDocClassClient(TDSRestConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TdsDocClassClient();
    void __fastcall Disconnect();
    System::UnicodeString __fastcall GetDefXML(const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetClassXML(System::UnicodeString ARef, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetClassZIPXML(System::UnicodeString ARef, const String& ARequestFilter = String());
    __int64 __fastcall GetCount(System::UnicodeString AId, System::UnicodeString AFilterParam, const String& ARequestFilter = String());
    TJSONObject* __fastcall GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetIdList_Cache(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter = String());
    TJSONObject* __fastcall GetValById(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetValById_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    TJSONObject* __fastcall GetValById10(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetValById10_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    TJSONObject* __fastcall Find(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall Find_Cache(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter = String());
    System::UnicodeString __fastcall ImportData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall InsertData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall EditData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall DeleteData(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall SetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString AVal, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString  &AMsg, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetSpecById(System::UnicodeString AId, int  &ASpecType, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetDocById(System::UnicodeString AId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall CreateDocPreview(System::UnicodeString AId, int  &Al, int  &At, int  &Ar, int  &Ab, double  &Aps, int  &Ao, const String& ARequestFilter = String());
    void __fastcall SaveDoc(System::UnicodeString AId);
    System::UnicodeString __fastcall CheckByPeriod(bool FCreateByType, System::UnicodeString ACode, System::UnicodeString ASpecGUI, const String& ARequestFilter = String());
    System::UnicodeString __fastcall DocListClearByDate(System::TDateTime ADate, const String& ARequestFilter = String());
    int __fastcall GetDocCountBySpec(System::UnicodeString ASpecOWNER, System::UnicodeString ASpecGUI, System::UnicodeString  &ARCMsg, const String& ARequestFilter = String());
    void __fastcall SetMainLPU(__int64 ALPUCode);
    System::UnicodeString __fastcall GetMainLPUParam(const String& ARequestFilter = String());
    System::UnicodeString __fastcall MainOwnerCode(const String& ARequestFilter = String());
    bool __fastcall IsMainOwnerCode(System::UnicodeString AOwnerCode, const String& ARequestFilter = String());
    System::UnicodeString __fastcall CopySpec(System::UnicodeString AName, System::UnicodeString  &ANewGUI, const String& ARequestFilter = String());
    int __fastcall CheckSpecExists(System::UnicodeString AName, System::UnicodeString  &AGUI, bool AIsEdit, const String& ARequestFilter = String());
    bool __fastcall CheckSpecExistsByCode(System::UnicodeString AGUI, const String& ARequestFilter = String());
    bool __fastcall CheckDocCreateType(System::UnicodeString ASpecGUI, int  &ACreateType, int ADefCreateType, int  &AGetCreateType, int  &AGetPeriod, const String& ARequestFilter = String());
    System::UnicodeString __fastcall DocCreateCheckExist(System::UnicodeString ASpecGUI, System::UnicodeString AAuthor, int APerType, System::TDateTime APeriodDateFr, System::TDateTime APeriodDateTo, const String& ARequestFilter = String());
    System::UnicodeString __fastcall CheckFilter(System::UnicodeString AFilterGUI, const String& ARequestFilter = String());
    System::UnicodeString __fastcall DocCreateCheckFilter(System::UnicodeString ASpecGUI, System::UnicodeString AFilterCode, System::UnicodeString  &AFilterName, System::UnicodeString  &AFilterGUI, const String& ARequestFilter = String());
    System::UnicodeString __fastcall DocCreate(System::UnicodeString ASpecGUI, System::UnicodeString AAuthor, System::UnicodeString AFilter, bool AConcr, int ACreateType, int APerType, System::TDateTime APeriodDateFr, System::TDateTime APeriodDateTo, const String& ARequestFilter = String());
    System::UnicodeString __fastcall StartDocCreate(System::UnicodeString ASpecGUI, System::UnicodeString AAuthor, System::UnicodeString AFilter, System::UnicodeString AFilterNameGUI, bool AConcr, int ACreateType, int APerType, System::TDateTime APeriodDateFr, System::TDateTime APeriodDateTo, bool ANotSave, const String& ARequestFilter = String());
    System::UnicodeString __fastcall DocCreateProgress(System::UnicodeString  &ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall LoadDocPreviewProgress(System::UnicodeString  &AMsg, const String& ARequestFilter = String());
    TJSONObject* __fastcall GetQList(System::UnicodeString AId, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetQList_Cache(System::UnicodeString AId, const String& ARequestFilter = String());
    void __fastcall SetQList(System::UnicodeString AId, TJSONObject* AList);
    System::UnicodeString __fastcall GetFilterByGUI(System::UnicodeString AGUI, const String& ARequestFilter = String());
  };

  class TdsVacStoreClassClient : public TDSAdminRestClient
  {
  private:
    TDSRestCommand *FDisconnectCommand;
    TDSRestCommand *FGetDefXMLCommand;
    TDSRestCommand *FGetClassXMLCommand;
    TDSRestCommand *FGetClassZIPXMLCommand;
    TDSRestCommand *FGetCountCommand;
    TDSRestCommand *FGetIdListCommand;
    TDSRestCommand *FGetIdListCommand_Cache;
    TDSRestCommand *FGetValByIdCommand;
    TDSRestCommand *FGetValByIdCommand_Cache;
    TDSRestCommand *FGetValById10Command;
    TDSRestCommand *FGetValById10Command_Cache;
    TDSRestCommand *FFindCommand;
    TDSRestCommand *FFindCommand_Cache;
    TDSRestCommand *FInsertDataCommand;
    TDSRestCommand *FEditDataCommand;
    TDSRestCommand *FDeleteDataCommand;
    TDSRestCommand *FSetTextDataCommand;
    TDSRestCommand *FGetTextDataCommand;
    TDSRestCommand *FSetMainLPUCommand;
    TDSRestCommand *FMainOwnerCodeCommand;
    TDSRestCommand *FIsMainOwnerCodeCommand;
    TDSRestCommand *FGetMIBPFormCommand;
    TDSRestCommand *FGetMIBPListCommand;
    TDSRestCommand *FGetMIBPListCommand_Cache;
  public:
    __fastcall TdsVacStoreClassClient(TDSRestConnection *ARestConnection);
    __fastcall TdsVacStoreClassClient(TDSRestConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TdsVacStoreClassClient();
    void __fastcall Disconnect();
    System::UnicodeString __fastcall GetDefXML(const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetClassXML(System::UnicodeString ARef, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetClassZIPXML(System::UnicodeString ARef, const String& ARequestFilter = String());
    __int64 __fastcall GetCount(System::UnicodeString AId, System::UnicodeString AFilterParam, const String& ARequestFilter = String());
    TJSONObject* __fastcall GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetIdList_Cache(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter = String());
    TJSONObject* __fastcall GetValById(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetValById_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    TJSONObject* __fastcall GetValById10(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetValById10_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    TJSONObject* __fastcall Find(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall Find_Cache(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter = String());
    System::UnicodeString __fastcall InsertData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall EditData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall DeleteData(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall SetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString AVal, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString  &AMsg, const String& ARequestFilter = String());
    void __fastcall SetMainLPU(__int64 ALPUCode);
    System::UnicodeString __fastcall MainOwnerCode(const String& ARequestFilter = String());
    bool __fastcall IsMainOwnerCode(System::UnicodeString AOwnerCode, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetMIBPForm(const String& ARequestFilter = String());
    TJSONObject* __fastcall GetMIBPList(System::UnicodeString AId, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetMIBPList_Cache(System::UnicodeString AId, const String& ARequestFilter = String());
  };

  class TdsEIDataClassClient : public TDSAdminRestClient
  {
  private:
    TDSRestCommand *FImportDataCommand;
    TDSRestCommand *FUploadDataCommand;
    TDSRestCommand *FGetExportDataHeaderCommand;
    TDSRestCommand *FGetPatDataByIdCommand;
    TDSRestCommand *FGetPatCardDataByIdCommand;
  public:
    __fastcall TdsEIDataClassClient(TDSRestConnection *ARestConnection);
    __fastcall TdsEIDataClassClient(TDSRestConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TdsEIDataClassClient();
    System::UnicodeString __fastcall ImportData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, System::UnicodeString  &AResMessage, const String& ARequestFilter = String());
    bool __fastcall UploadData(System::UnicodeString ADataPart, int AType, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetExportDataHeader(const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetPatDataById(System::UnicodeString ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetPatCardDataById(System::UnicodeString ARecId, TJSONObject* AClsRC, const String& ARequestFilter = String());
  };

  class TdsAdminClassClient : public TDSAdminRestClient
  {
  private:
    TDSRestCommand *FMigrateScriptProgressCommand;
    TDSRestCommand *FDisconnectCommand;
    TDSRestCommand *FGetDefXMLCommand;
    TDSRestCommand *FGetClassXMLCommand;
    TDSRestCommand *FGetClassZIPXMLCommand;
    TDSRestCommand *FSetClassZIPXMLCommand;
    TDSRestCommand *FGetCountCommand;
    TDSRestCommand *FGetIdListCommand;
    TDSRestCommand *FGetIdListCommand_Cache;
    TDSRestCommand *FGetValByIdCommand;
    TDSRestCommand *FGetValByIdCommand_Cache;
    TDSRestCommand *FGetValById10Command;
    TDSRestCommand *FGetValById10Command_Cache;
    TDSRestCommand *FFindCommand;
    TDSRestCommand *FFindCommand_Cache;
    TDSRestCommand *FFindExCommand;
    TDSRestCommand *FFindExCommand_Cache;
    TDSRestCommand *FInsertDataCommand;
    TDSRestCommand *FEditDataCommand;
    TDSRestCommand *FDeleteDataCommand;
    TDSRestCommand *FUploadDataCommand;
    TDSRestCommand *FCheckUserCommand;
    TDSRestCommand *FLoginUserCommand;
    TDSRestCommand *FLogoutCommand;
    TDSRestCommand *FCheckRTCommand;
    TDSRestCommand *FGetUpdateCodeCommand;
    TDSRestCommand *FFindAddrCommand;
    TDSRestCommand *FFindAddrCommand_Cache;
    TDSRestCommand *FAddrCodeToTextCommand;
  public:
    __fastcall TdsAdminClassClient(TDSRestConnection *ARestConnection);
    __fastcall TdsAdminClassClient(TDSRestConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TdsAdminClassClient();
    void __fastcall MigrateScriptProgress(TJSONObject* Sender);
    void __fastcall Disconnect();
    System::UnicodeString __fastcall GetDefXML(const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetClassXML(System::UnicodeString ARef, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetClassZIPXML(System::UnicodeString ARef, const String& ARequestFilter = String());
    bool __fastcall SetClassZIPXML(System::UnicodeString ARef, const String& ARequestFilter = String());
    __int64 __fastcall GetCount(System::UnicodeString AId, System::UnicodeString AFilterParam, const String& ARequestFilter = String());
    TJSONObject* __fastcall GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetIdList_Cache(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam, const String& ARequestFilter = String());
    TJSONObject* __fastcall GetValById(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetValById_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    TJSONObject* __fastcall GetValById10(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall GetValById10_Cache(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    TJSONObject* __fastcall Find(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall Find_Cache(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter = String());
    TJSONObject* __fastcall FindEx(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall FindEx_Cache(System::UnicodeString AId, System::UnicodeString AFindData, const String& ARequestFilter = String());
    System::UnicodeString __fastcall InsertData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall EditData(System::UnicodeString AId, TJSONObject* AValue, System::UnicodeString  &ARecId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall DeleteData(System::UnicodeString AId, System::UnicodeString ARecId, const String& ARequestFilter = String());
    bool __fastcall UploadData(System::UnicodeString ADataPart, int AType, const String& ARequestFilter = String());
    bool __fastcall CheckUser(System::UnicodeString AUser, System::UnicodeString APasswd, bool  &AValid, const String& ARequestFilter = String());
    System::UnicodeString __fastcall LoginUser(System::UnicodeString AId, const String& ARequestFilter = String());
    void __fastcall Logout();
    bool __fastcall CheckRT(System::UnicodeString AURT, System::UnicodeString AFunc, const String& ARequestFilter = String());
    System::UnicodeString __fastcall GetUpdateCode(const String& ARequestFilter = String());
    TJSONObject* __fastcall FindAddr(System::UnicodeString AStr, System::UnicodeString ADefAddr, int AParams, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall FindAddr_Cache(System::UnicodeString AStr, System::UnicodeString ADefAddr, int AParams, const String& ARequestFilter = String());
    System::UnicodeString __fastcall AddrCodeToText(System::UnicodeString ACode, int AParams, const String& ARequestFilter = String());
  };

  class TMISAPIClient : public TDSAdminRestClient
  {
  private:
    TDSRestCommand *FPatientF63Command;
    TDSRestCommand *FPatientF63Command_Cache;
    TDSRestCommand *FImportPatientCommand;
  public:
    __fastcall TMISAPIClient(TDSRestConnection *ARestConnection);
    __fastcall TMISAPIClient(TDSRestConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TMISAPIClient();
    TJSONObject* __fastcall PatientF63(System::UnicodeString APtId, const String& ARequestFilter = String());
    _di_IDSRestCachedJSONObject __fastcall PatientF63_Cache(System::UnicodeString APtId, const String& ARequestFilter = String());
    System::UnicodeString __fastcall ImportPatient(System::UnicodeString AMIS, TJSONObject* ASrc, const String& ARequestFilter = String());
  };

#endif

