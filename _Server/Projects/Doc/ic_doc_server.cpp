﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include "icApp.h"
#include "icSvc.h"
//---------------------------------------------------------------------------
USEFORM("..\..\src\KLADR\dsKLADRUnit.cpp", dsKLADRClass); /* TDataModule: File Type */
USEFORM("..\..\src\KLADR\dsKLADRDMUnit.cpp", dsKLADRDM); /* TDataModule: File Type */
USEFORM("..\..\src\Org\dsOrgUnit.cpp", dsOrgClass); /* TDataModule: File Type */
USEFORM("..\..\src\Org\dsOrgDMUnit.cpp", dsOrgDM); /* TDataModule: File Type */
USEFORM("..\..\src\dsSearch\dsSearchDMUnit.cpp", dsSearchDM); /* TDataModule: File Type */
USEFORM("..\..\srvc\doc\appbaseunit.cpp", AppBase); /* TDataModule: File Type */
USEFORM("..\..\srvc\doc\MainApp.cpp", MainForm);
USEFORM("..\..\srvc\doc\MainSrvc.cpp", ICDocServer); /* TService: File Type */
USEFORM("..\..\src\RegRest\dsRegDMUnit.cpp", dsRegDM); /* TDataModule: File Type */
USEFORM("..\..\src\Admin\dsAdminDMUnit.cpp", dsAdminDM); /* TDataModule: File Type */
USEFORM("..\..\src\Doc\dsDocDMUnit.cpp", dsDocDM); /* TDataModule: File Type */
USEFORM("..\..\src\Common\TransportSupport.cpp", DataModule1); /* TDataModule: File Type */
USEFORM("..\..\src\Common\WMDef.cpp", WMDefDM); /* TWebModule: File Type */
USEFORM("..\..\src\Doc\dsDocUnit.cpp", dsDocClass); /* TDataModule: File Type */
USEFORM("..\..\src\Admin\dsAdminUnit.cpp", dsAdminClass); /* TDataModule: File Type */
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
  String mode = "svc";
  if (ParamCount() == 1)
   {
     String option = ParamStr(1).LowerCase();
     if (option == "app")
      {
        mode = "app";
      }
   }
  if (mode == "svc")
   {
     return SvcWinMain();
   }
  else
   {
     return AppWinMain();
   }
}
//---------------------------------------------------------------------------


