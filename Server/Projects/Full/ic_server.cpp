﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include "icApp.h"
#include "icSvc.h"
//---------------------------------------------------------------------------
USEFORM("..\..\src\KLADR\dsKLADRDMUnit.cpp", dsKLADRDM); /* TDataModule: File Type */
USEFORM("..\..\src\KLADR\dsKLADRUnit.cpp", dsKLADRClass); /* TDataModule: File Type */
USEFORM("..\..\src\ic\dsICDMUnit.cpp", dsICDM); /* TDataModule: File Type */
USEFORM("..\..\src\ic\dsICUnit.cpp", dsICClass); /* TDataModule: File Type */
USEFORM("..\..\src\Plan\dsSrvPlanDMUnit.cpp", dsSrvPlanDM); /* TDataModule: File Type */
USEFORM("..\..\src\Org\dsOrgUnit.cpp", dsOrgClass); /* TDataModule: File Type */
USEFORM("..\..\src\Org\dsOrgDMUnit.cpp", dsOrgDM); /* TDataModule: File Type */
USEFORM("..\..\src\ExpImp\dsEIDataUnit.cpp", dsEIDataClass); /* TDataModule: File Type */
USEFORM("..\..\src\Doc\dsDocUnit.cpp", dsDocClass); /* TDataModule: File Type */
USEFORM("..\..\src\dsSearch\dsSearchDMUnit.cpp", dsSearchDM); /* TDataModule: File Type */
USEFORM("..\..\src\ExpImp\dsEIDataDMUnit.cpp", dsEIDataDM); /* TDataModule: File Type */
USEFORM("..\..\src\VacStore\dsVacStoreUnit.cpp", dsVacStoreClass); /* TDataModule: File Type */
USEFORM("..\..\srvc\full\appbaseunit.cpp", AppBase); /* TDataModule: File Type */
USEFORM("..\..\src\VacStore\dsVacStoreDMUnit.cpp", dsVacStoreDM); /* TDataModule: File Type */
USEFORM("..\..\srvc\full\MainSrvc.cpp", ICBaseServer); /* TService: File Type */
USEFORM("..\..\srvc\full\MainApp.cpp", MainForm);
USEFORM("..\..\src\RegRest\dsRegDMUnit.cpp", dsRegDM); /* TDataModule: File Type */
USEFORM("..\..\src\Plan\PrePlanerDMUnit.cpp", PrePlanerDM); /* TDataModule: File Type */
USEFORM("..\..\src\Plan\PlanLinkerDMUnit.cpp", PlanLinkerDM); /* TDataModule: File Type */
USEFORM("..\..\src\Admin\dsAdminDMUnit.cpp", dsAdminDM); /* TDataModule: File Type */
USEFORM("..\..\src\Admin\dsAdminUnit.cpp", dsAdminClass); /* TDataModule: File Type */
USEFORM("..\..\src\API\MIS\dsMISAPIUnit.cpp", MISAPI); /* TDataModule: File Type */
USEFORM("..\..\src\API\MIS\dsMISAPIDMUnit.cpp", dsMISAPIDM); /* TDataModule: File Type */
USEFORM("..\..\src\Admin\dsDBCheckUnit.cpp", dsDBCheckClass); /* TDataModule: File Type */
USEFORM("..\..\src\API\MIS\dsMISManagerDMUnit.cpp", dsMISManagerDM); /* TDataModule: File Type */
USEFORM("..\..\src\Common\WMDef.cpp", WMDefDM); /* TWebModule: File Type */
USEFORM("..\..\src\Common\TransportSupport.cpp", DataModule1); /* TDataModule: File Type */
USEFORM("..\..\src\DBSupport\dsDBSupportDMUnit.cpp", dsDBSupportDM); /* TDataModule: File Type */
USEFORM("..\..\src\Doc\dsDocDMUnit.cpp", dsDocDM); /* TDataModule: File Type */
USEFORM("..\..\src\API\MIS\netrika\netrikaDMUnit.cpp", NetrikaDM); /* TDataModule: File Type */
USEFORM("..\..\src\API\MIS\dsMISSyncDMUnit.cpp", dsMISSyncDM); /* TDataModule: File Type */
USEFORM("..\..\src\Common\DataMF.cpp", DataM); /* TDataModule: File Type */
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
  String mode = "svc";
  if (ParamCount() == 1)
   {
     String option = ParamStr(1).LowerCase();
     if (option == "app")
      {
        mode = "app";
      }
   }
  if (mode == "svc")
   {
     return SvcWinMain();
   }
  else
   {
     return AppWinMain();
   }
}
//---------------------------------------------------------------------------


