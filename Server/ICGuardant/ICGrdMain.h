//---------------------------------------------------------------------------
#ifndef ICGrdMainH
#define ICGrdMainH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <SvcMgr.hpp>
#include <vcl.h>
#include "AppOptBaseProv.h"
#include "AppOptions.h"
#include "AppOptXMLProv.h"
#include <Datasnap.DSClientRest.hpp>
#include <IPPeerClient.hpp>
//---------------------------------------------------------------------------
#include "ClientClassesUnit.h"
//---------------------------------------------------------------------------
class TICGuardant : public TService
{
__published:    // IDE-managed Components
 TAppOptions *SrvOpt;
 TAppOptXMLProv *SrvOptXMLData;
 TDSRestConnection *SrvConnect;
 void __fastcall ServiceAfterInstall(TService *Sender);
 void __fastcall FServiceStart(TService *Sender, bool &Started);
 void __fastcall ServiceExecute(TService *Sender);
 void __fastcall FServiceStop(TService *Sender, bool &Stopped);
 void __fastcall SrvOptLoadXML(TTagNode *AOptNode);
private:        // User declarations
  int FRestartAttampt, FChechAttampt;
    TDSAdminRestClient * CheckClient;
  bool FNightRestart;
  bool FCheckProcessed;
  bool __fastcall CheckSrvc();
  void __fastcall SrvcRestart();
  void __fastcall SrvcStart();
  void __fastcall FServiceExecute();
  bool __fastcall LoadOpt();
  bool __fastcall KillService(UnicodeString ASrvcName);
  TDateTime CurDate;
public:         // User declarations
	__fastcall TICGuardant(TComponent* Owner);
	TServiceController __fastcall GetServiceController(void);

	friend void __stdcall ServiceController(unsigned CtrlCode);
};
//---------------------------------------------------------------------------
extern PACKAGE TICGuardant *ICGuardant;
//---------------------------------------------------------------------------
#endif
