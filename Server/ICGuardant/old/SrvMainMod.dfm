object SrvMain: TSrvMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 609
  Top = 220
  Height = 116
  Width = 123
  object cmdServer: TServerSocket
    Active = False
    Port = 0
    ServerType = stNonBlocking
    OnClientConnect = cmdServerClientConnect
    OnClientDisconnect = cmdServerClientDisconnect
    OnClientRead = cmdServerClientRead
    OnClientWrite = cmdServerClientWrite
    OnClientError = cmdServerClientError
    Left = 26
    Top = 13
  end
end
