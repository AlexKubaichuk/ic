//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SyncClientMod.h"
#include "srvsetting.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

static const AnsiString ConvSrvcName = "ICSyncSrv";

TSyncClient *SyncClient;
//---------------------------------------------------------------------------
__fastcall TSyncClient::TSyncClient(TComponent* Owner, TConvertVPOptions* AAppOptions)
        : TDataModule(Owner)
{
  FAppOptions = AAppOptions;
  cmdSyncClient->Host = "localhost";
  if (FAppOptions->EIHost.Length())
   cmdSyncClient->Host = FAppOptions->EIHost;
  cmdSyncClient->Port = FAppOptions->EIPort;
  FCurConvStateStr = "";
  FInCmd = false;
}
//---------------------------------------------------------------------------
void __fastcall TSyncClient::cmdSyncClientRead(TObject *Sender,
      TCustomWinSocket *Socket)
{
  FCmdRet = Socket->ReceiveText();
}
//---------------------------------------------------------------------------
void __fastcall TSyncClient::cmdSyncClientError(TObject *Sender,
      TCustomWinSocket *Socket, TErrorEvent ErrorEvent, int &ErrorCode)
{
  CmdRetTimer->Enabled = false;
  FCmdRet = "Error";
  LogMessage("������ ��������� ������ �������, ��� = "+IntToStr(ErrorCode), EVENTLOG_ERROR_TYPE);
  ErrorCode = 0;
}
//---------------------------------------------------------------------------
void __fastcall TSyncClient::CmdRetTimerTimer(TObject *Sender)
{
  FInCmd = false;
  FCmdRet = "Error";
  CmdRetTimer->Enabled = false;
  LogMessage("������ ��������� ������ �������, �������� �������� ��������.", EVENTLOG_ERROR_TYPE);
}
//---------------------------------------------------------------------------
bool __fastcall TSyncClient::cmdSyncServerConnect()
{
  bool RC = cmdSyncClient->Active;
  try
   {
     if (!RC)
      {
        cmdSyncClient->Active = true;
        for (int i = 0; (i < 1000) && !cmdSyncClient->Active; i++)
         {
           Sleep(10);
           FProcessRequests();
         }
        RC = cmdSyncClient->Active;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSyncClient::SendSyncCommand(AnsiString ACmd)
{
  AnsiString RC = "";
  FInCmd = true;
  try
   {
     if (cmdSyncServerConnect())
      {
        FCmdRet = "";
        _DEBUG_LOG_("SendSyncCommand. Connected. Command: "+ACmd);
        cmdSyncClient->Socket->SendText(ACmd);
        CmdRetTimer->Enabled = true;
        while (!FCmdRet.Length())
         {
           Sleep(10);
           FProcessRequests();
         }
        RC = FCmdRet;
        CmdRetTimer->Enabled = false;
      }
     else
      {
        CmdRetTimer->Enabled = false;
        RC = "error command";
        LogMessage("�� ������� ���������� ���������� � �������� �������������.", EVENTLOG_ERROR_TYPE);
      }
   }
  __finally
   {
     FInCmd = false;
     CmdRetTimer->Enabled = false;
     FProcessRequests();
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TSyncClient::SendStartCommand(const AnsiString &AParams)
{
  bool RC = false;
  TStringList *FStartParams = new TStringList;
  try
   {
     FStartParams->Text = AParams.Trim();

     if (SendSyncCommand("start").LowerCase() != "error command")
      {
        for (int i = 0; i < FStartParams->Count; i++)
         SendSyncCommand(FStartParams->Strings[i]);
        SendSyncCommand("params_end");
        RC = true;
      }
   }
  __finally
   {
     delete FStartParams;
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TSyncClient::CheckSyncProgress()
{
  bool RC = false;
  try
   {
     while (IsConvProgress())
      {
        AnsiString Ret = SendSyncCommand("get progress");
        if (Ret.LowerCase() == "ok")
         {
           Ret = SendSyncCommand("get last state");
         }
        else if (Ret.Length())
         {
           if (FOnProgress)
            FOnProgress(Ret, 0);
         }
        LogMsg("CheckSyncProgress.  "+Ret);
      }
     RC = true;
   }
  __finally
   {
   }
  return RC;    
}
//---------------------------------------------------------------------------
bool __fastcall TSyncClient::IsConvProgress()
{
  bool RC = false;
  try
   {
     if (FInCmd)
      RC = true;
     else
      RC = (SendSyncCommand("get state").LowerCase() == "progress");
   }
  __finally
  {
  }
 return RC;
}
//---------------------------------------------------------------------------
void __fastcall TSyncClient::ClsDownloadStart(const AnsiString &AFN)
{
  FCurConvStateStr = "������ ������������";
  LogMessage(FCurConvStateStr.c_str(), EVENTLOG_INFORMATION_TYPE);
  try
   {
     _DEBUG_LOG_("ClsDownloadStart.");
     AnsiString ImportFN = AFN;
     _DEBUG_LOG_("SendStartCommand(\"start /i /c /culd /fn \""+ImportFN+"\" ok\")");
     if (SendStartCommand("/i\n/c\n/culd\n/fn\n\""+ImportFN+"\""))
      CheckSyncProgress();
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSyncClient::UnitCheck(const AnsiString &AFN)
{
  AnsiString RC = "";
  return RC;
}
//---------------------------------------------------------------------------
AnsiString __fastcall TSyncClient::UnitDownloadStart(const AnsiString &AFN)
{
  AnsiString RC = "";
  FCurConvStateStr = "������ ������ ���������";
  LogMessage(FCurConvStateStr.c_str(), EVENTLOG_INFORMATION_TYPE);
  try
   {
     _DEBUG_LOG_("UnitDownloadStart.");
     AnsiString ImportFN = AFN;
     _DEBUG_LOG_("SendStartCommand(\"start /i /c /u /k /culd /uuld /fn \""+ImportFN+"\" ok\")");
     if (SendStartCommand("/i\n/c\n/u\n/k\n/culd\n/uuld\n/fn\n\""+ImportFN+"\""))
      CheckSyncProgress();
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TSyncClient::UnitUploadStart(const AnsiString &AUCode, const AnsiString &AFN)
{
  bool RC = false;
  FCurConvStateStr = "������� ������ ��������";
  LogMessage(FCurConvStateStr.c_str(), EVENTLOG_INFORMATION_TYPE);
  try
   {
     AnsiString ExportFN = AFN;
     _DEBUG_LOG_("SendStartCommand(\"start /e /c /u /k /fn \""+ExportFN+"\" ok\")");
     if (SendStartCommand("/e\n/u\n/k\n/fn\n\""+ExportFN+"\"\n/ulbegin\n"+AUCode+"\n/ulend"))
      CheckSyncProgress();
     RC = true;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TSyncClient::UnitChangeGUINStart(const AnsiString &AClId, const AnsiString &AOldGUIN, const AnsiString &ANewGUIN)
{
  FCurConvStateStr = "������ �������� �������������� ��������/�����������";
  LogMessage(FCurConvStateStr.c_str(), EVENTLOG_INFORMATION_TYPE);
  try
   {
     if (AClId.Length())
      { // ������ �������� �������������� �����������
        _DEBUG_LOG_("SendSyncCommand(\"replace cguid "+AClId+" "+AOldGUIN+" "+ANewGUIN+")");
        if (SendSyncCommand("replace cguid").LowerCase() != "error command")
         {
           SendSyncCommand(AClId);
           SendSyncCommand(AOldGUIN);
           SendSyncCommand(ANewGUIN);
           if (SendSyncCommand("params_end").LowerCase() != "error command")
            CheckSyncProgress();
         }
      }
     else
      { //������ �������� �������������� ��������
        _DEBUG_LOG_("SendSyncCommand(\"replace uguid "+AOldGUIN+" "+ANewGUIN+")");
        if (SendSyncCommand("replace uguid").LowerCase() != "error command")
         {
           SendSyncCommand(AOldGUIN);
           SendSyncCommand(ANewGUIN);
           if (SendSyncCommand("params_end").LowerCase() != "error command")
            CheckSyncProgress();
         }
      }
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall TSyncClient::LogMessage(const AnsiString &AValue, unsigned EventType)
{
  if (FLogger)
   FLogger(AValue, EventType);
}
//---------------------------------------------------------------------------
void __fastcall TSyncClient::LogMsg(const AnsiString &AValue)
{
  if (FLogger)
   FLogger(AValue, -10);
}
//---------------------------------------------------------------------------
void __fastcall TSyncClient::_DEBUG_LOG_(const AnsiString &AValue)
{
  if (FLogger)
   FLogger(AValue, -5);
}
//---------------------------------------------------------------------------
void __fastcall TSyncClient::FProcessRequests()
{
  if (FProcessRequestsEvt)
   FProcessRequestsEvt(this);
}
//---------------------------------------------------------------------------
bool __fastcall TSyncClient::FGetProgress()
{
  bool RC = false;
  try
   {
     if (FGetProgressEvent)
       RC = FGetProgressEvent();
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

