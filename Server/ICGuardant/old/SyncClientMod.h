//---------------------------------------------------------------------------

#ifndef SyncClientModH
#define SyncClientModH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "Globals.h"
#include <ExtCtrls.hpp>
#include <ScktComp.hpp>
//---------------------------------------------------------------------------
class PACKAGE TSyncClient : public TDataModule
{
__published:	// IDE-managed Components
        TTimer *CmdRetTimer;
        TClientSocket *cmdSyncClient;
        void __fastcall cmdSyncClientRead(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall cmdSyncClientError(TObject *Sender,
          TCustomWinSocket *Socket, TErrorEvent ErrorEvent,
          int &ErrorCode);
        void __fastcall CmdRetTimerTimer(TObject *Sender);
private:	// User declarations
        AnsiString FCmdRet;
        AnsiString FCurConvStateStr;
        TConvertVPOptions* FAppOptions;
        bool FInCmd;
        TServiceLogEvent FLogger;
        TNotifyEvent FProcessRequestsEvt;
        TServiceLogEvent FOnProgress;
        TServiceGetProgressEvent FGetProgressEvent;

        void       __fastcall LogMessage(const AnsiString &AValue, unsigned EventType = 1);
        void       __fastcall LogMsg(const AnsiString &AValue);
        void       __fastcall _DEBUG_LOG_(const AnsiString &AValue);
        bool       __fastcall cmdSyncServerConnect();
        AnsiString __fastcall SendSyncCommand(AnsiString ACmd);
        bool       __fastcall SendStartCommand(const AnsiString &AParams);
        bool       __fastcall CheckSyncProgress();
        bool       __fastcall IsConvProgress();
        void       __fastcall FProcessRequests();
        bool       __fastcall FGetProgress();

public:		// User declarations
        __fastcall TSyncClient(TComponent* Owner, TConvertVPOptions* AAppOptions);

        void       __fastcall ClsDownloadStart(const AnsiString &AFN);
        AnsiString __fastcall UnitDownloadStart(const AnsiString &AFN);
        AnsiString __fastcall UnitCheck(const AnsiString &AFN);
        void       __fastcall UnitChangeGUINStart(const AnsiString &AClId, const AnsiString &AOldGUIN, const AnsiString &ANewGUIN);
        bool       __fastcall UnitUploadStart(const AnsiString &AUCode, const AnsiString &AFN);

        __property TServiceLogEvent OnLogMessage = {read=FLogger, write=FLogger};
        __property TServiceLogEvent OnProgress   = {read=FOnProgress, write=FOnProgress};
        __property TNotifyEvent OnProcessRequestsRequired = {read=FProcessRequestsEvt, write=FProcessRequestsEvt};
        __property AnsiString CurConvStateStr = {read=FCurConvStateStr, write=FCurConvStateStr};
        __property TServiceGetProgressEvent OnGetProgress = {read=FGetProgressEvent, write=FGetProgressEvent};
};
//---------------------------------------------------------------------------
extern PACKAGE TSyncClient *SyncClient;
//---------------------------------------------------------------------------
#endif
