object MainFm: TMainFm
  Left = 795
  Top = 321
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 
    #1055#1072#1085#1077#1083#1100' '#1091#1087#1088#1072#1074#1083#1077#1085#1080#1103' "'#1052#1077#1085#1077#1076#1078#1077#1088' '#1089#1080#1085#1093#1088#1086#1085#1080#1079#1072#1094#1080#1080' '#1041#1044' '#1055#1050' "'#1055#1086#1083#1080#1082#1083#1080#1085#1080#1082#1072'" ->' +
    ' '#1055#1050' "'#1059#1048'" v 6.8"'
  ClientHeight = 375
  ClientWidth = 647
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object ClientPn: TPanel
    Left = 0
    Top = 0
    Width = 647
    Height = 375
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Bevel1: TBevel
      Left = 0
      Top = 74
      Width = 647
      Height = 8
      Align = alTop
      Shape = bsTopLine
    end
    object Bevel2: TBevel
      Left = 0
      Top = 367
      Width = 647
      Height = 8
      Align = alBottom
      Shape = bsBottomLine
    end
    object DBFDBConnectStrLb: TLabel
      Left = 11
      Top = 96
      Width = 157
      Height = 13
      Caption = #1055#1091#1090#1100' '#1082' '#1080#1089#1093#1086#1076#1085#1086#1081' '#1073#1072#1079#1077' '#1076#1072#1085#1085#1099#1093':'
    end
    object FBDBConnectStrLb: TLabel
      Left = 11
      Top = 143
      Width = 139
      Height = 13
      Caption = #1055#1091#1090#1100' '#1082' '#1073#1072#1079#1077' '#1076#1072#1085#1085#1099#1093' '#1055#1050' '#1059#1048' '
    end
    object FooterPn: TPanel
      Left = 381
      Top = 82
      Width = 266
      Height = 285
      Align = alRight
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 1
      object CloseBtn: TButton
        Left = 158
        Top = 238
        Width = 99
        Height = 25
        Caption = #1047#1072#1082#1088#1099#1090#1100
        TabOrder = 0
        OnClick = CloseBtnClick
      end
      object OptionsBtn: TButton
        Left = 10
        Top = 208
        Width = 99
        Height = 25
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' ...'
        TabOrder = 1
        OnClick = OptionsBtnClick
      end
      object AboutBtn: TButton
        Left = 160
        Top = 208
        Width = 97
        Height = 25
        Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077'...'
        TabOrder = 2
        OnClick = AboutBtnClick
      end
      object cxGroupBox1: TcxGroupBox
        Left = 5
        Top = 8
        Caption = #1057#1077#1088#1074#1080#1089' '#1089#1080#1085#1093#1088#1086#1085#1080#1079#1072#1094#1080#1080' :'
        TabOrder = 3
        Height = 56
        Width = 252
        object SrvcRestartBtn: TButton
          Left = 22
          Top = 17
          Width = 97
          Height = 25
          Caption = #1055#1077#1088#1077#1079#1072#1087#1091#1089#1090#1080#1090#1100
          TabOrder = 0
          OnClick = SrvcRestartBtnClick
        end
        object SrvcStopBtn: TButton
          Left = 141
          Top = 17
          Width = 99
          Height = 25
          Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100
          TabOrder = 1
          OnClick = SrvcStopBtnClick
        end
      end
      object cxGroupBox2: TcxGroupBox
        Left = 5
        Top = 67
        Caption = #1057#1080#1085#1093#1088#1086#1085#1080#1079#1072#1094#1080#1103':'
        TabOrder = 4
        Height = 135
        Width = 252
        object FullSyncBtn: TButton
          Left = 29
          Top = 18
          Width = 193
          Height = 25
          Caption = #1055#1086#1083#1085#1072#1103' '#1089#1080#1085#1093#1088#1086#1085#1080#1079#1072#1094#1080#1103
          TabOrder = 0
          OnClick = FullSyncBtnClick
        end
        object ClsSyncBtn: TButton
          Left = 29
          Top = 45
          Width = 193
          Height = 25
          Caption = #1057#1080#1085#1093#1088#1086#1085#1080#1079#1072#1094#1080#1103' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1086#1074
          TabOrder = 1
          OnClick = ClsSyncBtnClick
        end
        object UnitSyncBtn: TButton
          Left = 29
          Top = 73
          Width = 193
          Height = 25
          Caption = #1057#1080#1085#1093#1088#1086#1085#1080#1079#1072#1094#1080#1103' '#1089#1087#1080#1089#1082#1072' '#1087#1072#1094#1080#1077#1085#1090#1086#1074
          TabOrder = 2
          OnClick = UnitSyncBtnClick
        end
        object StopBtn: TButton
          Left = 29
          Top = 102
          Width = 193
          Height = 25
          Caption = #1055#1088#1077#1088#1074#1072#1090#1100
          TabOrder = 3
          OnClick = StopBtnClick
        end
      end
      object Button1: TButton
        Left = 27
        Top = 242
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 5
        Visible = False
        OnClick = Button1Click
      end
    end
    object HeaderPn: TPanel
      Left = 0
      Top = 0
      Width = 647
      Height = 74
      Align = alTop
      BevelOuter = bvNone
      Color = clWindow
      TabOrder = 0
      object Image: TImage
        Left = 531
        Top = 8
        Width = 55
        Height = 55
        AutoSize = True
        Picture.Data = {
          07544269746D61703A100000424D3A1000000000000032040000280000003700
          0000370000000100080000000000080C0000120B0000120B0000FF0000000000
          0000171716001818170018181800191919001B1B1A001A1A19001C1C1B001C1C
          1C001F1F1E0007073B0016163A00232323002020200026262600242424002929
          29002E2E2E002E2E2D002E2D2A0029292700302F2A0032312C00343433003836
          34003D3A3B000000440001014A000202530000005D000A0A56001A1A59000F0F
          45000101640000006D0008086300000075000202710000007C000C0C7A001515
          6A0018177500312E510038345900222154002323630028266A0023227B002826
          76003834680038317300423D3400463E36003D354300443F73004A453800594C
          2F00544837005A513A004D402B00615236006D5C3A0065583900755C38006C61
          3600786B3A00544A57005B5149005C595D00464158004B446700544B6800554C
          7A006B5E4100665B4900675A5800705E4F006B5F6B006D6242007B6B43007467
          4700776654007F714200646464007A6B6600746978000000840000008D000D0D
          85000101940002029A0017178700151598000000AC000202A4000303B4001717
          A80034338400232192003533A5002F2CB5000000C3000000CA000202D5000202
          DD001616C8000000E5000707E6000000F3001616E5002D2AD0005D548D004742
          A100524BB800665D8D00655BB4007C6D89007B6D97006C6386009A3500009B36
          00009E3A00009F3C0000866D3E0088793F009C732D0095793900916C3100A241
          0000A4430000A7480000A94B0000AA4D0000AD510000AF540000B2590000B35B
          0000B7600000B7610000BB670000A67D3300A47A3700A8752B00846D48008C7C
          44008A734B008A775800937B4C009078410086756900917D6000C06D0000C06F
          0000C3720000C6760000C8790000C97B0300CA7C0000C77F1500897882008C7B
          980097823A00B9860D00A7892900AD843B00A5853B00B58B2900B58D3400BD94
          2700B8953800AB9023009A8B4E009A88550096886A0099847F00A88A4B00A18A
          5400AE955A00B0884100BD984A00B59B5900AC914400B89D6300A7916F00BCA0
          5400CC800000CF840000C7891500D0850000D0860400D48C0000D58D0300D393
          0D00D8920000D9930200DC980000DD9A0000DC990800D3941400D79A1500CB9B
          1300C7892600CA962500C4993800D1962300DFA41700C9A12A00CBA83800D3AA
          2C00DCAD2200D6A72900D5AB3400DCB32C00D7B43A00E09E0000E2A10000E5A5
          0000E6A70000E7A80400E8AA0000E6A80B00E3A81300EDB10000ECAF0000EBAF
          0700EDB61100E9B81B00F1B80000F5BD0000F6BE0200F3BA0500F2BB0C00E5B9
          2600E3BC3800DEB12200CDAC4400BFA04400D8B64700CFAE4A00F9C40000FAC5
          0000FBC60300FECB0000FECA0000FFCC0500FFCC0300FFCD0900F6C10F00F2C4
          1C00FCCD1300FACD1A00F5C71900ECC42A00EBC43800F4CA2600E9C545007676
          7676767676767676767676767676767676767676767676767676767676767676
          7676767676767676767676767676767676767676760078787878787878787878
          7878787878787878787878787878787878787878787878787878787878787878
          78787878787878787878787878007F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
          7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
          7F7F7F7F7F008181818181818181818181818181818181818181818181818181
          8181818181818181818181818181818181818181818181818181818181008383
          8383838383838383838383838383838383838383838383838383838383838383
          8383838383838383838383838383838383838383830085858585858585858585
          8585858585858585858585858585858585858585858585858585858585858585
          8585858585858585858585858500878787878787878787878787878787878787
          8787878787878787878787878787878787878787878787878787878787878787
          8787878787008888888888888888888888888888888888888888888888888888
          8888888888888888888888888888888888888888888888888888888888008A8A
          8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A
          8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A0096969696969696969696
          9696969696969696969696969696969696969696969696969696969696969696
          9696969696969696969696969600989898989898989898989898989898989898
          9898989898989898989898989898989898989898989898989898989898989898
          9898989898009A9A9A9A9A9A9A9A9A9A9A9A9A9A9BC8AE8E5218101011111010
          101110101011111010101018528EAEC89B9A9A9A9A9A9A9A9A9A9A9A9A00B8B8
          B8B8B8B8B8B8B8B8B8B8C87C3C320F0C06040405040404040404040405040404
          04040404060C0F323C8CBAB8B8B8B8B8B8B8B8B8B800BBBBBBBBBBBBBBBBBBBB
          C890180C020003070B0D0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0D0B070200
          020C3292CBBBBBBBBBBBBBBBBB00BDBDBDBDBDBDBDBDBFA4320700020D323C7D
          A3B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B2B28B7E48320E030007338BBEBD
          BDBDBDBDBD00C0C0C0C0C0C0C0C54F0B0203104FA6C6C0C0C0C0C0C0C0C0C0C0
          C0C0C0C0C0C0C0C0C0C0C0C0C0C0C1C6A5481103020E4EC6C0C0C0C0C000C2C2
          C2C2C2C2D13703020B48CAC4C2C2C2C2C2C2C2C2C2C2C2C2C2C2C2C2C2C2C2C2
          C2C2C2C2C2C2C2C2C2C4CA3D0C01063BBFC2C2C2C200D5D5D5D5D5D142030312
          A4DCD5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5D5
          D5D5D5DCA010020437C6D5D5D500D7D7D7D7DC3D030317A7D7D7D7D7D7D7D7D7
          D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7A81402
          044DDCD7D700DADADADA8F080315CDDADADADADADADADADADADADADADADADADA
          DADADADADADADADADADADADADADADADADADADADADADAA710020CA0DADA00DDDD
          DDCD11020DA8DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDE1CEA5CEE1DDDDDDDDDDDD
          DDDDE0D3A7A5CFE0DDDDDDDDDDDDDDB40C0215CFDD00E2E2E648020551E13D3A
          3A3838383342E9E2E2E2E2E78E2A1D1B1D2C4BD0E2E2CF3CD2AB442B1E1E2C2A
          95E7E2E2E2E2E2E54F050540E500E3E3CF0F0017E7E4CEA42B191A1A41EBE0E3
          E3E3CF2A1A1B274CAC91302CB3E4E90A1F191D92D3DCB2301B30EDE3E3E3E3E3
          D3120112D300EEEF7B03047BEEEEEEEEAF1C2022B7EEEEEEEEF72A1A1C28EBF0
          EEEEFA952FB2F8291927EBEEEEEEEEFB2F1C2DE7EEEEEEEEEE400507A000F2F7
          360013E9F2F2F2F2D426252595F2F2F2F2AF1B1C2494F2F2F2F2F2F3B62EFC43
          1B53F3F2F2F2F2F275232053F3F2F2F2F2D20D013800F1CE130036F8F1F1F1F1
          FD6158586EF1F1F1F5451C2157EAF1F1F1F1F1F1F353B09227D4F1F1F1F1F1F1
          7155232FF9F1F1F1F1F933001500F1AC0C0540F1F1F1F1F1F86F5C5C63F9F1F1
          FD2D20255BFCF1F1F1F1F1F1F1F5F3B060F8F1F1F1F1F1EA5F585528E7F1F1F1
          F1F33F030B00F1430605AAF1F1F1F1F1F174646568E8F1F1FB2721555FFCF1F1
          F1F1F1F1F1F1F1F7ECF1F1F1F3E89F685E5D565AFBF1F1F1F1F18F050800F116
          0608A9F1F1F1F1F1F1B6666967B5F1F1FB2823565FFEF1F1F1F1F1F1F1F1F1F1
          F1F1F3ED726C6966645C5847F3F1F1F1F1F1AF040600F1170608A9F1F1F1F1F1
          F1EC6A6B699FF1F1F92D23565FD4F1F1F1F1F1F1F1F1F1F1F1F59E68676B6B67
          655C62E8F1F1F1F1F1F1AF040600F1430604AAF1F1F1F1F1F1FD6D696770F5F1
          F34623565DB5F1F1F1F1F1F1F1F1F1F1F1B75E64666969666873FCF1F1F1F1F1
          F1F18F050800F1AB0C0540F1F1F1F1F1F1F570656563F9F1F1AF2455599EF1F1
          F1F1F1F1F1F3D4FAF46E5D5E65666DADFCF1F1F8F5F1F1F1F1F33F030E00F1EA
          120036F9F1F1F1F1F1F1735E5C5FFCF1F1FB2E25566FF9F1F1F1F1F1F1F831EB
          F160595C5E74FBF1F1F1F354EBF1F1F1F1FA32001500F1FD36000FD4F1F1F1F1
          F1F1B0595958B3F1F1F19424255BECF1F1F1F1F1F1FB2792F147555862F8F1F1
          F1F1F92E91F1F1F1F1CF0E013900EFEF8F05047BEFEFEFEFEFEFD457252394EF
          EFEFF735212571F0EFEFEFEFEFB01B4AEF95235547EFEFEFEFEFEA2243F0EFEF
          EF4E0307A000E3E3CF130216E9E3E3E3E3E3F72E201C46E4E3E3E3D0302126B5
          E3E3E3E3E130192AE4E12F212EE7E3E3E3F6461929F7E3E3D4140211D300E2E2
          E53F030540E5E2E2E5CE501D1A191F90CDE0E2E2D0462227ABE1E6CF4119090A
          D3E2CE2D1C35EAE8D24A1A090AD0E2E64D03054EE500DEDEDECF16020BB4DEDE
          DF5034343434181736EBDEDEDEE0AE452C2C2A1E2B428B38C7DEDED2502C1E2C
          2C2B49A639C7DDA40C0216D2DE00D8D8D8D9A0080216A7D8D8D9D9D9D9D9D9D9
          D9D9D8D8D8D8D8DBD1A1A1BAD0D9D8D9D8D8D8D8D8D0BAA1BAD0D8D8D9D8B211
          0208A4D8D800D6D6D6D6DC48040214A8D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6
          D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6D6B41102074FDBD6D600C3C3
          C3C3C3C6370402107DCCC3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3
          C3C3C3C3C3C3C3C3C3C3C3CC930F02043DC6C3C3C300C0C0C0C0C0C0BF3B0701
          0C3BA6C4C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
          C0C4A53908000848C5C0C0C0C000BDBDBDBDBDBDBDC54E0D02030F3DA3CBBEBD
          BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBECB8B3B0D02020F7AC6
          BDBDBDBDBD00B9B9B9B9B9B9B9B9BC8B360800020B173D7A7CA3B1B1B1B1B1B1
          B1B1B1B1B1B1B1B1B1B1B1A37C903B170B03000836A3BCB9B9B9B9B9B9009C9C
          9C9C9C9C9C9C9C9C9D7E330E030002040C0B0E0E0E0E0E0E0E0E0E0E0E0E0E0E
          0E0E0E0B0C050200050E367EBA9C9C9C9C9C9C9C9C0099999999999999999999
          99999D8C3E33120B0807070406060707070607070406060707070607080B1136
          3E8D9D9999999999999999999900979797979797979797979797979797979797
          9797979797979797979797979797979797979797979797979797979797979797
          9797979797008A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A
          8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A008989
          8989898989898989898989898989898989898989898989898989898989898989
          8989898989898989898989898989898989898989890086868686868686868686
          8686868686868686868686868686868686868686868686868686868686868686
          8686868686868686868686868600848484848484848484848484848484848484
          8484848484848484848484848484848484848484848484848484848484848484
          8484848484008282828282828282828282828282828282828282828282828282
          8282828282828282828282828282828282828282828282828282828282008080
          8080808080808080808080808080808080808080808080808080808080808080
          8080808080808080808080808080808080808080800079797979797979797979
          7979797979797979797979797979797979797979797979797979797979797979
          7979797979797979797979797900777777777777777777777777777777777777
          7777777777777777777777777777777777777777777777777777777777777777
          777777777700}
      end
      object CaptLb: TLabel
        Left = 16
        Top = 16
        Width = 385
        Height = 13
        AutoSize = False
        Caption = #1050#1086#1085#1074#1077#1088#1090#1077#1088' '#1073#1072#1079#1099' '#1076#1072#1085#1085#1099#1093
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        WordWrap = True
      end
    end
    object FBDBConnectStrEd: TcxTextEdit
      Left = 11
      Top = 164
      Enabled = False
      TabOrder = 2
      Width = 355
    end
    object DBFDBConnectStrEd: TcxTextEdit
      Left = 11
      Top = 115
      Enabled = False
      TabOrder = 3
      Width = 355
    end
    object Panel1: TPanel
      Left = 7
      Top = 193
      Width = 369
      Height = 145
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 4
      object HintLb: TLabel
        Left = 7
        Top = 12
        Width = 30
        Height = 13
        Caption = 'HintLb'
      end
      object CurProgressLb: TLabel
        Left = 7
        Top = 40
        Width = 70
        Height = 13
        Caption = 'CurProgressLb'
      end
      object TotalProgressLb: TLabel
        Left = 7
        Top = 93
        Width = 77
        Height = 13
        Caption = 'TotalProgressLb'
      end
      object CurPB: TcxProgressBar
        Left = 7
        Top = 64
        Properties.BarStyle = cxbsGradient
        Properties.BeginColor = clBlue
        Properties.EndColor = clBlack
        Properties.OverloadValue = 50.000000000000000000
        Style.BorderStyle = ebsFlat
        Style.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleFocused.BorderStyle = ebsFlat
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.BorderStyle = ebsFlat
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 0
        Width = 355
      end
      object TotalPB: TcxProgressBar
        Left = 7
        Top = 116
        Properties.BarStyle = cxbsGradient
        Properties.BeginColor = clBlue
        Properties.EndColor = clBlack
        Style.BorderColor = clWindowFrame
        Style.BorderStyle = ebsFlat
        StyleFocused.BorderStyle = ebsFlat
        StyleHot.BorderStyle = ebsFlat
        TabOrder = 1
        Width = 355
      end
    end
  end
  object ICSAboutDialog: TICSAboutDialog
    VIXML.Strings = (
      '<versioninfo project_name="'#1050#1086#1085#1074#1077#1088#1090#1077#1088' '#1041#1044'">'
      
        '  <companyinfo name="'#1054#1054#1054' &quot;'#1048#1085#1090#1077#1083#1083#1077#1082#1090#1091#1072#1083#1100#1085#1099#1077' '#1087#1088#1086#1075#1088#1072#1084#1084#1085#1099#1077' '#1089#1080#1089#1090 +
        #1077#1084#1099'&quot;, '#1060#1043#1053#1059' '#1053#1048#1050#1058#1048' '#1041#1058#1057'" contact="'#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075', '#1053#1077#1074#1089#1082#1080#1081' '#1087#1088'.' +
        ' 180, '#1090#1077#1083'./'#1092#1072#1082#1089' (812) 717-50-54, 274-13-81" http="http://www.inc' +
        'omsys.ru" email="support@incomsys.ru"/>'
      '</versioninfo>')
    Image.ovpdTransparent = False
    Left = 312
    Top = 56
    ovpdVIDefLanguageID = 1049
    ovpdVIDefCodePageID = 1251
    ovpdFileName = ''
    ovpdOptions = [aoShowAppImage, aoShowFileInfo, aoShowModulesInfo, aoShowLegalCopyright, aoShowCompanyInfo, aoShowSystemInfo]
  end
  object VerInfoReader: TVerInfoReader
    FileName = 'D:\Embarcadero\Studio\15.0\bin\bds.exe'
    Left = 344
    Top = 56
    ovpdDefLanguageID = 1049
    ovpdDefCodePageID = 1251
  end
  object PrTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = PrTimerTimer
    Left = 241
    Top = 91
  end
  object CmdRetTimer: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = CmdRetTimerTimer
    Left = 194
    Top = 92
  end
end
