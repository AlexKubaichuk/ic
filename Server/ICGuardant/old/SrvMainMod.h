//---------------------------------------------------------------------------

#ifndef SrvMainModH
#define SrvMainModH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ScktComp.hpp>
#include "Globals.h"
//---------------------------------------------------------------------------
class TSrvMain : public TDataModule
{
__published:	// IDE-managed Components
        TServerSocket *cmdServer;
        void __fastcall cmdServerClientConnect(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall cmdServerClientDisconnect(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall cmdServerClientError(TObject *Sender,
          TCustomWinSocket *Socket, TErrorEvent ErrorEvent,
          int &ErrorCode);
        void __fastcall cmdServerClientRead(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall cmdServerClientWrite(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall DataModuleCreate(TObject *Sender);
        void __fastcall DataModuleDestroy(TObject *Sender);
private:	// User declarations
        TConvertVPOptions *FAppOptions;
        bool FReadParamsMode;
        TServiceLogEvent  FLogger;
        TNotifyEvent FProcessRequestsEvt;
        TServiceCommandEvent FOnCommand;
        TServiceGetProgressEvent FGetProgressEvent;
        AnsiString  FCurCmd;
        AnsiString  FCmdWithPapams;
        TStringList *FCmdParams;
        void __fastcall LogMessage(const AnsiString &AValue, unsigned EventType);
        void __fastcall LogMsg(const AnsiString &AValue);
        void __fastcall _DEBUG_LOG_(const AnsiString &AValue);
        void __fastcall FProcessRequests();
        bool __fastcall FGetProgress();
        AnsiString __fastcall FSrvCommand(const AnsiString &ACmd, TStringList *ACmdParams);

        bool __fastcall FGetSrvActive();
        void __fastcall FSetSrvActive(bool AValue);

public:		// User declarations
        __fastcall TSrvMain(TComponent* Owner, TConvertVPOptions* AAppOptions);
        __property TServiceLogEvent OnLogMessage = {read=FLogger, write=FLogger};
        __property TNotifyEvent OnProcessRequestsRequired = {read=FProcessRequestsEvt, write=FProcessRequestsEvt};
        __property TServiceCommandEvent OnCommand = {read=FOnCommand, write=FOnCommand};
        __property AnsiString  CmdWithPapams = {read=FCmdWithPapams, write=FCmdWithPapams};
        __property TServiceGetProgressEvent OnGetProgress = {read=FGetProgressEvent, write=FGetProgressEvent};
        __property bool SrvActive = {read=FGetSrvActive, write=FSetSrvActive};
};
//---------------------------------------------------------------------------
extern PACKAGE TSrvMain *SrvMain;
//---------------------------------------------------------------------------
#endif
