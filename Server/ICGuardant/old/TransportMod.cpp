//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "TransportMod.h"
#include "srvsetting.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TTransportClient *TransportClient;
//---------------------------------------------------------------------------
__fastcall TTransportClient::TTransportClient(TComponent* Owner, TConvertVPOptions* AAppOptions)
        : TDataModule(Owner)
{
  FAppOptions = AAppOptions;
  try
   {
     FTransportModule = LoadLibraryEx (
                                        icsPrePath(ExtractFilePath(ParamStr(0))+"\\transportdll\\"+FAppOptions->TransportDllName).c_str(), // file name of module
                                        NULL,                                         // reserved, must be NULL
                                        LOAD_WITH_ALTERED_SEARCH_PATH                 // entry-point execution option
                                      );
   }
  catch (Exception &E)
   {
    _DEBUG_LOG_("Err.Msg: "+E.Message);
   }
  if(FTransportModule)
   {
     FTransportDllInit             = (TransportInitProc)GetProcAddress(FTransportModule,"Init");
     FTransportDllDeinit           = (TransportDeinitProc)GetProcAddress(FTransportModule,"Deinit");
     FTransportDllGetName          = (TransportGetNameProc)GetProcAddress(FTransportModule,"GetName");
     FTransportDllSendData         = (TransportSendDataProc)GetProcAddress(FTransportModule,"SendData");
     FTransportDllGetData          = (TransportGetDataProc)GetProcAddress(FTransportModule,"GetData");
     FTransportDllGetSetting       = (TransportGetSettingProc)GetProcAddress(FTransportModule,"GetSetting");
     FTransportDelPackage          = (TransportDelPackageProc)GetProcAddress(FTransportModule,"DeletePackage");
     FTransportDllGetParentOrgData = (TransportGetParentOrgDataProc)GetProcAddress(FTransportModule,"GetParentOrgData");

     if( FTransportDllInit &&
         FTransportDllDeinit &&
         FTransportDllGetName &&
         FTransportDllSendData &&
         FTransportDllGetData &&
         FTransportDllGetSetting &&
         FTransportDelPackage &&
         FTransportDllGetParentOrgData
       )
      {
        FTransportDllInit(0, icsPrePath(ExtractFilePath(ParamStr(0))).c_str(), FAppOptions->Defs->GetChildByName("transport_setting", true)->AsXML.c_str(), TransportDllLog);
      }
     else
      LogErr("������ �������� ���������� ����������. ������ ��������� ������������ ������������� ������.");
   }
  else
    LogErr("������ �������� ���������� ����������.");
  ReceiveTimer->Interval = FAppOptions->GetDataPeriod*60000l;
}
//---------------------------------------------------------------------------
void __fastcall TTransportClient::LogMessage(const AnsiString &AValue, unsigned EventType)
{
  if (FLogger)
   FLogger(AValue, EventType);
}
//---------------------------------------------------------------------------
void __fastcall TTransportClient::LogMsg(const AnsiString &AValue)
{
  if (FLogger)
   FLogger(AValue, -10);
}
//---------------------------------------------------------------------------
void __fastcall TTransportClient::LogErr(const AnsiString &AValue)
{
  if (FLogger)
   FLogger(AValue, -15);
}
//---------------------------------------------------------------------------
void __fastcall TTransportClient::_DEBUG_LOG_(const AnsiString &AValue)
{
  if (FLogger)
   FLogger(AValue, -5);
}
//---------------------------------------------------------------------------
void __fastcall TTransportClient::FProcessRequests()
{
  if (FProcessRequestsEvt)
   FProcessRequestsEvt(this);
}
//---------------------------------------------------------------------------
bool __fastcall TTransportClient::FGetProgress()
{
  bool RC = false;
  try
   {
     if (FGetProgressEvent)
       RC = FGetProgressEvent();
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TTransportClient::FSetProgress(bool AVal)
{
  if (FSetProgressEvent)
   FSetProgressEvent(AVal);
}
//---------------------------------------------------------------------------
bool __fastcall TTransportClient::FGetReceiveActive()
{
  return ReceiveTimer->Enabled;
}
//---------------------------------------------------------------------------
void __fastcall TTransportClient::FSetReceiveActive(bool AVal)
{
  ReceiveTimer->Enabled = AVal;
}
//---------------------------------------------------------------------------
void __fastcall TTransportClient::ReceiveTimerTimer(TObject *Sender)
{
  ReceiveTimer->Enabled = false;
  try
   {
     if (!FGetProgress())
      {
        FReceiveData();
      }
   }
  __finally
   {
     ReceiveTimer->Enabled = true;
   }
}
//---------------------------------------------------------------------------
void __fastcall TTransportClient::FReceiveData()
{
  _DEBUG_LOG_("Pr["+IntToStr((int)FGetProgress())+"] ReciveData.");
  if (!FGetProgress())
   {
     FSetProgress(true);
     char* FRecivedDataList = new char[5000];
     TTagNode *FReceivedDataNode = new TTagNode(NULL);
     TTagNode *FCmd = new TTagNode(NULL);
     memset(FRecivedDataList, 0, 5000);
     try
      {
        int pCount = 0;
        try
         {
           pCount = FTransportDllGetData("", "", "", FRecivedDataList);
           _DEBUG_LOG_("Pr["+IntToStr((int)FGetProgress())+"] TransportGetData. Count="+IntToStr(pCount));
         }
        catch (Exception &E)
         {
           LogErr("�������� ������� ������: "+E.Message);
         }
        FReceivedDataNode->AsXML = AnsiString(FRecivedDataList);
        TTagNode *itPkg = FReceivedDataNode->GetFirstChild();
        TTagNode *FAttachList, *FCmdNode;
        while (itPkg)
         {
           FAttachList = itPkg->GetChildByName("att_list");
           if (FAttachList)
            {
              FCmdNode = FAttachList->GetChildByName("cmd");
              if (FCmdNode)
               {
                 FCmd->LoadFromZIPXMLFile(FCmdNode->AV["val"]);
                 LogMsg("��������� ������ :\""+itPkg->AV["name"]+"\", ����: \""+itPkg->AV["subject"]+"\", ��: "+itPkg->AV["UserName"]+" [�����:"+itPkg->AV["UserAlias"]+" (id:"+itPkg->AV["UserID"]+")].");
                 FExecCommand(itPkg->AV["name"], itPkg->AV["UserID"], itPkg->AV["UserAlias"], itPkg->AV["UserName"], FAttachList, FCmd);
                 LogMsg("����� :\""+itPkg->AV["name"]+"\" ���������.");
               }
              else
               LogErr("����� :\""+itPkg->AV["name"]+"\". ����������� ���� \"cmd.zxml\".");
            }
           else
            LogErr("����� :\""+itPkg->AV["name"]+"\". ����������� ������ ������������ ������.");
           itPkg = itPkg->GetNext();
         }
      }
     __finally
      {
        FSetProgress(false);
        delete [] FRecivedDataList;
        delete FReceivedDataNode;
        delete FCmd;
      }
   }
}
//---------------------------------------------------------------------------

bool __fastcall TTransportClient::SendDataTo(TTransportPackData *APkg, const AnsiString &AExportFN)
{ // �������� ������ APkg->UserID
  bool RC = false;
  try
   {
     if (FileExists(AExportFN))
      {
        AnsiString CmdFN = FTmpOutFolderName+"\\cmd.zxml";
        _DEBUG_LOG_("File \""+AExportFN+"\" exists.");
        char FUserID[256];
        char FUserAlias[256];
        char FUserName[1024];
        memset(FUserID,0,256);
        memset(FUserAlias,0,256);
        memset(FUserName,0,1024);
        TTagNode *FCommandNode = new TTagNode(NULL);
        TTagNode *FAttNode = new TTagNode(NULL);
        try
         {
           FCommandNode->Name = "cmd";
           FCommandNode->AV["gui"] = NewGUI();
           FAttNode->Name = "attlist";

           FCommandNode->AddChild("import_unit")->AV["fn"] = ExtractFileName(AExportFN);
           FCommandNode->SaveToZIPXMLFile(CmdFN,"");

           FAttNode->AddChild("i")->AV["att"] = CmdFN;
           FAttNode->AddChild("i")->AV["att"] = AExportFN;
           try
            {
              strncpy(FUserID,    APkg->UserID.c_str(),    APkg->UserID.Length());
              strncpy(FUserAlias, APkg->UserAlias.c_str(), APkg->UserAlias.Length());
              strncpy(FUserName,  APkg->UserName.c_str(),  APkg->UserName.Length());
            }
           catch (Exception &E)
            {
              LogErr("TransportSendData. "+E.Message);
            }
           _DEBUG_LOG_("TransportGetParentOrgData: UserID: "+AnsiString(FUserID)+"  UserAlias: "+AnsiString(FUserAlias)+"  UserName: "+AnsiString(FUserName));
           try
            {
              FTransportDllSendData(FUserID, FUserAlias, FUserName, "export", FAttNode->AsXML.c_str());
              RC = true;
            }
           catch (Exception &E)
            {
              LogErr("TransportSendData. "+E.Message);
            }
           _DEBUG_LOG_("TransportSendData. Command: "+FAttNode->AsXML);
         }
        __finally
         {
           delete FCommandNode;
           delete FAttNode;
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TTransportClient::SendDataToParent(const AnsiString &AExportFN)
{ // �������� ������ ����������
  bool RC = false;
  TTransportPackData *FPkg = NULL;
  try
   {
     char FUserID[256];
     char FUserAlias[256];
     char FUserName[1024];
     memset(FUserID,0,256);
     memset(FUserAlias,0,256);
     memset(FUserName,0,1024);
     try
      {
        FTransportDllGetParentOrgData(FUserID, FUserAlias, FUserName);
        if (AnsiString(FUserID).Trim().Length())
         {
           FPkg = new TTransportPackData("", FUserID, FUserAlias, FUserName, NULL, NULL);
           RC = SendDataTo(FPkg, AExportFN);
         }
        else
         LogErr("TransportSendData. ������ �������� ��������. �� ����� ��� ����������� �����������.");
      }
     catch (Exception &E)
      {
        LogErr("TransportSendData. "+E.Message);
      }
   }
  __finally
   {
     if (FPkg) delete FPkg;
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TTransportClient::SendDataToOwner(TTransportPackData *APkg, const AnsiString &AExportFN)
{ // �������� ������ UserID
  bool RC = false;
  try
   {
     RC = SendDataTo(APkg, AExportFN);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TTransportClient::SendChGUINToOwner(TTransportPackData *APkg, const AnsiString &AOldGUID, const AnsiString &ANewGUID)
{ // �������� ������� �� ������ GUID, APkg->UserID
  bool RC = false;
  try
   {
     AnsiString CmdFN = FTmpOutFolderName+"\\cmd.zxml";
     char FUserID[256];
     char FUserAlias[256];
     char FUserName[1024];
     memset(FUserID,0,256);
     memset(FUserAlias,0,256);
     memset(FUserName,0,1024);
     TTagNode *FCommandNode = new TTagNode(NULL);
     TTagNode *FAttNode = new TTagNode(NULL);
     try
      {
        FCommandNode->Name = "cmd";
        FCommandNode->AV["gui"] = NewGUI();
        FAttNode->Name = "attlist";
        TTagNode *tmpNode = FCommandNode->AddChild("change_guin");
        tmpNode->AV["clid"] = "";
        tmpNode->AV["old"]  = AOldGUID;
        tmpNode->AV["new"]  = ANewGUID;
        FCommandNode->SaveToZIPXMLFile(CmdFN,"");

        FAttNode->AddChild("i")->AV["att"] = CmdFN;

        strncpy(FUserID,    APkg->UserID.c_str(),    APkg->UserID.Length());
        strncpy(FUserAlias, APkg->UserAlias.c_str(), APkg->UserAlias.Length());
        strncpy(FUserName,  APkg->UserName.c_str(),  APkg->UserName.Length());

        _DEBUG_LOG_("TransportGetParentOrgData: UserID: "+AnsiString(FUserID)+"  UserAlias: "+AnsiString(FUserAlias)+"  UserName: "+AnsiString(FUserName));
        try
         {
           FTransportDllSendData(FUserID, FUserAlias, FUserName, "change_guin", FAttNode->AsXML.c_str());
           RC = true;
         }
        catch (Exception &E)
         {
           LogErr("TransportSendData. "+E.Message);
         }
        _DEBUG_LOG_("TransportSendData. Command: "+FAttNode->AsXML);
      }
     __finally
      {
        delete FCommandNode;
        delete FAttNode;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TTransportClient::SendTicket(TTransportPackData *APkg)
{ // �������� ��������� � ��������� ������ ACmd->AV["gui"]
}
//---------------------------------------------------------------------------
void __fastcall TTransportClient::FExecCommand(const AnsiString &APkgName, const AnsiString &AUserID, const AnsiString &AUserAlias, const AnsiString &AUserName, TTagNode *AAttachNode, TTagNode *ACmd)
{
  if (FCmdExec)
   FCmdExec(APkgName, AUserID, AUserAlias, AUserName, AAttachNode, ACmd);
}
//---------------------------------------------------------------------------
void __fastcall TTransportClient::DeletePackage(const AnsiString &APkgID)
{
  FTransportDelPackage(APkgID.c_str());
}
//---------------------------------------------------------------------------

