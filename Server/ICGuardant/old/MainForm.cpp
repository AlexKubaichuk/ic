//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainForm.h"
#include "AxeUtil.h"
#include "Options.h"
#include "DKMBUtils.h"
#include <FileCtrl.hpp>
//#include "msgdef.h"
#include "SrvcUtil.h"

//#include "ADOData.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxProgressBar"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxEdit"
#pragma link "ICSAboutDialog"
#pragma link "DKVCLVerInfo"
#pragma link "cxButtonEdit"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "cxCheckBox"
#pragma link "cxGroupBox"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeels"
#pragma resource "*.dfm"

TMainFm *MainFm;
//---------------------------------------------------------------------------
static const AnsiString ConvSrvcName = "HospConvSrv";
//---------------------------------------------------------------------------
__fastcall TMainFm::TMainFm(TComponent* Owner) :
  TForm(Owner)
{
  Application->Title = Caption;
  FAppOptions = new TConvertVPOptions;
  try
   {
     FAppOptions->Load();
   }
  catch(EOptLoadErr &E)
   {
     throw E;
   }
//  TCP->RemoteHost = "localhost";
  cmdClient->Host = "localhost";
  if (FAppOptions->Host.Length())
   {
//     TCP->RemoteHost = FAppOptions->Host;
     cmdClient->Host = FAppOptions->Host;
   }
//  TCP->RemotePort = FAppOptions->Port;
  cmdClient->Port = FAppOptions->Port;
  InitHintLabel();
  InitProgressCtrls();
  InitParamCtrls();
  CheckSrvc(true);
}

//---------------------------------------------------------------------------
__fastcall TMainFm::~TMainFm()
{
  FAppOptions->Save();
  delete FAppOptions;
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
  if (IsConvProgress())
   _MSG_INF("��������.\n���������� ������������� �����������.\n��� ��������� ������������� ������� ������ \"��������\".","���������");
  CanClose = true;
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::InitHintLabel()
{
  HintLb->Caption = "";//"������� ��������� ��������� � ������� ������ \"�����\".";
  CaptLb->Caption = "�������� ������������� ���� ������ �� \"" + FAppOptions->DBName + "\" v" + FAppOptions->DBVersTxt;
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::InitProgressCtrls()
{
  HintLb->Caption = "";
  CurProgressLb->Caption = "";
  CurPB->Position = 0;
  TotalProgressLb->Caption = "";
  TotalPB->Position = 0;
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::InitParamCtrls()
{
  FBDBConnectStrLb->Caption = "���� � ���� ������ �� \"" + FAppOptions->DBName + "\":";
  FBDBConnectStrEd->Text    = FAppOptions->DBFilePath;
  DBFDBConnectStrEd->Text   = FAppOptions->SrcDBPath;
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::AboutBtnClick(TObject *Sender)
{
  ICSAboutDialog->Execute();
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::OptionsBtnClick(TObject *Sender)
{
  TOptionsFm *dlg = new TOptionsFm(this, true);
  try
  {
    dlg->AppOptions = FAppOptions;
    if (dlg->ShowModal() == mrOk)
     {
       FAppOptions->Assign(dlg->AppOptions);
       FAppOptions->Save();
       InitProgressCtrls();
       RestartConvService();
     }
  }
  __finally
  {
    delete dlg;
  }
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::RestartConvService()
{
  SC_HANDLE h_manager = NULL;
  SC_HANDLE h_svc     = NULL;

  SERVICE_STATUS_PROCESS ssStatus;

  try
   {
     OptionsBtn->Enabled  = false;
     FullSyncBtn->Enabled = false;
     ClsSyncBtn->Enabled  = false;
     UnitSyncBtn->Enabled = false;
     StopBtn->Enabled     = false;
//     CloseBtn->Enabled    = false;
     PrTimer->Enabled     = false;
     h_manager = OpenSCManager("" ,NULL, SC_MANAGER_CONNECT+SC_STATUS_PROCESS_INFO);
     if (h_manager)
      {
        h_svc = OpenService(h_manager, ConvSrvcName.c_str(), SERVICE_ALL_ACCESS);
        if (h_svc)
         {
           DWORD FStatus;
           int RC = ServiceStatusEx(h_svc, ssStatus);
           if (!RC)
            {
              bool CanSrvcStart = false;
              if (ssStatus.dwCurrentState == SERVICE_RUNNING)
               {
                 if (IsConvProgress())
                  _MSG_ERR("����������� �������������, ��� ���������� �������� ���������� �������������.","������");
                 else
                  { // ������ ����� ����� �������������
                    // ������������� ������
                    CurProgressLb->Caption = "��������� �������";
                    Application->ProcessMessages();
                    if (!cmdClient->Active) cmdClient->Active = false;
                    RC = ServiceStopEx(h_svc);
                    if (RC)
                     _MSG_ERR("������ ��������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(RC)+"\"","������");
                    else
                     CanSrvcStart = true;
                  }
               }
              else if (ssStatus.dwCurrentState == SERVICE_STOPPED)
               {
                 CanSrvcStart = true;
               }
              if (CanSrvcStart)
               {
                 CurProgressLb->Caption = "������ �������";
                 Application->ProcessMessages();
                 RC = ServiceStart(ConvSrvcName);
                 if (RC)
                  {
                    _MSG_ERR("������ ������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(RC)+"\"","������");
                    OptionsBtn->Enabled = true;
                    PrTimer->Enabled    = false;
                  }
                 else
                  {
                    if (!cmdClient->Active) cmdClient->Active = true;
                    for (int i = 0; i < 10; i++)
                     {
                       Sleep(100); Application->ProcessMessages();
                     }
                    bool FPr = IsConvProgress();
                    SetCtrlEnabled(!FPr);
                    PrTimer->Enabled = FPr;
                  }
               }
            }
           else
            {
              _MSG_ERR("������ ����������� ������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(RC)+"\"","������");
              OptionsBtn->Enabled = true;
            }
         }
        else
         _MSG_ERR("������ ����������� � ������� ��������� �������������.","������");
      }
   }
  __finally
   {
     if (h_svc)     CloseServiceHandle(h_svc);
     if (h_manager) CloseServiceHandle(h_manager);
     SetCtrlEnabled(true);
     CurProgressLb->Caption = "";
     Application->ProcessMessages();
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::CloseBtnClick(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::StopBtnClick(TObject *Sender)
{
  if (IsConvProgress())
   {
     if (_MSG_QUE("�� ������� � ���, ��� ������ �������� ���������� �������������?",
                  Application->Title.c_str()
                 ) == ID_YES)
      SendConvCommand("stop");
   }
}
//---------------------------------------------------------------------------
AnsiString __fastcall TMainFm::SendConvCommand(AnsiString ACmd)
{
  AnsiString RC = "";
  try
   {
     if (cmdClient->Active)
      {
        AnsiString FCmd = ACmd.Trim();
        TStringList *FCmdList = new TStringList;
        try
         {
           FCmdList->Text = FCmd;
           FCmd = "";
           for (int i = 0; i < FCmdList->Count; i++)
            FCmd += " "+FCmdList->Strings[i];
         }
        __finally
         {
           delete FCmdList;
         }
        FCmd = FCmd.Trim();
        FCmdRet = "";
        cmdClient->Socket->SendText(FCmd);
        CmdRetTimer->Enabled = true;
        while (!FCmdRet.Length())
         {
           Sleep(10);
           Application->ProcessMessages();
         }
        RC = FCmdRet;
        CmdRetTimer->Enabled = false;
      }
     else
       _MSG_ERR("�� ������� ���������� ���������� � ��������.", "������");
   }
  __finally
   {
     CmdRetTimer->Enabled = false;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::PrTimerTimer(TObject *Sender)
{
  PrTimer->Enabled = false;
//  SrvcRestartBtn->Enabled = true;
  SrvcStopBtn->Enabled    = true;
  if (!HintLb->Caption.Length()) HintLb->Caption = SendConvCommand("get state str");
  AnsiString Ret = SendConvCommand("get progress");
  if (Ret.Length())
   {
     try
      {
        if (Ret.LowerCase() == "ok")
         {
           PrTimer->Enabled = false;
           InitProgressCtrls();
           SetCtrlEnabled(true);
           Ret = SendConvCommand("get last state");
           if (Ret.Length() && Ret.LowerCase() != "ok")
            HintLb->Caption = Ret;
         }
        else
         {
           int TP, CP, TM, CM;
           TTagNode *PrData = new TTagNode(NULL);
           TTagNode *tmp;
           try
            {
              PrData->AsXML = Ret;
              tmp = PrData->GetFirstChild();
              CP = tmp->AV["p"].ToIntDef(0);
              CM = tmp->AV["m"].ToIntDef(0);
              if (CurProgressLb->Caption != tmp->AV["PCDATA"])
               CurProgressLb->Caption = tmp->AV["PCDATA"];
              tmp = tmp->GetNext();
              TP = tmp->AV["p"].ToIntDef(0);
              TM = tmp->AV["m"].ToIntDef(0);
              if (TotalProgressLb->Caption != tmp->AV["PCDATA"])
               TotalProgressLb->Caption = tmp->AV["PCDATA"];

              if (CurPB->Properties->Max != CM)
                CurPB->Properties->Max = CM;
              if (TotalPB->Properties->Max != TM)
                TotalPB->Properties->Max = TM;

              if (CurPB->Position != CP)
                CurPB->Position = CP;
              if (TotalPB->Position != TP)
                TotalPB->Position = TP;
            }
           __finally
            {
              delete PrData;
            }
         }
      }
     __finally
      {
        PrTimer->Enabled = true;
      }
   }
  else
   {
     PrTimer->Enabled = false;
     InitProgressCtrls();
     SetCtrlEnabled(true);
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::SetCtrlEnabled(bool AVal)
{
  if (AVal) HintLb->Caption = "";
  OptionsBtn->Enabled  = AVal;
  FullSyncBtn->Enabled = AVal;
  ClsSyncBtn->Enabled  = AVal;
  UnitSyncBtn->Enabled = AVal;
//  CloseBtn->Enabled    = AVal;
  StopBtn->Enabled     = !AVal;
}
//---------------------------------------------------------------------------
bool __fastcall TMainFm::IsConvProgress()
{
  bool RC = false;
  try
   {
     DWORD FStatus;
     int SRC = ServiceStatus(ConvSrvcName, FStatus);
     if (!SRC)
      {
        if (FStatus == SERVICE_RUNNING)
         RC = (SendConvCommand("get state").LowerCase() == "progress");
      }
   }
  __finally
  {
  }
 return RC;
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::FullSyncBtnClick(TObject *Sender)
{
  CheckSrvc(false);
  SendConvCommand("start");
  InitProgressCtrls();
  SetCtrlEnabled(false);
  PrTimer->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::ClsSyncBtnClick(TObject *Sender)
{
  CheckSrvc(false);
  SendConvCommand("start cls");
  InitProgressCtrls();
  SetCtrlEnabled(false);
  PrTimer->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::UnitSyncBtnClick(TObject *Sender)
{
  CheckSrvc(false);
  SendConvCommand("start unit");
  InitProgressCtrls();
  SetCtrlEnabled(false);
  PrTimer->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::Button1Click(TObject *Sender)
{
  PrTimer->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::SrvcRestartBtnClick(TObject *Sender)
{
  DWORD FStatus;
  PrTimer->Enabled = false;
  CurProgressLb->Caption = "����������� ������� �������";
  Application->ProcessMessages();
  int RC = ServiceStatus(ConvSrvcName, FStatus);
  if (!RC)
   {
     if ((FStatus == SERVICE_RUNNING) || (FStatus == SERVICE_STOPPED))
      {
        bool CanStartSrvc = true;
        if (FStatus == SERVICE_RUNNING)
         {
           bool FCanstop = false;
           if (IsConvProgress())
            {
              if (_MSG_QUE("����������� �������������, �� ������������� ������� ������������� ������?","���������") == ID_YES)
               {
                 PrTimer->Enabled = false;
                 SendConvCommand("stop");
                 InitProgressCtrls();
                 SetCtrlEnabled(true);
                 FCanstop = true;
               }
            }
           else
            FCanstop = true;
           if (FCanstop)
            {
              CurProgressLb->Caption = "��������� �������";
              Application->ProcessMessages();
              if (!cmdClient->Active) cmdClient->Active = false;
              RC = ServiceStop(ConvSrvcName);
              if (RC)
               {
                 CanStartSrvc = false;
                 _MSG_ERR("������ ��������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(RC)+"\"","������");
               }
              else
               {
//                 SrvcRestartBtn->Enabled = false;
                 SrvcStopBtn->Enabled    = false;
               }
            }
           else
            CanStartSrvc = false;
         }
        if (CanStartSrvc)
         {
           CurProgressLb->Caption = "������ �������";
           Application->ProcessMessages();
           RC = ServiceStart(ConvSrvcName);
           if (RC)
            {
              _MSG_ERR("������ ������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(RC)+"\"","������");
              OptionsBtn->Enabled = false;
              FullSyncBtn->Enabled   = false;
              ClsSyncBtn->Enabled   = false;
              UnitSyncBtn->Enabled   = false;
//              CloseBtn->Enabled   = false;
            }
           else
            {
//              SrvcRestartBtn->Enabled = false;
              if (!cmdClient->Active) cmdClient->Active = true;
              for (int i = 0; i < 10; i++)
               {
                 Sleep(100); Application->ProcessMessages();
               }
              SrvcStopBtn->Enabled    = true;
            }
           CurProgressLb->Caption = "";
           Application->ProcessMessages();
         }
      }
   }
  else
   {
     _MSG_ERR("������ ����������� ������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(RC)+"\"","������");
     OptionsBtn->Enabled  = false;
     FullSyncBtn->Enabled = false;
     ClsSyncBtn->Enabled  = false;
     UnitSyncBtn->Enabled = false;
//     CloseBtn->Enabled    = false;
     PrTimer->Enabled     = false;
   }
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::SrvcStopBtnClick(TObject *Sender)
{
  DWORD FStatus;
  PrTimer->Enabled = false;
  CurProgressLb->Caption = "����������� ������� �������";
  Application->ProcessMessages();
  int RC = ServiceStatus(ConvSrvcName, FStatus);
  if (!RC)
   {
     if (FStatus == SERVICE_RUNNING)
      {
        bool FCanstop = false;
        if (IsConvProgress())
         {
           if (_MSG_QUE("����������� �������������, �� ������������� ������� ���������� ������?","���������") == ID_YES)
            {
              PrTimer->Enabled = false;
              SendConvCommand("stop");
              InitProgressCtrls();
              SetCtrlEnabled(true);
              FCanstop = true;
            }
         }
        else
         FCanstop = true;
        if (FCanstop)
         {
           CurProgressLb->Caption = "��������� �������";
           Application->ProcessMessages();
           if (!cmdClient->Active) cmdClient->Active = false;
           RC = ServiceStop(ConvSrvcName);
           if (RC)
            _MSG_ERR("������ ��������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(RC)+"\"","������");
           else
            {
//              SrvcRestartBtn->Enabled = false;
              SrvcStopBtn->Enabled    = false;
            }
         }
      }
     else if (FStatus == SERVICE_STOPPED)
      {
//        SrvcRestartBtn->Enabled = false;
        SrvcStopBtn->Enabled    = false;
      }
     else
       _MSG_ERR("������ ��������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(RC)+"\"","������");
     CurProgressLb->Caption = "";
     Application->ProcessMessages();
   }
  else
   {
     _MSG_ERR("������ ����������� ������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(RC)+"\"","������");
//     SrvcRestartBtn->Enabled = false;
     SrvcStopBtn->Enabled    = false;
     OptionsBtn->Enabled  = false;
     FullSyncBtn->Enabled = false;
     ClsSyncBtn->Enabled  = false;
     UnitSyncBtn->Enabled = false;
//     CloseBtn->Enabled    = false;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TMainFm::CheckSrvc(bool ACheckState)
{
  bool RC = false;
  try
   {
     DWORD FStatus;
     int SrcvRC = ServiceStatus(ConvSrvcName, FStatus);
     if (!SrcvRC)
      {
        if (FStatus == SERVICE_STOPPED)
         {
           SrcvRC = ServiceStart(ConvSrvcName);
           if (SrcvRC)
            {
              _MSG_ERR("������ ������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(SrcvRC)+"\"","������");
//              SrvcRestartBtn->Enabled  = false;
              SrvcStopBtn->Enabled  = false;
              OptionsBtn->Enabled = false;
              FullSyncBtn->Enabled   = false;
              ClsSyncBtn->Enabled   = false;
              UnitSyncBtn->Enabled   = false;
              PrTimer->Enabled    = false;
            }
         }
        SrcvRC = ServiceStatus(ConvSrvcName, FStatus);
        if (FStatus == SERVICE_RUNNING)
         {
           if (!cmdClient->Active) cmdClient->Active = true;
           for (int i = 0; i < 10; i++)
            {
              Sleep(100); Application->ProcessMessages();
            }
           RC = true;
//           SrvcRestartBtn->Enabled  = true;
           SrvcStopBtn->Enabled  = true;
           if (ACheckState)
            {
              bool FPr = IsConvProgress();
              SetCtrlEnabled(!FPr);
              PrTimer->Enabled = FPr;
              if (!FPr)
               {
                  AnsiString Ret = SendConvCommand("get last state");
                  if (Ret.Length() && Ret.LowerCase() != "ok")
                   HintLb->Caption = Ret;
               }
            }
         }
      }
     else
      {
        _MSG_ERR("������ ����������� ������� ������� ��������� �������������.\n\n��������� ���������:\n\""+Dkutils::GetAPILastErrorFormatMessage(SrcvRC)+"\"","������");
//        SrvcRestartBtn->Enabled  = false;
        SrvcStopBtn->Enabled  = false;
        OptionsBtn->Enabled  = false;
        FullSyncBtn->Enabled = false;
        ClsSyncBtn->Enabled  = false;
        UnitSyncBtn->Enabled = false;
        PrTimer->Enabled     = false;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::cmdClientRead(TObject *Sender,
      TCustomWinSocket *Socket)
{
  FCmdRet = Socket->ReceiveText();
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TMainFm::CmdRetTimerTimer(TObject *Sender)
{
  CmdRetTimer->Enabled = false;
  _MSG_ERR("������ ��������� ������ �������, �������� �������� ��������.","������");
  FCmdRet = "Error";
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------

