//---------------------------------------------------------------------------

#ifndef MainFormH
#define MainFormH

//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxProgressBar.hpp"
#include <ExtCtrls.hpp>
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
#include "cxEdit.hpp"
#include "ICSAboutDialog.h"
#include <Dialogs.hpp>
#include "DKVCLVerInfo.h"

#include "Globals.h"
#include "cxButtonEdit.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include <Graphics.hpp>
#include "cxCheckBox.hpp"
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <Sockets.hpp>
#include "cxGroupBox.hpp"
#include <ScktComp.hpp>
#include "cxGraphics.hpp"
#include "cxLookAndFeels.hpp"

//---------------------------------------------------------------------------

class TMainFm : public TForm
{
__published:	// IDE-managed Components
    TPanel *ClientPn;

    TPanel *FooterPn;
    TPanel *HeaderPn;
    TBevel *Bevel1;
    TBevel *Bevel2;
    TButton *CloseBtn;
    TImage *Image;
        TLabel *CaptLb;
    TICSAboutDialog *ICSAboutDialog;
        TLabel *DBFDBConnectStrLb;
        TLabel *FBDBConnectStrLb;
    TVerInfoReader *VerInfoReader;
        TButton *OptionsBtn;
        TcxTextEdit *FBDBConnectStrEd;
        TcxTextEdit *DBFDBConnectStrEd;
        TTimer *PrTimer;
        TButton *AboutBtn;
        TPanel *Panel1;
        TLabel *HintLb;
        TLabel *CurProgressLb;
        TcxProgressBar *CurPB;
        TLabel *TotalProgressLb;
        TcxProgressBar *TotalPB;
        TcxGroupBox *cxGroupBox1;
        TButton *SrvcRestartBtn;
        TButton *SrvcStopBtn;
        TcxGroupBox *cxGroupBox2;
        TButton *FullSyncBtn;
        TButton *ClsSyncBtn;
        TButton *UnitSyncBtn;
        TButton *StopBtn;
        TButton *Button1;
        TTimer *CmdRetTimer;
    void __fastcall OptionsBtnClick(TObject *Sender);
    void __fastcall AboutBtnClick(TObject *Sender);
    void __fastcall CloseBtnClick(TObject *Sender);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall StopBtnClick(TObject *Sender);
        void __fastcall PrTimerTimer(TObject *Sender);
        void __fastcall FullSyncBtnClick(TObject *Sender);
        void __fastcall ClsSyncBtnClick(TObject *Sender);
        void __fastcall UnitSyncBtnClick(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall SrvcRestartBtnClick(TObject *Sender);
        void __fastcall SrvcStopBtnClick(TObject *Sender);
        void __fastcall cmdClientRead(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall CmdRetTimerTimer(TObject *Sender);


private:	// User declarations
    AnsiString __fastcall SendConvCommand(AnsiString ACmd);
    void __fastcall SetCtrlEnabled(bool AVal);
    TConvertVPOptions*  FAppOptions;        // Настройки приложения
    AnsiString  FCmdRet;
    void __fastcall RestartConvService();
    bool __fastcall IsConvProgress();
    bool __fastcall CheckSrvc(bool ACheckState);

protected:
    inline void __fastcall InitHintLabel();
    inline void __fastcall InitProgressCtrls();
    void __fastcall InitParamCtrls();



public:		// User declarations
    __fastcall TMainFm(TComponent* Owner);
    virtual __fastcall ~TMainFm();

    __property TConvertVPOptions* AppOptions = {read = FAppOptions};
};

//---------------------------------------------------------------------------

extern PACKAGE TMainFm *MainFm;

//---------------------------------------------------------------------------

#endif
