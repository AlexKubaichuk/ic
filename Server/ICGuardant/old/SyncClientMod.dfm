object SyncClient: TSyncClient
  OldCreateOrder = False
  Left = 331
  Top = 175
  Height = 373
  Width = 491
  object CmdRetTimer: TTimer
    Enabled = False
    Interval = 30000
    OnTimer = CmdRetTimerTimer
    Left = 29
    Top = 64
  end
  object cmdSyncClient: TClientSocket
    Active = False
    ClientType = ctNonBlocking
    Port = 0
    OnRead = cmdSyncClientRead
    OnError = cmdSyncClientError
    Left = 32
    Top = 9
  end
end
