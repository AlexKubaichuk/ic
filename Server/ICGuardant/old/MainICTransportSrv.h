//---------------------------------------------------------------------------
#ifndef MainICTransportSrvH
#define MainICTransportSrvH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <SvcMgr.hpp>
#include <vcl.h>
#include <Sockets.hpp>
//#include "Globals.h"
#include "icsLog.h"
#include "Globals.h"
#include "DKVCLVerInfo.h"
#include <ExtCtrls.hpp>
#include <ScktComp.hpp>
#include <list.h>

#include "SyncClientMod.h"
#include "SrvMainMod.h"
#include "TransportMod.h"
//---------------------------------------------------------------------------
class TICTransportSrv : public TService
{
__published:    // IDE-managed Components
        TVerInfoReader *VerInfoReader;
        void __fastcall ServiceStart(TService *Sender, bool &Started);
        void __fastcall ServiceStop(TService *Sender, bool &Stopped);
        void __fastcall ServiceExecute(TService *Sender);
        void __fastcall ServiceCreate(TObject *Sender);
        void __fastcall ServiceAfterInstall(TService *Sender);
        void __fastcall ServiceDestroy(TObject *Sender);
private:        // User declarations

       typedef list <TTransportPackData*> TReceiveCmdList;

       TReceiveCmdList FCmdBuffer;
       TReceiveCmdList FReceiveCmdBuffer;
       bool            FSrvProgress;

       TSyncClient *FSyncClient;
       TSrvMain    *FMainServer;
       TTransportClient *FTransportClient;
       TLog* FLog;           // ������
       AnsiString TmpOutFolderName;

       TConvertVPOptions*  FAppOptions;        // ��������� ����������
       AnsiString CurProgressCaption, TotalProgressCaption, LastConvStateCaption;//, CurConvStateStr;
       int CurPBPosition, TotalPBPosition, CurPBPropertiesMax, TotalPBPropertiesMax;

       void       __fastcall _DEBUG_LOG_(const AnsiString &AMsg);
       void       __fastcall SyncClientLogErrMessage(const AnsiString &AValue, unsigned int EventType);
       void       __fastcall SyncClientProgress(const AnsiString &AValue, unsigned int EventType);
       void       __fastcall FOnProcessRequests(TObject* Sender);
       bool       __fastcall FOnGetInProgress();
       void       __fastcall FOnSetInProgress(bool AVal);
       AnsiString __fastcall FOnServiceCommand(const AnsiString &ACmd, TStringList *ACmdParams);
       void       __fastcall FReceiveData();
       void       __fastcall FExecCommand(const AnsiString &APkgName, const AnsiString &AUserID, const AnsiString &AUserAlias, const AnsiString &AUserName, TTagNode *AAttachNode, TTagNode *ACmd);
       void       __fastcall FServiceExecute();
       void       __fastcall LoadOptions();
       void       __fastcall ConvertStop();
       void       __fastcall ExecutionBegin(TObject *Sender);
       void       __fastcall InitProgressCtrls();
protected:

public:         // User declarations
	__fastcall TICTransportSrv(TComponent* Owner);
	TServiceController __fastcall GetServiceController(void);
        __property TLog* Log = {read=FLog};           // ������

	friend void __stdcall ServiceController(unsigned CtrlCode);
};
//---------------------------------------------------------------------------
extern PACKAGE TICTransportSrv *ICTransportSrv;
extern void __stdcall TransportDllLog(char* AMessage);
//---------------------------------------------------------------------------
#endif
