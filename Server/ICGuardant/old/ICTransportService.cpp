#include <SysUtils.hpp>
#include <SvcMgr.hpp>
#pragma hdrstop
#define Application Svcmgr::Application
USEFORM("MainICTransportSrv.cpp", ICTransportSrv); /* TService: File Type */
USEFORM("SyncClientMod.cpp", SyncClient); /* TDataModule: File Type */
USEFORM("SrvMainMod.cpp", SrvMain); /* TDataModule: File Type */
USEFORM("TransportMod.cpp", TransportClient); /* TDataModule: File Type */
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	try
	{
		Application->Initialize();
		Application->CreateForm(__classid(TICTransportSrv), &ICTransportSrv);
                 Application->Run();
	}
	catch (Exception &exception)
	{
		Sysutils::ShowException(&exception, System::ExceptAddr());
	}
        catch(...)
        {
		try
		{
	        	throw Exception("");
		}
		catch(Exception &exception)
		{
			Sysutils::ShowException(&exception, System::ExceptAddr());
		}
        }
	return 0;
}

