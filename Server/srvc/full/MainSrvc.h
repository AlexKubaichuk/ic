//---------------------------------------------------------------------------
#ifndef MainSrvcH
#define MainSrvcH
//---------------------------------------------------------------------------
#include <System.SysUtils.hpp>
#include <Classes.hpp>
#include <SvcMgr.hpp>
#include <vcl.h>
//---------------------------------------------------------------------------
class TAppBase;
class TICBaseServer : public TService
{
__published:    // IDE-managed Components
 void __fastcall ServiceStart(Vcl::Svcmgr::TService *Sender, bool &Started);
 void __fastcall ServiceDestroy(TObject *Sender);
 void __fastcall ServiceAfterInstall(Vcl::Svcmgr::TService *Sender);

private:        // User declarations

protected:
  TAppBase* FServer;

  bool __fastcall DoStop(void);
  bool __fastcall DoPause(void);
  bool __fastcall DoContinue(void);
  void __fastcall DoInterrogate(void);

public:         // User declarations
  __fastcall TICBaseServer(TComponent* Owner);
  TServiceController __fastcall GetServiceController(void);

  friend void __stdcall ServiceController(unsigned CtrlCode);
};
//---------------------------------------------------------------------------
extern PACKAGE TICBaseServer *ICBaseServer;
//---------------------------------------------------------------------------
#endif

