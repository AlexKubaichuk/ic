//----------------------------------------------------------------------------

#ifndef appbaseunitH
#define appbaseunitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ExtCtrls.hpp>

#include <Datasnap.DSAuth.hpp>
#include <Datasnap.DSCommonServer.hpp>
#include <Datasnap.DSServer.hpp>

#include <Datasnap.DSTCPServerTransport.hpp>
#include <IPPeerServer.hpp>
#include <IdHTTPWebBrokerBridge.hpp>
#include "AppOptBaseProv.h"
#include "AppOptions.h"
#include "AppOptXMLProv.h"
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdServerIOHandler.hpp>
#include <IdSSL.hpp>
#include <IdSSLOpenSSL.hpp>

//----------------------------------------------------------------------------
#include "dsMKB.h"
#include "dsKLADRUnit.h"
#include "dsOrgUnit.h"
#include "dsICUnit.h"
#include "dsDocUnit.h"
#include "dsVacStoreUnit.h"
#include "dsEIDataUnit.h"
#include "dsAdminUnit.h"
#include "dsMISAPIUnit.h"
#include "dsMISManager.h"
#include "dsDBCheckUnit.h"

//----------------------------------------------------------------------------
class EBaseAppException : public System::Sysutils::Exception
{
    public:
    __fastcall EBaseAppException (const UnicodeString msg) : System::Sysutils::Exception (msg) { };
};
//---------------------------------------------------------------------------
class TAppBase : public TDataModule
{
__published:	// IDE-managed Components
  TDSServer *ICServer;
  TDSAuthenticationManager *DSAuthentication;
  TDSServerClass *dsICClassClass;
 TDSServerClass *dsDocClass;
 TDSServerClass *dsKLADRClass;
 TDSServerClass *dsOrgClass;
 TDSServerClass *dsVacStoreClass;
 TAppOptions *SrvOpt;
 TAppOptXMLProv *SrvOptXMLData;
 TIdServerIOHandlerSSLOpenSSL *SSLHandle;
 TDSServerClass *dsEIDataClass;
 TDSServerClass *DSAdminClass;
 TTimer *ClearTimer;
 TDSServerClass *MISAPIClass;
 TDSTCPServerTransport *DSTCPServerTransport;
 TDSServerClass *DBCheckClass;

  void __fastcall DSAuthenticationUserAuthenticate(TObject *Sender, const UnicodeString Protocol,
          const UnicodeString Context, const UnicodeString User,
          const UnicodeString Password, bool &valid,  TStrings *UserRoles);
  void __fastcall dsICClassClassGetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);
 void __fastcall ICServerError(TDSErrorEventObject *DSErrorEventObject);
 void __fastcall dsICClassClassCreateInstance(TDSCreateInstanceEventObject *DSCreateInstanceEventObject);
 void __fastcall dsICClassClassDestroyInstance(TDSDestroyInstanceEventObject *DSDestroyInstanceEventObject);
 void __fastcall DSAuthenticationUserAuthorize(TObject *Sender, TDSAuthorizeEventObject *AuthorizeEventObject,
          bool &valid);
 void __fastcall dsDocClassCreateInstance(TDSCreateInstanceEventObject *DSCreateInstanceEventObject);
 void __fastcall dsDocClassDestroyInstance(TDSDestroyInstanceEventObject *DSDestroyInstanceEventObject);
 void __fastcall dsDocClassGetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);
 void __fastcall dsKLADRClassGetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);
 void __fastcall dsKLADRClassCreateInstance(TDSCreateInstanceEventObject *DSCreateInstanceEventObject);
 void __fastcall dsKLADRClassDestroyInstance(TDSDestroyInstanceEventObject *DSDestroyInstanceEventObject);
 void __fastcall dsOrgClassCreateInstance(TDSCreateInstanceEventObject *DSCreateInstanceEventObject);
 void __fastcall dsOrgClassDestroyInstance(TDSDestroyInstanceEventObject *DSDestroyInstanceEventObject);
 void __fastcall dsOrgClassGetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);
 void __fastcall dsVacStoreClassCreateInstance(TDSCreateInstanceEventObject *DSCreateInstanceEventObject);
 void __fastcall dsVacStoreClassDestroyInstance(TDSDestroyInstanceEventObject *DSDestroyInstanceEventObject);
 void __fastcall dsVacStoreClassGetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);
 void __fastcall SrvOptLoadXML(TTagNode *AOptNode);
 void __fastcall SSLHandleGetPassword(UnicodeString &Password);
 void __fastcall dsEIDataClassCreateInstance(TDSCreateInstanceEventObject *DSCreateInstanceEventObject);
 void __fastcall dsEIDataClassGetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);
 void __fastcall dsEIDataClassDestroyInstance(TDSDestroyInstanceEventObject *DSDestroyInstanceEventObject);
 void __fastcall DSAdminClassCreateInstance(TDSCreateInstanceEventObject *DSCreateInstanceEventObject);
 void __fastcall DSAdminClassDestroyInstance(TDSDestroyInstanceEventObject *DSDestroyInstanceEventObject);
 void __fastcall DSAdminClassGetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);
 void __fastcall ClearTimerTimer(TObject *Sender);
 void __fastcall MISAPIClassCreateInstance(TDSCreateInstanceEventObject *DSCreateInstanceEventObject);
 void __fastcall MISAPIClassDestroyInstance(TDSDestroyInstanceEventObject *DSDestroyInstanceEventObject);
 void __fastcall MISAPIClassGetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);
 void __fastcall DSTCPServerTransportConnect(TDSTCPConnectEventObject &Event);
 void __fastcall DSTCPServerTransportDisconnect(TDSTCPDisconnectEventObject Event);
 void __fastcall DBCheckClassGetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);

private:	// User declarations
  typedef map<UnicodeString, TDataModule*> TClassUnitMap;
  typedef TDataModule* __fastcall (__closure *TCreateModule)();
  TAxeXMLContainer *FXMLList;

  TClassUnitMap FIcMap;
  TClassUnitMap FDocMap;
  TClassUnitMap FVSMap;
  TClassUnitMap FKLADRMap;
  TClassUnitMap FOrgMap;
  TClassUnitMap FEIDataMap;
  TClassUnitMap FMISAPIMap;

  TdsDBCheckClass *FDBCheck;

  TMISSyncManager *FMISSyncModule;
  TdsMKB *MKB;

  TIdHTTPWebBrokerBridge *FServer;

  int FPort;
  UnicodeString FCPath;
  UnicodeString FAdmUser, FAdmPasswd;

  TdsAdminClass* __fastcall AdmClass();
  bool __fastcall LoadOpt();
  void __fastcall Disconnect();
  bool __fastcall FGetActive();

  TDataModule* __fastcall CreateICClass();
  TDataModule* __fastcall CreateDocClass();
  TDataModule* __fastcall CreateVacStoreClass();
  TDataModule* __fastcall CreateKLADRClass();
  TDataModule* __fastcall CreateOrgClass();
  TDataModule* __fastcall CreateEIDataClass();
  TDataModule* __fastcall CreateMISAPIClass();
  TDataModule*   FSearchClass;
  TDataModule*   FAdminClass;

  void __fastcall FClearSes(UnicodeString ASesId);
  void __fastcall ClassDestroyInstance(TDSDestroyInstanceEventObject *DSDestroyInstanceEventObject, TClassUnitMap &AUnitMap, const UnicodeString AClassName);
  void __fastcall ClassCreateInstance(TDSCreateInstanceEventObject *DSCreateInstanceEventObject, TClassUnitMap &AUnitMap, TCreateModule ACreateNewInstance, const UnicodeString AClassName);

  void __fastcall MISOnLogMessage(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl = 1);
  void __fastcall MISOnLogError(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl = 1);
  void __fastcall MISOnLogSQL(UnicodeString  ASQL, UnicodeString  AFuncName, TTime AExecTime, int ALvl = 1);
  void __fastcall MISOnLogBegin(UnicodeString  AFuncName);
  void __fastcall MISOnLogEnd(UnicodeString  AFuncName);
  void __fastcall CheckBaseCount(int ACount);
  bool __fastcall FLoadXML(TTagNode * ItTag, UnicodeString & Src);
  void __fastcall CreateLogs();
  void __fastcall DeleteLogs();

  //  TTagNode *FDefXML, *FDocDefXML;
public:		// User declarations
  __fastcall TAppBase();
  __fastcall ~TAppBase(void);

  static TAppBase* __fastcall AppServer();
  void __fastcall Start();
  void __fastcall Stop();
  __property int Port = {read=FPort};
  __property bool Active = {read=FGetActive};
};
TDSServer *DSServer(void);
TDSAuthenticationManager *DSAuthenticationManager(void);

#endif

