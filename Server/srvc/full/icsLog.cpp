// ---------------------------------------------------------------------------
#pragma hdrstop
#include "icsLog.h"
#include <windows.h>
#include <stdio.h>
#include <psapi.h>
#include <System.Zip.hpp>
#include <System.DateUtils.hpp>
using namespace std;
// ---------------------------------------------------------------------------
#pragma package(smart_init)
namespace kabLog
 {
 // ###########################################################################
 // ##                                                                       ##
 // ##                                 TLog                                  ##
 // ##                                                                       ##
 // ###########################################################################
 TLog * ErrLog = NULL;
 TLog * MsgLog = NULL;
 TLog * AuthLog = NULL;
 TLog * PlanerLog = NULL;
 TLog * RequestLog = NULL;
 TLog * CostructLog = NULL;
 TLog * SyncLog = NULL;
 TLog * regizLog = NULL;
 // ---------------------------------------------------------------------------
 void __fastcall dsLogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
  {
   if (MsgLog)
    {
     if (AFuncName.Trim().Length())
      MsgLog->LogMessage(AFuncName + " > " + AMessage, ALvl);
     else
      MsgLog->LogMessage(AMessage, ALvl);
    }
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsLogWarning(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
  {
   if (MsgLog)
    {
     if (AFuncName.Trim().Length())
      MsgLog->LogWarning(AFuncName + " > " + AMessage, ALvl);
     else
      MsgLog->LogWarning(AMessage, ALvl);
    }
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsLogBegin(UnicodeString AFuncName, int ALvl)
  {
   if (MsgLog)
    MsgLog->LogMessage(AFuncName + ":begin", ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsLogEnd(UnicodeString AFuncName, int ALvl)
  {
   if (MsgLog)
    MsgLog->LogMessage(AFuncName + ":end", ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsLogC(UnicodeString AFuncName, bool AEnd)
  {
   if (CostructLog)
    {
     if (AEnd)
      CostructLog->LogMessage(AFuncName + "::end", 6);
     else
      CostructLog->LogMessage(AFuncName + "::begin", 6);
    }
   if (AEnd)
    dsLogEnd(AFuncName);
   else
    dsLogBegin(AFuncName);
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsLogD(UnicodeString AFuncName, bool AEnd)
  {
   if (AEnd)
    dsLogEnd(AFuncName);
   else
    dsLogBegin(AFuncName);
   if (CostructLog)
    {
     if (AEnd)
      CostructLog->LogMessage(AFuncName + "::end", 6);
     else
      CostructLog->LogMessage(AFuncName + "::begin", 6);
    }
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsLogAuth(UnicodeString AMessage, int ALvl)
  {
   if (MsgLog)
    MsgLog->LogMessage(AMessage, ALvl);
   if (AuthLog)
    AuthLog->LogMessage(AMessage, ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
  {
   if (ErrLog)
    ErrLog->LogError(AFuncName + " -> " + AMessage, ALvl);
   if (MsgLog)
    MsgLog->LogError(AFuncName + " -> " + AMessage, ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
  {
   if (MsgLog)
    MsgLog->LogMessage("  " + AFuncName + " [" + AExecTime.FormatString("ss.zzz") + "] SQL = " + ASQL, ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsPlanLogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
  {
   if (PlanerLog)
    {
     if (AFuncName.Trim().Length())
      PlanerLog->LogMessage(AFuncName + " > " + AMessage, ALvl);
     else
      PlanerLog->LogMessage(AMessage, ALvl);
    }
   dsLogMessage(AMessage, AFuncName, ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsPlanLogWarning(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
  {
   if (PlanerLog)
    {
     if (AFuncName.Trim().Length())
      PlanerLog->LogWarning(AFuncName + " > " + AMessage, ALvl);
     else
      PlanerLog->LogWarning(AMessage, ALvl);
    }
   dsLogWarning(AMessage, AFuncName, ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsRequestLogMessage(UnicodeString AMessage, UnicodeString AFuncName)
  {
   if (RequestLog)
    {
     if (AFuncName.Trim().Length())
      RequestLog->LogMessage(AFuncName + " > " + AMessage, 5);
     else
      RequestLog->LogMessage(AMessage, 5);
    }
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsPlanLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
  {
   dsLogSQL(ASQL, AFuncName, AExecTime, ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsPlanLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
  {
   if (PlanerLog)
    PlanerLog->LogError(AFuncName + " -> " + AMessage, ALvl);
   dsLogError(AMessage, AFuncName, ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsSyncLogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
  {
   if (SyncLog)
    {
     if (AFuncName.Trim().Length())
      SyncLog->LogMessage(AFuncName + " > " + AMessage, ALvl);
     else
      SyncLog->LogMessage(AMessage, ALvl);
    }
   dsLogMessage(AMessage, AFuncName, ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsSyncLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
  {
   dsLogSQL(ASQL, AFuncName, AExecTime, ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsSyncLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
  {
   if (SyncLog)
    SyncLog->LogError(AFuncName + " -> " + AMessage, ALvl);
   dsLogError(AMessage, AFuncName, ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsRegizLogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
  {
   if (regizLog)
    {
     if (AFuncName.Trim().Length())
      regizLog->LogMessage(AFuncName + " > " + AMessage, ALvl);
     else
      regizLog->LogMessage(AMessage, ALvl);
    }
  }
 // ---------------------------------------------------------------------------
 void __fastcall dsRegizLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
  {
   if (regizLog)
    regizLog->LogError(AFuncName + " -> " + AMessage, ALvl);
   dsLogError(AMessage, AFuncName, ALvl);
  }
 // ---------------------------------------------------------------------------
 }
