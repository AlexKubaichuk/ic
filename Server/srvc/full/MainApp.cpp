// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "MainApp.h"
// #include "icsLog.h"
#include <DSSession.hpp>
// ---------------------------------------------------------------------------
#include "appbaseunit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm * MainForm;
// ---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent * Owner) : TForm(Owner)
 {
  FServer = TAppBase::AppServer();
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::ButtonStartClick(TObject * Sender)
 {
  FServer->Start();
  PortED->Text    = IntToStr(FServer->Port);
  PortED->Enabled = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::ButtonStopClick(TObject * Sender)
 {
  FServer->Stop();
  PortED->Enabled = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::ApplicationEvents1Idle(TObject * Sender, bool & Done)
 {
  ButtonStart->Enabled = !FServer->Active;
  ButtonStop->Enabled  = FServer->Active;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::FormDestroy(TObject * Sender)
 {
  if (FServer)
   {
    FServer->Stop();
    Sleep(100);
    delete FServer;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::SchBegLineBtnClick(TObject * Sender)
 {
  UnicodeString RC = "";
  TTagNode * def = new TTagNode;
  // TdsSrvClassif *FSQLs;
  try
   {
    def->LoadFromZIPXMLFile("D:\\work\\imm\\icv7\\001D3500-00005882-DC28.zxml");
    def->Iterate(GetNode);
    def->Encoding = "utf-8";
    def->SaveToZIPXMLFile("D:\\work\\imm\\icv7\\001D3500-00005882-DC28.zxml.new");
    def->LoadFromZIPXMLFile("D:\\work\\imm\\icv7\\12063611-00008cd7-cd89.zxml");
    def->Iterate(GetNode);
    def->Encoding = "utf-8";
    def->SaveToZIPXMLFile("D:\\work\\imm\\icv7\\12063611-00008cd7-cd89.zxml.new");
   }
  __finally
   {
    delete def;
    // delete FSQLs;
   }
  ShowMessage("��");
 }
// ---------------------------------------------------------------------------
bool __fastcall TMainForm::GetNode(TTagNode * ANode)
 {
  if (ANode->CmpName("schema"))
   {
    if (!ANode->GetChildByName("begline"))
     {
      TTagNode * FBegNode = ANode->GetFirstChild()->Insert("begline", true);
      FBegNode->AV["uid"] = ANode->NewUID();
     }
   }
  else if (ANode->AV["value"].Trim().Length())
   {
    if (!ANode->CmpAV("name", "sql"))
     ANode->AV["value"] = ANode->AV["value"].Trim();
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::Button1Click(TObject * Sender)
 {
  UnicodeString RC = "";
  TTagNode * def = new TTagNode;
  // TdsSrvClassif *FSQLs;
  try
   {
    def->LoadFromXMLFile("D:\\work\\XML\\�����������\\12063611-00008cd7-cd89.xml");
    // def->LoadFromXMLFile("D:\\work\\imm\\icv7\\tr\\12063611-00008cd7-cd89.old.xml");
    CondName = new TStringList;
    TTagNode * CondList = def->GetChildByName("conditionlist");
    CondList->Iterate1st(OldSchVal);
    // CondList->Iterate(TrimVal);
    def->Encoding = "utf-8";
    def->SaveToXMLFile("D:\\work\\XML\\�����������\\12063611-00008cd7-cd89.xml.new");
    CondName->SaveToFile("D:\\work\\XML\\�����������\\12063611-00008cd7-cd89.xml.cond");
    // def->SaveToXMLFile("D:\\work\\imm\\icv7\\tr\\12063611-00008cd7-cd89.old.xml.new");
   }
  __finally
   {
    delete def;
    // delete FSQLs;
   }
  ShowMessage("��");
 }
// ---------------------------------------------------------------------------
bool __fastcall TMainForm::CheckCond(TTagNode * ANode)
 {
  if (ANode->CmpName("condrules"))
   {
    TTagNode * jump = ANode->GetRoot()->GetChildByAV("jump", "ref", ANode->AV["uid"], true);
    if (!jump)
     ANode->AV["depricated"] = "true";
   }
  return false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TMainForm::TrimVal(TTagNode * ANode)
 {
  if (ANode->CmpName("text") && !ANode->CmpAV("name", "sql"))
   {
    ANode->AV["value"] = ANode->AV["value"].Trim();
   }
  return false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TMainForm::OldSchVal(TTagNode * ANode)
 {
  if (ANode->CmpName("condrules"))
   {
    TTagNode * jump = ANode->GetChildByAV("IERef", "ref", "0E291426-00005882-2493.0079", true);
    if (jump)
     CondName->Add(ANode->AV["name"]);
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::Button3Click(TObject * Sender)
 {
  UnicodeString FN = "";
  TTagNode * def = new TTagNode;
  // TdsSrvClassif *FSQLs;
  try
   {
    FN = "D:\\work\\imm\\icv7\\doc\\3 ������� ��������_��� ������ � 7-��.zxml";
    def->LoadFromZIPXMLFile(FN);
    def->Iterate(TrimVal);
    def->Encoding = "utf-8";
    def->SaveToZIPXMLFile(FN + ".new.zxml");
    FN = "D:\\work\\imm\\icv7\\doc\\3_����.������� � �-5.zxml";
    def->LoadFromZIPXMLFile(FN);
    def->Iterate(TrimVal);
    def->Encoding = "utf-8";
    def->SaveToZIPXMLFile(FN + ".new.zxml");
    FN = "D:\\work\\imm\\icv7\\doc\\������ ����� ������.2017_����.�5 � ������_15 ��������.zxml";
    def->LoadFromZIPXMLFile(FN);
    def->Iterate(TrimVal);
    def->Encoding = "utf-8";
    def->SaveToZIPXMLFile(FN + ".new.zxml");
    FN = "D:\\work\\imm\\icv7\\doc\\��������_�����.zxml";
    def->LoadFromZIPXMLFile(FN);
    def->Iterate(TrimVal);
    def->Encoding = "utf-8";
    def->SaveToZIPXMLFile(FN + ".new.zxml");
   }
  __finally
   {
    delete def;
    // delete FSQLs;
   }
  ShowMessage("��");
 }
// ---------------------------------------------------------------------------
void __fastcall TMainForm::FormShow(TObject * Sender)
 {
  ButtonStartClick(ButtonStart);
 }
// ---------------------------------------------------------------------------
