
//---------------------------------------------------------------------------

#ifndef MainAppH
#define MainAppH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <AppEvnts.hpp>
#include <IdHTTPWebBrokerBridge.hpp>
//---------------------------------------------------------------------------
#include "XMLContainer.h"
//---------------------------------------------------------------------------
class TAppBase;
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
  TApplicationEvents *ApplicationEvents1;
  TButton *ButtonStop;
  TButton *ButtonStart;
 TEdit *PortED;
 TButton *SchBegLineBtn;
 TButton *Button1;
 TButton *Button3;
 TButton *Button2;
  void __fastcall ButtonStartClick(TObject *Sender);
  void __fastcall ButtonStopClick(TObject *Sender);
  void __fastcall ApplicationEvents1Idle(TObject *Sender, bool &Done);
 void __fastcall FormDestroy(TObject *Sender);
 void __fastcall SchBegLineBtnClick(TObject *Sender);
 void __fastcall Button1Click(TObject *Sender);
 void __fastcall Button3Click(TObject *Sender);
 void __fastcall FormShow(TObject *Sender);


private:	// User declarations
  TAppBase* FServer;
  bool __fastcall GetNode(TTagNode* ANode);
  bool __fastcall CheckCond(TTagNode* ANode);
  bool __fastcall TrimVal(TTagNode* ANode);
  bool __fastcall OldSchVal(TTagNode* ANode);
  TStringList *CondName;
public:		// User declarations
  __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif


