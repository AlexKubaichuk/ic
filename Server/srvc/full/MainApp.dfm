object MainForm: TMainForm
  Left = 271
  Top = 114
  Caption = #1057#1077#1088#1074#1077#1088' "'#1059#1048'-'#1051#1055#1059'"'
  ClientHeight = 176
  ClientWidth = 236
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ButtonStart: TButton
    Left = 24
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 0
    OnClick = ButtonStartClick
  end
  object ButtonStop: TButton
    Left = 105
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Stop'
    TabOrder = 1
    OnClick = ButtonStopClick
  end
  object PortED: TEdit
    Left = 24
    Top = 57
    Width = 156
    Height = 21
    BevelInner = bvNone
    BevelOuter = bvNone
    Color = clCream
    TabOrder = 2
  end
  object SchBegLineBtn: TButton
    Left = 24
    Top = 84
    Width = 75
    Height = 25
    Caption = 'SchBegLine'
    TabOrder = 3
    OnClick = SchBegLineBtnClick
  end
  object Button1: TButton
    Left = 118
    Top = 84
    Width = 75
    Height = 25
    Caption = 'SchCondrules'
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button3: TButton
    Left = 23
    Top = 113
    Width = 75
    Height = 25
    Caption = 'TrimVal'
    TabOrder = 5
    OnClick = Button3Click
  end
  object Button2: TButton
    Left = 118
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 6
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 7
    Top = 2
  end
end
