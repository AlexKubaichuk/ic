object AppBase: TAppBase
  OldCreateOrder = False
  Height = 495
  Width = 678
  object ICServer: TDSServer
    OnError = ICServerError
    AutoStart = False
    Left = 60
    Top = 10
  end
  object DSAuthentication: TDSAuthenticationManager
    OnUserAuthenticate = DSAuthenticationUserAuthenticate
    OnUserAuthorize = DSAuthenticationUserAuthorize
    Roles = <
      item
        AuthorizedRoles.Strings = (
          'reglist'
          'regclass'
          'setting'
          'doc')
        ApplyTo.Strings = (
          'admin')
      end
      item
        AuthorizedRoles.Strings = (
          'reglist'
          'regclass'
          'doc')
        ApplyTo.Strings = (
          'user')
      end>
    Left = 55
    Top = 79
  end
  object dsICClassClass: TDSServerClass
    OnGetClass = dsICClassClassGetClass
    OnCreateInstance = dsICClassClassCreateInstance
    OnDestroyInstance = dsICClassClassDestroyInstance
    Server = ICServer
    Left = 185
    Top = 15
  end
  object dsDocClass: TDSServerClass
    OnGetClass = dsDocClassGetClass
    OnCreateInstance = dsDocClassCreateInstance
    OnDestroyInstance = dsDocClassDestroyInstance
    Server = ICServer
    Left = 289
    Top = 15
  end
  object dsKLADRClass: TDSServerClass
    OnGetClass = dsKLADRClassGetClass
    OnCreateInstance = dsKLADRClassCreateInstance
    OnDestroyInstance = dsKLADRClassDestroyInstance
    Left = 185
    Top = 94
  end
  object dsOrgClass: TDSServerClass
    OnGetClass = dsOrgClassGetClass
    OnCreateInstance = dsOrgClassCreateInstance
    OnDestroyInstance = dsOrgClassDestroyInstance
    Left = 290
    Top = 94
  end
  object dsVacStoreClass: TDSServerClass
    OnGetClass = dsVacStoreClassGetClass
    OnCreateInstance = dsVacStoreClassCreateInstance
    OnDestroyInstance = dsVacStoreClassDestroyInstance
    Server = ICServer
    Left = 386
    Top = 17
  end
  object SrvOpt: TAppOptions
    OnLoadXML = SrvOptLoadXML
    DataProvider = SrvOptXMLData
    Left = 507
    Top = 22
  end
  object SrvOptXMLData: TAppOptXMLProv
    Left = 507
    Top = 87
  end
  object SSLHandle: TIdServerIOHandlerSSLOpenSSL
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    OnGetPassword = SSLHandleGetPassword
    Left = 53
    Top = 226
  end
  object dsEIDataClass: TDSServerClass
    OnGetClass = dsEIDataClassGetClass
    OnCreateInstance = dsEIDataClassCreateInstance
    OnDestroyInstance = dsEIDataClassDestroyInstance
    Server = ICServer
    Left = 190
    Top = 164
  end
  object DSAdminClass: TDSServerClass
    OnGetClass = DSAdminClassGetClass
    OnCreateInstance = DSAdminClassCreateInstance
    OnDestroyInstance = DSAdminClassDestroyInstance
    Server = ICServer
    LifeCycle = 'Server'
    Left = 182
    Top = 230
  end
  object ClearTimer: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = ClearTimerTimer
    Left = 573
    Top = 215
  end
  object MISAPIClass: TDSServerClass
    OnGetClass = MISAPIClassGetClass
    OnCreateInstance = MISAPIClassCreateInstance
    OnDestroyInstance = MISAPIClassDestroyInstance
    Server = ICServer
    LifeCycle = 'Server'
    Left = 182
    Top = 311
  end
  object DSTCPServerTransport: TDSTCPServerTransport
    Port = 5101
    Server = ICServer
    Filters = <>
    AuthenticationManager = DSAuthentication
    OnConnect = DSTCPServerTransportConnect
    OnDisconnect = DSTCPServerTransportDisconnect
    Left = 51
    Top = 306
  end
  object DBCheckClass: TDSServerClass
    OnGetClass = DBCheckClassGetClass
    Server = ICServer
    LifeCycle = 'Invocation'
    Left = 302
    Top = 230
  end
end
