//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include "icApp.h"


//---------------------------------------------------------------------------
USEFORM("..\..\srvc\full\MainApp.cpp", MainForm);
//---------------------------------------------------------------------------
int AppWinMain()
{
	try
	{
		Application->Initialize();
		Application->MainFormOnTaskBar = true;
  Application->CreateForm(__classid(TMainForm), &MainForm);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Sysutils::ShowException(&exception, System::ExceptAddr());
	}
		catch(...)
		{
		try
		{
				throw Exception("");
		}
		catch(Exception &exception)
		{
			Sysutils::ShowException(&exception, System::ExceptAddr());
		}
		}
	return 0;
}
