﻿// ----------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include <stdio.h>
#include <memory>
#include <Datasnap.DSHTTP.hpp>
// ---------------------------------------------------------------------------
#include "appbaseunit.h"
#include "icsLog.h"
#include "srvsetting.h"
#include <WebReq.hpp>
#pragma link "Web.WebReq"
#ifdef USEPACKAGES
#pragma link "IndySystem.bpi"
#pragma link "IndyCore.bpi"
#pragma link "IndyProtocols.bpi"
#else
#pragma comment(lib, "IndySystem")
#pragma comment(lib, "IndyCore")
#pragma comment(lib, "IndyProtocols")
#endif
#pragma link "IdHTTPWebBrokerBridge"
// ---------------------------------------------------------------------------
// #include "dsRegUnitsUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma resource "*.dfm"
// ---------------------------------------------------------------------------
TDSServer * FDSServer;
TDSAuthenticationManager * FDSAuthenticationManager;
// ---------------------------------------------------------------------------
extern PACKAGE TComponentClass WebModuleClass;
// ---------------------------------------------------------------------------
TAppBase * __fastcall TAppBase::AppServer()
 {
  static TAppBase * RC = new TAppBase();
  return RC;
 }
// ---------------------------------------------------------------------------
__fastcall TAppBase::TAppBase() : TDataModule(NULL)
 {
  CreateLogs();
  FAdmUser   = "";
  FAdmPasswd = "";
  FCPath     = icsPrePath(ExtractFilePath(ParamStr(0)));
  if (WebRequestHandler() != NULL)
   {
    WebRequestHandler()->WebModuleClass = WebModuleClass;
   }
  FServer = new TIdHTTPWebBrokerBridge(NULL);
  FPort       = _SERVER_PORT_;
  FAdminClass = NULL;
  MKB         = NULL;

 }
// ---------------------------------------------------------------------------
__fastcall TAppBase::~TAppBase(void)
 {
  delete FAdminClass;
  FAdminClass = NULL;
  Stop();
  Disconnect();
  FDSServer                = NULL;
  FDSAuthenticationManager = NULL;
  DeleteLogs();
  delete FServer;
  FServer = NULL;
  delete MKB;
  MKB = NULL;
 }
// ----------------------------------------------------------------------------
TdsAdminClass * __fastcall TAppBase::AdmClass()
 {
  return ((TdsAdminClass *)FAdminClass);
 }
// ----------------------------------------------------------------------------
void __fastcall TAppBase::DSAuthenticationUserAuthenticate(TObject * Sender, const UnicodeString Protocol,
  const UnicodeString Context, const UnicodeString User, const UnicodeString Password, bool & valid,
  TStrings * UserRoles)
 {
  bool FValid = false;
  UnicodeString FUserName = User;
  valid = (FAdmUser == User) && (FAdmPasswd == Password);
  if (valid)
   FUserName = "admin";
  else
   {
    valid = AdmClass()->CheckUser(User, Password, FValid);
    if (valid)
     {
      valid = (valid == FValid);
     }
   }
  if (valid)
   {
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes)
     {
      UserRoles->Add("UPS=" + Password);
      dsLogAuth("[UserAuthenticate] успех: {" + IntToStr(FSes->Id) + "}" + Protocol + "/" + Context + " '" +
        FUserName + "'");
     }
    else
     dsLogAuth("[UserAuthenticate] успех: " + Protocol + "/" + Context + " '" + FUserName + "'");
   }
  else
   {
    if ((User == "check") && (Password == "check"))
     valid = true;
    else
     dsLogAuth("[UserAuthenticate] отказ: " + Protocol + "/" + Context + " '" + FUserName + "'");
   }
  if (valid)
   {
    AdmClass()->LoginUser("");
   }
 }
// ----------------------------------------------------------------------------
void __fastcall TAppBase::DSAuthenticationUserAuthorize(TObject * Sender,
  TDSAuthorizeEventObject * AuthorizeEventObject, bool & valid)
 {
  valid = false;
  TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
  if (FSes)
   {
    valid = (AuthorizeEventObject->MethodAlias == "DSAdmin.GetServerMethodParameters") ||
      (AuthorizeEventObject->MethodAlias == "DSAdmin.GetPlatformName") ||
      (AuthorizeEventObject->MethodAlias == "DSAdmin.DescribeMethod");
    if (!valid)
     {
      valid = ((AuthorizeEventObject->MethodAlias == "TdsAdminClass.LoginUser") ||
        (AuthorizeEventObject->MethodAlias == "TdsAdminClass.LoginSysUser") ||
        (AuthorizeEventObject->MethodAlias == "TdsDBCheckClass.CheckConnect"));
      if (!valid)
       valid = AdmClass()->CheckRT(FSes->UserRoles->Text, AuthorizeEventObject->MethodAlias);
     }
    if (!valid)
     {
      UnicodeString FUserName = AuthorizeEventObject->UserName;
      if (FUserName == FAdmUser)
       FUserName = "admin";
      dsLogAuth("отказ в доступе: {" + IntToStr(FSes->Id) + "." + AuthorizeEventObject->MethodAlias + "} '" +
        FUserName + "'");
     }
   }
 }
// ---------------------------------------------------------------------------
TDSServer * DSServer(void)
 {
  return FDSServer;
 }
// ---------------------------------------------------------------------------
TDSAuthenticationManager * DSAuthenticationManager(void)
 {
  return FDSAuthenticationManager;
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::Disconnect()
 {
  TClassUnitMap::iterator i;
  // TDocMap FDocMap;
  for (i = FDocMap.begin(); i != FDocMap.end(); i++)
   delete i->second;
  FDocMap.clear();
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::ICServerError(TDSErrorEventObject * DSErrorEventObject)
 {
  dsLogError(DSErrorEventObject->Error->Message, __FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::CheckBaseCount(int ACount)
 {
  TStringList * OpenSesList = new TStringList;
  TStringList * OpenBaseList = new TStringList;
  try
   { // ACount - "СССББ" - ССС - количество соединений, ББ - количество баз
    int c = (int)(ACount / 100);
    int b = ACount - c * 100;
    if (!c)
     throw EBaseAppException(Now().FormatString(" --- dd.mm.yyyy hh.nn.ss"));
    c++ ;
    b++ ;
    TDSSession * FSes;
    int idx, sc;
    sc = 0;
    TDSSessionManager::Instance->GetOpenSessionKeys(OpenSesList);
    for (int i = 0; i < OpenSesList->Count; i++)
     {
      // dsLogAuth(" 111: ");
      FSes = TDSSessionManager::Instance->Session[OpenSesList->Strings[i]];
      // dsLogAuth(" 222: " + IntToStr(FSes->Status) + " __ " + IntToStr(TDSSessionStatus::Active)+"\n"+FSes->UserRoles->Text);
      if (FSes->Status == TDSSessionStatus::Active)
       {
        // dsLogAuth(" FSes->Status == Active ");
        // dsLogAuth(FSes->UserRoles->Text);
        if (FSes->UserRoles->IndexOf("SYS") == -1)
         sc++ ;
        idx = OpenBaseList->IndexOf(FSes->UserRoles->Values["ICDB"].UpperCase());
        if (idx == -1)
         OpenBaseList->Add(FSes->UserRoles->Values["ICDB"].UpperCase());
       }
     }
    dsLogAuth(" {" + IntToStr(sc) + "/" + IntToStr(OpenBaseList->Count) + "}");
    if ((int)(sc / c))
     throw EBaseAppException(Now().FormatString(" -- dd.mm.yyyy hh.nn.ss"));
    if ((int)(OpenBaseList->Count / b))
     throw EBaseAppException(Now().FormatString(" - dd.mm.yyyy hh.nn.ss"));
   }
  __finally
   {
    delete OpenSesList;
    delete OpenBaseList;
   }
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                            TdsDocClass                                     #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsDocClassGetClass(TDSServerClass * DSServerClass, TPersistentClass & PersistentClass)
 {
  dsLogBegin(__FUNC__);
  PersistentClass = __classid(TdsDocClass);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
TDataModule * __fastcall TAppBase::CreateDocClass()
 {
  dsLogBegin(__FUNC__);
  dsLogEnd(__FUNC__);
  return new TdsDocClass(NULL, MKB, FXMLList, SrvOpt);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsDocClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassCreateInstance(DSCreateInstanceEventObject, FDocMap, CreateDocClass, "dsDocClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::dsDocClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  ClassDestroyInstance(DSDestroyInstanceEventObject, FDocMap, "dsDocClass");
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                            TdsAdminClass                                  #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
void __fastcall TAppBase::DSAdminClassGetClass(TDSServerClass * DSServerClass, TPersistentClass & PersistentClass)
 {
  dsLogBegin(__FUNC__);
  PersistentClass = __classid(TdsAdminClass);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::DSAdminClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  try
   {
    DSCreateInstanceEventObject->ServerClassInstance = FAdminClass;
   }
  catch (Sysutils::Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::DSAdminClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject)
 {
  dsLogBegin(__FUNC__);
  try
   {
    DSDestroyInstanceEventObject->ServerClassInstance = NULL;
   }
  catch (Sysutils::Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
////******************************************************************************
void __fastcall TAppBase::ClassCreateInstance(TDSCreateInstanceEventObject * DSCreateInstanceEventObject,
  TClassUnitMap & AUnitMap, TCreateModule ACreateNewInstance, const UnicodeString AClassName)
 {
  dsLogBegin(__FUNC__);
  try
   {
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes && (FSes->Status != TDSSessionStatus::Terminated))
     {
      FSes->LifeDuration = 28800000; // 8h //86400000; //24h
      UnicodeString FAuthUser = DSCreateInstanceEventObject->ServerConnectionHandler->ConProperties->Values
        ["DSAuthenticationUser"];
      if (FAuthUser.Length())
       {
        if (FSes->UserName.UpperCase() == FAuthUser.UpperCase())
         {
          UnicodeString FDB = FSes->UserRoles->Values["ICDB"];
          TClassUnitMap::iterator FRC = AUnitMap.find(FSes->SessionName /* Id */);
          TDataModule * tmpIC = NULL;
          if (FRC != AUnitMap.end())
           tmpIC = FRC->second;
          else
           {
            try
             {
              tmpIC = ACreateNewInstance();
              dsLogMessage("create " + AClassName + ": " + FSes->Id, __FUNC__, 5);
             }
            catch (Sysutils::Exception & E)
             {
              dsLogError(" in new : " + E.Message, __FUNC__, 5);
             }
           }
          if (tmpIC)
           {
            AUnitMap[FSes->SessionName /* Id */] = tmpIC;
            DSCreateInstanceEventObject->ServerClassInstance = tmpIC;
           }
         }
        else
         dsLogError("Имя пользователя отличается от имени пользователя сессии.", __FUNC__);
       }
      else
       dsLogError("Ошибка определения имени пользователя [" +
        DSCreateInstanceEventObject->ServerConnectionHandler->ConProperties->Properties->Text + "]", __FUNC__);
     }
   }
  catch (Sysutils::Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes)
     FClearSes(FSes->SessionName);
    DSCreateInstanceEventObject->ServerClassInstance = NULL;
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::ClassDestroyInstance(TDSDestroyInstanceEventObject * DSDestroyInstanceEventObject,
  TClassUnitMap & AUnitMap, const UnicodeString AClassName)
 {
  dsLogBegin(__FUNC__);
  try
   {
    DSDestroyInstanceEventObject->ServerClassInstance = NULL;
   }
  catch (Sysutils::Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void TerminateThreads(void)
 {
  if (TDSSessionManager::Instance != NULL)
   {
    TDSSessionManager::Instance->TerminateAllSessions();
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TAppBase::FGetActive()
 {
  bool RC = false;
  try
   {
    if (FServer)
     RC = FServer->Active;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TAppBase::LoadOpt()
 {
  bool RC = false;
  try
   {
    try
     {
      UnicodeString FXMLPath = icsPrePath(ExtractFilePath(ParamStr(0)));
      UnicodeString FFN = icsPrePath(FXMLPath + "\\conf\\" + UnicodeString(_OPT_FILE_NAME_));
      if (!FileExists(FFN))
       {
        ForceDirectories(FXMLPath);
        TTagNode * tmp = new TTagNode;
        try
         {
          tmp->Encoding = "utf-8";
          tmp->Name     = "g";
          tmp->AsXML    = "<?xml version='1.0' encoding='utf-8'?>\
                            <g>                                    \
                             <i n='0020' default='localhost'/>     \
                             <i n='0021' default='3050'/>          \
                             <i n='0022' default='sysdba'/>        \
                             <i n='0023' default='masterkey'/>     \
                             <i n='0001' v='5100'/>                \
                             <i n='000C' v='0'/>                   \
                             <i n='000D' v=''/>                    \
                             <i n='000E' v=''/>                    \
                             <i n='000F' v=''/>                    \
                             <i n='0003' v='localhost'/>           \
                             <i n='0004' v='5100'/>                \
                             <i n='0006' v='localhost'/>           \
                             <i n='0007' v='5200'/>                \
                             <i n='0009' v='localhost'/>           \
                             <i n='000A' v='5100'/>                \
                             <i n='0010' v='0'/>                   \
                             <i n='0012' v='misAura'/>             \
                             <i n='0013' v=''/>                    \
                             <i n='0014' v=''/>                    \
                             <i n='0015' v='6'/>                   \
                             <i n='0019' v=''/>                    \
                             <i n='001A' v=''/>                    \
                            </g>";
          tmp->SaveToXMLFile(FFN, "");
         }
        __finally
         {
          delete tmp;
         }
       }
      SrvOptXMLData->FileName = FFN;
      SrvOpt->LoadXML();
      RC = true;
     }
    catch (Sysutils::Exception & E)
     {
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::Start()
 {
  if (!FServer->Active)
   {
    if (!FXMLList)
     FXMLList = new TAxeXMLContainer;
    FXMLList->OnExtGetXML = FLoadXML;
    dsLogMessage("Start()", __FUNC__);
    if (LoadOpt())
     {
      dsLogMessage(SrvOpt->Vals["fbserver"].AsStringDef("localhost"), __FUNC__);
      dsLogMessage(SrvOpt->Vals["fbport"].AsStringDef("3050"), __FUNC__);
      dsLogMessage(SrvOpt->Vals["fbuser"].AsStringDef("sysdba"), __FUNC__);
      dsLogMessage(SrvOpt->Vals["fbpass"].AsStringDef("masterkey"), __FUNC__);
      FPort = SrvOpt->Vals[_PORTNAME_].AsIntDef(_SERVER_PORT_);
      if (SrvOpt->Vals["usessl"].AsBool)
       {
        if (SrvOpt->Vals["RootCertFile"].AsString.Trim().Length())
         SSLHandle->SSLOptions->RootCertFile = icsPrePath(FCPath + "\\" + SrvOpt->Vals["RootCertFile"]);
        SSLHandle->SSLOptions->CertFile = icsPrePath(FCPath + "\\" + SrvOpt->Vals["CertFile"]);
        SSLHandle->SSLOptions->KeyFile  = icsPrePath(FCPath + "\\" + SrvOpt->Vals["KeyFile"]);
        SSLHandle->OnGetPassword        = SSLHandleGetPassword;
        FServer->IOHandler              = SSLHandle;
       }
      MsgLog->LogLevel = SrvOpt->Vals["logLevel"].AsIntDef(1);
     }
    FServer->Bindings->Clear();
    FServer->DefaultPort  = FPort;
    ICServer->HideDSAdmin = true;
    try
     {
      dsLogMessage("Server before Active", __FUNC__);
      FServer->Active = true;
     }
    catch (Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
    // ClearTimer->Enabled = true;
    try
     {
      FDSServer                = ICServer;
      FDSAuthenticationManager = DSAuthentication;
      FXMLList->GetXML("001D3500-00005882-DC28");
      FXMLList->GetXML("0E291426-00005882-2493");
      FXMLList->GetXML("12063611-00008cd7-cd89");
      FXMLList->GetXML("1232303C-0000A6C8-46DC");
      FXMLList->GetXML("4031DA5E-8AC52FB3-7D03");
      FXMLList->GetXML("40381E23-92155860-4448");
      FXMLList->GetXML("454A13CD-BE856CB1-E252");
      FXMLList->GetXML("549F0EBE-1B9B98A7-BD64");
      FXMLList->GetXML("569E1EB2-D0CFEF0F-BE9A");
      FXMLList->GetXML("57E3D087-D0CFEF0F-1A84");
      FXMLList->GetXML("5BCAE819-D0CFEF0F-0B87");
      FXMLList->GetXML("ICS_APP_OPT");
      FXMLList->GetXML("ICS_CLI_OPT");
      FXMLList->GetXML("ICS_DATA_DEF");
      FXMLList->GetXML("docsrvoptdef");
      FXMLList->GetXML("mainsrvoptdef");
      FXMLList->GetXML("MKB");
      FXMLList->GetXML("OKVD");
      FXMLList->GetXML("OKVED");
      FXMLList->GetXML("OKVED");
      FXMLList->GetXML("OKVP");
      FXMLList->GetXML("planSetting");
      FXMLList->GetXML("srvoptdef");
      FXMLList->GetXML("version");
      FXMLList->GetXML("vssrvoptdef");
      if (!MKB)
       MKB = new TdsMKB(FXMLList);
      ICServer->Start();
      if (!FAdminClass)
       FAdminClass = new TdsAdminClass(NULL, FXMLList, SrvOpt, false);
      AdmClass()->User = FAdmUser;
      AdmClass()->Pwd = FAdmPasswd;
      AdmClass()->OnSesClose = FClearSes;
     }
    catch (Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
    dsLogMessage("Server started", __FUNC__);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::Stop()
 {
  dsLogMessage("Server stoped", __FUNC__);
  if (TDSSessionManager::Instance != NULL)
   {
    TDSSessionManager::Instance->TerminateAllSessions();
   }
  FServer->Active = false;
  FServer->Bindings->Clear();
  ICServer->Stop();
  delete FXMLList;
  FXMLList                 = NULL;
  SSLHandle->OnGetPassword = NULL;
  FServer->IOHandler       = NULL;
 }
// ---------------------------------------------------------------------------
bool __fastcall TAppBase::FLoadXML(TTagNode * ItTag, UnicodeString & Src)
 {
  dsLogBegin(__FUNC__);
  dsLogMessage(Src, __FUNC__, 1);
  bool RC = false;
  try
   {
    UnicodeString xmlPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\" + Src + ".zxml");
    if (FileExists(xmlPath))
     {
      ItTag->LoadFromZIPXMLFile(xmlPath);
      RC = true;
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::SrvOptLoadXML(TTagNode * AOptNode)
 {
  UnicodeString FFN = icsPrePath(FCPath + "\\" + UnicodeString(_OPT_DEF_FILE_NAME_));
  if (FileExists(FFN))
   {
    AOptNode->LoadFromZIPXMLFile(FFN);
   }
  else
   dsLogError("Ошибка загрузки описания настроек '" + UnicodeString(_OPT_DEF_FILE_NAME_) + "'", __FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::SSLHandleGetPassword(UnicodeString & Password)
 {
  Password = SrvOpt->Vals["SSLKeyPass"].AsStringDef("").Trim();
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::FClearSes(UnicodeString ASesId)
 {
  TStringList * FSesList = new TStringList;
  dsLogBegin(__FUNC__);
  try
   {
    TClassUnitMap::iterator FRC;
    dsLogMessage(ASesId, "");
    FRC = FDocMap.find(ASesId);
    if (FRC != FDocMap.end())
     {
      delete FRC->second;
      FDocMap.erase(ASesId);
      dsLogMessage(ASesId, "FDocMap");
     }
    if (TDSSessionManager::Instance->Session[ASesId])
     {
      dsLogMessage(ASesId, "Instance->Session[ASesId]");
      TDSSessionManager::Instance->Session[ASesId]->Terminate();
      TDSSessionManager::Instance->RemoveSession(ASesId);
     }
   }
  __finally
   {
    delete FSesList;
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::ClearTimerTimer(TObject * Sender)
 { // очистка сессий
  TStringList * FSesList = new TStringList;
  dsLogBegin(__FUNC__);
  try
   {
    TDSSessionManager::Instance->GetOpenSessionKeys(FSesList);
    FSesList->Text = FSesList->Text.UpperCase();
    TClassUnitMap::iterator i;
    for (i = FDocMap.begin(); i != FDocMap.end(); i++)
     {
      if (FSesList->IndexOf(i->first.UpperCase()) == -1)
       {
        dsLogMessage(i->first, "FDocMap");
        delete i->second;
       }
     }
   }
  __finally
   {
    delete FSesList;
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsSyncLogMessage(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsSyncLogError(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  dsSyncLogSQL(ASQL, AFuncName, AExecTime, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogBegin(UnicodeString AFuncName)
 {
  dsLogBegin(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::MISOnLogEnd(UnicodeString AFuncName)
 {
  dsLogEnd(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::CreateLogs()
 {
  // err log
  if (ErrLog)
   delete ErrLog;
  ErrLog = new TLog("docerr");
  // msg log
  if (MsgLog)
   delete MsgLog;
  MsgLog           = new TLog("docmsg");
  MsgLog->LogLevel = 9;
  // auth log
  if (AuthLog)
   delete AuthLog;
  AuthLog = new TLog("docauth");
  // Planer Log
  // RequestLog
  if (RequestLog)
   delete RequestLog;
  RequestLog           = new TLog("docrequest");
  RequestLog->LogLevel = 9;
  // CostructLog
  if (CostructLog)
   delete CostructLog;
  CostructLog           = new TLog("doccostruct");
  CostructLog->LogLevel = 9;
 }
// ---------------------------------------------------------------------------
void __fastcall TAppBase::DeleteLogs()
 {
  // err log
  if (ErrLog)
   delete ErrLog;
  ErrLog = NULL;
  // msg log
  if (MsgLog)
   delete MsgLog;
  MsgLog = NULL;
  // auth log
  if (AuthLog)
   delete AuthLog;
  AuthLog = NULL;
  // RequestLog
  if (RequestLog)
   delete RequestLog;
  RequestLog = NULL;
  // CostructLog
  if (CostructLog)
   delete CostructLog;
  CostructLog = NULL;
 }
// ---------------------------------------------------------------------------
