//----------------------------------------------------------------------------

#ifndef appbaseunitH
#define appbaseunitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Datasnap.DSAuth.hpp>
#include <Datasnap.DSCommonServer.hpp>
#include <Datasnap.DSServer.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <Datasnap.DSTCPServerTransport.hpp>
#include <IPPeerServer.hpp>
#include <IdHTTPWebBrokerBridge.hpp>
#include "AppOptBaseProv.h"
#include "AppOptions.h"
#include "AppOptXMLProv.h"
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdServerIOHandler.hpp>
#include <IdSSL.hpp>
#include <IdSSLOpenSSL.hpp>
#include <Vcl.ExtCtrls.hpp>

//----------------------------------------------------------------------------
#include "dsMKB.h"
#include "dsKLADRUnit.h"
#include "dsOrgUnit.h"
#include "dsDocUnit.h"
#include "dsAdminUnit.h"
//----------------------------------------------------------------------------
class EBaseAppException : public System::Sysutils::Exception
{
    public:
    __fastcall EBaseAppException (const UnicodeString msg) : System::Sysutils::Exception (msg) { };
};
//---------------------------------------------------------------------------
class TAppBase : public TDataModule
{
__published:	// IDE-managed Components
  TDSServer *ICServer;
  TDSAuthenticationManager *DSAuthentication;
 TDSServerClass *dsDocClass;
 TAppOptions *SrvOpt;
 TAppOptXMLProv *SrvOptXMLData;
 TIdServerIOHandlerSSLOpenSSL *SSLHandle;
 TDSServerClass *DSAdminClass;
 TTimer *ClearTimer;
  void __fastcall DSAuthenticationUserAuthenticate(TObject *Sender, const UnicodeString Protocol,
          const UnicodeString Context, const UnicodeString User,
          const UnicodeString Password, bool &valid,  TStrings *UserRoles);
 void __fastcall ICServerError(TDSErrorEventObject *DSErrorEventObject);
 void __fastcall DSAuthenticationUserAuthorize(TObject *Sender, TDSAuthorizeEventObject *AuthorizeEventObject,
          bool &valid);
 void __fastcall dsDocClassCreateInstance(TDSCreateInstanceEventObject *DSCreateInstanceEventObject);
 void __fastcall dsDocClassDestroyInstance(TDSDestroyInstanceEventObject *DSDestroyInstanceEventObject);
 void __fastcall dsDocClassGetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);
 void __fastcall SrvOptLoadXML(TTagNode *AOptNode);
 void __fastcall SSLHandleGetPassword(UnicodeString &Password);
 void __fastcall DSAdminClassCreateInstance(TDSCreateInstanceEventObject *DSCreateInstanceEventObject);
 void __fastcall DSAdminClassDestroyInstance(TDSDestroyInstanceEventObject *DSDestroyInstanceEventObject);
 void __fastcall DSAdminClassGetClass(TDSServerClass *DSServerClass, TPersistentClass &PersistentClass);
 void __fastcall ClearTimerTimer(TObject *Sender);


private:	// User declarations
  typedef map<UnicodeString, TDataModule*> TClassUnitMap;
  typedef TDataModule* __fastcall (__closure *TCreateModule)();
  TAxeXMLContainer *FXMLList;

  TClassUnitMap FDocMap;

  TdsMKB *MKB;

  TIdHTTPWebBrokerBridge *FServer;

  int FPort;
  UnicodeString FCPath;
  UnicodeString FAdmUser, FAdmPasswd;

  TdsAdminClass* __fastcall AdmClass();
  bool __fastcall LoadOpt();
  void __fastcall Disconnect();
  bool __fastcall FGetActive();

  TDataModule* __fastcall CreateDocClass();
  TDataModule*   FSearchClass;
  TDataModule*   FAdminClass;

  void __fastcall FClearSes(UnicodeString ASesId);
  void __fastcall ClassDestroyInstance(TDSDestroyInstanceEventObject *DSDestroyInstanceEventObject, TClassUnitMap &AUnitMap, const UnicodeString AClassName);
  void __fastcall ClassCreateInstance(TDSCreateInstanceEventObject *DSCreateInstanceEventObject, TClassUnitMap &AUnitMap, TCreateModule ACreateNewInstance, const UnicodeString AClassName);

  void __fastcall MISOnLogMessage(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl = 1);
  void __fastcall MISOnLogError(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl = 1);
  void __fastcall MISOnLogSQL(UnicodeString  ASQL, UnicodeString  AFuncName, TTime AExecTime, int ALvl = 1);
  void __fastcall MISOnLogBegin(UnicodeString  AFuncName);
  void __fastcall MISOnLogEnd(UnicodeString  AFuncName);
  void __fastcall CheckBaseCount(int ACount);
  bool __fastcall FLoadXML(TTagNode * ItTag, UnicodeString & Src);

  void __fastcall CreateLogs();
  void __fastcall DeleteLogs();

public:		// User declarations
  __fastcall TAppBase();
  __fastcall ~TAppBase(void);

  static TAppBase* __fastcall AppServer();
  void __fastcall Start();
  void __fastcall Stop();
  __property int Port = {read=FPort};
  __property bool Active = {read=FGetActive};
};
TDSServer *DSServer(void);
TDSAuthenticationManager *DSAuthenticationManager(void);

#endif

