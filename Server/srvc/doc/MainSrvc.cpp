//---------------------------------------------------------------------------
#include "MainSrvc.h"
//---------------------------------------------------------------------------
#include "appbaseunit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TICDocServer *ICDocServer;
//---------------------------------------------------------------------------
__fastcall TICDocServer::TICDocServer(TComponent* Owner)
 : TService(Owner)
{
  FServer = TAppBase::AppServer();
}
//---------------------------------------------------------------------------
void __stdcall ServiceController(unsigned CtrlCode)
{
  ICDocServer->Controller(CtrlCode);
}
//---------------------------------------------------------------------------
TServiceController __fastcall TICDocServer::GetServiceController(void)
{
  return (TServiceController) ServiceController;
}
//---------------------------------------------------------------------------
bool __fastcall TICDocServer::DoContinue(void)
{
  bool doContinue = TService::DoContinue();
  FServer->Start();
  return doContinue;
}
//---------------------------------------------------------------------------
void __fastcall TICDocServer::DoInterrogate(void)
{
  TService::DoInterrogate();
}
//---------------------------------------------------------------------------
bool __fastcall TICDocServer::DoPause(void)
{
  FServer->Stop();
  return TService::DoPause();
}
//---------------------------------------------------------------------------
bool __fastcall TICDocServer::DoStop(void)
{
  FServer->Stop();
  LogMessage("ServiceStop", EVENTLOG_INFORMATION_TYPE);
  return TService::DoStop();
}
//---------------------------------------------------------------------------
void __fastcall TICDocServer::ServiceStart(TService *Sender, bool &Started)
{
  FServer->Start();
  LogMessage("ServiceStart", EVENTLOG_INFORMATION_TYPE);
}
//---------------------------------------------------------------------------
void __fastcall TICDocServer::ServiceDestroy(TObject *Sender)
{
  if (FServer) delete FServer;
}
//---------------------------------------------------------------------------
void __fastcall TICDocServer::ServiceAfterInstall(TService *Sender)
{
  SC_HANDLE schSCManager;
  SC_HANDLE schService;
  SERVICE_DESCRIPTION sd;
  LPTSTR szDesc = TEXT("Сервер документооборота");

  // Get a handle to the SCM database.

  schSCManager = OpenSCManager(
      NULL,                    // local computer
      NULL,                    // ServicesActive database
      SC_MANAGER_ALL_ACCESS);  // full access rights

  if (schSCManager)
   {
     // Get a handle to the service.

     schService = OpenService(
         schSCManager,            // SCM database
         Name.c_str(),               // name of service
         SERVICE_CHANGE_CONFIG);  // need change config access

     if (schService)
      {
        // Change the service description.

        sd.lpDescription = szDesc;

        if( !ChangeServiceConfig2(
            schService,                 // handle to service
            SERVICE_CONFIG_DESCRIPTION, // change: description
            &sd) )                      // new description
         {
            LogMessage("ChangeServiceConfig2 failed", EVENTLOG_ERROR_TYPE);
         }

        CloseServiceHandle(schService);
        CloseServiceHandle(schSCManager);
      }
     else
      {
         LogMessage("OpenService failed", EVENTLOG_ERROR_TYPE);
         CloseServiceHandle(schSCManager);
      }
   }
  else
   {
      LogMessage("OpenSCManager failed", EVENTLOG_ERROR_TYPE);
   }
}
//---------------------------------------------------------------------------


