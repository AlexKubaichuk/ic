object AppBase: TAppBase
  OldCreateOrder = False
  Height = 495
  Width = 678
  object ICServer: TDSServer
    OnError = ICServerError
    AutoStart = False
    Left = 60
    Top = 10
  end
  object DSAuthentication: TDSAuthenticationManager
    OnUserAuthenticate = DSAuthenticationUserAuthenticate
    OnUserAuthorize = DSAuthenticationUserAuthorize
    Roles = <
      item
        AuthorizedRoles.Strings = (
          'reglist'
          'regclass'
          'setting'
          'doc')
        ApplyTo.Strings = (
          'admin')
      end
      item
        AuthorizedRoles.Strings = (
          'reglist'
          'regclass'
          'doc')
        ApplyTo.Strings = (
          'user')
      end>
    Left = 55
    Top = 79
  end
  object dsDocClass: TDSServerClass
    OnGetClass = dsDocClassGetClass
    OnCreateInstance = dsDocClassCreateInstance
    OnDestroyInstance = dsDocClassDestroyInstance
    Server = ICServer
    Left = 289
    Top = 15
  end
  object SrvOpt: TAppOptions
    OnLoadXML = SrvOptLoadXML
    DataProvider = SrvOptXMLData
    Left = 507
    Top = 22
  end
  object SrvOptXMLData: TAppOptXMLProv
    Left = 507
    Top = 87
  end
  object SSLHandle: TIdServerIOHandlerSSLOpenSSL
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    OnGetPassword = SSLHandleGetPassword
    Left = 53
    Top = 226
  end
  object DSAdminClass: TDSServerClass
    OnGetClass = DSAdminClassGetClass
    OnCreateInstance = DSAdminClassCreateInstance
    OnDestroyInstance = DSAdminClassDestroyInstance
    Server = ICServer
    LifeCycle = 'Server'
    Left = 286
    Top = 158
  end
  object ClearTimer: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = ClearTimerTimer
    Left = 573
    Top = 215
  end
end
