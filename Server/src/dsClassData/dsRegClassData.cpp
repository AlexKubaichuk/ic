// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsRegClassData.h"
#include "icsLog.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
__fastcall EdsRegClassDataError::EdsRegClassDataError(UnicodeString Msg) : Sysutils::Exception(Msg)
 {
 };
// ---------------------------------------------------------------------------
__fastcall TdsRegClassData::TdsRegClassData(TTagNode * ARoot, TFDQuery * AQuery) : TdsRegBaseClassData()
 {
  dsLogC(__FUNC__);
  FRoot        = ARoot;
  FQuery       = AQuery;
  FOnGetExtSQL = NULL;
  dsLogC(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
__fastcall TdsRegClassData::~TdsRegClassData()
 {
  dsLogD(__FUNC__);
  dsLogD(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegClassData::GetClassSQL(UnicodeString AClsUID)
 {
  UnicodeString RC = "";
  try
   {
    TAnsiStrMap::iterator FRC = FSQLHash.find(AClsUID.UpperCase());
    if (FRC != FSQLHash.end())
     RC = FRC->second;
    else
     {
      TTagNode * clsNode = FRoot->GetTagByUID(AClsUID);
      if (!clsNode)
       throw EdsRegClassDataError("����������� �������� ��� �������������� uid='" + AClsUID + "'");
      else
       {
        RC                = FCreateClassSQL(clsNode);
        FSQLHash[AClsUID] = RC;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClassData::DoInitHash(UnicodeString AClsUID)
 {
  try
   {
    TTagNode * clsNode = FRoot->GetTagByUID(AClsUID);
    if (!clsNode)
     throw EdsRegClassDataError("����������� �������� ��� �������������� uid='" + AClsUID + "'");
    else
     {
      if (!GetClassSQL(AClsUID).Length())
       FSQLHash[AClsUID] = FCreateClassSQL(clsNode);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegClassData::FCreateClassSQL(TTagNode * clsNode)
 {
  UnicodeString RC = "";
  if (FOnGetExtSQL)
   {
    RC = FOnGetExtSQL(clsNode->AV["uid"]);
   }
  else
   {
    if (!clsNode)
     throw Exception("����������� �������� ��������������. ");
    if (clsNode->CmpName("class"))
     {
      UnicodeString FAlias = "C" + clsNode->AV["uid"];
      UnicodeString FKeyFL = GetKeyName(clsNode);
      int FChNum = 0;
      chBDInd = 0;
      UnicodeString FSelect, tmpSel;
      // if (clsNode->AV["tab"].Length())
      // FSelect = FAlias + "." + FKeyFL;
      // else
      // FSelect = FAlias + "." + FKeyFL;
      UnicodeString FSelFrom = GetTabName(clsNode) + " " + FAlias;
      try
       {
        TTagNode * itNode = clsNode->GetChildByName("fields", true)->GetFirstChild();
        TTagNode * flDef;
        UnicodeString FN, FullFN;
        while (itNode)
         {
          flDef = clsNode->GetTagByUID(itNode->AV["ref"]);
          if (flDef)
           {
            FN     = GetFlName(flDef);
            FullFN = FAlias + "." + FN;
            if (!flDef->CmpAV("comment", "key"))
             {
              if (FSelect.Length())
               FSelect += " ||' '|| ";
              tmpSel = FullFN;
              if (flDef->CmpName("choice"))
               {
                tmpSel = " (case " + FullFN;
                TTagNode * itChVal = flDef->GetFirstChild();
                while (itChVal)
                 {
                  if (itChVal->CmpName("choicevalue"))
                   tmpSel += " when " + itChVal->AV["value"] + " then '" + itChVal->AV["name"] + "'";
                  itChVal = itChVal->GetNext();
                 }
                tmpSel += " else '' end)";
               }
              else if (flDef->CmpName("choiceDB"))
               {
                TTagNode * refClDef = FRoot->GetTagByUID(flDef->AV["ref"]);
                // �������� �������� �������������� �� ������
                UnicodeString FRefClFrom = "";
                UnicodeString FRefClSelect = GetJoinClass(refClDef, FRefClFrom, FullFN);
                tmpSel = " (" + FRefClSelect + ") ";
                FSelFrom += FRefClFrom;
                chBDInd++ ;
               }
              FSelect += "case when " + FullFN + " is null then '' else " + tmpSel + " end ";
             }
           }
          itNode = itNode->GetNext();
         }
        RC = "Select distinct (" + FSelect + ") as STR_CLASS  From " + FSelFrom + " Where " + FAlias + "." + FKeyFL +
          "=:CL_CODE ";
        // RC = "Select ("+sel + ") as STR_CLASS " + xfrom ;
       }
      __finally
       {
       }
     }
   }
  // UnicodeString RC = "";
  // TList *flList  = new TList;
  // TList *clFrom = new TList;
  // try
  // {
  // if (FOnGetExtSQL)
  // {
  // RC = FOnGetExtSQL(clsNode->AV["uid"]);
  // }
  // else
  // {
  // UnicodeString ex = "";
  // UnicodeString sel = "";
  // flList->Clear();
  // Iterate(clsNode,getFieldDef,flList);
  // // ������������ ����� ��� RETURNS, SELECT, INTO
  // clFrom->Add(flList->Items[0]);
  // UnicodeString _uid = ((TTagNode*)flList->Items[0])->AV["uid"];
  // TTagNode *_i,*n;
  // UnicodeString _UID_,_pUID_;
  // int xlen;
  // for (int s = 1; s < flList->Count; s++)
  // {
  // _i = (TTagNode*)flList->Items[s];    // 0
  // if (_i->CmpName("choiceDB")) clFrom->Add(_i); // ������ ���������������
  // }
  // // ������������ FROM
  // _i = (TTagNode*)clFrom->Items[0];   // 0
  // _UID_ = _i->AV["uid"];               // 1
  // UnicodeString xfrom = " FROM CLASS_"+_UID_+" CL"+_UID_;
  // if (clFrom->Count == 1)
  // xfrom += " WHERE CL"+_UID_+".CODE=:CL_CODE ";
  // else
  // {
  // for (int j = 1; j < clFrom->Count; j++)
  // {
  // _i = (TTagNode*)clFrom->Items[j];    // 0
  // _UID_ = _i->AV["uid"];               // 1
  // _pUID_ = _i->GetParent()->AV["uid"]; // 3
  // if (j == 1)
  // xfrom +=      " JOIN CLASS_"+_UID_+" CL"+_UID_+" on (CL"+_UID_+".CODE=CL"+_pUID_+".R"+_UID_+" AND CL"+_pUID_+".CODE=:CL_CODE) ";
  // else
  // xfrom += " LEFT JOIN CLASS_"+_UID_+" CL"+_UID_+" on (CL"+_UID_+".CODE=CL"+_pUID_+".R"+_UID_+") ";
  // }
  // }
  // // ������������ STR_CLASS - ��������� ������������� ��������������
  // TTagNode *x;
  // TTagNode *ch;
  // TTagNode *xCl;
  // UnicodeString xFlName, xExtName;
  // for (int j = clFrom->Count-1; j >= 0; j--)
  // {
  // xCl = (TTagNode*)clFrom->Items[j];
  // _UID_ = xCl->AV["uid"];               // 1
  // _i = xCl->GetChildByName("description");
  // _i = _i->GetFirstChild()->GetFirstChild();
  // while (_i)
  // {
  // n = _i->GetTagByUID(_i->AV["ref"]);
  // if (n)
  // {
  // xFlName = "CL"+_UID_+".R"+n->AV["uid"];
  //
  // xExtName = (FUseName)? UnicodeString(n->AV["name"]+" "):UnicodeString("");
  // if (sel.Length()) sel += "||";
  //
  // sel += "(Case WHEN "+xFlName+" IS NULL THEN '' ";
  // if (n->CmpName("text"))
  // sel += "else (' "+xExtName+"'||"+xFlName+") ";
  // else if (n->CmpName("choice"))
  // {
  // ch = _i->GetFirstChild();
  // UnicodeString _Name,_Val;
  // while (ch)
  // {
  // if (ch->CmpName("choicevalue"))
  // sel += "WHEN "+xFlName+"="+ch->AV["value"]+" THEN ' "+xExtName+ch->AV["name"]+"' ";
  // ch = ch->GetNext();
  // }
  // }
  // else
  // sel += "else (' "+xExtName+"'||CAST("+xFlName+" AS VARCHAR(10))) ";
  // sel += " END) ";
  // }
  // _i = _i->GetNext();
  // }
  // }
  // RC = "Select ("+sel + ") as STR_CLASS " + xfrom ;
  // }
  // }
  // __finally
  // {
  // delete flList;
  // delete clFrom;
  // }
  return RC;
 }
// ---------------------------------------------------------------------------
// UnicodeString __fastcall DoGetData(UnicodeString AClsUID, UnicodeString ACode);
UnicodeString __fastcall TdsRegClassData::DoGetData(UnicodeString AClsUID, UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString FSQL = GetClassSQL(AClsUID);
    if (FSQL.Length())
     {
      FQuery->Close();
      if (!FQuery->Transaction->Active)
       FQuery->Transaction->StartTransaction();
      FQuery->SQL->Text = FSQL;
      FQuery->ParamByName("CL_CODE")->AsString = ACode;
      FQuery->OpenOrExecute();
      RC = FQuery->FieldByName("STR_CLASS")->AsString.Trim();
     }
   }
  __finally
   {
    if (FQuery->Transaction->Active)
     FQuery->Transaction->Commit();
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegClassData::Iterate(TTagNode * ASrcNode, TSLIterate ItItem, TList * ADest)
 {
  if (!ASrcNode)
   throw ETagNodeError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  TTagNode * SaveThis = ASrcNode;
  if (ItItem(ASrcNode, ADest))
   return true;
  if (SaveThis != ASrcNode)
   return false;
  TTagNode * ItNode = ASrcNode->GetFirstChild();
  while (ItNode)
   {
    SaveThis = ItNode->GetNext();
    if (Iterate(ItNode, ItItem, ADest))
     return true;
    ItNode = SaveThis;
   }
  return false;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegClassData::GetJoinClass(TTagNode * AClDef, UnicodeString & AFrom, UnicodeString AFN)
 {
  UnicodeString RC = "";
  if (AClDef->CmpName("class"))
   {
    TTagNode * FFlDef;
    try
     {
      // �������� ������ ���� �� ������ ������������ �����
      TTagNode * lDef = AClDef->GetChildByName("description")->GetFirstChild();
      TTagNode * itNode = lDef->GetFirstChild();
      UnicodeString FClTabName = GetTabName(AClDef);
      UnicodeString FClTabAlias = "Cl" + IntToStr(chBDInd) + AClDef->AV["uid"];
      UnicodeString pre, post;
      UnicodeString FFN = "";
      UnicodeString tmpFN = "";
      // ������������ ������ ����� ��� SELECT-�
      while (itNode)
       {
        FFlDef = FRoot->GetTagByUID(itNode->AV["ref"]);
        FFN    = FClTabAlias + "." + GetFlName(FFlDef);
        tmpFN  = FFN;
        if (FFlDef->CmpName("choice"))
         {
          tmpFN = " (case " + FFN;
          TTagNode * itChVal = FFlDef->GetFirstChild();
          while (itChVal)
           {
            if (itChVal->CmpName("choicevalue"))
             tmpFN += " when " + itChVal->AV["value"] + " then '" + itChVal->AV["name"] + "'";
            itChVal = itChVal->GetNext();
           }
          tmpFN += " else '' end)";
         }
        else if (FFlDef->CmpName("choiceDB"))
         {
          TTagNode * refClDef = FRoot->GetTagByUID(FFlDef->AV["ref"]); // �������� �������� �������������� �� ������
          UnicodeString FRefClFrom = "";
          UnicodeString FRefClSelect = GetJoinClass(refClDef, FRefClFrom, FFN);
          tmpFN = " " + FRefClSelect + " ";
          AFrom += FRefClFrom;
          chBDInd++ ;
         }
        pre = post = "";
        if (itNode->AV["pre"].Length())
         pre = "'" + itNode->AV["pre"] + " '||";
        if (itNode->AV["post"].Length())
         post = "||' " + itNode->AV["post"] + "'";
        if (lDef->Count > 1)
         {
          if (RC.Length())
           RC += " ||' '|| case when " + tmpFN + " is null then '' else " + pre + tmpFN + post + " end ";
          else
           RC += " case when " + tmpFN + " is null then '' else " + pre + tmpFN + post + " end ";
         }
        else
         {
          if (RC.Length())
           RC += " ||' '|| " + pre + tmpFN + post;
          else
           RC += pre + tmpFN + post;
         }
        itNode = itNode->GetNext();
       }
      if (AFrom.Length())
       AFrom = " left join ( " + FClTabName + " " + FClTabAlias + " " + AFrom + ")";
      else
       AFrom = " left join " + FClTabName + " " + FClTabAlias + "";
      AFrom += " on (" + FClTabAlias + "." + GetKeyName(AClDef) + " = " + AFN + ") ";
     }
    __finally
     {
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegClassData::GetKeyName(TTagNode * AClDef)
 {
  UnicodeString RC = "code";
  try
   {
    TTagNode * flDef = AClDef->GetChildByAV("", "comment", "key", true);
    if (flDef)
     {
      RC = flDef->AV["fl"];
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegClassData::GetTabName(TTagNode * ADefNode)
 {
  UnicodeString RC = "CLASS_" + ADefNode->AV["uid"];
  try
   {
    if (ADefNode->AV["tab"].Length())
     RC = ADefNode->AV["tab"];
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegClassData::GetFlName(TTagNode * ADefNode)
 {
  UnicodeString RC = "R" + ADefNode->AV["uid"];
  try
   {
    if (ADefNode->AV["fl"].Length())
     RC = ADefNode->AV["fl"];
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
