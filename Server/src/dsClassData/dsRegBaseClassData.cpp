//---------------------------------------------------------------------------
#include <vcl.h>

#pragma hdrstop

#include "dsRegBaseClassData.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
__fastcall EdsRegBaseClassDataError::EdsRegBaseClassDataError(UnicodeString Msg) : Sysutils::Exception(Msg) {};
//---------------------------------------------------------------------------
__fastcall TdsRegBaseClassData::TdsRegBaseClassData()
{
  FUseHash = false;
}
//---------------------------------------------------------------------------
__fastcall TdsRegBaseClassData::~TdsRegBaseClassData()
{
  for (TClassValuesMap::iterator i=FClsHash.begin(); i != FClsHash.end(); i++)
   delete i->second;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegBaseClassData::ClearHash(UnicodeString AClsUID)
{
  TAnsiStrMap *FHash;
  if (AClsUID.Length())
   {
     FHash = GetClassHash(AClsUID);
     if (FHash) FHash->clear();
   }
  else
   {
     for (TClassValuesMap::iterator i=FClsHash.begin(); i != FClsHash.end(); i++)
      i->second->clear();
   }
}
//---------------------------------------------------------------------------
TAnsiStrMap* __fastcall TdsRegBaseClassData::GetClassHash(UnicodeString AClsUID)
{
  TClassValuesMap::iterator RC = FClsHash.find(AClsUID.UpperCase());
  if (RC != FClsHash.end())
   return RC->second;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegBaseClassData::DoInitHash(UnicodeString AClsUID)
{
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegBaseClassData::DoGetData(UnicodeString AClsUID, UnicodeString ACode)
{
  return "";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegBaseClassData::GetData(UnicodeString AClsUID, UnicodeString ACode)
{
  UnicodeString RC = "";
  try
   {
     if (!FUseHash)
      {
        RC = DoGetData(AClsUID, ACode);
      }
     else
      {
        TAnsiStrMap* pClassHash = GetClassHash(AClsUID);
        if (!pClassHash)
         {
           pClassHash = new TAnsiStrMap;
           FClsHash[AClsUID.UpperCase()] = pClassHash;
           DoInitHash(AClsUID);
         }
        TAnsiStrMap::iterator FRC = pClassHash->find(ACode.UpperCase());
        if (FRC == pClassHash->end())
         {
           RC = DoGetData(AClsUID, ACode);
           if (RC.Length())
            (*pClassHash)[ACode.UpperCase()] = RC;
         }
        else
          RC = FRC->second;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

