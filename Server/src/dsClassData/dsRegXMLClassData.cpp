//---------------------------------------------------------------------------
#include <vcl.h>

#pragma hdrstop

#include "dsRegXMLClassData.h"
#include "icsLog.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
__fastcall TdsRegXMLClassData::TdsRegXMLClassData()
: TdsRegBaseClassData()
{
  dsLogC(__FUNC__);
  FOnGetXML = NULL;
  FUseHash = true;
  dsLogC(__FUNC__, true);
}
//---------------------------------------------------------------------------
__fastcall TdsRegXMLClassData::~TdsRegXMLClassData()
{
  dsLogD(__FUNC__);
  dsLogD(__FUNC__, true);
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegXMLClassData::FLoadData(TTagNode *ANode)
{
  if (ANode->CmpName("i"))
   {
     if (ANode->AV["e"].Length())
      (*tmpHash)["##"+ANode->AV["c"].UpperCase()] = ANode->AV["e"]+": "+ANode->AV["n"];
     else
      (*tmpHash)["##"+ANode->AV["c"].UpperCase()] = ANode->AV["n"];
   }
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegXMLClassData::DoInitHash(UnicodeString AClsUID)
{
  if (FOnGetXML)
   {
     TTagNode *FRoot = FOnGetXML(AClsUID);
     if (FRoot)
      {
        tmpHash = GetClassHash(AClsUID);
        if (tmpHash)
          FRoot->Iterate(FLoadData);
      }
   }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegXMLClassData::DoGetData(UnicodeString AClsUID, UnicodeString ACode)
{
  return "� � � � � �";
}
//---------------------------------------------------------------------------












