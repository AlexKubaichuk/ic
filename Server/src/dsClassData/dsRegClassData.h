//---------------------------------------------------------------------------

#ifndef dsRegClassDataH
#define dsRegClassDataH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FireDAC.Comp.Client.hpp>
#include "dsRegBaseClassData.h"
//---------------------------------------------------------------------------
typedef UnicodeString __fastcall (__closure *TRegClassDataExtSQLEvent)(UnicodeString AClsId);
typedef bool __fastcall (__closure *TSLIterate)(TTagNode *ANode, TList *Dest);
//---------------------------------------------------------------------------
class PACKAGE EdsRegClassDataError : public Exception
{
public:
     __fastcall EdsRegClassDataError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE TdsRegClassData : public TdsRegBaseClassData
{
private:
    TTagNode *FRoot;
    TFDQuery *FQuery;
    TAnsiStrMap FSQLHash;
    TRegClassDataExtSQLEvent  FOnGetExtSQL;
    int  chBDInd;

    UnicodeString __fastcall GetClassSQL(UnicodeString AClsUID);
    UnicodeString __fastcall FCreateClassSQL(TTagNode *clsNode);
    bool __fastcall Iterate(TTagNode *ASrcNode, TSLIterate ItItem, TList *ADest);

    void          __fastcall DoInitHash(UnicodeString AClsUID);
    UnicodeString __fastcall DoGetData(UnicodeString AClsUID, UnicodeString ACode);
    UnicodeString __fastcall GetJoinClass(TTagNode * AClDef, UnicodeString & AFrom, UnicodeString  AFN);
    UnicodeString __fastcall GetKeyName(TTagNode * AClDef);
    UnicodeString __fastcall GetTabName(TTagNode * ADefNode);
    UnicodeString __fastcall GetFlName(TTagNode * ADefNode);
public:

    __fastcall TdsRegClassData(TTagNode *ARoot, TFDQuery *AQuery);
    __fastcall ~TdsRegClassData();

    __property TRegClassDataExtSQLEvent  OnGetExtSQL = {read=FOnGetExtSQL, write=FOnGetExtSQL};

};
//---------------------------------------------------------------------------

#endif















