//---------------------------------------------------------------------------

#ifndef dsRegXMLClassDataH
#define dsRegXMLClassDataH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "dsRegBaseClassData.h"
//---------------------------------------------------------------------------
typedef TTagNode* __fastcall (__closure *TRegClassDataExtGetXMLEvent)(UnicodeString AClsId);
//---------------------------------------------------------------------------
class PACKAGE TdsRegXMLClassData : public TdsRegBaseClassData
{
private:
    TClassValuesMap FClsHash;
    TAnsiStrMap* tmpHash;
    TRegClassDataExtGetXMLEvent FOnGetXML;

    bool __fastcall FLoadData(TTagNode *ANode);

    void          __fastcall DoInitHash(UnicodeString AClsUID);
    UnicodeString __fastcall DoGetData(UnicodeString AClsUID, UnicodeString ACode);
public:

    __fastcall TdsRegXMLClassData();
    __fastcall ~TdsRegXMLClassData();

    __property TRegClassDataExtGetXMLEvent OnGetXML = {read = FOnGetXML, write=FOnGetXML};

};
//---------------------------------------------------------------------------

#endif
