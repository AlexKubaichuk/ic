//---------------------------------------------------------------------------

#ifndef dsRegBaseClassDataH
#define dsRegBaseClassDataH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "XMLContainer.h"
//---------------------------------------------------------------------------
//typedef map<UnicodeString,UnicodeString> TLongMap;
typedef map<UnicodeString, TAnsiStrMap*> TClassValuesMap;
//---------------------------------------------------------------------------
class PACKAGE EdsRegBaseClassDataError : public Exception
{
public:
     __fastcall EdsRegBaseClassDataError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE TdsRegBaseClassData : public TObject
{
protected:
    bool  FUseHash;
    bool  FUseName;
    TClassValuesMap FClsHash;

    TAnsiStrMap* __fastcall  GetClassHash(UnicodeString AClsUID);

    virtual void          __fastcall DoInitHash(UnicodeString AClsUID);
    virtual UnicodeString __fastcall DoGetData(UnicodeString AClsUID, UnicodeString ACode);

private:


public:
    __fastcall TdsRegBaseClassData();
    __fastcall ~TdsRegBaseClassData();

    void __fastcall ClearHash(UnicodeString AClsUID);
    UnicodeString __fastcall GetData(UnicodeString AClsUID, UnicodeString ACode);


    __property bool UseHash = {read=FUseHash, write=FUseHash};
    __property bool UseName = {read=FUseName, write=FUseName};

};
//---------------------------------------------------------------------------

#endif
