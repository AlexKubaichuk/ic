// ---------------------------------------------------------------------------
#pragma hdrstop
#include "dsRegCardEditors.h"
#include "JSONUtils.h"
#include "TimeChecker.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
__fastcall TdsRegCardEditors::TdsRegCardEditors(TdsRegDM * ADM)
 {
  FDM = ADM;
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    try
     {
      VacSch  = FDM->XMLList->GetXML("12063611-00008cd7-cd89");
      TestSch = FDM->XMLList->GetXML("001D3500-00005882-DC28");
      // FCommClsData = new TdsCommClassData(FQ);
     }
    catch (Exception & E)
     {
     }
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
   }
 }
// ---------------------------------------------------------------------------
__fastcall TdsRegCardEditors::~TdsRegCardEditors()
 {
  // delete FCommClsData;
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                      �����                                                 #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegCardEditors::ImportData(System::UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId, __int64 & AUnitCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  try
   {
    if (AId.UpperCase() == "1003")
     RC = ImportVac(citImport, AValue, ARecId, AUnitCode);
    else if (AId.UpperCase() == "102F")
     RC = ImportTest(citImport, AValue, ARecId, AUnitCode);
    else if (AId.UpperCase() == "1100")
     RC = ImportCheck(citImport, AValue, ARecId, AUnitCode);
    else if (AId.UpperCase() == "3030")
     RC = ImportOtvod(citImport, AValue, ARecId, AUnitCode);
   }
  __finally
   {
    dsRequestLogMessage(AValue->ToString(), "Import data card" + AId.Trim().UpperCase());
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegCardEditors::InsertData(System::UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId, __int64 & AUnitCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  try
   {
    if (AId.UpperCase() == "1003")
     RC = ImportVac(citInsert, AValue, ARecId, AUnitCode);
    else if (AId.UpperCase() == "102F")
     RC = ImportTest(citInsert, AValue, ARecId, AUnitCode);
    else if (AId.UpperCase() == "1100")
     RC = ImportCheck(citInsert, AValue, ARecId, AUnitCode);
    else if (AId.UpperCase() == "3030")
     RC = ImportOtvod(citInsert, AValue, ARecId, AUnitCode);
   }
  __finally
   {
    dsRequestLogMessage(AValue->ToString(), "Insert data card" + AId.Trim().UpperCase());
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegCardEditors::EditData(System::UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId, __int64 & AUnitCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    if (AId.UpperCase() == "1003")
     RC = ImportVac(citEdit, AValue, ARecId, AUnitCode);
    else if (AId.UpperCase() == "102F")
     RC = ImportTest(citEdit, AValue, ARecId, AUnitCode);
    else if (AId.UpperCase() == "1100")
     RC = ImportCheck(citEdit, AValue, ARecId, AUnitCode);
    else if (AId.UpperCase() == "3030")
     RC = ImportOtvod(citEdit, AValue, ARecId, AUnitCode);
   }
  __finally
   {
    dsRequestLogMessage(AValue->ToString(), "Edit data card" + AId.Trim().UpperCase());
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegCardEditors::DeleteData(System::UnicodeString AId, System::UnicodeString & ARecId,
  __int64 & AUnitCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    if (AId.UpperCase() == "1003")
     RC = DeleteVac(ARecId, AUnitCode);
    else if (AId.UpperCase() == "102F")
     RC = DeleteTest(ARecId, AUnitCode);
    else if (AId.UpperCase() == "1100")
     RC = DeleteCheck(ARecId, AUnitCode);
    else if (AId.UpperCase() == "3030")
     RC = DeleteOtvod(ARecId, AUnitCode);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                      ��������                                              #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegCardEditors::ImportVac(TCardImportType AImpType, TJSONObject * AValue,
  System::UnicodeString & ARecId, __int64 & AUnitCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  TFDQuery * FQ = FDM->CreateTempQuery();
  TFDQuery * InsQ = FDM->CreateTempQuery();
  UnicodeString SQL;
  // TTimeChecker *FTimeCheck = new TTimeChecker;
  try
   {
    // ���������� ��������
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    try
     {
      __int64 FCode = JSONToInt(AValue, "3183", -1);
      __int64 FUCode = JSONToInt(AValue, "017A");
      AUnitCode = FUCode;
      TDate FVacDate = JSONToDate(AValue, "1024");
      int FVacCode = JSONToInt(AValue, "1020", -1);
      // FTimeCheck->Check(" / 1");
      SQL = "Select CODE From CLASS_3003 Where R317A=:pUCODE and R3024=:pDate and R3020=:pVacCode";
      FDM->quExecParam(FQ, false, SQL, "pUCODE", FUCode, "", "pDate", FVacDate, "pVacCode", FVacCode);
      bool FErr = false;
      if (FQ->RecordCount)
       {
        FErr = (AImpType == citInsert) || ((AImpType == citEdit) && (FQ->FieldByName("CODE")->AsInteger != FCode));
        if (FErr)
         RC = "�������� ����������, ������� ��������� � ��������� ������.";
        else
         FCode = FQ->FieldByName("CODE")->AsInteger;
       }
      else if (AImpType != citEdit)
       FCode = -1;
      // FTimeCheck->Check(" 2");
      if (!FErr)
       {
        int FVacInf;
        UnicodeString FVacVar = JSONToString(AValue, "1021", "������");
        TStringList * FVacInfList = new TStringList;
        try
         {
          // "1021":"V1:3^016A;V1:4^016A;V1:5^016A",
          FVacInfList->Delimiter     = ';';
          FVacInfList->DelimitedText = FVacVar;
          for (int i = 0; (i < FVacInfList->Count) && !FErr; i++)
           {
            // V1:3^016A
            FVacInf = GetLPartB(GetRPartB(FVacInfList->Strings[i].UpperCase(), ':'), '^').ToIntDef(0);
            SQL     = "Select CODE, R3183 From CLASS_1003 Where R017A=:pUCODE and R1024=:pDate and R1075=:pInfCode";
            FDM->quExecParam(FQ, false, SQL, "pUCODE", FUCode, "", "pDate", FVacDate, "pInfCode", FVacInf);
            // FTimeCheck->Check(" 3");
            if (FQ->RecordCount)
             {
              FErr = (AImpType == citInsert) ||
                ((AImpType == citEdit) && (FQ->FieldByName("R3183")->AsInteger != FCode));
              if (FErr)
               RC = "�� ��������� ���� ���������� �������� �� �������� " + /* FCommClsData->InfName[ */ IntToStr
                 (FVacInf) /* ] */ + ", ������� ��������� � ��������� ������.";
              // else
              // FCode = FQ->FieldByName("R3183")->AsInteger;
             }
           }
          // for (int i = 0; i < 6-FVacInfList->Count; i++)
          // FTimeCheck->Check(" -");
         }
        __finally
         {
          delete FVacInfList;
         }
        // FTimeCheck->Check();
        if (!InsQ->Transaction->Active)
         InsQ->Transaction->StartTransaction();
        InsQ->SQL->Text =
          "UPDATE OR INSERT INTO class_3003 (code, cguid, R317A, R32AD, R3024, R3020, R3021, R3185, R3022, R3023, R3025, R3026, R3028, R30A8, R3027, R30AE, R30AF, R317F, R301F, R30B0, R3181, R30B1, R3182, R3155) values (:pcode, :pcguid, :pR317A, :pR32AD, :pR3024, :pR3020, :pR3021, :pR3185, :pR3022, :pR3023, :pR3025, :pR3026, :pR3028, :pR30A8, :pR3027, :pR30AE, :pR30AF, :pR317F, :pR301F, :pR30B0, :pR3181, :pR30B1, :pR3182, :pR3155) MATCHING (CODE)";
        InsQ->Prepare();
        if ((AImpType == citInsert) || (FCode == -1))
         FCode = GetNewCode("3003");
        UnicodeString FLastVar;
        TStringList * FVacVarList = new TStringList;
        try
         {
          // "1021":"V1:3^016A;V1:4^016A;V1:5^016A",
          FVacVarList->Delimiter     = ';';
          FVacVarList->DelimitedText = FVacVar;
          FVacVar                    = "";
          FLastVar                   = "";
          UnicodeString tmpVarVal;
          UnicodeString FVacSch, FVacType, CorrectVacSch;
          CorrectVacSch = "";
          UnicodeString FMIBP = JSONToString(AValue, "1020");
          for (int i = 0; i < FVacVarList->Count; i++)
           {
            FVacType = GetLPartB(FVacVarList->Strings[i].UpperCase(), ':');
            FVacSch  = GetRPartB(FVacVarList->Strings[i].UpperCase(), ':');
            FVacInf  = GetLPartB(FVacSch, '^').ToIntDef(0);
            FVacSch  = GetRPartB(FVacSch, '^');
            CheckVacSchCorrect(FMIBP, FVacSch, FVacType);
            // "V1:3^016A;V1:4^016A;V1:5^016A",
            FVacVarList->Strings[i] = FVacType + ":" + FVacInf + "^" + FVacSch;
            if (CorrectVacSch.Length())
             CorrectVacSch += ";" + FVacVarList->Strings[i];
            else
             CorrectVacSch = FVacVarList->Strings[i];
            tmpVarVal = GetLPartB(FVacVarList->Strings[i].UpperCase(), ':');
            if (!FLastVar.Length())
             {
              FLastVar = tmpVarVal;
              FVacVar  = FLastVar;
             }
            else
             {
              if (FLastVar != tmpVarVal)
               {
                FLastVar = tmpVarVal;
                FVacVar += ", " + FLastVar;
               }
             }
           }
          // FTimeCheck->Check(" 4");
          InsQ->ParamByName("pCode")->Value = FCode;
          if (AImpType != citEdit)
           InsQ->ParamByName("pcguid")->AsString = NewGUID();
          InsQ->ParamByName("pR317A")->Value = FUCode;
          InsQ->ParamByName("pR32AD")->Value = JSONToVar(AValue, "32AE");
          InsQ->ParamByName("pR3024")->Value = JSONToVar(AValue, "1024");
          // InsQ->ParamByName("pR30B4")->Value = JSONToVar(AValue, "10B4");
          InsQ->ParamByName("pR3020")->Value = JSONToVar(AValue, "1020");
          InsQ->ParamByName("pR3021")->Value = FVacVar;
          InsQ->ParamByName("pR3185")->Value = CorrectVacSch; // JSONToVar(AValue, "1021");
          InsQ->ParamByName("pR3022")->Value = JSONToVar(AValue, "1022");
          InsQ->ParamByName("pR3023")->Value = JSONToVar(AValue, "1023");
          InsQ->ParamByName("pR3025")->Value = JSONToVar(AValue, "1025");
          InsQ->ParamByName("pR3026")->Value = JSONToVar(AValue, "1026");
          InsQ->ParamByName("pR3028")->Value = JSONToVar(AValue, "1028");
          InsQ->ParamByName("pR30A8")->Value = JSONToVar(AValue, "10A8");
          InsQ->ParamByName("pR3027")->Value = JSONToVar(AValue, "1027");
          InsQ->ParamByName("pR30AE")->Value = JSONToVar(AValue, "10AE");
          InsQ->ParamByName("pR30AF")->Value = JSONToVar(AValue, "10AF");
          InsQ->ParamByName("pR317F")->Value = JSONToVar(AValue, "017F");
          InsQ->ParamByName("pR301F")->Value = JSONToVar(AValue, "101F");
          InsQ->ParamByName("pR30B0")->Value = JSONToVar(AValue, "10B0");
          InsQ->ParamByName("pR3181")->Value = JSONToVar(AValue, "0181");
          InsQ->ParamByName("pR30B1")->Value = JSONToVar(AValue, "10B1");
          InsQ->ParamByName("pR3182")->Value = JSONToVar(AValue, "0182");
          InsQ->ParamByName("pR3155")->Value = JSONToVar(AValue, "1155");
          InsQ->OpenOrExecute();
          // FTimeCheck->Check(" 5");
          // FTimeCheck->Check(" / UPDATE OR INSERT INTO class_3003");
          FDM->quExecParam(FQ, false, "Delete From CLASS_1003 Where R3183=:pR3183", "pR3183", FCode);
          // FTimeCheck->Check(" 6");
          // FTimeCheck->Check();
          FQ->SQL->Text =
            "UPDATE OR INSERT INTO CLASS_1003 (code, cguid, R017A, R3183, R32AE, R1024, R1020, R1021, R1075, R1022, R1023, R1025, R1026, R1028, R10A8, R1027, R10AE, R1092, R10AF, R017F, R101F, R10B0, R0181, R10B1, R0182, R1155) values (:pcode, :pcguid, :pR017A, :pR3183, :pR32AE, :pR1024, :pR1020, :pR1021, :pR1075, :pR1022, :pR1023, :pR1025, :pR1026, :pR1028, :pR10A8, :pR1027, :pR10AE, :pR1092, :pR10AF, :pR017F, :pR101F, :pR10B0, :pR0181, :pR10B1, :pR0182, :pR1155) MATCHING (CODE)";
          FQ->Prepare();
          // FTimeCheck->Check(" 7");
          FQ->ParamByName("pR017A")->Value = FUCode;
          FQ->ParamByName("pR3183")->Value = FCode;
          FQ->ParamByName("pR32AE")->Value = JSONToVar(AValue, "32AE");
          FQ->ParamByName("pR1024")->Value = JSONToVar(AValue, "1024");
          // FQ->ParamByName("pR10B4")->Value = JSONToVar(AValue, "10B4");
          FQ->ParamByName("pR1020")->Value = JSONToVar(AValue, "1020");
          // FQ->ParamByName("pR1021")->Value = JSONToVar(AValue, "1021");
          // FQ->ParamByName("pR1075")->Value = JSONToVar(AValue, "1075");
          FQ->ParamByName("pR1022")->Value = JSONToVar(AValue, "1022");
          FQ->ParamByName("pR1023")->Value = JSONToVar(AValue, "1023");
          FQ->ParamByName("pR1025")->Value = JSONToVar(AValue, "1025");
          FQ->ParamByName("pR1026")->Value = JSONToVar(AValue, "1026");
          FQ->ParamByName("pR1028")->Value = JSONToVar(AValue, "1028");
          FQ->ParamByName("pR10A8")->Value = JSONToVar(AValue, "10A8");
          FQ->ParamByName("pR1027")->Value = JSONToVar(AValue, "1027");
          FQ->ParamByName("pR10AE")->Value = JSONToVar(AValue, "10AE");
          // FQ->ParamByName("pR1092")->Value = JSONToVar(AValue, "1092");
          FQ->ParamByName("pR10AF")->Value = JSONToVar(AValue, "10AF");
          FQ->ParamByName("pR017F")->Value = JSONToVar(AValue, "017F");
          FQ->ParamByName("pR101F")->Value = JSONToVar(AValue, "101F");
          FQ->ParamByName("pR10B0")->Value = JSONToVar(AValue, "10B0");
          FQ->ParamByName("pR0181")->Value = JSONToVar(AValue, "0181");
          FQ->ParamByName("pR10B1")->Value = JSONToVar(AValue, "10B1");
          FQ->ParamByName("pR0182")->Value = JSONToVar(AValue, "0182");
          FQ->ParamByName("pR1155")->Value = JSONToVar(AValue, "1155");
          // UnicodeString FVacSch;
          // UnicodeString FMIBP = JSONToString(AValue, "1020");
          int FVacInf;
          ARecId = "";
          // FTimeCheck->Check(" 8");
          FCheckInfList.clear();
          for (int i = 0; i < FVacVarList->Count; i++)
           {
            // V1:3^016A
            FVacVar                = GetLPartB(FVacVarList->Strings[i].UpperCase(), ':');
            FVacSch                = GetRPartB(FVacVarList->Strings[i].UpperCase(), ':');
            FVacInf                = GetLPartB(FVacSch, '^').ToIntDef(0);
            FVacSch                = GetRPartB(FVacSch, '^');
            FCheckInfList[FVacInf] = FVacInf;
            // CheckVacSchCorrect(FMIBP, FVacSch, FVacVar);
            FQ->ParamByName("pCode")->Value = GetNewCode("1003");
            if (!ARecId.Length())
             ARecId = FQ->ParamByName("pCode")->AsString;
            FQ->ParamByName("pcguid")->AsString = NewGUID();
            FQ->ParamByName("pR1021")->Value = FVacVar;
            FQ->ParamByName("pR1075")->Value = FVacInf;
            FQ->ParamByName("pR1092")->Value = FVacSch;
            FQ->OpenOrExecute();
            if (FOnEdited)
             FOnEdited(FUCode, 1, FVacInf);
            // FTimeCheck->Check(" 9");
           }
          // for (int i = 0; i < 6-FVacVarList->Count; i++)
          // FTimeCheck->Check(" -");
          if (InsQ->Transaction->Active)
           InsQ->Transaction->Commit();
          // FTimeCheck->Check(" 10");
          if (FQ->Transaction->Active)
           FQ->Transaction->Commit();
          // FTimeCheck->Check(" 11");
          // FTimeCheck->Check(" / UPDATE OR INSERT INTO CLASS_1003 ["+IntToStr(FVacVarList->Count)+"]");
          CheckVacInPlan(FUCode, JSONToDate(AValue, "1024"), JSONToDate(AValue, "3315"), false);
          ClearHandSch(FUCode, true);
          // FTimeCheck->Check(" 12");
          // FTimeCheck->Check(" / CheckVacInPlan");
          RC = "ok";
          // dsLogMessage(FTimeCheck->GetStr(" >>> ������ �������� "), __FUNC__);
         }
        __finally
         {
          delete FVacVarList;
         }
       }
     }
    catch (Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
      if (FQ->Transaction->Active)
       FQ->Transaction->Rollback();
     }
   }
  __finally
   {
    // delete FTimeCheck;
    FDM->DeleteTempQuery(FQ);
    FDM->DeleteTempQuery(InsQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegCardEditors::DeleteVac(System::UnicodeString ARecId, __int64 & AUnitCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    try
     {
      FDM->quExecParam(FQ, false, "Select R32AE, R017A, R3183, R1024 From CLASS_1003 Where code=:pCode", "pCode",
      ARecId);
      if (FQ->RecordCount)
       {
        __int64 FPlanCode = FQ->FieldByName("R32AE")->AsInteger;
        __int64 FVacCode = FQ->FieldByName("R3183")->AsInteger;
        __int64 FUCode = FQ->FieldByName("R017A")->AsInteger;
        AUnitCode = FUCode;
        TDate FVacDate = FQ->FieldByName("R1024")->AsDateTime;
        FDM->quExecParam(FQ, false, "Delete  From CLASS_3003 Where code=:pCode", "pCode", FVacCode);
        FDM->quExecParam(FQ, false, "Delete  From CLASS_1003 Where R3183=:pCode", "pCode", FVacCode);
        if (FQ->Transaction->Active)
         FQ->Transaction->Commit();
        CheckVacInPlan(FUCode, FVacDate, 0, false);
        RC = "ok";
       }
     }
    catch (Exception & E)
     {
      if (FQ->Transaction->Active)
       FQ->Transaction->Rollback();
      RC = E.Message;
     }
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegCardEditors::CheckVacInPlan(__int64 AUCode, TDate AVacDate, TDate APlanDate, bool AFromOtvod)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  UnicodeString SQL;
  // TCheckCardItemMap FPrivMap;
  TCheckCardItemMap FPrivInfMap;
  TCheckCardItemMap FPlanMap;
  TCheckCardItemMap FPlanInfMap;
  TCheckCardItemMap::iterator FRC, itPriv, itInfPriv, itPlan, itInfPlan; // .                   12.10.18
  TDate HiPlanDate = TDate(IntToStr(DaysInMonth(Date())) + Date().FormatString(".mm.yyyy")); // 30.10.18
  if ((int)APlanDate)
   HiPlanDate = TDate(IntToStr(DaysInMonth(APlanDate)) + APlanDate.FormatString(".mm.yyyy")); // 30.10.18
  TDate HiDate = Date();
  if (AFromOtvod)
   HiPlanDate = TDate(IntToStr(DaysInMonth(AVacDate)) + AVacDate.FormatString(".mm.yyyy"));
  TDate LoPlanDate = TDate("01." + Sysutils::IncMonth(HiDate, -1).FormatString("mm.yyyy"));
  if ((int)APlanDate)
   LoPlanDate = TDate("01." + APlanDate.FormatString("mm.yyyy"));
  TDate LoDate = TDate("01." + HiDate.FormatString("mm.yyyy"));
  // dsLogMessage(LoDate.FormatString("dd.mm.yyyy") + " : " + AVacDate.FormatString("dd.mm.yyyy") + " : " + HiDate.FormatString("dd.mm.yyyy"), __FUNC__, 1);
  if ((AVacDate >= LoPlanDate) && (AVacDate <= HiPlanDate))
   {
    TFDQuery * FQ = FDM->CreateTempQuery();
    try
     {
      if (!FQ->Transaction->Active)
       FQ->Transaction->StartTransaction();
      try
       { // ������ �������� �� ��������� --------------------------------------
        SQL = "Select code, R1020, R32AE, R1075, R1021, R1024, R101F From CLASS_1003 Where R017A=:pUCode and R1024 <= :pHiDate and R1024 >= :pLoDate Order By R1024";
        FDM->quExecParam(FQ, false, SQL, "pUCode", AUCode, "", "pHiDate", HiDate, "pLoDate", LoDate);
        // ���� �������� LoDate HiDate
        while (!FQ->Eof)
         {
          if (FPrivInfMap.find(FQ->FieldByName("R1075")->AsInteger) == FPrivInfMap.end())
           FPrivInfMap[FQ->FieldByName("R1075")->AsInteger] = TCheckCardItem(FQ, 2);
          FQ->Next();
         }
        // ����� -------------------------------------------------------------
        // ������ ����
        SQL = "Select code, R3089, R328F, R32AB, R308B, R3088, R32FD, R3308, R32FC From CLASS_3084 Where R317D=:pUCode and R3088 <= :pHiDate and R3088 >= :pLoDate and R3086=1 Order By R3088";
        FDM->quExecParam(FQ, false, SQL, "pUCode", AUCode, "", "pHiDate", HiPlanDate, "pLoDate", LoPlanDate);
        while (!FQ->Eof)
         {
          if (FQ->FieldByName("R3089")->AsInteger) // ������ ����
           {
            if (FPlanMap.find(FQ->FieldByName("R3089")->AsInteger) == FPlanMap.end())
             FPlanMap[FQ->FieldByName("R3089")->AsInteger] = TCheckCardItem(FQ, 3);
           }
          else // ������� ��������
           {
            if (FPlanMap.find(-1 * FQ->FieldByName("R32AB")->AsInteger) == FPlanMap.end())
             FPlanMap[-1 * FQ->FieldByName("R32AB")->AsInteger] = TCheckCardItem(FQ, 3);
           }
          FQ->Next();
         }
        // ������ ���� �� ���������
        SQL = "Select code, R1089, R318F, R32A6, R108B, R1088, R3302, R3303, R3307 From CLASS_1084 Where R017D=:pUCode and R1088 <= :pHiDate and R1088 >= :pLoDate and R1086=1 Order By R1088";
        FDM->quExecParam(FQ, false, SQL, "pUCode", AUCode, "", "pHiDate", HiPlanDate, "pLoDate", LoPlanDate);
        while (!FQ->Eof)
         {
          if (FPlanInfMap.find(FQ->FieldByName("R32A6")->AsInteger) == FPlanInfMap.end())
           FPlanInfMap[FQ->FieldByName("R32A6")->AsInteger] = TCheckCardItem(FQ, 4);
          FQ->Next();
         }
        // -------------------------------
        // ���������, ��������
        for (itPriv = FPrivInfMap.begin(); itPriv != FPrivInfMap.end(); itPriv++)
         dsLogMessage("FPrivInfMap: " + itPriv->second.ToString(), __FUNC__, 5);
        for (itPriv = FPlanMap.begin(); itPriv != FPlanMap.end(); itPriv++)
         dsLogMessage("FPlanMap: " + itPriv->second.ToString(), __FUNC__, 5);
        for (itPriv = FPlanInfMap.begin(); itPriv != FPlanInfMap.end(); itPriv++)
         dsLogMessage("FPlanInfMap: " + itPriv->second.ToString(), __FUNC__, 5);
        // ########################################################################
        // ������� ����� �� ���������
        // ########################################################################
        dsLogMessage(" +������� ����� �� ��������� [" + IntToStr((int)FPlanInfMap.size()) + "]", __FUNC__, 5);
        Variant notExecType, notExecIll;
        bool FItemPresent;
        for (itInfPlan = FPlanInfMap.begin(); itInfPlan != FPlanInfMap.end(); itInfPlan++)
         {
          FRC          = FPrivInfMap.find(itInfPlan->second.Inf);
          FItemPresent = (FRC != FPrivInfMap.end());
          if (FItemPresent)
           { // �������� �����, ����� ���������� �������� ������ ������ ��� ����� ��������
            FItemPresent = (FRC->second.Date.FormatString("yyyymm").ToIntDef(0) >=
              itInfPlan->second.Date.FormatString("yyyymm").ToIntDef(0));
            // if (!FItemPresent)
            // FItemPresent = (FRC->second.Date.FormatString("yyyymm") = itInfPlan->second.Date.FormatString("yyyymm"));
            // TDate LoPlanDate = TDate("01." + Sysutils::IncMonth(HiDate, -1).FormatString("mm.yyyy"));
           }
          if (!FItemPresent)
           { // �������� �������� �� ��������� (���������� ������� ������������)
            notExecType = 1;
            notExecIll  = Variant::Empty();
            SQL = "Select R1066, R1082 From CLASS_1030 Where R017C=:pUCode and R1110=1 and R1083=:pInf and ((R1043=0 and R105F<=:pPrivPlanDate and R1065>:pPrivPlanDate)or(R1043=1 and R105F<=:pPrivPlanDate ))";
            FDM->quExecParam(FQ, true, SQL, "pUCode", AUCode, "", "pInf", itInfPlan->second.Inf, "pPrivPlanDate",
              itInfPlan->second.Date);
            if (FQ->RecordCount)
             { // ���� ���������� �������� �������� ��� �����
              dsLogMessage("����� [" + IntToStr(itInfPlan->second.Inf) + "] " + itInfPlan->second.Date.FormatString
                ("dd.mm.yyyy"));
              notExecType = FQ->FieldByName("R1066")->AsInteger;
              if (!FQ->FieldByName("R1066")->AsInteger)
               notExecIll = FQ->FieldByName("R1082")->AsString;
             }
            else
             dsLogMessage("��� ������ [" + IntToStr(itInfPlan->second.Inf) + "] " + itInfPlan->second.Date.FormatString
              ("dd.mm.yyyy"));
            SQL = "Update CLASS_1084 set R1098=0, R108D=null, R3305=null, R32AA=null, R3302=null, R3303=:pR3303, R3304=:pR3304 Where code=:pCode";
            FDM->quExecParam(FQ, true, SQL, "pCode", itInfPlan->second.Id, "", "pR3303", notExecType, "pR3304",
              notExecIll);
            itInfPlan->second.ExecMIBP = 0;
            itInfPlan->second.ExecDate = TDate(0);
            // itInfPlan->NoMIBP; // ����������� ����
            itInfPlan->second.NoExecType = notExecType; // ������� ������������
           }
          else
           {
            SQL = "Update CLASS_1084 set R1098=1, R108D=:pDate, R32AA=:pMIBP, R3305=:pOMIBP, R3302=:pInTLPU Where code=:pCode";
            FDM->quExecParam(FQ, true, SQL, "pCode", itInfPlan->second.Id, "", "pMIBP", FRC->second.MIBP, "pDate",
              FRC->second.Date, "pOMIBP", (FRC->second.MIBP == itInfPlan->second.MIBP), "pInTLPU",
              FRC->second.InThisLPU);
            itInfPlan->second.ExecMIBP   = FRC->second.MIBP;
            itInfPlan->second.ExecDate   = FRC->second.Date;
            itInfPlan->second.InThisLPU  = FRC->second.InThisLPU;
            itInfPlan->second.NoExecType = 1; // ������� ������������
           }
         }
        // ########################################################################
        // ������� �����
        // ########################################################################
        dsLogMessage(" +������� �����", __FUNC__, 5);
        bool FCheck, MIBPEqu, FFullCheck;
        TDate FLastDate;
        int CheckType;
        int NotExecType;
        int nET;
        bool InThisLPU;
        for (itPlan = FPlanMap.begin(); itPlan != FPlanMap.end(); itPlan++)
         {
          FCheck      = false;
          FFullCheck  = true;
          MIBPEqu     = true;
          FLastDate   = TDate(0);
          InThisLPU   = false;
          NotExecType = 1; // ��� �������
          for (itInfPlan = FPlanInfMap.begin(); itInfPlan != FPlanInfMap.end(); itInfPlan++)
           {
            if (((itPlan->second.MIBP && (itInfPlan->second.MIBP == itPlan->second.MIBP)) ||
              (!itPlan->second.MIBP && (itInfPlan->second.Inf == itPlan->second.Inf))) &&
              (itInfPlan->second.Date == itPlan->second.Date))
             {
              InThisLPU |= itInfPlan->second.InThisLPU;
              FCheck |= itInfPlan->second.ExecMIBP;
              FFullCheck &= itInfPlan->second.ExecMIBP;
              MIBPEqu &= (itPlan->second.MIBP == itInfPlan->second.ExecMIBP);
              if (!(int)FLastDate && (int)itInfPlan->second.ExecDate)
               FLastDate = itInfPlan->second.ExecDate;
              if (NotExecType == 1)
               {
                nET = itInfPlan->second.NoExecType;
                if (nET == 5) // inf '�������� �����' value='5'/>
                   NotExecType = 2; // '�������� �����' value='2'/>
                else if (nET == 0) // inf '�����������' value='0'
                   NotExecType = 3; // '�����' value='3'/>
                else if ((nET == 3) || (nET == 7))
                  // inf '����� �� ���/�����' value='3'  ����� �� ������������ � �����������' value='7'/>
                   NotExecType = 4; // '�����' value='4'/>
                else if (nET != 1) // inf '��� �������' value='1'/>
                   NotExecType = 13; // '������' value='13'/>
               }
             }
           }
          if (FCheck)
           {
            CheckType = 0;
            if (FFullCheck)
             {
              NotExecType = 1;
              CheckType   = 1;
             }
            else if (FCheck)
             CheckType = 2;
            SQL = "Update CLASS_3084 set R3098=:pType, R308D=:pDate, R32A7=:pOtherMIBP, R32FD=:pTLPU Where code=:pCode";
            FDM->quExecParam(FQ, false, SQL, "pCode", itPlan->second.Id, "", "pType", CheckType, "pOtherMIBP", !MIBPEqu,
              "pTLPU", (int)InThisLPU, "pDate", FLastDate);
           }
          else
           {
            // �������� �������� �� ��������� (���������� ������� ������������)
            SQL = "Update CLASS_3084 set R3098=0, R308D=null, R32A7=null, R32FD = null Where code=:pCode";
            FDM->quExecParam(FQ, false, SQL, "pCode", itPlan->second.Id);
           }
          SQL = "Update CLASS_3084 set R32FC=:pR32FC Where code=:pCode";
          FDM->quExecParam(FQ, true, SQL, "pCode", itPlan->second.Id, "", "pR32FC", NotExecType);
         }
        // ########################################################################
        // ������� �����, ��� �� �������� :) (���� � �����, ��� � ����� - ��������� ����������)
        // ########################################################################
        for (itInfPriv = FPrivInfMap.begin(); itInfPriv != FPrivInfMap.end(); itInfPriv++)
         {
          FRC = FPlanInfMap.find(itInfPriv->second.Inf);
          if (FRC == FPlanInfMap.end())
           { // �������� �������� �� ������� � ����� - ��������� ����������
           }
         }
        // -------------------------------
        RC = true;
       }
      catch (Exception & E)
       {
        if (FQ->Transaction->Active)
         FQ->Transaction->Rollback();
        RC = false;
       }
     }
    __finally
     {
      FDM->DeleteTempQuery(FQ);
     }
   }
  else
   RC = true;
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                      �����                                                 #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegCardEditors::ImportTest(TCardImportType AImpType, TJSONObject * AValue,
  System::UnicodeString & ARecId, __int64 & AUnitCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  UnicodeString SQL;
  TFDQuery * FQ = FDM->CreateTempQuery();
  TFDQuery * InsQ = FDM->CreateTempQuery();
  try
   {
    // ���������� �����
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    try
     {
      // xOldProbVal = "-1";
      /*

       { u"{"code":null,"017B":"88",
       "1031":"12.05.2015",
       "10B5":null,
       "1032":"1","1200":null,"1033":null,"1034":"0","103B":"1","10A7":"7","103F":"8","0184":null,"0186":null,"0188":null,"018A":null,"1093":null,"018B":null,"10A5":"1","10B2":null,"018D":null,"10B3":null,"018E":null,"10C1":null,"10C2":null,"1156":null}" }

       */
      __int64 FCode = JSONToInt(AValue, "code");
      __int64 FUCode = JSONToInt(AValue, "017B");
      AUnitCode = FUCode;
      TDate FTestDate = JSONToDate(AValue, "1031");
      int FTestCode = JSONToInt(AValue, "1032", -1);
      SQL = "Select CODE From CLASS_102F Where R017B=:pUCODE and R1031=:pDate and R1032=:pTestCode";
      FDM->quExecParam(FQ, false, SQL, "pUCODE", FUCode, "", "pDate", FTestDate, "pTestCode", FTestCode);
      bool FErr = false;
      if (FQ->RecordCount)
       {
        FErr = (AImpType == citInsert) || ((AImpType == citEdit) && (FQ->FieldByName("CODE")->AsInteger != FCode));
        if (FErr)
         RC = "����� ����������, ������� ��������� � ��������� ������.";
        else
         FCode = FQ->FieldByName("CODE")->AsInteger;
        // RC = "����� ���������� (���� - \""+FTestDate.FormatString("dd.mm.yyyy")+"\", ����� - \""+DM->TestList[xProb]+"\"), ������� ��������� � ��������� ������.";
       }
      else if (AImpType != citEdit)
       FCode = -1;
      if (!FErr)
       {
        if (InsQ->Transaction->Active)
         InsQ->Transaction->Commit();
        InsQ->Transaction->StartTransaction();
        InsQ->SQL->Text =
          "UPDATE OR INSERT INTO class_102f (code, cguid, R017B, R32AF, R1031, R1032, R1200, R1033, R1034, R103B, R10A7, R103F, R0184, R0186, R0188, R018A, R1093, R018B, R10A5, R10B2, R018D, R10B3, R018E, R10C1, R10C2, R1156) values (:pcode, :pcguid, :pR017B, :pR32AF, :pR1031, :pR1032, :pR1200, :pR1033, :pR1034, :pR103B, :pR10A7, :pR103F, :pR0184, :pR0186, :pR0188, :pR018A, :pR1093, :pR018B, :pR10A5, :pR10B2, :pR018D, :pR10B3, :pR018E, :pR10C1, :pR10C2, :pR1156) MATCHING (CODE)";
        // , R017B, R32AF, R1031, R1032, R1200, R1033, R1034, R103B, R10A7, R103F, R0184, R0186, R0188, R018A, R1093, R018B, R10A5, R10B2, R018D, R10B3, R018E, R10C1, R10C2, R1156
        InsQ->Prepare();
        if ((AImpType == citInsert) || (FCode == -1))
         FCode = GetNewCode("102F");
        InsQ->ParamByName("pCode")->Value = FCode;
        // InsQ->ParamByName("pcguid")->Value = FCode;
        // ����������� �������� ������� ���������� �����
        SQL = "Select Max(R1031) as MDATE From CLASS_102F Where R017B=:pUCode and R1031 < :pDate and R1032=:pTestCode";
        FDM->quExecParam(FQ, false, SQL, "pUCode", FUCode, "", "pDate", FTestDate, "pTestCode", FTestCode);
        if (FQ->RecordCount)
         {
          if (!FQ->FieldByName("MDATE")->IsNull)
           { // ���������� ���������� �����
            SQL = "Select R103F, R10A7 From CLASS_102F Where R017B=:pUCode and R1031= :pDate and R1032=:pTestCode";
            FDM->quExecParam(FQ, false, SQL, "pUCode", FUCode, "", "pDate", FQ->FieldByName("MDATE")->AsDateTime,
              "pTestCode", FTestCode);
            if (FQ->RecordCount)
             { // ���������� ���������� �����
              if (!FQ->FieldByName("R103F")->IsNull)
               InsQ->ParamByName("pR10C1")->Value = FQ->FieldByName("R103F")->Value;
              if (!FQ->FieldByName("R10A7")->IsNull)
               InsQ->ParamByName("pR10C2")->Value = FQ->FieldByName("R10A7")->Value;
             }
           }
         }
        FCheckInfList.clear();
        ARecId = IntToStr(FCode);
        InsQ->ParamByName("pcguid")->AsString = NewGUID();
        InsQ->ParamByName("pR017B")->Value = FUCode;
        InsQ->ParamByName("pR32AF")->Value = JSONToVar(AValue, "32AF");
        InsQ->ParamByName("pR1031")->Value = JSONToVar(AValue, "1031");
        // InsQ->ParamByName("pR10B5")->Value = JSONToVar(AValue, "10B5");
        InsQ->ParamByName("pR1032")->Value = JSONToVar(AValue, "1032");
        InsQ->ParamByName("pR1200")->Value = JSONToVar(AValue, "1200");
        InsQ->ParamByName("pR1033")->Value = JSONToVar(AValue, "1033");
        InsQ->ParamByName("pR1034")->Value = JSONToVar(AValue, "1034");
        InsQ->ParamByName("pR103B")->Value = JSONToVar(AValue, "103B");
        InsQ->ParamByName("pR10A7")->Value = JSONToVar(AValue, "10A7");
        InsQ->ParamByName("pR103F")->Value = JSONToVar(AValue, "103F");
        InsQ->ParamByName("pR0184")->Value = JSONToVar(AValue, "0184");
        InsQ->ParamByName("pR0186")->Value = JSONToVar(AValue, "0186");
        InsQ->ParamByName("pR0188")->Value = JSONToVar(AValue, "0188");
        InsQ->ParamByName("pR018A")->Value = JSONToVar(AValue, "018A");
        InsQ->ParamByName("pR1093")->Value = JSONToVar(AValue, "1093");
        InsQ->ParamByName("pR018B")->Value = JSONToVar(AValue, "018B");
        InsQ->ParamByName("pR10A5")->Value = JSONToVar(AValue, "10A5");
        InsQ->ParamByName("pR10B2")->Value = JSONToVar(AValue, "10B2");
        InsQ->ParamByName("pR018D")->Value = JSONToVar(AValue, "018D");
        InsQ->ParamByName("pR10B3")->Value = JSONToVar(AValue, "10B3");
        InsQ->ParamByName("pR018E")->Value = JSONToVar(AValue, "018E");
        // InsQ->ParamByName("pR10C1")->Value = JSONToVar(AValue, "10C1");
        // InsQ->ParamByName("pR10C2")->Value = JSONToVar(AValue, "10C2");
        InsQ->ParamByName("pR1156")->Value = JSONToVar(AValue, "1156");
        InsQ->OpenOrExecute();
        FCheckInfList[JSONToString(AValue, "1200")] = JSONToString(AValue, "1200");
        if (InsQ->Transaction->Active)
         InsQ->Transaction->Commit();
        if (FOnEdited)
         FOnEdited(FUCode, 2, JSONToInt(AValue, "1200"));
        CheckTestInPlan(FUCode, FTestDate, JSONToDate(AValue, "3316"), JSONToInt(AValue, "32AF"));
        ClearHandSch(FUCode, false);
        RC = "ok";
       }
     }
    catch (Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
      if (FQ->Transaction->Active)
       FQ->Transaction->Rollback();
     }
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
    FDM->DeleteTempQuery(InsQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegCardEditors::DeleteTest(System::UnicodeString ARecId, __int64 & AUnitCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    try
     {
      FDM->quExecParam(FQ, false, "Select R32AF, R017B, R1031, R1032, R1200 From CLASS_102F Where code=:pCode", "pCode",
        ARecId);
      if (FQ->RecordCount)
       {
        __int64 FPlanCode = FQ->FieldByName("R32AF")->AsInteger;
        __int64 FUCode = FQ->FieldByName("R017B")->AsInteger;
        TDate FTestDate = FQ->FieldByName("R1031")->AsDateTime;
        AUnitCode = FUCode;
        FDM->quExecParam(FQ, true, "Delete  From CLASS_102F Where code=:pCode", "pCode", ARecId);
        CheckTestInPlan(FUCode, FTestDate, 0, FPlanCode);
       }
      RC = "ok";
     }
    catch (Exception & E)
     {
      if (FQ->Transaction->Active)
       FQ->Transaction->Rollback();
      RC = E.Message;
     }
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
bool __fastcall TdsRegCardEditors::CheckTestInPlan(__int64 AUCode, TDate ATestDate, TDate APlanDate, __int64 APlanCode,
  bool AFromOtvod)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  UnicodeString SQL;
  // TCheckCardItemMap FPrivMap;
  TCheckCardItemMap FTestMap;
  TCheckCardItemMap FPlanMap;
  TCheckCardItemMap FPlanInfMap;
  TCheckCardItemMap::iterator FRC, itTest, itPlan, itInfPlan;
  TDate HiPlanDate = TDate(IntToStr(DaysInMonth(Date())) + Date().FormatString(".mm.yyyy"));
  if ((int)APlanDate)
   HiPlanDate = TDate(IntToStr(DaysInMonth(APlanDate)) + APlanDate.FormatString(".mm.yyyy"));
  TDate HiDate = Date();
  if (AFromOtvod)
   HiPlanDate = TDate(IntToStr(DaysInMonth(ATestDate)) + ATestDate.FormatString(".mm.yyyy"));
  TDate LoDate = TDate("01." + HiDate.FormatString("mm.yyyy"));
  TDate LoPlanDate = TDate("01." + Sysutils::IncMonth(HiDate, -1).FormatString("mm.yyyy"));
  if ((int)APlanDate)
   LoPlanDate = TDate("01." + APlanDate.FormatString("mm.yyyy"));
  if ((ATestDate >= LoPlanDate) && (ATestDate <= HiPlanDate))
   {
    TFDQuery * FQ = FDM->CreateTempQuery();
    try
     {
      if (!FQ->Transaction->Active)
       FQ->Transaction->StartTransaction();
      try
       { // ������ ����� -----------------------------------------------------
        dsLogMessage("������ �����", __FUNC__);
        SQL = "Select code, R1032, R32AF, R1200, R1031, R10A5 From CLASS_102f "
          "Where R017B=:pUCode and R1031 <= :pHiDate and R1031 >= :pLoDate Order By R1031 DESC";
        FDM->quExecParam(FQ, false, SQL, "pUCode", AUCode, "", "pHiDate", HiDate, "pLoDate", LoDate);
        while (!FQ->Eof)
         {
          if (FTestMap.find(FQ->FieldByName("R1200")->AsInteger) == FTestMap.end())
           FTestMap[FQ->FieldByName("R1200")->AsInteger] = TCheckCardItem(FQ, 5);
          FQ->Next();
         }
        // ���� -------------------------------------------------------------
        __int64 FPlanCode = 0;
        if (APlanCode)
         {
          FDM->quExecParam(FQ, false, "Select R328F  From CLASS_3084 Where code=:pCode", "pCode", APlanCode);
          FPlanCode = FQ->FieldByName("R328F")->AsInteger;
         }
        // ������ ����
        dsLogMessage("������ ����", __FUNC__);
        if (FPlanCode)
         {
          SQL = "Select code, R3089, R328F, R32AB, R308B, R3088, R32FD, R3308, R32FC From CLASS_3084 "
            "Where R317D=:pUCode and R328F=:pPlanCode and R3086=2 Order By R3088 DESC";
          FDM->quExecParam(FQ, false, SQL, "pUCode", AUCode, "", "pPlanCode", FPlanCode);
         }
        else
         {
          SQL = "Select code, R3089, R328F, R32AB, R308B, R3088, R32FD, R3308, R32FC From CLASS_3084 "
            "Where R317D=:pUCode and R3088 <= :pHiDate and R3088 >= :pLoDate and R3086=2 Order By R3088 DESC";
          FDM->quExecParam(FQ, false, SQL, "pUCode", AUCode, "", "pHiDate", HiPlanDate, "pLoDate", LoPlanDate);
         }
        while (!FQ->Eof)
         {
          FRC = FPlanMap.find(FQ->FieldByName("R3089")->AsInteger);
          if (FRC == FPlanMap.end())
           FPlanMap[FQ->FieldByName("R3089")->AsInteger] = TCheckCardItem(FQ, 3);
          FQ->Next();
         }
        // ������ ���� �� ���������
        dsLogMessage("������ ���� �� ���������", __FUNC__);
        if (FPlanCode)
         {
          SQL = "Select code, R1089, R318F, R32A6, R108B, R1088, R3302, R3303, R3307 From CLASS_1084 "
            "Where R017D=:pUCode and R318F=:pPlanCode and R1086=2 Order By R1088 DESC";
          FDM->quExecParam(FQ, false, SQL, "pUCode", AUCode, "", "pPlanCode", FPlanCode);
         }
        else
         {
          SQL = "Select code, R1089, R318F, R32A6, R108B, R1088, R3302, R3303, R3307 From CLASS_1084 "
            "Where R017D=:pUCode and R1088 <= :pHiDate and R1088 >= :pLoDate and R1086=2 Order By R1088 DESC";
          FDM->quExecParam(FQ, false, SQL, "pUCode", AUCode, "", "pHiDate", HiPlanDate, "pLoDate", LoPlanDate);
         }
        while (!FQ->Eof)
         {
          FRC = FPlanInfMap.find(FQ->FieldByName("R32A6")->AsInteger);
          if (FRC == FPlanInfMap.end())
           FPlanInfMap[FQ->FieldByName("R32A6")->AsInteger] = TCheckCardItem(FQ, 4);
          FQ->Next();
         }
        // -------------------------------
        // ���������, ��������
        for (itTest = FTestMap.begin(); itTest != FTestMap.end(); itTest++)
         dsLogMessage("FTestMap: " + itTest->second.ToString(), __FUNC__, 5);
        for (itTest = FPlanMap.begin(); itTest != FPlanMap.end(); itTest++)
         dsLogMessage("FPlanMap: " + itTest->second.ToString(), __FUNC__, 5);
        for (itTest = FPlanInfMap.begin(); itTest != FPlanInfMap.end(); itTest++)
         dsLogMessage("FPlanInfMap: " + itTest->second.ToString(), __FUNC__, 5);
        // ########################################################################
        // ������� ����� �� ���������
        // ########################################################################
        dsLogMessage("������� ����� �� ���������", __FUNC__);
        Variant notExecType, notExecIll;
        bool FItemPresent;
        for (itInfPlan = FPlanInfMap.begin(); itInfPlan != FPlanInfMap.end(); itInfPlan++)
         {
          FRC          = FTestMap.find(itInfPlan->second.Inf);
          FItemPresent = (FRC != FTestMap.end());
          if (FItemPresent)
           { // ����� �����,, ����� ���������� �������� ������ ������ ��� ����� ��������
            FItemPresent = (FRC->second.Date.FormatString("yyyymm").ToIntDef(0) >=
              itInfPlan->second.Date.FormatString("yyyymm").ToIntDef(0));
           }
          if (!FItemPresent)
           { // �������� �������� �� ��������� (���������� ������� ������������)
            /* , R3303=1, R3304=null */
            notExecType = 1;
            notExecIll  = Variant::Empty();
            SQL         = "Select R1066, R1082 From CLASS_1030 "
              "Where R017C=:pUCode and R1111=1 and R1083=:pInf and ((R1043=0 and R105F<=:pPrivPlanDate and R1065>:pPrivPlanDate)or(R1043=1 and R105F<=:pPrivPlanDate ))";
            FDM->quExecParam(FQ, true, SQL, "pUCode", AUCode, "", "pInf", itInfPlan->second.Inf, "pPrivPlanDate",
              itInfPlan->second.Date);
            if (FQ->RecordCount)
             { // ���� ���������� �������� �������� ��� �����
              dsLogMessage("����� [" + IntToStr(itInfPlan->second.Inf) + "] " + itInfPlan->second.Date.FormatString
                ("dd.mm.yyyy"));
              notExecType = FQ->FieldByName("R1066")->AsInteger;
              if (!FQ->FieldByName("R1066")->AsInteger)
               notExecIll = FQ->FieldByName("R1082")->AsString;
             }
            else
             dsLogMessage("��� ������ [" + IntToStr(itInfPlan->second.Inf) + "] " + itInfPlan->second.Date.FormatString
              ("dd.mm.yyyy"));
            SQL = "Update CLASS_1084 set R1098=0, R108D=null, R3305=null, R32AA=null, R3302=null, R3303=:pR3303, R3304=:pR3304 Where code=:pCode";
            FDM->quExecParam(FQ, true, SQL, "pCode", itInfPlan->second.Id, "", "pR3303", notExecType, "pR3304",
              notExecIll);
            itInfPlan->second.ExecMIBP = 0;
            itInfPlan->second.ExecDate = TDate(0);
            // itInfPlan->NoMIBP; // ����������� ����
            itInfPlan->second.NoExecType = notExecType; // ������� ������������
           }
          else
           { /* , R3303=null, R3304=null */
            SQL = "Update CLASS_1084 set R1098=1, R108D=:pDate, R32AA=:pMIBP, R3305=:pOMIBP, R3302=:pInTLPU Where code=:pCode";
            FDM->quExecParam(FQ, true, SQL, "pCode", itInfPlan->second.Id, "", "pMIBP", FRC->second.MIBP, "pDate",
              FRC->second.Date, "pOMIBP", (FRC->second.MIBP == itInfPlan->second.MIBP), "pInTLPU",
              FRC->second.InThisLPU);
            itInfPlan->second.ExecMIBP   = FRC->second.MIBP;
            itInfPlan->second.ExecDate   = FRC->second.Date;
            itInfPlan->second.InThisLPU  = FRC->second.InThisLPU;
            itInfPlan->second.NoExecType = 1; // ������� ������������
           }
         }
        // ########################################################################
        // ������� �����
        // ########################################################################
        dsLogMessage("������� �����", __FUNC__);
        bool FCheck, MIBPEqu, FFullCheck;
        TDate FLastDate;
        int CheckType;
        int NotExecType;
        int nET;
        bool InThisLPU;
        for (itPlan = FPlanMap.begin(); itPlan != FPlanMap.end(); itPlan++)
         {
          FCheck      = false;
          FFullCheck  = true;
          MIBPEqu     = true;
          FLastDate   = TDate(0);
          InThisLPU   = false;
          NotExecType = 1; // ��� �������
          for (itInfPlan = FPlanInfMap.begin(); itInfPlan != FPlanInfMap.end(); itInfPlan++)
           {
            if (((itPlan->second.MIBP && (itInfPlan->second.MIBP == itPlan->second.MIBP)) ||
              (!itPlan->second.MIBP && (itInfPlan->second.Inf == itPlan->second.Inf))) &&
              (itInfPlan->second.Date == itPlan->second.Date))
             {
              InThisLPU |= itInfPlan->second.InThisLPU;
              FCheck |= itInfPlan->second.ExecMIBP;
              FFullCheck &= itInfPlan->second.ExecMIBP;
              MIBPEqu &= (itPlan->second.MIBP == itInfPlan->second.ExecMIBP);
              if (!(int)FLastDate && (int)itInfPlan->second.ExecDate)
               FLastDate = itInfPlan->second.ExecDate;
              if (NotExecType == 1)
               {
                nET = itInfPlan->second.NoExecType;
                if (nET == 5) // inf '�������� �����' value='5'/>
                   NotExecType = 2; // '�������� �����' value='2'/>
                else if (nET == 0) // inf '�����������' value='0'
                   NotExecType = 3; // '�����' value='3'/>
                else if ((nET == 3) || (nET == 7))
                  // inf '����� �� ��������/�����' value='3'  ����� �� ������������ � �����������' value='7'/>
                   NotExecType = 4; // '�����' value='4'/>
                else if (nET != 1) // inf '��� �������' value='1'/>
                   NotExecType = 13; // '������' value='13'/>
               }
             }
           }
          if (FCheck)
           {
            CheckType = 0;
            if (FFullCheck)
             {
              NotExecType = 1;
              CheckType   = 1;
             }
            else if (FCheck)
             CheckType = 2;
            /* , R32FC=null, R32FF=null */
            SQL = "Update CLASS_3084 set R3098=:pType, R308D=:pDate, R32A7=:pOtherMIBP, R32FD=:pTLPU Where code=:pCode";
            FDM->quExecParam(FQ, true, SQL, "pCode", itPlan->second.Id, "", "pType", CheckType, "pOtherMIBP", !MIBPEqu,
              "pTLPU", (int)InThisLPU, "pDate", FLastDate);
           }
          else
           { /* , R32FC=1, R32FF=null */
            // �������� ����� �� ��������� (���������� ������� ������������)
            SQL = "Update CLASS_3084 set R3098=0, R308D=null, R32A7=null, R32FD = null Where code=:pCode";
            FDM->quExecParam(FQ, true, SQL, "pCode", itPlan->second.Id);
           }
          SQL = "Update CLASS_3084 set R32FC=:pR32FC Where code=:pCode";
          FDM->quExecParam(FQ, true, SQL, "pCode", itPlan->second.Id, "", "pR32FC", NotExecType);
         }
        // ########################################################################
        // ������� �����, ��� �� �������� :) (���� � �����, ��� � ����� - ��������� ����������)
        // ########################################################################
        for (itTest = FTestMap.begin(); itTest != FTestMap.end(); itTest++)
         {
          FRC = FPlanInfMap.find(itTest->second.Inf);
          if (FRC == FPlanInfMap.end())
           { // �������� �������� �� ������� � ����� - ��������� ����������
           }
         }
        // -------------------------------
        RC = true;
       }
      catch (Exception & E)
       {
        if (FQ->Transaction->Active)
         FQ->Transaction->Rollback();
        RC = false;
       }
     }
    __finally
     {
      FDM->DeleteTempQuery(FQ);
     }
   }
  else
   RC = true;
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                      ��������                                              #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegCardEditors::ImportCheck(TCardImportType AImpType, TJSONObject * AValue,
  System::UnicodeString & ARecId, __int64 & AUnitCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  UnicodeString SQL;
  TFDQuery * FQ = FDM->CreateTempQuery();
  TFDQuery * InsQ = FDM->CreateTempQuery();
  try
   {
    // ���������� �����
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    try
     {
      __int64 FCode = JSONToInt(AValue, "code");
      __int64 FUCode = JSONToInt(AValue, "1101");
      AUnitCode = FUCode;
      TDate FCheckDate = JSONToDate(AValue, "1104");
      int FCheckCode = JSONToInt(AValue, "1103", -1);
      SQL = "Select CODE From CLASS_1100 Where R1101=:pUCODE and R1104=:pDate and R1103=:pCheckCode";
      FDM->quExecParam(FQ, false, SQL, "pUCODE", FUCode, "", "pDate", FCheckDate, "pCheckCode", FCheckCode);
      bool FErr = false;
      if (FQ->RecordCount)
       {
        FErr = (AImpType == citInsert) || ((AImpType == citEdit) && (FQ->FieldByName("CODE")->AsInteger != FCode));
        if (FErr)
         RC = "�������� ����������, ������� ��������� � ��������� ������.";
        // RC = "�������� ���������� (���� - \""+FTestDate.FormatString("dd.mm.yyyy")+"\", ����� - \""+DM->TestList[xProb]+"\"), ������� ��������� � ��������� ������.";
       }
      else if (AImpType != citEdit)
       FCode = -1;
      if (!FErr)
       {
        if (InsQ->Transaction->Active)
         InsQ->Transaction->Commit();
        InsQ->Transaction->StartTransaction();
        InsQ->SQL->Text =
          "UPDATE OR INSERT INTO class_1100 (code, cguid, R1101, R32B0, R1104, R1103, R110F, R319B, R319C, R1107, R1108, R110A, R110B, R110C, R110D, R110E, R1160) values (:pcode, :pcguid, :pR1101, :pR32B0, :pR1104, :pR1103, :pR110F, :pR319B, :pR319C, :pR1107, :pR1108, :pR110A, :pR110B, :pR110C, :pR110D, :pR110E, :pR1160) MATCHING (CODE)";
        // , R1101, R32B0, R1104, R1103, R110F, R319B, R319C, R1107, R1108, R110A, R110B, R110C, R110D, R110E, R1160
        InsQ->Prepare();
        if (AImpType != citEdit)
         FCode = GetNewCode("1100");
        InsQ->ParamByName("pCode")->Value = FCode;
        ARecId = IntToStr(FCode);
        if (AImpType != citEdit)
         InsQ->ParamByName("pcguid")->AsString = NewGUID();
        InsQ->ParamByName("pR1101")->Value = FUCode;
        InsQ->ParamByName("pR32B0")->Value = JSONToVar(AValue, "32B0");
        InsQ->ParamByName("pR1104")->Value = JSONToVar(AValue, "1104");
        // InsQ->ParamByName("pR1105")->Value = JSONToVar(AValue, "1105");
        InsQ->ParamByName("pR1103")->Value = JSONToVar(AValue, "1103");
        InsQ->ParamByName("pR110F")->Value = JSONToVar(AValue, "110F");
        InsQ->ParamByName("pR319B")->Value = JSONToVar(AValue, "319B");
        InsQ->ParamByName("pR319C")->Value = JSONToVar(AValue, "319C");
        InsQ->ParamByName("pR1107")->Value = JSONToVar(AValue, "1107");
        InsQ->ParamByName("pR1108")->Value = JSONToVar(AValue, "1108");
        InsQ->ParamByName("pR110A")->Value = JSONToVar(AValue, "110A");
        InsQ->ParamByName("pR110B")->Value = JSONToVar(AValue, "110B");
        InsQ->ParamByName("pR110C")->Value = JSONToVar(AValue, "110C");
        InsQ->ParamByName("pR110D")->Value = JSONToVar(AValue, "110D");
        InsQ->ParamByName("pR110E")->Value = JSONToVar(AValue, "110E");
        InsQ->ParamByName("pR1160")->Value = JSONToVar(AValue, "1160");
        InsQ->OpenOrExecute();
        if (InsQ->Transaction->Active)
         InsQ->Transaction->Commit();
        if (FOnEdited)
         FOnEdited(FUCode, 3, JSONToInt(AValue, "1103"));
        CheckCheckInPlan();
        RC = "ok";
       }
     }
    catch (Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
      if (FQ->Transaction->Active)
       FQ->Transaction->Rollback();
     }
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
    FDM->DeleteTempQuery(InsQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegCardEditors::DeleteCheck(System::UnicodeString ARecId, __int64 & AUnitCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    try
     {
      FDM->quExecParam(FQ, false, "Select R1101 From CLASS_1100 Where code=:pCode", "pCode", ARecId);
      if (FQ->RecordCount)
       {
        __int64 FUCode = FQ->FieldByName("R1101")->AsInteger;
        FDM->quExecParam(FQ, true, "Delete  From CLASS_1100 Where code=:pCode", "pCode", ARecId);
        AUnitCode = FUCode;
        CheckCheckInPlan();
        RC = "ok";
       }
     }
    catch (Exception & E)
     {
      if (FQ->Transaction->Active)
       FQ->Transaction->Rollback();
      RC = E.Message;
     }
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegCardEditors::CheckCheckInPlan()
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    try
     {
      RC = "ok";
     }
    catch (Exception & E)
     {
      if (FQ->Transaction->Active)
       FQ->Transaction->Rollback();
      RC = E.Message;
     }
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                      ������                                                #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegCardEditors::ImportOtvod(TCardImportType AImpType, TJSONObject * AValue,
  System::UnicodeString & ARecId, __int64 & AUnitCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  UnicodeString SQL;
  TFDQuery * FQ = FDM->CreateTempQuery();
  TFDQuery * InsQ = FDM->CreateTempQuery();
  try
   {
    // ���������� �����
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    try
     {
      __int64 FCode = JSONToInt(AValue, "code");
      __int64 FUCode = JSONToInt(AValue, "317C");
      AUnitCode = FUCode;
      TDate FBegDate = JSONToDate(AValue, "305F");
      int FOtvodTypeCode = JSONToInt(AValue, "3043", -1);
      int FOtvodReasonCode = JSONToInt(AValue, "3066", -1);
      UnicodeString FInfListStr = JSONToString(AValue, "3083");
      SQL = "Select CODE From CLASS_3030 Where R317C=:pUCODE and R305F=:pDate and R3043=:pTypeCode and R3066=:pReasonCode";
      FDM->quExecParam(FQ, false, SQL, "pUCODE", FUCode, "", "pDate", FBegDate, "pTypeCode", FOtvodTypeCode,
        "pReasonCode", FOtvodReasonCode);
      bool FErr = false;
      if (FQ->RecordCount)
       {
        FErr = (AImpType == citInsert) || ((AImpType == citEdit) && (FQ->FieldByName("CODE")->AsInteger != FCode));
        if (FErr)
         RC = "����� ����������, ������� ��������� � ��������� ������.";
        else
         FCode = FQ->FieldByName("CODE")->AsInteger;
        // RC = "�������� ���������� (���� - \""+FTestDate.FormatString("dd.mm.yyyy")+"\", ����� - \""+DM->TestList[xProb]+"\"), ������� ��������� � ��������� ������.";
       }
      if (!FErr)
       {
        TStringList * FInfList = new TStringList;
        try
         {
          UnicodeString FIt = GetLPartB(FInfListStr, ',');
          if (!FIt.Length())
           {
            FIt         = FInfListStr;
            FInfListStr = "";
           }
          while (FIt.Length())
           {
            FInfList->Add(FIt);
            FInfListStr = GetRPartB(FInfListStr, ',');
            FIt         = GetLPartB(FInfListStr, ',');
            if (!FIt.Length())
             {
              FIt         = FInfListStr;
              FInfListStr = "";
             }
           }
          // FInfList->Delimiter     = ',';
          // FInfList->DelimitedText = FInfListStr;
          if (FInfList->Count)
           {
            if (GetLPartB(FInfList->Strings[0], ':').LowerCase() == "all")
             {
              FInfListStr = "��� ��������";
              FDM->quExec(FQ, false, "Select CODE From CLASS_003a");
              FInfList->Clear();
              while (!FQ->Eof)
               {
                FInfList->Add(FQ->FieldByName("code")->AsString);
                FQ->Next();
               }
             }
            else
             {
              FInfListStr = "";
              for (int i = 0; i < FInfList->Count; i++)
               {
                if (FInfListStr.Length())
                 FInfListStr += ", ";
                FInfListStr += GetRPartE(FInfList->Strings[i], ':'); ;
                FInfList->Strings[i] = GetLPartB(FInfList->Strings[i], ':');
               }
             }
           }
          if (InsQ->Transaction->Active)
           InsQ->Transaction->Commit();
          InsQ->Transaction->StartTransaction();
          InsQ->SQL->Text =
            "UPDATE OR INSERT INTO CLASS_3030 (code, cguid, R317C, R305F, R3065, R3043, R3083, R3066, R3082, R3110, R3111) " "values (:pcode, :pcguid, :pR317C, :pR305F, :pR3065, :pR3043, :pR3083, :pR3066, :pR3082, :pR3110, :pR3111) MATCHING (CODE)";
          InsQ->Prepare();
          if ((AImpType == citInsert) || (FCode == -1))
           FCode = GetNewCode("3030");
          InsQ->ParamByName("pCode")->Value = FCode;
          ARecId = IntToStr(FCode);
          if (AImpType != citEdit)
           InsQ->ParamByName("pcguid")->AsString = NewGUID();
          InsQ->ParamByName("pR317C")->Value = FUCode;
          InsQ->ParamByName("pR305F")->Value = JSONToVar(AValue, "305F");
          InsQ->ParamByName("pR3065")->Value = JSONToVar(AValue, "3065");
          InsQ->ParamByName("pR3043")->Value = JSONToVar(AValue, "3043");
          InsQ->ParamByName("pR3083")->Value = FInfListStr;
          InsQ->ParamByName("pR3066")->Value = JSONToVar(AValue, "3066");
          InsQ->ParamByName("pR3082")->Value = JSONToVar(AValue, "3082");
          InsQ->ParamByName("pR3110")->Value = JSONToVar(AValue, "3110");
          InsQ->ParamByName("pR3111")->Value = JSONToVar(AValue, "3111");
          InsQ->OpenOrExecute();
          FDM->quExecParam(FQ, false, "Delete From CLASS_1030 Where R3184=:pOtvCODE", "pOtvCODE", FCode);
          FQ->SQL->Text =
            "UPDATE OR INSERT INTO CLASS_1030 (code, cguid, R017C, R3184, R105F, R1065, R1043, R1083, R1066, R1082, R1110, R1111) " "values (:pcode, :pcguid, :pR017C, :pR3184, :pR105F, :pR1065, :pR1043, :pR1083, :pR1066, :pR1082, :pR1110, :pR1111) MATCHING (CODE)";
          FQ->Prepare();
          FQ->ParamByName("pR017C")->Value = FUCode;
          FQ->ParamByName("pR3184")->Value = FCode;
          FQ->ParamByName("pR105F")->Value = JSONToVar(AValue, "305F");
          FQ->ParamByName("pR1065")->Value = JSONToVar(AValue, "3065");
          FQ->ParamByName("pR1043")->Value = JSONToVar(AValue, "3043");
          FQ->ParamByName("pR1066")->Value = JSONToVar(AValue, "3066");
          FQ->ParamByName("pR1082")->Value = JSONToVar(AValue, "3082");
          FQ->ParamByName("pR1110")->Value = JSONToVar(AValue, "3110");
          FQ->ParamByName("pR1111")->Value = JSONToVar(AValue, "3111");
          for (int i = 0; i < FInfList->Count; i++)
           {
            FQ->ParamByName("pCode")->Value = GetNewCode("1030");
            FQ->ParamByName("pcguid")->AsString = NewGUID();
            FQ->ParamByName("pR1083")->Value = FInfList->Strings[i].ToIntDef(0);
            FQ->OpenOrExecute();
           }
          if (InsQ->Transaction->Active)
           InsQ->Transaction->Commit();
          if (FQ->Transaction->Active)
           FQ->Transaction->Commit();
          TDate CheckDate = Sysutils::IncMonth(Date(), -1);
          CheckDate = TDate(IntToStr(DaysInMonth(CheckDate)) + CheckDate.FormatString(".mm.yyyy"));
          CheckVacInPlan(FUCode, CheckDate, 0, true);
          CheckTestInPlan(FUCode, CheckDate, 0, 0, true);
          CheckDate = TDate(IntToStr(DaysInMonth(Date())) + Date().FormatString(".mm.yyyy"));
          CheckVacInPlan(FUCode, CheckDate, 0, true);
          CheckTestInPlan(FUCode, CheckDate, 0, 0, true);
          CheckDate = Sysutils::IncMonth(Date(), 1);
          CheckDate = TDate(IntToStr(DaysInMonth(CheckDate)) + CheckDate.FormatString(".mm.yyyy"));
          CheckVacInPlan(FUCode, CheckDate, 0, true);
          CheckTestInPlan(FUCode, CheckDate, 0, 0, true);
          RC = "ok";
         }
        __finally
         {
          delete FInfList;
         }
       }
     }
    catch (Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
      if (FQ->Transaction->Active)
       FQ->Transaction->Rollback();
     }
   }
  __finally
   {
    if (InsQ->Transaction->Active)
     InsQ->Transaction->Rollback();
    if (FQ->Transaction->Active)
     FQ->Transaction->Rollback();
    FDM->DeleteTempQuery(FQ);
    FDM->DeleteTempQuery(InsQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsRegCardEditors::DeleteOtvod(System::UnicodeString ARecId, __int64 & AUnitCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    try
     {
      FDM->quExecParam(FQ, false, "Select R317C From CLASS_3030 Where code=:pCode", "pCode", ARecId);
      if (FQ->RecordCount)
       {
        __int64 FUCode = FQ->FieldByName("R317C")->AsInteger;
        FDM->quExecParam(FQ, true, "Delete  From CLASS_3030 Where code=:pCode", "pCode", ARecId);
        FDM->quExecParam(FQ, true, "Delete  From CLASS_1030 Where R3184=:pCode", "pCode", ARecId);
        AUnitCode = FUCode;
        TDate CheckDate = Sysutils::IncMonth(Date(), -1);
        CheckDate = TDate(IntToStr(DaysInMonth(CheckDate)) + CheckDate.FormatString(".mm.yyyy"));
        CheckVacInPlan(FUCode, CheckDate, 0, true);
        CheckDate = TDate(IntToStr(DaysInMonth(Date())) + Date().FormatString(".mm.yyyy"));
        CheckVacInPlan(FUCode, CheckDate, 0, true);
        CheckDate = Sysutils::IncMonth(Date(), 1);
        CheckDate = TDate(IntToStr(DaysInMonth(CheckDate)) + CheckDate.FormatString(".mm.yyyy"));
        CheckVacInPlan(FUCode, CheckDate, 0, true);
        RC = "ok";
       }
     }
    catch (Exception & E)
     {
      if (FQ->Transaction->Active)
       FQ->Transaction->Rollback();
      RC = E.Message;
     }
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
__int64 __fastcall TdsRegCardEditors::GetNewCode(UnicodeString TabId)
 {
  __int64 RC = -1;
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    FQ->Command->CommandKind = skStoredProc;
    FQ->SQL->Text            = "MCODE_" + TabId;
    FQ->Prepare();
    FQ->OpenOrExecute();
    RC = FQ->FieldByName("MCODE")->AsInteger;
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegCardEditors::NewGUID()
 {
  return icsNewGUID().UpperCase();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegCardEditors::GetPrivVar(__int64 APrivCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    FDM->quExecParam(FQ, false, "select r3185 from class_3003 where code=:pcode", "pcode", APrivCode);
    if (!FQ->Eof)
     RC = FQ->FieldByName("r3185")->AsString;
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsRegCardEditors::GetOtvodInf(__int64 AOtvodCode)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    FDM->quExecParam(FQ, false, "select R1083 from class_1030 where R3184=:pR3184", "pR3184", AOtvodCode);
    while (!FQ->Eof)
     {
      if (RC.Length())
       RC += ";";
      RC += FQ->FieldByName("R1083")->AsString;
      FQ->Next();
     }
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsRegCardEditors::CheckVacSchCorrect(UnicodeString & AVac, UnicodeString & ASch,
  UnicodeString & AVacType)
 {
  // �������� ������������ �����
  if (AVacType.UpperCase().Pos("���"))
   { // �������������� ��������
    ASch = "_NULL_", AVacType = "���.";
   }
  else
   {
    if (!VacSchCorrect(AVac, ASch, AVacType))
     { // ����� ���������� �������� �� ����������
      ASch = GetNewSchByType(AVac, AVacType);
      if (!ASch.Length())
       { // ������ � ����� ���������� �� �������
        ASch = "_NULL_", AVacType = "���.";
       }
     }
   }
 }
// ----------------------------------------------------------------------------
bool __fastcall TdsRegCardEditors::VacSchCorrect(UnicodeString AVac, UnicodeString ASch, UnicodeString AVacType)
 {
  // schCorrect = VacSchCorrect(FQ, "R1020", "R1092", "R1021");
  bool RC = false;
  try
   {
    TTagNode * Line = VacSch->GetTagByUID(_UID(ASch));
    if (Line)
     {
      if (Line->CmpName("line"))
       { // ������ ����� �������  // �������� �� ���������� �������, ���� ����������, uid �����
        TTagNode * Sch = Line->GetParent();
        if (Sch)
         {
          RC = Sch->CmpAV("ref", AVac) && Sch->CmpAV("uid", _GUI(ASch)) && Line->CmpAV("name", AVacType);
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegCardEditors::GetNewSchByType(UnicodeString AMIBP, UnicodeString AVacType)
 {
  UnicodeString RC = "";
  FSchList = new TList;
  try
   {
    UnicodeString SchUID = AMIBP;
    VacSch->Iterate(FGetSchList, SchUID);
    SchUID = "";
    TTagNode * SchLine, *FDefSch, *FSch;
    FDefSch = NULL;
    for (int i = 0; (i < FSchList->Count) && !FDefSch; i++)
     {
      if (((TTagNode *)FSchList->Items[i])->CmpAV("def", "1"))
       FDefSch = (TTagNode *)FSchList->Items[i];
     }
    SchLine = NULL;
    if (FDefSch) // ����� �� ��������� �������
       SchLine = FDefSch->GetChildByAV("line", "name", AVacType.Trim());
    if (SchLine) // ������ � ����� ���������� �������
       RC = FDefSch->AV["uid"] + "." + SchLine->AV["uid"];
    else
     {
      SchLine = NULL;
      for (int i = 0; (i < FSchList->Count) && !SchLine; i++)
       {
        FDefSch = ((TTagNode *)FSchList->Items[i]);
        SchLine = FDefSch->GetChildByAV("line", "name", AVacType.Trim());
       }
      if (SchLine) // ������ � ����� ���������� �������
         RC = FDefSch->AV["uid"] + "." + SchLine->AV["uid"];
     }
   }
  __finally
   {
    delete FSchList;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegCardEditors::FGetSchList(TTagNode * ANode, UnicodeString & AMIBPCode)
 {
  if (ANode->CmpName("schema"))
   {
    if (ANode->CmpAV("ref", AMIBPCode))
     {
      FSchList->Add((void *)ANode);
     }
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegCardEditors::ClearHandSch(__int64 AUCode, bool AVac)
 {
  TTagNode * FSchData = new TTagNode;
  TTagNode * FSchRoot, *itInf;
  try
   {
    GetUnitSch(AUCode, FSchData);
    FSchRoot = FSchData->GetChildByName("sch", true);
    if (FSchRoot)
     {
      bool FChanges = false;
      for (TAnsiStrMap::iterator i = FCheckInfList.begin(); i != FCheckInfList.end(); i++)
       {
        itInf = FSchRoot->GetChildByAV("spr", "infref", i->first);
        if (itInf)
         {
          if (AVac)
           {
            if (itInf->AV["chvac"].ToIntDef(0))
             {
              itInf->AV["chvac"] = "0";
              FChanges |= true;
             }
           }
          else
           {
            if (itInf->AV["chtest"].ToIntDef(0))
             {
              itInf->AV["chtest"] = "0";
              FChanges |= true;
             }
           }
         }
       }
      if (FChanges)
       SetUnitSch(AUCode, FSchData->AsXML);
     }
   }
  __finally
   {
    delete FSchData;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegCardEditors::GetUnitSch(__int64 AUCode, TTagNode * ASch)
 {
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    try
     {
      if (!FDM->quExecParam(FQ, false, "Select data From UNIT_CARD_OPT where code=:pCode", "pCode", AUCode,
        "������ �������� ������������ ��� ��������").Length())
       {
        TTagNode * FSchRoot, *itSch, *itDefSch;
        if (!FQ->FieldByName("data")->IsNull) //
         {
          try
           {
            try
             {
              ASch->Encoding = "utf-8";
              ASch->AsXML    = FQ->FieldByName("data")->AsString;
             }
            catch (Exception & E)
             {
              TStringStream * FZipSch = new TStringStream;
              try
               {
                FZipSch->WriteData(FQ->FieldByName("data")->AsBytes, FQ->FieldByName("data")->AsBytes.Length);
                ASch->Encoding = "utf-8";
                ASch->SetZIPXML(FZipSch);
               }
              __finally
               {
                delete FZipSch;
               }
             }
           }
          catch (Exception & E)
           {
            dsLogError(E.Message, __FUNC__);
           }
         }
       }
     }
    catch (Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    FDM->DeleteTempQuery(FQ);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegCardEditors::SetUnitSch(__int64 AUCode, UnicodeString ASch)
 {
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    FDM->quExecParam(FQ, true, "UPDATE OR INSERT INTO UNIT_CARD_OPT (code, data) values(:pCode, :pData)", "pCode",
      AUCode, "���������� �������� ������������ ��� ��������", "pData", ASch);
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
   }
 }
// ---------------------------------------------------------------------------
