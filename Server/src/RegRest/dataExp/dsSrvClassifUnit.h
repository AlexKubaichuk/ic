//---------------------------------------------------------------------------
#ifndef dsSrvClassifUnitH
#define dsSrvClassifUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "srvsetting.h"
#include "XMLContainer.h"
#ifndef _CONFSETTTING
 #error _CONFSETTTING using are not defined
#endif
//---------------------------------------------------------------------------
class PACKAGE TdsSrvClassifSQLData : public TObject
{
private:
  UnicodeString FCountSQL;
  UnicodeString FClassSQL;
  UnicodeString FSelectSQL;
  UnicodeString FSelectRecSQL;
  UnicodeString FSelectRec10SQL;
//  UnicodeString FInsertSQL;
//  UnicodeString FUpdateSQL;
  UnicodeString FDeleteSQL;
  UnicodeString FKeyFl;
  UnicodeString FOrderSQL;
  TStringList *FListFields;     //Strings[i],(int)Objects[i] -> FieldName=FieldCaption,FieldWidth
  TStringList *FFields;     //Strings[i],(int)Objects[i] -> FieldName=FieldCaption,FieldWidth
  bool FQuotedKey;

  TTagNode *FClDef;
  UnicodeString FTabName;
  UnicodeString FTabAlias;

public:		// User declarations
  __fastcall TdsSrvClassifSQLData();
  __fastcall ~TdsSrvClassifSQLData();

  __property UnicodeString CountSQL     = {read=FCountSQL,  write=FCountSQL};
  __property UnicodeString ClassSQL     = {read=FClassSQL,  write=FClassSQL};
  __property UnicodeString SelectSQL    = {read=FSelectSQL, write=FSelectSQL};
  __property UnicodeString SelectRecSQL = {read=FSelectRecSQL, write=FSelectRecSQL};
  __property UnicodeString SelectRec10SQL = {read=FSelectRec10SQL, write=FSelectRec10SQL};
//  __property UnicodeString InsertSQL = {read=FInsertSQL, write=FInsertSQL};
//  __property UnicodeString UpdateSQL = {read=FUpdateSQL, write=FUpdateSQL};
  __property UnicodeString DeleteSQL = {read=FDeleteSQL, write=FDeleteSQL};
  __property UnicodeString OrderSQL  = {read=FOrderSQL, write=FOrderSQL};
  __property UnicodeString KeyFl = {read=FKeyFl, write=FKeyFl};
  __property bool QuotedKey = {read=FQuotedKey, write=FQuotedKey};

  __property TStringList *Fields = {read=FFields};
  __property TStringList *ListFields = {read=FListFields};

  __property TTagNode *Def = {read=FClDef, write=FClDef};
  __property UnicodeString TabName = {read=FTabName, write=FTabName};
  __property UnicodeString TabAlias = {read=FTabAlias, write=FTabAlias};
  UnicodeString __fastcall FieldNameById(UnicodeString AId);
};
//---------------------------------------------------------------------------
typedef map<UnicodeString, TdsSrvClassifSQLData*> TdsSrvClassifDataMap;
//---------------------------------------------------------------------------
class PACKAGE TdsSrvClassif : public TObject
{
private:	// User declarations
  TTagNode *FRoot;
  TdsSrvClassifSQLData *FSQLs;
  UnicodeString FPref, FAlias;
  UnicodeString SaveSelect;
  TStringList *FlList;

  UnicodeString alTabName;
  int  chBDInd;
//  TTagNode  *CallRootClass, *itxTag;

  UnicodeString __fastcall GetTabName(TTagNode *ADefNode);
  UnicodeString __fastcall GetFlName(TTagNode *ADefNode);

  bool __fastcall KeyFlExists(TTagNode *AClDef);
  UnicodeString __fastcall GetKeyName(TTagNode *AClDef);
  bool __fastcall QuotedKey(TTagNode *AClDef);
  bool __fastcall GetClassFl(TTagNode *itNode, UnicodeString &UID);
  UnicodeString __fastcall GetJoinClass(TTagNode *AClDef, UnicodeString &AFrom, UnicodeString AFN);
  UnicodeString __fastcall GetClassSQL(TTagNode *AClDef, UnicodeString &AFrom, UnicodeString AFN, int &AchBDInd);
  UnicodeString __fastcall FGetExtField(TTagNode *AFlDef, UnicodeString AAlias);
  UnicodeString __fastcall FCheckAddField(UnicodeString AFLDef, UnicodeString AFLName);
public:		// User declarations
  __fastcall TdsSrvClassif();
  void __fastcall CreateSQLs(TTagNode *ARoot, UnicodeString AUID, TdsSrvClassifSQLData *ASQLs, UnicodeString AAlias = "", UnicodeString APref = "");
};
//---------------------------------------------------------------------------
extern PACKAGE void __fastcall GetClassifSQLData(TTagNode *ARoot, UnicodeString AUID, TdsSrvClassifSQLData* ASQLs, UnicodeString AAlias = "", UnicodeString APref = "");
#endif
