//---------------------------------------------------------------------------
//#include <vcl.h>
#pragma hdrstop

#include "dsSrvClassifUnit.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
__fastcall TdsSrvClassifSQLData::TdsSrvClassifSQLData()
 {
  FCountSQL     = "";
  FSelectSQL    = "";
  FSelectRecSQL = "";
  FQuotedKey    = false;
  //FInsertSQL    = "";
  //FUpdateSQL    = "";
  FDeleteSQL  = "";
  FOrderSQL   = "";
  FListFields = new TStringList;
  FFields     = new TStringList;
 }
//---------------------------------------------------------------------------
__fastcall TdsSrvClassifSQLData::~TdsSrvClassifSQLData()
 {
  delete FListFields;
  delete FFields;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvClassifSQLData::FieldNameById(UnicodeString AId)
 {
  UnicodeString RC = AId;
  try
   {
    TTagNode * FFlDef = FClDef->GetTagByUID(AId);
    if (FFlDef)
     {
      RC = "R" + FFlDef->AV["uid"];
      if (FFlDef->AV["fl"].Length())
       RC = FFlDef->AV["fl"];
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
__fastcall TdsSrvClassif::TdsSrvClassif()
 {
 }
//---------------------------------------------------------------------------
void __fastcall TdsSrvClassif::CreateSQLs(TTagNode * ARoot, UnicodeString  AUID, TdsSrvClassifSQLData * ASQLs, UnicodeString  AAlias, UnicodeString  APref)
 {
  FSQLs  = ASQLs;
  FPref  = APref;
  FAlias = AAlias;
  if (!FAlias.Length())
   FAlias = "C" + AUID;
  if (FAlias == "C1000")
   FAlias = "CB";
  FRoot      = ARoot;
  FSQLs->Def = FRoot->GetTagByUID(AUID);
  if (!FSQLs->Def)
   throw Exception("����������� �������� ��������������. UID=" + AUID);
  UnicodeString _EMPTYSTR_ = "";
  if (FSQLs->Def && FSQLs->Def->CmpName("class"))
   {
    FSQLs->TabAlias = FAlias;
    UnicodeString FKeyFL = GetKeyName(FSQLs->Def);

    UnicodeString tmpClassSQL = "";
    UnicodeString tmpClassFrom = "";
    int FChNum = 0;
    tmpClassSQL     = GetClassSQL(FSQLs->Def, tmpClassFrom, "", FChNum);
    FSQLs->ClassSQL = "Select Distinct " + tmpClassSQL + " " + tmpClassFrom;
    FSQLs->ListFields->Clear();
    chBDInd = 0;

    UnicodeString FSelect, FListSel, FOrderSQL, tmpSel;
    FOrderSQL = "";
    if (FSQLs->Def->AV["tab"].Length())
     {
      FSelect  = FAlias + "." + FKeyFL;
      FListSel = FSelect;
     }
    else
     {
      FSelect  = FAlias + "." + FKeyFL;
      FListSel = FSelect;
     }

    FSQLs->TabName = GetTabName(FSQLs->Def);
    UnicodeString FSelFrom = FSQLs->TabName + " " + FAlias;
    UnicodeString FListFrom = FSelFrom;
    bool FChBDPres = false;

    FlList = new TStringList;
    try
     {
      FSQLs->Def->Iterate(GetClassFl, _EMPTYSTR_);
      TTagNode * flDef;
      UnicodeString FN, FullFN;
      for (int i = 0; i < FlList->Count; i++)
       {
        flDef = FSQLs->Def->GetTagByUID(FlList->Strings[i]);
        if (flDef)
         {
          FSQLs->Fields->Add(flDef->AV["uid"]);
          if (flDef->CmpAV("inlist", "l"))
           FSQLs->ListFields->Add(flDef->AV["uid"].UpperCase());

          FN     = GetFlName(flDef);
          FullFN = FAlias + "." + FN;
          if (!flDef->CmpAV("comment", "key"))
           {
            if (FSelect.Length())
             FSelect += ", ";
            if (FListSel.Length() && flDef->CmpAV("inlist", "l"))
             FListSel += ", ";

            if (flDef->CmpAV("inlist", "l"))
             {
              if (FOrderSQL.Length())
               FOrderSQL += ", ";
              if (flDef->CmpName("text") || (flDef->CmpName("extedit") && !flDef->AV["fieldtype"].Length() && flDef->AV["length"].ToIntDef(0)))
               {//��������� ����
                FOrderSQL += "Upper(" + FullFN + ")";
               }
              else
               FOrderSQL += FullFN;
             }

            FSelect += FullFN;
            if (flDef->CmpAV("inlist", "l"))
             FListSel += FullFN;

            if (flDef->CmpName("choice"))
             {
              tmpSel = ", (case " + FullFN;
              TTagNode * itChVal = flDef->GetFirstChild();
              while (itChVal)
               {
                if (itChVal->CmpName("choicevalue"))
                 tmpSel += " when " + itChVal->AV["value"] + " then '" + itChVal->AV["name"] + "'";
                itChVal = itChVal->GetNext();
               }
              tmpSel += " else '' end ) as " + FN + "Str";
              FSelect += tmpSel;
              if (flDef->CmpAV("inlist", "l"))
               FListSel += tmpSel;
             }
            else if (flDef->CmpName("choiceDB"))
             {
              FChBDPres = true;
              TTagNode * refClDef = FRoot->GetTagByUID(flDef->AV["ref"]); //�������� �������� �������������� �� ������
              UnicodeString FRefClFrom = "";
              UnicodeString FRefClSelect = GetJoinClass(refClDef, FRefClFrom, FullFN);
              tmpSel = ", (" + FRefClSelect + ") as " + FN + "Str";

              FSelect += tmpSel;
              FSelFrom += FRefClFrom;
              if (flDef->CmpAV("inlist", "l"))
               {
                FListSel += tmpSel;
                FListFrom += FRefClFrom;
               }
              chBDInd++ ;
             }
            else if (flDef->CmpName("extedit") && flDef->CmpAV("inlist", "l"))
             {
              FListSel += FGetExtField(flDef, FAlias);
             }
           }
         }
       }
      FSQLs->CountSQL = "Select Count(" + FAlias + "." + FKeyFL + ") as RCount From " + FSelFrom;
      FSQLs->SelectRecSQL   = "Select distinct " + FSelect + " From " + FSelFrom + " Where " + FAlias + "." + FKeyFL + "=:p" + FKeyFL;
      FSQLs->SelectRec10SQL = "Select distinct " + FSelect + " From " + FSelFrom + " Where " + FAlias + "." + FKeyFL + " in (_KEY_REP_VAL_)";
      FSQLs->SelectSQL      = "Select distinct " + FListSel + " From " + FListFrom;
      FSQLs->DeleteSQL      = "Delete From " + FSQLs->TabName + " Where " + FKeyFL + "=:p" + FKeyFL;
      FSQLs->KeyFl          = FKeyFL;
      FSQLs->QuotedKey      = QuotedKey(FSQLs->Def);
      FSQLs->OrderSQL       = FOrderSQL;
     }
    __finally
     {
      delete FlList;
     }
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TdsSrvClassif::GetClassFl(TTagNode * itNode, UnicodeString & UID)
 {
  if (itNode->CmpName("binary,text,digit,date,choice,choiceDB,choiceTree,extedit"))
   FlList->Add(itNode->AV["uid"]);
  return false;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvClassif::GetTabName(TTagNode * ADefNode)
 {
  UnicodeString RC = "CLASS_" + FPref + ADefNode->AV["uid"];
  try
   {
    if (ADefNode->AV["tab"].Length())
     RC = ADefNode->AV["tab"];
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvClassif::GetFlName(TTagNode * ADefNode)
 {
  UnicodeString RC = "R" + ADefNode->AV["uid"];
  try
   {
    if (ADefNode->AV["fl"].Length())
     RC = ADefNode->AV["fl"];
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvClassif::GetJoinClass(TTagNode * AClDef, UnicodeString & AFrom, UnicodeString  AFN)
 {
  UnicodeString RC = "";
  if (AClDef->CmpName("class"))
   {
    TTagNode * FFlDef;
    try
     {
      //�������� ������ ���� �� ������ ������������ �����
      TTagNode * lDef = AClDef->GetChildByName("description")->GetFirstChild();
      TTagNode * itNode = lDef->GetFirstChild();
      UnicodeString FClTabName = GetTabName(AClDef);
      UnicodeString FClTabAlias = "Cl" + IntToStr(chBDInd) + AClDef->AV["uid"];
      UnicodeString pre, post;
      UnicodeString FFN = "";
      UnicodeString tmpFN = "";
      //������������ ������ ����� ��� SELECT-�
      while (itNode)
       {
        FFlDef = FRoot->GetTagByUID(itNode->AV["ref"]);
        FFN    = FClTabAlias + "." + GetFlName(FFlDef);
        tmpFN  = FFN;
        if (FFlDef->CmpName("choice"))
         {
          tmpFN = " (case " + FFN;
          TTagNode * itChVal = FFlDef->GetFirstChild();
          while (itChVal)
           {
            if (itChVal->CmpName("choicevalue"))
             tmpFN += " when " + itChVal->AV["value"] + " then '" + itChVal->AV["name"] + "'";
            itChVal = itChVal->GetNext();
           }
          tmpFN += " else '' end)";
         }
        else if (FFlDef->CmpName("choiceDB"))
         {
          TTagNode * refClDef = FRoot->GetTagByUID(FFlDef->AV["ref"]); //�������� �������� �������������� �� ������
          UnicodeString FRefClFrom = "";
          UnicodeString FRefClSelect = GetJoinClass(refClDef, FRefClFrom, FFN);

          tmpFN = " " + FRefClSelect + " ";
          AFrom += FRefClFrom;

          chBDInd++ ;
         }
        pre = post = "";
        if (itNode->AV["pre"].Length())
         pre = "'" + itNode->AV["pre"] + " '||";
        if (itNode->AV["post"].Length())
         post = "||' " + itNode->AV["post"] + "'";
        if (lDef->Count > 1)
         {
          if (RC.Length())
           RC += " ||' '|| case when " + tmpFN + " is null then '' else " + pre + tmpFN + post + " end ";
          else
           RC += " case when " + tmpFN + " is null then '' else " + pre + tmpFN + post + " end ";
         }
        else
         {
          if (RC.Length())
           RC += " ||' '|| " + pre + tmpFN + post;
          else
           RC += pre + tmpFN + post;
         }
        itNode = itNode->GetNext();
       }
      if (AFrom.Length())
       AFrom = " left join ( " + FClTabName + " " + FClTabAlias + " " + AFrom + ")";
      else
       AFrom = " left join " + FClTabName + " " + FClTabAlias + "";
      AFrom += " on (" + FClTabAlias + "." + GetKeyName(AClDef) + " = " + AFN + ") ";
     }
    __finally
     {
     }
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvClassif::GetClassSQL(TTagNode * AClDef, UnicodeString & AFrom, UnicodeString  AFN, int & AchBDInd)
 {
  UnicodeString RC = "";
  if (AClDef->CmpName("class"))
   {
    TTagNode * FFlDef;
    try
     {
      //�������� ������ ���� �� ������ ������������ �����
      TTagNode * lDef = AClDef->GetChildByName("description")->GetFirstChild();
      TTagNode * itNode = lDef->GetFirstChild();
      UnicodeString FClTabName = GetTabName(AClDef);
      UnicodeString FClTabAlias = "Cl" + IntToStr(AchBDInd) + AClDef->AV["uid"];
      UnicodeString pre, post;
      UnicodeString FFN = "";
      UnicodeString tmpFN = "";
      //������������ ������ ����� ��� SELECT-�
      if (!AFrom.Length() && !AFN.Length())
       AFrom = " From " + FClTabName + " " + FClTabAlias + " ";
      while (itNode)
       {
        FFlDef = FRoot->GetTagByUID(itNode->AV["ref"]);
        FFN    = FClTabAlias + "." + GetFlName(FFlDef);
        tmpFN  = FFN;
        if (FFlDef->CmpName("choice"))
         {
          tmpFN = " (case " + FFN;
          TTagNode * itChVal = FFlDef->GetFirstChild();
          while (itChVal)
           {
            if (itChVal->CmpName("choicevalue"))
             tmpFN += " when " + itChVal->AV["value"] + " then '" + itChVal->AV["name"] + "'";
            itChVal = itChVal->GetNext();
           }
          tmpFN += " else '' end)";
         }
        else if (FFlDef->CmpName("choiceDB"))
         {
          TTagNode * refClDef = FRoot->GetTagByUID(FFlDef->AV["ref"]); //�������� �������� �������������� �� ������
          UnicodeString FRefClFrom = "";
          UnicodeString FRefClSelect = GetClassSQL(refClDef, FRefClFrom, FFN, AchBDInd);

          tmpFN = " " + FRefClSelect + " ";
          AFrom += FRefClFrom;

          AchBDInd++ ;
         }
        pre = post = "";
        if (itNode->AV["pre"].Length())
         pre = "'" + itNode->AV["pre"] + " '||";
        if (itNode->AV["post"].Length())
         post = "||' " + itNode->AV["post"] + "'";
        if (lDef->Count > 1)
         {
          if (RC.Length())
           RC += " ||' '|| case when " + tmpFN + " is null then '' else " + pre + tmpFN + post + " end ";
          else
           RC += " case when " + tmpFN + " is null then '' else " + pre + tmpFN + post + " end ";
         }
        else
         {
          if (RC.Length())
           RC += " ||' '|| " + pre + tmpFN + post;
          else
           RC += pre + tmpFN + post;
         }
        itNode = itNode->GetNext();
       }
      if (AFN.Length())
       {
        AFrom = " left join " + FClTabName + " " + FClTabAlias + " " + AFrom;
        AFrom += " on (" + FClTabAlias + "." + GetKeyName(AClDef) + " = " + AFN + ") ";
       }
      else
       {
        RC = FClTabAlias + "." + GetKeyName(AClDef) + ", (" + RC + ") as Name ";
       }
     }
    __finally
     {
     }
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvClassif::GetKeyName(TTagNode * AClDef)
 {
  UnicodeString RC = "code";
  try
   {
    TTagNode * flDef = AClDef->GetChildByAV("", "comment", "key", true);
    if (flDef)
     {
      RC = flDef->AV["fl"];
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsSrvClassif::QuotedKey(TTagNode * AClDef)
 {
  bool RC = false;
  try
   {
    TTagNode * flDef = AClDef->GetChildByAV("", "comment", "key", true);
    if (flDef)
     {
      RC = flDef->CmpName("text,date");
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsSrvClassif::KeyFlExists(TTagNode * AClDef)
 {
  bool RC = false;
  try
   {
    TTagNode * flDef = AClDef->GetChildByAV("", "comment", "key", true);
    RC = flDef;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall GetClassifSQLData(TTagNode * ARoot, UnicodeString  AUID, TdsSrvClassifSQLData * ASQLs, UnicodeString  AAlias, UnicodeString  APref)
 {
  try
   {
    TdsSrvClassif * tmpSQLData = new TdsSrvClassif();
    try
     {
      tmpSQLData->CreateSQLs(ARoot, AUID, ASQLs, AAlias, APref);
     }
    __finally
     {
      delete tmpSQLData;
     }
   }
  catch (Exception & E)
   {
    throw E;
   }
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvClassif::FGetExtField(TTagNode * AFlDef, UnicodeString  AAlias)
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString FRegGUI = AFlDef->GetRoot()->GetChildByName("passport")->AV["GUI"];
    //������������ -----------------------------------------------------------
    if (FRegGUI.UpperCase() == "40381E23-92155860-4448")
     {
      if (AFlDef->CmpAV("uid", "00B2"))//�������, ���. �����
           RC += FCheckAddField("0031", AAlias + ".r0031");
      else if (AFlDef->CmpAV("uid", "10B4"))//�������, �������� // "30B4->3155
       {
        RC += FCheckAddField("1024", AAlias + ".r1024");
        RC += FCheckAddField("1155", AAlias + ".r1155");
       }
      else if (AFlDef->CmpAV("uid", "30B4"))//�������, �������� // "30B4->3155
       {
        RC += FCheckAddField("3024", AAlias + ".r3024");
        RC += FCheckAddField("3155", AAlias + ".r3155");
       }
      else if (AFlDef->CmpAV("uid", "10B5"))//�������, �����
       {
        RC += FCheckAddField("1031", AAlias + ".r1031");
        RC += FCheckAddField("1156", AAlias + ".r1156");
       }
      else if (AFlDef->CmpAV("uid", "1105"))//�������, ��������
       {
        RC += FCheckAddField("1104", AAlias + ".r1104");
        RC += FCheckAddField("1160", AAlias + ".r1160");
       }
      else if (AFlDef->CmpAV("uid", "32A9"))//�������, ���� �� ���������
       {
        RC += FCheckAddField("1088", AAlias + ".r1088");
        RC += FCheckAddField("31A5", AAlias + ".r31A5");
       }
      else if (AFlDef->CmpAV("uid", "32A8"))//�������, ����
       {
        RC += FCheckAddField("3088", AAlias + ".r3088");
        RC += FCheckAddField("32A5", AAlias + ".r32A5");
       }
      else if (AFlDef->CmpAV("uid", "0136"))//�����
       {
#ifdef REG_USE_KLADR
        RC += FCheckAddField("0100", AAlias + ".r0100");
        RC += FCheckAddField("0033", AAlias + ".r0033");
#endif
       }
      else if (AFlDef->CmpAV("uid", "0025"))//�����������
       {
#ifdef REG_USE_ORG
        RC += FCheckAddField("0026", AAlias + ".r0026");
        RC += FCheckAddField("0027", AAlias + ".r0027");
        RC += FCheckAddField("0028", AAlias + ".r0028");
        RC += FCheckAddField("0111", AAlias + ".r0111");
        RC += FCheckAddField("0112", AAlias + ".r0112");
#endif
       }
      else if (AFlDef->CmpAV("uid", "1118"))//��������������� ����::����
       {
        RC += FCheckAddField("1116", AAlias + ".r1116");
       }
      else if (AFlDef->CmpAV("uid", "1089"))//����::����
       {
        RC += FCheckAddField("1086", AAlias + ".r1086");
       }
      else if (AFlDef->CmpAV("uid", "32AA"))//����::���� ����������
       {
        RC += FCheckAddField("1086", AAlias + ".r1086");
       }
      else if (AFlDef->CmpAV("uid", "3089"))//����::����
       {
        RC += FCheckAddField("3086", AAlias + ".r3086");
       }
      else if (AFlDef->CmpAV("uid", "308B"))//����::��� ����������
       {
        //RC += FCheckAddField("0112", "r0112");
       }
      else if (AFlDef->CmpAV("uid", "3082"))//�����::�����������
       {
        //RC += FCheckAddField("0112", "r0112");
       }
      else if (AFlDef->CmpAV("uid", "31A2"))//��������������� ����::����� �����
       {
        RC += FCheckAddField("31A4", AAlias + ".r31A4");
       }
     }
    //��������������� --------------------------------------------------------
    else if (FRegGUI.UpperCase() == "549F0EBE-1B9B98A7-BD64")
     {
      if (AFlDef->CmpAV("uid", "0007, 0015, 002E"))//��������:�����, ���������:�����, ������:�����
       {
        //RC += FCheckAddField("0112", "r0112");
       }
      else if (AFlDef->CmpAV("uid", "0017"))//��������:������
       {
#ifdef REG_USE_DOC
        RC += FCheckAddField("0019", AAlias + ".F_FROMDATE");
        RC += FCheckAddField("001A", AAlias + ".F_TODATE");
#endif
       }
      else if (AFlDef->CmpAV("uid", "0023"))//�����������, �����
       {
#ifdef REG_USE_KLADR
#endif
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvClassif::FCheckAddField(UnicodeString  AFLDef, UnicodeString  AFLName)
 {
  UnicodeString RC = "";
  try
   {
    if (FSQLs->ListFields->IndexOf(AFLDef.UpperCase()) == -1)
     RC = ", " + AFLName;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
