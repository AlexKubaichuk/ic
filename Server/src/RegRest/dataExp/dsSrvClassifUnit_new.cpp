//---------------------------------------------------------------------------
//#include <vcl.h>
#pragma hdrstop

#include "dsSrvClassifUnit.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
__fastcall TdsSrvClassifSQLData::TdsSrvClassifSQLData()
{
  FCountSQL     = "";
  FSelectSQL    = "";
  FSelectRecSQL = "";
  FQuotedKey    = false;
  FInsertSQL    = "";
  FUpdateSQL    = "";
  FDeleteSQL    = "";
  FOrderSQL     = "";
  FFields       = new TStringList;
}
//---------------------------------------------------------------------------
__fastcall TdsSrvClassifSQLData::~TdsSrvClassifSQLData()
{
  delete FFields;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvClassifSQLData::FieldNameById(UnicodeString AId)
{
  UnicodeString RC = AId;
  try
   {
     TTagNode *FFlDef = FClDef->GetTagByUID(AId);
     if (FFlDef)
      {
        RC = "R"+FFlDef->AV["uid"];
        if (FFlDef->AV["fl"].Length())
         RC = FFlDef->AV["fl"];
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
__fastcall TdsSrvClassif::TdsSrvClassif()
{
}
//---------------------------------------------------------------------------
void __fastcall TdsSrvClassif::CreateSQLs(TTagNode *ARoot, UnicodeString AUID, TdsSrvClassifSQLData *ASQLs, UnicodeString AAlias, UnicodeString APref)
{
  FSQLs = ASQLs;
  FPref = APref;
  FAlias = AAlias;
  if (!FAlias.Length())
   FAlias = "C"+AUID;
  if (FAlias == "C1000")
   FAlias = "CB";
  FRoot = ARoot;
  FSQLs->Def = FRoot->GetTagByUID(AUID);
  if (!FSQLs->Def)
   throw Exception("����������� �������� ��������������. UID="+AUID);
  UnicodeString _EMPTYSTR_ = "";
  if (FSQLs->Def && FSQLs->Def->CmpName("class"))
   {
     FSQLs->TabAlias = FAlias;
     UnicodeString FKeyFL = GetKeyName(FSQLs->Def);

     UnicodeString tmpClassSQL = "";
     UnicodeString tmpClassFrom = "";
     int FChNum = 0;
     tmpClassSQL = GetClassSQL(FSQLs->Def, tmpClassFrom, "", FChNum);
     FSQLs->ClassSQL = "Select Distinct "+ tmpClassSQL+" "+tmpClassFrom;

     chBDInd = 0;

     UnicodeString FSelect, FListSel, FInsertF, FInsertV, FUpdate, FOrderSQL, tmpSel;
     FOrderSQL = "";
     if (FSQLs->Def->AV["tab"].Length())
      {
        if (KeyFlExists(FSQLs->Def))
         {
           FSelect  = "";
           FListSel = "";
           FInsertF = "";
           FInsertV = "";
         }
        else
         {
           FSelect  = FAlias+"."+FKeyFL;
           FListSel = FSelect;
           FInsertF = FKeyFL;
           FInsertV = "?"+FKeyFL;
         }
        FUpdate  = "";
      }
     else
      {
        FSelect  = FAlias+"."+FKeyFL+", "+FAlias+".CGUID";
        FListSel = FSelect;
        FInsertF = FKeyFL+", CGUID";
        FInsertV = "?"+FKeyFL+", ?CGUID";
        FUpdate  = "CGUID=?CGUID";
      }

     FSQLs->TabName = GetTabName(FSQLs->Def);
     UnicodeString FSelFrom  = FSQLs->TabName+" "+FAlias;
     UnicodeString FListFrom = FSelFrom;
     bool FChBDPres = false;

     FlList = new TStringList;
     try
      {
        FSQLs->Def->Iterate(GetClassFl, _EMPTYSTR_);
        TTagNode *flDef;
        UnicodeString FN, FullFN;
        for (int i = 0; i < FlList->Count; i++)
         {
           flDef = FSQLs->Def->GetTagByUID(FlList->Strings[i]);
           if (flDef)
            {
              FSQLs->Fields->Add(flDef->AV["uid"]);
              FN = GetFlName(flDef);
              FullFN = FAlias+"."+FN;

              if (FSelect.Length())  FSelect  += ", ";
              if (FListSel.Length()) FListSel += ", ";
              if (FInsertF.Length()) FInsertF += ", ";
              if (FInsertV.Length()) FInsertV += ", ";
              if (FUpdate.Length())  FUpdate  += ", ";

              FInsertF += FN;
              FInsertV += ":"+FN;
              FUpdate  += FN+"=:"+FN;
              if (flDef->CmpAV("inlist","l"))
               {
                 if (FOrderSQL.Length())
                  FOrderSQL+=", ";
                 FOrderSQL += FullFN;
               }

              if (flDef->CmpName("choice"))
               {
                 tmpSel = FullFN + ", (case "+FullFN;
                 TTagNode *itChVal = flDef->GetFirstChild();
                 while (itChVal)
                  {
                    if (itChVal->CmpName("choicevalue"))
                     tmpSel += " when "+itChVal->AV["value"]+" then '"+itChVal->AV["name"]+"'";
                    itChVal = itChVal->GetNext();
                  }
                 tmpSel += " else '' end ) as "+FN+"Str";
                 FSelect += tmpSel;
                 if (flDef->CmpAV("inlist","l"))
                  FListSel += tmpSel;
               }
              else if (flDef->CmpName("choiceDB"))
               {
                 FChBDPres = true;
                 TTagNode *refClDef = FRoot->GetTagByUID(flDef->AV["ref"]); // �������� �������� �������������� �� ������
                 UnicodeString FRefClFrom = "";
                 UnicodeString FRefClSelect = GetJoinClass(refClDef, FRefClFrom, FullFN);
                 tmpSel  = ", ("+FRefClSelect+") as "+FN+"Str";

                 FSelect += tmpSel;
                 FSelFrom +=  FRefClFrom;
                 if (flDef->CmpAV("inlist","l"))
                  {
                    FListSel += tmpSel;
                    FListFrom += FRefClFrom;
                  }
                 chBDInd++;
               }
              else
               {
                 FSelect   += FullFN;
                 if (flDef->CmpAV("inlist","l"))
                  FListSel += FullFN;
               }
            }
         }
        FSQLs->CountSQL       = "Select Count("+FAlias+"."+FKeyFL+") as RCount From "+FSelFrom;
        FSQLs->SelectRecSQL   = "Select distinct "+FSelect+" From "+FSelFrom + " Where "+FAlias+"."+FKeyFL+"=:p"+FKeyFL;
        FSQLs->SelectRec10SQL = "Select distinct "+FSelect+" From "+FSelFrom + " Where "+FAlias+"."+FKeyFL+" in (_KEY_REP_VAL_)";
        FSQLs->SelectSQL      = "Select distinct "+FSelect+" From "+FSelFrom;
        FSQLs->InsertSQL      = "Insert Into "+FSQLs->TabName+" ("+FInsertF+") Values ("+FInsertV+")";
        FSQLs->UpdateSQL      = "Update "+FSQLs->TabName+" Set "+FUpdate+" Where "+FKeyFL+"=:OLD_"+FKeyFL;
        FSQLs->DeleteSQL      = "Delete From "+FSQLs->TabName+" Where "+FKeyFL+"=:p"+FKeyFL;
        FSQLs->KeyFl          = FKeyFL;
        FSQLs->QuotedKey      = QuotedKey(FSQLs->Def);
        FSQLs->OrderSQL       = FOrderSQL;
      }
     __finally
      {
        delete FlList;
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TdsSrvClassif::GetClassFl(TTagNode *itNode, UnicodeString &UID)
{
  if (itNode->CmpName("binary,text,digit,date,choice,choiceDB,choiceTree,extedit"))
   FlList->Add(itNode->AV["uid"]);
  return false;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvClassif::GetTabName(TTagNode *ADefNode)
{
  UnicodeString RC = "CLASS_"+FPref+ADefNode->AV["uid"];
  try
   {
     if (ADefNode->AV["tab"].Length())
      RC = ADefNode->AV["tab"];
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvClassif::GetFlName(TTagNode *ADefNode)
{
  UnicodeString RC = "R"+ADefNode->AV["uid"];
  try
   {
     if (ADefNode->AV["fl"].Length())
      RC = ADefNode->AV["fl"];
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvClassif::GetJoinClass(TTagNode *AClDef, UnicodeString &AFrom, UnicodeString AFN)
{
  UnicodeString RC = "";
  if (AClDef->CmpName("class"))
   {
     TTagNode *FFlDef;
     try
      {
        // �������� ������ ���� �� ������ ������������ �����
        TTagNode *itNode = AClDef->GetChildByName("description")->GetFirstChild()->GetFirstChild();
        UnicodeString FClTabName  = GetTabName(AClDef);
        UnicodeString FClTabAlias = "Cl"+IntToStr(chBDInd)+AClDef->AV["uid"];
        UnicodeString FFN = "";
        // ������������ ������ ����� ��� SELECT-�
        while (itNode)
         {
           FFlDef = FRoot->GetTagByUID(itNode->AV["ref"]);
           FFN = FClTabAlias+"."+GetFlName(FFlDef);
           if (FFlDef->CmpName("choice"))
            {
              if (RC.Length()) RC += " || (case "+FFN;
              else             RC += " (case "+FFN;
              TTagNode *itChVal = FFlDef->GetFirstChild();
              while (itChVal)
               {
                 if (itChVal->CmpName("choicevalue"))
                  RC += " when "+itChVal->AV["value"]+" then '"+itChVal->AV["name"]+"'";
                 itChVal = itChVal->GetNext();
               }
              RC += " else '' end)";
            }
           else if (FFlDef->CmpName("choiceDB"))
            {
              TTagNode *refClDef = FRoot->GetTagByUID(FFlDef->AV["ref"]); // �������� �������� �������������� �� ������
              UnicodeString FRefClFrom = "";
              UnicodeString FRefClSelect = GetJoinClass(refClDef, FRefClFrom, FFN);
              if (RC.Trim().Length())
               RC  += " || ("+FRefClSelect+")";
              else
               RC  += FRefClSelect;

              AFrom += FRefClFrom;

              chBDInd++;
            }
           else
            {
              if (RC.Length()) RC += " || "+FFN;
              else             RC += FFN;
            }
           itNode = itNode->GetNext();
         }
        if (AFrom.Length())
         AFrom = " left join ( "+FClTabName+" "+FClTabAlias+" "+AFrom + ")";
        else
         AFrom = " left join "+FClTabName+" "+FClTabAlias+"";
        AFrom += " on ("+FClTabAlias+"."+GetKeyName(AClDef)+" = "+AFN+") ";
      }
     __finally
      {
      }
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvClassif::GetClassSQL(TTagNode *AClDef, UnicodeString &AFrom, UnicodeString AFN, int &AchBDInd)
{
  UnicodeString RC = "";
  if (AClDef->CmpName("class"))
   {
     TTagNode *FFlDef;
     try
      {
        // �������� ������ ���� �� ������ ������������ �����
        TTagNode *itNode = AClDef->GetChildByName("description")->GetFirstChild()->GetFirstChild();
        UnicodeString FClTabName  = GetTabName(AClDef);
        UnicodeString FClTabAlias = "Cl"+IntToStr(AchBDInd)+AClDef->AV["uid"];
        UnicodeString FFN = "";
        // ������������ ������ ����� ��� SELECT-�
        if (!AFrom.Length() && !AFN.Length())
          AFrom = " From "+FClTabName+" "+FClTabAlias+" ";
        while (itNode)
         {
           FFlDef = FRoot->GetTagByUID(itNode->AV["ref"]);
           FFN = FClTabAlias+"."+GetFlName(FFlDef);
           if (FFlDef->CmpName("choice"))
            {
              if (RC.Length()) RC += " ||' '|| (case "+FFN;
              else             RC += " (case "+FFN;
              TTagNode *itChVal = FFlDef->GetFirstChild();
              while (itChVal)
               {
                 if (itChVal->CmpName("choicevalue"))
                  RC += " when "+itChVal->AV["value"]+" then '"+itChVal->AV["name"]+"'";
                 itChVal = itChVal->GetNext();
               }
              RC += " else '' end)";
            }
           else if (FFlDef->CmpName("choiceDB"))
            {
              TTagNode *refClDef = FRoot->GetTagByUID(FFlDef->AV["ref"]); // �������� �������� �������������� �� ������
              UnicodeString FRefClFrom = "";
              UnicodeString FRefClSelect = GetClassSQL(refClDef, FRefClFrom, FFN, AchBDInd);
              if (RC.Length()) RC  += " ||' '|| "+FRefClSelect+" ";
              else             RC  += " "+FRefClSelect+" ";

              AFrom += FRefClFrom;

              AchBDInd++;
            }
           else
            {
              if (RC.Length()) RC += " ||' '|| "+FFN;
              else             RC += FFN;
            }
           itNode = itNode->GetNext();
         }
        if (AFN.Length())
         {
           AFrom = " left join "+FClTabName+" "+FClTabAlias+" "+AFrom ;
           AFrom += " on ("+FClTabAlias+"."+GetKeyName(AClDef)+" = "+AFN+") ";
         }
        else
         {
           RC = FClTabAlias+"."+GetKeyName(AClDef)+", ("+RC+") as Name ";
         }
      }
     __finally
      {
      }
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvClassif::GetKeyName(TTagNode *AClDef)
{
  UnicodeString RC = "code";
  try
   {
     TTagNode *flDef = AClDef->GetChildByAV("","comment","key",true);
     if (flDef)
      {
        RC = flDef->AV["fl"];
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsSrvClassif::QuotedKey(TTagNode *AClDef)
{
  bool RC = false;
  try
   {
     TTagNode *flDef = AClDef->GetChildByAV("","comment","key",true);
     if (flDef)
      {
        RC = flDef->CmpName("text,date");
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsSrvClassif::KeyFlExists(TTagNode *AClDef)
{
  bool RC = false;
  try
   {
     TTagNode *flDef = AClDef->GetChildByAV("","comment","key",true);
     RC = flDef;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall GetClassifSQLData(TTagNode *ARoot, UnicodeString AUID, TdsSrvClassifSQLData* ASQLs, UnicodeString AAlias, UnicodeString APref)
{
  try
   {
     TdsSrvClassif *tmpSQLData = new TdsSrvClassif();
     try
      {
        tmpSQLData->CreateSQLs(ARoot, AUID, ASQLs, AAlias, APref);
      }
     __finally
      {
        delete tmpSQLData;
      }
   }
  catch (Exception &E)
   {
     throw E;
   }
}
//---------------------------------------------------------------------------

