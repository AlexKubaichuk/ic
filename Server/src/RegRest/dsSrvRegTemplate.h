//---------------------------------------------------------------------------

#ifndef dsSrvRegTemplateH
#define dsSrvRegTemplateH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.JSON.hpp>
//#include <Controls.hpp>
//#include <ExtCtrls.hpp>
//#include <Mask.hpp>
//#include <DB.hpp>
#include "XMLContainer.h"
//#include "RegEd.h"
//#include <StdCtrls.hpp>
//---------------------------------------------------------------------------
class PACKAGE EdsRegTemplateError : public System::Sysutils::Exception
{
public:
     __fastcall EdsRegTemplateError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE TdsRegTemplate : public TObject
{
private:
  bool FUseUpperFunc;
  TJSONObject *FVals;
  bool FUseParams;
//        TExtBtnClick  FGetExtWhere;
        TTagNode *Root;
        TTagNode *FltFl;
        TStringList *FSelList;

        UnicodeString __fastcall GetGrSelect(TTagNode *AGroup);
        UnicodeString __fastcall FGetWhere(TTagNode *AitNode, UnicodeString AAlias);
//        bool __fastcall FGetSelectedUpdateSQL(TTagNode *itxTag, UnicodeString& Values);
        bool __fastcall FGetSelectedTextValues(TTagNode *itxTag, UnicodeString& Values);
        bool __fastcall FGetSelectedTextFieldValues(TTagNode *itxTag, UnicodeString& Values);
        bool __fastcall FSetTreeFormList(TTagNode *itxTag, UnicodeString& tmp);
        bool __fastcall FSetExtClick(TTagNode *itxTag, UnicodeString& tmp);
  UnicodeString __fastcall CorrectDig(UnicodeString ASrc);
//  System::UnicodeString __fastcall JSONToString(TJSONObject *AValue, UnicodeString AId, UnicodeString ADef = "");
public:   // User declarations
    __fastcall TdsRegTemplate(UnicodeString AFields, TTagNode *ATree, TJSONObject *AValues);
    __fastcall ~TdsRegTemplate();

//    UnicodeString    __fastcall GetValue(UnicodeString AUID);
    UnicodeString    __fastcall GetWhere(UnicodeString AAlias = "");
    UnicodeString    __fastcall GetWhereClRep(UnicodeString AAlias, TStringList *ASelList);
//    UnicodeString    __fastcall GetSelectedTextValues(bool CanFieldName = false);
//    UnicodeString    __fastcall GetSelectedUpdateSQL();

    __property bool UseUpperFunc = {read=FUseUpperFunc, write=FUseUpperFunc};
    __property bool UseParams = {read=FUseParams, write=FUseParams};
    __property TJSONObject *Values = {read=FVals};

//    __property TExtBtnClick OnGetExtWhere = {read=FGetExtWhere, write=FGetExtWhere};
};
//---------------------------------------------------------------------------
#endif
