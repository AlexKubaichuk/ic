//---------------------------------------------------------------------------

#ifndef DataExchangeH
#define DataExchangeH
//---------------------------------------------------------------------------
#include <System.hpp>
#include <System.SysUtils.hpp>
//#include <Vcl.ExtCtrls.hpp>
#include "XMLContainer.h"
#include "dsDICommon.h"
#include "dsKLADRUnit.h"
//#include "diDataTypeDef.h"
//#include "ICSDBConnect.h"
//---------------------------------------------------------------------------

namespace DataExchange
{
//###########################################################################
//##                                                                       ##
//##                               ����������                              ##
//##                                                                       ##
//###########################################################################


//---------------------------------------------------------------------------

// ����������� ��� ������ ���������� TDataImporter � ��������

class EImportTransactionError : public Exception
{
public:
    __fastcall EImportTransactionError(const UnicodeString Msg) : Exception(Msg) {}
};

//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                             TDataImporter                             ##
//##                                                                       ##
//###########################################################################

//class TDataTable;
//---------------------------------------------------------------------------

// �������� ������� ������ � �������

//###########################################################################
//##                                                                       ##
//##                           TDataImporter                               ##
//##                                                                       ##
//###########################################################################
class TDataImporter : public TObject
{
private:
    UnicodeString  FSystemVersion;
    TdsDICommon      *FDM;
    TFDConnection *FConnection;
    TdsKLADRClass *FKLADR;
    TAnsiStrMap   FClsData;
    TDateTime     FImportBegTime;
    void __fastcall PrInit(int AMax, UnicodeString AMsg);
    void __fastcall PrCh(int APercent, UnicodeString AMsg);
    void __fastcall PrComp(UnicodeString AMsg);
    bool __fastcall FGetDBName(const UnicodeString AMessage, UnicodeString &AName, UnicodeString &ALogin, UnicodeString &APwd);

    TdsDIOnExtGetCodeEvent __fastcall FGetOnExtGetCode();
    void __fastcall FSetOnExtGetCode(TdsDIOnExtGetCodeEvent AVal);

    TDIImportCardDataEvent __fastcall FGetOnImportCard();
    void __fastcall FSetOnImportCard(TDIImportCardDataEvent AVal);


    TDataTable* __fastcall FGetClassifier(const UnicodeString AUID);
//    TDataTable* __fastcall FGetUnit(const UnicodeString AUID);
//    TDataTable* __fastcall FGetMainUnit();
    bool __fastcall FSetExtEditValue(TDataTable *ATable, TTagNode *ADefNode, UnicodeString AVal, UnicodeString AExtVal, TJSONObject *ACls = NULL);
    Variant __fastcall FGetClassValue(TTagNode *ADefNode, TJSONObject *ACls, const Variant &AValue);
    UnicodeString __fastcall FImportCard(UnicodeString AId, UnicodeString APtId, TJSONObject *AValues, UnicodeString APtCode, int &ACount);
public:
    __fastcall TDataImporter(TFDConnection *AConnection, const UnicodeString ARegGUI, const UnicodeString AImpGUI, TAxeXMLContainer *AXMLList);
    virtual __fastcall ~TDataImporter();

    // EConnectionError - ����������� ��� ������ ����������
    void __fastcall Connect(const UnicodeString ARegGUI, const UnicodeString AImpGUI);
    // �������� ���������� � ��������
    void __fastcall Disconnect();

    UnicodeString __fastcall NewGUID();

//    //���������� ��� �������� �� ���������� ������
//    bool __fastcall GetUnitCodeByParam(TTagNode *AUnitDefNode, __int64 &AUCode);

    // ������ ������ � ��������������� ������� �� �������
    // ERequaredFieldsError - �����������, ���� �� ��� ���� ������������
    //                        ����� ������� ��������
    // EUniqueInstanceError - ����������� ��� ��������� ������������
    //                        ������������� ������

//  System::UnicodeString __fastcall InsertData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString __fastcall InsertOrUpdateData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString __fastcall ImportData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId, UnicodeString & AResMessage);

//    // ������ �������
    __property UnicodeString SystemVersion = {read = FSystemVersion};
    // ������ TDataTable ��� �������������� � ��������� UID
    __property TDataTable* Classifiers[const UnicodeString AUID] = {read = FGetClassifier};
    // ������ TDataTable ��� ������� ���������������� �����
//    __property TDataTable* MainUnit = {read = FGetMainUnit};
    // ������ TDataTable ��� ����� � ��������� UID
//    __property TDataTable* Units[const UnicodeString AUID] = {read = FGetUnit};

    __property TdsDIOnExtGetCodeEvent OnExtGetCode = {read = FGetOnExtGetCode, write = FSetOnExtGetCode};
    __property TDIImportCardDataEvent OnImportCard = {read = FGetOnImportCard, write = FSetOnImportCard};
    __property TdsKLADRClass*  KLADR = {read = FKLADR, write = FKLADR};

};

//---------------------------------------------------------------------------
}; // end of namespace DataExchange
using namespace DataExchange;

#endif

