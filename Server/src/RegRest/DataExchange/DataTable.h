//---------------------------------------------------------------------------

#ifndef DataTableH
#define DataTableH

#include <System.Classes.hpp>

#include "dsDICommon.h"
#include "DataField.h"
#include "diDataTypeDef.h"
//---------------------------------------------------------------------------

namespace DataExchange
{
typedef Variant __fastcall (__closure *TFieldEditGetClassValueEvent)(TTagNode *ADefNode, TJSONObject *ACls, const Variant &AValue);
//---------------------------------------------------------------------------
// �����������, ���� ������ ��� ������� ������
class EImportError : public Exception
{
public:
    __fastcall EImportError(const UnicodeString Msg) : Exception(Msg) {}
};

//---------------------------------------------------------------------------
// �����������, ���� �� ��� ���� ������������ ����� ������ ������� ��������
class ERequaredFieldsError : public Exception
{
public:
    __fastcall ERequaredFieldsError(const UnicodeString Msg) : Exception(Msg) {}
};

//---------------------------------------------------------------------------
// ����������� ��� ��������� ������������ ������������� ������
class EUniqueInstanceError : public Exception
{
public:
    __fastcall EUniqueInstanceError(const UnicodeString Msg) : Exception(Msg) {}
};

//---------------------------------------------------------------------------


// ������ ������� ��
class PACKAGE TDataTable : public TObject
{
private:
    UnicodeString emptyStr;
    TdsDICommon *FDM;
    UnicodeString FTableName, FPKName, FLinkKeyName,FUID;
    TDataFieldMap FFields;
    TDataFieldMap FRUFields;
    TDataFieldMap FUIDFields;
    UnicodeString FClearCodes;
//    TdsDICommon      *FDM;
    TDISetExtEditValueEvent FOnSetExtEditValue;
    TFieldEditGetClassValueEvent FOnGetClassCodeValue;

//    TFDQuery *FQ;
//    TFDQuery *FImpQ;
    bool      FDelegated;
    TTagNode *FDataDef;
    TTagNode *FKeyScaleDef;
    TTagNode *FLinkKeyScaleDef;
//    TTagNode *FCGUIDScaleDef;
    TDataEncodeTableType FTableType;
    TFieldEditErrorEvent evtEditError;
    TRequaredFieldErrorEvent evtReqError;
    TDataField* __fastcall GetField(UnicodeString AFieldName);
    TDataField* __fastcall TryGetField(UnicodeString AFieldName);
    bool __fastcall FCreateFields(TTagNode *uiTag, UnicodeString &UID);
    UnicodeString __fastcall GetNewCode(UnicodeString AOldCode, bool &AIsAppend, UnicodeString UCODE = "");
  bool __fastcall CheckUnique(bool AIsAppend, UnicodeString &ANewCode, bool AImport = false);
  UnicodeString __fastcall FSimpleCheckUnique(TTagNode *ADescription, TTagNode *ANode, bool AIsAppend, UnicodeString &ANewCode, bool AImport = false);
public:
    // ADataDef - �������� �������
    // ������ ��������� � ���������������� ������, ��� ����� ���������������
    // �������� �� ���������
    __fastcall TDataTable(TdsDICommon *ADM, TTagNode* ADataDef);
    virtual __fastcall ~TDataTable();

    UnicodeString __fastcall GetValue(UnicodeString AOldKey, UnicodeString AFieldUID);
    bool __fastcall FieldExists(UnicodeString AFieldName);

    __property TdsDICommon *DM = {read = FDM};

//    __property TFDQuery *FieldQuery = {read = FQ};

    __property UnicodeString TableName = {read = FTableName};
    // ������������ ���� � ���������� ��������������� ������
    __property UnicodeString PKName = {read = FPKName};
    __property UnicodeString LinkKeyName = {read = FLinkKeyName};

    __property TDISetExtEditValueEvent OnSetExtEditValue = {read = FOnSetExtEditValue, write=FOnSetExtEditValue};
    __property TFieldEditGetClassValueEvent OnGetClassCodeValue = {read = FOnGetClassCodeValue, write=FOnGetClassCodeValue};

    // ������ � ����� ������ �� ������������, �� �������� ������������, UID-�
    __property TDataField* Fields[UnicodeString AName] = {read = GetField};

    // ��������� ������� �������� ��� ���� ������������ �����
    // ��� ������� ������������� ���� ��� �������� ��������� ������� OnRequaredFieldError
    // ���� ���������� ������� OnRequaredFieldError �� ����������, �� �����������
    // ���������� ERequaredFieldsError
    bool __fastcall CheckRequared();
    // ����������� � ���� �������� �� ���������
    void __fastcall New(TJSONObject *AValue = NULL, TJSONObject *ACls = NULL, bool AImport = false);
    // ��������� ������ � ��
    UnicodeString __fastcall InsertOrUpdate(UnicodeString &ARecId);
    UnicodeString __fastcall Import(UnicodeString &ARecId);

    // ������� ������� ������������ ������
    __property bool Delegated = {read = FDelegated};

    // ��������� ��� ������� ��������� ���� ������������ ��������
    // ��������� �� ���������� ������� �������� � �������, �� ��� ������ ��������
    // �������� ���������� ��������� ����������� ���� ����� ������
    __property TFieldEditErrorEvent OnFieldEditError = {read = evtEditError, write = evtEditError};
    // ��������� ������� CheckRequared() ���� ��� ������������� ���� �� �������
    // ��������
    __property TRequaredFieldErrorEvent OnRequaredFieldError = {read = evtReqError, write = evtReqError};

    __property UnicodeString ClearCodes = {read = FClearCodes, write = FClearCodes};
};
}; // end of namespace DataExchange
using namespace DataExchange;
#endif
