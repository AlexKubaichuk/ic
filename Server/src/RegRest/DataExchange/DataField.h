//---------------------------------------------------------------------------

#ifndef DataFieldH
#define DataFieldH
//#include "DataExchange.h"
//#include "DataScale.h"
#include "diDataTypeDef.h"

#include "dsDIEdit.h"
#include <System.JSON.hpp>
//!!!*#include "EncodeTable.h"
//---------------------------------------------------------------------------
namespace DataExchange
{
//---------------------------------------------------------------------------
typedef void __fastcall (__closure *TFieldEditErrorEvent)(TObject *ASender, TDataField *AField, const Variant &AValue, const TFieldEditErrorType AErrorType);
typedef void __fastcall (__closure *TRequaredFieldErrorEvent)(TObject *ASender, TDataField *AField);

//---------------------------------------------------------------------------
class EFieldError : public Exception
{
private:
    TDataField *FField;

public:
    __fastcall EFieldError(const UnicodeString Msg, TDataField *AField) : Exception(Msg)
    {
      FField = AField;
    }
    __property TDataField *Field = {read = FField};
};

//---------------------------------------------------------------------------

// ����������� ��� ������� ��������� ���� ������������ ��������
// AField     - ����
// AValue     - ������������� ��������
// AErrorType - ��� ������
class EFieldEditError : public EFieldError
{
private:
    Variant FValue;
    TFieldEditErrorType FErrorType;

public:
    __fastcall EFieldEditError(
      const UnicodeString Msg,
      TDataField *AField,
      const Variant &AValue,
      const TFieldEditErrorType AErrorType
    ) : EFieldError(Msg, AField)
    {
      FValue = AValue;
      FErrorType = AErrorType;
    }
    __property Variant Value = {read = FValue};
    __property TFieldEditErrorType ErrorType = {read = FErrorType};
};
//---------------------------------------------------------------------------
// ����������� ��� ������� ��������� �������� disabled-����
class EFieldDisabled : public EFieldError
{
public:
    __fastcall EFieldDisabled(const UnicodeString Msg, TDataField *AField) : EFieldError(Msg, AField) {};
};
//---------------------------------------------------------------------------
class PACKAGE TDataField : public TObject
{
private:
   TDataTable *FOwner;
   UnicodeString FName;
   Variant FValue;
   TTagNode   *FFieldDef;
//   TDataScale *FScale;
   bool FKeyField;
   Variant FDefaultValue;
//   TdsDICommon*     FDM;
   TdsDIEdit*     FSource;
   bool        CanSetValue;
   bool FReq, FEditable, FEnabled, FDelegated;
   TDataEncodeTable* FEncodeTable;
   TFieldEditErrorEvent evtFieldEditErr;
   TDataTable* __fastcall FGetOwner();
   Variant    __fastcall GetValAsVariant();
   bool       __fastcall GetValAsBool();
   TDateTime  __fastcall GetValAsDateTime();
   double     __fastcall GetValAsFloat();
   int        __fastcall GetValAsInteger();
   UnicodeString __fastcall GetValAsString();
   bool       __fastcall FGetEnabled();

   void __fastcall FSetValue(Variant AValue);
   void __fastcall SetValAsVariant(Variant AValue);
   void __fastcall SetValAsBool(bool AValue);
   void __fastcall SetValAsDateTime(TDateTime AValue);
   void __fastcall SetValAsFloat(double AValue);
   void __fastcall SetValAsInteger(int AValue);
   void __fastcall SetValAsString(UnicodeString AValue);
   void __fastcall CreateScale();

public:
    // ���������� ��������� :)
    void __fastcall    ImpEDSetValue(Variant AVal);
    Variant __fastcall ImpEDGetValue();
    UnicodeString __fastcall AsTypeCheck(bool &ANull, bool &AEmpty);


    // AFieldDef - �������� ����
    __fastcall TDataField(TDataTable *AOwner, TTagNode* AFieldDef);
    virtual __fastcall ~TDataField();
    // �������� �� ������� �������� ��� ������������� ����
    bool __fastcall CheckRequared();

    __property TDataTable* Owner = {read = FGetOwner};

    __property TDataEncodeTable* EncodeTable = {read = FEncodeTable};
    // ������������ ����
    __property UnicodeString Name = {read = FName};
    // �����
    __property Variant        Default = { read = FDefaultValue, write = FDefaultValue };
    // ������� �������������� ������� ��������
    __property bool Requared = {read = FReq};
    // ������� ���������������
    __property bool Editable = {read = FEditable};
    // ������� ��������� ����
    __property bool IsKey = {read = FKeyField, write=FKeyField};
    // (� ������������ � ��������� ������������)
    __property bool IntEn = {read = FEnabled, write = FEnabled};
    __property bool Enabled = {read = FGetEnabled};
    // ������� ������������ ������
    __property bool Delegated = {read = FDelegated};

    __property TTagNode* DefNode = {read = FFieldDef};


    // ��� ������� ��������� ���� ������������ �������� ��������� �������
    // OnFieldEditError
    // ���� ���������� ������� OnFieldEditError �� ����������, �� �����������
    // ���������� EFieldEditError
    // ��� ������� ��������� �������� disabled-���� ����������� ����������
    // EFieldDisabled

    __property Variant    Value       = {read = GetValAsVariant, write = SetValAsVariant};
    __property bool       AsBoolean   = {read=GetValAsBool,      write = SetValAsBool};
    __property TDateTime  AsDateTime  = {read=GetValAsDateTime,  write = SetValAsDateTime};
    __property double     AsFloat     = {read=GetValAsFloat,     write = SetValAsFloat};
    __property int        AsInteger   = {read=GetValAsInteger,   write = SetValAsInteger};
    __property UnicodeString AsString = {read=GetValAsString,    write = SetValAsString};

    // �������� �������� �� NULL
    bool __fastcall IsNull();
    // ��������� �������� � NULL
    void __fastcall Clear();

    // ��������� ��� ������� ��������� ���� ������������ ��������
    __property TFieldEditErrorEvent OnFieldEditError = {read = evtFieldEditErr, write = evtFieldEditErr};
};
}; // end of namespace DataExchange
using namespace DataExchange;
//---------------------------------------------------------------------------
#endif

