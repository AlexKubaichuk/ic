// ---------------------------------------------------------------------------
#pragma hdrstop
#include "DataExchange.h"
// !!!*#include "EncodeTable.h"
#include "srvsetting.h"
#ifndef _CONFSETTTING
#error _CONFSETTTING using are not defined
#endif
#include "DataTable.h"
#include "dsDIEdit.h"
#ifdef REG_USE_KLADR
#include "KLADRParsers.h"
#endif
#ifdef REG_USE_ORG
#include "OrgParsers.h"
#endif
#include "JSONUtils.h"
#include <System.DateUtils.hpp>
#include "TimeChecker.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                           TDataImporter                               ##
// ##                                                                       ##
// ###########################################################################
// ---------------------------------------------------------------------------
__fastcall TDataImporter::TDataImporter(TFDConnection * AConnection, const UnicodeString ARegGUI,
  const UnicodeString AImpGUI, TAxeXMLContainer * AXMLList)
 {
  dsLogC(__FUNC__);
  FConnection = AConnection;
  FDM         = new TdsDICommon(FConnection, AXMLList);
  FDM->Connect(ARegGUI, AImpGUI);
  FImportBegTime = Time();
  dsLogC(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
__fastcall TDataImporter::~TDataImporter()
 {
  dsLogD(__FUNC__);
  delete FDM;
  // delete FEncodeTable;
  dsLogD(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
void __fastcall TDataImporter::Connect(const UnicodeString ARegGUI, const UnicodeString AImpGUI)
 {
  dsLogBegin(__FUNC__);
  if (!FDM)
   throw EConnectionError("������ �������� ������ ������");
  FDM->Connect(ARegGUI, AImpGUI);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TDataImporter::Disconnect()
 {
  dsLogBegin(__FUNC__);
  FDM->Disconnect();
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
// #include <rpc.h>
UnicodeString __fastcall TDataImporter::NewGUID()
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "error";
  try
   {
    RC = icsNewGUID().UpperCase();
    dsLogMessage("GUIN = " + RC, __FUNC__);
   }
  __finally
   {
    dsLogEnd(__FUNC__);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDataImporter::PrInit(int AMax, UnicodeString AMsg)
 {
  /* MsgLab->Caption = AMsg;
   MsgLab->Update();
   PrBar->Properties->Max = AMax;
   PrBar->Position = 0;
   Application->ProcessMessages(); */
 }
// ---------------------------------------------------------------------------
void __fastcall TDataImporter::PrCh(int APercent, UnicodeString AMsg)
 {
  /* if (AMsg.Length())
   {
   MsgLab->Caption = AMsg;
   MsgLab->Update();
   }
   if (APercent != -1)
   PrBar->Position += APercent;
   Application->ProcessMessages(); */
 }
// ---------------------------------------------------------------------------
void __fastcall TDataImporter::PrComp(UnicodeString AMsg)
 {
  /* MsgLab->Caption = "";
   MsgLab->Update();
   PrBar->Properties->Max = 0;
   PrBar->Position = 0;
   Application->ProcessMessages(); */
 }
// ---------------------------------------------------------------------------
bool __fastcall TDataImporter::FGetDBName(const UnicodeString AMessage, UnicodeString & AName, UnicodeString & ALogin,
  UnicodeString & APwd)
 {
  dsLogBegin(__FUNC__);
  dsLogEnd(__FUNC__);
  throw EConnectionError(AMessage);
 }
// ---------------------------------------------------------------------------
TDataTable * __fastcall TDataImporter::FGetClassifier(const UnicodeString AUID)
 {
  dsLogBegin(__FUNC__);
  dsLogEnd(__FUNC__);
  return FDM->Classifiers[AUID];
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TDataImporter::InsertOrUpdateData(System::UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������.";
  try
   {
    try
     {
//      __int64 memsize = PrintProcessMemoryInfo().ToIntDef(0);
      // <unitgroup name='���' uid='00B5'>
      TDataTable * FImpUnit = Classifiers[AId];
      try
       {
        FImpUnit->OnSetExtEditValue = FSetExtEditValue;
//        dsLogMessage("1. Classifiers["+AId+"] Size: " + IntToStr(PrintProcessMemoryInfo().ToIntDef(0) - memsize), __FUNC__);
        FImpUnit->New(AValue, NULL, false);
//        dsLogMessage("2. New Size: " + IntToStr(PrintProcessMemoryInfo().ToIntDef(0) - memsize), __FUNC__);
        RC = FImpUnit->InsertOrUpdate(ARecId);
//        dsLogMessage("3. InsertOrUpdate Size: " + IntToStr(PrintProcessMemoryInfo().ToIntDef(0) - memsize), __FUNC__);
       }
      __finally
       {
        FImpUnit->OnSetExtEditValue = NULL;
        FDM->DeleteClassifier(AId);
       }
//      dsLogMessage("4. finally Size: " + IntToStr(PrintProcessMemoryInfo().ToIntDef(0) - memsize), __FUNC__);
     }
    catch (Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TDataImporter::ImportData(System::UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId, UnicodeString & AResMessage)
 {
  dsLogBegin(__FUNC__);
  AResMessage = "";
  dsLogMessage("ImportData: ID: " + AId + ", Value: " + AValue->ToString(), __FUNC__, 1);
  UnicodeString RC = "������.";
  TTimeChecker * FTimeCheck = new TTimeChecker;
  TDataTable * FImpUnit = NULL;
  bool PatExist = false;
  // TJSONObject * FValue = AValue;
  // TJSONObject * FValue = (TJSONObject *)TJSONObject::ParseJSONValue(AValue->ToString());
  if (AValue)
   {
    try
     {
      try
       {
        FTimeCheck->Check();
        TJSONObject * FPtData = (TJSONObject *)GetJSONValue(AValue, "pt");
        UnicodeString FSyncType = JSONToString(AValue, "synctype", "");
        if (FPtData)
         {
          if (JSONToString(FPtData, "001f").Length() && JSONToString(FPtData, "0020").Length())
            // && JSONToString(FPtData, "002f").Length()
           {
            // ����������� ��������
            dsLogMessage("Import patient begin", __FUNC__, 1);
            FImpUnit                      = Classifiers["1000"];
            PatExist                      = true;
            FImpUnit->OnSetExtEditValue   = FSetExtEditValue;
            FImpUnit->OnGetClassCodeValue = FGetClassValue;
            if (FSyncType.LowerCase() == "code")
             FImpUnit->ClearCodes = "0026, 0027, 0028, 0111, 0112, 011B, 0025, 00F0";
            FImpUnit->New(FPtData, (TJSONObject *)GetJSONValue(AValue, "cl"), true);
            RC                          = FImpUnit->Import(ARecId);
            FImpUnit->OnSetExtEditValue = NULL;
            FTimeCheck->Check(" / �������");
            dsLogMessage("Import patient end, new id = " + ARecId, __FUNC__, 1);
            // ����������� �����
            TJSONObject * FCards = (TJSONObject *)GetJSONValue(AValue, "cd");
            if (FCards)
             { // ����� ������������
              TJSONObject * FValue = (TJSONObject *)TJSONObject::ParseJSONValue(AValue->ToString());
              FCards = (TJSONObject *)GetJSONValue(FValue, "cd");
              try
               {
                TJSONObject * F3003Card = (TJSONObject *)GetJSONValue(FCards, "3003");
                DeleteJSONPair(FCards, "1003");
                if (F3003Card)
                 {
                  TJSONObject * FPrivRecData;
                  TJSONObject * FRec;
                  TJSONObject * FPrivData = new TJSONObject;
                  FCards->AddPair("1003", FPrivData);
                  TJSONPairEnumerator * itRec = F3003Card->GetEnumerator();
                  while (itRec->MoveNext())
                   {
                    FRec         = (TJSONObject *)itRec->Current->JsonValue;
                    FPrivRecData = new TJSONObject;
                    FPrivData->AddPair(JSONToString(FRec, "CODE"), FPrivRecData);
                    FPrivRecData->AddPair("CODE", JSONCopy(FRec, "CODE", true));
                    // <extedit name='��� ��������' uid='317A' ref='1000' depend='317A.317A'    >
                    FPrivRecData->AddPair("017A", JSONCopy(FRec, "317A", true));
                    // <extedit name='��� ��������' uid='317A' ref='1000' depend='317A.317A'    >
                    // <extedit name='��� ��������' uid='3183' ref='3003' depend='3183.3183'    >
                    FPrivRecData->AddPair("32AE", JSONCopy(FRec, "32AD", true));
                    // <extedit name='��� �����' uid='32AD' ref='3084' depend='32AD.32AD'    >
                    FPrivRecData->AddPair("1024", JSONCopy(FRec, "3024", true));
                    // <date name='���� ����������' uid='3024' required='1' inlist='l' default='CurrentDate'/>
                    FPrivRecData->AddPair("10B4", JSONCopy(FRec, "30B4", true));
                    // <extedit name='�������' uid='30B4' comment='calc' inlist='l' depend='3024'   length='9' >
                    FPrivRecData->AddPair("1020", JSONCopy(FRec, "3020", true));
                    // <choiceDB name='����' uid='3020' ref='0035' required='1' inlist='l' depend='3075.002D' >
                    FPrivRecData->AddPair("1021", JSONCopy(FRec, "3185", true));
                    // <text name='��� �� ���������' uid='3185' cast='no' fixedvar='0' format='' length='255' >
                    FPrivRecData->AddPair("1075", new TJSONNull);
                    // <choiceDB name='��������' uid='1075' ref='003A' required='1' depend='1020.002D' >
                    FPrivRecData->AddPair("1022", JSONCopy(FRec, "3022", true));
                    // <digit name='����' uid='3022' digits='5' decimal='4' min='0' max='99999' default='006A' units=''/>
                    FPrivRecData->AddPair("1023", JSONCopy(FRec, "3023", true));
                    // <text name='�����' uid='3023' cast='no' fixedvar='0' format='' length='20' >
                    FPrivRecData->AddPair("1025", JSONCopy(FRec, "3025", true));
                    // <binary name='������� ���������' uid='3025' required='1' default='uncheck'/>
                    FPrivRecData->AddPair("1026", JSONCopy(FRec, "3026", true));
                    // <binary name='���� ����� �������' uid='3026' required='1' default='uncheck'>
                    FPrivRecData->AddPair("1028", JSONCopy(FRec, "3028", true));
                    // <binary name='���� ������� �������' uid='3028' required='1' default='uncheck'>
                    FPrivRecData->AddPair("10A8", JSONCopy(FRec, "30A8", true));
                    // <choiceDB name='������ �������' uid='30A8' comment='empty' ref='0038' required='1' depend='3020.0039' linecount=''>
                    FPrivRecData->AddPair("1027", JSONCopy(FRec, "3027", true));
                    // <digit name='�������� �������' uid='3027' ref='008E' required='1'
                    FPrivRecData->AddPair("10AE", JSONCopy(FRec, "30AE", true));
                    // <binary name='������� ��������' uid='30AE' required='1' default='uncheck'/>
                    FPrivRecData->AddPair("1092", new TJSONNull);
                    // <extedit name='�����' uid='1092' required='1'    length='9' >
                    FPrivRecData->AddPair("10AF", JSONCopy(FRec, "30AF", true));
                    // <choice name='��� ������' uid='30AF' default='0'>
                    FPrivRecData->AddPair("017F", JSONCopy(FRec, "317F", true));
                    // <choiceDB name='���' uid='317F' ref='00C7' />
                    FPrivRecData->AddPair("101F", JSONCopy(FRec, "301F", true));
                    // <binary name='��������� � ������ ���' uid='301F' required='1' invname='��������� � ������ �����������' default='check'/>
                    FPrivRecData->AddPair("10B0", JSONCopy(FRec, "30B0", true));
                    // <choiceDB name='����' uid='30B0' ref='00AA' depend='317F.00AA;3180.00AA' />
                    FPrivRecData->AddPair("0181", JSONCopy(FRec, "3181", true));
                    // <choiceDB name='���. ������' uid='3181' ref='00AA' depend='317F.00AA;3180.00AA' />
                    FPrivRecData->AddPair("10B1", JSONCopy(FRec, "30B1", true));
                    // <choiceDB name='�������� ��������������' uid='30B1' ref='00AB' />
                    FPrivRecData->AddPair("0182", JSONCopy(FRec, "3182", true));
                    // <choiceDB name='���. ����������' uid='3182' ref='00BE' />
                    FPrivRecData->AddPair("1155", JSONCopy(FRec, "3155", true));
                    // <date name='�� ��������' uid='3155' />
                   }
                  DeleteJSONPair(FCards, "3003");
                 }
                int FRecCount;
                UnicodeString FCardRecId = ARecId;
                // ("3003", "317A", FCards, FCardRecId); // FAddCardData("3003", "317A", FCardRecId, JRC); //��������
                FImportCard("1003", "017A", FCards, FCardRecId, FRecCount);
                // FAddCardData("1003", "017A", FCardRecId, JRC); //�������� (�� ���������)
                FTimeCheck->Check(" / [" + IntToStr(FRecCount) + "]��������");
                FImportCard("102F", "017B", FCards, FCardRecId, FRecCount);
                // FAddCardData("102F", "017B", FCardRecId, JRC); //�����
                FTimeCheck->Check(" / [" + IntToStr(FRecCount) + "]�����");
                FImportCard("1100", "1101", FCards, FCardRecId, FRecCount);
                // FAddCardData("1100", "1101", FCardRecId, JRC); //��������
                FTimeCheck->Check(" / [" + IntToStr(FRecCount) + "]��������");
                FImportCard("3030", "317C", FCards, FCardRecId, FRecCount);
                // FAddCardData("3030", "317C", FCardRecId, JRC); //������
                FTimeCheck->Check(" / [" + IntToStr(FRecCount) + "]������");
                // ("1030", "017C", FCards, FCardRecId); // FAddCardData("1030", "017C", FCardRecId, JRC); //������ (�� ���������)
                // ("1112", "1113", FCards, FCardRecId); // FAddCardData("1112", "1113", FCardRecId, JRC); //��������������� ����
                // TDataTable *FImpUnit = Classifiers["1000"];
                // FImpUnit->OnSetExtEditValue = FSetExtEditValue;
                // FImpUnit->New(JSONPairValue(FValue->Pairs[0]), true);
                // RC = FImpUnit->Import(FCardRecId);
                // FImpUnit->OnSetExtEditValue = NULL;
               }
              __finally
               {
                delete FValue;
               }
             }
           }
          else
           { // ������
            if (AValue)
             dsLogError("������ ������� ������ ��������: " + AValue->ToString(), __FUNC__);
            else
             dsLogError("������ ������� ������ ��������", __FUNC__);
           }
         }
        else
         {
          FImpUnit                      = Classifiers[AId];
          FImpUnit->OnSetExtEditValue   = FSetExtEditValue;
          FImpUnit->OnGetClassCodeValue = FGetClassValue;
          TJSONObject * FRec = (TJSONObject *)GetJSONValue(AValue, "rec");
          TJSONObject * FCls = (TJSONObject *)GetJSONValue(AValue, "cl");
          if (FRec)
           {
            FImpUnit->New(FRec, FCls, true);
            RC = FImpUnit->Import(ARecId);
           }
          else
           {
            FImpUnit->New(AValue, NULL, true); // ���������� ������, ��� ���������������� ����� ���������������
            RC = FImpUnit->Import(ARecId);
           }
         }
       }
      catch (Exception & E)
       {
        dsLogError(E.Message, __FUNC__);
       }
     }
    __finally
     {
      if (FImpUnit)
       FImpUnit->OnSetExtEditValue = NULL;
      if (PatExist)
       FDM->DeleteClassifier("1000");
      else
       FDM->DeleteClassifier(AId);
      AResMessage = FTimeCheck->GetStr(" >->-> ������");
      dsLogMessage(AResMessage, __FUNC__);
      delete FTimeCheck;
     }
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TDataImporter::FImportCard(UnicodeString AId, UnicodeString APtId, TJSONObject * AValues,
  UnicodeString APtCode, int & ACount)
 {
  ACount = 0;
  dsLogBegin(UnicodeString(__FUNC__) + ":" + AId);
  UnicodeString RC = "";
  UnicodeString FCD = "";
  TJSONObject * tmpCard = (TJSONObject *)AValues->Values[AId];
  TJSONObject * FCard = NULL;
  if (tmpCard)
   FCard = (TJSONObject *)JSONCopy(tmpCard);
  try
   {
    if (FCard)
     {
      TJSONObject * FValue;
      TJSONObject * FRec;
      TTagNode * FlDef;
      TJSONPairEnumerator * itRec = FCard->GetEnumerator();
      TJSONPairEnumerator * itFl;
      TJSONPairEnumerator * itVal;
      while (itRec->MoveNext())
       { // ���� �� �������
        FRec = (TJSONObject *)itRec->Current->JsonValue;
        TJSONObject * ImportRec = new TJSONObject;
        try
         {
          if (FRec->ClassType()->ClassNameIs("TJSONObject"))
           {
            TJSONPairEnumerator * itFl = FRec->GetEnumerator();
            while (itFl->MoveNext())
             { // ���� �� �����
              FlDef = FDM->xmlImpDataDef->GetTagByUID(JSONEnumPairCode(itFl));
              if (FlDef)
               {
                FValue = (TJSONObject *)JSONPairValue(itFl->Current);
                // dsLogMessage(FlDef->AV["uid"]+"="+FValue->ToString(),__FUNC__, 6);
                if (!FValue->Null)
                 {
                  dsLogMessage("FlDef->Name; " + FlDef->Name + " ; classname=" + FValue->ClassType()->ClassName(),
                    __FUNC__);
                  if (FlDef->CmpName("binary,choice,choiceDB,choiceTree,extedit") && FValue->ClassType()->ClassNameIs
                    ("TJSONObject"))
                   {
                    // itFl->Current->JsonValue->Owned = false;
                    // delete itFl->Current->JsonValue;
                    // itFl->Current->JsonValue = FValue->Pairs[0]->JsonString;
                    dsLogMessage("pair;  \"" + JSONPairName(itFl->Current) + "\" : " + JSONPairName(FValue->Pairs[0]),
                      __FUNC__);
                    ImportRec->AddPair(JSONPairName(itFl->Current), JSONPairName(FValue->Pairs[0]));
                   }
                  else
                   {
                    dsLogMessage("not pair;  \"" + JSONPairName(itFl->Current) + "\" : " + JSONPairValue(itFl->Current)
                      ->ToString(), __FUNC__);
                    ImportRec->AddPair(JSONPairName(itFl->Current), JSONCopy(JSONPairValue(itFl->Current)));
                   }
                 }
                else
                 {
                  dsLogMessage("FValue->Null;  \"" + JSONPairName(itFl->Current) + "\" : TJSONNull", __FUNC__);
                  ImportRec->AddPair(JSONPairName(itFl->Current), new TJSONNull);
                 }
               }
              else
               {
                ImportRec->AddPair(JSONPairName(itFl->Current), JSONCopy(JSONPairValue(itFl->Current)));
                dsLogMessage("no FlDef;  \"" + JSONPairName(itFl->Current) + "\" : " + JSONPairValue(itFl->Current)
                  ->ToString(), __FUNC__);
               }
             }
           }
          IntToJSON(ImportRec, APtId, APtCode.ToIntDef(-1));
          dsLogMessage("before " + AId + " : " + RC + " Data:: " + ImportRec->ToString(), __FUNC__);
          RC += FDM->ImportCardData(AId, ImportRec, FCD) + "; ";
          // RC += "ok; ";
          ACount++ ;
          dsLogMessage(AId + " : " + RC, __FUNC__);
         }
        __finally
         {
          delete ImportRec;
         }
       }
     }
   }
  __finally
   {
    delete FCard;
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TdsDIOnExtGetCodeEvent __fastcall TDataImporter::FGetOnExtGetCode()
 {
  dsLogBegin(__FUNC__);
  TdsDIOnExtGetCodeEvent RC = NULL;
  try
   {
    if (FDM)
     RC = FDM->OnExtGetCode;
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TDataImporter::FSetOnExtGetCode(TdsDIOnExtGetCodeEvent AVal)
 {
  dsLogBegin(__FUNC__);
  if (FDM)
   FDM->OnExtGetCode = AVal;
  dsLogEnd(__FUNC__);
 }
// ----------------------------------------------------------------------------
TDIImportCardDataEvent __fastcall TDataImporter::FGetOnImportCard()
 {
  dsLogBegin(__FUNC__);
  TDIImportCardDataEvent RC = NULL;
  try
   {
    if (FDM)
     RC = FDM->OnImportCard;
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TDataImporter::FSetOnImportCard(TDIImportCardDataEvent AVal)
 {
  dsLogBegin(__FUNC__);
  if (FDM)
   FDM->OnImportCard = AVal;
  dsLogEnd(__FUNC__);
 }
// ----------------------------------------------------------------------------
bool __fastcall TDataImporter::FSetExtEditValue(TDataTable * ATable, TTagNode * ADefNode, UnicodeString AVal,
  UnicodeString AExtVal, TJSONObject * ACls)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  try
   {
    dsLogMessage(ADefNode->AV["name"], __FUNC__, 1);
    if (ADefNode->CmpAV("fl", "cguid"))
     {
      dsLogMessage(AVal, __FUNC__, 1);
      if (AVal.Trim().Length())
       ATable->Fields[ADefNode->AV["uid"]]->AsString = AVal.UpperCase();
      else
       ATable->Fields[ADefNode->AV["uid"]]->AsString = NewGUID();
     }
    else
     {
      UnicodeString FCode = AVal;
      UnicodeString FId = ADefNode->AV["uid"].UpperCase();
      UnicodeString FRegGUI = ADefNode->GetRoot()->GetChildByName("passport")->AV["GUI"];
      if (FRegGUI.UpperCase() == "40381E23-92155860-4448")
       {
        if (FId == "0136")
         { // ����� : �������
#ifdef REG_USE_KLADR
          if ((FCode.ToIntDef(0) == -1) && FKLADR)
           {
            FCode = AExtVal.Trim();
            if (FCode.Length())
             {
              TJSONObject * AddrRC = FKLADR->Find(FCode, "", 0x1F);
              UnicodeString FClCode, FClStr;
              TJSONPairEnumerator * itPair;
              FCode  = "";
              itPair = AddrRC->GetEnumerator();
              while (itPair->MoveNext() && !FCode.Length())
               {
                FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
                if (!SameText(FClCode, "msg"))
                 FCode = FClCode;
               }
             }
           }
          if (!FCode.Length())
           FCode = AVal;
          adresCode FAddrCode = ParseAddrStr(FCode);
          // ATable->Fields["0022"]->AsInteger = FAddrCode.State;
          ATable->Fields["0136"]->AsString = FAddrCode.AsShortString();
          if (FAddrCode.State)
           {
            ATable->Fields["0032"]->AsInteger = FAddrCode.Town1ID; // <extedit name='��������� �����' uid='0032'
            ATable->Fields["00FF"]->AsString  = FAddrCode.StreetID; // <extedit name='�����' uid='00FF'
            ATable->Fields["0100"]->AsString  = FAddrCode.HouseFull; // <extedit name='���' uid='0100'
            ATable->Fields["0033"]->AsString  = FAddrCode.Flat; // <extedit name='��������' uid='0033'
           }
#endif
         }
        else if (FId == "015A")
         { // ����� : ����� �������
#ifdef REG_USE_KLADR
          adresCode FAddrCode = ParseAddrStr(AVal);
          // ATable->Fields["0022"]->AsInteger = FAddrCode.State;
          ATable->Fields["015A"]->AsString = FAddrCode.AsShortString();
          // if (FAddrCode.State)
          // {
          // ATable->Fields["0032"]->AsInteger = FAddrCode.Town1ID; //<extedit name='��������� �����' uid='0032' depend='0136.0136'  length='11'/>
          // ATable->Fields["00FF"]->AsString  = FAddrCode.StreetID; //<extedit name='�����' uid='00FF' depend='0136.0136'  length='4'/>
          // ATable->Fields["0100"]->AsString  = FAddrCode.HouseFull; //<extedit name='���' uid='0100' depend='0136.0136'  length='50'/>
          // ATable->Fields["0033"]->AsString  = FAddrCode.Flat ; //<extedit name='��������' uid='0033' depend='0136.0136'  length='15'/>
          // }
#endif
         }
        else if ((FId == "019D") || (FId == "00CC") || (FId == "00E4"))
         { // ����� : ������������� ����,���,������������� ���,
#ifdef REG_USE_KLADR
          adresCode FAddrCode = ParseAddrStr(AVal);
          ATable->Fields[FId]->AsString = FAddrCode.AsShortString() + FAddrCode.HouseCodeStr();
#endif
         }
        else if (FId == "0102")
         { // ������� �� ������� (�������� �����, )
#ifdef REG_USE_KLADR
          adresCode FAddrCode = ParseAddrStr(AVal);
          ATable->Fields[FId]->AsString = FAddrCode.AsShortString() + FAddrCode.HouseCodeStr();
#endif
         }
        else if (FId == "0025")
         { // �����������
#ifdef REG_USE_ORG
          orgCode FOrgCode;
          if (FCode.ToIntDef(0))
           {
            FOrgCode.OrgID = FCode.ToIntDef(0);
            if (ATable->Fields["0026"]->AsString.Trim().Length())
             FOrgCode.L1Val = ATable->Fields["0026"]->AsString;
            if (ATable->Fields["0027"]->AsString.Trim().Length())
             FOrgCode.L2Val = ATable->Fields["0027"]->AsString;
            if (ATable->Fields["0028"]->AsString.Trim().Length())
             FOrgCode.L3Val = ATable->Fields["0028"]->AsString;
           }
          else
           FOrgCode = ParseOrgCodeStr(FCode);
          if ((FOrgCode.Type != TOrgTypeValue::NF) && FOrgCode.OrgID)
           {
            if (ACls)
             {
              FOrgCode.OrgID = FGetClassValue(ADefNode, ACls, FOrgCode.OrgID);
             }
            if (FOrgCode.OrgID)
             {
              if (FOrgCode.L1Val.Trim().Length())
               ATable->Fields["0026"]->AsString = FOrgCode.L1Val;
              else
               ATable->Fields["0026"]->Clear();
              if (FOrgCode.L2Val.Trim().Length())
               ATable->Fields["0027"]->AsString = FOrgCode.L2Val;
              else
               ATable->Fields["0027"]->Clear();
              if (FOrgCode.L3Val.Trim().Length())
               ATable->Fields["0028"]->AsString = FOrgCode.L3Val;
              else
               ATable->Fields["0028"]->Clear();
              ATable->Fields["0025"]->AsInteger = FOrgCode.OrgID;
              // <extedit name='�����������' uid='0025' comment='search' ref='0001' required='1' inlist='e' depend='011B.0001' linecount='3' showtype='1' >
              if (FOrgCode.Format.Length())
               ATable->Fields["0112"]->AsInteger = UIDInt(FOrgCode.Format);
              // <extedit name='������ ��������' uid='0112' ref='0108' depend='0025.0025'  />
             }
           }
#endif
         }
        else
         ATable->Fields[FId]->AsString = FCode;
       }
      else
       ATable->Fields[FId]->AsString = FCode;
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
Variant __fastcall TDataImporter::FGetClassValue(TTagNode * ADefNode, TJSONObject * ACls, const Variant & AValue)
 {
  dsLogBegin(__FUNC__);
  dsLogMessage("���� [" + ADefNode->AV["uid"] + "] [" + ADefNode->AV["ref"] + "] val=" + VarToStrDef(AValue, "-1"),
    __FUNC__);
  Variant RC = AValue;
  try
   {
    if (FImportBegTime < IncSecond(Time(), -6))
     {
      FClsData.clear();
     }
    FImportBegTime = Time();
    UnicodeString FindIdx = ADefNode->AV["ref"].UpperCase() + "_" + VarToStrDef(AValue, "0");
    dsLogMessage("����� " + FindIdx, __FUNC__);
    TAnsiStrMap::iterator FRC = FClsData.find(FindIdx);
    if (FRC == FClsData.end())
     {
      dsLogMessage("�� ����� " + FindIdx, __FUNC__);
      TJSONObject * Cl = (TJSONObject *)GetJSONValue(ACls, ADefNode->AV["ref"]);
      if (Cl)
       { // ���������� ADefNode->AV["ref"] ������
        dsLogMessage("���������� " + ADefNode->AV["ref"] + "; find: " + VarToStrDef(AValue, "0") + " values: " +
          Cl->ToString(), __FUNC__);
        TJSONObject * ClRec = (TJSONObject *)GetJSONValue(Cl, VarToStrDef(AValue, "0"));
        if (ClRec)
         { // ������ ��� ���� AValue �������
          dsLogMessage("������ � ����������� " + ADefNode->AV["ref"] + " values: " + ClRec->ToString(), __FUNC__);
          TJSONObject * ImpRec = new TJSONObject;
          try
           {
            // ImpRec->Owned = false;
            ImpRec->AddPair("rec", TJSONObject::ParseJSONValue(ClRec->ToString()));
            // ImpRec->AddPair("rec", ClRec);
            ImpRec->AddPair("cl", TJSONObject::ParseJSONValue(ACls->ToString()));
            // ImpRec->AddPair("cl", ACls);
            UnicodeString FRecID = "";
            UnicodeString FResMessage = "";
            UnicodeString PRC = ImportData(ADefNode->AV["ref"], ImpRec, FRecID, FResMessage);
            if (SameText(PRC, "ok"))
             {
              dsLogMessage("���� [" + ADefNode->AV["uid"] + "] [" + ADefNode->AV["ref"] + "] val=" + VarToStrDef(AValue,
                "-1") + "; setval=" + FRecID, __FUNC__);
              RC                = FRecID;
              FClsData[FindIdx] = FRecID;
             }
            else
             dsLogError(PRC, __FUNC__);
           }
          __finally
           {
            delete ImpRec;
           }
         }
       }
     }
    else
     {
      dsLogMessage("����� " + FindIdx, __FUNC__);
      RC = FRC->second;
      dsLogMessage("���� [" + ADefNode->AV["uid"] + "] [" + ADefNode->AV["ref"] + "] RCval=" + RC, __FUNC__);
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
