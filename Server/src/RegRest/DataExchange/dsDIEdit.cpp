//---------------------------------------------------------------------------
#include <vcl.h>
//#include <stdio.h>
#include <System.Math.hpp>
//#include <Math.h>
//#include <clipbrd.hpp>
#pragma hdrstop

#include "dsDIEdit.h"
#include "DataField.h"
#include "icsLog.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
//#include "TreeClass.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#define RefCtrl(aN)  ((TdsDIEdit*)aN->GetTagByUID(aN->AV["ref"].UpperCase())->Ctrl)
#define RefMCtrl(aN) ((TdsDIEdit*)aN->GetTagByUID(_GUI(aN->AV["depend"].UpperCase()))->Ctrl)
#define depCtrl(a)  ((TdsDIEdit*)((TTagNode*)SetValList->Items[a])->Ctrl)

//---------------------------------------------------------------------------
__fastcall TdsDIEdit::TdsDIEdit(TObject * AOwner, TTagNode * ANode, TFDConnection * AConnection)
 {
  FConnection = AConnection;
  SetEnList = new TList;
  SetValList = new TList;
  FOwner = NULL;
  FNode  = ANode;
  FNode->Ctrl = (TWinControl *)this;
  isEnabled = !(FNode->CmpAV("isedit", "no") && FNode->AV["default"].Length());
  FFlName   = "R" + FNode->AV["uid"].UpperCase();
  FOwner   = AOwner;
  ((TDataField *)FOwner)->IntEn = ((TDataField *)FOwner)->IntEn & isEnabled;
  if (FNode->CmpName("choice"))
   {
    //dsLogMessage("7_1", __FUNC__, 1);
   }
  else if (FNode->CmpName("choiceDB"))
   {
    FDepTab    = "";
    FDepFlVal  = "";
    FRetFlName = "";
    FRetVals   = "";
    FDepValues = "";
    //dsLogMessage("7_2", __FUNC__, 1);
   }
  else if (FNode->CmpName("choiceTree"))
   {
    //dsLogMessage("7_3", __FUNC__, 1);
   }
  else if (FNode->CmpName("date"))
   {
    if (FNode->CmpAV("default", "currentdate"))
     ((TDataField *)FOwner)->ImpEDSetValue(Variant(TDateTime::CurrentDate()));
    //dsLogMessage("7_4", __FUNC__, 1);
   }
  else if (FNode->CmpName("extedit,text,binary,digit"))
   {
    //dsLogMessage("7_5", __FUNC__, 1);
   }
  else
   {
    //dsLogMessage("7_6", __FUNC__, 1);
    dsLogError(" �, ���� ������ ���� \"" + FNode->Name + "\" !!!", __FUNC__);
    return;
   }
  //dsLogMessage("8", __FUNC__, 1);
  if (FNode->CmpName("choiceDB"))
   {//��������� ����������� �������� choiceDB �� ������ ���������
    if (FNode->AV["depend"].Length() == 9 && isRefMCtrl(FNode))
     RefMCtrl(FNode)->SetOnValDepend(FNode);
    //dsLogMessage("9", __FUNC__, 1);
   }
  if (FNode->GetChildByName("actuals"))
   {//��������� ����������� "Enabled"
    //dsLogMessage("10", __FUNC__, 1);
    TTagNode * itxNode = FNode->GetChildByName("actuals")->GetFirstChild();
    while (itxNode)
     {
      if (itxNode->AV["depend"].Length() == 9 && isRefMCtrl(itxNode))
       RefMCtrl(itxNode)->SetOnEnable(FNode);
      else if (itxNode->AV["ref"].Length() == 4 && isRefCtrl(itxNode))
       RefCtrl(itxNode)->SetOnEnable(FNode);
      itxNode = itxNode->GetNext();
     }
    //dsLogMessage("11", __FUNC__, 1);
   }
//!!!++  dsLogEnd(__FUNC__, 6);
 }
//---------------------------------------------------------------------------
__fastcall TdsDIEdit::~TdsDIEdit()
 {
//!!!++  dsLogBegin(__FUNC__, 6);
  //FNode->Ctrl = NULL;
//  if (SetValList)
   delete SetValList;
//  if (SetEnList)
   delete SetEnList;
  //if (CBList)
  //delete CBList;
//!!!++  dsLogEnd(__FUNC__, 6);
 }
//---------------------------------------------------------------------------
TFDQuery * __fastcall TdsDIEdit::CreateTempQuery()
 {
  TFDQuery * FQ = new TFDQuery(NULL);
  FQ->Transaction = new TFDTransaction(NULL);
  try
   {
    FQ->Transaction->Connection = FConnection;
    FQ->Connection              = FConnection;

    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();

   }
  __finally
   {
   }
  return FQ;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDIEdit::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDIEdit::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDIEdit::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
    UnicodeString AParamName, UnicodeString AId,
    UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  UnicodeString FParams = "; Params: ";
  if (AParamName.Length())
   FParams += AParamName + "=" + AId;
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParamName)->Value = AId;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL(((ALogComment.Length()) ? ALogComment : ASQL) + FParams, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDIEdit::CheckChDbValue(UnicodeString ACode)
 {
  // dsLogBegin(__FUNC__, 6);
  bool RC = false;
  if (ACode.Length())
   {
    TFDQuery * FQ = CreateTempQuery();
    try
     {
      UnicodeString FSQL = "Select Count(*) as RCOUNT From CLASS_" + FNode->AV["ref"].UpperCase() + " Where code=" + ACode;
      if (FDepValues.Length())
       {
        FRetVals = "";
        GetDependClassCode(FDepTab, FDepFlVal, FRetFlName, FRetVals);
        FDepValues = FRetVals.Trim();
        if (FDepValues.Length())
         FSQL += " and CODE in (" + FDepValues + ")";
       }
      quExec(FQ, false, FSQL);
      RC = FQ->FieldByName("RCOUNT")->AsInteger;
     }
    __finally
     {
      DeleteTempQuery(FQ);
     }
   }
  // dsLogEnd(__FUNC__, 6);
  return RC;
 }
//---------------------------------------------------------------------------
Variant __fastcall TdsDIEdit::GetChDBFieldValue(UnicodeString AFieldName, UnicodeString ACode)
 {
  // dsLogBegin(__FUNC__, 6);
  Variant RC = Variant::Empty();
  if (ACode.Length() && AFieldName.Length())
   {
    TFDQuery * FQ = CreateTempQuery();
    try
     {
      UnicodeString FSQL = "Select Distinct " + AFieldName + " as RCFIELD From CLASS_" + FNode->AV["ref"].UpperCase() + " Where code=" + ACode;
      if (FDepValues.Length())
       {
        FRetVals = "";
        GetDependClassCode(FDepTab, FDepFlVal, FRetFlName, FRetVals);
        FDepValues = FRetVals.Trim();
        if (FDepValues.Length())
         FSQL += " and CODE in (" + FDepValues + ")";
       }

      quExec(FQ, false, FSQL);
      RC = FQ->FieldByName("RCFIELD")->AsVariant;
     }
    __finally
     {
      DeleteTempQuery(FQ);
     }
   }
  // dsLogEnd(__FUNC__, 6);
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDIEdit::GetDependClassCode(UnicodeString ADepClName, UnicodeString ADepFlVal, UnicodeString ARetFlName, UnicodeString & ARetVals)
 {
  // dsLogBegin(__FUNC__, 6);
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    quExec(FQ, false, "Select " + ARetFlName + " From " + ADepClName + " Where " + ADepFlVal);
    while (!FQ->Eof)
     {
      ARetVals += FQ->FieldByName(ARetFlName)->AsString + ",";
      FQ->Next();
     }
    if (ARetVals == "")
     ARetVals = "-1";
    else
     ARetVals[ARetVals.Length()] = ' ';

   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  // dsLogEnd(__FUNC__, 6);
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDIEdit::FieldCast(UnicodeString AVal)
 {
  // dsLogBegin(__FUNC__, 6);
  UnicodeString xCast = FNode->AV["cast"].LowerCase();
  if (xCast == "lo")
   {
    return AVal.LowerCase();
   }
  else if (xCast == "up")
   {
    return AVal.UpperCase();
   }
  else if (xCast == "up1st")
   {
    return (AVal.SubString(1, 1).UpperCase() + AVal.SubString(2, AVal.Length() - 1).LowerCase());
   }
  else
   return AVal;
  // dsLogEnd(__FUNC__, 6);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDIEdit::SetDependVal(UnicodeString ADepClCode, __int64 ACode)
 {
  // dsLogBegin(__FUNC__, 6);
  FDepTab = "CLASS_" + _UID(FNode->AV["depend"]);
  TTagNode * _tmp = FNode->GetTagByUID(_UID(FNode->AV["depend"]));
  UnicodeString FDepUID = _UID(FNode->AV["depend"]);
  if (FDepUID == ADepClCode)
   {
    FDepFlVal = "CODE=" + IntToStr(ACode);
   }
  else if (FDepUID == FNode->AV["ref"])
   {
    FDepFlVal  = "R" + _tmp->GetChildByAV("", "ref", ADepClCode)->AV["uid"] + "=" + IntToStr(ACode);
    FRetFlName = "CODE";
   }
  else
   {
    FDepFlVal  = "R" + _tmp->GetChildByAV("", "ref", ADepClCode)->AV["uid"] + "=" + IntToStr(ACode);
    FRetFlName = "R" + _tmp->GetChildByAV("", "ref", FNode->AV["ref"])->AV["uid"];
   }
  FRetVals = "";
  GetDependClassCode(FDepTab, FDepFlVal, FRetFlName, FRetVals);
  FDepValues = FRetVals;
  ChDBValue  = "";
  DataChange();
  // dsLogEnd(__FUNC__, 6);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDIEdit::SetEnable(bool AEnable)
 {
  // dsLogBegin(__FUNC__, 6);
  if (AEnable & isEnabled)
   {
    ((TDataField *)FOwner)->IntEn = true;
   }
  else
   {
    ((TDataField *)FOwner)->IntEn = false;
    if (FNode->CmpName("text,digit"))
     {
      ((TDataField *)FOwner)->ImpEDSetValue(Variant(""));
     }
    else if (FNode->CmpName("date"))
     {
      ((TDataField *)FOwner)->ImpEDSetValue(Variant(TDateTime(0)));
     }
    else if (FNode->CmpName("choice"))
     {
      ((TDataField *)FOwner)->ImpEDSetValue(Variant(-1));
     }
    else if (FNode->CmpName("choiceDB"))
     {
      ((TDataField *)FOwner)->ImpEDSetValue(Variant(-1));
      ChDBValue = "";
     }
   }
  DataChange();
  // dsLogEnd(__FUNC__, 6);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDIEdit::SetValue(Variant AVal, bool AEnable)
 {
//  // dsLogBegin(__FUNC__, 6);
  dsLogMessage(FNode->AV["uid"]+"::"+FNode->AV["name"],__FUNC__, 6);
  Variant tmpValue = AVal;
  if (!(AVal.IsNull() || AVal.IsEmpty()))
   {
    int vInt;
    double vDouble;
    UnicodeString vAnsi;
    if (FNode->CmpName("text"))
     {
      vAnsi = VarToStr(tmpValue).Trim();
      if (vAnsi.Length() > FNode->AV["length"].ToInt())
       vAnsi = vAnsi.SubString(1, FNode->AV["length"].ToInt());
      tmpValue = Variant(FieldCast(vAnsi));
     }
    else if (FNode->CmpName("date"))
     {
      try
       {
        tmpValue = /*TDateTime(*/tmpValue/*)*/;
       }
      catch (EVariantTypeCastError & E)
       {
        throw EDataTypeError(E.Message);
       }
     }
    else if (FNode->CmpName("digit"))
     {
      if (!FNode->AV["decimal"].ToIntDef(0))
       {//����� �����
        try
         {
          vInt = int(tmpValue);
         }
        catch (EVariantTypeCastError & E)
         {
          throw EDataTypeError(E.Message);
         }
        if (IntToStr(vInt).Length() > FNode->AV["digits"].ToIntDef(0))
         throw EDataValueError(FNode->AV["name"] + "[" + FNode->AV["uid"] + "]; val={\"" + VarToStrDef(AVal, "������") + "\"}; �������� ������ �������������");
        if (!InRange(vInt, FNode->AV["min"].ToIntDef(0), FNode->AV["max"].ToIntDef(0)))
         throw EDataValueError((FNode->AV["name"] + "[" + FNode->AV["uid"] + "]; val={\"" + VarToStrDef(AVal, "������") + "\"}; �������� \"" + VarToStrDef(tmpValue,
            "null") + "\" ������� �� ������� ����������� ��������� [" + FNode->AV["min"] + " - " + FNode->AV["max"] + "]"));
       }
      else
       {
        try
         {
          vDouble = double(tmpValue);
         }
        catch (EVariantTypeCastError & E)
         {
          throw EDataTypeError(E.Message);
         }
        const int vP1Size = FNode->AV["digits"].ToIntDef(0) + 1;
        const int vP2Size = FNode->AV["decimal"].ToIntDef(0) + 1;
        wchar_t * vP1 = new wchar_t[vP1Size];
        wchar_t * vP2 = new wchar_t[vP2Size];
        UnicodeString sDouble;
        try
         {
          ZeroMemory(vP1, vP1Size);
          ZeroMemory(vP2, vP2Size);
          swscanf(VarToStr(tmpValue).c_str(), ("%[0-9],%" + FNode->AV["decimal"] + "[0-9]").c_str(), vP1, vP2);
          sDouble = UnicodeString(vP1).Trim() + "," + UnicodeString(vP2).Trim();
         }
        __finally
         {
          delete[]vP1;
          delete[]vP2;
         }
        tmpValue = Variant(StrToFloat(sDouble));
        //if (sDouble.Length() > FNode->AV["digits"].ToIntDef(0))
        //throw EDataValueError("�������� ������ �������������");
        if (!InRange(double(tmpValue), double(StrToFloat(FNode->AV["min"])), double(StrToFloat(FNode->AV["max"]))))
         throw EDataValueError((FNode->AV["name"] + "[" + FNode->AV["uid"] + "]; val={\"" + VarToStrDef(AVal, "������") + "\"}; �������� \"" + VarToStrDef(tmpValue,
            "null") + "\" ������� �� ������� ����������� ��������� [" + FNode->AV["min"] + " - " + FNode->AV["max"] + "]"));
       }
     }
    else if (FNode->CmpName("binary"))
     {
      try
       {
        vInt = int(tmpValue);
       }
      catch (EVariantTypeCastError & E)
       {
        throw EDataTypeError(E.Message);
       }
     }
    else if (FNode->CmpName("choice"))
     {
      try
       {
        vInt = int(tmpValue);
       }
      catch (EVariantTypeCastError & E)
       {
        throw EDataTypeError(E.Message);
       }

      if (!FNode->GetChildByAV("choicevalue", "value", IntToStr(vInt), true))
       {
        UnicodeString FChVals = "";
        TTagNode * itNode = FNode->GetFirstChild();
        while (itNode)
         {
          if (itNode->CmpName("choicevalue"))
           FChVals += "[" + itNode->AV["value"] + "] " + itNode->AV["name"] + "; ";
          itNode = itNode->GetNext();
         }
        throw EDataValueError(FNode->AV["name"] + "[" + FNode->AV["uid"] + "]; " + FChVals + "; val={\"" + VarToStrDef(AVal, "������") + "\"}; �������� ����������� � ������ ��������� ��������");
       }
     }
    else if (FNode->CmpName("choiceDB"))
     {
      UnicodeString ClCode = VarToStrDef(tmpValue, "0"); //!!!IntToStr(vInt);
      if (!ClCode.ToIntDef(0))
       {
        ChDBValue = "";
        tmpValue  = Variant::Empty();
       }
      else
       {
        if (!CheckChDbValue(ClCode.Trim()))
         throw EDataValueError(FNode->AV["name"] + "[" + FNode->AV["uid"] + "]; val={\"" + VarToStrDef(AVal, "������") + "\"}; �������� ����������� � ������ ��������� ��������");
        else
         {
          ChDBValue = ClCode;
          if (SetValList->Count > 0)
           {//���� ����������� �� ������ � ��� ��� choiceDB ����������
            UnicodeString FDepClCode = FNode->AV["ref"];
            for (int i = 0; i < SetValList->Count; i++)
             depCtrl(i)->SetDependVal(FDepClCode, ChDBValue.ToIntDef(0));
           }
         }
       }
     }
   }
  ((TDataField *)FOwner)->ImpEDSetValue(tmpValue);
  DataChange();
//  // dsLogEnd(__FUNC__, 6);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDIEdit::SetOnValDepend(TTagNode * ANode)
 {
  SetValList->Add(ANode);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDIEdit::SetOnEnable(TTagNode * ANode)
 {
  // dsLogBegin(__FUNC__, 6);
  SetEnList->Add(ANode);
  DataChange();
  // dsLogEnd(__FUNC__, 6);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDIEdit::DataChange()
 {
  // dsLogBegin(__FUNC__, 6);
  UnicodeString xRef, xUID, FUID;
  FUID = FNode->AV["uid"].UpperCase();
  //��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      ((TdsDIEdit *)xNode->Ctrl)->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  // dsLogEnd(__FUNC__, 6);
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDIEdit::GetValue(UnicodeString ARef)
 {
  return "";
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDIEdit::Condition(TTagNode * CondNode)
 {
//!!!++  // dsLogBegin(__FUNC__, 6);
  bool RC = false;
  TStringList * ExpList = new TStringList;
  TStringList * ParamList = new TStringList;
  try
   {
    UnicodeString xCond = CondNode->AV["condition"];
    CalcParam(CondNode->GetFirstChild(), ParamList/*pmList*/);
    //dsLogMessage("1", __FUNC__, 1);
    ExpList->Text = StringReplace(xCond, " ", "\n", TReplaceFlags() << rfReplaceAll);
    UnicodeString tmpOper;
    //dsLogMessage("2", __FUNC__, 1);
    for (int i = 0; i < ExpList->Count; i++)
     {
      tmpOper = ExpList->Strings[i];
      if (tmpOper[1] == '@')
       ExpList->Strings[i] = ParamList->Strings[tmpOper.SubString(2, tmpOper.Length() - 1).ToInt()]; //VarToStr(pmList[tmpOper.SubString(2, tmpOper.Length() - 1).ToInt()]);
     }
    //dsLogMessage("3", __FUNC__, 1);
    RC = (bool)IntCalcCondition(ExpList);
    //dsLogMessage("4", __FUNC__, 1);
   }
  __finally
   {
    delete ExpList;
    delete ParamList;
   }
//!!!++  // dsLogEnd(__FUNC__, 6);
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDIEdit::CalcParam(TTagNode * AFirst, TStringList * AParam)
 {
//!!!++  // dsLogBegin(__FUNC__, 6);
  TTagNode * xNode = AFirst;
  Variant RC = NULL;
  UnicodeString xxRef;
  while (xNode)
   {
    if (xNode->AV["depend"].Length() == 9 && isRefMCtrl(xNode))
     {
      xxRef = xNode->AV["depend"].SubString(6, 4) + "." + xNode->AV["ref"];
      RC    = RefMCtrl(xNode)->GetCompValue(xxRef);
     }
    else if (xNode->AV["ref"].Length() == 4 && isRefCtrl(xNode))
     RC = RefCtrl(xNode)->GetCompValue("");
    else if (xNode->CmpAV("name", "null"))
     RC = "NULL";
    else
     dsLogError("����������� ������ �� ������", __FUNC__);
    AParam->Add(VarToStr(RC));
    xNode = xNode->GetNext();
   }
//!!!++  // dsLogEnd(__FUNC__, 6);
 }
//---------------------------------------------------------------------------
Variant __fastcall TdsDIEdit::GetCompValue(UnicodeString ARef)//!!!
 {
//!!!++  // dsLogBegin(__FUNC__, 6);
  Variant RC = Variant::Empty();
  try
   {
    if (!ARef.Length() && FNode->CmpName("date,digit,choice,binary,text,extedit"))
     {
      if (FNode->CmpName("date"))
       RC = ((TDataField *)FOwner)->ImpEDGetValue(); /*Variant(TDateTime((double)((TDataField*)FOwner)->ImpEDGetValue()).FormatString("'dd.mm.yyyy'"));*/
      else if (FNode->CmpName("digit,choice,binary,text,extedit"))
       RC = ((TDataField *)FOwner)->ImpEDGetValue();
      else
       RC = 0;
     }
    else if (ARef.Length() && FNode->CmpName("choiceDB"))
     {
      /*
       //���� allFld = true  - ��������� �������� ���� �����
       //.   i-� ������ ClassCB - ��������� ��������
       //.   i-� ������ ClassCB - TStringList - ���:
       //.      0-� ������ = ""
       //.      0-� ������ = �������� ���� "CODE"
       //.      ��� i �� 1 �� Count
       //.        i-� ������ =  ���_����=���_#_��������
       //�����
       //i-� ������ ClassCB - ��������� ��������
       //i-� ������ ClassCB = �������� ���� "CODE"
       */
      TStringList * CBValues;
      if (FNode->CmpAV("ref", _GUI(ARef)))//ARef.SubString(1, 4) == cName(i).SubString(4, 4)
       {
        if (ChDBValue.Length())
         RC = GetChDBFieldValue("R" + ARef.SubString(6, 4), ChDBValue);
       }
     }
    else
     {
      dsLogError("��������� ������ ������ -> '" + ARef + "'", __FUNC__);
     }
   }
  __finally
   {
   }
//!!!++  // dsLogEnd(__FUNC__, 6);
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDIEdit::CheckReqValue()
 {
//  // dsLogBegin(__FUNC__, 6);
  bool RC = true;
  if (FNode->CmpAV("required", "yes"))
   {
    try
     {
      if (((TDataField *)FOwner)->IntEn)
       {
        if (!((TDataField *)FOwner)->IsNull())
         {
          Variant FVal = ((TDataField *)FOwner)->ImpEDGetValue();
          if (FNode->CmpName("text,digit,choiceTree,extedit"))
           RC = (VarToStr(FVal).Length());
          else if (FNode->CmpName("date"))
           RC = (int(FVal) != 0);
         }
        else
         RC = false;
       }
     }
    __finally
     {
     }
   }
//  // dsLogEnd(__FUNC__, 6);
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDIEdit::isRefCtrl(TTagNode * ANode)
 {
  if (ANode->GetTagByUID(ANode->AV["ref"]))
   if ((TdsDIEdit *)(ANode->GetTagByUID(ANode->AV["ref"])->Ctrl))
    return true;
  return false;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDIEdit::isRefMCtrl(TTagNode * ANode)
 {
  if (ANode->GetTagByUID(_GUI(ANode->AV["depend"])))
   if ((TdsDIEdit *)(ANode->GetTagByUID(_GUI(ANode->AV["depend"]))->Ctrl))
    return true;
  return false;
 }
//---------------------------------------------------------------------------
//#############################################################################
//#                        �������������� �������                             #
//#############################################################################

//---------------------------------------------------------------------------
int __fastcall TdsDIEdit::IntCalcCondition(TStringList * Exp)
 {
  //���������� �������� ">,<,<>,<=,>=,=,&,|,~"
  if (Exp->Count == 1)
   {
    return Exp->Strings[0].ToIntDef(-1);
   }
  //Variant P1, P2;
  bool IsStr1, IsStr2, Res;
  //enum class TCondOperation {coNone, coNot, coOr, coAnd, coEqu, coNotEqu, coLt, coLtEqu, coGt, coGtEqu };
  TCondOperation FCondOper;
  for (int i = 0; i < Exp->Count; i++)
   {
    FCondOper = TCondOperation::coNone;
    if (!Exp->Strings[i].Trim().Length())
     Exp->Strings[i] = "-1";
    else
     {
      switch (Exp->Strings[i][1])
       {
       case '~':
        FCondOper = TCondOperation::coNot;
        break;
       case '|':
        FCondOper = TCondOperation::coOr;
        break;
       case '&':
        FCondOper = TCondOperation::coAnd;
        break;
       case '=':
        FCondOper = TCondOperation::coEqu;
        break;
       case '<':
         {
          if (Exp->Strings[i].Length() == 1)//"<"
               FCondOper = TCondOperation::coLt;
          else if (Exp->Strings[i][2] == '>')//"<>"
               FCondOper = TCondOperation::coNotEqu;
          else if (Exp->Strings[i][2] == '=')//"<="
               FCondOper = TCondOperation::coLtEqu;
         }
       case '>':
         {
          if (Exp->Strings[i].Length() == 1)//">"
               FCondOper = TCondOperation::coGt;
          else if (Exp->Strings[i][2] == '=')//">="
               FCondOper = TCondOperation::coGtEqu;
         }
       }
     }

    if (FCondOper != TCondOperation::coNone)
     {
      if (FCondOper == TCondOperation::coNot)
       {
        if (!IsNull(Exp, i - 1))
         Exp->Strings[i] = IntToStr((int) !Exp->Strings[i - 1].ToIntDef(0));
        else
         Exp->Strings[i] = "NULL";
       }
      else
       {
        if (IsNull(Exp, i - 2) || IsNull(Exp, i - 1))
         Exp->Strings[i] = "NULL";
        else
         {
          IsStr1 = IsStr(Exp, i - 2);
          IsStr2 = IsStr(Exp, i - 1);
          Res    = false;
          if (IsStr1 && IsStr2)
           { // ��� �������� ������
            switch (FCondOper)
             {
             case TCondOperation::coEqu:
              Res = Exp->Strings[i - 2] == Exp->Strings[i - 1];
              break;
             case TCondOperation::coLt:
              Res = Exp->Strings[i - 2] < Exp->Strings[i - 1];
              break;
             case TCondOperation::coNotEqu:
              Res = Exp->Strings[i - 2] != Exp->Strings[i - 1];
              break;
             case TCondOperation::coLtEqu:
              Res = Exp->Strings[i - 2] <= Exp->Strings[i - 1];
              break;
             case TCondOperation::coGt:
              Res = Exp->Strings[i - 2] > Exp->Strings[i - 1];
              break;
             case TCondOperation::coGtEqu:
              Res = Exp->Strings[i - 2] >= Exp->Strings[i - 1];
              break;
             }
           }
          else if (!IsStr1 && !IsStr2)
           { //��� �������� �� ������
            switch (FCondOper)
             {
             case TCondOperation::coOr:
              Res = Exp->Strings[i - 2].ToIntDef(0) || Exp->Strings[i - 1].ToIntDef(0);
              break;
             case TCondOperation::coAnd:
              Res = Exp->Strings[i - 2].ToIntDef(0) && Exp->Strings[i - 1].ToIntDef(0);
              break;
             case TCondOperation::coEqu:
              Res = _wtof(Exp->Strings[i - 2].c_str()) == _wtof(Exp->Strings[i - 1].c_str());
              break;
             case TCondOperation::coLt:
              Res = _wtof(Exp->Strings[i - 2].c_str()) < _wtof(Exp->Strings[i - 1].c_str());
              break;
             case TCondOperation::coNotEqu:
              Res = _wtof(Exp->Strings[i - 2].c_str()) != _wtof(Exp->Strings[i - 1].c_str());
              break;
             case TCondOperation::coLtEqu:
              Res = _wtof(Exp->Strings[i - 2].c_str()) <= _wtof(Exp->Strings[i - 1].c_str());
              break;
             case TCondOperation::coGt:
              Res = _wtof(Exp->Strings[i - 2].c_str()) > _wtof(Exp->Strings[i - 1].c_str());
              break;
             case TCondOperation::coGtEqu:
              Res = _wtof(Exp->Strings[i - 2].c_str()) >= _wtof(Exp->Strings[i - 1].c_str());
              break;
             }
           }
          Exp->Strings[i] = IntToStr((int)Res);
         }
       }
      if (FCondOper == TCondOperation::coNot)
       Exp->Delete(i - 1);
      else
       {
        Exp->Delete(i - 2);
        Exp->Delete(i - 2);
       }
      return IntCalcCondition(Exp);
     }
   }
  //���
  throw ECondExpError("������ � �������, ���������� ��������");
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDIEdit::IsNull(TStringList * AExp, int AIdx)
 {
  return (AExp->Strings[AIdx].Trim().UpperCase() == "NULL");
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDIEdit::IsStr(TStringList * AExp, int AIdx)
 {
  return (AExp->Strings[AIdx].Trim()[1] == '\'');
 }
//---------------------------------------------------------------------------

