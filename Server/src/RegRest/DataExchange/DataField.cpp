// ---------------------------------------------------------------------------
#pragma hdrstop
#include "DataField.h"
#include "DataTable.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ###########################################################################
// ##                                                                       ##
// ##                              TDataField                               ##
// ##                                                                       ##
// ###########################################################################
// ---------------------------------------------------------------------------
__fastcall TDataField::TDataField(TDataTable * AOwner, TTagNode * AFieldDef)
 {
  // dsLogMessage(AFieldDef->AV["name"]/*AsXML*/, __FUNC__, 6);
  FOwner    = AOwner;
  FKeyField = AFieldDef->CmpAV("name", "key");
  FFieldDef = AFieldDef;
  // FDM = Owner->Owner->DM;
  CanSetValue = true;
  FEnabled    = true;
  // FScale      = NULL;
  FDelegated    = (bool)AFieldDef->AV["extfl"].Length();
  FName         = FFieldDef->AV["fieldname"];
  FSource       = new TdsDIEdit(this, AFieldDef, FOwner->DM->Connection);
  FDefaultValue = Variant::Empty();
  CreateScale();
 }
// ---------------------------------------------------------------------------
__fastcall TDataField::~TDataField()
 {
  // if (FScale)
  // delete FScale;
//  if (FSource)
   delete FSource;
 }
// ---------------------------------------------------------------------------
void __fastcall TDataField::CreateScale()
 {
  if (FFieldDef->CmpName("binary"))
   {
    if (FFieldDef->CmpAV("default", "uncheck"))
     FDefaultValue = Variant(0);
    else
     FDefaultValue = Variant(1);
   }
  else if (FFieldDef->CmpName("choice"))
   {
    TTagNode * itxNode = FFieldDef->GetFirstChild();
    while (itxNode)
     {
      if (itxNode->CmpName("choicevalue"))
       {
        if (FFieldDef->CmpAV("default", itxNode->AV["value"]))
         FDefaultValue = Variant(itxNode->AV["value"].ToIntDef(0));
       }
      itxNode = itxNode->GetNext();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TDataField::FSetValue(Variant AValue)
 {
  /* if (!Enabled&& !FDelegated)                                      //!!! ���� ���-�� ��������� ��� ����� ������������� �� �������
   throw EFieldDisabled("���� �� �������� ��� ��������������", this); */
  Variant tmpValue = Variant::Empty();
  try
   {
    if (!(AValue.IsNull() || AValue.IsEmpty()))
     {
      if (FFieldDef->CmpName("binary,choice"))
       tmpValue = (int)AValue;
      else if (FFieldDef->CmpName("digit"))
       tmpValue = AValue;
      else if (FFieldDef->CmpName("text"))
       tmpValue = AValue;
      else if (FFieldDef->CmpName("date"))
       {
        if (VarToStr(AValue).Length())
         tmpValue = TDateTime(VarToStr(AValue));
       }
      else if (FFieldDef->CmpName("extedit"))
       tmpValue = AValue;
      else if (FFieldDef->CmpName("choiceDB,class"))
       tmpValue = AValue; // !!! (__int64)AValue;
      else if (FFieldDef->CmpName("choiceTree"))
       tmpValue = (__int64)AValue;
      else
       tmpValue = AValue;
     }
   }
  catch (Exception & E)
   {
    throw EFieldEditError(E.Message, this, tmpValue, etInvalidDataValue);
   }
  try
   {
    FSource->SetValue(tmpValue);
   }
  catch (EDataTypeError & E)
   {
    if (!CanSetValue)
     throw EFieldEditError(E.Message, this, tmpValue, etInvalidDataType);
    if (evtFieldEditErr)
     {
      try
       {
        CanSetValue = false;
        evtFieldEditErr(this, this, tmpValue, etInvalidDataValue);
       }
      __finally
       {
        CanSetValue = true;
       }
     }
    else
     throw EFieldEditError(E.Message, this, tmpValue, etInvalidDataType);
   }
  catch (EDataValueError & E)
   {
    if (!CanSetValue)
     throw EFieldEditError(E.Message, this, tmpValue, etInvalidDataValue);
    if (evtFieldEditErr)
     {
      try
       {
        CanSetValue = false;
        evtFieldEditErr(this, this, tmpValue, etInvalidDataValue);
       }
      __finally
       {
        CanSetValue = true;
       }
     }
    else
     throw EFieldEditError(E.Message, this, tmpValue, etInvalidDataValue);
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TDataField::FGetEnabled()
 {
  return FEnabled;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDataField::CheckRequared()
 {
  return FSource->CheckReqValue();
 }
// ---------------------------------------------------------------------------
bool __fastcall TDataField::IsNull()
 {
  return (FValue.IsNull() || FValue.IsEmpty());
 }
// ---------------------------------------------------------------------------
void __fastcall TDataField::Clear()
 {
  FSetValue(Variant::Empty());
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDataField::AsTypeCheck(bool & ANull, bool & AEmpty)
 {
  // ���������� �������� AVal � ������� SQL
  // � ������������ � ����� ���� Src
  // ������������ ��� ������������ "UPDATE"
  TReplaceFlags rFlag;
  rFlag << rfReplaceAll;
  ANull  = IsNull();
  AEmpty = ANull;
  UnicodeString RC = "";
  try
   {
    if (!ANull && FEnabled)
     {
      AEmpty = !(AsString.Trim().Length());
      if (FFieldDef->CmpName("class,choiceDB,choiceTree,binary,choice,digit"))
       RC = FName + "=" + StringReplace(AsString, ",", ".", rFlag);
      else if (FFieldDef->CmpName("text"))
       RC = "Upper(" + FName + ")='" + StringReplace(AsString.UpperCase(), "'", "`", rFlag) + "'";
      else if (FFieldDef->CmpName("extedit"))
       {
        if (FFieldDef->CmpAV("fieldtype", "integer"))
         RC = FName + "=" + StringReplace(AsString, ",", ".", rFlag);
        else
         RC = "Upper(" + FName + ")='" + StringReplace(AsString.UpperCase(), "'", "`", rFlag) + "'";
       }
      else if (FFieldDef->CmpName("date"))
       RC = FName + "='" + AsDateTime.FormatString("dd.mm.yyyy") + "'";
     }
    else
     {
      if (FFieldDef->CmpName("class,choiceDB,choiceTree,binary,choice,digit"))
       RC = "(" + FName + "=0 or " + FName + " is null)";
      else if (FFieldDef->CmpName("text,extedit"))
       RC = "(" + FName + "='' or " + FName + " is null)";
      else if (FFieldDef->CmpName("date"))
       RC = FName + " is null";
      if (FName.UpperCase() == "R002F") // !!! ������� (��� ����������� �������� �� ������������ ��� ������ ��������)
       {
        ANull  = false;
        AEmpty = false;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
Variant __fastcall TDataField::GetValAsVariant()
 {
  return FValue;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDataField::GetValAsBool()
 {
  return bool((int)FValue);
 }
// ---------------------------------------------------------------------------
TDateTime __fastcall TDataField::GetValAsDateTime()
 {
  if (!(FValue.IsEmpty() || FValue.IsNull()))
   return TDateTime(VarToStr(FValue));
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
double __fastcall TDataField::GetValAsFloat()
 {
  return double(FValue);
 }
// ---------------------------------------------------------------------------
int __fastcall TDataField::GetValAsInteger()
 {
  return int(FValue);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDataField::GetValAsString()
 {
  return VarToStr(FValue);
 }
// ---------------------------------------------------------------------------
void __fastcall TDataField::SetValAsVariant(Variant AValue)
 {
  FSetValue(AValue);
 }
// ---------------------------------------------------------------------------
void __fastcall TDataField::SetValAsBool(bool AValue)
 {
  FSetValue(Variant((int)AValue));
 }
// ---------------------------------------------------------------------------
void __fastcall TDataField::SetValAsDateTime(TDateTime AValue)
 {
  FSetValue(Variant(AValue));
 }
// ---------------------------------------------------------------------------
void __fastcall TDataField::SetValAsFloat(double AValue)
 {
  FSetValue(Variant(AValue));
 }
// ---------------------------------------------------------------------------
void __fastcall TDataField::SetValAsInteger(int AValue)
 {
  FSetValue(Variant(AValue));
 }
// ---------------------------------------------------------------------------
void __fastcall TDataField::SetValAsString(UnicodeString AValue)
 {
  FSetValue(Variant(AValue));
 }
// ---------------------------------------------------------------------------
void __fastcall TDataField::ImpEDSetValue(Variant AVal)
 {
  FValue = AVal;
 }
// ---------------------------------------------------------------------------
Variant __fastcall TDataField::ImpEDGetValue()
 {
  return FValue;
 }
// ---------------------------------------------------------------------------
TDataTable * __fastcall TDataField::FGetOwner()
 {
  return FOwner;
 }
// ---------------------------------------------------------------------------
