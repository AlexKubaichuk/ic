// ---------------------------------------------------------------------------
// #include <vcl.h>
// #include <stdio.h>
// #include <clipbrd.hpp>
#pragma hdrstop

#include "dsDICommon.h"
// !!!*#include "EncodeTable.h"
#include "DataField.h"
#include "DataTable.h"

#pragma package(smart_init)

// ---------------------------------------------------------------------------
__fastcall TdsDICommon::TdsDICommon(TFDConnection * AConnection, TAxeXMLContainer * AXMLList)
 {
  dsLogBegin(__FUNC__);
  FConnection = AConnection;
  FActive     = false;
  FSystemMode = smUnknown;
  // !!!*  FEncodeTable = new TDataEncodeTable(this);
  FxmlImpDataDef = new TTagNode;
  FXMLList       = AXMLList;
  DTFormats[0]   = "dd.mm.yyyy �.";
  DTFormats[1]   = "dd.mm.yyyy hh:nn:ss";
  DTFormats[2]   = "hh:nn:ss";
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
__fastcall TdsDICommon::~TdsDICommon()
 {
  dsLogBegin(__FUNC__);
  delete FxmlImpDataDef;

  for (TDataTableMap::iterator i = FClasses.begin(); i != FClasses.end(); i++)
   delete i->second;
  FClasses.clear();
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDICommon::NewGUID()
 {
  dsLogBegin(__FUNC__);
  dsLogEnd(__FUNC__);
  return icsNewGUID().UpperCase();
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsDICommon::CreateTempQuery()
 {
  dsLogBegin(__FUNC__);
  TFDQuery * FQ = new TFDQuery(NULL);
  FQ->Transaction = new TFDTransaction(NULL);
  try
   {
    FQ->Transaction->Connection = FConnection;
    FQ->Connection              = FConnection;

    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();

   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return FQ;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDICommon::DeleteTempQuery(TFDQuery * AQuery)
 {
  dsLogBegin(__FUNC__);
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDICommon::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDICommon::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString AParamName,
 UnicodeString AId, UnicodeString ALogComment)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  TTime FBeg = Time();
  UnicodeString FParams = "; Params: ";
  if (AParamName.Length())
   FParams += AParamName + "=" + AId;
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParamName)->Value = AId;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL(((ALogComment.Length()) ? ALogComment : ASQL) + FParams, __FUNC__, FBeg - Time(), 5);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDICommon::quImportExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
 TDataFieldMap * AParams, UnicodeString ALogComment, UnicodeString AClearFields)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  TTime FBeg = Time();
  TDataField * tmpField;
  UnicodeString FParams = "; Params: ";
  for (TDataFieldMap::iterator i = AParams->begin(); i != AParams->end(); i++)
   {
    tmpField = i->second;
    if (!tmpField->IsKey && !tmpField->DefNode->CmpAV("comment", "calc"))
     {
      if (!tmpField->DefNode->CmpAV("uid", AClearFields))
       {
        if (tmpField->IsNull())
         FParams += "p" + tmpField->Name + "=null; ";
        else
         FParams += "p" + tmpField->Name + "=" + tmpField->AsString + "; ";
       }
     }
   }
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      for (TDataFieldMap::iterator i = AParams->begin(); i != AParams->end(); i++)
       {
        tmpField = i->second;
        if (!tmpField->DefNode->CmpAV("comment", "calc"))
         {
          if (!tmpField->DefNode->CmpAV("uid", AClearFields))
           {
            try
             {
              if (tmpField->IsNull())
               AQuery->ParamByName("p" + tmpField->Name)->Clear();
              else
               {
                if (tmpField->DefNode->CmpName("date"))
                 {
                  if (!tmpField->AsString.Trim().Length())
                   AQuery->ParamByName("p" + tmpField->Name)->Clear();
                  else
                   AQuery->ParamByName("p" + tmpField->Name)->Value = tmpField->AsDateTime;
                 }
                else
                 AQuery->ParamByName("p" + tmpField->Name)->Value = tmpField->Value;
               }
             }
            catch (System::Sysutils::Exception & E)
             {
              dsLogError(E.Message + "; param = " + tmpField->Name + "; value=" + tmpField->AsString, __FUNC__);
              throw;
             }
           }
         }
       }
      // dsLogMessage("before OpenOrExecute");
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL(((ALogComment.Length()) ? ALogComment : ASQL) + FParams, __FUNC__, FBeg - Time(), 5);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
TDataTable * __fastcall TdsDICommon::FGetClassifier(const UnicodeString AUID)
 {
  dsLogBegin(__FUNC__);
  TDataTable * RC = NULL;
  try
   {
    if (!FActive)
     throw EConnectionError("����������� ���������� � ��");
    TDataTableMap::iterator FRC = FClasses.find(AUID);
    if (FRC == FClasses.end())
     {
      TTagNode * clDef = FxmlImpDataDef->GetTagByUID(AUID);

      if (clDef && clDef->CmpName("class"))
       {
        RC             = new TDataTable(this, clDef);
        FClasses[AUID] = RC;
       }
      else
       throw EClassifierError("������������� ����������� � ������ ���������������, uid= \"" + AUID + "\" ");
     }
    else
     RC = FRC->second;
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDICommon::DeleteClassifier(const UnicodeString AUID)
 {
  dsLogBegin(__FUNC__);
  try
   {
    if (!FActive)
     throw EConnectionError("����������� ���������� � ��");
    TDataTableMap::iterator FRC = FClasses.find(AUID);
    if (FRC != FClasses.end())
     {
      delete FRC->second;
      FClasses.erase(AUID);
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDICommon::Connect(const UnicodeString ARegGUI, const UnicodeString AImpGUI)
 {
  dsLogBegin(__FUNC__);
  FActive     = false;
  FSystemMode = smNormal;

  try
   {
    try
     {
      TTagNode * tmp;

      // ������ �������� ������������ - registry
      FxmlImpDataDef->Assign(FXMLList->GetXML(ARegGUI), true);
      if (!FxmlImpDataDef->CmpName("registr"))
       throw EConnectionError("����������� �������� ��������� ������������� ������.");
      // ������ �������� ���������������� ����� - immcard
      // tmp = FxmlImpDataDef->GetChildByName("units");
      // if (!tmp)
      // tmp = FxmlImpDataDef->AddChild("units");

      FActive = true;
     }
    __finally
     {
     }
   }
  catch (Exception & E)
   {
    throw EConnectionError(E.Message);
   }
  catch (...)
   {
    throw EConnectionError("������ ���������� � ��.");
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDICommon::Disconnect()
 {
  dsLogBegin(__FUNC__);
  FActive     = false;
  FSystemMode = smUnknown;
  // delete FMainUnit;
  for (TDataTableMap::iterator i = FClasses.begin(); i != FClasses.end(); i++)
   delete i->second;
  FClasses.clear();
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDICommon::ImportCardData(System::UnicodeString AId, TJSONObject * AValue,
 System::UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "error";
  try
   {
    if (FOnImportCard)
     RC = FOnImportCard(AId, AValue, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
