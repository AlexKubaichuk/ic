//---------------------------------------------------------------------------

#ifndef diDataTypeDefH
#define diDataTypeDefH
#include <System.JSON.hpp>
//#include <map>
#include "XMLContainer.h"
//---------------------------------------------------------------------------
namespace DataExchange
{
class TDataEncodeTable;
class TDataTable;
class TDataField;
typedef  map<const UnicodeString, TDataTable*,ltstr> TDataTableMap;
typedef  map<const UnicodeString, TDataField*,ltstr> TDataFieldMap;
//---------------------------------------------------------------------------
typedef bool (__closure *TdsDIOnExtGetCodeEvent)(UnicodeString ATab, UnicodeString &ARet);
typedef bool __fastcall (__closure * TDISetExtEditValueEvent)(TDataTable *ATable, TTagNode *ADefNode, UnicodeString AVal, UnicodeString AExtVal, TJSONObject *ACls);
typedef System::UnicodeString __fastcall (__closure * TDIImportCardDataEvent)(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
//---------------------------------------------------------------------------
struct LtStr { bool operator()(UnicodeString s1, UnicodeString s2) const { return (s1.UpperCase() < s2.UpperCase());} };

enum TDataEncodeTableType {ettUnknown, ettClass, ettMainUnit};
enum TDataScaleType {stUnknown, stBinary, stChoice, stIntDigit, stFloatDigit, stText, stDate, stTime, stDateTime, stExtEdit, stChoiceDB, stChoiceTree };
enum TTextDataScaleCastType  {tctNo, tctLo, tctUp, tctUp1st};
enum TExtEditDataScaleEdType {eetEdit, eetChoice, eetCheckBox, eetComboEdit};
enum TChoiceTreeDataScaleType {ctsInt, ctsExt};

// ��� ������ ��������� �������� ����
enum TFieldEditErrorType {etInvalidDataType, etInvalidDataValue};
// ����� ������ �������
enum TSystemMode {smUnknown, smNormal, smLimited};
// ����� �������
enum TImportMode {imNormal, imLimited};
}; // end of namespace DataExchange
using namespace DataExchange;
#endif
