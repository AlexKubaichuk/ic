//---------------------------------------------------------------------------

#ifndef dsDIEditH
#define dsDIEditH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FireDAC.Comp.Client.hpp>
//#include "DataField.h"
//#include "DataExchange.h"
//#include <DB.hpp>
#include "XMLContainer.h"
//#include "FIBQuery.hpp"
////#include "pFIBQuery.hpp"
//#define F_DATE 1
//#define F_DATETIME 2
//#define F_TIME 3
//---------------------------------------------------------------------------
//#define max(a,b)  (((a) > (b)) ? (a) : (b))
//#define min(a,b)  (((a) < (b)) ? (a) : (b))
//---------------------------------------------------------------------------
namespace DataExchange
 {
 class EDataTypeError : public Exception
  {
  public:
   __fastcall EDataTypeError(const UnicodeString Msg) : Exception(Msg){};
  };
 //---------------------------------------------------------------------------
 class EDataValueError : public Exception
  {
  public:
   __fastcall EDataValueError(const UnicodeString Msg) : Exception(Msg){};
  };
 //---------------------------------------------------------------------------
 //class PACKAGE TDataField;
 class PACKAGE TdsDIEdit : public TObject
  {
private:
  enum class TCondOperation {coNone, coNot, coOr, coAnd, coEqu, coNotEqu, coLt, coLtEqu, coGt, coGtEqu };
  TFDConnection *FConnection;
   TObject *  FOwner;
   UnicodeString FDepTab, FDepFlVal, FRetFlName, FRetVals;
   TList *       SetValList;
   TList *       SetEnList;
   UnicodeString FFlName, FDepValues, ChDBValue;
   bool          isEnabled;

   void __fastcall GetDependClassCode(UnicodeString ADepClName, UnicodeString ADepFlVal, UnicodeString ARetFlName, UnicodeString & ARetVals);
   void __fastcall DataChange();
   bool __fastcall Condition(TTagNode * CondNode);
   Variant __fastcall GetCompValue(UnicodeString ARef);
   void __fastcall CalcParam(TTagNode * AFirst, TStringList * AParam);
   UnicodeString __fastcall FieldCast(UnicodeString AVal);
   void __fastcall SetDependVal(UnicodeString ADepClCode, __int64 ACode);

   bool __fastcall isRefCtrl(TTagNode * ANode);
   bool __fastcall isRefMCtrl(TTagNode * ANode);

   bool __fastcall CheckChDbValue(UnicodeString ACode);
   Variant __fastcall GetChDBFieldValue(UnicodeString  AFieldName, UnicodeString  ACode);

  TFDQuery * __fastcall CreateTempQuery();
  void __fastcall DeleteTempQuery(TFDQuery * AQuery);
  bool __fastcall quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool __fastcall quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString AParamName, UnicodeString AId, UnicodeString ALogComment = "");

  int __fastcall IntCalcCondition(TStringList* Exp);
//  Variant __fastcall GetOperandVal(UnicodeString AOpVal);
  bool __fastcall IsNull(TStringList *AExp, int AIdx);
  bool __fastcall IsStr(TStringList * AExp, int AIdx);
  public:
   TTagNode *    FNode;
   void __fastcall SetEnable(bool AEnable);
   void __fastcall SetValue(Variant AVal, bool AEnable = true);
   void __fastcall SetOnValDepend(TTagNode * ANode);
   void __fastcall SetOnEnable(TTagNode * ANode);
   bool __fastcall CheckReqValue();
   UnicodeString __fastcall GetValue(UnicodeString ARef);
  __published:
   __fastcall TdsDIEdit(TObject * AOwner, TTagNode * ANode, TFDConnection *AConnection);
   __fastcall ~TdsDIEdit();
  };
 }; //end of namespace DataExchange
using namespace DataExchange;
//---------------------------------------------------------------------------
#endif
