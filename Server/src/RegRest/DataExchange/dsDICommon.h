//---------------------------------------------------------------------------
#ifndef dsDICommonH
#define dsDICommonH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
#include <FireDAC.Comp.Client.hpp>
#include <System.JSON.hpp>
#include "icsLog.h"
#include "diDataTypeDef.h"
//#include "diDataTypeDef.h"
//#include "dsDIEdit.h"
/*
#include <Classes.hpp>
#include <DB.hpp>
#include <DBCtrls.hpp>
#include <Registry.hpp>
#include "FIBDatabase.hpp"
#include "FIBDataSet.hpp"
#include "FIBQuery.hpp"
#include "pFIBDatabase.hpp"
#include "pFIBDataSet.hpp"
#include "pFIBQuery.hpp"
//#include <ADODB.hpp>
*/
namespace DataExchange
{
//#define quFN(a) ImpDM->quFree->FN(a)
//#define quFNInt(a)  quFN(a)->AsInteger
//#define quFNStr(a)  quFN(a)->AsString
//#define quFNDate(a) quFN(a)->AsDateTime
//---------------------------------------------------------------------------
class EConnectionError : public Exception
{
public:
    __fastcall EConnectionError(const UnicodeString Msg) : Exception(Msg) {}
};

//---------------------------------------------------------------------------
// �����������, ���� ������ � ���������������
class EClassifierError : public Exception
{
public:
    __fastcall EClassifierError(const UnicodeString Msg) : Exception(Msg) {}
};

//---------------------------------------------------------------------------
class EUnitError : public Exception
{
public:
    __fastcall EUnitError(const UnicodeString Msg) : Exception(Msg) {}
};

//---------------------------------------------------------------------------
class PACKAGE TdsDICommon : public TObject
{
private:
  TFDConnection *FConnection;
  bool          FActive;

  TImportMode FMode;
  TSystemMode FSystemMode;
//!!!*  TDataEncodeTable *FEncodeTable;

  TDataTableMap FClasses;
//  TDataTableMap FUnits;
//  TDataTable  *FMainUnit;
  TdsDIOnExtGetCodeEvent FOnExtGetCode;
  TDIImportCardDataEvent FOnImportCard;

  TDataTable* __fastcall FGetClassifier(const UnicodeString AUID);
//  TDataTable* __fastcall FGetUnit(const UnicodeString AUID);
//  TDataTable* __fastcall FGetMainUnit();

  TTagNode     *FxmlImpDataDef;

  TAxeXMLContainer *FXMLList;

public:
  __fastcall TdsDICommon(TFDConnection *AConnection, TAxeXMLContainer *AXMLList);
  __fastcall ~TdsDICommon();

  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                                     UnicodeString AParamName, UnicodeString AId,
                                     UnicodeString ALogComment = "");
  bool __fastcall quImportExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, TDataFieldMap *AParams, UnicodeString ALogComment, UnicodeString AClearFields);

    UnicodeString   DTFormats[3];

    UnicodeString __fastcall NewGUID();
  void __fastcall Connect(const UnicodeString ARegGUI, const UnicodeString AImpGUI);
  void __fastcall Disconnect();
  UnicodeString __fastcall ImportCardData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);

  // ������� ���������� � ��������
  __property bool Active = {read = FActive};

  // ����� ������
  __property TImportMode Mode = {read = FMode, write = FMode};
  // ����� ������ �������
  // ������������ �� ������� �������� ������������ ������ � ���� ������
  // �������
  // ���� Active == false, �� SystemMode == smUnknown
  __property TSystemMode SystemMode = {read = FSystemMode};
//!!!*  __property TDataEncodeTable *EncodeTable = {read = FEncodeTable};

  // ������ TDataTable ��� �������������� � ��������� UID
  __property TDataTable* Classifiers[const UnicodeString AUID] = {read = FGetClassifier};
  void __fastcall DeleteClassifier(const UnicodeString AUID);
  // ������ TDataTable ��� ������� ���������������� �����
//  __property TDataTable* MainUnit = {read = FGetMainUnit};
  // ������ TDataTable ��� ����� � ��������� UID
//  __property TDataTable* Units[const UnicodeString AUID] = {read = FGetUnit};

  __property TTagNode* xmlImpDataDef     = {read = FxmlImpDataDef};
  __property TFDConnection *Connection     = {read = FConnection};

  __property TAxeXMLContainer *XMLList= {read = FXMLList, write = FXMLList};

    __property TdsDIOnExtGetCodeEvent OnExtGetCode = {read = FOnExtGetCode, write = FOnExtGetCode};
    __property TDIImportCardDataEvent OnImportCard = {read = FOnImportCard, write = FOnImportCard};
};
//---------------------------------------------------------------------------
}; // end of namespace DataExchange
using namespace DataExchange;
//---------------------------------------------------------------------------
#endif

