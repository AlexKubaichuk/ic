// ---------------------------------------------------------------------------
#pragma hdrstop
#include "DataTable.h"
#include "DataField.h"
#include "XMLContainer.h"
#include "JSONUtils.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ###########################################################################
// ##                                                                       ##
// ##                             TDataTable                                ##
// ##                                                                       ##
// ###########################################################################
#define _NEW_CODE_ -777
// ---------------------------------------------------------------------------
__fastcall TDataTable::TDataTable(TdsDICommon * ADM, TTagNode * ADataDef)
 {
  dsLogBegin(__FUNC__, 6);
  FClearCodes = "";
  dsLogMessage(ADataDef->AV["name"] /* AsXML */ , __FUNC__, 6);
  emptyStr = "";
  FDM      = ADM;
  FDataDef         = ADataDef;
  FDelegated       = (bool)ADataDef->AV["extfl"].Length();
  FKeyScaleDef     = new TTagNode;
  FLinkKeyScaleDef = new TTagNode;
  FKeyScaleDef->AsXML =
    "<text " "name='Key' " "uid='0000' " "ref='' " "required='yes' " "inlist='no' " "depend='' " "isedit='yes' "
    "length='255' " "is3D='yes' " "default=''/>";
  FLinkKeyScaleDef->AsXML =
    "<text " "name='Key' " "uid='0000' " "ref='' " "required='yes' " "inlist='no' " "depend='' " "isedit='yes' "
    "length='255' " "is3D='yes' " "default=''/>";
  FLinkKeyName = "";
  FTableName   = "";
  FPKName      = "";
  FUID         = "";
  FTableType   = ettUnknown;
  if (FDataDef->CmpName("class"))
   {
    FTableType = ettClass;
    UnicodeString tmpStr = FDataDef->AV["uid"];
    FDataDef->Iterate(FCreateFields, tmpStr); // Iterate1st
    FTableName = "CLASS_" + FDataDef->AV["uid"];
    if (FDataDef->AV["tab"].Length())
     FTableName = FDataDef->AV["tab"];
    FPKName = "CODE";
    TTagNode * FPKDef = FDataDef->GetChildByAV("", "comment", "key", true);
    if (FPKDef)
     {
      FPKName = FPKDef->AV["fl"].UpperCase();
     }
    FUID = FDataDef->AV["uid"];
    FKeyScaleDef->AV["uid"]       = FDataDef->AV["uid"];
    FKeyScaleDef->AV["fieldname"] = FPKName;
    FFields[FPKName]              = new TDataField(this, FKeyScaleDef);
    FFields[FPKName]->IsKey       = true;
    if (FPKDef)
     FUIDFields[FPKDef->AV["uid"].UpperCase()] = FFields[FPKName];
   }
  dsLogEnd(__FUNC__, 6);
 }
// ---------------------------------------------------------------------------
__fastcall TDataTable::~TDataTable()
 {
  dsLogBegin(__FUNC__, 6);
  for (TDataFieldMap::iterator i = FFields.begin(); i != FFields.end(); i++)
   delete i->second;
  FFields.clear();
  FRUFields.clear();
  FUIDFields.clear();
  if (FKeyScaleDef)
   delete FKeyScaleDef;
  if (FLinkKeyScaleDef)
   delete FLinkKeyScaleDef;
  dsLogEnd(__FUNC__, 6);
 }
// ---------------------------------------------------------------------------
bool __fastcall TDataTable::CheckRequared()
 {
  // !!!+  dsLogBegin(__FUNC__, 6);
  bool RC = false;
  for (TDataFieldMap::iterator i = FFields.begin(); i != FFields.end(); i++)
   {
    if (i->second->IsKey)
     {
      RC = !i->second->CheckRequared();
     }
    else if (!i->second->CheckRequared())
     {
      if (evtReqError)
       evtReqError(this, i->second);
      else
       throw ERequaredFieldsError("�� ������ �������� ��� ������������� ���� \"" + i->second->Name + "\"");
     }
   }
  // !!!  dsLogEnd(__FUNC__, 6);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDataTable::FieldExists(UnicodeString AFieldName)
 {
  dsLogBegin(__FUNC__, 6);
  bool RC = false;
  try
   {
    UnicodeString FFN = AFieldName.UpperCase();
    TDataFieldMap::iterator FRC = FFields.find(FFN);
    if (FRC != FFields.end())
     RC = true;
    else
     {
      FRC = FRUFields.find(FFN);
      if (FRC != FRUFields.end())
       RC = true;
      else
       {
        FRC = FUIDFields.find(FFN);
        if (FRC != FUIDFields.end())
         RC = true;
       }
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__, 6);
  return RC;
 }
// ---------------------------------------------------------------------------
TDataField * __fastcall TDataTable::TryGetField(UnicodeString AFieldName)
 {
//  dsLogBegin(__FUNC__, 6);
  TDataField * RC = NULL;
  try
   {
    UnicodeString FFN = AFieldName.UpperCase();
    TDataFieldMap::iterator FRC = FFields.find(FFN);
    if (FRC != FFields.end())
     RC = FRC->second;
    else
     {
      FRC = FRUFields.find(FFN);
      if (FRC != FRUFields.end())
       RC = FRC->second;
      else
       {
        FRC = FUIDFields.find(FFN);
        if (FRC != FUIDFields.end())
         RC = FRC->second;
       }
     }
   }
  __finally
   {
   }
//  dsLogEnd(__FUNC__, 6);
  return RC;
 }
// ---------------------------------------------------------------------------
TDataField * __fastcall TDataTable::GetField(UnicodeString AFieldName)
 {
//  dsLogBegin(__FUNC__, 6);
  TDataField * RC = TryGetField(AFieldName);
  try
   {
    if (!RC)
     throw EFieldError("���� \"" + AFieldName + "\" ����������� � ������ �����", NULL);
   }
  __finally
   {
   }
//  dsLogEnd(__FUNC__, 6);
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDataTable::GetNewCode(UnicodeString AOldCode, bool & AIsAppend, UnicodeString UCODE)
 {
  dsLogBegin(__FUNC__, 6);
  UnicodeString RC = AOldCode;
  if (AOldCode.UpperCase() == IntToStr(_NEW_CODE_).UpperCase())
   AIsAppend = true;
  // AIsAppend = (AOldCode.ToIntDef(0) == _NEW_CODE_);
  if (AIsAppend)
   {
    if (FDataDef->AV["tab"].Length())
     {
      if (FDM->OnExtGetCode)
       FDM->OnExtGetCode(FDataDef->AV["tab"], RC);
     }
    else
     {
      TFDQuery * FQ = FDM->CreateTempQuery();
      try
       {
        FQ->Transaction->StartTransaction();
        FQ->Command->CommandKind = skStoredProc;
        FQ->SQL->Text            = "MCODE_" + FUID;
        FQ->Prepare();
        FQ->OpenOrExecute();
        RC = FQ->FieldByName("MCODE")->AsInteger;
       }
      __finally
       {
        FDM->DeleteTempQuery(FQ);
       }
     }
   }
  dsLogEnd(__FUNC__, 6);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TDataTable::New(TJSONObject * AValue, TJSONObject * ACls, bool AImport)
 {
  dsLogBegin(__FUNC__, 6);
  for (TDataFieldMap::iterator i = FFields.begin(); i != FFields.end(); i++)
   i->second->ImpEDSetValue(i->second->Default);
  Fields[FPKName]->ImpEDSetValue(_NEW_CODE_);
  if (AValue)
   {
    UnicodeString FId, FVal;
    TJSONObject * FValue;
    TJSONString * tmpData;
    TTagNode * FFlDefNode;
    TDataField * tmpField;
    TJSONPairEnumerator * itPair = AValue->GetEnumerator();
    while (itPair->MoveNext())
     {
      FId      = JSONEnumPairCode(itPair);
      tmpField = TryGetField(FId);
      if (tmpField)
       {
        FFlDefNode      = tmpField->DefNode;
        FValue          = (TJSONObject *)JSONPairValue(itPair->Current);
        tmpField->Value = Variant::Empty();
        if (!FValue->Null)
         {
          if (AImport)
           {
            tmpData = (TJSONString *)FValue;
            if (FFlDefNode->CmpName("binary,choice,choiceDB,choiceTree,extedit"))
             {
              if (FValue->Count)
               {
                FVal = ((TJSONString *)FValue->Pairs[0]->JsonString)->Value();
                if (FVal.Trim().Length())
                 {
                  if (FFlDefNode->CmpName("choiceDB"))
                   {
                    if (FOnGetClassCodeValue)
                     tmpField->Value = FOnGetClassCodeValue(FFlDefNode, ACls, FVal);
                    else
                     tmpField->Value = FVal;
                   }
                  else if (FFlDefNode->CmpName("extedit"))
                   {
                    FOnSetExtEditValue(this, FFlDefNode, FVal,
                      ((TJSONString *)FValue->Pairs[0]->JsonValue)->Value(), ACls);
                   }
                  else if (FFlDefNode->CmpName("binary,choice"))
                   tmpField->AsInteger = FVal.ToInt();
                  else
                   tmpField->Value = FVal;
                 }
                else
                 tmpField->Value = Variant::Empty();
               }
             }
            else
             {
              if (!tmpData->Value().Trim().Length())
               tmpField->Value = Variant::Empty();
              else
               {
                if (FFlDefNode->CmpName("date"))
                 tmpField->AsDateTime = TDate(tmpData->Value());
                else
                 tmpField->Value = tmpData->Value();
               }
             }
           }
          else
           { // ���� CODE ����� �������������, ������� ��������� ��������
            FVal = JSONEnumPairValue(itPair);
            if (tmpField->DefNode->CmpName("extedit"))
             FOnSetExtEditValue(this, tmpField->DefNode, FVal, FVal, NULL);
            else
             tmpField->AsString = FVal;
           }
         }
       }
     }
   }
  dsLogEnd(__FUNC__, 6);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDataTable::InsertOrUpdate(UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__, 6);
  UnicodeString RC = "error";
//  __int64 memsize = PrintProcessMemoryInfo().ToIntDef(0);
  try
   {
    try
     {
      bool IsAppend = CheckRequared();
      UnicodeString NewCode = "0";
      NewCode = GetNewCode(Fields[FPKName]->AsString, IsAppend);
      // �������� �� ������� ������ �� ����� ����������� �� ������������
      CheckUnique(IsAppend, NewCode);
//      dsLogMessage("1. CheckUnique Size: " + IntToStr(PrintProcessMemoryInfo().ToIntDef(0) - memsize), __FUNC__);
      // �� � �������, �������� ��������� � ����
      UnicodeString flValues, flNames;
      flValues = "";
      flNames  = "";
      TDataField * tmpField;
      for (TDataFieldMap::iterator i = FFields.begin(); i != FFields.end(); i++)
       {
        tmpField = i->second;
        if (!tmpField->IsKey)
         {
          if (!tmpField->DefNode->CmpAV("comment", "calc"))
           {
            flNames += "," + tmpField->Name;
            flValues += ",:p" + tmpField->Name;
           }
         }
        else
         tmpField->AsString = NewCode;
       }
//      dsLogMessage("2. prepare SQL Size: " + IntToStr(PrintProcessMemoryInfo().ToIntDef(0) - memsize), __FUNC__);
      UnicodeString FSQL;
      if (IsAppend)
       FSQL = "Insert into " + FTableName + "(" + FPKName + flNames + ") Values (:p" + FPKName + flValues + ")";
      else
       FSQL = "Update or insert into  " + FTableName + "(" + FPKName + flNames + ") Values (:p" + FPKName +
         flValues + ")";
      TFDQuery * FQ = FDM->CreateTempQuery();
      try
       {
        try
         {
          if (FDM->quImportExecParam(FQ, true, FSQL, &FFields, "", ""))
           {
            ARecId = NewCode;
            RC     = "ok";
           }
//          dsLogMessage("3. quImportExecParam Size: " + IntToStr(PrintProcessMemoryInfo().ToIntDef(0) - memsize),            __FUNC__);
         }
        catch (Exception & E)
         {
          if (FQ->Transaction->Active)
           FQ->Transaction->Rollback();
          throw EImportError(E.Message);
         }
       }
      __finally
       {
        FDM->DeleteTempQuery(FQ);
       }
     }
    catch (Exception & E)
     {
      RC = E.Message;
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
//  dsLogMessage("4. finally Size: " + IntToStr(PrintProcessMemoryInfo().ToIntDef(0) - memsize), __FUNC__);
  dsLogEnd(__FUNC__, 6);
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDataTable::Import(UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__, 6);
  UnicodeString RC = "error";
  try
   {
    try
     {
      bool IsAppend = true;
      CheckRequared();
      UnicodeString NewCode = GetNewCode(Fields[FPKName]->AsString, IsAppend);
      // �������� �� ������� ������ �� ����� ����������� �� ������������
      CheckUnique(IsAppend, NewCode, true);
      // �� � �������, �������� ��������� � ����
      UnicodeString flValues, flNames;
      flValues = "";
      flNames  = "";
      TDataField * tmpField;
      for (TDataFieldMap::iterator i = FFields.begin(); i != FFields.end(); i++)
       { // ��������� ���������������� ������
        tmpField = i->second;
        if (!tmpField->IsKey)
         {
          if (!tmpField->DefNode->CmpAV("comment", "calc"))
           {
            if (!tmpField->DefNode->CmpAV("uid", FClearCodes))
             {
              flNames += "," + tmpField->Name;
              flValues += ",:p" + tmpField->Name;
             }
           }
         }
        else
         tmpField->AsString = NewCode;
       }
      UnicodeString FSQL;
      FSQL = "Update or insert into  " + FTableName + "(" + FPKName + flNames + ") Values (:p" + FPKName + flValues +
        ") MATCHING (" + FPKName + ")";
      TFDQuery * FQ = FDM->CreateTempQuery();
      try
       {
        try
         {
          if (FDM->quImportExecParam(FQ, true, FSQL, &FFields, "", FClearCodes))
           {
            ARecId = NewCode;
            RC     = "ok";
           }
         }
        catch (Exception & E)
         {
          if (FQ->Transaction->Active)
           FQ->Transaction->Rollback();
          throw EImportError(E.Message);
         }
       }
      __finally
       {
        FDM->DeleteTempQuery(FQ);
       }
     }
    catch (Exception & E)
     {
      RC = E.Message;
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__, 6);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDataTable::CheckUnique(bool AIsAppend, UnicodeString & ANewCode, bool AImport)
 {
  bool RC = false;
  try
   {
    try
     {
      TTagNode * FDescription = FDataDef->GetChildByName("description");
      if (FDescription)
       {
        TTagNode * FCheckFields = FDescription->GetChildByName("checkfields");
        if (FCheckFields)
         {
          UnicodeString ChRC;
          TTagNode * itNode = FCheckFields->GetChildByName("checkgroup");
          if (itNode)
           {
            while (itNode)
             {
              ChRC = FSimpleCheckUnique(FDescription, itNode->GetFirstChild(), AIsAppend, ANewCode, AImport);
              if (ChRC.Length())
               throw EUniqueInstanceError(ChRC);
              itNode = itNode->GetNext();
             }
           }
          else
           {
            ChRC = FSimpleCheckUnique(FDescription, FCheckFields->GetFirstChild(), AIsAppend, ANewCode, AImport);
            if (ChRC.Length())
             throw EUniqueInstanceError(ChRC);
           }
          RC = ChRC.Length();
         }
        else
         RC = true;
       }
      else
       throw EImportError("����������� �������� " + FDataDef->AV["uid"]);
     }
    catch (EUniqueInstanceError & E)
     {
      if (!AImport)
       throw;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDataTable::FSimpleCheckUnique(TTagNode * ADescription, TTagNode * ANode, bool AIsAppend,
  UnicodeString & ANewCode, bool AImport)
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString fUID, xWhere;
    xWhere = "";
    TTagNode * FFlNode;
    TTagNode * itNode = ANode;
    bool FIsNull, FtmpIsNull;
    bool FIsEmpty, FtmpIsEmpty;
    FIsNull  = false;
    FIsEmpty = false;
    while (itNode)
     {
      fUID = itNode->AV["ref"].UpperCase();
      if (xWhere.Length())
       xWhere += " and ";
      xWhere += Fields[fUID]->AsTypeCheck(FtmpIsNull, FtmpIsEmpty);
      if (!itNode->AV["canempty"].ToIntDef(0))
       {
        FIsNull |= FtmpIsNull;
        FIsEmpty |= FtmpIsEmpty;
       }
      itNode = itNode->GetNext();
     }
    xWhere = " Where " + xWhere;
    // dsLogMessage(xWhere, " >>>>>>>> ", 1);
    if (!FIsNull && !FIsEmpty)
     {
      TFDQuery * FQ = FDM->CreateTempQuery();
      try
       {
        FDM->quExec(FQ, false, "Select " + FPKName + " From " + FTableName + xWhere);
        if (FQ->RecordCount > 0)
         {
          if (AImport)
           {
            ANewCode = FQ->FieldByName(FPKName)->AsString;
            RC       = "exist";
           }
          else if (AIsAppend || (!AIsAppend && (FQ->FieldByName(FPKName)->AsString.UpperCase()
            != ANewCode.UpperCase())))
           {
            itNode = ADescription->GetChildByName("fields")->GetFirstChild();
            RC     = ADescription->AV["name"] + " \" ";
            while (itNode)
             {
              RC += Fields[itNode->AV["ref"].UpperCase()]->AsString + " ";
              itNode = itNode->GetNext();
             }
            RC += "\" ��� ����������!";
           }
         }
       }
      __finally
       {
        FDM->DeleteTempQuery(FQ);
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TDataTable::GetValue(UnicodeString AOldKey, UnicodeString AFieldUID)
 {
  dsLogBegin(__FUNC__, 6);
  UnicodeString RC = "";
  TDataField * tmpField = Fields[AFieldUID];
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    FDM->quExecParam(FQ, false, "Select " + tmpField->Name + " From " + FTableName + " Where " + FPKName + "=:p" +
      FPKName, "p" + FPKName, AOldKey);
    RC = FQ->FieldByName(tmpField->Name)->AsString;
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
   }
  dsLogEnd(__FUNC__, 6);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TDataTable::FCreateFields(TTagNode * uiTag, UnicodeString & UID)
 {
  // TTagNode *tmp = itTag;
  if (uiTag->CmpName("text,extedit,binary,date,digit,choice,choiceDB,choiceTree"))
   {
    // !!!++    dsLogBegin(__FUNC__, 6);
    if (!uiTag->CmpAV("uid", UID))
     {
      TDataField * tmpFl;
      uiTag->AV["fieldname"] = "R" + uiTag->AV["uid"];
      if (uiTag->AV["fl"].Length())
       uiTag->AV["fieldname"] = uiTag->AV["fl"];
      tmpFl                                       = new TDataField(this, uiTag);
      tmpFl->OnFieldEditError                     = evtEditError;
      FFields[uiTag->AV["fieldname"].UpperCase()] = tmpFl;
      FRUFields[uiTag->AV["name"].UpperCase()]    = tmpFl;
      FUIDFields[uiTag->AV["uid"].UpperCase()]    = tmpFl;
     }
    // !!!++    dsLogEnd(__FUNC__, 6);
   }
  return false;
 }
// ---------------------------------------------------------------------------
