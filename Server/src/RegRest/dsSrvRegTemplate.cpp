// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
// ---------------------------------------------------------------------------
#include "dsSrvRegTemplate.h"
#include "icsLog.h"
#include "srvsetting.h"
#ifndef _CONFSETTTING
#error _CONFSETTTING using are not defined
#endif
#ifdef REG_USE_KLADR
#include "KLADRParsers.h"
#endif
#ifdef REG_USE_ORG
#include "OrgParsers.h"
#endif
#include "JSONUtils.h"
// #include "RegSeparator.h"
// #include "Reg_DMF.h"
// #include "msgdef.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
/*
 <!ELEMENT root (fl|gr|separator)+>            //������
 <!ATTLIST root ref CDATA #IMPLIED>  // ref - uid �������������� ��� ��������
 // ����������� ������
 // ���� ref = NULL - ������ �����������
 // ��� ������� ����� �� �������� ������������

 <!ELEMENT gr (fl|gr|separator)+>              // ������
 <!ATTLIST gr ref CDATA #REQUIRED>   // ������ ��� �������� ����������� ����� ��
 // �������������� �� ������� ��������� �������
 // � uid-�� = ref, ������ ������� ����� ���� ������
 // ���� "choiceDB" - ������ ������ �� �������������

 <!ELEMENT fl EMPTY>                 // ����
 <!ATTLIST fl ref CDATA #REQUIRED    // �������� ���� �� �������� �������� ����������
 def CDATA #IMPLIED     // ��������� ����� ref - uid ��������
 show (0|1) "1"         // ���������� ��������� �������� � ???
 req (0|1) "0"          // �������������� ������� ��������
 "0" - �� �����������
 "1" - �����������
 "2" - �������������� ������������ ��������� ��������
 en  (0|1) "1">         // def - �������� �� ���������
 // en - ����������� ��� ��������������

 <!ELEMENT separator EMPTY>          // �����������

 AFields -  XML - �������� ����� �������
 AquFree -  ��������� �� FIBQuery ��� ������� � ��
 ATree - ��������� �� TTagNode - � ����������� ���������
 */
// ---------------------------------------------------------------------------
__fastcall EdsRegTemplateError::EdsRegTemplateError(UnicodeString Msg) : Sysutils::Exception(Msg)
 {
 };
// ---------------------------------------------------------------------------
__fastcall TdsRegTemplate::TdsRegTemplate(UnicodeString AFields, TTagNode * ATree, TJSONObject * AValues) : TObject()
 {
  FUseUpperFunc = true;
  FVals         = AValues;
  Root          = ATree;
  if (!AFields.Length())
   throw EdsRegTemplateError("�� ������ �������� �������.");
  FltFl        = new TTagNode(NULL);
  FltFl->AsXML = AFields;
  FUseParams   = true;
 }
// ---------------------------------------------------------------------------
__fastcall TdsRegTemplate::~TdsRegTemplate()
 {
  if (FltFl)
   delete FltFl;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplate::GetWhere(UnicodeString AAlias)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = FGetWhere(FltFl->GetFirstChild(), AAlias);
  dsLogMessage(RC, __FUNC__, 1);
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplate::GetWhereClRep(UnicodeString AAlias, TStringList * ASelList)
 {
  dsLogBegin(__FUNC__);
  FSelList = NULL;
  UnicodeString RC = "";
  try
   {
    FSelList = new TStringList;
    ASelList->Clear();
    RC             = FGetWhere(FltFl->GetFirstChild(), AAlias);
    ASelList->Text = FSelList->Text;
   }
  __finally
   {
    if (FSelList)
     {
      delete FSelList;
      FSelList = NULL;
     }
   }
  dsLogMessage(RC, __FUNC__, 1);
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplate::GetGrSelect(TTagNode * AGroup)
 {
  dsLogBegin(__FUNC__);
  UnicodeString xSel = "Select CODE From CLASS_";
  TTagNode * FRef = Root->GetTagByUID(AGroup->AV["ref"]);
  if (FRef)
   {
    if (FRef->CmpName("class"))
     xSel += FRef->AV["uid"].UpperCase() + " Where ";
    else
     xSel += FRef->AV["ref"].UpperCase() + " Where ";
   }
  else
   {
    xSel += "_ERROR_ Where ";
   }
  UnicodeString xWhere = FGetWhere(AGroup->GetFirstChild(), "");
  dsLogEnd(__FUNC__);
  if (xWhere.Length())
   return xSel + xWhere;
  else
   return "";
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplate::FGetWhere(TTagNode * AitNode, UnicodeString AAlias)
 {
  UnicodeString xWhere = "";
  UnicodeString Val1, Val2, itVal, FName, FFullName, iOper, FAlias;
  TTagNode * xTag;
  TTagNode * itNode = AitNode;
  while (itNode)
   {
    if (!itNode->CmpName("separator"))
     {
      xTag      = Root->GetTagByUID(itNode->AV["ref"]);
      iOper     = (!xWhere.Length()) ? " " : " and ";
      FName     = "R" + xTag->AV["uid"];
      FFullName = FName;
      FAlias    = "";
      if (AAlias.Length())
       {
        FAlias    = AAlias + ".";
        FFullName = FAlias + FName;
       }
      if (itNode->CmpName("fl"))
       {
        itVal = JSONToString(FVals, itNode->AV["ref"]);
        DeleteJSONPair(FVals, itNode->AV["ref"]);
        if (itVal.Length())
         {
          if (xTag->CmpName("binary,choice,choiceDB,class"))
           {
            if (FUseParams)
             {
              xWhere += iOper + FFullName + "=:p" + FName;
              FVals->AddPair(FName, itVal);
             }
            else
             xWhere += iOper + FFullName + "=" + itVal;
           }
          else if (xTag->CmpName("text"))
           {
            if (FUseUpperFunc)
             xWhere += iOper + " Upper(" + FFullName + ")";
            else
             {
              if (AAlias.Length())
               xWhere += iOper + " " + AAlias + ".U" + FName;
              else
               xWhere += iOper + " U" + FName;
             }
            if (FUseParams)
             {
              xWhere += "=:p" + FName;
              FVals->AddPair(FName, itVal);
             }
            else
             xWhere += "='" + itVal.UpperCase() + "'";
           }
          else if (xTag->CmpName("digit,date"))
           {
            if (wcschr(itVal.c_str(), ';'))
             {
              Val1 = GetLPartB(itVal, ';');
              Val2 = GetRPartB(itVal, ';');
              if (Val1.Length() && Val2.Length())
               {
                if (FUseParams)
                 {
                  xWhere += iOper + "(" + FFullName + ">=:pL" + FName + " and " + FFullName + "<=:pH" + FName + ")";
                  FVals->AddPair("L" + FName, Val1);
                  FVals->AddPair("H" + FName, Val2);
                 }
                else
                 xWhere += iOper + "(" + FFullName + ">=" + CorrectDig(Val1) + " and " + FFullName + "<=" +
                   CorrectDig(Val2) + ")";
               }
              else if (Val1.Length())
               {
                if (FUseParams)
                 {
                  xWhere += iOper + FFullName + ">=:pL" + FName;
                  FVals->AddPair("L" + FName, Val1);
                 }
                else
                 xWhere += iOper + FFullName + ">=" + CorrectDig(Val1);
               }
              else if (Val2.Length())
               {
                if (FUseParams)
                 {
                  xWhere += iOper + FFullName + "<=:pH" + FName;
                  FVals->AddPair("H" + FName, Val2);
                 }
                else
                 xWhere += iOper + FFullName + "<=" + CorrectDig(Val2);
               }
             }
            else
             {
              if (FUseParams)
               {
                xWhere += iOper + FFullName + "=:p" + FName;
                FVals->AddPair(FName, itVal);
               }
              else
               xWhere += iOper + FFullName + "=" + itVal;
             }
           }
          else if (xTag->CmpName("choiceTree"))
           {
            UnicodeString tmpTreeVal = GetLPartB(itVal, ':');
            if (tmpTreeVal.ToIntDef(0))
             {
              if (FUseParams)
               {
                xWhere += iOper + "(" + FFullName + ">=:pL" + FName + " and " + FFullName + "<=:pH" + FName + ")";
                FVals->AddPair("L" + FName, GetLPartB(GetRPartB(itVal, ':'), '#'));
                FVals->AddPair("H" + FName, GetRPartB(GetRPartB(itVal, ':'), '#'));
               }
              else
               xWhere += iOper + "(" + FFullName + ">=" + GetLPartB(GetRPartB(itVal, ':'), '#') + " and " + FFullName +
                 "<=" + GetRPartB(GetRPartB(itVal, ':'), '#') + ")";
             }
            else
             {
              if (FUseParams)
               {
                xWhere += iOper + FFullName + "=:p" + FName;
                FVals->AddPair(FName, GetLPartB(GetRPartB(itVal, ':'), '#'));
               }
              else
               xWhere += iOper + FFullName + "=" + GetLPartB(GetRPartB(itVal, ':'), '#');
             }
           }
          else if (xTag->CmpName("extedit"))
           {
            UnicodeString FExtWhere = "";
            if (xTag->CmpAV("uid", "019D, 00CC, 00E4, 0136, 015A, 0102"))
             { // �����
#ifdef REG_USE_KLADR
              adresCode FAddrCode = ParseAddrStr(itVal);
              UnicodeString FAddrParam = "";
              if (FAddrCode.RegID)
               FAddrParam += FAddrCode.RegCodeStr();
              if ((FAddrCode.Town1ID + FAddrCode.Town2ID + FAddrCode.Town3ID) || FAddrCode.StreetID)
               FAddrParam += FAddrCode.CityCodeStr();
              if (FAddrCode.StreetID)
               FAddrParam += FAddrCode.StreetCodeStr();
              FAddrParam += "%";
              if (FUseParams)
               {
                xWhere += iOper + FFullName + " like :p" + FName;
                FVals->AddPair(FName, FAddrParam);
                /*
                 UnicodeString temp, sep;
                 if (State)
                 { // ���������������
                 temp = House.Trim();
                 if (Vld.Trim().Length())
                 temp += "#V" + Vld.Trim();
                 if (Korp.Trim().Length())
                 temp += "#K" + Korp.Trim();
                 if (Build.Trim().Length())
                 temp += "#B" + Build.Trim();
                 if (ALeadSep)
                 {
                 if (temp.Length())
                 RC += "##" + temp;
                 else
                 RC += "## ";
                 }
                 else
                 {
                 if (temp.Length())
                 RC += temp;
                 }
                 }
                 */
                if (FAddrCode.House.Length())
                 {
                  xWhere += " and (" + FAlias + "R0100 = :pR0100 or R0100 like :pRP0100";
                  FVals->AddPair("R0100", FAddrCode.HouseCodeStr(false));
                  FVals->AddPair("RP0100", FAddrCode.HouseCodeStr(false) + "#%");
                 }
                if (FAddrCode.Flat.Length())
                 {
                  xWhere += " and " + FAlias + "R0033=:pR0033";
                  FVals->AddPair("R0033", FAddrCode.FlatCodeStr(false));
                 }
               }
              else
               {
                xWhere += iOper + FFullName + " like '" + FAddrParam + "'";
                if (FAddrCode.House.Length())
                 xWhere += " and (" + FAlias + "R0100 = '" + FAddrCode.HouseCodeStr(false) + "' or " + FAlias +
                   "R0100 like '" + FAddrCode.HouseCodeStr(false) + "#%')";
                if (FAddrCode.Flat.Length())
                 xWhere += " and " + FAlias + "R0033='" + FAddrCode.FlatCodeStr(false) + "'";
               }
#else
              xWhere += iOper + FFullName + " like '" + itVal + "'";
#endif
             }
            else if (xTag->CmpAV("uid", "0025"))
             { // �����������
              // dsLogMessage(itVal);
#ifdef REG_USE_ORG
              orgCode FOrgCode; // = ParseOrgCodeStr(itVal);
              if (itVal.ToIntDef(0))
               {
                FOrgCode.OrgID  = itVal.ToIntDef(0);
                FOrgCode.L1Val  = JSONToString(FVals, "0026");
                FOrgCode.L2Val  = JSONToString(FVals, "0027");
                FOrgCode.L3Val  = JSONToString(FVals, "0028");
                FOrgCode.Format = UIDStr(JSONToString(FVals, "0112").ToIntDef(0));
               }
              else
               FOrgCode = ParseOrgCodeStr(itVal);
              if (FOrgCode.Type != TOrgTypeValue::NF)
               {
                if (FUseParams)
                 {
                  if (FOrgCode.OrgID)
                   {
                    FExtWhere += " and " + FAlias + "R0025=:pR0025";
                    FVals->AddPair("R0025", FOrgCode.OrgID);
                   }
                  if (FOrgCode.L1Val.Length())
                   {
                    FExtWhere += " and Upper(" + FAlias + "R0026)=:pR0026";
                    FVals->AddPair("R0026", FOrgCode.L1Val.UpperCase());
                   }
                  if (FOrgCode.L2Val.Length())
                   {
                    FExtWhere += " and Upper(" + FAlias + "R0027)=:pR0027";
                    FVals->AddPair("R0027", FOrgCode.L2Val.UpperCase());
                   }
                  if (FOrgCode.L3Val.Length())
                   {
                    FExtWhere += " and Upper(" + FAlias + "R0028)=:pR0028";
                    FVals->AddPair("R0028", FOrgCode.L3Val.UpperCase());
                   }
//                  if (FOrgCode.State.Length())
//                   {
//                    FExtWhere += " and Upper(" + FAlias + "R0111)=:pR0111";
//                    FVals->AddPair("R0111", FOrgCode.State.UpperCase());
//                   }
//                  if (FOrgCode.Period)
//                   {
//                    FExtWhere += " and " + FAlias + "R0112=:pR0112";
//                    FVals->AddPair("R0112", IntToStr(FOrgCode.Period));
//                   }
                  if (FExtWhere.Length())
                   xWhere += iOper + "(" + FFullName + "=:p" + FName + FExtWhere + ")";
                  else
                   xWhere += iOper + FFullName + "=:p" + FName;
                  FVals->AddPair(FName, FOrgCode.OrgID);
                 }
                else
                 {
                  if (FOrgCode.OrgID)
                   FExtWhere += " and " + FAlias + "R0025=" + IntToStr(FOrgCode.OrgID);
                  if (FOrgCode.L1Val.Length())
                   FExtWhere += " and Upper(" + FAlias + "R0026)='" + FOrgCode.L1Val.UpperCase() + "'";
                  if (FOrgCode.L2Val.Length())
                   FExtWhere += " and Upper(" + FAlias + "R0027)='" + FOrgCode.L2Val.UpperCase() + "'";
                  if (FOrgCode.L3Val.Length())
                   FExtWhere += " and Upper(" + FAlias + "R0028)='" + FOrgCode.L3Val.UpperCase() + "'";
//                  if (FOrgCode.State.Length())
//                   FExtWhere += " and Upper(" + FAlias + "R0111)='" + FOrgCode.State.UpperCase() + "'";
//                  if (FOrgCode.Period)
//                   FExtWhere += " and " + FAlias + "R0112=" + IntToStr(FOrgCode.Period);
                  if (FExtWhere.Length())
                   xWhere += iOper + "(" + FFullName + "=" + IntToStr(FOrgCode.OrgID) + FExtWhere + ")";
                  else
                   xWhere += iOper + FFullName + "=" + IntToStr(FOrgCode.OrgID);
                 }
               }
#else
              xWhere += iOper + FFullName + " like '" + itVal + "'";
#endif
             }
            else if (xTag->CmpAV("uid", "1082,3082,3304,32FF"))
             { // �����������
              if (itVal.Pos("##"))
               {
                if (FUseParams)
                 {
                  xWhere += iOper + FFullName + " = :p" + FName;
                  FVals->AddPair(FName, itVal);
                 }
                else
                 {
                  xWhere += iOper + FFullName + " = '" + itVal + "'";
                 }
               }
              else
               {
                if (FUseParams)
                 {
                  xWhere += iOper + " Upper(" + FFullName + ") like :p" + FName;
                  FVals->AddPair(FName, itVal.UpperCase() + "%");
                 }
                else
                 {
                  xWhere += iOper + " Upper(" + FFullName + ") like '" + itVal.UpperCase() + "%'";
                 }
               }
             }
           }
         }
        dsLogMessage(itNode->AV["ref"] + " = " + FVals->ToString(), __FUNC__, 1);
       }
      else
       {
        UnicodeString GrSel = GetGrSelect(itNode);
        if (GrSel.Length())
         xWhere += iOper + FFullName + " in (" + GrSel + ")";
       }
     }
    dsLogMessage("before next " + itNode->AV["ref"], __FUNC__, 1);
    itNode = itNode->GetNext();
   }
  dsLogMessage("ret = " + xWhere, __FUNC__, 1);
  return xWhere;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplate::CorrectDig(UnicodeString ASrc)
 {
  UnicodeString RC = ASrc;
  try
   {
    TReplaceFlags rFlag;
    rFlag << rfReplaceAll;
    RC = StringReplace(RC, ",", ".", rFlag);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
