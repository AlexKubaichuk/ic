/*
  ����        - ICSDocSpec.cpp
  ������      - ��������������� (ICS)
  ��������    - �������� �������������.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 15.09.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>

#pragma hdrstop

//#include "icsDocConstDef.h"
#include "dsDocSpec.h"
#include "dsDocSrvConstDef.h"

#include <functional>
#include <iterator>
//#include "DKMBUtils.h"
//#include "DKRusMB.h"
//#include "DKAxeUtils.h"

//#include "FilterEdit.h"
//#include "ICSDocFilter.h"
//#include "SetRef.h"
//#include "AddList.h"
//#include "AddTable.h"
//#include "RulesExprEdit.h"
//#include "Count.h"
//#include "ListSortOrder.h"
//#include "DocElSizeEdit.h"

#pragma package(smart_init)

//---------------------------------------------------------------------------

namespace dsDocSpec
{

//###########################################################################
//##                                                                       ##
//##                                 Globals                               ##
//##                                                                       ##
//###########################################################################

//#define ONE_AV     "1"
//#define ZERO_AV    "0"
//#define ONE_VAV    cFMT(&_icsDocSpecONE_VAVCaption)
//#define ZERO_VAV   cFMT(&_icsDocSpecZERO_VAVCaption)
//#define TRUE_AV    "yes"
//#define FALSE_AV   "no"
//#define TRUE_VAV   cFMT(&_icsDocSpecTRUE_VAVCaption)
//#define FALSE_VAV  cFMT(&_icsDocSpecFALSE_VAVCaption)
//#define TAB_PRTY_ROW_AV   "row"
//#define TAB_PRTY_COL_AV   "col"
//#define TAB_PRTY_ROW_VAV  cFMT(&_icsDocSpecTabPrtyColCaption)
//#define TAB_PRTY_COL_VAV  cFMT(&_icsDocSpecTabPrtyRowCaption)
//#define EMPTY_LCOL        cFMT(&_icsDocSpecEmptyColCaption)

//#define GET_BIN_AV( BinVAV )   ( ( (BinVAV)  == ONE_VAV  ) ? ONE_AV    : ZERO_AV   )
//#define GET_BIN_VAV( BinAV )   ( ( (BinAV)   == ONE_AV   ) ? ONE_VAV   : ZERO_VAV  )
//#define GET_BOOL_AV( BoolVAV ) ( ( (BoolVAV) == TRUE_VAV ) ? TRUE_AV   : FALSE_AV  )
//#define GET_BOOL_VAV( BoolAV ) ( ( (BoolAV)  == TRUE_AV  ) ? TRUE_VAV  : FALSE_VAV )
//#define GET_BOOL_AV( BoolVAV ) ( ( (BoolVAV) == TRUE_VAV ) ? TRUE_AV   : FALSE_AV  )
//#define GET_TAB_PRTY_AV( TabPrtyVAV ) ( ( (TabPrtyVAV)  == TAB_PRTY_ROW_VAV  ) ? TAB_PRTY_ROW_AV   : TAB_PRTY_COL_AV  )
//#define GET_TAB_PRTY_VAV( TabPrtyAV ) ( ( (TabPrtyAV)   == TAB_PRTY_ROW_AV   ) ? TAB_PRTY_ROW_VAV  : TAB_PRTY_COL_VAV )

//---------------------------------------------------------------------------
//#pragma resource "ICSDoc.res"
//---------------------------------------------------------------------------

//const UnicodeString csCountViewText           = cFMT(&_icsDocSpecCountViewText);
//const UnicodeString csEmptyEllipsiseEditor    = cFMT(&_icsDocSpecEmptyEllipsiseEditor);
//const UnicodeString csNotEmptyEllipsiseEditor = cFMT(&_icsDocSpecNotEmptyEllipsiseEditor);
const UnicodeString csEmptyInsText            = dsDocSpecEmptyInsCaption;
//---------------------------------------------------------------------------

//�������������� ������ ��� ������ � ��������� TUIDsSet
struct SelectPareTagNodeRefAttrVal : public unary_function<pair<UnicodeString,TTagNode*>, UnicodeString>
{
  UnicodeString operator()(const pair<UnicodeString,TTagNode*> x) const;
};

UnicodeString SelectPareTagNodeRefAttrVal::operator()(const pair<UnicodeString,TTagNode*> x) const
{
  return x.second->AV["ref"];
}

//---------------------------------------------------------------------------

//�������������� ������ ��� ������ � ������ TTagNode'��
struct SelectTagNodeUID : public unary_function<TTagNode*, UnicodeString>
{
  const UnicodeString operator()(const TTagNode* x) const;
};

const UnicodeString SelectTagNodeUID::operator()(const TTagNode* x) const
{
  return const_cast<TTagNode*>(x)->AV["uid"];
}

//---------------------------------------------------------------------------

enum TImageListInd { iiParameters = 0, iiPreView, iiCut, iiCopy, iiPaste, iiDelete, iiInscription, iiInsText, iiList, iiLCol, iiTable, iiTCol, iiTRow, iiMoveUp, iiMoveDown };

//---------------------------------------------------------------------------

struct TTagNodeListPair
{
  TTagNodeList *first;
  TTagNodeList *second;
};

//---------------------------------------------------------------------------
bool __fastcall IsListColSingle1(const TTagNode *ndListEl)
{
  return IsListColSingle1Main(ndListEl) || IsListColSingle1Count(ndListEl);
}
//---------------------------------------------------------------------------
bool __fastcall IsListColSingle1Main(const TTagNode *ndListEl)
{
  bool bResult = false;
  if (const_cast<TTagNode*>(ndListEl)->CmpName("listcol"))
  {
    TTagNode *ndListColType = const_cast<TTagNode*>(ndListEl)->GetFirstChild();
    bResult = (
      ndListColType &&
      ndListColType->CmpName("is_attr")
    );
  }
  return bResult;
}

//---------------------------------------------------------------------------
bool __fastcall IsListColSingle1Count(const TTagNode *ndListEl)
{
  bool bResult = false;
  if ( const_cast<TTagNode*>(ndListEl)->CmpName( "listcol" ) )
  {
    TTagNode *ndListColType = const_cast<TTagNode*>(ndListEl)->GetFirstChild();
    bResult = (
      ndListColType &&
      ndListColType->CmpName("link_is") &&
      ndListColType->GetChildByName("count")
    );
  }
  return bResult;
}

//---------------------------------------------------------------------------
bool __fastcall IsListColSingle2( const TTagNode *ndListEl )
{
  return const_cast<TTagNode*>(ndListEl)->CmpName( "is_attr" ) && const_cast<TTagNode*>(ndListEl)->GetParent()->CmpName( "link_is" );
}
//---------------------------------------------------------------------------
bool __fastcall IsListColGroup( const TTagNode *ndListEl )
{
  return const_cast<TTagNode*>(ndListEl)->CmpName( "listcolgroup" ) || IsListColLinkISGroup( ndListEl );
}
//---------------------------------------------------------------------------
bool __fastcall IsListColLinkISGroup( const TTagNode *ndListEl )
{
  bool bResult = false;
  if ( const_cast<TTagNode*>(ndListEl)->CmpName( "listcol" ) )
  {
    TTagNode *ndListColType = const_cast<TTagNode*>(ndListEl)->GetFirstChild();
    if ( ndListColType->CmpName( "link_is" ) && ndListColType->GetChildByName( "is_attr" )  )
      bResult = true;
  }
  return bResult;
}
//---------------------------------------------------------------------------
bool __fastcall IsListCol( const TTagNode *ndListEl )
{
  return IsListColGroup( ndListEl ) || IsListColSingle( ndListEl );
}
//---------------------------------------------------------------------------
bool __fastcall IsListColSingle(const TTagNode *ndListEl )
{
  bool bResult;
  if      ( IsListColSingle1( ndListEl ) )
  {
    bResult = !GetListColGroupUnion( const_cast<TTagNode*>(ndListEl)->GetParent() );
  }
  else if ( IsListColSingle2( ndListEl ) )
  {
    bResult = !GetListColGroupUnion( const_cast<TTagNode*>(ndListEl)->GetParent( "listcol" ) );
  }
  else
  {
    bResult = GetListColGroupUnion( ndListEl );
  }
  return bResult;
}

//---------------------------------------------------------------------------
bool __fastcall GetListColGroupUnion( const TTagNode *ndListEl )
{
  bool RC = false;
  if ( IsListColGroup( ndListEl ) )
    if ( const_cast<TTagNode*>(ndListEl)->CmpName( "listcolgroup" ) )
      RC = const_cast<TTagNode*>(ndListEl)->CmpAV("union", "1");
    else //listcol
      RC = const_cast<TTagNode*>(ndListEl)->GetChildByName( "link_is" )->CmpAV("union", "1");
  return RC;
}

//---------------------------------------------------------------------------
UnicodeString __fastcall GetSizeAttrVal( TTagNode* TagNode, UnicodeString  AAttrName )
{
  UnicodeString SizeAttrVal = "";
  if      ( TagNode->CmpName( "list,table,tcol" ) || IsListColSingle1(TagNode) )
    SizeAttrVal = TagNode->AV[AAttrName];
  else if ( IsListColSingle2(TagNode) )
  {
    int nLColNo = GetListColSingle2Index( TagNode );

    TStringList *SizeValues = NULL;
    try
    {
      SizeValues = new TStringList;
      SizeValues->Text = StringReplace( TagNode->GetParent()->GetParent()->AV[AAttrName],
                                        ";",
                                        "\n",
                                        TReplaceFlags() << rfReplaceAll
      );
      if ( ( nLColNo >= 0 ) && ( nLColNo < SizeValues->Count ) )
        SizeAttrVal = SizeValues->Strings[nLColNo];
    }
    __finally
    {
      if ( SizeValues ) delete SizeValues;
    }
  }
  return SizeAttrVal;
}

//---------------------------------------------------------------------------
int __fastcall GetListColSingle2Index( const TTagNode* TagNode )
{
  int nIndex = -1;
  if ( IsListColSingle2(TagNode) )
  {
    TTagNode *ndISAttr = const_cast<TTagNode*>(TagNode);
    while( ndISAttr && ( ndISAttr->CmpName("is_attr") ) )
    {
      nIndex++;
      ndISAttr = ndISAttr->GetPrev();
    }
  }
  return nIndex;
}
//---------------------------------------------------------------------------
bool __fastcall IsListFirstCol( const TTagNode *ndListCol )
{
  bool bIsListFirstCol;
  if      ( IsListColSingle1( ndListCol ) || IsListColGroup( ndListCol ) )
    bIsListFirstCol = ( !const_cast<TTagNode*>(ndListCol)->GetPrev() );
  else if ( IsListColSingle2( ndListCol ) )
    bIsListFirstCol = ( ( !const_cast<TTagNode*>(ndListCol)->GetPrev() || !IsListColSingle2( const_cast<TTagNode*>(ndListCol)->GetPrev() ) ) );
  else
    bIsListFirstCol = false;
  return bIsListFirstCol;
}
//---------------------------------------------------------------------------
void __fastcall GetListSingleColsList( TTagNode *ANode, TTagNodeList *AListSingleCols )
{
  if ( IsListColSingle( ANode ) )
    AListSingleCols->push_back( ANode );
  TTagNode *ndChild = ANode->GetFirstChild();
  while( ndChild )
  {
    GetListSingleColsList( ndChild, AListSingleCols );
    ndChild = ndChild->GetNext();
  }

}
//---------------------------------------------------------------------------
bool __fastcall IsTableColSingle( const TTagNode *ndTableCol )
{
  return IsTableElSingle( ndTableCol, "tcol" );
}
//---------------------------------------------------------------------------
bool __fastcall IsTableRowSingle( const TTagNode *ndTableRow )
{
  return IsTableElSingle( ndTableRow, "trow" );
}
//---------------------------------------------------------------------------
bool __fastcall IsTableElSingle( const TTagNode *ndTableEl, UnicodeString AElName )
{
  return ( const_cast<TTagNode*>(ndTableEl)->CmpName( AElName ) && !const_cast<TTagNode*>(ndTableEl)->GetChildByName( AElName ) );
}
//---------------------------------------------------------------------------
__int64 __fastcall GetTagNodesMaxLevel( const TTagNode *ANode, UnicodeString AName )
{
  __int64 nLevel = 0;
  if ( const_cast<TTagNode*>(ANode)->CmpName( AName ) && ( ANode->Level > nLevel ) )
    nLevel = ANode->Level;
  TTagNode *ndChild = const_cast<TTagNode*>(ANode)->GetFirstChild();
  while( ndChild )
  {
    __int64 nChildNodesMaxLevel = GetTagNodesMaxLevel( ndChild, AName );
    if ( nChildNodesMaxLevel > nLevel )
      nLevel = nChildNodesMaxLevel;
    ndChild = ndChild->GetNext();
  }
  return nLevel;
}

//---------------------------------------------------------------------------
bool __fastcall IsTableFirstEl( const TTagNode *ndTableEl, UnicodeString AElName )
{
  return ( const_cast<TTagNode*>(ndTableEl)->CmpName( AElName ) && ( !const_cast<TTagNode*>(ndTableEl)->GetPrev() || !const_cast<TTagNode*>(ndTableEl)->GetPrev()->CmpName( AElName ) ) );
}
//---------------------------------------------------------------------------
bool __fastcall IsTableFirstCol( const TTagNode *ndTableCol )
{
  return IsTableFirstEl( ndTableCol, "tcol" );
}
//---------------------------------------------------------------------------
bool __fastcall IsTableFirstRow( const TTagNode *ndTableRow )
{
  return IsTableFirstEl( ndTableRow, "trow" );
}
//---------------------------------------------------------------------------
void __fastcall GetTableSingleColsList( TTagNode *ANode, TTagNodeList *ATableSingleCols )
{
  if ( IsTableColSingle( ANode ) )
    ATableSingleCols->push_back( ANode );
  TTagNode *ndChild = ANode->GetFirstChild();
  while( ndChild )
  {
    GetTableSingleColsList( ndChild, ATableSingleCols );
    ndChild = ndChild->GetNext();
  }
}

//---------------------------------------------------------------------------
void __fastcall GetTableSingleRowsList( TTagNode *ANode, TTagNodeList *ATableSingleRows )
{
  if ( IsTableRowSingle( ANode ) )
    ATableSingleRows->push_back( ANode );
  TTagNode *ndChild = ANode->GetFirstChild();
  while( ndChild )
  {
    GetTableSingleRowsList( ndChild, ATableSingleRows );
    ndChild = ndChild->GetNext();
  }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetInsTagViewText( TTagNode* ndInsText, TAxeXMLContainer *AXMLContainer, PINS_VIEW_TEXT pVT )
{
  UnicodeString Result = "";
  if (!ndInsText)
    throw System::Sysutils::Exception(UnicodeString(__FUNC__)+ " ndInsText is NULL");
  if (!AXMLContainer)
    throw System::Sysutils::Exception(UnicodeString(__FUNC__)+" AXMLContainer is NULL");

  if ( pVT )
  {
    pVT->Value = "";
    pVT->Ref   = "";
    pVT->Count = "";
  }

  //����������� �����
  if ( ndInsText->AV["value"] != "" )
  {
    Result = ndInsText->AV["value"];
    if ( pVT ) pVT->Value = ndInsText->AV["value"];
  }
  //������
  if      ( ndInsText->AV["ref"] != "" )
  {
    if ( Result != "" )
      Result += "   +   ";
    TTagNode* ndRef = ( _GUI( ndInsText->AV["ref"] ) == "" )
                      ? ndInsText->GetRefer( "ref" )
                      : AXMLContainer->GetRefer( ndInsText->AV["ref"] );
    //������ �� ������� �������� TechIS ��� DocIS
    if ( ndRef )
    {
      UnicodeString tmp = "";
      if      ( ndRef->CmpName( "ATTR" ) && ndRef->GetParent()->GetParent()->CmpName( "TechIS,DocIS" ) )
        tmp = ( ndRef->GetParent()->GetParent()->AV["name"] +
                " (" +
                ndRef->AV["name"] +
                ")"
        );
      //������ �� ������� ������� ( ����������� condrules ������������� ) IERef
      else if ( ndRef->CmpName( "IERef" ) )
        tmp = AXMLContainer->GetRefer( ndRef->AV["ref"] )->AV["name"];
      if ( tmp != "" )
      {
        Result += tmp;
        if ( pVT ) pVT->Ref = tmp;
      }
    }
  }
  //������� ����������
  else
  {
    TTagNode* ndCount = ndInsText->GetChildByName( "count" );
    if ( ndCount )
    {
      if ( Result != "" )
        Result += "   +   ";
      UnicodeString tmp = AXMLContainer->GetRefer( ndCount->AV["ref"] )->AV["name"];
      Result += FMT1(dsDocSpecGetCountCaption, tmp );
      if ( pVT ) pVT->Count = tmp;
    }
  }
  //�����������
  if ( Result == "" )
    Result = csEmptyInsText;
  return Result;
}
//---------------------------------------------------------------------------

}// end of namespace dsDocSpec


