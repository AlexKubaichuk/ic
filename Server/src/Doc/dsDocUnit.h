﻿//---------------------------------------------------------------------------

#ifndef dsDocUnitH
#define dsDocUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
//---------------------------------------------------------------------------
#include <FireDAC.Phys.FB.hpp>
#include <Data.DB.hpp>
//---------------------------------------------------------------------------
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
//---------------------------------------------------------------------------
#include "dsSrvRegUnit.h"
#include "dsDocDMUnit.h"
#include "dsKLADRUnit.h"
#include "dsOrgUnit.h"
//---------------------------------------------------------------------------
enum TDocModuleType {dmtNone, dmtDoc, dmtFlt, dmtSOrg, dmtSpec};
class DECLSPEC_DRTTI TdsDocClass : public TDataModule
{
__published:	// IDE-managed Components
 TFDConnection *DocConnection;
 TFDConnection *IcConnection;
private:	// User declarations
   TdsSrvRegistry  *FRegComp;
   TTagNode        *FDefXML;
   TdsDocDM        *FDM;
  UnicodeString FTextData;

  TdsKLADRClass   *FKLADRComp;
  TdsOrgClass     *FOrgComp;

   TDocModuleType FGetModuleType(System::UnicodeString AId);
   bool FGetNewTabCode(UnicodeString ATab, UnicodeString &ARet);
   UnicodeString FDataConv(TdsRegTextDataConvType AConvType, UnicodeString ATabId, UnicodeString AFlId, UnicodeString AData);

  UnicodeString FGetAddrStr(UnicodeString ACode, unsigned int AParams = 0x0FF);
  UnicodeString FGetOrgStr(UnicodeString ACode, unsigned int AParams = 0);
  UnicodeString FGetOrgFieldStr(__int64 ACode, UnicodeString AFieldName);
  UnicodeString __fastcall FGetExtAVal(TTagNode *ADefNode, TStringList *AFields);
  UnicodeString FGetFilterData(System::UnicodeString AGUI, unsigned int AParams = 0);

public:		// User declarations
  __fastcall TdsDocClass(TComponent* Owner, TdsMKB *AMKB, TAxeXMLContainer *AXMLList, TAppOptions *AOpt);
  __fastcall ~TdsDocClass();
  void Disconnect();

  UnicodeString GetDefXML();
  UnicodeString GetClassXML(UnicodeString ARef);
  UnicodeString GetClassZIPXML(UnicodeString ARef);
  __int64       GetCount(UnicodeString AId, UnicodeString AFilterParam);
  TJSONObject*  GetIdList(UnicodeString AId, int AMax, UnicodeString AFilterParam);
  TJSONObject*  GetValById(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  GetValById10(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  Find(UnicodeString AId, UnicodeString AFindData);
  UnicodeString ImportData(UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  UnicodeString InsertData(UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  UnicodeString EditData(UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  UnicodeString DeleteData(UnicodeString AId, UnicodeString ARecId);
  UnicodeString SetTextData(UnicodeString AId, UnicodeString AFlId, UnicodeString ARecId, UnicodeString AVal);
  UnicodeString GetTextData(UnicodeString AId, UnicodeString AFlId, UnicodeString ARecId, UnicodeString &AMsg);

  System::UnicodeString GetSpecById(UnicodeString AId, int &ASpecType);
  System::UnicodeString GetDocById(UnicodeString AId);
  System::UnicodeString CreateDocPreview(UnicodeString AId, int &Al, int &At, int &Ar, int &Ab, double &Aps, int &Ao);
  void                  SaveDoc(UnicodeString AId);
  System::UnicodeString CheckByPeriod(bool FCreateByType, UnicodeString ACode, UnicodeString ASpecGUI);
  System::UnicodeString DocListClearByDate(TDate ADate);
  int                   GetDocCountBySpec(UnicodeString ASpecOWNER, UnicodeString ASpecGUI, UnicodeString &ARCMsg);

  void                  SetMainLPU(__int64 ALPUCode);
  UnicodeString         GetMainLPUParam();
  UnicodeString         MainOwnerCode();
  bool                  IsMainOwnerCode(UnicodeString AOwnerCode);

  System::UnicodeString CopySpec(UnicodeString AName, UnicodeString &ANewGUI);
  int                   CheckSpecExists(UnicodeString AName, UnicodeString &AGUI, bool AIsEdit);
  bool                  CheckSpecExistsByCode(UnicodeString AGUI);

  bool                  CheckDocCreateType(UnicodeString ASpecGUI, int &ACreateType, int ADefCreateType, int &AGetCreateType, int &AGetPeriod);
  System::UnicodeString DocCreateCheckExist(UnicodeString ASpecGUI, UnicodeString AAuthor, int APerType, TDate APeriodDateFr, TDate APeriodDateTo);
  UnicodeString         CheckFilter(UnicodeString AFilterGUI);
  System::UnicodeString DocCreateCheckFilter(UnicodeString ASpecGUI, UnicodeString AFilterCode, UnicodeString &AFilterName, UnicodeString &AFilterGUI);
  System::UnicodeString DocCreate(UnicodeString ASpecGUI, UnicodeString AAuthor, UnicodeString AFilter, bool AConcr, int ACreateType, int APerType, TDate APeriodDateFr, TDate APeriodDateTo);
  System::UnicodeString StartDocCreate(UnicodeString ASpecGUI, UnicodeString AAuthor, UnicodeString AFilter, UnicodeString AFilterNameGUI, bool AConcr, int ACreateType, int APerType, TDate APeriodDateFr, TDate APeriodDateTo, bool ANotSave);
  System::UnicodeString DocCreateProgress(System::UnicodeString &ARecId);
  System::UnicodeString LoadDocPreviewProgress(System::UnicodeString &AMsg);
  TJSONObject* GetQList(UnicodeString AId);
  void         SetQList(UnicodeString AId, TJSONObject *AList);
  UnicodeString GetFilterByGUI(System::UnicodeString AGUI);
  bool          CanHandEdit(UnicodeString ASpecGUI);
};
//---------------------------------------------------------------------------
#endif
