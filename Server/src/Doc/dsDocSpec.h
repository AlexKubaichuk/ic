/*
  ����        - ICSDocSpec.h
  ������      - ��������������� (ICS)
  ��������    - �������� �������������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 15.09.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

    �������������� �������
*/
//---------------------------------------------------------------------------
/*
  ���� �� �������:
        1. ��� ��������� ������������ ��� �������� ������� �������������
           � ������� ��������� ������ ������������ ��������� �� ��������� ����������
           ��������� ������� ������������� ������������ ��� ����� �������� �� ������
           ������ ��� �� ������ ����� �� ��� ������� �������
        2. ���� ������� ���������� �������� ���������� ��������, ��� ��� ����������
           �������� ����������� ������ �� �����������
*/
//---------------------------------------------------------------------------

#ifndef dsDocSpecH
#define dsDocSpecH

//---------------------------------------------------------------------------

#include <System.SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <ActnList.hpp>
#include <Menus.hpp>
#include <set>
#include <list>

#include "fmtdef.h"
//#include "TB2Item.hpp"
#include "XMLContainer.h"

//#include "ICSDocSpecUtils.h"
//#include "ICSDocBaseEdit.h"
//#include "ICSDocViewer.h"
//#include "ParametersEdit.h"
//#include "FilterEditEx.h"


typedef list<UnicodeString> TUnicodeStringList;
typedef list<TTagNode*> TTagNodeList;
typedef list<TTagNodeList> TTagNodeList2;
typedef list<TTreeNode*> TTreeNodeList;

namespace dsDocSpec
{
using namespace std;

typedef struct tagINS_VIEW_TEXT
{
  UnicodeString Value;
  UnicodeString Ref;
  UnicodeString Count;
} INS_VIEW_TEXT, *PINS_VIEW_TEXT;

//��������� �������� �������� ���������

/*enum TCheckDocSpecResult {
  frOk = 0,     //Ok
  frEmpty = 1,  //� xml-���� "doccontent" ��� �������� ���������
  frNone = 2    //�������� ��������� �� �������
};   */

//---------------------------------------------------------------------------

// ��� �������� �������������
//#define COUNT_VIEW_TEXT         csCountViewText.c_str()
//#define EMPTY_ELLIPSISEDITOR    csEmptyEllipsiseEditor.c_str()
//#define NOTEMPTY_ELLIPSISEDITOR csNotEmptyEllipsiseEditor.c_str()
//#define EMPTY_INSTEXT           csEmptyInsText.c_str()

//---------------------------------------------------------------------------

//extern const UnicodeString csCountViewText;
//extern const UnicodeString csEmptyEllipsiseEditor;
//extern const UnicodeString csNotEmptyEllipsiseEditor;
extern const UnicodeString csEmptyInsText;

PACKAGE bool __fastcall IsListColSingle1Main(const TTagNode *ndListEl);
PACKAGE bool __fastcall IsListColSingle1Count(const TTagNode *ndListEl);
extern PACKAGE bool __fastcall IsListColSingle1(const TTagNode *ndListEl);

extern PACKAGE bool __fastcall IsListColSingle2( const TTagNode *ndListEl );

extern PACKAGE bool __fastcall IsListColLinkISGroup( const TTagNode *ndListEl );
extern PACKAGE bool __fastcall IsListColGroup( const TTagNode *ndListEl );

extern PACKAGE bool __fastcall IsListColSingle(const TTagNode *ndListEl );
extern PACKAGE bool __fastcall GetListColGroupUnion( const TTagNode *ndListEl );
extern PACKAGE bool __fastcall IsListCol( const TTagNode *ndListEl );

PACKAGE int __fastcall GetListColSingle2Index( const TTagNode* TagNode );
extern PACKAGE UnicodeString __fastcall GetSizeAttrVal( TTagNode* TagNode, UnicodeString  AAttrName );

extern PACKAGE bool __fastcall IsListFirstCol( const TTagNode *ndListCol );

extern PACKAGE void __fastcall GetListSingleColsList( TTagNode *ANode, TTagNodeList *AListSingleCols );

PACKAGE bool __fastcall IsTableElSingle( const TTagNode *ndTableEl, UnicodeString AElName );
extern PACKAGE bool __fastcall IsTableColSingle( const TTagNode *ndTableCol );
extern PACKAGE bool __fastcall IsTableRowSingle( const TTagNode *ndTableRow );

extern PACKAGE __int64 __fastcall GetTagNodesMaxLevel( const TTagNode *ANode, UnicodeString AName );

PACKAGE bool __fastcall IsTableFirstEl( const TTagNode *ndTableEl, UnicodeString AElName );
extern PACKAGE bool __fastcall IsTableFirstCol( const TTagNode *ndTableCol );
extern PACKAGE bool __fastcall IsTableFirstRow( const TTagNode *ndTableRow );

extern PACKAGE void __fastcall GetTableSingleColsList( TTagNode *ANode, TTagNodeList *ATableSingleCols );
extern PACKAGE void __fastcall GetTableSingleRowsList( TTagNode *ANode, TTagNodeList *ATableSingleRows );

extern PACKAGE UnicodeString __fastcall GetInsTagViewText( TTagNode* ndInsText, TAxeXMLContainer *AXMLContainer, PINS_VIEW_TEXT = NULL );

//extern PACKAGE bool __fastcall InsertListColSingle2SizeAttr( TTagNode* TagNode, UnicodeString  AAttrName, UnicodeString  AAttrVal );
//extern PACKAGE bool __fastcall DeleteListColSingle2SizeAttr( TTagNode* TagNode, UnicodeString  AAttrName );
//extern PACKAGE bool __fastcall SwapListColSingle2SizeAttrVals( TTagNode* TagNode1, TTagNode* TagNode2, UnicodeString  AAttrName );

} // end of namespace dsDocSpec
using namespace dsDocSpec;

#endif
