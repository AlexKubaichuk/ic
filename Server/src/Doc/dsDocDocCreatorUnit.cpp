//---------------------------------------------------------------------------
#include <vcl.h>
#include <SysUtils.hpp>
#include <stdio.h>
#pragma hdrstop

#include "dsDocDocCreatorUnit.h"
#include "dsDocSrvConstDef.h"
#include "ExtUtils.h"
//#include "DocPeriod.h"
//#include "FilterEditEx.h"

//#include "DKUtils.h"
//#include "msgdef.h"
//#include "icsDocExConstDef.h"
//#include "ICSDocExDataDef.h"
#include "ICSDATEUTIL.hpp"

//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------
//###########################################################################
//##                                                                       ##
//##                     TExecutor::TBackgroundDocCreator                  ##
//##                                                                       ##
//###########################################################################

class TdsDocCreator::TBackgroundDocCreator : public TThread
 {
 private:
  TdsDocCreator * FDC;

 protected:
  virtual void __fastcall Execute();

 public:
  __fastcall TBackgroundDocCreator(bool CreateSuspended, TdsDocCreator * ADC);

  void __fastcall ThreadSafeCall(TdcSyncMethod Method);
 };

//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                     TExecutor::TBackgroundDocCreator                  ##
//##                                                                       ##
//###########################################################################

__fastcall TdsDocCreator::TBackgroundDocCreator::TBackgroundDocCreator(bool CreateSuspended, TdsDocCreator * ADC) :
    TThread(CreateSuspended),
    FDC(ADC)
 {
 }

//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::TBackgroundDocCreator::Execute()
 {
  CoInitialize(NULL);
  FDC->CreateDocument();
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::TBackgroundDocCreator::ThreadSafeCall(TdcSyncMethod Method)
 {
  Synchronize(Method);
 }
//---------------------------------------------------------------------------
__fastcall TdsDocCreator::TdsDocCreator(TdsDocDM * ADM)
 {
  FDM                = ADM;
  FQ                 = FDM->CreateTempQuery();
  FQ1                = FDM->CreateTempQuery();
  FDQ                = FDM->CreateDocTempQuery();
  FDoc               = new TTagNode;
  FSpec              = new TTagNode;
  FFilter            = NULL;
  FOnGetAVal         = NULL;
  FOnGetDVal         = NULL;
  FDebugCreate       = false;
  FDocDebugXML       = new TTagNode(NULL);
  FDocDebugXML->Name = "DocDebugList";
  fdRepl << rfReplaceAll << rfIgnoreCase;
  FBkgCreator = NULL;
  FSQLCreator = new TdsDocSQLCreator(FDM->XMLList); /*FDM->ICSNom*/
  FErrMsgNode = NULL;
 }
//---------------------------------------------------------------------------
__fastcall TdsDocCreator::~TdsDocCreator()
 {
  delete FDocDebugXML;
  FDocDebugXML = NULL;
  delete FSQLCreator;
  delete FDoc;
  delete FSpec;
  if (FFilter)
   delete FFilter;
  if (FErrMsgNode)
   delete FErrMsgNode;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::SyncCall(TdcSyncMethod AMethod)
 {
  if (FBkgCreator)
   FBkgCreator->ThreadSafeCall(AMethod);
  else
   AMethod();
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogMsg);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::SyncCallLogMsg()
 {
  if (FOnLogMessage)
   FOnLogMessage(FLogMsg, FLogFuncName, FLogALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FErrMsgNode->AddChild("err")->AV["PCDATA"] = AMessage;
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogErr);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::SyncCallLogErr()
 {
  if (FOnLogError)
   FOnLogError(FLogMsg, FLogFuncName, FLogALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  FLogMsg      = ASQL;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  FLogExecTime = AExecTime;
  SyncCall(& SyncCallLogSQL);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::SyncCallLogSQL()
 {
  if (FOnLogSQL)
   FOnLogSQL(FLogMsg, FLogFuncName, FLogExecTime, FLogALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::LogBegin(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogBegin);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::SyncCallLogBegin()
 {
  if (FOnLogBegin)
   FOnLogBegin(FLogFuncName);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::LogEnd(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogEnd);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::SyncCallLogEnd()
 {
  if (FOnLogEnd)
   FOnLogEnd(FLogFuncName);
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDocCreator::CreateDocument()
 {
  LogBegin(__FUNC__);
  bool RC = false;
  FErrMsgNode       = new TTagNode;
  FErrMsgNode->Name = "errcontent";
  try
   {
    try
     {
      bool FCanCont = true;
      TTagNode * FPassport, *FSpecPass;
      FDoc->Name = "docum";
      FPassport  = FDoc->AddChild("passport", "GUI=" + FGUI);
      FSpecPass  = GetPassport(FSpec);
      SetDocSpecGUI(FDoc, GetSpecGUI(FSpec));
      FExtFilterName  = "";
      FExtFilterValue = "";
      if (FFilter)
       {
        FPassport->AV["GUIregion"] = GetRPartE(FFilterNameGUI, ':'); //FFilter->AV["gui"];
        FExtFilterName             = GetLPartE(FFilterNameGUI, ':'); //FFilter->AV["mainname"];
       }

      SetDocName(FDoc, GetSpecName(FSpec));
      SetDocOwner(FDoc, FAuthor);
      FPassport->AV["version"]   = FSpecPass->AV["version"];
      FPassport->AV["release"]   = FSpecPass->AV["release"];
      FPassport->AV["timestamp"] = TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss");
      UnicodeString FPeriodStr = "";
      if (FPerType)
       {
        if ((int)FDateFr && (int)FDateTo)
         FPeriodStr = FDateFr.FormatString("dd.mm.yyyy") + "-" + FDateTo.FormatString("dd.mm.yyyy");
        else if ((int)FDateFr)
         FPeriodStr = FDateFr.FormatString("dd.mm.yyyy") + "-";
        else if ((int)FDateTo)
         FPeriodStr = "-" + FDateTo.FormatString("dd.mm.yyyy");
        FSQLCreator->SetPeriod(FPerType, FDateFr, FDateTo);
       }
      SetDocPeriod(FDoc, IntToStr(FPerType));
      FPassport->AV["periodval"] = FPeriodStr;
      CreateSign(FDoc, FSpec);
      TTagNode * FFilterCondRules = NULL;
      TTagNode * FTabCR = NULL;
      if (FFilter)
       {
        FTabCR = FFilter->GetChildByName("condruleslist", true);
        if (FTabCR)
         {
          FFilterCondRules = FTabCR->GetChildByName("condrules", true);
          FTabCR           = FTabCR->GetChildByName("tabcondrules", true);
         }
        else if (FFilter->CmpName("condrules"))
         FFilterCondRules = FFilter;
        else
         FFilterCondRules = FFilter->GetChildByName("condrules", true);
       }
      if (FConcr)
       FDocCondRules = FFilterCondRules;
      else
       FDocCondRules = MergeCondRules(FSpec, FSpec->GetChildByName("condrules"), FFilterCondRules, false);

      if (FTabCR)
       MergeSpecTabCondRules(FSpec, FTabCR);

      if (FDocCondRules && (FDocCreateType == dctBase))
       FExtFilterValue = FGetFilterValues(FDocCondRules);
      FSetProgress(TkabProgressType::InitCommon, FSpec->GetChildByName("doccontent")->Count + FSpec->GetChildByName("doccontent")->GetCount(false, "table"), FMT(dsDocDocCreatorInitCaption));
      if (FDocCreateType == dctDoc)
       {
        FDocCreateType = dctEmpty;
        CreateContent(FSpec->GetChildByName("doccontent"), FDoc->AddChild("contens"));
        FDocCreateType = dctDoc;
        RC             = CreateDocFromDoc(FSpec, FDoc);
       }
      else
       {
        if (FSpec->GetCount(true, "condrules") == 0)
         {
          //FDocCreateType = dctEmpty;
          CreateContent(FSpec->GetChildByName("doccontent"), FDoc->AddChild("contens"));
         }
        else
         CreateContent(FSpec->GetChildByName("doccontent"), FDoc->AddChild("contens"));
        RC = true;
       }
      // *************************************************************
      FCanCont = false;

      UnicodeString FSPGUI = GetSpecGUI(FSpec).UpperCase();
      if (FSPGUI.Length() > 6)
       FCanCont = (FSPGUI.SubString(1, 6) == "EXTDOC") && FDM->OnExtCreateDoc;
      if (FCanCont)
       {
        UnicodeString ExtCreateRC = FDM->OnExtCreateDoc(FSpec, FDoc, FDocCondRules, FPerType, FDateFr, FDateTo);
        RC = ExtCreateRC.Trim().Length();
        if (RC)
         {
          FDoc->LoadFromZIPXMLFile(ExtCreateRC.Trim());
          if (FileExists(ExtCreateRC.Trim()))
           DeleteFile(ExtCreateRC.Trim());
         }
       }
      // *************************************************************
     }
    catch (Exception & E)
     {
      RC = false;
      FSetProgress(TkabProgressType::Error, 0, E.Message);
     }
   }
  __finally
   {
    //FDM->ICSNom.OlePropertySet("LPUCode",  StringToOleStr(FDM->MainOwnerCode));
    if (FErrMsgNode->Count)
     GetPassport(FDoc)->AddChild("tmp")->AsXML = FErrMsgNode->AsXML;
    if (RC)
     FSetProgress(TkabProgressType::CommComplite, -777, FGUI);
    delete FErrMsgNode;
    FErrMsgNode = NULL;
   }
  LogEnd(__FUNC__);
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocCreator::StartDocCreate(TTagNode * ASpec, UnicodeString AAuthor, TTagNode * AFilter, UnicodeString AFilterNameGUI, bool AConcr,
    TdsDocCreateType ActType, UnicodeString AGUI, TDate ADateFr, TDate ADateTo, int APerType)
 {
  UnicodeString RC = "error";
  try
   {
    FSpec->Assign(ASpec, true);
    FAuthor = AAuthor;
    delete FDoc;
    FDoc = new TTagNode;
    if (FFilter)
     delete FFilter;
    FFilter        = NULL;
    FFilterNameGUI = AFilterNameGUI;
    if (AFilter)
     {
      FFilter = new TTagNode;
      FFilter->Assign(AFilter, true);
     }
    FConcr = AConcr;
    FDocCreateType = ActType;
    FGUI           = AGUI;
    FDateFr        = ADateFr;
    FDateTo        = ADateTo;
    FPerType       = APerType;
    if (FBkgCreator)
     delete FBkgCreator;
    FBkgCreator = new TBackgroundDocCreator(false, this);
    RC          = "ok";
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDocCreator::DocCreate(TTagNode * ASpec, UnicodeString AAuthor, TTagNode * ADoc, TTagNode * AFilter, bool AConcr,
    TdsDocCreateType ActType, UnicodeString AGUI, TDate ADateFr, TDate ADateTo, int APerType)
 {
  bool RC = false;
  try
   {
    FSpec->Assign(ASpec, true);
    FAuthor = AAuthor;
    delete FDoc;
    FDoc = new TTagNode;
    if (FFilter)
     delete FFilter;
    FFilter = NULL;
    if (AFilter)
     {
      FFilter = new TTagNode;
      FFilter->Assign(AFilter, true);
     }
    FConcr = AConcr;
    FDocCreateType = ActType;
    FGUI           = AGUI;
    FDateFr        = ADateFr;
    FDateTo        = ADateTo;
    FPerType       = APerType;
    RC             = CreateDocument();
    if (RC)
     ADoc->Assign(FDoc, true);
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::CreateSign(TTagNode * ADoc, TTagNode * ASpec)
 {
  LogBegin(__FUNC__);
  ADoc->AddChild("description");
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::CreateContent(TTagNode * ASpec, TTagNode * AParent)
 {
  LogBegin(__FUNC__);
  if (ASpec)
   {
    if (ASpec->CmpName("doccontent"))
     {
      CreateContent(ASpec->GetFirstChild(), AParent);
     }
    else if (ASpec->CmpName("inscription"))
     {
      CreateInscription(ASpec, AParent);
      CreateContent(ASpec->GetNext(), AParent);
     }
    else if (ASpec->CmpName("list"))
     {
      CreateList(ASpec, AParent);
      CreateContent(ASpec->GetNext(), AParent);
     }
    else if (ASpec->CmpName("table"))
     {
      CreateTable(ASpec, AParent);
      CreateContent(ASpec->GetNext(), AParent);
     }
   }
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
//##############################################################################
//#                                  �������                                   #
//##############################################################################
void __fastcall TdsDocCreator::CreateInscription(TTagNode * ASpec, TTagNode * AParent)
 {
  LogBegin(__FUNC__);
  FSetProgress(TkabProgressType::InitStage, ASpec->Count, FMT(dsDocDocCreatorInscCreateCaption));
  TTagNode * itText = ASpec->GetFirstChild();
  TTagNode * xaxa;
  TTagNode * ox;
  TTagNode * kyky;
  TTagNode * dNode;
  if (FDebugCreate)
   dNode = FDocDebugXML->AddChild("inscription");

  TTagNode * sDoc = AParent->AddChild("lv", "uid=" + AParent->NewUID() + ",uidmain=" + ASpec->AV["UID"]);
  TTagNode * tmp, *tmp1;
  while (itText)
   {
    if (itText->CmpName("instext"))
     {
      TTagNode * tmp = sDoc->AddChild("lv", "uid=" + AParent->NewUID() + ",uidmain=" + itText->AV["UID"]);
      if (itText->AV["ref"].Length())
       {
        xaxa            = tmp->AddChild("iv");
        xaxa->AV["val"] = GetRefValue(itText->AV["ref"]);
        tmp->AddChild("iv", "val=");
        if (FDebugCreate)
         dNode->AddChild("item")->AV["val"] = xaxa->AV["val"];
       }
      else
       {
        if (FDebugCreate)
         kyky = dNode->AddChild("item");
        tmp->AddChild("iv", "val=");
        if (itText->GetCount(false, "count,calcrules") != 0)
         {
          tmp1 = itText->GetFirstChild();
          if (tmp1)
           {
            if (tmp1->CmpName("count"))
             {
              UnicodeString FCountSQL = "";
              //���������� ���������� � ������ ������� ���������� ��������� � ������� � ������ ������� ���������
              /*
               FSQLCreator->CreateSQL(tmp1->AV["ref"], tmp1->GetFirstChild(), NULL, "", FCountSQL, NULL);
               */
              FSQLCreator->CreateSQL(tmp1->AV["ref"],
                  MergeCondRules(tmp1, tmp1->GetChildByName("condrules"), FDocCondRules, false),
                  NULL, "", FCountSQL, NULL);
              //~~ ����� ������� - ������� ������������ SQL ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
              __DBGPrint("/*- inscription text begin---" + tmp1->AV["ref"] + "-------*/");
              __DBGPrint(FCountSQL);
              __DBGPrint("/*- inscription text end-----" + tmp1->AV["ref"] + "-------*/");
              //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
              FDM->quExec(FQ, false, FCountSQL);
              tmp->AddChild("iv")->AV["val"] = FQ->FieldByName("RCOUNT")->AsString;
              if (FDebugCreate && tmp1->GetFirstChild())
               {
                kyky->AddChild("condrules")->AsXML = StringReplace(tmp1->GetFirstChild()->AsXML, "uid", "repl_uid", fdRepl);
                kyky->AV["val"] = FQ->FieldByName("RCOUNT")->AsString;
               }
              //!!!                     FDM->qtCommit();
             }
            else
             {
              xaxa          = itText->GetChildByName("calcrules");
              ox            = tmp->AddChild("iv");
              ox->AV["val"] = GetCalcValue(xaxa);
              if (FDebugCreate && xaxa)
               {
                kyky->AddChild("condrules")->AsXML = StringReplace(xaxa->AsXML, "uid", "repl_uid", fdRepl);
                kyky->AV["val"] = ox->AV["val"];
               }
             }
           }
         }
        else
         tmp->AddChild("iv", "val=");
       }
     }
    itText = itText->GetNext();
   }
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocCreator::GetRefValue(UnicodeString ARef)
 {
  LogBegin(__FUNC__);
  UnicodeString RC = "";
  UnicodeString FFlList = "";
  UnicodeString _ProcName_ = "";
  UnicodeString FDocPerDate = ";";
  try
   {
    TTagNode * FISAttr = NULL;
    if (FDocCondRules)
     FISAttr = FDocCondRules->GetTagByUID(ARef);
    if (!FISAttr)
     FISAttr = FDM->XMLList->GetRefer(ARef);
    if (FISAttr)
     {
      if (FISAttr->CmpName("IERef"))
       {
        RC = GetCondValues(FISAttr, false);
       }
      else
       {
        if (((int)FDateFr) && ((int)FDateTo))
         FDocPerDate = FDateFr.FormatString("dd.mm.yyyy") + ";" + FDateTo.FormatString("dd.mm.yyyy");
        else if (((int)FDateFr))
         FDocPerDate = FDateFr.FormatString("dd.mm.yyyy") + ";";
        else if (((int)FDateTo))
         FDocPerDate = ";" + FDateTo.FormatString("dd.mm.yyyy");

        _ProcName_ = "A" + FISAttr->AV["procname"];

        if (_wcscmpi(_ProcName_.c_str(), L"ADocAttrVal") == 0)
         {
          FFlList += "period=" + GetDocPeriodValStr(GetDocPeriod(FDoc),
              GetDocPeriodValFrom(FDoc),
              GetDocPeriodValTo(FDoc)) + "\n";
          FFlList += "periodfr=" + GetDocPeriodValFrom(FDoc) + "\n";
          FFlList += "periodto=" + GetDocPeriodValTo(FDoc) + "\n";
          FFlList += "createdate=" + TDateTime::CurrentDate().FormatString("dd.mm.yyyy") + "\n";
          FFlList += "autor=" + GetOwnerName(GetDocOwner(FDoc)) + "\n";
          FFlList += "createtime=" + TDateTime::CurrentTime().FormatString("hh:nn:ss") + "\n";
          FFlList += "extfiltername=" + FExtFilterName + "\n";
          FFlList += "extfiltervalues=" + FExtFilterValue + "\n";
          FFlList += "oldyear=" + Icsdateutil::IncYear(TDateTime::CurrentDate(), -1).FormatString("yyyy") + "\n";
          FFlList += "oldmonth=" + Icsdateutil::IncMonth(TDateTime::CurrentDate(), -1).FormatString("mm") + "\n";
          if (FISAttr->AV["procparam"].Length())
           FFlList += ":" + FISAttr->AV["procparam"] + "\n";
         }
        else if (_wcscmpi(_ProcName_.c_str(), L"ASetOrgCode") == 0)
         {
          FFlList += "CODE=" + GetOwnerName(GetDocOwner(FDoc), "CODE") + "\n";
         }
        else if (_wcscmpi(_ProcName_.c_str(), L"ASetLPUName") == 0)
         {
          FFlList += "NAME=" + GetOwnerName(GetDocOwner(FDoc), "NAME") + "\n";
         }
        else if (_wcscmpi(_ProcName_.c_str(), L"ASetLPUFullName") == 0)
         {
          FFlList += "FULLNAME=" + GetOwnerName(GetDocOwner(FDoc), "FULLNAME") + "\n";
         }
        else if (_wcscmpi(_ProcName_.c_str(), L"ASetAddr") == 0)
         {
          FFlList += "ADDR=" + GetOwnerName(GetDocOwner(FDoc), "ADDR") + "\n";
         }
        else if (_wcscmpi(_ProcName_.c_str(), L"ASetPhone") == 0)
         {
          FFlList += "PHONE=" + GetOwnerName(GetDocOwner(FDoc), "PHONE") + "\n";
         }
        else if (_wcscmpi(_ProcName_.c_str(), L"ASetEmail") == 0)
         {
          FFlList += "EMAIL=" + GetOwnerName(GetDocOwner(FDoc), "EMAIL") + "\n";
         }
        RC = FGetNomAVal(_ProcName_, FDocPerDate, FFlList, FISAttr->AV["ref"], GetSpecGUI(FISAttr) + "." + FISAttr->AV["uid"]);
       }
     }
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocCreator::FGetNomAVal(UnicodeString AProcName, UnicodeString ADocPerDate, UnicodeString AFields, UnicodeString ARef, UnicodeString AAttrRef)
 {
  LogBegin(__FUNC__);
  UnicodeString RC = ADocPerDate;
  try
   {
    if (FOnGetAVal)
     {
      try
       {
        FOnGetAVal(AProcName, AFields, ARef, AAttrRef, RC);
       }
      catch (Exception & E)
       {
        LogError(cFMT2(dsDocSQLCreatorErrorFuncMissing, AProcName, E.Message), __FUNC__);
       }
     }
    else
     LogError("�� ����� ���������� ��� OnGetAVal", __FUNC__);

   }
  __finally
   {
   }
  LogEnd(__FUNC__);
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocCreator::GetCalcValue(TTagNode * ACalcRules)
 {
  return "test - GetCalcValue";
 }
//---------------------------------------------------------------------------
//##############################################################################
//#                                  ������                                    #
//##############################################################################
void __fastcall TdsDocCreator::CreateList(TTagNode * ASpec, TTagNode * AParent)
 {
  LogBegin(__FUNC__);
  //FSetProgress(0,"������������ ������",ptInitStage);
  TTagNode * sDocList = AParent->AddChild("lv", "uid=" + AParent->NewUID() + ",uidmain=" + ASpec->AV["UID"]);
  TTagNode * sRowList, *itColumn, *FCondRules;
  UnicodeString BaseISRef = ASpec->AV["ref"];
  TStringList * ColList = NULL;
  TStringList * FOrderList = NULL;
  UnicodeString FSQL;
  try
   {
    if (FDocCreateType != dctEmpty)
     {
      if (ASpec->GetChildByName("sortorder"))
       {//��������� ������ ����� ����������
        FOrderList = new TStringList;
        TTagNode * itOrd = ASpec->GetChildByName("sortorder")->GetFirstChild();
        TTagNode * tmpOrd, *tmpOrdIS, *FFlDef;
        UnicodeString OrdFN, OrdFNList, OrdFNId;
        TTagNode * FlDefRoot = NULL;
        while (itOrd)
         {
          tmpOrd = ASpec->GetTagByUID(itOrd->AV["ref"])->GetFirstChild();
          if (!tmpOrd->CmpName("link_is"))
           {//���������� �������� ������ �� ����� �������� �������� ������
            while (tmpOrd)
             {
              tmpOrdIS = FDM->XMLList->GetRefer(tmpOrd->AV["ref"]);
              if (tmpOrdIS)
               {
                OrdFN  = tmpOrdIS->AV["fields"];
                FFlDef = FDM->XMLList->GetRefer(tmpOrdIS->AV["ref"]);
                if (FFlDef)
                 {
                  FlDefRoot = FFlDef->GetRoot();
                  if (OrdFN.Pos(","))
                   {//������� ��������� �����
                    OrdFNList = OrdFN;
                    while (OrdFNList.Length())
                     {
                      if (!OrdFNList.Pos(","))
                       {
                        OrdFN     = OrdFNList;
                        OrdFNList = "";
                       }
                      else
                       {
                        OrdFN     = GetLPartB(OrdFNList, ',').Trim();
                        OrdFNList = GetRPartB(OrdFNList, ',').Trim();
                       }
                      if (OrdFN.Length() == 5)
                       {
                        FFlDef = FlDefRoot->GetTagByUID(OrdFN.SubString(2, 4));
                        if (FFlDef)
                         {
                          if (FFlDef->CmpName("text") || (FFlDef->CmpName("extedit") && !FFlDef->AV["fieldtype"].Length() && FFlDef->AV["length"].ToIntDef(0)))
                           {//��������� ����
                            OrdFN = "Upper(" + OrdFN + ")";
                           }
                         }
                       }
                      if (OrdFN.Length())
                       {
                        if (itOrd->CmpAV("sorttype", "incr"))
                         FOrderList->Add((OrdFN + " ASC"));
                        else
                         FOrderList->Add((OrdFN + " DESC"));
                       }
                     }
                    OrdFN = "";
                   }
                  else
                   {//������� ���� ����
                    if (FFlDef->CmpName("text") || (FFlDef->CmpName("extedit") && !FFlDef->AV["fieldtype"].Length() && FFlDef->AV["length"].ToIntDef(0)))
                     {//��������� ����
                      OrdFN = "Upper(" + OrdFN + ")";
                     }
                   }
                 }
                if (OrdFN.Length())
                 {
                  if (itOrd->CmpAV("sorttype", "incr"))
                   FOrderList->Add((OrdFN + " ASC"));
                  else
                   FOrderList->Add((OrdFN + " DESC"));
                 }
               }
              tmpOrd = tmpOrd->GetNext();
             }
           }
          itOrd = itOrd->GetNext();
         }
       }
      //���������� ������� ���������� ������ � ����� ������ ���������
      FCondRules = MergeCondRules(ASpec, ASpec->GetChildByName("condrules"), FDocCondRules, false);
      ColList = new TStringList;
      UnicodeString TabName = FSQLCreator->GetTabName(BaseISRef); //��� ������� �������� ��������
      //��������� ������ ����� ��� �������� ��������
      //+ ���� ����������� ��� ����� �� ���������� ����������
      CreateBaseTabFields(ASpec->GetChildByName("listcont"), ColList, BaseISRef);
      UnicodeString FCountSQL = "";
      //�������� SQL-��������� ��� �������� ��������
      FSQL = FSQLCreator->CreateSQL(BaseISRef, FCondRules, ColList, "", FCountSQL, FOrderList);
      FDM->quExec(FQ, false, FCountSQL);
      FSetProgress(TkabProgressType::InitStage, FQ->FieldByName("RCOUNT")->AsInteger, FMT1(dsDocDocCreatorListCreateCaption, FQ->FieldByName("RCOUNT")->AsString));
      //~~ ����� ������� - ������� ������������ SQL ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      __DBGPrint("/*- list begin---" + ASpec->AV["uid"] + "-------*/");
      __DBGPrint(FSQL);
      __DBGPrint("/*- list end-----" + ASpec->AV["uid"] + "-------*/");
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      FDM->quExec(FQ, false, FSQL);
      while (!FQ->Eof)
       {//���� �� ������� �������� ��������
        FSetProgress();
        sRowList = sDocList->AddChild("lv", "uid=" + AParent->NewUID());
        for (int i = 0; i < ColList->Count; i++)
         {//���� �� �������� ������
          itColumn = (TTagNode *)ColList->Objects[i];
          if (itColumn->CmpName("link_is"))
           {//� ������� ����(����) ��������� ��������
            GetLinkISValue(itColumn, sRowList, BaseISRef);
           }
          else
           {//� ������� ���� �������� ��������
            sRowList->AddChild("iv")->AV["val"] = GetListFlValue(itColumn, false);
           }
         }
        FQ->Next();
       }
      //FDM->qtCommit();
     }
   }
  __finally
   {
    if (ColList)
     delete ColList;
    //FDM->qtCommit();   //??? trFree1->Commit
    if (FOrderList)
     delete FOrderList;
   }
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::CreateBaseTabFields(TTagNode * ASpec, TStringList * AColList, UnicodeString ABaseISRef)
 {//������������ ������ ����� ��� �������� ��������
  LogBegin(__FUNC__);
  TTagNode * itNode = ASpec->GetFirstChild();
  if (ASpec->CmpName("listcont,listcolgroup"))
   {
    while (itNode)
     {
      CreateBaseTabFields(itNode, AColList, ABaseISRef);
      itNode = itNode->GetNext();
     }
   }
  else if (ASpec->CmpName("listcol"))
   {
    if (itNode->CmpName("is_attr"))
     {//������� �������� ��������
      if (itNode->AV["ref"].Length())
       AColList->AddObject(GetAttrFields(itNode->AV["ref"]), (TObject *)itNode);
     }
    else
     {//������ �� ��������� ��������
      AColList->AddObject("_link_is_=" + GetLinkISKey(itNode->AV["ref"], ABaseISRef), (TObject *)itNode);
     }
   }
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::CreateLinkTabFields(TTagNode * ALinkIS, TStringList * AColList)
 {//������������ ������ ����� ��� ��������� ��������
  LogBegin(__FUNC__);
  TTagNode * itNode = ALinkIS->GetFirstChild();
  while (itNode)
   {
    if (itNode->CmpName("is_attr"))
     {//������� �������� ��������
      if (itNode->AV["ref"].Length())
       AColList->AddObject(GetAttrFields(itNode->AV["ref"]), (TObject *)itNode);
     }
    itNode = itNode->GetNext();
   }
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocCreator::GetLinkISKey(UnicodeString ALinkISRef, UnicodeString ABaseISRef)
 {//��������� ������ �������� ����� ��� ����� �������� � ��������� ���������
  /*
   TTagNode *tmp;
   TStringList *FLinkPath = NULL;
   UnicodeString RC = "";
   try
   {
   FLinkPath = new TStringList;
   TTagNode *IS = FDM->XMLList->GetRefer(ABaseISRef);
   if (IS)
   {
   UnicodeString FAlias = IS->AV["tabalias"];
   tmp = IS->GetChildByName("refers");
   TTagNode *FRef = tmp->GetChildByAV("refer","ref",_UID(ALinkISRef));
   if (!FRef)
   {
   FSQLCreator->GetISLinkPath(tmp, ALinkISRef, FLinkPath);
   if (FLinkPath->Count)
   {
   FRef = (TTagNode*)FLinkPath->Objects[0];
   FRef = tmp->GetChildByAV("refer","ref",_UID(FRef->GetParent("is")->AV["uid"]));
   }
   }
   if (FRef)
   {
   tmp = FRef->GetFirstChild();
   while (tmp)
   {
   RC += FAlias+"."+tmp->AV["fieldin"]+" as "+FAlias+tmp->AV["fieldin"]+",";
   tmp = tmp->GetNext();
   }
   RC = RC.Delete(RC.Length(),1);
   }
   else
   LogError(FMT(dsDocDocCreatorErrorISLink),cFMT(dsDocCommonErrorCaption), __FUNC__);
   }
   }
   __finally
   {
   if (FLinkPath) delete FLinkPath;
   }
   return RC;
   */
  /*
   TTagNode *tmp;
   TStringList *FLinkPath = NULL;
   UnicodeString RC = "";
   try
   {
   FLinkPath = new TStringList;
   TTagNode *IS = FDM->XMLList->GetRefer(ABaseISRef);
   if (IS)
   {
   UnicodeString FAlias = IS->AV["tabalias"];
   tmp = IS->GetChildByName("refers");
   TTagNode *FRef = tmp->GetChildByAV("refer","ref",_UID(ALinkISRef));
   if (FRef)
   { // ���� ������ ����� ����� ����������
   tmp = FRef->GetFirstChild();
   while (tmp)
   {
   RC += FAlias+"."+tmp->AV["fieldin"]+" as "+FAlias+tmp->AV["fieldin"]+",";
   tmp = tmp->GetNext();
   }
   RC = RC.Delete(RC.Length(),1);
   }
   else
   { // ����������� ������ ����� ����� ����������
   FSQLCreator->GetISLinkPath(tmp, ALinkISRef, FLinkPath);
   if (FLinkPath->Count)
   {
   for (int i = FLinkPath->Count-1; i >= 0; i--)
   {
   FRef = (TTagNode*)FLinkPath->Objects[i];
   tmp = IS->GetTagByUID(FRef->AV["ref"]);
   if (tmp)
   FAlias = tmp->AV["tabalias"];
   else
   {
   RC = "";
   break;
   }
   tmp = FRef->GetFirstChild();
   while (tmp)
   {
   RC += FAlias+"."+tmp->AV["fieldout"]+" as "+FAlias+tmp->AV["fieldout"]+",";
   tmp = tmp->GetNext();
   }
   }
   RC = RC.Delete(RC.Length(),1);
   }
   else
   LogError(FMT(dsDocDocCreatorErrorISLink), __FUNC__,cFMT(dsDocCommonErrorCaption));
   }
   }
   }
   __finally
   {
   if (FLinkPath) delete FLinkPath;
   }
   return RC;
   */
  LogBegin(__FUNC__);
  TTagNode * tmp;
  TStringList * FLinkPath = NULL;
  UnicodeString RC = "";
  try
   {
    FLinkPath = new TStringList;
    TTagNode * IS = FDM->XMLList->GetRefer(ABaseISRef);
    if (IS)
     {
      UnicodeString FAlias = IS->AV["tabalias"];
      tmp = IS->GetChildByName("refers");
      TTagNode * FRef = tmp->GetChildByAV("refer", "ref", _UID(ALinkISRef));
      bool IsLink = false;
      if (FRef)
       {//���� ������ ����� ����� ����������
        IsLink = true;
       }
      else
       {//����������� ������ ����� ����� ����������
        FSQLCreator->GetISLinkPath(tmp, ALinkISRef, FLinkPath);
        if (FLinkPath->Count)
         {
          IsLink = true;
          FRef   = (TTagNode *)FLinkPath->Objects[FLinkPath->Count - 1];
         }
       }
      if (IsLink)
       {
        tmp = FRef->GetFirstChild();
        while (tmp)
         {
          RC += FAlias + "." + tmp->AV["fieldin"] + " as " + FAlias + tmp->AV["fieldin"] + ",";
          tmp = tmp->GetNext();
         }
        RC = RC.Delete(RC.Length(), 1);
       }
      else
       LogError(FMT(dsDocDocCreatorErrorISLink), __FUNC__);
     }
   }
  __finally
   {
    if (FLinkPath)
     delete FLinkPath;
   }
  LogEnd(__FUNC__);
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocCreator::GetAttrFields(UnicodeString ARefIS)
 {//��������� ��� �������� ��������� �������� � ��� ����� ��� �������� ��������

  //������� -
  //"procname"="tabalias"."field1" as "tabalias""field1", ... ,"tabalias"."fieldN" as "tabalias""fieldN"
  //������ - ChoiceValue=CB.NAME as CBNAME, CB.FAM as CBFAM
  LogBegin(__FUNC__);
  UnicodeString RC = "";
  TStringList * DDD = new TStringList;
  try
   {
    TTagNode * FISAttr = FDM->XMLList->GetRefer(ARefIS);
    if (FISAttr)
     {
      TReplaceFlags rFlag;
      rFlag << rfReplaceAll;
      UnicodeString FAlias = FSQLCreator->GetTabAlias(FISAttr);
      UnicodeString FFields = "";
      DDD->Text = StringReplace(FISAttr->AV["fields"], ",", "\n", rFlag);
      for (int i = 0; i < DDD->Count; i++)
       {
        FFields += FAlias + "." + DDD->Strings[i] + " as " + FAlias + DDD->Strings[i];
        if (i < DDD->Count - 1)
         FFields += ","; //��������� "," - ����� ���������� ����
       }
      RC = FISAttr->AV["procname"] + "=" + FFields;
     }
   }
  __finally
   {
    delete DDD;
   }
  LogEnd(__FUNC__);
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocCreator::GetListFlValue(TTagNode * AISRef, bool AIsLink)
 {//��������� �������� ����
  LogBegin(__FUNC__);
  UnicodeString RC = "";
  UnicodeString _ProcName_ = "";
  UnicodeString FDocPerDate = ";";
  TStringList * FFlList = NULL;
  try
   {
    UnicodeString FRef = AISRef->AV["ref"];
    TTagNode * FISAttr = FDM->XMLList->GetRefer(FRef);
    if (FISAttr)
     {
      TReplaceFlags rFlag;
      rFlag << rfReplaceAll;
      _ProcName_ = "A" + FISAttr->AV["procname"];
      UnicodeString FAlias = FSQLCreator->GetTabAlias(FISAttr);
      UnicodeString Fields = FISAttr->AV["fields"];
      if (!FISAttr->AV["procname"].Length())
       {
        if (AIsLink)
         RC = FQ1->FieldByName(FAlias + Fields)->AsString;
        else
         RC = FQ->FieldByName(FAlias + Fields)->AsString;
       }
      else
       {
        if (((int)FDateFr) && ((int)FDateTo))
         FDocPerDate = FDateFr.FormatString("dd.mm.yyyy") + ";" + FDateTo.FormatString("dd.mm.yyyy");
        else if (((int)FDateFr))
         FDocPerDate = FDateFr.FormatString("dd.mm.yyyy") + ";";
        else if (((int)FDateTo))
         FDocPerDate = ";" + FDateTo.FormatString("dd.mm.yyyy");

        FFlList       = new TStringList;
        FFlList->Text = StringReplace(Fields, ",", "\n", rFlag);
        for (int i = 0; i < FFlList->Count; i++)
         {
          if (AIsLink)
           {
            if (!FQ1->FieldByName(FAlias + FFlList->Strings[i])->IsNull)
             FFlList->Strings[i] = FFlList->Strings[i] + "=" + FQ1->FieldByName(FAlias + FFlList->Strings[i])->AsString;
            else
             FFlList->Strings[i] = FFlList->Strings[i] + "="; //-1
           }
          else
           {
            if (!FQ->FieldByName(FAlias + FFlList->Strings[i])->IsNull)
             FFlList->Strings[i] = FFlList->Strings[i] + "=" + FQ->FieldByName(FAlias + FFlList->Strings[i])->AsString;
            else
             FFlList->Strings[i] = FFlList->Strings[i] + "="; //-1
           }
         }

        if (FISAttr->AV["procparam"].Length())
         FFlList->Text = FFlList->Text + ":" + FISAttr->AV["procparam"] + "\n";
        RC = FGetNomAVal(_ProcName_, FDocPerDate, FFlList->Text, FISAttr->AV["ref"], GetSpecGUI(FISAttr) + "." + FISAttr->AV["uid"]);
       }
     }
   }
  __finally
   {
    if (FFlList)
     delete FFlList;
   }
  LogEnd(__FUNC__);
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::GetLinkISValue(TTagNode * AISLink, TTagNode * AParent, UnicodeString ABaseISRef)
 {
  LogBegin(__FUNC__);
  UnicodeString FSQL = "", FCountSQL = "", FKeyValue;
  UnicodeString FLinkISRef = AISLink->AV["ref"];
  TTagNode * FLinkIS = FDM->XMLList->GetRefer(FLinkISRef);
  TTagNode * FBaseIS = FDM->XMLList->GetRefer(ABaseISRef);
  TTagNode * FCondRules;
  TTagNode * tmp, *tmp1, *itFl;
  TStringList * FFields = NULL;
  TStringList * FOrderList = NULL;
  TStringList * FLinkPath = NULL;
  try
   {
    FLinkPath = new TStringList;
    if (FLinkIS && FBaseIS)
     {
      //��������� �������� �����
      UnicodeString lBr, rBr, cOper;
      lBr   = "";
      rBr   = "";
      cOper = " and ";
      UnicodeString FAlias = FSQLCreator->GetTabAlias(FLinkISRef);
      UnicodeString FBaseTabAlias = FSQLCreator->GetTabAlias(ABaseISRef);
      tmp = FBaseIS->GetChildByName("refers");
      TTagNode * FRef = tmp->GetChildByAV("refer", "ref", _UID(FLinkISRef));
      if (!FRef)
       {
        FSQLCreator->GetISLinkPath(tmp, FLinkISRef, FLinkPath);
        if (FLinkPath->Count)
         {
          FRef = (TTagNode *)FLinkPath->Objects[FLinkPath->Count - 1];
          tmp1 = FBaseIS->GetTagByUID(FRef->AV["ref"]);
          if (tmp1)
           FAlias = tmp1->AV["tabalias"];
          else
           FRef = NULL;
         }
        TTagNode * tmpIS;
        for (int i = 0; i < FLinkPath->Count; i++)
         {
          tmpIS                 = (TTagNode *)FLinkPath->Objects[i];
          tmpIS                 = tmpIS->GetParent("is");
          FLinkPath->Strings[i] = FSQLCreator->PrepareRef(tmpIS, tmpIS->AV["uid"]) + ":";
         }
       }
      if (!FRef)
       {
        LogError(FMT(dsDocDocCreatorErrorISLink), __FUNC__);
        return;
       }

      if (_wcscmpi(FRef->AV["keylink"].c_str(), L"or") == 0)
       {
        lBr   = "(";
        rBr   = ")";
        cOper = " or ";
       }
      tmp = FRef->GetFirstChild();
      if (tmp->CmpAV("fieldtype", "i"))
       FKeyValue = lBr + FAlias + "." + tmp->AV["fieldout"] + "=" + FQ->FieldByName(FBaseTabAlias + tmp->AV["fieldin"])->AsString;
      else if (tmp->CmpAV("fieldtype", "c"))
       FKeyValue = lBr + FAlias + "." + tmp->AV["fieldout"] + "='" + DateFS(FQ->FieldByName(FBaseTabAlias + tmp->AV["fieldin"])->AsDateTime) + "'";
      else if (tmp->CmpAV("fieldtype", "ch"))
       FKeyValue = lBr + " Upper(" + FAlias + "." + tmp->AV["fieldout"] + ")='" + FQ->FieldByName(FBaseTabAlias + tmp->AV["fieldin"])->AsString.UpperCase() + "'";
      tmp = tmp->GetNext();
      while (tmp)
       {
        if (tmp->CmpAV("fieldtype", "i"))
         FKeyValue += cOper + FAlias + "." + tmp->AV["fieldout"] + "=" + FQ->FieldByName(FBaseTabAlias + tmp->AV["fieldin"])->AsString;
        else if (tmp->CmpAV("fieldtype", "c"))
         FKeyValue += cOper + FAlias + "." + tmp->AV["fieldout"] + "='" + DateFS(FQ->FieldByName(FBaseTabAlias + tmp->AV["fieldin"])->AsDateTime) + "'";
        else if (tmp->CmpAV("fieldtype", "ch"))
         FKeyValue += cOper + " Upper(" + FAlias + "." + tmp->AV["fieldout"] + ")='" + FQ->FieldByName(FBaseTabAlias + tmp->AV["fieldin"])->AsString.UpperCase() + "'";
        tmp = tmp->GetNext();
       }
      FKeyValue += rBr;
      //��������� SQL-�
      if (!AISLink->AV["SQL"].Length())
       {
        if (AISLink->GetChildByName("count"))
         {
          FCondRules = MergeCondRules(AISLink->GetFirstChild(), AISLink->GetFirstChild()->GetChildByName("condrules"), FDocCondRules, false);
          /*
           FSQL = FSQLCreator->CreateSQL(AISLink->AV["ref"],
           AISLink->GetFirstChild()->GetChildByName("condrules"),
           NULL,
           "_KEYVALUE_",
           FCountSQL,
           NULL,
           FLinkPath);
           */
          FSQL = FSQLCreator->CreateSQL(AISLink->AV["ref"],
              FCondRules,
              NULL,
              "_KEYVALUE_",
              FCountSQL,
              NULL,
              FLinkPath);
         }
        else
         {
          if (AISLink->GetParent("list")->GetChildByName("sortorder"))
           {
            TTagNode * tmpOrd, *tmpOrdIS;
            FOrderList = new TStringList;
            TTagNode * Ord = AISLink->GetParent("list")->GetChildByName("sortorder");
            if (Ord->GetChildByAV("sortel", "ref", AISLink->GetParent("listcol")->AV["uid"]))
             {
              tmpOrd = AISLink->GetChildByName("is_attr");
              while (tmpOrd)
               {
                tmpOrdIS = FDM->XMLList->GetRefer(tmpOrd->AV["ref"]);
                if (tmpOrdIS)
                 {
                  if (Ord->CmpAV("sorttype", "incr"))
                   FOrderList->Add((tmpOrdIS->AV["fields"] + " ASC"));
                  else
                   FOrderList->Add((tmpOrdIS->AV["fields"] + " DESC"));
                 }
                tmpOrd = tmpOrd->GetNext();
               }
             }
           }
          FFields = new TStringList;
          CreateLinkTabFields(AISLink, FFields);
          FCondRules = MergeCondRules(AISLink, AISLink->GetChildByName("condrules"), FDocCondRules, false);
          /*
           FSQL = FSQLCreator->CreateSQL(AISLink->AV["ref"],
           AISLink->GetChildByName("condrules"),
           FFields,
           "_KEYVALUE_",
           FCountSQL,
           FOrderList,
           FLinkPath);
           */
          FSQL = FSQLCreator->CreateSQL(AISLink->AV["ref"],
              FCondRules,
              FFields,
              "_KEYVALUE_",
              FCountSQL,
              FOrderList,
              FLinkPath);
         }
        AISLink->AV["SQL"] = FSQL;
        AISLink->AV["SQLCount"] = FCountSQL;
       }
      if (AISLink->GetChildByName("count"))
       {//�������� ���������� ������� ��������� ��������
        FCountSQL = FSQLCreator->ReplaceKey(AISLink->AV["SQLCount"], FKeyValue);
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        __DBGPrint("/*- list ISLink Count begin---" + AISLink->AV["uid"] + "-------*/"); //
        __DBGPrint(FCountSQL); //
        __DBGPrint("/*- list ISLink Count end-----" + AISLink->AV["uid"] + "-------*/"); //
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        FDM->quExec(FQ1, false, FCountSQL);
        AParent->AddChild("iv")->AV["val"] = FQ1->FieldByName("RCOUNT")->AsString;
       }
      else
       {//�������� �������� ��������� ��������� ��������
        tmp  = AParent->AddChild("lv", "uid=" + AParent->NewUID() + ",uidmain=" + AISLink->GetParent()->AV["UID"]);
        FSQL = FSQLCreator->ReplaceKey(AISLink->AV["SQL"], FKeyValue);
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        __DBGPrint("/*- list ISLink ATTR begin---" + AISLink->AV["uid"] + "-------*/"); //
        __DBGPrint(FSQL); //
        __DBGPrint("/*- list ISLink ATTR end-----" + AISLink->AV["uid"] + "-------*/"); //
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        FDM->quExec(FQ1, false, FSQL);
        while (!FQ1->Eof)
         {
          tmp1 = tmp->AddChild("lv", "uid=" + AParent->NewUID());
          itFl = AISLink->GetChildByName("is_attr");
          while (itFl)
           {
            if (itFl->CmpName("is_attr"))
             tmp1->AddChild("iv")->AV["val"] = GetListFlValue(itFl, true);
            itFl = itFl->GetNext();
           }
          FQ1->Next();
         }
        //FDM->qt1Commit();
       }
     }
    else
     AParent->AddChild("iv", "val=");
   }
  __finally
   {
    if (FLinkPath)
     delete FLinkPath;
    if (FFields)
     delete FFields;
    if (FOrderList)
     delete FOrderList;
   }
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
//##############################################################################
//#                                  �������                                   #
//##############################################################################
void __fastcall TdsDocCreator::CreateTable(TTagNode * ATabDef, TTagNode * AParent)
 {
  //FSetProgress(0,"������������ �������",ptInitStage);
  LogBegin(__FUNC__);
  TTagNode * FCols, *FRows, *FCondRules, *FCol, *FRow;
  UnicodeString FBaseISRef = ATabDef->AV["ref"];

  TTagNode * tNode, *trNode, *ttNode, *tcNode, *tsNodes, *ittsNode, *tsDataNode;
  if (FDebugCreate)
   {
    tNode      = FDocDebugXML->AddChild("table");
    trNode     = tNode->AddChild("rows");
    tcNode     = tNode->AddChild("cols");
    tsNodes    = tNode->AddChild("cells");
    tsDataNode = tNode->AddChild("cellsdata");
   }
  FCols = ATabDef->GetChildByName("columns");
  FRows = ATabDef->GetChildByName("rows");
  //���������� ������� ��������� � �������
  FCondRules = MergeCondRules(ATabDef, ATabDef->GetChildByName("condrules"), FDocCondRules, false);
  bool FPrty = ATabDef->CmpAV("tablepriority", "row");
  TStringList * FColList = NULL;
  TStringList * FRowList = NULL;
  float * (* FCells) = NULL;
  //TAnsiStrMap tmpUnion;
  long elCount;
  try
   {
    if (FCols && FRows)
     {//���������� �������� ����� � ��������
      FColList = new TStringList;
      FRowList = new TStringList;
      //������ ������ ����� ��� FBaseISRef �� ������ �������, ������, ������
      GetTabItems(FCols, FColList, NULL, "tcol");
      GetTabItems(FRows, FRowList, NULL, "trow");
      FSetProgress(TkabProgressType::InitStage, FCols->GetCount(true, "tcol") * 2 + FRows->GetCount(true, "trow") * 2, FMT(dsDocDocCreatorTableCreateGetCount));

      FSetProgress(TkabProgressType::StageInc, 0, FMT(dsDocDocCreatorTableCreateRows));
      FRowColNum = 1;
      if (FDocCreateType != dctEmpty)
       GetTabSelItems(FBaseISRef, FRows, FRowList, NULL, "trow", FMT(dsDocDocCreatorTableCreateRow), trNode);

      FSetProgress(TkabProgressType::StageInc, 0, FMT(dsDocDocCreatorTableCreateCols));
      FRowColNum = 1;
      if (FDocCreateType != dctEmpty)
       GetTabSelItems(FBaseISRef, FCols, FColList, FCondRules, "tcol", FMT(dsDocDocCreatorTableCreateCol), tcNode);

      if (FDocCreateType == dctEmpty)
       FSetProgress(TkabProgressType::InitStage, FRowList->Count * FColList->Count, FMT(dsDocDocCreatorTableCreateCeills));
      else
       FSetProgress(TkabProgressType::InitStage, 2 * FRowList->Count * FColList->Count, FMT(dsDocDocCreatorTableCreateCeills));

      FCells = new float * [FRowList->Count];
      for (int i = 0; i < FRowList->Count; i++)
       FCells[i] = new float[FColList->Count];
      //��������� ������ �� ��������� �������
      UnicodeString rUID, cUID;
      for (int i = 0; i < FRowList->Count; i++)
       {
        FRow = (TTagNode *)FRowList->Objects[i];
        rUID = FRow->AV["uid"];
        for (int j = 0; j < FColList->Count; j++)
         {
          if (FDebugCreate)
           {
            ittsNode             = tsNodes->AddChild("cell");
            ittsNode->AV["href"] = ATabDef->AV["uid"] + "_" + IntToStr(i + 1) + "_" + IntToStr(j + 1);
           }
          //FSetProgress();
          FCol = (TTagNode *)FColList->Objects[j];
          cUID = FCol->AV["uid"];
          if (FDocCreateType == dctEmpty)
           FCells[i][j] = 0;
          else
           {
            elCount = 0;
            FSetProgress(TkabProgressType::StageInc, 0, FMT2(dsDocDocCreatorTableCreateCeill, IntToStr(i + 1), IntToStr(j + 1)));
            if (FUnitCodes[rUID] && FUnitCodes[cUID])
             {//��� ��������� �� ������
              __DBGPrint(L"  /*- ��� ��������� �� ������ -" + IntToStr((int)FUnitCodes[rUID]->size()) + "--" + IntToStr((int)FUnitCodes[cUID]->size()) + "---*/");
              if (FUnitCodes[rUID]->size() && FUnitCodes[cUID]->size())
               {//��� ��������� ����������, ���������� �����������
                TCodeList * un1;
                TCodeList * un2;
                if (FUnitCodes[rUID]->size() < FUnitCodes[cUID]->size())
                 {
                  un1 = FUnitCodes[rUID];
                  un2 = FUnitCodes[cUID];
                 }
                else
                 {
                  un1 = FUnitCodes[cUID];
                  un2 = FUnitCodes[rUID];
                 }
                for (TCodeList::iterator uc = un1->begin(); uc != un1->end(); uc++)
                 {
                  /*BUGFIX list*/
                  TCodeList::iterator RC = un2->find(uc->first);
                  if (RC != un2->end())
                   {
                    if (FDebugCreate)
                     ittsNode->AddChild("c")->AV["v"] = uc->first;
                    elCount++ ;
                   }
                 }
               }
              else
               elCount = 0;
             }
            else if (FUnitCodes[cUID])
             {//����������� �� ��������
              __DBGPrint("  /*- ����������� �� �������� --" + IntToStr((int)FUnitCodes[cUID]->size()) + "----*/");
              elCount = FUnitCodes[cUID]->size();
              if (FDebugCreate)
               {
                TCodeList * un1;
                un1 = FUnitCodes[cUID];
                for (TCodeList::iterator uc = un1->begin(); uc != un1->end(); uc++)
                 ittsNode->AddChild("c")->AV["v"] = uc->first;
               }
             }
            else if (FUnitCodes[rUID])
             {//����������� �� �������
              __DBGPrint("  /*- ����������� �� ������� --" + IntToStr((int)FUnitCodes[rUID]->size()) + "----*/");
              elCount = FUnitCodes[rUID]->size();
              if (FDebugCreate)
               {
                TCodeList * un1;
                un1 = FUnitCodes[rUID];
                for (TCodeList::iterator uc = un1->begin(); uc != un1->end(); uc++)
                 ittsNode->AddChild("c")->AV["v"] = uc->first;
               }
             }
            else
             {
              if (FDebugCreate)
               {
                elCount = 0;
                UnicodeString FdTabKeyFl;
                FdTabKeyFl = FSQLCreator->GetISTabKey(FBaseISRef);
                UnicodeString FSQL = "Select ";
                FSQL += FdTabKeyFl + " From ";
                FSQL += FSQLCreator->GetTabName(FBaseISRef) + " " + FSQLCreator->GetTabAlias(FBaseISRef);
                try
                 {
                  FDM->quExec(FQ, false, FSQL);
                  while (!FQ->Eof)
                   {
                    ittsNode->AddChild("c")->AV["v"] = FQ->FieldByName("CODE")->AsString;
                    elCount++ ;
                    FQ->Next();
                   }
                  //FDM->qtCommit();
                 }
                catch (...)
                 {
                  elCount = 0;
                 }
               }
              else
               {
                UnicodeString FSQL = "Select Count(Distinct ";
                FSQL += FSQLCreator->GetISTabKey(FBaseISRef) + ") as RCOUNT From ";
                FSQL += FSQLCreator->GetTabName(FBaseISRef) + " " + FSQLCreator->GetTabAlias(FBaseISRef);
                try
                 {
                  __DBGPrint(FSQL);
                  if (FDM->quExec(FQ, false, FSQL))
                   elCount = FQ->FieldByName("RCOUNT")->AsInteger;
                  //FDM->qtCommit();
                 }
                catch (...)
                 {
                  elCount = 0;
                 }
               }
             }
            FCells[i][j] = elCount;
           }
         }
       }
      if (FDocCreateType != dctEmpty)
       {
        //��������� ����������� ������
        if (FPrty)
         {//��������� ������ ����� �������
          FCalcTabData(FRowList, FColList->Count, FCells, true);
          FCalcTabData(FColList, FRowList->Count, FCells, false);
         }
        else
         {//��������� ������� ����� ������
          FCalcTabData(FColList, FRowList->Count, FCells, false);
          FCalcTabData(FRowList, FColList->Count, FCells, true);
         }
        //��������� ��������� ����������� ������
       }
      //���������� �� � ��������
      FRows = AParent->AddChild("lv", "uid=" + AParent->NewUID() + ",uidmain=" + ATabDef->AV["UID"]);
      for (int i = 0; i < FRowList->Count; i++)
       {
        FCols = FRows->AddChild("lv", "uid=" + AParent->NewUID());
        for (int j = 0; j < FColList->Count; j++)
         {
          if (FCells[i][j] == (long)FCells[i][j])
           {
            FCols->AddChild("iv")->AV["val"] = FloatToStr(FCells[i][j]);
            if (FDebugCreate)
             tsDataNode->AddChild(IntToStr(i) + "_" + IntToStr(j))->AV["val"] = FloatToStr(FCells[i][j]);
           }
          else
           {
            FCols->AddChild("iv")->AV["val"] = FloatToStrF(FCells[i][j], ffFixed, 18, 3);
            if (FDebugCreate)
             tsDataNode->AddChild(IntToStr(i) + "_" + IntToStr(j))->AV["val"] = FloatToStrF(FCells[i][j], ffFixed, 18, 3);
           }
         }
       }
     }
    else
     {
      LogError(FMT(dsDocDocCreatorErrorMissingColRowDef), __FUNC__);
      return;
     }
   }
  __finally
   {
    if (FCells)
     {
      for (int i = 0; i < FRowList->Count; i++)
       delete[]FCells[i];
      delete[]FCells;
     }
    for (TStrTreeMap::iterator i = FUnitCodes.begin(); i != FUnitCodes.end(); i++)
     {
      if (i->second)
       delete i->second;
      i->second = NULL;
     }
    FUnitCodes.clear();
    if (FColList)
     delete FColList;
    if (FRowList)
     delete FRowList;
   }
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::RecalcTable(TTagNode * ATabDef, TTagNode * AParent)
 {
  LogBegin(__FUNC__);
  TTagNode * FCols, *FRows;

  FCols = ATabDef->GetChildByName("columns");
  FRows = ATabDef->GetChildByName("rows");

  bool FPrty = ATabDef->CmpAV("tablepriority", "row");
  TStringList * FColList = NULL;
  TStringList * FRowList = NULL;
  float * (* FCells) = NULL;
  try
   {
    if (FCols && FRows)
     {//���������� �������� ����� � ��������
      FColList = new TStringList;
      FRowList = new TStringList;
      //������ ������ ����� ��� FBaseISRef �� ������ �������, ������, ������
      GetTabItems(FCols, FColList, NULL, "tcol");
      GetTabItems(FRows, FRowList, NULL, "trow");

      FCells = new float * [FRowList->Count];
      for (int i = 0; i < FRowList->Count; i++)
       FCells[i] = new float[FColList->Count];
      //��������� ������ �� ��������� �������
      LoadTable(ATabDef->AV["uid"], AParent, FCells, FRowList->Count, FColList->Count);

      if (FDocCreateType != dctEmpty)
       {
        //��������� ����������� ������
        if (FPrty)
         {//��������� ������ ����� �������
          FCalcTabData(FRowList, FColList->Count, FCells, true);
          FCalcTabData(FColList, FRowList->Count, FCells, false);
         }
        else
         {//��������� ������� ����� ������
          FCalcTabData(FColList, FRowList->Count, FCells, false);
          FCalcTabData(FRowList, FColList->Count, FCells, true);
         }
        //��������� ��������� ����������� ������
       }
      //���������� �� � ��������
      SaveTable(ATabDef->AV["uid"], AParent, FCells, FRowList->Count, FColList->Count);
     }
    else
     {
      LogError(FMT(dsDocDocCreatorErrorMissingColRowDef), __FUNC__);
      return;
     }
   }
  __finally
   {
    if (FCells)
     {
      for (int i = 0; i < FRowList->Count; i++)
       delete[]FCells[i];
      delete[]FCells;
     }
    if (FColList)
     delete FColList;
    if (FRowList)
     delete FRowList;
   }
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::GetTabItems(TTagNode * AItemList, TStringList * AItems, TTagNode * ARules, UnicodeString AItemType)
 {
  LogBegin(__FUNC__);
  //TTagNode *FRules = MergeCondRules(AItemList,AItemList->GetChildByName("condrules"), ARules, false);
  //TTagNode *FRules = MergeCondRules(AItemList,AItemList->GetChildByName("condrules"), ARules, false);
  TTagNode * itNode = AItemList->GetChildByName(AItemType);
  if (!itNode && AItemList->CmpName(AItemType))
   AItems->AddObject(AItemList->AV["uid"], (TObject *)AItemList);
  else
   {
    while (itNode)
     {
      //GetTabItems(itNode, AItems, FRules, AItemType);
      GetTabItems(itNode, AItems, NULL, AItemType);
      itNode = itNode->GetNext();
     }
   }
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::GetTabSelItems(UnicodeString ABaseISRef, TTagNode * AItemRoot, TStringList * AItems, TTagNode * ARules, UnicodeString AItemType, UnicodeString AItemTypeCyr,
    TTagNode * DebItems)
 {
  LogBegin(__FUNC__);
  TTagNode * FRules, *itNode, *debNode, *debcondNode, *debcodeNode, *debitemsNode;
  TTagNode * tmpCond = NULL;
  UnicodeString FSQL, CountSQL, BaseKey;
  BaseKey = FSQLCreator->GetISTabKey(ABaseISRef);
  if (_UID(BaseKey).Length())
   BaseKey = _UID(BaseKey);
  try
   {
    if (FDebugCreate)
     {
      debNode             = DebItems->AddChild(AItemType);
      debNode->AV["href"] = AItemRoot->AV["uid"];
      debcondNode         = debNode->AddChild("condrules");
      debcodeNode         = debNode->AddChild("codes");
     }

    tmpCond = new TTagNode(NULL);
    tmpCond->AssignEx(MergeCondRules(AItemRoot, AItemRoot->GetChildByName("condrules"), ARules, false), true);
    TTagNode * itNode; // = AItemRoot->GetChildByName(AItemType);
    FUnitCodes[AItemType + "s"] = NULL;
    if (tmpCond->Count)
     {
      if (FDebugCreate)
       debcondNode->AsXML = StringReplace(tmpCond->AsXML, "uid", "repl_uid", fdRepl);
      FUnitCodes[AItemType + "s"] = new TCodeList();
      FSQL                        = FSQLCreator->CreateSQL(ABaseISRef, tmpCond, NULL, "", CountSQL, NULL);
      if (FSQL.Length())
       {
        __DBGPrint("/*-begin----" + AItemTypeCyr + "  (root) " + AItemRoot->AV["uid"] + " -------*/");
        __DBGPrint(FSQL);
        __DBGPrint("/*-end----" + AItemTypeCyr + " (root)  " + AItemRoot->AV["uid"] + " -------*/");
        if (FDM->quExec(FQ, false, FSQL))
         {
          while (!FQ->Eof)
           {
            /*BUGFIX list*/
            (*(FUnitCodes[AItemType + "s"]))[FQ->FieldByName(BaseKey)->AsInteger] = 1;
            if (FDebugCreate)
             debcodeNode->AddChild("c")->AV["v"] = FQ->FieldByName(BaseKey)->AsString;
            FQ->Next();
           }
          //FDM->qtCommit();
         }
       }
     }
    itNode = AItemRoot->GetChildByName(AItemType);
    while (itNode)
     {
      //FSetProgress();
      if (FDebugCreate)
       FGetTabSelItems(ABaseISRef, itNode, AItemType, AItemTypeCyr, FUnitCodes[AItemType + "s"], debNode->AddChild(AItemType));
      else
       FGetTabSelItems(ABaseISRef, itNode, AItemType, AItemTypeCyr, FUnitCodes[AItemType + "s"]);
      itNode = itNode->GetNext();
     }
   }
  __finally
   {
    if (tmpCond)
     delete tmpCond;
   }
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::FGetTabSelItems(UnicodeString ABaseISRef, TTagNode * AItem, UnicodeString AItemType, UnicodeString AItemTypeCyr, TCodeList * AParentCodes, TTagNode * DebItems)
 {
  LogBegin(__FUNC__);
  TTagNode * FRules, *itNode, *debNode, *debcondNode, *debcodeNode, *debitemsNode;
  TTagNode * tmpCond = NULL;
  UnicodeString FSQL, CountSQL, BaseKey, itUID;
  BaseKey = FSQLCreator->GetISTabKey(ABaseISRef);
  itUID   = AItem->AV["uid"];
  if (_UID(BaseKey).Length())
   BaseKey = _UID(BaseKey);
  FSetProgress(TkabProgressType::StageInc, 0, FMT2(dsDocDocCreatorTableCreateCeillMsg, AItemTypeCyr, IntToStr(FRowColNum)));
  FRowColNum++ ;
  try
   {
    if (FDebugCreate)
     {
      debNode             = DebItems->AddChild(AItemType);
      debNode->AV["href"] = AItem->AV["uid"];
      debcondNode         = debNode->AddChild("condrules");
      debcodeNode         = debNode->AddChild("codes");
     }
    FUnitCodes[itUID] = NULL;
    if (AItem->GetChildByName("condrules"))
     {//������� ������ ������
      //������ SQL
      if (FDebugCreate)
       debcondNode->AsXML = StringReplace(AItem->GetChildByName("condrules")->AsXML, "uid", "repl_uid", fdRepl);
      FSQL              = FSQLCreator->CreateSQL(ABaseISRef, AItem->GetChildByName("condrules"), NULL, "", CountSQL, NULL);
      FUnitCodes[itUID] = new TCodeList();
      if (FSQL.Length())
       {//�������� SQL
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        __DBGPrint(FSQL);
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if (FDM->quExec(FQ, false, FSQL))
         {//SQL - ����������
          if (AParentCodes)
           {//������ �������� ���������� �� �������
            if (FQ->RecordCount && AParentCodes->size())
             {
              while (!FQ->Eof)
               {
                /*BUGFIX list*/
                TCodeList::iterator RC = AParentCodes->find(FQ->FieldByName(BaseKey)->AsInteger);
                if (RC != AParentCodes->end())
                 {//������ ���������� � ������ ��������
                  /*BUGFIX list*/
                  (*(FUnitCodes[itUID]))[FQ->FieldByName(BaseKey)->AsInteger] = 1;
                  if (FDebugCreate)
                   debcodeNode->AddChild("c")->AV["v"] = FQ->FieldByName(BaseKey)->AsString;
                 }
                FQ->Next();
               }
             }
           }
          else
           {//������ �������� �� ���������� ( ����� ��� :) )
            if (FQ->RecordCount)
             {
              while (!FQ->Eof)
               {
                /*BUGFIX list*/
                (*(FUnitCodes[itUID]))[FQ->FieldByName(BaseKey)->AsInteger] = 1;
                if (FDebugCreate)
                 debcodeNode->AddChild("c")->AV["v"] = FQ->FieldByName(BaseKey)->AsString;
                FQ->Next();
               }
             }
           }
          //FDM->qtCommit();
         }
       }
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      __DBGPrint("/*-end----" + AItemTypeCyr + "-" + IntToStr(FRowColNum - 1) + "-- " + AItem->AV["uid"] + "-------*/");
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     }
    else
     {
      if (AParentCodes)
       {//������ �������� ���������� �� �������
        FUnitCodes[itUID] = new TCodeList();
        /*BUGFIX list*/
        (*(FUnitCodes[itUID])) = TCodeList((*AParentCodes));
        if (FDebugCreate)
         {
          if (debcodeNode->GetParent())
           if (debcodeNode->GetParent()->GetChildByName("codes"))
            debcodeNode->Assign(debcodeNode->GetParent()->GetChildByName("codes"), true);
         }
        //for (TCodeList::iterator i = AParentCodes->begin(); i != AParentCodes->end(); i++ )
        //FUnitCodes[itUID]->push_back((*i));
       }
     }
    itNode = AItem->GetChildByName(AItemType);
    while (itNode)
     {
      //FSetProgress();
      if (FDebugCreate)
       FGetTabSelItems(ABaseISRef, itNode, AItemType, AItemTypeCyr, FUnitCodes[itUID], debNode->AddChild(AItemType));
      else
       FGetTabSelItems(ABaseISRef, itNode, AItemType, AItemTypeCyr, FUnitCodes[itUID]);
      itNode = itNode->GetNext();
     }
   }
  __finally
   {
    if (tmpCond)
     delete tmpCond;
   }
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::FCalcTabData(TStringList * AItems, int ACount, float ** ATab, bool ARow)
 {
  LogBegin(__FUNC__);
  TTagNode * FCalcNode;
  for (int i = 0; i < AItems->Count; i++)
   {//if ARow i - rows Else i - columns
    FCalcNode = ((TTagNode *)AItems->Objects[i])->GetChildByName("calcrules");
    if (FCalcNode)
     {//���������� ������� ����������
      for (int j = 0; j < ACount; j++)
       {//if ARow j - columns Else j - rows
        try
         {
          if (ARow)
           ATab[i][j] = GetCellCalcData(FCalcNode->GetChildByName("calc"), ATab, true, j);
          else
           ATab[j][i] = GetCellCalcData(FCalcNode->GetChildByName("calc"), ATab, false, j);
         }
        catch (...)
         {
          if (ARow)
           LogError(cFMT4(dsDocDocCreatorErrorCeilCalc,
              IntToStr(i + 1),
              IntToStr(j + 1),
              IntToStr(AItems->Count),
              IntToStr(ACount)), __FUNC__);
          else
           LogError(cFMT4(dsDocDocCreatorErrorCeilCalc,
              IntToStr(j + 1),
              IntToStr(i + 1),
              IntToStr(ACount),
              IntToStr(AItems->Count)), __FUNC__);
         }
       }
     }
   }
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
long __fastcall TdsDocCreator::GetCellCondData(UnicodeString ABaseISRef, TTagNode * ARow, TTagNode * ACol, TTagNode * ACond)
 {
  LogBegin(__FUNC__);
  TTime FBeg = Time();
  TTagNode * FCondR, *FCondC, *tmpCond;
  UnicodeString FSQL;
  long RC = 0;
  tmpCond = NULL;
  try
   {
    FCondR = ARow->GetChildByName("condrules");
    FCondC = ACol->GetChildByName("condrules");
    if (FCondR || FCondC || ACond)
     {
      tmpCond = new TTagNode(NULL);
      if (ACond)
       {
        tmpCond->AssignEx(ACond, true);
        tmpCond = MergeCondRules(NULL, tmpCond, FCondR, false);
        tmpCond = MergeCondRules(NULL, tmpCond, FCondC, false);
        //tmpCond = MergeCondRules(NULL,tmpCond, FCondR, true);
        //tmpCond = MergeCondRules(NULL,tmpCond, FCondC, true);
       }
      else if (FCondR)
       {
        tmpCond->AssignEx(FCondR, true);
        tmpCond = MergeCondRules(NULL, tmpCond, FCondC, false);
        //tmpCond = MergeCondRules(NULL,tmpCond, FCondC, true);
       }
      else
       tmpCond->AssignEx(FCondC, true);
      if (tmpCond->Count)
       FSQLCreator->CreateSQL(ABaseISRef, tmpCond, NULL, "", FSQL, NULL);
      else
       FSQLCreator->CreateSQL(ABaseISRef, NULL, NULL, "", FSQL, NULL);
     }
    else
     FSQLCreator->CreateSQL(ABaseISRef, NULL, NULL, "", FSQL, NULL);
    if (FSQL.Length())
     {
      if (FDM->quExec(FQ, false, FSQL))
       {
        RC = FQ->FieldByName("RCOUNT")->AsInteger;
        LogMessage((FBeg - Time()).FormatString("nn.ss.zzz") + "; SQL = " + FSQL, "", 5);
       }
     }
   }
  __finally
   {
    if (tmpCond)
     delete tmpCond;
   }
  LogEnd(__FUNC__);
  return RC;
 }
//---------------------------------------------------------------------------
float __fastcall TdsDocCreator::GetCellCalcData(TTagNode * ACalcNode, float ** ATab, bool ARow, int AInd)
 {
  LogBegin(__FUNC__);
  float RC = 0;
  try
   {
    TReplaceFlags rFlag;
    rFlag << rfReplaceAll;
    UnicodeString Expression = ACalcNode->AV["pol_exp"];
    TStringList * ExpList = new TStringList;
    ExpList->Text = StringReplace(Expression, " ", "\n", rFlag);
    UnicodeString tmpOper;
    int ind;
    wchar_t * cBuff = new wchar_t[80]; //!!!
    memset(cBuff, 0, 80);
    try
     {
      for (int i = 0; i < ExpList->Count; i++)
       {
        tmpOper = ExpList->Strings[i];
        if (tmpOper[1] == '#')
         {//������ �� �������� ������ ������� ��� ������ AInd
          if (tmpOper.LastDelimiter(":"))
           {
            UnicodeString sRow = GetPart1(tmpOper, ':');
            UnicodeString sCol = GetPart2(tmpOper, ':');
            sRow = sRow.SubString(2, sRow.Length() - 1);
            swprintf(cBuff, L"%f", ATab[sRow.ToInt() - 1][sCol.ToInt() - 1]);
           }
          else
           {
            ind = tmpOper.SubString(2, tmpOper.Length() - 1).ToInt() - 1;
            if (ARow)
             swprintf(cBuff, L"%f", ATab[ind][AInd]);
            else
             swprintf(cBuff, L"%f", ATab[AInd][ind]);
           }
          ExpList->Strings[i] = UnicodeString(cBuff);
         }
       }
     }
    __finally
     {
      delete[]cBuff;
     }
    try
     {
      RC = (float) CalcExpression(ExpList);
      //if (SSS == abs(SSS)) return SSS;
      //else                 return 0;
     }
    catch (EZeroDivide & E)
     {
      RC = 0;
     }
   }
  __finally
   {
    LogEnd(__FUNC__);
   }
  return RC;
 }
//---------------------------------------------------------------------------
//##############################################################################
//#                         ��������������� ���������                          #
//##############################################################################
TTagNode * __fastcall TdsDocCreator::MergeCondRules(TTagNode * AParent, TTagNode * ARules1, TTagNode * ARules2, bool AORType)
 {
  LogBegin(__FUNC__);
  TTagNode * RC = NULL;
  try
   {
    if (ARules1 && ARules2)
     {
      RC = FMergeCondRules(ARules1, ARules2, AORType);
     }
    else if (ARules1)
     RC = ARules1;
    else if (AParent && ARules2)
     {
      if (AParent == ARules2)
       RC = ARules2;
      else
       {
        TTagNode * tmp = AParent->AddChild("tmp");
        tmp->AssignEx(ARules2, true);
        RC = tmp;
       }
     }
    LogEnd(__FUNC__);
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
TTagNode * __fastcall TdsDocCreator::FMergeCondRules(TTagNode * ARules1, TTagNode * ARules2, bool AORType)
 {
  LogBegin(__FUNC__);
  if (!(ARules1 && ARules2))
   return NULL;
  TTagNode * FRules = NULL;
  try
   {
    TTagNode * tmp;
    FRules                  = new TTagNode(NULL);
    FRules->Name            = "condrules";
    FRules->AV["uniontype"] = (AORType) ? "or" : "and";
    //��������� ������� �� ARules1
    //ARules1 - �� ���� "condrules" ����������� � "ROR" ��� "RAND"
    //� ����������� �� �������� �������� "uniontype"
    if (ARules1->Count)
     {
      tmp = FRules->AddChild("tmp");
      tmp->AssignEx(ARules1, true);
      tmp->Name          = "r" + ARules1->GetAVDef("uniontype", "or");
      tmp->AV["not"]     = "0";
      tmp->AV["comment"] = "";
      tmp->AV["pure"]    = "1";
      tmp->DeleteAttr("uniontype");
     }
    //��������� ������� �� ARules2
    //ARules2 - �� ���� "condrules" ����������� � "ROR" ��� "RAND"
    //� ����������� �� �������� �������� "uniontype"
    if (ARules2->Count)
     {
      tmp = FRules->AddChild("tmp");
      tmp->AssignEx(ARules2, true);
      tmp->Name          = "r" + ARules2->GetAVDef("uniontype", "or");
      tmp->AV["not"]     = "0";
      tmp->AV["comment"] = "";
      tmp->AV["pure"]    = "1";
      tmp->DeleteAttr("uniontype");
     }
    //������������ � ARules1
    if (FRules->Count)
     ARules1->AssignEx(FRules, true);
   }
  __finally
   {
    if (FRules)
     delete FRules;
   }
  LogEnd(__FUNC__);
  return ARules1;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDocCreator::FGetDocItemByRef(TTagNode * ANode, UnicodeString & ARef)
 {
  if (ANode->CmpAV("ref", ARef))
   FSpecRefNodes[IntToStr(FSpecRefNodesCount++)] = ANode;
  return false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::MergeSpecTabCondRules(TTagNode * ASpec, TTagNode * ATabCRList)
 {
  TTagNode * DC = ASpec->GetChildByName("doccontent", true);
  TTagNode * itDC = DC->GetFirstChild();
  TTagNode * RefNode;
  TTagNode * itTabCR = ATabCRList->GetFirstChild();
  UnicodeString FRef;
  while (itTabCR)
   {
    //<list repl_uid='0009' repl_uidmain='' ref='0E291426-00005882-2493.0000' showzval='1' border='1' rownumbers='1' hstyle='' bstyle='' width='100%'>
    //<table repl_uid='0009' repl_uidmain='' ref='0E291426-00005882-2493.0000' showzval='1' border='1' rownumbers='1' hstyle='' bstyle='' width='100%'>
    //<link_is ref='0E291426-00005882-2493.007A' union='0'>
    FSpecRefNodes.clear();
    FSpecRefNodesCount = 0;
    FRef               = itTabCR->AV["ref"];
    ASpec->Iterate(FGetDocItemByRef, FRef);
    for (TTagNodeMap::iterator i = FSpecRefNodes.begin(); i != FSpecRefNodes.end(); i++)
     {
      RefNode = i->second;
      if (itTabCR->CmpAV("mr", "merge"))
       MergeCondRules(RefNode, RefNode->GetChildByName("condrules"), itTabCR, false);
      else if (itTabCR->CmpAV("mr", "replace"))
       {
        TTagNode * srcCR = RefNode->GetChildByName("condrules");
        if (!srcCR)
         srcCR = RefNode->AddChild("tmp");
        srcCR->AssignEx(itTabCR, true);
       }
     }
    itTabCR = itTabCR->GetNext();
   }
  /*
   if (DC)
   {
   TTagNode * itDC = DC->GetFirstChild();
   while (itDC && itTabCR)
   {
   if (!itDC->CmpName("inscription"))
   {//�������� ������� ��� ������
   if (itTabCR)
   {
   if (!itTabCR->CmpAV("mr", "skip"))
   {
   if (itTabCR->CmpAV("mr", "merge"))
   MergeCondRules(itDC, itDC->GetChildByName("condrules"), itTabCR, false);
   else if (itTabCR->CmpAV("mr", "replace"))
   {
   TTagNode * srcCR = itDC->GetChildByName("condrules");
   if (!srcCR)
   srcCR = itDC->AddChild("tmp");
   srcCR->AssignEx(itTabCR, true);
   }
   }
   itTabCR = itTabCR->GetNext();
   }
   }
   itDC = itDC->GetNext();
   }
   }
   */
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDocCreator::CreateDocFromDoc(TTagNode * ASpec, TTagNode * ADoc)
 {
  LogBegin(__FUNC__);
  TTagNode * FSteps;
  TTagNode * FRules = NULL;
  TTagNode * intSpec = NULL;
  TTagNode * intDoc = NULL;
  TTagNode * __tmp;
  TList * intDocList = NULL;
  TStringList * intDocListCount = NULL;
  try
   {
    FSteps          = ASpec->GetChildByName("docrules");
    FRules          = new TTagNode(NULL);
    intSpec         = new TTagNode(NULL);
    intDoc          = new TTagNode(NULL);
    intDocList      = new TList;
    intDocListCount = new TStringList;
    if (FSteps)
     {//������� ������������ ����������
      FSteps = FSteps->GetFirstChild();
      int FCurStep = 0;
      while (FSteps)
       {//���� �� "createstep"
        FCurStep++ ;
        if (FDM->GetSpecById(FSteps->AV["docref"], intSpec))
         {//���������� �������� �������������� ���������
          //�������� ������ ����������
          FDM->LoadDocs(FSteps, ADoc, intDocList);
          if (intDocList->Count == 0)
           {
            UnicodeString MSpecName, MPeriod, MPeriodicity, MOrgName;
            MPeriod      = "";
            MPeriodicity = "";
            MOrgName     = "";
            UnicodeString thisDocPer = GetDocPeriod(ADoc);
            UnicodeString thisDocPerFrom = GetDocPeriodValFrom(ADoc);
            UnicodeString thisDocPerTo = GetDocPeriodValTo(ADoc);

            UnicodeString intPeriodtype = FSteps->GetAVDef("periodtype", "this"); //(this|defined_value|abstruct_value)
            UnicodeString intPerioddef = FSteps->GetAVDef("perioddef", "undef"); //(year|halfyear|quarter|month|unique|undef)
            UnicodeString intPeriodvalue = FSteps->AV["periodvalue"];
            UnicodeString intDocPerFrom = GetPart1(intPeriodvalue, '-').Trim();
            UnicodeString intDocPerTo = GetPart2(intPeriodvalue, '-').Trim();

            UnicodeString intPeriodicity = FSteps->GetAVDef("periodicity", "equal").LowerCase(); //(year|halfyear|quarter|month|unique|no|equal|any)
            UnicodeString intOrgType = FSteps->GetAVDef("org_type", "child"); //(defined|child|this)
            UnicodeString intOrgCode = FSteps->AV["org_code"];

            //������ ������������� ����������
            if (intPeriodtype.LowerCase() == "this")
             {//������ ��������� � �������� ������������ ���������
              if (thisDocPerFrom.Length())
               MPeriod += " " + FMT(dsDocDocCreatorPeroidFromCaption) + " '" + DateFS(TDateTime(thisDocPerFrom)) + "'";
              if (thisDocPerTo.Length())
               MPeriod += " " + FMT(dsDocDocCreatorPeroidTo1Caption) + " '" + DateFS(TDateTime(thisDocPerTo)) + "'";
             }
            else if (intPeriodtype.LowerCase() == "defined_value")
             {//����������� �������� �������
              if (intPerioddef.LowerCase() != "undef")
               {
                if (intDocPerFrom.Length())
                 MPeriod += " " + FMT(dsDocDocCreatorPeroidFromCaption) + " '" + DateFS(TDateTime(intDocPerFrom)) + "'";
                if (intDocPerTo.Length())
                 MPeriod += " " + FMT(dsDocDocCreatorPeroidTo1Caption) + " '" + DateFS(TDateTime(intDocPerTo)) + "'";
               }
              else
               MPeriod += FMT(dsDocDocCreatorUndefPeroidCaption);
             }
            else if (intPeriodtype.LowerCase() == "abstruct_value")
             {//������������� �������� �������, ������������ ���� ������������
              MPeriod += " " + intPeriodvalue + " " + FMT(dsDocDocCreatorPeroidTo2Caption) + " '" + DateFS(Date()) + "'";
             }
            //������������� ������������� ����������
            if (intPeriodicity == "equal")
             MPeriodicity = FMT(dsDocDocCreatorPeriodicityEqual);
            else if (intPeriodicity == "year")
             MPeriodicity = FMT(dsDocDocCreatorPeriodicityYear);
            else if (intPeriodicity == "halfyear")
             MPeriodicity = FMT(dsDocDocCreatorPeriodicityHalfYear);
            else if (intPeriodicity == "quarter")
             MPeriodicity = FMT(dsDocDocCreatorPeriodicityQuarter);
            else if (intPeriodicity == "month")
             MPeriodicity = FMT(dsDocDocCreatorPeriodicityMonth);
            else if (intPeriodicity == "unique")
             MPeriodicity = FMT(dsDocDocCreatorPeriodicityUnique);
            else if (intPeriodicity == "no")
             MPeriodicity = FMT(dsDocDocCreatorPeriodicityNo);
            else if (intPeriodicity == "any")
             MPeriodicity = FMT(dsDocDocCreatorPeriodicityAny);

            //����������� ������ ������������� ����������
            if (intOrgType.LowerCase() == "defined")
             MOrgName = GetOwnerName(intOrgCode);
            else if (intOrgType.LowerCase() == "this")
             MOrgName = FMT(dsDocDocCreatorAuthorThis);
            else if (intOrgType.LowerCase() == "child")
             MOrgName = FMT(dsDocDocCreatorAuthorAllChild);

            LogError(FMT5(dsDocDocCreatorErrorNoIntDocs,
                IntToStr(FCurStep),
                FDM->GetSpecNameByRef(FSteps->AV["docref"]),
                MPeriod,
                MPeriodicity,
                MOrgName), __FUNC__);
            FSteps = FSteps->GetNext();
            continue;
           }
          FRules = FSteps->GetFirstChild();
          while (FRules)
           {
            if (FRules->CmpName("insrules"))
             {//���������� ��������
             }
            else if (FRules->CmpName("listrules"))
             {//���������� �������
             }
            else if (FRules->CmpName("tabrules"))
             {//���������� ������
              TTagNode * intTabSpec = NULL;
              TTagNode * TabSpec = NULL;
              int XFRCount, FRCount, FCCount;
              int intRCount, intCCount;
              float * (* FTab) = NULL;
              bool Cross = !FRules->CmpAV("crosstype", "normal");
              float * (** intTab) = NULL;
              try
               {
                //��������� ������� � ������� ����� ����������
                FRCount = 0;
                FCCount = 0;
                TabSpec = FRules->GetTagByUID(FRules->AV["thisref"]);
                DoLoadTable(TabSpec, FRCount, FCCount);
                XFRCount = FRCount;
                if (FCCount && FRCount)
                 {
                  FTab = new float * [FRCount];
                  for (int i = 0; i < FRCount; i++)
                   FTab[i] = new float[FCCount];
                  LoadTable(TabSpec->AV["uid"], ADoc, FTab, FRCount, FCCount);
                 }
                else
                 FTab = NULL;
                intTabSpec = intSpec->GetTagByUID(_UID(FRules->AV["ref"]));
                if (intTabSpec && FTab)
                 {//�������� ������������� ������� ����������
                  intTab = new float * *[intDocList->Count];
                  memset(intTab, 0, intDocList->Count);
                  //��������� ������� �� ������� ����� ����������
                  for (int i = 0; i < intDocList->Count; i++)
                   {
                    intRCount = 0;
                    intCCount = 0;
                    DoLoadTable(intTabSpec, intRCount, intCCount);
                    intDocListCount->Add((IntToStr(intRCount) + "." + IntToStr(intCCount)));
                    if (intCCount && intRCount)
                     {
                      intTab[i] = new float * [intRCount];
                      for (int j = 0; j < intRCount; j++)
                       intTab[i][j] = new float[intCCount];
                      LoadTable(intTabSpec->AV["uid"], (TTagNode *)intDocList->Items[i], intTab[i], intRCount, intCCount);
                     }
                    else
                     intTab[i] = NULL;
                   }
                  TTagNode * FTabCol, *FTabRow;
                  TTagNode * TabSpecRows, *TabSpecCols;
                  TTagNode * intTabSpecRows, *intTabSpecCols;
                  TabSpecRows    = TabSpec->GetChildByName("rows");
                  TabSpecCols    = TabSpec->GetChildByName("columns");
                  intTabSpecRows = intTabSpec->GetChildByName("rows");
                  intTabSpecCols = intTabSpec->GetChildByName("columns");
                  FTabCol        = FRules->GetChildByName("colrules")->GetFirstChild(); //������ �������
                  FTabRow        = FRules->GetChildByName("rowrules")->GetFirstChild(); //������ ������
                  int RIdx, CIdx, intRIdx, intCIdx, tmpVal;
                  while (FTabRow)
                   {
                    RIdx = GetTabIndex(FTabRow->AV["thisref"], TabSpecRows);
                    if (Cross)
                     intCIdx = GetTabIndex(_UID(FTabRow->AV["ref"]), intTabSpecCols);
                    else
                     intRIdx = GetTabIndex(_UID(FTabRow->AV["ref"]), intTabSpecRows);
                    __tmp = FTabCol;
                    while (__tmp)
                     {
                      CIdx = GetTabIndex(__tmp->AV["thisref"], TabSpecCols);
                      if (Cross)
                       intRIdx = GetTabIndex(_UID(__tmp->AV["ref"]), intTabSpecRows);
                      else
                       intCIdx = GetTabIndex(_UID(__tmp->AV["ref"]), intTabSpecCols);
                      tmpVal = 0;
                      for (int i = 0; i < intDocList->Count; i++)
                       {
                        if (intTab[i])
                         tmpVal += intTab[i][intRIdx][intCIdx];
                       }
                      FTab[RIdx][CIdx] = FTab[RIdx][CIdx] + tmpVal; //������������ 31.01.2007
                      __tmp = __tmp->GetNext();
                     }
                    FTabRow = FTabRow->GetNext();
                   }
                 }
               }
              __finally
               {
                if (FTab)
                 SaveTable(FRules->AV["thisref"], ADoc, FTab, FRCount, FCCount);
                if (intTab)
                 {
                  if (intDocList)
                   {
                    for (int i = 0; i < intDocList->Count; i++)
                     {
                      if (intTab[i])
                       {
                        for (int j = 0; j < _GUI(intDocListCount->Strings[i]).ToInt(); j++)
                         delete[]intTab[i][j];
                        delete[]intTab[i];
                       }
                     }
                   }
                  intDocList->Clear();
                  intDocListCount->Clear();
                  delete[]intTab;
                 }
                if (FTab)
                 {
                  for (int i = 0; i < XFRCount; i++)
                   delete[]FTab[i];
                  delete[]FTab;
                 }
                FTab = NULL;
                intTab = NULL;
               }
             }
            FRules = FRules->GetNext();
           }
         }
        FSteps = FSteps->GetNext();
       }
     }
    TTagNode * itTable = ASpec->GetChildByName("table", true);
    while (itTable)
     {
      if (itTable->CmpName("table"))
       {
        RecalcTable(itTable, ADoc);
       }
      itTable = itTable->GetNext();
     }
   }
  __finally
   {
    if (intSpec)
     delete intSpec;
    if (intDoc)
     delete intDoc;
    if (intDocList)
     {
      for (int i = 0; i < intDocList->Count; i++)
       delete((TTagNode *)intDocList->Items[i]);
      delete intDocList;
     }
    if (intDocListCount)
     delete intDocListCount;
    if (FRules)
     delete FRules;
   }
  LogEnd(__FUNC__);
  return true;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::DoLoadTable(TTagNode * ATab, int & FRCount, int & FCCount)
 {//������ ���������� ����� � �������� ��� ATab
  LogBegin(__FUNC__);
  UnicodeString tmp;
  tmp = "0";
  if (ATab->GetChildByName("columns"))
   ATab->GetChildByName("columns")->Iterate(FGetColCount, tmp);
  FCCount = tmp.ToInt();
  tmp     = "0";
  if (ATab->GetChildByName("rows"))
   ATab->GetChildByName("rows")->Iterate(FGetRowCount, tmp);
  FRCount = tmp.ToInt();
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::LoadTable(UnicodeString ATabUID, TTagNode * ADocTab, float ** FTab, int FRCount, int FCCount)
 {
  LogBegin(__FUNC__);
  UnicodeString tmp;
  if (FCCount && FRCount)
   {
    TTagNode * tmpTab = ADocTab->GetChildByName("contens");
    if (tmpTab)
     tmpTab = tmpTab->GetChildByAV("lv", "uidmain", ATabUID);
    else
     return;
    if (tmpTab)
     {
      TTagNode * tmpRow, *tmpCol;
      int cInd, rInd;
      tmpRow = tmpTab->GetFirstChild();
      rInd   = 0;
      while (tmpRow)
       {
        tmpCol = tmpRow->GetFirstChild();
        cInd   = 0;
        while (tmpCol)
         {
          try
           {
            FTab[rInd][cInd] = icsStrToFloat(tmpCol->AV["val"]);
           }
          catch (EConvertError & E)
           {
            FTab[rInd][cInd] = 0;
           }
          cInd++ ;
          tmpCol = tmpCol->GetNext();
         }
        rInd++ ;
        tmpRow = tmpRow->GetNext();
       }
     }
    else
     FTab = NULL;
   }
  else
   FTab = NULL;
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::SaveTable(UnicodeString ATabUID, TTagNode * ADocTab, float ** FTab, int FRCount, int FCCount)
 {
  LogBegin(__FUNC__);
  UnicodeString tmp;
  if (FCCount && FRCount)
   {
    TTagNode * tmpTab = ADocTab->GetChildByAV("lv", "uidmain", ATabUID, true);
    if (tmpTab)
     {
      TTagNode * tmpRow, *tmpCol;
      int cInd, rInd;
      rInd   = 0;
      tmpRow = tmpTab->GetFirstChild();
      while (tmpRow)
       {
        tmpCol = tmpRow->GetFirstChild();
        cInd   = 0;
        while (tmpCol)
         {
          tmpCol->AV["val"] = FloatToStr(FTab[rInd][cInd]);
          cInd++ ;
          tmpCol = tmpCol->GetNext();
         }
        rInd++ ;
        tmpRow = tmpRow->GetNext();
       }
     }
   }
  LogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
int __fastcall TdsDocCreator::GetTabIndex(UnicodeString AItemUID, TTagNode * ATabItemsSpec)
 {
  LogBegin(__FUNC__);
  int RC = 0;
  try
   {
    if (ATabItemsSpec->CmpName("rows,columns"))
     {
      UnicodeString FName = (ATabItemsSpec->CmpName("rows")) ? "trow" : "tcol";
      UnicodeString FRName = (ATabItemsSpec->CmpName("rows")) ? "������" : "�������";
      UnicodeString tmp = AItemUID + "=" + FName + ".0";
      if (ATabItemsSpec->Iterate(FGetItemsCount, tmp))
       RC = _UID(tmp).ToInt();
      else
       {
        LogError(FMT2(dsDocDocCreatorErrorRowColReferNotValid, FRName, AItemUID), __FUNC__);
       }
     }
    else
     {
      LogError(FMT(dsDocDocCreatorErrorCreateRowColNamesNotValid), __FUNC__);
     }
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDocCreator::FGetItemsCount(TTagNode * itTag, UnicodeString & ACount)
 {
  //������ ACount -> XXX=YYY.CCC
  //���              XXX - uid �������� ��� �������� ���� ������
  //YYY - ��� ���� (tcol | trow)
  //CCC - ������
  LogBegin(__FUNC__);
  UnicodeString TagName = _GUI(GetPart2(ACount, '='));
  UnicodeString TagCount = _UID(ACount);
  UnicodeString TagUID = _UID(GetPart1(ACount, '='));
  if (itTag->CmpName(TagName) && (itTag->GetCount(false, TagName) == 0))
   {
    if (itTag->CmpAV("uid", TagUID))
     return true;
    ACount = TagUID + "=" + TagName + "." + IntToStr(TagCount.ToInt() + 1);
   }
  LogEnd(__FUNC__);
  return false;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDocCreator::FGetColCount(TTagNode * itTag, UnicodeString & ACount)
 {
  LogBegin(__FUNC__);
  if (itTag->CmpName("tcol") && (itTag->GetCount(false, "tcol") == 0))
   ACount = IntToStr(ACount.ToInt() + 1);
  LogEnd(__FUNC__);
  return false;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDocCreator::FGetRowCount(TTagNode * itTag, UnicodeString & ACount)
 {
  LogBegin(__FUNC__);
  if (itTag->CmpName("trow") && (itTag->GetCount(false, "trow") == 0))
   ACount = IntToStr(ACount.ToInt() + 1);
  LogEnd(__FUNC__);
  return false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::__DBGPrint(UnicodeString AMessage)
 {
  LogMessage(AMessage, "", 5);
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocCreator::FGetFilterValues(TTagNode * ADocCondRules)
 {
  UnicodeString RC = "";
  if (ADocCondRules)
   {
    ADocCondRules->Iterate(FGetConValues, RC);
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDocCreator::FGetConValues(TTagNode * itTag, UnicodeString & ARC)
 {
  LogBegin(__FUNC__);
  ARC += GetCondValues(itTag, true);
  LogEnd(__FUNC__);
  return false;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocCreator::GetCondValues(TTagNode * AIENode, bool ACheckInclToName)
 {
  LogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    bool FCont = true;
    if (ACheckInclToName)
     FCont = AIENode->CmpAV("incltoname", "1");
    if (AIENode->CmpName("IERef") && FCont)
     {
      UnicodeString RCVal = "";
      UnicodeString IEVal = "";
      TTagNode * FISAttr = FDM->XMLList->GetRefer(AIENode->AV["ref"]);
      if (FISAttr && FOnGetDVal)
       {
        short CallRC = false;
        UnicodeString _ProcName_ = "Def" + FISAttr->AV["procname"];
        try
         {
          CallRC = FOnGetDVal(_ProcName_, AIENode, RCVal, IEVal);
          //!!! ��������� CallRC
         }
        __finally
         {
         }
       }
      if (RC.Length())
       RC += "; ";
      RC += RCVal.Trim();
     }
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
  return RC.Trim();
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocCreator::GetOwnerName(UnicodeString AOwnerCode, UnicodeString AFieldName)
 {
  LogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    FDM->quExecParam(FDQ, false, "Select " + AFieldName + " From SUBORDORG Where Upper(CODE)=:pCode", "pCode", AOwnerCode.UpperCase());
    if (FDQ->RecordCount)
     RC = FDQ->FieldByName(AFieldName)->AsString;
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::SyncCallOnProgress()
 {
  if (FOnProgress)
   FOnProgress(FPrType, FPrVal, FPrMsg);
 };
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::FSetProgress(TkabProgressType AType, int AVal, UnicodeString AMsg)
 {
  FPrType = AType;
  FPrVal  = AVal;
  FPrMsg  = AMsg;
  SyncCall(& SyncCallOnProgress);
  __DBGPrint("/* " + AMsg + " */");
 };
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocCreator::DateFS(TDateTime ADate)
 {
  UnicodeString RC = ADate.FormatString("dd.mm.yyyy");
  try
   {
    /*if (FOnGetDateStringFormat)
     {
     FOnGetDateStringFormat(RC);
     RC = ADate.FormatString(RC);
     }*/
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
TdsDocGetSVal __fastcall TdsDocCreator::FGetOnGetSVal()
 {
  TdsDocGetSVal RC = NULL;
  try
   {
    if (FSQLCreator)
     RC = FSQLCreator->OnGetSVal;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocCreator::FSetOnGetSVal(TdsDocGetSVal AVal)
 {
  if (FSQLCreator)
   FSQLCreator->OnGetSVal = AVal;
 }
//---------------------------------------------------------------------------
