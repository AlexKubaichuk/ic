﻿#ifndef DsdocsrvconstH
#define DsdocsrvconstH

#include <System.hpp>	// Pascal unit

namespace Dsdocsrvconst
{
  #define dsDocCaption  "Отмена"
  #define dsDocErrorTagEq  "Параметр должен соответствовать тегу \"%s\"."
  #define dsDocErrorXMLContainerNotDefined  "Не указан xml-контейнер."
  #define dsDocErrorDocNotDefined  "Неоходимо указать документ."
  #define dsDocErrorGUINotValid  "ГУИ спецификатора указанного документа не совпадает с ГУИ указанного спецификатора"
  #define dsDocErrorCommonDop  "Код ошибки - %s,\r\nсообщение - \"%s\"."
  #define dsDocErrorCreateOwnerNotValid    "Владелец должен быть потомком TdsDocSpec."
  #define dsDocErrorInputErrorCaption   "Ошибка ввода"
  #define dsDocErrorReqFieldCaption   "Поле \"%s\" должно быть заполнено."
  #define dsDocExExtFuncErrorCreateDocView   "Ошибка формирования просмотра (отсутствует описание документа)"
  #define dsDocDocSpecListCopyCaption  "копия"

//SQLCreator
  #define dsDocSQLCreatorErrorCondStructure   "Ошибка в структуре условий отбора"
  #define dsDocSQLCreatorErrorFuncMissing   "Вызов функции \"%s\" системное сообщение: %s"

// BaseEdit
  #define dsDocBaseEditCommonTBCaption  "Общие"
  #define dsDocBaseEditEditTBCaption  "Правка"
  #define dsDocBaseEditErrorSetDomainNum  "Номер домена должен начинаться с %s."
  #define dsDocBaseEditErrorGetNom  "Не найден номенклатор (GUI = %s)."
  #define dsDocBaseEditErrorGetDomain  "Домен с номером \"%s\" в номенклаторе не найден."
  #define dsDocBaseEditErrorNoXML  "Отсутствует xml."
  #define dsDocBaseEditErrorGenUID  "Генерация uid невозможна. Не установлено ни одно из следующих свойств: ParentXML, XML."

// Globals
//  #define dsDocGlobals  ""

//Filter
  #define dsDocFilterCaption  "Редактор фильтра"
  #define dsDocFilterFilterTBCaption  "Фильтр"

  #define dsDocFilterPassportCaption        "&Параметры"
  #define dsDocFilterPassportHint           "Параметры"
  #define dsDocFilterCustomLoadSaveCaption  "&Загрузить / сохранить"
  #define dsDocFilterCustomLoadSaveHint     "Загрузить / сохранить"
  #define dsDocFilterCustomLoadCaption      "Заг&рузить"
  #define dsDocFilterCustomLoadHint         "Загрузить"
  #define dsDocFilterORCaption              "И&ЛИ"
  #define dsDocFilterORHint                 "Добавить группу условий объединённных по {ИЛИ}"
  #define dsDocFilterANDCaption             "&И"
  #define dsDocFilterANDHint                "Добавить группу условий объединённных по {И}"
  #define dsDocFilterLogicNOTCaption        "ИЛИ&/И"
  #define dsDocFilterLogicNOTHint           "Сменить тип группы условий"
  #define dsDocFilterNOTCaption             "&НЕ"
  #define dsDocFilterNOTHint                "Отрицание значения условия"
  #define dsDocFilterAddCondCaption         "&Добавить условие"
  #define dsDocFilterAddCondHint            "Добавить условие"
  #define dsDocFilterDeleteCaption          "&Удалить"
  #define dsDocFilterDeleteHint             "Удалить"
  #define dsDocFilterKonkrCaption           "&Определить значение ..."
  #define dsDocFilterKonkrHint              "Определить значение ..."
  #define dsDocFilterDeKonkrCaption         "Вернуть к н&е определённому"
  #define dsDocFilterDeKonkrHint            "Вернуть к не определённому"
  #define dsDocFilterCommentCaption         "&Комментарий"
  #define dsDocFilterCommentHint            "Комментарий"
  #define dsDocFilterCheckCaption           "Про&верить"
  #define dsDocFilterCheckHint              "Проверить"

  #define dsDocFilterXMLTitle  "Фильтр"
  #define dsDocFilterErrorSetIsFilter  "Нельзя изменять признак фильтра для созданного фильтра."
  #define dsDocFilterErrorLoadNomDLL  "Ошибка загрузки \"nom_dll.dll\". Код ошибки - %s, сообщение - \"%s\"."
  #define dsDocFilterErrorFreeNomDLL  "Ошибка выгрузки \"nom_dll.dll\". Код ошибки - %s, сообщение - \"%s\"."
  #define dsDocFilterErrorCallProc  "%s - ошибка вызова \"%s\" из \"nom_dll.dll\"."
  #define dsDocFilterErrorCaption  "ОШИБКА"
  #define dsDocFilterConformCaption  "соответствующих"
  #define dsDocFilterCheckOkCaption  "Фильтр корректен"
  #define dsDocFilterCheckEmptyCaption  "Фильтр пустой"
  #define dsDocFilterCheckNoBodyCaption  "Фильтр отсутствует"

  #define dsDocFilterNewCaption  "Новый фильтр"
  #define dsDocFilterNewAuthorCaption  "Неизвестен"
  #define dsDocFilterErrorEmptyGroup  "Наличие пустых групп условий недопустимо."
  #define dsDocFilterErrorEmptyValue  "dsDocFilterErrorEmptyGroup"
  #define dsDocFilterErrorIS  "Основная информационная сущность с UID""ом  \"%s\" не найдена."
  #define dsDocFilterErrorISDomain  "Основная информационная сущность с UID""ом \"%s\" не пренадлежит домену с номером \"%s\"."

//  FilterEdit
  #define dsDocFilterEditCaption  "Формирование фильтра"
  #define dsDocFilterEditOkBtnCaption      "Применить"
  #define dsDocFilterEditCancelBtnCaption  "Отмена"
  #define dsDocFilterEditHelpBtnCaption    "&Помощь"

// FilterUtils
  #define dsDocFilterUtilsFilterVersion  "Версия фильтра \"%s\" не поддерживается. Требуется версия \"%s\" или ниже."

// Options
  #define dsDocOptions  ""

// Parsers
  #define dsDocParsersLexTypes01  "Имя"
  #define dsDocParsersLexTypes02  "Симольная константа"
  #define dsDocParsersLexTypes03  "Десятичная целая константа"
  #define dsDocParsersLexTypes04  "Десятичная вещественная константа"
  #define dsDocParsersLexTypes05  "Номер строки столбца"
  #define dsDocParsersLexTypes06  "Номер ячейки"
  #define dsDocParsersLexTypes07  "Операция СЛОЖЕНИЕ"
  #define dsDocParsersLexTypes08  "Операция ВЫЧИТАНИЕ"
  #define dsDocParsersLexTypes09  "Операция УМНОЖЕНИЕ"
  #define dsDocParsersLexTypes10  "Операция ДЕЛЕНИЕ"
  #define dsDocParsersLexTypes11  "Отношение РАВНО"
  #define dsDocParsersLexTypes12  "Отношение НЕ РАВНО"
  #define dsDocParsersLexTypes13  "Отношение МЕНЬШЕ"
  #define dsDocParsersLexTypes14  "Отношение БОЛЬШЕ"
  #define dsDocParsersLexTypes15  "Отношение МЕНЬШЕ ИЛИ РАВНО"
  #define dsDocParsersLexTypes16  "Отношение БОЛЬШЕ ИЛИ РАВНО"
  #define dsDocParsersLexTypes17  "Открывающая скобка"
  #define dsDocParsersLexTypes18  "Закрывающая скобка"
  #define dsDocParsersLexTypes19  "Операция НЕ"
  #define dsDocParsersLexTypes20  "Операция И"
  #define dsDocParsersLexTypes21  "Операция ИЛИ"

  #define dsDocParsersLexClasses1  "Имя неизвестного типа"
  #define dsDocParsersLexClasses2  "Имя или константа числового типа, номер строки/столбца или номер ячейки"
  #define dsDocParsersLexClasses3  "Имя или константа строкового типа"
  #define dsDocParsersLexClasses4  "Унарная операция"
  #define dsDocParsersLexClasses5  "Мультипликативная операция"
  #define dsDocParsersLexClasses6  "Аддитивная операция"
  #define dsDocParsersLexClasses7  "Операция отношения"
  #define dsDocParsersLexClasses8  "Открывающая скобка"
  #define dsDocParsersLexClasses9  "Закрывающая скобка"

  #define dsDocParsersLexErrors1  "Успешный анализ."
  #define dsDocParsersLexErrors2  "Пустое выражение."
  #define dsDocParsersLexErrors3  "Неверный символ."
  #define dsDocParsersLexErrors4  "Неожиданный конец лексемы."
  #define dsDocParsersLexErrors5  "Ожидается символ-разделитель."
  #define dsDocParsersLexErrors6  "Ожидается символ-цифры."
  #define dsDocParsersLexErrors7  "Ожидается символ-цифры или символ-разделитель."
  #define dsDocParsersLexErrors8  "Ожидается символ-цифры, символ-разделитель или символ "".""."
  #define dsDocParsersLexErrors9  "Ожидается символ-цифры, символ-разделитель или символ "":""."

  #define dsDocParsersSyntErrors01  "Успешный анализ."
  #define dsDocParsersSyntErrors02  "Дисбаланс скобок."
  #define dsDocParsersSyntErrors03  "Пустое выражение."
  #define dsDocParsersSyntErrors04  "Выражение не содержит операций."
  #define dsDocParsersSyntErrors05  "Операция не входит в состав выражения."
  #define dsDocParsersSyntErrors06  "Для операнда не указана операция."
  #define dsDocParsersSyntErrors07  "Для операции должен быть указан один и только один операнд."
  #define dsDocParsersSyntErrors08  "Для операции должны быть указаны два операнда."
  #define dsDocParsersSyntErrors09  "Для логической операции должны быть указаны логические операнды."
  #define dsDocParsersSyntErrors10  "Для арифметической операции должны быть указаны арифметические операнды."
  #define dsDocParsersSyntErrors11  "Для операции сравнения в качестве операндов могут выступать имя, константа, арифметическое выражение или номер строки столбца."
  #define dsDocParsersSyntErrors12  "Правила вычисления должны быть указаны в виде арифметического выражения или арифметического операнда."
  #define dsDocParsersSyntErrors13  "Правила контроля должны быть указаны в виде логического выражения."

  #define dsDocParsersSemErrors01  "Успешный анализ."
  #define dsDocParsersSemErrors02  "Значение константы слишком велико."
  #define dsDocParsersSemErrors03  "Номер слишком велик."
  #define dsDocParsersSemErrors04  "Нумерация начинается с \"1\"."
  #define dsDocParsersSemErrors05  "Номер строки слишком велик."
  #define dsDocParsersSemErrors06  "Нумерация строк начинается с \"1\"."
  #define dsDocParsersSemErrors07  "Номер столбца слишком велик."
  #define dsDocParsersSemErrors08  "Нумерация столбцов начинается с \"1\"."
  #define dsDocParsersSemErrors09  "Деление на ноль."
  #define dsDocParsersSemErrors10  "Для арифметической операции должны быть указаны арифметические операнды."
  #define dsDocParsersSemErrors11  "Для операции сравнения в качестве операндов могут выступать имя, константа, арифметическое выражение или номер строки столбца."

  #define dsDocParsersErrorPos  " Позиция %s."

  #define dsDocParsersErrorExprLex  "Отсутствует лексический анализатор."
  #define dsDocParsersErrorExprSynt  "Отсутствует синтаксический анализатор."
  #define dsDocParsersErrorExprSem  "Отсутствует семантический анализатор."

  #define dsDocParsersOk  "Успешный анализ."

  #define dsDocParsersLexError  "ЛЕКСИЧЕСКАЯ ОШИБКА: "
  #define dsDocParsersSyntError  "СИНТАКСИЧЕСКАЯ ОШИБКА: "
  #define dsDocParsersSemError  "СЕМАНТИЧЕСКАЯ ОШИБКА: "

  #define dsDocParsersNotConst  "НЕ"
  #define dsDocParsersAndConst  "И"
  #define dsDocParsersOrConst  "ИЛИ"
  #define dsDocParsersErrorPolandStr  "Не установлено свойство PolandStr."

// Spec
  #define dsDocSpecEmptyInsCaption  "Э л е м е н т  н а д п и с и  -  з н а ч е н и е  н е  у с т а н о в л е н о"
  #define dsDocSpecGetCountCaption  "Подсчет количества для \"%s\""

// SpecUtils
  #define dsDocSpecUtilsReqVersion  "Версия описания документа \"%s\" не поддерживается. Требуется версия \"%s\" или ниже."
  #define dsDocViewerNumTCellLTitle  "№ п/п"
  #define dsDocViewerErrorSetDocFormat  "Данная версия компонента не поддерживает указанный режим."
  #define dsDocViewerErrorNoSpecDef  "Должен быть указан спецификатор."
  #define dsDocViewerErrorNoDocDef  "Должен быть указан документ."
  #define dsDocViewerErrorCreateMSWord  "Данная версия компонента не поддерживает формирование документов в формате MS Word."
  #define dsDocViewerErrorPreviewTypeVal  "Значение \"ptUnknown\" параметра APreviewType используется только для внутренних целей."

//DocCreator
  #define dsDocDocCreatorCaption       "Формирование документа"
  #define dsDocDocCreatorStageByCommCaption   "Этап : %s из %s"

  #define dsDocDocCreatorDocLoadConf   "Данный документ за указанный период уже существует!!!\r\n Загрузить сохранённый документ?"
  #define dsDocDocCreatorDocLoadConfCaption   "Подтверждение загрузки документа"
  #define dsDocDocCreatorInitCaption   "Инициализация..."
  #define dsDocDocCreatorInscCreateCaption   "Формирование текста надписи"
  #define dsDocDocCreatorListCreateCaption   "Формирование списка (всего строк: \"%s\")"
  #define dsDocDocCreatorErrorISLink   "Отсутствует связь между сущностями"
  #define dsDocDocCreatorTableCreateGetCount   "Формирование таблицы (расчёт количества строк/столбцов)"
  #define dsDocDocCreatorTableCreateRows   "Формирование таблицы (формирование строк)"
  #define dsDocDocCreatorTableCreateRow   "строка"
  #define dsDocDocCreatorTableCreateCols   "Формирование таблицы (формирование колонок)"
  #define dsDocDocCreatorTableCreateCol   "колонка"
  #define dsDocDocCreatorTableCreateCeills   "Формирование таблицы (формирование ячеек)"
  #define dsDocDocCreatorTableCreateCeill   "Формирование таблицы, ячейка(%s,%s)"
  #define dsDocDocCreatorTableCreateCeillMsg   "Формирование таблицы (%s № %s)"
  #define dsDocDocCreatorErrorMissingColRowDef   "Отсутствует описание строк или колонок"
  #define dsDocDocCreatorErrorCeilCalc   "Ошибка при вычислении ячейки.\r\nстрока - %s колонка - %s; (%s,%s)"

  #define dsDocDocCreatorPeroidFromCaption   "с"
  #define dsDocDocCreatorPeroidTo1Caption   "по"
  #define dsDocDocCreatorPeroidTo2Caption   "до"
  #define dsDocDocCreatorUndefPeroidCaption   "Не указан"

  #define dsDocDocCreatorPeriodicityEqual   "Совпадает с периодичностью формируемого документа"
  #define dsDocDocCreatorPeriodicityYear   "Год"
  #define dsDocDocCreatorPeriodicityHalfYear   "Полугодие"
  #define dsDocDocCreatorPeriodicityQuarter   "Квартал"
  #define dsDocDocCreatorPeriodicityMonth   "Месяц"
  #define dsDocDocCreatorPeriodicityUnique   "Уникальная"
  #define dsDocDocCreatorPeriodicityNo   "Отсутствует"
  #define dsDocDocCreatorPeriodicityAny   "Любая"

  #define dsDocDocCreatorAuthorDefined   ""
  #define dsDocDocCreatorAuthorThis   "Своя"
  #define dsDocDocCreatorAuthorAllChild   "Все подчинённые"

  #define dsDocDocCreatorErrorNoIntDocs   "Этап %s\r\nОтсутствуют документы для интеграции.\r\n\r\nИнтегрируемые документы:\r\nОписание:\t\"%s\"\r\nПериод:\t\t\"%s\"\r\nПериодичность:\t\"%s\"\r\nОрганизация:\t\"%s\"."

  #define dsDocDocCreatorErrorCreateRowColNamesNotValid   "Наименование списка строк/столбцов не соответствует ""rows,columns""."
  #define dsDocDocCreatorErrorRowColReferNotValid   "Правила формирования документа по документу,\r\n указана ссылка на отсутствующую %s, идф=""%s""."

  #define dsDocDocCreatorSpecPrepare   "Подготовка описания документа"

//ExtFunc

  #define dsDocExtFuncDocPeriodLocStr1   "Год"
  #define dsDocExtFuncDocPeriodLocStr2   "Полугодие"
  #define dsDocExtFuncDocPeriodLocStr3   "Квартал"
  #define dsDocExtFuncDocPeriodLocStr4   "Месяц"
  #define dsDocExtFuncDocPeriodLocStr5   "Уникальная"
  #define dsDocExtFuncDocPeriodLocStr6   "Отсутствует"

  #define dsDocExtFuncDocPeriodValStrUniq1   "%s.%s.%s г. - %s.%s.%s г."
  #define dsDocExtFuncDocPeriodValStrUniq2   "с %s.%s.%s г."
  #define dsDocExtFuncDocPeriodValStrUniq3   "до %s.%s.%s г."

  #define dsDocExtFuncDocPeriodValStrMonth   "%s %s г."

  #define dsDocExtFuncDocPeriodValStrQuart1   "I квартал %s г."
  #define dsDocExtFuncDocPeriodValStrQuart2   "II квартал %s г."
  #define dsDocExtFuncDocPeriodValStrQuart3   "III квартал %s г."
  #define dsDocExtFuncDocPeriodValStrQuart4   "IV квартал %s г."

  #define dsDocExtFuncDocPeriodValStrHalfYear1   "I полугодие  %s г."
  #define dsDocExtFuncDocPeriodValStrHalfYear2   "II полугодие %s г."

  #define dsDocExtFuncDocPeriodValStrYear   "%s г."

  #define dsDocExtFuncErrorReadSettingCaption   "Ошибка чтения настроек"
  #define dsDocExtFuncErrorReadSettingMsg   "В параметрах настройки не заданы параметры ЛПУ"

  #define dsDocExtFuncCreateDocViewMsg   "Формирование отображения документа"
  #define dsDocExtFuncErrorCreateDocView   "Ошибка формирования просмотра (отсутствует описание документа)"

  #define dsDocExtFuncReplaceDocMsg   "Документ - \"%s\"\r\n за период: %s\r\n уже существует.\r\n\r\nЗаменить существующий документ?"
  #define dsDocExtFuncReplaceDocCaption   "Подтверждение замены документа"
  #define dsDocExtFuncMissingDocDef   "Отсутствует описание документа, код описания \"%s\""
  #define dsDocExtFuncNoDocDef   "Описание отсутствует"
  #define dsDocExtFuncFileVersionString   "StringFileInfo\\041904E3\\FileVersion"

  #define dsDocExtFuncRecCount1    "запись"
  #define dsDocExtFuncRecCount234  "записи"
  #define dsDocExtFuncRecCount     "записей"
}	/* namespace Dsdocsrvconst */
using namespace Dsdocsrvconst;
#endif	// DsdocsrvconstHPP
