﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsDocDMUnit.h"
#include "dsDocViewCreator.h"
#include "dsDocSrvConstDef.h"
#include "dsDocDocCreatorUnit.h"
#include "JSONUtils.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma link "AppOptBaseProv"
#pragma link "AppOptions"
#pragma link "AppOptXMLProv"
#pragma resource "*.dfm"
TdsDocDM * dsDocDM;
const int LoadBufferSize = 102400;
// ---------------------------------------------------------------------------
__fastcall TdsDocDM::TdsDocDM(TComponent * Owner, TFDConnection * ADocConnection, TFDConnection * AICConnection,
  TdsMKB * AMKB, TAxeXMLContainer * AXMLList) : TDataModule(Owner)
 {
  dsLogBegin(__FUNC__);
  FConnection    = AICConnection;
  FDocConnection = ADocConnection;
  FXMLList              = AXMLList;
  FClsData       = new TdsCommClassData(AICConnection);
  UnicodeString FXMLPath = icsPrePath(ExtractFilePath(ParamStr(0)));
  UnicodeString FFN = icsPrePath(FXMLPath + "\\conf\\ic_app_opt.xml");
  if (!FileExists(FFN))
   {
    ForceDirectories(FXMLPath);
    TTagNode * tmp = new TTagNode;
    try
     {
      tmp->Encoding = "utf-8";
      tmp->Name     = "g";
      tmp->SaveToXMLFile(FFN, "");
     }
    __finally
     {
      delete tmp;
     }
   }
  AppOptXMLData->FileName = FFN;
  AppOpt->LoadXML();
  FDefDocCreateType     = dctBaseDoc;
  FImmNomQuery          = CreateTempQuery();
  FDocNomQuery          = CreateDocTempQuery();
  FImmNom               = new TICSIMMNomDef(FImmNomQuery, FXMLList, "40381E23-92155860-4448", FClsData);
  FDocNom               = new TICSBaseDocNomDef(FDocNomQuery, FXMLList, "40381E23-92155860-4448");
  FMKB                  = AMKB;
  LoadLPUParam();
  FDocCreator      = NULL;
  FProgressCounter = NULL;
  FCreatedView     = NULL;
  LPUParam.Clear();
  FClassDataQuery = CreateTempQuery();
  FClassData      = new TdsRegClassData(FXMLList->GetXML("40381E23-92155860-4448"), FClassDataQuery);
  FSaveOwnerCode  = FImmNom->MainLPUCode;
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
__fastcall TdsDocDM::~TdsDocDM()
 {
  dsLogBegin(__FUNC__);
  delete FClassData;
  DeleteTempQuery(FClassDataQuery);
  // delete FMKB;
  delete FImmNom;
  delete FDocNom;
  DeleteTempQuery(FImmNomQuery);
  delete FDocNomQuery;
  if (FDocCreator)
   delete FDocCreator;
  if (FProgressCounter)
   delete FProgressCounter;
  if (FCreatedView)
   delete FCreatedView;
  delete FClsData;
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsDocDM::FCreateTempQuery(TFDConnection * AConnection)
 {
  dsLogBegin(__FUNC__);
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection = AConnection;
    FQ->Connection              = AConnection;
    // FQ->Transaction->Options->Isolation = xiUnspecified; //xiSerializible;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
    // FQ->Transaction->Options->Isolation = xiUnspecified; //xiSerializible;
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return FQ;
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsDocDM::CreateTempQuery()
 {
  return FCreateTempQuery(FConnection);
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsDocDM::CreateDocTempQuery()
 {
  return FCreateTempQuery(FDocConnection);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  dsLogBegin(__FUNC__);
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString AParam1Name,
  UnicodeString AParam1Val, UnicodeString ALogComment, UnicodeString AParam2Name, Variant AParam2Val,
  UnicodeString AParam3Name, Variant AParam3Val)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
      if (AParam2Name.Length())
       AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
      if (AParam3Name.Length())
       AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      UnicodeString FParams = "; Params: ";
      if (AParam1Name.Length())
       FParams += AParam1Name + "=" + AParam1Val + "; ";
      if (AParam2Name.Length())
       FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
      if (AParam3Name.Length())
       FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "");
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    if (ALogComment.Length())
     dsLogSQL(ALogComment, __FUNC__, FBeg - Time(), 5);
    else
     dsLogSQL(ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::GetSpecTextById(System::UnicodeString AId, int & AType)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    quExecParam(FDQ, false, "Select SPECTEXT, SPECTYPE From SPECIFIKATORS Where Upper(GUI)=:pGUI", "pGUI",
      AId.UpperCase());
    if (FDQ->RecordCount)
     {
      AType = FDQ->FieldByName("SPECTYPE")->AsInteger;
      RC    = FDQ->FieldByName("SPECTEXT")->AsString;
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::GetDocTextById(System::UnicodeString AId)
 {
  UnicodeString RC = "";
  dsLogBegin(__FUNC__);
  TTagNode * FTmpDoc = new TTagNode;
  try
   {
    FGetDoc(AId, FTmpDoc);
    RC = FTmpDoc->AsXML;
   }
  __finally
   {
    delete FTmpDoc;
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
bool __fastcall TdsDocDM::GetSpecById(System::UnicodeString AId, TTagNode * ASpec)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    quExecParam(FDQ, false, "Select SPECTEXT, SPECTYPE From SPECIFIKATORS Where Upper(GUI)=:pGUI", "pGUI",
      AId.UpperCase());
    if (FDQ->RecordCount)
     {
      ASpec->LoadFromBytes(FDQ->FieldByName("SPECTEXT")->AsBytes);
      RC = true;
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsDocDM::FGetFilter(System::UnicodeString AId, TTagNode * AFilter)
 {
  dsLogBegin(__FUNC__);
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    quExecParam(FDQ, false, "Select FLTTEXT From FILTERS Where CODE=:pCode", "pCode", AId);
    if (FDQ->RecordCount)
     {
      AFilter->LoadFromBytes(FDQ->FieldByName("FLTTEXT")->AsBytes);
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  dsLogEnd(__FUNC__);
 }
// ----------------------------------------------------------------------------
void __fastcall TdsDocDM::FGetFilterByGUI(System::UnicodeString AGUI, TTagNode * AFilter)
 {
  dsLogBegin(__FUNC__);
  if (AGUI.Pos("<?xml"))
   {
    AFilter->AsXML = AGUI;
   }
  else
   {
    TFDQuery * FDQ = CreateDocTempQuery();
    try
     {
      quExecParam(FDQ, false, "Select FLTTEXT From FILTERS Where Upper(gui)=:pGUI", "pGUI", AGUI.UpperCase());
      if (FDQ->RecordCount)
       {
        AFilter->LoadFromBytes(FDQ->FieldByName("FLTTEXT")->AsBytes);
       }
     }
    __finally
     {
      DeleteTempQuery(FDQ);
     }
   }
  dsLogEnd(__FUNC__);
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::GetFilterByGUI(System::UnicodeString AGUI)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  TTagNode * FFilter = new TTagNode;
  try
   {
    FGetFilterByGUI(AGUI, FFilter);
    RC = FFilter->AsXML;
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsDocDM::FGetDoc(System::UnicodeString AId, TTagNode * ADoc)
 {
  dsLogBegin(__FUNC__);
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    quExecParam(FDQ, false, "Select DOCTEXT From DOCUMENTS Where Upper(GUI)=:pGUI", "pGUI", AId.UpperCase());
    if (!FDQ->RecordCount)
     quExecParam(FDQ, false, "Select DOCTEXT From TEMPDOCUMENTS Where Upper(GUI)=:pGUI", "pGUI", AId.UpperCase());
    if (FDQ->RecordCount)
     {
      TStringStream * FZipDoc = new TStringStream;
      try
       {
        UnicodeString tmpVal = FDQ->FieldByName("DOCTEXT")->AsString;
        if (tmpVal.Length())
         {
          FZipDoc->WriteData(tmpVal.BytesOf(), tmpVal.Length());
          ADoc->SetZIPXML(FZipDoc);
         }
       }
      __finally
       {
        delete FZipDoc;
       }
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  dsLogEnd(__FUNC__);
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::CheckByPeriod(bool FCreateByType, UnicodeString ACode,
  UnicodeString ASpecGUI)
 {
  /* dsLogMessage("CheckByPeriod: ByType="+IntToStr((int)FCreateByType)+", Code="+ACode+", SpecGUI="+ASpecGUI);
   UnicodeString RC = "";
   try
   {
   try
   {
   FMissingSpecDoc = new TStringList;
   TTagNode *FPackDef = new TTagNode(NULL);
   UnicodeString OrgName;
   int FSpecType;
   int FMisCount;
   try
   {
   if (FCreateByType)
   FDM->qtExecDoc1("select distinct CODE, NAME, ADDR, PHONE from SUBORDORG where SUBORD="+ACode+" order by name");
   else
   FDM->qtExecDoc1("select distinct CODE, NAME, ADDR, PHONE from SUBORDORG where CODE='"+ACode+"' order by name");
   if (FDM->quFreeDoc1->RecordCount)
   {
   while (!FDM->quFreeDoc1->Eof)
   {
   FMisCount = FMissingSpecDoc->Count;
   FCheckOwnerCode = FDM->quFreeDoc1->FN("CODE")->AsString;
   if (FDM->GetSpec(FSpecGUIMap[SpecCB->ItemIndex], FPackDef, FSpecType))
   {
   FDM->CheckPackage(FCheckDocItem, FPackDef);
   }
   if (FMissingSpecDoc->Count > FMisCount)
   {
   OrgName = "<tr><td colspan='3' style='font-width: bold; background-color:#EEE; border-right: none;'>"+FDM->quFreeDoc1->FN("NAME")->AsString;
   if (FDM->quFreeDoc1->FN("ADDR")->AsString.Length() || FDM->quFreeDoc1->FN("PHONE")->AsString.Length())
   {
   OrgName += " [";
   if (FDM->quFreeDoc1->FN("ADDR")->AsString.Length())
   {
   OrgName += FDM->quFreeDoc1->FN("ADDR")->AsString;
   if (FDM->quFreeDoc1->FN("PHONE")->AsString.Length())
   OrgName += ", тлф: "+FDM->quFreeDoc1->FN("PHONE")->AsString;
   }
   else
   {
   if (FDM->quFreeDoc1->FN("PHONE")->AsString.Length())
   OrgName += "тлф: "+FDM->quFreeDoc1->FN("PHONE")->AsString+"]";
   }
   OrgName += "]";
   }
   OrgName += "</td></tr>";
   FMissingSpecDoc->Insert(FMisCount, OrgName);
   }
   FDM->quFreeDoc1->Next();
   }
   if (FMissingSpecDoc->Count)
   {
   FMissingSpecDoc->Insert(0,"<html>"+FDM->StyleDef+"<body><h2>Отсутствуют следующие документы</h2><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><th>Код</th><th>Наименование</th><th style='border-right: none;'>Период</th></tr>");
   FMissingSpecDoc->Add("</table></body></html>");
   }
   else
   {
   FMissingSpecDoc->Insert(0,"<html>"+FDM->StyleDef+"<body><h2>Документы в наличии</h2>");
   FMissingSpecDoc->Add("</body></html>");
   }
   }
   else
   {
   FMissingSpecDoc->Insert(0,"<html>"+FDM->StyleDef+"<body><h2>Отсутствуют указанные организации</h2>");
   FMissingSpecDoc->Add("</body></html>");
   }
   RC = FMissingSpecDoc->Text;
   }
   __finally
   {
   delete Dlg;
   delete FMissingSpecDoc;
   FMissingSpecDoc = NULL;
   delete FPackDef;
   }
   }
   catch(System::Sysutils::Exception &E)
   {
   dsLogError(E.Message, __FUNC__);
   }
   }
   __finally
   {
   }
   return RC; */
  return "<html><head><meta charset=\"windows-1251\"></head><body>Шота тут есть :)</body></html>";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::FCheckDocItem(TFDQuery * AQuery, UnicodeString ASpecName, UnicodeString ASpecGUI,
  int APerType, TDate ADateFr, TDate ADateTo)
 {
  dsLogBegin(__FUNC__);
  try
   {
    AQuery->Close();
    if (AQuery->Transaction->Active)
     AQuery->Transaction->Commit();
    AQuery->Transaction->StartTransaction();
    if (APerType)
     AQuery->SQL->Text =
       "Select count(GUI) as RCOUNT From DOCUMENTS Where Upper(F_OWNER) = :pF_OWNER and Upper(SPECGUI)=:pSPECGUI and F_PERIOD = :pF_PERIOD and F_FROMDATE=:pF_FROMDATE and F_TODATE=:pF_TODATE";
    else
     AQuery->SQL->Text =
       "Select count(GUI) as RCOUNT From DOCUMENTS Where Upper(F_OWNER) = :pF_OWNER and Upper(SPECGUI)=:pSPECGUI and F_PERIOD = :pF_PERIOD";
    AQuery->Prepare();
    // AQuery->ParamByName("pF_OWNER")->AsString   = FCheckOwnerCode.UpperCase();
    AQuery->ParamByName("pSPECGUI")->AsString = ASpecGUI.UpperCase();
    AQuery->ParamByName("pF_PERIOD")->AsInteger = APerType;
    if (APerType)
     {
      AQuery->ParamByName("pF_FROMDATE")->AsDateTime = ADateFr;
      AQuery->ParamByName("pF_TODATE")->AsDateTime = ADateTo;
     }
    AQuery->OpenOrExecute();
    if (!AQuery->FieldByName("RCOUNT")->AsInteger)
     {
      // FMissingSpecDoc->Add("<tr><td>"+ASpecGUI+"</td><td>"+ASpecName+"</td><td style='border-right: none;'>"+GetDocPeriodValStr(IntToStr(APerType), ADateFr.FormatString("dd.mm.yyyy"), ADateTo.FormatString("dd.mm.yyyy"))+"</td></tr>");
     }
   }
  __finally
   {
    AQuery->Transaction->Commit();
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::CheckPackage(TICSCheckPackItem ACheckItemEvt, TTagNode * APackDef, TFDQuery * AQuery)
 {
  /* TDocCreateType FCreateType;
   try
   {
   Application->ProcessMessages();

   FCreateType = ctBaseDoc;

   bool FPeriodisity = false;
   TTagNode *itSpec = APackDef->GetChildByName("doccontent",true)->GetFirstChild();
   while(itSpec)
   {
   FPeriodisity |= itSpec->AV["type"].ToIntDef(0);
   itSpec = itSpec->GetNext();
   }

   if (FCreateType != ctNone)
   { // формируем пакет
   TDate FPeriodDateFr = 0;
   TDate FPeriodDateTo = 0;
   short int FPerType = 0;
   if (FPeriodisity)
   {
   if (CurDocCreateMonth != -1)
   {
   FPeriodDateFr = GetFirstMonthDate(CurDocCreateMonth, CurDocCreateYear);
   FPeriodDateTo = GetLastMonthDate(CurDocCreateMonth, CurDocCreateYear);
   FPerType = 2;
   }
   else
   {
   TDocPeriodForm *Dlg = NULL;
   try
   {
   Dlg = new TDocPeriodForm(Application->Owner, ReqColor);
   FPerType = 0;
   Dlg->ShowModal();
   if (Dlg->ModalResult == mrOk)
   {
   FPerType = Dlg->pdType;
   FPeriodDateFr = Dlg->DateFr;
   FPeriodDateTo = Dlg->DateTo;
   }
   else
   FCreateType = ctNone;
   }
   __finally
   {
   if (Dlg) delete Dlg;
   }
   }
   }
   if (FCreateType != ctNone)
   {
   itSpec = APackDef->GetChildByName("doccontent",true)->GetFirstChild();
   int FSpecPerType;
   int FDocPerType;
   TDate FDocPeriodDateFr, FDocPeriodDateTo;
   TFDQuery *tmpQ = CreateTempQuery();
   try
   {
   while(itSpec)
   { // цикл по описаниям
   try
   {
   try
   {
   if (FPeriodisity)
   { // периодичность есть, мудохаемся с ней
   FSpecPerType = itSpec->AV["type"].ToIntDef(0);
   if (FSpecPerType)
   {
   FDocPeriodDateFr = FPeriodDateFr;
   FDocPeriodDateTo = FPeriodDateTo;
   FDocPerType =  GetPeriodValue(FSpecPerType, itSpec->AV["stype"].ToIntDef(0), ((FSpecPerType == 6)? UnicodeString(""):itSpec->AV["dfrom"]),
   ((FSpecPerType == 6)? itSpec->AV["dfrom"]:UnicodeString("")), itSpec->AV["dto"],
   itSpec->AV["offset"],
   FPerType, FDocPeriodDateFr, FDocPeriodDateTo);
   }
   }

   ACheckItemEvt(tmpQ, itSpec->AV["name"], itSpec->AV["spec_gui"], FDocPerType, FDocPeriodDateFr, FDocPeriodDateTo);
   }
   __finally
   {
   }
   }
   catch (Exception &E)
   {
   _MSG_ERRA((FMT(&_icsDocExCommonErrorCreateDocCaption)+"\n"+E.Message),cFMT(&_icsDocExCommonErrorCaption));
   }
   catch (...)
   {
   _MSG_ERRA(FMT(&_icsDocExCommonErrorCreateDocCaption),cFMT(&_icsDocExCommonErrorCaption));
   }
   itSpec = itSpec->GetNext();
   }
   }
   __finally
   {
   DeleteTempQuery(tmpQ);
   }
   }
   }
   }
   __finally
   {
   } */
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::DocListClearByDate(TDate ADate)
 {
  dsLogMessage("DocListClearByDate:  date: " + ADate.FormatString("dd.mm.yyyy"));
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = "ok";
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsDocDM::GetDocCountBySpec(UnicodeString ASpecOWNER, UnicodeString ASpecGUI, UnicodeString & ARCMsg)
 {
  int RC = 0;
  ARCMsg = "";
  dsLogMessage("GetDocCountBySpec:  SpecOWNER: " + ASpecOWNER + ", SpecGUI:" + ASpecGUI);
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    try
     {
      quExecParam(FDQ, false,
        "Select Count(*) as RCOUNT From DOCUMENTS Where Upper(F_OWNER)=:pF_OWNER and Upper(SPECGUI)=:pSPECGUI",
        "pF_OWNER", ASpecOWNER.UpperCase(), "", "pSPECGUI", ASpecGUI.UpperCase());
      RC = FDQ->FieldByName("RCOUNT")->AsInteger;
      if (RC)
       ARCMsg = "Существуют документы сформированные по данному описанию, удаление невозможно";
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::IsMainOwnerCode(UnicodeString AOwnerCode)
 {
  dsLogMessage("IsMainOwnerCode:  OwnerCode: " + AOwnerCode);
  bool RC = false;
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    try
     {
      RC = (AOwnerCode.UpperCase() == FMainOwnerCode);
      if (!RC)
       {
        quExecParam(FDQ, false, "Select Count(*) as RCOUNT From SUBORDORG Where SUBORD=2 and Upper(CODE)=:pCode",
          "pCode", AOwnerCode.UpperCase());
        RC = FDQ->FieldByName("RCOUNT")->AsInteger;
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::CopySpec(UnicodeString AName, UnicodeString & ANewGUI)
 {
  dsLogMessage("CopySpec:  Name: " + AName);
  UnicodeString RC = "error";
  TFDQuery * FDQ = CreateDocTempQuery();
  TFDQuery * FInsQ = CreateDocTempQuery();
  TTagNode * FSpec = new TTagNode;
  try
   {
    try
     {
      quExecParam(FDQ, false, "Select * From SPECIFIKATORS Where Upper(GUI)=:pGUI", "pGUI", AName.UpperCase());
      if (FDQ->RecordCount)
       {
        FSpec->AsXML = FDQ->FieldByName("SPECTEXT")->AsString;
        if (!FInsQ->Transaction->Active)
         FInsQ->Transaction->StartTransaction();
        FInsQ->SQL->Text =
          "UPDATE OR INSERT INTO SPECIFIKATORS (gui,name,SPECTEXT,F_OWNER,F_PERIOD,CREATEDATE,INTEGRATED,WTEMPLATE,SPECTYPE,MODULE_TYPE) values (:pgui,:pname,:pSPECTEXT,:pF_OWNER,:pF_PERIOD,:pCREATEDATE,:pINTEGRATED,:pWTEMPLATE,:pSPECTYPE,:pMODULE_TYPE) MATCHING (gui)";
        FInsQ->Prepare();
        UnicodeString FNewCode = NewGUI();
        UnicodeString FCopyName = GetCopyName(FDQ->FieldByName("NAME")->AsString);
        FInsQ->ParamByName("pGUI")->Value = FNewCode;
        FInsQ->ParamByName("pNAME")->Value = FCopyName;
        FInsQ->ParamByName("pF_OWNER")->Value = MainOwnerCode;
        FInsQ->ParamByName("pCREATEDATE")->Value = TDateTime::CurrentDate();
        FInsQ->ParamByName("pF_PERIOD")->Value = FDQ->FieldByName("F_PERIOD")->Value;
        FInsQ->ParamByName("pSPECTYPE")->Value = FDQ->FieldByName("SPECTYPE")->AsInteger;
        SetSpecGUI(FSpec, FNewCode);
        SetSpecOwner(FSpec, MainOwnerCode);
        SetSpecName(FSpec, FCopyName);
        FInsQ->ParamByName("pSPECTEXT")->Value = FSpec->AsXML;
        FInsQ->ParamByName("pINTEGRATED")->Value = FDQ->FieldByName("INTEGRATED")->AsInteger;
        FInsQ->ParamByName("pMODULE_TYPE")->Value = FDQ->FieldByName("MODULE_TYPE")->AsString;
        FInsQ->OpenOrExecute();
        if (FInsQ->Transaction->Active)
         FInsQ->Transaction->Commit();
        ANewGUI = FNewCode;
        RC      = "ok";
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete FSpec;
    DeleteTempQuery(FDQ);
    DeleteTempQuery(FInsQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsDocDM::CheckSpecExists(UnicodeString AName, UnicodeString & AGUI, bool AIsEdit)
 {
  dsLogMessage("CheckSpecExists:  Name: " + AName);
  int RC = 0;
  // enum TExistsType { etOk=0, etExistByName=1, etExistByCode=2};
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    try
     {
      quExecParam(FDQ, false, "Select Count(*) as RCOUNT From SPECIFIKATORS Where Upper(NAME)=:pName", "pName",
        AName.UpperCase());
      if (FDQ->FieldByName("RCOUNT")->AsInteger)
       RC = 1;
      if (!RC)
       {
        quExecParam(FDQ, false, "Select Count(*) as RCOUNT From SPECIFIKATORS Where Upper(GUI)=:pGUI", "pGUI",
          AGUI.UpperCase());
        if (FDQ->FieldByName("RCOUNT")->AsInteger)
         RC = 2;
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::CheckSpecExistsByCode(UnicodeString AGUI)
 {
  dsLogMessage("CheckSpecExistsByCode:  GUI: " + AGUI);
  bool RC = false;
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    try
     {
      quExecParam(FDQ, false, "Select Count(*) as RCOUNT From SPECIFIKATORS Where Upper(GUI)=:pGUI", "pGUI",
        AGUI.UpperCase());
      RC = FDQ->FieldByName("RCOUNT")->AsInteger;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::CreateDocPreview(UnicodeString AId, int & Al, int & At, int & Ar, int & Ab,
  double & Aps, int & Ao)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "error";
  TFDQuery * FDQ = CreateDocTempQuery();
  TTagNode * FDoc = new TTagNode;
  TTagNode * FSpec = new TTagNode;
  if (FCreatedView)
   delete FCreatedView;
  FCreatedView = NULL;
  try
   {
    FGetDoc(AId, FDoc);
    if (FDoc->GetChildByName("passport"))
     {
      try
       {
        TdsDocViewCreator * FDocView = new TdsDocViewCreator(this);
        try
         {
          quExecParam(FDQ, false, "Select SPECTEXT, WTEMPLATE From SPECIFIKATORS Where Upper(GUI)=:pGUI", "pGUI",
            FDoc->GetChildByName("passport")->AV["GUIspecificator"].UpperCase());
          if (FDQ->RecordCount > 0)
           {
            // TByteDynArray DDD = FQ->FieldByName("SPECTEXT")->AsBytes;
            // TEncoding *FFF = TEncoding::Default;
            // TEncoding::GetBufferEncoding(DDD, FFF);
            FSpec->LoadFromBytes(FDQ->FieldByName("SPECTEXT")->AsBytes);
            if (FDQ->FieldByName("WTEMPLATE")->IsNull || !FDQ->FieldByName("WTEMPLATE")->AsString.Length())
             {
              FDocView->XMLTemplate = NULL;
             }
            else
             {
              TTagNode * FTmpl = NULL;
              try
               {
                FTmpl = new TTagNode(NULL);
                try
                 {
                  FTmpl->AsXML          = FDQ->FieldByName("WTEMPLATE")->AsString;
                  FDocView->XMLTemplate = FTmpl;
                 }
                catch (System::Sysutils::Exception & E)
                 {
                  FDocView->XMLTemplate = NULL;
                  dsLogError("Установлен некорректный шаблон. " + E.Message, __FUNC__);
                 }
               }
              __finally
               {
                if (FTmpl)
                 delete FTmpl;
               }
             }
            try
             {
              TTagNode * FPageSetup = FSpec->GetChildByName("pagesetup", true);
              if (FPageSetup)
               {
                Al = FPageSetup->AV["leftfield"].ToIntDef(10) / 100;
                At = FPageSetup->AV["topfield"].ToIntDef(10) / 100;
                Ar = FPageSetup->AV["rightfield"].ToIntDef(10) / 100;
                Ab = FPageSetup->AV["bottomfield"].ToIntDef(10) / 100;
                if (FPageSetup->AV["prk"].Length())
                 Aps = StrToFloat(FPageSetup->AV["prk"]);
                else
                 Aps = 1;
                Ao = FPageSetup->AV["orientation"].ToIntDef(0);
               }
              FCreatedView = new TStringStream(FDocView->CreatePreview(ptDocument, FXMLList, FSpec, FDoc));
              FCreatedView->Seek(0, 0);
              RC = "ok";
             }
            catch (System::Sysutils::Exception & E)
             {
              dsLogError(E.Message, __FUNC__);
             }
           }
          else
           {
            dsLogError(dsDocExExtFuncErrorCreateDocView, __FUNC__);
           }
         }
        __finally
         {
          delete FDocView;
         }
       }
      catch (System::Sysutils::Exception & E)
       {
        dsLogError(E.Message, __FUNC__);
       }
     }
    else
     dsLogError("Ошибочная структура документа: " + FDoc->AsXML, __FUNC__);
   }
  __finally
   {
    DeleteTempQuery(FDQ);
    delete FDoc;
    delete FSpec;
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::LoadDocPreviewProgress(System::UnicodeString & AMsg)
 {
  UnicodeString RC = "";
  AMsg = "error";
  try
   {
    try
     {
      if (FCreatedView)
       {
        if (FCreatedView->Position + LoadBufferSize < FCreatedView->Size)
         RC = FCreatedView->ReadString(LoadBufferSize);
        else
         RC = FCreatedView->ReadString(FCreatedView->Size - FCreatedView->Position);
        if (FCreatedView->Position >= FCreatedView->Size)
         {
          AMsg = "ok";
          delete FCreatedView;
          FCreatedView = NULL;
         }
        else
         AMsg = "progress:" + IntToStr(FCreatedView->Position) + "/" + IntToStr(FCreatedView->Size);
       }
     }
    catch (Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::CheckDocCreateType(UnicodeString ASpecGUI, int & ACreateType, int ADefCreateType,
  int & AGetCreateType, int & AGetPeriod)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  AGetCreateType = 0;
  AGetPeriod     = 0;
  try
   {
    try
     {
      int FSpecType;
      UnicodeString FSpecText = GetSpecTextById(ASpecGUI, FSpecType);
      if (FSpecText.Length())
       {
        TTagNode * FSpec = new TTagNode;
        try
         {
          FSpec->AsXML   = FSpecText;
          ACreateType    = dctBaseDoc;
          AGetCreateType = 0;
          bool FDocRulesExist = FSpec->GetChildByName("docrules");
          AGetPeriod = FSpec->GetChildByName("passport")->AV["perioddef"].ToIntDef(0);
          if (FSpecType == 11) // номенклатор документооборота
           {
            if (ADefCreateType)
             ACreateType = ADefCreateType;
            else
             {
              if (FDocRulesExist)
               AGetCreateType = 1;
              else
               ACreateType = dctBase;
             }
            RC = true;
           }
          else if (FDefDocCreateType == dctDoc)
           {
            if (FDocRulesExist)
             ACreateType = dctDoc;
            else
             ACreateType = dctEmpty;
            RC = true;
           }
          else if (FDefDocCreateType == dctBase)
           {
            ACreateType = dctBase;
            RC          = true;
           }
          else if (FDefDocCreateType == dctBaseDoc)
           {
            if (ADefCreateType)
             ACreateType = ADefCreateType;
            else
             {
              if (FDocRulesExist)
               AGetCreateType = 1;
              else
               ACreateType = dctBase;
             }
            RC = true;
           }
          else
           ACreateType = dctNone;
          // AGetPeriod = 1;
         }
        __finally
         {
          delete FSpec;
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::CanHandEdit(UnicodeString ASpecGUI)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  try
   {
    try
     {
      int FSpecType;
      UnicodeString FSpecText = GetSpecTextById(ASpecGUI, FSpecType);
      if (FSpecText.Length())
       {
        TTagNode * FSpec = new TTagNode;
        try
         {
          FSpec->AsXML = FSpecText;
          RC           = (FSpec->GetChildByName("inscription", true) && FSpec->GetChildByName("table", true));
         }
        __finally
         {
          delete FSpec;
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::DocCreateCheckExist(TTagNode * ADoc)
 {
  return DocCreateCheckExist(GetDocSpecGUI(ADoc), GetDocOwner(ADoc), GetDocPeriod(ADoc).ToIntDef(0),
    GetDocPeriodDateFrom(ADoc), GetDocPeriodDateTo(ADoc));
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::DocCreateCheckExist(UnicodeString ASpecGUI, UnicodeString AAuthor,
  int APerType, TDate APeriodDateFr, TDate APeriodDateTo)
 {
  dsLogBegin(__FUNC__);
  System::UnicodeString RC = "";
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    UnicodeString tmpSQL =
      "Select GUI From DOCUMENTS Where Upper(SPECGUI)= :pSPECGUI and Upper(F_OWNER)= :pF_OWNER and F_PERIOD=:pF_PERIOD";
    try
     {
      if (APerType)
       tmpSQL += " and F_FROMDATE=:pF_FROMDATE and F_TODATE=:pF_TODATE";
      FDQ->Close();
      if (FDQ->Transaction->Active)
       FDQ->Transaction->Commit();
      FDQ->Transaction->StartTransaction();
      FDQ->SQL->Text = tmpSQL;
      FDQ->Prepare();
      FDQ->ParamByName("pSPECGUI")->Value = ASpecGUI.UpperCase();
      FDQ->ParamByName("pF_OWNER")->Value = AAuthor.UpperCase();
      FDQ->ParamByName("pF_PERIOD")->Value = APerType;
      if (APerType)
       {
        FDQ->ParamByName("pF_FROMDATE")->Value = APeriodDateFr;
        FDQ->ParamByName("pF_TODATE")->Value = APeriodDateTo;
       }
      FDQ->OpenOrExecute();
      if (FDQ->RecordCount > 0)
       {
        RC = FDQ->FieldByName("GUI")->AsString;
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + tmpSQL, __FUNC__);
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::CheckFilter(UnicodeString AFilterGUI)
 {
  dsLogBegin(__FUNC__);
  System::UnicodeString RC = "error";
  TTagNode * FExtFilter = new TTagNode;
  try
   {
    FGetFilterByGUI(AFilterGUI, FExtFilter);
    TTagNode * FExtDocCondRules = FExtFilter->GetChildByName("condrules");
    if (FExtDocCondRules)
     {
      if (CheckCondRules(FExtDocCondRules) == frOk)
       {
        RC = "ok";
       }
      else if (CheckCondRules(FExtDocCondRules) == frBad)
       {
        RC = FExtDocCondRules->AsXML;
       }
     }
    else
     RC = "ok";
   }
  __finally
   {
    delete FExtFilter;
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::DocCreateCheckFilter(UnicodeString ASpecGUI, UnicodeString AFilterCode,
  UnicodeString & AFilterName, UnicodeString & AFilterGUI)
 {
  dsLogBegin(__FUNC__);
  System::UnicodeString RC = "error";
  // TFDQuery *FQ = CreateTempQuery();
  TTagNode * FSpec = new TTagNode;
  TTagNode * FExtFilter = NULL;
  AFilterName = "";
  AFilterGUI  = "";
  try
   {
    TTagNode * FExtDocCondRules = NULL;
    GetSpecById(ASpecGUI, FSpec);
    if (AFilterCode.Length())
     {
      FExtFilter = new TTagNode;
      if (AFilterCode.Pos("<?xml"))
       FExtFilter->AsXML = AFilterCode;
      else
       FGetFilter(AFilterCode, FExtFilter);
      FExtDocCondRules = FExtFilter->GetChildByName("condrules");
      TTagNode * FPassport = GetPassport(FExtFilter, true);
      if (FPassport)
       {
        AFilterName = FPassport->AV["mainname"];
        AFilterGUI  = FPassport->AV["gui"];
       }
     }
    TTagNode * FDocCondRules = FSpec->GetChildByName("condrules");
    // объединяем фильтр документа и внешний фильтр
    FDocCondRules = MergeCondRules(FSpec, FDocCondRules, FExtDocCondRules, false);
    if (FDocCondRules)
     {
      if (CheckCondRules(FDocCondRules) == frOk)
       {
        RC = "ok";
       }
      else if (CheckCondRules(FDocCondRules) == frBad)
       {
        RC = FDocCondRules->AsXML;
       }
     }
    else
     RC = "ok";
   }
  __finally
   {
    delete FSpec;
    if (FExtFilter)
     delete FExtFilter;
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::DocCreate(UnicodeString ASpecGUI, UnicodeString AAuthor,
  UnicodeString AFilter, bool AConcr, int ACreateType, int APerType, TDate APeriodDateFr, TDate APeriodDateTo)
 {
  return "Функция DocCreate не поддерживается.";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsLogMessage(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsLogError(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  dsLogSQL(ASQL, AFuncName, AExecTime, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::LogBegin(UnicodeString AFuncName)
 {
  dsLogBegin(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::LogEnd(UnicodeString AFuncName)
 {
  dsLogEnd(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::FSetDefNom(TICSBaseNomDef * ANom)
 {
  FFDefNom = ANom;
  // if (ANom)
  // dsLogMessage("TdsDocDM::FSetDefNom >>> " + ANom->MainLPUCode);
  // dsLogMessage("TdsDocDM::FDocGetAVal FDefNom >>> " + IntToStr((int)ANom) /* ->MainLPUCode */);
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::StartDocCreate(UnicodeString ASpecGUI, UnicodeString AAuthor,
  UnicodeString AFilter, UnicodeString AFilterNameGUI, bool AConcr, int ACreateType, int APerType, TDate APeriodDateFr,
  TDate APeriodDateTo, bool ANotSave)
 {
  dsLogBegin(__FUNC__);
  FNotSaveDoc = ANotSave;
  System::UnicodeString RC = "";
  TTagNode * FSpec = new TTagNode;
  if (FDocCreator)
   delete FDocCreator;
  FDocCreator               = new TdsDocCreator(this);
  FDocCreator->OnLogMessage = LogMessage;
  FDocCreator->OnLogError   = LogError;
  FDocCreator->OnLogSQL     = LogSQL;
  FDocCreator->OnLogBegin   = LogBegin;
  FDocCreator->OnLogEnd     = LogEnd;
  if (!FProgressCounter)
   FProgressCounter = new TdsProgressCounter;
  FProgressCounter->Caption            = FMT(dsDocDocCreatorCaption);
  FProgressCounter->StageByCommCaption = FMT(dsDocDocCreatorStageByCommCaption);
  FProgressCounter->OnComplite         = DocCreateOnComplite;
  FProgressCounter->Init();
  TTagNode * FFilter = NULL;
  try
   {
    // !!!     FDM->ICSNom.OlePropertySet("LPUCode",  StringToOleStr(AAutor));
    if (AFilter.Length())
     {
      FFilter = new TTagNode;
      if (AFilter.Pos("<?xml"))
       FFilter->AsXML = AFilter;
      else
       FGetFilter(AFilter, FFilter);
     }
    GetSpecById(ASpecGUI, FSpec);
    FDocCreator->OnGetSVal  = FDocGetSVal;
    FDocCreator->OnGetAVal  = FDocGetAVal;
    FDocCreator->OnGetDVal  = FDocGetDVal;
    FDocCreator->OnProgress = FProgressCounter->SetProgress;
    TdsDocCreateType FDocCreateType = (TdsDocCreateType)ACreateType;
    if ((FDocCreateType == dctDoc) && !FSpec->GetChildByName("docrules"))
     FDocCreateType = dctEmpty;
    try
     {
      FSaveOwnerCode = FImmNom->MainLPUCode;
      try
       {
//         dsLogMessage("TdsDocDM::StartDocCreate 1  >>> " + FImmNom->MainLPUCode);
        FImmNom->MainLPUCode = AAuthor;
        FDocNom->MainLPUCode = AAuthor;
        FDefNom              = FImmNom;
//         dsLogMessage("TdsDocDM::StartDocCreate 2  >>> " + AAuthor);
//         dsLogMessage("TdsDocDM::StartDocCreate 3  >>> " + FDefNom->MainLPUCode);
//         dsLogMessage("TdsDocDM::StartDocCreate 4 FDefNom >>> " + IntToStr((int)FDefNom) /* ->MainLPUCode */);
        RC = FDocCreator->StartDocCreate(FSpec, AAuthor, FFilter, AFilterNameGUI, AConcr, FDocCreateType, NewGUI(),
          APeriodDateFr, APeriodDateTo, APerType);
//         dsLogMessage("TdsDocDM::StartDocCreate 5  >>> " + FDefNom->MainLPUCode);
       }
      __finally
       {
        // FImmNom->MainLPUCode = FSaveOwnerCode;
        // FDocNom->MainLPUCode = FSaveOwnerCode;
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    if (FFilter)
     delete FFilter;
    delete FSpec;
    // !!!     FDM->ICSNom.OlePropertySet("LPUCode",  StringToOleStr(FDM->MainOwnerCode));
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::DocCreateOnComplite(System::TObject * Sender)
 {
  if (FDocCreator)
   {
    FSaveDocument(FDocCreator->DocNode, FDocCreator->SpecNode, false, true);
   }
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::DocCreateProgress(System::UnicodeString & ARecId)
 {
  UnicodeString RC = "error";
  try
   {
    /*
     RC = "<pr SM='"+IntToStr(FStageMax)
     +"' SP='"+FloatToStrF(FStageProgress, ffFixed, 3, 1)
     +"' SMsg='"+FStageMsg
     +"' CM='"+IntToStr(FCommMax)
     +"' CP='"+FloatToStrF(FCommProgress, ffFixed, 3, 1)
     +"' CMsg='"+FCommMsg
     +"' CSMsg='"+FCommSumMsg
     +"' CS='"+IntToStr(FCurStage)
     +"' SC='"+IntToStr(FStageCount)
     +"' SEC='"+IntToStr(FStageElementsCount)
     +"' LCT='"+IntToStr((int)FCallType)
     +"' LCV='"+IntToStr(FCallVal)
     +"' LCMsg='"+FCallMsg+"'/>";
     */
    if (FProgressCounter->CallType == TkabProgressType::CommComplite)
     {
      FImmNom->MainLPUCode    = FSaveOwnerCode;
      FDocNom->MainLPUCode    = FSaveOwnerCode;
      ARecId                  = GetDocGUI(FDocCreator->DocNode); // FProgressCounter->CallMsg;
      FDocCreator->OnGetSVal  = NULL;
      FDocCreator->OnGetAVal  = NULL;
      FDocCreator->OnGetDVal  = NULL;
      FDocCreator->OnProgress = NULL;
      delete FDocCreator;
      FDocCreator = NULL;
      RC          = "ok";
     }
    else if (FProgressCounter->CallType == TkabProgressType::Error)
     {
      dsLogError(FProgressCounter->CallMsg, __FUNC__);
      FImmNom->MainLPUCode    = FSaveOwnerCode;
      FDocNom->MainLPUCode    = FSaveOwnerCode;
      FDocCreator->OnGetSVal  = NULL;
      FDocCreator->OnGetAVal  = NULL;
      FDocCreator->OnGetDVal  = NULL;
      FDocCreator->OnProgress = NULL;
      delete FDocCreator;
      FDocCreator = NULL;
      RC          = "error";
     }
    else
     {
      RC = FProgressCounter->AsString;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsDocDM::MergeCondRules(TTagNode * AParent, TTagNode * ARules1, TTagNode * ARules2, bool AORType)
 {
  dsLogBegin(__FUNC__);
  TTagNode * RC = NULL;
  try
   {
    if (ARules1 && ARules2)
     RC = FMergeCondRules(ARules1, ARules2, AORType);
    else
     {
      if (ARules1)
       RC = ARules1;
      else if (AParent && ARules2)
       {
        if (AParent == ARules2)
         return ARules2;
        TTagNode * tmp = AParent->AddChild("tmp");
        tmp->AssignEx(ARules2, true);
        RC = tmp;
       }
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsDocDM::FMergeCondRules(TTagNode * ARules1, TTagNode * ARules2, bool AORType)
 {
  dsLogBegin(__FUNC__);
  if (!(ARules1 && ARules2))
   return NULL;
  TTagNode * FRules = NULL;
  try
   {
    TTagNode * tmp;
    FRules                  = new TTagNode(NULL);
    FRules->Name            = "condrules";
    FRules->AV["uniontype"] = (AORType) ? "or" : "and";
    // добавляем правила из ARules1
    // ARules1 - из типа "condrules" преобразуем в "ROR" или "RAND"
    // в зависимости от значения атрибута "uniontype"
    if (ARules1->Count)
     {
      tmp = FRules->AddChild("tmp");
      tmp->AssignEx(ARules1, true);
      tmp->Name          = "r" + ARules1->GetAVDef("uniontype", "or");
      tmp->AV["not"]     = "0";
      tmp->AV["comment"] = "";
      tmp->AV["pure"]    = "1";
      tmp->DeleteAttr("uniontype");
     }
    // добавляем правила из ARules2
    // ARules2 - из типа "condrules" преобразуем в "ROR" или "RAND"
    // в зависимости от значения атрибута "uniontype"
    if (ARules2->Count)
     {
      tmp = FRules->AddChild("tmp");
      tmp->AssignEx(ARules2, true);
      tmp->Name          = "r" + ARules2->GetAVDef("uniontype", "or");
      tmp->AV["not"]     = "0";
      tmp->AV["comment"] = "";
      tmp->AV["pure"]    = "1";
      tmp->DeleteAttr("uniontype");
     }
    // переписываем в ARules1
    if (FRules->Count)
     ARules1->AssignEx(FRules, true);
   }
  __finally
   {
    if (FRules)
     delete FRules;
   }
  dsLogEnd(__FUNC__);
  return ARules1;
 }
// ---------------------------------------------------------------------------
TCheckDocFilterResult __fastcall TdsDocDM::InternalCheckCondRules(TTagNode * ACondRules)
 {
  dsLogBegin(__FUNC__);
  // Проверка на наличие пустых секций
  if (!ACondRules->CmpName("IERef,text") && !ACondRules->Count)
   return frBad;
  // Проверка на наличие неконкретизированных секций
  if (ACondRules->CmpName("IOR,IMOR,IAND") || (ACondRules->CmpName("IERef") && !ACondRules->Count))
   return frBad;
  TTagNode * ndChild = ACondRules->GetFirstChild();
  while (ndChild)
   {
    TCheckDocFilterResult Res = InternalCheckCondRules(ndChild);
    if (Res != frOk)
     return Res;
    ndChild = ndChild->GetNext();
   }
  dsLogEnd(__FUNC__);
  return frOk;
 }
// ---------------------------------------------------------------------------
TCheckDocFilterResult __fastcall TdsDocDM::CheckCondRules(TTagNode * ACondRules)
 {
  dsLogBegin(__FUNC__);
  if (!ACondRules)
   return frNone;
  if (!ACondRules->Count)
   return frEmpty;
  dsLogEnd(__FUNC__);
  return InternalCheckCondRules(ACondRules);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::LoadDocs(TTagNode * ACreateStep, TTagNode * ADoc, TList * ADocList)
 {
  dsLogBegin(__FUNC__);
  /*
   docref      - ГУИ спецификатора интегрируемых документов
   docrefmain  -
   periodtype  - период интегрируемых документов (период, за который выбираются
   интегрируемые документы):
   this           - совпадает с периодом формируемого документа
   defined_value  - фиксированный период
   abstruct_value - относительный период (относительно даты
   формирования документа)
   perioddef   - тип периода интегрируемых документов:
   year     - год
   halfyear - полугодие
   quarter  - квартал
   month    - месяц
   unique   - уникальный
   undef    - не определен
   periodvalue - значение периода интегрируемых документов:
   <Начало>-<Окончание>
   <Длительность>
   <Начало>, <Окончание> ::= DD.MM.YYYY
   DD   - день
   MM   - месяц
   YYYY - год
   <Начало> <= <Окончание>
   <Длительность>        ::= YC/MC/DC
   YC - количество лет     [0; 999]
   MC - количество месяцев [0; 12]
   DC - количество дней    [0; 31]
   periodicity - периодичность интегрируемых документов
   year     - год
   halfyear - полугодие
   quarter  - квартал
   month    - месяц
   unique   - уникальная
   no       - отсутствует
   equal    - совпадает с периодом интегрируемых документов
   any      - любая

   org_type    - тип организации
   defined - фиксированная
   child   - все подчиненные
   this    - своя територия
   org_code    - код организации, актуален при org_type == defined

   Допустимые сочетания значений атрибутов:
   periodtype     | perioddef | periodvalue
   ==================================================
   this |     undef |
   defined_value |      year | 01.01.YYYY-31.12.YYYY
   |  halfyear | 01.01.YYYY-30.06.YYYY
   |           | 01.07.YYYY-31.12.YYYY
   |   quarter | 01.01.YYYY-31.03.YYYY
   |           | 01.04.YYYY-30.06.YYYY
   |           | 01.07.YYYY-30.09.YYYY
   |           | 01.10.YYYY-31.12.YYYY
   |     month | 01.MM.YYYY-LD.MM.YYYY
   |    unique | DD.MM.YYYY-DD.MM.YYYY
   |     undef |
   abstruct_value |     undef |              YC/MC/DC
   */
  UnicodeString FSQL;
  UnicodeString thisDocPer = GetDocPeriod(ADoc);
  UnicodeString thisDocPerFrom = GetDocPeriodValFrom(ADoc);
  UnicodeString thisDocPerTo = GetDocPeriodValTo(ADoc);
  UnicodeString intDocref = ACreateStep->AV["docref"];
  UnicodeString intPeriodtype = ACreateStep->GetAVDef("periodtype", "this"); // (this|defined_value|abstruct_value)
  UnicodeString intPerioddef = ACreateStep->GetAVDef("perioddef", "undef");
  // (year|halfyear|quarter|month|unique|undef)
  UnicodeString intPeriodvalue = ACreateStep->AV["periodvalue"];
  UnicodeString intDocPerFrom = GetPart1(intPeriodvalue, '-').Trim();
  UnicodeString intDocPerTo = GetPart2(intPeriodvalue, '-').Trim();
  UnicodeString intPeriodicity = ACreateStep->GetAVDef("periodicity", "equal");
  // (year|halfyear|quarter|month|unique|no|equal|any)
  UnicodeString intOrgType = ACreateStep->GetAVDef("org_type", "child"); // (defined|child|this)
  UnicodeString intOrgCode = ACreateStep->AV["org_code"];
  FSQL = "Select DOCTEXT From DOCUMENTS Where ";
  FSQL += "SPECGUI='" + intDocref + "' and ";
  // период интегрируемых документов
  if (intPeriodtype.LowerCase() == "this")
   { // период совпадает с периодом формируемого документа
    if (thisDocPerFrom != "")
     FSQL += "F_FROMDATE>='" + DateFS(TDateTime(thisDocPerFrom)) + "' and ";
    else
     FSQL += "F_FROMDATE is NULL and ";
    if (thisDocPerTo != "")
     FSQL += "F_TODATE<='" + DateFS(TDateTime(thisDocPerTo)) + "' and ";
    else
     FSQL += "F_TODATE is NULL and ";
   }
  else if (intPeriodtype.LowerCase() == "defined_value")
   { // определённое значение периода
    if (intPerioddef.LowerCase() != "undef")
     {
      if (intDocPerFrom != "")
       FSQL += "F_FROMDATE>='" + DateFS(TDateTime(intDocPerFrom)) + "' and ";
      else
       FSQL += "F_FROMDATE is NULL and ";
      if (intDocPerTo != "")
       FSQL += "F_TODATE<='" + DateFS(TDateTime(intDocPerTo)) + "' and ";
      else
       FSQL += "F_TODATE is NULL and ";
     }
   }
  else if (intPeriodtype.LowerCase() == "abstruct_value")
   { // относительное значение периода, относительно даты формироватия
    FSQL += "(F_FROMDATE >= '" + FImmNom->DM->IncPeriod(Date().FormatString("dd.mm.yyyy"), intPeriodvalue, 0) +
      "' and ";
    FSQL += "F_TODATE <= '" + DateFS(Date()) + "') and ";
   }
  // периодичность интегрируемых документов
  if (intPeriodicity.LowerCase() == "equal")
   { // период совпадает с периодом формируемого документа
    FSQL += "F_PERIOD = " + thisDocPer + " and ";
   }
  else if (intPeriodicity.LowerCase() == "year")
   FSQL += "F_PERIOD = 5 and ";
  else if (intPeriodicity.LowerCase() == "halfyear")
   FSQL += "F_PERIOD = 4 and ";
  else if (intPeriodicity.LowerCase() == "quarter")
   FSQL += "F_PERIOD = 3 and ";
  else if (intPeriodicity.LowerCase() == "month")
   FSQL += "F_PERIOD = 2 and ";
  else if (intPeriodicity.LowerCase() == "unique")
   FSQL += "F_PERIOD = 1 and ";
  else if (intPeriodicity.LowerCase() == "no")
   FSQL += "F_PERIOD = 0 and ";
  // организации авторы интегрируемых документов
  if (intOrgType.LowerCase() == "defined")
   { // определённая организация
    FSQL += "F_OWNER='" + intOrgCode + "' ";
   }
  else if (intOrgType.LowerCase() == "this")
   { // своя
    FSQL += "F_OWNER='" + GetThisSpecOwner() + "' ";
   }
  else if (intOrgType.LowerCase() == "child")
   { // все подчинённые
    TStringList * tmp = new TStringList;
    GetChildSpecOwnerList(tmp);
    if (tmp->Count)
     {
      FSQL += "F_OWNER in ('" + tmp->Strings[0] + "'";
      for (int i = 1; i < tmp->Count; i++)
       FSQL += ",'" + tmp->Strings[i] + "'";
      FSQL += ")";
     }
    else
     {
      FSQL += "F_OWNER='NO_OWNER' ";
     }
    delete tmp;
   }
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    try
     {
      quExec(FDQ, false, FSQL);
      while (!FDQ->Eof)
       {
        ADocList->Add((void *)(new TTagNode(NULL)));
        TStringStream * FZipDoc = new TStringStream;
        try
         {
          FZipDoc->WriteData(FDQ->FieldByName("DOCTEXT")->AsString.BytesOf(),
            FDQ->FieldByName("DOCTEXT")->AsString.Length());
          ((TTagNode *)ADocList->Items[ADocList->Count - 1])->SetZIPXML(FZipDoc);
         }
        __finally
         {
          delete FZipDoc;
         }
        FDQ->Next();
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + FSQL, __FUNC__);
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::DateFS(TDateTime ADate)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = ADate.FormatString("dd.mm.yyyy");
  try
   {
    /* if (FOnGetDateStringFormat)
     {
     FOnGetDateStringFormat(RC);
     RC = ADate.FormatString(RC);
     } */
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::GetThisSpecOwner()
 {
  return FMainOwnerCode;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::GetChildSpecOwnerList(TStringList * AChildList, bool AIsName)
 {
  dsLogBegin(__FUNC__);
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    UnicodeString tmpSQL = "Select CODE, NAME From SUBORDORG Where SUBORD=0 Order By NAME";
    try
     {
      quExec(FDQ, false, tmpSQL);
      while (!FDQ->Eof)
       {
        if (AIsName)
         AChildList->Add(FDQ->FieldByName("CODE")->AsString + "=" + FDQ->FieldByName("NAME")->AsString);
        else
         AChildList->Add(FDQ->FieldByName("CODE")->AsString);
        FDQ->Next();
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + tmpSQL, __FUNC__);
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::GetSpecNameByRef(UnicodeString AGUI)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    UnicodeString tmpSQL = "Select NAME From SPECIFIKATORS Where Upper(GUI)=:pGUI";
    try
     {
      quExecParam(FDQ, false, tmpSQL, "pGUI", AGUI.UpperCase());
      if (FDQ->RecordCount)
       RC = FDQ->FieldByName("NAME")->AsString;
      else
       RC = FMT(dsDocExtFuncNoDocDef);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + tmpSQL, __FUNC__);
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::FDocGetSVal(UnicodeString AFuncName, TTagNode * ANode, UnicodeString & ATab,
  UnicodeString & ARet)
 {
  bool RC = false;
  try
   {
    try
     {
      // dsLogMessage("TdsDocDM::FDocGetAVal FDefNom >>> " + IntToStr((int)FDefNom));
      FDefNom->GetSVal(AFuncName, ANode, ATab, ARet);
     }
    catch (Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::FDocGetAVal(UnicodeString AFuncName, UnicodeString AFields, UnicodeString ARef,
  UnicodeString AAttrRef, UnicodeString & ARet)
 {
  bool RC = false;
  try
   {
    try
     {
      // dsLogMessage("TdsDocDM::FDocGetAVal FDefNom >>> " + IntToStr((int)FDefNom) /* ->MainLPUCode */);
      // dsLogMessage("TdsDocDM::FDocGetAVal FDefNom >>> " + FDefNom->MainLPUCode );
      FDefNom->GetAVal(AFuncName, AFields, ARef, AAttrRef, ARet);
      RC = true;
     }
    catch (Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::FDocGetDVal(UnicodeString AFuncName, TTagNode * ANode, UnicodeString & AText,
  UnicodeString & AIEXML)
 {
  bool RC = false;
  try
   {
    try
     {
      // dsLogMessage("TdsDocDM::FDocGetAVal FDefNom >>> " + IntToStr((int)FDefNom) /* ->MainLPUCode */);
      FDefNom->GetDVal(AFuncName, ANode, AText, AIEXML);
      RC = true;
     }
    catch (Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::InsertData(UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId)
 {
  dsLogMessage("InsertData: ID: " + AId + ", Value: " + AValue->ToString());
  return "";
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::EditData(UnicodeString AId, TJSONObject * AValue)
 {
  dsLogMessage("EditData: ID: " + AId + ", Value: " + AValue->ToString());
  return "";
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::DeleteData(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogMessage("DeleteData: ID: " + AId + ", RecId: " + ARecId);
  return "";
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::NewGUID()
 {
  return icsNewGUID().UpperCase();
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDocDM::GetNewTabCode(System::UnicodeString ATab, System::UnicodeString AKeyFl)
 {
  UnicodeString RC = "1";
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    quExec(FDQ, false, "Select Max(" + AKeyFl + ") as MCODE From " + ATab);
    if (FDQ->RecordCount)
     {
      if (!FDQ->FieldByName("MCODE")->IsNull)
       RC = IntToStr(FDQ->FieldByName("MCODE")->AsInteger + 1);
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsDocDM::FSetMainOwnerCode(UnicodeString AVal)
 {
  FMainOwnerCode = AVal.UpperCase();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::FSaveDocument(TTagNode * ADoc, TTagNode * ASpec, bool AHandCreate, int AIntegrated)
 {
  dsLogBegin(__FUNC__);
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    if (!FDQ->Transaction->Active)
     FDQ->Transaction->StartTransaction();
    if (CanSaveDoc(ASpec) && !FNotSaveDoc)
     {
      UnicodeString FDocGUI = "";
      FDocGUI = DocCreateCheckExist(ADoc).Trim();
      if (FDocGUI.Length())
       SetDocGUI(ADoc, FDocGUI);
      FDQ->SQL->Text =
        "UPDATE OR INSERT INTO DOCUMENTS (GUI, SPECGUI, NAME, DOCTEXT, F_OWNER, F_SPECOWNER, F_PERIOD, CREATEDATE, F_FROMDATE, F_TODATE, INTEGRATED, HANDCREATE, MODULE_TYPE) values (:pGUI, :pSPECGUI, :pNAME, :pDOCTEXT, :pF_OWNER, :pF_SPECOWNER, :pF_PERIOD, :pCREATEDATE, :pF_FROMDATE, :pF_TODATE, :pINTEGRATED, :pHANDCREATE, :pMODULE_TYPE)";
     }
    else
     FDQ->SQL->Text =
       "UPDATE OR INSERT INTO TEMPDOCUMENTS (GUI, SPECGUI, NAME, DOCTEXT, F_OWNER, F_SPECOWNER, F_PERIOD, CREATEDATE, F_FROMDATE, F_TODATE, INTEGRATED, HANDCREATE, MODULE_TYPE) values (:pGUI, :pSPECGUI, :pNAME, :pDOCTEXT, :pF_OWNER, :pF_SPECOWNER, :pF_PERIOD, :pCREATEDATE, :pF_FROMDATE, :pF_TODATE, :pINTEGRATED, :pHANDCREATE, :pMODULE_TYPE)";
    FDQ->Prepare();
    FDQ->ParamByName("pGUI")->Value = GetDocGUI(ADoc);
    FDQ->ParamByName("pSPECGUI")->Value = GetDocSpecGUI(ADoc);
    FDQ->ParamByName("pNAME")->Value = GetDocName(ADoc);
    TStringStream * FZipDoc = new TStringStream;
    try
     {
      ADoc->GetZIPXML(FZipDoc);
      FDQ->ParamByName("pDOCTEXT")->Value = FZipDoc->DataString;
     }
    __finally
     {
      delete FZipDoc;
     }
    FDQ->ParamByName("pF_OWNER")->Value = GetDocOwner(ADoc);
    FDQ->ParamByName("pF_SPECOWNER")->Value = GetSpecOwner(ASpec);
    FDQ->ParamByName("pF_PERIOD")->Value = GetDocPeriod(ADoc);
    FDQ->ParamByName("pCREATEDATE")->Value = Date();
    if (GetDocPeriodValFrom(ADoc).Trim().Length())
     FDQ->ParamByName("pF_FROMDATE")->Value = GetDocPeriodValFrom(ADoc).Trim();
    if (GetDocPeriodValTo(ADoc).Trim().Length())
     FDQ->ParamByName("pF_TODATE")->Value = GetDocPeriodValTo(ADoc).Trim();
    FDQ->ParamByName("pINTEGRATED")->Value = AIntegrated;
    FDQ->ParamByName("pHANDCREATE")->Value = (int)AHandCreate;
    FDQ->ParamByName("pMODULE_TYPE")->Value = "IC7";
    FDQ->Prepare();
    FDQ->OpenOrExecute();
    if (FDQ->Transaction->Active)
     FDQ->Transaction->Commit();
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TdsDocDM::FGetLPUCode()
 {
  return LPUParam.LPUCode;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::LoadLPUParam()
 {
  dsLogBegin(__FUNC__);
  TFDQuery * FDQ = CreateDocTempQuery();
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    quExec(FDQ, false, "SELECT * FROM SUBORDORG Where SUBORD=2");
    if (FDQ->RecordCount)
     {
      LPUParam.Name     = FDQ->FieldByName("NAME")->AsString;
      LPUParam.FullName = FDQ->FieldByName("FULLNAME")->AsString;
      LPUParam.Addr     = FDQ->FieldByName("ADDR")->AsString;
      LPUParam.CODE     = FDQ->FieldByName("CODE")->AsString;
      LPUParam.Email    = FDQ->FieldByName("EMAIL")->AsString;
      LPUParam.Phone    = FDQ->FieldByName("PHONE")->AsString;
      LPUParam.OwnerFIO = FDQ->FieldByName("OwnerFIO")->AsString;
      quExec(FQ, false, "SELECT code FROM class_00c7 where Upper(r011E)='" + LPUParam.CODE.UpperCase() + "'");
      if (FQ->RecordCount)
       LPUParam.LPUCode = FQ->FieldByName("CODE")->AsInteger;
      FMainOwnerCode = LPUParam.CODE;
      // Application->Title = ProdName+"  -  [ "+UnicodeString((ICSDocEx->DM->DefDocCreateType == ctDoc)?"СБОР":"ЛПУ")+": \""+LPUParam.Name+"\", код: {"+LPUParam.CODE+"} ]";
      FImmNom->MainLPUCode = FMainOwnerCode;
      FDocNom->MainLPUCode = FMainOwnerCode;
     }
    else
     {
      // _MSG_INFA(FMT(&_icsICConnectMissingLPUParam),FMT(&_icsICCommonMsgCaption));
      // Application->Title = DM->ProdName+"  -  [ "+UnicodeString((DM->ICSDocEx->DM->DefDocCreateType == ctDoc)?"СБОР":"ЛПУ")+": \""+DM->LPUParam.Name+"\", код: {"+DM->LPUParam.CODE+"} ]";
      FMainOwnerCode    = "";
      LPUParam.Name     = "";
      LPUParam.FullName = "";
      LPUParam.Addr     = "";
      LPUParam.CODE     = "";
      LPUParam.Email    = "";
      LPUParam.Phone    = "";
     }
    // if (DM->ICSDocEx->DM->quFreeDoc->Transaction->Active) DM->ICSDocEx->DM->quFreeDoc->Transaction->Commit();
   }
  __finally
   {
    DeleteTempQuery(FQ);
    DeleteTempQuery(FDQ);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::GetMainLPUParam()
 {
  UnicodeString RC = "";
  TTagNode * FParam = new TTagNode;
  try
   {
    FParam->Name           = "passport";
    FParam->AV["code"]     = LPUParam.CODE;
    FParam->AV["name"]     = LPUParam.Name;
    FParam->AV["fullname"] = LPUParam.FullName;
    FParam->AV["addr"]     = LPUParam.Addr;
    FParam->AV["phone"]    = LPUParam.Phone;
    FParam->AV["mail"]     = LPUParam.Email;
    FParam->AV["owner"]    = LPUParam.OwnerFIO;
    RC                     = FParam->AsXML;
   }
  __finally
   {
    delete FParam;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::GetCopyName(UnicodeString AName)
 {
  UnicodeString RC = "";
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    int CopyNum = 1;
    UnicodeString CopyName;
    while (!RC.Length())
     {
      CopyName = UnicodeString(dsDocDocSpecListCopyCaption) + " (" + IntToStr(CopyNum) + ") " + AName;
      quExec(FDQ, false, "Select Count(*) as RCOUNT From SPECIFIKATORS Where Upper(NAME)= '" +
        CopyName.UpperCase() + "' ");
      if (!FDQ->FieldByName("RCOUNT")->AsInteger)
       RC = CopyName;
      CopyNum++ ;
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::SetMainLPU(__int64 ALPUCode)
 {
  if (ALPUCode)
   { // выбрано ЛПУ
    TFDQuery * FDQ = CreateDocTempQuery();
    TFDQuery * FQ = CreateTempQuery();
    try
     {
      if (!FDQ->Transaction->Active)
       FDQ->Transaction->StartTransaction();
      FDQ->SQL->Text = "UPDATE OR INSERT INTO SUBORDORG (CODE, NAME, ADDR, SUBORD, EMAIL, PHONE, FULLNAME, OWNERFIO, INSERT_DT, UPDATE_DT) \
                                                   values (:pCODE, :pNAME, :pADDR, 2, :pEMAIL, :pPHONE, :pFULLNAME, :pOWNERFIO, :pINSERT_DT, :pUPDATE_DT) \
                                                   MATCHING(CODE)";
      FDQ->Prepare();
      quExecParam(FQ, false, "Select * from CLASS_00C7 Where code=:pCode", "pCode", ALPUCode);
      FDQ->ParamByName("pCODE")->Value = FQ->FieldByName("R011E")->Value;
      FDQ->ParamByName("pNAME")->Value = FQ->FieldByName("R00CA")->Value;
      FDQ->ParamByName("pFULLNAME")->Value = FQ->FieldByName("R00CB")->Value;
      FDQ->ParamByName("pADDR")->Value = FQ->FieldByName("R00CC")->Value;
      FDQ->ParamByName("pPHONE")->Value = FQ->FieldByName("R00CE")->Value;
      FDQ->ParamByName("pEMAIL")->Value = FQ->FieldByName("R00CF")->Value;
      FDQ->ParamByName("pOWNERFIO")->Value = FQ->FieldByName("R00CD")->Value;
      FDQ->ParamByName("pINSERT_DT")->Value = FQ->FieldByName("INSERT_DT")->Value;
      FDQ->ParamByName("pUPDATE_DT")->Value = FQ->FieldByName("UPDATE_DT")->Value;
      FDQ->OpenOrExecute();
      if (FQ->Transaction->Active)
       FQ->Transaction->Commit();
      if (FDQ->Transaction->Active)
       FDQ->Transaction->Commit();
      UnicodeString SaveMainOwnerCode = FMainOwnerCode;
      LoadLPUParam();
      if (SaveMainOwnerCode.UpperCase() != FMainOwnerCode.UpperCase())
       { // код организации сменился, обновляем описания и документы
        quExecParam(FDQ, true, "Update SPECIFIKATORS Set F_OWNER = :pNewOwner Where F_OWNER=:pOldOwner", "pNewOwner",
          FMainOwnerCode, "", "pOldOwner", SaveMainOwnerCode);
        quExecParam(FDQ, true, "Update DOCUMENTS Set F_OWNER =:pNewOwner Where F_OWNER=:pOldOwner", "pNewOwner",
          FMainOwnerCode, "", "pOldOwner", SaveMainOwnerCode);
        quExecParam(FDQ, true, "Update DOCUMENTS Set F_SPECOWNER =:pNewOwner Where F_SPECOWNER=:pOldOwner", "pNewOwner",
          FMainOwnerCode, "", "pOldOwner", SaveMainOwnerCode);
        TStringList * tmpDocList = NULL;
        try
         {
          tmpDocList = new TStringList;
          // описания документов - автор
          tmpDocList->Clear();
          quExecParam(FDQ, false, "Select GUI, SPECTEXT From  SPECIFIKATORS Where F_OWNER =:pNewOwner", "pNewOwner",
            FMainOwnerCode);
          while (!FDQ->Eof)
           {
            try
             {
              tmpDocList->AddObject(FDQ->FieldByName("GUI")->AsString, (TObject *) new TTagNode(NULL));
              ((TTagNode *)tmpDocList->Objects[tmpDocList->Count - 1])->AsXML = FDQ->FieldByName("SPECTEXT")->AsString;
             }
            catch (...)
             {
             }
            FDQ->Next();
           }
          for (int i = 0; i < tmpDocList->Count; i++)
           {
            SetSpecOwner(((TTagNode *)tmpDocList->Objects[i]), FMainOwnerCode);
            quExecParam(FDQ, false, "Update SPECIFIKATORS Set SPECTEXT = :pSPECTEXT Where Upper(GUI) = :pGUI",
              "pSPECTEXT", ((TTagNode *)tmpDocList->Objects[i])->AsXML, "", "pGUI", tmpDocList->Strings[i].UpperCase());
           }
          if (FDQ->Transaction->Active)
           FDQ->Transaction->Commit();
          for (int i = 0; i < tmpDocList->Count; i++)
           {
            delete(TTagNode *)tmpDocList->Objects[i];
           }
          // документы - автор
          tmpDocList->Clear();
          quExecParam(FDQ, false, "Select GUI, DOCTEXT From  DOCUMENTS Where F_OWNER =:pNewOwner", "pNewOwner",
            FMainOwnerCode);
          while (!FDQ->Eof)
           {
            try
             {
              tmpDocList->AddObject(FDQ->FieldByName("GUI")->AsString, (TObject *) new TTagNode(NULL));
              TStringStream * FZipDoc = new TStringStream;
              try
               {
                FZipDoc->WriteData(FDQ->FieldByName("DOCTEXT")->AsString.BytesOf(),
                  FDQ->FieldByName("DOCTEXT")->AsString.Length());
                ((TTagNode *)tmpDocList->Objects[tmpDocList->Count - 1])->SetZIPXML(FZipDoc);
               }
              __finally
               {
                delete FZipDoc;
               }
             }
            catch (...)
             {
             }
            FDQ->Next();
           }
          for (int i = 0; i < tmpDocList->Count; i++)
           {
            SetSpecOwner(((TTagNode *)tmpDocList->Objects[i]), FMainOwnerCode);
            TStringStream * FZipDoc = new TStringStream;
            try
             {
              ((TTagNode *)tmpDocList->Objects[i])->GetZIPXML(FZipDoc);
              quExecParam(FDQ, false, "Update DOCUMENTS Set DOCTEXT = :pDOCTEXT Where Upper(GUI) = :pGUI", "pDOCTEXT",
                FZipDoc->DataString, "", "pGUI", tmpDocList->Strings[i].UpperCase());
             }
            __finally
             {
              delete FZipDoc;
             }
           }
          if (FDQ->Transaction->Active)
           FDQ->Transaction->Commit();
          for (int i = 0; i < tmpDocList->Count; i++)
           {
            delete(TTagNode *)tmpDocList->Objects[i];
           }
         }
        __finally
         {
          if (tmpDocList)
           delete tmpDocList;
         }
       }
     }
    __finally
     {
      DeleteTempQuery(FQ);
      DeleteTempQuery(FDQ);
     }
   }
 }
// ---------------------------------------------------------------------------
TNomGetExtValuesEvent __fastcall TdsDocDM::FGetOnExtGetAVal()
 {
  if (FImmNom)
   return FImmNom->OnExtGetAVal;
  else if (FDocNom)
   return FDocNom->OnExtGetAVal;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::FSetOnExtGetAVal(TNomGetExtValuesEvent AValue)
 {
  if (FImmNom)
   FImmNom->OnExtGetAVal = AValue;
  if (FDocNom)
   FDocNom->OnExtGetAVal = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::SaveDoc(UnicodeString AId)
 {
  /* dsLogBegin(__FUNC__);
   TFDQuery *FDQ1 = CreateDocTempQuery();
   TFDQuery *FDQ2 = CreateDocTempQuery();
   try
   {
   quExecParam(FDQ1, false, "Select * From TEMPDOCUMENTS where Upper(GUI)=:pGUI", "pGUI", AId.UpperCase());
   if (FDQ1->RecordCount)
   {
   FDQ2->Close();
   if (!FDQ2->Transaction->Active) FDQ2->Transaction->StartTransaction();
   FDQ2->SQL->Text = "UPDATE OR INSERT INTO DOCUMENTS (GUI, SPECGUI, NAME, DOCTEXT, F_OWNER, F_SPECOWNER, F_PERIOD, CREATEDATE, F_FROMDATE, F_TODATE, INTEGRATED, HANDCREATE, MODULE_TYPE) values (:pGUI, :pSPECGUI, :pNAME, :pDOCTEXT, :pF_OWNER, :pF_SPECOWNER, :pF_PERIOD, :pCREATEDATE, :pF_FROMDATE, :pF_TODATE, :pINTEGRATED, :pHANDCREATE, :pMODULE_TYPE)";
   FDQ2->Prepare();
   FDQ2->ParamByName("pGUI")->Value          = FDQ1->FieldByName("GUI")->Value;
   FDQ2->ParamByName("pSPECGUI")->Value      = FDQ1->FieldByName("SPECGUI")->Value;
   FDQ2->ParamByName("pNAME")->Value         = FDQ1->FieldByName("NAME")->Value;
   FDQ2->ParamByName("pDOCTEXT")->Value      = FDQ1->FieldByName("DOCTEXT")->Value;
   *//*
   -           UnicodeString tmpVal = FDQ->FieldByName("DOCTEXT")->AsString;
   -           FZipDoc->WriteData(tmpVal.BytesOf(),tmpVal.Length());
   -           ADoc->SetZIPXML(FZipDoc);

   TStringStream *FZipDoc = new TStringStream;
   try
   {
   ADoc->GetZIPXML(FZipDoc);
   FDQ2->ParamByName("pDOCTEXT")->Value   = FZipDoc->DataString;
   }
   __finally
   {
   delete FZipDoc;
   }
   *//*
   FDQ2->ParamByName("pF_OWNER")->Value      = FDQ1->FieldByName("F_OWNER")->Value;
   FDQ2->ParamByName("pF_SPECOWNER")->Value  = FDQ1->FieldByName("F_SPECOWNER")->Value;
   FDQ2->ParamByName("pF_PERIOD")->Value     = FDQ1->FieldByName("F_PERIOD")->Value;
   FDQ2->ParamByName("pCREATEDATE")->Value   = FDQ1->FieldByName("CREATEDATE")->Value;
   FDQ2->ParamByName("pF_FROMDATE")->Value   = FDQ1->FieldByName("F_FROMDATE")->Value;
   FDQ2->ParamByName("pF_TODATE")->Value     = FDQ1->FieldByName("F_TODATE")->Value;
   FDQ2->ParamByName("pINTEGRATED")->Value   = FDQ1->FieldByName("INTEGRATED")->Value;
   FDQ2->ParamByName("pHANDCREATE")->Value   = FDQ1->FieldByName("HANDCREATE")->Value;
   FDQ2->ParamByName("pMODULE_TYPE")->Value  = FDQ1->FieldByName("MODULE_TYPE")->Value;
   FDQ2->Prepare();
   FDQ2->OpenOrExecute();
   if (FDQ2->Transaction->Active) FDQ2->Transaction->Commit();
   }
   }
   __finally
   {
   DeleteTempQuery(FDQ1);
   DeleteTempQuery(FDQ2);
   }
   dsLogEnd(__FUNC__); */
 }
// ---------------------------------------------------------------------------
/*
 void __fastcall TdsDocDM::LoadMKB()
 {
 FMKBData.clear();
 FMKBExtData.clear();
 FXMLList->GetXML("mkb")->Iterate(FLoadMKB);
 }
 //---------------------------------------------------------------------------
 bool __fastcall TdsDocDM::FLoadMKB(TTagNode* ANode)
 {
 if (ANode->CmpName("i"))
 {
 FMKBData["##"+ANode->AV["c"].UpperCase()]    = ANode->AV["n"];
 FMKBExtData["##"+ANode->AV["c"].UpperCase()] = ANode->AV["e"].UpperCase();
 }
 return false;
 }
 //---------------------------------------------------------------------------
 UnicodeString __fastcall TdsDocDM::MKBCodeToText(UnicodeString ACode)
 {
 // переводим кодовое отображение mkb в текстовое
 UnicodeString RC = "";
 try
 {
 if (ACode.Pos("##"))
 {
 TAnsiStrMap::iterator FRC = FMKBData.find(ACode);
 if (FRC != FMKBData.end())
 {
 RC = FMKBExtData[FRC->first]+": "+FRC->second;
 }
 }
 else
 RC = ACode;
 }
 __finally
 {
 }
 return RC;
 }
 //----------------------------------------------------------------------------
 */
TJSONObject * __fastcall TdsDocDM::GetQList(UnicodeString AId)
 {
  TJSONObject * RC = new TJSONObject;
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    UnicodeString FSQL = "";
    if (SameText(AId, "report") || SameText(AId, "planreport"))
     {
      FSQL = "Select distinct sp.NAME, sp.GUI From SPECIFIKATORS sp join AOPTIONS ao on (Upper(sp.gui)=Upper(ao.val)) Where Upper(ao.name)='"
        + AId.UpperCase() + "' Order by NAME";
     }
    else if (SameText(AId, "comreport") || SameText(AId, "complanreport"))
     {
      FSQL = "Select distinct NAME, GUI From SPECIFIKATORS Order by NAME";
     }
    else if (SameText(AId, "filter"))
     {
      FSQL = "Select distinct sp.NAME, sp.GUI From FILTERS sp join AOPTIONS ao on (Upper(sp.gui)=Upper(ao.val)) Where Upper(ao.name)='"
        + AId.UpperCase() + "' Order by NAME";
     }
    else if (SameText(AId, "comfilter"))
     {
      FSQL = "Select distinct NAME, GUI From FILTERS Order by NAME";
     }
    quExec(FDQ, false, FSQL);
    UnicodeString FDefGUI = AppOpt->Vals["defreport"].AsString;
    while (!FDQ->Eof)
     {
      if (SameText(FDQ->FieldByName("GUI")->AsString, FDefGUI))
       {
        RC->AddPair(FDQ->FieldByName("GUI")->AsString, FDQ->FieldByName("name")->AsString);
        break;
       }
      FDQ->Next();
     }
    FDQ->First();
    while (!FDQ->Eof)
     {
      if (!SameText(FDQ->FieldByName("GUI")->AsString, FDefGUI))
       RC->AddPair(FDQ->FieldByName("GUI")->AsString, FDQ->FieldByName("name")->AsString);
      FDQ->Next();
     }
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::SetQList(UnicodeString AId, TJSONObject * AList)
 {
  TFDQuery * FDQ = CreateDocTempQuery();
  try
   {
    UnicodeString FSQL = "Insert Into AOPTIONS (name, val) values (:pname, :pval)";
    UnicodeString FDelSQL = "Delete from AOPTIONS Where Upper(Name)=:pName";
    quExecParam(FDQ, true, FDelSQL, "pName", AId.UpperCase());
    bool FFirst = true;
    TJSONPairEnumerator * itPair = AList->GetEnumerator();
    while (itPair->MoveNext())
     {
      if (FFirst)
       {
        // !!!???           AppOpt->Vals["defreport"] = ((TJSONString*)itPair->Current->JsonString)->Value();
        AppOpt->Vals["defreport"] = JSONEnumPairCode(itPair);
        FFirst                    = false;
       }
      // !!!???        quExecParam(FDQ, false, FSQL, "pname", AId, "", "pval", ((TJSONString*)itPair->Current->JsonString)->Value());
      quExecParam(FDQ, false, FSQL, "pname", AId, "", "pval", JSONEnumPairCode(itPair));
     }
    if (FDQ->Transaction->Active)
     FDQ->Transaction->Commit();
   }
  __finally
   {
    DeleteTempQuery(FDQ);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::AppOptLoadXML(TTagNode * AOptNode)
 {
  /*
   UnicodeString FXMLPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml");
   UnicodeString FFN = icsPrePath(FXMLPath + "\\ICS_APP_OPT.zxml");
   if (FileExists(FFN))
   {
   AOptNode->LoadFromZIPXMLFile(FFN);
   }
   else
   dsLogError("Ошибка загрузки описания настроек 'defxml\\ICS_APP_OPT.zxml'", __FUNC__);
   */
  AOptNode->AsXML = FXMLList->GetXML("ICS_APP_OPT")->AsXML;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::GetClassData(UnicodeString ACode, UnicodeString AName)
 {
  UnicodeString RC = "";
  try
   {
    RC = FClassData->GetData(AName, ACode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::GetMIBPName(int AGUIType, UnicodeString ACode)
 {
  UnicodeString RC = "error";
  try
   {
    dsLogMessage(IntToStr(AGUIType) + " : " + ACode, __FUNC__, 5);
    try
     {
      switch (AGUIType)
       {
       case 1:
         {
          RC = GetClassData(ACode, "0035"); /* xName = "R0040"; xTab = "CLASS_0035"; */
          break;
         } // вакцины
       case 2:
         {
          RC = GetClassData(ACode, "002A"); /* xName = "R002C"; xTab = "CLASS_002A"; */
          break;
         } // пробы
       case 3:
         {
          RC = GetClassData(ACode, "013E"); /* xName = "R013F"; xTab = "CLASS_013E"; */
          break;
         } // проверки
       }
     }
    catch (Exception & E)
     {
      RC = E.Message;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::GetVacType(UnicodeString ASrc)
 {
  UnicodeString RC = "Ошибка : " + ASrc;
  try
   {
    UnicodeString FType, FLastType;
    UnicodeString FTypeList = ASrc;
    UnicodeString FInfType = GetLPartB(FTypeList, ';');
    FLastType = GetRPartB(FInfType, '.');
    bool FAllEqu = true;
    while (FInfType.Length() && FAllEqu)
     {
      FType     = GetRPartB(FInfType, '.');
      FAllEqu   = (FLastType == FType);
      FTypeList = GetRPartB(FTypeList, ';');
      FInfType  = GetLPartB(FTypeList, ';');
     }
    if (FAllEqu)
     RC = FLastType;
    else
     {
      RC       = "";
      FInfType = GetLPartB(FTypeList, ';');
      while (FInfType.Length())
       {
        RC += GetRPartB(FInfType, '.') + " " + GetClassData(GetLPartB(FInfType, '.'), "003A");
        FTypeList = GetRPartB(FTypeList, ';');
        FInfType  = GetLPartB(FTypeList, ';');
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
