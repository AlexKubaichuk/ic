/*
  ���������:

  27.11.2004 - ������� �.�.
        ��������� ������:
        Variant __fastcall TXMLTab::GetTab() const { return FTab; }
        void __fastcall TXMLTab::SetWidth( int ARow, int ACol, float fWidth, TXMLRulerStyle ARulerStyle = wdAdjustNone );
        �������� ��� TXMLRulerStyle

  29.06.2004 - ������� �.�.
        ��������� ������:
        void __fastcall TXMLTab::SetLeftIndent(int ARow, int ACol, float fCentimeters );
        void __fastcall TXMLTab::AppendRow(int ACount = 1);

        ��������� PACKAGE ��� �������� ������� ������

        ��������� �������� ������������ ���� Icsword

        ������ ������� � ����� MICSComp

        ������� �����
        void __fastcall TXMLTab::Preview(UnicodeString fName, UnicodeString ACaption = "", bool AOnSaveAs = false);
        ��
        void __fastcall TXMLTab::Preview(UnicodeString fName, UnicodeString ACaption = "", bool AOnSaveAs = false, int AHelpContext = 0 );
*/
//---------------------------------------------------------------------------

#ifndef IcsXMLDocH
#define IcsXMLDocH
//---------------------------------------------------------------------------
#include "AxeUtil.h"

namespace Icsxmldoc
{

//---------------------------------------------------------------------------

#define OleStr(a) StringToOleStr(a)
//Border type ()
#define  brdTop 0xFFFFFFFF   //r
#define  brdLeft 0xFFFFFFFE
#define  brdBottom 0xFFFFFFFD
#define  brdRight 0xFFFFFFFC
#define  brdHorizontal 0xFFFFFFFB
#define  brdVertical 0xFFFFFFFA

//---------------------------------------------------------------------------
//��������� - 27.11.2004 - ������� �.�.
//Controls the way Word adjusts cell widths.
//��� ��������� � Visual Basic ����� WdRulerStyle MS Office 97/2000/XP/2003
enum TXMLRulerStyle {
  wdAdjustNone = 0,             //Sets the width of all selected cells or columns to the specified value. Word preserves the width of all non-selected columns, shifting them to the right or left as necessary.
  wdAdjustSameWidth = 3,        //Sets the width of the cells in the first column only to the specified value. Word preserves the right edge of the table by adjusting the width of all other cells or columns to the same value.
  wdAdjustFirstColumn = 2,      //Sets the width of the cells in the first column only to the specified value. If there is more than one column, Word preserves the right edge of the table and the positions of the other columns.
  wdAdjustProportional = 1      //Sets the width of the cells in the first column only to the specified value. If multiple columns are selected, Word preserves the right edge of the table and the positions of the non-selected columns by proportionally adjusting the width of the other selected columns. If only one cell or column is selected, Word preserves the right edge of the table by proportionally adjusting the width of the other cells or columns.
};
//---------------------------------------------------------------------------
class TXMLTab;
//!!!class TXMLBorder;
//---------------------------------------------------------------------------
class PACKAGE EdsXMLError : public System::Sysutils::Exception
{
public:
  __fastcall EdsXMLError(UnicodeString AMsg);
};
//---------------------------------------------------------------------------
class PACKAGE TXMLDoc : public TObject
{
private:        // User declarations
        TTagNode *FDoc;
        TTagNode *FHead;
        TTagNode *FBody;
        TTagNode *FPageSetup;
        TStringList *tmpList;
        bool    FIsOpen;
        UnicodeString    FDocName;
        UnicodeString    FDOTName;
        void __fastcall ExtPreview(UnicodeString fName);
        UnicodeString __fastcall GetAPILastErrorFormatMessage( int &nErrorCode );
        void __fastcall FCreateHTML(TTagNode *ANode);
        UnicodeString __fastcall RepNonXML(UnicodeString ASrc, bool ARepSpace, bool ARepFormatTags);
        int __fastcall GetTM();
        int __fastcall GetLM();
        int __fastcall GetRM();
        int __fastcall GetBM();
        int __fastcall GetPO();
  UnicodeString __fastcall FGetAsString();
public:         // User declarations
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        void __fastcall SetDoc(TTagNode* ADoc);
        void __fastcall AsStrings(TStringList *ADest);
        TTagNode* __fastcall AsTagNode();
        bool __fastcall Open(UnicodeString fName);
        bool __fastcall Load(TTagNode *ASrc);
        TXMLTab* __fastcall GetTab(int AInd);
        void __fastcall DeleteTab(int AInd);
        __property int TopMargin = {read=GetTM};
        __property int LeftMargin = {read=GetLM};
        __property int RightMargin = {read=GetRM};
        __property int BottomMargin = {read=GetBM};
        __property int Orientation = {read=GetPO};
  __property UnicodeString AsString = {read = FGetAsString};
        void __fastcall SetTitle(UnicodeString ATitle);
        UnicodeString __fastcall GetTitle();
        void __fastcall InsertFile(UnicodeString AFileName);
        void __fastcall Preview(UnicodeString fName, UnicodeString ACaption = "", bool AOnSaveAs = false, int AHelpContext = 0, bool AShowExt = false);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        virtual __fastcall TXMLDoc();
        virtual __fastcall ~TXMLDoc();
};
//---------------------------------------------------------------------------
class PACKAGE TXMLTab :  public TTagNode
{
private:        // User declarations
//!!!        TXMLBorder* __fastcall FBorder(int ARow, int ACol, int brdType);
        TTagNode* __fastcall GetCell(int ARow, int ACol);
public:         // User declarations
//        inline void __fastcall SetTab(Variant ATab){FTab = ATab;};
        //��������� - 27.11.2004 - ������� �.�.
//        inline Variant __fastcall GetTab() const { return FTab; }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        long __fastcall Width(int ARow, int ACol);
        //��������� - 27.11.2004 - ������� �.�.
        void __fastcall SetWidth( int ARow, int ACol, float fWidth, TXMLRulerStyle ARulerStyle = wdAdjustNone );
        //-------------------------------------
        void __fastcall Delete();
        void __fastcall SetText(int ARow, int ACol, UnicodeString AText);
        //��������� - 23.06.2004 - ������� �.�.
        void __fastcall SetLeftIndent(int ARow, int ACol, float fCentimeters );
        //-------------------------------------
        void __fastcall SetTexture(int ARow, int ACol, int APercent);
        void __fastcall SetBold(int ARow, int ACol, bool ABold);
        void __fastcall SetFontColor(int ARow, int ACol, int AColor);
        void __fastcall AddRow(int ABeforeRow, int ACount = 1/*, TProgressBar *prBar = NULL*/);
        //��������� - 23.06.2004 - ������� �.�.
        void __fastcall AppendRow(int ACount = 1);
        //-------------------------------------
        void __fastcall AddCol(int ABeforeCol, int ACount = 1);
        void __fastcall DeleteRow(int ARow, int ACount = 1);
        void __fastcall DeleteCol(int ACol, int ACount = 1);
        void __fastcall Merge(int AFrRow, int AFrCol, int AToRow, int AToCol);
        void __fastcall MergeCol(int ARow, int AFrCol, int AToCol);
        void __fastcall MergeRow(int ACol, int AFrRow, int AToRow);
        void __fastcall SetHAlign(int ARow, int ACol, int AAlign);
        void __fastcall SetVAlign(int ARow, int ACol, int AAlign);
        void __fastcall Paste(int ARow, int ACol, UnicodeString APref = "");
//!!!        TXMLBorder* __fastcall TopBorder(int ARow, int ACol);
//!!!        TXMLBorder* __fastcall LeftBorder(int ARow, int ACol);
//!!!        TXMLBorder* __fastcall BottomBorder(int ARow, int ACol);
//!!!        TXMLBorder* __fastcall RightBorder(int ARow, int ACol);
//!!!        TXMLBorder* __fastcall HorizontalBorder(int ARow, int ACol);
//!!!        TXMLBorder* __fastcall VerticalBorder(int ARow, int ACol);
        int __fastcall RowCount();
        int __fastcall ColCount();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        __fastcall TXMLTab(TTagNode *ASrc);
};
//---------------------------------------------------------------------------
//!!!
/*
class PACKAGE TXMLBorder :  public TObject
{
private:        // User declarations
        Variant FBorder;
        UnicodeString    FDocName;
public:         // User declarations
        void __fastcall SetBorder(Variant ABorder);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        void __fastcall SetArtStyle(int AStyle);
        void __fastcall SetArtWidth(int AWidth);
        void __fastcall SetColor(int AColor);
        void __fastcall SetColorIndex(int AColor);
        void __fastcall SetLineStyle(int ALineStyle);
        void __fastcall SetLineWidth(int AWidth);
        void __fastcall SetVisible(bool AVis);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        __fastcall TXMLBorder();
};
*/
//---------------------------------------------------------------------------

} //end of namespace Icsword
using namespace Icsxmldoc;
#endif
