//---------------------------------------------------------------------------
#ifndef dsDocDocCreatorUnitH
#define dsDocDocCreatorUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
//#include "DocCreatorDM.h"
//#include "DocExDFM.h"

#include "dsDocDMUnit.h"
#include "dsDocSQLCreator.h"
//#include "doctypes.h"
//---------------------------------------------------------------------------

typedef  map<int,int> TCodeList;
typedef  map<const UnicodeString, TCodeList*> TStrTreeMap;

//ctNone - ���,
//ctEmpty - ������,
//ctBase - �� ����,
//ctDoc - �� ���������,
//ctBaseDoc  - �� ���� � �� ���������};

//---------------------------------------------------------------------------
class PACKAGE TdsDocCreator : public TObject
{
private:
  typedef void __fastcall (__closure *TdcSyncMethod)(void);

  TTagNode *FDoc;
  TTagNode *FSpec;
  TTagNode *FErrMsgNode;

  TTagNode *FFilter;
  TTagNode *FDocCondRules;

  TdsDocCreateType  FDocCreateType;
  TdsDocSQLCreator  *FSQLCreator;

  UnicodeString FAuthor, FGUI;
  UnicodeString FFilterNameGUI;
  bool FConcr;
  int FPerType;
  TTagNodeMap FSpecRefNodes;
  int FSpecRefNodesCount;


//        TTagNode     *FExtDocCondRules;
//        TFDocDM      *FDM;
        TdsDocDM     *FDM;

  bool         FDebugCreate;
  TFDQuery *FQ, *FQ1, *FDQ;
  TDate     FDateFr, FDateTo;

  TReplaceFlags fdRepl;
  TTagNode     *FDocDebugXML;
  UnicodeString   FExtFilterName,FExtFilterValue;
  int          FRowColNum;

  UnicodeString FLogMsg, FLogFuncName;
  int FLogALvl;
  TTime         FLogExecTime;

  TkabProgress FOnProgress;
  TdsDocGetAVal FOnGetAVal;
  TdsDocGetDVal FOnGetDVal;

  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;
  TOnLogSQLEvent      FOnLogSQL;

  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;


  class TBackgroundDocCreator;
  TBackgroundDocCreator * FBkgCreator;
  TkabProgressType FPrType;
  int              FPrVal;
  UnicodeString    FPrMsg;


        TStrTreeMap  FUnitCodes;
/*
        void __fastcall GetISLinkPath(TTagNode *ALinks, UnicodeString AISRef, TStringList *ATabList);
        bool __fastcall FGetISLinkPath(TTagNode *ALinks, UnicodeString AISRef, TStringList *ASearchPath, TStringList *ATabList);
        UnicodeString __fastcall GetRefByType(UnicodeString ABaseGUI, UnicodeString AISRef);
        UnicodeString __fastcall PrepareRef(TTagNode *ANode, UnicodeString AISRef);
*/
  bool __fastcall FGetDocItemByRef(TTagNode * ANode, UnicodeString & ARef);
  void __fastcall SyncCall(TdcSyncMethod AMethod);
  void __fastcall SyncCallOnProgress();

  void __fastcall LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl = 1);
  void __fastcall SyncCallLogMsg();
  void __fastcall LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl = 1);
  void __fastcall SyncCallLogErr();
  void __fastcall LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime = 0, int ALvl = 1);
  void __fastcall SyncCallLogSQL();
  void __fastcall LogBegin(UnicodeString AFuncName);
  void __fastcall SyncCallLogBegin();
  void __fastcall LogEnd(UnicodeString AFuncName);
  void __fastcall SyncCallLogEnd();

  bool __fastcall CreateDocument();

  TdsDocGetSVal __fastcall FGetOnGetSVal();
  void __fastcall FSetOnGetSVal(TdsDocGetSVal AVal);

        UnicodeString __fastcall DateFS(TDateTime ADate);

        UnicodeString __fastcall GetOwnerName(UnicodeString AOwnerCode, UnicodeString AFieldName = "NAME");
        void       __fastcall __DBGPrint(UnicodeString AMessage);
        UnicodeString __fastcall FGetFilterValues(TTagNode *ADocCondRules);
        bool       __fastcall FGetConValues(TTagNode *itTag, UnicodeString &ARC);
        UnicodeString __fastcall GetCondValues(TTagNode *AIENode, bool ACheckInclToName);
        void       __fastcall DoLoadTable(TTagNode *ATab, int &FRCount, int &FCCount);
        void       __fastcall LoadTable(UnicodeString ATabUID, TTagNode *ADocTab,float* *FTab, int FRCount, int FCCount);
        void       __fastcall SaveTable(UnicodeString ATabUID, TTagNode *ADocTab,float* *FTab, int FRCount, int FCCount);
        void       __fastcall CreateSign(TTagNode *ADoc,TTagNode *ASpec);
        bool       __fastcall CreateDocFromDoc(TTagNode *ASpec,TTagNode *ADoc);
        void       __fastcall CreateContent(TTagNode *ASpec,TTagNode *AParent);
        void       __fastcall CreateInscription(TTagNode *ASpec,TTagNode *AParent);
        UnicodeString __fastcall GetRefValue(UnicodeString ARef);
        UnicodeString __fastcall GetCalcValue(TTagNode *ACalcRules);

        void       __fastcall RecalcTable(TTagNode *ATabDef, TTagNode *AParent);

        void       __fastcall CreateList(TTagNode *ASpec,TTagNode *AParent);
        void       __fastcall CreateBaseTabFields(TTagNode *ASpec,TStringList *AColList, UnicodeString ABaseISRef);
        void       __fastcall CreateLinkTabFields(TTagNode *ALinkIS, TStringList *AColList);
        UnicodeString __fastcall GetLinkISKey(UnicodeString ALinkISRef, UnicodeString ABaseISRef);
        UnicodeString __fastcall GetAttrFields(UnicodeString ARefIS);
        UnicodeString __fastcall GetListFlValue(TTagNode* AISRef,bool AIsLink);
        void       __fastcall GetLinkISValue(TTagNode *AISLink, TTagNode *AParent, UnicodeString ABaseISRef);
        void       __fastcall CreateTable(TTagNode *ATabDef, TTagNode *AParent);
        void       __fastcall GetTabItems(TTagNode *AItemList, TStringList *AItems, TTagNode *ARules, UnicodeString AItemType);
        void       __fastcall GetTabSelItems(UnicodeString ABaseISRef, TTagNode *AItemRoot, TStringList *AItems, TTagNode *ARules, UnicodeString AItemType, UnicodeString AItemTypeCyr, TTagNode *DebItems = NULL);
        void       __fastcall FGetTabSelItems(UnicodeString ABaseISRef, TTagNode *AItem, UnicodeString AItemType, UnicodeString AItemTypeCyr, TCodeList *AParentCodes, TTagNode *DebItems = NULL);
        void       __fastcall FCalcTabData(TStringList *AItems,int ACount, float* *ATab, bool ARow);
        long       __fastcall GetCellCondData(UnicodeString ABaseISRef,TTagNode *ARow,TTagNode *ACol,TTagNode *ACond);
        float      __fastcall GetCellCalcData(TTagNode *ACalcNode,float** ATab,bool ARow, int AInd);
        TTagNode*  __fastcall MergeCondRules(TTagNode *AParent,TTagNode *ARules1, TTagNode *ARules2, bool AORType);
        TTagNode*  __fastcall FMergeCondRules(TTagNode *ARules1, TTagNode *ARules2, bool AORType);
        bool       __fastcall FGetColCount(TTagNode *itTag, UnicodeString &ACount);
        bool       __fastcall FGetRowCount(TTagNode *itTag, UnicodeString &ACount);
        int        __fastcall GetTabIndex(UnicodeString AItemUID, TTagNode *ATabItemsSpec);
        bool       __fastcall FGetItemsCount(TTagNode *itTag, UnicodeString &ACount);
        void       __fastcall FSetProgress(TkabProgressType AType = TkabProgressType::StageInc, int AVal = 0, UnicodeString AMsg = "");
       UnicodeString __fastcall FGetNomAVal(UnicodeString AProcName, UnicodeString ADocPerDate, UnicodeString AFields, UnicodeString ARef, UnicodeString AAttrRef);
  void __fastcall MergeSpecTabCondRules(TTagNode * ASpec, TTagNode * ATabCRList);
public:		// User declarations
//        TAxeXMLContainer *FXMLS;
//        HINSTANCE FExtProc;
        __fastcall TdsDocCreator(TdsDocDM *ADM);
        __fastcall ~TdsDocCreator();
        bool __fastcall DocCreate(TTagNode *ASpec, UnicodeString AAuthor, TTagNode *ADoc, TTagNode *AFilter, bool AConcr, TdsDocCreateType ActType, UnicodeString AGUI, TDate ADateFr, TDate ADateTo, int APerType);
        UnicodeString __fastcall StartDocCreate(TTagNode *ASpec, UnicodeString AAuthor, TTagNode *AFilter, UnicodeString AFilterNameGUI, bool AConcr,
                                         TdsDocCreateType ActType, UnicodeString AGUI, TDate ADateFr, TDate ADateTo, int APerType);

        __property TkabProgress OnProgress = {read=FOnProgress, write=FOnProgress};
        __property bool   DebugCreate = {read=FDebugCreate, write=FDebugCreate};
        __property TTagNode*  DocDebugXML = {read=FDocDebugXML};
  __property TTagNode*  DocNode = {read=FDoc};
  __property TTagNode*  SpecNode = {read=FSpec};

  __property TdsDocGetSVal OnGetSVal = {read=FGetOnGetSVal, write=FSetOnGetSVal};
  __property TdsDocGetAVal OnGetAVal = {read=FOnGetAVal, write=FOnGetAVal};
  __property TdsDocGetDVal OnGetDVal = {read=FOnGetDVal, write=FOnGetDVal};

  __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
  __property TOnLogMsgErrEvent OnLogError   = {read = FOnLogError, write = FOnLogError};
  __property TOnLogSQLEvent    OnLogSQL     = {read = FOnLogSQL, write = FOnLogSQL};

  __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
  __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};
};
//---------------------------------------------------------------------------
#endif
//---------------------------------------------------------------------------

