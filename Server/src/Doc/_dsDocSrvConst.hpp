﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'dsDocSrvConst.pas' rev: 28.00 (Windows)

#ifndef DsdocsrvconstHPP
#define DsdocsrvconstHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dsdocsrvconst
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE System::ResourceString _dsDocCaption;
#define Dsdocsrvconst_dsDocCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocErrorTagEq;
#define Dsdocsrvconst_dsDocErrorTagEq System::LoadResourceString(&Dsdocsrvconst::_dsDocErrorTagEq)
extern DELPHI_PACKAGE System::ResourceString _dsDocErrorXMLContainerNotDefined;
#define Dsdocsrvconst_dsDocErrorXMLContainerNotDefined System::LoadResourceString(&Dsdocsrvconst::_dsDocErrorXMLContainerNotDefined)
extern DELPHI_PACKAGE System::ResourceString _dsDocErrorDocNotDefined;
#define Dsdocsrvconst_dsDocErrorDocNotDefined System::LoadResourceString(&Dsdocsrvconst::_dsDocErrorDocNotDefined)
extern DELPHI_PACKAGE System::ResourceString _dsDocErrorGUINotValid;
#define Dsdocsrvconst_dsDocErrorGUINotValid System::LoadResourceString(&Dsdocsrvconst::_dsDocErrorGUINotValid)
extern DELPHI_PACKAGE System::ResourceString _dsDocErrorCommonDop;
#define Dsdocsrvconst_dsDocErrorCommonDop System::LoadResourceString(&Dsdocsrvconst::_dsDocErrorCommonDop)
extern DELPHI_PACKAGE System::ResourceString _dsDocErrorCreateOwnerNotValid;
#define Dsdocsrvconst_dsDocErrorCreateOwnerNotValid System::LoadResourceString(&Dsdocsrvconst::_dsDocErrorCreateOwnerNotValid)
extern DELPHI_PACKAGE System::ResourceString _dsDocErrorInputErrorCaption;
#define Dsdocsrvconst_dsDocErrorInputErrorCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocErrorInputErrorCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocErrorReqFieldCaption;
#define Dsdocsrvconst_dsDocErrorReqFieldCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocErrorReqFieldCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocExExtFuncErrorCreateDocView;
#define Dsdocsrvconst_dsDocExExtFuncErrorCreateDocView System::LoadResourceString(&Dsdocsrvconst::_dsDocExExtFuncErrorCreateDocView)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListCopyCaption;
#define Dsdocsrvconst_dsDocDocSpecListCopyCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocDocSpecListCopyCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocSQLCreatorErrorCondStructure;
#define Dsdocsrvconst_dsDocSQLCreatorErrorCondStructure System::LoadResourceString(&Dsdocsrvconst::_dsDocSQLCreatorErrorCondStructure)
extern DELPHI_PACKAGE System::ResourceString _dsDocSQLCreatorErrorFuncMissing;
#define Dsdocsrvconst_dsDocSQLCreatorErrorFuncMissing System::LoadResourceString(&Dsdocsrvconst::_dsDocSQLCreatorErrorFuncMissing)
extern DELPHI_PACKAGE System::ResourceString _dsDocBaseEditCommonTBCaption;
#define Dsdocsrvconst_dsDocBaseEditCommonTBCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocBaseEditCommonTBCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocBaseEditEditTBCaption;
#define Dsdocsrvconst_dsDocBaseEditEditTBCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocBaseEditEditTBCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocBaseEditErrorSetDomainNum;
#define Dsdocsrvconst_dsDocBaseEditErrorSetDomainNum System::LoadResourceString(&Dsdocsrvconst::_dsDocBaseEditErrorSetDomainNum)
extern DELPHI_PACKAGE System::ResourceString _dsDocBaseEditErrorGetNom;
#define Dsdocsrvconst_dsDocBaseEditErrorGetNom System::LoadResourceString(&Dsdocsrvconst::_dsDocBaseEditErrorGetNom)
extern DELPHI_PACKAGE System::ResourceString _dsDocBaseEditErrorGetDomain;
#define Dsdocsrvconst_dsDocBaseEditErrorGetDomain System::LoadResourceString(&Dsdocsrvconst::_dsDocBaseEditErrorGetDomain)
extern DELPHI_PACKAGE System::ResourceString _dsDocBaseEditErrorNoXML;
#define Dsdocsrvconst_dsDocBaseEditErrorNoXML System::LoadResourceString(&Dsdocsrvconst::_dsDocBaseEditErrorNoXML)
extern DELPHI_PACKAGE System::ResourceString _dsDocBaseEditErrorGenUID;
#define Dsdocsrvconst_dsDocBaseEditErrorGenUID System::LoadResourceString(&Dsdocsrvconst::_dsDocBaseEditErrorGenUID)
extern DELPHI_PACKAGE System::ResourceString _dsDocGlobals;
#define Dsdocsrvconst_dsDocGlobals System::LoadResourceString(&Dsdocsrvconst::_dsDocGlobals)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterCaption;
#define Dsdocsrvconst_dsDocFilterCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterFilterTBCaption;
#define Dsdocsrvconst_dsDocFilterFilterTBCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterFilterTBCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterPassportCaption;
#define Dsdocsrvconst_dsDocFilterPassportCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterPassportCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterPassportHint;
#define Dsdocsrvconst_dsDocFilterPassportHint System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterPassportHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterCustomLoadSaveCaption;
#define Dsdocsrvconst_dsDocFilterCustomLoadSaveCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterCustomLoadSaveCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterCustomLoadSaveHint;
#define Dsdocsrvconst_dsDocFilterCustomLoadSaveHint System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterCustomLoadSaveHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterCustomLoadCaption;
#define Dsdocsrvconst_dsDocFilterCustomLoadCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterCustomLoadCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterCustomLoadHint;
#define Dsdocsrvconst_dsDocFilterCustomLoadHint System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterCustomLoadHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterORCaption;
#define Dsdocsrvconst_dsDocFilterORCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterORCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterORHint;
#define Dsdocsrvconst_dsDocFilterORHint System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterORHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterANDCaption;
#define Dsdocsrvconst_dsDocFilterANDCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterANDCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterANDHint;
#define Dsdocsrvconst_dsDocFilterANDHint System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterANDHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterLogicNOTCaption;
#define Dsdocsrvconst_dsDocFilterLogicNOTCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterLogicNOTCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterLogicNOTHint;
#define Dsdocsrvconst_dsDocFilterLogicNOTHint System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterLogicNOTHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterNOTCaption;
#define Dsdocsrvconst_dsDocFilterNOTCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterNOTCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterNOTHint;
#define Dsdocsrvconst_dsDocFilterNOTHint System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterNOTHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterAddCondCaption;
#define Dsdocsrvconst_dsDocFilterAddCondCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterAddCondCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterAddCondHint;
#define Dsdocsrvconst_dsDocFilterAddCondHint System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterAddCondHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterDeleteCaption;
#define Dsdocsrvconst_dsDocFilterDeleteCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterDeleteCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterDeleteHint;
#define Dsdocsrvconst_dsDocFilterDeleteHint System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterDeleteHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterKonkrCaption;
#define Dsdocsrvconst_dsDocFilterKonkrCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterKonkrCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterKonkrHint;
#define Dsdocsrvconst_dsDocFilterKonkrHint System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterKonkrHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterDeKonkrCaption;
#define Dsdocsrvconst_dsDocFilterDeKonkrCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterDeKonkrCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterDeKonkrHint;
#define Dsdocsrvconst_dsDocFilterDeKonkrHint System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterDeKonkrHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterCommentCaption;
#define Dsdocsrvconst_dsDocFilterCommentCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterCommentCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterCommentHint;
#define Dsdocsrvconst_dsDocFilterCommentHint System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterCommentHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterCheckCaption;
#define Dsdocsrvconst_dsDocFilterCheckCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterCheckCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterCheckHint;
#define Dsdocsrvconst_dsDocFilterCheckHint System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterCheckHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterXMLTitle;
#define Dsdocsrvconst_dsDocFilterXMLTitle System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterXMLTitle)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterErrorSetIsFilter;
#define Dsdocsrvconst_dsDocFilterErrorSetIsFilter System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterErrorSetIsFilter)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterErrorLoadNomDLL;
#define Dsdocsrvconst_dsDocFilterErrorLoadNomDLL System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterErrorLoadNomDLL)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterErrorFreeNomDLL;
#define Dsdocsrvconst_dsDocFilterErrorFreeNomDLL System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterErrorFreeNomDLL)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterErrorCallProc;
#define Dsdocsrvconst_dsDocFilterErrorCallProc System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterErrorCallProc)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterErrorCaption;
#define Dsdocsrvconst_dsDocFilterErrorCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterErrorCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterConformCaption;
#define Dsdocsrvconst_dsDocFilterConformCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterConformCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterCheckOkCaption;
#define Dsdocsrvconst_dsDocFilterCheckOkCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterCheckOkCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterCheckEmptyCaption;
#define Dsdocsrvconst_dsDocFilterCheckEmptyCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterCheckEmptyCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterCheckNoBodyCaption;
#define Dsdocsrvconst_dsDocFilterCheckNoBodyCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterCheckNoBodyCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterNewCaption;
#define Dsdocsrvconst_dsDocFilterNewCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterNewCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterNewAuthorCaption;
#define Dsdocsrvconst_dsDocFilterNewAuthorCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterNewAuthorCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterErrorEmptyGroup;
#define Dsdocsrvconst_dsDocFilterErrorEmptyGroup System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterErrorEmptyGroup)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterErrorEmptyValue;
#define Dsdocsrvconst_dsDocFilterErrorEmptyValue System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterErrorEmptyValue)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterErrorIS;
#define Dsdocsrvconst_dsDocFilterErrorIS System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterErrorIS)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterErrorISDomain;
#define Dsdocsrvconst_dsDocFilterErrorISDomain System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterErrorISDomain)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterEditCaption;
#define Dsdocsrvconst_dsDocFilterEditCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterEditCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterEditOkBtnCaption;
#define Dsdocsrvconst_dsDocFilterEditOkBtnCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterEditOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterEditCancelBtnCaption;
#define Dsdocsrvconst_dsDocFilterEditCancelBtnCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterEditCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterEditHelpBtnCaption;
#define Dsdocsrvconst_dsDocFilterEditHelpBtnCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterEditHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocFilterUtilsFilterVersion;
#define Dsdocsrvconst_dsDocFilterUtilsFilterVersion System::LoadResourceString(&Dsdocsrvconst::_dsDocFilterUtilsFilterVersion)
extern DELPHI_PACKAGE System::ResourceString _dsDocOptions;
#define Dsdocsrvconst_dsDocOptions System::LoadResourceString(&Dsdocsrvconst::_dsDocOptions)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes01;
#define Dsdocsrvconst_dsDocParsersLexTypes01 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes01)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes02;
#define Dsdocsrvconst_dsDocParsersLexTypes02 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes02)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes03;
#define Dsdocsrvconst_dsDocParsersLexTypes03 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes03)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes04;
#define Dsdocsrvconst_dsDocParsersLexTypes04 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes04)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes05;
#define Dsdocsrvconst_dsDocParsersLexTypes05 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes05)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes06;
#define Dsdocsrvconst_dsDocParsersLexTypes06 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes06)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes07;
#define Dsdocsrvconst_dsDocParsersLexTypes07 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes07)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes08;
#define Dsdocsrvconst_dsDocParsersLexTypes08 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes08)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes09;
#define Dsdocsrvconst_dsDocParsersLexTypes09 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes09)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes10;
#define Dsdocsrvconst_dsDocParsersLexTypes10 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes10)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes11;
#define Dsdocsrvconst_dsDocParsersLexTypes11 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes11)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes12;
#define Dsdocsrvconst_dsDocParsersLexTypes12 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes12)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes13;
#define Dsdocsrvconst_dsDocParsersLexTypes13 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes13)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes14;
#define Dsdocsrvconst_dsDocParsersLexTypes14 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes14)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes15;
#define Dsdocsrvconst_dsDocParsersLexTypes15 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes15)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes16;
#define Dsdocsrvconst_dsDocParsersLexTypes16 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes16)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes17;
#define Dsdocsrvconst_dsDocParsersLexTypes17 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes17)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes18;
#define Dsdocsrvconst_dsDocParsersLexTypes18 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes18)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes19;
#define Dsdocsrvconst_dsDocParsersLexTypes19 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes19)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes20;
#define Dsdocsrvconst_dsDocParsersLexTypes20 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes20)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexTypes21;
#define Dsdocsrvconst_dsDocParsersLexTypes21 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexTypes21)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexClasses1;
#define Dsdocsrvconst_dsDocParsersLexClasses1 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexClasses1)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexClasses2;
#define Dsdocsrvconst_dsDocParsersLexClasses2 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexClasses2)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexClasses3;
#define Dsdocsrvconst_dsDocParsersLexClasses3 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexClasses3)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexClasses4;
#define Dsdocsrvconst_dsDocParsersLexClasses4 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexClasses4)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexClasses5;
#define Dsdocsrvconst_dsDocParsersLexClasses5 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexClasses5)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexClasses6;
#define Dsdocsrvconst_dsDocParsersLexClasses6 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexClasses6)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexClasses7;
#define Dsdocsrvconst_dsDocParsersLexClasses7 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexClasses7)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexClasses8;
#define Dsdocsrvconst_dsDocParsersLexClasses8 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexClasses8)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexClasses9;
#define Dsdocsrvconst_dsDocParsersLexClasses9 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexClasses9)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexErrors1;
#define Dsdocsrvconst_dsDocParsersLexErrors1 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexErrors1)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexErrors2;
#define Dsdocsrvconst_dsDocParsersLexErrors2 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexErrors2)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexErrors3;
#define Dsdocsrvconst_dsDocParsersLexErrors3 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexErrors3)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexErrors4;
#define Dsdocsrvconst_dsDocParsersLexErrors4 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexErrors4)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexErrors5;
#define Dsdocsrvconst_dsDocParsersLexErrors5 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexErrors5)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexErrors6;
#define Dsdocsrvconst_dsDocParsersLexErrors6 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexErrors6)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexErrors7;
#define Dsdocsrvconst_dsDocParsersLexErrors7 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexErrors7)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexErrors8;
#define Dsdocsrvconst_dsDocParsersLexErrors8 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexErrors8)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexErrors9;
#define Dsdocsrvconst_dsDocParsersLexErrors9 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexErrors9)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSyntErrors01;
#define Dsdocsrvconst_dsDocParsersSyntErrors01 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSyntErrors01)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSyntErrors02;
#define Dsdocsrvconst_dsDocParsersSyntErrors02 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSyntErrors02)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSyntErrors03;
#define Dsdocsrvconst_dsDocParsersSyntErrors03 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSyntErrors03)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSyntErrors04;
#define Dsdocsrvconst_dsDocParsersSyntErrors04 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSyntErrors04)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSyntErrors05;
#define Dsdocsrvconst_dsDocParsersSyntErrors05 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSyntErrors05)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSyntErrors06;
#define Dsdocsrvconst_dsDocParsersSyntErrors06 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSyntErrors06)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSyntErrors07;
#define Dsdocsrvconst_dsDocParsersSyntErrors07 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSyntErrors07)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSyntErrors08;
#define Dsdocsrvconst_dsDocParsersSyntErrors08 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSyntErrors08)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSyntErrors09;
#define Dsdocsrvconst_dsDocParsersSyntErrors09 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSyntErrors09)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSyntErrors10;
#define Dsdocsrvconst_dsDocParsersSyntErrors10 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSyntErrors10)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSyntErrors11;
#define Dsdocsrvconst_dsDocParsersSyntErrors11 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSyntErrors11)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSyntErrors12;
#define Dsdocsrvconst_dsDocParsersSyntErrors12 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSyntErrors12)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSyntErrors13;
#define Dsdocsrvconst_dsDocParsersSyntErrors13 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSyntErrors13)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSemErrors01;
#define Dsdocsrvconst_dsDocParsersSemErrors01 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSemErrors01)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSemErrors02;
#define Dsdocsrvconst_dsDocParsersSemErrors02 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSemErrors02)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSemErrors03;
#define Dsdocsrvconst_dsDocParsersSemErrors03 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSemErrors03)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSemErrors04;
#define Dsdocsrvconst_dsDocParsersSemErrors04 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSemErrors04)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSemErrors05;
#define Dsdocsrvconst_dsDocParsersSemErrors05 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSemErrors05)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSemErrors06;
#define Dsdocsrvconst_dsDocParsersSemErrors06 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSemErrors06)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSemErrors07;
#define Dsdocsrvconst_dsDocParsersSemErrors07 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSemErrors07)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSemErrors08;
#define Dsdocsrvconst_dsDocParsersSemErrors08 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSemErrors08)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSemErrors09;
#define Dsdocsrvconst_dsDocParsersSemErrors09 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSemErrors09)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSemErrors10;
#define Dsdocsrvconst_dsDocParsersSemErrors10 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSemErrors10)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSemErrors11;
#define Dsdocsrvconst_dsDocParsersSemErrors11 System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSemErrors11)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersErrorPos;
#define Dsdocsrvconst_dsDocParsersErrorPos System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersErrorPos)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersErrorExprLex;
#define Dsdocsrvconst_dsDocParsersErrorExprLex System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersErrorExprLex)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersErrorExprSynt;
#define Dsdocsrvconst_dsDocParsersErrorExprSynt System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersErrorExprSynt)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersErrorExprSem;
#define Dsdocsrvconst_dsDocParsersErrorExprSem System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersErrorExprSem)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersOk;
#define Dsdocsrvconst_dsDocParsersOk System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersOk)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersLexError;
#define Dsdocsrvconst_dsDocParsersLexError System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersLexError)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSyntError;
#define Dsdocsrvconst_dsDocParsersSyntError System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSyntError)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersSemError;
#define Dsdocsrvconst_dsDocParsersSemError System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersSemError)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersNotConst;
#define Dsdocsrvconst_dsDocParsersNotConst System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersNotConst)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersAndConst;
#define Dsdocsrvconst_dsDocParsersAndConst System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersAndConst)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersOrConst;
#define Dsdocsrvconst_dsDocParsersOrConst System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersOrConst)
extern DELPHI_PACKAGE System::ResourceString _dsDocParsersErrorPolandStr;
#define Dsdocsrvconst_dsDocParsersErrorPolandStr System::LoadResourceString(&Dsdocsrvconst::_dsDocParsersErrorPolandStr)
extern DELPHI_PACKAGE System::ResourceString _dsDocSpecEmptyInsCaption;
#define Dsdocsrvconst_dsDocSpecEmptyInsCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocSpecEmptyInsCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocSpecGetCountCaption;
#define Dsdocsrvconst_dsDocSpecGetCountCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocSpecGetCountCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocSpecUtilsReqVersion;
#define Dsdocsrvconst_dsDocSpecUtilsReqVersion System::LoadResourceString(&Dsdocsrvconst::_dsDocSpecUtilsReqVersion)
extern DELPHI_PACKAGE System::ResourceString _dsDocViewerNumTCellLTitle;
#define Dsdocsrvconst_dsDocViewerNumTCellLTitle System::LoadResourceString(&Dsdocsrvconst::_dsDocViewerNumTCellLTitle)
extern DELPHI_PACKAGE System::ResourceString _dsDocViewerErrorSetDocFormat;
#define Dsdocsrvconst_dsDocViewerErrorSetDocFormat System::LoadResourceString(&Dsdocsrvconst::_dsDocViewerErrorSetDocFormat)
extern DELPHI_PACKAGE System::ResourceString _dsDocViewerErrorNoSpecDef;
#define Dsdocsrvconst_dsDocViewerErrorNoSpecDef System::LoadResourceString(&Dsdocsrvconst::_dsDocViewerErrorNoSpecDef)
extern DELPHI_PACKAGE System::ResourceString _dsDocViewerErrorNoDocDef;
#define Dsdocsrvconst_dsDocViewerErrorNoDocDef System::LoadResourceString(&Dsdocsrvconst::_dsDocViewerErrorNoDocDef)
extern DELPHI_PACKAGE System::ResourceString _dsDocViewerErrorCreateMSWord;
#define Dsdocsrvconst_dsDocViewerErrorCreateMSWord System::LoadResourceString(&Dsdocsrvconst::_dsDocViewerErrorCreateMSWord)
extern DELPHI_PACKAGE System::ResourceString _dsDocViewerErrorPreviewTypeVal;
#define Dsdocsrvconst_dsDocViewerErrorPreviewTypeVal System::LoadResourceString(&Dsdocsrvconst::_dsDocViewerErrorPreviewTypeVal)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorCaption;
#define Dsdocsrvconst_dsDocDocCreatorCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorStageByCommCaption;
#define Dsdocsrvconst_dsDocDocCreatorStageByCommCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorStageByCommCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorDocLoadConf;
#define Dsdocsrvconst_dsDocDocCreatorDocLoadConf System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorDocLoadConf)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorDocLoadConfCaption;
#define Dsdocsrvconst_dsDocDocCreatorDocLoadConfCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorDocLoadConfCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorInitCaption;
#define Dsdocsrvconst_dsDocDocCreatorInitCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorInitCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorInscCreateCaption;
#define Dsdocsrvconst_dsDocDocCreatorInscCreateCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorInscCreateCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorListCreateCaption;
#define Dsdocsrvconst_dsDocDocCreatorListCreateCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorListCreateCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorErrorISLink;
#define Dsdocsrvconst_dsDocDocCreatorErrorISLink System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorErrorISLink)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateGetCount;
#define Dsdocsrvconst_dsDocDocCreatorTableCreateGetCount System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorTableCreateGetCount)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateRows;
#define Dsdocsrvconst_dsDocDocCreatorTableCreateRows System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorTableCreateRows)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateRow;
#define Dsdocsrvconst_dsDocDocCreatorTableCreateRow System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorTableCreateRow)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateCols;
#define Dsdocsrvconst_dsDocDocCreatorTableCreateCols System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorTableCreateCols)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateCol;
#define Dsdocsrvconst_dsDocDocCreatorTableCreateCol System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorTableCreateCol)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateCeills;
#define Dsdocsrvconst_dsDocDocCreatorTableCreateCeills System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorTableCreateCeills)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateCeill;
#define Dsdocsrvconst_dsDocDocCreatorTableCreateCeill System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorTableCreateCeill)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateCeillMsg;
#define Dsdocsrvconst_dsDocDocCreatorTableCreateCeillMsg System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorTableCreateCeillMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorErrorMissingColRowDef;
#define Dsdocsrvconst_dsDocDocCreatorErrorMissingColRowDef System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorErrorMissingColRowDef)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorErrorCeilCalc;
#define Dsdocsrvconst_dsDocDocCreatorErrorCeilCalc System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorErrorCeilCalc)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeroidFromCaption;
#define Dsdocsrvconst_dsDocDocCreatorPeroidFromCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorPeroidFromCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeroidTo1Caption;
#define Dsdocsrvconst_dsDocDocCreatorPeroidTo1Caption System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorPeroidTo1Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeroidTo2Caption;
#define Dsdocsrvconst_dsDocDocCreatorPeroidTo2Caption System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorPeroidTo2Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorUndefPeroidCaption;
#define Dsdocsrvconst_dsDocDocCreatorUndefPeroidCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorUndefPeroidCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityEqual;
#define Dsdocsrvconst_dsDocDocCreatorPeriodicityEqual System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorPeriodicityEqual)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityYear;
#define Dsdocsrvconst_dsDocDocCreatorPeriodicityYear System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorPeriodicityYear)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityHalfYear;
#define Dsdocsrvconst_dsDocDocCreatorPeriodicityHalfYear System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorPeriodicityHalfYear)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityQuarter;
#define Dsdocsrvconst_dsDocDocCreatorPeriodicityQuarter System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorPeriodicityQuarter)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityMonth;
#define Dsdocsrvconst_dsDocDocCreatorPeriodicityMonth System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorPeriodicityMonth)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityUnique;
#define Dsdocsrvconst_dsDocDocCreatorPeriodicityUnique System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorPeriodicityUnique)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityNo;
#define Dsdocsrvconst_dsDocDocCreatorPeriodicityNo System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorPeriodicityNo)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityAny;
#define Dsdocsrvconst_dsDocDocCreatorPeriodicityAny System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorPeriodicityAny)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorAuthorDefined;
#define Dsdocsrvconst_dsDocDocCreatorAuthorDefined System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorAuthorDefined)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorAuthorThis;
#define Dsdocsrvconst_dsDocDocCreatorAuthorThis System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorAuthorThis)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorAuthorAllChild;
#define Dsdocsrvconst_dsDocDocCreatorAuthorAllChild System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorAuthorAllChild)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorErrorNoIntDocs;
#define Dsdocsrvconst_dsDocDocCreatorErrorNoIntDocs System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorErrorNoIntDocs)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorErrorCreateRowColNamesNotValid;
#define Dsdocsrvconst_dsDocDocCreatorErrorCreateRowColNamesNotValid System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorErrorCreateRowColNamesNotValid)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorErrorRowColReferNotValid;
#define Dsdocsrvconst_dsDocDocCreatorErrorRowColReferNotValid System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorErrorRowColReferNotValid)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorSpecPrepare;
#define Dsdocsrvconst_dsDocDocCreatorSpecPrepare System::LoadResourceString(&Dsdocsrvconst::_dsDocDocCreatorSpecPrepare)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodLocStr1;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodLocStr1 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodLocStr1)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodLocStr2;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodLocStr2 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodLocStr2)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodLocStr3;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodLocStr3 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodLocStr3)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodLocStr4;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodLocStr4 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodLocStr4)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodLocStr5;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodLocStr5 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodLocStr5)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodLocStr6;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodLocStr6 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodLocStr6)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrUniq1;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodValStrUniq1 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodValStrUniq1)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrUniq2;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodValStrUniq2 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodValStrUniq2)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrUniq3;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodValStrUniq3 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodValStrUniq3)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrMonth;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodValStrMonth System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodValStrMonth)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrQuart1;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodValStrQuart1 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodValStrQuart1)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrQuart2;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodValStrQuart2 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodValStrQuart2)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrQuart3;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodValStrQuart3 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodValStrQuart3)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrQuart4;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodValStrQuart4 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodValStrQuart4)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrHalfYear1;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodValStrHalfYear1 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodValStrHalfYear1)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrHalfYear2;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodValStrHalfYear2 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodValStrHalfYear2)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrYear;
#define Dsdocsrvconst_dsDocExtFuncDocPeriodValStrYear System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncDocPeriodValStrYear)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncErrorReadSettingCaption;
#define Dsdocsrvconst_dsDocExtFuncErrorReadSettingCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncErrorReadSettingCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncErrorReadSettingMsg;
#define Dsdocsrvconst_dsDocExtFuncErrorReadSettingMsg System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncErrorReadSettingMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncCreateDocViewMsg;
#define Dsdocsrvconst_dsDocExtFuncCreateDocViewMsg System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncCreateDocViewMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncErrorCreateDocView;
#define Dsdocsrvconst_dsDocExtFuncErrorCreateDocView System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncErrorCreateDocView)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncReplaceDocMsg;
#define Dsdocsrvconst_dsDocExtFuncReplaceDocMsg System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncReplaceDocMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncReplaceDocCaption;
#define Dsdocsrvconst_dsDocExtFuncReplaceDocCaption System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncReplaceDocCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncMissingDocDef;
#define Dsdocsrvconst_dsDocExtFuncMissingDocDef System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncMissingDocDef)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncNoDocDef;
#define Dsdocsrvconst_dsDocExtFuncNoDocDef System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncNoDocDef)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncFileVersionString;
#define Dsdocsrvconst_dsDocExtFuncFileVersionString System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncFileVersionString)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncRecCount1;
#define Dsdocsrvconst_dsDocExtFuncRecCount1 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncRecCount1)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncRecCount234;
#define Dsdocsrvconst_dsDocExtFuncRecCount234 System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncRecCount234)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncRecCount;
#define Dsdocsrvconst_dsDocExtFuncRecCount System::LoadResourceString(&Dsdocsrvconst::_dsDocExtFuncRecCount)
}	/* namespace Dsdocsrvconst */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_DSDOCSRVCONST)
using namespace Dsdocsrvconst;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DsdocsrvconstHPP
