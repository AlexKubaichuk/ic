//---------------------------------------------------------------------------
#ifndef dsDocSrvTypeDefH
#define dsDocSrvTypeDefH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include "XMLContainer.h"
#include "fmtdef.h"
//---------------------------------------------------------------------------
typedef void __fastcall (__closure *TdsDocGetDateStringFormat)(UnicodeString &ARC);
typedef void __fastcall (__closure *TdsDocGetSVal)(UnicodeString AFuncName, TTagNode *ANode, UnicodeString &ATab, UnicodeString &ARet);
typedef bool __fastcall (__closure *TdsDocGetAVal)(UnicodeString AFuncName, UnicodeString AFields, UnicodeString ARef, UnicodeString AAttrRef, UnicodeString &ARet);
typedef bool __fastcall (__closure *TdsDocGetDVal)(UnicodeString AFuncName, TTagNode *ANode, UnicodeString &AText, UnicodeString &AIEXML);

//---------------------------------------------------------------------------
#endif

