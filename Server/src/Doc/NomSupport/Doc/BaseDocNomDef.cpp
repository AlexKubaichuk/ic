//---------------------------------------------------------------------------
#pragma hdrstop
#include <System.DateUtils.hpp>
#include "BaseDocNomDef.h"
//#include "DocPeriod.h"
//#include "PackPeriod.h"

//---------------------------------------------------------------------------
typedef map<const UnicodeString, UnicodeString> TStrMap;
TStrMap HashSel;
TStrMap ClassHashSel;
//---------------------------------------------------------------------------
#define flVal(a) AFields->Values[AFields->Names[a]]
//---------------------------------------------------------------------------
__fastcall TICSBaseDocNomDef::TICSBaseDocNomDef(TFDQuery *AQuery, TAxeXMLContainer *AXMLList, UnicodeString ARegGUI)
: TICSBaseNomDef(AQuery, AXMLList, ARegGUI)
{
  dsLogC(__FUNC__);
  dsLogC(__FUNC__, true);
}
//---------------------------------------------------------------------------
__fastcall TICSBaseDocNomDef::~TICSBaseDocNomDef()
{
  dsLogD(__FUNC__);
  dsLogD(__FUNC__, true);
}
//---------------------------------------------------------------------------
TGetAttrFunc __fastcall TICSBaseDocNomDef::GetAttrMethodAddr(UnicodeString AFuncName)
{
  TGetAttrFunc RC = NULL;

  if (AFuncName == "ASBOrg")          RC = ASBOrg;
  else if (AFuncName == "AYNVal")          RC = AYNVal;
  else if (AFuncName == "ADocSpecOwner")   RC = ADocSpecOwner;
  else if (AFuncName == "ADocPeriod")      RC = ADocPeriod;
  else if (AFuncName == "ADocPeriodicity") RC = ADocPeriodicity;

  if (!RC)
   RC = TICSBaseNomDef::GetAttrMethodAddr(AFuncName);
  return RC;
}
//---------------------------------------------------------------------------
TGetDefFunc __fastcall TICSBaseDocNomDef::GetDefMethodAddr(UnicodeString AFuncName)
{
  TGetDefFunc RC = NULL;

  if (AFuncName == "DefDocDef")              RC = DefDocDef;
  else if (AFuncName == "DefSBOrg")          RC = DefSBOrg;
  else if (AFuncName == "DefDocSpecOwner")   RC = DefDocSpecOwner;
  else if (AFuncName == "DefDocPeriod")      RC = DefDocPeriod;
  else if (AFuncName == "DefDocPeriodicity") RC = DefDocPeriodicity;
  else if (AFuncName == "DefDocYNVal")       RC = DefDocYNVal;

  if (!RC)
   RC = TICSBaseNomDef::GetDefMethodAddr(AFuncName);
  return RC;
}
//---------------------------------------------------------------------------
TGetSQLFunc __fastcall TICSBaseDocNomDef::GetSQLMethodAddr(UnicodeString AFuncName)
{
  TGetSQLFunc RC = NULL;

  if (AFuncName == "SQLDocDef")              RC = SQLDocDef;
  else if (AFuncName == "SQLSBOrg")          RC = SQLSBOrg;
  else if (AFuncName == "SQLDocSpecOwner")   RC = SQLDocSpecOwner;
  else if (AFuncName == "SQLDocPeriod")      RC = SQLDocPeriod;
  else if (AFuncName == "SQLDocPeriodicity") RC = SQLDocPeriodicity;
  else if (AFuncName == "SQLDocYNVal")       RC = SQLDocYNVal;

  if (!RC)
   RC = TICSBaseNomDef::GetSQLMethodAddr(AFuncName);
  return RC;
}
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#                   ��������� ��������� �������� ���������                   #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ASBOrg(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Count)
   {
     int FCode = GetPart2(AFields->Strings[0],'=').ToIntDef(-1);
     switch (FCode)
      {
       case 0: {ARet = "�����������"; break;}
       case 1: {ARet = "�����������"; break;}
       case 2: {ARet = "����"; break;}
      }
   }
  else
   ARet = "";
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::AYNVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Values[AFields->Names[0]] == "1")  ARet = "��";
  else                                            ARet = "���";
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ADocSpecOwner(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  FDM->qtExec("Select Name From SUBORDORG Where Upper(CODE)='"+AFields->Values[AFields->Names[0]].UpperCase()+"'");
  if (FDM->Query->RecordCount)
   ARet = FDM->Query->FieldByName("Name")->AsString;
  else
   ARet = "";
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ADocPeriod(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = GetDocPeriodValStr(AFields->Values[AFields->Names[0]], AFields->Values[AFields->Names[1]], AFields->Values[AFields->Names[2]]);
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ADocPeriodicity(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = "";
  switch (AFields->Values[AFields->Names[0]].ToIntDef(-1))
   {
     case 0: { ARet = "�����������"; break;}
     case 1: { ARet = "����������"; break;}
     case 2: { ARet = "�����"; break;}
     case 3: { ARet = "�������"; break;}
     case 4: { ARet = "���������"; break;}
     case 5: { ARet = "���"; break;}
   }
}
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#            ��������� ��������� �������� �������������� ��������� (Def...)  #
//#                                                                            #
//#            ��������� ������������ ������ SQL-�������             (SQL...)  #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
bool __fastcall TICSBaseDocNomDef::DefDocDef(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE)
{
  if (!FDM) return false;
  bool RC = false;
  TStringList *FValues = NULL;
  TStringList *FValues2 = NULL;
  try
   {
      FValues = new TStringList;
      FValues2 = new TStringList;

      FDM->qtExec("Select GUI, NAME From SPECIFIKATORS Order by NAME");
      int idx = 1;
      while (!FDM->Query->Eof)
       {
         FValues->AddObject(FDM->Query->FieldByName("name")->AsString,(TObject*)idx);
         FValues2->AddObject(FDM->Query->FieldByName("GUI")->AsString.UpperCase(),(TObject*)idx);
         idx++;
         FDM->Query->Next();
       }
      RC = FDM->DefChVal(AIE, AText, AXMLIE, FValues, FValues2);
   }
  __finally
   {
     if (FValues) delete FValues;
     if (FValues2) delete FValues2;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::SQLDocDef(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseDocNomDef::DefSBOrg(TTagNode *AIE, UnicodeString& AText,  UnicodeString& AXMLIE)
{
  if (!FDM) return false;
  bool RC = false;
  TStringList *FValues = NULL;
  try
   {
      FValues = new TStringList;

      FValues->AddObject("�����������",(TObject*)0);
      FValues->AddObject("�����������",(TObject*)1);
      FValues->AddObject("����",(TObject*)2);

      RC = FDM->DefChVal(AIE, AText, AXMLIE, FValues);
   }
  __finally
   {
     if (FValues) delete FValues;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::SQLSBOrg(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseDocNomDef::DefDocSpecOwner(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE)
{
  if (!FDM) return false;
  bool RC = false;
  TStringList *FValues = NULL;
  TStringList *FValues2 = NULL;
  try
   {
      FValues = new TStringList;
      FValues2 = new TStringList;
      FDM->qtExec("Select code, Name From SUBORDORG Order by NAME");
      int idx = 1;
      while (!FDM->Query->Eof)
       {
         FValues->AddObject(FDM->Query->FieldByName("name")->AsString,(TObject*)idx);
         FValues2->AddObject(FDM->Query->FieldByName("code")->AsString.UpperCase(),(TObject*)idx);
         idx++;
         FDM->Query->Next();
       }
      RC = FDM->DefChVal(AIE, AText, AXMLIE, FValues, FValues2);
   }
  __finally
   {
     if (FValues) delete FValues;
     if (FValues2) delete FValues2;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::SQLDocSpecOwner(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseDocNomDef::DefDocPeriod(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE)
{
/*
   // AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "����"
   // {
   //   1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
   //                            1,2 ��� �������; 0 - �������� ���
   //                                             1 - ������ �� ������� ����
   //                                             2 - ������ �� ����
   //   2-� - ���� ��;   ��� ���� "�������� ���"
   //                    ��� ����� "������ �� ..." �������� �������
   //   3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
   //                    ��� ���� "������ �� ������� ����"  - �� �������������;
   //   4-� - �������� - ��� ���� "�������� ���"  - �� �������������
   //         �������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
   if (!FDM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (AIE->GetFirstChild())
    {
      if (AIE->Count)
       {
         tmp = FISCondRules->AV["name"]+" = ";
         __tmp = AIE->GetFirstChild();
         int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
         bool FLast = (bool)(DType&1);
         DType = DType >> 1;
         if (FLast)
          {
            TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
            if (!FISAttr->AV["add_ref"].Length())
             tmp = "������ ��������� ������������";
            else
             tmp = FISAttr->AV["add_ref"];
          }
         TTagNode *DateType   = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam1 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam2 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam3 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam4 = __tmp;
         if (DType == 0)
          {// �������� ���
            if ((DateParam1 && (DateParam1->AV["value"].Length())) ||
                (DateParam2 && (DateParam2->AV["value"].Length())))
             {
               tmp += " "+DateType->AV["name"];
               if (DateParam1->AV["value"].Length())
                {
                  tmp += " "+DateParam1->AV["name"];
                  tmp += "='"+DateParam1->AV["value"]+"'";
                }
               if (DateParam2->AV["value"].Length())
                {
                  tmp += " "+DateParam2->AV["name"];
                  tmp += "='"+DateParam2->AV["value"]+"'";
                }
             }
          }
         else if (DType >= 3)
          { // 3 - ������ ������������ ���������
            // 4 - ������ ���� - ���� ��������� ������� ������������ ���������
            // 5 - ������ ��������� - ���� ��������� ������� ������������ ���������
            // 6 - ������ �������� - ���� ��������� ������� ������������ ���������
            // 7 - ������ ������ - ���� ��������� ������� ������������ ���������

            tmp += " "+DateType->AV["name"];
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         else
          {// ������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
            tmp += " "+DateType->AV["name"];
            if (DateParam1->AV["value"].Length())
             {
               tmp += " "+DateParam1->AV["name"];
               tmp += "='"+DateParam1->AV["value"]+"'";
             }
            if (DateParam2->AV["value"].Length())
             {
               tmp += " "+DateParam2->AV["name"];
               tmp += "='"+DateParam2->AV["value"]+"'";
             }
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         if (DateParam4)
          {
            tmp += " "+DateParam4->AV["name"];
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
*/
  if (!FDM) return false;
  bool RC = false;
  TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);

  if (AIE->GetFirstChild())
   {
     if (AIE->Count)
      {
        UnicodeString tmp = FISCondRules->AV["name"]+" = ";
        TTagNode *__tmp = AIE->GetFirstChild();
//        int FPType = __tmp->AV["value"].ToIntDef(0);
        __tmp = __tmp->GetNext();
        __tmp = __tmp->GetNext();

        tmp += __tmp->AV["value"];
        __tmp = __tmp->GetNext();
        if (__tmp->AV["value"].Length())
         tmp += ", "+__tmp->AV["value"];
        __tmp = __tmp->GetNext();
        while (__tmp)
         {
           if (__tmp->AV["value"].Length())
            tmp += ", "+__tmp->AV["name"]+": "+__tmp->AV["value"];
           __tmp = __tmp->GetNext();
         }

        AText = tmp.Trim();
      }
     RC = true;
   }
  AXMLIE = AIE->AsXML;
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::SQLDocPeriod(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode *FIS = FISAttr->GetParent("is");

  
  ATab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";

  UnicodeString FYear, FDateFr, FDateTo, FOffset;
  FYear = FDateFr = FDateTo = FOffset = "";
  int FType, FSType;
  FType = FSType = -1;
  if (AIE->Count)
   {
     FType   = AIE->GetChildByAV("text","ref","0")->AV["value"].ToIntDef(0);
     FSType  = AIE->GetChildByAV("text","ref","1")->AV["value"].ToIntDef(0);
     FYear   = AIE->GetChildByAV("text","ref","4")->AV["value"].Trim();
     FDateFr = AIE->GetChildByAV("text","ref","5")->AV["value"].Trim();
     FDateTo = AIE->GetChildByAV("text","ref","6")->AV["value"].Trim();
     FOffset = AIE->GetChildByAV("text","ref","7")->AV["value"].Trim();
   }
  UnicodeString FPer = GetPart1(UnicodeString(ARet),'#');
  if (FPer.Length())
   {
     TDate FDocPerDateFr, FDocPerDateTo;
     FDocPerDateFr = FDocPerDateTo = TDate(0);
     if (GetPart1(FPer,';').Length())
       FDocPerDateFr = TDate(GetPart1(FPer,';'));
     if (GetPart2(FPer,';').Length())
       FDocPerDateTo = TDate(GetPart2(FPer,';'));

     int FDocPer  = GetPeriodValue(FType, FSType, FYear, FDateFr, FDateTo,FOffset,
                             GetPart2(UnicodeString(ARet),'#').ToIntDef(-1), FDocPerDateFr, FDocPerDateTo);

     UnicodeString TA = FIS->AV["tabalias"]+".";
     ARet = "("+TA+"F_PERIOD = "+IntToStr(FDocPer)+" and "+TA+"F_FROMDATE='"+FDocPerDateFr.FormatString("dd.mm.yyyy")+"' and "+TA+"F_TODATE='"+FDocPerDateTo.FormatString("dd.mm.yyyy")+"')";
   }
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseDocNomDef::DefDocPeriodicity(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE)
{
  if (!FDM) return false;
  bool RC = false;
  TStringList *FValues = NULL;
  try
   {
     FValues = new TStringList;
     FValues->AddObject("�����������",(TObject*)0);
     FValues->AddObject("����������",(TObject*)1);
     FValues->AddObject("�����",(TObject*)2);
     FValues->AddObject("�������",(TObject*)3);
     FValues->AddObject("���������",(TObject*)4);
     FValues->AddObject("���",(TObject*)5);

     RC = FDM->DefChVal(AIE, AText, AXMLIE, FValues);
   }
  __finally
   {
     if (FValues) delete FValues;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::SQLDocPeriodicity(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseDocNomDef::DefDocYNVal(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE)
{
  if (!FDM) return false;
  bool RC = false;
  TStringList *FValues = NULL;
  try
   {
     FValues = new TStringList;
     FValues->AddObject("���",(TObject*)0);
     FValues->AddObject("��",(TObject*)1);

     RC = FDM->DefChVal(AIE, AText, AXMLIE, FValues);
   }
  __finally
   {
     if (FValues) delete FValues;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::SQLDocYNVal(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
int __fastcall TICSBaseDocNomDef::GetPeriodValue(int AType, int ASubType, UnicodeString AYear,
                              UnicodeString ADateFr, UnicodeString ADateTo,
                              UnicodeString AOffset,
                              int ADocPerType, TDate &ADocPerDateFr, TDate &ADocPerDateTo)
{
  int RC = -1; // �������������  - "year" 5, "halfyear 4; "quarter 3;  "month" 2;  "unique" 1;  ERR 0
  try
   {
     unsigned short FCY,FCM,FCD;
     int FQuart, FHalfYear;
     ADocPerDateTo.DecodeDate(&FCY, &FCM, &FCD);
     if (FCM < 7)
      {
        if (FCM < 4) FQuart = 1;
        else         FQuart = 2;
        FHalfYear = 1;
      }
     else
      {
        if (FCM < 10) FQuart = 3;
        else          FQuart = 4;
        FHalfYear = 2;
      }
     UnicodeString FYearStr;
     if (AYear.Length())  FYearStr = AYear;
     else                 FYearStr = IntToStr(FCY);
     try
      {
        switch (AType)
         {
           case 1:
           { // ��������� � ���
             RC = ADocPerType;
             break;
           }
           case 2:
           { // ��� // �������������  - "year" 5
             ADocPerDateFr = StrToDate("01.01."+FYearStr);
             ADocPerDateTo = StrToDate("31.12."+FYearStr);
             RC = 5;
             break;
           }
           case 3:
           { // ��������� // �������������  - "halfyear 4;
             if (ASubType > 1)
              { //��  "��������� ���"
                FHalfYear = ASubType - 1;
              }
             switch (FHalfYear)
              {
                case 1:
                { //"������");
                  ADocPerDateFr = StrToDate("01.01."+FYearStr);
                  ADocPerDateTo = StrToDate("30.06."+FYearStr);
                  break;
                }
                case 2:
                { //"������");
                  ADocPerDateFr = StrToDate("01.07."+FYearStr);
                  ADocPerDateTo = StrToDate("31.12."+FYearStr);
                  break;
                }
              }
             RC = 4;
             break;
           }
           case 4:
           { // ������� // �������������  - "quarter 3;
             if (ASubType > 1)
              { //��  "������� ���"
                FQuart = ASubType - 1;
              }
             switch (FQuart)
              {
                case 1:
                { //"������");
                  ADocPerDateFr = StrToDate("01.01."+FYearStr);
                  ADocPerDateTo = StrToDate("31.03."+FYearStr);
                  break;
                }
                case 2:
                { //"������");
                  ADocPerDateFr = StrToDate("01.04."+FYearStr);
                  ADocPerDateTo = StrToDate("30.06."+FYearStr);
                  break;
                }
                case 3:
                { //"������");
                  ADocPerDateFr = StrToDate("01.07."+FYearStr);
                  ADocPerDateTo = StrToDate("30.09."+FYearStr);
                  break;
                }
                case 4:
                { //"��������");
                  ADocPerDateFr = StrToDate("01.10."+FYearStr);
                  ADocPerDateTo = StrToDate("31.12."+FYearStr);
                  break;
                }
              }
             RC = 3;
             break;
           }
           case 5:
           { // ����� // �������������  - "month" 2;
             if (ASubType > 1)
              { //��  "����� ���"
                FCM = ASubType - 1;
              }
             UnicodeString FLastDay = "1";

             if (IsValidDate((Word)FYearStr.ToInt(), (Word)FCM, (Word)1))
              { // ��� � ����� ����������
                FLastDay = IntToStr(DaysInAMonth((Word)FYearStr.ToInt(), (Word)FCM));
              }

             if (FCM < 10)
              {
                ADocPerDateFr = StrToDate("01.0"+IntToStr(FCM)+"."+FYearStr);
                ADocPerDateTo = StrToDate(FLastDay+".0"+IntToStr(FCM)+"."+FYearStr);
              }
             else
              {
                ADocPerDateFr = StrToDate("01."+IntToStr(FCM)+"."+FYearStr);
                ADocPerDateTo = StrToDate(FLastDay+"."+IntToStr(FCM)+"."+FYearStr);
              }
             RC = 2;
             break;
           }
           case 6:
           { // �������� ��� // �������������  - "unique" 1;
             if (ASubType == 2)
              { //"��������� ��������";
                if (ADateFr.Length()) ADocPerDateFr = StrToDate(ADateFr);
                if (ADateTo.Length()) ADocPerDateTo = StrToDate(ADateTo);
              }
             RC = 1;
             break;
           }
         }
      }
     catch (Exception &E)
      {
        throw;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

