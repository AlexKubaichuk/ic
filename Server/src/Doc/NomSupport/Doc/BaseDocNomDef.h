//---------------------------------------------------------------------------

#ifndef BaseDocNomDefH
#define BaseDocNomDefH
//---------------------------------------------------------------------------
#include "BaseNomDef.h"
#include "dsDocXMLUtils.h"
//#include <vcl.h>
//#include "ICSDATEUTIL.hpp"

//#include "pgmsetting.h"
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#                   ��������� ��������� �������� ���������                   #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
#define _CorrectDate(a) TDateTime(a).FormatString("dd.mm.yyyy")
//---------------------------------------------------------------------------
class TICSBaseDocNomDef : public TICSBaseNomDef
{
private:

    void __fastcall ASBOrg(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AYNVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ADocSpecOwner(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ADocPeriod(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ADocPeriodicity(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);


    bool __fastcall DefDocDef(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    void __fastcall SQLDocDef(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefSBOrg(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    void __fastcall SQLSBOrg(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);

    bool __fastcall DefDocSpecOwner(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    void __fastcall SQLDocSpecOwner(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefDocPeriod(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    void __fastcall SQLDocPeriod(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefDocPeriodicity(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    void __fastcall SQLDocPeriodicity(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefDocYNVal(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    void __fastcall SQLDocYNVal(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);

    int __fastcall GetPeriodValue(int AType, int ASubType, UnicodeString AYear,
                                  UnicodeString ADateFr, UnicodeString ADateTo,
                                  UnicodeString AOffset,
                                  int ADocPerType, TDate &ADocPerDateFr, TDate &ADocPerDateTo);

protected:

    virtual TGetAttrFunc __fastcall GetAttrMethodAddr(UnicodeString AFuncName);
    virtual TGetDefFunc __fastcall GetDefMethodAddr(UnicodeString AFuncName);
    virtual TGetSQLFunc __fastcall GetSQLMethodAddr(UnicodeString AFuncName);
public:

    __fastcall TICSBaseDocNomDef(TFDQuery *AQuery, TAxeXMLContainer *AXMLList, UnicodeString ARegGUI);
    __fastcall ~TICSBaseDocNomDef();
};
#endif






