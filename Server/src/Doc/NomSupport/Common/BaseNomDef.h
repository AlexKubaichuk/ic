//---------------------------------------------------------------------------

#ifndef BaseNomDefH
#define BaseNomDefH
//---------------------------------------------------------------------------
//#include <vcl.h>
#include "NomDMF.h"
//#include "DateConkretiz.h"
//#include "TextConcretiz.h"

//#include "pgmsetting.h"
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#                   ��������� ��������� �������� ���������                   #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
#define _CorrectDate(a) TDateTime(a).FormatString("dd.mm.yyyy")
//---------------------------------------------------------------------------
typedef void __fastcall (__closure *TGetAttrFunc)(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
typedef bool __fastcall (__closure *TGetDefFunc)(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
typedef void __fastcall (__closure *TGetSQLFunc)(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
//---------------------------------------------------------------------------
class TICSBaseNomDef : public TObject
{
    typedef map<const UnicodeString, UnicodeString> TStrMap;
private:
    TStrMap HashSel;
    TStrMap ClassHashSel;
    UnicodeString __fastcall SetDay(int AYear, int AMonth, int ADay );
  bool __fastcall GetFl(TTagNode *itTag, UnicodeString &UIDs);

  TNomGetExtValuesEvent __fastcall FGetOnExtGetAVal();
  void __fastcall FSetOnExtGetAVal(TNomGetExtValuesEvent AValue);


    UnicodeString __fastcall _GetPart2(UnicodeString ARef, char Sep);
    void __fastcall ADocAttrVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AOwnerCode(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AOwnerName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AOwnerFullName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AOwnerAddr(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AOwnerPhone(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AOwnerEmail(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AOwnerManagerFIO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AFamIO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AAge(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AAgeY(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AChoiceValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AChoiceDBValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ALPUOtdelValue(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet, UnicodeString AAttrRef);
    void __fastcall ALPUUchValue(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet, UnicodeString AAttrRef);

    void __fastcall AFullAddrValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AFullPhoneValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AFullOrgValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AOrgValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AFullInshPolVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AFullULVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);


  UnicodeString __fastcall FGetMainLPUCode();
  void __fastcall FSetMainLPUCode(UnicodeString AValue);

  TNomGetStrValByCodeEvent __fastcall FGetOnGetAddrStr();
  void __fastcall FSetOnGetAddrStr(TNomGetStrValByCodeEvent AValue);

  TNomGetStrValByCodeEvent __fastcall FGetOnGetOrgStr();
  void __fastcall FSetOnGetOrgStr(TNomGetStrValByCodeEvent AValue);

  TNomGetFieldValByCodeEvent __fastcall FGetOnGetOrgFieldStr();
  void __fastcall FSetOnGetOrgFieldStr(TNomGetFieldValByCodeEvent AValue);

    bool __fastcall DefChoiceValue(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    void __fastcall SQLChoiceValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefAgePeriodLast(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    void __fastcall SQLAgePeriodLast(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefDatePeriodLast(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    void __fastcall SQLDatePeriodLast(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefChoiceDBValue(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    void __fastcall SQLChoiceDBValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefRegVal(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    void __fastcall SQLRegVal(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefTextValue(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    void __fastcall SQLTextValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefExtEditValue(TTagNode *AIE, UnicodeString& AText, UnicodeString& AXMLIE);
    void __fastcall SQLExtEditValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
  bool __fastcall DefOrgPres(TTagNode *AIE, UnicodeString& AText,  UnicodeString& AXMLIE);
  void __fastcall SQLOrgPres(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);

protected:
    TNomDM         *FDM;
    virtual void __fastcall DoGetOrgParam(UnicodeString &ARet, UnicodeString AFlName);
    UnicodeString __fastcall GetValIfNotEmpty(UnicodeString APref, UnicodeString AVal, UnicodeString APostf = "");
    UnicodeString __fastcall FGetGUI(TTagNode *ANode);
    UnicodeString __fastcall AgeStr(TDateTime ACurDate, TDateTime ABirthDay);
    UnicodeString __fastcall AgeStrY(TDateTime ACurDate, TDateTime ABirthDay);
    UnicodeString __fastcall FIncPeriod(UnicodeString AFrDate,UnicodeString APer, int IsSub);
    UnicodeString __fastcall AddPeriod(UnicodeString AScr1, UnicodeString AScr2);
    UnicodeString __fastcall FGetRefValue(TTagNode *ANode, UnicodeString ARef1,UnicodeString ARef2);
    void __fastcall GetSQLDefValue(TTagNode *AIE, UnicodeString ATemplate);
    void __fastcall GetSQLValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    UnicodeString __fastcall GetChildContAVDef(TTagNode *ANode, UnicodeString AVal, UnicodeString ADef);

    virtual TGetAttrFunc __fastcall GetAttrMethodAddr(UnicodeString AFuncName);
    virtual TGetDefFunc  __fastcall GetDefMethodAddr(UnicodeString AFuncName);
    virtual TGetSQLFunc  __fastcall GetSQLMethodAddr(UnicodeString AFuncName);


public:
    __fastcall TICSBaseNomDef(TFDQuery *AQuery, TAxeXMLContainer *AXMLList, UnicodeString ARegGUI);
    __fastcall ~TICSBaseNomDef();

  __property TNomGetExtValuesEvent OnExtGetAVal = {read=FGetOnExtGetAVal, write=FSetOnExtGetAVal};
  __property UnicodeString MainLPUCode = {read=FGetMainLPUCode, write=FSetMainLPUCode};
  __property TNomDM         *DM = {read=FDM};

  __property TNomGetStrValByCodeEvent OnGetAddrStr = {read = FGetOnGetAddrStr, write=FSetOnGetAddrStr};
  __property TNomGetStrValByCodeEvent OnGetOrgStr = {read = FGetOnGetOrgStr, write=FSetOnGetOrgStr};
  __property TNomGetFieldValByCodeEvent  OnGetOrgFieldStr = {read = FGetOnGetOrgFieldStr, write=FSetOnGetOrgFieldStr};

  void __fastcall GetAVal(UnicodeString AFuncName, UnicodeString AFields, UnicodeString ARef, UnicodeString AAttrRef, UnicodeString &ARet);
  bool __fastcall GetDVal(UnicodeString AFuncName, TTagNode* ANode, UnicodeString &AText, UnicodeString &AIEXML);
  void __fastcall GetSVal(UnicodeString AFuncName, TTagNode* ANode, UnicodeString &ATab, UnicodeString &ARet);
};
#endif

