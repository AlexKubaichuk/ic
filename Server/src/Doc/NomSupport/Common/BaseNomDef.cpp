// ---------------------------------------------------------------------------
#include <DateUtils.hpp>
#pragma hdrstop
#include "BaseNomDef.h"
#include <System.JSON.hpp>
#include "dsSrvRegTemplate.h"
// #include "DocPeriod.h"
// #include "PackPeriod.h"
// ---------------------------------------------------------------------------
#define flVal(a) AFields->Values[AFields->Names[a]]
// ---------------------------------------------------------------------------
__fastcall TICSBaseNomDef::TICSBaseNomDef(TFDQuery * AQuery, TAxeXMLContainer * AXMLList, UnicodeString ARegGUI)
 {
  dsLogC(__FUNC__);
  FDM = new TNomDM(AQuery, AXMLList, ARegGUI);
  dsLogC(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
__fastcall TICSBaseNomDef::~TICSBaseNomDef()
 {
  dsLogD(__FUNC__);
  delete FDM;
  dsLogD(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::GetAVal(UnicodeString AFuncName, UnicodeString AFields, UnicodeString ARef,
  UnicodeString AAttrRef, UnicodeString & ARet)
 {
  TStringList * tmpList = new TStringList;
  try
   {
    TGetAttrFunc FFunc = GetAttrMethodAddr(AFuncName);
    if (FFunc)
     {
      try
       {
        tmpList->Text = AFields;
        FFunc(tmpList, ARef, ARet, AAttrRef);
       }
      catch (Exception & E)
       {
        ARet = "������ � �������: " + AFuncName;
        dsLogError(E.Message, __FUNC__);
       }
     }
    else
     throw Exception(("������ ������ ������� � ������ '" + AFuncName + "'"));
   }
  __finally
   {
    delete tmpList;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::GetDVal(UnicodeString AFuncName, TTagNode * ANode, UnicodeString & AText,
  UnicodeString & AIEXML)
 {
  bool RC = false;
  try
   {
    TGetDefFunc FFunc = GetDefMethodAddr(AFuncName);
    if (FFunc)
     {
      RC = FFunc(ANode, AText, AIEXML);
     }
    else
     throw Exception(("������ ������ ������� � ������ '" + AFuncName + "'"));
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::GetSVal(UnicodeString AFuncName, TTagNode * ANode, UnicodeString & ATab,
  UnicodeString & ARet)
 {
  dsLogBegin(__FUNC__);
  try
   {
    dsLogMessage(AFuncName, __FUNC__, 1);
    TGetSQLFunc FFunc = GetSQLMethodAddr(AFuncName);
    if (FFunc)
     {
      FFunc(ANode, ATab, ARet);
     }
    else
     throw Exception(("������ ������ ������� � ������ '" + AFuncName + "'"));
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
TGetAttrFunc __fastcall TICSBaseNomDef::GetAttrMethodAddr(UnicodeString AFuncName)
 {
  TGetAttrFunc RC = NULL;
  if (AFuncName == "ADocAttrVal")
   RC = ADocAttrVal;
  // else if (AFuncName == "AExtEditValue")   RC = AExtEditValue;
  else if (AFuncName == "AOwnerCode")
   RC = AOwnerCode;
  else if (AFuncName == "AOwnerName")
   RC = AOwnerName;
  else if (AFuncName == "AOwnerFullName")
   RC = AOwnerFullName;
  else if (AFuncName == "AOwnerAddr")
   RC = AOwnerAddr;
  else if (AFuncName == "AOwnerPhone")
   RC = AOwnerPhone;
  else if (AFuncName == "AOwnerEmail")
   RC = AOwnerEmail;
  else if (AFuncName == "AFamIO")
   RC = AFamIO;
  else if (AFuncName == "AAge")
   RC = AAge;
  else if (AFuncName == "AAgeY")
   RC = AAgeY;
  else if (AFuncName == "AChoiceValue")
   RC = AChoiceValue;
  else if (AFuncName == "AChoiceDBValue")
   RC = AChoiceDBValue;
  else if (AFuncName == "AFullAddrValue")
   RC = AFullAddrValue;
  else if (AFuncName == "AFullPhoneValue")
   RC = AFullPhoneValue;
  else if (AFuncName == "ALPUOtdelValue")
   RC = ALPUOtdelValue;
  else if (AFuncName == "ALPUUchValue")
   RC = ALPUUchValue;
  else if (AFuncName == "AFullOrgValue")
   RC = AFullOrgValue;
  else if (AFuncName == "AOrgValue")
   RC = AOrgValue;
  else if (AFuncName == "AFullInshPolVal")
   RC = AFullInshPolVal;
  else if (AFuncName == "AFullULVal")
   RC = AFullULVal;
  return RC;
 }
// ---------------------------------------------------------------------------
TGetDefFunc __fastcall TICSBaseNomDef::GetDefMethodAddr(UnicodeString AFuncName)
 {
  TGetDefFunc RC = NULL;
  if (AFuncName == "DefChoiceValue")
   RC = DefChoiceValue;
  else if (AFuncName == "DefRegVal")
   RC = DefRegVal;
  else if (AFuncName == "DefTextValue")
   RC = DefTextValue;
  else if (AFuncName == "DefChoiceDBValue")
   RC = DefChoiceDBValue;
  else if (AFuncName == "DefAgePeriodLast")
   RC = DefAgePeriodLast;
  else if (AFuncName == "DefDatePeriodLast")
   RC = DefDatePeriodLast;
  else if (AFuncName == "DefExtEditValue")
   RC = DefExtEditValue;
  else if (AFuncName == "DefOrgPres")
   RC = DefOrgPres;
  return RC;
 }
// ---------------------------------------------------------------------------
TGetSQLFunc __fastcall TICSBaseNomDef::GetSQLMethodAddr(UnicodeString AFuncName)
 {
  TGetSQLFunc RC = NULL;
  if (AFuncName == "SQLChoiceValue")
   RC = SQLChoiceValue;
  else if (AFuncName == "SQLAgePeriodLast")
   RC = SQLAgePeriodLast;
  else if (AFuncName == "SQLDatePeriodLast")
   RC = SQLDatePeriodLast;
  else if (AFuncName == "SQLChoiceDBValue")
   RC = SQLChoiceDBValue;
  else if (AFuncName == "SQLRegVal")
   RC = SQLRegVal;
  else if (AFuncName == "SQLTextValue")
   RC = SQLTextValue;
  else if (AFuncName == "SQLExtEditValue")
   RC = SQLExtEditValue;
  else if (AFuncName == "SQLOrgPres")
   RC = SQLOrgPres;
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::GetValIfNotEmpty(UnicodeString APref, UnicodeString AVal, UnicodeString APostf)
 {
  UnicodeString RC = "";
  try
   {
    if (AVal.Trim().Length() && (AVal.Trim().ToIntDef(0) != -1))
     RC = APref + AVal.Trim() + APostf;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::SetDay(int AYear, int AMonth, int ADay)
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString sm = IntToStr(AYear);
    sm = sm.SubString(sm.Length(), 1);
    if (((sm == "1") || (sm == "2") || (sm == "3") || (sm == "4")) && !((AYear < 15) && (AYear > 10)))
     sm = "�. ";
    else
     sm = "�. ";
    if (AYear)
     RC += IntToStr(AYear) + sm;
    if (AMonth)
     RC += IntToStr(AMonth) + "�. ";
    if (ADay)
     RC += IntToStr(ADay) + "�.";
    if (!(AYear + AMonth + ADay))
     RC += "0�.";
   }
  __finally
   {
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::AgeStr(TDateTime ACurDate, TDateTime ABirthDay)
 {
  UnicodeString RC = "";
  try
   {
    if (ACurDate < ABirthDay)
     {
      RC = "-";
     }
    else
     {
      Word FYear, FMonth, FDay;
      DateDiff(ACurDate, ABirthDay, FDay, FMonth, FYear);
      RC = SetDay(FYear, FMonth, FDay);
     }
   }
  __finally
   {
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::AgeStrY(TDateTime ACurDate, TDateTime ABirthDay)
 {
  UnicodeString RC = "";
  try
   {
    if (ACurDate < ABirthDay)
     {
      RC = "-";
     }
    else
     {
      float DDD = (int)ACurDate - ((int)ABirthDay) - 2;
      unsigned short YY = 0, MM = 0, DD = 0;
      (ACurDate - ABirthDay + 1).TDateTime::DecodeDate(& YY, & MM, & DD);
      MM-- ;
      RC = FloatToStrF((DDD / 365.25), ffFixed, 16, 2) + " " + " �.";
     }
   }
  __finally
   {
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::FGetGUI(TTagNode * ANode)
 {
  return ANode->GetRoot()->GetChildByName("passport")->AV["gui"];
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::DoGetOrgParam(UnicodeString & ARet, UnicodeString AFlName)
 {
  FDM->qtExec("Select " + AFlName + " From SUBORDORG Where Upper(CODE)='" + FDM->MainLPUCode.UpperCase() + "'");
  if (FDM->Query->RecordCount)
   ARet = FDM->Query->FieldByName(AFlName)->AsString;
  else
   ARet = "";
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::GetChildContAVDef(TTagNode * ANode, UnicodeString AVal, UnicodeString ADef)
 {
  UnicodeString RC = ADef;
  try
   {
    TTagNode * _tmp = ANode->GetChildByAV("text", "ref", AVal);
    if (_tmp)
     RC = _tmp->GetAVDef("value", ADef);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::_GetPart2(UnicodeString ARef, char Sep)
 {
  int pos = ARef.Pos(Sep);
  return ARef.SubString(pos + 1, ARef.Length() - pos);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::AddPeriod(UnicodeString AScr1, UnicodeString AScr2)
 {
  int YY1, YY2, MM1, MM2, DD1, DD2;
  FDM->DecodePeriod(AScr1, YY1, MM1, DD1);
  FDM->DecodePeriod(AScr2, YY2, MM2, DD2);
  DD1 += DD2;
  DD2 = 0;
  if (DD1 > 31)
   {
    DD1 -= 31;
    DD2 = 1;
   }
  MM1 += MM2 + DD2;
  MM2 = 0;
  if (MM1 > 12)
   {
    MM1 -= 12;
    MM2 = 1;
   }
  YY1 += YY2 + MM2;
  return IntToStr(YY1) + "/" + IntToStr(MM1) + "/" + IntToStr(DD1);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::FIncPeriod(UnicodeString AFrDate, UnicodeString APer, int IsSub)
 {
  return FDM->IncPeriod(AFrDate, APer, IsSub);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::FGetRefValue(TTagNode * ANode, UnicodeString ARef1, UnicodeString ARef2)
 {
  if (ANode->GetParent("domain")->GetNext())
   return ARef1;
  else
   return ARef2;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::GetFl(TTagNode * itTag, UnicodeString & UIDs)
 {
  if (itTag->CmpName("fl"))
   UIDs += itTag->AV["ref"] + "\n";
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::GetSQLDefValue(TTagNode * AIE, UnicodeString ATemplate)
 {
  TTagNode * FTemplate = new TTagNode;
  TStringList * FFlList = new TStringList;
  TJSONObject * FValues = new TJSONObject;
  try
   {
    UnicodeString FFList = "";
    FTemplate->AsXML = ATemplate;
    FTemplate->Iterate(GetFl, FFList);
    FFlList->Text = FFList;
    TTagNode * tmp;
    TTagNode * FSQLNode = AIE->GetChildByAV("text", "name", "sql");
    if (!FSQLNode)
     FSQLNode = AIE->AddChild("text", "name=sql");
    for (int i = 0; i < FFlList->Count; i++)
     {
      tmp = AIE->GetChildByAV("text", "ref", IntToStr(i));
      if (tmp)
       FValues->AddPair(FFlList->Strings[i], tmp->AV["value"]);
     }
    if (FValues->Count)
     {
      TdsRegTemplate * FTmpl = new TdsRegTemplate(ATemplate, FDM->XMLList->GetXML("40381E23-92155860-4448"), FValues);
      FTmpl->UseParams = false;
      try
       {
        UnicodeString FTabAlias = "";
        TTagNode * NomTag = FDM->XMLList->GetRefer(AIE->AV["ref"]);
        if (NomTag)
         {
          NomTag = NomTag->GetTagByUID(NomTag->AV["param"]);
          if (NomTag)
           {
            NomTag = NomTag->GetParent("is");
            if (NomTag)
             FTabAlias = NomTag->AV["tabalias"];
           }
         }
        if (FTemplate->AV["subsel"].ToIntDef(0))
         {
          TStringList * tmpSelList = new TStringList;
          try
           {
            FSQLNode->AV["value"] = FTmpl->GetWhereClRep(FTabAlias, tmpSelList);
            for (int i = 0; i < tmpSelList->Count; i++)
             FSQLNode->AddChild("subtext")->AV["value"] = tmpSelList->Strings[i];
           }
          __finally
           {
            delete tmpSelList;
           }
         }
        else
         FSQLNode->AV["value"] = FTmpl->GetWhere(FTabAlias);
       }
      __finally
       {
       }
     }
   }
  __finally
   {
    delete FTemplate;
    delete FFlList;
    delete FValues;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::GetSQLValue(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode * FIS = FISAttr->GetParent("is");
  UnicodeString ISTab = FGetGUI(FISCondRules) + "." + FIS->AV["uid"] + ":";
  ATab = ISTab;
  if (!AIE->Count)
   {
    if (FIS->AV["tabalias"].Length())
     ARet = FIS->AV["tabalias"] + "." + FIS->AV["keyfield"] + "=" + FIS->AV["tabalias"] + "." + FIS->AV["keyfield"];
    else
     ARet = FIS->AV["maintable"] + "." + FIS->AV["keyfield"] + "=" + FIS->AV["maintable"] + "." + FIS->AV["keyfield"];
   }
  else
   {
    bool IsExtSel = false;
    if (FISCondRules->GetChildByName("root"))
     IsExtSel = (bool)(FISCondRules->GetChildByName("root")->AV["extsel"].ToIntDef(0));
    TTagNode * _tmp = AIE->GetChildByAV("text", "name", "sql");
    if (_tmp)
     {
      if (IsExtSel && _tmp->Count)
       {
        TReplaceFlags rFlag;
        rFlag << rfReplaceAll;
        TTagNode * subtmp;
        UnicodeString FSQL = _tmp->AV["value"];
        UnicodeString FSubSQL;
        int idx = 0;
        subtmp = _tmp->GetFirstChild();
        while (subtmp)
         {
          UnicodeString SelRC = "";
          FSubSQL = subtmp->AV["value"];
          TStrMap::iterator rc = HashSel.find(FSubSQL);
          if (rc == HashSel.end())
           {
            SelRC = "-1";
            FDM->qtExec(FSubSQL);
            while (!FDM->Query->Eof)
             {
              SelRC += "," + FDM->Query->FieldByName("CODE")->AsString;
              FDM->Query->Next();
             }
            HashSel[FSubSQL] = SelRC;
           }
          FSQL = Sysutils::StringReplace(FSQL, "_SEL" + IntToStr(idx), HashSel[FSubSQL], rFlag);
          idx++ ;
          subtmp = subtmp->GetNext();
         }
        ARet = FSQL;
       }
      else
       ARet = _tmp->AV["value"];
     }
    else
     ARet = "";
   }
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                   ��������� ��������� �������� ���������                   #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::ADocAttrVal(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  if (ARef.LowerCase().Pos("extfiltername"))
   {
    TStringList * FVals = new TStringList;
    try
     {
      FVals->Text = StringReplace(AFields->Values["extfiltername"].Trim(), "##", "\n", TReplaceFlags() << rfReplaceAll);
      if (ARef.Length() == 13)
       {
        if (FVals->Count)
         ARet = FVals->Strings[0];
       }
      else
       {
        int pos = UnicodeString(ARef[14]).ToIntDef(1) - 1;
        if (pos < FVals->Count)
         ARet = FVals->Strings[pos];
       }
     }
    __finally
     {
      delete FVals;
     }
   }
  else
   ARet = AFields->Values[ARef];
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AOwnerCode(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  DoGetOrgParam(ARet, "CODE");
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AOwnerName(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  DoGetOrgParam(ARet, "NAME");
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AOwnerFullName(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  DoGetOrgParam(ARet, "FULLNAME");
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AOwnerAddr(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  DoGetOrgParam(ARet, "ADDR");
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AOwnerPhone(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  DoGetOrgParam(ARet, "PHONE");
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AOwnerEmail(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  DoGetOrgParam(ARet, "EMAIL");
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AOwnerManagerFIO(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  DoGetOrgParam(ARet, "OWNERFIO");
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AFamIO(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  UnicodeString DDD = flVal(0);
  if (flVal(1).Length())
   DDD += " " + UnicodeString(flVal(1)[1]) + ".";
  if (flVal(2).Length())
   DDD += UnicodeString(flVal(2)[1]) + ".";
  ARet = DDD;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AAge(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  TDateTime FToDate = Date();
  UnicodeString FDP = GetPart1(UnicodeString(ARet), '#');
  if (GetPart2(FDP, ';').Length())
   {
    if (TDate(GetPart2(FDP, ';')) <= FToDate)
     FToDate = TDate(GetPart2(FDP, ';'));
   }
  try
   {
    ARet = AgeStr(FToDate, StrToDate(AFields->Values[AFields->Names[0]]));
   }
  catch (EConvertError & E)
   {
    ARet = "";
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AAgeY(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  TDateTime FToDate = Date();
  UnicodeString FDP = GetPart1(UnicodeString(ARet), '#');
  if (GetPart2(FDP, ';').Length())
   {
    if (TDate(GetPart2(FDP, ';')) <= FToDate)
     FToDate = TDate(GetPart2(FDP, ';'));
   }
  try
   {
    ARet = AgeStrY(FToDate, StrToDate(AFields->Values[AFields->Names[0]]));
   }
  catch (EConvertError & E)
   {
    ARet = "";
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AChoiceValue(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  ARet = "";
  TTagNode * FChoice = FDM->XMLList->GetRefer(ARef);
  if (FChoice->CmpName("choice"))
   {
    FChoice = FChoice->GetChildByAV("choicevalue", "value", AFields->Values[AFields->Names[0]]);
    if (FChoice)
     ARet = FChoice->AV["name"];
   }
  else if (FChoice->CmpName("binary"))
   {
    if (AFields->Values[AFields->Names[0]] == "1")
     ARet = "��";
    else
     ARet = "���";
   }
  else if (FChoice->CmpName("class, choiceDB"))
   {
    if (AFields->Values[AFields->Names[0]].ToIntDef(-1) != -1)
     {
      if (FChoice->CmpName("choiceDB"))
       FChoice = FChoice->GetTagByUID(FChoice->AV["ref"]);
      if (FChoice)
       {
        TTagNode * FieldList = FChoice->GetChildByName("description")->GetFirstChild();
        TTagNode * itField;
        TTagNode * ISAttr = FDM->XMLList->GetRefer(AAttrRef);
        UnicodeString OldRelation = "";
        UnicodeString FCol = _UID(ISAttr->AV["column"]);
        UnicodeString FSQL = "";
        itField = FieldList->GetFirstChild();
        while (itField)
         {
          if (!FSQL.Length())
           FSQL += "R" + itField->AV["ref"];
          else
           FSQL += ", R" + itField->AV["ref"];
          itField = itField->GetNext();
         }
        FSQL = "Select " + FSQL + " From " + _GUI(ISAttr->AV["column"]);
        if (ISAttr->AV["ref_parent"].Length())
         {
          while (ISAttr->AV["ref_parent"].Length())
           {
            OldRelation = ISAttr->AV["relation"];
            ISAttr      = FDM->XMLList->GetRefer(_GUI(AAttrRef) + "." + ISAttr->AV["ref_parent"]);
            if (!ISAttr->AV["ref_parent"].Length())
             FSQL += " join " + _GUI(ISAttr->AV["column"]) + " on (" + OldRelation + " and " +
               _GUI(ISAttr->AV["column"]) + ".CODE=" + AFields->Values[AFields->Names[0]] + ")";
            else
             FSQL += " join " + _GUI(ISAttr->AV["column"]) + " on (" + OldRelation + ")";
           }
         }
        else
         {
          FSQL += " Where " + _GUI(ISAttr->AV["column"]) + ".CODE=" + AFields->Values[AFields->Names[0]];
         }
        UnicodeString SelRC = "";
        try
         {
          TStrMap::iterator rc = ClassHashSel.find(FSQL);
          if (rc == ClassHashSel.end())
           {
            SelRC = "";
            FDM->qtExec(FSQL);
            itField = FieldList->GetFirstChild();
            while (itField)
             {
              if (!FDM->Query->FieldByName("R" + itField->AV["ref"])->IsNull)
               {
                if (!SelRC.Length())
                 SelRC = FDM->Query->FieldByName("R" + itField->AV["ref"])->AsString;
                else
                 SelRC += "/" + FDM->Query->FieldByName("R" + itField->AV["ref"])->AsString;
               }
              itField = itField->GetNext();
             }
            ClassHashSel[FSQL] = SelRC;
           }
          else
           SelRC = ClassHashSel[FSQL];
          ARet = SelRC;
         }
        catch (Exception & E)
         {
          ARet = "������ ��������� �������� ��� " + FChoice->Name + " : " + E.Message;
         }
       }
     }
    else
     ARet = "";
   }
  else if (FChoice->CmpName("extedit, choiceTree"))
   {
    if (FDM->OnExtGetAVal)
     {
      ARet = FDM->OnExtGetAVal(FChoice, AFields);
     }
   }
  else
   {
    ARet = "";
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AChoiceDBValue(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  TTagNode * FExtAttr = FDM->XMLList->GetRefer(ARef);
  if (FExtAttr)
   {
    ARet = FDM->GetClassData((long)AFields->Values["R" + FExtAttr->AV["uid"]].ToIntDef(-1), FExtAttr->AV["ref"]);
   }
  else
   {
    ARet = " �������� �� ���������� ";
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AFullAddrValue(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  AChoiceValue(AFields, "40381E23-92155860-4448.0136", ARet, AAttrRef);
  // ARet = AFields->Text;//ARef;
  // if (AFields->Values["R003D"] == "1")
  // {
  // ARet = ARef;
  /* ARet = FDM->KLADR->codeToText(AFields->Values["R0062"]);
   ARet = ARet + GetValIfNotEmpty(" �. ",AFields->Values["R0064"]);
   ARet = ARet + GetValIfNotEmpty(" ����. ",AFields->Values["R0065"]);
   ARet = ARet + GetValIfNotEmpty(" ��. ",AFields->Values["R0066"]);
   */        // !!!
  // }
  // else
  // {
  // ARet = "����� �� ������";
  // }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AFullPhoneValue(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  ARet = "";
  if (AFields->Values["R0148"].Length() && (AFields->Values["R0148"].Trim() != "8"))
   ARet = "(���.) " + AFields->Values["R0148"];
  if (AFields->Values["R014A"].Length() && (AFields->Values["R014A"].Trim() != "8"))
   {
    if (ARet.Length())
     ARet += ",  ";
    ARet += "(���.) " + AFields->Values["R014A"];
   }
  if (AFields->Values["R014C"].Length() && (AFields->Values["R014C"].Trim() != "8"))
   {
    if (ARet.Length())
     ARet += ",  ";
    ARet += "(���.) " + AFields->Values["R014C"];
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::ALPUOtdelValue(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  ARet = "";
  if (AFields->Values["R00B3"].ToIntDef(-1) != -1)
   {
    UnicodeString FSQL = "Select R00F7, R000F From CLASS_000E Where CODE=" + AFields->Values["R00B3"];
    try
     {
      TStrMap::iterator rc = ClassHashSel.find(FSQL);
      if (rc == ClassHashSel.end())
       {
        FDM->qtExec(FSQL);
        if (FDM->Query->RecordCount)
         {
          FSQL = "Select R000D From CLASS_000C Where CODE=" + FDM->Query->FieldByName("R00F7")->AsString;
          FDM->qtExec(FSQL);
          if (FDM->Query->RecordCount)
           {
            ARet               = FDM->Query->FieldByName("R000D")->AsString;
            ClassHashSel[FSQL] = ARet;
           }
         }
       }
      else
       ARet = ClassHashSel[FSQL];
     }
    catch (Exception & E)
     {
      ARet = "������ ��������� �������� ��� ��������� : " + E.Message;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::ALPUUchValue(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  ARet = "";
  if (AFields->Values["R00B3"].ToIntDef(-1) != -1)
   {
    UnicodeString FSQL = "Select R000F From CLASS_000E Where CODE=" + AFields->Values["R00B3"];
    try
     {
      TStrMap::iterator rc = ClassHashSel.find(FSQL);
      if (rc == ClassHashSel.end())
       {
        FDM->qtExec(FSQL);
        if (FDM->Query->RecordCount)
         {
          ARet               = FDM->Query->FieldByName("R000F")->AsString;
          ClassHashSel[FSQL] = ARet;
         }
       }
      else
       ARet = ClassHashSel[FSQL];
     }
    catch (Exception & E)
     {
      ARet = "������ ��������� �������� ��� ������� : " + E.Message;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AFullOrgValue(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  AChoiceValue(AFields, "40381E23-92155860-4448.0025", ARet, AAttrRef);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AOrgValue(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  ARet = FDM->GetOrgFieldByCode(AFields->Values["R0025"], GetRPartE(AFields->Text, ':'));
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AFullInshPolVal(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  ARet = "";
  if (AFields->Values["R0093"].Length() && AFields->Values["R009A"].Length())
   {
    ARet = FDM->GetClassData((long)AFields->Values["R0093"].ToIntDef(-1), "0095");
    ARet += " " + AFields->Values["R0099"].Trim();
    ARet = ARet.Trim();
    ARet += " �" + AFields->Values["R009A"].Trim();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AFullULVal(TStringList * AFields, UnicodeString ARef, UnicodeString & ARet,
  UnicodeString AAttrRef)
 {
  ARet = "";
  if (AFields->Values["R009C"].Length() && AFields->Values["R009E"].Length())
   {
    switch (AFields->Values["R009C"].Trim().ToIntDef(-1))
     {
     case 0:
       {
        ARet += "�������";
        break;
       }
     case 1:
       {
        ARet += "������������� � ��������";
        break;
       }
     case 2:
       {
        ARet += "������ ��./�";
        break;
       }
     }
    ARet += " " + AFields->Values["R009D"].Trim();
    ARet = ARet.Trim();
    ARet += " �" + AFields->Values["R009E"].Trim();
   }
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #            ��������� ��������� �������� �������������� ��������� (Def...)  #
// #                                                                            #
// #            ��������� ������������ ������ SQL-�������             (SQL...)  #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::DefChoiceValue(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  UnicodeString tmp = "";
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  /*
   if (NeedKonkr)
   {
   tmp = "<root><fl ref='"+_UID(FISAttr->AV["ref"])+"'/></root>";
   }
   */
  return FDM->DefRegValue(AIE, AText, AXMLIE, tmp, _GUI(FISAttr->AV["ref"]), false, false);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLChoiceValue(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * sqlNode = AIE->GetChildByAV("text", "name", "sql");
  if (sqlNode)
   {
    if (!sqlNode->AV["value"].Trim().Length())
     {
      UnicodeString FTmpl = "";
      TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
      TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      FTmpl = "<root><fl ref='" + _UID(FISAttr->AV["ref"]) + "'/></root>";
      GetSQLDefValue(AIE, FTmpl);
     }
   }
  GetSQLValue(AIE, ATab, ARet);
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::DefAgePeriodLast(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  // AIE - ������, ������ �� IE - ����� ����� 2-� �����������
  // ����� �� ���, �� �� �� �� ����� �����
  // ��� ���� "�������"
  // {
  // 1-� - ������� ��;
  // 2-� - ������� ��;
  // }
  if (!FDM)
   return false;
  bool iRC = false;
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  if (AIE->Count == 2)
   {
    TTagNode * _tmpNode = AIE->GetFirstChild();
    UnicodeString _tmp;
    _tmp = FISCondRules->AV["name"] + " = ";
    _tmp += _tmpNode->AV["name"] + _tmpNode->AV["value"];
    _tmpNode = _tmpNode->GetNext();
    _tmp += " " + _tmpNode->AV["name"] + _tmpNode->AV["value"];
    AText = _tmp.Trim();
    _tmp  = "";
    iRC   = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLAgePeriodLast(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  // �������� ������� ��������
  TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  // �������� ������� �������� ������������ �������� ����� �����������
  TTagNode * FAddISAttr = FISCondRules->GetTagByUID(FISAttr->AV["add_ref"]);
  TTagNode * FAddIS;
  TTagNode * FIS = FISAttr->GetParent("is");
  ATab = FGetGUI(FISCondRules) + "." + FIS->AV["uid"] + ":";
  UnicodeString xFr, xTo, flName, AddflName;
  xFr    = "";
  xTo    = "";
  flName = FIS->AV["tabalias"] + "." + GetPart1(FISAttr->AV["fields"], ',');
  // bool isCurrDate = false;
  UnicodeString FDP = GetPart1(UnicodeString(ARet), '#');
  TTagNode * TagFr, *TagTo;
  TagFr = AIE->GetChildByAV("text", "name", "��:");
  TagTo = AIE->GetChildByAV("text", "name", "��:");
  if (FAddISAttr)
   {
    FAddIS    = FAddISAttr->GetParent("is");
    AddflName = FAddIS->AV["tabalias"] + "." + FAddISAttr->AV["fields"];
    int FYears, FMonths, FDays;
    if (TagFr && (TagFr->AV["value"].Length()))
     {
      FDM->DecodePeriod(TagFr->AV["value"], FYears, FMonths, FDays);
      xFr = flName + "<= DATEADD(DAY, -" + IntToStr(FDays) + ",DATEADD(MONTH, -" + IntToStr(FMonths) +
        ",DATEADD(YEAR, -" + IntToStr(FYears) + "," + AddflName + ")))";
      // xFr = flName + "<= Inc_Period(" + AddflName + ",'" + TagFr->AV["value"] + "','/',1)";
     }
    if (TagTo && (TagTo->AV["value"].Length()))
     {
      FDM->DecodePeriod(TagTo->AV["value"], FYears, FMonths, FDays);
      xTo = flName + ">=  DATEADD(DAY, -" + IntToStr(FDays) + ",DATEADD(MONTH, -" + IntToStr(FMonths) +
        ",DATEADD(YEAR, -" + IntToStr(FYears) + "," + AddflName + ")))";
      // xTo = flName + ">= Inc_Period(" + AddflName + ",'" + TagTo->AV["value"] + "','/',1)";
     }
   }
  else
   {
    if (GetPart2(FDP, ';').Length())
     {
      if (TDate(GetPart2(FDP, ';')) > Date())
       AddflName = Date().FormatString("dd.mm.yyyy");
      else
       AddflName = GetPart2(FDP, ';');
     }
    else
     AddflName = Date().FormatString("dd.mm.yyyy");
    if (TagFr && (TagFr->AV["value"].Length()))
     xFr = flName + "<='" + FIncPeriod(AddflName, TagFr->AV["value"], 1) + "'";
    if (TagTo && (TagTo->AV["value"].Length()))
     xTo = flName + ">='" + FIncPeriod(AddflName, TagTo->AV["value"], 1) + "'";
   }
  if (xFr.Length() && xTo.Length())
   {
    ARet = "(" + xFr + " and " + xTo + ")";
   }
  else if (xFr.Length())
   {
    ARet = xFr;
   }
  else if (xTo.Length())
   {
    ARet = xTo;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::DefDatePeriodLast(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  // AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
  // ����� �� ���, �� �� �� �� ����� �����
  // ��� ���� "����"
  // {
  // 1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
  // 1,2 ��� �������; 0 - �������� ���
  // 1 - ������ �� ������� ����
  // 2 - ������ �� ����
  // 2-� - ���� ��;   ��� ���� "�������� ���"
  // ��� ����� "������ �� ..." �������� �������
  // 3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
  // ��� ���� "������ �� ������� ����"  - �� �������������;
  // 4-� - �������� - ��� ���� "�������� ���"  - �� �������������
  // �������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
  if (!FDM)
   return false;
  bool iRC = false;
  UnicodeString tmp = "";
  TTagNode * __tmp;
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  if (AIE->GetFirstChild())
   {
    if (AIE->Count)
     {
      tmp   = FISCondRules->AV["name"] + " = ";
      __tmp = AIE->GetFirstChild();
      int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
      bool FLast = (bool)(DType & 1);
      DType = DType >> 1;
      if (FLast)
       {
        TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
        if (!FISAttr->AV["add_ref"].Length())
         tmp = "������ ��������� ������������";
        else
         tmp = FISAttr->AV["add_ref"];
       }
      TTagNode * DateType = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam1 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam2 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam3 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam4 = __tmp;
      if (DType == 0)
       { // �������� ���
        if ((DateParam1 && (DateParam1->AV["value"].Length())) || (DateParam2 && (DateParam2->AV["value"].Length())))
         {
          tmp += " " + DateType->AV["name"];
          if (DateParam1->AV["value"].Length())
           {
            tmp += " " + DateParam1->AV["name"];
            tmp += "='" + DateParam1->AV["value"] + "'";
           }
          if (DateParam2->AV["value"].Length())
           {
            tmp += " " + DateParam2->AV["name"];
            tmp += "='" + DateParam2->AV["value"] + "'";
           }
         }
       }
      else if (DType >= 3)
       { // 3 - ������ ������������ ���������
        // 4 - ������ ���� - ���� ��������� ������� ������������ ���������
        // 5 - ������ ��������� - ���� ��������� ������� ������������ ���������
        // 6 - ������ �������� - ���� ��������� ������� ������������ ���������
        // 7 - ������ ������ - ���� ��������� ������� ������������ ���������
        tmp += " " + DateType->AV["name"];
        if (DateParam3->AV["value"].Length())
         {
          tmp += " " + DateParam3->AV["name"];
          tmp += "='" + DateParam3->AV["value"] + "'";
         }
       }
      else
       { // ������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
        tmp += " " + DateType->AV["name"];
        if (DateParam1->AV["value"].Length())
         {
          tmp += " " + DateParam1->AV["name"];
          tmp += "='" + DateParam1->AV["value"] + "'";
         }
        if (DateParam2->AV["value"].Length())
         {
          tmp += " " + DateParam2->AV["name"];
          tmp += "='" + DateParam2->AV["value"] + "'";
         }
        if (DateParam3->AV["value"].Length())
         {
          tmp += " " + DateParam3->AV["name"];
          tmp += "='" + DateParam3->AV["value"] + "'";
         }
       }
      if (DateParam4)
       {
        tmp += " " + DateParam4->AV["name"];
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLDatePeriodLast(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode * FIS = FISAttr->GetParent("is");
  TTagNode * DateOffs, *__tmp;
  ATab = FGetGUI(FISCondRules) + "." + FIS->AV["uid"] + ":";
  UnicodeString xFr, xTo, flName, xPer, xOffs, xTabName, xMaxSQL, xMaxTabName, MaxflName;
  xFr      = "";
  xTo      = "";
  xTabName = FIS->AV["maintable"];
  flName   = FIS->AV["tabalias"] + "." + FISAttr->AV["fields"];
  int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
  bool FLast = (bool)(DType & 1);
  DType = DType >> 1;
  TTagNode * TagFr, *TagTo;
  __tmp       = AIE->GetFirstChild()->GetNext();
  TagFr       = __tmp;
  __tmp       = __tmp->GetNext();
  TagTo       = __tmp;
  __tmp       = __tmp->GetNext();
  DateOffs    = __tmp;
  __tmp       = __tmp->GetNext();
  xMaxTabName = "DTT" + IntToStr((int)FLast);
  xMaxSQL     = flName + " = (Select Max(" + xMaxTabName + "." + FISAttr->AV["fields"] + ") From " + xTabName + " " +
    xMaxTabName;
  MaxflName = xMaxTabName + "." + FISAttr->AV["fields"];
  if (DType == 0)
   { // �������� ���
    if (TagFr && (TagFr->AV["value"].Length()))
     xFr = ">='" + _CorrectDate(TagFr->AV["value"]) + "'";
    if (TagTo && (TagTo->AV["value"].Length()))
     xTo = "<='" + _CorrectDate(TagTo->AV["value"]) + "'";
   }
  else if (DType == 1)
   { // ������ �� ������� ����
    xPer  = TagFr->AV["value"];
    xOffs = DateOffs->AV["value"];
    if (!xOffs.Length())
     {
      xFr = ">='" + FIncPeriod(Date().FormatString("dd.mm.yyyy"), xPer, 1) + "'";
      xTo = "<='" + Date().FormatString("dd.mm.yyyy") + "'";
     }
    else
     {
      xFr = ">='" + FIncPeriod(Date().FormatString("dd.mm.yyyy"), AddPeriod(xPer, xOffs), 1) + "'";
      xTo = "<='" + FIncPeriod(Date().FormatString("dd.mm.yyyy"), xOffs, 1) + "'";
     }
   }
  else if (DType == 2)
   { // ������ �� ���� ��������� ������� ������������ ���������
    UnicodeString FDocPer = GetPart1(UnicodeString(ARet), '#');
    if (FDocPer.Length())
     {
      if (GetPart2(FDocPer, ';').Length())
       FDocPer = GetPart2(FDocPer, ';');
      else
       FDocPer = Date().FormatString("dd.mm.yyyy");
     }
    else
     FDocPer = Date().FormatString("dd.mm.yyyy");
    xPer  = TagFr->AV["value"];
    xOffs = DateOffs->AV["value"];
    if (!xOffs.Length())
     {
      xFr = ">='" + FIncPeriod(FDocPer, xPer, 1) + "'";
      xTo = "<='" + FDocPer + "'";
     }
    else
     {
      xFr = ">='" + FIncPeriod(FDocPer, AddPeriod(xPer, xOffs), 1) + "'";
      xTo = "<='" + FIncPeriod(FDocPer, xOffs, 1) + "'";
     }
   }
  else if (DType >= 3)
   { // 3 - ������ ������������ ���������
    // 4 - ������ ���� - ���� ��������� ������� ������������ ���������
    // 5 - ������ ��������� - ���� ��������� ������� ������������ ���������
    // 6 - ������ �������� - ���� ��������� ������� ������������ ���������
    // 7 - ������ ������ - ���� ��������� ������� ������������ ���������
    UnicodeString FPer = GetPart1(UnicodeString(ARet), '#');
    if (FPer.Length())
     {
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
        if (GetPart1(FPer, ';').Length())
         xFr = ">='" + TDate(GetPart1(FPer, ';')).FormatString("dd.mm.yyyy") + "'";
        if (GetPart2(FPer, ';').Length())
         xTo = "<='" + TDate(GetPart2(FPer, ';')).FormatString("dd.mm.yyyy") + "'";
       }
      else
       {
        if (GetPart1(FPer, ';').Length())
         xFr = ">='" + FIncPeriod(GetPart1(FPer, ';'), xOffs, 1) + "'";
        if (GetPart2(FPer, ';').Length())
         xTo = "<='" + FIncPeriod(GetPart2(FPer, ';'), xOffs, 1) + "'";
       }
     }
   }
  if (xFr.Length())
   {
    xMaxSQL += " and " + MaxflName + xFr;
    xFr = flName + xFr;
   }
  if (xTo.Length())
   {
    xMaxSQL += " and " + MaxflName + xTo;
    xTo = flName + xTo;
   }
  if (xFr.Length() && xTo.Length())
   {
    if (FLast)
     ARet = "(" + xFr + " and " + xTo + " and " + xMaxSQL + ")";
    else
     ARet = "(" + xFr + " and " + xTo + ")";
   }
  else if (xFr.Length())
   {
    if (FLast)
     ARet = "(" + xFr + " and " + xMaxSQL + ")";
    else
     ARet = xFr;
   }
  else if (xTo.Length())
   {
    if (FLast)
     ARet = "(" + xTo + " and " + xMaxSQL + ")";
    else
     ARet = xTo;
   }
  else
   {
    if (FLast)
     ARet = xMaxSQL;
    else
     ARet = "";
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::DefChoiceDBValue(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  UnicodeString tmp = "";
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  /*
   if (NeedKonkr)
   {
   TTagNode *FExtAttr = FDM->XMLList->GetRefer(FISAttr->AV["ref"]);
   tmp = "<root>"
   "<fl ref='"+FExtAttr->AV["uid"]+"' req=1'/>"
   "</root>";
   }
   */
  return FDM->DefRegValue(AIE, AText, AXMLIE, tmp, _GUI(FISAttr->AV["ref"]), false, false);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLChoiceDBValue(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * sqlNode = AIE->GetChildByAV("text", "name", "sql");
  if (sqlNode)
   {
    if (!sqlNode->AV["value"].Trim().Length())
     {
      UnicodeString FTmpl = "";
      TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
      TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      TTagNode * FExtAttr = FDM->XMLList->GetRefer(FISAttr->AV["ref"]);
      FTmpl = "<root><fl ref='" + FExtAttr->AV["uid"] + "' req=1'/></root>";
      GetSQLDefValue(AIE, FTmpl);
     }
   }
  GetSQLValue(AIE, ATab, ARet);
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::DefRegVal(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  if (!this->FDM)
   return false;
  bool CanAllSelect = false;
  bool SubSel = false;
  UnicodeString tmp = "";
  UnicodeString FGUI = "";
  /*
   if (NeedKonkr)
   {
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (FISCondRules)
   {
   TTagNode *FRoot = FISCondRules->GetChildByName("root");
   if (FRoot)
   {
   tmp = FRoot->AsXML;
   CanAllSelect = (bool)(FRoot->AV["CanAllSelect"].ToIntDef(0));
   SubSel       = (bool)(FRoot->AV["subsel"].ToIntDef(0));
   FGUI         = FRoot->AV["gui_ref"];
   }
   }
   }
   */
  return FDM->DefRegValue(AIE, AText, AXMLIE, tmp, FGUI, CanAllSelect, SubSel);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLRegVal(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  dsLogMessage(AIE->AsXML, __FUNC__, 1);
  TTagNode * sqlNode = AIE->GetChildByAV("text", "name", "sql");
  if (sqlNode)
   {
    if (!sqlNode->AV["value"].Trim().Length())
     {
      UnicodeString FTmpl = "";
      TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
      if (FISCondRules)
       GetSQLDefValue(AIE, FISCondRules->GetChildByName("root")->AsXML);
     }
   }
  GetSQLValue(AIE, ATab, ARet);
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::DefTextValue(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  bool iRC = false;
  UnicodeString tmp = "";
  TTagNode * __tmp;
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  if (AIE->GetFirstChild())
   {
    if (AIE->Count > 1)
     {
      __tmp = AIE->GetFirstChild();
      tmp   = FISCondRules->AV["name"] + " =";
      while (__tmp->GetNext())
       {
        tmp += " " + __tmp->AV["name"];
        __tmp = __tmp->GetNext();
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLTextValue(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  if (AIE->Count)
   {
    UnicodeString FText = "";
    bool FCaseSens, FFullWord;
    FCaseSens = FFullWord = false;
    int FCompType = 2;
    TTagNode * tmp = AIE->GetChildByAV("text", "ref", "0");
    if (tmp)
     FText = tmp->AV["value"].Trim();
    tmp = AIE->GetChildByAV("text", "ref", "1");
    if (tmp)
     FCaseSens = (bool)tmp->AV["value"].ToIntDef(0);
    tmp = AIE->GetChildByAV("text", "ref", "2");
    if (tmp)
     FFullWord = (bool)tmp->AV["value"].ToIntDef(0);
    tmp = AIE->GetChildByAV("text", "ref", "3");
    if (tmp)
     {
      FCompType = tmp->AV["value"].ToIntDef(2);
      if (tmp->CmpAV("name", "SQL")) // ��������� ������ ��������
         tmp->AV["name"] = "��������� ��������";
     }
    TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
    TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
    TTagNode * FIS = FISAttr->GetParent("is");
    TTagNode * __tmp;
    UnicodeString FFieldName = FIS->AV["tabalias"] + "." + FISAttr->AV["fields"];
    TTagNode * FSQLNode = AIE->GetChildByAV("text", "name", "sql");
    if (!FSQLNode)
     FSQLNode = AIE->AddChild("text", "name=sql");
    FSQLNode->AV["ref"] = "4";
    switch (FCompType)
     {
     case 0:
       {
        FSQLNode->AV["value"] = "(" + FFieldName + " is null or " + FFieldName + " = '')";
        break;
       }
     case 1:
       {
        FSQLNode->AV["value"] = "(" + FFieldName + " is not null and " + FFieldName + " <> '')";
        break;
       }
     case 2:
       {
        if (FFullWord)
         {
          if (FCaseSens)
           FSQLNode->AV["value"] = FFieldName + "='" + FText + "'";
          else
           FSQLNode->AV["value"] = "Upper(" + FFieldName + ")='" + FText.UpperCase() + "'";
         }
        else
         {
          if (FCaseSens)
           FSQLNode->AV["value"] = FFieldName + " LIKE '%" + FText + "%'";
          else
           FSQLNode->AV["value"] = "Upper(" + FFieldName + ") LIKE '%" + FText.UpperCase() + "%'";
         }
        break;
       }
     }
   }
  GetSQLValue(AIE, ATab, ARet);
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::DefExtEditValue(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  // AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
  // ����� �� ���, �� �� �� �� ����� �����
  // ��� ���� "����"
  // {
  // 1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
  // 1,2 ��� �������; 0 - �������� ���
  // 1 - ������ �� ������� ����
  // 2 - ������ �� ����
  // 2-� - ���� ��;   ��� ���� "�������� ���"
  // ��� ����� "������ �� ..." �������� �������
  // 3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
  // ��� ���� "������ �� ������� ����"  - �� �������������;
  // 4-� - �������� - ��� ���� "�������� ���"  - �� �������������
  // �������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
  if (!FDM)
   return false;
  bool iRC = false;
  UnicodeString tmp = "";
  TTagNode * __tmp;
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  if (AIE->GetFirstChild())
   {
    if (AIE->Count)
     {
      // tmp = "";
      tmp   = FISCondRules->AV["name"] + " = ";
      __tmp = AIE->GetFirstChild();
      int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
      bool FLast = (bool)(DType & 1);
      DType = DType >> 1;
      if (FLast)
       {
        TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
        if (!FISAttr->AV["add_ref"].Length())
         tmp = "������ ��������� ������������";
        else
         tmp = FISAttr->AV["add_ref"];
       }
      TTagNode * DateType = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam1 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam2 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam3 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam4 = __tmp;
      if (DType == 0)
       { // �������� ���
        if ((DateParam1 && (DateParam1->AV["value"].Length())) || (DateParam2 && (DateParam2->AV["value"].Length())))
         {
          tmp += " " + DateType->AV["name"];
          if (DateParam1->AV["value"].Length())
           {
            tmp += " " + DateParam1->AV["name"];
            tmp += "='" + DateParam1->AV["value"] + "'";
           }
          if (DateParam2->AV["value"].Length())
           {
            tmp += " " + DateParam2->AV["name"];
            tmp += "='" + DateParam2->AV["value"] + "'";
           }
         }
       }
      else if (DType == 3)
       { // ������ ������������ ���������
        tmp += " " + DateType->AV["name"];
        if (DateParam3->AV["value"].Length())
         {
          tmp += " " + DateParam3->AV["name"];
          tmp += "='" + DateParam3->AV["value"] + "'";
         }
       }
      else
       { // ������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
        tmp += " " + DateType->AV["name"];
        if (DateParam1->AV["value"].Length())
         {
          tmp += " " + DateParam1->AV["name"];
          tmp += "='" + DateParam1->AV["value"] + "'";
         }
        if (DateParam2->AV["value"].Length())
         {
          tmp += " " + DateParam2->AV["name"];
          tmp += "='" + DateParam2->AV["value"] + "'";
         }
        if (DateParam3->AV["value"].Length())
         {
          tmp += " " + DateParam3->AV["name"];
          tmp += "='" + DateParam3->AV["value"] + "'";
         }
       }
      if (DateParam4)
       {
        tmp += " " + DateParam4->AV["name"];
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLExtEditValue(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode * FIS = FISAttr->GetParent("is");
  TTagNode * DateOffs, *__tmp;
  ATab = FGetGUI(FISCondRules) + "." + FIS->AV["uid"] + ":";
  UnicodeString xFr, xTo, flName, xPer, xOffs, xTabName, xMaxSQL, xMaxTabName, MaxflName;
  xFr      = "";
  xTo      = "";
  xTabName = FIS->AV["maintable"];
  flName   = FIS->AV["tabalias"] + "." + FISAttr->AV["fields"];
  int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
  bool FLast = (bool)(DType & 1);
  DType = DType >> 1;
  TTagNode * TagFr, *TagTo;
  __tmp       = AIE->GetFirstChild()->GetNext();
  TagFr       = __tmp;
  __tmp       = __tmp->GetNext();
  TagTo       = __tmp;
  __tmp       = __tmp->GetNext();
  DateOffs    = __tmp;
  __tmp       = __tmp->GetNext();
  xMaxTabName = "DTT" + IntToStr((int)FLast);
  xMaxSQL     = flName + " = (Select Max(" + xMaxTabName + "." + FISAttr->AV["fields"] + ") From " + xTabName + " " +
    xMaxTabName;
  MaxflName = xMaxTabName + "." + FISAttr->AV["fields"];
  if (DType == 0)
   { // �������� ���
    if (TagFr && (TagFr->AV["value"].Length()))
     xFr = ">='" + _CorrectDate(TagFr->AV["value"]) + "'";
    if (TagTo && (TagTo->AV["value"].Length()))
     xTo = "<='" + _CorrectDate(TagTo->AV["value"]) + "'";
   }
  else if (DType == 1)
   { // ������ �� ������� ����
    xPer  = TagFr->AV["value"];
    xOffs = DateOffs->AV["value"];
    if (!xOffs.Length())
     {
      xFr = ">='" + FIncPeriod(Date().FormatString("dd.mm.yyyy"), xPer, 1) + "'";
      xTo = "<='" + Date().FormatString("dd.mm.yyyy") + "'";
     }
    else
     {
      xFr = ">='" + FIncPeriod(Date().FormatString("dd.mm.yyyy"), AddPeriod(xPer, xOffs), 1) + "'";
      xTo = "<='" + FIncPeriod(Date().FormatString("dd.mm.yyyy"), xOffs, 1) + "'";
     }
   }
  else if (DType == 2)
   { // ������ �� ���� ��������� ������� ������������ ���������
    UnicodeString FDocPer = GetPart1(UnicodeString(ARet), '#');
    if (FDocPer.Length())
     {
      if (GetPart2(FDocPer, ';').Length())
       FDocPer = GetPart2(FDocPer, ';');
      else
       FDocPer = Date().FormatString("dd.mm.yyyy");
     }
    else
     FDocPer = Date().FormatString("dd.mm.yyyy");
    xPer  = TagFr->AV["value"];
    xOffs = DateOffs->AV["value"];
    if (!xOffs.Length())
     {
      xFr = ">='" + FIncPeriod(FDocPer, xPer, 1) + "'";
      xTo = "<='" + FDocPer + "'";
     }
    else
     {
      xFr = ">='" + FIncPeriod(FDocPer, AddPeriod(xPer, xOffs), 1) + "'";
      xTo = "<='" + FIncPeriod(FDocPer, xOffs, 1) + "'";
     }
   }
  else if (DType == 3)
   { // ������ ������������ ���������
    UnicodeString FPer = GetPart1(UnicodeString(ARet), '#');
    if (FPer.Length())
     {
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
        if (GetPart1(FPer, ';').Length())
         xFr = ">='" + TDate(GetPart1(FPer, ';')).FormatString("dd.mm.yyyy") + "'";
        if (GetPart2(FPer, ';').Length())
         xTo = "<='" + TDate(GetPart2(FPer, ';')).FormatString("dd.mm.yyyy") + "'";
       }
      else
       {
        if (GetPart1(FPer, ';').Length())
         xFr = ">='" + FIncPeriod(GetPart1(FPer, ';'), xOffs, 1) + "'";
        if (GetPart2(FPer, ';').Length())
         xTo = "<='" + FIncPeriod(GetPart2(FPer, ';'), xOffs, 1) + "'";
       }
     }
   }
  if (xFr.Length())
   {
    xMaxSQL += " and " + MaxflName + xFr;
    xFr = flName + xFr;
   }
  if (xTo.Length())
   {
    xMaxSQL += " and " + MaxflName + xTo;
    xTo = flName + xTo;
   }
  if (xFr.Length() && xTo.Length())
   {
    if (FLast)
     ARet = "(" + xFr + " and " + xTo + " and " + xMaxSQL + ")";
    else
     ARet = "(" + xFr + " and " + xTo + ")";
   }
  else if (xFr.Length())
   {
    if (FLast)
     ARet = "(" + xFr + " and " + xMaxSQL + ")";
    else
     ARet = xFr;
   }
  else if (xTo.Length())
   {
    if (FLast)
     ARet = "(" + xTo + " and " + xMaxSQL + ")";
    else
     ARet = xTo;
   }
  else
   {
    if (FLast)
     ARet = xMaxSQL;
    else
     ARet = "";
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::DefOrgPres(TTagNode * AIE, UnicodeString & AText, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  bool RC = false;
  TStringList * FValues = NULL;
  try
   {
    FValues = new TStringList;
    FValues->AddObject("���������� �����������", (TObject *)0);
    FValues->AddObject("������� �����������", (TObject *)1);
    RC = FDM->DefChVal(AIE, AText, AXMLIE, FValues);
   }
  __finally
   {
    if (FValues)
     delete FValues;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLOrgPres(TTagNode * AIE, UnicodeString & ATab, UnicodeString & ARet)
 {
  TTagNode * FValNode = AIE->GetFirstChild();
  if (FValNode && !FValNode->CmpAV("name", "sql"))
   {
    TTagNode * FSQLNode = AIE->GetChildByAV("text", "name", "sql");
    if (!FSQLNode)
     FSQLNode = AIE->AddChild("text", "name=sql");
    if (FValNode->AV["value"].ToIntDef(0))
     FSQLNode->AV["value"] = "(CB.R0025 is not null and CB.R0025 <> 0)";
    else
     FSQLNode->AV["value"] = "(CB.R0025 is null or CB.R0025 = 0)";
   }
  GetSQLValue(AIE, ATab, ARet);
 }
// ---------------------------------------------------------------------------
TNomGetExtValuesEvent __fastcall TICSBaseNomDef::FGetOnExtGetAVal()
 {
  if (FDM)
   return FDM->OnExtGetAVal;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::FSetOnExtGetAVal(TNomGetExtValuesEvent AValue)
 {
  if (FDM)
   FDM->OnExtGetAVal = AValue;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::FGetMainLPUCode()
 {
  if (FDM)
   {
    dsLogMessage("TICSBaseNomDef::FGetMainLPUCode " + FDM->MainLPUCode);
    return FDM->MainLPUCode;
   }
  else
   return "";
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::FSetMainLPUCode(UnicodeString AValue)
 {
  if (FDM)
   FDM->MainLPUCode = AValue;
  dsLogMessage("TICSBaseNomDef::FSetMainLPUCode " + FDM->MainLPUCode);
 }
// ---------------------------------------------------------------------------
TNomGetStrValByCodeEvent __fastcall TICSBaseNomDef::FGetOnGetAddrStr()
 {
  if (FDM)
   return FDM->OnGetAddrStr;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::FSetOnGetAddrStr(TNomGetStrValByCodeEvent AValue)
 {
  if (FDM)
   FDM->OnGetAddrStr = AValue;
 }
// ---------------------------------------------------------------------------
TNomGetStrValByCodeEvent __fastcall TICSBaseNomDef::FGetOnGetOrgStr()
 {
  if (FDM)
   return FDM->OnGetOrgStr;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::FSetOnGetOrgStr(TNomGetStrValByCodeEvent AValue)
 {
  if (FDM)
   FDM->OnGetOrgStr = AValue;
 }
// ---------------------------------------------------------------------------
TNomGetFieldValByCodeEvent __fastcall TICSBaseNomDef::FGetOnGetOrgFieldStr()
 {
  if (FDM)
   return FDM->OnGetOrgFieldStr;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::FSetOnGetOrgFieldStr(TNomGetFieldValByCodeEvent AValue)
 {
  if (FDM)
   FDM->OnGetOrgFieldStr = AValue;
 }
// ---------------------------------------------------------------------------
