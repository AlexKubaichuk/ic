﻿//---------------------------------------------------------------------------

#ifndef dsDocDMUnitH
#define dsDocDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <FireDAC.Phys.FB.hpp>
//#include <FireDAC.Comp.Client.hpp>
//#include <FireDAC.Phys.hpp>
//#include <FireDAC.Phys.Intf.hpp>
//#include <FireDAC.Stan.Def.hpp>
//#include <FireDAC.Stan.Error.hpp>
//#include <FireDAC.Stan.Intf.hpp>
//#include <FireDAC.Stan.Option.hpp>
//#include <FireDAC.UI.Intf.hpp>
//#include <Data.DB.hpp>
//#include <FireDAC.Comp.DataSet.hpp>
//#include <FireDAC.DApt.hpp>
//#include <FireDAC.DApt.Intf.hpp>
//#include <FireDAC.DatS.hpp>
//#include <FireDAC.Stan.Async.hpp>
//#include <FireDAC.Stan.Param.hpp>
//#include <FireDAC.Stan.Pool.hpp>
////---------------------------------------------------------------------------
//#include <FireDAC.Phys.FBDef.hpp>
//#include <FireDAC.Comp.UI.hpp>
//#include <FireDAC.VCLUI.Wait.hpp>
#include <System.JSON.hpp>
//#include "dsSrvClassifUnit.h"
#include "XMLContainer.h"
#include "dsDocXMLUtils.h"
#include "icsLog.h"
#include "dsDocSrvTypeDef.h"

#include "IMMNomDef.h"
#include "BaseDocNomDef.h"
#include "ProgressCounter.h"
#include "AppOptBaseProv.h"
#include "AppOptions.h"
#include "AppOptXMLProv.h"
#include "dsMKB.h"
#include "kabQueryUtils.h"
//---------------------------------------------------------------------------
enum TdsDocCreateType { dctNone = 0, dctEmpty = 1, dctBase = 2, dctDoc = 3, dctBaseDoc = 4};
enum TCheckDocFilterResult
{
  frOk = 0,     // Ok
  frBad = 1,    // Правила отбора содержат пустые или неконкретизированные секции
  frEmpty = 2,  // У xml-тега "condrules" нет дочерних элементов
  frNone = 3    // Фильтр не создан
};
//---------------------------------------------------------------------------
typedef void __fastcall (__closure *TICSCheckPackItem) (UnicodeString ASpecName, UnicodeString ASpecGUI, int APerType, TDate ADateFr, TDate ADateTo);
typedef UnicodeString __fastcall (__closure *TExtCreateDocEvent) (TTagNode *ASpec, TTagNode *ADocRoot, TTagNode *AFilter, int APerType, TDateTime APeriodDateFr, TDateTime APeriodDateTo);
//---------------------------------------------------------------------------
struct TLPU
{
    __int64 LPUCode;
    UnicodeString Name;
    UnicodeString FullName;
    UnicodeString Addr;
    UnicodeString CODE;
    UnicodeString Email;
    UnicodeString Phone;
    UnicodeString OwnerFIO;
    void Clear()
    {
      __int64 LPUCode = 0;
      UnicodeString Name = "";
      UnicodeString FullName = "";
      UnicodeString Addr = "";
      UnicodeString CODE = "";
      UnicodeString Email = "";
      UnicodeString Phone = "";
      UnicodeString OwnerFIO = "";
    };

};
//---------------------------------------------------------------------------
class TdsDocCreator;
class TdsDocDM : public TDataModule
{
__published:	// IDE-managed Components
 TAppOptions *AppOpt;
 TAppOptXMLProv *AppOptXMLData;
 void __fastcall AppOptLoadXML(TTagNode *AOptNode);
private:	// User declarations
  TFDConnection *FConnection;
  TFDConnection *FDocConnection;
  TAxeXMLContainer *FXMLList;
  TdsCommClassData * FClsData;

  TdsDocCreateType FDefDocCreateType;
  TExtCreateDocEvent FOnExtCreateDoc;
  TExtCreateDocEvent FOnExtDebugCreate;
  TICSIMMNomDef      *FImmNom;
  TICSBaseDocNomDef  *FDocNom;
  TICSBaseNomDef     *FFDefNom;
  TFDQuery* FImmNomQuery;
  TFDQuery* FDocNomQuery;
  TdsDocCreator *FDocCreator;
  TStringStream *FCreatedView;

  void __fastcall FSetDefNom(TICSBaseNomDef *ANom);
  __property TICSBaseNomDef     *FDefNom = {read=FFDefNom, write=FSetDefNom};

  TdsMKB      *FMKB;
//  TAnsiStrMap FMKBData;
//  TAnsiStrMap FMKBExtData;
  bool FNotSaveDoc;
  void __fastcall LoadLPUParam();
  __int64 __fastcall FGetLPUCode();

  TLPU LPUParam;
  UnicodeString   FMainOwnerCode;
  TdsProgressCounter *FProgressCounter;
  UnicodeString FSaveOwnerCode;

  TFDQuery* FClassDataQuery;
  TdsRegClassData *FClassData;

  void __fastcall FSetMainOwnerCode(UnicodeString AVal);

//  UnicodeString __fastcall GetSpecCopyName(UnicodeString AName);
  TFDQuery* __fastcall FCreateTempQuery(TFDConnection *AConnection);
  void      __fastcall FCheckDocItem(TFDQuery *AQuery, UnicodeString ASpecName, UnicodeString ASpecGUI, int APerType, TDate ADateFr, TDate ADateTo);
  void      __fastcall CheckPackage(TICSCheckPackItem ACheckItemEvt, TTagNode *APackDef, TFDQuery *AQuery);
  void      __fastcall FGetFilter(System::UnicodeString AId, TTagNode *AFilter);
  void      __fastcall FGetFilterByGUI(System::UnicodeString AGUI, TTagNode *AFilter);
  void      __fastcall FGetDoc(System::UnicodeString AId, TTagNode *ADoc);
  UnicodeString __fastcall DateFS(TDateTime ADate);
  UnicodeString __fastcall GetThisSpecOwner();
  void __fastcall GetChildSpecOwnerList(TStringList *AChildList, bool AIsName = false);
  UnicodeString __fastcall GetCopyName(UnicodeString AName);

  TTagNode* __fastcall MergeCondRules(TTagNode *AParent,TTagNode *ARules1, TTagNode *ARules2, bool AORType);
  TTagNode* __fastcall FMergeCondRules(TTagNode *ARules1, TTagNode *ARules2, bool AORType);
  TCheckDocFilterResult __fastcall InternalCheckCondRules(TTagNode* ACondRules);
  TCheckDocFilterResult __fastcall CheckCondRules(TTagNode* ACondRules);

  void __fastcall FDocGetSVal(UnicodeString AFuncName, TTagNode *ANode, UnicodeString &ATab, UnicodeString &ARet);
  bool __fastcall FDocGetAVal(UnicodeString AFuncName, UnicodeString AFields, UnicodeString ARef, UnicodeString AAttrRef, UnicodeString &ARet);
  bool __fastcall FDocGetDVal(UnicodeString AFuncName, TTagNode *ANode, UnicodeString &AText, UnicodeString &AIEXML);

  void __fastcall FSaveDocument(TTagNode *ADoc, TTagNode *ASpec, bool AHandCreate, int AIntegrated);

  TNomGetExtValuesEvent __fastcall FGetOnExtGetAVal();
  void __fastcall FSetOnExtGetAVal(TNomGetExtValuesEvent AValue);
  void __fastcall DocCreateOnComplite(System::TObject* Sender);
//  void __fastcall LoadMKB();
//  bool __fastcall FLoad/MKB(TTagNode* ANode);
  void __fastcall LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl = 1);
  void __fastcall LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl = 1);
  void __fastcall LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime = 0, int ALvl = 1);
  void __fastcall LogBegin(UnicodeString AFuncName);
  void __fastcall LogEnd(UnicodeString AFuncName);

public:		// User declarations

  __fastcall TdsDocDM(TComponent* Owner, TFDConnection *ADocConnection, TFDConnection *AICConnection, TdsMKB *AMKB, TAxeXMLContainer *AXMLList);
  __fastcall ~TdsDocDM();

  __property TAxeXMLContainer* XMLList = {read=FXMLList};
  __property TExtCreateDocEvent OnExtCreateDoc = {read=FOnExtCreateDoc, write=FOnExtCreateDoc};
  __property TExtCreateDocEvent OnExtDebugCreate = {read=FOnExtDebugCreate, write=FOnExtDebugCreate};

  bool      __fastcall GetSpecById(System::UnicodeString AId, TTagNode *ASpec);
  UnicodeString __fastcall GetSpecNameByRef(UnicodeString AGUI);
  TFDQuery* __fastcall CreateTempQuery();
  TFDQuery* __fastcall CreateDocTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AParam1Name, UnicodeString AParam1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AParam2Name = "", Variant AParam2Val = 0,
                               UnicodeString AParam3Name = "", Variant AParam3Val = 0);
  void      __fastcall LoadDocs(TTagNode *ACreateStep, TTagNode *ADoc, TList *ADocList);
  System::UnicodeString __fastcall NewGUID();
  System::UnicodeString __fastcall GetNewTabCode(System::UnicodeString ATab, System::UnicodeString AKeyFl);
  UnicodeString __fastcall GetFilterByGUI(System::UnicodeString AGUI);

  UnicodeString __fastcall GetClassData(UnicodeString ACode, UnicodeString AName);
  UnicodeString __fastcall GetMIBPName(int AGUIType, UnicodeString  ACode);
  UnicodeString __fastcall GetVacType(UnicodeString  ASrc);


  void                  __fastcall SetMainLPU(__int64 ALPUCode);
  System::UnicodeString __fastcall GetMainLPUParam();
  System::UnicodeString __fastcall GetSpecTextById(System::UnicodeString AId, int &AType);
  System::UnicodeString __fastcall GetDocTextById(System::UnicodeString AId);
  System::UnicodeString __fastcall CheckByPeriod(bool FCreateByType, UnicodeString ACode, UnicodeString ASpecGUI);
  System::UnicodeString __fastcall DocListClearByDate(TDate ADate);
//  UnicodeString         __fastcall MKBCodeToText(UnicodeString ACode);

  int                   __fastcall GetDocCountBySpec(UnicodeString ASpecOWNER, UnicodeString ASpecGUI, UnicodeString &ARCMsg);
  bool                  __fastcall IsMainOwnerCode(UnicodeString AOwnerCode);
  System::UnicodeString __fastcall CopySpec(UnicodeString AName, UnicodeString &ANewGUI);
  int                   __fastcall CheckSpecExists(UnicodeString AName, UnicodeString &AGUI, bool AIsEdit);
  bool                  __fastcall CheckSpecExistsByCode(UnicodeString AGUI);

  System::UnicodeString __fastcall CreateDocPreview(UnicodeString AId, int &Al, int &At, int &Ar, int &Ab, double &Aps, int &Ao);
  bool                  __fastcall CheckDocCreateType(UnicodeString ASpecGUI, int &ACreateType, int ADefCreateType, int &AGetCreateType, int &AGetPeriod);
  System::UnicodeString __fastcall DocCreateCheckExist(UnicodeString ASpecGUI, UnicodeString AAuthor, int APerType, TDate APeriodDateFr, TDate APeriodDateTo);
  System::UnicodeString __fastcall DocCreateCheckExist(TTagNode *ADoc);
  System::UnicodeString __fastcall CheckFilter(UnicodeString AFilterGUI);
  System::UnicodeString __fastcall DocCreateCheckFilter(UnicodeString ASpecGUI, UnicodeString AFilterCode, UnicodeString &AFilterName, UnicodeString &AFilterGUI);
  System::UnicodeString __fastcall DocCreate(UnicodeString ASpecGUI, UnicodeString AAuthor, UnicodeString AFilter, bool AConcr, int ACreateType, int APerType, TDate APeriodDateFr, TDate APeriodDateTo);
  System::UnicodeString __fastcall StartDocCreate(UnicodeString ASpecGUI, UnicodeString AAuthor, UnicodeString AFilter, UnicodeString AFilterNameGUI, bool AConcr, int ACreateType, int APerType, TDate APeriodDateFr, TDate APeriodDateTo, bool ANotSave = false);
  System::UnicodeString __fastcall DocCreateProgress(System::UnicodeString &ARecId);

  System::UnicodeString __fastcall InsertData(UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString __fastcall EditData(UnicodeString AId, TJSONObject *AValue);
  System::UnicodeString __fastcall DeleteData(UnicodeString AId, UnicodeString ARecId);
  void                  __fastcall SaveDoc(UnicodeString AId);
  System::UnicodeString __fastcall LoadDocPreviewProgress(System::UnicodeString &AMsg);
  TJSONObject* __fastcall GetQList(UnicodeString AId);
  void __fastcall SetQList(UnicodeString AId, TJSONObject *AList);
  bool __fastcall CanHandEdit(UnicodeString ASpecGUI);

  __property TNomGetExtValuesEvent OnExtGetAVal = {read=FGetOnExtGetAVal, write=FSetOnExtGetAVal};

  __property UnicodeString  MainOwnerCode = {read=FMainOwnerCode, write=FSetMainOwnerCode};
  __property __int64 LPUCode = {read=FGetLPUCode};
  __property TdsMKB      *MKB = {read=FMKB};

  __property TICSIMMNomDef*     ImmNom = {read=FImmNom};
  __property TICSBaseDocNomDef* DocNom = {read=FDocNom};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocDM *dsDocDM;
//---------------------------------------------------------------------------
#endif
