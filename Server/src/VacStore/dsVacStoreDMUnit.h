//---------------------------------------------------------------------------

#ifndef dsVacStoreDMUnitH
#define dsVacStoreDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Comp.Client.hpp>
//#include <FireDAC.Phys.hpp>
//#include <FireDAC.Phys.Intf.hpp>
//#include <FireDAC.Stan.Def.hpp>
//#include <FireDAC.Stan.Error.hpp>
//#include <FireDAC.Stan.Intf.hpp>
//#include <FireDAC.Stan.Option.hpp>
//#include <FireDAC.UI.Intf.hpp>
//#include <Data.DB.hpp>
//#include <FireDAC.Comp.DataSet.hpp>
//#include <FireDAC.DApt.hpp>
//#include <FireDAC.DApt.Intf.hpp>
//#include <FireDAC.DatS.hpp>
//#include <FireDAC.Stan.Async.hpp>
//#include <FireDAC.Stan.Param.hpp>
//#include <FireDAC.Stan.Pool.hpp>
////---------------------------------------------------------------------------
//#include <FireDAC.Phys.FBDef.hpp>
//#include <FireDAC.Comp.UI.hpp>
//#include <FireDAC.VCLUI.Wait.hpp>
#include <System.JSON.hpp>
#include "XMLContainer.h"
#include "icsLog.h"
#include "dsVacStoreSrvTypeDef.h"

//---------------------------------------------------------------------------
class TdsVacStoreDM : public TDataModule
{
__published:	// IDE-managed Components
private:	// User declarations
  TFDConnection *FConnection;
  TAxeXMLContainer *FXMLList;

  TFDQuery* FImmNomQuery;
  TFDQuery* FDocNomQuery;

  __int64 FLPUCode;

  UnicodeString   FMainOwnerCode;

  void __fastcall FSetMainOwnerCode(UnicodeString AVal);

//  UnicodeString __fastcall GetSpecCopyName(UnicodeString AName);
  TFDQuery* __fastcall FCreateTempQuery(TFDConnection *AConnection);
  UnicodeString __fastcall DateFS(TDateTime ADate);
  UnicodeString __fastcall GetThisSpecOwner();
//  void __fastcall GetChildSpecOwnerList(TStringList *AChildList, bool AIsName = false);

public:		// User declarations
  __fastcall TdsVacStoreDM(TComponent* Owner, TFDConnection *AICConnection, TAxeXMLContainer *AXMLList);
  __fastcall ~TdsVacStoreDM();

  __property TAxeXMLContainer* XMLList = {read=FXMLList, write=FXMLList};

  bool      __fastcall GetSpecById(System::UnicodeString AId, TTagNode *ASpec);
//  UnicodeString __fastcall GetSpecNameByRef(UnicodeString AGUI);
  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AP1Name,      Variant AP1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AP2Name = "", Variant AP2Val = 0,
                               UnicodeString AP3Name = "", Variant AP3Val = 0,
                               UnicodeString AP4Name = "", Variant AP4Val = 0,
                               UnicodeString AP5Name = "", Variant AP5Val = 0);
//  void          __fastcall LoadDocs(TTagNode *ACreateStep, TTagNode *ADoc, TList *ADocList);
  UnicodeString __fastcall NewGUID();
//  UnicodeString __fastcall GetNewTabCode(System::UnicodeString ATab, System::UnicodeString AKeyFl);
  TJSONObject*  __fastcall GetMIBPList(UnicodeString AId);
  void          __fastcall SetMainLPU(__int64 ALPUCode);

//  bool                  __fastcall IsMainOwnerCode(UnicodeString AOwnerCode);
  UnicodeString __fastcall InsertData(UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  UnicodeString __fastcall EditData(UnicodeString AId, TJSONObject *AValue);
  UnicodeString __fastcall DeleteData(UnicodeString AId, UnicodeString ARecId);
  void          __fastcall WriteOff(int AMIBPCode, UnicodeString  ASer, int ADoseCount, int ADosePerPrivCount);

  __property UnicodeString  MainOwnerCode = {read=FMainOwnerCode, write=FSetMainOwnerCode};
  __property __int64 LPUCode = {read=FLPUCode};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsVacStoreDM *dsVacStoreDM;
//---------------------------------------------------------------------------
#endif
