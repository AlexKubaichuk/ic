// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsVacStoreDMUnit.h"
// #include "dsDocSrvConstDef.h"
// #include "dsDocDocCreatorUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TdsVacStoreDM * dsVacStoreDM;
// ---------------------------------------------------------------------------
__fastcall TdsVacStoreDM::TdsVacStoreDM(TComponent * Owner, TFDConnection * AICConnection, TAxeXMLContainer * AXMLList)
  : TDataModule(Owner)
 {
  dsLogBegin(__FUNC__);
  FConnection           = AICConnection;
  FXMLList              = AXMLList;
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
__fastcall TdsVacStoreDM::~TdsVacStoreDM()
 {
  dsLogBegin(__FUNC__);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsVacStoreDM::FCreateTempQuery(TFDConnection * AConnection)
 {
  dsLogBegin(__FUNC__);
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = AConnection;
    FQ->Connection                       = AConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return FQ;
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsVacStoreDM::CreateTempQuery()
 {
  return FCreateTempQuery(FConnection);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsVacStoreDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  dsLogBegin(__FUNC__);
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsVacStoreDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsVacStoreDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString AP1Name,
  Variant AP1Val, UnicodeString ALogComment, UnicodeString AP2Name, Variant AP2Val, UnicodeString AP3Name,
  Variant AP3Val, UnicodeString AP4Name, Variant AP4Val, UnicodeString AP5Name, Variant AP5Val)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AP1Name)->Value = AP1Val;
      if (AP2Name.Length())
       AQuery->ParamByName(AP2Name)->Value = AP2Val;
      if (AP3Name.Length())
       AQuery->ParamByName(AP3Name)->Value = AP3Val;
      if (AP4Name.Length())
       AQuery->ParamByName(AP4Name)->Value = AP4Val;
      if (AP5Name.Length())
       AQuery->ParamByName(AP5Name)->Value = AP5Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      UnicodeString FParams = "; Params: ";
      if (AP1Name.Length())
       FParams += AP1Name + "=" + VarToStrDef(AP1Val, "") + "; ";
      if (AP2Name.Length())
       FParams += AP2Name + "=" + VarToStrDef(AP2Val, "") + "; ";
      if (AP3Name.Length())
       FParams += AP3Name + "=" + VarToStrDef(AP3Val, "") + "; ";
      if (AP4Name.Length())
       FParams += AP4Name + "=" + VarToStrDef(AP4Val, "") + "; ";
      if (AP5Name.Length())
       FParams += AP5Name + "=" + VarToStrDef(AP5Val, "");
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
// bool __fastcall TdsVacStoreDM::IsMainOwnerCode(UnicodeString AOwnerCode)
// {
// }
////---------------------------------------------------------------------------
// void __fastcall TdsVacStoreDM::LoadDocs(TTagNode *ACreateStep, TTagNode *ADoc, TList *ADocList)
// {
// }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsVacStoreDM::DateFS(TDateTime ADate)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = ADate.FormatString("dd.mm.yyyy");
  try
   {
    /* if (FOnGetDateStringFormat)
     {
     FOnGetDateStringFormat(RC);
     RC = ADate.FormatString(RC);
     } */
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsVacStoreDM::GetThisSpecOwner()
 {
  return FMainOwnerCode;
 }
// ---------------------------------------------------------------------------
// void __fastcall TdsVacStoreDM::GetChildSpecOwnerList(TStringList *AChildList, bool AIsName)
// {
// }
// ---------------------------------------------------------------------------
// UnicodeString __fastcall TdsVacStoreDM::GetSpecNameByRef(UnicodeString AGUI)
// {
// }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsVacStoreDM::InsertData(UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId)
 {
  dsLogMessage("InsertData: ID: " + AId + ", Value: " + AValue->ToString());
  UnicodeString RC = "";
  try
   {
    try
     {
      /*
       switch (FGetModuleType(AId))
       {
       case imtReg:
       {
       break;
       }
       case imtCard:
       {
       break;
       }
       case imtPlan:
       {
       break;
       }
       }
       */
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsVacStoreDM::EditData(UnicodeString AId, TJSONObject * AValue)
 {
  dsLogMessage("EditData: ID: " + AId + ", Value: " + AValue->ToString());
  UnicodeString RC = "";
  try
   {
    try
     {
      /*
       switch (FGetModuleType(AId))
       {
       case imtReg:
       {
       break;
       }
       case imtCard:
       {
       break;
       }
       case imtPlan:
       {
       break;
       }
       }
       */
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsVacStoreDM::DeleteData(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogMessage("DeleteData: ID: " + AId + ", RecId: " + ARecId);
  UnicodeString RC = "";
  try
   {
    try
     {
      /*
       switch (FGetModuleType(AId))
       {
       case imtReg:
       {
       break;
       }
       case imtCard:
       {
       break;
       }
       case imtPlan:
       {
       break;
       }
       }
       */
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsVacStoreDM::NewGUID()
 {
  return icsNewGUID().UpperCase();
 }
// ---------------------------------------------------------------------------
// System::UnicodeString __fastcall TdsVacStoreDM::GetNewTabCode(System::UnicodeString ATab, System::UnicodeString AKeyFl)
// {
// }
// ----------------------------------------------------------------------------
void __fastcall TdsVacStoreDM::FSetMainOwnerCode(UnicodeString AVal)
 {
  FMainOwnerCode = AVal.UpperCase();
 }
// ---------------------------------------------------------------------------
// UnicodeString __fastcall TdsVacStoreDM::GetSpecCopyName(UnicodeString AName)
// {
// }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsVacStoreDM::GetMIBPList(UnicodeString AId)
 {
  dsLogMessage("TdsVacStoreDM::GetMIBPList");
  TJSONObject * RC = new TJSONObject;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    if (AId == "1")
     { // �������
      quExec(FQ, false, "Select R1147 as MIBP, R1148 AS DCOUNT From CLASS_1122");
     }
    else if (AId == "2")
     { // �����
      quExec(FQ, false, "Select CODE as MIBP, 10000 AS DCOUNT From CLASS_002A");
     }
    while (!FQ->Eof)
     {
      if (FQ->FieldByName("DCOUNT")->AsInteger)
       RC->AddPair(FQ->FieldByName("MIBP")->AsInteger, FQ->FieldByName("DCOUNT")->AsInteger);
      FQ->Next();
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsVacStoreDM::SetMainLPU(__int64 ALPUCode)
 {
  if (ALPUCode)
   { // ������� ���
    TFDQuery * FQ = CreateTempQuery();
    try
     {
      quExec(FQ, false, "Update CLASS_00C7 Set R0104 = 0");
      quExecParam(FQ, false, "Update CLASS_00C7 Set R0104 = 1 Where code=:pCode", "pCode", ALPUCode);
      if (FQ->Transaction->Active)
       FQ->Transaction->Commit();
     }
    __finally
     {
      DeleteTempQuery(FQ);
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsVacStoreDM::WriteOff(int AMIBPCode, UnicodeString ASer, int ADoseCount, int ADosePerPrivCount)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  TFDQuery * FQ = CreateTempQuery();
  TFDQuery * FQ2 = CreateTempQuery();
  try
   {
    try
     {
      int FPrivCount = ADoseCount / ADosePerPrivCount;
      quExecParam(FQ2, false,
        "Update CLASS_1121 Set R1141 = R1141-:pCount Where R113F=:pVacCode and Upper(R1140) = :pSeria", "pCount",
        ADoseCount, "", "pVacCode", AMIBPCode, "pSeria", ASer.UpperCase());
      quExecParam(FQ2, false, "Update CLASS_1122 Set R1148 = R1148-:pCount Where R1147=:pVacCode ", "pCount",
        ADoseCount, "", "pVacCode", AMIBPCode);
      quExecParam(FQ, false, "Select first " + IntToStr(FPrivCount) +
        " cguid From CLASS_3003 where r301F > 0 and r3020 = :pr3020 and Upper(r3023) = :pr3023 and R3309 is null Order By R3024 desc",
        "pr3020", AMIBPCode, "", "pr3023", ASer.UpperCase());
      while (!FQ->Eof)
       {
        quExecParam(FQ2, false, "Update CLASS_3003 Set R3309=:pr3309 where cguid = :pcguid", "pr3309", Date(), "",
          "pcguid", FQ->FieldByName("cguid")->AsString);
          FQ->Next();
       }
      if (FQ2->Transaction->Active)
       FQ2->Transaction->Commit();
      if (FQ->Transaction->Active)
       FQ->Transaction->Commit();
     }
    catch (Exception & E)
     {
      if (FQ2->Transaction->Active)
       FQ2->Transaction->Commit();
      if (FQ->Transaction->Active)
       FQ->Transaction->Rollback();
      RC = E.Message;
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
    DeleteTempQuery(FQ2);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
