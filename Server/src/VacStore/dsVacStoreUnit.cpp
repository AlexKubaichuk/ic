﻿//---------------------------------------------------------------------------
//#include <vcl.h>
#pragma hdrstop

#include "dsVacStoreUnit.h"
#include <Datasnap.DSHTTP.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "System.Classes.TPersistent"
#pragma resource "*.dfm"

//TRegClassDM *RegClassDM;
//---------------------------------------------------------------------------
__fastcall TdsVacStoreClass::TdsVacStoreClass(TComponent * Owner, TAxeXMLContainer *AXMLList, TAppOptions *AOpt)
    : TDataModule(Owner)
 {
  dsLogC(__FUNC__);
  try
   {
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes)
     {
      //dsLogMessage(FSes->UserRoles->Text, __FUNC__);
      ConnectDB(IcConnection, AOpt, FSes->UserRoles->Values["VSDB"], __FUNC__);
      FDM                    = new TdsVacStoreDM(this, IcConnection, AXMLList);
      FRegComp               = new TdsSrvRegistry(this, NULL, IcConnection, FDM->XMLList, AOpt, "40381E23-92155860-4448", "", false);
      FRegComp->OnExtGetCode = FGetNewTabCode;
      FDefXML                = FDM->XMLList->GetXML("40381E23-92155860-4448");
      FRegComp->SetRegDef(FDefXML);
      FVSEditors = new TdsVacStoreEditors(FDM);
     }
   }
  catch (Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogC(__FUNC__, true);
 }
//---------------------------------------------------------------------------
__fastcall TdsVacStoreClass::~TdsVacStoreClass()
 {
  dsLogD(__FUNC__);
  DisconnectDB(IcConnection, __FUNC__);
  delete FRegComp;
  delete FVSEditors;
  delete FDM;
  dsLogD(__FUNC__, true);
 }
//---------------------------------------------------------------------------
void TdsVacStoreClass::Disconnect()
 {
  //FRegComp->Disconnect();
 }
//---------------------------------------------------------------------------
__int64 TdsVacStoreClass::GetCount(UnicodeString AId, UnicodeString AFilterParam)
 {
  if (AFilterParam.Length())
   dsLogMessage("GetCount: ID: " + AId + ", filter: " + AFilterParam);
  else
   dsLogMessage("GetCount: ID: " + AId);
  __int64 RC = 0;
  try
   {
    try
     {
      RC = FRegComp->GetCount(GetRPartE(AId).Trim().UpperCase(), AFilterParam);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONObject * TdsVacStoreClass::GetIdList(UnicodeString AId, int AMax, UnicodeString AFilterParam)
 {
  if (AFilterParam.Length())
   dsLogMessage("GetIdList: ID: " + AId + ", Max: " + IntToStr(AMax) + ", filter: " + AFilterParam, "", 5);
  else
   dsLogMessage("GetIdList: ID: " + AId + ", Max: " + IntToStr(AMax), "", 5);
  TJSONObject * RC;
  try
   {
    try
     {
      RC = FRegComp->GetIdList(GetRPartE(AId).Trim().UpperCase(), AMax, AFilterParam);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONObject * TdsVacStoreClass::GetValById(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogMessage("GetValById: ID: " + AId + ", RecId: " + ARecId);
  TJSONObject * RC;
  try
   {
    try
     {
      RC = FRegComp->GetValById(GetRPartB(AId).Trim(), ARecId);
      /*
       switch (FGetModuleType(AId))
       {
       case imtReg:
       {
       RC = FRegComp->GetValById(GetRPartB(AId).Trim().UpperCase(), ARecId);
       break;
       }
       case imtCard:
       {
       break;
       }
       case imtPlan:
       {
       break;
       }
       }
       */
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONObject * TdsVacStoreClass::GetValById10(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogMessage("GetValById: ID: " + AId + ", RecId: " + ARecId);
  TJSONObject * RC;
  try
   {
    try
     {
      RC = FRegComp->GetValById10(GetRPartB(AId).Trim(), ARecId);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONObject * TdsVacStoreClass::Find(UnicodeString AId, UnicodeString AFindData)
 {
  dsLogMessage("Find: ID: " + AId + ", FindParam: " + AFindData);
  TJSONObject * RC;
  try
   {
    try
     {
      RC = FRegComp->Find(GetRPartE(AId).Trim().UpperCase(), AFindData);
      /*
       switch (FGetModuleType(AId))
       {
       case imtReg:
       {
       RC = FRegComp->Find(GetRPartE(AId).Trim().UpperCase(), AFindParam);
       break;
       }
       case imtCard:
       {
       break;
       }
       case imtPlan:
       {
       break;
       }
       }
       */
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString TdsVacStoreClass::InsertData(UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecId)
 {
  dsLogMessage("InsertData: ID: " + AId + ", Value: " + AValue->ToString());
  UnicodeString RC = "";
  try
   {
    try
     {
      UnicodeString FID = GetRPartE(AId).Trim().UpperCase();
      if ((FID == "111F") || (FID == "1120") || (FID == "1124"))
       RC = FVSEditors->InsertData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId);
      else
       RC = FRegComp->InsertData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString TdsVacStoreClass::EditData(UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecId)
 {
  dsLogMessage("EditData: ID: " + AId + ", Value: " + AValue->ToString());
  UnicodeString RC = "";
  try
   {
    try
     {
      UnicodeString FID = GetRPartE(AId).Trim().UpperCase();
      if ((FID == "111F") || (FID == "1120") || (FID == "1124"))
       RC = FVSEditors->EditData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId);
      else
       RC = FRegComp->EditData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString TdsVacStoreClass::DeleteData(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogMessage("DeleteData: ID: " + AId + ", RecId: " + ARecId);
  UnicodeString RC = "";
  try
   {
    try
     {
      UnicodeString FID = GetRPartE(AId).Trim().UpperCase();
      if ((FID == "111F") || (FID == "1120") || (FID == "1124"))
       RC = FVSEditors->DeleteData(GetRPartE(AId).Trim().UpperCase(), ARecId);
      else
       RC = FRegComp->DeleteData(GetRPartE(AId).Trim().UpperCase(), ARecId);
      /*
       switch (FGetModuleType(AId))
       {
       case imtReg:
       {
       break;
       }
       case imtCard:
       {
       break;
       }
       case imtPlan:
       {
       break;
       }
       }
       */
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
UnicodeString TdsVacStoreClass::GetTextData(UnicodeString AId, UnicodeString AFlId, UnicodeString ARecId, UnicodeString & AMsg)
 {
  dsLogMessage("DeleteData: ID: " + AId + ", RecId: " + ARecId);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FRegComp->GetTextData(GetRPartE(AId).Trim().UpperCase(), AFlId, ARecId, AMsg);
      /*
       switch (FGetModuleType(AId))
       {
       case imtReg:
       {
       break;
       }
       case imtCard:
       {
       break;
       }
       case imtPlan:
       {
       break;
       }
       }
       */
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString TdsVacStoreClass::SetTextData(UnicodeString AId, UnicodeString AFlId, UnicodeString ARecId, UnicodeString AVal)
 {
  dsLogMessage("DeleteData: ID: " + AId + ", RecId: " + ARecId);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FRegComp->SetTextData(GetRPartE(AId).Trim().UpperCase(), AFlId, ARecId, AVal);
      /*
       switch (FGetModuleType(AId))
       {
       case imtReg:
       {
       break;
       }
       case imtCard:
       {
       break;
       }
       case imtPlan:
       {
       break;
       }
       }
       */
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString TdsVacStoreClass::GetDefXML()
 {
  dsLogMessage("GetDefXML");
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDefXML->AsXML;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString TdsVacStoreClass::GetClassXML(UnicodeString ARef)
 {
  dsLogMessage("GetClassXML: ID: " + ARef);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->XMLList->GetXML(ARef)->AsXML;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString TdsVacStoreClass::GetClassZIPXML(UnicodeString ARef)
 {
  dsLogMessage("GetClassZIPXML: ID: " + ARef);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->XMLList->GetXML(ARef)->AsXML;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
UnicodeString TdsVacStoreClass::MainOwnerCode()
 {
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->MainOwnerCode;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool TdsVacStoreClass::IsMainOwnerCode(UnicodeString AOwnerCode)
 {
  dsLogMessage("IsMainOwnerCode:  SpecOwner: " + AOwnerCode);
  bool RC = false;
  try
   {
    try
     {
      //RC = FDM->IsMainOwnerCode(AOwnerCode);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool TdsVacStoreClass::FGetNewTabCode(UnicodeString ATab, UnicodeString & ARet)
 {
  bool RC = false;
  try
   {
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
System::UnicodeString TdsVacStoreClass::GetMIBPForm()
 {
  dsLogMessage("GetMIBPForm");
  UnicodeString RC = "";
  TStringList * def = new TStringList;
  try
   {
    try
     {
      def->LoadFromFile(icsPrePath(ExtractFilePath(ParamStr(0)) + "\\mibp.htm"));
      RC = def->Text;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete def;
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONObject * TdsVacStoreClass::GetMIBPList(UnicodeString AId)
 {
  dsLogMessage("GetMIBPList");
  TJSONObject * RC;
  try
   {
    RC = FDM->GetMIBPList(AId);
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
void TdsVacStoreClass::SetMainLPU(__int64 ALPUCode)
 {
  try
   {
    try
     {
      FDM->SetMainLPU(ALPUCode);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
 }
//---------------------------------------------------------------------------
void TdsVacStoreClass::WriteOff(int AMIBPCode, UnicodeString  ASer, int ADoseCount, int ADosePerPrivCount)
 {
  try
   {
    try
     {
      FDM->WriteOff(AMIBPCode, ASer, ADoseCount, ADosePerPrivCount);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
 }
//---------------------------------------------------------------------------

