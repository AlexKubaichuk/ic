// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsKLADRDMUnit.h"
// #include "dsDocViewCreator.h"
// #include "dsDocSrvConstDef.h"
// #include "dsDocDocCreatorUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
// ---------------------------------------------------------------------------
#define MAX_SEARCH_COUNT 15
// ---------------------------------------------------------------------------
TdsKLADRDM * dsKLADRDM;
// ---------------------------------------------------------------------------
__fastcall TdsKLADRDM::TdsKLADRDM(TComponent * Owner, TFDConnection * AKLADRConnection,
 TFDConnection * ANFAddrConnection) : TDataModule(Owner)
 {
  FConnection       = AKLADRConnection;
  FNFAddrConnection = ANFAddrConnection;
  FDefaultAddr      = "";
  expandSocrs       = false;
  socr              = new StrStrMap;
  FUseCTTCache      = false;
  // ��������� ������ ��������
  FStreetTypeObj["�����"]                    = 1;
  FStreetTypeSocrObj[" ��."]                 = 1;
  FStreetTypeObj["�������� �����"]           = 2;
  FStreetTypeSocrObj[" �/�"]                 = 2;
  FStreetTypeObj["����������� ����"]         = 3;
  FStreetTypeSocrObj[" �/�"]                 = 3;
  FStreetTypeObj["��������� �������� �����"] = 4;
  FStreetTypeSocrObj[" ���"]                 = 4;
  // FStreetTypeDefIndex = 1;
  // ��������� ������ ����� ��� ��������
  FFlatTypeObj["��������"]  = 1;
  FFlatTypeSocrObj["��."]   = 1;
  FFlatTypeObj["����"]      = 2;
  FFlatTypeSocrObj["����"]  = 2;
  FFlatTypeObj["���������"] = 3;
  FFlatTypeSocrObj["���."]  = 3;
  // FFlatTypeDefIndex = 1;
 }
// ---------------------------------------------------------------------------
__fastcall TdsKLADRDM::~TdsKLADRDM()
 {
  socr->clear();
  delete socr;
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsKLADRDM::FCreateTempQuery(TFDConnection * AConnection)
 {
  //dsLogBegin(__FUNC__);
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = AConnection;
    FQ->Connection                       = AConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  //dsLogEnd(__FUNC__);
  return FQ;
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsKLADRDM::CreateTempQuery()
 {
  return FCreateTempQuery(FConnection);
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsKLADRDM::CreateNFTempQuery()
 {
  if (FNFAddrConnection)
   return FCreateTempQuery(FNFAddrConnection);
  else
   return FCreateTempQuery(FConnection);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsKLADRDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  //dsLogBegin(__FUNC__);
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
  //dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsKLADRDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  //dsLogBegin(__FUNC__);
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  //dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsKLADRDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString AParam1Name,
 Variant AParam1Val, UnicodeString ALogComment, UnicodeString AParam2Name, Variant AParam2Val,
 UnicodeString AParam3Name, Variant AParam3Val, UnicodeString AParam4Name, Variant AParam4Val,
 UnicodeString AParam5Name, Variant AParam5Val, UnicodeString AParam6Name, Variant AParam6Val)
 {
  //dsLogBegin(__FUNC__);
  bool RC = false;
  TTime FBeg = Time();
  UnicodeString FParams = "; Params: ";
  if (AParam1Name.Length())
   FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
  if (AParam2Name.Length())
   FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
  if (AParam3Name.Length())
   FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
  if (AParam4Name.Length())
   FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "") + "; ";
  if (AParam5Name.Length())
   FParams += AParam5Name + "=" + VarToStrDef(AParam5Val, "");
  if (AParam6Name.Length())
   FParams += AParam6Name + "=" + VarToStrDef(AParam6Val, "");
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
      if (AParam2Name.Length())
       AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
      if (AParam3Name.Length())
       AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
      if (AParam4Name.Length())
       AQuery->ParamByName(AParam4Name)->Value = AParam4Val;
      if (AParam5Name.Length())
       AQuery->ParamByName(AParam5Name)->Value = AParam5Val;
      if (AParam6Name.Length())
       AQuery->ParamByName(AParam6Name)->Value = AParam6Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL("[" + IntToStr(AQuery->RecordCount) + "] " + ((ALogComment.Length()) ? ALogComment : ASQL) + FParams,
     __FUNC__, FBeg - Time(), 5);
   }
  //dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsKLADRDM::ReplaceNonAddr(UnicodeString AStr)
 {
  UnicodeString RC = AStr.Trim();
  try
   {
    if (RC.Length())
     {
      System::Sysutils::TReplaceFlags flRep;
      flRep << rfReplaceAll << rfIgnoreCase;

      RC = StringReplace(RC, ",", ", ", flRep);
      RC = StringReplace(RC, "  ", " ", flRep);
      RC = StringReplace(RC, "  ", " ", flRep);
      RC = StringReplace(RC, "  ", " ", flRep);

      RC = StringReplace(RC, ", ������.", " ", flRep);
      RC = StringReplace(RC, ", ������", " ", flRep);
      RC = StringReplace(RC, ", ����.", " ", flRep);
      RC = StringReplace(RC, ", ����", " ", flRep);
      RC = StringReplace(RC, ", ���.", " ", flRep);
      RC = StringReplace(RC, ", �.", " ", flRep);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsKLADRDM::Find(UnicodeString AStr, UnicodeString ADefAddr, int AParams)
 {
  // dsLogMessage("Find: ID: "+ AId+", FindParam: "+AFindParam->ToString());
  // AParams - 0x01 - ������������ ����������������� �����
  // 0x02 - ������������� ������� ��������
  // 0x04 - ������������� ������� ���� � ���� (�� ...)
  // 0x08 - ������������� ������� ����� � ���� (��...)
  // 0x10 - ������������� ������� ���. ������
  TJSONObject * RC = new TJSONObject;
  TStringList * FAddr = new TStringList;
  UnicodeString tmpAddrStr, tmpAddrCode;
  try
   {
    try
     {
      if (AStr.Trim().Length())
       {
        UnicodeString FNFAddrStr = ReplaceNonAddr(AStr);

        UnicodeString FDefAddrStr = "";
        if (ADefAddr.Length() || FDefaultAddr.Length())
         { // ����� � ������������ ������ �� ���������
          if (ADefAddr.Length())
           FDefAddrStr = codeToText(ADefAddr, 0xFC /* "11111100" */ , true);
          else
           FDefAddrStr = codeToText(FDefaultAddr, 0xFC /* "11111100" */ , true);
         }
        if (textToCodeList(FDefAddrStr, FNFAddrStr, FAddr, true, 0x20 & AParams))
         { // ����� ���������
          RC->AddPair("msg", "ok");
          for (int i = 0; i < FAddr->Count; i++)
           {
            // 0x80 - 1 (�������)
            // 0x40 - 2 (�.�.)
            // 0x20 - 3 (�.�.)
            // 0x10 - 4 (�.�.)
            // 0x08 - 5 (�����)
            // 0x04 - 6 (����)
            // 0x02 - 7 (��������)
            // 0x01 - �������� ������
            // 0x10 - ������������� ������� ���. ������
            // 0x08 - ������������� ������� ����� � ���� (��...)
            // 0x04 - ������������� ������� ���� � ���� (�� ...)
            // 0x02 - ������������� ������� ��������
            RC->AddPair(FAddr->Strings[i].Trim(), codeToText(FAddr->Strings[i].Trim(),
             0xE0 + (AParams & 0x1E) /* "11111110" */));
           }
         }
        else
         { // ����� �� ���������, ��������� ��� ���������������
          // AParams - 0x01 - ������������ ����������������� �����
          if (AParams & 0x01)
           {
            RC->AddPair("msg", "ok");
            adresCode FAdres;
            FAdres.Clear();
            if (FAddr->Count)
             {
              FAdres = ParseAddrStr(FAddr->Strings[0].Trim());
             }
            FAdres.NFAddrID = FSaveNFAddr(AStr, FAdres);
            FAdres.State = 0;
            RC->AddPair(FAdres.AsString(), AStr);
           }
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete FAddr;
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsKLADRDM::codeToText(UnicodeString code, unsigned int AParams /* , UnicodeString params */ ,
 bool ANotNFAddr, bool AReplaceCach)
 {
  /*
   // ��������� ������� ����������� ������ � ���������
   // ������ �������� - ������� ������ ������ (� ������� �����)
   // ������ �������� - ��������� �� ��, ����� ������ ������������
   // ��� ����������� ���������� �����������. ������ ����� ������ ����
   // "�������", ��� � ��� 0 ��� 1, ��� ������������� ���������
   // (��� �� ����������) ���������������� ������ ������ � �������� �����.


   // �� ��� ��� ��� ��, ���
   //�� - ��� �������� ���������� ��������� (�������), ���� �������� ������������ � ���������� 2 � �������� �������������� ������� ���������� ��������� (�����);
   //l2 ��� - ��� ������;
   //l3 ��� - ��� ������;
   //l4 ��� - ��� ����������� ������,
   //STATUS
   //0 - ������ �� �������� ������� ���������������-���������������� �����������;
   //1 - ������ �������� ������� ������;
   //2 - ������ �������� ������� (��������) �������;
   //3 - ������ �������� ������������ � ������� ������ � ������� �������;
   //4 - ����������� �����, �.�. �����, � ������� ��������� ����� �������
   //    (������ ��� �������� 2-�� ������).

   //���� "������ �������" ������������ ��� ����������� ������������ ��������� ������
   //� �������������� ���� ������ �����:
   //���� - "1",         �� � ������ ����������� ������ � ���������� ����� (����� �� �����������);
   //���� - "2" ��� "3", �� � ������ ����������� ������ ����� ������� (������ � ����� �� �����������).

   */
  CTTCacheRecord * temp = NULL;
  bool CanDeleteTmp = true;
  UnicodeString RC = "";
  TFDQuery * FQ = CreateTempQuery();
  TFDQuery * FNFQ = CreateNFTempQuery();
  try
   {
    // ��������� ������� ����������� ������ � ���������
    // ������ �������� - ������� ������ ������ (� ������� �����)
    // ������ �������� - ��������� �� ��, ����� ������ ������������
    // ��� ����������� ���������� �����������. ������ ����� ������ ����
    // "�������", ��� � ��� 0 ��� 1, ��� ������������� ���������
    // (��� �� ����������) ���������������� ������ ������ � �������� �����.
    int FLvlIdx;
    bool NotSocr = AParams & 0x100;
    if (!code.IsEmpty())
     {
      if (CTTCache && FUseCTTCache && CacheEntryExists(code) && !ANotNFAddr && !AReplaceCach && !NotSocr)
       { // ������ ���� � ����
        RC = (*CTTCache)[code]->GetText(AParams);
       }
      else
       {
        // prepareData();
        try
         {
          // !!! ?              if (socr->empty()) fillSocrs();
          temp = new CTTCacheRecord;
          dsLogMessage(code, __FUNC__, 1);
          adresCode KLADRcode = ParseAddrStr(code);
          if (ANotNFAddr)
           KLADRcode.State = 1;
          // 2 - 3 - 3 - 3 - 4 - 4 - 4 - 4
          if (KLADRcode.State || (KLADRcode.RegID + KLADRcode.Town1ID + KLADRcode.Town2ID + KLADRcode.Town3ID /* +
            KLADRcode.StreetID */))
           { // ����� ������������
            dsLogMessage(KLADRcode.AsString(), __FUNC__, 1);
            if (AParams & 0xF8)
             {
              // dsLogMessage(KLADRcode.AsString(), __FUNC__, 1);
              UnicodeString query = "";
              query = "Select STATUS FROM KLADR14 WHERE ID1 = :id1 AND ID2 = :id2 AND ID3 = :id3 AND ID4 = :id4";
              quExecParam(FQ, false, query, "id1", KLADRcode.RegID, "������ ���������� ������", "id2",
               KLADRcode.Town1ID, "id3", KLADRcode.Town2ID, "id4", KLADRcode.Town3ID);
              if (FQ->RecordCount)
               temp->Status = FQ->FieldByName("STATUS")->AsInteger;
              // Q->Close();
              query = "";
              // ������ � 1 �� 4 �������
              for (int level = 1; level <= 4; level++)
               {
                if (!query.IsEmpty())
                 query += " OR ";
                query += " ( ID1 = " + IntToStr(level ? KLADRcode.RegID : 0);
                query += " AND ID2 = " + IntToStr(level >= 2 ? KLADRcode.Town1ID : 0);
                query += " AND ID3 = " + IntToStr(level >= 3 ? KLADRcode.Town2ID : 0);
                query += " AND ID4 = " + IntToStr(level >= 4 ? KLADRcode.Town3ID : 0);
                query += ") ";
               }
              query = "SELECT ID1, ID2, ID3, ID4, NAME, STATUS, OBJECTTYPE FROM KLADR14 WHERE (" + query +
               ") ORDER BY ID4, ID3, ID2, ID1";
              quExec(FQ, false, query, "�����, ��������� ����� (1-4)");
              int lastL1id, lastL2id, lastL3id, lastL4id;
              if (FQ->RecordCount)
               {
                lastL1id = FQ->FieldByName("ID1")->AsInteger;
                lastL2id = FQ->FieldByName("ID2")->AsInteger;
                lastL3id = FQ->FieldByName("ID3")->AsInteger;
                lastL4id = FQ->FieldByName("ID4")->AsInteger;
                FLvlIdx  = FGetLastLevelIdx(lastL1id, lastL2id, lastL3id, lastL4id);
                if (FLvlIdx != -1)
                 temp->Values[FLvlIdx] = getFullName(FQ->FieldByName("NAME")->AsString,
                 FQ->FieldByName("OBJECTTYPE")->AsString, NotSocr).Trim();
                FQ->Next();
               }
              UnicodeString result = "";
              while (!FQ->Eof)
               {
                // ��������� �� "��������". �.�. ����� ������ ������ ����� �������������
                // ��� ��� ����� ������������. ����������� �������� �������� � ������
                if (lastL1id == FQ->FieldByName("ID1")->AsInteger && lastL2id == FQ->FieldByName("ID2")
                 ->AsInteger && lastL3id == FQ->FieldByName("ID3")->AsInteger && lastL4id == FQ->FieldByName("ID4")
                 ->AsInteger)
                 {
                  // if (!showOldNames)
                  // {
                  // FQ->Next();
                  // continue;
                  // }
                  result = " (";
                  result += getFullName(FQ->FieldByName("NAME")->AsString, FQ->FieldByName("OBJECTTYPE")->AsString,
                   NotSocr);
                  FQ->Next();
                  while (!FQ->Eof && lastL1id == FQ->FieldByName("ID1")->AsInteger && lastL2id == FQ->FieldByName("ID2")
                   ->AsInteger && lastL3id == FQ->FieldByName("ID3")->AsInteger && lastL4id == FQ->FieldByName("ID4")
                   ->AsInteger)
                   {
                    result += ", ";
                    result += getFullName(FQ->FieldByName("NAME")->AsString, FQ->FieldByName("OBJECTTYPE")->AsString,
                     NotSocr);
                    FQ->Next();
                   }
                  result += ")";
                  FLvlIdx = FGetLastLevelIdx(lastL1id, lastL2id, lastL3id, lastL4id);
                  if (FLvlIdx != -1)
                   temp->Values[FLvlIdx] = (temp->Values[FLvlIdx] + result).Trim();
                 }
                else
                 {
                  lastL1id = FQ->FieldByName("ID1")->AsInteger;
                  lastL2id = FQ->FieldByName("ID2")->AsInteger;
                  lastL3id = FQ->FieldByName("ID3")->AsInteger;
                  lastL4id = FQ->FieldByName("ID4")->AsInteger;
                  FLvlIdx  = FGetLastLevelIdx(lastL1id, lastL2id, lastL3id, lastL4id);
                  if (FLvlIdx != -1)
                   temp->Values[FLvlIdx] = getFullName(FQ->FieldByName("NAME")->AsString,
                   FQ->FieldByName("OBJECTTYPE")->AsString, NotSocr).Trim();
                  FQ->Next();
                 }
               }
              // Q->Close  ();
              // ������ ������ ������, ����� / ������
              if (KLADRcode.StrObjType == sotStreet)
               {
                if (KLADRcode.StreetID)
                 {
                  quExecParam(FQ, false,
                   "SELECT NAME, OBJECTTYPE FROM KLADR5 WHERE " "ID1 = :id1 AND ID2 = :id2 AND ID3 = :id3 "
                   "AND ID4 = :id4 AND ID5 = :id5", "id1", KLADRcode.RegID, "�����, ����� (5)", "id2",
                   KLADRcode.Town1ID, "id3", KLADRcode.Town2ID, "id4", KLADRcode.Town3ID, "id5", KLADRcode.StreetID);
                  if (FQ->RecordCount)
                   temp->Values[4] = getFullName(FQ->FieldByName("NAME")->AsString,
                   FQ->FieldByName("OBJECTTYPE")->AsString, NotSocr).Trim();
                  // FQ->Close  ();
                 }
                if (FQ->Transaction->Active)
                 FQ->Transaction->Commit();
                // ������ ������� ������ (���)
                if ((KLADRcode.House + KLADRcode.Vld + KLADRcode.Korp + KLADRcode.Build).Length())
                 temp->Values[5] = GetHomeFullStr(KLADRcode, NotSocr);
                // ������ �������� ������ (��������)
                if ((KLADRcode.FlatObjType != fotNone) && !KLADRcode.Flat.IsEmpty())
                 {
                  if (NotSocr)
                   temp->Values[6] = " ��" + KLADRcode.Flat;
                  else
                   temp->Values[6] = getFlatTypeText(KLADRcode.FlatObjType) + " " + KLADRcode.Flat;
                 }
               }
              else if (KLADRcode.StrObjType != sotNone)
               { // �/�, �/� ...
                temp->Values[4] = getStreetTypeText(KLADRcode.StrObjType) + " " + PrepareStreetObjectVal
                 (KLADRcode.StreetObjectVal);
               }
              else if (KLADRcode.Town2ID + KLADRcode.Town3ID)
               {
                // ������ ������� ������ (���)
                if ((KLADRcode.House + KLADRcode.Vld + KLADRcode.Korp + KLADRcode.Build).Length())
                 temp->Values[5] = GetHomeFullStr(KLADRcode, NotSocr);
                // ������ �������� ������ (��������)
                if ((KLADRcode.FlatObjType != fotNone) && !KLADRcode.Flat.IsEmpty())
                 {
                  if (NotSocr)
                   temp->Values[6] = " ��" + KLADRcode.Flat;
                  else
                   temp->Values[6] = getFlatTypeText(KLADRcode.FlatObjType) + " " + KLADRcode.Flat;
                 }
               }
              // ������ �������� ������ (������)
              temp->Values[7] = getIndeks(KLADRcode).Trim();
             }
            else
             { // ������ ��� � ��������
              UnicodeString query = "";
              // ������ � 1 �� 4 �������
              temp->Values[0] = "";
              temp->Values[1] = "";
              temp->Values[2] = "";
              temp->Values[3] = "";
              temp->Values[4] = "";
              // ������ ������� ������ (���)
              if ((KLADRcode.House + KLADRcode.Vld + KLADRcode.Korp + KLADRcode.Build).Length())
               temp->Values[5] = GetHomeFullStr(KLADRcode, NotSocr);
              // ������ �������� ������ (��������)
              if ((KLADRcode.FlatObjType != fotNone) && !KLADRcode.Flat.IsEmpty())
               {
                if (NotSocr)
                 temp->Values[6] = " ��" + KLADRcode.Flat;
                else
                 temp->Values[6] = getFlatTypeText(KLADRcode.FlatObjType) + " " + KLADRcode.Flat;
               }
             }
            RC = temp->GetText(AParams);
            if (CTTCache && FUseCTTCache && !NotSocr)
             {
              (*CTTCache)[code] = temp;
              CanDeleteTmp = false;
             }
           }
          else
           { // ����� �� ������������
            if (KLADRcode.NFAddrID)
             {
              quExecParam(FNFQ, false, "SELECT NAME FROM KLADRNF where uid=:id", "id", KLADRcode.NFAddrID,
               "����������������� �����");
              if (FNFQ->RecordCount)
               {
                temp->Values[0] = FNFQ->FieldByName("NAME")->AsString;
                // Q->Close();
               }
              RC = temp->GetText(AParams);
              if (CTTCache && FUseCTTCache)
               {
                (*CTTCache)[code] = temp;
                CanDeleteTmp = false;
               }
             }
           }
         }
        __finally
         {
          if (FQ->Transaction->Active)
           FQ->Transaction->Commit();
         }
       }
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
    DeleteTempQuery(FNFQ);

    if (CanDeleteTmp && temp)
     delete temp;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsKLADRDM::CacheEntryExists(UnicodeString code)
 {
  CTTCacheMap::iterator i = CTTCache->find(code);
  return (i != CTTCache->end());
 }
// ---------------------------------------------------------------------------
int __fastcall TdsKLADRDM::FGetLastLevelIdx(int ALvl1, int ALvl2, int ALvl3, int ALvl4)
 {
  if (ALvl4)
   return 3;
  else if (ALvl3)
   return 2;
  else if (ALvl2)
   return 1;
  else if (ALvl1)
   return 0;
  else
   return -1;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsKLADRDM::getFullName(UnicodeString name, UnicodeString object, bool ANotSocr)
 {
  UnicodeString result;
  if (ANotSocr)
   result = name;
  else
   {
    if ((object == "����" && name.SubString(name.Length() - 2, 2) != "��") || object == "�" || object == "���" ||
     object == "��" || object == "�/�" || object == "���" || object == "���" || object == "�����" ||
     (object == "�����" && name.SubString(name.Length() - 2, 2) != "��") ||
     (object == "�����" && name.SubString(name.Length() - 2, 2) != "��") ||
     (object == "�������" && name.SubString(name.Length() - 2, 2) != "��") || object == "�" || object == "��" ||
     object == "�/�_�����" || object == "�/�_������" || object == "�/�_��" || object == "�/�_�����" ||
     object == "�/�_����" || object == "�/�_���" || object == "�/�_��" || object == "��" || object == "������" ||
     object == "�������" || object == "��-�" || object == "������" || object == "��" || object == "�" ||
     (object == "���" && name.SubString(name.Length() - 2, 2) != "��") ||
     (object == "���" && name.SubString(name.Length() - 2, 2) != "��") || object == "��" || object == "������" ||
     object == "�" || object == "�/�" || object == "�/�" || object == "�/��" || object == "������" ||
     object == "�������" || object == "��������" || object == "���" || object == "" || object == "�" ||
     object == "��" || object == "���" || object == "��" || object == "���" || object == "�" || object == "��" ||
     object == "" || (object == "�" && name.SubString(name.Length() - 2, 2) != "��"))
     {
      if (expandSocrs)
       result = (*socr)[object] + " " + name;
      else
       result = object + " " + name;
     }
    else
     {
      if (expandSocrs)
       result = name + " " + (*socr)[object];
      else
       result = name + " " + object;
     }
   }
  return result;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsKLADRDM::getFlatTypeText(TFlatObjectType AFlatType)
 {
  UnicodeString RC = "";
  try
   {
    for (StringIntMap::iterator i = FFlatTypeSocrObj.begin(); i != FFlatTypeSocrObj.end(); i++)
     {
      if (i->second == (int)AFlatType)
       {
        RC = i->first;
        break;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsKLADRDM::getStreetTypeText(TStreetObjectType AStreetObjType)
 {
  UnicodeString RC = " ";
  try
   {
    for (StringIntMap::iterator i = FStreetTypeSocrObj.begin(); i != FStreetTypeSocrObj.end(); i++)
     {
      if (i->second == (int)(AStreetObjType))
       {
        RC = i->first;
        break;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsKLADRDM::PrepareStreetObjectVal(UnicodeString AVal)
 {
  UnicodeString RC = AVal;
  try
   {
    for (int i = 1; i <= RC.Length(); i++)
     {
      if (RC[i] == '0')
       RC[i] = ' ';
      else
       i = RC.Length() + 1;
     }
   }
  __finally
   {
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsKLADRDM::getIndeks(adresCode adres)
 {
  UnicodeString RC = "";
  TFDQuery * FQ = CreateTempQuery();
  UnicodeString FSQL;
  try
   {
    try
     {
      if (adres.RegID || adres.Town1ID || adres.Town2ID || adres.Town3ID)
       {
        if (adres.StreetID > 0)
         { // ����� ������ �� ������� �����_5
          FSQL = "SELECT INDEKS FROM KLADR5 WHERE ID1 = :L1 AND ID2 = :L2 AND ID3 = :L3 AND ID4 = :L4 AND ID5 = :L5";
          quExecParam(FQ, false, FSQL, "L1", adres.RegID, "������ �� ������� �����_5", "L2", adres.Town1ID, "L3",
           adres.Town2ID, "L4", adres.Town3ID, "L5", adres.StreetID);
         }
        else
         { // ����� ������ �� ������� �����_14
          FSQL = "SELECT INDEKS FROM KLADR14 WHERE ID1 = :L1 AND ID2 = :L2 AND ID3 = :L3 AND ID4 = :L4";
          quExecParam(FQ, false, FSQL, "L1", adres.RegID, "������ �� ������� �����_14", "L2", adres.Town1ID, "L3",
           adres.Town2ID, "L4", adres.Town3ID);
         }
        RC = FQ->RecordCount ? FQ->FieldByName("INDEKS")->AsString : UnicodeString("");
       }
     }
    catch (...)
     {
      throw EKLADRError("������ ��������� �������");
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsKLADRDM::IsIndex(UnicodeString AAddr)
 {
  bool RC = false;
  try
   {
    if (AAddr.Length() == 6)
     {
      RC = true;
      for (int i = 1; (i <= 6) && RC; i++)
       RC &= ((AAddr[i] >= '0') && (AAddr[i] <= '9'));
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsKLADRDM::AddUnique(TStringList * ADest, TStringList * ASrc)
 {
  for (int i = 0; i < ASrc->Count; i++)
   {
    if (ADest->IndexOf(ASrc->Strings[i]) == -1)
     ADest->Add(ASrc->Strings[i]);
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsKLADRDM::textToCodeList(UnicodeString ADefAddr, UnicodeString AAddr, TStringList * ARetCodes,
 bool ABeginFrom, bool ASilent, bool AFullSearch)
 {
  //dsLogBegin(__FUNC__);
  bool RC = false;
  bool FOverMaxRC = false;

  TStringList * FAddrList = new TStringList;
  // dsLogMessage("5", "", 1);
  try
   {
    UnicodeString FCurItem = "";
    // dsLogMessage("FAddrNF->Count", IntToStr(FAddrNF->Count), 1);
    TNFAddrItems * FAddrNF = new TNFAddrItems(AAddr, ',');
    try
     {
      if (FAddrNF->Count)
       {
        int FFirstIdx = FAddrNF->GetFirstItemIndex();
        FCurItem = FAddrNF->Item[FFirstIdx]->PreparedText;
        if (IsIndex(FCurItem) && (FAddrNF->Count > 1))
         FFirstIdx++ ;

        RC = FtextToCodeList("", FAddrNF, FFirstIdx, psStart, ABeginFrom, FAddrList, FOverMaxRC, AFullSearch);
        AddUnique(ARetCodes, FAddrList);
        RC &= FtextToCodeList("", FAddrNF, FFirstIdx, psReg, ABeginFrom, FAddrList, FOverMaxRC, AFullSearch);
        AddUnique(ARetCodes, FAddrList);
        // if (FAddrList->Count)
        // ARetCodes->Text = ARetCodes->Text.Trim() + "\n" + FAddrList->Text.Trim();
       }
     }
    __finally
     {
      delete FAddrNF;
     }
    if (ADefAddr.Length())
     {
      UnicodeString FAddr = AAddr;
      if (!FAddr.UpperCase().Pos(ADefAddr.UpperCase()))
       {
        FAddr   = ADefAddr + ", " + FAddr;
        FAddrNF = new TNFAddrItems(FAddr, ',');
        try
         {
          if (FAddrNF->Count)
           {
            int FFirstIdx = FAddrNF->GetFirstItemIndex();
            FCurItem = FAddrNF->Item[FFirstIdx]->PreparedText;
            if (IsIndex(FCurItem) && (FAddrNF->Count > 1))
             FFirstIdx++ ;
            bool FOverMaxRC2 = false;
            RC |= FtextToCodeList("", FAddrNF, FFirstIdx, psStart, ABeginFrom, FAddrList, FOverMaxRC2, AFullSearch);
            AddUnique(ARetCodes, FAddrList);
            if (!(FOverMaxRC && FOverMaxRC2))
             FOverMaxRC = false;
            // if (FAddrList->Count)
            // ARetCodes->Text = ARetCodes->Text.Trim() + "\n" + FAddrList->Text.Trim();
           }
         }
        __finally
         {
          delete FAddrNF;
         }
       }
     }

    if (RC)
     {
      // dsLogMessage("----------------------------------------------------------", "", 1);
      // dsLogMessage(ARetCodes->Text, "", 1);
      // dsLogMessage("----------------------------------------------------------", "", 1);

      // ���������� ������������ ������� ���������, ������->���. �����->�����
      int maxLevel = 0;
      int curLevel;
      adresCode fAddrCode;
      for (int i = 0; (i < ARetCodes->Count) && (maxLevel != 2); i++)
       {
        if (!ARetCodes->Strings[i].Trim().Length())
         {
          ARetCodes->Delete(i);
          i-- ;
         }
        else
         {
          curLevel  = 0; // �� ��������� ���� ������
          fAddrCode = ParseAddrStr(ARetCodes->Strings[i]);
          if (fAddrCode.StreetID)
           curLevel = 2; // ���� ��� �����
          else if (fAddrCode.Town1ID + fAddrCode.Town2ID + fAddrCode.Town3ID)
           curLevel = 1; // ���� ��� ���. �����
          if (curLevel > maxLevel)
           maxLevel = curLevel;
         }
       }
      // ������� ������ ������� ����������� ������� ������ �������������
      for (int i = 0; i < ARetCodes->Count; i++)
       {
        fAddrCode = ParseAddrStr(ARetCodes->Strings[i]);
        curLevel  = 0; // �� ��������� ���� ������
        if (fAddrCode.StreetID)
         curLevel = 2; // ���� ��� �����
        else if (fAddrCode.Town1ID + fAddrCode.Town2ID + fAddrCode.Town3ID)
         curLevel = 1; // ���� ��� ���. �����
        if (curLevel < maxLevel)
         {
          ARetCodes->Delete(i);
          i-- ;
         }
       }
      if (FOverMaxRC)
       {
        RC = false;
        ARetCodes->Clear();
        if (!ASilent)
         ARetCodes->Add("���������� ��������� ����� " + IntToStr(MAX_SEARCH_COUNT) + ", �������� ������� ������.");
       }
      // dsLogMessage(">>>>>>>>>>>>>>>>>>>>>>>>>--------------------------------------", "", 1);
      // dsLogMessage(ARetCodes->Text, "", 1);
      // dsLogMessage(">>>>>>>>>>>>>>>>>>>>>>>>>--------------------------------------", "", 1);
     }
    else if (FOverMaxRC)
     {
      RC = false;
      ARetCodes->Clear();
      if (!ASilent)
       ARetCodes->Add("���������� ��������� ����� " + IntToStr(MAX_SEARCH_COUNT) + ", �������� ������� ������.");
     }
   }
  __finally
   {
    delete FAddrList;
   }
  //dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsKLADRDM::IndexToCodeList(UnicodeString AName, TStringList * ARetCodes)
 {
  bool RC = false;
  UnicodeString FName = AName.Trim();
  if (FName.Length() > 40)
   FName.SetLength(40);
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      adresCode adres;
      quExecParam(FQ, false, "SELECT ID1, ID2, ID3, ID4 FROM KLADR14 WHERE INDEKS = :pNAME", "pNAME", FName.UpperCase(),
       "�� �� �������");
      if (FQ->RecordCount)
       {
        while (!FQ->Eof)
         {
          adres.Clear();
          adres.RegID   = FQ->FieldByName("ID1")->AsInteger;
          adres.Town1ID = FQ->FieldByName("ID2")->AsInteger;
          adres.Town2ID = FQ->FieldByName("ID3")->AsInteger;
          adres.Town3ID = FQ->FieldByName("ID4")->AsInteger;
          AddUniqueItem(ARetCodes, adres.AsString());
          FQ->Next();
         }
        RC = true;
       }
      quExecParam(FQ, false, "SELECT ID1, ID2, ID3, ID4, ID5 FROM KLADR5 WHERE INDEKS = :pNAME", "pNAME",
       FName.UpperCase(), "����� �� �������");
      if (FQ->RecordCount)
       {
        while (!FQ->Eof)
         {
          adres.Clear();
          adres.RegID    = FQ->FieldByName("ID1")->AsInteger;
          adres.Town1ID  = FQ->FieldByName("ID2")->AsInteger;
          adres.Town2ID  = FQ->FieldByName("ID3")->AsInteger;
          adres.Town3ID  = FQ->FieldByName("ID4")->AsInteger;
          adres.StreetID = FQ->FieldByName("ID5")->AsInteger;
          AddUniqueItem(ARetCodes, adres.AsString());
          FQ->Next();
         }
        RC = true;
       }
     }
    catch (Exception & E)
     {
      throw ESearchException("������ ������.\n��������� ���������:\n" + E.Message);
     }
    catch (...)
     {
      throw ESearchException("Searching error");
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsKLADRDM::NPTextToCodeList(UnicodeString AAdresStr, TNFAddrWords * AName, TStringList * ARetCodes,
 bool ABeginFrom)
 {
  //dsLogBegin(__FUNC__);
  bool RC = false;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    UnicodeString FSQL;
    try
     {
      adresCode FNPAddr = ParseAddrStr(AAdresStr);
      UnicodeString FName = AName->PreparedText;
      bool FCanLike = false;
      for (int i = 1; i <= FName.Length(); i++)
       {
        if (FName[i] == ' ')
         {
          FCanLike = true;
          FName[i] = '%';
         }
       }
      if (FName.Trim().Length() > 40)
       FName = FName.Trim().SetLength(40);
      if (FCanLike)
       {
        FSQL = "SELECT ID1, ID2, ID3, ID4 FROM KLADR14 WHERE SNAME LIKE :pNAME";
        if (!ABeginFrom)
         FName = "%" + FName;
       }
      else
       {
        if (ABeginFrom)
         FSQL = "SELECT ID1, ID2, ID3, ID4 FROM KLADR14 WHERE SNAME STARTING WITH :pNAME";
        else
         FSQL = "SELECT ID1, ID2, ID3, ID4 FROM KLADR14 WHERE SNAME = :pNAME";
       }
      if (FNPAddr.RegID)
       FSQL += " AND ID1 = :L1";
      if (FNPAddr.Town1ID)
       FSQL += " AND ID2 = :L2";
      if (FNPAddr.Town2ID)
       FSQL += " AND ID3 = :L3";
      if (FNPAddr.Town3ID)
       FSQL += " AND ID4 = :L4";
      // FSQL += " and ((ID2<>0 AND not (ID3 = 0 AND ID4 = 0)) or (ID2=0))";  //��� ����� �������
      quExecParam(FQ, false, FSQL, "pNAME", FName.UpperCase(), "����� �� (1)", (FNPAddr.RegID) ? "L1" : "",
       FNPAddr.RegID, (FNPAddr.Town1ID) ? "L2" : "", FNPAddr.Town1ID, (FNPAddr.Town2ID) ? "L3" : "", FNPAddr.Town2ID,
       (FNPAddr.Town3ID) ? "L4" : "", FNPAddr.Town3ID);
      if (FQ->RecordCount)
       {
        while (!FQ->Eof)
         {
          FNPAddr.Clear();
          if (FQ->FieldByName("ID2")->AsInteger + FQ->FieldByName("ID3")->AsInteger + FQ->FieldByName("ID4")->AsInteger)
           {
            RC              = true;
            FNPAddr.RegID   = FQ->FieldByName("ID1")->AsInteger;
            FNPAddr.Town1ID = FQ->FieldByName("ID2")->AsInteger;
            FNPAddr.Town2ID = FQ->FieldByName("ID3")->AsInteger;
            FNPAddr.Town3ID = FQ->FieldByName("ID4")->AsInteger;
            if (FNPAddr.Town3ID)
             FNPAddr.State = 1;
            AddUniqueItem(ARetCodes, FNPAddr.AsString());
           }
          FQ->Next();
         }
       }
      else
       {
        if (FName.Trim().Length() > 40)
         FName = FName.Trim().SetLength(40);
        if (ABeginFrom)
         FSQL = "SELECT ID1, ID2, ID3, ID4 FROM KLADR14 WHERE SNAME STARTING WITH :pNAME";
        else
         {
          FSQL  = "SELECT ID1, ID2, ID3, ID4 FROM KLADR14 WHERE SNAME LIKE :pNAME";
          FName = "%" + FName + "%";
         }
        if (FNPAddr.RegID)
         FSQL += " AND ID1 = :L1";
        if (FNPAddr.Town1ID)
         FSQL += " AND ID2 = :L2";
        if (FNPAddr.Town2ID)
         FSQL += " AND ID3 = :L3";
        if (FNPAddr.Town3ID)
         FSQL += " AND ID4 = :L4";
        // FSQL += "  and ((ID2<>0 AND not (ID3 = 0 AND ID4 = 0)) or (ID2=0))";   //��� ����� �������
        quExecParam(FQ, false, FSQL, "pNAME", FName.UpperCase(), "����� �� (2)", (FNPAddr.RegID) ? "L1" : "",
         FNPAddr.RegID, (FNPAddr.Town1ID) ? "L2" : "", FNPAddr.Town1ID, (FNPAddr.Town2ID) ? "L3" : "", FNPAddr.Town2ID,
         (FNPAddr.Town3ID) ? "L4" : "", FNPAddr.Town3ID);
        if (FQ->RecordCount)
         {
          while (!FQ->Eof)
           {
            if (FQ->FieldByName("ID2")->AsInteger + FQ->FieldByName("ID3")->AsInteger + FQ->FieldByName("ID4")
             ->AsInteger)
             {
              FNPAddr.Clear();
              FNPAddr.RegID   = FQ->FieldByName("ID1")->AsInteger;
              FNPAddr.Town1ID = FQ->FieldByName("ID2")->AsInteger;
              FNPAddr.Town2ID = FQ->FieldByName("ID3")->AsInteger;
              FNPAddr.Town3ID = FQ->FieldByName("ID4")->AsInteger;
              if (FNPAddr.Town3ID)
               FNPAddr.State = 1;
              AddUniqueItem(ARetCodes, FNPAddr.AsString());
              RC = true;
             }
            FQ->Next();
           }
         }
       }
     }
    catch (Exception & E)
     {
      throw ESearchException(E.Message);
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  //dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsKLADRDM::FtextToCodeList(UnicodeString ACurAddrCode, TNFAddrItems * ASrcList, int ACurIdx,
 TAddrParseType ASearchState, bool ABeginFrom, TStringList * ARetCodes, bool & AOverMaxRC, bool AFullSearch)
 {
//  //dsLogBegin(__FUNC__);
  bool RC = false;
  // bool AOverMaxRC = false;
  TStringList * FAddrList = new TStringList;
  TStringList * FCurAddrList = new TStringList;
  ARetCodes->Clear();
  try
   {
    if (!AOverMaxRC)
     {
      UnicodeString FCurItem, FStrAddrCode;
      FCurItem = FStrAddrCode = "";
      adresCode tmpAddrCode;
      if (ACurIdx < ASrcList->Count)
       {
        FCurItem = ASrcList->Item[ACurIdx]->PreparedText;
        FCurAddrList->Clear();
        // bool FStreetSearch = false;
        // bool FHouseSearch = false;
        // bool FFlatSearch = false;
        TAddrParseType FNextSearchState = psEnd;
        TAddrParseType FCurSearchState = ASearchState;
        int FNextIdx = ACurIdx;

        // if (ASearchState == psStart)
        // {
        // FCurSearchState = psNP1;
        // dsLogMessage("psStart - psNP1", "", 1);
        // }

        if (FCurSearchState == psReg)
         {
          if (FCurItem.Length() && RegTextToCodeList(FCurItem, FCurAddrList, true))
           {
            FNextIdx++ ;
            RC = true;
           }
          FNextSearchState = psNP1;
          dsLogMessage("psReg: psReg -> psNP1", "", 1);
         }
        else if ((FCurSearchState == psNP1) || (FCurSearchState == psStart))
         { // ������(�) ������(�)
          /*
           if (FCurItem.Length() && RegTextToCodeList(FCurItem, FCurAddrList, true))
           {
           FNextIdx++ ;
           RC               = true;
           FNextSearchState = psNP1;
           dsLogMessage("psNP1: psReg - psNP1", "", 1);
           }
           else
           {
           if (FCurItem.Length() && NPTextToCodeList(ACurAddrCode, ASrcList->Item[ACurIdx], FCurAddrList, true))
           {
           FNextIdx++ ;
           RC = true;
           }
           FNextSearchState = psNP2;
           dsLogMessage("psNP1: psNP1 - psNP2", "", 1);
           }
           */
          if (FCurItem.Length() && NPTextToCodeList(ACurAddrCode, ASrcList->Item[ACurIdx], FCurAddrList, true))
           {
            FNextIdx++ ;
            RC               = true;
            FNextSearchState = psNP2;
            dsLogMessage("psNP1: psNP1 - psNP2", "", 1);
           }
          else if ((FCurSearchState == psStart) && FCurItem.Length() && RegTextToCodeList(FCurItem, FCurAddrList, true))
           {
            FNextIdx++ ;
            RC               = true;
            FNextSearchState = psNP1;
            dsLogMessage("psNP1: psReg - psNP1", "", 1);
           }
          else
           {
            FNextSearchState = psNP2;
            dsLogMessage("psNP1: psNP1 - psNP2", "", 1);
           }
          /*
           if (FCurItem.Length() && NPTextToCodeList(ACurAddrCode, ASrcList->Item[ACurIdx], FCurAddrList, true))
           {
           FNextIdx++ ;
           RC = true;
           }
           FNextSearchState = psNP2;
           dsLogMessage("psNP1: psNP1 - psNP2", "", 1);
           */
         }
        else if (FCurSearchState == psNP2)
         { // ����� ���. ������ 2-� �������
          if (FCurItem.Length() && NPTextToCodeList(ACurAddrCode, ASrcList->Item[ACurIdx], FCurAddrList, true))
           {
            FNextIdx++ ;
            RC = true;
           }
          FNextSearchState = psNP3;
          dsLogMessage("psNP2: psNP2 - psNP3", "", 1);
         }
        else if (FCurSearchState == psNP3)
         { // ����� ���. ������ 3-� �������
          if (FCurItem.Length() && NPTextToCodeList(ACurAddrCode, ASrcList->Item[ACurIdx], FCurAddrList, true))
           {
            FNextIdx++ ;
            RC = true;
           }
          FNextSearchState = psStreet;
          dsLogMessage("psNP3: psNP3 - psStreet", "", 1);
         }
        else if (FCurSearchState == psStreet)
         { // ����� �����
          if (FCurItem.Length() && StreetTextToCodeList(ACurAddrCode, ASrcList->Item[ACurIdx], FCurAddrList, true,
           AFullSearch))
           {
            FNextIdx++ ;
            RC = true;
           }
          FNextSearchState = psHouse;
          dsLogMessage("psStreet: psStreet - psHouse", "", 1);
         }
        else if (FCurSearchState == psHouse)
         { // ����� ����
          if (ACurIdx && FCurItem.Length() && HouseTextToCodeList(ACurAddrCode, ASrcList->Item[ACurIdx], FCurAddrList))
           {
            FNextIdx++ ;
            RC = true;
           }
          FNextSearchState = psFlat;
          dsLogMessage("psHouse: psHouse - psFlat", "", 1);
         }
        else if (FCurSearchState == psFlat)
         { // ����� ��������
          if (ACurIdx && FCurItem.Length() && FlatTextToCodeList(ACurAddrCode, ASrcList->Item[ACurIdx], FCurAddrList))
           {
            FNextIdx++ ;
            RC = true;
           }
          FNextSearchState = psEnd;
          dsLogMessage("psFlat: psFlat - psEnd", "", 1);
         }
        if (FCurSearchState != psEnd)
         {
          dsLogMessage("����", "", 1);
          AOverMaxRC = (FCurAddrList->Count > MAX_SEARCH_COUNT);
          if (FCurAddrList->Count)
           {
            for (int i = 0; (i < FCurAddrList->Count) && !AOverMaxRC; i++)
             {
              RC |= FtextToCodeList(FCurAddrList->Strings[i], ASrcList, FNextIdx, FNextSearchState, ABeginFrom,
               FAddrList, AOverMaxRC);
              if (FAddrList->Count)
               AddUnique(ARetCodes, FAddrList);
              else
               AddUniqueItem(ARetCodes, FCurAddrList->Strings[i]);
             }
           }
          else
           {
            RC = FtextToCodeList(ACurAddrCode, ASrcList, FNextIdx, FNextSearchState, ABeginFrom, FAddrList, AOverMaxRC);
            if (FAddrList->Count)
             AddUnique(ARetCodes, FAddrList);
            else
             AddUniqueItem(ARetCodes, ACurAddrCode);
           }
         }
       }
     }
   }
  __finally
   {
    delete FAddrList;
    delete FCurAddrList;
   }
 // //dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsKLADRDM::RegTextToCodeList(UnicodeString AName, TStringList * ARetCodes, bool ABeginFrom)
 {
  //dsLogBegin(__FUNC__);
  bool RC = false;
  UnicodeString FName = AName.Trim();
  if (FName.Length() > 40)
   FName.SetLength(40);
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      adresCode adres = ParseAddrStr("");
      UnicodeString FSQL;
      if (ABeginFrom)
       FSQL = "SELECT ID1 FROM KLADR14 WHERE SNAME STARTING WITH :pNAME AND ID2 = 0 AND ID3 = 0 AND ID4 = 0";
      else
       {
        FSQL  = "SELECT ID1 FROM KLADR14 WHERE SNAME LIKE :pNAME AND ID2 = 0 AND ID3 = 0 AND ID4 = 0";
        FName = "%" + FName + "%";
       }
      quExecParam(FQ, false, FSQL, "pNAME", FName.UpperCase(), "����� ������� (1)");
      if (FQ->RecordCount)
       {
        while (!FQ->Eof)
         {
          adres.Clear();
          adres.RegID = FQ->FieldByName("ID1")->AsInteger;
          AddUniqueItem(ARetCodes, adres.AsString());
          FQ->Next();
         }
        RC = true;
       }
     }
    catch (Exception & E)
     {
      throw ESearchException("������ ������.\n��������� ���������:\n" + E.Message);
     }
    catch (...)
     {
      throw ESearchException("Searching error");
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  //dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsKLADRDM::AddUniqueItem(TStringList * ADest, UnicodeString ASrc)
 {
  dsLogMessage(ASrc, __FUNC__, 1);
  if (ADest->IndexOf(ASrc) == -1)
   ADest->Add(ASrc);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsKLADRDM::StreetTextToCodeList(UnicodeString AAdresStr, TNFAddrWords * AName, TStringList * ARetCodes,
 bool ABeginFrom, bool AFullSearch)
 {
  //dsLogBegin(__FUNC__);
  bool RC = false;
  UnicodeString FName, FSQL;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      adresCode FStreetAddr = ParseAddrStr(AAdresStr);
      if (AName->Count == 2)
       {
        if (StrInList("�\�,�/�,�\�.,�/�.", AName->Word[0].UpperCase()))
         { // ������ � �/�
          FStreetAddr.StrObjType = sotArmyUnit;
          // FStreetAddr.NoStreets = true;
          FStreetAddr.StreetObjectVal = AName->Word[1];
          FStreetAddr.State           = 1;
          AddUniqueItem(ARetCodes, FStreetAddr.AsString());
          RC = true;
         }
        else if (StrInList("�\�,�/�,�\�.,�/�.", AName->Word[0].UpperCase()))
         { // ������ �/�
          FStreetAddr.StrObjType = sotOfficeBox;
          // FStreetAddr.NoStreets = true;
          FStreetAddr.StreetObjectVal = AName->Word[1];
          FStreetAddr.State           = 1;
          AddUniqueItem(ARetCodes, FStreetAddr.AsString());
          RC = true;
         }
        else if (StrInList("���,���.", AName->Word[0].UpperCase()))
         { // ������ � ���
          FStreetAddr.StrObjType = sotPostOffice;
          // FStreetAddr.NoStreets = true;
          FStreetAddr.StreetObjectVal = AName->Word[1];
          FStreetAddr.State           = 1;
          AddUniqueItem(ARetCodes, FStreetAddr.AsString());
          RC = true;
         }
       }
      if (!RC)
       {
        FName = AName->PreparedText.Trim();
        if (FName.Length() > 40)
         FName.SetLength(40);
        if (ABeginFrom)
         FSQL = "SELECT ID1, ID2, ID3, ID4, ID5 FROM KLADR5 WHERE SNAME STARTING WITH :pNAME AND ID1 = :L1 AND ID2 = :L2 AND ID3 = :L3 AND ID4 = :L4";
        else
         {
          FSQL = "SELECT ID1, ID2, ID3, ID4, ID5 FROM KLADR5 WHERE SNAME LIKE :pNAME AND ID1 = :L1 AND ID2 = :L2 AND ID3 = :L3 AND ID4 = :L4";
          FName = "%" + FName + "%";
         }
        quExecParam(FQ, false, FSQL, "pNAME", FName.UpperCase(), "����� �����(1)", "L1", FStreetAddr.RegID, "L2",
         FStreetAddr.Town1ID, "L3", FStreetAddr.Town2ID, "L4", FStreetAddr.Town3ID);
        if (FQ->RecordCount)
         {
          while (!FQ->Eof)
           {
            FStreetAddr.Clear();
            FStreetAddr.RegID      = FQ->FieldByName("ID1")->AsInteger;
            FStreetAddr.Town1ID    = FQ->FieldByName("ID2")->AsInteger;
            FStreetAddr.Town2ID    = FQ->FieldByName("ID3")->AsInteger;
            FStreetAddr.Town3ID    = FQ->FieldByName("ID4")->AsInteger;
            FStreetAddr.StreetID   = FQ->FieldByName("ID5")->AsInteger;
            FStreetAddr.StrObjType = sotStreet;
            FStreetAddr.State      = 1;
            AddUniqueItem(ARetCodes, FStreetAddr.AsString());
            FQ->Next();
           }
          RC = true;
         }
        if (AFullSearch)
         {
          // ����� %.Name%
          FName = AName->PreparedText.Trim();
          if (FName.Length() > 40)
           FName.SetLength(40);
          FName = "%." + FName + "%";
          FSQL  = "SELECT ID1, ID2, ID3, ID4, ID5 FROM KLADR5 " "WHERE SNAME LIKE :pNAME AND ID1 = :L1 AND ID2 = :L2 "
           "AND ID3 = :L3 AND ID4 = :L4";
          quExecParam(FQ, false, FSQL, "pNAME", FName.UpperCase(), "����� �����(2)", "L1", FStreetAddr.RegID, "L2",
           FStreetAddr.Town1ID, "L3", FStreetAddr.Town2ID, "L4", FStreetAddr.Town3ID);
          if (FQ->RecordCount)
           {
            while (!FQ->Eof)
             {
              FStreetAddr.Clear();
              FStreetAddr.RegID      = FQ->FieldByName("ID1")->AsInteger;
              FStreetAddr.Town1ID    = FQ->FieldByName("ID2")->AsInteger;
              FStreetAddr.Town2ID    = FQ->FieldByName("ID3")->AsInteger;
              FStreetAddr.Town3ID    = FQ->FieldByName("ID4")->AsInteger;
              FStreetAddr.StreetID   = FQ->FieldByName("ID5")->AsInteger;
              FStreetAddr.StrObjType = sotStreet;
              FStreetAddr.State      = 1;
              AddUniqueItem(ARetCodes, FStreetAddr.AsString());
              FQ->Next();
             }
            RC |= true;
           }
          // ����� % Name%
          FName = AName->PreparedText.Trim();
          if (FName.Length() > 40)
           FName.SetLength(40);
          FName = "% " + FName + "%";
          FSQL  = "SELECT ID1, ID2, ID3, ID4, ID5 FROM KLADR5 " "WHERE SNAME LIKE :pNAME AND ID1 = :L1 AND ID2 = :L2 "
           "AND ID3 = :L3 AND ID4 = :L4";
          // FName = AName->PreparedText.Trim();
          // if (FName.Length() > 40)
          // FName.SetLength(40);
          quExecParam(FQ, false, FSQL, "pNAME", FName.UpperCase(), "����� �����(3)", "L1", FStreetAddr.RegID, "L2",
           FStreetAddr.Town1ID, "L3", FStreetAddr.Town2ID, "L4", FStreetAddr.Town3ID);
          if (FQ->RecordCount)
           {
            while (!FQ->Eof)
             {
              FStreetAddr.Clear();
              FStreetAddr.RegID      = FQ->FieldByName("ID1")->AsInteger;
              FStreetAddr.Town1ID    = FQ->FieldByName("ID2")->AsInteger;
              FStreetAddr.Town2ID    = FQ->FieldByName("ID3")->AsInteger;
              FStreetAddr.Town3ID    = FQ->FieldByName("ID4")->AsInteger;
              FStreetAddr.StreetID   = FQ->FieldByName("ID5")->AsInteger;
              FStreetAddr.StrObjType = sotStreet;
              FStreetAddr.State      = 1;
              AddUniqueItem(ARetCodes, FStreetAddr.AsString());
              FQ->Next();
             }
            RC |= true;
           }
         }
       }
     }
    catch (Exception & E)
     {
      throw ESearchException("������ ������.\n��������� ���������:\n" + E.Message);
     }
    catch (...)
     {
      throw ESearchException("Searching error");
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  //dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsKLADRDM::HouseTextToCodeList(UnicodeString AAdresStr, TNFAddrWords * AName, TStringList * ARetCodes)
 {
  //dsLogBegin(__FUNC__);
  bool RC = false;
  try
   {
    try
     {
      // dsLogMessage("--------------------------------------", "-----------------------------", 1);
      // dsLogMessage(AAdresStr, "", 1);
      adresCode FHomeAddr = ParseAddrStr(AAdresStr);
      // dsLogMessage(FHomeAddr.AsString(), "", 1);
      ParseHouseStruct(AName->Text, FHomeAddr.House, FHomeAddr.Vld, FHomeAddr.Korp, FHomeAddr.Build);
      // dsLogMessage(AName->Text, FHomeAddr.House, 1);
      AddUniqueItem(ARetCodes, FHomeAddr.AsString());
      // dsLogMessage("--------------------------------------", "-----------------------------", 1);
      RC = true;
     }
    catch (Exception & E)
     {
      throw ESearchException("������ ������.\n��������� ���������:\n" + E.Message);
     }
    catch (...)
     {
      throw ESearchException("Searching error");
     }
   }
  __finally
   {
   }
  //dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsKLADRDM::FlatTextToCodeList(UnicodeString AAdresStr, TNFAddrWords * AName, TStringList * ARetCodes)
 {
  //dsLogBegin(__FUNC__);
  bool RC = false;
  try
   {
    try
     {
      adresCode FFlatAddr = ParseAddrStr(AAdresStr);
      FFlatAddr.FlatObjType = fotFlat;
      FFlatAddr.Flat        = AName->PreparedText.Trim();
      AddUniqueItem(ARetCodes, FFlatAddr.AsString());
      RC = true;
     }
    catch (Exception & E)
     {
      throw ESearchException("������ ������.\n��������� ���������:\n" + E.Message);
     }
    catch (...)
     {
      throw ESearchException("Searching error");
     }
   }
  __finally
   {
   }
  //dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TdsKLADRDM::FGetNewNFCode()
 {
  __int64 RC = 0;
  TFDQuery * FQ = CreateNFTempQuery();
  try
   {
    try
     {
      quExec(FQ, false, "SELECT Max(UID) as MCODE FROM KLADRNF", "Get NF ID");
      if (FQ->RecordCount)
       RC = FQ->FieldByName("MCODE")->AsInteger + 1;
     }
    catch (Exception & E)
     {
      throw EKLADRError("������ ����������� ������������� ���� ������.\n�������:\n" + E.Message);
     }
    catch (...)
     {
      throw EKLADRError("������ ����������� ������������� ���� ������.");
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TdsKLADRDM::FSaveNFAddr(UnicodeString ANFAddr, adresCode AAdres)
 {
  __int64 RC = AAdres.NFAddrID;
  TFDQuery * FQ = CreateNFTempQuery();
  try
   {
    try
     {
      __int64 FNewNFAddrCode = FGetNewNFCode();
      if (AAdres.NFAddrID)
       {
        quExec(FQ, false, "SELECT Count(*) as RCOUNT FROM KLADRNF where uid=" + IntToStr(AAdres.NFAddrID),
         "Get InsUp code");
        if (FQ->FieldByName("RCOUNT")->AsInteger)
         FNewNFAddrCode = AAdres.NFAddrID;
       }
      if (!quExecParam(FQ, true,
       "UPDATE OR INSERT INTO KLADRNF (UID, ID1, ID2, ID3,  ID4, NAME) values (:pUID, :pID1, :pID2, :pID3,  :pID4, :pNAME)  MATCHING (UID)",
       "pUID", FNewNFAddrCode, "���������� �������������������� ������.", "pID1", AAdres.RegID, "pID2", AAdres.Town1ID,
       "pID3", AAdres.Town2ID, "pID4", AAdres.Town3ID, "pNAME", ANFAddr))
       RC = 0;
      else
       RC = FNewNFAddrCode;
     }
    catch (Exception & e)
     {
      throw EKLADRError("������ ���������� ������.\n�������:\n" + e.Message);
     }
    catch (...)
     {
      throw EKLADRError("������ ���������� ������.");
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
