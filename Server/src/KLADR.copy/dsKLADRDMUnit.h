//---------------------------------------------------------------------------

#ifndef dsKLADRDMUnitH
#define dsKLADRDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>

#include "XMLContainer.h"
//#include "dsDocXMLUtils.h"
#include "icsLog.h"
//#include "dsDocSrvTypeDef.h"

#include "KLADRParsers.h"

#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
//---------------------------------------------------------------------------
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.VCLUI.Wait.hpp>
#include <System.JSON.hpp>
#include "kabQueryUtils.h"
//#include "dsSrvClassifUnit.h"
//---------------------------------------------------------------------------
class ESearchException : public Exception
{
    public:
    __fastcall ESearchException (const UnicodeString msg) : Exception (msg) { };
};
//---------------------------------------------------------------------------
class TdsKLADRDM : public TDataModule
{
__published:	// IDE-managed Components
private:	// User declarations
  TFDConnection *FNFAddrConnection;

  typedef map <UnicodeString, CTTCacheRecord*> CTTCacheMap;
  TFDConnection *FConnection;
  UnicodeString FDefaultAddr;
  CTTCacheMap*   CTTCache; // ����� ��� ����
  bool           FUseCTTCache;     // ���� ������������� ���� ��� ������� CodeToText, CodeToIndeks
  bool           expandSocrs;

  StrStrMap*         socr; // ����� ���������� (���� = ���������� � �.�.)
  StringIntMap    FStreetTypeObj;
  StringIntMap    FStreetTypeSocrObj;
  StringIntMap    FFlatTypeObj;
  StringIntMap    FFlatTypeSocrObj;

  TFDQuery*     __fastcall FCreateTempQuery(TFDConnection *AConnection);
  bool          __fastcall textToCodeList(UnicodeString ADefAddr, UnicodeString AAddr, TStringList *ARetCodes, bool ABeginFrom = false, bool ASilent=false, bool AFullSearch = true);
  bool          __fastcall CacheEntryExists(UnicodeString code);
  int           __fastcall FGetLastLevelIdx(int ALvl1, int ALvl2, int ALvl3, int ALvl4);
  UnicodeString __fastcall getFullName (UnicodeString name, UnicodeString object, bool ANotSocr);
  UnicodeString __fastcall getFlatTypeText(TFlatObjectType AFlatType);
  UnicodeString __fastcall getStreetTypeText(TStreetObjectType AStreetObjType);
  UnicodeString __fastcall PrepareStreetObjectVal(UnicodeString AVal);
  UnicodeString __fastcall getIndeks(adresCode adres);
  bool          __fastcall IndexToCodeList(UnicodeString AName, TStringList *ARetCodes);
  bool          __fastcall NPTextToCodeList(UnicodeString AAdresStr, TNFAddrWords *AName, TStringList *ARetCodes, bool ABeginFrom = false);
  bool          __fastcall FtextToCodeList(UnicodeString ACurAddrCode, TNFAddrItems *ASrcList, int ACurIdx, TAddrParseType ASearchState, bool ABeginFrom, TStringList *ARetCodes, bool &AOverMaxRC, bool AFullSearch = true);
  bool          __fastcall RegTextToCodeList(UnicodeString AName, TStringList *ARetCodes, bool ABeginFrom = false);
  void          __fastcall AddUniqueItem(TStringList *ADest, UnicodeString ASrc);

  bool          __fastcall StreetTextToCodeList(UnicodeString AAdresStr, TNFAddrWords *AName, TStringList *ARetCodes, bool ABeginFrom = false, bool AFullSearch = false);
  bool          __fastcall HouseTextToCodeList(UnicodeString AAdresStr, TNFAddrWords *AName, TStringList *ARetCodes);
  bool          __fastcall FlatTextToCodeList(UnicodeString AAdresStr, TNFAddrWords *AName, TStringList *ARetCodes);

  __int64       __fastcall FGetNewNFCode();
  __int64       __fastcall FSaveNFAddr(UnicodeString ANFAddr, adresCode AAdres);
  TFDQuery*     __fastcall CreateNFTempQuery();
  UnicodeString __fastcall ReplaceNonAddr(UnicodeString AStr);
  bool          __fastcall IsIndex(UnicodeString AAddr);
  void          __fastcall AddUnique(TStringList * ADest, TStringList * ASrc);


public:		// User declarations
  __fastcall TdsKLADRDM(TComponent* Owner, TFDConnection *AKLADRConnection, TFDConnection * ANFAddrConnection);
  __fastcall ~TdsKLADRDM();

  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AParam1Name,      Variant AParam1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AParam2Name = "", Variant AParam2Val = 0,
                               UnicodeString AParam3Name = "", Variant AParam3Val = 0,
                               UnicodeString AParam4Name = "", Variant AParam4Val = 0,
                               UnicodeString AParam5Name = "", Variant AParam5Val = 0,
                               UnicodeString AParam6Name = "", Variant AParam6Val = 0);


  // AParams - 0x01 - ������������ ����������������� �����
  //           0x02 - ������������� ������� ��������
  //           0x04 - ������������� ������� ���� � ���� (�� ...)
  //           0x08 - ������������� ������� ����� � ���� (��...)
  //           0x10 - ������������� ������� ���. ������
  TJSONObject* __fastcall Find(UnicodeString AStr, UnicodeString ADefAddr = "", int AParams = 0x1F);

  // 0x80 - 1 (�������)
  // 0x40 - 2 (�.�.)
  // 0x20 - 3 (�.�.)
  // 0x10 - 4 (�.�.)
  // 0x08 - 5 (�����)
  // 0x04 - 6 (����)
  // 0x02 - 7 (��������)
  // 0x01 - �������� ������
  UnicodeString __fastcall codeToText(UnicodeString code, unsigned int AParams = 0x0FF/*, UnicodeString  params ="11111111"*/, bool ANotNFAddr = false, bool AReplaceCach = false);
  __property bool UseCache = {read=FUseCTTCache, write=FUseCTTCache};     // ���� ������������� ���� ��� ������� CodeToText, CodeToIndeks
  __property UnicodeString DefAddr = {read=FDefaultAddr, write=FDefaultAddr};

};
//---------------------------------------------------------------------------
extern PACKAGE TdsKLADRDM *dsKLADRDM;
//---------------------------------------------------------------------------
#endif
