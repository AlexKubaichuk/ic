// ---------------------------------------------------------------------------
// #include <vcl.h>
#pragma hdrstop
#include "kabQueryUtils.h"
#pragma package(smart_init)
// ---------------------------------------------------------------------------
namespace kabQuery
 {
 __fastcall TkabQueryHelper::TkabQueryHelper(TFDConnection * AConnection)
  {
   FConnection = AConnection;
   FLogger     = new TkabLogHelper;
  }
 // ---------------------------------------------------------------------------
 __fastcall TkabQueryHelper::~TkabQueryHelper()
  {
   delete FLogger;
  }
 // ---------------------------------------------------------------------------
 TFDQuery * __fastcall TkabQueryHelper::CreateTempQuery()
  {
   FLogger->LogBegin(__FUNC__);
   TFDQuery * FQ = new TFDQuery(NULL);
   FQ->Transaction = new TFDTransaction(NULL);
   try
    {
     FQ->Transaction->Connection          = FConnection;
     FQ->Connection                       = FConnection;
     FQ->Transaction->Options->AutoStart  = false;
     FQ->Transaction->Options->AutoStop   = false;
     FQ->Transaction->Options->AutoCommit = false;
     FQ->Transaction->Options->StopOptions.Clear();
    }
   __finally
    {
    }
   FLogger->LogEnd(__FUNC__);
   return FQ;
  }
 // ---------------------------------------------------------------------------
 void __fastcall TkabQueryHelper::DeleteTempQuery(TFDQuery * AQuery)
  {
   FLogger->LogBegin(__FUNC__);
   if (AQuery->Transaction->Active)
    AQuery->Transaction->Rollback();
   AQuery->Transaction->Connection = NULL;
   AQuery->Connection              = NULL;
   delete AQuery->Transaction;
   delete AQuery;
   FLogger->LogEnd(__FUNC__);
  }
 // ---------------------------------------------------------------------------
 bool __fastcall TkabQueryHelper::Exec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
  {
   FLogger->LogBegin(__FUNC__);
   bool RC = false;
   TTime FBeg = Time();
   try
    {
     try
      {
       AQuery->Close();
       if (!AQuery->Transaction->Active)
        AQuery->Transaction->StartTransaction();
       AQuery->SQL->Text = ASQL;
       AQuery->OpenOrExecute();
       if (ACommit)
        AQuery->Transaction->Commit();
       RC = true;
      }
     catch (System::Sysutils::Exception & E)
      {
       FLogger->LogError(E.Message + "; SQL = " + ASQL, __FUNC__);
      }
    }
   __finally
    {
     FLogger->LogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
    }
   FLogger->LogEnd(__FUNC__);
   return RC;
  }
 // ---------------------------------------------------------------------------
 bool __fastcall TkabQueryHelper::ExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
   UnicodeString AParam1Name, Variant AParam1Val, UnicodeString ALogComment, UnicodeString AParam2Name,
   Variant AParam2Val, UnicodeString AParam3Name, Variant AParam3Val, UnicodeString AParam4Name, Variant AParam4Val,
   UnicodeString AParam5Name, Variant AParam5Val, UnicodeString AParam6Name, Variant AParam6Val)
  {
   FLogger->LogBegin(__FUNC__);
   bool RC = false;
   TTime FBeg = Time();
   UnicodeString FParams = "; Params: ";
   if (AParam1Name.Length())
    FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
   if (AParam2Name.Length())
    FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
   if (AParam3Name.Length())
    FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
   if (AParam4Name.Length())
    FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "") + "; ";
   if (AParam5Name.Length())
    FParams += AParam5Name + "=" + VarToStrDef(AParam5Val, "");
   if (AParam6Name.Length())
    FParams += AParam6Name + "=" + VarToStrDef(AParam6Val, "");
   try
    {
     try
      {
       AQuery->Close();
       if (!AQuery->Transaction->Active)
        AQuery->Transaction->StartTransaction();
       AQuery->SQL->Text = ASQL;
       AQuery->Prepare();
       AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
       if (AParam2Name.Length())
        AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
       if (AParam3Name.Length())
        AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
       if (AParam4Name.Length())
        AQuery->ParamByName(AParam4Name)->Value = AParam4Val;
       if (AParam5Name.Length())
        AQuery->ParamByName(AParam5Name)->Value = AParam5Val;
       if (AParam6Name.Length())
        AQuery->ParamByName(AParam6Name)->Value = AParam6Val;
       AQuery->OpenOrExecute();
       if (ACommit)
        AQuery->Transaction->Commit();
       RC = true;
      }
     catch (System::Sysutils::Exception & E)
      {
       FLogger->LogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
      }
    }
   __finally
    {
     FLogger->LogSQL("[" + IntToStr(AQuery->RecordCount) + "] " + ((ALogComment.Length()) ? ALogComment : ASQL) +
       FParams, __FUNC__, FBeg - Time(), 5);
    }
   FLogger->LogEnd(__FUNC__);
   return RC;
  }
 // ---------------------------------------------------------------------------
 bool __fastcall TkabQueryHelper::ExecParamStream(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
   UnicodeString AParam1Name, Variant AParam1Val, UnicodeString ALogComment, UnicodeString AParam2Name,
   TStream * AParam2Val)
  {
   FLogger->LogBegin(__FUNC__);
   bool RC = false;
   TTime FBeg = Time();
   UnicodeString FParams = "; Params: ";
   if (AParam1Name.Length())
    FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
   if (AParam2Name.Length())
    FParams += AParam2Name + "=stm; ";
   try
    {
     try
      {
       AQuery->Close();
       if (!AQuery->Transaction->Active)
        AQuery->Transaction->StartTransaction();
       AQuery->SQL->Text = ASQL;
       AQuery->Prepare();
       AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
       if (AParam2Name.Length())
        AQuery->ParamByName(AParam2Name)->LoadFromStream(AParam2Val, ftBlob);
       AQuery->OpenOrExecute();
       if (ACommit)
        AQuery->Transaction->Commit();
       RC = true;
      }
     catch (System::Sysutils::Exception & E)
      {
       FLogger->LogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
      }
    }
   __finally
    {
     FLogger->LogSQL(((ALogComment.Length()) ? ALogComment : ASQL) + FParams, __FUNC__, FBeg - Time(), 5);
    }
   FLogger->LogEnd(__FUNC__);
   return RC;
  }
 // ---------------------------------------------------------------------------
__int64 __fastcall TkabQueryHelper::GetNewCode(UnicodeString TabId)
 {
  __int64 RC = -1;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    FQ->Command->CommandKind = skStoredProc;
    FQ->SQL->Text            = "MCODE_" + TabId;
    FQ->Prepare();
    FQ->OpenOrExecute();
    RC = FQ->FieldByName("MCODE")->AsInteger;
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall NewGUID()
 {
  return icsNewGUID().UpperCase();
 }
// ---------------------------------------------------------------------------
 bool __fastcall ConnectDB(TFDConnection * AConnection, TAppOptions * AOpt, UnicodeString ADBPath, UnicodeString AFunc,
   TOnLogMsgErrEvent AErrEvt, TOnLogBegEndEvent ABegEvt, TOnLogBegEndEvent AEndEvt)
  {
   bool RC = false;
   try
    {
     UnicodeString FFunc = UnicodeString(__FUNC__);
     if (AFunc.Length())
      FFunc = AFunc + "::" + FFunc;
     if (ABegEvt)
      ABegEvt(FFunc);
     else
      dsLogBegin(FFunc);
     try
      {
       if (AConnection->Connected)
        AConnection->Connected = false;
       Sleep(50);
       AConnection->DriverName = "FB";
       AConnection->Params->Clear();
       AConnection->Params->Add("DriverID=FB");
       AConnection->Params->Add("Database=" + ADBPath);
       AConnection->Params->Add("CharacterSet=UTF8");
       AConnection->Params->Add("User_Name=" + AOpt->Vals["fbuser"].AsStringDef("sysdba"));
       AConnection->Params->Add("Port=" + AOpt->Vals["fbport"].AsStringDef("3050"));
       AConnection->Params->Add("Server=" + AOpt->Vals["fbserver"].AsStringDef("localhost"));
       AConnection->Params->Add("Password=" + AOpt->Vals["fbpass"].AsStringDef("masterkey"));
       AConnection->Params->Add("Pooled=False");
       AConnection->Connected = true;
       Sleep(50);
       RC = true;
      }
     catch (Exception & E)
      {
       if (AErrEvt)
        AErrEvt("������ ����������� � �� \"" + ADBPath + "\". ��������� ���������: " + E.Message, FFunc, 1);
       else
        dsLogError("������ ����������� � �� \"" + ADBPath + "\". ��������� ���������: " + E.Message, FFunc, 1);
      }
     // }
     if (AEndEvt)
      AEndEvt(FFunc);
     else
      dsLogEnd(FFunc);
    }
   __finally
    {
    }
   return RC;
  }
 // ---------------------------------------------------------------------------
 void __fastcall DisconnectDB(TFDConnection * AConnection, UnicodeString AFunc, TOnLogBegEndEvent ABegEvt,
   TOnLogBegEndEvent AEndEvt)
  {
   UnicodeString FFunc = UnicodeString(__FUNC__);
   if (AFunc.Length())
    FFunc = AFunc + "::" + FFunc;
   if (ABegEvt)
    ABegEvt(FFunc);
   else
    dsLogBegin(FFunc);
   if (AConnection->Connected)
    AConnection->Connected = false;
   Sleep(50);
   if (AEndEvt)
    AEndEvt(FFunc);
   else
    dsLogEnd(FFunc);
  }
 // ---------------------------------------------------------------------------
 UnicodeString __fastcall GetUSRDBName(TAppOptions * AOpt)
  {
   UnicodeString RC = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\USR.FDB");
   try
    {
     if (AOpt->Vals["connecttype"].AsStringDef("1") == "1")
      { // ���������
       RC = AOpt->Vals["usrpath"].AsStringDef(RC);
      }
     else
      { // ���������
       RC = AOpt->Vals["usralias"].AsStringDef(RC);
      }
    }
   __finally
    {
    }
   return RC;
  }
 // ---------------------------------------------------------------------------
 UnicodeString __fastcall GetKLADRDBName(TAppOptions * AOpt)
  {
   UnicodeString RC = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\KLADR.FDB");
   try
    {
     if (AOpt->Vals["connecttype"].AsStringDef("1") == "1")
      { // ���������
       RC = AOpt->Vals["kladrpath"].AsStringDef(RC);
      }
     else
      { // ���������
       RC = AOpt->Vals["kladralias"].AsStringDef(RC);
      }
    }
   __finally
    {
    }
   return RC;
  }
 // ---------------------------------------------------------------------------
 }
