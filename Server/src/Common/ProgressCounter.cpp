//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ProgressCounter.h"
//#include "icsDocExConstDef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
__fastcall TdsProgressCounter::TdsProgressCounter()
 {
  //Caption = FMT(&_icsDocExDocCreateProgressCaption);
  FStageByCommCaption = "step %s by %s";
  FPrCaption          = "create";
  FOnPrChange         = NULL;
  Init();
 }
//---------------------------------------------------------------------------
void __fastcall TdsProgressCounter::Init()
 {
  FCommMsg    = "";
  FStageMsg   = "";
  FCommSumMsg = "";

  FCallType = TkabProgressType::None;
  FCallVal  = 0;
  FCallMsg  = "";

  FCommMax      = FStageMax = 100.0;
  FCommProgress = FStageProgress = 0;

  FCommStep = FStageStep = FStageCommStep = 0;
  FCurStage = FStageCount = FStageElementsCount = 0;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsProgressCounter::SetProgress(TkabProgressType AType, int AVal, UnicodeString AMsg)
 {
  /*enum class TkabProgressType   {   None, Error,   InitCommon,   InitStage,   CommPerc,     StagePerc,   CommInc,      StageInc,   CommSetMsg,   StageSetMsg,   CommComplite, StageComplite   };*/
  if (FOnComplite && (AType == TkabProgressType::CommComplite))
   FOnComplite(this);
  switch (AType)
   {
    /*FCommMsg = "";     FStageMsg = "";     FCommSumMsg = "";     FCallType = TkabProgressType::None;     FCallVal = 0;     FCallMsg = "";     FCommMax = FStageMax = 100.0;     FCommProgress = FStageProgress = 0;     FCommStep = FStageStep = 0     FCurStage = FStageCount = FStageElementsCount = 0;*/
   case TkabProgressType::InitCommon:
     {
      FCommProgress = 0;
      if (AVal)
       {
        FCommStep   = FCommMax / AVal;
        FStageCount = AVal;
       }
      else
       {
        FCommStep   = FCommMax;
        FStageCount = 1;
       }
      if (AMsg.Length())
       FCommMsg = AMsg;
      break;
     }
   case TkabProgressType::InitStage:
     {
      FCommProgress = FCurStage * FCommStep;
      FCommSumMsg   = FloatToStrF(FCommProgress, ffFixed, 3, 1) + "% - " + FPrCaption;

      FCurStage++ ;
      FStageMsg      = Format(FStageByCommCaption, OPENARRAY(TVarRec, (IntToStr(CurStage), IntToStr(StageCount))));
      FStageProgress = 0;

      if (AVal)
       {
        FStageStep     = FStageMax / AVal;
        FStageCommStep = FCommStep / AVal;
       }
      else
       {
        FStageCommStep = FCommStep;
        FStageStep     = 1;
       }
      FStageElementsCount = AVal;
      if (AMsg.Length())
       FStageMsg = AMsg;
      break;
     }
   case TkabProgressType::StageInc:
     {
      if (AVal)
       FStageProgress += FStageStep * AVal;
      else
       FStageProgress += FStageStep;

      FCommProgress += FStageCommStep;

      FCommSumMsg = FloatToStrF(FCommProgress, ffFixed, 3, 1) + "% - " + FPrCaption;

      if (AMsg.Length())
       FStageMsg = AMsg;
      break;
     }
   case TkabProgressType::CommInc:
     {
      if (AVal)
       FCommProgress += FCommStep * AVal;
      else
       FCommProgress += FCommStep;

      if (AMsg.Length())
       FCommMsg = AMsg;
      break;
     }
   }
  FCallType = AType;
  FCallVal  = AVal;
  FCallMsg  = AMsg;
  if (FOnPrChange)
   FOnPrChange(this);
  return true;
 }
//---------------------------------------------------------------------------
void __fastcall TdsProgressCounter::SetCommonPercent(int APersent)
 {
  FCommProgress = APersent * 100;
  FCommSumMsg   = IntToStr(APersent) + "% - " + FPrCaption;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsProgressCounter::FGetAsString()
 {
  UnicodeString RC = "<error/>";
  try
   {
    /*
     enum class TkabProgressType
     {
     None, Error,
     InitCommon,   InitStage,
     CommPerc,     StagePerc,
     CommInc,      StageInc,
     CommSetMsg,   StageSetMsg,
     CommComplite, StageComplite
     };
     */
    RC = "<pr SM='" + IntToStr(FStageMax)
        + "' SP='" + FloatToStrF(FStageProgress, ffFixed, 3, 1)
        + "' SMsg='" + FStageMsg
        + "' CM='" + IntToStr(FCommMax)
        + "' CP='" + FloatToStrF(FCommProgress, ffFixed, 3, 1)
        + "' CMsg='" + FCommMsg
        + "' CSMsg='" + FCommSumMsg
        + "' CS='" + IntToStr(FCurStage)
        + "' SC='" + IntToStr(FStageCount)
        + "' SEC='" + IntToStr(FStageElementsCount)
        + "' LCT='" + IntToStr((int)FCallType)
        + "' LCV='" + IntToStr(FCallVal)
        + "' LCMsg='" + FCallMsg + "'/>";
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
