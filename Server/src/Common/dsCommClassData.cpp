//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

//---------------------------------------------------------------------------
#include "dsCommClassData.h"

#pragma package(smart_init)

//###########################################################################
//##                                                                       ##
//##                              Globals                                  ##
//##                                                                       ##
//###########################################################################

__fastcall TdsCommClassData::TdsCommClassData(TFDConnection * AConnection)
 {
  dsLogC(__FUNC__);

  TFDQuery * FQ = CreateTempQuery(AConnection);
  try
   {
    TIntMap * vti;
    // ***** ������ �������� + ���������� ���������� � ������������ ******
    if (quExec(FQ, false, "Select CODE as ps1, R003C as ps2, R006E as ps3 From CLASS_003A", "������ ��������"))
     {
      while (!FQ->Eof)
       {
        InfList[FQ->FieldByName("ps1")->AsString] = FQ->FieldByName("ps2")->AsString;
        ShortInfList[FQ->FieldByName("ps1")->AsString] = FQ->FieldByName("ps3")->AsString;
        FQ->Next();
       }
     }
    // ***** ������ ���� ******
    if (quExec(FQ, false, "Select CODE as ps1, R0040 as ps2 From CLASS_0035", "������ ����"))
     {
      while (!FQ->Eof)
       {
        VacList[FQ->FieldByName("ps1")->AsString] = FQ->FieldByName("ps2")->AsString;
        FQ->Next();
       }
     }
    // ***** ������ ���� ******
    if (quExec(FQ, false, "Select CODE as ps1, R002C as ps2 From CLASS_002A", "������ ����"))
     {
      while (!FQ->Eof)
       {
        TestList[FQ->FieldByName("ps1")->AsString] = FQ->FieldByName("ps2")->AsString;
        FQ->Next();
       }
     }
    // ***** ������ �������� ******
    if (quExec(FQ, false, "Select CODE as ps1, R013F as ps2 From CLASS_013E", "������ ��������"))
     {
      while (!FQ->Eof)
       {
        CheckList[FQ->FieldByName("ps1")->AsString] = FQ->FieldByName("ps2")->AsString;
        FQ->Next();
       }
     }
    // ***** ������ ������� ******
    if (quExec(FQ, false, "Select CODE as ps1, R1159 as ps2 From CLASS_0038", "������ �������"))
     {
      while (!FQ->Eof)
       {
        ReackList[FQ->FieldByName("ps1")->AsString] = FQ->FieldByName("ps2")->AsString;
        FQ->Next();
       }
     }
    // ***** �������� �� ������ � ����� �� ��������� ******
    if (quExec(FQ, false, "Select R007B as ps1, R007A as ps2 From CLASS_0079", "�������� �� ������ � ����� �� ���������"))
     {
      while (!FQ->Eof)
       {
        if (InfProbList.find(FQ->FieldByName("ps1")->AsInteger) == InfProbList.end())
         InfProbList[FQ->FieldByName("ps1")->AsInteger] = new TIntMap;
        vti = InfProbList[FQ->FieldByName("ps1")->AsInteger];
        (*vti)[FQ->FieldByName("ps2")->AsInteger] = 1;

        if (ProbInfList.find(FQ->FieldByName("ps2")->AsInteger) == ProbInfList.end())
         ProbInfList[FQ->FieldByName("ps2")->AsInteger] = new TIntMap;
        vti = ProbInfList[FQ->FieldByName("ps2")->AsInteger];
        (*vti)[FQ->FieldByName("ps1")->AsInteger] = 1;

        FQ->Next();
       }
     }
    // ***** �������� �� �������� � ������� �� ��������� ******
    if (quExec(FQ, false, "Select R003E as Inf, R003B as MIBP From CLASS_002D", "�������� �� �������� � ������� �� ���������"))
     {
      while (!FQ->Eof)
       {
        if (InfVacList.find(FQ->FieldByName("Inf")->AsInteger) == InfVacList.end())
         InfVacList[FQ->FieldByName("Inf")->AsInteger] = new TIntMap;
        vti = InfVacList[FQ->FieldByName("Inf")->AsInteger];
        (*vti)[FQ->FieldByName("MIBP")->AsInteger] = 1;

        if (VacInfList.find(FQ->FieldByName("MIBP")->AsInteger) == VacInfList.end())
         VacInfList[FQ->FieldByName("MIBP")->AsInteger] = new TIntMap;
        vti = VacInfList[FQ->FieldByName("MIBP")->AsInteger];
        (*vti)[FQ->FieldByName("Inf")->AsInteger] = 1;
        FQ->Next();
       }
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
  dsLogC(__FUNC__, true);
 }
//---------------------------------------------------------------------------
__fastcall TdsCommClassData::~TdsCommClassData()
 {
  dsLogD(__FUNC__);
  for (TIntListMap::iterator i = InfProbList.begin(); i != InfProbList.end(); i++)
   {
    if (i->second)
     delete i->second;
    i->second = NULL;
   }
  InfProbList.clear();
  for (TIntListMap::iterator i = ProbInfList.begin(); i != ProbInfList.end(); i++)
   {
    if (i->second)
     delete i->second;
    i->second = NULL;
   }
  ProbInfList.clear();
  for (TIntListMap::iterator i = InfVacList.begin(); i != InfVacList.end(); i++)
   {
    if (i->second)
     delete i->second;
    i->second = NULL;
   }
  InfVacList.clear();
  for (TIntListMap::iterator i = VacInfList.begin(); i != VacInfList.end(); i++)
   {
    if (i->second)
     delete i->second;
    i->second = NULL;
   }
  VacInfList.clear();

  VacList.clear();
  TestList.clear();
  InfList.clear();
  ShortInfList.clear();
  CheckList.clear();
  ReackList.clear();

  FVPrior.clear();
  FRVPrior.clear();
  PreTestList.clear();

  dsLogD(__FUNC__, true);
 }
//---------------------------------------------------------------------------
TFDQuery * __fastcall TdsCommClassData::CreateTempQuery(TFDConnection * AConnection)
 {
  TFDQuery * FQ = new TFDQuery(NULL);
  FQ->Transaction = new TFDTransaction(NULL);
  try
   {
    FQ->Transaction->Connection = AConnection;
    FQ->Connection              = AConnection;

    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();

   }
  __finally
   {
   }
  return FQ;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCommClassData::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::InfInVac(UnicodeString AInf, UnicodeString AVac)
 {//�������� ��������� �������� AInf � ������ ���� AVac
  bool RC = false;
  try
   {
    TIntListMap::iterator FIRC = InfVacList.find(AInf.ToIntDef(0));
    if (FIRC != InfVacList.end())
     {
      TIntMap::iterator FVRC = FIRC->second->find(AVac.ToIntDef(0));
      RC = (FVRC != FIRC->second->end());
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::GetMIBPInfList(int AMIBPCode, TIntMap *& ARC)
 {//������ �������� ����
  bool RC = false;
  ARC = NULL;
  try
   {
    TIntListMap::iterator FIRC = VacInfList.find(AMIBPCode);
    if (FIRC != VacInfList.end())
     {
      ARC = FIRC->second;
      RC  = true;
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
int __fastcall TdsCommClassData::GetMIBPInfCount(int AMIBPCode)
 {//������ �������� ����
  int RC = 0;
  try
   {
    TIntListMap::iterator FIRC = VacInfList.find(AMIBPCode);
    if (FIRC != VacInfList.end())
     {
      RC = FIRC->second->size();
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::GetInfMIBPList(int AInfCode, TIntMap *& ARC)
 {//������ ���� �� ��������
  bool RC = false;
  ARC = NULL;
  try
   {
    TIntListMap::iterator FIRC = InfVacList.find(AInfCode);
    if (FIRC != InfVacList.end())
     {
      ARC = FIRC->second;
      RC  = true;
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::InfInTest(UnicodeString AInf, UnicodeString ATest)
 {//�������� ��������� �������� AInf � ������ �������� ATest
  bool RC = false;
  try
   {
    TIntListMap::iterator FIRC = InfProbList.find(AInf.ToIntDef(0));
    if (FIRC != InfProbList.end())
     {
      TIntMap::iterator FVRC = FIRC->second->find(ATest.ToIntDef(0));
      RC = (FVRC != FIRC->second->end());
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL+", ��="+AQuery->Connection->Params->Database, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::FGetInfName(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    TAnsiStrMap::iterator FRC = InfList.find(ACode);
    if (FRC != InfList.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::FGetVacName(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    TAnsiStrMap::iterator FRC = VacList.find(ACode);
    if (FRC != VacList.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::FGetTestName(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    TAnsiStrMap::iterator FRC = TestList.find(ACode);
    if (FRC != TestList.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::FGetCheckName(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    TAnsiStrMap::iterator FRC = CheckList.find(ACode);
    if (FRC != CheckList.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::VacNameStr(__int64 ACode)
 {
  UnicodeString RC = "";
  try
   {
    RC = "[" + IntToStr(ACode) + "]\"" + VacList[IntToStr(ACode)] + "\"";
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::InfNameStr(__int64 ACode)
 {
  UnicodeString RC = "";
  try
   {
    RC = "[" + IntToStr(ACode) + "]\"" + InfList[IntToStr(ACode)] + "\"";
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCommClassData::LoadPlanData(TTagNode * APlanOpt)
 {
  try
   {
    TTagNode * itPrior = APlanOpt->GetChildByName("infdef", true);
    if (itPrior)
     {
      itPrior = itPrior->GetFirstChild();
      while (itPrior)
       {
        FVPrior[itPrior->AV["infref"].ToIntDef(0)]  = itPrior->AV["v"].ToIntDef(0);
        FRVPrior[itPrior->AV["infref"].ToIntDef(0)] = itPrior->AV["rv"].ToIntDef(0);
        itPrior                                     = itPrior->GetNext();
       }
     }
    // ***** ������ ����/���������������� ���� ******

    TTagNode * preTest = APlanOpt->GetChildByName("pretest", true);
    if (preTest)
     {
      TPreTestItem FPreTest;
      preTest = preTest->GetFirstChild();
      while (preTest)
       {
        FPreTest.Inf     = preTest->AV["inf"].ToIntDef(0);
        FPreTest.Test    = preTest->AV["test"].ToIntDef(0);
        FPreTest.Reak    = preTest->AV["rv"].ToIntDef(0);
        FPreTest.Actuals = preTest->AV["actuals"].ToIntDef(0);
        FPreTest.Kind    = (TPrePlanKind)preTest->AV["plantype"].ToIntDef(0);
        FPreTest.Pause   = preTest->AV["pause"].ToIntDef(0);
        FPreTest.MinAge  = preTest->AV["minage"];
        FPreTest.MaxAge  = preTest->AV["maxage"];
        PreTestList.push_back(FPreTest);

        preTest = preTest->GetNext();
       }
     }
   }
  __finally
   {
   }
 }
//---------------------------------------------------------------------------
int __fastcall TdsCommClassData::FGetVPrior(int ACode)
 {
  int RC = -1;
  try
   {
    TGIntMap::iterator FRC = FVPrior.find(ACode);
    if (FRC != FVPrior.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
int __fastcall TdsCommClassData::FGetRVPrior(int ACode)
 {
  int RC = -1;
  try
   {
    TGIntMap::iterator FRC = FRVPrior.find(ACode);
    if (FRC != FRVPrior.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------

