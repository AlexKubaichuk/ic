// ---------------------------------------------------------------------------
#pragma hdrstop
#include "kabBaseLog.h"
#include <windows.h>
#include <stdio.h>
#include <psapi.h>
#include <System.Zip.hpp>
#include <System.DateUtils.hpp>
using namespace std;
// ---------------------------------------------------------------------------
#pragma package(smart_init)
namespace kabLog
 {
 // ---------------------------------------------------------------------------
 UnicodeString __fastcall LogUser(TDSSession * ASes)
  {
   UnicodeString RC = "";
   try
    {
     if (ASes)
      {
       RC = ASes->SessionName + " {" + ASes->UserName + "}";
      }
    }
   __finally
    {
    }
   return RC;
  }
 // ---------------------------------------------------------------------------
 // ##############################################################################
 // #                                                                            #
 // #                            TLog                                            #
 // #                                                                            #
 // ##############################################################################
 // ---------------------------------------------------------------------------
 __fastcall TLog::TLog(UnicodeString ALogName)
  {
   FCalcWECount  = false;
   FStream       = NULL;
   FLogName      = ALogName;
   FWarningCount = 0;
   FErrorCount   = 0;
   FLogLevel     = 1;
   FLogPath      = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\log");
   if (!DirectoryExists(FLogPath))
    ForceDirectories(FLogPath);
  }
 // ---------------------------------------------------------------------------
 __fastcall TLog::~TLog()
  {
   FreeData();
  }
 // ---------------------------------------------------------------------------
 void __fastcall TLog::FreeData()
  {
   if (FStream)
    {
     delete FStream;
     FStream = NULL;
     if (FBackgroundZIP)
      delete FBackgroundZIP;
     FBackgroundZIP = NULL;
    }
  }
 // ---------------------------------------------------------------------------
 void __fastcall TLog::CheckActive()
  {
   if (FFileName.Length())
    {
     if (!FStream)
      {
       FStream = CreateFileStream(FFileName);
       LogMessage(FLogName + " log created at " + Now().FormatString("dd.mm.yyyy hh:nn:ss") +
         "  *******************************************");
      }
    }
   else
    {
     if (FCreateDate != Date())
      {
       FCreateDate = Date();
       UnicodeString FileName = FLogPath + "\\" + FLogName + Date().FormatString("yyyymmdd") + ".log";
       if (FStream)
        FreeData();
       FStream = CreateFileStream(FileName);
       LogMessage(FLogName + " log created at " + Now().FormatString("dd.mm.yyyy hh:nn:ss") +
         "  *******************************************");
       TDate OldDate = IncDay(FCreateDate, -1);
       ZIPLogFile(FLogPath + "\\" + FLogName + OldDate.FormatString("yyyymmdd") + ".log");
      }
    }
  }
 // ---------------------------------------------------------------------------
 TStream * __fastcall TLog::CreateFileStream(UnicodeString AFileName) const
  {
   UnicodeString FFN = icsPrePath(AFileName);
   if (!FileExists(FFN))
    {
     TStream * tmp = new TFileStream(FFN, fmCreate | fmShareDenyNone);
     delete tmp;
    }
   TStream * stream = new TFileStream(FFN, fmOpenReadWrite | fmShareDenyNone);
   try
    {
     stream->Seek(0, soFromEnd);
    }
   catch (...)
    {
     delete stream;
     throw;
    }
   return stream;
  }
 // ---------------------------------------------------------------------------
 UnicodeString __fastcall TLog::CurrentTime() const
  {
   return Time().FormatString("hh:nn:ss.zzz");
  }
 // ---------------------------------------------------------------------------
 UnicodeString __fastcall TLog::CurrentDate() const
  {
   return Date().FormatString("yyyy.mm.dd");
  }
 // ---------------------------------------------------------------------------
 void __fastcall TLog::Write(UnicodeString AStr)
  {
   CheckActive();
   TByteDynArray FByteBuf = TByteDynArray();
   // TEncoding * FEncode = ;
   FByteBuf = (TEncoding::UTF8)->GetBytes(AStr);
   FStream->WriteBuffer(FByteBuf, FByteBuf.Length);
  }
 // ---------------------------------------------------------------------------
 void __fastcall TLog::WriteLn(UnicodeString AStr)
  {
   Write(AStr + "\r\n");
  }
 // ---------------------------------------------------------------------------
 void __fastcall TLog::WriteLn()
  {
   Write("\r\n");
  }
 // ---------------------------------------------------------------------------
 void __fastcall TLog::LogMessage(UnicodeString AMessage, int ALvl)
  {
   if (ALvl <= FLogLevel)
    WriteLn(CurrentDate() + " " + CurrentTime() + " {" + PrintProcessMemoryInfo() + "} " +
     LogUser(TDSSessionManager::Instance->GetThreadSession()) + " " + AMessage);
  }
 // ---------------------------------------------------------------------------
 void __fastcall TLog::LogWarning(UnicodeString AMessage, int ALvl)
  {
   if (ALvl <= FLogLevel)
    {
     if (FCalcWECount)
      FWarningCount++ ;
     WriteLn(CurrentDate() + " " + CurrentTime() + " {" + PrintProcessMemoryInfo() + "} " +
       LogUser(TDSSessionManager::Instance->GetThreadSession()) + "  ��������������: " + AMessage);
    }
  }
 // ---------------------------------------------------------------------------
 void __fastcall TLog::LogError(UnicodeString AMessage, int ALvl)
  {
   if (FCalcWECount)
    FErrorCount++ ;
   WriteLn(CurrentDate() + " " + CurrentTime() + " {" + PrintProcessMemoryInfo() + "} " +
     LogUser(TDSSessionManager::Instance->GetThreadSession()) + "  ������: " + AMessage);
  }
 // ---------------------------------------------------------------------------
 UnicodeString __fastcall PrintProcessMemoryInfo(DWORD AProcessID)
  {
   DWORD processID = AProcessID;
   if (!processID)
    processID = GetCurrentProcessId();
   UnicodeString RC = "";
   UnicodeString tmp = "";
   HANDLE hProcess;
   PROCESS_MEMORY_COUNTERS pmc;
   hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processID);
   if (hProcess)
    {
     if (GetProcessMemoryInfo(hProcess, &pmc, sizeof(pmc)))
      {
       RC += tmp.sprintf(L"%d", pmc.WorkingSetSize);
      }
     CloseHandle(hProcess);
    }
   return RC;
  }
 // ---------------------------------------------------------------------------
 __fastcall TkabLogHelper::TkabLogHelper()
  {
  }
 // ---------------------------------------------------------------------------
 __fastcall TkabLogHelper::~TkabLogHelper()
  {
  }
 // ---------------------------------------------------------------------------
 void __fastcall TkabLogHelper::LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
  {
   if (FOnLogMessage)
    FOnLogMessage(AMessage, AFuncName, ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall TkabLogHelper::LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
  {
   if (FOnLogError)
    FOnLogError(AMessage, AFuncName, ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall TkabLogHelper::LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
  {
   if (FOnLogSQL)
    FOnLogSQL(ASQL, AFuncName, AExecTime, ALvl);
  }
 // ---------------------------------------------------------------------------
 void __fastcall TkabLogHelper::LogBegin(UnicodeString AFuncName)
  {
   if (FOnLogBegin)
    FOnLogBegin(AFuncName);
  }
 // ---------------------------------------------------------------------------
 void __fastcall TkabLogHelper::LogEnd(UnicodeString AFuncName)
  {
   if (FOnLogEnd)
    FOnLogEnd(AFuncName);
  }
 // ---------------------------------------------------------------------------
 // ###########################################################################
 // ##                                                                       ##
 // ##                     TBackgroundMISSync                                ##
 // ##                                                                       ##
 // ###########################################################################
 class TLog::TBackgroundZIP : public TThread
  {
  private:
   UnicodeString FFileName;
  protected:
   virtual void __fastcall Execute();
  public:
   __fastcall TBackgroundZIP(UnicodeString AFileName);
   __fastcall ~TBackgroundZIP();
  };
 // ---------------------------------------------------------------------------
 // ###########################################################################
 // ##                                                                       ##
 // ##                     TMISSync::TBackgroundMISSync                      ##
 // ##                                                                       ##
 // ###########################################################################
 __fastcall TLog::TBackgroundZIP::TBackgroundZIP(UnicodeString AFileName) : TThread(false), FFileName(AFileName)
  {
  }
 // ---------------------------------------------------------------------------
 __fastcall TLog::TBackgroundZIP::~TBackgroundZIP()
  {
  }
 // ---------------------------------------------------------------------------
 void __fastcall TLog::TBackgroundZIP::Execute()
  {
   UnicodeString FFN = icsPrePath(FFileName);
   if (FileExists(FFN))
    {
     TZipFile * FZF = new TZipFile;
     try
      {
       FZF->Open(FFN + ".zip", zmWrite);
       FZF->Add(FFN, ExtractFileName(FFileName));
       FZF->Close();
      }
     __finally
      {
       delete FZF;
       DeleteFile(FFN);
      }
    }
  }
 // ---------------------------------------------------------------------------
 void __fastcall TLog::ZIPLogFile(UnicodeString AFileName)
  {
   if (FBackgroundZIP)
    delete FBackgroundZIP;
   FBackgroundZIP = new TBackgroundZIP(AFileName);
  }
 // ---------------------------------------------------------------------------
 }
