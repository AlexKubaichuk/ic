//---------------------------------------------------------------------------

#ifndef TimeCheckerH
#define TimeCheckerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
//---------------------------------------------------------------------------
class TTimeChecker : public TObject
{
private:
    TTime FBeg, FCur;
    UnicodeString FValStr;

public:
    __fastcall TTimeChecker();
    virtual __fastcall ~TTimeChecker();

    void __fastcall Check(UnicodeString AMessage = "");
    UnicodeString __fastcall GetStr(UnicodeString AMessage = "");
};
//---------------------------------------------------------------------------
#endif
