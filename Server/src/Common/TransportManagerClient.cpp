// 
// Created by the DataSnap proxy generator.
// 23.01.2017 0:05:42
// 

#include "TransportManagerClient.h"

System::UnicodeString __fastcall TTransportManagerClient::GetUpstreamIncomingIds()
{
  if (FGetUpstreamIncomingIdsCommand == NULL)
  {
    FGetUpstreamIncomingIdsCommand = FDBXConnection->CreateCommand();
    FGetUpstreamIncomingIdsCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FGetUpstreamIncomingIdsCommand->Text = "TTransportManager.GetUpstreamIncomingIds";
    FGetUpstreamIncomingIdsCommand->Prepare();
  }
  FGetUpstreamIncomingIdsCommand->ExecuteUpdate();
  System::UnicodeString result = FGetUpstreamIncomingIdsCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TTransportManagerClient::GetDownstreamIncomingIds()
{
  if (FGetDownstreamIncomingIdsCommand == NULL)
  {
    FGetDownstreamIncomingIdsCommand = FDBXConnection->CreateCommand();
    FGetDownstreamIncomingIdsCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FGetDownstreamIncomingIdsCommand->Text = "TTransportManager.GetDownstreamIncomingIds";
    FGetDownstreamIncomingIdsCommand->Prepare();
  }
  FGetDownstreamIncomingIdsCommand->ExecuteUpdate();
  System::UnicodeString result = FGetDownstreamIncomingIdsCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TTransportManagerClient::GetIncomingDownstreamIds()
{
  if (FGetIncomingDownstreamIdsCommand == NULL)
  {
    FGetIncomingDownstreamIdsCommand = FDBXConnection->CreateCommand();
    FGetIncomingDownstreamIdsCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FGetIncomingDownstreamIdsCommand->Text = "TTransportManager.GetIncomingDownstreamIds";
    FGetIncomingDownstreamIdsCommand->Prepare();
  }
  FGetIncomingDownstreamIdsCommand->ExecuteUpdate();
  System::UnicodeString result = FGetIncomingDownstreamIdsCommand->Parameters->Parameter[0]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TTransportManagerClient::GetDownstreamIncomingIdsByPeer(System::UnicodeString PeerId)
{
  if (FGetDownstreamIncomingIdsByPeerCommand == NULL)
  {
    FGetDownstreamIncomingIdsByPeerCommand = FDBXConnection->CreateCommand();
    FGetDownstreamIncomingIdsByPeerCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FGetDownstreamIncomingIdsByPeerCommand->Text = "TTransportManager.GetDownstreamIncomingIdsByPeer";
    FGetDownstreamIncomingIdsByPeerCommand->Prepare();
  }
  FGetDownstreamIncomingIdsByPeerCommand->Parameters->Parameter[0]->Value->SetWideString(PeerId);
  FGetDownstreamIncomingIdsByPeerCommand->ExecuteUpdate();
  System::UnicodeString result = FGetDownstreamIncomingIdsByPeerCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

void __fastcall TTransportManagerClient::SendToUpstream(System::UnicodeString Id, System::UnicodeString FileName)
{
  if (FSendToUpstreamCommand == NULL)
  {
    FSendToUpstreamCommand = FDBXConnection->CreateCommand();
    FSendToUpstreamCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FSendToUpstreamCommand->Text = "TTransportManager.SendToUpstream";
    FSendToUpstreamCommand->Prepare();
  }
  FSendToUpstreamCommand->Parameters->Parameter[0]->Value->SetWideString(Id);
  FSendToUpstreamCommand->Parameters->Parameter[1]->Value->SetWideString(FileName);
  FSendToUpstreamCommand->ExecuteUpdate();
}

void __fastcall TTransportManagerClient::GetFromUpstream(System::UnicodeString Id, System::UnicodeString FileName)
{
  if (FGetFromUpstreamCommand == NULL)
  {
    FGetFromUpstreamCommand = FDBXConnection->CreateCommand();
    FGetFromUpstreamCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FGetFromUpstreamCommand->Text = "TTransportManager.GetFromUpstream";
    FGetFromUpstreamCommand->Prepare();
  }
  FGetFromUpstreamCommand->Parameters->Parameter[0]->Value->SetWideString(Id);
  FGetFromUpstreamCommand->Parameters->Parameter[1]->Value->SetWideString(FileName);
  FGetFromUpstreamCommand->ExecuteUpdate();
}

void __fastcall TTransportManagerClient::SendToDownstream(System::UnicodeString Id, System::UnicodeString FileName)
{
  if (FSendToDownstreamCommand == NULL)
  {
    FSendToDownstreamCommand = FDBXConnection->CreateCommand();
    FSendToDownstreamCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FSendToDownstreamCommand->Text = "TTransportManager.SendToDownstream";
    FSendToDownstreamCommand->Prepare();
  }
  FSendToDownstreamCommand->Parameters->Parameter[0]->Value->SetWideString(Id);
  FSendToDownstreamCommand->Parameters->Parameter[1]->Value->SetWideString(FileName);
  FSendToDownstreamCommand->ExecuteUpdate();
}

void __fastcall TTransportManagerClient::GetFromDownstream(System::UnicodeString Id, System::UnicodeString FileName)
{
  if (FGetFromDownstreamCommand == NULL)
  {
    FGetFromDownstreamCommand = FDBXConnection->CreateCommand();
    FGetFromDownstreamCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FGetFromDownstreamCommand->Text = "TTransportManager.GetFromDownstream";
    FGetFromDownstreamCommand->Prepare();
  }
  FGetFromDownstreamCommand->Parameters->Parameter[0]->Value->SetWideString(Id);
  FGetFromDownstreamCommand->Parameters->Parameter[1]->Value->SetWideString(FileName);
  FGetFromDownstreamCommand->ExecuteUpdate();
}

void __fastcall TTransportManagerClient::ResetStorage()
{
  if (FResetStorageCommand == NULL)
  {
    FResetStorageCommand = FDBXConnection->CreateCommand();
    FResetStorageCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FResetStorageCommand->Text = "TTransportManager.ResetStorage";
    FResetStorageCommand->Prepare();
  }
  FResetStorageCommand->ExecuteUpdate();
}

void __fastcall TTransportManagerClient::RebuildStorageIndex()
{
  if (FRebuildStorageIndexCommand == NULL)
  {
    FRebuildStorageIndexCommand = FDBXConnection->CreateCommand();
    FRebuildStorageIndexCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FRebuildStorageIndexCommand->Text = "TTransportManager.RebuildStorageIndex";
    FRebuildStorageIndexCommand->Prepare();
  }
  FRebuildStorageIndexCommand->ExecuteUpdate();
}


__fastcall  TTransportManagerClient::TTransportManagerClient(TDBXConnection *ADBXConnection)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = True;
}


__fastcall  TTransportManagerClient::TTransportManagerClient(TDBXConnection *ADBXConnection, bool AInstanceOwner)
{
  if (ADBXConnection == NULL) 
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = AInstanceOwner;
}


__fastcall  TTransportManagerClient::~TTransportManagerClient()
{
  delete FGetUpstreamIncomingIdsCommand;
  delete FGetDownstreamIncomingIdsCommand;
  delete FGetIncomingDownstreamIdsCommand;
  delete FGetDownstreamIncomingIdsByPeerCommand;
  delete FSendToUpstreamCommand;
  delete FGetFromUpstreamCommand;
  delete FSendToDownstreamCommand;
  delete FGetFromDownstreamCommand;
  delete FResetStorageCommand;
  delete FRebuildStorageIndexCommand;
}

