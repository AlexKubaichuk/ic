//---------------------------------------------------------------------------

#ifndef ProgressCounterH
#define ProgressCounterH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "XMLContainer.h"
//---------------------------------------------------------------------------
class PACKAGE TdsProgressCounter : public TObject
{
private:	// User declarations
  int FCommMax, FStageMax;
  double FCommStep, FCommProgress, FStageStep, FStageCommStep, FStageProgress;

  UnicodeString FCommMsg, FStageMsg, FCommSumMsg, FStageByCommCaption;

  int FCurStage;
  int FStageCount;
  int FStageElementsCount;

  TNotifyEvent   FOnPrChange;
  TNotifyEvent   FOnComplite;
  UnicodeString FPrCaption;
  TkabProgressType FCallType;
  int FCallVal;
  UnicodeString FCallMsg;
  UnicodeString __fastcall FGetAsString();

public:		// User declarations
  __fastcall TdsProgressCounter();

  bool __fastcall SetProgress(TkabProgressType AType, int AVal, UnicodeString AMsg);
  void __fastcall SetCommonPercent(int APersent);
  void __fastcall Init();

  //"������������ ���������";
  __property UnicodeString Caption  = {read=FPrCaption, write=FPrCaption};
  //'���� : %s �� %s';
  __property UnicodeString StageByCommCaption  = {read=FStageByCommCaption, write=FStageByCommCaption};


  __property int           StageMax           = {read=FStageMax};
  __property double        StageProgress      = {read=FStageProgress};
  __property UnicodeString StageMsg           = {read=FStageMsg};

  __property int           CommMax            = {read=FCommMax};
  __property double        CommProgress       = {read=FCommProgress};
  __property UnicodeString CommMsg            = {read=FCommMsg};
  __property UnicodeString CommSumMsg         = {read=FCommSumMsg};

  __property int           CurStage           = {read=FCurStage};
  __property int           StageCount         = {read=FStageCount};
  __property int           StageElementsCount = {read=FStageElementsCount};

  __property TkabProgressType CallType = {read=FCallType};
  __property int              CallVal  = {read=FCallVal};
  __property UnicodeString    CallMsg  = {read=FCallMsg};

  __property UnicodeString AsString = {read=FGetAsString};

  __property TNotifyEvent OnChange  = {read=FOnPrChange, write=FOnPrChange};
  __property TNotifyEvent OnComplite  = {read=FOnComplite, write=FOnComplite};
};
//---------------------------------------------------------------------------
#endif
