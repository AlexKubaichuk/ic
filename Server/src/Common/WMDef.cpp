﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include "WMDef.h"
// #include "SMDef.h"
// #include "dsRegClassUnit.h"
// #include "RegUnit.h"
#include "appbaseunit.h"
#include <Web.WebReq.hpp>
#include "icsLog.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TComponentClass WebModuleClass = __classid(TWMDefDM);
// ---------------------------------------------------------------------------
__fastcall TWMDefDM::TWMDefDM(TComponent * Owner) : TWebModule(Owner)
 {
 }
// ----------------------------------------------------------------------------
void __fastcall TWMDefDM::ServerFunctionInvokerHTMLTag(TObject * Sender, TTag Tag, const UnicodeString TagString,
  TStrings * TagParams, UnicodeString & ReplaceText)
 {
  dsLogMessage("Tag: " + IntToStr(Tag) + " " + TagString + "; TagParams: " + TagParams->Text, __FUNC__, 5);
  if (SameText(TagString, "urlpath"))
   ReplaceText = (Request->InternalScriptName);
  else if (SameText(TagString, "port"))
   ReplaceText = IntToStr(Request->ServerPort);
  else if (SameText(TagString, "host"))
   ReplaceText = (Request->Host);
  else if (SameText(TagString, "loginrequired"))
   {
    if (DSRESTWebDispatcher->AuthenticationManager != NULL)
     ReplaceText = "true";
    else
     ReplaceText = "false";
   }
  else if (SameText(TagString, "serverfunctionsjs"))
   ReplaceText = Request->InternalScriptName + "/js/serverfunctions.js";
  // else if (SameText(TagString, "classname"))
  // ReplaceText = __classid(TSMDefDM)->ClassName();
  else if (SameText(TagString, "servertime"))
   ReplaceText = DateTimeToStr(Now());
  else if (SameText(TagString, "serverfunctioninvoker"))
   {
    if (AllowServerFunctionInvoker())
     ReplaceText = "<div><a href='" + Request->InternalScriptName +
       "/ServerFunctionInvoker' target='_blank'>Server Functions</a></div>";
   }
  else
   ReplaceText = "";
 }
// ---------------------------------------------------------------------------
void __fastcall TWMDefDM::WebModuleDefaultAction(TObject * Sender, TWebRequest * Request, TWebResponse * Response,
  bool & Handled)
 {
  UnicodeString FContent = "";
  dsLogMessage("Request: " + WebRequestToShortString(Request), __FUNC__, 5);
  if (Request->InternalPathInfo == "" || Request->InternalPathInfo == "/")
   {
    try
     {
      // Response->Content = Index->Content();
      FContent                  = Index->Content();
      Response->ContentType     = "text/html; charset=UTF-8";
      Response->ContentEncoding = "UTF-8";
      Response->ContentStream   = new TMemoryStream;
      Response->ContentStream->Write(FContent.BytesOf(), FContent.BytesOf().Length);
      Response->ContentStream->Position = 0;
      // Response->SendResponse();
     }
    __finally
     {
      // delete Response->ContentStream;
      // Response->ContentStream = NULL;
     }
   }
  else
   Response->SendRedirect(Request->InternalScriptName + "/");
  UnicodeString Msg = "";
  if (FContent.Length() < 250)
   Msg = FContent;
  else
   Msg = FContent.SubString(1, 250) + " ...";
  dsLogMessage("Response: " + Msg, __FUNC__, 5);
 }
// ---------------------------------------------------------------------------
void __fastcall TWMDefDM::WebModuleBeforeDispatch(TObject * Sender, TWebRequest * Request, TWebResponse * Response,
  bool & Handled)
 {
  UnicodeString SID = "";
  TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
  if (FSes)
   SID = IntToStr(FSes->Id);
  dsLogMessage("{" + SID + "} Request: " + WebRequestToShortString(Request), __FUNC__, 5);
  // dsLogMessage("", __FUNC__, 5);
 }
// ---------------------------------------------------------------------------
bool __fastcall TWMDefDM::AllowServerFunctionInvoker(void)
 {
  return Request->RemoteAddr == "127.0.0.1" || Request->RemoteAddr == "0:0:0:0:0:0:0:1" || Request->RemoteAddr == "::1";
 }
// ---------------------------------------------------------------------------
void __fastcall TWMDefDM::WebFileDispatcherBeforeDispatch(TObject * Sender, const UnicodeString AFileName,
  TWebRequest * Request, TWebResponse * Response, bool & Handled)
 {
  Handled = False;
  if (SameFileName(ExtractFileName(AFileName), "serverfunctions.js"))
   {
    TDateTime D1, D2;
    if (FileAge(AFileName, D1) && FileAge(WebApplicationFileName(), D2) && (D1 < D2))
     {
      DSProxyGenerator->TargetDirectory = ExtractFilePath(AFileName);
      DSProxyGenerator->TargetUnitName  = ExtractFileName(AFileName);
      DSProxyGenerator->Write();
     }
   }
  UnicodeString SID = "";
  TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
  if (FSes)
   SID = IntToStr(FSes->Id);
  dsLogMessage("{" + SID + "} Response: " + WebResponseToString(Response), __FUNC__, 5);
  // FContent = Index->Content();
  // if (Request->PathTranslated.LowerCase().Pos(".css"))
  // Response->ContentType = "text/css; charset=UTF-8";
  // else if (Request->PathTranslated.LowerCase().Pos(".js"))
  // Response->ContentType = "application/javascript; charset=UTF-8";
  // else if (Request->PathTranslated.LowerCase().Pos("index"))
  // Response->ContentType = "text/html; charset=UTF-8";
  // Response->ContentEncoding = "UTF-8";
 }
// ---------------------------------------------------------------------------
void __fastcall TWMDefDM::WebModuleCreate(TObject * Sender)
 {
  FServerFunctionInvokerAction = ActionByName("ServerFunctionInvokerAction");
  DSServerMetaData->Server     = DSServer();
  DSRESTWebDispatcher->Server  = DSServer();
  if (DSServer()->Started)
   {
    DSRESTWebDispatcher->DbxContext = DSServer()->DbxContext;
    DSRESTWebDispatcher->Start();
   }
  DSRESTWebDispatcher->AuthenticationManager = DSAuthenticationManager();
 }
// ---------------------------------------------------------------------------
static void freeWebModules()
 {
  FreeWebModules();
 }
// ---------------------------------------------------------------------------
#pragma exit freeWebModules 33
// ---------------------------------------------------------------------------
void __fastcall TWMDefDM::DSRESTWebDispatcherHTTPTrace(TObject * Sender, TDSHTTPContext * AContext,
  TDSHTTPRequest * ARequest, TDSHTTPResponse * AResponse)
 {
  UnicodeString SID = "";
  TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
  if (FSes)
   {
    if (ARequest->Document.Pos("TdsAdminClass/LoginUser"))
     dsLogAuth(" { >> " + IntToStr(FSes->Id) + "} [" + ARequest->AuthUserName + "] : " + ARequest->RemoteIP);
    if (ARequest->Document.Pos("TdsAdminClass/LoginSysUser"))
     dsLogAuth(" { >>> " + IntToStr(FSes->Id) + "} [" + ARequest->AuthUserName + "] : " + ARequest->RemoteIP);
    SID = IntToStr(FSes->Id);
   }
  dsLogMessage("{" + SID + "} Request: " + DSHTTPRequestToString(ARequest) + "\t Response: " +
    DSHTTPResponseToString(AResponse), __FUNC__, 5);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TWMDefDM::DSHTTPRequestToString(TDSHTTPRequest * ARequest)
 {
  UnicodeString RC = "";
  try
   {
    RC += "Acc: " + ARequest->Accept; // public Specifies the Content-Types that are acceptable for the HTTP request.
    RC += "; AU: " + ARequest->AuthUserName; // public Specifies the authentication user name for the HTTP request.
    RC += "\t APassword: " + ARequest->AuthPassword;
    // public Specifies the authentication password for the HTTP request.
//    RC += "\t Command: " + ARequest->Command; // public Represents the HTTP command.
//    RC += "\t CommandType: " + ARequest->CommandType;
    // public Specifies the type of HTTP command to be processed by DataSnap.
    // RC += ", Disposed: "+        ARequest->Disposed; //protected Disposed is a read-only property that shows the current state of this object.
//    RC += "; Doc: " + ARequest->Document;
    RC += "\t URI: " + ARequest->URI; // public Specifies the uniform resource identifier (URI) for the HTTP request.
    // public Specifies the path, document name, and optional parameters for a DataSnap HTTP request.
    RC += "\t Params: " + ARequest->Params->Text; // public Specifies the HTTP header values.
    RC += "\t PostStream: " + IntToStr((__int64)ARequest->PostStream);
    // public Represents the stream used for passing data from a client POST request.
//    RC += "\t Pragma: " + ARequest->Pragma; // public Specifies the pragma field of the HTTP header.
//    RC += "\t ProtocolVersion: " + ARequest->ProtocolVersion;
    // public Specifies the version of the communications protocol.
    RC += "; rIP: " + ARequest->RemoteIP; // public Specifies the remote IP address for the HTTP request.
//    RC += "; UA: " + ARequest->UserAgent; // public Specifies the user agent.
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TWMDefDM::DSHTTPResponseToString(TDSHTTPResponse * AResponse)
 {
  UnicodeString RC = "";
  try
   {
    // RC += "CloseConnection: " + AResponse->CloseConnection; //public Indicates whether the connection should close after sending the response.
    RC += "CLen: " + IntToStr(AResponse->ContentLength); // public Specifies the length of the HTTP response content.
    // RC += "\t ContentStream: " + IntToStr((__int64)AResponse->ContentStream); //public Represents the content of the HTTP response.
    RC += "; CText: " + AResponse->ContentText; // public Specifies the textual content of the HTTP response.
    RC += "; CType: " + AResponse->ContentType; // public Specifies the Content-Type header of the HTTP response.
    // RC += ", Disposed: "+          AResponse->Disposed; //protected Disposed is a read-only property that shows the current state of this object.
    // RC += "; FreeContentStream: " + IntToStr((__int64)AResponse->FreeContentStream);
    // public Specifies whether the stream object for writing the contents of the HTTP response message to the web client is freed when destroying the TDSHTTPResponse class instance.
    // RC += "\t Pragma: " + AResponse->Pragma;             //public  Specifies the pragma field of the HTTP response.
    RC += "; RNo: " + IntToStr(AResponse->ResponseNo);
    // public Represents the response code received in an HTTP response.
    RC += "; RText: " + AResponse->ResponseText; // public Represents the textual message received in an HTTP response.
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TWMDefDM::WebRequestToShortString(TWebRequest * Request)
 {
  UnicodeString RC = "";
  try
   {
    RC += "; Auth: " + Request->Authorization;
    // RC += "\t Content: " + Request->Content;
    // RC += "\t ContentLength: " + Request->ContentLength;
    RC += "; Host: " + Request->Host;
    // RC += "\t InternalPathInfo: " + Request->InternalPathInfo;
    RC += "\t InternalScriptName: " + Request->InternalScriptName;
    // RC += "\t Method: " + Request->Method;
    // RC += "\t PathInfo: " + Request->PathInfo;
    RC += "; PathTrans: " + Request->PathTranslated;
    // RC += "\t ProtocolVersion: " + Request->ProtocolVersion;
    // RC += "\t RawPathInfo: " + Request->RawPathInfo;
    RC += "; rAddr: " + Request->RemoteAddr;
    RC += "; rHost: " + Request->RemoteHost;
    RC += "; rIP: " + Request->RemoteIP;
    // RC += "\t ScriptName: " + Request->ScriptName;
    RC += "; SP: " + IntToStr(Request->ServerPort);
    // RC += "\t Title: " + Request->Title;
    // RC += "\t URL: " + Request->URL;
    // RC += "\t UserAgent: " + Request->UserAgent;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TWMDefDM::WebResponseToString(TWebResponse * AResponse)
 {
  UnicodeString RC = "";
  try
   {
    // RC += "CloseConnection: " + AResponse->CloseConnection; //public Indicates whether the connection should close after sending the response.
    RC += "CLen: " + IntToStr(AResponse->ContentLength); // public Specifies the length of the HTTP response content.
    // RC += "\t ContentStream: " + IntToStr((__int64)AResponse->ContentStream); //public Represents the content of the HTTP response.
    RC += "; CText: " + AResponse->Content; // public Specifies the textual content of the HTTP response.
    RC += "; CType: " + AResponse->ContentType; // public Specifies the Content-Type header of the HTTP response.
    // RC += ", Disposed: "+          AResponse->Disposed; //protected Disposed is a read-only property that shows the current state of this object.
    // RC += "; FreeContentStream: " + IntToStr((__int64)AResponse->FreeContentStream);
    // public Specifies whether the stream object for writing the contents of the HTTP response message to the web client is freed when destroying the TDSHTTPResponse class instance.
    // RC += "\t Pragma: " + AResponse->Pragma;             //public  Specifies the pragma field of the HTTP response.
    RC += "; CEncoding: " + AResponse->ContentEncoding;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TWMDefDM::WebRequestToString(TWebRequest * Request)
 {
  UnicodeString RC = "";
  try
   {
    RC += "TWebRequest Accept: " + Request->Accept;
    RC += "\t Authorization: " + Request->Authorization;
    RC += "\t CacheControl: " + Request->CacheControl;
    RC += "\t Connection: " + Request->Connection;
    RC += "\t Content: " + Request->Content;
    RC += "\t ContentEncoding: " + Request->ContentEncoding;
    RC += "\t ContentFields: " + Request->ContentFields->Text;
    RC += "\t ContentLength: " + IntToStr(Request->ContentLength);
    // RC += ", ContentParser: "+    Request->ContentParser->Name;
    RC += "\t ContentType: " + Request->ContentType;
    RC += "\t ContentVersion: " + Request->ContentVersion;
    RC += "\t Cookie: " + Request->Cookie;
    RC += "\t CookieFields: " + Request->CookieFields->Text;
    RC += "\t Date: " + Request->Date.FormatString("dd.mm.yyyy");
    RC += "\t DerivedFrom: " + Request->DerivedFrom;
    // RC += ", Disposed: "+    Request->Disposed;
    RC += "\t Expires: " + Request->Expires.FormatString("dd.mm.yyyy");
    RC += "\t Files: " + IntToStr(Request->Files->Count);
    RC += "\t From: " + Request->From;
    RC += "\t Host: " + Request->Host;
    RC += "\t IfModifiedSince: " + Request->IfModifiedSince.FormatString("dd.mm.yyyy");
    RC += "\t InternalPathInfo: " + Request->InternalPathInfo;
    RC += "\t InternalScriptName: " + Request->InternalScriptName;
    RC += "\t Method: " + Request->Method;
    RC += "\t MethodType: " + IntToStr((int)Request->MethodType);
    RC += "\t PathInfo: " + Request->PathInfo;
    RC += "\t PathTranslated: " + Request->PathTranslated;
    RC += "\t ProtocolVersion: " + Request->ProtocolVersion;
    RC += "\t Query: " + Request->Query;
    RC += "\t QueryFields: " + Request->QueryFields->Text;
    RC += "\t RawContent: " + Request->RawContent;
    RC += "\t RawPathInfo: " + Request->RawPathInfo;
    RC += "\t Referer: " + Request->Referer;
    RC += "\t RemoteAddr: " + Request->RemoteAddr;
    RC += "\t RemoteHost: " + Request->RemoteHost;
    RC += "\t RemoteIP: " + Request->RemoteIP;
    RC += "\t ScriptName: " + Request->ScriptName;
    RC += "\t ServerPort: " + IntToStr(Request->ServerPort);
    RC += "\t Title: " + Request->Title;
    RC += "\t URL: " + Request->URL;
    RC += "\t UserAgent: " + Request->UserAgent;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TWMDefDM::WebModuleException(TObject * Sender, Sysutils::Exception * E, bool & Handled)
 {
  dsLogError(E->Message, __FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TWMDefDM::WMDefDMMISCommonPatientF63Action(TObject * Sender, TWebRequest * Request,
  TWebResponse * Response, bool & Handled)
 {
  // #############################################################################
  // #                                                                           #
  // #      Экспорт формы №63                                                    #
  // #                                                                           #
  // #############################################################################
  dsLogMessage("Request: " + WebRequestToShortString(Request), __FUNC__, 5);
  Response->SendRedirect("/datasnap/rest/TMISAPI/PatientF63/" + GetRPartE(Request->InternalPathInfo, '/'));
 }
// ---------------------------------------------------------------------------
void __fastcall TWMDefDM::WMDefDMMISCommonImportPatientAction(TObject * Sender, TWebRequest * Request,
  TWebResponse * Response, bool & Handled)
 {
  dsLogMessage("Request: " + WebRequestToShortString(Request), __FUNC__, 5);
  Response->SendRedirect("/datasnap/rest/TMISAPI/ImportPatient/MISCOMMON/" +
    GetRPartB(GetRPartB(GetRPartB(GetRPartB(Request->InternalPathInfo, '/'), '/'), '/'), '/'));
 }
// ---------------------------------------------------------------------------
void __fastcall TWMDefDM::WMDefDMAURAImportPatientAction(TObject * Sender, TWebRequest * Request,
  TWebResponse * Response, bool & Handled)
 {
  dsLogMessage("Request: " + WebRequestToShortString(Request), __FUNC__, 5);
  Response->SendRedirect("/datasnap/rest/TMISAPI/ImportPatient/AURA/" +
    GetRPartB(GetRPartB(GetRPartB(GetRPartB(Request->InternalPathInfo, '/'), '/'), '/'), '/'));
 }
// ---------------------------------------------------------------------------
void __fastcall TWMDefDM::IndexHTMLTag(TObject * Sender, TTag Tag, const UnicodeString TagString, TStrings * TagParams,
  UnicodeString & ReplaceText)
 {
  dsLogMessage("Tag: " + IntToStr(Tag) + " " + TagString + "; TagParams: " + TagParams->Text, __FUNC__, 5);
  if (SameText(TagString, "urlpath"))
   ReplaceText = (Request->InternalScriptName);
  else if (SameText(TagString, "port"))
   ReplaceText = IntToStr(Request->ServerPort);
  else if (SameText(TagString, "host"))
   ReplaceText = (Request->Host);
  else if (SameText(TagString, "loginrequired"))
   {
    if (DSRESTWebDispatcher->AuthenticationManager != NULL)
     ReplaceText = "true";
    else
     ReplaceText = "false";
   }
  else if (SameText(TagString, "serverfunctionsjs"))
   ReplaceText = Request->InternalScriptName + "/js/serverfunctions.js";
  // else if (SameText(TagString, "classname"))
  // ReplaceText = __classid(TSMDefDM)->ClassName();
  else if (SameText(TagString, "servertime"))
   ReplaceText = DateTimeToStr(Now());
  else
   ReplaceText = "";
 }
// ---------------------------------------------------------------------------

