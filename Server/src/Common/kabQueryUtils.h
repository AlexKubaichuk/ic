//---------------------------------------------------------------------------
#ifndef kabQueryUtilsH
#define kabQueryUtilsH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
#include <FireDAC.Comp.Client.hpp>
#include "AppOptions.h"
#include "icsLog.h"
namespace kabQuery
{
class PACKAGE TkabQueryHelper : public TObject
{
private:
  TFDConnection *FConnection;
  TkabLogHelper  *FLogger;
public:
  __fastcall TkabQueryHelper(TFDConnection * AConnection);
  __fastcall ~TkabQueryHelper();

  TFDQuery * __fastcall CreateTempQuery();
  void       __fastcall DeleteTempQuery(TFDQuery * AQuery);

  bool       __fastcall Exec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool       __fastcall ExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
                                  UnicodeString AParam1Name, Variant AParam1Val, UnicodeString ALogComment = "",
                                  UnicodeString AParam2Name = "", Variant AParam2Val = NULL,
                                  UnicodeString AParam3Name = "", Variant AParam3Val = NULL,
                                  UnicodeString AParam4Name = "", Variant AParam4Val = NULL,
                                  UnicodeString AParam5Name = "", Variant AParam5Val = NULL,
                                  UnicodeString AParam6Name = "", Variant AParam6Val = NULL);
  bool       __fastcall ExecParamStream(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
                                        UnicodeString AParam1Name, Variant AParam1Val, UnicodeString ALogComment = "",
                                        UnicodeString AParam2Name = "", TStream * AParam2Val = NULL);
  __int64    __fastcall GetNewCode(UnicodeString TabId);

  __property TkabLogHelper  *Logger = {read = FLogger, write = FLogger};
};
//---------------------------------------------------------------------------
extern PACKAGE UnicodeString __fastcall NewGUID();
extern PACKAGE bool __fastcall ConnectDB(TFDConnection * AConnection, TAppOptions *AOpt, UnicodeString ADBPath, UnicodeString AFunc="", TOnLogMsgErrEvent AErrEvt = NULL, TOnLogBegEndEvent ABegEvt = NULL, TOnLogBegEndEvent AEndEvt = NULL);
extern PACKAGE void __fastcall DisconnectDB(TFDConnection * AConnection, UnicodeString AFunc = "", TOnLogBegEndEvent ABegEvt = NULL, TOnLogBegEndEvent AEndEvt = NULL);
extern PACKAGE UnicodeString __fastcall GetUSRDBName(TAppOptions * AOpt);
extern PACKAGE UnicodeString __fastcall GetKLADRDBName(TAppOptions * AOpt);
}; // end of namespace kabQuery
using namespace kabQuery;
//---------------------------------------------------------------------------
#endif

