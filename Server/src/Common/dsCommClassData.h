//---------------------------------------------------------------------------

#ifndef dsCommClassDataH
#define dsCommClassDataH
//---------------------------------------------------------------------------
#include <FireDAC.Comp.Client.hpp>
#include <System.Classes.hpp>
#include "icsLog.h"
#include <list>
//---------------------------------------------------------------------------
// ��� ������������ ��������������� �����
enum class TPrePlanKind
{
  None,   // �� �����������
  PreVac, // ����������� ����������������
  PostVac // ����������� ����������������
};
//---------------------------------------------------------------------------
typedef struct tagPreTestItem {
  int           Inf;     // ��� �������� <choiceDB name='��������' uid='006E' ref='003A' required='1' inlist='l'/>
  __int64       Test;    // ��� ����� <choiceDB name='�����' uid='006D' ref='002A' required='1' inlist='l'/>
  int /*double*/ Reak;    //  <digit name='�������� �������' uid='006F' required='1' inlist='e' digits='2' min='0' max='99'/>
  int           Actuals; // <digit name='������������ ��������' uid='0070' required='1' inlist='e' digits='3' min='0' max='999' units='��.'/>
  TPrePlanKind  Kind;    // <choice name='�����������' uid='0076' required='1' inlist='e' default='0'>
                         //     <choicevalue name='�� �����������' value='0'/>
                         //     <choicevalue name='����������� ���������������� �����' value='1'/>
                         //     <choicevalue name='����������� ���������������� �����' value='2'/>
  int           Pause;    //<digit name='�����' uid='0072' required='1' inlist='e' digits='2' min='0' max='30' units='��.'>
  UnicodeString MinAge, MaxAge;

  tagPreTestItem() : Inf(-1),
                     Test(-1),
                     Reak(0),
                     Actuals(0),
                     Kind(TPrePlanKind::None),
                     Pause(0),
                     MinAge(""),
                     MaxAge("")
                     {};
} TPreTestItem;
//---------------------------------------------------------------------------
struct gtint
{
  bool operator()(int  v1, int v2) const
  {
    return (v1 > v2);
  }
};
//---------------------------------------------------------------------------
typedef map<int,int, gtint> TGIntMap;
typedef map<int,int> TIntMap;
typedef map<int,UnicodeString> TIntStrMap;
typedef map<int, TIntMap*> TIntListMap;
typedef list<TPreTestItem>      TPreTestItemList;          //������ ��������� ��������/����/��������/�������
//---------------------------------------------------------------------------
class TdsCommClassData : public TObject
{
private:
  int __fastcall FGetVPrior(int ACode);
  int __fastcall FGetRVPrior(int ACode);


  TGIntMap     FVPrior;
  TGIntMap     FRVPrior;

  UnicodeString __fastcall FGetInfName(UnicodeString ACode);
  UnicodeString __fastcall FGetVacName(UnicodeString ACode);
  UnicodeString __fastcall FGetTestName(UnicodeString ACode);
  UnicodeString __fastcall FGetCheckName(UnicodeString ACode);

  TFDQuery * __fastcall CreateTempQuery(TFDConnection * AConnection);
  void __fastcall DeleteTempQuery(TFDQuery * AQuery);

public:
  __fastcall TdsCommClassData(TFDConnection * AConnection);
  __fastcall ~TdsCommClassData();
 UnicodeString __fastcall VacNameStr(__int64 ACode);
 UnicodeString __fastcall InfNameStr(__int64 ACode);

  TAnsiStrMap VacList;
  TAnsiStrMap TestList;
  TAnsiStrMap InfList;
  TAnsiStrMap ShortInfList;
  TAnsiStrMap CheckList;
  TAnsiStrMap ReackList;

  TIntListMap InfVacList;
  TIntListMap InfProbList;

  TIntListMap VacInfList;
  TIntListMap ProbInfList;
  TPreTestItemList PreTestList;

  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool __fastcall InfInVac(UnicodeString AInf, UnicodeString AVac);
  bool __fastcall InfInTest(UnicodeString AInf, UnicodeString ATest);
  bool __fastcall GetMIBPInfList(int AMIBPCode, TIntMap *& ARC);
  int __fastcall  GetMIBPInfCount(int AMIBPCode);
  bool __fastcall GetInfMIBPList(int AInfCode, TIntMap *& ARC);
  __property UnicodeString InfName[UnicodeString ACode] = {read=FGetInfName};
  __property UnicodeString VacName[UnicodeString ACode] = {read=FGetVacName};
  __property UnicodeString TestName[UnicodeString ACode] = {read=FGetTestName};
  __property UnicodeString CheckName[UnicodeString ACode] = {read=FGetCheckName};

  void __fastcall LoadPlanData(TTagNode * APlanOpt);
  __property int VPrior[int ACode] = {read=FGetVPrior};
  __property int RVPrior[int ACode] = {read=FGetRVPrior};
};
//---------------------------------------------------------------------------
#endif

