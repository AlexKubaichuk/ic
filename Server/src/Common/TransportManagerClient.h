#ifndef TransportManagerClientH
#define TransportManagerClientH

#include "Data.DBXCommon.hpp"
#include "System.Classes.hpp"
#include "System.SysUtils.hpp"
#include "Data.DB.hpp"
#include "Data.SqlExpr.hpp"
#include "Data.DBXDBReaders.hpp"
#include "Data.DBXCDSReaders.hpp"

  class TTransportManagerClient : public TObject
  {
  private:
    TDBXConnection *FDBXConnection;
    bool FInstanceOwner;
    TDBXCommand *FGetUpstreamIncomingIdsCommand;
    TDBXCommand *FGetDownstreamIncomingIdsCommand;
    TDBXCommand *FGetIncomingDownstreamIdsCommand;
    TDBXCommand *FGetDownstreamIncomingIdsByPeerCommand;
    TDBXCommand *FSendToUpstreamCommand;
    TDBXCommand *FGetFromUpstreamCommand;
    TDBXCommand *FSendToDownstreamCommand;
    TDBXCommand *FGetFromDownstreamCommand;
    TDBXCommand *FResetStorageCommand;
    TDBXCommand *FRebuildStorageIndexCommand;
  public:
    __fastcall TTransportManagerClient(TDBXConnection *ADBXConnection);
    __fastcall TTransportManagerClient(TDBXConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TTransportManagerClient();
    System::UnicodeString __fastcall GetUpstreamIncomingIds();
    System::UnicodeString __fastcall GetDownstreamIncomingIds();
    System::UnicodeString __fastcall GetIncomingDownstreamIds();
    System::UnicodeString __fastcall GetDownstreamIncomingIdsByPeer(System::UnicodeString PeerId);
    void __fastcall SendToUpstream(System::UnicodeString Id, System::UnicodeString FileName);
    void __fastcall GetFromUpstream(System::UnicodeString Id, System::UnicodeString FileName);
    void __fastcall SendToDownstream(System::UnicodeString Id, System::UnicodeString FileName);
    void __fastcall GetFromDownstream(System::UnicodeString Id, System::UnicodeString FileName);
    void __fastcall ResetStorage();
    void __fastcall RebuildStorageIndex();
  };

#endif
