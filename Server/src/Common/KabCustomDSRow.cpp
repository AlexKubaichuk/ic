//---------------------------------------------------------------------------

#pragma hdrstop

#include "KabCustomDSRow.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------
//###########################################################################
//#                                                                         #
//#                     TkabCustomDataSetRow                                #
//#                                                                         #
//###########################################################################
//---------------------------------------------------------------------------

__fastcall EkabCustomDataSetRowError::EkabCustomDataSetRowError(UnicodeString Msg)
  : Exception(Msg)
{
}
//---------------------------------------------------------------------------
__fastcall TkabCustomDataSetRow::TkabCustomDataSetRow(TTagNode *AFlDef)
{
  NewRec(AFlDef);
}
//---------------------------------------------------------------------------
__fastcall TkabCustomDataSetRow::TkabCustomDataSetRow(TTagNode *AFlDef, TkabCustomDataSetRow *ASrc)
{
  NewRec(AFlDef);
  TTagNode *itFl = AFlDef->GetFirstChild();
  while (itFl)
   {
     if (itFl->CmpName("code,data"))
       FSetValue(itFl->Name, (ASrc)? ASrc->Value[itFl->Name] : Variant::Empty());
     else
      {
        FSetValue(itFl->AV["uid"], (ASrc)? ASrc->Value[itFl->AV["uid"]] : Variant::Empty());
        if (itFl->CmpName("binary,choice,choiceDB,extedit,choiceTree"))
         FSetStrValue(itFl->AV["uid"], (ASrc)? ASrc->StrValue[itFl->AV["uid"]] : Variant::Empty());
      }
     itFl = itFl->GetNext();
   }
}
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSetRow::NewRec(TTagNode *AFlDef)
{
  FFetched = false;
  FDataFields    = new TkabCustomDataSetFieldsMap;
  FStrDataFields = new TkabCustomDataSetFieldsMap;
  TTagNode *itFl = AFlDef->GetFirstChild();
  while (itFl)
   {
     if (itFl->CmpName("code,data"))
      (*FDataFields)[itFl->Name.UpperCase()] = Variant::Empty();
     else
      {
        (*FDataFields)[itFl->AV["uid"].UpperCase()] = Variant::Empty();
        if (itFl->CmpName("binary,choice,choiceDB,extedit,choiceTree"))
         (*FStrDataFields)[itFl->AV["uid"].UpperCase()] = Variant::Empty();
      }
     itFl = itFl->GetNext();
   }
}
//---------------------------------------------------------------------------
__fastcall TkabCustomDataSetRow::~TkabCustomDataSetRow()
{
  delete FDataFields;
  delete FStrDataFields;
}
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSetRow::FSetValue(UnicodeString AName, const Variant &AVal)
{
  try
   {
     TkabCustomDataSetFieldsMap::iterator FRC = FDataFields->find(AName.UpperCase());
     if (FRC != FDataFields->end())
      FRC->second = AVal;
     else
      throw EkabCustomDataSetRowError("field '"+AName+"' not found.");
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
Variant __fastcall TkabCustomDataSetRow::FGetValue(UnicodeString AName)
{
  Variant RC = Variant::Empty();
  try
   {
     TkabCustomDataSetFieldsMap::iterator FRC = FDataFields->find(AName.UpperCase());
     if (FRC != FDataFields->end())
      RC = FRC->second;
     else
      throw EkabCustomDataSetRowError("field '"+AName+"' not found.");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSetRow::FSetStrValue(UnicodeString AName, const Variant &AVal)
{
  try
   {
     TkabCustomDataSetFieldsMap::iterator FRC = FStrDataFields->find(AName.UpperCase());
     if (FRC != FStrDataFields->end())
      FRC->second = AVal;
     else
      throw EkabCustomDataSetRowError("field '"+AName+"' not found.");
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
Variant __fastcall TkabCustomDataSetRow::FGetStrValue(UnicodeString AName)
{
  Variant RC;
  try
   {
     TkabCustomDataSetFieldsMap::iterator FRC = FStrDataFields->find(AName.UpperCase());
     if (FRC != FStrDataFields->end())
      RC = FRC->second;
     else
      throw EkabCustomDataSetRowError("field '"+AName+"' not found.");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------


