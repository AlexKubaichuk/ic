// ---------------------------------------------------------------------------
#include <vcl.h>
#include <SysUtils.hpp>
#include <stdio.h>
#pragma hdrstop
#include "dsBackGroundExecutor.h"
//#include "dsDocSrvConstDef.h"
//#include "ExtUtils.h"
//#include "ICSDATEUTIL.hpp"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                     TExecutor::TBackgroundExecuter                  ##
// ##                                                                       ##
// ###########################################################################
class TdsExecuter::TBackgroundExecuter : public TThread
 {
 private:
  TdsExecuter * FDC;
 protected:
  virtual void __fastcall Execute();
 public:
  __fastcall TBackgroundExecuter(bool CreateSuspended, TdsExecuter * ADC);
  void __fastcall ThreadSafeCall(TdcSyncMethod Method);
 };
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                     TExecutor::TBackgroundExecuter                  ##
// ##                                                                       ##
// ###########################################################################
__fastcall TdsExecuter::TBackgroundExecuter::TBackgroundExecuter(bool CreateSuspended, TdsExecuter * ADC)
  : TThread(CreateSuspended), FDC(ADC)
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::TBackgroundExecuter::Execute()
 {
  CoInitialize(NULL);
  FDC->ExecuteProc();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::TBackgroundExecuter::ThreadSafeCall(TdcSyncMethod Method)
 {
  Synchronize(Method);
 }
// ---------------------------------------------------------------------------
__fastcall TdsExecuter::TdsExecuter()
 {
  fdRepl << rfReplaceAll << rfIgnoreCase;
  FBkgCreator = NULL;
  FErrMsgNode = NULL;
 }
// ---------------------------------------------------------------------------
__fastcall TdsExecuter::~TdsExecuter()
 {
  if (FBkgCreator)
   {
    FBkgCreator->Terminate();
    delete FBkgCreator;
   }
  FBkgCreator = NULL;
  if (FErrMsgNode)
   delete FErrMsgNode;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::SyncCall(TdcSyncMethod AMethod)
 {
  if (FBkgCreator)
   FBkgCreator->ThreadSafeCall(AMethod);
  else
   AMethod();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::SyncCallLogMsg()
 {
  if (FOnLogMessage)
   FOnLogMessage(FLogMsg, FLogFuncName, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogMsg);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::SyncCallLogErr()
 {
  if (FOnLogError)
   FOnLogError(FLogMsg, FLogFuncName, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FErrMsgNode->AddChild("err")->AV["PCDATA"] = AMessage;
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogErr);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::SyncCallLogSQL()
 {
  if (FOnLogSQL)
   FOnLogSQL(FLogMsg, FLogFuncName, FLogExecTime, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  FLogMsg      = ASQL;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  FLogExecTime = AExecTime;
  SyncCall(& SyncCallLogSQL);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::SyncCallLogBegin()
 {
  if (FOnLogBegin)
   FOnLogBegin(FLogFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::LogBegin(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogBegin);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::SyncCallLogEnd()
 {
  if (FOnLogEnd)
   FOnLogEnd(FLogFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::LogEnd(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogEnd);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::SyncCallOnProgress()
 {
  if (FOnProgress)
   FOnProgress(FPrType, FPrVal, FPrMsg);
 };
// ---------------------------------------------------------------------------
void __fastcall TdsExecuter::FSetProgress(TkabProgressType AType, int AVal, UnicodeString AMsg)
 {
  FPrType = AType;
  FPrVal  = AVal;
  FPrMsg  = AMsg;
  SyncCall(& SyncCallOnProgress);
 };
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsExecuter::DateFS(TDateTime ADate)
 {
  UnicodeString RC = ADate.FormatString("dd.mm.yyyy");
  try
   {
    /* if (FOnGetDateStringFormat)
     {
     FOnGetDateStringFormat(RC);
     RC = ADate.FormatString(RC);
     } */
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsExecuter::ExecuteProc()
 {
  LogBegin(__FUNC__);
  bool RC = false;
  try
   {
   }
  __finally
   {
   }
  LogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsExecuter::StartExecute()
 {
  UnicodeString RC = "error";
  try
   {
    if (FBkgCreator)
     delete FBkgCreator;
    FBkgCreator = new TBackgroundExecuter(false, this);
    RC          = "ok";
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------

