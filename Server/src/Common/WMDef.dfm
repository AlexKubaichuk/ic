object WMDefDM: TWMDefDM
  OldCreateOrder = False
  OnCreate = WebModuleCreate
  Actions = <
    item
      Name = 'ServerFunctionInvokerAction'
      PathInfo = '/ServerFunctionInvoker'
      Producer = ServerFunctionInvoker
    end
    item
      Default = True
      Name = 'DefaultAction'
      PathInfo = '/'
      OnAction = WebModuleDefaultAction
    end
    item
      Name = 'MISCommonImportPatient'
      PathInfo = '/mis/common/ImportPatient/*'
      OnAction = WMDefDMMISCommonImportPatientAction
    end
    item
      Name = 'MISCommonPatientF63'
      PathInfo = '/mis/common/PatientF63/*'
      OnAction = WMDefDMMISCommonPatientF63Action
    end
    item
      Name = 'AURAImportPatient'
      PathInfo = '/mis/aura/ImportPatient/*'
      OnAction = WMDefDMAURAImportPatientAction
    end
    item
      Name = 'AURAPatientF63'
      PathInfo = '/mis/aura/PatientF63/*'
      OnAction = WMDefDMMISCommonPatientF63Action
    end
    item
      Name = 'IndexAction'
      PathInfo = '/Index'
      Producer = Index
    end>
  BeforeDispatch = WebModuleBeforeDispatch
  OnException = WebModuleException
  Height = 333
  Width = 414
  object DSRESTWebDispatcher: TDSRESTWebDispatcher
    SessionTimeout = 86400000
    OnHTTPTrace = DSRESTWebDispatcherHTTPTrace
    WebDispatch.PathInfo = 'datasnap*'
    Left = 68
    Top = 29
  end
  object ServerFunctionInvoker: TPageProducer
    HTMLFile = 'Templates\ServerFunctionInvoker.html'
    OnHTMLTag = ServerFunctionInvokerHTMLTag
    Left = 56
    Top = 165
  end
  object WebFileDispatcher: TWebFileDispatcher
    WebFileExtensions = <
      item
        MimeType = 'text/css'
        Extensions = 'css'
      end
      item
        MimeType = 'text/javascript'
        Extensions = 'js'
      end
      item
        MimeType = 'image/x-png'
        Extensions = 'png'
      end
      item
        MimeType = 'text/html'
        Extensions = 'htm;html'
      end
      item
        MimeType = 'image/jpeg'
        Extensions = 'jpg;jpeg;jpe'
      end
      item
        MimeType = 'image/gif'
        Extensions = 'gif'
      end
      item
        MimeType = 'text/xml'
        Extensions = 'xml'
      end
      item
        MimeType = 'application/x-zip-compressed'
        Extensions = 'zip'
      end
      item
        MimeType = 'application/x-7z-compressed'
        Extensions = '7z'
      end
      item
        MimeType = 'application/x-zip-compressed'
        Extensions = 'zxml'
      end
      item
        MimeType = 'application/x-msdos-program'
        Extensions = 'exe'
      end>
    BeforeDispatch = WebFileDispatcherBeforeDispatch
    WebDirectories = <
      item
        DirectoryAction = dirInclude
        DirectoryMask = '*'
      end
      item
        DirectoryAction = dirExclude
        DirectoryMask = '\templates\*'
      end>
    RootDirectory = '.'
    Left = 65
    Top = 87
  end
  object DSProxyGenerator: TDSProxyGenerator
    ExcludeClasses = 'DSMetadata'
    MetaDataProvider = DSServerMetaData
    Writer = 'Java Script REST'
    Left = 302
    Top = 30
  end
  object DSServerMetaData: TDSServerMetaDataProvider
    Left = 296
    Top = 162
  end
  object DSProxyDispatcher: TDSProxyDispatcher
    DSProxyGenerator = DSProxyGenerator
    Left = 298
    Top = 92
  end
  object Index: TPageProducer
    HTMLFile = 'templates\index.html'
    OnHTMLTag = IndexHTMLTag
    Left = 56
    Top = 237
  end
end
