//---------------------------------------------------------------------------

#ifndef DataMFH
#define DataMFH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Vcl.Controls.hpp>
//#include <Vcl.StdCtrls.hpp>
//#include <Vcl.Forms.hpp>
#include "pgmsetting.h"
#include "icsLog.h"
//---------------------------------------------------------------------------
enum class TdsKeyStatus
{
ksOk,
ksKeyNotFound, // ��� ���� �����������
ksKeyNotValidPrId, // ��� ���� ������������, �� �� ��������� �� ������ ��������� � �����
ksKeyNotValidVerCommReadVer, // ������ ������ ������
ksKeyNotValidVer, // ������ � ����� < ������ �����, ������ ����
ksKeyNotValidVerReadVer, // �� ������ ��������� ������ �� �����
ksKeyNotValidVerIntVer, // ���������� ������ �� ������������� ������ �����
ksKeyNotValidVerKeyCode, // ��� ����� �� ��������
} ;
class PACKAGE EdsGrdError : public System::Sysutils::Exception
{
public:
     __fastcall EdsGrdError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class TDataM : public TDataModule
{
__published:
 void __fastcall DataModuleDestroy(TObject *Sender);
private:	// User declarations
  UnicodeString FMsg;
  UnicodeString FRCCode;
  TdsKeyStatus FRC;
  int FCCode;
  int __fastcall PreVer(int AVer);
#ifdef GUARDANT
    int __fastcall GetG(DWORD AMode, DWORD AFlags, DWORD AProg, DWORD AID, DWORD ASN, DWORD AVer, DWORD AMask, DWORD AType, DWORD AModels, DWORD AInterfaces);
    uint32_t dwID;
#endif
public:		// User declarations
#ifdef GUARDANT
    UnicodeString  __fastcall EnCode(const AnsiString  *AMsg, const int &ALen);
    CGrdDongle GKey;
    int Ver1, Ver2;
#endif
    __fastcall TDataM(TComponent* Owner);
  void __fastcall chkey();

    __property UnicodeString RC = {read=FMsg};
    __property UnicodeString RCCode = {read=FRCCode};
    __property TdsKeyStatus ECode = {read=FRC};
    __property int CCode = {read=FCCode};
};
//---------------------------------------------------------------------------
//extern PACKAGE TDataM *DataM;
//---------------------------------------------------------------------------
#endif

