//---------------------------------------------------------------------------

#ifndef dsUsersUnitH
#define dsUsersUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <DataSnap.DSServer.hpp>
//#include <Datasnap.DSProviderDataModuleAdapter.hpp>
//---------------------------------------------------------------------------
#include <Data.DB.hpp>


//#include <IPPeerClient.hpp>


#include "dsKLADRUnit.h"
#include "dsOrgUnit.h"
#include "dsEIDataDMUnit.h"
#include "dsSrvRegUnit.h"
#include "SVRClientClassesUnit.h"

#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <IPPeerClient.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>

//---------------------------------------------------------------------------
class DECLSPEC_DRTTI TdsAdminClass : public TDataModule
{
__published:	// IDE-managed Components
 TFDConnection *IcConnection;
 TFDTransaction *FDTransaction1;
 TDSRestConnection *DocConnnect;
 void __fastcall DataModuleDestroy(TObject *Sender);
private:	// User declarations
  TdsSrvRegistry  *FRegComp;
  TTagNode        *FDefXML;
  TdsEIDataDM     *FDM;
  TdsKLADRClass   *FKLADRComp;
  TdsOrgClass     *FOrgComp;
  TdsDocClassClient *FDocClient;
  UnicodeString   FUploadData;


  System::UnicodeString GetMainLPUParam();
  UnicodeString FGetAddrStr(UnicodeString ACode);
  UnicodeString FGetOrgStr(UnicodeString ACode);
  TJSONObject*  FGetPatCardDataById(UnicodeString ARecId, TJSONObject* AClsRC);
  bool          FAddCardData(UnicodeString ACardId, UnicodeString ACardPtId, UnicodeString ARecId, TJSONObject* ARC, TJSONObject* AClsRC);

public:		// User declarations
  __fastcall TdsAdminClass(TComponent* Owner, __int64 APort = 5100/*, TFDConnection *AConnection*/);
  __fastcall ~TdsEIDataClass();
  void Disconnect();
  __property TdsKLADRClass   *KLADRComp = { read = FKLADRComp };
  __property TdsOrgClass     *OrgComp   = { read = FOrgComp };

  UnicodeString GetDefXML();
  UnicodeString GetClassXML(UnicodeString ARef);
  UnicodeString GetClassZIPXML(UnicodeString ARef);
  bool          SetClassZIPXML(UnicodeString ARef);
  __int64       GetCount(UnicodeString AId, UnicodeString AFilterParam);
  TJSONObject*  GetIdList(UnicodeString AId, int AMax, UnicodeString AFilterParam);
  TJSONObject*  GetValById(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  GetValById10(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  Find(UnicodeString AId, UnicodeString AFindData);
  UnicodeString ImportData(UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);

  bool          UploadData(UnicodeString ADataPart, int AType);

  System::UnicodeString GetExportDataHeader();
  UnicodeString GetPatDataById(UnicodeString ARecId);
  UnicodeString GetPatCardDataById(UnicodeString ARecId, TJSONObject* AClsRC);
};
//---------------------------------------------------------------------------
//extern PACKAGE TdsICClass *dsICClass;
//---------------------------------------------------------------------------
#endif
