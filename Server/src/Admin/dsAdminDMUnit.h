//---------------------------------------------------------------------------

#ifndef dsAdminDMUnitH
#define dsAdminDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Vcl.ExtCtrls.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.VCLUI.Wait.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <Datasnap.DSHTTP.hpp>
#include <System.NetEncoding.hpp>
#include <System.JSON.hpp>
//---------------------------------------------------------------------------
#include "dsKLADRUnit.h"
#include "XMLContainer.h"
#include "OrgParsers.h"
#include "icsLog.h"
#include "dsAdminTypes.h"
//---------------------------------------------------------------------------
typedef void __fastcall(__closure * TSesClearEvent)(UnicodeString  ASesId);
//---------------------------------------------------------------------------
class TdsAdminDM : public TDataModule
 {

 __published: //IDE-managed Components
  TFDConnection *  DBCheckConnection;
 private: //User declarations
  TFDConnection * FConnection;
  //TdsKLADRClass *FKLADR;
  TTagNode * FDefXML;

  TIntLPUMap   FLPUMap;
  TIntUserMap  FUserMap;
  TStrUserMap  FNameUserMap;
  TStrUserMap  FSysUserMap;
  TIntDBMap    FDBMap;
  TIntRightMap FRightMap;

  __int64 __fastcall GetNewCode(UnicodeString  TabId);
  int __fastcall FGetUC();
  UnicodeString __fastcall NewGUID();
  TAxeXMLContainer * FXMLList;
  void __fastcall AddRt(TTagNode * ARoot, bool AVal, UnicodeString  AFlName);
  UnicodeString FAdmUser, FAdmUserPass;
  TAppOptions *FOpt;

  TFDQuery * __fastcall CreateDBCheckQuery();
  TFDQuery * __fastcall CreateTempQuery();
  bool __fastcall ConnectToDB(UnicodeString  ADBName);
  void __fastcall DeleteTempQuery(TFDQuery * AQuery);
  bool __fastcall quExec(TFDQuery * AQuery, bool ACommit, UnicodeString  ASQL, UnicodeString  ALogComment = "");
  bool __fastcall quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString  ASQL,
      UnicodeString  AParam1Name, Variant AParam1Val,
      UnicodeString  ALogComment = "",
      UnicodeString  AParam2Name = "", Variant AParam2Val = 0,
      UnicodeString  AParam3Name = "", Variant AParam3Val = 0,
      UnicodeString  AParam4Name = "", Variant AParam4Val = 0,
      UnicodeString  AParam5Name = "", Variant AParam5Val = 0);

 public: //User declarations
  __fastcall TdsAdminDM(TComponent * Owner, TFDConnection * AConnection, TAxeXMLContainer *AXMLList, TAppOptions *AOpt);
  __fastcall ~TdsAdminDM();

  void __fastcall DBList(TStrings *AList);
  void __fastcall PreloadData();
  void __fastcall UpdateLPUList(UnicodeString  ACode);
  UnicodeString __fastcall FindUser(UnicodeString  AUser, UnicodeString  APass);
  UnicodeString __fastcall FindSysUser(UnicodeString  AUser, UnicodeString  APass);
  UnicodeString __fastcall GetUserRight(TDSSession * ASes, UnicodeString ADBId, UnicodeString & AICDB, UnicodeString & ADocDB, UnicodeString & AVSDB, UnicodeString & AIntLPUCode, bool ASysUser = false);
  void __fastcall CheckUpdate();
  __property int UserCount =  {read = FGetUC};
  __property TTagNode * DefXML =   {read = FDefXML, write = FDefXML};
  __property TAxeXMLContainer * XMLList =   {read = FXMLList};
  __property UnicodeString User =   {read = FAdmUser, write = FAdmUser};
  __property UnicodeString Pwd =   {read = FAdmUserPass, write = FAdmUserPass};
 };
//---------------------------------------------------------------------------
extern PACKAGE TdsAdminDM * dsAdminDM;
//---------------------------------------------------------------------------
#endif
