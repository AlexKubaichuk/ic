object dsAdminClass: TdsAdminClass
  OldCreateOrder = False
  Height = 262
  Width = 495
  object IcConnection: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'User_Name=sysdba'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'Port=3050'
      'Server=localhost'
      'Pooled=False')
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    FormatOptions.AssignedValues = [fvFmtDisplayDateTime, fvFmtDisplayDate, fvFmtDisplayTime]
    FormatOptions.FmtDisplayDateTime = 'dd.mm.yyyy hh:nn:ss'
    FormatOptions.FmtDisplayDate = 'dd.mm.yyyy'
    FormatOptions.FmtDisplayTime = 'hh:nn:ss'
    ResourceOptions.AssignedValues = [rvDirectExecute, rvAutoConnect, rvSilentMode]
    ResourceOptions.DirectExecute = True
    ResourceOptions.SilentMode = True
    ResourceOptions.AutoConnect = False
    TxOptions.AutoStart = False
    TxOptions.AutoStop = False
    TxOptions.DisconnectAction = xdRollback
    LoginPrompt = False
    Left = 29
    Top = 14
  end
  object CheckConnection: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'User_Name=sysdba'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'Port=3050'
      'Server=localhost'
      'Pooled=False')
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    FormatOptions.AssignedValues = [fvFmtDisplayDateTime, fvFmtDisplayDate, fvFmtDisplayTime]
    FormatOptions.FmtDisplayDateTime = 'dd.mm.yyyy hh:nn:ss'
    FormatOptions.FmtDisplayDate = 'dd.mm.yyyy'
    FormatOptions.FmtDisplayTime = 'hh:nn:ss'
    ResourceOptions.AssignedValues = [rvDirectExecute, rvAutoConnect, rvSilentMode]
    ResourceOptions.DirectExecute = True
    ResourceOptions.SilentMode = True
    ResourceOptions.AutoConnect = False
    TxOptions.AutoStart = False
    TxOptions.AutoStop = False
    TxOptions.DisconnectAction = xdRollback
    LoginPrompt = False
    Left = 125
    Top = 14
  end
  object MigrateScript: TFDScript
    SQLScripts = <>
    Connection = CheckConnection
    Transaction = trMigrate
    ScriptOptions.BreakOnError = True
    ScriptOptions.TrimConsole = True
    ScriptOptions.Verify = True
    ScriptOptions.FileEncoding = ecUTF8
    ScriptOptions.CharacterSet = 'UTF8'
    ScriptOptions.SQLDialect = 3
    ScriptOptions.RaisePLSQLErrors = True
    Params = <>
    Macros = <>
    FetchOptions.AssignedValues = [evAutoClose]
    OnConsolePut = MigrateScriptConsolePut
    OnProgress = MigrateScriptProgress
    Left = 232
    Top = 16
  end
  object trMigrate: TFDTransaction
    Connection = CheckConnection
    Left = 232
    Top = 72
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Console'
    Left = 328
    Top = 16
  end
end
