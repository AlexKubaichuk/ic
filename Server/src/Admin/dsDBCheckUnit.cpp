﻿// ---------------------------------------------------------------------------
// #include <vcl.h>
#pragma hdrstop
#include "dsDBCheckUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "System.Classes.TPersistent"
#pragma resource "*.dfm"
// ---------------------------------------------------------------------------
__fastcall TdsDBCheckClass::TdsDBCheckClass(TComponent * Owner) : TDataModule(Owner)
 {
 }
// ---------------------------------------------------------------------------
__fastcall TdsDBCheckClass::~TdsDBCheckClass()
 {
 }
// ---------------------------------------------------------------------------
UnicodeString TdsDBCheckClass::CheckConnect(UnicodeString ADB, UnicodeString AServer, int APort, UnicodeString AUser,
  UnicodeString APass)
 {
  UnicodeString RC = "error";
  try
   {
    try
     {
      if (CheckConnection->Connected)
       CheckConnection->Connected = false;
      Sleep(50);
      CheckConnection->DriverName = "FB";
      CheckConnection->Params->Clear();
      CheckConnection->Params->Add("DriverID=FB");
      CheckConnection->Params->Add("Database=" + ADB);
      CheckConnection->Params->Add("CharacterSet=UTF8");
      CheckConnection->Params->Add("User_Name=" + AUser);
      CheckConnection->Params->Add("Port="+IntToStr(APort));
      CheckConnection->Params->Add("Server=" + AServer);
      CheckConnection->Params->Add("Password=" + APass);
      CheckConnection->Params->Add("Pooled=False");
      CheckConnection->Connected = true;
      Sleep(50);
      RC = "ok";
     }
    catch (Exception & E)
     {
      RC = E.Message;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
