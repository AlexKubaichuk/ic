﻿// ---------------------------------------------------------------------------
// #include <vcl.h>
#pragma hdrstop
#include "dsAdminUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "System.Classes.TPersistent"
#define _REGGUI_ "57E3D087-D0CFEF0F-1A84"
#pragma resource "*.dfm"
// TRegClassDM *RegClassDM;
// ---------------------------------------------------------------------------
__fastcall TdsVersion::TdsVersion(UnicodeString AVer)
 {
  FV1 = 0;
  FV2 = 0;
  FV3 = 0;
  FV4 = 0;
  if (AVer.Trim().Length())
   FSetStrVer(AVer.Trim());
 }
// ---------------------------------------------------------------------------
__fastcall TdsVersion::~TdsVersion()
 {
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsVersion::VerBE(UnicodeString AVer)
 {
  bool RC = false;
  TdsVersion * FVer = new TdsVersion(AVer);
  try
   {
    RC = (FV1 > FVer->V1);
    if (!RC && (FV1 == FVer->V1))
     {
      RC = (FV2 > FVer->V2);
      if (!RC && (FV2 == FVer->V2))
       {
        RC = (FV3 > FVer->V3);
        if (!RC && (FV3 == FVer->V3))
         RC = (FV4 >= FVer->V4);
       }
     }
   }
  __finally
   {
    delete FVer;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsVersion::FSetStrVer(UnicodeString AVer)
 {
  UnicodeString itVer = AVer;
  if (!itVer.Pos("."))
   FV1 = itVer.ToIntDef(0);
  else
   {
    FV1   = GetLPartB(itVer, '.').ToIntDef(0);
    itVer = GetRPartB(itVer, '.');
    if (!itVer.Pos("."))
     FV2 = itVer.ToIntDef(0);
    else
     {
      FV2   = GetLPartB(itVer, '.').ToIntDef(0);
      itVer = GetRPartB(itVer, '.');
      if (!itVer.Pos("."))
       FV3 = itVer.ToIntDef(0);
      else
       {
        FV3   = GetLPartB(itVer, '.').ToIntDef(0);
        itVer = GetRPartB(itVer, '.');
        if (!itVer.Pos("."))
         FV4 = itVer.ToIntDef(0);
        else
         FV4 = GetLPartB(itVer, '.').ToIntDef(0);
       }
     }
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsVersion::FGetStrVer()
 {
  return IntToStr(FV1) + "." + IntToStr(FV2) + "." + IntToStr(FV3) + "." + IntToStr(FV4);
 }
// ---------------------------------------------------------------------------
const UnicodeString ScriptOuputKindStr[] =
 {
  "soSeparator",
  "soEcho",
  "soScript",
  "soInfo",
  "soError",
  "soConnect",
  "soServerOutput",
  "soUserOutput",
  "soCommand",
  "soMacro",
  "soData",
  "soParam"
 };
__fastcall TdsAdminClass::TdsAdminClass(TComponent * Owner, TAxeXMLContainer * AXMLList, TAppOptions * AOpt,
  bool AUpdate) : TDataModule(Owner)
 {
  dsLogC(__FUNC__);
  try
   {
    FOpt = AOpt;
    if (AUpdate)
     CheckAndUpdateVersion(AXMLList->GetXML("version"));
    ConnectDB(IcConnection, AOpt, GetUSRDBName(AOpt), __FUNC__);
    FDM = new TdsAdminDM(this, IcConnection, AXMLList, AOpt);
    FDM->CheckUpdate();
    FDM->PreloadData();
    FRegComp    = new TdsSrvRegistry(this, NULL, IcConnection, FDM->XMLList, AOpt, _REGGUI_, "");
    FDM->DefXML = FDM->XMLList->GetXML(_REGGUI_);
    FDefXML     = FDM->DefXML;
    FRegComp->SetRegDef(FDefXML);
    FKLADRComp             = new TdsKLADRClass(this, IcConnection, AOpt);
    FRegComp->OnGetAddrStr = FGetAddrStr;
    FRegComp->KLADR        = FKLADRComp;
   }
  catch (Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogC(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
__fastcall TdsAdminClass::~TdsAdminClass()
 {
  dsLogD(__FUNC__);
  DisconnectDB(IcConnection, __FUNC__);
  Sleep(50);
  delete FRegComp;
  delete FDM;
  delete FKLADRComp;
  dsLogD(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
void TdsAdminClass::Disconnect()
 {
  // FRegComp->Disconnect();
 }
// ---------------------------------------------------------------------------
__int64 TdsAdminClass::GetCount(UnicodeString AId, UnicodeString AFilterParam)
 {
  if (AFilterParam.Length())
   {
    dsLogMessage("GetCount: ID: " + AId + ", filter: " + AFilterParam);
   }
  else
   {
    dsLogMessage("GetCount: ID: " + AId);
   }
  __int64 RC = 0;
  try
   {
    try
     {
      RC = FRegComp->GetCount(GetRPartE(AId).Trim().UpperCase(), AFilterParam);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsAdminClass::GetIdList(UnicodeString AId, int AMax, UnicodeString AFilterParam)
 {
  if (AFilterParam.Length())
   {
    dsLogMessage("GetIdList: ID: " + AId + ", Max: " + IntToStr(AMax) + ", filter: " + AFilterParam, "", 5);
   }
  else
   {
    dsLogMessage("GetIdList: ID: " + AId + ", Max: " + IntToStr(AMax), "", 5);
   }
  TJSONObject * RC;
  try
   {
    try
     {
      RC = FRegComp->GetIdList(GetRPartE(AId).Trim().UpperCase(), AMax, AFilterParam);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsAdminClass::GetValById(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogMessage("GetValById: ID: " + AId + ", RecId: " + ARecId);
  TJSONObject * RC;
  try
   {
    try
     {
      RC = FRegComp->GetValById(GetRPartB(AId).Trim(), ARecId);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsAdminClass::GetValById10(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogMessage("GetValById10: ID: " + AId + ", RecId: " + ARecId);
  TJSONObject * RC;
  try
   {
    try
     {
      RC = FRegComp->GetValById10(GetRPartB(AId).Trim(), ARecId);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsAdminClass::Find(UnicodeString AId, UnicodeString AFindData)
 {
  dsLogMessage("Find: ID: " + AId + ", FindParam: " + AFindData);
  TJSONObject * RC;
  try
   {
    try
     {
      RC = FRegComp->Find(GetRPartE(AId).Trim().UpperCase(), AFindData);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsAdminClass::FindEx(UnicodeString AId, UnicodeString AFindData)
 {
  dsLogMessage("FindEx: ID: " + AId + ", FindData: " + AFindData);
  TJSONObject * RC;
  try
   {
    try
     {
      // RC = FDM->FindEx(AId, AFindData);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsAdminClass::InsertData(UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecId)
 {
  dsLogMessage("InsertData: ID: " + AId + ", Value: " + AValue->ToString());
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FRegComp->InsertData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId);
      dsRequestLogMessage(AValue->ToString(), "Insert data class" + GetRPartE(AId).Trim().UpperCase());
      FDM->PreloadData();
      if (GetRPartE(AId).Trim().UpperCase() == "0008")
       {
        FDM->UpdateLPUList(ARecId);
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsAdminClass::EditData(UnicodeString AId, TJSONObject * AValue, System::UnicodeString & ARecId)
 {
  dsLogMessage("EditData: ID: " + AId + ", Value: " + AValue->ToString());
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FRegComp->EditData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId);
      dsRequestLogMessage(AValue->ToString(), "Edit data class" + GetRPartE(AId).Trim().UpperCase());
      FDM->PreloadData();
      if (GetRPartE(AId).Trim().UpperCase() == "0008")
       {
        FDM->UpdateLPUList(ARecId);
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsAdminClass::DeleteData(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogMessage("DeleteData: ID: " + AId + ", RecId: " + ARecId);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FRegComp->DeleteData(GetRPartE(AId).Trim().UpperCase(), ARecId);
      FDM->PreloadData();
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsAdminClass::GetDefXML()
 {
  dsLogMessage("GetDefXML");
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDefXML->AsXML;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsAdminClass::GetClassXML(UnicodeString ARef)
 {
  dsLogMessage("GetClassXML: ID: " + ARef);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->XMLList->GetXML(ARef)->AsXML;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsAdminClass::GetClassZIPXML(UnicodeString ARef)
 {
  dsLogMessage("GetClassZIPXML: ID: " + ARef);
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FDM->XMLList->GetXML(ARef)->AsXML;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
bool TdsAdminClass::UploadData(UnicodeString ADataPart, int AType)
 {
  bool RC = false;
  try
   {
    try
     {
      if (!AType)
       {
        FUploadData = "";
       }
      else
       {
        if (AType)
         {
          FUploadData += ADataPart;
         }
       }
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
bool TdsAdminClass::SetClassZIPXML(UnicodeString ARef)
 {
  dsLogMessage("SetClassZIPXML: ID: " + ARef);
  bool RC = false;
  TTagNode * def = new TTagNode;
  try
   {
    try
     {
      def->Encoding = "utf-8";
      UnicodeString xmlPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\" + ARef + ".zxml");
      def->AsXML = FUploadData;
      def->SaveToZIPXMLFile(xmlPath);
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete def;
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString TdsAdminClass::FGetAddrStr(UnicodeString ACode, unsigned int AParams)
 {
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FKLADRComp->AddrCodeToText(ACode, AParams /* "11111111" */);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool TdsAdminClass::CheckUser(UnicodeString AUser, UnicodeString APasswd, bool & AValid)
 {
  bool RC = false;
  AValid = RC;
  try
   {
    if (!AValid)
     {
      if (!FDM->FindUser(AUser, "").Length())
       {
        AValid = (FDM->FindUser(AUser, APasswd) == APasswd);
       }
     }
    RC = AValid;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool TdsAdminClass::CheckRTVal(TTagNode * ADef, UnicodeString AId)
 {
  bool RC = false;
  try
   {
    TTagNode * FRT = ADef->GetTagByUID(AId);
    if (FRT)
     {
      RC = FRT->AV["v"].ToIntDef(0);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void TdsAdminClass::SetUserRT(TTagNode * ART, UnicodeString ASesName, TDateTime AStartDate, bool AAdm)
 {
  if (AAdm)
   {
    FModulesRT[ASesName + "_TdsAdminClass"] = AStartDate;
   }
  else
   {
    FModulesRT[ASesName + "_TdsICClass"]       = AStartDate;
    FModulesRT[ASesName + "_TMISAPI"]          = AStartDate;
    FModulesRT[ASesName + "_TdsDocClass"]      = AStartDate;
    FModulesRT[ASesName + "_TdsVacStoreClass"] = AStartDate;
    // Редактирование' uid='0036'>
    if (CheckRTVal(ART, "0036"))
     FModulesRT[ASesName + "_TdsEIDataClass"] = AStartDate;
    FModulesRT[ASesName + "_TdsOrgClass"] = AStartDate;
   }
  FModulesRT[ASesName + "_TdsKLADRClass"] = AStartDate;
  // Доступ к УИ-ЛПУ' uid='003B'/>
  if (CheckRTVal(ART, "003B"))
   FCommRT[ASesName + "_003B"] = AStartDate;
  // Регистратура' uid='002A'>
  if (CheckRTVal(ART, "002A"))
   FCommRT[ASesName + "_002A"] = AStartDate;
  // Доступ к списку пациентов' uid='0033'>
  if (CheckRTVal(ART, "0033"))
   FCommRT[ASesName + "_0033"] = AStartDate;
  // Редактирование' uid='0036'>
  if (CheckRTVal(ART, "0036"))
   FCommRT[ASesName + "_0036"] = AStartDate;
  // Доступ к справочникам' uid='0037'>
  if (CheckRTVal(ART, "0037"))
   FCommRT[ASesName + "_0037"] = AStartDate;
  // Редактирование' uid='0038'>
  if (CheckRTVal(ART, "0038"))
   FCommRT[ASesName + "_0038"] = AStartDate;
  // Доступ к имм. карте' uid='002B'>
  if (CheckRTVal(ART, "002B"))
   FCommRT[ASesName + "_002B"] = AStartDate;
  // Редактирование' uid='0039'>
  if (CheckRTVal(ART, "0039"))
   FCommRT[ASesName + "_0039"] = AStartDate;
  // Доступ к списку планов' uid='002C'>
  if (CheckRTVal(ART, "002C"))
   FCommRT[ASesName + "_002C"] = AStartDate;
  // Выполнение планирования' uid='003A'>
  if (CheckRTVal(ART, "003A"))
   FCommRT[ASesName + "_003A"] = AStartDate;
  // Доступ к УИ-Документооборот' uid='003C'/>
  if (CheckRTVal(ART, "003C"))
   FCommRT[ASesName + "_003C"] = AStartDate;
  // Доступ к списку Описания документов' uid='002E'>
  if (CheckRTVal(ART, "002E"))
   FCommRT[ASesName + "_002E"] = AStartDate;
  // Редактирование' uid='003D'>
  if (CheckRTVal(ART, "003D"))
   FCommRT[ASesName + "_003D"] = AStartDate;
  // Доступ к списку Документы' uid='002D'>
  if (CheckRTVal(ART, "002D"))
   FCommRT[ASesName + "_002D"] = AStartDate;
  // Редактирование' uid='003E'>
  if (CheckRTVal(ART, "003E"))
   FCommRT[ASesName + "_003E"] = AStartDate;
  // Доступ к списку Фильтры' uid='002F'>
  if (CheckRTVal(ART, "002F"))
   FCommRT[ASesName + "_002F"] = AStartDate;
  // Редактирование' uid='003F'>
  if (CheckRTVal(ART, "003F"))
   FCommRT[ASesName + "_003F"] = AStartDate;
  // Склад МИБП' uid='0029'/>
  if (CheckRTVal(ART, "0029"))
   FCommRT[ASesName + "_0029"] = AStartDate;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsAdminClass::LoginSysUser(UnicodeString ABId)
 {
  UnicodeString RC = "<error/>";
  TTagNode * FRT = NULL;
  try
   {
    try
     {
      TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
      if (FSes)
       {
        UnicodeString SesName = FSes->SessionName;
        // dsLogMessage(">>>SES>>> " + FSes->UserRoles->Text, __FUNC__);
        UnicodeString ICDB, DocDB, VSDB, IntLPUCode;
        if (FSes->UserRoles->Count < 2)
         {
          if (FDM->UserCount)
           {
            ICDB       = "";
            DocDB      = "";
            VSDB       = "";
            IntLPUCode = "";
            RC         = FDM->GetUserRight(FSes, ABId, ICDB, DocDB, VSDB, IntLPUCode, true);
            FRT        = new TTagNode;
            try
             {
              FRT->AsXML = RC;
              if (FRT->Count == 1)
               { // у пользователя только одна БД
                SetUserRT(FRT, SesName, FSes->StartDateTime, false);
                FSes->UserRoles->Add("ID=" + FRT->GetFirstChild()->AV["id"]);
                FSes->UserRoles->Add("SYS");
                FSes->UserRoles->Add("LPUCode=" + IntLPUCode);
                FSes->UserRoles->Add("ICDB=" + ICDB);
                FSes->UserRoles->Add("DOCDB=" + DocDB);
                FSes->UserRoles->Add("VSDB=" + VSDB);
                // dsLogAuth("[LoginUser] >>> " + FSes->SessionName);
               }
             }
            catch (Exception & E)
             {
              RC = "<error/>";
              dsLogError(E.Message, __FUNC__);
             }
           }
          else
           {
            FRT = new TTagNode;
            try
             {
              FRT->AsXML =
                "<rt>" "<db id='admin' name='admin'>" "<r uid='003B' v='1'/>" "<r uid='002A' v='1'/>"
                "<r uid='0033' v='1'/>" "<r uid='0036' v='1'/>" "<r uid='0037' v='1'/>" "<r uid='0038' v='1'/>"
                "<r uid='002B' v='1'/>" "<r uid='0039' v='1'/>" "<r uid='002C' v='1'/>" "<r uid='003A' v='1'/>"
                "<r uid='003C' v='1'/>" "<r uid='002E' v='1'/>" "<r uid='003D' v='1'/>" "<r uid='002D' v='1'/>"
                "<r uid='003E' v='1'/>" "<r uid='002F' v='1'/>" "<r uid='003F' v='1'/>" "<r uid='0029' v='1'/>" "</db>"
                "</rt>";
              SetUserRT(FRT, SesName, FSes->StartDateTime, true);
              FSes->UserRoles->Add("ID=nouser");
              FSes->UserRoles->Add("SYS");
              FSes->UserRoles->Add("LPUCode=");
              FSes->UserRoles->Add("ICDB=nouser");
              FSes->UserRoles->Add("DOCDB=nouser");
              FSes->UserRoles->Add("VSDB=nouser");
              RC = FRT->AsXML;
             }
            catch (Exception & E)
             {
              dsLogError(E.Message, __FUNC__);
             }
           }
         }
        else
         {
          if (FDM->UserCount)
           {
            RC = FDM->GetUserRight(FSes, ABId, ICDB, DocDB, VSDB, IntLPUCode, true);
           }
         }
        // dsLogMessage(">>>SES>>> " + FSes->UserRoles->Text, __FUNC__);
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    if (FRT)
     {
      delete FRT;
     }
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsAdminClass::LoginUser(UnicodeString AId)
 {
  UnicodeString RC = "<error/>";
  TTagNode * FRT = NULL;
  try
   {
    try
     {
      TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
      if (FSes)
       {
        if (AId.LowerCase() == "exit")
         {
          TDSSession * FCloseSes;
          bool UserExist = false;
          TStringList * OpenSesList = new TStringList;
          try
           {
            TDSSessionManager::Instance->GetOpenSessionKeys(OpenSesList);
            for (int i = 0; (i < OpenSesList->Count) && !UserExist; i++)
             {
              FCloseSes = TDSSessionManager::Instance->Session[OpenSesList->Strings[i]];
              if (FCloseSes->Id != FSes->Id)
               UserExist = (FCloseSes->UserName.UpperCase() == FSes->UserName.UpperCase())
                 && FCloseSes->UserRoles->Values["ID"].Trim().Length();
              if (UserExist)
               {
                FCloseSes->Close();
                FCloseSes->Terminate();
                if (FSesClose)
                 {
                  dsLogAuth("[LogoutUser] " + FCloseSes->SessionName);
                  FSesClose(FCloseSes->SessionName);
                 }
               }
             }
           }
          __finally
           {
            delete OpenSesList;
           }
         }
        UnicodeString SesName = FSes->SessionName;
        // dsLogMessage(">>>SES>>> " + FSes->UserRoles->Text, __FUNC__);
        UnicodeString ICDB, DocDB, VSDB, IntLPUCode;
        if (FSes->UserRoles->Count < 2)
         {
          if (FDM->UserCount)
           {
            ICDB  = "";
            DocDB = "";
            VSDB  = "";
            RC    = FDM->GetUserRight(FSes, AId, ICDB, DocDB, VSDB, IntLPUCode);
            FRT   = new TTagNode;
            try
             {
              FRT->AsXML = RC;
              if (!FRT->CmpName("user_exist") && (FRT->Count == 1))
               { // у пользователя только одна БД
                SetUserRT(FRT, SesName, FSes->StartDateTime, false);
                FSes->UserRoles->Add("ID=" + FRT->GetFirstChild()->AV["id"]);
                FSes->UserRoles->Add("LPUCode=" + IntLPUCode);
                FSes->UserRoles->Add("ICDB=" + ICDB);
                FSes->UserRoles->Add("DOCDB=" + DocDB);
                FSes->UserRoles->Add("VSDB=" + VSDB);
                dsLogAuth("[LoginUser]  >> " + FSes->SessionName);
               }
             }
            catch (Exception & E)
             {
              RC = "<error/>";
              dsLogError(E.Message, __FUNC__);
             }
           }
          else
           { // юзверей нет, заходим под админом
            FRT = new TTagNode;
            try
             {
              FRT->AsXML =
                "<rt>" "<db id='admin' name='admin'>" "<r uid='003B' v='1'/>" "<r uid='002A' v='1'/>"
                "<r uid='0033' v='1'/>" "<r uid='0036' v='1'/>" "<r uid='0037' v='1'/>" "<r uid='0038' v='1'/>"
                "<r uid='002B' v='1'/>" "<r uid='0039' v='1'/>" "<r uid='002C' v='1'/>" "<r uid='003A' v='1'/>"
                "<r uid='003C' v='1'/>" "<r uid='002E' v='1'/>" "<r uid='003D' v='1'/>" "<r uid='002D' v='1'/>"
                "<r uid='003E' v='1'/>" "<r uid='002F' v='1'/>" "<r uid='003F' v='1'/>" "<r uid='0029' v='1'/>" "</db>"
                "</rt>";
              SetUserRT(FRT, SesName, FSes->StartDateTime, true);
              FSes->UserRoles->Add("ID=nouser");
              FSes->UserRoles->Add("LPUCode=");
              FSes->UserRoles->Add("ICDB=nouser");
              FSes->UserRoles->Add("DOCDB=nouser");
              FSes->UserRoles->Add("VSDB=nouser");
              RC = FRT->AsXML;
             }
            catch (Exception & E)
             {
              dsLogError(E.Message, __FUNC__);
             }
           }
         }
        else
         {
          if (FDM->UserCount)
           {
            UnicodeString ICDB, DocDB, VSDB;
            RC = FDM->GetUserRight(FSes, AId, ICDB, DocDB, VSDB, IntLPUCode);
           }
         }
        // dsLogMessage(">>>SES>>> " + FSes->UserRoles->Text, __FUNC__);
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    if (FRT)
     {
      delete FRT;
     }
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
void TdsAdminClass::Logout()
 {
  if (FSesClose)
   {
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes)
     {
      dsLogAuth("[LogoutUser] " + FSes->SessionName);
      FSesClose(FSes->SessionName);
     }
   }
 }
// ----------------------------------------------------------------------------
bool TdsAdminClass::CheckRT(UnicodeString AURT, UnicodeString AFunc)
 {
  bool RC = false;
  try
   {
    // ShowMessage(AFunc);
    if (FDM->UserCount)
     {
      UnicodeString FClass = GetLPartB(AFunc, '.');
      UnicodeString FFunc = GetRPartB(AFunc, '.');
      if (!RC)
       {
        TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
        if (FSes)
         {
          // dsLogMessage(FSes->UserRoles->Text, __FUNC__);
          // dsLogMessage("SU: "+FSes->UserName+" AU: "+User+" PAU: "+Pwd, __FUNC__);
          if ((FSes->UserRoles->Values["UPS"] == Pwd) && (FSes->UserName == User) && ((FClass == "TdsAdminClass") ||
            (FClass == "TdsKLADRClass")))
           {
            RC = true;
           }
          else if ((FClass == "TdsAdminClass") && (FFunc == "Logout"))
           {
            RC = true;
           }
          else
           {
            // ShowMessage(FSes->UserName+" === "+FSes->UserRoles->Values["UPS"]);
            if (!FDM->FindSysUser(FSes->UserName, FSes->UserRoles->Values["UPS"]).Length())
             {
              // ShowMessage("LoginUser");
              LoginUser("");
             }
            TStrDateMap::iterator FRC = FModulesRT.find(FSes->SessionName + "_" + FClass);
            RC = (FRC != FModulesRT.end());
            // if ] = FSes->StartDateTime;
            // FCommRT
           }
         }
       }
     }
    else
     {
      RC = true;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsAdminClass::FGetAdmUser()
 {
  if (FDM)
   {
    return FDM->User;
   }
  else
   {
    return "";
   }
 }
// ----------------------------------------------------------------------------
void __fastcall TdsAdminClass::FSetAdmUser(UnicodeString AVal)
 {
  if (FDM)
   {
    FDM->User = AVal;
   }
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsAdminClass::FGetAdmUserPass()
 {
  if (FDM)
   {
    return FDM->Pwd;
   }
  else
   {
    return "";
   }
 }
// ----------------------------------------------------------------------------
void __fastcall TdsAdminClass::FSetAdmUserPass(UnicodeString AVal)
 {
  if (FDM)
   {
    FDM->Pwd = AVal;
   }
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsAdminClass::FindAddr(UnicodeString AStr, UnicodeString ADefAddr, int AParams)
 {
  TJSONObject * RC = NULL;
  try
   {
    try
     {
      RC = FKLADRComp->Find(AStr, ADefAddr, AParams);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString TdsAdminClass::AddrCodeToText(UnicodeString ACode, int AParams)
 {
  return FGetAddrStr(ACode, AParams);
 }
// ----------------------------------------------------------------------------
void __fastcall TdsAdminClass::HLogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsLogMessage(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsAdminClass::HLogBegin(UnicodeString AFuncName)
 {
  dsLogBegin(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsAdminClass::HLogEnd(UnicodeString AFuncName)
 {
  dsLogEnd(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsAdminClass::HLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsLogError(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsAdminClass::HLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  dsLogSQL(ASQL, AFuncName, AExecTime, ALvl);
 }
// ---------------------------------------------------------------------------
void TdsAdminClass::CheckAndUpdateVersion(TTagNode * VerXML)
 {
  dsLogBegin(__FUNC__);
  TkabQueryHelper * QH = NULL;
  TFDQuery * FQ = NULL;
  try
   {
    UnicodeString KLADRDB = GetKLADRDBName(FOpt);
    UnicodeString UsrDB = GetUSRDBName(FOpt);
    TTagNode * BaseVer = VerXML->GetChildByName("base");
    TTagNode * DocVer = VerXML->GetChildByName("doc");
    TTagNode * SVVer = VerXML->GetChildByName("sv");
    TTagNode * UsrVer = VerXML->GetChildByName("usr");
    TTagNode * KLADRVer = VerXML->GetChildByName("kladr");
    CheckAndUpdateBaseVersion(KLADRVer, KLADRDB);
    if (CheckAndUpdateBaseVersion(UsrVer, UsrDB))
     { // , TAppOptions *AOpt
      ConnectDB(IcConnection, FOpt, UsrDB, __FUNC__);
      QH                       = new TkabQueryHelper(IcConnection);
      QH->Logger->OnLogMessage = HLogMessage;
      QH->Logger->OnLogError   = HLogError;
      QH->Logger->OnLogSQL     = HLogSQL;
      QH->Logger->OnLogBegin   = HLogBegin;
      QH->Logger->OnLogEnd     = HLogEnd;
      FQ                       = QH->CreateTempQuery();
      QH->Exec(FQ, false, "Select * from class_0008"); // список БД
      while (!FQ->Eof)
       {
        CheckAndUpdateBaseVersion(BaseVer, icsPrePath(FQ->FieldByName("r0011")->AsString));
        CheckAndUpdateBaseVersion(DocVer, icsPrePath(FQ->FieldByName("r001D")->AsString));
        CheckAndUpdateDataVersion(icsPrePath(FQ->FieldByName("r0011")->AsString));
        FQ->Next();
       }
     }
   }
  __finally
   {
    if (QH)
     {
      if (FQ)
       {
        QH->DeleteTempQuery(FQ);
       }
      delete QH;
     }
    DisconnectDB(IcConnection, __FUNC__);
   }
  dsLogEnd(__FUNC__);
 }
// ----------------------------------------------------------------------------
bool TdsAdminClass::CheckAndUpdateBaseVersion(TTagNode * AVerDef, UnicodeString ADB)
 {
  bool RC = false;
  dsLogBegin(__FUNC__);
  TTagNode * itNode;
  UnicodeString OldVer;
  try
   {
    if (AVerDef)
     {
      dsLogMessage("Migrate database: " + ADB);
      // , TAppOptions *AOpt
      ConnectDB(CheckConnection, FOpt, ADB, __FUNC__);
      TkabQueryHelper * QH = new TkabQueryHelper(CheckConnection);
      QH->Logger->OnLogMessage = HLogMessage;
      QH->Logger->OnLogError   = HLogError;
      QH->Logger->OnLogSQL     = HLogSQL;
      QH->Logger->OnLogBegin   = HLogBegin;
      QH->Logger->OnLogEnd     = HLogEnd;
      TFDQuery * FQ = QH->CreateTempQuery();
      try
       {
        QH->Exec(FQ, false, "Select * from DBVER order by actualdate desc, verval desc");
        // список БД
        if (FQ->RecordCount)
         {
          dsLogMessage("Migrate database: DB ver: '" + FQ->FieldByName("verval")->AsString + "',  new ver: '" +
            AVerDef->AV["ver"] + "'");
          if (!AVerDef->CmpAV("ver", FQ->FieldByName("verval")->AsString))
           { // версии не совпадают
            TTagNode * itMigrate = AVerDef->GetChildByAV("it", "ver", FQ->FieldByName("verval")->AsString);
            while (itMigrate)
             {
              OldVer    = itMigrate->AV["ver"];
              itMigrate = itMigrate->GetNext();
              if (itMigrate)
               {
                dsLogMessage("Migrate database: " + OldVer + " -> " + itMigrate->AV["ver"] + "; script: " +
                  itMigrate->AV["migrations"]);
                MigrateDB(CheckConnection, ADB, OldVer, itMigrate);
               }
             }
            RC = true;
           }
          else
           {
            RC = true;
           }
         }
       }
      __finally
       {
        QH->DeleteTempQuery(FQ);
        delete QH;
        DisconnectDB(CheckConnection, __FUNC__);
       }
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
bool TdsAdminClass::CheckAndUpdateDataVersion(UnicodeString ADB)
 {
  bool RC = false;
  dsLogBegin(__FUNC__);
  TTagNode * NewVer = new TTagNode;
  UnicodeString CurPath = ExtractFilePath(ParamStr(0)) + "\\migrations\\";
  UnicodeString DataVer = icsPrePath(CurPath + "dataver.xml");
  UnicodeString SQLScript = icsPrePath(CurPath + "update.sql");
  try
   {
    if (FileExists(DataVer) && FileExists(SQLScript))
     {
      NewVer->LoadFromXMLFile(DataVer);
      ConnectDB(CheckConnection, FOpt, ADB, __FUNC__);
      TkabQueryHelper * QH = new TkabQueryHelper(CheckConnection);
      QH->Logger->OnLogMessage = HLogMessage;
      QH->Logger->OnLogError   = HLogError;
      QH->Logger->OnLogSQL     = HLogSQL;
      QH->Logger->OnLogBegin   = HLogBegin;
      QH->Logger->OnLogEnd     = HLogEnd;
      TFDQuery * FQ = QH->CreateTempQuery();
      TdsVersion * CurAppVer = NULL;
      try
       {
        QH->Exec(FQ, false, "Select val from aoptions where upper(name)='DATAVER'");
        UnicodeString CurDataVer = Date().FormatString("yyyy.mm.dd");
        if (FQ->RecordCount)
         CurDataVer = FQ->FieldByName("val")->AsString;
        QH->Exec(FQ, false, "Select * from DBVER order by actualdate desc, verval desc");
        UnicodeString AppVer = "";
        if (FQ->RecordCount)
         AppVer = FQ->FieldByName("verval")->AsString;
        if (AppVer.Length())
         {
          CurAppVer = new TdsVersion(AppVer);
          if (CurAppVer->VerBE(NewVer->AV["app"]))
           {
            if (!NewVer->CmpAV("data", CurDataVer))
             {
              dsLogMessage("Migrate database: DB data ver: '" + CurDataVer + "',  new data ver: '" +
                NewVer->AV["data"] + "'");
              DisconnectDB(CheckConnection, __FUNC__);
              if (!CopyFile(ADB.c_str(), (ADB + "." + CurDataVer).c_str(), false))
               {
                dsLogError("Ошибка копирования " + ADB + " -> " + ADB + "." + CurDataVer, __FUNC__);
               }
              MigrateError = false;
              try
               {
                MigrateScript->SQLScriptFileName = SQLScript;
                if (MigrateScript->ValidateAll())
                 {
                  MigrateScript->ExecuteAll();
                 }
                else
                 {
                  dsLogError("Ошибка при проверке миграции: " + CurDataVer, __FUNC__);
                 }
                if (MigrateError)
                 {
                  DisconnectDB(CheckConnection, __FUNC__);
                  if (!CopyFile((ADB + "." + CurDataVer).c_str(), ADB.c_str(), false))
                   {
                    dsLogError("Ошибка копирования " + ADB + "." + CurDataVer + " -> " + ADB, __FUNC__);
                   }
                 }
               }
              catch (Exception & E)
               {
                dsLogError(CurDataVer + ": " + E.Message, __FUNC__);
               }
             }
           }
          else
           dsLogError("Версия базы данных меньше требуемой: текущая:" + AppVer + ", минимальная: " + NewVer->AV["app"],
            __FUNC__);
         }
       }
      __finally
       {
        QH->DeleteTempQuery(FQ);
        delete QH;
        DisconnectDB(CheckConnection, __FUNC__);
        if (CurAppVer)
         delete CurAppVer;
       }
     }
   }
  __finally
   {
    delete NewVer;
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
bool TdsAdminClass::MigrateDB(TFDConnection * AConnection, UnicodeString ADB, UnicodeString AOldDBVer,
  TTagNode * AVerDef)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  TTagNode * itNode;
  try
   {
    if (AVerDef)
     {
      UnicodeString SQLScript = ExtractFilePath(ParamStr(0)) + "\\migrations\\";
      SQLScript = icsPrePath(SQLScript + AVerDef->AV["migrations"] + ".sql");
      DisconnectDB(CheckConnection, __FUNC__);
      if (!CopyFile(ADB.c_str(), (ADB + "." + AOldDBVer).c_str(), false))
       {
        dsLogError("Ошибка копирования " + ADB + " -> " + ADB + "." + AOldDBVer, __FUNC__);
       }
      MigrateError = false;
      try
       {
        MigrateScript->SQLScriptFileName = SQLScript;
        if (MigrateScript->ValidateAll())
         {
          MigrateScript->ExecuteAll();
         }
        else
         {
          dsLogError("Ошибка при проверке миграции: " + AVerDef->AV["migrations"], __FUNC__);
         }
        if (MigrateError)
         {
          DisconnectDB(CheckConnection, __FUNC__);
          if (!CopyFile((ADB + "." + AOldDBVer).c_str(), ADB.c_str(), false))
           {
            dsLogError("Ошибка копирования " + ADB + "." + AOldDBVer + " -> " + ADB, __FUNC__);
           }
         }
       }
      catch (Exception & E)
       {
        dsLogError(AVerDef->AV["migrations"] + ": " + E.Message, __FUNC__);
       }
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsAdminClass::ScriptOuputKindToString(TFDScriptOuputKind AKind)
 {
  return ScriptOuputKindStr[AKind];
 }
// ---------------------------------------------------------------------------
void __fastcall TdsAdminClass::MigrateScriptConsolePut(TFDScript * AEngine, const UnicodeString AMessage,
  TFDScriptOuputKind AKind)
 {
  if (AKind == soError)
   {
    MigrateError = true;
   }
  dsLogMessage("MigrateScriptConsolePut: " + ScriptOuputKindToString(AKind) + ": " + AMessage);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsAdminClass::MigrateScriptProgress(TObject * Sender)
 {
  dsLogMessage("MigrateScriptProgress");
 }
// ---------------------------------------------------------------------------
UnicodeString TdsAdminClass::GetUpdateCode()
 {
  UnicodeString RC = "";
  TBase64Encoding * B64Encode = new TBase64Encoding;
  TTagNode * dbDef = new TTagNode;
  TStringList * DBList = new TStringList;
  TTagNode * itNode;
  TkabQueryHelper * QH = new TkabQueryHelper(CheckConnection);
  QH->Logger->OnLogMessage = HLogMessage;
  QH->Logger->OnLogError   = HLogError;
  QH->Logger->OnLogSQL     = HLogSQL;
  QH->Logger->OnLogBegin   = HLogBegin;
  QH->Logger->OnLogEnd     = HLogEnd;
  TStringStream * FStm = new TStringStream;
  TStringStream * FRCStm = new TStringStream;
  try
   {
    dbDef->Name = "dbdef";
    FDM->DBList(DBList);
    for (int i = 0; i < DBList->Count; i++)
     {
      try
       { // , TAppOptions *AOpt
        ConnectDB(CheckConnection, FOpt, GetLPartB(DBList->Strings[i], '@'), __FUNC__);
        TFDQuery * FQ = QH->CreateTempQuery();
        try
         {
          itNode            = dbDef->AddChild("db");
          itNode->AV["id"]  = icsNewGUID().UpperCase();
          itNode->AV["def"] = DBList->Strings[i];
          QH->Exec(FQ, true, "Update or Insert into aoptions VALUES ('40381E23-92155860-4448', '" + itNode->AV["id"] +
            "')", "Check");
          QH->Exec(FQ, false, "Select Count(*) as RCOUNT from class_1000", "Check");
          itNode->AV["ch"] = FQ->FieldByName("RCOUNT")->AsString;
          QH->Exec(FQ, false, "Select Count(*) as RCOUNT from class_3003", "Check");
          itNode->AV["ch"] = itNode->AV["ch"] + "/" + FQ->FieldByName("RCOUNT")->AsString;
          QH->Exec(FQ, false, "Select Count(*) as RCOUNT from class_102f", "Check");
          itNode->AV["ch"] = itNode->AV["ch"] + "/" + FQ->FieldByName("RCOUNT")->AsString;
          QH->Exec(FQ, false, "Select Count(*) as RCOUNT from class_3030", "Check");
          itNode->AV["ch"] = itNode->AV["ch"] + "/" + FQ->FieldByName("RCOUNT")->AsString;
         }
        __finally
         {
          QH->DeleteTempQuery(FQ);
          DisconnectDB(CheckConnection, __FUNC__);
         }
       }
      catch (...)
       {
       }
     }
    /*
     dbDef->GetZIPXML(FStm);
     FStm->Seek(0, 0);
     B64Encode->Encode(FStm, FRCStm);
     FRCStm->Seek(0, 0);
     UnicodeString RRC = FRCStm->DataString;
     RC = "\n---base64 1 --------------------------------------\n";
     RC += RRC;
     RC += "\n---base64 2 --------------------------------------\n";
     RRC = B64Encode->Encode(RRC);
     RC += RRC;
     RC += "\n---base64 2  after Decode -------------------------------\n";
     RRC = B64Encode->Decode(RRC);
     RC += RRC;
     RC += "\n---base64 1  after Decode -------------------------------\n";

     FStm->Clear();
     FRCStm->Clear();

     FStm->WriteString(RRC);
     FStm->Seek(0, 0);
     B64Encode->Decode(FStm, FRCStm);
     FRCStm->Seek(0, 0);
     dbDef->SetZIPXML(FRCStm);
     RRC = dbDef->AsXML;

     RC += RRC;
     */ dbDef->GetZIPXML(FStm);
    FStm->Seek(0, 0);
    B64Encode->Encode(FStm, FRCStm);
    FRCStm->Seek(0, 0);
    RC = FRCStm->DataString;
   }
  __finally
   {
    delete B64Encode;
    delete dbDef;
    delete DBList;
    delete QH;
    delete FStm;
    delete FRCStm;
   }
  return RC;
 }
// ---------------------------------------------------------------------------

