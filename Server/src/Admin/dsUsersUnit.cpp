﻿//---------------------------------------------------------------------------
//#include <vcl.h>
#pragma hdrstop

#include "dsUsersUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "System.Classes.TPersistent"

#pragma resource "*.dfm"

//TRegClassDM *RegClassDM;
//---------------------------------------------------------------------------
__fastcall TdsAdminClass::TdsAdminClass(TComponent* Owner, __int64 APort/*, TFDConnection *AConnection*/)
  : TDataModule(Owner)
{
  try
   {
     if (!IcConnection->Connected) IcConnection->Connected = true;
     FDM = new TdsEIDataDM(this, IcConnection);

     FRegComp = new TdsSrvRegistry(this, IcConnection/*AConnection*/,FDM->XMLList, "40381E23-92155860-4448","");
     FDM->DefXML = FDM->XMLList->GetXML("40381E23-92155860-4448");

     FDefXML = FDM->DefXML;
     FRegComp->SetRegDef(FDefXML);

     FKLADRComp = new TdsKLADRClass(NULL/*, IcConnection*/);
     FOrgComp   = new TdsOrgClass(NULL/*, IcConnection*/);
     FRegComp->OnGetAddrStr = FGetAddrStr;
     FRegComp->OnGetOrgStr  = FGetOrgStr;
     FRegComp->KLADR = FKLADRComp;

     DocConnnect->Port = APort;
     FDocClient = new TdsDocClassClient(DocConnnect);
   }
  catch(Exception &E)
   {
     dsLogError(E.Message, __FUNC__);
   }
}
//---------------------------------------------------------------------------
__fastcall TdsEIDataClass::~TdsAdminClass()
{
  if (IcConnection->Connected) IcConnection->Connected = false;
  Sleep(50);
  delete FRegComp;
  delete FDM;
  delete FKLADRComp;
  delete FOrgComp;
}
//---------------------------------------------------------------------------
void TdsAdminClass::Disconnect()
{
//  FRegComp->Disconnect();
}
//---------------------------------------------------------------------------
__int64 TdsAdminClass::GetCount(UnicodeString AId, UnicodeString AFilterParam)
{
  if (AFilterParam.Length())
   dsLogMessage("GetCount: ID: "+AId+", filter: "+AFilterParam);
  else
   dsLogMessage("GetCount: ID: "+AId);
  __int64 RC = 0;
  try
   {
     try
      {
        RC = FRegComp->GetCount(GetRPartE(AId).Trim().UpperCase(), AFilterParam);
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
   }
  return RC;
}
//----------------------------------------------------------------------------
TJSONObject* TdsAdminClass::GetIdList(UnicodeString AId, int AMax, UnicodeString AFilterParam)
{
  if (AFilterParam.Length())
   dsLogMessage("GetIdList: ID: "+ AId+", Max: "+IntToStr(AMax)+", filter: "+AFilterParam,"", 5);
  else
   dsLogMessage("GetIdList: ID: "+ AId+", Max: "+IntToStr(AMax),"", 5);
  TJSONObject* RC;
  try
   {
     try
      {
        RC = FRegComp->GetIdList(GetRPartE(AId).Trim().UpperCase(), AMax, AFilterParam);
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
   }
  return RC;
}
//----------------------------------------------------------------------------
TJSONObject* TdsAdminClass::GetValById(UnicodeString AId, UnicodeString ARecId)
{
  dsLogMessage("GetValById: ID: "+ AId+", RecId: "+ARecId);
  TJSONObject* RC;
  try
   {
     try
      {
         RC = FRegComp->GetValById(GetRPartB(AId).Trim(), ARecId);
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
   }
  return RC;
}
//----------------------------------------------------------------------------
TJSONObject* TdsAdminClass::GetValById10(UnicodeString AId, UnicodeString ARecId)
{
  dsLogMessage("GetValById10: ID: "+ AId+", RecId: "+ARecId);
  TJSONObject* RC;
  try
   {
     try
      {
        RC = FRegComp->GetValById10(GetRPartB(AId).Trim(), ARecId);
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
   }
  return RC;
}
//----------------------------------------------------------------------------
TJSONObject* TdsAdminClass::Find(UnicodeString AId, UnicodeString AFindData)
{
  dsLogMessage("Find: ID: "+ AId+", FindParam: "+AFindData);
  TJSONObject* RC;
  try
   {
     try
      {
        RC = FRegComp->Find(GetRPartE(AId).Trim().UpperCase(), AFindData);
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
   }
  return RC;
}
//----------------------------------------------------------------------------
System::UnicodeString TdsAdminClass::GetDefXML()
{
  dsLogMessage("GetDefXML");
  UnicodeString RC = "";
  try
   {
     try
      {
        RC = FDefXML->AsXML;
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
   }
  return RC;
}
//----------------------------------------------------------------------------
System::UnicodeString TdsAdminClass::GetClassXML(UnicodeString ARef)
{
  dsLogMessage("GetClassXML: ID: "+ ARef);
  UnicodeString RC = "";
  TTagNode *def = new TTagNode;
  try
   {
     try
      {
        def->Encoding = "utf-8";
        UnicodeString xmlPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\"+ARef+".xml");
        def->LoadFromXMLFile(xmlPath);
        RC = def->AsXML;
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
     delete def;
   }
  return RC;
}
//----------------------------------------------------------------------------
System::UnicodeString TdsAdminClass::GetClassZIPXML(UnicodeString ARef)
{
  dsLogMessage("GetClassZIPXML: ID: "+ ARef);
  UnicodeString RC = "";
  TTagNode *def = new TTagNode;
  try
   {
     try
      {
        def->Encoding = "utf-8";
        UnicodeString xmlPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\"+ARef+".zxml");
        def->LoadFromZIPXMLFile(xmlPath);
        RC = def->AsXML;
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
     delete def;
   }
  return RC;
}
//----------------------------------------------------------------------------
bool TdsAdminClass::UploadData(UnicodeString ADataPart, int AType)
{
  bool RC = false;
  try
   {
     try
      {
        if (!AType)
         FUploadData = "";
        else if (AType)
         FUploadData += ADataPart;
        RC = true;
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
   }
  return RC;
}
//----------------------------------------------------------------------------
bool TdsAdminClass::SetClassZIPXML(UnicodeString ARef)
{
  dsLogMessage("SetClassZIPXML: ID: "+ ARef);
  bool RC = false;
  TTagNode *def = new TTagNode;
  try
   {
     try
      {
        def->Encoding = "utf-8";
        UnicodeString xmlPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\defxml\\"+ARef+".zxml");
        def->AsXML = FUploadData;
        def->SaveToZIPXMLFile(xmlPath);
        RC = true;
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
     delete def;
   }
  return RC;
}
//----------------------------------------------------------------------------
UnicodeString TdsAdminClass::FGetAddrStr(UnicodeString ACode)
{
  UnicodeString RC = "";
  try
   {
     try
      {
        RC = FKLADRComp->AddrCodeToText(ACode, "11111111");
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString TdsAdminClass::FGetOrgStr(UnicodeString ACode)
{
  UnicodeString RC = "";
  try
   {
     try
      {
        RC = FOrgComp->OrgCodeToText(ACode);
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
System::UnicodeString TdsAdminClass::GetMainLPUParam()
{
  return FDocClient->GetMainLPUParam();
}
//---------------------------------------------------------------------------
System::UnicodeString TdsAdminClass::GetExportDataHeader()
{
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  TTagNode *def = new TTagNode;
  try
   {
     try
      {

        def->Encoding = "utf-8";
        def->Name = "imm-ei";
        TTagNode *Pass = def->AddChild("pasport");
        def->AddChild("content");
        Pass->AsXML = GetMainLPUParam();
        Pass->AV["gui"]   = icsNewGUID().UpperCase();
        Pass->AV["ver"]   = "700";
        RC = def->AsXML;
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
     delete def;
   }
  dsLogEnd(__FUNC__);
  return RC;
}
//----------------------------------------------------------------------------
void __fastcall TdsAdminClass::DataModuleDestroy(TObject *Sender)
{
  delete FDocClient;
}
//---------------------------------------------------------------------------
UnicodeString TdsAdminClass::GetPatDataById(UnicodeString ARecId)
{
  dsLogMessage("RecId: "+ARecId, __FUNC__);
  UnicodeString RC;
  TJSONObject* JRC = new TJSONObject;
  TJSONObject* ClsRC = new TJSONObject;
  try
   {
     try
      {
        JRC->AddPair("pt", FRegComp->GetImportValById("1000", ARecId, ClsRC));
        JRC->AddPair("cd", FGetPatCardDataById(ARecId, ClsRC));
        JRC->AddPair("cl", ClsRC);
        RC = JRC->ToString();
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
     delete JRC;
   }
  return RC;
}
//----------------------------------------------------------------------------
UnicodeString TdsAdminClass::GetPatCardDataById(UnicodeString ARecId, TJSONObject* AClsRC)
{
  dsLogMessage("RecId: "+ARecId, __FUNC__);
  UnicodeString RC;
  try
   {
     try
      {
        RC = FGetPatCardDataById(ARecId, AClsRC)->ToString();
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
   }
  return RC;
}
//----------------------------------------------------------------------------
bool TdsAdminClass::FAddCardData(UnicodeString ACardId, UnicodeString ACardPtId, UnicodeString ARecId, TJSONObject* ARC, TJSONObject* AClsRC)
{
  bool RC = false;
  TJSONObject* FCardData = new TJSONObject;
  try
   {
     TJSONArray *FIdList = (TJSONArray*)FRegComp->GetIdList(ACardId, 0, "{\""+ACardPtId+"\":\""+ARecId+"\"}");
     UnicodeString FCardRecId;
     for (int i = 0; i < FIdList->Count; i++)
      {
        FCardRecId = ((TJSONString*)FIdList->Items[i])->Value();
        FCardData->AddPair(FCardRecId, FRegComp->GetImportValById(ACardId, FCardRecId, AClsRC));
      }
     ARC->AddPair(ACardId, FCardData);
     RC = true;
   }
  __finally
   {
   }
  return RC;
}
//----------------------------------------------------------------------------
TJSONObject* TdsAdminClass::FGetPatCardDataById(UnicodeString ARecId, TJSONObject* AClsRC)
{
  dsLogMessage("RecId: "+ARecId, __FUNC__);
  TJSONObject* JRC = new TJSONObject;
  try
   {
     try
      {
        FAddCardData("3003", "317A", ARecId, JRC, AClsRC); //Прививки
//      FAddCardData("1003", "017A", ARecId, JRC, AClsRC); //Прививки (по инфекциям)
        FAddCardData("102F", "017B", ARecId, JRC, AClsRC); //Пробы
        FAddCardData("1100", "1101", ARecId, JRC, AClsRC); //Проверки
        FAddCardData("3030", "317C", ARecId, JRC, AClsRC); //Отводы
        FAddCardData("1030", "017C", ARecId, JRC, AClsRC); //Отводы (по инфекциям)
        FAddCardData("1112", "1113", ARecId, JRC, AClsRC); //Предварительный план

//        JRC->AddPair("3188", FRegComp->GetValById("3188", ARecId)); //Список планов
//        JRC->AddPair("1084", FRegComp->GetValById("1084", ARecId)); //План по инфекциям
//        JRC->AddPair("3084", FRegComp->GetValById("3084", ARecId)); //План
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
   }
  return JRC;
}
//----------------------------------------------------------------------------
System::UnicodeString TdsAdminClass::ImportData(UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId)
{
  dsLogMessage("ImportData: ID: "+ AId+", Value: "+AValue->ToString());
  UnicodeString RC = "";
  try
   {
     try
      {
        RC = FRegComp->ImportData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId);
      }
     catch(System::Sysutils::Exception &E)
      {
        dsLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
   }
  return RC;
}
//----------------------------------------------------------------------------

