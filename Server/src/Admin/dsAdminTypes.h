#ifndef dsAdminTypesH
#define dsAdminTypesH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <map>
//---------------------------------------------------------------------------
struct TdsLPU
{//   <class name='���' uid='00C7'
  UnicodeString Code;     //   <text name='���' uid='011E'
  UnicodeString IntCode;     //   <text name='���' uid='011E'
  UnicodeString Name;     //   <text name='������������' uid='00CA'
  UnicodeString FullName; //   <text name='������ ������������' uid='00CB'
  UnicodeString Addr;     //   <extedit name='�����' uid='00CC'
  UnicodeString OwnerFIO; //   <text name='�.�.�. ������������' uid='00CD'
  UnicodeString Phone;    //   <text name='�������' uid='00CE'
  UnicodeString EMail;    //   <text name='��. �����' uid='00CF'
  TdsLPU()
  {
    Code     = "";
    IntCode  = "";
    Name     = "";
    FullName = "";
    Addr     = "";
    OwnerFIO = "";
    Phone    = "";
    EMail    = "";
  };
  void operator = (TdsLPU AVal)
  {
    Code     = AVal.Code;
    IntCode  = AVal.IntCode;
    Name     = AVal.Name;
    FullName = AVal.FullName;
    Addr     = AVal.Addr;
    OwnerFIO = AVal.OwnerFIO;
    Phone    = AVal.Phone;
    EMail    = AVal.EMail;
  };
};
typedef map <__int64, TdsLPU> TIntLPUMap;
//---------------------------------------------------------------------------
struct TdsUser
{ //   <class name='������ �������������' uid='0002' comment='' ref='unit' required='0' inlist='l' depend='' isedit='1' is3D='1' tab=''>
  __int64       Code;
  UnicodeString Login; //<text name='�����' uid='0016' comment='' ref='' required='1' inlist='l' depend='' cast='no' fixedvar='0' isedit='1' linecount='' is3D='1' fl='' format='' minleft='' length='50' default=''/>
  UnicodeString Pass; //<text name='������' uid='0017' comment='' ref='' required='1' inlist='n' depend='' cast='up1st' fixedvar='0' isedit='1' linecount='2' is3D='1' fl='' format='' minleft='90' length='50' default=''/>
  UnicodeString Fam; //<text name='�������' uid='0013' comment='' ref='' required='1' inlist='l' depend='' cast='up1st' fixedvar='0' isedit='1' linecount='' is3D='1' fl='' format='' minleft='' length='30' default=''/>
  UnicodeString Name; //<text name='���' uid='0014' comment='' ref='' required='1' inlist='l' depend='' cast='up1st' fixedvar='0' isedit='1' linecount='' is3D='1' fl='' format='' minleft='' length='30' default=''/>
  UnicodeString SecName; //<text name='��������' uid='0015' comment='' ref='' required='1' inlist='l' depend='' cast='up1st' fixedvar='0' isedit='1' linecount='' is3D='1' fl='' format='' minleft='' length='30' default=''/>
  TdsUser()
  {
    Code  = 0;
    Login = "";
    Pass  = "";
    Fam   = "";
    Name  = "";
    SecName = "";
  };
  void operator = (TdsUser AVal)
  {
    Code  = AVal.Code;
    Login = AVal.Login;
    Pass  = AVal.Pass;
    Fam   = AVal.Fam;
    Name  = AVal.Name;
    SecName = AVal.SecName;
  };
};
typedef map <__int64, TdsUser> TIntUserMap;
typedef map <UnicodeString, TdsUser> TStrUserMap;
//---------------------------------------------------------------------------
struct TdsDB
{ //   <class name='���� ������' uid='0008' comment='' ref='' required='0' inlist='l' depend='' isedit='1' is3D='1' tab=''>
  __int64 LPUCode; //<choiceDB name='���' uid='0120' comment='' ref='00C7' required='1' inlist='l' depend='' isedit='1' is3D='1' fl='' minleft='' default='' linecount=''/>
  UnicodeString ICDBPath; //<extedit name='���� � �� &quot;��-���&quot;' uid='0011' comment='' ref='' required='0' inlist='l' depend='' isedit='1' is3D='1' minleft='50' linecount='2' showtype='0' btncount='1' fl='' fieldtype='varchar' length='255' default=''/>
  UnicodeString DocDBPath; //<extedit name='���� � �� &quot;��-���-���������������&quot;' uid='001D' comment='' ref='' required='0' inlist='l' depend='' isedit='1' is3D='1' minleft='50' linecount='2' showtype='0' btncount='1' fl='' fieldtype='varchar' length='255' default=''/>
  UnicodeString Comment; //<text name='��������' uid='000E' comment='' ref='' required='0' inlist='e' depend='' cast='no' fixedvar='0' isedit='1' linecount='' is3D='1' fl='' format='' minleft='' length='100' default=''/>
  TdsDB()
  {
    LPUCode   = 0;
    ICDBPath  = "";
    DocDBPath = "";
    Comment   = "";
  };
  void operator = (TdsDB AVal)
  {
    LPUCode   = AVal.LPUCode;
    ICDBPath  = AVal.ICDBPath;
    DocDBPath = AVal.DocDBPath;
    Comment   = AVal.Comment;
  };
};
typedef map <__int64, TdsDB> TIntDBMap;
//---------------------------------------------------------------------------
struct TdsRight
{ //<class name='������������ �� ��' uid='0018' comment='' ref='' required='0' inlist='l' depend='' isedit='1' is3D='1' tab=''>
  __int64 DBCode;      // ���� ������' uid='0019' comment='' ref='0008' required='1' inlist='l' depend='' isedit='1' is3D='1' fl='' minleft='' default='' linecount=''/>
  __int64 UserCode;    // ������������' uid='001A' comment='' ref='0002' required='1' inlist='l' depend='' isedit='1' is3D='1' fl='' minleft='' default='' linecount=''/>
//  bool Admin;          // <�����������������' uid='001E' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'/>
//  bool AdminLPU;       // <������� ������ ���' uid='011F' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'/>
  bool IC;             // <������ � &quot;��-���&quot;' uid='003B' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'/>
  bool ICReg;          // <������������' uid='002A' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool ICRegUList;     // <������ � ������ ���������' uid='0033' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool ICRegUListEdit; // <��������������' uid='0036' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool ICRegCList;     // <������ � ������������' uid='0037' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool ICRegCListEdit; // <��������������' uid='0038' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool ICCard;         // <������ � ���. �����' uid='002B' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool ICCardEdit;     // <��������������' uid='0039' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool ICPlan;         // <������ � ������ ������' uid='002C' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool ICPlanPlaning;  // <���������� ������������' uid='003A' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool Doc;            // ������ � &quot;��-���������������&quot;' uid='003C' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'/>
  bool DocSpec;        // <������ � ������ &quot;�������� ����������&quot;' uid='002E' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool DocSpecEdit;    // <��������������' uid='003D' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool DocDoc;         // <������ � ������ &quot;���������&quot;' uid='002D' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool DocDocEdit;     // <��������������' uid='003E' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool DocFilter;      // <������ � ������ &quot;�������&quot;' uid='002F' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool DocFilterEdit;  // <��������������' uid='003F' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'>
  bool VS;             // <����� ����' uid='0029' comment='' ref='' required='0' inlist='n' depend='' invname='' isedit='1' is3D='1' fl='' default='uncheck'/>
  TdsRight()
  {
    DBCode         = 0;
    UserCode       = 0;
//    Admin          = false;
//    AdminLPU       = false;
    IC             = false;
    ICReg          = false;
    ICRegUList     = false;
    ICRegUListEdit = false;
    ICRegCList     = false;
    ICRegCListEdit = false;
    ICCard         = false;
    ICCardEdit     = false;
    ICPlan         = false;
    ICPlanPlaning  = false;
    Doc            = false;
    DocSpec        = false;
    DocSpecEdit    = false;
    DocDoc         = false;
    DocDocEdit     = false;
    DocFilter      = false;
    DocFilterEdit  = false;
    VS             = false;
  };
  void operator = (TdsRight AVal)
  {
    DBCode         = AVal.DBCode;
    UserCode       = AVal.UserCode;
//    Admin          = AVal.Admin;
//    AdminLPU       = AVal.AdminLPU;
    IC             = AVal.IC;
    ICReg          = AVal.ICReg;
    ICRegUList     = AVal.ICRegUList;
    ICRegUListEdit = AVal.ICRegUListEdit;
    ICRegCList     = AVal.ICRegCList;
    ICRegCListEdit = AVal.ICRegCListEdit;
    ICCard         = AVal.ICCard;
    ICCardEdit     = AVal.ICCardEdit;
    ICPlan         = AVal.ICPlan;
    ICPlanPlaning  = AVal.ICPlanPlaning;
    Doc            = AVal.Doc;
    DocSpec        = AVal.DocSpec;
    DocSpecEdit    = AVal.DocSpecEdit;
    DocDoc         = AVal.DocDoc;
    DocDocEdit     = AVal.DocDocEdit;
    DocFilter      = AVal.DocFilter;
    DocFilterEdit  = AVal.DocFilterEdit;
    VS             = AVal.VS;
  };
};
typedef map <__int64, TdsRight> TIntRightMap;
//---------------------------------------------------------------------------
//enum TdsRegExtFilterSetType { efsUnit, efsUnitSel, efsClass, efsExtUnit1, efsExtUnit2, efsExtUnit3, efsExtUnit4};
typedef UnicodeString (__closure *TdsAdminUserRightEvent)(UnicodeString AUser, UnicodeString ADBPath);
#endif
