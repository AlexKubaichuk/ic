//---------------------------------------------------------------------------

#ifndef dsAdminUnitH
#define dsAdminUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <DataSnap.DSServer.hpp>
//#include <Datasnap.DSProviderDataModuleAdapter.hpp>
//---------------------------------------------------------------------------
#include <FireDAC.Phys.FB.hpp>
#include <Data.DB.hpp>


//#include <IPPeerClient.hpp>



#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <IPPeerClient.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Comp.Script.hpp>
#include <FireDAC.Comp.ScriptCommands.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.ConsoleUI.Wait.hpp>
#include <FireDAC.Stan.Util.hpp>

#include "dsKLADRUnit.h"
#include "dsAdminDMUnit.h"
#include "dsSrvRegUnit.h"
//---------------------------------------------------------------------------
class TdsVersion : public TObject
{
private:
 int FV1, FV2, FV3, FV4;
 void __fastcall FSetStrVer(UnicodeString AVer);
 UnicodeString __fastcall FGetStrVer();
public:
 __fastcall TdsVersion(UnicodeString AVer);
 __fastcall ~TdsVersion();

 bool __fastcall VerBE(UnicodeString AVer);

 __property UnicodeString StrVer = {read = FGetStrVer, write=FSetStrVer};
 __property int V1 = {read = FV1};
 __property int V2 = {read = FV2};
 __property int V3 = {read = FV3};
 __property int V4 = {read = FV4};

};
//---------------------------------------------------------------------------
class DECLSPEC_DRTTI TdsAdminClass : public TDataModule
{
__published:	// IDE-managed Components
 TFDConnection *IcConnection;
 TFDConnection *CheckConnection;
 TFDScript *MigrateScript;
 TFDTransaction *trMigrate;
 TFDGUIxWaitCursor *FDGUIxWaitCursor1;
 void __fastcall MigrateScriptConsolePut(TFDScript *AEngine, const UnicodeString AMessage,
          TFDScriptOuputKind AKind);
 void __fastcall MigrateScriptProgress(TObject *Sender);

private:	// User declarations
  TdsSrvRegistry  *FRegComp;
  TTagNode        *FDefXML;
  TdsAdminDM     *FDM;
  TdsKLADRClass   *FKLADRComp;
  UnicodeString      FUploadData;
  typedef map<UnicodeString, TDateTime> TStrDateMap;
  TStrDateMap        FModulesRT;
  TStrDateMap        FCommRT;
  TSesClearEvent     FSesClose;
  bool               MigrateError;
  TAppOptions        *FOpt;
  UnicodeString __fastcall FGetAdmUser();
  void __fastcall FSetAdmUser(UnicodeString AVal);

  UnicodeString __fastcall FGetAdmUserPass();
  void __fastcall FSetAdmUserPass(UnicodeString AVal);

  void __fastcall HLogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl);
  void __fastcall HLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl);
  void __fastcall HLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl);
  void __fastcall HLogBegin(UnicodeString AFuncName);
  void __fastcall HLogEnd(UnicodeString AFuncName);


  bool CheckRTVal(TTagNode *ADef, UnicodeString AId);
  UnicodeString FGetAddrStr(UnicodeString ACode, unsigned int AParams);
  void CheckAndUpdateVersion(TTagNode * VerXML);
  bool CheckAndUpdateBaseVersion(TTagNode *AVerDef, UnicodeString ADB);
  bool CheckAndUpdateDataVersion(UnicodeString ADB);
  bool MigrateDB(TFDConnection *AConnection, UnicodeString ADB, UnicodeString AOldDBVer, TTagNode * AVerDef);
  UnicodeString __fastcall ScriptOuputKindToString(TFDScriptOuputKind AKind);
  void SetUserRT(TTagNode * ART, UnicodeString ASesName, TDateTime AStartDate, bool AAdm);

public:		// User declarations
  __fastcall TdsAdminClass(TComponent* Owner/*, TFDConnection *AConnection*/, TAxeXMLContainer *AXMLList, TAppOptions *AOpt, bool AUpdate = true);
  __fastcall ~TdsAdminClass();
  void Disconnect();
  __property TdsKLADRClass   *KLADRComp = { read = FKLADRComp };

  UnicodeString GetDefXML();
  UnicodeString GetClassXML(UnicodeString ARef);
  UnicodeString GetClassZIPXML(UnicodeString ARef);
  bool          SetClassZIPXML(UnicodeString ARef);
  __int64       GetCount(UnicodeString AId, UnicodeString AFilterParam);
  TJSONObject*  GetIdList(UnicodeString AId, int AMax, UnicodeString AFilterParam);
  TJSONObject*  GetValById(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  GetValById10(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  Find(UnicodeString AId, UnicodeString AFindData);
  TJSONObject*  FindEx(UnicodeString AId, UnicodeString AFindData);
  UnicodeString InsertData(UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  UnicodeString EditData(UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  UnicodeString DeleteData(UnicodeString AId, UnicodeString ARecId);

  bool          UploadData(UnicodeString ADataPart, int AType);

  bool CheckUser(UnicodeString AUser, UnicodeString APasswd, bool &AValid);
  UnicodeString LoginUser(UnicodeString AId);
  UnicodeString LoginSysUser(UnicodeString ABId);
  void Logout();
  bool CheckRT(UnicodeString AURT, UnicodeString AFunc);
  UnicodeString GetUpdateCode();

  TJSONObject * FindAddr(UnicodeString AStr, UnicodeString ADefAddr, int AParams);
  UnicodeString AddrCodeToText(UnicodeString ACode, int AParams);

  __property UnicodeString User = {read=FGetAdmUser, write=FSetAdmUser};
  __property UnicodeString Pwd = {read=FGetAdmUserPass, write=FSetAdmUserPass};
  __property TSesClearEvent OnSesClose = {read=FSesClose, write=FSesClose};

};
//---------------------------------------------------------------------------
//extern PACKAGE TdsICClass *dsICClass;
//---------------------------------------------------------------------------
#endif
