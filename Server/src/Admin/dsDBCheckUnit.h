//---------------------------------------------------------------------------

#ifndef dsDBCheckUnitH
#define dsDBCheckUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
#include <FireDAC.Phys.FB.hpp>
#include <Data.DB.hpp>

#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <IPPeerClient.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Comp.Script.hpp>
#include <FireDAC.Comp.ScriptCommands.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.ConsoleUI.Wait.hpp>
#include <FireDAC.Stan.Util.hpp>

//#include "SVRClientClassesUnit.h"
//---------------------------------------------------------------------------
class DECLSPEC_DRTTI TdsDBCheckClass : public TDataModule
{
__published:	// IDE-managed Components
 TFDConnection *CheckConnection;

private:	// User declarations

public:		// User declarations
  __fastcall TdsDBCheckClass(TComponent* Owner);
  __fastcall ~TdsDBCheckClass();
  UnicodeString CheckConnect(UnicodeString ADB, UnicodeString AServer, int APort, UnicodeString AUser, UnicodeString APass);
};
//---------------------------------------------------------------------------
//extern PACKAGE TdsICClass *dsICClass;
//---------------------------------------------------------------------------
#endif
