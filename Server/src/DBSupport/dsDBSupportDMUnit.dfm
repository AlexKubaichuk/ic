object dsDBSupportDM: TdsDBSupportDM
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Height = 482
  Width = 711
  object IcConnection: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'User_Name=sysdba'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'Server=localhost'
      'Port=3050'
      'Pooled=False')
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    FormatOptions.AssignedValues = [fvFmtDisplayDateTime, fvFmtDisplayDate, fvFmtDisplayTime]
    FormatOptions.FmtDisplayDateTime = 'dd#mm#yyyy hh:nn:ss'
    FormatOptions.FmtDisplayDate = 'dd#mm#yyyy'
    FormatOptions.FmtDisplayTime = 'hh:nn:ss'
    ResourceOptions.AssignedValues = [rvDirectExecute, rvAutoConnect, rvSilentMode]
    ResourceOptions.DirectExecute = True
    ResourceOptions.SilentMode = True
    ResourceOptions.AutoConnect = False
    TxOptions.AutoStart = False
    TxOptions.AutoStop = False
    TxOptions.DisconnectAction = xdRollback
    LoginPrompt = False
    Left = 29
    Top = 15
  end
  object ICConnect: TDSRestConnection
    Host = 'localhost'
    Port = 5100
    LoginPrompt = False
    Left = 151
    Top = 17
    UniqueId = '{2561E080-5D09-4396-9D34-969204DAB404}'
  end
end
