//---------------------------------------------------------------------------
 
#ifndef dsDBReplanH
#define dsDBReplanH

//#include "DKCfg.h"      // ��������� �������� ����������

#include <System.hpp>
#include <Classes.hpp>
//#include <DateUtils.hpp>

//#include "DKClasses.h"
//#include "XMLContainer.h"
//#include "ICSDATEUTIL.hpp"
//#include "DKUtils.h"

#include "dsDBSupport.h"
//#include "dsMISSyncDMUnit.h"
//#include "PlanerDebug.h"

//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                              TdsDBReplan                                 ##
//##                                                                       ##
//###########################################################################

//---------------------------------------------------------------------------
class PACKAGE TdsDBReplan : public TdsDBSupport
{
public:
  __fastcall TdsDBReplan(TAppOptions *AOpt, UnicodeString AUser, UnicodeString APass);
  __fastcall ~TdsDBReplan();
  System::UnicodeString __fastcall Sync();
};
//---------------------------------------------------------------------------
#endif
