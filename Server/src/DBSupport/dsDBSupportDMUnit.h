//---------------------------------------------------------------------------

#ifndef dsDBSupportDMUnitH
#define dsDBSupportDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.JSON.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <IPPeerClient.hpp>
#include <REST.Authenticator.Basic.hpp>
#include <REST.Client.hpp>
//---------------------------------------------------------------------------
#include <FireDAC.Phys.FB.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include "kabQueryUtils.h"
#include "SVRClientClassesUnit.h"
#include "XMLContainer.h"
#include "icsLog.h"
#include "SVRClientClassesUnit.h"
//---------------------------------------------------------------------------
class TdsDBSupportDM : public TDataModule
{
__published:	// IDE-managed Components
 TFDConnection *IcConnection;
 TDSRestConnection *ICConnect;
 void __fastcall DataModuleDestroy(TObject *Sender);
private:	// User declarations
  UnicodeString FDBId;
  UnicodeString FDBPath;
  bool FTerminated;
  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;
  TOnLogSQLEvent      FOnLogSQL;

  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;

  bool Logged;
  TdsICClassClient *FICClient;
  TdsAdminClassClient *FAdminClient;

  void __fastcall LogMessage(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall LogError(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall LogSQL(UnicodeString ASQL, UnicodeString AFuncName = "", TTime AExecTime = 0, int ALvl = 1);
  void __fastcall LogBegin(UnicodeString AFuncName);
  void __fastcall LogEnd(UnicodeString AFuncName);

public:		// User declarations
  __fastcall TdsDBSupportDM(TComponent* Owner, TAppOptions *AOpt, UnicodeString AUser = "", UnicodeString APass = "");

  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AParam1Name,      Variant AParam1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AParam2Name = "", Variant AParam2Val = 0,
                               UnicodeString AParam3Name = "", Variant AParam3Val = 0,
                               UnicodeString AParam4Name = "", Variant AParam4Val = 0);

  UnicodeString __fastcall RePlan(UnicodeString ALPUID, __int64 AUCode, bool relog = false);

  __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
  __property TOnLogMsgErrEvent OnLogError = {read = FOnLogError, write = FOnLogError};

  __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
  __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};
  __property TOnLogSQLEvent    OnLogSQL   = {read = FOnLogSQL, write = FOnLogSQL};
  void __fastcall UpdateCardGenCode(UnicodeString AGenId);

  __property UnicodeString DBId = {read=FDBId, write=FDBId};
  __property UnicodeString DBPath = {read=FDBPath, write=FDBPath};
  __property bool Terminated = {read=FTerminated, write=FTerminated};

};
//---------------------------------------------------------------------------
extern PACKAGE TdsDBSupportDM *dsDBSupportDM;
//---------------------------------------------------------------------------
#endif
