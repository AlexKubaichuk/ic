// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsDBSupportDMUnit.h"
#include "srvsetting.h"
// #include "dsIC67Sync.h"
// #include "ICSDATEUTIL.hpp"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TdsDBSupportDM * dsDBSupportDM;
// ---------------------------------------------------------------------------
__fastcall TdsDBSupportDM::TdsDBSupportDM(TComponent * Owner, TAppOptions * AOpt, UnicodeString AUser,
  UnicodeString APass) : TDataModule(Owner)
 {
  FTerminated = false;
  try
   {
    Logged = false;
    if ((AUser + APass).Length())
     {
      ICConnect->UserName = AUser;
      ICConnect->Password = APass;
      ICConnect->Port     = AOpt->Vals[_PORTNAME_].AsIntDef(_SERVER_PORT_);
      FICClient           = new TdsICClassClient(ICConnect);
      FAdminClient        = new TdsAdminClassClient(ICConnect);
     }
   }
  catch (System::Sysutils::Exception & E)
   {
    LogError(E.Message, __FUNC__);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::DataModuleDestroy(TObject * Sender)
 {
  delete FICClient;
  delete FAdminClient;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  if (FOnLogMessage)
   FOnLogMessage(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  if (FOnLogError)
   FOnLogError(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  if (FOnLogSQL)
   FOnLogSQL(ASQL, AFuncName, AExecTime, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::LogBegin(UnicodeString AFuncName)
 {
  if (FOnLogBegin)
   FOnLogBegin(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::LogEnd(UnicodeString AFuncName)
 {
  if (FOnLogEnd)
   FOnLogEnd(AFuncName);
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsDBSupportDM::CreateTempQuery()
 {
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = IcConnection;
    FQ->Connection                       = IcConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  return FQ;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDBSupportDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDBSupportDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
  UnicodeString AParam1Name, Variant AParam1Val, UnicodeString ALogComment, UnicodeString AParam2Name,
  Variant AParam2Val, UnicodeString AParam3Name, Variant AParam3Val, UnicodeString AParam4Name, Variant AParam4Val)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
      if (AParam2Name.Length())
       AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
      if (AParam3Name.Length())
       AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
      if (AParam4Name.Length())
       AQuery->ParamByName(AParam4Name)->Value = AParam4Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      UnicodeString FParams = "; Params: ";
      if (AParam1Name.Length())
       FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
      if (AParam2Name.Length())
       FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
      if (AParam3Name.Length())
       FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
      if (AParam4Name.Length())
       FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "");
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupportDM::UpdateCardGenCode(UnicodeString AGenId)
 {
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    UnicodeString GenNum = "1";
    quExec(FQ, false, "SELECT Max(code) as MCODE FROM CLASS_" + AGenId);
    if (FQ->RecordCount)
     {
      if (!FQ->FieldByName("MCODE")->IsNull)
       GenNum = FQ->FieldByName("MCODE")->AsString;
     }
    quExec(FQ, false, "ALTER SEQUENCE GEN_" + AGenId + "_ID RESTART WITH " + GenNum);
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDBSupportDM::RePlan(UnicodeString ALPUID, __int64 AUCode, bool relog)
 {
  if (!Logged)
   {
    FAdminClient->LoginSysUser(ALPUID);
    Logged = true;
   }
  if (Logged)
   {
    if (relog)
     {
      FAdminClient->Logout();
      ICConnect->SessionID = "";
//      ICConnect->Reset();

      Sleep(100);
//      ICConnect->Login();

      FAdminClient->LoginSysUser(ALPUID);
     }
    return FICClient->CreateUnitPlan(AUCode);
   }
 }
// ---------------------------------------------------------------------------
