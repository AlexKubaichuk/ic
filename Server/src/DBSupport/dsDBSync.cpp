﻿/*
 Файл        - Planer.cpp
 Проект      - Управление иммунизацией.
 Программа "Управление иммунизацией"
 Описание    - Планировщик.
 Файл реализации
 Разработчик - Ковалев Д.В.
 Дата        - 09.05.2004
 */
// ---------------------------------------------------------------------------
#include <string.h>
#pragma hdrstop
#include <algorithm>
#include <vcl.h>
#include <System.SysUtils.hpp>
#include "dsDBSync.h"
#include "JSONUtils.h"
// #include "dsAuraSync.h"
// #include "dsMISCommonSync.h"
// #include "dsSrvRegTemplate.h"
// #include "OrgParsers.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ###########################################################################
// ##                                                                       ##
// ##                              TdsDBSync                             ##
// ##                                                                       ##
// ###########################################################################
__fastcall TdsDBSync::TdsDBSync(TAppOptions * AOpt) : TdsDBSupport(AOpt)
 {
  FStage = "Обновление генераторов";
 }
// ---------------------------------------------------------------------------
__fastcall TdsDBSync::~TdsDBSync()
 {
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDBSync::Sync()
 {
  CallLogBegin(__FUNC__);
  UnicodeString RC = "Ошибка";
  FTerminated   = false;
  FInSync       = true;
  FCurrProgress = 0;
  FMaxProgress  = 0;
  FStage        = "Обновление генераторов";
  try
   {
    try
     {
      try
       {
        FDM->Terminated = false;
        if (ConnectDB(FDM->IcConnection, FOpt, DBId, __FUNC__, CallLogErr, CallLogBegin, CallLogEnd))
         {
          if (!FTerminated)
           FDM->UpdateCardGenCode("3003");
          if (!FTerminated)
           FDM->UpdateCardGenCode("1003");
          if (!FTerminated)
           FDM->UpdateCardGenCode("102F");
          if (!FTerminated)
           FDM->UpdateCardGenCode("1100");
          if (!FTerminated)
           FDM->UpdateCardGenCode("3030");
          if (!FTerminated)
           FDM->UpdateCardGenCode("1030");
          if (!FTerminated)
           FDM->UpdateCardGenCode("1112");
          if (!FTerminated)
           FDM->UpdateCardGenCode("111F"); // Журнал прихода
          if (!FTerminated)
           FDM->UpdateCardGenCode("1120"); // Журнал расхода
          if (!FTerminated)
           FDM->UpdateCardGenCode("1121"); // МИБП (по серии)
          if (!FTerminated)
           FDM->UpdateCardGenCode("1122"); // МИБП (общий)
          if (!FTerminated)
           FDM->UpdateCardGenCode("1123"); // Контроль t°c
          if (!FTerminated)
           FDM->UpdateCardGenCode("1124"); // Журнал списаний
          if (!FTerminated)
           FDM->UpdateCardGenCode("1131"); // Источники
          if (!FTerminated)
           FDM->UpdateCardGenCode("330F"); // Выдача
          if (!FTerminated)
           FDM->UpdateCardGenCode("32E8"); // Список холодильников
          if (!FTerminated)
           FDM->UpdateCardGenCode("32EE"); // Список периодов контроля t°c
         }
       }
      __finally
       {
        DisconnectDB(FDM->IcConnection, __FUNC__, CallLogBegin, CallLogEnd);
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      CallLogErr(E.Message, __FUNC__);
     }
   }
  __finally
   {
    FInSync = false;
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
