//---------------------------------------------------------------------------

#ifndef dsDBSupportH
#define dsDBSupportH

//#include "DKCfg.h"      // ��������� �������� ����������

#include <System.hpp>
#include <Classes.hpp>
//#include <DateUtils.hpp>

//#include "DKClasses.h"
//#include "XMLContainer.h"
//#include "ICSDATEUTIL.hpp"
//#include "DKUtils.h"

#include "dsDBSupportDMUnit.h"
//#include "dsMISSyncDMUnit.h"

//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                              TdsDBSupport                                 ##
//##                                                                       ##
//###########################################################################

//---------------------------------------------------------------------------
class PACKAGE TdsDBSupport : public TObject
{
typedef void __fastcall (__closure *TdsSyncMethod)(void);
private:

  class TBackgroundDBSupport;
  TBackgroundDBSupport * FBkgSyncModule;
  void __fastcall SyncCall(TdsSyncMethod AMethod);


  TNotifyEvent    FOnIncProgress;      // �������, ��� ��������� �������� ���������� �������
                                         // ��������� ������� ProgressBar'�
  UnicodeString FLogMsg, FLogFuncName;
  int FLogALvl;
  TTime         FLogExecTime;

  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;
  TOnLogSQLEvent      FOnLogSQL;

  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;


  inline void __fastcall CallOnIncProgress();
  void __fastcall SyncCallOnIncProgress();

  UnicodeString __fastcall FGetDBId();
  void __fastcall FSetDBId(UnicodeString AValue);

  UnicodeString __fastcall FGetDBPath();
  void __fastcall FSetDBPath(UnicodeString AValue);

protected:

  UnicodeString   FStage;
  int             FCurrProgress;
  int             FMaxProgress;        // Max �������� ��� ProgressBar'�
  bool            FTerminated;
  bool            FInSync;
  TdsDBSupportDM* FDM;
  TAppOptions     *FOpt;

  void __fastcall SyncCallLogMsg();
  void __fastcall SyncCallLogErr();
  void __fastcall SyncCallLogSQL();
  void __fastcall SyncCallLogBegin();
  void __fastcall SyncCallLogEnd();

  void __fastcall CallLogMsg(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogErr(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl = 1);
  void __fastcall CallLogBegin(UnicodeString AFuncName);
  void __fastcall CallLogEnd(UnicodeString AFuncName);

public:
    System::UnicodeString Proc;
    System::UnicodeString __fastcall StartSync();
    System::UnicodeString __fastcall StopSync();
    __property bool Terminated = {read = FTerminated};
    __property bool InSync = {read = FInSync};

    int __fastcall GetMaxProgress();
    int __fastcall GetCurProgress();
    UnicodeString __fastcall GetCurStage() const;

    __fastcall TdsDBSupport(TAppOptions *AOpt, UnicodeString AUser = "", UnicodeString APass = "");
    virtual __fastcall ~TdsDBSupport();

    __property int          MaxProgress   = { read = FMaxProgress };
    __property TNotifyEvent OnIncProgress = { read = FOnIncProgress, write = FOnIncProgress };

    __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
    __property TOnLogMsgErrEvent OnLogError   = {read = FOnLogError, write = FOnLogError};
    __property TOnLogSQLEvent    OnLogSQL     = {read = FOnLogSQL, write = FOnLogSQL};

    __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
    __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};

  __property UnicodeString DBId = {read=FGetDBId, write=FSetDBId};
  __property UnicodeString DBPath = {read=FGetDBPath, write=FSetDBPath};

  virtual System::UnicodeString __fastcall Sync();
};
//---------------------------------------------------------------------------
#endif
