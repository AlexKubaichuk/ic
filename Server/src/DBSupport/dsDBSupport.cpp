/*
 ������      - ���������� ������������.
 ��������� "���������� ������������"
 ��������    - �����������.
 ���� ����������
 ����������� - ������� �.�.
 ����        - 09.05.2004
 */
// ---------------------------------------------------------------------------
#include <string.h>
#pragma hdrstop
#include <algorithm>
#include <vcl.h>
#include <System.SysUtils.hpp>
#include "dsDBSupport.h"
#include "JSONUtils.h"
// #include "dsAuraSync.h"
// #include "dsMISCommonSync.h"
// #include "dsSrvRegTemplate.h"
// #include "OrgParsers.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ###########################################################################
// ##                                                                       ##
// ##                     TBackgroundDBSupport                                ##
// ##                                                                       ##
// ###########################################################################
class TdsDBSupport::TBackgroundDBSupport : public TThread
 {
 private:
  TdsDBSupport * FSyncModule;
 protected:
  virtual void __fastcall Execute();
 public:
  __fastcall TBackgroundDBSupport(bool CreateSuspended, TdsDBSupport * ASyncModule);
  __fastcall ~TBackgroundDBSupport();
  void __fastcall ThreadSafeCall(TdsSyncMethod Method);
 };
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                     TdsDBSupport::TBackgroundDBSupport                      ##
// ##                                                                       ##
// ###########################################################################
__fastcall TdsDBSupport::TBackgroundDBSupport::TBackgroundDBSupport(bool CreateSuspended, TdsDBSupport * ASyncModule)
  : TThread(CreateSuspended), FSyncModule(ASyncModule)
 {
 }
// ---------------------------------------------------------------------------
__fastcall TdsDBSupport::TBackgroundDBSupport::~TBackgroundDBSupport()
 {
  FSyncModule = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::TBackgroundDBSupport::Execute()
 {
  CoInitialize(NULL);
  FSyncModule->Sync();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::TBackgroundDBSupport::ThreadSafeCall(TdsSyncMethod Method)
 {
  Synchronize(Method);
 }
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                              TdsDBSupport                             ##
// ##                                                                       ##
// ###########################################################################
__fastcall TdsDBSupport::TdsDBSupport(TAppOptions * AOpt, UnicodeString AUser, UnicodeString APass)
 {
  FOpt              = AOpt;
  FDM               = new TdsDBSupportDM(NULL, AOpt, AUser, APass);
  FDM->OnLogMessage = CallLogMsg;
  FDM->OnLogError   = CallLogErr;
  FDM->OnLogSQL     = CallLogSQL;
  FDM->OnLogBegin   = CallLogBegin;
  FDM->OnLogEnd     = CallLogEnd;
  FBkgSyncModule    = NULL;
  FStage            = "";
  FMaxProgress      = 10;
  FOnIncProgress    = NULL;
  FInSync           = false;
 }
// ---------------------------------------------------------------------------
__fastcall TdsDBSupport::~TdsDBSupport()
 {
  StopSync();
  if (FBkgSyncModule)
   delete FBkgSyncModule;
  FBkgSyncModule    = NULL;
  FDM->OnLogMessage = NULL;
  FDM->OnLogError   = NULL;
  FDM->OnLogBegin   = NULL;
  FDM->OnLogEnd     = NULL;
  delete FDM;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::CallOnIncProgress()
 {
  SyncCall(& SyncCallOnIncProgress);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::SyncCallOnIncProgress()
 {
  if (FOnIncProgress)
   FOnIncProgress(this);
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDBSupport::StartSync()
 {
  CallLogBegin(__FUNC__);
  UnicodeString RC = "error";
  CallLogMsg(GetCurStage(), __FUNC__);
  try
   {
    if (InSync)
     CallLogErr("����������� " + GetCurStage() + ".", __FUNC__);
    else
     {
      if (FBkgSyncModule)
       {
        FBkgSyncModule->Terminate();
        Sleep(50);
        delete FBkgSyncModule;
        FBkgSyncModule = NULL;
       }
      FBkgSyncModule = new TBackgroundDBSupport(false, this);
      RC = "ok";
     }
   }
  __finally
   {
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDBSupport::StopSync()
 {
  UnicodeString RC = "error";
  CallLogBegin(__FUNC__);
  try
   {
    Proc            = "";
    FDM->Terminated = true;
    FTerminated     = true;
    if (FBkgSyncModule && InSync)
     {
      FInSync = false;
      delete FBkgSyncModule;
      FBkgSyncModule = NULL;
     }
    RC = "ok";
   }
  __finally
   {
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsDBSupport::SyncCall(TdsSyncMethod AMethod)
 {
  if (FBkgSyncModule)
   FBkgSyncModule->ThreadSafeCall(AMethod);
  else
   AMethod();
 }
// ---------------------------------------------------------------------------
int __fastcall TdsDBSupport::GetMaxProgress()
 {
  return FMaxProgress;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsDBSupport::GetCurProgress()
 {
  return FCurrProgress;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDBSupport::GetCurStage() const
 {
  return FStage;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::CallLogMsg(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogMsg);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::CallLogErr(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogErr);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::CallLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  FLogMsg      = ASQL;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  FLogExecTime = AExecTime;
  SyncCall(& SyncCallLogSQL);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::CallLogBegin(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogBegin);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::CallLogEnd(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogEnd);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::SyncCallLogMsg()
 {
  if (FOnLogMessage)
   FOnLogMessage(FLogMsg, FLogFuncName, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::SyncCallLogErr()
 {
  if (FOnLogError)
   FOnLogError(FLogMsg, FLogFuncName, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::SyncCallLogSQL()
 {
  if (FOnLogSQL)
   FOnLogSQL(FLogMsg, FLogFuncName, FLogExecTime, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::SyncCallLogBegin()
 {
  if (FOnLogBegin)
   FOnLogBegin(FLogFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::SyncCallLogEnd()
 {
  if (FOnLogEnd)
   FOnLogEnd(FLogFuncName);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDBSupport::FGetDBId()
 {
  return FDM->DBId;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::FSetDBId(UnicodeString AValue)
 {
  FDM->DBId = AValue;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDBSupport::FGetDBPath()
 {
  return FDM->DBPath;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDBSupport::FSetDBPath(UnicodeString AValue)
 {
  FDM->DBPath = AValue;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDBSupport::Sync()
 {
  return "error";
 }
// ----------------------------------------------------------------------------
