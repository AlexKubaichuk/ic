﻿//---------------------------------------------------------------------------
        
#ifndef dsDBSyncH
#define dsDBSyncH

//#include "DKCfg.h"      // Директивы условной компиляции

#include <System.hpp>
#include <Classes.hpp>
//#include <DateUtils.hpp>

//#include "DKClasses.h"
//#include "XMLContainer.h"
//#include "ICSDATEUTIL.hpp"
//#include "DKUtils.h"

#include "dsDBSupport.h"
//#include "dsMISSyncDMUnit.h"
//#include "PlanerDebug.h"

//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                              TdsDBSync                                 ##
//##                                                                       ##
//###########################################################################

//---------------------------------------------------------------------------
class PACKAGE TdsDBSync : public TdsDBSupport
{
public:
  __fastcall TdsDBSync(TAppOptions *AOpt);
  __fastcall ~TdsDBSync();

  System::UnicodeString __fastcall Sync();
};
//---------------------------------------------------------------------------
#endif
