/*
 ����        - Planer.cpp
 ������      - ���������� ������������.
 ��������� "���������� ������������"
 ��������    - �����������.
 ���� ����������
 ����������� - ������� �.�.
 ����        - 09.05.2004
 */
// ---------------------------------------------------------------------------
#include <string.h>
#pragma hdrstop
#include <algorithm>
#include <vcl.h>
#include <System.SysUtils.hpp>
#include "dsDBReplan.h"
#include "JSONUtils.h"
// #include "dsAuraSync.h"
// #include "dsMISCommonSync.h"
// #include "dsSrvRegTemplate.h"
// #include "OrgParsers.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                              TdsDBReplan                             ##
// ##                                                                       ##
// ###########################################################################
__fastcall TdsDBReplan::TdsDBReplan(TAppOptions * AOpt, UnicodeString AUser, UnicodeString APass)
  : TdsDBSupport(AOpt, AUser, APass)
 {
  FStage = "����������������";
 }
// ---------------------------------------------------------------------------
__fastcall TdsDBReplan::~TdsDBReplan()
 {
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsDBReplan::Sync()
 {
  CallLogBegin(__FUNC__);
  UnicodeString RC = "������";
  FTerminated     = false;
  FInSync         = true;
  FCurrProgress   = 0;
  FMaxProgress    = 0;
  FStage          = "����������������";
  FDM->Terminated = false;
  ConnectDB(FDM->IcConnection, FOpt, DBPath, __FUNC__, CallLogErr, CallLogBegin, CallLogEnd);
  TFDQuery * FQ = FDM->CreateTempQuery();
  TStringList * UCodes = new TStringList;
  int itFSave = 100;
  int itReconect = 1000;
  __int64 memsize;
  try
   {
    try
     {
      UnicodeString RepFile = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\MISSyncData\\" + DBId + ".repl");
      if (!FileExists(RepFile))
       {
        FDM->quExec(FQ, false, "SELECT code from class_1000 where R32B3 <> 5");
        while (!FQ->Eof && !FTerminated)
         {
          UCodes->Add(FQ->FieldByName("code")->AsString);
          FQ->Next();
         }
        if (!DirectoryExists(icsPrePath(ExtractFilePath(ParamStr(0)) + "\\MISSyncData")))
         ForceDirectories(icsPrePath(ExtractFilePath(ParamStr(0)) + "\\MISSyncData"));
        UCodes->SaveToFile(RepFile);
       }
      UCodes->LoadFromFile(RepFile);
      UnicodeString ptCode;
      for (int i = UCodes->Count - 1; (i >= 0) && !FTerminated; i--)
       {
        // Sleep(300);
        // memsize = PrintProcessMemoryInfo().ToIntDef(0);
        ptCode = UCodes->Strings[i].Trim();
        if (ptCode.ToIntDef(0))
         {
          FDM->quExec(FQ, true, "Update class_1000 set R3195=null where code=" + ptCode);
          // dsLogMessage("1. TdsDBSupportDM::FullReplan for " + ptCode + " Size: " +            IntToStr(PrintProcessMemoryInfo().ToIntDef(0) - memsize), __FUNC__);
          if (FDM->RePlan(DBId, UCodes->Strings[i].Trim().ToIntDef(0), itReconect == 1) == "ok")
           UCodes->Delete(i);
          // dsLogMessage("2. TdsDBSupportDM::FullReplan for " + ptCode + " Size: " +            IntToStr(PrintProcessMemoryInfo().ToIntDef(0) - memsize), __FUNC__);
         }
        else
         UCodes->Delete(i);
        if (--itReconect < 0)
         itReconect = 1000;
        if (--itFSave < 0)
         {
          dsLogMessage("3. TdsDBReplan::SaveToFile", __FUNC__);
          UCodes->SaveToFile(RepFile);
          itFSave = 100;
         }
        // dsLogMessage("3 TdsDBSupportDM::FullReplan for " + ptCode + " Size: " +          IntToStr(PrintProcessMemoryInfo().ToIntDef(0) - memsize), __FUNC__);
        // Sleep(100);
        // dsLogMessage("4 TdsDBSupportDM::FullReplan for " + ptCode + " Size: " +          IntToStr(PrintProcessMemoryInfo().ToIntDef(0) - memsize), __FUNC__);
       }
      if (!UCodes->Count)
       DeleteFile(RepFile);
     }
    catch (Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
    delete UCodes;
    DisconnectDB(FDM->IcConnection, __FUNC__, CallLogBegin, CallLogEnd);
    FInSync = false;
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
