// ---------------------------------------------------------------------------
#pragma hdrstop
#include "KLADRParsers.h"
#include "srvsetting.h"
#include "icsLog.h"
#include <System.Character.hpp>
#ifndef _CONFSETTTING
#error _CONFSETTTING using are not defined
#endif
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
// using namespace System::Character;
void __fastcall KLADR::ShowDebug(UnicodeString AText)
 {
#ifdef _DEBUG_MESSAGE
  // dsLogMessage(AText, "", 1);
#endif
 }
// ---------------------------------------------------------------------------
__fastcall EKLADRError::EKLADRError(const UnicodeString Msg) : Exception(Msg)
 {
 }
// ---------------------------------------------------------------------------
// ###########################################################################
// #                                                                         #
// #                           CTTCacheRecord                                #
// #                                                                         #
// ###########################################################################
// ---------------------------------------------------------------------------
__fastcall CTTCacheRecord::CTTCacheRecord()
 {
  // dsLogBegin(__FUNC__);
  Status = 0;
  for (int i = 0; i < 8; i++)
   Values[i] = "";
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall CTTCacheRecord::GetText(unsigned int AParams)
 {
  // dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  unsigned int FParams = AParams;
  try
   {
    /*
     �� ��� ��� ��� ��, ���
     �� - ��� �������� ���������� ���������(�������), ���� �������� ������������ � ���������� 2 � �������� �������������� ������� ���������� ���������(�����);
     l2 ��� - ��� ������;
     l3 ��� - ��� ������;
     l4 ��� - ��� ����������� ������,
     STATUS
     0 - ������ �� �������� ������� ���������������-���������������� �����������;
     1 - ������ �������� ������� ������;
     2 - ������ �������� �������(��������) �������;
     3 - ������ �������� ������������ � ������� ������ � ������� �������;
     4 - ����������� �����, �.�. �����, � ������� ��������� ����� �������
     (������ ��� �������� 2-�� ������).

     ���� "������ �������" ������������ ��� ����������� ������������ ��������� ������
     � �������������� ���� ������ �����:
     ���� - "1",         �� � ������ ����������� ������ � ���������� �����(����� �� �����������);
     ���� - "2" ��� "3", �� � ������ ����������� ������ ����� �������(������ � ����� �� �����������).
     */
    if (Status == 1)
     { // � ������ ����������� ������ � ���������� �����(����� �� �����������);
      FParams &= 0xBF;
     }
    else if ((Status == 2) || (Status == 3))
     { // � ������ ����������� ������ ����� �������(������ � ����� �� �����������).
      FParams &= 0x3F;
     }
    // ������ � 1 �� 7 ������
    for (int i = 0; i < 7; i++)
     {
      if ((FParams & (0x80 >> i)) && Values[i].Length())
       {
        if (RC.Length())
         {
          if (Values[i].Length())
           RC += ", " + Values[i];
         }
        else
         RC = Values[i];
       }
     }
    if ((FParams & 0x01) && !RC.IsEmpty() && !Values[7].IsEmpty())
     {
      if (RC.Length())
       RC = Values[7] + ", " + RC;
      else
       RC = Values[7];
     }
   }
  __finally
   {
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
// ###########################################################################
// #                                                                         #
// #                        TNFAddrWords                                     #
// #                                                                         #
// ###########################################################################
// ---------------------------------------------------------------------------
__fastcall TNFAddrWords::TNFAddrWords(UnicodeString ASrc) : TStringList()
 {
  // dsLogBegin(__FUNC__);
  FResWords        = new StringIntMap;
  FReplasedObjects = new StrStrMap;
  (*FResWords)["���"] = 1;
  (*FResWords)["�����������"] = 1;
  (*FResWords)["����"] = 1;
  (*FResWords)["����������� ����"] = 2;
  (*FResWords)["����������"] = 1;
  (*FResWords)["����"] = 1;
  (*FResWords)["����������"] = 1;
  (*FResWords)["���������� �������"] = 2;
  (*FResWords)["����������"] = 1;
  (*FResWords)["���������� �����"] = 2;
  (*FResWords)["�����"] = 1;
  (*FResWords)["�����"] = 1;
  (*FResWords)["���"] = 1;
  (*FResWords)["�������"] = 1;
  // FResWords[["���"]                                 = 1;
  (*FResWords)["�������"] = 1;
  (*FResWords)["�����"] = 1;
  (*FResWords)["�������"] = 1;
  (*FResWords)["�������"] = 1;
  (*FResWords)["���"] = 1;
  (*FResWords)["�������-������������"] = 1;
  (*FResWords)["����������"] = 1;
  (*FResWords)["�������-������������ ����������"] = 2;
  //(*FResWords)["�������"] = 1;
  // (*  FResWor)ds["������"]                              = 1;
  (*FResWords)["������ �������"] = 2;
  (*FResWords)["�������"] = 1;
  (*FResWords)["���"] = 1;
  (*FResWords)["������"] = 1;
  (*FResWords)["�/�_��"] = 1;
  (*FResWords)["�/� �������.(��������) �����"] = 4;
  (*FResWords)["�/�_�����"] = 1;
  (*FResWords)["��������������� �����"] = 2;
  (*FResWords)["�/�_������"] = 1;
  (*FResWords)["��������������� �������"] = 2;
  (*FResWords)["�/�_�����"] = 1;
  (*FResWords)["��������������� ���������"] = 3;
  (*FResWords)["�/�_��"] = 1;
  (*FResWords)["��������������� �������"] = 2;
  (*FResWords)["�/�_����"] = 1;
  (*FResWords)["��������������� ����"] = 2;
  (*FResWords)["�/�_���"] = 1;
  (*FResWords)["��������������� �������"] = 2;
  (*FResWords)["���������������� �����"] = 2;
  (*FResWords)["��������"] = 1;
  // (*  FResWor)ds["�����"]                               = 1;
  (*FResWords)["����� �����"] = 2;
  (*FResWords)["�����"] = 1;
  (*FResWords)["������"] = 1;
  (*FResWords)["�������"] = 1;
  (*FResWords)["�����"] = 1;
  // (*FResWords)["�������"] = 1;
  (*FResWords)["��������"] = 1;
  (*FResWords)["������"] = 1;
  (*FResWords)["������"] = 1;
  (*FResWords)["������"] = 1;
  //(*FResWords)["����"] = 1;
  (*FResWords)["����"] = 1;
  // (*  FResWor)ds["���������"]                           = 1;
  (*FResWords)["��������� �������"] = 2;
  (*FResWords)["���"] = 1;
  (*FResWords)["����������"] = 1;
  // (*  FResWor)ds["�����"]                               = 1;
  (*FResWords)["������"] = 1;
  (*FResWords)["��������"] = 1;
  (*FResWords)["���"] = 1;
  (*FResWords)["���-�"] = 1;
  (*FResWords)["����������"] = 1;
  (*FResWords)["����"] = 1;
  // (*  FResWor)ds["����������"]                          = 1;
  (*FResWords)["����������"] = 1;
  (*FResWords)["���������� �����"] = 2;
  (*FResWords)["�������"] = 1;
  (*FResWords)["�����"] = 1;
//  (*FResWords)["������"] = 1;
  (*FResWords)["����"] = 1;
  (*FResWords)["�������"] = 1;
  (*FResWords)["��������"] = 1;
  (*FResWords)["�������������"] = 1;
  (*FResWords)["������������� �����"] = 2;
//  (*FResWords)["���������"] = 1;
  (*FResWords)["��������"] = 1;
  (*FResWords)["�������"] = 1;
  (*FResWords)["������"] = 1;
  (*FResWords)["����������"] = 1;
  //(*FResWords)["�������"] = 1;
  (*FResWords)["����������"] = 1;
  (*FResWords)["����"] = 1;
  (*FResWords)["������� ���������� ����"] = 3;
  (*FResWords)["�/��"] = 1;
  (*FResWords)["������� �(���) �������(�)"] = 3;
//  (*FResWords)["�������"] = 1;
  (*FResWords)["��������"] = 1;
  (*FResWords)["���������"] = 1;
  (*FResWords)["�������� ���������"] = 2;
  (*FResWords)["������"] = 1;
  (*FResWords)["��������"] = 1;
//  (*FResWords)["������������"] = 1;
  (*FResWords)["����"] = 1;
  (*FResWords)["������������ ����"] = 2;
  (*FResWords)["�������"] = 1;
  (*FResWords)["��������"] = 1;
  (*FResWords)["��������"] = 1;
  (*FResWords)["������"] = 1;
  (*FResWords)["�������"] = 1;
  // (*FResWords)["�������"]                             = 1;
  (*FResWords)["������� �������"] = 2;
  (*FResWords)["���"] = 1;
  (*FResWords)["�������"] = 1;
  (*FResWords)["�����"] = 1;
  (*FResWords)["����������"] = 1;
  (*FResWords)["����"] = 1;
  (*FResWords)["���"] = 1;
  (*FResWords)["������� �������������� ������������"] = 3;
  //(*FResWords)["����"] = 1;
  (*FResWords)["�������� �������������"] = 2;
  (*FResWords)["�������� �����"] = 1;
  (*FResWords)["�/��"] = 1;
  (*FResWords)["�������� ������������� �����������"] = 3;
  (*FResWords)["�������� ���������"] = 2;
  (*FResWords)["�/�"] = 1;
  (*FResWords)["���������"] = 1;
  (*FResWords)["�����"] = 1;
  // (*FResWords)["������"]                             = 1;
  (*FResWords)["�����"] = 1;
  (*FResWords)["�������"] = 1;
  (*FResWords)["�������"] = 1;
  (*FResWords)["��������"] = 1;
  (*FResWords)["�����"] = 1;
  (*FResWords)["����������"] = 1;
  (*FResWords)["�����"] = 1;
  (*FResWords)["�����"] = 1;
  (*FResWords)["�����"] = 1;
  (*FResWords)["����"] = 1;
  (*FResWords)["�������"] = 1;
  (*FResWords)["�����"] = 1;
  (*FResWords)["�����"] = 1;
  (*FResWords)["�����"] = 1;
  // ��������� ������ ������ �������� ������, � ������ ������ � ���� ���� :)
  (*FReplasedObjects)["���"] = "�����-���������";
  (*FReplasedObjects)["�-��"] = "�����-���������";
  (*FReplasedObjects)["�-�"] = "�����-���������";
  (*FReplasedObjects)["�.��"] = "�����-���������";
  (*FReplasedObjects)["���"] = "������";
  // (*FReplasedObjects)["��"] = "�������������";
  Text = ASrc;
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
__fastcall TNFAddrWords::~TNFAddrWords()
 {
  // dsLogBegin(__FUNC__);
  FResWords->clear();
  FReplasedObjects->clear();
  delete FResWords;
  delete FReplasedObjects;
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
bool __fastcall TNFAddrWords::IsNotBr(wchar_t ASrc)
 {
  return !((ASrc == ' ') || (ASrc == '(') || (ASrc == '{') || (ASrc == '[') || (ASrc == ')') || (ASrc == '}') ||
    (ASrc == ']'));
 }
// ---------------------------------------------------------------------------
bool __fastcall TNFAddrWords::IsLBr(wchar_t ASrc)
 {
  return ((ASrc == '(') || (ASrc == '{') || (ASrc == '['));
 }
// ---------------------------------------------------------------------------
bool __fastcall TNFAddrWords::IsRBr(wchar_t ASrc)
 {
  return ((ASrc == ')') || (ASrc == '}') || (ASrc == ']'));
 }
// ---------------------------------------------------------------------------
bool __fastcall TNFAddrWords::Cmp(UnicodeString ASrc)
 {
  // dsLogBegin(__FUNC__);
  bool RC = false;
  UnicodeString FSrc = ASrc.Trim().UpperCase();
  UnicodeString FRW, FRWF, FRWS;
  try
   {
    if (FSrc.Length())
     {
      bool IsDot = FSrc[FSrc.Length()] == '.';
      if (IsDot)
       FSrc.Delete(FSrc.Length(), 1);
      if (FSrc.Length())
       {
        StringIntMap::iterator FRC = FResWords->find(FSrc);
        RC = (FRC != FResWords->end());
        for (StringIntMap::iterator rw = FResWords->begin(); (rw != FResWords->end()) && !RC; rw++)
         {
          FRW = rw->first.Trim();
          if ((rw->second > 1) && !IsDot)
           {
            // 2 � ����� ���� �������� �� 1�1�... 1�/1�
            FRWF = FRW[1];
            FRWS = FRW[1];
            for (int i = 1; i <= FRW.Length(); i++)
             {
              if (FRW[i] == ' ')
               {
                if ((i + 1) < FRW.Length())
                 {
                  FRWF += FRW[i + 1];
                  FRWS += "/" + FRW[i + 1];
                 }
               }
             }
            RC = (FRWF == FSrc);
            if (!RC)
             RC = (FRWS == FSrc);
           }
          // �������� �� ���������� �� �����������
          if (rw->second == 1)
           {
            for (int i = 1; (i <= FRW.Length()) && !RC; i++)
             RC = (FRW.SubString(1, i) == FSrc);
            // �������� �� ���������� �� ����������� � "-"
            if (FSrc.Pos("-") && !RC)
             {
              for (int i = 1; (i <= FRW.Length()) && !RC; i++)
               for (int j = i + 2; (j <= FRW.Length()) && !RC; j++)
                RC = (FRW.SubString(1, i) + "-" + FRW.SubString(j, FRW.Length() - j + 1) == FSrc);
             }
           }
         }
       }
     }
   }
  __finally
   {
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TNFAddrWords::setText(UnicodeString ASrc)
 {
  // dsLogBegin(__FUNC__);
  FText = ASrc;
  UnicodeString FSrc = FText.UpperCase();
  UnicodeString tmp = "";
  for (int i = 1; i <= FSrc.Length(); i++)
   {
    if ((unsigned int)FSrc[i] >= 32)
     {
      if (IsNotBr(FSrc[i]) && (FSrc[i] != '.'))
       {
        tmp += FSrc[i];
       }
      else
       {
        if (FSrc[i] == '.')
         tmp += FSrc[i];
        tmp = tmp.Trim();
        if (tmp.Length())
         AddObject(GetReplasedWord(tmp), (TObject *)wtWord);
        if (IsLBr(FSrc[i]))
         AddObject(UnicodeString(FSrc[i]), (TObject *)wtLBR);
        else if (IsRBr(FSrc[i]))
         AddObject(UnicodeString(FSrc[i]), (TObject *)wtRBR);
        tmp = "";
       }
     }
   }
  tmp = tmp.Trim();
  if (tmp.Length())
   AddObject(GetReplasedWord(tmp), (TObject *)wtWord);
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
bool __fastcall TNFAddrWords::CanClear(UnicodeString ASrc)
 {
  // dsLogBegin(__FUNC__);
  bool RC = false;
  try
   {
   }
  __finally
   {
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNFAddrWords::getPreparedText()
 {
  // dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  UnicodeString tmp;
  int tmpCount;
  bool FClosed;
  try
   {
    if (FirstType == wtLBR)
     { // ������ ������� ����������� ������
      if (WordType[2] == wtRBR)
       {
        if (Cmp(tmp))
         {
          for (int i = 0; i < 3; i++)
           Delete(0);
         }
       }
     }
    if (LastType == wtRBR)
     { // ��������� ������� ����������� ������
      if (WordType[Count - 3] == wtLBR)
       {
        if (Cmp(tmp))
         {
          int id = Count - 3;
          for (int i = 0; i < 3; i++)
           Delete(id);
         }
       }
     }
    for (int i = 0; i < Count; i++)
     {
      if (Cmp(Strings[i]))
       {
        Delete(i);
        i-- ;
       }
     }
    for (int i = 0; i < Count; i++)
     {
      if (!RC.Length())
       RC = Strings[i];
      else
       RC += " " + Strings[i];
     }
   }
  __finally
   {
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNFAddrWords::getWord(int AIdx)
 {
  if ((AIdx >= 0) && (AIdx < Count))
   return Strings[AIdx];
  else
   return "";
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNFAddrWords::GetReplasedWord(UnicodeString AVal)
 {
  UnicodeString RC = AVal;
  try
   {
    StrStrMap::iterator FRC = FReplasedObjects->find(AVal.UpperCase());
    if (FRC != FReplasedObjects->end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TNFAddrWords::setWord(int AIdx, UnicodeString AVal)
 {
  // dsLogBegin(__FUNC__);
  if ((AIdx >= 0) && (AIdx < Count))
   {
    Strings[AIdx] = GetReplasedWord(AVal);
   }
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNFAddrWords::getFirst()
 {
  if (Count)
   return Strings[0];
  else
   return "";
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNFAddrWords::getLast()
 {
  if (Count)
   return Strings[Count - 1];
  else
   return "";
 }
// ---------------------------------------------------------------------------
ParseWordType __fastcall TNFAddrWords::getType(int AIdx)
 {
  if ((AIdx >= 0) && (AIdx < Count))
   return (ParseWordType)(int)Objects[AIdx];
  else
   return wtNone;
 }
// ---------------------------------------------------------------------------
ParseWordType __fastcall TNFAddrWords::getFirstType()
 {
  if (Count)
   return (ParseWordType)(int)Objects[0];
  else
   return wtNone;
 }
// ---------------------------------------------------------------------------
ParseWordType __fastcall TNFAddrWords::getLastType()
 {
  if (Count)
   return (ParseWordType)(int)Objects[Count - 1];
  else
   return wtNone;
 }
// ---------------------------------------------------------------------------
// ###########################################################################
// #                                                                         #
// #                 TNFAddrItems                                            #
// #                                                                         #
// ###########################################################################
// ---------------------------------------------------------------------------
__fastcall TNFAddrItems::TNFAddrItems(UnicodeString ASrc, wchar_t ASeparator, bool AExtended)
  : TObjectList() /* TStringList() */
 {
  // dsLogBegin(__FUNC__);
  FSeparator    = ASeparator;
  FDopSeparator = ',';
  FExtended     = AExtended;
  Text          = ASrc;
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
__fastcall TNFAddrItems::~TNFAddrItems()
 {
  // dsLogBegin(__FUNC__);
  DeleteItems();
  // dsLogEnd(__FUNC__);
  /* for (int i = 0; i < Count; i++)
   {
   if (Items[i])
   {
   delete Items[i];
   Items[i] = NULL;
   }
   }
   Text = ""; */
 }
// ---------------------------------------------------------------------------
void __fastcall TNFAddrItems::DeleteItems()
 {
  // dsLogBegin(__FUNC__);
  Clear();
  // dsLogMessage("6","", 1);
  FText = "";
  // dsLogMessage("7","", 1);
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TNFAddrItems::setText(UnicodeString ASrc)
 {
  // dsLogBegin(__FUNC__);
  DeleteItems();
  FText = Correct(ASrc).Trim();
  UnicodeString FCurItem = "";
  bool FSep;
  if (FText.Length())
   {
    for (int i = 1; i <= FText.Length(); i++)
     {
      FSep = (FText[i] == FSeparator) || (FText[i] == FDopSeparator);
      // !!!        if(!((FText[i] == ',') || (FText[i] == ' ')))
      if ((FExtended && !(FSep || (FText[i] == '.'))) || (!FExtended && !FSep))
       {
        FCurItem += FText[i];
       }
      else
       {
        if (FExtended && (FText[i] == '.'))
         {
          FCurItem += FText[i];
         }
        FCurItem = FCurItem.Trim();
        Add(new TNFAddrWords(FCurItem));
        // AddObject(FCurItem, new TNFAddrWords(FCurItem));
        FCurItem = "";
       }
     }
    FCurItem = FCurItem.Trim();
    Add(new TNFAddrWords(FCurItem));
    // AddObject(FCurItem, new TNFAddrWords(FCurItem));
   }
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TNFAddrItems::setTextEx(UnicodeString ASrc)
 {
  // dsLogBegin(__FUNC__);
  DeleteItems();
  FText = Correct(ASrc).Trim();
  UnicodeString FCurItem = "";
  bool FSep;
  if (FText.Length())
   {
    for (int i = 1; i <= FText.Length(); i++)
     {
      FSep = (FText[i] == FSeparator) || (FText[i] == FDopSeparator);
      // !!!        if(!((FText[i] == ',') || (FText[i] == ' ')))
      if ((FExtended && !(FSep || (FText[i] == '.'))) || (!FExtended && !FSep))
       {
        FCurItem += FText[i];
       }
      else
       {
        if (FExtended && (FText[i] == '.'))
         {
          FCurItem += FText[i];
         }
        FCurItem = FCurItem.Trim();
        Add(new TNFAddrWords(FCurItem));
        // AddObject(FCurItem, new TNFAddrWords(FCurItem));
        FCurItem = "";
       }
     }
    FCurItem = FCurItem.Trim();
    Add(new TNFAddrWords(FCurItem));
    // AddObject(FCurItem, new TNFAddrWords(FCurItem));
   }
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
TNFAddrWords * __fastcall TNFAddrItems::getItem(int AIdx)
 {
  if ((AIdx >= 0) && (AIdx < Count))
   return (TNFAddrWords *)Items[AIdx];
  else
   throw Exception("TNFAddrItems::getItem. Index out of range.");
 }
// ---------------------------------------------------------------------------
int __fastcall TNFAddrItems::GetFirstItemIndex()
 {
  int RC = -1;
  try
   {
    for (int i = 0; (i < Count) && (RC == -1); i++)
     {
      if (Item[i]->PreparedText.Length())
       RC = i;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TNFAddrWords * __fastcall TNFAddrItems::getFirst()
 {
  if (Count)
   return (TNFAddrWords *)Items[0];
  else
   throw Exception("TNFAddrItems::getFirst. Index out of range.");
 }
// ---------------------------------------------------------------------------
TNFAddrWords * __fastcall TNFAddrItems::getLast()
 {
  if (Count)
   return (TNFAddrWords *)Items[Count - 1];
  else
   throw Exception("TNFAddrItems::getLast. Index out of range.");
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                      TFormatedAddrDataElement                              #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
__fastcall TFormatedAddrDataElement::TFormatedAddrDataElement(UnicodeString AValue, TFormatedAddrDataType AType)
 {
  // dsLogBegin(__FUNC__);
  FData = AValue.Trim();
  FType = AType;
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                      TFormatedAddrData                                     #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
__fastcall TFormatedAddrData::TFormatedAddrData(UnicodeString ASrc, bool ASimpleParse)
 {
  // dsLogBegin(__FUNC__);
  FSrcData = ASrc.Trim();
  if (ASimpleParse)
   SimpleParse();
  else
   Parse();
  // dsLogBegin(__FUNC__);
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TFormatedAddrData::Parse()
 {
  // dsLogBegin(__FUNC__);
  UnicodeString tmp = "";
  wchar_t Ch;
  TFormatedAddrDataType FParseType;
  TFormatedAddrDataType FLastParseType;
  FParseType     = adtNone;
  FLastParseType = adtNone;
  for (int i = 1; i <= FSrcData.Length(); i++)
   {
    Ch = FSrcData[i];
    if (FParseType == adtNone)
     { // ��������� ���������, �������� �� ������ :)
      if (FLastParseType != adtNone)
       { // ��������� ���������
        if (tmp.Length())
         Add((void *)(new TFormatedAddrDataElement(tmp, FLastParseType)));
        tmp            = "";
        FLastParseType = adtNone;
       }
      if ((Ch >= '0') && (Ch <= '9'))
       { // �������� �������� �����
        tmp            = UnicodeString(Ch);
        FParseType     = adtDigit;
        FLastParseType = FParseType;
       }
      else if (((Ch >= L'�') && (Ch <= L'�')) || ((Ch >= L'�') && (Ch <= L'�')) || ((Ch == L'�') || (Ch == L'�')))
       { // �������� �������� �������
        tmp            = UnicodeString(Ch);
        FParseType     = adtString;
        FLastParseType = FParseType;
       }
      else if (Ch == L'-')
       {
        Add(new TFormatedAddrDataElement("-", adtRegion));
       }
      else if (Ch == L'/')
       {
        Add(new TFormatedAddrDataElement("/", adtDevider));
       }
      else if (Ch == L'(')
       { // �������� �������� ����� ������ ��������
        tmp            = "";
        FParseType     = adtFlatRegBegin;
        FLastParseType = FParseType;
       }
      else if (Ch == L':')
       { // �������� �������� ����� ������ ��������
        tmp            = "";
        FParseType     = adtFlatRegEnd;
        FLastParseType = FParseType;
       }
      else if (Ch == ')')
       {
       }
      else
       {
        // ������ ������
        throw Exception("������ ������� ������ ����");
       }
     }
    else if (FParseType == adtDigit)
     { // �������� �����
      if ((Ch >= L'0') && (Ch <= L'9'))
       {
        tmp += UnicodeString(Ch);
       }
      else
       {
        FParseType = adtNone;
        i-- ;
       }
     }
    else if (FParseType == adtString)
     { // �������� �������
      if (((Ch >= L'�') && (Ch <= L'�')) || ((Ch >= L'�') && (Ch <= L'�')) || ((Ch == L'�') || (Ch == L'�')))
       {
        tmp += UnicodeString(Ch);
       }
      else
       {
        FParseType = adtNone;
        i-- ;
       }
     }
    else if (FParseType == adtFlatRegBegin)
     { // �������� ����� ������ ��������
      if ((Ch >= L'0') && (Ch <= L'9'))
       {
        tmp += UnicodeString(Ch);
       }
      else
       {
        FParseType = adtNone;
        i-- ;
       }
     }
    else if (FParseType == adtFlatRegEnd)
     { // �������� ����� ������ ��������
      if ((Ch >= L'0') && (Ch <= L'9'))
       {
        tmp += UnicodeString(Ch);
       }
      else
       {
        FParseType = adtNone;
        i-- ;
       }
     }
   }
  if (FLastParseType != adtNone)
   { // ��������� ���������
    if (tmp.Length())
     Add((void *)(new TFormatedAddrDataElement(tmp, FLastParseType)));
   }
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
// #include <System.SysUtils.hpp>
void __fastcall TFormatedAddrData::SimpleParse()
 {
  // dsLogBegin(__FUNC__);
  UnicodeString tmp = "";
  wchar_t Ch;
  TFormatedAddrDataType FParseType;
  TFormatedAddrDataType FLastParseType;
  FParseType     = adtNone;
  FLastParseType = adtNone;
  String DDD = FSrcData;
  for (int i = 1; i <= FSrcData.Length(); i++)
   {
    Ch = FSrcData[i];
    if (FParseType == adtNone)
     { // ��������� ���������, �������� �� ������ :)
      if (FLastParseType != adtNone)
       { // ���-�� ��������, ��������� ���������
        if (tmp.Length())
         Add((void *)(new TFormatedAddrDataElement(tmp, FLastParseType)));
        tmp            = "";
        FLastParseType = adtNone;
       }
      tmp = Ch;
      if ((Ch >= L'0') && (Ch <= L'9'))
       { // �������� �������� �����
        FParseType     = adtDigit;
        FLastParseType = FParseType;
       }
      else if (((Ch >= L'�') && (Ch <= L'�')) || ((Ch >= L'�') && (Ch <= L'�')) || (Ch == L'�') || (Ch == L'�') ||
        (Ch == L'/') || (Ch == L'\\') || (Ch == L'-'))
       { // �������� �������� �������
        FParseType     = adtString;
        FLastParseType = FParseType;
       }
      else
       {
        tmp        = "";
        FParseType = adtNone;
       }
     }
    else if (FParseType == adtDigit)
     { // �������� �����
      if ((Ch >= L'0') && (Ch <= L'9'))
       {
        tmp += Ch;
       }
      else
       {
        FParseType = adtNone;
        i-- ;
       }
     }
    else if (FParseType == adtString)
     { // �������� �������
      if (((Ch >= L'�') && (Ch <= L'�')) || ((Ch >= L'�') && (Ch <= L'�')) || (Ch == L'�') || (Ch == L'�') ||
        (Ch == L'/') || (Ch == L'\\') || (Ch == L'-'))
       {
        tmp += Ch;
       }
      else
       {
        FParseType = adtNone;
        i-- ;
       }
     }
   }
  if (FLastParseType != adtNone)
   { // ��������� ���������
    if (tmp.Length())
     Add((void *)(new TFormatedAddrDataElement(tmp, FLastParseType)));
   }
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
TFormatedAddrDataElement * __fastcall TFormatedAddrData::FGetElement(int Idx)
 {
  // dsLogBegin(__FUNC__);
  TFormatedAddrDataElement * RC = NULL;
  try
   {
    RC = (TFormatedAddrDataElement *)TList::Items[Idx];
   }
  __finally
   {
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
TFormatedAddrDataElement * __fastcall TFormatedAddrData::FGetElementByType(TFormatedAddrDataType AType)
 {
  // dsLogBegin(__FUNC__);
  TFormatedAddrDataElement * RC = NULL;
  try
   {
    for (int i = 0; (i < Count) && !RC; i++)
     {
      if (Items[i]->Type == AType)
       RC = Items[i];
     }
   }
  __finally
   {
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
// ###########################################################################
// #                                                                         #
// #                   THouseElement, THouseElementList                      #
// #                                                                         #
// ###########################################################################
__fastcall THouseElement::THouseElement()
 {
  // dsLogBegin(__FUNC__);
  ShowDebug(UnicodeString(__FUNC__));
  Type          = htNone;
  TypeStr       = "";
  Value         = "";
  Text          = "";
  Begin         = End = 0;
  FormatedValue = NULL;
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall THouseElement::FSetValue(UnicodeString AValue)
 {
  // dsLogBegin(__FUNC__);
  FValue        = AValue;
  FormatedValue = new TFormatedAddrData(AValue);
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
__fastcall THouseElement::~THouseElement()
 {
  // dsLogBegin(__FUNC__);
  if (FormatedValue)
   delete FormatedValue;
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
__fastcall THouseElementList::THouseElementList(UnicodeString AText)
 {
  // dsLogBegin(__FUNC__);
  FHouseFormats[0] = "��?           { ������: �, �3 }", FHouseFormats[1] =
    "��?�?         { ������: 3, 2�, 34, 45�, 7�5, 53�4 }", FHouseFormats[2] =
    "��?�?/��?�?   { ������: 3/5, 3/2�, 2�/7�5, 53�4/7�5, 39�4/53�7 }", FHouseFormats[3] =
    "��?-��?       { ������: 5-7, 5-7�, 5�-7�, 19�-7, 19�-16� }", FHouseFormats[4] =
    "�-�           { ������: 1-�, 23-� }", FHouseFormats[5] =
    "��?�?-��?�?   { ������: 3-5, 3-2�, 3-34, 3-45�, 45�-7�5, 39�4-53�7 }", FHouseFormatByType[htDom] = "0,1,2,3,4";
  FHouseFormatByType[htVld]     = "0,1,2,5";
  FHouseFormatByType[htKorp]    = "0,1,2";
  FHouseFormatByType[htStr]     = "0,1,2";
  FHouseSelectorsByType[htDom]  = "/ -";
  FHouseSelectorsByType[htVld]  = "/ -";
  FHouseSelectorsByType[htKorp] = "/";
  FHouseSelectorsByType[htStr]  = "/";
  ElTypeStr[htDom]              = "� ����";
  ElTypeStr[htVld]              = "� ��������";
  ElTypeStr[htKorp]             = "� �������";
  ElTypeStr[htStr]              = "� ��������";
  UnicodeString Src = AText.Trim();
  int FPos, ItemBeg, ItemEnd;
  if (Src.Length())
   {
    UnicodeString FCurItem = "";
    UnicodeString FCurType;
    ItemBeg = ItemEnd = 1;
    for (int i = 1; i <= Src.Length(); i++)
     {
      if (Src[i] != ' ')
       FCurItem += Src[i];
      else
       {
        FCurItem = FCurItem.Trim();
        if ((FCurItem.UpperCase() == apVld) || (FCurItem.UpperCase() == apKorp) || (FCurItem.UpperCase() == apBuild))
         FCurItem += Src[i];
        else
         {
          ItemEnd = i;
          THouseElement * tmpItem = new THouseElement;
          FPos     = FCurItem.Pos(" ");
          FCurType = "";
          if (FPos)
           {
            tmpItem->Value = FCurItem.SubString(FPos + 1, FCurItem.Length() - FPos);
            FCurType       = FCurItem.SubString(1, FPos - 1).UpperCase();
           }
          else
           {
            FCurType = FCurItem.Trim().UpperCase();
            if (!((FCurType == apVld) || (FCurType == apKorp) || (FCurType == apBuild)))
             tmpItem->Value = FCurItem;
           }
          tmpItem->Type = htDom;
          if (FCurType == apVld)
           tmpItem->Type = htVld;
          else if (FCurType == apKorp)
           tmpItem->Type = htKorp;
          else if (FCurType == apBuild)
           tmpItem->Type = htStr;
          tmpItem->TypeStr = ElTypeStr[tmpItem->Type];
          tmpItem->Text    = FCurItem;
          tmpItem->Begin   = ItemBeg;
          tmpItem->End     = ItemEnd;
          Add((void *)tmpItem);
          ItemBeg  = i + 1;
          FCurItem = "";
         }
       }
     }
    if (FCurItem.Length())
     {
      ItemEnd = FCurItem.Length();
      THouseElement * tmpItem = new THouseElement;
      FPos     = FCurItem.Pos(" ");
      FCurType = "";
      if (FPos)
       {
        tmpItem->Value = FCurItem.SubString(FPos + 1, FCurItem.Length() - FPos);
        FCurType       = FCurItem.SubString(1, FPos - 1).UpperCase();
       }
      else
       {
        FCurType = FCurItem.Trim().UpperCase();
        if (!((FCurType == apVld) || (FCurType == apKorp) || (FCurType == apBuild)))
         tmpItem->Value = FCurItem;
       }
      tmpItem->Type = htDom;
      if (FCurType == apVld)
       tmpItem->Type = htVld;
      else if (FCurType == apKorp)
       tmpItem->Type = htKorp;
      else if (FCurType == apBuild)
       tmpItem->Type = htStr;
      tmpItem->TypeStr = ElTypeStr[tmpItem->Type];
      tmpItem->Text    = FCurItem;
      tmpItem->Begin   = ItemBeg;
      tmpItem->End     = ItemEnd;
      Add((void *)tmpItem);
     }
   }
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
__fastcall THouseElementList::~THouseElementList()
 {
  // dsLogBegin(__FUNC__);
  for (int i = 0; i < Count; i++)
   {
    if (Items[i])
     delete Items[i];
    TList::Items[i] = NULL;
   }
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
THouseElement * __fastcall THouseElementList::FGetElement(int Idx)
 {
  // dsLogBegin(__FUNC__);
  THouseElement * RC = NULL;
  try
   {
    if ((Idx >= 0) && (Idx < Count))
     RC = (THouseElement *)TList::Items[Idx];
   }
  __finally
   {
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
THouseElement * __fastcall THouseElementList::FGetElementByType(THouseElementType AType)
 {
  // dsLogBegin(__FUNC__);
  THouseElement * RC = NULL;
  try
   {
    for (int i = 0; (i < Count) && !RC; i++)
     {
      if (Items[i]->Type == AType)
       RC = Items[i];
     }
   }
  __finally
   {
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
THouseElement * __fastcall THouseElementList::FGetElementByPos(int CurPos)
 {
  // dsLogBegin(__FUNC__);
  THouseElement * RC = NULL;
  try
   {
    for (int i = 0; (i < Count) && !RC; i++)
     {
      if ((CurPos >= Items[i]->Begin) && (CurPos <= Items[i]->End))
       RC = Items[i];
     }
   }
  __finally
   {
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall THouseElementList::GetHouseFormatByType(THouseElementType AType, UnicodeString ATypeStr,
  UnicodeString & Capt, UnicodeString & Msg)
 {
  // dsLogBegin(__FUNC__);
  int CurFormat;
  UnicodeString CurFormats;
  Capt = "���������� ������� ��� �������� <" + ATypeStr + ">: ";
  Msg  = "���������� �����������: " + FHouseSelectorsByType[AType];
  Msg += "\n\n�������:";
  CurFormats = FHouseFormatByType[AType];
  for (int i = 1; i <= CurFormats.Length(); i++)
   {
    CurFormat = UnicodeString(CurFormats[i]).ToIntDef(-1);
    if (CurFormat != -1)
     {
      Msg += "\n  " + FHouseFormats[CurFormat];
     }
   }
  Msg += "\n�����������: \
           \n  <�> - �����;  <�> - ����� �������� ��������;  <?> - �������������� ������";
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                      TAddrListDataElement                                  #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
__fastcall TAddrListDataElement::TAddrListDataElement(UnicodeString AValue, TAddrListDataType AType)
 {
  // dsLogBegin(__FUNC__);
  UnicodeString tmp = AValue.Trim().UpperCase();
  FData = NULL;
  FType = AType;
  if (tmp == "�")
   FType = ldtAddrEven;
  else if (tmp == "�")
   FType = ldtAddrOdd;
  else
   FData = new THouseElementList(AValue);
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
__fastcall TAddrListDataElement::~TAddrListDataElement()
 {
  // dsLogBegin(__FUNC__);
  if (FData)
   delete FData;
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                      TAddrListData                                         #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
__fastcall TAddrListData::TAddrListData()
 {
  // dsLogBegin(__FUNC__);
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
__fastcall TAddrListData::~TAddrListData()
 {
  // dsLogBegin(__FUNC__);
  for (int i = 0; i < Count; i++)
   {
    if (Items[i])
     delete(TAddrListDataElement *)Items[i];
    TList::Items[i] = NULL;
   }
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TAddrListData::Parse(UnicodeString AData)
 {
  // dsLogBegin(__FUNC__);
  UnicodeString tmp = "";
  wchar_t Ch;
  TAddrListDataType FParseType;
  TAddrListDataType FLastParseType;
  FParseType     = ldtAddr;
  FLastParseType = ldtAddr;
  bool FFlatRegBegin = false;
  for (int i = 1; i <= AData.Length(); i++)
   {
    Ch = AData[i];
    if (FParseType == ldtAddr)
     { // �������� �����
      if (Ch == ',')
       { // ������� �����
        if (tmp.Length())
         Add((void *)(new TAddrListDataElement(tmp, ldtAddr)));
        tmp            = "";
        FLastParseType = FParseType;
       }
      else if ((Ch == ':') && !FFlatRegBegin)
       { // ������� ������ ����� ��������� �������
        if (tmp.Length())
         Add((void *)(new TAddrListDataElement(tmp, ldtAddrRegBegin)));
        tmp            = "";
        FParseType     = ldtAddrRegEnd;
        FLastParseType = FParseType;
       }
      else
       {
        tmp += UnicodeString(Ch);
        if (Ch == '(')
         FFlatRegBegin = true;
        if (Ch == ')')
         FFlatRegBegin = false;
       }
     }
    else if (FParseType == ldtAddrRegEnd)
     { // �������� ������ ����� ��������� �������
      if (Ch == ',')
       {
        if (tmp.Length())
         Add((void *)(new TAddrListDataElement(tmp, ldtAddrRegEnd)));
        tmp            = "";
        FParseType     = ldtAddr;
        FLastParseType = FParseType;
       }
      else
       {
        tmp += UnicodeString(Ch);
       }
     }
   }
  if (tmp.Length())
   Add((void *)(new TAddrListDataElement(tmp, FLastParseType)));
  // dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
TAddrListDataElement * __fastcall TAddrListData::FGetElement(int Idx)
 {
  // dsLogBegin(__FUNC__);
  TAddrListDataElement * RC = NULL;
  try
   {
    RC = (TAddrListDataElement *)TList::Items[Idx];
   }
  __finally
   {
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
TAddrListDataElement * __fastcall TAddrListData::FGetElementByType(TAddrListDataType AType)
 {
  // dsLogBegin(__FUNC__);
  TAddrListDataElement * RC = NULL;
  try
   {
    for (int i = 0; (i < Count) && !RC; i++)
     {
      if (Items[i]->Type == AType)
       RC = Items[i];
     }
   }
  __finally
   {
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TAddrListData::InAddr(UnicodeString AData, UnicodeString AFlat)
 {
  // dsLogBegin(__FUNC__);
  bool RC = false;
  THouseElementList * FAddr = new THouseElementList(AData);
  try
   {
    THouseElementList * FHEL;
    THouseElementList * FHEL2;
    THouseElement * FHE;
    THouseElement * FHEIn;
    int FHouse;
    int FFlat;
    int ElCount;
    UnicodeString FVal, FAddrVal;
    bool FEqu, FInAddr;
    for (int i = 0; (i < Count) && !RC; i++)
     {
      FHEL = Items[i]->Value;
      if ((Items[i]->Type == ldtAddrEven) || (Items[i]->Type == ldtAddrOdd))
       { // ׸����, ��������
        FHE = FAddr->ItemByType[htDom];
        if (FHE)
         {
          if (FHE->FormatedValue->Count)
           {
            if (FHE->FormatedValue->Items[0]->Type == adtDigit)
             {
              FHouse = FHE->FormatedValue->Items[0]->Value.ToIntDef(0);
              if (Items[i]->Type == ldtAddrEven)
               { // ׸����
                RC = !(FHouse % 2);
               }
              else
               { // ��������
                RC = (FHouse % 2);
               }
             }
           }
         }
       }
      else if (Items[i]->Type == ldtAddr)
       { // �����
        if (FAddr->Count == FHEL->Count)
         {
          FInAddr = true;
          for (int j = 0; (j < FHEL->Count) && FInAddr; j++)
           {
            FInAddr &= FAddr->Items[j]->Type == FHEL->Items[j]->Type;
            if (FInAddr && FHEL->Items[j]->FormatedValue->ItemByType[adtFlatRegBegin])
             { // ���� �������� �������
              for (int k = 0; (k < FAddr->Items[j]->FormatedValue->Count) && FInAddr; k++)
               {
                FInAddr &= FAddr->Items[j]->FormatedValue->Items[k]->Value == FHEL->Items[j]->FormatedValue->Items
                  [k]->Value;
               }
              FFlat = AFlat.ToIntDef(-1);
              if (FInAddr && (FFlat != -1))
               {
                FInAddr &= ((FFlat >= FHEL->Items[j]->FormatedValue->ItemByType[adtFlatRegBegin]->Value.ToIntDef(-1)) &&
                  (FFlat <= FHEL->Items[j]->FormatedValue->ItemByType[adtFlatRegEnd]->Value.ToIntDef(-1)));
               }
              else
               FInAddr = false;
             }
            else
             FInAddr &= FAddr->Items[j]->Value == FHEL->Items[j]->Value;
           }
          RC = FInAddr;
         }
       }
      else
       { // �������� �������
        // FHEL - ����� ��
        // FHEL2 - ����� ��
        // FAddr - ������������� �����
        i++ ;
        FHEL2   = Items[i]->Value;
        ElCount = (FHEL->Count > FHEL2->Count) ? FHEL2->Count : FHEL->Count;
        FInAddr = true;
        for (int j = 0; (j < ElCount) && FInAddr; j++)
         {
          FInAddr &= FHEL->Items[j]->Type == FHEL2->Items[j]->Type;
         }
        if (FInAddr)
         { // ������� ������� ���������
          FEqu = true;
          for (int j = 0; (j < FHEL->Count) && FInAddr; j++)
           { // ���������� ����� � ������ ������ ���������
            FHE   = FHEL->Items[j];
            FHEIn = FAddr->Items[j];
            if (FHEIn)
             {
              if (FHEIn->Type == FHE->Type)
               {
                if (FHEIn->FormatedValue->Items[0]->Type == FHE->FormatedValue->Items[0]->Type)
                 {
                  FVal     = FHE->FormatedValue->Items[0]->Value;
                  FAddrVal = FHEIn->FormatedValue->Items[0]->Value;
                  if (FHEIn->FormatedValue->Items[0]->Type == adtDigit)
                   {
                    if (FEqu)
                     FInAddr &= (FAddrVal.ToIntDef(0) >= FVal.ToIntDef(0));
                    FEqu &= FAddrVal.ToIntDef(0) == FVal.ToIntDef(0);
                   }
                  else
                   {
                    if (FEqu)
                     FInAddr &= (FAddrVal >= FVal);
                    FEqu &= FAddrVal == FVal;
                   }
                 }
                else
                 FInAddr = false;
               }
              else
               FInAddr = false;
             }
            else
             FInAddr = false;
           }
          if (FInAddr)
           { // ����� >= ������ ����� ���������
            FEqu = true;
            for (int j = 0; (j < FHEL2->Count) && FInAddr; j++)
             { // ���������� ����� �� ������ ������ ���������
              FHE   = FHEL2->Items[j];
              FHEIn = FAddr->Items[j];
              if (FHEIn)
               {
                if (FHEIn->Type == FHE->Type)
                 {
                  if (FHEIn->FormatedValue->Items[0]->Type == FHE->FormatedValue->Items[0]->Type)
                   {
                    FVal     = FHE->FormatedValue->Items[0]->Value;
                    FAddrVal = FHEIn->FormatedValue->Items[0]->Value;
                    if (FHEIn->FormatedValue->Items[0]->Type == adtDigit)
                     {
                      if (FEqu)
                       FInAddr &= (FAddrVal.ToIntDef(0) <= FVal.ToIntDef(0));
                      FEqu &= FAddrVal.ToIntDef(0) == FVal.ToIntDef(0);
                     }
                    else
                     {
                      if (FEqu)
                       FInAddr &= (FAddrVal <= FVal);
                      FEqu &= FAddrVal == FVal;
                     }
                   }
                  else
                   FInAddr = false;
                 }
                else
                 FInAddr = false;
               }
              else
               FInAddr = false;
             }
           }
         }
        RC = FInAddr;
       }
     }
   }
  __finally
   {
    delete FAddr;
   }
  // dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                      Utils                                                 #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
adresCode __fastcall KLADR::ParseAddrStr(UnicodeString AAddrStr, bool ATemplateMode)
 {
  // //dsLogBegin(__FUNC__);
  adresCode RC;
  try
   {
    try
     {
      if (AAddrStr.Trim().Length() > 23)
       {
        RC.RegID   = AAddrStr.SubString(1, 2).ToIntDef(0);
        RC.Town1ID = AAddrStr.SubString(3, 3).ToIntDef(0);
        RC.Town2ID = AAddrStr.SubString(6, 3).ToIntDef(0);
        RC.Town3ID = AAddrStr.SubString(9, 3).ToIntDef(0);
        if (AAddrStr[22] == '@')
         { // ������ 2+
          RC.FormatVer = AAddrStr.SubString(23, 2).ToIntDef(2);
          RC.State     = AAddrStr.SubString(25, 1).ToIntDef(0);
          if (RC.State)
           { // ����� ������������
            RC.StrObjType  = (TStreetObjectType)(AAddrStr.SubString(26, 1).ToIntDef(0));
            RC.FlatObjType = (TFlatObjectType)(AAddrStr.SubString(27, 1).ToIntDef(0));
            if (RC.StrObjType == sotStreet)
             RC.StreetID = AAddrStr.SubString(12, 4).ToIntDef(0);
            else if (RC.StrObjType != sotNone)
             {
              RC.StreetObjectVal = AAddrStr.SubString(12, 10);
              if (RC.StreetObjectVal.ToIntDef(-1) != -1)
               RC.StreetObjectVal = IntToStr(RC.StreetObjectVal.ToIntDef(-1));
             }
            RC.Index = AAddrStr.SubString(28, 6);
            if (!RC.Index.ToIntDef(0))
             RC.Index = "";
            int FHFPos = AAddrStr.Pos("##");
            if (FHFPos)
             { // ���� ��� � ��������
              // <���> :=  (���)? (���)? (����)? (���)?
              UnicodeString tmp = AAddrStr.SubString(FHFPos + 1, AAddrStr.Length() - FHFPos);
              FHFPos = tmp.Pos("##");
              if (FHFPos)
               { // ���� ��������
                RC.HouseFull = tmp.SubString(2, FHFPos - 2).Trim(); // ���
                RC.Flat      = tmp.SubString(FHFPos + 2, tmp.Length() - FHFPos - 1).Trim(); // ��������
               }
              else
               { // ��� ��������
                RC.HouseFull = tmp.SubString(2, tmp.Length() - 1).Trim(); // ���
               }
              GetHouseStruct(RC.HouseFull, RC.House, RC.Vld, RC.Korp, RC.Build);
             }
           }
          else
           { // ����� �� ������������
            RC.NFAddrID = AAddrStr.SubString(12, 10).ToIntDef(0);
           }
         }
        else
         { // ������ 1+
          RC.StreetID = AAddrStr.SubString(12, 4).ToIntDef(0);
          // �������� ����-��� ������� -101 "�� �������" �� �������
          if (RC.StreetID == -101 && !ATemplateMode)
           RC.StreetID = 0;
          RC.StrObjType  = sotStreet; // ���(���/�)
          RC.HouseFull   = AAddrStr.SubString(20, 4); // ���
          RC.FlatObjType = fotFlat; // ���(�� / ���� / �/�)
          RC.Flat        = AAddrStr.SubString(27, 4); // ��������
          RC.Index       = AAddrStr.SubString(31, 6); // ������
          if (!RC.Index.ToIntDef(0))
           RC.Index = "";
          int FHFPos = AAddrStr.Pos("##");
          if (FHFPos)
           { // ���� ��� � �������� � ����� �������
            UnicodeString tmp = AAddrStr.SubString(FHFPos + 1, AAddrStr.Length() - FHFPos);
            FHFPos = tmp.Pos("##");
            int HType = RC.HouseFull.ToIntDef(1); // ���(���/�)
            if (FHFPos)
             { // ���� ��������
              if (HType == 2)
               { // ��� ��� �/�
                if (RC.StreetID == -102)
                 { // � ���
                  RC.StrObjType      = sotPostOffice;
                  RC.StreetObjectVal = tmp.SubString(FHFPos + 2, tmp.Length() - FHFPos - 1).Trim(); // � ���
                  RC.HouseFull       = "";
                 }
                else if (RC.StreetID == -103)
                 { // � �/�
                  RC.StrObjType      = sotArmyUnit;
                  RC.StreetObjectVal = tmp.SubString(FHFPos + 2, tmp.Length() - FHFPos - 1).Trim(); // � �/�
                  RC.HouseFull       = "";
                 }
                else
                 { // �������, ��� �����
                 }
               }
              else
               {
                RC.HouseFull = tmp.SubString(2, FHFPos - 2).Trim(); // ���
                int FType = RC.Flat.ToIntDef(1); // ���(�� / ���� / �/�)
                if (FType == 4)
                 {
                  RC.StreetObjectVal = tmp.SubString(FHFPos + 2, tmp.Length() - FHFPos - 1).Trim(); // �/�
                  RC.StrObjType      = sotOfficeBox;
                  RC.HouseFull       = "";
                 }
                else
                 {
                  RC.FlatObjType = (TFlatObjectType) FType;
                  RC.Flat        = tmp.SubString(FHFPos + 2, tmp.Length() - FHFPos - 1).Trim(); // ��������
                 }
               }
             }
            else
             { // ��� ��������
              RC.HouseFull = tmp.SubString(2, tmp.Length() - 1).Trim(); // ���
             }
           }
          while (RC.HouseFull.Length() && RC.HouseFull[1] == '0')
           RC.HouseFull.Delete(1, 1);
          if (RC.HouseFull.Trim().Length())
           RC.HouseFull = RC.HouseFull;
          GetHouseStruct(RC.HouseFull, RC.House, RC.Vld, RC.Korp, RC.Build);
          while (RC.Flat.Length() && RC.Flat[1] == '0')
           RC.Flat.Delete(1, 1);
         }
       }
     }
    catch (...)
     {
      throw("Error parsing adres string");
     }
   }
  __finally
   {
   }
  // //dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall KLADR::GetHouseStruct(UnicodeString AHouseStr, UnicodeString & AHouse, UnicodeString & AVld,
  UnicodeString & AKorp, UnicodeString & ABuild)
 {
  // �. (���)? ���. (���)? ����. (����)? ���. (���)?
  bool RC = false;
  try
   {
    // temp = AAddrCode->House.Trim()+"#V"+AAddrCode->Vld.Trim()+"#K"+AAddrCode->Korp.Trim()+"#B"+AAddrCode->Build.Trim();
    UnicodeString tmp = AHouseStr.UpperCase().Trim();
    UnicodeString tmp2 = AHouseStr.Trim();
    if (tmp.Length())
     {
      int FVPos = tmp.Pos("#V");
      int FKPos = tmp.Pos("#K");
      int FBPos = tmp.Pos("#B");
      int FEPos = 0;
      if (FVPos)
       FEPos = FVPos - 1;
      else if (FKPos)
       FEPos = FKPos - 1;
      else if (FBPos)
       FEPos = FBPos - 1;
      else
       FEPos = tmp.Length() + 1;
      AHouse = tmp2.SubString(1, FEPos).Trim();
      if (FVPos)
       { // ���� ��������
        if (FKPos)
         FEPos = FKPos - 1;
        else if (FBPos)
         FEPos = FBPos - 1;
        else
         FEPos = tmp.Length() + 1;
        AVld = tmp2.SubString(FVPos + 2, FEPos - FVPos - 1).Trim();
       }
      if (FKPos)
       { // ���� ������
        if (FBPos)
         FEPos = FBPos - 1;
        else
         FEPos = tmp.Length() + 1;
        AKorp = tmp2.SubString(FKPos + 2, FEPos - FKPos - 1).Trim();
       }
      if (FBPos)
       { // ���� ��������
        FEPos  = tmp.Length() + 1;
        ABuild = tmp2.SubString(FBPos + 2, FEPos - FBPos - 1).Trim();
       }
      RC = AHouse.Length() + AVld.Length() + AKorp.Length() + ABuild.Length();
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall KLADR::GetHomeFullStr(adresCode AAddrCode, bool ANotSocr)
 {
  // (���)? ���. (���)? ����. (����)? ���. (���)?
  UnicodeString RC = "";
  try
   {
    if (AAddrCode.House.Trim().Length())
     {
      if (ANotSocr)
       RC += "�" + AAddrCode.House.Trim();
      else
       RC += "�. " + AAddrCode.House.Trim();
     }
    if (AAddrCode.Vld.Trim().Length())
     {
      if (ANotSocr)
       RC += " �" + AAddrCode.Vld.Trim();
      else
       RC += " ���. " + AAddrCode.Vld.Trim();
     }
    if (AAddrCode.Korp.Trim().Length())
     {
      if (ANotSocr)
       RC += " �" + AAddrCode.Korp.Trim();
      else
       RC += " ����. " + AAddrCode.Korp.Trim();
     }
    if (AAddrCode.Build.Trim().Length())
     {
      if (ANotSocr)
       RC += " �" + AAddrCode.Build.Trim();
      else
       RC += " ���. " + AAddrCode.Build.Trim();
     }
    RC = RC.Trim();
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall KLADR::ParseHouseStruct(UnicodeString AHouseStr, UnicodeString & AHouse, UnicodeString & AVld,
  UnicodeString & AKorp, UnicodeString & ABuild)
 {
  bool RC = true;
  TFormatedAddrData * FAddrNF = NULL;
  try
   {
    UnicodeString FCI;
    int a = 7;
    // typedef enum {htNone, htDom, htVld, htKorp, htStr};
    THouseElementType FState = htNone;
    THouseElementType FLastState = htDom;
    // lst
    // FAddrNF = new TNFAddrItems(AHouseStr, ' ', true);
    FAddrNF = new TFormatedAddrData(AHouseStr, true);
    for (int i = 0; i < FAddrNF->Count; i++)
     {
      // FCI = FAddrNF->Item[i]->Text.UpperCase().Trim();
      FCI = FAddrNF->Items[i]->Value.UpperCase().Trim();
      if (FCI.Length())
       {
        if (FState == htNone)
         {
          if (StrInList("�,�.,���,���.", FCI))
           FState = htDom;
          else if (StrInList("��,��.,����.,����,���,���.,�.,��������", FCI))
           FState = htVld;
          else if (StrInList("�,�.,����,����.,���,���.,������", FCI))
           FState = htKorp;
          else if (StrInList("�,�.,��,��.,���,���.,��������,���-�", FCI))
           FState = htStr;
          else
           {
            if (FLastState == htDom)
             {
              AHouse = FAddrNF->Items[i]->Value;
              if ((AHouse.Length() > 3) && (AHouse.ToIntDef(-1) == -1))
               RC = false;
              else
               FLastState = htKorp;
             }
            else if (FLastState == htKorp)
             {
              AKorp      = FAddrNF->Items[i]->Value;
              FLastState = htVld;
             }
            else if (FLastState == htVld)
             {
              // AVld       = FAddrNF->Item[i]->Text;
              AVld       = FAddrNF->Items[i]->Value;
              FLastState = htStr;
             }
            else if (FLastState == htStr)
             {
              ABuild     = FAddrNF->Items[i]->Value;
              FLastState = htNone;
             }
            else
             break;
           }
         }
        else if (FState == htDom)
         {
          AHouse     = FAddrNF->Items[i]->Value;
          FLastState = htKorp;
          FState     = htNone;
         }
        else if (FState == htKorp)
         {
          AKorp      = FAddrNF->Items[i]->Value;
          FLastState = htVld;
          FState     = htNone;
         }
        else if (FState == htVld)
         {
          AVld       = FAddrNF->Items[i]->Value;
          FLastState = htStr;
          FState     = htNone;
         }
        else if (FState == htStr)
         {
          ABuild = FAddrNF->Items[i]->Value;
          break;
         }
       }
     }
   }
  __finally
   {
    if (FAddrNF)
     delete FAddrNF;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__fastcall adresCode::adresCode()
 {
  Clear();
 };
// ---------------------------------------------------------------------------
void __fastcall adresCode::Clear()
 {
  FormatVer       = 2;
  RegID           = Town1ID = Town2ID = Town3ID = StreetID = State = NFAddrID = 0;
  StrObjType      = sotNone;
  FlatObjType     = fotNone;
  StreetObjectVal = Index = House = Vld = Korp = Build = HouseFull = Flat = "";
  NoStreets       = false;
 };
// ---------------------------------------------------------------------------
UnicodeString __fastcall adresCode::RegCodeStr()
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString temp = IntToStr(RegID);
    while (temp.Length() < 2)
     temp = "0" + temp;
    RC = temp;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall adresCode::CityCodeStr()
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString temp = IntToStr(Town1ID);
    while (temp.Length() < 3)
     temp = "0" + temp;
    RC += temp;
    temp = IntToStr(Town2ID);
    while (temp.Length() < 3)
     temp = "0" + temp;
    RC += temp;
    temp = IntToStr(Town3ID);
    while (temp.Length() < 3)
     temp = "0" + temp;
    RC += temp;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall adresCode::StreetCodeStr()
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString temp;
    if (State)
     { // ���������������
      if (StrObjType == sotStreet)
       {
        temp = IntToStr(StreetID);
        while (temp.Length() < 4)
         temp = "0" + temp;
        RC += temp + "000000";
       }
      else
       {
        temp = StreetObjectVal;
        if (temp.Length() > 10)
         temp = temp.SubString(1, 10);
        while (temp.Length() < 10)
         temp = "0" + temp;
        RC += temp;
       }
      RC += "@" + kladrFormatVer + "1";
      RC += IntToStr(StrObjType);
     }
    else
     { // �����������������
      temp = IntToStr(NFAddrID);
      while (temp.Length() < 10)
       temp = "0" + temp;
      RC += temp + "@" + kladrFormatVer + "00";
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall adresCode::AsShortString()
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString temp;
    temp = IntToStr(RegID);
    while (temp.Length() < 2)
     temp = "0" + temp;
    RC   = temp;
    temp = IntToStr(Town1ID);
    while (temp.Length() < 3)
     temp = "0" + temp;
    RC += temp;
    temp = IntToStr(Town2ID);
    while (temp.Length() < 3)
     temp = "0" + temp;
    RC += temp;
    temp = IntToStr(Town3ID);
    while (temp.Length() < 3)
     temp = "0" + temp;
    RC += temp;
    if (State)
     { // ���������������
      if (StrObjType == sotStreet)
       {
        temp = IntToStr(StreetID);
        while (temp.Length() < 4)
         temp = "0" + temp;
        RC += temp + "000000";
       }
      else
       {
        temp = StreetObjectVal;
        if (temp.Length() > 10)
         temp = temp.SubString(1, 10);
        while (temp.Length() < 10)
         temp = "0" + temp;
        RC += temp;
       }
      RC += "@" + kladrFormatVer + "1";
      RC += IntToStr(StrObjType);
      RC += IntToStr(FlatObjType);
      RC += Index;
     }
    else
     { // �����������������
      temp = IntToStr(NFAddrID);
      while (temp.Length() < 10)
       temp = "0" + temp;
      RC += temp + "@" + kladrFormatVer + "000000000##";
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall adresCode::AsString()
 {
  UnicodeString RC = "";
  try
   {
    RC += AsShortString();
    RC += HouseCodeStr();
    RC += FlatCodeStr();
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall adresCode::HouseCodeStr(bool ALeadSep)
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString temp, sep;
    if (State)
     { // ���������������
      temp = House.Trim();
      if (Vld.Trim().Length())
       temp += "#V" + Vld.Trim();
      if (Korp.Trim().Length())
       temp += "#K" + Korp.Trim();
      if (Build.Trim().Length())
       temp += "#B" + Build.Trim();
      if (ALeadSep)
       {
        if (temp.Length())
         RC += "##" + temp;
        else
         RC += "## ";
       }
      else
       {
        if (temp.Length())
         RC += temp;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall adresCode::FlatCodeStr(bool ALeadSep)
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString temp;
    if (State)
     { // ���������������
      temp = Flat.Trim();
      if (ALeadSep)
       {
        if (temp.Length())
         RC += "##" + temp;
       }
      else
       {
        if (temp.Length())
         RC += temp;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void adresCode:: operator = (adresCode AVal)
 {
  FormatVer       = AVal.FormatVer;
  RegID           = AVal.RegID;
  Town1ID         = AVal.Town1ID;
  Town2ID         = AVal.Town2ID;
  Town3ID         = AVal.Town3ID;
  StreetID        = AVal.StreetID;
  StreetObjectVal = AVal.StreetObjectVal;
  NoStreets       = AVal.NoStreets;
  State           = AVal.State;
  StrObjType      = AVal.StrObjType;
  Index           = AVal.Index;
  House           = AVal.House;
  Vld             = AVal.Vld;
  Korp            = AVal.Korp;
  Build           = AVal.Build;
  HouseFull       = AVal.HouseFull;
  FlatObjType     = AVal.FlatObjType;
  Flat            = AVal.Flat;
  NFAddrID        = AVal.NFAddrID;
 }
// ---------------------------------------------------------------------------
bool adresCode:: operator != (adresCode AVal)
 {
  bool RC = true;
  RC &= FormatVer == AVal.FormatVer;
  RC &= RegID == AVal.RegID;
  RC &= Town1ID == AVal.Town1ID;
  RC &= Town2ID == AVal.Town2ID;
  RC &= Town3ID == AVal.Town3ID;
  RC &= StreetID == AVal.StreetID;
  RC &= StreetObjectVal == AVal.StreetObjectVal;
  RC &= NoStreets == AVal.NoStreets;
  RC &= State == AVal.State;
  RC &= StrObjType == AVal.StrObjType;
  RC &= Index == AVal.Index;
  RC &= House == AVal.House;
  RC &= Vld == AVal.Vld;
  RC &= Korp == AVal.Korp;
  RC &= Build == AVal.Build;
  RC &= HouseFull == AVal.HouseFull;
  RC &= FlatObjType == AVal.FlatObjType;
  RC &= Flat == AVal.Flat;
  RC &= NFAddrID == AVal.NFAddrID;
  return !RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall KLADR::AddrInAddr(UnicodeString AUnitAddr, UnicodeString APlanAddr)
 {
  bool RC = false;
  adresCode FUnitAddr = ParseAddrStr(AUnitAddr);
  adresCode FPlanAddr = ParseAddrStr(APlanAddr);
  RC = FUnitAddr.State == FPlanAddr.State;
  if (RC)
   {
    if (FPlanAddr.StrObjType)
     { // ������� �����
      RC &= FUnitAddr.StreetID == FPlanAddr.StreetID;
      RC &= FUnitAddr.StreetObjectVal == FPlanAddr.StreetObjectVal;
      RC &= FUnitAddr.StrObjType == FPlanAddr.StrObjType;
      RC &= FUnitAddr.NoStreets == FPlanAddr.NoStreets;
     }
    if (RC)
     { // ������� ����� � ��� ��������� � ������ ��������
      // ��� ����� �� ����������� � ����������
      if (FPlanAddr.Town1ID || FPlanAddr.Town2ID || FPlanAddr.Town3ID)
       { // ������ ��������� �����
        RC &= FUnitAddr.Town1ID == FPlanAddr.Town1ID;
        RC &= FUnitAddr.Town2ID == FPlanAddr.Town2ID;
        RC &= FUnitAddr.Town3ID == FPlanAddr.Town3ID;
       }
      if (RC)
       { // ������ ��������� ����� � �� ��������� � ��������� ������� ��������
        // ��� ��������� ����� �� ���������� � ����������
        if (FPlanAddr.RegID)
         { // ������ ������
          RC &= FUnitAddr.RegID == FPlanAddr.RegID;
         }
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall KLADR::Correct(UnicodeString ASrc)
 {
  UnicodeString RC = ASrc;
  try
   {
    TReplaceFlags rFlag;
    rFlag << rfReplaceAll;
    RC = StringReplace(RC, /* C �������� */ "C", /* � ��������� */ "�", rFlag);
    RC = StringReplace(RC, /* c �������� */ "c", /* � ��������� */ "�", rFlag);
    rFlag << rfIgnoreCase;
    // ���������� �����
    RC = StringReplace(RC, "�.", "", rFlag);
    RC = StringReplace(RC, "���.", "", rFlag);
    RC = StringReplace(RC, "�.", "", rFlag);
    RC = StringReplace(RC, "���.", "", rFlag);
    // �����
    RC = StringReplace(RC, "��.", "", rFlag);
    RC = StringReplace(RC, "��.", "", rFlag);
    RC = StringReplace(RC, " �.", " ", rFlag);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
