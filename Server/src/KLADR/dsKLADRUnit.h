//---------------------------------------------------------------------------

#ifndef dsKLADRUnitH
#define dsKLADRUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
//---------------------------------------------------------------------------
#include <FireDAC.Phys.FB.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
//---------------------------------------------------------------------------
#include "dsKLADRDMUnit.h"
//---------------------------------------------------------------------------
class DECLSPEC_DRTTI TdsKLADRClass : public TDataModule
{
__published:	// IDE-managed Components
 TFDConnection *KLADRConnection;
private:	// User declarations
  TdsKLADRDM        *FDM;
  UnicodeString __fastcall GetDefAddr();
  void __fastcall SetDefAddr(UnicodeString AValue);
public:		// User declarations
  __fastcall TdsKLADRClass(TComponent* Owner, TFDConnection *AConnection, TAppOptions *AOpt);
  __fastcall ~TdsKLADRClass();
  // Find:
  // AStr - ������ ������ ��� �������������, ����� ","
  // ADefAddr - ����� �� ���������
  // AParams - 0x01 - ������������ ����������������� �����
  //           0x02 - ������������� ������� ��������
  //           0x04 - ������������� ������� ���� � ���� (�� ...)
  //           0x08 - ������������� ������� ����� � ���� (��...)
  //           0x10 - ������������� ������� ���. ������
  TJSONObject* Find(UnicodeString AStr, UnicodeString ADefAddr, int AParams);
  UnicodeString AddrCodeToText(UnicodeString ACode, int AParams);
  __property UnicodeString DefAddr = {read=GetDefAddr, write=SetDefAddr};
};
//---------------------------------------------------------------------------
#endif
