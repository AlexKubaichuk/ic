﻿// ---------------------------------------------------------------------------
// #include <vcl.h>
#pragma hdrstop

#include "dsKLADRUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "System.Classes.TPersistent"
#pragma resource "*.dfm"

// TRegClassDM *RegClassDM;
// ---------------------------------------------------------------------------
__fastcall TdsKLADRClass::TdsKLADRClass(TComponent * Owner, TFDConnection * AConnection, TAppOptions *AOpt) : TDataModule(Owner)
 {
  dsLogC(__FUNC__);
  try
   {
    ConnectDB(KLADRConnection, AOpt, GetKLADRDBName(AOpt), __FUNC__);
    FDM = new TdsKLADRDM(this, KLADRConnection, AConnection);
   }
  catch (Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogC(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
__fastcall TdsKLADRClass::~TdsKLADRClass()
 {
  dsLogD(__FUNC__);
  DisconnectDB(KLADRConnection, __FUNC__);
  Sleep(50);
  delete FDM;
  dsLogD(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
TJSONObject * TdsKLADRClass::Find(UnicodeString AStr, UnicodeString ADefAddr, int AParams)
 {
  // AFullSearch
  dsLogMessage("Find: str: \"" + AStr + "\"; DefAddr: \"" + ADefAddr + "\"; AParams: \"" + IntToStr(AParams) + "\"");
  return FDM->Find(AStr, ADefAddr, AParams);
 }
// ----------------------------------------------------------------------------
UnicodeString TdsKLADRClass::AddrCodeToText(UnicodeString ACode, int AParams)
 {
  dsLogMessage("Code: \"" + ACode + "\"; Setting: \"" + IntToStr((int)AParams) + "\"");
  return FDM->codeToText(ACode, AParams);
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsKLADRClass::GetDefAddr()
 {
  if (FDM)
   return FDM->DefAddr;
  else
   return "";
 }
// ----------------------------------------------------------------------------
void __fastcall TdsKLADRClass::SetDefAddr(UnicodeString AValue)
 {
  if (FDM)
   FDM->DefAddr = AValue;
 }
// ----------------------------------------------------------------------------
