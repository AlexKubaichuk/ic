// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsMKB.h"
// #include "ICSDATEUTIL.hpp"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
__fastcall TdsMKB::TdsMKB(TAxeXMLContainer * AXMLList)
 {
  dsLogC(__FUNC__);
  FMKBData    = new TAnsiStrMap;
  FMKBLoData  = new TAnsiStrMap;
  FMKBExtData = new TAnsiStrMap;
  TTagNode * FMKBXML = AXMLList->GetXML("mkb");
  try
   {
    FMKBData->clear();
    FMKBLoData->clear();
    FMKBExtData->clear();
    FMKBXML->Iterate(FLoadMKB);
   }
  __finally
   {
   }
  dsLogC(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
__fastcall TdsMKB::~TdsMKB()
 {
  dsLogD(__FUNC__);
  FMKBData->clear();
  FMKBLoData->clear();
  FMKBExtData->clear();
  delete FMKBData;
  delete FMKBLoData;
  delete FMKBExtData;
  dsLogD(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsMKB::FLoadMKB(TTagNode * ANode)
 {
  if (ANode->CmpName("i"))
   {
    (*FMKBData)["##" + ANode->AV["c"].UpperCase()] = ANode->AV["n"];
    (*FMKBLoData)["##" + ANode->AV["c"].UpperCase()] = ANode->AV["n"].UpperCase();
    (*FMKBExtData)["##" + ANode->AV["c"].UpperCase()] = ANode->AV["e"].UpperCase();
   }
  return false;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsMKB::Find(UnicodeString AStr)
 {
  TJSONObject * RC = new TJSONObject;
  try
   {
    try
     {
      if (AStr.Trim().Length())
       {
        UnicodeString FDataStr = AStr.Trim().UpperCase();
        UnicodeString FExtCodeStr = "";
        if (FDataStr.Pos(": "))
         {
          FExtCodeStr = GetLPartB(FDataStr, ':').Trim();
          FDataStr    = GetRPartB(FDataStr, ':').Trim();
         }
        RC->AddPair("msg", "ok");
        bool FFind = false;
        for (TAnsiStrMap::iterator i = FMKBLoData->begin(); i != FMKBLoData->end(); i++)
         {
          if (i->second.Pos(FDataStr))
           {
            FFind = true;
            RC->AddPair(i->first, (* FMKBExtData)[i->first] + ": " + (* FMKBData)[i->first]);
           }
         }
        if (!FFind)
         {
          RC->AddPair(AStr, AStr);
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsMKB::CodeToText(UnicodeString ACode)
 {
  // ��������� ������� ����������� mkb � ���������
  UnicodeString RC = "";
  try
   {
    if (ACode.Pos("##"))
     {
      TAnsiStrMap::iterator FRC = FMKBData->find(ACode);
      if (FRC != FMKBData->end())
       {
        RC = (*FMKBExtData)[FRC->first] + ": " + FRC->second;
       }
     }
    else
     RC = ACode;
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsMKB::CodeToExtCode(UnicodeString ACode)
 {
  // ��������� ������� ����������� mkb � ���������
  UnicodeString RC = "";
  try
   {
    if (ACode.Pos("##"))
     {
      TAnsiStrMap::iterator FRC = FMKBData->find(ACode);
      if (FRC != FMKBData->end())
       {
        RC = (*FMKBExtData)[FRC->first];
       }
     }
    else
     RC = ACode;
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------

