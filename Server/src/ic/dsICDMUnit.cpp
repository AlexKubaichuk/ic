// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsICDMUnit.h"
// #include "ICSDATEUTIL.hpp"
#include "PrePlaner.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TdsICDM * dsICDM;
// ---------------------------------------------------------------------------
__fastcall TdsICDM::TdsICDM(TComponent * Owner, TFDConnection * AConnection, TdsMKB * AMKB, TAxeXMLContainer * AXMLList)
  : TDataModule(Owner)
 {
  FConnection           = AConnection;
  FXMLList              = AXMLList;
  FMKB                  = AMKB;
  // !!!  UpdateCardGenCode();
 }
// ---------------------------------------------------------------------------
__fastcall TdsICDM::~TdsICDM()
 {
  // for (TdsSrvClassifDataMap::iterator i = FClassifData.begin(); i != FClassifData.end(); i++)
  // delete i->second;
  // FClassifData.clear();
  // delete FKLADR;
  // delete FMKB;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsICDM::FindEx(System::UnicodeString AId, System::UnicodeString AFindData)
 {
  if (AId.UpperCase() == "ORG")
   return new TJSONObject;
  else
   return new TJSONObject;
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsICDM::CreateTempQuery()
 {
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = FConnection;
    FQ->Connection                       = FConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  return FQ;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsICDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsICDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString AParam1Name,
  Variant AParam1Val, UnicodeString ALogComment, UnicodeString AParam2Name, Variant AParam2Val,
  UnicodeString AParam3Name, Variant AParam3Val, UnicodeString AParam4Name, Variant AParam4Val)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
      if (AParam2Name.Length())
       AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
      if (AParam3Name.Length())
       AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
      if (AParam4Name.Length())
       AQuery->ParamByName(AParam4Name)->Value = AParam4Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      UnicodeString FParams = "; Params: ";
      if (AParam1Name.Length())
       FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
      if (AParam2Name.Length())
       FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
      if (AParam3Name.Length())
       FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
      if (AParam4Name.Length())
       FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "");
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TdsICDM::GetNewCode(UnicodeString TabId)
 {
  __int64 RC = -1;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    FQ->Command->CommandKind = skStoredProc;
    FQ->SQL->Text            = "MCODE_" + TabId;
    FQ->Prepare();
    FQ->OpenOrExecute();
    RC = FQ->FieldByName("MCODE")->AsInteger;
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICDM::NewGUID()
 {
  return icsNewGUID().UpperCase();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICDM::AppendTableRowSeparator(TXMLTab * ATab)
 {
  ATab->AppendRow();
  int nLastRow = ATab->RowCount();
  // ATab->SetText( nLastRow, 1, "\n" );
  for (int i = 1; i <= ATab->ColCount(); i++)
   {
    // !!!    ATab->LeftBorder  ( nLastRow, i )->SetVisible(false);
    // !!!    ATab->RightBorder ( nLastRow, i )->SetVisible(false);
    // !!!    ATab->BottomBorder( nLastRow, i )->SetVisible(false);
    ATab->SetText(nLastRow, i, "\n");
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICDM::GetParamValue(TTagNode * ARoot, UnicodeString APName, UnicodeString ADefVal)
 {
  UnicodeString RC = ADefVal;
  try
   {
    TTagNode * FRC = ARoot->GetChildByAV("i", "n", APName);
    if (FRC)
     RC = FRC->AV["v"];
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICDM::GetF63(UnicodeString AUnitId, UnicodeString APrintParams)
 {
  const UnicodeString csPlanPeriodActualOtvodSQLCond = " ( " "( " "O.R3043 = 1 AND " // ���������� �����
    "O.R305F <= :pDatePE " ") OR " "( " "O.R3043 = 0 AND " // ��������� �����
    "O.R305F <= :pDatePE AND " "O.R3065 >= :pDatePB " ") " ") ";
  UnicodeString RC = "";
  TFDQuery * FQ = CreateTempQuery();
  TTagNode * PlDef = new TTagNode;
  TXMLDoc * FDoc = new TXMLDoc();
  UnicodeString FSQL;
  TTagNode * FParams = new TTagNode;
  try
   {
    if (APrintParams.Length())
     FParams->AsXML = APrintParams;
    const UnicodeString csFalseTxt = "�������������";
    const UnicodeString csNotCheckTxt = "�� ���������";
    // *************************************************************************
    // �������������
    // *************************************************************************
    __int64 PtId = AUnitId.ToIntDef(-1); // ��� ��������, ��� �������� ����������� ����������� �����
    UnicodeString FFull = GetParamValue(FParams, "Full", "0");
    bool FullData = false;
    bool ShortData = false;
    if (FFull.LowerCase() == "short")
     ShortData = true;
    else
     FullData = FFull.ToIntDef(0); // ������� ������������� �������� � �������� ������ ��������� ��������
    if (ShortData)
     FDoc->Open(icsPrePath(ExtractFilePath(ParamStr(0)) + "\\template\\F63Short.xml"));
    else
     FDoc->Open(icsPrePath(ExtractFilePath(ParamStr(0)) + "\\template\\F63.xml"));
    bool PrivivListCreate = 1;
    // GetParamValue(FParams, "Vac", "1").ToIntDef(1);  //������� ������������� �������� � �������� ������ ��������� ��������
    bool ProbListCreate = 1;
    // GetParamValue(FParams, "Test", "1").ToIntDef(1); //������� ������������� �������� � �������� ������ ��������� ����
    bool OtvodListCreate = GetParamValue(FParams, "Otv", "1").ToIntDef(1);
    // ������� ������������� �������� � �������� ������ ����������
    bool PlanCreate = GetParamValue(FParams, "Plan", "1").ToIntDef(1);
    // ������� ������������� �������� � �������� ���� �� ��������� ����� ( ��. PlanMonth )
    UnicodeString TbsNotCheck = GetParamValue(FParams, "tbsNotCheck", "�� ���������");
    // ������� �� ���������, ���������� ���:
    UnicodeString TbsComm = GetParamValue(FParams, "tbsComm", "����"); // ������� �����/������� �������, ���������� ���:
    UnicodeString TbsNotComm = GetParamValue(FParams, "tbsNotComm", "���");
    // ���������� �����/������� �������, ���������� ���:
    UnicodeString OtherNotCheck = GetParamValue(FParams, "otherNotCheck", ""); // ������� �� ���������, ���������� ���:
    UnicodeString OtherComm = GetParamValue(FParams, "otherComm", "+");
    // ������� �����/������� �������, ���������� ���:
    UnicodeString OtherNotComm = GetParamValue(FParams, "otherNotComm", "-");
    // ���������� �����/������� �������, ���������� ���:
    int nTabInd = 1; // ������ ������� �������
    TXMLTab * CurTab; // ������� �������
    TDate PtBirthDay; // ���� �������� ��������
    // ����������� ���������� ��������
    int nPrivivCount = 0;
    if (PrivivListCreate)
     {
      quExecParam(FQ, false, "SELECT COUNT(*) as RCOUNT FROM CLASS_1003 P WHERE P.R017A = :pPtId", "pPtId", PtId);
      nPrivivCount = FQ->FieldByName("RCOUNT")->AsInteger;
     }
    // ����������� ���������� ����
    int nProbCount = 0;
    if (ProbListCreate)
     {
      quExecParam(FQ, false, "SELECT COUNT(*) as RCOUNT FROM CLASS_102F P WHERE P.R017B = :pPtId", "pPtId", PtId);
      nProbCount = FQ->FieldByName("RCOUNT")->AsInteger;
     }
    // ����������� �������� ��������� �������
    TDate DatePB, DatePE, FPlanMonth;
    FPlanMonth = CalculatePlanPeriod(Date(), DatePB, DatePE);
    // ����������� ���������� ����������
    int nOtvodCount = 0;
    if (OtvodListCreate)
     {
      if (FullData)
       quExecParam(FQ, false, "SELECT COUNT(*) as RCOUNT FROM CLASS_3030 O WHERE O.R317C = :pPtId", "pPtId", PtId);
      else
       quExecParam(FQ, false, "SELECT COUNT(*) as RCOUNT FROM CLASS_3030 O WHERE O.R317C = :pPtId AND " +
        csPlanPeriodActualOtvodSQLCond, "pPtId", PtId, "", "pDatePB", DatePB, "pDatePE", DatePE);
      nOtvodCount = FQ->FieldByName("RCOUNT")->AsInteger;
     }
    // ����������� ���������� ����������� �������� � ����
    int nPlanItemCount = 0;
    if (PlanCreate)
     {
      FSQL = "SELECT COUNT(*) as RCOUNT FROM CLASS_1112 pl "
        "WHERE pl.R1113 = :pPtId and pl.R1115 >= :mDate and pl.R1115 <=:hDate";
      quExecParam(FQ, false, FSQL, "pPtId", PtId, "", "mDate", Date(), "hDate", Sysutils::IncMonth(Date(), 3));
      nPlanItemCount = FQ->FieldByName("RCOUNT")->AsInteger;
     }
    // *************************************************************************
    // �������� � ��������
    // *************************************************************************
    FSQL = "SELECT R001F, R0020, R002F, R0031 AS BirthDay,"
      " R0136 AS Addr, R0100 AS House, R0033 AS Flat, R0122 as Ncard," " R0148 as tlf1, R014A as tlf2, R014C as tlf3,"
      " R0093  as spComp, R0099 as spSer, R009A as spNum," " R0025 AS Org, R0026 AS OL1, R0027 AS OL2, R0028 AS OL3"
      " FROM CLASS_1000 WHERE CODE = :pPtId";
    quExecParam(FQ, false, FSQL, "pPtId", PtId);
    UnicodeString AddrStr = "";
    UnicodeString OrgStr = "";
    UnicodeString SP = "";
    // �����
    if (!FQ->FieldByName("Addr")->IsNull && FQ->FieldByName("Addr")->AsString.Length())
     {
      adresCode FAddrCode = ParseAddrStr(FQ->FieldByName("Addr")->AsString);
      if (!FQ->FieldByName("House")->IsNull && FQ->FieldByName("House")->AsString.Length())
       ParseHouseStruct(FQ->FieldByName("House")->AsString, FAddrCode.House, FAddrCode.Vld, FAddrCode.Korp,
        FAddrCode.Build);
      if (!FQ->FieldByName("Flat")->IsNull && FQ->FieldByName("Flat")->AsString.Length())
       FAddrCode.Flat = FQ->FieldByName("Flat")->AsString;
      AddrStr = GetAddrStr(FAddrCode.AsString());
      if (!FQ->FieldByName("tlf1")->IsNull && FQ->FieldByName("tlf1")->AsString.Length())
       AddrStr += ", ���. ���.: " + FQ->FieldByName("tlf1")->AsString;
      if (!FQ->FieldByName("tlf2")->IsNull && FQ->FieldByName("tlf2")->AsString.Length())
       AddrStr += ", ���. ���.: " + FQ->FieldByName("tlf2")->AsString;
      if (!FQ->FieldByName("tlf3")->IsNull && FQ->FieldByName("tlf3")->AsString.Length())
       AddrStr += ", ���. ���.: " + FQ->FieldByName("tlf3")->AsString;
     }
    // �����������
    if (!FQ->FieldByName("Org")->IsNull && FQ->FieldByName("Org")->AsString.Length())
     {
      orgCode FOrgCode;
      if (FQ->FieldByName("Org")->AsString.ToIntDef(0))
       FOrgCode.OrgID = FQ->FieldByName("Org")->AsString.ToIntDef(0);
      else
       FOrgCode = ParseOrgCodeStr(FQ->FieldByName("Org")->AsString);
      if (!FQ->FieldByName("OL1")->IsNull && FQ->FieldByName("OL1")->AsString.Length())
       FOrgCode.L1Val = FQ->FieldByName("OL1")->AsString;
      if (!FQ->FieldByName("OL2")->IsNull && FQ->FieldByName("OL2")->AsString.Length())
       FOrgCode.L2Val = FQ->FieldByName("OL2")->AsString;
      if (!FQ->FieldByName("OL3")->IsNull && FQ->FieldByName("OL3")->AsString.Length())
       FOrgCode.L3Val = FQ->FieldByName("OL3")->AsString;
      OrgStr = GetOrgStr(FOrgCode.AsString());
     }
    // �����
    if (!FQ->FieldByName("spSer")->IsNull && FQ->FieldByName("spSer")->AsString.Length())
     SP = FQ->FieldByName("spSer")->AsString;
    if (!FQ->FieldByName("spNum")->IsNull && FQ->FieldByName("spNum")->AsString.Length())
     SP += " " + FQ->FieldByName("spNum")->AsString;
    if (ShortData)
     {
      // #########################################################################
      // #                                                                       #
      // #         �������� ������� �63                                          #
      // #                                                                       #
      // #########################################################################
      // *************************************************************************
      // ����� ���������
      // *************************************************************************
      CurTab     = FDoc->GetTab(nTabInd++);
      PtBirthDay = FQ->FieldByName("BirthDay")->AsDateTime;
      CurTab->SetText(1, 1, AddrStr); // �����
      CurTab->SetText(2, 1, OrgStr); // �����������
      CurTab->SetText(3, 1, SP.Trim()); // �����
      // *************************************************************************
      // ������ ����������
      // *************************************************************************
      if (OtvodListCreate && nOtvodCount)
       {
        CurTab = FDoc->GetTab(nTabInd++);
        // ���������� ������� ������ �������
        map<int, UnicodeString>OtvodReasonMap;
        TTagNode * ndChoice = FDefXML->GetTagByUID("1066");
        if (ndChoice)
         {
          TTagNode * ndChoiceValue = ndChoice->GetFirstChild();
          while (ndChoiceValue)
           {
            OtvodReasonMap[StrToInt(ndChoiceValue->AV["value"])] = ndChoiceValue->AV["name"];
            ndChoiceValue = ndChoiceValue->GetNext();
           }
         }
        // ��� ������� ������������ ���������
        if (nOtvodCount > 1)
         CurTab->AppendRow(nOtvodCount - 1);
        FSQL = "SELECT O.R305F AS BegDate, O.R3065 AS EndDate, O.R3043 AS OType, "
          "O.R3083 AS InfList, O.R3066 AS Reason,  O.R3082 AS MKBId " "FROM CLASS_3030 O WHERE O.R317C = :pPtId";
        if (!FullData)
         FSQL += " AND " + csPlanPeriodActualOtvodSQLCond;
        FSQL += " ORDER BY O.R305F, O.R3065, O.R3043"; // , I.R003C
        quExecParam(FQ, false, FSQL, "pPtId", PtId, "", "pDatePB", DatePB, "pDatePE", DatePE);
        int nRowInd = 2;
        while (!FQ->Eof)
         {
          CurTab->SetText(nRowInd, 1, FQ->FieldByName("BegDate")->AsString); // ��
          if (!FQ->FieldByName("EndDate")->IsNull && (int)FQ->FieldByName("EndDate")->AsDateTime)
           CurTab->SetText(nRowInd, 2, FQ->FieldByName("EndDate")->AsString); // ��
          CurTab->SetText(nRowInd, 3, FQ->FieldByName("InfList")->AsString); // ��������� ��������
          // ������� ���������
          if (FQ->FieldByName("Reason")->IsNull)
           CurTab->SetText(nRowInd, 4, "--- �� ������� ---");
          else
           {
            // �����������
            if (!FQ->FieldByName("Reason")->AsInteger)
             CurTab->SetText(nRowInd, 4, FMKB->CodeToText(FQ->FieldByName("MKBId")->AsString));
            else
             CurTab->SetText(nRowInd, 4, OtvodReasonMap[FQ->FieldByName("Reason")->AsInteger]);
           }
          nRowInd++ ;
          FQ->Next();
         }
       }
      else
       FDoc->DeleteTab(nTabInd);
      // *************************************************************************
      // ������ ����
      // *************************************************************************
      if (ProbListCreate && nProbCount)
       {
        CurTab = FDoc->GetTab(nTabInd++);
        // ��� ������ �����
        UnicodeString Reac;
        FSQL = "SELECT P.R1031 AS PDate, P.R1032 AS ProbId, P.R103B AS IsDone, "
          "P.R103F AS ReacVal,R0184 as Val2, R0186 as Val3, R0188 as Val4, R018A as Val5, " "Pr.R002C AS PName, F.R008E As Fmt FROM ( CLASS_102F P INNER JOIN CLASS_002A Pr ON "
          "P.R1032 = Pr.CODE ) LEFT JOIN CLASS_0038 F ON P.R10A7 = F.CODE " "WHERE P.R017B = :pPtId ORDER BY P.R1032, P.R1031 desc";
        quExecParam(FQ, false, FSQL, "pPtId", PtId);
        int nRowInd = 2;
        int nTestCount = 2;
        int nPrevTestId = -1;
        while (!FQ->Eof)
         {
          // ��������
          if (FQ->FieldByName("ProbId")->AsInteger != nPrevTestId)
           {
            nPrevTestId = FQ->FieldByName("ProbId")->AsInteger;
            nTestCount  = 2;
           }
          if (nTestCount)
           { // 2-� ��������� �����
            CurTab->AddRow(nRowInd++);
            CurTab->SetText(nRowInd, 1, FQ->FieldByName("PDate")->AsString); // .  ����
            CurTab->SetText(nRowInd, 2, FQ->FieldByName("PName")->AsString); // .  �����
            // �������
            // ���� ������� ���������
            if (FQ->FieldByName("IsDone")->AsInteger)
             {
              // ���� ��� �������� �������
              if (FQ->FieldByName("ReacVal")->IsNull)
               Reac = csFalseTxt; // ��� �������
              else
               {
                // ���� �������� �������
                Reac = FQ->FieldByName("Fmt")->AsString + " " + FQ->FieldByName("ReacVal")->AsString;
                if (!FQ->FieldByName("Val2")->IsNull && FQ->FieldByName("Val2")->AsString.Trim().Length())
                 Reac += "; " + FQ->FieldByName("Val2")->AsString.Trim();
                if (!FQ->FieldByName("Val3")->IsNull && FQ->FieldByName("Val2")->AsString.Trim().Length())
                 Reac += "; " + FQ->FieldByName("Val3")->AsString.Trim();
                if (!FQ->FieldByName("Val4")->IsNull && FQ->FieldByName("Val2")->AsString.Trim().Length())
                 Reac += "; " + FQ->FieldByName("Val4")->AsString.Trim();
                if (!FQ->FieldByName("Val5")->IsNull && FQ->FieldByName("Val2")->AsString.Trim().Length())
                 Reac += "; " + FQ->FieldByName("Val5")->AsString.Trim();
               }
             }
            else
             Reac = csNotCheckTxt;
            CurTab->SetText(nRowInd, 3, Reac.Trim());
            nTestCount-- ;
           }
          FQ->Next();
         }
        // ������-����������� ����� ��������� ���������
        AppendTableRowSeparator(CurTab);
       }
      else
       FDoc->DeleteTab(nTabInd);
      // *************************************************************************
      // ������ ��������
      // *************************************************************************
      if (PrivivListCreate && nPrivivCount)
       {
        CurTab = FDoc->GetTab(nTabInd++);
        // ��� ������ ��������
        FSQL = "SELECT P.R1024 AS PDate, P.R1021 AS PType, P.R10AE AS IsRound, P.R1020 AS VakId, "
          " P.R1075 AS InfId, P.R1023 AS Serial,  P.R1022 AS Doza, P.R1025 AS IsDone, P.R1026 AS IsCommon, " " P.R1028 AS IsLoc, P.R1027 AS LocVal, V.R0040 AS VName, F.R008E As Fmt, I.R003C AS IName "
          "FROM ( ( CLASS_1003 P INNER JOIN CLASS_0035 V ON " " P.R1020 = V.CODE ) INNER JOIN CLASS_003A I ON P.R1075 = I.CODE "
          " ) LEFT JOIN CLASS_0038 F ON P.R10A8 = F.CODE " "WHERE P.R017A = :pPtId ORDER BY P.R1075, P.R1024 desc";
        quExecParam(FQ, false, FSQL, "pPtId", PtId);
        int nRowInd = 2;
        int nPrevInfId = -1;
        while (!FQ->Eof)
         {
          // ��������
          if (FQ->FieldByName("InfId")->AsInteger != nPrevInfId)
           {
            nPrevInfId = FQ->FieldByName("InfId")->AsInteger;
            CurTab->AddRow(nRowInd);
            CurTab->AddRow(nRowInd);
            CurTab->MergeCol(nRowInd, 1, 5);
            CurTab->SetHAlign(nRowInd, 1, 1); // ������������ �� ������
            CurTab->SetBold(nRowInd, 1, true); // bold
            CurTab->SetText(nRowInd, 1, FQ->FieldByName("IName")->AsString);
            nRowInd++ ;
            // ������ � ��������
            CurTab->SetText(nRowInd, 1, FQ->FieldByName("PDate")->AsString); // ����
            CurTab->SetText(nRowInd, 2, FQ->FieldByName("VName")->AsString); // ����
            CurTab->SetText(nRowInd, 3, FQ->FieldByName("PType")->AsString); // ���
            UnicodeString LocR;
            // ���� ������� ���������
            if (FQ->FieldByName("IsDone")->AsInteger)
             {
              if (FQ->FieldByName("InfId")->AsInteger == 1)
               { // ��������� ---------------------------------
                // ���� ��� �������� ������� �������
                if (FQ->FieldByName("LocVal")->IsNull || !FQ->FieldByName("LocVal")->AsInteger) // ���� ������� �������
                   LocR = (FQ->FieldByName("IsLoc")->AsInteger) ? TbsComm : TbsNotComm;
                else // ���� �������� ������� �������
                   LocR = FQ->FieldByName("Fmt")->AsString + " " + FQ->FieldByName("LocVal")->AsString;
                CurTab->SetText(nRowInd, 5, LocR); // ������� �������
               }
              else
               { // �� ��������� ---------------------------------
                // ���� ��� �������� ������� �������
                if (FQ->FieldByName("LocVal")->IsNull || !FQ->FieldByName("LocVal")->AsInteger) // ���� ������� �������
                   LocR = (FQ->FieldByName("IsLoc")->AsInteger) ? OtherComm : OtherNotComm;
                else // ���� �������� ������� �������
                   LocR = FQ->FieldByName("Fmt")->AsString + " " + FQ->FieldByName("LocVal")->AsString;
                CurTab->SetText(nRowInd, 5, LocR); // ������� �������
               }
             }
            else
             {
              if (FQ->FieldByName("InfId")->AsInteger == 1)
               CurTab->SetText(nRowInd, 4, TbsNotCheck);
              else
               CurTab->SetText(nRowInd, 4, OtherNotCheck);
             }
            nRowInd++ ;
           }
          FQ->Next();
         }
        // ������-����������� ����� ��������� ���������
        AppendTableRowSeparator(CurTab);
       }
      else
       FDoc->DeleteTab(nTabInd);
     }
    else
     {
      // #########################################################################
      // #                                                                       #
      // #         ������ ������� �63                                            #
      // #                                                                       #
      // #########################################################################
      // *************************************************************************
      // ����� ���������
      // *************************************************************************
      CurTab = FDoc->GetTab(nTabInd++);
      // ������������ ���
      // CurTab->SetText(1, 1, GetLPUFullName());
      // ���� ������������ ���������
      CurTab->SetText(2, 1, Date().FormatString("dd.mm.yyyy"));
      if (FQ->FieldByName("Ncard")->AsString.Length()) // ������
         CurTab->SetText(3, 1, "����������� ����� � " + FQ->FieldByName("Ncard")->AsString);
      CurTab = FDoc->GetTab(nTabInd++);
      CurTab->SetText(1, 2, FQ->FieldByName("R001F")->AsString + " " + FQ->FieldByName("R0020")->AsString + " " +
        FQ->FieldByName("R002F")->AsString); // ���
      CurTab->SetText(2, 2, FQ->FieldByName("BirthDay")->AsString); // ���� ��������
      CurTab->SetText(3, 2, AddrStr); // �����
      CurTab->SetText(4, 2, OrgStr); // �����������
      CurTab->SetText(5, 2, SP.Trim()); // �����
      PtBirthDay = FQ->FieldByName("BirthDay")->AsDateTime;
      // *************************************************************************
      // ������ ��������
      // *************************************************************************
      if (PrivivListCreate && nPrivivCount)
       {
        CurTab = FDoc->GetTab(nTabInd++);
        // ��� ������ ��������
        if (nPrivivCount > 1)
         CurTab->AppendRow(nPrivivCount - 1);
        FSQL = "SELECT P.R1024 AS PDate, P.R1021 AS PType, P.R10AE AS IsRound, P.R1020 AS VakId, "
          " P.R1075 AS InfId, P.R1023 AS Serial,  P.R1022 AS Doza, P.R1025 AS IsDone, P.R1026 AS IsCommon, " " P.R1028 AS IsLoc, P.R1027 AS LocVal, V.R0040 AS VName, F.R008E As Fmt, I.R003C AS IName "
          "FROM ( ( CLASS_1003 P INNER JOIN CLASS_0035 V ON " " P.R1020 = V.CODE ) INNER JOIN CLASS_003A I ON P.R1075 = I.CODE "
          " ) LEFT JOIN CLASS_0038 F ON P.R10A8 = F.CODE " "WHERE P.R017A = :pPtId ORDER BY P.R1075, P.R1024";
        quExecParam(FQ, false, FSQL, "pPtId", PtId);
        int nRowInd = 4;
        int nPrevInfId = -1;
        while (!FQ->Eof)
         {
          // ��������
          if (FQ->FieldByName("InfId")->AsInteger != nPrevInfId)
           {
            nPrevInfId = FQ->FieldByName("InfId")->AsInteger;
            CurTab->AddRow(nRowInd);
            if (ShortData)
             CurTab->MergeCol(nRowInd, 1, 5);
            else
             CurTab->MergeCol(nRowInd, 1, 9);
            CurTab->SetHAlign(nRowInd, 1, 1); // ������������ �� ������
            CurTab->SetBold(nRowInd, 1, true); // bold
            CurTab->SetText(nRowInd, 1, FQ->FieldByName("IName")->AsString);
            nRowInd++ ;
           }
          CurTab->SetText(nRowInd, 1, FQ->FieldByName("PDate")->AsString); // ����
          CurTab->SetText(nRowInd, 2, DateDurationToStr(PtBirthDay, FQ->FieldByName("PDate")->AsDateTime)); // �������
          CurTab->SetText(nRowInd, 3, FQ->FieldByName("VName")->AsString); // ����
          CurTab->SetText(nRowInd, 4, FQ->FieldByName("PType")->AsString); // ���
          if (FQ->FieldByName("IsRound")->AsInteger) // ���
             CurTab->SetText(nRowInd, 5, "+");
          CurTab->SetText(nRowInd, 6, FQ->FieldByName("Serial")->AsString); // �����
          CurTab->SetText(nRowInd, 7, FQ->FieldByName("Doza")->AsString); // ����
          UnicodeString CommonR, LocR;
          // ���� ������� ���������
          if (FQ->FieldByName("IsDone")->AsInteger)
           {
            if (FQ->FieldByName("InfId")->AsInteger == 1)
             { // ��������� ---------------------------------
              CommonR = (FQ->FieldByName("IsCommon")->AsInteger) ? TbsComm : TbsNotComm; // ����� �������
              // ���� ��� �������� ������� �������
              if (FQ->FieldByName("LocVal")->IsNull || !FQ->FieldByName("LocVal")->AsInteger) // ���� ������� �������
                 LocR = (FQ->FieldByName("IsLoc")->AsInteger) ? TbsComm : TbsNotComm;
              else // ���� �������� ������� �������
                 LocR = FQ->FieldByName("Fmt")->AsString + " " + FQ->FieldByName("LocVal")->AsString;
              CurTab->SetText(nRowInd, 8, CommonR); // ����� �������
              CurTab->SetText(nRowInd, 9, LocR); // ������� �������
             }
            else
             { // �� ��������� ---------------------------------
              CommonR = (FQ->FieldByName("IsCommon")->AsInteger) ? OtherComm : OtherNotComm; // ����� �������
              // ���� ��� �������� ������� �������
              if (FQ->FieldByName("LocVal")->IsNull || !FQ->FieldByName("LocVal")->AsInteger)
                // ���� ������� �������
                 LocR = (FQ->FieldByName("IsLoc")->AsInteger) ? OtherComm : OtherNotComm;
              else
                // ���� �������� ������� �������
                 LocR = FQ->FieldByName("Fmt")->AsString + " " + FQ->FieldByName("LocVal")->AsString;
              // ����� �������
              if (ShortData)
               CurTab->SetText(nRowInd, 4, CommonR);
              else
               CurTab->SetText(nRowInd, 8, CommonR);
              // ������� �������
              if (ShortData)
               CurTab->SetText(nRowInd, 5, LocR);
              else
               CurTab->SetText(nRowInd, 9, LocR);
             }
           }
          else
           {
            if (ShortData)
             {
              CurTab->MergeCol(nRowInd, 4, 5);
              if (FQ->FieldByName("InfId")->AsInteger == 1)
               CurTab->SetText(nRowInd, 4, TbsNotCheck);
              else
               CurTab->SetText(nRowInd, 4, OtherNotCheck);
             }
            else
             {
              CurTab->MergeCol(nRowInd, 8, 9);
              if (FQ->FieldByName("InfId")->AsInteger == 1)
               CurTab->SetText(nRowInd, 8, TbsNotCheck);
              else
               CurTab->SetText(nRowInd, 8, OtherNotCheck);
             }
           }
          FQ->Next();
          nRowInd++ ;
         }
        // ������-����������� ����� ��������� ���������
        AppendTableRowSeparator(CurTab);
       }
      else
       FDoc->DeleteTab(nTabInd);
      // *************************************************************************
      // ������ ����
      // *************************************************************************
      if (ProbListCreate && nProbCount)
       {
        CurTab = FDoc->GetTab(nTabInd++);
        // ��� ������ �����
        if (nProbCount > 1)
         CurTab->AppendRow(nProbCount - 1);
        FSQL = "SELECT P.R1031 AS PDate, P.R1032 AS ProbId, P.R103B AS IsDone, "
          "P.R103F AS ReacVal,R0184 as Val2, R0186 as Val3, R0188 as Val4, R018A as Val5, " "Pr.R002C AS PName, F.R008E As Fmt FROM ( CLASS_102F P INNER JOIN CLASS_002A Pr ON "
          "P.R1032 = Pr.CODE ) LEFT JOIN CLASS_0038 F ON P.R10A7 = F.CODE " "WHERE P.R017B = :pPtId ORDER BY P.R1031";
        quExecParam(FQ, false, FSQL, "pPtId", PtId);
        int nRowInd = 3;
        while (!FQ->Eof)
         {
          CurTab->SetText(nRowInd, 1, FQ->FieldByName("PDate")->AsString); // .                                ����
          if (!ShortData)
           CurTab->SetText(nRowInd, 2, DateDurationToStr(PtBirthDay, FQ->FieldByName("PDate")->AsDateTime)); // �������
          if (ShortData)
           CurTab->SetText(nRowInd, 2, FQ->FieldByName("PName")->AsString); // .                                �����
          else
           CurTab->SetText(nRowInd, 3, FQ->FieldByName("PName")->AsString); // .                                �����
          // �������
          UnicodeString Reac;
          // ���� ������� ���������
          if (FQ->FieldByName("IsDone")->AsInteger)
           {
            // ���� ��� �������� �������
            if (FQ->FieldByName("ReacVal")->IsNull)
             Reac = csFalseTxt; // ��� �������
            else
             {
              // ���� �������� �������
              Reac = FQ->FieldByName("Fmt")->AsString + " " + FQ->FieldByName("ReacVal")->AsString;
              if (!FQ->FieldByName("Val2")->IsNull && FQ->FieldByName("Val2")->AsString.Trim().Length())
               Reac += "; " + FQ->FieldByName("Val2")->AsString.Trim();
              if (!FQ->FieldByName("Val3")->IsNull && FQ->FieldByName("Val2")->AsString.Trim().Length())
               Reac += "; " + FQ->FieldByName("Val3")->AsString.Trim();
              if (!FQ->FieldByName("Val4")->IsNull && FQ->FieldByName("Val2")->AsString.Trim().Length())
               Reac += "; " + FQ->FieldByName("Val4")->AsString.Trim();
              if (!FQ->FieldByName("Val5")->IsNull && FQ->FieldByName("Val2")->AsString.Trim().Length())
               Reac += "; " + FQ->FieldByName("Val5")->AsString.Trim();
             }
           }
          else
           Reac = csNotCheckTxt;
          if (ShortData)
           CurTab->SetText(nRowInd, 3, Reac.Trim());
          else
           CurTab->SetText(nRowInd, 4, Reac.Trim());
          FQ->Next();
          nRowInd++ ;
         }
        // ������-����������� ����� ��������� ���������
        AppendTableRowSeparator(CurTab);
       }
      else
       FDoc->DeleteTab(nTabInd);
      // *************************************************************************
      // ������ ����������
      // *************************************************************************
      if (OtvodListCreate && nOtvodCount)
       {
        CurTab = FDoc->GetTab(nTabInd++);
        // ���������� ������� ������ �������
        map<int, UnicodeString>OtvodReasonMap;
        TTagNode * ndChoice = FDefXML->GetTagByUID("1066");
        if (ndChoice)
         {
          TTagNode * ndChoiceValue = ndChoice->GetFirstChild();
          while (ndChoiceValue)
           {
            OtvodReasonMap[StrToInt(ndChoiceValue->AV["value"])] = ndChoiceValue->AV["name"];
            ndChoiceValue = ndChoiceValue->GetNext();
           }
         }
        // ��� ������� ������������ ���������
        if (nOtvodCount > 1)
         CurTab->AppendRow(nOtvodCount - 1);
        FSQL = "SELECT O.R305F AS BegDate, O.R3065 AS EndDate, O.R3043 AS OType, "
          "O.R3083 AS InfList, O.R3066 AS Reason,  O.R3082 AS MKBId " "FROM CLASS_3030 O WHERE O.R317C = :pPtId";
        if (!FullData)
         FSQL += " AND " + csPlanPeriodActualOtvodSQLCond;
        FSQL += " ORDER BY O.R305F, O.R3065, O.R3043"; // , I.R003C
        if (!FullData)
         quExecParam(FQ, false, FSQL, "pPtId", PtId, "", "pDatePB", DatePB, "pDatePE", DatePE);
        else
         quExecParam(FQ, false, FSQL, "pPtId", PtId);
        int nRowInd = 3;
        while (!FQ->Eof)
         {
          CurTab->SetText(nRowInd, 1, FQ->FieldByName("BegDate")->AsString); // ��
          if (!FQ->FieldByName("EndDate")->IsNull && (int)FQ->FieldByName("EndDate")->AsDateTime)
           CurTab->SetText(nRowInd, 2, FQ->FieldByName("EndDate")->AsString); // ��
          CurTab->SetText(nRowInd, 3, FQ->FieldByName("InfList")->AsString); // ��������� ��������
          // ������� ���������
          if (FQ->FieldByName("Reason")->IsNull)
           CurTab->SetText(nRowInd, 4, "--- �� ������� ---");
          else
           {
            // �����������
            if (!FQ->FieldByName("Reason")->AsInteger)
             CurTab->SetText(nRowInd, 4, FMKB->CodeToText(FQ->FieldByName("MKBId")->AsString));
            else
             CurTab->SetText(nRowInd, 4, OtvodReasonMap[FQ->FieldByName("Reason")->AsInteger]);
           }
          nRowInd++ ;
          FQ->Next();
         }
        // ������-����������� ����� ��������� ���������
        AppendTableRowSeparator(CurTab);
       }
      else
       FDoc->DeleteTab(nTabInd);
      // *************************************************************************
      // ����
      // *************************************************************************
      if (PlanCreate && nPlanItemCount)
       {
        CurTab = FDoc->GetTab(nTabInd++);
        // ��� ������� �������� �����
        if (nPlanItemCount > 1)
         CurTab->AppendRow(nPlanItemCount - 1);
        TTagNode * itNode = PlDef->GetFirstChild();
        TTagNode * itNode1;
        FSQL = "SELECT pl.R1115 AS PlDate, Pl.R1116 as PlType, Pl.R3187 AS PlInf, "
          "i.r003c as InfName, Pl.R1118 AS PlMIBP, Pl.R1117 AS PlVacType " "FROM CLASS_1112 pl  left join class_003a i on (i.code=Pl.R3187) "
          "WHERE pl.R1113 = :pPtId and pl.R1115 >= :mDate and pl.R1115 <=:hDate";
        FSQL += " ORDER BY PlDate "; // , I.R003C
        quExecParam(FQ, false, FSQL, "pPtId", PtId, "", "mDate", Date(), "hDate", Sysutils::IncMonth(Date(), 3));
        int nRowInd = 3;
        while (!FQ->Eof)
         {
          // ���� �����
          CurTab->SetText(nRowInd, 1, FQ->FieldByName("PlDate")->AsDateTime.FormatString("dd.mm.yyyy"));
          switch (FQ->FieldByName("PlType")->AsInteger)
           {
           case 1:
            CurTab->SetText(nRowInd, 2, "��������");
            break;
           case 2:
            CurTab->SetText(nRowInd, 2, "�����");
            break;
           case 3:
            CurTab->SetText(nRowInd, 2, "��������");
            break;
           }
          // CurTab->SetText(nRowInd, 2, itNode->AV["name"]);
          // ����
          CurTab->SetText(nRowInd, 3, FQ->FieldByName("PlVacType")->AsString);
          CurTab->SetText(nRowInd, 4, FQ->FieldByName("InfName")->AsString);
          /*
           if (itNode->Count)
           {
           UnicodeString FVType = itNode->AV["vactype"] + "$br$";
           itNode1 = itNode->GetFirstChild();
           while (itNode1)
           {
           FVType += itNode1->AV["vactype"] + "$br$";
           itNode1 = itNode1->GetNext();
           }
           CurTab->SetText(nRowInd, 3, FVType);
           }
           else
           CurTab->SetText(nRowInd, 3, itNode->AV["vactype"]);
           itNode = itNode->GetNext(); */
          nRowInd++ ;
          FQ->Next();
         }
        // ������-����������� ����� ��������� ���������
        // AppendTableRowSeparator(CurTab);
       }
      else
       FDoc->DeleteTab(nTabInd);
     }
    RC = FDoc->AsString;
   }
  __finally
   {
    delete FParams;
    delete FDoc;
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICDM::GetPlanPrintData(__int64 ACode, UnicodeString & ASpecGUI, TTagNode * AFilter, TDate & AFrDate,
  TDate & AToDate)
 {
  __int64 RC = -1;
  TFDQuery * FQ = CreateTempQuery();
  TTagNode * FFilter = new TTagNode;
  FFilter->Encoding = "utf-8";
  int FPlanType;
  // TTagNode *FCondRules = FFilter->GetChildByName("condrules", true);
  try
   {
    // {"0083":"0","00EF":"1","00B3":"19","type":"0","planperiod":"1","planprintformat":"10320938-0000BEC0-0B7C"}
    quExecParam(FQ, false, "Select R318A, R318B as Name, R318C as PlanParam From CLASS_3188 Where code=:pCode",
      "pCode", ACode);
    if (FQ->RecordCount)
     {
      TJSONObject * FPlanParam = (TJSONObject *)TJSONObject::ParseJSONValue(FQ->FieldByName("PlanParam")->AsString);
      TDate FPlanPerod = FQ->FieldByName("R318A")->AsDateTime;
      ASpecGUI = JSONToString(FPlanParam, "planprintformat");
      // ((TJSONString *)FPlanParam->Values["planprintformat"])->Value();
      FPlanType = JSONToInt(FPlanParam, "type", 0); // ((TJSONString *)FPlanParam->Values["type"])->Value().ToIntDef(0);
      // unsigned short FCY, FCM, FCD;
      // FPlanPerod.DecodeDate(& FCY, & FCM, & FCD);
      int PlanPer = JSONToInt(FPlanParam, "planperiod", 0);
      UnicodeString FMainName = FQ->FieldByName("Name")->AsString + "##" + FPlanPerod.FormatString("mmmm yyyy");
      AFrDate = FPlanPerod;
      if ((FPlanType == 3) && (PlanPer > 1))
       { // ������� �� planperiod ������� �����
        AToDate = Sysutils::IncMonth(AFrDate, PlanPer - 1);
        AToDate = TDate(IntToStr(DaysInMonth(AToDate)) + AToDate.FormatString(".mm.yyyy"));
        FMainName += " - " + AToDate.FormatString("mmmm yyyy");
       }
      else
       AToDate = TDate(IntToStr(DaysInMonth(FPlanPerod)) + FPlanPerod.FormatString(".mm.yyyy"));
      TTagNode * IERefNode, *TextNode;
      // <!ELEMENT region (passport,(condruleslist|condrules))>
      // <!ELEMENT condruleslist (condrules,tabcondrules)>
      // <!ELEMENT tabcondrules (condrules+)>
      FFilter->AsXML =
        "<region> <passport GUI='17372323-0000E062-85AC' mainname='' autor='' version='1' release='0' timestamp='20151207235535' defversion='3.0.0'/> <condruleslist><condrules uniontype='and'/><tabcondrules/></condruleslist></region>";
      TTagNode * Passport = FFilter->GetChildByName("passport");
      TTagNode * CRList = FFilter->GetChildByName("condruleslist");
      // TTagNode * CondRules = FFilter->GetChildByName("condrules");
      TTagNode * CondRules = CRList->GetChildByName("condrules");
      TTagNode * TabCR = CRList->GetChildByName("tabcondrules");
      Passport->AV["mainname"] = FMainName;
      if ((FPlanType >= 0) && (FPlanType < 5))
       {
        UnicodeString FFlVal;
        if (!FPlanType)
         {
          // �������: ������������� ��� / �������
          FFlVal = JSONToString(FPlanParam, "R00F1");
          if (FFlVal.Length())
           {
            IERefNode =
              AddIERef(CondRules, "<IERef uid='0001' ref='0E291426-00005882-2493.033C' not='0' incltoname='1'/>");
            AddIEText(IERefNode, "���= ���", FFlVal, "0");
            FFlVal = JSONToString(FPlanParam, "R00B3");
            if (FFlVal.Length())
             AddIEText(IERefNode, "�������=�������", FFlVal, "1");
            AddIESQLText(IERefNode);
           }
         }
        else
         {
          // �������: �����������
          FFlVal = JSONToString(FPlanParam, "R0025");
          if (FFlVal.Length())
           {
            IERefNode =
              AddIERef(CondRules, "<IERef uid='0001' ref='0E291426-00005882-2493.001F' not='0' incltoname='1'/>");
            AddIEText(IERefNode, "�����������= �����������", FFlVal, "5");
            AddIESQLText(IERefNode);
           }
         }
        // �������: ���������� ������
        FFlVal = JSONToString(FPlanParam, "R0023");
        if (FFlVal.Length())
         {
          IERefNode =
            AddIERef(CondRules, "<IERef uid='0002' ref='0E291426-00005882-2493.0564' not='0' incltoname='1'/>");
          AddIEText(IERefNode, "���������� ������=������", FFlVal, "0");
          AddIESQLText(IERefNode);
         }
        // �������: �������� ������
        IERefNode = AddIERef(CondRules, "<IERef uid='0003' ref='0E291426-00005882-2493.0110' not='0' incltoname='1'/>");
        AddIEText(IERefNode, "�������� ������", "0", "0");
        AddIEText(IERefNode, AFrDate.FormatString("mmmm yyyy"), AFrDate.FormatString("dd.mm.yyyy"), "1");
        AddIEText(IERefNode, AToDate.FormatString("mmmm yyyy"), AToDate.FormatString("dd.mm.yyyy"), "2");
        AddIESQLText(IERefNode);
        // �������: ��� �����
        IERefNode = AddIERef(CondRules, "<IERef uid='0004' ref='0E291426-00005882-2493.056F' not='0' incltoname='1'/>");
        AddIEText(IERefNode, "���=���", IntToStr(FPlanType + 1), "0");
        AddIESQLText(IERefNode);
        // ��������� ������� ��� ������/�������
        // ====== ���� (�� ���������) ========================================
        CondRules           = TabCR->AddChild("condrules");
        CondRules->AV["mr"] = "replace";
        // <IS uid='007A' name='���� (�� ���������)' ref='40381E23-92155860-4448.1084' maintable='CLASS_1084' tabalias='PLN' keyfield='CODE'>
        CondRules->AV["ref"] = "0E291426-00005882-2493.007A";
        // <IE uid='0570' name='��� ����� (��)' procname='RegVal' param='0569'>
        IERefNode = AddIERef(CondRules, "<IERef uid='0005' ref='0E291426-00005882-2493.0570' not='0' incltoname='1'/>");
        AddIEText(IERefNode, "��=��", IntToStr(ACode), "0");
        AddIESQLText(IERefNode);
        // ====== ���� (�� ����)      ========================================
        // <root>
        // <fl ref='328F'/>
        CondRules           = TabCR->AddChild("condrules");
        CondRules->AV["mr"] = "replace";
        // < IS uid = '053D' name = '���� (�� ����)' ref = '40381E23-92155860-4448.3084' maintable = 'CLASS_3084' tabalias = 'PLNC' keyfield = 'CODE' >
        CondRules->AV["ref"] = "0E291426-00005882-2493.053D";
        // <IE uid='056E' name='���� (�� ����): ��� �����' procname='RegVal' param='056B'>
        IERefNode = AddIERef(CondRules, "<IERef uid='0006' ref='0E291426-00005882-2493.056E' not='0' incltoname='1'/>");
        AddIEText(IERefNode, "��=��", IntToStr(ACode), "0");
        AddIESQLText(IERefNode);
        // //�������: ��� �����
        // IERefNode = AddIERef(CondRules, "<IERef uid='0005' ref='0E291426-00005882-2493.056F' not='0' incltoname='1'/>");
        // AddIEText(IERefNode, "���=���", IntToStr(FPlanType + 1), "0");
        // AddIESQLText(IERefNode);
       }
     }
    AFilter->AsXML = FFilter->AsXML;
   }
  __finally
   {
    delete FFilter;
    DeleteTempQuery(FQ);
   }
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsICDM::AddIERef(TTagNode * AOwner, UnicodeString ADef)
 {
  TTagNode * RC = NULL;
  try
   {
    RC        = AOwner->AddChild("IERef");
    RC->AsXML = ADef;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsICDM::AddIEText(TTagNode * AOwner, UnicodeString AName, UnicodeString AVal, UnicodeString ARef)
 {
  TTagNode * RC = NULL;
  try
   {
    RC              = AOwner->AddChild("text");
    RC->AV["name"]  = AName;
    RC->AV["value"] = AVal;
    RC->AV["ref"]   = ARef;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsICDM::AddIESQLText(TTagNode * AOwner)
 {
  TTagNode * RC = NULL;
  try
   {
    RC             = AOwner->AddChild("text");
    RC->AV["name"] = "sql";
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICDM::GetVacDefDoze(TDate APrivDate, TDate APatBirthday, UnicodeString AVacCode)
 {
  UnicodeString RC = "0,00";
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    if (AVacCode.ToIntDef(-5) != -5)
     {
      quExecParam(FQ, false,
        "Select R0195 as VacDoze, R0196 as MinAge, R0197 as MaxAge From CLASS_0193 Where R0194=:pR0194", "pR0194",
        AVacCode);
      UnicodeString FMin, FMax, FVal;
      dsLogMessage("", __FUNC__, 5);
      FVal = DateDiffToPeriodStr(APrivDate, APatBirthday);
      dsLogMessage("Age = " + FVal, __FUNC__, 5);
      bool FInRange = false;
      while (!FQ->Eof && !FInRange)
       {
        FMin = "0/0/0";
        FMax = "999/11/31";
        if (!FQ->FieldByName("MinAge")->IsNull)
         FMin = FQ->FieldByName("MinAge")->AsString;
        if (!FQ->FieldByName("MaxAge")->IsNull)
         FMax = FQ->FieldByName("MaxAge")->AsString;
        dsLogMessage("FMin = " + FMin + " FMax = " + FMax);
        FInRange = PeriodInRange(FMin, FMax, FVal);
        if (FInRange)
         RC = FQ->FieldByName("VacDoze")->AsString;
        FQ->Next();
       }
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICDM::SetMainLPU(__int64 ALPUCode)
 {
  if (ALPUCode)
   { // ������� ���
    TFDQuery * FQ = CreateTempQuery();
    try
     {
      quExec(FQ, false, "Update CLASS_00C7 Set R0104 = 0");
      quExecParam(FQ, false, "Update CLASS_00C7 Set R0104 = 1 Where code=:pCode", "pCode", ALPUCode);
      if (FQ->Transaction->Active)
       FQ->Transaction->Commit();
     }
    __finally
     {
      DeleteTempQuery(FQ);
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICDM::UpdateCardGenCode()
 {
  FUpdateCardGenCode("3003");
  FUpdateCardGenCode("1003");
  FUpdateCardGenCode("102F");
  FUpdateCardGenCode("1100");
  FUpdateCardGenCode("3030");
  FUpdateCardGenCode("1030");
  FUpdateCardGenCode("1112");
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICDM::FUpdateCardGenCode(UnicodeString AGenId)
 {
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    UnicodeString GenNum = "1";
    quExec(FQ, false, "SELECT Max(code) as MCODE FROM CLASS_" + AGenId);
    if (FQ->RecordCount)
     {
      if (!FQ->FieldByName("MCODE")->IsNull)
       GenNum = FQ->FieldByName("MCODE")->AsString;
     }
    quExec(FQ, false, "ALTER SEQUENCE GEN_" + AGenId + "_ID RESTART WITH " + GenNum);
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TdsICDM::GetMainLPUCode(const UnicodeString ACode)
 {
  __int64 RC = 0;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    quExecParam(FQ, false, "SELECT code FROM CLASS_00C7 Where Upper(r011E)=:pCode", "pCode", ACode.UpperCase());
    if (FQ->RecordCount)
     RC = FQ->FieldByName("code")->AsInteger;
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICDM::UpdateSearch()
 {
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    quExec(FQ, true, "Delete from search_data");
    quExec(FQ, true, "Delete from search_addr_data");
    quExec(FQ, true, "Delete from search_org_data");
    quExec(FQ, true, "Update class_1000 set code=code");
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
 }
// ----------------------------------------------------------------------------
void __fastcall TdsICDM::GetExtCount(UnicodeString & AExtCount)
 {
  TFDQuery * FQ = CreateTempQuery();
  TStringList * FTmpl = new TStringList;
  try
   {
    UnicodeString tmplPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\template\\cc.tmpl");
    if (FileExists(tmplPath))
     FTmpl->LoadFromFile(tmplPath);
    tmplPath = FTmpl->Text;
    System::Sysutils::TReplaceFlags flRep;
    flRep << rfReplaceAll << rfIgnoreCase;
    UnicodeString v1, v2, v3, v4, v5, v6;
    __int64 VC = 0;
    v1 = v2 = v3 = v4 = v5 = v6 = "0";
    quExec(FQ, true, "Select Count(*) as RCOUNT, R32B3 From class_1000 group by R32B3");
    while (!FQ->Eof)
     {
      if (FQ->FieldByName("r32B3")->AsInteger == 1)
       v1 = FQ->FieldByName("RCOUNT")->AsString;
      else if (FQ->FieldByName("r32B3")->AsInteger == 2)
       v2 = FQ->FieldByName("RCOUNT")->AsString;
      else if (FQ->FieldByName("r32B3")->AsInteger == 3)
       v3 = FQ->FieldByName("RCOUNT")->AsString;
      else if (FQ->FieldByName("r32B3")->AsInteger == 4)
       v4 = FQ->FieldByName("RCOUNT")->AsString;
      else if (FQ->FieldByName("r32B3")->AsInteger == 5)
       v5 = FQ->FieldByName("RCOUNT")->AsString;
      else if (FQ->FieldByName("r32B3")->AsInteger == 6)
       v6 = FQ->FieldByName("RCOUNT")->AsString;
      VC += FQ->FieldByName("RCOUNT")->AsInteger;
      FQ->Next();
     }
    tmplPath = StringReplace(tmplPath, "__UC__", v1, flRep); // ������������� �� �������' value='1'/>
    tmplPath  = StringReplace(tmplPath, "__OC__", v2, flRep); // ������������� �� �����������' value='2'/>
    tmplPath  = StringReplace(tmplPath, "__PC__", v3, flRep); // ������������� �� ������' value='3'/>
    tmplPath  = StringReplace(tmplPath, "__KC__", v4, flRep); // ��������������' value='4'/>
    tmplPath  = StringReplace(tmplPath, "__AC__", v5, flRep); // �����' value='5'/>
    tmplPath  = StringReplace(tmplPath, "__NC__", v6, flRep); // �� �������������' value='6'/>
    tmplPath  = StringReplace(tmplPath, "__CC__", IntToStr(VC), flRep);
    AExtCount = tmplPath;
   }
  __finally
   {
    DeleteTempQuery(FQ);
    delete FTmpl;
   }
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsICDM::GetAddrStr(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetAddrStr)
     {
      RC = FOnGetAddrStr(ACode, 0x07E);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsICDM::GetOrgStr(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetOrgStr)
     {
      RC = FOnGetOrgStr(ACode, 0);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsICDM::GetPlanVacVar(UnicodeString AUCode, TDate ADate)
 {
  UnicodeString RC = "";
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    TDate FDate = TDate("01." + ADate.FormatString("mm.yyyy"));
    TDate FDate2 = Sysutils::IncMonth(FDate, 1);
    quExecParam(FQ, true, "Select r3187, r1117 From class_1112 Where r1113=:pUCode and (R31A2=:pR31A2 or R31A2=:pR31A22)", "pUCode", AUCode,
      "", "pR31A2", FDate, "pR31A22",FDate2);
    while (!FQ->Eof)
     {
      if (RC.Length())
       RC += ";" + FQ->FieldByName("r3187")->AsString + "." + FQ->FieldByName("r1117")->AsString;
      else
       RC = FQ->FieldByName("r3187")->AsString + "." + FQ->FieldByName("r1117")->AsString;
      FQ->Next();
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
