//---------------------------------------------------------------------------

#ifndef dsICDMUnitH
#define dsICDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <FireDAC.Phys.FB.hpp>
//#include <FireDAC.Comp.Client.hpp>
//#include <FireDAC.Phys.hpp>
//#include <FireDAC.Phys.Intf.hpp>
//#include <FireDAC.Stan.Def.hpp>
//#include <FireDAC.Stan.Error.hpp>
//#include <FireDAC.Stan.Intf.hpp>
//#include <FireDAC.Stan.Option.hpp>
//#include <FireDAC.UI.Intf.hpp>
//#include <Data.DB.hpp>
//#include <FireDAC.Comp.DataSet.hpp>
//#include <FireDAC.DApt.hpp>
//#include <FireDAC.DApt.Intf.hpp>
//#include <FireDAC.DatS.hpp>
//#include <FireDAC.Stan.Async.hpp>
//#include <FireDAC.Stan.Param.hpp>
//#include <FireDAC.Stan.Pool.hpp>
//#include <FireDAC.Phys.FBDef.hpp>
//#include <FireDAC.Comp.UI.hpp>
//#include <FireDAC.VCLUI.Wait.hpp>
//---------------------------------------------------------------------------
#include <System.JSON.hpp>
#include "ICUtils.h"
#include "IcsXMLDoc.h"
#include "dsKLADRUnit.h"
#include "XMLContainer.h"
#include "dsRegDMUnit.h"
#include "dsSrvClassifUnit.h"
#include "OrgParsers.h"
#include "icsLog.h"
#include "dsMKB.h"
#include "kabQueryUtils.h"
//---------------------------------------------------------------------------
class TdsICDM : public TDataModule
{
__published:	// IDE-managed Components
private:	// User declarations
  TFDConnection *FConnection;
//  TdsKLADRClass *FKLADR;
  TTagNode *FDefXML;
  TList *FClList;
  TList *FFlList;
//  TdsSrvClassifDataMap  FClassifData;

  TdsMKB      *FMKB;
  __int64 __fastcall GetNewCode(UnicodeString TabId);
  UnicodeString __fastcall NewGUID();
  TAxeXMLContainer *FXMLList;

  TdsRegGetStrValByCodeEvent FOnGetAddrStr;
  TdsRegGetStrValByCodeEvent FOnGetOrgStr;

  void __fastcall AppendTableRowSeparator( TXMLTab *ATab );

//  bool __fastcall FLoadMKB(TTagNode* ANode);
  UnicodeString __fastcall GetParamValue(TTagNode *ARoot, UnicodeString APName, UnicodeString ADefVal);
  UnicodeString __fastcall GetAddrStr(UnicodeString ACode);
  UnicodeString __fastcall GetOrgStr(UnicodeString ACode);

  TTagNode * __fastcall AddIERef(TTagNode * AOwner, UnicodeString  ADef);
  TTagNode * __fastcall AddIEText(TTagNode * AOwner, UnicodeString  AName, UnicodeString  AVal, UnicodeString  ARef);
  TTagNode * __fastcall AddIESQLText(TTagNode * AOwner);
  void __fastcall UpdateCardGenCode();
  void __fastcall FUpdateCardGenCode(UnicodeString AGenId);

public:		// User declarations
  __fastcall TdsICDM(TComponent* Owner, TFDConnection *AConnection, TdsMKB *AMKB, TAxeXMLContainer *AXMLList);
  __fastcall ~TdsICDM();

  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AParam1Name,      Variant AParam1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AParam2Name = "", Variant AParam2Val = 0,
                               UnicodeString AParam3Name = "", Variant AParam3Val = 0,
                               UnicodeString AParam4Name = "", Variant AParam4Val = 0);

  TJSONObject*  __fastcall FindEx(System::UnicodeString AId, System::UnicodeString AFindData);
  UnicodeString __fastcall GetF63(UnicodeString AUnitId, UnicodeString APrintParams);
  UnicodeString __fastcall GetVacDefDoze(TDate APrivDate, TDate APatBirthday, UnicodeString AVacCode);
  void          __fastcall SetMainLPU(__int64 ALPUCode);
  __int64       __fastcall GetMainLPUCode(const UnicodeString ACode);

  void __fastcall UpdateSearch();
  void __fastcall GetExtCount(UnicodeString &AExtCount);
  UnicodeString __fastcall GetPlanVacVar(UnicodeString AUCode, TDate ADate);

  void __fastcall GetPlanPrintData(__int64 ACode, UnicodeString  &ASpecGUI, TTagNode *AFilter, TDate &AFrDate, TDate &AToDate);
  __property TTagNode* DefXML = {read=FDefXML, write=FDefXML};
  __property TAxeXMLContainer *XMLList = {read=FXMLList};
  __property TdsMKB      *MKB = {read=FMKB};

  __property TdsRegGetStrValByCodeEvent OnGetAddrStr = {read=FOnGetAddrStr, write=FOnGetAddrStr};
  __property TdsRegGetStrValByCodeEvent OnGetOrgStr = {read=FOnGetOrgStr, write=FOnGetOrgStr};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsICDM *dsICDM;
//---------------------------------------------------------------------------
#endif
