//---------------------------------------------------------------------------

#ifndef dsICUnitH
#define dsICUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <IPPeerClient.hpp>
#include <Data.DB.hpp>
#include <Datasnap.DSClientRest.hpp>
//---------------------------------------------------------------------------
#include "AppOptBaseProv.h"
#include "AppOptions.h"
#include "AppOptXMLProv.h"
#include "dsKLADRUnit.h"
#include "dsOrgUnit.h"
#include "dsSrvRegUnit.h"
#include "dsSrvPlanUnit.h"
#include "SVRClientClassesUnit.h"
//#include "AppOptBaseProv.h"
//#include "AppOptions.h"
//#include "AppOptXMLProv.h"
#include "DataMF.h"
#include "dsAdminTypes.h"
#include "dsICDMUnit.h"
//---------------------------------------------------------------------------
enum TICModuleType {imtNone, imtReg, imtCard, imtPlan};
//---------------------------------------------------------------------------
class DECLSPEC_DRTTI TdsICClass : public TDataModule
{
__published:	// IDE-managed Components
 TFDConnection *IcConnection;
 TDSRestConnection *SRVConnect;
 TAppOptions *AppOpt;
 TAppOptXMLProv *AppOptXMLData;
 TDSRestConnection *DocConnect;
 void __fastcall AppOptLoadXML(TTagNode *AOptNode);
private:	// User declarations
  TdsSrvRegistry  *FRegComp;
  TdsSrvPlaner    *FPlanComp;
  TTagNode        *FDefXML;
  TdsICDM         *FDM;
  TDataM          *FGDM;
  TdsKLADRClass   *FKLADRComp;
  TdsOrgClass     *FOrgComp;
  UnicodeString   FUploadData;

 // TdsICClassClient  *FAppClient;
  TdsDocClassClient *FDocClient;
  TdsVacStoreClassClient *FVSClient;
  TdsAdminClassClient    *FDocAdmin;
  TdsAdminClassClient    *FVSAdmin;
  bool DocLogged;
  bool VSLogged;
//  TdsKLADRClassClient *FKLADRClient;
//  TdsOrgClassClient   *FOrgClient;
  UnicodeString FGDMRC;
  UnicodeString FGDMRCCode;
  UnicodeString FGDMCCode;
  TdsKeyStatus  FGDMECode;
  UnicodeString FLPUId;
  __int64       FLPUCode;

   TICModuleType FGetModuleType(System::UnicodeString AId);
   void  FOnGetValById(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARetData);
   void  FOnGetValById10(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARetData);
   System::UnicodeString FOnInsertData(System::UnicodeString AId, System::UnicodeString AValue, System::UnicodeString &ARecId);
   System::UnicodeString FOnDeleteData(System::UnicodeString AId, System::UnicodeString ARecId);
   void  FOnGetMIBPList(System::UnicodeString AId, TJSONObject *& ARetData);
  UnicodeString FGetAddrStr(UnicodeString ACode, unsigned int AParams = 0x0FF);
  UnicodeString FGetOrgStr(UnicodeString ACode, unsigned int AParams = 0);
  bool  FCheckSpecExists(System::UnicodeString AId);
  UnicodeString FGetFilterData(System::UnicodeString AGUI, unsigned int AParams = 0);
  UnicodeString __fastcall FOnUnitDataChanged(__int64 APtId);
  void FCheckDocLogin();
  void FCardEdited(__int64 AUCode, int AType, int AInfCode);
  UnicodeString SetTriggers(int AVer, UnicodeString AClsID, UnicodeString AState);
  void          UpdateGenCode(UnicodeString AGenId);

public:		// User declarations
  __fastcall TdsICClass(TComponent* Owner, TdsMKB *AMKB, TAxeXMLContainer *AXMLList, TAppOptions *AOpt);
  __fastcall ~TdsICClass();
  void Disconnect();
  __property TdsKLADRClass   *KLADRComp = { read = FKLADRComp };
  __property TdsOrgClass     *OrgComp   = { read = FOrgComp };
  __property UnicodeString CCode = {read=FGDMCCode};

  UnicodeString GetDefXML();
  UnicodeString GetClassXML(UnicodeString ARef);
  UnicodeString GetClassZIPXML(UnicodeString ARef);
  bool          SetClassZIPXML(UnicodeString ARef);
  __int64       GetCount(UnicodeString AId, UnicodeString AFilterParam, UnicodeString &AExtCount);
  TJSONObject*  GetIdList(UnicodeString AId, int AMax, UnicodeString AFilterParam);
  TJSONObject*  GetValById(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  GetValById10(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  Find(UnicodeString AId, UnicodeString AFindData);
  TJSONObject*  FindEx(UnicodeString AId, UnicodeString AFindData);
  UnicodeString InsertData(UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  UnicodeString EditData(UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  UnicodeString DeleteData(UnicodeString AId, UnicodeString ARecId);

  UnicodeString GetF63(UnicodeString ARef, UnicodeString APrintParams);
  UnicodeString GetVacDefDoze(TDate APrivDate, TDate APatBirthday, System::UnicodeString AVacCode);
  UnicodeString CreateUnitPlan(__int64 APtId);
//  UnicodeString CreatePlan(TJSONObject *AVal, System::UnicodeString &ARecId);
  UnicodeString DeletePlan(System::UnicodeString ARecId);
  UnicodeString DeletePatientPlan(UnicodeString AId, UnicodeString ARecId);
  UnicodeString StartCreatePlan(TJSONObject *AVal);
  UnicodeString StopCreatePlan();
  UnicodeString CheckCreatePlanProgress(System::UnicodeString &ARecId);
  UnicodeString GetPatPlanOptions(__int64 APatCode);
  UnicodeString GetPrivVar(__int64 APrivCode);
  UnicodeString GetOtvodInf(__int64 AOtvodCode);
  void          SetPatPlanOptions(__int64 APatCode, System::UnicodeString AData);
  UnicodeString PrintPlan(__int64 ACode);
  TJSONObject*  GetPlanPrintFormats();
  UnicodeString GetAppOptData();
  void          SetAppOptData(UnicodeString ARef);
  void          SetMainLPU(__int64 ALPUCode);
  TJSONObject*  FindMKB(UnicodeString AStr);
  UnicodeString MKBCodeToText(UnicodeString ACode);
  System::UnicodeString GetPlanTemplateDef(int AType);
  System::UnicodeString GetPlanOpt();
  void SetPlanOpt(UnicodeString AOpt);
  bool          UploadData(UnicodeString ADataPart, int AType);
  System::UnicodeString GetCliOptData();
  void                  SetCliOptData(UnicodeString ARef);
  void          SetUpdateSearch(bool AVal);
  bool          PatGrChData(__int64 APtId, UnicodeString AData);
  TJSONObject * FindAddr(UnicodeString AStr, UnicodeString ADefAddr, int AParams);
  UnicodeString AddrCodeToText(UnicodeString ACode, int AParams);
  __int64       LPUCode();
  TJSONObject* FindOrg(UnicodeString AStr);
  UnicodeString OrgCodeToText(UnicodeString ACode, UnicodeString ASetting = "11111111");
  UnicodeString GetPlanVacVar(UnicodeString AUCode, TDate ADate);
  void SetPatDefSch(__int64 APatCode, System::UnicodeString AData);
  UnicodeString GetProductVersion();
  UnicodeString GetMinDataVersion();
  void  UpdateGenCode();

  UnicodeString DisableTriggers(int AVer, UnicodeString AClsID);
  UnicodeString AddDeleteScript(int AVer, UnicodeString AClsID);
  UnicodeString EnableTriggers(int AVer, UnicodeString AClsID);

};
//---------------------------------------------------------------------------
//extern PACKAGE TdsICClass *dsICClass;
//---------------------------------------------------------------------------
#endif
