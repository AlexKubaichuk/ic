object dsEIDataClass: TdsEIDataClass
  OldCreateOrder = False
  Height = 262
  Width = 495
  object IcConnection: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'CharacterSet=UTF8'
      'Password=masterkey'
      'User_Name=sysdba'
      'Server=localhost'
      'Port=3050'
      'Pooled=False')
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    FormatOptions.AssignedValues = [fvFmtDisplayDateTime, fvFmtDisplayDate, fvFmtDisplayTime]
    FormatOptions.FmtDisplayDateTime = 'dd.mm.yyyy hh:nn:ss'
    FormatOptions.FmtDisplayDate = 'dd.mm.yyyy'
    FormatOptions.FmtDisplayTime = 'hh:nn:ss'
    ResourceOptions.AssignedValues = [rvDirectExecute, rvAutoConnect, rvSilentMode]
    ResourceOptions.DirectExecute = True
    ResourceOptions.SilentMode = True
    ResourceOptions.AutoConnect = False
    TxOptions.AutoStart = False
    TxOptions.AutoStop = False
    TxOptions.DisconnectAction = xdRollback
    LoginPrompt = False
    Left = 29
    Top = 15
  end
  object ICConnect: TDSRestConnection
    Host = 'localhost'
    Port = 5100
    LoginPrompt = False
    Left = 151
    Top = 17
    UniqueId = '{2561E080-5D09-4396-9D34-969204DAB404}'
  end
end
