﻿// ---------------------------------------------------------------------------
// #include <vcl.h>
#pragma hdrstop
#include <Datasnap.DSHTTP.hpp>
#include "dsEIDataUnit.h"
#include "JSONUtils.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "System.Classes.TPersistent"
#pragma resource "*.dfm"
// TRegClassDM *RegClassDM;
// ---------------------------------------------------------------------------
__fastcall TdsEIDataClass::TdsEIDataClass(TComponent * Owner, TAxeXMLContainer * AXMLList, TAppOptions *AOpt)
  : TDataModule(Owner)
 {
  dsLogC(__FUNC__);
  try
   {
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    FLPUID = "";
    Logged = false;
    if (FSes)
     {
      FLPUID = FSes->UserRoles->Values["ID"];
      ConnectDB(IcConnection, AOpt, FSes->UserRoles->Values["ICDB"], __FUNC__);
      FDM      = new TdsEIDataDM(this, IcConnection, AXMLList);
      FRegComp = new TdsSrvRegistry(this, NULL, IcConnection, FDM->XMLList, AOpt, "40381E23-92155860-4448", "");
      FDM->DefXML = FDM->XMLList->GetXML("40381E23-92155860-4448");
      FDefXML = FDM->DefXML;
      FRegComp->SetRegDef(FDefXML);
      FKLADRComp             = new TdsKLADRClass(this, IcConnection, AOpt);
      FOrgComp               = new TdsOrgClass(NULL, FDM->XMLList->GetXML("5BCAE819-D0CFEF0F-0B87"), AOpt);
      FRegComp->OnGetAddrStr = FGetAddrStr;
      FRegComp->OnGetOrgStr  = FGetOrgStr;
      FRegComp->KLADR        = FKLADRComp;
      ICConnect->UserName    = FSes->UserName;
      ICConnect->Password    = FSes->UserRoles->Values["UPS"];
      ICConnect->Port        = AOpt->Vals[_PORTNAME_].AsIntDef(_SERVER_PORT_);
      FICClient              = new TdsICClassClient(ICConnect);
      FAdminClient           = new TdsAdminClassClient(ICConnect);
     }
   }
  catch (Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogC(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
__fastcall TdsEIDataClass::~TdsEIDataClass()
 {
  dsLogD(__FUNC__);
  DisconnectDB(IcConnection, __FUNC__);
  delete FRegComp;
  delete FDM;
  delete FKLADRComp;
  delete FOrgComp;
  delete FICClient;
  delete FAdminClient;
  dsLogD(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
bool TdsEIDataClass::UploadData(UnicodeString ADataPart, int AType)
 {
  bool RC = false;
  try
   {
    try
     {
      if (!AType)
       FUploadData = "";
      else if (AType)
       FUploadData += ADataPart;
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString TdsEIDataClass::FGetAddrStr(UnicodeString ACode, unsigned int AParams)
 {
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FKLADRComp->AddrCodeToText(ACode, AParams /* "11111111" */);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString TdsEIDataClass::FGetOrgStr(UnicodeString ACode, unsigned int AParams)
 {
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FOrgComp->OrgCodeToText(ACode);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString TdsEIDataClass::GetMainLPUParam()
 {
  UnicodeString RC = "";
  TTagNode * FParam = new TTagNode;
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    FDM->quExec(FQ, false, "SELECT * FROM class_00c7 where Upper(r011E)='" + FLPUID.UpperCase() + "'");
    FParam->Name           = "passport";
    FParam->AV["code"]     = FQ->FieldByName("r011E")->AsString;
    FParam->AV["name"]     = FQ->FieldByName("r00CA")->AsString;
    FParam->AV["fullname"] = FQ->FieldByName("r00CB")->AsString;
    FParam->AV["addr"]     = FQ->FieldByName("r00CC")->AsString;
    FParam->AV["phone"]    = FQ->FieldByName("r00CE")->AsString;
    FParam->AV["mail"]     = FQ->FieldByName("r00CF")->AsString;
    FParam->AV["owner"]    = FQ->FieldByName("r00CD")->AsString;
    RC                     = FParam->AsXML;
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
    delete FParam;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString TdsEIDataClass::GetExportDataHeader()
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  TTagNode * def = new TTagNode;
  try
   {
    try
     {
      def->Encoding = "utf-8";
      def->Name     = "imm-ei";
      TTagNode * Pass = def->AddChild("pasport");
      def->AddChild("content");
      Pass->AsXML     = GetMainLPUParam();
      Pass->AV["gui"] = icsNewGUID().UpperCase();
      Pass->AV["ver"] = "700";
      RC              = def->AsXML;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete def;
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString TdsEIDataClass::GetPatDataById(UnicodeString ARecId)
 {
  dsLogMessage("RecId: " + ARecId, __FUNC__);
  UnicodeString RC;
  TJSONObject * JRC = new TJSONObject;
  TJSONObject * ClsRC = new TJSONObject;
  try
   {
    try
     {
      JRC->AddPair("pt", FRegComp->GetImportValById("1000", ARecId, ClsRC));
      JRC->AddPair("cd", FGetPatCardDataById(ARecId, ClsRC));
      JRC->AddPair("cl", ClsRC);
      RC = JRC->ToString();
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete JRC;
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString TdsEIDataClass::GetPatCardDataById(UnicodeString ARecId, TJSONObject * AClsRC)
 {
  dsLogMessage("RecId: " + ARecId, __FUNC__);
  UnicodeString RC;
  try
   {
    try
     {
      RC = FGetPatCardDataById(ARecId, AClsRC)->ToString();
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
bool TdsEIDataClass::FAddCardData(UnicodeString ACardId, UnicodeString ACardPtId, UnicodeString ARecId,
  TJSONObject * ARC, TJSONObject * AClsRC)
 {
  bool RC = false;
  TJSONObject * FCardData = new TJSONObject;
  try
   {
    TJSONObject * tmpFlList;
    TJSONArray * FIdList = (TJSONArray *)FRegComp->GetIdList(ACardId, 0, "{\"" + ACardPtId + "\":\"" + ARecId + "\"}");
    UnicodeString FCardRecId;
    for (int i = 0; i < FIdList->Count; i++)
     {
      tmpFlList  = NULL;
      FCardRecId = ((TJSONString *)FIdList->Items[i])->Value();
      if (!FCardRecId.Length())
       {
        tmpFlList  = ((TJSONObject *)FIdList->Items[i]);
        FCardRecId = JSONToString(tmpFlList, "code");
       }
      FCardData->AddPair(FCardRecId, FRegComp->GetImportValById(ACardId, FCardRecId, AClsRC));
     }
    ARC->AddPair(ACardId, FCardData);
    RC = true;
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * TdsEIDataClass::FGetPatCardDataById(UnicodeString ARecId, TJSONObject * AClsRC)
 {
  dsLogMessage("RecId: " + ARecId, __FUNC__);
  TJSONObject * JRC = new TJSONObject;
  try
   {
    try
     {
      FAddCardData("3003", "317A", ARecId, JRC, AClsRC); // Прививки
      // FAddCardData("1003", "017A", ARecId, JRC, AClsRC); //Прививки (по инфекциям)
      FAddCardData("102F", "017B", ARecId, JRC, AClsRC); // Пробы
      FAddCardData("1100", "1101", ARecId, JRC, AClsRC); // Проверки
      FAddCardData("3030", "317C", ARecId, JRC, AClsRC); // Отводы
      FAddCardData("1030", "017C", ARecId, JRC, AClsRC); // Отводы (по инфекциям)
      FAddCardData("1112", "1113", ARecId, JRC, AClsRC); // Предварительный план
      // JRC->AddPair("3188", FRegComp->GetValById("3188", ARecId)); //Список планов
      // JRC->AddPair("1084", FRegComp->GetValById("1084", ARecId)); //План по инфекциям
      // JRC->AddPair("3084", FRegComp->GetValById("3084", ARecId)); //План
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return JRC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString TdsEIDataClass::ImportData(UnicodeString AId, TJSONObject * AValue, UnicodeString & ARecId,
  UnicodeString & AResMessage)
 {
  dsLogMessage("ImportData: ID: " + AId + ", Value: " + AValue->ToString());
  AResMessage = "";
  UnicodeString RC = "";
  if (AValue)
   {
    try
     {
      try
       {
        RC = FRegComp->ImportData(GetRPartE(AId).Trim().UpperCase(), AValue, ARecId, AResMessage);
        if (SameText(RC, "ok"))
         {
          TJSONObject * FPtData = (TJSONObject *)GetJSONValue(AValue, "pt");
          if (FPtData)
           {
            if (!Logged)
             FAdminClient->LoginSysUser(FLPUID);
            FICClient->CreateUnitPlan(ARecId.ToIntDef(0));
            Logged = true;
           }
         }
       }
      catch (System::Sysutils::Exception & E)
       {
        dsLogError(E.Message, __FUNC__);
       }
     }
    __finally
     {
     }
   }
  return RC;
 }
// ----------------------------------------------------------------------------
