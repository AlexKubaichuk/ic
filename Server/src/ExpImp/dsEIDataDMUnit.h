//---------------------------------------------------------------------------

#ifndef dsEIDataDMUnitH
#define dsEIDataDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Vcl.ExtCtrls.hpp>
//#include <FireDAC.Phys.FB.hpp>
//#include <FireDAC.Comp.Client.hpp>
//#include <FireDAC.Phys.hpp>
//#include <FireDAC.Phys.Intf.hpp>
//#include <FireDAC.Stan.Def.hpp>
//#include <FireDAC.Stan.Error.hpp>
//#include <FireDAC.Stan.Intf.hpp>
//#include <FireDAC.Stan.Option.hpp>
//#include <FireDAC.UI.Intf.hpp>
//#include <Data.DB.hpp>
//#include <FireDAC.Comp.DataSet.hpp>
//#include <FireDAC.DApt.hpp>
//#include <FireDAC.DApt.Intf.hpp>
//#include <FireDAC.DatS.hpp>
//#include <FireDAC.Stan.Async.hpp>
//#include <FireDAC.Stan.Param.hpp>
//#include <FireDAC.Stan.Pool.hpp>
//#include <FireDAC.Phys.FBDef.hpp>
//#include <FireDAC.Comp.UI.hpp>
//#include <FireDAC.VCLUI.Wait.hpp>
//#include <Datasnap.DSClientRest.hpp>
//---------------------------------------------------------------------------
#include <System.JSON.hpp>
//#include "ICUtils.h"
//#include "IcsXMLDoc.h"
#include "dsKLADRUnit.h"
#include "XMLContainer.h"
//#include "dsSrvClassifUnit.h"
#include "OrgParsers.h"
#include "icsLog.h"
//#include <IPPeerClient.hpp>
//---------------------------------------------------------------------------
class TdsEIDataDM : public TDataModule
{
__published:	// IDE-managed Components
private:	// User declarations
  TFDConnection *FConnection;
//  TdsKLADRClass *FKLADR;
  TTagNode *FDefXML;
  TList *FClList;
  TList *FFlList;
//  TdsSrvClassifDataMap  FClassifData;
  __int64 __fastcall GetNewCode(UnicodeString TabId);
  UnicodeString __fastcall NewGUID();
  TAxeXMLContainer *FXMLList;

  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AParam1Name,      Variant AParam1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AParam2Name = "", Variant AParam2Val = 0,
                               UnicodeString AParam3Name = "", Variant AParam3Val = 0,
                               UnicodeString AParam4Name = "", Variant AParam4Val = 0);


public:		// User declarations
  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");

  __fastcall TdsEIDataDM(TComponent* Owner, TFDConnection *AConnection, TAxeXMLContainer *AXMLList);
  __fastcall ~TdsEIDataDM();

  __property TTagNode* DefXML = {read=FDefXML, write=FDefXML};
  __property TAxeXMLContainer *XMLList = {read=FXMLList};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsEIDataDM *dsEIDataDM;
//---------------------------------------------------------------------------
#endif
