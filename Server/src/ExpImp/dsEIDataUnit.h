//---------------------------------------------------------------------------

#ifndef dsEIDataUnitH
#define dsEIDataUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <DataSnap.DSServer.hpp>
//#include <Datasnap.DSProviderDataModuleAdapter.hpp>
//---------------------------------------------------------------------------
#include <FireDAC.Phys.FB.hpp>
#include <Data.DB.hpp>


//#include <IPPeerClient.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <IPPeerClient.hpp>
#include "dsKLADRUnit.h"
#include "dsOrgUnit.h"
#include "dsEIDataDMUnit.h"
#include "dsSrvRegUnit.h"
#include "SVRClientClassesUnit.h"

//---------------------------------------------------------------------------
class DECLSPEC_DRTTI TdsEIDataClass : public TDataModule
{
__published:	// IDE-managed Components
 TFDConnection *IcConnection;
 TDSRestConnection *ICConnect;
private:	// User declarations
  TdsSrvRegistry  *FRegComp;
  TTagNode        *FDefXML;
  TdsEIDataDM     *FDM;
  TdsKLADRClass   *FKLADRComp;
  TdsOrgClass     *FOrgComp;
  TdsICClassClient *FICClient;
  TdsAdminClassClient *FAdminClient;
  UnicodeString   FUploadData;
  UnicodeString   FLPUID;
  bool Logged;


  System::UnicodeString GetMainLPUParam();
  UnicodeString FGetAddrStr(UnicodeString ACode, unsigned int AParams = 0x0FF);
  UnicodeString FGetOrgStr(UnicodeString ACode, unsigned int AParams = 0);
  TJSONObject*  FGetPatCardDataById(UnicodeString ARecId, TJSONObject* AClsRC);
  bool          FAddCardData(UnicodeString ACardId, UnicodeString ACardPtId, UnicodeString ARecId, TJSONObject* ARC, TJSONObject* AClsRC);

public:		// User declarations
  __fastcall TdsEIDataClass(TComponent* Owner, TAxeXMLContainer *AXMLList, TAppOptions *AOpt);
  __fastcall ~TdsEIDataClass();
  UnicodeString ImportData(UnicodeString AId, TJSONObject *AValue, UnicodeString &ARecId, UnicodeString & AResMessage);

  bool          UploadData(UnicodeString ADataPart, int AType);


  System::UnicodeString GetExportDataHeader();
  UnicodeString GetPatDataById(UnicodeString ARecId);
  UnicodeString GetPatCardDataById(UnicodeString ARecId, TJSONObject* AClsRC);
};
//---------------------------------------------------------------------------
//extern PACKAGE TdsICClass *dsICClass;
//---------------------------------------------------------------------------
#endif
