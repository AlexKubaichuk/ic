﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsSrvPlanUnit.h"
#include "CheckSch.h"
#include "Winapi.TlHelp32.hpp"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
HANDLE GetProcessHandle(wchar_t * szExeName)
 {
  HANDLE RC = NULL;
  try
   {
    Winapi::Tlhelp32::PROCESSENTRY32 Pc =
     {
      0
     };
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);
    if (hSnapshot != INVALID_HANDLE_VALUE)
     {
      Pc.dwSize = sizeof(Winapi::Tlhelp32::PROCESSENTRY32);
      if (Process32First(hSnapshot, &Pc))
       {
        do
         {
          if (!_wcscmpi(Pc.szExeFile, szExeName))
           RC = OpenProcess(PROCESS_TERMINATE, false, Pc.th32ProcessID);
         }
        while (Process32Next(hSnapshot, &Pc) && !RC);
       }
      if (hSnapshot != INVALID_HANDLE_VALUE)
       CloseHandle(hSnapshot);
      if (RC == INVALID_HANDLE_VALUE)
       {
        CloseHandle(RC);
        RC = NULL;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__fastcall TdsSrvPlaner::TdsSrvPlaner(TComponent * Owner, TFDConnection * AConnection, TAxeXMLContainer * AXMLList,
  const UnicodeString ARegGUI, const UnicodeString AImpGUI) : TComponent(Owner)
 {
  dsLogC(__FUNC__);
  FClsData = new TdsCommClassData(AConnection);
  FDM      = new TdsSrvPlanDM(this, AConnection, AXMLList, FClsData);
  FClsData->LoadPlanData(FDM->PlanOpt);
  // FPlaner = new TdsPlaner(FDM);
  // 00000000000000000000000000000000000000000000000000000
  // FPPDM              = new TPrePlanerDM(NULL, AConnection, AXMLList, FClsData);
  /*
   FSQLCreator            = ASQLCreator;


   FImmNom = AImmNom;
   //  FImmNom = new TICSIMMNomDef(FImmNomQuery, FXMLList, "40381E23-92155860-4448", FClsData);
   */
  FImmNomQuery           = FDM->CreateTempQuery();
  FImmNom                = new TICSIMMNomDef(FImmNomQuery, AXMLList, "40381E23-92155860-4448", FClsData);
  FSQLCreator            = new TdsDocSQLCreator(AXMLList);
  FSQLCreator->OnGetSVal = FDocGetSVal;
  FPrePlaner = new TPrePlaner( /* FPPDM */ AConnection, FDM->PlanOpt, AXMLList, FClsData, FImmNom, FSQLCreator);
  FPlanLinker                     = new TPlanLinker(AConnection, FDM->PlanOpt, AXMLList, FClsData);
  FPrePlaner->VacSchXML           = FDM->VacSchXML;
  FPrePlaner->TestSchXML          = FDM->TestSchXML;
  FPrePlaner->CheckSchXML         = FDM->CheckSchXML;
  FPrePlaner->OnPlanerStageChange = PrePlanerStageChange;
  FPrePlaner->OnPlanDeleteData    = OnPlanDeleteData;
  FPrePlaner->OnPlanInsertData    = OnPlanInsertData;
  FPrePlaner->OnLogMessage        = FLogMessage;
  FPrePlaner->OnLogError          = FLogError;
  FPrePlaner->OnLogBegin          = FLogBegin;
  FPrePlaner->OnLogEnd            = FLogEnd;
  FPrePlaner->OnSaveDebugLog      = FSaveDebugLog;
  FPlanningStatus                 = psNone;
  // //DebugPlanLog = NULL;
  // //PlanningErrorLog = NULL;
  FPlanLinker->OnPlanerStageChange = PlanLinkerStageChange;
  FPlanLinker->OnLogMessage        = FLogMessage;
  FPlanLinker->OnLogError          = FLogError;
  FPlanLinker->OnLogBegin          = FLogBegin;
  FPlanLinker->OnLogEnd            = FLogEnd;
  // 00000000000000000000000000000000000000000000000000000
  dsLogC(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FDocGetSVal(UnicodeString AFuncName, TTagNode * ANode, UnicodeString & ATab,
  UnicodeString & ARet)
 {
  dsLogBegin(__FUNC__);
  FImmNom->GetSVal(AFuncName, ANode, ATab, ARet);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
__fastcall TdsSrvPlaner::~TdsSrvPlaner()
 {
  dsLogD(__FUNC__);
  // 00000000000000000000000000000000000000000000000000000
  FPlanLinker->OnLogMessage  = NULL;
  FPlanLinker->OnLogError    = NULL;
  FPlanLinker->OnLogBegin    = NULL;
  FPlanLinker->OnLogEnd      = NULL;
  FPrePlaner->OnLogMessage   = NULL;
  FPrePlaner->OnLogError     = NULL;
  FPrePlaner->OnLogBegin     = NULL;
  FPrePlaner->OnLogEnd       = NULL;
  FPrePlaner->OnSaveDebugLog = NULL;
  delete FPlanLinker;
  delete FPrePlaner;
  // 00000000000000000000000000000000000000000000000000000
  // delete FPlaner;
  // delete FPPDM;
  delete FSQLCreator;
  delete FImmNom;
  FDM->DeleteTempQuery(FImmNomQuery);
  delete FDM;
  delete FClsData;
  dsLogD(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::Disconnect()
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::SetRegDef(TTagNode * AVal)
 {
  FDM->RegDef = AVal;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TdsSrvPlaner::GetCount(System::UnicodeString AId, System::UnicodeString AFilterParam)
 {
  dsLogBegin(__FUNC__);
  __int64 RC = 0;
  try
   {
    RC = FDM->GetCount(AId, AFilterParam);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvPlaner::GetIdList(System::UnicodeString AId, int AMax,
  System::UnicodeString AFilterParam)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC;
  try
   {
    RC = FDM->GetIdList(AId, AMax, AFilterParam);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvPlaner::GetValById(System::UnicodeString AId, System::UnicodeString ARecId)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC;
  try
   {
    RC = FDM->GetValById(AId, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvPlaner::GetValById10(System::UnicodeString AId, System::UnicodeString ARecId)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC;
  try
   {
    RC = FDM->GetValById10(AId, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvPlaner::Find(System::UnicodeString AId, TJSONObject * AFindParam)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC;
  try
   {
    RC = FDM->Find(AId, AFindParam);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::InsertData(System::UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId)
 {
  UnicodeString RC = "";
  try
   {
    // RC = FDataImporter->InsertOrUpdateData(AId, AValue, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::EditData(System::UnicodeString AId, TJSONObject * AValue,
  System::UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    // RC = FDataImporter->InsertOrUpdateData(AId, AValue, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::DeleteData(System::UnicodeString AId, System::UnicodeString ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    RC = FDM->DeleteData(AId, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::GetTextData(System::UnicodeString AId, System::UnicodeString AFlId,
  System::UnicodeString ARecId, UnicodeString & AMsg)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    // if (FDataConv)
    // RC = FDataConv(cdtGet, AId, AFlId, FDM->GetTextData(AId, AFlId, ARecId, AMsg));
    // else
    // RC = FDM->GetTextData(AId, AFlId, ARecId, AMsg);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::SetTextData(System::UnicodeString AId, System::UnicodeString AFlId,
  System::UnicodeString ARecId, System::UnicodeString AVal)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    // if (FDataConv)
    // RC = FDM->SetTextData(AId, AFlId, ARecId, FDataConv(cdtSet, AId, AFlId, AVal));
    // else
    // RC = FDM->SetTextData(AId, AFlId, ARecId, AVal);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #       План для пациента                                                    #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::CreateUnitPlan(__int64 APtId)
 {
  // __int64 memsize = PrintProcessMemoryInfo().ToIntDef(0);
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  // 0000000000000000000000000000000000000000000000000000000000000000000000000
  TTime FBeg = Time();
  dsPlanLogMessage("Старт планирования для " + IntToStr(APtId), __FUNC__);
  CheckAndUpdate(APtId);
  dsPlanLogMessage("CheckAndUpdate для " + IntToStr(APtId) + ": " + (Time() - FBeg).FormatString("nn.ss.zzz"),
    __FUNC__);
  dsPlanLogMessage("Планирование для " + IntToStr(APtId), __FUNC__);
  TPrePlaner * FUPrePlaner = new TPrePlaner(FDM->Connection, FDM->PlanOpt, FDM->XMLList, FClsData, FImmNom,
    FSQLCreator);
  try
   {
    FUPrePlaner->VacSchXML           = FDM->VacSchXML;
    FUPrePlaner->TestSchXML          = FDM->TestSchXML;
    FUPrePlaner->CheckSchXML         = FDM->CheckSchXML;
    FUPrePlaner->OnPlanerStageChange = PrePlanerStageChange;
    FUPrePlaner->OnPlanDeleteData    = OnPlanDeleteData;
    FUPrePlaner->OnPlanInsertData    = OnPlanInsertData;
    FUPrePlaner->OnLogMessage        = FLogMessage;
    FUPrePlaner->OnLogError          = FLogError;
    FUPrePlaner->OnLogBegin          = FLogBegin;
    FUPrePlaner->OnLogEnd            = FLogEnd;
    FUPrePlaner->OnSaveDebugLog      = FSaveDebugLog;
    FPlanningStatus                  = psNone;
    TDate FBirthDay = 0;
    UnicodeString FFIO = FDM->GetUnitData(APtId, FBirthDay);
    if (FCreateUnitPlan(APtId, FUPrePlaner, Date(), FBirthDay, FFIO))
     RC = "ok";
   }
  __finally
   {
    FUPrePlaner->OnLogMessage   = NULL;
    FUPrePlaner->OnLogError     = NULL;
    FUPrePlaner->OnLogBegin     = NULL;
    FUPrePlaner->OnLogEnd       = NULL;
    FUPrePlaner->OnSaveDebugLog = NULL;
    delete FUPrePlaner;
    dsPlanLogMessage("Длительность планирования для " + IntToStr(APtId) + ": " + (Time() - FBeg).FormatString
      ("nn.ss.zzz"), __FUNC__);
   }
  // 0000000000000000000000000000000000000000000000000000000000000000000000000
  dsLogEnd(__FUNC__);
  // dsPlanLogMessage("TdsSrvPlaner::CreateUnitPlan for " + IntToStr(APtId) + " Size: " +    IntToStr(PrintProcessMemoryInfo().ToIntDef(0) - memsize), __FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
bool __fastcall TdsSrvPlaner::FCreateUnitPlan(__int64 APtId, TPrePlaner * APrePlaner, TDate ABegDate, TDate ABirthDay,
  UnicodeString APtFIO)
 { // Планирование для пациента
  bool RC = false;
  try
   {
    dsPlanLogMessage("Планирование", __FUNC__, 5);
    FPlanningStatus = psInProgress;
    try
     {
      RC = APrePlaner->CreateUnitPlan(ABegDate, APtId, ABirthDay, APtFIO);
     }
    catch (Exception & E)
     {
      dsPlanLogError(E.Message, __FUNC__);
      if (E.Message.LowerCase().Pos("out of memory"))
       {
        HANDLE kill = GetProcessHandle(L"ic_server.exe");
        DWORD fdwExit = 0;
        GetExitCodeProcess(kill, & fdwExit);
        TerminateProcess(kill, fdwExit);
        CloseHandle(kill);
       }
      RC = false;
     }
   }
  __finally
   {
    FPlanningStatus = psNone;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FOnCreateUnitPlan(__int64 APtId, TDate ABegDate, TDate ABirthDay, UnicodeString APtFIO)
 {
  TTime FBeg = Time();
  try
   {
    FCreateUnitPlan(APtId, FPrePlaner, ABegDate, ABirthDay, APtFIO);
   }
  __finally
   {
    dsPlanLogMessage("Длительность планирования для " + IntToStr(APtId) + ": " + (Time() - FBeg).FormatString
      ("nn.ss.zzz"));
   }
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #       План на участок/организацию                                          #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::StartCreatePlan(TJSONObject * AVal)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "ошибка";
  try
   {
    // 0000000000000000000000000000000000000000000000000000000000000000
    dsLogBegin(__FUNC__);
    UnicodeString RC = "ошибка";
    FPrePlaner->OnPlanDeleteData = OnPlanDeleteData;
    FPrePlaner->OnPlanInsertData = OnPlanInsertData;
    try
     {
      RC = FPlanLinker->StartCreatePlan(AVal, FOnCreateUnitPlan);
     }
    __finally
     {
     }
    dsLogEnd(__FUNC__);
    return RC;
    // 0000000000000000000000000000000000000000000000000000000000000000
    // RC = FPlaner->StartCreatePlan(AVal);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::StopCreatePlan()
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "ошибка";
  try
   {
    // 0000000000000000000000000000000000000000000000000000000000000000
    dsLogBegin(__FUNC__);
    UnicodeString RC = "ошибка";
    try
     {
      RC = FPlanLinker->StopCreatePlan();
     }
    __finally
     {
      PlanCreateEnd();
     }
    dsLogEnd(__FUNC__);
    return RC;
    // 0000000000000000000000000000000000000000000000000000000000000000
    // RC = FPlaner->StopCreatePlan();
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::CheckCreatePlanProgress(System::UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "ошибка";
  ARecId = "-1";
  try
   {
    // 0000000000000000000000000000000000000000000000000000000000000000
    if (FPlanLinker->Planing)
     {
      RC = FPlanLinker->GetCurStage() + " [" + IntToStr(FPlanLinker->GetCurProgress()) + "/" +
        IntToStr(FPlanLinker->GetMaxProgress()) + "]";
     }
    else
     {
      PlanCreateEnd();
      ARecId = FPlanLinker->PCRecId;
      RC     = "ok";
     }
    // 0000000000000000000000000000000000000000000000000000000000000000
    // RC = FPlaner->GetPlanProgress(ARecId);
    if (RC == "error")
     {
     }
    else if (RC == "ok")
     {
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::DeletePlan(System::UnicodeString ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "ошибка";
  try
   {
    RC = FDM->DeletePlan(ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsSrvPlaner::DeletePatientPlan(UnicodeString AId, UnicodeString ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "Ошибка";
  try
   {
    RC = FDM->DeletePatientPlan(AId, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
// 0000000000000000000000000000000000000000000000000000000000000000
void __fastcall TdsSrvPlaner::PlanCreateEnd()
 {
  FPrePlaner->OnPlanDeleteData = NULL;
  FPrePlaner->OnPlanInsertData = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FLogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsPlanLogMessage(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsPlanLogError(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FLogBegin(UnicodeString AFuncName)
 {
  dsLogBegin(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FLogEnd(UnicodeString AFuncName)
 {
  dsLogEnd(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FSaveDebugLog(UnicodeString AXML)
 {
  dsLogBegin(__FUNC__);
  if (AXML.Length())
   {
    dsPlanLogMessage("==============================================================================");
    dsPlanLogMessage(AXML);
    dsPlanLogMessage("==============================================================================");
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::PlanLinkerStageChange(TObject * Sender, TPlanLinkerStage NewPlanerStage)
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::PrePlanerStageChange(TObject * Sender, TPlanerStage NewPlanerStage)
 {
  if (FPlanningStatus == psInProgress)
   {
    switch (NewPlanerStage)
     {
     case psInitialization:
      FLogMessage("\"psInitialization\"", __FUNC__, 5);
      break;
     case psUpdatePlans:
      FLogMessage("\"psUpdatePlans\"", __FUNC__, 5);
      break;
     case psPlanDop:
      FLogMessage("\"psPlanDop\"", __FUNC__, 5);
      break;
     case psPlanPriviv:
      FLogMessage("\"psPlanPriviv\"", __FUNC__, 5);
      break;
     case psPlanProb:
      FLogMessage("\"psPlanProb\"", __FUNC__, 5);
      break;
     case psAppendDopPrivivByPrevProb:
      FLogMessage("\"psAppendDopPrivivByPrevProb\"", __FUNC__, 5);
      break;
     case psCorrectPolyVak:
      FLogMessage("\"psCorrectPolyVak\"", __FUNC__, 5);
      break;
     case psCorrectPrivivPlanDate:
      FLogMessage("\"psCorrectPrivivPlanDate\"", __FUNC__, 5);
      break;
     case psAppendPrepareProb:
      FLogMessage("\"psAppendPrepareProb\"", __FUNC__, 5);
      break;
     case psApplyPlan:
      FLogMessage("\"psApplyPlan\"", __FUNC__, 5);
      break;
     }
    Application->ProcessMessages();
   }
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::GetPatPlanOptions(__int64 APatCode)
 {
  dsLogBegin(UnicodeString(__FUNC__) + "PtId :" + IntToStr(APatCode));
  CheckAndUpdate(APatCode); // !!!
  UnicodeString RC = FDM->GetPatPlanOptions(APatCode);
  dsLogMessage(RC);
  dsLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::SetPatPlanOptions(__int64 APatCode, System::UnicodeString AData)
 {
  dsLogBegin(__FUNC__);
  TTagNode * tmpSch = new TTagNode;
  try
   {
    tmpSch->AsXML = AData;
    TTagNode * FPatPlan = tmpSch->GetChildByName("comm", true);
    if (FPatPlan)
     delete FPatPlan;
    FDM->SetUnitSch(APatCode, tmpSch->AsXML);
   }
  __finally
   {
    delete tmpSch;
   }
  dsLogEnd(__FUNC__);
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::SetPatDefSch(__int64 APatCode, System::UnicodeString AData)
 {
  dsLogBegin(__FUNC__);
  TTagNode * FSetSch = new TTagNode;
  TTagNode * FPatSch = new TTagNode;
  TTagNode * FRCSch;
  try
   {
    FSetSch->AsXML = AData;
    FPatSch->AsXML = GetPatPlanOptions(APatCode);
    /*
     <ppls>
     <passport gui='16312F31-0000D7C7-7A07'/>
     <content>
     <sch>
     <spr infref='1' vac='0007.27CA' test='0001.0002' tvac='_NULL_' ttest='_NULL_'/>
     <spr infref='2' vac='0033.0036' test='_NULL_' tvac='_NULL_' ttest='_NULL_'/>
     <spr infref='34' vac='0073.27DC' test='_NULL_' tvac='_NULL_' ttest='_NULL_'/>
     <spr infref='35' vac='_NULL_' test='_NULL_' tvac='_NULL_' ttest='_NULL_'/>
     </sch>
     <vacplan>
     */
    TTagNode * itSetSch = FSetSch->GetFirstChild();
    while (itSetSch)
     {
      FRCSch = FPatSch->GetChildByAV("spr", "infref", itSetSch->AV["infref"], true);
      if (FRCSch)
       {
        if (itSetSch->AV["v"].Length())
         {
          FRCSch->AV["vac"]   = itSetSch->AV["v"];
          FRCSch->AV["chvac"] = "1";
         }
        if (itSetSch->AV["t"].Length())
         {
          FRCSch->AV["test"]   = itSetSch->AV["t"];
          FRCSch->AV["chtest"] = "1";
         }
       }
      itSetSch = itSetSch->GetNext();
     }
    FDM->SetUnitSch(APatCode, FPatSch->AsXML);
   }
  __finally
   {
    delete FSetSch;
    delete FPatSch;
   }
  dsLogEnd(__FUNC__);
 }
// ----------------------------------------------------------------------------
TdsOnPlanGetValByIdEvent __fastcall TdsSrvPlaner::FGetOnPlanGetValById()
 {
  TdsOnPlanGetValByIdEvent RC = NULL;
  if (FDM)
   RC = FDM->OnGetValById;
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FSetOnPlanGetValById(TdsOnPlanGetValByIdEvent AVal)
 {
  if (FDM)
   FDM->OnGetValById = AVal;
 }
// ----------------------------------------------------------------------------
TdsOnPlanGetValById10Event __fastcall TdsSrvPlaner::FGetOnPlanGetValById10()
 {
  TdsOnPlanGetValById10Event RC = NULL;
  if (FDM)
   RC = FDM->OnGetValById10;
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FSetOnPlanGetValById10(TdsOnPlanGetValById10Event AVal)
 {
  if (FDM)
   FDM->OnGetValById10 = AVal;
 }
// ----------------------------------------------------------------------------
TdsOnPlanDeleteDataEvent __fastcall TdsSrvPlaner::FGetOnPlanDeleteData()
 {
  TdsOnPlanDeleteDataEvent RC = NULL;
  if (FDM)
   RC = FDM->OnPlanDeleteData;
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FSetOnPlanDeleteData(TdsOnPlanDeleteDataEvent AVal)
 {
  if (FDM)
   FDM->OnPlanDeleteData = AVal;
 }
// ----------------------------------------------------------------------------
TdsOnPlanInsertDataEvent __fastcall TdsSrvPlaner::FGetOnPlanInsertData()
 {
  TdsOnPlanInsertDataEvent RC = NULL;
  if (FDM)
   RC = FDM->OnPlanInsertData;
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FSetOnPlanInsertData(TdsOnPlanInsertDataEvent AVal)
 {
  if (FDM)
   FDM->OnPlanInsertData = AVal;
 }
// ----------------------------------------------------------------------------
TdsOnPlanGetMIBPListEvent __fastcall TdsSrvPlaner::FGetOnGetMIBPList()
 {
  TdsOnPlanGetMIBPListEvent RC = NULL;
  if (FPlanLinker)
   RC = FPlanLinker->OnGetMIBPList;
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FSetOnGetMIBPList(TdsOnPlanGetMIBPListEvent AVal)
 {
  if (FPlanLinker)
   FPlanLinker->OnGetMIBPList = AVal;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::CheckAndUpdate(__int64 AUCode)
 {
  dsLogBegin(__FUNC__);
  if (FDM)
   {
    TCheckSch * FUpd = new TCheckSch(FDM);
    try
     {
      FUpd->CheckAndUpdate(AUCode);
     }
    __finally
     {
      delete FUpd;
     }
   }
  dsLogEnd(__FUNC__);
 }
// ----------------------------------------------------------------------------
TdsPlanGetStrValByCodeEvent __fastcall TdsSrvPlaner::FGetOnGetOrgStr()
 {
  TdsPlanGetStrValByCodeEvent RC = NULL;
  if (FPlanLinker)
   RC = FPlanLinker->OnGetOrgStr;
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FSetOnGetOrgStr(TdsPlanGetStrValByCodeEvent AVal)
 {
  if (FPlanLinker)
   FPlanLinker->OnGetOrgStr = AVal;
 }
// ----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvPlaner::GetPlanPrintFormats()
 {
  TJSONObject * RC = new TJSONObject;
  try
   {
    try
     {
      TTagNode * FPPS = FDM->PlanOpt->GetChildByName("plan_print_formats", true);
      if (FPPS)
       {
        FPPS = FPPS->GetFirstChild();
        while (FPPS)
         {
          if (FCheckSpecExists)
           {
            if (FCheckSpecExists(FPPS->AV["ref"]))
             RC->AddPair(FPPS->AV["ref"], FPPS->AV["name"]);
           }
          FPPS = FPPS->GetNext();
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsPlanLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::GetPlanTemplateDef(int AType)
 {
  UnicodeString RC = "error";
  try
   {
    RC = FDM->GetPlanTemplateDef(AType);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::GetPlanOpt()
 {
  UnicodeString RC = "error";
  try
   {
    RC = FDM->GetPlanOpt();
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::SetPlanOpt(UnicodeString AOpts)
 {
  FDM->SetPlanOpt(AOpts);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::ClearHandSch(__int64 AUCode, int AType, int AInfCode)
 {
  TTagNode * USch = new TTagNode;
  try
   {
    USch->AsXML = FDM->GetUnitSch(AUCode);
    TTagNode * InfSch = USch->GetChildByName("sch", true);
    if (InfSch)
     {
      InfSch = InfSch->GetChildByAV("spr", "infref", IntToStr(AInfCode));
      if (InfSch)
       {
        UnicodeString AttrName = "";
        switch (AType)
         {
         case 1:
          AttrName = "chvac";
          break;
         case 2:
          AttrName = "chtest";
          break;
         case 3:
          AttrName = "chcheck";
          break;
         }
        InfSch->AV[AttrName] = "0";
        FDM->SetUnitSch(AUCode, USch->AsXML);
       }
     }
   }
  __finally
   {
    delete USch;
   }
 }
// ---------------------------------------------------------------------------
