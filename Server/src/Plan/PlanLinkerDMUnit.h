//---------------------------------------------------------------------------

#ifndef PlanLinkerDMUnitH
#define PlanLinkerDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Vcl.ExtCtrls.hpp>
//#include <FireDAC.Phys.FB.hpp>
//#include <FireDAC.Comp.Client.hpp>
//#include <FireDAC.Phys.hpp>
//#include <FireDAC.Phys.Intf.hpp>
//#include <FireDAC.Stan.Def.hpp>
//#include <FireDAC.Stan.Error.hpp>
//#include <FireDAC.Stan.Intf.hpp>
//#include <FireDAC.Stan.Option.hpp>
//#include <FireDAC.UI.Intf.hpp>
//#include <Data.DB.hpp>
//#include <FireDAC.Comp.DataSet.hpp>
//#include <FireDAC.DApt.hpp>
//#include <FireDAC.DApt.Intf.hpp>
//#include <FireDAC.DatS.hpp>
//#include <FireDAC.Stan.Async.hpp>
//#include <FireDAC.Stan.Param.hpp>
//#include <FireDAC.Stan.Pool.hpp>
//#include <FireDAC.Phys.FBDef.hpp>
//#include <FireDAC.Comp.UI.hpp>
//#include <FireDAC.VCLUI.Wait.hpp>
////---------------------------------------------------------------------------
#include "dsRegClassData.h"
#include <System.JSON.hpp>
#include "XMLContainer.h"
#include "dsSrvClassifUnit.h"
#include "icsLog.h"
#include <list>
#include "dsDocSQLCreator.h"
#include "IMMNomDef.h"
#include "JSONUtils.h"
#include "dsCommClassData.h"
#include "PlanerCommon.h"
//---------------------------------------------------------------------------
//���� ������ ������������
//---------------------------------------------------------------------------
typedef void          (__closure *TdsOnPlanGetValByIdEvent)(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARetData);
typedef void          (__closure *TdsOnPlanGetValById10Event)(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARetData);
//typedef UnicodeString (__closure *TdsOnPlanInsertDataEvent)(System::UnicodeString AId, System::UnicodeString AValue, System::UnicodeString &ARecId);
typedef void          (__closure *TdsOnPlanGetMIBPListEvent)(System::UnicodeString AId, TJSONObject *& ARetData);
typedef void          __fastcall (__closure *TdsOnPlanCreateUnitPlanEvent)(__int64 APtId, TDate ABegDate, TDate ABirthDay, UnicodeString APtFIO);
typedef UnicodeString  (__closure *TdsPlanGetStrValByCodeEvent)(UnicodeString ACode, unsigned int AParams);
//---------------------------------------------------------------------------
typedef map<int, UnicodeString> TPlanVacTypeList;          //������ ����������� ��������/����
//---------------------------------------------------------------------------
typedef struct tagPrePlanRec
{
  __int64       UCode;        // ��� ��������' uid='1113' ref='1000' depend='1113.1113' isedit='0' fieldtype='integer'/>
  TDate         PatBirthDay;     // ���� �����' uid='111B'/>
  short         Brig;         // ��������� ����������' uid='3193'/>
  TDate         PrePlanDate;  // ���� ���������������� �����' uid='1115' inlist='l'/>
  short         PlanItemType; // ���' uid='1116' default='1'>          <choicevalue name='��������' value='1'/>          <choicevalue name='�����' value='2'/>          <choicevalue name='��������' value='3'/>        </choice>
  UnicodeString Fam,Name,SecName;      // FIO
  int           SupportState;          // choice name='&amp;������ ������������' uid='32B3' required='1' inlist='l' default='0'>
  UnicodeString OrgLev;      // ������ �����������
  UnicodeString VacType;      // ���/��������' uid='1117' inlist='l' cast='no' length='10'/>
  UnicodeString AllVacType;   // inf1.VacType;inf2.VacType; ...
  int           Inf;          // ��������' uid='3187' ref='003A' inlist='l'/>
  __int64       MIBP;         // ����' uid='1118' ref='0035' inlist='l'/>
  __int64       SrcMIBP;         // <choiceDB name='�������� �����' uid='3314' ref='0035' isedit='0'/>
  TDate         DatePlanTest;         // <choiceDB name='�������� �����' uid='3317' ref='0035' isedit='0'/>
  UnicodeString TypePlanTest;         // <choiceDB name='�������� �����' uid='3318' ref='0035' isedit='0'/>

  int           MIBPType;     //  0  - ����� �������, 1 - ����� �����������  2 - ������ ���������
  int           Prior;        // ���������' uid='1119' inlist='l' digits='4' min='-999' max='9999'/>
  TPlanVacTypeList TypeList;
                              // � �����' uid='111A'/>
  TDate         PlanDate;     // ���� �����' uid='111B'/>
  TDate         PlanMonth;
  int           Src;          // ��������' uid='111C' inlist='l' default='0'>          <choicevalue name='����������' value='0'/>          <choicevalue name='������ ����������' value='1'/>          <choicevalue name='����������� �� ���������' value='2'/>          <choicevalue name='����������� �� �� ���������' value='3'/>          <choicevalue name='����������� ��� ���������������' value='4'/>          <choicevalue name='����������� �� ����' value='5'/>          <choicevalue name='����������� �� ���� ��� ���������������' value='6'/>
  bool          InPlan;
  tagPrePlanRec() {};
  UnicodeString InfVacType()
  {
    return IntToStr(Inf)+"."+VacType+";";
  };
  tagPrePlanRec(TFDQuery *AQ)
  {
    UCode        = AQ->FieldByName("R1113")->AsInteger;
    Fam          = AQ->FieldByName("R001f")->AsString;
    Name         = AQ->FieldByName("R0020")->AsString;
    SecName        = AQ->FieldByName("R002f")->AsString;
    SupportState = AQ->FieldByName("R32B3")->AsInteger;
    OrgLev       = AQ->FieldByName("R0026")->AsString+
    "$"+AQ->FieldByName("R0027")->AsString+
    "$"+AQ->FieldByName("R0028")->AsString;
    PatBirthDay  = AQ->FieldByName("R0031")->AsDateTime;
    Brig         = AQ->FieldByName("R3193")->AsInteger;
    PrePlanDate  = AQ->FieldByName("R1115")->AsDateTime;
    PlanMonth    = AQ->FieldByName("R31A2")->AsDateTime;
    PlanItemType = AQ->FieldByName("R1116")->AsInteger;
    VacType      = AQ->FieldByName("R1117")->AsString;
    Inf          = AQ->FieldByName("R3187")->AsInteger;
    MIBP         = AQ->FieldByName("R1118")->AsInteger;
    SrcMIBP      = AQ->FieldByName("R3314")->AsInteger;
    DatePlanTest = AQ->FieldByName("R3317")->AsDateTime;
    TypePlanTest = AQ->FieldByName("R3318")->AsString;

    if (PlanItemType == 1)
     {//R1116 = �������� - 1,  ����� - 2,  �������� - 3
       MIBPType = (int)(MIBP/10000);
       MIBP     -= 10000*MIBPType;
     }
    else
     MIBPType = 0;

    Prior        = AQ->FieldByName("R1119")->AsInteger;
                              // � �����' uid='111A'/>
    PlanDate     = AQ->FieldByName("R111B")->AsDateTime;
    Src          = AQ->FieldByName("R111C")->AsInteger;
    InPlan       = false;
    AllVacType   = InfVacType();
  };
  tagPrePlanRec(tagPrePlanRec *ASrc)
  {
    UCode        = ASrc->UCode;
    Fam          = ASrc->Fam;
    Name         = ASrc->Name;
    SecName        = ASrc->SecName;
    OrgLev       = ASrc->OrgLev;
    SupportState = ASrc->SupportState;
    PatBirthDay  = ASrc->PatBirthDay;
    Brig         = ASrc->Brig;
    PlanMonth    = ASrc->PlanMonth;
    PrePlanDate  = ASrc->PrePlanDate;
    PlanItemType = ASrc->PlanItemType;
    VacType      = ASrc->VacType;
    Inf          = ASrc->Inf;
    DatePlanTest = ASrc->DatePlanTest;
    TypePlanTest = ASrc->TypePlanTest;
    MIBP         = ASrc->MIBP;
    SrcMIBP      = ASrc->SrcMIBP;
    MIBPType     = ASrc->MIBPType;
    Prior        = ASrc->Prior;
//    TypeList = TPlanVacTypeList(ASrc->TypeList);
                              // � �����' uid='111A'/>
    PlanDate     = ASrc->PlanDate;
    Src          = ASrc->Src;
    InPlan       = ASrc->InPlan;
    AllVacType   = ASrc->AllVacType;
  };
  UnicodeString ToString()
  {
    return ", UCode:"+IntToStr(UCode)+
    ", FIO:"+Fam+
    " "+Name+
    " "+SecName+
    " "+OrgLev+
    ", SupState:"+IntToStr(SupportState)+
    ", Brig:"+IntToStr(Brig)+
    ", PrePlanDate:"+PrePlanDate.FormatString("dd.mm.yyyy")+
    ", PlanItemType:"+IntToStr(PlanItemType)+
    ", VacType:"+VacType +
    ", Inf:"+IntToStr(Inf)+
    ", MIBP:"+IntToStr(MIBP)+
    ", Prior:"+IntToStr(Prior)+
    ", PlanDate:"+PlanDate.FormatString("dd.mm.yyyy")+
    ", SrcMIBP:"+IntToStr(SrcMIBP)+
    ", Src:"+IntToStr(Src);
  };
}
TPrePlanRec;
typedef map<UnicodeString, TPrePlanRec*> TUnitPrePlan;
typedef map<int, TPrePlanRec*> TUnitPriorPrePlan;
//---------------------------------------------------------------------------
//typedef map<int,int> TIntMap;
//typedef map<int, TIntMap*> TIntListMap;
//---------------------------------------------------------------------------

class TPlanLinkerDM : public TDataModule
{
__published:	// IDE-managed Components
private:	// User declarations
  TFDConnection *FConnection;

  TAxeXMLContainer *FXMLList;

  TdsDocSQLCreator *FSQLCreator;

  TTagNode *FVacSchXML;
  TTagNode *FTestSchXML;
  TTagNode *FCheckSchXML;
  TTagNode *FMIBPAgeDef;
  TdsPlanGetStrValByCodeEvent  FOnGetOrgStr;


  TdsRegClassData *FClassData;
//  TICSIMMNomDef      *FImmNom;
//  TFDQuery* FImmNomQuery;
  TFDQuery* FClassDataQuery;
  int FCommPlanCount, FCurrPlan;
  TdsSrvClassifDataMap  FClassifData;
//  TdsOnPlanInsertDataEvent  FOnPlanInsertData;
  TdsOnPlanGetMIBPListEvent FOnGetMIBPList;
  TdsOnPlanCreateUnitPlanEvent FOnCreateUnitPlan;
  TUnitPrePlan   *FUnitPrePlan;
  TdsCommClassData *FClsData;

  TIntMap         *FStoreMIBPList;
  TIntMap         *FDefStoreMIBPList;
  TIntMap         *FSortedStoreMIBPList;
  TIntMap         *FDefSortedStoreMIBPList;
  TIntMap         *FStoreTestList;
  TIntMap         *FDefStoreTestList;

//  void          __fastcall FRegGetSVal(UnicodeString AFuncName, TTagNode *ANode, UnicodeString &ATab, UnicodeString &ARet);


//  void __fastcall FDocGetSVal(UnicodeString AFuncName, TTagNode *ANode, UnicodeString &ATab, UnicodeString &ARet);

  void __fastcall ClearHash();
public:		// User declarations
  __fastcall TPlanLinkerDM(TComponent* Owner, TFDConnection *AConnection, TTagNode *APlanOpt, TAxeXMLContainer *AXMLList, TdsCommClassData *AClsData);
  __fastcall ~TPlanLinkerDM();

  UnicodeString __fastcall GetOrgName(TJSONObject *AValue);
//  UnicodeString __fastcall GetOrgFullCode(__int64 ACode);
  UnicodeString __fastcall GetUchName(TJSONObject *AValue);
  UnicodeString __fastcall GetFltName(TJSONObject *AValue);
  __int64 __fastcall GetNewCode(UnicodeString TabId);
  UnicodeString __fastcall NewGUID();

//  Variant __fastcall JSONToVar(TJSONObject *AValue, UnicodeString AId, Variant ADef = Variant::Empty());
//  __int64 __fastcall JSONToInt(TJSONObject *AValue, UnicodeString AId, __int64 ADef = 0);
//  System::UnicodeString __fastcall JSONToString(TJSONObject *AValue, UnicodeString AId, UnicodeString ADef = "");

  UnicodeString __fastcall GetClassData(long ACode, UnicodeString AName);
  void          __fastcall GetMIBPFromStore();
  TJSONObject* __fastcall GetDefMIBP(int AType);
  bool __fastcall MIBPInStore(__int64 AMIBPCode);
  bool __fastcall MIBPInDefStore(__int64 AMIBPCode);
//  bool __fastcall CheckMIBPPlanCompat(__int64 AMIBPCode);
//  bool __fastcall CheckCompatible(__int64 AMIBP1, __int64 AMIBP2);
  TTagNode    *FPlanOpt;
  TTagNode    *FPlanOptDefSch;

  UnicodeString __fastcall GetPlanTemplateDef(int AType);
  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AParam1Name, UnicodeString AParam1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AParam2Name = "", Variant AParam2Val = 0,
                               UnicodeString AParam3Name = "", Variant AParam3Val = 0,
                               UnicodeString AParam4Name = "", Variant AParam4Val = 0);


//  void          __fastcall GetUnitDopPlanCodes(__int64 AUCode, UnicodeString &AAddrCode, UnicodeString &AOrgCode, UnicodeString &AUchCode, UnicodeString &AProfCode);
  System::UnicodeString __fastcall GetPlanProgress();
  bool __fastcall CheckMIBPByAge(TDate APatBirthDay, TDate FPrePlanDate, __int64 FMIBPCode);
  void __fastcall SortByCountAndPrior(TIntMap * AMIBPList, TIntMap *& ASortedMIBPList);

  __property TTagNode *PlanOpt = {read=FPlanOpt};

  __property TTagNode *VacSchXML = {read=FVacSchXML};

  __property TAxeXMLContainer* XMLList = {read=FXMLList};
  __property TdsCommClassData *ClsData = {read=FClsData};

  __property TdsOnPlanGetMIBPListEvent OnGetMIBPList    = {read=FOnGetMIBPList, write=FOnGetMIBPList};
  __property TdsOnPlanCreateUnitPlanEvent OnCreateUnitPlan    = {read=FOnCreateUnitPlan, write=FOnCreateUnitPlan};
  __property TdsPlanGetStrValByCodeEvent  OnGetOrgStr = {read = FOnGetOrgStr, write=FOnGetOrgStr};
};
//---------------------------------------------------------------------------
extern PACKAGE TPlanLinkerDM *PlanLinkerDM;
//---------------------------------------------------------------------------
#endif
