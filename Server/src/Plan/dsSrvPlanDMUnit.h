//---------------------------------------------------------------------------

#ifndef dsSrvPlanDMUnitH
#define dsSrvPlanDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Vcl.ExtCtrls.hpp>
//#include <FireDAC.Phys.FB.hpp>
//#include <FireDAC.Comp.Client.hpp>
//#include <FireDAC.Phys.hpp>
//#include <FireDAC.Phys.Intf.hpp>
//#include <FireDAC.Stan.Def.hpp>
//#include <FireDAC.Stan.Error.hpp>
//#include <FireDAC.Stan.Intf.hpp>
//#include <FireDAC.Stan.Option.hpp>
//#include <FireDAC.UI.Intf.hpp>
//#include <Data.DB.hpp>
//#include <FireDAC.Comp.DataSet.hpp>
//#include <FireDAC.DApt.hpp>
//#include <FireDAC.DApt.Intf.hpp>
//#include <FireDAC.DatS.hpp>
//#include <FireDAC.Stan.Async.hpp>
//#include <FireDAC.Stan.Param.hpp>
//#include <FireDAC.Stan.Pool.hpp>
//#include <FireDAC.Phys.FBDef.hpp>
//#include <FireDAC.Comp.UI.hpp>
//#include <FireDAC.VCLUI.Wait.hpp>
//---------------------------------------------------------------------------
#include "dsRegClassData.h"
#include <System.JSON.hpp>
#include "XMLContainer.h"
#include "dsSrvClassifUnit.h"
#include "icsLog.h"
#include <list>
#include "dsDocSQLCreator.h"
#include "IMMNomDef.h"
#include "JSONUtils.h"
#include "dsCommClassData.h"
#include "PlanerCommon.h"
//---------------------------------------------------------------------------
//���� ������ ������������
//---------------------------------------------------------------------------
typedef void          (__closure *TdsOnPlanGetValByIdEvent)(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARetData);
typedef void          (__closure *TdsOnPlanGetValById10Event)(System::UnicodeString AId, System::UnicodeString ARecId, TJSONObject *& ARetData);

typedef UnicodeString (__closure *TdsOnPlanDeleteDataEvent)(System::UnicodeString AId, System::UnicodeString ARecId);
typedef UnicodeString (__closure *TdsOnPlanInsertDataEvent)(System::UnicodeString AId, System::UnicodeString AValue, System::UnicodeString &ARecId);
typedef void          (__closure *TdsOnPlanGetMIBPListEvent)(System::UnicodeString AId, TJSONObject *& ARetData);
typedef UnicodeString  (__closure *TdsPlanGetStrValByCodeEvent)(UnicodeString ACode, unsigned int AParams);
//---------------------------------------------------------------------------
//typedef map<int,int> TIntMap;
//typedef map<int, TIntMap*> TIntListMap;
//---------------------------------------------------------------------------

class TdsSrvPlanDM : public TDataModule
{
__published:	// IDE-managed Components
private:	// User declarations
  TFDConnection *FConnection;


  TTagNode *FRegDef;
  void __fastcall FSetRegDef(TTagNode *AVal);

  TTagNode *FUnitDataDef;
  bool __fastcall FSetFieldsDef(TTagNode *ANode, UnicodeString &ATmp);

  TAxeXMLContainer *FXMLList;

  TdsDocSQLCreator *FSQLCreator;

  TTagNode *FVacSchXML;
  TTagNode *FTestSchXML;
  TTagNode *FCheckSchXML;
  TTagNode *FMIBPAgeDef;


  TICSIMMNomDef      *FImmNom;
  TdsCommClassData *FClsData;
  TFDQuery* FImmNomQuery;
  int FCommPlanCount, FCurrPlan;
  TdsSrvClassifDataMap  FClassifData;
  TdsOnPlanGetValByIdEvent  FOnPlanGetValById;
  TdsOnPlanGetValById10Event  FOnPlanGetValById10;

  TdsOnPlanDeleteDataEvent  FOnPlanDeleteData;
  TdsOnPlanInsertDataEvent  FOnPlanInsertData;
//  TdsOnPlanGetMIBPListEvent FOnGetMIBPList;
//  TdsPlanGetStrValByCodeEvent  FOnGetOrgStr;
//  TAnsiStrMap   FSelectSQLMap;
//  TAnsiStrMap   FClassSQLMap;
//  TAnsiStrMap   FSelectRecSQLMap;
//  TAnsiStrMap   FCountSQLMap;
//  TAnsiStrMap   FDeleteSQLMap;
//  TAnsiStrMap   FKeyFlMap;
//  TListMap      FFields;
//  TAnsiStrMap   FSelectSQLMap;
//  TAnsiStrMap   FSelectSQLMap;

  UnicodeString __fastcall FGetDepVal(UnicodeString AVals, UnicodeString AUID );
  bool __fastcall FDepValIsNull(UnicodeString AVals);
  UnicodeString __fastcall GetDependValSQL(UnicodeString ARef, bool AEmptyIfNoDepend);
  void __fastcall GetDependClassCode(UnicodeString ADepClName, UnicodeString ADepFlVal, UnicodeString ARetFlName, UnicodeString &ARetVals);

  void __fastcall CheckSQLs(UnicodeString AId);
  void __fastcall GetSQLs(UnicodeString AId);
  UnicodeString __fastcall GetKeyFl(UnicodeString AId);
  UnicodeString __fastcall GetFlNameById(UnicodeString ATabId, UnicodeString AFlId);
  UnicodeString __fastcall GetTabName(UnicodeString AId);
  UnicodeString __fastcall GetCountSQL(UnicodeString AId);
  UnicodeString __fastcall GetDeleteSQL(UnicodeString AId);
  UnicodeString __fastcall GetClassSQL(UnicodeString AId);
  UnicodeString __fastcall GetSelectSQL(UnicodeString AId);
  UnicodeString __fastcall GetSelectRecSQL(UnicodeString AId);
  UnicodeString __fastcall GetSelectRec10SQL(UnicodeString AId);
  UnicodeString __fastcall GetOrderSQL(UnicodeString AId);
  TStringList*  __fastcall GetFieldsList(UnicodeString AId);
  bool          __fastcall IsQuotedKey(UnicodeString AId);
  UnicodeString __fastcall GetWhereFromFilter (TTagNode *AFlt, UnicodeString AMainISUID);
  void          __fastcall FRegGetSVal(UnicodeString AFuncName, TTagNode *ANode, UnicodeString &ATab, UnicodeString &ARet);

  void __fastcall FDocGetSVal(UnicodeString AFuncName, TTagNode *ANode, UnicodeString &ATab, UnicodeString &ARet);
  void _fastcall LoadPlanSetting();

public:		// User declarations
  __fastcall TdsSrvPlanDM(TComponent* Owner, TFDConnection *AConnection, TAxeXMLContainer *AXMLList, TdsCommClassData *AClsData);
  __fastcall ~TdsSrvPlanDM();

  UnicodeString __fastcall NewGUID();

  TTagNode    *FPlanOpt;
  TTagNode    *FPlanOptDefSch;

  UnicodeString __fastcall GetPlanTemplateDef(int AType);
  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AParam1Name, UnicodeString AParam1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AParam2Name = "", Variant AParam2Val = 0,
                               UnicodeString AParam3Name = "", Variant AParam3Val = 0,
                               UnicodeString AParam4Name = "", Variant AParam4Val = 0);
  bool __fastcall quExecParamStm(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
      UnicodeString AParam1Name, UnicodeString AParam1Val,
      UnicodeString ALogComment,    UnicodeString AParam2Name, TStream *AParam2Val);


  __int64       __fastcall GetCount(UnicodeString AId, System::UnicodeString AFilterParam);
  TJSONObject*  __fastcall GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam);
  TJSONObject*  __fastcall GetValById(System::UnicodeString AId, System::UnicodeString ARecId);
  TJSONObject*  __fastcall GetValById10(System::UnicodeString AId, System::UnicodeString ARecId);
  TJSONObject*  __fastcall Find(System::UnicodeString AId, TJSONObject *AFindParam);
  UnicodeString __fastcall DeleteData(System::UnicodeString AId, System::UnicodeString ARecId);
  UnicodeString __fastcall DeletePlan(System::UnicodeString ARecId);
  UnicodeString __fastcall DeletePatientPlan(System::UnicodeString AId, System::UnicodeString ARecId);
  UnicodeString __fastcall SetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString AVal);
  UnicodeString __fastcall GetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, UnicodeString &AMsg);
  void          __fastcall GetUnitDopPlanCodes(__int64 AUCode, UnicodeString &AAddrCode, UnicodeString &AOrgCode, UnicodeString &AUchCode, UnicodeString &AProfCode);
  UnicodeString __fastcall GetPatPlanOptions(__int64 APatCode);
  UnicodeString __fastcall GetUnitSch(__int64 AUCode);
  UnicodeString __fastcall GetDefUnitSch(__int64 AUCode);
  void __fastcall SetUnitSch(__int64 AUCode, UnicodeString ASch);
  System::UnicodeString __fastcall GetPlanProgress();
  System::UnicodeString __fastcall GetPlanOpt();
  void __fastcall SetPlanOpt(UnicodeString AOpts);
  UnicodeString __fastcall GetUnitData(UnicodeString AUCode, TDate & ABithDay);

  __property TTagNode *RegDef = {read = FRegDef, write=FSetRegDef};
  __property TTagNode *PlanOpt = {read=FPlanOpt};
  __property TTagNode *UnitDataDef = {read=FUnitDataDef};

  __property TTagNode *VacSchXML = {read=FVacSchXML};
  __property TTagNode *TestSchXML = {read=FTestSchXML};
  __property TTagNode *CheckSchXML = {read=FCheckSchXML};

  __property TAxeXMLContainer* XMLList = {read=FXMLList};
  __property TdsCommClassData *ClsData = {read=FClsData};
  __property TFDConnection *Connection = {read=FConnection};


  __property TdsOnPlanDeleteDataEvent  OnPlanDeleteData = {read=FOnPlanDeleteData, write=FOnPlanDeleteData};
  __property TdsOnPlanInsertDataEvent  OnPlanInsertData = {read=FOnPlanInsertData, write=FOnPlanInsertData};
  __property TdsOnPlanGetValByIdEvent  OnGetValById     = {read=FOnPlanGetValById, write=FOnPlanGetValById};
  __property TdsOnPlanGetValById10Event  OnGetValById10     = {read=FOnPlanGetValById10, write=FOnPlanGetValById10};
//  __property TdsOnPlanGetMIBPListEvent OnGetMIBPList    = {read=FOnGetMIBPList, write=FOnGetMIBPList};
//  __property TdsPlanGetStrValByCodeEvent  OnGetOrgStr = {read = FOnGetOrgStr, write=FOnGetOrgStr};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsSrvPlanDM *dsSrvPlanDM;
//---------------------------------------------------------------------------
#endif
