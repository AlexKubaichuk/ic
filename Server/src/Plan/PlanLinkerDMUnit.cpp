// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <System.DateUtils.hpp>
#include "PlanLinkerDMUnit.h"
#include "dsSrvRegTemplate.h"
#include "OrgParsers.h"
#include "KLADRParsers.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TPlanLinkerDM * PlanLinkerDM;
const UnicodeString csXMLDateFmtStr = "dd.mm.yyyy";
const wchar_t cchXMLDateSeparator = '.';
const wchar_t cchXMLPeriodSeparator = '-';
const wchar_t cchXMLDurationSeparator = '/';
// ---------------------------------------------------------------------------
__fastcall TPlanLinkerDM::TPlanLinkerDM(TComponent * Owner, TFDConnection * AConnection, TTagNode *APlanOpt, TAxeXMLContainer * AXMLList,
  TdsCommClassData * AClsData) : TDataModule(Owner)
 {
  FConnection             = AConnection;
  FClsData                = AClsData;
  FStoreMIBPList          = new TIntMap;
  FDefStoreMIBPList       = new TIntMap;
  FSortedStoreMIBPList    = new TIntMap;
  FDefSortedStoreMIBPList = new TIntMap;
  FStoreTestList          = new TIntMap;
  FDefStoreTestList       = new TIntMap;
  FXMLList                = AXMLList;
//  FPlanOpt                = FXMLList->GetXML("planSetting");
  FPlanOpt                = APlanOpt;
  FPlanOptDefSch          = FPlanOpt->GetChildByName("infdef", true);
  FVacSchXML              = FXMLList->GetXML("12063611-00008cd7-cd89");
  FMIBPAgeDef             = FVacSchXML->GetChildByName("mibpparams", true);
  // FSQLCreator            = new TdsDocSQLCreator(FXMLList); /*FDM->ICSNom*/
  // FSQLCreator->OnGetSVal = FDocGetSVal;
  FOnCreateUnitPlan = NULL;
  // FImmNomQuery    = CreateTempQuery();
  FClassDataQuery = CreateTempQuery();
  FClassData      = new TdsRegClassData(FXMLList->GetXML("40381E23-92155860-4448"), FClassDataQuery);
  // FImmNom = new TICSIMMNomDef(FImmNomQuery, FXMLList, "40381E23-92155860-4448", FClsData);
 }
// ---------------------------------------------------------------------------
__fastcall TPlanLinkerDM::~TPlanLinkerDM()
 {
  // delete FImmNom;
  // DeleteTempQuery(FImmNomQuery);
  // delete FSQLCreator;
  for (TdsSrvClassifDataMap::iterator i = FClassifData.begin(); i != FClassifData.end(); i++)
   delete i->second;
  FClassifData.clear();
  delete FStoreMIBPList;
  delete FDefStoreMIBPList;
  delete FSortedStoreMIBPList;
  delete FDefSortedStoreMIBPList;
  delete FStoreTestList;
  delete FDefStoreTestList;
  delete FClassData;
  DeleteTempQuery(FClassDataQuery);
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TPlanLinkerDM::GetPlanTemplateDef(int AType)
 {
  UnicodeString RC = "<root/>";
  try
   {
    UnicodeString FFN = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\template\\pl" + IntToStr(AType) + ".xml");
    if (FileExists(FFN))
     {
      TTagNode * tmplNode = new TTagNode;
      try
       {
        tmplNode->LoadFromXMLFile(FFN);
        RC = tmplNode->AsXML;
       }
      __finally
       {
        delete tmplNode;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TPlanLinkerDM::GetClassData(long ACode, UnicodeString AName)
 {
  UnicodeString RC = "";
  try
   {
    try
     {
      RC = FClassData->GetData(AName, ACode);
     }
    catch (Exception & E)
     {
      dsLogError(E.Message + "; AName = " + AName, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinkerDM::ClearHash()
 {
  FClassData->ClearHash("");
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TPlanLinkerDM::CreateTempQuery()
 {
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = FConnection;
    FQ->Connection                       = FConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  return FQ;
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinkerDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
// ---------------------------------------------------------------------------
bool __fastcall TPlanLinkerDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsPlanLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TPlanLinkerDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
  UnicodeString AParam1Name, UnicodeString AParam1Val, UnicodeString ALogComment, UnicodeString AParam2Name,
  Variant AParam2Val, UnicodeString AParam3Name, Variant AParam3Val, UnicodeString AParam4Name, Variant AParam4Val)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
      if (AParam2Name.Length())
       AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
      if (AParam3Name.Length())
       AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
      if (AParam4Name.Length())
       AQuery->ParamByName(AParam4Name)->Value = AParam4Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      UnicodeString FParams = "; Params: ";
      if (AParam1Name.Length())
       FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
      if (AParam2Name.Length())
       FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
      if (AParam3Name.Length())
       FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
      if (AParam4Name.Length())
       FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "");
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    UnicodeString FParams = "; Params: ";
    if (AParam1Name.Length())
     FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
    if (AParam2Name.Length())
     FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
    if (AParam3Name.Length())
     FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
    if (AParam4Name.Length())
     FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "");
    dsLogSQL(((ALogComment.Length()) ? ALogComment : ASQL) + FParams, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
/* void __fastcall TPlanLinkerDM::GetUnitDopPlanCodes(__int64 AUCode, UnicodeString & AAddrCode, UnicodeString & AOrgCode, UnicodeString & AUchCode, UnicodeString & AProfCode)
 {
 AAddrCode = "";
 AOrgCode  = "";
 AUchCode  = "";
 AProfCode = "";
 TFDQuery * FQ = CreateTempQuery();
 try
 {
 try
 {
 dsPlanLogMessage("PtId : " + IntToStr(AUCode), __FUNC__, 5);
 //�����' uid = '0136'
 //�����������' uid = '0025'
 //���' uid = '00F1'
 //�������' uid = '00B3'
 if (quExecParam(FQ, false, "Select R0136, R0025, R00F1, R00B3 From class_1000 where code=:pCode", "pCode", AUCode))
 {
 if (!FQ->FieldByName("R0136")->IsNull)
 AAddrCode = FQ->FieldByName("R0136")->AsString;
 if (!FQ->FieldByName("R0025")->IsNull)
 AOrgCode = FQ->FieldByName("R0025")->AsString;
 if (!FQ->FieldByName("R00B3")->IsNull)
 AUchCode = FQ->FieldByName("R00B3")->AsString;
 //if (!FQ->FieldByName("R")->IsNull)
 //AProfCode = FQ->FieldByName("R")->AsString;
 }
 }
 catch (Exception & E)
 {
 dsPlanLogError("PtId : " + IntToStr(AUCode) + ", ��������� ���������: " + E.Message, __FUNC__, 5);
 }
 }
 __finally
 {
 if (FQ->Transaction->Active)
 FQ->Transaction->Commit();
 DeleteTempQuery(FQ);
 }
 }
 //--------------------------------------------------------------------------- */
// void __fastcall TPlanLinkerDM::FDocGetSVal(UnicodeString AFuncName, TTagNode * ANode, UnicodeString & ATab, UnicodeString & ARet)
// {
// FImmNom->GetSVal(AFuncName, ANode, ATab, ARet);
// }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TPlanLinkerDM::GetUchName(TJSONObject * AValue)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "� � � � � �";
  try
   {
    __int64 FUchCode = JSONToInt(AValue, "00B3", -1);
    if (FUchCode != -1)
     RC = " ��.: " + GetClassData(FUchCode, "000E");
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TPlanLinkerDM::GetOrgName(TJSONObject * AValue)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "� � � � � �";
  try
   {
    if (FOnGetOrgStr)
     {
      if (JSONToString(AValue, "0025").Length())
       {
        orgCode FOrgCode;
        FOrgCode.OrgID  = JSONToString(AValue, "0025").ToIntDef(0);
        FOrgCode.L1Val  = JSONToString(AValue, "0026");
        FOrgCode.L2Val  = JSONToString(AValue, "0027");
        FOrgCode.L3Val  = JSONToString(AValue, "0028");
        FOrgCode.Format = UIDStr(JSONToString(AValue, "0112").ToIntDef(0));
        if (FOrgCode.OrgID)
         {
          RC = FOnGetOrgStr(FOrgCode.AsString(), 0);
         }
        else
         RC = "�� ������ ��� �����������";
       }
     }
    RC = "���.: " + RC;
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TPlanLinkerDM::GetFltName(TJSONObject * AValue)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "� � � � � �";
  try
   {
    RC = "������";
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinkerDM::GetMIBPFromStore()
 {
  try
   {
    if (FOnGetMIBPList)
     {
      TJSONObject * MIBPRet = NULL;
      TJSONPairEnumerator * itPair;
      FOnGetMIBPList(1, MIBPRet);
      if (MIBPRet)
       {
        FStoreMIBPList->clear();
        FSortedStoreMIBPList->clear();
        int FMIBPCode, FMIBPCount;
        itPair = MIBPRet->GetEnumerator();
        while (itPair->MoveNext())
         {
          FMIBPCode  = ((TJSONNumber *)itPair->Current->JsonString)->AsInt;
          FMIBPCount = ((TJSONNumber *)itPair->Current->JsonValue)->AsInt;
          if (FClsData->VacInfList.find(FMIBPCode) != FClsData->VacInfList.end())
           {
            (*FStoreMIBPList)[FMIBPCode] = FMIBPCount;
            (*FSortedStoreMIBPList)[((FClsData->GetMIBPInfCount(FMIBPCode) << 16) & 0xFFFF0000) + FMIBPCode] =
              FMIBPCode;
           }
          else
           dsPlanLogError("��� ���� (" + IntToStr(FMIBPCode) + ") �� ������ ������ ��������.", __FUNC__);
         }
       }
      TJSONObject * TestRet = NULL;
      FOnGetMIBPList(2, TestRet);
      if (TestRet)
       {
        FStoreTestList->clear();
        int FTestCode, FTestCount;
        itPair = TestRet->GetEnumerator();
        while (itPair->MoveNext())
         {
          FTestCode  = ((TJSONNumber *)itPair->Current->JsonString)->AsInt;
          FTestCount = ((TJSONNumber *)itPair->Current->JsonValue)->AsInt;
          (*FStoreTestList)[FTestCode] = FTestCount;
         }
       }
     }
    if (true)
     {
      TJSONObject * MIBPRet = GetDefMIBP(1);
      TJSONPairEnumerator * itPair;
      if (MIBPRet)
       {
        FDefStoreMIBPList->clear();
        FDefSortedStoreMIBPList->clear();
        int FMIBPCode, FMIBPCount;
        itPair = MIBPRet->GetEnumerator();
        while (itPair->MoveNext())
         {
          FMIBPCode  = ((TJSONNumber *)itPair->Current->JsonString)->AsInt;
          FMIBPCount = ((TJSONNumber *)itPair->Current->JsonValue)->AsInt;
          if (FClsData->VacInfList.find(FMIBPCode) != FClsData->VacInfList.end())
           {
            (*FDefStoreMIBPList)[FMIBPCode] = FMIBPCount;
            (*FDefSortedStoreMIBPList)[((FClsData->GetMIBPInfCount(FMIBPCode) << 16) & 0xFFFF0000) + FMIBPCode] =
              FMIBPCode;
           }
          else
           dsPlanLogError("��� ���� (" + IntToStr(FMIBPCode) + ") �� ������ ������ ��������.", __FUNC__);
         }
       }
      TJSONObject * TestRet = GetDefMIBP(2);
      if (TestRet)
       {
        FStoreTestList->clear();
        int FTestCode, FTestCount;
        itPair = TestRet->GetEnumerator();
        while (itPair->MoveNext())
         {
          FTestCode  = ((TJSONNumber *)itPair->Current->JsonString)->AsInt;
          FTestCount = ((TJSONNumber *)itPair->Current->JsonValue)->AsInt;
          (*FDefStoreTestList)[FTestCode] = FTestCount;
         }
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TPlanLinkerDM::MIBPInStore(__int64 AMIBPCode)
 {
  bool RC = false;
  try
   {
    TIntMap::iterator FRC = FStoreMIBPList->find(AMIBPCode);
    RC = (FRC != FStoreMIBPList->end());
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TPlanLinkerDM::MIBPInDefStore(__int64 AMIBPCode)
 {
  bool RC = false;
  try
   {
    TIntMap::iterator FRC = FDefStoreMIBPList->find(AMIBPCode);
    RC = (FRC != FDefStoreMIBPList->end());
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TPlanLinkerDM::GetDefMIBP(int AType)
 {
  TJSONObject * RC = new TJSONObject;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    UnicodeString FSQL = "";
    if (AType == 1)
     {
      FSQL = "Select CODE From CLASS_0035";
     }
    else if (AType == 2)
     {
      FSQL = "Select CODE From CLASS_002a";
     }
    if (FSQL.Length())
     {
      if (quExec(FQ, false, FSQL))
       {
        while (!FQ->Eof)
         {
          RC->AddPair(FQ->FieldByName("code")->AsString, "10000");
          FQ->Next();
         }
       }
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TPlanLinkerDM::GetNewCode(UnicodeString TabId)
 {
  __int64 RC = -1;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    if (!FQ->Transaction->Active)
     FQ->Transaction->StartTransaction();
    FQ->Command->CommandKind = skStoredProc;
    FQ->SQL->Text            = "MCODE_" + TabId;
    FQ->Prepare();
    FQ->OpenOrExecute();
    RC = FQ->FieldByName("MCODE")->AsInteger;
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TPlanLinkerDM::NewGUID()
 {
  return icsNewGUID().UpperCase();
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TPlanLinkerDM::GetPlanProgress()
 {
  UnicodeString RC = "progress";
  try
   {
    Application->ProcessMessages();
    RC = IntToStr(FCurrPlan) + "/" + IntToStr(FCommPlanCount);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
// void __fastcall TPlanLinkerDM::FRegGetSVal(UnicodeString AFuncName, TTagNode * ANode, UnicodeString & ATab, UnicodeString & ARet)
// {
// FImmNom->GetSVal(AFuncName, ANode, ATab, ARet);
// }
// ---------------------------------------------------------------------------
bool __fastcall TPlanLinkerDM::CheckMIBPByAge(TDate APatBirthDay, TDate FPrePlanDate, __int64 FMIBPCode)
 {
  bool RC = true;
  try
   {
    if (FMIBPAgeDef)
     {
      TTagNode * FMIBPParam = FMIBPAgeDef->GetChildByAV("m", "ref", IntToStr(FMIBPCode));
      if (FMIBPParam)
       {
        bool inRange = true;
        if (FMIBPParam->AV["f"].Length()) // FPrePlanDate >= APatBirthDay+������� ��
           inRange &= (CompareDate(FPrePlanDate, IncDate(APatBirthDay, FMIBPParam->AV["f"])) != LessThanValue);
        if (inRange && FMIBPParam->AV["t"].Length()) // FPrePlanDate < APatBirthDay+������� ��
           inRange &= (CompareDate(FPrePlanDate, IncDate(APatBirthDay, FMIBPParam->AV["t"])) == LessThanValue);
        RC = inRange;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TPlanLinkerDM::SortByCountAndPrior(TIntMap * AMIBPList, TIntMap *& ASortedMIBPList)
 {
  for (TIntMap::iterator itMIBP = AMIBPList->begin(); itMIBP != AMIBPList->end(); itMIBP++)
   {
    (*ASortedMIBPList)[(((100 - FClsData->GetMIBPInfCount(itMIBP->first)) << 16) & 0xFFFF0000) + itMIBP->first] =
      itMIBP->first;
   }
 }
// ---------------------------------------------------------------------------
