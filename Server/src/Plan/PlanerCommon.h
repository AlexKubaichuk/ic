//---------------------------------------------------------------------------

#ifndef PlanerCommonH
#define PlanerCommonH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
//#include "dsRegClassData.h"
#include <System.JSON.hpp>
//#include "XMLContainer.h"
//#include "dsSrvClassifUnit.h"
//#include "icsLog.h"
//#include <list>
//#include "dsDocSQLCreator.h"
//#include "IMMNomDef.h"
//#include "JSONUtils.h"
//#include "dsCommClassData.h"
//---------------------------------------------------------------------------
extern PACKAGE void __fastcall XMLStrToDuration(UnicodeString AValue, int *AYears, int *AMonths, int *ADays);
extern PACKAGE TDate __fastcall IncDate(TDate ABase, UnicodeString APeriod);
extern PACKAGE TDate __fastcall CalculatePlanPeriod(TDate ADate, TDate &ADateBP, TDate &ADateEP);
//---------------------------------------------------------------------------
#endif
