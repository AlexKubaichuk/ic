/*
 ����        - Planer.cpp
 ������      - ���������� ������������.
 ��������� "���������� ������������"
 ��������    - �����������.
 ���� ����������
 ����������� - ������� �.�.
 ����        - 09.05.2004
 */
//---------------------------------------------------------------------------

#include <string.h>
#pragma hdrstop
#include <algorithm>
#include <vcl.h>

//#include "DKDBUtils.h"
#include "DKSTL.h"

#include "PrePlaner.h"
#include "dsSrvRegTemplate.h"
#include "JSONUtils.h"
//#include "SchList.h"
//#include "ICSDocGlobals.h"

//---------------------------------------------------------------------------
/*extern*/ const UnicodeString csEndLineLabel = "��������� �����";
/*extern*/ const UnicodeString csBegLineLabel = "������ �����";

#pragma package(smart_init)

//###########################################################################
//##                                                                       ##
//##                     TExecutor::TBackgroundPlaner                      ##
//##                                                                       ##
//###########################################################################

class TPrePlaner::TBackgroundPlaner : public TThread
 {
 private:
  TPrePlaner * FPlaner;
  TDate        FBegDate;
  __int64      FPtCode;

 protected:
  virtual void __fastcall Execute();

 public:
  __fastcall TBackgroundPlaner(bool CreateSuspended, TPrePlaner * APlaner, TDate ABegDate, __int64 APtCode);
  __fastcall ~TBackgroundPlaner();

  void __fastcall ThreadSafeCall(TdsSyncMethod Method);
 };

//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                     TExecutor::BackgroundExecutor                     ##
//##                                                                       ##
//###########################################################################

__fastcall TPrePlaner::TBackgroundPlaner::TBackgroundPlaner(bool CreateSuspended, TPrePlaner * APlaner, TDate ABegDate, __int64 APtCode) :
    TThread(CreateSuspended),
    FPlaner(APlaner),
    FBegDate(ABegDate),
    FPtCode(APtCode)
 {
 }

//---------------------------------------------------------------------------
__fastcall TPrePlaner::TBackgroundPlaner::~TBackgroundPlaner()
 {
  FPlaner = NULL;
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::TBackgroundPlaner::Execute()
 {
  //CallLogBegin();
  CoInitialize(NULL);

  FPlaner->CreateUnitPlan(FBegDate, FPtCode);
  //CallLogEnd();
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::TBackgroundPlaner::ThreadSafeCall(TdsSyncMethod Method)
 {
  Synchronize(Method);
 }
//---------------------------------------------------------------------------
//###########################################################################
//##                                                                       ##
//##                            TIMMJumpTrace                              ##
//##                                                                       ##
//###########################################################################

__fastcall TIMMJumpTrace::TIMMJumpTrace(TdsPlanDM * ADM)
 {
  FDM       = ADM;
  TraceKind = TPlanItemKind::pkUnknown;
 }
//---------------------------------------------------------------------------
__fastcall TIMMJumpTrace::~TIMMJumpTrace()
 {
 }
//---------------------------------------------------------------------------
void __fastcall TIMMJumpTrace::Clear()
 {
  JumpTraceList.clear();
 }

//---------------------------------------------------------------------------

void __fastcall TIMMJumpTrace::Append(TTagNode * ASchemeLine, TJumpKind AJumpKind, UnicodeString AJumpReason)
 {
  if (!ASchemeLine)
   throw DKClasses::ENullArgument(__FUNC__, "ASchemeLine");

  TRACE_ITEM TraceItem;
  TraceItem.SchemeUID = ASchemeLine->GetParent()->AV["uid"];
  TraceItem.LineUID   = ASchemeLine->AV["uid"];
  TraceItem.Name      = ASchemeLine->GetParent()->AV["name"] +
      "." +
      (
      (ASchemeLine->CmpName("endline"))
      ? csEndLineLabel.LowerCase()
      : (
      (ASchemeLine->CmpName("beginline"))
      ? csBegLineLabel.LowerCase()
      : ASchemeLine->AV["name"]
      )
      );
  TraceItem.RefOwner   = StrToInt(ASchemeLine->GetParent()->AV["ref"]);
  TraceItem.JumpKind   = AJumpKind;
  TraceItem.JumpReason = AJumpReason;
  JumpTraceList.push_back(TraceItem);
 }

//---------------------------------------------------------------------------

bool __fastcall TIMMJumpTrace::FindByLineUID(UnicodeString ALineUID, TJumpTraceListIt * AJumpTraceListIt)
 {
  TJumpTraceListIt i = find_if(JumpTraceList.begin(),
      JumpTraceList.end(),
      DKSTL::compose1(bind2nd(equal_to<UnicodeString>(),
      ALineUID),
      SelectTraceItemLineUID()));
  if (AJumpTraceListIt)
   (* AJumpTraceListIt) = i;
  return (i != JumpTraceList.end());
 }

//---------------------------------------------------------------------------
int __fastcall TIMMJumpTrace::GetTraceStrings(TStringList * TraceStrs)
 {
  if (!TraceStrs)
   throw DKClasses::ENullArgument(__FUNC__, "TraceStrs");

  UnicodeString Str;
  TraceStrs->Clear();
  for (TJumpTraceListIt i = JumpTraceList.begin(); i != JumpTraceList.end(); i++)
   {
    Str = ((TraceKind == TPlanItemKind::pkVakchina) ? "���� " + FDM->VacName[IntToStr((*i).RefOwner)] : "����� " + FDM->TestName[IntToStr((*i).RefOwner)]) +
        " (" + (*i).Name + ")#";
    if ((* i).JumpKind == jkUnknown)
     Str += (* i).JumpReason;
    else
     {
      Str += "( �������: ";
      switch ((*i).JumpKind)
       {
       case jkSrok:
        Str += "�� �����";
        break;
       case jkAge:
        Str += "�� ��������";
        break;
       case jkCond:
        Str += "�� �������";
        break;
       case jkEnd:
        Str += "�����������";
        break;
       }
      Str += "; ��������� ��� ��������: " + (* i).JumpReason;
      Str += " )";
     }
    TraceStrs->Add(Str);
   }
  return TraceStrs->Count;
 }

//###########################################################################
//##                                                                       ##
//##                              TPrePlaner                               ##
//##                                                                       ##
//###########################################################################

//ADB                  - ������� ���� ������
//FDebugList           - ���� ��� ���������� ������
//[default = ""]

__fastcall TPrePlaner::TPrePlaner(TdsPlanDM * ADM)
 {
  FDM              = ADM;
  FUnitPlanSetting = new TTagNode;
  FVacSchXML       = NULL;
  FTestSchXML      = NULL;
  FCheckSchXML     = NULL;

  FBkgPlaner  = NULL;
  FPCStage    = "";
  FPCommStage = "";

  FXML = NULL;

  FDebugLog            = new TPrePlanerDebug(FDM);
  FDebugLog->OnSaveLog = CallSaveDebugLog;

  FSetDebugLog();

  FPtId     = -1;
  FPtFIO    = "";
  FUnitData = NULL;

  FJumpTrace = new TIMMJumpTrace(FDM);

  FMaxProgress         = 10;
  FOnIncProgress       = NULL;
  FOnPlanerStageChange = NULL;

  FErrLog = NULL;

  FTempSL = new TStringList;
 }

//---------------------------------------------------------------------------

__fastcall TPrePlaner::~TPrePlaner()
 {
//  delete FDM;
  delete FDebugLog;
  FXML = NULL;
  delete FJumpTrace;
  delete FTempSL;
  if (FUnitData)
   delete FUnitData;
  if (FBkgPlaner)
   delete FBkgPlaner;
  FBkgPlaner = NULL;
  delete FUnitPlanSetting;
  //if (FDebugFile) delete FDebugFile;
 }

//---------------------------------------------------------------------------
TTagNode * __fastcall TPrePlaner::FGetDebugLog()
 {
  if (FDebugLog)
   return FDebugLog->Log;
  else
   return NULL;
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::FSetDebugLog()
 {
  if (FDebugLog)
   {
    FDebugLog->InitLog();
    FDebugLog->PrivPlan      = &FPrivPlan;
    FDebugLog->DopPrivivPlan = &FDopPrivivPlan;
    FDebugLog->ProbPlan      = &FTestPlan;
    FDebugLog->DopProbPlan   = &FDopTestPlan;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::CheckData(UnicodeString AMethodName)
 {
  if (FPtId == -1)
   throw EIMMPlanerError(AMethodName, "�� ������ ������� (PtId).");
  if (!FVacSchXML)
   throw EIMMPlanerError(AMethodName, "�� ������ xml-�������� �� ������� ����������.");

  if (!FTestSchXML)
   throw EIMMPlanerError(AMethodName, "�� ������ xml-�������� �� ������� ����.");

  if (!FCheckSchXML)
   throw EIMMPlanerError(AMethodName, "�� ������ xml-�������� �� ������� ��������.");
 }

//---------------------------------------------------------------------------
void __fastcall TPrePlaner::CallOnIncProgress()
 {
  SyncCall(& SyncCallOnIncProgress);
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::SyncCallOnIncProgress()
 {
  if (FOnIncProgress)
   FOnIncProgress(this);
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::CallOnPlanerStageChange(TPlanerStage NewPlanerStage)
 {
  FPlanerStage = NewPlanerStage;
  SyncCall(& SyncCallOnPlanerStageChange);
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::SyncCallOnPlanerStageChange()
 {
  if (FOnPlanerStageChange)
   OnPlanerStageChange(this, FPlanerStage);
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::SetVacSchXML(TTagNode * AValue)
 {
  if (FVacSchXML != AValue)
   {
    if (!AValue->CmpName("imm-s"))
     throw EIMMPlanerError(__FUNC__, "���������� ������� xml-�������� �� ������� ���������� (��� imm-s).");
    FVacSchXML = AValue;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::SetTestSchXML(TTagNode * AValue)
 {
  if (FTestSchXML != AValue)
   {
    if (!AValue->CmpName("imm-s"))
     throw EIMMPlanerError(__FUNC__, "���������� ������� xml-�������� �� ������� ���� (��� imm-s).");
    FTestSchXML = AValue;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::SetCheckSchXML(TTagNode * AValue)
 {
  if (FCheckSchXML != AValue)
   {
    if (!AValue->CmpName("imm-s"))
     throw EIMMPlanerError(__FUNC__, "���������� ������� xml-�������� �� ������� �������� (��� imm-s).");
    FCheckSchXML = AValue;
   }
 }
//---------------------------------------------------------------------------

void __fastcall TPrePlaner::SetPtId(__int64 Value)
 {
  //if ( FPtId != Value )
  //{
  FPtId = Value;
  if (FUnitData)
   delete FUnitData;
  FUnitData = new TkabCustomDataSetRow(FDM->UnitDataDef);
  FGetPatData();
  FUnitPlanSetting->AsXML = FDM->GetUnitSch(FPtId);

  if (!GetPtAge(FPtAge.Years, FPtAge.Months, FPtAge.Days))
   throw EIMMPlanerError(__FUNC__, "������� �� ������.");
  FPtFIO = VarToStr(FUnitData->Value[PatLName]) + " " + VarToStr(FUnitData->Value[PatName]) + " " + VarToStr(FUnitData->Value[PatSName]);
  //}
 }

//---------------------------------------------------------------------------
void __fastcall TPrePlaner::FGetPatData()
 {
  if (FDM->OnGetValById)
   {
    TJSONObject * tmpRowData;
    TJSONObject * tmpFlData;
    TJSONString * tmpData;
    UnicodeString FFlId;
    UnicodeString FFlVal;
    //FFlVal = Row[ARowId]->Value["code"];
    FDM->OnGetValById("1000", FPtId, tmpRowData);
    TTagNode * itField = FDM->UnitDataDef->GetFirstChild();
    while (itField)
     {
      if (!itField->CmpName("code,data"))
       {
        FFlId     = itField->AV["uid"];
        tmpFlData = (TJSONObject *)tmpRowData->Values[FFlId];
        if (tmpFlData)
         {
          if (!tmpFlData->Null)
           {
            tmpData = (TJSONString *)tmpFlData;
            if (itField->CmpName("binary,choice,choiceDB,choiceTree,extedit"))
             {
              FUnitData->Value[FFlId]    = Variant::Empty();
              FUnitData->StrValue[FFlId] = "";
              if (tmpFlData->Count)
               {
                FFlVal = ((TJSONString *)tmpFlData->Pairs[0]->JsonString)->Value();
                if (FFlVal.Trim().Length())
                 {
                  if (itField->CmpName("binary,choice,choiceDB"))
                   FUnitData->Value[FFlId] = FFlVal.ToInt();
                  else
                   FUnitData->Value[FFlId] = FFlVal;

                 }
                else
                 FUnitData->Value[FFlId] = Variant::Empty();
                FUnitData->StrValue[FFlId] = ((TJSONString *)tmpFlData->Pairs[0]->JsonValue)->Value();
               }
             }
            else
             {
              if (!tmpData->Value().Trim().Length())
               FUnitData->Value[FFlId] = Variant::Empty();
              else
               {
                if (itField->CmpName("date"))
                 FUnitData->Value[FFlId] = TDate(tmpData->Value());
                else
                 FUnitData->Value[FFlId] = tmpData->Value();
               }
             }
           }
         }
       }
      itField = itField->GetNext();
     }
    FUnitData->Fetched = true;
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TPrePlaner::IsDebugPrint()
 {
  return FDebugLog->Log;
 }
//---------------------------------------------------------------------------
//����������� �������� ��������, ��� �������� ����������� ����
bool __fastcall TPrePlaner::GetPtAge(Word & Years, Word & Months, Word & Days)
 {
  CheckData(__FUNC__);

  bool Result = false;
  Years       = 0;
  Months      = 0;
  Days        = 0;
  FPtBirthDay = TDate((double)FUnitData->Value[PatBitrhDay]);
  DateDiff(FPtBirthDay, FBeginDate, Days, Months, Years);
  Result = true;
  return Result;
 }
//---------------------------------------------------------------------------
//������������� ( ����������� ������� ������������ )
void __fastcall TPrePlaner::CalcPlanPeriod(TDate ABegDate)
 {
  FBeginDate = ABegDate;
  FPlanMonth = CalculatePlanPeriod(FDatePB, FDatePE);
  //FDebugLog->DopID       = FDopID;
  //FDebugLog->PPP         = FPPP;
  FDebugLog->LogPlanPeriod(FDatePB, FDatePE, FPlanMonth);
 }
//---------------------------------------------------------------------------
//������ ������� ������������
TDate __fastcall TPrePlaner::CalculatePlanPeriod(TDate & ADateBP, TDate & ADateEP)
 {
  return ::CalculatePlanPeriod(FBeginDate, ADateBP, ADateEP);
 }
//---------------------------------------------------------------------------
TTagNode * __fastcall TPrePlaner::GetSchLineByLastItem(TPlanItemKind APlanItemKind, int AInf, bool fRoundMode, TTagNode * APlanOptSch)
 {
  //!!! ��������� ��������� ���� ������������� ��� �������� ����
  TTagNode * RC = NULL;
  try
   {
    TCardItemList::iterator itCard;
    TCardItemList::iterator CardItem;
    TTagNode * FPlanOptLine, *FPlanOptSch;
    FPlanOptLine = FPlanOptSch = NULL;
    if (APlanOptSch)
     {
      if (APlanOptSch->CmpName("begline, line") && FDM->InfInVac(IntToStr(AInf), APlanOptSch->GetParent()->AV["ref"]))
       {
        FPlanOptLine = APlanOptSch;
        FPlanOptSch  = APlanOptSch->GetParent();
       }
      else if (APlanOptSch->CmpName("schema") && FDM->InfInVac(IntToStr(AInf), APlanOptSch->AV["ref"]))
       FPlanOptSch = APlanOptSch;
     }
    bool FExist = false;
    //��������

    if (APlanItemKind == TPlanItemKind::pkVakchina)
     {
      FCardPrivList.sort(CardItemGtDate());
      for (itCard = FCardPrivList.begin(); (itCard != FCardPrivList.end()) && !FExist; itCard++)
       {
        FExist = ((itCard->Tur == fRoundMode) && (itCard->Inf == AInf) && (itCard->VacType != "���."));
        if (FExist)
         CardItem = itCard;
       }
      if (FExist)
       {//���� ��������� �������� �� �������� AInf
        TTagNode * FPrivSch = NULL;
        TTagNode * FPrivSchLine = NULL;
        bool FSchCorrect = false;
        FPrivSchLine = FXML->GetTagByUID(_UID(CardItem->Sch));
        if (FPrivSchLine && FPrivSchLine->CmpName("line"))
         FPrivSch = FPrivSchLine->GetParent();

        FSchCorrect =
            (//true -  ���� ���������: �������, ��� ����������, uid �����
            //� ������� �� �������� AInf
            FPrivSch
            && FPrivSch->CmpAV("ref", IntToStr(CardItem->Id))
            && FPrivSch->CmpAV("uid", GetLPartB(CardItem->Sch, '.'))
            && FPrivSchLine->CmpAV("name", CardItem->VacType)
            && FDM->InfInVac(IntToStr(AInf), IntToStr(CardItem->Id))
            );
        if (!FSchCorrect)
         {
          FPrivSch     = NULL;
          FPrivSchLine = GetSchemeLineByVacType(CardItem->Id, CardItem->VacType);
          if (FPrivSchLine && FPrivSchLine->CmpName("line"))
           FPrivSch = FPrivSchLine->GetParent();
         }
        if (FPlanOptLine)
         RC = FPlanOptLine;
        else if (FPrivSchLine)
         RC = FPrivSchLine;

        bool UsePrivSch = true;
        if (FPrivSch && FPlanOptSch)
         {
          RC = FPrivSchLine;
          if (!FPlanOptSch->CmpAV("uid", FPrivSch->AV["uid"]))
           {// // ����� � ���������� � � ��������� �������� ����������
            if (FPlanOptLine->CmpName("line") && (VacTypeToInt(FPlanOptLine->AV["name"]) >= VacTypeToInt(FPrivSchLine->AV["name"])))
             RC = FPlanOptLine;
           }
         }
       }
      else
       {//��� �������� �� �������� AInf, ���� ����� �� ���������

        if (FPlanOptLine && FDM->InfInVac(IntToStr(AInf), FPlanOptSch->AV["ref"]))
         {//���� ��������� ������ �� �������� ���� ��������
          RC = FPlanOptLine;
         }
        else
         {//���� ��������� ������ �� �������� ���� �� ���������
          UnicodeString FDefSch = FDM->GetDefSch(AInf, 0).UpperCase();
          if ((FDefSch != csReqFldNULL) && FDefSch.Length())
           RC = FXML->GetTagByUID(_UID(FDefSch));
         }
       }
     }
    //�����
    else if (APlanItemKind == TPlanItemKind::pkTest)
     {
      FCardTestList.sort(CardItemGtDate());
      for (itCard = FCardTestList.begin(); (itCard != FCardTestList.end()) && !FExist; itCard++)
       {
        FExist = (itCard->Inf == AInf);
        if (FExist)
         CardItem = itCard;
       }
      if (FExist)
       {
        if ((CardItem->Sch != csReqFldNULL) && CardItem->Sch.Length())
         RC = FXML->GetTagByUID(_UID(CardItem->Sch));
       }
      else
       {
        UnicodeString FDefSch = FDM->GetDefSch(AInf, 1).UpperCase();
        if ((FDefSch != csReqFldNULL) && FDefSch.Length())
         RC = FXML->GetTagByUID(_UID(FDefSch));
       }
     }
    //��������
    else if (APlanItemKind == TPlanItemKind::pkCheck)
     {
      FCardCheckList.sort(CardItemGtDate());
      for (itCard = FCardCheckList.begin(); (itCard != FCardCheckList.end()) && !FExist; itCard++)
       {
       }
      if (FExist)
       {
       }
      else
       {
        UnicodeString FDefSch = FDM->GetDefSch(AInf, 2).UpperCase();
        if ((FDefSch != csReqFldNULL) && FDefSch.Length())
         RC = FXML->GetTagByUID(_UID(FDefSch));
       }
     }
    if (!RC)
     RC = APlanOptSch;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
int __fastcall TPrePlaner::VacTypeToInt(UnicodeString AVacType)
 {
  dsPlanLogMessage(AVacType, __FUNC__, 5);
  int RC = 0;
  try
   {//V1     - V99             V1 < RV1/1 < RV1
    //RV1/99 - RV99/99
    //RV1    - RV99
    UnicodeString VT = AVacType.UpperCase();
    if (VT[1] == 'V')//V
         RC = GetRPartB(VT, 'V').ToIntDef(1);
    else //RV
     {
      VT = GetRPartB(VT, 'R');
      if (VT.Pos("/"))
       RC = GetLPartB(VT, '/').ToIntDef(1) * 100 + GetRPartB(VT, '/').ToIntDef(1);
      else
       RC = VT.ToIntDef(1) * 1000;
     }
   }
  __finally
   {
   }
  dsPlanLogMessage(IntToStr(RC), __FUNC__, 5);
  return RC;
 }
//---------------------------------------------------------------------------
TTagNode * __fastcall TPrePlaner::GetSchemeLineByVacType(__int64 AMIBId, UnicodeString AVacType)
 {
  TTagNode * RC = NULL;
  try
   {
    TTagNodeList FItemSchList;
    TTagNodeList::iterator itSch;
    TTagNode * SchLine, *FDefSch, *FSch;
    FDefSch = NULL;
    SchLine = NULL;
    UnicodeString MIBPId = IntToStr(AMIBId);

    //FXML->GetRoot()->GetChildByName("schemalist")->Iterate(FGetSchList, MIBPRef);
    TTagNode * itNode = FXML->GetRoot()->GetChildByName("schema", true);
    while (itNode)
     {
      if (itNode->CmpName("schema") && itNode->CmpAV("ref", MIBPId))
       {
        FItemSchList.push_back(itNode);
        if (itNode->CmpAV("def", "1"))
         FDefSch = itNode;
       }
      itNode = itNode->GetNext();
     }

    if (FDefSch)//����� �� ��������� �������
         SchLine = FDefSch->GetChildByAV("line", "name", AVacType.Trim());
    if (SchLine)//������ � ����� ���������� �������
         RC = SchLine;
    else
     {
      for (itSch = FItemSchList.begin(); (itSch != FItemSchList.end()) && !SchLine; itSch++)
       SchLine = (*itSch)->GetChildByAV("line", "name", AVacType.Trim());

      if (SchLine)//������ � ����� ���������� �������
           RC = SchLine;
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------

//����������� ������ �����, ��������������� ��������� ����������� �������� �
//��������� ����������, ���� ������ ����� �� �����������
//��� ��������� ����� �� ����������

TTagNode * __fastcall TPrePlaner::GetBegSchemeLine(TPlanItemKind PlanItemKind, int InfekId, bool fRoundMode)
 {
  TTagNode * RC = NULL;
  try
   {
    int ErrCode = -1;

    UnicodeString SchFN = (PlanItemKind == TPlanItemKind::pkVakchina) ? ((fRoundMode) ? "tvac" : "vac") : ((fRoundMode) ? "ttest" : "test");

    TTagNode * FSchDefNode = FUnitPlanSetting->GetChildByAV("spr", "infref", IntToStr(InfekId), true);
    TTagNode * FSch = NULL;

    if (FSchDefNode)
     {
      if (!FSchDefNode->CmpAV(SchFN, csReqFldNULL) && FSchDefNode->AV[SchFN].Length())
       {
        FSch = FXML->GetTagByUID(_UID(FSchDefNode->AV[SchFN]));
       }
     }
    RC = GetSchLineByLastItem(PlanItemKind, InfekId, fRoundMode, FSch);
    if (!RC)
     {
      ErrCode = ERR_NO_BEG_SCHEME;
     }
    /*
     if (FSchDefNode )
     {
     ErrCode = ERR_NO_BEG_SCHEME;
     }
     else
     {
     if ( FSchDefNode->CmpAV(SchFN, csReqFldNULL))
     {
     #ifdef THROW_IF_VAK_START_SCH_WAS_NOT_SET
     if ( PlanItemKind == TPlanItemKind::pkVakchina )
     ErrCode = ERR_NO_BEG_SCHEME;
     #endif
     #ifdef THROW_IF_PROB_START_SCH_WAS_NOT_SET
     if ( PlanItemKind == TPlanItemKind::pkTest )
     ErrCode = ERR_NO_BEG_SCHEME;
     #endif
     }
     else
     {
     UnicodeString BegSchemeUID = _UID(FSchDefNode->AV[SchFN]);
     RC = FXML->GetTagByUID(BegSchemeUID);
     if ( !RC )
     ErrCode = ERR_BAD_BEG_SCHEME;
     else if (!RC->CmpName("begline,line,endline"))
     ErrCode = ERR_BAD_BEG_SCHEME;
     }
     }
     */

    if (ErrCode != -1)
     {
      UnicodeString InfekName, BegSchemeInfo, ErrMsg;
      InfekName     = FDM->InfName[IntToStr(InfekId)];
      BegSchemeInfo = UnicodeString((PlanItemKind == TPlanItemKind::pkVakchina) ? "����������" : "����") + " ��� �������� \"" + InfekName + "\"";
      switch (ErrCode)
       {
       case ERR_NO_BEG_SCHEME:
        ErrMsg = (ASTR)"����������� ��������� ����� " + BegSchemeInfo + " ��� �������� \"" + FPtFIO + "\".";
        break;
       case ERR_BAD_BEG_SCHEME:
        ErrMsg = (ASTR)"��� �������� \"" + FPtFIO + "\" ������� �������������� ��������� ����� " + BegSchemeInfo + ".";
        break;
       }
      CallLogErr(ErrMsg + "; ��� ������ - " + IntToStr(ErrCode) + ".", __FUNC__);
      throw DKCommonExceptionEx(ErrMsg, ErrCode, NULL);
     }
    //#################################################################
    //#      DebugLog                                                 #
    //#################################################################
    FDebugLog->StartSch(RC); //#
    //#################################################################
   }
  __finally
   {
   }
  return RC;
 }

//---------------------------------------------------------------------------

//����������� ������ �����, ��������������� ����������� ��������

bool __fastcall TPrePlaner::GetCurSchemeLine(
    TPlanItemKind APlanItemKind,
    int AInfekId,
    bool ALastPlanItemExists,
    TDate ALastPlanItemDate,
    TDate APlanItemDate,
    TTagNode * ABegSchemeLine,
    TTagNode *& ACurSchemeLine,
    TDate & ASrokPlanDate,
    TTagNode *& ASrokPlanSchemeLine
    )
 {
  bool Result = false;
  ACurSchemeLine      = ABegSchemeLine;
  ASrokPlanDate       = TDateTime(0);
  ASrokPlanSchemeLine = NULL;

  FJumpTrace->TraceKind = APlanItemKind;
  FJumpTrace->Clear();
  FJumpTrace->Append(ABegSchemeLine, jkUnknown, "��������� ������ �����");

  bool bGetNext = true;

  //��������� �������� �� �����
  if (ALastPlanItemExists)
   {
    TTagNode * SrokJump = ABegSchemeLine->GetChildByName("srokjump");
    if (SrokJump)
     {
      TTagNode * SrokRef = SrokJump->GetChildByAV("refer", "infref", IntToStr(AInfekId));
      if (SrokRef)
       {
        ALastPlanItemDate = IncDate(ALastPlanItemDate, SrokJump->AV["actualtime"]);

        if (CompareDate(APlanItemDate, ALastPlanItemDate) == GreaterThanValue)
         {//���� ���������� ������ ��� ���������+���� ������������
          if (SrokRef->AV["ref"] == EmptyRef)
           {
            FJumpTrace->Append(ACurSchemeLine, jkUnknown, "������� �� ��������� ������������");
            ACurSchemeLine = NULL;
            bGetNext       = false;
           }
          else
           {
            TTagNode * NextSchemeLine = FXML->GetTagByUID(SrokRef->AV["ref"]);
            if (NextSchemeLine)
             {
              FJumpTrace->Append(NextSchemeLine, jkSrok, "����������� ����� ��������");
              ACurSchemeLine = NextSchemeLine;
              bGetNext       = false;
             }
           }
         }
        else
         {//���� ���������� ������ ��� ���������+���� ������������
          //�������  ��������� ����������� �������� � ����� ���������+���� ������������
          //������� ����� ����������� ���� �� ��������� ��������� �������
          if (SrokRef->AV["ref"] != EmptyRef)
           {
            TTagNode * NextSchemeLine = FXML->GetTagByUID(SrokRef->AV["ref"]);
            if (NextSchemeLine && !NextSchemeLine->CmpName("endline"))
             {
              FJumpTrace->Append(NextSchemeLine, jkSrok, "����������� ����� �������� (���)");
              ASrokPlanDate       = ALastPlanItemDate;
              ASrokPlanSchemeLine = NextSchemeLine;
             }
           }
         }
       }
     }
   }
  else
   FJumpTrace->Append(ABegSchemeLine, jkUnknown, "����������� ����������");

  //����� ��������� ������ �����
  if (bGetNext)
   {
    ACurSchemeLine = ABegSchemeLine->GetNext();
    if (ACurSchemeLine)
     FJumpTrace->Append(ACurSchemeLine, jkUnknown, "���������������� ������� � ���������� ������");
   }

  while (ACurSchemeLine)
   {
    bool bNextSchemeLineSet = false;
    TTagNode * CurJump = ACurSchemeLine->GetFirstChild();
    while (CurJump)//���� �� ��������� ����� �������� �� �����
     {
      if (!CurJump->CmpName("srokjump"))
       {
        TTagNode * CurRef, *NextSchemeLine;
        TJump FJump;
        CurRef = CurJump->GetChildByAV("refer", "infref", IntToStr(AInfekId), true);
        if (CurRef)
         {
          bool bCondOk = false;
          //�������� ������� �������� �� ��������
          if (CurJump->CmpName("agejump"))
           {
            bCondOk = (CompareDate(APlanItemDate, IncDate(FPtBirthDay, CurJump->AV["max_age"])) != LessThanValue);
            FJump   = TJump(jkAge, "�������� ������� ��������");
           }
          //�������� ������� �������� �� �������
          else if (CurJump->CmpName("jump"))
           {
            bCondOk = FDM->CondCheck(CurJump->GetTagByUID(CurJump->AV["ref"]), FPtId);
            FJump   = TJump(jkCond, "��������� ������� \"" + CurJump->GetTagByUID(CurJump->AV["ref"])->AV["name"] + "\"");
           }
          //��� ������������ ��������
          else if (CurJump->CmpName("endjump"))
           {
            bCondOk = true;
            FJump   = TJump(jkEnd, "����������� �������");
           }
          if (bCondOk)
           {
            if (CurRef->CmpAV("ref", EmptyRef))
             {
              FJumpTrace->Append(ACurSchemeLine, jkUnknown, "������� �� ��������� ������������");
              ACurSchemeLine     = NULL;
              bNextSchemeLineSet = true;
              break;
             }
            else
             {
              NextSchemeLine = FXML->GetTagByUID(CurRef->AV["ref"]);
              if (NextSchemeLine)
               {
                //����� ������ �����, ��������������� ������ ��������
                bool bIsSchemeLoop = FJumpTrace->FindByLineUID(NextSchemeLine->AV["uid"]);
                FJumpTrace->Append(NextSchemeLine, FJump.Kind, FJump.Reason);
                if (bIsSchemeLoop)
                 {
                  FJumpTrace->GetTraceStrings(FTempSL);
                  throw DKCommonExceptionEx("��������� ������������ �� ������� �������������� � ������� ����.", ERR_SCHEMES_LOOP, FTempSL);
                 }
                ACurSchemeLine = NextSchemeLine;
                bNextSchemeLineSet = true;
                break;
               }
             }
           }
         }
       }
      CurJump = CurJump->GetNext();
     }
    if (!bNextSchemeLineSet)
     {
      if (ACurSchemeLine->CmpName("endline"))
       ACurSchemeLine = NULL;
      else
       Result = true;
      break;
     }
   }

  //#################################################################
  //#      DebugLog                                                 #
  //#################################################################
  if (FDebugLog->Log)                             //#
   {                                              //#
    FJumpTrace->GetTraceStrings(FTempSL);         //#
    FDebugLog->CIL_Data(ACurSchemeLine, FTempSL); //#
   }                                              //#
  //#################################################################
  if (!ACurSchemeLine && ASrokPlanSchemeLine)
   {
    ACurSchemeLine = ASrokPlanSchemeLine;
    Result         = true;
   }
  return Result;
 }

//---------------------------------------------------------------------------

//����������� ���� ������������ ���������� ��������/�����

TDate __fastcall TPrePlaner::GetPlanItemPlanDate(int AInfId, bool ARoundMode, TPlanItemKind APlanItemKind, TDate APlanDate, bool bLPlanItemExists, TDate ALastPlanItemDate, TTagNode * ACurrLine)
 {
  TDate RC = APlanDate;
  try
   {
    if (ACurrLine->AV["min_pause"].Length())
     {//������� ���. ����� �� ���������� � ���� ����������
      UnicodeString FVacType = ACurrLine->AV["min_pause_vactype"].Trim();
      TDate tmpDate = ALastPlanItemDate;
      if (FVacType.Length())
       {
        TDateTime FVTCDate;
        if (FVacType.Pos(":"))
         {
          int idx = GetLPartB(FVacType, ':').ToIntDef(0);
          FVacType = GetRPartB(FVacType, ':');
          /*
           �� ��������� �������� / ����� / ��������
           �� ��������� ��������
           �� ��������� �����
           �� ��������� ��������
           �� ��������:
           �� �����:
           �� ��������:
           */
          switch (idx)
           {
           case 0: //�� ��������� �������� / ����� / ��������
             {
              GetLastPlanItemDate(AInfId, ARoundMode, TPlanItemKind::pkVakchina, FVTCDate);
              if (FVTCDate > tmpDate)
               tmpDate = FVTCDate;
              GetLastPlanItemDate(AInfId, ARoundMode, TPlanItemKind::pkTest, FVTCDate);
              if (FVTCDate > tmpDate)
               tmpDate = FVTCDate;
              GetLastPlanItemDate(AInfId, ARoundMode, TPlanItemKind::pkCheck, FVTCDate);
              if (FVTCDate > tmpDate)
               tmpDate = FVTCDate;
              break;
             }
           case 1: //�� ���������� ��������
             {
              if (APlanItemKind != TPlanItemKind::pkVakchina)
               if (GetLastPlanItemDate(AInfId, ARoundMode, TPlanItemKind::pkVakchina, FVTCDate))
                tmpDate = FVTCDate;
              break;
             }
           case 2: //�� ���������� �����
             {
              if (APlanItemKind != TPlanItemKind::pkTest)
               if (GetLastPlanItemDate(AInfId, ARoundMode, TPlanItemKind::pkTest, FVTCDate))
                tmpDate = FVTCDate;
              break;
             }
           case 3: //�� ���������� ��������
             {
              if (APlanItemKind != TPlanItemKind::pkCheck)
               if (GetLastPlanItemDate(AInfId, ARoundMode, TPlanItemKind::pkCheck, FVTCDate))
                tmpDate = FVTCDate;
              break;
             }
           case 4: //�� ��������:
             {
              if (FVacType.Length() && FGetVacDateByType(AInfId, ARoundMode, TPlanItemKind::pkVakchina, FVTCDate, FVacType))
               tmpDate = FVTCDate;
              break;
             }
           case 5: //�� �����:
             {
              if (FVacType.Length() && FGetVacDateByType(AInfId, ARoundMode, TPlanItemKind::pkTest, FVTCDate, FVacType))
               tmpDate = FVTCDate;
              break;
             }
           case 6: //�� ��������:
             {
              if (FVacType.Length() && FGetVacDateByType(AInfId, ARoundMode, TPlanItemKind::pkCheck, FVTCDate, FVacType))
               tmpDate = FVTCDate;
              break;
             }
           }
         }
        else
         {
          if (FGetVacDateByType(AInfId, ARoundMode, APlanItemKind, FVTCDate, FVacType))
           tmpDate = FVTCDate;
         }
       }
      tmpDate = IncDate(tmpDate, ACurrLine->AV["min_pause"]);
      if (tmpDate > RC)
       RC = tmpDate;
     }
    if (ACurrLine->AV["min_age"].Length())
     {//����� ���. �������
      TDate tmpDate = IncDate(FPtBirthDay, ACurrLine->AV["min_age"]);
      if (tmpDate > RC)
       RC = tmpDate;
     }
    if (ACurrLine->AV["season_beg"].Length())
     {//������ ����� ���������� 0-11
      int CM = RC.FormatString("mm").ToIntDef(0) - 1;
      int FSBeg = ACurrLine->AV["season_beg"].ToIntDef(0);
      int FSEnd = ACurrLine->AV["season_end"].ToIntDef(11);
      if (!((FSBeg <= CM) && (CM <= FSEnd)))
       {//�� ������ � ����� ����������
        if (CM < FSBeg)
         CM = FSBeg - CM;
        else
         CM = FSBeg - CM + 11;
        RC = IncDate(RC, "0/" + IntToStr(CM) + "/0");
       }
     }
   }
  __finally
   {
   }

  //#################################################################
  //#      DebugLog                                                 #
  //#################################################################
  FDebugLog->CIL_ItemPlanDate(RC); //#
  //#################################################################

  //������������� �� ���� ������ ��������� � ����������� �� �����������
  if (RC.DayOfWeek() == 1)
   RC = Dateutils::IncDay(RC, 1);
  return RC;
 }

//---------------------------------------------------------------------------
//�������� �� ������������� ������������ �������� �� ����������� �������������� �����
bool __fastcall TPrePlaner::CheckNeedForPrivivPlanByPrevProba(int nInfekId, TDate APrivivPlanDate)
 {
  return true;
 }
//---------------------------------------------------------------------------
bool __fastcall TPrePlaner::FindInfekPlanItemInPlan(TPlanItemKind PlanItemKind, int nInfekId, TPlanItemListIt * PlanItemListIt)
 {
  bool RC = false;
  TPlanItemList * PlanItemList = (PlanItemKind == TPlanItemKind::pkVakchina) ? &FPrivPlan : &FTestPlan;
  for (TPlanItemListIt i = PlanItemList->begin(); (i != PlanItemList->end()) && !RC; i++)
   if (((*i).InfekId == nInfekId) && ((*i).PlanItemKind == PlanItemKind))
    {
     RC = true;
     if (PlanItemListIt)
      (* PlanItemListIt) = i;
    }
  return RC;
 }
//---------------------------------------------------------------------------
int __fastcall TPrePlaner::DecodeSchemeLineIds(TPlanItemListIt itItem, TSchemeLineIds & AIds)
 {
  AIds.clear();
  TStringList * sl = new TStringList;
  try
   {
    sl->Text = StringReplace(itItem->SchemeLineUID, ";", "\n", TReplaceFlags() << rfReplaceAll);
    for (int i = 0; i < sl->Count; i++)
     AIds[_GUI(sl->Strings[i]).ToInt()] = _UID(sl->Strings[i]);
   }
  __finally
   {
    delete sl;
   }
  return AIds.size();
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::GetPlanInfList(TPlanItemKind APlanItemKind, TInfItemList & APlanInfList)
 {
  TTagNode * itSch = FUnitPlanSetting->GetChildByName("sch", true);
  TTagNode * FPersPlan = FDM->PlanOpt->GetChildByName("vacplan", true);
  TTagNode * FCommPlan = FDM->PlanOpt->GetChildByName("vacplan", true);
  TTagNode * itPersPlan, *itCommPlan;
  if (itSch)
   {
    itSch = itSch->GetFirstChild();
    while (itSch)
     {
      itPersPlan = FPersPlan->GetChildByAV("vpr", "infref", itSch->AV["infref"], true);
      itCommPlan = FCommPlan->GetChildByAV("vpr", "infref", itSch->AV["infref"], true);
      if (itCommPlan || itPersPlan)
       {
        if (itCommPlan)
         {
          if ((APlanItemKind == TPlanItemKind::pkVakchina) && itCommPlan->CmpAV("vac", "1"))
           APlanInfList.push_back(INF_ITEM(itSch->AV["infref"].ToIntDef(0), 0, itSch->AV["tvac"]));
          else if ((APlanItemKind == TPlanItemKind::pkTest) && itCommPlan->CmpAV("test", "1"))
           APlanInfList.push_back(INF_ITEM(itSch->AV["infref"].ToIntDef(0), 0, itSch->AV["ttest"]));
         }
        else
         {
          if ((APlanItemKind == TPlanItemKind::pkVakchina) && itPersPlan->CmpAV("vac", "1"))
           APlanInfList.push_back(INF_ITEM(itSch->AV["infref"].ToIntDef(0), 0, itSch->AV["tvac"]));
          else if ((APlanItemKind == TPlanItemKind::pkTest) && itPersPlan->CmpAV("test", "1"))
           APlanInfList.push_back(INF_ITEM(itSch->AV["infref"].ToIntDef(0), 0, itSch->AV["ttest"]));
         }
       }
      else
       {
        if ((APlanItemKind == TPlanItemKind::pkVakchina) && !(itSch->CmpAV("tvac", "") || itSch->CmpAV("tvac", "_NULL_")))
         APlanInfList.push_back(INF_ITEM(itSch->AV["infref"].ToIntDef(0), 0, itSch->AV["tvac"]));
        else if ((APlanItemKind == TPlanItemKind::pkTest) && !(itSch->CmpAV("ttest", "") || itSch->CmpAV("ttest", "_NULL_")))
         APlanInfList.push_back(INF_ITEM(itSch->AV["infref"].ToIntDef(0), 0, itSch->AV["ttest"]));
       }
      itSch = itSch->GetNext();
     }
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TPrePlaner::GetLastPlanItemDate(int AInfId, bool ARoundMode, TPlanItemKind APlanItemKind, TDate & ADate)
 {
  bool RC = false;
  ADate = TDate(0);
  try
   {
    UnicodeString FKey;
    TItemDateMap::iterator FRC;
    TPlanItemDateList::iterator FIRC;
    UnicodeString FSQL = "";
    if (APlanItemKind == TPlanItemKind::pkVakchina)
     {
      if (ARoundMode)
       {
        FIRC = FVacDateByTurInf.find(AInfId);
        if (FIRC != FVacDateByTurInf.end())
         {
          ADate = FIRC->second;
          RC    = true;
         }
       }
      else
       {
        FIRC = FVacDateByInf.find(AInfId);
        if (FIRC != FVacDateByInf.end())
         {
          ADate = FIRC->second;
          RC    = true;
         }
       }
     }
    else if (APlanItemKind == TPlanItemKind::pkTest)
     {
      FIRC = FTestDateByInf.find(AInfId);
      if (FIRC != FTestDateByInf.end())
       {
        ADate = FIRC->second;
        RC    = true;
       }
     }
    else if (APlanItemKind == TPlanItemKind::pkCheck)
     {
      FIRC = FCheckDateByInf.find(AInfId);
      if (FIRC != FCheckDateByInf.end())
       {
        ADate = FIRC->second;
        RC    = true;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
TDate __fastcall TPrePlaner::GetPlanItemEarliestDate(int AInfId, TPlanItemKind APlanItemKind)
 {
  TDate RC;
  TPlanItemDateList::iterator FIRC;
  try
   {
    UnicodeString pInfId = IntToStr(AInfId);

    TPlanItemDateList::iterator FRC;
    if (APlanItemKind == TPlanItemKind::pkVakchina)
     {
      FRC = FVacOtvodDateByInf.find(AInfId);
      if (FRC != FVacOtvodDateByInf.end())
       RC = Dateutils::IncDay(FRC->second, 1);
      else
       RC = FBeginDate;
     }
    else if (APlanItemKind == TPlanItemKind::pkTest)
     {
      //CallLogMsg("����� ������ �� �������� "+IntToStr(AInfId), __FUNC__, 5);
      FRC = FTestOtvodDateByInf.find(AInfId);
      if (FRC != FTestOtvodDateByInf.end())
       {
        //CallLogMsg("������ �� �������� ������, ���� ��������� "+DateToStr(FRC->second), __FUNC__, 5);
        RC = Dateutils::IncDay(FRC->second, 1);
       }
      else
       RC = FBeginDate;
     }
    else
     RC = FBeginDate;

    TDate tmpDate;
    int tmpPause;
    //��� ��������� ������ ����������� �������� ��� ��������
    if (APlanItemKind == TPlanItemKind::pkVakchina)
     {//���� �� k ��� ���� ����� ���������� �� k - � � i - � ��������(��.������������)
      for (TAnsiStrMap::iterator itInf = FDM->InfList.begin(); itInf != FDM->InfList.end(); itInf++)
       {//select CODE from CLASS_003A into :itInf do
        if (pInfId != itInf->first)
         {
          FIRC = FVacDateByInfComm.find(itInf->first.ToIntDef(0));
          if (FIRC != FVacDateByInfComm.end())
           {
            tmpDate = FIRC->second;
            if (GetInfInfPause(tmpPause, itInf->first, pInfId))
             {
              tmpDate = Dateutils::IncDay(tmpDate, tmpPause);
              if (tmpDate > RC)
               RC = tmpDate;
             }
           }
         }
       }

      //���� �� k ��� ���� ����� ������ � ��������� �� i - � ��������(��.������������)
      for (TAnsiStrMap::iterator itTest = FDM->TestList.begin(); itTest != FDM->TestList.end(); itTest++)
       {//select CODE from CLASS_003A into :itInf do
        FIRC = FTestDateByTest.find(itTest->first.ToIntDef(0));
        if (FIRC != FTestDateByTest.end())
         {
          tmpDate = FIRC->second;
          if (GetTestInfPause(tmpPause, itTest->first, pInfId))
           {
            tmpDate = Dateutils::IncDay(tmpDate, tmpPause);
            if (tmpDate > RC)
             RC = tmpDate;
           }
         }
       }
     }
    //��� ��������� ������ ����������� �������� ��� ����
    else
     {
      //���� �� k ��� ���� ����� ������� �� k - � � i - � ��������(��.������������)
      for (TAnsiStrMap::iterator itInf = FDM->InfList.begin(); itInf != FDM->InfList.end(); itInf++)
       {//select CODE from CLASS_003A into :itInf do
        if (pInfId != itInf->first)
         {
          FIRC = FTestDateByInf.find(itInf->first.ToIntDef(0));
          if (FIRC != FTestDateByInf.end())
           {
            tmpDate = FIRC->second;
            if (GetTestTestPause(tmpPause, itInf->first, pInfId))
             {
              tmpDate = Dateutils::IncDay(tmpDate, tmpPause);
              if (tmpDate > RC)
               RC = tmpDate;
             }
           }
         }
       }
      //���� �� k ��� ���� ����� ��������� �� k - � �������� � ������ �� i - � ��������(��.������������)
      for (TAnsiStrMap::iterator itInf = FDM->InfList.begin(); itInf != FDM->InfList.end(); itInf++)
       {//select CODE from CLASS_003A into :itInf do
        if (pInfId != itInf->first)
         {
          FIRC = FVacDateByInfComm.find(itInf->first.ToIntDef(0));
          if (FIRC != FVacDateByInfComm.end())
           {
            tmpDate = FIRC->second;
            if (GetInfTestPause(tmpPause, itInf->first, pInfId))
             {
              tmpDate = Dateutils::IncDay(tmpDate, tmpPause);
              if (tmpDate > RC)
               RC = tmpDate;
             }
           }
         }
       }
     }
   }
  __finally
   {
    if (RC < FBeginDate)
     RC = FBeginDate;
   }
  return RC;
 }
//---------------------------------------------------------------------------

//���� �� ��������� ��� ������������ ��������/�����

int __fastcall TPrePlaner::PlanItemPlan(TPlanItemKind PlanItemKind)
 {
  int nResult = 0;
  int tmpMIBPCode;

  if (PlanItemKind == TPlanItemKind::pkVakchina)
   FXML = FVacSchXML;
  else if (PlanItemKind == TPlanItemKind::pkTest)
   FXML = FTestSchXML;
  else if (PlanItemKind == TPlanItemKind::pkCheck)
   FXML = FCheckSchXML;

  TInfItemList FPlanInfList;
  GetPlanInfList(PlanItemKind, FPlanInfList);

  //########################################################################################
  //#      DebugLog                                                                        #
  //########################################################################################
  if (FDebugLog->Log)                       //#
   {                                        //#
    FDebugLog->ItemListBegin(PlanItemKind); //#
    for (TInfItemList::iterator i = FPlanInfList.begin(); i != FPlanInfList.end(); i++)//#
         FDebugLog->SetItemList(i->Id, i->Type); //#
    FDebugLog->ItemListEnd();                    //#
   }                                             //#
  //########################################################################################

  for (TInfItemList::iterator inf = FPlanInfList.begin(); inf != FPlanInfList.end(); inf++)
   {
    int InfekId = inf->Id;
    int InfekType = inf->Type;

    //RoundSchemeExists(PlanItemKind, InfekId);
    //fRoundMode = true ��� ������� ������� �����  (fRoundMode - ����� ������������ "�� �����" ��� "�� ����")
    bool fRoundMode = !(!inf->TurSch.Length() || (inf->TurSch.LowerCase() == "_null_"));
    bool fDoPlan = true;
    TDate FPlanDate = GetPlanItemEarliestDate(InfekId, PlanItemKind);
    while (fDoPlan)
     {
      fDoPlan = false;

      TDate LPlanItemDate;
      bool LPlanItemExists = GetLastPlanItemDate(InfekId, fRoundMode, PlanItemKind, LPlanItemDate);

      //#######################################################################################
      //#      DebugLog                                                                       #
      //#######################################################################################
      FDebugLog->SetCIL(PlanItemKind, inf->Id, fRoundMode, LPlanItemExists, LPlanItemDate); //#
      //#######################################################################################

      bool bCurSchemeLineExists = false;
      TTagNode * BegSchemeLine;
      TTagNode * CurSchemeLine;

      TTagNode * SrokPlanSchemeLine = NULL;
      TDateTime SrokPlanDate = TDateTime(0);
      try
       {
        //����������� ������ �����, ��������������� ��������� ����������� ��������/�����
        BegSchemeLine = GetBegSchemeLine(PlanItemKind, InfekId, fRoundMode);
        //����������� ������ ����� ��� ����������� ��������/�����
        if (BegSchemeLine)
         bCurSchemeLineExists = GetCurSchemeLine(PlanItemKind, InfekId, LPlanItemExists, LPlanItemDate, FPlanDate, BegSchemeLine, CurSchemeLine, SrokPlanDate, SrokPlanSchemeLine);
        if (CurSchemeLine == SrokPlanSchemeLine)
         FPlanDate = SrokPlanDate;
       }
      catch (DKCommonExceptionEx & E)
       {
        //######################################################################
        //#      DebugLog                                                      #
        //######################################################################
        FDebugLog->CIL_ExceptSch(E.Id == ERR_SCHEMES_LOOP); //#
        //######################################################################
        throw;
       }

      if (!bCurSchemeLineExists)
       {
        if (fRoundMode && !InfekType)
         {
          //����������� � ���� �� ����
          fRoundMode = false;
          fDoPlan    = true;
          //����� ������� �����
          TTagNode * itSch = FUnitPlanSetting->GetChildByAV("spr", "infref", IntToStr(InfekId), true);
          if (itSch)
           {
            if (PlanItemKind == TPlanItemKind::pkVakchina)
             itSch->AV["tvac"] = csReqFldNULL;
            else
             itSch->AV["ttest"] = csReqFldNULL;
           }
          //####################################################################
          //#      DebugLog                                                    #
          //####################################################################
          FDebugLog->CIL_RetToPlan(); //#
          //####################################################################
         }
       }
      else
       {
        //����������� ���� ������������ ���������� ��������/�����

        FPlanDate = GetPlanItemPlanDate(InfekId, fRoundMode, PlanItemKind, FPlanDate, LPlanItemExists, LPlanItemDate, CurSchemeLine);
        if (SrokPlanSchemeLine && (CurSchemeLine != SrokPlanSchemeLine))
         {//���� ������� �� ����� � ������� ������ �� ��������� �� ������� �������� �� �����
          SrokPlanDate = GetPlanItemPlanDate(InfekId, fRoundMode, PlanItemKind, SrokPlanDate, LPlanItemExists, LPlanItemDate, SrokPlanSchemeLine);
          if (CompareDate(FPlanDate, SrokPlanDate) == GreaterThanValue)
           {//������� �� ����� ���������� ������
            FPlanDate     = SrokPlanDate;
            CurSchemeLine = SrokPlanSchemeLine;
           }
         }

        bool bIsOk;
        if (PlanItemKind == TPlanItemKind::pkVakchina)
         bIsOk = CheckNeedForPrivivPlanByPrevProba(InfekId, FPlanDate);
        else
         bIsOk = true;
        //���� �� �������� �� ������ �������� � �����
        if (!FindInfekPlanItemInPlan(PlanItemKind, InfekId))
         {
          //���������� ��������� ��� ��������� � ���� � ������ ����������� ��������/����
          PLAN_ITEM PlanItem;
          PlanItem.InfekId      = InfekId;
          PlanItem.PlanItemKind = PlanItemKind;
          tmpMIBPCode           = CurSchemeLine->GetParent("schema")->AV["ref"].ToIntDef(-1);
          PlanItem.OwnerId      = tmpMIBPCode + 10000 * CurSchemeLine->AV["link"].ToIntDef(0);

          if (PlanItemKind == TPlanItemKind::pkVakchina)
           PlanItem.OwnerName = FDM->VacName[IntToStr(tmpMIBPCode)];
          else
           PlanItem.OwnerName = FDM->TestName[IntToStr(tmpMIBPCode)];

          PlanItem.PlanDate       = FPlanDate;
          PlanItem.PlanMonth      = TDate("01." + PlanItem.PlanDate.FormatString("mm.yyyy"));
          PlanItem.PlanDayByMonth = FPlanDate;
          PlanItem.SchemeLineUID  = IntToStr(InfekId) + "." + CurSchemeLine->AV["uid"];
          PlanItem.Type_Prep      = CurSchemeLine->AV["name"];
          PlanItem.Source         = (fRoundMode) ? pisPlanerRnd : pisPlanerClndr;

          if (PlanItemKind == TPlanItemKind::pkVakchina)
           {
            int VPriority, RVPriority;
            VPriority  = FDM->VPrior[InfekId];
            RVPriority = FDM->RVPrior[InfekId];

            if (PlanItem.Type_Prep[1] == 'V')
             {
              PlanItem.Priority      = (PlanItem.Type_Prep[2] == '1') ? VPriority : nMaxVPriority;
              PlanItem.V_RV_Priority = VPriority;
             }
            else
             {
              UnicodeString MinorRVInd = PlanItem.Type_Prep.SubString(PlanItem.Type_Prep.LastDelimiter("/") + 1, 1);
              PlanItem.Priority      = (MinorRVInd[1] == '1') ? RVPriority : nMaxRVPriority;
              PlanItem.V_RV_Priority = RVPriority;
             }
           }
          else
           PlanItem.Priority = -1;
          if (PlanItemKind == TPlanItemKind::pkVakchina)
           FPrivPlan.push_back(PlanItem);
          else
           FTestPlan.push_back(PlanItem);
          nResult++ ;
         }
       }
     }
    //FDM->fdstPlanInfek->Next();
   }
  return nResult;
 }

//---------------------------------------------------------------------------
void __fastcall TPrePlaner::CheckByConstCancel()
 {
  TPlanItemListIt k;
  TPlanItemDateList::iterator FRC;
  if (FConstVacOtvodDateByInf.size())
   {
    k = FPrivPlan.begin();
    while (k != FPrivPlan.end())
     {
      FRC = FConstVacOtvodDateByInf.find((*k).InfekId);
      if (FRC != FConstVacOtvodDateByInf.end())
       {
        if ((*k).PlanDate >= FRC->second)
         k = FPrivPlan.erase(k);
        else
         k++ ;
       }
      else
       k++ ;
     }
   }
  if (FConstTestOtvodDateByInf.size())
   {
    k = FTestPlan.begin();
    while (k != FTestPlan.end())
     {
      FRC = FConstTestOtvodDateByInf.find((*k).InfekId);
      if (FRC != FConstTestOtvodDateByInf.end())
       {
        if ((*k).PlanDate >= FRC->second)
         k = FTestPlan.erase(k);
        else
         k++ ;
       }
      else
       k++ ;
     }
   }
 }
//---------------------------------------------------------------------------

// ***************************************************************************
//������������� ��� ������������ ���������� �������� � ������������ �
//������� ����� ������� � ����������
// ***************************************************************************

//---------------------------------------------------------------------------
//��������� � ���� ���������� (��������������� ����)
void __fastcall TPrePlaner::AppendPrepareProb()
 {
  //############################################################################
  //#      DebugLog                                                            #
  //############################################################################
  FDebugLog->DebugPrintListPlans(5); //"����� ���������� ����������"          #
  //############################################################################
  try
   {
    TPreTestItemList::iterator itPreTest;
    int iPause, nInfekId;
    for (itPreTest = FDM->PreTestList.begin(); itPreTest != FDM->PreTestList.end(); itPreTest++)
     {//���� �� ����/���������������� ������
      if (itPreTest->Kind != TPrePlanKind::None)
       {
        //<choicevalue name='�� �����������' value='0'/>
        //<choicevalue name='����������� ���������������� �����' value='1'/>
        //<choicevalue name='����������� ���������������� �����' value='2'/>

        TPlanItemListIt PlanVacItem;
        iPause   = itPreTest->Pause;
        nInfekId = itPreTest->Inf;

        if (FindInfekPlanItemInPlan(TPlanItemKind::pkVakchina, nInfekId, &PlanVacItem))
         {
          bool FCont = true;
          if (itPreTest->MinAge.Length())
           FCont = (CompareDate(PlanVacItem->PlanDate, IncDate(FPtBirthDay, itPreTest->MinAge)) != LessThanValue);
          if (itPreTest->MaxAge.Length())
           FCont = (CompareDate(PlanVacItem->PlanDate, IncDate(FPtBirthDay, itPreTest->MaxAge)) == LessThanValue);

          if (FCont)
           {
            dsPlanLogMessage("���������� ����/���������������� �����: " + FDM->TestName[IntToStr(itPreTest->Test)], __FUNC__, 5);
            PLAN_ITEM PreTest;
            PreTest.PlanItemKind  = TPlanItemKind::pkTest;
            PreTest.InfekId       = nInfekId;
            PreTest.OwnerId       = itPreTest->Test;
            PreTest.OwnerName     = FDM->TestName[IntToStr(itPreTest->Test)];
            PreTest.SchemeLineUID = "";
            PreTest.Type_Prep     = "D";
            if (PlanVacItem->Source == pisPlanerRnd)
             PreTest.Source = pisPlanerRndPrev;
            else
             PreTest.Source = pisPlanerPrev;

            TDate FSavePlanMonth = TDate(0);
            TDate tmpDate = Dateutils::IncDay(PlanVacItem->PlanDate, iPause); //iPause+1
            if (MonthOf(PlanVacItem->PlanDate) != MonthOf(tmpDate))
             {//���� ���������� �������� ������� �� ������� ������ ��-�� ����
              //��������� �������� � ����� �� ������ ���������� ������
              //PlanVacItem->PlanDate = Dateutils::IncDay(PlanVacItem->PlanDate, iPause+5);
              FSavePlanMonth        = PlanVacItem->PlanMonth;
              PlanVacItem->PlanDate = TDate("01." + System::Sysutils::IncMonth(PlanVacItem->PlanDate).FormatString("mm.yyyy"));
              //PlanVacItem->PlanMonth = TDate("01."+PlanVacItem->PlanDate.FormatString("mm.yyyy"));
              PlanVacItem->PlanDayByMonth = PlanVacItem->PlanDate;
              dsPlanLogMessage("���� ���������� �������� ������� �� ������� ������ [" + FSavePlanMonth.FormatString("mmmm yyyy") + "] ��-�� ����, ��������� �������� �� ������ ���������� ������",
                  __FUNC__, 5);
             }
            if (itPreTest->Kind == TPrePlanKind::PreVac)
             {//����������������
              PreTest.PlanDate  = PlanVacItem->PlanDate;
              PreTest.PlanMonth = PlanVacItem->PlanMonth; //TDate("01."+PlanItem.PlanDate.FormatString("mm.yyyy"));
              dsPlanLogMessage("����������������: " + PreTest.PlanMonth.FormatString("mmmm yyyy"), __FUNC__, 5);
              //�������� �������� �� ����.�����+�����
              PlanVacItem->PlanDate = Dateutils::IncDay(PreTest.PlanDate, iPause);
              //PlanVacItem->PlanMonth = TDate("01."+PlanVacItem->PlanDate.FormatString("mm.yyyy"));
              PlanVacItem->PlanDayByMonth = PlanVacItem->PlanDate;
             }
            else
             {//����������������
              PreTest.PlanDate  = Dateutils::IncDay(PlanVacItem->PlanDate, iPause);
              PreTest.PlanMonth = PlanVacItem->PlanMonth; //TDate("01."+PlanItem.PlanDate.FormatString("mm.yyyy"));
              dsPlanLogMessage("����������������: " + PreTest.PlanMonth.FormatString("mmmm yyyy"), __FUNC__, 5);
             }
            PreTest.PlanDayByMonth = PlanVacItem->PlanDate;
            //���������� �� ����� ����� � ����� itPreTest->Test
            //��������� ����� � ������� ���� �� � � ������� �������� ���������
            TPlanItemListIt k;
            TDate itDate = PreTest.PlanMonth;
            while ((int)itDate)
             {
              k = FPrivPlan.begin();
              while (k != FPrivPlan.end())
               {
                if (((*k).OwnerId == PreTest.OwnerId) &&
                    ((*k).PlanItemKind == TPlanItemKind::pkTest) &&
                    ((*k).PlanMonth == itDate))
                 {
                  dsPlanLogMessage("� ������ �������� ����� �����: " + k->PlanMonth.FormatString("mmmm yyyy") + "  " + itDate.FormatString("mmmm yyyy"), __FUNC__, 5);
                  k = FPrivPlan.erase(k);
                 }
                else
                 k++ ;
               }
              k = FTestPlan.begin();
              while (k != FTestPlan.end())
               {
                if (((*k).OwnerId == PreTest.OwnerId) &&
                    ((*k).PlanItemKind == TPlanItemKind::pkTest) &&
                    ((*k).PlanMonth == itDate))
                 {
                  dsPlanLogMessage("� ������ ���� ����� �����: " + k->PlanMonth.FormatString("mmmm yyyy") + "  " + itDate.FormatString("mmmm yyyy"), __FUNC__, 5);
                  k = FTestPlan.erase(k);
                 }
                else
                 k++ ;
               }
              k = FDopTestPlan.begin();
              while (k != FDopTestPlan.end())
               {
                if ((*k).OwnerId == PreTest.OwnerId)
                 {
                  dsPlanLogMessage("� ������ ���� ����� �����: " + k->PlanMonth.FormatString("mmmm yyyy") + "  " + itDate.FormatString("mmmm yyyy"), __FUNC__, 5);
                  k = FDopTestPlan.erase(k);
                 }
                else
                 k++ ;
               }
              itDate = FSavePlanMonth;
              FSavePlanMonth = TDate(0);
             }
            FDopTestPlan.push_back(PreTest);
           }
         }
       }
     }
    //############################################################################
    //#      DebugLog                                                            #
    //############################################################################
    FDebugLog->DebugPrintListPlans(6); //"����� ���������� ����������"          #
    //############################################################################
   }
  __finally
   {
   }
 }

//---------------------------------------------------------------------------

//��������� ��������������� ����� � ��
void __fastcall TPrePlaner::ClearPlan()
 {
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    FDM->quExecParam(FQ, true, "delete from class_1112 where R1113 = :pR1113", "pR1113", FPtId);
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
   }
 }

//---------------------------------------------------------------------------
void __fastcall TPrePlaner::ApplyPlan(TPlanItemKind PlanItemKind, bool IsMain)
 {
  try
   {
    if (FDM->OnPlanInsertData)
     {
      TPlanItemList * PlanItemList;

      if (PlanItemKind == TPlanItemKind::pkVakchina)
       PlanItemList = (IsMain) ? &FPrivPlan : &FDopPrivivPlan;
      else
       PlanItemList = (IsMain) ? &FTestPlan : &FDopTestPlan;
      UnicodeString FRecId = "";
      bool FPlanSaved = true;
      for (TPlanItemListIt i = PlanItemList->begin(); i != PlanItemList->end(); i++)
       {
        try
         {
          FPlanSaved &= SameText(FDM->OnPlanInsertData("1112", FGetValues(*i), FRecId), "ok");
         }
        catch (System::Sysutils::Exception & E)
         {
          CallLogErr(E.Message, __FUNC__);
         }
       }
      if (FPlanSaved)
       {
        PlanItemList->sort(PlanItemListLessPlanDate());
        TDate FPlDate = PlanItemList->front().PlanDate;
        TFDQuery * FQ = FDM->CreateTempQuery();
        try
         {
          FDM->quExecParam(FQ, false, "Select r3195 from class_1000 where code=:pPtId", "pPtId", FPtId);
          if (FQ->FieldByName("r3195")->AsDateTime > FPlDate)
           FDM->quExecParam(FQ, true, "Update class_1000 set r3195=:pR3195 where code=:pPtId", "pR3195", FPlDate, "���������� ���� ���������� �����", "pPtId", FPtId);
         }
        __finally
         {
          FDM->DeleteTempQuery(FQ);
         }
       }
     }
   }
  __finally
   {
   }
 }
//---------------------------------------------------------------------------
TJSONObject * __fastcall TPrePlaner::FGetValues(PLAN_ITEM AItem)
 {
  TJSONObject * RC = new TJSONObject;
  try
   {
    RC->AddPair("32DC", icsNewGUID().UpperCase()); //<extedit name='cguid' uid='32DC' required='1' depend='32DC.32DC' isedit='0' fl='cguid' length='40'/>
    RC->AddPair("1113", FPtId);                    //<extedit name='��� ��������' uid='1113' ref='1000' depend='1113.1113' isedit='0' fieldtype='integer'/>

    RC->AddPair("3193", FDM->GetBrigExec(((int)AItem.PlanItemKind) + 1, AItem.InfekId)); //<binary name='��������� ����������' uid='3193'/>

    RC->AddPair("1115", AItem.PlanDate);                //<date name='���� ���������������� �����' uid='1115' inlist='l'/>
    RC->AddPair("31A2", AItem.PlanMonth);               //<date name='���� ���������������� �����' uid='1115' inlist='l'/>
    RC->AddPair("31A4", AItem.PlanDayByMonth);          //<date name='���� ���������������� �����' uid='1115' inlist='l'/>
    RC->AddPair("1116", ((int)AItem.PlanItemKind) + 1); //<choice name='���' uid='1116' default='1'>  �������� - 1,  ����� - 2,  �������� - 3
    RC->AddPair("1117", AItem.Type_Prep);               //<text name='���/��������' uid='1117' inlist='l' cast='no' length='10'/>
    RC->AddPair("3187", AItem.InfekId);                 //<choiceDB name='��������' uid='3187' ref='003A' inlist='l'/>
    RC->AddPair("1118", AItem.OwnerId);                 //<extedit name='����' uid='1118' ref='0035' inlist='l' fieldtype='integer'/>
    RC->AddPair("1119", AItem.Priority);                //<digit name='���������' uid='1119' inlist='l' digits='4' min='-999' max='9999'/>

    //RC->AddPair("111A", AItem.Priority);              // <binary name='� �����' uid='111A'/>
    RC->AddPair("111B", new TJSONNull); //<date name='���� �����' uid='111B'/>

    RC->AddPair("111C", AItem.Source); //<choice name='��������' uid='111C' inlist='l' default='0'>
    //<choicevalue name='����������' value='0'/>
    //<choicevalue name='������ ����������' value='1'/>
    //<choicevalue name='����������� �� ���������' value='2'/>
    //<choicevalue name='����������� �� �� ���������' value='3'/>
    //<choicevalue name='����������� ��� ���������������' value='4'/>
    //<choicevalue name='����������� �� ����' value='5'/>
    //<choicevalue name='����������� �� ���� ��� ���������������' value='6'/>

    RC->AddPair("31A3", FPtBirthDay); //<choice name='��������' uid='111C' inlist='l' default='0'>
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::GetUnitCardData()
 {
  FCardPrivList.clear();
  FCardTestList.clear();
  FCardCheckList.clear();
  FCardCancelList.clear();

  FVacDateByInfComm.clear();
  FVacDateByInf.clear();
  FVacDateByTurInf.clear();
  FTestDateByTest.clear();
  FTestDateByInf.clear();
  FCheckDateByInf.clear();

  FVacOtvodDateByInf.clear();
  FTestOtvodDateByInf.clear();
  FConstVacOtvodDateByInf.clear();
  FConstTestOtvodDateByInf.clear();

  TPlanItemDateList::iterator FRC;
  UnicodeString FSQL;
  //FSQL = "select max(R1031) as RETDATE from CLASS_102F where r1200 = :pInfekId and R017B = :pPtId";

  TCardItem itCard;
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    //��������� ��������
    FSQL = "select * from CLASS_1003 where R017A = :pPtId";
    if (FDM->quExecParam(FQ, false, FSQL, "pPtId", FPtId))
     {
      while (!FQ->Eof)
       {
        itCard.Kind    = TCardItemKind::Vac;
        itCard.Id      = FQ->FieldByName("R1020")->AsInteger;
        itCard.Inf     = FQ->FieldByName("R1075")->AsInteger;
        itCard.Tur     = FQ->FieldByName("R10AE")->AsInteger;
        itCard.VacType = FQ->FieldByName("R1021")->AsString.UpperCase();
        itCard.Date    = FQ->FieldByName("R1024")->AsDateTime;
        itCard.Sch     = FQ->FieldByName("R1092")->AsString.UpperCase();
        FCardPrivList.push_back(itCard);
        FQ->Next();
       }
     }
    //��������� �����
    FSQL = "select * from CLASS_102F where R017B = :pPtId";
    if (FDM->quExecParam(FQ, false, FSQL, "pPtId", FPtId))
     {
      while (!FQ->Eof)
       {
        itCard.Kind = TCardItemKind::Test;
        itCard.Id   = FQ->FieldByName("R1032")->AsInteger;
        itCard.Inf  = FQ->FieldByName("R1200")->AsInteger;
        itCard.Date = FQ->FieldByName("R1031")->AsDateTime;
        itCard.Sch  = FQ->FieldByName("R1093")->AsString.UpperCase();
        FCardTestList.push_back(itCard);
        FQ->Next();
       }
     }
    //��������� ��������
    FSQL = "select * from CLASS_1100 where R1101 = :pPtId";
    if (FDM->quExecParam(FQ, false, FSQL, "pPtId", FPtId))
     {
      while (!FQ->Eof)
       {
        itCard.Kind = TCardItemKind::Check;
        itCard.Id   = FQ->FieldByName("R1103")->AsInteger;
        itCard.Date = FQ->FieldByName("R1104")->AsDateTime;
        itCard.Sch  = FQ->FieldByName("R1107")->AsString;
        FCardCheckList.push_back(itCard);
        FQ->Next();
       }
     }
    //��������� ������
    FSQL = "select * from CLASS_1030 where R017C = :pPtId";
    if (FDM->quExecParam(FQ, false, FSQL, "pPtId", FPtId))
     {
      while (!FQ->Eof)
       {
        //CallLogMsg("��������� ������ ������� ��� �������� "+IntToStr(FPtId), __FUNC__, 5);
        itCard.Kind    = TCardItemKind::Cancel;
        itCard.Id      = FQ->FieldByName("R1066")->AsInteger;
        itCard.Inf     = FQ->FieldByName("R1083")->AsInteger;
        itCard.Tur     = FQ->FieldByName("R1043")->AsInteger;
        itCard.Date    = FQ->FieldByName("R105F")->AsDateTime;
        itCard.EndDate = FQ->FieldByName("R1065")->AsDateTime;
        itCard.Vac     = FQ->FieldByName("R1110")->AsInteger;
        itCard.Test    = FQ->FieldByName("R1111")->AsInteger;
        FCardCancelList.push_back(itCard);
        FQ->Next();
       }
     }
    TCardItemList::iterator itCard;
    //#########################################################################
    //#                                                                       #
    //#  ��������                                                             #
    //#                                                                       #
    //#########################################################################
    FCardPrivList.sort(CardItemGtDate());
    for (itCard = FCardPrivList.begin(); itCard != FCardPrivList.end(); itCard++)
     {
      //������������ ���� �������� �� ���� ��������
      FRC = FVacDateByInfComm.find(itCard->Inf);
      if (FRC != FVacDateByInfComm.end())
       {
        if (FRC->second < itCard->Date)
         FVacDateByInfComm[itCard->Inf] = itCard->Date;
       }
      else
       FVacDateByInfComm[itCard->Inf] = itCard->Date;
      if (itCard->Tur)
       {//������������ ���� ������� �������� �� ���� ��������
        FRC = FVacDateByTurInf.find(itCard->Inf);
        if (FRC != FVacDateByTurInf.end())
         {
          if (FRC->second < itCard->Date)
           FVacDateByTurInf[itCard->Inf] = itCard->Date;
         }
        else
         FVacDateByTurInf[itCard->Inf] = itCard->Date;
       }
      else
       {//������������ ���� �������� �� ���� ��������
        FRC = FVacDateByInf.find(itCard->Inf);
        if (FRC != FVacDateByInf.end())
         {
          if (FRC->second < itCard->Date)
           FVacDateByInf[itCard->Inf] = itCard->Date;
         }
        else
         FVacDateByInf[itCard->Inf] = itCard->Date;
       }
     }
    //#########################################################################
    //#                                                                       #
    //#  �����                                                                #
    //#                                                                       #
    //#########################################################################
    FCardTestList.sort(CardItemGtDate());
    for (itCard = FCardTestList.begin(); itCard != FCardTestList.end(); itCard++)
     {//������������ ���� ���� �� ���� �����
      FRC = FTestDateByTest.find(itCard->Id);
      if (FRC != FTestDateByTest.end())
       {
        if (FRC->second < itCard->Date)
         FTestDateByTest[itCard->Id] = itCard->Date;
       }
      else
       FTestDateByTest[itCard->Id] = itCard->Date;
      //������������ ���� ���� �� ���� ��������
      FRC = FTestDateByInf.find(itCard->Inf);
      if (FRC != FTestDateByInf.end())
       {
        if (FRC->second < itCard->Date)
         FTestDateByInf[itCard->Inf] = itCard->Date;
       }
      else
       FTestDateByInf[itCard->Inf] = itCard->Date;
     }
    //#########################################################################
    //#                                                                       #
    //#  ��������                                                             #
    //#                                                                       #
    //#########################################################################
    FCardCheckList.sort(CardItemGtDate());
    for (itCard = FCardCheckList.begin(); itCard != FCardCheckList.end(); itCard++)
     {
      //������������ ���� ���� �� ���� ��������
      FRC = FCheckDateByInf.find(itCard->Inf);
      if (FRC != FCheckDateByInf.end())
       {
        if (FRC->second < itCard->Date)
         FCheckDateByInf[itCard->Inf] = itCard->Date;
       }
      else
       FCheckDateByInf[itCard->Inf] = itCard->Date;
     }

    //#########################################################################
    //#                                                                       #
    //#  ������                                                               #
    //#                                                                       #
    //#########################################################################

    FCardCancelList.sort(CardItemGtEndDate());
    TDate FCurDate = FBeginDate;
    for (itCard = FCardCancelList.begin(); itCard != FCardCancelList.end(); itCard++)
     {//������������ ���� ��������� ������ �� ���� ��������
      //CallLogMsg("�������� ������� ��� �������� "+IntToStr(FPtId), __FUNC__, 5);
      if (!itCard->Tur && (itCard->Date <= FCurDate))
       {//��������� ����� � ���� ������ ������ ������ ���� ������ ��������� �������
        //CallLogMsg("��������� ����� ��� �������� "+IntToStr(FPtId), __FUNC__, 5);
        if (itCard->Vac)
         {//����� ��������
          FRC = FVacOtvodDateByInf.find(itCard->Inf);
          if (FRC != FVacOtvodDateByInf.end())
           {
            if (FRC->second < itCard->EndDate)
             FVacOtvodDateByInf[itCard->Inf] = itCard->EndDate;
           }
          else
           FVacOtvodDateByInf[itCard->Inf] = itCard->EndDate;
         }
        if (itCard->Test)
         {//����� �����
          //CallLogMsg("����� �� "+IntToStr(itCard->Inf)+" ��������� "+DateToStr(itCard->EndDate), __FUNC__, 5);

          FRC = FTestOtvodDateByInf.find(itCard->Inf);
          if (FRC != FTestOtvodDateByInf.end())
           {
            if (FRC->second < itCard->EndDate)
             FTestOtvodDateByInf[itCard->Inf] = itCard->EndDate;
           }
          else
           FTestOtvodDateByInf[itCard->Inf] = itCard->EndDate;
         }
       }
      else if (itCard->Tur)
       {//���������� �����
        //CallLogMsg("���������� ����� ��� �������� "+IntToStr(FPtId), __FUNC__, 5);
        if (itCard->Vac)
         {
          FRC = FConstVacOtvodDateByInf.find(itCard->Inf);
          if (FRC != FConstVacOtvodDateByInf.end())
           {
            if (FRC->second > itCard->Date)
             FConstVacOtvodDateByInf[itCard->Inf] = itCard->Date;
           }
          else
           FConstVacOtvodDateByInf[itCard->Inf] = itCard->Date;
         }
        else if (itCard->Test)
         {
          FRC = FConstTestOtvodDateByInf.find(itCard->Inf);
          if (FRC != FConstTestOtvodDateByInf.end())
           {
            if (FRC->second > itCard->Date)
             FConstTestOtvodDateByInf[itCard->Inf] = itCard->Date;
           }
          else
           FConstTestOtvodDateByInf[itCard->Inf] = itCard->Date;
         }
       }
     }
   }
  __finally
   {
    FDM->DeleteTempQuery(FQ);
   }
 }
//---------------------------------------------------------------------------

//������������

bool __fastcall TPrePlaner::CreateUnitPlan(TDate ABegDate, __int64 APtCode)
 {
  bool RC = false;
  FPlaning = true;
  //FLastItemDateMap.clear();

  try
   {
    CallOnPlanerStageChange(psInitialization);
    if (FDebugLog)
     FDebugLog->InitLog();
    CalcPlanPeriod(ABegDate);
    PtId = APtCode;
    CheckData(__FUNC__);
    GetUnitCardData();

    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    //�������������
    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    FPrivPlan.clear();
    FTestPlan.clear();
    FDopPrivivPlan.clear();
    FDopTestPlan.clear();

    CallOnIncProgress();

    //#      DebugLog                                                            #
    FDebugLog->LogPatData(FPtId, FPtFIO, FPtBirthDay, //#
        IntToStr(FPtAge.Years) + "�. " +              //#
        IntToStr(FPtAge.Months) + "�. " +             //#
        IntToStr(FPtAge.Days) + "�.",                 //#
        FUnitPlanSetting->AsXML);                     //#

    CallOnIncProgress();

    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    //���� �� ��������� ��� ������������ ��������
    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    //#      DebugLog                                                            #
    FDebugLog->Set_CVL_CTL(); //#

    CallOnPlanerStageChange(psPlanPriviv);
    PlanItemPlan(TPlanItemKind::pkVakchina);
    CallOnIncProgress();

    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    //���� �� ��������� ��� ������������ �����
    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    CallOnPlanerStageChange(psPlanProb);
    PlanItemPlan(TPlanItemKind::pkTest);
    CallOnIncProgress();

    CallOnPlanerStageChange(psPlanProb);
    CheckByConstCancel();
    CallOnIncProgress();

    //#      DebugLog                                                            #
    FDebugLog->DebugPrintListPlans(1/*"�� ����������"*/); //#

    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    //���������� �����
    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    CallOnIncProgress();

    //���������� ��� ���������� ��������/���� � ������� ������������ ��� � ������ ������
    CallOnPlanerStageChange(psCorrectPrivivPlanDate);
    CorrectPlanDateByMonth();
    CallOnIncProgress();

    //������������� ��� ������������ ���������� �������� � ������������ � �������
    //����� ������� � ����������
    //CallOnIncProgress();

    //��������� � ���� ���������� (��������������� ����)
    //CallOnPlanerStageChange( psAppendPrepareProb );
    AppendPrepareProb();
    //CallOnIncProgress();
    //

    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    //��������� ��������������� ����� � ��
    //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    //CallOnPlanerStageChange( psApplyPlan );
    ClearPlan();
    ApplyPlan(TPlanItemKind::pkVakchina, true);
    CallOnIncProgress();
    ApplyPlan(TPlanItemKind::pkTest, true);
    CallOnIncProgress();
    ApplyPlan(TPlanItemKind::pkTest, false);
    CallOnIncProgress();

    RC = true;
   }
  __finally
   {
    //FLastItemDateMap.clear();
    CallSaveDebugLog(NULL);
    FPrivPlan.clear();
    FTestPlan.clear();
    FDopPrivivPlan.clear();
    FDopTestPlan.clear();

    FCardPrivList.clear();
    FCardTestList.clear();
    FCardCheckList.clear();
    FCardCancelList.clear();

    FVacDateByInfComm.clear();
    FVacDateByInf.clear();
    FVacDateByTurInf.clear();
    FTestDateByTest.clear();
    FTestDateByInf.clear();
    FCheckDateByInf.clear();

    FVacOtvodDateByInf.clear();
    FTestOtvodDateByInf.clear();
    FConstVacOtvodDateByInf.clear();
    FConstTestOtvodDateByInf.clear();

    if (FUnitData)
     delete FUnitData;
    FUnitData               = NULL;
    FUnitPlanSetting->AsXML = "<root/>";

    FPlaning = false;
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TPrePlaner::GetPauseValue(int & APause, UnicodeString AType1, UnicodeString ARef1, UnicodeString AType2, UnicodeString ARef2)
 {
  bool RC = false;
  APause = 0;
  try
   {
    TTagNode * FPause = FDM->PlanOpt->GetChildByName("pause", true);
    if (FPause)
     {
      TTagNode * FDefPause = FPause->GetChildByName("pr" + AType1 + AType2, true);
      if (FDefPause)
       {
        APause = FDefPause->AV["val"].ToIntDef(0);
        RC     = true;
        TTagNode * itPause = FPause->GetChildByName("content");
        if (itPause)
         itPause = itPause->GetFirstChild();
        while (itPause)
         {
          if (itPause->CmpAV("objtype1", AType1) && itPause->CmpAV("objref1", ARef1) &&
              itPause->CmpAV("objtype2", AType2) && itPause->CmpAV("objref2", ARef2))
           {
            APause = itPause->AV["val"].ToIntDef(0);
            break;
           }
          itPause = itPause->GetNext();
         }
       }
     }
    else
     throw Exception("����������� ������ �������� \"�����\"");
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TPrePlaner::GetInfInfPause(int & APause, UnicodeString ARef1, UnicodeString ARef2)
 {
  return GetPauseValue(APause, "i", ARef1, "i", ARef2);
 }
//---------------------------------------------------------------------------
bool __fastcall TPrePlaner::GetTestInfPause(int & APause, UnicodeString ARef1, UnicodeString ARef2)
 {
  return GetPauseValue(APause, "t", ARef1, "i", ARef2);
 }
//---------------------------------------------------------------------------
bool __fastcall TPrePlaner::GetTestTestPause(int & APause, UnicodeString ARef1, UnicodeString ARef2)
 {
  return GetPauseValue(APause, "t", ARef1, "t", ARef2);
 }
//---------------------------------------------------------------------------
bool __fastcall TPrePlaner::GetInfTestPause(int & APause, UnicodeString ARef1, UnicodeString ARef2)
 {
  return GetPauseValue(APause, "i", ARef1, "t", ARef2);
 }
//---------------------------------------------------------------------------
int __fastcall TPrePlaner::GetMaxTestPause(int ATestCode)
 {
  int RC = 0;
  try
   {
    UnicodeString FTestCode = IntToStr(ATestCode);
    TTagNode * FPause = FDM->PlanOpt->GetChildByName("pause", true);
    if (FPause)
     {
      TTagNode * FDefPause = FPause->GetChildByName("prti", true);
      if (FDefPause)
       {
        RC = FDefPause->AV["val"].ToIntDef(0);
        TTagNode * itPause = FPause->GetChildByName("content");
        if (itPause)
         itPause = itPause->GetFirstChild();
        while (itPause)
         {
          if (itPause->CmpAV("objtype1", "t") && itPause->CmpAV("objref1", FTestCode))
           {
            if (RC < itPause->AV["val"].ToIntDef(0))
             RC = itPause->AV["val"].ToIntDef(0);
           }
          itPause = itPause->GetNext();
         }
       }
     }
    else
     throw Exception("����������� ������ �������� \"�����\"");
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
int __fastcall TPrePlaner::GetMaxPauseByMonth(TDate ADate)
 {
  int RC = 0;
  try
   {
    int tmpPause = 0;
    for (TPlanItemListIt itTest = FTestPlan.begin(); (itTest != FTestPlan.end()) && !RC; itTest++)
     {
      if (itTest->PlanMonth == ADate)
       {
        tmpPause = GetMaxTestPause(itTest->OwnerId);
        if (RC < tmpPause)
         RC = tmpPause;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::CorrectPlanDateByMonth()
 {
  //���������� ��� ���������� ��������/���� � ������� ������������ ��� � ������ ������
  FDebugLog->DebugPrintListPlans(3);

  TPlanItemList TempList, tmpTestPlan;
  tmpTestPlan.assign(FTestPlan.begin(), FTestPlan.end());
  TDate tmpDate;
  int MaxTestPause;
  for (TPlanItemListIt itTest = FTestPlan.begin(); itTest != FTestPlan.end(); itTest++)
   {
    tmpDate      = itTest->PlanDate;
    MaxTestPause = GetMaxTestPause(itTest->OwnerId);
    tmpDate      = Dateutils::IncDay(tmpDate, MaxTestPause); //MaxTestPause+1
    if (MonthOf(itTest->PlanDate) != MonthOf(tmpDate))
     {//���� ���������� �������� ������� �� ������� ������ ��-�� ����
      //��������� ����� �� ������ ���������� ������
      itTest->PlanDate = TDate("01." + System::Sysutils::IncMonth(itTest->PlanDate).FormatString("mm.yyyy"));
      //itTest->PlanDate = Dateutils::IncDay(itTest->PlanDate, MaxTestPause+5);
      itTest->PlanMonth      = TDate("01." + itTest->PlanDate.FormatString("mm.yyyy"));
      itTest->PlanDayByMonth = itTest->PlanDate;
     }
   }

  TempList.splice(TempList.end(), FPrivPlan);
  TempList.splice(TempList.end(), tmpTestPlan);

  TempList.sort(PlanItemListGreaterPlanDate());

  TDate FMaxDate = TDate(0);
  TDate FLast = TDate(0);
  int FPause;
  for (TPlanItemListIt i = TempList.begin(); i != TempList.end(); i++)
   {
    if (i->PlanMonth == FLast)
     {
      if ((i->PlanItemKind == TPlanItemKind::pkVakchina))
       {
        i->PlanDate = FMaxDate;
       }
      i->PlanDayByMonth = FMaxDate;
     }
    else
     {
      FLast  = i->PlanMonth;
      FPause = GetMaxPauseByMonth(i->PlanMonth);
      if (FPause)
       {
        FMaxDate = Dateutils::IncDay(i->PlanDate, FPause);
        if ((i->PlanItemKind == TPlanItemKind::pkVakchina))
         i->PlanDate = FMaxDate;
        i->PlanDayByMonth = FMaxDate;
       }
      else
       FMaxDate = i->PlanDate;
     }
   }
  FPrivPlan.clear();
  FTestPlan.clear();
  for (TPlanItemListIt i = TempList.begin(); i != TempList.end(); i++)
   {
    if (i->PlanItemKind == TPlanItemKind::pkVakchina)
     FPrivPlan.push_back(* i);
    else
     FTestPlan.push_back(* i);
   }
  //FPrivPlan.assign(TempList.begin(), TempList.end());
  FDebugLog->DebugPrintListPlans(4);
 }
//---------------------------------------------------------------------------
bool __fastcall TPrePlaner::FGetVacDateByType(int AInfId, bool ARoundMode, TPlanItemKind APlanItemKind, TDate & ADate, UnicodeString AVacType)
 {
  bool RC = false;
  try
   {
    TCardItemList::iterator itCard;
    TCardItemList::iterator CardItem;

    UnicodeString FVacType = AVacType.UpperCase();
    if (APlanItemKind == TPlanItemKind::pkVakchina)
     {
      FCardPrivList.sort(CardItemGtDate());
      for (itCard = FCardPrivList.begin(); (itCard != FCardPrivList.end()) && !RC; itCard++)
       {
        RC = ((itCard->Tur == ARoundMode) && (itCard->Inf == AInfId) && (itCard->VacType == FVacType));
        if (RC)
         ADate = itCard->Date;
       }
     }
    else if (APlanItemKind == TPlanItemKind::pkTest)
     {
      TPlanItemDateList::iterator FIRC;
      FIRC = FTestDateByInf.find(AInfId);
      if (FIRC != FTestDateByInf.end())
       {
        ADate = FIRC->second;
        RC    = true;
       }
     }
    else if (APlanItemKind == TPlanItemKind::pkCheck)
     {
      TPlanItemDateList::iterator FIRC;
      FIRC = FCheckDateByInf.find(AInfId);
      if (FIRC != FCheckDateByInf.end())
       {
        ADate = FIRC->second;
        RC    = true;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#       ���������� ����� ��� �����������                                     #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
System::UnicodeString __fastcall TPrePlaner::StartCreateUnitPlan(TDate ABegDate, __int64 APtCode)
 {
  CallLogBegin(__FUNC__);
  UnicodeString RC = "error";
  try
   {
    if (!Planing)
     {
      if (FBkgPlaner)
       {
        FBkgPlaner->Terminate();
        Sleep(50);
        delete FBkgPlaner;
        FBkgPlaner = NULL;
       }
      FBkgPlaner = new TBackgroundPlaner(false, this, ABegDate, APtCode);
      RC = "ok";
     }
    else
     RC = "planing";
   }
  __finally
   {
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TPrePlaner::StopCreatePlan()
 {
  UnicodeString RC = "error";
  CallLogBegin(__FUNC__);
  try
   {
    if (FBkgPlaner && Planing)
     {
      CallSaveDebugLog(NULL);
      FTerminated = true;
      FPlaning    = false;
      delete FBkgPlaner;
      FBkgPlaner = NULL;
     }
    RC = "ok";
   }
  __finally
   {
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
void __fastcall TPrePlaner::SyncCall(TdsSyncMethod AMethod)
 {
  if (FBkgPlaner)
   FBkgPlaner->ThreadSafeCall(AMethod);
  else
   AMethod();
 }
//---------------------------------------------------------------------------
int __fastcall TPrePlaner::GetMaxProgress()
 {
  return FCommPlanCount;
 }
//---------------------------------------------------------------------------
int __fastcall TPrePlaner::GetCurProgress()
 {
  return FCurrPlan;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TPrePlaner::GetCurStage() const
 {
  return FPCStage;
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::CallLogMsg(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogMsg);
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::SyncCallLogMsg()
 {
  if (FOnLogMessage)
   FOnLogMessage(FLogMsg, FLogFuncName, FLogALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::CallLogErr(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogErr);
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::SyncCallLogErr()
 {
  if (FOnLogError)
   FOnLogError(FLogMsg, FLogFuncName, FLogALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::CallLogBegin(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogBegin);
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::SyncCallLogBegin()
 {
  if (FOnLogBegin)
   FOnLogBegin(FLogFuncName);
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::CallLogEnd(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogEnd);
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::SyncCallLogEnd()
 {
  if (FOnLogEnd)
   FOnLogEnd(FLogFuncName);
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::CallSaveDebugLog(TObject * Sender)
 {
  SyncCall(& SyncCallSaveDebugLog);
 }
//---------------------------------------------------------------------------
void __fastcall TPrePlaner::SyncCallSaveDebugLog()
 {
  CallLogBegin(__FUNC__);
  //if (FDebugLog)
  //FDebugLog->Log->SaveToXMLFile("c:\\trace.xml","");
  if (FOnSaveDebugLog && FDebugLog)
   FOnSaveDebugLog(FDebugLog->Log->AsXML);
  //FSetDebugLog();
  CallLogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
