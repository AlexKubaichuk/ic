//---------------------------------------------------------------------------

#ifndef dsSrvPlanUnitH
#define dsSrvPlanUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
#include "dsPlaner.h"
#include "dsPlanDMUnit.h"
//#include "DataExchange.h"
//#include "diDataTypeDef.h"
//---------------------------------------------------------------------------
//typedef enum {cdtGet, cdtSet} TdsRegTextDataConvType;

typedef bool  (__closure *TdsPlanCheckSpecExistsEvent)(UnicodeString AId);
//---------------------------------------------------------------------------
class PACKAGE TdsSrvPlaner : public TComponent
{
__published:
private:
  TdsPlanDM      *FDM;
  TdsPlaner      *FPlaner;

  TdsPlanCheckSpecExistsEvent FCheckSpecExists;

//  TdsDIOnExtGetCodeEvent __fastcall FGetOnExtGetCode();
//  void __fastcall FSetOnExtGetCode(TdsDIOnExtGetCodeEvent AVal);

//  TdsRegTextDataConvEvent FDataConv;
//---------------------------------------------------------------------------
  TdsOnPlanGetValByIdEvent  __fastcall FGetOnPlanGetValById();
  void __fastcall FSetOnPlanGetValById(TdsOnPlanGetValByIdEvent AVal);

  TdsOnPlanGetValById10Event  __fastcall FGetOnPlanGetValById10();
  void __fastcall FSetOnPlanGetValById10(TdsOnPlanGetValById10Event AVal);

  TdsOnPlanInsertDataEvent  __fastcall FGetOnPlanInsertData();
  void __fastcall FSetOnPlanInsertData(TdsOnPlanInsertDataEvent AVal);

  TdsOnPlanGetMIBPListEvent  __fastcall FGetOnGetMIBPList();
  void __fastcall FSetOnGetMIBPList(TdsOnPlanGetMIBPListEvent AVal);

  TdsPlanGetStrValByCodeEvent __fastcall FGetOnGetOrgStr();
  void __fastcall FSetOnGetOrgStr(TdsPlanGetStrValByCodeEvent AVal);

public:
  __fastcall TdsSrvPlaner(TComponent* Owner, TFDConnection *AConnection, TAxeXMLContainer *AXMLList, const UnicodeString ARegGUI, const UnicodeString AImpGUI);
  __fastcall ~TdsSrvPlaner();
  void __fastcall SetRegDef(TTagNode *AVal);
  void __fastcall Disconnect();

  __int64               __fastcall GetCount(System::UnicodeString AId, System::UnicodeString AFilterParam);
  TJSONObject*          __fastcall GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam);
  TJSONObject*          __fastcall GetValById(System::UnicodeString AId, System::UnicodeString ARecId);
  TJSONObject*          __fastcall GetValById10(System::UnicodeString AId, System::UnicodeString ARecId);
  TJSONObject*          __fastcall Find(System::UnicodeString AId, TJSONObject *AFindParam);
  System::UnicodeString __fastcall InsertData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString __fastcall EditData(System::UnicodeString AId, TJSONObject *AValue, System::UnicodeString &ARecId);
  System::UnicodeString __fastcall DeleteData(System::UnicodeString AId, System::UnicodeString ARecId);
  System::UnicodeString __fastcall SetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString AVal);
  System::UnicodeString __fastcall GetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, UnicodeString &AMsg);
  System::UnicodeString __fastcall CreateUnitPlan(__int64 APtId);
//  System::UnicodeString __fastcall CreatePlan(TJSONObject *AVal, System::UnicodeString &ARecId);
  System::UnicodeString __fastcall DeletePlan(System::UnicodeString ARecId);

  System::UnicodeString __fastcall GetPatPlanOptions(__int64 APatCode);
  void                  __fastcall SetPatPlanOptions(__int64 APatCode, System::UnicodeString AData);
  void                  __fastcall CheckAndUpdate(__int64 AUCode);
  System::UnicodeString __fastcall StartCreatePlan(TJSONObject *AVal);
  System::UnicodeString __fastcall StopCreatePlan();
  System::UnicodeString __fastcall CheckCreatePlanProgress(System::UnicodeString &ARecId);
  TJSONObject* __fastcall GetPlanPrintFormats();
  System::UnicodeString __fastcall GetPlanTemplateDef(int AType);
  System::UnicodeString __fastcall GetPlanOpt();
  void                  __fastcall SetPlanOpt(UnicodeString AOpts);

  __property TdsOnPlanGetValByIdEvent  OnGetValById     = {read=FGetOnPlanGetValById, write=FSetOnPlanGetValById};
  __property TdsOnPlanGetValById10Event  OnGetValById10     = {read=FGetOnPlanGetValById10, write=FSetOnPlanGetValById10};
  __property TdsOnPlanInsertDataEvent  OnPlanInsertData = {read=FGetOnPlanInsertData, write=FSetOnPlanInsertData};
  __property TdsOnPlanGetMIBPListEvent OnGetMIBPList    = {read=FGetOnGetMIBPList,    write=FSetOnGetMIBPList};

  __property TdsPlanGetStrValByCodeEvent  OnGetOrgStr = {read = FGetOnGetOrgStr, write=FSetOnGetOrgStr};
  __property TdsPlanCheckSpecExistsEvent   OnCheckSpecExists = {read = FCheckSpecExists, write=FCheckSpecExists};

//  __property TdsDIOnExtGetCodeEvent OnExtGetCode = {read = FGetOnExtGetCode, write = FSetOnExtGetCode};
//  __property TdsRegTextDataConvEvent OnDataConv = {read = FDataConv, write=FDataConv};
};
//---------------------------------------------------------------------------
#endif
