//---------------------------------------------------------------------------

#ifndef dsPlanerH
#define dsPlanerH
//---------------------------------------------------------------------------
//#include "PlanData.h"
#include "PrePlaner.h"
#include "PlanLinker.h"
#include "XMLContainer.h"
#include "dsPlanDMUnit.h"
//---------------------------------------------------------------------------
class PACKAGE TdsPlaner: public TObject
{
private:
  enum TPlanningStatus
   {
     psNone,               // ������������ �� �����������
     psInProgress,         // ������������ �����������
     psTermNormal,         // ���������� ���������� ������������
     psTermByUser,         // ������������ �������� �������������
     psTermOnFormClose     // ������������ �������� ��� �������� �����
   };
  TdsPlanDM  *FDM;
  TPrePlaner  *FIMMPlaner;          // �����������
  TPlanLinker *FPlanLinker;         // ����������� �����
  TTagNode   *DebugPlanLog;
  TStringList *PlanningErrorLog;

  TPlanningStatus FPlanningStatus;     // ������ �������� ������������
  void __fastcall PlanLinkerStageChange( TObject* Sender, TPlanLinkerStage NewPlanerStage );
  void __fastcall PlanerStageChange( TObject* Sender, TPlanerStage NewPlanerStage );
  void __fastcall FLogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl);
  void __fastcall FLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl);
  void __fastcall FLogBegin(UnicodeString AFuncName);
  void __fastcall FLogEnd(UnicodeString AFuncName);
  void __fastcall FSaveDebugLog(UnicodeString AXML);

  void __fastcall PlanCreateEnd();
  void __fastcall FCreateUnitPlan(__int64 APtId, TPrePlaner *APrePlaner, TDate ABegDate);
  void __fastcall FOnCreateUnitPlan(__int64 APtId, TDate ABegDate);


protected:
public:
  __fastcall TdsPlaner(TdsPlanDM   *ADM);
  __fastcall ~TdsPlaner();
  void __fastcall CreateUnitPlan(__int64 APtId);
//  UnicodeString __fastcall CreatePlan(TJSONObject *AVal, System::UnicodeString &ARecId);
  UnicodeString __fastcall StartCreatePlan(TJSONObject *AVal);
  UnicodeString __fastcall StopCreatePlan();
  UnicodeString __fastcall GetPlanProgress(System::UnicodeString &ARecCode);
};
//---------------------------------------------------------------------------
#endif


