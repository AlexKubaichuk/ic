//---------------------------------------------------------------------------

#pragma hdrstop

#include "dsPlaner.h"
#include "icsLog.h"
#include "DKUtils.h"
#include "DKClasses.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall TdsPlaner::TdsPlaner(TdsPlanDM * ADM)
 {
  FDM         = ADM;
  FIMMPlaner  = new TPrePlaner(FDM);
  FPlanLinker = new TPlanLinker(FDM);

  FIMMPlaner->VacSchXML           = FDM->VacSchXML;
  FIMMPlaner->TestSchXML          = FDM->TestSchXML;
  FIMMPlaner->CheckSchXML         = FDM->CheckSchXML;
  FIMMPlaner->OnPlanerStageChange = PlanerStageChange;

  FIMMPlaner->OnLogMessage   = FLogMessage;
  FIMMPlaner->OnLogError     = FLogError;
  FIMMPlaner->OnLogBegin     = FLogBegin;
  FIMMPlaner->OnLogEnd       = FLogEnd;
  FIMMPlaner->OnSaveDebugLog = FSaveDebugLog;

  FPlanningStatus = psNone;
  // //DebugPlanLog = NULL;
  // //PlanningErrorLog = NULL;

  FPlanLinker->OnPlanerStageChange = PlanLinkerStageChange;

  FPlanLinker->OnLogMessage = FLogMessage;
  FPlanLinker->OnLogError   = FLogError;
  FPlanLinker->OnLogBegin   = FLogBegin;
  FPlanLinker->OnLogEnd     = FLogEnd;
 }
//---------------------------------------------------------------------------
__fastcall TdsPlaner::~TdsPlaner()
 {
  FPlanLinker->OnLogMessage = NULL;
  FPlanLinker->OnLogError   = NULL;
  FPlanLinker->OnLogBegin   = NULL;
  FPlanLinker->OnLogEnd     = NULL;

  FIMMPlaner->OnLogMessage = NULL;
  FIMMPlaner->OnLogError   = NULL;
  FIMMPlaner->OnLogBegin   = NULL;
  FIMMPlaner->OnLogEnd     = NULL;
  FIMMPlaner->OnSaveDebugLog = NULL;

  delete FPlanLinker;
  delete FIMMPlaner;
  //if (PlanningErrorLog) delete PlanningErrorLog;
  //if (DebugPlanLog)     delete DebugPlanLog;
 }
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#       ���� ��� ��������                                                    #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
void __fastcall TdsPlaner::CreateUnitPlan(__int64 APtId)
 {
  TTime FBeg = Time();
  dsPlanLogMessage("������������ ��� " + IntToStr(APtId), __FUNC__);

  TPrePlaner * FPrePlaner = new TPrePlaner(FDM);
  try
   {
    FPrePlaner->VacSchXML           = FDM->VacSchXML;
    FPrePlaner->TestSchXML          = FDM->TestSchXML;
    FPrePlaner->CheckSchXML         = FDM->CheckSchXML;
    FPrePlaner->OnPlanerStageChange = PlanerStageChange;

    FPrePlaner->OnLogMessage   = FLogMessage;
    FPrePlaner->OnLogError     = FLogError;
    FPrePlaner->OnLogBegin     = FLogBegin;
    FPrePlaner->OnLogEnd       = FLogEnd;
    FPrePlaner->OnSaveDebugLog = FSaveDebugLog;
    FPlanningStatus            = psNone;
    FCreateUnitPlan(APtId, FPrePlaner, Date());
   }
  __finally
   {
    FPrePlaner->OnLogMessage   = NULL;
    FPrePlaner->OnLogError     = NULL;
    FPrePlaner->OnLogBegin     = NULL;
    FPrePlaner->OnLogEnd       = NULL;
    FPrePlaner->OnSaveDebugLog = NULL;

    delete FPrePlaner;
    dsPlanLogMessage("������������ ������������ ��� " + IntToStr(APtId) + ": " + (Time() - FBeg).FormatString("nn.ss.zzz"), __FUNC__);
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::FCreateUnitPlan(__int64 APtId, TPrePlaner * APrePlaner, TDate ABegDate)
 {//������������ ��� ��������
  try
   {
    dsPlanLogMessage("������������", __FUNC__, 5);
    FPlanningStatus = psInProgress;
    try
     {
      APrePlaner->CreateUnitPlan(ABegDate, APtId);
     }
    catch (DKCommonExceptionEx & E)
     {
      dsPlanLogError("��� ������ - " + IntToStr(E.Id) + "; ���������  - " + E.Message + " ������ ���������:" + E.DetMsg->Text, __FUNC__);
     }
    catch (Exception & E)
     {
      dsPlanLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    FPlanningStatus = psNone;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::FOnCreateUnitPlan(__int64 APtId, TDate ABegDate)
 {
  TTime FBeg = Time();
  try
   {
    FCreateUnitPlan(APtId, FIMMPlaner, ABegDate);
   }
  __finally
   {
    dsPlanLogMessage("������������ ������������ ��� " + IntToStr(APtId) + ": " + (Time() - FBeg).FormatString("nn.ss.zzz"));
   }
 }
//---------------------------------------------------------------------------
/*
 UnicodeString __fastcall TdsPlaner::CreatePlan(TJSONObject * AVal, System::UnicodeString & ARecId)
 {
 UnicodeString RC = "������";
 return RC;
 }
 //---------------------------------------------------------------------------
 */
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#       ���� �� �������/�����������                                          #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
UnicodeString __fastcall TdsPlaner::StartCreatePlan(TJSONObject * AVal)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  try
   {
    FDM->OnCreateUnitPlan = FOnCreateUnitPlan;
    RC                    = FPlanLinker->StartCreatePlan(AVal);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsPlaner::StopCreatePlan()
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "������";
  try
   {
    RC = FPlanLinker->StopCreatePlan();
   }
  __finally
   {
    FDM->OnCreateUnitPlan = NULL;
    PlanCreateEnd();
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsPlaner::GetPlanProgress(System::UnicodeString & ARecCode)
 {
  UnicodeString RC = "������";
  ARecCode = "-1";
  try
   {
    if (FPlanLinker->Planing)
     {
      RC = FPlanLinker->GetCurStage() + " [" + IntToStr(FPlanLinker->GetCurProgress()) + "/" + IntToStr(FPlanLinker->GetMaxProgress()) + "]";
     }
    else
     {
      PlanCreateEnd();
      ARecCode = FPlanLinker->PCRecId;
      RC       = "ok";
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::PlanCreateEnd()
 {
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::FLogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsPlanLogMessage(AMessage, AFuncName, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::FLogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsPlanLogError(AMessage, AFuncName, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::FLogBegin(UnicodeString AFuncName)
 {
  dsLogBegin(AFuncName);
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::FLogEnd(UnicodeString AFuncName)
 {
  dsLogEnd(AFuncName);
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::FSaveDebugLog(UnicodeString AXML)
 {
  dsLogBegin(__FUNC__);
  if (AXML.Length())
   {
    dsPlanLogMessage("==============================================================================");
    dsPlanLogMessage(AXML);
    dsPlanLogMessage("==============================================================================");
   }
  dsLogEnd(__FUNC__);
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::PlanLinkerStageChange(TObject * Sender, TPlanLinkerStage NewPlanerStage)
 {
 }
//---------------------------------------------------------------------------
void __fastcall TdsPlaner::PlanerStageChange(TObject * Sender, TPlanerStage NewPlanerStage)
 {
  if (/*(*/FPlanningStatus == psInProgress/*) && FPlanningProgress*/)
   {
    switch (NewPlanerStage)
     {
     case psInitialization:
      FLogMessage("\"psInitialization\"", __FUNC__, 5);
      break;
     case psUpdatePlans:
      FLogMessage("\"psUpdatePlans\"", __FUNC__, 5);
      break;
     case psPlanDop:
      FLogMessage("\"psPlanDop\"", __FUNC__, 5);
      break;
     case psPlanPriviv:
      FLogMessage("\"psPlanPriviv\"", __FUNC__, 5);
      break;
     case psPlanProb:
      FLogMessage("\"psPlanProb\"", __FUNC__, 5);
      break;
     case psAppendDopPrivivByPrevProb:
      FLogMessage("\"psAppendDopPrivivByPrevProb\"", __FUNC__, 5);
      break;
     case psCorrectPolyVak:
      FLogMessage("\"psCorrectPolyVak\"", __FUNC__, 5);
      break;
     case psCorrectPrivivPlanDate:
      FLogMessage("\"psCorrectPrivivPlanDate\"", __FUNC__, 5);
      break;
     case psAppendPrepareProb:
      FLogMessage("\"psAppendPrepareProb\"", __FUNC__, 5);
      break;
     case psApplyPlan:
      FLogMessage("\"psApplyPlan\"", __FUNC__, 5);
      break;
     }
    Application->ProcessMessages();
   }
 }

//---------------------------------------------------------------------------
