﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsSrvPlanUnit.h"
#include "CheckSch.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall TdsSrvPlaner::TdsSrvPlaner(TComponent * Owner, TFDConnection * AConnection, TAxeXMLContainer * AXMLList, const UnicodeString ARegGUI, const UnicodeString AImpGUI)
    : TComponent(Owner)
 {
  dsLogC(__FUNC__);
  FDM = new TdsPlanDM(this, AConnection, AXMLList);
  FDM->Connect();
  FPlaner = new TdsPlaner(FDM);
  dsLogC(__FUNC__, true);
 }
//---------------------------------------------------------------------------
__fastcall TdsSrvPlaner::~TdsSrvPlaner()
 {
  dsLogD(__FUNC__);
  FDM->Disconnect();
  delete FPlaner;
  delete FDM;
  dsLogD(__FUNC__, true);
 }
//---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::Disconnect()
 {
  FDM->Disconnect();
 }
//---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::SetRegDef(TTagNode * AVal)
 {
  FDM->RegDef = AVal;
 }
//---------------------------------------------------------------------------
__int64 __fastcall TdsSrvPlaner::GetCount(System::UnicodeString  AId, System::UnicodeString  AFilterParam)
 {
  dsLogBegin(__FUNC__);
  __int64 RC = 0;
  try
   {
    RC = FDM->GetCount(AId, AFilterParam);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvPlaner::GetIdList(System::UnicodeString  AId, int AMax, System::UnicodeString  AFilterParam)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC;
  try
   {
    RC = FDM->GetIdList(AId, AMax, AFilterParam);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvPlaner::GetValById(System::UnicodeString  AId, System::UnicodeString  ARecId)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC;
  try
   {
    RC = FDM->GetValById(AId, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvPlaner::GetValById10(System::UnicodeString  AId, System::UnicodeString  ARecId)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC;
  try
   {
    RC = FDM->GetValById10(AId, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvPlaner::Find(System::UnicodeString  AId, TJSONObject * AFindParam)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC;
  try
   {
    RC = FDM->Find(AId, AFindParam);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::InsertData(System::UnicodeString  AId, TJSONObject * AValue, System::UnicodeString & ARecId)
 {
  UnicodeString RC = "";
  try
   {
    //RC = FDataImporter->InsertOrUpdateData(AId, AValue, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::EditData(System::UnicodeString  AId, TJSONObject * AValue, System::UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    //RC = FDataImporter->InsertOrUpdateData(AId, AValue, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::DeleteData(System::UnicodeString  AId, System::UnicodeString  ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    RC = FDM->DeleteData(AId, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::GetTextData(System::UnicodeString  AId, System::UnicodeString  AFlId, System::UnicodeString  ARecId, UnicodeString & AMsg)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    //if (FDataConv)
    //RC = FDataConv(cdtGet, AId, AFlId, FDM->GetTextData(AId, AFlId, ARecId, AMsg));
    //else
    //RC = FDM->GetTextData(AId, AFlId, ARecId, AMsg);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::SetTextData(System::UnicodeString  AId, System::UnicodeString  AFlId, System::UnicodeString  ARecId,
    System::UnicodeString  AVal)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    //if (FDataConv)
    //RC = FDM->SetTextData(AId, AFlId, ARecId, FDataConv(cdtSet, AId, AFlId, AVal));
    //else
    //RC = FDM->SetTextData(AId, AFlId, ARecId, AVal);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::CreateUnitPlan(__int64 APtId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    CheckAndUpdate(APtId);
//    FPlaner->CreateUnitPlan(APtId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
/*
System::UnicodeString __fastcall TdsSrvPlaner::CreatePlan(TJSONObject * AVal, System::UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "ошибка";
  try
   {
    RC = FPlaner->CreatePlan(AVal, ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
 */
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::DeletePlan(System::UnicodeString  ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "ошибка";
  try
   {
    RC = FDM->DeletePlan(ARecId);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::StartCreatePlan(TJSONObject * AVal)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "ошибка";
  try
   {
    RC = FPlaner->StartCreatePlan(AVal);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::StopCreatePlan()
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "ошибка";
  try
   {
    RC = FPlaner->StopCreatePlan();
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::CheckCreatePlanProgress(System::UnicodeString & ARecId)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "error";
  ARecId = "";
  try
   {
    RC = FPlaner->GetPlanProgress(ARecId);
    if (RC == "error")
     {
      FDM->OnCreateUnitPlan = NULL;
     }
    else if (RC == "ok")
     {
      FDM->OnCreateUnitPlan = NULL;
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::GetPatPlanOptions(__int64 APatCode)
 {
  dsLogBegin(UnicodeString(__FUNC__) + "PtId :" + IntToStr(APatCode));
  CheckAndUpdate(APatCode);
  UnicodeString RC = FDM->GetPatPlanOptions(APatCode);
  dsLogEnd(__FUNC__);
  return RC;
 }
//----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::SetPatPlanOptions(__int64 APatCode, System::UnicodeString AData)
 {
  dsLogBegin(__FUNC__);
  TTagNode * tmpSch = new TTagNode;
  try
   {
    tmpSch->AsXML = AData;
    TTagNode * FPatPlan = tmpSch->GetChildByName("comm", true);
    if (FPatPlan)
     delete FPatPlan;
    FDM->SetUnitSch(APatCode, tmpSch->AsXML);
   }
  __finally
   {
    delete tmpSch;
   }
  dsLogEnd(__FUNC__);
 }
//----------------------------------------------------------------------------
TdsOnPlanGetValByIdEvent __fastcall TdsSrvPlaner::FGetOnPlanGetValById()
 {
  TdsOnPlanGetValByIdEvent RC = NULL;
  if (FDM)
   RC = FDM->OnGetValById;
  return RC;
 }
//----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FSetOnPlanGetValById(TdsOnPlanGetValByIdEvent AVal)
 {
  if (FDM)
   FDM->OnGetValById = AVal;
 }
//----------------------------------------------------------------------------
TdsOnPlanGetValById10Event __fastcall TdsSrvPlaner::FGetOnPlanGetValById10()
 {
  TdsOnPlanGetValById10Event RC = NULL;
  if (FDM)
   RC = FDM->OnGetValById10;
  return RC;
 }
//----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FSetOnPlanGetValById10(TdsOnPlanGetValById10Event AVal)
 {
  if (FDM)
   FDM->OnGetValById10 = AVal;
 }
//----------------------------------------------------------------------------
TdsOnPlanInsertDataEvent __fastcall TdsSrvPlaner::FGetOnPlanInsertData()
 {
  TdsOnPlanInsertDataEvent RC = NULL;
  if (FDM)
   RC = FDM->OnPlanInsertData;
  return RC;
 }
//----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FSetOnPlanInsertData(TdsOnPlanInsertDataEvent AVal)
 {
  if (FDM)
   FDM->OnPlanInsertData = AVal;
 }
//----------------------------------------------------------------------------
TdsOnPlanGetMIBPListEvent __fastcall TdsSrvPlaner::FGetOnGetMIBPList()
 {
  TdsOnPlanGetMIBPListEvent RC = NULL;
  if (FDM)
   RC = FDM->OnGetMIBPList;
  return RC;
 }
//----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FSetOnGetMIBPList(TdsOnPlanGetMIBPListEvent AVal)
 {
  if (FDM)
   FDM->OnGetMIBPList = AVal;
 }
//----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::CheckAndUpdate(__int64 AUCode)
 {
  dsLogBegin(__FUNC__);
  if (FDM)
   {
    TCheckSch * FUpd = new TCheckSch(FDM);
    try
     {
      FUpd->CheckAndUpdate(AUCode);
     }
    __finally
     {
      delete FUpd;
     }
   }
  dsLogEnd(__FUNC__);
 }
//----------------------------------------------------------------------------
TdsPlanGetStrValByCodeEvent __fastcall TdsSrvPlaner::FGetOnGetOrgStr()
 {
  TdsPlanGetStrValByCodeEvent RC = NULL;
  if (FDM)
   RC = FDM->OnGetOrgStr;
  return RC;
 }
//----------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::FSetOnGetOrgStr(TdsPlanGetStrValByCodeEvent AVal)
 {
  if (FDM)
   FDM->OnGetOrgStr = AVal;
 }
//----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSrvPlaner::GetPlanPrintFormats()
 {
  TJSONObject * RC = new TJSONObject;
  try
   {
    try
     {
      TTagNode * FPPS = FDM->PlanOpt->GetChildByName("plan_print_formats", true);
      if (FPPS)
       {
        FPPS = FPPS->GetFirstChild();
        while (FPPS)
         {
          if (FCheckSpecExists)
           {
            if (FCheckSpecExists(FPPS->AV["ref"]))
             RC->AddPair(FPPS->AV["ref"], FPPS->AV["name"]);
           }
          FPPS = FPPS->GetNext();
         }
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      dsPlanLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::GetPlanTemplateDef(int AType)
 {
  UnicodeString RC = "error";
  try
   {
    RC = FDM->GetPlanTemplateDef(AType);
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsSrvPlaner::GetPlanOpt()
 {
  UnicodeString RC = "error";
  try
   {
    RC = FDM->GetPlanOpt();
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSrvPlaner::SetPlanOpt(UnicodeString AOpts)
 {
  FDM->SetPlanOpt(AOpts);
 }
//---------------------------------------------------------------------------
