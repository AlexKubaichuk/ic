//---------------------------------------------------------------------------
#include <vcl.h>


#pragma hdrstop

#include <System.DateUtils.hpp>
#include "dsPlanDMUnit.h"
#include "dsSrvRegTemplate.h"
#include "OrgParsers.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

const UnicodeString csXMLDateFmtStr   = "dd.mm.yyyy";
const wchar_t cchXMLDateSeparator     = '.';
const wchar_t cchXMLPeriodSeparator   = '-';
const wchar_t cchXMLDurationSeparator = '/';

//---------------------------------------------------------------------------
const UnicodeString csReqFldNULL = "_NULL_";
//---------------------------------------------------------------------------
const int nMaxVPriority = 1;
const int nMinVPriority = 999;
const int nMaxRVPriority = nMaxVPriority;
const int nMinRVPriority = nMinVPriority;
//---------------------------------------------------------------------------
const UnicodeString PatLName           = "001F";
const UnicodeString PatName            = "0020";
const UnicodeString PatSName           = "002F";
const UnicodeString PatBitrhDay        = "0031";
//---------------------------------------------------------------------------
//###########################################################################
//##                                                                       ##
//##                              Globals                                  ##
//##                                                                       ##
//###########################################################################

//�������������� ������� ��� ���������� ������ ��������/����/��������/�������
bool CardItemGtDate::operator() (TCardItem x, TCardItem y)
{
  return x.Date > y.Date;
};
//---------------------------------------------------------------------------
bool CardItemLtDate::operator() (TCardItem x, TCardItem y)
{
  return x.Date < y.Date;
};
//---------------------------------------------------------------------------
bool CardItemGtEndDate::operator() (TCardItem x, TCardItem y)
{
  return x.EndDate > y.EndDate;
};
//---------------------------------------------------------------------------
bool CardItemLtEndDate::operator() (TCardItem x, TCardItem y)
{
  return x.EndDate < y.EndDate;
};
//---------------------------------------------------------------------------
bool PlanItemListLessOwner::operator() (PLAN_ITEM x, PLAN_ITEM y)
{
  return x.OwnerId < y.OwnerId;
};
//---------------------------------------------------------------------------
bool PlanItemListGreaterPriority::operator() (PLAN_ITEM x, PLAN_ITEM y)
{
  if ( x.Priority == y.Priority )
    return x.V_RV_Priority < y.V_RV_Priority;
  else
    return x.Priority < y.Priority;
}
//---------------------------------------------------------------------------
bool PlanItemListLessPlanDate::operator() (PLAN_ITEM x, PLAN_ITEM y)
{
  return x.PlanDate < y.PlanDate;
}
//---------------------------------------------------------------------------
bool PlanItemListGreaterPlanDate::operator() (PLAN_ITEM x, PLAN_ITEM y)
{
  return x.PlanDate > y.PlanDate;
}
//---------------------------------------------------------------------------
__fastcall TdsCommClassData::TdsCommClassData(TComponent* Owner, TFDConnection *AConnection, TAxeXMLContainer *AXMLList)
  : TDataModule(Owner)
{
  FConnection = AConnection;
  FRegDef = new TTagNode;
  FUnitDataDef = new TTagNode;
  FUnitDataDef->Name = "fld";
//  FPlanOpt = new TTagNode;
  FStoreMIBPList          = new TIntMap;
  FDefStoreMIBPList       = new TIntMap;
  FSortedStoreMIBPList    = new TIntMap;
  FDefSortedStoreMIBPList = new TIntMap;
  FStoreTestList          = new TIntMap;
  FDefStoreTestList       = new TIntMap;

  FXMLList = AXMLList;

  FPlanOpt = FXMLList->GetXML("planSetting");
  FPlanOptDefSch = FPlanOpt->GetChildByName("infdef", true);
//  FVacSchXML = new TTagNode;
//  FTestSchXML = new TTagNode;
//  FCheckSchXML = new TTagNode;
  FVacSchXML   = FXMLList->GetXML("12063611-00008cd7-cd89");
  FTestSchXML  = FXMLList->GetXML("001D3500-00005882-DC28");
  FCheckSchXML = FXMLList->GetXML("569E1EB2-D0CFEF0F-BE9A");

  FSQLCreator = new TdsDocSQLCreator(FXMLList);     /*FDM->ICSNom*/
  FSQLCreator->OnGetSVal = FDocGetSVal;
  FOnCreateUnitPlan = NULL;

  FImmNomQuery = CreateTempQuery();
  FClassDataQuery = CreateTempQuery();
  FClassData = new TdsRegClassData(FXMLList->GetXML("40381E23-92155860-4448"), FClassDataQuery);

  FImmNom = new TICSIMMNomDef(FImmNomQuery, FXMLList, "40381E23-92155860-4448");
}
//---------------------------------------------------------------------------
__fastcall TdsCommClassData::~TdsCommClassData()
{
  delete FImmNom;
  delete FImmNomQuery;

  delete FRegDef;
  delete FUnitDataDef;
//  delete FPlanOpt;
//  delete FVacSchXML;
 // delete FTestSchXML;
 // delete FCheckSchXML;
  for (TdsSrvClassifDataMap::iterator i = FClassifData.begin(); i != FClassifData.end(); i++)
   delete i->second;
  FClassifData.clear();
  delete FStoreMIBPList;
  delete FDefStoreMIBPList;
  delete FSortedStoreMIBPList;
  delete FDefSortedStoreMIBPList;
  delete FStoreTestList;
  delete FDefStoreTestList;

  delete FClassData;
  delete FClassDataQuery;
}
//---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsCommClassData::GetPlanTemplateDef(int AType)
{
  UnicodeString RC = "<root/>";
  try
   {
     UnicodeString FFN = icsPrePath(ExtractFilePath(ParamStr(0))+"\\template\\pl"+IntToStr(AType)+".xml");
     if (FileExists(FFN))
      {
        TTagNode *tmplNode = new TTagNode;
        try
         {
           tmplNode->LoadFromXMLFile(FFN);
           RC = tmplNode->AsXML;
         }
        __finally
         {
           delete tmplNode;
         }
      }
/*
     switch (AType)
     {
       case 0:
       { // �������
//                 <fl ref='0083' def='0' en='0'/>\

         RC = "<root>\
                 <fl ref='0023' def='0' en='0'/>\
                 <fl ref='00F1' req='1'/>\
                 <fl ref='00F9'/>\
                 <fl ref='00B3'/>\
                </root>";
         break;
       }
       case 1:
       case 2:
       case 3:
       case 4:
       {
         // �����������
//                 <fl ref='0083' def='0' en='0'/>\
         RC =  "<root>\
                 <fl ref='0023' def='1' req='1'/>\
                 <fl ref='011B'/>\
                 <fl ref='0026'/>\
                 <fl ref='0027'/>\
                 <fl ref='0028'/>\
                 <fl ref='0111'/>\
                 <fl ref='0112'/>\
                 <fl ref='0025'/>\
                 </root>";
         break;
       }
       case 5:
       {
         break;
       }
     }
*/
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetClassData(long ACode, UnicodeString AName)
{
  UnicodeString RC = "";
  try
   {
     RC = FClassData->GetData(AName, ACode);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsCommClassData::ClearHash()
{
  FClassData->ClearHash("");
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::Connect()
{
  bool RC = false;

  TFDQuery *FQ = CreateTempQuery();
  try
   {
     TIntMap *vti;
     // ***** ������ �������� + ���������� ���������� � ������������ ******
     if (quExec(FQ, false, "Select CODE as ps1, R003C as ps2 From CLASS_003A", "������ ��������"))
      {
        while (!FQ->Eof)
         {
           InfList[FQ->FieldByName("ps1")->AsString] = FQ->FieldByName("ps2")->AsString;
           //InfListCode[FQ->FieldByName("ps2")->AsString] = FQ->FieldByName("ps1")->AsString;
           FQ->Next();
         }
      }
     // ***** ������ ���� ******
     if (quExec(FQ, false, "Select CODE as ps1, R0040 as ps2 From CLASS_0035", "������ ����"))
      {
        while (!FQ->Eof)
         {
           VacList[FQ->FieldByName("ps1")->AsString] = FQ->FieldByName("ps2")->AsString;
           //VacListCode[FQ->FieldByName("ps2")->AsString] = FQ->FieldByName("ps1")->AsString;
           FQ->Next();
         }
      }
     // ***** ������ ���� ******
     if (quExec(FQ, false, "Select CODE as ps1, R002C as ps2 From CLASS_002A", "������ ����"))
      {
        while (!FQ->Eof)
         {
           TestList[FQ->FieldByName("ps1")->AsString] = FQ->FieldByName("ps2")->AsString;
           //TestListCode[FQ->FieldByName("ps2")->AsString] = FQ->FieldByName("ps1")->AsString;
           FQ->Next();
         }
      }
     // ***** ������ �������� ******
     if (quExec(FQ, false, "Select CODE as ps1, R013F as ps2 From CLASS_013E", "������ ��������"))
      {
        while (!FQ->Eof)
         {
           CheckList[FQ->FieldByName("ps1")->AsString] = FQ->FieldByName("ps2")->AsString;
           //CheckListCode[FQ->FieldByName("ps2")->AsString] = FQ->FieldByName("ps1")->AsString;
           FQ->Next();
         }
      }
     // ***** �������� �� ������ � ����� �� ��������� ******
     if (quExec(FQ, false, "Select R007B as ps1, R007A as ps2 From CLASS_0079", "�������� �� ������ � ����� �� ���������"))
      {
        while (!FQ->Eof)
         {
           if (InfProbList.find(FQ->FieldByName("ps1")->AsInteger) == InfProbList.end())
            InfProbList[FQ->FieldByName("ps1")->AsInteger] = new TIntMap;
           vti = InfProbList[FQ->FieldByName("ps1")->AsInteger];
           (*vti)[FQ->FieldByName("ps2")->AsInteger] = 1;

           if (ProbInfList.find(FQ->FieldByName("ps2")->AsInteger) == ProbInfList.end())
            ProbInfList[FQ->FieldByName("ps2")->AsInteger] = new TIntMap;
           vti = ProbInfList[FQ->FieldByName("ps2")->AsInteger];
           (*vti)[FQ->FieldByName("ps1")->AsInteger] = 1;

           FQ->Next();
         }
      }
     // ***** �������� �� �������� � ������� �� ��������� ******
     if (quExec(FQ, false, "Select R003E as Inf, R003B as MIBP From CLASS_002D", "�������� �� �������� � ������� �� ���������"))
      {
        while (!FQ->Eof)
         {
           if (InfVacList.find(FQ->FieldByName("Inf")->AsInteger) == InfVacList.end())
            InfVacList[FQ->FieldByName("Inf")->AsInteger] = new TIntMap;
           vti = InfVacList[FQ->FieldByName("Inf")->AsInteger];
           (*vti)[FQ->FieldByName("MIBP")->AsInteger] = 1;

           if (VacInfList.find(FQ->FieldByName("MIBP")->AsInteger) == VacInfList.end())
            VacInfList[FQ->FieldByName("MIBP")->AsInteger] = new TIntMap;
           vti = VacInfList[FQ->FieldByName("MIBP")->AsInteger];
           (*vti)[FQ->FieldByName("Inf")->AsInteger] = 1;
           FQ->Next();
         }
      }

     TTagNode *itPrior = FPlanOptDefSch;
     if (itPrior)
      {
        itPrior = itPrior->GetFirstChild();
        while(itPrior)
         {
           FVPrior[itPrior->AV["infref"].ToIntDef(0)]  = itPrior->AV["v"].ToIntDef(0);
           FRVPrior[itPrior->AV["infref"].ToIntDef(0)] = itPrior->AV["rv"].ToIntDef(0);
           itPrior = itPrior->GetNext();
         }
      }
     // ***** ������ ����/���������������� ���� ******
     if (quExec(FQ, false, "Select R006E as Inf, R006D as Test, R006F as Reak, R0070 as Actuals, R0076 as Kind, R0072 as Pause FROM CLASS_006C", "������ ����/���������������� ����"))
      {
        TPreTestItem FPreTest;
        while (!FQ->Eof)
         {
           FPreTest.Inf     = FQ->FieldByName("Inf")->AsInteger;
           FPreTest.Test    = FQ->FieldByName("Test")->AsInteger;
           FPreTest.Reak    = FQ->FieldByName("Reak")->AsInteger;
           FPreTest.Actuals = FQ->FieldByName("Actuals")->AsInteger;
           FPreTest.Kind    = (TPrePlanKind)FQ->FieldByName("Kind")->AsInteger;
           FPreTest.Pause   = FQ->FieldByName("Pause")->AsInteger;
           PreTestList.push_back(FPreTest);

           FQ->Next();
         }
      }
     RC = true;
   }
  __finally
   {
     if (FQ->Transaction->Active) FQ->Transaction->Commit();
     DeleteTempQuery(FQ);
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::InfInVac(UnicodeString AInf, UnicodeString AVac)
{ // �������� ��������� �������� AInf � ������ ���� AVac
  bool RC = false;
  try
   {
     TIntListMap::iterator FIRC = InfVacList.find(AInf.ToIntDef(0));
     if (FIRC != InfVacList.end())
      {
        TIntMap::iterator FVRC = FIRC->second->find(AVac.ToIntDef(0));
        RC =  (FVRC != FIRC->second->end());
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::GetMIBPInfList(int AMIBPCode, TIntMap *& ARC)
{ // ������ �������� ����
  bool RC = false;
  ARC = NULL;
  try
   {
     TIntListMap::iterator FIRC = VacInfList.find(AMIBPCode);
     if (FIRC != VacInfList.end())
      {
        ARC = FIRC->second;
        RC  = true;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
int __fastcall TdsCommClassData::GetMIBPInfCount(int AMIBPCode)
{ // ������ �������� ����
  int RC = 0;
  try
   {
     TIntListMap::iterator FIRC = VacInfList.find(AMIBPCode);
     if (FIRC != VacInfList.end())
      {
        RC = FIRC->second->size();
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::GetInfMIBPList(int AInfCode, TIntMap *& ARC)
{ // ������ ���� �� ��������
  bool RC = false;
  ARC = NULL;
  try
   {
     TIntListMap::iterator FIRC = InfVacList.find(AInfCode);
     if (FIRC != InfVacList.end())
      {
        ARC = FIRC->second;
        RC  = true;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::InfInPlanVac(TUnitPriorPrePlan *APlanVac, int AInfCode, int &AInfPrior, bool &AAlreadyInPlan)
{ // ������ ���� �� ��������
  bool RC = false;
  AInfPrior = -2;
  try
   {
//     dsPlanLogMessage("InfInPlanVac: "+IntToStr((int)APlanVac->size())+" : "+InfNameStr(AInfCode));
     TUnitPriorPrePlan::iterator FPItem;
     for (FPItem = APlanVac->begin(); (FPItem != APlanVac->end()) && !RC; FPItem++)
      {
//        dsPlanLogMessage("InfInPlanVac: "+InfNameStr(FPItem->second->Inf));
        AAlreadyInPlan = FPItem->second->InPlan;
        RC = (FPItem->second->Inf == AInfCode);
        if (RC) AInfPrior = FPItem->second->Prior;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::InfInTest(UnicodeString AInf, UnicodeString ATest)
{ // �������� ��������� �������� AInf � ������ �������� ATest
  bool RC = false;
  try
   {
     TIntListMap::iterator FIRC = InfProbList.find(AInf.ToIntDef(0));
     if (FIRC != InfProbList.end())
      {
        TIntMap::iterator FVRC = FIRC->second->find(ATest.ToIntDef(0));
        RC =  (FVRC != FIRC->second->end());
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::Disconnect()
{
  bool RC = false;
  try
   {
//     if(IcConnection->Connected) IcConnection->Connected = false;
//     Sleep(50);
//     if (DBManager->Active) DBManager->Active = false;
//     Sleep(50);
     RC = true;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsCommClassData::FSetRegDef(TTagNode *AVal)
{
  FRegDef->AsXML = AVal->AsXML;

  TTagNode *FUnitDef = FRegDef->GetTagByUID("1000");
  if (FUnitDef)
   {
     FUnitDataDef->Name = "fld";

     UnicodeString _EMPTYSTR_;
     FUnitDataDef->AddChild("code");
     FUnitDataDef->AddChild("data");
     FUnitDef->Iterate(FSetFieldsDef, _EMPTYSTR_);
   }
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::FSetFieldsDef(TTagNode *ANode, UnicodeString &ATmp)
{
  if (ANode->CmpName("binary,choice,choiceDB,choiceTree,text,date,extedit,digit"))
   FUnitDataDef->AddChild()->Assign(ANode,true);
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsCommClassData::GetSQLs(UnicodeString AId)
{
  TdsSrvClassifSQLData *tmpData = new TdsSrvClassifSQLData;
  try
   {
     GetClassifSQLData(FRegDef, AId, tmpData);
     FClassifData[AId] = tmpData;
   }
  catch(Exception &E)
   {
     dsPlanLogError(E.Message+"; ID = "+AId, __FUNC__);
     throw E;
   }
/*
  TdsSrvClassif *FSQLs = new TdsSrvClassif( "", "");
  try
   {
     FSelectSQLMap[AId]    = FSQLs->SQLs->SelectSQL;
     FClassSQLMap[AId]     = FSQLs->SQLs->ClassSQL;
     FCountSQLMap[AId]     = FSQLs->SQLs->CountSQL;
     FDeleteSQLMap[AId]    = FSQLs->SQLs->DeleteSQL;
     FSelectRecSQLMap[AId] = FSQLs->SQLs->SelectRecSQL;
     FKeyFlMap[AId]        = FSQLs->SQLs->KeyFl;
     FFields[AId] = new TStringList;
     FFields[AId]->Text = FSQLs->SQLs->Fields->Text;
   }
  __finally
   {
     delete FSQLs;
   }
   */
}
//---------------------------------------------------------------------------
void __fastcall TdsCommClassData::CheckSQLs(UnicodeString AId)
{
  try
   {
     TdsSrvClassifDataMap::iterator FRC = FClassifData.find(AId);
     if (FRC == FClassifData.end())
       GetSQLs(AId);
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetKeyFl(UnicodeString AId)
{
  UnicodeString RC = "";

  try
   {
     CheckSQLs(AId);
     RC = FClassifData[AId]->KeyFl;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetFlNameById(UnicodeString ATabId, UnicodeString AFlId)
{
  UnicodeString RC = "";

  try
   {
     CheckSQLs(ATabId);
     RC = FClassifData[ATabId]->FieldNameById(AFlId);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetTabName(UnicodeString AId)
{
  UnicodeString RC = "";

  try
   {
     CheckSQLs(AId);
     RC = FClassifData[AId]->TabName;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetCountSQL(UnicodeString AId)
{
  UnicodeString RC = "";

  try
   {
     CheckSQLs(AId);
     RC = FClassifData[AId]->CountSQL;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetDeleteSQL(UnicodeString AId)
{
  UnicodeString RC = "";

  try
   {
     CheckSQLs(AId);
     RC = FClassifData[AId]->DeleteSQL;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetClassSQL(UnicodeString AId)
{
  UnicodeString RC = "";

  try
   {
     CheckSQLs(AId);
     RC = FClassifData[AId]->ClassSQL;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetSelectSQL(UnicodeString AId)
{
  UnicodeString RC = "";

  try
   {
     CheckSQLs(AId);
     RC = FClassifData[AId]->SelectSQL;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetOrderSQL(UnicodeString AId)
{
  UnicodeString RC = "";

  try
   {
     CheckSQLs(AId);
     RC = FClassifData[AId]->OrderSQL;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetSelectRecSQL(UnicodeString AId)
{
  UnicodeString RC = "";

  try
   {
     CheckSQLs(AId);
     RC = FClassifData[AId]->SelectRecSQL;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetSelectRec10SQL(UnicodeString AId)
{
  UnicodeString RC = "";

  try
   {
     CheckSQLs(AId);
     RC = FClassifData[AId]->SelectRec10SQL;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::IsQuotedKey(UnicodeString AId)
{
  bool RC = false;

  try
   {
     CheckSQLs(AId);
     RC = FClassifData[AId]->QuotedKey;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TStringList* __fastcall TdsCommClassData::GetFieldsList(UnicodeString AId)
{
  TStringList* RC = NULL;

  try
   {
     CheckSQLs(AId);
     RC = FClassifData[AId]->Fields;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
__int64 __fastcall TdsCommClassData::GetCount(UnicodeString AId, System::UnicodeString AFilterParam)
{
  __int64 RC = 0;
  TFDQuery *FQ = CreateTempQuery();
  TJSONObject *tmpFlData = NULL;
  TTagNode *tmpFlt = NULL;
  try
   {
     bool FFiltered = false;
     if (AFilterParam.Length())
      {
        if (AFilterParam[1]=='{')
         {
           tmpFlData = (TJSONObject*)TJSONObject::ParseJSONValue(AFilterParam);
           FFiltered = true;
         }
        else if (AFilterParam.Pos("<?xml"))
         {
           tmpFlt = new TTagNode;
           tmpFlt->AsXML = AFilterParam;
           FFiltered = true;
         }
      }

     if (FFiltered)
      {
        if (tmpFlt)
         {
           if (quExec(FQ, false, GetCountSQL(AId)+" where "+GetWhereFromFilter (tmpFlt, "0E291426-00005882-2493.0000")))
            RC = FQ->FieldByName("RCount")->AsInteger;
         }
        else
         {
           UnicodeString FFilterWhere = "";
           TJSONPairEnumerator *itPair = tmpFlData->GetEnumerator();
           while (itPair->MoveNext())
            {
              if (FFilterWhere.Length()) FFilterWhere += " and ";
              FFilterWhere += " r"+((TJSONString*)itPair->Current->JsonString)->Value()+"="+((TJSONString*)itPair->Current->JsonValue)->Value();
            }
           if (quExec(FQ, false, GetCountSQL(AId)+" where "+FFilterWhere))
            RC = FQ->FieldByName("RCount")->AsInteger;
         }
      }
     else
      {
        if (quExec(FQ, false, GetCountSQL(AId)))
         RC = FQ->FieldByName("RCount")->AsInteger;
      }
   }
  __finally
   {
     if (FQ->Transaction->Active) FQ->Transaction->Commit();
     DeleteTempQuery(FQ);
   }
  return RC;
}
//---------------------------------------------------------------------------
TJSONObject* __fastcall TdsCommClassData::GetIdList(System::UnicodeString AId, int AMax, System::UnicodeString AFilterParam)
{
  TJSONArray* RC = new TJSONArray;
  TFDQuery *FQ = CreateTempQuery();
  TJSONObject *tmpFlData = NULL;
  TTagNode *tmpFlt = NULL;
  try
   {
     bool FFiltered = false;
     if (AFilterParam.Length())
      {
        if (AFilterParam[1]=='{')
         {
           tmpFlData = (TJSONObject*)TJSONObject::ParseJSONValue(AFilterParam);
           FFiltered = true;
         }
        else if (AFilterParam.Pos("<?xml"))
         {
           tmpFlt = new TTagNode;
           tmpFlt->AsXML = AFilterParam;
           FFiltered = true;
         }
      }
     UnicodeString FSQL = "";
     if (FFiltered)
      {
        if (tmpFlt)
         FSQL = GetSelectSQL(AId)+" where "+GetWhereFromFilter (tmpFlt, "0E291426-00005882-2493.0000");
        else
         {
           UnicodeString FFilterWhere = "";
           TJSONPairEnumerator *itPair = tmpFlData->GetEnumerator();
           while (itPair->MoveNext())
            {
              if (FFilterWhere.Length()) FFilterWhere += " and ";
              FFilterWhere += " r"+((TJSONString*)itPair->Current->JsonString)->Value()+"="+((TJSONString*)itPair->Current->JsonValue)->Value();
            }
           FSQL = GetSelectSQL(AId)+" where "+FFilterWhere;
         }
      }
     else
       FSQL = GetSelectSQL(AId);

     if (GetOrderSQL(AId).Length())
       FSQL += " Order by "+GetOrderSQL(AId);

     if (quExec(FQ, false, FSQL))
      {
        while (!FQ->Eof)
         {
           RC->Add(FQ->FieldByName(GetKeyFl(AId))->AsString);
           FQ->Next();
         }
      }
   }
  __finally
   {
     DeleteTempQuery(FQ);
     if (tmpFlt) delete tmpFlt;
   }
  return (TJSONObject*)RC;
}
//---------------------------------------------------------------------------
TJSONObject* __fastcall TdsCommClassData::GetValById(System::UnicodeString AId, System::UnicodeString ARecId)
{
  TJSONObject* RC = new TJSONObject;
  TFDQuery *FQ = CreateTempQuery();
  try
   {
     if (!AId.Pos("."))
      { // �������� ��� ������
        if (quExecParam(FQ, false, GetSelectRecSQL(AId), "p"+GetKeyFl(AId), ARecId))
         {
           if (FQ->RecordCount)
            {
              TTagNode *flDef;
              UnicodeString FId, FFN;
              TStringList* FFlList = GetFieldsList(AId);
              RC->AddPair(GetKeyFl(AId),ARecId);
              for (int i = 0; i < FFlList->Count; i++)
               {
                 FId = FFlList->Strings[i];
                 flDef = FRegDef->GetTagByUID(FId);
                 FFN = "R"+FId;
                 if (flDef->AV["fl"].Length())
                  FFN = flDef->AV["fl"];
                 if (FQ->FieldByName(FFN)->IsNull)
                  {
                    RC->AddPair(FId, new TJSONNull);
                  }
                 else
                  {
                    if (flDef->CmpName("choice, choiceDB"))
                     {
                       RC->AddPair(FId,TJSONObject::ParseJSONValue("{\""+FQ->FieldByName(FFN)->AsString+"\":\""+FQ->FieldByName(FFN+"Str")->AsString+"\"}"));
                     }
                    else if (flDef->CmpName("binary"))
                     {
                       RC->AddPair(FId,TJSONObject::ParseJSONValue("{\""+FQ->FieldByName(FFN)->AsString+"\":\""+UnicodeString((FQ->FieldByName(FFN)->AsInteger)?"+":"")+"\"}"));
                     }
                    else if (flDef->CmpName("choiceTree, extedit"))
                     {
                       if (!(flDef->CmpName("extedit") && flDef->CmpAV("fieldtype","integer") && (flDef->AV["length"].ToIntDef(0) > 64)))
                        RC->AddPair(FId,TJSONObject::ParseJSONValue("{\""+FQ->FieldByName(FFN)->AsString+"\":\""+FQ->FieldByName(FFN)->AsString+"\"}"));
                     }
                    else
                     RC->AddPair(FId, FQ->FieldByName(FFN)->AsString);
                  }
               }
            }
         }
      }
     else
      { // �������� ����
        // uid.uid
        UnicodeString FCmd   =  GetLPartB(AId).LowerCase();
        UnicodeString FRefId =  GetRPartB(AId);
        if (FCmd == "f")
         { // �������� �������� ���� Id2, �������������� Id1 � �����  ARecId
           TTagNode *flDef;
           UnicodeString Id1, Id2, FTab, FFN;
           Id1 =  GetLPartB(FRefId).LowerCase();
           Id2 =  GetRPartB(FRefId).LowerCase();
           flDef = FRegDef->GetTagByUID(Id1);
           if (flDef)
            {
              FTab = "class_"+flDef->AV["uid"];
              if (flDef->AV["tab"].Length())
                FTab = flDef->AV["tab"];

              flDef = FRegDef->GetTagByUID(Id2);
              if (flDef)
               {
                 FFN = "R"+flDef->AV["uid"];
                 if (flDef->AV["fl"].Length())
                   FFN = flDef->AV["fl"];

                 if (quExecParam(FQ, false, "Select ("+FFN+") as FlVal From "+FTab+" Where "+GetKeyFl(Id1)+"=:FCode", "FCode" ,ARecId))
                  {
                    if (FQ->FieldByName("FlVal")->IsNull)
                     RC->AddPair(Id2, new TJSONNull);
                    else
                     RC->AddPair(Id2, FQ->FieldByName("FlVal")->AsVariant);
                  }
               }
            }
         }
        else if (FCmd == "l")
         { //������ ���� ���.������
           if (quExec(FQ, false, GetClassSQL(FRefId)+" Order by name"))
            {
              while (!FQ->Eof)
               {
                 RC->AddPair(FQ->FieldByName(GetKeyFl(FRefId))->AsString, FQ->FieldByName("name")->AsString);
                 FQ->Next();
               }
            }
         }
        else if (FCmd == "d")
         { //������ ���� ���.������ � �������������
           TTagNode *FClDef = FRegDef->GetTagByUID(GetLPartB(FRefId,':'));
           if (FClDef)
            {
              UnicodeString FSQL = GetDependValSQL(FRefId, FClDef->CmpAV("comment","empty"));
              if (FSQL.Length())
               FSQL += " Order by name";
              if (FSQL.Length() && FClDef)
               {
                 if (quExec(FQ, false, FSQL))
                  {
                    while (!FQ->Eof)
                     {
                       RC->AddPair(FQ->FieldByName(GetKeyFl(FClDef->AV["ref"]))->AsString, FQ->FieldByName("name")->AsString);
                       FQ->Next();
                     }
                  }
               }
            }
         }
        else if (FCmd == "s2")
         { //������ ���� ���.������ � �������������
           UnicodeString FSQL = FRefId;
           if (FSQL.Length())
            {
              if (quExec(FQ, false, FSQL))
               {
                 while (!FQ->Eof)
                  {
                    RC->AddPair(FQ->FieldByName("ps1")->AsString, FQ->FieldByName("ps2")->AsString);
                    FQ->Next();
                  }
               }
            }
         }
        else if (FCmd == "s3")
         { //������ ���� ���.������ � �������������
           UnicodeString FSQL = FRefId;
           if (FSQL.Length())
            {
              if (quExec(FQ, false, FSQL))
               {
                 while (!FQ->Eof)
                  {
                    RC->AddPair(FQ->FieldByName("ps1")->AsString, FQ->FieldByName("ps2")->AsString+":"+FQ->FieldByName("ps3")->AsString);
                    FQ->Next();
                  }
               }
            }
         }
        else if (FCmd == "s4")
         { //������ ���� ���.������ � �������������
           UnicodeString FSQL = FRefId;
           if (FSQL.Length())
            {
              if (quExec(FQ, false, FSQL))
               {
                 while (!FQ->Eof)
                  {
                    RC->AddPair(FQ->FieldByName("ps1")->AsString, FQ->FieldByName("ps2")->AsString+":"+FQ->FieldByName("ps3")->AsString+"#"+FQ->FieldByName("ps4")->AsString);
                    FQ->Next();
                  }
               }
            }
         }
      }
   }
  __finally
   {
     DeleteTempQuery(FQ);
   }
  return RC;
}
//---------------------------------------------------------------------------
TJSONObject* __fastcall TdsCommClassData::GetValById10(System::UnicodeString AId, System::UnicodeString ARecId)
{
  TJSONObject* RC = new TJSONObject;
  TJSONObject* RowRC;
  UnicodeString FRecId = ARecId;
  TFDQuery *FQ = CreateTempQuery();
  try
   {
     if (!AId.Pos("."))
      { // �������� ��� ������
//*******************
        System::Sysutils::TReplaceFlags flRep;
        flRep << rfReplaceAll << rfIgnoreCase;
        if (IsQuotedKey(AId))
         {
           TStringList *QL = new TStringList;
           QL->Delimiter = ',';
           QL->DelimitedText = FRecId;
           FRecId = "";
           try
            {
              for (int i = 0; i < QL->Count; i++)
               {
                 if (FRecId.Length()) FRecId += ",";
                 FRecId += "'"+QL->Strings[i].Trim()+"'";
               }
            }
           __finally
            {
              delete QL;
            }
         }
//*******************
        if (quExec(FQ, false, StringReplace(GetSelectRec10SQL(AId), "_KEY_REP_VAL_", FRecId, flRep)))
         {
           TTagNode *flDef;
           UnicodeString FId, FFN;
           TStringList* FFlList = GetFieldsList(AId);
           while (!FQ->Eof)
            {
              RowRC = new TJSONObject;
              RowRC->AddPair("code", FQ->FieldByName(GetKeyFl(AId))->AsString);
              for (int i = 0; i < FFlList->Count; i++)
               {
                 FId = FFlList->Strings[i];
                 flDef = FRegDef->GetTagByUID(FId);
                 FFN = "R"+FId;
                 if (flDef->AV["fl"].Length())
                  FFN = flDef->AV["fl"];
                 if (FQ->FieldByName(FFN)->IsNull)
                  {
                    RowRC->AddPair(FId, new TJSONNull);
                  }
                 else
                  {
                    if (flDef->CmpName("choice, choiceDB"))
                     {
                       RowRC->AddPair(FId,TJSONObject::ParseJSONValue("{\""+FQ->FieldByName(FFN)->AsString+"\":\""+FQ->FieldByName(FFN+"Str")->AsString+"\"}"));
                     }
                    else if (flDef->CmpName("binary"))
                     {
                       RowRC->AddPair(FId,TJSONObject::ParseJSONValue("{\""+FQ->FieldByName(FFN)->AsString+"\":\""+UnicodeString((FQ->FieldByName(FFN)->AsInteger)?"+":"")+"\"}"));
                     }
                    else if (flDef->CmpName("choiceTree, extedit"))
                     {
                       if (!(flDef->CmpName("extedit") && flDef->CmpAV("fieldtype","integer") && (flDef->AV["length"].ToIntDef(0) > 64)))
                        RowRC->AddPair(FId,TJSONObject::ParseJSONValue("{\""+FQ->FieldByName(FFN)->AsString+"\":\""+FQ->FieldByName(FFN)->AsString+"\"}"));
//                        RowRC->AddPair(FId,/*TJSONObject::ParseJSONValue(*/GetExtEditVal(flDef, ARecId, FQ->FieldByName(FFN)->AsString, FQ)/*)*/);
                     }
                    else
                     RowRC->AddPair(FId, FQ->FieldByName(FFN)->AsString);
                  }
               }
              RC->AddPair(GetKeyFl(AId), RowRC);
              FQ->Next();
            }
/*
           if (FQ->RecordCount)
            {
              TTagNode *flDef;
              UnicodeString FId, FFN;
              TStringList* FFlList = GetFieldsList(AId);
              for (int i = 0; i < FFlList->Count; i++)
               {
                 FId = FFlList->Strings[i];
                 flDef = FRegDef->GetTagByUID(FId);
                 FFN = "R"+FId;
                 if (flDef->AV["fl"].Length())
                  FFN = flDef->AV["fl"];
                 if (FQ->FieldByName(FFN)->IsNull)
                  {
                    RC->AddPair(FId, new TJSONNull);
                  }
                 else
                  {
                    if (flDef->CmpName("choice, choiceDB"))
                     {
                       RC->AddPair(FId,TJSONObject::ParseJSONValue("{\""+FQ->FieldByName(FFN)->AsString+"\":\""+FQ->FieldByName(FFN+"Str")->AsString+"\"}"));
                     }
                    else if (flDef->CmpName("binary"))
                     {
                       RC->AddPair(FId,TJSONObject::ParseJSONValue("{\""+FQ->FieldByName(FFN)->AsString+"\":\""+UnicodeString((FQ->FieldByName(FFN)->AsInteger)?"+":"")+"\"}"));
                     }
                    else if (flDef->CmpName("choiceTree, extedit"))
                     {
                       if (!(flDef->CmpName("extedit") && flDef->CmpAV("fieldtype","integer") && (flDef->AV["length"].ToIntDef(0) > 64)))
                        RC->AddPair(FId,TJSONObject::ParseJSONValue("{\""+FQ->FieldByName(FFN)->AsString+"\":\""+FQ->FieldByName(FFN)->AsString+"\"}"));
                     }
                    else
                     RC->AddPair(FId, FQ->FieldByName(FFN)->AsString);
                  }
               }
              RC->AddPair(GetKeyFl(AId),ARecId);
            }
*/
         }
      }
     else
      { // �������� ����
        dsPlanLogError("error ID: "+AId, __FUNC__);
      }
   }
  __finally
   {
     DeleteTempQuery(FQ);
   }
  return RC;
}
//---------------------------------------------------------------------------
TJSONObject* __fastcall TdsCommClassData::Find(System::UnicodeString AId, TJSONObject *AFindParam)
{
  TJSONObject* RC;
//  TdsSrvClassif *FSQLs = new TdsSrvClassif(FRegDef, AId, "", "");

  try
   {
//     RC = (TJSONObject*)TJSONObject::ParseJSONValue(UnicodeString("[1,2,3,4,5]"));
//     quExec(GetCountSQL(AId));
//     RC = (TJSONObject*)TJSONObject::ParseJSONValue("{\"code\":"+ARecId+",\"003D\":{\"5\":\"����\"},\"002C\":\"����\",\"0062\":{\"3\":\"�������\"},\"0063\":0.5,\"0069\":1,\"0067\":{\"3\":\"�������\"},\"0068\":100,\"006B\":1,\"00C6\":1}");
//     RC = quFree->FieldByName("RCount")->AsInteger;
//     FSQLs->SQLs;
//     RC = GetCount(AId);
   }
  __finally
   {
//     delete FSQLs;
   }
  return RC;
}
//---------------------------------------------------------------------------
TFDQuery* __fastcall TdsCommClassData::CreateTempQuery()
{
  TFDQuery* FQ    = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
     FQ->Transaction->Connection = FConnection;
     FQ->Connection = FConnection;

     FQ->Transaction->Options->AutoStart = false;
     FQ->Transaction->Options->AutoStop = false;
     FQ->Transaction->Options->AutoCommit = false;
     FQ->Transaction->Options->StopOptions.Clear();

   }
  __finally
   {
   }
  return FQ;
}
//---------------------------------------------------------------------------
void __fastcall TdsCommClassData::DeleteTempQuery(TFDQuery *AQuery)
{
  delete AQuery->Transaction;
  delete AQuery;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
{
  bool RC = false;
  TTime FBeg = Time();
  try
   {
     try
      {
        AQuery->Close();
        if (!AQuery->Transaction->Active) AQuery->Transaction->StartTransaction();
        AQuery->SQL->Text = ASQL;
        AQuery->OpenOrExecute();
        if (ACommit) AQuery->Transaction->Commit();
        RC = true;
      }
     catch(System::Sysutils::Exception &E)
      {
        dsPlanLogError(E.Message+"; SQL = "+ASQL, __FUNC__);
      }
   }
  __finally
   {
     if (ALogComment.Length())
      dsLogSQL(ALogComment, __FUNC__, FBeg - Time(), 5);
     else
      dsLogSQL(ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
UnicodeString AParam1Name, UnicodeString AParam1Val,
UnicodeString ALogComment,
UnicodeString AParam2Name, Variant AParam2Val,
UnicodeString AParam3Name, Variant AParam3Val,
UnicodeString AParam4Name, Variant AParam4Val)
{
  bool RC = false;
  TTime FBeg = Time();
  try
   {
     try
      {
        AQuery->Close();
        if (!AQuery->Transaction->Active) AQuery->Transaction->StartTransaction();
        AQuery->SQL->Text = ASQL;
        AQuery->Prepare();
        AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
        if (AParam2Name.Length())
         AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
        if (AParam3Name.Length())
         AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
        if (AParam4Name.Length())
         AQuery->ParamByName(AParam4Name)->Value = AParam4Val;
        AQuery->OpenOrExecute();
        if (ACommit) AQuery->Transaction->Commit();
        RC = true;
      }
     catch(System::Sysutils::Exception &E)
      {
        UnicodeString FParams = "; Params: ";
        if (AParam1Name.Length()) FParams += AParam1Name+"="+VarToStrDef(AParam1Val, "")+"; ";
        if (AParam2Name.Length()) FParams += AParam2Name+"="+VarToStrDef(AParam2Val, "")+"; ";
        if (AParam3Name.Length()) FParams += AParam3Name+"="+VarToStrDef(AParam3Val, "")+"; ";
        if (AParam4Name.Length()) FParams += AParam4Name+"="+VarToStrDef(AParam4Val, "");

        dsLogError(E.Message+"; SQL = "+ASQL+FParams, __FUNC__);
      }
   }
  __finally
   {
     if (ALogComment.Length())
      dsLogSQL(ALogComment, __FUNC__, FBeg - Time(), 5);
     else
      dsLogSQL(ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsCommClassData::GetDependClassCode(UnicodeString ADepClName, UnicodeString ADepFlVal, UnicodeString ARetFlName, UnicodeString &ARetVals)
{
//  TFDQuery *FQ = CreateTempQuery();
//     DeleteTempQuery(FQ);
  TFDQuery *FQ = CreateTempQuery();
  try
   {
     if (quExec(FQ, false, "Select "+ARetFlName+" From "+ADepClName+" Where "+ADepFlVal))
      {
        UnicodeString FRetVals = "";
        int FCount = 0;
        while (!FQ->Eof && (FCount < 200))
         {
           FRetVals += FQ->FieldByName(ARetFlName)->AsString + ",";
           FCount++;
           FQ->Next();
         }
        if (FCount >=199)
         {
           FRetVals = "Select "+ARetFlName+" From "+ADepClName+" Where "+ADepFlVal;
         }
        else
         {
           if (!FRetVals.Length()) FRetVals = "-1";
           else                    FRetVals[FRetVals.Length()] = ' ';
         }
        ARetVals = FRetVals;
      }
   }
  __finally
   {
     DeleteTempQuery(FQ);
   }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::FGetDepVal(UnicodeString AVals, UnicodeString AUID )
{
  UnicodeString RC = "";
  try
   {
     UnicodeString FDep, FDepList;
     FDepList = AVals;
     if (FDepList.Length() >= 6)
      {
        while (FDepList.Length() && !RC.Length())
         {
           FDep  = GetLPartB(FDepList,';').Trim();
           if (!FDep.Length())
            FDep = FDepList;
           if (GetLPartB(FDep,'=').Trim().UpperCase() == AUID.UpperCase())
             RC = GetRPartB(FDep,'=').Trim();
           FDepList = GetRPartB(FDepList,';').Trim();
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::FDepValIsNull(UnicodeString AVals)
{
  bool RC = true;
  try
   {
     UnicodeString FDep, FDepList;
     FDepList = AVals;
     if (FDepList.Length() >= 6)
      {
        while (FDepList.Length() && RC)
         {
           FDep  = GetLPartB(FDepList,';').Trim();
           if (!FDep.Length())
            FDep = FDepList;
           RC &= !GetRPartB(FDep,'=').Trim().Length();
           FDepList = GetRPartB(FDepList,';').Trim();
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetDependValSQL(UnicodeString ARef, bool AEmptyIfNoDepend)
{
  UnicodeString RC = "";
  try
   {
     //  FCtrlOwner->FillClass("d."+FNode->AV["ref"]+":"+FNode->AV["depend"]+":"+ADepClCode+":"+ADepUID, (TStringList*)ED->Properties->Items, ACode);
     //  UnicodeString FRef = ARef;
     UnicodeString FNodeUid = GetLPartB(ARef,':');     // uid choiceDB ������� �����������
     UnicodeString FDepVals = GetRPartB(ARef,':');     // �������� ��������� �� ������� ������� � ������� "UID=��������;"
     TTagNode *FDef = FRegDef->GetTagByUID(FNodeUid);
     if (FDef)  // ���� choiceDB
      {
        UnicodeString FTabPref = "";
        UnicodeString FNodeRef = FDef->AV["ref"];               // ������� �� �������������, �� �������� ���� ������
        UnicodeString FNodeDepend = FDef->AV["depend"]+";";     // ������ ������������ � ������� uid-��������.id-�������������� � ������� ��������� �����������

   //     FRef = GetRPartB(FRef,':');
   //     UnicodeString ADepClCode = GetLPartB(FRef,':').UpperCase();
   //     UnicodeString ADepUID = GetRPartB(FRef,':').UpperCase();

        UnicodeString FDep,FDepList;
        FDepList = FNodeDepend;
        if (FDepList.Length() >= 9)
         {
           UnicodeString FDepTab, FDepWhereVal, FRetFlName, FRetVals;
           if (!FDepValIsNull(FDepVals))
            {
              UnicodeString FDepUID;
              TTagNode *FDepClDef, *FDepNode, *_tmp;
              FDepTab = ("CLASS_"+FTabPref+FNodeRef).UpperCase();
              FDepClDef = FRegDef->GetTagByUID(FNodeRef);
              bool IsEquDepend = true;
              while (FDepList.Length() && IsEquDepend)
               {
                 FDep  = GetLPartB(FDepList,';').UpperCase();
                 IsEquDepend &= (FDepTab == "CLASS_"+FTabPref+_UID(FDep));

                 FDepList = GetRPartB(FDepList,';').Trim();
               }
              FDepList = FNodeDepend;
              UnicodeString FCtrlUID,FDepCode, FDepClCode, FFlName;
              if (IsEquDepend )
               { // ��� ����������� � ����� �������, ����� ������� ����� where
                 FRetVals = "";
                 FDepWhereVal = "";
                 while (FDepList.Length())
                  {
                    FDep  = GetLPartB(FDepList,';').Trim();
                    FCtrlUID = _GUI(FDep);
                    _tmp = FRegDef->GetTagByUID(FCtrlUID);
                    FDepClCode = _tmp->AV["ref"];
                    FDepCode   = FGetDepVal(FDepVals, FCtrlUID);
                    FDepNode = FDepClDef->GetChildByAV("","ref",FDepClCode);

                    if (FDepNode)
                     {
                       FFlName = "Cl0"+FNodeRef+".R"+FDepNode->AV["uid"];
                       if (FDepCode.Length() && (FDepCode.ToIntDef(-1) != -2))
                        {
                          if (FDepWhereVal.Length())
                           FDepWhereVal += " and ";
                          FDepWhereVal += FFlName+"="+FDepCode;
                        }
                       else
                        {
                          if (FDepWhereVal.Length())
                           FDepWhereVal += " and ";
                          FDepWhereVal += " ("+FFlName+"=0 or "+FFlName+" is null)";
                        }
                     }
                    FDepList = GetRPartB(FDepList,';').Trim();
                  }
               }
              else
               {
                 FDepList = FNodeDepend;
                 while (FDepList.Length())
                  {
                    FDep  = GetPart1(FDepList,';').Trim();
                    if (!FDep.Length())
                     FDep = FDepList;

                    // FDep � ������� uid-��������.id-�������������� � ������� ��������� �����������
                    FCtrlUID = _GUI(FDep);          // FCtrlUID - uid ������� �� �������� ������� ��������
                    FDepCode = FGetDepVal(FDepVals, FCtrlUID);
                    if (FDepCode.Length())
                     {
                       FDepUID = _UID(FDep);                // FDepUID uid �������������� � ������� ��������� �����
                       FDepTab = "CLASS_"+FTabPref+FDepUID;

                       FDepClDef = FRegDef->GetTagByUID(FDepUID);  // ���� �������������� � ������� ��������� �����

                       FDepClCode = FRegDef->GetTagByUID(FCtrlUID)->AV["ref"];                // � ������ ����� �������������� FDepClDef
                       FDepNode = FDepClDef->GetChildByAV("","ref",FDepClCode/*ADepClCode*/); // ���������� ���� � ref= ������_��_��������_�������_��������->AV["ref"]
                                                                                              // ���� � ���, ��� ������������ ����� ������������ �� ���������� ������
                       FRetVals = "";

                       if (FDepCode.ToIntDef(-1) != -2) // ���������, ������ ����� ???
                        {
                          if (FDepCode.ToIntDef(-1) == -1)
                           {
                             FDepWhereVal = "(R"+FDepNode->AV["uid"]+" is NULL or R"+FDepNode->AV["uid"]+"=0)";
                             FRetFlName   = "CODE";
                           }
                          else
                           {
                             if (FDepUID == FDepClCode/*ADepClCode*/)   // ��� �������� �������� ����������, ��� ����� ���� ���� ��������� ���������������
                              {
                                FDepWhereVal = "CODE="+FDepCode;
                              }
                             else if (FDepUID == FNodeRef)   // ����������� ��������� � �������������� �������� �� �������� ������� ��������
                              {
                                FDepWhereVal = "R"+FDepNode->AV["uid"]+"="+FDepCode;
                                FRetFlName   = "CODE";
                              }
                             else             // ����������� ��������� � ��������� ��������������
                              {
                                FDepWhereVal = "R"+FDepNode->AV["uid"]+"="+FDepCode;
                                FRetFlName   = "R"+FDepClDef->GetChildByAV("","ref",FNodeRef)->AV["uid"];
                              }
                           }
                          GetDependClassCode(FDepTab, FDepWhereVal, FRetFlName, FRetVals);
                          FDepWhereVal = "code in ("+FRetVals+")";
                        }
                     }
                    FDepList = GetRPartB(FDepList,';').Trim();
                  }
               }
            }
           if (FDepWhereVal.Length())
            RC = GetClassSQL(FNodeRef)+" Where "+FDepWhereVal;//     InvadeClassCB(cBox(0),0, false, cNode(0),quFree,FTabPref,FRetVals,FCtrlOwner->DefXML); //+++
           else
            {
              if (!AEmptyIfNoDepend)
               RC = GetClassSQL(FNodeRef);//     InvadeClassCB(cBox(0),0, false, cNode(0),quFree,FTabPref,FRetVals,FCtrlOwner->DefXML); //+++
            }
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsCommClassData::DeleteData(System::UnicodeString AId, System::UnicodeString ARecId)
{
  UnicodeString RC = "error";
  TFDQuery *FQ = CreateTempQuery();
  try
   {
     try
      {
        if (quExecParam(FQ, true, GetDeleteSQL(AId),"p"+GetKeyFl(AId),ARecId))
         RC = "ok";
      }
     catch(Exception &E)
      {
        RC = E.Message;
      }
   }
  __finally
   {
     DeleteTempQuery(FQ);
   }
  return RC;
}
//----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsCommClassData::DeletePlan(System::UnicodeString ARecId)
{
  return DeleteData("3188", ARecId);
}
//----------------------------------------------------------------------------
System::UnicodeString  __fastcall TdsCommClassData::GetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, UnicodeString &AMsg)
{
  UnicodeString RC = "";
  AMsg = "error";
  TFDQuery *FQ = CreateTempQuery();
  try
   {
     try
      {
        if (quExecParam(FQ, false, "Select "+GetFlNameById(AId, AFlId)+" From "+GetTabName(AId)+" where "+GetKeyFl(AId)+"=:pKey", "pKey", ARecId))
         {
           RC = FQ->FieldByName(GetFlNameById(AId, AFlId))->AsString;
           AMsg = "ok";
         }
      }
     catch(Exception &E)
      {
        RC = E.Message;
      }
   }
  __finally
   {
     DeleteTempQuery(FQ);
   }
  return RC;
}
//----------------------------------------------------------------------------
System::UnicodeString  __fastcall TdsCommClassData::SetTextData(System::UnicodeString AId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString AVal)
{
  UnicodeString RC = "error";
  TFDQuery *FQ = CreateTempQuery();
  try
   {
     try
      {
        if (quExecParam(FQ, true, "Update "+GetTabName(AId)+" set "+GetFlNameById(AId, AFlId)+"=:pFl where "+GetKeyFl(AId)+"=:pKey","pFl", AVal, "", "pKey", ARecId))
          RC = "ok";
      }
     catch(Exception &E)
      {
        RC = E.Message;
      }
   }
  __finally
   {
     if (FQ->Transaction->Active) FQ->Transaction->Commit();
     DeleteTempQuery(FQ);
   }
  return RC;
}
//----------------------------------------------------------------------------
void __fastcall XMLStrToDuration(UnicodeString AValue, int *AYears, int *AMonths, int *ADays)
{
  AValue = AValue.Trim();
  const UnicodeString csErrMsg = "\"" + AValue + "\" is not a valid duration.";
  if (!AValue.Length())
    throw EConvertError(csErrMsg);

  TStrings *sl = new TStringList;
  try
  {
    try
    {
      sl->Text = StringReplace(AValue, cchXMLDurationSeparator, "\n", TReplaceFlags() << rfReplaceAll);
      if (sl->Count < 3)
        throw EConvertError(csErrMsg);
      *AYears  = sl->Strings[0].ToInt();
      *AMonths = sl->Strings[1].ToInt();
      *ADays   = sl->Strings[2].ToInt();
    }
    catch(EConvertError&)
    {
      throw EConvertError(csErrMsg);
    }
  }
  __finally
  {
    delete sl;
  }
}

//---------------------------------------------------------------------------
TDate __fastcall IncDate(TDate ABase, UnicodeString APeriod)
{
  TDate RC = 0;
  try
   {
     int tmpYears, tmpMonths, tmpDays;
     XMLStrToDuration(APeriod, &tmpYears, &tmpMonths, &tmpDays);
     RC = Dateutils::IncYear( Sysutils::IncMonth( Dateutils::IncDay( ABase, tmpDays), tmpMonths), tmpYears );
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::FGetInfName(UnicodeString ACode)
{
  UnicodeString RC = "";
  try
   {
     TAnsiStrMap::iterator FRC = InfList.find(ACode);
     if (FRC != InfList.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::FGetVacName(UnicodeString ACode)
{
  UnicodeString RC = "";
  try
   {
     TAnsiStrMap::iterator FRC = VacList.find(ACode);
     if (FRC != VacList.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::FGetTestName(UnicodeString ACode)
{
  UnicodeString RC = "";
  try
   {
     TAnsiStrMap::iterator FRC = TestList.find(ACode);
     if (FRC != TestList.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::FGetCheckName(UnicodeString ACode)
{
  UnicodeString RC = "";
  try
   {
     TAnsiStrMap::iterator FRC = CheckList.find(ACode);
     if (FRC != CheckList.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
int __fastcall TdsCommClassData::FGetVPrior(int ACode)
{
  int RC = -1;
  try
   {
     TIntMap::iterator FRC = FVPrior.find(ACode);
     if (FRC != FVPrior.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
int __fastcall TdsCommClassData::FGetRVPrior(int ACode)
{
  int RC = -1;
  try
   {
     TIntMap::iterator FRC = FRVPrior.find(ACode);
     if (FRC != FRVPrior.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetPlanOpt()
{
  return FPlanOpt->AsXML;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetUnitSch(__int64 AUCode)
{
  UnicodeString RC = "";
  TTagNode* tmpSch = new TTagNode;
  TFDQuery *FQ = CreateTempQuery();
  try
   {
     try
      {
        dsPlanLogMessage("TdsCommClassData::GetUnitSch: PtId : "+IntToStr(AUCode), "", 5);
        if (quExecParam(FQ, false, "Select data From UNIT_CARD_OPT where code=:pCode", "pCode", AUCode))
         {
           TTagNode *FSchRoot, *itSch, *itDefSch;
           TTagNode *FSchDefRoot = FPlanOptDefSch;
           if (!FSchDefRoot)
            throw Exception("����������� ��������� ���� �� ���������");
           bool FCreateNew = false;
           if (FQ->FieldByName("data")->IsNull) // ��� ����
             FCreateNew = true;
           else
            {
              try
               {
                 TStringStream *FZipSch = new TStringStream;
                 try
                  {
//                    UnicodeString tmpVal = ;
                    FZipSch->WriteData(FQ->FieldByName("data")->AsBytes, FQ->FieldByName("data")->AsBytes.Length);
                    tmpSch->Encoding = "utf-8";
                    tmpSch->SetZIPXML(FZipSch);
                  }
                 __finally
                  {
                    delete FZipSch;
                  }

                 FSchRoot = tmpSch->GetChildByName("sch", true);
                 if (!FSchRoot)
                  FCreateNew = true;
               }
              catch (Exception &E)
               {
                 FCreateNew = true;
               }
            }
           if (FCreateNew)
            {
              tmpSch->Name = "ppls";
              tmpSch->Encoding = "utf-8";
              tmpSch->AddChild("passport")->AV["gui"] = NewGUI();
              FSchRoot = tmpSch->AddChild("content")->AddChild("sch");
              itDefSch = FSchDefRoot->GetFirstChild();
              while(itDefSch)
               {
                 itSch = FSchRoot->AddChild("spr");
                 itSch->AV["infref"] = itDefSch->AV["infref"];
                 itSch->AV["vac"]    = itDefSch->AV["vschdef"];
                 itSch->AV["test"]   = itDefSch->AV["tschdef"];
                 itSch->AV["tvac"]   = "_NULL_";
                 itSch->AV["ttest"]  = "_NULL_";
                 itDefSch = itDefSch->GetNext();
               }
            }
           else
            {
              itDefSch = FSchDefRoot->GetFirstChild();
              while(itDefSch)
               {
                 itSch = FSchRoot->GetChildByAV("spr", "infref", itDefSch->AV["infref"]);
                 if (!itSch)
                  {
                    itSch = FSchRoot->AddChild("spr");
                    itSch->AV["infref"] = itDefSch->AV["infref"];
                    itSch->AV["vac"]    = itDefSch->AV["vschdef"];
                    itSch->AV["test"]   = itDefSch->AV["tschdef"];
                    itSch->AV["tvac"]   = "_NULL_";
                    itSch->AV["ttest"]  = "_NULL_";
                  }
                 itDefSch = itDefSch->GetNext();
               }
            }
           RC = tmpSch->AsXML;
         }
      }
     catch(Exception &E)
      {
        RC = E.Message;
      }
   }
  __finally
   {
     delete tmpSch;
     if (FQ->Transaction->Active) FQ->Transaction->Commit();
     DeleteTempQuery(FQ);
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetDefUnitSch(__int64 AUCode)
{
  UnicodeString RC = "";
  TTagNode* tmpSch = new TTagNode;
  try
   {
     tmpSch->AsXML = GetUnitSch(AUCode);

     TTagNode *FSchRoot, *itSch, *itDefSch;
     TTagNode *FSchDefRoot = FPlanOptDefSch;

     FSchRoot = tmpSch->GetChildByName("sch", true);
     FSchRoot->DeleteChild();
     itDefSch = FSchDefRoot->GetFirstChild();
     while(itDefSch)
      {
        itSch = FSchRoot->AddChild("spr");
        itSch->AV["infref"] = itDefSch->AV["infref"];
        itSch->AV["vac"]    = itDefSch->AV["vschdef"];
        itSch->AV["test"]   = itDefSch->AV["tschdef"];
        itSch->AV["tvac"]   = "_NULL_";
        itSch->AV["ttest"]  = "_NULL_";
        itDefSch = itDefSch->GetNext();
      }
     RC = tmpSch->AsXML;
   }
  __finally
   {
     delete tmpSch;
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetDefSch(__int64 AInf, int AType)
{ // AType 0 - �������� , 1 - �����, 2 - ��������
  UnicodeString RC = "";
  try
   {

     TTagNode *itDefSch = FPlanOptDefSch->GetChildByAV("", "infref", IntToStr(AInf));
     if (itDefSch)
      {
        if (!AType)
         {
           RC = itDefSch->AV["vschdef"];
         }
        else if (AType == 1)
         {
           RC = itDefSch->AV["tschdef"];
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsCommClassData::SetUnitSch(__int64 AUCode, UnicodeString ASch)
{
  dsPlanLogMessage("TdsCommClassData::SetUnitSch: ID = "+IntToStr(AUCode), "", 5);
  TTagNode* tmpSch = new TTagNode;
  TStringStream *FZipSch = new TStringStream;
  TFDQuery *FQ = CreateTempQuery();
  try
   {
     tmpSch->AsXML = ASch;
     tmpSch->GetZIPXML(FZipSch);
     quExecParam(FQ, true, "UPDATE OR INSERT INTO UNIT_CARD_OPT (code, data) values(:pCode, :pData)", "pCode", AUCode, "", "pData",FZipSch->DataString);
   }
  __finally
   {
     delete tmpSch;
     delete FZipSch;
     DeleteTempQuery(FQ);
   }
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::CondCheck(TTagNode *nFltId, int APtCode)
{
  bool RC = false;
  if (nFltId)
   {
     TFDQuery *FQ = CreateTempQuery();
     try
      {
        UnicodeString FCountSQL = "";
        FSQLCreator->CreateSQL("0E291426-00005882-2493.006C", nFltId, NULL, "CB.CODE="+IntToStr(APtCode), FCountSQL, NULL);
        if (quExec(FQ, false, FCountSQL))
         {
           RC = (bool)FQ->FieldByName("RCOUNT")->AsInteger;
         }
      }
     __finally
      {
        if (FQ->Transaction->Active) FQ->Transaction->Commit();
        DeleteTempQuery(FQ);
      }
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::FDocGetSVal(UnicodeString AFuncName, TTagNode *ANode, UnicodeString &ATab, UnicodeString &ARet)
{
  FImmNom->GetSVal(AFuncName, ANode, ATab, ARet);
}
//---------------------------------------------------------------------------
int __fastcall DKCorrectDay( Word Year, Word Month, Word Day )
{
  Word DaysCount = DaysInAMonth(Year,Month);
  if ( Day > DaysCount )
    Day = DaysCount;
  return Day;
}
//---------------------------------------------------------------------------
bool __fastcall DKIsDateInPeriod( TDate Date, TDate PeriodBeg, TDate PeriodEnd )
{
  return ( ( ( CompareDate( Date, PeriodEnd ) == LessThanValue    ) ||
             ( CompareDate( Date, PeriodEnd ) == EqualsValue      ) ) &&
           ( ( CompareDate( Date, PeriodBeg ) == GreaterThanValue ) ||
             ( CompareDate( Date, PeriodBeg ) == EqualsValue      ) ) );
}
//---------------------------------------------------------------------------
int __fastcall AgeCmp( Word Years1, Word Months1, Word Days1, Word Years2, Word Months2, Word Days2 )
{
  int Result;
  if      ( Years1  < Years2  ) Result = -1;
  else if ( Years1  > Years2  ) Result = 1;
  else if ( Months1 < Months2 ) Result = -1;
  else if ( Months1 > Months2 ) Result = 1;
  else if ( Days1   < Days2   ) Result = -1;
  else if ( Days1   > Days2   ) Result = 1;
  else                          Result = 0;
  return Result;
}
//---------------------------------------------------------------------------
bool __fastcall IsNullSch(UnicodeString ASch)
{
  return (ASch.UpperCase().Trim() == "_NULL_");
}
//---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsCommClassData::GetUchName(TJSONObject *AValue)
{
  dsLogBegin(__FUNC__);
  UnicodeString RC = "� � � � � �";
  try
   {
     __int64 FLPUCode = JSONToInt(AValue,"00F1", -1);
     __int64 FLPUPartCode = JSONToInt(AValue,"00F9", -1);
     __int64 FUchCode = JSONToInt(AValue,"00B3", -1);
     bool FLPUPartPres = false;
//     ShowMessage(IntToStr(FLPUCode));
     if (FLPUCode != -1)
      {
        RC = GetClassData(FLPUCode, "00C7");
        if (RC.Length() > 250) RC.SetLength(250);
      }
     if (FLPUPartCode != -1)
      {
        FLPUPartPres = true;
        RC = GetClassData(FLPUPartCode, "000C");
        if (RC.Length() > 250) RC.SetLength(250);
      }
     if (FUchCode != -1)
      {
        if (FLPUPartPres)
         {
           UnicodeString FUchStr = GetClassData(FUchCode, "000E");
           if ((RC.Length() + FUchStr.Length()) > 249)
            {
              RC.SetLength(247-FUchStr.Length());
              RC += ". ��.:"+FUchStr;
            }
           else
             RC += " ��.:"+FUchStr;
         }
        else
         RC = GetClassData(FUchCode, "000E");
      }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
}
//---------------------------------------------------------------------------
/*System::UnicodeString __fastcall TdsCommClassData::GetOrgFullCode(__int64 ACode)
{
  dsLogBegin(__FUNC__);
  UnicodeString RC = IntToStr(ACode);
  try
   {
     orgCode FOrgCode;
     FOrgCode.OrgID = JSONToString(AValue, "0025").ToIntDef(0);
     RC = FOrgCode.AsString();
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
}
*/
//---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsCommClassData::GetOrgName(TJSONObject *AValue)
{
  dsLogBegin(__FUNC__);
  UnicodeString RC = "� � � � � �";
  try
   {
     if (FOnGetOrgStr)
      {
        if (JSONToString(AValue, "0025").Length() && JSONToString(AValue, "0023").ToIntDef(0))
         {
           orgCode FOrgCode;
           FOrgCode.OrgID = JSONToString(AValue, "0025").ToIntDef(0);
           FOrgCode.L1Val = JSONToString(AValue,"0026");
           FOrgCode.L2Val = JSONToString(AValue,"0027");
           FOrgCode.L3Val = JSONToString(AValue,"0028");
           FOrgCode.State = JSONToString(AValue,"0111");
           FOrgCode.Period = JSONToString(AValue,"0112").ToIntDef(0);
           if (FOrgCode.OrgID)
            {
              RC = FOnGetOrgStr(FOrgCode.AsString());
            }
           else
            RC = "�� ������ ��� �����������";
         }
      }
     RC = "���.: "+RC;
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
}
//---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsCommClassData::GetFltName(TJSONObject *AValue)
{
  dsLogBegin(__FUNC__);
  UnicodeString RC = "� � � � � �";
  try
   {
     RC = "������";
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsCommClassData::GetMIBPFromStore()
{
  try
   {
     if (FOnGetMIBPList)
      {
        TJSONObject *MIBPRet = NULL;
        TJSONPairEnumerator *itPair;
        FOnGetMIBPList(1, MIBPRet);
        if (MIBPRet)
         {
           FStoreMIBPList->clear();
           FSortedStoreMIBPList->clear();
           int FMIBPCode, FMIBPCount;
           itPair = MIBPRet->GetEnumerator();
           while (itPair->MoveNext())
            {
              FMIBPCode  =((TJSONNumber*)itPair->Current->JsonString)->AsInt;
              FMIBPCount =((TJSONNumber*)itPair->Current->JsonValue)->AsInt;
              if (VacInfList.find(FMIBPCode) != VacInfList.end())
               {
                (*FStoreMIBPList)[FMIBPCode] = FMIBPCount;
                (*FSortedStoreMIBPList)[((VacInfList[FMIBPCode]->size()<<16)&0xFFFF0000)+FMIBPCode] = FMIBPCode;
               }
              else
               dsPlanLogError("��� ���� ("+IntToStr(FMIBPCode)+") �� ������ ������ ��������.", __FUNC__);
            }
         }
        TJSONObject *TestRet = NULL;
        FOnGetMIBPList(2, TestRet);
        if (TestRet)
         {
           FStoreTestList->clear();
           int FTestCode, FTestCount;
           itPair = TestRet->GetEnumerator();
           while (itPair->MoveNext())
            {
              FTestCode  =((TJSONNumber*)itPair->Current->JsonString)->AsInt;
              FTestCount =((TJSONNumber*)itPair->Current->JsonValue)->AsInt;
              (*FStoreTestList)[FTestCode] = FTestCount;
            }
         }
      }
     if (true)
      {
        TJSONObject *MIBPRet = GetDefMIBP(1);
        TJSONPairEnumerator *itPair;
        if (MIBPRet)
         {
           FDefStoreMIBPList->clear();
           FDefSortedStoreMIBPList->clear();
           int FMIBPCode, FMIBPCount;
           itPair = MIBPRet->GetEnumerator();
           while (itPair->MoveNext())
            {
              FMIBPCode  =((TJSONNumber*)itPair->Current->JsonString)->AsInt;
              FMIBPCount =((TJSONNumber*)itPair->Current->JsonValue)->AsInt;
              if (VacInfList.find(FMIBPCode) != VacInfList.end())
               {
                 (*FDefStoreMIBPList)[FMIBPCode] = FMIBPCount;
                 (*FDefSortedStoreMIBPList)[((VacInfList[FMIBPCode]->size()<<16)&0xFFFF0000)+FMIBPCode] = FMIBPCode;
               }
              else
               dsPlanLogError("��� ���� ("+IntToStr(FMIBPCode)+") �� ������ ������ ��������.", __FUNC__);
            }
         }
        TJSONObject *TestRet = GetDefMIBP(2);
        if (TestRet)
         {
           FStoreTestList->clear();
           int FTestCode, FTestCount;
           itPair = TestRet->GetEnumerator();
           while (itPair->MoveNext())
            {
              FTestCode  =((TJSONNumber*)itPair->Current->JsonString)->AsInt;
              FTestCount =((TJSONNumber*)itPair->Current->JsonValue)->AsInt;
              (*FDefStoreTestList)[FTestCode] = FTestCount;
            }
         }
      }
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::MIBPInStore(__int64 AMIBPCode)
{
  bool RC = false;
  try
   {
     TIntMap::iterator FRC = FStoreMIBPList->find(AMIBPCode);
     RC =  (FRC != FStoreMIBPList->end());
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::MIBPInDefStore(__int64 AMIBPCode)
{
  bool RC = false;
  try
   {
     TIntMap::iterator FRC = FDefStoreMIBPList->find(AMIBPCode);
     RC =  (FRC != FDefStoreMIBPList->end());
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TJSONObject* __fastcall TdsCommClassData::GetDefMIBP(int AType)
{
  TJSONObject* RC = new TJSONObject;
  TFDQuery *FQ = CreateTempQuery();
  try
   {
     UnicodeString FSQL = "";
     if (AType == 1)
      {
        FSQL = "Select CODE From CLASS_0035";
      }
     else if (AType == 2)
      {
        FSQL = "Select CODE From CLASS_002a";
      }
     if (FSQL.Length())
      {
        if (quExec(FQ, false, FSQL))
         {
           while (!FQ->Eof)
            {
              RC->AddPair(FQ->FieldByName("code")->AsString, "10000");
              FQ->Next();
            }
         }
      }
   }
  __finally
   {
     if (FQ->Transaction->Active) FQ->Transaction->Commit();
     DeleteTempQuery(FQ);
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::CheckCompatible(__int64 AMIBP1, __int64 AMIBP2)
{
  bool RC = true;
  try
   {
     UnicodeString FCode1 = IntToStr(AMIBP1);
     UnicodeString FCode2 = IntToStr(AMIBP2);
     dsPlanLogMessage("�������� �� ������������� "+VacNameStr(AMIBP1)+" -> "+VacNameStr(AMIBP2));
     TTagNode *FIncompNode = PlanOpt->GetChildByName("incompMIBP", true);
     if (FIncompNode)
      {
//        dsPlanLogMessage(" ����� FIncompNode");
        TTagNode *itNode = FIncompNode->GetFirstChild();
        while (itNode)
         {
//           dsPlanLogMessage(" ����� �� FIncompNode");
           if (itNode->CmpAV("objref1", FCode1))
            {
              if (itNode->CmpAV("objref2", "-1,"+FCode2))
                RC = false;
            }
           else if (itNode->CmpAV("objref1", FCode2))
            {
              if (itNode->CmpAV("objref2", "-1,"+FCode1))
                RC = false;
            }
           itNode = itNode->GetNext();
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::CheckMIBPPlanCompat(__int64 AMIBPCode)
{
  bool RC = false;
  try
   {
     bool Comp = true;
     for (TIntMap::iterator itVac = FTempPlanMIBPList.begin(); (itVac != FTempPlanMIBPList.end()) && Comp; itVac++)
      {
        Comp &= CheckCompatible(itVac->second, AMIBPCode);
      }
     RC = Comp;
     if (!RC)
       dsPlanLogMessage(VacNameStr(AMIBPCode)+" ������������ c ��������� ���������� �����.");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::VacNameStr(__int64 ACode)
{
  UnicodeString RC = "";
  try
   {
     RC = "["+IntToStr(ACode)+"]\""+VacList[IntToStr(ACode)]+"\"";
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::InfNameStr(__int64 ACode)
{
  UnicodeString RC = "";
  try
   {
     RC = "["+IntToStr(ACode)+"]\""+InfList[IntToStr(ACode)]+"\"";
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsCommClassData::UnionPriviv(TUnitPrePlan *ASrc, TUnitPrePlan *ADest, int APlanType)
{
  //ASrc  - key : type.infcode type: 1 - priv, 2 - test, 3 - check
  //ADest - key : type.infcode type: 1 - priv, 2 - test, 3 - check, 4 - privbyinf[, 5 - testbyinf , 6 - checkbyinf] ?
  TUnitPriorPrePlan *FPrivList  = new TUnitPriorPrePlan;
  TUnitPrePlan *FTestList  = new TUnitPrePlan;
  TUnitPrePlan *FCheckList = new TUnitPrePlan;
  try
   {
     for (TUnitPrePlan::iterator i = ASrc->begin(); i != ASrc->end(); i++)
      {
        //��� �������� - 1,  ����� - 2,  �������� - 3
        if (i->second->PlanItemType == 1)
          (*FPrivList)[i->second->Prior*100+i->second->Inf] = i->second;
        else if (i->second->PlanItemType == 2)
          (*FTestList)[i->first] = i->second;
        else if (i->second->PlanItemType == 3)
          (*FCheckList)[i->first] = i->second;
      }
     FTempPlanMIBPList.clear();
     TUnitPriorPrePlan::iterator itPriv, eraseItem;

     TIntListMap::iterator FRC, FInfRC;
     TIntMap *FMIBPList, *FInfList;
     TIntMap::iterator itMIBP, itInf;
     bool FAllInf, FAlreadyInPlan;
     __int64 FMIBPCode;

     int FMaxMIBPInfCount, FMaxVacPrior, FInfPrior, FVacPrior;
     bool ContPlan, InStore, PlanCompat, BrigExec;
     UnicodeString FReason;
     // ��������, ������ -------------------------------------------------------
     dsPlanLogMessage("���� �� ���������(���������), �����: "+IntToStr((int)FPrivList->size()));
     for (itPriv = FPrivList->begin(); itPriv != FPrivList->end(); itPriv++)
      { // ���� �� ���������(���������) ������
        FReason = "";
        dsPlanLogMessage("�������� ��� "+InfNameStr(itPriv->second->Inf)+"  --- ������ ----------------------------");
        ContPlan = !itPriv->second->InPlan;
        BrigExec = true;
        switch (APlanType)
         {
           case 0: { break;}                                   //[1] �������:       ������ ���� (������ ��� �������� � �����)
           case 1: { BrigExec = !itPriv->second->Brig; break;} //[2] �����������:   ����������� ���� (��� �������� � ���� ����������� ��������)
           case 2: { break;}                                   //[3] �����������:   ������ ���� (������ ��� �������� � �����)
           case 3: { BrigExec = itPriv->second->Brig; break;}  //[4] �����������:   ���� ��� ���������� ���������� (�����)
           case 4: { BrigExec = itPriv->second->Brig; break;}  //[5] �����������:   ���� ��� ���������� ���������� (�������� ��� ������ ��������)
           case 5: { break;}                                   //[6] ����������� ������������ (������������ �� �������)
         }
        if (ContPlan && BrigExec)
         {
           if (GetInfMIBPList(itPriv->second->Inf, FMIBPList))
            { // �������� ������ ������ �� �������� <itPriv->second->Inf>
              FMIBPCode = -1;
              FMaxMIBPInfCount = 0;
              FMaxVacPrior = 9999;
              PlanCompat = true;
              for (itMIBP = FMIBPList->begin(); itMIBP != FMIBPList->end(); itMIBP++)
               { // ���� �� ���� ��� �������� <itPriv->second->Inf>
                 //  MIBPInDefStore(__int64 AMIBPCode);
                 FReason = "";
                 PlanCompat = CheckMIBPPlanCompat(itMIBP->first);
                 InStore  = MIBPInStore(itMIBP->first);
                 if (!PlanCompat)
                  {
                    FReason = VacNameStr(itMIBP->first)+" ������������ � ���������� �����.";
                    if (!InStore)
                      FReason = VacNameStr(itMIBP->first)+" ����������� � ������� � ������������ � ���������� �����.";
                  }
                 else
                  {
                    if (!InStore)
                      FReason = VacNameStr(itMIBP->first)+" ����������� � �������.";
                  }
                 ContPlan = PlanCompat & InStore;
                 if (ContPlan)
                  {
                    FVacPrior = 9999;
                    if (GetMIBPInfList(itMIBP->first, FInfList))
                     { //�������� ������ �������� ������� <itMIBP->second>
                       FAllInf = true;
                       FAlreadyInPlan = false;
                       if (FInfList->size() > 1)
                        {
                          dsPlanLogMessage("  ���� �� ��������� "+VacNameStr(itMIBP->first));
                          for (itInf = FInfList->begin(); (itInf != FInfList->end()) && FAllInf && !FAlreadyInPlan; itInf++)
                           { // ���� �� ��������� ����
                             FAllInf &= InfInPlanVac(FPrivList, itInf->first, FInfPrior, FAlreadyInPlan);
                             if (FAllInf && !FAlreadyInPlan && (FInfPrior < FVacPrior) && (itInf->first != itPriv->second->Inf))
                              FVacPrior = FInfPrior;
                           }
                          if (FAllInf && !FAlreadyInPlan && (FMaxMIBPInfCount < FInfList->size()))
                           {
                             FMaxMIBPInfCount = FInfList->size();
                             if (FMaxVacPrior == itPriv->second->Prior)
                              { // �� ������ ������� ���� �����������, �������������� ��������� �� ��������� ���������
                                FMaxVacPrior = FVacPrior;
                                FMIBPCode    = itMIBP->first;
                              }
                             else if (FVacPrior < FMaxVacPrior)
                              {
                                FMaxVacPrior = FVacPrior;
                                FMIBPCode    = itMIBP->first;
                                dsPlanLogMessage("   "+InfNameStr(itPriv->second->Inf)+" ���������� "+VacNameStr(FMIBPCode));
                              }
                           }
                          if (!FAllInf)
                           dsPlanLogError("   ������� "+VacNameStr(itMIBP->first)+" ��������� �� �����, �������: �� ��� �������� ������� ������������ � �����.", __FUNC__);
                          else if (FAlreadyInPlan)
                           dsPlanLogError("   ������� "+VacNameStr(itMIBP->first)+" ��������� �� �����, �������: �������� ������� ��������� ������������, ��� �������������� � �����.", __FUNC__);
                        }
                       else if (FInfList->size() == 1)
                        { // �������� ���� ��������� ������
                          if (!itPriv->second->InPlan)
                           {
                             // �����������, ������� ���������:
                             // ������� == ������� �� ����� ������� ��������������� �������� � ������ ������� �����������
                             // ���
                             // ������� �� ����� ������� ��������������� �������� �����������
                             if (!itPriv->second->MIBP ||
                                  ( itPriv->second->MIBP &&
                                    (GetMIBPInfCount(itPriv->second->MIBP) == 1) &&
                                    (itPriv->second->MIBP == itMIBP->first)
                                  ) ||
                                  itPriv->second->MIBP &&
                                  (GetMIBPInfCount(itPriv->second->MIBP) > 1)
                                )
                              {
                                if (!FMaxMIBPInfCount)
                                 {
                                   FMaxMIBPInfCount = 1;
                                   FMaxVacPrior = itPriv->second->Prior;
                                   FMIBPCode    = itMIBP->first;
                                   dsPlanLogMessage("   "+InfNameStr(itPriv->second->Inf)+" ���������� "+VacNameStr(FMIBPCode));
                                 }
                              }
                           }
                          else
                           dsPlanLogError("   ������� "+VacNameStr(itMIBP->first)+" ��������� �� �����, �������: ������� ������������ � �����.", __FUNC__);
                        }
                       else
                        dsPlanLogError("   ������� "+VacNameStr(itMIBP->first)+", ����������� ������ ��������.", __FUNC__);
                     }
                  }
                 else
                  {
                    dsPlanLogError("   "+FReason, __FUNC__);
                  }
               } // ���� �� ���� ��� �������� <itPriv->second->Inf> �����
              if (FMIBPCode != -1)
               { // ��������� ������� �� ��������� ������, �������� ������� ������� �� ������  FPrivList
                 FTempPlanMIBPList[itPriv->second->Prior*100+itPriv->second->Inf] = FMIBPCode;
                 dsPlanLogMessage("  �������� �� ��������� ������ "+InfList[IntToStr(itPriv->second->Inf)]+" ���������� "+VacNameStr(FMIBPCode));
                 if (GetMIBPInfList(FMIBPCode, FInfList))
                  {
//                    dsPlanLogMessage("  GetMIBPInfList(FMIBPCode, FInfList) "+IntToStr((int)FInfList->size()));
                    for (eraseItem = FPrivList->begin(); eraseItem != FPrivList->end(); eraseItem++)
                     {
                       if (FInfList->find(eraseItem->second->Inf) != FInfList->end())
                        {
                          eraseItem->second->InPlan = true;
//                          dsPlanLogMessage("  �������� �������� "+InfList[IntToStr(eraseItem->second->Inf)]+" ��� ����������� � ���� �� "+VacNameStr(FMIBPCode));
                        }
                     }
                  }
               }
              else
               { // ��� ������ � ������� �� ��������� ��������
                 if (PlanCompat)
                  {
                    FTempPlanMIBPList[itPriv->second->Prior*100+itPriv->second->Inf] = -1*itPriv->second->Inf;
                    dsPlanLogMessage("  �������� �� ��������� ������ "+InfList[IntToStr(itPriv->second->Inf)]+" ��� �������� �������");
                    itPriv->second->InPlan = true;

                    if (FMIBPList->size())
                     dsPlanLogError("  �������� �� �������� "+InfNameStr(itPriv->second->Inf)+" ��������� � ���� ��� �������� �������, ������� ��. ����.", __FUNC__);
                    else
                     dsPlanLogError("  �� ������ ����������� ������� ��� �������� "+InfNameStr(itPriv->second->Inf)+", �������� ��������� � ���� ��� �������� �������.", __FUNC__);
                  }
                 else
                  {
                    if (FMIBPList->size())
                     dsPlanLogError("  �������� �� �������� "+InfNameStr(itPriv->second->Inf)+" ��������� �� �����, ������� ��. ����.", __FUNC__);
                    else
                     dsPlanLogError("  �� ������ ����������� ������� ��� �������� "+InfNameStr(itPriv->second->Inf)+", �������� ��������� �� �����.", __FUNC__);
                  }
               }
            }
           else
            dsPlanLogError("  �� ������� ������� ��� �������� "+InfNameStr(itPriv->second->Inf), __FUNC__);
         }
        else
         {
           if (ContPlan)
            dsPlanLogMessage("  �������� "+InfNameStr(itPriv->second->Inf)+" ������������ � ������������ � ����������� �����, ��� ����� - "+IntToStr(APlanType)+".");
           else
            dsPlanLogMessage("  �������� "+InfNameStr(itPriv->second->Inf)+" ������������. �������: ���������� ������������.");
         }
        dsPlanLogMessage("�������� ��� "+InfNameStr(itPriv->second->Inf)+"  --- ��������� ----------------------------");
      } // ���� �� ���������(���������) �����
     if (FTempPlanMIBPList.size())
      { // � ���� ���� ������
        TUnitPrePlan::iterator FPPItem, FDestItem;

        for (TIntMap::iterator p = FTempPlanMIBPList.begin(); p != FTempPlanMIBPList.end(); p++)
         {
           if (p->second > 0)
            { // ���� ����
              if (GetMIBPInfList(p->second, FInfList))
               {
                 for (itInf = FInfList->begin(); itInf != FInfList->end(); itInf++)
                  {
                    FPPItem = ASrc->find("1."+IntToStr(itInf->first));
                    if (FPPItem != ASrc->end())
                     {
                       FDestItem = ADest->find("1."+IntToStr(p->second));
                       if (FDestItem == ADest->end())
                        {
                          (*ADest)["1."+IntToStr(p->second)]       = new TPrePlanRec(FPPItem->second);
                          (*ADest)["1."+IntToStr(p->second)]->MIBP = p->second;
                          (*ADest)["1."+IntToStr(p->second)]->Inf  = 0;
                          (*ADest)["1."+IntToStr(p->second)]->TypeList[FPPItem->second->Inf]  = FPPItem->second->VacType;
                        }
                       else
                        {
                          FDestItem->second->AllVacType = FDestItem->second->AllVacType + FPPItem->second->InfVacType();
                          FDestItem->second->TypeList[FPPItem->second->Inf]  = FPPItem->second->VacType;
                        }
                     }
                  }
               }
              dsPlanLogMessage("�������� "+VacNameStr(p->second));
            }
           else
            { // �� ���� ����
              UnicodeString FtmpInfCode = IntToStr(-1*p->second);

              FPPItem = ASrc->find("1."+FtmpInfCode);
              if (FPPItem != ASrc->end())
               {
                 FDestItem = ADest->find("4."+FtmpInfCode);
                 if (FDestItem == ADest->end())
                  {
                    (*ADest)["4."+FtmpInfCode]       = new TPrePlanRec(FPPItem->second);
                    (*ADest)["4."+FtmpInfCode]->MIBP = 0;
                    dsPlanLogMessage("�������� �������� �� "+InfNameStr(-1*p->second));
                  }
                 else
                  dsPlanLogMessage("�������� �� �������� "+InfNameStr(-1*p->second)+" ��� ������������ � �����.");
               }

/*
              if (GetMIBPInfList(p->second, FInfList))
               {

                 for (itInf = FInfList->begin(); itInf != FInfList->end(); itInf++)
                  {
                  }
               }
*/
            }
         }
      }
     // ��������, ����� --------------------------------------------------------
     // ����� ------------------------------------------------------------------
     for (TUnitPrePlan::iterator i = FTestList->begin(); i != FTestList->end(); i++)
      {
        if (ADest->find(i->first) == ADest->end())
         {
           (*ADest)[i->first] = new TPrePlanRec(i->second);
           (*ADest)[i->first]->Inf = 0;
         }
      }

   }
  __finally
   {
     delete FPrivList;
     delete FTestList;
     delete FCheckList;
   }
}
//----------------------------------------------------------------------------
void __fastcall TdsCommClassData::AddUnitPlan(TFDQuery *AQ, __int64 APlanCode, TUnitPrePlan *AUnitPrePlan, int APlanType)
{
  TIntMap  *FInfList;
  TIntMap::iterator itInf;
  TUnitPrePlan *FUnitPlanPriv = new TUnitPrePlan;
  try
   {
     UnionPriviv(AUnitPrePlan, FUnitPlanPriv, APlanType);
     try
      {
        if (!AQ->Transaction->Active) AQ->Transaction->StartTransaction();

        AQ->SQL->Text = "UPDATE OR INSERT INTO class_3084 (code, cguid, R328F, R32B2, R317D, R3298, R3299, R329A, R3087, R3088, R3086, R3089, R32AB, R308B, R3291, R308C, R3098, R308D, R32A5) values (:pcode, :pcguid, :pR328F, :pR32B2, :pR317D, :pR3298, :pR3299, :pR329A, :pR3087, :pR3088, :pR3086, :pR3089, :pR32AB, :pR308B, :pR3291, :pR308C, :pR3098, :pR308D, :pR32A5) MATCHING (CODE)";
        AQ->Prepare();
        AQ->ParamByName("pR328F")->Value = APlanCode;// '��� �����' uid='328F'
        AQ->ParamByName("pR32B2")->Value = APlanType;
        // ����
        for (TUnitPrePlan::iterator i = FUnitPlanPriv->begin(); i != FUnitPlanPriv->end(); i++)
         {
           AQ->ParamByName("pCode")->Value = GetNewCode("3084");
           AQ->ParamByName("pcguid")->Value = NewGUID();
           AQ->ParamByName("pR317D")->Value = i->second->UCode;        // extedit name='��� ��������' uid='017D' ref='1000' depend='017D.017D' isedit='0' fieldtype='integer'/>
           AQ->ParamByName("pR3298")->Value = i->second->Fam;          // text name='�������' uid='3198' inlist='l' length='30'/>
           AQ->ParamByName("pR3299")->Value = i->second->Name;         // text name='���' uid='3199' inlist='l' length='20'/>
           AQ->ParamByName("pR329A")->Value = i->second->SName;        // text name='��������' uid='319A' inlist='l' length='30'/>
           AQ->ParamByName("pR3087")->Value = i->second->PlanMonth;    // date name='���� ��' uid='1087' required='1' inlist='e'/>
           AQ->ParamByName("pR3088")->Value = i->second->PrePlanDate;  // date name='���� �����' uid='1088' required='1' inlist='l'/>
           AQ->ParamByName("pR3086")->Value = i->second->PlanItemType; // binary name='����/�����/��������?' uid='1086' required='1'/>
           AQ->ParamByName("pR3089")->Value = i->second->MIBP;         //_UID(i->first).ToIntDef(-1); // choiceDB name='����' uid='1089' ref='0035' required='1' inlist='l'>
           AQ->ParamByName("pR32AB")->Value = i->second->Inf;          // choiceDB name='��������' uid='32AB' ref='003A' inlist='l
           AQ->ParamByName("pR308B")->Value = i->second->AllVacType;   // extedit name='���/��������' uid='108B' required='1' inlist='e' length='255'/>
           AQ->ParamByName("pR3291")->Value = i->second->Brig;         // binary name='��������� ����������' uid='3191'/>
           AQ->ParamByName("pR308C")->Value = i->second->Prior;        // digit name='���������' uid='108C' required='1' digits='3' min='0' max='999'/>
           AQ->ParamByName("pR3098")->Value = 0;                       // binary name='��������/�����/�������� ���������' uid='1098' required='1' inlist='e'/>
           AQ->ParamByName("pR308D")->Value = Variant::Empty();         //<date name='���� ����������' uid='308D' inlist='e' default='CurrentDate'>
           AQ->ParamByName("pR32A5")->Value = i->second->PatBirthDay;  //<date name='�� ��������' uid='32A5' isedit='0' default='patbd'/>

//  AQ->ParamByName("pR1085")->Value = i->second->PrePlanDate;           // choice name='������' uid='1085' required='1' inlist='l' default='0'>

           AQ->OpenOrExecute();
         }
        // ���� �� ���������
        AQ->SQL->Text = "UPDATE OR INSERT INTO class_1084 (code, cguid, R318F, R32B1, R017D, R3198, R3199, R319A, R1087, R1088, R32A6, R1086, R1089, R108B, R3191, R108C, R1098, R108D, R31A5) values (:pcode, :pcguid, :pR318F, :pR32B1, :pR017D, :pR3198, :pR3199, :pR319A, :pR1087, :pR1088, :pR32A6, :pR1086, :pR1089, :pR108B, :pR3191, :pR108C, :pR1098, :pR108D, :pR31A5) MATCHING (CODE)";
        AQ->Prepare();
        AQ->ParamByName("pR318F")->Value = APlanCode;// '��� �����' uid='318F'
        AQ->ParamByName("pR32B1")->Value = APlanType;
//        __int64 FVacCode;
        for (TUnitPrePlan::iterator i = FUnitPlanPriv->begin(); i != FUnitPlanPriv->end(); i++)
         {
//           FVacCode = _UID(i->first).ToIntDef(-1);
//           AQ->ParamByName("pCode")->Value = GetNewCode("1084");
//           AQ->ParamByName("pcguid")->Value = NewGUID();
           AQ->ParamByName("pR017D")->Value = i->second->UCode;        // extedit name='��� ��������' uid='017D' ref='1000' depend='017D.017D' isedit='0' fieldtype='integer'/>
           AQ->ParamByName("pR3198")->Value = i->second->Fam;        // text name='�������' uid='3198' inlist='l' length='30'/>
           AQ->ParamByName("pR3199")->Value = i->second->Name;        // text name='���' uid='3199' inlist='l' length='20'/>
           AQ->ParamByName("pR319A")->Value = i->second->SName;        // text name='��������' uid='319A' inlist='l' length='30'/>
           AQ->ParamByName("pR1087")->Value = i->second->PlanMonth;  // date name='���� ��' uid='1087' required='1' inlist='e'/>
           AQ->ParamByName("pR1088")->Value = i->second->PrePlanDate;  // date name='���� �����' uid='1088' required='1' inlist='l'/>
//           AQ->ParamByName("pR32A6")->Value =
           AQ->ParamByName("pR1086")->Value = i->second->PlanItemType; // binary name='����/�����/��������?' uid='1086' required='1'/>

           AQ->ParamByName("pR1089")->Value = i->second->MIBP;// FVacCode;                // choiceDB name='����' uid='1089' ref='0035' required='1' inlist='l'>
//           AQ->ParamByName("pR108B")->Value = i->second->VacType;      // extedit name='���/��������' uid='108B' required='1' inlist='e' length='255'/>
           AQ->ParamByName("pR3191")->Value = i->second->Brig;         // binary name='��������� ����������' uid='3191'/>
           AQ->ParamByName("pR108C")->Value = i->second->Prior;        // digit name='���������' uid='108C' required='1' digits='3' min='0' max='999'/>
           AQ->ParamByName("pR1098")->Value = 0;                       // binary name='��������/�����/�������� ���������' uid='1098' required='1' inlist='e'/>
           AQ->ParamByName("pR108D")->Value = Variant::Empty();         //<date name='���� ����������' uid='108D' inlist='e' default='CurrentDate'>
           AQ->ParamByName("pR31A5")->Value = i->second->PatBirthDay;                       //<date name='�� ��������' uid='31A5' isedit='0' default='patbd'/>
           if (i->second->MIBP)
            { // ���� ����
              if (GetMIBPInfList(i->second->MIBP, FInfList))
               {
//                  i->second->AllVacType
                 for (itInf = FInfList->begin(); itInf != FInfList->end(); itInf++)
                  { // ���� �� ��������� ����
                    AQ->ParamByName("pCode")->Value = GetNewCode("1084");
                    AQ->ParamByName("pcguid")->Value = NewGUID();
                    AQ->ParamByName("pR32A6")->Value = itInf->first;
                    AQ->ParamByName("pR108B")->Value = i->second->TypeList[itInf->first];      // extedit name='���/��������' uid='108B' required='1' inlist='e' length='255'/>
                    AQ->OpenOrExecute();
                  }
               }
            }
           else
            { // �� ���� ����
              AQ->ParamByName("pCode")->Value = GetNewCode("1084");
              AQ->ParamByName("pcguid")->Value = NewGUID();
              AQ->ParamByName("pR32A6")->Value = i->second->Inf;
              AQ->ParamByName("pR108B")->Value = i->second->VacType;      // extedit name='���/��������' uid='108B' required='1' inlist='e' length='255'/>
              AQ->OpenOrExecute();
            }
//AQ->ParamByName("pR1085")->Value = i->second->PrePlanDate;  // choice name='������' uid='1085' required='1' inlist='l' default='0'>
         }
        if (AQ->Transaction->Active) AQ->Transaction->Commit();
      }
     catch (Exception &E)
      {
        dsPlanLogError(E.Message, __FUNC__);
      }
   }
  __finally
   {
     for (TUnitPrePlan::iterator i = AUnitPrePlan->begin(); i != AUnitPrePlan->end(); i++)
      delete i->second;
     AUnitPrePlan->clear();
     delete FUnitPlanPriv;
   }
}
//----------------------------------------------------------------------------
__int64 __fastcall TdsCommClassData::GetNewCode(UnicodeString TabId)
{
  __int64 RC = -1;
  TFDQuery *FQ = CreateTempQuery();
  try
   {
     if (!FQ->Transaction->Active)   FQ->Transaction->StartTransaction();
     FQ->Command->CommandKind = skStoredProc;
     FQ->SQL->Text = "MCODE_"+TabId;
     FQ->Prepare();
     FQ->OpenOrExecute();
     RC = FQ->FieldByName("MCODE")->AsInteger;
   }
  __finally
   {
     DeleteTempQuery(FQ);
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::NewGUID()
{
  return icsNewGUID().UpperCase();
}
//---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsCommClassData::GetPlanProgress()
{
  UnicodeString RC = "progress";
  try
   {
     Application->ProcessMessages();
     RC = IntToStr(FCurrPlan)+"/"+IntToStr(FCommPlanCount);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
int __fastcall TdsCommClassData::GetBrigExec(int APlanItemType, __int64 AInfId)
{
  int RC = 0;
  try
   {
     TTagNode *brigexecNode = PlanOpt->GetChildByName("brigexec", true);
     if (brigexecNode)
      {
        TTagNode *itNode = brigexecNode->GetChildByAV("be","inf",IntToStr(AInfId));
        if (itNode)
         {
           if (APlanItemType == 1)
             RC = itNode->AV["vac"].ToIntDef(0);
           else if (APlanItemType == 2)
             RC = itNode->AV["test"].ToIntDef(0);
           else if (APlanItemType == 3)
             RC = itNode->AV["check"].ToIntDef(0);
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TDate __fastcall CalculatePlanPeriod(TDate ADate, TDate &ADateBP, TDate &ADateEP)
{
  TDate RC = TDate(0);
  TDate PlanDate = ADate;
  TDate CurDate = Date();

  if (PlanDate < CurDate)
   PlanDate = CurDate;

  Word CurYear, CurMonth, CurDay;
  DecodeDate( CurDate, CurYear, CurMonth, CurDay );

  Word PlanYear, PlanMonth, PlanDay;
  DecodeDate( PlanDate, PlanYear, PlanMonth, PlanDay );

  if (EncodeDate(CurYear, CurMonth, 1) < EncodeDate(PlanYear, PlanMonth, 1))
   { // ���������� ���� �� ��������� �����
     ADateBP = EncodeDate(PlanYear, PlanMonth, 1);
     ADateEP = Dateutils::IncDay(Sysutils::IncMonth(ADateBP,1), -1);
     RC = ADateBP;
   }
  else
   { // ���������� ���� �� ������� �����
     ADateBP = EncodeDate(CurYear, CurMonth, CurDay);
     ADateEP = Dateutils::IncDay(Sysutils::IncMonth(EncodeDate(CurYear, CurMonth, 1),1), -1);
     ADateBP = Dateutils::IncDay(ADateBP, 2);
     if (ADateBP > ADateEP)
      ADateBP = ADateEP;
     RC = EncodeDate(CurYear, CurMonth, 1);
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCommClassData::GetWhereFromFilter (TTagNode *AFlt, UnicodeString AMainISUID)
{
  UnicodeString RC = "";
  UnicodeString FCountSQL = "";
  bool FIsDist = false;
  bool FIsAddKey = false;
  TStringList *TabList     = new TStringList;
  TdsDocSQLCreator *FSQLCreator = new TdsDocSQLCreator(FXMLList);
  FSQLCreator->OnGetSVal = FRegGetSVal;
  try
   {
     TTagNode *FDocCondRules;
     if (AFlt->CmpName("condrules"))
      FDocCondRules = AFlt;
     else
      FDocCondRules = AFlt->GetChildByName("condrules",true);
     if (FDocCondRules)
      {
        try
         {
           if (!FSQLCreator->CreateWhere(FDocCondRules,TabList,RC, AMainISUID))
            RC = "";
   //        else
   //         ATabList = FSQLCreator->CreateFrom(AMainISUID, TabList, &FIsDist,"",&FIsAddKey);
         }
        catch (Exception &E)
         {
           dsPlanLogError("FSQLCreator->CreateWhere : "+E.Message+"; CondRules = "+FDocCondRules->AsXML, __FUNC__, 5);
         }
      }
   }
  __finally
   {
     delete TabList;
     delete FSQLCreator;
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCommClassData::FRegGetSVal(UnicodeString AFuncName, TTagNode *ANode, UnicodeString &ATab, UnicodeString &ARet)
{
  FImmNom->GetSVal(AFuncName, ANode, ATab, ARet);
  return true;
}
//---------------------------------------------------------------------------

