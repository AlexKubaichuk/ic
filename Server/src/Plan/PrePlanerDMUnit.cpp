// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <System.DateUtils.hpp>
#include "PrePlanerDMUnit.h"
#include "dsSrvRegTemplate.h"
// #include "OrgParsers.h"
// #include "KLADRParsers.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TPrePlanerDM * PrePlanerDM;
// const UnicodeString csXMLDateFmtStr = "dd.mm.yyyy";
// const wchar_t cchXMLDateSeparator = '.';
// const wchar_t cchXMLPeriodSeparator = '-';
const wchar_t cchXMLDurationSeparator = '/';
// ---------------------------------------------------------------------------
const UnicodeString csReqFldNULL = "_NULL_";
// ---------------------------------------------------------------------------
const int nMaxVPriority = 1;
const int nMaxRVPriority = nMaxVPriority;
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                              Globals                                  ##
// ##                                                                       ##
// ###########################################################################
// �������������� ������� ��� ���������� ������ ��������/����/��������/�������
bool CardItemGtDate:: operator()(TCardItem x, TCardItem y)
 {
  return x.Date > y.Date;
 };
// ---------------------------------------------------------------------------
bool CardItemGtEndDate:: operator()(TCardItem x, TCardItem y)
 {
  return x.EndDate > y.EndDate;
 };
// ---------------------------------------------------------------------------
bool PlanItemListLessPlanDate:: operator()(PLAN_ITEM x, PLAN_ITEM y)
 {
  return x.PlanDate < y.PlanDate;
 }
// ---------------------------------------------------------------------------
bool PlanItemListGreaterPlanDate:: operator()(PLAN_ITEM x, PLAN_ITEM y)
 {
  return x.PlanDate > y.PlanDate;
 }
// ---------------------------------------------------------------------------
__fastcall TPrePlanerDM::TPrePlanerDM(TComponent * Owner, TFDConnection * AConnection, TTagNode *APlanOpt, TAxeXMLContainer * AXMLList,
  TdsCommClassData * AClsData, TICSIMMNomDef * AImmNom, TdsDocSQLCreator * ASQLCreator) : TDataModule(Owner)
 {
  dsLogBegin(__FUNC__);
  FConnection    = AConnection;
  FClsData       = AClsData;
  FXMLList       = AXMLList;
//  FPlanOpt       = FXMLList->GetXML("planSetting");
  FPlanOpt       = APlanOpt;
  FPlanOptDefSch = FPlanOpt->GetChildByName("infdef", true);
  FVacSchXML     = FXMLList->GetXML("12063611-00008cd7-cd89");
  FTestSchXML    = FXMLList->GetXML("001D3500-00005882-DC28");
  FCheckSchXML   = FXMLList->GetXML("569E1EB2-D0CFEF0F-BE9A");
  FMIBPAgeDef    = FVacSchXML->GetChildByName("mibpparams", true);
  FSQLCreator    = ASQLCreator;
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
__fastcall TPrePlanerDM::~TPrePlanerDM()
 {
  dsLogBegin(__FUNC__);
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TPrePlanerDM::CreateTempQuery()
 {
  dsLogBegin(__FUNC__);
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = FConnection;
    FQ->Connection                       = FConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return FQ;
 }
// ---------------------------------------------------------------------------
void __fastcall TPrePlanerDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  dsLogBegin(__FUNC__);
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
bool __fastcall TPrePlanerDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsPlanLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TPrePlanerDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
  UnicodeString AParam1Name, UnicodeString AParam1Val, UnicodeString ALogComment, UnicodeString AParam2Name,
  Variant AParam2Val, UnicodeString AParam3Name, Variant AParam3Val, UnicodeString AParam4Name, Variant AParam4Val)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
      if (AParam2Name.Length())
       AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
      if (AParam3Name.Length())
       AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
      if (AParam4Name.Length())
       AQuery->ParamByName(AParam4Name)->Value = AParam4Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      UnicodeString FParams = "; Params: ";
      if (AParam1Name.Length())
       FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
      if (AParam2Name.Length())
       FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
      if (AParam3Name.Length())
       FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
      if (AParam4Name.Length())
       FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "");
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    UnicodeString FParams = "; Params: ";
    if (AParam1Name.Length())
     FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
    if (AParam2Name.Length())
     FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
    if (AParam3Name.Length())
     FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
    if (AParam4Name.Length())
     FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "");
    dsLogSQL(((ALogComment.Length()) ? ALogComment : ASQL) + FParams, __FUNC__, FBeg - Time(), 5);
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
/* int __fastcall TPrePlanerDM::FGetVPrior(int ACode)
 {
 int RC = -1;
 try
 {
 TGIntMap::iterator FRC = FVPrior.find(ACode);
 if (FRC != FVPrior.end())
 RC = FRC->second;
 }
 __finally
 {
 }
 return RC;
 }
 //---------------------------------------------------------------------------
 int __fastcall TPrePlanerDM::FGetRVPrior(int ACode)
 {
 int RC = -1;
 try
 {
 TGIntMap::iterator FRC = FRVPrior.find(ACode);
 if (FRC != FRVPrior.end())
 RC = FRC->second;
 }
 __finally
 {
 }
 return RC;
 } */
// ---------------------------------------------------------------------------
UnicodeString __fastcall TPrePlanerDM::GetUnitSch(__int64 AUCode)
 {
  UnicodeString RC = "";
  TTagNode * tmpSch = new TTagNode;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      dsPlanLogMessage("PtId : " + IntToStr(AUCode), __FUNC__, 5);
      if (quExecParam(FQ, false, "Select data From UNIT_CARD_OPT where code=:pCode", "pCode", AUCode,
        "������ �������� ������������ ��� ��������"))
       {
        TTagNode * FSchRoot, *itSch, *itDefSch;
        TTagNode * FSchDefRoot = FPlanOptDefSch;
        if (!FSchDefRoot)
         throw Exception("����������� ��������� ���� �� ���������");
        bool FCreateNew = false;
        if (FQ->FieldByName("data")->IsNull) // ��� ����
           FCreateNew = true;
        else
         {
          try
           {
            try
             {
              tmpSch->Encoding = "utf-8";
              tmpSch->AsXML    = FQ->FieldByName("data")->AsString;
             }
            catch (Exception & E)
             {
              TStringStream * FZipSch = new TStringStream;
              try
               {
                FZipSch->WriteData(FQ->FieldByName("data")->AsBytes, FQ->FieldByName("data")->AsBytes.Length);
                tmpSch->Encoding = "utf-8";
                tmpSch->SetZIPXML(FZipSch);
               }
              __finally
               {
                delete FZipSch;
               }
             }
            FSchRoot = tmpSch->GetChildByName("sch", true);
            if (!FSchRoot)
             FCreateNew = true;
           }
          catch (Exception & E)
           {
            FCreateNew = true;
           }
         }
        if (FCreateNew)
         {
          tmpSch->Name     = "ppls";
          tmpSch->Encoding = "utf-8";
          tmpSch->AddChild("passport")->AV["gui"] = NewGUI();
          FSchRoot = tmpSch->AddChild("content")->AddChild("sch");
          itDefSch = FSchDefRoot->GetFirstChild();
          while (itDefSch)
           {
            itSch               = FSchRoot->AddChild("spr");
            itSch->AV["infref"] = itDefSch->AV["infref"];
            itSch->AV["vac"]    = itDefSch->AV["vschdef"];
            itSch->AV["test"]   = itDefSch->AV["tschdef"];
            itSch->AV["tvac"]   = "_NULL_";
            itSch->AV["ttest"]  = "_NULL_";
            itDefSch            = itDefSch->GetNext();
           }
         }
        else
         {
          itDefSch = FSchDefRoot->GetFirstChild();
          while (itDefSch)
           {
            itSch = FSchRoot->GetChildByAV("spr", "infref", itDefSch->AV["infref"]);
            if (!itSch)
             {
              itSch               = FSchRoot->AddChild("spr");
              itSch->AV["infref"] = itDefSch->AV["infref"];
              itSch->AV["vac"]    = itDefSch->AV["vschdef"];
              itSch->AV["test"]   = itDefSch->AV["tschdef"];
              itSch->AV["tvac"]   = "_NULL_";
              itSch->AV["ttest"]  = "_NULL_";
             }
            itDefSch = itDefSch->GetNext();
           }
         }
        RC = tmpSch->AsXML;
       }
     }
    catch (Exception & E)
     {
      RC = E.Message;
     }
   }
  __finally
   {
    delete tmpSch;
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TPrePlanerDM::GetUnitDopPlanCodes(__int64 AUCode, UnicodeString & AAddrCode, UnicodeString & AOrgCode,
  UnicodeString & AUchCode, UnicodeString & AProfCode)
 {
  dsLogBegin(__FUNC__);
  AAddrCode = "";
  AOrgCode  = "";
  AUchCode  = "";
  AProfCode = "";
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      dsPlanLogMessage("PtId : " + IntToStr(AUCode), __FUNC__, 5);
      // �����' uid = '0136'
      // �����������' uid = '0025'
      // ���' uid = '00F1'
      // �������' uid = '00B3'
      if (quExecParam(FQ, false, "Select R32B3, R0136, R0025, R00F1, R00B3 From class_1000 where code=:pCode", "pCode",
        AUCode))
       {
        if (!FQ->FieldByName("R0136")->IsNull)
         AAddrCode = FQ->FieldByName("R0136")->AsString;
        // ��� ����������� ����������� ������ ��� ��������
        // <������������� �� �����������' value='2'/>
        // <��������������' value='4'/>
        if (!FQ->FieldByName("R0025")->IsNull && ((FQ->FieldByName("R32B3")->AsInteger == 2) ||
          (FQ->FieldByName("R32B3")->AsInteger == 4)))
         AOrgCode = FQ->FieldByName("R0025")->AsString;
        // ��� ������� ����������� ������ ��� ��������
        // <������������� �� �������' value='1'/>
        // <������������� �� ������' value='3'/>
        if (!FQ->FieldByName("R00B3")->IsNull && ((FQ->FieldByName("R32B3")->AsInteger == 1) ||
          (FQ->FieldByName("R32B3")->AsInteger == 3)))
         AUchCode = FQ->FieldByName("R00B3")->AsString;
        // if (!FQ->FieldByName("R")->IsNull)
        // AProfCode = FQ->FieldByName("R")->AsString;
       }
     }
    catch (Exception & E)
     {
      dsPlanLogError("PtId : " + IntToStr(AUCode) + ", ��������� ���������: " + E.Message, __FUNC__, 5);
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
   }
  dsLogEnd(__FUNC__);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TPrePlanerDM::GetDefSch(__int64 AInf, int AType)
 { // AType 0 - �������� , 1 - �����, 2 - ��������
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  try
   {
    TTagNode * itDefSch = FPlanOptDefSch->GetChildByAV("", "infref", IntToStr(AInf));
    if (itDefSch)
     {
      if (!AType)
       {
        RC = itDefSch->AV["vschdef"];
       }
      else if (AType == 1)
       {
        RC = itDefSch->AV["tschdef"];
       }
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TPrePlanerDM::CondCheck(TTagNode * nFltId, int APtCode)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  if (nFltId)
   {
    TFDQuery * FQ = CreateTempQuery();
    try
     {
      UnicodeString FCountSQL = "";
      FSQLCreator->CreateSQL("0E291426-00005882-2493.006C", nFltId, NULL, "CB.CODE=" + IntToStr(APtCode),
        FCountSQL, NULL);
      if (quExec(FQ, false, FCountSQL))
       {
        RC = (bool)FQ->FieldByName("RCOUNT")->AsInteger;
       }
     }
    __finally
     {
      if (FQ->Transaction->Active)
       FQ->Transaction->Commit();
      DeleteTempQuery(FQ);
     }
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
// void __fastcall TPrePlanerDM::FDocGetSVal(UnicodeString AFuncName, TTagNode * ANode, UnicodeString & ATab, UnicodeString & ARet)
// {
// dsLogBegin(__FUNC__);
// FImmNom->GetSVal(AFuncName, ANode, ATab, ARet);
// dsLogEnd(__FUNC__);
// }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TPrePlanerDM::NewGUID()
 {
  return icsNewGUID().UpperCase();
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TPrePlanerDM::GetPlanProgress()
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "progress";
  try
   {
    Application->ProcessMessages();
    RC = IntToStr(FCurrPlan) + "/" + IntToStr(FCommPlanCount);
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TPrePlanerDM::GetBrigExec(int APlanItemType, __int64 AInfId)
 {
  dsLogBegin(__FUNC__);
  int RC = 0;
  try
   {
    TTagNode * brigexecNode = PlanOpt->GetChildByName("brigexec", true);
    if (brigexecNode)
     {
      TTagNode * itNode = brigexecNode->GetChildByAV("be", "inf", IntToStr(AInfId));
      if (itNode)
       {
        if (APlanItemType == 1)
         RC = itNode->AV["vac"].ToIntDef(0);
        else if (APlanItemType == 2)
         RC = itNode->AV["test"].ToIntDef(0);
        else if (APlanItemType == 3)
         RC = itNode->AV["check"].ToIntDef(0);
       }
     }
   }
  __finally
   {
   }
  dsLogEnd(__FUNC__);
  return RC;
 }
// ---------------------------------------------------------------------------
