//---------------------------------------------------------------------------
#include <vcl.h>

#pragma hdrstop

#include <System.DateUtils.hpp>
#include "PlanerCommon.h"
//#include "dsSrvRegTemplate.h"
//#include "OrgParsers.h"
//#include "KLADRParsers.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//const UnicodeString csXMLDateFmtStr = "dd.mm.yyyy";
//const wchar_t cchXMLDateSeparator = '.';
//const wchar_t cchXMLPeriodSeparator = '-';
const wchar_t cchXMLDurationSeparator = '/';

//---------------------------------------------------------------------------
void __fastcall XMLStrToDuration(UnicodeString AValue, int * AYears, int * AMonths, int * ADays)
 {
  AValue = AValue.Trim();
  const UnicodeString csErrMsg = "\"" + AValue + "\" is not a valid duration.";
  if (!AValue.Length())
   throw EConvertError(csErrMsg);

  TStrings * sl = new TStringList;
  try
   {
    try
     {
      sl->Text = StringReplace(AValue, cchXMLDurationSeparator, "\n", TReplaceFlags() << rfReplaceAll);
      if (sl->Count < 3)
       throw EConvertError(csErrMsg);
      *AYears  = sl->Strings[0].ToInt();
      *AMonths = sl->Strings[1].ToInt();
      *ADays   = sl->Strings[2].ToInt();
     }
    catch (EConvertError &)
     {
      throw EConvertError(csErrMsg);
     }
   }
  __finally
   {
    delete sl;
   }
 }

//---------------------------------------------------------------------------
TDate __fastcall IncDate(TDate ABase, UnicodeString APeriod)
 {
  TDate RC = 0;
  try
   {
    int tmpYears, tmpMonths, tmpDays;
    XMLStrToDuration(APeriod, & tmpYears, & tmpMonths, & tmpDays);
    RC = Dateutils::IncYear(Sysutils::IncMonth(Dateutils::IncDay(ABase, tmpDays), tmpMonths), tmpYears);
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
TDate __fastcall CalculatePlanPeriod(TDate ADate, TDate & ADateBP, TDate & ADateEP)
 {
  TDate RC = TDate(0);
  TDate PlanDate = ADate;
  TDate CurDate = Date();

  if (PlanDate < CurDate)
   PlanDate = CurDate;

  Word CurYear, CurMonth, CurDay;
  DecodeDate(CurDate, CurYear, CurMonth, CurDay);

  Word PlanYear, PlanMonth, PlanDay;
  DecodeDate(PlanDate, PlanYear, PlanMonth, PlanDay);

  if (EncodeDate(CurYear, CurMonth, 1) < EncodeDate(PlanYear, PlanMonth, 1))
   {//���������� ���� �� ��������� �����
    ADateBP = EncodeDate(PlanYear, PlanMonth, 1);
    ADateEP = Dateutils::IncDay(Sysutils::IncMonth(ADateBP, 1), -1);
    RC      = ADateBP;
   }
  else
   {//���������� ���� �� ������� �����
    ADateBP = EncodeDate(CurYear, CurMonth, CurDay);
    ADateEP = Dateutils::IncDay(Sysutils::IncMonth(EncodeDate(CurYear, CurMonth, 1), 1), -1);
//    ADateBP = Dateutils::IncDay(ADateBP, 2);
    if (ADateBP > ADateEP)
     ADateBP = ADateEP;
    RC = EncodeDate(CurYear, CurMonth, 1);
   }
  return RC;
 }
//---------------------------------------------------------------------------
