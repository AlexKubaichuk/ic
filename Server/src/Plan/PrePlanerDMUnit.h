//---------------------------------------------------------------------------

#ifndef PrePlanerDMUnitH
#define PrePlanerDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Vcl.ExtCtrls.hpp>
//#include <FireDAC.Phys.FB.hpp>
//#include <FireDAC.Comp.Client.hpp>
//#include <FireDAC.Phys.hpp>
//#include <FireDAC.Phys.Intf.hpp>
//#include <FireDAC.Stan.Def.hpp>
//#include <FireDAC.Stan.Error.hpp>
//#include <FireDAC.Stan.Intf.hpp>
//#include <FireDAC.Stan.Option.hpp>
//#include <FireDAC.UI.Intf.hpp>
//#include <Data.DB.hpp>
//#include <FireDAC.Comp.DataSet.hpp>
//#include <FireDAC.DApt.hpp>
//#include <FireDAC.DApt.Intf.hpp>
//#include <FireDAC.DatS.hpp>
//#include <FireDAC.Stan.Async.hpp>
//#include <FireDAC.Stan.Param.hpp>
//#include <FireDAC.Stan.Pool.hpp>
//#include <FireDAC.Phys.FBDef.hpp>
//#include <FireDAC.Comp.UI.hpp>
//#include <FireDAC.VCLUI.Wait.hpp>
//---------------------------------------------------------------------------
//#include "dsRegClassData.h"
#include <System.JSON.hpp>
#include "XMLContainer.h"
//#include "dsSrvClassifUnit.h"
#include "icsLog.h"
#include <list>
#include "dsDocSQLCreator.h"
#include "IMMNomDef.h"
#include "JSONUtils.h"
#include "dsCommClassData.h"
#include "PlanerCommon.h"
//---------------------------------------------------------------------------
//���� ������ ������������
#define ERR_NO_BEG_SCHEME       1               //����������� ��������� ����� ��� ��������
#define ERR_BAD_BEG_SCHEME      2               //������� �������������� ��������� �����
#define ERR_SCHEMES_LOOP        3               //������������ �� ������� �������������� � ������� ����
//---------------------------------------------------------------------------

typedef UnicodeString (__closure *TdsOnPlanDeleteDataEvent)(System::UnicodeString AId, System::UnicodeString ARecId);
typedef UnicodeString (__closure *TdsOnPlanInsertDataEvent)(System::UnicodeString AId, System::UnicodeString AValue, System::UnicodeString &ARecId);
typedef void          __fastcall (__closure *TdsOnPlanSaveDebugEvent)(UnicodeString AXML);
//---------------------------------------------------------------------------
// ��� ��������� �� ������ ������������
// ��� �������� ������������
enum class TPlanItemKind
{
  pkVakchina = 0,       // �������
  pkTest     = 1,       // �����
  pkCheck    = 2,       // ��������
  pkUnknown
};
//---------------------------------------------------------------------------
// ��� �������� ����������� �����
enum class TCardItemKind
{
  Vac     = 0,       // ��������
  Test    = 1,       // �����
  Check   = 2,       // ��������
  Cancel  = 3,       // �����
  Unknown
};
//---------------------------------------------------------------------------
// ��� ��������
enum TJumpKind
{
  jkUnknown,    // �����������
  jkSrok,       // �� �����
  jkAge,        // �� ��������
  jkCond,       // �� �������
  jkEnd         // �� ���������
};
//---------------------------------------------------------------------------
struct TJump{
  TJumpKind Kind;
  UnicodeString Reason;

  TJump(): Kind(jkUnknown), Reason(""){};
  TJump(TJumpKind AKind, UnicodeString AReason): Kind(AKind), Reason(AReason){};
};
// ��� "���������", �� �������� �������� ������ � ������� "����" (CARD_1084)
enum TPlanItemSource
{
  pisUnknown        = -1,
  pisOffPlan        = 0,        // ����������
  pisManual         = 1,        // ������ ����������
  pisPlanerClndr    = 2,        // ����������� �� ���������
  pisPlanerOffClndr = 3,        // ����������� �� �� ���������
  pisPlanerPrev     = 4,        // ����������� ��� ���������������
  pisPlanerRnd      = 5,        // ����������� �� ����
  pisPlanerRndPrev  = 6         // ����������� �� ���� ��� ���������������
};
//---------------------------------------------------------------------------
//������� ������ ����������� ��������/����
typedef struct tagPLAN_ITEM {
  TPlanItemKind PlanItemKind;           // ��� �������� ( ��������/����� )
  int           InfekId;                // ��� �������� (-1 ��� �������������� ��������/����� ��� ���
                                        // ������������ ��������)
  int           OwnerId;                // ��� ����/�����
  int           SrcId;                  // ��� ���� �������� ��������������� ����� (������ ��� ����)
  TDate         DatePlanTest;           // ���� �������� ��������
  UnicodeString TypePlanTest;           // ��� �������� �����
  UnicodeString OwnerName;              // ������������ ����/�����
  TDate         PlanDate;               // ���� ������������ ���������� ��������/�����
  TDate         PlanMonth;              // ������ ���� ������ ���� ������������ ����������
  TDate         PlanDayByMonth;         // ����������� ���� ������
  UnicodeString SchemeLineUID;          // InfekId.<UID ������ �����, �� ������� ������������� ��������/�����>
                                        //  ( "" ��� �������������� ��������/����� )
                                        // ��� ������������ �������� ������ uid'�� ����������� ";"
  UnicodeString Type_Prep;              // ��� ���������� ( Vi,RVi/j,D ) / ����� ����� (��.i,D),
                                        // ��� ������������ �������� ������ ����������� ";"
  int           Priority;               // ��������� ���������� ( -1 ��� ����� )
  int           V_RV_Priority;          // ��������� ����������/������������ (�� �������� ����������� ��������)
                                        // ��� -1 ��� �����
  TDate         VypDate;                // ���� ��������� ���������� ��������/�����
  TPlanItemSource Source;               // ��� "���������", �� �������� �������� ������ � ������� "����" (CARD_1084)
  tagPLAN_ITEM() : PlanItemKind(TPlanItemKind::pkUnknown),
                   InfekId(-1),
                   OwnerId(-1),
                   SrcId(0),
                   OwnerName(""),
                   PlanDate(0),
                   PlanDayByMonth(0),
                   PlanMonth(0),
                   SchemeLineUID(""),
                   Type_Prep(""),
                   Priority(-1),
                   V_RV_Priority(-1),
                   VypDate(0),
                   DatePlanTest(0),
                   TypePlanTest(""),
                   Source(pisUnknown) {};
}PLAN_ITEM, *PPLAN_ITEM;
//---------------------------------------------------------------------------
typedef struct tagCardItem {
  TCardItemKind Kind;    // ��� �������� ( ��������/�����/��������/����� )
  __int64       Id;      // ��� ����/�����/��������/������� ������
  int           Inf;     // ��� ��������
  bool          Tur;     // ������� ���� (������ ��� ��������) (���������/����������) ��� �������
  UnicodeString VacType; // V / RV
  TDate         Date;    // ���� ����������
  TDate         EndDate; // ���� ��������� ��� �������
  UnicodeString Sch;     // �����
  bool          Vac;     // ����� ��������
  bool          Test;    // ����� �����

  tagCardItem() : Kind(TCardItemKind::Unknown),
                  Id(-1),
                  Inf(-1),
                  Tur(false),
                  Vac(false),
                  Test(false),
                  VacType(""),
                  Date(0),
                  EndDate(0),
                  Sch(""){};
} TCardItem;

//---------------------------------------------------------------------------
//typedef list<TPreTestItem>      TPreTestItemList;          //������ ��������� ��������/����/��������/�������
typedef list<TCardItem>         TCardItemList;          //������ ��������� ��������/����/��������/�������
typedef list<PLAN_ITEM>         TPlanItemList;          //������ ����������� ��������/����
typedef map<int, TDate>         TPlanItemDateList;          //������ ����������� ��������/����
typedef TPlanItemList::iterator TPlanItemListIt;        //�������� ��� ������ ����������� ��������/����
//---------------------------------------------------------------------------
//������� ������-������ ��� ��������� ����� �������� ����
typedef struct tagTRACE_ITEM {
  UnicodeString    SchemeUID;              //UID �����
  UnicodeString    LineUID;                //UID ������
  UnicodeString    Name;                   //������ ����������� ������ ����� ( <������������ �����>.<������������ ������> )
  int           RefOwner;               //��� ��������������� ���� ��� �����
  TJumpKind     JumpKind;               //��� ��������, � ���������� �������� ������ ������� ����� � ������
  UnicodeString    JumpReason;             //������� ���������� ��������, � ���������� �������� ������ ������� ����� � ������

  tagTRACE_ITEM() : SchemeUID(""),
                    LineUID(""),
                    Name(""),
                    RefOwner(-1),
                    JumpKind(jkUnknown),
                    JumpReason("") {};
}TRACE_ITEM, *PTRACE_ITEM;

//---------------------------------------------------------------------------
typedef list<TRACE_ITEM>         TJumpTraceList;        //������-������ ��� ��������� ����� �������� ����
typedef TJumpTraceList::iterator TJumpTraceListIt;      //�������� ��� ������ ��� ��������� ����� �������� ����
//---------------------------------------------------------------------------
//������� ������ ����������� ��������
typedef struct tagINF_ITEM {
  int             Id;                // ��� ��������
  int             Type;              // ��� 0 - ����, 1- ���
  UnicodeString   TurSch;            // ����� ��� ����
  tagINF_ITEM() : Id(-1),
                  Type(-1),
                  TurSch("") {};
  tagINF_ITEM(int AId, int AType, UnicodeString ATurSch) : Id(AId),
                                                           Type(AType),
                                                           TurSch(ATurSch) {};
}INF_ITEM;
//---------------------------------------------------------------------------
typedef list<INF_ITEM>         TInfItemList;          //������ ����������� ��������
//---------------------------------------------------------------------------
/*struct gtint
{
  bool operator()(int  v1, int v2) const
  {
    return (v1 > v2);
  }
};
//---------------------------------------------------------------------------
typedef map<int,int, gtint> TGIntMap;  */
//typedef map<int, TIntMap*> TIntListMap;
//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                              Globals                                  ##
//##                                                                       ##
//###########################################################################


//�������������� ������� ��� ���������� ������ ��������/����/��������/�������
struct CardItemGtDate : public binary_function<TCardItem, TCardItem, bool>
{
  bool operator() (TCardItem x, TCardItem y);
};
struct CardItemGtEndDate : public binary_function<TCardItem, TCardItem, bool>
{
  bool operator() (TCardItem x, TCardItem y);
};
//�������������� ������� ��� ���������� ������ ����������� ��������/����

struct PlanItemListLessPlanDate : public binary_function<PLAN_ITEM, PLAN_ITEM, bool>
{
  bool operator() (PLAN_ITEM x, PLAN_ITEM y);
};

struct PlanItemListGreaterPlanDate : public binary_function<PLAN_ITEM, PLAN_ITEM, bool>
{
  bool operator() (PLAN_ITEM x, PLAN_ITEM y);
};


//---------------------------------------------------------------------------
class TPrePlanerDM : public TDataModule
{
__published:	// IDE-managed Components
private:	// User declarations
  TFDConnection *FConnection;



  TAxeXMLContainer *FXMLList;
  TdsCommClassData *FClsData;
  TdsDocSQLCreator *FSQLCreator;

  TTagNode *FVacSchXML;
  TTagNode *FTestSchXML;
  TTagNode *FCheckSchXML;
  TTagNode *FMIBPAgeDef;


//  TICSIMMNomDef      *FImmNom;
//  TFDQuery* FImmNomQuery;
  int FCommPlanCount, FCurrPlan;

  TdsOnPlanDeleteDataEvent  FOnPlanDeleteData;
  TdsOnPlanInsertDataEvent  FOnPlanInsertData;

//  int __fastcall FGetVPrior(int ACode);
//  int __fastcall FGetRVPrior(int ACode);
//  void __fastcall FDocGetSVal(UnicodeString AFuncName, TTagNode *ANode, UnicodeString &ATab, UnicodeString &ARet);

public:		// User declarations
  __fastcall TPrePlanerDM(TComponent* Owner, TFDConnection *AConnection, TTagNode *APlanOpt, TAxeXMLContainer *AXMLList, TdsCommClassData *AClsData, TICSIMMNomDef *AImmNom, TdsDocSQLCreator *ASQLCreator);
  __fastcall ~TPrePlanerDM();

//  TPreTestItemList PreTestList;
  UnicodeString __fastcall NewGUID();
  int __fastcall GetBrigExec(int APlanItemType, __int64 AInfId);
  UnicodeString __fastcall GetDefSch(__int64 AInf, int AType);

//  TGIntMap     FVPrior;
//  TGIntMap     FRVPrior;
  TTagNode    *FPlanOpt;
  TTagNode    *FPlanOptDefSch;

  bool __fastcall CondCheck(TTagNode *nFltId, int APtCode);

  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AParam1Name, UnicodeString AParam1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AParam2Name = "", Variant AParam2Val = 0,
                               UnicodeString AParam3Name = "", Variant AParam3Val = 0,
                               UnicodeString AParam4Name = "", Variant AParam4Val = 0);


  void          __fastcall GetUnitDopPlanCodes(__int64 AUCode, UnicodeString &AAddrCode, UnicodeString &AOrgCode, UnicodeString &AUchCode, UnicodeString &AProfCode);
  UnicodeString __fastcall GetUnitSch(__int64 AUCode);
  System::UnicodeString __fastcall GetPlanProgress();

  __property TTagNode *PlanOpt = {read=FPlanOpt};

  __property TTagNode *VacSchXML = {read=FVacSchXML};
  __property TTagNode *TestSchXML = {read=FTestSchXML};
  __property TTagNode *CheckSchXML = {read=FCheckSchXML};

//  __property int VPrior[int ACode] = {read=FGetVPrior};
//  __property int RVPrior[int ACode] = {read=FGetRVPrior};

  __property TAxeXMLContainer* XMLList = {read=FXMLList};
  __property TdsCommClassData *ClsData = {read=FClsData};


  __property TdsOnPlanDeleteDataEvent  OnPlanDeleteData = {read=FOnPlanDeleteData, write=FOnPlanDeleteData};
  __property TdsOnPlanInsertDataEvent  OnPlanInsertData = {read=FOnPlanInsertData, write=FOnPlanInsertData};
};
//---------------------------------------------------------------------------
extern PACKAGE TPrePlanerDM *PrePlanerDM;
//---------------------------------------------------------------------------
extern PACKAGE const UnicodeString csReqFldNULL;

extern PACKAGE const int nMaxVPriority;
extern PACKAGE const int nMaxRVPriority;
//---------------------------------------------------------------------------
#endif
