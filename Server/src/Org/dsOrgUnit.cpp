﻿// ---------------------------------------------------------------------------
// #include <vcl.h>
#pragma hdrstop
#include "dsOrgUnit.h"
#include <Datasnap.DSHTTP.hpp>
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "System.Classes.TPersistent"
#pragma resource "*.dfm"
// TRegClassDM *RegClassDM;
// ---------------------------------------------------------------------------
__fastcall TdsOrgClass::TdsOrgClass(TComponent * Owner, TTagNode * AFormatDef, TAppOptions *AOpt) : TDataModule(Owner)
 {
  dsLogC(__FUNC__);
  try
   {
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes)
     {
      // FFormatDef = AFormatDef;
      ConnectDB(OrgConnection, AOpt, FSes->UserRoles->Values["ICDB"], __FUNC__);
      FDM = new TdsOrgDM(this, OrgConnection, AFormatDef);
     }
   }
  catch (Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogC(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
__fastcall TdsOrgClass::~TdsOrgClass()
 {
  dsLogD(__FUNC__);
  DisconnectDB(OrgConnection, __FUNC__);
  Sleep(50);
  delete FDM;
  dsLogD(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
TJSONObject * TdsOrgClass::Find(UnicodeString AStr)
 {
  TJSONObject * RC = NULL;
  dsLogBegin(__FUNC__);
  try
   {
    dsLogMessage("Find: str: " + AStr);
    RC = FDM->Find(AStr);
   }
  __finally
   {
    dsLogEnd(__FUNC__);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString TdsOrgClass::OrgCodeToText(UnicodeString ACode, UnicodeString ASetting)
 {
  UnicodeString RC = "";
  dsLogBegin(__FUNC__);
  try
   {
    try
     {
      dsLogMessage("Code: " + ACode + ", Setting: " + ASetting);
      RC = FDM->OrgCodeToText(ACode, ASetting);
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    dsLogEnd(__FUNC__);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsOrgClass::GetOrgField(__int64 AOrgCode, UnicodeString AFlName)
 {
  dsLogMessage("GetOrgField: Code: " + IntToStr(AOrgCode) + ", FlName: " + AFlName);
  return FDM->GetOrgField(AOrgCode, AFlName);
 }
// ----------------------------------------------------------------------------
