// ---------------------------------------------------------------------------
#pragma hdrstop
#include "OrgParsers.h"
#include <dialogs.hpp>
#include "srvsetting.h"
#ifndef _CONFSETTTING
#error _CONFSETTTING using are not defined
#endif
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
orgFormats::orgFormats()
 {
  id = L1Val = L2Val = L3Val = "";
 };
// ---------------------------------------------------------------------------
void __fastcall ShowDebug(UnicodeString AText)
 {
#ifdef _DEBUG_MESSAGE
  // dsLogMessage(AText, "", 1);
#endif
 }
// ---------------------------------------------------------------------------
__fastcall EOrgError::EOrgError(const UnicodeString Msg) : Exception(Msg)
 {
 }
// ---------------------------------------------------------------------------
// ###########################################################################
// #                                                                         #
// #                           TOrgCacheRecord                                #
// #                                                                         #
// ###########################################################################
// ---------------------------------------------------------------------------
__fastcall TOrgCacheRecord::TOrgCacheRecord(TTagNode * AFormatDef)
 {
  ShowDebug(UnicodeString(__FUNC__));
  FFormatDef = AFormatDef;
  Status     = 0;
  for (int i = 0; i < 6; i++)
   Values[i] = "";
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TOrgCacheRecord::GetText(UnicodeString Params, TTagNode * AFormatDef)
 {
  ShowDebug(UnicodeString(__FUNC__));
  UnicodeString RC = "";
  UnicodeString FParams = Params;
  TStringList * Formats = new TStringList;
  TStringList * FRC = new TStringList;
  try
   {
    // Params[1] Values[0] =  ������������ �����������
    // Params[2] Values[1] = ��1
    // Params[3] Values[2] = ��2
    // Params[4] Values[3] = ��3
    // Params[5] Values[4] = ������
    // Values[5] = �����
    UnicodeString FON, FL1, FL2, FL3;
    FON = "";
    FL1 = "";
    FL2 = "";
    FL3 = "";
    int ALvl1, ALvl2, ALvl3;
    orgFormats * FFormatData;
    UnicodeString FFormat, FShortFormat;
    GetOrgStrFormatByOKVED(FFormatDef, Values[5].ToIntDef(0), Values[6], Formats);
    if (Params[1] == '1')
     FON = Values[0].Trim();
    for (int i = 0; i < Formats->Count; i++)
     {
      FFormatData = (orgFormats *)Formats->Objects[i];
      if ((Params[2] == '1') && Values[1].Length())
       {
        FL1 = Format(FFormatData->L1Val, OPENARRAY(TVarRec, (Values[1]))).Trim();
        if ((Params[3] == '1') && Values[2].Length())
         {
          FL2 = Format(FFormatData->L2Val, OPENARRAY(TVarRec, (Values[2]))).Trim();
          if ((Params[4] == '1') && Values[3].Length())
           {
            FL3 = Format(FFormatData->L3Val, OPENARRAY(TVarRec, (Values[3]))).Trim();
           }
         }
       }
      if (FON.Length())
       {
        if ((FL1 + FL2 + FL3).Length())
         RC = FON + ", " + FL1 + FL2 + FL3;
        else
         RC = FON;
       }
      else
       RC = FL1 + FL2 + FL3;
      RC = RC.Trim();
      if (FRC->IndexOf(RC) == -1)
       FRC->Add(FFormatData->id + ":" + RC);
     }
   }
  __finally
   {
    RC = FRC->Text.Trim();
    delete Formats;
    delete FRC;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
// ###########################################################################
// #                                                                         #
// #                        TNFOrgWords                                      #
// #                                                                         #
// ###########################################################################
// ---------------------------------------------------------------------------
__fastcall TNFOrgWords::TNFOrgWords(UnicodeString ASrc) : TStringList()
 {
  ShowDebug(UnicodeString(__FUNC__));
  FResWords = new StringIntMap;
  (*FResWords)["�����"] = 1;
  (*FResWords)["���������"] = 1;
  (*FResWords)["����"] = 1;
  (*FResWords)["������"] = 1;
  (*FResWords)["���"] = 1;
  (*FResWords)["�������"] = 1;
  (*FResWords)["�����"] = 1;
  (*FResWords)["���������"] = 1;
  (*FResWords)["�������"] = 1;
  // FResWords["�����������"] = 1;
  // ��������� ������ ������ �������� ������, � ������ ������ � ���� ���� :)
  // FReplasedObjects["��"]  = "�����";
  // FReplasedObjects["��."] = "�����";
  // FReplasedObjects["�/�"] = "������� ���";
  // FReplasedObjects["�/C"] = "������� ���"; //C - ���������
  // FReplasedObjects["�\�"] = "������� ���";
  // FReplasedObjects["�\C"] = "������� ���"; //C - ���������
  // FReplasedObjects["��"]  = "������� ���";
  // FReplasedObjects["�C"]  = "������� ���"; //C - ���������
  // FReplasedObjects["��."] = "������� ���";
  // FReplasedObjects["�C."] = "������� ���"; //C - ���������
  // FReplasedObjects["��"]  = "�����";
  // FReplasedObjects["��."] = "�����";
  // FReplasedObjects["��"]  = "������";
  // FReplasedObjects["��."] = "������";
  Text = ASrc;
 }
// ---------------------------------------------------------------------------
__fastcall TNFOrgWords::~TNFOrgWords()
 {
  FResWords->clear();
  delete FResWords;
  ShowDebug(UnicodeString(__FUNC__));
 }
// ---------------------------------------------------------------------------
bool __fastcall TNFOrgWords::IsNotBr(wchar_t ASrc)
 {
  ShowDebug(UnicodeString(__FUNC__));
  return !((ASrc == ' ') || (ASrc == '(') || (ASrc == '{') || (ASrc == '[') || (ASrc == ')') || (ASrc == '}') ||
    (ASrc == ']'));
 }
// ---------------------------------------------------------------------------
bool __fastcall TNFOrgWords::IsLBr(wchar_t ASrc)
 {
  ShowDebug(UnicodeString(__FUNC__));
  return ((ASrc == '(') || (ASrc == '{') || (ASrc == '['));
 }
// ---------------------------------------------------------------------------
bool __fastcall TNFOrgWords::IsRBr(wchar_t ASrc)
 {
  ShowDebug(UnicodeString(__FUNC__));
  return ((ASrc == ')') || (ASrc == '}') || (ASrc == ']'));
 }
// ---------------------------------------------------------------------------
bool __fastcall TNFOrgWords::Cmp(UnicodeString ASrc)
 {
  ShowDebug(UnicodeString(__FUNC__));
  bool RC = false;
  UnicodeString FSrc = ASrc.Trim().UpperCase();
  UnicodeString FRW, FRWF, FRWS;
  try
   {
    if (FSrc.Length())
     {
      bool IsDot = FSrc[FSrc.Length()] == '.';
      if (IsDot)
       FSrc.Delete(FSrc.Length(), 1);
      if (FSrc.Length())
       {
        StringIntMap::iterator FRC = FResWords->find(FSrc);
        RC = (FRC != FResWords->end());
        for (StringIntMap::iterator rw = FResWords->begin(); (rw != FResWords->end()) && !RC; rw++)
         {
          FRW = rw->first.Trim();
          if ((rw->second > 1) && !IsDot)
           {
            // 2 � ����� ���� �������� �� 1�1�... 1�/1�
            FRWF = FRW[1];
            FRWS = FRW[1];
            for (int i = 1; i <= FRW.Length(); i++)
             {
              if (FRW[i] == ' ')
               {
                if ((i + 1) < FRW.Length())
                 {
                  FRWF += FRW[i + 1];
                  FRWS += "/" + FRW[i + 1];
                 }
               }
             }
            RC = (FRWF == FSrc);
            if (!RC)
             RC = (FRWS == FSrc);
           }
          // �������� �� ���������� �� �����������
          if (rw->second == 1)
           {
            for (int i = 1; (i <= FRW.Length()) && !RC; i++)
             RC = (FRW.SubString(1, i) == FSrc);
            // �������� �� ���������� �� ����������� � "-"
            if (FSrc.Pos("-") && !RC)
             {
              for (int i = 1; (i <= FRW.Length()) && !RC; i++)
               for (int j = i + 2; (j <= FRW.Length()) && !RC; j++)
                RC = (FRW.SubString(1, i) + "-" + FRW.SubString(j, FRW.Length() - j + 1) == FSrc);
             }
           }
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TNFOrgWords::setText(UnicodeString ASrc)
 {
  ShowDebug(UnicodeString(__FUNC__));
  FText = ASrc;
  UnicodeString FSrc = FText.UpperCase();
  UnicodeString tmp = "";
  for (int i = 1; i <= FSrc.Length(); i++)
   {
    if ((unsigned int)FSrc[i] >= 32)
     {
      if (IsNotBr(FSrc[i]) && (FSrc[i] != '.'))
       {
        tmp += FSrc[i];
       }
      else
       {
        if (FSrc[i] == '.')
         tmp += FSrc[i];
        tmp = tmp.Trim();
        if (tmp.Length())
         {
          // StrStrMap::iterator FRC = FReplasedObjects.find(tmp.UpperCase());
          // if(FRC != FReplasedObjects.end())
          // tmp = FRC->second;
          AddObject(tmp, (TObject *)wtWord);
         }
        if (IsLBr(FSrc[i]))
         AddObject(UnicodeString(FSrc[i]), (TObject *)wtLBR);
        else if (IsRBr(FSrc[i]))
         AddObject(UnicodeString(FSrc[i]), (TObject *)wtRBR);
        tmp = "";
       }
     }
   }
  tmp = tmp.Trim();
  if (tmp.Length())
   {
    // StrStrMap::iterator FRC = FReplasedObjects.find(tmp.UpperCase());
    // if(FRC != FReplasedObjects.end())
    // tmp = FRC->second;
    AddObject(tmp, (TObject *)wtWord);
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TNFOrgWords::CanClear(UnicodeString ASrc)
 {
  ShowDebug(UnicodeString(__FUNC__));
  bool RC = false;
  try
   {
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNFOrgWords::getPreparedText()
 {
  ShowDebug(UnicodeString(__FUNC__));
  UnicodeString RC = "";
  UnicodeString tmp;
  int tmpCount;
  bool FClosed;
  try
   {
    if (FirstType == wtLBR)
     { // ������ ������� ����������� ������
      if (WordType[2] == wtRBR)
       {
        if (Cmp(tmp))
         {
          for (int i = 0; i < 3; i++)
           Delete(0);
         }
       }
     }
    if (LastType == wtRBR)
     { // ��������� ������� ����������� ������
      if (WordType[Count - 3] == wtLBR)
       {
        if (Cmp(tmp))
         {
          int id = Count - 3;
          for (int i = 0; i < 3; i++)
           Delete(id);
         }
       }
     }
    for (int i = 0; i < Count; i++)
     {
      if (Cmp(Strings[i]))
       {
        Delete(i);
        i-- ;
       }
     }
    for (int i = 0; i < Count; i++)
     {
      if (!RC.Length())
       RC = Strings[i];
      else
       RC += " " + Strings[i];
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNFOrgWords::getWord(int AIdx)
 {
  ShowDebug(UnicodeString(__FUNC__));
  if ((AIdx >= 0) && (AIdx < Count))
   return Strings[AIdx];
  else
   return "";
 }
// ---------------------------------------------------------------------------
void __fastcall TNFOrgWords::setWord(int AIdx, UnicodeString AVal)
 {
  ShowDebug(UnicodeString(__FUNC__));
  if ((AIdx >= 0) && (AIdx < Count))
   {
    // StrStrMap::iterator FRC = FReplasedObjects.find(AVal.UpperCase());
    // if (FRC != FReplasedObjects.end())
    // Strings[AIdx] = FRC->second;
    // else
    Strings[AIdx] = AVal;
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNFOrgWords::getFirst()
 {
  ShowDebug(UnicodeString(__FUNC__));
  if (Count)
   return Strings[0];
  else
   return "";
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNFOrgWords::getLast()
 {
  ShowDebug(UnicodeString(__FUNC__));
  if (Count)
   return Strings[Count - 1];
  else
   return "";
 }
// ---------------------------------------------------------------------------
ParseWordType __fastcall TNFOrgWords::getType(int AIdx)
 {
  ShowDebug(UnicodeString(__FUNC__));
  if ((AIdx >= 0) && (AIdx < Count))
   return (ParseWordType)(int)Objects[AIdx];
  else
   return wtNone;
 }
// ---------------------------------------------------------------------------
ParseWordType __fastcall TNFOrgWords::getFirstType()
 {
  ShowDebug(UnicodeString(__FUNC__));
  if (Count)
   return (ParseWordType)(int)Objects[0];
  else
   return wtNone;
 }
// ---------------------------------------------------------------------------
ParseWordType __fastcall TNFOrgWords::getLastType()
 {
  ShowDebug(UnicodeString(__FUNC__));
  if (Count)
   return (ParseWordType)(int)Objects[Count - 1];
  else
   return wtNone;
 }
// ---------------------------------------------------------------------------
// ###########################################################################
// #                                                                         #
// #                 TNFOrgItems                                            #
// #                                                                         #
// ###########################################################################
// ---------------------------------------------------------------------------
__fastcall TNFOrgItems::TNFOrgItems(UnicodeString ASrc, wchar_t ASeparator, bool AExtended) : TStringList()
 {
  ShowDebug(UnicodeString(__FUNC__));
  FSeparator    = ASeparator;
  FDopSeparator = ',';
  FExtended     = AExtended;
  Text          = ASrc;
 }
// ---------------------------------------------------------------------------
__fastcall TNFOrgItems::~TNFOrgItems()
 {
  ShowDebug(UnicodeString(__FUNC__));
  for (int i = 0; i < Count; i++)
   {
    if (Objects[i])
     {
      delete Objects[i];
      Objects[i] = NULL;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TNFOrgItems::setText(UnicodeString ASrc)
 {
  ShowDebug(UnicodeString(__FUNC__));
  FText = ASrc.Trim();
  for (int i = 0; i < Count; i++)
   {
    if (Objects[i])
     {
      delete Objects[i];
      Objects[i] = NULL;
     }
   }
  Clear();
  UnicodeString FCurItem = "";
  if (FText.Length())
   {
    for (int i = 1; i <= FText.Length(); i++)
     {
      // !!!        if(!((FText[i] == ',') || (FText[i] == ' ')))
      if ((FExtended && !(((FText[i] == FSeparator) || (FText[i] == FDopSeparator)) || (FText[i] == '.'))) ||
        (!FExtended && (FText[i] != FSeparator) && (FText[i] != FDopSeparator)))
       {
        FCurItem += FText[i];
       }
      else
       {
        if (FExtended && (FText[i] == '.'))
         FCurItem += FText[i];
        FCurItem = FCurItem.Trim();
        AddObject(FCurItem, new TNFOrgWords(FCurItem));
        FCurItem = "";
       }
     }
    FCurItem = FCurItem.Trim();
    AddObject(FCurItem, new TNFOrgWords(FCurItem));
   }
 }
// ---------------------------------------------------------------------------
TNFOrgWords * __fastcall TNFOrgItems::getItem(int AIdx)
 {
  ShowDebug(UnicodeString(__FUNC__));
  if ((AIdx >= 0) && (AIdx < Count))
   return (TNFOrgWords *)Objects[AIdx];
  else
   throw Exception("TNFOrgItems::getItem. Index out of range.");
 }
// ---------------------------------------------------------------------------
TNFOrgWords * __fastcall TNFOrgItems::getFirst()
 {
  ShowDebug(UnicodeString(__FUNC__));
  if (Count)
   return (TNFOrgWords *)Objects[0];
  else
   throw Exception("TNFOrgItems::getFirst. Index out of range.");
 }
// ---------------------------------------------------------------------------
TNFOrgWords * __fastcall TNFOrgItems::getLast()
 {
  ShowDebug(UnicodeString(__FUNC__));
  if (Count)
   return (TNFOrgWords *)Objects[Count - 1];
  else
   throw Exception("TNFOrgItems::getLast. Index out of range.");
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                      Utils                                                 #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
orgCode __fastcall ParseOrgCodeStr(UnicodeString AOrgStr, bool ATemplateMode)
 {
  orgCode RC;
  try
   {
    try
     {
      if (AOrgStr.Trim().Length())
       {
        UnicodeString tmp = AOrgStr.Trim();
        int FVerPos = tmp.Pos("@");
        UnicodeString FOrgCode = "";
        UnicodeString FLvlStr = "";
        if (FVerPos)
         { // <���_�����������>.<�������_�����������������>@<������_�������>##<���_�����>[#F<���_�������>][#1<��������_������1>][#2<��������_������2>][#3<��������_������3>][#S<��������_���������>][#P<���_��������_�������>]
          FOrgCode = tmp.SubString(1, FVerPos - 1).Trim(); // <���_�����������>.<�������_�����������������>
          FLvlStr  = tmp.SubString(FVerPos + 1, tmp.Length() - FVerPos).Trim();
          // <������_�������>##<���_�����>[#1<��������_������1>][#2<��������_������2>][#3<��������_������3>][#S<��������_���������>][#P<���_��������_�������>]
          int LvlValPos = FLvlStr.Pos("##");
          if (LvlValPos)
           { // ���� �������� �������
            RC.FormatVer = FLvlStr.SubString(1, LvlValPos - 1).ToIntDef(1);
            FLvlStr      = FLvlStr.SubString(LvlValPos + 2, FLvlStr.Length() - LvlValPos - 1);
           }
          else
           { // ��� �������� �������
            RC.FormatVer = FLvlStr.ToIntDef(1);
            FLvlStr      = "";
           }
         }
        else
         { // <���_�����������>.<�������_�����������������>
          FOrgCode = tmp;
         }
        if (FOrgCode.Length())
         {
          int NFPos = FOrgCode.Pos(".");
          RC.OrgID = FOrgCode.SubString(1, NFPos - 1).ToIntDef(0);
          RC.Type  = TOrgTypeValue(FOrgCode.SubString(NFPos + 1, FOrgCode.Length() - NFPos).ToIntDef(0));
         }
        if (FLvlStr.Length())
         {
          GetOrgLvlStruct(FLvlStr, RC.OKVED, RC.L1Val, RC.L2Val, RC.L3Val, RC.Format, RC.Period);
         }
       }
     }
    catch (...)
     {
      throw("Error parsing org string");
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall OrgLevVal(UnicodeString ASrc, UnicodeString ASep)
 {
  /*
   int RC = 999;
   try
   {
   if (A1 && (A1 < RC))
   RC = A1;
   if (A2 && (A2 < RC))
   RC = A2;
   if (A3 && (A3 < RC))
   RC = A3;
   if (A4 && (A4 < RC))
   RC = A4;
   if (A5 && (A5 < RC))
   RC = A5;
   if (RC == 999)
   RC = 0;
   }
   __finally
   {
   }
   return RC;
   */
  UnicodeString RC = "";
  try
   {
    int EPos = ASrc.Pos(ASep);
    if (EPos)
     {
      UnicodeString tmp = ASrc.SubString(EPos + 2, ASrc.Length() - EPos - 1).Trim();
      EPos = tmp.Pos("#");
      if (EPos)
       RC = tmp.SubString(1, EPos - 1).Trim();
      else
       RC = tmp.Trim();
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall GetOrgLvlStruct(UnicodeString ASrcStr, int & AOKVED, UnicodeString & AL1, UnicodeString & AL2,
  UnicodeString & AL3, UnicodeString & AFormat, __int64 & APeriod)
 {
  // <���_�����>[#F<��� �������>][#1<��������_������1>][#2<��������_������2>][#3<��������_������3>][#P<���_��������_�������>]
  bool RC = false;
  try
   {
    System::Sysutils::TReplaceFlags flRep;
    flRep << rfReplaceAll << rfIgnoreCase;
    UnicodeString tmp = StringReplace(ASrcStr.Trim(), "#f", "#F", flRep);
    UnicodeString tmp2;
    tmp = StringReplace(tmp, "#p", "#P", flRep);
    if (tmp.Length())
     {
      int Len;
      int EPos = tmp.Pos("#");
      if (EPos)
       Len = EPos - 1;
      else
       Len = tmp.Length();
      AOKVED  = tmp.SubString(1, Len).Trim().ToIntDef(0);
      tmp     = tmp.SubString(EPos, tmp.Length() - EPos + 1).Trim();
      AFormat = OrgLevVal(tmp, "#F");
      AL1     = OrgLevVal(tmp, "#1");
      AL2     = OrgLevVal(tmp, "#2");
      AL3     = OrgLevVal(tmp, "#3");
      APeriod = OrgLevVal(tmp, "#P").ToIntDef(0);
      RC      = true;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall GetLvlType(UnicodeString AVal)
 {
  // 0 - ����������
  // 1 - ��
  // 3 - �
  // 4 - �
  // 5 - ��
  int RC = 0;
  try
   {
    bool IsDig, Cont;
    Cont = true;
    for (int i = 1; (i <= AVal.Length()) && Cont; i++)
     {
      IsDig = (AVal[i] >= '0') && (AVal[i] <= '9');
      switch (RC)
       {
       case 0:
         {
          RC = 4 - IsDig;
          break;
         } // 0 - ����������
       case 1:
         {
          Cont = false;
          break;
         } // 1 - ��
       case 3:
         {
          if (!IsDig)
           RC = 1;
          break;
         } // 3 - �
       case 4:
         {
          if (IsDig)
           RC = 5;
          break;
         } // 4 - �
       case 5:
         {
          Cont = false;
          break;
         } // 5 - ��
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall DivideLevelVal(UnicodeString ASrc, UnicodeString & AVal)
 {
  UnicodeString RC = "";
  AVal = "";
  try
   {
    for (int i = 1; i <= ASrc.Length(); i++)
     {
      if ((ASrc[i] >= '0') && (ASrc[i] <= '9'))
       RC += ASrc[i];
      else
       AVal += ASrc[i];
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall IsLvlCorrectType(int AType, UnicodeString AVal)
 {
  // 0 - ��� ������
  // 1 - ��
  // 2 - �/�
  // 3 - �
  // 4 - �
  // 5 - ��
  bool RC = false;
  int FValType = GetLvlType(AVal);
  try
   {
    switch (AType)
     {
     case 1:
       {
        RC = true;
        break;
       }
     case 2:
       {
        break;
       }
     case 3:
       {
        break;
       }
     case 4:
       {
        break;
       }
     case 5:
       {
        RC = !((AVal[1] >= '0') && (AVal[1] <= '9'));
        break;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
orgCode __fastcall ParseOrgLvlStruct(TTagNode * AFormatDef, TNFOrgItems * AValues, int ALvlIdx, orgCode AOrg)
 {
  orgCode RC = AOrg;
  try
   {
    UnicodeString FCI;
    int FL1Type, FL2Type, FL3Type;
    int FL1ValType, FL2ValType, FL3ValType;
    UnicodeString FSF = "";
    GetOrgFormatTypeByOKVED(AFormatDef, AOrg.OKVED, FL1Type, FL2Type, FL3Type);
    UnicodeString FL1, FL2, FL3;
    FL1 = FL2 = FL3 = "";
    for (int i = ALvlIdx; i < AValues->Count; i++)
     {
      if ((i - ALvlIdx) == 0)
       FL1 = AValues->Item[i]->PreparedText;
      else if ((i - ALvlIdx) == 1)
       FL2 = AValues->Item[i]->PreparedText;
      else if ((i - ALvlIdx) == 2)
       FL3 = AValues->Item[i]->PreparedText;
     }
    // 0 - ��� ������
    // 1 - ��
    // 2 - �/�
    // 3 - �
    // 4 - �
    // 5 - ��
    if (FL1Type && FL1.Length())
     { // ����� ��� ������
      FL1ValType = GetLvlType(FL1);
      if (FL1Type == 2)
       {
        if ((FL1ValType == 1) || (FL1ValType == 5))
         { // �� || ��
          FL1 = DivideLevelVal(FL1, FL2);
         }
       }
      RC.L1Val = FL1;
      if (FL2Type && FL2.Length())
       { // ����� ��� ������
        RC.L2Val = FL2;
        if (FL3Type && FL3.Length())
         { // ����� ��� ������
          RC.L3Val = FL3;
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__fastcall orgCode::orgCode()
 {
  Clear();
 };
// ---------------------------------------------------------------------------
void __fastcall orgCode::Clear()
 {
  FormatVer = 1;
  OKVED     = Period = OrgID = 0;
  Type      = TOrgTypeValue::Base;
  Format    = L1Val = L2Val = L3Val = "";
 };
// ---------------------------------------------------------------------------
UnicodeString __fastcall orgCode::AsShortString()
 { // <���_�����������>.<�������_�����������������>
  UnicodeString RC = "";
  try
   {
    RC = IntToStr(OrgID) + "." + IntToStr((int)Type);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall orgCode::AsString()
 { // <���_�����������>.<�������_�����������������>@<������_�������>##<���_��������_�������>[#1<��������_������1>][#2<��������_������2>][#3<��������_������3>][#S<��������_���������>]
  UnicodeString RC = "";
  try
   {
    RC += AsShortString() + "@" + IntToStr(FormatVer) + LevelValStr();
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall orgCode::LevelValStr()
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString temp;
    if (OrgID && ((int)Type != 1))
     { // ������� ��������������� �����������
      temp = IntToStr(OKVED);
      if (Format.Trim().Length())
       temp += "#F" + Format.Trim();
      if (L1Val.Trim().Length())
       temp += "#1" + L1Val.Trim();
      if (L2Val.Trim().Length())
       temp += "#2" + L2Val.Trim();
      if (L3Val.Trim().Length())
       temp += "#3" + L3Val.Trim();
      if (Period)
       temp += "#P" + IntToStr(Period);
      RC += "##" + temp;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void orgCode:: operator = (orgCode AVal)
 {
  FormatVer = AVal.FormatVer;
  Type      = AVal.Type;
  OrgID     = AVal.OrgID;
  L1Val     = AVal.L1Val;
  L2Val     = AVal.L2Val;
  L3Val     = AVal.L3Val;
  Format    = AVal.Format;
  Period    = AVal.Period;
  OKVED     = AVal.OKVED;
 }
// ---------------------------------------------------------------------------
bool orgCode:: operator != (orgCode AVal)
 {
  bool RC = true;
  RC &= FormatVer == AVal.FormatVer;
  RC &= Type == AVal.Type;
  RC &= OrgID == AVal.OrgID;
  RC &= L1Val == AVal.L1Val;
  RC &= L2Val == AVal.L2Val;
  RC &= L3Val == AVal.L3Val;
  RC &= Format == AVal.Format;
  RC &= Period == AVal.Period;
  RC &= OKVED == AVal.OKVED;
  return !RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TOrgCacheRecord::AddLevelFormats(TTagNode * AFormatDef, TStringList * AFormatList)
 {
  orgFormats * FFormatData = new orgFormats;
  FFormatData->id    = AFormatDef->AV["uid"];
  FFormatData->L1Val = AFormatDef->AV["f1"];
  FFormatData->L2Val = AFormatDef->AV["f2"];
  FFormatData->L3Val = AFormatDef->AV["f3"];
  AFormatList->AddObject("", (TObject *)FFormatData);
 }
// ---------------------------------------------------------------------------
void __fastcall TOrgCacheRecord::GetOrgStrFormatByOKVED(TTagNode * AFormatDef, int AOKVED, UnicodeString AFormatCode,
  TStringList * AFormatList)
 {
  UnicodeString f1, f2, f3;
  f1 = "%s";
  f2 = ", %s";
  f3 = ", %s";
  orgFormats * FFormatData;
  try
   {
    if (AFormatDef)
     {
      bool FCont = true;
      if (AFormatCode.Length())
       {
        TTagNode * itFormatNode = AFormatDef->GetTagByUID(AFormatCode);
        if (itFormatNode && itFormatNode->CmpName("item"))
         {
          AddLevelFormats(itFormatNode, AFormatList);
          FCont = false;
         }
       }
      if (FCont)
       {
        TTagNode * itNode = AFormatDef->GetChildByName("content");
        TTagNode * itNode2;
        if (itNode)
         {
          f1     = itNode->AV["f1"];
          f2     = itNode->AV["f2"];
          f3     = itNode->AV["f3"];
          itNode = itNode->GetFirstChild();
          while (itNode)
           {
            if ((AOKVED >= itNode->AV["cfr"].ToIntDef(0)) && (AOKVED <= itNode->AV["cto"].ToIntDef(0)))
             {
              itNode2 = itNode->GetFirstChild();
              while (itNode2)
               {
                AddLevelFormats(itNode2, AFormatList);
                itNode2 = itNode2->GetNext();
               }
             }
            itNode = itNode->GetNext();
           }
          if (!AFormatList->Count)
           { // �� ������� �������� ��� ���������� ����
            FFormatData        = new orgFormats;
            FFormatData->id    = "";
            FFormatData->L1Val = f1;
            FFormatData->L2Val = f2;
            FFormatData->L3Val = f3;
            AFormatList->AddObject("", (TObject *)FFormatData);
           }
         }
       }
     }
    else
     {
      FFormatData        = new orgFormats;
      FFormatData->id    = "";
      FFormatData->L1Val = f1;
      FFormatData->L2Val = f2;
      FFormatData->L3Val = f3;
      AFormatList->AddObject("", (TObject *)FFormatData);
     }
   }
  __finally
   {
   }
 }
// ----------------------------------------------------------------------------
void __fastcall GetOrgFormatTypeByOKVED(TTagNode * AFormatDef, int AOKVED, int & ALvl1, int & ALvl2, int & ALvl3)
 {
  // 0 - ��� ������
  // 1 - ��
  // 2 - �/�
  // 3 - �
  // 4 - �
  // 5 - ��
  ALvl1 = 1;
  ALvl2 = 1;
  ALvl3 = 1;
  try
   {
    TTagNode * itNode = AFormatDef->GetChildByName("content");
    if (itNode)
     {
      itNode = itNode->GetFirstChild();
      while (itNode)
       {
        if ((AOKVED >= itNode->AV["cfr"].ToIntDef(0)) && (AOKVED <= itNode->AV["cto"].ToIntDef(0)))
         {
          ALvl1 = itNode->AV["type"].ToIntDef(1);
          ALvl2 = itNode->AV["type"].ToIntDef(1);
         }
        itNode = itNode->GetNext();
       }
     }
   }
  __finally
   {
   }
 }
// ----------------------------------------------------------------------------
