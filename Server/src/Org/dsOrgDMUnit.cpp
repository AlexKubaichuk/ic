// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsOrgDMUnit.h"
// #include "dsDocViewCreator.h"
// #include "dsDocSrvConstDef.h"
// #include "dsDocDocCreatorUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
// ---------------------------------------------------------------------------
#define MAX_SEARCH_COUNT 60
// ---------------------------------------------------------------------------
TdsOrgDM * dsOrgDM;
// ---------------------------------------------------------------------------
__fastcall TdsOrgDM::TdsOrgDM(TComponent * Owner, TFDConnection * AOrgConnection, TTagNode * AFormatDef)
  : TDataModule(Owner)
 {
  FConnection  = AOrgConnection;
  FUseCTTCache = false;
  FFormatDef   = AFormatDef;
 }
// ---------------------------------------------------------------------------
__fastcall TdsOrgDM::~TdsOrgDM()
 {
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsOrgDM::FCreateTempQuery(TFDConnection * AConnection)
 {
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = AConnection;
    FQ->Connection                       = AConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  return FQ;
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsOrgDM::CreateTempQuery()
 {
  return FCreateTempQuery(FConnection);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsOrgDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsOrgDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsOrgDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString AParam1Name,
  Variant AParam1Val, UnicodeString ALogComment, UnicodeString AParam2Name, Variant AParam2Val,
  UnicodeString AParam3Name, Variant AParam3Val, UnicodeString AParam4Name, Variant AParam4Val,
  UnicodeString AParam5Name, Variant AParam5Val)
 {
  bool RC = false;
  TTime FBeg = Time();
  UnicodeString FParams = "; Params: ";
  if (AParam1Name.Length())
   FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
  if (AParam2Name.Length())
   FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
  if (AParam3Name.Length())
   FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
  if (AParam4Name.Length())
   FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "") + "; ";
  if (AParam5Name.Length())
   FParams += AParam5Name + "=" + VarToStrDef(AParam5Val, "");
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
      if (AParam2Name.Length())
       AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
      if (AParam3Name.Length())
       AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
      if (AParam4Name.Length())
       AQuery->ParamByName(AParam4Name)->Value = AParam4Val;
      if (AParam5Name.Length())
       AQuery->ParamByName(AParam5Name)->Value = AParam5Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL("[" + IntToStr(AQuery->RecordCount) + "] " + ((ALogComment.Length()) ? ALogComment : ASQL) + FParams,
      __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsOrgDM::Find(UnicodeString AStr)
 {
  dsLogBegin(__FUNC__);
  TJSONObject * RC = new TJSONObject;
  TStringList * FOrg = new TStringList;
  UnicodeString tmpOrgStr, tmpOrgCode;
  UnicodeString FNFOrgStr = AStr.Trim();
  TStringList * FOrgLevels = new TStringList;
  orgCode FOrgCode;
  try
   {
    try
     {
      if (FNFOrgStr.Length())
       {
        if (FNFOrgStr.LowerCase().Pos("findbycode"))
         { // ����� �� ����
          FOrgCode.OrgID   = GetRPartB(FNFOrgStr, ':').ToIntDef(0);
          FOrgLevels->Text = codeToText(FOrgCode.AsString(), "11111110", true).Trim();
          if (FOrgLevels->Count)
           {
            RC->AddPair("msg", "ok");
            FOrgCode.Format = GetLPartB(FOrgLevels->Strings[0], ':');
            RC->AddPair(FOrgCode.AsString(), GetRPartB(FOrgLevels->Strings[0], ':'));
           }
         }
        else
         {
          if (textToCodeList(FNFOrgStr, FOrg, true))
           {
            RC->AddPair("msg", "ok");
            for (int i = 0; i < FOrg->Count; i++)
             {
              FOrgCode         = ParseOrgCodeStr(FOrg->Strings[i].Trim());
              FOrgLevels->Text = codeToText(FOrg->Strings[i].Trim(), "11111110").Trim();
              for (int j = 0; j < FOrgLevels->Count; j++)
               {
                FOrgCode.Format = GetLPartB(FOrgLevels->Strings[j], ':');
                RC->AddPair(FOrgCode.AsString(), GetRPartB(FOrgLevels->Strings[j], ':'));
               }
             }
           }
          else
           {
            for (int i = 0; i < FOrg->Count; i++)
             RC->AddPair("msg", FOrg->Strings[i]);
           }
         }
       }
      else
       RC->AddPair("msg", "������, ����������� ������ ��� ������.");
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    delete FOrg;
    delete FOrgLevels;
    dsLogEnd(__FUNC__);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsOrgDM::OrgCodeToText(UnicodeString code, UnicodeString params, bool ANotNFOrg,
  bool AReplaceCach)
 {
  UnicodeString RC = code;
  dsLogBegin(__FUNC__);
  TStringList * FOrgLevels = new TStringList;
  orgCode FOrgCode;
  try
   {
    try
     {
      FOrgCode         = ParseOrgCodeStr(code.Trim());
      FOrgLevels->Text = codeToText(FOrgCode.AsString(), params, ANotNFOrg, AReplaceCach).Trim();
      if (FOrgLevels->Count)
       {
        RC = GetRPartB(FOrgLevels->Strings[0], ':');
       }
     }
    catch (Exception & E)
     {
      dsLogMessage(E.Message);
     }
   }
  __finally
   {
    delete FOrgLevels;
    dsLogEnd(__FUNC__);
   }
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TdsOrgDM::codeToText(UnicodeString code, UnicodeString params, bool ANotNFOrg,
  bool AReplaceCach)
 {
  /*
   // ��������� ������� ����������� ����������� � ���������

   */
  dsLogBegin(__FUNC__);
  TOrgCacheRecord * temp = NULL;
  UnicodeString RC = "";
  int ss = 0;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    try
     {
      // dsLogMessage("1 -----------");
      // orgCode __fastcall ParseOrgCodeStr(UnicodeString AOrgStr, bool ATemplateMode)
      int FLvlIdx;
      if (code.Length())
       {
        // dsLogMessage("2 -----------");
        if (CTTCache && FUseCTTCache && CacheEntryExists(code) && !ANotNFOrg && !AReplaceCach)
         { // ������ ���� � ����
          // dsLogMessage("3 -----------");
          temp = (*CTTCache)[code];
         }
        else
         {
          // dsLogMessage("4 -----------");
          try
           {
            // dsLogMessage("5 -----------");
            temp = new TOrgCacheRecord(FFormatDef);
            // dsLogMessage("6 -----------");
            orgCode Orgcode = ParseOrgCodeStr(code);
            // dsLogMessage("7 -----------");
            // if (ANotNFOrg) Orgcode.State = 1;
            if (Orgcode.OrgID)
             {
              // dsLogMessage("8 -----------");
              if (Orgcode.Type != TOrgTypeValue::NF)
               { // ����������� �������������
                // dsLogMessage("9 -----------");
                quExecParam(FQ, false, "Select R0008, R00A3 FROM CLASS_0001 Where code=:pCode", "pCode", Orgcode.OrgID,
                  "������������ �����������");
                // dsLogMessage("10 -----------");
                if (FQ->RecordCount)
                 {
                  // dsLogMessage("11 -----------");
                  temp->Values[0] = FQ->FieldByName("R0008")->AsString;
                  temp->Values[1] = Orgcode.L1Val;
                  temp->Values[2] = Orgcode.L2Val;
                  temp->Values[3] = Orgcode.L3Val;
                  temp->Values[6] = Orgcode.Format;
                  // dsLogMessage("12 -----------");
                  if (Orgcode.Period)
                   {
                    // dsLogMessage("13 -----------");
                    quExecParam(FQ, false, "Select R010C FROM CLASS_0108 Where code=:pCode", "pCode", Orgcode.Period,
                      "������������ ������� ��������");
                    if (FQ->RecordCount)
                     temp->Values[4] = FQ->FieldByName("R010C")->AsString;
                    // dsLogMessage("14 -----------");
                   }
                  // dsLogMessage("15 -----------");
                  temp->Values[5] = FQ->FieldByName("R00A3")->AsString;
                  // dsLogMessage("16 -----------");
                 }
                // dsLogMessage("17 -----------");
                if (CTTCache && FUseCTTCache)
                 {
                  // dsLogMessage("18 -----------");
                  (*CTTCache)[code] = temp;
                  // dsLogMessage("19 -----------");
                 }
               }
              else
               { // ����������� �� �������������
                // dsLogMessage("20 -----------");
                quExecParam(FQ, false, "SELECT NAME FROM OrgNF where uid=:id", "id", Orgcode.OrgID);
                if (FQ->RecordCount)
                 temp->Values[0] = FQ->FieldByName("NAME")->AsString;
                // dsLogMessage("21 -----------");
                if (CTTCache && FUseCTTCache)
                 {
                  // dsLogMessage("22 -----------");
                  (*CTTCache)[code] = temp;
                  // dsLogMessage("23 -----------");
                 }
               }
             }
           }
          __finally
           {
            if (FQ->Transaction->Active)
             FQ->Transaction->Commit();
           }
         }
        if (temp)
         {
          // dsLogMessage("24 -----------");
          RC = temp->GetText(params, FFormatDef);
          // dsLogMessage("RC = "+RC);
          // dsLogMessage("25 -----------");
         }
       }
     }
    catch (Exception & E)
     {
      dsLogError(E.Message, __FUNC__);
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
//    if (CanDeleteTmp && temp)
//     delete temp;
    dsLogEnd(__FUNC__);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsOrgDM::textToCodeList(UnicodeString AOrg, TStringList * ARetCodes, bool ABeginFrom, bool ASilent)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  bool FOverMaxRC = false;
  TNFOrgItems * FOrgNF = new TNFOrgItems(AOrg, ','); // ','
  UnicodeString FPreparedOrgStr = "";
  FPreparedOrgStr = FPreparedOrgStr.Trim();
  TStringList * FOrgList = new TStringList;
  TStringList * FCurOrgList = new TStringList;
  int FFirstIdx = -1;
  try
   {
    UnicodeString FCurItem = "";
    if (FOrgNF->Count)
     {
      while ((FFirstIdx < FOrgNF->Count) && !FCurItem.Length())
       {
        FFirstIdx++ ;
        FCurItem = FOrgNF->Item[FFirstIdx]->PreparedText;
       }
      int OrgLvls;
      if (OrgTextToCodeList(FOrgNF, FCurOrgList, OrgLvls, ABeginFrom))
       { // ������� �����������
        FOverMaxRC = (FCurOrgList->Count > MAX_SEARCH_COUNT);
        orgCode FCurrOrg;
        for (int i = 0; (i < FCurOrgList->Count) && !FOverMaxRC; i++)
         { // ������ � ��������� ������
          FCurrOrg = ParseOrgCodeStr(FCurOrgList->Strings[i]);
          FCurrOrg = ParseOrgLvlStruct(FFormatDef, FOrgNF, OrgLvls, FCurrOrg);
          ARetCodes->Add(FCurrOrg.AsString());
         }
        RC = true;
       }
      if (!RC && FOverMaxRC)
       {
        RC = false;
        ARetCodes->Clear();
        if (!ASilent)
         ARetCodes->Add("���������� ��������� ����� " + IntToStr(MAX_SEARCH_COUNT) + ", �������� ������� ������.");
       }
     }
   }
  __finally
   {
    delete FOrgNF;
    delete FOrgList;
    delete FCurOrgList;
    dsLogEnd(__FUNC__);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsOrgDM::textToCodeList2(UnicodeString AOrg, TStringList * ARetCodes, bool ABeginFrom, bool ASilent)
 {
  dsLogBegin(__FUNC__);
  bool RC = false;
  bool FOverMaxRC = false;
  TStringList * FCurOrgList = new TStringList;
  TStringList * FNFOrgList = new TStringList;
  try
   {
    System::Sysutils::TReplaceFlags flRep;
    flRep << rfReplaceAll << rfIgnoreCase;
    FNFOrgList->Text = StringReplace(AOrg.Trim(), ",", "\n", flRep);
    FNFOrgList->Text = StringReplace(FNFOrgList->Text.Trim(), ",", "\n", flRep);
    UnicodeString FCurItem = "";
    if (FNFOrgList->Count)
     {
      int OrgLvls = OrgTextToCodeList2(FNFOrgList, FCurOrgList);
      if (OrgLvls)
       { // ������� �����������
        FOverMaxRC = (FCurOrgList->Count > MAX_SEARCH_COUNT);
        orgCode FCurrOrg;
        for (int i = 0; (i < FCurOrgList->Count) && !FOverMaxRC; i++)
         { // ������ � ��������� ������
          FCurrOrg = ParseOrgCodeStr(FCurOrgList->Strings[i]);
          // FCurrOrg = ParseOrgLvlStruct(FOrgNF, OrgLvls, FCurrOrg);
          ARetCodes->Add(FCurrOrg.AsString());
         }
        RC = true;
       }
      if (!RC && FOverMaxRC)
       {
        ARetCodes->Clear();
        if (!ASilent)
         ARetCodes->Add("���������� ��������� ����� " + IntToStr(MAX_SEARCH_COUNT) + ", �������� ������� ������.");
       }
     }
   }
  __finally
   {
    delete FCurOrgList;
    dsLogEnd(__FUNC__);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsOrgDM::CacheEntryExists(UnicodeString code)
 {
  dsLogBegin(__FUNC__);
  CTTCacheMap::iterator i = CTTCache->find(code);
  dsLogEnd(__FUNC__);
  return (i != CTTCache->end());
 }
// ---------------------------------------------------------------------------
int __fastcall TdsOrgDM::FGetLastLevelIdx(int ALvl1, int ALvl2, int ALvl3, int ALvl4)
 {
  if (ALvl4)
   return 3;
  else if (ALvl3)
   return 2;
  else if (ALvl2)
   return 1;
  else if (ALvl1)
   return 0;
  else
   return -1;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsOrgDM::PrepareStreetObjectVal(UnicodeString AVal)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = AVal;
  try
   {
    for (int i = 1; i <= RC.Length(); i++)
     {
      if (RC[i] == '0')
       RC[i] = ' ';
      else
       i = RC.Length() + 1;
     }
   }
  __finally
   {
    dsLogEnd(__FUNC__);
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsOrgDM::OrgTextToCodeList(TNFOrgItems * AValues, TStringList * ARetCodes, int & ALvl, bool ABeginFrom)
 {
  dsLogBegin(__FUNC__);
  ALvl = 0;
  bool RC = false;
  TFDQuery * FQ = CreateTempQuery();
  TFDQuery * FQ2 = CreateTempQuery();
  try
   {
    orgCode tmpOrgCode;
    System::Sysutils::TReplaceFlags flRep;
    flRep << rfReplaceAll << rfIgnoreCase;
    UnicodeString FSearchText = "";
    try
     {
      for (int mwc = 1; (mwc > 0) && !RC; mwc--) // ���� mwc = 3
       {
        ALvl        = mwc;
        FSearchText = "";
        for (int i = 0; (i < AValues->Count) && (i < mwc) && !RC; i++)
         {
          if (AValues->Item[i]->PreparedText.Length())
           {
            if (FSearchText.Length())
             FSearchText += "%";
            FSearchText += AValues->Item[i]->PreparedText;
           }
         }
        FSearchText = FSearchText.Trim();
        if (FSearchText.Length())
         {
          FSearchText = StringReplace(FSearchText, " ", "%", flRep);
          quExecParam(FQ, false, "SELECT distinct code, r00A3 from class_0001 where Upper(r0008) like :pName", "pNAME",
            ("%" + FSearchText + "%").UpperCase());
          if (FQ->RecordCount)
           {
            while (!FQ->Eof)
             {
              tmpOrgCode.Clear();
              tmpOrgCode.OrgID = FQ->FieldByName("code")->AsInteger;
              // <class name='������������� �����������' uid='32F2' inlist='l'>
              quExecParam(FQ2, false, "SELECT code from class_32F2 where r32F4 = :pOrgCode", "pOrgCode",
                FQ->FieldByName("code")->AsString);
              if (FQ2->RecordCount)
               {
                tmpOrgCode.Type = TOrgTypeValue::Main;
               }
              else
               {
                // <class name='�������������� �����������' uid='32F3' inlist='l'>
                quExecParam(FQ2, false, "SELECT code from class_32F3 where r32F5 = :pOrgCode", "pOrgCode",
                  FQ->FieldByName("code")->AsString);
                if (FQ2->RecordCount)
                 {
                  tmpOrgCode.Type = TOrgTypeValue::Control;
                 }
               }
              tmpOrgCode.OKVED = FQ->FieldByName("r00A3")->AsInteger;
              AddUnique(ARetCodes, tmpOrgCode.AsString());
              FQ->Next();
             }
            RC = true;
           }
         }
       }
     }
    catch (Exception & E)
     {
      throw EOrgSearchException("������ ������.\n��������� ���������:\n" + E.Message);
     }
    catch (...)
     {
      throw EOrgSearchException("Searching error");
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
    if (FQ2->Transaction->Active)
     FQ2->Transaction->Commit();
    DeleteTempQuery(FQ2);
    dsLogEnd(__FUNC__);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsOrgDM::OrgTextToCodeList2(TStringList * AValues, TStringList * ARetCodes)
 {
  /*
   dsLogBegin(__FUNC__);
   int RC = 0;
   TFDQuery * FQ = CreateTempQuery();
   TFDQuery * FQ2 = CreateTempQuery();
   try
   {
   orgCode tmpOrgCode;
   System::Sysutils::TReplaceFlags flRep;
   flRep << rfReplaceAll << rfIgnoreCase;
   UnicodeString FSearchText = "";
   try
   {
   for (int mwc = 1; (mwc > 0) && !RC; mwc--) // ���� mwc = 3
   {
   RC        = mwc;
   FSearchText = "";
   for (int i = 0; (i < AValues->Count) && (i < mwc) && !RC; i++)
   {
   if (AValues->Item[i]->PreparedText.Length())
   {
   if (FSearchText.Length())
   FSearchText += "%";
   FSearchText += AValues->Item[i]->PreparedText;
   }
   }
   FSearchText = FSearchText.Trim();
   if (FSearchText.Length())
   {
   FSearchText = StringReplace(FSearchText, " ", "%", flRep);
   quExecParam(FQ, false, "SELECT distinct code, r00A3 from class_0001 where Upper(r0008) like :pName", "pNAME",
   ("%" + FSearchText + "%").UpperCase());
   if (FQ->RecordCount)
   {
   while (!FQ->Eof)
   {
   tmpOrgCode.Clear();
   tmpOrgCode.OrgID = FQ->FieldByName("code")->AsInteger;
   // <class name='������������� �����������' uid='32F2' inlist='l'>
   quExecParam(FQ2, false, "SELECT code from class_32F2 where r32F4 = :pOrgCode", "pOrgCode",
   FQ->FieldByName("code")->AsString);
   if (FQ2->RecordCount)
   {
   tmpOrgCode.Type = TOrgTypeValue::Main;
   }
   else
   {
   // <class name='�������������� �����������' uid='32F3' inlist='l'>
   quExecParam(FQ2, false, "SELECT code from class_32F3 where r32F5 = :pOrgCode", "pOrgCode",
   FQ->FieldByName("code")->AsString);
   if (FQ2->RecordCount)
   {
   tmpOrgCode.Type = TOrgTypeValue::Control;
   }
   }
   tmpOrgCode.OKVED = FQ->FieldByName("r00A3")->AsInteger;
   AddUnique(ARetCodes, tmpOrgCode.AsString());
   FQ->Next();
   }
   RC = true;
   }
   }
   }
   }
   catch (Exception & E)
   {
   throw EOrgSearchException("������ ������.\n��������� ���������:\n" + E.Message);
   }
   catch (...)
   {
   throw EOrgSearchException("Searching error");
   }
   }
   __finally
   {
   if (FQ->Transaction->Active)
   FQ->Transaction->Commit();
   DeleteTempQuery(FQ);
   if (FQ2->Transaction->Active)
   FQ2->Transaction->Commit();
   DeleteTempQuery(FQ2);
   dsLogEnd(__FUNC__);
   }
   return RC;
   */
  return 0;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsOrgDM::GetOrgField(__int64 AOrgCode, UnicodeString AFlName)
 {
  dsLogBegin(__FUNC__);
  UnicodeString RC = "";
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    UnicodeString FFlName = AFlName.Trim();
    TAnsiStrMap::iterator FRC = FFlValMap.find((FFlName + "_" + IntToStr(AOrgCode)).UpperCase());
    if (FRC != FFlValMap.end())
     {
      RC = FRC->second;
     }
    else
     {
      quExecParam(FQ, false, "SELECT distinct " + FFlName + " from class_0001 where code=:pCode", "pCode", AOrgCode);
      if (FQ->RecordCount)
       {
        __int64 FExtCode = -1;
        if (FFlName.UpperCase() == "R0008")
          // text name='������������' ref='no' required='1' inlist='l' depend='no' cast='no' length='100'/>
           RC = FQ->FieldByName(FFlName)->AsString;
        else if (FFlName.UpperCase() == "R00AF")
          // binary name='�������������' inlist='e' invname='���������������' default='check'/>
           RC = (FQ->FieldByName(FFlName)->AsInteger) ? "��" : "���";
        else if (FFlName.UpperCase() == "R00B1") // binary name='��������������' inlist='e' invname='����������������'>
           RC = (FQ->FieldByName(FFlName)->AsInteger) ? "��" : "���";
        else if (FFlName.UpperCase() == "R0107") // text name='�����' inlist='e' cast='no' linecount='3' length='255'/>
           RC = FQ->FieldByName(FFlName)->AsString;
        else if (FFlName.UpperCase() == "R0009") // text name='�������' ref='no' inlist='l' depend='no' length='20'/>
           RC = FQ->FieldByName(FFlName)->AsString;
        else if (FFlName.UpperCase() == "R00A3")
          // choiceTree name='��� ������������ �� �����' ref='00A1' required='1' inlist='e' dlgtitle='��� ������������' linecount='3'/>
         {
          FExtCode = FQ->FieldByName(FFlName)->AsInteger;
         }
        else if (FFlName.UpperCase() == "R3196") // choiceDB name='���' ref='00C7' default='Main'/>
         {
          quExecParam(FQ, false, "SELECT distinct R00CA from class_00C7 where code=:pCode", "pCode",
            FQ->FieldByName(FFlName)->AsInteger);
          if (FQ->RecordCount)
           RC = FQ->FieldByName("R00CA")->AsString;
         }
        else if (FFlName.UpperCase() == "R011A") // choiceDB name='������' ref='0117' inlist='e'/>
         {
          quExecParam(FQ, false, "SELECT distinct R0119 from class_0117 where code=:pCode", "pCode",
            FQ->FieldByName(FFlName)->AsInteger);
          if (FQ->RecordCount)
           RC = FQ->FieldByName("R0119")->AsString;
         }
        else if (FFlName.UpperCase() == "R010A") // choiceDB name='�������������' ref='0109' inlist='e'/>
         {
          quExecParam(FQ, false, "SELECT distinct R0110 from class_0109 where code=:pCode", "pCode",
            FQ->FieldByName(FFlName)->AsInteger);
          if (FQ->RecordCount)
           RC = FQ->FieldByName("R0110")->AsString;
         }
       }
      if (RC.Length())
       FFlValMap[(FFlName + "_" + IntToStr(AOrgCode)).UpperCase()] = RC;
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    DeleteTempQuery(FQ);
    dsLogEnd(__FUNC__);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsOrgDM::AddUnique(TStringList * ADest, UnicodeString ASrc)
 {
  if (ADest->IndexOf(ASrc) == -1)
   ADest->Add(ASrc);
 }
// ---------------------------------------------------------------------------
