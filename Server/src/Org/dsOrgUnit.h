//---------------------------------------------------------------------------

#ifndef dsOrgUnitH
#define dsOrgUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
//---------------------------------------------------------------------------
#include <FireDAC.Phys.FB.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
//---------------------------------------------------------------------------
#include "dsOrgDMUnit.h"
//---------------------------------------------------------------------------
class DECLSPEC_DRTTI TdsOrgClass : public TDataModule
{
__published:	// IDE-managed Components
 TFDConnection *OrgConnection;
private:	// User declarations
  TdsOrgDM        *FDM;
//  TTagNode *FFormatDef;

public:		// User declarations
  __fastcall TdsOrgClass(TComponent* Owner, TTagNode *AFormatDef, TAppOptions *AOpt);
  __fastcall ~TdsOrgClass();

  TJSONObject* Find(UnicodeString AStr);
  UnicodeString OrgCodeToText(UnicodeString ACode, UnicodeString ASetting = "11111111");
  UnicodeString __fastcall GetOrgField(__int64 AOrgCode, UnicodeString AFlName);
};
//---------------------------------------------------------------------------
#endif
