//---------------------------------------------------------------------------

#ifndef dsOrgDMUnitH
#define dsOrgDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>

#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
//---------------------------------------------------------------------------
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.VCLUI.Wait.hpp>
#include <System.JSON.hpp>

#include "kabQueryUtils.h"
#include "XMLContainer.h"
//#include "dsDocXMLUtils.h"
#include "icsLog.h"
//#include "dsDocSrvTypeDef.h"

#include "OrgParsers.h"

//#include "dsSrvClassifUnit.h"
//---------------------------------------------------------------------------
class EOrgSearchException : public Exception
{
    public:
    __fastcall EOrgSearchException (const UnicodeString msg) : Exception (msg) { };
};
//---------------------------------------------------------------------------
class TdsOrgDM : public TDataModule
{
__published:	// IDE-managed Components
private:	// User declarations
  typedef map <UnicodeString, TOrgCacheRecord*> CTTCacheMap;
  TFDConnection *FConnection;
  CTTCacheMap*   CTTCache; // ����� ��� ����
  TAnsiStrMap    FFlValMap;
  bool           FUseCTTCache;     // ���� ������������� ���� ��� ������� CodeToText, CodeToIndeks
  TTagNode *FFormatDef;
//  bool           expandSocrs;

//  StrStrMap*         socr; // ����� ���������� (���� = ���������� � �.�.)

  TFDQuery*     __fastcall FCreateTempQuery(TFDConnection *AConnection);
  bool          __fastcall textToCodeList(UnicodeString AOrg, TStringList *ARetCodes, bool ABeginFrom = false, bool ASilent=false);
  bool          __fastcall textToCodeList2(UnicodeString AOrg, TStringList * ARetCodes, bool ABeginFrom = false, bool ASilent=false);
  bool          __fastcall CacheEntryExists(UnicodeString code);
  int           __fastcall FGetLastLevelIdx(int ALvl1, int ALvl2, int ALvl3, int ALvl4);
  UnicodeString __fastcall PrepareStreetObjectVal(UnicodeString AVal);
  bool          __fastcall OrgTextToCodeList(TNFOrgItems *AValues, TStringList *ARetCodes, int &ALvl, bool ABeginFrom = false);
  int           __fastcall OrgTextToCodeList2(TStringList * AValues, TStringList * ARetCodes);
  void          __fastcall AddUnique(TStringList *ADest, UnicodeString ASrc);


  UnicodeString __fastcall codeToText(UnicodeString code, UnicodeString  params ="11111111", bool ANotNFOrg = false, bool AReplaceCach = false);
public:		// User declarations
  __fastcall TdsOrgDM(TComponent* Owner, TFDConnection *AOrgConnection, TTagNode *AFormatDef);
  __fastcall ~TdsOrgDM();

  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AParam1Name,      Variant AParam1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AParam2Name = "", Variant AParam2Val = 0,
                               UnicodeString AParam3Name = "", Variant AParam3Val = 0,
                               UnicodeString AParam4Name = "", Variant AParam4Val = 0,
                               UnicodeString AParam5Name = "", Variant AParam5Val = 0);

  TJSONObject* __fastcall Find(UnicodeString AStr);
  UnicodeString __fastcall OrgCodeToText(UnicodeString code, UnicodeString  params ="11111111", bool ANotNFOrg = false, bool AReplaceCach = false);
  UnicodeString __fastcall GetOrgField(__int64 AOrgCode, UnicodeString AFlName);
  __property bool UseCache = {read=FUseCTTCache, write=FUseCTTCache};     // ���� ������������� ���� ��� ������� CodeToText, CodeToIndeks

};
//---------------------------------------------------------------------------
extern PACKAGE TdsOrgDM *dsOrgDM;
//---------------------------------------------------------------------------
#endif
