//---------------------------------------------------------------------------

#ifndef dsXMLManagerUnitH
#define dsXMLManagerUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "XMLContainer.h"
#include "icsLog.h"
//---------------------------------------------------------------------------
class TXMLManager : public TAxeXMLContainer
{
private:	// User declarations

  bool __fastcall FLoadXML(TTagNode *ItTag, UnicodeString &Src);
public:		// User declarations

  __fastcall TXMLManager();
  __fastcall ~TXMLManager();

};
//---------------------------------------------------------------------------
#endif
