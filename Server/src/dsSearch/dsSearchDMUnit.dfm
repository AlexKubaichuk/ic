object dsSearchDM: TdsSearchDM
  OldCreateOrder = False
  Height = 482
  Width = 711
  object SearchCon: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'CharacterSet=UTF8'
      'User_Name=sysdba'
      'Server=localhost'
      'Port=3050'
      'Password=masterkey'
      'Pooled=False')
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    FormatOptions.AssignedValues = [fvFmtDisplayDateTime, fvFmtDisplayDate, fvFmtDisplayTime]
    FormatOptions.FmtDisplayDateTime = 'dd.mm.yyyy hh:nn:ss'
    FormatOptions.FmtDisplayDate = 'dd.mm.yyyy'
    FormatOptions.FmtDisplayTime = 'hh:nn:ss'
    ResourceOptions.AssignedValues = [rvDirectExecute, rvAutoConnect, rvSilentMode]
    ResourceOptions.DirectExecute = True
    ResourceOptions.SilentMode = True
    ResourceOptions.AutoConnect = False
    TxOptions.AutoStart = False
    TxOptions.AutoStop = False
    TxOptions.DisconnectAction = xdRollback
    LoginPrompt = False
    Left = 29
    Top = 15
  end
end
