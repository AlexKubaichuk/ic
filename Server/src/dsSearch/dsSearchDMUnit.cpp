// ---------------------------------------------------------------------------
#include <vcl.h>

#pragma hdrstop

#include "dsSearchDMUnit.h"
// #include "ICSDATEUTIL.hpp"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TdsSearchDM * dsSearchDM;
// ---------------------------------------------------------------------------
__fastcall TdsSearchDM::TdsSearchDM(TComponent * Owner, const UnicodeString ADBPath, TAppOptions *AOpt) : TDataModule(Owner)
 {
  dsLogC(__FUNC__);
  if (ADBPath.Length())
   {
    ConnectDB(SearchCon, AOpt, ADBPath, __FUNC__);
   }
  dsLogC(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
__fastcall TdsSearchDM::~TdsSearchDM()
 {
  dsLogD(__FUNC__);
  DisconnectDB(SearchCon, __FUNC__);
  dsLogD(__FUNC__, true);
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TdsSearchDM::CreateTempQuery()
 {
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection = SearchCon;
    FQ->Connection              = SearchCon;

    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  return FQ;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSearchDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSearchDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSearchDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString AParam1Name,
 Variant AParam1Val, UnicodeString ALogComment, UnicodeString AParam2Name, Variant AParam2Val,
 UnicodeString AParam3Name, Variant AParam3Val, UnicodeString AParam4Name, Variant AParam4Val)
 {
  bool RC = false;
  TTime FBeg = Time();
  UnicodeString FParams = "; Params: ";
  if (AParam1Name.Length())
   FParams += AParam1Name + "=" + VarToStrDef(AParam1Val, "") + "; ";
  if (AParam2Name.Length())
   FParams += AParam2Name + "=" + VarToStrDef(AParam2Val, "") + "; ";
  if (AParam3Name.Length())
   FParams += AParam3Name + "=" + VarToStrDef(AParam3Val, "") + "; ";
  if (AParam4Name.Length())
   FParams += AParam4Name + "=" + VarToStrDef(AParam4Val, "");
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AParam1Name)->Value = AParam1Val;
      if (AParam2Name.Length())
       AQuery->ParamByName(AParam2Name)->Value = AParam2Val;
      if (AParam3Name.Length())
       AQuery->ParamByName(AParam3Name)->Value = AParam3Val;
      if (AParam4Name.Length())
       AQuery->ParamByName(AParam4Name)->Value = AParam4Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      dsLogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    dsLogSQL(((ALogComment.Length()) ? ALogComment : ASQL) + FParams, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSearchDM::Update(UnicodeString ACode, UnicodeString AStr)
 {
  // FUnitListSearch->Add(ACode, AStr);
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall FNStr(TFDQuery * AQ, UnicodeString AFlName)
 {
  return AQ->FieldByName(AFlName)->AsString;
 }
// ----------------------------------------------------------------------------
TJSONObject * __fastcall TdsSearchDM::Find(UnicodeString AId, UnicodeString AFindData)
 {
  dsLogMessage("Find: ID: " + AId + ", FindParam: " + AFindData);
  TJSONArray * RC = new TJSONArray;
  TFDQuery * FQ = CreateTempQuery();
  TStringList * FSWords = new TStringList;
  UnicodeString FSearchIdx;
  bool ContinueSearch = false;
  TTagNode * FlDef = NULL;
  TTagNode * itFl, *itFlDef;
  UnicodeString FSearchFields[3] =
   {
    "001F",
    "0020",
    "002F"
   };
  if (AId != "1000")
   {
    TTagNode * ClsDef = FRegDef->GetTagByUID(AId);
    if (ClsDef)
     {
      FlDef = ClsDef->GetChildByName("fields", true);
      if (FlDef)
       {
        itFl = FlDef->GetFirstChild();
        while (itFl && !ContinueSearch)
         {
          itFlDef = itFl->GetTagByUID(itFl->AV["ref"]);
          if (itFlDef)
           ContinueSearch |= itFlDef->CmpName("text");
          itFl = itFl->GetNext();
         }
       }
     }
   }
  else
   ContinueSearch = true;
  try
   {
    if (ContinueSearch)
     {
      try
       {
        TReplaceFlags FRepFl;
        FRepFl << rfReplaceAll;
        bool Findbyfield = AFindData.Pos("findbyfield");
        if (Findbyfield)
         FSWords->Text = GetRPartB(StringReplace(AFindData.UpperCase(), " ", "\n", FRepFl), ':');
        else
         FSWords->Text = StringReplace(AFindData.UpperCase(), " ", "\n", FRepFl);
        TAnsiStrMap::iterator FRC;
        FFindDataCode.clear();
        if (Findbyfield)
         {
          bool FirstFind = true;
          for (int i = FSWords->Count - 1; i > 2; i--) // ����� ���� �� ����� ��������� ���������� �����(3)
            FSWords->Delete(i);
          for (int i = 0; (i < FSWords->Count) && (FirstFind || (!FirstFind && FFindDataCode.size())); i++)
           {
            if (FSWords->Strings[i].Trim() != "*")
             { // "*"  - �������� ���� � ������ ������� ������  - ������, "* ����" - ����� ���� � ������ "����";
              FCurFindCodes.clear();
              quExecParam(FQ, false,
               "Select distinct(code), flname, sortidx from search_data where strval STARTING WITH :sVal and FLNAME=:pFLNAME",
               "sVal", FSWords->Strings[i], "", "pFLNAME", FSearchFields[i]);
              while (!FQ->Eof) // ����� �����
               {
                FSearchIdx = FNStr(FQ, "sortidx").UpperCase() + "_" + FNStr(FQ, "code");
                if (FirstFind)
                 FFindDataCode[FSearchIdx] = FNStr(FQ, "code");
                else
                 FCurFindCodes[FSearchIdx] = FNStr(FQ, "code");
                FQ->Next();
               }
              if (!FirstFind)
               { // �� ������ ������
                FEraseDataCode.clear();
                for (TAnsiStrMap::iterator sci = FFindDataCode.begin(); sci != FFindDataCode.end(); sci++)
                 {
                  if (FCurFindCodes.find(sci->first) == FCurFindCodes.end())
                   FEraseDataCode[sci->first] = 1;
                 }
                for (TAnsiStrMap::iterator sci = FEraseDataCode.begin(); sci != FEraseDataCode.end(); sci++)
                 FFindDataCode.erase(sci->first);
               }
              FirstFind = false;
             }
           }
         }
        else
         {
          for (int i = 0; (i < FSWords->Count) && (!i || (i && FFindDataCode.size())); i++)
           {
            FCurFindCodes.clear();
            if (AId == "1000")
             {
              quExecParam(FQ, false,
               "Select distinct(code), flname, sortidx from search_data where strval STARTING WITH :sVal", "sVal",
               FSWords->Strings[i]);
              while (!FQ->Eof)
               {
                FSearchIdx = FNStr(FQ, "sortidx").UpperCase().Trim() + "_" + FNStr(FQ, "code");
                if (!i)
                 FFindDataCode[FSearchIdx] = FNStr(FQ, "code");
                else
                 FCurFindCodes[FSearchIdx] = FNStr(FQ, "code");
                FQ->Next();
               }
              quExecParam(FQ, false,
               "Select distinct(code), flname, sortidx from search_addr_data where strval STARTING WITH :sVal", "sVal",
               FSWords->Strings[i]);
              while (!FQ->Eof) // ����� �� ������
               {
                FSearchIdx = FNStr(FQ, "sortidx").UpperCase().Trim() + "_" + FNStr(FQ, "code");
                if (!i)
                 FFindDataCode[FSearchIdx] = FNStr(FQ, "code");
                else
                 FCurFindCodes[FSearchIdx] = FNStr(FQ, "code");
                FQ->Next();
               }
              quExecParam(FQ, false,
               "Select distinct(code), flname, sortidx from search_org_data where strval STARTING WITH :sVal", "sVal",
               FSWords->Strings[i]);
              while (!FQ->Eof) // ����� �� �����������
               {
                FSearchIdx = FNStr(FQ, "sortidx").UpperCase().Trim() + "_" + FNStr(FQ, "code");
                if (!i)
                 FFindDataCode[FSearchIdx] = FNStr(FQ, "code");
                else
                 FCurFindCodes[FSearchIdx] = FNStr(FQ, "code");
                FQ->Next();
               }
             }
            else
             {
              TTagNode * itFl = FlDef->GetFirstChild();
              while (itFl)
               {
                itFlDef = itFl->GetTagByUID(itFl->AV["ref"]);

                if (itFlDef)
                 {
                  if (itFlDef->CmpName("text"))
                   {
                    quExecParam(FQ, false, "Select distinct(code) from class_" + AId + " where Upper(r" +
                     itFl->AV["ref"] + ") STARTING WITH :sVal", "sVal", FSWords->Strings[i]);
                    while (!FQ->Eof)
                     {
                      FSearchIdx = "_1_" + FNStr(FQ, "code");
                      if (!i)
                       FFindDataCode[FSearchIdx] = FNStr(FQ, "code");
                      else
                       FCurFindCodes[FSearchIdx] = FNStr(FQ, "code");
                      FQ->Next();
                     }
                   }
                 }
                itFl = itFl->GetNext();
               }
             }
            if (i)
             { // �� ������ ������
              FEraseDataCode.clear();
              for (TAnsiStrMap::iterator sci = FFindDataCode.begin(); sci != FFindDataCode.end(); sci++)
               {
                if (FCurFindCodes.find(sci->first) == FCurFindCodes.end())
                 FEraseDataCode[sci->first] = 1;
               }
              for (TAnsiStrMap::iterator sci = FEraseDataCode.begin(); sci != FEraseDataCode.end(); sci++)
               FFindDataCode.erase(sci->first);
             }
           }
         }

        for (TAnsiStrMap::iterator sci = FFindDataCode.begin(); sci != FFindDataCode.end(); sci++)
         RC->Add(sci->second);
        dsLogMessage("Count: " + IntToStr(RC->Count), __FUNC__);
       }
      catch (System::Sysutils::Exception & E)
       {
        dsLogError(E.Message, __FUNC__);
       }
     }
   }
  __finally
   {
    FCurFindCodes.clear();
    FFindDataCode.clear();
    FEraseDataCode.clear();
    delete FSWords;
    if (FQ->Transaction->Active)
     FQ->Transaction->Rollback();
    DeleteTempQuery(FQ);
   }
  return (TJSONObject *)RC;
 }
// ----------------------------------------------------------------------------
