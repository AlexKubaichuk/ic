//---------------------------------------------------------------------------
#ifndef dsMISManagerH
#define dsMISManagerH

#include <System.hpp>
#include <Classes.hpp>
//#include <DateUtils.hpp>

//#include "pFIBDatabase.hpp"
//#include "DKClasses.h"
//#include "XMLContainer.h"
//#include "ICSDATEUTIL.hpp"
//#include "DKUtils.h"

//---------------------------------------------------------------------------
#include "dsMISManagerDMUnit.h"
#include "dsMISSyncModule.h"
#include "dsDBSupport.h"
//---------------------------------------------------------------------------
class PACKAGE TMISSyncManager : public TObject
{
typedef void __fastcall (__closure *TdsSyncMethod)(void);
private:
  class TBackgroundMISSync;
  TBackgroundMISSync * FBkgSyncModule;
  bool FTerminated;
  bool FInSync;

  int             FCurrProgress;
  int             FMaxProgress;        // Max �������� ��� ProgressBar'�
  TNotifyEvent    FOnIncProgress;      // �������, ��� ��������� �������� ���������� ������� ��������� ������� ProgressBar'�
  TAppOptions     *FOpt;
  UnicodeString FStage;

  UnicodeString FLogMsg, FLogFuncName;
  int           FLogALvl;
  TTime         FLogExecTime;

  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;
  TOnLogSQLEvent      FOnLogSQL;

  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;


  void __fastcall SyncCall(TdsSyncMethod AMethod);

  void __fastcall CallOnIncProgress();
  void __fastcall SyncCallOnIncProgress();


//  TdsMISSyncDM* FDM;

  typedef map<UnicodeString, TMISSync*> TMISMap;
  TdsMISManagerDM *FDM;
  TMISMap FMISList;

  typedef map<UnicodeString, TdsDBSupport*> TDBSupportMap;
  typedef map<UnicodeString, TDate> TDBSupportDateMap;
  TDBSupportMap FDBSupportMap;
  TDBSupportDateMap FDBSupportDateMap;
  TTime FBTime, FETime;
  TDate FDBSupportDate;

  void __fastcall LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl);
  void __fastcall LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl);
  void __fastcall LogBegin(UnicodeString AFuncName);
  void __fastcall LogEnd(UnicodeString AFuncName);

  UnicodeString __fastcall GetDBId(UnicodeString  ACode);

  UnicodeString __fastcall MISName(int AMisType);
  UnicodeString __fastcall MISIdListFunc(int AMisType);
  UnicodeString __fastcall MISPatDataFunc(int AMisType);

  bool __fastcall CheckShedule();
  bool __fastcall CheckDBSupportShedule();

protected:
  void __fastcall SyncCallLogMsg();
  void __fastcall SyncCallLogErr();
  void __fastcall SyncCallLogSQL();
  void __fastcall SyncCallLogBegin();
  void __fastcall SyncCallLogEnd();

  void __fastcall CallLogMsg(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogErr(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl = 1);
  void __fastcall CallLogBegin(UnicodeString AFuncName);
  void __fastcall CallLogEnd(UnicodeString AFuncName);

  UnicodeString __fastcall Sync();
public:
  __fastcall TMISSyncManager(TAxeXMLContainer *AXMLList, TAppOptions *AOpt);
  virtual __fastcall ~TMISSyncManager();

    UnicodeString __fastcall StartSync();
    UnicodeString __fastcall StopSync();

    __property bool Terminated = {read = FTerminated};
    __property bool InSync = {read = FInSync};

    int __fastcall GetMaxProgress();
    int __fastcall GetCurProgress();
    UnicodeString __fastcall GetCurStage() const;


    __property int          MaxProgress   = { read = FMaxProgress };
    __property TNotifyEvent OnIncProgress = { read = FOnIncProgress, write = FOnIncProgress };

    __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
    __property TOnLogMsgErrEvent OnLogError   = {read = FOnLogError, write = FOnLogError};
    __property TOnLogSQLEvent    OnLogSQL     = {read = FOnLogSQL, write = FOnLogSQL};

    __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
    __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};


  __property  TTime SyncStartTime = {read=FBTime, write=FBTime};
  __property  TTime SyncStopTime = {read=FETime, write=FETime};

  System::UnicodeString __fastcall CreatePlan();
};
//---------------------------------------------------------------------------
#endif
