//---------------------------------------------------------------------------
#ifndef dsIC67SyncH
#define dsIC67SyncH
//---------------------------------------------------------------------------
#include <System.hpp>
#include <Classes.hpp>
#include "dsMISBaseSync.h"
#include <FireDAC.Comp.Client.hpp>
//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                              TIC67Sync                                ##
//##                                                                       ##
//###########################################################################

//---------------------------------------------------------------------------
typedef map<UnicodeString, TJSONObject*> TJSONMap;
typedef UnicodeString __fastcall (__closure *TCheckAddrEvent)(UnicodeString AAddr, UnicodeString AHouse, UnicodeString ABuilding, UnicodeString AFlat);
class PACKAGE TIC67Sync : public TdsMISBaseSync
{
private:
  TFDConnection *FIC67Connection;
  TAnsiStrMap    InfMap;
  TCheckAddrEvent FCheckAddr;

  TFDQuery * __fastcall CreateTempQuery();
  void       __fastcall DeleteTempQuery(TFDQuery * AQuery);
  bool       __fastcall quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  TJSONObject * __fastcall NewObjPair2(TFDQuery *AQ, UnicodeString AFlName );
  void          __fastcall SetClData(TJSONObject * ACls, UnicodeString  AId, UnicodeString APref, UnicodeString  ACode, UnicodeString ALPU, UnicodeString  AOtdel);

  TJSONObject * __fastcall FGetPatRegDataById(UnicodeString ARecId, UnicodeString APref, TDate & ABirthday, TDate & AOtvToDate, TJSONObject * ACls);

  bool __fastcall CheckCard(UnicodeString AUCode, UnicodeString APref, TDate AOtvDate);
  bool __fastcall Convert1003(TJSONObject * ACardRoot, UnicodeString  AUCode, UnicodeString APref, TDate ABirthday, TJSONObject * ACls = NULL);
  bool __fastcall Convert102f(TJSONObject * ACardRoot, UnicodeString  AUCode, UnicodeString APref, TDate ABirthday, TJSONObject * ACls = NULL);
  bool __fastcall Convert1030(TJSONObject * ACardRoot, UnicodeString  AUCode, UnicodeString APref, TDate ABirthday, TDate ADayTo, TJSONObject * ACls = NULL);
  UnicodeString __fastcall PrepareStr(UnicodeString AText);

  UnicodeString __fastcall AsDTF(TFDQuery *AQ, UnicodeString AFN);
  UnicodeString __fastcall AsStr(TFDQuery *AQ, UnicodeString AFN);
  __int64 __fastcall AsInt(TFDQuery *AQ, UnicodeString AFN);

  public:

  __fastcall TIC67Sync(TFDConnection *AConnection);
  __fastcall ~TIC67Sync();

  TJSONArray *  __fastcall GetPatIdList();
//  TJSONObject * __fastcall GetPatDataById(UnicodeString AId);
  UnicodeString __fastcall GetPatDataById(UnicodeString AId);
  void          __fastcall DeletePat(UnicodeString AId);
  bool          __fastcall IsFullSync();
  __property TCheckAddrEvent OnCheckAddr = {read=FCheckAddr,write=FCheckAddr};

  TJSONObject*  __fastcall Convert(TJSONObject* ASrc);
};
//---------------------------------------------------------------------------
#endif
