//---------------------------------------------------------------------------

#ifndef dsMISSyncDMUnitH
#define dsMISSyncDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <IPPeerClient.hpp>
#include <REST.Authenticator.Basic.hpp>
#include <REST.Client.hpp>
//---------------------------------------------------------------------------
#include <System.JSON.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include "XMLContainer.h"
#include "icsLog.h"
#include "kabQueryUtils.h"
#include "SVRClientClassesUnit.h"
//---------------------------------------------------------------------------
enum class TMISSyncType {None, Full, Code};
class TdsMISSyncDM : public TDataModule
{
__published:	// IDE-managed Components
 TRESTClient *MISClient;
 TRESTRequest *MISRequest;
 TRESTResponse *MISResponse;
 THTTPBasicAuthenticator *HTTPAuth;
 TDSRestConnection *ICImportConnect;
 TFDConnection *IC67Connection;
 void __fastcall DataModuleDestroy(TObject *Sender);
private:	// User declarations
  UnicodeString FIdListFunc, FPatDataFunc, FDBId, FLPUId, FAddrCompData;
  TdsEIDataClassClient * FEIClient;
  TdsAdminClassClient*   FAdminClient;
  TAnsiStrMap *FAddrComp;
  TMISSyncType        FSyncType;

  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;
  TOnLogSQLEvent      FOnLogSQL;

  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;

  UnicodeString __fastcall FGetBaseURI();
  void __fastcall FSetBaseURI(UnicodeString AValue);

  UnicodeString __fastcall FGetUser();
  void __fastcall FSetUser(UnicodeString AValue);

  UnicodeString __fastcall FGetPassword();
  void __fastcall FSetPassword(UnicodeString AValue);

  void __fastcall LogMessage(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall LogError(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall LogSQL(UnicodeString ASQL, UnicodeString AFuncName = "", TTime AExecTime = 0, int ALvl = 1);
  void __fastcall LogBegin(UnicodeString AFuncName);
  void __fastcall LogEnd(UnicodeString AFuncName);
  UnicodeString __fastcall CheckAddr67(UnicodeString AAddr, UnicodeString AHouse, UnicodeString ABuilding, UnicodeString AFlat);
  UnicodeString __fastcall HouseFlatCodeStr(UnicodeString AHouse, UnicodeString ABuilding, UnicodeString AFlat);


public:		// User declarations

  __fastcall TdsMISSyncDM(TComponent* Owner, short APort);

  TJSONArray *  __fastcall GetPatIdList(bool AIsIC67);
  TJSONObject * __fastcall GetPatData(UnicodeString  AId, bool AIsIC67);
  void          __fastcall DeletePat(UnicodeString AId);
  bool          __fastcall IsFullSync();
  bool __fastcall ImportPatData(TJSONObject *AData, UnicodeString & AResMessage);
  void __fastcall PrepareAddr67Comp();
  void __fastcall ClearAddr67Comp();

  void __fastcall CreateConnectionClients();
  void __fastcall DeleteConnectionClients();

  __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
  __property TOnLogMsgErrEvent OnLogError = {read = FOnLogError, write = FOnLogError};

  __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
  __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};
  __property TOnLogSQLEvent    OnLogSQL   = {read = FOnLogSQL, write = FOnLogSQL};

  __property UnicodeString BaseURI = {read=FGetBaseURI, write=FSetBaseURI};
  __property UnicodeString IdListFunc = {read=FIdListFunc, write=FIdListFunc};
  __property UnicodeString PatDataFunc = {read=FPatDataFunc, write=FPatDataFunc};

  __property UnicodeString DBId = {read=FDBId, write=FDBId};
  __property UnicodeString LPUId = {read=FLPUId, write=FLPUId};
  __property UnicodeString User = {read=FGetUser, write=FSetUser};
  __property UnicodeString Password = {read=FGetPassword, write=FSetPassword};
  __property UnicodeString AddrCompData = {read=FAddrCompData, write=FAddrCompData};
  __property TMISSyncType  SyncType = {read=FSyncType, write=FSyncType};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsMISSyncDM *dsMISSyncDM;
//---------------------------------------------------------------------------
#endif
