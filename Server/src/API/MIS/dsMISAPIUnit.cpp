﻿//---------------------------------------------------------------------------
//#include <vcl.h>
#pragma hdrstop
#include "dsMISAPIUnit.h"
#include "dsMISSyncModule.h"
#include "JSONUtils.h"
#include <Datasnap.DSHTTP.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "System.Classes.TPersistent"
#pragma resource "*.dfm"
//TRegClassDM *RegClassDM;
//---------------------------------------------------------------------------
__fastcall TMISAPI::TMISAPI(TComponent * Owner, TdsMKB * AMKB, TAxeXMLContainer *AXMLList, TAppOptions *AOpt) : TDataModule(Owner)
 {
  dsLogC(__FUNC__);
  try
   {
    FOpt = AOpt;
    FMKB = AMKB;
    FDM  = new TdsMISAPIDM(this, AXMLList);
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    //FLPUID = "";
    if (FSes)
     {
      ConnectDB(FDM->ICConnection, AOpt, FSes->UserRoles->Values["ICDB"], __FUNC__);
      FDM->DefXML     = FDM->XMLList->GetXML("40381E23-92155860-4448");
      FDefXML         = FDM->DefXML;
      FClassDataQuery = FDM->CreateTempQuery();
      FClassData      = new TdsRegClassData(FDM->XMLList->GetXML("40381E23-92155860-4448"), FClassDataQuery);
     }
   }
  catch (Sysutils::Exception & E)
   {
    dsLogError(E.Message, __FUNC__);
   }
  dsLogC(__FUNC__, true);
 }
//---------------------------------------------------------------------------
__fastcall TMISAPI::~TMISAPI()
 {
  dsLogD(__FUNC__);
  DisconnectDB(FDM->ICConnection, __FUNC__);
  Sleep(50);
  delete FClassDataQuery;
  delete FClassData;
  delete FDM;
  dsLogD(__FUNC__, true);
 }
//---------------------------------------------------------------------------
int __fastcall TMISAPI::ReacCountByFormat(TFDQuery * AQ, UnicodeString AId)
 {
  int RC = 0;
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    UnicodeString FLName = "R" + AId;
    TTagNode * itemDef = FDM->DefXML->GetTagByUID(AId);
    if (itemDef)
     {
      if (itemDef->AV["fl"].Length())
       FLName = itemDef->AV["fl"];
      if (!AQ->FieldByName(FLName)->IsNull)
       {
        FDM->quExecParam(FQ, false, "Select R115A as RCount From class_0038 where code=:pCode", "pCode", AQ->FieldByName(FLName)->AsInteger);
        if (FQ->RecordCount)
         RC = FQ->FieldByName("RCount")->AsInteger;
       }
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    FDM->DeleteTempQuery(FQ);
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONValue * __fastcall TMISAPI::AsDate(TFDQuery * AQ, UnicodeString AId)
 {
  TJSONValue * RC = NULL;
  try
   {
    UnicodeString FLName = "R" + AId;
    TTagNode * itemDef = FDM->DefXML->GetTagByUID(AId);
    if (itemDef)
     {
      if (itemDef->AV["fl"].Length())
       FLName = itemDef->AV["fl"];
      if (!AQ->FieldByName(FLName)->IsNull)
       {
        RC = new TJSONString(AQ->FieldByName(FLName)->AsDateTime.FormatString("dd.mm.yyyy"));
       }
     }
   }
  __finally
   {
    if (!RC)
     RC = new TJSONNull;
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONValue * __fastcall TMISAPI::AsBinary(TFDQuery * AQ, UnicodeString AId)
 {
  TJSONValue * RC = NULL;
  try
   {
    UnicodeString FLName = "R" + AId;
    TTagNode * itemDef = FDM->DefXML->GetTagByUID(AId);
    if (itemDef)
     {
      if (itemDef->AV["fl"].Length())
       FLName = itemDef->AV["fl"];
      if (!AQ->FieldByName(FLName)->IsNull)
       {
        if (AQ->FieldByName(FLName)->AsInteger)
         RC = new TJSONTrue;
        else
         RC = new TJSONFalse;
       }
     }
   }
  __finally
   {
    if (!RC)
     RC = new TJSONNull;
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONValue * __fastcall TMISAPI::AsJSONValue(TFDQuery * AQ, UnicodeString AId)
 {
  TJSONValue * RC = NULL;
  try
   {
    UnicodeString FLName = "R" + AId;
    TTagNode * itemDef = FDM->DefXML->GetTagByUID(AId);
    if (itemDef)
     {
      if (itemDef->AV["fl"].Length())
       FLName = itemDef->AV["fl"];
      if (!AQ->FieldByName(FLName)->IsNull)
       RC = new TJSONString(AQ->FieldByName(FLName)->AsString);
     }
   }
  __finally
   {
    if (!RC)
     RC = new TJSONNull;
   }
  return RC;
 }
//----------------------------------------------------------------------------
UnicodeString __fastcall TMISAPI::AsIntStr(TFDQuery * AQ, UnicodeString AFlName)
 {
  UnicodeString RC = "0";
  try
   {
    if (!AQ->FieldByName(AFlName)->IsNull)
     RC = AQ->FieldByName(AFlName)->AsString;
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONValue * __fastcall TMISAPI::TestReacValue(TFDQuery * AQ)
 {

  TJSONValue * RC = NULL;
  try
   {
    //Формат< choiceDB name = 'Формат реакции' uid = '10A7'
    int TestReacCount = ReacCountByFormat(AQ, "10A7");
    UnicodeString FReac = "";
    //< digit name = 'Значение' uid = '103F' '0184' '0186' '0188' '018A'
    if (TestReacCount)
     {
      switch (TestReacCount)
       {
       case 5:
        FReac = ";" + AsIntStr(AQ, "R018A");
       case 4:
        FReac = ";" + AsIntStr(AQ, "R0188") + FReac;
       case 3:
        FReac = ";" + AsIntStr(AQ, "R0186") + FReac;
       case 2:
        FReac = ";" + AsIntStr(AQ, "R0184") + FReac;
       case 1:
        FReac = AsIntStr(AQ, "R103F") + FReac;
       }
      RC = new TJSONString(FReac);
     }
    else
     RC = new TJSONNull;
   }
  __finally
   {
    if (!RC)
     RC = new TJSONNull;
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONArray * __fastcall TMISAPI::VacInfList(TFDQuery * AQ, UnicodeString AId)
 {
  TJSONArray * RC = new TJSONArray;
  TFDQuery * FQ = FDM->CreateTempQuery();
  TJSONObject * InfObj;
  try
   {
    FDM->quExecParam(FQ, false, "Select R003E as InfCode From class_002D where R003B=:pCode", "pCode", AQ->FieldByName("R" + AId)->AsInteger);

    while (!FQ->Eof)
     {
      InfObj = new TJSONObject;
      InfObj->AddPair("id", FQ->FieldByName("InfCode")->AsString);
      InfObj->AddPair("name", FClassData->GetData("003A", FQ->FieldByName("InfCode")->AsString));
      RC->AddElement(InfObj);
      FQ->Next();
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    FDM->DeleteTempQuery(FQ);
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONValue * __fastcall TMISAPI::AsClassObj(TFDQuery * AQ, UnicodeString AId)
 {
  TJSONValue * RC = NULL;
  try
   {
    UnicodeString FLName = "R" + AId;
    TTagNode * itemDef = FDM->DefXML->GetTagByUID(AId);
    if (itemDef)
     {
      if (itemDef->AV["fl"].Length())
       FLName = itemDef->AV["fl"];
      if (!AQ->FieldByName(FLName)->IsNull)
       {
        //{"id":"<код>","name":"<знач.>"}
        RC = new TJSONObject;
        ((TJSONObject *)RC)->AddPair("id", AQ->FieldByName(FLName)->AsString);
        ((TJSONObject *)RC)->AddPair("name", FClassData->GetData(itemDef->AV["ref"], AQ->FieldByName(FLName)->AsString));
       }
     }
   }
  __finally
   {
    if (!RC)
     RC = new TJSONNull;
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONValue * __fastcall TMISAPI::AsChoiceObj(TFDQuery * AQ, UnicodeString AId)
 {
  TJSONValue * RC = NULL;
  try
   {
    UnicodeString FLName = "R" + AId;
    TTagNode * itemDef = FDM->DefXML->GetTagByUID(AId);
    if (itemDef)
     {
      if (itemDef->AV["fl"].Length())
       FLName = itemDef->AV["fl"];
      if (!AQ->FieldByName(FLName)->IsNull)
       {
        TTagNode * chVal = itemDef->GetChildByAV("choicevalue", "value", AQ->FieldByName(FLName)->AsString);
        if (chVal)
         {
          //{"id":"<код>","name":"<знач.>"}
          RC = new TJSONObject;
          ((TJSONObject *)RC)->AddPair("id", chVal->AV["value"]);
          ((TJSONObject *)RC)->AddPair("name", chVal->AV["name"]);
         }
       }
     }
   }
  __finally
   {
    if (!RC)
     RC = new TJSONNull;
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONValue * __fastcall TMISAPI::AsMKBCode(TFDQuery * AQ, UnicodeString AId)
 {
  TJSONValue * RC = NULL;
  try
   {
    //arItem->AddPair("disease", AsMKBCode(FQ, "3082"));      //Заболевание	<disease>	"disease":{"id":"<код>","name":"<знач.>"}          < extedit name = 'Заболевание' uid = '3082' linecount = '3' showtype = '1' length = '255' >
    UnicodeString FLName = "R" + AId;
    if (!AQ->FieldByName(FLName)->IsNull)
     {
      RC = new TJSONObject;
      ((TJSONObject *)RC)->AddPair("id", AQ->FieldByName(FLName)->AsString);
      ((TJSONObject *)RC)->AddPair("name", FMKB->CodeToText(AQ->FieldByName(FLName)->AsString));
     }
   }
  __finally
   {
    if (!RC)
     RC = new TJSONNull;
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONArray * __fastcall TMISAPI::GetPrivData(__int64 APatId)
 {
  TJSONArray * RC = new TJSONArray;
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    TJSONObject * arItem;
    TJSONObject * ReacObj;
    TJSONObject * ValObj;
    FDM->quExecParam(FQ, false, "Select * From class_3003 where R317A=:pCode", "pCode", APatId);
    while (!FQ->Eof)
     {
      arItem = new TJSONObject;
      RC->AddElement(arItem);
      //Прививки	<priv_list>	[<priv>1,<priv>2 … <priv>n]
      //Прививка	<priv>	{<date>,<vac>,<vactype>,<inflist>,<dose>,<ser>,<reac>,<inthislpu>,<doc>,<ms>}
      arItem->AddPair("date", AsDate(FQ, "3024"));         //Дата выполнения	<date>	"date":"<дата>*1" <date name='Дата выполнения' uid='3024'   default='CurrentDate'
      arItem->AddPair("vac", AsClassObj(FQ, "3020"));      //МИБП	<vac>	"vac":{"id":"<код>","name":"<знач.>"}            <choiceDB name='МИБП' uid='3020'
      arItem->AddPair("vactype", AsJSONValue(FQ, "3021")); //Вид	<vactype>	"vactype":"<вид>"            //<text name='Вид' uid='3021' <text name='Вид по инфекциям' uid='3185'

      arItem->AddPair("inflist", VacInfList(FQ, "3020")); //Список инфекций	<inflist>	"inflist":[{"id":"<код1>","name":"<знач.1>"},{"id":"<код2>","name":"<знач.2>"}, …]
      arItem->AddPair("dose", AsJSONValue(FQ, "3022"));   //Доза	<dose>	"dose":"<знач.>"          //<digit name='Доза' uid='3022'
      arItem->AddPair("ser", AsJSONValue(FQ, "3023"));    //Серия	<ser>	"ser":"<знач.>"          //<text name='Серия' uid='3023'

      //Реакция: reac:{<check>,<com>,<loc>,<val>}
      ReacObj = new TJSONObject;
      arItem->AddPair("reac", ReacObj); //Реакция проверена	<rcheck>	"check":"<знач.>"            //<binary name='Реакция проверена' uid='3025'

      ReacObj->AddPair("check", AsBinary(FQ, "3025")); //Реакция проверена	<rcheck>	"check":"<знач.>"            //<binary name='Реакция проверена' uid='3025'
      ReacObj->AddPair("com", AsBinary(FQ, "3026"));   //Есть общая реакция	<rcom>	"com":"<знач.>"            //<binary name='Есть общая реакция' uid='3026'  >
      ReacObj->AddPair("loc", AsBinary(FQ, "3028"));   //Есть местная реакция	<rloc>	"loc":"<знач.>"            //<binary name='Есть местная реакция' uid='3028'  >

      //Значение реакции: val:{<type>,<val>}
      ValObj = new TJSONObject;
      ReacObj->AddPair("val", ValObj);

      ValObj->AddPair("type", AsClassObj(FQ, "30A8")); //Формат	<type>	"type":{"id":"<код>","name":"<знач.>"}            <choiceDB name='Формат реакции' uid='30A8'
      if (ReacCountByFormat(FQ, "30A8"))
       ValObj->AddPair("val", AsJSONValue(FQ, "3027")); //Значение	<val>	"val":"<знач.>"            <digit name='Значение реакции' uid='3027'
      else
       ValObj->AddPair("val", new TJSONNull); //Значение	<val>	"val":"<знач.>"            <digit name='Значение реакции' uid='3027'

      arItem->AddPair("inthislpu", AsBinary(FQ, "301F")); //Выполнена в данном ЛПУ	<inthislpu>	"inthislpu":"<знач.>"          <binary name='Выполнена в данном ЛПУ' uid='301F'
      arItem->AddPair("doc", AsClassObj(FQ, "30B0"));     //Врач	<doc>	"doc":{"id":"<код>","name":"<знач.>"}          <choiceDB name='Врач' uid='30B0'
      arItem->AddPair("ms", AsClassObj(FQ, "3181"));      //Мед. сестра	<ms>	"ms":{"id":"<код>","name":"<знач.>"}          <choiceDB name='Мед. сестра' uid='3181'
      FQ->Next();
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    FDM->DeleteTempQuery(FQ);
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONArray * __fastcall TMISAPI::GetTestData(__int64 APatId)
 {
  TJSONArray * RC = new TJSONArray;
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    TJSONObject * arItem;
    TJSONObject * ReacObj;
    TJSONObject * ValObj;
    FDM->quExecParam(FQ, false, "Select * From class_102f where R017B=:pCode", "pCode", APatId);
    while (!FQ->Eof)
     {
      arItem = new TJSONObject;
      RC->AddElement(arItem);

      //Пробы	<test_list>	[<test>1,<test>2 … <test>n]
      //Проба:	<test>	{<date>,<test>,<ser>,<reac>,<inthislpu>,<doc>,<ms>}
      arItem->AddPair("date", AsDate(FQ, "1031"));     //Дата выполнения	<date>	"date":"<дата>*1"      < date name = 'Дата выполнения' uid = '1031'
      arItem->AddPair("test", AsClassObj(FQ, "1032")); //Проба	<test>	"test":{"id":"<код>","name":"<знач.>"}          < choiceDB name = 'Проба' uid = '1032'
      arItem->AddPair("inf", AsClassObj(FQ, "1200"));  //< choiceDB name = 'Инфекция' uid = '1200'
      arItem->AddPair("ser", AsJSONValue(FQ, "1033")); //Серия препарата	<ser>	"ser":"<знач.>"          < text name = 'Серия препарата' uid = '1033'

      //Реакция:	<reac>	reac:{<rcheck>,<rval>}
      ReacObj = new TJSONObject;
      arItem->AddPair("reac", ReacObj);

      ReacObj->AddPair("check", AsBinary(FQ, "103B")); //Реакция проверена	<rcheck>	"check":"<знач.>"          < binary name = 'Реакция проверена' uid = '103B'

      //Значение реакции:	<rval>	val:{<type>,<val>}
      ValObj = new TJSONObject;
      ReacObj->AddPair("val", ValObj);

      ValObj->AddPair("type", AsClassObj(FQ, "10A7")); //Формат	<type>	"type":{"id":"<код>","name":"<знач.>"}          < choiceDB name = 'Формат реакции' uid = '10A7'
      ValObj->AddPair("val", TestReacValue(FQ));       //Значение	<val>	"val":"<знач.>" < digit name = 'Значение' uid = '103F' '0184' '0186' '0188' '018A'

      arItem->AddPair("inthislpu", AsBinary(FQ, "10A5")); //Выполнена в данном ЛПУ	<inthislpu>	"inthislpu":{"id":"<код>","name":"<знач.>"} < binary name = 'Выполнена в данном ЛПУ' uid = '10A5'
      arItem->AddPair("doc", AsClassObj(FQ, "10B2"));     //Врач <doc> "doc":{"id":"<код>","name":"<знач.>"}          < choiceDB name = 'Врач' uid = '10B2'
      arItem->AddPair("ms", AsClassObj(FQ, "018D"));      //Мед. сестра <ms> "ms":{"id":"<код>","name":"<знач.>"} < choiceDB name = 'Мед. сестра' uid = '018D'
      FQ->Next();
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    FDM->DeleteTempQuery(FQ);
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONArray * __fastcall TMISAPI::GetCancelData(__int64 APatId)
 {
  TJSONArray * RC = new TJSONArray;
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    TJSONObject * arItem;
    TJSONObject * DisObj;
    FDM->quExecParam(FQ, false, "Select * From class_3030 where R317C=:pCode", "pCode", APatId);
    while (!FQ->Eof)
     {
      arItem = new TJSONObject;
      RC->AddElement(arItem);

      //Отводы	<cancel_list>	[<cancel>1,<cancel>2 … <cancel>n]
      //Отвод:	<cancel>	{<bdate>,<edate>,<type>,<inflist>,<reason>,<disease>,<nvac>,<ntest>}
      arItem->AddPair("bdate", AsDate(FQ, "305F"));        //Начало действия	<bdate>	"bdate":"<дата>*1"          < date name = 'Начало действия' uid = '305F'
      arItem->AddPair("edate", AsDate(FQ, "3065"));        //Окончание действия	<edate>	"edate":"<дата>*1"          < date name = 'Окончание действия' uid = '3065'
      arItem->AddPair("type", AsChoiceObj(FQ, "3043"));    //Тип отвода	<type>	"type":{"id":"<код>","name":"<знач.>"}          < choice name = 'Отвод' uid = '3043' >
      arItem->AddPair("inflist", AsJSONValue(FQ, "3083")); //Список отводимых инфекций	<inflist>	"inflist":[{"id":"<код1>","name":"<знач.1>"}, …] < text name = 'Список инфекций' uid = '3083'
      arItem->AddPair("reason", AsChoiceObj(FQ, "3066"));  //Причина	<reason>	"reason":{"id":"<код>","name":"<знач.>"}          < choice name = 'Причина' uid = '3066' >
      arItem->AddPair("disease", AsMKBCode(FQ, "3082"));   //Заболевание	<disease>	"disease":{"id":"<код>","name":"<знач.>"}          < extedit name = 'Заболевание' uid = '3082'
      arItem->AddPair("nvac", AsBinary(FQ, "3110"));       //Отвод прививки	<nvac>	"nvac":"<знач.>"          < binary name = 'Отвод от прививки' uid = '3110'
      arItem->AddPair("ntest", AsBinary(FQ, "3111"));      //Отвод пробы	<ntest>	"ntest":"<знач.>"           < binary name = 'Отвод от пробы' uid = '3111'
      FQ->Next();
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    FDM->DeleteTempQuery(FQ);
   }
  return RC;
 }
//----------------------------------------------------------------------------

// ****************************************************************************
// *                                                                          *
// *                API                                                       *
// *                                                                          *
// ****************************************************************************

//----------------------------------------------------------------------------
TJSONObject * TMISAPI::PatientF63(UnicodeString APtId)
 {
  //Формат данных формы №63: {"ptid":"<знач.>","priv":<priv_list>,"test":<test_list>,"cancel":<cancel_list>}, <знач.> для "ptid"
  //в формате ИДп («Ид. ЛПУ (int32)»+ «_»+«Ид. пациента (int32)»).

  TJSONObject * RC = new TJSONObject;
  TFDQuery * FQ = FDM->CreateTempQuery();
  try
   {
    __int64 FPtCode;
    try
     {
      FDM->quExecParam(FQ, false, "Select code From class_1000 where Upper(CGUID)=:pCode", "pCode", APtId.UpperCase());
      if (FQ->RecordCount)
       {
        FPtCode = FQ->FieldByName("code")->AsInteger;
        RC->AddPair("ptid", APtId);
        RC->AddPair("priv", GetPrivData(FPtCode));
        RC->AddPair("test", GetTestData(FPtCode));
        RC->AddPair("cancel", GetCancelData(FPtCode));
       }
      else
       RC->AddPair("error", "Пациент не найден в БД УИ-ЛПУ.");
     }
    catch (Sysutils::Exception & E)
     {
      delete RC;
      RC = new TJSONObject;
      RC->AddPair("error", "Ошибка формирования ф63. " + E.Message);
     }
   }
  __finally
   {
    if (FQ->Transaction->Active)
     FQ->Transaction->Commit();
    FDM->DeleteTempQuery(FQ);
   }
  return RC;
 }
//----------------------------------------------------------------------------
void __fastcall TMISAPI::LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsLogMessage(AMessage, AFuncName, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TMISAPI::LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  dsLogError(AMessage, AFuncName, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TMISAPI::LogBegin(UnicodeString AFuncName)
 {
  dsLogBegin(AFuncName);
 }
//---------------------------------------------------------------------------
void __fastcall TMISAPI::LogEnd(UnicodeString AFuncName)
 {
  dsLogEnd(AFuncName);
 }
//---------------------------------------------------------------------------
UnicodeString TMISAPI::ImportPatient(UnicodeString AMIS, TJSONObject * ASrc)
 {
  UnicodeString RC = "error";
  TMISSync * FSyncModule = new TMISSync(FOpt);
  try
   {
    TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
    if (FSes)
     {
      FSyncModule->OnLogMessage = LogMessage;
      FSyncModule->OnLogError   = LogError;
      FSyncModule->OnLogBegin   = LogBegin;
      FSyncModule->OnLogEnd     = LogEnd;
      FSyncModule->User         = FSes->UserName;
      FSyncModule->Password     = FSes->UserRoles->Values["UPS"];
      UnicodeString FResMessage = "";
      FSyncModule->ImportPatient(ASrc, AMIS, FResMessage);
      RC = "ok";
     }
   }
  __finally
   {
    FSyncModule->OnLogMessage = NULL;
    FSyncModule->OnLogError   = NULL;
    FSyncModule->OnLogBegin   = NULL;
    FSyncModule->OnLogEnd     = NULL;
    delete FSyncModule;
   }
  return RC;
 }
//----------------------------------------------------------------------------
