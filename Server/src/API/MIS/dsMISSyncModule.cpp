/*
 ����        - Planer.cpp
 ������      - ���������� ������������.
 ��������� "���������� ������������"
 ��������    - �����������.
 ���� ����������
 ����������� - ������� �.�.
 ����        - 09.05.2004
 */
// ---------------------------------------------------------------------------
#include <string.h>
#pragma hdrstop
#include <algorithm>
#include <vcl.h>
#include <System.SysUtils.hpp>
#include "dsMISSyncModule.h"
#include "JSONUtils.h"
#include "srvsetting.h"
#include "dsAuraSync.h"
#include "dsMISCommonSync.h"
// #include "dsSrvRegTemplate.h"
// #include "OrgParsers.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ###########################################################################
// ##                                                                       ##
// ##                     TBackgroundMISSync                                ##
// ##                                                                       ##
// ###########################################################################
class TMISSync::TBackgroundMISSync : public TThread
 {
 private:
  TMISSync * FSyncModule;
 protected:
  virtual void __fastcall Execute();
 public:
  __fastcall TBackgroundMISSync(bool CreateSuspended, TMISSync * ASyncModule);
  __fastcall ~TBackgroundMISSync();
  void __fastcall ThreadSafeCall(TdsSyncMethod Method);
 };
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                     TMISSync::TBackgroundMISSync                      ##
// ##                                                                       ##
// ###########################################################################
__fastcall TMISSync::TBackgroundMISSync::TBackgroundMISSync(bool CreateSuspended, TMISSync * ASyncModule)
  : TThread(CreateSuspended), FSyncModule(ASyncModule)
 {
 }
// ---------------------------------------------------------------------------
__fastcall TMISSync::TBackgroundMISSync::~TBackgroundMISSync()
 {
  FSyncModule = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::TBackgroundMISSync::Execute()
 {
  CoInitialize(NULL);
  FSyncModule->Sync();
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::TBackgroundMISSync::ThreadSafeCall(TdsSyncMethod Method)
 {
  Synchronize(Method);
 }
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                              TMISSync                                 ##
// ##                                                                       ##
// ###########################################################################
__fastcall TMISSync::TMISSync(TAppOptions * AOpt)
 {
  FDM               = new TdsMISSyncDM(NULL, AOpt->Vals[_PORTNAME_].AsIntDef(_SERVER_PORT_));
  FOpt              = AOpt;
  FDM->OnLogMessage = CallLogMsg;
  FDM->OnLogError   = CallLogErr;
  FDM->OnLogSQL     = CallLogSQL;
  FDM->OnLogBegin   = CallLogBegin;
  FDM->OnLogEnd     = CallLogEnd;
  FBkgSyncModule    = NULL;
  FStage            = "";
  FMaxProgress      = 10;
  FOnIncProgress    = NULL;
  IsIC67            = false;
 }
// ---------------------------------------------------------------------------
__fastcall TMISSync::~TMISSync()
 {
  StopSync();
  if (FBkgSyncModule)
   delete FBkgSyncModule;
  FBkgSyncModule    = NULL;
  FDM->OnLogMessage = NULL;
  FDM->OnLogError   = NULL;
  FDM->OnLogBegin   = NULL;
  FDM->OnLogEnd     = NULL;
  delete FDM;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::CallOnIncProgress()
 {
  SyncCall(& SyncCallOnIncProgress);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::SyncCallOnIncProgress()
 {
  if (FOnIncProgress)
   FOnIncProgress(this);
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TMISSync::StartSync()
 {
  CallLogBegin(__FUNC__);
  UnicodeString RC = "error";
  try
   {
    if (InSync)
     CallLogErr("����������� �������������.", __FUNC__);
    else
     {
      if (!BaseURI.Length())
       {
        if (IsIC67)
         CallLogErr("�� ������ ���� � �� �� 6.7", __FUNC__);
        else
         CallLogErr("�� ������ ����� ������� ���.", __FUNC__);
       }
      else
       {
        if (!IdListFunc.Length())
         CallLogErr("�� ������� ��� ������� ��������� ������ ���������.", __FUNC__);
        else
         {
          if (!PatDataFunc.Length())
           CallLogErr("�� ������� ��� ������� ��������� ������ ��������.", __FUNC__);
          else
           {
            if (FBkgSyncModule)
             {
              FBkgSyncModule->Terminate();
              Sleep(50);
              delete FBkgSyncModule;
              FBkgSyncModule = NULL;
             }
            FBkgSyncModule = new TBackgroundMISSync(false, this);
            RC = "ok";
           }
         }
       }
     }
   }
  __finally
   {
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TMISSync::StopSync()
 {
  UnicodeString RC = "error";
  CallLogBegin(__FUNC__);
  try
   {
    FTerminated = true;
    if (FBkgSyncModule && InSync)
     {
      FInSync = false;
      delete FBkgSyncModule;
      FBkgSyncModule = NULL;
     }
    RC = "ok";
   }
  __finally
   {
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
void __fastcall TMISSync::SyncCall(TdsSyncMethod AMethod)
 {
  if (FBkgSyncModule)
   FBkgSyncModule->ThreadSafeCall(AMethod);
  else
   AMethod();
 }
// ---------------------------------------------------------------------------
int __fastcall TMISSync::GetMaxProgress()
 {
  return FMaxProgress;
 }
// ---------------------------------------------------------------------------
int __fastcall TMISSync::GetCurProgress()
 {
  return FCurrProgress;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TMISSync::GetCurStage() const
 {
  return FStage;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::CallLogMsg(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogMsg);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::CallLogErr(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogErr);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::CallLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  FLogMsg      = ASQL;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  FLogExecTime = AExecTime;
  SyncCall(& SyncCallLogSQL);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::CallLogBegin(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogBegin);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::CallLogEnd(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogEnd);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::SyncCallLogMsg()
 {
  if (FOnLogMessage)
   FOnLogMessage(FLogMsg, FLogFuncName, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::SyncCallLogErr()
 {
  if (FOnLogError)
   FOnLogError(FLogMsg, FLogFuncName, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::SyncCallLogSQL()
 {
  if (FOnLogSQL)
   FOnLogSQL(FLogMsg, FLogFuncName, FLogExecTime, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::SyncCallLogBegin()
 {
  if (FOnLogBegin)
   FOnLogBegin(FLogFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::SyncCallLogEnd()
 {
  if (FOnLogEnd)
   FOnLogEnd(FLogFuncName);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TMISSync::FGetBaseURI()
 {
  return FDM->BaseURI;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::FSetBaseURI(UnicodeString AValue)
 {
  FDM->BaseURI = AValue;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TMISSync::FGetIdListFunc()
 {
  return FDM->IdListFunc;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::FSetIdListFunc(UnicodeString AValue)
 {
  FDM->IdListFunc = AValue;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TMISSync::FGetPatDataFunc()
 {
  return FDM->PatDataFunc;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::FSetPatDataFunc(UnicodeString AValue)
 {
  FDM->PatDataFunc = AValue;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TMISSync::FGetAddrCompData()
 {
  return FDM->AddrCompData;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::FSetAddrCompData(UnicodeString AValue)
 {
  FDM->AddrCompData = AValue;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TMISSync::FGetUser()
 {
  return FDM->User;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::FSetUser(UnicodeString AValue)
 {
  FDM->User = AValue;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TMISSync::FGetPassword()
 {
  return FDM->Password;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::FSetPassword(UnicodeString AValue)
 {
  FDM->Password = AValue;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TMISSync::FGetDBId()
 {
  return FDM->DBId;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::FSetDBId(UnicodeString AValue)
 {
  FDM->DBId = AValue;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TMISSync::FGetLPUId()
 {
  return FDM->LPUId;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::FSetLPUId(UnicodeString AValue)
 {
  FDM->LPUId = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::FSetMIS(UnicodeString AValue)
 {
  FMIS   = AValue;
  IsIC67 = (FMIS.UpperCase() == "IC67");
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TMISSync::Sync()
 {
  CallLogBegin(__FUNC__);
  UnicodeString RC = "������";
  FTerminated   = false;
  FInSync       = true;
  FCurrProgress = 0;
  FMaxProgress  = 0;
  FStage        = "�������������";
  FDM->CreateConnectionClients(); // 1 �������
  bool FCont = false;
  try
   {
    try
     {
      // ******* Body begin ***************************
      TStringList * IdList = new TStringList;
      try
       {
        UnicodeString FSyncDataPath = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\MISSyncData");
        UnicodeString FIdFN = icsPrePath(FSyncDataPath + "\\MISData" + DBId + ".dat");
        if (!FileExists(FIdFN) && !IsIC67)
         {
          ForceDirectories(FSyncDataPath);
          IdList->SaveToFile(FIdFN);
         }
        if (!IsIC67)
         {
          FCont = true;
          IdList->LoadFromFile(FIdFN);
         }
        else
         {
          FDM->PrepareAddr67Comp(); // , TAppOptions *AOpt
          try
           {
            FCont = ConnectDB(FDM->IC67Connection, FOpt, IdListFunc, __FUNC__, CallLogErr, CallLogBegin, CallLogEnd);
           }
          catch (System::Sysutils::Exception & E)
           {
            CallLogErr("������ ����������� � ��::" + DBId + ":: " + E.Message, __FUNC__);
           }
         }
        if (!IdList->Count && FCont)
         {
          TJSONArray * IdArray = FDM->GetPatIdList(IsIC67);
          if (IdArray)
           {
            for (int i = 0; (i < IdArray->Count) && !FTerminated; i++)
             {
              if (!IdArray->Items[i]->Null)
               IdList->Add(((TJSONString *)IdArray->Items[i])->Value());
             }
            if (!IsIC67)
             IdList->SaveToFile(FIdFN);
            delete IdArray;
           }
         }
        if (FCont)
         {
          UnicodeString FPtCode;
          TJSONObject * FPtData = NULL;
          int itSavingStep = 100;
          CallLogMsg(FMIS + "::" + DBId + ":: ������������� ������ ���������, �����: " + IntToStr(IdList->Count),
            __FUNC__, 1);
          UnicodeString FImportMessage;
          int impCount = 0;
          for (int i = 0; (i < IdList->Count) && !FTerminated; i++)
           {
            FPtCode = IdList->Strings[i].Trim();
            if (FPtCode.Length() && (FPtCode.LowerCase() != "null"))
             {
              CallLogMsg(FMIS + "::" + DBId + " >> id: " + FPtCode, __FUNC__, 1);
              try
               {
                FPtData = FDM->GetPatData(FPtCode, IsIC67);
                if (FPtData)
                 {
                  try
                   {
                    CallLogMsg(FPtData->ToString(), __FUNC__, 1);
                    try
                     {
                      FImportMessage = "";
                      if (ImportPatient(FPtData, FMIS, FImportMessage).LowerCase() == "ok")
                       {
                        impCount++ ;
                        CallLogMsg(FMIS + "::" + DBId + ":: ������������, id[" + IntToStr(impCount) + "]: " + FPtCode +
                          " " + FImportMessage, __FUNC__, 1);
                        IdList->Delete(i);
                        if (IsIC67)
                         FDM->DeletePat(FPtCode);
                        i-- ;
                       }
                     }
                    catch (System::Sysutils::Exception & E)
                     {
                      CallLogErr(FMIS + "::" + DBId + ":: In ImportPatient: " + E.Message, __FUNC__);
                     }
                   }
                  __finally
                   {
                    FPtData = NULL;
                   }
                 }
                else
                 CallLogErr(FMIS + "::" + DBId + ":: Null data in GetPatientData: " + FPtCode, __FUNC__);
               }
              catch (System::Sysutils::Exception & E)
               {
                CallLogErr(FMIS + "::" + DBId + ":: In GetPatData: " + E.Message, __FUNC__);
               }
             }
            if (--itSavingStep < 0)
             {
              itSavingStep = 100;
              if (!IsIC67)
               IdList->SaveToFile(FIdFN);
             }
            if (IsIC67 && !(i % 5000))
             {
              DisconnectDB(FDM->IC67Connection, __FUNC__, CallLogBegin, CallLogEnd);
              ConnectDB(FDM->IC67Connection, FOpt, IdListFunc, __FUNC__, CallLogErr, CallLogBegin, CallLogEnd);
             }
           }
          if (!IsIC67)
           IdList->SaveToFile(FIdFN);
          CallLogMsg(FMIS + "::" + DBId + ":: ����� ������������� ������ ���������, �� �����������������: " +
            IntToStr(IdList->Count), __FUNC__, 1);
         }
       }
      __finally
       {
        delete IdList;
        if (IsIC67 && FCont)
         {
          bool DelDB = false;
          DelDB = FDM->IsFullSync();
          DisconnectDB(FDM->IC67Connection, __FUNC__, CallLogBegin, CallLogEnd);
          if (DelDB)
           {
            Sleep(500);
            DeleteFile(IdListFunc);
           }
         }
       }
      // ******* Body end *****************************
      RC = "ok";
     }
    catch (System::Sysutils::Exception & E)
     {
      CallLogErr(E.Message, __FUNC__);
     }
   }
  __finally
   {
    FDM->ClearAddr67Comp();
    FDM->DeleteConnectionClients(); // 1 �������
    FInSync = false;
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
UnicodeString __fastcall TMISSync::ImportPatient(TJSONObject * ASrc, UnicodeString AMIS, UnicodeString & AResMessage)
 {
  UnicodeString RC = "error";
  AResMessage = "";
  try
   {
    TJSONObject * ConvRC = ConvertMISData(ASrc, AMIS);
    if (ConvRC)
     {
      if (FDM->ImportPatData(ConvRC, AResMessage))
       RC = "ok";
     }
    else
     RC = "������ �������������� ������.";
   }
  __finally
   {
   }
  return RC;
 }
// ----------------------------------------------------------------------------
TJSONObject * __fastcall TMISSync::ConvertMISData(TJSONObject * ASrc, UnicodeString AMIS)
 {
  CallLogBegin(__FUNC__);
  TJSONObject * RC = NULL;
  if (IsIC67)
   RC = ASrc;
  else
   {
    TdsMISBaseSync * FConvertModule = NULL;
    try
     {
      try
       {
        if (AMIS.UpperCase() == "MISCOMMON")
         FConvertModule = new TMISCommonSync;
        else if (AMIS.UpperCase() == "AURA")
         FConvertModule = new TAuraSync;
        // else if (FMIS.UpperCase() == "SAMSON")
        // FConvertModule = new T....Sync;
        // else if (FMIS.UpperCase() == "VISTA")
        // FConvertModule = new T....Sync;
        // else if (FMIS.UpperCase() == "SHOTA")
        // FConvertModule = new T....Sync;
        FConvertModule->LPUId        = FDM->LPUId;
        FConvertModule->OnLogMessage = CallLogMsg;
        FConvertModule->OnLogError   = CallLogErr;
        FConvertModule->OnLogBegin   = CallLogBegin;
        FConvertModule->OnLogEnd     = CallLogEnd;
        RC                           = FConvertModule->Convert(ASrc);
       }
      catch (System::Sysutils::Exception & E)
       {
        CallLogErr(FMIS + "::" + DBId + ":: " + E.Message, __FUNC__);
       }
     }
    __finally
     {
      delete FConvertModule;
     }
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
TMISSyncType __fastcall TMISSync::FGetSyncType()
 {
  return FDM->SyncType;
 }
// ---------------------------------------------------------------------------
void __fastcall TMISSync::FSetSyncType(TMISSyncType AValue)
 {
  FDM->SyncType = AValue;
 }
// ---------------------------------------------------------------------------
