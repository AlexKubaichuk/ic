//---------------------------------------------------------------------------

#ifndef dsMISSyncModuleH
#define dsMISSyncModuleH

//#include "DKCfg.h"      // ��������� �������� ����������

#include <System.hpp>
#include <Classes.hpp>
//#include <DateUtils.hpp>

//#include "DKClasses.h"
//#include "XMLContainer.h"
//#include "ICSDATEUTIL.hpp"
//#include "DKUtils.h"

#include "dsMISSyncDMUnit.h"
//#include "PlanerDebug.h"

//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                              TMISSync                                 ##
//##                                                                       ##
//###########################################################################

//---------------------------------------------------------------------------
class PACKAGE TMISSync : public TObject
{
typedef void __fastcall (__closure *TdsSyncMethod)(void);
private:
    // �������� �� ��������

  class TBackgroundMISSync;
  TBackgroundMISSync * FBkgSyncModule;
  bool FTerminated;
  bool FInSync;
  void __fastcall SyncCall(TdsSyncMethod AMethod);


  int             FMaxProgress;        // Max �������� ��� ProgressBar'�
  TNotifyEvent    FOnIncProgress;      // �������, ��� ��������� �������� ���������� �������
                                         // ��������� ������� ProgressBar'�
  UnicodeString FLogMsg, FLogFuncName;
  int FLogALvl;
  TTime         FLogExecTime;
  TAppOptions         *FOpt;
  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;
  TOnLogSQLEvent      FOnLogSQL;

  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;


  inline void __fastcall CallOnIncProgress();
  void __fastcall SyncCallOnIncProgress();

  UnicodeString FStage;

  int FCurrProgress ;

  TdsMISSyncDM* FDM;

  UnicodeString FMIS;
  bool IsIC67;

  UnicodeString __fastcall FGetBaseURI();
  void __fastcall FSetBaseURI(UnicodeString AValue);

  UnicodeString __fastcall FGetAddrCompData();
  void __fastcall FSetAddrCompData(UnicodeString AValue);

  UnicodeString __fastcall FGetIdListFunc();
  void __fastcall FSetIdListFunc(UnicodeString AValue);

  UnicodeString __fastcall FGetPatDataFunc();
  void __fastcall FSetPatDataFunc(UnicodeString AValue);

  UnicodeString __fastcall FGetUser();
  void __fastcall FSetUser(UnicodeString AValue);

  UnicodeString __fastcall FGetPassword();
  void __fastcall FSetPassword(UnicodeString AValue);

  UnicodeString __fastcall FGetDBId();
  void __fastcall FSetDBId(UnicodeString AValue);

  UnicodeString __fastcall FGetLPUId();
  void __fastcall FSetLPUId(UnicodeString AValue);

  void __fastcall FSetMIS(UnicodeString AValue);

  TMISSyncType __fastcall FGetSyncType();
  void __fastcall FSetSyncType(TMISSyncType AValue);


  TJSONObject* __fastcall ConvertMISData(TJSONObject* ASrc, UnicodeString AMIS);
  // ���������� �� ���������� ���
//  TJSONObject* __fastcall ConvertFromAura(TJSONObject* ASrc);
  // ----------------------------

//  TJSONValue * __fastcall AsObj(TJSONObject * ASrc, UnicodeString  AId);
//  void __fastcall SetClData(TJSONObject * ACls, UnicodeString  AId, TJSONObject * ASrc);
//  void __fastcall AddClsData(TJSONObject * ACls, UnicodeString  AId, TJSONObject * ASrc);
//  void __fastcall AddPatOrg(TJSONObject * APtRoot, TJSONObject * AClsRoot, TJSONObject * ASrc);
protected:
  void __fastcall SyncCallLogMsg();
  void __fastcall SyncCallLogErr();
  void __fastcall SyncCallLogSQL();
  void __fastcall SyncCallLogBegin();
  void __fastcall SyncCallLogEnd();

  void __fastcall CallLogMsg(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogErr(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl = 1);
  void __fastcall CallLogBegin(UnicodeString AFuncName);
  void __fastcall CallLogEnd(UnicodeString AFuncName);

public:
    System::UnicodeString __fastcall StartSync();
    System::UnicodeString __fastcall StopSync();
    __property bool Terminated = {read = FTerminated};
    __property bool InSync = {read = FInSync};
    int __fastcall GetMaxProgress();
    int __fastcall GetCurProgress();
    UnicodeString __fastcall GetCurStage() const;

    __fastcall TMISSync(TAppOptions *AOpt);
    virtual __fastcall ~TMISSync();

    __property int          MaxProgress   = { read = FMaxProgress };
    __property TNotifyEvent OnIncProgress = { read = FOnIncProgress, write = FOnIncProgress };

    __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
    __property TOnLogMsgErrEvent OnLogError   = {read = FOnLogError, write = FOnLogError};
    __property TOnLogSQLEvent    OnLogSQL     = {read = FOnLogSQL, write = FOnLogSQL};

    __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
    __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};

  __property UnicodeString BaseURI = {read=FGetBaseURI, write=FSetBaseURI};
  __property UnicodeString IdListFunc = {read=FGetIdListFunc, write=FSetIdListFunc};
  __property UnicodeString PatDataFunc = {read=FGetPatDataFunc, write=FSetPatDataFunc};

  __property UnicodeString MIS = {read=FMIS, write=FSetMIS};
  __property UnicodeString DBId = {read=FGetDBId, write=FSetDBId};
  __property UnicodeString LPUId = {read=FGetLPUId, write=FSetLPUId};
  __property UnicodeString User = {read=FGetUser, write=FSetUser};
  __property UnicodeString Password = {read=FGetPassword, write=FSetPassword};
  __property UnicodeString AddrCompData = {read=FGetAddrCompData, write=FSetAddrCompData};
  __property TMISSyncType  SyncType = {read=FGetSyncType, write=FSetSyncType};


  System::UnicodeString __fastcall Sync();
  UnicodeString __fastcall ImportPatient(TJSONObject * ASrc, UnicodeString AMIS, UnicodeString & AResMessage);
};
//---------------------------------------------------------------------------
#endif
