object dsMISAPIDM: TdsMISAPIDM
  OldCreateOrder = False
  Height = 147
  Width = 117
  object ICConnection: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'CharacterSet=UTF8'
      'Password=masterkey'
      'User_Name=sysdba'
      'Server=localhost'
      'Port=3050'
      'Pooled=False')
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    FormatOptions.AssignedValues = [fvFmtDisplayDateTime, fvFmtDisplayDate, fvFmtDisplayTime]
    FormatOptions.FmtDisplayDateTime = 'dd.mm.yyyy hh:nn:ss'
    FormatOptions.FmtDisplayDate = 'dd.mm.yyyy'
    FormatOptions.FmtDisplayTime = 'hh:nn:ss'
    ResourceOptions.AssignedValues = [rvDirectExecute, rvAutoConnect, rvSilentMode]
    ResourceOptions.DirectExecute = True
    ResourceOptions.SilentMode = True
    ResourceOptions.AutoConnect = False
    TxOptions.AutoStart = False
    TxOptions.AutoStop = False
    TxOptions.DisconnectAction = xdRollback
    LoginPrompt = False
    Left = 42
    Top = 15
  end
end
