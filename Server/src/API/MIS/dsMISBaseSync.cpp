//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsMISBaseSync.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------
//###########################################################################
//##                                                                       ##
//##                         TdsMISBaseSync                                ##
//##                                                                       ##
//###########################################################################
__fastcall TdsMISBaseSync::TdsMISBaseSync()
 {
 }
//---------------------------------------------------------------------------
__fastcall TdsMISBaseSync::~TdsMISBaseSync()
 {
 }
//----------------------------------------------------------------------------
void __fastcall TdsMISBaseSync::LogMessage(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl)
 {
  if (FOnLogMessage)
   FOnLogMessage(AMessage, AFuncName, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TdsMISBaseSync::LogError(UnicodeString  AMessage, UnicodeString  AFuncName, int ALvl)
 {
  if (FOnLogError)
   FOnLogError(AMessage, AFuncName, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TdsMISBaseSync::LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  if (FOnLogSQL)
   FOnLogSQL(ASQL, AFuncName, AExecTime, ALvl);
 }
//---------------------------------------------------------------------------
void __fastcall TdsMISBaseSync::LogBegin(UnicodeString  AFuncName)
 {
  if (FOnLogBegin)
   FOnLogBegin(AFuncName);
 }
//---------------------------------------------------------------------------
void __fastcall TdsMISBaseSync::LogEnd(UnicodeString  AFuncName)
 {
  if (FOnLogEnd)
   FOnLogEnd(AFuncName);
 }
//---------------------------------------------------------------------------
/*
TJSONObject * __fastcall TdsMISBaseSync::NewObjPair(UnicodeString  AId, UnicodeString  AVal)
 {
  return (new TJSONObject)->AddPair(AId, AVal);
 }
//----------------------------------------------------------------------------
TJSONObject * __fastcall TdsMISBaseSync::NewObjPair(UnicodeString  AId, TJSONValue * AVal)
 {
  return (new TJSONObject)->AddPair(AId, AVal);
 }
//----------------------------------------------------------------------------
bool __fastcall TdsMISBaseSync::IsNull(TJSONObject * ASrc, UnicodeString  AId)
 {
  bool RC = true;
  try
   {
    RC = GetJSONValue(ASrc, AId)->Null;
   }
  __finally
   {
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONValue * __fastcall TdsMISBaseSync::AsObj(TJSONObject * ASrc, UnicodeString  AId)
 {
  TJSONValue * RC = NULL;
  try
   {
    TJSONValue * Val = GetJSONValue(ASrc, AId);
    if (!Val->Null)
     {
      RC = new TJSONObject;
      ((TJSONObject *)RC)->AddPair(JSONToString(ASrc, AId), TJSONObject::ParseJSONValue(Val->ToString()));
     }
   }
  __finally
   {
    if (!RC)
     RC = new TJSONNull;
   }
  return RC;
 }
//----------------------------------------------------------------------------
TJSONValue * __fastcall TdsMISBaseSync::AsNewJSONValue(TJSONObject * ASrc, UnicodeString  AId)
 {
  TJSONValue * RC = NULL;
  try
   {
    RC = TJSONObject::ParseJSONValue(GetJSONValue(ASrc, AId)->ToString());
   }
  __finally
   {
    if (!RC)
     RC = new TJSONNull;
   }
  return RC;
 }
//----------------------------------------------------------------------------
*/
void __fastcall TdsMISBaseSync::AddClData(TJSONObject * ACls, UnicodeString  AId, UnicodeString  ACode, TJSONObject * AClData)
 {
  TJSONObject * FCl = (TJSONObject *)GetJSONValue(ACls, AId);
  if (!FCl)
   {//���� ��� ����������� AId, �� ������ ���
    FCl = new TJSONObject;
    ACls->AddPair(AId.UpperCase(), FCl);
   }
  if (FCl)
   {//������ ���, ���������� ����������
    if (!GetJSONValue(FCl, ACode))
     FCl->AddPair(ACode.UpperCase(), AClData);
   }
 }
//---------------------------------------------------------------------------
TJSONObject * __fastcall TdsMISBaseSync::Convert(TJSONObject * ASrc)
 {
  return NULL;
 }
//----------------------------------------------------------------------------
void __fastcall TdsMISBaseSync::AddPatState(TJSONObject * APtRoot, bool AUch, bool AOrg, bool AExit)
 {
  UnicodeString SupportType = "6"; //<choice name='������ ������������' uid='32B3'   >
  //.                                             ������������� �� ������� - 1
  //.                                             ������������� �� ����������� - 2
  //.                                             ������������� �� ������ - 3
  //.                                             �������������� - 4
  //.                                             ����� - 5
  //.                                             �� ������������� - 6

  if (AExit)
   SupportType = "5"; //�� ������������� - 6
  else if (AOrg)
   SupportType = "2"; //������������� �� ����������� - 2
  else if (AUch)
   SupportType = "1"; //������������� �� ������� - 1

  APtRoot->AddPair("32B3", (new TJSONObject)->AddPair(SupportType, SupportType)); //<choice name='������ ������������' uid='32B3'   >
 }
//---------------------------------------------------------------------------
