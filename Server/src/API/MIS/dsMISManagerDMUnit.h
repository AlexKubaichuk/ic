//---------------------------------------------------------------------------

#ifndef dsMISManagerDMUnitH
#define dsMISManagerDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Vcl.ExtCtrls.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.VCLUI.Wait.hpp>
#include <Datasnap.DSClientRest.hpp>
#include <System.JSON.hpp>
//---------------------------------------------------------------------------
//#include "ICUtils.h"
//#include "IcsXMLDoc.h"
#include "dsKLADRUnit.h"
#include "XMLContainer.h"
//#include "dsSrvClassifUnit.h"
#include "OrgParsers.h"
#include "icsLog.h"
//#include <IPPeerClient.hpp>
//---------------------------------------------------------------------------
class TdsMISManagerDM : public TDataModule
{
__published:	// IDE-managed Components
 TFDConnection *UsrConnection;
private:	// User declarations
  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;
  TOnLogSQLEvent      FOnLogSQL;

  TTagNode *FDefXML;
  UnicodeString __fastcall NewGUID();
  TAxeXMLContainer *FXMLList;
  TAppOptions *FOpt;

  void __fastcall LogMessage(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall LogError(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall LogEnd(UnicodeString  AFuncName);
  void __fastcall LogBegin(UnicodeString  AFuncName);
  void __fastcall LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl = 1);

public:		// User declarations
  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  bool      __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AParam1Name,      Variant AParam1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AParam2Name = "", Variant AParam2Val = 0,
                               UnicodeString AParam3Name = "", Variant AParam3Val = 0,
                               UnicodeString AParam4Name = "", Variant AParam4Val = 0);

  void __fastcall ConnectUsr();
  void __fastcall DisconnectUsr();

  __fastcall TdsMISManagerDM(TComponent* Owner, TAxeXMLContainer *AXMLList, TAppOptions *AOpt);
  __fastcall ~TdsMISManagerDM();

  __property TTagNode* DefXML = {read=FDefXML, write=FDefXML};
  __property TAxeXMLContainer *XMLList = {read=FXMLList};

  __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
  __property TOnLogMsgErrEvent OnLogError   = {read = FOnLogError, write = FOnLogError};
  __property TOnLogSQLEvent    OnLogSQL     = {read = FOnLogSQL, write = FOnLogSQL};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsMISManagerDM *dsMISManagerDM;
//---------------------------------------------------------------------------
#endif
