object dsMISSyncDM: TdsMISSyncDM
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Height = 482
  Width = 711
  object MISClient: TRESTClient
    Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    AutoCreateParams = False
    BaseURL = 'http://217.79.8.202:8732'
    Params = <>
    HandleRedirects = True
    Left = 47
    Top = 22
  end
  object MISRequest: TRESTRequest
    AutoCreateParams = False
    Client = MISClient
    Params = <>
    Response = MISResponse
    SynchronizedEvents = False
    Left = 45
    Top = 96
  end
  object MISResponse: TRESTResponse
    Left = 44
    Top = 161
  end
  object HTTPAuth: THTTPBasicAuthenticator
    Username = '1'
    Password = '1'
    Left = 184
    Top = 28
  end
  object ICImportConnect: TDSRestConnection
    Host = 'localhost'
    Port = 5200
    UserName = '1'
    Password = '1'
    LoginPrompt = False
    Left = 280
    Top = 29
    UniqueId = '{4D093C19-8D93-44E3-98DB-1E477669284E}'
  end
  object IC67Connection: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'CharacterSet=NONE'
      'Password=masterkey'
      'User_Name=sysdba'
      'Server=localhost'
      'Port=3050'
      'Pooled=False')
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    FormatOptions.AssignedValues = [fvFmtDisplayDateTime, fvFmtDisplayDate, fvFmtDisplayTime]
    FormatOptions.FmtDisplayDateTime = 'dd.mm.yyyy hh:nn:ss'
    FormatOptions.FmtDisplayDate = 'dd.mm.yyyy'
    FormatOptions.FmtDisplayTime = 'hh:nn:ss'
    ResourceOptions.AssignedValues = [rvDirectExecute, rvAutoConnect, rvSilentMode]
    ResourceOptions.DirectExecute = True
    ResourceOptions.SilentMode = True
    ResourceOptions.AutoConnect = False
    TxOptions.AutoStart = False
    TxOptions.AutoStop = False
    TxOptions.DisconnectAction = xdRollback
    LoginPrompt = False
    Left = 369
    Top = 30
  end
end
