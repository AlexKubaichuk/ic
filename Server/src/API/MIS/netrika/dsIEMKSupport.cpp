/*
 ������      - ���������� ������������.
 ��������� "���������� ������������"
 ��������    - �����������.
 ���� ����������
 ����������� - ������� �.�.
 ����        - 09.05.2004
 */
// ---------------------------------------------------------------------------
#include <string.h>
#pragma hdrstop
#include <algorithm>
#include <vcl.h>
#include <System.SysUtils.hpp>
#include "dsIEMKSupport.h"
#include "JSONUtils.h"
// #include "dsAuraSync.h"
// #include "dsMISCommonSync.h"
// #include "dsSrvRegTemplate.h"
// #include "OrgParsers.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ###########################################################################
// ##                                                                       ##
// ##                     TBackgroundIEMKSupport                              ##
// ##                                                                       ##
// ###########################################################################
class TdsIEMKSupport::TBackgroundIEMKSupport : public TThread
 {
 private:
  TdsIEMKSupport * FSyncModule;
 protected:
  virtual void __fastcall Execute();
 public:
  __fastcall TBackgroundIEMKSupport(bool CreateSuspended, TdsIEMKSupport * ASyncModule);
  __fastcall ~TBackgroundIEMKSupport();
  void __fastcall ThreadSafeCall(TdsSyncMethod Method);
 };
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                     TdsIEMKSupport::TBackgroundIEMKSupport              ##
// ##                                                                       ##
// ###########################################################################
__fastcall TdsIEMKSupport::TBackgroundIEMKSupport::TBackgroundIEMKSupport(bool CreateSuspended,
  TdsIEMKSupport * ASyncModule) : TThread(CreateSuspended), FSyncModule(ASyncModule)
 {
 }
// ---------------------------------------------------------------------------
__fastcall TdsIEMKSupport::TBackgroundIEMKSupport::~TBackgroundIEMKSupport()
 {
  FSyncModule = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::TBackgroundIEMKSupport::Execute()
 {
  CoInitialize(NULL);
  FSyncModule->Sync();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::TBackgroundIEMKSupport::ThreadSafeCall(TdsSyncMethod Method)
 {
  Synchronize(Method);
 }
// ---------------------------------------------------------------------------
// ###########################################################################
// ##                                                                       ##
// ##                              TdsIEMKSupport                           ##
// ##                                                                       ##
// ###########################################################################
__fastcall TdsIEMKSupport::TdsIEMKSupport(TdsMKB * AMKB, TAppOptions * AOpt, UnicodeString AUser, UnicodeString APass)
 {
  FOpt              = AOpt;
  FDM               = new TNetrikaDM(NULL, AMKB);
  FDM->OnLogMessage = CallLogMsg;
  FDM->OnLogError   = CallLogErr;
  FDM->OnLogSQL     = CallLogSQL;
  FDM->OnLogBegin   = CallLogBegin;
  FDM->OnLogEnd     = CallLogEnd;
  FCanSend          = false;
  idLPU             = "";
  TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
  if (FSes)
   idLPU = FSes->UserRoles->Values["LPUCode"];
  if (AOpt->Vals["urlmpi"].AsString.Length() && AOpt->Vals["urliemk"].AsString.Length() && idLPU.Length())
   {
    try
     {
      FDM->CreateServices(AOpt->Vals["urlmpi"].AsString + "?wsdl", AOpt->Vals["urliemk"].AsString + "?wsdl", AOpt);
      FCanSend = true;
     }
    catch (Exception & E)
     {
     }
   }
  FBkgSyncModule = NULL;
  FStage         = "";
  FMaxProgress   = 10;
  FOnIncProgress = NULL;
  FInSync        = false;
 }
// ---------------------------------------------------------------------------
__fastcall TdsIEMKSupport::~TdsIEMKSupport()
 {
  StopSync();
  if (FBkgSyncModule)
   delete FBkgSyncModule;
  FBkgSyncModule = NULL;
  delete FDM;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::CallOnIncProgress()
 {
  SyncCall(& SyncCallOnIncProgress);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::SyncCallOnIncProgress()
 {
  if (FOnIncProgress)
   FOnIncProgress(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::PopFront()
 {
  SyncCall(& SyncCallPopFront);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::SyncCallPopFront()
 {
  if (!FCallQuery.empty())
   FCallQuery.pop_front();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::SyncCall(TdsSyncMethod AMethod)
 {
  if (FBkgSyncModule)
   FBkgSyncModule->ThreadSafeCall(AMethod);
  else
   AMethod();
 }
// ---------------------------------------------------------------------------
int __fastcall TdsIEMKSupport::GetMaxProgress()
 {
  return FMaxProgress;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsIEMKSupport::GetCurProgress()
 {
  return FCurrProgress;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsIEMKSupport::GetCurStage() const
 {
  return FStage;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::CallLogMsg(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogMsg);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::CallLogErr(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  FLogMsg      = AMessage;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  SyncCall(& SyncCallLogErr);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::CallLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  FLogMsg      = ASQL;
  FLogFuncName = AFuncName;
  FLogALvl     = ALvl;
  FLogExecTime = AExecTime;
  SyncCall(& SyncCallLogSQL);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::CallLogBegin(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogBegin);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::CallLogEnd(UnicodeString AFuncName)
 {
  FLogFuncName = AFuncName;
  SyncCall(& SyncCallLogEnd);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::SyncCallLogMsg()
 {
  if (FOnLogMessage)
   FOnLogMessage(FLogMsg, FLogFuncName, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::SyncCallLogErr()
 {
  if (FOnLogError)
   FOnLogError(FLogMsg, FLogFuncName, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::SyncCallLogSQL()
 {
  if (FOnLogSQL)
   FOnLogSQL(FLogMsg, FLogFuncName, FLogExecTime, FLogALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::SyncCallLogBegin()
 {
  if (FOnLogBegin)
   FOnLogBegin(FLogFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsIEMKSupport::SyncCallLogEnd()
 {
  if (FOnLogEnd)
   FOnLogEnd(FLogFuncName);
 }
// ---------------------------------------------------------------------------
System::UnicodeString __fastcall TdsIEMKSupport::SendREGIZData(UnicodeString AId, TJSONObject * AValue, int AStatus)
 {
  CallLogBegin(__FUNC__);
  UnicodeString RC = "error";
  CallLogMsg(GetCurStage(), __FUNC__);
  // CallLogMsg(IntToStr(AStatus) + "$" + AId + ":" + AValue->ToString(), __FUNC__);
  FSendId     = AId;
  FSendStatus = AStatus;
  FCardData   = AValue->ToString();
  try
   {
    if (FCanSend)
     {
      if (InSync)
       {
        FCallQuery.push_back((IntToStr(AStatus) + "$" + AId + ":" + AValue->ToString()));
        CallLogMsg("��������� � ������� �� ��������: " + AId + ".", __FUNC__);
       }
      else
       {
        FInSync = true;
        if (FBkgSyncModule)
         {
          FBkgSyncModule->Terminate();
          Sleep(50);
          delete FBkgSyncModule;
          FBkgSyncModule = NULL;
         }
        FBkgSyncModule = new TBackgroundIEMKSupport(false, this);
        RC = "ok";
       }
     }
   }
  __finally
   {
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsIEMKSupport::StopSync()
 {
  UnicodeString RC = "error";
  CallLogBegin(__FUNC__);
  try
   {
    Proc        = "";
    FTerminated = true;
    if (FBkgSyncModule && InSync)
     {
      FInSync = false;
      delete FBkgSyncModule;
      FBkgSyncModule = NULL;
     }
    RC = "ok";
   }
  __finally
   {
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsIEMKSupport::Sync()
 {
  UnicodeString RC = "error";
  CallLogBegin(__FUNC__);
  UnicodeString FId, FData;
  int FStatus;
  try
   {
    FInSync = true;
    SendData(FSendId, FCardData, FSendStatus);
    while (!FCallQuery.empty() && !FTerminated)
     {
      FId     = FCallQuery.front();
      FData   = GetRPartB(FId, ':');
      FId     = GetLPartB(FId, ':');
      FStatus = GetLPartB(FId, '$').ToIntDef(0);
      FId     = GetRPartB(FId, '$');
      SendData(FId, FData, FStatus);
      PopFront();
     }
    RC = "";
   }
  __finally
   {
    FInSync = false;
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
System::UnicodeString __fastcall TdsIEMKSupport::SendData(UnicodeString AId, UnicodeString AData, int ASendStatus)
 {
  UnicodeString RC = "error";
  CallLogBegin(__FUNC__);
  TTagNode * FPatData = new TTagNode;
  TTagNode * FData = new TTagNode;
  TJSONObject * FSendData = (TJSONObject *)TJSONObject::ParseJSONValue(AData);
  // CallLogMsg(IntToStr(ASendStatus) + ": " + AId + ":  " + AData, __FUNC__);
  bool FCont;
  try
   {
    UnicodeString UCode;
    UnicodeString FRC;
    FCont = true;
    FRC   = "";
    if (AId == "1000")
     {
      FPatData->Name            = "patient";
      FPatData->AV["idPat"]     = JSONToString(FSendData, "32D2");
      FPatData->AV["Family"]    = JSONToString(FSendData, "001F");
      FPatData->AV["Name"]      = JSONToString(FSendData, "0020");
      FPatData->AV["SurName"]   = JSONToString(FSendData, "002F");
      FPatData->AV["BirthDate"] = JSONToString(FSendData, "0031");
      FPatData->AV["Sex"]       = IntToStr(JSONToInt(FSendData, "0021") + 1);
      FPatData->AV["DocN"]      = JSONToString(FSendData, "00A5");
      FCont                     = true;
     }
    else
     {
      if (AId == "1003")
       UCode = JSONObjToString(FSendData, "017A", ASendStatus);
      else if (AId.LowerCase() == "102f")
       UCode = JSONObjToString(FSendData, "017B", ASendStatus);
      else if (AId == "3030")
       UCode = JSONObjToString(FSendData, "317C", ASendStatus);
      else if (AId == "1112")
       UCode = JSONObjToString(FSendData, "1113", ASendStatus);
      FCont = FDM->GetPatData(UCode, FPatData);
     }
    if (FCont)
     {
      FPatData->AV["guid"]  = FOpt->Vals["keympi"].AsString;
      FPatData->AV["idLPU"] = idLPU;
      FRC                   = FDM->SendPatient(FPatData);
      if (!FRC.Length())
       {
        FData->AV["guid"]  = FOpt->Vals["keyiemk"].AsString;
        FData->AV["idLPU"] = idLPU;
        FData->AV["idPat"] = FPatData->AV["idPat"];
        if (AId != "1000")
         {
          if (!ASendStatus)
           {
            if (JSONToInt(FSendData, "code", 1) == 1)
             FData->AV["Status"] = "1";
            else
             FData->AV["Status"] = "2";
           }
          else
           FData->AV["Status"] = IntToStr(ASendStatus);
         }
        if (AId == "1000")
         { // ������ ����� ��������
          FDM->SendCardData(FPatData->AV["idPat"], FOpt->Vals["keyiemk"].AsString, idLPU);
         }
        else if (AId == "1003")
         {
          // card1003 > {"code":null,"32D7":"22106011-A5E7-4008-A6AB-C5C703C747B5","017A":"12661","3183":null,"32AE":"0","1024":"13.10.2019","10B4":null,"1020":"57","1021":"RV1/1:14^231A.231E;RV1/1:3^231A.231E;RV1/1:4^231A.231E;RV1/1:5^231A.231E","1075":"14","1022":"0","1023":null,"1025":"0","1026":"0","1028":"0","10A8":null,"1027":null,"10AE":"0","1092":null,"10AF":null,"101F":"1","017F":"1","10B0":null,"0181":null,"10B1":null,"0182":null,"330A":null,"3315":null,"1155":"09.12.1999"}
          FData->AV["RecId"] = JSONObjToString(FSendData, "32D7", ASendStatus).Trim(); // 32D7
          if (!FData->AV["RecId"].Length())
           FData->AV["RecId"] = JSONToString(FSendData, "code", ASendStatus).Trim(); // 32D7
          FData->AV["VacDate"] = JSONToString(FSendData, "1024");
          FData->AV["VacCode"] = FDM->GetVacCode(JSONObjToInt(FSendData, "1020", ASendStatus));
          // "1021":"RV1/1:14^231A.231E;RV1/1:3^231A.231E;RV1/1:4^231A.231E;RV1/1:5^231A.231E"
          FRC = JSONToString(FSendData, "1021");
          while (FRC.Length())
           {
            if (ASendStatus == 3)
             {
              FData->AV["VacType"] = FRC;
              FData->AddChild("i")->AV["Inf"] = FDM->GetInfCode(JSONObjToInt(FSendData, "1075", ASendStatus));
              FRC = "";
             }
            else
             {
              FData->AV["VacType"] = GetLPartB(FRC, ':');
              FRC                  = GetRPartB(FRC, ':');
              FData->AddChild("i")->AV["Inf"] = FDM->GetInfCode(GetLPartB(FRC, '^').ToIntDef(0));
              if (FRC == GetRPartB(FRC, ';'))
               FRC = "";
              else
               FRC = GetRPartB(FRC, ';');
             }
           }
          if (FDM->SendPriv(FData).Length())
           FDM->SendPriv(FData);
          RC = "";
         }
        else if (AId.LowerCase() == "102f")
         {
          // card102F > {"32AF":"0","1031":"13.10.2019","10B5":null,"1032":"11","1200":"26","1033":null,"1034":"0","103B":"1","10A7":"3","103F":"22","0184":null,"0186":null,"0188":null,"018A":null,"1093":"001B.0021","10A5":"1","018B":"1","10B2":null,"018D":null,"10B3":null,"018E":null,"10C1":null,"10C2":null,"330B":null,"3316":null,"1156":"09.12.1999"}
          FData->AV["RecId"]    = JSONObjToString(FSendData, "32D8", ASendStatus);
          if (!FData->AV["RecId"].Length())
           FData->AV["RecId"] = JSONToString(FSendData, "code", ASendStatus).Trim();
          FData->AV["TestDate"] = JSONToString(FSendData, "1031");
          FData->AV["TestCode"] = FDM->GetTestCode(JSONObjToInt(FSendData, "1032", ASendStatus));
          FData->AV["TestType"] = "3";
          FData->AddChild("i")->AV["Inf"] = FDM->GetInfCode(JSONObjToInt(FSendData, "1200", ASendStatus));
          if (FDM->SendTest(FData).Length())
           FDM->SendTest(FData);
          RC = "";
         }
        else if (AId == "3030")
         {
          // card3030 > {"305F":"13.10.2019","3065":"13.11.2019","3043":"0","3083":"all:��� ��������","3066":"3","3110":"1","3111":"0"}
          FData->AV["RecId"]     = JSONObjToString(FSendData, "32DA", ASendStatus);
          if (!FData->AV["RecId"].Length())
           FData->AV["RecId"] = JSONToString(FSendData, "code", ASendStatus).Trim();
          FData->AV["DateStart"] = JSONToString(FSendData, "305F");
          FData->AV["DateEnd"]   = JSONToString(FSendData, "3065");
          FData->AV["Type"]      = IntToStr(JSONToInt(FSendData, "3043") + 1);
          // 1	��������� 2	����������
          FData->AV["Reason"] = FDM->GetReasonCode(JSONToInt(FSendData, "3066"));
          FData->AV["MKB10"]  = FDM->GetMKBCode(JSONToString(FSendData, "3082"));
          FRC                 = JSONToString(FSendData, "3083");
          if (GetLPartB(FRC, ':').LowerCase() == "all" || ASendStatus == 3)
           {
            for (TIntStrMap::iterator i = FDM->InfCodes.begin(); i != FDM->InfCodes.end(); i++)
             FData->AddChild("i")->AV["Inf"] = i->second;
           }
          else
           {
            while (FRC.Length())
             {
              FData->AddChild("i")->AV["Inf"] = FDM->GetInfCode(GetLPartB(FRC, ':').ToIntDef(0));
              if (FRC == GetRPartB(FRC, ','))
               FRC = "";
              else
               FRC = GetRPartB(FRC, ',');
             }
           }
          if (FDM->SendCancel(FData).Length())
           FDM->SendCancel(FData);
          RC = "";
         }
        else if (AId == "1112")
         {
          // card1003 > {"code":null,"32D7":"22106011-A5E7-4008-A6AB-C5C703C747B5","017A":"12661","3183":null,"32AE":"0","1024":"13.10.2019","10B4":null,"1020":"57","1021":"RV1/1:14^231A.231E;RV1/1:3^231A.231E;RV1/1:4^231A.231E;RV1/1:5^231A.231E","1075":"14","1022":"0","1023":null,"1025":"0","1026":"0","1028":"0","10A8":null,"1027":null,"10AE":"0","1092":null,"10AF":null,"101F":"1","017F":"1","10B0":null,"0181":null,"10B1":null,"0182":null,"330A":null,"3315":null,"1155":"09.12.1999"}
          FData->AV["RecId"]   = JSONObjToString(FSendData, "32DC", ASendStatus);
          if (!FData->AV["RecId"].Length())
           FData->AV["RecId"] = JSONToString(FSendData, "code", ASendStatus).Trim();
          FData->AV["VacDate"] = JSONToString(FSendData, "1115");
          FData->AV["VacType"] = JSONToString(FSendData, "1117");
          if (JSONObjToInt(FSendData, "1116", ASendStatus) == 1)
           FData->AV["PlanType"] = "vac";
          else
           FData->AV["PlanType"] = "test";
          FData->AddChild("i")->AV["Inf"] = FDM->GetInfCode(JSONObjToInt(FSendData, "3187", ASendStatus));
          if (FDM->SendPlanData(FData).Length())
           FDM->SendPlanData(FData);
          RC = "";
         }
       }
      else
       CallLogErr(FRC, __FUNC__);
     }
   }
  __finally
   {
    delete FData;
    delete FPatData;
    delete FSendData;
   }
  CallLogEnd(__FUNC__);
  return RC;
 }
// ----------------------------------------------------------------------------
__int64 __fastcall JSONObjToInt(TJSONObject * AValue, UnicodeString AId, int ASendStatus)
 {
  __int64 RC = 0;
  try
   {
    if (AValue)
     {
      TJSONObject * FVal = (TJSONObject *)GetJSONValue(AValue, AId);
      if (FVal)
       {
        if (!FVal->Null)
         {
          if (ASendStatus == 3)
           RC = JSONPairName(FVal->Pairs[0]).ToIntDef(0);
          else
           RC = ((TJSONNumber *)FVal)->AsInt64;
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall JSONObjToString(TJSONObject * AValue, UnicodeString AId, int ASendStatus)
 {
  UnicodeString RC = "";
  try
   {
    if (AValue)
     {
      TJSONObject * FVal = (TJSONObject *)GetJSONValue(AValue, AId);
      if (FVal)
       {
        if (!FVal->Null)
         {
          if (ASendStatus == 3)
           RC = JSONPairName(FVal->Pairs[0]);
          else
           RC = ((TJSONString *)FVal)->Value();
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
