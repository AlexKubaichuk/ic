//---------------------------------------------------------------------------

#ifndef netrikaDMUnitH
#define netrikaDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>

#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <FireDAC.Comp.Client.hpp>

#include <Data.DB.hpp>
#include <Soap.InvokeRegistry.hpp>
#include <Soap.Rio.hpp>
#include <Soap.SOAPHTTPClient.hpp>

//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "EMKService.h"
#include "PixService.h"
#ifndef NETRIKATEST
 #include "kabBaseLog.h"
 #include "AppOptions.h"
 #include "dsMKB.h"
 #include "dsCommClassData.h"
#endif
//---------------------------------------------------------------------------
#ifdef NETRIKATEST
 enum class TSOAPLogType {ltRequest, ltResponse};
 typedef void __fastcall(__closure * TSOAPLogRequestEvent)(const UnicodeString AService, TSOAPLogType AType, const UnicodeString AMessage);
#endif
class TNetrikaDM : public TDataModule
{
__published:	// IDE-managed Components
 TFDConnection *IcConnection;
 THTTPRIO *HTTPPat;
 THTTPRIO *HTTPPriv;
 THTTPRIO *HTTPRIO1;
 void __fastcall HTTPPatBeforeExecute(const UnicodeString MethodName, TStream *SOAPRequest);
 void __fastcall HTTPPatAfterExecute(const UnicodeString MethodName, TStream *SOAPResponse);
 void __fastcall HTTPPrivBeforeExecute(const UnicodeString MethodName, TStream *SOAPRequest);
 void __fastcall HTTPPrivAfterExecute(const UnicodeString MethodName, TStream *SOAPResponse);
private:	// User declarations
  _di_IEmkService iemk;
  _di_IPixService mpi;
  String __fastcall GenerateGUID();
#ifndef NETRIKATEST
  TdsMKB * FMKB;
#endif

#ifndef NETRIKATEST
  TIntStrMap          FInfCodes;
  TIntStrMap          FVacCodes;
  TIntStrMap          FTestCodes;
#endif


#ifndef NETRIKATEST
  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;
  TOnLogSQLEvent      FOnLogSQL;
  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;

  void __fastcall LogMessage(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall LogError(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall LogSQL(UnicodeString ASQL, UnicodeString AFuncName = "", TTime AExecTime = 0, int ALvl = 1);
  void __fastcall LogBegin(UnicodeString AFuncName);
  void __fastcall LogEnd(UnicodeString AFuncName);
#else
  TSOAPLogRequestEvent FOnLog;
#endif

#ifndef NETRIKATEST
  TFDQuery* __fastcall CreateTempQuery();
  void      __fastcall DeleteTempQuery(TFDQuery *AQuery);
  bool      __fastcall quExec(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment = "");
  UnicodeString __fastcall quExecParam(TFDQuery *AQuery, bool ACommit, UnicodeString ASQL,
                               UnicodeString AP1Name,      Variant AP1Val,
                               UnicodeString ALogComment = "",
                               UnicodeString AP2Name = "", Variant AP2Val = 0,
                               UnicodeString AP3Name = "", Variant AP3Val = 0,
                               UnicodeString AP4Name = "", Variant AP4Val = 0,
                               UnicodeString AP5Name = "", Variant AP5Val = 0);
#endif
public:		// User declarations
#ifndef NETRIKATEST
 __fastcall TNetrikaDM(TComponent* Owner, TdsMKB * AMKB);
  void __fastcall CreateServices(System::String PatWSDL, System::String IEMKWSDL, TAppOptions * AOpt);
#else
 __fastcall TNetrikaDM(TComponent* Owner);
  void __fastcall CreateServices(System::String PatWSDL, System::String IEMKWSDL);
#endif
  UnicodeString __fastcall SendPatient(TTagNode *AData);
  UnicodeString __fastcall SendPriv(TTagNode *AData);
  UnicodeString __fastcall SendTest(TTagNode *AData);
  UnicodeString __fastcall SendCancel(TTagNode *AData);
  UnicodeString __fastcall SendCardData(UnicodeString AUCode, UnicodeString AGUID, UnicodeString ALPU);
  UnicodeString __fastcall SendPlanData(TTagNode * AData);

  bool          __fastcall GetPatData(System::UnicodeString APtId, TTagNode *AData);
  UnicodeString __fastcall GetInfCode(int ACode);
  UnicodeString __fastcall GetVacCode(int ACode);
  UnicodeString __fastcall GetTestCode(int ACode);
  UnicodeString __fastcall GetReasonCode(int ACode);
  UnicodeString __fastcall GetMKBCode(UnicodeString ACode);

#ifndef NETRIKATEST
  __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
  __property TOnLogMsgErrEvent OnLogError = {read = FOnLogError, write = FOnLogError};

  __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
  __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};
  __property TOnLogSQLEvent    OnLogSQL   = {read = FOnLogSQL, write = FOnLogSQL};

  __property TIntStrMap        InfCodes   = {read = FInfCodes};
#else
  __property TSOAPLogRequestEvent Logger   = {read = FOnLog, write = FOnLog};
#endif
};
//---------------------------------------------------------------------------
extern PACKAGE TNetrikaDM *NetrikaDM;
//---------------------------------------------------------------------------
#endif

