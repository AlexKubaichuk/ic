// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl>0
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl>1
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl>2
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl>3
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl>4
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl>5
// Encoding : utf-8
// Codegen  : [wfForceSOAP11+]
// Version  : 1.0
// (10.10.2019 20:35:39 - - $Rev: 69934 $)
// ************************************************************************ //

#ifndef   PixServiceH
#define   PixServiceH

#include <System.hpp>
#include <Soap.InvokeRegistry.hpp>
#include <Soap.XSBuiltIns.hpp>
#include <Soap.SOAPHTTPClient.hpp>

#if !defined(SOAP_REMOTABLE_CLASS)
#define SOAP_REMOTABLE_CLASS __declspec(delphiclass)
#endif
#if !defined(IS_OPTN)
#define IS_OPTN 0x0001
#endif
#if !defined(IS_UNBD)
#define IS_UNBD 0x0002
#endif
#if !defined(IS_NLBL)
#define IS_NLBL 0x0004
#endif
#if !defined(IS_REF)
#define IS_REF 0x0080
#endif


namespace NS_PixService {

// ************************************************************************ //
// The following types, referred to in the WSDL document are not being represented
// in this file. They are either aliases[@] of other types represented or were referred
// to but never[!] declared in the document. The types from the latter category
// typically map to predefined/known XML or Embarcadero types; however, they could also 
// indicate incorrect WSDL documents that failed to declare or import a schema type.
// ************************************************************************ //
// !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]
// !:boolean         - "http://www.w3.org/2001/XMLSchema"[Gbl]
// !:dateTime        - "http://www.w3.org/2001/XMLSchema"[Gbl]
// !:int             - "http://www.w3.org/2001/XMLSchema"[Gbl]
// !:unsignedByte    - "http://www.w3.org/2001/XMLSchema"[Gbl]

class SOAP_REMOTABLE_CLASS VersionInfo2;
class SOAP_REMOTABLE_CLASS VersionInfo;
class SOAP_REMOTABLE_CLASS RequestFault;
class SOAP_REMOTABLE_CLASS RequestFault2;
class SOAP_REMOTABLE_CLASS DocumentDto2;
class SOAP_REMOTABLE_CLASS DocumentDto;
class SOAP_REMOTABLE_CLASS PrivilegeDto2;
class SOAP_REMOTABLE_CLASS PrivilegeDto;
class SOAP_REMOTABLE_CLASS JobDto2;
class SOAP_REMOTABLE_CLASS JobDto;
class SOAP_REMOTABLE_CLASS ContactDto2;
class SOAP_REMOTABLE_CLASS ContactDto;
class SOAP_REMOTABLE_CLASS AddressDto2;
class SOAP_REMOTABLE_CLASS AddressDto;
class SOAP_REMOTABLE_CLASS BirthPlaceDto2;
class SOAP_REMOTABLE_CLASS BirthPlaceDto;
class SOAP_REMOTABLE_CLASS ContactPersonDto2;
class SOAP_REMOTABLE_CLASS ContactPersonDto;
class SOAP_REMOTABLE_CLASS PatientDto2;
class SOAP_REMOTABLE_CLASS PatientDto;

enum class SourceType   /* "http://schemas.datacontract.org/2004/07/N3.EMK.Domain.Enums"[GblSmpl] */
{
  Unknown, 
  Reg, 
  Fed
};

class SourceType_TypeInfoHolder : public TObject {
  SourceType __instanceType;
public:
__published:
  __property SourceType __propType = { read=__instanceType };
};



// ************************************************************************ //
// XML       : VersionInfo, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.Shared.Core
// ************************************************************************ //
class VersionInfo2 : public TRemotable {
private:
  UnicodeString   FDatabaseVersion;
  bool            FDatabaseVersion_Specified;
  UnicodeString   FServiceVersion;
  bool            FServiceVersion_Specified;
  void __fastcall SetDatabaseVersion(int Index, UnicodeString _prop_val)
  {  FDatabaseVersion = _prop_val; FDatabaseVersion_Specified = true;  }
  bool __fastcall DatabaseVersion_Specified(int Index)
  {  return FDatabaseVersion_Specified;  } 
  void __fastcall SetServiceVersion(int Index, UnicodeString _prop_val)
  {  FServiceVersion = _prop_val; FServiceVersion_Specified = true;  }
  bool __fastcall ServiceVersion_Specified(int Index)
  {  return FServiceVersion_Specified;  } 
__published:
  __property UnicodeString DatabaseVersion = { index=(IS_OPTN | IS_NLBL), read=FDatabaseVersion, write=SetDatabaseVersion, stored = DatabaseVersion_Specified };
  __property UnicodeString ServiceVersion = { index=(IS_OPTN | IS_NLBL), read=FServiceVersion, write=SetServiceVersion, stored = ServiceVersion_Specified };
};


typedef DynamicArray<RequestFault2*> ArrayOfRequestFault2; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common"[GblCplx] */
typedef ArrayOfRequestFault2 ArrayOfRequestFault; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common"[Flt][GblElm] */
typedef DynamicArray<PatientDto2*> ArrayOfPatientDto; /* "http://schemas.datacontract.org/2004/07/EMKService.Data.Dto"[GblCplx] */


// ************************************************************************ //
// XML       : VersionInfo, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.Shared.Core
// ************************************************************************ //
class VersionInfo : public VersionInfo2 {
private:
__published:
};




// ************************************************************************ //
// XML       : RequestFault, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common
// Info      : Fault
// Base Types: RequestFault
// ************************************************************************ //
class RequestFault : public ERemotableException {
private:
  UnicodeString   FPropertyName;
  bool            FPropertyName_Specified;
  UnicodeString   FMessage;
  bool            FMessage_Specified;
  int             FErrorCode;
  bool            FErrorCode_Specified;
  ArrayOfRequestFault2 FErrors;
  bool            FErrors_Specified;
  void __fastcall SetPropertyName(int Index, UnicodeString _prop_val)
  {  FPropertyName = _prop_val; FPropertyName_Specified = true;  }
  bool __fastcall PropertyName_Specified(int Index)
  {  return FPropertyName_Specified;  } 
  void __fastcall SetMessage(int Index, UnicodeString _prop_val)
  {  FMessage = _prop_val; FMessage_Specified = true;  }
  bool __fastcall Message_Specified(int Index)
  {  return FMessage_Specified;  } 
  void __fastcall SetErrorCode(int Index, int _prop_val)
  {  FErrorCode = _prop_val; FErrorCode_Specified = true;  }
  bool __fastcall ErrorCode_Specified(int Index)
  {  return FErrorCode_Specified;  } 
  void __fastcall SetErrors(int Index, ArrayOfRequestFault2 _prop_val)
  {  FErrors = _prop_val; FErrors_Specified = true;  }
  bool __fastcall Errors_Specified(int Index)
  {  return FErrors_Specified;  } 

public:
  __fastcall ~RequestFault();
__published:
  __property UnicodeString PropertyName = { index=(IS_OPTN | IS_NLBL), read=FPropertyName, write=SetPropertyName, stored = PropertyName_Specified };
  __property UnicodeString    Message = { index=(IS_OPTN | IS_NLBL), read=FMessage, write=SetMessage, stored = Message_Specified };
  __property int         ErrorCode = { index=(IS_OPTN), read=FErrorCode, write=SetErrorCode, stored = ErrorCode_Specified };
  __property ArrayOfRequestFault2     Errors = { index=(IS_OPTN | IS_NLBL), read=FErrors, write=SetErrors, stored = Errors_Specified };
};




// ************************************************************************ //
// XML       : RequestFault, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common
// ************************************************************************ //
class RequestFault2 : public TRemotable {
private:
  UnicodeString   FPropertyName;
  bool            FPropertyName_Specified;
  UnicodeString   FMessage;
  bool            FMessage_Specified;
  int             FErrorCode;
  bool            FErrorCode_Specified;
  ArrayOfRequestFault2 FErrors;
  bool            FErrors_Specified;
  void __fastcall SetPropertyName(int Index, UnicodeString _prop_val)
  {  FPropertyName = _prop_val; FPropertyName_Specified = true;  }
  bool __fastcall PropertyName_Specified(int Index)
  {  return FPropertyName_Specified;  } 
  void __fastcall SetMessage(int Index, UnicodeString _prop_val)
  {  FMessage = _prop_val; FMessage_Specified = true;  }
  bool __fastcall Message_Specified(int Index)
  {  return FMessage_Specified;  } 
  void __fastcall SetErrorCode(int Index, int _prop_val)
  {  FErrorCode = _prop_val; FErrorCode_Specified = true;  }
  bool __fastcall ErrorCode_Specified(int Index)
  {  return FErrorCode_Specified;  } 
  void __fastcall SetErrors(int Index, ArrayOfRequestFault2 _prop_val)
  {  FErrors = _prop_val; FErrors_Specified = true;  }
  bool __fastcall Errors_Specified(int Index)
  {  return FErrors_Specified;  } 

public:
  __fastcall ~RequestFault2();
__published:
  __property UnicodeString PropertyName = { index=(IS_OPTN | IS_NLBL), read=FPropertyName, write=SetPropertyName, stored = PropertyName_Specified };
  __property UnicodeString    Message = { index=(IS_OPTN | IS_NLBL), read=FMessage, write=SetMessage, stored = Message_Specified };
  __property int         ErrorCode = { index=(IS_OPTN), read=FErrorCode, write=SetErrorCode, stored = ErrorCode_Specified };
  __property ArrayOfRequestFault2     Errors = { index=(IS_OPTN | IS_NLBL), read=FErrors, write=SetErrors, stored = Errors_Specified };
};




// ************************************************************************ //
// XML       : DocumentDto, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class DocumentDto2 : public TRemotable {
private:
  UnicodeString   FDocN;
  bool            FDocN_Specified;
  UnicodeString   FDocS;
  bool            FDocS_Specified;
  UnicodeString   FDocumentName;
  bool            FDocumentName_Specified;
  TXSDateTime*    FExpiredDate;
  bool            FExpiredDate_Specified;
  UnicodeString   FIdDocumentType;
  bool            FIdDocumentType_Specified;
  UnicodeString   FIdProvider;
  bool            FIdProvider_Specified;
  TXSDateTime*    FIssuedDate;
  bool            FIssuedDate_Specified;
  UnicodeString   FProviderName;
  bool            FProviderName_Specified;
  UnicodeString   FRegionCode;
  bool            FRegionCode_Specified;
  TXSDateTime*    FStartDate;
  bool            FStartDate_Specified;
  void __fastcall SetDocN(int Index, UnicodeString _prop_val)
  {  FDocN = _prop_val; FDocN_Specified = true;  }
  bool __fastcall DocN_Specified(int Index)
  {  return FDocN_Specified;  } 
  void __fastcall SetDocS(int Index, UnicodeString _prop_val)
  {  FDocS = _prop_val; FDocS_Specified = true;  }
  bool __fastcall DocS_Specified(int Index)
  {  return FDocS_Specified;  } 
  void __fastcall SetDocumentName(int Index, UnicodeString _prop_val)
  {  FDocumentName = _prop_val; FDocumentName_Specified = true;  }
  bool __fastcall DocumentName_Specified(int Index)
  {  return FDocumentName_Specified;  } 
  void __fastcall SetExpiredDate(int Index, TXSDateTime* _prop_val)
  {  FExpiredDate = _prop_val; FExpiredDate_Specified = true;  }
  bool __fastcall ExpiredDate_Specified(int Index)
  {  return FExpiredDate_Specified;  } 
  void __fastcall SetIdDocumentType(int Index, UnicodeString _prop_val)
  {  FIdDocumentType = _prop_val; FIdDocumentType_Specified = true;  }
  bool __fastcall IdDocumentType_Specified(int Index)
  {  return FIdDocumentType_Specified;  } 
  void __fastcall SetIdProvider(int Index, UnicodeString _prop_val)
  {  FIdProvider = _prop_val; FIdProvider_Specified = true;  }
  bool __fastcall IdProvider_Specified(int Index)
  {  return FIdProvider_Specified;  } 
  void __fastcall SetIssuedDate(int Index, TXSDateTime* _prop_val)
  {  FIssuedDate = _prop_val; FIssuedDate_Specified = true;  }
  bool __fastcall IssuedDate_Specified(int Index)
  {  return FIssuedDate_Specified;  } 
  void __fastcall SetProviderName(int Index, UnicodeString _prop_val)
  {  FProviderName = _prop_val; FProviderName_Specified = true;  }
  bool __fastcall ProviderName_Specified(int Index)
  {  return FProviderName_Specified;  } 
  void __fastcall SetRegionCode(int Index, UnicodeString _prop_val)
  {  FRegionCode = _prop_val; FRegionCode_Specified = true;  }
  bool __fastcall RegionCode_Specified(int Index)
  {  return FRegionCode_Specified;  } 
  void __fastcall SetStartDate(int Index, TXSDateTime* _prop_val)
  {  FStartDate = _prop_val; FStartDate_Specified = true;  }
  bool __fastcall StartDate_Specified(int Index)
  {  return FStartDate_Specified;  } 

public:
  __fastcall ~DocumentDto2();
__published:
  __property UnicodeString       DocN = { index=(IS_OPTN | IS_NLBL), read=FDocN, write=SetDocN, stored = DocN_Specified };
  __property UnicodeString       DocS = { index=(IS_OPTN | IS_NLBL), read=FDocS, write=SetDocS, stored = DocS_Specified };
  __property UnicodeString DocumentName = { index=(IS_OPTN | IS_NLBL), read=FDocumentName, write=SetDocumentName, stored = DocumentName_Specified };
  __property TXSDateTime* ExpiredDate = { index=(IS_OPTN | IS_NLBL), read=FExpiredDate, write=SetExpiredDate, stored = ExpiredDate_Specified };
  __property UnicodeString IdDocumentType = { index=(IS_OPTN), read=FIdDocumentType, write=SetIdDocumentType, stored = IdDocumentType_Specified };
  __property UnicodeString IdProvider = { index=(IS_OPTN | IS_NLBL), read=FIdProvider, write=SetIdProvider, stored = IdProvider_Specified };
  __property TXSDateTime* IssuedDate = { index=(IS_OPTN | IS_NLBL), read=FIssuedDate, write=SetIssuedDate, stored = IssuedDate_Specified };
  __property UnicodeString ProviderName = { index=(IS_OPTN | IS_NLBL), read=FProviderName, write=SetProviderName, stored = ProviderName_Specified };
  __property UnicodeString RegionCode = { index=(IS_OPTN | IS_NLBL), read=FRegionCode, write=SetRegionCode, stored = RegionCode_Specified };
  __property TXSDateTime*  StartDate = { index=(IS_OPTN | IS_NLBL), read=FStartDate, write=SetStartDate, stored = StartDate_Specified };
};




// ************************************************************************ //
// XML       : DocumentDto, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class DocumentDto : public DocumentDto2 {
private:
__published:
};


typedef DynamicArray<AddressDto2*> ArrayOfAddressDto; /* "http://schemas.datacontract.org/2004/07/EMKService.Data.Dto"[GblCplx] */


// ************************************************************************ //
// XML       : PrivilegeDto, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class PrivilegeDto2 : public TRemotable {
private:
  UnicodeString   FComment;
  bool            FComment_Specified;
  TXSDateTime*    FDateEnd;
  bool            FDateEnd_Specified;
  TXSDateTime*    FDateStart;
  bool            FDateStart_Specified;
  UnicodeString   FDisabilityDegree;
  bool            FDisabilityDegree_Specified;
  unsigned char   FIdDisabilityType;
  bool            FIdDisabilityType_Specified;
  unsigned char   FIdPrivilegeType;
  bool            FIdPrivilegeType_Specified;
  bool            FIsMain;
  bool            FIsMain_Specified;
  UnicodeString   FMkbCode;
  bool            FMkbCode_Specified;
  DocumentDto2*   FPrivilegeDocument;
  bool            FPrivilegeDocument_Specified;
  void __fastcall SetComment(int Index, UnicodeString _prop_val)
  {  FComment = _prop_val; FComment_Specified = true;  }
  bool __fastcall Comment_Specified(int Index)
  {  return FComment_Specified;  } 
  void __fastcall SetDateEnd(int Index, TXSDateTime* _prop_val)
  {  FDateEnd = _prop_val; FDateEnd_Specified = true;  }
  bool __fastcall DateEnd_Specified(int Index)
  {  return FDateEnd_Specified;  } 
  void __fastcall SetDateStart(int Index, TXSDateTime* _prop_val)
  {  FDateStart = _prop_val; FDateStart_Specified = true;  }
  bool __fastcall DateStart_Specified(int Index)
  {  return FDateStart_Specified;  } 
  void __fastcall SetDisabilityDegree(int Index, UnicodeString _prop_val)
  {  FDisabilityDegree = _prop_val; FDisabilityDegree_Specified = true;  }
  bool __fastcall DisabilityDegree_Specified(int Index)
  {  return FDisabilityDegree_Specified;  } 
  void __fastcall SetIdDisabilityType(int Index, unsigned char _prop_val)
  {  FIdDisabilityType = _prop_val; FIdDisabilityType_Specified = true;  }
  bool __fastcall IdDisabilityType_Specified(int Index)
  {  return FIdDisabilityType_Specified;  } 
  void __fastcall SetIdPrivilegeType(int Index, unsigned char _prop_val)
  {  FIdPrivilegeType = _prop_val; FIdPrivilegeType_Specified = true;  }
  bool __fastcall IdPrivilegeType_Specified(int Index)
  {  return FIdPrivilegeType_Specified;  } 
  void __fastcall SetIsMain(int Index, bool _prop_val)
  {  FIsMain = _prop_val; FIsMain_Specified = true;  }
  bool __fastcall IsMain_Specified(int Index)
  {  return FIsMain_Specified;  } 
  void __fastcall SetMkbCode(int Index, UnicodeString _prop_val)
  {  FMkbCode = _prop_val; FMkbCode_Specified = true;  }
  bool __fastcall MkbCode_Specified(int Index)
  {  return FMkbCode_Specified;  } 
  void __fastcall SetPrivilegeDocument(int Index, DocumentDto2* _prop_val)
  {  FPrivilegeDocument = _prop_val; FPrivilegeDocument_Specified = true;  }
  bool __fastcall PrivilegeDocument_Specified(int Index)
  {  return FPrivilegeDocument_Specified;  } 

public:
  __fastcall ~PrivilegeDto2();
__published:
  __property UnicodeString    Comment = { index=(IS_OPTN | IS_NLBL), read=FComment, write=SetComment, stored = Comment_Specified };
  __property TXSDateTime*    DateEnd = { index=(IS_OPTN), read=FDateEnd, write=SetDateEnd, stored = DateEnd_Specified };
  __property TXSDateTime*  DateStart = { index=(IS_OPTN), read=FDateStart, write=SetDateStart, stored = DateStart_Specified };
  __property UnicodeString DisabilityDegree = { index=(IS_OPTN | IS_NLBL), read=FDisabilityDegree, write=SetDisabilityDegree, stored = DisabilityDegree_Specified };
  __property unsigned char IdDisabilityType = { index=(IS_OPTN | IS_NLBL), read=FIdDisabilityType, write=SetIdDisabilityType, stored = IdDisabilityType_Specified };
  __property unsigned char IdPrivilegeType = { index=(IS_OPTN), read=FIdPrivilegeType, write=SetIdPrivilegeType, stored = IdPrivilegeType_Specified };
  __property bool           IsMain = { index=(IS_OPTN), read=FIsMain, write=SetIsMain, stored = IsMain_Specified };
  __property UnicodeString    MkbCode = { index=(IS_OPTN | IS_NLBL), read=FMkbCode, write=SetMkbCode, stored = MkbCode_Specified };
  __property DocumentDto2* PrivilegeDocument = { index=(IS_OPTN | IS_NLBL), read=FPrivilegeDocument, write=SetPrivilegeDocument, stored = PrivilegeDocument_Specified };
};




// ************************************************************************ //
// XML       : PrivilegeDto, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class PrivilegeDto : public PrivilegeDto2 {
private:
__published:
};




// ************************************************************************ //
// XML       : JobDto, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class JobDto2 : public TRemotable {
private:
  UnicodeString   FCompanyName;
  bool            FCompanyName_Specified;
  TXSDateTime*    FDateEnd;
  bool            FDateEnd_Specified;
  TXSDateTime*    FDateStart;
  bool            FDateStart_Specified;
  UnicodeString   FOgrnCode;
  bool            FOgrnCode_Specified;
  UnicodeString   FPosition;
  bool            FPosition_Specified;
  UnicodeString   FSphere;
  bool            FSphere_Specified;
  void __fastcall SetCompanyName(int Index, UnicodeString _prop_val)
  {  FCompanyName = _prop_val; FCompanyName_Specified = true;  }
  bool __fastcall CompanyName_Specified(int Index)
  {  return FCompanyName_Specified;  } 
  void __fastcall SetDateEnd(int Index, TXSDateTime* _prop_val)
  {  FDateEnd = _prop_val; FDateEnd_Specified = true;  }
  bool __fastcall DateEnd_Specified(int Index)
  {  return FDateEnd_Specified;  } 
  void __fastcall SetDateStart(int Index, TXSDateTime* _prop_val)
  {  FDateStart = _prop_val; FDateStart_Specified = true;  }
  bool __fastcall DateStart_Specified(int Index)
  {  return FDateStart_Specified;  } 
  void __fastcall SetOgrnCode(int Index, UnicodeString _prop_val)
  {  FOgrnCode = _prop_val; FOgrnCode_Specified = true;  }
  bool __fastcall OgrnCode_Specified(int Index)
  {  return FOgrnCode_Specified;  } 
  void __fastcall SetPosition(int Index, UnicodeString _prop_val)
  {  FPosition = _prop_val; FPosition_Specified = true;  }
  bool __fastcall Position_Specified(int Index)
  {  return FPosition_Specified;  } 
  void __fastcall SetSphere(int Index, UnicodeString _prop_val)
  {  FSphere = _prop_val; FSphere_Specified = true;  }
  bool __fastcall Sphere_Specified(int Index)
  {  return FSphere_Specified;  } 

public:
  __fastcall ~JobDto2();
__published:
  __property UnicodeString CompanyName = { index=(IS_OPTN | IS_NLBL), read=FCompanyName, write=SetCompanyName, stored = CompanyName_Specified };
  __property TXSDateTime*    DateEnd = { index=(IS_OPTN | IS_NLBL), read=FDateEnd, write=SetDateEnd, stored = DateEnd_Specified };
  __property TXSDateTime*  DateStart = { index=(IS_OPTN | IS_NLBL), read=FDateStart, write=SetDateStart, stored = DateStart_Specified };
  __property UnicodeString   OgrnCode = { index=(IS_OPTN | IS_NLBL), read=FOgrnCode, write=SetOgrnCode, stored = OgrnCode_Specified };
  __property UnicodeString   Position = { index=(IS_OPTN | IS_NLBL), read=FPosition, write=SetPosition, stored = Position_Specified };
  __property UnicodeString     Sphere = { index=(IS_OPTN | IS_NLBL), read=FSphere, write=SetSphere, stored = Sphere_Specified };
};




// ************************************************************************ //
// XML       : JobDto, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class JobDto : public JobDto2 {
private:
__published:
};




// ************************************************************************ //
// XML       : ContactDto, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class ContactDto2 : public TRemotable {
private:
  UnicodeString   FContactValue;
  bool            FContactValue_Specified;
  unsigned char   FIdContactType;
  bool            FIdContactType_Specified;
  void __fastcall SetContactValue(int Index, UnicodeString _prop_val)
  {  FContactValue = _prop_val; FContactValue_Specified = true;  }
  bool __fastcall ContactValue_Specified(int Index)
  {  return FContactValue_Specified;  } 
  void __fastcall SetIdContactType(int Index, unsigned char _prop_val)
  {  FIdContactType = _prop_val; FIdContactType_Specified = true;  }
  bool __fastcall IdContactType_Specified(int Index)
  {  return FIdContactType_Specified;  } 
__published:
  __property UnicodeString ContactValue = { index=(IS_OPTN | IS_NLBL), read=FContactValue, write=SetContactValue, stored = ContactValue_Specified };
  __property unsigned char IdContactType = { index=(IS_OPTN), read=FIdContactType, write=SetIdContactType, stored = IdContactType_Specified };
};




// ************************************************************************ //
// XML       : ContactDto, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class ContactDto : public ContactDto2 {
private:
__published:
};




// ************************************************************************ //
// XML       : AddressDto, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class AddressDto2 : public TRemotable {
private:
  UnicodeString   FAppartment;
  bool            FAppartment_Specified;
  UnicodeString   FBuilding;
  bool            FBuilding_Specified;
  UnicodeString   FCity;
  bool            FCity_Specified;
  UnicodeString   FGeoData;
  bool            FGeoData_Specified;
  unsigned char   FIdAddressType;
  bool            FIdAddressType_Specified;
  int             FPostalCode;
  bool            FPostalCode_Specified;
  UnicodeString   FStreet;
  bool            FStreet_Specified;
  UnicodeString   FStringAddress;
  bool            FStringAddress_Specified;
  void __fastcall SetAppartment(int Index, UnicodeString _prop_val)
  {  FAppartment = _prop_val; FAppartment_Specified = true;  }
  bool __fastcall Appartment_Specified(int Index)
  {  return FAppartment_Specified;  } 
  void __fastcall SetBuilding(int Index, UnicodeString _prop_val)
  {  FBuilding = _prop_val; FBuilding_Specified = true;  }
  bool __fastcall Building_Specified(int Index)
  {  return FBuilding_Specified;  } 
  void __fastcall SetCity(int Index, UnicodeString _prop_val)
  {  FCity = _prop_val; FCity_Specified = true;  }
  bool __fastcall City_Specified(int Index)
  {  return FCity_Specified;  } 
  void __fastcall SetGeoData(int Index, UnicodeString _prop_val)
  {  FGeoData = _prop_val; FGeoData_Specified = true;  }
  bool __fastcall GeoData_Specified(int Index)
  {  return FGeoData_Specified;  } 
  void __fastcall SetIdAddressType(int Index, unsigned char _prop_val)
  {  FIdAddressType = _prop_val; FIdAddressType_Specified = true;  }
  bool __fastcall IdAddressType_Specified(int Index)
  {  return FIdAddressType_Specified;  } 
  void __fastcall SetPostalCode(int Index, int _prop_val)
  {  FPostalCode = _prop_val; FPostalCode_Specified = true;  }
  bool __fastcall PostalCode_Specified(int Index)
  {  return FPostalCode_Specified;  } 
  void __fastcall SetStreet(int Index, UnicodeString _prop_val)
  {  FStreet = _prop_val; FStreet_Specified = true;  }
  bool __fastcall Street_Specified(int Index)
  {  return FStreet_Specified;  } 
  void __fastcall SetStringAddress(int Index, UnicodeString _prop_val)
  {  FStringAddress = _prop_val; FStringAddress_Specified = true;  }
  bool __fastcall StringAddress_Specified(int Index)
  {  return FStringAddress_Specified;  } 
__published:
  __property UnicodeString Appartment = { index=(IS_OPTN | IS_NLBL), read=FAppartment, write=SetAppartment, stored = Appartment_Specified };
  __property UnicodeString   Building = { index=(IS_OPTN | IS_NLBL), read=FBuilding, write=SetBuilding, stored = Building_Specified };
  __property UnicodeString       City = { index=(IS_OPTN | IS_NLBL), read=FCity, write=SetCity, stored = City_Specified };
  __property UnicodeString    GeoData = { index=(IS_OPTN | IS_NLBL), read=FGeoData, write=SetGeoData, stored = GeoData_Specified };
  __property unsigned char IdAddressType = { index=(IS_OPTN), read=FIdAddressType, write=SetIdAddressType, stored = IdAddressType_Specified };
  __property int        PostalCode = { index=(IS_OPTN | IS_NLBL), read=FPostalCode, write=SetPostalCode, stored = PostalCode_Specified };
  __property UnicodeString     Street = { index=(IS_OPTN | IS_NLBL), read=FStreet, write=SetStreet, stored = Street_Specified };
  __property UnicodeString StringAddress = { index=(IS_OPTN | IS_NLBL), read=FStringAddress, write=SetStringAddress, stored = StringAddress_Specified };
};




// ************************************************************************ //
// XML       : AddressDto, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class AddressDto : public AddressDto2 {
private:
__published:
};




// ************************************************************************ //
// XML       : BirthPlaceDto, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class BirthPlaceDto2 : public TRemotable {
private:
  UnicodeString   FCity;
  bool            FCity_Specified;
  UnicodeString   FCountry;
  bool            FCountry_Specified;
  UnicodeString   FRegion;
  bool            FRegion_Specified;
  void __fastcall SetCity(int Index, UnicodeString _prop_val)
  {  FCity = _prop_val; FCity_Specified = true;  }
  bool __fastcall City_Specified(int Index)
  {  return FCity_Specified;  } 
  void __fastcall SetCountry(int Index, UnicodeString _prop_val)
  {  FCountry = _prop_val; FCountry_Specified = true;  }
  bool __fastcall Country_Specified(int Index)
  {  return FCountry_Specified;  } 
  void __fastcall SetRegion(int Index, UnicodeString _prop_val)
  {  FRegion = _prop_val; FRegion_Specified = true;  }
  bool __fastcall Region_Specified(int Index)
  {  return FRegion_Specified;  } 
__published:
  __property UnicodeString       City = { index=(IS_OPTN | IS_NLBL), read=FCity, write=SetCity, stored = City_Specified };
  __property UnicodeString    Country = { index=(IS_OPTN | IS_NLBL), read=FCountry, write=SetCountry, stored = Country_Specified };
  __property UnicodeString     Region = { index=(IS_OPTN | IS_NLBL), read=FRegion, write=SetRegion, stored = Region_Specified };
};




// ************************************************************************ //
// XML       : BirthPlaceDto, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class BirthPlaceDto : public BirthPlaceDto2 {
private:
__published:
};


typedef DynamicArray<DocumentDto2*> ArrayOfDocumentDto; /* "http://schemas.datacontract.org/2004/07/EMKService.Data.Dto"[GblCplx] */
typedef DynamicArray<ContactDto2*> ArrayOfContactDto; /* "http://schemas.datacontract.org/2004/07/EMKService.Data.Dto"[GblCplx] */


// ************************************************************************ //
// XML       : ContactPersonDto, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class ContactPersonDto2 : public TRemotable {
private:
  ArrayOfContactDto FContactList;
  bool            FContactList_Specified;
  UnicodeString   FFamilyName;
  bool            FFamilyName_Specified;
  UnicodeString   FGivenName;
  bool            FGivenName_Specified;
  UnicodeString   FIdPersonMis;
  bool            FIdPersonMis_Specified;
  unsigned char   FIdRelationType;
  bool            FIdRelationType_Specified;
  UnicodeString   FMiddleName;
  bool            FMiddleName_Specified;
  void __fastcall SetContactList(int Index, ArrayOfContactDto _prop_val)
  {  FContactList = _prop_val; FContactList_Specified = true;  }
  bool __fastcall ContactList_Specified(int Index)
  {  return FContactList_Specified;  } 
  void __fastcall SetFamilyName(int Index, UnicodeString _prop_val)
  {  FFamilyName = _prop_val; FFamilyName_Specified = true;  }
  bool __fastcall FamilyName_Specified(int Index)
  {  return FFamilyName_Specified;  } 
  void __fastcall SetGivenName(int Index, UnicodeString _prop_val)
  {  FGivenName = _prop_val; FGivenName_Specified = true;  }
  bool __fastcall GivenName_Specified(int Index)
  {  return FGivenName_Specified;  } 
  void __fastcall SetIdPersonMis(int Index, UnicodeString _prop_val)
  {  FIdPersonMis = _prop_val; FIdPersonMis_Specified = true;  }
  bool __fastcall IdPersonMis_Specified(int Index)
  {  return FIdPersonMis_Specified;  } 
  void __fastcall SetIdRelationType(int Index, unsigned char _prop_val)
  {  FIdRelationType = _prop_val; FIdRelationType_Specified = true;  }
  bool __fastcall IdRelationType_Specified(int Index)
  {  return FIdRelationType_Specified;  } 
  void __fastcall SetMiddleName(int Index, UnicodeString _prop_val)
  {  FMiddleName = _prop_val; FMiddleName_Specified = true;  }
  bool __fastcall MiddleName_Specified(int Index)
  {  return FMiddleName_Specified;  } 

public:
  __fastcall ~ContactPersonDto2();
__published:
  __property ArrayOfContactDto ContactList = { index=(IS_OPTN | IS_NLBL), read=FContactList, write=SetContactList, stored = ContactList_Specified };
  __property UnicodeString FamilyName = { index=(IS_OPTN | IS_NLBL), read=FFamilyName, write=SetFamilyName, stored = FamilyName_Specified };
  __property UnicodeString  GivenName = { index=(IS_OPTN | IS_NLBL), read=FGivenName, write=SetGivenName, stored = GivenName_Specified };
  __property UnicodeString IdPersonMis = { index=(IS_OPTN | IS_NLBL), read=FIdPersonMis, write=SetIdPersonMis, stored = IdPersonMis_Specified };
  __property unsigned char IdRelationType = { index=(IS_OPTN), read=FIdRelationType, write=SetIdRelationType, stored = IdRelationType_Specified };
  __property UnicodeString MiddleName = { index=(IS_OPTN | IS_NLBL), read=FMiddleName, write=SetMiddleName, stored = MiddleName_Specified };
};




// ************************************************************************ //
// XML       : ContactPersonDto, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class ContactPersonDto : public ContactPersonDto2 {
private:
__published:
};




// ************************************************************************ //
// XML       : PatientDto, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class PatientDto2 : public TRemotable {
private:
  ArrayOfAddressDto FAddresses;
  bool            FAddresses_Specified;
  TXSDateTime*    FBirthDate;
  bool            FBirthDate_Specified;
  BirthPlaceDto2* FBirthPlace;
  bool            FBirthPlace_Specified;
  ContactPersonDto2* FContactPerson;
  bool            FContactPerson_Specified;
  ArrayOfContactDto FContacts;
  bool            FContacts_Specified;
  TXSDateTime*    FDeathTime;
  bool            FDeathTime_Specified;
  ArrayOfDocumentDto FDocuments;
  bool            FDocuments_Specified;
  UnicodeString   FFamilyName;
  bool            FFamilyName_Specified;
  UnicodeString   FGivenName;
  bool            FGivenName_Specified;
  unsigned char   FIdBloodType;
  bool            FIdBloodType_Specified;
  UnicodeString   FIdGlobal;
  bool            FIdGlobal_Specified;
  unsigned char   FIdLivingAreaType;
  bool            FIdLivingAreaType_Specified;
  UnicodeString   FIdPatientMIS;
  bool            FIdPatientMIS_Specified;
  bool            FIsVip;
  bool            FIsVip_Specified;
  JobDto2*        FJob;
  bool            FJob_Specified;
  UnicodeString   FMiddleName;
  bool            FMiddleName_Specified;
  PrivilegeDto2*  FPrivilege;
  bool            FPrivilege_Specified;
  unsigned char   FSex;
  bool            FSex_Specified;
  unsigned char   FSocialGroup;
  bool            FSocialGroup_Specified;
  UnicodeString   FSocialStatus;
  bool            FSocialStatus_Specified;
  void __fastcall SetAddresses(int Index, ArrayOfAddressDto _prop_val)
  {  FAddresses = _prop_val; FAddresses_Specified = true;  }
  bool __fastcall Addresses_Specified(int Index)
  {  return FAddresses_Specified;  } 
  void __fastcall SetBirthDate(int Index, TXSDateTime* _prop_val)
  {  FBirthDate = _prop_val; FBirthDate_Specified = true;  }
  bool __fastcall BirthDate_Specified(int Index)
  {  return FBirthDate_Specified;  } 
  void __fastcall SetBirthPlace(int Index, BirthPlaceDto2* _prop_val)
  {  FBirthPlace = _prop_val; FBirthPlace_Specified = true;  }
  bool __fastcall BirthPlace_Specified(int Index)
  {  return FBirthPlace_Specified;  } 
  void __fastcall SetContactPerson(int Index, ContactPersonDto2* _prop_val)
  {  FContactPerson = _prop_val; FContactPerson_Specified = true;  }
  bool __fastcall ContactPerson_Specified(int Index)
  {  return FContactPerson_Specified;  } 
  void __fastcall SetContacts(int Index, ArrayOfContactDto _prop_val)
  {  FContacts = _prop_val; FContacts_Specified = true;  }
  bool __fastcall Contacts_Specified(int Index)
  {  return FContacts_Specified;  } 
  void __fastcall SetDeathTime(int Index, TXSDateTime* _prop_val)
  {  FDeathTime = _prop_val; FDeathTime_Specified = true;  }
  bool __fastcall DeathTime_Specified(int Index)
  {  return FDeathTime_Specified;  } 
  void __fastcall SetDocuments(int Index, ArrayOfDocumentDto _prop_val)
  {  FDocuments = _prop_val; FDocuments_Specified = true;  }
  bool __fastcall Documents_Specified(int Index)
  {  return FDocuments_Specified;  } 
  void __fastcall SetFamilyName(int Index, UnicodeString _prop_val)
  {  FFamilyName = _prop_val; FFamilyName_Specified = true;  }
  bool __fastcall FamilyName_Specified(int Index)
  {  return FFamilyName_Specified;  } 
  void __fastcall SetGivenName(int Index, UnicodeString _prop_val)
  {  FGivenName = _prop_val; FGivenName_Specified = true;  }
  bool __fastcall GivenName_Specified(int Index)
  {  return FGivenName_Specified;  } 
  void __fastcall SetIdBloodType(int Index, unsigned char _prop_val)
  {  FIdBloodType = _prop_val; FIdBloodType_Specified = true;  }
  bool __fastcall IdBloodType_Specified(int Index)
  {  return FIdBloodType_Specified;  } 
  void __fastcall SetIdGlobal(int Index, UnicodeString _prop_val)
  {  FIdGlobal = _prop_val; FIdGlobal_Specified = true;  }
  bool __fastcall IdGlobal_Specified(int Index)
  {  return FIdGlobal_Specified;  } 
  void __fastcall SetIdLivingAreaType(int Index, unsigned char _prop_val)
  {  FIdLivingAreaType = _prop_val; FIdLivingAreaType_Specified = true;  }
  bool __fastcall IdLivingAreaType_Specified(int Index)
  {  return FIdLivingAreaType_Specified;  } 
  void __fastcall SetIdPatientMIS(int Index, UnicodeString _prop_val)
  {  FIdPatientMIS = _prop_val; FIdPatientMIS_Specified = true;  }
  bool __fastcall IdPatientMIS_Specified(int Index)
  {  return FIdPatientMIS_Specified;  } 
  void __fastcall SetIsVip(int Index, bool _prop_val)
  {  FIsVip = _prop_val; FIsVip_Specified = true;  }
  bool __fastcall IsVip_Specified(int Index)
  {  return FIsVip_Specified;  } 
  void __fastcall SetJob(int Index, JobDto2* _prop_val)
  {  FJob = _prop_val; FJob_Specified = true;  }
  bool __fastcall Job_Specified(int Index)
  {  return FJob_Specified;  } 
  void __fastcall SetMiddleName(int Index, UnicodeString _prop_val)
  {  FMiddleName = _prop_val; FMiddleName_Specified = true;  }
  bool __fastcall MiddleName_Specified(int Index)
  {  return FMiddleName_Specified;  } 
  void __fastcall SetPrivilege(int Index, PrivilegeDto2* _prop_val)
  {  FPrivilege = _prop_val; FPrivilege_Specified = true;  }
  bool __fastcall Privilege_Specified(int Index)
  {  return FPrivilege_Specified;  } 
  void __fastcall SetSex(int Index, unsigned char _prop_val)
  {  FSex = _prop_val; FSex_Specified = true;  }
  bool __fastcall Sex_Specified(int Index)
  {  return FSex_Specified;  } 
  void __fastcall SetSocialGroup(int Index, unsigned char _prop_val)
  {  FSocialGroup = _prop_val; FSocialGroup_Specified = true;  }
  bool __fastcall SocialGroup_Specified(int Index)
  {  return FSocialGroup_Specified;  } 
  void __fastcall SetSocialStatus(int Index, UnicodeString _prop_val)
  {  FSocialStatus = _prop_val; FSocialStatus_Specified = true;  }
  bool __fastcall SocialStatus_Specified(int Index)
  {  return FSocialStatus_Specified;  } 

public:
  __fastcall ~PatientDto2();
__published:
  __property ArrayOfAddressDto  Addresses = { index=(IS_OPTN | IS_NLBL), read=FAddresses, write=SetAddresses, stored = Addresses_Specified };
  __property TXSDateTime*  BirthDate = { index=(IS_OPTN), read=FBirthDate, write=SetBirthDate, stored = BirthDate_Specified };
  __property BirthPlaceDto2* BirthPlace = { index=(IS_OPTN | IS_NLBL), read=FBirthPlace, write=SetBirthPlace, stored = BirthPlace_Specified };
  __property ContactPersonDto2* ContactPerson = { index=(IS_OPTN | IS_NLBL), read=FContactPerson, write=SetContactPerson, stored = ContactPerson_Specified };
  __property ArrayOfContactDto   Contacts = { index=(IS_OPTN | IS_NLBL), read=FContacts, write=SetContacts, stored = Contacts_Specified };
  __property TXSDateTime*  DeathTime = { index=(IS_OPTN | IS_NLBL), read=FDeathTime, write=SetDeathTime, stored = DeathTime_Specified };
  __property ArrayOfDocumentDto  Documents = { index=(IS_OPTN | IS_NLBL), read=FDocuments, write=SetDocuments, stored = Documents_Specified };
  __property UnicodeString FamilyName = { index=(IS_OPTN | IS_NLBL), read=FFamilyName, write=SetFamilyName, stored = FamilyName_Specified };
  __property UnicodeString  GivenName = { index=(IS_OPTN | IS_NLBL), read=FGivenName, write=SetGivenName, stored = GivenName_Specified };
  __property unsigned char IdBloodType = { index=(IS_OPTN | IS_NLBL), read=FIdBloodType, write=SetIdBloodType, stored = IdBloodType_Specified };
  __property UnicodeString   IdGlobal = { index=(IS_OPTN | IS_NLBL), read=FIdGlobal, write=SetIdGlobal, stored = IdGlobal_Specified };
  __property unsigned char IdLivingAreaType = { index=(IS_OPTN | IS_NLBL), read=FIdLivingAreaType, write=SetIdLivingAreaType, stored = IdLivingAreaType_Specified };
  __property UnicodeString IdPatientMIS = { index=(IS_OPTN | IS_NLBL), read=FIdPatientMIS, write=SetIdPatientMIS, stored = IdPatientMIS_Specified };
  __property bool            IsVip = { index=(IS_OPTN), read=FIsVip, write=SetIsVip, stored = IsVip_Specified };
  __property JobDto2*          Job = { index=(IS_OPTN | IS_NLBL), read=FJob, write=SetJob, stored = Job_Specified };
  __property UnicodeString MiddleName = { index=(IS_OPTN | IS_NLBL), read=FMiddleName, write=SetMiddleName, stored = MiddleName_Specified };
  __property PrivilegeDto2*  Privilege = { index=(IS_OPTN | IS_NLBL), read=FPrivilege, write=SetPrivilege, stored = Privilege_Specified };
  __property unsigned char        Sex = { index=(IS_OPTN), read=FSex, write=SetSex, stored = Sex_Specified };
  __property unsigned char SocialGroup = { index=(IS_OPTN | IS_NLBL), read=FSocialGroup, write=SetSocialGroup, stored = SocialGroup_Specified };
  __property UnicodeString SocialStatus = { index=(IS_OPTN | IS_NLBL), read=FSocialStatus, write=SetSocialStatus, stored = SocialStatus_Specified };
};




// ************************************************************************ //
// XML       : PatientDto, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/EMKService.Data.Dto
// ************************************************************************ //
class PatientDto : public PatientDto2 {
private:
__published:
};



// ************************************************************************ //
// Namespace : http://tempuri.org/
// soapAction: |http://tempuri.org/IServiceSupport/GetVersion|http://tempuri.org/IPixService/AddPatient|http://tempuri.org/IPixService/UpdatePatient|http://tempuri.org/IPixService/GetPatient|http://tempuri.org/IPixService/GetPatientByGlobalId
// transport : http://schemas.xmlsoap.org/soap/http
// style     : document
// use       : literal
// binding   : BasicHttpBinding_IPixService
// service   : PixService
// port      : BasicHttpBinding_IPixService
// URL       : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc
// ************************************************************************ //
__interface INTERFACE_UUID("{F8FFB595-677D-82E1-338E-0CA2B46792C4}") IPixService : public IInvokable
{
public:
  virtual VersionInfo2*   GetVersion() = 0; 
  virtual void            AddPatient(const UnicodeString guid, const UnicodeString idLPU, const PatientDto2* patient) = 0; 
  virtual void            UpdatePatient(const UnicodeString guid, const UnicodeString idLPU, const PatientDto2* patient) = 0; 
  virtual ArrayOfPatientDto GetPatient(const UnicodeString guid, const UnicodeString idLPU, const PatientDto2* patient, const SourceType idSource) = 0; 
  virtual PatientDto2*    GetPatientByGlobalId(const UnicodeString guid, const UnicodeString patientId, const UnicodeString idLpu) = 0; 
};
typedef DelphiInterface<IPixService> _di_IPixService;

_di_IPixService GetIPixService(bool useWSDL=false, System::String addr= System::String(), Soaphttpclient::THTTPRIO* HTTPRIO=0);
_di_IPixService GetIPixService(System::String WsdlAddr= System::String(), Soaphttpclient::THTTPRIO* HTTPRIO=0);


};     // NS_PixService

#if !defined(NO_IMPLICIT_NAMESPACE_USE)
using  namespace NS_PixService;
#endif



#endif // PixServiceH

