//---------------------------------------------------------------------------

#ifndef netrika_testH
#define netrika_testH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "netrikaDMUnit.h"
//---------------------------------------------------------------------------
class TForm39 : public TForm
{
__published:	// IDE-managed Components
 TButton *PatBtn;
 TButton *PrivBtn;
 TButton *TestBtn;
 TButton *CancelBtn;
 TMemo *Memo1;
 void __fastcall PatBtnClick(TObject *Sender);
 void __fastcall PrivBtnClick(TObject *Sender);
 void __fastcall TestBtnClick(TObject *Sender);
 void __fastcall CancelBtnClick(TObject *Sender);
private:	// User declarations
  void __fastcall Log(const UnicodeString AService, TSOAPLogType AType, const UnicodeString AMessage);
public:		// User declarations
 __fastcall TForm39(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm39 *Form39;
//---------------------------------------------------------------------------
#endif

