// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "netrika_test.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm39 * Form39;
// ---------------------------------------------------------------------------
const UnicodeString guidic = "4ddebe21-db9e-4716-add0-5970db6269a8";
// 1) IdLpu f9fc300a-740b-484b-a83f-ac4ed169611d - �����-������������� ��������������� ��������� ���������� ��������������� "��������� ����������� �88"
// 2) IdLpu 7951cd24-16bf-4058-b449-e59d6df6bb87 - ������� ��������������� ��������� �16 ��� ���� "��������� ����������� �114"
// 3) IdLpu 34d19ca3-6d4c-4120-a6a0-cd1264c70006  - �����-������������� ��������������� ��������� ���������� ��������������� "��������� �������� ������ ������������������ ���������"
const UnicodeString idLPU = "20dfadd0-c709-43b0-a130-5a16301b0217";
const UnicodeString idPat = "111-222-333-444-555-666";
__fastcall TForm39::TForm39(TComponent * Owner) : TForm(Owner)
 {
  // const char * PatWSDL = "http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?wsdl";
  const char * PatWSDL = "D:\\work\\ic\\Server\\src\\API\\MIS\\netrika\\wsdl\\PixService.wsdl";
  // const char * IEMKWSDL = "http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?wsdl";
  const char * IEMKWSDL = "D:\\work\\ic\\Server\\src\\API\\MIS\\netrika\\wsdl\\EMKService.wsdl";
  NetrikaDM->CreateServices(PatWSDL, IEMKWSDL);
  NetrikaDM->Logger = Log;
  Memo1->Lines->Clear();
 }
// ---------------------------------------------------------------------------
void __fastcall TForm39::Log(const UnicodeString AService, TSOAPLogType AType, const UnicodeString AMessage)
 {
  // Memo1->Lines->Add(AMessage);
  Memo1->Lines->Text = Memo1->Lines->Text + "\n" + AMessage;
 }
// ---------------------------------------------------------------------------
void __fastcall TForm39::PatBtnClick(TObject * Sender)
 {
  TTagNode * tmp = new TTagNode;
  tmp->Name        = "patient";
  tmp->AV["guid"]  = guidic;
  tmp->AV["idLPU"] = idLPU;
  tmp->AV["idPat"] = idPat;
  // <a:IdPatientMIS>//idPat
  tmp->AV["Family"]    = "���������";
  tmp->AV["Name"]      = "�����";
  tmp->AV["SurName"]   = "���������";
  tmp->AV["BirthDate"] = "08.08.1988";
  tmp->AV["Sex"]       = "2"; /* 3	�� ������������ 2	������� 1	������� */
  tmp->AV["DocN"]      = "28104272849";
  // <patient
  // <a:IdPatientMIS>//idPat
  // <a:FamilyName>Family
  // <a:GivenName>Name
  // <a:MiddleName SurName
  // <a:BirthDate>BirthDate
  // <a:Sex>Sex
  // <a:Documents>
  // <a:DocumentDto>
  // <a:DocN>DocN
  // <a:IdDocumentType>223</a:IdDocumentType>
  NetrikaDM->SendPatient(tmp);
  delete tmp;
 }
// ---------------------------------------------------------------------------
void __fastcall TForm39::PrivBtnClick(TObject * Sender)
 {
  TTagNode * tmp = new TTagNode;
  tmp->AV["guid"]    = guidic;
  tmp->AV["idLPU"]   = idLPU;
  tmp->AV["idPat"]   = idPat;
  tmp->AV["VacDate"] = "21.10.2014";
  tmp->AV["VacCode"] = "224";
  tmp->AV["VacType"] = "V3";
  tmp->AddChild("i")->AV["Inf"] = "3";
  tmp->AddChild("i")->AV["Inf"] = "4";
  tmp->AddChild("i")->AV["Inf"] = "5";
  NetrikaDM->SendPriv(tmp);
  delete tmp;
 }
// ---------------------------------------------------------------------------
void __fastcall TForm39::TestBtnClick(TObject * Sender)
 {
  TTagNode * tmp = new TTagNode;
  tmp->AV["guid"]     = guidic;
  tmp->AV["idLPU"]    = idLPU;
  tmp->AV["idPat"]    = idPat;
  tmp->AV["TestDate"] = "21.10.2014";
  tmp->AV["TestCode"] = "1";
  tmp->AV["TestType"] = "3";
  tmp->AddChild("i")->AV["Inf"] = "1";
  NetrikaDM->SendTest(tmp);
  delete tmp;
 }
// ---------------------------------------------------------------------------
void __fastcall TForm39::CancelBtnClick(TObject * Sender)
 {
  TTagNode * tmp = new TTagNode;
  tmp->AV["guid"]      = guidic;
  tmp->AV["idLPU"]     = idLPU;
  tmp->AV["idPat"]     = idPat;
  tmp->AV["DateStart"] = "21.10.2014";
  tmp->AV["DateEnd"]   = "21.11.2014";
  tmp->AV["Type"]      = "1"; // 1	��������� 2	����������
  tmp->AV["Reason"]    = "3";
  // 5	����������� ��� �����������
  // 9	������
  // 1	�����������
  // 2	��������
  // 8	������� � ����������
  // 3	����� �� ��������/�����
  // 6	����� �� ������������ � �����������
  // 7	������������
  // 4	������������ �������������
  tmp->AV["MKB10"] = "K51.8";
  tmp->AddChild("i")->AV["Inf"] = "1";
  tmp->AddChild("i")->AV["Inf"] = "2";
  tmp->AddChild("i")->AV["Inf"] = "3";
  tmp->AddChild("i")->AV["Inf"] = "4";
  tmp->AddChild("i")->AV["Inf"] = "5";
  tmp->AddChild("i")->AV["Inf"] = "6";
  tmp->AddChild("i")->AV["Inf"] = "7";
  NetrikaDM->SendCancel(tmp);
  delete tmp;
 }
// ---------------------------------------------------------------------------
