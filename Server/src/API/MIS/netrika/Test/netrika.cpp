//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
USEFORM("netrika_test.cpp", Form39);
USEFORM("..\netrikaDMUnit.cpp", NetrikaDM); /* TDataModule: File Type */
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
 try
 {
   Application->Initialize();
   Application->MainFormOnTaskBar = true;
   Application->CreateForm(__classid(TNetrikaDM), &NetrikaDM);
   Application->CreateForm(__classid(TForm39), &Form39);
   Application->Run();
 }
 catch (Exception &exception)
 {
   Application->ShowException(&exception);
 }
 catch (...)
 {
   try
   {
    throw Exception("");
   }
   catch (Exception &exception)
   {
    Application->ShowException(&exception);
   }
 }
 return 0;
}
//---------------------------------------------------------------------------

