object Form39: TForm39
  Left = 0
  Top = 0
  Caption = 'Form39'
  ClientHeight = 627
  ClientWidth = 723
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PatBtn: TButton
    Left = 40
    Top = 40
    Width = 75
    Height = 25
    Caption = #1055#1072#1094#1080#1077#1085#1090
    TabOrder = 0
    OnClick = PatBtnClick
  end
  object PrivBtn: TButton
    Left = 240
    Top = 40
    Width = 75
    Height = 25
    Caption = #1055#1088#1080#1074
    TabOrder = 1
    OnClick = PrivBtnClick
  end
  object TestBtn: TButton
    Left = 321
    Top = 40
    Width = 75
    Height = 25
    Caption = #1055#1088#1086#1073#1072
    TabOrder = 2
    OnClick = TestBtnClick
  end
  object CancelBtn: TButton
    Left = 402
    Top = 40
    Width = 75
    Height = 25
    Caption = #1054#1090#1074#1086#1076
    TabOrder = 3
    OnClick = CancelBtnClick
  end
  object Memo1: TMemo
    Left = 0
    Top = 160
    Width = 723
    Height = 467
    Align = alBottom
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'Memo1')
    ScrollBars = ssBoth
    TabOrder = 4
  end
end
