//---------------------------------------------------------------------------

#ifndef dsIEMKSupportH
#define dsIEMKSupportH

#include <System.hpp>
#include <Classes.hpp>
#include <list>


#include "netrikaDMUnit.h"

//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                              TdsIEMKSupport                                 ##
//##                                                                       ##
//###########################################################################
              
//---------------------------------------------------------------------------
class PACKAGE TdsIEMKSupport : public TObject
{
typedef void __fastcall (__closure *TdsSyncMethod)(void);
typedef list<UnicodeString> TdsQuery;
private:

  class TBackgroundIEMKSupport;
  TBackgroundIEMKSupport * FBkgSyncModule;
  void __fastcall SyncCall(TdsSyncMethod AMethod);
  TdsQuery       FCallQuery;


  TNotifyEvent    FOnIncProgress;      // �������, ��� ��������� �������� ���������� �������
                                         // ��������� ������� ProgressBar'�
  UnicodeString FLogMsg, FLogFuncName;
  int FLogALvl;
  TTime         FLogExecTime;

  TOnLogMsgErrEvent   FOnLogMessage;
  TOnLogMsgErrEvent   FOnLogError;
  TOnLogSQLEvent      FOnLogSQL;

  TOnLogBegEndEvent   FOnLogBegin;
  TOnLogBegEndEvent   FOnLogEnd;


  inline void __fastcall CallOnIncProgress();
  void __fastcall SyncCallOnIncProgress();

  void __fastcall PopFront();
  void __fastcall SyncCallPopFront();
  System::UnicodeString __fastcall SendData(UnicodeString AId, UnicodeString AData, int ASendStatus);

protected:

  UnicodeString   FStage;
  int             FCurrProgress;
  int             FMaxProgress;        // Max �������� ��� ProgressBar'�
  bool            FTerminated;
  bool            FInSync;
  bool            FCanSend;
  TNetrikaDM      *FDM;
  TAppOptions     *FOpt;
  UnicodeString   idLPU;
  UnicodeString   FSendId;
  UnicodeString   FCardData;
  int             FSendStatus;


  void __fastcall SyncCallLogMsg();
  void __fastcall SyncCallLogErr();
  void __fastcall SyncCallLogSQL();
  void __fastcall SyncCallLogBegin();
  void __fastcall SyncCallLogEnd();

  void __fastcall CallLogMsg(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogErr(UnicodeString AMessage, UnicodeString AFuncName = "", int ALvl = 1);
  void __fastcall CallLogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl = 1);
  void __fastcall CallLogBegin(UnicodeString AFuncName);
  void __fastcall CallLogEnd(UnicodeString AFuncName);

public:
    System::UnicodeString Proc;
    System::UnicodeString __fastcall TdsIEMKSupport::SendREGIZData(UnicodeString AId,  TJSONObject * AValue, int AStatus);
    System::UnicodeString __fastcall StopSync();
    __property bool Terminated = {read = FTerminated};
    __property bool InSync = {read = FInSync};

    int __fastcall GetMaxProgress();
    int __fastcall GetCurProgress();
    UnicodeString __fastcall GetCurStage() const;

    __fastcall TdsIEMKSupport(TdsMKB * AMKB, TAppOptions *AOpt, UnicodeString AUser = "", UnicodeString APass = "");
    virtual __fastcall ~TdsIEMKSupport();

    __property int          MaxProgress   = { read = FMaxProgress };
    __property TNotifyEvent OnIncProgress = { read = FOnIncProgress, write = FOnIncProgress };

    __property TOnLogMsgErrEvent OnLogMessage = {read = FOnLogMessage, write = FOnLogMessage};
    __property TOnLogMsgErrEvent OnLogError   = {read = FOnLogError, write = FOnLogError};
    __property TOnLogSQLEvent    OnLogSQL     = {read = FOnLogSQL, write = FOnLogSQL};

    __property TOnLogBegEndEvent OnLogBegin = {read = FOnLogBegin, write = FOnLogBegin};
    __property TOnLogBegEndEvent OnLogEnd   = {read = FOnLogEnd, write = FOnLogEnd};

    __property bool        CanSend   = { read = FCanSend};


  virtual System::UnicodeString __fastcall Sync();
};
  __int64 __fastcall JSONObjToInt(TJSONObject * AValue, UnicodeString AId, int ASendStatus);
  UnicodeString __fastcall JSONObjToString(TJSONObject * AValue, UnicodeString AId, int ASendStatus);

//---------------------------------------------------------------------------
#endif
