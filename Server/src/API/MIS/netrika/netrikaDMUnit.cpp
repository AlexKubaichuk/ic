// ---------------------------------------------------------------------------
#pragma hdrstop
#include "netrikaDMUnit.h"
#ifndef NETRIKATEST
#include "kabQueryUtils.h"
#endif
// #include <Dialogs.hpp>
// #include <System.Win.ComObj.hpp>
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "Vcl.Controls.TControl"
#pragma resource "*.dfm"
TNetrikaDM * NetrikaDM;
// ---------------------------------------------------------------------------
#ifndef NETRIKATEST
__fastcall TNetrikaDM::TNetrikaDM(TComponent * Owner, TdsMKB * AMKB) : TDataModule(Owner)
 {
  FMKB = AMKB;
  mpi  = NULL;
  iemk = NULL;
 }
#else
__fastcall TNetrikaDM::TNetrikaDM(TComponent * Owner) : TDataModule(Owner)
 {
  mpi  = NULL;
  iemk = NULL;
 }
#endif
// ---------------------------------------------------------------------------
#ifndef NETRIKATEST
void __fastcall TNetrikaDM::CreateServices(System::String PatWSDL, System::String IEMKWSDL, TAppOptions * AOpt)
 {
  TDSSession * FSes = TDSSessionManager::Instance->GetThreadSession();
  if (FSes)
   {
    ConnectDB(IcConnection, AOpt, FSes->UserRoles->Values["ICDB"], __FUNC__);
    mpi  = GetIPixService(PatWSDL, HTTPPat);
    iemk = GetIEmkService(IEMKWSDL, HTTPPriv);
    TFDQuery * FQ = CreateTempQuery();
    try
     {
      // ***** ������ ��������
      if (quExec(FQ, false, "Select CODE as ps1, R3319 as ps2 From CLASS_003A", "������ ��������"))
       {
        while (!FQ->Eof)
         {
          FInfCodes[FQ->FieldByName("ps1")->AsInteger] = FQ->FieldByName("ps2")->AsString;
          FQ->Next();
         }
       }
      // ***** ������ ������
      if (quExec(FQ, false, "Select CODE as ps1, R331A as ps2 From CLASS_0035", "������ ������"))
       {
        while (!FQ->Eof)
         {
          FVacCodes[FQ->FieldByName("ps1")->AsInteger] = FQ->FieldByName("ps2")->AsString;
          FQ->Next();
         }
       }
      // ***** ������ ����
      if (quExec(FQ, false, "Select CODE as ps1, R331B as ps2 From CLASS_002A", "������ ����"))
       {
        while (!FQ->Eof)
         {
          FTestCodes[FQ->FieldByName("ps1")->AsInteger] = FQ->FieldByName("ps2")->AsString;
          FQ->Next();
         }
       }
     }
    __finally
     {
      if (FQ->Transaction->Active)
       FQ->Transaction->Commit();
      DeleteTempQuery(FQ);
     }
   }
 }
#else
void __fastcall TNetrikaDM::CreateServices(System::String PatWSDL, System::String IEMKWSDL)
 {
  mpi  = GetIPixService(PatWSDL, HTTPPat);
  iemk = GetIEmkService(IEMKWSDL, HTTPPriv);
 }
#endif
// ---------------------------------------------------------------------------
void __fastcall TNetrikaDM::HTTPPatBeforeExecute(const UnicodeString MethodName, TStream * SOAPRequest)
 {
  TTagNode * tmpNode = new TTagNode;
  try
   {
    SOAPRequest->Seek(0, 0);
    tmpNode->LoadFromStream(SOAPRequest);
#ifndef NETRIKATEST
    LogMessage("pat: " + tmpNode->AsXML, __FUNC__, 5);
#else
    if (FOnLog)
     FOnLog("pat", TSOAPLogType::ltRequest, tmpNode->AsXML);
#endif
   }
  __finally
   {
    delete tmpNode;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TNetrikaDM::HTTPPatAfterExecute(const UnicodeString MethodName, TStream * SOAPResponse)
 {
  TTagNode * tmpNode = new TTagNode;
  try
   {
    SOAPResponse->Seek(0, 0);
    tmpNode->LoadFromStream(SOAPResponse);
#ifndef NETRIKATEST
    LogMessage("pat: " + tmpNode->AsXML, __FUNC__, 5);
#else
    if (FOnLog)
     FOnLog("pat", TSOAPLogType::ltResponse, tmpNode->AsXML);
#endif
   }
  __finally
   {
    delete tmpNode;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TNetrikaDM::HTTPPrivBeforeExecute(const UnicodeString MethodName, TStream * SOAPRequest)
 {
  TTagNode * tmpNode = new TTagNode;
  try
   {
    // TEncoding * FReadEncoding = TEncoding::UTF8;
    // TByteDynArray FReadBuf = TByteDynArray();
    // // SOAPRequest->Seek(0, soFromBeginning);
    // SOAPRequest->Seek(0, soFromEnd);
    // int FReadSize = SOAPRequest->Size;
    // FReadBuf.Length = FReadSize;
    // SOAPRequest->Seek(0, soFromBeginning);
    // SOAPRequest->ReadBuffer(FReadBuf, FReadSize);
    // LogMessage("be 1 >>> : " + FReadEncoding->GetString(FReadBuf), __FUNC__, 5);
    SOAPRequest->Seek(0, soFromBeginning);
    tmpNode->LoadFromStream(SOAPRequest);
    // LogMessage("be 2 >>> : " + tmpNode->AsXML, __FUNC__, 5);
    TTagNode * infNode = tmpNode->GetChildByName("Inf", true);
    if (!infNode)
     infNode = tmpNode->GetChildByName("ExemptionInf", true);
    if (infNode)
     {
      infNode->AV["xmlns:b"] = "http://schemas.microsoft.com/2003/10/Serialization/Arrays";
      infNode                = infNode->GetFirstChild();
      while (infNode)
       {
        infNode->Name = "b:int";
        infNode       = infNode->GetNext();
       }
      TEncoding * FEncoding = TEncoding::UTF8;
      TByteDynArray FBuf = FEncoding->GetBytes(tmpNode->AsXML);
      SOAPRequest->Seek(0, 0);
      SOAPRequest->WriteBuffer(FBuf, FBuf.Length);
      // LogMessage("be 3 >>> : " + FEncoding->GetString(FBuf), __FUNC__, 5);
     }
#ifndef NETRIKATEST
    // SOAPRequest->Seek(0, 0);
    // tmpNode->LoadFromStream(SOAPRequest);
    LogMessage("iemk: " + tmpNode->AsXML, __FUNC__, 5);
#else
    if (FOnLog)
     {
      // SOAPRequest->Seek(0, 0);
      // tmpNode->LoadFromStream(SOAPRequest);
      FOnLog("iemk", TSOAPLogType::ltRequest, tmpNode->AsXML);
     }
#endif
   }
  __finally
   {
    delete tmpNode;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TNetrikaDM::HTTPPrivAfterExecute(const UnicodeString MethodName, TStream * SOAPResponse)
 {
  TTagNode * tmpNode = new TTagNode;
  try
   {
    SOAPResponse->Seek(0, 0);
    tmpNode->LoadFromStream(SOAPResponse);
#ifndef NETRIKATEST
    LogMessage("iemk: " + tmpNode->AsXML, __FUNC__, 5);
#else
    if (FOnLog)
     FOnLog("iemk", TSOAPLogType::ltResponse, tmpNode->AsXML);
#endif
   }
  __finally
   {
    delete tmpNode;
   }
 }
// ---------------------------------------------------------------------------
String __fastcall TNetrikaDM::GenerateGUID()
 {
  TGUID guid;
  ::CreateGUID(guid);
  UnicodeString GG = System::Sysutils::GUIDToString(guid);
  GG.Delete(1, 1);
  GG.Delete(GG.Length(), 1);
  return GG;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNetrikaDM::SendPatient(TTagNode * AData)
 {
  UnicodeString RC = "";
  UnicodeString guid = AData->AV["guid"];
  UnicodeString idLPU = AData->AV["idLPU"];
  PatientDto2 * patient = new PatientDto2;
  try
   {
    try
     {
      patient->IdPatientMIS          = AData->AV["idPat"];
      patient->BirthDate             = new TXSDateTime;
      patient->BirthDate->AsDateTime = TDateTime(AData->AV["BirthDate"]);
      patient->FamilyName            = AData->AV["Family"];
      patient->GivenName             = AData->AV["Name"];
      patient->MiddleName            = AData->AV["SurName"];
      patient->Sex                   = (unsigned char)AData->AV["Sex"][1];
      if (AData->AV["DocN"].Trim().Length())
       {
        patient->Documents                    = NS_PixService::ArrayOfDocumentDto();
        patient->Documents.Length             = 1;
        patient->Documents[0]                 = new NS_PixService::DocumentDto2;
        patient->Documents[0]->DocN           = AData->AV["DocN"];
        patient->Documents[0]->IdDocumentType = "223";
        patient->Documents[0]->ProviderName   = "���";
       }
      mpi->AddPatient(guid, idLPU, patient);
     }
    catch (Exception & E)
     {
#ifndef NETRIKATEST
      LogError(E.Message, __FUNC__, 5);
#else
      if (FOnLog)
       FOnLog("error", TSOAPLogType::ltResponse, E.Message);
#endif
      RC = E.Message;
     }
   }
  __finally
   {
    // delete patient;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNetrikaDM::SendPriv(TTagNode * AData)
 {
  UnicodeString RC = "";
  UnicodeString guid = AData->AV["guid"];
  UnicodeString idLPU = AData->AV["idLPU"];
  UnicodeString idPat = AData->AV["idPat"];
  Vaccination2 * medRecord = new Vaccination2;
  try
   {
    try
     {
      // LogMessage("1 >>>" + AData->AsXML, __FUNC__, 5);
      medRecord->Lpu              = idLPU;
      medRecord->Date             = new TXSDateTime;
      medRecord->Date->AsDateTime = TDateTime(AData->AV["VacDate"]);
      medRecord->Code             = AData->AV["VacCode"].ToIntDef(0);
      // LogMessage("1.1 >>>" + AData->AsXML, __FUNC__, 5);
      medRecord->VacType = AData->AV["VacType"];
      // LogMessage("1.2 >>>" + AData->AsXML, __FUNC__, 5);
      medRecord->Inf = ArrayOfint();
      // medRecord->Inf.Length = AData->Count;
      medRecord->IdVaccinationMis = AData->AV["RecId"];
      medRecord->Status           = AData->AV["Status"].ToIntDef(1);
      // 1; // 1.2.643.2.69.1.1.1.172 1 ����� 2 ��������� 3 ��������
      TTagNode * itInf = AData->GetFirstChild();
      int i = 0;
      // LogMessage("2 >>>" + AData->AsXML, __FUNC__, 5);
      while (itInf)
       {
        if (itInf->AV["Inf"].ToIntDef(0) && (itInf->AV["Inf"].ToIntDef(0) != -1))
         i++ ;
        else
         LogError("error inf code -1", __FUNC__, 5);
        itInf = itInf->GetNext();
       }
      medRecord->Inf.Length = i;
      itInf = AData->GetFirstChild();
      i     = 0;
      while (itInf)
       {
        if (itInf->AV["Inf"].ToIntDef(0) && (itInf->AV["Inf"].ToIntDef(0) != -1))
         medRecord->Inf[i++] = itInf->AV["Inf"].ToIntDef(0);
        else
         LogError("error inf code -1", __FUNC__, 5);
        itInf = itInf->GetNext();
       }
      iemk->AddMedRecord(guid, idLPU, idPat, "", medRecord);
     }
    catch (Exception & E)
     {
#ifndef NETRIKATEST
      LogError(E.Message, __FUNC__, 5);
#else
      if (FOnLog)
       FOnLog("error", TSOAPLogType::ltResponse, E.Message);
#endif
      RC = E.Message;
     }
   }
  __finally
   {
    delete medRecord;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNetrikaDM::SendTest(TTagNode * AData)
 {
  UnicodeString RC = "";
  UnicodeString guid = AData->AV["guid"];
  UnicodeString idLPU = AData->AV["idLPU"];
  UnicodeString idPat = AData->AV["idPat"];
  Reaction2 * medRecord = new Reaction2;
  try
   {
    try
     {
      medRecord->Lpu              = idLPU;
      medRecord->Date             = new TXSDateTime;
      medRecord->Date->AsDateTime = TDateTime(AData->AV["TestDate"]);
      medRecord->ReactionType     = AData->AV["TestType"].ToIntDef(3);
      medRecord->Code             = AData->AV["TestCode"].ToIntDef(0);
      medRecord->Inf              = ArrayOfint();
      medRecord->Inf.Length       = AData->Count;
      medRecord->IdReactionMis    = AData->AV["RecId"];
      medRecord->Status           = AData->AV["Status"].ToIntDef(1);
      // 1; // 1.2.643.2.69.1.1.1.172 1 ����� 2 ��������� 3 ��������
      TTagNode * itInf = AData->GetFirstChild();
      int i = 0;
      // LogMessage(AData->AsXML, __FUNC__, 5);
      while (itInf)
       {
        if (itInf->AV["Inf"].ToIntDef(0) && (itInf->AV["Inf"].ToIntDef(0) != -1))
         medRecord->Inf[i++] = itInf->AV["Inf"].ToIntDef(0);
        else
         LogError("error inf code -1", __FUNC__, 5);
        itInf = itInf->GetNext();
       }
      iemk->AddMedRecord(guid, idLPU, idPat, "", medRecord);
     }
    catch (Exception & E)
     {
#ifndef NETRIKATEST
      LogError(E.Message, __FUNC__, 5);
#else
      if (FOnLog)
       FOnLog("error", TSOAPLogType::ltResponse, E.Message);
#endif
      RC = E.Message;
     }
   }
  __finally
   {
    delete medRecord;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNetrikaDM::SendCancel(TTagNode * AData)
 {
  UnicodeString RC = "";
  UnicodeString guid = AData->AV["guid"];
  UnicodeString idLPU = AData->AV["idLPU"];
  UnicodeString idPat = AData->AV["idPat"];
  // 1; // 1.2.643.2.69.1.1.1.172 1 ����� 2 ��������� 3 ��������
  MedicalExemption2 * medRecord = new MedicalExemption2;
  try
   {
    try
     {
      TDate FEndDate;
      medRecord->IdMedicalExemptionMis = AData->AV["RecId"];
      medRecord->Status                = AData->AV["Status"].ToIntDef(1);
      medRecord->DateStart             = new TXSDateTime;
      medRecord->DateStart->AsDateTime = TDateTime(AData->AV["DateStart"]);
      if (TryStrToDate(AData->AV["DateEnd"], FEndDate))
       {
        medRecord->DateEnd             = new TXSDateTime;
        medRecord->DateEnd->AsDateTime = TDateTime(AData->AV["DateEnd"]);
       }
      medRecord->ExemptionType = AData->AV["Type"].ToIntDef(3);
      medRecord->ExemptionReason  = AData->AV["Reason"].ToIntDef(3);
      medRecord->ExemptionDisease = AData->AV["MKB10"];
      medRecord->ExemptionInf     = ArrayOfint();
      TTagNode * itInf = AData->GetFirstChild();
      int i = 0;
      while (itInf)
       {
        if (itInf->AV["Inf"].ToIntDef(0) && (itInf->AV["Inf"].ToIntDef(0) != -1))
         i++ ;
        else
         LogError("error inf code -1", __FUNC__, 5);
        itInf = itInf->GetNext();
       }
      medRecord->ExemptionInf.Length = i;
      itInf = AData->GetFirstChild();
      i     = 0;
      while (itInf)
       {
        if (itInf->AV["Inf"].ToIntDef(0) && (itInf->AV["Inf"].ToIntDef(0) != -1))
         medRecord->ExemptionInf[i++] = itInf->AV["Inf"].ToIntDef(0);
        itInf = itInf->GetNext();
       }
      iemk->AddMedRecord(guid, idLPU, idPat, "", medRecord);
     }
    catch (Exception & E)
     {
#ifndef NETRIKATEST
      LogError(E.Message, __FUNC__, 5);
#else
      if (FOnLog)
       FOnLog("error", TSOAPLogType::ltResponse, E.Message);
#endif
      RC = E.Message;
     }
   }
  __finally
   {
    delete medRecord;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNetrikaDM::SendPlanData(TTagNode * AData)
 {
  UnicodeString RC = "";
  UnicodeString guid = AData->AV["guid"];
  UnicodeString idLPU = AData->AV["idLPU"];
  UnicodeString idPat = AData->AV["idPat"];
  ImmunizationPlan2 * medRecord = new ImmunizationPlan2;
  // if (true)
  // medRecord = new VaccinationType2;
  // else
  // medRecord = new ReactionType2;
  try
   {
    try
     {
      // LogMessage("1 >>>" + AData->AsXML, __FUNC__, 5);
      // __property ImmunizationType2* ImmunizationType = { index=(IS_OPTN | IS_NLBL), read=FImmunizationType, write=SetImmunizationType, stored = ImmunizationType_Specified };
      // __property ArrayOfint        Inf = { index=(IS_OPTN | IS_NLBL), read=FInf, write=SetInf, stored = Inf_Specified };
      // __property TXSDateTime*       Date = { index=(IS_OPTN), read=FDate, write=SetDate, stored = Date_Specified };
      // __property UnicodeString    VacType = { index=(IS_OPTN | IS_NLBL), read=FVacType, write=SetVacType, stored = VacType_Specified };
      // medRecord->Lpu              = idLPU;
      if (AData->CmpAV("PlanType", "vac"))
       medRecord->ImmunizationType = new VaccinationType2;
      else
       medRecord->ImmunizationType = new ReactionType2;
      medRecord->Date             = new TXSDateTime;
      medRecord->Date->AsDateTime = TDateTime(AData->AV["VacDate"]);
      medRecord->VacType          = AData->AV["VacType"];
      // LogMessage("1.2 >>>" + AData->AsXML, __FUNC__, 5);
      medRecord->Inf = ArrayOfint();
      // medRecord->Inf.Length = AData->Count;
      // medRecord->IdVaccinationMis = AData->AV["RecId"];
      // medRecord->Status           = AData->AV["Status"].ToIntDef(1);
      // 1; // 1.2.643.2.69.1.1.1.172 1 ����� 2 ��������� 3 ��������
      TTagNode * itInf = AData->GetFirstChild();
      int i = 0;
      // LogMessage("2 >>>" + AData->AsXML, __FUNC__, 5);
      while (itInf)
       {
        if (itInf->AV["Inf"].ToIntDef(0) && (itInf->AV["Inf"].ToIntDef(0) != -1))
         i++ ;
        else
         LogError("error inf code -1", __FUNC__, 5);
        itInf = itInf->GetNext();
       }
      medRecord->Inf.Length = i;
      itInf = AData->GetFirstChild();
      i     = 0;
      while (itInf)
       {
        if (itInf->AV["Inf"].ToIntDef(0) && (itInf->AV["Inf"].ToIntDef(0) != -1))
         medRecord->Inf[i++] = itInf->AV["Inf"].ToIntDef(0);
        else
         LogError("error inf code -1", __FUNC__, 5);
        itInf = itInf->GetNext();
       }
      iemk->AddMedRecord(guid, idLPU, idPat, "", medRecord);
     }
    catch (Exception & E)
     {
#ifndef NETRIKATEST
      LogError(E.Message, __FUNC__, 5);
#else
      if (FOnLog)
       FOnLog("error", TSOAPLogType::ltResponse, E.Message);
#endif
      RC = E.Message;
     }
   }
  __finally
   {
    delete medRecord;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
#ifndef NETRIKATEST
bool __fastcall TNetrikaDM::GetPatData(System::UnicodeString APtId, TTagNode * AData)
 {
  bool RC = false;
  TFDQuery * FQ = CreateTempQuery();
  try
   {
    UnicodeString SQL = "Select cguid, R001F, R0020, R002F, R0031, R0021, R00A5 From class_1000 where code=:pCode";
    if (!quExecParam(FQ, false, SQL, "pCode", APtId).Length())
     {
      if (FQ->RecordCount)
       {
        AData->Name            = "patient";
        AData->AV["idPat"]     = FQ->FieldByName("cguid")->AsString;
        AData->AV["Family"]    = FQ->FieldByName("R001F")->AsString;
        AData->AV["Name"]      = FQ->FieldByName("R0020")->AsString;
        AData->AV["SurName"]   = FQ->FieldByName("R002F")->AsString;
        AData->AV["BirthDate"] = FQ->FieldByName("R0031")->AsDateTime.FormatString("dd.mm.yyyy");
        AData->AV["Sex"]       = IntToStr(FQ->FieldByName("R0021")->AsInteger + 1);
        AData->AV["DocN"]      = FQ->FieldByName("R00A5")->AsString;
        RC                     = true;
       }
     }
   }
  __finally
   {
    DeleteTempQuery(FQ);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TFDQuery * __fastcall TNetrikaDM::CreateTempQuery()
 {
  TFDQuery * FQ = new TFDQuery(this);
  FQ->Transaction = new TFDTransaction(this);
  try
   {
    FQ->Transaction->Connection          = IcConnection;
    FQ->Connection                       = IcConnection;
    FQ->Transaction->Options->AutoStart  = false;
    FQ->Transaction->Options->AutoStop   = false;
    FQ->Transaction->Options->AutoCommit = false;
    FQ->Transaction->Options->StopOptions.Clear();
   }
  __finally
   {
   }
  return FQ;
 }
// ---------------------------------------------------------------------------
void __fastcall TNetrikaDM::DeleteTempQuery(TFDQuery * AQuery)
 {
  if (AQuery->Transaction->Active)
   AQuery->Transaction->Rollback();
  AQuery->Transaction->Connection = NULL;
  AQuery->Connection              = NULL;
  delete AQuery->Transaction;
  delete AQuery;
 }
// ---------------------------------------------------------------------------
bool __fastcall TNetrikaDM::quExec(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL, UnicodeString ALogComment)
 {
  bool RC = false;
  TTime FBeg = Time();
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = true;
     }
    catch (System::Sysutils::Exception & E)
     {
      LogError(E.Message + "; SQL = " + ASQL, __FUNC__);
     }
   }
  __finally
   {
    LogSQL((ALogComment.Length()) ? ALogComment : ASQL, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNetrikaDM::quExecParam(TFDQuery * AQuery, bool ACommit, UnicodeString ASQL,
  UnicodeString AP1Name, Variant AP1Val, UnicodeString ALogComment, UnicodeString AP2Name, Variant AP2Val,
  UnicodeString AP3Name, Variant AP3Val, UnicodeString AP4Name, Variant AP4Val, UnicodeString AP5Name, Variant AP5Val)
 {
  UnicodeString RC = "error";
  TTime FBeg = Time();
  UnicodeString FParams = "; Params: ";
  if (AP1Name.Length())
   FParams += AP1Name + "=" + VarToStrDef(AP1Val, "") + "; ";
  if (AP2Name.Length())
   FParams += AP2Name + "=" + VarToStrDef(AP2Val, "") + "; ";
  if (AP3Name.Length())
   FParams += AP3Name + "=" + VarToStrDef(AP3Val, "") + "; ";
  if (AP4Name.Length())
   FParams += AP4Name + "=" + VarToStrDef(AP4Val, "") + "; ";
  if (AP5Name.Length())
   FParams += AP5Name + "=" + VarToStrDef(AP5Val, "");
  try
   {
    try
     {
      AQuery->Close();
      if (!AQuery->Transaction->Active)
       AQuery->Transaction->StartTransaction();
      AQuery->SQL->Text = ASQL;
      AQuery->Prepare();
      AQuery->ParamByName(AP1Name)->Value = AP1Val;
      if (AP2Name.Length())
       AQuery->ParamByName(AP2Name)->Value = AP2Val;
      if (AP3Name.Length())
       AQuery->ParamByName(AP3Name)->Value = AP3Val;
      if (AP4Name.Length())
       AQuery->ParamByName(AP4Name)->Value = AP4Val;
      if (AP5Name.Length())
       AQuery->ParamByName(AP5Name)->Value = AP5Val;
      AQuery->OpenOrExecute();
      if (ACommit)
       AQuery->Transaction->Commit();
      RC = "";
     }
    catch (System::Sysutils::Exception & E)
     {
      LogError(E.Message + "; SQL = " + ASQL + FParams, __FUNC__);
     }
   }
  __finally
   {
    if (ALogComment.Length())
     LogSQL(ALogComment + FParams, __FUNC__, FBeg - Time(), 5);
    else
     LogSQL(ASQL + FParams, __FUNC__, FBeg - Time(), 5);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TNetrikaDM::LogMessage(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  if (FOnLogMessage)
   FOnLogMessage(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TNetrikaDM::LogError(UnicodeString AMessage, UnicodeString AFuncName, int ALvl)
 {
  if (FOnLogError)
   FOnLogError(AMessage, AFuncName, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TNetrikaDM::LogSQL(UnicodeString ASQL, UnicodeString AFuncName, TTime AExecTime, int ALvl)
 {
  if (FOnLogSQL)
   FOnLogSQL(ASQL, AFuncName, AExecTime, ALvl);
 }
// ---------------------------------------------------------------------------
void __fastcall TNetrikaDM::LogBegin(UnicodeString AFuncName)
 {
  if (FOnLogBegin)
   FOnLogBegin(AFuncName);
 }
// ---------------------------------------------------------------------------
void __fastcall TNetrikaDM::LogEnd(UnicodeString AFuncName)
 {
  if (FOnLogEnd)
   FOnLogEnd(AFuncName);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNetrikaDM::GetInfCode(int ACode)
 {
  UnicodeString RC = "-1";
  try
   {
    TIntStrMap::iterator FRC = FInfCodes.find(ACode);
    if (FRC != FInfCodes.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNetrikaDM::GetVacCode(int ACode)
 {
  UnicodeString RC = "-1";
  try
   {
    TIntStrMap::iterator FRC = FVacCodes.find(ACode);
    if (FRC != FVacCodes.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNetrikaDM::GetTestCode(int ACode)
 {
  UnicodeString RC = "-1";
  try
   {
    TIntStrMap::iterator FRC = FTestCodes.find(ACode);
    if (FRC != FTestCodes.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNetrikaDM::GetReasonCode(int ACode)
 {
  UnicodeString RC = "9"; // ������
  try
   {
    switch (ACode)
     {
     case 0:
      RC = "1";
      break; // �����������
     case 2:
      RC = "2";
      break; // ��������
     case 3:
      RC = "3";
      break; // ����� �� ��������/�����
     case 4:
      RC = "4";
      break; // ������������ �������������
     case 6:
      RC = "5";
      break; // ����������� ��� �����������
     case 7:
      RC = "6";
      break; // ����� �� ������������ � �����������
     case 8:
      RC = "7";
      break; // ������������
     case 9:
      RC = "8";
      break; // ������� � ����������
     case 13:
      RC = "9";
      break; // ������
     default:
      RC = "9";
      break;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNetrikaDM::GetMKBCode(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    if (FMKB)
     RC = FMKB->CodeToExtCode(ACode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TNetrikaDM::SendCardData(UnicodeString AUCode, UnicodeString AGUID, UnicodeString ALPU)
 {
  UnicodeString RC = "";
  UnicodeString FRC;
  TFDQuery * FQ = CreateTempQuery();
  TFDQuery * FQ2 = CreateTempQuery();
  TTagNode * FData = new TTagNode;
  try
   {
    try
     {
      UnicodeString SQL = "Select code From  class_1000  where upper(cguid)=:pcguid";
      if (!quExecParam(FQ, false, SQL, "pcguid", AUCode.UpperCase()).Length())
       {
        if (FQ->RecordCount)
         {
          UnicodeString UCode = FQ->FieldByName("code")->AsString;
          FData->AV["guid"]  = AGUID;
          FData->AV["idLPU"] = ALPU;
          FData->AV["idPat"] = AUCode;
          SQL                = "Select code, CGUID, R3024, R3020, R3185 From  class_3003  where R317A=:pUCode";
          if (!quExecParam(FQ, false, SQL, "pUCode", UCode).Length())
           {
            while (!FQ->Eof)
             {
              try
               {
                FData->DeleteChild();
                FData->AV["RecId"] = FQ->FieldByName("CGUID")->AsString.UpperCase().Trim();
                if (!FData->AV["RecId"].Length())
                 FData->AV["RecId"] = FQ->FieldByName("code")->AsString;
                FData->AV["Status"]  = "1";
                FData->AV["VacDate"] = FQ->FieldByName("R3024")->AsDateTime.FormatString("dd.mm.yyyy");
                FData->AV["VacCode"] = GetVacCode(FQ->FieldByName("R3020")->AsInteger);
                FRC                  = FQ->FieldByName("R3185")->AsString;
                while (FRC.Length())
                 {
                  FData->AV["VacType"] = GetLPartB(FRC, ':');
                  FRC                  = GetRPartB(FRC, ':');
                  FData->AddChild("i")->AV["Inf"] = GetInfCode(GetLPartB(FRC, '^').ToIntDef(0));
                  if (FRC == GetRPartB(FRC, ';'))
                   FRC = "";
                  else
                   FRC = GetRPartB(FRC, ';');
                 }
                if (SendPriv(FData).Length())
                 SendPriv(FData);
               }
              catch (Exception & E)
               {
                LogError(E.Message, __FUNC__, 6);
               }
              FQ->Next();
             }
           }
          SQL = "Select code, CGUID, R1031, R1032, R1200 From  class_102f  where R017B=:pUCode";
          if (!quExecParam(FQ, false, SQL, "pUCode", UCode).Length())
           {
            while (!FQ->Eof)
             {
              try
               {
                FData->DeleteChild();
                FData->AV["RecId"] = FQ->FieldByName("CGUID")->AsString.UpperCase().Trim();
                if (!FData->AV["RecId"].Length())
                 FData->AV["RecId"] = FQ->FieldByName("code")->AsString;
                FData->AV["Status"]   = "1";
                FData->AV["TestDate"] = FQ->FieldByName("R1031")->AsDateTime.FormatString("dd.mm.yyyy");
                FData->AV["TestCode"] = GetTestCode(FQ->FieldByName("R1032")->AsInteger);
                FData->AV["TestType"] = "3";
                FData->AddChild("i")->AV["Inf"] = GetInfCode(FQ->FieldByName("R1200")->AsInteger);
                if (SendTest(FData).Length())
                 SendTest(FData);
               }
              catch (Exception & E)
               {
                LogError(E.Message, __FUNC__, 6);
               }
              FQ->Next();
             }
           }
          SQL = "Select code, CGUID, code, R305F, R3065, R3043, R3066, R3082, R3083 From  class_3030  where R317C=:pUCode";
          if (!quExecParam(FQ, false, SQL, "pUCode", UCode).Length())
           {
            while (!FQ->Eof)
             {
              try
               {
                FData->DeleteChild();
                FData->AV["RecId"] = FQ->FieldByName("CGUID")->AsString.UpperCase().Trim();
                if (!FData->AV["RecId"].Length())
                 FData->AV["RecId"] = FQ->FieldByName("code")->AsString;
                FData->AV["Status"]    = "1";
                FData->AV["DateStart"] = FQ->FieldByName("R305F")->AsDateTime.FormatString("dd.mm.yyyy");
                FData->AV["DateEnd"]   = "";
                if (!FQ->FieldByName("R3065")->IsNull)
                 if (((int)FQ->FieldByName("R3065")->AsDateTime))
                  FData->AV["DateEnd"] = FQ->FieldByName("R3065")->AsDateTime.FormatString("dd.mm.yyyy");
                FData->AV["Type"]   = IntToStr(FQ->FieldByName("R3043")->AsInteger + 1);
                FData->AV["Reason"] = GetReasonCode(FQ->FieldByName("R3066")->AsInteger);
                FData->AV["MKB10"]  = GetMKBCode(FQ->FieldByName("R3082")->AsString);
                SQL                 = "Select R1083 From  class_1030  where R3184=:pCode";
                if (!quExecParam(FQ2, false, SQL, "pCode", FQ->FieldByName("code")->AsString).Length())
                 {
                  while (!FQ2->Eof)
                   {
                    FData->AddChild("i")->AV["Inf"] = GetInfCode(FQ2->FieldByName("R1083")->AsInteger);
                    FQ2->Next();
                   }
                 }
                if (SendCancel(FData).Length())
                 SendCancel(FData);
               }
              catch (Exception & E)
               {
                LogError(E.Message, __FUNC__, 6);
               }
              FQ->Next();
             }
           }
          SQL = "Select code, CGUID, R1115, R1117, R1116, R3187 From  class_1112  where R1113=:pUCode";
          if (!quExecParam(FQ, false, SQL, "pUCode", UCode).Length())
           {
            while (!FQ->Eof)
             {
              try
               {
                FData->DeleteChild();
                FData->AV["RecId"] = FQ->FieldByName("CGUID")->AsString.UpperCase().Trim();
                if (!FData->AV["RecId"].Length())
                 FData->AV["RecId"] = FQ->FieldByName("code")->AsString;
                FData->AV["VacDate"] = FQ->FieldByName("R1115")->AsDateTime.FormatString("dd.mm.yyyy");
                FData->AV["VacType"] = FQ->FieldByName("R1117")->AsString;
                if (FQ->FieldByName("R1116")->AsInteger)
                 FData->AV["PlanType"] = "vac";
                else
                 FData->AV["PlanType"] = "test";
                FData->AddChild("i")->AV["Inf"] = GetInfCode(FQ->FieldByName("R3187")->AsInteger);
                if (SendPlanData(FData).Length())
                 SendPlanData(FData);
               }
              catch (Exception & E)
               {
                LogError(E.Message, __FUNC__, 6);
               }
              FQ->Next();
             }
           }
         }
       }
     }
    catch (Exception & E)
     {
      LogError(E.Message, __FUNC__, 5);
      RC = E.Message;
     }
   }
  __finally
   {
    delete FData;
    DeleteTempQuery(FQ);
    DeleteTempQuery(FQ2);
   }
  return RC;
 }
// ---------------------------------------------------------------------------
#endif
