object NetrikaDM: TNetrikaDM
  OldCreateOrder = False
  Height = 293
  Width = 461
  object IcConnection: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'User_Name=sysdba'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'Server=localhost'
      'Port=3050'
      'Pooled=False')
    FetchOptions.AssignedValues = [evAutoClose]
    FetchOptions.AutoClose = False
    FormatOptions.AssignedValues = [fvFmtDisplayDateTime, fvFmtDisplayDate, fvFmtDisplayTime]
    FormatOptions.FmtDisplayDateTime = 'dd#mm#yyyy hh:nn:ss'
    FormatOptions.FmtDisplayDate = 'dd#mm#yyyy'
    FormatOptions.FmtDisplayTime = 'hh:nn:ss'
    ResourceOptions.AssignedValues = [rvDirectExecute, rvAutoConnect, rvSilentMode]
    ResourceOptions.DirectExecute = True
    ResourceOptions.SilentMode = True
    ResourceOptions.AutoConnect = False
    TxOptions.AutoStart = False
    TxOptions.AutoStop = False
    TxOptions.DisconnectAction = xdRollback
    LoginPrompt = False
    Left = 45
    Top = 103
  end
  object HTTPPat: THTTPRIO
    OnAfterExecute = HTTPPatAfterExecute
    OnBeforeExecute = HTTPPatBeforeExecute
    WSDLLocation = 'D:\work\ic\Server\src\API\MIS\netrika\PixService.wsdl'
    Service = 'PixService'
    Port = 'BasicHttpBinding_IPixService'
    HTTPWebNode.UseUTF8InHeader = True
    HTTPWebNode.InvokeOptions = [soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI]
    HTTPWebNode.WebNodeOptions = []
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 42
    Top = 37
  end
  object HTTPPriv: THTTPRIO
    OnAfterExecute = HTTPPrivAfterExecute
    OnBeforeExecute = HTTPPrivBeforeExecute
    WSDLLocation = 'D:\work\ic\Server\src\API\MIS\netrika\EMKService.wsdl'
    Service = 'EmkService'
    Port = 'BasicHttpBinding_IEmkService'
    HTTPWebNode.UseUTF8InHeader = True
    HTTPWebNode.InvokeOptions = [soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI]
    HTTPWebNode.WebNodeOptions = []
    Converter.Options = [soSendUntyped, soSendMultiRefObj, soSendMultiRefArray, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML, soXXXXHdr, soFormElementUnqualifed, soDontSendVarArrayType]
    Left = 130
    Top = 37
  end
  object HTTPRIO1: THTTPRIO
    OnAfterExecute = HTTPPrivAfterExecute
    OnBeforeExecute = HTTPPrivBeforeExecute
    WSDLLocation = 'D:\work\ic\Server\src\API\MIS\netrika\EMKService.wsdl'
    Service = 'EmkService'
    Port = 'BasicHttpBinding_IEmkService'
    HTTPWebNode.UseUTF8InHeader = True
    HTTPWebNode.InvokeOptions = [soIgnoreInvalidCerts, soAutoCheckAccessPointViaUDDI]
    HTTPWebNode.WebNodeOptions = []
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 266
    Top = 53
  end
end
