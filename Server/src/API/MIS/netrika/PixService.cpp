// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl
// >Import : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl>0
// >Import : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl>1
// >Import : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl>2
// >Import : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl>3
// >Import : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl>4
// >Import : http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl>5
// Encoding : utf-8
// Codegen  : [wfForceSOAP11+]
// Version  : 1.0
// (10.10.2019 20:35:39 - - $Rev: 69934 $)
// ************************************************************************ //
#include <System.hpp>
#pragma hdrstop
#include "PixService.h"
namespace NS_PixService
 {
 static const char * defSvc = "PixService";
 static const char * defPrt = "BasicHttpBinding_IPixService";
 _di_IPixService GetIPixService(bool useWSDL, System::String addr, Soaphttpclient::THTTPRIO * HTTPRIO)
  {
   static const char * defWSDL = "http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc?singleWsdl";
   static const char * defURL = "http://r78-rc.zdrav.netrika.ru/EMK4/PixService.svc";
   if (addr == "")
    addr = useWSDL ? defWSDL : defURL;
   Soaphttpclient::THTTPRIO * rio = HTTPRIO ? HTTPRIO : new Soaphttpclient::THTTPRIO(0);
   if (useWSDL)
    {
     rio->WSDLLocation = addr;
     rio->Service      = defSvc;
     rio->Port         = defPrt;
    }
   else
    {
     rio->URL = addr;
    }
   _di_IPixService service;
   rio->QueryInterface(service);
   if (!service && !HTTPRIO)
    delete rio;
   return service;
  }
 // ------------------------------------------------------------------------------
 _di_IPixService GetIPixService(System::String WsdlAddr, Soaphttpclient::THTTPRIO * HTTPRIO)
  {
   Soaphttpclient::THTTPRIO * rio = HTTPRIO;
   rio->WSDLLocation = WsdlAddr;
   rio->Service      = defSvc;
   rio->Port         = defPrt;
   _di_IPixService service;
   rio->QueryInterface(service);
   if (!service && !HTTPRIO)
    delete rio;
   return service;
  }
 // ------------------------------------------------------------------------------
 __fastcall RequestFault::~RequestFault()
  {
   for (int i = 0; i < FErrors.Length; i++)
    if (FErrors[i])
     delete FErrors[i];
  }
 __fastcall RequestFault2::~RequestFault2()
  {
   for (int i = 0; i < FErrors.Length; i++)
    if (FErrors[i])
     delete FErrors[i];
  }
 __fastcall DocumentDto2::~DocumentDto2()
  {
   delete FExpiredDate;
   delete FIssuedDate;
   delete FStartDate;
  }
 __fastcall PrivilegeDto2::~PrivilegeDto2()
  {
   delete FDateEnd;
   delete FDateStart;
   delete FPrivilegeDocument;
  }
 __fastcall JobDto2::~JobDto2()
  {
   delete FDateEnd;
   delete FDateStart;
  }
 __fastcall ContactPersonDto2::~ContactPersonDto2()
  {
   for (int i = 0; i < FContactList.Length; i++)
    if (FContactList[i])
     delete FContactList[i];
  }
 __fastcall PatientDto2::~PatientDto2()
  {
   delete FBirthDate;
   delete FBirthPlace;
   delete FContactPerson;
   delete FDeathTime;
   delete FJob;
   delete FPrivilege;
   for (int i = 0; i < FAddresses.Length; i++)
    if (FAddresses[i])
     delete FAddresses[i];
   for (int i = 0; i < FContacts.Length; i++)
    if (FContacts[i])
     delete FContacts[i];
   for (int i = 0; i < FDocuments.Length; i++)
    if (FDocuments[i])
     delete FDocuments[i];
  }
 // ************************************************************************ //
 // This routine registers the interfaces and types exposed by the WebService.
 // ************************************************************************ //
 static void RegTypes()
  {
   /* IPixService */
   InvRegistry()->RegisterInterface(__delphirtti(IPixService), L"http://tempuri.org/", L"utf-8");
   InvRegistry()->RegisterAllSOAPActions(__delphirtti(IPixService),
     L"|http://tempuri.org/IServiceSupport/GetVersion|http://tempuri.org/IPixService/AddPatient|http://tempuri.org/IPixService/UpdatePatient|http://tempuri.org/IPixService/GetPatient|http://tempuri.org/IPixService/GetPatientByGlobalId");
   InvRegistry()->RegisterInvokeOptions(__delphirtti(IPixService), ioDocument);
   /* IPixService.GetVersion */
   InvRegistry()->RegisterMethodInfo(__delphirtti(IPixService), "GetVersion", "", "[ReturnName='GetVersionResult']",
     IS_OPTN | IS_NLBL);
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "GetVersion", "GetVersionResult", L"",
     L"[Namespace='http://schemas.datacontract.org/2004/07/N3.Shared.Core']", IS_NLBL);
   /* IPixService->AddPatient */
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "AddPatient", "guid", L"", L"", IS_NLBL);
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "AddPatient", "idLPU", L"", L"", IS_NLBL);
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "AddPatient", "patient", L"",
     L"[Namespace='http://schemas.datacontract.org/2004/07/EMKService.Data.Dto']", IS_NLBL);
   /* IPixService->UpdatePatient */
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "UpdatePatient", "guid", L"", L"", IS_NLBL);
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "UpdatePatient", "idLPU", L"", L"", IS_NLBL);
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "UpdatePatient", "patient", L"",
     L"[Namespace='http://schemas.datacontract.org/2004/07/EMKService.Data.Dto']", IS_NLBL);
   /* IPixService.GetPatient */
   InvRegistry()->RegisterMethodInfo(__delphirtti(IPixService), "GetPatient", "", "[ReturnName='GetPatientResult']",
     IS_OPTN | IS_NLBL);
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "GetPatient", "guid", L"", L"", IS_NLBL);
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "GetPatient", "idLPU", L"", L"", IS_NLBL);
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "GetPatient", "patient", L"",
     L"[Namespace='http://schemas.datacontract.org/2004/07/EMKService.Data.Dto']", IS_NLBL);
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "GetPatient", "idSource", L"",
     L"[Namespace='http://schemas.datacontract.org/2004/07/N3.EMK.Domain.Enums']");
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "GetPatient", "GetPatientResult", L"",
     L"[Namespace='http://schemas.datacontract.org/2004/07/EMKService.Data.Dto', ArrayItemName='PatientDto']", IS_NLBL);
   /* IPixService.GetPatientByGlobalId */
   InvRegistry()->RegisterMethodInfo(__delphirtti(IPixService), "GetPatientByGlobalId", "",
     "[ReturnName='GetPatientByGlobalIdResult']", IS_OPTN | IS_NLBL);
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "GetPatientByGlobalId", "guid", L"", L"", IS_NLBL);
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "GetPatientByGlobalId", "patientId", L"", L"", IS_NLBL);
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "GetPatientByGlobalId", "idLpu", L"", L"", IS_NLBL);
   InvRegistry()->RegisterParamInfo(__delphirtti(IPixService), "GetPatientByGlobalId", "GetPatientByGlobalIdResult",
     L"", L"[Namespace='http://schemas.datacontract.org/2004/07/EMKService.Data.Dto']", IS_NLBL);
   /* ArrayOfAddressDto */
   RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfAddressDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"ArrayOfAddressDto");
   /* ArrayOfPatientDto */
   RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfPatientDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"ArrayOfPatientDto");
   /* SourceType */
   RemClassRegistry()->RegisterXSInfo(GetClsMemberTypeInfo(__typeinfo(SourceType_TypeInfoHolder)),
     L"http://schemas.datacontract.org/2004/07/N3.EMK.Domain.Enums", L"SourceType");
   /* ArrayOfContactDto */
   RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfContactDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"ArrayOfContactDto");
   /* ArrayOfDocumentDto */
   RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfDocumentDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"ArrayOfDocumentDto");
   /* VersionInfo */
   RemClassRegistry()->RegisterXSClass(__classid(VersionInfo2),
     L"http://schemas.datacontract.org/2004/07/N3.Shared.Core", L"VersionInfo2", L"VersionInfo");
   /* ArrayOfRequestFault */
   RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfRequestFault2),
     L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common", L"ArrayOfRequestFault2", L"ArrayOfRequestFault");
   /* ArrayOfRequestFault */
   RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfRequestFault2),
     L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common", L"ArrayOfRequestFault2", L"ArrayOfRequestFault");
   /* SourceType */
   RemClassRegistry()->RegisterXSInfo(GetClsMemberTypeInfo(__typeinfo(SourceType_TypeInfoHolder)),
     L"http://schemas.datacontract.org/2004/07/N3.EMK.Domain.Enums", L"SourceType");
   /* ArrayOfPatientDto */
   RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfPatientDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"ArrayOfPatientDto");
   /* VersionInfo */
   RemClassRegistry()->RegisterXSClass(__classid(VersionInfo),
     L"http://schemas.datacontract.org/2004/07/N3.Shared.Core", L"VersionInfo");
   /* RequestFault */
   RemClassRegistry()->RegisterXSClass(__classid(RequestFault),
     L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common", L"RequestFault");
   RemClassRegistry()->RegisterExternalPropName(__typeinfo(RequestFault), L"Errors", L"[ArrayItemName='RequestFault']");
   /* RequestFault */
   RemClassRegistry()->RegisterXSClass(__classid(RequestFault2),
     L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common", L"RequestFault2", L"RequestFault");
   RemClassRegistry()->RegisterExternalPropName(__typeinfo(RequestFault2), L"Errors",
     L"[ArrayItemName='RequestFault']");
   /* DocumentDto */
   RemClassRegistry()->RegisterXSClass(__classid(DocumentDto2),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"DocumentDto2", L"DocumentDto");
   /* DocumentDto */
   RemClassRegistry()->RegisterXSClass(__classid(DocumentDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"DocumentDto");
   /* ArrayOfAddressDto */
   RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfAddressDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"ArrayOfAddressDto");
   /* PrivilegeDto */
   RemClassRegistry()->RegisterXSClass(__classid(PrivilegeDto2),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"PrivilegeDto2", L"PrivilegeDto");
   /* PrivilegeDto */
   RemClassRegistry()->RegisterXSClass(__classid(PrivilegeDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"PrivilegeDto");
   /* JobDto */
   RemClassRegistry()->RegisterXSClass(__classid(JobDto2),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"JobDto2", L"JobDto");
   /* JobDto */
   RemClassRegistry()->RegisterXSClass(__classid(JobDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"JobDto");
   /* ContactDto */
   RemClassRegistry()->RegisterXSClass(__classid(ContactDto2),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"ContactDto2", L"ContactDto");
   /* ContactDto */
   RemClassRegistry()->RegisterXSClass(__classid(ContactDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"ContactDto");
   /* AddressDto */
   RemClassRegistry()->RegisterXSClass(__classid(AddressDto2),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"AddressDto2", L"AddressDto");
   /* AddressDto */
   RemClassRegistry()->RegisterXSClass(__classid(AddressDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"AddressDto");
   /* BirthPlaceDto */
   RemClassRegistry()->RegisterXSClass(__classid(BirthPlaceDto2),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"BirthPlaceDto2", L"BirthPlaceDto");
   /* BirthPlaceDto */
   RemClassRegistry()->RegisterXSClass(__classid(BirthPlaceDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"BirthPlaceDto");
   /* ArrayOfDocumentDto */
   RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfDocumentDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"ArrayOfDocumentDto");
   /* ArrayOfContactDto */
   RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfContactDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"ArrayOfContactDto");
   /* ContactPersonDto */
   RemClassRegistry()->RegisterXSClass(__classid(ContactPersonDto2),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"ContactPersonDto2", L"ContactPersonDto");
   RemClassRegistry()->RegisterExternalPropName(__typeinfo(ContactPersonDto2), L"ContactList",
     L"[ArrayItemName='ContactDto']");
   /* ContactPersonDto */
   RemClassRegistry()->RegisterXSClass(__classid(ContactPersonDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"ContactPersonDto");
   /* PatientDto */
   RemClassRegistry()->RegisterXSClass(__classid(PatientDto2),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"PatientDto2", L"PatientDto");
   RemClassRegistry()->RegisterExternalPropName(__typeinfo(PatientDto2), L"Addresses", L"[ArrayItemName='AddressDto']");
   RemClassRegistry()->RegisterExternalPropName(__typeinfo(PatientDto2), L"Contacts", L"[ArrayItemName='ContactDto']");
   RemClassRegistry()->RegisterExternalPropName(__typeinfo(PatientDto2), L"Documents",
     L"[ArrayItemName='DocumentDto']");
   /* PatientDto */
   RemClassRegistry()->RegisterXSClass(__classid(PatientDto),
     L"http://schemas.datacontract.org/2004/07/EMKService.Data.Dto", L"PatientDto");
  }
#pragma startup RegTypes 32
 }; // NS_PixService
