// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>0
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>1
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>2
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>3
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>4
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>5
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>6
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>7
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>8
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>9
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>10
// Encoding : utf-8
// Codegen  : [wfOutputLiteralTypes+]
// Version  : 1.0
// (01.04.2020 22:01:55 - - $Rev: 69934 $)
// ************************************************************************ //

#ifndef   EMKServiceH
#define   EMKServiceH

#include <System.hpp>
#include <Soap.InvokeRegistry.hpp>
#include <Soap.XSBuiltIns.hpp>
#include <Soap.SOAPHTTPClient.hpp>

#if !defined(SOAP_REMOTABLE_CLASS)
#define SOAP_REMOTABLE_CLASS __declspec(delphiclass)
#endif
#if !defined(IS_OPTN)
#define IS_OPTN 0x0001
#endif
#if !defined(IS_UNBD)
#define IS_UNBD 0x0002
#endif
#if !defined(IS_NLBL)
#define IS_NLBL 0x0004
#endif
#if !defined(IS_REF)
#define IS_REF 0x0080
#endif


namespace NS_EMKService {

// ************************************************************************ //
// The following types, referred to in the WSDL document are not being represented
// in this file. They are either aliases[@] of other types represented or were referred
// to but never[!] declared in the document. The types from the latter category
// typically map to predefined/known XML or Embarcadero types; however, they could also
// indicate incorrect WSDL documents that failed to declare or import a schema type.
// ************************************************************************ //
// !:anyURI          - "http://www.w3.org/2001/XMLSchema"[Gbl]
// !:base64Binary    - "http://www.w3.org/2001/XMLSchema"[Gbl]
// !:boolean         - "http://www.w3.org/2001/XMLSchema"[Gbl]
// !:decimal         - "http://www.w3.org/2001/XMLSchema"[Gbl]
// !:unsignedShort   - "http://www.w3.org/2001/XMLSchema"[Gbl]
// !:unsignedByte    - "http://www.w3.org/2001/XMLSchema"[Gbl]
// !:dateTime        - "http://www.w3.org/2001/XMLSchema"[Gbl]
// !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]
// !:int             - "http://www.w3.org/2001/XMLSchema"[Gbl]

class SOAP_REMOTABLE_CLASS HealthGroup2;
class SOAP_REMOTABLE_CLASS ValueQuantity2;
class SOAP_REMOTABLE_CLASS HealthGroup;
class SOAP_REMOTABLE_CLASS MedDocumentDto_PersonalSign2;
class SOAP_REMOTABLE_CLASS MedDocumentDto_PersonalSign;
class SOAP_REMOTABLE_CLASS ValueQuantity;
class SOAP_REMOTABLE_CLASS HealthGroupInfo2;
class SOAP_REMOTABLE_CLASS HealthGroupInfo;
class SOAP_REMOTABLE_CLASS Participant2;
class SOAP_REMOTABLE_CLASS Participant;
class SOAP_REMOTABLE_CLASS CaseBase2;
class SOAP_REMOTABLE_CLASS Person2;
class SOAP_REMOTABLE_CLASS Person;
class SOAP_REMOTABLE_CLASS PersonWithIdentity2;
class SOAP_REMOTABLE_CLASS PersonWithIdentity;
class SOAP_REMOTABLE_CLASS HumanName2;
class SOAP_REMOTABLE_CLASS HumanName;
class SOAP_REMOTABLE_CLASS PacsResult_ImageData2;
class SOAP_REMOTABLE_CLASS PacsResult_ImageData;
class SOAP_REMOTABLE_CLASS MedDocumentDto_DocumentAttachment2;
class SOAP_REMOTABLE_CLASS MedDocumentDto_DocumentAttachment;
class SOAP_REMOTABLE_CLASS StringValue2;
class SOAP_REMOTABLE_CLASS StringValue;
class SOAP_REMOTABLE_CLASS Recommendation2;
class SOAP_REMOTABLE_CLASS Recommendation;
class SOAP_REMOTABLE_CLASS ReferralInfo2;
class SOAP_REMOTABLE_CLASS ReferralInfo;
class SOAP_REMOTABLE_CLASS AssisNote2;
class SOAP_REMOTABLE_CLASS AssisNote;
class SOAP_REMOTABLE_CLASS SickListInfo2;
class SOAP_REMOTABLE_CLASS SickListInfo;
class SOAP_REMOTABLE_CLASS VersionInfo2;
class SOAP_REMOTABLE_CLASS VersionInfo;
class SOAP_REMOTABLE_CLASS MedicalStaff2;
class SOAP_REMOTABLE_CLASS MedicalStaff;
class SOAP_REMOTABLE_CLASS IdentityDocument2;
class SOAP_REMOTABLE_CLASS IdentityDocument;
class SOAP_REMOTABLE_CLASS Quantity2;
class SOAP_REMOTABLE_CLASS Quantity;
class SOAP_REMOTABLE_CLASS Param2;
class SOAP_REMOTABLE_CLASS RequestFault;
class SOAP_REMOTABLE_CLASS RequestFault2;
class SOAP_REMOTABLE_CLASS Observation2;
class SOAP_REMOTABLE_CLASS Observation;
class SOAP_REMOTABLE_CLASS ReferenceRange2;
class SOAP_REMOTABLE_CLASS ReferenceRange;
class SOAP_REMOTABLE_CLASS CodeableValue2;
class SOAP_REMOTABLE_CLASS CodeableValue;
class SOAP_REMOTABLE_CLASS NumericalValue2;
class SOAP_REMOTABLE_CLASS NumericalValue;
class SOAP_REMOTABLE_CLASS DiagnosisInfo2;
class SOAP_REMOTABLE_CLASS DiagnosisInfo;
class SOAP_REMOTABLE_CLASS StepBase2;
class SOAP_REMOTABLE_CLASS StepBase;
class SOAP_REMOTABLE_CLASS MedRecord2;
class SOAP_REMOTABLE_CLASS Problem2;
class SOAP_REMOTABLE_CLASS AppointedMedication2;
class SOAP_REMOTABLE_CLASS TfomsInfo2;
class SOAP_REMOTABLE_CLASS DeathInfo2;
class SOAP_REMOTABLE_CLASS Diagnosis2;
class SOAP_REMOTABLE_CLASS ClinicMainDiagnosis2;
class SOAP_REMOTABLE_CLASS ClinicMainDiagnosis;
class SOAP_REMOTABLE_CLASS AnatomopathologicalClinicMainDiagnosis2;
class SOAP_REMOTABLE_CLASS AnatomopathologicalClinicMainDiagnosis;
class SOAP_REMOTABLE_CLASS Diagnosis;
class SOAP_REMOTABLE_CLASS MedDocument2;
class SOAP_REMOTABLE_CLASS Referral2;
class SOAP_REMOTABLE_CLASS Referral;
class SOAP_REMOTABLE_CLASS AmbulanceInfo2;
class SOAP_REMOTABLE_CLASS AmbulanceInfo;
class SOAP_REMOTABLE_CLASS DeathCertificate2;
class SOAP_REMOTABLE_CLASS DeathCertificate;
class SOAP_REMOTABLE_CLASS Form106U2;
class SOAP_REMOTABLE_CLASS Form106U;
class SOAP_REMOTABLE_CLASS PacsResult2;
class SOAP_REMOTABLE_CLASS PacsResult;
class SOAP_REMOTABLE_CLASS ReferralMSE2;
class SOAP_REMOTABLE_CLASS ReferralMSE;
class SOAP_REMOTABLE_CLASS MedDocument;
class SOAP_REMOTABLE_CLASS DispensaryBase2;
class SOAP_REMOTABLE_CLASS DispensaryBase;
class SOAP_REMOTABLE_CLASS DispensaryOne2;
class SOAP_REMOTABLE_CLASS DispensaryOne;
class SOAP_REMOTABLE_CLASS DispensaryOnceByTwoYears2;
class SOAP_REMOTABLE_CLASS DispensaryOnceByTwoYears;
class SOAP_REMOTABLE_CLASS DispensaryTwo2;
class SOAP_REMOTABLE_CLASS DispensaryTwo;
class SOAP_REMOTABLE_CLASS Form103U2;
class SOAP_REMOTABLE_CLASS Form103U;
class SOAP_REMOTABLE_CLASS ConsultNote2;
class SOAP_REMOTABLE_CLASS ConsultNote;
class SOAP_REMOTABLE_CLASS DischargeSummary2;
class SOAP_REMOTABLE_CLASS DischargeSummary;
class SOAP_REMOTABLE_CLASS Form106_2U2;
class SOAP_REMOTABLE_CLASS Form106_2U;
class SOAP_REMOTABLE_CLASS SickList2;
class SOAP_REMOTABLE_CLASS SickList;
class SOAP_REMOTABLE_CLASS LaboratoryReport2;
class SOAP_REMOTABLE_CLASS LaboratoryReport;
class SOAP_REMOTABLE_CLASS Form027U2;
class SOAP_REMOTABLE_CLASS Form027U;
class SOAP_REMOTABLE_CLASS CaseBase;
class SOAP_REMOTABLE_CLASS Initiator2;
class SOAP_REMOTABLE_CLASS Initiator;
class SOAP_REMOTABLE_CLASS Guardian2;
class SOAP_REMOTABLE_CLASS Guardian;
class SOAP_REMOTABLE_CLASS CaseStat2;
class SOAP_REMOTABLE_CLASS CaseStat;
class SOAP_REMOTABLE_CLASS CaseAmb2;
class SOAP_REMOTABLE_CLASS CaseAmb;
class SOAP_REMOTABLE_CLASS CaseAcps2;
class SOAP_REMOTABLE_CLASS CaseAcps;
class SOAP_REMOTABLE_CLASS CaseTmc2;
class SOAP_REMOTABLE_CLASS CaseTmc;
class SOAP_REMOTABLE_CLASS StepAmb2;
class SOAP_REMOTABLE_CLASS StepAmb;
class SOAP_REMOTABLE_CLASS StepStat2;
class SOAP_REMOTABLE_CLASS StepStat;
class SOAP_REMOTABLE_CLASS Param;
class SOAP_REMOTABLE_CLASS DeathInfo;
class SOAP_REMOTABLE_CLASS MedRecord;
class SOAP_REMOTABLE_CLASS TfomsInfo;
class SOAP_REMOTABLE_CLASS AppointedMedication;
class SOAP_REMOTABLE_CLASS Problem;
class SOAP_REMOTABLE_CLASS Privilege2;
class SOAP_REMOTABLE_CLASS Privilege;
class SOAP_REMOTABLE_CLASS ImmunizationType2;
class SOAP_REMOTABLE_CLASS ReactionType2;
class SOAP_REMOTABLE_CLASS ReactionType;
class SOAP_REMOTABLE_CLASS ImmunizationType;
class SOAP_REMOTABLE_CLASS NonDrugTreatment2;
class SOAP_REMOTABLE_CLASS NonDrugTreatment;
class SOAP_REMOTABLE_CLASS PaymentInfo2;
class SOAP_REMOTABLE_CLASS PaymentInfo;
class SOAP_REMOTABLE_CLASS Service2;
class SOAP_REMOTABLE_CLASS Service;
class SOAP_REMOTABLE_CLASS Immunize2;
class SOAP_REMOTABLE_CLASS Vaccination2;
class SOAP_REMOTABLE_CLASS Vaccination;
class SOAP_REMOTABLE_CLASS ImmunizationPlan2;
class SOAP_REMOTABLE_CLASS ImmunizationPlan;
class SOAP_REMOTABLE_CLASS Reaction2;
class SOAP_REMOTABLE_CLASS Reaction;
class SOAP_REMOTABLE_CLASS MedicalExemption2;
class SOAP_REMOTABLE_CLASS MedicalExemption;
class SOAP_REMOTABLE_CLASS Immunize;
class SOAP_REMOTABLE_CLASS Procedure2;
class SOAP_REMOTABLE_CLASS Procedure;
class SOAP_REMOTABLE_CLASS AllergyBase2;
class SOAP_REMOTABLE_CLASS AllergyNonDrug2;
class SOAP_REMOTABLE_CLASS AllergyNonDrug;
class SOAP_REMOTABLE_CLASS AllergyBase;
class SOAP_REMOTABLE_CLASS SocialGroup2;
class SOAP_REMOTABLE_CLASS SocialGroup;
class SOAP_REMOTABLE_CLASS Disability2;
class SOAP_REMOTABLE_CLASS Disability;
class SOAP_REMOTABLE_CLASS SocialAnamnesis2;
class SOAP_REMOTABLE_CLASS SocialAnamnesis;
class SOAP_REMOTABLE_CLASS ResInstr2;
class SOAP_REMOTABLE_CLASS ResInstr;
class SOAP_REMOTABLE_CLASS VaccinationType2;
class SOAP_REMOTABLE_CLASS VaccinationType;
class SOAP_REMOTABLE_CLASS AllergyDrug2;
class SOAP_REMOTABLE_CLASS AllergyDrug;
class SOAP_REMOTABLE_CLASS Scores2;
class SOAP_REMOTABLE_CLASS Scores;
class SOAP_REMOTABLE_CLASS ResInstr_Activity2;
class SOAP_REMOTABLE_CLASS ResInstr_Activity;



// ************************************************************************ //
// XML       : HealthGroup, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class HealthGroup2 : public TRemotable {
private:
  MedicalStaff2*  FDoctor;
  bool            FDoctor_Specified;
  HealthGroupInfo2* FHealthGroupInfo;
  bool            FHealthGroupInfo_Specified;
  void __fastcall SetDoctor(int Index, MedicalStaff2* _prop_val)
  {  FDoctor = _prop_val; FDoctor_Specified = true;  }
  bool __fastcall Doctor_Specified(int Index)
  {  return FDoctor_Specified;  }
  void __fastcall SetHealthGroupInfo(int Index, HealthGroupInfo2* _prop_val)
  {  FHealthGroupInfo = _prop_val; FHealthGroupInfo_Specified = true;  }
  bool __fastcall HealthGroupInfo_Specified(int Index)
  {  return FHealthGroupInfo_Specified;  }

public:
  __fastcall ~HealthGroup2();
__published:
  __property MedicalStaff2*     Doctor = { index=(IS_OPTN | IS_NLBL), read=FDoctor, write=SetDoctor, stored = Doctor_Specified };
  __property HealthGroupInfo2* HealthGroupInfo = { index=(IS_OPTN | IS_NLBL), read=FHealthGroupInfo, write=SetHealthGroupInfo, stored = HealthGroupInfo_Specified };
};


typedef DynamicArray<AssisNote2*> ArrayOfAssisNote; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc"[GblCplx] */
typedef DynamicArray<Recommendation2*> ArrayOfRecommendation; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc"[GblCplx] */
typedef DynamicArray<MedDocumentDto_DocumentAttachment2*> ArrayOfMedDocumentDto_DocumentAttachment; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc"[GblCplx] */
typedef DynamicArray<Diagnosis2*> ArrayOfDiagnosis; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag"[GblCplx] */
typedef DynamicArray<Observation2*> ArrayOfObservation; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc"[GblCplx] */
typedef DynamicArray<MedDocumentDto_PersonalSign2*> ArrayOfMedDocumentDto_PersonalSign; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc"[GblCplx] */
typedef DynamicArray<ReferenceRange2*> ArrayOfReferenceRange; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc"[GblCplx] */


// ************************************************************************ //
// XML       : ValueQuantity, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class ValueQuantity2 : public TRemotable {
private:
__published:
};




// ************************************************************************ //
// XML       : HealthGroup, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class HealthGroup : public HealthGroup2 {
private:
__published:
};




// ************************************************************************ //
// XML       : MedDocumentDto.PersonalSign, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class MedDocumentDto_PersonalSign2 : public TRemotable {
private:
  TByteDynArray   FSign;
  bool            FSign_Specified;
  MedicalStaff2*  FDoctor;
  bool            FDoctor_Specified;
  void __fastcall SetSign(int Index, TByteDynArray _prop_val)
  {  FSign = _prop_val; FSign_Specified = true;  }
  bool __fastcall Sign_Specified(int Index)
  {  return FSign_Specified;  }
  void __fastcall SetDoctor(int Index, MedicalStaff2* _prop_val)
  {  FDoctor = _prop_val; FDoctor_Specified = true;  }
  bool __fastcall Doctor_Specified(int Index)
  {  return FDoctor_Specified;  }

public:
  __fastcall ~MedDocumentDto_PersonalSign2();
__published:
  __property TByteDynArray       Sign = { index=(IS_OPTN | IS_NLBL), read=FSign, write=SetSign, stored = Sign_Specified };
  __property MedicalStaff2*     Doctor = { index=(IS_OPTN | IS_NLBL), read=FDoctor, write=SetDoctor, stored = Doctor_Specified };
};


typedef DynamicArray<RequestFault2*> ArrayOfRequestFault2; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common"[GblCplx] */
typedef ArrayOfRequestFault2 ArrayOfRequestFault; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common"[Flt][GblElm] */


// ************************************************************************ //
// XML       : MedDocumentDto.PersonalSign, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class MedDocumentDto_PersonalSign : public MedDocumentDto_PersonalSign2 {
private:
__published:
};


typedef DynamicArray<Param2*>     ArrayOfParam;   /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec"[GblCplx] */


// ************************************************************************ //
// XML       : ValueQuantity, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class ValueQuantity : public ValueQuantity2 {
private:
__published:
};


typedef DynamicArray<IdentityDocument2*> ArrayOfIdentityDocument; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto"[GblCplx] */


// ************************************************************************ //
// XML       : HealthGroupInfo, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class HealthGroupInfo2 : public TRemotable {
private:
  unsigned char   FIdHealthGroup;
  bool            FIdHealthGroup_Specified;
  TXSDateTime*    FDate;
  bool            FDate_Specified;
  void __fastcall SetIdHealthGroup(int Index, unsigned char _prop_val)
  {  FIdHealthGroup = _prop_val; FIdHealthGroup_Specified = true;  }
  bool __fastcall IdHealthGroup_Specified(int Index)
  {  return FIdHealthGroup_Specified;  }
  void __fastcall SetDate(int Index, TXSDateTime* _prop_val)
  {  FDate = _prop_val; FDate_Specified = true;  }
  bool __fastcall Date_Specified(int Index)
  {  return FDate_Specified;  }

public:
  __fastcall ~HealthGroupInfo2();
__published:
  __property unsigned char IdHealthGroup = { index=(IS_OPTN), read=FIdHealthGroup, write=SetIdHealthGroup, stored = IdHealthGroup_Specified };
  __property TXSDateTime*       Date = { index=(IS_OPTN), read=FDate, write=SetDate, stored = Date_Specified };
};




// ************************************************************************ //
// XML       : HealthGroupInfo, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class HealthGroupInfo : public HealthGroupInfo2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Participant, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class Participant2 : public TRemotable {
private:
  MedicalStaff2*  FDoctor;
  bool            FDoctor_Specified;
  unsigned char   FIdRole;
  bool            FIdRole_Specified;
  void __fastcall SetDoctor(int Index, MedicalStaff2* _prop_val)
  {  FDoctor = _prop_val; FDoctor_Specified = true;  }
  bool __fastcall Doctor_Specified(int Index)
  {  return FDoctor_Specified;  }
  void __fastcall SetIdRole(int Index, unsigned char _prop_val)
  {  FIdRole = _prop_val; FIdRole_Specified = true;  }
  bool __fastcall IdRole_Specified(int Index)
  {  return FIdRole_Specified;  }

public:
  __fastcall ~Participant2();
__published:
  __property MedicalStaff2*     Doctor = { index=(IS_OPTN | IS_NLBL), read=FDoctor, write=SetDoctor, stored = Doctor_Specified };
  __property unsigned char     IdRole = { index=(IS_OPTN), read=FIdRole, write=SetIdRole, stored = IdRole_Specified };
};




// ************************************************************************ //
// XML       : Participant, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class Participant : public Participant2 {
private:
__published:
};




// ************************************************************************ //
// XML       : CaseBase, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case
// ************************************************************************ //
class CaseBase2 : public TRemotable {
private:
  TXSDateTime*    FOpenDate;
  bool            FOpenDate_Specified;
  TXSDateTime*    FCloseDate;
  bool            FCloseDate_Specified;
  UnicodeString   FHistoryNumber;
  bool            FHistoryNumber_Specified;
  UnicodeString   FIdCaseMis;
  bool            FIdCaseMis_Specified;
  unsigned char   FIdCaseAidType;
  bool            FIdCaseAidType_Specified;
  unsigned char   FIdPaymentType;
  bool            FIdPaymentType_Specified;
  unsigned char   FConfidentiality;
  bool            FConfidentiality_Specified;
  unsigned char   FDoctorConfidentiality;
  bool            FDoctorConfidentiality_Specified;
  unsigned char   FCuratorConfidentiality;
  bool            FCuratorConfidentiality_Specified;
  UnicodeString   FIdLpu;
  bool            FIdLpu_Specified;
  unsigned char   FIdCaseResult;
  bool            FIdCaseResult_Specified;
  UnicodeString   FComment;
  bool            FComment_Specified;
  MedicalStaff2*  FDoctorInCharge;
  bool            FDoctorInCharge_Specified;
  Participant2*   FAuthenticator;
  bool            FAuthenticator_Specified;
  Participant2*   FAuthor;
  bool            FAuthor_Specified;
  Participant2*   FLegalAuthenticator;
  bool            FLegalAuthenticator_Specified;
  Guardian2*      FGuardian;
  bool            FGuardian_Specified;
  UnicodeString   FIdPatientMis;
  bool            FIdPatientMis_Specified;
  unsigned char   FAdmissionCondition;
  bool            FAdmissionCondition_Specified;
  unsigned char   FCaseVisitType;
  bool            FCaseVisitType_Specified;
  void __fastcall SetOpenDate(int Index, TXSDateTime* _prop_val)
  {  FOpenDate = _prop_val; FOpenDate_Specified = true;  }
  bool __fastcall OpenDate_Specified(int Index)
  {  return FOpenDate_Specified;  }
  void __fastcall SetCloseDate(int Index, TXSDateTime* _prop_val)
  {  FCloseDate = _prop_val; FCloseDate_Specified = true;  }
  bool __fastcall CloseDate_Specified(int Index)
  {  return FCloseDate_Specified;  }
  void __fastcall SetHistoryNumber(int Index, UnicodeString _prop_val)
  {  FHistoryNumber = _prop_val; FHistoryNumber_Specified = true;  }
  bool __fastcall HistoryNumber_Specified(int Index)
  {  return FHistoryNumber_Specified;  }
  void __fastcall SetIdCaseMis(int Index, UnicodeString _prop_val)
  {  FIdCaseMis = _prop_val; FIdCaseMis_Specified = true;  }
  bool __fastcall IdCaseMis_Specified(int Index)
  {  return FIdCaseMis_Specified;  }
  void __fastcall SetIdCaseAidType(int Index, unsigned char _prop_val)
  {  FIdCaseAidType = _prop_val; FIdCaseAidType_Specified = true;  }
  bool __fastcall IdCaseAidType_Specified(int Index)
  {  return FIdCaseAidType_Specified;  }
  void __fastcall SetIdPaymentType(int Index, unsigned char _prop_val)
  {  FIdPaymentType = _prop_val; FIdPaymentType_Specified = true;  }
  bool __fastcall IdPaymentType_Specified(int Index)
  {  return FIdPaymentType_Specified;  }
  void __fastcall SetConfidentiality(int Index, unsigned char _prop_val)
  {  FConfidentiality = _prop_val; FConfidentiality_Specified = true;  }
  bool __fastcall Confidentiality_Specified(int Index)
  {  return FConfidentiality_Specified;  }
  void __fastcall SetDoctorConfidentiality(int Index, unsigned char _prop_val)
  {  FDoctorConfidentiality = _prop_val; FDoctorConfidentiality_Specified = true;  }
  bool __fastcall DoctorConfidentiality_Specified(int Index)
  {  return FDoctorConfidentiality_Specified;  }
  void __fastcall SetCuratorConfidentiality(int Index, unsigned char _prop_val)
  {  FCuratorConfidentiality = _prop_val; FCuratorConfidentiality_Specified = true;  }
  bool __fastcall CuratorConfidentiality_Specified(int Index)
  {  return FCuratorConfidentiality_Specified;  }
  void __fastcall SetIdLpu(int Index, UnicodeString _prop_val)
  {  FIdLpu = _prop_val; FIdLpu_Specified = true;  }
  bool __fastcall IdLpu_Specified(int Index)
  {  return FIdLpu_Specified;  }
  void __fastcall SetIdCaseResult(int Index, unsigned char _prop_val)
  {  FIdCaseResult = _prop_val; FIdCaseResult_Specified = true;  }
  bool __fastcall IdCaseResult_Specified(int Index)
  {  return FIdCaseResult_Specified;  }
  void __fastcall SetComment(int Index, UnicodeString _prop_val)
  {  FComment = _prop_val; FComment_Specified = true;  }
  bool __fastcall Comment_Specified(int Index)
  {  return FComment_Specified;  }
  void __fastcall SetDoctorInCharge(int Index, MedicalStaff2* _prop_val)
  {  FDoctorInCharge = _prop_val; FDoctorInCharge_Specified = true;  }
  bool __fastcall DoctorInCharge_Specified(int Index)
  {  return FDoctorInCharge_Specified;  }
  void __fastcall SetAuthenticator(int Index, Participant2* _prop_val)
  {  FAuthenticator = _prop_val; FAuthenticator_Specified = true;  }
  bool __fastcall Authenticator_Specified(int Index)
  {  return FAuthenticator_Specified;  }
  void __fastcall SetAuthor(int Index, Participant2* _prop_val)
  {  FAuthor = _prop_val; FAuthor_Specified = true;  }
  bool __fastcall Author_Specified(int Index)
  {  return FAuthor_Specified;  }
  void __fastcall SetLegalAuthenticator(int Index, Participant2* _prop_val)
  {  FLegalAuthenticator = _prop_val; FLegalAuthenticator_Specified = true;  }
  bool __fastcall LegalAuthenticator_Specified(int Index)
  {  return FLegalAuthenticator_Specified;  }
  void __fastcall SetGuardian(int Index, Guardian2* _prop_val)
  {  FGuardian = _prop_val; FGuardian_Specified = true;  }
  bool __fastcall Guardian_Specified(int Index)
  {  return FGuardian_Specified;  }
  void __fastcall SetIdPatientMis(int Index, UnicodeString _prop_val)
  {  FIdPatientMis = _prop_val; FIdPatientMis_Specified = true;  }
  bool __fastcall IdPatientMis_Specified(int Index)
  {  return FIdPatientMis_Specified;  }
  void __fastcall SetAdmissionCondition(int Index, unsigned char _prop_val)
  {  FAdmissionCondition = _prop_val; FAdmissionCondition_Specified = true;  }
  bool __fastcall AdmissionCondition_Specified(int Index)
  {  return FAdmissionCondition_Specified;  }
  void __fastcall SetCaseVisitType(int Index, unsigned char _prop_val)
  {  FCaseVisitType = _prop_val; FCaseVisitType_Specified = true;  }
  bool __fastcall CaseVisitType_Specified(int Index)
  {  return FCaseVisitType_Specified;  }

public:
  __fastcall ~CaseBase2();
__published:
  __property TXSDateTime*   OpenDate = { index=(IS_OPTN), read=FOpenDate, write=SetOpenDate, stored = OpenDate_Specified };
  __property TXSDateTime*  CloseDate = { index=(IS_OPTN), read=FCloseDate, write=SetCloseDate, stored = CloseDate_Specified };
  __property UnicodeString HistoryNumber = { index=(IS_OPTN | IS_NLBL), read=FHistoryNumber, write=SetHistoryNumber, stored = HistoryNumber_Specified };
  __property UnicodeString  IdCaseMis = { index=(IS_OPTN | IS_NLBL), read=FIdCaseMis, write=SetIdCaseMis, stored = IdCaseMis_Specified };
  __property unsigned char IdCaseAidType = { index=(IS_OPTN), read=FIdCaseAidType, write=SetIdCaseAidType, stored = IdCaseAidType_Specified };
  __property unsigned char IdPaymentType = { index=(IS_OPTN), read=FIdPaymentType, write=SetIdPaymentType, stored = IdPaymentType_Specified };
  __property unsigned char Confidentiality = { index=(IS_OPTN), read=FConfidentiality, write=SetConfidentiality, stored = Confidentiality_Specified };
  __property unsigned char DoctorConfidentiality = { index=(IS_OPTN), read=FDoctorConfidentiality, write=SetDoctorConfidentiality, stored = DoctorConfidentiality_Specified };
  __property unsigned char CuratorConfidentiality = { index=(IS_OPTN), read=FCuratorConfidentiality, write=SetCuratorConfidentiality, stored = CuratorConfidentiality_Specified };
  __property UnicodeString      IdLpu = { index=(IS_OPTN | IS_NLBL), read=FIdLpu, write=SetIdLpu, stored = IdLpu_Specified };
  __property unsigned char IdCaseResult = { index=(IS_OPTN), read=FIdCaseResult, write=SetIdCaseResult, stored = IdCaseResult_Specified };
  __property UnicodeString    Comment = { index=(IS_OPTN | IS_NLBL), read=FComment, write=SetComment, stored = Comment_Specified };
  __property MedicalStaff2* DoctorInCharge = { index=(IS_OPTN | IS_NLBL), read=FDoctorInCharge, write=SetDoctorInCharge, stored = DoctorInCharge_Specified };
  __property Participant2* Authenticator = { index=(IS_OPTN | IS_NLBL), read=FAuthenticator, write=SetAuthenticator, stored = Authenticator_Specified };
  __property Participant2*     Author = { index=(IS_OPTN | IS_NLBL), read=FAuthor, write=SetAuthor, stored = Author_Specified };
  __property Participant2* LegalAuthenticator = { index=(IS_OPTN | IS_NLBL), read=FLegalAuthenticator, write=SetLegalAuthenticator, stored = LegalAuthenticator_Specified };
  __property Guardian2*   Guardian = { index=(IS_OPTN | IS_NLBL), read=FGuardian, write=SetGuardian, stored = Guardian_Specified };
  __property UnicodeString IdPatientMis = { index=(IS_OPTN | IS_NLBL), read=FIdPatientMis, write=SetIdPatientMis, stored = IdPatientMis_Specified };
  __property unsigned char AdmissionCondition = { index=(IS_OPTN), read=FAdmissionCondition, write=SetAdmissionCondition, stored = AdmissionCondition_Specified };
  __property unsigned char CaseVisitType = { index=(IS_OPTN), read=FCaseVisitType, write=SetCaseVisitType, stored = CaseVisitType_Specified };
};




// ************************************************************************ //
// XML       : Person, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class Person2 : public TRemotable {
private:
  HumanName2*     FHumanName;
  bool            FHumanName_Specified;
  unsigned char   FSex;
  bool            FSex_Specified;
  TXSDateTime*    FBirthdate;
  bool            FBirthdate_Specified;
  UnicodeString   FIdPersonMis;
  bool            FIdPersonMis_Specified;
  void __fastcall SetHumanName(int Index, HumanName2* _prop_val)
  {  FHumanName = _prop_val; FHumanName_Specified = true;  }
  bool __fastcall HumanName_Specified(int Index)
  {  return FHumanName_Specified;  }
  void __fastcall SetSex(int Index, unsigned char _prop_val)
  {  FSex = _prop_val; FSex_Specified = true;  }
  bool __fastcall Sex_Specified(int Index)
  {  return FSex_Specified;  }
  void __fastcall SetBirthdate(int Index, TXSDateTime* _prop_val)
  {  FBirthdate = _prop_val; FBirthdate_Specified = true;  }
  bool __fastcall Birthdate_Specified(int Index)
  {  return FBirthdate_Specified;  }
  void __fastcall SetIdPersonMis(int Index, UnicodeString _prop_val)
  {  FIdPersonMis = _prop_val; FIdPersonMis_Specified = true;  }
  bool __fastcall IdPersonMis_Specified(int Index)
  {  return FIdPersonMis_Specified;  }

public:
  __fastcall ~Person2();
__published:
  __property HumanName2*  HumanName = { index=(IS_OPTN | IS_NLBL), read=FHumanName, write=SetHumanName, stored = HumanName_Specified };
  __property unsigned char        Sex = { index=(IS_OPTN), read=FSex, write=SetSex, stored = Sex_Specified };
  __property TXSDateTime*  Birthdate = { index=(IS_OPTN | IS_NLBL), read=FBirthdate, write=SetBirthdate, stored = Birthdate_Specified };
  __property UnicodeString IdPersonMis = { index=(IS_OPTN | IS_NLBL), read=FIdPersonMis, write=SetIdPersonMis, stored = IdPersonMis_Specified };
};




// ************************************************************************ //
// XML       : Person, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class Person : public Person2 {
private:
__published:
};




// ************************************************************************ //
// XML       : PersonWithIdentity, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class PersonWithIdentity2 : public Person2 {
private:
  ArrayOfIdentityDocument FDocuments;
  bool            FDocuments_Specified;
  void __fastcall SetDocuments(int Index, ArrayOfIdentityDocument _prop_val)
  {  FDocuments = _prop_val; FDocuments_Specified = true;  }
  bool __fastcall Documents_Specified(int Index)
  {  return FDocuments_Specified;  }

public:
  __fastcall ~PersonWithIdentity2();
__published:
  __property ArrayOfIdentityDocument  Documents = { index=(IS_OPTN | IS_NLBL), read=FDocuments, write=SetDocuments, stored = Documents_Specified };
};




// ************************************************************************ //
// XML       : PersonWithIdentity, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class PersonWithIdentity : public PersonWithIdentity2 {
private:
__published:
};




// ************************************************************************ //
// XML       : HumanName, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class HumanName2 : public TRemotable {
private:
  UnicodeString   FGivenName;
  bool            FGivenName_Specified;
  UnicodeString   FMiddleName;
  bool            FMiddleName_Specified;
  UnicodeString   FFamilyName;
  bool            FFamilyName_Specified;
  void __fastcall SetGivenName(int Index, UnicodeString _prop_val)
  {  FGivenName = _prop_val; FGivenName_Specified = true;  }
  bool __fastcall GivenName_Specified(int Index)
  {  return FGivenName_Specified;  }
  void __fastcall SetMiddleName(int Index, UnicodeString _prop_val)
  {  FMiddleName = _prop_val; FMiddleName_Specified = true;  }
  bool __fastcall MiddleName_Specified(int Index)
  {  return FMiddleName_Specified;  }
  void __fastcall SetFamilyName(int Index, UnicodeString _prop_val)
  {  FFamilyName = _prop_val; FFamilyName_Specified = true;  }
  bool __fastcall FamilyName_Specified(int Index)
  {  return FFamilyName_Specified;  }
__published:
  __property UnicodeString  GivenName = { index=(IS_OPTN | IS_NLBL), read=FGivenName, write=SetGivenName, stored = GivenName_Specified };
  __property UnicodeString MiddleName = { index=(IS_OPTN | IS_NLBL), read=FMiddleName, write=SetMiddleName, stored = MiddleName_Specified };
  __property UnicodeString FamilyName = { index=(IS_OPTN | IS_NLBL), read=FFamilyName, write=SetFamilyName, stored = FamilyName_Specified };
};




// ************************************************************************ //
// XML       : HumanName, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class HumanName : public HumanName2 {
private:
__published:
};




// ************************************************************************ //
// XML       : PacsResult.ImageData, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class PacsResult_ImageData2 : public TRemotable {
private:
  UnicodeString   FUid;
  bool            FUid_Specified;
  UnicodeString   FPacs;
  bool            FPacs_Specified;
  UnicodeString   FConclusion;
  bool            FConclusion_Specified;
  UnicodeString   FStatusReport;
  bool            FStatusReport_Specified;
  void __fastcall SetUid(int Index, UnicodeString _prop_val)
  {  FUid = _prop_val; FUid_Specified = true;  }
  bool __fastcall Uid_Specified(int Index)
  {  return FUid_Specified;  }
  void __fastcall SetPacs(int Index, UnicodeString _prop_val)
  {  FPacs = _prop_val; FPacs_Specified = true;  }
  bool __fastcall Pacs_Specified(int Index)
  {  return FPacs_Specified;  }
  void __fastcall SetConclusion(int Index, UnicodeString _prop_val)
  {  FConclusion = _prop_val; FConclusion_Specified = true;  }
  bool __fastcall Conclusion_Specified(int Index)
  {  return FConclusion_Specified;  }
  void __fastcall SetStatusReport(int Index, UnicodeString _prop_val)
  {  FStatusReport = _prop_val; FStatusReport_Specified = true;  }
  bool __fastcall StatusReport_Specified(int Index)
  {  return FStatusReport_Specified;  }
__published:
  __property UnicodeString        Uid = { index=(IS_OPTN | IS_NLBL), read=FUid, write=SetUid, stored = Uid_Specified };
  __property UnicodeString       Pacs = { index=(IS_OPTN | IS_NLBL), read=FPacs, write=SetPacs, stored = Pacs_Specified };
  __property UnicodeString Conclusion = { index=(IS_OPTN | IS_NLBL), read=FConclusion, write=SetConclusion, stored = Conclusion_Specified };
  __property UnicodeString StatusReport = { index=(IS_OPTN | IS_NLBL), read=FStatusReport, write=SetStatusReport, stored = StatusReport_Specified };
};




// ************************************************************************ //
// XML       : PacsResult.ImageData, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class PacsResult_ImageData : public PacsResult_ImageData2 {
private:
__published:
};




// ************************************************************************ //
// XML       : MedDocumentDto.DocumentAttachment, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class MedDocumentDto_DocumentAttachment2 : public TRemotable {
private:
  TByteDynArray   FData;
  bool            FData_Specified;
  TByteDynArray   FOrganizationSign;
  bool            FOrganizationSign_Specified;
  ArrayOfMedDocumentDto_PersonalSign FPersonalSigns;
  bool            FPersonalSigns_Specified;
  UnicodeString   FMimeType;
  bool            FMimeType_Specified;
  UnicodeString   FUrl;
  bool            FUrl_Specified;
  void __fastcall SetData(int Index, TByteDynArray _prop_val)
  {  FData = _prop_val; FData_Specified = true;  }
  bool __fastcall Data_Specified(int Index)
  {  return FData_Specified;  }
  void __fastcall SetOrganizationSign(int Index, TByteDynArray _prop_val)
  {  FOrganizationSign = _prop_val; FOrganizationSign_Specified = true;  }
  bool __fastcall OrganizationSign_Specified(int Index)
  {  return FOrganizationSign_Specified;  }
  void __fastcall SetPersonalSigns(int Index, ArrayOfMedDocumentDto_PersonalSign _prop_val)
  {  FPersonalSigns = _prop_val; FPersonalSigns_Specified = true;  }
  bool __fastcall PersonalSigns_Specified(int Index)
  {  return FPersonalSigns_Specified;  }
  void __fastcall SetMimeType(int Index, UnicodeString _prop_val)
  {  FMimeType = _prop_val; FMimeType_Specified = true;  }
  bool __fastcall MimeType_Specified(int Index)
  {  return FMimeType_Specified;  }
  void __fastcall SetUrl(int Index, UnicodeString _prop_val)
  {  FUrl = _prop_val; FUrl_Specified = true;  }
  bool __fastcall Url_Specified(int Index)
  {  return FUrl_Specified;  }

public:
  __fastcall ~MedDocumentDto_DocumentAttachment2();
__published:
  __property TByteDynArray       Data = { index=(IS_OPTN | IS_NLBL), read=FData, write=SetData, stored = Data_Specified };
  __property TByteDynArray OrganizationSign = { index=(IS_OPTN | IS_NLBL), read=FOrganizationSign, write=SetOrganizationSign, stored = OrganizationSign_Specified };
  __property ArrayOfMedDocumentDto_PersonalSign PersonalSigns = { index=(IS_OPTN | IS_NLBL), read=FPersonalSigns, write=SetPersonalSigns, stored = PersonalSigns_Specified };
  __property UnicodeString   MimeType = { index=(IS_OPTN | IS_NLBL), read=FMimeType, write=SetMimeType, stored = MimeType_Specified };
  __property UnicodeString        Url = { index=(IS_OPTN | IS_NLBL), read=FUrl, write=SetUrl, stored = Url_Specified };
};




// ************************************************************************ //
// XML       : MedDocumentDto.DocumentAttachment, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class MedDocumentDto_DocumentAttachment : public MedDocumentDto_DocumentAttachment2 {
private:
__published:
};




// ************************************************************************ //
// XML       : StringValue, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class StringValue2 : public ValueQuantity2 {
private:
  UnicodeString   FValue;
  bool            FValue_Specified;
  void __fastcall SetValue(int Index, UnicodeString _prop_val)
  {  FValue = _prop_val; FValue_Specified = true;  }
  bool __fastcall Value_Specified(int Index)
  {  return FValue_Specified;  }
__published:
  __property UnicodeString      Value = { index=(IS_OPTN | IS_NLBL), read=FValue, write=SetValue, stored = Value_Specified };
};




// ************************************************************************ //
// XML       : StringValue, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class StringValue : public StringValue2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Recommendation, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class Recommendation2 : public TRemotable {
private:
  TXSDateTime*    FDate;
  bool            FDate_Specified;
  MedicalStaff2*  FDoctor;
  bool            FDoctor_Specified;
  UnicodeString   FText;
  bool            FText_Specified;
  void __fastcall SetDate(int Index, TXSDateTime* _prop_val)
  {  FDate = _prop_val; FDate_Specified = true;  }
  bool __fastcall Date_Specified(int Index)
  {  return FDate_Specified;  }
  void __fastcall SetDoctor(int Index, MedicalStaff2* _prop_val)
  {  FDoctor = _prop_val; FDoctor_Specified = true;  }
  bool __fastcall Doctor_Specified(int Index)
  {  return FDoctor_Specified;  }
  void __fastcall SetText(int Index, UnicodeString _prop_val)
  {  FText = _prop_val; FText_Specified = true;  }
  bool __fastcall Text_Specified(int Index)
  {  return FText_Specified;  }

public:
  __fastcall ~Recommendation2();
__published:
  __property TXSDateTime*       Date = { index=(IS_OPTN), read=FDate, write=SetDate, stored = Date_Specified };
  __property MedicalStaff2*     Doctor = { index=(IS_OPTN | IS_NLBL), read=FDoctor, write=SetDoctor, stored = Doctor_Specified };
  __property UnicodeString       Text = { index=(IS_OPTN | IS_NLBL), read=FText, write=SetText, stored = Text_Specified };
};




// ************************************************************************ //
// XML       : Recommendation, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class Recommendation : public Recommendation2 {
private:
__published:
};




// ************************************************************************ //
// XML       : ReferralInfo, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class ReferralInfo2 : public TRemotable {
private:
  UnicodeString   FReason;
  bool            FReason_Specified;
  UnicodeString   FIdReferralMis;
  bool            FIdReferralMis_Specified;
  unsigned char   FIdReferralType;
  bool            FIdReferralType_Specified;
  TXSDateTime*    FIssuedDateTime;
  bool            FIssuedDateTime_Specified;
  unsigned char   FHospitalizationOrder;
  bool            FHospitalizationOrder_Specified;
  UnicodeString   FMkbCode;
  bool            FMkbCode_Specified;
  void __fastcall SetReason(int Index, UnicodeString _prop_val)
  {  FReason = _prop_val; FReason_Specified = true;  }
  bool __fastcall Reason_Specified(int Index)
  {  return FReason_Specified;  }
  void __fastcall SetIdReferralMis(int Index, UnicodeString _prop_val)
  {  FIdReferralMis = _prop_val; FIdReferralMis_Specified = true;  }
  bool __fastcall IdReferralMis_Specified(int Index)
  {  return FIdReferralMis_Specified;  }
  void __fastcall SetIdReferralType(int Index, unsigned char _prop_val)
  {  FIdReferralType = _prop_val; FIdReferralType_Specified = true;  }
  bool __fastcall IdReferralType_Specified(int Index)
  {  return FIdReferralType_Specified;  }
  void __fastcall SetIssuedDateTime(int Index, TXSDateTime* _prop_val)
  {  FIssuedDateTime = _prop_val; FIssuedDateTime_Specified = true;  }
  bool __fastcall IssuedDateTime_Specified(int Index)
  {  return FIssuedDateTime_Specified;  }
  void __fastcall SetHospitalizationOrder(int Index, unsigned char _prop_val)
  {  FHospitalizationOrder = _prop_val; FHospitalizationOrder_Specified = true;  }
  bool __fastcall HospitalizationOrder_Specified(int Index)
  {  return FHospitalizationOrder_Specified;  }
  void __fastcall SetMkbCode(int Index, UnicodeString _prop_val)
  {  FMkbCode = _prop_val; FMkbCode_Specified = true;  }
  bool __fastcall MkbCode_Specified(int Index)
  {  return FMkbCode_Specified;  }

public:
  __fastcall ~ReferralInfo2();
__published:
  __property UnicodeString     Reason = { index=(IS_OPTN | IS_NLBL), read=FReason, write=SetReason, stored = Reason_Specified };
  __property UnicodeString IdReferralMis = { index=(IS_OPTN | IS_NLBL), read=FIdReferralMis, write=SetIdReferralMis, stored = IdReferralMis_Specified };
  __property unsigned char IdReferralType = { index=(IS_OPTN), read=FIdReferralType, write=SetIdReferralType, stored = IdReferralType_Specified };
  __property TXSDateTime* IssuedDateTime = { index=(IS_OPTN), read=FIssuedDateTime, write=SetIssuedDateTime, stored = IssuedDateTime_Specified };
  __property unsigned char HospitalizationOrder = { index=(IS_OPTN), read=FHospitalizationOrder, write=SetHospitalizationOrder, stored = HospitalizationOrder_Specified };
  __property UnicodeString    MkbCode = { index=(IS_OPTN | IS_NLBL), read=FMkbCode, write=SetMkbCode, stored = MkbCode_Specified };
};




// ************************************************************************ //
// XML       : ReferralInfo, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class ReferralInfo : public ReferralInfo2 {
private:
__published:
};




// ************************************************************************ //
// XML       : AssisNote, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class AssisNote2 : public TRemotable {
private:
  TXSDateTime*    FAssisTimestamp;
  bool            FAssisTimestamp_Specified;
  UnicodeString   FMedAssistance;
  bool            FMedAssistance_Specified;
  UnicodeString   FEffect;
  bool            FEffect_Specified;
  void __fastcall SetAssisTimestamp(int Index, TXSDateTime* _prop_val)
  {  FAssisTimestamp = _prop_val; FAssisTimestamp_Specified = true;  }
  bool __fastcall AssisTimestamp_Specified(int Index)
  {  return FAssisTimestamp_Specified;  }
  void __fastcall SetMedAssistance(int Index, UnicodeString _prop_val)
  {  FMedAssistance = _prop_val; FMedAssistance_Specified = true;  }
  bool __fastcall MedAssistance_Specified(int Index)
  {  return FMedAssistance_Specified;  }
  void __fastcall SetEffect(int Index, UnicodeString _prop_val)
  {  FEffect = _prop_val; FEffect_Specified = true;  }
  bool __fastcall Effect_Specified(int Index)
  {  return FEffect_Specified;  }

public:
  __fastcall ~AssisNote2();
__published:
  __property TXSDateTime* AssisTimestamp = { index=(IS_OPTN), read=FAssisTimestamp, write=SetAssisTimestamp, stored = AssisTimestamp_Specified };
  __property UnicodeString MedAssistance = { index=(IS_OPTN | IS_NLBL), read=FMedAssistance, write=SetMedAssistance, stored = MedAssistance_Specified };
  __property UnicodeString     Effect = { index=(IS_OPTN | IS_NLBL), read=FEffect, write=SetEffect, stored = Effect_Specified };
};




// ************************************************************************ //
// XML       : AssisNote, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class AssisNote : public AssisNote2 {
private:
__published:
};




// ************************************************************************ //
// XML       : SickListInfo, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class SickListInfo2 : public TRemotable {
private:
  UnicodeString   FNumber;
  bool            FNumber_Specified;
  TXSDateTime*    FDateStart;
  bool            FDateStart_Specified;
  Guardian2*      FCaregiver;
  bool            FCaregiver_Specified;
  TXSDateTime*    FDateEnd;
  bool            FDateEnd_Specified;
  unsigned char   FDisabilityDocReason;
  bool            FDisabilityDocReason_Specified;
  unsigned char   FDisabilityDocState;
  bool            FDisabilityDocState_Specified;
  bool            FIsPatientTaker;
  bool            FIsPatientTaker_Specified;
  void __fastcall SetNumber(int Index, UnicodeString _prop_val)
  {  FNumber = _prop_val; FNumber_Specified = true;  }
  bool __fastcall Number_Specified(int Index)
  {  return FNumber_Specified;  }
  void __fastcall SetDateStart(int Index, TXSDateTime* _prop_val)
  {  FDateStart = _prop_val; FDateStart_Specified = true;  }
  bool __fastcall DateStart_Specified(int Index)
  {  return FDateStart_Specified;  }
  void __fastcall SetCaregiver(int Index, Guardian2* _prop_val)
  {  FCaregiver = _prop_val; FCaregiver_Specified = true;  }
  bool __fastcall Caregiver_Specified(int Index)
  {  return FCaregiver_Specified;  }
  void __fastcall SetDateEnd(int Index, TXSDateTime* _prop_val)
  {  FDateEnd = _prop_val; FDateEnd_Specified = true;  }
  bool __fastcall DateEnd_Specified(int Index)
  {  return FDateEnd_Specified;  }
  void __fastcall SetDisabilityDocReason(int Index, unsigned char _prop_val)
  {  FDisabilityDocReason = _prop_val; FDisabilityDocReason_Specified = true;  }
  bool __fastcall DisabilityDocReason_Specified(int Index)
  {  return FDisabilityDocReason_Specified;  }
  void __fastcall SetDisabilityDocState(int Index, unsigned char _prop_val)
  {  FDisabilityDocState = _prop_val; FDisabilityDocState_Specified = true;  }
  bool __fastcall DisabilityDocState_Specified(int Index)
  {  return FDisabilityDocState_Specified;  }
  void __fastcall SetIsPatientTaker(int Index, bool _prop_val)
  {  FIsPatientTaker = _prop_val; FIsPatientTaker_Specified = true;  }
  bool __fastcall IsPatientTaker_Specified(int Index)
  {  return FIsPatientTaker_Specified;  }

public:
  __fastcall ~SickListInfo2();
__published:
  __property UnicodeString     Number = { index=(IS_OPTN | IS_NLBL), read=FNumber, write=SetNumber, stored = Number_Specified };
  __property TXSDateTime*  DateStart = { index=(IS_OPTN), read=FDateStart, write=SetDateStart, stored = DateStart_Specified };
  __property Guardian2*  Caregiver = { index=(IS_OPTN | IS_NLBL), read=FCaregiver, write=SetCaregiver, stored = Caregiver_Specified };
  __property TXSDateTime*    DateEnd = { index=(IS_OPTN), read=FDateEnd, write=SetDateEnd, stored = DateEnd_Specified };
  __property unsigned char DisabilityDocReason = { index=(IS_OPTN), read=FDisabilityDocReason, write=SetDisabilityDocReason, stored = DisabilityDocReason_Specified };
  __property unsigned char DisabilityDocState = { index=(IS_OPTN), read=FDisabilityDocState, write=SetDisabilityDocState, stored = DisabilityDocState_Specified };
  __property bool       IsPatientTaker = { index=(IS_OPTN | IS_NLBL), read=FIsPatientTaker, write=SetIsPatientTaker, stored = IsPatientTaker_Specified };
};




// ************************************************************************ //
// XML       : SickListInfo, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class SickListInfo : public SickListInfo2 {
private:
__published:
};




// ************************************************************************ //
// XML       : VersionInfo, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.Shared.Core
// ************************************************************************ //
class VersionInfo2 : public TRemotable {
private:
  UnicodeString   FDatabaseVersion;
  bool            FDatabaseVersion_Specified;
  UnicodeString   FServiceVersion;
  bool            FServiceVersion_Specified;
  void __fastcall SetDatabaseVersion(int Index, UnicodeString _prop_val)
  {  FDatabaseVersion = _prop_val; FDatabaseVersion_Specified = true;  }
  bool __fastcall DatabaseVersion_Specified(int Index)
  {  return FDatabaseVersion_Specified;  }
  void __fastcall SetServiceVersion(int Index, UnicodeString _prop_val)
  {  FServiceVersion = _prop_val; FServiceVersion_Specified = true;  }
  bool __fastcall ServiceVersion_Specified(int Index)
  {  return FServiceVersion_Specified;  }
__published:
  __property UnicodeString DatabaseVersion = { index=(IS_OPTN | IS_NLBL), read=FDatabaseVersion, write=SetDatabaseVersion, stored = DatabaseVersion_Specified };
  __property UnicodeString ServiceVersion = { index=(IS_OPTN | IS_NLBL), read=FServiceVersion, write=SetServiceVersion, stored = ServiceVersion_Specified };
};




// ************************************************************************ //
// XML       : VersionInfo, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.Shared.Core
// ************************************************************************ //
class VersionInfo : public VersionInfo2 {
private:
__published:
};




// ************************************************************************ //
// XML       : MedicalStaff, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class MedicalStaff2 : public TRemotable {
private:
  PersonWithIdentity2* FPerson;
  bool            FPerson_Specified;
  UnicodeString   FIdLpu;
  bool            FIdLpu_Specified;
  unsigned short  FIdSpeciality;
  bool            FIdSpeciality_Specified;
  unsigned short  FIdPosition;
  bool            FIdPosition_Specified;
  int             FIdMedicalStaff;
  bool            FIdMedicalStaff_Specified;
  UnicodeString   FSpecialityName;
  bool            FSpecialityName_Specified;
  UnicodeString   FPositionName;
  bool            FPositionName_Specified;
  void __fastcall SetPerson(int Index, PersonWithIdentity2* _prop_val)
  {  FPerson = _prop_val; FPerson_Specified = true;  }
  bool __fastcall Person_Specified(int Index)
  {  return FPerson_Specified;  }
  void __fastcall SetIdLpu(int Index, UnicodeString _prop_val)
  {  FIdLpu = _prop_val; FIdLpu_Specified = true;  }
  bool __fastcall IdLpu_Specified(int Index)
  {  return FIdLpu_Specified;  }
  void __fastcall SetIdSpeciality(int Index, unsigned short _prop_val)
  {  FIdSpeciality = _prop_val; FIdSpeciality_Specified = true;  }
  bool __fastcall IdSpeciality_Specified(int Index)
  {  return FIdSpeciality_Specified;  }
  void __fastcall SetIdPosition(int Index, unsigned short _prop_val)
  {  FIdPosition = _prop_val; FIdPosition_Specified = true;  }
  bool __fastcall IdPosition_Specified(int Index)
  {  return FIdPosition_Specified;  }
  void __fastcall SetIdMedicalStaff(int Index, int _prop_val)
  {  FIdMedicalStaff = _prop_val; FIdMedicalStaff_Specified = true;  }
  bool __fastcall IdMedicalStaff_Specified(int Index)
  {  return FIdMedicalStaff_Specified;  }
  void __fastcall SetSpecialityName(int Index, UnicodeString _prop_val)
  {  FSpecialityName = _prop_val; FSpecialityName_Specified = true;  }
  bool __fastcall SpecialityName_Specified(int Index)
  {  return FSpecialityName_Specified;  }
  void __fastcall SetPositionName(int Index, UnicodeString _prop_val)
  {  FPositionName = _prop_val; FPositionName_Specified = true;  }
  bool __fastcall PositionName_Specified(int Index)
  {  return FPositionName_Specified;  }

public:
  __fastcall ~MedicalStaff2();
__published:
  __property PersonWithIdentity2*     Person = { index=(IS_OPTN | IS_NLBL), read=FPerson, write=SetPerson, stored = Person_Specified };
  __property UnicodeString      IdLpu = { index=(IS_OPTN | IS_NLBL), read=FIdLpu, write=SetIdLpu, stored = IdLpu_Specified };
  __property unsigned short IdSpeciality = { index=(IS_OPTN), read=FIdSpeciality, write=SetIdSpeciality, stored = IdSpeciality_Specified };
  __property unsigned short IdPosition = { index=(IS_OPTN), read=FIdPosition, write=SetIdPosition, stored = IdPosition_Specified };
  __property int        IdMedicalStaff = { index=(IS_OPTN), read=FIdMedicalStaff, write=SetIdMedicalStaff, stored = IdMedicalStaff_Specified };
  __property UnicodeString SpecialityName = { index=(IS_OPTN | IS_NLBL), read=FSpecialityName, write=SetSpecialityName, stored = SpecialityName_Specified };
  __property UnicodeString PositionName = { index=(IS_OPTN | IS_NLBL), read=FPositionName, write=SetPositionName, stored = PositionName_Specified };
};




// ************************************************************************ //
// XML       : MedicalStaff, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class MedicalStaff : public MedicalStaff2 {
private:
__published:
};




// ************************************************************************ //
// XML       : IdentityDocument, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class IdentityDocument2 : public TRemotable {
private:
  UnicodeString   FDocN;
  bool            FDocN_Specified;
  UnicodeString   FDocS;
  bool            FDocS_Specified;
  UnicodeString   FDocumentName;
  bool            FDocumentName_Specified;
  TXSDateTime*    FExpiredDate;
  bool            FExpiredDate_Specified;
  unsigned char   FIdDocumentType;
  bool            FIdDocumentType_Specified;
  int             FIdProvider;
  bool            FIdProvider_Specified;
  TXSDateTime*    FIssuedDate;
  bool            FIssuedDate_Specified;
  UnicodeString   FProviderName;
  bool            FProviderName_Specified;
  UnicodeString   FRegionCode;
  bool            FRegionCode_Specified;
  TXSDateTime*    FStartDate;
  bool            FStartDate_Specified;
  void __fastcall SetDocN(int Index, UnicodeString _prop_val)
  {  FDocN = _prop_val; FDocN_Specified = true;  }
  bool __fastcall DocN_Specified(int Index)
  {  return FDocN_Specified;  }
  void __fastcall SetDocS(int Index, UnicodeString _prop_val)
  {  FDocS = _prop_val; FDocS_Specified = true;  }
  bool __fastcall DocS_Specified(int Index)
  {  return FDocS_Specified;  }
  void __fastcall SetDocumentName(int Index, UnicodeString _prop_val)
  {  FDocumentName = _prop_val; FDocumentName_Specified = true;  }
  bool __fastcall DocumentName_Specified(int Index)
  {  return FDocumentName_Specified;  }
  void __fastcall SetExpiredDate(int Index, TXSDateTime* _prop_val)
  {  FExpiredDate = _prop_val; FExpiredDate_Specified = true;  }
  bool __fastcall ExpiredDate_Specified(int Index)
  {  return FExpiredDate_Specified;  }
  void __fastcall SetIdDocumentType(int Index, unsigned char _prop_val)
  {  FIdDocumentType = _prop_val; FIdDocumentType_Specified = true;  }
  bool __fastcall IdDocumentType_Specified(int Index)
  {  return FIdDocumentType_Specified;  }
  void __fastcall SetIdProvider(int Index, int _prop_val)
  {  FIdProvider = _prop_val; FIdProvider_Specified = true;  }
  bool __fastcall IdProvider_Specified(int Index)
  {  return FIdProvider_Specified;  }
  void __fastcall SetIssuedDate(int Index, TXSDateTime* _prop_val)
  {  FIssuedDate = _prop_val; FIssuedDate_Specified = true;  }
  bool __fastcall IssuedDate_Specified(int Index)
  {  return FIssuedDate_Specified;  }
  void __fastcall SetProviderName(int Index, UnicodeString _prop_val)
  {  FProviderName = _prop_val; FProviderName_Specified = true;  }
  bool __fastcall ProviderName_Specified(int Index)
  {  return FProviderName_Specified;  }
  void __fastcall SetRegionCode(int Index, UnicodeString _prop_val)
  {  FRegionCode = _prop_val; FRegionCode_Specified = true;  }
  bool __fastcall RegionCode_Specified(int Index)
  {  return FRegionCode_Specified;  }
  void __fastcall SetStartDate(int Index, TXSDateTime* _prop_val)
  {  FStartDate = _prop_val; FStartDate_Specified = true;  }
  bool __fastcall StartDate_Specified(int Index)
  {  return FStartDate_Specified;  }

public:
  __fastcall ~IdentityDocument2();
__published:
  __property UnicodeString       DocN = { index=(IS_OPTN | IS_NLBL), read=FDocN, write=SetDocN, stored = DocN_Specified };
  __property UnicodeString       DocS = { index=(IS_OPTN | IS_NLBL), read=FDocS, write=SetDocS, stored = DocS_Specified };
  __property UnicodeString DocumentName = { index=(IS_OPTN | IS_NLBL), read=FDocumentName, write=SetDocumentName, stored = DocumentName_Specified };
  __property TXSDateTime* ExpiredDate = { index=(IS_OPTN | IS_NLBL), read=FExpiredDate, write=SetExpiredDate, stored = ExpiredDate_Specified };
  __property unsigned char IdDocumentType = { index=(IS_OPTN), read=FIdDocumentType, write=SetIdDocumentType, stored = IdDocumentType_Specified };
  __property int        IdProvider = { index=(IS_OPTN | IS_NLBL), read=FIdProvider, write=SetIdProvider, stored = IdProvider_Specified };
  __property TXSDateTime* IssuedDate = { index=(IS_OPTN | IS_NLBL), read=FIssuedDate, write=SetIssuedDate, stored = IssuedDate_Specified };
  __property UnicodeString ProviderName = { index=(IS_OPTN | IS_NLBL), read=FProviderName, write=SetProviderName, stored = ProviderName_Specified };
  __property UnicodeString RegionCode = { index=(IS_OPTN | IS_NLBL), read=FRegionCode, write=SetRegionCode, stored = RegionCode_Specified };
  __property TXSDateTime*  StartDate = { index=(IS_OPTN | IS_NLBL), read=FStartDate, write=SetStartDate, stored = StartDate_Specified };
};




// ************************************************************************ //
// XML       : IdentityDocument, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class IdentityDocument : public IdentityDocument2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Quantity, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class Quantity2 : public TRemotable {
private:
  int             FIdUnit;
  bool            FIdUnit_Specified;
  TXSDecimal*     FValue;
  bool            FValue_Specified;
  void __fastcall SetIdUnit(int Index, int _prop_val)
  {  FIdUnit = _prop_val; FIdUnit_Specified = true;  }
  bool __fastcall IdUnit_Specified(int Index)
  {  return FIdUnit_Specified;  }
  void __fastcall SetValue(int Index, TXSDecimal* _prop_val)
  {  FValue = _prop_val; FValue_Specified = true;  }
  bool __fastcall Value_Specified(int Index)
  {  return FValue_Specified;  }

public:
  __fastcall ~Quantity2();
__published:
  __property int            IdUnit = { index=(IS_OPTN), read=FIdUnit, write=SetIdUnit, stored = IdUnit_Specified };
  __property TXSDecimal*      Value = { index=(IS_OPTN), read=FValue, write=SetValue, stored = Value_Specified };
};




// ************************************************************************ //
// XML       : Quantity, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class Quantity : public Quantity2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Param, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Param2 : public TRemotable {
private:
  int             FCode;
  bool            FCode_Specified;
  UnicodeString   FValue;
  bool            FValue_Specified;
  void __fastcall SetCode(int Index, int _prop_val)
  {  FCode = _prop_val; FCode_Specified = true;  }
  bool __fastcall Code_Specified(int Index)
  {  return FCode_Specified;  }
  void __fastcall SetValue(int Index, UnicodeString _prop_val)
  {  FValue = _prop_val; FValue_Specified = true;  }
  bool __fastcall Value_Specified(int Index)
  {  return FValue_Specified;  }
__published:
  __property int              Code = { index=(IS_OPTN), read=FCode, write=SetCode, stored = Code_Specified };
  __property UnicodeString      Value = { index=(IS_OPTN | IS_NLBL), read=FValue, write=SetValue, stored = Value_Specified };
};




// ************************************************************************ //
// XML       : RequestFault, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common
// Info      : Fault
// Base Types: RequestFault
// ************************************************************************ //
class RequestFault : public ERemotableException {
private:
  UnicodeString   FPropertyName;
  bool            FPropertyName_Specified;
  UnicodeString   FMessage;
  bool            FMessage_Specified;
  int             FErrorCode;
  bool            FErrorCode_Specified;
  ArrayOfRequestFault2 FErrors;
  bool            FErrors_Specified;
  void __fastcall SetPropertyName(int Index, UnicodeString _prop_val)
  {  FPropertyName = _prop_val; FPropertyName_Specified = true;  }
  bool __fastcall PropertyName_Specified(int Index)
  {  return FPropertyName_Specified;  }
  void __fastcall SetMessage(int Index, UnicodeString _prop_val)
  {  FMessage = _prop_val; FMessage_Specified = true;  }
  bool __fastcall Message_Specified(int Index)
  {  return FMessage_Specified;  }
  void __fastcall SetErrorCode(int Index, int _prop_val)
  {  FErrorCode = _prop_val; FErrorCode_Specified = true;  }
  bool __fastcall ErrorCode_Specified(int Index)
  {  return FErrorCode_Specified;  }
  void __fastcall SetErrors(int Index, ArrayOfRequestFault2 _prop_val)
  {  FErrors = _prop_val; FErrors_Specified = true;  }
  bool __fastcall Errors_Specified(int Index)
  {  return FErrors_Specified;  }

public:
  __fastcall ~RequestFault();
__published:
  __property UnicodeString PropertyName = { index=(IS_OPTN | IS_NLBL), read=FPropertyName, write=SetPropertyName, stored = PropertyName_Specified };
  __property UnicodeString    Message = { index=(IS_OPTN | IS_NLBL), read=FMessage, write=SetMessage, stored = Message_Specified };
  __property int         ErrorCode = { index=(IS_OPTN), read=FErrorCode, write=SetErrorCode, stored = ErrorCode_Specified };
  __property ArrayOfRequestFault2     Errors = { index=(IS_OPTN | IS_NLBL), read=FErrors, write=SetErrors, stored = Errors_Specified };
};




// ************************************************************************ //
// XML       : RequestFault, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common
// ************************************************************************ //
class RequestFault2 : public TRemotable {
private:
  UnicodeString   FPropertyName;
  bool            FPropertyName_Specified;
  UnicodeString   FMessage;
  bool            FMessage_Specified;
  int             FErrorCode;
  bool            FErrorCode_Specified;
  ArrayOfRequestFault2 FErrors;
  bool            FErrors_Specified;
  void __fastcall SetPropertyName(int Index, UnicodeString _prop_val)
  {  FPropertyName = _prop_val; FPropertyName_Specified = true;  }
  bool __fastcall PropertyName_Specified(int Index)
  {  return FPropertyName_Specified;  }
  void __fastcall SetMessage(int Index, UnicodeString _prop_val)
  {  FMessage = _prop_val; FMessage_Specified = true;  }
  bool __fastcall Message_Specified(int Index)
  {  return FMessage_Specified;  }
  void __fastcall SetErrorCode(int Index, int _prop_val)
  {  FErrorCode = _prop_val; FErrorCode_Specified = true;  }
  bool __fastcall ErrorCode_Specified(int Index)
  {  return FErrorCode_Specified;  }
  void __fastcall SetErrors(int Index, ArrayOfRequestFault2 _prop_val)
  {  FErrors = _prop_val; FErrors_Specified = true;  }
  bool __fastcall Errors_Specified(int Index)
  {  return FErrors_Specified;  }

public:
  __fastcall ~RequestFault2();
__published:
  __property UnicodeString PropertyName = { index=(IS_OPTN | IS_NLBL), read=FPropertyName, write=SetPropertyName, stored = PropertyName_Specified };
  __property UnicodeString    Message = { index=(IS_OPTN | IS_NLBL), read=FMessage, write=SetMessage, stored = Message_Specified };
  __property int         ErrorCode = { index=(IS_OPTN), read=FErrorCode, write=SetErrorCode, stored = ErrorCode_Specified };
  __property ArrayOfRequestFault2     Errors = { index=(IS_OPTN | IS_NLBL), read=FErrors, write=SetErrors, stored = Errors_Specified };
};




// ************************************************************************ //
// XML       : Observation, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class Observation2 : public TRemotable {
private:
  int             FCode;
  bool            FCode_Specified;
  ValueQuantity2* FValueQuantity;
  bool            FValueQuantity_Specified;
  TXSDateTime*    FDateTime;
  bool            FDateTime_Specified;
  UnicodeString   FInterpretation;
  bool            FInterpretation_Specified;
  ArrayOfReferenceRange FReferenceRanges;
  bool            FReferenceRanges_Specified;
  void __fastcall SetCode(int Index, int _prop_val)
  {  FCode = _prop_val; FCode_Specified = true;  }
  bool __fastcall Code_Specified(int Index)
  {  return FCode_Specified;  }
  void __fastcall SetValueQuantity(int Index, ValueQuantity2* _prop_val)
  {  FValueQuantity = _prop_val; FValueQuantity_Specified = true;  }
  bool __fastcall ValueQuantity_Specified(int Index)
  {  return FValueQuantity_Specified;  }
  void __fastcall SetDateTime(int Index, TXSDateTime* _prop_val)
  {  FDateTime = _prop_val; FDateTime_Specified = true;  }
  bool __fastcall DateTime_Specified(int Index)
  {  return FDateTime_Specified;  }
  void __fastcall SetInterpretation(int Index, UnicodeString _prop_val)
  {  FInterpretation = _prop_val; FInterpretation_Specified = true;  }
  bool __fastcall Interpretation_Specified(int Index)
  {  return FInterpretation_Specified;  }
  void __fastcall SetReferenceRanges(int Index, ArrayOfReferenceRange _prop_val)
  {  FReferenceRanges = _prop_val; FReferenceRanges_Specified = true;  }
  bool __fastcall ReferenceRanges_Specified(int Index)
  {  return FReferenceRanges_Specified;  }

public:
  __fastcall ~Observation2();
__published:
  __property int              Code = { index=(IS_OPTN), read=FCode, write=SetCode, stored = Code_Specified };
  __property ValueQuantity2* ValueQuantity = { index=(IS_OPTN | IS_NLBL), read=FValueQuantity, write=SetValueQuantity, stored = ValueQuantity_Specified };
  __property TXSDateTime*   DateTime = { index=(IS_OPTN), read=FDateTime, write=SetDateTime, stored = DateTime_Specified };
  __property UnicodeString Interpretation = { index=(IS_OPTN | IS_NLBL), read=FInterpretation, write=SetInterpretation, stored = Interpretation_Specified };
  __property ArrayOfReferenceRange ReferenceRanges = { index=(IS_OPTN | IS_NLBL), read=FReferenceRanges, write=SetReferenceRanges, stored = ReferenceRanges_Specified };
};




// ************************************************************************ //
// XML       : Observation, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class Observation : public Observation2 {
private:
__published:
};




// ************************************************************************ //
// XML       : ReferenceRange, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class ReferenceRange2 : public TRemotable {
private:
  int             FRangeType;
  bool            FRangeType_Specified;
  int             FIdUnit;
  bool            FIdUnit_Specified;
  UnicodeString   FValue;
  bool            FValue_Specified;
  void __fastcall SetRangeType(int Index, int _prop_val)
  {  FRangeType = _prop_val; FRangeType_Specified = true;  }
  bool __fastcall RangeType_Specified(int Index)
  {  return FRangeType_Specified;  }
  void __fastcall SetIdUnit(int Index, int _prop_val)
  {  FIdUnit = _prop_val; FIdUnit_Specified = true;  }
  bool __fastcall IdUnit_Specified(int Index)
  {  return FIdUnit_Specified;  }
  void __fastcall SetValue(int Index, UnicodeString _prop_val)
  {  FValue = _prop_val; FValue_Specified = true;  }
  bool __fastcall Value_Specified(int Index)
  {  return FValue_Specified;  }
__published:
  __property int         RangeType = { index=(IS_OPTN), read=FRangeType, write=SetRangeType, stored = RangeType_Specified };
  __property int            IdUnit = { index=(IS_OPTN | IS_NLBL), read=FIdUnit, write=SetIdUnit, stored = IdUnit_Specified };
  __property UnicodeString      Value = { index=(IS_OPTN | IS_NLBL), read=FValue, write=SetValue, stored = Value_Specified };
};




// ************************************************************************ //
// XML       : ReferenceRange, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class ReferenceRange : public ReferenceRange2 {
private:
__published:
};




// ************************************************************************ //
// XML       : CodeableValue, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class CodeableValue2 : public ValueQuantity2 {
private:
  int             FIdUnit;
  bool            FIdUnit_Specified;
  UnicodeString   FValueSystem;
  bool            FValueSystem_Specified;
  UnicodeString   FValue;
  bool            FValue_Specified;
  void __fastcall SetIdUnit(int Index, int _prop_val)
  {  FIdUnit = _prop_val; FIdUnit_Specified = true;  }
  bool __fastcall IdUnit_Specified(int Index)
  {  return FIdUnit_Specified;  }
  void __fastcall SetValueSystem(int Index, UnicodeString _prop_val)
  {  FValueSystem = _prop_val; FValueSystem_Specified = true;  }
  bool __fastcall ValueSystem_Specified(int Index)
  {  return FValueSystem_Specified;  }
  void __fastcall SetValue(int Index, UnicodeString _prop_val)
  {  FValue = _prop_val; FValue_Specified = true;  }
  bool __fastcall Value_Specified(int Index)
  {  return FValue_Specified;  }
__published:
  __property int            IdUnit = { index=(IS_OPTN | IS_NLBL), read=FIdUnit, write=SetIdUnit, stored = IdUnit_Specified };
  __property UnicodeString ValueSystem = { index=(IS_OPTN | IS_NLBL), read=FValueSystem, write=SetValueSystem, stored = ValueSystem_Specified };
  __property UnicodeString      Value = { index=(IS_OPTN | IS_NLBL), read=FValue, write=SetValue, stored = Value_Specified };
};




// ************************************************************************ //
// XML       : CodeableValue, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class CodeableValue : public CodeableValue2 {
private:
__published:
};




// ************************************************************************ //
// XML       : NumericalValue, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class NumericalValue2 : public ValueQuantity2 {
private:
  int             FIdUnit;
  bool            FIdUnit_Specified;
  int             FValue;
  bool            FValue_Specified;
  void __fastcall SetIdUnit(int Index, int _prop_val)
  {  FIdUnit = _prop_val; FIdUnit_Specified = true;  }
  bool __fastcall IdUnit_Specified(int Index)
  {  return FIdUnit_Specified;  }
  void __fastcall SetValue(int Index, int _prop_val)
  {  FValue = _prop_val; FValue_Specified = true;  }
  bool __fastcall Value_Specified(int Index)
  {  return FValue_Specified;  }
__published:
  __property int            IdUnit = { index=(IS_OPTN | IS_NLBL), read=FIdUnit, write=SetIdUnit, stored = IdUnit_Specified };
  __property int             Value = { index=(IS_OPTN), read=FValue, write=SetValue, stored = Value_Specified };
};




// ************************************************************************ //
// XML       : NumericalValue, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class NumericalValue : public NumericalValue2 {
private:
__published:
};




// ************************************************************************ //
// XML       : DiagnosisInfo, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag
// ************************************************************************ //
class DiagnosisInfo2 : public TRemotable {
private:
  unsigned char   FIdDiseaseType;
  bool            FIdDiseaseType_Specified;
  TXSDateTime*    FDiagnosedDate;
  bool            FDiagnosedDate_Specified;
  unsigned char   FIdDiagnosisType;
  bool            FIdDiagnosisType_Specified;
  UnicodeString   FComment;
  bool            FComment_Specified;
  unsigned char   FDiagnosisChangeReason;
  bool            FDiagnosisChangeReason_Specified;
  unsigned char   FDiagnosisStage;
  bool            FDiagnosisStage_Specified;
  unsigned char   FIdDispensaryState;
  bool            FIdDispensaryState_Specified;
  unsigned char   FIdTraumaType;
  bool            FIdTraumaType_Specified;
  unsigned char   FMESImplementationFeature;
  bool            FMESImplementationFeature_Specified;
  int             FMedicalStandard;
  bool            FMedicalStandard_Specified;
  ArrayOfParam    FParams;
  bool            FParams_Specified;
  UnicodeString   FMkbCode;
  bool            FMkbCode_Specified;
  unsigned char   FDiseaseStatus;
  bool            FDiseaseStatus_Specified;
  UnicodeString   FMkbCodeChange;
  bool            FMkbCodeChange_Specified;
  void __fastcall SetIdDiseaseType(int Index, unsigned char _prop_val)
  {  FIdDiseaseType = _prop_val; FIdDiseaseType_Specified = true;  }
  bool __fastcall IdDiseaseType_Specified(int Index)
  {  return FIdDiseaseType_Specified;  }
  void __fastcall SetDiagnosedDate(int Index, TXSDateTime* _prop_val)
  {  FDiagnosedDate = _prop_val; FDiagnosedDate_Specified = true;  }
  bool __fastcall DiagnosedDate_Specified(int Index)
  {  return FDiagnosedDate_Specified;  }
  void __fastcall SetIdDiagnosisType(int Index, unsigned char _prop_val)
  {  FIdDiagnosisType = _prop_val; FIdDiagnosisType_Specified = true;  }
  bool __fastcall IdDiagnosisType_Specified(int Index)
  {  return FIdDiagnosisType_Specified;  }
  void __fastcall SetComment(int Index, UnicodeString _prop_val)
  {  FComment = _prop_val; FComment_Specified = true;  }
  bool __fastcall Comment_Specified(int Index)
  {  return FComment_Specified;  }
  void __fastcall SetDiagnosisChangeReason(int Index, unsigned char _prop_val)
  {  FDiagnosisChangeReason = _prop_val; FDiagnosisChangeReason_Specified = true;  }
  bool __fastcall DiagnosisChangeReason_Specified(int Index)
  {  return FDiagnosisChangeReason_Specified;  }
  void __fastcall SetDiagnosisStage(int Index, unsigned char _prop_val)
  {  FDiagnosisStage = _prop_val; FDiagnosisStage_Specified = true;  }
  bool __fastcall DiagnosisStage_Specified(int Index)
  {  return FDiagnosisStage_Specified;  }
  void __fastcall SetIdDispensaryState(int Index, unsigned char _prop_val)
  {  FIdDispensaryState = _prop_val; FIdDispensaryState_Specified = true;  }
  bool __fastcall IdDispensaryState_Specified(int Index)
  {  return FIdDispensaryState_Specified;  }
  void __fastcall SetIdTraumaType(int Index, unsigned char _prop_val)
  {  FIdTraumaType = _prop_val; FIdTraumaType_Specified = true;  }
  bool __fastcall IdTraumaType_Specified(int Index)
  {  return FIdTraumaType_Specified;  }
  void __fastcall SetMESImplementationFeature(int Index, unsigned char _prop_val)
  {  FMESImplementationFeature = _prop_val; FMESImplementationFeature_Specified = true;  }
  bool __fastcall MESImplementationFeature_Specified(int Index)
  {  return FMESImplementationFeature_Specified;  }
  void __fastcall SetMedicalStandard(int Index, int _prop_val)
  {  FMedicalStandard = _prop_val; FMedicalStandard_Specified = true;  }
  bool __fastcall MedicalStandard_Specified(int Index)
  {  return FMedicalStandard_Specified;  }
  void __fastcall SetParams(int Index, ArrayOfParam _prop_val)
  {  FParams = _prop_val; FParams_Specified = true;  }
  bool __fastcall Params_Specified(int Index)
  {  return FParams_Specified;  }
  void __fastcall SetMkbCode(int Index, UnicodeString _prop_val)
  {  FMkbCode = _prop_val; FMkbCode_Specified = true;  }
  bool __fastcall MkbCode_Specified(int Index)
  {  return FMkbCode_Specified;  }
  void __fastcall SetDiseaseStatus(int Index, unsigned char _prop_val)
  {  FDiseaseStatus = _prop_val; FDiseaseStatus_Specified = true;  }
  bool __fastcall DiseaseStatus_Specified(int Index)
  {  return FDiseaseStatus_Specified;  }
  void __fastcall SetMkbCodeChange(int Index, UnicodeString _prop_val)
  {  FMkbCodeChange = _prop_val; FMkbCodeChange_Specified = true;  }
  bool __fastcall MkbCodeChange_Specified(int Index)
  {  return FMkbCodeChange_Specified;  }

public:
  __fastcall ~DiagnosisInfo2();
__published:
  __property unsigned char IdDiseaseType = { index=(IS_OPTN), read=FIdDiseaseType, write=SetIdDiseaseType, stored = IdDiseaseType_Specified };
  __property TXSDateTime* DiagnosedDate = { index=(IS_OPTN), read=FDiagnosedDate, write=SetDiagnosedDate, stored = DiagnosedDate_Specified };
  __property unsigned char IdDiagnosisType = { index=(IS_OPTN), read=FIdDiagnosisType, write=SetIdDiagnosisType, stored = IdDiagnosisType_Specified };
  __property UnicodeString    Comment = { index=(IS_OPTN | IS_NLBL), read=FComment, write=SetComment, stored = Comment_Specified };
  __property unsigned char DiagnosisChangeReason = { index=(IS_OPTN), read=FDiagnosisChangeReason, write=SetDiagnosisChangeReason, stored = DiagnosisChangeReason_Specified };
  __property unsigned char DiagnosisStage = { index=(IS_OPTN), read=FDiagnosisStage, write=SetDiagnosisStage, stored = DiagnosisStage_Specified };
  __property unsigned char IdDispensaryState = { index=(IS_OPTN), read=FIdDispensaryState, write=SetIdDispensaryState, stored = IdDispensaryState_Specified };
  __property unsigned char IdTraumaType = { index=(IS_OPTN), read=FIdTraumaType, write=SetIdTraumaType, stored = IdTraumaType_Specified };
  __property unsigned char MESImplementationFeature = { index=(IS_OPTN), read=FMESImplementationFeature, write=SetMESImplementationFeature, stored = MESImplementationFeature_Specified };
  __property int        MedicalStandard = { index=(IS_OPTN), read=FMedicalStandard, write=SetMedicalStandard, stored = MedicalStandard_Specified };
  __property ArrayOfParam     Params = { index=(IS_OPTN | IS_NLBL), read=FParams, write=SetParams, stored = Params_Specified };
  __property UnicodeString    MkbCode = { index=(IS_OPTN | IS_NLBL), read=FMkbCode, write=SetMkbCode, stored = MkbCode_Specified };
  __property unsigned char DiseaseStatus = { index=(IS_OPTN | IS_NLBL), read=FDiseaseStatus, write=SetDiseaseStatus, stored = DiseaseStatus_Specified };
  __property UnicodeString MkbCodeChange = { index=(IS_OPTN | IS_NLBL), read=FMkbCodeChange, write=SetMkbCodeChange, stored = MkbCodeChange_Specified };
};




// ************************************************************************ //
// XML       : DiagnosisInfo, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag
// ************************************************************************ //
class DiagnosisInfo : public DiagnosisInfo2 {
private:
__published:
};




// ************************************************************************ //
// XML       : StepBase, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step
// ************************************************************************ //
class StepBase2 : public TRemotable {
private:
  TXSDateTime*    FDateStart;
  bool            FDateStart_Specified;
  TXSDateTime*    FDateEnd;
  bool            FDateEnd_Specified;
  UnicodeString   FComment;
  bool            FComment_Specified;
  unsigned char   FIdPaymentType;
  bool            FIdPaymentType_Specified;
  MedicalStaff2*  FDoctor;
  bool            FDoctor_Specified;
  UnicodeString   FIdStepMis;
  bool            FIdStepMis_Specified;
  void __fastcall SetDateStart(int Index, TXSDateTime* _prop_val)
  {  FDateStart = _prop_val; FDateStart_Specified = true;  }
  bool __fastcall DateStart_Specified(int Index)
  {  return FDateStart_Specified;  }
  void __fastcall SetDateEnd(int Index, TXSDateTime* _prop_val)
  {  FDateEnd = _prop_val; FDateEnd_Specified = true;  }
  bool __fastcall DateEnd_Specified(int Index)
  {  return FDateEnd_Specified;  }
  void __fastcall SetComment(int Index, UnicodeString _prop_val)
  {  FComment = _prop_val; FComment_Specified = true;  }
  bool __fastcall Comment_Specified(int Index)
  {  return FComment_Specified;  }
  void __fastcall SetIdPaymentType(int Index, unsigned char _prop_val)
  {  FIdPaymentType = _prop_val; FIdPaymentType_Specified = true;  }
  bool __fastcall IdPaymentType_Specified(int Index)
  {  return FIdPaymentType_Specified;  }
  void __fastcall SetDoctor(int Index, MedicalStaff2* _prop_val)
  {  FDoctor = _prop_val; FDoctor_Specified = true;  }
  bool __fastcall Doctor_Specified(int Index)
  {  return FDoctor_Specified;  }
  void __fastcall SetIdStepMis(int Index, UnicodeString _prop_val)
  {  FIdStepMis = _prop_val; FIdStepMis_Specified = true;  }
  bool __fastcall IdStepMis_Specified(int Index)
  {  return FIdStepMis_Specified;  }

public:
  __fastcall ~StepBase2();
__published:
  __property TXSDateTime*  DateStart = { index=(IS_OPTN), read=FDateStart, write=SetDateStart, stored = DateStart_Specified };
  __property TXSDateTime*    DateEnd = { index=(IS_OPTN), read=FDateEnd, write=SetDateEnd, stored = DateEnd_Specified };
  __property UnicodeString    Comment = { index=(IS_OPTN | IS_NLBL), read=FComment, write=SetComment, stored = Comment_Specified };
  __property unsigned char IdPaymentType = { index=(IS_OPTN), read=FIdPaymentType, write=SetIdPaymentType, stored = IdPaymentType_Specified };
  __property MedicalStaff2*     Doctor = { index=(IS_OPTN | IS_NLBL), read=FDoctor, write=SetDoctor, stored = Doctor_Specified };
  __property UnicodeString  IdStepMis = { index=(IS_OPTN | IS_NLBL), read=FIdStepMis, write=SetIdStepMis, stored = IdStepMis_Specified };
};




// ************************************************************************ //
// XML       : StepBase, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step
// ************************************************************************ //
class StepBase : public StepBase2 {
private:
__published:
};




// ************************************************************************ //
// XML       : MedRecord, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class MedRecord2 : public TRemotable {
private:
__published:
};




// ************************************************************************ //
// XML       : Problem, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Problem2 : public MedRecord2 {
private:
  UnicodeString   FIdProblemMis;
  bool            FIdProblemMis_Specified;
  UnicodeString   FDiagnosisMkb;
  bool            FDiagnosisMkb_Specified;
  TXSDateTime*    FDate;
  bool            FDate_Specified;
  MedicalStaff2*  FPerformer;
  bool            FPerformer_Specified;
  UnicodeString   FComment;
  bool            FComment_Specified;
  bool            FStatus;
  bool            FStatus_Specified;
  void __fastcall SetIdProblemMis(int Index, UnicodeString _prop_val)
  {  FIdProblemMis = _prop_val; FIdProblemMis_Specified = true;  }
  bool __fastcall IdProblemMis_Specified(int Index)
  {  return FIdProblemMis_Specified;  }
  void __fastcall SetDiagnosisMkb(int Index, UnicodeString _prop_val)
  {  FDiagnosisMkb = _prop_val; FDiagnosisMkb_Specified = true;  }
  bool __fastcall DiagnosisMkb_Specified(int Index)
  {  return FDiagnosisMkb_Specified;  }
  void __fastcall SetDate(int Index, TXSDateTime* _prop_val)
  {  FDate = _prop_val; FDate_Specified = true;  }
  bool __fastcall Date_Specified(int Index)
  {  return FDate_Specified;  }
  void __fastcall SetPerformer(int Index, MedicalStaff2* _prop_val)
  {  FPerformer = _prop_val; FPerformer_Specified = true;  }
  bool __fastcall Performer_Specified(int Index)
  {  return FPerformer_Specified;  }
  void __fastcall SetComment(int Index, UnicodeString _prop_val)
  {  FComment = _prop_val; FComment_Specified = true;  }
  bool __fastcall Comment_Specified(int Index)
  {  return FComment_Specified;  }
  void __fastcall SetStatus(int Index, bool _prop_val)
  {  FStatus = _prop_val; FStatus_Specified = true;  }
  bool __fastcall Status_Specified(int Index)
  {  return FStatus_Specified;  }

public:
  __fastcall ~Problem2();
__published:
  __property UnicodeString IdProblemMis = { index=(IS_OPTN | IS_NLBL), read=FIdProblemMis, write=SetIdProblemMis, stored = IdProblemMis_Specified };
  __property UnicodeString DiagnosisMkb = { index=(IS_OPTN | IS_NLBL), read=FDiagnosisMkb, write=SetDiagnosisMkb, stored = DiagnosisMkb_Specified };
  __property TXSDateTime*       Date = { index=(IS_OPTN), read=FDate, write=SetDate, stored = Date_Specified };
  __property MedicalStaff2*  Performer = { index=(IS_OPTN | IS_NLBL), read=FPerformer, write=SetPerformer, stored = Performer_Specified };
  __property UnicodeString    Comment = { index=(IS_OPTN | IS_NLBL), read=FComment, write=SetComment, stored = Comment_Specified };
  __property bool           Status = { index=(IS_OPTN), read=FStatus, write=SetStatus, stored = Status_Specified };
};




// ************************************************************************ //
// XML       : AppointedMedication, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class AppointedMedication2 : public MedRecord2 {
private:
  UnicodeString   FAnatomicTherapeuticChemicalClassification;
  bool            FAnatomicTherapeuticChemicalClassification_Specified;
  Quantity2*      FCourseDose;
  bool            FCourseDose_Specified;
  Quantity2*      FDayDose;
  bool            FDayDose_Specified;
  unsigned short  FDaysCount;
  bool            FDaysCount_Specified;
  MedicalStaff2*  FDoctor;
  bool            FDoctor_Specified;
  TXSDateTime*    FIssuedDate;
  bool            FIssuedDate_Specified;
  UnicodeString   FMedicineIssueType;
  bool            FMedicineIssueType_Specified;
  UnicodeString   FMedicineName;
  bool            FMedicineName_Specified;
  unsigned short  FMedicineType;
  bool            FMedicineType_Specified;
  unsigned char   FMedicineUseWay;
  bool            FMedicineUseWay_Specified;
  UnicodeString   FNumber;
  bool            FNumber_Specified;
  Quantity2*      FOneTimeDose;
  bool            FOneTimeDose_Specified;
  int             FIdINN;
  bool            FIdINN_Specified;
  UnicodeString   FSeria;
  bool            FSeria_Specified;
  bool            FStatus;
  bool            FStatus_Specified;
  void __fastcall SetAnatomicTherapeuticChemicalClassification(int Index, UnicodeString _prop_val)
  {  FAnatomicTherapeuticChemicalClassification = _prop_val; FAnatomicTherapeuticChemicalClassification_Specified = true;  }
  bool __fastcall AnatomicTherapeuticChemicalClassification_Specified(int Index)
  {  return FAnatomicTherapeuticChemicalClassification_Specified;  }
  void __fastcall SetCourseDose(int Index, Quantity2* _prop_val)
  {  FCourseDose = _prop_val; FCourseDose_Specified = true;  }
  bool __fastcall CourseDose_Specified(int Index)
  {  return FCourseDose_Specified;  }
  void __fastcall SetDayDose(int Index, Quantity2* _prop_val)
  {  FDayDose = _prop_val; FDayDose_Specified = true;  }
  bool __fastcall DayDose_Specified(int Index)
  {  return FDayDose_Specified;  }
  void __fastcall SetDaysCount(int Index, unsigned short _prop_val)
  {  FDaysCount = _prop_val; FDaysCount_Specified = true;  }
  bool __fastcall DaysCount_Specified(int Index)
  {  return FDaysCount_Specified;  }
  void __fastcall SetDoctor(int Index, MedicalStaff2* _prop_val)
  {  FDoctor = _prop_val; FDoctor_Specified = true;  }
  bool __fastcall Doctor_Specified(int Index)
  {  return FDoctor_Specified;  }
  void __fastcall SetIssuedDate(int Index, TXSDateTime* _prop_val)
  {  FIssuedDate = _prop_val; FIssuedDate_Specified = true;  }
  bool __fastcall IssuedDate_Specified(int Index)
  {  return FIssuedDate_Specified;  }
  void __fastcall SetMedicineIssueType(int Index, UnicodeString _prop_val)
  {  FMedicineIssueType = _prop_val; FMedicineIssueType_Specified = true;  }
  bool __fastcall MedicineIssueType_Specified(int Index)
  {  return FMedicineIssueType_Specified;  }
  void __fastcall SetMedicineName(int Index, UnicodeString _prop_val)
  {  FMedicineName = _prop_val; FMedicineName_Specified = true;  }
  bool __fastcall MedicineName_Specified(int Index)
  {  return FMedicineName_Specified;  }
  void __fastcall SetMedicineType(int Index, unsigned short _prop_val)
  {  FMedicineType = _prop_val; FMedicineType_Specified = true;  }
  bool __fastcall MedicineType_Specified(int Index)
  {  return FMedicineType_Specified;  }
  void __fastcall SetMedicineUseWay(int Index, unsigned char _prop_val)
  {  FMedicineUseWay = _prop_val; FMedicineUseWay_Specified = true;  }
  bool __fastcall MedicineUseWay_Specified(int Index)
  {  return FMedicineUseWay_Specified;  }
  void __fastcall SetNumber(int Index, UnicodeString _prop_val)
  {  FNumber = _prop_val; FNumber_Specified = true;  }
  bool __fastcall Number_Specified(int Index)
  {  return FNumber_Specified;  }
  void __fastcall SetOneTimeDose(int Index, Quantity2* _prop_val)
  {  FOneTimeDose = _prop_val; FOneTimeDose_Specified = true;  }
  bool __fastcall OneTimeDose_Specified(int Index)
  {  return FOneTimeDose_Specified;  }
  void __fastcall SetIdINN(int Index, int _prop_val)
  {  FIdINN = _prop_val; FIdINN_Specified = true;  }
  bool __fastcall IdINN_Specified(int Index)
  {  return FIdINN_Specified;  }
  void __fastcall SetSeria(int Index, UnicodeString _prop_val)
  {  FSeria = _prop_val; FSeria_Specified = true;  }
  bool __fastcall Seria_Specified(int Index)
  {  return FSeria_Specified;  }
  void __fastcall SetStatus(int Index, bool _prop_val)
  {  FStatus = _prop_val; FStatus_Specified = true;  }
  bool __fastcall Status_Specified(int Index)
  {  return FStatus_Specified;  }

public:
  __fastcall ~AppointedMedication2();
__published:
  __property UnicodeString AnatomicTherapeuticChemicalClassification = { index=(IS_OPTN | IS_NLBL), read=FAnatomicTherapeuticChemicalClassification, write=SetAnatomicTherapeuticChemicalClassification, stored = AnatomicTherapeuticChemicalClassification_Specified };
  __property Quantity2* CourseDose = { index=(IS_OPTN | IS_NLBL), read=FCourseDose, write=SetCourseDose, stored = CourseDose_Specified };
  __property Quantity2*    DayDose = { index=(IS_OPTN | IS_NLBL), read=FDayDose, write=SetDayDose, stored = DayDose_Specified };
  __property unsigned short  DaysCount = { index=(IS_OPTN), read=FDaysCount, write=SetDaysCount, stored = DaysCount_Specified };
  __property MedicalStaff2*     Doctor = { index=(IS_OPTN | IS_NLBL), read=FDoctor, write=SetDoctor, stored = Doctor_Specified };
  __property TXSDateTime* IssuedDate = { index=(IS_OPTN), read=FIssuedDate, write=SetIssuedDate, stored = IssuedDate_Specified };
  __property UnicodeString MedicineIssueType = { index=(IS_OPTN | IS_NLBL), read=FMedicineIssueType, write=SetMedicineIssueType, stored = MedicineIssueType_Specified };
  __property UnicodeString MedicineName = { index=(IS_OPTN | IS_NLBL), read=FMedicineName, write=SetMedicineName, stored = MedicineName_Specified };
  __property unsigned short MedicineType = { index=(IS_OPTN), read=FMedicineType, write=SetMedicineType, stored = MedicineType_Specified };
  __property unsigned char MedicineUseWay = { index=(IS_OPTN), read=FMedicineUseWay, write=SetMedicineUseWay, stored = MedicineUseWay_Specified };
  __property UnicodeString     Number = { index=(IS_OPTN | IS_NLBL), read=FNumber, write=SetNumber, stored = Number_Specified };
  __property Quantity2* OneTimeDose = { index=(IS_OPTN | IS_NLBL), read=FOneTimeDose, write=SetOneTimeDose, stored = OneTimeDose_Specified };
  __property int             IdINN = { index=(IS_OPTN), read=FIdINN, write=SetIdINN, stored = IdINN_Specified };
  __property UnicodeString      Seria = { index=(IS_OPTN | IS_NLBL), read=FSeria, write=SetSeria, stored = Seria_Specified };
  __property bool           Status = { index=(IS_OPTN | IS_NLBL), read=FStatus, write=SetStatus, stored = Status_Specified };
};




// ************************************************************************ //
// XML       : TfomsInfo, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class TfomsInfo2 : public MedRecord2 {
private:
  int             FCount;
  bool            FCount_Specified;
  UnicodeString   FIdTfomsType;
  bool            FIdTfomsType_Specified;
  TXSDecimal*     FTariff;
  bool            FTariff_Specified;
  void __fastcall SetCount(int Index, int _prop_val)
  {  FCount = _prop_val; FCount_Specified = true;  }
  bool __fastcall Count_Specified(int Index)
  {  return FCount_Specified;  }
  void __fastcall SetIdTfomsType(int Index, UnicodeString _prop_val)
  {  FIdTfomsType = _prop_val; FIdTfomsType_Specified = true;  }
  bool __fastcall IdTfomsType_Specified(int Index)
  {  return FIdTfomsType_Specified;  }
  void __fastcall SetTariff(int Index, TXSDecimal* _prop_val)
  {  FTariff = _prop_val; FTariff_Specified = true;  }
  bool __fastcall Tariff_Specified(int Index)
  {  return FTariff_Specified;  }

public:
  __fastcall ~TfomsInfo2();
__published:
  __property int             Count = { index=(IS_OPTN), read=FCount, write=SetCount, stored = Count_Specified };
  __property UnicodeString IdTfomsType = { index=(IS_OPTN | IS_NLBL), read=FIdTfomsType, write=SetIdTfomsType, stored = IdTfomsType_Specified };
  __property TXSDecimal*     Tariff = { index=(IS_OPTN), read=FTariff, write=SetTariff, stored = Tariff_Specified };
};




// ************************************************************************ //
// XML       : DeathInfo, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class DeathInfo2 : public MedRecord2 {
private:
  UnicodeString   FMkbCode;
  bool            FMkbCode_Specified;
  void __fastcall SetMkbCode(int Index, UnicodeString _prop_val)
  {  FMkbCode = _prop_val; FMkbCode_Specified = true;  }
  bool __fastcall MkbCode_Specified(int Index)
  {  return FMkbCode_Specified;  }
__published:
  __property UnicodeString    MkbCode = { index=(IS_OPTN | IS_NLBL), read=FMkbCode, write=SetMkbCode, stored = MkbCode_Specified };
};




// ************************************************************************ //
// XML       : Diagnosis, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag
// ************************************************************************ //
class Diagnosis2 : public MedRecord2 {
private:
  DiagnosisInfo2* FDiagnosisInfo;
  bool            FDiagnosisInfo_Specified;
  MedicalStaff2*  FDoctor;
  bool            FDoctor_Specified;
  void __fastcall SetDiagnosisInfo(int Index, DiagnosisInfo2* _prop_val)
  {  FDiagnosisInfo = _prop_val; FDiagnosisInfo_Specified = true;  }
  bool __fastcall DiagnosisInfo_Specified(int Index)
  {  return FDiagnosisInfo_Specified;  }
  void __fastcall SetDoctor(int Index, MedicalStaff2* _prop_val)
  {  FDoctor = _prop_val; FDoctor_Specified = true;  }
  bool __fastcall Doctor_Specified(int Index)
  {  return FDoctor_Specified;  }

public:
  __fastcall ~Diagnosis2();
__published:
  __property DiagnosisInfo2* DiagnosisInfo = { index=(IS_OPTN | IS_NLBL), read=FDiagnosisInfo, write=SetDiagnosisInfo, stored = DiagnosisInfo_Specified };
  __property MedicalStaff2*     Doctor = { index=(IS_OPTN | IS_NLBL), read=FDoctor, write=SetDoctor, stored = Doctor_Specified };
};




// ************************************************************************ //
// XML       : ClinicMainDiagnosis, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag
// ************************************************************************ //
class ClinicMainDiagnosis2 : public Diagnosis2 {
private:
  ArrayOfDiagnosis FComplications;
  bool            FComplications_Specified;
  void __fastcall SetComplications(int Index, ArrayOfDiagnosis _prop_val)
  {  FComplications = _prop_val; FComplications_Specified = true;  }
  bool __fastcall Complications_Specified(int Index)
  {  return FComplications_Specified;  }

public:
  __fastcall ~ClinicMainDiagnosis2();
__published:
  __property ArrayOfDiagnosis Complications = { index=(IS_OPTN | IS_NLBL), read=FComplications, write=SetComplications, stored = Complications_Specified };
};




// ************************************************************************ //
// XML       : ClinicMainDiagnosis, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag
// ************************************************************************ //
class ClinicMainDiagnosis : public ClinicMainDiagnosis2 {
private:
__published:
};




// ************************************************************************ //
// XML       : AnatomopathologicalClinicMainDiagnosis, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag
// ************************************************************************ //
class AnatomopathologicalClinicMainDiagnosis2 : public ClinicMainDiagnosis2 {
private:
__published:
};




// ************************************************************************ //
// XML       : AnatomopathologicalClinicMainDiagnosis, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag
// ************************************************************************ //
class AnatomopathologicalClinicMainDiagnosis : public AnatomopathologicalClinicMainDiagnosis2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Diagnosis, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag
// ************************************************************************ //
class Diagnosis : public Diagnosis2 {
private:
__published:
};




// ************************************************************************ //
// XML       : MedDocument, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class MedDocument2 : public MedRecord2 {
private:
  ArrayOfMedDocumentDto_DocumentAttachment FAttachments;
  bool            FAttachments_Specified;
  MedicalStaff2*  FAuthor;
  bool            FAuthor_Specified;
  TXSDateTime*    FCreationDate;
  bool            FCreationDate_Specified;
  UnicodeString   FFhirMedDocumentType;
  bool            FFhirMedDocumentType_Specified;
  UnicodeString   FHeader;
  bool            FHeader_Specified;
  UnicodeString   FIdDocumentMis;
  bool            FIdDocumentMis_Specified;
  ArrayOfObservation FObservations;
  bool            FObservations_Specified;
  void __fastcall SetAttachments(int Index, ArrayOfMedDocumentDto_DocumentAttachment _prop_val)
  {  FAttachments = _prop_val; FAttachments_Specified = true;  }
  bool __fastcall Attachments_Specified(int Index)
  {  return FAttachments_Specified;  }
  void __fastcall SetAuthor(int Index, MedicalStaff2* _prop_val)
  {  FAuthor = _prop_val; FAuthor_Specified = true;  }
  bool __fastcall Author_Specified(int Index)
  {  return FAuthor_Specified;  }
  void __fastcall SetCreationDate(int Index, TXSDateTime* _prop_val)
  {  FCreationDate = _prop_val; FCreationDate_Specified = true;  }
  bool __fastcall CreationDate_Specified(int Index)
  {  return FCreationDate_Specified;  }
  void __fastcall SetFhirMedDocumentType(int Index, UnicodeString _prop_val)
  {  FFhirMedDocumentType = _prop_val; FFhirMedDocumentType_Specified = true;  }
  bool __fastcall FhirMedDocumentType_Specified(int Index)
  {  return FFhirMedDocumentType_Specified;  }
  void __fastcall SetHeader(int Index, UnicodeString _prop_val)
  {  FHeader = _prop_val; FHeader_Specified = true;  }
  bool __fastcall Header_Specified(int Index)
  {  return FHeader_Specified;  }
  void __fastcall SetIdDocumentMis(int Index, UnicodeString _prop_val)
  {  FIdDocumentMis = _prop_val; FIdDocumentMis_Specified = true;  }
  bool __fastcall IdDocumentMis_Specified(int Index)
  {  return FIdDocumentMis_Specified;  }
  void __fastcall SetObservations(int Index, ArrayOfObservation _prop_val)
  {  FObservations = _prop_val; FObservations_Specified = true;  }
  bool __fastcall Observations_Specified(int Index)
  {  return FObservations_Specified;  }

public:
  __fastcall ~MedDocument2();
__published:
  __property ArrayOfMedDocumentDto_DocumentAttachment Attachments = { index=(IS_OPTN | IS_NLBL), read=FAttachments, write=SetAttachments, stored = Attachments_Specified };
  __property MedicalStaff2*     Author = { index=(IS_OPTN | IS_NLBL), read=FAuthor, write=SetAuthor, stored = Author_Specified };
  __property TXSDateTime* CreationDate = { index=(IS_OPTN), read=FCreationDate, write=SetCreationDate, stored = CreationDate_Specified };
  __property UnicodeString FhirMedDocumentType = { index=(IS_OPTN | IS_NLBL), read=FFhirMedDocumentType, write=SetFhirMedDocumentType, stored = FhirMedDocumentType_Specified };
  __property UnicodeString     Header = { index=(IS_OPTN | IS_NLBL), read=FHeader, write=SetHeader, stored = Header_Specified };
  __property UnicodeString IdDocumentMis = { index=(IS_OPTN | IS_NLBL), read=FIdDocumentMis, write=SetIdDocumentMis, stored = IdDocumentMis_Specified };
  __property ArrayOfObservation Observations = { index=(IS_OPTN | IS_NLBL), read=FObservations, write=SetObservations, stored = Observations_Specified };
};




// ************************************************************************ //
// XML       : Referral, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class Referral2 : public MedDocument2 {
private:
  ReferralInfo2*  FReferralInfo;
  bool            FReferralInfo_Specified;
  MedicalStaff2*  FDepartmentHead;
  bool            FDepartmentHead_Specified;
  UnicodeString   FIdSourceLpu;
  bool            FIdSourceLpu_Specified;
  UnicodeString   FIdTargetLpu;
  bool            FIdTargetLpu_Specified;
  UnicodeString   FReferralID;
  bool            FReferralID_Specified;
  UnicodeString   FRelatedID;
  bool            FRelatedID_Specified;
  void __fastcall SetReferralInfo(int Index, ReferralInfo2* _prop_val)
  {  FReferralInfo = _prop_val; FReferralInfo_Specified = true;  }
  bool __fastcall ReferralInfo_Specified(int Index)
  {  return FReferralInfo_Specified;  }
  void __fastcall SetDepartmentHead(int Index, MedicalStaff2* _prop_val)
  {  FDepartmentHead = _prop_val; FDepartmentHead_Specified = true;  }
  bool __fastcall DepartmentHead_Specified(int Index)
  {  return FDepartmentHead_Specified;  }
  void __fastcall SetIdSourceLpu(int Index, UnicodeString _prop_val)
  {  FIdSourceLpu = _prop_val; FIdSourceLpu_Specified = true;  }
  bool __fastcall IdSourceLpu_Specified(int Index)
  {  return FIdSourceLpu_Specified;  }
  void __fastcall SetIdTargetLpu(int Index, UnicodeString _prop_val)
  {  FIdTargetLpu = _prop_val; FIdTargetLpu_Specified = true;  }
  bool __fastcall IdTargetLpu_Specified(int Index)
  {  return FIdTargetLpu_Specified;  }
  void __fastcall SetReferralID(int Index, UnicodeString _prop_val)
  {  FReferralID = _prop_val; FReferralID_Specified = true;  }
  bool __fastcall ReferralID_Specified(int Index)
  {  return FReferralID_Specified;  }
  void __fastcall SetRelatedID(int Index, UnicodeString _prop_val)
  {  FRelatedID = _prop_val; FRelatedID_Specified = true;  }
  bool __fastcall RelatedID_Specified(int Index)
  {  return FRelatedID_Specified;  }

public:
  __fastcall ~Referral2();
__published:
  __property ReferralInfo2* ReferralInfo = { index=(IS_OPTN | IS_NLBL), read=FReferralInfo, write=SetReferralInfo, stored = ReferralInfo_Specified };
  __property MedicalStaff2* DepartmentHead = { index=(IS_OPTN | IS_NLBL), read=FDepartmentHead, write=SetDepartmentHead, stored = DepartmentHead_Specified };
  __property UnicodeString IdSourceLpu = { index=(IS_OPTN | IS_NLBL), read=FIdSourceLpu, write=SetIdSourceLpu, stored = IdSourceLpu_Specified };
  __property UnicodeString IdTargetLpu = { index=(IS_OPTN | IS_NLBL), read=FIdTargetLpu, write=SetIdTargetLpu, stored = IdTargetLpu_Specified };
  __property UnicodeString ReferralID = { index=(IS_OPTN | IS_NLBL), read=FReferralID, write=SetReferralID, stored = ReferralID_Specified };
  __property UnicodeString  RelatedID = { index=(IS_OPTN | IS_NLBL), read=FRelatedID, write=SetRelatedID, stored = RelatedID_Specified };
};




// ************************************************************************ //
// XML       : Referral, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class Referral : public Referral2 {
private:
__published:
};




// ************************************************************************ //
// XML       : AmbulanceInfo, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class AmbulanceInfo2 : public MedDocument2 {
private:
  UnicodeString   FSubAmbulance;
  bool            FSubAmbulance_Specified;
  UnicodeString   FBrigadeNumber;
  bool            FBrigadeNumber_Specified;
  unsigned char   FMedResult;
  bool            FMedResult_Specified;
  ArrayOfAssisNote FAssisList;
  bool            FAssisList_Specified;
  TXSDateTime*    FTransportTime;
  bool            FTransportTime_Specified;
  UnicodeString   FHospitalName;
  bool            FHospitalName_Specified;
  void __fastcall SetSubAmbulance(int Index, UnicodeString _prop_val)
  {  FSubAmbulance = _prop_val; FSubAmbulance_Specified = true;  }
  bool __fastcall SubAmbulance_Specified(int Index)
  {  return FSubAmbulance_Specified;  }
  void __fastcall SetBrigadeNumber(int Index, UnicodeString _prop_val)
  {  FBrigadeNumber = _prop_val; FBrigadeNumber_Specified = true;  }
  bool __fastcall BrigadeNumber_Specified(int Index)
  {  return FBrigadeNumber_Specified;  }
  void __fastcall SetMedResult(int Index, unsigned char _prop_val)
  {  FMedResult = _prop_val; FMedResult_Specified = true;  }
  bool __fastcall MedResult_Specified(int Index)
  {  return FMedResult_Specified;  }
  void __fastcall SetAssisList(int Index, ArrayOfAssisNote _prop_val)
  {  FAssisList = _prop_val; FAssisList_Specified = true;  }
  bool __fastcall AssisList_Specified(int Index)
  {  return FAssisList_Specified;  }
  void __fastcall SetTransportTime(int Index, TXSDateTime* _prop_val)
  {  FTransportTime = _prop_val; FTransportTime_Specified = true;  }
  bool __fastcall TransportTime_Specified(int Index)
  {  return FTransportTime_Specified;  }
  void __fastcall SetHospitalName(int Index, UnicodeString _prop_val)
  {  FHospitalName = _prop_val; FHospitalName_Specified = true;  }
  bool __fastcall HospitalName_Specified(int Index)
  {  return FHospitalName_Specified;  }

public:
  __fastcall ~AmbulanceInfo2();
__published:
  __property UnicodeString SubAmbulance = { index=(IS_OPTN | IS_NLBL), read=FSubAmbulance, write=SetSubAmbulance, stored = SubAmbulance_Specified };
  __property UnicodeString BrigadeNumber = { index=(IS_OPTN | IS_NLBL), read=FBrigadeNumber, write=SetBrigadeNumber, stored = BrigadeNumber_Specified };
  __property unsigned char  MedResult = { index=(IS_OPTN), read=FMedResult, write=SetMedResult, stored = MedResult_Specified };
  __property ArrayOfAssisNote  AssisList = { index=(IS_OPTN | IS_NLBL), read=FAssisList, write=SetAssisList, stored = AssisList_Specified };
  __property TXSDateTime* TransportTime = { index=(IS_OPTN | IS_NLBL), read=FTransportTime, write=SetTransportTime, stored = TransportTime_Specified };
  __property UnicodeString HospitalName = { index=(IS_OPTN | IS_NLBL), read=FHospitalName, write=SetHospitalName, stored = HospitalName_Specified };
};




// ************************************************************************ //
// XML       : AmbulanceInfo, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class AmbulanceInfo : public AmbulanceInfo2 {
private:
__published:
};




// ************************************************************************ //
// XML       : DeathCertificate, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class DeathCertificate2 : public MedDocument2 {
private:
  UnicodeString   FDeathReason;
  bool            FDeathReason_Specified;
  void __fastcall SetDeathReason(int Index, UnicodeString _prop_val)
  {  FDeathReason = _prop_val; FDeathReason_Specified = true;  }
  bool __fastcall DeathReason_Specified(int Index)
  {  return FDeathReason_Specified;  }
__published:
  __property UnicodeString DeathReason = { index=(IS_OPTN | IS_NLBL), read=FDeathReason, write=SetDeathReason, stored = DeathReason_Specified };
};




// ************************************************************************ //
// XML       : DeathCertificate, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class DeathCertificate : public DeathCertificate2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Form106U, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class Form106U2 : public MedDocument2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Form106U, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class Form106U : public Form106U2 {
private:
__published:
};




// ************************************************************************ //
// XML       : PacsResult, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class PacsResult2 : public MedDocument2 {
private:
  TXSDateTime*    FProtocolDate;
  bool            FProtocolDate_Specified;
  UnicodeString   FCode;
  bool            FCode_Specified;
  PacsResult_ImageData2* FImage;
  bool            FImage_Specified;
  void __fastcall SetProtocolDate(int Index, TXSDateTime* _prop_val)
  {  FProtocolDate = _prop_val; FProtocolDate_Specified = true;  }
  bool __fastcall ProtocolDate_Specified(int Index)
  {  return FProtocolDate_Specified;  }
  void __fastcall SetCode(int Index, UnicodeString _prop_val)
  {  FCode = _prop_val; FCode_Specified = true;  }
  bool __fastcall Code_Specified(int Index)
  {  return FCode_Specified;  }
  void __fastcall SetImage(int Index, PacsResult_ImageData2* _prop_val)
  {  FImage = _prop_val; FImage_Specified = true;  }
  bool __fastcall Image_Specified(int Index)
  {  return FImage_Specified;  }

public:
  __fastcall ~PacsResult2();
__published:
  __property TXSDateTime* ProtocolDate = { index=(IS_OPTN), read=FProtocolDate, write=SetProtocolDate, stored = ProtocolDate_Specified };
  __property UnicodeString       Code = { index=(IS_OPTN | IS_NLBL), read=FCode, write=SetCode, stored = Code_Specified };
  __property PacsResult_ImageData2*      Image = { index=(IS_OPTN | IS_NLBL), read=FImage, write=SetImage, stored = Image_Specified };
};




// ************************************************************************ //
// XML       : PacsResult, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class PacsResult : public PacsResult2 {
private:
__published:
};




// ************************************************************************ //
// XML       : ReferralMSE, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class ReferralMSE2 : public MedDocument2 {
private:
__published:
};




// ************************************************************************ //
// XML       : ReferralMSE, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class ReferralMSE : public ReferralMSE2 {
private:
__published:
};




// ************************************************************************ //
// XML       : MedDocument, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class MedDocument : public MedDocument2 {
private:
__published:
};




// ************************************************************************ //
// XML       : DispensaryBase, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class DispensaryBase2 : public MedDocument2 {
private:
  bool            FIsGuested;
  bool            FIsGuested_Specified;
  bool            FHasExtraResearchRefferal;
  bool            FHasExtraResearchRefferal_Specified;
  bool            FIsUnderObservation;
  bool            FIsUnderObservation_Specified;
  bool            FHasExpertCareRefferal;
  bool            FHasExpertCareRefferal_Specified;
  bool            FHasPrescribeCure;
  bool            FHasPrescribeCure_Specified;
  bool            FHasHealthResortRefferal;
  bool            FHasHealthResortRefferal_Specified;
  HealthGroup2*   FHealthGroup;
  bool            FHealthGroup_Specified;
  ArrayOfRecommendation FRecommendations;
  bool            FRecommendations_Specified;
  void __fastcall SetIsGuested(int Index, bool _prop_val)
  {  FIsGuested = _prop_val; FIsGuested_Specified = true;  }
  bool __fastcall IsGuested_Specified(int Index)
  {  return FIsGuested_Specified;  }
  void __fastcall SetHasExtraResearchRefferal(int Index, bool _prop_val)
  {  FHasExtraResearchRefferal = _prop_val; FHasExtraResearchRefferal_Specified = true;  }
  bool __fastcall HasExtraResearchRefferal_Specified(int Index)
  {  return FHasExtraResearchRefferal_Specified;  }
  void __fastcall SetIsUnderObservation(int Index, bool _prop_val)
  {  FIsUnderObservation = _prop_val; FIsUnderObservation_Specified = true;  }
  bool __fastcall IsUnderObservation_Specified(int Index)
  {  return FIsUnderObservation_Specified;  }
  void __fastcall SetHasExpertCareRefferal(int Index, bool _prop_val)
  {  FHasExpertCareRefferal = _prop_val; FHasExpertCareRefferal_Specified = true;  }
  bool __fastcall HasExpertCareRefferal_Specified(int Index)
  {  return FHasExpertCareRefferal_Specified;  }
  void __fastcall SetHasPrescribeCure(int Index, bool _prop_val)
  {  FHasPrescribeCure = _prop_val; FHasPrescribeCure_Specified = true;  }
  bool __fastcall HasPrescribeCure_Specified(int Index)
  {  return FHasPrescribeCure_Specified;  }
  void __fastcall SetHasHealthResortRefferal(int Index, bool _prop_val)
  {  FHasHealthResortRefferal = _prop_val; FHasHealthResortRefferal_Specified = true;  }
  bool __fastcall HasHealthResortRefferal_Specified(int Index)
  {  return FHasHealthResortRefferal_Specified;  }
  void __fastcall SetHealthGroup(int Index, HealthGroup2* _prop_val)
  {  FHealthGroup = _prop_val; FHealthGroup_Specified = true;  }
  bool __fastcall HealthGroup_Specified(int Index)
  {  return FHealthGroup_Specified;  }
  void __fastcall SetRecommendations(int Index, ArrayOfRecommendation _prop_val)
  {  FRecommendations = _prop_val; FRecommendations_Specified = true;  }
  bool __fastcall Recommendations_Specified(int Index)
  {  return FRecommendations_Specified;  }

public:
  __fastcall ~DispensaryBase2();
__published:
  __property bool        IsGuested = { index=(IS_OPTN), read=FIsGuested, write=SetIsGuested, stored = IsGuested_Specified };
  __property bool       HasExtraResearchRefferal = { index=(IS_OPTN), read=FHasExtraResearchRefferal, write=SetHasExtraResearchRefferal, stored = HasExtraResearchRefferal_Specified };
  __property bool       IsUnderObservation = { index=(IS_OPTN), read=FIsUnderObservation, write=SetIsUnderObservation, stored = IsUnderObservation_Specified };
  __property bool       HasExpertCareRefferal = { index=(IS_OPTN), read=FHasExpertCareRefferal, write=SetHasExpertCareRefferal, stored = HasExpertCareRefferal_Specified };
  __property bool       HasPrescribeCure = { index=(IS_OPTN), read=FHasPrescribeCure, write=SetHasPrescribeCure, stored = HasPrescribeCure_Specified };
  __property bool       HasHealthResortRefferal = { index=(IS_OPTN), read=FHasHealthResortRefferal, write=SetHasHealthResortRefferal, stored = HasHealthResortRefferal_Specified };
  __property HealthGroup2* HealthGroup = { index=(IS_OPTN | IS_NLBL), read=FHealthGroup, write=SetHealthGroup, stored = HealthGroup_Specified };
  __property ArrayOfRecommendation Recommendations = { index=(IS_OPTN | IS_NLBL), read=FRecommendations, write=SetRecommendations, stored = Recommendations_Specified };
};




// ************************************************************************ //
// XML       : DispensaryBase, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class DispensaryBase : public DispensaryBase2 {
private:
__published:
};




// ************************************************************************ //
// XML       : DispensaryOne, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class DispensaryOne2 : public DispensaryBase2 {
private:
  bool            FHasSecondStageRefferal;
  bool            FHasSecondStageRefferal_Specified;
  void __fastcall SetHasSecondStageRefferal(int Index, bool _prop_val)
  {  FHasSecondStageRefferal = _prop_val; FHasSecondStageRefferal_Specified = true;  }
  bool __fastcall HasSecondStageRefferal_Specified(int Index)
  {  return FHasSecondStageRefferal_Specified;  }
__published:
  __property bool       HasSecondStageRefferal = { index=(IS_OPTN), read=FHasSecondStageRefferal, write=SetHasSecondStageRefferal, stored = HasSecondStageRefferal_Specified };
};




// ************************************************************************ //
// XML       : DispensaryOne, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class DispensaryOne : public DispensaryOne2 {
private:
__published:
};




// ************************************************************************ //
// XML       : DispensaryOnceByTwoYears, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class DispensaryOnceByTwoYears2 : public DispensaryBase2 {
private:
__published:
};




// ************************************************************************ //
// XML       : DispensaryOnceByTwoYears, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class DispensaryOnceByTwoYears : public DispensaryOnceByTwoYears2 {
private:
__published:
};




// ************************************************************************ //
// XML       : DispensaryTwo, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class DispensaryTwo2 : public DispensaryBase2 {
private:
__published:
};




// ************************************************************************ //
// XML       : DispensaryTwo, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class DispensaryTwo : public DispensaryTwo2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Form103U, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class Form103U2 : public MedDocument2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Form103U, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class Form103U : public Form103U2 {
private:
__published:
};




// ************************************************************************ //
// XML       : ConsultNote, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class ConsultNote2 : public MedDocument2 {
private:
__published:
};




// ************************************************************************ //
// XML       : ConsultNote, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class ConsultNote : public ConsultNote2 {
private:
__published:
};




// ************************************************************************ //
// XML       : DischargeSummary, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class DischargeSummary2 : public MedDocument2 {
private:
__published:
};




// ************************************************************************ //
// XML       : DischargeSummary, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class DischargeSummary : public DischargeSummary2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Form106_2U, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class Form106_2U2 : public MedDocument2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Form106_2U, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class Form106_2U : public Form106_2U2 {
private:
__published:
};




// ************************************************************************ //
// XML       : SickList, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class SickList2 : public MedDocument2 {
private:
  SickListInfo2*  FSickListInfo;
  bool            FSickListInfo_Specified;
  void __fastcall SetSickListInfo(int Index, SickListInfo2* _prop_val)
  {  FSickListInfo = _prop_val; FSickListInfo_Specified = true;  }
  bool __fastcall SickListInfo_Specified(int Index)
  {  return FSickListInfo_Specified;  }

public:
  __fastcall ~SickList2();
__published:
  __property SickListInfo2* SickListInfo = { index=(IS_OPTN | IS_NLBL), read=FSickListInfo, write=SetSickListInfo, stored = SickListInfo_Specified };
};




// ************************************************************************ //
// XML       : SickList, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class SickList : public SickList2 {
private:
__published:
};




// ************************************************************************ //
// XML       : LaboratoryReport, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class LaboratoryReport2 : public MedDocument2 {
private:
__published:
};




// ************************************************************************ //
// XML       : LaboratoryReport, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class LaboratoryReport : public LaboratoryReport2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Form027U, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class Form027U2 : public MedDocument2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Form027U, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc
// ************************************************************************ //
class Form027U : public Form027U2 {
private:
__published:
};


typedef DynamicArray<unsigned char> ArrayOfunsignedByte; /* "http://schemas.microsoft.com/2003/10/Serialization/Arrays"[GblCplx] */


// ************************************************************************ //
// XML       : CaseBase, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case
// ************************************************************************ //
class CaseBase : public CaseBase2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Initiator, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class Initiator2 : public TRemotable {
private:
  unsigned char   FInitiatorType;
  bool            FInitiatorType_Specified;
  MedicalStaff2*  FDoctor;
  bool            FDoctor_Specified;
  void __fastcall SetInitiatorType(int Index, unsigned char _prop_val)
  {  FInitiatorType = _prop_val; FInitiatorType_Specified = true;  }
  bool __fastcall InitiatorType_Specified(int Index)
  {  return FInitiatorType_Specified;  }
  void __fastcall SetDoctor(int Index, MedicalStaff2* _prop_val)
  {  FDoctor = _prop_val; FDoctor_Specified = true;  }
  bool __fastcall Doctor_Specified(int Index)
  {  return FDoctor_Specified;  }

public:
  __fastcall ~Initiator2();
__published:
  __property unsigned char InitiatorType = { index=(IS_OPTN), read=FInitiatorType, write=SetInitiatorType, stored = InitiatorType_Specified };
  __property MedicalStaff2*     Doctor = { index=(IS_OPTN | IS_NLBL), read=FDoctor, write=SetDoctor, stored = Doctor_Specified };
};




// ************************************************************************ //
// XML       : Initiator, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class Initiator : public Initiator2 {
private:
__published:
};


typedef DynamicArray<StepAmb2*>   ArrayOfStepAmb; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step"[GblCplx] */


// ************************************************************************ //
// XML       : Guardian, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class Guardian2 : public TRemotable {
private:
  PersonWithIdentity2* FPerson;
  bool            FPerson_Specified;
  unsigned char   FIdRelationType;
  bool            FIdRelationType_Specified;
  UnicodeString   FUnderlyingDocument;
  bool            FUnderlyingDocument_Specified;
  void __fastcall SetPerson(int Index, PersonWithIdentity2* _prop_val)
  {  FPerson = _prop_val; FPerson_Specified = true;  }
  bool __fastcall Person_Specified(int Index)
  {  return FPerson_Specified;  }
  void __fastcall SetIdRelationType(int Index, unsigned char _prop_val)
  {  FIdRelationType = _prop_val; FIdRelationType_Specified = true;  }
  bool __fastcall IdRelationType_Specified(int Index)
  {  return FIdRelationType_Specified;  }
  void __fastcall SetUnderlyingDocument(int Index, UnicodeString _prop_val)
  {  FUnderlyingDocument = _prop_val; FUnderlyingDocument_Specified = true;  }
  bool __fastcall UnderlyingDocument_Specified(int Index)
  {  return FUnderlyingDocument_Specified;  }

public:
  __fastcall ~Guardian2();
__published:
  __property PersonWithIdentity2*     Person = { index=(IS_OPTN | IS_NLBL), read=FPerson, write=SetPerson, stored = Person_Specified };
  __property unsigned char IdRelationType = { index=(IS_OPTN), read=FIdRelationType, write=SetIdRelationType, stored = IdRelationType_Specified };
  __property UnicodeString UnderlyingDocument = { index=(IS_OPTN | IS_NLBL), read=FUnderlyingDocument, write=SetUnderlyingDocument, stored = UnderlyingDocument_Specified };
};




// ************************************************************************ //
// XML       : Guardian, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto
// ************************************************************************ //
class Guardian : public Guardian2 {
private:
__published:
};


typedef DynamicArray<StepStat2*>  ArrayOfStepStat; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step"[GblCplx] */
typedef DynamicArray<MedRecord2*> ArrayOfMedRecord; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec"[GblCplx] */


// ************************************************************************ //
// XML       : CaseStat, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case
// ************************************************************************ //
class CaseStat2 : public CaseBase2 {
private:
  UnicodeString   FDeliveryCode;
  bool            FDeliveryCode_Specified;
  unsigned char   FIdIntoxicationType;
  bool            FIdIntoxicationType_Specified;
  unsigned char   FIdTypeFromDiseaseStart;
  bool            FIdTypeFromDiseaseStart_Specified;
  unsigned char   FIdRepetition;
  bool            FIdRepetition_Specified;
  unsigned char   FHospitalizationOrder;
  bool            FHospitalizationOrder_Specified;
  unsigned char   FIdTransportIntern;
  bool            FIdTransportIntern_Specified;
  ArrayOfStepStat FSteps;
  bool            FSteps_Specified;
  unsigned char   FHospResult;
  bool            FHospResult_Specified;
  ArrayOfMedRecord FMedRecords;
  bool            FMedRecords_Specified;
  unsigned char   FIdHospChannel;
  bool            FIdHospChannel_Specified;
  bool            FRW1Mark;
  bool            FRW1Mark_Specified;
  bool            FAIDSMark;
  bool            FAIDSMark_Specified;
  ArrayOfunsignedByte FPrehospitalDefects;
  bool            FPrehospitalDefects_Specified;
  UnicodeString   FAdmissionComment;
  bool            FAdmissionComment_Specified;
  unsigned char   FDischargeCondition;
  bool            FDischargeCondition_Specified;
  UnicodeString   FDischargeComment;
  bool            FDischargeComment_Specified;
  UnicodeString   FDietComment;
  bool            FDietComment_Specified;
  UnicodeString   FTreatComment;
  bool            FTreatComment_Specified;
  UnicodeString   FWorkComment;
  bool            FWorkComment_Specified;
  UnicodeString   FOtherComment;
  bool            FOtherComment_Specified;
  void __fastcall SetDeliveryCode(int Index, UnicodeString _prop_val)
  {  FDeliveryCode = _prop_val; FDeliveryCode_Specified = true;  }
  bool __fastcall DeliveryCode_Specified(int Index)
  {  return FDeliveryCode_Specified;  }
  void __fastcall SetIdIntoxicationType(int Index, unsigned char _prop_val)
  {  FIdIntoxicationType = _prop_val; FIdIntoxicationType_Specified = true;  }
  bool __fastcall IdIntoxicationType_Specified(int Index)
  {  return FIdIntoxicationType_Specified;  }
  void __fastcall SetIdTypeFromDiseaseStart(int Index, unsigned char _prop_val)
  {  FIdTypeFromDiseaseStart = _prop_val; FIdTypeFromDiseaseStart_Specified = true;  }
  bool __fastcall IdTypeFromDiseaseStart_Specified(int Index)
  {  return FIdTypeFromDiseaseStart_Specified;  }
  void __fastcall SetIdRepetition(int Index, unsigned char _prop_val)
  {  FIdRepetition = _prop_val; FIdRepetition_Specified = true;  }
  bool __fastcall IdRepetition_Specified(int Index)
  {  return FIdRepetition_Specified;  }
  void __fastcall SetHospitalizationOrder(int Index, unsigned char _prop_val)
  {  FHospitalizationOrder = _prop_val; FHospitalizationOrder_Specified = true;  }
  bool __fastcall HospitalizationOrder_Specified(int Index)
  {  return FHospitalizationOrder_Specified;  }
  void __fastcall SetIdTransportIntern(int Index, unsigned char _prop_val)
  {  FIdTransportIntern = _prop_val; FIdTransportIntern_Specified = true;  }
  bool __fastcall IdTransportIntern_Specified(int Index)
  {  return FIdTransportIntern_Specified;  }
  void __fastcall SetSteps(int Index, ArrayOfStepStat _prop_val)
  {  FSteps = _prop_val; FSteps_Specified = true;  }
  bool __fastcall Steps_Specified(int Index)
  {  return FSteps_Specified;  }
  void __fastcall SetHospResult(int Index, unsigned char _prop_val)
  {  FHospResult = _prop_val; FHospResult_Specified = true;  }
  bool __fastcall HospResult_Specified(int Index)
  {  return FHospResult_Specified;  }
  void __fastcall SetMedRecords(int Index, ArrayOfMedRecord _prop_val)
  {  FMedRecords = _prop_val; FMedRecords_Specified = true;  }
  bool __fastcall MedRecords_Specified(int Index)
  {  return FMedRecords_Specified;  }
  void __fastcall SetIdHospChannel(int Index, unsigned char _prop_val)
  {  FIdHospChannel = _prop_val; FIdHospChannel_Specified = true;  }
  bool __fastcall IdHospChannel_Specified(int Index)
  {  return FIdHospChannel_Specified;  }
  void __fastcall SetRW1Mark(int Index, bool _prop_val)
  {  FRW1Mark = _prop_val; FRW1Mark_Specified = true;  }
  bool __fastcall RW1Mark_Specified(int Index)
  {  return FRW1Mark_Specified;  }
  void __fastcall SetAIDSMark(int Index, bool _prop_val)
  {  FAIDSMark = _prop_val; FAIDSMark_Specified = true;  }
  bool __fastcall AIDSMark_Specified(int Index)
  {  return FAIDSMark_Specified;  }
  void __fastcall SetPrehospitalDefects(int Index, ArrayOfunsignedByte _prop_val)
  {  FPrehospitalDefects = _prop_val; FPrehospitalDefects_Specified = true;  }
  bool __fastcall PrehospitalDefects_Specified(int Index)
  {  return FPrehospitalDefects_Specified;  }
  void __fastcall SetAdmissionComment(int Index, UnicodeString _prop_val)
  {  FAdmissionComment = _prop_val; FAdmissionComment_Specified = true;  }
  bool __fastcall AdmissionComment_Specified(int Index)
  {  return FAdmissionComment_Specified;  }
  void __fastcall SetDischargeCondition(int Index, unsigned char _prop_val)
  {  FDischargeCondition = _prop_val; FDischargeCondition_Specified = true;  }
  bool __fastcall DischargeCondition_Specified(int Index)
  {  return FDischargeCondition_Specified;  }
  void __fastcall SetDischargeComment(int Index, UnicodeString _prop_val)
  {  FDischargeComment = _prop_val; FDischargeComment_Specified = true;  }
  bool __fastcall DischargeComment_Specified(int Index)
  {  return FDischargeComment_Specified;  }
  void __fastcall SetDietComment(int Index, UnicodeString _prop_val)
  {  FDietComment = _prop_val; FDietComment_Specified = true;  }
  bool __fastcall DietComment_Specified(int Index)
  {  return FDietComment_Specified;  }
  void __fastcall SetTreatComment(int Index, UnicodeString _prop_val)
  {  FTreatComment = _prop_val; FTreatComment_Specified = true;  }
  bool __fastcall TreatComment_Specified(int Index)
  {  return FTreatComment_Specified;  }
  void __fastcall SetWorkComment(int Index, UnicodeString _prop_val)
  {  FWorkComment = _prop_val; FWorkComment_Specified = true;  }
  bool __fastcall WorkComment_Specified(int Index)
  {  return FWorkComment_Specified;  }
  void __fastcall SetOtherComment(int Index, UnicodeString _prop_val)
  {  FOtherComment = _prop_val; FOtherComment_Specified = true;  }
  bool __fastcall OtherComment_Specified(int Index)
  {  return FOtherComment_Specified;  }

public:
  __fastcall ~CaseStat2();
__published:
  __property UnicodeString DeliveryCode = { index=(IS_OPTN | IS_NLBL), read=FDeliveryCode, write=SetDeliveryCode, stored = DeliveryCode_Specified };
  __property unsigned char IdIntoxicationType = { index=(IS_OPTN), read=FIdIntoxicationType, write=SetIdIntoxicationType, stored = IdIntoxicationType_Specified };
  __property unsigned char IdTypeFromDiseaseStart = { index=(IS_OPTN), read=FIdTypeFromDiseaseStart, write=SetIdTypeFromDiseaseStart, stored = IdTypeFromDiseaseStart_Specified };
  __property unsigned char IdRepetition = { index=(IS_OPTN), read=FIdRepetition, write=SetIdRepetition, stored = IdRepetition_Specified };
  __property unsigned char HospitalizationOrder = { index=(IS_OPTN), read=FHospitalizationOrder, write=SetHospitalizationOrder, stored = HospitalizationOrder_Specified };
  __property unsigned char IdTransportIntern = { index=(IS_OPTN), read=FIdTransportIntern, write=SetIdTransportIntern, stored = IdTransportIntern_Specified };
  __property ArrayOfStepStat      Steps = { index=(IS_OPTN | IS_NLBL), read=FSteps, write=SetSteps, stored = Steps_Specified };
  __property unsigned char HospResult = { index=(IS_OPTN), read=FHospResult, write=SetHospResult, stored = HospResult_Specified };
  __property ArrayOfMedRecord MedRecords = { index=(IS_OPTN | IS_NLBL), read=FMedRecords, write=SetMedRecords, stored = MedRecords_Specified };
  __property unsigned char IdHospChannel = { index=(IS_OPTN), read=FIdHospChannel, write=SetIdHospChannel, stored = IdHospChannel_Specified };
  __property bool          RW1Mark = { index=(IS_OPTN), read=FRW1Mark, write=SetRW1Mark, stored = RW1Mark_Specified };
  __property bool         AIDSMark = { index=(IS_OPTN), read=FAIDSMark, write=SetAIDSMark, stored = AIDSMark_Specified };
  __property ArrayOfunsignedByte PrehospitalDefects = { index=(IS_OPTN | IS_NLBL), read=FPrehospitalDefects, write=SetPrehospitalDefects, stored = PrehospitalDefects_Specified };
  __property UnicodeString AdmissionComment = { index=(IS_OPTN | IS_NLBL), read=FAdmissionComment, write=SetAdmissionComment, stored = AdmissionComment_Specified };
  __property unsigned char DischargeCondition = { index=(IS_OPTN), read=FDischargeCondition, write=SetDischargeCondition, stored = DischargeCondition_Specified };
  __property UnicodeString DischargeComment = { index=(IS_OPTN | IS_NLBL), read=FDischargeComment, write=SetDischargeComment, stored = DischargeComment_Specified };
  __property UnicodeString DietComment = { index=(IS_OPTN | IS_NLBL), read=FDietComment, write=SetDietComment, stored = DietComment_Specified };
  __property UnicodeString TreatComment = { index=(IS_OPTN | IS_NLBL), read=FTreatComment, write=SetTreatComment, stored = TreatComment_Specified };
  __property UnicodeString WorkComment = { index=(IS_OPTN | IS_NLBL), read=FWorkComment, write=SetWorkComment, stored = WorkComment_Specified };
  __property UnicodeString OtherComment = { index=(IS_OPTN | IS_NLBL), read=FOtherComment, write=SetOtherComment, stored = OtherComment_Specified };
};




// ************************************************************************ //
// XML       : CaseStat, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case
// ************************************************************************ //
class CaseStat : public CaseStat2 {
private:
__published:
};




// ************************************************************************ //
// XML       : CaseAmb, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case
// ************************************************************************ //
class CaseAmb2 : public CaseBase2 {
private:
  unsigned char   FIdCasePurpose;
  bool            FIdCasePurpose_Specified;
  unsigned char   FIdCaseType;
  bool            FIdCaseType_Specified;
  unsigned char   FIdAmbResult;
  bool            FIdAmbResult_Specified;
  bool            FIsActive;
  bool            FIsActive_Specified;
  ArrayOfStepAmb  FSteps;
  bool            FSteps_Specified;
  ArrayOfMedRecord FMedRecords;
  bool            FMedRecords_Specified;
  void __fastcall SetIdCasePurpose(int Index, unsigned char _prop_val)
  {  FIdCasePurpose = _prop_val; FIdCasePurpose_Specified = true;  }
  bool __fastcall IdCasePurpose_Specified(int Index)
  {  return FIdCasePurpose_Specified;  }
  void __fastcall SetIdCaseType(int Index, unsigned char _prop_val)
  {  FIdCaseType = _prop_val; FIdCaseType_Specified = true;  }
  bool __fastcall IdCaseType_Specified(int Index)
  {  return FIdCaseType_Specified;  }
  void __fastcall SetIdAmbResult(int Index, unsigned char _prop_val)
  {  FIdAmbResult = _prop_val; FIdAmbResult_Specified = true;  }
  bool __fastcall IdAmbResult_Specified(int Index)
  {  return FIdAmbResult_Specified;  }
  void __fastcall SetIsActive(int Index, bool _prop_val)
  {  FIsActive = _prop_val; FIsActive_Specified = true;  }
  bool __fastcall IsActive_Specified(int Index)
  {  return FIsActive_Specified;  }
  void __fastcall SetSteps(int Index, ArrayOfStepAmb _prop_val)
  {  FSteps = _prop_val; FSteps_Specified = true;  }
  bool __fastcall Steps_Specified(int Index)
  {  return FSteps_Specified;  }
  void __fastcall SetMedRecords(int Index, ArrayOfMedRecord _prop_val)
  {  FMedRecords = _prop_val; FMedRecords_Specified = true;  }
  bool __fastcall MedRecords_Specified(int Index)
  {  return FMedRecords_Specified;  }

public:
  __fastcall ~CaseAmb2();
__published:
  __property unsigned char IdCasePurpose = { index=(IS_OPTN), read=FIdCasePurpose, write=SetIdCasePurpose, stored = IdCasePurpose_Specified };
  __property unsigned char IdCaseType = { index=(IS_OPTN), read=FIdCaseType, write=SetIdCaseType, stored = IdCaseType_Specified };
  __property unsigned char IdAmbResult = { index=(IS_OPTN), read=FIdAmbResult, write=SetIdAmbResult, stored = IdAmbResult_Specified };
  __property bool         IsActive = { index=(IS_OPTN), read=FIsActive, write=SetIsActive, stored = IsActive_Specified };
  __property ArrayOfStepAmb      Steps = { index=(IS_OPTN | IS_NLBL), read=FSteps, write=SetSteps, stored = Steps_Specified };
  __property ArrayOfMedRecord MedRecords = { index=(IS_OPTN | IS_NLBL), read=FMedRecords, write=SetMedRecords, stored = MedRecords_Specified };
};




// ************************************************************************ //
// XML       : CaseAmb, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case
// ************************************************************************ //
class CaseAmb : public CaseAmb2 {
private:
__published:
};




// ************************************************************************ //
// XML       : CaseAcps, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case
// ************************************************************************ //
class CaseAcps2 : public CaseBase2 {
private:
  ArrayOfMedRecord FMedRecords;
  bool            FMedRecords_Specified;
  void __fastcall SetMedRecords(int Index, ArrayOfMedRecord _prop_val)
  {  FMedRecords = _prop_val; FMedRecords_Specified = true;  }
  bool __fastcall MedRecords_Specified(int Index)
  {  return FMedRecords_Specified;  }

public:
  __fastcall ~CaseAcps2();
__published:
  __property ArrayOfMedRecord MedRecords = { index=(IS_OPTN | IS_NLBL), read=FMedRecords, write=SetMedRecords, stored = MedRecords_Specified };
};




// ************************************************************************ //
// XML       : CaseAcps, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case
// ************************************************************************ //
class CaseAcps : public CaseAcps2 {
private:
__published:
};




// ************************************************************************ //
// XML       : CaseTmc, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case
// ************************************************************************ //
class CaseTmc2 : public CaseBase2 {
private:
  UnicodeString   FTmcID;
  bool            FTmcID_Specified;
  unsigned char   FTmcForm;
  bool            FTmcForm_Specified;
  unsigned char   FTmcGoal;
  bool            FTmcGoal_Specified;
  Initiator2*     FInitiator;
  bool            FInitiator_Specified;
  ArrayOfMedRecord FMedRecords;
  bool            FMedRecords_Specified;
  void __fastcall SetTmcID(int Index, UnicodeString _prop_val)
  {  FTmcID = _prop_val; FTmcID_Specified = true;  }
  bool __fastcall TmcID_Specified(int Index)
  {  return FTmcID_Specified;  }
  void __fastcall SetTmcForm(int Index, unsigned char _prop_val)
  {  FTmcForm = _prop_val; FTmcForm_Specified = true;  }
  bool __fastcall TmcForm_Specified(int Index)
  {  return FTmcForm_Specified;  }
  void __fastcall SetTmcGoal(int Index, unsigned char _prop_val)
  {  FTmcGoal = _prop_val; FTmcGoal_Specified = true;  }
  bool __fastcall TmcGoal_Specified(int Index)
  {  return FTmcGoal_Specified;  }
  void __fastcall SetInitiator(int Index, Initiator2* _prop_val)
  {  FInitiator = _prop_val; FInitiator_Specified = true;  }
  bool __fastcall Initiator_Specified(int Index)
  {  return FInitiator_Specified;  }
  void __fastcall SetMedRecords(int Index, ArrayOfMedRecord _prop_val)
  {  FMedRecords = _prop_val; FMedRecords_Specified = true;  }
  bool __fastcall MedRecords_Specified(int Index)
  {  return FMedRecords_Specified;  }

public:
  __fastcall ~CaseTmc2();
__published:
  __property UnicodeString      TmcID = { index=(IS_OPTN | IS_NLBL), read=FTmcID, write=SetTmcID, stored = TmcID_Specified };
  __property unsigned char    TmcForm = { index=(IS_OPTN), read=FTmcForm, write=SetTmcForm, stored = TmcForm_Specified };
  __property unsigned char    TmcGoal = { index=(IS_OPTN), read=FTmcGoal, write=SetTmcGoal, stored = TmcGoal_Specified };
  __property Initiator2*  Initiator = { index=(IS_OPTN | IS_NLBL), read=FInitiator, write=SetInitiator, stored = Initiator_Specified };
  __property ArrayOfMedRecord MedRecords = { index=(IS_OPTN | IS_NLBL), read=FMedRecords, write=SetMedRecords, stored = MedRecords_Specified };
};




// ************************************************************************ //
// XML       : CaseTmc, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case
// ************************************************************************ //
class CaseTmc : public CaseTmc2 {
private:
__published:
};




// ************************************************************************ //
// XML       : StepAmb, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step
// ************************************************************************ //
class StepAmb2 : public StepBase2 {
private:
  unsigned char   FIdVisitPlace;
  bool            FIdVisitPlace_Specified;
  unsigned char   FIdVisitPurpose;
  bool            FIdVisitPurpose_Specified;
  ArrayOfMedRecord FMedRecords;
  bool            FMedRecords_Specified;
  void __fastcall SetIdVisitPlace(int Index, unsigned char _prop_val)
  {  FIdVisitPlace = _prop_val; FIdVisitPlace_Specified = true;  }
  bool __fastcall IdVisitPlace_Specified(int Index)
  {  return FIdVisitPlace_Specified;  }
  void __fastcall SetIdVisitPurpose(int Index, unsigned char _prop_val)
  {  FIdVisitPurpose = _prop_val; FIdVisitPurpose_Specified = true;  }
  bool __fastcall IdVisitPurpose_Specified(int Index)
  {  return FIdVisitPurpose_Specified;  }
  void __fastcall SetMedRecords(int Index, ArrayOfMedRecord _prop_val)
  {  FMedRecords = _prop_val; FMedRecords_Specified = true;  }
  bool __fastcall MedRecords_Specified(int Index)
  {  return FMedRecords_Specified;  }

public:
  __fastcall ~StepAmb2();
__published:
  __property unsigned char IdVisitPlace = { index=(IS_OPTN), read=FIdVisitPlace, write=SetIdVisitPlace, stored = IdVisitPlace_Specified };
  __property unsigned char IdVisitPurpose = { index=(IS_OPTN), read=FIdVisitPurpose, write=SetIdVisitPurpose, stored = IdVisitPurpose_Specified };
  __property ArrayOfMedRecord MedRecords = { index=(IS_OPTN | IS_NLBL), read=FMedRecords, write=SetMedRecords, stored = MedRecords_Specified };
};




// ************************************************************************ //
// XML       : StepAmb, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step
// ************************************************************************ //
class StepAmb : public StepAmb2 {
private:
__published:
};




// ************************************************************************ //
// XML       : StepStat, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step
// ************************************************************************ //
class StepStat2 : public StepBase2 {
private:
  UnicodeString   FHospitalDepartmentName;
  bool            FHospitalDepartmentName_Specified;
  UnicodeString   FIdHospitalDepartment;
  bool            FIdHospitalDepartment_Specified;
  unsigned char   FIdRegimen;
  bool            FIdRegimen_Specified;
  UnicodeString   FWardNumber;
  bool            FWardNumber_Specified;
  UnicodeString   FBedNumber;
  bool            FBedNumber_Specified;
  unsigned char   FBedProfile;
  bool            FBedProfile_Specified;
  unsigned short  FDaySpend;
  bool            FDaySpend_Specified;
  ArrayOfMedRecord FMedRecords;
  bool            FMedRecords_Specified;
  void __fastcall SetHospitalDepartmentName(int Index, UnicodeString _prop_val)
  {  FHospitalDepartmentName = _prop_val; FHospitalDepartmentName_Specified = true;  }
  bool __fastcall HospitalDepartmentName_Specified(int Index)
  {  return FHospitalDepartmentName_Specified;  }
  void __fastcall SetIdHospitalDepartment(int Index, UnicodeString _prop_val)
  {  FIdHospitalDepartment = _prop_val; FIdHospitalDepartment_Specified = true;  }
  bool __fastcall IdHospitalDepartment_Specified(int Index)
  {  return FIdHospitalDepartment_Specified;  }
  void __fastcall SetIdRegimen(int Index, unsigned char _prop_val)
  {  FIdRegimen = _prop_val; FIdRegimen_Specified = true;  }
  bool __fastcall IdRegimen_Specified(int Index)
  {  return FIdRegimen_Specified;  }
  void __fastcall SetWardNumber(int Index, UnicodeString _prop_val)
  {  FWardNumber = _prop_val; FWardNumber_Specified = true;  }
  bool __fastcall WardNumber_Specified(int Index)
  {  return FWardNumber_Specified;  }
  void __fastcall SetBedNumber(int Index, UnicodeString _prop_val)
  {  FBedNumber = _prop_val; FBedNumber_Specified = true;  }
  bool __fastcall BedNumber_Specified(int Index)
  {  return FBedNumber_Specified;  }
  void __fastcall SetBedProfile(int Index, unsigned char _prop_val)
  {  FBedProfile = _prop_val; FBedProfile_Specified = true;  }
  bool __fastcall BedProfile_Specified(int Index)
  {  return FBedProfile_Specified;  }
  void __fastcall SetDaySpend(int Index, unsigned short _prop_val)
  {  FDaySpend = _prop_val; FDaySpend_Specified = true;  }
  bool __fastcall DaySpend_Specified(int Index)
  {  return FDaySpend_Specified;  }
  void __fastcall SetMedRecords(int Index, ArrayOfMedRecord _prop_val)
  {  FMedRecords = _prop_val; FMedRecords_Specified = true;  }
  bool __fastcall MedRecords_Specified(int Index)
  {  return FMedRecords_Specified;  }

public:
  __fastcall ~StepStat2();
__published:
  __property UnicodeString HospitalDepartmentName = { index=(IS_OPTN | IS_NLBL), read=FHospitalDepartmentName, write=SetHospitalDepartmentName, stored = HospitalDepartmentName_Specified };
  __property UnicodeString IdHospitalDepartment = { index=(IS_OPTN | IS_NLBL), read=FIdHospitalDepartment, write=SetIdHospitalDepartment, stored = IdHospitalDepartment_Specified };
  __property unsigned char  IdRegimen = { index=(IS_OPTN), read=FIdRegimen, write=SetIdRegimen, stored = IdRegimen_Specified };
  __property UnicodeString WardNumber = { index=(IS_OPTN | IS_NLBL), read=FWardNumber, write=SetWardNumber, stored = WardNumber_Specified };
  __property UnicodeString  BedNumber = { index=(IS_OPTN | IS_NLBL), read=FBedNumber, write=SetBedNumber, stored = BedNumber_Specified };
  __property unsigned char BedProfile = { index=(IS_OPTN), read=FBedProfile, write=SetBedProfile, stored = BedProfile_Specified };
  __property unsigned short   DaySpend = { index=(IS_OPTN), read=FDaySpend, write=SetDaySpend, stored = DaySpend_Specified };
  __property ArrayOfMedRecord MedRecords = { index=(IS_OPTN | IS_NLBL), read=FMedRecords, write=SetMedRecords, stored = MedRecords_Specified };
};




// ************************************************************************ //
// XML       : StepStat, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step
// ************************************************************************ //
class StepStat : public StepStat2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Param, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Param : public Param2 {
private:
__published:
};




// ************************************************************************ //
// XML       : DeathInfo, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class DeathInfo : public DeathInfo2 {
private:
__published:
};




// ************************************************************************ //
// XML       : MedRecord, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class MedRecord : public MedRecord2 {
private:
__published:
};




// ************************************************************************ //
// XML       : TfomsInfo, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class TfomsInfo : public TfomsInfo2 {
private:
__published:
};




// ************************************************************************ //
// XML       : AppointedMedication, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class AppointedMedication : public AppointedMedication2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Problem, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Problem : public Problem2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Privilege, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Privilege2 : public TRemotable {
private:
  int             FCode;
  bool            FCode_Specified;
  TXSDateTime*    FStart;
  bool            FStart_Specified;
  TXSDateTime*    FEnd;
  bool            FEnd_Specified;
  void __fastcall SetCode(int Index, int _prop_val)
  {  FCode = _prop_val; FCode_Specified = true;  }
  bool __fastcall Code_Specified(int Index)
  {  return FCode_Specified;  }
  void __fastcall SetStart(int Index, TXSDateTime* _prop_val)
  {  FStart = _prop_val; FStart_Specified = true;  }
  bool __fastcall Start_Specified(int Index)
  {  return FStart_Specified;  }
  void __fastcall SetEnd(int Index, TXSDateTime* _prop_val)
  {  FEnd = _prop_val; FEnd_Specified = true;  }
  bool __fastcall End_Specified(int Index)
  {  return FEnd_Specified;  }

public:
  __fastcall ~Privilege2();
__published:
  __property int              Code = { index=(IS_OPTN), read=FCode, write=SetCode, stored = Code_Specified };
  __property TXSDateTime*      Start = { index=(IS_OPTN), read=FStart, write=SetStart, stored = Start_Specified };
  __property TXSDateTime*        End = { index=(IS_OPTN), read=FEnd, write=SetEnd, stored = End_Specified };
};




// ************************************************************************ //
// XML       : Privilege, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Privilege : public Privilege2 {
private:
__published:
};


typedef DynamicArray<int>         ArrayOfint;     /* "http://schemas.microsoft.com/2003/10/Serialization/Arrays"[GblCplx] */


// ************************************************************************ //
// XML       : ImmunizationType, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class ImmunizationType2 : public TRemotable {
private:
__published:
};




// ************************************************************************ //
// XML       : ReactionType, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class ReactionType2 : public ImmunizationType2 {
private:
__published:
};




// ************************************************************************ //
// XML       : ReactionType, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class ReactionType : public ReactionType2 {
private:
__published:
};




// ************************************************************************ //
// XML       : ImmunizationType, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class ImmunizationType : public ImmunizationType2 {
private:
__published:
};




// ************************************************************************ //
// XML       : NonDrugTreatment, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class NonDrugTreatment2 : public MedRecord2 {
private:
  UnicodeString   FName;
  bool            FName_Specified;
  UnicodeString   FScheme;
  bool            FScheme_Specified;
  TXSDateTime*    FStart;
  bool            FStart_Specified;
  TXSDateTime*    FEnd;
  bool            FEnd_Specified;
  void __fastcall SetName(int Index, UnicodeString _prop_val)
  {  FName = _prop_val; FName_Specified = true;  }
  bool __fastcall Name_Specified(int Index)
  {  return FName_Specified;  }
  void __fastcall SetScheme(int Index, UnicodeString _prop_val)
  {  FScheme = _prop_val; FScheme_Specified = true;  }
  bool __fastcall Scheme_Specified(int Index)
  {  return FScheme_Specified;  }
  void __fastcall SetStart(int Index, TXSDateTime* _prop_val)
  {  FStart = _prop_val; FStart_Specified = true;  }
  bool __fastcall Start_Specified(int Index)
  {  return FStart_Specified;  }
  void __fastcall SetEnd(int Index, TXSDateTime* _prop_val)
  {  FEnd = _prop_val; FEnd_Specified = true;  }
  bool __fastcall End_Specified(int Index)
  {  return FEnd_Specified;  }

public:
  __fastcall ~NonDrugTreatment2();
__published:
  __property UnicodeString       Name = { index=(IS_OPTN | IS_NLBL), read=FName, write=SetName, stored = Name_Specified };
  __property UnicodeString     Scheme = { index=(IS_OPTN | IS_NLBL), read=FScheme, write=SetScheme, stored = Scheme_Specified };
  __property TXSDateTime*      Start = { index=(IS_OPTN | IS_NLBL), read=FStart, write=SetStart, stored = Start_Specified };
  __property TXSDateTime*        End = { index=(IS_OPTN | IS_NLBL), read=FEnd, write=SetEnd, stored = End_Specified };
};




// ************************************************************************ //
// XML       : NonDrugTreatment, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class NonDrugTreatment : public NonDrugTreatment2 {
private:
__published:
};




// ************************************************************************ //
// XML       : PaymentInfo, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class PaymentInfo2 : public TRemotable {
private:
  unsigned char   FHealthCareUnit;
  bool            FHealthCareUnit_Specified;
  unsigned char   FIdPaymentType;
  bool            FIdPaymentType_Specified;
  unsigned char   FPaymentState;
  bool            FPaymentState_Specified;
  int             FQuantity;
  bool            FQuantity_Specified;
  TXSDecimal*     FTariff;
  bool            FTariff_Specified;
  void __fastcall SetHealthCareUnit(int Index, unsigned char _prop_val)
  {  FHealthCareUnit = _prop_val; FHealthCareUnit_Specified = true;  }
  bool __fastcall HealthCareUnit_Specified(int Index)
  {  return FHealthCareUnit_Specified;  }
  void __fastcall SetIdPaymentType(int Index, unsigned char _prop_val)
  {  FIdPaymentType = _prop_val; FIdPaymentType_Specified = true;  }
  bool __fastcall IdPaymentType_Specified(int Index)
  {  return FIdPaymentType_Specified;  }
  void __fastcall SetPaymentState(int Index, unsigned char _prop_val)
  {  FPaymentState = _prop_val; FPaymentState_Specified = true;  }
  bool __fastcall PaymentState_Specified(int Index)
  {  return FPaymentState_Specified;  }
  void __fastcall SetQuantity(int Index, int _prop_val)
  {  FQuantity = _prop_val; FQuantity_Specified = true;  }
  bool __fastcall Quantity_Specified(int Index)
  {  return FQuantity_Specified;  }
  void __fastcall SetTariff(int Index, TXSDecimal* _prop_val)
  {  FTariff = _prop_val; FTariff_Specified = true;  }
  bool __fastcall Tariff_Specified(int Index)
  {  return FTariff_Specified;  }

public:
  __fastcall ~PaymentInfo2();
__published:
  __property unsigned char HealthCareUnit = { index=(IS_OPTN), read=FHealthCareUnit, write=SetHealthCareUnit, stored = HealthCareUnit_Specified };
  __property unsigned char IdPaymentType = { index=(IS_OPTN), read=FIdPaymentType, write=SetIdPaymentType, stored = IdPaymentType_Specified };
  __property unsigned char PaymentState = { index=(IS_OPTN), read=FPaymentState, write=SetPaymentState, stored = PaymentState_Specified };
  __property int          Quantity = { index=(IS_OPTN), read=FQuantity, write=SetQuantity, stored = Quantity_Specified };
  __property TXSDecimal*     Tariff = { index=(IS_OPTN), read=FTariff, write=SetTariff, stored = Tariff_Specified };
};




// ************************************************************************ //
// XML       : PaymentInfo, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class PaymentInfo : public PaymentInfo2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Service, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Service2 : public MedRecord2 {
private:
  TXSDateTime*    FDateEnd;
  bool            FDateEnd_Specified;
  TXSDateTime*    FDateStart;
  bool            FDateStart_Specified;
  UnicodeString   FIdServiceType;
  bool            FIdServiceType_Specified;
  ArrayOfParam    FParams;
  bool            FParams_Specified;
  PaymentInfo2*   FPaymentInfo;
  bool            FPaymentInfo_Specified;
  Participant2*   FPerformer;
  bool            FPerformer_Specified;
  UnicodeString   FServiceName;
  bool            FServiceName_Specified;
  bool            FStatus;
  bool            FStatus_Specified;
  void __fastcall SetDateEnd(int Index, TXSDateTime* _prop_val)
  {  FDateEnd = _prop_val; FDateEnd_Specified = true;  }
  bool __fastcall DateEnd_Specified(int Index)
  {  return FDateEnd_Specified;  }
  void __fastcall SetDateStart(int Index, TXSDateTime* _prop_val)
  {  FDateStart = _prop_val; FDateStart_Specified = true;  }
  bool __fastcall DateStart_Specified(int Index)
  {  return FDateStart_Specified;  }
  void __fastcall SetIdServiceType(int Index, UnicodeString _prop_val)
  {  FIdServiceType = _prop_val; FIdServiceType_Specified = true;  }
  bool __fastcall IdServiceType_Specified(int Index)
  {  return FIdServiceType_Specified;  }
  void __fastcall SetParams(int Index, ArrayOfParam _prop_val)
  {  FParams = _prop_val; FParams_Specified = true;  }
  bool __fastcall Params_Specified(int Index)
  {  return FParams_Specified;  }
  void __fastcall SetPaymentInfo(int Index, PaymentInfo2* _prop_val)
  {  FPaymentInfo = _prop_val; FPaymentInfo_Specified = true;  }
  bool __fastcall PaymentInfo_Specified(int Index)
  {  return FPaymentInfo_Specified;  }
  void __fastcall SetPerformer(int Index, Participant2* _prop_val)
  {  FPerformer = _prop_val; FPerformer_Specified = true;  }
  bool __fastcall Performer_Specified(int Index)
  {  return FPerformer_Specified;  }
  void __fastcall SetServiceName(int Index, UnicodeString _prop_val)
  {  FServiceName = _prop_val; FServiceName_Specified = true;  }
  bool __fastcall ServiceName_Specified(int Index)
  {  return FServiceName_Specified;  }
  void __fastcall SetStatus(int Index, bool _prop_val)
  {  FStatus = _prop_val; FStatus_Specified = true;  }
  bool __fastcall Status_Specified(int Index)
  {  return FStatus_Specified;  }

public:
  __fastcall ~Service2();
__published:
  __property TXSDateTime*    DateEnd = { index=(IS_OPTN), read=FDateEnd, write=SetDateEnd, stored = DateEnd_Specified };
  __property TXSDateTime*  DateStart = { index=(IS_OPTN), read=FDateStart, write=SetDateStart, stored = DateStart_Specified };
  __property UnicodeString IdServiceType = { index=(IS_OPTN | IS_NLBL), read=FIdServiceType, write=SetIdServiceType, stored = IdServiceType_Specified };
  __property ArrayOfParam     Params = { index=(IS_OPTN | IS_NLBL), read=FParams, write=SetParams, stored = Params_Specified };
  __property PaymentInfo2* PaymentInfo = { index=(IS_OPTN | IS_NLBL), read=FPaymentInfo, write=SetPaymentInfo, stored = PaymentInfo_Specified };
  __property Participant2*  Performer = { index=(IS_OPTN | IS_NLBL), read=FPerformer, write=SetPerformer, stored = Performer_Specified };
  __property UnicodeString ServiceName = { index=(IS_OPTN | IS_NLBL), read=FServiceName, write=SetServiceName, stored = ServiceName_Specified };
  __property bool           Status = { index=(IS_OPTN | IS_NLBL), read=FStatus, write=SetStatus, stored = Status_Specified };
};




// ************************************************************************ //
// XML       : Service, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Service : public Service2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Immunize, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Immunize2 : public MedRecord2 {
private:
  MedicalStaff2*  FPerformer;
  bool            FPerformer_Specified;
  void __fastcall SetPerformer(int Index, MedicalStaff2* _prop_val)
  {  FPerformer = _prop_val; FPerformer_Specified = true;  }
  bool __fastcall Performer_Specified(int Index)
  {  return FPerformer_Specified;  }

public:
  __fastcall ~Immunize2();
__published:
  __property MedicalStaff2*  Performer = { index=(IS_OPTN | IS_NLBL), read=FPerformer, write=SetPerformer, stored = Performer_Specified };
};




// ************************************************************************ //
// XML       : Vaccination, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Vaccination2 : public Immunize2 {
private:
  UnicodeString   FLpu;
  bool            FLpu_Specified;
  TXSDateTime*    FDate;
  bool            FDate_Specified;
  int             FCode;
  bool            FCode_Specified;
  UnicodeString   FVacType;
  bool            FVacType_Specified;
  ArrayOfint      FInf;
  bool            FInf_Specified;
  UnicodeString   FIdVaccinationMis;
  bool            FIdVaccinationMis_Specified;
  int             FStatus;
  bool            FStatus_Specified;
  void __fastcall SetLpu(int Index, UnicodeString _prop_val)
  {  FLpu = _prop_val; FLpu_Specified = true;  }
  bool __fastcall Lpu_Specified(int Index)
  {  return FLpu_Specified;  }
  void __fastcall SetDate(int Index, TXSDateTime* _prop_val)
  {  FDate = _prop_val; FDate_Specified = true;  }
  bool __fastcall Date_Specified(int Index)
  {  return FDate_Specified;  }
  void __fastcall SetCode(int Index, int _prop_val)
  {  FCode = _prop_val; FCode_Specified = true;  }
  bool __fastcall Code_Specified(int Index)
  {  return FCode_Specified;  }
  void __fastcall SetVacType(int Index, UnicodeString _prop_val)
  {  FVacType = _prop_val; FVacType_Specified = true;  }
  bool __fastcall VacType_Specified(int Index)
  {  return FVacType_Specified;  }
  void __fastcall SetInf(int Index, ArrayOfint _prop_val)
  {  FInf = _prop_val; FInf_Specified = true;  }
  bool __fastcall Inf_Specified(int Index)
  {  return FInf_Specified;  }
  void __fastcall SetIdVaccinationMis(int Index, UnicodeString _prop_val)
  {  FIdVaccinationMis = _prop_val; FIdVaccinationMis_Specified = true;  }
  bool __fastcall IdVaccinationMis_Specified(int Index)
  {  return FIdVaccinationMis_Specified;  }
  void __fastcall SetStatus(int Index, int _prop_val)
  {  FStatus = _prop_val; FStatus_Specified = true;  }
  bool __fastcall Status_Specified(int Index)
  {  return FStatus_Specified;  }

public:
  __fastcall ~Vaccination2();
__published:
  __property UnicodeString        Lpu = { index=(IS_OPTN | IS_NLBL), read=FLpu, write=SetLpu, stored = Lpu_Specified };
  __property TXSDateTime*       Date = { index=(IS_OPTN), read=FDate, write=SetDate, stored = Date_Specified };
  __property int              Code = { index=(IS_OPTN), read=FCode, write=SetCode, stored = Code_Specified };
  __property UnicodeString    VacType = { index=(IS_OPTN | IS_NLBL), read=FVacType, write=SetVacType, stored = VacType_Specified };
  __property ArrayOfint        Inf = { index=(IS_OPTN | IS_NLBL), read=FInf, write=SetInf, stored = Inf_Specified };
  __property UnicodeString IdVaccinationMis = { index=(IS_OPTN | IS_NLBL), read=FIdVaccinationMis, write=SetIdVaccinationMis, stored = IdVaccinationMis_Specified };
  __property int            Status = { index=(IS_OPTN), read=FStatus, write=SetStatus, stored = Status_Specified };
};




// ************************************************************************ //
// XML       : Vaccination, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Vaccination : public Vaccination2 {
private:
__published:
};




// ************************************************************************ //
// XML       : ImmunizationPlan, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class ImmunizationPlan2 : public Immunize2 {
private:
  ImmunizationType2* FImmunizationType;
  bool            FImmunizationType_Specified;
  ArrayOfint      FInf;
  bool            FInf_Specified;
  TXSDateTime*    FDate;
  bool            FDate_Specified;
  UnicodeString   FVacType;
  bool            FVacType_Specified;
  void __fastcall SetImmunizationType(int Index, ImmunizationType2* _prop_val)
  {  FImmunizationType = _prop_val; FImmunizationType_Specified = true;  }
  bool __fastcall ImmunizationType_Specified(int Index)
  {  return FImmunizationType_Specified;  }
  void __fastcall SetInf(int Index, ArrayOfint _prop_val)
  {  FInf = _prop_val; FInf_Specified = true;  }
  bool __fastcall Inf_Specified(int Index)
  {  return FInf_Specified;  }
  void __fastcall SetDate(int Index, TXSDateTime* _prop_val)
  {  FDate = _prop_val; FDate_Specified = true;  }
  bool __fastcall Date_Specified(int Index)
  {  return FDate_Specified;  }
  void __fastcall SetVacType(int Index, UnicodeString _prop_val)
  {  FVacType = _prop_val; FVacType_Specified = true;  }
  bool __fastcall VacType_Specified(int Index)
  {  return FVacType_Specified;  }

public:
  __fastcall ~ImmunizationPlan2();
__published:
  __property ImmunizationType2* ImmunizationType = { index=(IS_OPTN | IS_NLBL), read=FImmunizationType, write=SetImmunizationType, stored = ImmunizationType_Specified };
  __property ArrayOfint        Inf = { index=(IS_OPTN | IS_NLBL), read=FInf, write=SetInf, stored = Inf_Specified };
  __property TXSDateTime*       Date = { index=(IS_OPTN), read=FDate, write=SetDate, stored = Date_Specified };
  __property UnicodeString    VacType = { index=(IS_OPTN | IS_NLBL), read=FVacType, write=SetVacType, stored = VacType_Specified };
};




// ************************************************************************ //
// XML       : ImmunizationPlan, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class ImmunizationPlan : public ImmunizationPlan2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Reaction, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Reaction2 : public Immunize2 {
private:
  UnicodeString   FLpu;
  bool            FLpu_Specified;
  int             FReactionType;
  bool            FReactionType_Specified;
  TXSDateTime*    FDate;
  bool            FDate_Specified;
  int             FCode;
  bool            FCode_Specified;
  ArrayOfint      FInf;
  bool            FInf_Specified;
  int             FMedicalReactionType;
  bool            FMedicalReactionType_Specified;
  UnicodeString   FMedicalReactionValue;
  bool            FMedicalReactionValue_Specified;
  UnicodeString   FIdReactionMis;
  bool            FIdReactionMis_Specified;
  int             FStatus;
  bool            FStatus_Specified;
  void __fastcall SetLpu(int Index, UnicodeString _prop_val)
  {  FLpu = _prop_val; FLpu_Specified = true;  }
  bool __fastcall Lpu_Specified(int Index)
  {  return FLpu_Specified;  }
  void __fastcall SetReactionType(int Index, int _prop_val)
  {  FReactionType = _prop_val; FReactionType_Specified = true;  }
  bool __fastcall ReactionType_Specified(int Index)
  {  return FReactionType_Specified;  }
  void __fastcall SetDate(int Index, TXSDateTime* _prop_val)
  {  FDate = _prop_val; FDate_Specified = true;  }
  bool __fastcall Date_Specified(int Index)
  {  return FDate_Specified;  }
  void __fastcall SetCode(int Index, int _prop_val)
  {  FCode = _prop_val; FCode_Specified = true;  }
  bool __fastcall Code_Specified(int Index)
  {  return FCode_Specified;  }
  void __fastcall SetInf(int Index, ArrayOfint _prop_val)
  {  FInf = _prop_val; FInf_Specified = true;  }
  bool __fastcall Inf_Specified(int Index)
  {  return FInf_Specified;  }
  void __fastcall SetMedicalReactionType(int Index, int _prop_val)
  {  FMedicalReactionType = _prop_val; FMedicalReactionType_Specified = true;  }
  bool __fastcall MedicalReactionType_Specified(int Index)
  {  return FMedicalReactionType_Specified;  }
  void __fastcall SetMedicalReactionValue(int Index, UnicodeString _prop_val)
  {  FMedicalReactionValue = _prop_val; FMedicalReactionValue_Specified = true;  }
  bool __fastcall MedicalReactionValue_Specified(int Index)
  {  return FMedicalReactionValue_Specified;  }
  void __fastcall SetIdReactionMis(int Index, UnicodeString _prop_val)
  {  FIdReactionMis = _prop_val; FIdReactionMis_Specified = true;  }
  bool __fastcall IdReactionMis_Specified(int Index)
  {  return FIdReactionMis_Specified;  }
  void __fastcall SetStatus(int Index, int _prop_val)
  {  FStatus = _prop_val; FStatus_Specified = true;  }
  bool __fastcall Status_Specified(int Index)
  {  return FStatus_Specified;  }

public:
  __fastcall ~Reaction2();
__published:
  __property UnicodeString        Lpu = { index=(IS_OPTN | IS_NLBL), read=FLpu, write=SetLpu, stored = Lpu_Specified };
  __property int        ReactionType = { index=(IS_OPTN), read=FReactionType, write=SetReactionType, stored = ReactionType_Specified };
  __property TXSDateTime*       Date = { index=(IS_OPTN), read=FDate, write=SetDate, stored = Date_Specified };
  __property int              Code = { index=(IS_OPTN), read=FCode, write=SetCode, stored = Code_Specified };
  __property ArrayOfint        Inf = { index=(IS_OPTN | IS_NLBL), read=FInf, write=SetInf, stored = Inf_Specified };
  __property int        MedicalReactionType = { index=(IS_OPTN | IS_NLBL), read=FMedicalReactionType, write=SetMedicalReactionType, stored = MedicalReactionType_Specified };
  __property UnicodeString MedicalReactionValue = { index=(IS_OPTN | IS_NLBL), read=FMedicalReactionValue, write=SetMedicalReactionValue, stored = MedicalReactionValue_Specified };
  __property UnicodeString IdReactionMis = { index=(IS_OPTN | IS_NLBL), read=FIdReactionMis, write=SetIdReactionMis, stored = IdReactionMis_Specified };
  __property int            Status = { index=(IS_OPTN), read=FStatus, write=SetStatus, stored = Status_Specified };
};




// ************************************************************************ //
// XML       : Reaction, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Reaction : public Reaction2 {
private:
__published:
};




// ************************************************************************ //
// XML       : MedicalExemption, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class MedicalExemption2 : public Immunize2 {
private:
  TXSDateTime*    FDateStart;
  bool            FDateStart_Specified;
  TXSDateTime*    FDateEnd;
  bool            FDateEnd_Specified;
  int             FExemptionType;
  bool            FExemptionType_Specified;
  ArrayOfint      FExemptionInf;
  bool            FExemptionInf_Specified;
  int             FExemptionReason;
  bool            FExemptionReason_Specified;
  UnicodeString   FExemptionDisease;
  bool            FExemptionDisease_Specified;
  UnicodeString   FIdMedicalExemptionMis;
  bool            FIdMedicalExemptionMis_Specified;
  int             FStatus;
  bool            FStatus_Specified;
  void __fastcall SetDateStart(int Index, TXSDateTime* _prop_val)
  {  FDateStart = _prop_val; FDateStart_Specified = true;  }
  bool __fastcall DateStart_Specified(int Index)
  {  return FDateStart_Specified;  }
  void __fastcall SetDateEnd(int Index, TXSDateTime* _prop_val)
  {  FDateEnd = _prop_val; FDateEnd_Specified = true;  }
  bool __fastcall DateEnd_Specified(int Index)
  {  return FDateEnd_Specified;  }
  void __fastcall SetExemptionType(int Index, int _prop_val)
  {  FExemptionType = _prop_val; FExemptionType_Specified = true;  }
  bool __fastcall ExemptionType_Specified(int Index)
  {  return FExemptionType_Specified;  }
  void __fastcall SetExemptionInf(int Index, ArrayOfint _prop_val)
  {  FExemptionInf = _prop_val; FExemptionInf_Specified = true;  }
  bool __fastcall ExemptionInf_Specified(int Index)
  {  return FExemptionInf_Specified;  }
  void __fastcall SetExemptionReason(int Index, int _prop_val)
  {  FExemptionReason = _prop_val; FExemptionReason_Specified = true;  }
  bool __fastcall ExemptionReason_Specified(int Index)
  {  return FExemptionReason_Specified;  }
  void __fastcall SetExemptionDisease(int Index, UnicodeString _prop_val)
  {  FExemptionDisease = _prop_val; FExemptionDisease_Specified = true;  }
  bool __fastcall ExemptionDisease_Specified(int Index)
  {  return FExemptionDisease_Specified;  }
  void __fastcall SetIdMedicalExemptionMis(int Index, UnicodeString _prop_val)
  {  FIdMedicalExemptionMis = _prop_val; FIdMedicalExemptionMis_Specified = true;  }
  bool __fastcall IdMedicalExemptionMis_Specified(int Index)
  {  return FIdMedicalExemptionMis_Specified;  }
  void __fastcall SetStatus(int Index, int _prop_val)
  {  FStatus = _prop_val; FStatus_Specified = true;  }
  bool __fastcall Status_Specified(int Index)
  {  return FStatus_Specified;  }

public:
  __fastcall ~MedicalExemption2();
__published:
  __property TXSDateTime*  DateStart = { index=(IS_OPTN), read=FDateStart, write=SetDateStart, stored = DateStart_Specified };
  __property TXSDateTime*    DateEnd = { index=(IS_OPTN | IS_NLBL), read=FDateEnd, write=SetDateEnd, stored = DateEnd_Specified };
  __property int        ExemptionType = { index=(IS_OPTN), read=FExemptionType, write=SetExemptionType, stored = ExemptionType_Specified };
  __property ArrayOfint ExemptionInf = { index=(IS_OPTN | IS_NLBL), read=FExemptionInf, write=SetExemptionInf, stored = ExemptionInf_Specified };
  __property int        ExemptionReason = { index=(IS_OPTN), read=FExemptionReason, write=SetExemptionReason, stored = ExemptionReason_Specified };
  __property UnicodeString ExemptionDisease = { index=(IS_OPTN | IS_NLBL), read=FExemptionDisease, write=SetExemptionDisease, stored = ExemptionDisease_Specified };
  __property UnicodeString IdMedicalExemptionMis = { index=(IS_OPTN | IS_NLBL), read=FIdMedicalExemptionMis, write=SetIdMedicalExemptionMis, stored = IdMedicalExemptionMis_Specified };
  __property int            Status = { index=(IS_OPTN), read=FStatus, write=SetStatus, stored = Status_Specified };
};




// ************************************************************************ //
// XML       : MedicalExemption, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class MedicalExemption : public MedicalExemption2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Immunize, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Immunize : public Immunize2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Procedure, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Procedure2 : public MedRecord2 {
private:
  UnicodeString   FCode;
  bool            FCode_Specified;
  TXSDateTime*    FDate;
  bool            FDate_Specified;
  MedicalStaff2*  FPerformer;
  bool            FPerformer_Specified;
  void __fastcall SetCode(int Index, UnicodeString _prop_val)
  {  FCode = _prop_val; FCode_Specified = true;  }
  bool __fastcall Code_Specified(int Index)
  {  return FCode_Specified;  }
  void __fastcall SetDate(int Index, TXSDateTime* _prop_val)
  {  FDate = _prop_val; FDate_Specified = true;  }
  bool __fastcall Date_Specified(int Index)
  {  return FDate_Specified;  }
  void __fastcall SetPerformer(int Index, MedicalStaff2* _prop_val)
  {  FPerformer = _prop_val; FPerformer_Specified = true;  }
  bool __fastcall Performer_Specified(int Index)
  {  return FPerformer_Specified;  }

public:
  __fastcall ~Procedure2();
__published:
  __property UnicodeString       Code = { index=(IS_OPTN | IS_NLBL), read=FCode, write=SetCode, stored = Code_Specified };
  __property TXSDateTime*       Date = { index=(IS_OPTN | IS_NLBL), read=FDate, write=SetDate, stored = Date_Specified };
  __property MedicalStaff2*  Performer = { index=(IS_OPTN | IS_NLBL), read=FPerformer, write=SetPerformer, stored = Performer_Specified };
};




// ************************************************************************ //
// XML       : Procedure, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Procedure : public Procedure2 {
private:
__published:
};




// ************************************************************************ //
// XML       : AllergyBase, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class AllergyBase2 : public MedRecord2 {
private:
  int             FType;
  bool            FType_Specified;
  UnicodeString   FComment;
  bool            FComment_Specified;
  TXSDateTime*    FTime;
  bool            FTime_Specified;
  int             FReactionCode;
  bool            FReactionCode_Specified;
  int             FIdStep;
  bool            FIdStep_Specified;
  void __fastcall SetType(int Index, int _prop_val)
  {  FType = _prop_val; FType_Specified = true;  }
  bool __fastcall Type_Specified(int Index)
  {  return FType_Specified;  }
  void __fastcall SetComment(int Index, UnicodeString _prop_val)
  {  FComment = _prop_val; FComment_Specified = true;  }
  bool __fastcall Comment_Specified(int Index)
  {  return FComment_Specified;  }
  void __fastcall SetTime(int Index, TXSDateTime* _prop_val)
  {  FTime = _prop_val; FTime_Specified = true;  }
  bool __fastcall Time_Specified(int Index)
  {  return FTime_Specified;  }
  void __fastcall SetReactionCode(int Index, int _prop_val)
  {  FReactionCode = _prop_val; FReactionCode_Specified = true;  }
  bool __fastcall ReactionCode_Specified(int Index)
  {  return FReactionCode_Specified;  }
  void __fastcall SetIdStep(int Index, int _prop_val)
  {  FIdStep = _prop_val; FIdStep_Specified = true;  }
  bool __fastcall IdStep_Specified(int Index)
  {  return FIdStep_Specified;  }

public:
  __fastcall ~AllergyBase2();
__published:
  __property int              Type = { index=(IS_OPTN), read=FType, write=SetType, stored = Type_Specified };
  __property UnicodeString    Comment = { index=(IS_OPTN | IS_NLBL), read=FComment, write=SetComment, stored = Comment_Specified };
  __property TXSDateTime*       Time = { index=(IS_OPTN), read=FTime, write=SetTime, stored = Time_Specified };
  __property int        ReactionCode = { index=(IS_OPTN), read=FReactionCode, write=SetReactionCode, stored = ReactionCode_Specified };
  __property int            IdStep = { index=(IS_OPTN), read=FIdStep, write=SetIdStep, stored = IdStep_Specified };
};




// ************************************************************************ //
// XML       : AllergyNonDrug, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class AllergyNonDrug2 : public AllergyBase2 {
private:
  UnicodeString   FDescription;
  bool            FDescription_Specified;
  void __fastcall SetDescription(int Index, UnicodeString _prop_val)
  {  FDescription = _prop_val; FDescription_Specified = true;  }
  bool __fastcall Description_Specified(int Index)
  {  return FDescription_Specified;  }
__published:
  __property UnicodeString Description = { index=(IS_OPTN | IS_NLBL), read=FDescription, write=SetDescription, stored = Description_Specified };
};




// ************************************************************************ //
// XML       : AllergyNonDrug, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class AllergyNonDrug : public AllergyNonDrug2 {
private:
__published:
};




// ************************************************************************ //
// XML       : AllergyBase, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class AllergyBase : public AllergyBase2 {
private:
__published:
};




// ************************************************************************ //
// XML       : SocialGroup, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class SocialGroup2 : public TRemotable {
private:
  int             FCode;
  bool            FCode_Specified;
  UnicodeString   FText;
  bool            FText_Specified;
  void __fastcall SetCode(int Index, int _prop_val)
  {  FCode = _prop_val; FCode_Specified = true;  }
  bool __fastcall Code_Specified(int Index)
  {  return FCode_Specified;  }
  void __fastcall SetText(int Index, UnicodeString _prop_val)
  {  FText = _prop_val; FText_Specified = true;  }
  bool __fastcall Text_Specified(int Index)
  {  return FText_Specified;  }
__published:
  __property int              Code = { index=(IS_OPTN), read=FCode, write=SetCode, stored = Code_Specified };
  __property UnicodeString       Text = { index=(IS_OPTN | IS_NLBL), read=FText, write=SetText, stored = Text_Specified };
};




// ************************************************************************ //
// XML       : SocialGroup, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class SocialGroup : public SocialGroup2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Disability, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Disability2 : public TRemotable {
private:
  int             FCode;
  bool            FCode_Specified;
  TXSDateTime*    FDate;
  bool            FDate_Specified;
  int             FOrder;
  bool            FOrder_Specified;
  void __fastcall SetCode(int Index, int _prop_val)
  {  FCode = _prop_val; FCode_Specified = true;  }
  bool __fastcall Code_Specified(int Index)
  {  return FCode_Specified;  }
  void __fastcall SetDate(int Index, TXSDateTime* _prop_val)
  {  FDate = _prop_val; FDate_Specified = true;  }
  bool __fastcall Date_Specified(int Index)
  {  return FDate_Specified;  }
  void __fastcall SetOrder(int Index, int _prop_val)
  {  FOrder = _prop_val; FOrder_Specified = true;  }
  bool __fastcall Order_Specified(int Index)
  {  return FOrder_Specified;  }

public:
  __fastcall ~Disability2();
__published:
  __property int              Code = { index=(IS_OPTN), read=FCode, write=SetCode, stored = Code_Specified };
  __property TXSDateTime*       Date = { index=(IS_OPTN), read=FDate, write=SetDate, stored = Date_Specified };
  __property int             Order = { index=(IS_OPTN | IS_NLBL), read=FOrder, write=SetOrder, stored = Order_Specified };
};




// ************************************************************************ //
// XML       : Disability, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Disability : public Disability2 {
private:
__published:
};


typedef DynamicArray<Privilege2*> ArrayOfPrivilege; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec"[GblCplx] */


// ************************************************************************ //
// XML       : SocialAnamnesis, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class SocialAnamnesis2 : public MedRecord2 {
private:
  ArrayOfPrivilege FPrivileges;
  bool            FPrivileges_Specified;
  Disability2*    FDisability;
  bool            FDisability_Specified;
  ArrayOfint      FSocialRiskFactors;
  bool            FSocialRiskFactors_Specified;
  ArrayOfint      FOccupationalHazards;
  bool            FOccupationalHazards_Specified;
  ArrayOfint      FBadHabits;
  bool            FBadHabits_Specified;
  int             FRegistryArea;
  bool            FRegistryArea_Specified;
  SocialGroup2*   FSocialGroup;
  bool            FSocialGroup_Specified;
  void __fastcall SetPrivileges(int Index, ArrayOfPrivilege _prop_val)
  {  FPrivileges = _prop_val; FPrivileges_Specified = true;  }
  bool __fastcall Privileges_Specified(int Index)
  {  return FPrivileges_Specified;  }
  void __fastcall SetDisability(int Index, Disability2* _prop_val)
  {  FDisability = _prop_val; FDisability_Specified = true;  }
  bool __fastcall Disability_Specified(int Index)
  {  return FDisability_Specified;  }
  void __fastcall SetSocialRiskFactors(int Index, ArrayOfint _prop_val)
  {  FSocialRiskFactors = _prop_val; FSocialRiskFactors_Specified = true;  }
  bool __fastcall SocialRiskFactors_Specified(int Index)
  {  return FSocialRiskFactors_Specified;  }
  void __fastcall SetOccupationalHazards(int Index, ArrayOfint _prop_val)
  {  FOccupationalHazards = _prop_val; FOccupationalHazards_Specified = true;  }
  bool __fastcall OccupationalHazards_Specified(int Index)
  {  return FOccupationalHazards_Specified;  }
  void __fastcall SetBadHabits(int Index, ArrayOfint _prop_val)
  {  FBadHabits = _prop_val; FBadHabits_Specified = true;  }
  bool __fastcall BadHabits_Specified(int Index)
  {  return FBadHabits_Specified;  }
  void __fastcall SetRegistryArea(int Index, int _prop_val)
  {  FRegistryArea = _prop_val; FRegistryArea_Specified = true;  }
  bool __fastcall RegistryArea_Specified(int Index)
  {  return FRegistryArea_Specified;  }
  void __fastcall SetSocialGroup(int Index, SocialGroup2* _prop_val)
  {  FSocialGroup = _prop_val; FSocialGroup_Specified = true;  }
  bool __fastcall SocialGroup_Specified(int Index)
  {  return FSocialGroup_Specified;  }

public:
  __fastcall ~SocialAnamnesis2();
__published:
  __property ArrayOfPrivilege Privileges = { index=(IS_OPTN | IS_NLBL), read=FPrivileges, write=SetPrivileges, stored = Privileges_Specified };
  __property Disability2* Disability = { index=(IS_OPTN | IS_NLBL), read=FDisability, write=SetDisability, stored = Disability_Specified };
  __property ArrayOfint SocialRiskFactors = { index=(IS_OPTN | IS_NLBL), read=FSocialRiskFactors, write=SetSocialRiskFactors, stored = SocialRiskFactors_Specified };
  __property ArrayOfint OccupationalHazards = { index=(IS_OPTN | IS_NLBL), read=FOccupationalHazards, write=SetOccupationalHazards, stored = OccupationalHazards_Specified };
  __property ArrayOfint  BadHabits = { index=(IS_OPTN | IS_NLBL), read=FBadHabits, write=SetBadHabits, stored = BadHabits_Specified };
  __property int        RegistryArea = { index=(IS_OPTN), read=FRegistryArea, write=SetRegistryArea, stored = RegistryArea_Specified };
  __property SocialGroup2* SocialGroup = { index=(IS_OPTN | IS_NLBL), read=FSocialGroup, write=SetSocialGroup, stored = SocialGroup_Specified };
};




// ************************************************************************ //
// XML       : SocialAnamnesis, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class SocialAnamnesis : public SocialAnamnesis2 {
private:
__published:
};


typedef DynamicArray<ResInstr_Activity2*> ArrayOfResInstr_Activity; /* "http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec"[GblCplx] */


// ************************************************************************ //
// XML       : ResInstr, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class ResInstr2 : public MedRecord2 {
private:
  TXSDateTime*    FDate;
  bool            FDate_Specified;
  int             FType;
  bool            FType_Specified;
  int             FPriority;
  bool            FPriority_Specified;
  UnicodeString   FText;
  bool            FText_Specified;
  MedicalStaff2*  FPerformer;
  bool            FPerformer_Specified;
  ArrayOfResInstr_Activity FActivities;
  bool            FActivities_Specified;
  void __fastcall SetDate(int Index, TXSDateTime* _prop_val)
  {  FDate = _prop_val; FDate_Specified = true;  }
  bool __fastcall Date_Specified(int Index)
  {  return FDate_Specified;  }
  void __fastcall SetType(int Index, int _prop_val)
  {  FType = _prop_val; FType_Specified = true;  }
  bool __fastcall Type_Specified(int Index)
  {  return FType_Specified;  }
  void __fastcall SetPriority(int Index, int _prop_val)
  {  FPriority = _prop_val; FPriority_Specified = true;  }
  bool __fastcall Priority_Specified(int Index)
  {  return FPriority_Specified;  }
  void __fastcall SetText(int Index, UnicodeString _prop_val)
  {  FText = _prop_val; FText_Specified = true;  }
  bool __fastcall Text_Specified(int Index)
  {  return FText_Specified;  }
  void __fastcall SetPerformer(int Index, MedicalStaff2* _prop_val)
  {  FPerformer = _prop_val; FPerformer_Specified = true;  }
  bool __fastcall Performer_Specified(int Index)
  {  return FPerformer_Specified;  }
  void __fastcall SetActivities(int Index, ArrayOfResInstr_Activity _prop_val)
  {  FActivities = _prop_val; FActivities_Specified = true;  }
  bool __fastcall Activities_Specified(int Index)
  {  return FActivities_Specified;  }

public:
  __fastcall ~ResInstr2();
__published:
  __property TXSDateTime*       Date = { index=(IS_OPTN), read=FDate, write=SetDate, stored = Date_Specified };
  __property int              Type = { index=(IS_OPTN), read=FType, write=SetType, stored = Type_Specified };
  __property int          Priority = { index=(IS_OPTN), read=FPriority, write=SetPriority, stored = Priority_Specified };
  __property UnicodeString       Text = { index=(IS_OPTN | IS_NLBL), read=FText, write=SetText, stored = Text_Specified };
  __property MedicalStaff2*  Performer = { index=(IS_OPTN | IS_NLBL), read=FPerformer, write=SetPerformer, stored = Performer_Specified };
  __property ArrayOfResInstr_Activity Activities = { index=(IS_OPTN | IS_NLBL), read=FActivities, write=SetActivities, stored = Activities_Specified };
};




// ************************************************************************ //
// XML       : ResInstr, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class ResInstr : public ResInstr2 {
private:
__published:
};




// ************************************************************************ //
// XML       : VaccinationType, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class VaccinationType2 : public ImmunizationType2 {
private:
__published:
};




// ************************************************************************ //
// XML       : VaccinationType, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class VaccinationType : public VaccinationType2 {
private:
__published:
};




// ************************************************************************ //
// XML       : AllergyDrug, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class AllergyDrug2 : public AllergyBase2 {
private:
  int             FIdINN;
  bool            FIdINN_Specified;
  int             FId;
  bool            FId_Specified;
  void __fastcall SetIdINN(int Index, int _prop_val)
  {  FIdINN = _prop_val; FIdINN_Specified = true;  }
  bool __fastcall IdINN_Specified(int Index)
  {  return FIdINN_Specified;  }
  void __fastcall SetId(int Index, int _prop_val)
  {  FId = _prop_val; FId_Specified = true;  }
  bool __fastcall Id_Specified(int Index)
  {  return FId_Specified;  }
__published:
  __property int             IdINN = { index=(IS_OPTN), read=FIdINN, write=SetIdINN, stored = IdINN_Specified };
  __property int                Id = { index=(IS_OPTN), read=FId, write=SetId, stored = Id_Specified };
};




// ************************************************************************ //
// XML       : AllergyDrug, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class AllergyDrug : public AllergyDrug2 {
private:
__published:
};




// ************************************************************************ //
// XML       : Scores, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Scores2 : public MedRecord2 {
private:
  TXSDateTime*    FDate;
  bool            FDate_Specified;
  UnicodeString   FScale;
  bool            FScale_Specified;
  UnicodeString   FValue;
  bool            FValue_Specified;
  void __fastcall SetDate(int Index, TXSDateTime* _prop_val)
  {  FDate = _prop_val; FDate_Specified = true;  }
  bool __fastcall Date_Specified(int Index)
  {  return FDate_Specified;  }
  void __fastcall SetScale(int Index, UnicodeString _prop_val)
  {  FScale = _prop_val; FScale_Specified = true;  }
  bool __fastcall Scale_Specified(int Index)
  {  return FScale_Specified;  }
  void __fastcall SetValue(int Index, UnicodeString _prop_val)
  {  FValue = _prop_val; FValue_Specified = true;  }
  bool __fastcall Value_Specified(int Index)
  {  return FValue_Specified;  }

public:
  __fastcall ~Scores2();
__published:
  __property TXSDateTime*       Date = { index=(IS_OPTN), read=FDate, write=SetDate, stored = Date_Specified };
  __property UnicodeString      Scale = { index=(IS_OPTN | IS_NLBL), read=FScale, write=SetScale, stored = Scale_Specified };
  __property UnicodeString      Value = { index=(IS_OPTN | IS_NLBL), read=FValue, write=SetValue, stored = Value_Specified };
};




// ************************************************************************ //
// XML       : Scores, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class Scores : public Scores2 {
private:
__published:
};




// ************************************************************************ //
// XML       : ResInstr.Activity, global, <complexType>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class ResInstr_Activity2 : public TRemotable {
private:
  UnicodeString   FCode;
  bool            FCode_Specified;
  TXSDateTime*    FDate;
  bool            FDate_Specified;
  void __fastcall SetCode(int Index, UnicodeString _prop_val)
  {  FCode = _prop_val; FCode_Specified = true;  }
  bool __fastcall Code_Specified(int Index)
  {  return FCode_Specified;  }
  void __fastcall SetDate(int Index, TXSDateTime* _prop_val)
  {  FDate = _prop_val; FDate_Specified = true;  }
  bool __fastcall Date_Specified(int Index)
  {  return FDate_Specified;  }

public:
  __fastcall ~ResInstr_Activity2();
__published:
  __property UnicodeString       Code = { index=(IS_OPTN | IS_NLBL), read=FCode, write=SetCode, stored = Code_Specified };
  __property TXSDateTime*       Date = { index=(IS_OPTN), read=FDate, write=SetDate, stored = Date_Specified };
};




// ************************************************************************ //
// XML       : ResInstr.Activity, global, <element>
// Namespace : http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec
// ************************************************************************ //
class ResInstr_Activity : public ResInstr_Activity2 {
private:
__published:
};



// ************************************************************************ //
// Namespace : http://tempuri.org/
// soapAction: |http://tempuri.org/IServiceSupport/GetVersion|http://tempuri.org/IEmkService/AddCase|http://tempuri.org/IEmkService/CreateCase|http://tempuri.org/IEmkService/CloseCase|http://tempuri.org/IEmkService/UpdateCase|http://tempuri.org/IEmkService/AddMedRecord|http://tempuri.org/IEmkService/AddStepToCase|http://tempuri.org/IEmkService/CancelCase
// transport : http://schemas.xmlsoap.org/soap/http
// style     : document
// use       : literal
// binding   : BasicHttpBinding_IEmkService
// service   : EmkService
// port      : BasicHttpBinding_IEmkService
// URL       : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc
// ************************************************************************ //
__interface INTERFACE_UUID("{FFB3045B-A395-F8B2-73D7-CB853923F176}") IEmkService : public IInvokable
{
public:
  virtual VersionInfo2*   GetVersion() = 0;
  virtual void            AddCase(const UnicodeString guid, const CaseBase2* caseDto) = 0;
  virtual void            CreateCase(const UnicodeString guid, const CaseBase2* createCaseDto) = 0;
  virtual void            CloseCase(const UnicodeString guid, const CaseBase2* caseDto) = 0;
  virtual void            UpdateCase(const UnicodeString guid, const CaseBase2* caseDto) = 0;
  virtual void            AddMedRecord(const UnicodeString guid, const UnicodeString idLpu, const UnicodeString idPatientMis, const UnicodeString idCaseMis, const MedRecord2* medRecord) = 0;
  virtual void            AddStepToCase(const UnicodeString guid, const UnicodeString idLpu, const UnicodeString idPatientMis, const UnicodeString idCaseMis, const StepBase2* step) = 0;
  virtual void            CancelCase(const UnicodeString guid, const UnicodeString idLpu, const UnicodeString idPatientMis, const UnicodeString idCaseMis) = 0;
};
typedef DelphiInterface<IEmkService> _di_IEmkService;

_di_IEmkService GetIEmkService(bool useWSDL=false, System::String addr= System::String(), Soaphttpclient::THTTPRIO* HTTPRIO=0);
_di_IEmkService GetIEmkService(System::String WsdlAddr, Soaphttpclient::THTTPRIO * HTTPRIO=0);


};     // NS_EMKService

#if !defined(NO_IMPLICIT_NAMESPACE_USE)
using  namespace NS_EMKService;
#endif



#endif // EMKServiceH

