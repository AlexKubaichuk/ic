// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>0
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>1
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>2
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>3
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>4
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>5
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>6
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>7
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>8
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>9
//  >Import : http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singlewsdl>10
// Encoding : utf-8
// Codegen  : [wfOutputLiteralTypes+]
// Version  : 1.0
// (01.04.2020 22:01:55 - - $Rev: 69934 $)
// ************************************************************************ //

#include <System.hpp>
#pragma hdrstop

#include "EMKService.h"



namespace NS_EMKService {

 static const char * defSvc = "EmkService";
 static const char * defPrt = "BasicHttpBinding_IEmkService";
 _di_IEmkService GetIEmkService(bool useWSDL, System::String addr, Soaphttpclient::THTTPRIO * HTTPRIO)
  {
   static const char * defWSDL = "http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc?singleWsdl";
   static const char * defURL = "http://r78-rc.zdrav.netrika.ru/EMK4/EMKService.svc";
   if (addr == "")
    addr = useWSDL ? defWSDL : defURL;
   Soaphttpclient::THTTPRIO * rio = HTTPRIO ? HTTPRIO : new Soaphttpclient::THTTPRIO(0);
   if (useWSDL)
    {
     rio->WSDLLocation = addr;
     rio->Service      = defSvc;
     rio->Port         = defPrt;
    }
   else
    {
     rio->URL = addr;
    }
   _di_IEmkService service;
   rio->QueryInterface(service);
   if (!service && !HTTPRIO)
    delete rio;
   return service;
  }
 // ------------------------------------------------------------------------------
 _di_IEmkService GetIEmkService(System::String WsdlAddr, Soaphttpclient::THTTPRIO * HTTPRIO)
  {
   Soaphttpclient::THTTPRIO * rio = HTTPRIO;
   rio->WSDLLocation = WsdlAddr;
   rio->Service      = defSvc;
   rio->Port         = defPrt;
   _di_IEmkService service;
   rio->QueryInterface(service);
   if (!service && !HTTPRIO)
    delete rio;
   return service;
  }
 // ------------------------------------------------------------------------------

__fastcall HealthGroup2::~HealthGroup2()
{
  delete FDoctor;
  delete FHealthGroupInfo;
}

__fastcall MedDocumentDto_PersonalSign2::~MedDocumentDto_PersonalSign2()
{
  delete FDoctor;
}

__fastcall HealthGroupInfo2::~HealthGroupInfo2()
{
  delete FDate;
}

__fastcall Participant2::~Participant2()
{
  delete FDoctor;
}

__fastcall CaseBase2::~CaseBase2()
{
  delete FOpenDate;
  delete FCloseDate;
  delete FDoctorInCharge;
  delete FAuthenticator;
  delete FAuthor;
  delete FLegalAuthenticator;
  delete FGuardian;
}

__fastcall Person2::~Person2()
{
  delete FHumanName;
  delete FBirthdate;
}

__fastcall PersonWithIdentity2::~PersonWithIdentity2()
{
  for(int i=0; i<FDocuments.Length; i++)
    if (FDocuments[i])
      delete FDocuments[i];
}

__fastcall MedDocumentDto_DocumentAttachment2::~MedDocumentDto_DocumentAttachment2()
{
  for(int i=0; i<FPersonalSigns.Length; i++)
    if (FPersonalSigns[i])
      delete FPersonalSigns[i];
}

__fastcall Recommendation2::~Recommendation2()
{
  delete FDate;
  delete FDoctor;
}

__fastcall ReferralInfo2::~ReferralInfo2()
{
  delete FIssuedDateTime;
}

__fastcall AssisNote2::~AssisNote2()
{
  delete FAssisTimestamp;
}

__fastcall SickListInfo2::~SickListInfo2()
{
  delete FDateStart;
  delete FCaregiver;
  delete FDateEnd;
}

__fastcall MedicalStaff2::~MedicalStaff2()
{
  delete FPerson;
}

__fastcall IdentityDocument2::~IdentityDocument2()
{
  delete FExpiredDate;
  delete FIssuedDate;
  delete FStartDate;
}

__fastcall Quantity2::~Quantity2()
{
  delete FValue;
}

__fastcall RequestFault::~RequestFault()
{
  for(int i=0; i<FErrors.Length; i++)
    if (FErrors[i])
      delete FErrors[i];
}

__fastcall RequestFault2::~RequestFault2()
{
  for(int i=0; i<FErrors.Length; i++)
    if (FErrors[i])
      delete FErrors[i];
}

__fastcall Observation2::~Observation2()
{
  delete FValueQuantity;
  delete FDateTime;
  for(int i=0; i<FReferenceRanges.Length; i++)
    if (FReferenceRanges[i])
      delete FReferenceRanges[i];
}

__fastcall DiagnosisInfo2::~DiagnosisInfo2()
{
  delete FDiagnosedDate;
  for(int i=0; i<FParams.Length; i++)
    if (FParams[i])
      delete FParams[i];
}

__fastcall StepBase2::~StepBase2()
{
  delete FDateStart;
  delete FDateEnd;
  delete FDoctor;
}

__fastcall Problem2::~Problem2()
{
  delete FDate;
  delete FPerformer;
}

__fastcall AppointedMedication2::~AppointedMedication2()
{
  delete FCourseDose;
  delete FDayDose;
  delete FDoctor;
  delete FIssuedDate;
  delete FOneTimeDose;
}

__fastcall TfomsInfo2::~TfomsInfo2()
{
  delete FTariff;
}

__fastcall Diagnosis2::~Diagnosis2()
{
  delete FDiagnosisInfo;
  delete FDoctor;
}

__fastcall ClinicMainDiagnosis2::~ClinicMainDiagnosis2()
{
  for(int i=0; i<FComplications.Length; i++)
    if (FComplications[i])
      delete FComplications[i];
}

__fastcall MedDocument2::~MedDocument2()
{
  delete FAuthor;
  delete FCreationDate;
  for(int i=0; i<FAttachments.Length; i++)
    if (FAttachments[i])
      delete FAttachments[i];
  for(int i=0; i<FObservations.Length; i++)
    if (FObservations[i])
      delete FObservations[i];
}

__fastcall Referral2::~Referral2()
{
  delete FReferralInfo;
  delete FDepartmentHead;
}

__fastcall AmbulanceInfo2::~AmbulanceInfo2()
{
  delete FTransportTime;
  for(int i=0; i<FAssisList.Length; i++)
    if (FAssisList[i])
      delete FAssisList[i];
}

__fastcall PacsResult2::~PacsResult2()
{
  delete FProtocolDate;
  delete FImage;
}

__fastcall DispensaryBase2::~DispensaryBase2()
{
  delete FHealthGroup;
  for(int i=0; i<FRecommendations.Length; i++)
    if (FRecommendations[i])
      delete FRecommendations[i];
}

__fastcall SickList2::~SickList2()
{
  delete FSickListInfo;
}

__fastcall Initiator2::~Initiator2()
{
  delete FDoctor;
}

__fastcall Guardian2::~Guardian2()
{
  delete FPerson;
}

__fastcall CaseStat2::~CaseStat2()
{
  for(int i=0; i<FSteps.Length; i++)
    if (FSteps[i])
      delete FSteps[i];
  for(int i=0; i<FMedRecords.Length; i++)
    if (FMedRecords[i])
      delete FMedRecords[i];
}

__fastcall CaseAmb2::~CaseAmb2()
{
  for(int i=0; i<FSteps.Length; i++)
    if (FSteps[i])
      delete FSteps[i];
  for(int i=0; i<FMedRecords.Length; i++)
    if (FMedRecords[i])
      delete FMedRecords[i];
}

__fastcall CaseAcps2::~CaseAcps2()
{
  for(int i=0; i<FMedRecords.Length; i++)
    if (FMedRecords[i])
      delete FMedRecords[i];
}

__fastcall CaseTmc2::~CaseTmc2()
{
  delete FInitiator;
  for(int i=0; i<FMedRecords.Length; i++)
    if (FMedRecords[i])
      delete FMedRecords[i];
}

__fastcall StepAmb2::~StepAmb2()
{
  for(int i=0; i<FMedRecords.Length; i++)
    if (FMedRecords[i])
      delete FMedRecords[i];
}

__fastcall StepStat2::~StepStat2()
{
  for(int i=0; i<FMedRecords.Length; i++)
    if (FMedRecords[i])
      delete FMedRecords[i];
}

__fastcall Privilege2::~Privilege2()
{
  delete FStart;
  delete FEnd;
}

__fastcall NonDrugTreatment2::~NonDrugTreatment2()
{
  delete FStart;
  delete FEnd;
}

__fastcall PaymentInfo2::~PaymentInfo2()
{
  delete FTariff;
}

__fastcall Service2::~Service2()
{
  delete FDateEnd;
  delete FDateStart;
  delete FPaymentInfo;
  delete FPerformer;
  for(int i=0; i<FParams.Length; i++)
    if (FParams[i])
      delete FParams[i];
}

__fastcall Immunize2::~Immunize2()
{
  delete FPerformer;
}

__fastcall Vaccination2::~Vaccination2()
{
  delete FDate;
}

__fastcall ImmunizationPlan2::~ImmunizationPlan2()
{
  delete FImmunizationType;
  delete FDate;
}

__fastcall Reaction2::~Reaction2()
{
  delete FDate;
}

__fastcall MedicalExemption2::~MedicalExemption2()
{
  delete FDateStart;
  delete FDateEnd;
}

__fastcall Procedure2::~Procedure2()
{
  delete FDate;
  delete FPerformer;
}

__fastcall AllergyBase2::~AllergyBase2()
{
  delete FTime;
}

__fastcall Disability2::~Disability2()
{
  delete FDate;
}

__fastcall SocialAnamnesis2::~SocialAnamnesis2()
{
  delete FDisability;
  delete FSocialGroup;
  for(int i=0; i<FPrivileges.Length; i++)
    if (FPrivileges[i])
      delete FPrivileges[i];
}

__fastcall ResInstr2::~ResInstr2()
{
  delete FDate;
  delete FPerformer;
  for(int i=0; i<FActivities.Length; i++)
    if (FActivities[i])
      delete FActivities[i];
}

__fastcall Scores2::~Scores2()
{
  delete FDate;
}

__fastcall ResInstr_Activity2::~ResInstr_Activity2()
{
  delete FDate;
}

// ************************************************************************ //
// This routine registers the interfaces and types exposed by the WebService.
// ************************************************************************ //
static void RegTypes()
{
  /* IEmkService */
  InvRegistry()->RegisterInterface(__delphirtti(IEmkService), L"http://tempuri.org/", L"utf-8");
  InvRegistry()->RegisterAllSOAPActions(__delphirtti(IEmkService), L"|http://tempuri.org/IServiceSupport/GetVersion|http://tempuri.org/IEmkService/AddCase|http://tempuri.org/IEmkService/CreateCase|http://tempuri.org/IEmkService/CloseCase|http://tempuri.org/IEmkService/UpdateCase|http://tempuri.org/IEmkService/AddMedRecord|http://tempuri.org/IEmkService/AddStepToCase|http://tempuri.org/IEmkService/CancelCase");
  InvRegistry()->RegisterInvokeOptions(__delphirtti(IEmkService), ioDocument);
  /* IEmkService.GetVersion */
  InvRegistry()->RegisterMethodInfo(__delphirtti(IEmkService), "GetVersion", "",
                                    "[ReturnName='GetVersionResult']", IS_OPTN | IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "GetVersion", "GetVersionResult", L"",
                                   L"[Namespace='http://schemas.datacontract.org/2004/07/N3.Shared.Core']", IS_NLBL);
  /* IEmkService->AddCase */
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "AddCase", "guid", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "AddCase", "caseDto", L"",
                                   L"[Namespace='http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case']", IS_NLBL);
  /* IEmkService->CreateCase */
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "CreateCase", "guid", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "CreateCase", "createCaseDto", L"",
                                   L"[Namespace='http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case']", IS_NLBL);
  /* IEmkService->CloseCase */
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "CloseCase", "guid", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "CloseCase", "caseDto", L"",
                                   L"[Namespace='http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case']", IS_NLBL);
  /* IEmkService->UpdateCase */
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "UpdateCase", "guid", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "UpdateCase", "caseDto", L"",
                                   L"[Namespace='http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case']", IS_NLBL);
  /* IEmkService->AddMedRecord */
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "AddMedRecord", "guid", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "AddMedRecord", "idLpu", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "AddMedRecord", "idPatientMis", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "AddMedRecord", "idCaseMis", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "AddMedRecord", "medRecord", L"",
                                   L"[Namespace='http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec']", IS_NLBL);
  /* IEmkService->AddStepToCase */
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "AddStepToCase", "guid", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "AddStepToCase", "idLpu", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "AddStepToCase", "idPatientMis", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "AddStepToCase", "idCaseMis", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "AddStepToCase", "step", L"",
                                   L"[Namespace='http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step']", IS_NLBL);
  /* IEmkService->CancelCase */
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "CancelCase", "guid", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "CancelCase", "idLpu", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "CancelCase", "idPatientMis", L"",
                                   L"", IS_NLBL);
  InvRegistry()->RegisterParamInfo(__delphirtti(IEmkService), "CancelCase", "idCaseMis", L"",
                                   L"", IS_NLBL);
  /* ArrayOfReferenceRange */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfReferenceRange), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ArrayOfReferenceRange");
  /* ArrayOfIdentityDocument */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfIdentityDocument), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"ArrayOfIdentityDocument");
  /* ArrayOfParam */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfParam), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ArrayOfParam");
  /* ArrayOfMedRecord */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfMedRecord), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ArrayOfMedRecord");
  /* ArrayOfObservation */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfObservation), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ArrayOfObservation");
  /* ArrayOfMedDocumentDto.DocumentAttachment */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfMedDocumentDto_DocumentAttachment), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ArrayOfMedDocumentDto_DocumentAttachment", L"ArrayOfMedDocumentDto.DocumentAttachment");
  /* ArrayOfStepStat */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfStepStat), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step", L"ArrayOfStepStat");
  /* ArrayOfMedDocumentDto.PersonalSign */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfMedDocumentDto_PersonalSign), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ArrayOfMedDocumentDto_PersonalSign", L"ArrayOfMedDocumentDto.PersonalSign");
  /* ArrayOfStepAmb */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfStepAmb), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step", L"ArrayOfStepAmb");
  /* ArrayOfRecommendation */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfRecommendation), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ArrayOfRecommendation");
  /* ArrayOfDiagnosis */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfDiagnosis), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag", L"ArrayOfDiagnosis");
  /* ArrayOfAssisNote */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfAssisNote), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ArrayOfAssisNote");
  /* ArrayOfunsignedByte */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfunsignedByte), L"http://schemas.microsoft.com/2003/10/Serialization/Arrays", L"ArrayOfunsignedByte");
  /* ArrayOfPrivilege */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfPrivilege), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ArrayOfPrivilege");
  /* ArrayOfResInstr.Activity */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfResInstr_Activity), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ArrayOfResInstr_Activity", L"ArrayOfResInstr.Activity");
  /* ArrayOfint */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfint), L"http://schemas.microsoft.com/2003/10/Serialization/Arrays", L"ArrayOfint");
  /* HealthGroup */
  RemClassRegistry()->RegisterXSClass(__classid(HealthGroup2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"HealthGroup2", L"HealthGroup");
  /* ArrayOfAssisNote */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfAssisNote), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ArrayOfAssisNote");
  /* ArrayOfRecommendation */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfRecommendation), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ArrayOfRecommendation");
  /* ArrayOfMedDocumentDto.DocumentAttachment */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfMedDocumentDto_DocumentAttachment), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ArrayOfMedDocumentDto_DocumentAttachment", L"ArrayOfMedDocumentDto.DocumentAttachment");
  /* ArrayOfDiagnosis */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfDiagnosis), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag", L"ArrayOfDiagnosis");
  /* ArrayOfObservation */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfObservation), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ArrayOfObservation");
  /* ArrayOfMedDocumentDto.PersonalSign */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfMedDocumentDto_PersonalSign), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ArrayOfMedDocumentDto_PersonalSign", L"ArrayOfMedDocumentDto.PersonalSign");
  /* ArrayOfReferenceRange */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfReferenceRange), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ArrayOfReferenceRange");
  /* ValueQuantity */
  RemClassRegistry()->RegisterXSClass(__classid(ValueQuantity2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ValueQuantity2", L"ValueQuantity");
  /* HealthGroup */
  RemClassRegistry()->RegisterXSClass(__classid(HealthGroup), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"HealthGroup");
  /* MedDocumentDto.PersonalSign */
  RemClassRegistry()->RegisterXSClass(__classid(MedDocumentDto_PersonalSign2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"MedDocumentDto_PersonalSign2", L"MedDocumentDto.PersonalSign");
  /* ArrayOfRequestFault */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfRequestFault2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common", L"ArrayOfRequestFault2", L"ArrayOfRequestFault");
  /* ArrayOfRequestFault */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfRequestFault2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common", L"ArrayOfRequestFault2", L"ArrayOfRequestFault");
  /* MedDocumentDto.PersonalSign */
  RemClassRegistry()->RegisterXSClass(__classid(MedDocumentDto_PersonalSign), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"MedDocumentDto_PersonalSign", L"MedDocumentDto.PersonalSign");
  /* ArrayOfParam */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfParam), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ArrayOfParam");
  /* ValueQuantity */
  RemClassRegistry()->RegisterXSClass(__classid(ValueQuantity), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ValueQuantity");
  /* ArrayOfIdentityDocument */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfIdentityDocument), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"ArrayOfIdentityDocument");
  /* HealthGroupInfo */
  RemClassRegistry()->RegisterXSClass(__classid(HealthGroupInfo2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"HealthGroupInfo2", L"HealthGroupInfo");
  /* HealthGroupInfo */
  RemClassRegistry()->RegisterXSClass(__classid(HealthGroupInfo), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"HealthGroupInfo");
  /* Participant */
  RemClassRegistry()->RegisterXSClass(__classid(Participant2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"Participant2", L"Participant");
  /* Participant */
  RemClassRegistry()->RegisterXSClass(__classid(Participant), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"Participant");
  /* CaseBase */
  RemClassRegistry()->RegisterXSClass(__classid(CaseBase2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case", L"CaseBase2", L"CaseBase");
  /* Person */
  RemClassRegistry()->RegisterXSClass(__classid(Person2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"Person2", L"Person");
  /* Person */
  RemClassRegistry()->RegisterXSClass(__classid(Person), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"Person");
  /* PersonWithIdentity */
  RemClassRegistry()->RegisterXSClass(__classid(PersonWithIdentity2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"PersonWithIdentity2", L"PersonWithIdentity");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(PersonWithIdentity2), L"Documents", L"[ArrayItemName='IdentityDocument']");
  /* PersonWithIdentity */
  RemClassRegistry()->RegisterXSClass(__classid(PersonWithIdentity), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"PersonWithIdentity");
  /* HumanName */
  RemClassRegistry()->RegisterXSClass(__classid(HumanName2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"HumanName2", L"HumanName");
  /* HumanName */
  RemClassRegistry()->RegisterXSClass(__classid(HumanName), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"HumanName");
  /* PacsResult.ImageData */
  RemClassRegistry()->RegisterXSClass(__classid(PacsResult_ImageData2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"PacsResult_ImageData2", L"PacsResult.ImageData");
  /* PacsResult.ImageData */
  RemClassRegistry()->RegisterXSClass(__classid(PacsResult_ImageData), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"PacsResult_ImageData", L"PacsResult.ImageData");
  /* MedDocumentDto.DocumentAttachment */
  RemClassRegistry()->RegisterXSClass(__classid(MedDocumentDto_DocumentAttachment2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"MedDocumentDto_DocumentAttachment2", L"MedDocumentDto.DocumentAttachment");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(MedDocumentDto_DocumentAttachment2), L"PersonalSigns", L"[ArrayItemName='MedDocumentDto.PersonalSign']");
  /* MedDocumentDto.DocumentAttachment */
  RemClassRegistry()->RegisterXSClass(__classid(MedDocumentDto_DocumentAttachment), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"MedDocumentDto_DocumentAttachment", L"MedDocumentDto.DocumentAttachment");
  /* StringValue */
  RemClassRegistry()->RegisterXSClass(__classid(StringValue2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"StringValue2", L"StringValue");
  /* StringValue */
  RemClassRegistry()->RegisterXSClass(__classid(StringValue), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"StringValue");
  /* Recommendation */
  RemClassRegistry()->RegisterXSClass(__classid(Recommendation2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"Recommendation2", L"Recommendation");
  /* Recommendation */
  RemClassRegistry()->RegisterXSClass(__classid(Recommendation), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"Recommendation");
  /* ReferralInfo */
  RemClassRegistry()->RegisterXSClass(__classid(ReferralInfo2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ReferralInfo2", L"ReferralInfo");
  /* ReferralInfo */
  RemClassRegistry()->RegisterXSClass(__classid(ReferralInfo), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ReferralInfo");
  /* AssisNote */
  RemClassRegistry()->RegisterXSClass(__classid(AssisNote2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"AssisNote2", L"AssisNote");
  /* AssisNote */
  RemClassRegistry()->RegisterXSClass(__classid(AssisNote), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"AssisNote");
  /* SickListInfo */
  RemClassRegistry()->RegisterXSClass(__classid(SickListInfo2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"SickListInfo2", L"SickListInfo");
  /* SickListInfo */
  RemClassRegistry()->RegisterXSClass(__classid(SickListInfo), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"SickListInfo");
  /* VersionInfo */
  RemClassRegistry()->RegisterXSClass(__classid(VersionInfo2), L"http://schemas.datacontract.org/2004/07/N3.Shared.Core", L"VersionInfo2", L"VersionInfo");
  /* VersionInfo */
  RemClassRegistry()->RegisterXSClass(__classid(VersionInfo), L"http://schemas.datacontract.org/2004/07/N3.Shared.Core", L"VersionInfo");
  /* MedicalStaff */
  RemClassRegistry()->RegisterXSClass(__classid(MedicalStaff2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"MedicalStaff2", L"MedicalStaff");
  /* MedicalStaff */
  RemClassRegistry()->RegisterXSClass(__classid(MedicalStaff), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"MedicalStaff");
  /* IdentityDocument */
  RemClassRegistry()->RegisterXSClass(__classid(IdentityDocument2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"IdentityDocument2", L"IdentityDocument");
  /* IdentityDocument */
  RemClassRegistry()->RegisterXSClass(__classid(IdentityDocument), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"IdentityDocument");
  /* Quantity */
  RemClassRegistry()->RegisterXSClass(__classid(Quantity2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"Quantity2", L"Quantity");
  /* Quantity */
  RemClassRegistry()->RegisterXSClass(__classid(Quantity), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"Quantity");
  /* Param */
  RemClassRegistry()->RegisterXSClass(__classid(Param2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Param2", L"Param");
  /* RequestFault */
  RemClassRegistry()->RegisterXSClass(__classid(RequestFault), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common", L"RequestFault");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(RequestFault), L"Errors", L"[ArrayItemName='RequestFault']");
  /* RequestFault */
  RemClassRegistry()->RegisterXSClass(__classid(RequestFault2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Common", L"RequestFault2", L"RequestFault");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(RequestFault2), L"Errors", L"[ArrayItemName='RequestFault']");
  /* Observation */
  RemClassRegistry()->RegisterXSClass(__classid(Observation2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"Observation2", L"Observation");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(Observation2), L"ReferenceRanges", L"[ArrayItemName='ReferenceRange']");
  /* Observation */
  RemClassRegistry()->RegisterXSClass(__classid(Observation), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"Observation");
  /* ReferenceRange */
  RemClassRegistry()->RegisterXSClass(__classid(ReferenceRange2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ReferenceRange2", L"ReferenceRange");
  /* ReferenceRange */
  RemClassRegistry()->RegisterXSClass(__classid(ReferenceRange), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ReferenceRange");
  /* CodeableValue */
  RemClassRegistry()->RegisterXSClass(__classid(CodeableValue2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"CodeableValue2", L"CodeableValue");
  /* CodeableValue */
  RemClassRegistry()->RegisterXSClass(__classid(CodeableValue), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"CodeableValue");
  /* NumericalValue */
  RemClassRegistry()->RegisterXSClass(__classid(NumericalValue2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"NumericalValue2", L"NumericalValue");
  /* NumericalValue */
  RemClassRegistry()->RegisterXSClass(__classid(NumericalValue), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"NumericalValue");
  /* DiagnosisInfo */
  RemClassRegistry()->RegisterXSClass(__classid(DiagnosisInfo2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag", L"DiagnosisInfo2", L"DiagnosisInfo");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(DiagnosisInfo2), L"Params", L"[ArrayItemName='Param']");
  /* DiagnosisInfo */
  RemClassRegistry()->RegisterXSClass(__classid(DiagnosisInfo), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag", L"DiagnosisInfo");
  /* StepBase */
  RemClassRegistry()->RegisterXSClass(__classid(StepBase2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step", L"StepBase2", L"StepBase");
  /* StepBase */
  RemClassRegistry()->RegisterXSClass(__classid(StepBase), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step", L"StepBase");
  /* MedRecord */
  RemClassRegistry()->RegisterXSClass(__classid(MedRecord2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"MedRecord2", L"MedRecord");
  /* Problem */
  RemClassRegistry()->RegisterXSClass(__classid(Problem2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Problem2", L"Problem");
  /* AppointedMedication */
  RemClassRegistry()->RegisterXSClass(__classid(AppointedMedication2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"AppointedMedication2", L"AppointedMedication");
  /* TfomsInfo */
  RemClassRegistry()->RegisterXSClass(__classid(TfomsInfo2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"TfomsInfo2", L"TfomsInfo");
  /* DeathInfo */
  RemClassRegistry()->RegisterXSClass(__classid(DeathInfo2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"DeathInfo2", L"DeathInfo");
  /* Diagnosis */
  RemClassRegistry()->RegisterXSClass(__classid(Diagnosis2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag", L"Diagnosis2", L"Diagnosis");
  /* ClinicMainDiagnosis */
  RemClassRegistry()->RegisterXSClass(__classid(ClinicMainDiagnosis2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag", L"ClinicMainDiagnosis2", L"ClinicMainDiagnosis");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(ClinicMainDiagnosis2), L"Complications", L"[ArrayItemName='Diagnosis']");
  /* ClinicMainDiagnosis */
  RemClassRegistry()->RegisterXSClass(__classid(ClinicMainDiagnosis), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag", L"ClinicMainDiagnosis");
  /* AnatomopathologicalClinicMainDiagnosis */
  RemClassRegistry()->RegisterXSClass(__classid(AnatomopathologicalClinicMainDiagnosis2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag", L"AnatomopathologicalClinicMainDiagnosis2", L"AnatomopathologicalClinicMainDiagnosis");
  /* AnatomopathologicalClinicMainDiagnosis */
  RemClassRegistry()->RegisterXSClass(__classid(AnatomopathologicalClinicMainDiagnosis), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag", L"AnatomopathologicalClinicMainDiagnosis");
  /* Diagnosis */
  RemClassRegistry()->RegisterXSClass(__classid(Diagnosis), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.Diag", L"Diagnosis");
  /* MedDocument */
  RemClassRegistry()->RegisterXSClass(__classid(MedDocument2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"MedDocument2", L"MedDocument");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(MedDocument2), L"Attachments", L"[ArrayItemName='MedDocumentDto.DocumentAttachment']");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(MedDocument2), L"Observations", L"[ArrayItemName='Observation']");
  /* Referral */
  RemClassRegistry()->RegisterXSClass(__classid(Referral2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"Referral2", L"Referral");
  /* Referral */
  RemClassRegistry()->RegisterXSClass(__classid(Referral), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"Referral");
  /* AmbulanceInfo */
  RemClassRegistry()->RegisterXSClass(__classid(AmbulanceInfo2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"AmbulanceInfo2", L"AmbulanceInfo");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(AmbulanceInfo2), L"AssisList", L"[ArrayItemName='AssisNote']");
  /* AmbulanceInfo */
  RemClassRegistry()->RegisterXSClass(__classid(AmbulanceInfo), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"AmbulanceInfo");
  /* DeathCertificate */
  RemClassRegistry()->RegisterXSClass(__classid(DeathCertificate2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"DeathCertificate2", L"DeathCertificate");
  /* DeathCertificate */
  RemClassRegistry()->RegisterXSClass(__classid(DeathCertificate), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"DeathCertificate");
  /* Form106U */
  RemClassRegistry()->RegisterXSClass(__classid(Form106U2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"Form106U2", L"Form106U");
  /* Form106U */
  RemClassRegistry()->RegisterXSClass(__classid(Form106U), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"Form106U");
  /* PacsResult */
  RemClassRegistry()->RegisterXSClass(__classid(PacsResult2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"PacsResult2", L"PacsResult");
  /* PacsResult */
  RemClassRegistry()->RegisterXSClass(__classid(PacsResult), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"PacsResult");
  /* ReferralMSE */
  RemClassRegistry()->RegisterXSClass(__classid(ReferralMSE2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ReferralMSE2", L"ReferralMSE");
  /* ReferralMSE */
  RemClassRegistry()->RegisterXSClass(__classid(ReferralMSE), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ReferralMSE");
  /* MedDocument */
  RemClassRegistry()->RegisterXSClass(__classid(MedDocument), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"MedDocument");
  /* DispensaryBase */
  RemClassRegistry()->RegisterXSClass(__classid(DispensaryBase2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"DispensaryBase2", L"DispensaryBase");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(DispensaryBase2), L"Recommendations", L"[ArrayItemName='Recommendation']");
  /* DispensaryBase */
  RemClassRegistry()->RegisterXSClass(__classid(DispensaryBase), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"DispensaryBase");
  /* DispensaryOne */
  RemClassRegistry()->RegisterXSClass(__classid(DispensaryOne2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"DispensaryOne2", L"DispensaryOne");
  /* DispensaryOne */
  RemClassRegistry()->RegisterXSClass(__classid(DispensaryOne), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"DispensaryOne");
  /* DispensaryOnceByTwoYears */
  RemClassRegistry()->RegisterXSClass(__classid(DispensaryOnceByTwoYears2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"DispensaryOnceByTwoYears2", L"DispensaryOnceByTwoYears");
  /* DispensaryOnceByTwoYears */
  RemClassRegistry()->RegisterXSClass(__classid(DispensaryOnceByTwoYears), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"DispensaryOnceByTwoYears");
  /* DispensaryTwo */
  RemClassRegistry()->RegisterXSClass(__classid(DispensaryTwo2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"DispensaryTwo2", L"DispensaryTwo");
  /* DispensaryTwo */
  RemClassRegistry()->RegisterXSClass(__classid(DispensaryTwo), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"DispensaryTwo");
  /* Form103U */
  RemClassRegistry()->RegisterXSClass(__classid(Form103U2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"Form103U2", L"Form103U");
  /* Form103U */
  RemClassRegistry()->RegisterXSClass(__classid(Form103U), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"Form103U");
  /* ConsultNote */
  RemClassRegistry()->RegisterXSClass(__classid(ConsultNote2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ConsultNote2", L"ConsultNote");
  /* ConsultNote */
  RemClassRegistry()->RegisterXSClass(__classid(ConsultNote), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"ConsultNote");
  /* DischargeSummary */
  RemClassRegistry()->RegisterXSClass(__classid(DischargeSummary2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"DischargeSummary2", L"DischargeSummary");
  /* DischargeSummary */
  RemClassRegistry()->RegisterXSClass(__classid(DischargeSummary), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"DischargeSummary");
  /* Form106_2U */
  RemClassRegistry()->RegisterXSClass(__classid(Form106_2U2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"Form106_2U2", L"Form106_2U");
  /* Form106_2U */
  RemClassRegistry()->RegisterXSClass(__classid(Form106_2U), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"Form106_2U");
  /* SickList */
  RemClassRegistry()->RegisterXSClass(__classid(SickList2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"SickList2", L"SickList");
  /* SickList */
  RemClassRegistry()->RegisterXSClass(__classid(SickList), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"SickList");
  /* LaboratoryReport */
  RemClassRegistry()->RegisterXSClass(__classid(LaboratoryReport2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"LaboratoryReport2", L"LaboratoryReport");
  /* LaboratoryReport */
  RemClassRegistry()->RegisterXSClass(__classid(LaboratoryReport), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"LaboratoryReport");
  /* Form027U */
  RemClassRegistry()->RegisterXSClass(__classid(Form027U2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"Form027U2", L"Form027U");
  /* Form027U */
  RemClassRegistry()->RegisterXSClass(__classid(Form027U), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec.MedDoc", L"Form027U");
  /* ArrayOfunsignedByte */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfunsignedByte), L"http://schemas.microsoft.com/2003/10/Serialization/Arrays", L"ArrayOfunsignedByte");
  /* CaseBase */
  RemClassRegistry()->RegisterXSClass(__classid(CaseBase), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case", L"CaseBase");
  /* Initiator */
  RemClassRegistry()->RegisterXSClass(__classid(Initiator2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"Initiator2", L"Initiator");
  /* Initiator */
  RemClassRegistry()->RegisterXSClass(__classid(Initiator), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"Initiator");
  /* ArrayOfStepAmb */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfStepAmb), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step", L"ArrayOfStepAmb");
  /* Guardian */
  RemClassRegistry()->RegisterXSClass(__classid(Guardian2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"Guardian2", L"Guardian");
  /* Guardian */
  RemClassRegistry()->RegisterXSClass(__classid(Guardian), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto", L"Guardian");
  /* ArrayOfStepStat */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfStepStat), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step", L"ArrayOfStepStat");
  /* ArrayOfMedRecord */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfMedRecord), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ArrayOfMedRecord");
  /* CaseStat */
  RemClassRegistry()->RegisterXSClass(__classid(CaseStat2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case", L"CaseStat2", L"CaseStat");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(CaseStat2), L"Steps", L"[ArrayItemName='StepStat']");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(CaseStat2), L"MedRecords", L"[ArrayItemName='MedRecord']");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(CaseStat2), L"PrehospitalDefects", L"[ArrayItemName='unsignedByte']");
  /* CaseStat */
  RemClassRegistry()->RegisterXSClass(__classid(CaseStat), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case", L"CaseStat");
  /* CaseAmb */
  RemClassRegistry()->RegisterXSClass(__classid(CaseAmb2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case", L"CaseAmb2", L"CaseAmb");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(CaseAmb2), L"Steps", L"[ArrayItemName='StepAmb']");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(CaseAmb2), L"MedRecords", L"[ArrayItemName='MedRecord']");
  /* CaseAmb */
  RemClassRegistry()->RegisterXSClass(__classid(CaseAmb), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case", L"CaseAmb");
  /* CaseAcps */
  RemClassRegistry()->RegisterXSClass(__classid(CaseAcps2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case", L"CaseAcps2", L"CaseAcps");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(CaseAcps2), L"MedRecords", L"[ArrayItemName='MedRecord']");
  /* CaseAcps */
  RemClassRegistry()->RegisterXSClass(__classid(CaseAcps), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case", L"CaseAcps");
  /* CaseTmc */
  RemClassRegistry()->RegisterXSClass(__classid(CaseTmc2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case", L"CaseTmc2", L"CaseTmc");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(CaseTmc2), L"MedRecords", L"[ArrayItemName='MedRecord']");
  /* CaseTmc */
  RemClassRegistry()->RegisterXSClass(__classid(CaseTmc), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Case", L"CaseTmc");
  /* StepAmb */
  RemClassRegistry()->RegisterXSClass(__classid(StepAmb2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step", L"StepAmb2", L"StepAmb");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(StepAmb2), L"MedRecords", L"[ArrayItemName='MedRecord']");
  /* StepAmb */
  RemClassRegistry()->RegisterXSClass(__classid(StepAmb), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step", L"StepAmb");
  /* StepStat */
  RemClassRegistry()->RegisterXSClass(__classid(StepStat2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step", L"StepStat2", L"StepStat");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(StepStat2), L"MedRecords", L"[ArrayItemName='MedRecord']");
  /* StepStat */
  RemClassRegistry()->RegisterXSClass(__classid(StepStat), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.Step", L"StepStat");
  /* Param */
  RemClassRegistry()->RegisterXSClass(__classid(Param), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Param");
  /* DeathInfo */
  RemClassRegistry()->RegisterXSClass(__classid(DeathInfo), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"DeathInfo");
  /* MedRecord */
  RemClassRegistry()->RegisterXSClass(__classid(MedRecord), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"MedRecord");
  /* TfomsInfo */
  RemClassRegistry()->RegisterXSClass(__classid(TfomsInfo), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"TfomsInfo");
  /* AppointedMedication */
  RemClassRegistry()->RegisterXSClass(__classid(AppointedMedication), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"AppointedMedication");
  /* Problem */
  RemClassRegistry()->RegisterXSClass(__classid(Problem), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Problem");
  /* Privilege */
  RemClassRegistry()->RegisterXSClass(__classid(Privilege2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Privilege2", L"Privilege");
  /* Privilege */
  RemClassRegistry()->RegisterXSClass(__classid(Privilege), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Privilege");
  /* ArrayOfint */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfint), L"http://schemas.microsoft.com/2003/10/Serialization/Arrays", L"ArrayOfint");
  /* ImmunizationType */
  RemClassRegistry()->RegisterXSClass(__classid(ImmunizationType2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ImmunizationType2", L"ImmunizationType");
  /* ReactionType */
  RemClassRegistry()->RegisterXSClass(__classid(ReactionType2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ReactionType2", L"ReactionType");
  /* ReactionType */
  RemClassRegistry()->RegisterXSClass(__classid(ReactionType), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ReactionType");
  /* ImmunizationType */
  RemClassRegistry()->RegisterXSClass(__classid(ImmunizationType), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ImmunizationType");
  /* NonDrugTreatment */
  RemClassRegistry()->RegisterXSClass(__classid(NonDrugTreatment2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"NonDrugTreatment2", L"NonDrugTreatment");
  /* NonDrugTreatment */
  RemClassRegistry()->RegisterXSClass(__classid(NonDrugTreatment), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"NonDrugTreatment");
  /* PaymentInfo */
  RemClassRegistry()->RegisterXSClass(__classid(PaymentInfo2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"PaymentInfo2", L"PaymentInfo");
  /* PaymentInfo */
  RemClassRegistry()->RegisterXSClass(__classid(PaymentInfo), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"PaymentInfo");
  /* Service */
  RemClassRegistry()->RegisterXSClass(__classid(Service2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Service2", L"Service");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(Service2), L"Params", L"[ArrayItemName='Param']");
  /* Service */
  RemClassRegistry()->RegisterXSClass(__classid(Service), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Service");
  /* Immunize */
  RemClassRegistry()->RegisterXSClass(__classid(Immunize2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Immunize2", L"Immunize");
  /* Vaccination */
  RemClassRegistry()->RegisterXSClass(__classid(Vaccination2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Vaccination2", L"Vaccination");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(Vaccination2), L"Inf", L"[ArrayItemName='int']");
  /* Vaccination */
  RemClassRegistry()->RegisterXSClass(__classid(Vaccination), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Vaccination");
  /* ImmunizationPlan */
  RemClassRegistry()->RegisterXSClass(__classid(ImmunizationPlan2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ImmunizationPlan2", L"ImmunizationPlan");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(ImmunizationPlan2), L"Inf", L"[ArrayItemName='int']");
  /* ImmunizationPlan */
  RemClassRegistry()->RegisterXSClass(__classid(ImmunizationPlan), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ImmunizationPlan");
  /* Reaction */
  RemClassRegistry()->RegisterXSClass(__classid(Reaction2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Reaction2", L"Reaction");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(Reaction2), L"Inf", L"[ArrayItemName='int']");
  /* Reaction */
  RemClassRegistry()->RegisterXSClass(__classid(Reaction), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Reaction");
  /* MedicalExemption */
  RemClassRegistry()->RegisterXSClass(__classid(MedicalExemption2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"MedicalExemption2", L"MedicalExemption");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(MedicalExemption2), L"ExemptionInf", L"[ArrayItemName='int']");
  /* MedicalExemption */
  RemClassRegistry()->RegisterXSClass(__classid(MedicalExemption), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"MedicalExemption");
  /* Immunize */
  RemClassRegistry()->RegisterXSClass(__classid(Immunize), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Immunize");
  /* Procedure */
  RemClassRegistry()->RegisterXSClass(__classid(Procedure2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Procedure2", L"Procedure");
  /* Procedure */
  RemClassRegistry()->RegisterXSClass(__classid(Procedure), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Procedure");
  /* AllergyBase */
  RemClassRegistry()->RegisterXSClass(__classid(AllergyBase2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"AllergyBase2", L"AllergyBase");
  /* AllergyNonDrug */
  RemClassRegistry()->RegisterXSClass(__classid(AllergyNonDrug2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"AllergyNonDrug2", L"AllergyNonDrug");
  /* AllergyNonDrug */
  RemClassRegistry()->RegisterXSClass(__classid(AllergyNonDrug), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"AllergyNonDrug");
  /* AllergyBase */
  RemClassRegistry()->RegisterXSClass(__classid(AllergyBase), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"AllergyBase");
  /* SocialGroup */
  RemClassRegistry()->RegisterXSClass(__classid(SocialGroup2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"SocialGroup2", L"SocialGroup");
  /* SocialGroup */
  RemClassRegistry()->RegisterXSClass(__classid(SocialGroup), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"SocialGroup");
  /* Disability */
  RemClassRegistry()->RegisterXSClass(__classid(Disability2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Disability2", L"Disability");
  /* Disability */
  RemClassRegistry()->RegisterXSClass(__classid(Disability), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Disability");
  /* ArrayOfPrivilege */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfPrivilege), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ArrayOfPrivilege");
  /* SocialAnamnesis */
  RemClassRegistry()->RegisterXSClass(__classid(SocialAnamnesis2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"SocialAnamnesis2", L"SocialAnamnesis");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(SocialAnamnesis2), L"Privileges", L"[ArrayItemName='Privilege']");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(SocialAnamnesis2), L"SocialRiskFactors", L"[ArrayItemName='int']");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(SocialAnamnesis2), L"OccupationalHazards", L"[ArrayItemName='int']");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(SocialAnamnesis2), L"BadHabits", L"[ArrayItemName='int']");
  /* SocialAnamnesis */
  RemClassRegistry()->RegisterXSClass(__classid(SocialAnamnesis), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"SocialAnamnesis");
  /* ArrayOfResInstr.Activity */
  RemClassRegistry()->RegisterXSInfo(__delphirtti(ArrayOfResInstr_Activity), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ArrayOfResInstr_Activity", L"ArrayOfResInstr.Activity");
  /* ResInstr */
  RemClassRegistry()->RegisterXSClass(__classid(ResInstr2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ResInstr2", L"ResInstr");
  RemClassRegistry()->RegisterExternalPropName(__typeinfo(ResInstr2), L"Activities", L"[ArrayItemName='ResInstr.Activity']");
  /* ResInstr */
  RemClassRegistry()->RegisterXSClass(__classid(ResInstr), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ResInstr");
  /* VaccinationType */
  RemClassRegistry()->RegisterXSClass(__classid(VaccinationType2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"VaccinationType2", L"VaccinationType");
  /* VaccinationType */
  RemClassRegistry()->RegisterXSClass(__classid(VaccinationType), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"VaccinationType");
  /* AllergyDrug */
  RemClassRegistry()->RegisterXSClass(__classid(AllergyDrug2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"AllergyDrug2", L"AllergyDrug");
  /* AllergyDrug */
  RemClassRegistry()->RegisterXSClass(__classid(AllergyDrug), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"AllergyDrug");
  /* Scores */
  RemClassRegistry()->RegisterXSClass(__classid(Scores2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Scores2", L"Scores");
  /* Scores */
  RemClassRegistry()->RegisterXSClass(__classid(Scores), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"Scores");
  /* ResInstr.Activity */
  RemClassRegistry()->RegisterXSClass(__classid(ResInstr_Activity2), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ResInstr_Activity2", L"ResInstr.Activity");
  /* ResInstr.Activity */
  RemClassRegistry()->RegisterXSClass(__classid(ResInstr_Activity), L"http://schemas.datacontract.org/2004/07/N3.EMK.Dto.MedRec", L"ResInstr_Activity", L"ResInstr.Activity");
}
#pragma startup RegTypes 32

};     // NS_EMKService


