﻿// ---------------------------------------------------------------------------
#pragma hdrstop
#include "InsertClassItem.h"
#include "ExtUtils.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
__fastcall TInsertClassItem::TInsertClassItem(TTagNode * ADef)
 {
  FSrcNode    = ADef;
  FDefNode    = TkabCustomDataSet::CreateDefNode(ADef);
  FGetValById = NULL;
  FDataRow    = new TkabCustomDataSetRow(FDefNode);
 }
// ---------------------------------------------------------------------------
__fastcall TInsertClassItem::~TInsertClassItem()
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TInsertClassItem::NewRec()
 {
  try
   {
    if (FInsertData && FDefNode)
     {
      TTagNode * itNode = FDefNode->GetFirstChild();
      while (itNode)
       {
        if (itNode->CmpAV("name", "cguid"))
         FDataRow->Value[itNode->AV["uid"]] = icsNewGUID();
        else if (itNode->AV["uid"].Length())
         FDataRow->Value[itNode->AV["uid"]] = Variant::Empty();
        itNode = itNode->GetNext();
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TInsertClassItem::Execute(UnicodeString & ARecID, UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    try
     {
      if (FInsertData && FDefNode)
       {
        RC = FInsertData("reg." + FSrcNode->AV["uid"], FDataRow->GetJSONValues(), ARecID, ARC);
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      ARC = E.Message;
     }
    catch (...)
     {
      ARC = "Ошибка";
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TInsertClassItem::FLoadJSONData(TJSONObject * AData, TStrings * AList)
 {
  TJSONPairEnumerator * itPair;
  UnicodeString FClStr;
  int FClCode;
  itPair = AData->GetEnumerator();
  while (itPair->MoveNext())
   {
    FClCode = ((TJSONString *)itPair->Current->JsonString)->Value().ToIntDef(0);
    FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
    AList->AddObject(FClStr, (TObject *)FClCode);
   }
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TInsertClassItem::GetListData(UnicodeString ASel, UnicodeString ACode)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FGetValById)
     FGetValById(ASel, ACode, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TInsertClassItem::Preload()
 {
  // TJSONObject * RetData = NULL;
  // TJSONPairEnumerator * itPair;
  // UnicodeString FClCode, FClStr;
  // try
  // {
  // RetData = GetListData("reg.s2.Select CODE as ps1, R0064 as ps2 From CLASS_0035", "0");
  // if (RetData)
  // {
  // itPair = RetData->GetEnumerator();
  // while (itPair->MoveNext())
  // {
  // FClCode                          = ((TJSONString *)itPair->Current->JsonString)->Value();
  // FClStr                           = ((TJSONString *)itPair->Current->JsonValue)->Value();
  // MIBPBinReak[FClCode.ToIntDef(0)] = (FClStr == "1");
  // }
  // }
  // ReakValEnabled.clear();
  // RetData = GetListData("reg.s2.Select CODE as ps1, R115A as ps2 From CLASS_0038", "0");
  // if (RetData)
  // {
  // itPair = RetData->GetEnumerator();
  // while (itPair->MoveNext())
  // {
  // FClCode                             = ((TJSONString *)itPair->Current->JsonString)->Value();
  // FClStr                              = ((TJSONString *)itPair->Current->JsonValue)->Value();
  // ReakValEnabled[FClCode.ToIntDef(0)] = FClStr.ToIntDef(0);
  // }
  // }
  // }
  // __finally
  // {
  // }
 }
// ---------------------------------------------------------------------------
bool __fastcall TInsertClassItem::CheckInput(UnicodeString AMIBPCode, UnicodeString AReacCode, UnicodeString AReacVal,
  bool fullCheck)
 {
  bool RC = AMIBPCode.ToIntDef(0);
  try
   {
    // if (RC && (!FBinReak || FBinReak && !MIBPBinReak[AMIBPCode.ToIntDef(0)]))
    // {
    // if (fullCheck)
    // {
    // RC &= AReacCode.Length();
    // if (RC && ReakValEnabled[AReacCode.ToIntDef(0)])
    // {
    // RC &= AReacVal.Length();
    // }
    // }
    // else
    // {
    // if (AReacCode.Length())
    // {
    // if (RC && ReakValEnabled[AReacCode.ToIntDef(0)])
    // {
    // RC &= AReacVal.Length();
    // }
    // }
    // }
    // }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
/*
 UnicodeString RC = "";
 UnicodeString SQL = "reg.s2.";
 TStringList * InfList = new TStringList;
 try
 {
 if (FSchDefNode && FGetValById)
 {
 TTagNode * LineNode = FSchDefNode->GetTagByUID(ASch);
 if (LineNode && LineNode->CmpName("line"))
 {
 TTagNode * SchNode = LineNode->GetParent();
 UnicodeString VacCode = SchNode->AV["ref"];
 SQL += "Select R003E as ps1, R003B as ps2 From CLASS_002D where R003B=" + VacCode;
 FLoadJSONData(GetListData(SQL), InfList);
 // inf_code:vactype^uid_scheme.uid_scheme_line
 // "1021":"V1:3^016A;V1:4^016A;V1:5^016A",
 for (int i = 0; i < InfList->Count; i++)
 {
 if (RC.Length())
 RC += ";";
 RC += LineNode->AV["name"] + ":";
 RC += IntToStr((int)InfList->Objects[i]) + "^";
 RC += SchNode->AV["uid"] + "." + LineNode->AV["uid"];
 }
 }
 }
 }
 __finally
 {
 delete InfList;
 }
 return RC;
 */
