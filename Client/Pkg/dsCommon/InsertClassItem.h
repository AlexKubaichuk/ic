﻿//---------------------------------------------------------------------------

#ifndef InsertClassItemH
#define InsertClassItemH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
#include "KabCustomDS.h"
#include "dsRegTypeDef.h"
#include "cxTL.hpp"
//---------------------------------------------------------------------------
class TInsertClassItem : public TObject
{
private:	// User declarations
  TkabCustomDataSetRow *FDataRow;
  TTagNode *FDefNode, *FSrcNode;

  TdsGetValByIdEvent FGetValById;
  TdsInsertDataEvent FInsertData;

  void __fastcall TInsertClassItem::FLoadJSONData(TJSONObject *AData, TStrings *AList);
  TJSONObject* __fastcall GetListData(UnicodeString ASel, UnicodeString ACode = "0");
public:		// User declarations
  __fastcall TInsertClassItem(TTagNode *ADef);
  __fastcall ~TInsertClassItem();

  __property TdsGetValByIdEvent OnGetValById = {read = FGetValById, write=FGetValById};
  __property TdsInsertDataEvent OnInsertData = {read = FInsertData, write = FInsertData};

  __property TkabCustomDataSetRow *Data = {read=FDataRow};

  void __fastcall NewRec();
  bool __fastcall Execute(UnicodeString & ARecID, UnicodeString & ARC);
  UnicodeString __fastcall Preload();
  bool __fastcall CheckInput(UnicodeString AMIBPCode, UnicodeString AReacCode, UnicodeString AReacVal, bool fullCheck = false);
};
//---------------------------------------------------------------------------
#endif
