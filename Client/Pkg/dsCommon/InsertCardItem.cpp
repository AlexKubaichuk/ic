﻿// ---------------------------------------------------------------------------
#pragma hdrstop
#include "InsertCardItem.h"
#include "ExtUtils.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
__fastcall TInsertCardItem::TInsertCardItem(TTagNode * ADef, TTagNode * ASchDef)
 {
  ReacMap     = new TReakList;
  FBinReak    = false;
  FDefNode    = ADef;
  FSchDefNode = ASchDef;
  FGetValById = NULL;
  FRowDefNode = TkabCustomDataSet::CreateDefNode(ADef);
  FDataRow    = new TkabCustomDataSetRow(FRowDefNode);
  FInLPU      = Variant::Empty();
  FLPU        = Variant::Empty();
  FDoc        = Variant::Empty();
  FMedSis     = Variant::Empty();
  FFinSrc     = Variant::Empty();
  FExtInf     = Variant::Empty();
 }
// ---------------------------------------------------------------------------
__fastcall TInsertCardItem::~TInsertCardItem()
 {
  for (TReakList::iterator i = ReacMap->begin(); i != ReacMap->end(); i++)
   delete i->second;
  ReacMap->clear();
  delete ReacMap;
 }
// ---------------------------------------------------------------------------
void __fastcall TInsertCardItem::NewRec(TDate APtBirthDay)
 {
  try
   {
    if (FInsertData && FDefNode)
     {
      if (FDefNode->CmpAV("uid", "1003"))
       {
        FDataRow->Value["32D7"] = icsNewGUID(); // extedit name='cguid'
        FDataRow->Value["017A"] = Variant::Empty(); // extedit name='Код пациента'
        FDataRow->Value["3183"] = Variant::Empty(); // extedit name='Код прививки'
        FDataRow->Value["32AE"] = Variant::Empty(); // extedit name='Код плана'
        FDataRow->Value["1024"] = Variant::Empty(); // date name='Дата выполнения'
        FDataRow->Value["10B4"] = Variant::Empty(); // extedit name='Возраст'
        FDataRow->Value["1020"] = Variant::Empty(); // choiceDB name='МИБП'
        FDataRow->Value["1021"] = Variant::Empty(); // text name='Вид'
        FDataRow->Value["1075"] = Variant::Empty(); // choiceDB name='Инфекция'
        FDataRow->Value["1022"] = Variant::Empty(); // digit name='Доза'
        FDataRow->Value["1023"] = Variant::Empty(); // text name='Серия'
        FDataRow->Value["1025"] = 0; // binary name='Реакция проверена'
        FDataRow->Value["1026"] = 0; // binary name='Есть общая реакция'
        FDataRow->Value["1028"] = 0; // binary name='Есть местная реакция'
        FDataRow->Value["10A8"] = Variant::Empty(); // choiceDB name='Формат реакции'
        FDataRow->Value["1027"] = Variant::Empty(); // digit name='Значение реакции'
        FDataRow->Value["10AE"] = 0; // binary name='Туровая прививка'
        FDataRow->Value["1092"] = Variant::Empty(); // extedit name='Схема'
        FDataRow->Value["10AF"] = 0; // choice name='Тур закрыт'
        FDataRow->Value["101F"] = FInLPU; // binary name='Выполнена в данном ЛПУ'
        FDataRow->Value["017F"] = FLPU; // choiceDB name='ЛПУ'
        FDataRow->Value["10B0"] = FDoc; // choiceDB name='Врач'
        FDataRow->Value["0181"] = FMedSis; // choiceDB name='Мед. сестра'
        FDataRow->Value["10B1"] = FFinSrc; // choiceDB name='Источник финансирования'
        FDataRow->Value["0182"] = FExtInf; // choiceDB name='Доп. информация'
        FDataRow->Value["330A"] = Variant::Empty(); // date name='Дата списания'
        FDataRow->Value["1155"] = APtBirthDay; // date name='ДР пациента'
       }
      else if (FDefNode->CmpAV("uid", "102f"))
       {
        FDataRow->Value["32D8"] = icsNewGUID(); // extedit name='cguid' uid='32D8'
        FDataRow->Value["017B"] = Variant::Empty(); // extedit name='Код пациента' uid='017B'
        FDataRow->Value["32AF"] = Variant::Empty(); // extedit name='Код плана' uid='32AF'
        FDataRow->Value["1031"] = Variant::Empty(); // date name='Дата выполнения' uid='1031'
        FDataRow->Value["10B5"] = Variant::Empty(); // extedit name='Возраст' uid='10B5'
        FDataRow->Value["1032"] = Variant::Empty(); // choiceDB name='Проба' uid='1032'
        FDataRow->Value["1200"] = Variant::Empty(); // choiceDB name='Инфекция' uid='1200'
        FDataRow->Value["1033"] = Variant::Empty(); // text name='Серия препарата' uid='1033'
        FDataRow->Value["1034"] = Variant::Empty(); // choice name='Повод' uid='1034'
        FDataRow->Value["103B"] = 0; // binary name='Реакция проверена' uid='103B'
        FDataRow->Value["10A7"] = Variant::Empty(); // choiceDB name='Формат реакции' uid='10A7'
        FDataRow->Value["103F"] = Variant::Empty(); // digit name='Значение' uid='103F'
        FDataRow->Value["0184"] = Variant::Empty(); // digit name='Значение 2' uid='0184'
        FDataRow->Value["0186"] = Variant::Empty(); // digit name='Значение 3' uid='0186'
        FDataRow->Value["0188"] = Variant::Empty(); // digit name='Значение 4' uid='0188'
        FDataRow->Value["018A"] = Variant::Empty(); // digit name='Значение 5' uid='018A'
        FDataRow->Value["1093"] = Variant::Empty(); // extedit name='Схема' uid='1093'
        FDataRow->Value["10A5"] = FInLPU; // binary name='Выполнена в данном ЛПУ' uid='10A5'
        FDataRow->Value["018B"] = FLPU; // choiceDB name='ЛПУ' uid='018B'
        FDataRow->Value["10B2"] = FDoc; // choiceDB name='Врач' uid='10B2'
        FDataRow->Value["018D"] = FMedSis; // choiceDB name='Мед. сестра' uid='018D'
        FDataRow->Value["10B3"] = FFinSrc; // choiceDB name='Источник финансирования' uid='10B3'
        FDataRow->Value["018E"] = FExtInf; // choiceDB name='Доп. информация' uid='018E'
        FDataRow->Value["10C1"] = Variant::Empty(); // digit name='Предыдущее значение реакции' uid='10C1'
        FDataRow->Value["10C2"] = Variant::Empty(); // digit name='Предыдущий формат реакции' uid='10C2'
        FDataRow->Value["330B"] = Variant::Empty(); // date name='Дата списания' uid='330B'/>
        FDataRow->Value["1156"] = APtBirthDay; // date name='ДР пациента' uid='1156'
       }
      else if (FDefNode->CmpAV("uid", "1100"))
       {
        FDataRow->Value["32D9"] = icsNewGUID(); // extedit name='cguid'
        FDataRow->Value["1101"] = Variant::Empty(); // extedit name='Код пациента'
        FDataRow->Value["32B0"] = Variant::Empty(); // extedit name='Код плана'
        FDataRow->Value["1104"] = Variant::Empty(); // date name='Дата выполнения'
        FDataRow->Value["1105"] = Variant::Empty(); // extedit name='Возраст'
        FDataRow->Value["1103"] = Variant::Empty(); // choiceDB name='Проверка'
        FDataRow->Value["110F"] = Variant::Empty(); // choiceDB name='Результат'
        FDataRow->Value["319B"] = Variant::Empty(); // text name='Результат(значение)'
        FDataRow->Value["319C"] = Variant::Empty(); // text name='№ ответа'
        FDataRow->Value["1107"] = Variant::Empty(); // extedit name='Схема'
        FDataRow->Value["110A"] = FInLPU; // binary name='Выполнена в данном ЛПУ'
        FDataRow->Value["1108"] = FLPU; // choiceDB name='ЛПУ'
        FDataRow->Value["110B"] = FDoc; // choiceDB name='Врач'
        FDataRow->Value["110C"] = FMedSis; // choiceDB name='Мед. сестра'
        FDataRow->Value["110D"] = FFinSrc; // choiceDB name='Источник финансирования'
        FDataRow->Value["110E"] = FExtInf; // choiceDB name='Доп. информация'
        // FDataRow->Value["330C"] = AUCode;  // date name='Дата списания'
        FDataRow->Value["1160"] = APtBirthDay; // date name='ДР пациента'
       }
      else if (FDefNode->CmpAV("uid", "3030"))
       {
        FDataRow->Value["32DA"] = icsNewGUID(); // extedit name='cguid'
        FDataRow->Value["317C"] = Variant::Empty(); // <extedit name='Код пациента' uid='317C'
        FDataRow->Value["305F"] = Variant::Empty(); // <date name='Начало действия' uid='305F' required='1'
        FDataRow->Value["3065"] = Variant::Empty(); // <date name='Окончание действия' uid='3065'
        FDataRow->Value["3043"] = Variant::Empty(); // <choice name='Отвод' uid='3043' required='1'
        FDataRow->Value["3083"] = Variant::Empty(); // <text name='Список инфекций' uid='3083'
        FDataRow->Value["3066"] = Variant::Empty(); // <choice name='Причина' uid='3066' required='1'
        FDataRow->Value["3082"] = Variant::Empty(); // <extedit name='Заболевание' uid='3082' required='1'
        FDataRow->Value["3110"] = Variant::Empty(); // <binary name='Отвод от прививки' uid='3110'
        FDataRow->Value["3111"] = Variant::Empty(); // <binary name='Отвод от пробы' uid='3111'
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TInsertCardItem::Execute(__int64 AUCode, UnicodeString & ARecID, UnicodeString & ARC)
 {
  bool RC = false;
  try
   {
    try
     {
      if (FInsertData && FDefNode)
       {
        if (FDefNode->CmpAV("uid", "1003"))
         {
          FDataRow->Value["017A"] = AUCode; // extedit name='Код пациента'
         }
        else if (FDefNode->CmpAV("uid", "102f"))
         {
          FDataRow->Value["017B"] = AUCode; // extedit name='Код пациента'
         }
        else if (FDefNode->CmpAV("uid", "1100"))
         {
          FDataRow->Value["1101"] = AUCode; // extedit name='Код пациента'
         }
        else if (FDefNode->CmpAV("uid", "3030"))
         {
          FDataRow->Value["317C"] = AUCode; // extedit name='Код пациента'
         }
        RC = FInsertData("card." + FDefNode->AV["uid"], FDataRow->GetJSONValues(), ARecID, ARC);
       }
     }
    catch (System::Sysutils::Exception & E)
     {
      ARC = E.Message;
     }
    catch (...)
     {
      ARC = "Ошибка";
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TInsertCardItem::FLoadJSONData(TJSONObject * AData, TStrings * AList)
 {
  TJSONPairEnumerator * itPair;
  UnicodeString FClStr;
  int FClCode;
  itPair = AData->GetEnumerator();
  while (itPair->MoveNext())
   {
    FClCode = ((TJSONString *)itPair->Current->JsonString)->Value().ToIntDef(0);
    FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
    AList->AddObject(FClStr, (TObject *)FClCode);
   }
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TInsertCardItem::GetListData(UnicodeString ASel, UnicodeString ACode)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FGetValById)
     FGetValById(ASel, ACode, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TInsertCardItem::GetPrivVar(UnicodeString ASch)
 {
  UnicodeString RC = "";
  UnicodeString SQL = "reg.s2.";
  TStringList * InfList = new TStringList;
  try
   {
    if (FSchDefNode && FGetValById)
     {
      TTagNode * LineNode = FSchDefNode->GetTagByUID(ASch);
      if (LineNode && LineNode->CmpName("line"))
       {
        TTagNode * SchNode = LineNode->GetParent();
        UnicodeString VacCode = SchNode->AV["ref"];
        SQL += "Select R003E as ps1, R003B as ps2 From CLASS_002D where R003B=" + VacCode;
        FLoadJSONData(GetListData(SQL), InfList);
        // inf_code:vactype^uid_scheme.uid_scheme_line
        // "1021":"V1:3^016A;V1:4^016A;V1:5^016A",
        for (int i = 0; i < InfList->Count; i++)
         {
          if (RC.Length())
           RC += ";";
          RC += LineNode->AV["name"] + ":";
          RC += IntToStr((int)InfList->Objects[i]) + "^";
          RC += SchNode->AV["uid"] + "." + LineNode->AV["uid"];
         }
       }
     }
   }
  __finally
   {
    delete InfList;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TInsertCardItem::GetTestInf(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    TJSONObject * DataRC = GetListData("reg.f.002A.003D", ACode);
    TJSONPairEnumerator * itPair;
    UnicodeString FClStr;
    int FClCode;
    itPair = DataRC->GetEnumerator();
    while (itPair->MoveNext())
     RC = ((TJSONString *)itPair->Current->JsonValue)->Value().ToIntDef(0);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TInsertCardItem::FillVacVar(UnicodeString ACode, TStrings * AItems)
 {
  TTagNode * itSch;
  TTagNode * itLine;
  UnicodeString FBaseSch;
  AItems->Clear();
  try
   {
    if (FSchDefNode)
     {
      itSch = FSchDefNode->GetChildByName("schema", true);
      while (itSch)
       {
        if (itSch->CmpAV("ref", ACode))
         { // схема для МИБП, пробы ACode
          if (itSch->CmpAV("def", "1"))
           {
            FBaseSch = " стартовая схема ";
           }
          else
           {
            FBaseSch = " схема ";
           }
          itLine = itSch->GetFirstChild();
          while (itLine)
           {
            if (itLine->CmpName("line"))
             {
              AItems->AddObject(itLine->AV["name"] + FBaseSch + itSch->AV["name"],
              (TObject *)UIDInt(itLine->AV["uid"]));
             }
            itLine = itLine->GetNext();
           }
         }
        itSch = itSch->GetNext();
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TInsertCardItem::GetSchLineByVacVar(UnicodeString AVacVar, UnicodeString AMIBPCode)
 {
  TTagNode * itSch;
  TTagNode * itLine;
  UnicodeString RC = "";
  try
   {
    if (FSchDefNode)
     {
      itSch = FSchDefNode->GetChildByName("schema", true);
      while (itSch)
       {
        if (itSch->CmpAV("ref", AMIBPCode) && itSch->CmpAV("def", "1"))
         { // схема для МИБП, пробы ACode и это схема по умолчанию
          itLine = itSch->GetChildByAV("line", "name", AVacVar);
          if (itLine)
           {
            RC    = itSch->AV["uid"] + "." + itLine->AV["uid"];
            itSch = NULL;
           }
         }
        if (itSch)
         itSch = itSch->GetNext();
       }
      if (!RC.Length())
       { // схему по умолчанию не нашли
        itSch = FSchDefNode->GetChildByAV("schema", "ref", AMIBPCode, true);
        if (itSch)
         { // схема для МИБП, пробы ACode
          itLine = itSch->GetChildByAV("line", "name", AVacVar);
          if (itLine)
           RC = itSch->AV["uid"] + "." + itLine->AV["uid"];
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TInsertCardItem::Preload()
 {
  TJSONObject * RetData = NULL;
  TJSONPairEnumerator * itPair;
  UnicodeString FClCode, FClStr;
  try
   {
    RetData = GetListData("reg.s2.Select CODE as ps1, R0064 as ps2 From CLASS_0035", "0");
    if (RetData)
     {
      itPair = RetData->GetEnumerator();
      while (itPair->MoveNext())
       {
        FClCode                          = ((TJSONString *)itPair->Current->JsonString)->Value();
        FClStr                           = ((TJSONString *)itPair->Current->JsonValue)->Value();
        MIBPBinReak[FClCode.ToIntDef(0)] = (FClStr == "1");
       }
     }
    ReakValEnabled.clear();
    RetData = GetListData("reg.s2.Select CODE as ps1, R115A as ps2 From CLASS_0038", "0");
    if (RetData)
     {
      itPair = RetData->GetEnumerator();
      while (itPair->MoveNext())
       {
        FClCode                             = ((TJSONString *)itPair->Current->JsonString)->Value();
        FClStr                              = ((TJSONString *)itPair->Current->JsonValue)->Value();
        ReakValEnabled[FClCode.ToIntDef(0)] = FClStr.ToIntDef(0);
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TInsertCardItem::CheckInput(UnicodeString AMIBPCode, UnicodeString AReacCode, UnicodeString AReacVal,
  bool fullCheck)
 {
  bool RC = AMIBPCode.ToIntDef(0);
  try
   {
    if (RC && (!FBinReak || FBinReak && !MIBPBinReak[AMIBPCode.ToIntDef(0)]))
     {
      if (fullCheck)
       {
        RC &= AReacCode.Length();
        if (RC && ReakValEnabled[AReacCode.ToIntDef(0)])
         {
          RC &= AReacVal.Length();
         }
       }
      else
       {
        if (AReacCode.Length())
         {
          if (RC && ReakValEnabled[AReacCode.ToIntDef(0)])
           {
            RC &= AReacVal.Length();
           }
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TInsertCardItem::ReakIsBin(int AMIBPCode)
 {
  bool RC = FBinReak;
  try
   {
    TBinReak::iterator FRC = MIBPBinReak.find(AMIBPCode);
    if (FRC != MIBPBinReak.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TInsertCardItem::ReakCanVal(int AReacCode)
 {
  bool RC = false;
  try
   {
    TBinReak::iterator FRC = ReakValEnabled.find(AReacCode);
    if (FRC != ReakValEnabled.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TInsertCardItem::FillReacVar(TStrings * AItems, int AMIBPCode, int APlType)
 {
  TJSONObject * RetData = NULL;
  TJSONPairEnumerator * itPair;
  UnicodeString FClCode, FClStr;
  // ReacVar->Properties->OnChange = NULL;
  AItems->Clear();
  UnicodeString Req1 = "";
  try
   {
    TStringList * ValList = NULL;
    UnicodeString ReakCode = IntToStr(APlType) + "_" + IntToStr(AMIBPCode);
    TReakList::iterator FRC = ReacMap->find(ReakCode);
    if (FRC == ReacMap->end())
     {
      ValList = new TStringList;
      (*ReacMap)[ReakCode] = ValList;
      switch (APlType)
       {
       case 1: // Caption = "Vac";
         {
          if (!ReakIsBin(AMIBPCode))
           Req1 = "reg.s2.Select fr.CODE as ps1, fr.R008E as ps2 From CLASS_0038 fr left join class_0039 frm on (fr.code=frm.r0091) where frm.R0090="
             + IntToStr(AMIBPCode);
          break;
         }
       case 2: // Caption = "Test";
         {
          Req1 = "reg.s2.Select fr.CODE as ps1, fr.R008E as ps2 From CLASS_0038 fr left join class_002B frm on (fr.code=frm.r0037) where frm.R002E="
            + IntToStr(AMIBPCode);
          break;
         }
       case 3: // Caption = "Check";
         {
          break;
         }
       }
      if (Req1.Length())
       {
        RetData = GetListData(Req1, "0");
        if (RetData)
         {
          itPair = RetData->GetEnumerator();
          while (itPair->MoveNext())
           {
            FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
            FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
            ValList->AddObject(FClStr, (TObject *)FClCode.ToIntDef(0));
           }
         }
       }
     }
    else
     ValList = FRC->second;
    AItems->Assign(ValList);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall GetTLValue(TcxTreeListNode * ANode, int AIdx)
 {
  UnicodeString RC = "";
  try
   {
    if (ANode->TreeList->IsEditing && (ANode->TreeList->FocusedColumn->ItemIndex == AIdx) && ANode->Selected)
     RC = VarToStr(ANode->TreeList->InplaceEditor->EditingValue);
    else
     RC = ANode->Texts[AIdx];
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
/*
TJSONObject * __fastcall TInsertCardItem::GetJSONValues()
 {
  TJSONObject * RC = new TJSONObject;
  try
   {
    TTagNode * itFl = FRowDefNode->GetFirstChild();
    UnicodeString FVal, FName;
    Variant tmpVal;
    while (itFl)
     {
      FName = "";
      if (!itFl->CmpName("code,data"))
       FName = itFl->AV["uid"];
      else if (itFl->CmpName("code"))
       FName = itFl->Name;
      ShowMessage("name: "+FName);
      if (FName.Length())
       {
        tmpVal = FDataRow->Value[FName];
        ShowMessage("Value: "+VarToStr(tmpVal));
        if (!(tmpVal.IsEmpty() || tmpVal.IsNull()))
         {
          if (itFl->CmpName("date"))
           {
            if (!int(tmpVal))
             RC->AddPair(FName, new TJSONNull);
            else
             RC->AddPair(FName, tmpVal);
           }
          else
           RC->AddPair(FName, tmpVal);
         }
        else
         RC->AddPair(FName, new TJSONNull);
       }
      itFl = itFl->GetNext();
     }
        ShowMessage("----------------- -dddddddddddddddddd");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
*/
