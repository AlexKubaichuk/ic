﻿//---------------------------------------------------------------------------

#ifndef InsertCardItemH
#define InsertCardItemH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
#include "KabCustomDS.h"
#include "dsRegTypeDef.h"
#include "cxTL.hpp"
//---------------------------------------------------------------------------
class TInsertCardItem : public TObject
{
private:	// User declarations
  TkabCustomDataSetRow *FDataRow;
  TTagNode *FDefNode, *FRowDefNode, *FSchDefNode;

  Variant FInLPU, FLPU, FDoc, FMedSis, FFinSrc, FExtInf;

  TdsGetValByIdEvent FGetValById;
  TdsInsertDataEvent FInsertData;

  bool notReak;
  bool FBinReak;
  typedef map<int, bool> TBinReak;
  TBinReak MIBPBinReak;
  TBinReak ReakValEnabled;

  typedef map<UnicodeString, TStringList*> TReakList;
  TReakList *ReacMap;
//  TJSONObject * __fastcall GetJSONValues();

  void __fastcall TInsertCardItem::FLoadJSONData(TJSONObject *AData, TStrings *AList);
  TJSONObject* __fastcall GetListData(UnicodeString ASel, UnicodeString ACode = "0");
public:		// User declarations
  __fastcall TInsertCardItem(TTagNode *ADef, TTagNode *ASchDef);
  __fastcall ~TInsertCardItem();

  __property TdsGetValByIdEvent OnGetValById = {read = FGetValById, write=FGetValById};
  __property TdsInsertDataEvent OnInsertData = {read = FInsertData, write = FInsertData};

  __property TkabCustomDataSetRow *Data = {read=FDataRow};
  __property Variant InLPU = {read = FInLPU, write = FInLPU};
  __property Variant LPU = {read = FLPU, write = FLPU};
  __property Variant Doc = {read = FDoc, write = FDoc};
  __property Variant MedSis = {read = FMedSis, write = FMedSis};
  __property Variant FinSrc = {read = FFinSrc, write = FFinSrc};
  __property Variant ExtInf = {read = FExtInf, write = FExtInf};
  __property bool BinReak = {read = FBinReak, write = FBinReak};

  void __fastcall NewRec(TDate APtBirthDay);
  bool __fastcall Execute(__int64 AUCode, UnicodeString & ARecID, UnicodeString & ARC);
  UnicodeString __fastcall GetPrivVar(UnicodeString ASch);
  UnicodeString __fastcall GetTestInf(UnicodeString ACode);
  UnicodeString __fastcall GetSchLineByVacVar(UnicodeString AVacVar, UnicodeString AMIBPCode);
  UnicodeString __fastcall Preload();
  void __fastcall FillVacVar(UnicodeString ACode, TStrings * AItems);
  bool __fastcall CheckInput(UnicodeString AMIBPCode, UnicodeString AReacCode, UnicodeString AReacVal, bool fullCheck = false);
  void __fastcall FillReacVar(TStrings * AItems, int AMIBPCode, int APlType);
  bool __fastcall ReakIsBin(int AMIBPCode);
  bool __fastcall ReakCanVal(int AReacCode);
};
extern PACKAGE UnicodeString __fastcall GetTLValue(TcxTreeListNode *ANode, int AIdx);
//---------------------------------------------------------------------------
#endif
