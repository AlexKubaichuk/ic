//---------------------------------------------------------------------------
#ifndef dsICCommonTypeDefH
#define dsICCommonTypeDefH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include "XMLContainer.h"
#include "KabCustomDS.h"
#include "dsRegExtFilterSetTypes.h"
#include "dsRegTypeDef.h"
//---------------------------------------------------------------------------
typedef bool __fastcall (__closure *TdsGetF63Event)(System::UnicodeString ARef, System::UnicodeString &RC);
typedef UnicodeString __fastcall (__closure *TdsGetVacDefDozeEvent)(TDate APrivDate, TDate APatBirthday, System::UnicodeString AVacCode);
//---------------------------------------------------------------------------
#endif

