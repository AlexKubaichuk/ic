//---------------------------------------------------------------------------
//***************************************************************************
//
//                      ��������� ������ � �� �����.
//      ��� ������ ������ Execute ������������ ����� ������ ������.
//
//      ��� ������ ����������:
//      1. ��������� ���������� ��. ��� ����� ������-���� ��� �����
//         �������� ��������� �� TpFIBDatabase � ������������
//      1.1 ��������� �� ������ ��������� ����������� �������, ��������������
//          ����������� �������� �����������.
//      2. ������� ����� ���������� Execute(UnicodeString =""), ������� ���������
//         � ���������� ����������. ��� �������� - ��������� ����� ���
//         �������������� / ����������.
//
//
//***************************************************************************

#ifndef KLADRH
#define KLADRH
//---------------------------------------------------------------------------
/*
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "FIBDatabase.hpp"
#include "FIBQuery.hpp"
#include "pFIBDatabase.hpp"
#include "pFIBQuery.hpp"
*/
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "FIBDatabase.hpp"
#include "FIBQuery.hpp"
#include "pFIBDatabase.hpp"
#include "pFIBQuery.hpp"



#include <cxLabel.hpp>
#include <cxControls.hpp>
#include <cxStyles.hpp>
#include "cxDropDownEdit.hpp"
#include <cxTL.hpp>
#include "cxButtons.hpp"
#include "cxHint.hpp"

#include "KLADRParsers.h"
//---------------------------------------------------------------------------
typedef bool __fastcall (__closure *TGetButtonBitmapEvent)(int ABtnIdx, TcxEditButton *AButton, UnicodeString &AHint);
//---------------------------------------------------------------------------
typedef bool __fastcall (__closure *KLADRSearchFunc)(UnicodeString adresStr, UnicodeString name, UnicodeString &ret_code, bool ABeginFrom);
enum EKLADRSearchType {stRegion = 1, stNP, stStreet, stNP_WC, stStreet_WC };
typedef enum {cctReg, cctTown, cctStreet, cctHouse, cctFlat, cctAll} TCascadeClearType;

//---------------------------------------------------------------------------

typedef map <UnicodeString, CTTCacheRecord*> CTTCacheMap;
typedef map <UnicodeString, TControl*> formControls;
typedef map <UnicodeString, TcxTreeListNode*> nodesMap;
//typedef map <int, TcxTreeList*> tlByRegMap;//!!!-
//typedef map <int, TcxTreeListNodes*> tlByRegMap;

//---------------------------------------------------------------------------
class PACKAGE TKLADRFORM : public TComponent
{
protected:

private:
    bool       FRegionEnabled;  // ����� ������� ���/����
    bool       FTownEnabled;    // ����� ���.������ ���/����
    bool       FTownRequired;
    bool       FStreetEnabled;  // ����� ����� ���/����
    bool       FStreetRequired; // ��������� ����� ���/����
    bool       FStreetVisible;  // ����������� ����� ���/����
    bool       FHouseRequired;  // ��������� ���� ���/����
    bool       FHouseVisible;   // ����������� ���� ���/����
    bool       FFlatRequired;   // ��������� �������� ���/����
    bool       FFlatVisible;    // ����������� �������� ���/����
    bool       FIndexVisible;   // ����������� �������
    bool       firstIDChange;   // ����, ����������� ��������� ���������� �����
                                // (���������� �� ����� �������������)
    bool       dataPrepared;    // ���� ������������� ���������� ������ � ������
    bool       expandSocrs;     // ���� ����������� ����������
    bool       showOldNames;    // ���� ���������� ������ �������� � ������ ������
    bool       useCTTCache;     // ���� ������������� ���� ��� ������� CodeToText, CodeToIndeks
    bool       templateMode;
    bool       isFiltered;
    bool       FSaveNFAddrPrompt;
    bool       FSaveModifAddrPrompt;
    int        FStreetTypeDefIndex;
    int        FFlatTypeDefIndex;
    int        FFlatTypeDefaultIdx;
    UnicodeString FSaveIndeks;
    UnicodeString FDefaultAddr;
    TcxHintStyleController *FHintCtrl;
    bool       FHintShowing;
    bool       FCanShowHint;
    bool       FCanHideHint;
    bool       FNFAddrModify;
    bool       FIsNFAddr;
    UnicodeString FAdresCodeStr;
    UnicodeString FAdresShortCodeStr;
    UnicodeString FHouseStr;
    UnicodeString FFlatStr;
    adresCode         adres; // ������� �����
    adresCode     origAdres; // �������������� �����
    UnicodeString  SQLTemplate;
    UnicodeString adresTextStr;
    TcxStyle*        grayed; // ����� ��� �������� ���.�������
    TcxStyle*        normal;
    TcxStyle*       content;
    TcxStyle*     selection;
    TForm*             form; // ������������ � ������ ����������
    TPanel*             pnl;
    TpFIBQuery*           Q;
//    TpFIBQuery*          Q2;
    TpFIBDatabase*       FDB;
    TpFIBTransaction*    TR;
    formControls*     ctrls; // ����� ���� ���������
    StringIntMap*   regions; // ����� �������� (�������� - ��)
    StringIntMap*   streets; // ����� ����
    StringIntMap*    houses; // ����� �����
    StrStrMap*         socr; // ����� ���������� (���� = ���������� � �.�.)
    nodesMap*       tlNodes; // ����� ���-���������� �������
//    tlByRegMap*   regionsTL; // ����� ���������(���.������) �� �������� (��� ��������� ���������)
    CTTCacheMap*   CTTCache; // ����� ��� ���� 
    UnicodeString  curTemplate; // ������� ������ �������
    TColor         reqColor; // ���� ���������
    KLADRSearchFunc searchFuncs[6];
    StringIntMap    FExtStreetObj;
    StringIntMap    FStreetTypeObj;
    StringIntMap    FFlatTypeObj;
    StringIntMap    FStreetTypeSocrObj;
    StringIntMap    FFlatTypeSocrObj;
    StrStrMap       FFindedObjects;
    TGetButtonBitmapEvent FOnGetButtonBitmap;

    void         __fastcall FQPrepare(UnicodeString ASQL);
    void         __fastcall FQExec(UnicodeString ASQL);

    void         __fastcall FSetCtrlProps (TControl *ADest, UnicodeString AName, UnicodeString ACaptText, int ALeft, int ATop, int AWidth, int AHeight, int &ATabOrder, TWinControl *AParent = NULL);

    TcxControl*    __fastcall GetCtrl(const UnicodeString ACtrlName);
    TcxLabel*      __fastcall GetLabCtrl(const UnicodeString ACtrlName);
    TcxMaskEdit*   __fastcall GetMaskEditCtrl(const UnicodeString ACtrlName);
    TcxButtonEdit* __fastcall GetButtonEditCtrl(const UnicodeString ACtrlName);
    TcxListBox*    __fastcall GetListBoxCtrl(const UnicodeString ACtrlName);
    TcxComboBox*   __fastcall GetComboBoxCtrl(const UnicodeString ACtrlName);
    TcxTreeList*   __fastcall GetTreeListCtrl(const UnicodeString ACtrlName);
    TcxButton*     __fastcall GetBtnCtrl(const UnicodeString ACtrlName);

    bool __fastcall textToCodeReg       (UnicodeString adresStr, UnicodeString name, UnicodeString &ret_code, bool ABeginFrom = false);
    bool __fastcall textToCodeNP        (UnicodeString adresStr, UnicodeString name, UnicodeString &ret_code, bool ABeginFrom = false);
    bool __fastcall textToCodeStreet    (UnicodeString adresStr, UnicodeString name, UnicodeString &ret_code, bool ABeginFrom = false);
    bool __fastcall textToCodeReg_WC    (UnicodeString adresStr, UnicodeString name, UnicodeString &ret_code, bool ABeginFrom = false);
    bool __fastcall textToCodeNP_WC     (UnicodeString adresStr, UnicodeString name, UnicodeString &ret_code, bool ABeginFrom = false);
    bool __fastcall textToCodeStreet_WC (UnicodeString adresStr, UnicodeString name, UnicodeString &ret_code, bool ABeginFrom = false);
    bool __fastcall FtextToCodeList     (UnicodeString ACurAddrCode, TNFAddrItems *ASrcList, int ACurIdx, TAddrParseType ASearchState, bool ABeginFrom, TStringList *ARetCodes, bool &AOverMaxRC);

    int  __fastcall FGetLastLevelIdx    (int ALvl1, int ALvl2, int ALvl3, int ALvl4);

    void       __fastcall AddUnique(TStringList *ADest, UnicodeString ASrc);
    bool       __fastcall IndexToCodeList(UnicodeString AName, TStringList *ARetCodes);
    bool       __fastcall RegTextToCodeList(UnicodeString AName, TStringList *ARetCodes, bool ABeginFrom = false);
    bool       __fastcall NPTextToCodeList(UnicodeString AAdresStr, TNFAddrWords *AName, TStringList *ARetCodes, bool ABeginFrom = false, int AStatusBeg = -1, int AStatusEnd = -1);
    bool       __fastcall StreetTextToCodeList(UnicodeString AAdresStr, TNFAddrWords *AName, TStringList *ARetCodes, bool ABeginFrom = false, bool AFullSearch = false);
    bool       __fastcall HouseTextToCodeList(UnicodeString AAdresStr, TNFAddrWords *AName, TStringList *ARetCodes);
    bool       __fastcall FlatTextToCodeList(UnicodeString AAdresStr, TNFAddrWords *AName, TStringList *ARetCodes);

    bool       __fastcall IsActiveControl(UnicodeString AName);
    void       __fastcall FSetActivControl();
    TpFIBDatabase* __fastcall getDB ();
    void __fastcall setDB (TpFIBDatabase* ADB);

    bool       __fastcall connectToDB();
    void       __fastcall prepareData();
    void       __fastcall createVisual();
    void       __fastcall deleteVisual();
    void       __fastcall createColumns(TcxTreeList* tl);
    void       __fastcall fillRegions();
    void       __fastcall fillSocrs();
    void       __fastcall fillTowns();
    void       __fastcall queryToTreeList (TcxTreeList*, TpFIBQuery*);
    UnicodeString __fastcall getFullName (UnicodeString name, UnicodeString object);
    void       __fastcall fillStreetObjType(bool AFillStreetType);
    void       __fastcall FSetDefStreetType();
    void       __fastcall fillStreets(bool ACheckLastState = true);
    void       __fastcall fillDoma();
    void       __fastcall fillFlats();
    void       __fastcall cascadeIDClear(TCascadeClearType ALevel);
    UnicodeString __fastcall FObjectFullName(UnicodeString AName, UnicodeString AObjType);
    UnicodeString __fastcall FGetSQLTemplate();
    bool       __fastcall FIsNotFormalAddr();
    int        __fastcall FGetObjCode(StringIntMap *AMap, UnicodeString AVal);
    bool __fastcall nasPunktExists             (UnicodeString npID);
    bool __fastcall domExists                  (UnicodeString dom_number);
    bool __fastcall CacheEntryExists           (UnicodeString code);
    unsigned int __fastcall getTime            (); // ��� ������ ������� ����������
    UnicodeString __fastcall getIndeks            (adresCode adres);
    UnicodeString __fastcall FCalcIndeks          (adresCode adres);
    UnicodeString __fastcall getStreetTypeText(TStreetObjectType AStreetObjType);
    UnicodeString __fastcall getFlatTypeText(TFlatObjectType AFlatType);


    // ������
    void       __fastcall onFormClose            (TObject* Sender, TCloseAction& cAction);
    void       __fastcall onFormShow             (TObject* Sender);
    void       __fastcall updateControlsState(); // ��������/��������� �������� ������������ �� ���������
    void       __fastcall FOnNFAddrChanged(TObject *Sender);
    void       __fastcall FOnRegionChanged(TObject* Sender);
    void       __fastcall FOnTownChanged(TcxCustomTreeList *Sender, TcxTreeListNode *APrevFocusedNode, TcxTreeListNode *AFocusedNode);
    void       __fastcall FOnTownExit(TObject* Sender);
    void       __fastcall FOnStreetTypeEnter(TObject *Sender);
    void       __fastcall FOnStreetTypeChanged(TObject *Sender);
    void       __fastcall FOnStreetChanged(TObject* Sender);
    void       __fastcall FOnHouseChanged(TObject *Sender);
    void       __fastcall FOnVldChanged(TObject *Sender);
    void       __fastcall FOnKorpChanged(TObject *Sender);
    void       __fastcall FOnBuildChanged(TObject *Sender);
    void       __fastcall FOnHouseTextValidate(TObject *Sender, Variant &DisplayValue, TCaption &ErrorText, bool &Error);
    void       __fastcall FOnFlatTypeChanged(TObject *Sender);
    void       __fastcall FOnFlatTextChanged(TObject* Sender);
    void       __fastcall FOnIndeksChanged(TObject* Sender);
    int        __fastcall FGetNewNFCode();
    void       __fastcall FSaveNFAddr();
    void __fastcall OkBtnClicked           (TObject* Sender);
    void __fastcall CancelBtnClicked       (TObject* Sender);

    void       __fastcall FSetFindedAddr(UnicodeString AAddrCodeStr);
    void       __fastcall NFAddrLBDblClick(TObject *Sender);
    void __fastcall NFAddrLBExit           (TObject* Sender);

    void       __fastcall FOnNFAddrBtnClick(TObject *Sender, int AButtonIndex);

    void       __fastcall FOnCBKeyDown(TObject* Sender, WORD &Key, TShiftState Shift);
    void       __fastcall FOnNFAddrLBKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
    void       __fastcall FOnNFAddrBEKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
    void       __fastcall FOnFormKeyDown(TObject* Sender, WORD &Key, TShiftState Shift);

    void __fastcall FHintMouseEnter        (TObject *Sender);
    void __fastcall FHintMouseLeave        (TObject *Sender);
    void __fastcall FHouseEnter            (TObject *Sender);
    void __fastcall FHouseExit             (TObject *Sender);


    void __fastcall setAdresCodeStr(UnicodeString AVal);       // ��������� �������������� �����

    void __fastcall FSetStreetRequired  (bool nv);
    void __fastcall FSetStreetVisible  (bool nv);
    void __fastcall FSetHouseRequired  (bool nv);
    void __fastcall FSetHouseVisible  (bool nv);
    void __fastcall FSetFlatRequired (bool nv);
    void __fastcall FSetFlatVisible (bool nv);
    UnicodeString __fastcall PrepareStreetObjectVal(UnicodeString AVal);

public:
//    adresCode  __fastcall parseAdresStr        (UnicodeString adr);

    bool __fastcall Execute           (bool isTemplate, UnicodeString adr ="use_default");
    bool __fastcall isAdresCorrect    (UnicodeString code);
    void __fastcall FlushCTTCache     ();
    void __fastcall ClearCacheEntry(UnicodeString code);
    UnicodeString __fastcall codeToText  (UnicodeString code, UnicodeString  params ="11111111", bool ANotNFAddr = false, bool AReplaceCach = false); // �������������� ���� ������ � ��� ��������� �����������
    UnicodeString __fastcall codeToTextEx (UnicodeString code);
    bool       __fastcall TextToCode  (UnicodeString code, UnicodeString name, int searchType, UnicodeString &ret_code, bool ABeginFrom = false); //
    bool       __fastcall textToCodeList  (UnicodeString AAddr, TStringList *ARetCodes, bool ABeginFrom = false, bool ASilent=false); //
    __fastcall TKLADRFORM  (TComponent* Owner);
    __fastcall ~TKLADRFORM ();

    __property UnicodeString AddrCode = { read = FAdresCodeStr, write = setAdresCodeStr};       // ��������� �������������� �����, (adres ���� ���� ������ ������ ��), origAdres �����
    __property UnicodeString AddrShortCode = { read = FAdresShortCodeStr};  // ��� ������ ��� ���� � ��������

    __property UnicodeString adresSQLTemplate = { read = SQLTemplate};
    __property UnicodeString AddrString       = { read = adresTextStr};
    __property UnicodeString House            = { read = FHouseStr};
    __property UnicodeString Flat             = { read = FFlatStr};
    __property UnicodeString DefAddr          = { read = FDefaultAddr, write = FDefaultAddr};

__published:
    __property TpFIBDatabase* Database     = { read = getDB,           write = setDB};
    __property bool UseCodeToTextCache     = { read = useCTTCache,     write = useCTTCache};
    __property bool RegionEnabled          = { read = FRegionEnabled,  write = FRegionEnabled};
    __property bool NasPunktEnabled        = { read = FTownEnabled,    write = FTownEnabled};
    __property bool NasPunktRequired       = { read = FTownRequired,   write = FTownRequired};
    __property bool StreetEnabled          = { read = FStreetEnabled,  write = FStreetEnabled};
    __property bool StreetRequired         = { read = FStreetRequired, write = FSetStreetRequired};
    __property bool StreetVisible          = { read = FStreetVisible,  write = FSetStreetVisible};
    __property bool DomRequired            = { read = FHouseRequired,  write = FSetHouseRequired};
    __property bool DomVisible             = { read = FHouseVisible,   write = FSetHouseVisible};
    __property bool FlatRequired           = { read = FFlatRequired,   write = FSetFlatRequired};
    __property bool FlatVisible            = { read = FFlatVisible,    write = FSetFlatVisible};
    __property bool IndeksVisible          = { read = FIndexVisible,   write = FIndexVisible};
    __property TColor RequiredColor        = { read = reqColor,        write = reqColor};
    __property bool ExpandSocr             = { read = expandSocrs,     write = expandSocrs};
    __property bool ShowOldNames           = { read = showOldNames,    write = showOldNames};
    __property bool CanShowHint            = { read = FCanShowHint,    write = FCanShowHint};
    __property bool SaveNFAddrPrompt       = { read = FSaveNFAddrPrompt,write = FSaveNFAddrPrompt};
    __property bool SaveModifAddrPrompt    = { read = FSaveModifAddrPrompt,write = FSaveModifAddrPrompt};
    __property bool IsNFAddr               = { read = FIsNFAddr};

    __property TGetButtonBitmapEvent OnGetButtonBitmap = { read = FOnGetButtonBitmap, write = FOnGetButtonBitmap};
};

//---------------------------------------------------------------------------

class ESearchException : public Exception
{
    public:
    __fastcall ESearchException (const UnicodeString msg) : Exception (msg) { };
};

#endif
