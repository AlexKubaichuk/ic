//---------------------------------------------------------------------------

#ifndef KLADRUtilH
#define KLADRUtilH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
//#include <cxLabel.hpp>
//#include <cxControls.hpp>
//#include <cxStyles.hpp>
//#include "cxDropDownEdit.hpp"
//#include <cxTL.hpp>
//#include "cxButtons.hpp"
#include <cxHint.hpp>

//#define _DEBUG_MESSAGE

#include "map.h"
//---------------------------------------------------------------------------
// ��������� �� ������� ������
enum NFParseType {psReg, psNP1, psNP2, psNP3, psStreet, psHouse, psFlat, psNone, psEnd};
enum ParseWordType {wtLBR, wtRBR, wtWord, wtNone};
typedef NFParseType TAddrParseType;

typedef map <UnicodeString, int > StringIntMap;
typedef map <UnicodeString, UnicodeString> StrStrMap;

struct PACKAGE adresCode
{
    int level1ID; // ��� �������
    int level2ID; // |
    int level3ID; // | -> ��� ���.������
    int level4ID; // |
    int level5ID; // ��� �����
    int level6ID; // ��� ����
    // �.�. � ����� �� ��� ����� "�����" ����, ������ ��� ��������������
    // ���������� level6IDa(����� ����) level6IDb(������) ��� ���������
    // ����������� ������� �����. ��� ������ ����� level6ID ����� ����� 0
    UnicodeString level6IDa; // ����� ���� (������ ����)
    int level6IDt;        // ��� (���/�) (������ ����)
    UnicodeString level6IDb; // ������ (������ ����)
    UnicodeString level7ID;  // �������� (������ ����)
    int level7IDt;        // ��� (�� / ���� / �/�) (������ ����)
    UnicodeString indeks;

    __fastcall adresCode () { Clear(); };

    void __fastcall Clear ()
    {
        level1ID  = level2ID  = level3ID = level4ID = level5ID = level6ID = level6IDt = level7IDt = 0;
        level6IDa = level6IDb = level7ID = indeks   = "";
    };

    void operator = (adresCode rVal)
    {
        level1ID  = rVal.level1ID;
        level2ID  = rVal.level2ID;
        level3ID  = rVal.level3ID;
        level4ID  = rVal.level4ID;
        level5ID  = rVal.level5ID;
        level6ID  = rVal.level6ID;
        level6IDt = rVal.level6IDt;
        level6IDa = rVal.level6IDa;
        level6IDb = rVal.level6IDb;
        level7IDt = rVal.level7IDt;
        level7ID  = rVal.level7ID;
        indeks    = rVal.indeks;
    }
};
//---------------------------------------------------------------------------
class PACKAGE TNFAddrWords : public TStringList
{
private:
    UnicodeString FText;
    StringIntMap FResWords;
    StrStrMap    FReplasedObjects;

    void __fastcall setText(UnicodeString ASrc);
    UnicodeString __fastcall getWord(int AIdx);
    void __fastcall setWord(int AIdx, UnicodeString AVal);
    UnicodeString __fastcall getPreparedText();
    UnicodeString __fastcall getLast();
    UnicodeString __fastcall getFirst();

    bool __fastcall IsNotBr(char ASrc);
    bool __fastcall IsLBr(char ASrc);
    bool __fastcall IsRBr(char ASrc);
    bool __fastcall Cmp(UnicodeString ASrc);
    bool __fastcall CanClear(UnicodeString ASrc);

    ParseWordType __fastcall getType(int AIdx);
    ParseWordType __fastcall getFirstType();
    ParseWordType __fastcall getLastType();

public:
    __property UnicodeString Word[int AIdx] = {read = getWord, write = setWord};
    __property UnicodeString Last           = {read = getLast};
    __property UnicodeString First          = {read = getFirst};

    __property ParseWordType WordType[int AIdx] = {read = getType};
    __property ParseWordType FirstType          = {read = getFirstType};
    __property ParseWordType LastType           = {read = getLastType};

    __property UnicodeString Text           = {read = FText, write=setText};
    __property UnicodeString PreparedText   = {read = getPreparedText};

    __fastcall TNFAddrWords(UnicodeString ASrc);
    __fastcall ~TNFAddrWords();
};

//---------------------------------------------------------------------------
class PACKAGE TNFAddrItems : public TStringList
{
private:
    UnicodeString FText;
    void __fastcall setText(UnicodeString ASrc);

    TNFAddrWords* __fastcall getItem(int AIdx);
    TNFAddrWords* __fastcall getLast();
    TNFAddrWords* __fastcall getFirst();
public:
    __property UnicodeString Text              = {read = FText, write=setText};

    __property TNFAddrWords* Item[int AIdx] = {read = getItem};

    __property TNFAddrWords* Last           = {read = getLast};
    __property TNFAddrWords* First          = {read = getFirst};

    __fastcall TNFAddrItems(UnicodeString ASrc);
    __fastcall ~TNFAddrItems();
};
//---------------------------------------------------------------------------
enum THouseElementType {htNone, htDom, htVld, htKorp, htStr};
typedef map<THouseElementType, UnicodeString> TElTypeStrMap;
//---------------------------------------------------------------------------
class PACKAGE THouseElement : public TObject
{
public:
  THouseElementType Type;
  UnicodeString TypeStr;
  UnicodeString Value;
  UnicodeString Text;
  int Begin;
  int End;
  __fastcall THouseElement();

};
//---------------------------------------------------------------------------
class PACKAGE THouseElementList : public TList
{
private:
      TElTypeStrMap ElTypeStr;
      THouseElement* __fastcall FGetElement(int Idx);
      THouseElement* __fastcall FGetElementByType(THouseElementType AType);
      THouseElement* __fastcall FGetElementByPos(int CurPos);
public:
      TElTypeStrMap FHouseFormatByType;
      TElTypeStrMap FHouseSelectorsByType;
      UnicodeString FHouseFormats[6];


      __fastcall THouseElementList(UnicodeString AText);
      __fastcall ~THouseElementList();

      void __fastcall GetHouseFormatByType(THouseElementType AType, UnicodeString ATypeStr, UnicodeString &Capt, UnicodeString &Msg);

      __property THouseElement* Items[int Idx] = {read=FGetElement};
      __property THouseElement* ItemByType[THouseElementType AType] = {read=FGetElementByType};
      __property THouseElement* ItemByPos[int CurPos] = {read=FGetElementByPos};
};
//---------------------------------------------------------------------------
extern PACKAGE void __fastcall ShowHouseHint(TcxHintStyleController *AHintCtrl, UnicodeString AText, int Left, int Top);
extern PACKAGE void __fastcall ShowDebug(UnicodeString AText);
//---------------------------------------------------------------------------
#endif
