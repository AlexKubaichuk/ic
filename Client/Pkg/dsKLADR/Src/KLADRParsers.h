//---------------------------------------------------------------------------

#ifndef KLADRParsersH
#define KLADRParsersH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
//#include <cxLabel.hpp>
//#include <cxControls.hpp>
//#include <cxStyles.hpp>
//#include "cxDropDownEdit.hpp"
//#include <cxTL.hpp>
//#include "cxButtons.hpp"
#include <cxHint.hpp>

//#define _DEBUG_MESSAGE

#include <map>

#include "AxeUtil.h"
#include "KLADRConst.h"
//---------------------------------------------------------------------------
using namespace std;
// ��������� �� ������� ������
typedef enum {psReg, psNP1, psNP2, psNP3, psStreet, psHouse, psFlat, psNone, psEnd} NFParseType;
typedef enum {wtLBR, wtRBR, wtWord, wtNone} ParseWordType;
typedef enum {adtNone, adtDigit, adtString, adtDevider, adtRegion, adtFlatRegion, adtFlatRegBegin, adtFlatRegEnd} TFormatedAddrDataType;
typedef enum {ldtNone, ldtAddr, ldtAddrRegBegin, ldtAddrRegEnd, ldtAddrEven, ldtAddrOdd} TAddrListDataType;
typedef enum {htNone, htDom, htVld, htKorp, htStr}THouseElementType;
typedef enum {
              sotNone = 0,      // 0 - �� ������ (��� ����������������� ������� ��� ���� �����/������ �� �����������))
              sotStreet = 1,    // 1 - �����
              sotArmyUnit = 2,  // 2 - �������� �����
              sotOfficeBox = 3, // 3 - ����������� ����
              sotPostOffice= 4  // 4 - ��������� �������� �����
             } TStreetObjectType;

typedef enum {
              fotNone = 0,   // 0 - �� ������
              fotFlat = 1,   // 1 - ��������
              fotOffice = 2, // 2 - ����
              fotRoom = 3    // 3 - ���������
             } TFlatObjectType;

typedef map<THouseElementType, UnicodeString> TElTypeStrMap;
//---------------------------------------------------------------------------
typedef NFParseType TAddrParseType;

typedef map <UnicodeString, int > StringIntMap;
typedef map <UnicodeString, UnicodeString> StrStrMap;

struct PACKAGE adresCode
{
    int FormatVer; // ������ �������
    int RegID;     // ��� �������
    int Town1ID;   // |
    int Town2ID;   // | -> ��� ���.������
    int Town3ID;   // |
    int StreetID;  // ��� �����
    bool NoStreets;
    UnicodeString StreetObjectVal; // �����/������������ ������� 10 �������� (�/�, �/�, ���) ����������� ������ �����
    int   State;                   // 0 - �����������������, 1 - ���������������
    TStreetObjectType StrObjType;  // ��� �������

    TFlatObjectType FlatObjType;   // ��� ���������
    UnicodeString Index;             // ������

    UnicodeString House;              // � ����
    UnicodeString Vld;                // � ��������
    UnicodeString Korp;               // � �������
    UnicodeString Build;              // � ��������

    // <���> :=  (���)? (���)? (����)? (���)?
    UnicodeString HouseFull;          // �. � ���� ���. � �������� ����. � ������� ���. � ��������
    UnicodeString Flat;               // ��������

    int NFAddrID;                  // ��� ������������������ ������

    __fastcall adresCode ();
    void __fastcall Clear ();
    UnicodeString __fastcall AsShortString();
    UnicodeString __fastcall AsString ();
    UnicodeString __fastcall HouseCodeStr();
    UnicodeString __fastcall FlatCodeStr();
    void operator = (adresCode AVal);
    bool operator != (adresCode AVal);
};
//---------------------------------------------------------------------------
class PACKAGE EKLADRError : public Exception
{
public:
    __fastcall EKLADRError(const UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE CTTCacheRecord : public TObject
{
public:
    int Status;
    UnicodeString Values[8];

    __fastcall CTTCacheRecord ( );
    UnicodeString __fastcall GetText(UnicodeString Params = "11111111");
};
//---------------------------------------------------------------------------
class PACKAGE TNFAddrWords : public TStringList
{
private:
    UnicodeString FText;
    StringIntMap FResWords;
    StrStrMap    FReplasedObjects;

    void __fastcall setText(UnicodeString ASrc);
    UnicodeString __fastcall getWord(int AIdx);
    void __fastcall setWord(int AIdx, UnicodeString AVal);
    UnicodeString __fastcall getPreparedText();
    UnicodeString __fastcall getLast();
    UnicodeString __fastcall getFirst();

    bool __fastcall IsNotBr(char ASrc);
    bool __fastcall IsLBr(char ASrc);
    bool __fastcall IsRBr(char ASrc);
    bool __fastcall Cmp(UnicodeString ASrc);
    bool __fastcall CanClear(UnicodeString ASrc);

    ParseWordType __fastcall getType(int AIdx);
    ParseWordType __fastcall getFirstType();
    ParseWordType __fastcall getLastType();

public:
    __property UnicodeString Word[int AIdx] = {read = getWord, write = setWord};
    __property UnicodeString Last           = {read = getLast};
    __property UnicodeString First          = {read = getFirst};

    __property ParseWordType WordType[int AIdx] = {read = getType};
    __property ParseWordType FirstType          = {read = getFirstType};
    __property ParseWordType LastType           = {read = getLastType};

    __property UnicodeString Text           = {read = FText, write=setText};
    __property UnicodeString PreparedText   = {read = getPreparedText};

    __fastcall TNFAddrWords(UnicodeString ASrc);
    __fastcall ~TNFAddrWords();
};

//---------------------------------------------------------------------------
class PACKAGE TNFAddrItems : public TStringList
{
private:
    UnicodeString FText;
    char       FSeparator;
    bool       FExtended;
    void __fastcall setText(UnicodeString ASrc);

    TNFAddrWords* __fastcall getItem(int AIdx);
    TNFAddrWords* __fastcall getLast();
    TNFAddrWords* __fastcall getFirst();
public:
    __property UnicodeString Text              = {read = FText, write=setText};

    __property TNFAddrWords* Item[int AIdx] = {read = getItem};

    __property TNFAddrWords* Last           = {read = getLast};
    __property TNFAddrWords* First          = {read = getFirst};

    __fastcall TNFAddrItems(UnicodeString ASrc, char ASeparator = ',', bool AExtended = false);
    __fastcall ~TNFAddrItems();
};
//---------------------------------------------------------------------------
class PACKAGE TFormatedAddrDataElement : public TObject
{
private:
   UnicodeString FData;
   TFormatedAddrDataType FType;
public:
      __fastcall TFormatedAddrDataElement(UnicodeString AValue,TFormatedAddrDataType AType);
      __property UnicodeString            Value = {read=FData};
      __property TFormatedAddrDataType Type  = {read=FType};
};
//---------------------------------------------------------------------------
class PACKAGE TFormatedAddrData : public TList
{
private:
  UnicodeString FSrcData;
  void __fastcall Parse();
  TFormatedAddrDataElement* __fastcall FGetElement(int Idx);
  TFormatedAddrDataElement* __fastcall FGetElementByType(TFormatedAddrDataType AType);
public:
      __fastcall TFormatedAddrData(UnicodeString ASrc);
      __property TFormatedAddrDataElement* Items[int Idx] = {read=FGetElement};
      __property TFormatedAddrDataElement* ItemByType[TFormatedAddrDataType AType] = {read=FGetElementByType};
};
//---------------------------------------------------------------------------
class PACKAGE THouseElement : public TObject
{
private:
  UnicodeString FValue;
  void __fastcall FSetValue(UnicodeString AValue);
public:
  THouseElementType Type;
  UnicodeString TypeStr;
  UnicodeString Text;
  TFormatedAddrData *FormatedValue;
  int Begin;
  int End;
  __property UnicodeString Value = {read=FValue, write=FSetValue};
  __fastcall THouseElement();
  __fastcall ~THouseElement();

};
//---------------------------------------------------------------------------
class PACKAGE THouseElementList : public TList
{
private:
      TElTypeStrMap ElTypeStr;
      THouseElement* __fastcall FGetElement(int Idx);
      THouseElement* __fastcall FGetElementByType(THouseElementType AType);
      THouseElement* __fastcall FGetElementByPos(int CurPos);
public:
      TElTypeStrMap FHouseFormatByType;
      TElTypeStrMap FHouseSelectorsByType;
      UnicodeString FHouseFormats[6];


      __fastcall THouseElementList(UnicodeString AText);
      __fastcall ~THouseElementList();

      void __fastcall GetHouseFormatByType(THouseElementType AType, UnicodeString ATypeStr, UnicodeString &Capt, UnicodeString &Msg);

      __property THouseElement* Items[int Idx] = {read=FGetElement};
      __property THouseElement* ItemByType[THouseElementType AType] = {read=FGetElementByType};
      __property THouseElement* ItemByPos[int CurPos] = {read=FGetElementByPos};
};
//---------------------------------------------------------------------------
class PACKAGE TAddrListDataElement : public TObject
{
private:
      THouseElementList *FData;
      TAddrListDataType FType;
public:
      __fastcall TAddrListDataElement(UnicodeString AValue, TAddrListDataType AType);
      __fastcall ~TAddrListDataElement();
      __property THouseElementList *Value = {read=FData};
      __property TAddrListDataType  Type  = {read=FType};
};
//---------------------------------------------------------------------------
class PACKAGE TAddrListData : public TList
{
private:
      TAddrListDataElement* __fastcall FGetElement(int Idx);
      TAddrListDataElement* __fastcall FGetElementByType(TAddrListDataType AType);
public:
      void __fastcall Parse(UnicodeString AData);
      bool __fastcall InAddr(UnicodeString AData, UnicodeString AFlat="");
      __fastcall TAddrListData();
      __fastcall ~TAddrListData();
      __property TAddrListDataElement* Items[int Idx] = {read=FGetElement};
      __property TAddrListDataElement* ItemByType[TAddrListDataType AType] = {read=FGetElementByType};
};
//---------------------------------------------------------------------------
extern PACKAGE void __fastcall ShowHouseHint(TcxHintStyleController *AHintCtrl, UnicodeString AText, int Left, int Top);
extern PACKAGE void __fastcall ShowDebug(UnicodeString AText);
extern PACKAGE adresCode __fastcall ParseAddrStr (UnicodeString AAddrStr, bool ATemplateMode = false);
extern PACKAGE UnicodeString __fastcall GetHomeFullStr(adresCode AAddrCode);
extern PACKAGE bool __fastcall GetHouseStruct(UnicodeString AHouseStr, UnicodeString &AHouse, UnicodeString &AVld, UnicodeString &AKorp, UnicodeString &ABuild);
extern PACKAGE bool __fastcall ParseHouseStruct(UnicodeString AHouseStr, UnicodeString &AHouse, UnicodeString &AVld, UnicodeString &AKorp, UnicodeString &ABuild);
extern PACKAGE UnicodeString __fastcall OldAddrCodeToString (adresCode adres_code);
//---------------------------------------------------------------------------
#endif
