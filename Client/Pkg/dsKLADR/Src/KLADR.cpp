//---------------------------------------------------------------------------

#include "KLADR.h"
#include <basepch.h>
#include <locale.h>

#pragma hdrstop

#pragma package(smart_init)


#include "KLADRConst.h"
#include <msgdef.h>

#define PERFTEST 0
#define TOPSTEP 25
#define MAX_SEARCH_COUNT 15
#define LEFTMARGIN 10
#define CTRLWIDTH 220

//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TKLADRFORM *)
{
    new TKLADRFORM(NULL);
}

//---------------------------------------------------------------------------

//###########################################################################
//#                                                                         #
//#                             TKLADRFORM                                  #
//#                                                                         #
//###########################################################################

//---------------------------------------------------------------------------
__fastcall TKLADRFORM::TKLADRFORM(TComponent* Owner) : TComponent(Owner)
{
    FAdresCodeStr = "";
    FSaveIndeks = "";
    FAdresShortCodeStr = "";
    FDB          = NULL;
    grayed       = NULL;
    normal       = NULL;
    content      = NULL;
    selection    = NULL;
    ctrls        = NULL;
    regions      = NULL;
    streets      = NULL;
    houses       = NULL;
    tlNodes      = NULL;
//    regionsTL    = NULL;
    Q            = NULL;
//    Q2           = NULL;
    TR           = NULL;
    reqColor     = clInfoBk;
    SQLTemplate  = "";
    adresTextStr = "";
    dataPrepared = 0;
    isFiltered   = 0;

    FHintShowing = false;
    FCanHideHint = false;
    FIsNFAddr    = false;

    FHintCtrl = new TcxHintStyleController(this);
    FHintCtrl->Global                          = false;
    FHintCtrl->HintHidePause                   = 10000;
    FHintCtrl->HintShortPause                  = 500;
    ((TcxHintStyle*)FHintCtrl->HintStyle)->BorderColor          = clNavy;
    ((TcxHintStyle*)FHintCtrl->HintStyle)->CallOutPosition      = cxbpNone;
    ((TcxHintStyle*)FHintCtrl->HintStyle)->CaptionFont->Charset = DEFAULT_CHARSET;
    ((TcxHintStyle*)FHintCtrl->HintStyle)->CaptionFont->Color   = clWindowText;
    ((TcxHintStyle*)FHintCtrl->HintStyle)->CaptionFont->Name    = "MS Sans Serif";
    ((TcxHintStyle*)FHintCtrl->HintStyle)->CaptionFont->Style   = (TFontStyles() << fsBold);
    ((TcxHintStyle*)FHintCtrl->HintStyle)->Font->Size           = 10;
    ((TcxHintStyle*)FHintCtrl->HintStyle)->Font->Name           = "Courier New";
    ((TcxHintStyle*)FHintCtrl->HintStyle)->IconType             = cxhiNone;
    ((TcxHintStyle*)FHintCtrl->HintStyle)->IconSize             = cxisSmall;
    ((TcxHintStyle*)FHintCtrl->HintStyle)->Rounded              = true;
    ((TcxHintStyle*)FHintCtrl->HintStyle)->RoundRadius          = 3;
    ((TcxHintStyle*)FHintCtrl->HintStyle)->Animate              = cxhaFadeIn;
    ((TcxHintStyle*)FHintCtrl->HintStyle)->AnimationDelay       = 500;
    ((TcxHintStyle*)FHintCtrl->HintStyle)->Standard             = false;

    searchFuncs[1] = &textToCodeReg;
    searchFuncs[2] = &textToCodeNP;
    searchFuncs[3] = &textToCodeStreet;
    searchFuncs[4] = &textToCodeNP_WC;
    searchFuncs[5] = &textToCodeStreet_WC;
    FFindedObjects.clear();

    // ��������� ������ ��������

    FStreetTypeObj["�����"] = 1;                    FStreetTypeSocrObj[" ��."] = 1;
    FStreetTypeObj["�������� �����"]   = 2;         FStreetTypeSocrObj[" �/�"] = 2;
    FStreetTypeObj["����������� ����"]   = 3;       FStreetTypeSocrObj[" �/�"] = 3;
    FStreetTypeObj["��������� �������� �����"] = 4; FStreetTypeSocrObj[" ���"] = 4;
    FStreetTypeDefIndex = 1;
    // ��������� ������ ����� ��� ��������
    FFlatTypeObj["��������"]  = 1; FFlatTypeSocrObj["��."]  = 1;
    FFlatTypeObj["����"]      = 2; FFlatTypeSocrObj["����"] = 2;
    FFlatTypeObj["���������"] = 3; FFlatTypeSocrObj["���."] = 3;
    FFlatTypeDefIndex = 1;

}

//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::prepareData ()
{
  if (dataPrepared) return;
  grayed    = new TcxStyle (NULL);
  normal    = new TcxStyle (NULL);
  content   = new TcxStyle (NULL);
  selection = new TcxStyle (NULL);

  grayed->Color        = clBtnFace;
  normal->Color        = clWhite;
  selection->Color     = clHighlight;
  selection->TextColor = clWhite;

  ctrls     = new formControls;
  regions   = new StringIntMap;
  streets   = new StringIntMap;
  houses    = new StringIntMap;
  tlNodes   = new nodesMap;
//    regionsTL = new tlByRegMap;
  socr      = new StrStrMap;
  CTTCache  = new CTTCacheMap;

  Q  = new TpFIBQuery       (this);
//  Q2 = new TpFIBQuery       (this);
  TR = new TpFIBTransaction (this);
  Q->Transaction  = TR;
//  Q2->Transaction = TR;

  dataPrepared = 1;
}
//---------------------------------------------------------------------------
__fastcall TKLADRFORM::~TKLADRFORM()
{
  if (ctrls)
   {
     deleteVisual();
     delete ctrls;
   }

  if (CTTCache)
   {
     FlushCTTCache();
     delete CTTCache;
   }

  if (regions)     delete regions;
  if (streets)     delete streets;
  if (houses)      delete houses;
  if (socr)        delete socr;
  if (tlNodes)     delete tlNodes;
  if (pnl)         delete pnl;
  if (FHintCtrl)   ((TcxHintStyle*)FHintCtrl->HintStyle)->Standard = true;
  if (grayed)      delete grayed;
  if (normal)      delete normal;
  if (content)     delete content;
  if (selection)   delete selection;
  if (Q)           delete Q;
//  if (Q2)          delete Q2;
  if (TR)          delete TR;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FlushCTTCache ()
{
  for (CTTCacheMap::iterator i = CTTCache->begin(); i != CTTCache->end(); i++)
   {
     if (i->second)
       delete i->second;
     i->second = NULL;
   }
  CTTCache->clear();
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FQPrepare(UnicodeString ASQL)
{
  Q->Close();
  if (!Q->Transaction->Active) Q->Transaction->StartTransaction();
  Q->SQL->Text = ASQL;
  Q->Prepare();
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FQExec(UnicodeString ASQL)
{
  Q->Close();
  if (!Q->Transaction->Active) Q->Transaction->StartTransaction();
  Q->SQL->Text = ASQL;
  Q->Prepare();
  Q->ExecQuery();
}
//---------------------------------------------------------------------------
TpFIBDatabase* __fastcall TKLADRFORM::getDB()
{
  return FDB;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::setDB(TpFIBDatabase* ADB)
{
  FDB = ADB;
}
//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::connectToDB()
{
  if (!FDB)
   throw EKLADRError("��������� �����: �� ������� ��");

  try
   {
      if (!FDB->Connected)
          FDB->Connected = 1;

      Q->Database         = FDB;
//      Q2->Database        = FDB;
      if (Q->Transaction->Active)
          Q->Transaction->Active = 0;
      Q->Transaction->DefaultDatabase = FDB;

      FQExec("Select Count (rdb$field_name) as RCOUNT from rdb$relation_fields where Upper(rdb$relation_name) = 'KLADR14' and Upper(rdb$field_name) = 'UNAME'");

      if (Q->FN("RCOUNT")->AsInteger == 0)
       {
         Q->Close  ();
         if (Q->Transaction->Active) Q->Transaction->Commit();
         throw EKLADRError("��������� �����: ���������� ��������� ������.");
       }
      return 1;
   }
  catch (...)
   {
      throw EKLADRError("��������� �����: �� ������� �������������� � ��");
   }
}
//---------------------------------------------------------------------------
TcxControl* __fastcall TKLADRFORM::GetCtrl(const UnicodeString ACtrlName)
{
  TcxControl *RC = NULL;
  try
   {
     formControls::iterator RFC = ctrls->find(ACtrlName);
     if (RFC != ctrls->end())
      RC = (TcxControl*)RFC->second;
     else
       throw EKLADRError("Control \""+ACtrlName+"\" not found.");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TcxLabel* __fastcall TKLADRFORM::GetLabCtrl(const UnicodeString ACtrlName)
{
  return (TcxLabel*)GetCtrl(ACtrlName);
}
//---------------------------------------------------------------------------
TcxMaskEdit* __fastcall TKLADRFORM::GetMaskEditCtrl(const UnicodeString ACtrlName)
{
  return (TcxMaskEdit*)GetCtrl(ACtrlName);
}
//---------------------------------------------------------------------------
TcxButtonEdit* __fastcall TKLADRFORM::GetButtonEditCtrl(const UnicodeString ACtrlName)
{
  return (TcxButtonEdit*)GetCtrl(ACtrlName);
}
//---------------------------------------------------------------------------
TcxListBox* __fastcall TKLADRFORM::GetListBoxCtrl(const UnicodeString ACtrlName)
{
  return (TcxListBox*)GetCtrl(ACtrlName);
}
//---------------------------------------------------------------------------
TcxComboBox* __fastcall TKLADRFORM::GetComboBoxCtrl(const UnicodeString ACtrlName)
{
  return (TcxComboBox*)GetCtrl(ACtrlName);
}
//---------------------------------------------------------------------------
TcxTreeList* __fastcall TKLADRFORM::GetTreeListCtrl(const UnicodeString ACtrlName)
{
  return (TcxTreeList*)GetCtrl(ACtrlName);
}
//---------------------------------------------------------------------------
TcxButton* __fastcall TKLADRFORM::GetBtnCtrl(const UnicodeString ACtrlName)
{
  return (TcxButton*)GetCtrl(ACtrlName);
}
//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::Execute (bool isTemplate, UnicodeString adr)
{
  bool RC = false;
  try
   {
     FHintCtrl->Global = true;
     templateMode = isTemplate;
     prepareData();
     if (!connectToDB())
         return 0;
     if (adr != "use_default" )
       origAdres = ParseAddrStr(adr);
     else if (!FAdresCodeStr.IsEmpty())
       origAdres = ParseAddrStr(FAdresCodeStr);
     else
       origAdres.Clear();

     FHouseStr = "";
     FFlatStr = "";

     if (!origAdres.RegID && !FRegionEnabled)
      throw EKLADRError("������ �������� ���������� �����:\n�� ����� ������ �� ���������.");
     else if (!origAdres.Town1ID && !origAdres.Town2ID && !origAdres.Town3ID && !FTownEnabled && FTownRequired)
      throw EKLADRError("������ �������� ���������� �����:\n�� ����� ���.����� �� ���������.");
     else if (!origAdres.StreetID && !FStreetEnabled && FStreetRequired)
      throw EKLADRError("������ �������� ���������� �����:\n�� ������ ����� �� ���������.");

     ((TcxHintStyle*)FHintCtrl->HintStyle)->Standard             = false;
     content->Color = FTownEnabled ? reqColor : clBtnFace;
     adres          = origAdres;
     firstIDChange  = 1;
     createVisual();
     fillRegions();
     fillTowns();
     fillStreetObjType(adres.StrObjType == sotStreet);
     FSetDefStreetType();
     fillStreets();
     fillDoma();
     fillFlats();

     updateControlsState();
     if (FIndexVisible && !origAdres.Index.IsEmpty())
       GetMaskEditCtrl("EditIndeks")->Text = origAdres.Index;

     firstIDChange = 0;
     RC = (form->ShowModal () == mrOk);
   }
  __finally
   {
     deleteVisual();
     FHintCtrl->Global = false;
     ((TcxHintStyle*)FHintCtrl->HintStyle)->Standard = true;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::createColumns (TcxTreeList* tl)
{
    TcxTreeListColumn* col;
    col                    = tl->CreateColumn();
    col->Name              = "nameCol";
    col->Caption->Text     = "��������� �����";
    col->Width             = tl->Width - 25;
    col->Options->Editing  = 0;
    col->Options->IncSearch  = true;
    col->Options->Sorting  = 1;
    col->SortOrder         = soAscending;
    col->Options->Sizing   = 0;
    col->Options->Moving   = 0;

    col          = tl->CreateColumn();
    col->Name    = "Town1ID";
    col->Visible = 0;

    col          = tl->CreateColumn();
    col->Name    = "Town2ID";
    col->Visible = 0;

    col          = tl->CreateColumn();
    col->Name    = "Town3ID";
    col->Visible = 0;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FSetCtrlProps (TControl *ADest, UnicodeString AName, UnicodeString ACaptText, int ALeft, int ATop, int AWidth, int AHeight, int &ATabOrder, TWinControl *AParent)
{
  (*ctrls)[AName] = (TControl*)ADest;
  ADest->Name = AName;
  if (ALeft != -1)
   ADest->Left = ALeft;
  if (ATop != -1)
  ADest->Top = ATop;
  if (AWidth != -1)
   ADest->Width = AWidth;
  if (AHeight != -1)
   ADest->Height = AHeight;
  if (AParent)
   ADest->Parent = AParent;
  if (ADest->ClassNameIs("TcxLabel"))
   {
     TcxLabel *label = (TcxLabel*)ADest;
     label->Caption                         = ACaptText;
     label->Style->LookAndFeel->Kind        = lfStandard;
     label->Style->LookAndFeel->NativeStyle = 1;
   }
  else if (ADest->ClassNameIs("TcxMaskEdit"))
   {
     TcxMaskEdit *medit = (TcxMaskEdit*)ADest;
     medit->Text                            = ACaptText;
     medit->TabOrder                        = ATabOrder; ATabOrder++;
     medit->Style->LookAndFeel->Kind        = lfStandard;
     medit->Style->LookAndFeel->NativeStyle = 1;
     medit->Properties->MaskKind            = emkRegExprEx;
     medit->OnKeyDown                       = FOnFormKeyDown;
   }
  else if (ADest->ClassNameIs("TcxComboBox"))
   {
     TcxComboBox *cb = (TcxComboBox*)ADest;
     cb->Text                            = ACaptText;
     cb->TabOrder                        = ATabOrder; ATabOrder++;
     cb->Style->LookAndFeel->Kind        = lfStandard;
     cb->Style->LookAndFeel->NativeStyle = 1;
     cb->Properties->DropDownListStyle   = lsEditFixedList;
     cb->Properties->ImmediatePost       = 1;
     cb->OnKeyDown                       = FOnCBKeyDown;
   }
  else if (ADest->ClassNameIs("TcxButtonEdit"))
   {
     TcxButtonEdit *be = (TcxButtonEdit*)ADest;
     be->TabOrder                        = ATabOrder; ATabOrder++;
     be->Style->LookAndFeel->Kind        = lfStandard;
     be->Style->LookAndFeel->NativeStyle = 1;
//     be->Text                            = ACaptText;
     be->OnKeyDown                       = FOnNFAddrBEKeyDown;
     be->Properties->OnButtonClick       = FOnNFAddrBtnClick;
     be->Properties->Buttons->Add();
     be->Properties->Buttons->Add();

     TcxEditButton *tmpBtn;
     UnicodeString FHint;
     bool FSetBmp;
     be->ShowHint = true;
     for (int i = 0; i < be->Properties->Buttons->Count; i++)
      {
        tmpBtn = be->Properties->Buttons->Items[i];
        tmpBtn->Kind = bkText;
        switch (i)
         {
           case 0:
           {
             be->Properties->ClickKey = 0;//TShortCut("Enter");
             break;
           }
           case 1:
           {
             break;
           }
           case 2:
           {
             be->Properties->ClearKey = TShortCut("Ctrl+Del");
             break;
           }
         }
        FSetBmp = false;
        FHint = "";
        switch (i)
         {
           case 0:
           {
             tmpBtn->Hint    = "����� � ������������ ������ �� ��������� [Enter]";
             break;
           }
           case 1:
           {
             tmpBtn->Hint    = "����� �� ��������� ������ [Ctrl+Enter]";
             break;
           }
           case 2:
           {
             tmpBtn->Hint    = "�������� [Ctrl+Del]";
             break;
           }
         }
        if (FOnGetButtonBitmap)
         {
           tmpBtn->Kind = bkGlyph;
           FSetBmp = FOnGetButtonBitmap(i, tmpBtn, FHint);
//           tmpBtn->Hint = FHint;
         }
        if (!FSetBmp)
         {
           switch (i)
            {
              case 0:
              {
                tmpBtn->Caption = "�����";
                break;
              }
              case 1:
              {
                tmpBtn->Caption = "����� +";
                break;
              }
              case 2:
              {
                tmpBtn->Caption = "X";
                break;
              }
            }
         }
      }
   }
/*  else if (ADest->ClassNameIs("TcxPopupEdit"))
   {
     TcxTextEdit *te = (TcxTextEdit*)ADest;
     te->TabOrder                           = ATabOrder; ATabOrder++;
     te->Style->LookAndFeel->Kind           = lfStandard;
     te->Style->LookAndFeel->NativeStyle    = 1;
//     te->Text                               = ACaptText;
   }
*/
  else if (ADest->ClassNameIs("TcxButton"))
   {
     TcxButton *btn = (TcxButton *)ADest;
     btn->Caption                  = ACaptText;
//     btn->TabStop                  = true;
     btn->TabOrder                 = ATabOrder; ATabOrder++;
     btn->LookAndFeel->Kind        = lfStandard;
     btn->LookAndFeel->NativeStyle = 1;
     btn->OnKeyDown                = FOnFormKeyDown;
   }
  else if (ADest->ClassNameIs("TcxListBox"))
   {
     TcxListBox *lb = (TcxListBox*)ADest;
     lb->Style->LookAndFeel->Kind           = lfStandard;
     lb->Style->LookAndFeel->NativeStyle    = 1;
   }
  else if (ADest->ClassNameIs("TcxTreeList"))
   {
     TcxTreeList *tl = (TcxTreeList*)ADest;
     tl->TabOrder                 = ATabOrder; ATabOrder++;
     tl->LookAndFeel->Kind        = lfStandard;
     tl->LookAndFeel->NativeStyle = 1;
     tl->OnKeyDown                = FOnFormKeyDown;
     tl->OptionsSelection->HideFocusRect = false;
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::createVisual()
{
  FNFAddrModify = false;
  int FTabOrder = 0;
  int curTop         = 10;
  TcxLabel* label    = NULL;
  TcxMaskEdit* medit = NULL;
//  TcxTextEdit* te    = NULL;
  TcxButtonEdit* be  = NULL;
  TcxListBox*  lb    = NULL;
  TcxComboBox* cb    = NULL;
  TcxButton* btn     = NULL;

  form              = new TForm (this);
  form->Caption     = "�������� �����";
  form->Position    = poScreenCenter;
  FSetCtrlProps (form, "MainKLADRForm", "", -1, -1, 500, 550, FTabOrder);

  form->OnShow      = onFormShow;
  form->KeyPreview  = false;
//  form->OnKeyDown   = FOnFormKeyDown;
//  form->KeyPreview  = true;
  form->OnClose     = onFormClose;

  form->BorderStyle = bsSingle;
  form->BorderIcons >> biMinimize;
  form->BorderIcons >> biMaximize;
  form->BorderIcons >> biSystemMenu;

  // ������ ������������������ ������    TcxControl

  label                                  = new TcxLabel (form);
  FSetCtrlProps (label, "LabelNFaddr", "������ ��� ����� (������, ��������� �����, �����):", LEFTMARGIN, curTop, 55, -1, FTabOrder, form);
  (*ctrls)["LabelNFaddr"]                = (TControl*)label;
  curTop += TOPSTEP;

  lb                                     = new TcxListBox (form);
  FSetCtrlProps (lb, "NFAddrLB", "", LEFTMARGIN, curTop, 100, -1, FTabOrder, form);
  lb->Visible                            = false;
  lb->ItemIndex                          = -1;
  lb->Items->Clear();

  lb->OnDblClick                         = NFAddrLBDblClick;
  lb->OnKeyDown                          = FOnNFAddrLBKeyDown;
  lb->OnExit                             = NFAddrLBExit;


  be                                     = new TcxButtonEdit (form);

  FSetCtrlProps (be, "NFAddrBE", "", LEFTMARGIN, curTop, form->Width - LEFTMARGIN * 2, 227, FTabOrder, form);

//  be->Hint                               = kcEditNFAddrHint;
//  be->ShowHint                           = FCanShowHint;
  be->Properties->OnChange               = FOnNFAddrChanged;
  be->Text                               = codeToText(adres.AsString(),"11111110");
  FNFAddrModify = false;
//  te->OnKeyDown                          = FOnEditNFAddrKeyDown;

//  btn                                    = new TcxButton (form);
//  FSetCtrlProps (btn, "NFAddrBtn", "�����", te->Left + te->Width + 5, curTop, 60, 21, FTabOrder, form);
//  btn->Hint                              = "���������� �����";
//  btn->ShowHint                          = FCanShowHint;
//
//  btn->OnClick                           = FOnNFAddrBtnClick;

  curTop += TOPSTEP+5;

  // �����������
  TBevel *sep                            = new TBevel (form);
  FSetCtrlProps (sep, "EditNFAddrSep", "", -1, curTop, form->Width+5, 3, FTabOrder, form);
  sep->Shape                             = bsTopLine;
  curTop += 3;

  // �������� ������
  if (FIndexVisible)
   {
     label                                  = new TcxLabel (form);
     FSetCtrlProps (label, "LabelIndeks", "�������� ������ :", LEFTMARGIN, curTop, -1, -1, FTabOrder, form);

     medit                                  = new TcxMaskEdit (form);
     FSetCtrlProps (medit, "EditIndeks", "", form->Width - 120 - LEFTMARGIN, curTop, 120, -1, FTabOrder, form);
     medit->Properties->EditMask            = kcIndeksEditMask;
     medit->Properties->Alignment->Horz     = taRightJustify;
     medit->Hint                            = "";

     medit->Properties->OnChange            = FOnIndeksChanged;

     curTop += TOPSTEP;
   }

  // ������
  label                               = new TcxLabel (form);
  FSetCtrlProps (label, "LabelRegion", "������ :", LEFTMARGIN, curTop, -1, -1, FTabOrder, form);

  cb                                  = new TcxComboBox (form);
  FSetCtrlProps (cb, "ComboRegion", "", form->Width - CTRLWIDTH - LEFTMARGIN, curTop, CTRLWIDTH, -1, FTabOrder, form);
  if (!templateMode)
   cb->Style->Color                = reqColor;
  if (!FRegionEnabled) cb->Enabled            = 0;

  cb->Properties->OnEditValueChanged  = FOnRegionChanged;

  curTop += TOPSTEP-2;

  // ���������� �����

/*  TcxPopupEdit *pe                  = new TcxPopupEdit(form);
  FSetCtrlProps (pe, "PunktPE", "", LEFTMARGIN, curTop, form->Width - LEFTMARGIN * 2, 227, FTabOrder, form);
  curTop += pe->Height+3;   */

  TcxTreeList *tl                  = new TcxTreeList(form);
  FSetCtrlProps (tl, "PunktTL", "", LEFTMARGIN, curTop, form->Width - LEFTMARGIN * 2, 227, FTabOrder, form);

/*  pe->Properties->PopupControl = tl;
  pe->Properties->IncrementalSearch = true;    */

  tl->OptionsView->CellEndEllipsis = 1;
  tl->OptionsBehavior->CellHints   = 1;
  tl->OptionsBehavior->ExpandOnIncSearch = true;
  tl->OptionsBehavior->IncSearch = true;
  if (FTownRequired && !templateMode)
   {
     tl->Styles->Content    = content;
     tl->Styles->Background = content;
   }
  if (!FTownEnabled)
   {
     tl->Enabled          = 0;
     tl->Styles->Inactive = selection;
   }
  createColumns (tl);

  tl->Visible = true;
  tl->OnExit                       = FOnTownExit;
  tl->OnFocusedNodeChanged         = FOnTownChanged;

  curTop += tl->Height+3;

  // �����
  if (FStreetVisible)
   {
      cb                                     = new TcxComboBox (form);
      FSetCtrlProps (cb, "ComboStreetType", "", LEFTMARGIN, curTop, 120, -1, FTabOrder, form);
      if (FHouseRequired && !templateMode)
          cb->Style->Color                   = reqColor;
      cb->Hint                               = "";
      cb->ItemIndex                          = -1;

      cb->OnEnter                            = FOnStreetTypeEnter;
      cb->Properties->OnEditValueChanged     = FOnStreetTypeChanged;

      cb                                     = new TcxComboBox (form);
      FSetCtrlProps (cb, "ComboStreet", "", label->Left + label->Width + 3, curTop, (tl->Left + tl->Width) - (label->Left + label->Width + 3), -1, FTabOrder, form);
      if (FStreetRequired && !templateMode)
          cb->Style->Color                   = reqColor;
      if (!FStreetEnabled ) cb->Enabled                = 0;
      cb->Hint                               = "";

      cb->Properties->OnEditValueChanged     = FOnStreetChanged;
      cb->Properties->OnChange               = FOnStreetChanged;

      curTop += TOPSTEP;
   }

  // ���
  if (FHouseVisible)
  {
      // ���
      label                                  = new TcxLabel (form);
      FSetCtrlProps (label, "LabelHome", "��� :", LEFTMARGIN, curTop, -1, -1, FTabOrder, form);

      medit                                  = new TcxMaskEdit (form);
      FSetCtrlProps (medit, "HouseMaskEdit", "", 150, curTop, 90, -1, FTabOrder, form);
      medit->Properties->EditMask            = kcHouseEditMask;
      medit->ShowHint                        = false;
      medit->Properties->Alignment->Horz     = taRightJustify;
      if (FHouseRequired && !templateMode)
          medit->Style->Color                = reqColor;

      medit->OnEnter                         = FHouseEnter;
      medit->OnExit                          = FHouseExit;
      medit->Properties->OnChange            = FOnHouseChanged;
      medit->Properties->OnValidate          = FOnHouseTextValidate;

      // ��������
      label                                  = new TcxLabel (form);
      FSetCtrlProps (label, "LabelVld", "�������� :", medit->Left+medit->Width+5, curTop, -1, -1, FTabOrder, form);

      medit                                  = new TcxMaskEdit (form);
      FSetCtrlProps (medit, "VldMaskEdit", "", label->Left+150, curTop, 90, -1, FTabOrder, form);
      medit->Properties->EditMask            = kcVldEditMask;
      medit->ShowHint                        = false;
      medit->Properties->Alignment->Horz     = taRightJustify;
      if (FHouseRequired && !templateMode)
          medit->Style->Color                = reqColor;

      medit->OnEnter                         = FHouseEnter;
      medit->OnExit                          = FHouseExit;
      medit->Properties->OnChange            = FOnVldChanged;
      medit->Properties->OnValidate          = FOnHouseTextValidate;

      curTop += TOPSTEP;

      // ������
      label                                  = new TcxLabel (form);
      FSetCtrlProps (label, "LabelKorp", "������ :", LEFTMARGIN, curTop, -1, -1, FTabOrder, form);

      medit                                  = new TcxMaskEdit (form);
      FSetCtrlProps (medit, "KorpMaskEdit", "", 150, curTop, 90, -1, FTabOrder, form);
      medit->Properties->EditMask            = kcKorpEditMask;
      medit->ShowHint                        = false;
      medit->Properties->Alignment->Horz     = taRightJustify;
      if (FHouseRequired && !templateMode)
          medit->Style->Color                = reqColor;

      medit->OnEnter                         = FHouseEnter;
      medit->OnExit                          = FHouseExit;
      medit->Properties->OnChange            = FOnKorpChanged;
      medit->Properties->OnValidate          = FOnHouseTextValidate;

      // ��������
      label                                  = new TcxLabel (form);
      FSetCtrlProps (label, "LabelBuild", "�������� :", medit->Left+medit->Width+5, curTop, -1, -1, FTabOrder, form);

      medit                                  = new TcxMaskEdit (form);
      FSetCtrlProps (medit, "BuildMaskEdit", "", label->Left+150, curTop, 90, -1, FTabOrder, form);
      medit->Properties->EditMask            = kcBuildEditMask;
      medit->ShowHint                        = false;
      medit->Properties->Alignment->Horz     = taRightJustify;
      if (FHouseRequired && !templateMode)
          medit->Style->Color                = reqColor;

      medit->OnEnter                         = FHouseEnter;
      medit->OnExit                          = FHouseExit;
      medit->Properties->OnChange            = FOnBuildChanged;
      medit->Properties->OnValidate          = FOnHouseTextValidate;

      curTop += TOPSTEP;
  }

  // ��������
  if (FFlatVisible)
  {
      cb                                     = new TcxComboBox (form);
      FSetCtrlProps (cb, "ComboFlatType", "", LEFTMARGIN, curTop, 90, -1, FTabOrder, form);
      if (FHouseRequired && !templateMode)
          cb->Style->Color                   = reqColor;
      for (StringIntMap::iterator i = FFlatTypeObj.begin(); i != FFlatTypeObj.end(); i++)
       {
         cb->Properties->Items->AddObject(i->first,(TObject*)i->second);
         if (i->second == FFlatTypeDefIndex)
          FFlatTypeDefaultIdx = cb->Properties->Items->Count-1;
       }
      cb->Hint                               = "";
      cb->ItemIndex                          = 0;

      cb->Properties->OnEditValueChanged     = FOnFlatTypeChanged;

      medit                                  = new TcxMaskEdit (form);
      FSetCtrlProps (medit, "FlatEdit", "", 150, curTop, 90, -1, FTabOrder, form);
      medit->Properties->EditMask            = kcFlatEditMask;
      medit->Properties->Alignment->Horz     = taRightJustify;
      medit->Hint                            = "";

      if (FFlatRequired && !templateMode)
          medit->Style->Color                = reqColor;

      medit->Properties->OnChange            = FOnFlatTextChanged;

      curTop += TOPSTEP;
  }

  curTop += 5;

  // ������ �������������
  btn                           = new TcxButton (form);
  FSetCtrlProps (btn, "okBtn", "���������", form->Width - LEFTMARGIN - btn->Width*2 - 5, curTop, -1, -1, FTabOrder, form);
  btn->OnClick                  = OkBtnClicked;

  btn                           = new TcxButton (form);
  FSetCtrlProps (btn, "cancelBtn", "������", form->Width - LEFTMARGIN - btn->Width, curTop, -1, -1, FTabOrder, form);
  btn->OnClick                  = CancelBtnClicked;

  form->ClientHeight = curTop + btn->Height + 5;
}

//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::deleteVisual()
{
  if (ctrls)
   {
     for (formControls::iterator i = ctrls->begin(); i != ctrls->end(); i++)
      {
        if (i->second && (i->first != "MainKLADRForm"))
         {
           delete i->second;
           i->second = NULL;
         }
      }
     for (formControls::iterator i = ctrls->begin(); i != ctrls->end(); i++)
      {
        if (i->second)
         delete i->second;
        i->second = NULL;
      }
     ctrls->clear();
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::onFormShow (TObject* Sender)
{
  FSetActivControl();
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FSetActivControl ()
{
  GetCtrl("NFAddrBE")->SetFocus();
  int FPos = GetButtonEditCtrl("NFAddrBE")->Text.Length();
  if (FPos)
   GetButtonEditCtrl("NFAddrBE")->SelStart = FPos;

/*  if (!adres.RegID && FRegionEnabled)
   GetCtrl("NFAddrBE")->SetFocus();
  else if (!adres.Town1ID && !adres.Town2ID && !adres.Town3ID && FTownEnabled && !adres.StreetID && GetCtrl("PunktTL")->Enabled)
   GetCtrl("PunktTL")->SetFocus();
  else if ((adres.StrObjType == sotNone) && FStreetVisible && GetCtrl("ComboStreetType")->Enabled)
   GetCtrl("ComboStreetType")->SetFocus();
  else if (!adres.StreetID && FStreetVisible && GetCtrl("ComboStreet")->Enabled)
   GetCtrl("ComboStreet")->SetFocus();
  else if (adres.House.IsEmpty() && FHouseVisible && GetCtrl("HouseMaskEdit")->Enabled)
   GetCtrl("HouseMaskEdit")->SetFocus();
  else if (adres.Vld.IsEmpty() && FHouseVisible && GetCtrl("VldMaskEdit")->Enabled)
   GetCtrl("VldMaskEdit")->SetFocus();
  else if (adres.Korp.IsEmpty() && FHouseVisible && GetCtrl("KorpMaskEdit")->Enabled)
   GetCtrl("KorpMaskEdit")->SetFocus();
  else if (adres.Build.IsEmpty() && FHouseVisible && GetCtrl("BuildMaskEdit")->Enabled)
   GetCtrl("BuildMaskEdit")->SetFocus();

  else if ((adres.FlatObjType == fotNone) && FFlatVisible && GetCtrl("ComboFlatType")->Enabled)
   GetCtrl("ComboFlatType")->SetFocus();
  else if (adres.Flat.IsEmpty() && FFlatVisible && GetCtrl("FlatEdit")->Enabled)
   GetCtrl("FlatEdit")->SetFocus();  */
}

//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::updateControlsState()
{
  // �������� - ��������� ��������
  GetCtrl("PunktTL")->Enabled = adres.RegID && FTownEnabled;

  bool FStrEn = (((adres.Town1ID+adres.Town2ID+adres.Town3ID) && !((adres.RegID == MSC_ID)||(adres.RegID == SPB_ID)))||
                ((adres.RegID == MSC_ID)||(adres.RegID == SPB_ID))) &&
                FStreetVisible && FStreetEnabled &&
                !(adres.Town1ID  && !(adres.Town2ID+adres.Town3ID));
  if (FStreetVisible)
   {
     GetCtrl("ComboStreetType")->Enabled = FStrEn;
     GetCtrl("ComboStreet")->Enabled     = FStrEn && !((adres.StrObjType == sotNone) && adres.NoStreets);
   }

  bool FHouseEn = (adres.StrObjType == sotStreet) && adres.StreetID;
  if (FHouseVisible)
   {
     GetCtrl("HouseMaskEdit")->Enabled  = FHouseEn;
     GetCtrl("VldMaskEdit")->Enabled    = FHouseEn;
     GetCtrl("KorpMaskEdit")->Enabled   = FHouseEn;
     GetCtrl("BuildMaskEdit")->Enabled  = FHouseEn;
   }

  bool FFlatEn = (adres.House+adres.Vld+adres.Korp+adres.Build).Length() && FFlatVisible;
  if (FFlatVisible)
   {
     GetCtrl("ComboFlatType")->Enabled = FFlatEn;
     GetCtrl("FlatEdit")->Enabled      = FFlatEn;
   }

  // ����� ������� ���� �����?
/*  if (!templateMode &&
       (
         !adres.RegID ||
         (FTownRequired && !adres.Town1ID && !adres.Town2ID && !adres.Town3ID && adres.RegID != MSC_ID && adres.RegID != SPB_ID) ||
         (FStreetRequired && !adres.StreetID) ||
         (FHouseRequired && (!adres.HouseFull.IsEmpty())) ||
         (FFlatRequired && adres.Flat.IsEmpty())
      )
    )
   {
      GetCtrl("okBtn")->Enabled = 0;
   }
  else*/
  GetCtrl("okBtn")->Enabled = 1;
  GetButtonEditCtrl("NFAddrBE")->Text = codeToText(adres.AsString(),"11111110");
  FNFAddrModify = false;
}

//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::fillSocrs ()
{
  try
   {
//     FQPrepare();
     FQExec("SELECT DISTINCT SHORTNAME, FULLNAME FROM SOCRBASE");

     while (!Q->Eof)
      {
         (*socr)[Q->FN("SHORTNAME")->AsString] = Q->FN("FULLNAME")->AsString;
         Q->Next();
      }

     Q->Close  ();
     if (Q->Transaction->Active) Q->Transaction->Commit();
   }
  catch (Exception &e)
   {
     UnicodeString message = "������ ��� ���������� ������ ����������.\n";
     message           += "�������:\n" + e.Message;
     throw EKLADRError(message.c_str());
   }
  catch (...)
   {
     throw EKLADRError("������ ��� ���������� ������ ����������");
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::queryToTreeList (TcxTreeList* TL, TpFIBQuery* Q)
{
    TcxTreeListNode *node;
    int level4id, level3id, level2id, level1id;

//    TL->BeginUpdate();
    /*
    if (templateMode)
    {
        node = TL->Add();
        node->Values[0] = TEMPLATE_EMPTY_TEXT;
        node->Values[1] = 0;
        node->Values[2] = 0;
        node->Values[3] = 0;
    }
    */

    while (!Q->Eof)
    {
        level4id = Q->FN("LEVEL4ID")->AsInteger;
        level3id = Q->FN("LEVEL3ID")->AsInteger;
        level2id = Q->FN("LEVEL2ID")->AsInteger;
        level1id = Q->FN("LEVEL1ID")->AsInteger;
        node = NULL;
        if (level4id)
        {
            // ��� ������ 4 ������
            if (level3id)
            {
                // ��� ������ ����������� ������ �������� ������
                if ((*tlNodes)[IntToStr(level2id)+"-"+IntToStr(level3id)])
                 node = (*tlNodes)[IntToStr(level2id)+"-"+IntToStr(level3id)]->AddChild();
            }
            else if (level2id)
            {
                // ��� ������ ����������� ������ ������� ������
                if ((*tlNodes)[IntToStr(level2id)])
                 node = (*tlNodes)[IntToStr(level2id)]->AddChild();
            }
            else
            {
                // ��� ������ ������ �� �����������
                // ��-�������� �� ������ ���� ����� �������=)
                node = TL->Add();
            }
            (*tlNodes)[IntToStr(level2id)+"-"+IntToStr(level3id)+"-"+IntToStr(level4id)] = node;
        }
        else if (level3id)
        {
            // ��� ������ 3 ������
            if (level2id)
             { // � ���� ������ ���� ������ ������� ������
               if ((*tlNodes)[IntToStr(level2id)])
                node = (*tlNodes)[IntToStr(level2id)]->AddChild();
             }
            else  // ��� ��������������� ������
             node = TL->Add();
            (*tlNodes)[IntToStr(level2id)+"-"+IntToStr(level3id)] = node;

        }
        else if (level2id)
        {
            // ��� ������ 2 ������
            node = TL->Add();
            (*tlNodes)[IntToStr(level2id)] = node;
        }
        else if (level1id == MSC_ID || level1id == SPB_ID)
        {
            // ������ ������ ��� ��� � ������
            node = TL->Add();
            level2id = 0;
            level3id = 0;
            level4id = 0;
            (*tlNodes)[0]   = node;
        }
        if (node)
         {
            node->Values[0] = FObjectFullName(Q->FN("NAME")->AsString,Q->FN("OBJECTTYPE")->AsString);
            node->Values[1] = level2id;
            node->Values[2] = level3id;
            node->Values[3] = level4id;
         }
        Q->Next();
    }
//    TL->EndUpdate();
}
//---------------------------------------------------------------------------
int __fastcall TKLADRFORM::FGetObjCode(StringIntMap *AMap, UnicodeString AVal)
{
  StringIntMap::iterator i = AMap->find(AVal);
  return (i == AMap->end()) ? 0 : i->second;
}
//---------------------------------------------------------------------------
/*
bool __fastcall TKLADRFORM::regionExists (UnicodeString rName)
{
    StringIntMap::iterator i = regions->find (rName);
    return i == regions->end() ? 0 : 1;
}
*/
//---------------------------------------------------------------------------
/*
bool __fastcall TKLADRFORM::regionTLExists (int regionID)
{
    tlByRegMap::iterator i = regionsTL->find (regionID);
    return i == regionsTL->end() ? 0 : 1;
}
*/
//---------------------------------------------------------------------------

bool __fastcall TKLADRFORM::nasPunktExists (UnicodeString npID)
{
    nodesMap::iterator i = tlNodes->find (npID);
    return i == tlNodes->end() ? 0 : 1;
}

//---------------------------------------------------------------------------
/*
bool __fastcall TKLADRFORM::streetExists (UnicodeString sName)
{
    StringIntMap::iterator i = streets->find (sName);
    return i == streets->end() ? 0 : 1;
}
*/
//---------------------------------------------------------------------------

bool __fastcall TKLADRFORM::domExists (UnicodeString dom_number)
{
    StringIntMap::iterator result = houses->find (dom_number);
    return result == houses->end () ? 0 : 1;
}

//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::CacheEntryExists (UnicodeString code)
{
    CTTCacheMap::iterator i = CTTCache->find (code);
    return i == CTTCache->end () ? 0 : 1;
}

//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::ClearCacheEntry(UnicodeString code)
{
    CTTCacheMap::iterator i = CTTCache->find (code);
    if (i != CTTCache->end ())
     {
       if (i->second)
        delete i->second;
       CTTCache->erase(code);
     }
}

//---------------------------------------------------------------------------
UnicodeString __fastcall TKLADRFORM::FObjectFullName(UnicodeString AName, UnicodeString AObjType)
{
  return AName+" ("+AObjType+")";
}
//---------------------------------------------------------------------------
unsigned int __fastcall TKLADRFORM::getTime ()
{
    unsigned short hours;
    unsigned short minutes;
    unsigned short seconds;
    unsigned short mseconds;
    Now ().DecodeTime(&hours, &minutes, &seconds, &mseconds);
    return mseconds + seconds * 1000 + minutes * 60 * 1000 + hours * 60 * 60 * 1000;
}

//---------------------------------------------------------------------------
int __fastcall TKLADRFORM::FGetLastLevelIdx(int ALvl1, int ALvl2, int ALvl3, int ALvl4)
{
  if (ALvl4) return 3;
  else if (ALvl3) return 2;
  else if (ALvl2) return 1;
  else if (ALvl1) return 0;
  else            return -1;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TKLADRFORM::PrepareStreetObjectVal(UnicodeString AVal)
{
  UnicodeString RC = AVal;
  try
   {
     for (int i = 1; i <= RC.Length(); i++)
      {
        if (RC[i] == '0') RC[i] = ' ';
        else i = RC.Length()+1;
      }
   }
  __finally
   {
   }
  return RC.Trim();  
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TKLADRFORM::codeToText(UnicodeString code, UnicodeString params, bool ANotNFAddr, bool AReplaceCach)
{
/*
    // ��������� ������� ����������� ������ � ���������
    // ������ �������� - ������� ������ ������ (� ������� �����)
    // ������ �������� - ��������� �� ��, ����� ������ ������������
    // ��� ����������� ���������� �����������. ������ ����� ������ ����
    // "�������", ��� � ��� 0 ��� 1, ��� ������������� ���������
    // (��� �� ����������) ���������������� ������ ������ � �������� �����.


// �� ��� ��� ��� ��, ���
//�� - ��� �������� ���������� ��������� (�������), ���� �������� ������������ � ���������� 2 � �������� �������������� ������� ���������� ��������� (�����);
//l2 ��� - ��� ������;
//l3 ��� - ��� ������;
//l4 ��� - ��� ����������� ������,
//STATUS
//0 - ������ �� �������� ������� ���������������-���������������� �����������;
//1 - ������ �������� ������� ������;
//2 - ������ �������� ������� (��������) �������;
//3 - ������ �������� ������������ � ������� ������ � ������� �������;
//4 - ����������� �����, �.�. �����, � ������� ��������� ����� �������
//    (������ ��� �������� 2-�� ������).

//���� "������ �������" ������������ ��� ����������� ������������ ��������� ������
//� �������������� ���� ������ �����:
//���� - "1",         �� � ������ ����������� ������ � ���������� ����� (����� �� �����������);
//���� - "2" ��� "3", �� � ������ ����������� ������ ����� ������� (������ � ����� �� �����������).

*/
  CTTCacheRecord* temp = NULL;
  bool CanDeleteTmp = true;
  UnicodeString RC = "";
  try
   {
     // ��������� ������� ����������� ������ � ���������
     // ������ �������� - ������� ������ ������ (� ������� �����)
     // ������ �������� - ��������� �� ��, ����� ������ ������������
     // ��� ����������� ���������� �����������. ������ ����� ������ ����
     // "�������", ��� � ��� 0 ��� 1, ��� ������������� ���������
     // (��� �� ����������) ���������������� ������ ������ � �������� �����.

     int FLvlIdx;
     if (!code.IsEmpty())
      {
        if (CTTCache && useCTTCache && CacheEntryExists (code) && !ANotNFAddr && !AReplaceCach)
         { // ������ ���� � ����
           RC = (*CTTCache)[code]->GetText(params);
         }
        else
         {
           prepareData();
           if (connectToDB())
            {
              try
               {
                 if (socr->empty()) fillSocrs();
                 temp = new CTTCacheRecord;
                 adresCode KLADRcode = ParseAddrStr(code);
                 if (ANotNFAddr) KLADRcode.State = 1;
                 // 2 - 3 - 3 - 3 - 4 - 4 - 4 - 4
                 if (KLADRcode.State || (KLADRcode.RegID+KLADRcode.Town1ID+KLADRcode.Town2ID+KLADRcode.Town3ID+KLADRcode.StreetID))
                  { // ����� ������������
                    UnicodeString query  = "";

                    query = "Select STATUS FROM KLADR14 WHERE ";
                    query += " LEVEL1ID = " + IntToStr (KLADRcode.RegID);
                    query += " AND LEVEL2ID = " + IntToStr (KLADRcode.Town1ID);
                    query += " AND LEVEL3ID = " + IntToStr (KLADRcode.Town2ID);
                    query += " AND LEVEL4ID = " + IntToStr (KLADRcode.Town3ID);
                    FQExec(query);

                    if (Q->RecordCount)
                     temp->Status = Q->FN("STATUS")->AsInteger;
                    Q->Close();
                    query = "";
                    // ������ � 1 �� 4 �������

                    for (int level = 1; level <= 4; level++)
                     {
                       if (!query.IsEmpty()) query += " OR ";

                       query += " (";
                       query += " LEVEL1ID = " + IntToStr (level ? KLADRcode.RegID : 0);
                       query += " AND LEVEL2ID = " + IntToStr (level >= 2 ? KLADRcode.Town1ID : 0);
                       query += " AND LEVEL3ID = " + IntToStr (level >= 3 ? KLADRcode.Town2ID : 0);
                       query += " AND LEVEL4ID = " + IntToStr (level >= 4 ? KLADRcode.Town3ID : 0);
                       query += ") ";
                     }
                    query = "SELECT LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID, NAME, STATUS, OBJECTTYPE FROM KLADR14 WHERE (" + query + ") ORDER BY LEVEL4ID, LEVEL3ID, LEVEL2ID, LEVEL1ID";
                    FQExec(query);

                    int lastL1id, lastL2id, lastL3id, lastL4id;

                    if (Q->RecordCount)
                     {
                       lastL1id = Q->FN("LEVEL1ID")->AsInteger;
                       lastL2id = Q->FN("LEVEL2ID")->AsInteger;
                       lastL3id = Q->FN("LEVEL3ID")->AsInteger;
                       lastL4id = Q->FN("LEVEL4ID")->AsInteger;
                       FLvlIdx = FGetLastLevelIdx(lastL1id, lastL2id, lastL3id, lastL4id);
                       if (FLvlIdx != -1)
                        temp->Values[FLvlIdx] = getFullName (Q->FN("NAME")->AsString, Q->FN("OBJECTTYPE")->AsString).Trim();
                       Q->Next();
                     }
                    UnicodeString result = "";
                    while (!Q->Eof)
                     {
                       // ��������� �� "��������". �.�. ����� ������ ������ ����� �������������
                       // ��� ��� ����� ������������. ����������� �������� �������� � ������
                       if (lastL1id == Q->FN("LEVEL1ID")->AsInteger &&
                            lastL2id == Q->FN("LEVEL2ID")->AsInteger &&
                            lastL3id == Q->FN("LEVEL3ID")->AsInteger &&
                            lastL4id == Q->FN("LEVEL4ID")->AsInteger)
                        {
                          if (!showOldNames)
                           {
                              Q->Next();
                              continue;
                           }

                          result = " (";
                          result += getFullName (Q->FN("NAME")->AsString, Q->FN("OBJECTTYPE")->AsString);
                          Q->Next();

                          while (!Q->Eof &&
                                  lastL1id == Q->FN("LEVEL1ID")->AsInteger &&
                                  lastL2id == Q->FN("LEVEL2ID")->AsInteger &&
                                  lastL3id == Q->FN("LEVEL3ID")->AsInteger &&
                                  lastL4id == Q->FN("LEVEL4ID")->AsInteger)
                           {
                             result += ", ";
                             result += getFullName (Q->FN("NAME")->AsString, Q->FN("OBJECTTYPE")->AsString);
                             Q->Next();
                           }
                          result += ")";

                          FLvlIdx = FGetLastLevelIdx(lastL1id, lastL2id, lastL3id, lastL4id);
                          if (FLvlIdx != -1)
                           temp->Values[FLvlIdx] = (temp->Values[FLvlIdx]+result).Trim();
                        }
                       else
                        {
                          lastL1id = Q->FN("LEVEL1ID")->AsInteger;
                          lastL2id = Q->FN("LEVEL2ID")->AsInteger;
                          lastL3id = Q->FN("LEVEL3ID")->AsInteger;
                          lastL4id = Q->FN("LEVEL4ID")->AsInteger;

                          FLvlIdx = FGetLastLevelIdx(lastL1id, lastL2id, lastL3id, lastL4id);
                          if (FLvlIdx != -1)
                           temp->Values[FLvlIdx] = getFullName (Q->FN("NAME")->AsString, Q->FN("OBJECTTYPE")->AsString).Trim();

                          Q->Next();
                        }
                     }
                    Q->Close  ();
                    // ������ ������ ������, ����� / ������
                    if (KLADRcode.StrObjType == sotStreet)
                     {
                       if (KLADRcode.StreetID)
                        {
                          FQPrepare("SELECT NAME, OBJECTTYPE FROM KLADR5 WHERE \
                                          LEVEL1ID = :pL1ID AND LEVEL2ID = :pL2ID AND LEVEL3ID = :pL3ID \
                                          AND LEVEL4ID = :pL4ID AND LEVEL5ID = :pL5ID");
                          Q->ParamByName("pL1ID")->AsInteger = KLADRcode.RegID;
                          Q->ParamByName("pL2ID")->AsInteger = KLADRcode.Town1ID;
                          Q->ParamByName("pL3ID")->AsInteger = KLADRcode.Town2ID;
                          Q->ParamByName("pL4ID")->AsInteger = KLADRcode.Town3ID;
                          Q->ParamByName("pL5ID")->AsInteger = KLADRcode.StreetID;
                          Q->ExecQuery();
                          if (Q->RecordCount)
                           temp->Values[4] = getFullName (Q->FN("NAME")->AsString, Q->FN("OBJECTTYPE")->AsString).Trim();
                          Q->Close  ();
                        }
                       if (Q->Transaction->Active) Q->Transaction->Commit();
                       // ������ ������� ������ (���)
                       if ((KLADRcode.House+KLADRcode.Vld+KLADRcode.Korp+KLADRcode.Build).Length())
                         temp->Values[5] = GetHomeFullStr(KLADRcode);
                       // ������ �������� ������ (��������)
                       if ((KLADRcode.FlatObjType != fotNone) && !KLADRcode.Flat.IsEmpty())
                        temp->Values[6] = getFlatTypeText(KLADRcode.FlatObjType)+" "+KLADRcode.Flat;
                     }
                    else if (KLADRcode.StrObjType != sotNone)
                     { // �/�, �/� ...
                       temp->Values[4] = getStreetTypeText(KLADRcode.StrObjType)+" "+PrepareStreetObjectVal(KLADRcode.StreetObjectVal);
                     }
                    // ������ �������� ������ (������)
                    temp->Values[7] = getIndeks (KLADRcode).Trim();

                    RC = temp->GetText(params);
                    if (CTTCache && useCTTCache)
                     {
                        (*CTTCache)[code] = temp;
                        CanDeleteTmp = false;
                     }
                  }
                 else
                  { // ����� �� ������������
                    if (KLADRcode.NFAddrID)
                     {
                       FQExec("SELECT NAME FROM KLADRNF where uid="+IntToStr(KLADRcode.NFAddrID));
                       if (Q->RecordCount)
                        {
                          temp->Values[0] = Q->FN("NAME")->AsString;
                          Q->Close();
                        }
                       RC = temp->GetText(params);
                       if (CTTCache && useCTTCache)
                        {
                           (*CTTCache)[code] = temp;
                           CanDeleteTmp = false;
                        }
                     }
                  }
               }
              __finally
               {
                 if (Q->Transaction->Active) Q->Transaction->Commit();
               }
            }
         }
      }
   }
  __finally
   {
     if (CanDeleteTmp && temp)
       delete temp;
   }
  return RC;
}

//---------------------------------------------------------------------------
UnicodeString __fastcall TKLADRFORM::codeToTextEx (UnicodeString code)
{
  CTTCacheRecord* temp = NULL;
  bool CanDeleteTmp = true;
  UnicodeString RC = "";
  try
   {
     // ��������� ������� ����������� ������ � ���������
     // ������ �������� - ������� ������ ������ (� ������� �����)
     // ������ �������� - ��������� �� ��, ����� ������ ������������
     // ��� ����������� ���������� �����������. ������ ����� ������ ����
     // "�������", ��� � ��� 0 ��� 1, ��� ������������� ���������
     // (��� �� ����������) ���������������� ������ ������ � �������� �����.

     if (code.IsEmpty())
         return "";

     if (CTTCache && useCTTCache && CacheEntryExists (code))
         return (*CTTCache)[code]->GetText("10000000");

     prepareData();

     if (!connectToDB())
         return "";

     if (socr->empty())
         fillSocrs();

     temp = new CTTCacheRecord;
     adresCode KLADRcode = ParseAddrStr(code);

     UnicodeString query  = "";

     query = "SELECT NAME, OBJECTTYPE FROM KLADR14 WHERE ";
     query += " LEVEL1ID = " + IntToStr (KLADRcode.RegID);
     query += " AND LEVEL2ID = " + IntToStr (KLADRcode.Town1ID);
     query += " AND LEVEL3ID = " + IntToStr (KLADRcode.Town2ID);
     query += " AND LEVEL4ID = " + IntToStr (KLADRcode.Town3ID);
     FQExec(query);

     if (Q->RecordCount)
     {
       temp->Values[0] = getFullName (Q->FN("NAME")->AsString, Q->FN("OBJECTTYPE")->AsString).Trim();
       temp->Status = 4;
     }
     Q->Close  ();
     if (Q->Transaction->Active) Q->Transaction->Commit();
     RC = temp->GetText("10000000");
     if (CTTCache && useCTTCache)
      {
         (*CTTCache)[code] = temp;
         CanDeleteTmp = false;
      }
   }
  __finally
   {
     if (CanDeleteTmp && temp)
       delete temp;
   }
  return RC;
}

//---------------------------------------------------------------------------
UnicodeString __fastcall TKLADRFORM::getFullName (UnicodeString name, UnicodeString object)
{
  UnicodeString result;
  if ((object == "����" && name.SubString(name.Length()-2,2) != "��") ||
      object == "�" || object == "���" || object == "��" || object == "�/�" ||
      object == "���" || object == "���" || object == "�����" ||
      (object == "�����" && name.SubString(name.Length()-2,2) != "��") ||
      (object == "�����" && name.SubString(name.Length()-2,2) != "��") ||
      (object == "�������" && name.SubString(name.Length()-2,2) != "��") ||
      object == "�" || object == "��" || object == "�/�_�����" ||
      object == "�/�_������" || object == "�/�_��" || object == "�/�_�����" ||
      object == "�/�_����" || object == "�/�_���" || object == "�/�_��" ||
      object == "��" || object == "������" || object == "�������" ||
      object == "��-�" || object == "������" || object == "��" ||
      object == "�" ||
      (object == "���" && name.SubString(name.Length()-2,2) != "��") ||
      (object == "���" && name.SubString(name.Length()-2,2) != "��") ||
      object == "��" || object == "������" || object == "�" ||
      object == "�/�" || object == "�/�" || object == "�/��" ||
      object == "������" || object == "�������" || object == "��������" ||
      object == "���" || object == "" || object == "�" || object == "��" ||
      object == "���" || object == "��" || object == "���" ||
      object == "�" || object == "��" || object == "" ||
      (object == "�" && name.SubString(name.Length()-2,2) != "��"))
   {
     if (expandSocrs) result = (*socr)[object] + " " + name;
     else             result = object + " " + name;
   }
  else
   {
     if (expandSocrs) result = name + " " + (*socr)[object];
     else             result = name + " " + object;
   }
  return result;
}

//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::cascadeIDClear(TCascadeClearType ALevel)
{
  switch (ALevel)
   {
     case cctAll:
      {
        adres.RegID  = 0;
        GetComboBoxCtrl("ComboRegion")->ItemIndex = -1;
        GetComboBoxCtrl("ComboRegion")->Text = "";
      }
     case cctReg:
      {
        adres.Town1ID  = 0;
        adres.Town2ID  = 0;
        adres.Town3ID  = 0;
        adres.State = false;
//        GetTreeListCtrl("PunktTL")->No   GetComboBoxCtrl()->ItemIndex = -1;
      }
     case cctTown:
      {
        adres.StrObjType = sotNone;
        adres.StreetObjectVal = "";
        adres.StreetID = 0;
        adres.NoStreets = false;
        adres.State = false;
        if (FStreetVisible)
         {
           GetComboBoxCtrl("ComboStreetType")->ItemIndex = -1;
           GetComboBoxCtrl("ComboStreetType")->Text      = "";
           GetComboBoxCtrl("ComboStreet")->Text          = "";
         }
      }
     case cctStreet:
      {
        adres.House           = "";
        adres.Vld             = "";
        adres.Korp            = "";
        adres.Build           = "";
        adres.HouseFull       = "";
        if (FHouseVisible)
         {
           GetMaskEditCtrl("HouseMaskEdit")->Text = "";
           GetMaskEditCtrl("VldMaskEdit")->Text   = "";
           GetMaskEditCtrl("KorpMaskEdit")->Text  = "";
           GetMaskEditCtrl("BuildMaskEdit")->Text = "";
         }

        adres.Index = FCalcIndeks(adres);
        if (FIndexVisible)
         GetMaskEditCtrl("EditIndeks")->Text = adres.Index;
      }
     case cctHouse:
      {
        adres.FlatObjType     = fotNone;
        adres.Flat            = "";
        if (FFlatVisible)
         {
           GetComboBoxCtrl("ComboFlatType")->ItemIndex = -1;
           GetComboBoxCtrl("ComboFlatType")->Text      = "";
           GetMaskEditCtrl("FlatEdit")->Text           = "";
         }
      }
     break; 
     default: adres.Clear();
   }
}

//***************************************************************************
//                                                                          *
//                             ������                                       *
//                                                                          *
//***************************************************************************

//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnNFAddrChanged(TObject *Sender)
{
  FNFAddrModify = true;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::fillRegions()
{
  // ��������� ������ ��������
  try
   {
     try
      {
         FQExec("SELECT LEVEL1ID, NAME, OBJECTTYPE FROM KLADR14 \
                         WHERE LEVEL2ID = 0 AND LEVEL3ID = 0 AND LEVEL4ID = 0 \
                         ORDER BY NAME");

         int tempIndex;
         int focusedStringIndex = -1;
         UnicodeString regionName;

         TcxComboBox* cb = GetComboBoxCtrl("ComboRegion");
         cb->Properties->Items->Clear();

         regions->clear();
         cb->Properties->Items->Clear();

         if (templateMode)
          {
             cb->Properties->Items->Add (TEMPLATE_EMPTY_TEXT);
             (*regions)[regionName] = 0; //!!! ? � ���� ��
          }

         for (int i = 0; i < Q->RecordCount; i++)
          {
             regionName = FObjectFullName(Q->FN("NAME")->AsString,Q->FN("OBJECTTYPE")->AsString);
             (*regions)[regionName] = Q->FN("LEVEL1ID")->AsInteger;
             tempIndex = cb->Properties->Items->Add (regionName);

             if (Q->FN("LEVEL1ID")->AsInteger == adres.RegID)
                 focusedStringIndex = tempIndex;

             Q->Next();
          }

         Q->Close  ();
         if (Q->Transaction->Active) Q->Transaction->Commit();

         if (focusedStringIndex != -1)
             cb->ItemIndex = focusedStringIndex;
         else
             cb->ItemIndex = -1;

      }
     catch (Exception &e)
      {
         UnicodeString message = "������ ��� ���������� ������ ��������.\n";
         message           += "�������:\n" + e.Message;
         throw EKLADRError(message.c_str());
      }
     catch (...)
      {
         throw EKLADRError("������ ��� ���������� ������ ��������");
      }
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnRegionChanged(TObject *Sender)
{
  if (firstIDChange)   return;

  UnicodeString newName = ((TcxComboBox*)Sender)->EditValue;
  adres.RegID = FGetObjCode(regions, newName);
//  if (!firstIDChange)
  cascadeIDClear(cctReg);
  fillTowns();
  updateControlsState();
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::fillTowns()
{
  if (adres.RegID == 0)
   {
     GetTreeListCtrl("PunktTL")->Clear();
     GetTreeListCtrl("PunktTL")->Styles->Background = grayed;
     adres.Town1ID = 0;
     adres.Town2ID = 0;
     adres.Town3ID = 0;
     adres.NoStreets = false;
//!!!*     fillStreets();
   }
  else
   {
     GetTreeListCtrl("PunktTL")->Enabled            = FTownEnabled;
     GetTreeListCtrl("PunktTL")->Styles->Background = FTownEnabled ? normal : grayed;
     TcxTreeList* targetTL = GetTreeListCtrl("PunktTL");
     targetTL->BeginUpdate();
     targetTL->Clear();//!!!-
     tlNodes->clear        ();
     FQPrepare("SELECT LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID, NAME, OBJECTTYPE \
                     FROM KLADR14 WHERE LEVEL1ID = :pLEVEL1ID \
                     ORDER BY LEVEL4ID, LEVEL3ID");
     Q->ParamByName ("pLEVEL1ID")->AsInteger = adres.RegID;
     Q->ExecQuery();
     if (templateMode)
      {
        TcxTreeListNode* node = targetTL->AddFirst(targetTL->Root);
        node->Values[0] = TEMPLATE_EMPTY_TEXT;
        node->Values[1] = 0;
        node->Values[2] = 0;
        node->Values[3] = 0;
      }

     queryToTreeList (targetTL, Q);

     Q->Close  ();
     if (Q->Transaction->Active) Q->Transaction->Commit();
     targetTL->EndUpdate  ();
     TcxTreeListNode *node = GetTreeListCtrl("PunktTL")->TopNode;
     node->Focused = true;
     GetTreeListCtrl("PunktTL")->BeginUpdate();
     try
      {
        while (node)
         {
            if (node->Values[1] == adres.Town1ID &&
                 node->Values[2] == adres.Town2ID &&
                 node->Values[3] == adres.Town3ID)
            {
                 node->Focused = 1;
                 node->MakeVisible();
                 if (FStreetVisible)
                  {
                    adres.NoStreets = (adres.Town1ID  && !(adres.Town2ID+adres.Town3ID));
                    if (!adres.NoStreets)
                     {
                       FQPrepare("SELECT Count(*) as STRCOUNT FROM KLADR5 WHERE \
                                  LEVEL1ID = :pLEVEL1ID AND LEVEL2ID = :pLEVEL2ID \
                                  AND LEVEL3ID = :pLEVEL3ID AND LEVEL4ID = :pLEVEL4ID");
                       Q->ParamByName("pLEVEL1ID")->AsInteger = adres.RegID;
                       Q->ParamByName("pLEVEL2ID")->AsInteger = adres.Town1ID;
                       Q->ParamByName("pLEVEL3ID")->AsInteger = adres.Town2ID;
                       Q->ParamByName("pLEVEL4ID")->AsInteger = adres.Town3ID;
                       Q->ExecQuery();
                       adres.NoStreets = !Q->FN("STRCOUNT")->AsInteger;
                     }
                  }
                 node = GetTreeListCtrl("PunktTL")->LastNode;
            }
           node = node->GetNext();
         }
      }
     __finally
      {
        GetTreeListCtrl("PunktTL")->EndUpdate();
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnTownChanged(TcxCustomTreeList *Sender, TcxTreeListNode *APrevFocusedNode,
          TcxTreeListNode *AFocusedNode)
{
  if (firstIDChange)
      return;
  if (!AFocusedNode)
   {
     cascadeIDClear(cctReg);
   }
  else
   {
     adres.Town1ID = AFocusedNode->Values[1];
     adres.Town2ID = AFocusedNode->Values[2];
     adres.Town3ID = AFocusedNode->Values[3];
   }
  cascadeIDClear(cctTown);
  if (FStreetVisible)
   {
     adres.NoStreets = (adres.Town1ID  && !(adres.Town2ID+adres.Town3ID));
     if (!adres.NoStreets)
      {
        FQPrepare("SELECT Count(*) as STRCOUNT FROM KLADR5 WHERE \
                   LEVEL1ID = :pLEVEL1ID AND LEVEL2ID = :pLEVEL2ID \
                   AND LEVEL3ID = :pLEVEL3ID AND LEVEL4ID = :pLEVEL4ID");
        Q->ParamByName("pLEVEL1ID")->AsInteger = adres.RegID;
        Q->ParamByName("pLEVEL2ID")->AsInteger = adres.Town1ID;
        Q->ParamByName("pLEVEL3ID")->AsInteger = adres.Town2ID;
        Q->ParamByName("pLEVEL4ID")->AsInteger = adres.Town3ID;
        Q->ExecQuery();
        adres.NoStreets = !Q->FN("STRCOUNT")->AsInteger;
      }
     // ��������� ������ ���� � ������ ���� ������ ��������� �����, ��� ������ �� ���� �� ������ (��� ��� ��� ����)
     fillStreetObjType(!adres.NoStreets);
     FSetDefStreetType();
     fillStreets();
   }
  updateControlsState();
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnTownExit(TObject* Sender)
{
/*    if (!adres.StreetID)
        fillStreets();
    if (!adres.level6ID || adres.level6IDa.IsEmpty())
        fillDoma();             */
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FSetDefStreetType()
{
  if ((adres.StrObjType == sotNone) && !adres.NoStreets)
   {
     int idx = GetComboBoxCtrl("ComboStreetType")->Properties->Items->IndexOfObject((TObject*)FStreetTypeDefIndex);
     if (idx != -1)
      {
        GetComboBoxCtrl("ComboStreetType")->ItemIndex = idx;
        adres.StrObjType = sotStreet;
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::fillStreetObjType(bool AFillStreetType)
{
  TcxComboBox* cb = GetComboBoxCtrl("ComboStreetType");
  try
   {
     cb->Properties->BeginUpdate();
     cb->Properties->Items->Clear();
     for (StringIntMap::iterator i = FStreetTypeObj.begin(); i != FStreetTypeObj.end(); i++)
      {
        if (((i->second == FStreetTypeDefIndex)&& AFillStreetType) || (i->second != FStreetTypeDefIndex))
         cb->Properties->Items->AddObject(i->first,(TObject*)i->second);
      }
     int idx = cb->Properties->Items->IndexOfObject((TObject*)adres.StrObjType);
     cb->ItemIndex = idx;
     TcxComboBox* strcb = GetComboBoxCtrl("ComboStreet");
     if (adres.StrObjType == FStreetTypeDefIndex)
      {
        strcb->Properties->DropDownListStyle = lsEditFixedList;
        strcb->Properties->MaskKind = emkStandard;
        strcb->Properties->EditMask = "";
      }
     else
      {
        strcb->Properties->DropDownListStyle = lsEditList;
        strcb->Properties->Items->Clear();
        strcb->Properties->MaskKind = emkRegExprEx;
        strcb->Properties->EditMask = "\\S{0,10}";
      }
   }
  __finally
   {
     cb->Properties->EndUpdate();
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnStreetTypeEnter(TObject *Sender)
{
  if (!adres.NoStreets && adres.StrObjType == sotStreet)
   {
//     GetCtrl("ComboStreet")->SetFocus();
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnStreetTypeChanged(TObject *Sender)
{
  if (firstIDChange) return;
  TcxComboBox* cb = (TcxComboBox*)Sender;
  if (cb->ItemIndex != -1)
   {
     adres.StrObjType = (TStreetObjectType)cb->Properties->Items->Objects[cb->ItemIndex];
     TcxComboBox* strcb = GetComboBoxCtrl("ComboStreet");
     if (adres.StrObjType == FStreetTypeDefIndex)
      {
        strcb->Properties->DropDownListStyle = lsEditFixedList;
        strcb->Properties->MaskKind = emkStandard;
        strcb->Properties->EditMask = "";
        fillStreets(false);
      }
     else
      {
        strcb->Properties->DropDownListStyle = lsEditList;
        strcb->Properties->Items->Clear();
        strcb->Properties->MaskKind = emkRegExprEx;
        strcb->Properties->EditMask = "\\S{0,10}";
        strcb->Text = "";
      }
   }
  updateControlsState();
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::fillStreets(bool ACheckLastState)
{
  // ��� ��������� �������� ����� ����� ������� ��������� ��������������
  // ������, ����� �� ��������� ���� � �� �� ������� �� ��������� ���
  static int lastLevel1ID, lastLevel2ID, lastLevel3ID, lastLevel4ID;

  if (!FStreetVisible ||
       (!firstIDChange && lastLevel1ID == adres.RegID && lastLevel2ID == adres.Town1ID &&
         lastLevel3ID == adres.Town2ID && lastLevel4ID == adres.Town3ID && ACheckLastState)
    )
   {
      return;
   }
  TcxComboBox* cb = GetComboBoxCtrl("ComboStreet");
  try
   {
     cb->Properties->BeginUpdate ();
     if (!((adres.StrObjType == sotStreet)||(adres.StrObjType == sotNone)) && adres.StreetObjectVal.Length())
      {
        cb->Text = PrepareStreetObjectVal(adres.StreetObjectVal);
      }
     else
      {
        lastLevel1ID = adres.RegID;
        lastLevel2ID = adres.Town1ID;
        lastLevel3ID = adres.Town2ID;
        lastLevel4ID = adres.Town3ID;

        cb->Properties->Items->Clear();
        streets->clear();
        if (!adres.NoStreets)
         { // ������ ��������� �����, � �� �����
           if (templateMode)
            {
               cb->Properties->Items->Add (TEMPLATE_EMPTY_TEXT);
               (*streets)[TEMPLATE_EMPTY_TEXT] = -101;
            }

           FQPrepare("SELECT LEVEL5ID, NAME, OBJECTTYPE FROM KLADR5 WHERE \
                           LEVEL1ID = :pLEVEL1ID AND LEVEL2ID = :pLEVEL2ID \
                           AND LEVEL3ID = :pLEVEL3ID AND LEVEL4ID = :pLEVEL4ID order by NAME");
           Q->ParamByName("pLEVEL1ID")->AsInteger = lastLevel1ID;//adres.RegID;
           Q->ParamByName("pLEVEL2ID")->AsInteger = lastLevel2ID;//adres.Town1ID;
           Q->ParamByName("pLEVEL3ID")->AsInteger = lastLevel3ID;//adres.Town2ID;
           Q->ParamByName("pLEVEL4ID")->AsInteger = lastLevel4ID;//adres.Town3ID;
           Q->ExecQuery();

           int temp;
           int selectedItemIndex = -1;
           int level5id;
           UnicodeString name;
           for (int i = 0; i < Q->RecordCount; i++)
            {
               name = FObjectFullName (Q->FN("NAME")->AsString,
                                     Q->FN("OBJECTTYPE")->AsString);
               level5id = Q->FN("LEVEL5ID")->AsInteger;
               (*streets)[name] = level5id;

               temp = cb->Properties->Items->Add(name);

               if (level5id == adres.StreetID)
                   selectedItemIndex = temp;

               Q->Next();
            }

//           adres.NoStreets = !(cb->Properties->Items->Count - (int)templateMode);


           if (selectedItemIndex != -1)
               cb->ItemIndex = selectedItemIndex;
           else if (templateMode && adres.StreetID == -101)
               cb->ItemIndex = cb->Properties->Items->IndexOf (TEMPLATE_EMPTY_TEXT);
           else
            {
               cb->ItemIndex = -1;
            }
         }
      }

   }
  __finally
   {
     Q->Close  ();
     if (Q->Transaction->Active) Q->Transaction->Commit();
     cb->Properties->EndUpdate();
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnStreetChanged(TObject *Sender)
{
  if (firstIDChange)  return;
  UnicodeString newStreet = ((TcxComboBox*)Sender)->EditValue;
  if (adres.StrObjType == sotStreet)
   {
     adres.StreetID = FGetObjCode(streets, newStreet);
     adres.StreetObjectVal = "";
     adres.State = true;
   }
  else if (adres.StrObjType != sotNone)
   {
     newStreet = ((TcxComboBox*)Sender)->EditingValue;
     adres.StreetID = 0;
     adres.StreetObjectVal = newStreet;
     adres.State = true;
   }
  cascadeIDClear(cctStreet);
  fillDoma();
  updateControlsState();
}

//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::fillDoma()
{
  if (!FHouseVisible)  return;
  GetMaskEditCtrl("HouseMaskEdit")->Text = adres.House;
  GetMaskEditCtrl("VldMaskEdit")->Text   = adres.Vld;
  GetMaskEditCtrl("KorpMaskEdit")->Text  = adres.Korp;
  GetMaskEditCtrl("BuildMaskEdit")->Text = adres.Build;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnHouseTextValidate(TObject *Sender,
      Variant &DisplayValue, TCaption &ErrorText, bool &Error)
{
  Error = false;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnHouseChanged(TObject *Sender)
{
  TcxMaskEdit *ed = (TcxMaskEdit*)Sender;
  if (!FCanHideHint && FCanShowHint)
   {
     ShowHouseHint(FHintCtrl, ed->EditingValue, 5, 10);
     FHintShowing = true;
   }
  if (firstIDChange) return;
  adres.House = UnicodeString(ed->Text).Trim();
  fillFlats();
  updateControlsState();
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnVldChanged(TObject *Sender)
{
  TcxMaskEdit *ed = (TcxMaskEdit*)Sender;
  if (!FCanHideHint && FCanShowHint)
   {
     ShowHouseHint(FHintCtrl, ed->EditingValue, 5, 10);
     FHintShowing = true;
   }
  if (firstIDChange) return;
  adres.Vld = UnicodeString(ed->Text).Trim();
  fillFlats();
  updateControlsState();
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnKorpChanged(TObject *Sender)
{
  TcxMaskEdit *ed = (TcxMaskEdit*)Sender;
  if (!FCanHideHint && FCanShowHint)
   {
     ShowHouseHint(FHintCtrl, ed->EditingValue, 5, 10);
     FHintShowing = true;
   }
  if (firstIDChange) return;
  adres.Korp = UnicodeString(ed->Text).Trim();
  fillFlats();
  updateControlsState();
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnBuildChanged(TObject *Sender)
{
  TcxMaskEdit *ed = (TcxMaskEdit*)Sender;
  if (!FCanHideHint && FCanShowHint)
   {
     ShowHouseHint(FHintCtrl, ed->EditingValue, 5, 10);
     FHintShowing = true;
   }
  if (firstIDChange) return;
  adres.Build = UnicodeString(ed->Text).Trim();
  fillFlats();
  updateControlsState();
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::fillFlats()
{
  if (!FFlatVisible) return;
  if ((adres.House+adres.Vld+adres.Korp+adres.Build).Length())
   {
     TcxComboBox *cb = GetComboBoxCtrl("ComboFlatType");
     if (adres.FlatObjType == fotNone)
      {
        adres.FlatObjType = fotFlat;
        cb->ItemIndex = cb->Properties->Items->IndexOfObject((TObject*)fotFlat);
      }
     else
      cb->ItemIndex = cb->Properties->Items->IndexOfObject((TObject*)adres.FlatObjType);
     GetMaskEditCtrl("FlatEdit")->Text = adres.Flat;
   }
  else
   cascadeIDClear(cctHouse);
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnFlatTypeChanged(TObject *Sender)
{
  if (firstIDChange) return;
  TcxComboBox* cb = (TcxComboBox*)Sender;
  if (cb->ItemIndex != -1)
   adres.FlatObjType = (TFlatObjectType)cb->Properties->Items->Objects[cb->ItemIndex];
  else
   {
     adres.FlatObjType = fotNone;
     adres.Flat = "";
   }
  updateControlsState();
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnFlatTextChanged(TObject *Sender)
{
  if (firstIDChange) return;

  adres.Flat = UnicodeString(((TcxMaskEdit*)Sender)->Text).Trim();
  updateControlsState();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TKLADRFORM::FCalcIndeks(adresCode adres)
{
  UnicodeString RC = "";
  try
   {
     UnicodeString tmpIndex = getIndeks (adres);
     if (tmpIndex.Length())
      RC = tmpIndex;
     else
      RC = FSaveIndeks;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnIndeksChanged(TObject *Sender)
{
  UnicodeString tmpIndex = UnicodeString(((TcxMaskEdit*)Sender)->Text).Trim();
  if (tmpIndex.Length() == 6)
   {
     FSaveIndeks = tmpIndex;
     adres.Index = tmpIndex;
   }
  else
   FSaveIndeks = "";
  updateControlsState();
}
//---------------------------------------------------------------------------
int __fastcall TKLADRFORM::FGetNewNFCode()
{
  int RC = 0;
  try
   {
     try
      {
        FQExec("SELECT Max(UID) as MCODE FROM KLADRNF");
        if (Q->RecordCount)
         RC = Q->FN("MCODE")->AsInteger+1;
      }
     catch (Exception &e)
      {
        throw EKLADRError("������ ����������� ������������� ���� ������.\n�������:\n" + e.Message);
      }
     catch (...)
      {
        throw EKLADRError("������ ����������� ������������� ���� ������.");
      }
   }
  __finally
   {
     if (Q->Transaction->Active) Q->Transaction->Rollback();
   }
  return RC; 
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FSaveNFAddr()
{
  try
   {
     try
      {
        int FNewNFAddrCode = FGetNewNFCode();
        FQExec("SELECT Count(*) as RCOUNT FROM KLADRNF where uid="+IntToStr(adres.NFAddrID));
        if (Q->FN("RCOUNT")->AsInteger)
         {
           FQPrepare("Update KLADRNF Set LEVEL1ID=:pLEVEL1ID, LEVEL2ID=:pLEVEL2ID, LEVEL3ID=:pLEVEL3ID,  LEVEL4ID=:pLEVEL4ID, NAME=:pNAME where uid="+IntToStr(adres.NFAddrID));
           FNewNFAddrCode = adres.NFAddrID;
         }
        else
         {
           FQPrepare("Insert into KLADRNF (UID, LEVEL1ID, LEVEL2ID, LEVEL3ID,  LEVEL4ID, NAME) values (:pUID, :pLEVEL1ID, :pLEVEL2ID, :pLEVEL3ID,  :pLEVEL4ID, :pNAME)");
           Q->ParamByName("pUID")->AsInteger = FNewNFAddrCode;
         }
        Q->ParamByName("pLEVEL1ID")->AsInteger = adres.RegID;
        Q->ParamByName("pLEVEL2ID")->AsInteger = adres.Town1ID;
        Q->ParamByName("pLEVEL3ID")->AsInteger = adres.Town2ID;
        Q->ParamByName("pLEVEL4ID")->AsInteger = adres.Town3ID;
        Q->ParamByName("pNAME")->AsString      = GetButtonEditCtrl("NFAddrBE")->Text;
        Q->ExecQuery();
        if (Q->Transaction->Active) Q->Transaction->Commit();
        adres.NFAddrID = FNewNFAddrCode;
      }
     catch (Exception &e)
      {
        throw EKLADRError("������ ���������� ������.\n�������:\n" + e.Message);
      }
     catch (...)
      {
        throw EKLADRError("������ ���������� ������.");
      }
   }
  __finally
   {
     if (Q->Transaction->Active) Q->Transaction->Rollback();
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::OkBtnClicked (TObject *Sender)
{
//  bool FCanCont = !IsActiveControl("NFAddrLB");

//  if (FCanCont)
//   {

     FIsNFAddr = FIsNotFormalAddr();
     if (!templateMode && FIsNFAddr)
      {
        if (FSaveNFAddrPrompt)
        _MSG_INFA("��������!\n��������� �� ��� ��������� ����.\n����� ����� �������� ��� �����������������.","���������");
        FSaveNFAddr();
      }
     FAdresCodeStr      = adres.AsString();
     FAdresShortCodeStr = adres.AsShortString();
     adresTextStr       = codeToText(FAdresCodeStr, "11111111", false, true);
     SQLTemplate        = FGetSQLTemplate();
     if (FHouseVisible)
      FHouseStr = adres.HouseCodeStr();
     if (FFlatVisible)
      FFlatStr = adres.FlatCodeStr();

     form->ModalResult = mrOk;
//   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::CancelBtnClicked (TObject *Sender)
{
  bool FCanCancel = false;
  if ((adres != origAdres) || FNFAddrModify)
   {
     if (FSaveModifAddrPrompt)
      {
        if (_MSG_QUEA("��������!\n� ����� ������� ���������.\n��������� ���������?","���������") == ID_YES)
         {
           GetBtnCtrl("okBtn")->Click();
         }
        else
         FCanCancel = true;
      }
   }
  else
   FCanCancel = true;
  if (FCanCancel)
   {
     adres              = origAdres;
     FAdresCodeStr      = adres.AsString();
     FAdresShortCodeStr = adres.AsShortString();
     adresTextStr       = codeToText(FAdresCodeStr);
     SQLTemplate        = FGetSQLTemplate();
     FIsNFAddr          = FIsNotFormalAddr();
     if (FHouseVisible)
      FHouseStr = adres.HouseCodeStr();
     if (FFlatVisible)
      FFlatStr = adres.FlatCodeStr();
     form->ModalResult  = mrCancel;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::FIsNotFormalAddr()
{
  bool RC = false;
  try
   {
     RC = (FTownRequired && !(((adres.Town1ID+adres.Town2ID+adres.Town3ID) && !((adres.RegID == MSC_ID)||(adres.RegID == SPB_ID)))||((adres.RegID == MSC_ID)||(adres.RegID == SPB_ID))));
     if (!RC)
      {
        RC = (FStreetRequired &&
              FStreetVisible &&
              (
                (
                  ( (adres.StrObjType == sotStreet) && !adres.StreetID)|| 
                    (((adres.StrObjType == sotArmyUnit)||(adres.StrObjType == sotOfficeBox)||(adres.StrObjType == sotPostOffice)) && !adres.StreetObjectVal.Length())||
                    ((adres.StrObjType == sotNone)&& !adres.NoStreets)
                  )
                )
              );
        if (!RC)
         {
           RC = (FHouseRequired && FHouseVisible && !(adres.House.Length()+adres.Vld.Length()+adres.Korp.Length()+adres.Build.Length()));
           if (!RC)
            RC = (FFlatRequired && FFlatVisible && !adres.Flat.Length());
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TKLADRFORM::FGetSQLTemplate()
{
  UnicodeString result = FAdresCodeStr;

  if (!adres.RegID)
   {
     result[1] = '_';
     result[2] = '_';
   }
  if (!adres.Town1ID && !adres.Town2ID && !adres.Town3ID)
   {
     result.Delete (3, 9);
     result.Insert (UnicodeString().StringOfChar('_',9), 3);
   }
  if (adres.StrObjType == sotStreet)
   {
     if (!adres.StreetID)
      {
        result.Delete (12,4);
        result.Insert (UnicodeString().StringOfChar('_',4), 12);
      }
   }
  else if (adres.StrObjType != sotNone)
   {
     if (!adres.StreetObjectVal.Length())
      {
        result.Delete (12,10);
        result.Insert (UnicodeString().StringOfChar('_',10), 12);
      }
   }
  else
   result[26] = '_';
/* if (!adres.level6ID)
   {
     result.Delete (16,4);
     result.Insert(UnicodeString().StringOfChar('_',4), 16);
   }
  if (adres.level6IDa.IsEmpty()) //!!! ���� ����������
   {
     result.Delete (20,4);
     result.Insert(UnicodeString().StringOfChar('_',4), 20);
   }

//  if (adres.level6IDb.IsEmpty())
//   {
//     result.Delete (24,3);
//     result.Insert(UnicodeString().StringOfChar('_',3), 24);
//   }

  if (adres.level7ID.IsEmpty()) //!!! ���� ����������
   {
     result.Delete (27,4);
     result.Insert(UnicodeString().StringOfChar('_',4), 27);
   }
  result.Delete (31,6);
  result.Insert(UnicodeString().StringOfChar('_',6), 31);
  */ //!!!
  return result;
}

//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::setAdresCodeStr(UnicodeString AVal)
{
  FAdresCodeStr = AVal;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FSetFindedAddr(UnicodeString AAddrCodeStr)
{
  adres = ParseAddrStr (AAddrCodeStr);

  GetButtonEditCtrl("NFAddrBE")->Text = codeToText(AAddrCodeStr.Trim(), "11111110");
  FNFAddrModify = false;
//  cascadeIDClear(cctTown);
  firstIDChange  = 1;
  fillRegions();
  fillTowns();
  fillStreetObjType(!adres.NoStreets); //
  FSetDefStreetType();
  fillStreets();

  fillDoma();
  fillFlats();
  firstIDChange  = 0;
  updateControlsState();

  if (FIndexVisible && !origAdres.Index.IsEmpty())
   GetMaskEditCtrl("EditIndeks")->Text = adres.Index;
  FSetActivControl();
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::NFAddrLBDblClick(TObject *Sender)
{
  TcxListBox *lb  = (TcxListBox*) Sender;
  if (lb->ItemIndex != -1)
   {
     FSetFindedAddr(FFindedObjects[lb->Items->Strings[lb->ItemIndex].UpperCase()]);
     FFindedObjects.clear();
     lb->Visible = false;
     GetButtonEditCtrl("NFAddrBE")->SetFocus();
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::NFAddrLBExit (TObject* Sender)
{
  GetListBoxCtrl("NFAddrLB")->Visible = false;
}
//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::textToCodeList(UnicodeString AAddr, TStringList *ARetCodes, bool ABeginFrom, bool ASilent)
{
  bool RC = false;
  bool FOverMaxRC = false;

  TNFAddrItems *FAddrNF = new TNFAddrItems(AAddr);
  UnicodeString FPreparedAddrStr = "";
/*  for (int i = 0; i < FAddrNF->Count; i++)
   {
     if (FAddrNF->Item[i]->PreparedText.Length())
      {
        if (FPreparedAddrStr.Length())
         FPreparedAddrStr += ",";
        FPreparedAddrStr += FAddrNF->Item[i]->PreparedText;
      }
   }  */
  FPreparedAddrStr = FPreparedAddrStr.Trim();
  TStringList *FAddrList = new TStringList;
  TStringList *FCurAddrList = new TStringList;
  int FFirstIdx = -1;
  try
   {
     UnicodeString FCurItem = "";
     if (FAddrNF->Count)
      {
        while ((FFirstIdx < FAddrNF->Count) && !FCurItem.Length())
         {
           FFirstIdx++;
           FCurItem = FAddrNF->Item[FFirstIdx]->PreparedText;
         }
        bool IsIndex = false;
        if (FCurItem.Length() == 6)
         {
           IsIndex = true;
           for (int i = 1; (i <= 6) && IsIndex; i++)
            IsIndex &= ((FCurItem[i]>='0')&&(FCurItem[i]<='9'));
         }
        if (IsIndex)
         {
           if (IndexToCodeList(FCurItem, FCurAddrList))
            {
              FOverMaxRC = (FCurAddrList->Count > MAX_SEARCH_COUNT);
              ARetCodes->Text = FCurAddrList->Text.Trim();
              RC = true;
            }
         }
        else
         {
           // ������ ������� ����� ���� ��� �������� ��� ���. �������
           if (NPTextToCodeList("", FAddrNF->Item[FFirstIdx], FCurAddrList, true, 2, 3))
            { // ������ ������� ���.�����, ���� ���.����� 2-�� ������
              FOverMaxRC = (FCurAddrList->Count > MAX_SEARCH_COUNT);
              for (int i = 0; (i < FCurAddrList->Count) && !FOverMaxRC; i++)
               {
                 RC |= FtextToCodeList (FCurAddrList->Strings[i], FAddrNF, FFirstIdx, psNP2, ABeginFrom, FAddrList, FOverMaxRC);
                 if (FAddrList->Count) ARetCodes->Text = ARetCodes->Text.Trim()+"\n"+FAddrList->Text.Trim();
                 else                  ARetCodes->Add(FCurAddrList->Strings[i]);
               }
            }
           else if (RegTextToCodeList(FCurItem, FCurAddrList, true))
            { // ������ ������� ������, ���� ���.����� 1-�� ������
              FOverMaxRC = (FCurAddrList->Count > MAX_SEARCH_COUNT);
              for (int i = 0; (i < FCurAddrList->Count) && !FOverMaxRC; i++)
               {
                 RC |= FtextToCodeList (FCurAddrList->Strings[i], FAddrNF, FFirstIdx+1, psNP1, ABeginFrom, FAddrList, FOverMaxRC);
                 if (FAddrList->Count) ARetCodes->Text = ARetCodes->Text.Trim()+"\n"+FAddrList->Text.Trim();
                 else                  ARetCodes->Add(FCurAddrList->Strings[i]);
               }
            }
         }
        if (RC)
         {
           // ���������� ������������ ������� ���������, ������->���. �����->�����
           int maxLevel = 0;
           int curLevel;
           adresCode fAddrCode;
           for (int i = 0; (i < ARetCodes->Count) && (maxLevel != 2); i++)
            {
              if (!ARetCodes->Strings[i].Trim().Length())
               {
                 ARetCodes->Delete(i);
                 i--;
               }
              else
               {
                 curLevel = 0;  // �� ��������� ���� ������
                 fAddrCode = ParseAddrStr(ARetCodes->Strings[i]);

                 if (fAddrCode.StreetID)
                  curLevel = 2;  // ���� ��� �����
                 else if (fAddrCode.Town1ID + fAddrCode.Town2ID+fAddrCode.Town3ID)
                  curLevel = 1;  // ���� ��� ���. �����
                 if (curLevel > maxLevel) maxLevel = curLevel;
               }
            }
           // ������� ������ ������� ����������� ������� ������ �������������
           for (int i = 0; i < ARetCodes->Count; i++)
            {
              fAddrCode = ParseAddrStr(ARetCodes->Strings[i]);
              curLevel = 0;  // �� ��������� ���� ������
              if (fAddrCode.StreetID)
               curLevel = 2;  // ���� ��� �����
              else if (fAddrCode.Town1ID + fAddrCode.Town2ID+fAddrCode.Town3ID)
               curLevel = 1;  // ���� ��� ���. �����
              if (curLevel < maxLevel)
               {
                 ARetCodes->Delete(i);
                 i--;
               }
            }
          if (FOverMaxRC)
           {
             RC = false;
             if (!ASilent)
              MessageBox(NULL, ("���������� ��������� ����� "+IntToStr(MAX_SEARCH_COUNT)+", �������� ������� ������.").c_str(),_T("���������"), MB_ICONINFORMATION);
           }
         }
        else if (FOverMaxRC)
         {
           RC = false;
           if (!ASilent)
            MessageBox(NULL, ("���������� ��������� ����� "+IntToStr(MAX_SEARCH_COUNT)+", �������� ������� ������.").c_str(),_T("���������"), MB_ICONINFORMATION);
         }
      }
   }
  __finally
   {
     delete FAddrNF;
     delete FAddrList;
     delete FCurAddrList;
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::FtextToCodeList(UnicodeString ACurAddrCode, TNFAddrItems *ASrcList, int ACurIdx, TAddrParseType ASearchState, bool ABeginFrom, TStringList *ARetCodes, bool &AOverMaxRC)
{
  bool RC = false;
//  bool AOverMaxRC = false;
  TStringList *FAddrList = new TStringList;
  TStringList *FCurAddrList = new TStringList;
  ARetCodes->Clear();
  try
   {
     if (!AOverMaxRC)
      {
        UnicodeString FCurItem, FStrAddrCode;
        FCurItem = FStrAddrCode = "";
        adresCode tmpAddrCode;
        if (ACurIdx < ASrcList->Count)
         {
           FCurItem = ASrcList->Item[ACurIdx]->PreparedText;
           FCurAddrList->Clear();
           bool FNPSearch = false;
           bool FStreetSearch = false;
           bool FHouseSearch = false;
           bool FFlatSearch = false;
           TAddrParseType FCurSearchState;
           if (ASearchState == psNP1)
            { // ������(�) ������(�)
              // ����� ���. ������ 1-� �������
              tmpAddrCode = ParseAddrStr(ACurAddrCode);
              FNPSearch = true;
              FCurSearchState = psNP2;
              FStreetSearch = (tmpAddrCode.RegID == MSC_ID || tmpAddrCode.RegID == SPB_ID);
            }
           else if (ASearchState == psNP2)
            { // ����� ���. ������ 2-� �������
              FNPSearch = true;
              FCurSearchState = psNP3;
              FStreetSearch = true;
            }
           else if (ASearchState == psNP3)
            { // ����� ���. ������ 3-� �������
              FNPSearch = true;
              FCurSearchState = psStreet;
              FStreetSearch = true;
            }
           else if (ASearchState == psStreet)
            { // ����� �����
              FStreetSearch = true;
              FCurSearchState = psHouse;
            }
           else if (ASearchState == psHouse)
            { // ����� �����
              FHouseSearch = true;
              FCurSearchState = psFlat;
            }
           else if (ASearchState == psFlat)
            { // ����� �����
              FFlatSearch = true;
            }
           if (FNPSearch && NPTextToCodeList(ACurAddrCode, ASrcList->Item[ACurIdx], FCurAddrList, true))
            {
              AOverMaxRC = (FCurAddrList->Count > MAX_SEARCH_COUNT);
              for (int i = 0; (i < FCurAddrList->Count) && !AOverMaxRC; i++)
               {
                 FtextToCodeList (FCurAddrList->Strings[i], ASrcList, ACurIdx+1, FCurSearchState, ABeginFrom, FAddrList, AOverMaxRC);
                 if (FAddrList->Count) ARetCodes->Text = ARetCodes->Text.Trim()+"\n"+FAddrList->Text.Trim();
                 else                  ARetCodes->Add(FCurAddrList->Strings[i]);
               }
              RC = true;
            }
           else if (FStreetSearch && StreetTextToCodeList(ACurAddrCode, ASrcList->Item[ACurIdx], FCurAddrList, true, true))
            {
              AOverMaxRC = (FCurAddrList->Count > MAX_SEARCH_COUNT);
              for (int i = 0; (i < FCurAddrList->Count) && !AOverMaxRC; i++)
               {
                 FtextToCodeList (FCurAddrList->Strings[i], ASrcList, ACurIdx+1, psHouse, ABeginFrom, FAddrList, AOverMaxRC);
                 if (FAddrList->Count) ARetCodes->Text = ARetCodes->Text.Trim()+"\n"+FAddrList->Text.Trim();
                 else                  ARetCodes->Add(FCurAddrList->Strings[i]);
               }
              RC = true;
            }
           else if (FHouseSearch && HouseTextToCodeList(ACurAddrCode, ASrcList->Item[ACurIdx], FCurAddrList))
            {
              AOverMaxRC = (FCurAddrList->Count > MAX_SEARCH_COUNT);
              for (int i = 0; (i < FCurAddrList->Count) && !AOverMaxRC; i++)
               {
                 FtextToCodeList (FCurAddrList->Strings[i], ASrcList, ACurIdx+1, psFlat, ABeginFrom, FAddrList, AOverMaxRC);
                 if (FAddrList->Count) ARetCodes->Text = ARetCodes->Text.Trim()+"\n"+FAddrList->Text.Trim();
                 else                  ARetCodes->Add(FCurAddrList->Strings[i]);
               }
            }
           else if (FFlatSearch && FlatTextToCodeList(ACurAddrCode, ASrcList->Item[ACurIdx], FCurAddrList))
            {
              AOverMaxRC = (FCurAddrList->Count > MAX_SEARCH_COUNT);
              if (FCurAddrList->Count) ARetCodes->Text = ARetCodes->Text.Trim()+"\n"+FCurAddrList->Text.Trim();
              else                     ARetCodes->Add(ACurAddrCode);
              RC = true;
            }
         }
      }
   }
  __finally
   {
     delete FAddrList;
     delete FCurAddrList;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnNFAddrBtnClick(TObject *Sender, int AButtonIndex)
{
  FNFAddrModify = false;
  if (AButtonIndex == 2)
   { // ������� ���������
     GetButtonEditCtrl("NFAddrBE")->Text = "";
     return;
   }
  TStringList *FAddr = new TStringList;
  UnicodeString tmpAddrStr, tmpAddrCode;
  try
   {
     TcxButtonEdit *FNFAddrBE = GetButtonEditCtrl("NFAddrBE");
     if (FNFAddrBE->Text.Length())
      {
        UnicodeString FNFAddrStr = FNFAddrBE->Text.Trim();
        UnicodeString FDefAddrStr = "";
        if (!AButtonIndex)
         { // ����� � ������������ ������ �� ���������
           if (FDefaultAddr.Length())
            FDefAddrStr = codeToText(FDefaultAddr,"11100000", true);
           else
            FDefAddrStr = codeToText(adres.AsString(),"11100000", true);
         }
        if (FDefAddrStr.Length())
         {
           if (!FNFAddrStr.UpperCase().Pos(FDefAddrStr.UpperCase()))
            FNFAddrStr = FDefAddrStr+", "+FNFAddrStr;
         }
        FFindedObjects.clear();
        if (textToCodeList(FNFAddrStr, FAddr, true))
         {
           ClearCacheEntry(adres.AsString());
           cascadeIDClear(cctAll);
           // ����������� �� ���������
           if (FAddr->Count == 1)
            {
              FSetFindedAddr(FAddr->Strings[0].UpperCase());
            }
           else if (FAddr->Count > 1)
            {
              TcxListBox *lb  = GetListBoxCtrl("NFAddrLB");
              TcxButtonEdit *te = GetButtonEditCtrl("NFAddrBE");
              lb->Items->Clear();
              for (int i = 0; i < FAddr->Count; i++)
               {
                 tmpAddrCode = FAddr->Strings[i].Trim();
                 tmpAddrStr  = codeToText(tmpAddrCode, "11111110");
                 FFindedObjects[tmpAddrStr.UpperCase()] = tmpAddrCode;
                 lb->Items->Add(tmpAddrStr);
               }
              lb->Left = te->Left;
              lb->Top =  te->Top + te->Height+1;
              lb->Width = te->Width;

              lb->Visible = true;
              lb->BringToFront();
              lb->SetFocus();
              lb->ItemIndex = 0;
            }
         }
        else
         {
           UnicodeString tmpSearchStr = FNFAddrBE->Text;
           ClearCacheEntry(adres.AsString());
           cascadeIDClear(cctAll);
           FNFAddrBE->Text = tmpSearchStr;
           FNFAddrModify = false;
           FNFAddrBE->SetFocus();
         }
      }
   }
  __finally
   {
     delete FAddr;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::IsActiveControl(UnicodeString AName)
{
  bool RC = false;
  try
   {
    if (form->ActiveControl)
     {
       if (form->ActiveControl->Name.Trim().Length())
        RC = (form->ActiveControl->Name == AName);
       else if (form->ActiveControl->Owner)
        RC = (form->ActiveControl->Owner->Name == AName);
     }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnCBKeyDown(TObject* Sender, WORD &Key, TShiftState Shift)
{
  if (Key == VK_RETURN)
   {
     if (!((TcxComboBox*)Sender)->DroppedDown)
      GetBtnCtrl("okBtn")->Click();
   }
  else
   FOnFormKeyDown(Sender, Key, Shift);
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnNFAddrBEKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
  if (Key == VK_RETURN) //(Key == 'F') && Shift.Contains(ssCtrl)
   {
     if (FNFAddrModify)
      {
        if (Shift.Contains(ssCtrl))
         FOnNFAddrBtnClick(NULL, 1);
        else
         FOnNFAddrBtnClick(NULL, 0);
      }
     else
      GetBtnCtrl("okBtn")->Click();
   }
  else if ((Key == VK_DELETE) && Shift.Contains(ssCtrl))
   FOnNFAddrBtnClick(NULL, 2);
  else if (Key == VK_ESCAPE)
   FOnFormKeyDown(Sender, Key, Shift);
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnNFAddrLBKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if (Key == VK_RETURN)
   NFAddrLBDblClick((TcxListBox*) Sender);
  else if (Key == VK_ESCAPE)
   {
     FFindedObjects.clear();
     ((TcxListBox*)Sender)->Visible = false;
     GetButtonEditCtrl("NFAddrBE")->SetFocus();
     FNFAddrModify = true;
   }
  else
   FOnFormKeyDown(Sender, Key, Shift);
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FOnFormKeyDown(TObject* Sender, WORD &Key, TShiftState Shift)
{
  if (Key == VK_ESCAPE)
   {
     if (!IsActiveControl("NFAddrLB"))
      {
        if (FHintShowing && FCanShowHint)
         {
           FHintCtrl->HideHint();
           FCanHideHint = true;
         }
        else
         GetBtnCtrl("cancelBtn")->Click();
      }
   }
  else if (Key == VK_RETURN)
   {
     GetBtnCtrl("okBtn")->Click();
   }
}

//---------------------------------------------------------------------------
UnicodeString __fastcall TKLADRFORM::getIndeks (adresCode adres)
{
  UnicodeString RC = "";
  try
   {
     try
      {
        if (adres.RegID || adres.Town1ID || adres.Town2ID || adres.Town3ID)
         {
           if (adres.StreetID > 0)
            {
               // ����� ������ �� ������� �����_5
               FQPrepare("SELECT INDEKS FROM KLADR5 WHERE LEVEL1ID = :L1 \
                               AND LEVEL2ID = :L2 AND LEVEL3ID = :L3 AND \
                               LEVEL4ID = :L4 AND LEVEL5ID = :L5");
               Q->ParamByName("L5")->AsInteger = adres.StreetID;
            }
           else
            {
               // ����� ������ �� ������� �����_14
               FQPrepare("SELECT INDEKS FROM KLADR14 WHERE LEVEL1ID = :L1 \
                               AND LEVEL2ID = :L2 AND LEVEL3ID = :L3 AND LEVEL4ID = :L4");
            }
           Q->ParamByName("L1")->AsInteger = adres.RegID;
           Q->ParamByName("L2")->AsInteger = adres.Town1ID;
           Q->ParamByName("L3")->AsInteger = adres.Town2ID;
           Q->ParamByName("L4")->AsInteger = adres.Town3ID;
           Q->ExecQuery();

           RC = Q->RecordCount ? Q->FN("INDEKS")->AsString : UnicodeString("");
         }
      }
     catch(...)
      {
        throw EKLADRError("������ ��������� �������");
      }
   }
  __finally
   {
     Q->Close();
     if (Q->Transaction->Active) Q->Transaction->Rollback();
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::onFormClose (TObject* Sender, TCloseAction& cAction)
{
  if (FCanShowHint)
   {
     FHintCtrl->HideHint();
     FHintShowing = false;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::isAdresCorrect (UnicodeString code)
{
    try
    {
        adresCode adres = ParseAddrStr(code);

        // ������� �������� �� ������������� ���� ��������� �����
        if (!adres.RegID ||
             (FTownRequired && !adres.Town1ID && !adres.Town2ID && !adres.Town3ID) ||
             (FStreetRequired && !(adres.StreetID || adres.StreetObjectVal.Length())) || //&& (!adres.level6ID && adres.level6IDa.IsEmpty())) ||
             (FHouseRequired && !(adres.House.Length() || adres.Vld.Length() || adres.Korp.Length() || adres.Build.Length() )) ||
             (FFlatRequired && !adres.Flat.Length())
          )
        {
            return 0;
        }


        bool result = 0;
        if (adres.StreetID > 0)
         {
            // ���� ��������� ��� ����, ������ ��������� �� ������� ����� ������
            FQPrepare("SELECT UID FROM KLADR5 WHERE LEVEL1ID = :pL1 AND \
                            LEVEL2ID =:pL2 AND LEVEL3ID = :pL3 AND LEVEL4ID = :pL4 \
                            AND LEVEL5ID = :pL5");
            Q->ParamByName("pL1")->AsInteger = adres.RegID;
            Q->ParamByName("pL2")->AsInteger = adres.Town1ID;
            Q->ParamByName("pL3")->AsInteger = adres.Town2ID;
            Q->ParamByName("pL4")->AsInteger = adres.Town3ID;
            Q->ParamByName("pL5")->AsInteger = adres.StreetID;
            Q->ExecQuery();

            if (Q->RecordCount) result = 1;
         }
        else
         {
            // ����� �� ��������� - �������� ������� ���.������
            // ��� �������� ��� ��������: ����� "��� ����" ��� ����� �� ������� ������
            FQPrepare("SELECT UID FROM KLADR14 WHERE LEVEL1ID = :pL1 AND \
                            LEVEL2ID =:pL2 AND LEVEL3ID = :pL3 AND LEVEL4ID = :pL4");
            Q->ParamByName("pL1")->AsInteger = adres.RegID;
            Q->ParamByName("pL2")->AsInteger = adres.Town1ID;
            Q->ParamByName("pL3")->AsInteger = adres.Town2ID;
            Q->ParamByName("pL4")->AsInteger = adres.Town3ID;
            Q->ExecQuery();

            // ���� ����� �� ������� ������ � ��������� ���.����� ����������,
            // �� �� ��� �������� �������������, ����� ��� ��������� ���.�����
            // �� ������� � ���� ���� (�.�. ������� "��� ����" ���������� ������
            // �����, ����� � ���.������ ������������� ��� ����
            if (Q->RecordCount && !adres.StreetID)
             {
               result = 1;
             }
            else
             {
                // �������� ����� �� ���� ������ ����� � ����� �.������
                FQPrepare("SELECT UID FROM KLADR5 WHERE LEVEL1ID = :pL1 \
                                AND LEVEL2ID = :pL2 AND LEVEL3ID = :pL3 \
                                AND LEVEL4ID = :pL4");
                Q->ParamByName("pL1")->AsInteger = adres.RegID;
                Q->ParamByName("pL2")->AsInteger = adres.Town1ID;
                Q->ParamByName("pL3")->AsInteger = adres.Town2ID;
                Q->ParamByName("pL4")->AsInteger = adres.Town3ID;
                Q->ExecQuery();

                if (!Q->RecordCount)
                 {
                    // �������� �� ������� ����������� � ����� ���.������
                    UnicodeString query = "SELECT UID FROM KLADR14 WHERE ";
                    if (!adres.Town3ID && !adres.Town2ID && !adres.Town1ID)
                        query += "(LEVEL1ID = :pL1 AND (LEVEL2ID != 0 OR LEVEL3ID != 0 OR LEVEL4ID != 0)";
                    else if (!adres.Town3ID && !adres.Town2ID)
                        query += "(LEVEL1ID = :pL1 AND LEVEL2ID = :pL2 AND (LEVEL3ID != 0 OR LEVEL4ID != 0)";
                    else if (!adres.Town3ID)
                        query += "(LEVEL1ID = :pL1 AND LEVEL2ID = :pL2 AND LEVEL3ID = :pL3 AND LEVEL4ID != 0)";
                    else
                        query += "1 = 0";


                    FQPrepare(query);
                    int temp;
                    if (Q->ParamExist("pL1", temp))
                        Q->ParamByName("pL1")->AsInteger = adres.RegID;
                    if (Q->ParamExist("pL2", temp))
                        Q->ParamByName("pL2")->AsInteger = adres.Town1ID;
                    if (Q->ParamExist("pL3", temp))
                        Q->ParamByName("pL3")->AsInteger = adres.Town2ID;
                    Q->ExecQuery();

                    if (Q->RecordCount)
                        result = 0;
                    else
                        result = 1;
                 }
                else result = 0;
             }
//            else result = 0;
         } // if (adres.StreetID)

        Q->Close  ();
        if (Q->Transaction->Active) Q->Transaction->Commit();

        return result;
    }
    catch (...)
    {
        Q->Close  ();
        if (Q->Transaction->Active) Q->Transaction->Rollback();
        throw EKLADRError("������ �������� ������������ ������");
    }
}

//---------------------------------------------------------------------------


//***************************************************************************
//                                                                          *
//                           ����� ������                                   *
//                                                                          *
//***************************************************************************

bool __fastcall TKLADRFORM::TextToCode (UnicodeString adresStr, UnicodeString name, int searchType, UnicodeString &ret_code, bool ABeginFrom)
{
    UnicodeString orig_ret_code = ret_code;
    try
    {
        prepareData();

        if (!connectToDB())
            return false;

        KLADRSearchFunc search_func = searchFuncs[searchType];

        if (search_func)
            return search_func (adresStr, name, ret_code, ABeginFrom);

        return false;
    }
    catch (ESearchException &E)
    {
        ret_code = orig_ret_code;
        throw EKLADRError(E.Message);
    }
    catch (...)
    {
        ret_code = orig_ret_code;
        throw EKLADRError("Unknown Error");
    }
}

//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::textToCodeReg (UnicodeString adresStr, UnicodeString name, UnicodeString &ret_code, bool ABeginFrom)
{
  bool RC = false;
  UnicodeString FName = name.Trim();
  if (FName.Length() > 40) FName.SetLength(40);
  try
   {
     try
      {
        adresCode adres = ParseAddrStr(adresStr);

        FQPrepare("SELECT LEVEL1ID FROM KLADR14 WHERE UNAME = :pNAME \
                        AND LEVEL2ID = 0 AND LEVEL3ID = 0 AND LEVEL4ID = 0");
        Q->ParamByName("pNAME")->AsString = FName.UpperCase();
        Q->ExecQuery ();

        if (Q->RecordCount)
         {
           adres.Clear();
           adres.RegID = Q->FN("LEVEL1ID")->AsInteger;
           ret_code = adres.AsString();
           RC = true;
         }
      }
     catch (Exception &E)
      {
        throw ESearchException ("������ ������.\n��������� ���������:\n"+E.Message);
      }
     catch (...)
      {
        throw ESearchException ("Searching error");
      }
   }
  __finally
   {
     Q->Close  ();
     if (Q->Transaction->Active) Q->Transaction->Commit();
   }
  return RC;
}

//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::textToCodeNP (UnicodeString adresStr, UnicodeString name, UnicodeString &ret_code, bool ABeginFrom)
{
  bool RC = false;
  UnicodeString FName = name.Trim();
  if (FName.Length() > 40) FName.SetLength(40);
  try
   {
     try
      {
        adresCode adres = ParseAddrStr(adresStr);
        FQPrepare("SELECT LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID FROM KLADR14 \
                        WHERE UNAME = :pNAME AND LEVEL1ID = :pLEVEL1ID");
        Q->ParamByName("pNAME")->AsString      = FName.UpperCase();
        Q->ParamByName("pLEVEL1ID")->AsInteger = adres.RegID;
        Q->ExecQuery();

        if (Q->RecordCount)
         {
           adres.Clear();
           adres.RegID = Q->FN("LEVEL1ID")->AsInteger;
           adres.Town1ID = Q->FN("LEVEL2ID")->AsInteger;
           adres.Town2ID = Q->FN("LEVEL3ID")->AsInteger;
           adres.Town3ID = Q->FN("LEVEL4ID")->AsInteger;
           ret_code = adres.AsString();
           RC = true;
         }
      }
     catch (Exception &E)
      {
        throw ESearchException ("������ ������.\n��������� ���������:\n"+E.Message);
      }
     catch (...)
      {
        throw ESearchException ("Searching error");
      }
   }
  __finally
   {
     Q->Close  ();
     if (Q->Transaction->Active) Q->Transaction->Commit();
   }
  return RC;
}

//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::textToCodeStreet (UnicodeString adresStr, UnicodeString name, UnicodeString &ret_code, bool ABeginFrom)
{
  bool RC = false;
  UnicodeString FName = name.Trim();
  if (FName.Length() > 40) FName.SetLength(40);
  try
   {
     try
      {
        adresCode adres = ParseAddrStr(adresStr);
        FQPrepare("SELECT LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID, LEVEL5ID FROM KLADR5 \
                        WHERE UNAME = :pNAME AND LEVEL1ID = :pLEVEL1ID AND LEVEL2ID = :pLEVEL2ID \
                        AND LEVEL3ID = :pLEVEL3ID AND LEVEL4ID = :pLEVEL4ID");
        Q->ParamByName("pNAME")->AsString      = FName.UpperCase();
        Q->ParamByName("pLEVEL1ID")->AsInteger = adres.RegID;
        Q->ParamByName("pLEVEL2ID")->AsInteger = adres.Town1ID;
        Q->ParamByName("pLEVEL3ID")->AsInteger = adres.Town2ID;
        Q->ParamByName("pLEVEL4ID")->AsInteger = adres.Town3ID;
        Q->ExecQuery();

        if (Q->RecordCount)
         {
           adres.Clear();
           adres.RegID = Q->FN("LEVEL1ID")->AsInteger;
           adres.Town1ID = Q->FN("LEVEL2ID")->AsInteger;
           adres.Town2ID = Q->FN("LEVEL3ID")->AsInteger;
           adres.Town3ID = Q->FN("LEVEL4ID")->AsInteger;
           adres.StreetID = Q->FN("LEVEL5ID")->AsInteger;
           ret_code = adres.AsString();
           RC = true;
         }
      }
     catch (Exception &E)
      {
        throw ESearchException ("������ ������.\n��������� ���������:\n"+E.Message);
      }
     catch (...)
      {
        throw ESearchException ("Searching error");
      }
   }
  __finally
   {
     Q->Close  ();
     if (Q->Transaction->Active) Q->Transaction->Commit();
   }
  return RC;
}

//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::textToCodeReg_WC (UnicodeString adresStr, UnicodeString name, UnicodeString &ret_code, bool ABeginFrom)
{
  bool RC = false;
  UnicodeString FName = name.Trim();
  if (FName.Length() > 40) FName.SetLength(40);
  try
   {
     try
      {
        adresCode adres = ParseAddrStr(adresStr);

        UnicodeString FSBText = (ABeginFrom)? "":"%";
        FQPrepare("SELECT LEVEL1ID FROM KLADR14 WHERE UNAME LIKE '"+FSBText+"'||:pNAME||'%'  \
                        AND LEVEL2ID = 0 AND LEVEL3ID = 0 AND LEVEL4ID = 0");
        Q->ParamByName("pNAME")->AsString = FName.UpperCase();
        Q->ExecQuery ();

        if (Q->RecordCount)
         {
            adres.Clear();
            adres.RegID = Q->FN("LEVEL1ID")->AsInteger;
            ret_code = adres.AsString();
            RC = true;
         }
      }
     catch (Exception &E)
      {
        throw ESearchException ("������ ������.\n��������� ���������:\n"+E.Message);
      }
     catch (...)
      {
        throw ESearchException ("Searching error");
      }
   }
  __finally
   {
     Q->Close  ();
     if (Q->Transaction->Active) Q->Transaction->Commit();
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::textToCodeNP_WC (UnicodeString adresStr, UnicodeString name, UnicodeString &ret_code, bool ABeginFrom)
{
  bool RC = false;
  UnicodeString FName = name.Trim();
  if (FName.Length() > 40) FName.SetLength(40);
  try
   {
     try
      {
        adresCode adres = ParseAddrStr(adresStr);

        UnicodeString FSBText = (ABeginFrom)? "":"%";
        FQPrepare("SELECT LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID FROM KLADR14 \
                        WHERE UNAME LIKE '"+FSBText+"'||:pNAME||'%' AND LEVEL1ID = :pLEVEL1ID");
        Q->ParamByName("pNAME")->AsString      = FName.UpperCase();
        Q->ParamByName("pLEVEL1ID")->AsInteger = adres.RegID;
        Q->ExecQuery();

        if (Q->RecordCount)
         {
            adres.Clear();
            adres.RegID = Q->FN("LEVEL1ID")->AsInteger;
            adres.Town1ID = Q->FN("LEVEL2ID")->AsInteger;
            adres.Town2ID = Q->FN("LEVEL3ID")->AsInteger;
            adres.Town3ID = Q->FN("LEVEL4ID")->AsInteger;
            ret_code = adres.AsString();
            RC = true;
         }
      }
     catch (Exception &E)
      {
        throw ESearchException ("������ ������.\n��������� ���������:\n"+E.Message);
      }
     catch (...)
      {
        throw ESearchException ("Searching error");
      }
   }
  __finally
   {
     Q->Close();
     if (Q->Transaction->Active) Q->Transaction->Commit();
   }
  return RC;
}

//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::textToCodeStreet_WC (UnicodeString adresStr, UnicodeString name, UnicodeString &ret_code, bool ABeginFrom)
{
  bool RC = false;
  UnicodeString FName = name.Trim();
  if (FName.Length() > 40) FName.SetLength(40);
  try
   {
     try
      {
        adresCode adres = ParseAddrStr(adresStr);

        UnicodeString FSBText = (ABeginFrom)? "":"%";
        FQPrepare("SELECT LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID, LEVEL5ID FROM KLADR5 \
                        WHERE UNAME LIKE '"+FSBText+"'||:pNAME||'%' AND LEVEL1ID = :pLEVEL1ID AND LEVEL2ID = :pLEVEL2ID \
                        AND LEVEL3ID = :pLEVEL3ID AND LEVEL4ID = :pLEVEL4ID");
        Q->ParamByName("pNAME")->AsString      = FName.UpperCase();
        Q->ParamByName("pLEVEL1ID")->AsInteger = adres.RegID;
        Q->ParamByName("pLEVEL2ID")->AsInteger = adres.Town1ID;
        Q->ParamByName("pLEVEL3ID")->AsInteger = adres.Town2ID;
        Q->ParamByName("pLEVEL4ID")->AsInteger = adres.Town3ID;
        Q->ExecQuery();

        if (Q->RecordCount)
         {
            adres.Clear();
            adres.RegID = Q->FN("LEVEL1ID")->AsInteger;
            adres.Town1ID = Q->FN("LEVEL2ID")->AsInteger;
            adres.Town2ID = Q->FN("LEVEL3ID")->AsInteger;
            adres.Town3ID = Q->FN("LEVEL4ID")->AsInteger;
            adres.StreetID = Q->FN("LEVEL5ID")->AsInteger;
            ret_code = adres.AsString();
            RC = true;
         }
      }
     catch (Exception &E)
      {
        throw ESearchException ("������ ������.\n��������� ���������:\n"+E.Message);
      }
     catch (...)
      {
         throw ESearchException ("Searching error");
      }
   }
  __finally
   {
     Q->Close  ();
     if (Q->Transaction->Active) Q->Transaction->Commit();
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::AddUnique (TStringList *ADest, UnicodeString ASrc)
{
  if (ADest->IndexOf(ASrc) == -1)
   ADest->Add(ASrc);
}
//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::RegTextToCodeList(UnicodeString AName, TStringList *ARetCodes, bool ABeginFrom)
{
  bool RC = false;
  UnicodeString FName = AName.Trim();
  if (FName.Length() > 40) FName.SetLength(40);
  try
   {
     try
      {
          adresCode adres = ParseAddrStr("");

          UnicodeString FSBText = (ABeginFrom)? "":"%";
          FQPrepare("SELECT LEVEL1ID FROM KLADR14 WHERE UNAME LIKE '"+FSBText+"'||:pNAME||'%'  \
                          AND LEVEL2ID = 0 AND LEVEL3ID = 0 AND LEVEL4ID = 0");
          Q->ParamByName("pNAME")->AsString = FName.UpperCase();
          Q->ExecQuery ();

          if (Q->RecordCount)
          {
            while (!Q->Eof)
             {
               adres.Clear();
               adres.RegID = Q->FN("LEVEL1ID")->AsInteger;
               AddUnique(ARetCodes, adres.AsString());
               Q->Next();
             }
            RC = true;
          }
      }
     catch (Exception &E)
      {
        throw ESearchException ("������ ������.\n��������� ���������:\n"+E.Message);
      }
     catch (...)
      {
          throw ESearchException ("Searching error");
      }
   }
  __finally
   {
     Q->Close();
     if (Q->Transaction->Active) Q->Transaction->Rollback();
   }
  return RC;
}

//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::IndexToCodeList(UnicodeString AName, TStringList *ARetCodes)
{
  bool RC = false;
  UnicodeString FName = AName.Trim();
  if (FName.Length() > 40) FName.SetLength(40);
  try
   {
     try
     {
         adresCode adres;

         FQPrepare("SELECT LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID FROM KLADR14 \
                         WHERE INDEKS = :pNAME");
         Q->ParamByName("pNAME")->AsString      = FName.UpperCase();
         Q->ExecQuery();

         if (Q->RecordCount)
         {
           while (!Q->Eof)
            {
              adres.Clear();
              adres.RegID = Q->FN("LEVEL1ID")->AsInteger;
              adres.Town1ID = Q->FN("LEVEL2ID")->AsInteger;
              adres.Town2ID = Q->FN("LEVEL3ID")->AsInteger;
              adres.Town3ID = Q->FN("LEVEL4ID")->AsInteger;
              AddUnique(ARetCodes, adres.AsString());
              Q->Next();
            }
           RC = true;
         }
         FQPrepare("SELECT LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID, LEVEL5ID FROM KLADR5 \
                         WHERE INDEKS = :pNAME");
         Q->ParamByName("pNAME")->AsString      = FName.UpperCase();
         Q->ExecQuery();

         if (Q->RecordCount)
         {
           while (!Q->Eof)
            {
              adres.Clear();
              adres.RegID = Q->FN("LEVEL1ID")->AsInteger;
              adres.Town1ID = Q->FN("LEVEL2ID")->AsInteger;
              adres.Town2ID = Q->FN("LEVEL3ID")->AsInteger;
              adres.Town3ID = Q->FN("LEVEL4ID")->AsInteger;
              adres.StreetID = Q->FN("LEVEL5ID")->AsInteger;
              AddUnique(ARetCodes, adres.AsString());
              Q->Next();
            }
           RC = true;
         }
     }
     catch (Exception &E)
      {
        throw ESearchException ("������ ������.\n��������� ���������:\n"+E.Message);
      }
     catch (...)
     {
       throw ESearchException ("Searching error");
     }
   }
  __finally
   {
     Q->Close();
     if (Q->Transaction->Active) Q->Transaction->Rollback();
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::NPTextToCodeList(UnicodeString AAdresStr, TNFAddrWords *AName, TStringList *ARetCodes, bool ABeginFrom, int AStatusBeg, int AStatusEnd)
{
  bool RC = false;
  try
   {
     UnicodeString FSQL;
     try
      {
        adresCode FNPAddr = ParseAddrStr(AAdresStr);
        UnicodeString FName = AName->PreparedText;
        for (int i = 1; i <= FName.Length(); i++)
         {
           if (FName[i] == ' ')
            FName[i] = '%';
         }
        UnicodeString FSBText = (ABeginFrom)? "":"%";
        FSQL = "SELECT LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID FROM KLADR14 \
                WHERE UNAME LIKE '"+FSBText+"'||:pNAME";

        if (FNPAddr.RegID)    FSQL += " AND LEVEL1ID = :pLEVEL1ID";
        if (FNPAddr.Town1ID)  FSQL += " AND LEVEL2ID = :pLEVEL2ID";
        if (FNPAddr.Town2ID)  FSQL += " AND LEVEL3ID = :pLEVEL3ID";
        if (FNPAddr.Town3ID)  FSQL += " AND LEVEL4ID = :pLEVEL4ID";
//         if (AStatusBeg != -1) FSQL += " AND STATUS >= :pSTATUSB";
//         if (AStatusEnd != -1) FSQL += " AND STATUS <= :pSTATUSE";
                              FSQL += " and ((LEVEL2ID<>0 AND not (LEVEL3ID = 0 AND LEVEL4ID = 0)) or (LEVEL2ID=0))";

        FQPrepare(FSQL);
        if (FNPAddr.RegID)    Q->ParamByName("pLEVEL1ID")->AsInteger = FNPAddr.RegID;
        if (FNPAddr.Town1ID)  Q->ParamByName("pLEVEL2ID")->AsInteger = FNPAddr.Town1ID;
        if (FNPAddr.Town2ID)  Q->ParamByName("pLEVEL3ID")->AsInteger = FNPAddr.Town2ID;
        if (FNPAddr.Town3ID)  Q->ParamByName("pLEVEL4ID")->AsInteger = FNPAddr.Town3ID;
//         if (AStatusBeg != -1) Q->ParamByName("pSTATUSB")->AsInteger  = AStatusBeg;
//         if (AStatusEnd != -1) Q->ParamByName("pSTATUSE")->AsInteger  = AStatusEnd;
        if (FName.Trim().Length() > 40) FName = FName.Trim().SetLength(40);
        Q->ParamByName("pNAME")->AsString      = FName.UpperCase();
        Q->ExecQuery();

        if (Q->RecordCount)
         {
           while (!Q->Eof)
            {
              FNPAddr.Clear();
              FNPAddr.RegID   = Q->FN("LEVEL1ID")->AsInteger;
              FNPAddr.Town1ID = Q->FN("LEVEL2ID")->AsInteger;
              FNPAddr.Town2ID = Q->FN("LEVEL3ID")->AsInteger;
              FNPAddr.Town3ID = Q->FN("LEVEL4ID")->AsInteger;
              AddUnique(ARetCodes, FNPAddr.AsString());
              Q->Next();
            }
           RC = true;
         }
        else
         {
           UnicodeString FSBText = (ABeginFrom)? "":"%";
           FSQL = "SELECT LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID FROM KLADR14 \
                           WHERE UNAME LIKE '"+FSBText+"'||:pNAME||'%'";

           if (FNPAddr.RegID)    FSQL += " AND LEVEL1ID = :pLEVEL1ID";
           if (FNPAddr.Town1ID)  FSQL += " AND LEVEL2ID = :pLEVEL2ID";
           if (FNPAddr.Town2ID)  FSQL += " AND LEVEL3ID = :pLEVEL3ID";
           if (FNPAddr.Town3ID)  FSQL += " AND LEVEL4ID = :pLEVEL4ID";
   //         if (AStatusBeg != -1) FSQL += " AND STATUS >= :pSTATUSB";
   //         if (AStatusEnd != -1) FSQL += " AND STATUS <= :pSTATUSE";
                                 FSQL += "  and ((LEVEL2ID<>0 AND not (LEVEL3ID = 0 AND LEVEL4ID = 0)) or (LEVEL2ID=0))";

           FQPrepare(FSQL);
           if (FNPAddr.RegID)    Q->ParamByName("pLEVEL1ID")->AsInteger = FNPAddr.RegID;
           if (FNPAddr.Town1ID)  Q->ParamByName("pLEVEL2ID")->AsInteger = FNPAddr.Town1ID;
           if (FNPAddr.Town2ID)  Q->ParamByName("pLEVEL3ID")->AsInteger = FNPAddr.Town2ID;
           if (FNPAddr.Town3ID)  Q->ParamByName("pLEVEL4ID")->AsInteger = FNPAddr.Town3ID;
   //         if (AStatusBeg != -1) Q->ParamByName("pSTATUSB")->AsInteger  = AStatusBeg;
   //         if (AStatusEnd != -1) Q->ParamByName("pSTATUSE")->AsInteger  = AStatusEnd;
           if (FName.Trim().Length() > 40) FName = FName.Trim().SetLength(40);
           Q->ParamByName("pNAME")->AsString     = FName.UpperCase();
           Q->ExecQuery();

           if (Q->RecordCount)
            {
              while (!Q->Eof)
               {
                 FNPAddr.Clear();
                 FNPAddr.RegID   = Q->FN("LEVEL1ID")->AsInteger;
                 FNPAddr.Town1ID = Q->FN("LEVEL2ID")->AsInteger;
                 FNPAddr.Town2ID = Q->FN("LEVEL3ID")->AsInteger;
                 FNPAddr.Town3ID = Q->FN("LEVEL4ID")->AsInteger;
                 AddUnique(ARetCodes, FNPAddr.AsString());
                 Q->Next();
               }
              RC = true;
            }
         }
      }
     catch (Exception &E)
      {
        throw ESearchException (E.Message);
      }
   }
  __finally
   {
     Q->Close();
     if (Q->Transaction->Active) Q->Transaction->Rollback();
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::StreetTextToCodeList(UnicodeString AAdresStr, TNFAddrWords *AName, TStringList *ARetCodes, bool ABeginFrom, bool AFullSearch)
{
  bool RC = false;
  UnicodeString FName;
  try
   {
     if (StreetVisible)
      {
        try
         {
           adresCode FStreetAddr = ParseAddrStr(AAdresStr);
           if (AName->Count == 2)
            {
              if (StrInList("�\�,�/�,�\�.,�/�.", AName->Word[0].UpperCase()))
               { // ������ � �/�
                 FStreetAddr.StrObjType = sotArmyUnit;
   //              FStreetAddr.NoStreets = true;
                 FStreetAddr.StreetObjectVal = AName->Word[1];
                 FStreetAddr.State = 1;
                 AddUnique(ARetCodes, FStreetAddr.AsString());
                 RC = true;
               }
              else if (StrInList("�\�,�/�,�\�.,�/�.", AName->Word[0].UpperCase()))
               { //������ �/�
                 FStreetAddr.StrObjType = sotOfficeBox;
   //              FStreetAddr.NoStreets = true;
                 FStreetAddr.StreetObjectVal = AName->Word[1];
                 FStreetAddr.State = 1;
                 AddUnique(ARetCodes, FStreetAddr.AsString());
                 RC = true;
               }
              else if (StrInList("���,���.", AName->Word[0].UpperCase()))
               { //������ � ���
                 FStreetAddr.StrObjType = sotPostOffice;
   //              FStreetAddr.NoStreets = true;
                 FStreetAddr.StreetObjectVal = AName->Word[1];
                 FStreetAddr.State = 1;
                 AddUnique(ARetCodes, FStreetAddr.AsString());
                 RC = true;
               }

            }
           if (!RC)
            {
              UnicodeString FSBText = (ABeginFrom)? "":"%";
              FQPrepare("SELECT LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID, LEVEL5ID FROM KLADR5 \
                              WHERE UNAME LIKE '"+FSBText+"'||:pNAME||'%' AND LEVEL1ID = :pLEVEL1ID AND LEVEL2ID = :pLEVEL2ID \
                              AND LEVEL3ID = :pLEVEL3ID AND LEVEL4ID = :pLEVEL4ID");

              FName = AName->PreparedText.Trim();
              if (FName.Length() > 40) FName.SetLength(40);

              Q->ParamByName("pNAME")->AsString      = FName.UpperCase();
              Q->ParamByName("pLEVEL1ID")->AsInteger = FStreetAddr.RegID;
              Q->ParamByName("pLEVEL2ID")->AsInteger = FStreetAddr.Town1ID;
              Q->ParamByName("pLEVEL3ID")->AsInteger = FStreetAddr.Town2ID;
              Q->ParamByName("pLEVEL4ID")->AsInteger = FStreetAddr.Town3ID;
              Q->ExecQuery();

              if (Q->RecordCount)
               {
                 while (!Q->Eof)
                  {
                    FStreetAddr.Clear();
                    FStreetAddr.RegID = Q->FN("LEVEL1ID")->AsInteger;
                    FStreetAddr.Town1ID = Q->FN("LEVEL2ID")->AsInteger;
                    FStreetAddr.Town2ID = Q->FN("LEVEL3ID")->AsInteger;
                    FStreetAddr.Town3ID = Q->FN("LEVEL4ID")->AsInteger;
                    FStreetAddr.StreetID = Q->FN("LEVEL5ID")->AsInteger;
                    FStreetAddr.StrObjType = sotStreet;
                    FStreetAddr.State = 1;
                    AddUnique(ARetCodes, FStreetAddr.AsString());
                    Q->Next();
                  }
                 RC = true;
               }
              if (AFullSearch)
               {
                 // ����� %.Name%
                 FSBText = "%.";
                 FQPrepare("SELECT LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID, LEVEL5ID FROM KLADR5 \
                                 WHERE UNAME LIKE '"+FSBText+"'||:pNAME||'%' AND LEVEL1ID = :pLEVEL1ID AND LEVEL2ID = :pLEVEL2ID \
                                 AND LEVEL3ID = :pLEVEL3ID AND LEVEL4ID = :pLEVEL4ID");

                 FName = AName->PreparedText.Trim();
                 if (FName.Length() > 40) FName.SetLength(40);
                 Q->ParamByName("pNAME")->AsString      = FName.UpperCase();
                 Q->ParamByName("pLEVEL1ID")->AsInteger = FStreetAddr.RegID;
                 Q->ParamByName("pLEVEL2ID")->AsInteger = FStreetAddr.Town1ID;
                 Q->ParamByName("pLEVEL3ID")->AsInteger = FStreetAddr.Town2ID;
                 Q->ParamByName("pLEVEL4ID")->AsInteger = FStreetAddr.Town3ID;
                 Q->ExecQuery();

                 if (Q->RecordCount)
                  {
                    while (!Q->Eof)
                     {
                       FStreetAddr.Clear();
                       FStreetAddr.RegID = Q->FN("LEVEL1ID")->AsInteger;
                       FStreetAddr.Town1ID = Q->FN("LEVEL2ID")->AsInteger;
                       FStreetAddr.Town2ID = Q->FN("LEVEL3ID")->AsInteger;
                       FStreetAddr.Town3ID = Q->FN("LEVEL4ID")->AsInteger;
                       FStreetAddr.StreetID = Q->FN("LEVEL5ID")->AsInteger;
                       FStreetAddr.StrObjType = sotStreet;
                       FStreetAddr.State = 1;
                       AddUnique(ARetCodes, FStreetAddr.AsString());
                       Q->Next();
                     }
                    RC |= true;
                  }
                 // ����� % Name%
                 FSBText = "% ";
                 FQPrepare("SELECT LEVEL1ID, LEVEL2ID, LEVEL3ID, LEVEL4ID, LEVEL5ID FROM KLADR5 \
                                 WHERE UNAME LIKE '"+FSBText+"'||:pNAME||'%' AND LEVEL1ID = :pLEVEL1ID AND LEVEL2ID = :pLEVEL2ID \
                                 AND LEVEL3ID = :pLEVEL3ID AND LEVEL4ID = :pLEVEL4ID");
                 FName = AName->PreparedText.Trim();
                 if (FName.Length() > 40) FName.SetLength(40);

                 Q->ParamByName("pNAME")->AsString      = FName.UpperCase();
                 Q->ParamByName("pLEVEL1ID")->AsInteger = FStreetAddr.RegID;
                 Q->ParamByName("pLEVEL2ID")->AsInteger = FStreetAddr.Town1ID;
                 Q->ParamByName("pLEVEL3ID")->AsInteger = FStreetAddr.Town2ID;
                 Q->ParamByName("pLEVEL4ID")->AsInteger = FStreetAddr.Town3ID;
                 Q->ExecQuery();

                 if (Q->RecordCount)
                  {
                    while (!Q->Eof)
                     {
                       FStreetAddr.Clear();
                       FStreetAddr.RegID = Q->FN("LEVEL1ID")->AsInteger;
                       FStreetAddr.Town1ID = Q->FN("LEVEL2ID")->AsInteger;
                       FStreetAddr.Town2ID = Q->FN("LEVEL3ID")->AsInteger;
                       FStreetAddr.Town3ID = Q->FN("LEVEL4ID")->AsInteger;
                       FStreetAddr.StreetID = Q->FN("LEVEL5ID")->AsInteger;
                       FStreetAddr.StrObjType = sotStreet;
                       FStreetAddr.State = 1;
                       AddUnique(ARetCodes, FStreetAddr.AsString());
                       Q->Next();
                     }
                    RC |= true;
                  }
               }
            }
         }
        catch (Exception &E)
         {
           throw ESearchException ("������ ������.\n��������� ���������:\n"+E.Message);
         }
        catch (...)
         {
           throw ESearchException ("Searching error");
         }
      }
   }
  __finally
   {
     Q->Close();
     if (Q->Transaction->Active) Q->Transaction->Rollback();
   }
  return RC;
}

//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::HouseTextToCodeList(UnicodeString AAdresStr, TNFAddrWords *AName, TStringList *ARetCodes)
{
  bool RC = false;
  try
   {
     if (DomVisible)
      {
        try
         {
           adresCode FHomeAddr = ParseAddrStr(AAdresStr);

           ParseHouseStruct(AName->Text, FHomeAddr.House, FHomeAddr.Vld, FHomeAddr.Korp, FHomeAddr.Build);
           AddUnique(ARetCodes, FHomeAddr.AsString());
           RC = true;
         }
        catch (Exception &E)
         {
           throw ESearchException ("������ ������.\n��������� ���������:\n"+E.Message);
         }
        catch (...)
         {
           throw ESearchException ("Searching error");
         }
      }
   }
  __finally
   {
   }
  return RC;
}

//---------------------------------------------------------------------------
bool __fastcall TKLADRFORM::FlatTextToCodeList(UnicodeString AAdresStr, TNFAddrWords *AName, TStringList *ARetCodes)
{
  bool RC = false;
  try
   {
     if (FlatVisible)
      {
        try
         {
           adresCode FFlatAddr = ParseAddrStr(AAdresStr);
           FFlatAddr.FlatObjType = fotFlat;
           FFlatAddr.Flat = AName->PreparedText.Trim();
           AddUnique(ARetCodes, FFlatAddr.AsString());
           RC = true;
         }
        catch (Exception &E)
         {
           throw ESearchException ("������ ������.\n��������� ���������:\n"+E.Message);
         }
        catch (...)
         {
           throw ESearchException ("Searching error");
         }
      }
   }
  __finally
   {
   }
  return RC;
}

//---------------------------------------------------------------------------
UnicodeString __fastcall TKLADRFORM::getStreetTypeText(TStreetObjectType AStreetObjType)
{
  UnicodeString RC = " ";
  try
   {
     for (StringIntMap::iterator i = FStreetTypeSocrObj.begin(); i != FStreetTypeSocrObj.end(); i++)
      {
        if (i->second == (int)(AStreetObjType))
         {
           RC = i->first;
           break;
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TKLADRFORM::getFlatTypeText(TFlatObjectType AFlatType)
{
  UnicodeString RC = "";
  try
   {
     for (StringIntMap::iterator i = FFlatTypeSocrObj.begin(); i != FFlatTypeSocrObj.end(); i++)
      {
        if (i->second == (int)AFlatType)
         {
           RC = i->first;
           break;
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FHintMouseEnter(TObject *Sender)
{
  if (FCanShowHint && !FHintShowing && ((TControl*)Sender)->Hint.Length())
   {
     FHintShowing = true;
     FHintCtrl->ShowHint(5, 5, "������ �����:", ((TControl*)Sender)->Hint);
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FHintMouseLeave(TObject *Sender)
{
  if (FHintShowing && FCanShowHint)
   {
     FHintShowing = false;
     FHintCtrl->HideHint();
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FHouseEnter(TObject *Sender)
{
  if (FCanShowHint)
   {
     FCanHideHint = false;
     TcxMaskEdit *ed = GetMaskEditCtrl("HouseMaskEdit");
     ShowHouseHint(FHintCtrl, ed->EditingValue, 5, 10);
     FHintShowing = true;
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FHouseExit(TObject *Sender)
{
  if (FCanShowHint)
   {
     FHintCtrl->HideHint();
     FHintShowing = false;
   }
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FSetStreetRequired(bool nv)
{
   if  (nv) FStreetVisible  = 1;
   FStreetRequired  = nv;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FSetStreetVisible(bool nv)
{
  if (!nv) FStreetRequired  = 0;
  FStreetVisible  = nv;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FSetHouseRequired(bool nv)
{
  if  (nv) FHouseVisible  = 1;
  FHouseRequired  = nv;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FSetHouseVisible(bool nv)
{
  if (!nv) FHouseRequired  = 0;
  FHouseVisible  = nv;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FSetFlatRequired(bool nv)
{
  if  (nv) FFlatVisible = 1;
  FFlatRequired = nv;
}
//---------------------------------------------------------------------------
void __fastcall TKLADRFORM::FSetFlatVisible(bool nv)
{
  if (!nv) FFlatRequired = 0;
  FFlatVisible = nv;
}
//---------------------------------------------------------------------------
namespace Kladr
{
    void __fastcall PACKAGE Register()
    {
         TComponentClass classes[1] = {__classid(TKLADRFORM)};
         RegisterComponents("ICS", classes, 0);
    }
}
//---------------------------------------------------------------------------

