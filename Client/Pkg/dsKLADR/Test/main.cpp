//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "dxSkinBlack"
#pragma link "dxSkinBlue"
#pragma link "dxSkinCaramel"
#pragma link "dxSkinCoffee"
#pragma link "dxSkinDarkRoom"
#pragma link "dxSkinDarkSide"
#pragma link "dxSkinFoggy"
#pragma link "dxSkinGlassOceans"
#pragma link "dxSkiniMaginary"
#pragma link "dxSkinLilian"
#pragma link "dxSkinLiquidSky"
#pragma link "dxSkinLondonLiquidSky"
#pragma link "dxSkinMcSkin"
#pragma link "dxSkinMoneyTwins"
#pragma link "dxSkinOffice2007Black"
#pragma link "dxSkinOffice2007Blue"
#pragma link "dxSkinOffice2007Green"
#pragma link "dxSkinOffice2007Pink"
#pragma link "dxSkinOffice2007Silver"
#pragma link "dxSkinOffice2010Black"
#pragma link "dxSkinOffice2010Blue"
#pragma link "dxSkinOffice2010Silver"
#pragma link "dxSkinPumpkin"
#pragma link "dxSkinsCore"
#pragma link "dxSkinsDefaultPainters"
#pragma link "dxSkinSeven"
#pragma link "dxSkinSharp"
#pragma link "dxSkinSilver"
#pragma link "dxSkinSpringTime"
#pragma link "dxSkinStardust"
#pragma link "dxSkinSummer2008"
#pragma link "dxSkinValentine"
#pragma link "dxSkinXmas2008Blue"
#pragma link "FIBDatabase"
#pragma link "pFIBDatabase"
#pragma resource "*.dfm"
TMainForm *MainForm;
//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
/*   KL = new TKLADRFORM(this);
   KL->Database = KLADRBase;
   KL->UseCodeToTextCache = true;
   KL->RegionEnabled      = true;
   KL->NasPunktEnabled    = true;
   KL->StreetEnabled      = true;
   KL->StreetRequired     = true;
   KL->StreetVisible      = true;
   KL->IndeksVisible      = true;
   KL->DomVisible         = true;
   KL->FlatVisible        = true;
   KL->CanShowHint        = true;
   KL->RequiredColor      = clYellow;*/
   FAddrCode = "";


        cxMaskEdit1->Properties->EditMask            = "(((([�-��-�]\\d?) | ((\\d+[�-��-�]?\\d?) (\\/(\\d+[�-��-�]?\\d?))?) | ((\\d+[�-��-�]?)-(\\d+[�-��-�]?)) | (\\d+-[�-��-�]))' ')?((���\\.' '( ([�-��-�]\\d?) | ((\\d+[�-��-�]?\\d?) (\\/(\\d+[�-��-�]?\\d?))?) | ((\\d+[�-��-�]?\\d?)-(\\d+[�-��-�]?\\d?))))' ')?((����\\.' '(([�-��-�]\\d?) | ((\\d+[�-��-�]?\\d?) (\\/(\\d+[�-��-�]?\\d?))?)))' ')?(���\\.' ' ( ([�-��-�]\\d?) | ((\\d+[�-��-�]?\\d?) (\\/(\\d+[�-��-�]?\\d?))?)))?)";
        cxMaskEdit1->Hint                            = "\
������ �����: [� ����] [��������] [������] [��������]\n \
\n\
\n� ����     :           ( �[�] | ((�+[�][�])(/�+[�][�])*)   | (�+[�]-(�+[�]) | �+-[�])' '\
\n� �������� :  (���.' ' ( �[�] | ((�+[�][�])(/(�+[�][�]))*) |  �+[�][�]-�+[�][�]))' '    \
\n� �������  :  (����.' '( �[�] | ((�+[�][�])(/(�+[�][�]))*)))' '                         \
\n� �������� :   ���.' ' ( �[�] | ((�+[�][�])(/(�+[�][�]))*))                             \
\n                                                                                        \
\n�������:                                                                                \
\n� ����     :   2, 2�, 23�5, �, �3, 67/19, 67/19�, 45�4/34�6, 15-19, 15-19�, 24-�        \
\n� �������� :   ���. 2, ���. 2�, ���. 23�5, ���. �, ���. �3, ���. 67/19, ���. 67/19�,\
\n               ���. 45�4/34�6, ���. 15-19, ���. 15-19�6, ���. 15�3-19�6 \
\n� �������  :   ����. 2, ����. 2�, ����. 23�5, ����. �, ����. �3, ����. 67/19, \
\n               ����. 67/19�, ����. 45�4/34�6                                   \
\n� �������� :   ���. 2, ���. 2�, ���. 23�5, ���. �, ���. �3, ���. 67/19, ���. 67/19�, ���. 45�4/34�6                                           \
\n                                                                                                                                              \
\n������� ������� � ����:                             \
\n��� 32                 32                           \
\n��� �                  �                            \
\n��� 1�                 1�                           \
\n��� 1-�                1-�                          \
\n��� 1�                 1�                           \
\n��� 21-25              21-25                        \
\n��� 5/34               5/34                         \
\n��� 5/34 ������ 1      5/34 ����. 1                 \
\n��� 6 ��������  2      6 ���. 2                     \
\n��� 5/2 �������� 2�    5/2 ���. 2�                  \
\n��������2�             ���. 2�                      \
\n������ 32              ����. 32                     \
\n������ �               ����. �                      \
\n������ 1�              ����. 1�                     \
\n������ 1-�             ����. 1�                     \
\n������ 5/34            ����. 5/34                   \
\n������ 6 �������� 2    ����. 6 ���. 2               \
\n������ 5/2 �������� 2� ����. 5/2 ���. 2�            \
\n������ 6 �������� 2_7  ����. 6 ���. 2/7             \
\n                                                    \
\n                                                    \
\n                                                    \
\n�����������:  �   - ����� 0-9, � - ����� �������� ��������,                  \
\n              []  - �������� �� ����������� ��� �����,                       \
\n              +   - ���� ��� ����� ��������,                                 \
\n              *   - ���� ��� ����� ��������,                                 \
\n              |   - ���� ���, ��������� ��������� �������� ������� �����,    \
\n              ' ' - ������.                                                  ";

        cxMaskEdit1->ShowHint                        = true;
DDShowHint = false;
/*
Formats[0] = "��?           { ������: �, �3 }",
Formats[1] = "�+�?�?         { ������: 3, 2�, 34, 45�, 7�5, 53�4 }",
Formats[2] = "�+�?�?/�+�?�?   { ������: 3/3, 3/2�, 3/34, 3/45�, 3/7�5, 3/53�4,\
\n                        2�/3, 2�/2�, 2�/34, 2�/45�, 2�/7�5, 2�/53�4,\
\n                        34/3, 34/2�, 34/34, 34/45�, 34/7�5, 34/53�4,\
\n                        45�/3, 45�/2�, 45�/34, 45�/45�, 45�/7�5, 45�/53�4,\
\n                        7�5/3, 7�5/2�, 7�5/34, 7�5/45�, 7�5/7�5, 7�5/53�4,\
\n                        53�4/3, 53�4/2�, 53�4/34, 53�4/45�, 53�4/7�5, 53�4/53�4\
              }",
Formats[3] = "�+�?-�+�?       { ������: 5-7, 5-16, 5-7�, 5-16,\
\n                        12-7, 12-16, 12-7�, 12-16�,\
\n                        5�-7, 5�-16, 5�-7�, 5�-16�,\
\n                        19�-7, 19�-16, 19�-7�, 19�-16�\
              }",
Formats[4] = "�+-�           { ������: 1-�, 23-� }",
Formats[5] = "�+�?�?-�+�?�?   { ������: 3-3, 3-2�, 3-34, 3-45�, 3-7�5, 3-53�4,\
\n                        2�-3, 2�-2�, 2�-34, 2�-45�, 2�-7�5, 2�-53�4,\
\n                        34-3, 34-2�, 34-34, 34-45�, 34-7�5, 34-53�4,\
\n                        45�-3, 45�-2�, 45�-34, 45�-45�, 45�-7�5, 45�-53�4,\
\n                        7�5-3, 7�5-2�, 7�5-34, 7�5-45�, 7�5-7�5, 7�5-53�4,\
\n                        53�4-3, 53�4-2�, 53�4-34, 53�4-45�, 53�4-7�5, 53�4-53�4\
               }",
*/

}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ExecuteBtnClick(TObject *Sender)
{
  KLADRFORM1->Execute(false,FAddrCode);
  FAddrCode = KLADRFORM1->adresCodeStr;
  AddrLab->Caption = KLADRFORM1->codeToText(FAddrCode);
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FormDestroy(TObject *Sender)
{
  delete KL;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::ExecuteTemplateBtnClick(TObject *Sender)
{
  KL->Execute(true,FAddrCode);
  FAddrCode = KL->adresCodeStr;
  AddrLab->Caption = KL->codeToText(FAddrCode);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button1Click(TObject *Sender)
{
  AddrLab->Caption = KLADRFORM1->codeToText("78000000000000000000000000000000");
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::cxMaskEdit1PropertiesValidate(TObject *Sender,
      Variant &DisplayValue, TCaption &ErrorText, bool &Error)
{
  Error = false;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
   if (Key == VK_RETURN)
    {
      ActiveControl->Owner->Name;
      ActiveControl->Parent->Name;
      cxMaskEdit1;
      int a = 0;
    }
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::cxMaskEdit1PropertiesEditValueChanged(
      TObject *Sender)
{
  int a = 1;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::cxHintStyleController1ShowHintEx(
      TObject *Sender, UnicodeString &Caption, UnicodeString &HintStr,
      bool &CanShow, THintInfo &HintInfo)
{
  int a = 1;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::cxHintStyleController1HintStyleChanged(
      TObject *Sender, TcxCustomHintStyle *AStyle)
{
  int a = 1;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::cxHintStyleController1ShowHint(TObject *Sender,
      UnicodeString &HintStr, bool &CanShow, THintInfo &HintInfo)
{
  int a = 1;
}
//---------------------------------------------------------------------------

