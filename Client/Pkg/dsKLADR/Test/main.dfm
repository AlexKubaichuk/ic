object MainForm: TMainForm
  Left = 523
  Top = 225
  ActiveControl = cxMaskEdit1
  BorderIcons = [biSystemMenu, biMinimize, biMaximize, biHelp]
  BorderStyle = bsDialog
  Caption = 'MainForm'
  ClientHeight = 203
  ClientWidth = 438
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object AddrLab: TLabel
    Left = 9
    Top = 112
    Width = 412
    Height = 80
    AutoSize = False
    WordWrap = True
  end
  object ExecuteBtn: TButton
    Left = 14
    Top = 10
    Width = 75
    Height = 25
    HelpType = htKeyword
    HelpKeyword = '568'
    Caption = 'Execute'
    TabOrder = 0
    OnClick = ExecuteBtnClick
  end
  object ExecuteTemplateBtn: TButton
    Left = 99
    Top = 9
    Width = 128
    Height = 25
    Caption = 'Execute template'
    TabOrder = 1
    OnClick = ExecuteTemplateBtnClick
  end
  object Button1: TButton
    Left = 240
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 2
    OnClick = Button1Click
  end
  object cxMaskEdit1: TcxMaskEdit
    Left = 22
    Top = 58
    HelpKeyword = '1235'
    Properties.HideSelection = False
    Properties.MaskKind = emkRegExprEx
    Properties.MaxLength = 0
    Properties.ValidateOnEnter = False
    Properties.OnEditValueChanged = cxMaskEdit1PropertiesEditValueChanged
    Properties.OnValidate = cxMaskEdit1PropertiesValidate
    TabOrder = 3
    Width = 214
  end
  object Edit1: TEdit
    Left = 290
    Top = 59
    Width = 121
    Height = 21
    Hint = 'r tert ert ert ert ert ertert'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    Text = 'Edit1'
  end
  object KLADRBase: TpFIBDatabase
    DBName = 'C:\Program Files (x86)\akdo\dp\DatV4\akdo_dp.gdb'
    DBParams.Strings = (
      'user_name=sysdba'
      'password=masterkey'
      'sql_role_name=')
    SQLDialect = 1
    Timeout = 0
    LibraryName = 'fbclient.dll'
    WaitForRestoreConnect = 0
    Left = 398
    Top = 6
  end
  object KLADRFORM1: TKLADRFORM
    Database = KLADRBase
    UseCodeToTextCache = True
    RegionEnabled = True
    NasPunktEnabled = True
    NasPunktRequired = True
    StreetEnabled = True
    StreetRequired = True
    StreetVisible = True
    DomVisible = True
    FlatVisible = True
    IndeksVisible = True
    RequiredColor = clInfoBk
    SetFilterHotKey = 120
    ClearFilterHotKey = 121
    SetFilterPict.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFF22FFFF
      FFFFFFFFFF222FFFFFFFFFFFFF2222FFFFFFFFFFFF22222FFFFFFFFFFF222222
      FFFFF222222222222FFFF2222222222222FFF22222222222222FF22222222222
      222FF2222222222222FFF222222222222FFFFFFFFF222222FFFFFFFFFF22222F
      FFFFFFFFFF2222FFFFFFFFFFFF222FFFFFFFFFFFFF22FFFFFFFF}
    ClearFilterPict.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFF99FF
      FFFFFFFFFFF999FFFFFFFFFFFF9999FFFFFFFFFFF99999FFFFFFFFFF999999FF
      FFFFFFF999999999999FFF9999999999999FF99999999999999FF99999999999
      999FFF9999999999999FFFF999999999999FFFFF999999FFFFFFFFFFF99999FF
      FFFFFFFFFF9999FFFFFFFFFFFFF999FFFFFFFFFFFFFF99FFFFFF}
    CanShowHint = True
    Left = 104
    Top = 99
  end
end
