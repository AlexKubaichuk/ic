//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "KLADRFORM.h"
#include "map.h"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "dxSkinBlack.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinCaramel.hpp"
#include "dxSkinCoffee.hpp"
#include "dxSkinDarkRoom.hpp"
#include "dxSkinDarkSide.hpp"
#include "dxSkinFoggy.hpp"
#include "dxSkinGlassOceans.hpp"
#include "dxSkiniMaginary.hpp"
#include "dxSkinLilian.hpp"
#include "dxSkinLiquidSky.hpp"
#include "dxSkinLondonLiquidSky.hpp"
#include "dxSkinMcSkin.hpp"
#include "dxSkinMoneyTwins.hpp"
#include "dxSkinOffice2007Black.hpp"
#include "dxSkinOffice2007Blue.hpp"
#include "dxSkinOffice2007Green.hpp"
#include "dxSkinOffice2007Pink.hpp"
#include "dxSkinOffice2007Silver.hpp"
#include "dxSkinOffice2010Black.hpp"
#include "dxSkinOffice2010Blue.hpp"
#include "dxSkinOffice2010Silver.hpp"
#include "dxSkinPumpkin.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinsDefaultPainters.hpp"
#include "dxSkinSeven.hpp"
#include "dxSkinSharp.hpp"
#include "dxSkinSilver.hpp"
#include "dxSkinSpringTime.hpp"
#include "dxSkinStardust.hpp"
#include "dxSkinSummer2008.hpp"
#include "dxSkinValentine.hpp"
#include "dxSkinXmas2008Blue.hpp"
#include "FIBDatabase.hpp"
#include "pFIBDatabase.hpp"
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TButton *ExecuteBtn;
        TpFIBDatabase *KLADRBase;
        TLabel *AddrLab;
        TButton *ExecuteTemplateBtn;
        TButton *Button1;
        TcxMaskEdit *cxMaskEdit1;
        TEdit *Edit1;
        TKLADRFORM *KLADRFORM1;
        void __fastcall ExecuteBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall ExecuteTemplateBtnClick(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall cxMaskEdit1PropertiesValidate(TObject *Sender,
          Variant &DisplayValue, TCaption &ErrorText, bool &Error);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall cxMaskEdit1PropertiesEditValueChanged(
          TObject *Sender);
        void __fastcall cxHintStyleController1ShowHintEx(TObject *Sender,
          UnicodeString &Caption, UnicodeString &HintStr, bool &CanShow,
          THintInfo &HintInfo);
        void __fastcall cxHintStyleController1HintStyleChanged(
          TObject *Sender, TcxCustomHintStyle *AStyle);
        void __fastcall cxHintStyleController1ShowHint(TObject *Sender,
          UnicodeString &HintStr, bool &CanShow, THintInfo &HintInfo);
private:	// User declarations
        bool DDShowHint;
        TKLADRFORM *KL;
        UnicodeString FAddrCode;
        int enterStyle;

public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
