//---------------------------------------------------------------------------
#include <DateUtils.hpp>
#pragma hdrstop
#include "BaseDocNomDef.h"
//#include "DocPeriod.h"
#include "PackPeriod.h"

//---------------------------------------------------------------------------
typedef map<const UnicodeString, UnicodeString> TStrMap;
TStrMap HashSel;
TStrMap ClassHashSel;
//---------------------------------------------------------------------------
#define flVal(a) AFields->Values[AFields->Names[a]]
//---------------------------------------------------------------------------
__fastcall TICSBaseDocNomDef::TICSBaseDocNomDef(TAutoFunc_DM *ADM)
{
  FDM = ADM;
}
//---------------------------------------------------------------------------
TGetAttrFunc __fastcall TICSBaseDocNomDef::GetAttrMethodAddr(UnicodeString AFuncName)
{
  TGetAttrFunc RC = NULL;

  if (AFuncName == "ADocAttrVal")          RC = ADocAttrVal;
  else if (AFuncName == "ASBOrg")          RC = ASBOrg;
  else if (AFuncName == "AYNVal")          RC = AYNVal;
  else if (AFuncName == "ADocSpecOwner")   RC = ADocSpecOwner;
  else if (AFuncName == "ADocPeriod")      RC = ADocPeriod;
  else if (AFuncName == "ADocPeriodicity") RC = ADocPeriodicity;
  else if (AFuncName == "ASetOrgCode")     RC = ASetOrgCode;
  else if (AFuncName == "ASetLPUName")     RC = ASetLPUName;
  else if (AFuncName == "ASetLPUFullName") RC = ASetLPUFullName;
  else if (AFuncName == "ASetAddr")        RC = ASetAddr;
  else if (AFuncName == "ASetOKPO")        RC = ASetOKPO;
  else if (AFuncName == "ASetSOATO")       RC = ASetSOATO;
  else if (AFuncName == "ASetOKVED")       RC = ASetOKVED;
  else if (AFuncName == "ASetOKATO")       RC = ASetOKATO;
  else if (AFuncName == "ASetPhone")       RC = ASetPhone;
  else if (AFuncName == "ASetEmail")       RC = ASetEmail;


  return RC;
}
//---------------------------------------------------------------------------
TGetDefFunc __fastcall TICSBaseDocNomDef::GetDefMethodAddr(UnicodeString AFuncName)
{
  TGetDefFunc RC = NULL;

  if (AFuncName == "DefDocDef")         RC = DefDocDef;
  else if (AFuncName == "DefSBOrg")          RC = DefSBOrg;
  else if (AFuncName == "DefDocSpecOwner")   RC = DefDocSpecOwner;
  else if (AFuncName == "DefDocPeriod")      RC = DefDocPeriod;
  else if (AFuncName == "DefDocPeriodicity") RC = DefDocPeriodicity;
  else if (AFuncName == "DefDocYNVal")       RC = DefDocYNVal;

  return RC;
}
//---------------------------------------------------------------------------
TGetSQLFunc __fastcall TICSBaseDocNomDef::GetSQLMethodAddr(UnicodeString AFuncName)
{
  TGetSQLFunc RC = NULL;

  if (AFuncName == "SQLDocDef")         RC = SQLDocDef;
  else if (AFuncName == "SQLSBOrg")          RC = SQLSBOrg;
  else if (AFuncName == "SQLDocSpecOwner")   RC = SQLDocSpecOwner;
  else if (AFuncName == "SQLDocPeriod")      RC = SQLDocPeriod;
  else if (AFuncName == "SQLDocPeriodicity") RC = SQLDocPeriodicity;
  else if (AFuncName == "SQLDocYNVal")       RC = SQLDocYNVal;


  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseDocNomDef::GetValIfNotEmpty(UnicodeString APref, UnicodeString AVal, UnicodeString APostf)
{
  UnicodeString RC = "";
  try
   {
     if (AVal.Trim().Length() && (AVal.Trim().ToIntDef(0)!= -1) )
      RC = APref+AVal.Trim()+APostf;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseDocNomDef::SetDay(int AYear, int AMonth, int ADay )
{
  UnicodeString RC = "";
  try
   {
     UnicodeString sm = IntToStr(AYear);
     sm = sm.SubString(sm.Length(),1);
     if (((sm == "1")||(sm == "2")||(sm == "3")||(sm == "4"))&&!((AYear<15)&&(AYear>10))) sm = "�. ";
     else                                                    sm = "�. ";
     if (AYear)   RC += IntToStr(AYear)+sm;
     if (AMonth)  RC += IntToStr(AMonth)+"�. ";
     if (ADay)    RC += IntToStr(ADay)+"�.";
     if (!(AYear+AMonth+ADay)) RC += "0�.";
   }
  __finally
   {
   }
  return RC.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseDocNomDef::AgeStr(TDateTime ACurDate, TDateTime ABirthDay)
{
  UnicodeString RC = "";
  try
   {
     if (ACurDate < ABirthDay)
      {
        RC = "-";
      }
     else
      {
        Word FYear,FMonth,FDay;
        DateDiff(ACurDate, ABirthDay, FDay, FMonth, FYear);
        RC = SetDay(FYear,FMonth,FDay);
      }
   }
  __finally
   {
   }
  return RC.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseDocNomDef::AgeStrY(TDateTime ACurDate, TDateTime ABirthDay)
{
  UnicodeString RC = "";
  try
   {
     if (ACurDate < ABirthDay)
      {
        RC = "-";
      }
     else
      {
        float DDD = (int)ACurDate - ((int)ABirthDay)-2;
        unsigned short YY=0,MM=0,DD=0;
        (ACurDate-ABirthDay+1).TDateTime::DecodeDate(&YY,&MM,&DD);  MM--;
        RC = FloatToStrF((DDD/365.25),ffFixed,16,2)+" "+" �.";
      }
   }
  __finally
   {
   }
  return RC.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseDocNomDef::FGetGUI(TTagNode *ANode)
{
  return ANode->GetRoot()->GetChildByName("passport")->AV["gui"];
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::FGetOrgParam(UnicodeString &ARet, UnicodeString AFlName)
{
  FDM->qtExec("Select "+AFlName+" From SUBORDORG Where Upper(CODE)='"+FDM->MainLPUCode.UpperCase()+"'",false);
  if (FDM->quFreeA->RecordCount)
   ARet = FDM->quFreeA->FN(AFlName)->AsString;
  else
   ARet = "";
#ifndef _ADO_
  if (FDM->trFreeA->Active) FDM->trFreeA->Commit();
#endif
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ASetOrgCode(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  FGetOrgParam(ARet, "CODE");
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ASetLPUName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  FGetOrgParam(ARet, "NAME");
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ASetLPUFullName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  FGetOrgParam(ARet, "FULLNAME");
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ASetAddr(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  FGetOrgParam(ARet, "ADDR");
  UnicodeString tmp = ARet.Trim();
  if (tmp.Length() > 30)
   {
     bool IsDigit = true;
     for (int i = 1; (i < tmp.Length())&&IsDigit; i++)
      {
        if (!((UnicodeString(tmp[i]).ToIntDef(-1) != -1)||(tmp[i] == '#')))
         IsDigit = false;
      }
     if (IsDigit)
      ARet = FDM->KLADR->codeToText(tmp);
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ASetOKPO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  FGetOrgParam(ARet, "OKPO");
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ASetSOATO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  FGetOrgParam(ARet, "SOATO");
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ASetOKVED(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  FGetOrgParam(ARet, "OKVED");
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ASetOKATO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  FGetOrgParam(ARet, "OKATO");
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ASetPhone(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  FGetOrgParam(ARet, "PHONE");
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ASetEmail(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  FGetOrgParam(ARet, "EMAIL");
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseDocNomDef::GetChildContAVDef(TTagNode *ANode, UnicodeString AVal, UnicodeString ADef)
{
  TTagNode *_tmp = ANode->GetFirstChild();
  UnicodeString _tmpAV;
  while (_tmp)
   {
     _tmpAV = _tmp->AV["name"];
     if (strstr(_tmpAV.c_str(),AVal.c_str()))  break;
     _tmp = _tmp->GetNext();
   }
  if (_tmp) return _tmp->GetAVDef("value",ADef);
  else      return ADef;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseDocNomDef::_GetPart2(UnicodeString ARef, char Sep)
{
  int pos = ARef.Pos(Sep);
  return ARef.SubString(pos+1,ARef.Length()-pos);
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::DecodePeriod(UnicodeString AScr, int &AYear, int &AMonth, int &ADay)
{
  AYear  = GetPart1(AScr,'/').ToInt();
  AMonth = GetPart1(_GetPart2(AScr,'/'),'/').ToInt();
  ADay   = GetPart2(_GetPart2(AScr,'/'),'/').ToInt();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseDocNomDef::AddPeriod(UnicodeString AScr1, UnicodeString AScr2)
{
  int YY1,YY2,MM1,MM2,DD1,DD2;
  DecodePeriod(AScr1,YY1,MM1,DD1);
  DecodePeriod(AScr2,YY2,MM2,DD2);
  DD1 += DD2;             DD2 = 0;
  if (DD1>31) {DD1 -= 31; DD2 = 1;}
  MM1 += MM2 + DD2; MM2 = 0;
  if (MM1>12) {MM1 -= 12; MM2 = 1;}
  YY1 += YY2 + MM2;
  return IntToStr(YY1)+"/"+IntToStr(MM1)+"/"+IntToStr(DD1);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseDocNomDef::FIncPeriod(UnicodeString AFrDate,UnicodeString APer, int IsSub)
{
  TDateTime RC = TDate(0);
  try
   {
     int FYears, FMonths, FDays;
     DecodePeriod(APer, FYears, FMonths, FDays);
     if (IsSub)
      {
       FYears  *= -1;
       FMonths *= -1;
       FDays   *= -1;
      }
     RC = Dateutils::IncYear
     (
       Sysutils::IncMonth
       (
         Dateutils::IncDay(StrToDate(AFrDate),FDays),
         FMonths
       ),
       FYears
     );
   }
  __finally
   {
   }
  return RC.FormatString("dd.mm.yyyy");
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseDocNomDef::FGetRefValue(TTagNode *ANode, UnicodeString ARef1,UnicodeString ARef2)
{
  if (ANode->GetParent("domain")->GetNext()) return ARef1;
  else                                       return ARef2;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::GetSQLValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   UnicodeString ISTab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   ATab = ISTab;
   if (!AIE->Count)
    {
      if (FIS->AV["tabalias"].Length())
       ARet = FIS->AV["tabalias"]+"."+FIS->AV["keyfield"]+"="+FIS->AV["tabalias"]+"."+FIS->AV["keyfield"];
      else
       ARet = FIS->AV["maintable"]+"."+FIS->AV["keyfield"]+"="+FIS->AV["maintable"]+"."+FIS->AV["keyfield"];
    }
   else
    {
      bool  IsExtSel = false;
      if (FISCondRules->GetChildByName("root"))
       IsExtSel = (bool)(FISCondRules->GetChildByName("root")->AV["extsel"].ToIntDef(0));
      TTagNode *_tmp = AIE->GetChildByAV("text","name","sql");
      if (_tmp)
       {
         if (IsExtSel)
          {
            if (_tmp->Count)
             {
               TReplaceFlags rFlag;
               rFlag << rfReplaceAll;
               TTagNode *subtmp;
               UnicodeString FSQL = _tmp->AV["value"];
               UnicodeString FSubSQL;
               int idx = 0;
               subtmp = _tmp->GetFirstChild();
               while (subtmp)
                {
                  UnicodeString SelRC = "";
                  FSubSQL = subtmp->AV["value"];
                  TStrMap::iterator rc = HashSel.find(FSubSQL);
                  if (rc == HashSel.end())
                   {
                     SelRC = "-1";
                     FDM->qtExec(FSubSQL,false);
                     while (!FDM->quFreeA->Eof)
                      {
                        SelRC += ","+FDM->quFreeA->FN("CODE")->AsString;
                        FDM->quFreeA->Next();
                      }
                     HashSel[FSubSQL] = SelRC;
                   }
                  FSQL = StringReplace(FSQL, "_SEL"+IntToStr(idx), HashSel[FSubSQL], rFlag);
                  idx++;
                  subtmp = subtmp->GetNext();
                }
               ARet = FSQL;
             }
            else
             ARet = _tmp->AV["value"];
          }
         else
          ARet = _tmp->AV["value"];
       }
      else      ARet = "";
    }
}
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#                   ��������� ��������� �������� ���������                   #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ADocAttrVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = AFields->Values[ARef];
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ASBOrg(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Count)
   {
     int FCode = GetPart2(AFields->Strings[0],'=').ToIntDef(-1);
     switch (FCode)
      {
       case 0: {ARet = "�����������"; break;}
       case 1: {ARet = "�����������"; break;}
       case 2: {ARet = "����"; break;}
      }
   }
  else
   ARet = "";
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::AYNVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Values[AFields->Names[0]] == "1")  ARet = "��";
  else                                            ARet = "���";
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ADocSpecOwner(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  FDM->qtExec("Select Name From SUBORDORG Where Upper(CODE)='"+AFields->Values[AFields->Names[0]].UpperCase()+"'",false);
  if (FDM->quFreeA->RecordCount)
   ARet = FDM->quFreeA->FN("Name")->AsString;
  else
   ARet = "";
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ADocPeriod(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = GetDocPeriodValStr(AFields->Values[AFields->Names[0]], AFields->Values[AFields->Names[1]], AFields->Values[AFields->Names[2]]);
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::ADocPeriodicity(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = "";
  switch (AFields->Values[AFields->Names[0]].ToIntDef(-1))
   {
     case 0: { ARet = "�����������"; break;}
     case 1: { ARet = "����������"; break;}
     case 2: { ARet = "�����"; break;}
     case 3: { ARet = "�������"; break;}
     case 4: { ARet = "���������"; break;}
     case 5: { ARet = "���"; break;}
   }
}
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#            ��������� ��������� �������� �������������� ��������� (Def...)  #
//#                                                                            #
//#            ��������� ������������ ������ SQL-�������             (SQL...)  #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
bool __fastcall TICSBaseDocNomDef::DefDatePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   // AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "����"
   // {
   //   1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
   //                            1,2 ��� �������; 0 - �������� ���
   //                                             1 - ������ �� ������� ����
   //                                             2 - ������ �� ����
   //   2-� - ���� ��;   ��� ���� "�������� ���"
   //                    ��� ����� "������ �� ..." �������� �������
   //   3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
   //                    ��� ���� "������ �� ������� ����"  - �� �������������;
   //   4-� - �������� - ��� ���� "�������� ���"  - �� �������������
   //         �������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
   if (!FDM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (NeedKonkr)
    {
      TDateConForm *Dlg = NULL;
      try
       {
         Dlg = new TDateConForm(Application->Owner);
         int FSubType;
         UnicodeString FSubTypeCapt = "";
         TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
         if (FISCondRules->GetParent("IS") == FISCondRules->GetParent("domain")->GetFirstChild()) // ���� ��������
          FSubType = 0;
         else  // ���� ������������
          {
            FSubType = 1;
            if (!FISAttr->AV["add_ref"].Length())
             FSubTypeCapt = "������ ��������� ������������";
            else
             {
               if (FISAttr->AV["add_ref"].Pos("#"))
                {
                  if (GetPart2(FISAttr->AV["add_ref"],'#').ToIntDef(0))
                   FSubTypeCapt = GetPart1(FISAttr->AV["add_ref"],'#');
                  else
                   {
                     FSubType = 0;
                     FSubTypeCapt = "";
                   }
                }
               else
                FSubTypeCapt = FISAttr->AV["add_ref"];
             }
          }
         bool DoNotDop = FISAttr->CmpAV("ref","4031DA5E-8AC52FB3-7D03.1024");
         Dlg->SetType(_scDate,AIE,FSubType,FSubTypeCapt,DoNotDop);
         Dlg->MsgLab->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count)
       {
         tmp = FISCondRules->AV["name"]+" = ";
         __tmp = AIE->GetFirstChild();
         int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
         bool FLast = (bool)(DType&1);
         DType = DType >> 1;
         if (FLast)
          {
            TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
            if (!FISAttr->AV["add_ref"].Length())
             tmp = "������ ��������� ������������";
            else
             tmp = FISAttr->AV["add_ref"];
          }
         TTagNode *DateType   = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam1 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam2 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam3 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam4 = __tmp;
         if (DType == 0)
          {// �������� ���
            if ((DateParam1 && (DateParam1->AV["value"].Length())) ||
                (DateParam2 && (DateParam2->AV["value"].Length())))
             {
               tmp += " "+DateType->AV["name"];
               if (DateParam1->AV["value"].Length())
                {
                  tmp += " "+DateParam1->AV["name"];
                  tmp += "='"+DateParam1->AV["value"]+"'";
                }
               if (DateParam2->AV["value"].Length())
                {
                  tmp += " "+DateParam2->AV["name"];
                  tmp += "='"+DateParam2->AV["value"]+"'";
                }
             }
          }
         else if (DType >= 3)
          { // 3 - ������ ������������ ���������
            // 4 - ������ ���� - ���� ��������� ������� ������������ ���������
            // 5 - ������ ��������� - ���� ��������� ������� ������������ ���������
            // 6 - ������ �������� - ���� ��������� ������� ������������ ���������
            // 7 - ������ ������ - ���� ��������� ������� ������������ ���������

            tmp += " "+DateType->AV["name"];
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         else
          {// ������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
            tmp += " "+DateType->AV["name"];
            if (DateParam1->AV["value"].Length())
             {
               tmp += " "+DateParam1->AV["name"];
               tmp += "='"+DateParam1->AV["value"]+"'";
             }
            if (DateParam2->AV["value"].Length())
             {
               tmp += " "+DateParam2->AV["name"];
               tmp += "='"+DateParam2->AV["value"]+"'";
             }
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         if (DateParam4)
          {
            tmp += " "+DateParam4->AV["name"];
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::SQLDatePeriodLast(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   TTagNode *DateOffs, *__tmp;
   ATab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   UnicodeString xFr,xTo,flName,xPer,xOffs,xTabName,xMaxSQL,xMaxTabName,MaxflName;
   xFr = ""; xTo = "";
   xTabName = FIS->AV["maintable"];
   flName = FIS->AV["tabalias"]+"."+FISAttr->AV["fields"];
   int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
   bool FLast = (bool)(DType&1);
   DType = DType >> 1;
   TTagNode *TagFr, *TagTo;
   __tmp = AIE->GetFirstChild()->GetNext();
   TagFr = __tmp; __tmp = __tmp->GetNext();
   TagTo = __tmp; __tmp = __tmp->GetNext();
   DateOffs = __tmp; __tmp = __tmp->GetNext();
   xMaxTabName = "DTT"+IntToStr((int)FLast);
   xMaxSQL =  flName+" = (Select Max("+xMaxTabName+"."+FISAttr->AV["fields"]+") From "+xTabName+" "+xMaxTabName;
   MaxflName = xMaxTabName+"."+FISAttr->AV["fields"];
   if (DType == 0)
    {// �������� ���
      if (TagFr && (TagFr->AV["value"].Length()))
       xFr = ">='"+_CorrectDate(TagFr->AV["value"])+"'";
      if (TagTo && (TagTo->AV["value"].Length()))
       xTo = "<='"+_CorrectDate(TagTo->AV["value"])+"'";
    }
   else if (DType == 1)
    {// ������ �� ������� ����
      xPer = TagFr->AV["value"];
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
         xFr = ">='"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),xPer,1)+"'";
         xTo = "<='"+Date().FormatString("dd.mm.yyyy")+"'";
       }
      else
       {
         xFr = ">='"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),AddPeriod(xPer,xOffs),1)+"'";
         xTo = "<='"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),xOffs,1)+"'";
       }
    }
   else if (DType == 2)
    {// ������ �� ���� ��������� ������� ������������ ���������
      UnicodeString FDocPer = GetPart1(UnicodeString(ARet),'#');
      if (FDocPer.Length())
       {
         if (GetPart2(FDocPer,';').Length())
          FDocPer = GetPart2(FDocPer,';');
         else
          FDocPer = Date().FormatString("dd.mm.yyyy");
       }
      else
       FDocPer = Date().FormatString("dd.mm.yyyy");
      xPer  = TagFr->AV["value"];
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
         xFr = ">='"+FIncPeriod(FDocPer,xPer,1)+"'";
         xTo = "<='"+FDocPer+"'";
       }
      else
       {
         xFr = ">='"+FIncPeriod(FDocPer,AddPeriod(xPer,xOffs),1)+"'";
         xTo = "<='"+FIncPeriod(FDocPer,xOffs,1)+"'";
       }
    }
   else if (DType >= 3)
    { // 3 - ������ ������������ ���������
      // 4 - ������ ���� - ���� ��������� ������� ������������ ���������
      // 5 - ������ ��������� - ���� ��������� ������� ������������ ���������
      // 6 - ������ �������� - ���� ��������� ������� ������������ ���������
      // 7 - ������ ������ - ���� ��������� ������� ������������ ���������
      UnicodeString FPer = GetPart1(UnicodeString(ARet),'#');
      if (FPer.Length())
       {
         xOffs = DateOffs->AV["value"];
         if (!xOffs.Length())
          {
            if (GetPart1(FPer,';').Length())
             xFr = ">='"+TDate(GetPart1(FPer,';')).FormatString("dd.mm.yyyy")+"'";
            if (GetPart2(FPer,';').Length())
             xTo = "<='"+TDate(GetPart2(FPer,';')).FormatString("dd.mm.yyyy")+"'";
          }
         else
          {
            if (GetPart1(FPer,';').Length())
             xFr = ">='"+FIncPeriod(GetPart1(FPer,';'),xOffs,1)+"'";
            if (GetPart2(FPer,';').Length())
             xTo = "<='"+FIncPeriod(GetPart2(FPer,';'),xOffs,1)+"'";
          }
       }
    }
   if (xFr.Length())
    {
      xMaxSQL += " and "+MaxflName+xFr;
      xFr = flName+xFr;
    }
   if (xTo.Length())
    {
      xMaxSQL += " and "+MaxflName+xTo;
      xTo = flName+xTo;
    }
   if (xFr.Length() && xTo.Length())
    {
      if (FLast)
       ARet = "("+xFr+" and "+xTo+" and "+xMaxSQL+")";
      else
       ARet = "("+xFr+" and "+xTo+")";
    }
   else if (xFr.Length())
    {
      if (FLast)
       ARet = "("+xFr+" and "+xMaxSQL+")";
      else
       ARet = xFr;
    }
   else if (xTo.Length())
    {
      if (FLast)
       ARet = "("+xTo+" and "+xMaxSQL+")";
      else
       ARet = xTo;
    }
   else
    {
      if (FLast)
       ARet = xMaxSQL;
      else
       ARet = "";
    }
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::GetAtttInfo(UnicodeString AFuncName, TStringList *AFields, UnicodeString ARef, UnicodeString &ARet, UnicodeString AAttrRef)
{
  TGetAttrFunc FFunc = GetAttrMethodAddr(AFuncName);

  if (FFunc)
   FFunc(AFields, ARef, ARet, AAttrRef);
  else
   throw Exception(("������ ������ ������� � ������ '"+AFuncName+"'").c_str());
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseDocNomDef::GetDefInfo(UnicodeString AFuncName, TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
  TGetDefFunc FFunc = GetDefMethodAddr(AFuncName);

  if (FFunc)
   return FFunc(AIE, AText, NeedKonkr, AXMLIE);
  else
   throw Exception(("������ ������ ������� � ������ '"+AFuncName+"'").c_str());
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::GetSQLInfo(UnicodeString AFuncName, TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  TGetSQLFunc FFunc = GetSQLMethodAddr(AFuncName);
  if (FFunc)
   FFunc(AIE, ATab, ARet);
  else
   throw Exception(("������ ������ ������� � ������ '"+AFuncName+"'").c_str());
}
//---------------------------------------------------------------------------



bool __fastcall TICSBaseDocNomDef::DefDocDef(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
  if (!FDM) return false;
  bool RC = false;
//   if (NeedKonkr)
//    {
  TStringList *FValues = NULL;
  TStringList *FValues2 = NULL;
  try
   {
      FValues = new TStringList;
      FValues2 = new TStringList;

      FDM->qtExec("Select GUI, NAME From SPECIFIKATORS Order by NAME");
      int idx = 1;
      while (!FDM->quFreeA->Eof)
       {
         FValues->AddObject(FDM->quFreeA->FN("name")->AsString,(TObject*)idx);
         FValues2->AddObject(FDM->quFreeA->FN("GUI")->AsString.UpperCase(),(TObject*)idx);
         idx++;
         FDM->quFreeA->Next();
       }
      RC = FDM->DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues, FValues2);
   }
  __finally
   {
     if (FValues) delete FValues;
     if (FValues2) delete FValues2;
   }
//    }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::SQLDocDef(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseDocNomDef::DefSBOrg(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
  if (!FDM) return false;
  bool RC = false;
//   if (NeedKonkr)
//    {
  TStringList *FValues = NULL;
  try
   {
      FValues = new TStringList;

      FValues->AddObject("�����������",(TObject*)0);
      FValues->AddObject("�����������",(TObject*)1);
      FValues->AddObject("����",(TObject*)2);

      RC = FDM->DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
   }
  __finally
   {
     if (FValues) delete FValues;
   }
//    }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::SQLSBOrg(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseDocNomDef::DefDocSpecOwner(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
  if (!FDM) return false;
  bool RC = false;
  TStringList *FValues = NULL;
  TStringList *FValues2 = NULL;
  try
   {
      FValues = new TStringList;
      FValues2 = new TStringList;
      FDM->qtExec("Select code, Name From SUBORDORG Order by NAME");
      int idx = 1;
      while (!FDM->quFreeA->Eof)
       {
         FValues->AddObject(FDM->quFreeA->FN("name")->AsString,(TObject*)idx);
         FValues2->AddObject(FDM->quFreeA->FN("code")->AsString.UpperCase(),(TObject*)idx);
         idx++;
         FDM->quFreeA->Next();
       }
      RC = FDM->DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues, FValues2);
   }
  __finally
   {
     if (FValues) delete FValues;
     if (FValues2) delete FValues2;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::SQLDocSpecOwner(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseDocNomDef::DefDocPeriod(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
/*
   // AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "����"
   // {
   //   1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
   //                            1,2 ��� �������; 0 - �������� ���
   //                                             1 - ������ �� ������� ����
   //                                             2 - ������ �� ����
   //   2-� - ���� ��;   ��� ���� "�������� ���"
   //                    ��� ����� "������ �� ..." �������� �������
   //   3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
   //                    ��� ���� "������ �� ������� ����"  - �� �������������;
   //   4-� - �������� - ��� ���� "�������� ���"  - �� �������������
   //         �������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
   if (!FDM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (AIE->GetFirstChild())
    {
      if (AIE->Count)
       {
         tmp = FISCondRules->AV["name"]+" = ";
         __tmp = AIE->GetFirstChild();
         int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
         bool FLast = (bool)(DType&1);
         DType = DType >> 1;
         if (FLast)
          {
            TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
            if (!FISAttr->AV["add_ref"].Length())
             tmp = "������ ��������� ������������";
            else
             tmp = FISAttr->AV["add_ref"];
          }
         TTagNode *DateType   = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam1 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam2 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam3 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam4 = __tmp;
         if (DType == 0)
          {// �������� ���
            if ((DateParam1 && (DateParam1->AV["value"].Length())) ||
                (DateParam2 && (DateParam2->AV["value"].Length())))
             {
               tmp += " "+DateType->AV["name"];
               if (DateParam1->AV["value"].Length())
                {
                  tmp += " "+DateParam1->AV["name"];
                  tmp += "='"+DateParam1->AV["value"]+"'";
                }
               if (DateParam2->AV["value"].Length())
                {
                  tmp += " "+DateParam2->AV["name"];
                  tmp += "='"+DateParam2->AV["value"]+"'";
                }
             }
          }
         else if (DType >= 3)
          { // 3 - ������ ������������ ���������
            // 4 - ������ ���� - ���� ��������� ������� ������������ ���������
            // 5 - ������ ��������� - ���� ��������� ������� ������������ ���������
            // 6 - ������ �������� - ���� ��������� ������� ������������ ���������
            // 7 - ������ ������ - ���� ��������� ������� ������������ ���������

            tmp += " "+DateType->AV["name"];
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         else
          {// ������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
            tmp += " "+DateType->AV["name"];
            if (DateParam1->AV["value"].Length())
             {
               tmp += " "+DateParam1->AV["name"];
               tmp += "='"+DateParam1->AV["value"]+"'";
             }
            if (DateParam2->AV["value"].Length())
             {
               tmp += " "+DateParam2->AV["name"];
               tmp += "='"+DateParam2->AV["value"]+"'";
             }
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         if (DateParam4)
          {
            tmp += " "+DateParam4->AV["name"];
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
*/
  if (!FDM) return false;
  bool RC = false;
  TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);

  if (NeedKonkr)
   {
     TPackPeriodForm *Dlg = NULL;
     try
      {
        UnicodeString FPeriodDate = "";
        TDate FPeriodDateFr = 0;
        TDate FPeriodDateTo = 0;

//        short int FPerType = 0;
        Dlg = new TPackPeriodForm(Application->Owner);
        Dlg->ShowModal();
        if (Dlg->ModalResult == mrOk)
         {
           if (AIE->Count) AIE->DeleteChild();

           AIE->AddChild("text","name=type,value="+IntToStr(Dlg->Type)+",ref=0");
           AIE->AddChild("text","name=subtype,value="+IntToStr(Dlg->SubType)+",ref=1");
           AIE->AddChild("text","name=typestr ,value="+Dlg->TypeStr+",ref=2");
           AIE->AddChild("text","name=subtypestr ,value="+Dlg->SubTypeStr+",ref=3");
           AIE->AddChild("text","name=���,value="+Dlg->Year+",ref=4");
           AIE->AddChild("text","name=��,value="+Dlg->DateFr+",ref=5");
           AIE->AddChild("text","name=��,value="+Dlg->DateTo+",ref=6");
           AIE->AddChild("text","name=��������,value="+Dlg->Offset+",ref=7");
         }
      }
     __finally
      {
        if (Dlg) delete Dlg;
      }
   }
  if (AIE->GetFirstChild())
   {
     if (AIE->Count)
      {
        UnicodeString tmp = FISCondRules->AV["name"]+" = ";
        TTagNode *__tmp = AIE->GetFirstChild();
//        int FPType = __tmp->AV["value"].ToIntDef(0);
        __tmp = __tmp->GetNext();
        __tmp = __tmp->GetNext();

        tmp += __tmp->AV["value"];
        __tmp = __tmp->GetNext();
        if (__tmp->AV["value"].Length())
         tmp += ", "+__tmp->AV["value"];
        __tmp = __tmp->GetNext();
        while (__tmp)
         {
           if (__tmp->AV["value"].Length())
            tmp += ", "+__tmp->AV["name"]+": "+__tmp->AV["value"];
           __tmp = __tmp->GetNext();
         }

        AText = tmp.Trim();
      }
     RC = true;
   }
  AXMLIE = AIE->AsXML;
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::SQLDocPeriod(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode *FIS = FISAttr->GetParent("is");

  
  ATab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";

  UnicodeString FYear, FDateFr, FDateTo, FOffset;
  FYear = FDateFr = FDateTo = FOffset = "";
  int FType, FSType;
  FType = FSType = -1;
  if (AIE->Count)
   {
     FType   = AIE->GetChildByAV("text","ref","0")->AV["value"].ToIntDef(0);
     FSType  = AIE->GetChildByAV("text","ref","1")->AV["value"].ToIntDef(0);
     FYear   = AIE->GetChildByAV("text","ref","4")->AV["value"].Trim();
     FDateFr = AIE->GetChildByAV("text","ref","5")->AV["value"].Trim();
     FDateTo = AIE->GetChildByAV("text","ref","6")->AV["value"].Trim();
     FOffset = AIE->GetChildByAV("text","ref","7")->AV["value"].Trim();
   }
  UnicodeString FPer = GetPart1(UnicodeString(ARet),'#');
  if (FPer.Length())
   {
     TDate FDocPerDateFr, FDocPerDateTo;
     FDocPerDateFr = FDocPerDateTo = TDate(0);
     if (GetPart1(FPer,';').Length())
       FDocPerDateFr = TDate(GetPart1(FPer,';'));
     if (GetPart2(FPer,';').Length())
       FDocPerDateTo = TDate(GetPart2(FPer,';'));

     int FDocPer  = GetPeriodValue(FType, FSType, FYear, FDateFr, FDateTo,FOffset,
                             GetPart2(UnicodeString(ARet),'#').ToIntDef(-1), FDocPerDateFr, FDocPerDateTo);

     UnicodeString TA = FIS->AV["tabalias"]+".";
     ARet = "("+TA+"F_PERIOD = "+IntToStr(FDocPer)+" and "+TA+"F_FROMDATE='"+FDocPerDateFr.FormatString("dd.mm.yyyy")+"' and "+TA+"F_TODATE='"+FDocPerDateTo.FormatString("dd.mm.yyyy")+"')";
   }
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseDocNomDef::DefDocPeriodicity(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
  if (!FDM) return false;
  bool RC = false;
  TStringList *FValues = NULL;
  try
   {
     FValues = new TStringList;
     FValues->AddObject("�����������",(TObject*)0);
     FValues->AddObject("����������",(TObject*)1);
     FValues->AddObject("�����",(TObject*)2);
     FValues->AddObject("�������",(TObject*)3);
     FValues->AddObject("���������",(TObject*)4);
     FValues->AddObject("���",(TObject*)5);

     RC = FDM->DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
   }
  __finally
   {
     if (FValues) delete FValues;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::SQLDocPeriodicity(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseDocNomDef::DefDocYNVal(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
  if (!FDM) return false;
  bool RC = false;
  TStringList *FValues = NULL;
  try
   {
     FValues = new TStringList;
     FValues->AddObject("���",(TObject*)0);
     FValues->AddObject("��",(TObject*)1);

     RC = FDM->DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
   }
  __finally
   {
     if (FValues) delete FValues;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseDocNomDef::SQLDocYNVal(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------

