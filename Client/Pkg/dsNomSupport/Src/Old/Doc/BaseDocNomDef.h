//---------------------------------------------------------------------------

#ifndef BaseDocNomDefH
#define BaseDocNomDefH
//---------------------------------------------------------------------------
#include <vcl.h>
#include "Dmf_Auto.h"
#include "DATEUTIL.hpp"
#include "DateConkretiz.h"
#include "TextConcretiz.h"

//#include "pgmsetting.h"
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#                   ��������� ��������� �������� ���������                   #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
#define _CorrectDate(a) TDateTime(a).FormatString("dd.mm.yyyy")
//---------------------------------------------------------------------------
typedef void __fastcall (__closure *TGetAttrFunc)(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
typedef bool __fastcall (__closure *TGetDefFunc)(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
typedef void __fastcall (__closure *TGetSQLFunc)(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
class TICSBaseDocNomDef : public TObject
{
private:
    UnicodeString __fastcall SetDay(int AYear, int AMonth, int ADay );
    UnicodeString __fastcall _GetPart2(UnicodeString ARef, char Sep);
    void __fastcall DecodePeriod(UnicodeString AScr, int &AYear, int &AMonth, int &ADay);
    void __fastcall FGetOrgParam(UnicodeString &ARet, UnicodeString AFlName);
protected:
    TAutoFunc_DM   *FDM;
    UnicodeString __fastcall GetValIfNotEmpty(UnicodeString APref, UnicodeString AVal, UnicodeString APostf = "");
    UnicodeString __fastcall FGetGUI(TTagNode *ANode);
    UnicodeString __fastcall AgeStr(TDateTime ACurDate, TDateTime ABirthDay);
    UnicodeString __fastcall AgeStrY(TDateTime ACurDate, TDateTime ABirthDay);
    UnicodeString __fastcall FIncPeriod(UnicodeString AFrDate,UnicodeString APer, int IsSub);
    UnicodeString __fastcall AddPeriod(UnicodeString AScr1, UnicodeString AScr2);
    UnicodeString __fastcall FGetRefValue(TTagNode *ANode, UnicodeString ARef1,UnicodeString ARef2);
    void __fastcall GetSQLValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    UnicodeString __fastcall GetChildContAVDef(TTagNode *ANode, UnicodeString AVal, UnicodeString ADef);
public:
    void __fastcall ADocAttrVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall GetAtttInfo(UnicodeString AFuncName, TStringList *AFields, UnicodeString ARef, UnicodeString &ARet, UnicodeString AAttrRef);
    bool __fastcall GetDefInfo(UnicodeString AFuncName, TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall GetSQLInfo(UnicodeString AFuncName, TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    virtual TGetAttrFunc __fastcall GetAttrMethodAddr(UnicodeString AFuncName);
    virtual TGetDefFunc __fastcall GetDefMethodAddr(UnicodeString AFuncName);
    virtual TGetSQLFunc __fastcall GetSQLMethodAddr(UnicodeString AFuncName);

__published:

    void __fastcall ASetOrgCode(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetLPUName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetLPUFullName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetAddr(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetOKPO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetSOATO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetOKVED(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetOKATO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetPhone(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetEmail(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASBOrg(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AYNVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ADocSpecOwner(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ADocPeriod(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ADocPeriodicity(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);

    bool __fastcall DefDatePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall SQLDatePeriodLast(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);

    bool __fastcall DefDocDef(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall SQLDocDef(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefSBOrg(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall SQLSBOrg(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);

    bool __fastcall DefDocSpecOwner(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall SQLDocSpecOwner(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefDocPeriod(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall SQLDocPeriod(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefDocPeriodicity(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall SQLDocPeriodicity(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefDocYNVal(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall SQLDocYNVal(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);

    __fastcall TICSBaseDocNomDef(TAutoFunc_DM *ADM);
    __fastcall ~TICSBaseDocNomDef(){};
};
#endif






