// AKDOREPORTIMPL.H : Declaration of the TAKDOReportImpl

#ifndef ICSDocExImplH
#define ICSDocExImplH

#define ATL_APARTMENT_THREADED

#include "ICSNomSupport_TLB.h"
#include "Dmf_Auto.h"
#include "BaseDocNomDef.h"


/////////////////////////////////////////////////////////////////////////////
// TAKDOReportImpl     Implements IAKDOReport, default interface of AKDOReport
// ThreadingModel : Neutral
// Dual Interface : TRUE
// Event Support  : FALSE
// Default ProgID : ICSRSys.AKDOReport
// Description    : AKDO personal report system
/////////////////////////////////////////////////////////////////////////////
class ATL_NO_VTABLE TICSDocExSupImpl :
  public CComObjectRootEx<CComMultiThreadModel>,
  public CComCoClass<TICSDocExSupImpl, &CLSID_ICSDocExSup>,
  public IDispatchImpl<IICSDocExSup, &IID_IICSDocExSup, &LIBID_ICSNomSupport>
{
private:
  TAutoFunc_DM   *MDM;
  TICSBaseDocNomDef *FuncDef;
public:
  TICSDocExSupImpl()
  {
    MDM = new TAutoFunc_DM;
    FuncDef = NULL;
  }
  ~TICSDocExSupImpl()
  {
    delete MDM;
  }
  // Data used when registering Object
  //
  DECLARE_THREADING_MODEL(otNeutral);
  DECLARE_PROGID("ICSNomSupport.ICSDocExSup");
  DECLARE_DESCRIPTION("ICS DocEx system");

  // Function invoked to (un)register object
  //
  static HRESULT WINAPI UpdateRegistry(BOOL bRegister)
  {
    TTypedComServerRegistrarT<TICSDocExSupImpl>
    regObj(GetObjectCLSID(), GetProgID(), GetDescription());
    return regObj.UpdateRegistry(bRegister);
  }


BEGIN_COM_MAP(TICSDocExSupImpl)
  COM_INTERFACE_ENTRY(IICSDocExSup)
  COM_INTERFACE_ENTRY2(IDispatch, IICSDocExSup)
END_COM_MAP()

// IAKDOReport
public:

  STDMETHOD(get_Version(LPSTR* Value));
  STDMETHOD(get_XMLContainer(long* Value));
  STDMETHOD(set_XMLContainer(long Value));
  STDMETHOD(get_DBName(BSTR* Value));
  STDMETHOD(get_UserName(BSTR* Value));
  STDMETHOD(get_UserPWD(BSTR* Value));
  STDMETHOD(set_DBName(BSTR Value));
  STDMETHOD(set_UserName(BSTR Value));
  STDMETHOD(set_UserPWD(BSTR Value));
  STDMETHOD(get_Charset(BSTR* Value));
  STDMETHOD(set_Charset(BSTR Value));
  STDMETHOD(get_UpperFunc(BSTR* Value));
  STDMETHOD(set_UpperFunc(BSTR Value));
  STDMETHOD(GetAVal(BSTR FuncName, BSTR Fields, BSTR Ref, BSTR AttrRef,
      BSTR* Ret));
  STDMETHOD(GetDVal(BSTR FuncName, long Node, VARIANT_BOOL NeedKonkr,
      BSTR* Text, BSTR* IEXML, short* Res));
  STDMETHOD(GetSVal(BSTR FuncName, long Node, BSTR* Tab, BSTR* Ret));
  STDMETHOD(Deinit());
  STDMETHOD(Init());
  STDMETHOD(get_RegGUI(BSTR* Value));
  STDMETHOD(get_RegKey(BSTR* Value));
  STDMETHOD(set_RegGUI(BSTR Value));
  STDMETHOD(set_RegKey(BSTR Value));
  STDMETHOD(ClearHash());
  STDMETHOD(get_LPUCode(BSTR* Value));
  STDMETHOD(set_LPUCode(BSTR Value));
};

#endif //AKDOReportImplH
