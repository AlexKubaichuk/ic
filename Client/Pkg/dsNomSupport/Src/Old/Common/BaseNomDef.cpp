//---------------------------------------------------------------------------
#include <DateUtils.hpp>
#pragma hdrstop
#include "BaseNomDef.h"
//#include "DocPeriod.h"
#include "PackPeriod.h"

//---------------------------------------------------------------------------
typedef map<const UnicodeString, UnicodeString> TStrMap;
TStrMap HashSel;
TStrMap ClassHashSel;
//---------------------------------------------------------------------------
#define flVal(a) AFields->Values[AFields->Names[a]]
//---------------------------------------------------------------------------
__fastcall TICSBaseNomDef::TICSBaseNomDef(TAutoFunc_DM *ADM)
{
  FDM = ADM;
}
//---------------------------------------------------------------------------
TGetAttrFunc __fastcall TICSBaseNomDef::GetAttrMethodAddr(UnicodeString AFuncName)
{
  TGetAttrFunc RC = NULL;

  if (AFuncName == "ADocAttrVal")          RC = ADocAttrVal;
  else if (AFuncName == "AExtEditValue")   RC = AExtEditValue;
  else if (AFuncName == "ASetOrgCode")     RC = ASetOrgCode;
  else if (AFuncName == "ASetLPUName")     RC = ASetLPUName;
  else if (AFuncName == "ASetLPUFullName") RC = ASetLPUFullName;
  else if (AFuncName == "ASetAddr")        RC = ASetAddr;
  else if (AFuncName == "ASetOKPO")        RC = ASetOKPO;
  else if (AFuncName == "ASetSOATO")       RC = ASetSOATO;
  else if (AFuncName == "ASetOKVED")       RC = ASetOKVED;
  else if (AFuncName == "ASetOKATO")       RC = ASetOKATO;
  else if (AFuncName == "ASetPhone")       RC = ASetPhone;
  else if (AFuncName == "ASetEmail")       RC = ASetEmail;
  else if (AFuncName == "AFamIO")          RC = AFamIO;
  else if (AFuncName == "AAge")            RC = AAge;
  else if (AFuncName == "AAgeY")           RC = AAgeY;
  else if (AFuncName == "AChoiceValue")    RC = AChoiceValue;
  else if (AFuncName == "AChoiceDBValue")  RC = AChoiceDBValue;
  else if (AFuncName == "AFullAddrVal")    RC = AFullAddrVal;
  else if (AFuncName == "AFullOrgAddrVal") RC = AFullOrgAddrVal;
  else if (AFuncName == "AFullBaseOrgVal") RC = AFullBaseOrgVal;
  else if (AFuncName == "AFullExtOrgVal")  RC = AFullExtOrgVal;
  else if (AFuncName == "AFullOrgVal")     RC = AFullOrgVal;
  else if (AFuncName == "AFullInshPolVal") RC = AFullInshPolVal;
  else if (AFuncName == "AFullULVal")      RC = AFullULVal;

  return RC;
}
//---------------------------------------------------------------------------
TGetDefFunc __fastcall TICSBaseNomDef::GetDefMethodAddr(UnicodeString AFuncName)
{
  TGetDefFunc RC = NULL;

  if (AFuncName == "DefChoiceValue")         RC = DefChoiceValue;
  else if (AFuncName == "DefRegVal")         RC = DefRegVal;
  else if (AFuncName == "DefTextValue")      RC = DefTextValue;
  else if (AFuncName == "DefChoiceDBValue")  RC = DefChoiceDBValue;
  else if (AFuncName == "DefAgePeriodLast")  RC = DefAgePeriodLast;
  else if (AFuncName == "DefDatePeriodLast") RC = DefDatePeriodLast;
  else if (AFuncName == "DefExtEditValue")   RC = DefExtEditValue;

  return RC;
}
//---------------------------------------------------------------------------
TGetSQLFunc __fastcall TICSBaseNomDef::GetSQLMethodAddr(UnicodeString AFuncName)
{
  TGetSQLFunc RC = NULL;

  if (AFuncName == "SQLChoiceValue")         RC = SQLChoiceValue;
  else if (AFuncName == "SQLAgePeriodLast")  RC = SQLAgePeriodLast;
  else if (AFuncName == "SQLDatePeriodLast") RC = SQLDatePeriodLast;
  else if (AFuncName == "SQLChoiceDBValue")  RC = SQLChoiceDBValue;
  else if (AFuncName == "SQLRegVal")         RC = SQLRegVal;
  else if (AFuncName == "SQLTextValue")      RC = SQLTextValue;
  else if (AFuncName == "SQLExtEditValue")   RC = SQLExtEditValue;

  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::GetValIfNotEmpty(UnicodeString APref, UnicodeString AVal, UnicodeString APostf)
{
  UnicodeString RC = "";
  try
   {
     if (AVal.Trim().Length() && (AVal.Trim().ToIntDef(0)!= -1) )
      RC = APref+AVal.Trim()+APostf;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::SetDay(int AYear, int AMonth, int ADay )
{
  UnicodeString RC = "";
  try
   {
     UnicodeString sm = IntToStr(AYear);
     sm = sm.SubString(sm.Length(),1);
     if (((sm == "1")||(sm == "2")||(sm == "3")||(sm == "4"))&&!((AYear<15)&&(AYear>10))) sm = "�. ";
     else                                                    sm = "�. ";
     if (AYear)   RC += IntToStr(AYear)+sm;
     if (AMonth)  RC += IntToStr(AMonth)+"�. ";
     if (ADay)    RC += IntToStr(ADay)+"�.";
     if (!(AYear+AMonth+ADay)) RC += "0�.";
   }
  __finally
   {
   }
  return RC.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::AgeStr(TDateTime ACurDate, TDateTime ABirthDay)
{
  UnicodeString RC = "";
  try
   {
     if (ACurDate < ABirthDay)
      {
        RC = "-";
      }
     else
      {
        Word FYear,FMonth,FDay;
        DateDiff(ACurDate, ABirthDay, FDay, FMonth, FYear);
        RC = SetDay(FYear,FMonth,FDay);
      }
   }
  __finally
   {
   }
  return RC.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::AgeStrY(TDateTime ACurDate, TDateTime ABirthDay)
{
  UnicodeString RC = "";
  try
   {
     if (ACurDate < ABirthDay)
      {
        RC = "-";
      }
     else
      {
        float DDD = (int)ACurDate - ((int)ABirthDay)-2;
        unsigned short YY=0,MM=0,DD=0;
        (ACurDate-ABirthDay+1).TDateTime::DecodeDate(&YY,&MM,&DD);  MM--;
        RC = FloatToStrF((DDD/365.25),ffFixed,16,2)+" "+" �.";
      }
   }
  __finally
   {
   }
  return RC.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::FGetGUI(TTagNode *ANode)
{
  return ANode->GetRoot()->GetChildByName("passport")->AV["gui"];
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::GetChildContAVDef(TTagNode *ANode, UnicodeString AVal, UnicodeString ADef)
{
  TTagNode *_tmp = ANode->GetFirstChild();
  UnicodeString _tmpAV;
  while (_tmp && !_tmp->CmpAV("name",AVal))
   {
     _tmp = _tmp->GetNext();
   }
  if (_tmp) return _tmp->GetAVDef("value",ADef);
  else      return ADef;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::_GetPart2(UnicodeString ARef, char Sep)
{
  int pos = ARef.Pos(Sep);
  return ARef.SubString(pos+1,ARef.Length()-pos);
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::DecodePeriod(UnicodeString AScr, int &AYear, int &AMonth, int &ADay)
{
  AYear  = GetPart1(AScr,'/').ToInt();
  AMonth = GetPart1(_GetPart2(AScr,'/'),'/').ToInt();
  ADay   = GetPart2(_GetPart2(AScr,'/'),'/').ToInt();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::AddPeriod(UnicodeString AScr1, UnicodeString AScr2)
{
  int YY1,YY2,MM1,MM2,DD1,DD2;
  DecodePeriod(AScr1,YY1,MM1,DD1);
  DecodePeriod(AScr2,YY2,MM2,DD2);
  DD1 += DD2;             DD2 = 0;
  if (DD1>31) {DD1 -= 31; DD2 = 1;}
  MM1 += MM2 + DD2; MM2 = 0;
  if (MM1>12) {MM1 -= 12; MM2 = 1;}
  YY1 += YY2 + MM2;
  return IntToStr(YY1)+"/"+IntToStr(MM1)+"/"+IntToStr(DD1);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::FIncPeriod(UnicodeString AFrDate,UnicodeString APer, int IsSub)
{
  TDateTime RC = TDate(0);
  try
   {
     int FYears, FMonths, FDays;
     DecodePeriod(APer, FYears, FMonths, FDays);
     if (IsSub)
      {
       FYears  *= -1;
       FMonths *= -1;
       FDays   *= -1;
      }
     RC = Dateutils::IncYear
     (
       Sysutils::IncMonth
       (
         Dateutils::IncDay(StrToDate(AFrDate),FDays),
         FMonths
       ),
       FYears
     );
   }
  __finally
   {
   }
  return RC.FormatString("dd.mm.yyyy");
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomDef::FGetRefValue(TTagNode *ANode, UnicodeString ARef1,UnicodeString ARef2)
{
  if (ANode->GetParent("domain")->GetNext()) return ARef1;
  else                                       return ARef2;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::GetSQLValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   UnicodeString ISTab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   ATab = ISTab;
   if (!AIE->Count)
    {
      if (FIS->AV["tabalias"].Length())
       ARet = FIS->AV["tabalias"]+"."+FIS->AV["keyfield"]+"="+FIS->AV["tabalias"]+"."+FIS->AV["keyfield"];
      else
       ARet = FIS->AV["maintable"]+"."+FIS->AV["keyfield"]+"="+FIS->AV["maintable"]+"."+FIS->AV["keyfield"];
    }
   else
    {
      bool  IsExtSel = false;
      if (FISCondRules->GetChildByName("root"))
       IsExtSel = (bool)(FISCondRules->GetChildByName("root")->AV["extsel"].ToIntDef(0));
      TTagNode *_tmp = AIE->GetChildByAV("text","name","sql");
      if (_tmp)
       {
         if (IsExtSel)
          {
            if (_tmp->Count)
             {
               TReplaceFlags rFlag;
               rFlag << rfReplaceAll;
               TTagNode *subtmp;
               UnicodeString FSQL = _tmp->AV["value"];
               UnicodeString FSubSQL;
               int idx = 0;
               subtmp = _tmp->GetFirstChild();
               while (subtmp)
                {
                  UnicodeString SelRC = "";
                  FSubSQL = subtmp->AV["value"];
                  TStrMap::iterator rc = HashSel.find(FSubSQL);
                  if (rc == HashSel.end())
                   {
                     SelRC = "-1";
                     FDM->qtExec(FSubSQL,false);
                     while (!FDM->quFreeA->Eof)
                      {
                        SelRC += ","+FDM->quFreeA->FN("CODE")->AsString;
                        FDM->quFreeA->Next();
                      }
                     HashSel[FSubSQL] = SelRC;
                   }
                  FSQL = Sysutils::StringReplace(FSQL, "_SEL"+IntToStr(idx), HashSel[FSubSQL], rFlag);
                  idx++;
                  subtmp = subtmp->GetNext();
                }
               ARet = FSQL;
             }
            else
             ARet = _tmp->AV["value"];
          }
         else
          ARet = _tmp->AV["value"];
       }
      else      ARet = "";
    }
}
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#                   ��������� ��������� �������� ���������                   #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::ADocAttrVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = AFields->Values[ARef];
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AExtEditValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = "����� ��������";
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::ASetOrgCode(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = AFields->Values["CODE"];
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::ASetLPUName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = AFields->Values["NAME"];
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::ASetLPUFullName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = AFields->Values["FULLNAME"];
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::ASetAddr(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = AFields->Values["ADDR"];
  UnicodeString tmp = ARet.Trim();
  if (tmp.Length() > 30)
   {
     bool IsDigit = true;
     for (int i = 1; (i < tmp.Length())&&IsDigit; i++)
      {
        if (!((UnicodeString(tmp[i]).ToIntDef(-1) != -1)||(tmp[i] == '#')))
         IsDigit = false;
      }
     if (IsDigit)
      ARet = FDM->KLADR->codeToText(tmp);
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::ASetOKPO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = AFields->Values["OKPO"];
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::ASetSOATO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = AFields->Values["SOATO"];
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::ASetOKVED(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = AFields->Values["OKVED"];
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::ASetOKATO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = AFields->Values["OKATO"];
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::ASetPhone(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = AFields->Values["PHONE"];
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::ASetEmail(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = AFields->Values["EMAIL"];
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AFamIO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  UnicodeString DDD = flVal(0);
  if (flVal(1).Length())
   DDD += " "+UnicodeString(flVal(1)[1])+".";
  if (flVal(2).Length())
   DDD += UnicodeString(flVal(2)[1])+".";
  ARet = DDD;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AAge(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  TDateTime FToDate = Date();
  UnicodeString FDP = GetPart1(UnicodeString(ARet),'#');
  if (GetPart2(FDP,';').Length())
   {
     if (TDate(GetPart2(FDP,';')) <= FToDate)
      FToDate = TDate(GetPart2(FDP,';'));
   }
  try
   {
     ARet = AgeStr(FToDate,StrToDate(AFields->Values[AFields->Names[0]]));
   }
  catch (EConvertError &E)
   {
     ARet = "";
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AAgeY(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  TDateTime FToDate = Date();
  UnicodeString FDP = GetPart1(UnicodeString(ARet),'#');
  if (GetPart2(FDP,';').Length())
   {
     if (TDate(GetPart2(FDP,';')) <= FToDate)
      FToDate = TDate(GetPart2(FDP,';'));
   }
  try
   {
     ARet = AgeStrY(FToDate,StrToDate(AFields->Values[AFields->Names[0]]));
   }
  catch (EConvertError &E)
   {
     ARet = "";
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AChoiceValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  TTagNode *FChoice = FDM->XMLList->GetRefer(ARef);
  if (FChoice->CmpName("choice"))
   {
     FChoice = FChoice->GetChildByAV("choicevalue","value",AFields->Values[AFields->Names[0]]);
     if (FChoice)
      ARet = FChoice->AV["name"];
     else
      ARet = "";
   }
  else if (FChoice->CmpName("choiceTree"))
   {
     TNomTreeClassForm *Dlg = NULL;
//     if (FChoice->CmpAV("uid","004F")) // ��� �� �����
//      Dlg = FDM->TreeFormList->TreeClassForm[FDM->RegGUI+".0013"];
//     else if (FChoice->CmpAV("uid","004A")) // ���-10
     Dlg = FDM->TreeFormList->TreeClassForm[FDM->GetXMLGUI(FChoice)+"."+FChoice->AV["uid"]];
     if (Dlg)
      ARet = Dlg->ElementExtCode[(long)AFields->Values[AFields->Names[0]].ToIntDef(-1)]+" "+Dlg->ElementName[(long)AFields->Values[AFields->Names[0]].ToIntDef(-1)];
     else
      ARet = "";
   }
  else if (FChoice->CmpName("binary"))
   {
     if (AFields->Values[AFields->Names[0]] == "1")  ARet = "��";
     else                                            ARet = "���";
   }
  else if (FChoice->CmpName("class,choiceBD"))
   {
     if (AFields->Values[AFields->Names[0]].ToIntDef(-1) != -1)
      {
        if (FChoice->CmpName("choiceBD"))
         FChoice = FChoice->GetTagByUID(FChoice->AV["ref"]);
        if (FChoice)
         {

           TTagNode *FieldList = FChoice->GetChildByName("description")->GetFirstChild();
           TTagNode *itField;
           TTagNode *ISAttr = FDM->XMLList->GetRefer(AAttrRef);
           UnicodeString OldRelation = "";
           UnicodeString FCol = _UID(ISAttr->AV["column"]);
           UnicodeString FSQL = "";
           itField = FieldList->GetFirstChild();
           while (itField)
            {
              if (!FSQL.Length())
               FSQL += "R"+itField->AV["ref"];
              else
               FSQL += ", R"+itField->AV["ref"];
              itField = itField->GetNext();
            }
           FSQL = "Select "+FSQL+" From "+_GUI(ISAttr->AV["column"]);
           if (ISAttr->AV["ref_parent"].Length())
            {
              while (ISAttr->AV["ref_parent"].Length())
               {
                 OldRelation = ISAttr->AV["relation"];
                 ISAttr = FDM->XMLList->GetRefer(_GUI(AAttrRef)+"."+ISAttr->AV["ref_parent"]);
                 if (!ISAttr->AV["ref_parent"].Length())
                  FSQL += " join "+_GUI(ISAttr->AV["column"])+" on ("+OldRelation+" and "+_GUI(ISAttr->AV["column"])+".CODE="+AFields->Values[AFields->Names[0]]+")";
                 else
                  FSQL += " join "+_GUI(ISAttr->AV["column"])+" on ("+OldRelation+")";
               }
            }
           else
            {
              FSQL += " Where "+_GUI(ISAttr->AV["column"])+".CODE="+AFields->Values[AFields->Names[0]];
            }
           UnicodeString SelRC = "";
           try
            {
              TStrMap::iterator rc = ClassHashSel.find(FSQL);
              if (rc == ClassHashSel.end())
               {
                 SelRC = "";
                 FDM->qtExec(FSQL,false);
                 itField = FieldList->GetFirstChild();
                 while (itField)
                  {
                    if (!FDM->quFreeA->FN("R"+itField->AV["ref"])->IsNull)
                     {
                       if (!SelRC.Length())
                        SelRC = FDM->quFreeA->FN("R"+itField->AV["ref"])->AsString;
                       else
                        SelRC += "/"+FDM->quFreeA->FN("R"+itField->AV["ref"])->AsString;
                     }
                    itField = itField->GetNext();
                  }
                 ClassHashSel[FSQL] = SelRC;
               }
              else
               SelRC = ClassHashSel[FSQL];
              ARet = SelRC;
            }
           catch (...)
            {
              ARet = "������";
            }
         }
      }
     else
      ARet = "";
   }
  else if (FChoice->CmpName("extedit"))
   {
     if (FChoice->CmpAV("uid","0062,0052,50AD,002A"))
      { // �����
        ARet = FDM->KLADR->codeToText(AFields->Values[AFields->Names[0]]);
      }
     else if (FChoice->CmpAV("uid","0041,0043"))
      { //�����������, ��������, ��������������
        ARet = FDM->GetClassData(AFields->Values[AFields->Names[0]].ToIntDef(-1),"0001");
      }
     else if (FChoice->CmpAV("uid","0014"))
      { //������ ��������
        ARet = FDM->GetClassData(AFields->Values["R0014"].ToIntDef(-1),"0057");
      }
     else
      {
        ARet = AFields->Values[AFields->Names[0]];
      }
   }
  else
   {
     ARet = "";
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AChoiceDBValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  TTagNode *FExtAttr = FDM->XMLList->GetRefer(ARef);
  if (FExtAttr)
   {
     ARet = FDM->GetClassData((long)AFields->Values[("R"+FExtAttr->AV["uid"]).c_str()].ToIntDef(-1),FExtAttr->AV["ref"]);
   }
  else
   {
     ARet = " �������� �� ���������� ";
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AFullAddrVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Values["R003D"] == "1")
   {
     ARet = FDM->KLADR->codeToText(AFields->Values["R0062"]);
     ARet = ARet + GetValIfNotEmpty(" �. ",AFields->Values["R0064"]);
     ARet = ARet + GetValIfNotEmpty(" ����. ",AFields->Values["R0065"]);
     ARet = ARet + GetValIfNotEmpty(" ��. ",AFields->Values["R0066"]);
   }
  else
   {
     ARet = "����� �� ������";
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AFullOrgAddrVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = GetValIfNotEmpty("",FDM->KLADR->codeToText(AFields->Values["R0052"]));
  ARet = ARet + GetValIfNotEmpty(" �., ����. ",AFields->Values["R004D"]);
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AFullBaseOrgVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Values["R0045"].ToIntDef(0) > 0)
   {
     ARet = FDM->GetClassData(AFields->Values["R0041"].ToIntDef(-1),"0001")+"\n";
     ARet = ARet + " "+AFields->Values["R0004"].Trim();
     ARet = ARet + " "+AFields->Values["R0025"].Trim();
     ARet = ARet + " "+AFields->Values["R0026"].Trim();
     if (AFields->Values["R0045"].ToIntDef(0) == 1)
      ARet = ARet + "  "+AFields->Values["R0005"];
     else
      ARet = ARet + "  "+FDM->GetClassData(AFields->Values["R0014"].ToIntDef(-1),"0057");
   }
  else
   ARet = "";
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AFullExtOrgVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Values["R0044"].ToIntDef(0) > 0)
   {
     ARet = FDM->GetClassData(AFields->Values["R0043"].ToIntDef(-1),"0001")+"\n";
     ARet = ARet + AFields->Values["R0015"];
   }
  else
   ARet = "";
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AFullOrgVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Values["R0045"].ToIntDef(0) > 0)
   {
     ARet = FDM->GetClassData(AFields->Values["R0041"].ToIntDef(-1),"0001")+"\n";
     ARet = ARet + " "+AFields->Values["R0004"].Trim();
     ARet = ARet + " "+AFields->Values["R0025"].Trim();
     ARet = ARet + " "+AFields->Values["R0026"].Trim();
     if (AFields->Values["R0045"].ToIntDef(0) == 1)
      ARet = ARet + "  "+AFields->Values["R0005"];
     else
      ARet = ARet + "  "+FDM->GetClassData(AFields->Values["R0014"].ToIntDef(-1),"0057");
   }
  else if (AFields->Values["R0044"].ToIntDef(0) > 0)
   {
     ARet = FDM->GetClassData(AFields->Values["R0043"].ToIntDef(-1),"0001")+"\n";
     ARet = ARet + AFields->Values["R0015"];
   }
  else
   {
     ARet = "�� �����������";
     int noval = AFields->Values["R003E"].Trim().ToIntDef(-1);
     switch (noval)
      {
        case 0: {ARet += " �� ��������� ��������"; break;}
        case 1: {ARet += " �� ���������� ��������"; break;}
        case 2: {ARet += " �� ������ ��������"; break;}
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AFullInshPolVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Values["R003B"] == "1")
   {
     ARet = FDM->GetClassData((long)AFields->Values["R002E"].ToIntDef(-1),"002A");
     ARet += " "+AFields->Values["R0035"].Trim();
     ARet += " � "+AFields->Values["R0036"].Trim();
   }
  else
   {
     ARet = "";
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::AFullULVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Values["R003C"] == "1")
   {
     ARet = "";
     int noval = AFields->Values["R0037"].Trim().ToIntDef(-1);
     switch (noval)
      {
        case 0: {ARet += "�������"; break;}
        case 1: {ARet += "������������� � ��������"; break;}
      }
     ARet += " "+AFields->Values["R0038"].Trim();
     ARet += " � "+AFields->Values["R0039"].Trim();
   }
  else
   {
     ARet = "";
   }
}
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#            ��������� ��������� �������� �������������� ��������� (Def...)  #
//#                                                                            #
//#            ��������� ������������ ������ SQL-�������             (SQL...)  #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::DefChoiceValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   if (!FDM) return false;
   UnicodeString tmp = "";
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   if (NeedKonkr)
    {
      tmp = "<root><fl ref='"+_UID(FISAttr->AV["ref"])+"'/></root>";
    }


   return FDM->DefRegValue(AIE, AText, NeedKonkr, AXMLIE, tmp, _GUI(FISAttr->AV["ref"]), false, false);
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLChoiceValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::DefAgePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   // AIE - ������, ������ �� IE - ����� ����� 2-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "�������"
   // {
   //   1-� - ������� ��;
   //   2-� - ������� ��;
   // }
   if (!FDM) return false;
   bool iRC = false;
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (NeedKonkr)
    {
      TDateConForm *Dlg = NULL;
      try
       {
         Dlg = new TDateConForm(Application->Owner);
         int FSubType;
         Dlg->SetType(_scRDate,AIE,FSubType,"");
         Dlg->MsgLab->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->Count == 2)
    {
      TTagNode *_tmpNode = AIE->GetFirstChild();
      UnicodeString _tmp;
      _tmp = FISCondRules->AV["name"]+" = ";
      _tmp += _tmpNode->AV["name"]+_tmpNode->AV["value"];
      _tmpNode = _tmpNode->GetNext();
      _tmp += " "+_tmpNode->AV["name"]+_tmpNode->AV["value"];
      AText = _tmp.Trim();
      _tmp = "";
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLAgePeriodLast(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  // �������� ������� ��������
  TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  // �������� ������� �������� ������������ �������� ����� �����������
  TTagNode *FAddISAttr = FISCondRules->GetTagByUID(FISAttr->AV["add_ref"]);
  TTagNode *FAddIS;
  TTagNode *FIS = FISAttr->GetParent("is");
  ATab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
  UnicodeString xFr,xTo,flName,AddflName;
  xFr = ""; xTo = "";
  flName = FIS->AV["tabalias"]+"."+GetPart1(FISAttr->AV["fields"],',');
//   bool isCurrDate = false;
  UnicodeString FDP = GetPart1(UnicodeString(ARet),'#');
  if (FAddISAttr)
   {
     FAddIS = FAddISAttr->GetParent("is");
     AddflName = FAddIS->AV["tabalias"]+"."+FAddISAttr->AV["fields"];
   }
  else
   {
     if (GetPart2(FDP,';').Length())
      {
        if (TDate(GetPart2(FDP,';')) > Date())
         AddflName = "'"+Date().FormatString("dd.mm.yyyy")+"'";
        else
         AddflName = "'"+GetPart2(FDP,';')+"'";

      }
     else
      AddflName = "'"+Date().FormatString("dd.mm.yyyy")+"'";
//      isCurrDate = true;
   }
  TTagNode *TagFr, *TagTo;
  TagFr = AIE->GetChildByAV("text","name","��:");
  TagTo = AIE->GetChildByAV("text","name","��:");
  if (TagFr && (TagFr->AV["value"].Length()))
   {
      xFr = flName+"<="+UnicodeString("Inc_Period")+"("+AddflName+",'"+TagFr->AV["value"]+"','/',1)";
   }
  if (TagTo && (TagTo->AV["value"].Length()))
   {
      xTo = flName+">="+UnicodeString("Inc_Period")+"("+AddflName+",'"+TagTo->AV["value"]+"','/',1)";
   }
  if (xFr.Length() && xTo.Length())
   {
     ARet = "("+xFr+" and "+xTo+")";
   }
  else if (xFr.Length())
   {
     ARet = xFr;
   }
  else if (xTo.Length())
   {
     ARet = xTo;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::DefDatePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   // AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "����"
   // {
   //   1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
   //                            1,2 ��� �������; 0 - �������� ���
   //                                             1 - ������ �� ������� ����
   //                                             2 - ������ �� ����
   //   2-� - ���� ��;   ��� ���� "�������� ���"
   //                    ��� ����� "������ �� ..." �������� �������
   //   3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
   //                    ��� ���� "������ �� ������� ����"  - �� �������������;
   //   4-� - �������� - ��� ���� "�������� ���"  - �� �������������
   //         �������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
   if (!FDM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (NeedKonkr)
    {
      TDateConForm *Dlg = NULL;
      try
       {
         Dlg = new TDateConForm(Application->Owner);
         int FSubType;
         UnicodeString FSubTypeCapt = "";
         TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
         if (FISCondRules->GetParent("IS") == FISCondRules->GetParent("domain")->GetFirstChild()) // ���� ��������
          FSubType = 0;
         else  // ���� ������������
          {
            FSubType = 1;
            if (!FISAttr->AV["add_ref"].Length())
             FSubTypeCapt = "������ ��������� ������������";
            else
             {
               if (FISAttr->AV["add_ref"].Pos("#"))
                {
                  if (GetPart2(FISAttr->AV["add_ref"],'#').ToIntDef(0))
                   FSubTypeCapt = GetPart1(FISAttr->AV["add_ref"],'#');
                  else
                   {
                     FSubType = 0;
                     FSubTypeCapt = "";
                   }
                }
               else
                FSubTypeCapt = FISAttr->AV["add_ref"];
             }
          }
         bool DoNotDop = FISAttr->CmpAV("ref","4031DA5E-8AC52FB3-7D03.1024");
         Dlg->SetType(_scDate,AIE,FSubType,FSubTypeCapt,DoNotDop);
         Dlg->MsgLab->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count)
       {
         tmp = FISCondRules->AV["name"]+" = ";
         __tmp = AIE->GetFirstChild();
         int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
         bool FLast = (bool)(DType&1);
         DType = DType >> 1;
         if (FLast)
          {
            TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
            if (!FISAttr->AV["add_ref"].Length())
             tmp = "������ ��������� ������������";
            else
             tmp = FISAttr->AV["add_ref"];
          }
         TTagNode *DateType   = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam1 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam2 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam3 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam4 = __tmp;
         if (DType == 0)
          {// �������� ���
            if ((DateParam1 && (DateParam1->AV["value"].Length())) ||
                (DateParam2 && (DateParam2->AV["value"].Length())))
             {
               tmp += " "+DateType->AV["name"];
               if (DateParam1->AV["value"].Length())
                {
                  tmp += " "+DateParam1->AV["name"];
                  tmp += "='"+DateParam1->AV["value"]+"'";
                }
               if (DateParam2->AV["value"].Length())
                {
                  tmp += " "+DateParam2->AV["name"];
                  tmp += "='"+DateParam2->AV["value"]+"'";
                }
             }
          }
         else if (DType >= 3)
          { // 3 - ������ ������������ ���������
            // 4 - ������ ���� - ���� ��������� ������� ������������ ���������
            // 5 - ������ ��������� - ���� ��������� ������� ������������ ���������
            // 6 - ������ �������� - ���� ��������� ������� ������������ ���������
            // 7 - ������ ������ - ���� ��������� ������� ������������ ���������

            tmp += " "+DateType->AV["name"];
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         else
          {// ������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
            tmp += " "+DateType->AV["name"];
            if (DateParam1->AV["value"].Length())
             {
               tmp += " "+DateParam1->AV["name"];
               tmp += "='"+DateParam1->AV["value"]+"'";
             }
            if (DateParam2->AV["value"].Length())
             {
               tmp += " "+DateParam2->AV["name"];
               tmp += "='"+DateParam2->AV["value"]+"'";
             }
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         if (DateParam4)
          {
            tmp += " "+DateParam4->AV["name"];
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLDatePeriodLast(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   TTagNode *DateOffs, *__tmp;
   ATab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   UnicodeString xFr,xTo,flName,xPer,xOffs,xTabName,xMaxSQL,xMaxTabName,MaxflName;
   xFr = ""; xTo = "";
   xTabName = FIS->AV["maintable"];
   flName = FIS->AV["tabalias"]+"."+FISAttr->AV["fields"];
   int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
   bool FLast = (bool)(DType&1);
   DType = DType >> 1;
   TTagNode *TagFr, *TagTo;
   __tmp = AIE->GetFirstChild()->GetNext();
   TagFr = __tmp; __tmp = __tmp->GetNext();
   TagTo = __tmp; __tmp = __tmp->GetNext();
   DateOffs = __tmp; __tmp = __tmp->GetNext();
   xMaxTabName = "DTT"+IntToStr((int)FLast);
   xMaxSQL =  flName+" = (Select Max("+xMaxTabName+"."+FISAttr->AV["fields"]+") From "+xTabName+" "+xMaxTabName;
   MaxflName = xMaxTabName+"."+FISAttr->AV["fields"];
   if (DType == 0)
    {// �������� ���
      if (TagFr && (TagFr->AV["value"].Length()))
       xFr = ">='"+_CorrectDate(TagFr->AV["value"])+"'";
      if (TagTo && (TagTo->AV["value"].Length()))
       xTo = "<='"+_CorrectDate(TagTo->AV["value"])+"'";
    }
   else if (DType == 1)
    {// ������ �� ������� ����
      xPer = TagFr->AV["value"];
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
         xFr = ">='"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),xPer,1)+"'";
         xTo = "<='"+Date().FormatString("dd.mm.yyyy")+"'";
       }
      else
       {
         xFr = ">='"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),AddPeriod(xPer,xOffs),1)+"'";
         xTo = "<='"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),xOffs,1)+"'";
       }
    }
   else if (DType == 2)
    {// ������ �� ���� ��������� ������� ������������ ���������
      UnicodeString FDocPer = GetPart1(UnicodeString(ARet),'#');
      if (FDocPer.Length())
       {
         if (GetPart2(FDocPer,';').Length())
          FDocPer = GetPart2(FDocPer,';');
         else
          FDocPer = Date().FormatString("dd.mm.yyyy");
       }
      else
       FDocPer = Date().FormatString("dd.mm.yyyy");
      xPer  = TagFr->AV["value"];
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
         xFr = ">='"+FIncPeriod(FDocPer,xPer,1)+"'";
         xTo = "<='"+FDocPer+"'";
       }
      else
       {
         xFr = ">='"+FIncPeriod(FDocPer,AddPeriod(xPer,xOffs),1)+"'";
         xTo = "<='"+FIncPeriod(FDocPer,xOffs,1)+"'";
       }
    }
   else if (DType >= 3)
    { // 3 - ������ ������������ ���������
      // 4 - ������ ���� - ���� ��������� ������� ������������ ���������
      // 5 - ������ ��������� - ���� ��������� ������� ������������ ���������
      // 6 - ������ �������� - ���� ��������� ������� ������������ ���������
      // 7 - ������ ������ - ���� ��������� ������� ������������ ���������
      UnicodeString FPer = GetPart1(UnicodeString(ARet),'#');
      if (FPer.Length())
       {
         xOffs = DateOffs->AV["value"];
         if (!xOffs.Length())
          {
            if (GetPart1(FPer,';').Length())
             xFr = ">='"+TDate(GetPart1(FPer,';')).FormatString("dd.mm.yyyy")+"'";
            if (GetPart2(FPer,';').Length())
             xTo = "<='"+TDate(GetPart2(FPer,';')).FormatString("dd.mm.yyyy")+"'";
          }
         else
          {
            if (GetPart1(FPer,';').Length())
             xFr = ">='"+FIncPeriod(GetPart1(FPer,';'),xOffs,1)+"'";
            if (GetPart2(FPer,';').Length())
             xTo = "<='"+FIncPeriod(GetPart2(FPer,';'),xOffs,1)+"'";
          }
       }
    }
   if (xFr.Length())
    {
      xMaxSQL += " and "+MaxflName+xFr;
      xFr = flName+xFr;
    }
   if (xTo.Length())
    {
      xMaxSQL += " and "+MaxflName+xTo;
      xTo = flName+xTo;
    }
   if (xFr.Length() && xTo.Length())
    {
      if (FLast)
       ARet = "("+xFr+" and "+xTo+" and "+xMaxSQL+")";
      else
       ARet = "("+xFr+" and "+xTo+")";
    }
   else if (xFr.Length())
    {
      if (FLast)
       ARet = "("+xFr+" and "+xMaxSQL+")";
      else
       ARet = xFr;
    }
   else if (xTo.Length())
    {
      if (FLast)
       ARet = "("+xTo+" and "+xMaxSQL+")";
      else
       ARet = xTo;
    }
   else
    {
      if (FLast)
       ARet = xMaxSQL;
      else
       ARet = "";
    }
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::DefChoiceDBValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   if (!FDM) return false;
   UnicodeString tmp = "";
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   if (NeedKonkr)
    {
      TTagNode *FExtAttr = FDM->XMLList->GetRefer(FISAttr->AV["ref"]);
      tmp = "<root>"
            "<fl ref='"+FExtAttr->AV["uid"]+"' req=1'/>"
            "</root>";
    }


   return FDM->DefRegValue(AIE, AText, NeedKonkr, AXMLIE, tmp, _GUI(FISAttr->AV["ref"]), false, false);
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLChoiceDBValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::DefRegVal(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   if (!this->FDM) return false;
   bool CanAllSelect = false;
   bool SubSel = false;
   UnicodeString tmp = "";
   UnicodeString FGUI = "";
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
      if (FISCondRules)
       {
         TTagNode *FRoot = FISCondRules->GetChildByName("root");
         if (FRoot)
          {
            tmp = FRoot->AsXML;
            CanAllSelect = (bool)(FRoot->AV["CanAllSelect"].ToIntDef(0));
            SubSel       = (bool)(FRoot->AV["subsel"].ToIntDef(0));
            FGUI         = FRoot->AV["gui_ref"];
          }
       }
    }
   

  return FDM->DefRegValue(AIE, AText, NeedKonkr, AXMLIE, tmp, FGUI, CanAllSelect, SubSel);
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLRegVal(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::DefTextValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   if (!FDM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (NeedKonkr)
    {
      TTextConForm *Dlg = NULL;
      try
       {
         Dlg = new TTextConForm(Application->Owner, FISCondRules->AV["name"],AIE,FDM);
         Dlg->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         __tmp = AIE->GetFirstChild();
         tmp = FISCondRules->AV["name"]+" =";
         while (__tmp->GetNext())
          {
            tmp += " "+__tmp->AV["name"];
            __tmp = __tmp->GetNext();
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLTextValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  if (AIE->Count)
   {
     UnicodeString FText = "";
     bool FCaseSens, FFullWord;
     FCaseSens = FFullWord = false;
     int FCompType = 2;

     TTagNode *tmp = AIE->GetChildByAV("text","ref","0");
     if (tmp) FText = tmp->AV["value"].Trim();

     tmp = AIE->GetChildByAV("text","ref","1");
     if (tmp) FCaseSens= (bool)tmp->AV["value"].ToIntDef(0);

     tmp = AIE->GetChildByAV("text","ref","2");
     if (tmp) FFullWord = (bool)tmp->AV["value"].ToIntDef(0);

     tmp = AIE->GetChildByAV("text","ref","3");
     if (tmp)
      {
        FCompType = tmp->AV["value"].ToIntDef(2);
        if (tmp->CmpAV("name","SQL")) // ��������� ������ ��������
         tmp->AV["name"] = "��������� ��������";
      }

     TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
     TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
     TTagNode *FIS = FISAttr->GetParent("is");
     TTagNode *__tmp;
     UnicodeString FFieldName = FIS->AV["tabalias"]+"."+FISAttr->AV["fields"];


     TTagNode *FSQLNode = AIE->GetChildByAV("text","name","sql");
     if (!FSQLNode) FSQLNode = AIE->AddChild("text","name=sql");
     FSQLNode->AV["ref"] = "4";

     switch (FCompType)
      {
        case 0:
        {
          FSQLNode->AV["value"] = "("+FFieldName + " is null or "+FFieldName+" = '')";
          break;
        }
        case 1:
        {
          FSQLNode->AV["value"] = "("+FFieldName + " is not null and "+FFieldName+" <> '')";
          break;
        }
        case 2:
        {
          if (FFullWord)
           {
             if (FCaseSens)
              FSQLNode->AV["value"] = FFieldName + "='"+FText+"'";
             else
              FSQLNode->AV["value"] = FDM->UpperFunc+"("+FFieldName + ")='"+FText.UpperCase()+"'";
           }
          else
           {
             if (FCaseSens)
              FSQLNode->AV["value"] = FFieldName + " LIKE '%"+FText+"%'";
             else
              FSQLNode->AV["value"] = FDM->UpperFunc+"(" + FFieldName + ") LIKE '%"+FText.UpperCase()+"%'";
           }
          break;
        }
      }
   }
  GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::GetAtttInfo(UnicodeString AFuncName, TStringList *AFields, UnicodeString ARef, UnicodeString &ARet, UnicodeString AAttrRef)
{
  TGetAttrFunc FFunc = GetAttrMethodAddr(AFuncName);
  if (FFunc)
   FFunc(AFields, ARef, ARet, AAttrRef);
  else
   throw Exception(("������ ������ ������� � ������ '"+AFuncName+"'").c_str());
}
//---------------------------------------------------------------------------
bool __fastcall TICSBaseNomDef::GetDefInfo(UnicodeString AFuncName, TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
  TGetDefFunc FFunc = GetDefMethodAddr(AFuncName);
  if (FFunc)
   return FFunc(AIE, AText, NeedKonkr, AXMLIE);
  else
   throw Exception(("������ ������ ������� � ������ '"+AFuncName+"'").c_str());
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::GetSQLInfo(UnicodeString AFuncName, TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
  TGetSQLFunc FFunc = GetSQLMethodAddr(AFuncName);
  if (FFunc)
   FFunc(AIE, ATab, ARet);
  else
   throw Exception(("������ ������ ������� � ������ '"+AFuncName+"'").c_str());
}
//---------------------------------------------------------------------------


bool __fastcall TICSBaseNomDef::DefExtEditValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   // AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "����"
   // {
   //   1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
   //                            1,2 ��� �������; 0 - �������� ���
   //                                             1 - ������ �� ������� ����
   //                                             2 - ������ �� ����
   //   2-� - ���� ��;   ��� ���� "�������� ���"
   //                    ��� ����� "������ �� ..." �������� �������
   //   3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
   //                    ��� ���� "������ �� ������� ����"  - �� �������������;
   //   4-� - �������� - ��� ���� "�������� ���"  - �� �������������
   //         �������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
   if (!FDM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (NeedKonkr)
    {
      TDateConForm *Dlg = NULL;
      try
       {
         Dlg = new TDateConForm(Application->Owner);
         int FSubType;
         UnicodeString FSubTypeCapt = "";
         TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
         if (FISCondRules->GetParent("IS") == FISCondRules->GetParent("domain")->GetFirstChild()) // ���� ��������
          FSubType = 0;
         else  // ���� ������������
          {
            if (!FISAttr->AV["add_ref"].Length())
             FSubTypeCapt = "������ ��������� ������������";
            else
             FSubTypeCapt = FISAttr->AV["add_ref"];
            FSubType = 1;
          }
         bool DoNotDop = FISAttr->CmpAV("ref","4031DA5E-8AC52FB3-7D03.1024");
         Dlg->SetType(_scDate,AIE,FSubType,FSubTypeCapt,DoNotDop);
         Dlg->MsgLab->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count)
       {
//         tmp = "";
         tmp = FISCondRules->AV["name"]+" = ";
         __tmp = AIE->GetFirstChild();
         int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
         bool FLast = (bool)(DType&1);
         DType = DType >> 1;
         if (FLast)
          {
            TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
            if (!FISAttr->AV["add_ref"].Length())
             tmp = "������ ��������� ������������";
            else
             tmp = FISAttr->AV["add_ref"];
          }
         TTagNode *DateType   = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam1 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam2 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam3 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam4 = __tmp;
         if (DType == 0)
          {// �������� ���
            if ((DateParam1 && (DateParam1->AV["value"].Length())) ||
                (DateParam2 && (DateParam2->AV["value"].Length())))
             {
               tmp += " "+DateType->AV["name"];
               if (DateParam1->AV["value"].Length())
                {
                  tmp += " "+DateParam1->AV["name"];
                  tmp += "='"+DateParam1->AV["value"]+"'";
                }
               if (DateParam2->AV["value"].Length())
                {
                  tmp += " "+DateParam2->AV["name"];
                  tmp += "='"+DateParam2->AV["value"]+"'";
                }
             }
          }
         else if (DType == 3)
          {// ������ ������������ ���������
            tmp += " "+DateType->AV["name"];
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         else
          {// ������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
            tmp += " "+DateType->AV["name"];
            if (DateParam1->AV["value"].Length())
             {
               tmp += " "+DateParam1->AV["name"];
               tmp += "='"+DateParam1->AV["value"]+"'";
             }
            if (DateParam2->AV["value"].Length())
             {
               tmp += " "+DateParam2->AV["name"];
               tmp += "='"+DateParam2->AV["value"]+"'";
             }
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         if (DateParam4)
          {
            tmp += " "+DateParam4->AV["name"];
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomDef::SQLExtEditValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   TTagNode *DateOffs, *__tmp;
   ATab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   UnicodeString xFr,xTo,flName,xPer,xOffs,xTabName,xMaxSQL,xMaxTabName,MaxflName;
   xFr = ""; xTo = "";
   xTabName = FIS->AV["maintable"];
   flName = FIS->AV["tabalias"]+"."+FISAttr->AV["fields"];
   int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
   bool FLast = (bool)(DType&1);
   DType = DType >> 1;
   TTagNode *TagFr, *TagTo;
   __tmp = AIE->GetFirstChild()->GetNext();
   TagFr = __tmp; __tmp = __tmp->GetNext();
   TagTo = __tmp; __tmp = __tmp->GetNext();
   DateOffs = __tmp; __tmp = __tmp->GetNext();
   xMaxTabName = "DTT"+IntToStr((int)FLast);
   xMaxSQL =  flName+" = (Select Max("+xMaxTabName+"."+FISAttr->AV["fields"]+") From "+xTabName+" "+xMaxTabName;
   MaxflName = xMaxTabName+"."+FISAttr->AV["fields"];
   if (DType == 0)
    {// �������� ���
      if (TagFr && (TagFr->AV["value"].Length()))
       xFr = ">='"+_CorrectDate(TagFr->AV["value"])+"'";
      if (TagTo && (TagTo->AV["value"].Length()))
       xTo = "<='"+_CorrectDate(TagTo->AV["value"])+"'";
    }
   else if (DType == 1)
    {// ������ �� ������� ����
      xPer = TagFr->AV["value"];
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
         xFr = ">='"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),xPer,1)+"'";
         xTo = "<='"+Date().FormatString("dd.mm.yyyy")+"'";
       }
      else
       {
         xFr = ">='"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),AddPeriod(xPer,xOffs),1)+"'";
         xTo = "<='"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),xOffs,1)+"'";
       }
    }
   else if (DType == 2)
    {// ������ �� ���� ��������� ������� ������������ ���������
      UnicodeString FDocPer = GetPart1(UnicodeString(ARet),'#');
      if (FDocPer.Length())
       {
         if (GetPart2(FDocPer,';').Length())
          FDocPer = GetPart2(FDocPer,';');
         else
          FDocPer = Date().FormatString("dd.mm.yyyy");
       }
      else
       FDocPer = Date().FormatString("dd.mm.yyyy");
      xPer  = TagFr->AV["value"];
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
         xFr = ">='"+FIncPeriod(FDocPer,xPer,1)+"'";
         xTo = "<='"+FDocPer+"'";
       }
      else
       {
         xFr = ">='"+FIncPeriod(FDocPer,AddPeriod(xPer,xOffs),1)+"'";
         xTo = "<='"+FIncPeriod(FDocPer,xOffs,1)+"'";
       }
    }
   else if (DType == 3)
    {// ������ ������������ ���������
      UnicodeString FPer = GetPart1(UnicodeString(ARet),'#');
      if (FPer.Length())
       {
         xOffs = DateOffs->AV["value"];
         if (!xOffs.Length())
          {
            if (GetPart1(FPer,';').Length())
             xFr = ">='"+TDate(GetPart1(FPer,';')).FormatString("dd.mm.yyyy")+"'";
            if (GetPart2(FPer,';').Length())
             xTo = "<='"+TDate(GetPart2(FPer,';')).FormatString("dd.mm.yyyy")+"'";
          }
         else
          {
            if (GetPart1(FPer,';').Length())
             xFr = ">='"+FIncPeriod(GetPart1(FPer,';'),xOffs,1)+"'";
            if (GetPart2(FPer,';').Length())
             xTo = "<='"+FIncPeriod(GetPart2(FPer,';'),xOffs,1)+"'";
          }
       }
    }
   if (xFr.Length())
    {
      xMaxSQL += " and "+MaxflName+xFr;
      xFr = flName+xFr;
    }
   if (xTo.Length())
    {
      xMaxSQL += " and "+MaxflName+xTo;
      xTo = flName+xTo;
    }
   if (xFr.Length() && xTo.Length())
    {
      if (FLast)
       ARet = "("+xFr+" and "+xTo+" and "+xMaxSQL+")";
      else
       ARet = "("+xFr+" and "+xTo+")";
    }
   else if (xFr.Length())
    {
      if (FLast)
       ARet = "("+xFr+" and "+xMaxSQL+")";
      else
       ARet = xFr;
    }
   else if (xTo.Length())
    {
      if (FLast)
       ARet = "("+xTo+" and "+xMaxSQL+")";
      else
       ARet = xTo;
    }
   else
    {
      if (FLast)
       ARet = xMaxSQL;
      else
       ARet = "";
    }
}
//---------------------------------------------------------------------------

