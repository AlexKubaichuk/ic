//---------------------------------------------------------------------------

#ifndef InqConH
#define InqConH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Mask.hpp>
#include "XMLContainer.h"
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include <Menus.hpp>
#include "cxCalendar.hpp"
#include "Dmf_Auto.h"
//---------------------------------------------------------------------------
class TConForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *MsgLab;
        TPanel *BtnPanel;
        TLabel *Label3;
        TGroupBox *FromGB;
        TGroupBox *ToGB;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TcxComboBox *ValueCB;
        TcxDateEdit *FromDE;
        TcxDateEdit *ToDE;
        TcxTextEdit *FromED;
        TcxTextEdit *ToED;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall ValueCBChange(TObject *Sender);
private:	// User declarations
        int     EDType;
        TAutoFunc_DM   *FDM;
        TStringList *FCodeValues;
public:		// User declarations
        TTagNode *Result;
        void __fastcall SetType(int AType,TTagNode *ANode);
        __fastcall TConForm(TComponent* Owner, TAutoFunc_DM *ADM, TStringList *ACodeValues = NULL);
};
//---------------------------------------------------------------------------
extern PACKAGE TConForm *ConForm;
//---------------------------------------------------------------------------
#endif
