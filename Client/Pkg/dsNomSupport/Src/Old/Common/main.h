//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <vcl.h>
#include "Dmf_Auto.h"
#include "pgmsetting.h"
//---------------------------------------------------------------------------
#define _DLLEXP_ extern "C" void __declspec(dllexport)
#define _DLLEXPB_ extern "C" bool __declspec(dllexport)
//##############################################################################
//#                                                                            #
//#                   ��������� ��������� �������� ���������                   #
//#                                                                            #
//##############################################################################
_DLLEXP_ __stdcall ADocAttrVal(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AExtEditValue(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);

_DLLEXP_ __stdcall ASetOrgCode(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ASetLPUName(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ASetLPUFullName(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ASetAddr(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ASetOKPO(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ASetSOATO(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ASetOKVED(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ASetOKATO(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ASetPhone(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ASetEmail(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);

_DLLEXP_ __stdcall AFamIO(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AAge(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AMKBDiag(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AInqAge(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AInqChVal(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AZaklPres(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AChoiceValue(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AChoiceDBValue(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AAMInqPer(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AAMInqDiag(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AAMInqIllType(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AAMInqType(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AAMInqWrk(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AAMInqLPU(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ADOutval(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
#ifdef _IMM
_DLLEXP_ __stdcall AAddrValue(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall AOrgValue(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall APatPrivAge(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
#endif
#ifdef _AKDO
_DLLEXP_ __stdcall AInqChValValue(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ADDocSpec(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ADInqDiag(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ADST(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ADHZ(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ADVFirst(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
_DLLEXP_ __stdcall ADDiagT(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef);
#endif
//##############################################################################
//#                                                                            #
//#            ��������� ��������� �������� �������������� ��������� (Def...)  #
//#                                                                            #
//#            ��������� ������������ ������ SQL-�������             (SQL...)  #
//#                                                                            #
//##############################################################################
_DLLEXPB_ __stdcall DefChoiceValue(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLChoiceValue(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefAgePeriodLast(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLAgePeriodLast(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefDatePeriodLast(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLDatePeriodLast(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefChoiceDBValue(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLChoiceDBValue(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefInqValue(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLInqValue(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefInqTreeValue(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLInqTreeValue(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefInqValueVal(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLInqValueVal(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefPresZakl(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLPresZakl(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefRegVal(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLRegVal(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefTextValue(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLTextValue(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefMInqPer(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLMInqPer(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefMInqIllType(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLMInqIllType(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefMInqType(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLMInqType(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefMInqWrk(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLMInqWrk(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefMInqLPU(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLMInqLPU(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefDOutval(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLDOutval(TTagNode *AIE, char* ATab, char* ARet);
#ifdef _IMM
_DLLEXPB_ __stdcall DefOtvodDatePeriodLast(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLOtvodDatePeriodLast(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefPlanPrivReak(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLPlanPrivReak(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefPlanProbReak(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLPlanProbReak(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefPlanPres(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLPlanPres(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefExtEditValue(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLExtEditValue(TTagNode *AIE, char* ATab, char* ARet);
#endif
#ifdef _AKDO
_DLLEXPB_ __stdcall DefDDocSpec(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLDDocSpec(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefDST(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLDST(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefDHZ(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLDHZ(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefDVFirst(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLDVFirst(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefDDiagT(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLDDiagT(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefDDispU(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLDDispU(TTagNode *AIE, char* ATab, char* ARet);
_DLLEXPB_ __stdcall DefDLinkDiag(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE);
_DLLEXP_ __stdcall SQLDLinkDiag(TTagNode *AIE, char* ATab, char* ARet);
#endif

#endif
