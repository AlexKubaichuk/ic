object DateConForm: TDateConForm
  Left = 624
  Top = 248
  HelpContext = 4022
  BorderIcons = []
  BorderStyle = bsToolWindow
  BorderWidth = 3
  Caption = #1054#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1103' ...'
  ClientHeight = 229
  ClientWidth = 388
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object MsgLab: TLabel
    Left = 0
    Top = 13
    Width = 388
    Height = 13
    Align = alTop
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    WordWrap = True
  end
  object Label3: TLabel
    Left = 0
    Top = 0
    Width = 388
    Height = 13
    Align = alTop
    Caption = #1047#1085#1072#1095#1077#1085#1080#1077' '#1076#1083#1103' '#1101#1083#1077#1084#1077#1085#1090#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    WordWrap = True
  end
  object BtnPanel: TPanel
    Left = 0
    Top = 189
    Width = 388
    Height = 40
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 2
    DesignSize = (
      388
      40)
    object OkBtn: TcxButton
      Left = 221
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      OnClick = OkBtnClick
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
    end
    object CancelBtn: TcxButton
      Left = 303
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
    end
  end
  object FromGB: TGroupBox
    Left = 7
    Top = 80
    Width = 143
    Height = 74
    Caption = ' '#1054#1090': '
    TabOrder = 0
    object FromFormatLab: TLabel
      Left = 11
      Top = 49
      Width = 98
      Height = 13
      Caption = #1092#1086#1088#1084#1072#1090': '#1075#1075#1075'/'#1084#1084'/'#1076#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object FromED: TcxMaskEdit
      Left = 11
      Top = 23
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d?\d?\d\/(0?\d|1?[0-1])\/([0-2]?\d|3?[0-1])'
      Properties.MaxLength = 0
      Properties.OnValidate = FromEDPropertiesValidate
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 0
      Width = 90
    end
    object FromDE: TcxDateEdit
      Left = 11
      Top = 23
      Properties.ButtonGlyph.Data = {
        3E020000424D3E0200000000000036000000280000000D0000000D0000000100
        1800000000000802000000000000000000000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C000C56E35D6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAA
        D6ADAAD6ADAAD6ADAA00C56E36FFFFFFC0C0C0FFFFFFD3A9A7FFFFFFC0C0C0FF
        FFFFD3A6A6FFFFFFC0C0C0FFFFFFC1838300C56E36FFFFFFFFFFFFFFFFFFD6AE
        ACFFFFFFFFFFFFFFFFFFD5ABABFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7
        D7B2B7D6AEACD6ADAA0000FF0000FF0000FFD6ADAAD6AEACD7AFAFD6AEACC387
        8700C56E36FFFFFFC0C0C0FFFFFF0000FFFFFFFFC0C0C0FFFFFF0000FFFFFFFF
        C0C0C0FFFFFFC2868600C56E36FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
        FFFF0000FFFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7D7B2B7D7B2B7D6AD
        AA0000FF0000FF0000FFD6ADAAD7AFAFD7AFAFDBB7B7C3878700C56E36FFFFFF
        C0C0C0FFFFFFD6ADAAFFFFFFC0C0C0FFFFFFD5AAA9FFFFFFFFFFFFFFFFFFC386
        8500C56E36FFFFFFFFFFFFFFFFFFD8B7BEFFFFFFFFFFFFFFFFFFD7B4BCFFFFFF
        C0C0C0FFFFFFC38C9400C36C33C36C33C46D34C46D34C36D34C46D34C46D34C4
        6D34C36D34C46D34C46D34C46D34C36C3400C56C33C56C33C56C33C56C33CD69
        00CD6900CD6900CD6900CD6900CD6900CD6600CA5F00CD640000C36C33C36C33
        C36C33C46D33C46D34C46D33C46D33C46D33C46D33C46D33C46C33C36C33C36C
        3300}
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 1
      Width = 90
    end
  end
  object ToGB: TGroupBox
    Left = 240
    Top = 80
    Width = 140
    Height = 74
    Caption = ' '#1044#1086': '
    TabOrder = 1
    object ToFormatLab: TLabel
      Left = 14
      Top = 46
      Width = 98
      Height = 13
      Caption = #1092#1086#1088#1084#1072#1090': '#1075#1075#1075'/'#1084#1084'/'#1076#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object ToED: TcxMaskEdit
      Left = 13
      Top = 20
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d?\d?\d\/(0?\d|1?[0-1])\/([0-2]?\d|3?[0-1])'
      Properties.MaxLength = 0
      Properties.OnValidate = FromEDPropertiesValidate
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 0
      Width = 90
    end
    object ToDE: TcxDateEdit
      Left = 13
      Top = 20
      Properties.ButtonGlyph.Data = {
        3E020000424D3E0200000000000036000000280000000D0000000D0000000100
        1800000000000802000000000000000000000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C000C56E35D6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAA
        D6ADAAD6ADAAD6ADAA00C56E36FFFFFFC0C0C0FFFFFFD3A9A7FFFFFFC0C0C0FF
        FFFFD3A6A6FFFFFFC0C0C0FFFFFFC1838300C56E36FFFFFFFFFFFFFFFFFFD6AE
        ACFFFFFFFFFFFFFFFFFFD5ABABFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7
        D7B2B7D6AEACD6ADAA0000FF0000FF0000FFD6ADAAD6AEACD7AFAFD6AEACC387
        8700C56E36FFFFFFC0C0C0FFFFFF0000FFFFFFFFC0C0C0FFFFFF0000FFFFFFFF
        C0C0C0FFFFFFC2868600C56E36FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
        FFFF0000FFFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7D7B2B7D7B2B7D6AD
        AA0000FF0000FF0000FFD6ADAAD7AFAFD7AFAFDBB7B7C3878700C56E36FFFFFF
        C0C0C0FFFFFFD6ADAAFFFFFFC0C0C0FFFFFFD5AAA9FFFFFFFFFFFFFFFFFFC386
        8500C56E36FFFFFFFFFFFFFFFFFFD8B7BEFFFFFFFFFFFFFFFFFFD7B4BCFFFFFF
        C0C0C0FFFFFFC38C9400C36C33C36C33C46D34C46D34C36D34C46D34C46D34C4
        6D34C36D34C46D34C46D34C46D34C36C3400C56C33C56C33C56C33C56C33CD69
        00CD6900CD6900CD6900CD6900CD6900CD6600CA5F00CD640000C36C33C36C33
        C36C33C46D33C46D34C46D33C46D33C46D33C46D33C46D33C46C33C36C33C36C
        3300}
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 1
      Width = 90
    end
  end
  object ValueCB: TcxComboBox
    Left = 5
    Top = 36
    Properties.DropDownListStyle = lsFixedList
    Properties.OnChange = ValueCBChange
    Style.LookAndFeel.Kind = lfStandard
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.Kind = lfStandard
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.Kind = lfStandard
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.Kind = lfStandard
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 3
    Width = 382
  end
  object LastChB: TcxCheckBox
    Left = 5
    Top = 59
    Style.LookAndFeel.Kind = lfStandard
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.Kind = lfStandard
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.Kind = lfStandard
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.Kind = lfStandard
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 4
    OnClick = LastChBClick
    Width = 285
  end
  object NotDop: TcxCheckBox
    Left = 5
    Top = 160
    Caption = #1053#1077' '#1072#1085#1072#1083#1080#1079#1080#1088#1086#1074#1072#1090#1100' '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077' '#1087#1088#1080#1074#1080#1074#1082#1080
    Style.LookAndFeel.Kind = lfStandard
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.Kind = lfStandard
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.Kind = lfStandard
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.Kind = lfStandard
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 5
    Visible = False
    Width = 291
  end
end
