object PackPeriodForm: TPackPeriodForm
  Left = 575
  Top = 315
  BorderIcons = []
  BorderStyle = bsToolWindow
  BorderWidth = 5
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1092#1086#1088#1084#1080#1088#1086#1074#1085#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
  ClientHeight = 210
  ClientWidth = 392
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object CaptionLab: TLabel
    Left = 0
    Top = 0
    Width = 392
    Height = 13
    Align = alTop
    Caption = #1059#1082#1072#1078#1080#1090#1077' '#1087#1077#1088#1080#1086#1076' '#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1103' '#1076#1086#1082#1091#1084#1077#1090#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Bevel3: TBevel
    Left = 0
    Top = 166
    Width = 392
    Height = 3
    Align = alBottom
    Shape = bsTopLine
  end
  object BtnPanel: TPanel
    Left = 0
    Top = 169
    Width = 392
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      392
      41)
    object OkBtn: TcxButton
      Left = 202
      Top = 8
      Width = 90
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      TabOrder = 0
      OnClick = OkBtnClick
      LookAndFeel.NativeStyle = True
    end
    object CancelBtn: TcxButton
      Left = 295
      Top = 8
      Width = 90
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.NativeStyle = True
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 13
    Width = 392
    Height = 147
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 1
    object DateFrLab: TLabel
      Left = 6
      Top = 53
      Width = 13
      Height = 13
      Caption = #1054#1090
      Enabled = False
    end
    object DateToLab: TLabel
      Left = 154
      Top = 53
      Width = 15
      Height = 13
      Caption = #1044#1086
      Enabled = False
    end
    object Label1: TLabel
      Left = 5
      Top = 129
      Width = 382
      Height = 13
      Align = alBottom
      Caption = '* '#1055#1060#1044' - '#1087#1077#1088#1080#1086#1076' '#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 6
      Top = 91
      Width = 54
      Height = 13
      Caption = #1057#1084#1077#1097#1077#1085#1080#1077
    end
    object Bevel1: TBevel
      Left = 5
      Top = 79
      Width = 382
      Height = 5
      Shape = bsTopLine
    end
    object ToFormatLab: TLabel
      Left = 179
      Top = 107
      Width = 98
      Height = 13
      Caption = #1092#1086#1088#1084#1072#1090': '#1075#1075#1075'/'#1084#1084'/'#1076#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object PerDataCB: TcxComboBox
      Left = 136
      Top = 17
      Properties.DropDownListStyle = lsFixedList
      Properties.OnChange = PerDataCBPropertiesChange
      Style.Color = clWindow
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 0
      Width = 135
    end
    object DateFrDE: TcxDateEdit
      Left = 33
      Top = 49
      Properties.ButtonGlyph.Data = {
        3E020000424D3E0200000000000036000000280000000D0000000D0000000100
        1800000000000802000000000000000000000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C000C56E35D6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAA
        D6ADAAD6ADAAD6ADAA00C56E36FFFFFFC0C0C0FFFFFFD3A9A7FFFFFFC0C0C0FF
        FFFFD3A6A6FFFFFFC0C0C0FFFFFFC1838300C56E36FFFFFFFFFFFFFFFFFFD6AE
        ACFFFFFFFFFFFFFFFFFFD5ABABFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7
        D7B2B7D6AEACD6ADAA0000FF0000FF0000FFD6ADAAD6AEACD7AFAFD6AEACC387
        8700C56E36FFFFFFC0C0C0FFFFFF0000FFFFFFFFC0C0C0FFFFFF0000FFFFFFFF
        C0C0C0FFFFFFC2868600C56E36FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
        FFFF0000FFFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7D7B2B7D7B2B7D6AD
        AA0000FF0000FF0000FFD6ADAAD7AFAFD7AFAFDBB7B7C3878700C56E36FFFFFF
        C0C0C0FFFFFFD6ADAAFFFFFFC0C0C0FFFFFFD5AAA9FFFFFFFFFFFFFFFFFFC386
        8500C56E36FFFFFFFFFFFFFFFFFFD8B7BEFFFFFFFFFFFFFFFFFFD7B4BCFFFFFF
        C0C0C0FFFFFFC38C9400C36C33C36C33C46D34C46D34C36D34C46D34C46D34C4
        6D34C36D34C46D34C46D34C46D34C36C3400C56C33C56C33C56C33C56C33CD69
        00CD6900CD6900CD6900CD6900CD6900CD6600CA5F00CD640000C36C33C36C33
        C36C33C46D33C46D34C46D33C46D33C46D33C46D33C46D33C46C33C36C33C36C
        3300}
      Style.Color = clWindow
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 1
      Width = 90
    end
    object DateToDE: TcxDateEdit
      Left = 181
      Top = 49
      Properties.ButtonGlyph.Data = {
        3E020000424D3E0200000000000036000000280000000D0000000D0000000100
        1800000000000802000000000000000000000000000000000000C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C000C56E35D6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAA
        D6ADAAD6ADAAD6ADAA00C56E36FFFFFFC0C0C0FFFFFFD3A9A7FFFFFFC0C0C0FF
        FFFFD3A6A6FFFFFFC0C0C0FFFFFFC1838300C56E36FFFFFFFFFFFFFFFFFFD6AE
        ACFFFFFFFFFFFFFFFFFFD5ABABFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7
        D7B2B7D6AEACD6ADAA0000FF0000FF0000FFD6ADAAD6AEACD7AFAFD6AEACC387
        8700C56E36FFFFFFC0C0C0FFFFFF0000FFFFFFFFC0C0C0FFFFFF0000FFFFFFFF
        C0C0C0FFFFFFC2868600C56E36FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
        FFFF0000FFFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7D7B2B7D7B2B7D6AD
        AA0000FF0000FF0000FFD6ADAAD7AFAFD7AFAFDBB7B7C3878700C56E36FFFFFF
        C0C0C0FFFFFFD6ADAAFFFFFFC0C0C0FFFFFFD5AAA9FFFFFFFFFFFFFFFFFFC386
        8500C56E36FFFFFFFFFFFFFFFFFFD8B7BEFFFFFFFFFFFFFFFFFFD7B4BCFFFFFF
        C0C0C0FFFFFFC38C9400C36C33C36C33C46D34C46D34C36D34C46D34C46D34C4
        6D34C36D34C46D34C46D34C46D34C36C3400C56C33C56C33C56C33C56C33CD69
        00CD6900CD6900CD6900CD6900CD6900CD6600CA5F00CD640000C36C33C36C33
        C36C33C46D33C46D34C46D33C46D33C46D33C46D33C46D33C46C33C36C33C36C
        3300}
      Style.Color = clWindow
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 2
      Width = 90
    end
    object PerTypeCB: TcxComboBox
      Left = 6
      Top = 17
      Properties.DropDownListStyle = lsFixedList
      Properties.Items.Strings = (
        #1057#1086#1074#1087#1072#1076#1072#1077#1090' '#1089' '#1055#1060#1044
        #1043#1086#1076
        #1055#1086#1083#1091#1075#1086#1076#1080#1077
        #1050#1074#1072#1088#1090#1072#1083
        #1052#1077#1089#1103#1094
        #1048#1085#1090#1077#1088#1074#1072#1083' '#1076#1072#1090)
      Properties.OnChange = PerTypeCBPropertiesChange
      Style.Color = clWindow
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 3
      Width = 124
    end
    object YearED: TcxMaskEdit
      Left = 334
      Top = 17
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d{4}'
      Properties.MaxLength = 0
      Properties.OnValidate = YearEDPropertiesValidate
      TabOrder = 4
      Width = 54
    end
    object YearChB: TcxCheckBox
      Left = 284
      Top = 17
      Caption = #1043#1086#1076
      Properties.ImmediatePost = True
      Properties.NullStyle = nssUnchecked
      Properties.OnChange = YearChBPropertiesChange
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 5
      Width = 46
    end
    object ShiftED: TcxMaskEdit
      Left = 181
      Top = 87
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d?\d?\d\/(0?\d|1?[0-1])\/([0-2]?\d|3?[0-1])'
      Properties.MaxLength = 0
      Properties.OnValidate = ShiftEDPropertiesValidate
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 6
      Width = 90
    end
  end
end
