//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "DateConkretiz.h"
#include "Dmf_Auto.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxCheckBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "cxCalendar"
#pragma link "cxGroupBox"
#pragma link "cxRadioGroup"
#pragma resource "*.dfm"
TDateConForm *DateConForm;
//---------------------------------------------------------------------------
__fastcall TDateConForm::TDateConForm(TComponent* Owner)
        : TForm(Owner)
{
  canValidateError = false;
}
//---------------------------------------------------------------------------
void __fastcall TDateConForm::SetType(int AType,TTagNode *ANode, int ASubType, UnicodeString ASubTypeCaption, bool ANotDop)
{
  EDType = AType;
  Result = ANode;
  EDSubType = ASubType;
  LastChB->Checked = false;
  if (EDType == _scDate)
   {
     ValueCB->Clear();
     ValueCB->Properties->Items->Add("�������� ���");
     ValueCB->Properties->Items->Add("������ �� ������� ����");
     ValueCB->Properties->Items->Add("������ �� ���� ��������� ������� ������������ ���������");
     ValueCB->Properties->Items->Add("������ ������������ ���������");
     ValueCB->Properties->Items->Add("������ ���� - ���� ��������� ������� ������������ ���������");
     ValueCB->Properties->Items->Add("������ ��������� - ���� ��������� ������� ������������ ���������");
     ValueCB->Properties->Items->Add("������ �������� - ���� ��������� ������� ������������ ���������");
     ValueCB->Properties->Items->Add("������ ������ - ���� ��������� ������� ������������ ���������");
     LastChB->Caption = ASubTypeCaption;
     if (EDSubType == 0) // ���� �������� ��������
      {
        LastChB->Enabled = false;
        LastChB->Checked = false;
        LastChB->Visible = false;
      }
     else
      LastChB->Enabled = true;
     ValueCBChange(ValueCB);
     NotDop->Visible = ANotDop;
     if (Result->Count >= 3)
      {
        TTagNode *__tmp = Result->GetFirstChild();
        ValueCB->ItemIndex = __tmp->AV["value"].ToInt()>>1;
        ValueCBChange(ValueCB);
        if (LastChB->Enabled)
         LastChB->Checked = (bool)(__tmp->AV["value"].ToInt()&1);
        __tmp = __tmp->GetNext();
        if (__tmp->AV["value"] != "")
         {
           if (ValueCB->ItemIndex == 0) // �������� ���
            FromDE->Text = __tmp->AV["value"];
           else // ������ �� ������� ���� ,������ �� ����
            FromED->Text = __tmp->AV["value"];
         }
        __tmp = __tmp->GetNext();
        if (__tmp->AV["value"] != "")
         {
           if (ValueCB->ItemIndex == 1) // ������ �� ������� ����
            ToDE->Text   = TDateTime::CurrentDate();
           else // ������ �� ���� , �������� ���
            ToDE->Text   = __tmp->AV["value"];
         }
        __tmp = __tmp->GetNext();
        if (__tmp) // ������ �� ...
         {
           if (__tmp->AV["value"] != "")
            {
              ToED->Text = __tmp->AV["value"];
            }
         }
        if (ANotDop)
         {
           LastChBClick(NULL);
           __tmp = __tmp->GetNext();
           if (__tmp) // �� ������������� ��������������
            {
              NotDop->Checked = (bool)(__tmp->AV["value"].ToInt());
            }
         }
      }
   }
  else if (EDType == _scRDate)
   {
     ValueCB->Clear();
     ValueCB->Enabled = false;
     LastChB->Enabled = false;
     LastChB->Caption = "";
     LastChB->Visible = false;
     FromGB->Caption = "��:";
     ToGB->Caption   = "��:";
     FromDE->Visible = false;
     ToDE->Visible   = false;
     FromED->Enabled = true;
     ToED->Enabled   = true;
     if (Result->Count == 2)
      {
        TTagNode *__tmp = Result->GetFirstChild();
        FromED->Text = __tmp->AV["value"];
        __tmp = __tmp->GetNext();
        ToED->Text   = __tmp->AV["value"];
      }
   }
  else
   OkBtn->Enabled = false;
}
//---------------------------------------------------------------------------
bool __fastcall TDateConForm::CheckInput()
{
  UnicodeString _tmpVal;
  if (EDType == _scDate)
   {
      if (ValueCB->ItemIndex == 0)
       { // �������� ���
         if ((!FromDE->Text.Length())&&(!ToDE->Text.Length())&&!LastChB->Checked)
          {
            MessageBox(NULL,"�� ������� �������� ��� ��������� ���","���������",MB_ICONINFORMATION);
            ActiveControl = FromDE;
            return false;
          }
       }
      else if (ValueCB->ItemIndex == 1)
       { // ������ �� ������� ����
         if (FromED->Text == "")
          {
            MessageBox(NULL,"�� ������� �������� ��� �������","���������",MB_ICONINFORMATION);
            ActiveControl = FromED;
            return false;
          }
       }
      else if (ValueCB->ItemIndex == 2)
       { // ������ �� ���� ��������� ������� ������������ ���������
          if (!FromED->Text.Length())
           {
             MessageBox(NULL,"�� ������� �������� ��� �������","���������",MB_ICONINFORMATION);
             ActiveControl = FromED;
             return false;
           }
       }
   }
  else if (EDType == _scRDate)
   {
      if ((FromED->Text == "")&&(ToED->Text == ""))
       {
         MessageBox(NULL,"���������� ������� ������� ���� �� ������ ���������","���������",MB_ICONINFORMATION);
         if (FromED->Text == "")
          ActiveControl = FromED;
         else
          ActiveControl = ToED;
         return false;
       }
      try
       {
         canValidateError = true;
         ActiveControl = FromED;
         if (!FromED->ValidateEdit(true))
          {
            return false;
          }
         ActiveControl = ToED;
         if (!ToED->ValidateEdit(true))
          {
            ActiveControl = ToED;
            return false;
          }
       }
      __finally
       {
         canValidateError = false;
       }
   }
  return true;
}
//---------------------------------------------------------------------------
void __fastcall TDateConForm::OkBtnClick(TObject *Sender)
{
  UnicodeString _tmpVal;
  if (!CheckInput()) return;
  if (EDType == _scDate)
   {
      int FType = ValueCB->ItemIndex << 1;
      int Ch = (int)(LastChB->Checked);
      FType |= Ch;
      if (Result->Count) Result->DeleteChild();
      Result->AddChild("text","name="+ValueCB->Properties->Items->Strings[ValueCB->ItemIndex]+",value="+IntToStr(FType)+",ref=0");
      if (ValueCB->ItemIndex == 0)
       { // �������� ���
         _tmpVal = "";
         if (FromDE->Text.Length())
          {
            _tmpVal = FromDE->Text;
            Result->AddChild("text","name=��:,value="+_tmpVal+",ref=1");
          }
         else
          Result->AddChild("text","name=,value=,ref=1");
         _tmpVal = "";
         if (ToDE->Text.Length())
          {
            _tmpVal = ToDE->Text;
            Result->AddChild("text","name=��:,value="+_tmpVal+",ref=2");
          }
         else
          Result->AddChild("text","name=,value=,ref=2");
         Result->AddChild("text","name=,value=,ref=3");
       }
      else if (ValueCB->ItemIndex == 1)
       { // ������ �� ������� ����
         Result->AddChild("text","name=������:,value="+FromED->Text+",ref=1");
         Result->AddChild("text","name=,value=,ref=2");
         if (ToED->Text.Length())
          Result->AddChild("text","name=�������� �������:,value="+ToED->Text+",ref=3");
         else
          Result->AddChild("text","name=,value=,ref=3");
       }
      else if (ValueCB->ItemIndex == 2)
       { // ������ �� ���� ��������� ������� ������������ ���������
         Result->AddChild("text","name=������:,value="+FromED->Text+",ref=1");
         Result->AddChild("text","name=,value=,ref=2");
         if (ToED->Text.Length())
          Result->AddChild("text","name=�������� �������:,value="+ToED->Text+",ref=3");
         else
          Result->AddChild("text","name=,value=,ref=3");
       }
      else if (ValueCB->ItemIndex >= 3)
       { // ������ ������������ ���������
         // 0 - �������� ���
         // 1 - ������ �� ������� ����
         // 2 - ������ �� ���� ��������� ������� ������������ ���������
         // 3 - ������ ������������ ���������
         // 4 - ������ ���� - ���� ��������� ������� ������������ ���������
         // 5 - ������ ��������� - ���� ��������� ������� ������������ ���������
         // 6 - ������ �������� - ���� ��������� ������� ������������ ���������
         // 7 - ������ ������ - ���� ��������� ������� ������������ ���������
         Result->AddChild("text","name=,value=,ref=1");
         Result->AddChild("text","name=,value=,ref=2");
         if (ToED->Text.Length())
          Result->AddChild("text","name=�������� �������:,value="+ToED->Text+",ref=3");
         else
          Result->AddChild("text","name=,value=,ref=3");
       }
      if (NotDop->Visible && LastChB->Checked)
       {
         Result->AddChild("text","name="+UnicodeString((NotDop->Checked)? "�� ������������� �������������� ��������" : "")+",value="+UnicodeString((NotDop->Checked)? "1" : "0")+",ref=4");
       }
   }
  else if (EDType == _scRDate)
   {
      if (Result->Count) Result->DeleteChild();
      _tmpVal = "";
      if (FromED->Text != "")
       {
         _tmpVal = FromED->Text;
         Result->AddChild("text","name=��:,value="+_tmpVal+",ref=0");
       }
      else
       Result->AddChild("text","name=,value=,ref=0");
      _tmpVal = "";
      if (ToED->Text != "")
       {
         _tmpVal = ToED->Text;
         Result->AddChild("text","name=��:,value="+_tmpVal+",ref=1");
       }
      else
       Result->AddChild("text","name=,value=,ref=1");
   }
  else
   {
      MessageBox(NULL,"������������� ��� ���������","���������",MB_ICONINFORMATION);
      ModalResult = mrCancel;
   }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TDateConForm::ValueCBChange(TObject *Sender)
{
  if (EDType == _scDate)
   {
     if (ValueCB->ItemIndex == -1) ValueCB->ItemIndex = 0;
     if (ValueCB->ItemIndex == 0)
      { // �������� ���
        FromGB->Caption = "�� ����:";
        ToGB->Caption   = "�� ����:";
        FromDE->Visible = true;
        ToDE->Visible   = true;
        ToDE->Text = "";
        FromDE->Text = "";

        FromED->Visible = false;
        ToED->Visible   = false;

        FromFormatLab->Visible = false;
        ToFormatLab->Visible   = false;
      }
     else if (ValueCB->ItemIndex == 1)
      { // ������ �� ������� ����
        FromGB->Caption = "������������ �������:";
        ToGB->Caption   = "��������:";

        FromDE->Visible = false;
        ToDE->Visible   = false;

        FromED->Visible = true;
        FromED->Enabled = true;
        ToED->Visible   = true;

        FromFormatLab->Visible = true;
        ToFormatLab->Visible   = true;
      }
     else if (ValueCB->ItemIndex == 2)
      { // ������ �� ���� ��������� ������� ������������ ���������
        FromGB->Caption = "������������ �������:";
        ToGB->Caption   = "��������:";

        FromDE->Visible = false;
        ToDE->Visible   = false;

        FromED->Visible = true;
        FromED->Enabled = true;
        ToED->Visible   = true;

        FromFormatLab->Visible = true;
        ToFormatLab->Visible   = true;
      }
     else if (ValueCB->ItemIndex >= 3)
      { // ������ ������������ ���������

        // 0 - �������� ���
        // 1 - ������ �� ������� ����
        // 2 - ������ �� ���� ��������� ������� ������������ ���������
        // 3 - ������ ������������ ���������
        // 4 - ������ ���� - ���� ��������� ������� ������������ ���������
        // 5 - ������ ��������� - ���� ��������� ������� ������������ ���������
        // 6 - ������ �������� - ���� ��������� ������� ������������ ���������
        // 7 - ������ ������ - ���� ��������� ������� ������������ ���������


        FromGB->Caption = "";
        ToGB->Caption   = "��������:";

        FromDE->Visible = false;
        ToDE->Visible   = false;

        FromED->Visible = true;
        FromED->Enabled = false;
        ToED->Visible   = true;

        FromFormatLab->Visible = true;
        ToFormatLab->Visible   = true;
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TDateConForm::LastChBClick(TObject *Sender)
{
  if (NotDop->Visible)
   NotDop->Enabled = LastChB->Checked;
  if (!LastChB->Checked)
   NotDop->Checked = false;
}
//---------------------------------------------------------------------------
void __fastcall TDateConForm::FromEDPropertiesValidate(TObject *Sender,
      Variant &DisplayValue, TCaption &ErrorText, bool &Error)
{
  if (Error)
   {
     UnicodeString tmpVal = UnicodeString(DisplayValue).Trim();
     Error = false;
     if (!tmpVal.LastDelimiter("/"))
      { // ����� ���
        DisplayValue = Variant(tmpVal+"/0/0");
      }
     else
      {
        if (tmpVal.Pos("/") == tmpVal.LastDelimiter("/"))
         { // ����� ���+"/"+�������?
           if (tmpVal.LastDelimiter("/") == tmpVal.Length())
            { // ����� ���+"/"
              DisplayValue = Variant(tmpVal+"0/0");
            }
           else
            { // ����� ���+"/"+�������
              DisplayValue = Variant(tmpVal+"/0");
            }
         }
        else
         {
           if (tmpVal.LastDelimiter("/") == tmpVal.Length())
            { // ����� ���+"/"+�������+"/"
              DisplayValue = Variant(tmpVal+"0");
            }
           else
            { // �������, �� �� ������ ���� ���������
              Error = true;
              ErrorText = "������� ������������ �������� ��������.";
            }
         }
      }
   }
}
//---------------------------------------------------------------------------

