//---------------------------------------------------------------------------
#ifndef NomTreeClassH
#define NomTreeClassH
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <StdCtrls.hpp>
#include <Mask.hpp>
//#include "Registry_ICScomp.h"
#include <Buttons.hpp>
#include "cxButtonEdit.hpp"
#include "cxButtons.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include <Menus.hpp>
#include "cxGraphics.hpp"
#include "dxStatusBar.hpp"
#include "XMLContainer.h"
//---------------------------------------------------------------------------
typedef  map<long, UnicodeString,ltstr> TIntStrMap;
class TAutoFunc_DM;
class TNomTreeClassForm : public TForm
{
__published:	// IDE-managed Components
        TTreeView *ClassTree;
        TPanel *BtnPanel;
        TLabel *Label1;
        TPanel *Panel1;
        TcxCheckBox *SelGroupChB;
        TcxButtonEdit *QSearchED;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TdxStatusBar *dxStatusBar1;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall ClassTreeEditing(TObject *Sender, TTreeNode *Node,
          bool &AllowEdit);
        void __fastcall QSearchEDPropertiesButtonClick(TObject *Sender,
          int AButtonIndex);
        void __fastcall OkBtnClick(TObject *Sender);
private:	// User declarations
       int FindPos;

       TIntStrMap  FExtCodes;
       TIntStrMap  FNames;
       TAnsiStrMap FNameByExtCodes;
       UnicodeString __fastcall FGetElementExtCode(long ACode);
       UnicodeString __fastcall FGetElementName(long ACode);
       UnicodeString __fastcall FGetElementNameByExtCode(UnicodeString AExtCode);

       UnicodeString  tmpName,tmpName1,FSQL;
       TAutoFunc_DM   *FDM;
public:		// User declarations
       UnicodeString clText,clExtCode;
       long       clCode;
       bool       clSelectGroup;
       long       clCodeLast;
       __fastcall TNomTreeClassForm(TComponent* Owner,TTagNode* ANode,UnicodeString AClName,UnicodeString AWhere, TAutoFunc_DM *ADM);
       __property UnicodeString ElementExtCode[long] = {read=FGetElementExtCode};
       __property UnicodeString ElementName[long] = {read=FGetElementName};
       __property UnicodeString ElementNameByExtCode[UnicodeString] = {read=FGetElementNameByExtCode};
};
//---------------------------------------------------------------------------
extern PACKAGE TNomTreeClassForm *NomTreeClassForm;
//---------------------------------------------------------------------------
#endif
