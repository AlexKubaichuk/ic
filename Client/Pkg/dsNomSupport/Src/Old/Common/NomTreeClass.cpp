//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdlib.h>
#pragma hdrstop

#include "NomTreeClass.h"
#include "Dmf_Auto.h"
//#include "Registry_ICScomp.h"
//---------------------------------------------------------------------------
//#define AutoFunc_DM->quFreeA AutoFunc_DM->quFree
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxButtons"
#pragma link "cxCheckBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "cxGraphics"
#pragma link "dxStatusBar"
#pragma resource "*.dfm"
TNomTreeClassForm *NomTreeClassForm;
//---------------------------------------------------------------------------
__fastcall TNomTreeClassForm::TNomTreeClassForm(TComponent* Owner,TTagNode* ANode,UnicodeString AClName,UnicodeString AWhere, TAutoFunc_DM *ADM)
	: TForm(Owner)
{
  FDM = ADM;
  FindPos = 0;
  TTagNode *itNode = ANode;
  UnicodeString iCode,iExtCode,iLev,iName,iTabName,iWhere,iTitle,iMess;
  iCode    = itNode->AV["codefield"].UpperCase();
  iExtCode = itNode->AV["extcodefield"].UpperCase();
  iLev     = itNode->AV["levelfield"].UpperCase();
  iName    = itNode->AV["namefield"].UpperCase();
  iTabName = itNode->AV["tblname"].UpperCase();
  iWhere   = AWhere.UpperCase();
  iTitle   = itNode->AV["name"];
  // �������� ���������� �������
  FSQL = "Select COUNT("+iCode+") AS CLCOUNT From "+iTabName;
  if (iWhere != "") FSQL += " Where "+iWhere;
  FDM->qtExec(FSQL);
  iMess = "�������� �������������� \""+iTitle+"\" ��� \""+AClName+"\"";
  // ������������ ��������
  FSQL = "Select "+iCode+","+iExtCode+","+iLev+","+iName+" From "+iTabName;
  if (iWhere != "") FSQL += " Where "+iWhere;
  FSQL += " Order By  "+iCode;
  FDM->qtExec(FSQL);
  TTreeNode *clRoot;
  TList *tmpNodes = new TList;
  int Level, tmpLevel;
  Level = 0;
  TTreeNode *tmpNode1;
  TTreeNode *tmpNode2;
  clRoot = ClassTree->Items->Add(NULL, iTitle);
  tmpNodes->Add(clRoot);
  long subL = 0;
  long sCode = -1;
  int sLev = 0;
  try
   {
     while (!FDM->quFreeA->Eof)
      {
        tmpLevel = FDM->quFreeA->FN(iLev.c_str())->AsInteger;
        tmpLevel -= subL;
        if (tmpLevel > (Level+1))
         {
           sCode = (long)FDM->quFreeA->FN(iCode.c_str())->AsInteger;
           sLev = tmpLevel;
           subL = tmpLevel - 1;
           tmpLevel -= subL;
         }
        if (tmpLevel < Level)
         {
           if ((sLev >= FDM->quFreeA->FN(iLev.c_str())->AsInteger)||((sCode+1)!=(long)FDM->quFreeA->FN(iCode.c_str())->AsInteger))
            {
              tmpLevel = 1;
              sLev = FDM->quFreeA->FN(iLev.c_str())->AsInteger;
              subL = FDM->quFreeA->FN(iLev.c_str())->AsInteger - 1;
            }
         }
        else
         {
           if ((sCode+1)!=(long)FDM->quFreeA->FN(iCode.c_str())->AsInteger)
            {
              tmpLevel = 1;
              sLev = FDM->quFreeA->FN(iLev.c_str())->AsInteger;
              subL = FDM->quFreeA->FN(iLev.c_str())->AsInteger - 1;
            }
         }
        sCode = (long)FDM->quFreeA->FN(iCode.c_str())->AsInteger;
        tmpNode2 = (TTreeNode*)tmpNodes->Items[tmpLevel - 1];
        tmpName = FDM->quFreeA->FN(iExtCode.c_str())->AsString+": "+FDM->quFreeA->FN(iName.c_str())->AsString;
        tmpNode1 = ClassTree->Items->AddChildObject(tmpNode2,tmpName.c_str(),(void*)((long)FDM->quFreeA->FN(iCode.c_str())->AsInteger));
        if (Level != tmpLevel)
         {
           if (tmpLevel == tmpNodes->Count) tmpNodes->Add(tmpNode1);
           else tmpNodes->Items[tmpLevel] = tmpNode1;
         }
        else
         tmpNodes->Items[tmpLevel] = tmpNode1;
        Level = tmpLevel;

        FExtCodes[(long)FDM->quFreeA->FN(iCode.c_str())->AsInteger] = FDM->quFreeA->FN(iExtCode.c_str())->AsString;
        FNames[(long)FDM->quFreeA->FN(iCode.c_str())->AsInteger] = FDM->quFreeA->FN(iName.c_str())->AsString;
        FNameByExtCodes[FDM->quFreeA->FN(iExtCode.c_str())->AsString] = FDM->quFreeA->FN(iName.c_str())->AsString;

        FDM->quFreeA->Next();
      }
   }
  catch (Exception &E)
   {
     MessageBox(NULL,E.Message.c_str(),"������ �������� ��������������",MB_ICONSTOP);
   }
  FDM->quFreeA->Close();
#ifndef _ADO_
  if (FDM->trFreeA->Active) FDM->trFreeA->Rollback();
#endif
  if (tmpNodes) delete tmpNodes;
  tmpNodes = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TNomTreeClassForm::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if (Key == VK_RETURN) OkBtnClick(this);
  if (Shift.Contains(ssCtrl) && Key == 'F')
   QSearchEDPropertiesButtonClick(QSearchED,0);
}
//---------------------------------------------------------------------------
void __fastcall TNomTreeClassForm::ClassTreeEditing(TObject *Sender,
      TTreeNode *Node, bool &AllowEdit)
{
  AllowEdit = false;
}
//---------------------------------------------------------------------------
void __fastcall TNomTreeClassForm::QSearchEDPropertiesButtonClick(
      TObject *Sender, int AButtonIndex)
{
  if (ClassTree->Items->Count)
   {
     ActiveControl = QSearchED;
     tmpName = QSearchED->Text.UpperCase();
     if (!ClassTree->Selected)
       ClassTree->Select(ClassTree->Items->Item[0]);
     if (tmpName == "")
      ClassTree->Select(ClassTree->Items->Item[0]);
     else
      {
        long i;
        for (i = ClassTree->Selected->AbsoluteIndex+1; i < ClassTree->Items->Count; i++)
         {
           if (ClassTree->Items->Item[i]->Text.UpperCase().Pos(tmpName))
            {
              FindPos = i;
              ClassTree->Select(ClassTree->Items->Item[i]);
              return;
            }
         }
        for (i = 0; i <= ClassTree->Selected->AbsoluteIndex; i++)
         {
           if (ClassTree->Items->Item[i]->Text.UpperCase().Pos(tmpName))
            {
              FindPos = i;
              ClassTree->Select(ClassTree->Items->Item[i]);
              return;
            }
         }
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TNomTreeClassForm::OkBtnClick(TObject *Sender)
{
  if (!ClassTree->Selected) return;
  clText = ClassTree->Selected->Text;
  int Ind = clText.Pos(": ");
  if (Ind < 1) {ActiveControl = QSearchED; return;}
  clCode = (long)ClassTree->Selected->Data;
  clExtCode = clText.SubString(1,Ind-1);
  clText    = clText.SubString(Ind+2,clText.Length()-Ind-1);
  clSelectGroup = SelGroupChB->Checked;
  clCodeLast = -1;
  if (clSelectGroup)
   {
     if (ClassTree->Selected->HasChildren)
      {
        TTreeNode *lastChild = ClassTree->Selected;
        while (lastChild->GetLastChild())
         lastChild = lastChild->GetLastChild();
        clCodeLast = (long)lastChild->Data;
      }
     else
      {
        clCodeLast = (long)ClassTree->Selected->Data;
      }
   }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TNomTreeClassForm::FGetElementExtCode(long ACode)
{
  UnicodeString RC = "";
  try
   {
     TIntStrMap::iterator FRC = FExtCodes.find(ACode);
     if (FRC != FExtCodes.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
       TIntStrMap  FNames;
       TAnsiStrMap FNameByExtCodes;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TNomTreeClassForm::FGetElementName(long ACode)
{
  UnicodeString RC = "";
  try
   {
     TIntStrMap::iterator FRC = FNames.find(ACode);
     if (FRC != FNames.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TNomTreeClassForm::FGetElementNameByExtCode(UnicodeString AExtCode)
{
  UnicodeString RC = "";
  try
   {
     TAnsiStrMap::iterator FRC = FNameByExtCodes.find(AExtCode);
     if (FRC != FNameByExtCodes.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

