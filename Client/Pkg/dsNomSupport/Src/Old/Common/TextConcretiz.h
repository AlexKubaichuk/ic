//---------------------------------------------------------------------------

#ifndef TextConcretizH
#define TextConcretizH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "Dmf_Auto.h"
#include "cxButtons.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxTextEdit.hpp"
#include <Menus.hpp>
#include "cxDropDownEdit.hpp"
#include "cxGraphics.hpp"
#include "cxMaskEdit.hpp"
//---------------------------------------------------------------------------
class TTextConForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TLabel *TextLab;
        TcxTextEdit *TextED;
        TcxCheckBox *FullWordChB;
        TcxCheckBox *CaseSensChB;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TcxComboBox *CompTypeCB;
        TLabel *Label1;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall CompTypeCBPropertiesChange(TObject *Sender);
private:	// User declarations
        TTagNode *Result;
        TAutoFunc_DM   *FDM;
public:		// User declarations
        __fastcall TTextConForm(TComponent* Owner, UnicodeString ACapt,TTagNode *ANode, TAutoFunc_DM *ADM);
};
//---------------------------------------------------------------------------
extern PACKAGE TTextConForm *TextConForm;
//---------------------------------------------------------------------------
#endif
