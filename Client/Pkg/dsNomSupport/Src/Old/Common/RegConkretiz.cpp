//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "RegConkretiz.h"
#include "NomTreeClass.h"
#include "icsOrgEdit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxLookAndFeelPainters"
#pragma resource "*.dfm"
TRegConForm *RegConForm;
//---------------------------------------------------------------------------
__fastcall TRegConForm::TRegConForm(TComponent *Owner, TAutoFunc_DM *ADM,UnicodeString ATemplXML, TTagNode *ParamRoot, UnicodeString ARootGUI, bool AisAll, bool isNeedDef, bool AIsSubSel)
        : TForm(Owner)
{
  FDM = ADM;
  FIsSubSel = AIsSubSel;
  IsAll = AisAll;
  Result = ParamRoot;
  FTempXML = new TTagNode(NULL);
  FTempXML->AsXML = ATemplXML;
  UnicodeString tagNum = "0";
  FTempXML->Iterate(FSetValues,tagNum);
  TmplNode = new TTagNode(NULL);
  FRootNode = FDM->XMLList->GetXML(ARootGUI);

  TmplNode->Assign(FRootNode,true);
/*
      tmp += "<fl ref='00F1' def='"+IntToStr(DM->LPUParam.LPUCode)+"' en='0'/>";
      if (DM->LPUParam.LPUPartCode != -1)
       tmp += "<fl ref='00F9' def='"+IntToStr(DM->LPUParam.LPUPartCode)+"' en='0'/>";//req='1'
      else
       tmp += "<fl ref='00F9'/>"; //req='1'
*/
  TTagNode *FLPUNode = FTempXML->GetChildByAV("fl", "ref", "00F1");
  if (FLPUNode)
   {
     FLPUNode->AV["def"] = IntToStr(FDM->LPUCode);
     FLPUNode->AV["en"] = "0";
   }
  FLPUNode = FTempXML->GetChildByAV("fl", "ref", "00F9");
  if (FLPUNode)
   {
     if (FDM->LPUPartCode != -1)
      {
        FLPUNode->AV["def"] = IntToStr(FDM->LPUPartCode);
        FLPUNode->AV["en"] = "0";
      }
   }
  FTempl = new TICSTemplate(FTempXML->AsXML, CompPanel, FDM->quFreeA, "", "", TmplNode, clInfoBk);

  FTempl->UpperFunc = FDM->UpperFunc;
  FTempl->OnTreeBtnClick = FOnTreeBtnClick;
  FTempl->OnExtBtnClick = FExtBtnClick;
  FTempl->OnGetExtWhere = FExtGetWhere;
  CompPanel->Height = CompPanel->Height+5;
  Height = CompPanel->Height+(Height-ClientHeight)+BtnPanel->Height;
  Width = CompPanel->Width+6;
}
//---------------------------------------------------------------------------
void __fastcall TRegConForm::OkBtnClick(TObject *Sender)
{
  ActiveControl = OkBtn;
  if (!FTempl->CheckInput()) return;
  Result->DeleteChild();
  if (FTempl->IsEmpty()&&IsAll)
   {
     Result->AddChild("text","name=���,value=1");
     Result->AddChild("text","name=sql,value=1=1");
   }
  else
   {
     TTagNode *__tmp, *__tmp2;
     UnicodeString FlList = "";
     FTempXML->Iterate(GetFl,FlList);
     TStringList *FFlList = NULL;
     try
      {
        FFlList = new TStringList;
        FFlList->Text = FlList;
        for (int i = 0; i < FFlList->Count; i++)
         {
           if (FTempl->GetValue(FFlList->Strings[i]) != "")
            {
              __tmp = Result->AddChild("text");
              __tmp->AddAttr("name",FTempl->GetName(FFlList->Strings[i]));
              __tmp->AddAttr("value",FTempl->GetValue(FFlList->Strings[i]));
              __tmp->AddAttr("ref",IntToStr(i));
              __tmp2 = TmplNode->GetTagByUID(FFlList->Strings[i]);
              if (__tmp2)
               {
                 if (__tmp2->CmpName("extedit"))
                  {
                    if (__tmp2->CmpAV("isedit","no")&& !__tmp2->CmpAV("depend",""))
                     __tmp->AddAttr("show","0");
                  }
               }
            }
         }
      }
     __finally
      {
        if (FFlList) delete FFlList;
      }
     if (!FTempl->IsEmpty())
      {
        __tmp = Result->AddChild("text","name=sql");
        UnicodeString FTabAlias = "";
        TTagNode *NomTag = FDM->XMLList->GetRefer(Result->AV["ref"]);
        if (NomTag)
         {
           NomTag = NomTag->GetTagByUID(NomTag->AV["param"]);
           if (NomTag)
            {
              NomTag = NomTag->GetParent("is");
              if (NomTag)
               FTabAlias = NomTag->AV["tabalias"];
            }
         }
        if (FIsSubSel)
         {
           TStringList *tmpSelList = new TStringList;
           __tmp->AddAttr("value",FTempl->GetWhereClRep(FTabAlias,tmpSelList));
           for (int i = 0; i < tmpSelList->Count; i++)
            __tmp->AddChild("subtext")->AddAttr("value",tmpSelList->Strings[i]);
           delete tmpSelList;
         }
        else
         __tmp->AddAttr("value",FTempl->GetWhere(FTabAlias));
      }
   }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
bool __fastcall TRegConForm::GetFl(TTagNode *itTag, UnicodeString &UIDs)
{
  if (itTag->CmpName("fl"))
   UIDs += itTag->AV["ref"]+"\n";
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TRegConForm::FormDestroy(TObject *Sender)
{
//  FTempl->TreeFormList = NULL;
  delete FTempl;
  delete FTempXML;
  delete TmplNode;
}
//---------------------------------------------------------------------------
void __fastcall TRegConForm::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if (Key == VK_RETURN) {OkBtnClick(OkBtn); Key = 0;}
}
//---------------------------------------------------------------------------
bool __fastcall TRegConForm::FSetValues(TTagNode *itTag, UnicodeString &TagNum)
{
  if (itTag->CmpName("fl"))
   {
     TTagNode *sText = Result->GetChildByAV("text","ref",TagNum);
     if (sText) itTag->AV["def"] = sText->AV["value"].Trim();
     TagNum = IntToStr(TagNum.ToInt()+1);
   }
  return false;
}
//---------------------------------------------------------------------------
bool __fastcall TRegConForm::FOnTreeBtnClick(TTagNode *ItTag, bool &AIsGroup, UnicodeString &ACodeBeg, UnicodeString &ACodeEnd, UnicodeString &AExtCode, UnicodeString &AText, bool isClicked)
{
  bool RC = false;
  try
   {
     if (isClicked)
      {
        UnicodeString FRef = (FDM->GetXMLGUI(ItTag)+"."+ItTag->AV["uid"]).UpperCase();
        TNomTreeClassForm *Dlg = FDM->TreeFormList->TreeClassForm[FRef];
        Dlg->Caption = ItTag->AV["dlgtitle"];
        Dlg->ShowModal();
        if (Dlg->ModalResult == mrOk)
         {
           AIsGroup = Dlg->clSelectGroup;
           ACodeBeg = IntToStr(Dlg->clCode);
           ACodeEnd = IntToStr(Dlg->clCodeLast);
           AExtCode = Dlg->clExtCode;
           AText =    Dlg->clText;
           RC = true;
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TRegConForm::FOnGetOrgList(bool ATemplate, long AGroup, TStringList *ADest)
{
  if (AGroup != -1)
   FDM->qtExec("Select CODE, R0008 from class_0001 Where R011A="+IntToStr(AGroup), false);
  else
   FDM->qtExec("Select CODE, R0008 from class_0001", false);
  ADest->Clear();
  if (ATemplate)
   ADest->AddObject("",(TObject*)0);
  while (!FDM->quFreeA->Eof)
   {
     ADest->AddObject(FDM->quFreeA->FN("R0008")->AsString,(TObject*)FDM->quFreeA->FN("CODE")->AsInteger);
     FDM->quFreeA->Next();
   }
  if (FDM->quFreeA->Transaction->Active) FDM->quFreeA->Transaction->Commit();
}
//---------------------------------------------------------------------------
bool __fastcall TRegConForm::FExtBtnClick(TTagNode *ItTag, UnicodeString &Src,
      TDataSource *ASource, bool isClicked, TObject *ACtrlList, int ABtnIdx)
{
  TRegEDContainer *CtrList = (TRegEDContainer*)ACtrlList;
  bool RC = false;
  try
   {
//##############################################################################
//#                                                                            #
//#            click                                                           #
//#                                                                            #
//##############################################################################
     if (isClicked)
      {
        UnicodeString FSrc = Src.Trim();
        if (ItTag->CmpAV("uid","0062,0052,50AD,002A"))
         { // �����
//           if (!FSrc.Length())
//             FSrc = UnicodeString(FDM->AppOpt->Vals["defaultaddr"]).Trim();
           if (FDM->KLADR->Execute(!ASource, FSrc))
            {
              RC = true;
              Src = FDM->KLADR->AddrCode+"="+FDM->KLADR->codeToText(FDM->KLADR->AddrCode);
            }
         }
        else if (ItTag->CmpAV("uid","0041,0043"))
         {// ����������� �
           TStringList *OrgValList = NULL;
           TStringList *OrgGrValList = NULL;
           TStringList *LearnPerList = NULL;
           TicsOrgEditForm *Dlg = NULL;
           bool FBase, FCWork;
//           FTemplate = !ASource;
           FBase = ItTag->CmpAV("uid","0041");
           UnicodeString FCode = "";
           try
            {
              if (FBase)
               {
                 FCWork = (CtrList->TemplateItems["0045"]->GetValue("") == "1");
                 LearnPerList = new TStringList;
                 FInvadeClassCB(LearnPerList, 0, true ,TmplNode->GetTagByUID("0057"), false, (TDataSet*)FDM->quFreeA, "","", TmplNode->GetTagByUID("0057"));
               }
              else
               FCWork = (CtrList->TemplateItems["0044"]->GetValue("") == "1");
              OrgValList = new TStringList;
              OrgGrValList = new TStringList;
              FInvadeClassCB(OrgValList, 0, true ,TmplNode->GetTagByUID("0001"), false, (TDataSet*)FDM->quFreeA, "","", TmplNode->GetTagByUID("0001"));

              Dlg = new TicsOrgEditForm(this, true, FBase, FCWork, OrgGrValList, OrgValList, LearnPerList);
              Dlg->Code = Src.ToIntDef(0);
              if (FBase)
               {
                 Dlg->L1 = CtrList->TemplateItems["0004"]->GetValue("");
                 Dlg->L2 = CtrList->TemplateItems["0025"]->GetValue("");
                 Dlg->L3 = CtrList->TemplateItems["0026"]->GetValue("");
                 Dlg->OrgPlace = CtrList->TemplateItems["0005"]->GetValue("");
                 Dlg->LearnPer = CtrList->TemplateItems["0014"]->GetValue("").ToIntDef(-1);
               }
              else
               {
                 Dlg->OrgPlace = CtrList->TemplateItems["0015"]->GetValue("");
                 Dlg->LearnPer = -1;
               }
              Dlg->ShowModal();
              if (Dlg->ModalResult == mrOk)
               {
                 UnicodeString FSrc = "0=\n";
                 if (Dlg->Code)
                  FSrc = IntToStr(Dlg->Code)+"="+Dlg->StrCode+"\n";
                 if (FBase)
                  {
                    FSrc += " "+Dlg->L1;
                    FSrc += " "+Dlg->L2;
                    FSrc += " "+Dlg->L3+"  ";
                    if (FCWork)
                     FSrc += "  "+Dlg->OrgPlace;
                    else
                     FSrc += "  "+Dlg->StrLearnPer;

                    CtrList->TemplateItems["0004"]->SetValue(Dlg->L1);
                    CtrList->TemplateItems["0025"]->SetValue(Dlg->L2);
                    CtrList->TemplateItems["0026"]->SetValue(Dlg->L3);
                    CtrList->TemplateItems["0005"]->SetValue(Dlg->OrgPlace);
                    if (Dlg->LearnPer)
                     CtrList->TemplateItems["0014"]->SetValue(IntToStr(Dlg->LearnPer));
                  }
                 else
                  {
                    FSrc += "\n"+Dlg->OrgPlace;
                    CtrList->TemplateItems["0015"]->SetValue(Dlg->OrgPlace);
                  }
                 Src = FSrc;
                 RC = true;
               }
            }
           __finally
            {
              if (Dlg) delete Dlg;
              if (OrgValList) delete OrgValList;
              if (OrgGrValList) delete OrgGrValList;
              if (LearnPerList) delete LearnPerList;
            }
         }
        else if (ItTag->CmpAV("uid","0025,0115"))
         {// ����������� �,�
           TStringList *OrgValList = NULL;
           TStringList *OrgGroupValList = NULL;
           TStringList *LearnPerList = NULL;
           TicsOrgEditForm *Dlg = NULL;
           bool FBase;
           bool FtmpWork;
           bool FTemplate;
           FTemplate = true;
           FBase = ItTag->CmpAV("uid","0025");
           UnicodeString FCode = "";
           try
            {
              if (FBase)
               {
                 FtmpWork = (CtrList->GetEDValue(FTemplate, "0023") == "1");
                 LearnPerList = new TStringList;
                 FInvadeClassCB(LearnPerList, 0, FTemplate ,TmplNode->GetTagByUID("0108"), false, (TDataSet*)FDM->quFreeA, "","", TmplNode->GetTagByUID("0108"));
               }
              else
               {
                 FtmpWork = (CtrList->GetEDValue(FTemplate,"0113") == "1");
               }
              OrgValList      = new TStringList;
              OrgGroupValList = new TStringList;
              FInvadeClassCB(OrgGroupValList, 0, FTemplate ,TmplNode->GetTagByUID("0117"), false, (TDataSet*)FDM->quFreeA, "","", TmplNode->GetTagByUID("0117"));
              FInvadeClassCB(OrgValList,      0, FTemplate ,TmplNode->GetTagByUID("0001"), false, (TDataSet*)FDM->quFreeA, "","", TmplNode->GetTagByUID("0001"));

              Dlg = new TicsOrgEditForm(this, FTemplate, FBase, FtmpWork, OrgGroupValList, OrgValList, LearnPerList);
              Dlg->OnGetOrgList = FOnGetOrgList;
              if (FBase)
               Dlg->GroupCode = CtrList->GetEDValue(FTemplate,"011B").ToIntDef(-1);
              else
               Dlg->GroupCode = CtrList->GetEDValue(FTemplate,"011C").ToIntDef(-1);
              Dlg->Code = Src.ToIntDef(-1);
              if (FBase)
               {
                 Dlg->L1 = CtrList->GetEDValue(FTemplate,"0026");
                 Dlg->L2 = CtrList->GetEDValue(FTemplate,"0027");
                 Dlg->L3 = CtrList->GetEDValue(FTemplate,"0028");
                 Dlg->OrgPlace = CtrList->GetEDValue(FTemplate,"0111");
                 Dlg->LearnPer = CtrList->GetEDValue(FTemplate,"0112").ToIntDef(-1);
               }
              else
               Dlg->OrgPlace = CtrList->GetEDValue(FTemplate,"0114");
              Dlg->ShowModal();
              if (Dlg->ModalResult == mrOk)
               {
                 UnicodeString FSrc = IntToStr(Dlg->Code)+"="+Dlg->StrCode+"\n";
                 if (FBase)
                  {
                    FSrc += " "+Dlg->L1;
                    FSrc += " "+Dlg->L2;
                    FSrc += " "+Dlg->L3+"  ";
                    if (FtmpWork)
                     FSrc += "  "+Dlg->OrgPlace;
                    else
                     FSrc += "  "+Dlg->StrLearnPer;
                    if (FBase)
                     CtrList->SetEDValue(FTemplate,"011B",IntToStr(Dlg->GroupCode));
                    else
                     CtrList->SetEDValue(FTemplate,"011C",IntToStr(Dlg->GroupCode));

                    CtrList->SetEDValue(FTemplate,"0026",Dlg->L1);
                    CtrList->SetEDValue(FTemplate,"0027",Dlg->L2);
                    CtrList->SetEDValue(FTemplate,"0028",Dlg->L3);
                    CtrList->SetEDValue(FTemplate,"0111",Dlg->OrgPlace);
                    CtrList->SetEDValue(FTemplate,"0112",IntToStr(Dlg->LearnPer));
                  }
                 else
                  {
                    FSrc += "\n"+Dlg->OrgPlace;
                    CtrList->SetEDValue(FTemplate,"0114",Dlg->OrgPlace);
                  }
                 Src = FSrc;
                 RC = true;
               }
            }
           __finally
            {
              if (Dlg)
               {
                 Dlg->OnGetOrgList = NULL;
                 delete Dlg;
               }
              if (OrgGroupValList) delete OrgGroupValList;
              if (OrgValList)      delete OrgValList;
              if (LearnPerList)    delete LearnPerList;
            }
         }
        else if (ItTag->CmpAV("uid","004B"))
         {// �������� �� ������������
         }
      }
     else
      {
//##############################################################################
//#                                                                            #
//#      get edit value                                                        #
//#                                                                            #
//##############################################################################
        RC = true;
        if (ItTag->CmpAV("uid","0062,0052,50AD,002A"))
         { // �����
           Src = Src+"="+FDM->KLADR->codeToText(Src);
         }
        else if (ItTag->CmpAV("uid","0041"))
         {// ����������� �
            UnicodeString FSrc = Src;
            if ((CtrList->TemplateItems["0045"]->GetValue("").ToIntDef(0) > 0))
             {
               FSrc = Src+"="+FDM->GetClassData(Src.ToIntDef(-1),"0001")+"\n";
               FSrc += " "+CtrList->TemplateItems["0004"]->GetValue("").Trim();
               FSrc += " "+CtrList->TemplateItems["0025"]->GetValue("").Trim();
               FSrc += " "+CtrList->TemplateItems["0026"]->GetValue("").Trim();
               if ((CtrList->TemplateItems["0045"]->GetValue("").ToIntDef(0) == 1))
                FSrc += "  "+CtrList->TemplateItems["0005"]->GetValue("");
               else
                FSrc += "  "+FDM->GetClassData(CtrList->TemplateItems["0014"]->GetValue("").ToIntDef(-1),"0057");
             }
            Src = FSrc.Trim();
         }
        else if (ItTag->CmpAV("uid","0043"))
         {// ����������� �
            UnicodeString FSrc = Src;
            if ((CtrList->TemplateItems["0044"]->GetValue("").ToIntDef(0) > 0))
             {
               FSrc = Src+"="+FDM->GetClassData(Src.ToIntDef(-1),"0001")+" ";
               FSrc += CtrList->TemplateItems["0015"]->GetValue("");
             }
            Src = FSrc;
         }
        else if (ItTag->CmpAV("uid","0025"))
         {// ����������� �
            UnicodeString FSrc = Src;
            if (ASource)
             {
               if ((CtrList->EditItems["0023"]->GetValue("").ToIntDef(0) > 0))
                {
                  FSrc = Src+"="+FDM->GetClassData(Src.ToIntDef(-1),"0001")+"\n";
                  FSrc += " "+CtrList->EditItems["0026"]->GetValue("").Trim();
                  FSrc += " "+CtrList->EditItems["0027"]->GetValue("").Trim();
                  FSrc += " "+CtrList->EditItems["0028"]->GetValue("").Trim();
                  if ((CtrList->EditItems["0023"]->GetValue("").ToIntDef(0) == 1))
                   FSrc += "  "+CtrList->EditItems["0111"]->GetValue("");
                  else
                   FSrc += "  "+FDM->GetClassData(CtrList->EditItems["0112"]->GetValue("").ToIntDef(-1),"0108");
                }
             }
            Src = FSrc;
         }
        else if (ItTag->CmpAV("uid","0115"))
         {// ����������� �
            UnicodeString FSrc = Src;
            if (ASource)
             {
               if ((CtrList->EditItems["0113"]->GetValue("").ToIntDef(0) > 0))
                {
                  FSrc = Src+"="+FDM->GetClassData(Src.ToIntDef(-1),"0001")+"\n";
                  FSrc += CtrList->EditItems["0114"]->GetValue("");
                }
             }
            Src = FSrc;
         }
        else if (ItTag->CmpAV("uid","004B"))
         {// �������� �� ������������
            UnicodeString FSrc = "-";
            if (ASource)
             {
//               FSrc = DM->GetDiagList(FIBDS(ASource)->FN("CODE")->AsInteger, 2, NULL, NULL).Trim();
               if (FSrc.Length())
                FSrc = "���.="+FSrc;
             }
            Src = FSrc;
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TRegConForm::FExtGetWhere(TTagNode *ItTag, UnicodeString &Src,
      TDataSource *ASource, bool isClicked, TObject *ACtrlList, int ABtnIdx)
{
  TRegEDContainer *CtrList = (TRegEDContainer*)ACtrlList;
  bool RC = false;
  try
   {
     UnicodeString FSrc = "";
     if (ItTag->CmpAV("uid","0062,0052,50AD,002A"))
      {// �����
        FSrc = Src.Trim();
        if (FSrc.Length() > 30) FSrc = FSrc.SubString(1,30);
        int i = -1;
        for (i = FSrc.Length(); i > 0; i--)
         {
           if (FSrc[i] != '0') break;
         }
        if (i != -1) FSrc = FSrc.SubString(1,i);
        if (FSrc.Length())
         {
           if (ItTag->CmpAV("uid","0062"))
            Src = "CB.R0062 starting with '"+FSrc+"'";
           else if (ItTag->CmpAV("uid","50AD"))
            Src = "CB.R50AD starting with '"+FSrc+"'";
           else if (ItTag->CmpAV("uid","002A"))
            Src = "CB.R002A starting with '"+FSrc+"'";
           else
            Src = "CL01.R0052 starting with '"+FSrc+"'";
           RC = true;
         }
      }
     else if (ItTag->CmpAV("uid","0041"))
      {// ����������� � ����
        FSrc = "";
        if ((CtrList->TemplateItems["0045"]->GetValue("").ToIntDef(0) > 0))
         {

           FSrc += SetValIfNotNull(CtrList->TemplateItems["0041"]->GetValue(""), "CB.R0041", FSrc.Length(), false);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0004"]->GetValue(""), "CB.R0004", FSrc.Length(), true);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0025"]->GetValue(""), "CB.R0025", FSrc.Length(), true);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0026"]->GetValue(""), "CB.R0026", FSrc.Length(), true);
           if ((CtrList->TemplateItems["0045"]->GetValue("").ToIntDef(0) == 1))
            FSrc += SetValIfNotNull(CtrList->TemplateItems["0005"]->GetValue(""), "CB.R0005", FSrc.Length(), true);
           else
            FSrc += SetValIfNotNull(CtrList->TemplateItems["0014"]->GetValue(""), "CB.R0014", FSrc.Length(), false);
         }
        if (FSrc.Trim().Length())
         {
           Src = "("+FSrc+")";
           RC = true;
         }
      }
     else if (ItTag->CmpAV("uid","0025"))
      {// ����������� � ��
        FSrc = "";
        if ((CtrList->TemplateItems["0023"]->GetValue("").ToIntDef(0) > 0))
         {
           FSrc += SetValIfNotNull(CtrList->TemplateItems["011B"]->GetValue(""), "CB.R011B", FSrc.Length(), false);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0025"]->GetValue(""), "CB.R0025", FSrc.Length(), false);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0026"]->GetValue(""), "CB.R0026", FSrc.Length(), true);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0027"]->GetValue(""), "CB.R0027", FSrc.Length(), true);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0028"]->GetValue(""), "CB.R0028", FSrc.Length(), true);
           if ((CtrList->TemplateItems["0023"]->GetValue("").ToIntDef(0) == 1))
            FSrc += SetValIfNotNull(CtrList->TemplateItems["0111"]->GetValue(""), "CB.R0111", FSrc.Length(), true);
           else
            FSrc += SetValIfNotNull(CtrList->TemplateItems["0112"]->GetValue(""), "CB.R0112", FSrc.Length(), false);
         }
        if (FSrc.Trim().Length())
         {
           Src = "("+FSrc+")";
           RC = true;
         }
      }
     else if (ItTag->CmpAV("uid","0043"))
      {// ����������� � ����
        FSrc = "";
        if ((CtrList->TemplateItems["0044"]->GetValue("").ToIntDef(0) > 0))
         {
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0043"]->GetValue(""), "CB.R0043", FSrc.Length(), false);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0015"]->GetValue(""), "CB.R0015", FSrc.Length(), true);
         }
        if (FSrc.Trim().Length())
         {
           Src = "("+FSrc+")";
           RC = true;
         }
      }
     else if (ItTag->CmpAV("uid","0115"))
      {// ����������� � ��
        FSrc = "";
        if ((CtrList->TemplateItems["0113"]->GetValue("").ToIntDef(0) > 0))
         {
           FSrc += SetValIfNotNull(CtrList->TemplateItems["011C"]->GetValue(""), "CB.R011C", FSrc.Length(), false);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0115"]->GetValue(""), "CB.R0115", FSrc.Length(), false);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0114"]->GetValue(""), "CB.R0114", FSrc.Length(), true);
         }
        if (FSrc.Trim().Length())
         {
           Src = "("+FSrc+")";
           RC = true;
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TRegConForm::SetValIfNotNull(UnicodeString AVal, UnicodeString AId,bool AAnd, bool AQuote)
{
  UnicodeString RC = "";
  try
   {
     if ((!AQuote&&(AVal.ToIntDef(0) > 0))||(AQuote && AVal.Trim().Length()))
      {
        if (AAnd) RC = " and";
        if (AQuote)
         RC += " "+FDM->UpperFunc+"("+AId+")='"+AVal.Trim().UpperCase()+"'";
        else
         RC += " "+AId+"="+AVal.Trim();
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

