//---------------------------------------------------------------------------
#ifndef PackPeriodH
#define PackPeriodH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxCalendar.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxMaskEdit.hpp"
#include "cxRadioGroup.hpp"
#include "cxSpinEdit.hpp"
#include "cxTextEdit.hpp"
#include "cxCheckBox.hpp"
//---------------------------------------------------------------------------
class PACKAGE TPackPeriodForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *CaptionLab;
        TPanel *BtnPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TPanel *Panel3;
        TcxComboBox *PerDataCB;
        TLabel *DateFrLab;
        TcxDateEdit *DateFrDE;
        TLabel *DateToLab;
        TcxDateEdit *DateToDE;
        TcxComboBox *PerTypeCB;
        TLabel *Label1;
        TcxMaskEdit *YearED;
        TcxCheckBox *YearChB;
        TLabel *Label2;
        TBevel *Bevel1;
        TLabel *ToFormatLab;
        TcxMaskEdit *ShiftED;
        TBevel *Bevel3;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall PerDataCBPropertiesChange(TObject *Sender);
        void __fastcall PerTypeCBPropertiesChange(TObject *Sender);
        void __fastcall YearEDPropertiesValidate(TObject *Sender,
          Variant &DisplayValue, TCaption &ErrorText, bool &Error);
        void __fastcall YearChBPropertiesChange(TObject *Sender);
        void __fastcall ShiftEDPropertiesValidate(TObject *Sender,
          Variant &DisplayValue, TCaption &ErrorText, bool &Error);
private:	// User declarations

        unsigned short FCY,FCM,FCD;
//        int       FpdType;  // "year" 5, "halfyear 4; "quarter 3;  "month" 2;  "unique" 1;  ERR 0
        UnicodeString __fastcall GetLastDay(int AMons, int AYear);
//        TDocExDM *FDM;

        int __fastcall FGetPerType();
        int  __fastcall FGetPerSubType();

        UnicodeString  __fastcall FGetPerTypeStr();
        UnicodeString  __fastcall FGetPerSubTypeStr();

        UnicodeString  __fastcall FGetPerYear();

        UnicodeString  __fastcall FGetPerDateFr();
        UnicodeString  __fastcall FGetPerDateTo();

        UnicodeString  __fastcall FGetPerOffset();

public:		// User declarations
        __fastcall TPackPeriodForm(TComponent* Owner);

        __property int Type = {read=FGetPerType};
        __property int SubType = {read=FGetPerSubType};

        __property UnicodeString TypeStr = {read=FGetPerTypeStr};
        __property UnicodeString SubTypeStr = {read=FGetPerSubTypeStr};

        __property UnicodeString Year = {read=FGetPerYear};

        __property UnicodeString DateFr = {read=FGetPerDateFr};
        __property UnicodeString DateTo = {read=FGetPerDateTo};

        __property UnicodeString Offset = {read=FGetPerOffset};
};
//---------------------------------------------------------------------------
extern PACKAGE TPackPeriodForm *PackPeriodForm;
//---------------------------------------------------------------------------
extern PACKAGE int __fastcall GetPeriodValue(int AType, int ASubType, UnicodeString AYear,
                              UnicodeString ADateFr, UnicodeString ADateTo,
                              UnicodeString AOffset,
                              int ADocPerType, TDate &ADocPerDateFr, TDate &ADocPerDateTo);
#endif
