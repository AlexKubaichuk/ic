//---------------------------------------------------------------------------
#ifndef Dmf_AutoH
#define Dmf_AutoH
#include "FIBDatabase.hpp"
#include "FIBQuery.hpp"
#include "pFIBDatabase.hpp"
#include "pFIBQuery.hpp"
#include <Classes.hpp>
#include "XMLContainer.h"
#include "NomTreeClass.h"
#include "KLADR.h"

#define _scError    0
#define _scDigit    1
#define _scChoice   2
#define _scDiapazon 3
#define _scDate     4
#define _scRDate    5

//#include "datadef.h"

#define AsTextDate AsDateTime.FormatString("dd.mm.yyyy")
#include "ICSNomSupport_TLB.h"
#include "ICSClassData.h"
//---------------------------------------------------------------------------
typedef map<UnicodeString,TNomTreeClassForm*> TTreeFormMap;
//typedef map<long ,UnicodeString> TLongMap;
typedef map<UnicodeString,TAnsiStrMap*> TTreeValuesMap;
//typedef map<UnicodeString,TLongMap*> TClassValuesMap;

//---------------------------------------------------------------------------
class TAutoFunc_DM;
class TICSTreeList : public TObject
{
private:
    TTreeFormMap     FFormList;
    TTreeValuesMap   FTreeValues;
    TAxeXMLContainer *XMLList;
    TNomTreeClassForm* __fastcall FGetTreeForm(UnicodeString ARef);
    TTagNode *FReg;
    TAutoFunc_DM *FDM;
public:
    UnicodeString __fastcall GetValue(UnicodeString AUID,UnicodeString AVal);
__published:
    __property TNomTreeClassForm* TreeClassForm[UnicodeString ARef] = {read=FGetTreeForm};
    __fastcall TICSTreeList(TAxeXMLContainer *AXMLList,UnicodeString ARegGUI,TAutoFunc_DM *ADM);
    __fastcall ~TICSTreeList();
};
//---------------------------------------------------------------------------
class TAutoFunc_DM : public TObject
{
private:	// User declarations
//       bool __fastcall GetTreeCh(void *uiTag, UnicodeString &UID);
    UnicodeString FRegGUI;
    UnicodeString FRegKey;
    UnicodeString FDBName;
    UnicodeString FMainLPUCode;
    UnicodeString FUser;
    UnicodeString FUserPwd;
    UnicodeString FCharset;
    UnicodeString FUpperFunc;
    TAxeXMLContainer *FXMLList;
    TKLADRFORM *FKLADR;
//    TClassValuesMap   FClassValues;
//    TLongMap* __fastcall GetClassHash(UnicodeString AClassUID);
public:		// User declarations
    int LPUCode;
    int LPUPartCode;
    TICSClassData *FClassData;
    TpFIBDatabase *AUTOFUNC_DB;
    TpFIBQuery *quFreeA;
    TpFIBQuery *quComm;
    TpFIBTransaction *trDefaultAUTO;
    TpFIBTransaction *trFreeA;
    TpFIBTransaction *trComm;
    // ������ ������
    TAnsiStrMap  VacList;
    TAnsiStrMap  VacListCode;
    // ������ ����
    TAnsiStrMap  ProbList;
    TAnsiStrMap  ProbListCode;
    // ������ ��������
    TAnsiStrMap  InfList;
    TAnsiStrMap  InfListCode;
    int FPr1,FPr2,FPrC;
    TNomTreeClassForm *MKBDlg;
    TAnsiStrMap MKB;
    TAnsiStrMap MrkUnits;
    TAnsiStrMap LPUList;
    TList *tmpChTree;
    TTagNode *Reg;
    TTagNode *MKBClRoot;
    TICSTreeList *TreeFormList;
    void __fastcall qtExec(UnicodeString ASQL, bool aCommit = false);
    void __fastcall qcExec(UnicodeString ASQL, bool aCommit = false);
    void __fastcall CreateTreeFormList();
    int  __fastcall GetScaleStrs(TTagNode *ANode, TStringList *AList);
    UnicodeString __fastcall GetXMLProjectCode(TTagNode* ANode);
    UnicodeString __fastcall GetXMLGUI(TTagNode* ANode);
    UnicodeString __fastcall GetScaleText(TTagNode *ANode, int AVal);
    UnicodeString __fastcall GetDiapazonStr(TTagNode *AOut, int AVal);
    UnicodeString __fastcall GetSelScaleStr(TTagNode *ANode, int AVal);
    UnicodeString __fastcall GetTreeScaleStr(TTagNode *ANode, int AVal);
    UnicodeString __fastcall GetTreeScaleStrVal(TTagNode *itNode, int* ICount, int AVal);
    UnicodeString __fastcall GetScaleUID(TTagNode *ANode);
    void       __fastcall ClearHash();
    void       __fastcall ExecProc(UnicodeString AProcName, TpFIBQuery *AQuery, UnicodeString AParamName, Variant AParam);
    UnicodeString __fastcall GetClassData(long ACode, UnicodeString AName);

    bool __fastcall DefRegValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE, UnicodeString ATemplate, UnicodeString AGUI,bool AisAll, bool AIsSubSel);
    bool __fastcall DefChVal(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE, TStringList *AValues, TStringList *ACodeValues = NULL);
//    void __fastcall CreateTreeFormList();
    void __fastcall Connect();
    void __fastcall Disconnect();
    __fastcall TAutoFunc_DM();
    __fastcall ~TAutoFunc_DM();

    __property UnicodeString RegGUI    = {read=FRegGUI, write=FRegGUI};
    __property UnicodeString RegKey    = {read=FRegKey, write=FRegKey};

    __property UnicodeString MainLPUCode = {read=FMainLPUCode, write=FMainLPUCode};

    __property UnicodeString DBName    = {read=FDBName, write=FDBName};
    __property UnicodeString User      = {read=FUser, write=FUser};
    __property UnicodeString UserPwd   = {read=FUserPwd, write=FUserPwd};
    __property UnicodeString Charset   = {read=FCharset, write=FCharset};
    __property UnicodeString UpperFunc = {read=FUpperFunc, write=FUpperFunc};
    __property TAxeXMLContainer* XMLList = {read=FXMLList, write=FXMLList};
    __property TKLADRFORM *KLADR    = {read=FKLADR};
};
//---------------------------------------------------------------------------
/*
extern void __fastcall GetTreeValues(TTagNode *ANode, TStringList *AList, int *Count);
*/
extern int __fastcall PregnancyStrToInt(UnicodeString AVal);
extern UnicodeString __fastcall IntToPregnancyStr(const int &AVal);
extern Extended __fastcall ICSStrToFloat(UnicodeString AVal);
extern UnicodeString __fastcall GetDocPeriodValStr(UnicodeString APerType, UnicodeString AValFrom, UnicodeString AValTo);

//extern PACKAGE TAutoFunc_DM *AutoFunc_DM;
//---------------------------------------------------------------------------
#endif

