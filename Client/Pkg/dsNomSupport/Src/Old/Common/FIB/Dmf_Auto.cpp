//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <math.h>
#include "Dmf_Auto.h"
#include "RegConkretiz.h"
#include "InqCon.h"
#include "DxLocale.h"
#include "NomTreeClass.h"

#define quFNStr(a) quFreeA->FN(a)->AsString
#define quFNInt(a) quFreeA->FN(a)->AsInteger
#define quFNFloat(a) quFreeA->FN(a)->AsFloat
#define quFNDate(a) quFreeA->FN(a)->AsDateTime
//---------------------------------------------------------------------------
#pragma package(smart_init)
//TAutoFunc_DM *AutoFunc_DM;
//---------------------------------------------------------------------------
__fastcall TAutoFunc_DM::TAutoFunc_DM()
{
  AUTOFUNC_DB = new TpFIBDatabase(NULL);
  AUTOFUNC_DB->DefaultTransaction = trDefaultAUTO;
  AUTOFUNC_DB->SQLDialect = 3;
  AUTOFUNC_DB->Timeout = 0;
  AUTOFUNC_DB->LibraryName = "fbclient.dll";
  AUTOFUNC_DB->WaitForRestoreConnect = 0;

  trDefaultAUTO = new TpFIBTransaction(NULL);
  trDefaultAUTO->DefaultDatabase = AUTOFUNC_DB;
  trDefaultAUTO->TimeoutAction = TARollback;

  trFreeA = new TpFIBTransaction(NULL);
  trFreeA->DefaultDatabase = AUTOFUNC_DB;
  trFreeA->TimeoutAction = TARollback;

  quFreeA = new TpFIBQuery(NULL);
  quFreeA->Transaction = trFreeA;
  quFreeA->Database = AUTOFUNC_DB;

  trComm = new TpFIBTransaction(NULL);
  trComm->DefaultDatabase = AUTOFUNC_DB;
  trComm->TimeoutAction = TARollback;

  quComm = new TpFIBQuery(NULL);
  quComm->Transaction = trComm;
  quComm->Database = AUTOFUNC_DB;

  FKLADR = new TKLADRFORM(NULL);
  FKLADR->Database = AUTOFUNC_DB;
  FKLADR->RegionEnabled = true;
  FKLADR->NasPunktEnabled = true;
  FKLADR->StreetEnabled = true;
  FKLADR->StreetRequired = true;
  FKLADR->StreetVisible = true;
  FKLADR->RequiredColor = clInfoBk;

  TreeFormList = NULL;
  MKBDlg = NULL;
  MKBClRoot = new TTagNode(NULL);
  MKBClRoot->AV["codefield"] = "CODE";
  MKBClRoot->AV["extcodefield"] = "EXTCODE";
  MKBClRoot->AV["levelfield"] = "LEV";
  MKBClRoot->AV["namefield"] = "NAME";
  MKBClRoot->AV["tblname"] = "MKB";
  MKBClRoot->AV["name"] = "���-10";
  XMLList = NULL;
}
//---------------------------------------------------------------------------
__fastcall TAutoFunc_DM::~TAutoFunc_DM()
{
  delete FClassData;
//  if (FNomType == intIMM)
//   {
     VacList.clear();
     VacListCode.clear();
     ProbList.clear();
     ProbListCode.clear();
     InfList.clear();
     InfListCode.clear();
//   }
//  if (FNomType == intAKDO)
//   {
//     LPUList.clear();
//   }
  MKB.clear();
  if (TreeFormList)  delete TreeFormList;
  if (MKBDlg) delete MKBDlg;
  if (MKBClRoot) delete MKBClRoot;
  XMLList = NULL;
  if (AUTOFUNC_DB->Connected) AUTOFUNC_DB->Close();

  for (int i = 0; i < 50; i++)
   {
     Application->ProcessMessages();
     Sleep(10);
   }
  delete FKLADR; 
  delete trFreeA;
  delete trDefaultAUTO;

  delete quFreeA;
  delete AUTOFUNC_DB;
}
//---------------------------------------------------------------------------
#include "NomTreeClass.h"
void __fastcall TAutoFunc_DM::Connect()
{
  LPUCode = -1;
  LPUPartCode = -1;

#ifdef _ADO_
  AUTOFUNC_DB->ConnectionString = FDBName;
#else
  AUTOFUNC_DB->DBParams->Clear();
  AUTOFUNC_DB->DBParams->Add(("user_name="+FUser.LowerCase()).c_str());
  AUTOFUNC_DB->DBParams->Add(("password="+FUserPwd.LowerCase()).c_str());
  AUTOFUNC_DB->DBParams->Add("sql_role_name=");
  if (FCharset.Length())
   AUTOFUNC_DB->DBParams->Add(("lc_ctype="+FCharset.LowerCase()).c_str());

  AUTOFUNC_DB->DBName = UnicodeString(FDBName);
#endif
  try
   {
     AUTOFUNC_DB->Open();
     CreateTreeFormList();
     FClassData = new TICSClassData(XMLList->GetXML(FRegGUI), quFreeA);

//     if ((FNomType == intAKDO)||(FNomType == intIMM))
//      {
        MKB.clear();
        qtExec("Select CODE,EXTCODE,NAME From MKB ",false);
        while (!quFreeA->Eof)
         {
           MKB[quFreeA->FN("CODE")->AsString] =
                            quFreeA->FN("EXTCODE")->AsString +" "+
                            quFreeA->FN("NAME")->AsString;
           quFreeA->Next();
         }

//        MKBDlg = new TNomTreeClassForm(Application->Owner,MKBClRoot,"���-10","");
        MKBDlg = new TNomTreeClassForm(Application->Owner,MKBClRoot,"���-10","", this);
        MKBDlg->Caption = "���-10";
//      }
/*     if (FNomType == intAKDO)
      {
        LPUList.clear();
        try
         {

           qtExec("Select Count (rdb$relation_name) as TCOUNT From rdb$relations Where Upper(rdb$relation_name) = 'LPULIST'",false);
           if (quFreeA->FN("TCOUNT")->AsInteger)
            {
              qtExec("Select * From LPULIST",false);
              while (!quFreeA->Eof)
               {
                 LPUList[quFreeA->FN("CODE")->AsString] =
                                      quFreeA->FN("NAME")->AsString;
                 quFreeA->Next();
               }
            }
         }
        catch(...)
         {
         }
      }
      */
//     if (FNomType == intIMM)
//      {
        try
         {
           qtExec("SELECT code FROM CLASS_00C7 where R0104=1");
           if (quFreeA->RecordCount)
            LPUCode     = quFNInt("code");

           qtExec("SELECT code FROM CLASS_000C Where R0105=1");
           if (quFreeA->RecordCount)
            LPUPartCode     = quFNInt("code");
         }
        catch (...)
         {
           LPUCode     = -1;
           LPUPartCode = -1;
         }

        qtExec("Select CODE, R0040 From CLASS_0035");
        while (!quFreeA->Eof)
         {// ������ ������ ������
           VacList[quFNStr("CODE")] = quFNStr("R0040");
           VacListCode[quFNStr("R0040")] = quFNStr("CODE");
           quFreeA->Next();
         }
        qtExec("Select CODE, R002C From CLASS_002A");
        while (!quFreeA->Eof)
         {// ������ ������ ����
           ProbList[quFNStr("CODE")] = quFNStr("R002C");
           ProbListCode[quFNStr("R002C")] = quFNStr("CODE");
           quFreeA->Next();
         }
        qtExec("Select CODE, R003C From CLASS_003A");
        while (!quFreeA->Eof)
         {// ������ ������ ��������
           InfList[quFNStr("CODE")] = quFNStr("R003C");
           InfListCode[quFNStr("R003C")] = quFNStr("CODE");
           quFreeA->Next();
         }
        trFreeA->Commit();
//      }
     FPr1 = 0;
     FPr2 = 0;
     FPrC = 0;
/*     if (FNomType == intCRDS)
      {
        qtExec("Select R0034, R0035 From CLASS_0033");
        while (!quFreeA->Eof)
         {// ���������� ������� ����� �� ���������� + �����������
           if (quFNInt("R0035") == 1)
            FPr1 = quFNInt("R0034");
           if (quFNInt("R0035") == 2)
            FPr2 = quFNInt("R0034");
           if (quFNInt("R0035") == 3)
            FPrC = quFNInt("R0034");
           quFreeA->Next();
         }
        trFreeA->Commit();

        qtExec("Select CODE, R0026 From CLASS_000A");
        while (!quFreeA->Eof)
         {// ������ ������ ��������
           MrkUnits[quFNStr("CODE")] = quFNStr("R0026");
//           InfListCode[quFNStr("R003C")] = quFNStr("CODE");
           quFreeA->Next();
         }
        trFreeA->Commit();
      }
*/
     DxLocale();
   }
  catch (Exception &E)
   {
     ShowMessage(E.Message);
   }
  //````````````````````````````````````````````````````````````
}
//---------------------------------------------------------------------------
void __fastcall TAutoFunc_DM::Disconnect()
{
  if (TreeFormList)
   {
     delete TreeFormList;
     TreeFormList = NULL;
   }
}
//---------------------------------------------------------------------------
void __fastcall TAutoFunc_DM::CreateTreeFormList()
{
  TreeFormList = new TICSTreeList(XMLList,FRegGUI,this);
}
//---------------------------------------------------------------------------
void __fastcall TAutoFunc_DM::qtExec(UnicodeString ASQL, bool aCommit)
{
   if (!trFreeA->Active) trFreeA->StartTransaction();
   quFreeA->Close();
   quFreeA->SQL->Clear();
   quFreeA->SQL->Text = ASQL;
   quFreeA->Prepare();
   quFreeA->ExecQuery();
   if (trFreeA->Active&&aCommit) trFreeA->Commit();
}
//---------------------------------------------------------------------------
void __fastcall TAutoFunc_DM::qcExec(UnicodeString ASQL, bool aCommit)
{
   if (!quComm->Transaction->Active) quComm->Transaction->StartTransaction();
   quComm->Close();
   quComm->SQL->Clear();
   quComm->SQL->Text = ASQL;
   quComm->Prepare();
   quComm->ExecQuery();
   if (quComm->Transaction->Active&&aCommit) quComm->Transaction->Commit();
}
//---------------------------------------------------------------------------
void __fastcall GetTreeValues(TTagNode *ANode, TStringList *AList, int *Count)
{
  TTagNode *itNode = ANode->GetFirstChild();
  if (ANode->CmpName("treegroup"))
   AList->AddObject(ANode->AV["name"],(TObject*)(*Count));
  else
   {
    if (ANode->GetParent()->CmpName("treegroup"))
     AList->AddObject(ANode->GetParent()->AV["name"]+":"+ANode->AV["name"],(TObject*)(*Count));
    else
     AList->AddObject(ANode->AV["name"],(TObject*)(*Count));
   }
  (*Count)++;
  while (itNode)
   {
     GetTreeValues(itNode, AList,Count);
     itNode = itNode->GetNext();
   }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAutoFunc_DM::GetScaleUID(TTagNode *ANode)
{
   TTagNode *iTag = ANode;
   if (iTag == NULL) return "";
   if (iTag->GetAttrByName("scaleuid"))
    {
      if (!iTag->AV["scaleuid"].Length()) return GetScaleUID(iTag->GetParent());
      else                                return iTag->AV["scaleuid"];
    }
   return GetScaleUID(iTag->GetParent());
}
//---------------------------------------------------------------------------
int __fastcall TAutoFunc_DM::GetScaleStrs(TTagNode *ANode, TStringList *AList)
{
   TTagNode *Scale = ANode->GetTagByUID(GetScaleUID(ANode));
   if (Scale->CmpName("scale_binary"))
    {
      AList->AddObject("�������",(TObject*)1);
      AList->AddObject("�� �������",(TObject*)0);
    }
   else if (Scale->CmpName("scale_select"))
    {
      Scale = Scale->GetFirstChild();
      while (Scale)
       {
         AList->AddObject(Scale->AV["name"],(TObject*)Scale->AV["value"].ToInt());
         Scale = Scale->GetNext();
       }
    }
   else if (Scale->CmpName("scale_digit"))
    {
      if (Scale->AV["decimal"].ToIntDef(0) == 0)
       {
         AList->AddObject("min",(TObject*)Scale->AV["min"].ToInt());
         AList->AddObject("max",(TObject*)Scale->AV["max"].ToInt());
       }
      else
       {
         AList->AddObject("min",(TObject*)((int)(ICSStrToFloat(Scale->AV["min"])*(10*Scale->AV["decimal"].ToIntDef(0)))));
         AList->AddObject("max",(TObject*)((int)(ICSStrToFloat(Scale->AV["max"])*(10*Scale->AV["decimal"].ToIntDef(0)))));
         AList->AddObject("scale",(TObject*)Scale->AV["decimal"].ToIntDef(0));
       }
      return _scDigit;
    }
   else if (Scale->CmpName("scale_diapazon"))
    {
      Scale = Scale->GetFirstChild();
      while (Scale)
       {
         if (Scale->AV["value"] == "<<")
          AList->AddObject(Scale->AV["name"],(TObject*)Scale->GetParent()->AV["min"].ToInt());
         else if (Scale->AV["value"] == ">>")
          AList->AddObject(Scale->AV["name"],(TObject*)Scale->GetParent()->AV["max"].ToInt());
         else
          AList->AddObject(Scale->AV["name"],(TObject*)Scale->AV["value"].ToInt());
         Scale = Scale->GetNext();
       }
      return _scDiapazon;
    }
   else if (Scale->CmpName("scale_tree"))
    {
      Scale = Scale->GetFirstChild();
      int COu = 0;
      if (Scale->CmpName("tree_int"))
       GetTreeValues(Scale,AList,&COu);
      else
       {
         qtExec("Select * From "+Scale->AV["tblname"]);
         while (!quFreeA->Eof)
          {
            AList->AddObject((quFNStr(Scale->AV["extcodefield"])+": "+quFNStr(Scale->AV["namefield"])).c_str(),(TObject*)quFNInt(Scale->AV["codefield"]));
            quFreeA->Next();
          }
       }
    }
   else
    {
      MessageBox(NULL,"������������� ��� �����","���������",MB_ICONINFORMATION);
      return _scError;
    }
   return _scChoice;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAutoFunc_DM::GetXMLProjectCode(TTagNode* ANode)
{
   return (ANode->GetRoot()->GetChildByName("passport")->AV["projectcode"].UpperCase());
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAutoFunc_DM::GetXMLGUI(TTagNode* ANode)
{
   return (ANode->GetRoot()->GetChildByName("passport")->AV["GUI"].UpperCase());
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAutoFunc_DM::GetScaleText(TTagNode *ANode, int AVal)
{
   TTagNode *Scale = ANode->GetTagByUID(GetScaleUID(ANode));
   if (Scale->CmpName("scale_binary,scale_select"))
    {
      return GetSelScaleStr(ANode, AVal);
    }
   else if (Scale->CmpName("scale_diapazon"))
    {
      return GetDiapazonStr(ANode, AVal);
    }
   else if (Scale->CmpName("scale_tree"))
    {
      return GetTreeScaleStr(ANode, AVal);
    }
   else
    {
      MessageBox(NULL,"������������� ��� �����","���������",MB_ICONINFORMATION);
      return "";
    }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAutoFunc_DM::GetDiapazonStr(TTagNode *AOut, int AVal)
{
   TTagNode *Scale = AOut->GetTagByUID(GetScaleUID(AOut));
   int xMin,xMax;
   UnicodeString xVal;
   xMin = Scale->AV["min"].ToInt();
   xMax = Scale->AV["max"].ToInt();
   Scale = Scale->GetFirstChild();
   while (Scale)
    {
      xVal = Scale->AV["value"];
      if (xVal == "<<")
       {
         if (AVal < xMin) return Scale->AV["name"];
       }
      else if (xVal == ">>")
       {
         if (AVal > xMax) return Scale->AV["name"];
       }
      else
       {
         if (AVal <= xVal.ToInt()) return Scale->AV["name"];
       }
      Scale = Scale->GetNext();
    }
   return "";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAutoFunc_DM::GetSelScaleStr(TTagNode *ANode, int AVal)
{
   TTagNode *itScale = ANode->GetTagByUID(GetScaleUID(ANode));
   if (itScale->CmpName("scale_binary"))
    {
      if (AVal > 0) return "��";
    }
   else
    {
      itScale = itScale->GetFirstChild();
      while (itScale)
       {
         if (itScale->AV["value"].ToInt() == AVal)
          {
            return itScale->AV["name"];
          }
         itScale = itScale->GetNext();
       }
    }
   return "";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAutoFunc_DM::GetTreeScaleStr(TTagNode *ANode, int AVal)
{
   TTagNode *itScale = ANode->GetTagByUID(GetScaleUID(ANode))->GetFirstChild();
   int iC = 0;
   if (itScale->CmpName("tree_int"))
    return GetTreeScaleStrVal(itScale,&iC,AVal);
   else
    {
       qtExec("Select * From "+itScale->AV["tblname"]+" Where "+itScale->AV["codefield"]+"="+IntToStr(AVal));
       if (quFreeA->RecordCount)
        return quFreeA->FN(itScale->AV["extcodefield"])->AsString+": "+quFreeA->FN(itScale->AV["namefield"])->AsString;
       else
        return "";
    }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAutoFunc_DM::GetTreeScaleStrVal(TTagNode *itNode, int* ICount, int AVal)
{
   TTagNode *itScale = itNode->GetFirstChild();
   UnicodeString RC = "";
   while (itScale)
    {
      if (*ICount == AVal)
       return itScale->AV["name"];
      *ICount = *ICount+1;
      if (itScale->Count)
       {
         RC = GetTreeScaleStrVal(itScale,ICount,AVal);
         if (RC != "") return RC;
       }
      itScale = itScale->GetNext();
    }
   return "";
}
//---------------------------------------------------------------------------
//#include "Unit1.h"
bool __fastcall TAutoFunc_DM::DefRegValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE, UnicodeString ATemplate, UnicodeString AGUI, bool AisAll, bool AIsSubSel)
{
   bool iRC = false;
   UnicodeString tmp;
   if (NeedKonkr)
    {
      UnicodeString FGUI;
      if (AGUI == "")
       {
         TTagNode *FISCondRules = XMLList->GetRefer(AIE->AV["ref"]);
         TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
         TTagNode *FExtAttr = XMLList->GetRefer(FISAttr->AV["ref"]);
         FGUI = FExtAttr->GetRoot()->GetChildByName("passport")->AV["gui"];
       }
      else
       FGUI = AGUI;
      TRegConForm *Dlg = NULL;
      try
       {
         Dlg = new TRegConForm(NULL, this, ATemplate, AIE, FGUI, AisAll, true, AIsSubSel);
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         TTagNode *__tmp = AIE->GetFirstChild();
         tmp = "";
         TReplaceFlags rFlag;
         rFlag << rfReplaceAll;
         while (__tmp)
          {
            if (!__tmp->CmpAV("name","sql"))
             {
               if (__tmp->AV["show"].ToIntDef(1))
                tmp += " "+StringReplace(__tmp->AV["name"], "\n", " ", rFlag);
             }
            __tmp = __tmp->GetNext();
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
bool __fastcall TAutoFunc_DM::DefChVal(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE, TStringList *AValues, TStringList *ACodeValues)
{
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = XMLList->GetRefer(AIE->AV["ref"]);
      TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      TConForm *ConDlg = NULL;
      try
       {
         ConDlg = new TConForm(Application->Owner, this, ACodeValues);
         ConDlg->MsgLab->Caption = FISAttr->AV["name"];
         ConDlg->ValueCB->Properties->Items->Assign(AValues);
         ConDlg->SetType(_scChoice,AIE);
         ConDlg->ShowModal();
       }
      __finally
       {
         if (ConDlg) delete ConDlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         __tmp = AIE->GetFirstChild();
         tmp = "";
         while (__tmp->GetNext())
          {
            tmp += " "+__tmp->AV["name"];
            __tmp = __tmp->GetNext();
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
void __fastcall TAutoFunc_DM::ExecProc(UnicodeString AProcName, TpFIBQuery *AQuery, UnicodeString AParamName, Variant AParam)
{
   Variant DDD[1];
   DDD[0] = AParam;
   if (AQuery->Transaction->Active) AQuery->Transaction->Rollback();
   AQuery->Close();
   AQuery->SQL->Clear();
   AQuery->Transaction->StartTransaction();
   AQuery->ExecProcedure(AProcName.c_str(),DDD,0);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAutoFunc_DM::GetClassData(long ACode, UnicodeString AName)
{
  UnicodeString RC = "";
  try
   {
     RC = FClassData->GetData(AName, ACode);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TAutoFunc_DM::ClearHash()
{
  FClassData->ClearHash("");
}
//---------------------------------------------------------------------------
__fastcall TICSTreeList::TICSTreeList(TAxeXMLContainer *AXMLList,UnicodeString ARegGUI,TAutoFunc_DM *ADM)
{
  FDM = ADM;
  XMLList = AXMLList;
  FReg = XMLList->GetXML(ARegGUI);
}
//---------------------------------------------------------------------------
__fastcall TICSTreeList::~TICSTreeList()
{
   for (TTreeValuesMap::iterator i = FTreeValues.begin(); i != FTreeValues.end(); i++)
    delete i->second;
   for (TTreeFormMap::iterator i = FFormList.begin(); i != FFormList.end(); i++)
    delete i->second;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSTreeList::GetValue(UnicodeString AUID, UnicodeString AVal)
{
  TTreeValuesMap::iterator RC = FTreeValues.find(AUID.UpperCase());
  if (RC != FTreeValues.end())
   return (*(FTreeValues[AUID.UpperCase()]))[AVal];
  else
   return "";
}
//---------------------------------------------------------------------------
TNomTreeClassForm* __fastcall TICSTreeList::FGetTreeForm(UnicodeString ARef)
{
  TTreeFormMap::iterator RC = FFormList.find(_UID(ARef).UpperCase());
  if (RC == FFormList.end())
   {
     TTagNode *FTreeChoice = XMLList->GetRefer(ARef);
     if (FTreeChoice)
      {
        UnicodeString FName,FWhere;
        FName  = FTreeChoice->AV["name"];
        FWhere = FTreeChoice->AV["wherestr"];
        TTagNode *TreeClass = FReg->GetTagByUID(FTreeChoice->AV["ref"]);
        if (TreeClass)
         {
           FFormList[_UID(ARef).UpperCase()] = new TNomTreeClassForm(Application->Owner,TreeClass,FName,FWhere,FDM);
           FTreeValues[_UID(ARef).UpperCase()] = new TAnsiStrMap;
           TAnsiStrMap *ValMap = FTreeValues[_UID(ARef).UpperCase()];
           UnicodeString FCodeFL, FExtCodeFL, FNameFL, FTabName;
           FCodeFL    = TreeClass->AV["codefield"];
           FExtCodeFL = TreeClass->AV["extcodefield"];
           FNameFL    = TreeClass->AV["namefield"];
           FTabName   = TreeClass->AV["tblname"];
           FDM->qtExec("Select "+FCodeFL+", "+FExtCodeFL+", "+FNameFL+" From "+FTabName,false);
           while (!FDM->quFreeA->Eof)
            {
              (*ValMap)[FDM->quFreeA->FN(FCodeFL)->AsString] =
                          FDM->quFreeA->FN(FExtCodeFL)->AsString +" "+
                          FDM->quFreeA->FN(FNameFL)->AsString;
              FDM->quFreeA->Next();
            }
         }
      }
   }
  return FFormList[_UID(ARef).UpperCase()];
}
//---------------------------------------------------------------------------
int __fastcall PregnancyStrToInt(UnicodeString AVal)
{
  UnicodeString FWeek,FDay;
  FWeek = GetPart1(AVal,'�');
  FDay = GetPart1(GetPart2(AVal,'�'),'�');
  if (!FDay.Length()) FDay = "0";
  return FWeek.ToIntDef(0)*7+FDay.ToIntDef(0);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall IntToPregnancyStr(const int &AVal)
{
  if (AVal%7) return IntToStr(AVal/7)+"�"+IntToStr(AVal%7)+"�";
  else        return IntToStr(AVal/7)+"�";
}
//---------------------------------------------------------------------------
/*
void __fastcall TAutoFunc_DM::CreateTreeFormList()
{
   // ������������ ������ ���������������
   Reg = XMLList->GetXML("40381E23-92155860-4448");
   // ������������ ������ ���������� ���������������
   tmpChTree = new TList;
   Reg->Iterate(GetTreeCh,UnicodeString(""));
   TTagNode *xN, *itNode;
   for (int i = 0; i < tmpChTree->Count; i++)
    {
      xN = (TTagNode*)tmpChTree->Items[i];
      itNode = Reg->GetTagByUID(xN->AV["ref"]);
      TreeFormList->AddObject(xN->AV["uid"], (TObject*)new TNomTreeClassForm(this,itNode,xN->AV["name"],xN->AV["wherestr"]));
    }
#ifdef _IMM
//   TTagNode *ImmMKB = XMLList->GetXML("4031DA5E-8AC52FB3-7D03")->GetTagByUID("1082");
   TreeFormList->AddObject("1082", (TObject*)new TNomTreeClassForm(this,Reg->GetTagByUID("2000"),"�����������",""));
#endif
   delete tmpChTree; tmpChTree = NULL;
}
//---------------------------------------------------------------------------
bool __fastcall TAutoFunc_DM::GetTreeCh(void *uiTag, UnicodeString &UID)
{
   if (((TTagNode*)uiTag)->CmpName("choiceTree"))
    tmpChTree->Add(uiTag);
   return false;
}
//---------------------------------------------------------------------------
*/
Extended __fastcall ICSStrToFloat(UnicodeString AVal)
{
  UnicodeString FVal = AVal.Trim();
  TReplaceFlags rFlag;
  rFlag << rfReplaceAll;
  FVal = StringReplace(FVal, ",", DecimalSeparator, rFlag);
  FVal = StringReplace(FVal, ".", DecimalSeparator, rFlag);
  FVal = StringReplace(FVal, " ", "", rFlag);
  FVal = StringReplace(FVal, ThousandSeparator, "", rFlag);

  return StrToFloat(FVal);
}
//---------------------------------------------------------------------------
inline int __fastcall SpecPeriodInt(UnicodeString APeriod)
{
  if (APeriod.ToIntDef(-1) != -1)
   {
     if (APeriod.ToIntDef(-1))    return 1;
     else                         return 0;
   }
  else                            return 0;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocPeriodValStr(UnicodeString APerType, UnicodeString AValFrom, UnicodeString AValTo)
{
   UnicodeString RC = "";
   UnicodeString FFrDay,FFrMonth,FFrYear,FToDay,FToMonth,FToYear,FFrMonthFull,FToMonthFull;
   FFrDay = "";FFrMonth = "";FFrYear = "";FToDay = "";FToMonth = "";FToYear = "";FFrMonthFull = "";FToMonthFull = "";
   TDateTime tmpDate;
   if (TryStrToDate(AValFrom,tmpDate))
    {
      FFrDay       = StrToDate(AValFrom).FormatString("dd");
      FFrMonth     = StrToDate(AValFrom).FormatString("mm");
      FFrMonthFull = StrToDate(AValFrom).FormatString("mmmm");
      FFrYear      = StrToDate(AValFrom).FormatString("yyyy");
    }
   if (TryStrToDate(AValTo,tmpDate))
    {
      FToDay       = StrToDate(AValTo).FormatString("dd");
      FToMonth     = StrToDate(AValTo).FormatString("mm");
      FToMonthFull = StrToDate(AValTo).FormatString("mmmm");
      FToYear      = StrToDate(AValTo).FormatString("yyyy");
    }

   int FPerType;
   if (APerType.ToIntDef(-1) == -1)
    FPerType = SpecPeriodInt(APerType);
   else
    FPerType = APerType.ToInt();
   if (FPerType == 0)
    {//"�����������"
      return "";
    }
   else if (FPerType == 1)
    {//"����������"
      if (AValFrom.Length() && AValTo.Length())
       {
         RC =  Format("%s.%s.%s �. - %s.%s.%s �.",OPENARRAY( TVarRec, (FFrDay,FFrMonth,FFrYear,FToDay,FToMonth,FToYear) ) );
       }
      else if (AValFrom.Length())
       RC =  Format("� %s.%s.%s �.",OPENARRAY( TVarRec, (FFrDay,FFrMonth,FFrYear) ) );
      else if (AValTo.Length())
       RC =  Format("�� %s.%s.%s �.",OPENARRAY( TVarRec, (FToDay,FToMonth,FToYear) ) );
    }
   else if (FPerType == 2)
    {//"�����"
      RC =  Format("%s %s �.",OPENARRAY( TVarRec, (FFrMonthFull,FFrYear) ) );
    }
   else if (FPerType == 3)
    {//"�������"
      int quarter = StrToDate(AValFrom).FormatString("mm").ToInt();
      if ((quarter >=1)&&(quarter<=3))
       RC =  Format("I ������� %s �.",OPENARRAY( TVarRec, (FFrYear) ) );
      else if ((quarter >=4)&&(quarter<=6))
       RC =  Format("II ������� %s �.",OPENARRAY( TVarRec, (FFrYear) ) );
      else if ((quarter >=7)&&(quarter<=9))
       RC =  Format("III ������� %s �.",OPENARRAY( TVarRec, (FFrYear) ) );
      else if ((quarter >=10)&&(quarter<=12))
       RC =  Format("IV ������� %s �.",OPENARRAY( TVarRec, (FFrYear) ) );
    }
   else if (FPerType == 4)
    {//"���������"
      int quarter = StrToDate(AValFrom).FormatString("mm").ToInt();
      if ((quarter >=1)&&(quarter<=6))
       RC =  Format("I ���������  %s �.",OPENARRAY( TVarRec, (FFrYear) ) );
      else if ((quarter >=7)&&(quarter<=12))
       RC =  Format("II ��������� %s �.",OPENARRAY( TVarRec, (FFrYear) ) );
    }
   else if (FPerType == 5)
    {//"���"
      RC = Format("%s �.",OPENARRAY( TVarRec, (FFrYear) ) );
    }
   else
    RC = "";
   return RC;
}
//---------------------------------------------------------------------------

