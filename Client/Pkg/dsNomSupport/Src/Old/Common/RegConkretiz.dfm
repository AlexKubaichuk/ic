object RegConForm: TRegConForm
  Left = 307
  Top = 355
  HelpContext = 4012
  BorderIcons = []
  BorderStyle = bsToolWindow
  BorderWidth = 3
  Caption = #1054#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1103' ...'
  ClientHeight = 254
  ClientWidth = 386
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object CompPanel: TPanel
    Left = 0
    Top = 0
    Width = 386
    Height = 220
    BevelOuter = bvNone
    TabOrder = 0
  end
  object BtnPanel: TPanel
    Left = 0
    Top = 214
    Width = 386
    Height = 40
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    DesignSize = (
      386
      40)
    object OkBtn: TcxButton
      Left = 219
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      OnClick = OkBtnClick
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
    end
    object CancelBtn: TcxButton
      Left = 302
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
    end
  end
end
