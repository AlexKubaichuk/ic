//---------------------------------------------------------------------------

#ifndef BaseNomDefH
#define BaseNomDefH
//---------------------------------------------------------------------------
#include <vcl.h>
#include "Dmf_Auto.h"
#include "ICSDATEUTIL.hpp"
#include "DateConkretiz.h"
#include "TextConcretiz.h"

//#include "pgmsetting.h"
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#                   ��������� ��������� �������� ���������                   #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
#define _CorrectDate(a) TDateTime(a).FormatString("dd.mm.yyyy")
//---------------------------------------------------------------------------
typedef void __fastcall (__closure *TGetAttrFunc)(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
typedef bool __fastcall (__closure *TGetDefFunc)(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
typedef void __fastcall (__closure *TGetSQLFunc)(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
class TICSBaseNomDef : public TObject
{
private:
    UnicodeString __fastcall SetDay(int AYear, int AMonth, int ADay );
    UnicodeString __fastcall _GetPart2(UnicodeString ARef, char Sep);
    void __fastcall DecodePeriod(UnicodeString AScr, int &AYear, int &AMonth, int &ADay);
protected:
    TAutoFunc_DM   *FDM;
    UnicodeString __fastcall GetValIfNotEmpty(UnicodeString APref, UnicodeString AVal, UnicodeString APostf = "");
    UnicodeString __fastcall FGetGUI(TTagNode *ANode);
    UnicodeString __fastcall AgeStr(TDateTime ACurDate, TDateTime ABirthDay);
    UnicodeString __fastcall AgeStrY(TDateTime ACurDate, TDateTime ABirthDay);
    UnicodeString __fastcall FIncPeriod(UnicodeString AFrDate,UnicodeString APer, int IsSub);
    UnicodeString __fastcall AddPeriod(UnicodeString AScr1, UnicodeString AScr2);
    UnicodeString __fastcall FGetRefValue(TTagNode *ANode, UnicodeString ARef1,UnicodeString ARef2);
    void __fastcall GetSQLValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    UnicodeString __fastcall GetChildContAVDef(TTagNode *ANode, UnicodeString AVal, UnicodeString ADef);
public:
    void __fastcall GetAtttInfo(UnicodeString AFuncName, TStringList *AFields, UnicodeString ARef, UnicodeString &ARet, UnicodeString AAttrRef);
    bool __fastcall GetDefInfo(UnicodeString AFuncName, TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall GetSQLInfo(UnicodeString AFuncName, TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    virtual TGetAttrFunc __fastcall GetAttrMethodAddr(UnicodeString AFuncName);
    virtual TGetDefFunc __fastcall GetDefMethodAddr(UnicodeString AFuncName);
    virtual TGetSQLFunc __fastcall GetSQLMethodAddr(UnicodeString AFuncName);

__published:
    void __fastcall ADocAttrVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AExtEditValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetOrgCode(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetLPUName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetLPUFullName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetAddr(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetOKPO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetSOATO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetOKVED(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetOKATO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetPhone(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASetEmail(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AFamIO(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AAge(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AAgeY(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AChoiceValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AChoiceDBValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);

    void __fastcall AFullAddrVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AFullBaseOrgVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AFullExtOrgVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AFullOrgVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AFullOrgAddrVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AFullInshPolVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AFullULVal(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);


    bool __fastcall DefChoiceValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall SQLChoiceValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefAgePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall SQLAgePeriodLast(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefDatePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall SQLDatePeriodLast(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefChoiceDBValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall SQLChoiceDBValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefRegVal(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall SQLRegVal(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefTextValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall SQLTextValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    bool __fastcall DefExtEditValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    void __fastcall SQLExtEditValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);

    __fastcall TICSBaseNomDef(TAutoFunc_DM *ADM);
    __fastcall ~TICSBaseNomDef(){};
};
#endif






