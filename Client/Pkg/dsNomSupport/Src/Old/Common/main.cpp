//---------------------------------------------------------------------------
#include <DateUtils.hpp>
#include <StrUtils.hpp>
#include <Registry.hpp>
#pragma hdrstop
#include "DATEUTIL.hpp"
#include "DxLocale.h"
#include "main.h"
#include "DateConkretiz.h"
#include "InqCon.h"
#include "NomTreeClass.h"
#include "TextConcretiz.h"
#include "datadef.h"
#ifdef _IMM
  #include "RegPPConkretiz.h"
  #include "SchConkretiz.h"
  #include "ProbReak.h"
#endif
#ifdef _AKDO
 #ifdef _IMM
   #define _AKDO_IMM
 #endif
#endif

//---------------------------------------------------------------------------
typedef map<const UnicodeString, UnicodeString> TStrMap;
TStrMap HashSel;
TStrMap ClassHashSel;
//---------------------------------------------------------------------------
#define flVal(a) AFields->Values[AFields->Names[a]]
//---------------------------------------------------------------------------

#ifdef _IMM
//############################################################################
//#                                                                          #
//#                           Imm section                                    #
//#                                                                          #
//############################################################################
//---------------------------------------------------------------------------
int __fastcall CorrectDay( Word Year, Word Month, Word Day )
{
  Word DaysCount = DaysInAMonth(Year,Month);
  if ( Day > DaysCount )
    Day = DaysCount;
  return Day;
}
//---------------------------------------------------------------------------
bool __fastcall CalcPlanPeriod(TDate *ADateBP, TDate *ADateEP, TDate *APlanMonth)
{  //����������� ������� ������������ � ������ ������������
   // return: (������� ����� < ���� ������ ������������)

   TDate CurDate;
   Word DayNP,CurYear, CurMonth, CurDay, DayBP, MonthBP, YearBP, DayEP, MonthEP, YearEP;

   if ((int)(*APlanMonth)) CurDate = (*APlanMonth);
   else                    CurDate = Date();

   DecodeDate( CurDate, CurYear, CurMonth, CurDay );

   TRegIniFile *IMMIni = new TRegIniFile ("IMM");
   IMMIni->RootKey = HKEY_LOCAL_MACHINE;
   IMMIni->OpenKey(K_REG,false);
   DayNP  = IMMIni->ReadInteger("LPUParam","DAY_PLAN",25);
   IMMIni->CloseKey();
   delete IMMIni;

   if ( CurDay < DayNP)
    {
      MonthEP = CurMonth;
      YearEP  = CurYear;

      MonthBP = Dateutils::MonthOf( Sysutils::IncMonth(CurDate,-1));
      YearBP  = ( MonthEP == 1 ) ? YearEP - 1 : YearEP;
    }
   else
    {
      MonthBP   = CurMonth;
      YearBP    = CurYear;

      MonthEP = Dateutils::MonthOf( Sysutils::IncMonth(CurDate));
      YearEP  = ( MonthBP == 12 ) ? CurYear + 1 : CurYear;
    }
   DayBP = CorrectDay(YearBP, MonthBP, DayNP);
   DayEP = CorrectDay(YearEP, MonthEP, 99);

   (*ADateBP) = EncodeDate(YearBP, MonthBP, DayBP);
   (*ADateEP) = EncodeDate(YearEP, MonthEP, DayEP);
   if ((*ADateEP) > CurDate) (*ADateEP) = CurDate;
   (*APlanMonth) = EncodeDate(YearEP, MonthEP, 1);
   return (Date().FormatString("dd").ToIntDef(1) < DayNP);
}
//---------------------------------------------------------------------------
#endif
#ifdef _AKDO
//############################################################################
//#                                                                          #
//#                          AKDO section                                    #
//#                                                                          #
//############################################################################

//---------------------------------------------------------------------------
#endif
//---------------------------------------------------------------------------
UnicodeString __fastcall SetDay(int AYear, int AMonth, int ADay )
{
  UnicodeString RC = "";
  try
   {
     UnicodeString sm = IntToStr(AYear);
     sm = sm.SubString(sm.Length(),1);
     if (((sm == "1")||(sm == "2")||(sm == "3")||(sm == "4"))&&!((AYear<15)&&(AYear>10))) sm = "�. ";
     else                                                    sm = "�. ";
     if (AYear)   RC += IntToStr(AYear)+sm;
     if (AMonth)  RC += IntToStr(AMonth)+"�. ";
     if (ADay)    RC += IntToStr(ADay)+"�.";
     if (!(AYear+AMonth+ADay)) RC += "0�.";
   }
  __finally
   {
   }
  return RC.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall AgeStr(TDateTime ACurDate, TDateTime ABirthDay)
{
  UnicodeString RC = "";
  try
   {
     if (ACurDate < ABirthDay)
      {
        RC = "-";
      }
     else
      {
        Word FYear,FMonth,FDay;
        DateDiff(ACurDate, ABirthDay, FDay, FMonth, FYear);
        RC = SetDay(FYear,FMonth,FDay);
      }
   }
  __finally
   {
   }
  return RC.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall FGetGUI(TTagNode *ANode)
{
  return ANode->GetRoot()->GetChildByName("passport")->AV["gui"];
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetChildContAVDef(TTagNode *ANode, UnicodeString AVal, UnicodeString ADef)
{
  TTagNode *_tmp = ANode->GetFirstChild();
  UnicodeString _tmpAV;
  while (_tmp)
   {
     _tmpAV = _tmp->AV["name"];
     if (strstr(_tmpAV.c_str(),AVal.c_str()))  break;
     _tmp = _tmp->GetNext();
   }
  if (_tmp) return _tmp->GetAVDef("value",ADef);
  else      return ADef;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall _GetPart2(UnicodeString ARef, char Sep)
{
  int pos = ARef.Pos(Sep);
  return ARef.SubString(pos+1,ARef.Length()-pos);
}
//---------------------------------------------------------------------------
void __fastcall DecodePeriod(UnicodeString AScr, int &AYear, int &AMonth, int &ADay)
{
  AYear  = GetPart1(AScr,'/').ToInt();
  AMonth = GetPart1(_GetPart2(AScr,'/'),'/').ToInt();
  ADay   = GetPart2(_GetPart2(AScr,'/'),'/').ToInt();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall AddPeriod(UnicodeString AScr1, UnicodeString AScr2)
{
  int YY1,YY2,MM1,MM2,DD1,DD2;
  DecodePeriod(AScr1,YY1,MM1,DD1);
  DecodePeriod(AScr2,YY2,MM2,DD2);
  DD1 += DD2;             DD2 = 0;
  if (DD1>31) {DD1 -= 31; DD2 = 1;}
  MM1 += MM2 + DD2; MM2 = 0;
  if (MM1>12) {MM1 -= 12; MM2 = 1;}
  YY1 += YY2 + MM2;
  return IntToStr(YY1)+"/"+IntToStr(MM1)+"/"+IntToStr(DD1);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall FIncPeriod(UnicodeString AFrDate,UnicodeString APer, int IsSub)
{
  TDateTime RC = TDate(0);
  try
   {
     int FYears, FMonths, FDays;
     DecodePeriod(APer, FYears, FMonths, FDays);
     if (IsSub)
      {
       FYears  *= -1;
       FMonths *= -1;
       FDays   *= -1;
      }
     RC = Dateutils::IncYear
     (
       Sysutils::IncMonth
       (
         Dateutils::IncDay(StrToDate(AFrDate),FDays),
         FMonths
       ),
       FYears
     );
   }
  __finally
   {
   }
  return RC._FS;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall FGetRefValue(TTagNode *ANode, UnicodeString ARef1,UnicodeString ARef2)
{
  if (ANode->GetParent("domain")->GetNext()) return ARef1;
  else                                       return ARef2;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall InitEx(ShortString ADBName, ShortString AUName, ShortString APwd, ShortString ACHSet, TStringList *AXMLList, HWND FHandle)
{
  Application->Handle = FHandle;
  AutoFunc_DM = new TAutoFunc_DM;
//  Application->CreateForm(__classid(TNomTreeClassForm), &NomTreeClassForm);
#ifdef _ADO_
  AutoFunc_DM->AUTOFUNC_DB->ConnectionString = UnicodeString(ADBName);
#else
  AutoFunc_DM->AUTOFUNC_DB->DBParams->Clear();
  AutoFunc_DM->AUTOFUNC_DB->DBParams->Add(("user_name="+UnicodeString(AUName).LowerCase()).c_str());
  AutoFunc_DM->AUTOFUNC_DB->DBParams->Add(("password="+UnicodeString(APwd).LowerCase()).c_str());
  AutoFunc_DM->AUTOFUNC_DB->DBParams->Add("sql_role_name=");
  if (UnicodeString(ACHSet).Length())
   AutoFunc_DM->AUTOFUNC_DB->DBParams->Add(("lc_ctype="+UnicodeString(ACHSet).LowerCase()).c_str());

  AutoFunc_DM->AUTOFUNC_DB->DBName = UnicodeString(ADBName);
#endif
  try
   {

     AutoFunc_DM->AUTOFUNC_DB->Open();
     AutoFunc_DM->XMLList = (TAxeXMLContainer*)AXMLList;
     AutoFunc_DM->CreateTreeFormList();
#ifdef _AKDO_IMM
     AutoFunc_DM->MKB.clear();
     AutoFunc_DM->qtExec("Select CODE,EXTCODE,NAME From MKB ",false);
     while (!AutoFunc_DM->quFreeA->Eof)
      {
        AutoFunc_DM->MKB[AutoFunc_DM->quFreeA->_FN("CODE")->AsString] =
                         AutoFunc_DM->quFreeA->_FN("EXTCODE")->AsString +" "+
                         AutoFunc_DM->quFreeA->_FN("NAME")->AsString;
        AutoFunc_DM->quFreeA->Next();
      }
     AutoFunc_DM->MKBDlg = new TNomTreeClassForm(Application->Owner,AutoFunc_DM->MKBClRoot,"���-10","");
     AutoFunc_DM->MKBDlg->Caption = "���-10";
     //````````````````````````````````````````````````````````````
#endif
   #ifdef _AKDO
     AutoFunc_DM->LPUList.clear();
     try
      {

        AutoFunc_DM->qtExec("Select Count (rdb$relation_name) as TCOUNT From rdb$relations Where Upper(rdb$relation_name) = 'LPULIST'",false);
        if (AutoFunc_DM->quFreeA->_FN("TCOUNT")->AsInteger)
         {
           AutoFunc_DM->qtExec("Select * From LPULIST",false);
           while (!AutoFunc_DM->quFreeA->Eof)
            {
              AutoFunc_DM->LPUList[AutoFunc_DM->quFreeA->_FN("CODE")->AsString] =
                                   AutoFunc_DM->quFreeA->_FN("NAME")->AsString;
              AutoFunc_DM->quFreeA->Next();
            }
         }
      }
     catch(...)
      {
      }
   #endif
   #ifdef _IMM
     AutoFunc_DM->qtExec("Select CODE, R0040 From CLASS_0035");
     while (!AutoFunc_DM->quFreeA->Eof)
      {// ������ ������ ������
        AutoFunc_DM->VacList[quFNStr("CODE")] = quFNStr("R0040");
        AutoFunc_DM->VacListCode[quFNStr("R0040")] = quFNStr("CODE");
        AutoFunc_DM->quFreeA->Next();
      }
     AutoFunc_DM->qtExec("Select CODE, R002C From CLASS_002A");
     while (!AutoFunc_DM->quFreeA->Eof)
      {// ������ ������ ����
        AutoFunc_DM->ProbList[quFNStr("CODE")] = quFNStr("R002C");
        AutoFunc_DM->ProbListCode[quFNStr("R002C")] = quFNStr("CODE");
        AutoFunc_DM->quFreeA->Next();
      }
     AutoFunc_DM->qtExec("Select CODE, R003C From CLASS_003A");
     while (!AutoFunc_DM->quFreeA->Eof)
      {// ������ ������ ��������
        AutoFunc_DM->InfList[quFNStr("CODE")] = quFNStr("R003C");
        AutoFunc_DM->InfListCode[quFNStr("R003C")] = quFNStr("CODE");
        AutoFunc_DM->quFreeA->Next();
      }
     AutoFunc_DM->trFreeA->Commit();
   #endif
     DxLocale();
   }
  catch (Exception &E)
   {
     ShowMessage(E.Message);
   }
  //````````````````````````````````````````````````````````````
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall Init(ShortString ADBName, TStringList *AXMLList, HWND FHandle)
{
  InitEx(ADBName, "SYSDBA", "MASTERKEY", "", AXMLList, FHandle);
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall DeInit()
{
  delete AutoFunc_DM;
}
//---------------------------------------------------------------------------
void __stdcall GetSQLValue(TTagNode *AIE, char* ATab, char* ARet)
{
   TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   UnicodeString ISTab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   strcpy(ATab,ISTab.c_str());
   if (!AIE->Count)
    {
      if (FIS->AV["tabalias"].Length())
       strcpy(ARet,(FIS->AV["tabalias"]+"."+FIS->AV["keyfield"]+"="+FIS->AV["tabalias"]+"."+FIS->AV["keyfield"]).c_str());
      else
       strcpy(ARet,(FIS->AV["maintable"]+"."+FIS->AV["keyfield"]+"="+FIS->AV["maintable"]+"."+FIS->AV["keyfield"]).c_str());
    }
   else
    {
      bool  IsExtSel = false;
      if (FISCondRules->GetChildByName("root"))
       IsExtSel = (bool)(FISCondRules->GetChildByName("root")->AV["extsel"].ToIntDef(0));
      TTagNode *_tmp = AIE->GetChildByAV("text","name","sql");
      if (_tmp)
       {
         if (IsExtSel)
          {
            if (_tmp->Count)
             {
               TReplaceFlags rFlag;
               rFlag << rfReplaceAll;
               TTagNode *subtmp;
               UnicodeString FSQL = _tmp->AV["value"];
               UnicodeString FSubSQL;
               int idx = 0;
               subtmp = _tmp->GetFirstChild();
               while (subtmp)
                {
                  UnicodeString SelRC = "";
                  FSubSQL = subtmp->AV["value"];
                  TStrMap::iterator rc = HashSel.find(FSubSQL);
                  if (rc == HashSel.end())
                   {
                     SelRC = "-1";
                     AutoFunc_DM->qtExec(FSubSQL,false);
                     while (!AutoFunc_DM->quFreeA->Eof)
                      {
                        SelRC += ","+AutoFunc_DM->quFreeA->_FN("CODE")->AsString;
                        AutoFunc_DM->quFreeA->Next();
                      }
                     HashSel[FSubSQL] = SelRC;
                   }
                  FSQL = StringReplace(FSQL, "_SEL"+IntToStr(idx), HashSel[FSubSQL], rFlag);
                  idx++;
                  subtmp = subtmp->GetNext();
                }
               strcpy(ARet,FSQL.c_str());
             }
            else
             strcpy(ARet,_tmp->AV["value"].c_str());
          }
         else
          strcpy(ARet,_tmp->AV["value"].c_str());
       }
      else      strcpy(ARet,"");
    }
}
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#                   ��������� ��������� �������� ���������                   #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ADocAttrVal(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  strcpy(ARet,AFields->Values[ARef].c_str());
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AExtEditValue(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  strcpy(ARet,"����� ��������");
}
//---------------------------------------------------------------------------
void __stdcall FGetOrgParam(char* ARet, UnicodeString AFlName)
{
  AutoFunc_DM->qtExec("Select "+AFlName+" From SUBORDORG Where SUBORD=2",false);
  if (AutoFunc_DM->quFreeA->RecordCount)
   strcpy(ARet,AutoFunc_DM->quFreeA->_FN(AFlName)->AsString.c_str());
  else
   strcpy(ARet,"");
#ifndef _ADO_
  if (AutoFunc_DM->trFreeA->Active) AutoFunc_DM->trFreeA->Commit();
#endif
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ASetOrgCode(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  FGetOrgParam(ARet, "CODE");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ASetLPUName(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  FGetOrgParam(ARet, "NAME");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ASetLPUFullName(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  FGetOrgParam(ARet, "FULLNAME");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ASetAddr(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  FGetOrgParam(ARet, "ADDR");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ASetOKPO(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  FGetOrgParam(ARet, "OKPO");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ASetSOATO(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  FGetOrgParam(ARet, "SOATO");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ASetOKVED(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  FGetOrgParam(ARet, "OKVED");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ASetOKATO(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  FGetOrgParam(ARet, "OKATO");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ASetPhone(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  FGetOrgParam(ARet, "PHONE");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ASetEmail(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  FGetOrgParam(ARet, "EMAIL");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AFamIO(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  UnicodeString DDD = flVal(0);
  if (flVal(1).Length())
   DDD += " "+UnicodeString(flVal(1)[1])+".";
  if (flVal(2).Length())
   DDD += UnicodeString(flVal(2)[1])+".";
  strcpy(ARet,DDD.c_str());
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AAge(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  TDateTime FToDate = TDate(_CDATEF_);
  if (GetPart2(UnicodeString(ARet),';').Length())
   {
     if (TDate(GetPart2(UnicodeString(ARet),';')) <= FToDate)
      FToDate = TDate(GetPart2(UnicodeString(ARet),';'));
   }
  try
   {
     strcpy(ARet,AgeStr(FToDate,StrToDate(AFields->Values[AFields->Names[0]])).c_str());
   }
  catch (EConvertError &E)
   {
     strcpy(ARet,"");
   }
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AMKBDiag(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  strcpy(ARet,AutoFunc_DM->MKB[AFields->Values[AFields->Names[0]]].c_str());
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AInqAge(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  try
   {
     strcpy(ARet,AgeStr(StrToDate(AFields->Values[AFields->Names[1]]),StrToDate(AFields->Values[AFields->Names[0]])).c_str());
   }
  catch (EConvertError &E)
   {
     strcpy(ARet,"");
   }
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AInqChVal(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  try
   {
     TTagNode *FExtAttr = AutoFunc_DM->XMLList->GetRefer(ARef);
     if (FExtAttr)
      {
        UnicodeString flName = "P"+GetXMLProjectCode(FExtAttr)+FExtAttr->AV["uid"].c_str();
        strcpy(ARet,GetScaleText(FExtAttr,AFields->Values[flName].ToIntDef(-1)).Trim().c_str());
      }
     else
      strcpy(ARet," �������� �� ���������� ");
   }
  catch(...)
   {
     strcpy(ARet," ");
   }
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AInqChValValue(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  try
   {
     TTagNode *FExtAttr = AutoFunc_DM->XMLList->GetRefer(ARef);
     if (FExtAttr)
      {
        UnicodeString flName = "P"+GetXMLProjectCode(FExtAttr)+FExtAttr->AV["uid"].c_str();
        strcpy(ARet,AFields->Values[flName].c_str());
      }
     else
      strcpy(ARet," �������� �� ���������� ");
   }
  catch(...)
   {
     strcpy(ARet," ");
   }
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AZaklPres(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  if (AFields->Values[AFields->Names[0]] == "1")  strcpy(ARet,"��");
  else                                            strcpy(ARet,"���");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AChoiceValue(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  TTagNode *FChoice = AutoFunc_DM->XMLList->GetRefer(ARef);
  if (FChoice->CmpName("choice"))
   {
     FChoice = FChoice->GetChildByAV("choicevalue","value",AFields->Values[AFields->Names[0]]);
     if (FChoice)
      strcpy(ARet,FChoice->AV["name"].c_str());
     else
      strcpy(ARet,"");
   }
  else if (FChoice->CmpName("binary"))
   {
     if (AFields->Values[AFields->Names[0]] == "1")  strcpy(ARet,"��");
     else                                            strcpy(ARet,"���");
   }
  else if (FChoice->CmpName("class,choiceBD"))
   {
     if (AFields->Values[AFields->Names[0]].ToIntDef(-1) != -1)
      {
        if (FChoice->CmpName("choiceBD"))
         FChoice = FChoice->GetTagByUID(FChoice->AV["ref"]);
        if (FChoice)
         {

           TTagNode *FieldList = FChoice->GetChildByName("description")->GetFirstChild();
           TTagNode *itField;
           TTagNode *ISAttr = AutoFunc_DM->XMLList->GetRefer(AAttrRef);
           UnicodeString OldRelation = "";
           UnicodeString FCol = _UID(ISAttr->AV["column"]);
           UnicodeString FSQL = "";
           itField = FieldList->GetFirstChild();
           while (itField)
            {
              if (!FSQL.Length())
               FSQL += "R"+itField->AV["ref"];
              else
               FSQL += ", R"+itField->AV["ref"];
              itField = itField->GetNext();
            }
           FSQL = "Select "+FSQL+" From "+_GUI(ISAttr->AV["column"]);
           if (ISAttr->AV["ref_parent"].Length())
            {
              while (ISAttr->AV["ref_parent"].Length())
               {
                 OldRelation = ISAttr->AV["relation"];
                 ISAttr = AutoFunc_DM->XMLList->GetRefer(_GUI(AAttrRef)+"."+ISAttr->AV["ref_parent"]);
                 if (!ISAttr->AV["ref_parent"].Length())
                  FSQL += " join "+_GUI(ISAttr->AV["column"])+" on ("+OldRelation+" and "+_GUI(ISAttr->AV["column"])+".CODE="+AFields->Values[AFields->Names[0]]+")";
                 else
                  FSQL += " join "+_GUI(ISAttr->AV["column"])+" on ("+OldRelation+")";
               }
            }
           else
            {
              FSQL += " Where "+_GUI(ISAttr->AV["column"])+".CODE="+AFields->Values[AFields->Names[0]];
            }
           UnicodeString SelRC = "";
           try
            {
              TStrMap::iterator rc = ClassHashSel.find(FSQL);
              if (rc == ClassHashSel.end())
               {
                 SelRC = "";
                 AutoFunc_DM->qtExec(FSQL,false);
                 itField = FieldList->GetFirstChild();
                 while (itField)
                  {
                    if (!AutoFunc_DM->quFreeA->_FN("R"+itField->AV["ref"])->IsNull)
                     {
                       if (!SelRC.Length())
                        SelRC = AutoFunc_DM->quFreeA->_FN("R"+itField->AV["ref"])->AsString;
                       else
                        SelRC += "/"+AutoFunc_DM->quFreeA->_FN("R"+itField->AV["ref"])->AsString;
                     }
                    itField = itField->GetNext();
                  }
                 ClassHashSel[FSQL] = SelRC;
               }
              else
               SelRC = ClassHashSel[FSQL];
              strcpy(ARet,SelRC.c_str());
            }
           catch (...)
            {
              strcpy(ARet,"������");
            }
         }
      }
     else
      strcpy(ARet,"");
   }
  else
   {
     strcpy(ARet,"");
   }
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AChoiceDBValue(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  TTagNode *FExtAttr = AutoFunc_DM->XMLList->GetRefer(ARef);
  if (FExtAttr)
   {
     Variant DDD[1];
     DDD[0] = Variant(AFields->Values[("R"+FExtAttr->AV["uid"]).c_str()]);
     AutoFunc_DM->quFreeA->Close();
     AutoFunc_DM->quFreeA->SQL->Clear();
     if (!AutoFunc_DM->trFreeA->Active) AutoFunc_DM->trFreeA->StartTransaction();
     AutoFunc_DM->quFreeA->ExecProcedure(("CLASSDATA_"+FExtAttr->AV["ref"]).c_str(),DDD,0);
     strcpy(ARet,quFNStr("STR_CLASS").Trim().c_str());
     AutoFunc_DM->trFreeA->Commit();
   }
  else
   {
     strcpy(ARet," �������� �� ���������� ");
   }
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AAMInqPer(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  try
   {
     strcpy(ARet,AgeStr(StrToDate(AFields->Values[AFields->Names[0]]),StrToDate(AFields->Values[AFields->Names[1]])).c_str());
   }
  catch (EConvertError &E)
   {
     strcpy(ARet,"");
   }
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AAMInqDiag(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  strcpy(ARet,AutoFunc_DM->MKB[AFields->Values[AFields->Names[0]]].c_str());
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AAMInqIllType(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  int IllType  = AFields->Values[AFields->Names[0]].ToIntDef(0);
  switch (IllType)
   {
     case 1:  { strcpy(ARet,"������"); break;}
     case 2:  { strcpy(ARet,"������� � ����� ��������.����.�����������"); break;}
     case 3:  { strcpy(ARet,"������� ��������. � ��������. ���� ��� �����"); break;}
     case 4:  { strcpy(ARet,"���������� ����.�����������"); break;}
     default: { strcpy(ARet,"�� ������"); }
   }
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AAMInqType(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  int FType  = AFields->Values[AFields->Names[0]].ToIntDef(0);
  if (FType == 1) strcpy(ARet,"���������");
  else            strcpy(ARet,"���������");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AAMInqWrk(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  int InqWrk  = AFields->Values[AFields->Names[0]].ToIntDef(0);
  switch (InqWrk)
   {
     case 0:  { strcpy(ARet,"����."); break;}
     case 1:  { strcpy(ARet,"���."); break;}
     default: { strcpy(ARet,"��.�����."); }
   }
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AAMInqLPU(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  strcpy(ARet,AutoFunc_DM->LPUList[AFields->Values[AFields->Names[0]]].c_str());
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ADOutval(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  try
   {
     TTagNode *FExtAttr = AutoFunc_DM->XMLList->GetRefer(ARef);
     if (FExtAttr)
      {
        UnicodeString flName = "PD"+GetXMLProjectCode(FExtAttr)+FExtAttr->AV["uid"].c_str();
        if (AFields->Values[flName].ToIntDef(-1) == 1)
         strcpy(ARet,"��");
        else
         strcpy(ARet,"���");
      }
     else
      strcpy(ARet," �������� �� ���������� ");
   }
  catch(...)
   {
     strcpy(ARet," ");
   }
}
//---------------------------------------------------------------------------
#ifdef _IMM
//############################################################################
//#                                                                          #
//#                           Imm section                                    #
//#                                                                          #
//############################################################################
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall APlanVac(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  UnicodeString DDD = "";
  if (flVal(1) == "0")
   {// ����
     TReplaceFlags rFlag;
     rFlag << rfReplaceAll;
     TStringList *tmp = new TStringList;
     tmp->Text =  StringReplace(flVal(4), "#", "\n", rFlag);
     DDD += AutoFunc_DM->VacList[flVal(2)]; // ������������ �������
     if (tmp->Count)
      {
       tmp->Delete(0);
       if (tmp->Count == 1)
        {
         DDD += " "+_UID(tmp->Strings[0]);
        }
       else
        {
          // ���������, �� ������ ���������� ����� ����������
          bool isAll = true;
          for (int i = 1; i < tmp->Count; i++)
           if (_UID(tmp->Strings[i]) != _UID(tmp->Strings[0]))
            isAll = false;
          if (isAll)
           {// ���� ���������� ���������
             if (_UID(tmp->Strings[0]).Trim().UpperCase() == "D")
              DDD += " ���.";
             else
              DDD += " "+_UID(tmp->Strings[0]);
           }
          else
           {// ���� ���������� ������
             DDD += " (";
             for (int i = 0; i < tmp->Count; i++)
              if (tmp->Strings[i].Length())
               {
                 if (_UID(tmp->Strings[i]).Trim().UpperCase() == "D")
                  DDD += AutoFunc_DM->InfList[_GUI(tmp->Strings[i])]+" ���.;";
                 else
                  DDD += AutoFunc_DM->InfList[_GUI(tmp->Strings[i])]+" "+_UID(tmp->Strings[i])+";";
               }
             DDD += ")";
           }
        }
      }
     else
         DDD += " (���.)";
     delete tmp;
   }
  else
   {// �����
     DDD += AutoFunc_DM->ProbList[flVal(3)];
   }
  strcpy(ARet,DDD.c_str());
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AAddrValue(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  if (AFields->Values["R0022"] == "1")
   {
     Variant DDD[1];
     DDD[0] = Variant(AFields->Values["R0032"]);
     AutoFunc_DM->quFreeA->Close();
     AutoFunc_DM->quFreeA->SQL->Clear();
     if (!AutoFunc_DM->trFreeA->Active) AutoFunc_DM->trFreeA->StartTransaction();
     AutoFunc_DM->quFreeA->ExecProcedure("CLASSDATA_0017",DDD,0);
     strcpy(ARet,quFNStr("STR_CLASS").Trim().c_str());
     strcat(ARet," ��. ");
     strcat(ARet,AFields->Values["R0033"].Trim().c_str());
     AutoFunc_DM->trFreeA->Commit();
   }
  else
   {
     strcpy(ARet,"(�� ������.) ");
     strcat(ARet,AFields->Values["R0024"].Trim().c_str());
   }
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall AOrgValue(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  if (AFields->Values["R0023"] == "1")
   {
     Variant DDD[1];
     DDD[0] = Variant(AFields->Values["R0025"]);
     AutoFunc_DM->quFreeA->Close();
     AutoFunc_DM->quFreeA->SQL->Clear();
     if (!AutoFunc_DM->trFreeA->Active) AutoFunc_DM->trFreeA->StartTransaction();
     AutoFunc_DM->quFreeA->ExecProcedure("CLASSDATA_0001",DDD,0);
     strcpy(ARet,quFNStr("STR_CLASS").Trim().c_str());
     strcat(ARet," ");
     if (quFNInt("P_0004"))
      {
        strcat(ARet,quFNStr("P_0005").Trim().c_str());
        for (int i = 0; i < quFNInt("P_0004"); i++)
         {
           strcat(ARet," ");
           if (AFields->Values[("R002"+IntToStr(6+i)).c_str()].Trim() != "-1")
            strcat(ARet,AFields->Values[("R002"+IntToStr(6+i)).c_str()].Trim().c_str());
         }
      }
     AutoFunc_DM->trFreeA->Commit();
   }
  else
   {
     strcpy(ARet,"�� �����������");
   }
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall APatPrivAge(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  try
   {
     strcpy(ARet,AgeStr(StrToDate(AFields->Values[AFields->Names[1]]),StrToDate(AFields->Values[AFields->Names[0]])).c_str());
   }
  catch (EConvertError &E)
   {
     strcpy(ARet,"");
   }
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ASchName(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  if (AFields->Count)
   {
     UnicodeString RC = "";
     UnicodeString flName = GetPart1(AFields->Strings[0],'=').UpperCase();
     TTagNode *SchList;
     UnicodeString xName,xTab;
     if ((flName == "R1092")||(flName == "R1078"))
      { //��������
         SchList = AutoFunc_DM->XMLList->GetXML("12063611-00008cd7-cd89");
         SchList = SchList->GetTagByUID(_GUI(GetPart2(AFields->Strings[0],'=').UpperCase()));
         xName = "R0040";
         xTab = "CLASS_0035";
      }
     else if ((flName == "R1093")||(flName == "R1079"))
      { //�����
         SchList = AutoFunc_DM->XMLList->GetXML("001D3500-00005882-DC28");
         SchList = SchList->GetTagByUID(_GUI(GetPart2(AFields->Strings[0],'=').UpperCase()));
         xName = "R002C";
         xTab = "CLASS_002A";
      }
     if (SchList)
      {
        AutoFunc_DM->qtExec("Select "+xName+" From "+xTab+" Where code="+SchList->AV["ref"]);
        if (AutoFunc_DM->quFreeA->RecordCount)
         RC = AutoFunc_DM->quFreeA->FN(xName.c_str())->AsString+" (������� "+SchList->AV["name"]+")";
      }
     strcpy(ARet,RC.c_str());
   }
  else
   strcpy(ARet,"");
}
//---------------------------------------------------------------------------
#endif
#ifdef _AKDO
//############################################################################
//#                                                                          #
//#                          AKDO section                                    #
//#                                                                          #
//############################################################################
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ADDocSpec(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  int InqWrk  = AFields->Values[AFields->Names[0]].ToIntDef(0);
  switch (InqWrk)
   {
     case 1:  { strcpy(ARet,"����������"); break;}
     case 2:  { strcpy(ARet,"���������������"); break;}
     case 3:  { strcpy(ARet,"���������"); break;}
     case 4:  { strcpy(ARet,"����������"); break;}
     case 5:  { strcpy(ARet,"���������"); break;}
     case 6:  { strcpy(ARet,"����������������"); break;}
     case 7:  { strcpy(ARet,"�������"); break;}
     case 8:  { strcpy(ARet,"���"); break;}
     case 9:  { strcpy(ARet,"��������"); break;}
     case 10:  { strcpy(ARet,"������������"); break;}
     case 11:  { strcpy(ARet,"��������"); break;}
     case 12:  { strcpy(ARet,"�������"); break;}
     case 13:  { strcpy(ARet,"�������"); break;}
     case 14:  { strcpy(ARet,"�����������"); break;}
     case 15:  { strcpy(ARet,"�������"); break;}
     case 16:  { strcpy(ARet,"�����������"); break;}
     case 17:  { strcpy(ARet,"����������"); break;}
     case 18:  { strcpy(ARet,"��������"); break;}
     case 19:  { strcpy(ARet,"��������"); break;}
     case 20:  { strcpy(ARet,"������"); break;}
     case 21:  { strcpy(ARet,"������������"); break;}
     default: { strcpy(ARet,""); }
   }
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ADInqDiag(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  strcpy(ARet,AutoFunc_DM->MKB[AFields->Values[AFields->Names[0]]].c_str());
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ADST(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  int InqWrk  = AFields->Values[AFields->Names[0]].ToIntDef(0);
  switch (InqWrk)
   {
     case 0:  { strcpy(ARet,"�� �������"); break;}
     case 1:  { strcpy(ARet,"I"); break;}
     case 2:  { strcpy(ARet,"II"); break;}
     case 3:  { strcpy(ARet,"III"); break;}
     case 4:  { strcpy(ARet,"IV"); break;}
     case 5:  { strcpy(ARet,"V"); break;}
     default: { strcpy(ARet,""); }
   }
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ADHZ(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  if (AFields->Values[AFields->Names[0]].ToIntDef(0) == 0) strcpy(ARet,"���");
  else                                                      strcpy(ARet,"��");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ADVFirst(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  if (AFields->Values[AFields->Names[0]].ToIntDef(0) == 0) strcpy(ARet,"���");
  else                                                      strcpy(ARet,"��");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ADDiagT(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  if (AFields->Values[AFields->Names[0]].ToIntDef(0) == 0) strcpy(ARet,"���");
  else                                                      strcpy(ARet,"��");
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall ADDispU(TStringList *AFields, ShortString ARef, char* ARet,ShortString AAttrRef)
{
  int InqWrk  = AFields->Values[AFields->Names[0]].ToIntDef(0);
  switch (InqWrk)
   {
     case 0:  { strcpy(ARet,"���"); break;}
     case 1:  { strcpy(ARet,"������� �����"); break;}
     case 2:  { strcpy(ARet,"���� �������"); break;}
     default: { strcpy(ARet,""); }
   }
}
//---------------------------------------------------------------------------
#endif
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#            ��������� ��������� �������� �������������� ��������� (Def...)  #
//#                                                                            #
//#            ��������� ������������ ������ SQL-�������             (SQL...)  #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefChoiceValue(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   UnicodeString tmp = "";
   TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   if (NeedKonkr)
    {
      tmp = "<root><fl ref='"+_UID(FISAttr->AV["ref"])+"'/></root>";
    }
   return DefRegValue(AIE, AText, NeedKonkr, AXMLIE, tmp, _GUI(FISAttr->AV["ref"]), false, false);
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLChoiceValue(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefAgePeriodLast(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   // AIE - ������, ������ �� IE - ����� ����� 2-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "�������"
   // {
   //   1-� - ������� ��;
   //   2-� - ������� ��;
   // }
   if (!AutoFunc_DM) return false;
   bool iRC = false;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
      TDateConForm *Dlg = NULL;
      try
       {
         Dlg = new TDateConForm(Application->Owner);
         int FSubType;
         Dlg->SetType(_scRDate,AIE,FSubType,"");
         Dlg->MsgLab->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->Count == 2)
    {
      TTagNode *_tmpNode = AIE->GetFirstChild();
      UnicodeString _tmp;
      _tmp = _tmpNode->AV["name"]+_tmpNode->AV["value"];
      _tmpNode = _tmpNode->GetNext();
      _tmp += " "+_tmpNode->AV["name"]+_tmpNode->AV["value"];
      strcpy(AText,_tmp.c_str());
      _tmp = "";
      iRC = true;
    }
   strcpy(AXMLIE,AIE->AsXML.c_str());
   return iRC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLAgePeriodLast(TTagNode *AIE, char* ATab, char* ARet)
{
   TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
   // �������� ������� ��������
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   // �������� ������� �������� ������������ �������� ����� ����������
   TTagNode *FAddISAttr = FISCondRules->GetTagByUID(FISAttr->AV["add_ref"]);
   TTagNode *FAddIS;
   TTagNode *FIS = FISAttr->GetParent("is");
   strcpy(ATab,(FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":").c_str());
   UnicodeString xFr,xTo,flName,AddflName;
   xFr = ""; xTo = "";
   flName = FIS->AV["tabalias"]+"."+GetPart1(FISAttr->AV["fields"],',');
//   bool isCurrDate = false;
   if (FAddISAttr)
    {
      FAddIS = FAddISAttr->GetParent("is");
      AddflName = FAddIS->AV["tabalias"]+"."+FAddISAttr->AV["fields"];
    }
   else
    {
      if (GetPart2(UnicodeString(ARet),';').Length())
       {
         if (TDate(GetPart2(UnicodeString(ARet),';')) > TDate(_CDATEF_))
          AddflName = "'"+_CDATEF_+"'";
         else
          AddflName = "'"+GetPart2(UnicodeString(ARet),';')+"'";

       }
      else
       AddflName = "'"+_CDATEF_+"'";
//      isCurrDate = true;
    }
   TTagNode *TagFr, *TagTo;
   TagFr = AIE->GetChildByAV("text","name","��:");
   TagTo = AIE->GetChildByAV("text","name","��:");
   if (TagFr && (TagFr->AV["value"].Length()))
    {
       xFr = flName+"<="+_IncPeriod+"("+AddflName+",'"+TagFr->AV["value"]+"','/',1)";
    }
   if (TagTo && (TagTo->AV["value"].Length()))
    {
       xTo = flName+">="+_IncPeriod+"("+AddflName+",'"+TagTo->AV["value"]+"','/',1)";
    }
   if (xFr.Length() && xTo.Length())
    {
      strcpy(ARet,("("+xFr+" and "+xTo+")").c_str());
    }
   else if (xFr.Length())
    {
      strcpy(ARet,xFr.c_str());
    }
   else if (xTo.Length())
    {
      strcpy(ARet,xTo.c_str());
    }
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefDatePeriodLast(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   // AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "����"
   // {
   //   1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
   //                            1,2 ��� �������; 0 - �������� ���
   //                                             1 - ������ �� ������� ����
   //                                             2 - ������ �� ����
   //   2-� - ���� ��;   ��� ���� "�������� ���"
   //                    ��� ����� "������ �� ..." �������� �������
   //   3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
   //                    ��� ���� "������ �� ������� ����"  - �� �������������;
   //   4-� - �������� - ��� ���� "�������� ���"  - �� �������������
   //         �������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
   if (!AutoFunc_DM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
   if (NeedKonkr)
    {
      TDateConForm *Dlg = NULL;
      try
       {
         Dlg = new TDateConForm(Application->Owner);
         int FSubType;
         UnicodeString FSubTypeCapt = "";
         TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
         if (FISCondRules->GetParent("IS") == FISCondRules->GetParent("domain")->GetFirstChild()) // ���� ��������
          FSubType = 0;
         else  // ���� ������������
          {
            if (!FISAttr->AV["add_ref"].Length())
             FSubTypeCapt = "������ ��������� ������������";
            else
             FSubTypeCapt = FISAttr->AV["add_ref"];
            FSubType = 1;
          }
         bool DoNotDop = FISAttr->CmpAV("ref","4031DA5E-8AC52FB3-7D03.1024");
         Dlg->SetType(_scDate,AIE,FSubType,FSubTypeCapt,DoNotDop);
         Dlg->MsgLab->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count)
       {
         tmp = "";
         __tmp = AIE->GetFirstChild();
         int DType = AIE->GetFirstChild()->GetAVDef("value","0").ToInt();
         bool FLast = (bool)(DType&1);
         DType = DType >> 1;
         if (FLast)
          {
            TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
            if (!FISAttr->AV["add_ref"].Length())
             tmp = "������ ��������� ������������";
            else
             tmp = FISAttr->AV["add_ref"];
          }
         TTagNode *DateType   = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam1 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam2 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam3 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam4 = __tmp;
         if (DType == 0)
          {// �������� ���
            if ((DateParam1 && (DateParam1->AV["value"].Length())) ||
                (DateParam2 && (DateParam2->AV["value"].Length())))
             {
               tmp += " "+DateType->AV["name"];
               if (DateParam1->AV["value"].Length())
                {
                  tmp += " "+DateParam1->AV["name"];
                  tmp += "='"+DateParam1->AV["value"]+"'";
                }
               if (DateParam2->AV["value"].Length())
                {
                  tmp += " "+DateParam2->AV["name"];
                  tmp += "='"+DateParam2->AV["value"]+"'";
                }
             }
          }
         else if (DType == 3)
          {// ������ ������������ ���������
            tmp += " "+DateType->AV["name"];
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         else
          {// ������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
            tmp += " "+DateType->AV["name"];
            if (DateParam1->AV["value"].Length())
             {
               tmp += " "+DateParam1->AV["name"];
               tmp += "='"+DateParam1->AV["value"]+"'";
             }
            if (DateParam2->AV["value"].Length())
             {
               tmp += " "+DateParam2->AV["name"];
               tmp += "='"+DateParam2->AV["value"]+"'";
             }
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         if (DateParam4)
          {
            tmp += " "+DateParam4->AV["name"];
          }
         strcpy(AText,tmp.Trim().c_str());
       }
      iRC = true;
    }
   strcpy(AXMLIE,AIE->AsXML.c_str());
   return iRC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLDatePeriodLast(TTagNode *AIE, char* ATab, char* ARet)
{
   TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   TTagNode *DateOffs, *__tmp;
   bool Otvod = false;
   strcpy(ATab,(FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":").c_str());
   UnicodeString xFr,xTo,flName,xPer,xOffs,xTabName,xMaxSQL,xMaxTabName,MaxflName;
   xFr = ""; xTo = "";
   xTabName = FIS->AV["maintable"];
   flName = FIS->AV["tabalias"]+"."+FISAttr->AV["fields"];
   int DType = AIE->GetFirstChild()->GetAVDef("value","0").ToInt();
   bool FLast = (bool)(DType&1);
   DType = DType >> 1;
   TTagNode *TagFr, *TagTo, *NotDop;
   __tmp = AIE->GetFirstChild()->GetNext();
   TagFr = __tmp; __tmp = __tmp->GetNext();
   TagTo = __tmp; __tmp = __tmp->GetNext();
   DateOffs = __tmp; __tmp = __tmp->GetNext();
   NotDop = __tmp;
   xMaxTabName = "DTT"+IntToStr((int)FLast);
   xMaxSQL =  flName+" = (Select Max("+xMaxTabName+"."+FISAttr->AV["fields"]+") From "+xTabName+" "+xMaxTabName;
#ifdef _IMM
   xMaxSQL += " Where "+xMaxTabName+".UCODE="+FIS->AV["tabalias"]+".UCODE";
#endif
#ifdef _AKDO
   xMaxSQL += " Where "+xMaxTabName+".CD="+FIS->AV["tabalias"]+".CD)";
#endif
   MaxflName = xMaxTabName+"."+FISAttr->AV["fields"];
   if (DType == 0)
    {// �������� ���
      if (TagFr && (TagFr->AV["value"].Length()))
       xFr = ">='"+_CorrectDate(TagFr->AV["value"])+"'";
      if (TagTo && (TagTo->AV["value"].Length()))
       xTo = "<='"+_CorrectDate(TagTo->AV["value"])+"'";
    }
   else if (DType == 1)
    {// ������ �� ������� ����
      xPer = TagFr->AV["value"];
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
         xFr = ">='"+FIncPeriod(_CDATEF_,xPer,1)+"'";
         xTo = "<='"+_CDATEF_+"'";
       }
      else
       {
         xFr = ">='"+FIncPeriod(_CDATEF_,AddPeriod(xPer,xOffs),1)+"'";
         xTo = "<='"+FIncPeriod(_CDATEF_,xOffs,1)+"'";
       }
    }
   else if (DType == 2)
    {// ������ �� ���� ��������� ������� ������������ ���������
      UnicodeString FDocPer = UnicodeString(ARet);
      if (FDocPer.Length())
       {
         if (GetPart2(FDocPer,';').Length())
          FDocPer = GetPart2(FDocPer,';');
         else
          FDocPer = _CDATEF_;
       }
      else
       FDocPer = _CDATEF_;
      xPer  = TagFr->AV["value"];
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
         xFr = ">='"+FIncPeriod(FDocPer,xPer,1)+"'";
         xTo = "<='"+FDocPer+"'";
       }
      else
       {
         xFr = ">='"+FIncPeriod(FDocPer,AddPeriod(xPer,xOffs),1)+"'";
         xTo = "<='"+FIncPeriod(FDocPer,xOffs,1)+"'";
       }
    }
   else if (DType == 3)
    {// ������ ������������ ���������
      UnicodeString FPer = UnicodeString(ARet);
      if (FPer.Length())
       {
         xOffs = DateOffs->AV["value"];
         if (!xOffs.Length())
          {
            if (GetPart1(FPer,';').Length())
             xFr = ">='"+TDate(GetPart1(FPer,';'))._FS+"'";
            if (GetPart2(FPer,';').Length())
             xTo = "<='"+TDate(GetPart2(FPer,';'))._FS+"'";
          }
         else
          {
            if (GetPart1(FPer,';').Length())
             xFr = ">='"+FIncPeriod(GetPart1(FPer,';'),xOffs,1)+"'";
            if (GetPart2(FPer,';').Length())
             xTo = "<='"+FIncPeriod(GetPart2(FPer,';'),xOffs,1)+"'";
          }
       }
    }
   if (xFr.Length())
    {
      xMaxSQL += " and "+MaxflName+xFr;
      xFr = flName+xFr;
    }
   if (xTo.Length())
    {
      xMaxSQL += " and "+MaxflName+xTo;
      xTo = flName+xTo;
    }
   if (FLast)
    {
#ifdef _IMM
      TTagNode *tmp;
      // ��������
      tmp = AIE->GetParent();
      tmp = tmp->GetChildByAV("IERef","ref","0E291426-00005882-2493."+FGetRefValue(FISAttr, "0070","0086"));
      if (tmp)
       {
         if (tmp->Count > 1) // ���������� �������� ?
          {
            TTagNode *itNode = tmp->GetFirstChild();
            while (itNode)
             {
               if (itNode->CmpAV("ref","1"))       // ���������� �������� �������� !!!
                xMaxSQL += " and "+xMaxTabName+".R1075="+itNode->AV["value"].Trim();
               else if (itNode->CmpAV("ref","2"))  // ���������� �������� ������� !!!
                xMaxSQL += " and "+xMaxTabName+".R1020="+itNode->AV["value"].Trim();
//               else if (itNode->CmpAV("ref","3"))  // ���������� �������� ����� !!!
//                xMaxSQL += " and Upper("+xMaxTabName+".R1023)='"+itNode->AV["value"].Trim().UpperCase()+"'";
//               else if (itNode->CmpAV("ref","4"))  // ���������� �������� ���� ���������� !!!
//                xMaxSQL += " and Upper("+xMaxTabName+".R1021)='"+itNode->AV["value"].Trim().UpperCase()+"'";
               itNode = itNode->GetNext();
             }
          }
         if (NotDop)
          {
            if (NotDop->CmpAV("value","1"))
             xMaxSQL += " and Upper("+xMaxTabName+".R1021)<>'���.'";
          }
       }
      else
       {
          // �����
          tmp = AIE->GetParent();
          tmp = tmp->GetChildByAV("IERef","ref","0E291426-00005882-2493."+FGetRefValue(FISAttr, "0044","00B0"));
          if (tmp)
          {
            tmp = tmp->GetChildByAV("text","ref","0");
            if (tmp) // ���������� �������� ����� ?
             {
               xMaxSQL += " and "+xMaxTabName+".R1032="+tmp->AV["value"];
             }
          }
       }
      xMaxSQL += ")";
      strcpy(ARet,xMaxSQL.c_str());
#endif
    }
   if (xFr.Length() && xTo.Length())
    {
      if (FLast)
       strcpy(ARet,("("+xFr+" and "+xTo+" and "+xMaxSQL+")").c_str());
      else
       strcpy(ARet,("("+xFr+" and "+xTo+")").c_str());
    }
   else if (xFr.Length())
    {
      if (FLast)
       strcpy(ARet,("("+xFr+" and "+xMaxSQL+")").c_str());
      else
       strcpy(ARet,xFr.c_str());
    }
   else if (xTo.Length())
    {
      if (FLast)
       strcpy(ARet,("("+xTo+" and "+xMaxSQL+")").c_str());
      else
       strcpy(ARet,xTo.c_str());
    }
   else
    {
      if (FLast)
       strcpy(ARet,xMaxSQL.c_str());
      else
       strcpy(ARet,"");
    }
}
//---------------------------------------------------------------------------
#ifdef _IMM
_DLLEXPB_ __stdcall DefOtvodDatePeriodLast(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   // AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "����"
   // {
   //   1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
   //                            1,2 ��� �������; 0 - �������� ���
   //                                             1 - ������ �� ������� ����
   //                                             2 - ������ �� ����
   //   2-� - ���� ��;   ��� ���� "�������� ���"
   //                    ��� ����� "������ �� ..." �������� �������
   //   3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
   //                    ��� ���� "������ �� ������� ����"  - �� �������������;
   //   4-� - �������� - ��� ���� "�������� ���"  - �� �������������
   //         �������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
   if (!AutoFunc_DM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
   if (NeedKonkr)
    {
      TDateConForm *Dlg = NULL;
      try
       {
         Dlg = new TDateConForm(Application->Owner);
         int FSubType;
         UnicodeString FSubTypeCapt = "";
         TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
         if (FISCondRules->GetParent("IS") == FISCondRules->GetParent("domain")->GetFirstChild()) // ���� ��������
          FSubType = 0;
         else  // ���� ������������
          {
            if (!FISAttr->AV["add_ref"].Length())
             FSubTypeCapt = "������ ��������� ������������";
            else
             FSubTypeCapt = FISAttr->AV["add_ref"];
            FSubType = 1;
          }
         bool DoNotDop = FISAttr->CmpAV("ref","4031DA5E-8AC52FB3-7D03.1024");
         Dlg->SetType(_scDate,AIE,FSubType,FSubTypeCapt,DoNotDop);
         Dlg->MsgLab->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count)
       {
         tmp = "";
         __tmp = AIE->GetFirstChild();
         int DType = AIE->GetFirstChild()->GetAVDef("value","0").ToInt();
         bool FLast = (bool)(DType&1);
         DType = DType >> 1;
         if (FLast)
          {
            TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
            if (!FISAttr->AV["add_ref"].Length())
             tmp = "������ ��������� ������������";
            else
             tmp = FISAttr->AV["add_ref"];
          }
         TTagNode *DateType   = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam1 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam2 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam3 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam4 = __tmp;
         if (DType == 0)
          {// �������� ���
            if ((DateParam1 && (DateParam1->AV["value"].Length())) ||
                (DateParam2 && (DateParam2->AV["value"].Length())))
             {
               tmp += " "+DateType->AV["name"];
               if (DateParam1->AV["value"].Length())
                {
                  tmp += " "+DateParam1->AV["name"];
                  tmp += "='"+DateParam1->AV["value"]+"'";
                }
               if (DateParam2->AV["value"].Length())
                {
                  tmp += " "+DateParam2->AV["name"];
                  tmp += "='"+DateParam2->AV["value"]+"'";
                }
             }
          }
         else if (DType == 3)
          {// ������ ������������ ���������
            tmp += " "+DateType->AV["name"];
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         else
          {// ������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
            tmp += " "+DateType->AV["name"];
            if (DateParam1->AV["value"].Length())
             {
               tmp += " "+DateParam1->AV["name"];
               tmp += "='"+DateParam1->AV["value"]+"'";
             }
            if (DateParam2->AV["value"].Length())
             {
               tmp += " "+DateParam2->AV["name"];
               tmp += "='"+DateParam2->AV["value"]+"'";
             }
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         if (DateParam4)
          {
            tmp += " "+DateParam4->AV["name"];
          }
         strcpy(AText,tmp.Trim().c_str());
       }
      iRC = true;
    }
   strcpy(AXMLIE,AIE->AsXML.c_str());
   return iRC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLOtvodDatePeriodLast(TTagNode *AIE, char* ATab, char* ARet)
{
   TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   TTagNode *DateOffs, *__tmp;
   bool Otvod = false;
   Otvod = FISCondRules->CmpAV("uid","007B,007C");
   strcpy(ATab,(FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":").c_str());
   UnicodeString xFr,xTo,flName,xPer,xOffs,xTabName,xMaxSQL,xMaxTabName,MaxflName;
   xFr = ""; xTo = "";
   xTabName = FIS->AV["maintable"];
   flName = FIS->AV["tabalias"]+"."+FISAttr->AV["fields"];
   int DType = AIE->GetFirstChild()->GetAVDef("value","0").ToInt();
   bool FLast = (bool)(DType&1);
   DType = DType >> 1;
   TTagNode *TagFr, *TagTo, *NotDop;
   __tmp = AIE->GetFirstChild()->GetNext();
   TagFr = __tmp; __tmp = __tmp->GetNext();
   TagTo = __tmp; __tmp = __tmp->GetNext();
   DateOffs = __tmp; __tmp = __tmp->GetNext();
   NotDop = __tmp;
   xMaxTabName = "DTT"+IntToStr((int)FLast);
   xMaxSQL =  flName+" = (Select Max("+xMaxTabName+"."+FISAttr->AV["fields"]+") From "+xTabName+" "+xMaxTabName;
   xMaxSQL += " Where "+xMaxTabName+".UCODE="+FIS->AV["tabalias"]+".UCODE";
   MaxflName = xMaxTabName+"."+FISAttr->AV["fields"];
   if (DType == 0)
    {// �������� ���
      if (TagFr && (TagFr->AV["value"].Length()))
       xFr = "'"+_CorrectDate(TagFr->AV["value"])+"'";
      if (TagTo && (TagTo->AV["value"].Length()))
       xTo = "'"+_CorrectDate(TagTo->AV["value"])+"'";
    }
   else if (DType == 1)
    {// ������ �� ������� ����
      xPer = TagFr->AV["value"];
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
         xFr = "'"+FIncPeriod(_CDATEF_,xPer,1)+"'";
         xTo = "'"+_CDATEF_+"'";
       }
      else
       {
         xFr = "'"+FIncPeriod(_CDATEF_,AddPeriod(xPer,xOffs),1)+"'";
         xTo = "'"+FIncPeriod(_CDATEF_,xOffs,1)+"'";
       }
    }
   else if (DType == 2)
    {// ������ �� ���� ��������� ������� ������������ ���������
      UnicodeString FDocPer = UnicodeString(ARet);
      if (FDocPer.Length())
       {
         if (GetPart2(FDocPer,';').Length())
          FDocPer = GetPart2(FDocPer,';');
         else
          FDocPer = _CDATEF_;
       }
      else
       FDocPer = _CDATEF_;
      xPer  = TagFr->AV["value"];
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
         xFr = ">='"+FIncPeriod(FDocPer,xPer,1)+"'";
         xTo = "<='"+FDocPer+"'";
       }
      else
       {
         xFr = ">='"+FIncPeriod(FDocPer,AddPeriod(xPer,xOffs),1)+"'";
         xTo = "<='"+FIncPeriod(FDocPer,xOffs,1)+"'";
       }
    }
   else if (DType == 3)
    {// ������ ������������ ���������
      UnicodeString FPer = UnicodeString(ARet);
      if (FPer.Length())
       {
         xOffs = DateOffs->AV["value"];
         if (!xOffs.Length())
          {
            if (GetPart1(FPer,';').Length())
             xFr = "'"+TDate(GetPart1(FPer,';'))._FS+"'";
            if (GetPart2(FPer,';').Length())
             xTo = "'"+TDate(GetPart2(FPer,';'))._FS+"'";
          }
         else
          {
            if (GetPart1(FPer,';').Length())
             xFr = "'"+FIncPeriod(GetPart1(FPer,';'),xOffs,1)+"'";
            if (GetPart2(FPer,';').Length())
             xTo = "'"+FIncPeriod(GetPart2(FPer,';'),xOffs,1)+"'";
          }
       }
    }
   UnicodeString frFlName,toFlName,frMaxFlName,toMaxFlName,CommSQL;
   frFlName = xTabName+".R105F";
   toFlName = xTabName+".R1065";
   frMaxFlName = xMaxTabName+".R105F";
   toMaxFlName = xMaxTabName+".R1065";
   CommSQL = "";
   if (xFr.Length() && xTo.Length())
    {
      xMaxSQL += " and "+frMaxFlName+" <= "+xTo+" and ("+toMaxFlName+">="+xFr+" or "+toMaxFlName+" is NULL)";
      CommSQL = frFlName+" <= "+xTo+" and ("+toFlName+">="+xFr+" or "+toFlName+" is NULL)";
    }
   else if (xFr.Length())
    {
      xMaxSQL += " and ("+toMaxFlName+">="+xFr+" or "+toMaxFlName+" is NULL)";
      CommSQL = "("+toFlName+">="+xFr+" or "+toFlName+" is NULL)";
    }
   else if (xTo.Length())
    {
      xMaxSQL += " and "+frMaxFlName+" <= "+xTo;
      CommSQL = frFlName+" <= "+xTo;
    }
   if (FLast)
    {
      TTagNode *tmp;
      tmp = AIE->GetParent();
      tmp = tmp->GetChildByAV("IERef","ref","0E291426-00005882-2493."+FGetRefValue(FISAttr, "004F","00BB"));
      if (tmp)
       {
         tmp = tmp->GetChildByAV("text","ref","0");
         if (tmp) // ���������� �������� �������� ?
          {
            xMaxSQL += " and "+xMaxTabName+".R1083="+tmp->AV["value"];
          }
       }
      tmp = AIE->GetParent();
      tmp = tmp->GetChildByAV("IERef","ref","0E291426-00005882-2493."+FGetRefValue(FISAttr, "004E","00BA"));
      if (tmp)
       {
         tmp = tmp->GetChildByAV("text","ref","0");
         if (tmp) // �������� ��� ������ ?
          {
            xMaxSQL += " and "+xMaxTabName+".R1043="+tmp->AV["value"];
          }
       }
      xMaxSQL += ")";
    }
   if (CommSQL.Length())
    {
      if (FLast)
       strcpy(ARet,("("+CommSQL+" and "+xMaxSQL+")").c_str());
      else
       strcpy(ARet,("("+CommSQL+")").c_str());
    }
   else
    {
      if (FLast)
       strcpy(ARet,xMaxSQL.c_str());
      else
       strcpy(ARet,"");
    }
}
//---------------------------------------------------------------------------
#endif
_DLLEXPB_ __stdcall DefChoiceDBValue(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   UnicodeString tmp = "";
   TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   if (NeedKonkr)
    {
      TTagNode *FExtAttr = AutoFunc_DM->XMLList->GetRefer(FISAttr->AV["ref"]);
      tmp = "<root>"
            "<fl ref='"+FExtAttr->AV["uid"]+"' req=1'/>"
            "</root>";
    }
   return DefRegValue(AIE, AText, NeedKonkr, AXMLIE, tmp, _GUI(FISAttr->AV["ref"]), false, false);
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLChoiceDBValue(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefInqValue(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
      TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      TTagNode *FExtAttr = AutoFunc_DM->XMLList->GetRefer(FISAttr->AV["ref"]);
      TConForm *ConDlg = NULL;
      TStringList *DDD = NULL;
      try
       {
         DDD = new TStringList;
         ConDlg = new TConForm(Application->Owner);
         ConDlg->MsgLab->Caption = FExtAttr->AV["name"];
         int scType = GetScaleStrs(FExtAttr, DDD);
         ConDlg->ValueCB->Properties->Items->Assign(DDD);
         ConDlg->SetType(scType,AIE);
         ConDlg->ShowModal();
       }
      __finally
       {
         if (ConDlg) delete ConDlg;
         if (DDD) delete DDD;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         __tmp = AIE->GetFirstChild();
         tmp = "";
         while (__tmp->GetNext())
          {
            tmp += " "+__tmp->AV["name"];
            __tmp = __tmp->GetNext();
          }
         strcpy(AText,tmp.Trim().c_str());
       }
      iRC = true;
    }
   strcpy(AXMLIE,AIE->AsXML.c_str());
   return iRC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLInqValue(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefInqTreeValue(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   if (NeedKonkr)
    {
      try
       {
         AutoFunc_DM->MKBDlg->ShowModal();
         if (AutoFunc_DM->MKBDlg->ModalResult == mrOk)
          {
            AIE->DeleteChild();
            TTagNode *tmp = AIE->AddChild("text");
            tmp->AddAttr("name",AutoFunc_DM->MKBDlg->clExtCode+" "+AutoFunc_DM->MKBDlg->clText);
            tmp->AddAttr("value",IntToStr(AutoFunc_DM->MKBDlg->clCode));
            tmp->AddAttr("ref","0");
            AIE->AddChild("text","name=������ ,value="+IntToStr((int)AutoFunc_DM->MKBDlg->clSelectGroup)+", ref=1");
            AIE->AddChild("text","name=���1 ,value="+IntToStr(AutoFunc_DM->MKBDlg->clCode)+", ref=2");
            AIE->AddChild("text","name=���2 ,value="+IntToStr(AutoFunc_DM->MKBDlg->clCodeLast)+", ref=3");
            if (AutoFunc_DM->MKBDlg->clSelectGroup)
             AIE->AddChild("text","name=SQL ,value=(MKB.CODE >= "+IntToStr(AutoFunc_DM->MKBDlg->clCode)+" and MKB.CODE <= "+IntToStr(AutoFunc_DM->MKBDlg->clCodeLast)+"), ref=4");
            else
             AIE->AddChild("text","name=SQL ,value=MKB.CODE = "+IntToStr(AutoFunc_DM->MKBDlg->clCode)+", ref=4");
          }
       }
      __finally
       {
       }
    }
   if (AIE->GetFirstChild())
    {
      tmp = "";
      if (AIE->Count > 1)
       {
         if (AIE->GetFirstChild()->GetNext())
          {
            if (AIE->GetFirstChild()->GetNext()->AV["value"] != "0")
             tmp += "{��������� ���������}";
          }
         tmp += AIE->GetFirstChild()->AV["name"];
         strcpy(AText,tmp.Trim().c_str());
       }
      iRC = true;
    }
   strcpy(AXMLIE,AIE->AsXML.c_str());
   return iRC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLInqTreeValue(TTagNode *AIE, char* ATab, char* ARet)
{
   TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   UnicodeString FTabName = FIS->AV["tabalias"];
   UnicodeString FColName = FTabName+"."+FISAttr->AV["fields"];
   UnicodeString ISTab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   strcpy(ATab,ISTab.c_str());
   if (!AIE->Count)
    {
      strcpy(ARet,"");
    }
   else
    {
      TTagNode *FMCode = AIE->GetChildByAV("text","ref","1");
      if (FMCode)
       {
#ifdef _IMM
         if (FMCode->CmpAV("value","1"))
          strcpy(ARet,("("+FColName+" >= "+FMCode->GetNext()->AV["value"]+" and "+FColName+" <= "+FMCode->GetNext()->GetNext()->AV["value"]+")").c_str());
         else
          strcpy(ARet,(" "+FColName+" = "+FMCode->GetNext()->AV["value"]).c_str());
#endif
#ifdef _AKDO
         UnicodeString LoCode, HiCode;
         LoCode = FMCode->GetNext()->AV["value"];
         HiCode = FMCode->GetNext()->GetNext()->AV["value"];
         if (FMCode->CmpAV("value","1"))
          strcpy(ARet,("("+FColName+" >= "+LoCode+" and "+FColName+" <= "+HiCode+")").c_str());
         else
          strcpy(ARet,(" "+FColName+" = "+LoCode).c_str());
#endif
       }
      else      strcpy(ARet,"");
    }
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefInqValueVal(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
      TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      TTagNode *FExtAttr = AutoFunc_DM->XMLList->GetRefer(FISAttr->AV["ref"]);
      TConForm *ConDlg = NULL;
      TStringList *DDD = NULL;
      try
       {
         DDD = new TStringList;
         ConDlg = new TConForm(Application->Owner);
         ConDlg->MsgLab->Caption = FExtAttr->AV["name"];
         DDD->AddObject("min",(TObject*)0);
         DDD->AddObject("max",(TObject*)1000);
         int scType = _scDigit;
         ConDlg->ValueCB->Properties->Items->Assign(DDD);
         ConDlg->SetType(scType,AIE);
         ConDlg->ShowModal();
       }
      __finally
       {
         if (ConDlg) delete ConDlg;
         if (DDD) delete DDD;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         __tmp = AIE->GetFirstChild();
         tmp = "";
         while (__tmp->GetNext())
          {
            tmp += " "+__tmp->AV["name"];
            __tmp = __tmp->GetNext();
          }
         strcpy(AText,tmp.Trim().c_str());
       }
      iRC = true;
    }
   strcpy(AXMLIE,AIE->AsXML.c_str());
   return iRC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLInqValueVal(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefPresZakl(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool RC = false;
//   if (NeedKonkr)
//    {
   TStringList *FValues = NULL;
   try
    {
       FValues = new TStringList;
       FValues->AddObject("������ ����������",(TObject*)0);
       FValues->AddObject("������ �������",(TObject*)1);
       RC = DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
    }
   __finally
    {
      if (FValues) delete FValues;
    }
//    }
   return RC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLPresZakl(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefRegVal(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool CanAllSelect = false;
   bool SubSel = false;
   UnicodeString tmp = "";
   UnicodeString FGUI = "";
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
      if (FISCondRules)
       {
         TTagNode *FRoot = FISCondRules->GetChildByName("root");
         if (FRoot)
          {
            tmp = FRoot->AsXML;
            CanAllSelect = (bool)(FRoot->AV["CanAllSelect"].ToIntDef(0));
            SubSel       = (bool)(FRoot->AV["subsel"].ToIntDef(0));
            FGUI         = FRoot->AV["gui_ref"];
          }
       }
    }
   return DefRegValue(AIE, AText, NeedKonkr, AXMLIE, tmp, FGUI, CanAllSelect, SubSel);
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLRegVal(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefTextValue(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
      TTextConForm *Dlg = NULL;
      try
       {
         Dlg = new TTextConForm(Application->Owner, FISCondRules->AV["name"],AIE);
         Dlg->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         __tmp = AIE->GetFirstChild();
         tmp = "";
         while (__tmp->GetNext())
          {
            tmp += " "+__tmp->AV["name"];
            __tmp = __tmp->GetNext();
          }
         strcpy(AText,tmp.Trim().c_str());
       }
      iRC = true;
    }
   strcpy(AXMLIE,AIE->AsXML.c_str());
   return iRC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLTextValue(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefMInqPer(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   // AIE - ������, ������ �� IE - ����� ����� 2-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "�������"
   // {
   //   1-� - ������� ��;
   //   2-� - ������� ��;
   // }
   if (!AutoFunc_DM) return false;
   bool iRC = false;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
      TDateConForm *Dlg = NULL;
      try
       {
         Dlg = new TDateConForm(Application->Owner);
         int FSubType;
         Dlg->SetType(_scRDate,AIE,FSubType,"");
         Dlg->MsgLab->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->Count == 2)
    {
      TTagNode *_tmpNode = AIE->GetFirstChild();
      UnicodeString _tmp;
      _tmp = _tmpNode->AV["name"]+_tmpNode->AV["value"];
      _tmpNode = _tmpNode->GetNext();
      _tmp += " "+_tmpNode->AV["name"]+_tmpNode->AV["value"];
      strcpy(AText,_tmp.c_str());
      _tmp = "";
      iRC = true;
    }
   strcpy(AXMLIE,AIE->AsXML.c_str());
   return iRC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLMInqPer(TTagNode *AIE, char* ATab, char* ARet)
{
   TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
   // �������� ������� ��������
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   // �������� ������� �������� ������������ �������� ����� ����������
   TTagNode *FAddISAttr = FISCondRules->GetTagByUID(FISAttr->AV["add_ref"]);
   TTagNode *FAddIS;
   TTagNode *FIS = FISAttr->GetParent("is");
   strcpy(ATab,(FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":").c_str());
   UnicodeString xFr,xTo,flName,AddflName;
   xFr = ""; xTo = "";
   flName = FIS->AV["tabalias"]+"."+GetPart1(FISAttr->AV["fields"],',');
//   bool isCurrDate = false;
   if (FAddISAttr)
    {
      FAddIS = FAddISAttr->GetParent("is");
      AddflName = FAddIS->AV["tabalias"]+"."+FAddISAttr->AV["fields"];
    }
   else
    {
      AddflName = "'"+_CDATEF_+"'";
//      isCurrDate = true;
    }
   TTagNode *TagFr, *TagTo;
   TagFr = AIE->GetChildByAV("text","name","��:");
   TagTo = AIE->GetChildByAV("text","name","��:");
   if (TagFr && (TagFr->AV["value"].Length()))
    {
       xFr = "MORBIDITY.INQDATEBEG<="+_IncPeriod+"(MORBIDITY.INQDATEEND,'"+TagFr->AV["value"]+"','/',1)";
    }
   if (TagTo && (TagTo->AV["value"].Length()))
    {
       xTo = "MORBIDITY.INQDATEBEG>="+_IncPeriod+"(MORBIDITY.INQDATEEND,'"+TagFr->AV["value"]+"','/',1)";
    }
   if (xFr.Length() && xTo.Length())
    {
      strcpy(ARet,("("+xFr+" and "+xTo+")").c_str());
    }
   else if (xFr.Length())
    {
      strcpy(ARet,xFr.c_str());
    }
   else if (xTo.Length())
    {
      strcpy(ARet,xTo.c_str());
    }
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefMInqIllType(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool RC = false;
//   if (NeedKonkr)
//    {
   TStringList *FValues = NULL;
   try
    {
       FValues = new TStringList;
       FValues->AddObject("�� ������",(TObject*)0);
       FValues->AddObject("������",(TObject*)1);
       FValues->AddObject("������� � ����� ��������.����.�����������",(TObject*)2);
       FValues->AddObject("������� ��������. � ��������. ���� ��� �����",(TObject*)3);
       FValues->AddObject("���������� ����.�����������",(TObject*)4);
       RC = DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
    }
   __finally
    {
      if (FValues) delete FValues;
    }
//    }
   return RC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLMInqIllType(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefMInqType(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool RC = false;
//   if (NeedKonkr)
//    {
   TStringList *FValues = NULL;
   try
    {
       FValues = new TStringList;
       FValues->AddObject("���������",(TObject*)1);
       FValues->AddObject("���������",(TObject*)2);
       RC = DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
    }
   __finally
    {
      if (FValues) delete FValues;
    }
//    }
   return RC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLMInqType(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefMInqWrk(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool RC = false;
//   if (NeedKonkr)
//    {
   TStringList *FValues = NULL;
   try
    {
       FValues = new TStringList;
       FValues->AddObject("����.",(TObject*)0);
       FValues->AddObject("���.",(TObject*)1);
       FValues->AddObject("��.�����.",(TObject*)2);
       RC = DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
    }
   __finally
    {
      if (FValues) delete FValues;
    }
//    }
   return RC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLMInqWrk(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefMInqLPU(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool RC = false;
//   if (NeedKonkr)
//    {
   TStringList *FValues = NULL;
   try
    {
       FValues = new TStringList;
       for (TAnsiStrMap::iterator i = AutoFunc_DM->LPUList.begin(); i != AutoFunc_DM->LPUList.end(); i++)
         FValues->AddObject(i->second,(TObject*)(i->first.ToInt()));
       RC = DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
    }
   __finally
    {
      if (FValues) delete FValues;
    }
//    }
   return RC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLMInqLPU(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefDOutval(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool RC = false;
   TStringList *FValues = NULL;
   try
    {
       FValues = new TStringList;
       FValues->AddObject("�� ���������",(TObject*)0);
       FValues->AddObject("���������",(TObject*)1);
       RC = DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
    }
   __finally
    {
      if (FValues) delete FValues;
    }
   return RC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLDOutval(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefDLinkDiag(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool RC = false;
//   if (NeedKonkr)
//    {
   TStringList *FValues = NULL;
   try
    {
       FValues = new TStringList;
       FValues->AddObject("������ ����������",(TObject*)0);
       FValues->AddObject("������ �������",(TObject*)1);
       RC = DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
    }
   __finally
    {
      if (FValues) delete FValues;
    }
//    }
   return RC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLDLinkDiag(TTagNode *AIE, char* ATab, char* ARet)
{
   TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   UnicodeString FTabName = FIS->AV["tabalias"];
   UnicodeString FColName = FTabName+"."+FISAttr->AV["fields"];
   UnicodeString ISTab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   strcpy(ATab,ISTab.c_str());
   if (AIE->GetFirstChild()->AV["value"] == "0")
    strcpy(ARet," IDIAG.DIAG not in (Select MRB1.DIAGCODE from MORBIDITY MRB1 where MRB1.code=IDIAG.CD)");
   else
    strcpy(ARet," IDIAG.DIAG in (Select MRB1.DIAGCODE from MORBIDITY MRB1 where MRB1.code=IDIAG.CD)");
}
//---------------------------------------------------------------------------
#ifdef _IMM
_DLLEXPB_ __stdcall DefPlanPrivReak(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
      TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
//      TTagNode *FExtAttr = AutoFunc_DM->XMLList->GetRefer(FISAttr->AV["ref"]);
      __tmp = AIE->GetParent();
      __tmp = __tmp->GetChildByAV("IERef","ref","0E291426-00005882-2493."+FGetRefValue(FISAttr, "0070","0086"));
      tmp = "";
      if (__tmp)
       {
         __tmp = __tmp->GetChildByAV("text","ref","2");
         if (__tmp) 
          { // ���������� �������� ������� !!!
            tmp = __tmp->AV["value"];
          }
       }
      if (!tmp.Length())
       {
         ShowMessage("�� ����� ����!\n ������ ������ ������� �� ��������, \n�.�. ������ ������� ������� �� ���� ����");
         return false;
       }
      tmp = "<root>"
            "<fl ref='1020' def='"+tmp+"' en='0'/>"
            "<fl ref='1025'/>"
            "<fl ref='1026'/>"
            "<fl ref='1028'/>"
            "<fl ref='10A8'/>"
            "<fl ref='1027'/>"
            "</root>";
    }
   return DefRegValue(AIE, AText, NeedKonkr, AXMLIE, tmp, "", false, false);
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLPlanPrivReak(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefPlanProbReak(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   UnicodeString tmp;
   TTagNode *__tmp;
   bool iRC = false;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
      TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      __tmp = AIE->GetParent();
      __tmp = __tmp->GetChildByAV("IERef","ref","0E291426-00005882-2493."+FGetRefValue(FISAttr, "0044","00B0"));
      tmp = "";
      if (__tmp)
       {
         __tmp = __tmp->GetChildByAV("text","ref","0");
         if (__tmp)
          {// ���������� �������� ����� !!!
             tmp = __tmp->AV["value"];
          }
       }
      if (!tmp.Length())
       {
         ShowMessage("�� ������ �����!\n ������ ������ ������� �� ��������, \n�.�. ������ ������� ������� �� ���� �����");
         return false;
       }
      TProbReakForm *Dlg = NULL;
      try
       {
         Dlg = new TProbReakForm(Application->Owner, AIE, NeedKonkr,tmp);
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         __tmp = AIE->GetFirstChild();
         tmp = "";
         while (__tmp->GetNext())
          {
            tmp += " "+__tmp->AV["name"];
            __tmp = __tmp->GetNext();
          }
         strcpy(AText,tmp.Trim().c_str());
       }
      iRC = true;
    }
   strcpy(AXMLIE,AIE->AsXML.c_str());
   return iRC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLPlanProbReak(TTagNode *AIE, char* ATab, char* ARet)
{
   TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   strcpy(ATab,(FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":").c_str());
   UnicodeString FSQL = "";
   TTagNode *_tmp;
   FSQL = GetChildContAVDef(AIE, "������ ���������:", "-1");
   int FFixVal, FDelta, FDelta2;
   if (FSQL != "-1")
    {
      if (FSQL == "0")
       { // ������ ����������
          FFixVal = GetChildContAVDef(AIE, "����������� �������� ������:", "0").ToIntDef(0);
          FDelta  = GetChildContAVDef(AIE, "����������:", "0").ToIntDef(0);
          FDelta2 = GetChildContAVDef(AIE, "���������� �� ���������:", "0").ToIntDef(0);
          if (!(FDelta2 > 0))
           FDelta2 = FFixVal;
          FSQL =  "PROB.R1041 = 1 and PROB.R10A7=2 and ( ";
          FSQL += "(PROB.R103F >= "+IntToStr(FFixVal)+" and PROB.R10C2=2 and ";
          FSQL += "(PROB.R103F - PROB.R10C1) >= "+IntToStr(FDelta)+") or ";
          FSQL += "(PROB.R103F >= "+IntToStr(FDelta2)+" and PROB.R10C2=7))";
       }
      else
       { // ������ ������
          FFixVal = GetChildContAVDef(AIE, "����������� �������� ������:", "0").ToIntDef(0);
          FSQL = "PROB.R1041 = 1 and PROB.R103F > "+IntToStr(FFixVal)+" and PROB.R10A7=2 and (PROB.R10C1<2 or PROB.R10C1 is NULL)";
       }
    }
   else
    FSQL = "";
   _tmp = AIE->GetChildByAV("text","name","sql");
   if (_tmp)
    {
      if (FSQL.Length()) FSQL = FSQL+" and "+_tmp->AV["value"];
      else               FSQL = _tmp->AV["value"];
    }
   strcpy(ARet,FSQL.c_str());
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefPlanPres(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool iRC = false;
   UnicodeString tmp;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
      TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      TTagNode *FExtAttr = AutoFunc_DM->XMLList->GetRefer(FISAttr->AV["ref"]);
      tmp = "<root>"
            "<fl ref='1086'/>"
            "<fl ref='1089'/>"
            "<fl ref='108A'/>"
            "</root>";
      TRegPPConForm *Dlg = NULL;
      try
       {
         Dlg = new TRegPPConForm(Application->Owner, tmp, AIE, FExtAttr->GetRoot()->CmpName("registr"), NeedKonkr);
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         TTagNode *__tmp = AIE->GetFirstChild();
         tmp = "";
         while (__tmp)
          {
            if (!__tmp->CmpAV("name","sql"))
             tmp += " "+__tmp->AV["name"];
            __tmp = __tmp->GetNext();
          }
         strcpy(AText,tmp.Trim().c_str());
       }
      iRC = true;
    }
   strcpy(AXMLIE,AIE->AsXML.c_str());
   return iRC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLPlanPres(TTagNode *AIE, char* ATab, char* ARet)
{
   TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   strcpy(ATab,(FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":").c_str());
   TTagNode *_tmp = AIE->GetChildByAV("text","name","sql");
   if (_tmp)
    {
      TDate FDateBP;
      TDate FDateEP;
      TDate FPlanMonth;
      CalcPlanPeriod(&FDateBP, &FDateEP, &FPlanMonth);
      UnicodeString cdPlan,pdPlan;
      cdPlan = FPlanMonth.FormatString("01.mm.yyyy");
      pdPlan = Dateutil::IncMonth(FPlanMonth,-1).FormatString("01.mm.yyyy");
      TReplaceFlags rFlag;
      rFlag << rfReplaceAll;
      strcpy(ARet,StringReplace(StringReplace(_tmp->AV["value"], "_PREV_", pdPlan, rFlag), "_CURR_", cdPlan, rFlag).c_str());
    }
   else      strcpy(ARet,"");
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefExtEditValue(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   UnicodeString tmp;
   TTagNode *__tmp;
   bool iRC = false;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
      TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      __tmp = AIE->GetParent();
      __tmp = __tmp->GetChildByAV("IERef","ref","0E291426-00005882-2493."+FGetRefValue(FISAttr, "0050","006D"));
      tmp = "";
      if (__tmp)
       {
         __tmp = __tmp->GetChildByAV("text","ref","0");
         if (__tmp)
          { // ���������� �������� �������� !!!
            tmp = __tmp->AV["value"];
          }
       }
      if (!tmp.Length())
       {
         ShowMessage("�� ������ ����������!\n ������ ����� ���������� �������� �� ��������, \n�.�. ����� ���������� �������� ������� �� ���� ��������");
         return false;
       }
      TSchForm *Dlg = NULL;
      try
       {
         Dlg = new TSchForm(Application->Owner, AIE, NeedKonkr,tmp);
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         __tmp = AIE->GetFirstChild();
         tmp = "";
         while (__tmp->GetNext())
          {
            tmp += " "+__tmp->AV["name"];
            __tmp = __tmp->GetNext();
          }
         strcpy(AText,tmp.Trim().c_str());
       }
      iRC = true;
    }
   strcpy(AXMLIE,AIE->AsXML.c_str());
   return iRC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLExtEditValue(TTagNode *AIE, char* ATab, char* ARet)
{
   TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   strcpy(ATab,(FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":").c_str());
   TTagNode *_tmp = AIE->GetChildByAV("text","name","sql");
   if (_tmp) strcpy(ARet,_tmp->AV["value"].c_str());
   else      strcpy(ARet,"");
}
//---------------------------------------------------------------------------
#endif
#ifdef _AKDO
_DLLEXPB_ __stdcall DefDDocSpec(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool RC = false;
//   if (NeedKonkr)
//    {
   TStringList *FValues = NULL;
   try
    {
       FValues = new TStringList;
       FValues->AddObject("����������",(TObject*)1);
       FValues->AddObject("���������������",(TObject*)2);
       FValues->AddObject("���������",(TObject*)3);
       FValues->AddObject("����������",(TObject*)4);
       FValues->AddObject("���������",(TObject*)5);
       FValues->AddObject("����������������",(TObject*)6);
       FValues->AddObject("�������",(TObject*)7);
       FValues->AddObject("���",(TObject*)8);
       FValues->AddObject("��������",(TObject*)9);
       FValues->AddObject("������������",(TObject*)10);
       FValues->AddObject("��������",(TObject*)11);
       FValues->AddObject("�������",(TObject*)12);
       FValues->AddObject("�������",(TObject*)13);
       FValues->AddObject("�����������",(TObject*)14);
       FValues->AddObject("�������",(TObject*)15);
       FValues->AddObject("�����������",(TObject*)16);
       FValues->AddObject("����������",(TObject*)17);
       FValues->AddObject("��������",(TObject*)18);
       FValues->AddObject("��������",(TObject*)19);
       FValues->AddObject("������",(TObject*)20);
       FValues->AddObject("������������",(TObject*)21);
       RC = DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
    }
   __finally
    {
      if (FValues) delete FValues;
    }
//    }
   return RC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLDDocSpec(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefDST(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool RC = false;
//   if (NeedKonkr)
//    {
   TStringList *FValues = NULL;
   try
    {
       FValues = new TStringList;
       FValues->AddObject("�� �������",(TObject*)0);
       FValues->AddObject("I",(TObject*)1);
       FValues->AddObject("II",(TObject*)2);
       FValues->AddObject("III",(TObject*)3);
       FValues->AddObject("IV",(TObject*)4);
       FValues->AddObject("V",(TObject*)5);
       RC = DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
    }
   __finally
    {
      if (FValues) delete FValues;
    }
//    }
   return RC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLDST(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefDHZ(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool RC = false;
//   if (NeedKonkr)
//    {
   TStringList *FValues = NULL;
   try
    {
       FValues = new TStringList;
       FValues->AddObject("���",(TObject*)0);
       FValues->AddObject("��",(TObject*)1);
       RC = DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
    }
   __finally
    {
      if (FValues) delete FValues;
    }
//    }
   return RC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLDHZ(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefDVFirst(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool RC = false;
//   if (NeedKonkr)
//    {
   TStringList *FValues = NULL;
   try
    {
       FValues = new TStringList;
       FValues->AddObject("���",(TObject*)0);
       FValues->AddObject("��",(TObject*)1);
       RC = DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
    }
   __finally
    {
      if (FValues) delete FValues;
    }
//    }
   return RC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLDVFirst(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefDDiagT(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool RC = false;
//   if (NeedKonkr)
//    {
   TStringList *FValues = NULL;
   try
    {
       FValues = new TStringList;
       FValues->AddObject("���",(TObject*)0);
       FValues->AddObject("��",(TObject*)1);
       RC = DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
    }
   __finally
    {
      if (FValues) delete FValues;
    }
//    }
   return RC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLDDiagT(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
_DLLEXPB_ __stdcall DefDDispU(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE)
{
   if (!AutoFunc_DM) return false;
   bool RC = false;
//   if (NeedKonkr)
//    {
   TStringList *FValues = NULL;
   try
    {
       FValues = new TStringList;
       FValues->AddObject("���",(TObject*)0);
       FValues->AddObject("������� �����",(TObject*)1);
       FValues->AddObject("���� �������",(TObject*)2);
       RC = DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
    }
   __finally
    {
      if (FValues) delete FValues;
    }
//    }
   return RC;
}
//---------------------------------------------------------------------------
_DLLEXP_ __stdcall SQLDDispU(TTagNode *AIE, char* ATab, char* ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
#endif
//---------------------------------------------------------------------------