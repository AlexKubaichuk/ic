//---------------------------------------------------------------------------

#ifndef RegConkretizH
#define RegConkretizH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "XMLContainer.h"
#include "ICSTemplate.h"
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
#include "Dmf_Auto.h"
//---------------------------------------------------------------------------
class TRegConForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *CompPanel;
        TPanel *BtnPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
        bool FIsSubSel,IsAll;
        TTagNode *FTempXML;
        TTagNode *FRootNode;
        void __fastcall FOnGetOrgList(bool ATemplate, long AGroup, TStringList *ADest);
        bool __fastcall FOnTreeBtnClick(TTagNode *ItTag, bool &AIsGroup, UnicodeString &ACodeBeg, UnicodeString &ACodeEnd, UnicodeString &AExtCode, UnicodeString &AText, bool isClicked);
        bool __fastcall FExtBtnClick(TTagNode *ItTag, UnicodeString &Src,  TDataSource *ASource, bool isClicked, TObject *ACtrlList, int ABtnIdx);
        bool __fastcall FExtGetWhere(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource, bool isClicked, TObject *ACtrlList, int ABtnIdx);
        bool  __fastcall GetFl(TTagNode *itTag, UnicodeString &UIDs);
        bool __fastcall FSetValues(TTagNode *itTag, UnicodeString &TagNum);
        UnicodeString __fastcall TRegConForm::SetValIfNotNull(UnicodeString AVal, UnicodeString AId,bool AAnd, bool AQuote = true);
        TTagNode *TmplNode;
        TICSTemplate *FTempl;
        TAutoFunc_DM   *FDM;
public:		// User declarations
        TStringList *ValuesList;
        TTagNode *Result;
        __fastcall TRegConForm(TComponent *Owner, TAutoFunc_DM *ADM,UnicodeString ATemplXML, TTagNode *ParamRoot, UnicodeString ARootGUI, bool AisAll, bool isNeedDef, bool AIsSubSel = false);
};
//---------------------------------------------------------------------------
extern PACKAGE TRegConForm *RegConForm;
//---------------------------------------------------------------------------
#endif
