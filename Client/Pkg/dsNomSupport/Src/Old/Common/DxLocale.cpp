//---------------------------------------------------------------------------


#pragma hdrstop

#include "DxLocale.h"

#include "cxEditConsts.hpp"
#include "cxExtEditConsts.hpp"
#include "cxFilterConsts.hpp"
#include "cxGridPopupMenuConsts.hpp"
#include "cxPCConsts.hpp"
#include "dxDockConsts.hpp"
#include "dxThemeConsts.hpp"
#include "dxBarStrs.hpp"
#include "cxGridStrs.hpp"
//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
void __fastcall DxLocale()
{
//#include "cxEditConsts.hpp"
   cxSetResourceString(&_cxSBlobButtonCancel,"&������");
   cxSetResourceString(&_cxSBlobButtonClose,"&�������");
//   cxSetResourceString(&_cxSBlobButtonOK,"&OK");
//   cxSetResourceString(&_cxSBlobMemo,"(MEMO)");
//   cxSetResourceString(&_cxSBlobMemoEmpty,"(memo)");
//   cxSetResourceString(&_cxSBlobPicture,"(PICTURE)");
//   cxSetResourceString(&_cxSBlobPictureEmpty,"(picture)");
//   cxSetResourceString(&_cxSDateBOM,"bom");
//   cxSetResourceString(&_cxSDateEOM,"eom");
   cxSetResourceString(&_cxSDateError,"������������ ����");
//   cxSetResourceString(&_cxSDateFifth,"fifth");
//   cxSetResourceString(&_cxSDateFirst,"first");
//   cxSetResourceString(&_cxSDateFourth,"fourth");
//   cxSetResourceString(&_cxSDateFriday,"Friday");
//   cxSetResourceString(&_cxSDateMonday,"Monday");
//   cxSetResourceString(&_cxSDateNow,"now");
   cxSetResourceString(&_cxSDatePopupClear,"��������");
   cxSetResourceString(&_cxSDatePopupToday,"�������");
//   cxSetResourceString(&_cxSDateSaturday,"Saturday");
//   cxSetResourceString(&_cxSDateSecond,"second");
//   cxSetResourceString(&_cxSDateSeventh,"seventh");
//   cxSetResourceString(&_cxSDateSixth,"sixth");
//   cxSetResourceString(&_cxSDateSunday,"Sunday");
//   cxSetResourceString(&_cxSDateThird,"third");
//   cxSetResourceString(&_cxSDateThursday,"Thursday");
//   cxSetResourceString(&_cxSDateToday,"today");
//   cxSetResourceString(&_cxSDateTomorrow,"tomorrow");
//   cxSetResourceString(&_cxSDateTuesday,"Tuesday");
//   cxSetResourceString(&_cxSDateWednesday,"Wednesday");
//   cxSetResourceString(&_cxSDateYesterday,"yesterday");

//   cxSetResourceString(&_cxSEditCheckBoxChecked,"True");
//   cxSetResourceString(&_cxSEditCheckBoxGrayed,"");
//   cxSetResourceString(&_cxSEditCheckBoxUnchecked,"False");
//   cxSetResourceString(&_cxSEditInvalidRepositoryItem,"The repository item is not acceptable");
//   cxSetResourceString(&_cxSEditNumericValueConvertError,"Could not convert to numeric value");
//   cxSetResourceString(&_cxSEditPopupCircularReferencingError,"Circular referencing is not allowed");
//   cxSetResourceString(&_cxSEditTimeConvertError,"Could not convert to time");
//   cxSetResourceString(&_cxSEditValidateErrorText,"Invalid input value. Use escape key to abandon changes");
//   cxSetResourceString(&_cxSEditValueOutOfBounds,"Value out of bounds");
   cxSetResourceString(&_cxSMenuItemCaptionCopy,"&����������");
   cxSetResourceString(&_cxSMenuItemCaptionCut,"&��������");
   cxSetResourceString(&_cxSMenuItemCaptionDelete,"&�������");
   cxSetResourceString(&_cxSMenuItemCaptionLoad,"&��������...");
   cxSetResourceString(&_cxSMenuItemCaptionPaste,"���&�����");
   cxSetResourceString(&_cxSMenuItemCaptionSave,"��������� &���...");
//   cxSetResourceString(&_cxSRadioGroupDefaultCaption,"");
//   cxSetResourceString(&_scxMaskEditEmptyMaskCollectionFile,"The mask collection file is empty");
//   cxSetResourceString(&_scxMaskEditIllegalFileFormat,"Illegal file format");
//   cxSetResourceString(&_scxMaskEditInvalidEditValue,"The edit value is invalid");
//   cxSetResourceString(&_scxMaskEditMaskCollectionFiles,"Mask collection files");
//   cxSetResourceString(&_scxMaskEditNoMask,"None");
//   cxSetResourceString(&_scxMaskEditRegExprError,"Regular expression errors:");
//   cxSetResourceString(&_scxRegExprCantCreateEmptyAlt,"The alternative should not be empty");
//   cxSetResourceString(&_scxRegExprCantCreateEmptyBlock,"The block should not be empty");
//   cxSetResourceString(&_scxRegExprCantCreateEmptyEnum,"Can't create empty enumeration");
//   cxSetResourceString(&_scxRegExprCantUsePlusQuantifier,"The '+' quantifier cannot be applied here");
//   cxSetResourceString(&_scxRegExprCantUseStarQuantifier,"The '*' quantifier cannot be applied here");
//   cxSetResourceString(&_scxRegExprChar,"Char");
//   cxSetResourceString(&_scxRegExprEmptySourceStream,"The source stream is empty");
//   cxSetResourceString(&_scxRegExprHexNumberExpected,"Hexadecimal number expected but '%s' found");
//   cxSetResourceString(&_scxRegExprHexNumberExpected0,"Hexadecimal number expected");
//   cxSetResourceString(&_scxRegExprIllegalQuantifier,"Illegal quantifier '%s'");
//   cxSetResourceString(&_scxRegExprIllegalSymbol,"Illegal '%s'");
//   cxSetResourceString(&_scxRegExprIncorrectSpace,"The space character is not allowed after '\\'");
//   cxSetResourceString(&_scxRegExprLine,"Line");
//   cxSetResourceString(&_scxRegExprMissing,"Missing '%s'");
//   cxSetResourceString(&_scxRegExprNotSupportQuantifier,"The parameter quantifiers are not supported");
//   cxSetResourceString(&_scxRegExprSubrangeOrder,"The starting character of the subrange must be less than the finishing one");
//   cxSetResourceString(&_scxRegExprTooBigReferenceNumber,"Too big reference number");
//   cxSetResourceString(&_scxRegExprUnnecessary,"Unnecessary '%s'");
//   cxSetResourceString(&_scxSHyperLinkPrefix,"http://");
//   cxSetResourceString(&_scxSCalcError,"Error");

//#include "cxFilterConsts.hpp"
//   cxSetResourceString(&_cxSFilterAddCondition,"�������� &�������");
//   cxSetResourceString(&_cxSFilterAddGroup,"�������� &Group");
//   cxSetResourceString(&_cxSFilterBoolOperatorAnd,"AND");
//   cxSetResourceString(&_cxSFilterBoolOperatorNotAnd,"NOT AND");
//   cxSetResourceString(&_cxSFilterBoolOperatorNotOr,"NOT OR");
//   cxSetResourceString(&_cxSFilterBoolOperatorOr,"OR");
//   cxSetResourceString(&_cxSFilterClearAll,"Clear &All");
//   cxSetResourceString(&_cxSFilterControlDialogActionApplyCaption,"&���������");//   cxSetResourceString(&_cxSFilterControlDialogActionCancelCaption,"������");//   cxSetResourceString(&_cxSFilterControlDialogActionOkCaption,"Ok");//   cxSetResourceString(&_cxSFilterControlDialogActionOpenCaption,"&�������...");//   cxSetResourceString(&_cxSFilterControlDialogActionSaveCaption,"&��������� ���...");//   cxSetResourceString(&_cxSFilterControlDialogCaption,"Filter builder");
//   cxSetResourceString(&_cxSFilterControlDialogFileExt,"flt");
//   cxSetResourceString(&_cxSFilterControlDialogFileFilter,"Filters (*.flt)|*.flt'");
//   cxSetResourceString(&_cxSFilterControlDialogNewFile,"�����.flt");
//   cxSetResourceString(&_cxSFilterControlDialogOpenDialogCaption,"Open an existing filter");//   cxSetResourceString(&_cxSFilterControlDialogSaveDialogCaption,"Save the active filter to file");//   cxSetResourceString(&_cxSFilterControlNullString,"<empty>");
//   cxSetResourceString(&_cxSFilterDialogCaption,"Custom Filter");
//   cxSetResourceString(&_cxSFilterDialogCharactersSeries,"to represent any series of characters");
//   cxSetResourceString(&_cxSFilterDialogInvalidValue,"Invalid value");
//   cxSetResourceString(&_cxSFilterDialogOperationAnd,"AND");
//   cxSetResourceString(&_cxSFilterDialogOperationOr,"OR");
//   cxSetResourceString(&_cxSFilterDialogRows,"Show rows where:");
//   cxSetResourceString(&_cxSFilterDialogSingleCharacter,"to represent any single character");
//   cxSetResourceString(&_cxSFilterDialogUse,"Use");
//   cxSetResourceString(&_cxSFilterErrorBuilding,"Can''t build filter from source");
//   cxSetResourceString(&_cxSFilterFooterAddCondition,"press button to add new condition");
//   cxSetResourceString(&_cxSFilterGroupCaption,"applies to the following conditions");
//   cxSetResourceString(&_cxSFilterRemoveRow,"&Remove Row");
//   cxSetResourceString(&_cxSFilterRootButtonCaption,"Filter");
//   cxSetResourceString(&_cxSFilterRootGroupCaption,"<root>");

  cxSetResourceString(&_dxSBAR_LOOKUPDIALOGCAPTION,"Select value");
  cxSetResourceString(&_dxSBAR_LOOKUPDIALOGOK,"OK");
  cxSetResourceString(&_dxSBAR_LOOKUPDIALOGCANCEL,"Cancel");

  cxSetResourceString(&_dxSBAR_DIALOGOK,"OK");
  cxSetResourceString(&_dxSBAR_DIALOGCANCEL,"Cancel");
  cxSetResourceString(&_dxSBAR_COLOR_STR_0,"Black");
  cxSetResourceString(&_dxSBAR_COLOR_STR_1,"Maroon");
  cxSetResourceString(&_dxSBAR_COLOR_STR_2,"Green");
  cxSetResourceString(&_dxSBAR_COLOR_STR_3,"Olive");
  cxSetResourceString(&_dxSBAR_COLOR_STR_4,"Navy");
  cxSetResourceString(&_dxSBAR_COLOR_STR_5,"Purple");
  cxSetResourceString(&_dxSBAR_COLOR_STR_6,"Teal");
  cxSetResourceString(&_dxSBAR_COLOR_STR_7,"Gray");
  cxSetResourceString(&_dxSBAR_COLOR_STR_8,"Silver");
  cxSetResourceString(&_dxSBAR_COLOR_STR_9,"Red");
  cxSetResourceString(&_dxSBAR_COLOR_STR_10,"Lime");
  cxSetResourceString(&_dxSBAR_COLOR_STR_11,"Yellow");
  cxSetResourceString(&_dxSBAR_COLOR_STR_12,"Blue");
  cxSetResourceString(&_dxSBAR_COLOR_STR_13,"Fuchsia");
  cxSetResourceString(&_dxSBAR_COLOR_STR_14,"Aqua");
  cxSetResourceString(&_dxSBAR_COLOR_STR_15,"White");
  cxSetResourceString(&_dxSBAR_COLORAUTOTEXT,"(automatic)");
  cxSetResourceString(&_dxSBAR_COLORCUSTOMTEXT,"(custom)");
  cxSetResourceString(&_dxSBAR_DATETODAY,"Today");
  cxSetResourceString(&_dxSBAR_DATECLEAR,"Clear");
  cxSetResourceString(&_dxSBAR_DATEDIALOGCAPTION,"Select the date");
  cxSetResourceString(&_dxSBAR_TREEVIEWDIALOGCAPTION,"Select item");
  cxSetResourceString(&_dxSBAR_IMAGEDIALOGCAPTION,"Select item");
  cxSetResourceString(&_dxSBAR_IMAGEINDEX,"Image Index");
  cxSetResourceString(&_dxSBAR_IMAGETEXT,"Text");
  cxSetResourceString(&_dxSBAR_PLACEFORCONTROL,"The place for the ");
  cxSetResourceString(&_dxSBAR_CANTASSIGNCONTROL,"You cannot assign the same control to more than one TdxBarControlContainerItem.");

  cxSetResourceString(&_dxSBAR_WANTTORESETTOOLBAR,"Are you sure you want to reset the changes made to the ''%s'' toolbar?");
  cxSetResourceString(&_dxSBAR_WANTTORESETUSAGEDATA,"This will delete the record of the commands you''ve used in this application and restore the default set of visible commands to the menus and toolbars. It will not undo any explicit customizations.   Are you sure you want to proceed?");
  cxSetResourceString(&_dxSBAR_BARMANAGERMORETHENONE ,"A Form should contain only a single TdxBarManager");
  cxSetResourceString(&_dxSBAR_BARMANAGERBADOWNER,"TdxBarManager should have as its Owner - TForm (TCustomForm)");
  cxSetResourceString(&_dxSBAR_NOBARMANAGERS,"There are no TdxBarManagers available");
  cxSetResourceString(&_dxSBAR_WANTTODELETETOOLBAR,"Are you sure you want to delete the ''%s'' toolbar?");
  cxSetResourceString(&_dxSBAR_WANTTODELETECATEGORY,"Are you sure you want to delete the ''%s'' category?");
  cxSetResourceString(&_dxSBAR_WANTTOCLEARCOMMANDS,"Are you sure you want to delete all commands in the ''%s'' category?");
  cxSetResourceString(&_dxSBAR_RECURSIVESUBITEMS,"You cannot create recursive subitems");
  cxSetResourceString(&_dxSBAR_COMMANDNAMECANNOTBEBLANK,"A command name cannot be blank. Please enter a name.");
  cxSetResourceString(&_dxSBAR_TOOLBAREXISTS,"A toolbar named ''%s'' already exists. Type another name.");
  cxSetResourceString(&_dxSBAR_RECURSIVEGROUPS,"You cannot create recursive groups");

  cxSetResourceString(&_dxSBAR_DEFAULTCATEGORYNAME,"Default");

  cxSetResourceString(&_dxSBAR_CP_ADDSUBITEM,"Add &SubItem");
  cxSetResourceString(&_dxSBAR_CP_ADDBUTTON,"Add &Button");
  cxSetResourceString(&_dxSBAR_CP_ADDITEM,"Add &Item");
  cxSetResourceString(&_dxSBAR_CP_DELETEBAR,"Delete Bar");

  cxSetResourceString(&_dxSBAR_CP_RESET,"&Reset");
  cxSetResourceString(&_dxSBAR_CP_DELETE,"&Delete");
  cxSetResourceString(&_dxSBAR_CP_NAME,"&Name:");
  cxSetResourceString(&_dxSBAR_CP_CAPTION,"&Caption:");
  cxSetResourceString(&_dxSBAR_CP_DEFAULTSTYLE,"Defa&ult style");
  cxSetResourceString(&_dxSBAR_CP_TEXTONLYALWAYS,"&Text Only (Always)");
  cxSetResourceString(&_dxSBAR_CP_TEXTONLYINMENUS,"Text &Only (in Menus)");
  cxSetResourceString(&_dxSBAR_CP_IMAGEANDTEXT,"Image &and Text");
  cxSetResourceString(&_dxSBAR_CP_BEGINAGROUP,"Begin a &Group");
  cxSetResourceString(&_dxSBAR_CP_VISIBLE,"&Visible");
  cxSetResourceString(&_dxSBAR_CP_MOSTRECENTLYUSED,"&Most recently used");

  cxSetResourceString(&_dxSBAR_ADDEX,"Add...");
  cxSetResourceString(&_dxSBAR_RENAMEEX,"Rename...");
  cxSetResourceString(&_dxSBAR_DELETE,"Delete");
  cxSetResourceString(&_dxSBAR_CLEAR,"Clear");
  cxSetResourceString(&_dxSBAR_VISIBLE,"Visible");
  cxSetResourceString(&_dxSBAR_OK,"OK");
  cxSetResourceString(&_dxSBAR_CANCEL,"Cancel");
  cxSetResourceString(&_dxSBAR_SUBMENUEDITOR,"SubMenu Editor...");
  cxSetResourceString(&_dxSBAR_SUBMENUEDITORCAPTION,"ExpressBars SubMenu Editor");
  cxSetResourceString(&_dxSBAR_INSERTEX,"Insert...");

  cxSetResourceString(&_dxSBAR_MOVEUP,"Move Up");
  cxSetResourceString(&_dxSBAR_MOVEDOWN,"Move Down");
  cxSetResourceString(&_dxSBAR_POPUPMENUEDITOR,"PopupMenu Editor...");
  cxSetResourceString(&_dxSBAR_TABSHEET1," ������ &������������ ");
  cxSetResourceString(&_dxSBAR_TABSHEET2," &������� ");
  cxSetResourceString(&_dxSBAR_TABSHEET3," �&�������� ");
  cxSetResourceString(&_dxSBAR_TOOLBARS,"������ &������������:");
  cxSetResourceString(&_dxSBAR_TNEW,"&�����...");
  cxSetResourceString(&_dxSBAR_TRENAME,"�������������...");
  cxSetResourceString(&_dxSBAR_TDELETE,"&�������");
  cxSetResourceString(&_dxSBAR_TRESET,"&�����...");
  cxSetResourceString(&_dxSBAR_CLOSE,"�������");
  cxSetResourceString(&_dxSBAR_CAPTION,"���������");
  cxSetResourceString(&_dxSBAR_CATEGORIES,"���������:");
  cxSetResourceString(&_dxSBAR_COMMANDS,"�������:");
  cxSetResourceString(&_dxSBAR_DESCRIPTION,"��������  ");

  cxSetResourceString(&_dxSBAR_CUSTOMIZE,"&���������...");
  cxSetResourceString(&_dxSBAR_ADDREMOVEBUTTONS,"&�������� ��� ������� ������");
  cxSetResourceString(&_dxSBAR_MOREBUTTONS,"More Buttons");
  cxSetResourceString(&_dxSBAR_RESETTOOLBAR,"�&����");
  cxSetResourceString(&_dxSBAR_EXPAND,"���������� (Ctrl-Down)");
  cxSetResourceString(&_dxSBAR_DRAGTOMAKEMENUFLOAT,"Drag to make this menu float");

  cxSetResourceString(&_dxSBAR_TOOLBARNEWNAME ,"Custom ");
  cxSetResourceString(&_dxSBAR_CATEGORYADD ,"Add Category");
  cxSetResourceString(&_dxSBAR_CATEGORYINSERT ,"Insert Category");
  cxSetResourceString(&_dxSBAR_CATEGORYRENAME ,"Rename Category");
  cxSetResourceString(&_dxSBAR_TOOLBARADD ,"Add Toolbar");
  cxSetResourceString(&_dxSBAR_TOOLBARRENAME ,"Rename Toolbar");
  cxSetResourceString(&_dxSBAR_CATEGORYNAME ,"&Category name:");
  cxSetResourceString(&_dxSBAR_TOOLBARNAME ,"&Toolbar name:");
  cxSetResourceString(&_dxSBAR_CUSTOMIZINGFORM,"Customization Form...");

  cxSetResourceString(&_dxSBAR_MODIFY,"... modify");
  cxSetResourceString(&_dxSBAR_PERSMENUSANDTOOLBARS,"Personalized Menus and Toolbars  ");
  cxSetResourceString(&_dxSBAR_MENUSSHOWRECENTITEMS,"Me&nus show recently used commands first");
  cxSetResourceString(&_dxSBAR_SHOWFULLMENUSAFTERDELAY,"Show f&ull menus after a short delay");
  cxSetResourceString(&_dxSBAR_RESETUSAGEDATA,"&Reset my usage data");

  cxSetResourceString(&_dxSBAR_OTHEROPTIONS,"Other  ");
  cxSetResourceString(&_dxSBAR_LARGEICONS,"&Large icons");
  cxSetResourceString(&_dxSBAR_HINTOPT1,"Show Tool&Tips on toolbars");
  cxSetResourceString(&_dxSBAR_HINTOPT2,"Show s&hortcut keys in ToolTips");
  cxSetResourceString(&_dxSBAR_MENUANIMATIONS,"&Menu animations:");
  cxSetResourceString(&_dxSBAR_MENUANIM1,"(None)");
  cxSetResourceString(&_dxSBAR_MENUANIM2,"Random");
  cxSetResourceString(&_dxSBAR_MENUANIM3,"Unfold");
  cxSetResourceString(&_dxSBAR_MENUANIM4,"Slide");
  cxSetResourceString(&_dxSBAR_MENUANIM5,"Fade");
  cxSetResourceString(&_scxGridNoDataInfoText,"");

}
//---------------------------------------------------------------------------
