//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <math.h>
#include "Dmf_Auto.h"
#include "RegConkretiz.h"
#include "InqCon.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TAutoFunc_DM *AutoFunc_DM;
//---------------------------------------------------------------------------
__fastcall TAutoFunc_DM::TAutoFunc_DM(TComponent* Owner)
    : TDataModule(Owner)
{
  TreeFormList = new TStringList;
  MKBDlg = NULL;
  MKBClRoot = new TTagNode(NULL);
  MKBClRoot->AV["codefield"] = "CODE";
  MKBClRoot->AV["extcodefield"] = "EXTCODE";
  MKBClRoot->AV["levelfield"] = "LEV";
  MKBClRoot->AV["namefield"] = "NAME";
  MKBClRoot->AV["tblname"] = "MKB";
  MKBClRoot->AV["name"] = "���-10";
}
//---------------------------------------------------------------------------
void __fastcall TAutoFunc_DM::DMDestroy(TObject *Sender)
{
  if (TreeFormList)
   {
      for (int i = 0; i < TreeFormList->Count; i++)
       {
         delete ((TNomTreeClassForm*)TreeFormList->Objects[i]);
       }
      delete TreeFormList;
   }
  if (MKBDlg) delete MKBDlg;
  if (MKBClRoot) delete MKBClRoot;
  XMLList = NULL;
  if (AUTOFUNC_DB->Connected) AUTOFUNC_DB->Close();
  Sleep(500);
}
//---------------------------------------------------------------------------
void __fastcall TAutoFunc_DM::qtExec(UnicodeString ASQL, bool aCommit)
{
   quFreeA->Close();
   quFreeA->CommandText = ASQL;
   quFreeA->Open();
}
//---------------------------------------------------------------------------
void __fastcall TAutoFunc_DM::DataModuleCreate(TObject *Sender)
{
   XMLList = NULL;
}
//---------------------------------------------------------------------------
void __fastcall GetTreeValues(TTagNode *ANode, TStringList *AList, int *Count)
{
  TTagNode *itNode = ANode->GetFirstChild();
  if (ANode->CmpName("treegroup"))
   AList->AddObject(ANode->GetAV("name"),(TObject*)(*Count));
  else
   {
    if (ANode->GetParent()->CmpName("treegroup"))
     AList->AddObject(ANode->GetParent()->GetAV("name")+":"+ANode->GetAV("name"),(TObject*)(*Count));
    else
     AList->AddObject(ANode->GetAV("name"),(TObject*)(*Count));
   }
  (*Count)++;
  while (itNode)
   {
     GetTreeValues(itNode, AList,Count);
     itNode = itNode->GetNext();
   }
}
//---------------------------------------------------------------------------
int __fastcall GetScaleStrs(TTagNode *ANode, TStringList *AList)
{
   TTagNode *Scale = ANode->GetTagByUID(ANode->GetScaleUID());
   if (Scale->CmpName("scale_binary"))
    {
      AList->AddObject("�������",(TObject*)1);
      AList->AddObject("�� �������",(TObject*)0);
    }
   else if (Scale->CmpName("scale_select"))
    {
      Scale = Scale->GetFirstChild();
      while (Scale)
       {
         AList->AddObject(Scale->GetAV("name"),(TObject*)Scale->GetAV("value").ToInt());
         Scale = Scale->GetNext();
       }
    }
   else if (Scale->CmpName("scale_digit"))
    {
      if (Scale->AV["decimal"].ToIntDef(0) == 0)
       {
         AList->AddObject("min",(TObject*)Scale->GetAV("min").ToInt());
         AList->AddObject("max",(TObject*)Scale->GetAV("max").ToInt());
       }
      else
       {
         AList->AddObject("min",(TObject*)((int)(StrToFloat(Scale->GetAV("min"))*(10*Scale->AV["decimal"].ToIntDef(0)))));
         AList->AddObject("max",(TObject*)((int)(StrToFloat(Scale->GetAV("max"))*(10*Scale->AV["decimal"].ToIntDef(0)))));
         AList->AddObject("scale",(TObject*)Scale->AV["decimal"].ToIntDef(0));
       }
      return _scDigit;
    }
   else if (Scale->CmpName("scale_diapazon"))
    {
      Scale = Scale->GetFirstChild();
      while (Scale)
       {
         if (Scale->GetAV("value") == "<<")
          AList->AddObject(Scale->GetAV("name"),(TObject*)Scale->GetParent()->GetAV("min").ToInt());
         else if (Scale->GetAV("value") == ">>")
          AList->AddObject(Scale->GetAV("name"),(TObject*)Scale->GetParent()->GetAV("max").ToInt());
         else
          AList->AddObject(Scale->GetAV("name"),(TObject*)Scale->GetAV("value").ToInt());
         Scale = Scale->GetNext();
       }
      return _scDiapazon;
    }
   else if (Scale->CmpName("scale_tree"))
    {
      Scale = Scale->GetFirstChild();
      int COu = 0;
      if (Scale->CmpName("tree_int"))
       GetTreeValues(Scale,AList,&COu);
      else
       {
         AutoFunc_DM->qtExec("Select * From "+Scale->GetAV("tblname"));
         while (!AutoFunc_DM->quFreeA->Eof)
          {
            AList->AddObject((quFNStr(Scale->GetAV("extcodefield"))+": "+quFNStr(Scale->GetAV("namefield"))).c_str(),(TObject*)quFNInt(Scale->GetAV("codefield")));
            AutoFunc_DM->quFreeA->Next();
          }
       }
    }
   else
    {
      MessageBox(NULL,"������������� ��� �����","���������",MB_ICONINFORMATION);
      return _scError;
    }
   return _scChoice;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetXMLProjectCode(TTagNode* ANode)
{
   return (ANode->GetRoot()->GetChildByName("passport")->GetAV("projectcode").UpperCase());
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetXMLGUI(TTagNode* ANode)
{
   return (ANode->GetRoot()->GetChildByName("passport")->GetAV("GUI").UpperCase());
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetScaleText(TTagNode *ANode, int AVal)
{
   TTagNode *Scale = ANode->GetTagByUID(ANode->GetScaleUID());
   if (Scale->CmpName("scale_binary,scale_select"))
    {
      return GetSelScaleStr(ANode, AVal);
    }
   else if (Scale->CmpName("scale_diapazon"))
    {
      return GetDiapazonStr(ANode, AVal);
    }
   else if (Scale->CmpName("scale_tree"))
    {
      return GetTreeScaleStr(ANode, AVal);
    }
   else
    {
      MessageBox(NULL,"������������� ��� �����","���������",MB_ICONINFORMATION);
      return "";
    }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDiapazonStr(TTagNode *AOut, int AVal)
{
   TTagNode *Scale = AOut->GetTagByUID(AOut->GetScaleUID());
   int xMin,xMax;
   UnicodeString xVal;
   xMin = Scale->GetAV("min").ToInt();
   xMax = Scale->GetAV("max").ToInt();
   Scale = Scale->GetFirstChild();
   while (Scale)
    {
      xVal = Scale->GetAV("value");
      if (xVal == "<<")
       {
         if (AVal < xMin) return Scale->GetAV("name");
       }
      else if (xVal == ">>")
       {
         if (AVal > xMax) return Scale->GetAV("name");
       }
      else
       {
         if (AVal <= xVal.ToInt()) return Scale->GetAV("name");
       }
      Scale = Scale->GetNext();
    }
   return "";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetSelScaleStr(TTagNode *ANode, int AVal)
{
   TTagNode *itScale = ANode->GetTagByUID(ANode->GetScaleUID());
   if (itScale->CmpName("scale_binary"))
    {
      if (AVal > 0) return "��";
    }
   else
    {
      itScale = itScale->GetFirstChild();
      while (itScale)
       {
         if (itScale->GetAV("value").ToInt() == AVal)
          {
            return itScale->GetAV("name");
          }
         itScale = itScale->GetNext();
       }
    }
   return "";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetTreeScaleStr(TTagNode *ANode, int AVal)
{
   TTagNode *itScale = ANode->GetTagByUID(ANode->GetScaleUID())->GetFirstChild();
   int iC = 0;
   if (itScale->CmpName("tree_int"))
    return GetTreeScaleStrVal(itScale,&iC,AVal);
   else
    {
       AutoFunc_DM->qtExec("Select * From "+itScale->GetAV("tblname")+" Where "+itScale->GetAV("codefield")+"="+IntToStr(AVal));
       if (AutoFunc_DM->quFreeA->RecordCount)
        return AutoFunc_DM->quFreeA->_FN(itScale->GetAV("extcodefield"))->AsString+": "+AutoFunc_DM->quFreeA->_FN(itScale->GetAV("namefield"))->AsString;
       else
        return "";
    }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetTreeScaleStrVal(TTagNode *itNode, int* ICount, int AVal)
{
   TTagNode *itScale = itNode->GetFirstChild();
   UnicodeString RC = "";
   while (itScale)
    {
      if (*ICount == AVal)
       return itScale->GetAV("name");
      *ICount = *ICount+1;
      if (itScale->Count)
       {
         RC = GetTreeScaleStrVal(itScale,ICount,AVal);
         if (RC != "") return RC;
       }
      itScale = itScale->GetNext();
    }
   return "";
}
//---------------------------------------------------------------------------
bool __fastcall DefRegValue(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE, UnicodeString ATemplate, UnicodeString AGUI, bool AisAll, bool AIsSubSel)
{
   bool iRC = false;
   UnicodeString tmp;
   if (NeedKonkr)
    {
      UnicodeString FGUI;
      if (AGUI == "")
       {
         TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->GetAV("ref"));
         TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->GetAV("param"));
         TTagNode *FExtAttr = AutoFunc_DM->XMLList->GetRefer(FISAttr->GetAV("ref"));
         FGUI = FExtAttr->GetRoot()->GetChildByName("passport")->GetAV("gui");
       }
      else
       FGUI = AGUI;
      TRegConForm *Dlg = NULL;
      try
       {
         Dlg = new TRegConForm(AutoFunc_DM, ATemplate, AIE, FGUI, AisAll, true, AIsSubSel);
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         TTagNode *__tmp = AIE->GetFirstChild();
         tmp = "";
         while (__tmp)
          {
            if (!__tmp->CmpAV("name","sql"))
             tmp += " "+__tmp->GetAV("name");
            __tmp = __tmp->GetNext();
          }
         strcpy(AText,tmp.Trim().c_str());
       }
      iRC = true;
    }
   strcpy(AXMLIE,AIE->AsXML.c_str());
   return iRC;
}
//---------------------------------------------------------------------------
void __fastcall TAutoFunc_DM::CreateTreeFormList()
{
   // ������������ ������ ���������������
   Reg = XMLList->GetXML("40381E23-92155860-4448");
   // ������������ ������ ���������� ���������������
   tmpChTree = new TList;
   Reg->Iterate(GetTreeCh,UnicodeString(""));
   TTagNode *xN, *itNode;
   for (int i = 0; i < tmpChTree->Count; i++)
    {
      xN = (TTagNode*)tmpChTree->Items[i];
      itNode = Reg->GetTagByUID(xN->GetAV("ref"));
      TreeFormList->AddObject(xN->GetAV("uid"), (TObject*)new TNomTreeClassForm(this,itNode,xN->GetAV("name"),xN->GetAV("wherestr")));
    }
   delete tmpChTree; tmpChTree = NULL;
}
//---------------------------------------------------------------------------
bool __fastcall TAutoFunc_DM::GetTreeCh(void *uiTag, UnicodeString &UID)
{
   if (((TTagNode*)uiTag)->CmpName("choiceTree"))
    tmpChTree->Add(uiTag);
   return false;
}
//---------------------------------------------------------------------------
bool __fastcall DefChVal(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE, TStringList *AValues)
{
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = AutoFunc_DM->XMLList->GetRefer(AIE->AV["ref"]);
      TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      TConForm *ConDlg = NULL;
      try
       {
         ConDlg = new TConForm(Application->Owner);
         ConDlg->MsgLab->Caption = FISAttr->AV["name"];
         ConDlg->ValueCB->Properties->Items->Assign(AValues);
         ConDlg->SetType(_scChoice,AIE);
         ConDlg->ShowModal();
       }
      __finally
       {
         if (ConDlg) delete ConDlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         __tmp = AIE->GetFirstChild();
         tmp = "";
         while (__tmp->GetNext())
          {
            tmp += " "+__tmp->AV["name"];
            __tmp = __tmp->GetNext();
          }
         strcpy(AText,tmp.Trim().c_str());
       }
      iRC = true;
    }
   strcpy(AXMLIE,AIE->AsXML.c_str());
   return iRC;
}
//---------------------------------------------------------------------------

