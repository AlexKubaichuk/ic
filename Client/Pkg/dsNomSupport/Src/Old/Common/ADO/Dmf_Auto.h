//---------------------------------------------------------------------------
#ifndef Dmf_AutoH
#define Dmf_AutoH
#include <Classes.hpp>
#include "AxeUtil.h"
#include <ADODB.hpp>
#include <DB.hpp>
#include "NomTreeClass.h"

#define _scError    0
#define _scDigit    1
#define _scChoice   2
#define _scDiapazon 3
#define _scDate     4
#define _scRDate    5

#include "pgmsetting.h"

#ifdef _ADO_
 #define _CDATEF_ TDateTime::CurrentDate().FormatString("yyyy-mm-dd")
 #define _FN FieldByName
 #define _FS FormatString("yyyy-mm-dd")
 #define _IncPeriod  UnicodeString("dbo.IncPeriod")
#else
 #define _CDATEF_ TDateTime::CurrentDate().FormatString("dd.mm.yyyy")
 #define _FN FN
 #define _FS FormatString("dd.mm.yyyy")
 #define _IncPeriod  UnicodeString("Inc_Period")
#endif

#define quFNStr(a) AutoFunc_DM->quFreeA->FieldByName(a)->AsString
#define quFNInt(a) AutoFunc_DM->quFreeA->FieldByName(a)->AsInteger
#define quFNFloat(a) AutoFunc_DM->quFreeA->FieldByName(a)->AsFloat
#define quFNDate(a) AutoFunc_DM->quFreeA->FieldByName(a)->AsDateTime
#define AsTextDate AsDateTime.FormatString("dd.mm.yyyy")
//---------------------------------------------------------------------------
class TAutoFunc_DM : public TDataModule
{
__published:	// IDE-managed Components
        TADOConnection *AUTOFUNC_DB;
        TADODataSet *quFreeA;
        void __fastcall DMDestroy(TObject *Sender);
        void __fastcall DataModuleCreate(TObject *Sender);

private:	// User declarations
       bool __fastcall GetTreeCh(void *uiTag, UnicodeString &UID);
public:		// User declarations
    TAnsiStrMap  MKB;
    TAnsiStrMap  LPUList;
    // ᯨ᮪ ���樭
    TAnsiStrMap  VacList;
    TAnsiStrMap  VacListCode;
    // ᯨ᮪ �஡
    TAnsiStrMap  ProbList;
    TAnsiStrMap  ProbListCode;
    // ᯨ᮪ ��䥪権
    TAnsiStrMap  InfList;
    TAnsiStrMap  InfListCode;

    TAxeXMLContainer *XMLList;
    TNomTreeClassForm *MKBDlg;
    TList *tmpChTree;
    TTagNode *Reg;
    TTagNode *MKBClRoot;
    TStringList *TreeFormList;
    void __fastcall qtExec(UnicodeString ASQL, bool aCommit = false);
    void __fastcall CreateTreeFormList();
    __fastcall TAutoFunc_DM(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern int  __fastcall GetScaleStrs(TTagNode *ANode, TStringList *AList);
extern void __fastcall GetTreeValues(TTagNode *ANode, TStringList *AList, int *Count);
extern UnicodeString __fastcall GetXMLProjectCode(TTagNode* ANode);
extern UnicodeString __fastcall GetXMLGUI(TTagNode* ANode);
extern UnicodeString __fastcall GetScaleText(TTagNode *ANode, int AVal);
extern UnicodeString __fastcall GetDiapazonStr(TTagNode *AOut, int AVal);
extern UnicodeString __fastcall GetSelScaleStr(TTagNode *ANode, int AVal);
extern UnicodeString __fastcall GetTreeScaleStr(TTagNode *ANode, int AVal);
extern UnicodeString __fastcall GetTreeScaleStrVal(TTagNode *itNode, int* ICount, int AVal);
extern bool __fastcall DefRegValue(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE, UnicodeString ATemplate, UnicodeString AGUI,bool AisAll, bool AIsSubSel);
extern bool __fastcall DefChVal(TTagNode *AIE, char *AText, bool NeedKonkr, char* AXMLIE, TStringList *AValues);

extern PACKAGE TAutoFunc_DM *AutoFunc_DM;
//---------------------------------------------------------------------------
#endif

