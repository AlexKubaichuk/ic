//---------------------------------------------------------------------------

#ifndef SchConkretizH
#define SchConkretizH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "Dmf_Auto.h"
#include "ICSTemplate.h"
#include "ToolEdit.hpp"
#include <Mask.hpp>
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TSchForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *CompPanel;
        TComboEdit *VakCBE;
        TComboEdit *ProbCBE;
        TRadioButton *VakRB;
        TRadioButton *ProbRB;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall VakRBClick(TObject *Sender);
        void __fastcall VakCBEButtonClick(TObject *Sender);
        void __fastcall ProbCBEButtonClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
private:	// User declarations
        bool __fastcall CheckInput();
        UnicodeString FInfCode,FVakSch,FProbSch;
        TTagNode *TmplNode;
        TICSTemplate *FTempl;
        TAutoFunc_DM *FDM;
public:		// User declarations
        TTagNode *Result;
        __fastcall TSchForm(TComponent* Owner,TTagNode *AIE, bool isNeedDef, UnicodeString AInfCode, TAutoFunc_DM *ADM);
};
//---------------------------------------------------------------------------
extern PACKAGE TSchForm *SchForm;
//---------------------------------------------------------------------------
#endif
