//---------------------------------------------------------------------------
#include <vcl\vcl.h>
#pragma hdrstop

#include "ShemaList.h"
//---------------------------------------------------------------------------
#define quFNStr(a) FDM->quFreeA->FN(a)->AsString
#define quFNInt(a) FDM->quFreeA->FN(a)->AsInteger
#define quFNFloat(a) FDM->quFreeA->FN(a)->AsFloat
#define quFNDate(a) FDM->quFreeA->FN(a)->AsDateTime
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxLookAndFeelPainters"
#pragma resource "*.dfm"
TSchemaListForm *SchemaListForm;
//---------------------------------------------------------------------------
__fastcall TSchemaListForm::TSchemaListForm(TComponent* Owner,long InfCode, bool isVac, UnicodeString AVal,TAutoFunc_DM *ADM)
	: TForm(Owner)
{
  FDM = ADM;
  UnicodeString xName,xTab,xCode;
  TTagNode *SchList;
  if (isVac)
   {
     xName = "R003B";
     xTab = "CLASS_002D";
     xCode = "R003E";
     SchList = FDM->XMLList->GetXML("12063611-00008cd7-cd89");
   }
  else
   {
     xName = "R007A";
     xTab = "CLASS_0079";
     xCode = "R007B";
     SchList = FDM->XMLList->GetXML("001D3500-00005882-DC28");
   }
  SchemaLB->Clear();
  TStringList *VacList = new TStringList;
  TStringList *ProbList = new TStringList;
  xValues = new TStringList;

  FDM->qtExec("Select CODE, R0040 From CLASS_0035 ");
  while (!FDM->quFreeA->Eof)
   {
     VacList->AddObject(quFNStr("R0040"),(TObject*)quFNInt("CODE"));
     FDM->quFreeA->Next();
   }
  FDM->qtExec("Select CODE, R002C From CLASS_002A ");
  while (!FDM->quFreeA->Eof)
   {
     ProbList->AddObject(quFNStr("R002C"),(TObject*)quFNInt("CODE"));
     FDM->quFreeA->Next();
   }
  TStringList *DDD = new TStringList;
  FDM->qtExec("Select "+xName+" From "+xTab+" Where "+xCode+"="+IntToStr(InfCode));
  while (!FDM->quFreeA->Eof)
   {
     DDD->Add(quFNStr(xName.c_str()).c_str());
     FDM->quFreeA->Next();
   }
  FDM->trFreeA->Commit();
  SchList = SchList->GetChildByName("schemalist")->GetFirstChild();
  int ind = 0;
  SchemaLB->Items->Add("�������� ��������");
  SchemaLB->Items->Add("�� ����������");
  xValues->Add("");
  xValues->Add("_NULL_");
  while (SchList)
   {
     if (DDD->IndexOf(SchList->AV["ref"]) != -1)
      {
        if (isVac)
         SchemaLB->Items->Add((VacList->Strings[VacList->IndexOfObject((TObject*)SchList->AV["ref"].ToInt())]+" (������� '"+SchList->AV["name"]+"')").c_str());
        else
         SchemaLB->Items->Add((ProbList->Strings[ProbList->IndexOfObject((TObject*)SchList->AV["ref"].ToInt())]+" (������� '"+SchList->AV["name"]+"')").c_str());
        xValues->Add((SchList->AV["uid"]+"."+SchList->GetFirstChild()->AV["uid"]).c_str());
        if ((SchList->AV["uid"]+"."+SchList->GetFirstChild()->AV["uid"]) == AVal) ind = SchemaLB->Items->Count-1;
      }
     SchList = SchList->GetNext();
   }
  delete DDD;
  if (AVal == "_NULL_") SchemaLB->ItemIndex = 1;
  else                  SchemaLB->ItemIndex = ind;
  delete VacList;
  delete ProbList;
}
//---------------------------------------------------------------------------
void __fastcall TSchemaListForm::FormDestroy(TObject *Sender)
{
  delete xValues;
}
//---------------------------------------------------------------------------
void __fastcall TSchemaListForm::OkBtnClick(TObject *Sender)
{
  if (SchemaLB->ItemIndex >= 0)
   {
     Value = SchemaLB->Items->Strings[SchemaLB->ItemIndex]+"="+xValues->Strings[SchemaLB->ItemIndex];
     ModalResult = mrOk;
   }
}
//---------------------------------------------------------------------------

