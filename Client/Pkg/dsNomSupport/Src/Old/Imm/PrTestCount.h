//---------------------------------------------------------------------------

#ifndef PrTestCountH
#define PrTestCountH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "cxCheckListBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <ExtCtrls.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TCountForm : public TForm
{
__published:	// IDE-managed Components
        TcxCheckListBox *cxCheckListBox1;
        TPanel *BtnPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TLabel *Label1;
        TPanel *Panel1;
        TcxMaskEdit *cxMaskEdit1;
        TcxMaskEdit *cxMaskEdit2;
        TLabel *Label2;
        TLabel *Label3;
private:	// User declarations
public:		// User declarations
        __fastcall TCountForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TCountForm *CountForm;
//---------------------------------------------------------------------------
#endif
