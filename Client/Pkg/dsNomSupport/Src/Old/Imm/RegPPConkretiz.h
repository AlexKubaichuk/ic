//---------------------------------------------------------------------------

#ifndef RegPPConkretizH
#define RegPPConkretizH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "Dmf_Auto.h"
#include "RegED.h"
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxGroupBox.hpp"
#include "cxMaskEdit.hpp"
#include "cxRadioGroup.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
class TRegPPConForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *Panel1;
        TLabel *Label1;
        TLabel *Label3;
        TLabel *ApplyLab;
        TLabel *VacTypeSepLab;
        TLabel *VacLab;
        TLabel *ProbLab;
        TBevel *Bevel1;
        TBevel *Bevel2;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TcxComboBox *PlanMonthCB;
        TcxRadioButton *PrivRB;
        TcxRadioGroup *VacTypeRG;
        TcxMaskEdit *VacType1ED;
        TcxComboBox *InfCB;
        TcxComboBox *VacCB;
        TcxComboBox *ProbCB;
        TcxComboBox *ApplyCB;
        TcxRadioButton *ProbRB;
        TcxMaskEdit *VacType2ED;
        TLabel *Label2;
        TcxComboBox *PlanTypeCB;
        TCheckBox *NotApplyCBChB;
        TCheckBox *NotPlanTypeCBChB;
        TBevel *Bevel3;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall VacTypeRGClick(TObject *Sender);
        void __fastcall ProbRBClick(TObject *Sender);
        void __fastcall PrivRBClick(TObject *Sender);
        void __fastcall InfCBChange(TObject *Sender);
private:	// User declarations
        bool  __fastcall GetFl(TTagNode *itTag, UnicodeString &UIDs);
        void __fastcall GetDepClCode(UnicodeString ADepClName, UnicodeString ADepFlVal, UnicodeString ARetFlName, UnicodeString &ARetVals);
        TTagNode *RegNode;
        TAutoFunc_DM *FDM;
public:		// User declarations
        TStringList *ValuesList;
        TTagNode *Result;
        __fastcall TRegPPConForm(TComponent *Owner,UnicodeString ATemplXML, TTagNode *ParamRoot, bool isReg, bool isNeedDef, TAutoFunc_DM *ADM);
};
//---------------------------------------------------------------------------
extern PACKAGE TRegPPConForm *RegPPConForm;
//---------------------------------------------------------------------------
#endif
