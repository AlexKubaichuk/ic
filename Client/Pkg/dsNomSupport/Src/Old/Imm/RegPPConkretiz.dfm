object RegPPConForm: TRegPPConForm
  Left = 490
  Top = 307
  HelpContext = 4012
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = #1054#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1103' ...'
  ClientHeight = 324
  ClientWidth = 325
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 284
    Width = 325
    Height = 40
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      325
      40)
    object OkBtn: TcxButton
      Left = 158
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      OnClick = OkBtnClick
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
    end
    object CancelBtn: TcxButton
      Left = 239
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 325
    Height = 284
    Align = alClient
    TabOrder = 0
    object Bevel1: TBevel
      Left = -20
      Top = 66
      Width = 352
      Height = 79
      Shape = bsFrame
    end
    object Bevel2: TBevel
      Left = -24
      Top = 143
      Width = 367
      Height = 44
      Shape = bsFrame
    end
    object Label1: TLabel
      Left = 19
      Top = 37
      Width = 64
      Height = 13
      Caption = #1042#1072#1082#1094#1080#1085#1072#1094#1080#1103':'
    end
    object Label3: TLabel
      Left = 19
      Top = 11
      Width = 36
      Height = 13
      Caption = #1052#1077#1089#1103#1094':'
    end
    object ApplyLab: TLabel
      Left = 5
      Top = 189
      Width = 96
      Height = 13
      Caption = #1060#1072#1082#1090' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103':'
    end
    object VacTypeSepLab: TLabel
      Left = 265
      Top = 107
      Width = 6
      Height = 16
      Caption = '/'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object VacLab: TLabel
      Left = 39
      Top = 79
      Width = 35
      Height = 13
      Caption = #1052#1048#1041#1055':'
    end
    object ProbLab: TLabel
      Left = 37
      Top = 162
      Width = 35
      Height = 13
      Caption = #1055#1088#1086#1073#1072':'
    end
    object Label2: TLabel
      Left = 5
      Top = 237
      Width = 84
      Height = 13
      Caption = #1048#1089#1090#1086#1095#1085#1080#1082' '#1087#1083#1072#1085#1072':'
    end
    object Bevel3: TBevel
      Left = 0
      Top = 230
      Width = 333
      Height = 9
      Shape = bsTopLine
    end
    object PlanMonthCB: TcxComboBox
      Left = 170
      Top = 7
      Properties.DropDownListStyle = lsEditFixedList
      Properties.Items.Strings = (
        ''
        #1058#1077#1082#1091#1097#1080#1081
        #1055#1088#1077#1076#1099#1076#1091#1097#1080#1081)
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 0
      Width = 144
    end
    object PrivRB: TcxRadioButton
      Left = 17
      Top = 59
      Width = 113
      Height = 17
      Caption = #1040#1085#1072#1083#1080#1079' '#1087#1088#1080#1074#1080#1074#1082#1080
      Checked = True
      TabOrder = 1
      TabStop = True
      OnClick = PrivRBClick
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
    end
    object VacTypeRG: TcxRadioGroup
      Left = 19
      Top = 94
      Caption = #1042#1080#1076' '#1074#1072#1082#1094#1080#1085#1072#1094#1080#1080
      ParentFont = False
      Properties.Columns = 4
      Properties.Items = <
        item
          Caption = #1053#1077#1090
        end
        item
          Caption = 'V'
        end
        item
          Caption = 'RV'
        end
        item
          Caption = #1044#1086#1087'.'
        end>
      ItemIndex = 0
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clBlack
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 2
      OnClick = VacTypeRGClick
      Height = 34
      Width = 192
    end
    object VacType1ED: TcxMaskEdit
      Left = 226
      Top = 105
      Enabled = False
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d{0,3}'
      Properties.MaxLength = 0
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 3
      Width = 35
    end
    object InfCB: TcxComboBox
      Left = 170
      Top = 33
      Properties.DropDownListStyle = lsEditFixedList
      Properties.OnChange = InfCBChange
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 4
      Width = 144
    end
    object VacCB: TcxComboBox
      Left = 170
      Top = 75
      Properties.DropDownListStyle = lsEditFixedList
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 5
      Width = 144
    end
    object ProbCB: TcxComboBox
      Left = 170
      Top = 158
      Enabled = False
      Properties.DropDownListStyle = lsEditFixedList
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 6
      Width = 144
    end
    object ApplyCB: TcxComboBox
      Left = 46
      Top = 204
      Properties.DropDownListStyle = lsEditFixedList
      Properties.Items.Strings = (
        ''
        #1074#1099#1087#1086#1083#1085#1077#1085#1080#1077
        #1074#1099#1087#1086#1083#1085#1077#1085#1080#1077' '#1087#1086#1083#1080#1082#1083#1080#1085#1080#1082#1086#1081
        #1074#1099#1087#1086#1083#1085#1077#1085#1080#1077' '#1087#1086' '#1087#1083#1072#1085#1091
        #1074#1099#1087#1086#1083#1085#1077#1085#1080#1077' '#1085#1077' '#1087#1086' '#1087#1083#1072#1085#1091' ('#1086#1090#1084#1077#1090#1082#1072' '#1076#1088#1091#1075#1080#1084' '#1052#1048#1041#1055')'
        #1074#1099#1087#1086#1083#1085#1077#1085#1080#1077' '#1074' '#1095#1091#1078#1086#1084' '#1091#1095#1088#1077#1078#1076#1077#1085#1080#1080
        #1074#1099#1087#1086#1083#1085#1077#1085#1080#1077' '#1085#1072' '#1076#1088#1091#1075#1086#1081' '#1090#1077#1088#1088#1080#1090#1086#1088#1080#1080)
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 7
      Width = 273
    end
    object ProbRB: TcxRadioButton
      Left = 19
      Top = 136
      Width = 98
      Height = 17
      Caption = #1040#1085#1072#1083#1080#1079' '#1087#1088#1086#1073#1099
      TabOrder = 8
      OnClick = ProbRBClick
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
    end
    object VacType2ED: TcxMaskEdit
      Left = 274
      Top = 105
      Enabled = False
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d{0,3}'
      Properties.MaxLength = 0
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 9
      Width = 35
    end
    object PlanTypeCB: TcxComboBox
      Left = 46
      Top = 253
      Properties.DropDownListStyle = lsEditFixedList
      Properties.Items.Strings = (
        ''
        #1055#1083#1072#1085#1086#1074#1072#1103
        #1056#1091#1095#1085#1086#1077' '#1076#1086#1073#1072#1074#1083#1077#1085#1080#1077
        #1055#1083#1072#1085#1080#1088#1086#1074#1097#1080#1082' '#1087#1086' '#1082#1072#1083#1077#1085#1076#1072#1088#1102
        #1055#1083#1072#1085#1080#1088#1086#1074#1097#1080#1082' '#1085#1077' '#1087#1086' '#1082#1072#1083#1077#1085#1076#1072#1088#1102
        #1055#1083#1072#1085#1080#1088#1086#1074#1097#1080#1082' '#1082#1072#1082' '#1087#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1072#1103
        #1055#1083#1072#1085#1080#1088#1086#1074#1097#1080#1082' '#1087#1086' '#1090#1091#1088#1091
        #1055#1083#1072#1085#1080#1088#1086#1074#1097#1080#1082' '#1087#1086' '#1090#1091#1088#1091' '#1082#1072#1082' '#1087#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1072#1103)
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 10
      Width = 273
    end
    object NotApplyCBChB: TCheckBox
      Left = 5
      Top = 206
      Width = 40
      Height = 17
      Caption = #1053#1045
      TabOrder = 11
    end
    object NotPlanTypeCBChB: TCheckBox
      Left = 5
      Top = 254
      Width = 38
      Height = 17
      Caption = #1053#1045
      TabOrder = 12
    end
  end
end
