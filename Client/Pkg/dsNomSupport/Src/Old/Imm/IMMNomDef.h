//---------------------------------------------------------------------------

#ifndef IMMNomDefH
#define IMMNomDefH
//---------------------------------------------------------------------------
#include <vcl.h>
#include "BaseNomDef.h"
//---------------------------------------------------------------------------
class TICSIMMNomDef : public TICSBaseNomDef
{
private:
    int __fastcall CorrectDay( Word Year, Word Month, Word Day );
    bool __fastcall CalcPlanPeriod(TDate *ADateBP, TDate *ADateEP, TDate *APlanMonth);
public:
    TGetAttrFunc __fastcall GetAttrMethodAddr(UnicodeString AFuncName);
    TGetDefFunc __fastcall GetDefMethodAddr(UnicodeString AFuncName);
    TGetSQLFunc __fastcall GetSQLMethodAddr(UnicodeString AFuncName);
__published:
    void __fastcall AMKBDiag(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall APlanVac(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AAddrValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AFullAddrValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);    // 6.8+
    void __fastcall AOrgValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall AFullOrgValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);     // 6.8+
    void __fastcall AFullDopOrgValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);  // 6.8+
    void __fastcall APatPrivAge(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);
    void __fastcall ASchName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef);

    bool __fastcall DefDatePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefOtvodDatePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefInqTreeValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefPlanPrivReak(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefPlanProbReak(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefPlanPres(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefExtEditValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);

    void __fastcall SQLDatePeriodLast(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    void __fastcall SQLOtvodDatePeriodLast(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    void __fastcall SQLInqTreeValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    void __fastcall SQLPlanPrivReak(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    void __fastcall SQLPlanProbReak(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    void __fastcall SQLPlanPres(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
    void __fastcall SQLExtEditValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);

    __fastcall TICSIMMNomDef(TAutoFunc_DM *ADM): TICSBaseNomDef(ADM){};
    virtual __fastcall ~TICSIMMNomDef(){};
};
#endif
