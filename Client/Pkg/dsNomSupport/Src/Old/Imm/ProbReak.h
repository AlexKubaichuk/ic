//---------------------------------------------------------------------------

#ifndef ProbReakH
#define ProbReakH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "Dmf_Auto.h"
#include "ICSTemplate.h"
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TProbReakForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *CompPanel;
        TComboBox *ReakTypeCB;
        TLabel *ReakTypeLab;
        TEdit *FromED;
        TEdit *ToED;
        TLabel *FromLab;
        TLabel *ToLab;
        TCheckBox *AnalizChB;
        TBevel *Bevel1;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TLabel *ToLab2;
        TEdit *ToED2;
        void __fastcall AnalizChBClick(TObject *Sender);
        void __fastcall ReakTypeCBChange(TObject *Sender);
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
private:	// User declarations
        bool __fastcall CheckInput();
        TTagNode* TmplNode;
        TICSTemplate *FTempl;
        TAutoFunc_DM *FDM;
public:		// User declarations
        TTagNode *Result;
        __fastcall TProbReakForm(TComponent* Owner,TTagNode *AIE, bool isNeedDef, UnicodeString AProbCode,TAutoFunc_DM *ADM);
};
//---------------------------------------------------------------------------
extern PACKAGE TProbReakForm *ProbReakForm;
//---------------------------------------------------------------------------
#endif
