//---------------------------------------------------------------------------
#ifndef ShemaListH
#define ShemaListH
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <StdCtrls.hpp>
//---------------------------------------------------------------------------
#include "Dmf_Auto.h"
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TSchemaListForm : public TForm
{
__published:	// IDE-managed Components
    TPanel *Panel1;
    TListBox *SchemaLB;
    TcxButton *OkBtn;
    TcxButton *CancelBtn;
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall OkBtnClick(TObject *Sender);
private:	// User declarations
    TAutoFunc_DM *FDM;
    TStringList *xValues;
public:		// User declarations
    UnicodeString Value;
    __fastcall TSchemaListForm(TComponent* Owner,long InfCode, bool isVac, UnicodeString AVal,TAutoFunc_DM *ADM);
};
//---------------------------------------------------------------------------
extern PACKAGE TSchemaListForm *SchemaListForm;
//---------------------------------------------------------------------------
#endif
