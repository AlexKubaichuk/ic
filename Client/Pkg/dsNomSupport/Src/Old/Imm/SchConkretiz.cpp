//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "SchConkretiz.h"
#include "ShemaList.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ToolEdit"
#pragma link "cxButtons"
#pragma link "cxLookAndFeelPainters"
#pragma resource "*.dfm"
TSchForm *SchForm;
//---------------------------------------------------------------------------
__fastcall TSchForm::TSchForm(TComponent* Owner,TTagNode *AIE, bool isNeedDef, UnicodeString AInfCode,TAutoFunc_DM *ADM)
        : TForm(Owner)
{
   FDM = ADM;
   FVakSch = "";
   FProbSch = "";
   Result = AIE;
   FInfCode = AInfCode.Trim();
   UnicodeString tmp = "<root>"
                    "<fl ref='1077' def='"+AInfCode+"' en='0'/>"
                    "</root>";
   TmplNode = new TTagNode(NULL); /* TODO -okab -c:) : ����� ���� */
   TmplNode->Assign(FDM->XMLList->GetXML("4031DA5E-8AC52FB3-7D03"),true);
   FTempl = new TICSTemplate(tmp, CompPanel, FDM->quFreeA, "","", TmplNode, clInfoBk);
   FTempl->UpperFunc = FDM->UpperFunc;
}
//---------------------------------------------------------------------------
void __fastcall TSchForm::OkBtnClick(TObject *Sender)
{
  if (!CheckInput()) return;
  TTagNode *__tmp;
  TStringList *FFlList = new TStringList;
  FFlList->Text =  "1077";
  Result->DeleteChild();
  for (int i = 0; i < FFlList->Count; i++)
   {
     if (FTempl->GetValue(FFlList->Strings[i]) != "")
      {
        __tmp = Result->AddChild("text");
        __tmp->AddAttr("name",FTempl->GetName(FFlList->Strings[i]));
        __tmp->AddAttr("value",FTempl->GetValue(FFlList->Strings[i]));
      }
   }
  delete FFlList;
  if (VakRB->Checked)
   {
     TTagNode *tmp =  Result->AddChild("text");
     tmp->AV["name"] = "����� ��� �������="+VakCBE->Text;
     tmp->AV["value"] = FVakSch;
   }
  else
   {
     TTagNode *tmp =  Result->AddChild("text");
     tmp->AV["name"] = "����� ��� �����="+ProbCBE->Text;
     tmp->AV["value"] = FProbSch;
   }
  if (!FTempl->IsEmpty())
   {
     __tmp = Result->AddChild("text","name=sql");
     UnicodeString FTabAlias = "";
     TTagNode *NomTag = FDM->XMLList->GetRefer(Result->AV["ref"]);
     if (NomTag)
      {
        NomTag = NomTag->GetTagByUID(NomTag->AV["param"]);
        if (NomTag)
         {
           NomTag = NomTag->GetParent("is");
           if (NomTag)
            FTabAlias = NomTag->AV["tabalias"];
         }
      }
     UnicodeString tmpSch = "";
     if (VakRB->Checked)
      {
        if (FVakSch.Length())
         {
           if (FVakSch == "_NULL_")
            tmpSch = FTabAlias+".R1078 = '"+FVakSch+"'";
           else
            tmpSch = FDM->UpperFunc+"("+FTabAlias+".R1078) LIKE '"+_GUI(FVakSch).UpperCase()+".%'";
         }
      }
     else
      {
        if (FProbSch.Length())
         {
           if (FProbSch == "_NULL_")
            tmpSch = FTabAlias+".R1079 = '"+FProbSch+"'";
           else
            tmpSch = FDM->UpperFunc+"("+FTabAlias+".R1079) LIKE '"+_GUI(FProbSch).UpperCase()+".%'";
         }
      }
     if (tmpSch.Length())
      __tmp->AddAttr("value",FTempl->GetWhere(FTabAlias)+" and "+tmpSch);
     else
      __tmp->AddAttr("value",FTempl->GetWhere(FTabAlias));
   }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
bool __fastcall TSchForm::CheckInput()
{
  if (!FTempl->CheckInput()) return false;
  if (VakRB->Checked)
   {
     if (VakCBE->Text == "")
      {
        MessageBox(NULL,"�������� ����� ��� ������� ������ ���� ���������� !!!","������ �����",MB_ICONHAND);
        ActiveControl = VakCBE;
        return false;
      }
   }
  else
   {
     if (ProbCBE->Text == "")
      {
        MessageBox(NULL,"�������� ����� ��� ����� ������ ���� ���������� !!!","������ �����",MB_ICONHAND);
        ActiveControl = ProbCBE;
        return false;
      }
   }
  return true;
}
//---------------------------------------------------------------------------
void __fastcall TSchForm::VakRBClick(TObject *Sender)
{
  if (VakRB->Checked)
   {
     VakCBE->Color = clInfoBk;
     VakCBE->Enabled = true;
     ProbCBE->Color = clBtnFace;
     ProbCBE->Enabled = false;
     ProbCBE->Text = "";
     FVakSch = "";
     FProbSch = "";
   }
  else
   {
     VakCBE->Color = clBtnFace;
     VakCBE->Enabled = false;
     VakCBE->Text = "";
     ProbCBE->Color = clInfoBk;
     ProbCBE->Enabled = true;
     FVakSch = "";
     FProbSch = "";
   }
}
//---------------------------------------------------------------------------
void __fastcall TSchForm::VakCBEButtonClick(TObject *Sender)
{
  TSchemaListForm *Dlg = NULL;
  try
   {
     Dlg = new TSchemaListForm(this,(long)(FInfCode.ToIntDef(0)), true, "",FDM);
     Dlg->ShowModal();
     if (Dlg->ModalResult == mrOk)
      {
        FProbSch = "";
        if (GetPart2(Dlg->Value,'=') != "")
         {
           VakCBE->Text = GetPart1(Dlg->Value,'=');
           FVakSch = GetPart2(Dlg->Value,'=');
         }
        else
         {
           VakCBE->Text = "";
           FVakSch = "";
         }
      }
   }
  __finally
   {
     if (Dlg) delete Dlg;
   }
}
//---------------------------------------------------------------------------
void __fastcall TSchForm::ProbCBEButtonClick(TObject *Sender)
{
  TSchemaListForm *Dlg = NULL;
  try
   {
     Dlg = new TSchemaListForm(this,(long)FInfCode.ToInt(), false, "",FDM);
     Dlg->ShowModal();
     if (Dlg->ModalResult == mrOk)
      {
        FVakSch = "";
        if (GetPart2(Dlg->Value,'=') != "")
         {
           ProbCBE->Text = GetPart1(Dlg->Value,'=');
           FProbSch = GetPart2(Dlg->Value,'=');
         }
        else
         {
           ProbCBE->Text = "";
           FProbSch = "";
         }
      }
   }
  __finally
   {
     if (Dlg) delete Dlg;
   }
}
//---------------------------------------------------------------------------
void __fastcall TSchForm::FormDestroy(TObject *Sender)
{
  delete FTempl;
  delete TmplNode;
}
//---------------------------------------------------------------------------

