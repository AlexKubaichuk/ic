//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "RegPPConkretiz.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxGroupBox"
#pragma link "cxMaskEdit"
#pragma link "cxRadioGroup"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
#define __CLCODE(a)  (long)((TStringList*)a->Properties->Items->Objects[a->ItemIndex])->Objects[0]
//#define __CLCODE(a)  (long)(a->Properties->Items->Objects[a->ItemIndex])
#define __CLIndexCODE(a,b)  (long)((TStringList*)a->Properties->Items->Objects[b])->Objects[0]
//#define __CLIndexCODE(a,b)  (long)(a->Properties->Items->Objects[b])
#define __CLSTR(a)   a->Properties->Items->Strings[a->ItemIndex]
TRegPPConForm *RegPPConForm;
//---------------------------------------------------------------------------
__fastcall TRegPPConForm::TRegPPConForm(TComponent *Owner,UnicodeString ATemplXML, TTagNode *ParamRoot, bool isReg, bool isNeedDef,TAutoFunc_DM *ADM)
        : TForm(Owner)
{
//  IsReg = isReg;
  FDM = ADM;
  Result = ParamRoot;
  RegNode = FDM->XMLList->GetXML("40381E23-92155860-4448");
  FInvadeClassCB(InfCB->Properties->Items,  0, true ,RegNode->GetTagByUID("003A"), true, (TDataSet*)FDM->quFreeA, "", "", RegNode);
  FInvadeClassCB(VacCB->Properties->Items,  0, true ,RegNode->GetTagByUID("0035"), true, (TDataSet*)FDM->quFreeA, "", "", RegNode);
  FInvadeClassCB(ProbCB->Properties->Items, 0, true ,RegNode->GetTagByUID("002A"), true, (TDataSet*)FDM->quFreeA, "", "", RegNode);
  TTagNode *sText;
  sText = Result->GetChildByAV("text","ref","0");
  if (sText) PlanMonthCB->ItemIndex = sText->AV["value"].ToInt()+1;

  sText = Result->GetChildByAV("text","ref","1");
  if (sText)
   {
     for (int i = 1; i < InfCB->Properties->Items->Count; i++)
      {
        if (__CLIndexCODE(InfCB,i) == sText->AV["value"].ToInt())
         {
           InfCB->ItemIndex = i;
           InfCBChange(InfCB); break;
         }
      }
   }

  sText = Result->GetChildByAV("text","ref","2");
  if (sText)
   {
     if ((bool)sText->AV["value"].ToInt()) PrivRB->Checked = true;
     else                                     ProbRB->Checked = true;
   }

  sText = Result->GetChildByAV("text","ref","3");
  if (sText)
   {
     for (int i = 1; i < VacCB->Properties->Items->Count; i++)
      {
        if (__CLIndexCODE(VacCB,i) == sText->AV["value"].ToInt())
         {
           VacCB->ItemIndex = i; break;
         }
      }
   }

  sText = Result->GetChildByAV("text","ref","4");
  if (sText)
   {
     VacTypeRG->ItemIndex = 1; // V
     VacType1ED->Text = sText->AV["value"].SubString(2,sText->AV["value"].Length()-1);
   }
  sText = Result->GetChildByAV("text","ref","5");
  if (sText)
   {
     VacTypeRG->ItemIndex = 2; // RV
     UnicodeString xVal = sText->AV["value"];
     int xDel = xVal.LastDelimiter("/");
     if (xDel)
      {
        VacType1ED->Text = xVal.SubString(3,xDel-3);
        VacType2ED->Text = xVal.SubString(xDel+1,xVal.Length()-xDel);
      }
     else
      VacType1ED->Text = sText->AV["value"].SubString(3,sText->AV["value"].Length()-2);
   }
  sText = Result->GetChildByAV("text","ref","6");
  if (sText)
   {
     VacTypeRG->ItemIndex = 3; // ���.
   }

  sText = Result->GetChildByAV("text","ref","7");
  if (sText)
   {
     for (int i = 1; i < ProbCB->Properties->Items->Count; i++)
      {
        if (__CLIndexCODE(ProbCB,i) == sText->AV["value"].ToInt())
         {
           ProbCB->ItemIndex = i; break;
         }
      }
   }

  sText = Result->GetChildByAV("text","ref","8");
  if (sText) ApplyCB->ItemIndex = sText->AV["value"].ToIntDef(0);
  sText = Result->GetChildByAV("text","ref","9");
  if (sText) PlanTypeCB->ItemIndex = sText->AV["value"].ToIntDef(0);

  sText = Result->GetChildByAV("text","ref","10");
  if (sText) NotApplyCBChB->Checked = sText->AV["value"].ToIntDef(0);
  sText = Result->GetChildByAV("text","ref","11");
  if (sText) NotPlanTypeCBChB->Checked = sText->AV["value"].ToIntDef(0);
}
//---------------------------------------------------------------------------
void __fastcall TRegPPConForm::OkBtnClick(TObject *Sender)
{
  if (!((PlanMonthCB->ItemIndex > 0) ||
        (InfCB->ItemIndex > 0) ||
        (VacCB->ItemIndex > 0) ||
        (ProbCB->ItemIndex > 0) ||
        (VacType1ED->Text != "")||
        (VacType2ED->Text != "")
       )
     )
    {
      ShowMessage ("���������� ������ ������� 1 ��������");
      return;
    }
  TTagNode *__tmp;
  UnicodeString FSQL = "";
  Result->DeleteChild();
  if (PlanMonthCB->ItemIndex > 0)
   {
      __tmp = Result->AddChild("text");
      __tmp->AddAttr("name","����� �����="+__CLSTR(PlanMonthCB));
      __tmp->AddAttr("value",IntToStr(PlanMonthCB->ItemIndex-1));
      __tmp->AddAttr("ref","0");
      if (PlanMonthCB->ItemIndex == 1)
       FSQL += " and PLN.R1087='_CURR_'";
      else
       FSQL += " and PLN.R1087='_PREV_'";
   }
  if (PrivRB->Checked)
   { //������ ��������
      __tmp = Result->AddChild("text");
      __tmp->AddAttr("name","������ ��������");
      __tmp->AddAttr("value",IntToStr((int)PrivRB->Checked));
      __tmp->AddAttr("ref","2");
     if (VacCB->ItemIndex > 0)
      {
         __tmp = Result->AddChild("text");
         __tmp->AddAttr("name","����="+__CLSTR(VacCB));
         __tmp->AddAttr("value",__CLCODE(VacCB));
         __tmp->AddAttr("ref","3");
         FSQL += " and PLN.R1086=0 and PLN.R1089="+IntToStr(__CLCODE(VacCB));
      }
     else
      {
         if (InfCB->ItemIndex > 0)
          {
            __tmp = Result->AddChild("text");
            __tmp->AddAttr("name","���������� ��="+__CLSTR(InfCB));
            __tmp->AddAttr("value",__CLCODE(InfCB));
            __tmp->AddAttr("ref","1");
//            FSQL += " and PLN.R1086=0 and PLN.VACTYPE LIKE '%#"+IntToStr(__CLCODE(InfCB))+".%'";
            FSQL += " and PLN.R1086=0 and PLN.R1089 in (Select CL.R003B From CLASS_002D CL Where CL.R003E="+IntToStr(__CLCODE(InfCB))+") ";
          }
         else
          FSQL += " and PLN.R1086=0";
      }
     switch (VacTypeRG->ItemIndex)
      {
        case 1:
         {
            __tmp = Result->AddChild("text");
            __tmp->AddAttr("name","��� ����������=V"+VacType1ED->Text);
            __tmp->AddAttr("value","V"+VacType1ED->Text);
            __tmp->AddAttr("ref","4");
           FSQL += " and PLN.VACTYPE LIKE '%.V"+VacType1ED->Text+"#%'";
           break;
         }
        case 2:
         {
            __tmp = Result->AddChild("text");
            __tmp->AddAttr("name","��� ����������=RV"+VacType1ED->Text+"/"+VacType2ED->Text);
            __tmp->AddAttr("value","RV"+VacType1ED->Text+"/"+VacType2ED->Text);
            __tmp->AddAttr("ref","5");
           if ((VacType1ED->Text == "")&&(VacType2ED->Text == ""))
            FSQL += " and PLN.VACTYPE LIKE '%.RV%#%'";
           else if ((VacType1ED->Text != "")&&(VacType2ED->Text != ""))
            FSQL += " and PLN.VACTYPE LIKE '%.RV"+VacType1ED->Text+"/"+VacType2ED->Text+"#%'";
           else if (VacType1ED->Text != "")
            FSQL += " and PLN.VACTYPE LIKE '%.RV"+VacType1ED->Text+"/%#%'";
           else
            FSQL += " and PLN.VACTYPE LIKE '%.RV%/"+VacType2ED->Text+"#%'";
           break;
         }
        case 3:
         {
            __tmp = Result->AddChild("text");
            __tmp->AddAttr("name","��� ����������=���.");
            __tmp->AddAttr("value","D");
            __tmp->AddAttr("ref","6");
            FSQL += " and PLN.VACTYPE LIKE '%.D#%'";
           break;
         }
      }
   }
  else
   { //������ �����
      __tmp = Result->AddChild("text");
      __tmp->AddAttr("name","������ �����");
      __tmp->AddAttr("value",IntToStr((int)PrivRB->Checked));
      __tmp->AddAttr("ref","2");
     if (ProbCB->ItemIndex > 0)
      {
         __tmp = Result->AddChild("text");
         __tmp->AddAttr("name","�����="+__CLSTR(ProbCB));
         __tmp->AddAttr("value",__CLCODE(ProbCB));
         __tmp->AddAttr("ref","7");
         FSQL += " and PLN.R1086=1 and PLN.R108A="+IntToStr(__CLCODE(ProbCB));
      }
     else
      {
         if (InfCB->ItemIndex > 0)
          {
            __tmp = Result->AddChild("text");
            __tmp->AddAttr("name","���������� ��="+__CLSTR(InfCB));
            __tmp->AddAttr("value",__CLCODE(InfCB));
            __tmp->AddAttr("ref","1");
            FSQL += " and PLN.R1086=1 and PLN.VACTYPE LIKE '%#"+IntToStr(__CLCODE(InfCB))+".%'";
          }
         else
          FSQL += " and PLN.R1086=1";
      }
   }
  if (ApplyCB->ItemIndex > 0)
   {
      switch (ApplyCB->ItemIndex)
       {
         case 1:
          { //����������
            if (NotApplyCBChB->Checked)
            FSQL += " and PLN.F_RESULT = 0";
            else
            FSQL += " and PLN.F_RESULT <> 0";
            break;
          }
         case 2:
          { //���.������������
            if (NotApplyCBChB->Checked)
            FSQL += " and PLN.F_RESULT not in (3,4)";
            else
            FSQL += " and PLN.F_RESULT in (3,4)";
            break;
          }
         case 3:
          { //���.�� �����
            if (NotApplyCBChB->Checked)
            FSQL += " and PLN.F_RESULT <> 3";
            else
            FSQL += " and PLN.F_RESULT = 3";
            break;
          }
         case 4:
          { //���. �� �� �����
            if (NotApplyCBChB->Checked)
            FSQL += " and PLN.F_RESULT <> 4";
            else
            FSQL += " and PLN.F_RESULT = 4";
            break;
          }
         case 5:
          { //���.� ����� ���.
            if (NotApplyCBChB->Checked)
            FSQL += " and PLN.F_RESULT <> 2";
            else
            FSQL += " and PLN.F_RESULT = 2";
            break;
          }
         case 6:
          { //���.�� ��.����.
            if (NotApplyCBChB->Checked)
            FSQL += " and PLN.F_RESULT <> 1";
            else
            FSQL += " and PLN.F_RESULT = 1";
            break;
          }
       }
      __tmp = Result->AddChild("text");
      if (NotApplyCBChB->Checked)
        __tmp->AddAttr("name","���� ����������=�� "+__CLSTR(ApplyCB));
      else
        __tmp->AddAttr("name","���� ����������="+__CLSTR(ApplyCB));
      __tmp->AddAttr("value",IntToStr(ApplyCB->ItemIndex));
      __tmp->AddAttr("ref","8");
   }
  if (PlanTypeCB->ItemIndex > 0)
   {
      switch (PlanTypeCB->ItemIndex)
       {
         case 1:
          { //��������
            if (NotPlanTypeCBChB->Checked)
            FSQL += " and PLN.F_PLAN = 0";
            else
            FSQL += " and PLN.F_PLAN <> 0";
            break;
          }
         case 2:
          { //������ ����������
            if (NotPlanTypeCBChB->Checked)
            FSQL += " and PLN.F_PLAN <> 1";
            else
            FSQL += " and PLN.F_PLAN = 1";
            break;
          }
         case 3:
          { //����������� �� ���������
            if (NotPlanTypeCBChB->Checked)
            FSQL += " and PLN.F_PLAN <> 2";
            else
            FSQL += " and PLN.F_PLAN = 2";
            break;
          }
         case 4:
          { //����������� �� �� ���������
            if (NotPlanTypeCBChB->Checked)
            FSQL += " and PLN.F_PLAN <> 3";
            else
            FSQL += " and PLN.F_PLAN = 3";
            break;
          }
         case 5:
          { //����������� ��� ���������������
            if (NotPlanTypeCBChB->Checked)
            FSQL += " and PLN.F_PLAN <> 4";
            else
            FSQL += " and PLN.F_PLAN = 4";
            break;
          }
         case 6:
          { //����������� �� ����
            if (NotPlanTypeCBChB->Checked)
            FSQL += " and PLN.F_PLAN <> 5";
            else
            FSQL += " and PLN.F_PLAN = 5";
            break;
          }
         case 7:
          { //����������� �� ���� ��� ���������������
            if (NotPlanTypeCBChB->Checked)
            FSQL += " and PLN.F_PLAN <> 6";
            else
            FSQL += " and PLN.F_PLAN = 6";
            break;
          }
       }
      __tmp = Result->AddChild("text");
      if (NotPlanTypeCBChB->Checked)
        __tmp->AddAttr("name","�������� �����=�� "+__CLSTR(PlanTypeCB));
      else
        __tmp->AddAttr("name","�������� �����="+__CLSTR(PlanTypeCB));
      __tmp->AddAttr("value",IntToStr(PlanTypeCB->ItemIndex));
      __tmp->AddAttr("ref","9");
   }
  if (NotApplyCBChB->Checked)
   {
     __tmp = Result->AddChild("text");
     __tmp->AddAttr("name","");
     __tmp->AddAttr("value","1");
     __tmp->AddAttr("ref","10");
   }
  if (NotPlanTypeCBChB->Checked)
   {
     __tmp = Result->AddChild("text");
     __tmp->AddAttr("name","");
     __tmp->AddAttr("value","1");
     __tmp->AddAttr("ref","11");
   }

  if (FSQL != "")
   {
      __tmp = Result->AddChild("text");
      __tmp->AddAttr("name","sql");
      __tmp->AddAttr("value",FSQL.SubString(6,FSQL.Length()-5));
   }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
bool __fastcall TRegPPConForm::GetFl(TTagNode *itTag, UnicodeString &UIDs)
{
  if (itTag->CmpName("fl"))
   UIDs += itTag->AV["ref"]+"\n";
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TRegPPConForm::VacTypeRGClick(TObject *Sender)
{
  VacType1ED->Enabled = false;
  VacType2ED->Enabled = false;
  if (VacTypeRG->ItemIndex == 1)
   {
     VacType1ED->Enabled = true;
   }
  else if (VacTypeRG->ItemIndex == 2)
   {
     VacType1ED->Enabled = true;
     VacType2ED->Enabled = true;
   }
}
//---------------------------------------------------------------------------
void __fastcall TRegPPConForm::ProbRBClick(TObject *Sender)
{
  ProbLab->Enabled = true;
  ProbCB->Enabled = true;

  VacLab->Enabled = false;
  VacCB->Enabled = false;
  VacCB->ItemIndex = -1;
  VacTypeRG->Enabled = false;
  VacType1ED->Enabled = false;
  VacType2ED->Enabled = false;
  VacTypeSepLab->Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall TRegPPConForm::PrivRBClick(TObject *Sender)
{
  ProbLab->Enabled = false;
  ProbCB->Enabled = false;
  ProbCB->ItemIndex = -1;

  VacLab->Enabled = true;
  VacCB->Enabled = true;
  VacTypeRG->Enabled = true;
  VacType1ED->Enabled = true;
  VacType2ED->Enabled = true;
  VacTypeSepLab->Enabled = true;
  VacTypeRGClick(VacTypeRG);
}
//---------------------------------------------------------------------------
void __fastcall TRegPPConForm::InfCBChange(TObject *Sender)
{
  UnicodeString FInfCodes;
  if (InfCB->ItemIndex > 0)
   { //#define __CLCODE(a)  (long)((TStringList*)a->Properties->Items->Objects[a->ItemIndex])->Objects[0]
     long ClCode = __CLCODE(InfCB);
     FInfCodes = "";
     GetDepClCode("CLASS_002D", "R003E="+IntToStr(ClCode), "R003B", FInfCodes);
     if (FInfCodes != "-1")
      FInvadeClassCB(VacCB->Properties->Items, 0, true ,RegNode->GetTagByUID("0035"), true, (TDataSet*)FDM->quFreeA, "", FInfCodes, RegNode);
     else
      VacCB->Clear();
     FInfCodes = "";
     GetDepClCode("CLASS_0079", "R007B="+IntToStr(ClCode), "R007A", FInfCodes);
     if (FInfCodes != "-1")
      FInvadeClassCB(ProbCB->Properties->Items, 0, true ,RegNode->GetTagByUID("002A"), true, (TDataSet*)FDM->quFreeA, "", FInfCodes, RegNode);
     else
      ProbCB->Clear();
   }
  else
   {
     FInvadeClassCB(VacCB->Properties->Items, 0, true ,RegNode->GetTagByUID("0035"), true, (TDataSet*)FDM->quFreeA, "", "", RegNode);
     FInvadeClassCB(ProbCB->Properties->Items, 0, true ,RegNode->GetTagByUID("002A"), true, (TDataSet*)FDM->quFreeA, "", "", RegNode);
   }
}
//---------------------------------------------------------------------------
void __fastcall TRegPPConForm::GetDepClCode(UnicodeString ADepClName, UnicodeString ADepFlVal, UnicodeString ARetFlName, UnicodeString &ARetVals)
{
    FDM->quFreeA->Close();
    if (FDM->quFreeA->Transaction->Active)
     FDM->quFreeA->Transaction->Rollback();
    FDM->quFreeA->SQL->Clear();
    FDM->quFreeA->SQL->Add(("Select "+ARetFlName+" From "+ADepClName).c_str());
    FDM->quFreeA->SQL->Add(("Where "+ADepFlVal).c_str());
    if (!FDM->quFreeA->Transaction->Active)
     FDM->quFreeA->Transaction->StartTransaction();
    FDM->quFreeA->Prepare();
    FDM->quFreeA->ExecQuery();
    while (!FDM->quFreeA->Eof)
     {
       ARetVals += FDM->quFreeA->FN(ARetFlName)->AsString + ",";
       FDM->quFreeA->Next();
     }
    if (FDM->quFreeA->Transaction->Active)
     FDM->quFreeA->Transaction->Commit();
    if (ARetVals == "") ARetVals = "-1";
    else                ARetVals[ARetVals.Length()] = ' ';
}
//---------------------------------------------------------------------------

