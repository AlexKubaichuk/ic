object CountForm: TCountForm
  Left = 450
  Top = 320
  BorderStyle = bsToolWindow
  BorderWidth = 3
  Caption = 'CountForm'
  ClientHeight = 204
  ClientWidth = 356
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 40
    Width = 356
    Height = 26
    Align = alTop
    Caption = 
      #1055#1088#1080' '#1072#1085#1072#1083#1080#1079#1077' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1072' '#1091#1095#1080#1090#1099#1074#1072#1090#1100' '#1091#1089#1083#1086#1074#1080#1103' '#1076#1083#1103' '#1089#1083#1077#1076#1091#1102#1097#1080#1093' '#1087#1072#1088#1072#1084#1077#1090#1088#1086 +
      #1074' ('#1074' '#1089#1083#1091#1095#1072#1077' '#1080#1093' '#1085#1072#1083#1080#1095#1080#1103'):'
    WordWrap = True
  end
  object cxCheckListBox1: TcxCheckListBox
    Left = 0
    Top = 66
    Width = 356
    Height = 97
    Align = alTop
    Items = <
      item
        Text = 'dddddddddd'
      end
      item
        Text = 'dddddddddddddd'
      end
      item
        Text = 'dddddddddddddddddddddddddd'
      end
      item
        Text = 'dddddddddddddddddddddddddddddddd'
      end
      item
        Text = 'dddddddddddddd'
      end
      item
        Text = 'ddddddddddddddddddddddddddddd'
      end>
    Style.LookAndFeel.Kind = lfStandard
    Style.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.Kind = lfStandard
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.Kind = lfStandard
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.Kind = lfStandard
    StyleHot.LookAndFeel.NativeStyle = True
    TabOrder = 0
  end
  object BtnPanel: TPanel
    Left = 0
    Top = 164
    Width = 356
    Height = 40
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    DesignSize = (
      356
      40)
    object OkBtn: TcxButton
      Left = 187
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
    end
    object CancelBtn: TcxButton
      Left = 270
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 356
    Height = 40
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 2
    object Label2: TLabel
      Left = 6
      Top = 13
      Width = 16
      Height = 13
      Caption = #1054#1090':'
    end
    object Label3: TLabel
      Left = 174
      Top = 13
      Width = 18
      Height = 13
      Caption = #1044#1086':'
    end
    object cxMaskEdit1: TcxMaskEdit
      Left = 31
      Top = 9
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d+{0,2}'
      Properties.MaxLength = 0
      TabOrder = 0
      Width = 73
    end
    object cxMaskEdit2: TcxMaskEdit
      Left = 201
      Top = 9
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d+{0,2}'
      Properties.MaxLength = 0
      TabOrder = 1
      Width = 77
    end
  end
end
