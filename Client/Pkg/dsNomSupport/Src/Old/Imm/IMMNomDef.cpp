#include "IMMNomDef.h"
#include <DateUtils.hpp>
#include <Registry.hpp>

#include "RegPPConkretiz.h"
#include "SchConkretiz.h"
#include "ProbReak.h"

#define flVal(a) AFields->Values[AFields->Names[a]]


#define quFNStr(a) FDM->quFreeA->FN(a)->AsString
#define quFNInt(a) FDM->quFreeA->FN(a)->AsInteger
#define quFNFloat(a) FDM->quFreeA->FN(a)->AsFloat
#define quFNDate(a) FDM->quFreeA->FN(a)->AsDateTime
//---------------------------------------------------------------------------
TGetAttrFunc __fastcall TICSIMMNomDef::GetAttrMethodAddr(UnicodeString AFuncName)
{
  TGetAttrFunc RC = NULL;

  if (AFuncName == "AMKBDiag")              RC = AMKBDiag;
  else if (AFuncName == "APlanVac")         RC = APlanVac;
  else if (AFuncName == "AAddrValue")       RC = AAddrValue;
  else if (AFuncName == "AFullAddrValue")   RC = AFullAddrValue;
  else if (AFuncName == "AOrgValue")        RC = AOrgValue;
  else if (AFuncName == "AFullOrgValue")    RC = AFullOrgValue;
  else if (AFuncName == "AFullDopOrgValue") RC = AFullDopOrgValue;
  else if (AFuncName == "APatPrivAge")      RC = APatPrivAge;
  else if (AFuncName == "ASchName")         RC = ASchName;

  if (!RC)
   RC = TICSBaseNomDef::GetAttrMethodAddr(AFuncName);
  return RC;
}
//---------------------------------------------------------------------------
TGetDefFunc __fastcall TICSIMMNomDef::GetDefMethodAddr(UnicodeString AFuncName)
{
  TGetDefFunc RC = NULL;

  if (AFuncName == "DefDatePeriodLast")           RC = DefDatePeriodLast;
  else if (AFuncName == "DefOtvodDatePeriodLast") RC = DefOtvodDatePeriodLast;
  else if (AFuncName == "DefInqTreeValue")        RC = DefInqTreeValue;
  else if (AFuncName == "DefPlanPrivReak")        RC = DefPlanPrivReak;
  else if (AFuncName == "DefPlanProbReak")        RC = DefPlanProbReak;
  else if (AFuncName == "DefPlanPres")            RC = DefPlanPres;
  else if (AFuncName == "DefExtEditValue")        RC = DefExtEditValue;

  if (!RC)
   RC = TICSBaseNomDef::GetDefMethodAddr(AFuncName);
  return RC;
}
//---------------------------------------------------------------------------
TGetSQLFunc __fastcall TICSIMMNomDef::GetSQLMethodAddr(UnicodeString AFuncName)
{
  TGetSQLFunc RC = NULL;

  if (AFuncName == "SQLDatePeriodLast")           RC = SQLDatePeriodLast;
  else if (AFuncName == "SQLOtvodDatePeriodLast") RC = SQLOtvodDatePeriodLast;
  else if (AFuncName == "SQLInqTreeValue")        RC = SQLInqTreeValue;
  else if (AFuncName == "SQLPlanPrivReak")        RC = SQLPlanPrivReak;
  else if (AFuncName == "SQLPlanProbReak")        RC = SQLPlanProbReak;
  else if (AFuncName == "SQLPlanPres")            RC = SQLPlanPres;
  else if (AFuncName == "SQLExtEditValue")        RC = SQLExtEditValue;

  if (!RC)
   RC = TICSBaseNomDef::GetSQLMethodAddr(AFuncName);
  return RC;
}
//---------------------------------------------------------------------------
int __fastcall TICSIMMNomDef::CorrectDay( Word Year, Word Month, Word Day )
{
  Word DaysCount = DaysInAMonth(Year,Month);
  if ( Day > DaysCount )
    Day = DaysCount;
  return Day;
}
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::CalcPlanPeriod(TDate *ADateBP, TDate *ADateEP, TDate *APlanMonth)
{  //����������� ������� ������������ � ������ ������������
   // return: (������� ����� < ���� ������ ������������)

   TDate CurDate;
   Word DayNP,CurYear, CurMonth, CurDay, DayBP, MonthBP, YearBP, DayEP, MonthEP, YearEP;

   if ((int)(*APlanMonth)) CurDate = (*APlanMonth);
   else                    CurDate = Date();

   DecodeDate( CurDate, CurYear, CurMonth, CurDay );

   TRegIniFile *IMMIni = new TRegIniFile ("IMM");
   IMMIni->RootKey = HKEY_LOCAL_MACHINE;
   IMMIni->OpenKey(FDM->RegKey,false);
   DayNP  = IMMIni->ReadInteger("LPUParam","DAY_PLAN",25);
   IMMIni->CloseKey();
   delete IMMIni;

   if ( CurDay < DayNP)
    {
      MonthEP = CurMonth;
      YearEP  = CurYear;

      MonthBP = Dateutils::MonthOf( Sysutils::IncMonth(CurDate,-1));
      YearBP  = ( MonthEP == 1 ) ? YearEP - 1 : YearEP;
    }
   else
    {
      MonthBP   = CurMonth;
      YearBP    = CurYear;

      MonthEP = Dateutils::MonthOf( Sysutils::IncMonth(CurDate));
      YearEP  = ( MonthBP == 12 ) ? CurYear + 1 : CurYear;
    }
   DayBP = CorrectDay(YearBP, MonthBP, DayNP);
   DayEP = CorrectDay(YearEP, MonthEP, 99);

   (*ADateBP) = EncodeDate(YearBP, MonthBP, DayBP);
   (*ADateEP) = EncodeDate(YearEP, MonthEP, DayEP);
   if ((*ADateEP) > CurDate) (*ADateEP) = CurDate;
   (*APlanMonth) = EncodeDate(YearEP, MonthEP, 1);
   return (Date().FormatString("dd").ToIntDef(1) < DayNP);
}
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#                   ��������� ��������� �������� ���������                   #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::AMKBDiag(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  ARet = FDM->MKB[AFields->Values[AFields->Names[0]]];
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::APlanVac(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  UnicodeString DDD = "";
  if (flVal(1) == "0")
   {// ����
     TReplaceFlags rFlag;
     rFlag << rfReplaceAll;
     TStringList *tmp = new TStringList;
     tmp->Text =  StringReplace(flVal(4), "#", "\n", rFlag);
     DDD += FDM->VacList[flVal(2)]; // ������������ �������
     if (tmp->Count)
      {
       tmp->Delete(0);
       if (tmp->Count == 1)
        {
         DDD += " "+_UID(tmp->Strings[0]);
        }
       else
        {
          // ���������, �� ������ ���������� ����� ����������
          bool isAll = true;
          for (int i = 1; i < tmp->Count; i++)
           if (_UID(tmp->Strings[i]) != _UID(tmp->Strings[0]))
            isAll = false;
          if (isAll)
           {// ���� ���������� ���������
             if (_UID(tmp->Strings[0]).Trim().UpperCase() == "D")
              DDD += " ���.";
             else
              DDD += " "+_UID(tmp->Strings[0]);
           }
          else
           {// ���� ���������� ������
             DDD += " (";
             for (int i = 0; i < tmp->Count; i++)
              if (tmp->Strings[i].Length())
               {
                 if (_UID(tmp->Strings[i]).Trim().UpperCase() == "D")
                  DDD += FDM->InfList[_GUI(tmp->Strings[i])]+" ���.;";
                 else
                  DDD += FDM->InfList[_GUI(tmp->Strings[i])]+" "+_UID(tmp->Strings[i])+";";
               }
             DDD += ")";
           }
        }
      }
     else
         DDD += " (���.)";
     delete tmp;
   }
  else
   {// �����
     DDD += FDM->ProbList[flVal(3)];
   }
  ARet = DDD;
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::AAddrValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Values["R0022"] == "1")
   {
     Variant DDD[1];
     DDD[0] = Variant(AFields->Values["R0032"]);
     FDM->quFreeA->Close();
     FDM->quFreeA->SQL->Clear();
     if (!FDM->trFreeA->Active) FDM->trFreeA->StartTransaction();
     FDM->quFreeA->ExecProcedure("CLASSDATA_0017",DDD,0);
     ARet = quFNStr("STR_CLASS").Trim()+" ��. "+AFields->Values["R0033"].Trim();
     FDM->trFreeA->Commit();
   }
  else
   {
     ARet = "(�� ������.) "+AFields->Values["R0024"].Trim();
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::AFullAddrValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Values["R0022"].ToIntDef(0))
   {
     // ��������� �����
     ARet = FDM->GetClassData(AFields->Values["R0032"].ToIntDef(-1),"0019");
     if (AFields->Values["R00FF"].ToIntDef(0) > 0)
      { // �����
        ARet += ", "+FDM->GetClassData(AFields->Values["R00FF"].ToIntDef(-1),"0018");
      }
     if (AFields->Values["R0100"].Length()&&(AFields->Values["R0100"] != "-1"))
      { // ���
        ARet += ", �. "+AFields->Values["R0100"];
        if (AFields->Values["R0033"].Length()&&(AFields->Values["R0033"] != "-1"))
         { // ��������
           ARet += ", ��. "+AFields->Values["R0033"];
         }
      }
   }
  else
   {
     ARet = AFields->Values["R0024"].Trim();
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::AOrgValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Values["R0023"] == "1")
   {
     Variant DDD[1];
     DDD[0] = Variant(AFields->Values["R0025"]);
     FDM->quFreeA->Close();
     FDM->quFreeA->SQL->Clear();
     if (!FDM->trFreeA->Active) FDM->trFreeA->StartTransaction();
     FDM->quFreeA->ExecProcedure("CLASSDATA_0001",DDD,0);
     ARet = quFNStr("STR_CLASS").Trim()+" ";
     if (quFNInt("P_0004"))
      {
        ARet += quFNStr("P_0005").Trim();
        for (int i = 0; i < quFNInt("P_0004"); i++)
         {
           ARet += " ";
           if (AFields->Values[("R002"+IntToStr(6+i)).c_str()].Trim() != "-1")
            ARet += AFields->Values[("R002"+IntToStr(6+i)).c_str()].Trim();
         }
      }
     FDM->trFreeA->Commit();
   }
  else
   {
     ARet = "�� �����������";
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::AFullOrgValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Values["R0023"].ToIntDef(0))
   {
     if (AFields->Values["R0023"].ToIntDef(0) == 1)
      ARet = "��������, ";
     else
      ARet = "������, ";
     ARet += FDM->GetClassData(AFields->Values["R0025"].ToIntDef(-1),"0001");
     if (AFields->Values["R0026"].Length())
      ARet += " "+AFields->Values["R0026"].Trim();
     if (AFields->Values["R0027"].Length())
      ARet += " "+AFields->Values["R0027"].Trim();
     if (AFields->Values["R0028"].Length())
      ARet += " "+AFields->Values["R0028"].Trim();
     if ((AFields->Values["R0023"].ToIntDef(0) == 1))
      {
       if (AFields->Values["R0111"].Length())
        ARet += ", "+AFields->Values["R0111"];
      }
     else
      ARet += ", "+FDM->GetClassData(AFields->Values["R0112"].ToIntDef(-1),"0108");
   }
  else
   {
     ARet = "";
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::AFullDopOrgValue(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Values["R0113"].ToIntDef(0))
   {
     if (AFields->Values["R0113"].ToIntDef(0) == 1)
      ARet = "��������, ";
     else
      ARet = "������, ";
     ARet += FDM->GetClassData(AFields->Values["R0115"].ToIntDef(-1),"0001");
     if (AFields->Values["R0114"].Length())
      ARet += " "+AFields->Values["R0114"].Trim();
   }
  else
   {
     ARet = "";
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::APatPrivAge(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  try
   {
     ARet = AgeStr(StrToDate(AFields->Values[AFields->Names[1]]),StrToDate(AFields->Values[AFields->Names[0]]));
   }
  catch (EConvertError &E)
   {
     ARet = "";
   }
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::ASchName(TStringList *AFields, UnicodeString  ARef, UnicodeString &ARet,UnicodeString  AAttrRef)
{
  if (AFields->Count)
   {
     UnicodeString RC = "";
     UnicodeString flName = GetPart1(AFields->Strings[0],'=').UpperCase();
     TTagNode *SchList;
     UnicodeString xName,xTab;
     if ((flName == "R1092")||(flName == "R1078"))
      { //��������
         SchList = FDM->XMLList->GetXML("12063611-00008cd7-cd89");
         SchList = SchList->GetTagByUID(_GUI(GetPart2(AFields->Strings[0],'=').UpperCase()));
         xName = "R0040";
         xTab = "CLASS_0035";
      }
     else if ((flName == "R1093")||(flName == "R1079"))
      { //�����
         SchList = FDM->XMLList->GetXML("001D3500-00005882-DC28");
         SchList = SchList->GetTagByUID(_GUI(GetPart2(AFields->Strings[0],'=').UpperCase()));
         xName = "R002C";
         xTab = "CLASS_002A";
      }
     if (SchList)
      {
        FDM->qtExec("Select "+xName+" From "+xTab+" Where code="+SchList->AV["ref"]);
        if (FDM->quFreeA->RecordCount)
         RC = FDM->quFreeA->FN(xName.c_str())->AsString+" (������� "+SchList->AV["name"]+")";
      }
     ARet = RC;
   }
  else
   ARet = "";
}
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#            ��������� ��������� �������� �������������� ��������� (Def...)  #
//#                                                                            #
//#            ��������� ������������ ������ SQL-�������             (SQL...)  #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefDatePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   // AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "����"
   // {
   //   1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
   //                            1,2 ��� �������; 0 - �������� ���
   //                                             1 - ������ �� ������� ����
   //                                             2 - ������ �� ����
   //   2-� - ���� ��;   ��� ���� "�������� ���"
   //                    ��� ����� "������ �� ..." �������� �������
   //   3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
   //                    ��� ���� "������ �� ������� ����"  - �� �������������;
   //   4-� - �������� - ��� ���� "�������� ���"  - �� �������������
   //         �������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
   if (!FDM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (NeedKonkr)
    {
      TDateConForm *Dlg = NULL;
      try
       {
         Dlg = new TDateConForm(Application->Owner);
         int FSubType;
         UnicodeString FSubTypeCapt = "";
         TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
         if (FISCondRules->GetParent("IS") == FISCondRules->GetParent("domain")->GetFirstChild()) // ���� ��������
          FSubType = 0;
         else  // ���� ������������
          {
            if (!FISAttr->AV["add_ref"].Length())
             FSubTypeCapt = "������ ��������� ������������";
            else
             FSubTypeCapt = FISAttr->AV["add_ref"];
            FSubType = 1;
          }
         bool DoNotDop = FISAttr->CmpAV("ref","4031DA5E-8AC52FB3-7D03.1024");
         Dlg->SetType(_scDate,AIE,FSubType,FSubTypeCapt,DoNotDop);
         Dlg->MsgLab->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count)
       {
         tmp = "";
         __tmp = AIE->GetFirstChild();
         int DType = AIE->GetFirstChild()->GetAVDef("value","0").ToInt();
         bool FLast = (bool)(DType&1);
         DType = DType >> 1;
         if (FLast)
          {
            TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
            if (!FISAttr->AV["add_ref"].Length())
             tmp = "������ ��������� ������������";
            else
             tmp = FISAttr->AV["add_ref"];
          }
         TTagNode *DateType   = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam1 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam2 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam3 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam4 = __tmp;
         if (DType == 0)
          {// �������� ���
            if ((DateParam1 && (DateParam1->AV["value"].Length())) ||
                (DateParam2 && (DateParam2->AV["value"].Length())))
             {
               tmp += " "+DateType->AV["name"];
               if (DateParam1->AV["value"].Length())
                {
                  tmp += " "+DateParam1->AV["name"];
                  tmp += "='"+DateParam1->AV["value"]+"'";
                }
               if (DateParam2->AV["value"].Length())
                {
                  tmp += " "+DateParam2->AV["name"];
                  tmp += "='"+DateParam2->AV["value"]+"'";
                }
             }
          }
         else if (DType == 3)
          {// ������ ������������ ���������
            tmp += " "+DateType->AV["name"];
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         else
          {// ������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
            tmp += " "+DateType->AV["name"];
            if (DateParam1->AV["value"].Length())
             {
               tmp += " "+DateParam1->AV["name"];
               tmp += "='"+DateParam1->AV["value"]+"'";
             }
            if (DateParam2->AV["value"].Length())
             {
               tmp += " "+DateParam2->AV["name"];
               tmp += "='"+DateParam2->AV["value"]+"'";
             }
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         if (DateParam4)
          {
            tmp += " "+DateParam4->AV["name"];
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLDatePeriodLast(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   TTagNode *DateOffs, *__tmp;
   bool Otvod = false;
   ATab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   UnicodeString xFr,xTo,flName,xPer,xOffs,xTabName,xMaxSQL,xMaxTabName,MaxflName;
   xFr = ""; xTo = "";
   xTabName = FIS->AV["maintable"];
   flName = FIS->AV["tabalias"]+"."+FISAttr->AV["fields"];
   int DType = AIE->GetFirstChild()->GetAVDef("value","0").ToInt();
   bool FLast = (bool)(DType&1);
   DType = DType >> 1;
   TTagNode *TagFr, *TagTo, *NotDop;
   __tmp = AIE->GetFirstChild()->GetNext();
   TagFr = __tmp; __tmp = __tmp->GetNext();
   TagTo = __tmp; __tmp = __tmp->GetNext();
   DateOffs = __tmp; __tmp = __tmp->GetNext();
   NotDop = __tmp;
   xMaxTabName = "DTT"+IntToStr((int)FLast);
   xMaxSQL =  flName+" = (Select Max("+xMaxTabName+"."+FISAttr->AV["fields"]+") From "+xTabName+" "+xMaxTabName;
   xMaxSQL += " Where "+xMaxTabName+".UCODE="+FIS->AV["tabalias"]+".UCODE";
   MaxflName = xMaxTabName+"."+FISAttr->AV["fields"];
   if (DType == 0)
    {// �������� ���
      if (TagFr && (TagFr->AV["value"].Length()))
       xFr = ">='"+_CorrectDate(TagFr->AV["value"])+"'";
      if (TagTo && (TagTo->AV["value"].Length()))
       xTo = "<='"+_CorrectDate(TagTo->AV["value"])+"'";
    }
   else if (DType == 1)
    {// ������ �� ������� ����
      xPer = TagFr->AV["value"];
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
         xFr = ">='"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),xPer,1)+"'";
         xTo = "<='"+Date().FormatString("dd.mm.yyyy")+"'";
       }
      else
       {
         xFr = ">='"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),AddPeriod(xPer,xOffs),1)+"'";
         xTo = "<='"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),xOffs,1)+"'";
       }
    }
   else if (DType == 2)
    {// ������ �� ���� ��������� ������� ������������ ���������
      UnicodeString FDocPer = GetPart1(UnicodeString(ARet),'#');
      if (FDocPer.Length())
       {
         if (GetPart2(FDocPer,';').Length())
          FDocPer = GetPart2(FDocPer,';');
         else
          FDocPer = Date().FormatString("dd.mm.yyyy");
       }
      else
       FDocPer = Date().FormatString("dd.mm.yyyy");
      xPer  = TagFr->AV["value"];
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
         xFr = ">='"+FIncPeriod(FDocPer,xPer,1)+"'";
         xTo = "<='"+FDocPer+"'";
       }
      else
       {
         xFr = ">='"+FIncPeriod(FDocPer,AddPeriod(xPer,xOffs),1)+"'";
         xTo = "<='"+FIncPeriod(FDocPer,xOffs,1)+"'";
       }
    }
   else if (DType == 3)
    {// ������ ������������ ���������
      UnicodeString FPer = GetPart1(UnicodeString(ARet),'#');
      if (FPer.Length())
       {
         xOffs = DateOffs->AV["value"];
         if (!xOffs.Length())
          {
            if (GetPart1(FPer,';').Length())
             xFr = ">='"+TDate(GetPart1(FPer,';')).FormatString("dd.mm.yyyy")+"'";
            if (GetPart2(FPer,';').Length())
             xTo = "<='"+TDate(GetPart2(FPer,';')).FormatString("dd.mm.yyyy")+"'";
          }
         else
          {
            if (GetPart1(FPer,';').Length())
             xFr = ">='"+FIncPeriod(GetPart1(FPer,';'),xOffs,1)+"'";
            if (GetPart2(FPer,';').Length())
             xTo = "<='"+FIncPeriod(GetPart2(FPer,';'),xOffs,1)+"'";
          }
       }
    }
   if (xFr.Length())
    {
      xMaxSQL += " and "+MaxflName+xFr;
      xFr = flName+xFr;
    }
   if (xTo.Length())
    {
      xMaxSQL += " and "+MaxflName+xTo;
      xTo = flName+xTo;
    }
   if (FLast)
    {
      TTagNode *tmp;
      // ��������
      tmp = AIE->GetParent();
      tmp = tmp->GetChildByAV("IERef","ref","0E291426-00005882-2493."+FGetRefValue(FISAttr, "0070","0086"));
      if (tmp)
       {
         if (tmp->Count > 1) // ���������� �������� ?
          {
            TTagNode *itNode = tmp->GetFirstChild();
            while (itNode)
             {
               if (itNode->CmpAV("ref","1"))       // ���������� �������� �������� !!!
                xMaxSQL += " and "+xMaxTabName+".R1075="+itNode->AV["value"].Trim();
               else if (itNode->CmpAV("ref","2"))  // ���������� �������� ������� !!!
                xMaxSQL += " and "+xMaxTabName+".R1020="+itNode->AV["value"].Trim();
//               else if (itNode->CmpAV("ref","3"))  // ���������� �������� ����� !!!
//                xMaxSQL += " and "+FDM->UpperFunc+"("+xMaxTabName+".R1023)='"+itNode->AV["value"].Trim().UpperCase()+"'";
//               else if (itNode->CmpAV("ref","4"))  // ���������� �������� ���� ���������� !!!
//                xMaxSQL += " and "+FDM->UpperFunc+"("+xMaxTabName+".R1021)='"+itNode->AV["value"].Trim().UpperCase()+"'";
               itNode = itNode->GetNext();
             }
          }
         if (NotDop)
          {
            if (NotDop->CmpAV("value","1"))
             xMaxSQL += " and "+FDM->UpperFunc+"("+xMaxTabName+".R1021)<>'���.'";
          }
       }
      else
       {
          // �����
          tmp = AIE->GetParent();
          tmp = tmp->GetChildByAV("IERef","ref","0E291426-00005882-2493."+FGetRefValue(FISAttr, "0044","00B0"));
          if (tmp)
          {
            tmp = tmp->GetChildByAV("text","ref","0");
            if (tmp) // ���������� �������� ����� ?
             {
               xMaxSQL += " and "+xMaxTabName+".R1032="+tmp->AV["value"];
             }
          }
       }
      xMaxSQL += ")";
      ARet = xMaxSQL;
    }
   if (xFr.Length() && xTo.Length())
    {
      if (FLast)
       ARet = "("+xFr+" and "+xTo+" and "+xMaxSQL+")";
      else
       ARet = "("+xFr+" and "+xTo+")";
    }
   else if (xFr.Length())
    {
      if (FLast)
       ARet = "("+xFr+" and "+xMaxSQL+")";
      else
       ARet = xFr;
    }
   else if (xTo.Length())
    {
      if (FLast)
       ARet = "("+xTo+" and "+xMaxSQL+")";
      else
       ARet = xTo;
    }
   else
    {
      if (FLast)
       ARet = xMaxSQL;
      else
       ARet = "";
    }
}
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefOtvodDatePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   // AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "����"
   // {
   //   1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
   //                            1,2 ��� �������; 0 - �������� ���
   //                                             1 - ������ �� ������� ����
   //                                             2 - ������ �� ����
   //   2-� - ���� ��;   ��� ���� "�������� ���"
   //                    ��� ����� "������ �� ..." �������� �������
   //   3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
   //                    ��� ���� "������ �� ������� ����"  - �� �������������;
   //   4-� - �������� - ��� ���� "�������� ���"  - �� �������������
   //         �������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
   if (!FDM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (NeedKonkr)
    {
      TDateConForm *Dlg = NULL;
      try
       {
         Dlg = new TDateConForm(Application->Owner);
         int FSubType;
         UnicodeString FSubTypeCapt = "";
         TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
         if (FISCondRules->GetParent("IS") == FISCondRules->GetParent("domain")->GetFirstChild()) // ���� ��������
          FSubType = 0;
         else  // ���� ������������
          {
            if (!FISAttr->AV["add_ref"].Length())
             FSubTypeCapt = "������ ��������� ������������";
            else
             FSubTypeCapt = FISAttr->AV["add_ref"];
            FSubType = 1;
          }
         bool DoNotDop = FISAttr->CmpAV("ref","4031DA5E-8AC52FB3-7D03.1024");
         Dlg->SetType(_scDate,AIE,FSubType,FSubTypeCapt,DoNotDop);
         Dlg->MsgLab->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count)
       {
         tmp = "";
         __tmp = AIE->GetFirstChild();
         int DType = AIE->GetFirstChild()->GetAVDef("value","0").ToInt();
         bool FLast = (bool)(DType&1);
         DType = DType >> 1;
         if (FLast)
          {
            TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
            if (!FISAttr->AV["add_ref"].Length())
             tmp = "������ ��������� ������������";
            else
             tmp = FISAttr->AV["add_ref"];
          }
         TTagNode *DateType   = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam1 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam2 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam3 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam4 = __tmp;
         if (DType == 0)
          {// �������� ���
            if ((DateParam1 && (DateParam1->AV["value"].Length())) ||
                (DateParam2 && (DateParam2->AV["value"].Length())))
             {
               tmp += " "+DateType->AV["name"];
               if (DateParam1->AV["value"].Length())
                {
                  tmp += " "+DateParam1->AV["name"];
                  tmp += "='"+DateParam1->AV["value"]+"'";
                }
               if (DateParam2->AV["value"].Length())
                {
                  tmp += " "+DateParam2->AV["name"];
                  tmp += "='"+DateParam2->AV["value"]+"'";
                }
             }
          }
         else if (DType == 3)
          {// ������ ������������ ���������
            tmp += " "+DateType->AV["name"];
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         else
          {// ������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
            tmp += " "+DateType->AV["name"];
            if (DateParam1->AV["value"].Length())
             {
               tmp += " "+DateParam1->AV["name"];
               tmp += "='"+DateParam1->AV["value"]+"'";
             }
            if (DateParam2->AV["value"].Length())
             {
               tmp += " "+DateParam2->AV["name"];
               tmp += "='"+DateParam2->AV["value"]+"'";
             }
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         if (DateParam4)
          {
            tmp += " "+DateParam4->AV["name"];
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLOtvodDatePeriodLast(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   TTagNode *DateOffs, *__tmp;
   bool Otvod = false;
   Otvod = FISCondRules->CmpAV("uid","007B,007C");
   ATab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   UnicodeString xFr,xTo,flName,xPer,xOffs,xTabName,xMaxSQL,xMaxTabName,MaxflName;
   xFr = ""; xTo = "";
   xTabName = FIS->AV["maintable"];
   if (FIS->AV["tabalias"].Length())
    xTabName = FIS->AV["tabalias"];
   flName = FIS->AV["tabalias"]+"."+FISAttr->AV["fields"];
   int DType = AIE->GetFirstChild()->GetAVDef("value","0").ToInt();
   bool FLast = (bool)(DType&1);
   DType = DType >> 1;
   TTagNode *TagFr, *TagTo, *NotDop;
   __tmp = AIE->GetFirstChild()->GetNext();
   TagFr = __tmp; __tmp = __tmp->GetNext();
   TagTo = __tmp; __tmp = __tmp->GetNext();
   DateOffs = __tmp; __tmp = __tmp->GetNext();
   NotDop = __tmp;
   xMaxTabName = "DTT"+IntToStr((int)FLast);
   xMaxSQL =  flName+" = (Select Max("+xMaxTabName+"."+FISAttr->AV["fields"]+") From "+xTabName+" "+xMaxTabName;
   xMaxSQL += " Where "+xMaxTabName+".UCODE="+FIS->AV["tabalias"]+".UCODE";
   MaxflName = xMaxTabName+"."+FISAttr->AV["fields"];
   if (DType == 0)
    {// �������� ���
      if (TagFr && (TagFr->AV["value"].Length()))
       xFr = "'"+_CorrectDate(TagFr->AV["value"])+"'";
      if (TagTo && (TagTo->AV["value"].Length()))
       xTo = "'"+_CorrectDate(TagTo->AV["value"])+"'";
    }
   else if (DType == 1)
    {// ������ �� ������� ����
      xPer = TagFr->AV["value"];
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
         xFr = "'"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),xPer,1)+"'";
         xTo = "'"+Date().FormatString("dd.mm.yyyy")+"'";
       }
      else
       {
         xFr = "'"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),AddPeriod(xPer,xOffs),1)+"'";
         xTo = "'"+FIncPeriod(Date().FormatString("dd.mm.yyyy"),xOffs,1)+"'";
       }
    }
   else if (DType == 2)
    {// ������ �� ���� ��������� ������� ������������ ���������
      UnicodeString FDocPer = GetPart1(UnicodeString(ARet),'#');
      if (FDocPer.Length())
       {
         if (GetPart2(FDocPer,';').Length())
          FDocPer = GetPart2(FDocPer,';');
         else
          FDocPer = Date().FormatString("dd.mm.yyyy");
       }
      else
       FDocPer = Date().FormatString("dd.mm.yyyy");
      xPer  = TagFr->AV["value"];
      xOffs = DateOffs->AV["value"];
      if (!xOffs.Length())
       {
         xFr = ">='"+FIncPeriod(FDocPer,xPer,1)+"'";
         xTo = "<='"+FDocPer+"'";
       }
      else
       {
         xFr = ">='"+FIncPeriod(FDocPer,AddPeriod(xPer,xOffs),1)+"'";
         xTo = "<='"+FIncPeriod(FDocPer,xOffs,1)+"'";
       }
    }
   else if (DType == 3)
    {// ������ ������������ ���������
      UnicodeString FPer = GetPart1(UnicodeString(ARet),'#');
      if (FPer.Length())
       {
         xOffs = DateOffs->AV["value"];
         if (!xOffs.Length())
          {
            if (GetPart1(FPer,';').Length())
             xFr = "'"+TDate(GetPart1(FPer,';')).FormatString("dd.mm.yyyy")+"'";
            if (GetPart2(FPer,';').Length())
             xTo = "'"+TDate(GetPart2(FPer,';')).FormatString("dd.mm.yyyy")+"'";
          }
         else
          {
            if (GetPart1(FPer,';').Length())
             xFr = "'"+FIncPeriod(GetPart1(FPer,';'),xOffs,1)+"'";
            if (GetPart2(FPer,';').Length())
             xTo = "'"+FIncPeriod(GetPart2(FPer,';'),xOffs,1)+"'";
          }
       }
    }
   UnicodeString frFlName,toFlName,frMaxFlName,toMaxFlName,CommSQL;
   frFlName = xTabName+".R105F";
   toFlName = xTabName+".R1065";
   frMaxFlName = xMaxTabName+".R105F";
   toMaxFlName = xMaxTabName+".R1065";
   CommSQL = "";
   if (xFr.Length() && xTo.Length())
    {
      xMaxSQL += " and "+frMaxFlName+" <= "+xTo+" and ("+toMaxFlName+">="+xFr+" or "+toMaxFlName+" is NULL)";
      CommSQL = frFlName+" <= "+xTo+" and ("+toFlName+">="+xFr+" or "+toFlName+" is NULL)";
    }
   else if (xFr.Length())
    {
      xMaxSQL += " and ("+toMaxFlName+">="+xFr+" or "+toMaxFlName+" is NULL)";
      CommSQL = "("+toFlName+">="+xFr+" or "+toFlName+" is NULL)";
    }
   else if (xTo.Length())
    {
      xMaxSQL += " and "+frMaxFlName+" <= "+xTo;
      CommSQL = frFlName+" <= "+xTo;
    }
   if (FLast)
    {
      TTagNode *tmp;
      tmp = AIE->GetParent();
      tmp = tmp->GetChildByAV("IERef","ref","0E291426-00005882-2493."+FGetRefValue(FISAttr, "004F","00BB"));
      if (tmp)
       {
         tmp = tmp->GetChildByAV("text","ref","0");
         if (tmp) // ���������� �������� �������� ?
          {
            xMaxSQL += " and "+xMaxTabName+".R1083="+tmp->AV["value"];
          }
       }
      tmp = AIE->GetParent();
      tmp = tmp->GetChildByAV("IERef","ref","0E291426-00005882-2493."+FGetRefValue(FISAttr, "004E","00BA"));
      if (tmp)
       {
         tmp = tmp->GetChildByAV("text","ref","0");
         if (tmp) // �������� ��� ������ ?
          {
            xMaxSQL += " and "+xMaxTabName+".R1043="+tmp->AV["value"];
          }
       }
      xMaxSQL += ")";
    }
   if (CommSQL.Length())
    {
      if (FLast)
       ARet = "("+CommSQL+" and "+xMaxSQL+")";
      else
       ARet = "("+CommSQL+")";
    }
   else
    {
      if (FLast)
       ARet = xMaxSQL;
      else
       ARet = "";
    }
}
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefInqTreeValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   if (!FDM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   if (NeedKonkr)
    {
      try
       {
         FDM->MKBDlg->ShowModal();
         if (FDM->MKBDlg->ModalResult == mrOk)
          {
            AIE->DeleteChild();
            TTagNode *tmp = AIE->AddChild("text");
            tmp->AddAttr("name",FDM->MKBDlg->clExtCode+" "+FDM->MKBDlg->clText);
            tmp->AddAttr("value",IntToStr(FDM->MKBDlg->clCode));
            tmp->AddAttr("ref","0");
            AIE->AddChild("text","name=������ ,value="+IntToStr((int)FDM->MKBDlg->clSelectGroup)+", ref=1");
            AIE->AddChild("text","name=���1 ,value="+IntToStr(FDM->MKBDlg->clCode)+", ref=2");
            AIE->AddChild("text","name=���2 ,value="+IntToStr(FDM->MKBDlg->clCodeLast)+", ref=3");
            if (FDM->MKBDlg->clSelectGroup)
             AIE->AddChild("text","name=SQL ,value=(MKB.CODE >= "+IntToStr(FDM->MKBDlg->clCode)+" and MKB.CODE <= "+IntToStr(FDM->MKBDlg->clCodeLast)+"), ref=4");
            else
             AIE->AddChild("text","name=SQL ,value=MKB.CODE = "+IntToStr(FDM->MKBDlg->clCode)+", ref=4");
          }
       }
      __finally
       {
       }
    }
   if (AIE->GetFirstChild())
    {
      tmp = "";
      if (AIE->Count > 1)
       {
         if (AIE->GetFirstChild()->GetNext())
          {
            if (AIE->GetFirstChild()->GetNext()->AV["value"] != "0")
             tmp += "{��������� ���������}";
          }
         tmp += AIE->GetFirstChild()->AV["name"];
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLInqTreeValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   UnicodeString FTabName = FIS->AV["tabalias"];
   UnicodeString FColName = FTabName+"."+FISAttr->AV["fields"];
   UnicodeString ISTab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   ATab = ISTab;
   if (!AIE->Count)
    {
      ARet = "";
    }
   else
    {
      TTagNode *FMCode = AIE->GetChildByAV("text","ref","1");
      if (FMCode)
       {
         if (FMCode->CmpAV("value","1"))
          ARet = "("+FColName+" >= "+FMCode->GetNext()->AV["value"]+" and "+FColName+" <= "+FMCode->GetNext()->GetNext()->AV["value"]+")";
         else
          ARet = " "+FColName+" = "+FMCode->GetNext()->AV["value"];
       }
      else      ARet = "";
    }
}
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefPlanPrivReak(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   if (!FDM) return false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
      TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      __tmp = AIE->GetParent();
      __tmp = __tmp->GetChildByAV("IERef","ref","0E291426-00005882-2493."+FGetRefValue(FISAttr, "0070","0086"));
      tmp = "";
      if (__tmp)
       {
         __tmp = __tmp->GetChildByAV("text","ref","2");
         if (__tmp)
          { // ���������� �������� ������� !!!
            tmp = __tmp->AV["value"];
          }
       }
      if (!tmp.Length())
       {
         ShowMessage("�� ����� ����!\n ������ ������ ������� ����������, \n�.�. ������ ������� ������� �� ���� ����");
         return false;
       }
      tmp = "<root>"
            "<fl ref='1020' def='"+tmp+"' en='0'/>"
            "<fl ref='1025'/>"
            "<fl ref='1026'/>"
            "<fl ref='1028'/>"
            "<fl ref='10A8'/>"
            "<fl ref='1027'/>"
            "</root>";
    }
   return FDM->DefRegValue(AIE, AText, NeedKonkr, AXMLIE, tmp, "", false, false);
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLPlanPrivReak(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   GetSQLValue(AIE, ATab, ARet);
}
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefPlanProbReak(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   if (!FDM) return false;
   UnicodeString tmp;
   TTagNode *__tmp;
   bool iRC = false;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
      TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      __tmp = AIE->GetParent();
      __tmp = __tmp->GetChildByAV("IERef","ref","0E291426-00005882-2493."+FGetRefValue(FISAttr, "0044","00B0"));
      tmp = "";
      if (__tmp)
       {
         __tmp = __tmp->GetChildByAV("text","ref","0");
         if (__tmp)
          {// ���������� �������� ����� !!!
             tmp = __tmp->AV["value"];
          }
       }
      if (!tmp.Length())
       {
         ShowMessage("�� ������ �����!\n ������ ������ ������� ����������, \n�.�. ������ ������� ������� �� ���� �����");
         return false;
       }
      TProbReakForm *Dlg = NULL;
      try
       {
         Dlg = new TProbReakForm(Application->Owner, AIE, NeedKonkr,tmp, FDM);
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         __tmp = AIE->GetFirstChild();
         tmp = "";
         while (__tmp->GetNext())
          {
            tmp += " "+__tmp->AV["name"];
            __tmp = __tmp->GetNext();
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLPlanProbReak(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   ATab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   UnicodeString FSQL = "";
   TTagNode *_tmp;
   FSQL = GetChildContAVDef(AIE, "������ ���������:", "-1");
   int FFixVal, FDelta, FDelta2;
   if (FSQL != "-1")
    {
      if (FSQL == "0")
       { // ������ ����������
          FFixVal = GetChildContAVDef(AIE, "����������� �������� ������:", "0").ToIntDef(0);
          FDelta  = GetChildContAVDef(AIE, "����������:", "0").ToIntDef(0);
          FDelta2 = GetChildContAVDef(AIE, "���������� �� ���������:", "0").ToIntDef(0);
          if (!(FDelta2 > 0))
           FDelta2 = FFixVal;
          FSQL =  "PROB.R1041 = 1 and PROB.R10A7=2 and ( ";
          FSQL += "(PROB.R103F >= "+IntToStr(FFixVal)+" and PROB.R10C2=2 and ";
          FSQL += "(PROB.R103F - PROB.R10C1) >= "+IntToStr(FDelta)+") or ";
          FSQL += "(PROB.R103F >= "+IntToStr(FDelta2)+" and PROB.R10C2=7))";
       }
      else
       { // ������ ������
          FFixVal = GetChildContAVDef(AIE, "����������� �������� ������:", "0").ToIntDef(0);
          FSQL = "PROB.R1041 = 1 and PROB.R103F > "+IntToStr(FFixVal)+" and PROB.R10A7=2 and (PROB.R10C1<2 or PROB.R10C1 is NULL)";
       }
    }
   else
    FSQL = "";
   _tmp = AIE->GetChildByAV("text","name","sql");
   if (_tmp)
    {
      if (FSQL.Length()) FSQL = FSQL+" and "+_tmp->AV["value"];
      else               FSQL = _tmp->AV["value"];
    }
   ARet = FSQL;
}
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefPlanPres(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   if (!FDM) return false;
   bool iRC = false;
   UnicodeString tmp;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
      TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      TTagNode *FExtAttr = FDM->XMLList->GetRefer(FISAttr->AV["ref"]);
      tmp = "<root>"
            "<fl ref='1086'/>"
            "<fl ref='1089'/>"
            "<fl ref='108A'/>"
            "</root>";
      TRegPPConForm *Dlg = NULL;
      try
       {
         Dlg = new TRegPPConForm(Application->Owner, tmp, AIE, FExtAttr->GetRoot()->CmpName("registr"), NeedKonkr,FDM);
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         TTagNode *__tmp = AIE->GetFirstChild();
         tmp = "";
         while (__tmp)
          {
            if (!__tmp->CmpAV("name","sql"))
             tmp += " "+__tmp->AV["name"];
            __tmp = __tmp->GetNext();
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLPlanPres(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   ATab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   TTagNode *_tmp = AIE->GetChildByAV("text","name","sql");
   if (_tmp)
    {
      TDate FDateBP;
      TDate FDateEP;
      TDate FPlanMonth;
      CalcPlanPeriod(&FDateBP, &FDateEP, &FPlanMonth);
      UnicodeString cdPlan,pdPlan;
      cdPlan = FPlanMonth.FormatString("01.mm.yyyy");
      pdPlan = Dateutil::IncMonth(FPlanMonth,-1).FormatString("01.mm.yyyy");
      TReplaceFlags rFlag;
      rFlag << rfReplaceAll;
      ARet = StringReplace(StringReplace(_tmp->AV["value"], "_PREV_", pdPlan, rFlag), "_CURR_", cdPlan, rFlag);
    }
   else      ARet = "";
}
//---------------------------------------------------------------------------
bool __fastcall TICSIMMNomDef::DefExtEditValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   if (!FDM) return false;
   UnicodeString tmp;
   TTagNode *__tmp;
   bool iRC = false;
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
      TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      __tmp = AIE->GetParent();
      __tmp = __tmp->GetChildByAV("IERef","ref","0E291426-00005882-2493."+FGetRefValue(FISAttr, "0050","006D"));
      tmp = "";
      if (__tmp)
       {
         __tmp = __tmp->GetChildByAV("text","ref","0");
         if (__tmp)
          { // ���������� �������� �������� !!!
            tmp = __tmp->AV["value"];
          }
       }
      if (!tmp.Length())
       {
         ShowMessage("�� ������ ����������!\n ������ ����� ���������� �������� ����������, \n�.�. ����� ���������� �������� ������� �� ���� ��������");
         return false;
       }
      TSchForm *Dlg = NULL;
      try
       {
         Dlg = new TSchForm(Application->Owner, AIE, NeedKonkr,tmp,FDM);
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         __tmp = AIE->GetFirstChild();
         tmp = "";
         while (__tmp->GetNext())
          {
            tmp += " "+__tmp->AV["name"];
            __tmp = __tmp->GetNext();
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
void __fastcall TICSIMMNomDef::SQLExtEditValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   ATab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   TTagNode *_tmp = AIE->GetChildByAV("text","name","sql");
   if (_tmp) ARet = _tmp->AV["value"];
   else      ARet = "";
}
//---------------------------------------------------------------------------

