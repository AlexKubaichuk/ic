object SchForm: TSchForm
  Left = 647
  Top = 338
  BorderStyle = bsToolWindow
  Caption = #1042#1099#1073#1086#1088' '#1089#1093#1077#1084' '#1087#1083#1072#1085#1080#1088#1086#1074#1072#1085#1080#1103
  ClientHeight = 189
  ClientWidth = 310
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 149
    Width = 310
    Height = 40
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      310
      40)
    object OkBtn: TcxButton
      Left = 145
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      OnClick = OkBtnClick
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
    end
    object CancelBtn: TcxButton
      Left = 226
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
    end
  end
  object CompPanel: TPanel
    Left = 0
    Top = 0
    Width = 310
    Height = 149
    Align = alClient
    TabOrder = 1
    object VakCBE: TComboEdit
      Left = 11
      Top = 65
      Width = 291
      Height = 21
      Color = clInfoBk
      DirectInput = False
      GlyphKind = gkEllipsis
      NumGlyphs = 1
      TabOrder = 0
      OnButtonClick = VakCBEButtonClick
    end
    object ProbCBE: TComboEdit
      Left = 11
      Top = 111
      Width = 291
      Height = 21
      Color = clBtnFace
      DirectInput = False
      Enabled = False
      GlyphKind = gkEllipsis
      NumGlyphs = 1
      TabOrder = 1
      OnButtonClick = ProbCBEButtonClick
    end
    object VakRB: TRadioButton
      Left = 11
      Top = 46
      Width = 153
      Height = 17
      Caption = #1052#1048#1041#1055
      Checked = True
      TabOrder = 2
      TabStop = True
      OnClick = VakRBClick
    end
    object ProbRB: TRadioButton
      Left = 11
      Top = 89
      Width = 124
      Height = 17
      Caption = #1057#1093#1077#1084#1072' '#1076#1083#1103' '#1087#1088#1086#1073#1099
      TabOrder = 3
      OnClick = VakRBClick
    end
  end
end
