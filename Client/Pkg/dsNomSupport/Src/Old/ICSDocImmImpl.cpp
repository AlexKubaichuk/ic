// AKDOREPORTIMPL : Implementation of TAKDOReportImpl (CoClass: AKDOReport, Interface: IAKDOReport)

#include <vcl.h>
#pragma hdrstop

#include "ICSDocImmImpl.h"

#include "Dmf_Auto.h"
#include "IMMNomDef.h"
#define _VERSION_  "1.0.0.0"

//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::get_Version(LPSTR* Value)
{
  try
  {
    strcpy(*Value,_VERSION_);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::get_XMLContainer(long* Value)
{
  try
  {
    if (MDM)
     *Value = (long)(MDM->XMLList);
    else
     *Value = NULL;
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::set_XMLContainer(long Value)
{
  try
  {
    if (MDM)
     MDM->XMLList  = (TAxeXMLContainer*)Value;
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::get_DBName(BSTR* Value)
{
  try
  {
    if (MDM)
     *Value = StringToOleStr(MDM->DBName);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::get_UserName(BSTR* Value)
{
  try
  {
    if (MDM)
     *Value = StringToOleStr(MDM->User);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::get_UserPWD(BSTR* Value)
{
  try
  {
    if (MDM)
     *Value = StringToOleStr(MDM->UserPwd);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::set_DBName(BSTR Value)
{
  try
  {
    if (MDM) MDM->DBName = UnicodeString(Value);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::set_UserName(BSTR Value)
{
  try
  {
    if (MDM) MDM->User = UnicodeString(Value);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::set_UserPWD(BSTR Value)
{
  try
  {
    if (MDM) MDM->UserPwd = UnicodeString(Value);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::get_Charset(BSTR* Value)
{
  try
  {
    if (MDM)
     *Value = StringToOleStr(MDM->Charset);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::set_Charset(BSTR Value)
{
  try
  {
    if (MDM) MDM->Charset = UnicodeString(Value);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::get_UpperFunc(BSTR* Value)
{
  try
  {
    if (MDM)
     *Value = StringToOleStr(MDM->UpperFunc);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::set_UpperFunc(BSTR Value)
{
  try
  {
    if (MDM) MDM->UpperFunc = UnicodeString(Value);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::GetAVal(BSTR FuncName, BSTR Fields, BSTR Ref,
  BSTR AttrRef, BSTR* Ret)
{
  try
   {
     if (FuncDef)
      {
        TStringList *tmp = NULL;
        UnicodeString FRet;
        FRet = WideCharToString(*Ret);
        try
         {
           tmp = new TStringList;  
           tmp->Text = WideCharToString(Fields);
           FuncDef->GetAtttInfo(WideCharToString(FuncName), tmp, WideCharToString(Ref), FRet, WideCharToString(AttrRef));
           if (FRet.Length())
            wcscpy(*Ret,StringToOleStr(FRet));
           else
            *Ret = '\0'; 
         }
        __finally
         {
           if (tmp) delete tmp;
         }
      }
   }
  catch(Exception &e)
   {
     return Error(e.Message.c_str(), IID_IICSDocImm);
   }
  return S_OK;
}
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::GetDVal(BSTR FuncName, long Node,
  VARIANT_BOOL NeedKonkr, BSTR* Text, BSTR* IEXML, short* Res)
{
  try
   {
     *Res = true;
     if (FuncDef)
      {
        UnicodeString FText;
        UnicodeString FIE;
        TTagNode *FNode = NULL;
        try
         {
           *Res = false;
           FNode = new TTagNode(NULL);
           UnicodeString FUID = ((TTagNode*)Node)->AV["uid"];
           FNode->Assign(((TTagNode*)Node)->GetRoot(),true);
           FNode = FNode->GetTagByUID(FUID);
           if (FNode)
            *Res = FuncDef->GetDefInfo(WideCharToString(FuncName), FNode, FText, (bool)NeedKonkr, FIE);
           if (*Res)
            {
              if (FText.Length())
               wcscpy(*Text,StringToOleStr(FText));
              else
               *Text = '\0';
              if (FIE.Length())
               wcscpy(*IEXML,StringToOleStr(FIE));
              else
               *IEXML = '\0';
            }
         }
        __finally
         {
           if (FNode) delete FNode;
         }
      }
   }
  catch(Exception &e)
   {
     return Error(e.Message.c_str(), IID_IICSDocImm);
   }
  return S_OK;
}
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::GetSVal(BSTR FuncName, long Node, BSTR* Tab,
  BSTR* Ret)
{
  try
   {
     if (FuncDef)
      {
        try
         {
           UnicodeString FTab;
           UnicodeString FRet;
           FRet = WideCharToString(*Ret);
           FuncDef->GetSQLInfo(WideCharToString(FuncName), (TTagNode*)Node, FTab, FRet);
           if (FTab.Length())
            wcscpy(*Tab,StringToOleStr(FTab));
           else
            *Tab = '\0';
           if (FRet.Length())
            wcscpy(*Ret,StringToOleStr(FRet));
           else
            *Ret = '\0';
         }
        __finally
         {
         }
      }
   }
  catch(Exception &e)
   {
     return Error(e.Message.c_str(), IID_IICSDocImm);
   }
  return S_OK;
}
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::Deinit()
{
  if (MDM) MDM->Disconnect();
  return S_OK;
}
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::Init()
{
  if (MDM) MDM->Connect();
  FuncDef = new TICSIMMNomDef(MDM);
  return S_OK;
}
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::get_RegGUI(BSTR* Value)
{
  try
  {
    if (MDM)
     *Value = StringToOleStr(MDM->RegGUI);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::get_RegKey(BSTR* Value)
{
  try
  {
    if (MDM)
     *Value = StringToOleStr(MDM->RegKey);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::set_RegGUI(BSTR Value)
{
  try
  {
    if (MDM) MDM->RegGUI = UnicodeString(Value);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::set_RegKey(BSTR Value)
{
  try
  {
    if (MDM) MDM->RegKey = UnicodeString(Value);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};
//------------------------------------------------------------------------------
STDMETHODIMP TICSDocImmImpl::ClearHash()
{
  try
  {
    if (MDM) MDM->ClearHash();
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
}
//------------------------------------------------------------------------------

STDMETHODIMP TICSDocImmImpl::get_LPUCode(BSTR* Value)
{
  try
  {
    if (MDM)
     *Value = StringToOleStr(MDM->MainLPUCode);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};


STDMETHODIMP TICSDocImmImpl::set_LPUCode(BSTR Value)
{
  try
  {
    if (MDM) MDM->MainLPUCode = UnicodeString(Value);
  }
  catch(Exception &e)
  {
    return Error(e.Message.c_str(), IID_IICSDocImm);
  }
  return S_OK;
};



