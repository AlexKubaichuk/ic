//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "TextConcretiz.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxCheckBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TTextConForm *TextConForm;
//---------------------------------------------------------------------------
__fastcall TTextConForm::TTextConForm(TComponent* Owner, UnicodeString ACapt,TTagNode *ANode, TAxeXMLContainer *AXMLList)
        : TForm(Owner)
{
  FXMLList = AXMLList;
  TextLab->Caption = ACapt;
  Result = ANode;
  if (ANode->Count)
   {
     TTagNode *tmp = ANode->GetChildByAV("text","ref","0");
     if (tmp) TextED->Text = tmp->AV["value"];
     tmp = ANode->GetChildByAV("text","ref","1");
     if (tmp) CaseSensChB->Checked = (bool)tmp->AV["value"].ToIntDef(0);
     tmp = ANode->GetChildByAV("text","ref","2");
     if (tmp) FullWordChB->Checked = (bool)tmp->AV["value"].ToIntDef(0);
     tmp = ANode->GetChildByAV("text","ref","3");
     if (tmp) CompTypeCB->ItemIndex = tmp->AV["value"].ToIntDef(-1);
   }
}
//---------------------------------------------------------------------------
void __fastcall TTextConForm::OkBtnClick(TObject *Sender)
{
  if (CompTypeCB->ItemIndex == -1)
   {
     ActiveControl = CompTypeCB;
     _MSG_ERR("�� ������� �������� ��� ���� \"�������������\".","������ �����");
     return;
   }
  else if ((CompTypeCB->ItemIndex == 2) && !TextED->Text.Length())
   {
     ActiveControl = TextED;
     _MSG_ERR("�� ������� ������������� ��������.","������ �����");
     return;
   }

  TTagNode *FISCondRules = FXMLList->GetRefer(Result->AV["ref"]);
  TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode *FIS = FISAttr->GetParent("is");
  TTagNode *__tmp;
  UnicodeString FFieldName = FIS->AV["tabalias"]+"."+FISAttr->AV["fields"];
  if (Result->Count) Result->DeleteChild();
  __tmp = Result->AddChild("text");
  __tmp->AddAttr("name",TextED->Text);
  __tmp->AddAttr("value",TextED->Text);
  __tmp->AddAttr("ref","0");
  __tmp = Result->AddChild("text");
  __tmp->AddAttr("name",(CaseSensChB->Checked)?"{� ������ ��������}":"");
  __tmp->AddAttr("value",IntToStr((int)CaseSensChB->Checked));
  __tmp->AddAttr("ref","1");
  __tmp = Result->AddChild("text");
  __tmp->AddAttr("name",(FullWordChB->Checked)?"{����� �������}":"");
  __tmp->AddAttr("value",IntToStr((int)FullWordChB->Checked));
  __tmp->AddAttr("ref","2");
  __tmp = Result->AddChild("text");
  __tmp->AddAttr("name",CompTypeCB->Properties->Items->Strings[CompTypeCB->ItemIndex]);
  __tmp->AddAttr("value",IntToStr((int)CompTypeCB->ItemIndex));
  __tmp->AddAttr("ref","3");
  __tmp = Result->AddChild("text","name=sql");
  __tmp->AddAttr("value","");
  if (FullWordChB->Checked)
   {
     if (CaseSensChB->Checked)
      __tmp->AV["value"] = FFieldName + "='"+TextED->Text+"'";
     else
      __tmp->AV["value"] = "Upper("+FFieldName + ")='"+TextED->Text.UpperCase()+"'";
   }
  else
   {
     if (CaseSensChB->Checked)
      __tmp->AV["value"] = FFieldName + " LIKE '%"+TextED->Text+"%'";
     else
      __tmp->AV["value"] = "Upper(" + FFieldName + ") LIKE '%"+TextED->Text.UpperCase()+"%'";
   }
  __tmp->AddAttr("ref","4");
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TTextConForm::CompTypeCBPropertiesChange(TObject *Sender)
{
  TextED->Enabled      = (CompTypeCB->ItemIndex == 2);
  CaseSensChB->Enabled = (CompTypeCB->ItemIndex == 2);
  FullWordChB->Enabled = (CompTypeCB->ItemIndex == 2);
  if (CompTypeCB->ItemIndex != 2)
   {
     TextED->Text = "";
     CaseSensChB->Checked = false;
     FullWordChB->Checked = false;
   }
}
//---------------------------------------------------------------------------

