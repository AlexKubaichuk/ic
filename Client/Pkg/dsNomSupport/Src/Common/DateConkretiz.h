//---------------------------------------------------------------------------

#ifndef DateConkretizH
#define DateConkretizH

//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxCalendar.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDateUtils.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "dxCore.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
//---------------------------------------------------------------------------
#define _scError    0
#define _scDigit    1
#define _scChoice   2
#define _scDiapazon 3
#define _scDate     4
#define _scRDate    5
//---------------------------------------------------------------------------
class TDateConForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *MsgLab;
        TPanel *BtnPanel;
        TLabel *Label3;
        TGroupBox *FromGB;
        TGroupBox *ToGB;
        TLabel *FromFormatLab;
        TLabel *ToFormatLab;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TcxComboBox *ValueCB;
        TcxMaskEdit *FromED;
        TcxMaskEdit *ToED;
        TcxDateEdit *FromDE;
        TcxDateEdit *ToDE;
        TcxCheckBox *LastChB;
        TcxCheckBox *NotDop;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall ValueCBChange(TObject *Sender);
        bool __fastcall CheckInput();
        void __fastcall LastChBClick(TObject *Sender);
        void __fastcall FromEDPropertiesValidate(TObject *Sender,
          Variant &DisplayValue, TCaption &ErrorText, bool &Error);
private:	// User declarations
        int     EDType,EDSubType;
        bool    canValidateError;
public:		// User declarations
        TTagNode *Result;
        void __fastcall SetType(int AType,TTagNode *ANode, int ASubType, UnicodeString ASubTypeCaption, bool ANotDop = false);
        __fastcall TDateConForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TDateConForm *DateConForm;
//---------------------------------------------------------------------------
#endif
