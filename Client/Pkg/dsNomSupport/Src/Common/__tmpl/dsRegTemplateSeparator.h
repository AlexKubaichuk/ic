//---------------------------------------------------------------------------

#ifndef dsRegTemplateSeparatorH
#define dsRegTemplateSeparatorH
//---------------------------------------------------------------------------
class PACKAGE TdsRegTEmplateSeparator : public TPanel
{
private:
    TLabel *Lab;
    TBevel *ED;
public:
__published:
    __fastcall TdsRegTEmplateSeparator::TdsRegTEmplateSeparator (TComponent *AOwner, int AWidth, UnicodeString ACaption);
    __fastcall ~TdsRegTEmplateSeparator();
};
//---------------------------------------------------------------------------
#endif
