//---------------------------------------------------------------------------
#include <vcl.h>

#pragma hdrstop

#include "dsRegTemplateSeparator.h"
#include "AxeUtil.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall TdsRegTEmplateSeparator::TdsRegTEmplateSeparator (TComponent *AOwner, int AWidth, UnicodeString ACaption)
           :TPanel(AOwner)
{
  Lab = NULL;
  Width = AWidth-2;
  Left = 1;
  Parent = (TWinControl*)AOwner;

  Caption = "";
  Color = DrColor(clBtnFace,16);
  BevelInner = bvNone; BevelOuter = bvNone;
  Lab = new TLabel(this);
  Lab->Parent = this;
//  Lab->Transparent = true;
  Lab->Font->Color = clWindowText;
  Lab->Font->Style.Clear();
  Lab->Font->Style = TFontStyles() << fsBold;
  Lab->Top = -1; Lab->Left = 5;
  Lab->Caption = "";
  if (ACaption.Trim().Length())
   Lab->Caption = " "+ACaption+" ";
  Lab->Font->Color = clBlack;
  ED = new TBevel(this);
  ED->Parent = this;  ED->Top = 5;  ED->Left = -4;
  ED->Width = Width+20;
  ED->SendToBack();
  Height = 12;
}
//---------------------------------------------------------------------------
__fastcall TdsRegTEmplateSeparator::~TdsRegTEmplateSeparator()
{
  delete ED;
  if (Lab) delete Lab;
  ED  = NULL;
  Lab = NULL;
}
//---------------------------------------------------------------------------

