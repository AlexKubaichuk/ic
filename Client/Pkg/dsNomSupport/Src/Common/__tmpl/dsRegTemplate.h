//---------------------------------------------------------------------------

#ifndef dsRegTemplateH
#define dsRegTemplateH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Controls.hpp>
//#include <ExtCtrls.hpp>
//#include <Mask.hpp>
//#include <StdCtrls.hpp>
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "dsRegEd.h"
#include "dsRegEDContainer.h"
#include "dsRegEDTemplateDataProvider.h"
//---------------------------------------------------------------------------
class PACKAGE EdsRegTemplateError : public Exception
{
public:
     __fastcall EdsRegTemplateError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE TdsRegTemplate : public TObject
{
private:
  TColor FReqColor;
  bool FNativeStyle;
  TcxLookAndFeelKind FLFKind;
  bool FIsTemplateField;
  TWinControl *FCompPanel;

  TdsRegEDContainer *FCtrList;
  TdsRegEDTemplateDataProvider *FDataProvider;

  UnicodeString _ES_;
  TTagNode *FRegDef;
  TTagNode *FFltFl;
  TStringList *FTmplFieldList;
  TObjectList *FSepList;

        TTreeBtnClick FOnTreeBtnClick;
        TExtBtnClick  FOnExtBtnClick;
        TExtBtnClick  FGetExtWhere;
        void __fastcall FSetTreeBtnClick(TTreeBtnClick AOnTreeBtnClick);
        void __fastcall FSetExtBtnClick(TExtBtnClick AOnExtBtnClick);

        int      xTop,MaxW;

        bool __fastcall FPrepare();
        bool __fastcall CreateTmpl(TTagNode *itxTag, UnicodeString& tmp);
        bool __fastcall FSetDefaultValue(TTagNode *itxTag, UnicodeString& tmp);
        bool __fastcall FCheckInput(TTagNode *itxTag, UnicodeString& tmp);
        UnicodeString __fastcall FGetValue(UnicodeString AUID, bool ABinaryNames);
        bool __fastcall FGetSelectedTextValues(TTagNode *itxTag, UnicodeString& Values);
        bool __fastcall FGetSelectedTextFieldValues(TTagNode *itxTag, UnicodeString& Values);
        bool __fastcall FSetTreeFormList(TTagNode *itxTag, UnicodeString& tmp);
        bool __fastcall FSetExtClick(TTagNode *itxTag, UnicodeString& tmp);
        int FLastTop,FLastHeight;
        int FHeight, FWidth;
        int FlCounter;
        bool MS,FNextCol;
        void __fastcall FSetWidth(int AWidth);
public:   // User declarations
  __fastcall TdsRegTemplate(TTagNode *ARegDef);
  __fastcall ~TdsRegTemplate();

  void __fastcall CreateTemplate(UnicodeString AFields);

    bool __fastcall CheckInput();
    bool __fastcall IsEmpty();
    UnicodeString    __fastcall GetValue(UnicodeString AUID);
    UnicodeString    __fastcall GetName(UnicodeString AUID);
    UnicodeString    __fastcall GetSelectedTextValues(bool CanFieldName = false);
    TWinControl*  __fastcall GetFirstEnabledControl();
    void __fastcall Clear();

  __property int Height = {read=FHeight};
  __property int Width = {read=FWidth, write=FSetWidth};

  __property TColor ReqColor           = {read=FReqColor, write=FReqColor};
  __property bool NativeStyle          = {read=FNativeStyle, write=FNativeStyle};
  __property TcxLookAndFeelKind LFKind = {read=FLFKind, write=FLFKind};

  __property bool IsTemplateField = {read=FIsTemplateField, write=FIsTemplateField};
  __property TWinControl *TmplPanel = {read=FCompPanel, write=FCompPanel};

  __property TdsRegEDContainer *CtrList = {read=FCtrList};
  __property TdsRegEDTemplateDataProvider *DataProvider = {read=FDataProvider};

    __property TTreeBtnClick OnTreeBtnClick = {read=FOnTreeBtnClick, write=FSetTreeBtnClick};
    __property TExtBtnClick OnExtBtnClick = {read=FOnExtBtnClick, write=FSetExtBtnClick};
    __property TExtBtnClick OnGetExtWhere = {read=FGetExtWhere, write=FGetExtWhere};
};
//---------------------------------------------------------------------------
#endif
