//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop


//---------------------------------------------------------------------------
#include "dsRegTemplate.h"
#include "dsRegTemplateSeparator.h"
//#include "Reg_DMF.h"
#include "msgdef.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
/*
<!ELEMENT root (fl|gr|separator)+>            //������
<!ATTLIST root ref CDATA #IMPLIED>  // ref - uid �������������� ��� ��������
                                    // ����������� ������
                                    // ���� ref = NULL - ������ �����������
                                    // ��� ������� ����� �� �������� ������������

<!ELEMENT gr (fl|gr|separator)+>              // ������
<!ATTLIST gr ref CDATA #REQUIRED>   // ������ ��� �������� ����������� ����� ��
                                    // �������������� �� ������� ��������� �������
                                    // � uid-�� = ref, ������ ������� ����� ���� ������
                                    // ���� "choiceDB" - ������ ������ �� �������������

<!ELEMENT fl EMPTY>                 // ����
<!ATTLIST fl ref CDATA #REQUIRED    // �������� ���� �� �������� �������� ����������
             def CDATA #IMPLIED     // ��������� ����� ref - uid ��������
             show (0|1) "1"         // ���������� ��������� �������� � ???
             req (0|1) "0"          // �������������� ������� ��������
                            "0" - �� �����������
                            "1" - �����������
                            "2" - �������������� ������������ ��������� ��������
             en  (0|1) "1">         // def - �������� �� ���������
                                    // en - ����������� ��� ��������������

<!ELEMENT separator EMPTY>          // �����������

 AFields -  XML - �������� ����� �������
 ACompPanel - ������ ��� ���������� ��������� �������
 AquFree -  ��������� �� FIBQuery ��� ������� � ��
 ATreePath - ������ ���� � ����� �������� ���
 ATree - ��������� �� TTagNode - � ����������� ���������
*/
//---------------------------------------------------------------------------
__fastcall EdsRegTemplateError::EdsRegTemplateError(UnicodeString Msg) : Sysutils::Exception(Msg.c_str()) {};
//---------------------------------------------------------------------------
__fastcall TdsRegTemplate::TdsRegTemplate(TTagNode *ARegDef)
{
  FReqColor = clInfoBk;
  FNativeStyle = true;
  FLFKind = lfStandard;
  FIsTemplateField = false;
  FRegDef = ARegDef;
  FCompPanel = NULL;
  FFltFl = new TTagNode(NULL);
  FTmplFieldList  = new TStringList;
  FSepList = new TObjectList;
  FDataProvider = new TdsRegEDTemplateDataProvider;
  _ES_ = "";
}
//---------------------------------------------------------------------------
void __fastcall TdsRegTemplate::CreateTemplate(UnicodeString AFields)
{
  if (!AFields.Length())
   throw Exception("�� ����� ������ ����� �������.");
  FFltFl->AsXML = AFields;

  FHeight = 0;
  FWidth  = 0;
  FLastTop = 0;
  FLastHeight = 0;
  if (!FCompPanel)
   throw Exception("�� ����� ��������� ��� �������.");

  FTmplFieldList->Clear();
  FSepList->Clear();

  xTop = 2; MaxW = 20;

  FCtrList = new TdsRegEDContainer(FCompPanel->Owner, FCompPanel);
  FCtrList->DefXML = FRegDef;
  FCtrList->ReqColor = FReqColor;
  FCtrList->NativeStyle = FNativeStyle;
  FCtrList->LFKind = FLFKind;
  FCtrList->TemplateField = FIsTemplateField;
  FCtrList->DataProvider = FDataProvider;

  FDataProvider->DefXML = FRegDef;

  MS = false;
  int FlCount = FFltFl->GetCount(true, "fl,separator");
  if ((FlCount+2)*21 > Screen->Height/2)
   MS = true;
  FlCounter = 0;
  FNextCol = false;
  FFltFl->Iterate(CreateTmpl,_ES_);

  MaxW = max(MaxW, FCompPanel->Width - 5);

  for (int i = 0; i < FTmplFieldList->Count; i++)
   {
     FCtrList->TemplateItems[FTmplFieldList->Strings[i]]->Width = MaxW-10;
     FCtrList->TemplateItems[FTmplFieldList->Strings[i]]->UpdateChanges();
   }
  for (int i = 0; i < FSepList->Count; i++)
   {
     ((TdsRegTEmplateSeparator*)FSepList->Items[i])->Width = MaxW-10;
   }
  if (FTmplFieldList->Count)
   {
     FHeight = FLastTop + FLastHeight + 3;
     if (MS&&FNextCol)
      FWidth  = MaxW*2+10;
     else
      FWidth  = MaxW+5;

     FCompPanel->Height = FHeight;
     FCompPanel->Width =  FWidth;
     FFltFl->Iterate(FSetDefaultValue,_ES_);
   }
  else
   {
     FHeight = 0;
     FWidth  = 0;
   }
}
//---------------------------------------------------------------------------
__fastcall TdsRegTemplate::~TdsRegTemplate()
{
  delete FCtrList;
  delete FDataProvider;
  delete FTmplFieldList;
  delete FSepList;
  delete FFltFl;
}
//---------------------------------------------------------------------------
TWinControl* __fastcall TdsRegTemplate::GetFirstEnabledControl()
{
  TControl *tmpCtrl;
  TdsRegEDItem *tmpItem;
  TTagNode *itNode = FFltFl->GetFirstChild();
  while (itNode)
   {
     if (FCtrList->GetTemplateControl(itNode->AV["ref"]))
      {
        tmpItem = FCtrList->TemplateItems[itNode->AV["ref"]];
        tmpCtrl = tmpItem->GetFirstEnabledControl();
        if (tmpCtrl)
         {
           return (TWinControl*)tmpCtrl;
         }
      }
     itNode = itNode->GetNext();
   }
  return NULL;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::CreateTmpl(TTagNode *itxTag, UnicodeString& tmp)
{
  TdsRegEDItem *tmpItem;
  TTagNode   *FNode;


  FlCounter++;
  if (itxTag->CmpName("fl"))
   {
     FNode = FRegDef->GetTagByUID(itxTag->AV["ref"]);
     if (FNode)
      {
        if (FNode->CmpAV("isedit","no")&& !FNode->CmpAV("depend",""))
         {
           tmpItem = FCtrList->AddTemplateItem(FNode, itxTag->AV["def"]);
           if (FNode->CmpName("choiceDB"))
            tmpItem->SetEnableEx((bool)itxTag->AV["en"].ToIntDef(1));
           else
            tmpItem->SetEnable((bool)itxTag->AV["en"].ToIntDef(1));
           tmpItem->SetIsVal(true);
           FTmplFieldList->Add(FNode->AV["uid"]);
           tmpItem->Visible = false;
         }
        else
         {
           tmpItem = FCtrList->AddTemplateItem(FNode, itxTag->AV["def"]);
           if (FNode->CmpName("choiceDB"))
            tmpItem->SetEnableEx((bool)itxTag->AV["en"].ToIntDef(1));
           else
            tmpItem->SetEnable((bool)itxTag->AV["en"].ToIntDef(1));
           tmpItem->SetIsVal(true);
           if (MS&&FNextCol)
            tmpItem->Left = FCompPanel->Width+2;
           else
            tmpItem->Left = 2;
           tmpItem->Top = xTop;
           xTop += 2 + tmpItem->Height;
           MaxW = max(tmpItem->Width,MaxW);
           if (FLastTop < xTop) FLastTop = xTop;
           FLastHeight = tmpItem->Height;
           FTmplFieldList->Add(FNode->AV["uid"]);
         }
        if (itxTag->AV["req"].ToIntDef(0) == 1)
         tmpItem->Required = true;
        else if (!itxTag->AV["req"].ToIntDef(0))
         tmpItem->Required = false;
      }
     else
      throw EdsRegTemplateError((FMT1(icsTemplateRefError,itxTag->AV["ref"])).c_str());
   }
  else if (itxTag->CmpName("separator"))
   {
     TdsRegTEmplateSeparator *tmpSep = new TdsRegTEmplateSeparator(FCompPanel, FCompPanel->Width, itxTag->AV["comment"]);
     FSepList->Add(tmpSep);
     xTop += 3;
     tmpSep->Top = xTop;
     xTop += 12;
     if (MS&&FNextCol)
      tmpSep->Left = FCompPanel->Width+2;
     else
      tmpSep->Left = 3;

     if (FLastTop < xTop) FLastTop = xTop;
     FLastHeight = 4;
     if (MS && !FNextCol)
      {
        if ((FlCounter)*21 > Screen->Height/2)
         {
           xTop = 0;
           FNextCol = true;
         }
      }
   }
  return false;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::FSetDefaultValue(TTagNode *itxTag, UnicodeString& tmp)
{
  if (itxTag->CmpName("fl"))
   {
     if (itxTag->GetAttrByName("def"))
      {
        FCtrList->TemplateItems[itxTag->AV["ref"]]->SetValue(itxTag->AV["def"]);
      }
   }
  return false;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::FPrepare()
{  // ������ ��������� � xml
  bool IsEmpty = true;
  try
   {
     for (int i = 0; i < FTmplFieldList->Count; i++)
      {
        FCtrList->TemplateItems[FTmplFieldList->Strings[i]]->UpdateChanges();
        IsEmpty &= !FCtrList->IsTemplate(FTmplFieldList->Strings[i]);
      }
   }
  __finally
   {
   }
  return IsEmpty;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::CheckInput()
{
  bool RC = false;
  try
   {
     UnicodeString DDD = "1";
     FFltFl->Iterate(FCheckInput,DDD);
     if (DDD.Length())
      {
        if (FFltFl->CmpAV("blank","1"))
         RC = true;
        else
         {
           if (!IsEmpty())  RC = true;
           else
            {
              _MSG_ERRA(FMT(icsTemplateReqEmpty), FMT(icsErrorMsgCaption));
            }
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::FCheckInput(TTagNode *itxTag, UnicodeString& tmp)
{
  bool RC = false;
  try
   {
     if (itxTag->CmpName("fl") && itxTag->AV["req"].ToIntDef(0))
      RC = !FCtrList->TemplateItems[itxTag->AV["ref"]]->CheckReqValue(NULL);
     if (RC) tmp = "";
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegTemplate::Clear()
{
  for (int i = 0; i < FTmplFieldList->Count; i++)
   FCtrList->TemplateData->Value[FTmplFieldList->Strings[i]] = Variant::Empty();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplate::GetValue(UnicodeString AUID)
{
  return FGetValue(AUID.UpperCase(),false);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplate::GetName(UnicodeString AUID)
{
  TTagNode *xTag;
  xTag = FRegDef->GetTagByUID(AUID);
  if (xTag)
   {
     return xTag->AV["name"]+"="+FCtrList->TemplateItems[xTag->AV["uid"]]->GetValueStr();
   }
  else
   return "";
}
//---------------------------------------------------------------------------
void __fastcall TdsRegTemplate::FSetWidth(int AWidth)
{
  MaxW = AWidth - 5;
  for (int i = 0; i < FSepList->Count; i++)
   {
     ((TdsRegTEmplateSeparator*)FSepList->Items[i])->Width = MaxW+10;
   }
  for (int i = 0; i < FTmplFieldList->Count; i++)
   {
     FCtrList->TemplateItems[FTmplFieldList->Strings[i]]->Width = MaxW-10;
     FCtrList->TemplateItems[FTmplFieldList->Strings[i]]->UpdateChanges();
   }
  FWidth = AWidth;
  FCompPanel->Width = AWidth;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::IsEmpty()
{
  return FPrepare();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplate::GetSelectedTextValues(bool CanFieldName)
{
  UnicodeString RC = "";
  if (CanFieldName)
   FFltFl->Iterate(FGetSelectedTextFieldValues,RC);
  else
   FFltFl->Iterate(FGetSelectedTextValues,RC);
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::FGetSelectedTextValues(TTagNode *itxTag, UnicodeString& Values)
{
  if (itxTag->CmpName("fl"))
   {
     TTagNode *xTag;
     xTag = FRegDef->GetTagByUID(itxTag->AV["ref"]);
     if (xTag)
      {
        if (itxTag->AV["show"].ToIntDef(1))
         {
           if (FGetValue(itxTag->AV["ref"],true).Length())
            Values += FCtrList->TemplateItems[xTag->AV["uid"]]->GetValueStr(true) + "\n";
         }
      }
   }
  return false;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::FGetSelectedTextFieldValues(TTagNode *itxTag, UnicodeString& Values)
{
  if (itxTag->CmpName("fl"))
   {
     TTagNode *xTag;
     xTag = FRegDef->GetTagByUID(itxTag->AV["ref"]);
     if (xTag)
      {
        if (itxTag->AV["show"].ToIntDef(1))
         {
           if (FGetValue(itxTag->AV["ref"],true).Length())
            Values += xTag->AV["name"] + "#" + FCtrList->TemplateItems[xTag->AV["uid"]]->GetValueStr(true) + "\n";
         }
      }
   }
  return false;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplate::FGetValue(UnicodeString AUID, bool ABinaryNames)
{
  TTagNode *xTag;
  xTag = FRegDef->GetTagByUID(AUID);
  Variant iVal = FCtrList->TemplateData->Value[xTag->AV["uid"]];
  if (!iVal.IsEmpty())
   {
     if (xTag->CmpName("binary"))
      {
        if (ABinaryNames)
         return ((int)iVal)? xTag->AV["name"]:xTag->AV["invname"];
        else
         return UnicodeString(((int)iVal)? "1":"0");
      }
     else if (xTag->CmpName("text,choice,choiceDB,extedit,digit,choiceTree"))
      return VarToStr(iVal);
     else if (xTag->CmpName("date"))
      return TDateTime(VarToStr(iVal)).FormatString("dd.mm.yyyy");
     else return "";
   }
  else return "";
}
//---------------------------------------------------------------------------
void __fastcall TdsRegTemplate::FSetTreeBtnClick(TTreeBtnClick AOnTreeBtnClick)
{
   FOnTreeBtnClick = AOnTreeBtnClick;
   FFltFl->Iterate(FSetTreeFormList, _ES_);
}
//---------------------------------------------------------------------------
void __fastcall TdsRegTemplate::FSetExtBtnClick(TExtBtnClick AOnExtBtnClick)
{
   FOnExtBtnClick = AOnExtBtnClick;
   FFltFl->Iterate(FSetExtClick, _ES_);
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::FSetTreeFormList(TTagNode *itxTag, UnicodeString& tmp)
{
  if (itxTag->CmpName("fl"))
   {
     TTagNode *tmpNode = FRegDef->GetTagByUID(itxTag->AV["ref"]);
     if (tmpNode)
      {
        if (tmpNode->CmpName("choiceTree"))
        FCtrList->TemplateItems[tmpNode->AV["uid"]]->OnTreeBtnClick = FOnTreeBtnClick;
      }
   }
  return false;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::FSetExtClick(TTagNode *itxTag, UnicodeString& tmp)
{
  if (itxTag->CmpName("fl"))
   {
     TTagNode *tmpNode = FRegDef->GetTagByUID(itxTag->AV["ref"]);
     if (tmpNode)
      {
        if (tmpNode->CmpName("extedit"))
        FCtrList->TemplateItems[tmpNode->AV["uid"]]->OnExtBtnClick = FOnExtBtnClick;
      }
   }
  return false;
}
//---------------------------------------------------------------------------

