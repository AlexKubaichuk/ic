//---------------------------------------------------------------------------

#ifndef TextConcretizH
#define TextConcretizH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
//---------------------------------------------------------------------------
class TTextConForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TLabel *TextLab;
        TcxTextEdit *TextED;
        TcxCheckBox *FullWordChB;
        TcxCheckBox *CaseSensChB;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TcxComboBox *CompTypeCB;
        TLabel *Label1;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall CompTypeCBPropertiesChange(TObject *Sender);
private:	// User declarations
        TTagNode *Result;
        TAxeXMLContainer *FXMLList;
//        TAutoFunc_DM   *FDM;
public:		// User declarations
        __fastcall TTextConForm(TComponent* Owner, UnicodeString ACapt,TTagNode *ANode, TAxeXMLContainer *AXMLList);
};
//---------------------------------------------------------------------------
extern PACKAGE TTextConForm *TextConForm;
//---------------------------------------------------------------------------
#endif
