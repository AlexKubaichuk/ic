//---------------------------------------------------------------------------
#include <vcl.h>

#pragma hdrstop

#include "dsDocXMLUtils.h"
#include "dsDocSrvConstDef.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------
TTagNode* __fastcall GetPassport(TTagNode *ANode, bool AFromRoot)
{
  if (AFromRoot)
   return ANode->GetRoot()->GetChildByName("passport");
  else
   return ANode->GetChildByName("passport");
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetSpecName(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = FPassport->AV["mainname"];
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocName(TTagNode *ANode, bool AFromRoot)
{
  return GetSpecName(ANode, AFromRoot);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetSpecGUI(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = FPassport->AV["GUI"];
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocGUI(TTagNode *ANode, bool AFromRoot)
{
   return GetSpecGUI(ANode, AFromRoot);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetSpecPeriod(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = FPassport->GetAVDef("perioddef","0");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocPeriod(TTagNode *ANode, bool AFromRoot)
{
   return GetSpecPeriod(ANode, AFromRoot);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetSpecOwner(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = FPassport->AV["autor"];
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocOwner(TTagNode *ANode, bool AFromRoot)
{
   return GetSpecOwner(ANode, AFromRoot);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocSpecGUI(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = FPassport->AV["GUIspecificator"];
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall SetDocSpecGUI(TTagNode *ANode,UnicodeString AVal, bool AFromRoot)
{
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      FPassport->AV["GUIspecificator"] = AVal;
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall SetSpecName(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      FPassport->AV["mainname"] = AVal;
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall SetDocName(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
   SetSpecName(ANode, AVal, AFromRoot);
}
//---------------------------------------------------------------------------
void __fastcall SetSpecGUI(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      FPassport->AV["GUI"] = AVal;
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall SetDocGUI(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
   SetSpecGUI(ANode, AVal, AFromRoot);
}
//---------------------------------------------------------------------------
void __fastcall SetSpecPeriod(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      FPassport->AV["perioddef"] = AVal;
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall SetDocPeriod(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
   SetSpecPeriod(ANode, AVal, AFromRoot);
}
//---------------------------------------------------------------------------
void __fastcall SetSpecOwner(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      FPassport->AV["autor"] = AVal;
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall SetDocOwner(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
   SetSpecOwner(ANode, AVal, AFromRoot);
}
//---------------------------------------------------------------------------
int __fastcall SpecPeriodInt(UnicodeString APeriod)
{
  if (APeriod.ToIntDef(-1) != -1)
   {
     if (APeriod.ToIntDef(-1))    return 1;
     else                         return 0;
   }
  else                            return 0;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall SpecPeriodStr(int APeriod)
{
  if (APeriod) return "1";
  else         return "0";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall SpecPeriodRuStr(int APeriod)
{
  return UnicodeString((APeriod)?"�":"���")+"������������";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall DocPeriodRuStr(int APeriod)
{
  switch (APeriod)
   {
     case 5  : return FMT(&_dsDocExtFuncDocPeriodLocStr1);
     case 4  : return FMT(&_dsDocExtFuncDocPeriodLocStr2);
     case 3  : return FMT(&_dsDocExtFuncDocPeriodLocStr3);
     case 2  : return FMT(&_dsDocExtFuncDocPeriodLocStr4);
     case 1  : return FMT(&_dsDocExtFuncDocPeriodLocStr5);
     default : return FMT(&_dsDocExtFuncDocPeriodLocStr6);
   }
}
//---------------------------------------------------------------------------
void __fastcall SetDocPeriodVal(TTagNode *ANode, UnicodeString AFVal, UnicodeString ATVal, bool AFromRoot)
{
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      FPassport->AV["periodval"] = AFVal+"-"+ATVal;
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocPeriodValFrom(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = GetPart1(FPassport->AV["periodval"],'-').Trim();
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocPeriodValTo(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = GetPart2(FPassport->AV["periodval"],'-').Trim();;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocPeriodValStr(UnicodeString APerType, UnicodeString AValFrom, UnicodeString AValTo)
{
   UnicodeString RC = "";
   UnicodeString FFrDay,FFrMonth,FFrYear,FToDay,FToMonth,FToYear,FFrMonthFull,FToMonthFull;
   FFrDay = "";FFrMonth = "";FFrYear = "";FToDay = "";FToMonth = "";FToYear = "";FFrMonthFull = "";FToMonthFull = "";
   TDateTime tmpDate;
   if (TryStrToDate(AValFrom,tmpDate))
    {
      FFrDay       = StrToDate(AValFrom).FormatString("dd");
      FFrMonth     = StrToDate(AValFrom).FormatString("mm");
      FFrMonthFull = StrToDate(AValFrom).FormatString("mmmm");
      FFrYear      = StrToDate(AValFrom).FormatString("yyyy");
    }
   if (TryStrToDate(AValTo,tmpDate))
    {
      FToDay       = StrToDate(AValTo).FormatString("dd");
      FToMonth     = StrToDate(AValTo).FormatString("mm");
      FToMonthFull = StrToDate(AValTo).FormatString("mmmm");
      FToYear      = StrToDate(AValTo).FormatString("yyyy");
    }


   int FPerType;
   if (APerType.ToIntDef(-1) == -1)
    FPerType = SpecPeriodInt(APerType);
   else
    FPerType = APerType.ToInt();
   if (FPerType == 0)
    {//"�����������"
      return "";
    }
   else if (FPerType == 1)
    {//"����������"
      if (AValFrom.Length() && AValTo.Length())
       {
         RC =  FMT6(&_dsDocExtFuncDocPeriodValStrUniq1,
                      FFrDay,FFrMonth,FFrYear,FToDay,FToMonth,FToYear);
       }
      else if (AValFrom.Length())
       RC =  FMT3(&_dsDocExtFuncDocPeriodValStrUniq2,
                    FFrDay,FFrMonth,FFrYear);
      else if (AValTo.Length())
       RC =  FMT3(&_dsDocExtFuncDocPeriodValStrUniq3,
                    FToDay,FToMonth,FToYear);
    }
   else if (FPerType == 2)
    {//"�����"
      RC =  FMT2(&_dsDocExtFuncDocPeriodValStrMonth, FFrMonthFull,FFrYear);
    }
   else if (FPerType == 3)
    {//"�������"
      int quarter = StrToDate(AValFrom).FormatString("mm").ToInt();
      if ((quarter >=1)&&(quarter<=3))
       RC =  FMT1(&_dsDocExtFuncDocPeriodValStrQuart1, FFrYear);
      else if ((quarter >=4)&&(quarter<=6))
       RC =  FMT1(&_dsDocExtFuncDocPeriodValStrQuart2, FFrYear);
      else if ((quarter >=7)&&(quarter<=9))
       RC =  FMT1(&_dsDocExtFuncDocPeriodValStrQuart3, FFrYear);
      else if ((quarter >=10)&&(quarter<=12))
       RC =  FMT1(&_dsDocExtFuncDocPeriodValStrQuart4, FFrYear);
    }
   else if (FPerType == 4)
    {//"���������"
      int quarter = StrToDate(AValFrom).FormatString("mm").ToInt();
      if ((quarter >=1)&&(quarter<=6))
       RC =  FMT1(&_dsDocExtFuncDocPeriodValStrHalfYear1, FFrYear);
      else if ((quarter >=7)&&(quarter<=12))
       RC =  FMT1(&_dsDocExtFuncDocPeriodValStrHalfYear2, FFrYear);
    }
   else if (FPerType == 5)
    {//"���"
      RC =  FMT1(&_dsDocExtFuncDocPeriodValStrYear, FFrYear);
    }
   else
    RC = "";
   return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetGUI(TTagNode *ANode)
{
  return ANode->GetRoot()->GetChildByName("passport")->AV["gui"];
}
//---------------------------------------------------------------------------
TDateTime __fastcall TSToDate(UnicodeString ATS)
{//�������������� timestamp to datetime
   try
    {
      if (ATS != "")  return StrToDate(ATS.SubString(7,2)+"."+ATS.SubString(5,2)+"."+ATS.SubString(1,4));
      else            return NULL;
    }
   catch (...)
    {
      return NULL;
    }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall dsDocCorrectCount(int ACount)
{
 UnicodeString RC;
 RC = IntToStr(ACount);
 int xpCount = StrToInt(RC[RC.Length()]);
 RC = IntToStr(ACount)+" ";
 if ((ACount == 0)||((ACount > 4)&&(ACount < 21))||((ACount > 20)&&(xpCount > 4)))
  RC += FMT(&_dsDocExtFuncRecCount);
 else
  {
    if (xpCount == 1)                    RC += FMT(&_dsDocExtFuncRecCount1);
    else if (xpCount > 1 && xpCount < 5) RC += FMT(&_dsDocExtFuncRecCount234);
    else                                 RC += FMT(&_dsDocExtFuncRecCount1);
  }
 return RC;
}
//---------------------------------------------------------------------------

