//---------------------------------------------------------------------------
#ifndef dsNomCommonH
#define dsNomCommonH

#include <System.Classes.hpp>
//#include <FireDAC.Comp.Client.hpp>
#include "XMLContainer.h"
#include "dsRegTypeDef.h"
#include "dsRegEDFunc.h"
//#include "icsLog.h"
//#include "NomTreeClass.h"
//!!!#include "KLADR.h"

#define _scError    0
#define _scDigit    1
#define _scChoice   2
#define _scDiapazon 3
#define _scDate     4
#define _scRDate    5

//#include "datadef.h"

#define AsTextDate AsDateTime.FormatString("dd.mm.yyyy")
//!!!#include "ICSClassData.h"
//---------------------------------------------------------------------------
class PACKAGE TdsDocTreeMapNode : public TObject
{
private:
    UnicodeString FItemExtCode, FItemName;
    UnicodeString __fastcall FGetAsExtCodeName();
public:
    __fastcall TdsDocTreeMapNode(UnicodeString AExtCode, UnicodeString AName);
    __fastcall ~TdsDocTreeMapNode();

    __property UnicodeString AsName = {read=FItemName};
    __property UnicodeString AsExtCodeName = {read=FGetAsExtCodeName};
    __property UnicodeString AsExtCode = {read=FItemExtCode};
};
//---------------------------------------------------------------------------
//!!!typedef map<UnicodeString,TdsDocTreeMapNode*> TdsTreeItemMap;
//!!!typedef map<UnicodeString, TdsTreeItemMap*> TdsTreeClassMap;
//---------------------------------------------------------------------------
class PACKAGE TdsNomCommon : public TObject
{
private:	// User declarations
  TdsGetValByIdEvent  FOnGetValById;
  TdsGetClassXMLEvent FOnGetClassXML;
  TExtBtnClick FExtBtnClick;
  TcxEditStyleController *FStyleController;

//       bool __fastcall GetTreeCh(void *uiTag, UnicodeString &UID);
//!!!    TdsRegClassData *FClassData;
    UnicodeString FRegGUI;
  UnicodeString  FXMLPath;
  UnicodeString  FDataPath;

    UnicodeString FMainLPUCode;
    __int64 FLPUCode;

    //!!!    TFDQuery *FQuery;
    TAxeXMLContainer *FXMLList;
    TTagNode *FReg;
//!!!    TKLADRFORM *FKLADR;
//    TClassValuesMap   FClassValues;
//    TLongMap* __fastcall GetClassHash(UnicodeString AClassUID);
  UnicodeString __fastcall kabNSGetClassXML(UnicodeString AId);
public:		// User declarations
    __fastcall TdsNomCommon(TAxeXMLContainer *AXMLList, UnicodeString ARegGUI);
    __fastcall ~TdsNomCommon();
  __property TcxEditStyleController *StyleController = {read=FStyleController,write=FStyleController};

//!!!    void __fastcall qtExec(UnicodeString ASQL);
    void       __fastcall ClearHash();
//    void       __fastcall ExecProc(UnicodeString AProcName, TpFIBQuery *AQuery, UnicodeString AParamName, Variant AParam);
    UnicodeString __fastcall GetClassData(long ACode, UnicodeString AName);

    bool __fastcall DefRegValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE, UnicodeString ATemplate, UnicodeString AGUI,bool AisAll, bool AIsSubSel);
    bool __fastcall DefChVal(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE, TStringList *AValues, TStringList *ACodeValues = NULL);
  void __fastcall OnGetXML(UnicodeString AId, TTagNode *& ARC);

    __property UnicodeString RegGUI    = {read=FRegGUI};
//!!!    __property TFDQuery *Query    = {read=FQuery};
    __property UnicodeString MainLPUCode = {read=FMainLPUCode, write=FMainLPUCode};
    __property TTagNode *Reg = {read=FReg};

    __property TAxeXMLContainer* XMLList = {read=FXMLList};

  __property __int64 LPUCode   = {read=FLPUCode,write=FLPUCode};

  __property TdsGetValByIdEvent OnGetValById   = {read=FOnGetValById,write=FOnGetValById};
  __property TdsGetClassXMLEvent OnGetClassXML = {read=FOnGetClassXML,write=FOnGetClassXML};
  __property TExtBtnClick          OnExtBtnClick        = {read=FExtBtnClick, write=FExtBtnClick};
  __property UnicodeString       XMLPath = {read=FXMLPath, write=FXMLPath};
  __property UnicodeString       DataPath = {read=FDataPath, write=FDataPath};
//!!!    __property TKLADRFORM *KLADR    = {read=FKLADR};
};
//---------------------------------------------------------------------------
/*
extern void __fastcall GetTreeValues(TTagNode *ANode, TStringList *AList, int *Count);
*/

//extern PACKAGE TdsNomCommon *AutoFunc_DM;
//---------------------------------------------------------------------------
#endif

