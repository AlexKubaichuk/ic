//---------------------------------------------------------------------------

#ifndef InqConH
#define InqConH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxCalendar.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDateUtils.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "dxCore.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
//---------------------------------------------------------------------------
#define _scError    0
#define _scDigit    1
#define _scChoice   2
#define _scDiapazon 3
#define _scDate     4
#define _scRDate    5
//---------------------------------------------------------------------------
class TConForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *MsgLab;
        TPanel *BtnPanel;
        TGroupBox *FromGB;
        TGroupBox *ToGB;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TcxComboBox *ValueCB;
        TcxDateEdit *FromDE;
        TcxDateEdit *ToDE;
        TcxTextEdit *FromED;
        TcxTextEdit *ToED;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall ValueCBChange(TObject *Sender);
private:	// User declarations
        int     EDType;
        TAxeXMLContainer *FXMLList;
//        TAutoFunc_DM   *FDM;
        TStringList *FCodeValues;
public:		// User declarations
        TTagNode *Result;
        void __fastcall SetType(int AType,TTagNode *ANode);
        __fastcall TConForm(TComponent* Owner, TAxeXMLContainer *AXMLList, TStringList *ACodeValues = NULL);
};
//---------------------------------------------------------------------------
extern PACKAGE TConForm *ConForm;
//---------------------------------------------------------------------------
#endif
