//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "InqCon.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxCalendar"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDateUtils"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "dxCore"
#pragma resource "*.dfm"
TConForm *ConForm;
//---------------------------------------------------------------------------
__fastcall TConForm::TConForm(TComponent* Owner, TAxeXMLContainer *AXMLList, TStringList *ACodeValues)
        : TForm(Owner)
{
  FXMLList = AXMLList;
  FCodeValues = ACodeValues;
}
//---------------------------------------------------------------------------
void __fastcall TConForm::SetType(int AType,TTagNode *ANode)
{
  EDType = AType;
  Result = ANode;
  if ((EDType == _scChoice)||(EDType == _scDiapazon))
   {
     FromGB->Enabled = false;
     ToGB->Enabled   = false;

     FromED->Enabled = false;
     ToED->Enabled   = false;

     FromDE->Enabled = false;
     ToDE->Enabled   = false;

     if (Result->GetFirstChild())
      {
        int idx = -1;
        if (FCodeValues)
         {
           idx = FCodeValues->IndexOf(Result->GetFirstChild()->AV["value"].UpperCase());
           if (idx != -1)
            idx = (int)ValueCB->Properties->Items->Objects[idx];
         }
        else
         idx = Result->GetFirstChild()->AV["value"].ToInt();
        idx = ValueCB->Properties->Items->IndexOfObject((TObject*)idx);
        ValueCB->ItemIndex = idx;
      }
   }
  else if (EDType == _scDigit)
   {
     ValueCB->Enabled = false;

     FromDE->Enabled = false;
     ToDE->Enabled   = false;

     TTagNode *__tmp = Result->GetFirstChild();
     if (__tmp)
      {
        TReplaceFlags rFlag;
        rFlag << rfReplaceAll;
        UnicodeString FVal = StringReplace(__tmp->AV["value"], ",", FormatSettings.DecimalSeparator, rFlag);
        FVal = StringReplace(FVal, ".", FormatSettings.DecimalSeparator, rFlag);

        FromED->Text = FVal;
        __tmp = __tmp->GetNext();
        FVal = StringReplace(__tmp->AV["value"], ",", FormatSettings.DecimalSeparator, rFlag);
        FVal = StringReplace(FVal, ".", FormatSettings.DecimalSeparator, rFlag);
        ToED->Text   = FVal;
      }
   }
  else if (EDType == _scDate)
   {
     FromED->Enabled = false;
     ToED->Enabled   = false;

     ValueCB->Clear();
     ValueCB->Properties->Items->Add("��������� ������������");
     ValueCB->Properties->Items->Add("�������� ���");
     ValueCB->Properties->OnChange = ValueCBChange;
     if (Result->Count == 2)
      {
        ValueCB->ItemIndex = 1;
        TTagNode *__tmp = Result->GetFirstChild();
        if (__tmp->AV["value"] != "")
         FromDE->Text = __tmp->AV["value"];
        __tmp = __tmp->GetNext();
        if (__tmp->AV["value"] != "")
         ToDE->Text   = __tmp->AV["value"];
      }
   }
  else if (EDType == _scRDate)
   {
     ValueCB->Enabled = false;

     FromED->Enabled = false;
     ToED->Enabled   = false;

     TTagNode *__tmp = Result->GetFirstChild();
     if (__tmp)
      {
        FromDE->Text = __tmp->AV["value"];
        __tmp = __tmp->GetNext();
        ToDE->Text   = __tmp->AV["value"];
      }
   }
  else
   OkBtn->Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall TConForm::OkBtnClick(TObject *Sender)
{
  TTagNode *FISCondRules = FXMLList->GetRefer(Result->AV["ref"]);
  TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
  TTagNode *FExtAttr = NULL;
  if (FISAttr->AV["ref"] != "")
   FExtAttr = FXMLList->GetRefer(FISAttr->AV["ref"]);
  TTagNode *FIS = FISAttr->GetParent("is");
  TTagNode *__tmp;
  UnicodeString FFieldName = FIS->AV["tabalias"]+"."+FISAttr->AV["fields"];
//  UnicodeString FFieldName = "REF1_M."+FISAttr->AV["fields"];
  if ((EDType == _scChoice)||(EDType == _scDiapazon))
   {
     if (ValueCB->ItemIndex < 0)
      {
        _MSG_ERRA("�� ������� ��������","������");
        ActiveControl = ValueCB;
        return;
      }
     else
      {
        if (Result->Count) Result->DeleteChild();
        TTagNode *__tmp = Result->AddChild("text");
        __tmp->AV["name"] = ValueCB->Properties->Items->Strings[ValueCB->ItemIndex];
        if (EDType == _scChoice)
         {
           UnicodeString tmpVal;
           if (FCodeValues)
            {
              tmpVal = FCodeValues->Strings[ValueCB->ItemIndex].UpperCase();
              __tmp->AV["value"] = tmpVal;
              __tmp = Result->AddChild("text","name=sql");
              __tmp->AV["value"] = " Upper("+FFieldName+")='"+tmpVal+"' ";
/*
        int idx = -1;
        if (FCodeValues)
         {
           int idx = FCodeValues->IndexOf(Result->GetFirstChild()->AV["value"].UpperCase());
           if (idx != -1)
            idx = (int)ValueCB->Properties->Items->Objects[idx];
         }
        else
         idx = Result->GetFirstChild()->AV["value"].ToInt();
        ValueCB->ItemIndex = ValueCB->Properties->Items->IndexOfObject((TObject*)idx);
*/

            }
           else
            {
              tmpVal = IntToStr((int)ValueCB->Properties->Items->Objects[ValueCB->ItemIndex]);
              __tmp->AV["value"] = tmpVal;
              __tmp = Result->AddChild("text","name=sql");
              __tmp->AV["value"] = " "+FFieldName+"="+tmpVal+" ";
            }
         }
        else
         {
           __tmp->AV["value"] = IntToStr(ValueCB->ItemIndex);
           __tmp = Result->AddChild("text","name=sql");
           if (FExtAttr)
            {
/* //!!! AKDO

              TTagNode *Scale = FExtAttr->GetTagByUID(FDM->GetScaleUID(FExtAttr));
              int xMin,xMax;
              UnicodeString xVal,xVal1;
              xMin = Scale->AV["min"].ToInt();
              xMax = Scale->AV["max"].ToInt();
              Scale = Scale->GetFirstChild();
              int xxInd = 0;
              while (Scale)
               {
                 xVal = Scale->AV["value"];
                 if (xxInd == ValueCB->ItemIndex)
                  {
                    if (xVal == "<<")
                     {
                       __tmp->AV["value"] = " "+FFieldName+"<"+IntToStr(xMin)+" ";
                     }
                    else if (xVal == ">>")
                     {
                       __tmp->AV["value"] = " "+FFieldName+">"+IntToStr(xMax)+" ";
                     }
                    else
                     {
                       xVal1 = Scale->GetPrev()->AV["value"];
                       if (xVal1 == "<<") xVal1 = FFieldName+">="+IntToStr(xMin);
                       else               xVal1 = FFieldName+">"+xVal1;
                       __tmp->AV["value"] = "("+xVal1+" and "+FFieldName+"<="+xVal+")";
                     }
                    break;
                  }
                 Scale = Scale->GetNext(); xxInd++;
               }
*/
            }
         }
      }
   }
  else if (EDType == _scDigit)
   {
      if ((FromED->Text == "")&&(ToED->Text == ""))
       {
         _MSG_ERRA("�� ������� ��������","������");
         ActiveControl = FromED;
         return;
       }
      else
       {
         if (Result->Count) Result->DeleteChild();
         TTagNode *__tmp = Result->AddChild("text");
         __tmp->AV["name"] = "��:"+FromED->Text;
         __tmp->AV["value"] = FromED->Text;

         __tmp = Result->AddChild("text");
         __tmp->AV["name"] = "��:"+ToED->Text;
         __tmp->AV["value"] = ToED->Text;

         TReplaceFlags rFlag;
         rFlag << rfReplaceAll;
         UnicodeString FFromVal, FToVal;

         FFromVal = StringReplace(FromED->Text.Trim(), FormatSettings.DecimalSeparator, ".", rFlag);
         FFromVal = StringReplace(FFromVal, ",", ".", rFlag);

         FToVal = StringReplace(ToED->Text.Trim(), FormatSettings.DecimalSeparator, ".", rFlag);
         FToVal = StringReplace(FToVal, ",", ".", rFlag);

         __tmp = Result->AddChild("text","name=sql");
         if (FFromVal.Length() && FToVal.Trim().Length())
           __tmp->AV["value"] = " "+FFieldName+">="+FFromVal+" and "+FFieldName+"<"+FToVal;
         else if (FFromVal.Length())
           __tmp->AV["value"] = " "+FFieldName+">="+FFromVal;
         else if (FToVal.Length())
           __tmp->AV["value"] = " "+FFieldName+"<"+FToVal;
       }
   }
  else
   return;
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TConForm::ValueCBChange(TObject *Sender)
{
  if (EDType == _scDate)
   {
     if (ValueCB->ItemIndex == 1)
      {
        FromDE->Enabled = true;
        ToDE->Enabled   = true;
      }
     else
      {
        FromDE->Enabled = false;
        ToDE->Enabled   = false;
      }
   }
}
//---------------------------------------------------------------------------

