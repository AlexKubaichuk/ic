object RegConForm: TRegConForm
  Left = 307
  Top = 355
  HelpContext = 4012
  BorderIcons = []
  BorderStyle = bsDialog
  BorderWidth = 3
  Caption = #1054#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1103' ...'
  ClientHeight = 279
  ClientWidth = 395
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  GlassFrame.Bottom = 45
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object CompPanel: TPanel
    Left = 0
    Top = 1
    Width = 394
    Height = 220
    BevelOuter = bvNone
    BorderWidth = 3
    ParentBackground = False
    TabOrder = 0
  end
  object BtnPanel: TPanel
    Left = 0
    Top = 239
    Width = 395
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      395
      40)
    object OkBtn: TcxButton
      Left = 228
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      TabOrder = 0
      OnClick = OkBtnClick
    end
    object CancelBtn: TcxButton
      Left = 311
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
end
