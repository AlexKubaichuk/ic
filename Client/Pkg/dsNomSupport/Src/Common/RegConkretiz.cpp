//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "RegConkretiz.h"
//#include "NomTreeClass.h"
//#include "icsOrgEdit.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma resource "*.dfm"
TRegConForm *RegConForm;
//---------------------------------------------------------------------------
__fastcall TRegConForm::TRegConForm(TdsNomCommon *ADM, UnicodeString ATemplXML, TTagNode *ParamRoot, UnicodeString ARootGUI, bool AisAll, bool isNeedDef, bool AIsSubSel)
        : TForm(Application->Owner)
{
  FDM = ADM;
//  FXMLList = AXMLList;
  FIsSubSel = AIsSubSel;
  IsAll = AisAll;
  Result = ParamRoot;
  FTempXML = new TTagNode(NULL);
  FTempXML->AsXML = ATemplXML;
  UnicodeString tagNum = "0";
  FTemplateData = new TdsRegTemplateData;
  FTempXML->Iterate(FSetValues,tagNum);
//  TmplNode = new TTagNode;
//  FRootNode = FDM->XMLList->GetXML(ARootGUI);

//  TmplNode->Assign(FRootNode,true);
  TmplNode = FDM->XMLList->GetXML(ARootGUI);
  TTagNode *FLPUNode = FTempXML->GetChildByAV("fl", "ref", "00F1");
  if (FLPUNode)
   {
     if (!IsTemplate("00F1"))
      FTemplateData->Value["00F1"] = FDM->LPUCode;
   }

  FTempl = new TdsRegTemplate(TmplNode);
  FTempl->StyleController = FDM->StyleController;
  FTempl->TmplPanel = CompPanel;
  FTempl->DataProvider->OnGetValById  = FDM->OnGetValById;
  FTempl->DataPath      = FDM->DataPath;
  FTempl->OnExtBtnClick = FDM->OnExtBtnClick;
//  FTempl->DataProvider->OnGetCompValue  = FDM->OOnGetValById;
  FTempl->DataProvider->OnGetClassXML = FDM->OnGetXML;

  FTempl->CreateTemplate(FTempXML->AsXML, FTemplateData);
  FTempl->OnKeyDown            = FormKeyDown;

//!!!  FTempl->OnGetExtWhere = FExtGetWhere;
  CompPanel->Height = CompPanel->Height+5;
  Height = CompPanel->Height+(Height-ClientHeight)+BtnPanel->Height;
  Width = CompPanel->Width+6;
}
//---------------------------------------------------------------------------
void __fastcall TRegConForm::OkBtnClick(TObject *Sender)
{
  ActiveControl = OkBtn;
  if (!FTempl->CheckInput()) return;
  Result->DeleteChild();
  if (FTempl->IsEmpty())
   {
     if (IsAll)
      {
        Result->AddChild("text","name=���,value=1");
        Result->AddChild("text","name=sql,value=1=1");
      }
     else
      {
        _MSG_ERR("������ ������", "������");
        return;
      }
   }
  else
   {
     TTagNode *__tmp, *__tmp2;
     UnicodeString FlList = "";
     FTempXML->Iterate(GetFl,FlList);
     TStringList *FFlList = NULL;
     try
      {
        FFlList = new TStringList;
        FFlList->Text = FlList;
        for (int i = 0; i < FFlList->Count; i++)
         {
           if (FTempl->GetValue(FFlList->Strings[i]).Length())
            {
              __tmp = Result->AddChild("text");
              __tmp->AddAttr("name",FTempl->GetName(FFlList->Strings[i]));
              __tmp->AddAttr("value",FTempl->GetValue(FFlList->Strings[i]));
              __tmp->AddAttr("ref",IntToStr(i));
              __tmp2 = TmplNode->GetTagByUID(FFlList->Strings[i]);
              if (__tmp2)
               {
                 if (__tmp2->CmpName("extedit"))
                  {
                    if (__tmp2->CmpAV("isedit","no")&& !__tmp2->CmpAV("depend",""))
                     __tmp->AddAttr("show","0");
                  }
               }
            }
         }
      }
     __finally
      {
        if (FFlList) delete FFlList;
      }
     if (!FTempl->IsEmpty())
      {
        __tmp = Result->AddChild("text","name=sql");
        UnicodeString FTabAlias = "";
        TTagNode *NomTag = FDM->XMLList->GetRefer(Result->AV["ref"]);
        if (NomTag)
         {
           NomTag = NomTag->GetTagByUID(NomTag->AV["param"]);
           if (NomTag)
            {
              NomTag = NomTag->GetParent("is");
              if (NomTag)
               FTabAlias = NomTag->AV["tabalias"];
            }
         }
/*
        if (FIsSubSel)
         {
           TStringList *tmpSelList = new TStringList;
           __tmp->AddAttr("value",FTempl->GetWhereClRep(FTabAlias,tmpSelList));
           for (int i = 0; i < tmpSelList->Count; i++)
            __tmp->AddChild("subtext")->AddAttr("value",tmpSelList->Strings[i]);
           delete tmpSelList;
         }
        else
         __tmp->AddAttr("value",FTempl->GetWhere(FTabAlias));
*/
      }
   }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
bool __fastcall TRegConForm::GetFl(TTagNode *itTag, UnicodeString &UIDs)
{
  if (itTag->CmpName("fl"))
   UIDs += itTag->AV["ref"]+"\n";
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TRegConForm::FormDestroy(TObject *Sender)
{
//  FTempl->TreeFormList = NULL;
  delete FTempl;
  delete FTempXML;
//  delete TmplNode;
  delete FTemplateData;
}
//---------------------------------------------------------------------------
void __fastcall TRegConForm::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if (Key == VK_RETURN) {OkBtnClick(OkBtn); Key = 0;}
}
//---------------------------------------------------------------------------
bool __fastcall TRegConForm::FSetValues(TTagNode *itTag, UnicodeString &TagNum)
{
  if (itTag->CmpName("fl"))
   {
     TTagNode *sText = Result->GetChildByAV("text","ref",TagNum);
     if (sText)
      FTemplateData->Value[itTag->AV["ref"]] = sText->AV["value"].Trim();
     TagNum = IntToStr(TagNum.ToInt()+1);
   }
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TRegConForm::FOnGetOrgList(bool ATemplate, long AGroup, TStringList *ADest)
{
/*  //!!!
  if (AGroup != -1)
   FDM->qtExec("Select CODE, R0008 from class_0001 Where R011A="+IntToStr(AGroup), false);
  else
   FDM->qtExec("Select CODE, R0008 from class_0001", false);
  ADest->Clear();
  if (ATemplate)
   ADest->AddObject("",(TObject*)0);
  while (!FDM->quFreeA->Eof)
   {
     ADest->AddObject(FDM->quFreeA->FN("R0008")->AsString,(TObject*)FDM->quFreeA->FN("CODE")->AsInteger);
     FDM->quFreeA->Next();
   }
  if (FDM->quFreeA->Transaction->Active) FDM->quFreeA->Transaction->Commit();
*/
}
//---------------------------------------------------------------------------
bool __fastcall TRegConForm::FExtGetWhere(TTagNode *ItTag, UnicodeString &Src,
      TDataSource *ASource, bool isClicked, TObject *ACtrlList, int ABtnIdx)
{
  TdsRegEDContainer *CtrList = (TdsRegEDContainer*)ACtrlList;
  bool RC = false;
  try
   {
     UnicodeString FSrc = "";
     if (ItTag->CmpAV("uid","0062,0052,50AD,002A"))
      {// �����
        FSrc = Src.Trim();
        if (FSrc.Length() > 30) FSrc = FSrc.SubString(1,30);
        int i = -1;
        for (i = FSrc.Length(); i > 0; i--)
         {
           if (FSrc[i] != '0') break;
         }
        if (i != -1) FSrc = FSrc.SubString(1,i);
        if (FSrc.Length())
         {
           if (ItTag->CmpAV("uid","0062"))
            Src = "CB.R0062 starting with '"+FSrc+"'";
           else if (ItTag->CmpAV("uid","50AD"))
            Src = "CB.R50AD starting with '"+FSrc+"'";
           else if (ItTag->CmpAV("uid","002A"))
            Src = "CB.R002A starting with '"+FSrc+"'";
           else
            Src = "CL01.R0052 starting with '"+FSrc+"'";
           RC = true;
         }
      }
     else if (ItTag->CmpAV("uid","0041"))
      {// ����������� � ����
        FSrc = "";
        if ((CtrList->TemplateItems["0045"]->GetValue("").ToIntDef(0) > 0))
         {

           FSrc += SetValIfNotNull(CtrList->TemplateItems["0041"]->GetValue(""), "CB.R0041", FSrc.Length(), false);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0004"]->GetValue(""), "CB.R0004", FSrc.Length(), true);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0025"]->GetValue(""), "CB.R0025", FSrc.Length(), true);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0026"]->GetValue(""), "CB.R0026", FSrc.Length(), true);
           if ((CtrList->TemplateItems["0045"]->GetValue("").ToIntDef(0) == 1))
            FSrc += SetValIfNotNull(CtrList->TemplateItems["0005"]->GetValue(""), "CB.R0005", FSrc.Length(), true);
           else
            FSrc += SetValIfNotNull(CtrList->TemplateItems["0014"]->GetValue(""), "CB.R0014", FSrc.Length(), false);
         }
        if (FSrc.Trim().Length())
         {
           Src = "("+FSrc+")";
           RC = true;
         }
      }
     else if (ItTag->CmpAV("uid","0025"))
      {// ����������� � ��
        FSrc = "";
        if ((CtrList->TemplateItems["0025"]->GetValue("").ToIntDef(0) > 0))
         {
           FSrc += SetValIfNotNull(CtrList->TemplateItems["011B"]->GetValue(""), "CB.R011B", FSrc.Length(), false);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0025"]->GetValue(""), "CB.R0025", FSrc.Length(), false);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0026"]->GetValue(""), "CB.R0026", FSrc.Length(), true);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0027"]->GetValue(""), "CB.R0027", FSrc.Length(), true);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0028"]->GetValue(""), "CB.R0028", FSrc.Length(), true);
         }
        if (FSrc.Trim().Length())
         {
           Src = "("+FSrc+")";
           RC = true;
         }
      }
     else if (ItTag->CmpAV("uid","0043"))
      {// ����������� � ����
        FSrc = "";
        if ((CtrList->TemplateItems["0044"]->GetValue("").ToIntDef(0) > 0))
         {
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0043"]->GetValue(""), "CB.R0043", FSrc.Length(), false);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0015"]->GetValue(""), "CB.R0015", FSrc.Length(), true);
         }
        if (FSrc.Trim().Length())
         {
           Src = "("+FSrc+")";
           RC = true;
         }
      }
     else if (ItTag->CmpAV("uid","0115"))
      {// ����������� � ��
        FSrc = "";
        if ((CtrList->TemplateItems["0113"]->GetValue("").ToIntDef(0) > 0))
         {
           FSrc += SetValIfNotNull(CtrList->TemplateItems["011C"]->GetValue(""), "CB.R011C", FSrc.Length(), false);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0115"]->GetValue(""), "CB.R0115", FSrc.Length(), false);
           FSrc += SetValIfNotNull(CtrList->TemplateItems["0114"]->GetValue(""), "CB.R0114", FSrc.Length(), true);
         }
        if (FSrc.Trim().Length())
         {
           Src = "("+FSrc+")";
           RC = true;
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TRegConForm::SetValIfNotNull(UnicodeString AVal, UnicodeString AId,bool AAnd, bool AQuote)
{
  UnicodeString RC = "";
  try
   {
     if ((!AQuote&&(AVal.ToIntDef(0) > 0))||(AQuote && AVal.Trim().Length()))
      {
        if (AAnd) RC = " and";
        if (AQuote)
         RC += " Upper("+AId+")='"+AVal.Trim().UpperCase()+"'";
        else
         RC += " "+AId+"="+AVal.Trim();
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TRegConForm::IsTemplate(UnicodeString AId)
{
  return !(FTemplateData->Value[AId].IsEmpty() || FTemplateData->Value[AId].IsNull());
}
//---------------------------------------------------------------------------

