//---------------------------------------------------------------------------
#include <DateUtils.hpp>
#pragma hdrstop
#include "dsBaseNomSupport.h"
//#include "TextConcretiz.h"
#include "PackPeriod.h"

//---------------------------------------------------------------------------
//typedef map<const UnicodeString, UnicodeString> TStrMap;
//TStrMap HashSel;
//TStrMap ClassHashSel;
//---------------------------------------------------------------------------
#define flVal(a) AFields->Values[AFields->Names[a]]
//---------------------------------------------------------------------------
__fastcall TdsBaseNomSupport::TdsBaseNomSupport(TAxeXMLContainer *AXMLList, UnicodeString ARegGUI)
{
  FDM = new TdsNomCommon(AXMLList, ARegGUI);
}
//---------------------------------------------------------------------------
__fastcall TdsBaseNomSupport::~TdsBaseNomSupport()
{
  delete FDM;
}
//---------------------------------------------------------------------------
bool __fastcall TdsBaseNomSupport::GetDVal(UnicodeString AFuncName, TTagNode* ANode, UnicodeString &AText, bool ANeedKonkr, UnicodeString &AIEXML)
{
  bool RC = false;
  try
   {
     TGetDefFunc FFunc = GetDefMethodAddr(AFuncName);
     if (FFunc)
      {
        RC = FFunc(ANode, AText, ANeedKonkr, AIEXML);
      }
     else
      throw Exception(("������ ������ ������� � ������ '"+AFuncName+"'"));
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TGetDefFunc __fastcall TdsBaseNomSupport::GetDefMethodAddr(UnicodeString AFuncName)
{
  TGetDefFunc RC = NULL;

  if (AFuncName == "DefChoiceValue")         RC = DefChoiceValue;
  else if (AFuncName == "DefRegVal")         RC = DefRegVal;
  else if (AFuncName == "DefTextValue")      RC = DefTextValue;
  else if (AFuncName == "DefChoiceDBValue")  RC = DefChoiceDBValue;
  else if (AFuncName == "DefAgePeriodLast")  RC = DefAgePeriodLast;
  else if (AFuncName == "DefDatePeriodLast") RC = DefDatePeriodLast;
  else if (AFuncName == "DefExtEditValue")   RC = DefExtEditValue;

  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsBaseNomSupport::FGetGUI(TTagNode *ANode)
{
  return ANode->GetRoot()->GetChildByName("passport")->AV["gui"];
}
//---------------------------------------------------------------------------
/*
UnicodeString __fastcall TdsBaseNomSupport::GetValIfNotEmpty(UnicodeString APref, UnicodeString AVal, UnicodeString APostf)
{
  UnicodeString RC = "";
  try
   {
     if (AVal.Trim().Length() && (AVal.Trim().ToIntDef(0)!= -1) )
      RC = APref+AVal.Trim()+APostf;
   }
  __finally
   {
   }
  return RC;
}
*/
//---------------------------------------------------------------------------
/*
UnicodeString __fastcall TdsBaseNomSupport::SetDay(int AYear, int AMonth, int ADay )
{
  UnicodeString RC = "";
  try
   {
     UnicodeString sm = IntToStr(AYear);
     sm = sm.SubString(sm.Length(),1);
     if (((sm == "1")||(sm == "2")||(sm == "3")||(sm == "4"))&&!((AYear<15)&&(AYear>10))) sm = "�. ";
     else                                                    sm = "�. ";
     if (AYear)   RC += IntToStr(AYear)+sm;
     if (AMonth)  RC += IntToStr(AMonth)+"�. ";
     if (ADay)    RC += IntToStr(ADay)+"�.";
     if (!(AYear+AMonth+ADay)) RC += "0�.";
   }
  __finally
   {
   }
  return RC.Trim();
}
*/
//---------------------------------------------------------------------------
/*
UnicodeString __fastcall TdsBaseNomSupport::AgeStr(TDateTime ACurDate, TDateTime ABirthDay)
{
  UnicodeString RC = "";
  try
   {
     if (ACurDate < ABirthDay)
      {
        RC = "-";
      }
     else
      {
        Word FYear,FMonth,FDay;
        DateDiff(ACurDate, ABirthDay, FDay, FMonth, FYear);
        RC = SetDay(FYear,FMonth,FDay);
      }
   }
  __finally
   {
   }
  return RC.Trim();
}
*/
//---------------------------------------------------------------------------
/*
UnicodeString __fastcall TdsBaseNomSupport::AgeStrY(TDateTime ACurDate, TDateTime ABirthDay)
{
  UnicodeString RC = "";
  try
   {
     if (ACurDate < ABirthDay)
      {
        RC = "-";
      }
     else
      {
        float DDD = (int)ACurDate - ((int)ABirthDay)-2;
        unsigned short YY=0,MM=0,DD=0;
        (ACurDate-ABirthDay+1).TDateTime::DecodeDate(&YY,&MM,&DD);  MM--;
        RC = FloatToStrF((DDD/365.25),ffFixed,16,2)+" "+" �.";
      }
   }
  __finally
   {
   }
  return RC.Trim();
}
*/
//---------------------------------------------------------------------------
/*
void __fastcall TdsBaseNomSupport::FGetOrgParam(UnicodeString &ARet, UnicodeString AFlName)
{
  FDM->qtExec("Select "+AFlName+" From SUBORDORG Where Upper(CODE)='"+FDM->MainLPUCode.UpperCase()+"'");
  if (FDM->Query->RecordCount)
   ARet = FDM->Query->FieldByName(AFlName)->AsString;
  else
   ARet = "";
}
   */
//---------------------------------------------------------------------------
/*
UnicodeString __fastcall TdsBaseNomSupport::GetChildContAVDef(TTagNode *ANode, UnicodeString AVal, UnicodeString ADef)
{
  TTagNode *_tmp = ANode->GetFirstChild();
  UnicodeString _tmpAV;
  while (_tmp && !_tmp->CmpAV("name",AVal))
   {
     _tmp = _tmp->GetNext();
   }
  if (_tmp) return _tmp->GetAVDef("value",ADef);
  else      return ADef;
}
*/
//---------------------------------------------------------------------------
/*
UnicodeString __fastcall TdsBaseNomSupport::_GetPart2(UnicodeString ARef, char Sep)
{
  int pos = ARef.Pos(Sep);
  return ARef.SubString(pos+1,ARef.Length()-pos);
}
*/
//---------------------------------------------------------------------------
/*
void __fastcall TdsBaseNomSupport::DecodePeriod(UnicodeString AScr, int &AYear, int &AMonth, int &ADay)
{
  AYear  = GetPart1(AScr,'/').ToInt();
  AMonth = GetPart1(_GetPart2(AScr,'/'),'/').ToInt();
  ADay   = GetPart2(_GetPart2(AScr,'/'),'/').ToInt();
}
*/
//---------------------------------------------------------------------------
/*
UnicodeString __fastcall TdsBaseNomSupport::AddPeriod(UnicodeString AScr1, UnicodeString AScr2)
{
  int YY1,YY2,MM1,MM2,DD1,DD2;
  DecodePeriod(AScr1,YY1,MM1,DD1);
  DecodePeriod(AScr2,YY2,MM2,DD2);
  DD1 += DD2;             DD2 = 0;
  if (DD1>31) {DD1 -= 31; DD2 = 1;}
  MM1 += MM2 + DD2; MM2 = 0;
  if (MM1>12) {MM1 -= 12; MM2 = 1;}
  YY1 += YY2 + MM2;
  return IntToStr(YY1)+"/"+IntToStr(MM1)+"/"+IntToStr(DD1);
}
*/
//---------------------------------------------------------------------------
/*
UnicodeString __fastcall TdsBaseNomSupport::FIncPeriod(UnicodeString AFrDate,UnicodeString APer, int IsSub)
{
  TDateTime RC = TDate(0);
  try
   {
     int FYears, FMonths, FDays;
     DecodePeriod(APer, FYears, FMonths, FDays);
     if (IsSub)
      {
       FYears  *= -1;
       FMonths *= -1;
       FDays   *= -1;
      }
     RC = Dateutils::IncYear
     (
       Sysutils::IncMonth
       (
         Dateutils::IncDay(StrToDate(AFrDate),FDays),
         FMonths
       ),
       FYears
     );
   }
  __finally
   {
   }
  return RC.FormatString("dd.mm.yyyy");
}
*/
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsBaseNomSupport::FGetRefValue(TTagNode *ANode, UnicodeString ARef1,UnicodeString ARef2)
{
  if (ANode->GetParent("domain")->GetNext()) return ARef1;
  else                                       return ARef2;
}
//---------------------------------------------------------------------------
/*  //!!!
void __fastcall TdsBaseNomSupport::GetSQLValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet)
{
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   TTagNode *FIS = FISAttr->GetParent("is");
   UnicodeString ISTab = FGetGUI(FISCondRules)+"."+FIS->AV["uid"]+":";
   ATab = ISTab;
   if (!AIE->Count)
    {
      if (FIS->AV["tabalias"].Length())
       ARet = FIS->AV["tabalias"]+"."+FIS->AV["keyfield"]+"="+FIS->AV["tabalias"]+"."+FIS->AV["keyfield"];
      else
       ARet = FIS->AV["maintable"]+"."+FIS->AV["keyfield"]+"="+FIS->AV["maintable"]+"."+FIS->AV["keyfield"];
    }
   else
    {
      bool  IsExtSel = false;
      if (FISCondRules->GetChildByName("root"))
       IsExtSel = (bool)(FISCondRules->GetChildByName("root")->AV["extsel"].ToIntDef(0));
      TTagNode *_tmp = AIE->GetChildByAV("text","name","sql");
      if (_tmp)
       {
         if (IsExtSel)
          {
            if (_tmp->Count)
             {
               TReplaceFlags rFlag;
               rFlag << rfReplaceAll;
               TTagNode *subtmp;
               UnicodeString FSQL = _tmp->AV["value"];
               UnicodeString FSubSQL;
               int idx = 0;
               subtmp = _tmp->GetFirstChild();
               while (subtmp)
                {
                  UnicodeString SelRC = "";
                  FSubSQL = subtmp->AV["value"];
                  TStrMap::iterator rc = HashSel.find(FSubSQL);
                  if (rc == HashSel.end())
                   {
                     SelRC = "-1";
                     FDM->qtExec(FSubSQL);
                     while (!FDM->Query->Eof)
                      {
                        SelRC += ","+FDM->Query->FieldByName("CODE")->AsString;
                        FDM->Query->Next();
                      }
                     HashSel[FSubSQL] = SelRC;
                   }
                  FSQL = Sysutils::StringReplace(FSQL, "_SEL"+IntToStr(idx), HashSel[FSubSQL], rFlag);
                  idx++;
                  subtmp = subtmp->GetNext();
                }
               ARet = FSQL;
             }
            else
             ARet = _tmp->AV["value"];
          }
         else
          ARet = _tmp->AV["value"];
       }
      else      ARet = "";
    }
}
*/
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#            ��������� ��������� �������� �������������� ��������� (Def...)  #
//#                                                                            #
//#            ��������� ������������ ������ SQL-�������             (SQL...)  #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
bool __fastcall TdsBaseNomSupport::DefChoiceValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   if (!FDM) return false;
   UnicodeString tmp = "";
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   if (NeedKonkr)
    {
      tmp = "<root><fl ref='"+_UID(FISAttr->AV["ref"])+"'/></root>";
    }


   return FDM->DefRegValue(AIE, AText, NeedKonkr, AXMLIE, tmp, _GUI(FISAttr->AV["ref"]), false, false);
}
//---------------------------------------------------------------------------
bool __fastcall TdsBaseNomSupport::DefAgePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   // AIE - ������, ������ �� IE - ����� ����� 2-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "�������"
   // {
   //   1-� - ������� ��;
   //   2-� - ������� ��;
   // }
   if (!FDM) return false;
   bool iRC = false;
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (NeedKonkr)
    {
      TDateConForm *Dlg = NULL;
      try
       {
         Dlg = new TDateConForm(Application->Owner);
         int FSubType;
         Dlg->SetType(_scRDate,AIE,FSubType,"");
         Dlg->MsgLab->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->Count == 2)
    {
      TTagNode *_tmpNode = AIE->GetFirstChild();
      UnicodeString _tmp;
      _tmp = FISCondRules->AV["name"]+" = ";
      _tmp += _tmpNode->AV["name"]+_tmpNode->AV["value"];
      _tmpNode = _tmpNode->GetNext();
      _tmp += " "+_tmpNode->AV["name"]+_tmpNode->AV["value"];
      AText = _tmp.Trim();
      _tmp = "";
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsBaseNomSupport::DefDatePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   // AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "����"
   // {
   //   1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
   //                            1,2 ��� �������; 0 - �������� ���
   //                                             1 - ������ �� ������� ����
   //                                             2 - ������ �� ����
   //   2-� - ���� ��;   ��� ���� "�������� ���"
   //                    ��� ����� "������ �� ..." �������� �������
   //   3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
   //                    ��� ���� "������ �� ������� ����"  - �� �������������;
   //   4-� - �������� - ��� ���� "�������� ���"  - �� �������������
   //         �������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
   if (!FDM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (NeedKonkr)
    {
      TDateConForm *Dlg = NULL;
      try
       {
         Dlg = new TDateConForm(Application->Owner);
         int FSubType;
         UnicodeString FSubTypeCapt = "";
         TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
         if (FISCondRules->GetParent("IS") == FISCondRules->GetParent("domain")->GetFirstChild()) // ���� ��������
          FSubType = 0;
         else  // ���� ������������
          {
            FSubType = 1;
            if (!FISAttr->AV["add_ref"].Length())
             FSubTypeCapt = "������ ��������� ������������";
            else
             {
               if (FISAttr->AV["add_ref"].Pos("#"))
                {
                  if (GetPart2(FISAttr->AV["add_ref"],'#').ToIntDef(0))
                   FSubTypeCapt = GetPart1(FISAttr->AV["add_ref"],'#');
                  else
                   {
                     FSubType = 0;
                     FSubTypeCapt = "";
                   }
                }
               else
                FSubTypeCapt = FISAttr->AV["add_ref"];
             }
          }
         bool DoNotDop = FISAttr->CmpAV("ref","40381E23-92155860-4448.1024");
         Dlg->SetType(_scDate,AIE,FSubType,FSubTypeCapt,DoNotDop);
         Dlg->MsgLab->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count)
       {
         tmp = FISCondRules->AV["name"]+" = ";
         __tmp = AIE->GetFirstChild();
         int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
         bool FLast = (bool)(DType&1);
         DType = DType >> 1;
         if (FLast)
          {
            TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
            if (!FISAttr->AV["add_ref"].Length())
             tmp = "������ ��������� ������������";
            else
             tmp = FISAttr->AV["add_ref"];
          }
         TTagNode *DateType   = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam1 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam2 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam3 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam4 = __tmp;
         if (DType == 0)
          {// �������� ���
            if ((DateParam1 && (DateParam1->AV["value"].Length())) ||
                (DateParam2 && (DateParam2->AV["value"].Length())))
             {
               tmp += " "+DateType->AV["name"];
               if (DateParam1->AV["value"].Length())
                {
                  tmp += " "+DateParam1->AV["name"];
                  tmp += "='"+DateParam1->AV["value"]+"'";
                }
               if (DateParam2->AV["value"].Length())
                {
                  tmp += " "+DateParam2->AV["name"];
                  tmp += "='"+DateParam2->AV["value"]+"'";
                }
             }
          }
         else if (DType >= 3)
          { // 3 - ������ ������������ ���������
            // 4 - ������ ���� - ���� ��������� ������� ������������ ���������
            // 5 - ������ ��������� - ���� ��������� ������� ������������ ���������
            // 6 - ������ �������� - ���� ��������� ������� ������������ ���������
            // 7 - ������ ������ - ���� ��������� ������� ������������ ���������

            tmp += " "+DateType->AV["name"];
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         else
          {// ������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
            tmp += " "+DateType->AV["name"];
            if (DateParam1->AV["value"].Length())
             {
               tmp += " "+DateParam1->AV["name"];
               tmp += "='"+DateParam1->AV["value"]+"'";
             }
            if (DateParam2->AV["value"].Length())
             {
               tmp += " "+DateParam2->AV["name"];
               tmp += "='"+DateParam2->AV["value"]+"'";
             }
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         if (DateParam4)
          {
            tmp += " "+DateParam4->AV["name"];
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsBaseNomSupport::DefChoiceDBValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   if (!FDM) return false;
   UnicodeString tmp = "";
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
   if (NeedKonkr)
    {
      TTagNode *FExtAttr = FDM->XMLList->GetRefer(FISAttr->AV["ref"]);
      tmp = "<root>"
            "<fl ref='"+FExtAttr->AV["uid"]+"' req=1'/>"
            "</root>";
    }


   return FDM->DefRegValue(AIE, AText, NeedKonkr, AXMLIE, tmp, _GUI(FISAttr->AV["ref"]), false, false);
}
//---------------------------------------------------------------------------
bool __fastcall TdsBaseNomSupport::DefRegVal(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   if (!this->FDM) return false;
   bool CanAllSelect = false;
   bool SubSel = false;
   UnicodeString tmp = "";
   UnicodeString FGUI = "";
   if (NeedKonkr)
    {
      TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
      if (FISCondRules)
       {
         TTagNode *FRoot = FISCondRules->GetChildByName("root");
         if (FRoot)
          {
            tmp = FRoot->AsXML;
            CanAllSelect = (bool)(FRoot->AV["CanAllSelect"].ToIntDef(0));
            SubSel       = (bool)(FRoot->AV["subsel"].ToIntDef(0));
            FGUI         = FRoot->AV["gui_ref"];
          }
       }
    }
   

  return FDM->DefRegValue(AIE, AText, NeedKonkr, AXMLIE, tmp, FGUI, CanAllSelect, SubSel);
}
//---------------------------------------------------------------------------
bool __fastcall TdsBaseNomSupport::DefTextValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   if (!FDM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (NeedKonkr)
    {
      TTextConForm *Dlg = NULL;
      try
       {
         Dlg = new TTextConForm(Application->Owner, FISCondRules->AV["name"], AIE, FDM->XMLList);
         Dlg->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count > 1)
       {
         __tmp = AIE->GetFirstChild();
         tmp = FISCondRules->AV["name"]+" =";
         while (__tmp->GetNext())
          {
            tmp += " "+__tmp->AV["name"];
            __tmp = __tmp->GetNext();
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsBaseNomSupport::DefExtEditValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
   // AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "����"
   // {
   //   1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
   //                            1,2 ��� �������; 0 - �������� ���
   //                                             1 - ������ �� ������� ����
   //                                             2 - ������ �� ����
   //   2-� - ���� ��;   ��� ���� "�������� ���"
   //                    ��� ����� "������ �� ..." �������� �������
   //   3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
   //                    ��� ���� "������ �� ������� ����"  - �� �������������;
   //   4-� - �������� - ��� ���� "�������� ���"  - �� �������������
   //         �������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
   if (!FDM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (NeedKonkr)
    {
      TDateConForm *Dlg = NULL;
      try
       {
         Dlg = new TDateConForm(Application->Owner);
         int FSubType;
         UnicodeString FSubTypeCapt = "";
         TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
         if (FISCondRules->GetParent("IS") == FISCondRules->GetParent("domain")->GetFirstChild()) // ���� ��������
          FSubType = 0;
         else  // ���� ������������
          {
            if (!FISAttr->AV["add_ref"].Length())
             FSubTypeCapt = "������ ��������� ������������";
            else
             FSubTypeCapt = FISAttr->AV["add_ref"];
            FSubType = 1;
          }
         bool DoNotDop = FISAttr->CmpAV("ref","40381E23-92155860-4448.1024");
         Dlg->SetType(_scDate,AIE,FSubType,FSubTypeCapt,DoNotDop);
         Dlg->MsgLab->Caption = FISCondRules->AV["name"];
         Dlg->ShowModal();
         iRC = (Dlg->ModalResult == mrOk);
       }
      __finally
       {
         if (Dlg) delete Dlg;
       }
    }
   if (AIE->GetFirstChild())
    {
      if (AIE->Count)
       {
//         tmp = "";
         tmp = FISCondRules->AV["name"]+" = ";
         __tmp = AIE->GetFirstChild();
         int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
         bool FLast = (bool)(DType&1);
         DType = DType >> 1;
         if (FLast)
          {
            TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
            if (!FISAttr->AV["add_ref"].Length())
             tmp = "������ ��������� ������������";
            else
             tmp = FISAttr->AV["add_ref"];
          }
         TTagNode *DateType   = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam1 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam2 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam3 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam4 = __tmp;
         if (DType == 0)
          {// �������� ���
            if ((DateParam1 && (DateParam1->AV["value"].Length())) ||
                (DateParam2 && (DateParam2->AV["value"].Length())))
             {
               tmp += " "+DateType->AV["name"];
               if (DateParam1->AV["value"].Length())
                {
                  tmp += " "+DateParam1->AV["name"];
                  tmp += "='"+DateParam1->AV["value"]+"'";
                }
               if (DateParam2->AV["value"].Length())
                {
                  tmp += " "+DateParam2->AV["name"];
                  tmp += "='"+DateParam2->AV["value"]+"'";
                }
             }
          }
         else if (DType == 3)
          {// ������ ������������ ���������
            tmp += " "+DateType->AV["name"];
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         else
          {// ������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
            tmp += " "+DateType->AV["name"];
            if (DateParam1->AV["value"].Length())
             {
               tmp += " "+DateParam1->AV["name"];
               tmp += "='"+DateParam1->AV["value"]+"'";
             }
            if (DateParam2->AV["value"].Length())
             {
               tmp += " "+DateParam2->AV["name"];
               tmp += "='"+DateParam2->AV["value"]+"'";
             }
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         if (DateParam4)
          {
            tmp += " "+DateParam4->AV["name"];
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
}
//---------------------------------------------------------------------------
TdsGetValByIdEvent __fastcall TdsBaseNomSupport::FGetOnGetValById()
{
  if (FDM)
   return FDM->OnGetValById;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsBaseNomSupport::FSetOnGetValById(TdsGetValByIdEvent AValue)
{
  if (FDM)
   FDM->OnGetValById = AValue;
}
//---------------------------------------------------------------------------
TdsGetClassXMLEvent __fastcall TdsBaseNomSupport::FGetOnGetClassXML()
{
  if (FDM)
   return FDM->OnGetClassXML;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsBaseNomSupport::FSetOnGetClassXML(TdsGetClassXMLEvent AValue)
{
  if (FDM)
   FDM->OnGetClassXML = AValue;
}
//---------------------------------------------------------------------------
TExtBtnClick __fastcall TdsBaseNomSupport::FGetExtBtnClick()
{
  if (FDM)
   return FDM->OnExtBtnClick;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsBaseNomSupport::FSetExtBtnClick(TExtBtnClick AValue)
{
  if (FDM)
   FDM->OnExtBtnClick = AValue;
}
//---------------------------------------------------------------------------
__int64 __fastcall TdsBaseNomSupport::FGetLPUCode()
{
  if (FDM)
   return FDM->LPUCode;
  else
   return -1;
}
//---------------------------------------------------------------------------
void __fastcall TdsBaseNomSupport::FSetLPUCode(__int64 AValue)
{
  if (FDM)
   FDM->LPUCode = AValue;
}
//---------------------------------------------------------------------------
TcxEditStyleController* __fastcall TdsBaseNomSupport::FGetStyleController()
{
  if (FDM)
   return FDM->StyleController;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsBaseNomSupport::FSetStyleController(TcxEditStyleController* AValue)
{
  if (FDM)
   FDM->StyleController = AValue;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsBaseNomSupport::FGetXMLPath()
{
  if (FDM)
   return FDM->XMLPath;
  else
   return "";
}
//---------------------------------------------------------------------------
void __fastcall TdsBaseNomSupport::FSetXMLPath(UnicodeString AVal)
{
  if (FDM)
   FDM->XMLPath = AVal;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsBaseNomSupport::FGetDataPath()
{
  if (FDM)
   return FDM->DataPath;
  else
   return "";
}
//---------------------------------------------------------------------------
void __fastcall TdsBaseNomSupport::FSetDataPath(UnicodeString AVal)
{
  if (FDM)
   FDM->DataPath = AVal;
}
//---------------------------------------------------------------------------

