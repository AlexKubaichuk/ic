object TextConForm: TTextConForm
  Left = 561
  Top = 489
  BorderStyle = bsToolWindow
  Caption = 'TextConForm'
  ClientHeight = 169
  ClientWidth = 346
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object TextLab: TLabel
    Left = 13
    Top = 10
    Width = 39
    Height = 13
    Caption = 'TextLab'
  end
  object Label1: TLabel
    Left = 13
    Top = 33
    Width = 81
    Height = 13
    Caption = #1040#1085#1072#1083#1080#1079#1080#1088#1086#1074#1072#1090#1100':'
    FocusControl = CompTypeCB
  end
  object Panel1: TPanel
    Left = 0
    Top = 129
    Width = 346
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 4
    DesignSize = (
      346
      40)
    object OkBtn: TcxButton
      Left = 176
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      OnClick = OkBtnClick
    end
    object CancelBtn: TcxButton
      Left = 259
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object TextED: TcxTextEdit
    Left = 13
    Top = 74
    Enabled = False
    Style.Color = clInfoBk
    TabOrder = 1
    Width = 320
  end
  object FullWordChB: TcxCheckBox
    Left = 187
    Top = 98
    Caption = #1057#1083#1086#1074#1086' '#1094#1077#1083#1080#1082#1086#1084
    Enabled = False
    TabOrder = 3
    Width = 121
  end
  object CaseSensChB: TcxCheckBox
    Left = 13
    Top = 98
    Caption = #1057' '#1091#1095#1105#1090#1086#1084' '#1088#1077#1075#1080#1089#1090#1088#1072
    Enabled = False
    TabOrder = 2
    Width = 121
  end
  object CompTypeCB: TcxComboBox
    Left = 13
    Top = 50
    Properties.DropDownListStyle = lsFixedList
    Properties.Items.Strings = (
      #1054#1090#1089#1091#1090#1089#1090#1074#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1103
      #1053#1072#1083#1080#1095#1080#1077' '#1083#1102#1073#1086#1075#1086' '#1079#1085#1072#1095#1077#1085#1080#1103
      #1059#1082#1072#1079#1072#1085#1085#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077)
    Properties.OnChange = CompTypeCBPropertiesChange
    TabOrder = 0
    Width = 320
  end
end
