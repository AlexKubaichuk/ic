//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riChoiceTree.h"
//---------------------------------------------------------------------------
#include "irTreeClass.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED_BE       dynamic_cast<TcxButtonEdit*>(ED)
#define cExtLab      static_cast<TLabel*>(CBList->Items[0])
//---------------------------------------------------------------------------
__fastcall TriChoiceTree::TriChoiceTree(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref, TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  TDataSource* ASrc, bool IsApp)
                   :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                                 APref, AReqColor, ACtrlOwner,
                                                 ASrc, IsApp)
{
  //������ ����� ��������
  Lab = new TLabel(this);   Lab->Parent = this;
  Lab->Transparent = true;
  Lab->Caption = FNode->AV["name"]+":";
  // ������ ����� ��� ���������� ��������
  CBList = new TList;
  CBList->Add(new TLabel(this));
  cExtLab->Parent = this;
  cExtLab->Transparent = true;
  cExtLab->AutoSize = false;
  cExtLab->ParentFont = true;
  cExtLab->WordWrap = true;
  cExtLab->Color = LtColor(clBtnFace,16);
//  cExtLab->Font->Color = clGray;
//  cExtLab->Font->Size = 6;
  // ������ �������
  ED = new TcxButtonEdit(this);
  tED_BE->Parent =  this;
  tED_BE->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
  tED_BE->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
  tED_BE->Properties->ClickKey = ShortCut(VK_DOWN,TShiftState() << ssAlt);
  tED_BE->Properties->ReadOnly = true;
  tED_BE->Properties->ViewStyle = vsHideCursor;
  TcxEditButton *tmpBtn = tED_BE->Properties->Buttons->Add();
  tmpBtn->Kind = bkText;
  tmpBtn->Caption = "X";
//      tmpBtn->Glyph->LoadFromResourceName((int)HInstance,"CLEAR");
  tED_BE->Properties->OnButtonClick = TreeBtnClick;
  // ������ ���������
  if (Required) tED_BE->Style->Color = FReqColor;
  Lab->FocusControl = ED;
  TreeClasses = NULL;
  if (SrcDS()->Active)
   {
     if (IsAppend)
      {
        if (IsTemplate(FNode))
         {
           if (FSetEditValue(FGetBeginCode(_TemplateValue(FNode)).ToIntDef(0)))
            irquSetAsInt(SrcDS(), FFlName, FGetBeginCode(_TemplateValue(FNode)).ToIntDef(0));
           else
            irquSetAsNull(SrcDS(),FFlName);
         }
      }
     else
      FSetEditValue(irquAsStr(SrcDS(),FFlName));
   }
  // ������������ � �������

  Lab->Top = 2;  Lab->Left = 2;
  if (Lab->Width > (AParent->Width-2))
   {
     Lab->Font->Size = Lab->Font->Size - 1;
     Lab->Update();
     if (Lab->Width > (AParent->Width - 2))
      Lab->Width = AParent->Width-2;
   }
  ED->Constraints->MinWidth = 60;

  if (FNode->AV["linecount"].ToIntDef(0) < 2)
   {
     tED_BE->Left = Lab->Width+5;
     tED_BE->Top = 0;
   }
  else
   {
     tED_BE->Left = 2;
     tED_BE->Top = Lab->Height+2;
     TwoLine = true;
   }
  tED_BE->Width = AParent->Width - 4 - tED_BE->Left;

  if ((tED_BE->Left + tED_BE->Width) > (AParent->Width - 4))
   {
     TwoLine = true;
     tED_BE->Left = 4;
     tED_BE->Top = Lab->Height;
     tED_BE->Width = AParent->Width - 4 - tED_BE->Left;
   }
  cExtLab->Left = Lab->Left;
  cExtLab->Width = Width-4;
  cExtLab->Height = (cExtLab->Height-2)*(FNode->AV["linecount"].ToIntDef(1)-1);
  cExtLab->Top = tED_BE->Top+tED_BE->Height;
  CMaxH = cExtLab->Top + cExtLab->Height+2; CMinH = CMaxH;
  Height = CMaxH;
//***************************************************************************
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::SetOnDependValues()
{
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::TreeBtnClick(TObject *Sender, int AButtonIndex)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString SrcCode = "";
  if (AButtonIndex == 0)
   {
     if (FExtBtnClick)
      {
        UnicodeString Src;
        if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
        if (FExtBtnClick(FNode,Src,FSrc,true,(TObject*) FCtrlOwner, 0))
         {
           int ind = Src.Pos("=");
           SrcCode = Src.SubString(ind+1,Src.Length()-ind);
           if (SrcDSEditable())
            {
              if (FSetEditValue(SrcCode))
               irquSetAsStr(SrcDS(),FFlName,SrcCode);
              else
               irquSetAsNull(SrcDS(),FFlName);
            }
         }
      }
     if (TreeClasses)
      {
        int clIndex = TreeClasses->IndexOf(FNode->AV["uid"]);
        TirTreeClassForm *Dlg = (TirTreeClassForm*)TreeClasses->Objects[clIndex];
        Dlg->Caption = FNode->AV["dlgtitle"];
        __int64 FClCode = irquAsStr(SrcDS(),FFlName).ToIntDef(0);
        Dlg->clCode = FClCode;
        Dlg->ShowModal();
        if (Dlg->ModalResult == mrOk)
         {
           if (SrcDSEditable())
            {
              if (FSetEditValue(IntToStr(Dlg->clCode)))
               irquSetAsInt(SrcDS(),FFlName,Dlg->clCode);
              else
               irquSetAsNull(SrcDS(),FFlName);
            }
         }
      }
   }
  else
   {
     if (SrcDSEditable())
      {
        FSetEditValue("");
        irquSetAsNull(SrcDS(),FFlName);
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::FSetTreeBtnClick(TTreeBtnClick ATreeBtnClick)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   FTreeBtnClick = ATreeBtnClick;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::FSetExtBtnClick(TExtBtnClick AExtBtnClick)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FExtBtnClick = AExtBtnClick;
/*  if (AExtBtnClick)
   {
     if (SrcDS()->Active)
      {
        UnicodeString SrcCode = "";
        if (IsAppend)
         {
           SrcCode = FNode->AV["default"].Trim();
           if (IsTemplate(FNode))
            SrcCode = _TemplateValue(FNode);
         }
        else
         SrcCode = irquAsStr(SrcDS(),FFlName);
        if (FExtBtnClick(FNode,SrcCode,FSrc, false,(TObject*) FCtrlOwner,0))
         {
           SrcCode = FSetEditValue(SrcCode);
           if (SrcDSEditable())
            irquSetAsStr(SrcDS(),FFlName,SrcCode);
         }
      }
   }*/
}
//---------------------------------------------------------------------------
__fastcall TriChoiceTree::~TriChoiceTree()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  tED_BE->Properties->OnButtonClick = NULL;
  delete cExtLab;
  delete tED_BE;
  TreeClasses = NULL;
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceTree::FSetEditValue(UnicodeString AValue)
{
  bool RC = false;
  try
   {
     if (AValue.ToIntDef(-1) != -1)
      {
        try
         {
           TTagNode *clN = FCtrlOwner->GetNode(AVUp(FNode,"ref"));
           irExecProc("CLASSDATA_"+FTabPref+AVUp(clN,"tblname"), quFree, "CL_CODE", Variant((__int64)AValue.ToIntDef(-1)));
           cExtLab->Caption = irquAsStr(quFree,"P_NAME");
           tED_BE->Text     = irquAsStr(quFree,"P_EXTCODE");
           RC = true;
         }
        __finally
         {
           irExecCloseProc(quFree,true);
         }
      }
     else
      {
        cExtLab->Caption = "";
        tED_BE->Text     = "";
      }
     riDataChange(this);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::SetEDLeft(int ALeft)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine) ED->Left = ALeft;
  cExtLab->Width = ED->Left+ED->Width;
  Width = ED->Left+ED->Width+4;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!TwoLine&&ED)  *AEDLeft = ED->Left;
  else               *AEDLeft = -1;
  if (!TwoLine&&Lab) *ALabR = Lab->Left+Lab->Width;
  else               *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::cResize(TObject *Sender)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     cExtLab->Left = 2; cExtLab->Width = Width-4;
     if (TwoLine) {ED->Left = 2; ED->Width = Width-4; }
     else          ED->Left = Width - ED->Width - 2;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceTree::UpdateChanges()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTree::GetValueStr(bool ABinaryNames)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  xVal = tED_BE->Text;
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::SetIsVal(bool AEnable)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isEnabled = AEnable;
  Lab->Enabled = isEnabled && isEdit && !isDelegate;
  ED->Enabled = Lab->Enabled;
  cExtLab->Enabled = Lab->Enabled;
  if (isEdit&&SrcDSEditable() && !isDelegate && (!IsAppend || !isTemplate))
   {
     FSetEditValue("");
     irquSetAsNull(SrcDS(),FFlName);
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnable(AEnable);
  if (SrcDSEditable())
   {
     if (FSetEditValue(AVal))
      irquSetAsStr(SrcDS(),FFlName,AVal);
     else
      irquSetAsNull(SrcDS(),FFlName);
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else            Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTree::FDefaultDataChange(TObject *Sender)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriChoiceTree::GetControl(int AIdx)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FNode->CmpName("text,date,datetime,time,digit,extedit,binary,choice,choiceTree"))
    return (TControl*)ED;
   else
    return NULL;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriChoiceTree::GetFirstEnabledControl()
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTree::GetValue(UnicodeString ARef)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (SrcDS()->Active)
    return irquAsStr(SrcDS(),FFlName);
   else
    return "";
}
//---------------------------------------------------------------------------
Variant __fastcall TriChoiceTree::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" ' tagtype not supported'");
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceTree::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED)
         {
           if (ED->Enabled)
            {
              if (!((TcxButtonEdit*)ED)->Text.Length())  RC = false;
            }
         }
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
              UnicodeString ErrMessage;
              if (Lab) ErrMessage = Lab->Caption;
              else     ErrMessage = FNode->AV["name"];
              ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
              MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
              ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTree::FGetBeginCode(UnicodeString ASrc)
{
  return GetPart1(GetPart2(ASrc,':'),'#');
}
//---------------------------------------------------------------------------
