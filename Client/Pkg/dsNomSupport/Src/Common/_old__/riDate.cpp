//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riDate.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define dbED_DE      ((TcxDBDateEdit*)ED)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#pragma resource     "calendar.RES"
//---------------------------------------------------------------------------
__fastcall TriDate::TriDate(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref, TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  TDataSource* ASrc, bool IsApp)
                   :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                                 APref, AReqColor, ACtrlOwner,
                                                 ASrc, IsApp)
{
   TRect xSize = Rect(0,0,120,21); //  default ������� � ������ ��� "ED"
   Lab = new TLabel(this);  Lab->Parent = this;
   Lab->Transparent = true;
   Lab->Top = 2;            Lab->Left = 4;
   Lab->Caption = FNode->AV["name"]+":";
   ED = new TcxDBDateEdit(this);
   ED->Parent = this;
   dbED_DE->Properties->OnChange = FDefaultDataChange;

   dbED_DE->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
   dbED_DE->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
   dbED_DE->Properties->ButtonGlyph->LoadFromResourceName(reinterpret_cast<int>(HInstance), "CALENDARBTN");
//   ((TcxDBDateEdit*)ED)->Ctl3D = is3D;
   xSize = Rect(0,0,85,21);
   ED->Constraints->MaxWidth = 85;
   dbED_DE->DataBinding->DataField = FFlName; dbED_DE->DataBinding->DataSource = FSrc;
//   dbED_DE->YearDigits = dyFour;
   if (Required) dbED_DE->Style->Color = FReqColor;
   if (IsAppend)
    if (AVLo(FNode,"default") == "currentdate")
     if (SrcDSEditable()) irquSetAsDate(SrcDS(),FFlName, Date());
   if (IsTemplate(FNode))
    {
      if (SrcDSEditable()) irquSetAsVar(SrcDS(),FFlName,_TemplateValue(FNode));
    }
   ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
   ED->Constraints->MinWidth = 60;
   ED->Parent = this;
   ED->Width = 95;
//   if (ED->Width < 60) ED->Width = 60;
   ED->Top = xSize.Top;
   ED->Height = xSize.Bottom;
   if (Lab->Width > (AParent->Width-2))
    {
      Lab->Font->Size = Lab->Font->Size - 1;
      Lab->Update();
      if (Lab->Width > (AParent->Width - 2))
       Lab->Width = AParent->Width-2;
    }
   ED->Left = Lab->Left + Lab->Width + 5;
   if ((ED->Left + ED->Width) > (AParent->Width - 2))
    {
      TwoLine = true;  Lab->Top = 0;
      ED->Left = Lab->Left+5;  ED->Top = Lab->Height;
      CMaxH = 34; CMinH = 34;
      Height = CMaxH;
    }
   if (Lab&&ED)
    Lab->FocusControl = ED;
   if (Parent->ClassNameIs("TriDate"))
    {
      Anchors.Clear();
      Anchors << akLeft << akRight << akTop;
    }
}
//---------------------------------------------------------------------------
void __fastcall TriDate::SetOnDependValues()
{
  if (FNode->AV["depend"].Length() == 9 && isRefMCtrl(_GUI(FNode->AV["depend"])))
   RefMCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(FNode);
  else if (FNode->AV["ref"].Length() == 4 && isRefCtrl(FNode))
   RefCtrl(FNode)->SetOnLab(FNode);
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
__fastcall TriDate::~TriDate()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   dbED_DE->Properties->OnChange = NULL;
   delete dbED_DE;
}
//---------------------------------------------------------------------------
void __fastcall TriDate::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine) ED->Left = ALeft;
  Width = ED->Left+ED->Width+4;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriDate::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!TwoLine) *AEDLeft = ED->Left;
  else          *AEDLeft = -1;
  if (!TwoLine) *ALabR = Lab->Left+Lab->Width;
  else          *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriDate::cResize(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     if (TwoLine) {ED->Left = 2; ED->Width = 95;}
     else          ED->Left = Width - ED->Width - 2;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriDate::UpdateChanges()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  dbED_DE->Properties->OnChange((TObject*)ED);
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriDate::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  if (ED->Enabled)
   if (((int)dbED_DE->Date != Cxdateutils::NullDate))      xVal += dbED_DE->Date.FormatString("dd.mm.yyyy");
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriDate::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriDate::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isEnabled = AEnable;
  Lab->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  if (isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate))
   {
     dbED_DE->Style->Color = (Required)? FReqColor : clWindow;
   }
  else
   {
     dbED_DE->Style->Color = clBtnFace;
     if (isEdit && !isDelegate && (!IsAppend || !isTemplate))
      dbED_DE->Date = Cxdateutils::NullDate;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDate::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnable(AEnable);
  if (SrcDSEditable()) irquSetAsDate(SrcDS(),FFlName,StrToDate(AVal));
}
//---------------------------------------------------------------------------
void __fastcall TriDate::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else            Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriDate::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  dbED_DE->Properties->OnChange = riDataChange;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriDate::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TriDate::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDate::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriDate::GetControl(int AIdx)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (TControl*)ED;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriDate::GetFirstEnabledControl()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriDate::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if ((int)dbED_DE->Date != Cxdateutils::NullDate)
    return dbED_DE->Date.FormatString("dd.mm.yyyy");
  return "";
}
//---------------------------------------------------------------------------
Variant __fastcall TriDate::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return Variant(dbED_DE->Date.FormatString("'dd.mm.yyyy'"));
}
//---------------------------------------------------------------------------
bool __fastcall TriDate::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED->Enabled)
          if ((int)dbED_DE->Date == Cxdateutils::NullDate)  RC = false;
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
           UnicodeString ErrMessage;
           if (Lab) ErrMessage = Lab->Caption;
           else     ErrMessage = FNode->AV["name"];
           ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
           MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
           ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------

