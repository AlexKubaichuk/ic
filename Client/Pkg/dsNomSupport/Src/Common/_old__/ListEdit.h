//---------------------------------------------------------------------------
#ifndef ListEditH
#define ListEditH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
//---------------------------------------------------------------------------
#include "Reg_DMF.h"
#include "Registry_ICScomp.h"
/*#include <Buttons.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
#include "cxGraphics.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlack.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinCaramel.hpp"
#include "dxSkinCoffee.hpp"
#include "dxSkinDarkRoom.hpp"
#include "dxSkinDarkSide.hpp"
#include "dxSkinFoggy.hpp"
#include "dxSkinGlassOceans.hpp"
#include "dxSkiniMaginary.hpp"
#include "dxSkinLilian.hpp"
#include "dxSkinLiquidSky.hpp"
#include "dxSkinLondonLiquidSky.hpp"
#include "dxSkinMcSkin.hpp"
#include "dxSkinMoneyTwins.hpp"
#include "dxSkinOffice2007Black.hpp"
#include "dxSkinOffice2007Blue.hpp"
#include "dxSkinOffice2007Green.hpp"
#include "dxSkinOffice2007Pink.hpp"
#include "dxSkinOffice2007Silver.hpp"
#include "dxSkinOffice2010Black.hpp"
#include "dxSkinOffice2010Blue.hpp"
#include "dxSkinOffice2010Silver.hpp"
#include "dxSkinPumpkin.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinsDefaultPainters.hpp"
#include "dxSkinSeven.hpp"
#include "dxSkinSharp.hpp"
#include "dxSkinSilver.hpp"
#include "dxSkinSpringTime.hpp"
#include "dxSkinStardust.hpp"
#include "dxSkinSummer2008.hpp"
#include "dxSkinValentine.hpp"
#include "dxSkinXmas2008Blue.hpp"    */
//---------------------------------------------------------------------------
class PACKAGE TListEditForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *CompPanel;
        TBevel *Bevel1;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
        bool __fastcall FSetFocus(TTagNode *itxTag, UnicodeString& tmp);
        void __fastcall FormDestroy(TObject *Sender);
private:
    TRegistry_ICS* RegComp;
    TRegEDItem   *LastCtrl;
    bool         isAppend,OneGroup,isLeft;
    TcxPageControl  *Group1st;
    TRegEDItem   *FirstCtrl;
    TRegEDItem   *LastLeftCtrl;
    TcxTabSheet    *gPage;
    TTagNode     *UnitNode;
    TTagNode     *UnitDesc;
    UnicodeString AddrSel,ErrMessage,FTabPref;
    int      LeftLine;
    UnicodeString xName;
    TRegEDContainer *FCtrList;
    int FLastTop,FLastHeight;
    bool __fastcall GetInput(TTagNode *itTag, UnicodeString &UID);
    void __fastcall CreateUnitList(TWinControl *AParent, bool isAppend,TTagNode* ANode,int *ATop,int *ALeft,TList *GroupComp,int APIndex = 0);
    bool __fastcall CheckInput();
    bool __fastcall FCtrlDataChange(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource);
public:		// User declarations
    __fastcall TListEditForm(TComponent* Owner,bool isAp,UnicodeString ATabPref, TRegistry_ICS* ARegComp);
};
//---------------------------------------------------------------------------
extern PACKAGE TListEditForm *ListEditForm;
//---------------------------------------------------------------------------
#endif

