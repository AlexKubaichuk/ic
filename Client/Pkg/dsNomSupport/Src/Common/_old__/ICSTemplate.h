//---------------------------------------------------------------------------

#ifndef ICSTemplateH
#define ICSTemplateH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Mask.hpp>
#include "FIBQuery.hpp"
#include "pFIBQuery.hpp"
#include <DB.hpp>
#include "AxeUtil.h"
#include "RegEd.h"
#include "Registry_ICScomp.h"
#include <StdCtrls.hpp>
//---------------------------------------------------------------------------
class PACKAGE EICSTemplateError : public Exception
{
public:
     __fastcall EICSTemplateError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE TICSTemplate : public TObject
{
private:
        TPanel *CompPanel;
        TTreeBtnClick FOnTreeBtnClick;
        TExtBtnClick  FOnExtBtnClick;
        TExtBtnClick  FGetExtWhere;
        void __fastcall FSetTreeBtnClick(TTreeBtnClick AOnTreeBtnClick);
        void __fastcall FSetExtBtnClick(TExtBtnClick AOnExtBtnClick);
        TTagNode *Root;
        TTagNode *FltFl;
        TStringList *CList;
        TStringList *FSelList;
        TDataSet *xquFree;
        int      xTop,MaxW;
        TColor FReqColor;
        bool     isCreate,FADO;
        UnicodeString FTabPref,FUpperFunc;
        bool __fastcall FPrepare();
        UnicodeString __fastcall GetGrSelect(TTagNode *AGroup);
        bool __fastcall CreateTmpl(TTagNode *itxTag, UnicodeString& tmp);
        bool __fastcall FGetCBCount(TTagNode *itxTag, UnicodeString &ACount);
        bool __fastcall FClear(TTagNode *itxTag, UnicodeString& tmp);
        bool __fastcall FSetValue(TTagNode *itxTag, UnicodeString& tmp);
        UnicodeString __fastcall FGetWhere(TTagNode *AitNode, UnicodeString AAlias,bool AClRep = false);
        bool __fastcall FCheckInput(TTagNode *itxTag, UnicodeString& tmp);
        UnicodeString __fastcall FGetValue(UnicodeString AUID, bool ABinaryNames);
        bool __fastcall FGetSelectedUpdateSQL(TTagNode *itxTag, UnicodeString& Values);
        UnicodeString __fastcall AsTypeUpdate(TTagNode *ADefNode, UnicodeString AVal);
        bool __fastcall FGetSelectedTextValues(TTagNode *itxTag, UnicodeString& Values);
        bool __fastcall FGetSelectedTextFieldValues(TTagNode *itxTag, UnicodeString& Values);
        bool __fastcall FSetTreeFormList(TTagNode *itxTag, UnicodeString& tmp);
        bool __fastcall FSetExtClick(TTagNode *itxTag, UnicodeString& tmp);
        TRegEDContainer *FCtrList;
        int FLastTop,FLastHeight;
        int FHeight, FWidth;
        int FlCounter;
        bool MS,FNextCol;
        bool FIsTemplateField;
        void __fastcall FSetWidth(int AWidth);
public:   // User declarations
    bool __fastcall CheckInput();
    bool __fastcall IsEmpty();
    UnicodeString    __fastcall GetValue(UnicodeString AUID);
    UnicodeString    __fastcall GetName(UnicodeString AUID);
    UnicodeString    __fastcall GetWhere(UnicodeString AAlias = "");
    UnicodeString    __fastcall GetWhereClRep(UnicodeString AAlias, TStringList *ASelList);
    UnicodeString    __fastcall GetSelectedTextValues(bool CanFieldName = false);
    UnicodeString    __fastcall GetSelectedUpdateSQL();
    TWinControl*  __fastcall GetFirstEnabledControl();
    void __fastcall Clear();
//    bool __fastcall IsEmpty();
    __fastcall ~TICSTemplate();
__published:
    __fastcall TICSTemplate(UnicodeString AFields, TPanel *ACompPanel, TADODataSet *AquFree, UnicodeString APref, UnicodeString ATreePath, TTagNode *ATree,TColor AReqColor, bool ANativeStyle = true, TcxLookAndFeelKind ALFKind = lfStandard, bool AIsTemplateField = false);
    __fastcall TICSTemplate(UnicodeString AFields, TPanel *ACompPanel, TpFIBQuery *AquFree, UnicodeString APref, UnicodeString ATreePath, TTagNode *ATree, TColor AReqColor, bool ANativeStyle = true, TcxLookAndFeelKind ALFKind = lfStandard, bool AIsTemplateField = false);
    void __fastcall FInit(UnicodeString AFields, TPanel *ACompPanel, TDataSet *AquFree, UnicodeString APref, UnicodeString ATreePath, TTagNode *ATree, TColor AReqColor, bool ANativeStyle = true, TcxLookAndFeelKind ALFKind = lfStandard);
    __property int Height = {read=FHeight};
    __property int Width = {read=FWidth, write=FSetWidth};
    __property UnicodeString UpperFunc = {read=FUpperFunc, write=FUpperFunc,stored=true};

    __property TRegEDContainer *CtrList = {read=FCtrList};

    __property TTreeBtnClick OnTreeBtnClick = {read=FOnTreeBtnClick, write=FSetTreeBtnClick};
    __property TExtBtnClick OnExtBtnClick = {read=FOnExtBtnClick, write=FSetExtBtnClick};
    __property TExtBtnClick OnGetExtWhere = {read=FGetExtWhere, write=FGetExtWhere};
};
//---------------------------------------------------------------------------
#endif
