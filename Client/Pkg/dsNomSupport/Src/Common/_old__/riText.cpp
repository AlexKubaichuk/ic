//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riText.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


#define dbED         ((TcxDBMaskEdit*)ED)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED_CB      ((TcxComboBox*)ED)
//---------------------------------------------------------------------------
__fastcall TriText::TriText(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref, TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  TDataSource* ASrc, bool IsApp)
                   :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                                 APref, AReqColor, ACtrlOwner,
                                                 ASrc, IsApp)
{
   TRect xSize = Rect(0,0,120,21); //  default ������� � ������ ��� "ED"
   Lab = new TLabel(this);  Lab->Parent = this;
   Lab->Transparent = true;
   Lab->Top = 2;            Lab->Left = 4;
   Lab->Caption = FNode->AV["name"]+":";
   FOnGetFormat = ACtrlOwner->OnGetFormat;
   if (isFV)
    {
      ED = new TcxComboBox(this);
      xSize = Rect(0,0,16,23);
      if (Required) tED_CB->Style->Color = FReqColor;
      ED->Parent = this;
      ((TcxComboBox*)ED)->Properties->OnChange = FDefaultDataChange;
      tED_CB->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
      tED_CB->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
      tED_CB->Properties->Items->Clear();
      xSize.Right = 5*FNode->AV["length"].ToInt()+GetSystemMetrics(SM_CXVSCROLL);
      if (SrcDS()->Active)
       {
         if (IsAppend)
          {
            if (SrcDS()->State != dsBrowse)
             irquSetAsStr(SrcDS(),FFlName,FNode->AV["default"]);
            tED_CB->Text = FNode->AV["default"];
          }
         else
          {
            tED_CB->Text = irquAsStr(SrcDS(),FFlName);
          }
       }
      tED_CB->Properties->MaxLength = FNode->AV["length"].ToInt();
      tED_CB->CMinW = 60;
      tED_CB->OnExit = FFieldCast;
    }
   else
    {
      ED = new TcxDBMaskEdit(this);
      ED->Parent = this;
      dbED->Properties->OnChange = FDefaultDataChange;
      dbED->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
      dbED->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
//      dbED->Ctl3D = is3D;
      dbED->DataBinding->DataField = FFlName; dbED->DataBinding->DataSource = FSrc;
      if (Required) dbED->Style->Color = FReqColor;
      int xW = 5*FNode->AV["length"].ToInt();
      xSize = Rect(0,0,xW,23);
      dbED->Properties->MaxLength = FNode->AV["length"].ToInt();
      dbED->CMinW = 60;
      dbED->OnExit = FFieldCast;
      if (FNode->AV["format"].Length())
       {
         dbED->Properties->MaskKind = emkRegExprEx;
         if (FOnGetFormat)
          dbED->Properties->EditMask = FOnGetFormat(FNode->AV["format"]);
         else
          dbED->Properties->EditMask = FNode->AV["format"];
       }
    }
   if (IsTemplate(FNode))
    {
      if (SrcDSEditable()) irquSetAsVar(SrcDS(),FFlName,_TemplateValue(FNode));
    }
   ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
   ED->Constraints->MinWidth = 60;
   ED->Parent = this;
   ED->Width = xSize.Right+10;
   if (ED->Width < 60) ED->Width = 60;
   ED->Top = xSize.Top;
   ED->Height = xSize.Bottom;
   if (Lab->Width > (AParent->Width-2))
    {
      Lab->Font->Size = Lab->Font->Size - 1;
      Lab->Update();
      if (Lab->Width > (AParent->Width - 2))
       Lab->Width = AParent->Width-2;
    }
   ED->Left = Lab->Left + Lab->Width + 5;
   if ((ED->Left + ED->Width) > (AParent->Width - 2))
    {
      TwoLine = true;  Lab->Top = 0;
      ED->Left = Lab->Left+5;  ED->Top = Lab->Height;
      CMaxH = 36; CMinH = 36;
      Height = CMaxH;
      if ((ED->Left + ED->Width) > (AParent->Width - 2))
       ED->Width = AParent->Width - 4 - ED->Left;
      if (FNode->CmpName("text"))
       ED->Width = AParent->Width - 12 - ED->Left;
    }
   Lab->FocusControl = ED;
   if (Parent->ClassNameIs("TriText"))
    {
      Anchors.Clear();
      Anchors << akLeft << akRight << akTop;
    }
}
//---------------------------------------------------------------------------
void __fastcall TriText::SetOnDependValues()
{
  if (FNode->AV["depend"].Length() == 9 && isRefMCtrl(_GUI(FNode->AV["depend"])))
   RefMCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(FNode);
  else if (FNode->AV["ref"].Length() == 4 && isRefCtrl(FNode))
   RefCtrl(FNode)->SetOnLab(FNode);
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
void __fastcall TriText::SetFixedVar(UnicodeString AVals)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (isFV)
    {
      UnicodeString tmp = tED_CB->Text;
      tED_CB->Properties->Items->Text = AVals;
      int ind = tED_CB->Properties->Items->IndexOf(tmp);
      if (ind != -1)
       {
         tED_CB->ItemIndex = ind;
         tED_CB->Text = tmp;
       }
      else
       tED_CB->Text = tmp;
    }
}
//---------------------------------------------------------------------------
__fastcall TriText::~TriText()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (!isFV) dbED->Properties->OnChange    = NULL;
   else       tED_CB->Properties->OnChange  = NULL;
   if (!isFV) delete dbED;
   else       delete tED_CB;
}
//---------------------------------------------------------------------------
void __fastcall TriText::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine) ED->Left = ALeft;
  Width = ED->Left+ED->Width+4;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriText::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!TwoLine)  *AEDLeft = ED->Left;
  else           *AEDLeft = -1;
  if (!TwoLine)  *ALabR = Lab->Left+Lab->Width;
  else           *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriText::cResize(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     if (TwoLine) {ED->Left = 2; ED->Width = Width-4;}
     else          ED->Left = Width - ED->Width - 2;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriText::FFieldCast(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   UnicodeString DDD;
   bool isED = Sender->ClassNameIs("TcxDBMaskEdit");
   if (isED)
    {
      if (!((TcxDBMaskEdit*)Sender)->Text.Length()) return;
      DDD = ((TcxDBMaskEdit*)Sender)->Text.Trim();
    }
   else
    {
      if (!((TcxComboBox*)Sender)->Text.Length()) return;
      DDD = ((TcxComboBox*)Sender)->Text.Trim();
    }
   DDD = FieldCast(FNode,DDD);
   if (isED) ((TcxDBMaskEdit*)Sender)->Text = DDD;
   else      ((TcxComboBox*)Sender)->Text = DDD;
   if (SrcDSEditable()) irquSetAsStr(SrcDS(),"R"+AVUp(FNode,"uid"),DDD);

}
//---------------------------------------------------------------------------
bool __fastcall TriText::UpdateChanges()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!isFV) dbED->Properties->OnChange((TObject*)ED);
  else       tED_CB->Properties->OnChange((TObject*)ED);
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriText::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  if (ED->Enabled)
   {
     if (!isFV)
      {
        if (dbED->Text.Length())            xVal += dbED->Text;
      }
     else
      {
        if (tED_CB->Text.Length())            xVal += tED_CB->Text;
      }
   }
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriText::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriText::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isEnabled = AEnable;
  Lab->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  if (isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate))
   {
     if (!isFV)
      dbED->Style->Color = (Required)? FReqColor : clWindow;
     else
      tED_CB->Style->Color = (Required)? FReqColor : clWindow;
   }
  else
   {
     if (!isFV)
      {
        dbED->Style->Color = clBtnFace;
        if (isEdit && !isDelegate && (!IsAppend || !isTemplate))
         dbED->Text = "";
      }
     else
      {
        tED_CB->Style->Color = clBtnFace;
        if (isEdit && !isDelegate && (!IsAppend || !isTemplate))
         tED_CB->Text = "";
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriText::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnable(AEnable);
  if (!isFV)
   {
     if (SrcDSEditable()) irquSetAsStr(SrcDS(),FFlName,AVal);
   }
  else
   {
      if (SrcDSEditable())
       {
         irquSetAsStr(SrcDS(),FFlName,AVal);
         tED_CB->Text = AVal;
       }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriText::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else            Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriText::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  if (!isFV)
   dbED->Properties->OnChange = riDataChange;
  else
   tED_CB->Properties->OnChange = riDataChange;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriText::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
  if (!TmpCtrl) return;
// ������ �������� ��� ��������� �������� �������� ����� choiceBD ----------------------------------
  Variant xVal; xVal.Empty();
  if (ED->Enabled||isVal)
   {
     if (!isFV)
      {
        if (dbED->Text.Length())    xVal = Variant(dbED->Text);
      }
     else
      {
        if (tED_CB->Text.Length()) xVal = Variant(tED_CB->Text);
      }
   }
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  _TemplateValue(FNode) = xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriText::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (isFV)
   { // ������ �������� ��� "text" ���� ���������� ������� fixedvar
     if (SrcDSEditable()) irquSetAsStr(SrcDS(),FFlName,tED_CB->Text);
   }
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
  if (SetLabList->Count > 0) // ��������� ������ ��� Label �������� �� choice ��� choiceBD
   {
/*
     if (FNode->CmpName("choice")) // ����� �� choice
      {
        int ItmIndex;
        if (TmpCtrl) ItmIndex  = tED_CB->ItemIndex;
        else         ItmIndex  = tED_CB->ItemIndex;
        if (ItmIndex >= (int)TmpCtrl)
         {
           for (int i = 0; i < SetLabList->Count; i++)
            {
              xRef = ((TTagNode*)SetLabList->Items[i])->AV["ref"].UpperCase(); // ref == uid.choice
              if (xRef == FUID)
               {
                 if (TmpCtrl) labCtrl(i)->SetLabel(tED_CB->Items->Strings[tED_CB->ItemIndex]);
                 else         labCtrl(i)->SetLabel(tED_CB->Items->Strings[tED_CB->ItemIndex]);
               }
            }
         }
      }
*/
   }
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriText::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriText::GetControl(int AIdx)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (TControl*)ED;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriText::GetFirstEnabledControl()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriText::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return dbED->Text;
}
//---------------------------------------------------------------------------
Variant __fastcall TriText::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" 'tagtype not supported'");
}
//---------------------------------------------------------------------------
bool __fastcall TriText::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED->Enabled)
         {
           if (!dbED->Text.Length())  RC = false;
         }
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
              UnicodeString ErrMessage;
              if (Lab) ErrMessage = Lab->Caption;
              else     ErrMessage = FNode->AV["name"];
              ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
              MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
              ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------

