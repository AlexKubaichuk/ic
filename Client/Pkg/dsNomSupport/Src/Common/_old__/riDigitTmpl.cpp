//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riDigitTmpl.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED         ((TcxMaskEdit*)ED)
#define tED2        ((TcxMaskEdit*)ED2)
//---------------------------------------------------------------------------
__fastcall TriDigitTmpl::TriDigitTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref,  TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  UnicodeString AVal)
           :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                      APref,  AReqColor,
                                      ACtrlOwner,  AVal)
{
  TRect xSize = Rect(0,0,120,21); //  default ������� � ������ ��� "ED"
  Lab = new TLabel(this);  Lab->Parent = this;
  Lab->Transparent = true;
  Lab->Top = 2;            Lab->Left = 4;
  Lab->Caption = FNode->AV["name"]+":";
  ED = new TcxMaskEdit(this);
  ED2 = new TcxMaskEdit(this);
  EDLab = new TLabel(this);   EDLab->Parent  = this;
  EDLab->Transparent = true;
  EDLab2 = new TLabel(this);  EDLab2->Parent = this;
  EDLab2->Transparent = true;
  EDLab->Caption =  "��:";
  EDLab2->Caption = "��:";
  int xW = 5*FNode->AV["digits"].ToInt();
  if (xW > 180) xW = 180;
  xSize = Rect(0,0,xW,43);
  FIsFloat = (FNode->AV["decimal"].ToIntDef(-1) != -1);

  tED->Properties->Alignment->Horz = taRightJustify;
  tED->Properties->MaskKind = emkRegExpr;
  tED->Properties->EditMask = icsCalcMask(FIsFloat, FNode->AV["digits"].ToIntDef(0), FNode->AV["decimal"].ToIntDef(0), FNode->AV["min"]);
  tED->Properties->OnValidate         = FEditValidate;

  tED2->Properties->Alignment->Horz = taRightJustify;
  tED2->Properties->MaskKind = emkRegExpr;
  tED2->Properties->EditMask = icsCalcMask(FIsFloat, FNode->AV["digits"].ToIntDef(0), FNode->AV["decimal"].ToIntDef(0), FNode->AV["min"]);
  tED2->Properties->OnValidate         = FEditValidate;

  ED->Constraints->MinWidth = 60;
  ED->Left = Lab->Width+5;
  ED->Top = xSize.Top;
  if (xSize.Right+10 < 60)
   ED->Width = 60;
  else
   ED->Width = xSize.Right+10;
//   ED->Height = xSize.Bottom;
  ED->Parent =  this;
  Width = ED->Left+ED->Width;
  ED2->Constraints->MinWidth = 60;
  CMaxH = 58; CMinH = 58;
  Height = CMaxH;
  ED2->Top = ED->Top+ED->Height+2;
  ED2->Left = ED->Left;
  ED2->Width = ED->Width;
  ED2->Height = ED->Height;
  ED2->Parent =  this;
  if (!FCtrlOwner->TemplateField && !_TemplateValue(FNode).IsEmpty())
   {
     tED->Text = GetPart1(UnicodeString((wchar_t*)_TemplateValue(FNode)),';');
     tED2->Text = GetPart2(UnicodeString((wchar_t*)_TemplateValue(FNode)),';');
   }
  EDLab->Top  =  ED->Top + ED->Height/2 - EDLab->Height/2;
  EDLab2->Top =  ED2->Top + ED2->Height/2 - EDLab2->Height/2;

  EDLab->Left  =  ED->Left  - 2 - EDLab->Width;
  EDLab2->Left =  ED2->Left - 2 - EDLab2->Width;

  Lab->FocusControl = ED;
  if (Parent->ClassNameIs("TriDigitTmpl"))
   {
     Anchors.Clear();
     Anchors << akLeft << akRight << akTop;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetOnDependValues()
{
  if (FNode->AV["depend"].Length() == 9 && isRefMCtrl(_GUI(FNode->AV["depend"])))
   RefMCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(FNode);
  else if (FNode->AV["ref"].Length() == 4 && isRefCtrl(FNode))
   RefCtrl(FNode)->SetOnLab(FNode);
  FSetOnDependValues();
  tED->Properties->OnChange  = riDataChange;
  tED2->Properties->OnChange = riDataChange;
}
//---------------------------------------------------------------------------
__fastcall TriDigitTmpl::~TriDigitTmpl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   tED->Properties->OnChange    = NULL;
   tED2->Properties->OnChange = NULL;
   delete EDLab;
   delete EDLab2;
   delete tED;
   delete tED2;
}
//---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  ED->Left = ALeft;
  ED2->Left = ALeft;
  Width = ED->Left+ED->Width+4;
  EDLab->Left  =  ED->Left - 2 - EDLab->Width;
  EDLab2->Left =  ED2->Left- 2 - EDLab2->Width;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!TwoLine&&ED)  *AEDLeft = ED->Left;
  else               *AEDLeft = -1;
  if (!TwoLine&&Lab) *ALabR = Lab->Left+Lab->Width;
  else               *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::cResize(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     ED->Left     =  Width - ED->Width - 2;
     ED2->Left    =  Width - ED2->Width - 2;
     EDLab->Left  =  ED->Left-2 - EDLab->Width;
     EDLab2->Left =  ED2->Left-2 - EDLab2->Width;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriDigitTmpl::UpdateChanges()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  tED->Properties->OnChange((TObject*)ED);
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriDigitTmpl::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  if (ED)
   {
     if (ED->Enabled)
      {
        if ((tED->Text.Length())||(tED2->Text.Length()))
         {
           xVal += tED->Text+ ";"+ tED2->Text;
         }
      }
   }
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  Lab->Enabled = AEnable;
  ED->Enabled  = AEnable;
  ED2->Enabled = AEnable;
  tED->Style->Color   = (AEnable)?((Required)? FReqColor:clWindow):clBtnFace;
  tED2->Style->Color  = (AEnable)?((Required)? FReqColor:clWindow):clBtnFace;
  if (!AEnable)
   {
     tED->Text  = "";
     tED2->Text = "";
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
//  SetEnable(AEnable);
  if (wcschr(AVal.c_str(),';'))
   {
     tED->Text = GetPart1(AVal,';');
     tED2->Text = GetPart2(AVal,';');
   }
  else
   tED->Text = AVal;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else            Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  tED->Properties->OnChange  = riDataChange;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
  if (!TmpCtrl) return;
// ������ �������� ��� ��������� �������� �������� ����� choiceBD ----------------------------------
  Variant xVal; xVal.Empty();
  if (ED)
   {
     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     if (ED->Enabled||isVal)
      {
        if ((tED->Text.Length())||(tED2->Text.Length()))
         {
           xVal = Variant(tED->Text+";"+tED2->Text);
         }
      }
     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     _TemplateValue(FNode) = xVal;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if ((tED->Text.Trim().Length())||(tED2->Text.Trim().Length()))  Src = tED->Text.Trim()+";"+tED2->Text.Trim();
     FGetDataChange(FNode,Src,NULL);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
  if (SetLabList->Count > 0) // ��������� ������ ��� Label �������� �� choice ��� choiceBD
   {
/*
     if (FNode->CmpName("choice")) // ����� �� choice
      {
        int ItmIndex;
        if (TmpCtrl) ItmIndex  = tED_CB->ItemIndex;
        else         ItmIndex  = tED_CB->ItemIndex;
        if (ItmIndex >= (int)TmpCtrl)
         {
           for (int i = 0; i < SetLabList->Count; i++)
            {
              xRef = ((TTagNode*)SetLabList->Items[i])->AV["ref"].UpperCase(); // ref == uid.choice
              if (xRef == FUID)
               {
                 if (TmpCtrl) labCtrl(i)->SetLabel(tED_CB->Items->Strings[tED_CB->ItemIndex]);
                 else         labCtrl(i)->SetLabel(tED_CB->Items->Strings[tED_CB->ItemIndex]);
               }
            }
         }
      }
*/
   }
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if ((tED->Text.Trim().Length())||(tED2->Text.Trim().Length()))  Src = tED->Text.Trim()+";"+tED2->Text.Trim();
     FGetDataChange(FNode,Src,NULL);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriDigitTmpl::GetControl(int AIdx)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (TControl*)ED;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriDigitTmpl::GetFirstEnabledControl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriDigitTmpl::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return tED->Text+";"+tED2->Text;
}
//---------------------------------------------------------------------------
Variant __fastcall TriDigitTmpl::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if ((tED->Text+tED2->Text).Length()) return Variant(tED->Text+";"+tED2->Text);
  else                                 return Variant::Empty();
}
//---------------------------------------------------------------------------
bool __fastcall TriDigitTmpl::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED)
         {
           if (ED->Enabled)
            {
              if (!tED->Text.Length())  RC = false;
            }
         }
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
              UnicodeString ErrMessage;
              if (Lab) ErrMessage = Lab->Caption;
              else     ErrMessage = FNode->AV["name"];
              ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
              MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
              ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::FEditValidate(TObject *Sender, Variant &DisplayValue, TCaption &ErrorText, bool &Error)
{
  TReplaceFlags rFlag;
  rFlag << rfReplaceAll;
  if (FIsFloat)
   { //������������
     if (UnicodeString((wchar_t*)DisplayValue).Length())
      DisplayValue = Variant
                     (
                       StringReplace
                       (
                         StringReplace(UnicodeString((wchar_t*)DisplayValue),".",FormatSettings.DecimalSeparator,rFlag),
                         ",",
                         FormatSettings.DecimalSeparator,rFlag
                       )
                     );
   }
  Error = false;
}
//---------------------------------------------------------------------------
void __fastcall TriDigitTmpl::FSetEDRequired(bool AVal)
{
  FRequired = AVal;
  tED->Style->Color = (Required)? FReqColor:clWindow;
  tED2->Style->Color = (Required)? FReqColor:clWindow;
}
//---------------------------------------------------------------------------

