//---------------------------------------------------------------------------
#ifndef Reg_DMFH
#define Reg_DMFH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
#include "FIBDatabase.hpp"
#include "FIBDataSet.hpp"
#include "FIBQuery.hpp"
#include "pFIBDatabase.hpp"
#include "pFIBDataSet.hpp"
#include "pFIBQuery.hpp"
//---------------------------------------------------------------------------
#include "RegED.h"
#include "icsRegConstDef.h"
#include "ICSClassData.h"
/*#include <Classes.hpp>
#include <DB.hpp>
#include <DBCtrls.hpp>
#include <Registry.hpp>
#include <ADODB.hpp>
#include "FIBDatabase.hpp"
#include "FIBDataSet.hpp"
#include "FIBQuery.hpp"
#include "pFIBDatabase.hpp"
#include "pFIBDataSet.hpp"
#include "pFIBQuery.hpp"
//---------------------------------------------------------------------------
#include "RegED.h"
#include "icsRegConstDef.h"
#include "ICSClassData.h"  */
//---------------------------------------------------------------------------
//namespace IcsReg
//{
class PACKAGE TicsREG_DM : public TDataModule
{
__published:	// IDE-managed Components
        TpFIBDatabase *REG_DB;
    TpFIBQuery *quFree;
    TpFIBTransaction *trDefault;
    TpFIBTransaction *trList;
    TDataSource *srcList;
        TpFIBDataSet *quReg;
        TpFIBTransaction *trReg;
        TDataSource *srcReg;
        TpFIBTransaction *trFree;
        TpFIBQuery *quSearch;
        TpFIBTransaction *trSearch;
        TpFIBQuery *xquFree;
        TpFIBTransaction *xtrFree;
        TpFIBDataSet *quSelList;
        TpFIBTransaction *trSelList;
        TDataSource *srcSelList;
        TDataSource *srcAList;
        TDataSource *srcAReg;
        TDataSource *srcASelList;
        TpFIBDataSet *quList;
        void __fastcall DataModuleCreate(TObject *Sender);
        void __fastcall DataModuleDestroy(TObject *Sender);

private:
//    TClassHashTable *FClassHash;
public:
    TICSClassData *FClassData;
    UnicodeString   DTFormats[3];
    __int64         *SearchList;
    __int64         SearchIndex,SeachCount;
    TObject      *UnitList;
    TObject      *UnitCard;
    TTagNode     *RegUnit;
    TTagNode     *SelRegUnit;
    TTagNode     *Reg;
    TTagNode     *Classes;
    TStringList  *TreeClasses;
    UnicodeString __fastcall NewGUID();
    __int64 __fastcall GetMCode(UnicodeString AGenName);
    void __fastcall qtExec(UnicodeString ASQL, bool aCommit = false);
    void __fastcall qtStart(UnicodeString ASQL);
    void __fastcall qtExecCommit();
    void __fastcall qtRollback();
    __fastcall TicsREG_DM(TComponent* Owner);
};
//}; // end of namespace DataExchange
//using namespace IcsReg;
//---------------------------------------------------------------------------
extern PACKAGE TicsREG_DM *icsREG_DM;
extern PACKAGE UnicodeString __fastcall CorrectCount(int ACount);
//---------------------------------------------------------------------------
#endif

