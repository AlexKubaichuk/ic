//---------------------------------------------------------------------------
#ifndef SearchUnitH
#define SearchUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
//---------------------------------------------------------------------------
#include "ICSTemplate.h"
#include "SearchSetting.h"
/*#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Mask.hpp>
#include <StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
#include "cxGraphics.hpp"
#include "dxSkinSummer2008.hpp"
#include "dxSkinValentine.hpp"
#include "dxSkinXmas2008Blue.hpp"     */
//---------------------------------------------------------------------------
typedef void __fastcall (__closure *TSearchPat)(__int64);
class PACKAGE TSearchForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TcxButton *SearchSettingBtn;
        TcxButton *CancelBtn;
        TcxButton *OkBtn;
        TLabeledEdit *SearchED;
        TPanel *SEDPanel;
        TPanel *CompPanel;
        TcxButton *ExtBtn;
        void __fastcall SearchBtnClick(TObject *Sender);
        void __fastcall SearchEDKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall SearchSettingBtnClick(TObject *Sender);
        void __fastcall ExtBtnClick(TObject *Sender);
private:	// User declarations
        void __fastcall CreateExtFields();
        bool __fastcall CreateElList(TTagNode *itTag, UnicodeString &UID);
        bool __fastcall FOnTreeBtnClick(TTagNode *ItTag, bool &AIsGroup, UnicodeString &ACodeBeg, UnicodeString &ACodeEnd, UnicodeString &AExtCode, UnicodeString &AText, bool isClicked);
        bool FExtSearch;
        TICSTemplate *FTmpl;
        TStringList *FlCodeList;
        TRegistry_ICS* RegComp;
        TSearchSettingOptions *FSearchOpt;
//        TRegIniFile  *RegIni;
        UnicodeString FSTmplText,FGroupName, /*FFullTN,*/ FUnitTableName, FUnitTableAlias;
         TTagNode *UnitNode;
        TTagNode *DefNode;
        UnicodeString FExtWhere;
        UnicodeString /*TblName,*/FTabPref;
public:		// User declarations
        __fastcall TSearchForm(TComponent* Owner,UnicodeString ATabPref, TRegistry_ICS* ARegComp, TTagNode *ASearchDef, UnicodeString ATabName, UnicodeString ATabAlias, UnicodeString AExtWhere);
};
//---------------------------------------------------------------------------
extern PACKAGE TSearchForm *SearchForm;
//---------------------------------------------------------------------------
#endif

