//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "riBinary.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define dbED_ChB     ((TcxDBCheckBox*)ED)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
//---------------------------------------------------------------------------
__fastcall TriBinary::TriBinary(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                UnicodeString APref, TColor AReqColor,
                                TRegEDContainer *ACtrlOwner,
                                TDataSource* ASrc, bool IsApp)
                   :
                                TRegEDItem(AOwner, AParent,ANode,AquFree,
                                           APref, AReqColor, ACtrlOwner,
                                           ASrc, IsApp)
{
   ED = new TcxDBCheckBox(this);
   ED->Parent = this;
   dbED_ChB->OnClick = FDefaultDataChange;
//   dbED_ChB->Ctl3D = is3D;
   dbED_ChB->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
   dbED_ChB->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
   dbED_ChB->Transparent = true;

//   this->Color = clGray;

   dbED_ChB->Caption = FNode->AV["name"];
   dbED_ChB->Properties->Alignment = taLeftJustify;
   dbED_ChB->DataBinding->DataField = FFlName;
   dbED_ChB->DataBinding->DataSource = FSrc;
   dbED_ChB->Properties->ValueChecked = "1";
   dbED_ChB->Properties->ValueUnchecked = "0";
   CMaxH = 21; CMinH = 21; //Height = 18;
   if (IsAppend && SrcDSEditable())
     irquSetAsInt(SrcDS(),FFlName, (FNode->CmpAV("default","uncheck")? 0:1));
   if (IsTemplate(FNode))
    {
      if (SrcDSEditable()) irquSetAsInt(SrcDS(),FFlName,((int)_TemplateValue(FNode) == 0)? 0:1);
    }
   dbED_ChB->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
   dbED_ChB->Constraints->MinWidth = 60;
//   dbED_ChB->Width = dbED_ChB->Caption.Length()*7+10;
   dbED_ChB->Width = dbED_ChB->Canvas->TextWidth(dbED_ChB->Caption)+15;
   Height = 21;
   ED->Top = 1;
   ED->Left = 5;
/*
   if (Parent->ClassNameIs("TriBinary"))
    {
      Anchors.Clear();
      Anchors << akLeft << akRight << akTop;
    }
*/
}
//---------------------------------------------------------------------------
void __fastcall TriBinary::SetOnDependValues()
{
  if (FNode->AV["depend"].Length() == 9 && isRefMCtrl(_GUI(FNode->AV["depend"])))
   RefMCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(FNode);
  else if (FNode->AV["ref"].Length() == 4 && isRefCtrl(FNode))
   RefCtrl(FNode)->SetOnLab(FNode);
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
__fastcall TriBinary::~TriBinary()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   dbED_ChB->OnClick = NULL;
   dbED_ChB->Parent = NULL;
   delete dbED_ChB;
   ED = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TriBinary::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  Width = ED->Left+ED->Width+2;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriBinary::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  *AEDLeft = -1;
  *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriBinary::cResize(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     ED->Left = 2;
     ED->Width = Width-4;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriBinary::UpdateChanges()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  dbED_ChB->OnClick((TObject*)ED);//!!!
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriBinary::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  if (ED->Enabled)
   {
     if (ABinaryNames)
      {
        if (dbED_ChB->State != cbsGrayed)//!!!
         xVal += (dbED_ChB->Checked)?
                             FNode->AV["name"]:
                             FNode->AV["invname"];//!!!
      }
     else
      {
        if (dbED_ChB->State != cbsGrayed)//!!!
         xVal += UnicodeString((dbED_ChB->Checked)?
                             FMT(icsriBinaryCheckTxt).c_str() :
                             FMT(icsriBinaryUnCheckTxt).c_str());//!!!
      }
   }
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriBinary::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriBinary::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isEnabled = AEnable;
  ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  if (!(isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate)))
   {
     if (isEdit && !isDelegate && (!IsAppend || !isTemplate))
      {
        dbED_ChB->Checked = false;
        if (SrcDSEditable())
         irquSetAsInt(SrcDS(),"R"+AVUp(FNode,"uid"),0);
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriBinary::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnable(AEnable);
  if (SrcDSEditable()) irquSetAsInt(SrcDS(),FFlName,AVal.ToInt());
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriBinary::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  dbED_ChB->Caption = (xLab.Length())? xLab:UnicodeString("");
  dbED_ChB->Update();
/*  SIZE sz;
  TLabel *tmpLab = new TLabel(this);
  tmpLab->Parent = this;
  tmpLab->Transparent = true;
  tmpLab->Caption = xLab;
  tmpLab->Font->Assign(dbED_ChB->Style->Font);//!!!
  ::GetTextExtentPoint32(tmpLab->Canvas->Handle,xLab.c_str(),xLab.Length(),&sz);
  ED->Width = 25+sz.cx;
  this->Width = ED->Width+5;
  delete tmpLab;
*/
}
//---------------------------------------------------------------------------
void __fastcall TriBinary::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  dbED_ChB->OnClick = riDataChange;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriBinary::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TriBinary::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
}
//---------------------------------------------------------------------------
void __fastcall TriBinary::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriBinary::GetControl(int AIdx)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (TControl*)ED;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriBinary::GetFirstEnabledControl()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriBinary::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return IntToStr((int)dbED_ChB->Checked);
}
//---------------------------------------------------------------------------
Variant __fastcall TriBinary::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  Variant RC = Variant::Empty();
  try
   {
     if (FCtrlOwner->OnGetCompValue)
      {
        if (dbED_ChB->State != cbsGrayed) RC = Variant((int)dbED_ChB->Checked);
        FCtrlOwner->OnGetCompValue(FNode, RC);
      }
     else
      {
        if (dbED_ChB->State != cbsGrayed) RC = Variant((int)dbED_ChB->Checked);
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TriBinary::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED->Enabled && (dbED_ChB->State == cbsGrayed))
         RC = false;
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
           UnicodeString ErrMessage;
           ErrMessage = FNode->AV["name"];
           ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
           MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
           ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------

