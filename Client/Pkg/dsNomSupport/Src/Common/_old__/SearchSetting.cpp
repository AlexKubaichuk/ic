//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SearchSetting.h"
#include "icsRegConstDef.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxCheckBox"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLabel"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma resource "*.dfm"
TSearchSettingForm *SearchSettingForm;
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#                         TSearchSettingOptions                              #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
__fastcall TSearchSettingOptions::TSearchSettingOptions(TpFIBQuery *AQuery, UnicodeString AGrName)
{
  FQuery = AQuery;
  FGrName = AGrName.UpperCase();
  Load();
}
//---------------------------------------------------------------------------
void __fastcall TSearchSettingOptions::Load()
{
  FOpt.clear();
  try
   {
     if (!FQuery->Transaction->Active) FQuery->Transaction->StartTransaction();
     FQuery->Close();
     FQuery->SQL->Clear();
     FQuery->SQL->Text = "Select * From AOPTIONS Where NAME LIKE 'SO\\_"+FGrName+"\\_%' escape '\\'";
     FQuery->Prepare();
     FQuery->ExecQuery();
     UnicodeString FOptName;
     while (!FQuery->Eof)
      {
        FOptName = FQuery->FN("NAME")->AsString.UpperCase();
        FOptName = GetPart2(FOptName,'_');
        FOpt[FOptName] = (bool)FQuery->FN("VAL")->AsString.ToIntDef(0);
        FQuery->Next();
      }
   }
  __finally
   {
     FQuery->Transaction->Commit();
   }
}
//---------------------------------------------------------------------------
__fastcall TSearchSettingOptions::~TSearchSettingOptions()
{
}
//---------------------------------------------------------------------------
bool __fastcall TSearchSettingOptions::GetOpt(UnicodeString AId, bool ADef)
{
  bool RC = ADef;
  try
   {
     TBoolMap::iterator FRC = FOpt.find(AId.UpperCase());
     if (FRC != FOpt.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TSearchSettingOptions::SetOpt(UnicodeString AId, bool AVal)
{
  FOpt[AId.UpperCase()] = AVal;
}
//---------------------------------------------------------------------------
void __fastcall TSearchSettingOptions::Save()
{
  if (!FQuery->Transaction->Active) FQuery->Transaction->StartTransaction();
  try
   {
     int FCount;
     for (TBoolMap::iterator i = FOpt.begin(); i != FOpt.end(); i++)
      {
        FQuery->Close();
        FQuery->SQL->Text = "Select Count(*) as RCOUNT From AOPTIONS Where NAME = 'SO_"+FGrName+"_"+i->first+"'";
        FQuery->Prepare();
        FQuery->ExecQuery();
        FCount = FQuery->FN("RCOUNT")->AsInteger;
        FQuery->Close();
        if (FCount)
         FQuery->SQL->Text = "Update AOPTIONS Set VAL='"+IntToStr(((int)i->second))+"' Where NAME = 'SO_"+FGrName+"_"+i->first+"'";
        else
         FQuery->SQL->Text = "Insert Into AOPTIONS (NAME, VAL) Values('SO_"+FGrName+"_"+i->first+"', '"+IntToStr(((int)i->second))+"')";
        FQuery->Prepare();
        FQuery->ExecQuery();
      }
   }
  __finally
   {
     if (FQuery->Transaction->Active) FQuery->Transaction->Commit();
   }
}
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#                         TSearchSettingForm                                 #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
__fastcall TSearchSettingForm::TSearchSettingForm(TComponent* Owner, UnicodeString ATabPref, TRegistry_ICS* ARegComp, TTagNode *ASearchDef)
        : TForm(Owner)
{
  FDef = ASearchDef;
  Caption = FMT(icsRegSearchSettingCaption);
  FieldListLab->Caption = FMT(icsRegSearchSettingFieldListLabCaption);
  OkBtn->Caption = FMT(icsRegSearchSettingSaveBtnCaption);
  CancelBtn->Caption = FMT(icsRegSearchSettingCancelBtnCaption);
  FRegComp = ARegComp;
  if (ASearchDef->CmpName("unit"))
   FGroupName = ATabPref+ASearchDef->AV["tblname"];
  else
   FGroupName = "CLASS_"+ATabPref+ASearchDef->AV["uid"];
  FSearchOpt = new TSearchSettingOptions(FRegComp->DM->quFree, FGroupName);
  TTagNode *itSrcNode;
//  TcxTreeListNode *itDestNode;
  itSrcNode = ASearchDef->GetChildByName("description")->GetFirstChild()->GetFirstChild();
  UnicodeString FSearchFields = "";
  while (itSrcNode)
   {
     if (itSrcNode->GetTagByUID(itSrcNode->AV["ref"]))
      {
        if (itSrcNode->GetTagByUID(itSrcNode->AV["ref"])->CmpName("text,digit"))
         FSearchFields +=itSrcNode->AV["ref"]+",";
      }
     itSrcNode = itSrcNode->GetNext();
   }

  if (ASearchDef->CmpName("unit"))
   itSrcNode = ASearchDef->GetChildByName("unitgroup");
  else
   itSrcNode = ASearchDef->GetFirstChild()->GetNext();
  sElemenList->Clear();
  while (itSrcNode)
   {
     if (!itSrcNode->CmpAV("uid",FSearchFields))
      NodeToTreeNode(itSrcNode, sElemenList->Root->AddChild(), FSearchFields);
     itSrcNode = itSrcNode->GetNext();
   }
}
//---------------------------------------------------------------------------
bool __fastcall TSearchSettingForm::CheckNode(UnicodeString UID)
{
  bool RC = false;
  try
   {
     TTagNode *flDef = FDef->GetTagByUID(UID);
     TTagNode *condDef;
     TTagNode *condItem;
     if (flDef)
      {
        condDef = flDef->GetChildByName("actuals");
        if (condDef)
         {
           condItem = condDef->GetFirstChild();
           bool CRC = true;
           while (condItem)
            {
              CRC &= GetNodeValue(condItem->AV["ref"], flDef->AV["name"]);
              if (!CRC) break;
              condItem = condItem->GetNext();
            }
           RC = CRC;
         }
        else
         RC = true;
      }
     else
      _MSG_ERR("�� ������ �������, ���='"+UID+"'","������");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TSearchSettingForm::GetNodeValue(UnicodeString UID, UnicodeString AName, bool ASilent)
{
  bool RC = false;
  try
   {
     TTLNodeMap::iterator FRC = TreeNodes.find(UID.UpperCase());
     TcxTreeListNode *FCheckNode = NULL;
     if (FRC != TreeNodes.end())
      {
        FCheckNode = FRC->second;
        if (!FCheckNode->Values[1].IsNull() && !FCheckNode->Values[1].IsEmpty())
         RC = (bool)FCheckNode->Values[1];
      }
     if (!ASilent)
      {
        if (!RC)
         {
           TTagNode *tmp = FDef->GetTagByUID(UID);
           if (tmp)
            {
              _MSG_ERR("��������� ������� ������� �������� ���� '"+tmp->AV["name"]+"', �.�. �� ���� ������� ���� '"+AName+"'.","������");
              if (FCheckNode)
               {
                 FCheckNode->MakeVisible();
                 FCheckNode->Focused = true;
               }

            }
           else
            _MSG_ERR("�� ������ �������, ���='"+UID+"'","������");
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TSearchSettingForm::CheckInput()
{
  bool RC = false;
  try
   {
     bool FRC = true;
     for (int i = 0; i < sElemenList->Count; i++)
      {
        if (sElemenList->Items[i]->Texts[3].ToIntDef(1))
         {
           if (GetNodeValue(sElemenList->Items[i]->Texts[2], "", true))
            {
              if (!CheckNode(sElemenList->Items[i]->Texts[2]))
               {
                 FRC = false;
                 break;
               }
            }
         }
      }
     RC = FRC;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TSearchSettingForm::OkBtnClick(TObject *Sender)
{
  if (CheckInput())
   {
     try
      {
        for (int i = 0; i < sElemenList->Count; i++)
         {
           if (sElemenList->Items[i]->Texts[3].ToIntDef(1))
            {
              if (!sElemenList->Items[i]->Values[1].IsNull() && !sElemenList->Items[i]->Values[1].IsEmpty())
               FSearchOpt->SetOpt(sElemenList->Items[i]->Texts[2],
                             (bool)sElemenList->Items[i]->Values[1]);
            }
         }
      }
     __finally
      {
        FSearchOpt->Save();
      }
     ModalResult = mrOk;
   }
}
//---------------------------------------------------------------------------
void __fastcall TSearchSettingForm::FormDestroy(TObject *Sender)
{
  delete FSearchOpt;
}
//---------------------------------------------------------------------------
void __fastcall TSearchSettingForm::NodeToTreeNode(TTagNode *ASrc, TcxTreeListNode *ADest, UnicodeString ASearchFields)
{
  if (!ASrc->CmpAV("uid",ASearchFields))
   {
     ADest->DeleteChildren();
     ADest->Texts[0]  = ASrc->AV["name"];
     if (ASrc->CmpName("unitgroup"))
      ADest->Texts[1] = "";
     else
      ADest->Values[1] = FSearchOpt->GetOpt(ASrc->AV["uid"]);
     ADest->Texts[2]  = ASrc->AV["uid"].UpperCase();
     TreeNodes[ASrc->AV["uid"].UpperCase()] = ADest;
     if (ASrc->CmpName("unitgroup"))
      {
        ADest->Texts[3]  = "0";
        TTagNode *itNode = ASrc->GetFirstChild();
        while (itNode)
         {
           if (!(itNode->AV["depend"].Length() && itNode->CmpAV("isedit","no")))
            {
              if (itNode->CmpName("extedit") || (!itNode->CmpName("extedit")&&(itNode->CmpAV("inlist","list,list_ext")||itNode->CmpName("unitgroup"))))
               NodeToTreeNode(itNode, ADest->AddChild(), ASearchFields);
            }
           itNode = itNode->GetNext();
         }
      }
   }
  else
   {
     ADest->Texts[0]  = ASrc->AV["name"];
     ADest->Texts[3]  = "0";
   }
}
//---------------------------------------------------------------------------
void __fastcall TSearchSettingForm::sElemenListKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
  if (sElemenList->FocusedNode)
   {
     if (sElemenList->FocusedNode->Count)
      {
        if (Key == VK_LEFT)
         sElemenList->FocusedNode->Collapse(false);
        else if (Key == VK_RIGHT)
         sElemenList->FocusedNode->Expand(false);
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TSearchSettingForm::ValColGetEditProperties(
      TcxTreeListColumn *Sender, TcxTreeListNode *ANode,
      TcxCustomEditProperties *&EditProperties)
{
  if (ANode)
   {
     if (ANode->Texts[3].ToIntDef(1))
       EditProperties = SetValChB->Properties;
   }
}
//---------------------------------------------------------------------------
void __fastcall TSearchSettingForm::sElemenListStylesGetContentStyle(TcxCustomTreeList *Sender,
          TcxTreeListColumn *AColumn, TcxTreeListNode *ANode, TcxStyle *&AStyle)

{
  if (ANode)
   {
     if (!ANode->Texts[3].ToIntDef(1))
      AStyle = headStyle;
   }
}
//---------------------------------------------------------------------------

void __fastcall TSearchSettingForm::sElemenListEditing(TcxCustomTreeList *Sender,
          TcxTreeListColumn *AColumn, bool &Allow)
{
  Allow = false;
  if (sElemenList->FocusedNode)
   {
     Allow = sElemenList->FocusedNode->Texts[3].ToIntDef(1);
   }
}
//---------------------------------------------------------------------------
