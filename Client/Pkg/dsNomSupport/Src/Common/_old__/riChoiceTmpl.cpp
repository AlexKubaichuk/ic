//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riChoiceTmpl.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED_CB      ((TcxComboBox*)ED)
//---------------------------------------------------------------------------
__fastcall TriChoiceTmpl::TriChoiceTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref,  TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  UnicodeString AVal)
           :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                      APref,  AReqColor,
                                      ACtrlOwner,  AVal)
{
  Lab = new TLabel(this);  Lab->Parent = this;
  Lab->Transparent = true;
  Lab->Top = 2;            Lab->Left = 4;
  Lab->Caption = FNode->AV["name"]+":";
  ED = new TcxComboBox(this);
  tED_CB->Properties->DropDownListStyle = lsEditFixedList;
  tED_CB->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
  tED_CB->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
  if (Required) tED_CB->Style->Color = FReqColor;
  ED->Parent = this;
  tED_CB->Properties->Items->Clear();
  TTagNode *itxNode = FNode->GetFirstChild();
  int MaxW = 16;
  tED_CB->Properties->Items->AddObject("",(TObject*)16777215);
  while(itxNode)
   {
     if (itxNode->CmpName("choicevalue"))
      {
        tED_CB->Properties->Items->AddObject(itxNode->AV["name"].c_str(),(TObject*)itxNode->AV["value"].ToInt());
        if (MaxW < tED_CB->Canvas->TextWidth(itxNode->AV["name"])) MaxW = tED_CB->Canvas->TextWidth(itxNode->AV["name"]);
      }
     itxNode = itxNode->GetNext();
   }
//  ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  tED_CB->Properties->OnChange = riDataChange;
  ED->Constraints->MinWidth = 60;
  ED->Width = MaxW+GetSystemMetrics(SM_CXVSCROLL)+GetSystemMetrics(SM_CXBORDER)*2+10;
  if (ED->Width < 60) ED->Width = 60;
  ED->Top = 0;
  ED->Height = 21;
  Height = 25;
  if (Lab->Width > (AParent->Width-2))
   {
     Lab->Font->Size = Lab->Font->Size - 1;
     Lab->Update();
     if (Lab->Width > (AParent->Width - 2))
      Lab->Width = AParent->Width-2;
   }
  ED->Left = Lab->Left + Lab->Width + 5;
  if ((ED->Left + ED->Width) > (AParent->Width - 2))
   {
     TwoLine = true;  Lab->Top = 0;
     ED->Left = Lab->Left+5;  ED->Top = Lab->Height;
     CMaxH = 38; CMinH = 38;
     Height = CMaxH;
     if ((ED->Left + ED->Width) > (AParent->Width - 2))
      ED->Width = AParent->Width - 4 - ED->Left;
   }
   if (Lab&&ED)
    Lab->FocusControl = ED;
   if (Parent->ClassNameIs("TriChoiceTmpl"))
    {
      Anchors.Clear();
      Anchors << akLeft << akRight << akTop;
    }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetOnDependValues()
{
  if (FNode->AV["depend"].Length() == 9 && isRefMCtrl(_GUI(FNode->AV["depend"])))
   RefMCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(FNode);
  else if (FNode->AV["ref"].Length() == 4 && isRefCtrl(FNode))
   RefCtrl(FNode)->SetOnLab(FNode);
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
__fastcall TriChoiceTmpl::~TriChoiceTmpl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  tED_CB->Properties->OnChange = NULL;
  delete tED_CB;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine)
   {
     if ((ALeft+ED->Width) < Width -6)
      ED->Left = ALeft;
   }
  Width = ED->Left+ED->Width+4;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (TwoLine)
   {
     *AEDLeft = -1;
     *ALabR = -1;
   }
  else
   {
     *AEDLeft = ED->Left;
     *ALabR = Lab->Left+Lab->Width;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::cResize(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     if (TwoLine) {ED->Left = 2; ED->Width = Width-4;}
     else          ED->Left = Width - ED->Width - 2;
     TNotifyEvent FSaveOnChange = tED_CB->Properties->OnChange;
     int FSaveIndex = tED_CB->ItemIndex;
     tED_CB->ItemIndex = -1;
     tED_CB->ItemIndex = FSaveIndex;
     tED_CB->Properties->OnChange = FSaveOnChange;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceTmpl::UpdateChanges()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  tED_CB->Properties->OnChange((TObject*)ED);
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTmpl::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
     if (ED)
      {
        if (ED->Enabled)
         {
              if (tED_CB->ItemIndex >= 1)     xVal += tED_CB->Properties->Items->Strings[tED_CB->ItemIndex];
         }
      }
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  Lab->Enabled = AEnable;
  ED->Enabled = AEnable;

  tED_CB->Style->Color = (AEnable)?((Required)? FReqColor:clWindow):clBtnFace;
  if (!AEnable)
   tED_CB->ItemIndex = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
//  SetEnable(AEnable);
  if (AVal.Length())
   {
     int ind = tED_CB->Properties->Items->IndexOfObject((TObject*)AVal.ToInt());
     if (ind != -1) tED_CB->ItemIndex = ind;
   }
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else               Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetOnLab(TTagNode* ANode)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetLabList->Add(ANode);
  tED_CB->Properties->OnChange = riDataChange;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  tED_CB->Properties->OnChange  = riDataChange;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
  if (!TmpCtrl) return;
// ������ �������� ��� ��������� �������� �������� ����� choiceBD ----------------------------------
  Variant xVal; xVal.Empty();
  if (ED)
   {
     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     if (ED->Enabled||isVal)
      {
        if (tED_CB->ItemIndex >= 1)     xVal = Variant((int)tED_CB->Properties->Items->Objects[tED_CB->ItemIndex]);
      }
     //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     _TemplateValue(FNode) = xVal;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "-1";
     if (tED_CB->ItemIndex >= 1)     Src = IntToStr((int)tED_CB->Properties->Items->Objects[tED_CB->ItemIndex]);
     FGetDataChange(FNode,Src,NULL);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
  if (SetLabList->Count > 0) // ��������� ������ ��� Label �������� �� choice ��� choiceBD
   {
     if (FNode->CmpName("choice")) // ����� �� choice
      {
        int ItmIndex;
        if (TmpCtrl) ItmIndex  = tED_CB->ItemIndex;
        else         ItmIndex  = tED_CB->ItemIndex;
        if (ItmIndex >= (int)TmpCtrl)
         {
           for (int i = 0; i < SetLabList->Count; i++)
            {
              xRef = ((TTagNode*)SetLabList->Items[i])->AV["ref"].UpperCase(); // ref == uid.choice
              if (xRef == FUID)
               {
                 if (TmpCtrl) labCtrl(i)->SetLabel(tED_CB->Properties->Items->Strings[tED_CB->ItemIndex]);
                 else         labCtrl(i)->SetLabel(tED_CB->Properties->Items->Strings[tED_CB->ItemIndex]);
               }
            }
         }
      }
   }
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "-1";
     if (tED_CB->ItemIndex >= 1)     Src = IntToStr((int)tED_CB->Properties->Items->Objects[tED_CB->ItemIndex]);
     FGetDataChange(FNode,Src,NULL);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriChoiceTmpl::GetControl(int AIdx)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (TControl*)ED;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriChoiceTmpl::GetFirstEnabledControl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTmpl::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (tED_CB->ItemIndex >= 1)
   return IntToStr(((int)tED_CB->Properties->Items->Objects[tED_CB->ItemIndex]));
  else
   return "";
}
//---------------------------------------------------------------------------
Variant __fastcall TriChoiceTmpl::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (tED_CB->ItemIndex >= 1) return Variant((int)tED_CB->Properties->Items->Objects[tED_CB->ItemIndex]);
  else                        return Variant::Empty();
}
//---------------------------------------------------------------------------
bool __fastcall TriChoiceTmpl::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED->Enabled)
         RC = !(tED_CB->ItemIndex < 1);
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
           UnicodeString ErrMessage;
           if (Lab) ErrMessage = Lab->Caption;
           else     ErrMessage = FNode->AV["name"];
           ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
           MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
           ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::FSetEDRequired(bool AVal)
{
  FRequired = AVal;
  tED_CB->Style->Color = (Required)? FReqColor:clWindow;
}
//---------------------------------------------------------------------------

