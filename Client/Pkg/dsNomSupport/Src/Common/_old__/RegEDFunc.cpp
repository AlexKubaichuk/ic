//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "RegEDFunc.h"
#include "math.h"
#include "icsRegConstDef.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------
void __fastcall irExecSelect(UnicodeString ASQL, TDataSet *AQuery)
{
  if (AQuery->ClassNameIs("TADODataSet"))
   {
     TADODataSet *FQuery = (TADODataSet*)AQuery;
     if (FQuery->Active) FQuery->Close();
     FQuery->CommandType = cmdText;
     FQuery->CommandText = ASQL;
     FQuery->Open();
   }
  else if (AQuery->ClassNameIs("TpFIBQuery"))
   {
     TpFIBQuery *FQuery = (TpFIBQuery*)AQuery;
     FQuery->Close();
     if (FQuery->Transaction->Active) FQuery->Transaction->Rollback();
     FQuery->SQL->Clear();
     FQuery->SQL->Text = ASQL;
     if (!FQuery->Transaction->Active) FQuery->Transaction->StartTransaction();
     FQuery->Prepare();
     FQuery->ExecQuery();
   }
}
//---------------------------------------------------------------------------
void __fastcall irExecClose(TDataSet *AQuery,bool ACommit)
{
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        TADODataSet *FQuery = (TADODataSet*)AQuery;
        if (FQuery->Active) FQuery->Close();
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        TpFIBQuery *FQuery = (TpFIBQuery*)AQuery;
        FQuery->Close();
        if (ACommit)
         {
           if (FQuery->Transaction->Active) FQuery->Transaction->Commit();
         }
        else
         {
           if (FQuery->Transaction->Active) FQuery->Transaction->Rollback();
         }
      }
}
//---------------------------------------------------------------------------
void __fastcall irExecProc(UnicodeString AProcName, TDataSet *AQuery, UnicodeString AParamName, Variant AParam)
{
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        TADODataSet *FQuery = (TADODataSet*)AQuery;
        if (FQuery->Active) FQuery->Close();
        FQuery->CommandType = cmdStoredProc;
        FQuery->CommandText = AProcName.c_str();
        FQuery->Parameters->Refresh();
        FQuery->Parameters->ParamByName(AParamName.c_str())->Value = AParam;
        FQuery->Open();
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
         TpFIBQuery *FQuery = (TpFIBQuery*)AQuery;
         Variant DDD[1];
         DDD[0] = AParam;
         if (FQuery->Transaction->Active) FQuery->Transaction->Rollback();
         FQuery->Close();
         FQuery->SQL->Clear();
         FQuery->Transaction->StartTransaction();
         FQuery->ExecProcedure(AProcName.c_str(),DDD,0);
      }
}
//---------------------------------------------------------------------------
void __fastcall irExecCloseProc(TDataSet *AQuery,bool ACommit)
{
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        TADODataSet *FQuery = (TADODataSet*)AQuery;
        FQuery->Close();
        FQuery->CommandType = cmdText;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        TpFIBQuery *FQuery = (TpFIBQuery*)AQuery;
        FQuery->Close();
        if (ACommit)
         {
           if (FQuery->Transaction->Active) FQuery->Transaction->Commit();
         }
        else
         {
           if (FQuery->Transaction->Active) FQuery->Transaction->Rollback();
         }
      }
}
//---------------------------------------------------------------------------
bool __fastcall irIsEof(TDataSet *AQuery)
{
   if (AQuery->ClassNameIs("TADODataSet"))
    {
      TADODataSet *FQuery = (TADODataSet*)AQuery;
      return FQuery->Eof;
    }
   else if (AQuery->ClassNameIs("TpFIBQuery"))
    {
      TpFIBQuery *FQuery = (TpFIBQuery*)AQuery;
      return FQuery->Eof;
    }
   return true;
}
//---------------------------------------------------------------------------
UnicodeString  __fastcall irquFieldStr(TDataSet *AQuery, int AIdx)
{
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        return ((TADODataSet*)AQuery)->Fields->Fields[AIdx]->AsString;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        return ((TpFIBQuery*)AQuery)->Fields[AIdx]->AsString;
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        return ((TpFIBDataSet*)AQuery)->Fields->Fields[AIdx]->AsString;
      }
     else return "";
}
//---------------------------------------------------------------------------
UnicodeString  __fastcall irquFieldName(TDataSet *AQuery, int AIdx)
{
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        return ((TADODataSet*)AQuery)->Fields->Fields[AIdx]->Name;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        return ((TpFIBQuery*)AQuery)->Fields[AIdx]->Name;
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        return ((TpFIBDataSet*)AQuery)->Fields->Fields[AIdx]->Name;
      }
     else return "";
}
//---------------------------------------------------------------------------
void  __fastcall irquNext(TDataSet *AQuery)
{
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        ((TADODataSet*)AQuery)->Next();
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        ((TpFIBQuery*)AQuery)->Next();
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        ((TpFIBDataSet*)AQuery)->Next();
      }
}
//---------------------------------------------------------------------------
int  __fastcall irquFieldCount(TDataSet *AQuery)
{
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        return ((TADODataSet*)AQuery)->FieldCount;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        return ((TpFIBQuery*)AQuery)->FieldCount();
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        return ((TpFIBDataSet*)AQuery)->FieldCount;
      }
     else return 0;
}
//---------------------------------------------------------------------------
UnicodeString  __fastcall irquAsStr(TDataSet *AQuery, UnicodeString AName)
{
     if (irquIsNull(AQuery, AName))
      return  "";
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        return ((TADODataSet*)AQuery)->FieldByName(AName.c_str())->AsString;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        return ((TpFIBQuery*)AQuery)->FN(AName.c_str())->AsString;
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        /*BUGFIX FN*/
        return ((TpFIBDataSet*)AQuery)->FieldByName(AName.c_str())->AsString;
      }
     else return "";
}
//---------------------------------------------------------------------------
void  __fastcall irquSetAsStr(TDataSet *AQuery, UnicodeString AName, UnicodeString AVal)
{
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        ((TADODataSet*)AQuery)->FieldByName(AName.c_str())->AsString = AVal;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        ((TpFIBQuery*)AQuery)->FN(AName.c_str())->AsString = AVal;
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        /*BUGFIX FN*/
        ((TpFIBDataSet*)AQuery)->FieldByName(AName.c_str())->AsString = AVal;
      }
}
//---------------------------------------------------------------------------
__int64 __fastcall irquAsInt(TDataSet *AQuery, UnicodeString AName)
{
     if (irquIsNull(AQuery, AName))
      return 0;
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        return ((TADODataSet*)AQuery)->FieldByName(AName.c_str())->AsInteger;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        return ((TpFIBQuery*)AQuery)->FN(AName.c_str())->AsInteger;
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        /*BUGFIX FN*/
        return ((TpFIBDataSet*)AQuery)->FieldByName(AName.c_str())->AsInteger;
      }
     else return 0;
}
//---------------------------------------------------------------------------
void  __fastcall irquSetAsInt(TDataSet *AQuery, UnicodeString AName, int AVal)
{
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        ((TADODataSet*)AQuery)->FieldByName(AName.c_str())->AsInteger = AVal;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        ((TpFIBQuery*)AQuery)->FN(AName.c_str())->AsInteger = AVal;
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        /*BUGFIX FN*/
        ((TpFIBDataSet*)AQuery)->FieldByName(AName.c_str())->AsInteger = AVal;
      }
}
//---------------------------------------------------------------------------
float  __fastcall irquAsFloat(TDataSet *AQuery, UnicodeString AName)
{
     if (irquIsNull(AQuery, AName))
      return 0.0;
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        return ((TADODataSet*)AQuery)->FieldByName(AName.c_str())->AsFloat;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        return ((TpFIBQuery*)AQuery)->FN(AName.c_str())->AsFloat;
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        /*BUGFIX FN*/
        return ((TpFIBDataSet*)AQuery)->FieldByName(AName.c_str())->AsFloat;
      }
     else return 0.0;
}
//---------------------------------------------------------------------------
void  __fastcall irquSetAsFloat(TDataSet *AQuery, UnicodeString AName, float AVal)
{
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        ((TADODataSet*)AQuery)->FieldByName(AName.c_str())->AsFloat = AVal;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        ((TpFIBQuery*)AQuery)->FN(AName.c_str())->AsFloat = AVal;
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        /*BUGFIX FN*/
        ((TpFIBDataSet*)AQuery)->FieldByName(AName.c_str())->AsFloat = AVal;
      }
}
//---------------------------------------------------------------------------
Variant  __fastcall irquAsVar(TDataSet *AQuery, UnicodeString AName)
{
     if (irquIsNull(AQuery, AName))
      return Variant::Empty();
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        return ((TADODataSet*)AQuery)->FieldByName(AName.c_str())->AsVariant;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        return ((TpFIBQuery*)AQuery)->FN(AName.c_str())->AsVariant;
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        /*BUGFIX FN*/
        return ((TpFIBDataSet*)AQuery)->FieldByName(AName.c_str())->AsVariant;
      }
     else return Variant::Empty();
}
//---------------------------------------------------------------------------
void  __fastcall irquSetAsVar(TDataSet *AQuery, UnicodeString AName, Variant AVal)
{
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        ((TADODataSet*)AQuery)->FieldByName(AName.c_str())->AsVariant = AVal;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        ((TpFIBQuery*)AQuery)->FN(AName.c_str())->AsVariant = AVal;
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        /*BUGFIX FN*/
        ((TpFIBDataSet*)AQuery)->FieldByName(AName.c_str())->AsVariant = AVal;
      }
}
//---------------------------------------------------------------------------
TDateTime  __fastcall irquAsDate(TDataSet *AQuery, UnicodeString AName)
{
     if (irquIsNull(AQuery, AName))
      return TDateTime(0);
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        return ((TADODataSet*)AQuery)->FieldByName(AName.c_str())->AsDateTime;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        return ((TpFIBQuery*)AQuery)->FN(AName.c_str())->AsDateTime;
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        /*BUGFIX FN*/
        return ((TpFIBDataSet*)AQuery)->FieldByName(AName.c_str())->AsDateTime;
      }
     else return TDateTime(0);
}
//---------------------------------------------------------------------------
void  __fastcall irquSetAsDate(TDataSet *AQuery, UnicodeString AName, TDateTime AVal)
{
     if (AQuery->ClassNameIs("TADODataSet")) 
      {
        ((TADODataSet*)AQuery)->FieldByName(AName.c_str())->AsDateTime = AVal;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        ((TpFIBQuery*)AQuery)->FN(AName.c_str())->AsDateTime = AVal;
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        /*BUGFIX FN*/
        ((TpFIBDataSet*)AQuery)->FieldByName(AName.c_str())->AsDateTime = AVal;
      }
}
//---------------------------------------------------------------------------
void  __fastcall irquSetAsNull(TDataSet *AQuery, UnicodeString AName)
{
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        ((TADODataSet*)AQuery)->FieldByName(AName.c_str())->Clear();
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        ((TpFIBQuery*)AQuery)->FN(AName.c_str())->Clear();
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        /*BUGFIX FN*/
        ((TpFIBDataSet*)AQuery)->FieldByName(AName.c_str())->Clear();
      }
}
//---------------------------------------------------------------------------
bool  __fastcall irquIsNull(TDataSet *AQuery, UnicodeString AName)
{
     if (AQuery->ClassNameIs("TADODataSet"))
      {
        return ((TADODataSet*)AQuery)->FieldByName(AName.c_str())->IsNull;
      }
     else if (AQuery->ClassNameIs("TpFIBQuery"))
      {
        return ((TpFIBQuery*)AQuery)->FN(AName.c_str())->IsNull;
      }
     else if (AQuery->ClassNameIs("TpFIBDataSet"))
      {
        /*BUGFIX FN*/
        return ((TpFIBDataSet*)AQuery)->FieldByName(AName.c_str())->IsNull;
      }
     else return false;
}
//---------------------------------------------------------------------------
Variant& __fastcall _TemplateValue(TTagNode *ANode)
{
  return ANode->GetAttrByName("uid")->cValue;
}
//---------------------------------------------------------------------------
void __fastcall ClearClassCBItems(TStrings* AClassCB)
{
  try
   {
     for (int i = 0; i < AClassCB->Count; i++)
      {
        if (AClassCB->Strings[i].Trim().Length() && ((int)AClassCB->Objects[i]) != 16777215)
         {
           TObject *tmpList = AClassCB->Objects[i];
           try
            {
              if (tmpList->ClassNameIs("TStringList"))
               delete (TStringList*)tmpList;
            }
           catch(...)
            {
            }
         }
      }
     AClassCB->Clear();
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------

void __fastcall FInvadeClassCB(TStrings* ClassCB, __int64 ClassCode, bool isFirstNULL,TTagNode *CNode, bool allFld, TDataSet *quFree, UnicodeString APref, UnicodeString ACodeVals, TTagNode *ADefRoot)
{
  // ���� allFld = true  - ��������� �������� ���� �����
  //   i-� ������ ClassCB - ��������� ��������
  //   i-� ������ ClassCB - TStringList - ���:
  //                        0-� ������ = ""
  //                        0-� ������ = �������� ���� "CODE"
  //                        ��� i �� 1 �� Count
  //                         i-� ������ =  ���_����=���_#_��������
  // �����
  //   i-� ������ ClassCB - ��������� ��������
  //   i-� ������ ClassCB = �������� ���� "CODE"

  TTagNode *itNode = CNode->GetChildByName("description")->GetFirstChild()->GetFirstChild();
  TStringList *OutFl = NULL;      // ������ ����� �������� ������� ������������ ���
                                  // ������������� �������� ������
  TStringList *FlList = NULL;     // ����� ������ �����
  TStringList *chFlList = NULL;   // ������ ����� ���� "������" ������ ������� ��������
                                  // �������� ������������ ���� � ������ ��� ���=��������
  TStringList *chBDFlList = NULL; // ������ ����� ���� "������ �� ��" ������ ������� ��������
                                  // �������� ������������ ���� � ������ ��� ���=��������
  try
   {
     OutFl = new TStringList;
     FlList = new TStringList;
     chFlList = new TStringList;
     chBDFlList = new TStringList;
     // ��������� ������ �������� �����
     int isAddName;
     TTagNode *tmpNode;
     while (itNode)
      {
        isAddName = 1;
        tmpNode = ADefRoot->GetTagByUID(itNode->AV["ref"]);
        if (tmpNode)
         if (tmpNode->CmpName("text"))
          if (tmpNode->CmpAV("cast","up1st"))
           isAddName = 0;
        OutFl->AddObject(itNode->AV["ref"].UpperCase(),(TObject*)isAddName);
        itNode = itNode->GetNext();
      }
     UnicodeString xSelect, xWhere ,xOrder; xSelect = ""; xWhere = ""; xOrder = "";
     itNode = CNode->GetChildByName("description")->GetNext();
     int xIndex;
     UnicodeString fName;
     while (itNode)
      {
        fName = "R"+itNode->AV["uid"].UpperCase();
        if (itNode->CmpName("choice"))
         { // ��������� ������ ���=�������� ��� �������� ���� "������"
           TStringList *__tmpL;
           TTagNode *tmpN = itNode->GetFirstChild();
           chFlList->AddObject(fName,(TObject*) new TStringList);
           __tmpL = (TStringList*)chFlList->Objects[chFlList->Count-1];
           while (tmpN)
            {
              if (tmpN->CmpName("choicevalue"))
               __tmpL->Add((tmpN->AV["value"]+"="+tmpN->AV["name"]).c_str());
              tmpN = tmpN->GetNext();
            }
         }
        if (itNode->CmpName("choiceBD"))
         {
           TStringList *__tmpL;
           TTagNode *tmpN = ADefRoot->GetTagByUID(itNode->AV["ref"]);
           chBDFlList->AddObject(fName,(TObject*) new TStringList);
           __tmpL = (TStringList*)chBDFlList->Objects[chBDFlList->Count-1];
           FInvadeClassCB(__tmpL, 0, false ,tmpN, false,quFree,APref,"",ADefRoot);
         }
        if (itNode->CmpName("class")) xWhere = "Where "+fName+"="+IntToStr(ClassCode);
        else
         {
           if (FlList->IndexOf(fName.c_str()) == -1)
            FlList->Add(fName.c_str());
         }
/*
     if (FAlias.Length())
      tmpFlName = FAlias+"."+FieldList->Strings[i].UpperCase();
     else
      tmpFlName = FieldList->Strings[i].UpperCase();
     if (!xSelect.Pos(tmpFlName))
      xSelect += tmpFlName+",";
*/
        if (!xSelect.Pos(fName))
         xSelect += fName+",";
        xIndex = OutFl->IndexOf(itNode->AV["uid"].UpperCase());
        if (xIndex >= 0) OutFl->Strings[xIndex] = fName+"="+itNode->AV["name"];
        itNode = itNode->GetNext();
      }
     for (int i = 0; i < OutFl->Count; i++)
      {
        if (i < OutFl->Count-1) xOrder += OutFl->Names[i]+",";
        else                    xOrder += OutFl->Names[i];
      }
     if (ACodeVals.Length())
      {
        if (!xWhere.Length()) xWhere += "Where ";
        else                  xWhere += " and ";
        xWhere += " CODE in ("+ACodeVals+")";
      }
     xSelect += "CODE";
     ClearClassCBItems(ClassCB);
     irExecSelect("Select "+xSelect+" From CLASS_"+APref+CNode->AV["uid"].UpperCase()+" "+xWhere+" Order By "+xOrder,quFree);
     if (isFirstNULL) ClassCB->AddObject("",(TObject*)16777215);
     UnicodeString xFieldValue, xFLab, xCommon, flName, xVal, xName;
     TStringList *__tmpL;
     int Ind;
     while (!irIsEof(quFree))
      {
       xCommon = "";
       for (int i = 0; i < OutFl->Count; i++)
        { // ��������� ��������� ������ ��������������
          flName = OutFl->Names[i];
          Ind = chFlList->IndexOf(flName);
          if (Ind != -1) // ���� ���� � ������ ����� � ����� "������" ?
           { // ��������� ������ � �������: ���_����=���_#_��������
             __tmpL = (TStringList*)chFlList->Objects[Ind];
             xFieldValue = __tmpL->Values[irquAsStr(quFree,flName)];
           }
          else
           {
             Ind = chBDFlList->IndexOf(flName);
             if (Ind != -1) // ���� ���� � ������ ����� � ����� "������ �� ��" ?
              { // ��������� �������� ���� � ����� "������ �� ��"
                __tmpL = (TStringList*)chBDFlList->Objects[Ind];
                Ind = __tmpL->IndexOfObject((TObject*)irquAsInt(quFree,flName));
                if (Ind != -1) xFieldValue = __tmpL->Strings[Ind];
                else           ShowMessage(FMT(icsRegEDFuncErrorGetValues));
              }
             else
              xFieldValue = irquAsStr(quFree,flName); // �������� ����
           }
          if (!xCommon.Length())
           xCommon = xFieldValue;
          else
           {
             if (xFieldValue.Trim().Length())
              {
                if (((int)OutFl->Objects[i]))
                 xCommon += " " + OutFl->Values[OutFl->Names[i]] + " " + xFieldValue;
                else
                 xCommon += " " + xFieldValue;
              }
           }
        }
       if (allFld)
        {// ��������� �������� ��� ��������� ����� ��������������
          ClassCB->AddObject(xCommon.Trim(),(TObject*)new TStringList);
          TStringList *DDD = ((TStringList*)ClassCB->Objects[ClassCB->Count-1]);
          DDD->AddObject("",(TObject*)irquAsInt(quFree,"CODE"));
          for (int i = 0; i < FlList->Count; i++)
           {
             flName = FlList->Strings[i];
             Ind = chFlList->IndexOf(flName);
             if (Ind != -1) // ���� ���� � ������ ����� � ����� "������" ?
              { // ��������� ������ � �������: ���_����=���_#_��������
                __tmpL = (TStringList*)chFlList->Objects[Ind];
                xVal  = __tmpL->Values[irquAsStr(quFree,flName)];
                xName = irquAsStr(quFree,flName);
                DDD->Add((flName+"="+xName+"_#_"+xVal).c_str());
              }
             else
              {
                Ind = chBDFlList->IndexOf(flName);
                if (Ind != -1) // ���� ���� � ������ ����� � ����� "������ �� ��" ?
                 { // ��������� �������� ���� � ����� "������ �� ��"
                   __tmpL = (TStringList*)chBDFlList->Objects[Ind];
                   Ind = __tmpL->IndexOfObject((TObject*)irquAsInt(quFree,flName));
                   if (Ind != -1) DDD->Add((flName+"="+__tmpL->Strings[Ind]).c_str());
                 }
                else
                 DDD->Add((flName+"="+irquAsStr(quFree,flName)).c_str());
              }
           }
        }
       else
        ClassCB->AddObject(xCommon.Trim(),(TObject*)irquAsInt(quFree,"CODE"));
       irquNext(quFree);
      }
   }
  __finally
   {
     if (OutFl) delete OutFl; OutFl = NULL;
     if (FlList) delete FlList; FlList = NULL;
     if (chFlList)
      {
        for (int i = 0; i < chFlList->Count; i++)
         delete (TStringList*)chFlList->Objects[i];
        delete chFlList; chFlList = NULL;
      }
     if (chBDFlList)
      {
        for (int i = 0; i < chBDFlList->Count; i++)
         delete (TStringList*)chBDFlList->Objects[i];
        delete chBDFlList; chBDFlList = NULL;
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall InvadeClassCB(TComboBox* ClassCB, __int64 ClassCode, bool isFirstNULL,TTagNode *CNode, TpFIBQuery *quFree, UnicodeString APref,UnicodeString ACodeVals, TTagNode *ADefRoot)
{
  FInvadeClassCB(ClassCB->Items, ClassCode, isFirstNULL,CNode,true,(TDataSet*)quFree, APref, ACodeVals,ADefRoot);
  int MaxW = ClassCB->Width;
  for (int i = 0; i < ClassCB->Items->Count; i++)
   {
     if (MaxW < ClassCB->Items->Strings[i].Length()*6) MaxW = ClassCB->Items->Strings[i].Length()*6;
   }
  ClassCB->ItemIndex = -1;
  ClassCB->Text = "";
}
//---------------------------------------------------------------------------
void __fastcall InvadeClassCB(TComboBox* ClassCB, __int64 ClassCode, bool isFirstNULL,TTagNode *CNode, TDataSet *quFree, UnicodeString APref,UnicodeString ACodeVals, TTagNode *ADefRoot)
{
  FInvadeClassCB(ClassCB->Items, ClassCode, isFirstNULL,CNode,true,quFree, APref, ACodeVals, ADefRoot);
  int MaxW = ClassCB->Width;
  for (int i = 0; i < ClassCB->Items->Count; i++)
   {
     if (MaxW < ClassCB->Items->Strings[i].Length()*6) MaxW = ClassCB->Items->Strings[i].Length()*6;
   }
  ClassCB->ItemIndex = -1;
  ClassCB->Text = "";
}
//---------------------------------------------------------------------------
void __fastcall InvadeClassCB(TcxComboBox* ClassCB, __int64 ClassCode, bool isFirstNULL,TTagNode *CNode, TpFIBQuery *quFree, UnicodeString APref,UnicodeString ACodeVals, TTagNode *ADefRoot)
{
  FInvadeClassCB(ClassCB->Properties->Items, ClassCode, isFirstNULL,CNode,true,(TDataSet*)quFree, APref, ACodeVals,ADefRoot);
  int MaxW = ClassCB->Width;
  for (int i = 0; i < ClassCB->Properties->Items->Count; i++)
   {
     if (MaxW < ClassCB->Properties->Items->Strings[i].Length()*6) MaxW = ClassCB->Properties->Items->Strings[i].Length()*6;
   }
  ClassCB->ItemIndex = -1;
  ClassCB->Text = "";
}
//---------------------------------------------------------------------------
void __fastcall InvadeClassCB(TcxComboBox* ClassCB, __int64 ClassCode, bool isFirstNULL,TTagNode *CNode, TDataSet *quFree, UnicodeString APref,UnicodeString ACodeVals, TTagNode *ADefRoot)
{
  FInvadeClassCB(ClassCB->Properties->Items, ClassCode, isFirstNULL,CNode,true,quFree, APref, ACodeVals, ADefRoot);
  int MaxW = ClassCB->Width;
  for (int i = 0; i < ClassCB->Properties->Items->Count; i++)
   {
     if (MaxW < ClassCB->Properties->Items->Strings[i].Length()*6) MaxW = ClassCB->Properties->Items->Strings[i].Length()*6;
   }
  ClassCB->ItemIndex = -1;
  ClassCB->Text = "";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetWhere(TTagNode *ADesc, __int64 AUCode, TStringList *AList, UnicodeString APref, UnicodeString AUpperFunc)
{
  UnicodeString xWhere = "";
  UnicodeString itVal,iFName,iOper;
  TTagNode *itNode = ADesc->GetChildByName("maskfields");
  if (itNode) itNode = itNode->GetFirstChild();
  TTagNode *xTag;
  if (AList)
   {
     itNode = itNode->GetRoot();
     for (int i = 0; i < AList->Count; i++)
      {
        xTag = itNode->GetTagByUID(AList->Names[i]);
        if (!xWhere.Length()) iOper = " ";
        else              iOper = " and ";
        iFName = " R"+xTag->AV["uid"].UpperCase();
        itVal = AList->Values[AList->Names[i]];
        if (itVal.Length())
         {
           if (xTag->CmpName("binary"))                  xWhere += iOper+iFName+"="+itVal;
           else if (xTag->CmpName("text"))
            {
              if (AUpperFunc.Length())
               xWhere += iOper+AUpperFunc+"("+iFName+")='"+itVal.UpperCase()+"'";
              else
               xWhere += iOper+"U"+iFName+"='"+itVal.UpperCase()+"'";
            }
           else if (xTag->CmpName("date,datetime,time")) xWhere += iOper+iFName+"='"+itVal+"'";
           else if (xTag->CmpName("digit"))              xWhere += iOper+iFName+"="+itVal;
           else if (xTag->CmpName("choice"))             xWhere += iOper+iFName+"="+itVal;
           else if (xTag->CmpName("choiceBD,class"))     xWhere += iOper+GetClassSelect(itVal,xTag,APref);
           else if (xTag->CmpName("choiceTree"))
            {
              UnicodeString tmpTreeVal = GetPart1(itVal,':');
              if (tmpTreeVal.ToIntDef(0))
               xWhere += iOper+"("+iFName+">="+GetPart1(GetPart2(itVal,':'),'#')+" and "+iFName+"<="+GetPart2(GetPart2(itVal,':'),'#')+")";
              else
               xWhere += iOper+iFName+"="+GetPart1(GetPart2(itVal,':'),'#');
            }
         }
      }
     return xWhere;
   }
  else
   {
     while (itNode)
      {
        if (itNode->CmpName("field"))
         xTag = itNode->GetTagByUID(itNode->AV["ref"]);
        else
         {
           itNode = itNode->GetNext();
           continue;
         }
        if (!xWhere.Length()) iOper = " ";
        else              iOper = " and ";
        iFName = " R"+xTag->AV["uid"].UpperCase();
        Variant iVal = _TemplateValue(xTag);
        if (!iVal.IsEmpty())
         {
           if (xTag->CmpName("binary"))                  xWhere += iOper+iFName+"="+UnicodeString(((int)iVal == 0)? "0":"1");
           else if (xTag->CmpName("datetime,time"))      xWhere += iOper+iFName+"='"+UnicodeString((wchar_t*)iVal)+"'";
           else if (xTag->CmpName("text"))
            {
              if (AUpperFunc.Length())
               xWhere += iOper+AUpperFunc+"("+iFName+")='"+UnicodeString((wchar_t*)iVal).UpperCase()+"'";
              else
               xWhere += iOper+"U"+iFName+"='"+UnicodeString((wchar_t*)iVal).UpperCase()+"'";
            }
           else if (xTag->CmpName("digit"))              xWhere += iOper+iFName+"="+UnicodeString((wchar_t*)iVal);
           else if (xTag->CmpName("date"))               xWhere += iOper+iFName+"='"+TDateTime((wchar_t*)iVal).FormatString("dd.mm.yyyy")+"'";
           else if (xTag->CmpName("choice"))             xWhere += iOper+iFName+"="+UnicodeString((wchar_t*)iVal);
           else if (xTag->CmpName("choiceBD,class"))     xWhere += iOper+GetClassSelect(UnicodeString((wchar_t*)iVal),xTag,APref);
           else if (xTag->CmpName("choiceTree"))
            {
              UnicodeString tmpTreeVal = GetPart1(iVal,':');
              if (tmpTreeVal.ToIntDef(0))
               xWhere += iOper+"("+iFName+">="+GetPart1(GetPart2(iVal,':'),'#')+" and "+iFName+"<="+GetPart2(GetPart2(iVal,':'),'#')+")";
              else
               xWhere += iOper+iFName+"="+GetPart1(GetPart2(iVal,':'),'#');
            }
         }
        itNode = itNode->GetNext();
      }
     if (AUCode < 0)
      {
        if (xWhere.Length()) return UnicodeString(" "+xWhere);
        else              return UnicodeString("");
      }
     else
      {
        if (xWhere.Length()) return UnicodeString(" UCODE="+IntToStr(AUCode)+" and "+xWhere);
        else              return UnicodeString(" UCODE="+IntToStr(AUCode));
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall T_tmp::getClassCode(TTagNode *itTag, UnicodeString &UID)
{
  if (itTag->CmpName("class"))
   if (!UID.Length())
    UID += itTag->AV["uid"].UpperCase();
   else
    UID += "\n"+itTag->AV["uid"].UpperCase();
  return false;
}
//---------------------------------------------------------------------------
bool __fastcall IsClassSelect(UnicodeString ACode,TTagNode *ANode)
{
  TStringList *SSS = NULL;
  TStringList *ClVal = NULL;
  T_tmp *XXX = NULL;
  bool RC = false;
  try
   {
     UnicodeString DDD,xFrom;
     T_tmp *XXX = new T_tmp;
     TTagNode *itNode;
     if (ANode->CmpName("choiceBD,class"))
      {
        SSS = new TStringList;
        ClVal = new TStringList;
        if (ANode->CmpName("class")) itNode = ANode;
        else                         itNode = ANode->GetTagByUID(ANode->AV["ref"]);
        itNode->ReIterateGroup(XXX->getClassCode,DDD);
        ClVal->Text = ACode; SSS->Text = DDD;
        if (SSS->Count != ClVal->Count) RC = true;
      }
   }
  __finally
   {
     if (SSS)   delete SSS;  SSS = NULL;
     if (ClVal) delete ClVal;  ClVal = NULL;
     if (XXX) delete XXX;  XXX = NULL;
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetClassSelectOnly(UnicodeString ACode,TTagNode *ANode,UnicodeString APref)
{
  UnicodeString RC = "";
  try
   {
     RC = FClassSelect(ANode,ACode,APref,"");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetClassSelect(UnicodeString ACode,TTagNode *ANode,UnicodeString APref, UnicodeString AFlName)
{
  UnicodeString RC = "";
  try
   {
     UnicodeString xFlName;
     if (!AFlName.Length())
      xFlName = " R"+ANode->AV["uid"].UpperCase();
     else
      xFlName = AFlName;
     RC = FClassSelect(ANode,ACode,APref,xFlName);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall FClassSelect(TTagNode *ANode,UnicodeString ACode,UnicodeString APref, UnicodeString AFlName)
{
  TStringList *SSS = NULL;
  TStringList *ClVal = NULL;
  T_tmp *XXX = NULL;
  UnicodeString RC = "";
  try
   {
     XXX = new T_tmp;
     UnicodeString DDD,xFrom,xFName;
     TTagNode *itNode;
     if (ANode->CmpName("choiceBD,class"))
      {
        SSS = new TStringList;
        ClVal = new TStringList;
        if (ANode->CmpName("class")) itNode = ANode;
        else                         itNode = ANode->GetTagByUID(ANode->AV["ref"]);
        itNode->ReIterateGroup(XXX->getClassCode,DDD);
        ClVal->Text = ACode; SSS->Text = DDD;
        DDD = ""; xFrom = "";
        if (SSS->Count == ClVal->Count)
         {
           if (AFlName.Length())
            RC = UnicodeString(AFlName+"="+ClVal->Strings[ClVal->Count-1]);
         }
        else
         {
           for (int i = ClVal->Count; i < SSS->Count; i++)
            {
              if (!xFrom.Length())
               {
                 xFrom = "From CLASS_"+APref+SSS->Strings[i];
                 if ((SSS->Count - ClVal->Count) == 1)
                  xFrom += " Where CLASS_"+APref+SSS->Strings[i]+".R"+SSS->Strings[i-1]+" = "+ClVal->Strings[i-1];
                 else
                  xFrom += " join CLASS_"+APref+SSS->Strings[i+1]+" on (CLASS_"+APref+SSS->Strings[i]+".CODE = CLASS_"+APref+SSS->Strings[i+1]+".R"+SSS->Strings[i]+" and CLASS_"+APref+SSS->Strings[i]+".R"+SSS->Strings[i-1]+" = "+ClVal->Strings[i-1]+")";
               }
              else if (i > (ClVal->Count+1))
               xFrom += " join CLASS_"+APref+SSS->Strings[i]+" on (CLASS_"+APref+SSS->Strings[i-1]+".CODE = CLASS_"+APref+SSS->Strings[i]+".R"+SSS->Strings[i-1]+")";
            }
           if (AFlName.Length())
             RC = UnicodeString(AFlName+" in (Select CLASS_"+APref+SSS->Strings[SSS->Count-1]+".CODE "+xFrom+")");
           else
             RC = UnicodeString("Select CLASS_"+APref+SSS->Strings[SSS->Count-1]+".CODE "+xFrom);
         }
      }
   }
  __finally
   {
     if (SSS)   delete SSS;  SSS = NULL;
     if (ClVal) delete ClVal;  ClVal = NULL;
     if (XXX) delete XXX;  XXX = NULL;
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall IsTemplate(TTagNode *itTag)
{
  if (itTag->GetAttrByName("uid"))
   {
     return !(_TemplateValue(itTag).IsEmpty()||_TemplateValue(itTag).IsNull());
   }
  else
   return false;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall FieldCast(TTagNode *ADefNode, UnicodeString AVal)
{
  UnicodeString FVal = AVal.Trim();
  if (FVal.Length())
   {
     if (ADefNode->CmpAV("cast","lo"))
      FVal = FVal.LowerCase();
     else if (ADefNode->CmpAV("cast","up"))
      FVal = FVal.UpperCase();
     else if (ADefNode->CmpAV("cast","up1st"))
      FVal = FVal.SubString(1,1).UpperCase() + FVal.SubString(2,FVal.Length()-1).LowerCase();
   }
  return FVal;
}
//---------------------------------------------------------------------------
Extended __fastcall ICSStrToFloat(UnicodeString AVal)
{
  UnicodeString FVal = AVal.Trim();
  TReplaceFlags rFlag;
  rFlag << rfReplaceAll;
  FVal = StringReplace(FVal, ",", FormatSettings.DecimalSeparator, rFlag);
  FVal = StringReplace(FVal, ".", FormatSettings.DecimalSeparator, rFlag);
  FVal = StringReplace(FVal, " ", "", rFlag);
  FVal = StringReplace(FVal, FormatSettings.ThousandSeparator, "", rFlag);

  return StrToFloat(FVal);
}
//---------------------------------------------------------------------------
Extended __fastcall ICSStrToFloatDef(UnicodeString AVal, Extended ADefVal)
{
  try
   {
     return ICSStrToFloat(AVal);
   }
  catch (EConvertError& E)
   {
     return ADefVal;
   }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall icsCalcMask(bool AIsFolat, int ADigits, int ADecimal, UnicodeString AMin)
{
  UnicodeString RC = "";
  try
   {
     UnicodeString tmpMask;
     UnicodeString tmpMaskSub = "";
     if (!AIsFolat)
      {
        int FMin = AMin.ToIntDef(0);
        if (abs(FMin) != FMin)
         {
          if (ADigits > 1)
           tmpMaskSub = "[-]\\d\\d{"+IntToStr(ADigits-2)+"}";
          else
           throw Exception("��� ������������� ����� ���������� �������� ������ ���� ������ 1");
         }
        RC = "\\d{"+IntToStr(ADigits)+"}";
      }
     else
      {
        Extended FMin = ICSStrToFloatDef(AMin, 0.0);
        if (fabs(FMin) != FMin)
         {
           for (int i = 1; i <= ADecimal; i++)
            {
              tmpMask = "[-]";
              for (int j = 1; j <= ADigits - 2 - i; j++)
               tmpMask += "\\d?";
              tmpMask = "("+tmpMask+"[.,]?\\d{"+IntToStr(i)+"})";
              if (tmpMaskSub.Length())
               tmpMaskSub += "|"+tmpMask;
              else
               tmpMaskSub = tmpMask;
            }
         }
        for (int i = 1; i <= ADecimal; i++)
         {
           tmpMask = "";
           for (int j = 1; j <= ADigits - 1 - i; j++)
            tmpMask += "\\d?";
           tmpMask = "("+tmpMask+"[.,]?\\d{"+IntToStr(i)+"})";
           if (RC.Length())
            RC += "|"+tmpMask;
           else
            RC = tmpMask;
         }
      }
     if (tmpMaskSub.Length())
      RC = "("+tmpMaskSub+")|("+RC+")";
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

