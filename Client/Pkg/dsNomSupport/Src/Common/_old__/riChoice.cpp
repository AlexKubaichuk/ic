//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "riChoice.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED_CB      ((TcxComboBox*)ED)
//---------------------------------------------------------------------------
__fastcall TriChoice::TriChoice(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref, TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  TDataSource* ASrc, bool IsApp)
                   :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                                 APref, AReqColor, ACtrlOwner,
                                                 ASrc, IsApp)
{
  Lab = new TLabel(this);  Lab->Parent = this;
  Lab->Transparent = true;
  Lab->Top = 2;            Lab->Left = 4;
  Lab->Caption = FNode->AV["name"]+":";
  ED = new TcxComboBox(this);
  tED_CB->Properties->DropDownListStyle = lsEditFixedList;
  tED_CB->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
  tED_CB->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
  if (Required) tED_CB->Style->Color = FReqColor;
  ED->Parent = this;
//  if (!is3D)
//   {
//     tED_CB->Ctl3D = !is3D;
//     tED_CB->BevelKind = bkTile;
//   }
  tED_CB->Properties->Items->Clear();
  TTagNode *itxNode = FNode->GetFirstChild();
  int MaxW = 16; 

  while(itxNode)
   {
     if (itxNode->CmpName("choicevalue"))
      {
        tED_CB->Properties->Items->AddObject(itxNode->AV["name"].c_str(),(TObject*)itxNode->AV["value"].ToInt());
        if (MaxW < tED_CB->Canvas->TextWidth(itxNode->AV["name"])) MaxW = tED_CB->Canvas->TextWidth(itxNode->AV["name"]);
      }
     itxNode = itxNode->GetNext();
   }
  int sVal = -2;
  if (SrcDS()->Active)
   {
     if (IsAppend) sVal = FNode->AV["default"].ToInt();
     else          sVal = irquAsInt(SrcDS(),FFlName);
     if (IsTemplate(FNode))
      sVal = (int)_TemplateValue(FNode);
     int xind = tED_CB->Properties->Items->IndexOfObject((TObject*)sVal);
     if (xind != -1)
      {
         if (SrcDS()->State != dsBrowse)
          irquSetAsInt(SrcDS(),FFlName,sVal);
         tED_CB->ItemIndex = -1;
         tED_CB->ItemIndex = xind;
         tED_CB->Text = "empty";
      }
   }
  ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  tED_CB->Properties->OnChange = riDataChange;
  ED->Constraints->MinWidth = 60;
  ED->Parent = this;
  ED->Width = MaxW+GetSystemMetrics(SM_CXVSCROLL)+GetSystemMetrics(SM_CXBORDER)*2+10;
  if (ED->Width < 60) ED->Width = 60;
  ED->Top = 0; 
  ED->Height = 21;
  Height = 25;
  if (Lab->Width > (AParent->Width-2))
   {
     Lab->Font->Size = Lab->Font->Size - 1;
     Lab->Update();
     if (Lab->Width > (AParent->Width - 2))
      Lab->Width = AParent->Width-2;
   }
  ED->Left = Lab->Left + Lab->Width + 5;
  if ((ED->Left + ED->Width) > (AParent->Width - 2))
   {
     TwoLine = true;  Lab->Top = 0;
     ED->Left = Lab->Left+5;  ED->Top = Lab->Height;
     CMaxH = 38; CMinH = 38;
     Height = CMaxH;
     if ((ED->Left + ED->Width) > (AParent->Width - 2))
      ED->Width = AParent->Width - 4 - ED->Left;
   }
   if (Lab&&ED)
    Lab->FocusControl = ED;
   if (Parent->ClassNameIs("TriChoice"))
    {
      Anchors.Clear();
      Anchors << akLeft << akRight << akTop;
    }
}
//---------------------------------------------------------------------------
void __fastcall TriChoice::SetOnDependValues()
{
  if (FNode->AV["depend"].Length() == 9 && isRefMCtrl(_GUI(FNode->AV["depend"])))
   RefMCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(FNode);
  else if (FNode->AV["ref"].Length() == 4 && isRefCtrl(FNode))
   RefCtrl(FNode)->SetOnLab(FNode);
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
__fastcall TriChoice::~TriChoice()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   tED_CB->Properties->OnChange = NULL;
   delete tED_CB;
}
//---------------------------------------------------------------------------
void __fastcall TriChoice::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine)
   {
     if ((ALeft+ED->Width) < Width -6)
      ED->Left = ALeft;
   }
  Width = ED->Left+ED->Width;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriChoice::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (TwoLine)
   {
     *AEDLeft = -1;
     *ALabR = -1;
   }
  else
   {
     *AEDLeft = ED->Left;
     *ALabR = Lab->Left+Lab->Width;
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoice::cResize(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     if (TwoLine) {ED->Left = 2; ED->Width = Width-4;}
     else          ED->Left = Width - ED->Width - 2;
     tED_CB->Properties->OnChange = NULL;
     int FSaveIndex = tED_CB->ItemIndex;
     tED_CB->ItemIndex = -1;
     tED_CB->ItemIndex = FSaveIndex;
     tED_CB->Properties->OnChange = riDataChange;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriChoice::UpdateChanges()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  tED_CB->Properties->OnChange((TObject*)ED);
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoice::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  if (ED->Enabled)
   {
     if (tED_CB->ItemIndex >= 1)
      xVal += tED_CB->Properties->Items->Strings[tED_CB->ItemIndex];
   }
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriChoice::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriChoice::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isEnabled = AEnable;
  Lab->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  if (isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate))
   {
     tED_CB->Style->Color = (Required)? FReqColor : clWindow;
   }
  else
   {
     tED_CB->Style->Color = clBtnFace;
     if (isEdit && !isDelegate && (!IsAppend || !isTemplate))
      {
        if (SrcDSEditable())
         irquSetAsNull(SrcDS(),FFlName);
        tED_CB->ItemIndex = -1;
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoice::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnable(AEnable);
  int xind = tED_CB->Properties->Items->IndexOfObject((TObject*)AVal.ToIntDef(0));
  if (xind != -1)
   {
     tED_CB->ItemIndex = -1;
     tED_CB->ItemIndex = xind;
     tED_CB->Text = AVal;
     if (SrcDSEditable())
      {
        irquSetAsStr(SrcDS(),FFlName,AVal);
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoice::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) Lab->Caption = xLab+":";
  else            Lab->Caption = "";//FNode->AV["name"]+":";
  Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TriChoice::SetOnLab(TTagNode* ANode)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetLabList->Add(ANode);
  tED_CB->Properties->OnChange = riDataChange;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriChoice::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  tED_CB->Properties->OnChange = riDataChange;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriChoice::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TriChoice::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (SrcDSEditable())
   {
     if (ED->Enabled)
      {
        if (tED_CB->ItemIndex != -1)  irquSetAsInt(SrcDS(),FFlName,(int)tED_CB->Properties->Items->Objects[tED_CB->ItemIndex]);
        else
         {
           if (isEdit && !isDelegate && (!IsAppend || !isTemplate))
            irquSetAsNull(SrcDS(),FFlName);
         }
      }
     else
      {
        if (isEdit && !isDelegate && (!IsAppend || !isTemplate))
         irquSetAsNull(SrcDS(),FFlName);
      }
   }
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
  if (SetLabList->Count > 0) // ��������� ������ ��� Label �������� �� choice ��� choiceBD
   {
     if (FNode->CmpName("choice")) // ����� �� choice
      {
        int ItmIndex;
        ItmIndex  = tED_CB->ItemIndex;
        if (ItmIndex >= 0)
         {
           for (int i = 0; i < SetLabList->Count; i++)
            {
              xRef = ((TTagNode*)SetLabList->Items[i])->AV["ref"].UpperCase(); // ref == uid.choice
              if (xRef == FUID)
               labCtrl(i)->SetLabel(tED_CB->Properties->Items->Strings[tED_CB->ItemIndex]);
            }
         }
      }
   }
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TriChoice::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriChoice::GetControl(int AIdx)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FNode->CmpName("text,date,datetime,time,digit,extedit,binary,choice,choiceTree"))
    return (TControl*)ED;
   else
    return NULL;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriChoice::GetFirstEnabledControl()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriChoice::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (tED_CB->ItemIndex >= 0)
   return IntToStr(((int)tED_CB->Properties->Items->Objects[tED_CB->ItemIndex]));
  return "";
}
//---------------------------------------------------------------------------
Variant __fastcall TriChoice::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  Variant RC = Variant::Empty();
  try
   {
     if (FCtrlOwner->OnGetCompValue)
      {
        if (tED_CB->ItemIndex >= 0) RC = Variant((int)(int)tED_CB->Properties->Items->Objects[tED_CB->ItemIndex]);
        FCtrlOwner->OnGetCompValue(FNode, RC);
      }
     else
      {
        if (tED_CB->ItemIndex >= 0) RC = Variant((int)(int)tED_CB->Properties->Items->Objects[tED_CB->ItemIndex]);
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TriChoice::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED->Enabled)
         if (tED_CB->ItemIndex < 0)  RC = false;
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
              UnicodeString ErrMessage;
              if (Lab) ErrMessage = Lab->Caption;
              else     ErrMessage = FNode->AV["name"];
              ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
              MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
              ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------

