//---------------------------------------------------------------------------

#ifndef ShortCutInfPanelH
#define ShortCutInfPanelH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
//#include <Forms.hpp>
#include <ActnList.hpp>
//---------------------------------------------------------------------------
class TSepLab : public TLabel
{
public:		// User declarations
        __fastcall TSepLab(TComponent* Owner, int ANum);
};
//---------------------------------------------------------------------------
class TCaptLab : public TLabel
{
public:		// User declarations
        __fastcall TCaptLab(TComponent* Owner, UnicodeString ACapt, int ANum);
};
//---------------------------------------------------------------------------
class TSCLab : public TLabel
{
public:		// User declarations
        __fastcall TSCLab(TComponent* Owner, UnicodeString ACapt, int ANum);
};
//---------------------------------------------------------------------------
class TShortCutInfLab : public TPanel
{
private:	// User declarations
public:		// User declarations
        __fastcall TShortCutInfLab(TComponent* Owner, UnicodeString ACapt, TShortCut AShortCut, int ANum);
//        __fastcall ~TShortCutInfPanel();
};
//---------------------------------------------------------------------------
class TShortCutInfPanel : public TPanel
{
private:	// User declarations
        TList    *FCtrlList;
        TNotifyEvent FExtResize;
        void __fastcall FInfPanelResize(TObject *Sender);
        void __fastcall FResize(int AIdx, int ALine);
public:		// User declarations
        void __fastcall AddItem(UnicodeString ACapt, TShortCut AShortCut);
        __property TNotifyEvent ExtResize = {read=FExtResize, write=FExtResize};
        __fastcall TShortCutInfPanel(TComponent* Owner, TActionList *AActList);
        __fastcall ~TShortCutInfPanel();
};
//---------------------------------------------------------------------------
#endif
