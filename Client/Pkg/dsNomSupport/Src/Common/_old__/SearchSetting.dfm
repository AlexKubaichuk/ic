object SearchSettingForm: TSearchSettingForm
  Left = 408
  Top = 189
  ActiveControl = sElemenList
  BorderStyle = bsToolWindow
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1087#1086#1080#1089#1082#1072
  ClientHeight = 471
  ClientWidth = 379
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 434
    Width = 379
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 432
    ExplicitWidth = 375
    DesignSize = (
      379
      37)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 379
      Height = 5
      Align = alTop
      Shape = bsTopLine
      ExplicitWidth = 375
    end
    object OkBtn: TcxButton
      Left = 211
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = OkBtnClick
      ExplicitLeft = 207
    end
    object CancelBtn: TcxButton
      Left = 295
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      ModalResult = 2
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ExplicitLeft = 291
    end
  end
  object CompPanel: TPanel
    Left = 0
    Top = 0
    Width = 379
    Height = 434
    Align = alClient
    AutoSize = True
    BevelOuter = bvNone
    Constraints.MinWidth = 260
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitWidth = 375
    ExplicitHeight = 432
    object FieldListLab: TcxLabel
      Left = 0
      Top = 0
      Align = alTop
      Caption = #1057#1087#1080#1089#1086#1082' '#1087#1086#1083#1077#1081' '#1076#1083#1103' '#1087#1086#1080#1089#1082#1072':'
      ExplicitWidth = 375
    end
    object sElemenList: TcxTreeList
      Left = 0
      Top = 17
      Width = 379
      Height = 417
      Align = alClient
      Bands = <
        item
        end>
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      Navigator.Buttons.CustomButtons = <>
      OptionsView.CellEndEllipsis = True
      OptionsView.Headers = False
      OptionsView.UseNodeColorForIndent = False
      Styles.OnGetContentStyle = sElemenListStylesGetContentStyle
      TabOrder = 1
      OnEditing = sElemenListEditing
      OnKeyDown = sElemenListKeyDown
      ExplicitWidth = 375
      ExplicitHeight = 415
      Data = {
        00000500D50300000F00000044617461436F6E74726F6C6C6572310400000012
        000000546378537472696E6756616C7565547970651200000054637853747269
        6E6756616C75655479706512000000546378537472696E6756616C7565547970
        6512000000546378537472696E6756616C7565547970650E000000445855464D
        54000003000000240418041E04010101445855464D5400000700000024043004
        3C0438043B0438044F04010101445855464D5400000300000018043C044F0401
        0101445855464D540000080000001E044204470435044104420432043E040101
        01445855464D540000100000001E044004330430043D04380437043E04320430
        043D043D043E04410442044C04010101445855464D5400000B0000001E044004
        330430043D04380437043004460438044F04010101445855464D5400000F0000
        001E044004330430043D04380437043004460438044F04200028001E04290001
        0101445855464D5400001000000014043E043F042E0020001E04400433043004
        3D04380437043004460438044F04010101445855464D5400000F0000001E0440
        04330430043D04380437043004460438044F0420002800140429000101014458
        55464D5400000500000010043404400435044104010101445855464D5400000A
        0000001504410442044C04200030043404400435044104010101445855464D54
        0000110000001D04300441042E0020003F0443043D043A0442042C0020004304
        3B04380446043004010101445855464D5400000300000014043E043C04010101
        445855464D540000020000001A04320401010103000000000000001200030000
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0100000008000000000000
        000000FFFFFFFFFFFFFFFFFFFFFFFF0200000008000000000000000000FFFFFF
        FFFFFFFFFFFFFFFFFF0300000008000000000000000000FFFFFFFFFFFFFFFFFF
        FFFFFF040000001200040000000000000000000000FFFFFFFFFFFFFFFFFFFFFF
        FF0500000008000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0600000008
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0700000008000000000000
        000000FFFFFFFFFFFFFFFFFFFFFFFF0800000008000000000000000000FFFFFF
        FFFFFFFFFFFFFFFFFF090000001200040000000000000000000000FFFFFFFFFF
        FFFFFFFFFFFFFF0A00000008000000000000000000FFFFFFFFFFFFFFFFFFFFFF
        FF0B00000008000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0C00000008
        000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0D00000008000000000000
        000000FFFFFFFFFFFFFFFFFFFFFFFF1A0803000000}
      object NameCol: TcxTreeListColumn
        Caption.Text = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1083#1103' '#1076#1083#1103' '#1087#1086#1080#1089#1082#1072
        DataBinding.ValueType = 'String'
        Options.Sizing = False
        Options.Editing = False
        Options.Focusing = False
        Options.Moving = False
        Options.Sorting = False
        Width = 296
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object ValCol: TcxTreeListColumn
        DataBinding.ValueType = 'String'
        Options.Sizing = False
        Options.Moving = False
        Options.Sorting = False
        Width = 50
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
        OnGetEditProperties = ValColGetEditProperties
      end
      object IDCol: TcxTreeListColumn
        Visible = False
        DataBinding.ValueType = 'String'
        Options.Editing = False
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object CanEditCol: TcxTreeListColumn
        Visible = False
        DataBinding.ValueType = 'String'
        Options.Editing = False
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object SetValChB: TcxCheckBox
      Left = 149
      Top = 382
      Caption = 'SetValChB'
      TabOrder = 2
      Transparent = True
      Visible = False
      Width = 121
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 80
    Top = 65535
    PixelsPerInch = 96
    object headStyle: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = clSilver
      TextColor = clWhite
    end
  end
end
