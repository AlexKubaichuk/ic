//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ShortCutInfPanel.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
const int BaseCtrlLeft = 1;
const int BaseCtrlTop = 1;
const int BaseLineHeight = 15;
__fastcall TCaptLab::TCaptLab(TComponent* Owner, UnicodeString ACapt, int ANum): TLabel(Owner)
{
  this->Name = "CaptLab"+IntToStr(ANum);
  this->ParentFont = false;
  this->Font->Color = clNavy;
  this->Font->Size = 6;
  this->Top = BaseCtrlTop;
  this->Caption = ACapt;
  this->Transparent = true;
  this->Parent = (TWinControl*)Owner;
}
//---------------------------------------------------------------------------
__fastcall TSCLab::TSCLab(TComponent* Owner, UnicodeString ACapt, int ANum): TLabel(Owner)
{
  this->Name = "SCLab"+IntToStr(ANum);
  this->ParentFont = false;
  this->Font->Style = TFontStyles() << fsBold;
  this->Top = BaseCtrlTop;
  this->Font->Size = 6;
  this->Caption = ACapt;
  this->Transparent = true;
  this->Parent = (TWinControl*)Owner;
}
//---------------------------------------------------------------------------
__fastcall TSepLab::TSepLab(TComponent* Owner, int ANum): TLabel(Owner)
{
  this->Name = "SepLab"+IntToStr(ANum);
  this->ParentFont = false;
  this->Top = BaseCtrlTop;
  this->Font->Size = 6;
  this->Caption = "|";
  this->Transparent = true;
  this->Parent = (TWinControl*)Owner;
}
//---------------------------------------------------------------------------
__fastcall TShortCutInfLab::TShortCutInfLab(TComponent* Owner, UnicodeString ACapt, TShortCut AShortCut, int ANum): TPanel(Owner)
{
  this->BevelOuter = bvLowered;
  this->Height = BaseLineHeight+BaseCtrlTop;
  this->Color = clBtnFace;

  TLabel *tmpLab;
  int FCtrlLeft = 0;
//  tmpLab = new TSepLab(this, ANum);
//  tmpLab->Left = FCtrlLeft;
//  FCtrlLeft += tmpLab->Width;

  tmpLab = new TSCLab(this, " "+ShortCutToText(AShortCut)+": ", ANum);
  tmpLab->Left = FCtrlLeft;
  FCtrlLeft += tmpLab->Width;

  tmpLab = new TCaptLab(this, ACapt+" ", ANum);
  tmpLab->Left = FCtrlLeft;
  FCtrlLeft += tmpLab->Width;

  this->Parent = (TWinControl*)Owner;
  this->Width = FCtrlLeft+2;
}
//---------------------------------------------------------------------------
__fastcall TShortCutInfPanel::TShortCutInfPanel(TComponent* Owner, TActionList *AActList)
        : TPanel(Owner)
{
  this->BevelOuter = bvLowered;
  this->Height = BaseLineHeight+BaseCtrlTop;
  TAction *FAct;
  FCtrlList = new TList;
  for (int i = 0; i < AActList->ActionCount; i++)
   {
     FAct = (TAction*)AActList->Actions[i];
     if ((int)FAct->ShortCut && FAct->Visible)
      {
        AddItem(FAct->Caption, FAct->ShortCut);
      }
   }
//  FCtrlList->Add((void*)new TSepLab(this,FCtrlList->Count));
  this->OnResize = FInfPanelResize;
}
//---------------------------------------------------------------------------
__fastcall TShortCutInfPanel::~TShortCutInfPanel()
{
  delete FCtrlList;
}
//---------------------------------------------------------------------------
void __fastcall TShortCutInfPanel::AddItem(UnicodeString ACapt, TShortCut AShortCut)
{
  TShortCutInfLab *tmpLab;
  int FCtrlLeft;
  if (FCtrlList->Count)
   {
     tmpLab = ((TShortCutInfLab*)FCtrlList->Items[FCtrlList->Count-1]);
     FCtrlLeft = tmpLab->Left+tmpLab->Width;
   }
  else
   FCtrlLeft = BaseCtrlLeft;

  tmpLab = new TShortCutInfLab(this, ACapt, AShortCut, FCtrlList->Count);
  tmpLab->Left = FCtrlLeft;
  FCtrlLeft += tmpLab->Width;
  FCtrlList->Add((void*)tmpLab);
}
//---------------------------------------------------------------------------
void __fastcall TShortCutInfPanel::FInfPanelResize(TObject *Sender)
{
  int FLine = 0;
  TShortCutInfLab *tmpLab;
  FResize(0, 0);
  for (int i = 0; i < FCtrlList->Count; i++)
   {
     tmpLab = (TShortCutInfLab*)FCtrlList->Items[i];
     if ((tmpLab->Left+tmpLab->Width) > this->Width)
      {
        FResize(i, ++FLine);
      }
   }
  this->Height = (FLine+1)*BaseLineHeight+BaseCtrlTop;
  if (FExtResize)
   {
     FExtResize(this);
   }
}
//---------------------------------------------------------------------------
void __fastcall TShortCutInfPanel::FResize(int AIdx, int ALine)
{
  TShortCutInfLab *tmpLab;
  int FCtrlLeft = BaseCtrlLeft;
  for (int i = AIdx; i < FCtrlList->Count; i++)
   {
     tmpLab = (TShortCutInfLab*)FCtrlList->Items[i];
     tmpLab->Left = FCtrlLeft;
     tmpLab->Top = ALine*BaseLineHeight+BaseCtrlTop;
     FCtrlLeft += tmpLab->Width;
   }
}
//---------------------------------------------------------------------------

