//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdlib.h>
#pragma hdrstop

#include "irTreeClass.h"
#include "Reg_DMF.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxButtons"
#pragma link "cxCheckBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxProgressBar"
#pragma link "cxTextEdit"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma link "dxSkinsdxStatusBarPainter"
#pragma link "dxStatusBar"
#pragma resource "*.dfm"
TirTreeClassForm *irTreeClassForm;
//---------------------------------------------------------------------------
__fastcall TirTreeClassForm::TirTreeClassForm(TComponent* Owner,void* ANode,UnicodeString AClName,UnicodeString AWhere,TClassCallBack ACallBack, TRegistry_ICS* ARegComp)
	: TForm(Owner)
{
  FInit(ANode,AClName,AWhere,ACallBack, ARegComp->DM->quFree);
}
//---------------------------------------------------------------------------
__fastcall TirTreeClassForm::TirTreeClassForm(TComponent* Owner,void* ANode,UnicodeString AClName,UnicodeString AWhere,TClassCallBack ACallBack, TpFIBQuery* AQuery)
	: TForm(Owner)
{
  FInit(ANode,AClName,AWhere,ACallBack, AQuery);
}
//---------------------------------------------------------------------------
void __fastcall TirTreeClassForm::FInit(void* ANode,UnicodeString AClName,UnicodeString AWhere,TClassCallBack ACallBack, TpFIBQuery* AQuery)
{
  SelGroupChB->Caption = FMT(icsRegTreeClassSelGroupCaption);
  FindLab->Caption = FMT(icsRegTreeClassFindLabCaption);
  OkBtn->Caption = FMT(icsRegTreeClassApplyBtnCaption);
  CancelBtn->Caption = FMT(icsRegTreeClassCancelBtnCaption);
  FindPos = 0;
  TTagNode *itNode = (TTagNode*)ANode;
  UnicodeString iCode,iExtCode,iLev,iName,iTabName,iWhere,iTitle,iMess;
  iCode    = itNode->AV["codefield"].UpperCase();
  iExtCode = itNode->AV["extcodefield"].UpperCase();
  iLev     = itNode->AV["levelfield"].UpperCase();
  iName    = itNode->AV["namefield"].UpperCase();
  iTabName = itNode->AV["tblname"].UpperCase();
  iWhere   = AWhere.UpperCase();
  iTitle   = itNode->AV["name"];
  // �������� ���������� �������
  AQuery->Close();
  if (AQuery->Transaction->Active) AQuery->Transaction->Rollback();
  AQuery->SQL->Clear();
  AQuery->SQL->Add(("Select COUNT("+iCode+") AS CLCOUNT From "+iTabName).c_str());
  if (iWhere.Length()) AQuery->SQL->Add(("Where "+iWhere).c_str());
  if (!AQuery->Transaction->Active) AQuery->Transaction->StartTransaction();
  AQuery->Prepare();
  AQuery->ExecQuery();
  __int64 itCount = AQuery->FN("CLCOUNT")->AsInteger;
  NodeMap = new void*[itCount+1];
  __int64 itI = 0;
  iMess = FMT2(icsRegTreeClassLoadMsg,iTitle,AClName);
  if (ACallBack) ACallBack(0,iMess);
  // ������������ ��������
  AQuery->Close();
  if (AQuery->Transaction->Active) AQuery->Transaction->Rollback();
  AQuery->SQL->Clear();
  AQuery->SQL->Add(("Select "+iCode+","+iExtCode+","+iLev+","+iName+" From "+iTabName).c_str());
  if (iWhere.Length()) AQuery->SQL->Add(("Where "+iWhere).c_str());
  AQuery->SQL->Add(("Order By  "+iCode).c_str());
  if (!AQuery->Transaction->Active) AQuery->Transaction->StartTransaction();
  AQuery->Prepare();
  AQuery->ExecQuery();
  TTreeNode *clRoot;
  TList *tmpNodes = new TList;
  int Level, tmpLevel;
  Level = 0;
  TTreeNode *tmpNode1;
  TTreeNode *tmpNode2;
  clRoot = ClassTree->Items->Add(NULL, iTitle);
  NodeMap[0] = clRoot;
  tmpNodes->Add(clRoot);
  int i = 1;
  __int64 subL = 0;
  __int64 sCode = -1;
  int sLev = 0;
  try
   {
     while (!AQuery->Eof)
      {
        tmpLevel = AQuery->FN(iLev.c_str())->AsInteger;
        tmpLevel -= subL;
        if (tmpLevel > (Level+1))
         {
           sCode = (__int64)AQuery->FN(iCode.c_str())->AsInteger;
           sLev = tmpLevel;
           subL = tmpLevel - 1;
           tmpLevel -= subL;
         }
        if (tmpLevel < Level)
         {
           if ((sLev >= AQuery->FN(iLev.c_str())->AsInteger)||((sCode+1)!=(__int64)AQuery->FN(iCode.c_str())->AsInteger))
            {
              tmpLevel = 1;
              sLev = AQuery->FN(iLev.c_str())->AsInteger;
              subL = AQuery->FN(iLev.c_str())->AsInteger - 1;
            }
         }
        else
         {
           if ((sCode+1)!=(__int64)AQuery->FN(iCode.c_str())->AsInteger)
            {
              tmpLevel = 1;
              sLev = AQuery->FN(iLev.c_str())->AsInteger;
              subL = AQuery->FN(iLev.c_str())->AsInteger - 1;
            }
         }
        sCode = (__int64)AQuery->FN(iCode.c_str())->AsInteger;
        tmpNode2 = (TTreeNode*)tmpNodes->Items[tmpLevel - 1];
        tmpName = AQuery->FN(iExtCode.c_str())->AsString+": "+AQuery->FN(iName.c_str())->AsString;
        tmpNode1 = ClassTree->Items->AddChildObject(tmpNode2,tmpName.c_str(),(void*)((__int64)AQuery->FN(iCode.c_str())->AsInteger));
        NodeMap[i] = tmpNode1; i++;
        if (Level != tmpLevel)
         {
           if (tmpLevel == tmpNodes->Count) tmpNodes->Add(tmpNode1);
           else tmpNodes->Items[tmpLevel] = tmpNode1;
         }
        else
         tmpNodes->Items[tmpLevel] = tmpNode1;
        Level = tmpLevel;
        AQuery->Next();
        itI++;
        if (ACallBack)
         {
           if (itCount > 0) ACallBack(((itI*100)/itCount),iMess);
           else             ACallBack(itI,iMess);
         }
      }
   }
  catch (Exception &E)
   {
     MessageBox(NULL,E.Message.c_str(),cFMT(icsRegTreeClassLoadError),MB_ICONSTOP);
   }
  AQuery->Close();
  if (AQuery->Transaction->Active) AQuery->Transaction->Rollback();
  if (tmpNodes) delete tmpNodes;
  tmpNodes = NULL;
  PrBar->Properties->Max = ClassTree->Items->Count;
}
//---------------------------------------------------------------------------
void __fastcall TirTreeClassForm::BtnOkClick(TObject *Sender)
{
  clText = ClassTree->Selected->Text;
  int Ind = clText.Pos(": ");
  if (Ind < 1) {ActiveControl = QSearchED; return;}
  FclCode = (__int64)ClassTree->Selected->Data;
  clExtCode = clText.SubString(1,Ind-1);
  clText    = clText.SubString(Ind+2,clText.Length()-Ind-1);
  clSelectGroup = (SelGroupChB->Visible)?SelGroupChB->Checked : false;
  clCodeLast = -1;
  if (clSelectGroup)
   {
     if (ClassTree->Selected->getNextSibling())
      clCodeLast = (__int64)ClassTree->Items->Item[ClassTree->Selected->getNextSibling()->AbsoluteIndex-1]->Data;
     else
      clCodeLast = (__int64)ClassTree->Items->Item[ClassTree->Items->Count-2]->Data;
   }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TirTreeClassForm::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if (Key == VK_RETURN) BtnOkClick(this);
  if (Shift.Contains(ssCtrl) && Key == 'F')
   QSearchEDPropertiesButtonClick(QSearchED,0);
}
//---------------------------------------------------------------------------
void __fastcall TirTreeClassForm::ClassTreeEditing(TObject *Sender,
      TTreeNode *Node, bool &AllowEdit)
{
  AllowEdit = false;
}
//---------------------------------------------------------------------------
void __fastcall TirTreeClassForm::QSearchEDPropertiesButtonClick(
      TObject *Sender, int AButtonIndex)
{
  if (ClassTree->Items->Count)
   {
     try
      {
        void** SelNode;
        void** EndNode = NodeMap+ClassTree->Items->Count;
        void** itNode;
        ActiveControl = QSearchED;
        tmpName = QSearchED->Text.UpperCase();
        if (!ClassTree->Selected)
          ClassTree->Select(ClassTree->Items->Item[0]);
        if (!tmpName.Length())
         ClassTree->Select(ClassTree->Items->Item[0]);
        else
         {
           bool CanCont = true;
           PrBar->Position = 0;
           PrBar->Visible = true;
           for (itNode = NodeMap+ClassTree->Selected->AbsoluteIndex+1; (itNode != EndNode) && CanCont; itNode++)
            {
              PrBar->Position++; Application->ProcessMessages();
              if (wcsstr(((TTreeNode*)itNode[0])->Text.UpperCase().c_str(),tmpName.c_str()))
               {
                 ClassTree->Select(((TTreeNode*)itNode[0]));
                 ((TTreeNode*)itNode[0])->MakeVisible();
                 ActiveControl = ClassTree;
                 CanCont = false;
                 break;
               }
            }
           SelNode = NodeMap+ClassTree->Selected->AbsoluteIndex+1;
           for (itNode = NodeMap; (itNode != SelNode) && CanCont; itNode++)
            {
              PrBar->Position++; Application->ProcessMessages();
              if (wcsstr(((TTreeNode*)itNode[0])->Text.UpperCase().c_str(),tmpName.c_str()))
               {
                 ClassTree->Select(((TTreeNode*)itNode[0]));
                 ((TTreeNode*)itNode[0])->MakeVisible();
                 ActiveControl = ClassTree;
                 CanCont = false;
                 break;
               }
            }
         }
      }
     __finally
      {
        PrBar->Visible = false;
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TirTreeClassForm::FSetClCode(__int64 AVal)
{
  FclCode = AVal;
  ClassTree->Select(ClassTree->Items->Item[0]);
  if (AVal)
   {
     if (ClassTree->Items->Count)
      {
        void** EndNode = NodeMap+ClassTree->Items->Count;
        void** itNode;
        for (itNode = NodeMap; itNode != EndNode; itNode++)
         {
           Application->ProcessMessages();
           if ((__int64)((TTreeNode*)itNode[0])->Data == AVal)
            {
              ClassTree->Select(((TTreeNode*)itNode[0]));
              ActiveControl = ClassTree;
              break;
            }
         }
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TirTreeClassForm::FormDestroy(TObject *Sender)
{
  delete[] NodeMap;
}
//---------------------------------------------------------------------------
void __fastcall TirTreeClassForm::ClassTreeChange(TObject *Sender,
      TTreeNode *Node)
{
   if (Node)
    StatusBar->Panels->Items[1]->Text = Node->Text;
   else
    StatusBar->Panels->Items[1]->Text = "";
}
//---------------------------------------------------------------------------
bool __fastcall TirTreeClassForm::FGetTemplateMode()
{
  return !SelGroupChB->Visible;
}
//---------------------------------------------------------------------------
void __fastcall TirTreeClassForm::FSetTemplateMode(bool AMode)
{
  SelGroupChB->Visible = AMode;
}
//---------------------------------------------------------------------------

