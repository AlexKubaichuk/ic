//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "RegLab.h"
#include "RegEDFunc.h"
#include "cxGroupBox.hpp"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define dbLab       ((TDBText*)ED)

//---------------------------------------------------------------------------
__fastcall TRegLabItem::TRegLabItem (TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                     UnicodeString APref,
                                     TRegEDContainer *ACtrlOwner,
                                     TDataSource* ASrc, int AWidth)
           :TPanel(AOwner)   // ����������� ��� DBLabel-controls
{
  if (!ACtrlOwner)
   throw ERegLabError(FMT(&_iscRegEDErrorNoOwner));
  FCtrlOwner = ACtrlOwner;
  FTabPref = APref;
  quFree = AquFree;
  FSrc = ASrc; SetLabList = new TList; SetEnList = new TList;
//   CBList = NULL;
  ED = NULL; Lab = NULL; FNode = ANode; //FNode->LabCtrl = this;
  Width = AWidth-5;
  Left = 1;
  TwoLine = false; Parent = AParent; Alignment = taLeftJustify; Caption = "";
  BevelInner = bvNone; BevelOuter = bvNone;
  FFlName = "R"+FNode->AV["uid"].UpperCase();
  isTmpVal = false;
  Lab = new TLabel(this);  Lab->Parent = this;
//   Lab->Transparent = true;
  Lab->Top = 1;            Lab->Left = 2;
  Lab->Font->Color = clWindowText;
  Lab->Font->Style.Clear();
//   Lab->Font->Style = TFontStyles() << fsBold;
  Lab->Caption = FNode->AV["name"]+":";
  if (FNode->CmpName("text,date,datetime,time,digit,choice"))
   {
     ED = new TDBText(this);
     dbLab->Font->Style = TFontStyles() << fsBold;
     dbLab->DataField = FFlName; dbLab->DataSource = FSrc;
     int cW = Width - Lab->Left - Lab->Width - 4;
     int xW = 0;
     if (FNode->CmpName("date"))          xW = 85;
     else if (FNode->CmpName("text"))     xW = 6*FNode->AV["length"].ToInt();
     else if (FNode->CmpName("datetime")) xW = 140;
     else if (FNode->CmpName("time"))     xW = 70;
     else if (FNode->CmpName("digit"))    xW = 6*FNode->AV["digits"].ToInt();
     else if (FNode->CmpName("choice"))
      {
        TTagNode *itxNode = FNode->GetFirstChild();
        while(itxNode)
         {
           if (itxNode->CmpName("choicevalue"))
            {
              if (xW < itxNode->AV["name"].Length()*8) xW = itxNode->AV["name"].Length()*8;
            }
           itxNode = itxNode->GetNext();
         }
      }
     if (xW < cW)
      {
        dbLab->Left = Lab->Left+Lab->Width+4;
        dbLab->Top = Lab->Top;
        Height = 14;
      }
     else
      {
        dbLab->Left = Lab->Left+2;
        dbLab->Top = Lab->Top+Lab->Height;
        dbLab->Width = Width - 6;
        Height = 28;
      }
     dbLab->Update();
   }
  else if (FNode->CmpName("binary"))
   {
     Lab->Caption = FNode->AV["name"];
     Height = 14;
   }
  else if (FNode->CmpName("choiceBD,class,choiceTree,extedit"))
   {
     ED = new TLabel(this);
     ((TLabel*)ED)->Parent = this;
     ((TLabel*)ED)->Transparent = true;
     ((TLabel*)ED)->Font->Style = TFontStyles() << fsBold;
//      CBList = new TStringList;
     if (FNode->CmpName("extedit"))
      ((TLabel*)ED)->Caption = GetExtData(); //FNode->AV["name"]+":"+
     else if (SrcDS()->Active)
      {
        try
         {
           if (FNode->CmpName("choiceTree"))
            {
              TTagNode *clN = FCtrlOwner->GetNode(AVUp(FNode,"ref"));
              irExecProc("CLASSDATA_"+FTabPref+AVUp(clN,"tblname"), quFree, "CL_CODE", Variant(irquAsInt(SrcDS(),FFlName)));
              if (FNode->AV["dformat"] == "extcode")
               ((TLabel*)ED)->Caption = irquAsStr(quFree,"P_EXTCODE");
              else if (FNode->AV["dformat"] == "text")
               ((TLabel*)ED)->Caption = irquAsStr(quFree,"P_NAME");
              else
               ((TLabel*)ED)->Caption = irquAsStr(quFree,"P_EXTCODE")+" "+irquAsStr(quFree,"P_NAME");
            }
           else
            {
              if (FNode->CmpName("choiceBD"))
               ((TLabel*)ED)->Caption = FCtrlOwner->ClassData->GetData(FTabPref+AVUp(FNode,"ref"),irquAsInt(SrcDS(),FFlName));
              else
               ((TLabel*)ED)->Caption = FCtrlOwner->ClassData->GetData(FTabPref+AVUp(FNode,"uid"),irquAsInt(SrcDS(),FFlName));
            }
         }
        __finally
         {
           irExecCloseProc(quFree,true);
         }
      }
   }
  else if (FNode->CmpName("unitgroup"))
   {
     Color = DrColor(clBtnFace,16);
     Lab->Top = -1; Lab->Left = 5;
//      Lab->Caption = " "+FNode->AV["name"]+" ";
     Lab->Visible = false;
     Lab->Font->Color = clNavy;
     ED = new TcxGroupBox(this);//TBevel(this);
     ED->Parent = this;  ED->Top = 0;  ED->Left = 0; // ED->Top = 5;  ED->Left = -4;
     ED->Width = Width-17;//20;
     ((TcxGroupBox*)ED)->Caption = " "+FNode->AV["name"]+" ";
     ((TcxGroupBox*)ED)->Style->Font->Color = clNavy;
//      ((TBevel*)ED)->SendToBack();
     Height = 15;
     ED->Anchors.Clear();
     ED->Anchors << akLeft << akRight << akTop;
   }
  else
   {
     MessageBox(NULL,(" Tag type error: \""+UnicodeString(FNode->Name)+"\" !!!").c_str(),FMT(icsErrorMsgCaption).c_str(),MB_ICONSTOP);
     return;
   }
  if (ED && !FNode->CmpName("unitgroup"))
   {
     ED->Parent = this;
     int lnCount = FNode->AV["linecount"].ToIntDef(0);
     if (lnCount == 0)
      {
        if (FNode->AV["minleft"].ToIntDef(0))
         {
           if ((Lab->Left+Lab->Width+4) > FNode->AV["minleft"].ToIntDef(0))
            ED->Left = Lab->Left+Lab->Width+4;
           else
            ED->Left = FNode->AV["minleft"].ToIntDef(0);
         }
        else
         ED->Left = Lab->Left+Lab->Width+4;
        ED->Top = Lab->Top;
        ((TLabel*)ED)->Width = Width - ED->Left - 20;
        ((TLabel*)ED)->Constraints->MaxHeight = 14;
        ((TLabel*)ED)->Constraints->MinHeight = 14;
        ED->Anchors.Clear();
        ED->Anchors << akLeft << akRight << akTop;
        ((TLabel*)ED)->WordWrap = false;
        ((TLabel*)ED)->AutoSize = false;
        ((TLabel*)ED)->Update();
        Height = 14;
      }
     else
      {
        ((TLabel*)ED)->AutoSize = false;
        ED->Anchors.Clear();
        ED->Anchors << akLeft << akRight << akTop;
        ED->Left = Lab->Left+2;
        ED->Top = Lab->Top+Lab->Height+1;
        ((TLabel*)ED)->Width = Width - ED->Left - 6;
        ((TLabel*)ED)->Constraints->MaxHeight = 14*lnCount;
        ((TLabel*)ED)->Constraints->MinHeight = 14*lnCount;
        Height = 14*lnCount+14;
        ((TLabel*)ED)->WordWrap = true;
        ((TLabel*)ED)->Update();
      }
   }
  Anchors.Clear();
  Anchors << akLeft << akRight << akTop;
}
//---------------------------------------------------------------------------
void __fastcall TRegLabItem::FOnLabResize(TObject *Sender)
{
 if (ED)
  {
    if (FNode->CmpName("choiceBD,class,choiceTree,extedit"))
     {
       ((TLabel*)ED)->Update();
     }
  }
}
//---------------------------------------------------------------------------
void __fastcall TRegLabItem::SetOnDependValues()
{
   if (FNode->CmpName("text,date,datetime,time,digit,choice,binary"))
    {// ��������� ����������� ��� �����
      if (FNode->AV["depend"].Length() == 9 && isRefMLabCtrl(FNode))
       RefMLabCtrl(FNode)->SetOnLab(FNode);
      else if (FNode->AV["ref"].Length() == 4 && isRefLabCtrl(FNode))
       RefLabCtrl(FNode)->SetOnLab(FNode);
    }
   if (FNode->GetChildByName("actuals"))
    {// ��������� ����������� "Enabled"
      TTagNode *itxNode = FNode->GetChildByName("actuals")->GetFirstChild();
      while (itxNode)
       {
         TRegLabItem* SSS;
         if (itxNode->AV["depend"].Length() == 9 && isRefMLabCtrl(itxNode))
          {
            SSS = RefMLabCtrl(itxNode);
            if (SSS) SSS->SetOnEnable(FNode);
          }
         else if (itxNode->AV["ref"].Length() == 4 && isRefLabCtrl(itxNode))
          {
            SSS = RefLabCtrl(itxNode);
            if (SSS) SSS->SetOnEnable(FNode);
          }
         itxNode = itxNode->GetNext();
       }
    }
   if (FNode->AV["inlist"].LowerCase() == "no")
    {
      CMinH = 0; CMaxH = 0; Height = 0;
    }
}
//---------------------------------------------------------------------------
__fastcall TRegLabItem::~TRegLabItem()
{
  if (FNode->CmpName("text,date,datetime,time,digit,choice"))    delete  dbLab;
  else if (FNode->CmpName("choiceBD,class,choiceTree,extedit"))  delete ((TLabel*)ED);
  else if (FNode->CmpName("unitgroup"))                          delete ((TBevel*)ED);
//  FNode->LabCtrl = NULL;
  if (Lab) delete Lab;
  Lab = NULL;
  if (SetLabList) delete SetLabList;
  SetLabList = NULL;
  if (SetEnList) delete SetEnList;
  SetEnList = NULL;
//  if (CBList)   delete CBList;
//  CBList = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TRegLabItem::SetEnable(bool AEnable)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (AEnable)
   {
     if (Lab) Lab->Font->Color = clWindowText;
     if (ED)
      {
        if (FNode->CmpName("text,date,datetime,time,digit,choice"))
          dbLab->DataField = FFlName;
        ED->Enabled = true;
      }

   }
  else
   {
     if (Lab) Lab->Font->Color = DrColor(clBtnFace,32);
     if (ED)
      {
        if (FNode->CmpName("text,date,datetime,time,digit,choice"))
          dbLab->DataField = "";
        ED->Enabled = false;
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TRegLabItem::SetLabel(UnicodeString xLab)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FNode->CmpName("binary"))
    {
      if (xLab.Length()) Lab->Caption = xLab;
      else            Lab->Caption = "";//FNode->AV["name"]+":";
    }
   else
    {
      if (xLab.Length()) Lab->Caption = xLab+":";
      else            Lab->Caption = "";//FNode->AV["name"]+":";
    }
   Lab->Update();
}
//---------------------------------------------------------------------------
void __fastcall TRegLabItem::SetOnLab(TTagNode* ANode)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   SetLabList->Add(ANode);
   riDataChange();
}
//---------------------------------------------------------------------------
void __fastcall TRegLabItem::SetOnEnable(TTagNode* ANode)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   SetEnList->Add(ANode);
   riDataChange();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TRegLabItem::GetExtData()
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   UnicodeString Src = "";
   if (FGetExtData)
    {
      if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
      FGetExtData(FNode,Src,FSrc);
    }
   return Src;
}
//---------------------------------------------------------------------------
void __fastcall TRegLabItem::riDataChange()
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
//   if (FNode->CmpName("choice"))
   if (FNode->CmpName("choiceBD,class,choiceTree"))
    {
      if (SrcDS()->Active)
       {
         if (irquAsInt(SrcDS(),FFlName) == 0)
          {
           ((TLabel*)ED)->Caption = "";
          }
         else
          {
            try
             {
               if (FNode->CmpName("choiceTree"))
                {
                  TTagNode *clN = FCtrlOwner->GetNode(AVUp(FNode,"ref"));
                  irExecProc("CLASSDATA_"+FTabPref+AVUp(clN,"tblname"), quFree, "CL_CODE", Variant((__int64)irquAsInt(SrcDS(),FFlName)));
                  if (FNode->AV["dformat"] == "extcode")
                   ((TLabel*)ED)->Caption = irquAsStr(quFree,"P_EXTCODE");
                  else if (FNode->AV["dformat"] == "text")
                   ((TLabel*)ED)->Caption = irquAsStr(quFree,"P_NAME");
                  else
                   ((TLabel*)ED)->Caption = irquAsStr(quFree,"P_EXTCODE")+" "+irquAsStr(quFree,"P_NAME");
                }
               else
                {
                  if (FNode->CmpName("choiceBD"))
                   ((TLabel*)ED)->Caption = FCtrlOwner->ClassData->GetData(FTabPref+AVUp(FNode,"ref"),(__int64)irquAsInt(SrcDS(),FFlName));
                  else
                   ((TLabel*)ED)->Caption = FCtrlOwner->ClassData->GetData(FTabPref+AVUp(FNode,"uid"),(__int64)irquAsInt(SrcDS(),FFlName));
                }
             }
            __finally
             {
               irExecCloseProc(quFree,true);
             }
          }
       }
    }
   else if (FNode->CmpName("extedit"))
    {
      ((TLabel*)ED)->Caption = GetExtData();
    }
   else if (FNode->CmpName("binary"))
    {
      if (SrcDS()->Active)
       {
         if (_TemplateValue(FNode).IsEmpty())
          {
            if (FNode->AV["depend"].Length() != 9)
             {
               Lab->Caption = FNode->AV["name"];
             }
            Lab->Color = clBtnFace;
            if (irquAsInt(SrcDS(),FFlName) == 0)
             Lab->Font->Color = DrColor(clBtnFace,32);
            else
             Lab->Font->Color = clWindowText;
          }
         else
          {
            if (FNode->AV["depend"].Length() != 9)
             {
               if (int(_TemplateValue(FNode)) == 0)
                 Lab->Caption = FNode->AV["invname"];
               else
                 Lab->Caption = FNode->AV["name"];
             }
            Lab->Color = clBlue;
            Lab->Font->Color = clWhite;
          }
         Lab->Update();
       }
    }
  if (!FNode->CmpName("unitgroup,binary"))
   {
     if (_TemplateValue(FNode).IsEmpty())
      {
        Lab->Color = clBtnFace;
        if (Lab->Font->Color == clWhite)
         Lab->Font->Color = clWindowText;
      }
     else
      {
        Lab->Color = clBlue;
        Lab->Font->Color = clWhite;
      }
   }
  UnicodeString xRef,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ���������� ����� --------------------------------------------------------------------------------
  if ((SetLabList->Count > 0)&&SrcDS()->Active) // ��������� ������ ��� Label �������� �� choice ��� choiceBD
   {
     if (FNode->CmpName("choice")) // ����� �� choice
      {
        for (int i = 0; i < SetLabList->Count; i++)
         {
           xRef = ((TTagNode*)SetLabList->Items[i])->AV["ref"].UpperCase(); // ref == uid.choice
           if (xRef == FUID)
            labLabCtrl(i)->SetLabel(irquAsStr(SrcDS(),FFlName));
         }
      }
     else if (FNode->CmpName("choiceBD")) // ����� �� choiceBD
      {
        TTagNode *xNode;
        for (int i = 0; i < SetLabList->Count; i++)
         {
           xNode = ((TTagNode*)SetLabList->Items[i]);
           xRef = xNode->AV["depend"].UpperCase();     // ������ xxxx.yyyy
           // xxxx == uid.choiceBD yyyy == uid.��������������* ��� ref.choiceBD == uid.��������������
           if ((xRef.Length() == 9)&&(xRef.SubString(1,4) == FUID))
            {                                            // uid.��������������* - uid ����� ��� ������������ ��������������
              xRef = xNode->AV["ref"]; // ref - uid ���� �������������� �������� �������� �������� ������
              FGetExtCtrl(xNode->AV["uid"])->SetLabel(GetClValue(xRef));
            }
         }
      }
   }
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
//     return Variant(cFields[ARef.SubString(6,4)]);
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TRegLabItem::Condition(TTagNode *CondNode)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC;
  UnicodeString xCond  = CondNode->AV["condition"];
  Variant* pmList; pmList = new Variant[CondNode->Count];
  CalcParam(CondNode->GetFirstChild(),pmList);
  wchar_t *spCh;
  wchar_t *Src = new wchar_t[xCond.Length()+1];
  wcscpy(Src,xCond.c_str());
  spCh = wcschr(Src,' ');
  while (spCh) {spCh[0] = '\n'; spCh = wcschr(Src,' '); }
  TStringList  *ExpList = new TStringList;
  ExpList->Text = UnicodeString(Src);
  UnicodeString tmpOper;
  for (int i = 0; i < ExpList->Count; i++)
   {
     tmpOper = ExpList->Strings[i];
     if (tmpOper[1] == '@')
      ExpList->Strings[i] = UnicodeString((wchar_t*)pmList[tmpOper.SubString(2,tmpOper.Length()-1).ToInt()]);
   }
  RC = (bool)CalcCondition(ExpList);
  if (Src) delete[] Src;
  Src = NULL;
  if (pmList)  delete[] pmList;
  pmList = NULL;
  if (ExpList) delete ExpList;
  ExpList = NULL;
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TRegLabItem::CalcParam(TTagNode *AFirst,Variant* AParam)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   TTagNode *xNode = AFirst;
   Variant RC = NULL;
   int i = 0;
   UnicodeString xxRef;
   while(xNode)
    {
      if (xNode->AV["depend"].Length() == 9 && isRefMLabCtrl(xNode))
       {
         xxRef = xNode->AV["depend"].SubString(6,4)+"."+ AVUp(xNode,"ref");
         RC = RefMLabCtrl(xNode)->GetCompValue(xxRef);
       }
      else if (xNode->AV["ref"].Length() == 4 && isRefLabCtrl(xNode))
        RC = RefLabCtrl(xNode)->GetCompValue("");
      else ShowMessage(FMT(&_iscRegEDErrorNoRef));
      AParam[i] = RC;
      i++;
      xNode = xNode->GetNext();
    }
}
//---------------------------------------------------------------------------
Variant __fastcall TRegLabItem::GetCompValue(UnicodeString ARef)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if ((!ARef.Length())&&FNode->CmpName("date,datetime,time,digit,choice,binary,choiceTree"))
   {
     if (SrcDS()->Active)
      {
        if (irquIsNull(SrcDS(),FFlName))
         return Variant(-2);
        else
         {
           if (FNode->CmpName("date"))
            return Variant(irquAsDate(SrcDS(),FFlName).FormatString("'dd.mm.yyyy'"));
           else if (FNode->CmpName("datetime,time,digit"))
            return Variant(irquAsStr(SrcDS(),FFlName));
           else if (FNode->CmpName("choice,binary,choiceTree"))
            return Variant((int)irquAsInt(SrcDS(),FFlName));
         }
      }
     else   return Variant(-2);
   }
  else if ((ARef.Length())&&FNode->CmpName("choiceBD"))
   {
     return Variant(GetClValue(ARef.SubString(6,4)));
   }
  else
   {
      ShowMessage(FMT1(&_iscRegEDErrorRefObject,ARef).c_str());
      return Variant(-2);
   }
  return -2;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TRegLabItem::GetClValue(UnicodeString ARef)
{
  if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString RC = "";
  try
   {
     if (FNode->CmpName("choiceBD"))
      irExecProc("CLASSDATA_"+FTabPref+AVUp(FNode,"ref"), quFree, "CL_CODE", Variant((__int64)irquAsInt(SrcDS(),FFlName)));
     else
      irExecProc("CLASSDATA_"+FTabPref+AVUp(FNode,"uid"), quFree, "CL_CODE", Variant((__int64)irquAsInt(SrcDS(),FFlName)));
     RC = irquAsStr(quFree,"P_"+ARef);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TRegLabItem* __fastcall TRegLabItem::FGetExtCtrl(UnicodeString AUID)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (FCtrlOwner)
    return FCtrlOwner->GetLabControl(AUID);
   else
    throw ERegLabError(FMT(&_iscRegEDErrorNoOwner));
}
//---------------------------------------------------------------------------
bool __fastcall TRegLabItem::isRefLabCtrl(TTagNode *ANode)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   TTagNode *tmp = FCtrlOwner->GetNode(AVUp(ANode,"ref"));
   if (tmp)
    if (FGetExtCtrl(tmp->AV["uid"])) return true;
   return false;
}
//---------------------------------------------------------------------------
bool __fastcall TRegLabItem::isRefMLabCtrl(TTagNode *ANode)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   TTagNode *tmp = FCtrlOwner->GetNode(_GUI(AVUp(ANode,"depend")));
   if (tmp)
    if (FGetExtCtrl(tmp->AV["uid"])) return true;
   return false;
}
//---------------------------------------------------------------------------
TRegLabItem* __fastcall TRegLabItem::RefLabCtrl(TTagNode* ARef)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FCtrlOwner->GetNode(ARef->AV["ref"]))
   return FGetExtCtrl(ARef->AV["ref"]);
  else
   return NULL;
}
//---------------------------------------------------------------------------
TRegLabItem* __fastcall TRegLabItem::RefMLabCtrl(TTagNode* ARef)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FCtrlOwner->GetNode(_GUI(ARef->AV["depend"])))
   return FGetExtCtrl(_GUI(ARef->AV["depend"]));
  else
   return NULL;
}
//---------------------------------------------------------------------------
TRegLabItem* __fastcall TRegLabItem::labLabCtrl(int AIdx)
{
   if (!this) throw ERegLabError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return FGetExtCtrl(((TTagNode*)SetLabList->Items[AIdx])->AV["uid"]);
}
//---------------------------------------------------------------------------
TDataSet* __fastcall TRegLabItem::SrcDS()
{
  return FSrc->DataSet;
}
//---------------------------------------------------------------------------
bool __fastcall TRegLabItem::SrcDSEditable()
{
  return (SrcDS()->Active && (SrcDS()->State != dsBrowse));
}
//---------------------------------------------------------------------------

