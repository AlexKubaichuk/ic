object ITemplateForm: TITemplateForm
  Left = 368
  Top = 221
  HelpContext = 1002
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  BorderWidth = 3
  Caption = #1060#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1077' '#1096#1072#1073#1083#1086#1085#1072
  ClientHeight = 109
  ClientWidth = 354
  Color = clBtnFace
  Constraints.MinHeight = 120
  Constraints.MinWidth = 300
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 68
    Width = 354
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 45
    ExplicitWidth = 317
    DesignSize = (
      354
      41)
    object BtnBevel: TBevel
      Left = 0
      Top = 0
      Width = 354
      Height = 2
      Align = alTop
      Shape = bsBottomLine
      ExplicitWidth = 317
    end
    object OkBtn: TcxButton
      Left = 186
      Top = 9
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      TabOrder = 0
      OnClick = OkBtnClick
      ExplicitLeft = 149
    end
    object CancelBtn: TcxButton
      Left = 270
      Top = 9
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      ModalResult = 2
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = CancelBtnClick
      ExplicitLeft = 233
    end
  end
  object CompPanel: TPanel
    Left = 0
    Top = 0
    Width = 323
    Height = 40
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 1
  end
end
