//---------------------------------------------------------------------------
#include <vcl.h>
//#include <stdio.h>
//#include <clipbrd.hpp>
#pragma hdrstop

#include "riBinaryTmpl.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED_ChB     ((TcxCheckBox*)ED)
//---------------------------------------------------------------------------
__fastcall TriBinaryTmpl::TriBinaryTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref,  TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  UnicodeString AVal)
            :
                                      TRegEDItem(AOwner, AParent,ANode,AquFree,
                                      APref,  AReqColor,
                                      ACtrlOwner,  AVal)
{
  ED = new TcxCheckBox(this);
  tED_ChB->Caption = FNode->AV["name"];
  tED_ChB->Properties->AllowGrayed = true;
  tED_ChB->State = cbsGrayed;

  tED_ChB->Style->LookAndFeel->NativeStyle = FCtrlOwner->NativeStyle;
  tED_ChB->Style->LookAndFeel->Kind = FCtrlOwner->LFKind;
  tED_ChB->Transparent = true;

  CMaxH = 21; CMinH = 21;
  tED_ChB->Constraints->MinWidth = 60;
  tED_ChB->Left = 5;
  tED_ChB->Top = 1;
  tED_ChB->Height = 21;
  tED_ChB->Parent =  this;
  tED_ChB->Width = tED_ChB->Canvas->TextWidth(tED_ChB->Caption)+25;
  Height = 21;
  Width = ED->Left+ED->Width;
  if (!FCtrlOwner->TemplateField && !_TemplateValue(FNode).IsEmpty())
   tED_ChB->Checked = int(_TemplateValue(FNode));
/*
   if (Parent->ClassNameIs("TriBinaryTmpl"))
    {
      Anchors.Clear();
      Anchors << akLeft << akRight << akTop;
    }
*/
}
//---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetOnDependValues()
{
  if (FNode->AV["depend"].Length() == 9 && isRefMCtrl(_GUI(FNode->AV["depend"])))
   RefMCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(FNode);
  else if (FNode->AV["ref"].Length() == 4 && isRefCtrl(FNode))
   RefCtrl(FNode)->SetOnLab(FNode);
  FSetOnDependValues();
  tED_ChB->OnClick = riDataChange;
}
//---------------------------------------------------------------------------
__fastcall TriBinaryTmpl::~TriBinaryTmpl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   tED_ChB->OnClick = NULL;
   delete tED_ChB;
}
//---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
  if (!TwoLine&&ED&&(!FNode->CmpName("binary"))) ED->Left = ALeft;
  if (FNode->CmpName("digit,date")&&TmpCtrl)     ED2->Left = ALeft;
  if (ED)                                Width = ED->Left+ED->Width+4;
  if (TmpCtrl)
   if (FNode->CmpName("choiceTree"))      Width = ED->Left+ED->Width+4+ED2->Width;
  else
   if (FNode->CmpName("choiceTree"))      Width = ED->Left+ED->Width+4;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!TwoLine&&ED&&(!FNode->CmpName("binary")))  *AEDLeft = ED->Left;
  else                                            *AEDLeft = -1;
  if (!TwoLine&&Lab&&(!FNode->CmpName("binary"))) *ALabR = Lab->Left+Lab->Width;
  else                                            *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::cResize(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (!IsAlign)
   {
     ED->Left = 2;
     ED->Width = Width-4;
   }
}
//---------------------------------------------------------------------------
bool __fastcall TriBinaryTmpl::UpdateChanges()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  tED_ChB->OnClick((TObject*)ED);
  return !IsTemplate(FNode);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriBinaryTmpl::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  if (ED->Enabled)
   {
     if (ABinaryNames)
      {
        if (tED_ChB->State != cbsGrayed) xVal += (tED_ChB->Checked)? FNode->AV["name"]:FNode->AV["invname"];
      }
     else
      {
        if (tED_ChB->State != cbsGrayed)
         xVal += UnicodeString((tED_ChB->Checked)?
                 FMT(icsriBinaryCheckTxt).c_str() :
                 FMT(icsriBinaryUnCheckTxt).c_str());
      }
   }
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  ED->Enabled = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetValue(UnicodeString AVal,bool AEnable)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
//  SetEnable(AEnable);
  if (AVal.Length())
   {
     tED_ChB->Checked = (bool)(AVal.ToInt());
   }
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetLabel(UnicodeString xLab)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (xLab.Length()) tED_ChB->Caption = xLab;
  else            tED_ChB->Caption = "";//FNode->AV["name"]+":";
  tED_ChB->Update();
  SIZE sz;
  TLabel *tmpLab = new TLabel(this);
  tmpLab->Parent = this;
  tmpLab->Transparent = true;
  tmpLab->Caption = xLab;
  tmpLab->Font->Assign(tED_ChB->Style->Font);
  ::GetTextExtentPoint32(tmpLab->Canvas->Handle,xLab.c_str(),xLab.Length(),&sz);
  ED->Width = 25+sz.cx;
  this->Width = ED->Width+5;
  delete tmpLab;
}
//---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetEnList->Add(ANode);
  tED_ChB->OnClick = riDataChange;
  riDataChange(this);
}
//---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::riDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  FDataChange(Sender);
  if (!TmpCtrl) return;
// ������ �������� ��� ��������� �������� �������� ����� choiceBD ----------------------------------
  Variant xVal; xVal.Empty();
  if (ED->Enabled||isVal)
   {
     if (tED_ChB->State != cbsGrayed) xVal = Variant(tED_ChB->Checked);
   }
  _TemplateValue(FNode) = xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// ��������� enabled ��� ��������� ��������� -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
  if (FGetDataChange)
   {
     UnicodeString Src = IntToStr((int)tED_ChB->Checked);
     FGetDataChange(FNode,Src,NULL);
   }
}
//---------------------------------------------------------------------------
void __fastcall TriBinaryTmpl::FDefaultDataChange(TObject *Sender)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (FGetDataChange)
   {
     UnicodeString Src = IntToStr((int)tED_ChB->Checked);
     FGetDataChange(FNode,Src,NULL);
   }
}
//---------------------------------------------------------------------------
TControl* __fastcall TriBinaryTmpl::GetControl(int AIdx)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (TControl*)ED;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriBinaryTmpl::GetFirstEnabledControl()
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return (ED->Enabled&&isEdit)? ((TControl*)ED) : NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriBinaryTmpl::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return IntToStr((int)tED_ChB->Checked);
}
//---------------------------------------------------------------------------
Variant __fastcall TriBinaryTmpl::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  if (tED_ChB->State != cbsGrayed)
   return Variant((int)tED_ChB->Checked);
  else
   return Variant::Empty();
}
//---------------------------------------------------------------------------
bool __fastcall TriBinaryTmpl::CheckReqValue(TcxPageControl *APC)
{
  if (!this)throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  bool RC = true;
  if (Required)
   {
     try
      {
        if (ED->Enabled)
         RC = (tED_ChB->State != cbsGrayed);
      }
     __finally
      {
        if (!RC)
         {
           if (APC) APC->ActivePageIndex = this->Tag;
           UnicodeString ErrMessage;
           if (Lab) ErrMessage = Lab->Caption;
           else     ErrMessage = FNode->AV["name"];
           ErrMessage = FMT1(icsRegItemErrorReqField,ErrMessage);
           MessageBox(Handle,ErrMessage.c_str(),FMT(icsInputErrorMsgCaption).c_str(),MB_ICONWARNING);
           ED->SetFocus();
         }
      }
   }
  return RC;
}
//---------------------------------------------------------------------------

