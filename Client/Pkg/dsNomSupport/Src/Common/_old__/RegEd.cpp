//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "RegEd.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
//---------------------------------------------------------------------------
__fastcall TRegEDItem::TRegEDItem(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref, TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  TDataSource* ASrc, bool IsApp)
                  :TPanel(AOwner)   // TcxGroupBox ����������� ��� DB-controls
{
   // common init
   if (!ACtrlOwner)                                            
    throw ERegEDError(FMT(&_iscRegEDErrorNoOwner));
   FCtrlOwner = ACtrlOwner;
   FReqColor =  AReqColor;
   FTabPref = APref;
   FExtBtnClick = NULL;
   FTreeBtnClick = NULL;
   FGetDataChange = NULL;                           
   quFree = AquFree;
   TmpCtrl = false;
   IsAlign = false;
   isVal = false;
   IsAppend = IsApp;  FSrc = ASrc; SetLabList = new TList; SetEnList = new TList;
   SetValList = new TList; ED2 = NULL;
   CBList = NULL; ED = NULL; Lab = NULL; FNode = ANode; //FNode->Ctrl = this;
   TRect xSize = Rect(0,0,120,21); //  default ������� � ������ ��� "ED"
   is3D = (AVLo(FNode,"is3D") == "yes");
   isFV = FNode->CmpAV("fixedvar","yes");
   isDelegate = FNode->CmpAV("isdelegate","yes");
   isEdit = !(AVLo(FNode,"isedit") == "no") && !isDelegate;
   isEnabled = !(!isEdit && (AVLo(FNode,"default").Length()));
   Height = 22;  CMaxH = 22;  CMinH = 22;
   TwoLine = false; Parent = AParent; Alignment = taLeftJustify; Caption = "";
   BevelInner = bvNone; BevelOuter = bvNone;

   FFlName = "R"+AVUp(FNode,"uid");
   FRequired  = (AVLo(FNode,"required") == "yes");
   OnResize = cResize;
   isTemplate = IsTemplate(FNode);
}
//---------------------------------------------------------------------------
void __fastcall TRegEDItem::FSetOnDependValues()
{
  if (FNode->GetChildByName("actuals"))
   {// ��������� ����������� "Enabled"
     TTagNode *itxNode = FNode->GetChildByName("actuals")->GetFirstChild();
     UnicodeString FDep,FDep2;
     while (itxNode)
      {
        FDep2 = itxNode->AV["depend"].UpperCase();
        bool FCanFill = true;
        if (FDep2.Length() >= 9)
         {
           while (FDep2.Length())
            {
              FDep  = GetPart1(FDep2,';').Trim();
              if (!FDep.Length())
               FDep = FDep2;
              if (FDep.Length() == 9 && isRefMCtrl(_GUI(FDep)))
               {
                 RefMCtrl(_GUI(FDep))->SetOnEnable(FNode);
                 FCanFill = false;
               }
              FDep2 = GetPart2B(FDep2,';').Trim();
            }
         }
        if (FCanFill && (itxNode->AV["ref"].Length() == 4) && isRefCtrl(itxNode))
         RefCtrl(itxNode)->SetOnEnable(FNode);
        itxNode = itxNode->GetNext();
      }
   }
}
//---------------------------------------------------------------------------
__fastcall TRegEDItem::TRegEDItem(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,TDataSet *AquFree,
                                  UnicodeString APref,  TColor AReqColor,
                                  TRegEDContainer *ACtrlOwner,
                                  UnicodeString AVal)
           :TPanel(AOwner)   // TcxGroupBox ����������� ��� Template-controls
{
   if (!ACtrlOwner)
    throw ERegEDTemplateError(FMT(&_iscRegEDErrorNoOwner));
   FCtrlOwner = ACtrlOwner;
   FReqColor =  AReqColor;
   FTabPref = APref;
   FExtBtnClick = NULL;
   FTreeBtnClick = NULL;
   FGetDataChange = NULL;
   quFree = AquFree;
   TmpCtrl = true; ED2 = NULL;
   IsAppend = false;  FSrc = NULL; SetLabList = new TList; SetEnList = new TList;
   SetValList = new TList;
   CBList = NULL; ED = NULL; Lab = NULL; FNode = ANode; //FNode->Ctrl = this;
   TRect xSize = Rect(0,0,120,21); //  default ������� � ������ ��� "ED"
   Height = 22;  CMaxH = 22;  CMinH = 22;
   TwoLine = false; Parent = AParent; Alignment = taLeftJustify; Caption = "";
   BevelInner = bvNone; BevelOuter = bvNone;
   isDelegate = false;
   isEdit = true;//!(AVLo(FNode,"isedit") == "no");
   is3D = (AVLo(FNode,"is3D") == "yes");
   isFV = false;//FNode->CmpAV("fixedvar","yes");

   FFlName = "R"+AVUp(FNode,"uid");
   FRequired  = (AVLo(FNode,"required") == "yes");
   OnResize = cResize;
}
//---------------------------------------------------------------------------
TRegEDItem* __fastcall TRegEDItem::FGetExtCtrl(UnicodeString AUID)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
   if (TmpCtrl)
    {
      if (FCtrlOwner)
       return FCtrlOwner->GetTemplateControl(AUID);
      else
       throw ERegEDTemplateError(FMT(&_iscRegEDErrorNoOwner));
    }
   else
    {
      if (FCtrlOwner)
       return FCtrlOwner->GetEDControl(AUID);
      else
       throw ERegEDError(FMT(&_iscRegEDErrorNoOwner));
    }
}
//---------------------------------------------------------------------------
TRegEDItem* __fastcall TRegEDItem::labCtrl(int AIdx)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
  return FGetExtCtrl(((TTagNode*)SetLabList->Items[AIdx])->AV["uid"]);
}
//---------------------------------------------------------------------------
TRegEDItem* __fastcall TRegEDItem::depCtrl(int AIdx)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
  return FGetExtCtrl(((TTagNode*)SetValList->Items[AIdx])->AV["uid"]);
}
//---------------------------------------------------------------------------
__fastcall TRegEDItem::~TRegEDItem()
{
  if (!this)
   {
     if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
     else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   }
  if (Lab) delete Lab;
  if (SetValList) delete SetValList;
  if (SetLabList) delete SetLabList;
  if (SetEnList) delete SetEnList;
  if (CBList)   delete CBList;
//  this->Parent = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TRegEDItem::SetOnValDepend(TTagNode* ANode)
{
  if (!this)
   {
     if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
     else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   }
  SetValList->Add(ANode);
}
//---------------------------------------------------------------------------
void __fastcall TRegEDItem::FDefaultDataChange(TObject *Sender)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
  if (FGetDataChange)
   {
     UnicodeString Src = "";
//     if (SrcDS()->Active) Src = irquAsStr(SrcDS(),FFlName);
     FGetDataChange(FNode,Src,FSrc);
   }
}
//---------------------------------------------------------------------------
bool __fastcall TRegEDItem::Condition(TTagNode *CondNode)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
  bool RC;
  UnicodeString xCond  = CondNode->AV["condition"];
  Variant* pmList; pmList = new Variant[CondNode->Count];
  CalcParam(CondNode->GetFirstChild(),pmList);
  wchar_t *spCh;
  wchar_t *Src = new wchar_t[xCond.Length()+1];
  wcscpy(Src,xCond.c_str());
  spCh = wcschr(Src,' ');
  while (spCh) {spCh[0] = '\n'; spCh = wcschr(Src,' '); }
  TStringList  *ExpList = new TStringList;
  ExpList->Text = UnicodeString(Src);
  UnicodeString tmpOper;
  for (int i = 0; i < ExpList->Count; i++)
   {
     tmpOper = ExpList->Strings[i];
     if (tmpOper[1] == '@')
      ExpList->Strings[i] = UnicodeString((wchar_t*)pmList[tmpOper.SubString(2,tmpOper.Length()-1).ToInt()]);
   }
  RC = (bool)CalcCondition(ExpList);
  if (Src) delete[] Src;
  Src = NULL;
  if (pmList)  delete[] pmList;
  pmList = NULL;
  if (ExpList) delete ExpList;
  ExpList = NULL;
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TRegEDItem::CalcParam(TTagNode *AFirst,Variant* AParam)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
   TTagNode *xNode = AFirst;
   Variant RC = NULL;
   Variant RC2 = NULL;
   int i = 0;
   UnicodeString xxRef;
   UnicodeString FDep,FDep2;

   while(xNode)
    {
      FDep2 = xNode->AV["depend"].UpperCase();
      bool FCanFill = true;
      if (FDep2.Length() >= 9)
       {
         while (FDep2.Length())
          {
            FDep  = GetPart1(FDep2,';').Trim();
            if (!FDep.Length())
             FDep = FDep2;
            if (FDep.Length() == 9 && isRefMCtrl(_GUI(FDep)))
             {
               xxRef = _UID(FDep)+"."+ xNode->AV["ref"];
               RC = RefMCtrl(_GUI(FDep))->GetCompValue(xxRef);
               FCanFill = false;

/*               if (!RC2.IsNull() && !RC2.IsEmpty() )
                if (UnicodeString(RC2).ToIntDef(-1) != -1)
                 RC = RC2;*/
             }
            FDep2 = GetPart2B(FDep2,';').Trim();
          }
       }
      if (FCanFill)
       {
         if ((xNode->AV["ref"].Length() == 4) && isRefCtrl(xNode))
          RC = RefCtrl(xNode)->GetCompValue("");
         else ShowMessage(FMT(&_iscRegEDErrorNoRef));
       }
      AParam[i] = RC;
      i++;
      xNode = xNode->GetNext();
    }
}
//---------------------------------------------------------------------------
TRegEDItem* __fastcall TRegEDItem::RefCtrl(TTagNode* ARef)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
  if (FCtrlOwner->GetNode(ARef->AV["ref"]))
   return FGetExtCtrl(ARef->AV["ref"]);
  else
   return NULL;
}
//---------------------------------------------------------------------------
TRegEDItem* __fastcall TRegEDItem::RefMCtrl(UnicodeString ADepend)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
  if (FCtrlOwner->GetNode(ADepend))
   return FGetExtCtrl(ADepend);
  else
   return NULL;
}
//---------------------------------------------------------------------------
bool __fastcall TRegEDItem::isRefCtrl(TTagNode *ANode)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
   if (FCtrlOwner->GetNode(AVUp(ANode,"ref")))
    if (FGetExtCtrl(AVUp(ANode,"ref")))
     return true;
   return false;
}
//---------------------------------------------------------------------------
bool __fastcall TRegEDItem::isRefMCtrl(UnicodeString ADepend)
{
   if (!this)
    {
      if (TmpCtrl) throw ERegEDTemplateError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
      else         throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
    }
   if (FCtrlOwner->GetNode(ADepend))
    if (FGetExtCtrl(ADepend))
     return true;
   return false;
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::GetClassCode(UnicodeString &ACode,__int64 cCODE,TTagNode *ANode)
{
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::cResize(TObject *Sender)
{
}
//---------------------------------------------------------------------------
Variant      __fastcall TRegEDItem::GetCompValue(UnicodeString ARef)
{
  return Variant::Empty();
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::FDataChange(TObject *Sender)
{
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::FSetExtBtnClick(TExtBtnClick AExtBtnClick)
{
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::FSetTreeBtnClick(TTreeBtnClick ATreeBtnClick)
{
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::SetDependVal(UnicodeString ADepClCode, __int64 ACode, UnicodeString ADepUID)
{
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::SetOnDependValues()
{
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::SetOnLab(TTagNode* ANode)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  SetLabList->Add(ANode);
  riDataChange(this);
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::riDataChange(TObject *Sender)
{
}
//---------------------------------------------------------------------------
UnicodeString   __fastcall TRegEDItem::GetValueStr(bool ABinaryNames)
{
  return "";
}
//---------------------------------------------------------------------------
TStringList* __fastcall TRegEDItem::GetStrings(UnicodeString AClRef)
{
  return NULL;
}
//---------------------------------------------------------------------------
TControl*    __fastcall TRegEDItem::GetControl(int AIdx)
{
  return NULL;
}
//---------------------------------------------------------------------------
TControl*    __fastcall TRegEDItem::GetFirstEnabledControl()
{
  return NULL;
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::SetEnable(bool AEnable)
{
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::SetEnableEx(bool AEnable)
{
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::SetIsVal(bool AEnable)
{
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::SetValue(UnicodeString AVal,bool AEnable)
{
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::SetLabel(UnicodeString xLab)
{
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::SetOnEnable(TTagNode* ANode)
{
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::SetEDLeft(int ALeft)
{
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::GetEDLeft(int *ALabR, int *AEDLeft)
{
}
//---------------------------------------------------------------------------
UnicodeString   __fastcall TRegEDItem::GetValue(UnicodeString ARef)
{
  return "";
}
//---------------------------------------------------------------------------
UnicodeString   __fastcall TRegEDItem::GetExtValue(UnicodeString ARef)
{
  return "";
}
//---------------------------------------------------------------------------
void         __fastcall TRegEDItem::SetFixedVar(UnicodeString AVals)
{
}
//---------------------------------------------------------------------------
bool         __fastcall TRegEDItem::UpdateChanges()
{
  return false;
}
//---------------------------------------------------------------------------
bool         __fastcall TRegEDItem::CheckReqValue(TcxPageControl *APC)
{
  return false;
}
//---------------------------------------------------------------------------
TDataSet* __fastcall TRegEDItem::SrcDS()
{
  if (FSrc) return FSrc->DataSet;
  else return NULL;
}
//---------------------------------------------------------------------------
bool __fastcall TRegEDItem::SrcDSEditable()
{
  return (SrcDS()->Active && (SrcDS()->State != dsBrowse));
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TRegEDItem::GetPart2B(UnicodeString ARef, char Sep)
{
  int pos = ARef.Pos(Sep);
  if (pos)
   return ARef.SubString(pos+1,ARef.Length()-pos);
  else
   return "";
}
//---------------------------------------------------------------------------
void __fastcall TRegEDItem::FSetEDRequired(bool AVal)
{
}
//---------------------------------------------------------------------------

