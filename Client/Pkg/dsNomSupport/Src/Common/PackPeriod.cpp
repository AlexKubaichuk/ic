//---------------------------------------------------------------------------

#include <vcl.h>
#include <DateUtils.hpp>
#pragma hdrstop

#include "PackPeriod.h"
//#include "reportdef.h"
#include "msgdef.h"
//#include "icsDocExConstDef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//#ifndef _CONFSETTTING
// #error _CONFSETTTING using are not defined
//#endif

#pragma link "cxButtons"
#pragma link "cxCalendar"
#pragma link "cxCheckBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDateUtils"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "dxCore"
#pragma resource "*.dfm"
TPackPeriodForm *PackPeriodForm;
//---------------------------------------------------------------------------
__fastcall TPackPeriodForm::TPackPeriodForm(TComponent* Owner)
        : TForm(Owner)
{
//  FDM = ADM;

//!!!  Caption = FMT(icsDocExDocPeriodCaption);
//!!!  CaptionLab->Caption = FMT(icsDocExDocPeriodCaptionLabCaption);
//!!!  DateFrLab->Caption = FMT(icsDocExDocPeriodDateFrLabCaption);
//!!!  DateToLab->Caption = FMT(icsDocExDocPeriodDateToLabCaption);
//!!!  OkBtn->Caption = FMT(icsDocExDocPeriodOkBtnCaption);
//!!!  CancelBtn->Caption = FMT(icsDocExDocPeriodCancelBtnCaption);

//  YearRBClick(NULL);
  PerTypeCB->ItemIndex = 0;
//  DateFrDE->Style->Color = FReqColor;
//  DateToDE->Style->Color = FReqColor;
//  DateFr = TDate(0);
//  DateTo = TDate(0);
  Date().DecodeDate(&FCY, &FCM, &FCD);
}
//---------------------------------------------------------------------------
void __fastcall TPackPeriodForm::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if ((Key == VK_RETURN)&&OkBtn->Enabled) OkBtnClick(OkBtn);
}
//---------------------------------------------------------------------------
void __fastcall TPackPeriodForm::OkBtnClick(TObject *Sender)
{
/*   pdType = 0;
   if (YearRB->Checked)
    {
      try
       {
         DateFr = StrToDate(("01.01."+YearED->Text).c_str());
         DateTo = StrToDate(("31.12."+YearED->Text).c_str());
         pdType = 5;
       }
      catch (...)
       {
          _MSG_ERR(FMT(icsDocExDocPeriodErrorYearFormat),FMT(icsDocExCommonErrorCaption));
          return;
       }
    }
   else if (HalfYearRB->Checked)
    {
      try
       {
         if (HalfYearCB->ItemIndex < 0) return;
         if (HalfYearCB->ItemIndex == 0)
          {
            DateFr = StrToDate(("01.01."+YearED->Text).c_str());
            DateTo = StrToDate(("30.06."+YearED->Text).c_str());
          }
         else
          {
            DateFr = StrToDate(("01.07."+YearED->Text).c_str());
            DateTo = StrToDate(("31.12."+YearED->Text).c_str());
          }
         pdType = 4;
       }
      catch (...)
       {
          _MSG_ERR(FMT(icsDocExDocPeriodErrorDateFormat),FMT(icsDocExCommonErrorCaption));
          return;
       }
    }
   else if (QuarterRB->Checked)
    {
      try
       {
         if (QuarterCB->ItemIndex == 0)
          {
            DateFr = StrToDate(("01.01."+YearED->Text).c_str());
            DateTo = StrToDate(("31.03."+YearED->Text).c_str());
          }
         else if (QuarterCB->ItemIndex == 1)
          {
            DateFr = StrToDate(("01.04."+YearED->Text).c_str());
            DateTo = StrToDate(("30.06."+YearED->Text).c_str());
          }
         else if (QuarterCB->ItemIndex == 2)
          {
            DateFr = StrToDate(("01.07."+YearED->Text).c_str());
            DateTo = StrToDate(("30.09."+YearED->Text).c_str());
          }
         else if (QuarterCB->ItemIndex == 3)
          {
            DateFr = StrToDate(("01.10."+YearED->Text).c_str());
            DateTo = StrToDate(("31.12."+YearED->Text).c_str());
          }
         else return;
         pdType = 3;
       }
      catch (...)
       {
          _MSG_ERR(FMT(icsDocExDocPeriodErrorDateFormat),FMT(icsDocExCommonErrorCaption));
          return;
       }
    }
   else if (MonsRB->Checked)
    {
      try
       {
         if (MonsCB->ItemIndex < 0) return;
         if (MonsCB->ItemIndex < 9)
          {
            DateFr = StrToDate(("01.0"+IntToStr(MonsCB->ItemIndex+1)+"."+YearED->Text).c_str());
            DateTo = StrToDate((GetLastDay(MonsCB->ItemIndex+1,YearED->Text.ToInt())+".0"+IntToStr(MonsCB->ItemIndex+1)+"."+YearED->Text).c_str());
          }
         else
          {
            DateFr = StrToDate(("01."+IntToStr(MonsCB->ItemIndex+1)+"."+YearED->Text).c_str());
            DateTo = StrToDate((GetLastDay(MonsCB->ItemIndex+1,YearED->Text.ToInt())+"."+IntToStr(MonsCB->ItemIndex+1)+"."+YearED->Text).c_str());
          }
         pdType = 2;
       }
      catch (...)
       {
          _MSG_ERR(FMT(icsDocExDocPeriodErrorDateFormat),FMT(icsDocExCommonErrorCaption));
          return;
       }
    }
   else if (PeriodRB->Checked)
    {
      try
       {
         pdType = 1;
         if (DateFrDE->Text.Length()) DateFr = DateFrDE->Date;
         if (DateToDE->Text.Length()) DateTo = DateToDE->Date;
       }
      catch (...)
       {
          _MSG_ERR(FMT(icsDocExDocPeriodErrorYearFormat),FMT(icsDocExCommonErrorCaption));
          return;
       }
    }         */
   ModalResult = mrOk;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TPackPeriodForm::GetLastDay(int AMons, int AYear)
{
  if (IsValidDate((Word)AYear, (Word)AMons, (Word)1))
   {// ��� � ����� ����������
     return IntToStr(DaysInAMonth((Word)AYear, (Word)AMons));
   }
  else
   return "1";
}
//---------------------------------------------------------------------------
void __fastcall TPackPeriodForm::PerDataCBPropertiesChange(TObject *Sender)
{
  if (PerDataCB->ItemIndex > 0)
   {
     if (PerTypeCB->ItemIndex == 5)
      {
        DateFrLab->Enabled = true;
        DateFrDE->Enabled = true;
        DateToLab->Enabled = true;
        DateToDE->Enabled = true;
      }
     else
      {
         YearChB->Enabled = true;
         YearChB->Checked = false;
         YearED->Enabled = true;
         YearED->Text = "";
      }
   }
  else
   {
         YearChB->Enabled = false;
         YearED->Enabled = false;
         YearED->Text = "";
   }

/*
//  YearED->Enabled = true;
//  YearED->Style->Color = FReqColor;

  DateFrLab->Enabled = false;
  DateFrDE->Enabled = false;
  DateToLab->Enabled = false;
  DateToDE->Enabled = false;

  DateFrDE->Text = "";
  DateToDE->Text = "";

  PerDataCB->Properties->Items->Clear();
  if (DocPeriodRB->Checked)
   {
     PerDataCB->Properties->Items->Clear();
   }
  else if (YearRB->Checked)
   {
     HalfYearCB->Enabled  = false;
     QuarterCB->Enabled   = false;
     MonsCB->Enabled      = false;
     MonsCB->Visible      = true;

     HalfYearCB->ItemIndex = -1;
     QuarterCB->ItemIndex = -1;
     MonsCB->ItemIndex = -1;

     YearED->Value = YY;
   }
  else if (HalfYearRB->Checked)
   {
     QuarterCB->Visible  = false;
     MonsCB->Visible     = false;

     HalfYearCB->Visible       = true;
     HalfYearCB->Enabled       = true;
     HalfYearCB->Style->Color  = FReqColor;

     HalfYearCB->ItemIndex = (int)((MM-1)/6);
   }
  else if (QuarterRB->Checked)
   {
     HalfYearCB->Visible = false;
     MonsCB->Visible     = false;

     QuarterCB->Visible       = true;
     QuarterCB->Enabled       = true;
     QuarterCB->Style->Color  = FReqColor;
     QuarterCB->ItemIndex = (int)((MM-1)/3);
   }
  else if (MonsRB->Checked)
   {
     HalfYearCB->Visible = false;
     QuarterCB->Visible  = false;

     MonsCB->Visible       = true;
     MonsCB->Enabled       = true;
     MonsCB->Style->Color  = FReqColor;
     MonsCB->ItemIndex = MM - 1;
   }
  else if (PeriodRB->Checked)
   {
     YearED->Enabled = false;
     YearED->Style->Color = clBtnFace;

     HalfYearCB->Visible = false;
     QuarterCB->Visible  = false;
     MonsCB->Visible      = true;
     MonsCB->ItemIndex = -1;
     MonsCB->Enabled      = false;
     MonsCB->Style->Color = clBtnFace;

     DateFrLab->Enabled = true;
     DateFrDE->Enabled = true;
     DateToLab->Enabled = true;
     DateToDE->Enabled = true;
     DateToDE->Date = TDateTime::CurrentDate();
   }*/
}
//---------------------------------------------------------------------------
void __fastcall TPackPeriodForm::PerTypeCBPropertiesChange(TObject *Sender)
{
  PerDataCB->Enabled = false;
  YearED->Enabled = false;
  YearED->Text = "";
  DateFrLab->Enabled = false;
  YearChB->Enabled = false;
  YearChB->Checked = false;
  DateFrDE->Enabled = false;
  DateToLab->Enabled = false;
  DateToDE->Enabled = false;
  DateFrDE->Text = "";
  DateToDE->Text = "";

  PerDataCB->Properties->Items->Clear();
  if (PerTypeCB->ItemIndex == 0)
   {
   }
  else if (PerTypeCB->ItemIndex == 1)
   {
     YearChB->Enabled = true;
//     YearED->Enabled = true;
//     YearED->Text = "";
   }
  else if (PerTypeCB->ItemIndex == 2)
   {
     PerDataCB->Enabled = true;
     PerDataCB->Properties->Items->Add("��������� ���");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->ItemIndex = 0;
   }
  else if (PerTypeCB->ItemIndex == 3)
   {
     PerDataCB->Enabled = true;
     PerDataCB->Properties->Items->Add("������� ���");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("��������");
     PerDataCB->ItemIndex = 0;
   }
  else if (PerTypeCB->ItemIndex == 4)
   {
     PerDataCB->Enabled = true;

     PerDataCB->Properties->Items->Add("����� ���");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("�������");
     PerDataCB->Properties->Items->Add("����");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("���");
     PerDataCB->Properties->Items->Add("����");
     PerDataCB->Properties->Items->Add("����");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("��������");
     PerDataCB->Properties->Items->Add("�������");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("�������");
     PerDataCB->ItemIndex = 0;
   }
  else if (PerTypeCB->ItemIndex == 5)
   {
     PerDataCB->Enabled = true;
     PerDataCB->Properties->Items->Add("�������� ���");
     PerDataCB->Properties->Items->Add("��������� ��������");
     PerDataCB->ItemIndex = 0;
   }
}
//---------------------------------------------------------------------------
void __fastcall TPackPeriodForm::YearChBPropertiesChange(TObject *Sender)
{
  YearED->Enabled = YearChB->Checked;
  if (YearED->Enabled)
   YearED->Text = FCY;
  else
   YearED->Text = "";
}
//---------------------------------------------------------------------------
void __fastcall TPackPeriodForm::ShiftEDPropertiesValidate(TObject *Sender,
      Variant &DisplayValue, TCaption &ErrorText, bool &Error)
{
  if (Error)
   {
     UnicodeString tmpVal = VarToStr(DisplayValue).Trim();
     Error = false;
     if (!tmpVal.LastDelimiter("/"))
      { // ����� ���
        DisplayValue = Variant(tmpVal+"/0/0");
      }
     else
      {
        if (tmpVal.Pos("/") == tmpVal.LastDelimiter("/"))
         { // ����� ���+"/"+�������?
           if (tmpVal.LastDelimiter("/") == tmpVal.Length())
            { // ����� ���+"/"
              DisplayValue = Variant(tmpVal+"0/0");
            }
           else
            { // ����� ���+"/"+�������
              DisplayValue = Variant(tmpVal+"/0");
            }
         }
        else
         {
           if (tmpVal.LastDelimiter("/") == tmpVal.Length())
            { // ����� ���+"/"+�������+"/"
              DisplayValue = Variant(tmpVal+"0");
            }
           else
            { // �������, �� �� ������ ���� ���������
              Error = true;
              ErrorText = "������� ������������ �������� ��������.";
            }
         }
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TPackPeriodForm::YearEDPropertiesValidate(TObject *Sender,
      Variant &DisplayValue, TCaption &ErrorText, bool &Error)
{
  if (Error)
   {
     UnicodeString tmpVal = VarToStr(DisplayValue).Trim();
     Error = false;
     switch (tmpVal.Length())
      {
        case 0: {DisplayValue = Variant(IntToStr(FCY)); break;}
        case 1: {DisplayValue = Variant(IntToStr(FCY).SubString(1,3)+tmpVal); break;}
        case 2: {DisplayValue = Variant(IntToStr(FCY).SubString(1,2)+tmpVal); break;}
        case 3: {DisplayValue = Variant(IntToStr(FCY).SubString(1,1)+tmpVal); break;}
        default: {
                   Error = true;
                   ErrorText = "������� ������������ �������� ����.";
                 }
      }
   }
}
//---------------------------------------------------------------------------
int __fastcall TPackPeriodForm::FGetPerType()
{
  if (PerTypeCB->ItemIndex != -1)
   {
     return PerTypeCB->ItemIndex+1;
   }
  else
    return 0;
}
//---------------------------------------------------------------------------
int __fastcall TPackPeriodForm::FGetPerSubType()
{
  if ((PerDataCB->ItemIndex != -1) && (PerTypeCB->ItemIndex > 1))
   {
     return PerDataCB->ItemIndex+1;
   }
  else
    return 0;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TPackPeriodForm::FGetPerTypeStr()
{
  if (PerTypeCB->ItemIndex != -1)
   {
     return PerTypeCB->Properties->Items->Strings[PerTypeCB->ItemIndex];
   }
  else
    return "";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TPackPeriodForm::FGetPerSubTypeStr()
{
  if ((PerDataCB->ItemIndex != -1) && (PerTypeCB->ItemIndex > 1))
   {
     return PerDataCB->Properties->Items->Strings[PerDataCB->ItemIndex];
   }
  else
    return "";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TPackPeriodForm::FGetPerYear()
{
  return YearED->Text.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TPackPeriodForm::FGetPerDateFr()
{
  return DateFrDE->Text.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TPackPeriodForm::FGetPerDateTo()
{
  return DateToDE->Text.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TPackPeriodForm::FGetPerOffset()
{
  return ShiftED->Text.Trim();
}
//---------------------------------------------------------------------------
int __fastcall GetPeriodValue(int AType, int ASubType, UnicodeString AYear,
                              UnicodeString ADateFr, UnicodeString ADateTo,
                              UnicodeString AOffset,
                              int ADocPerType, TDate &ADocPerDateFr, TDate &ADocPerDateTo)
{
  int RC = -1; // �������������  - "year" 5, "halfyear 4; "quarter 3;  "month" 2;  "unique" 1;  ERR 0
  try
   {
     unsigned short FCY,FCM,FCD;
     int FQuart, FHalfYear;
     ADocPerDateTo.DecodeDate(&FCY, &FCM, &FCD);
     if (FCM < 7)
      {
        if (FCM < 4) FQuart = 1;
        else         FQuart = 2;
        FHalfYear = 1;
      }
     else
      {
        if (FCM < 10) FQuart = 3;
        else          FQuart = 4;
        FHalfYear = 2;
      }
     UnicodeString FYearStr;
     if (AYear.Length())  FYearStr = AYear;
     else                 FYearStr = IntToStr(FCY);
     try
      {
        switch (AType)
         {
           case 1:
           { // ��������� � ���
             RC = ADocPerType;
             break;
           }
           case 2:
           { // ��� // �������������  - "year" 5
             ADocPerDateFr = StrToDate("01.01."+FYearStr);
             ADocPerDateTo = StrToDate("31.12."+FYearStr);
             RC = 5;
             break;
           }
           case 3:
           { // ��������� // �������������  - "halfyear 4;
             if (ASubType > 1)
              { //��  "��������� ���"
                FHalfYear = ASubType - 1;
              }
             switch (FHalfYear)
              {
                case 1:
                { //"������");
                  ADocPerDateFr = StrToDate("01.01."+FYearStr);
                  ADocPerDateTo = StrToDate("30.06."+FYearStr);
                  break;
                }
                case 2:
                { //"������");
                  ADocPerDateFr = StrToDate("01.07."+FYearStr);
                  ADocPerDateTo = StrToDate("31.12."+FYearStr);
                  break;
                }
              }
             RC = 4;
             break;
           }
           case 4:
           { // ������� // �������������  - "quarter 3;
             if (ASubType > 1)
              { //��  "������� ���"
                FQuart = ASubType - 1;
              }
             switch (FQuart)
              {
                case 1:
                { //"������");
                  ADocPerDateFr = StrToDate("01.01."+FYearStr);
                  ADocPerDateTo = StrToDate("31.03."+FYearStr);
                  break;
                }
                case 2:
                { //"������");
                  ADocPerDateFr = StrToDate("01.04."+FYearStr);
                  ADocPerDateTo = StrToDate("30.06."+FYearStr);
                  break;
                }
                case 3:
                { //"������");
                  ADocPerDateFr = StrToDate("01.07."+FYearStr);
                  ADocPerDateTo = StrToDate("30.09."+FYearStr);
                  break;
                }
                case 4:
                { //"��������");
                  ADocPerDateFr = StrToDate("01.10."+FYearStr);
                  ADocPerDateTo = StrToDate("31.12."+FYearStr);
                  break;
                }
              }
             RC = 3;
             break;
           }
           case 5:
           { // ����� // �������������  - "month" 2;
             if (ASubType > 1)
              { //��  "����� ���"
                FCM = ASubType - 1;
              }
             UnicodeString FLastDay = "1";

             if (IsValidDate((Word)FYearStr.ToInt(), (Word)FCM, (Word)1))
              { // ��� � ����� ����������
                FLastDay = IntToStr(DaysInAMonth((Word)FYearStr.ToInt(), (Word)FCM));
              }

             if (FCM < 10)
              {
                ADocPerDateFr = StrToDate("01.0"+IntToStr(FCM)+"."+FYearStr);
                ADocPerDateTo = StrToDate(FLastDay+".0"+IntToStr(FCM)+"."+FYearStr);
              }
             else
              {
                ADocPerDateFr = StrToDate("01."+IntToStr(FCM)+"."+FYearStr);
                ADocPerDateTo = StrToDate(FLastDay+"."+IntToStr(FCM)+"."+FYearStr);
              }
             RC = 2;
             break;
           }
           case 6:
           { // �������� ��� // �������������  - "unique" 1;
             if (ASubType == 2)
              { //"��������� ��������";
                if (ADateFr.Length()) ADocPerDateFr = StrToDate(ADateFr);
                if (ADateTo.Length()) ADocPerDateTo = StrToDate(ADateTo);
              }
             RC = 1;
             break;
           }
         }
      }
     catch (...)
      {
//              _MSG_ERR(FMT(icsDocExDocPeriodErrorYearFormat),FMT(icsDocExCommonErrorCaption));
         throw;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------


