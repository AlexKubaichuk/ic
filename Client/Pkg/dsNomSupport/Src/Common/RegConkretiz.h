//---------------------------------------------------------------------------

#ifndef RegConkretizH
#define RegConkretizH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "dsRegTemplate.h"
#include "dsNomCommon.h"
//---------------------------------------------------------------------------
class TRegConForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *CompPanel;
        TPanel *BtnPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
  TdsNomCommon *FDM;
  TdsRegTemplateData *FTemplateData;
        bool FIsSubSel,IsAll;
//        TAxeXMLContainer *FXMLList;
        TTagNode *FTempXML;
        TTagNode *FRootNode;

  bool __fastcall IsTemplate(UnicodeString AId);
        void __fastcall FOnGetOrgList(bool ATemplate, long AGroup, TStringList *ADest);
        bool __fastcall FExtGetWhere(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource, bool isClicked, TObject *ACtrlList, int ABtnIdx);
        bool  __fastcall GetFl(TTagNode *itTag, UnicodeString &UIDs);
        bool __fastcall FSetValues(TTagNode *itTag, UnicodeString &TagNum);
        UnicodeString __fastcall TRegConForm::SetValIfNotNull(UnicodeString AVal, UnicodeString AId,bool AAnd, bool AQuote = true);
        TTagNode *TmplNode;
        TdsRegTemplate *FTempl;
//        TAutoFunc_DM   *FDM;
public:		// User declarations
        TStringList *ValuesList;
        TTagNode *Result;
        __fastcall TRegConForm(TdsNomCommon *ADM, UnicodeString ATemplXML, TTagNode *ParamRoot, UnicodeString ARootGUI, bool AisAll, bool isNeedDef, bool AIsSubSel = false);
};
//---------------------------------------------------------------------------
extern PACKAGE TRegConForm *RegConForm;
//---------------------------------------------------------------------------
#endif
