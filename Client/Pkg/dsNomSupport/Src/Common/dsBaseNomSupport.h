//---------------------------------------------------------------------------

#ifndef dsBaseNomSupportH
#define dsBaseNomSupportH
//---------------------------------------------------------------------------
//#include <vcl.h>
#include "ICSDATEUTIL.hpp"
#include "dsNomCommon.h"
#include "DateConkretiz.h"
#include "TextConcretiz.h"

//#include "pgmsetting.h"
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#                   ��������� ��������� �������� ���������                   #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
#define _CorrectDate(a) TDateTime(a).FormatString("dd.mm.yyyy")
//---------------------------------------------------------------------------
typedef bool __fastcall (__closure *TGetDefFunc)(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
class TdsBaseNomSupport : public TObject
{
    typedef map<const UnicodeString, UnicodeString> TStrMap;
private:
    TStrMap HashSel;
    TStrMap ClassHashSel;
//    void __fastcall FGetOrgParam(UnicodeString &ARet, UnicodeString AFlName);
//    UnicodeString __fastcall SetDay(int AYear, int AMonth, int ADay );
//    UnicodeString __fastcall _GetPart2(UnicodeString ARef, char Sep);
//    void __fastcall DecodePeriod(UnicodeString AScr, int &AYear, int &AMonth, int &ADay);
  TdsGetValByIdEvent __fastcall FGetOnGetValById();
  void __fastcall FSetOnGetValById(TdsGetValByIdEvent AValue);

  TdsGetClassXMLEvent __fastcall FGetOnGetClassXML();
  void __fastcall FSetOnGetClassXML(TdsGetClassXMLEvent AValue);

  TExtBtnClick __fastcall FGetExtBtnClick();
  void __fastcall FSetExtBtnClick(TExtBtnClick AValue);

  UnicodeString __fastcall FGetXMLPath();
  void __fastcall FSetXMLPath(UnicodeString AVal);


  UnicodeString __fastcall FGetDataPath();
  void __fastcall FSetDataPath(UnicodeString AVal);

    bool __fastcall DefChoiceValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefAgePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefDatePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefChoiceDBValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefRegVal(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefTextValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefExtEditValue(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);

  __int64 __fastcall FGetLPUCode();
  void __fastcall FSetLPUCode(__int64 AValue);

  TcxEditStyleController* __fastcall FGetStyleController();
  void __fastcall FSetStyleController(TcxEditStyleController*AValue);

protected:
    TdsNomCommon   *FDM;
    UnicodeString __fastcall FGetGUI(TTagNode *ANode);
//    UnicodeString __fastcall GetValIfNotEmpty(UnicodeString APref, UnicodeString AVal, UnicodeString APostf = "");
//    UnicodeString __fastcall AgeStr(TDateTime ACurDate, TDateTime ABirthDay);
//    UnicodeString __fastcall AgeStrY(TDateTime ACurDate, TDateTime ABirthDay);
//    UnicodeString __fastcall FIncPeriod(UnicodeString AFrDate,UnicodeString APer, int IsSub);
//    UnicodeString __fastcall AddPeriod(UnicodeString AScr1, UnicodeString AScr2);
    UnicodeString __fastcall FGetRefValue(TTagNode *ANode, UnicodeString ARef1,UnicodeString ARef2);
//    void __fastcall GetSQLValue(TTagNode *AIE, UnicodeString &ATab, UnicodeString &ARet);
//    UnicodeString __fastcall GetChildContAVDef(TTagNode *ANode, UnicodeString AVal, UnicodeString ADef);

    virtual TGetDefFunc  __fastcall GetDefMethodAddr(UnicodeString AFuncName);


public:
    __fastcall TdsBaseNomSupport(TAxeXMLContainer *AXMLList, UnicodeString ARegGUI);
    __fastcall ~TdsBaseNomSupport();

  bool __fastcall GetDVal(UnicodeString AFuncName, TTagNode* ANode, UnicodeString &AText, bool ANeedKonkr, UnicodeString &AIEXML);
  __property TdsGetValByIdEvent  OnGetValById  = {read=FGetOnGetValById,write=FSetOnGetValById};
  __property TdsGetClassXMLEvent OnGetClassXML = {read=FGetOnGetClassXML,write=FSetOnGetClassXML};
  __property TExtBtnClick        OnExtBtnClick = {read=FGetExtBtnClick, write=FSetExtBtnClick};

  __property __int64 LPUCode   = {read=FGetLPUCode,write=FSetLPUCode};
  __property TcxEditStyleController *StyleController = {read=FGetStyleController,write=FSetStyleController};
  __property UnicodeString XMLPath = {read=FGetXMLPath, write=FSetXMLPath};
  __property UnicodeString DataPath = {read=FGetDataPath, write=FSetDataPath};
};
#endif






