//---------------------------------------------------------------------------

#ifndef dsDocNomSupportH
#define dsDocNomSupportH
//---------------------------------------------------------------------------
#include "dsBaseNomSupport.h"
#include "dsDocXMLUtils.h"
//#include <vcl.h>
//#include "ICSDATEUTIL.hpp"

//#include "pgmsetting.h"
//---------------------------------------------------------------------------
//##############################################################################
//#                                                                            #
//#                   ��������� ��������� �������� ���������                   #
//#                                                                            #
//##############################################################################
//---------------------------------------------------------------------------
#define _CorrectDate(a) TDateTime(a).FormatString("dd.mm.yyyy")
//---------------------------------------------------------------------------
class TdsDocNomSupport : public TdsBaseNomSupport
{
private:



    bool __fastcall DefDocDef(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefSBOrg(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);

    bool __fastcall DefDocSpecOwner(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefDocPeriod(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefDocPeriodicity(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefDocYNVal(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);

/*    int __fastcall GetPeriodValue(int AType, int ASubType, UnicodeString AYear,
                                  UnicodeString ADateFr, UnicodeString ADateTo,
                                  UnicodeString AOffset,
                                  int ADocPerType, TDate &ADocPerDateFr, TDate &ADocPerDateTo);     *///!!!

protected:

    virtual TGetDefFunc __fastcall GetDefMethodAddr(UnicodeString AFuncName);
public:

    __fastcall TdsDocNomSupport(TAxeXMLContainer *AXMLList, UnicodeString ARegGUI);
    __fastcall ~TdsDocNomSupport();
};
#endif






