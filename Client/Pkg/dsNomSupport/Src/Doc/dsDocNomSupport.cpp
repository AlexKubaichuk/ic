//---------------------------------------------------------------------------
#pragma hdrstop
#include <System.DateUtils.hpp>
#include "dsDocNomSupport.h"
//#include "DocPeriod.h"
#include "PackPeriod.h"

//---------------------------------------------------------------------------
typedef map<const UnicodeString, UnicodeString> TStrMap;
TStrMap HashSel;
TStrMap ClassHashSel;
//---------------------------------------------------------------------------
#define flVal(a) AFields->Values[AFields->Names[a]]
//---------------------------------------------------------------------------
__fastcall TdsDocNomSupport::TdsDocNomSupport(TAxeXMLContainer *AXMLList, UnicodeString ARegGUI)
: TdsBaseNomSupport(AXMLList, ARegGUI)
{
}
//---------------------------------------------------------------------------
__fastcall TdsDocNomSupport::~TdsDocNomSupport()
{
}
//---------------------------------------------------------------------------
TGetDefFunc __fastcall TdsDocNomSupport::GetDefMethodAddr(UnicodeString AFuncName)
{
  TGetDefFunc RC = NULL;

  if (AFuncName == "DefDocDef")              RC = DefDocDef;
  else if (AFuncName == "DefSBOrg")          RC = DefSBOrg;
  else if (AFuncName == "DefDocSpecOwner")   RC = DefDocSpecOwner;
  else if (AFuncName == "DefDocPeriod")      RC = DefDocPeriod;
  else if (AFuncName == "DefDocPeriodicity") RC = DefDocPeriodicity;
  else if (AFuncName == "DefDocYNVal")       RC = DefDocYNVal;

  if (!RC)
   RC = TdsBaseNomSupport::GetDefMethodAddr(AFuncName);
  return RC;
}
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#            ��������� ��������� �������� �������������� ��������� (Def...)  #
//#                                                                            #
//#            ��������� ������������ ������ SQL-�������             (SQL...)  #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
bool __fastcall TdsDocNomSupport::DefDocDef(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
/*  //!!!
  if (!FDM) return false;
  bool RC = false;
  TStringList *FValues = NULL;
  TStringList *FValues2 = NULL;
  try
   {
      FValues = new TStringList;
      FValues2 = new TStringList;

      FDM->qtExec("Select GUI, NAME From SPECIFIKATORS Order by NAME");
      int idx = 1;
      while (!FDM->Query->Eof)
       {
         FValues->AddObject(FDM->Query->FieldByName("name")->AsString,(TObject*)idx);
         FValues2->AddObject(FDM->Query->FieldByName("GUI")->AsString.UpperCase(),(TObject*)idx);
         idx++;
         FDM->Query->Next();
       }
      RC = FDM->DefChVal(AIE, AText, AXMLIE, FValues, FValues2);
   }
  __finally
   {
     if (FValues) delete FValues;
     if (FValues2) delete FValues2;
   }
  return RC;
*/
}
//---------------------------------------------------------------------------
bool __fastcall TdsDocNomSupport::DefSBOrg(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
  if (!FDM) return false;
  bool RC = false;
  TStringList *FValues = NULL;
  try
   {
      FValues = new TStringList;

      FValues->AddObject("�����������",(TObject*)0);
      FValues->AddObject("�����������",(TObject*)1);
      FValues->AddObject("����",(TObject*)2);

      RC = FDM->DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
   }
  __finally
   {
     if (FValues) delete FValues;
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsDocNomSupport::DefDocSpecOwner(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
/*  //!!!
  if (!FDM) return false;
  bool RC = false;
  TStringList *FValues = NULL;
  TStringList *FValues2 = NULL;
  try
   {
      FValues = new TStringList;
      FValues2 = new TStringList;
      FDM->qtExec("Select code, Name From SUBORDORG Order by NAME");
      int idx = 1;
      while (!FDM->Query->Eof)
       {
         FValues->AddObject(FDM->Query->FieldByName("name")->AsString,(TObject*)idx);
         FValues2->AddObject(FDM->Query->FieldByName("code")->AsString.UpperCase(),(TObject*)idx);
         idx++;
         FDM->Query->Next();
       }
      RC = FDM->DefChVal(AIE, AText, AXMLIE, FValues, FValues2);
   }
  __finally
   {
     if (FValues) delete FValues;
     if (FValues2) delete FValues2;
   }
  return RC;
  */

}
//---------------------------------------------------------------------------
bool __fastcall TdsDocNomSupport::DefDocPeriod(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
/*
   // AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
   // ����� �� ���, �� �� �� �� ����� �����
   // ��� ���� "����"
   // {
   //   1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
   //                            1,2 ��� �������; 0 - �������� ���
   //                                             1 - ������ �� ������� ����
   //                                             2 - ������ �� ����
   //   2-� - ���� ��;   ��� ���� "�������� ���"
   //                    ��� ����� "������ �� ..." �������� �������
   //   3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
   //                    ��� ���� "������ �� ������� ����"  - �� �������������;
   //   4-� - �������� - ��� ���� "�������� ���"  - �� �������������
   //         �������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
   if (!FDM) return false;
   bool iRC = false;
   UnicodeString tmp = "";
   TTagNode *__tmp;
   TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
   if (AIE->GetFirstChild())
    {
      if (AIE->Count)
       {
         tmp = FISCondRules->AV["name"]+" = ";
         __tmp = AIE->GetFirstChild();
         int DType = AIE->GetFirstChild()->AV["value"].ToIntDef(0);
         bool FLast = (bool)(DType&1);
         DType = DType >> 1;
         if (FLast)
          {
            TTagNode *FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
            if (!FISAttr->AV["add_ref"].Length())
             tmp = "������ ��������� ������������";
            else
             tmp = FISAttr->AV["add_ref"];
          }
         TTagNode *DateType   = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam1 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam2 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam3 = __tmp; __tmp = __tmp->GetNext();
         TTagNode *DateParam4 = __tmp;
         if (DType == 0)
          {// �������� ���
            if ((DateParam1 && (DateParam1->AV["value"].Length())) ||
                (DateParam2 && (DateParam2->AV["value"].Length())))
             {
               tmp += " "+DateType->AV["name"];
               if (DateParam1->AV["value"].Length())
                {
                  tmp += " "+DateParam1->AV["name"];
                  tmp += "='"+DateParam1->AV["value"]+"'";
                }
               if (DateParam2->AV["value"].Length())
                {
                  tmp += " "+DateParam2->AV["name"];
                  tmp += "='"+DateParam2->AV["value"]+"'";
                }
             }
          }
         else if (DType >= 3)
          { // 3 - ������ ������������ ���������
            // 4 - ������ ���� - ���� ��������� ������� ������������ ���������
            // 5 - ������ ��������� - ���� ��������� ������� ������������ ���������
            // 6 - ������ �������� - ���� ��������� ������� ������������ ���������
            // 7 - ������ ������ - ���� ��������� ������� ������������ ���������

            tmp += " "+DateType->AV["name"];
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         else
          {// ������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
            tmp += " "+DateType->AV["name"];
            if (DateParam1->AV["value"].Length())
             {
               tmp += " "+DateParam1->AV["name"];
               tmp += "='"+DateParam1->AV["value"]+"'";
             }
            if (DateParam2->AV["value"].Length())
             {
               tmp += " "+DateParam2->AV["name"];
               tmp += "='"+DateParam2->AV["value"]+"'";
             }
            if (DateParam3->AV["value"].Length())
             {
               tmp += " "+DateParam3->AV["name"];
               tmp += "='"+DateParam3->AV["value"]+"'";
             }
          }
         if (DateParam4)
          {
            tmp += " "+DateParam4->AV["name"];
          }
         AText = tmp.Trim();
       }
      iRC = true;
    }
   AXMLIE = AIE->AsXML;
   return iRC;
*/
  if (!FDM) return false;
  bool RC = false;
  TTagNode *FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);

  if (NeedKonkr)
   {
     TPackPeriodForm *Dlg = NULL;
     try
      {
        UnicodeString FPeriodDate = "";
        TDate FPeriodDateFr = 0;
        TDate FPeriodDateTo = 0;

//        short int FPerType = 0;
        Dlg = new TPackPeriodForm(Application->Owner);
        Dlg->ShowModal();
        if (Dlg->ModalResult == mrOk)
         {
           if (AIE->Count) AIE->DeleteChild();

           AIE->AddChild("text","name=type,value="+IntToStr(Dlg->Type)+",ref=0");
           AIE->AddChild("text","name=subtype,value="+IntToStr(Dlg->SubType)+",ref=1");
           AIE->AddChild("text","name=typestr ,value="+Dlg->TypeStr+",ref=2");
           AIE->AddChild("text","name=subtypestr ,value="+Dlg->SubTypeStr+",ref=3");
           AIE->AddChild("text","name=���,value="+Dlg->Year+",ref=4");
           AIE->AddChild("text","name=��,value="+Dlg->DateFr+",ref=5");
           AIE->AddChild("text","name=��,value="+Dlg->DateTo+",ref=6");
           AIE->AddChild("text","name=��������,value="+Dlg->Offset+",ref=7");
         }
      }
     __finally
      {
        if (Dlg) delete Dlg;
      }
   }
  if (AIE->GetFirstChild())
   {
     if (AIE->Count)
      {
        UnicodeString tmp = FISCondRules->AV["name"]+" = ";
        TTagNode *__tmp = AIE->GetFirstChild();
//        int FPType = __tmp->AV["value"].ToIntDef(0);
        __tmp = __tmp->GetNext();
        __tmp = __tmp->GetNext();

        tmp += __tmp->AV["value"];
        __tmp = __tmp->GetNext();
        if (__tmp->AV["value"].Length())
         tmp += ", "+__tmp->AV["value"];
        __tmp = __tmp->GetNext();
        while (__tmp)
         {
           if (__tmp->AV["value"].Length())
            tmp += ", "+__tmp->AV["name"]+": "+__tmp->AV["value"];
           __tmp = __tmp->GetNext();
         }

        AText = tmp.Trim();
      }
     RC = true;
   }
  AXMLIE = AIE->AsXML;
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsDocNomSupport::DefDocPeriodicity(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
  if (!FDM) return false;
  bool RC = false;
  TStringList *FValues = NULL;
  try
   {
     FValues = new TStringList;
     FValues->AddObject("�����������",(TObject*)0);
     FValues->AddObject("����������",(TObject*)1);
     FValues->AddObject("�����",(TObject*)2);
     FValues->AddObject("�������",(TObject*)3);
     FValues->AddObject("���������",(TObject*)4);
     FValues->AddObject("���",(TObject*)5);

     RC = FDM->DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
   }
  __finally
   {
     if (FValues) delete FValues;
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsDocNomSupport::DefDocYNVal(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE)
{
  if (!FDM) return false;
  bool RC = false;
  TStringList *FValues = NULL;
  try
   {
     FValues = new TStringList;
     FValues->AddObject("���",(TObject*)0);
     FValues->AddObject("��",(TObject*)1);

     RC = FDM->DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
   }
  __finally
   {
     if (FValues) delete FValues;
   }
  return RC;
}
//---------------------------------------------------------------------------
/*
int __fastcall TdsDocNomSupport::GetPeriodValue(int AType, int ASubType, UnicodeString AYear,
                              UnicodeString ADateFr, UnicodeString ADateTo,
                              UnicodeString AOffset,
                              int ADocPerType, TDate &ADocPerDateFr, TDate &ADocPerDateTo)
{
  int RC = -1; // �������������  - "year" 5, "halfyear 4; "quarter 3;  "month" 2;  "unique" 1;  ERR 0
  try
   {
     unsigned short FCY,FCM,FCD;
     int FQuart, FHalfYear;
     ADocPerDateTo.DecodeDate(&FCY, &FCM, &FCD);
     if (FCM < 7)
      {
        if (FCM < 4) FQuart = 1;
        else         FQuart = 2;
        FHalfYear = 1;
      }
     else
      {
        if (FCM < 10) FQuart = 3;
        else          FQuart = 4;
        FHalfYear = 2;
      }
     UnicodeString FYearStr;
     if (AYear.Length())  FYearStr = AYear;
     else                 FYearStr = IntToStr(FCY);
     try
      {
        switch (AType)
         {
           case 1:
           { // ��������� � ���
             RC = ADocPerType;
             break;
           }
           case 2:
           { // ��� // �������������  - "year" 5
             ADocPerDateFr = StrToDate("01.01."+FYearStr);
             ADocPerDateTo = StrToDate("31.12."+FYearStr);
             RC = 5;
             break;
           }
           case 3:
           { // ��������� // �������������  - "halfyear 4;
             if (ASubType > 1)
              { //��  "��������� ���"
                FHalfYear = ASubType - 1;
              }
             switch (FHalfYear)
              {
                case 1:
                { //"������");
                  ADocPerDateFr = StrToDate("01.01."+FYearStr);
                  ADocPerDateTo = StrToDate("30.06."+FYearStr);
                  break;
                }
                case 2:
                { //"������");
                  ADocPerDateFr = StrToDate("01.07."+FYearStr);
                  ADocPerDateTo = StrToDate("31.12."+FYearStr);
                  break;
                }
              }
             RC = 4;
             break;
           }
           case 4:
           { // ������� // �������������  - "quarter 3;
             if (ASubType > 1)
              { //��  "������� ���"
                FQuart = ASubType - 1;
              }
             switch (FQuart)
              {
                case 1:
                { //"������");
                  ADocPerDateFr = StrToDate("01.01."+FYearStr);
                  ADocPerDateTo = StrToDate("31.03."+FYearStr);
                  break;
                }
                case 2:
                { //"������");
                  ADocPerDateFr = StrToDate("01.04."+FYearStr);
                  ADocPerDateTo = StrToDate("30.06."+FYearStr);
                  break;
                }
                case 3:
                { //"������");
                  ADocPerDateFr = StrToDate("01.07."+FYearStr);
                  ADocPerDateTo = StrToDate("30.09."+FYearStr);
                  break;
                }
                case 4:
                { //"��������");
                  ADocPerDateFr = StrToDate("01.10."+FYearStr);
                  ADocPerDateTo = StrToDate("31.12."+FYearStr);
                  break;
                }
              }
             RC = 3;
             break;
           }
           case 5:
           { // ����� // �������������  - "month" 2;
             if (ASubType > 1)
              { //��  "����� ���"
                FCM = ASubType - 1;
              }
             UnicodeString FLastDay = "1";

             if (IsValidDate((Word)FYearStr.ToInt(), (Word)FCM, (Word)1))
              { // ��� � ����� ����������
                FLastDay = IntToStr(DaysInAMonth((Word)FYearStr.ToInt(), (Word)FCM));
              }

             if (FCM < 10)
              {
                ADocPerDateFr = StrToDate("01.0"+IntToStr(FCM)+"."+FYearStr);
                ADocPerDateTo = StrToDate(FLastDay+".0"+IntToStr(FCM)+"."+FYearStr);
              }
             else
              {
                ADocPerDateFr = StrToDate("01."+IntToStr(FCM)+"."+FYearStr);
                ADocPerDateTo = StrToDate(FLastDay+"."+IntToStr(FCM)+"."+FYearStr);
              }
             RC = 2;
             break;
           }
           case 6:
           { // �������� ��� // �������������  - "unique" 1;
             if (ASubType == 2)
              { //"��������� ��������";
                if (ADateFr.Length()) ADocPerDateFr = StrToDate(ADateFr);
                if (ADateTo.Length()) ADocPerDateTo = StrToDate(ADateTo);
              }
             RC = 1;
             break;
           }
         }
      }
     catch (Exception &E)
      {
        throw;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
*/
