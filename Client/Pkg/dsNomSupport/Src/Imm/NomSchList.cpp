//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "NomSchList.h"
#include "dsRegEDTemplateDataProvider.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxInplaceContainer"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma resource "*.dfm"
TNomSchListForm * NomSchListForm;
//---------------------------------------------------------------------------
__fastcall TNomSchListForm::TNomSchListForm(TComponent * Owner, TdsNomCommon * ADM, TTagNode * AIE, bool isVac)
    : TForm(Owner)
 {
  FDM    = ADM;
  Result = AIE;

  TTagNode * SchList;
  if (isVac)
   SchList = FDM->XMLList->GetXML("12063611-00008cd7-cd89");
  else
   SchList = FDM->XMLList->GetXML("001D3500-00005882-DC28");

  TTagNode *sText;
  UnicodeString FValue = "";
  sText = Result->GetChildByAV("text","ref","0");
  if (sText)
   FValue = sText->AV["value"].Trim().UpperCase();
  SchemaTL->Clear();

  TStringList * VacList = new TStringList;
  TStringList * ProbList = new TStringList;

  FillClassCB(FDM->OnGetValById, "l.0035", VacList, -1);
  FillClassCB(FDM->OnGetValById, "l.002A", ProbList, -1);
  try
   {

    SchList = SchList->GetChildByName("schemalist")->GetFirstChild();
    int ind = 0;

    TcxTreeListNode * SchNode;
    while (SchList)
     {
      try
       {
        SchNode = SchemaTL->Root->AddChild();
        if (isVac)
         SchNode->Texts[0] = VacList->Strings[VacList->IndexOfObject((TObject *)SchList->AV["ref"].ToInt())];
        else
         SchNode->Texts[0] = ProbList->Strings[ProbList->IndexOfObject((TObject *)SchList->AV["ref"].ToInt())];

        SchNode->Texts[1] = SchList->AV["name"];
        SchNode->Texts[2] = SchList->AV["uid"] + "." + SchList->GetFirstChild()->AV["uid"];
        if (FValue ==SchNode->Texts[2].Trim().UpperCase())
         SchNode->Focused = true;
       }
      catch (Exception & E)
       {
        ShowMessage("������ ����������� ������������ ����: " + SchList->AV["ref"]);
       }
      SchList = SchList->GetNext();
     }
   }
  __finally
   {
    delete VacList;
    delete ProbList;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TNomSchListForm::OkBtnClick(TObject * Sender)
 {
  if (SchemaTL->SelectionCount)
   {
   Result->DeleteChild();
    SchemaTL->Selections[0]->Texts[0] + "=" + SchemaTL->Selections[0]->Texts[2];
   TTagNode *__tmp;
   __tmp = Result->AddChild("text");
   __tmp->AddAttr("name",SchemaTL->Selections[0]->Texts[0]+".{ "+SchemaTL->Selections[0]->Texts[1]+" } ");
   __tmp->AddAttr("value",SchemaTL->Selections[0]->Texts[2]);
   __tmp->AddAttr("ref","0");

    ModalResult = mrOk;
   }
 }
//---------------------------------------------------------------------------
