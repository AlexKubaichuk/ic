//---------------------------------------------------------------------------
#ifndef NomSchListH
#define NomSchListH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxInplaceContainer.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "dsNomCommon.h"
//---------------------------------------------------------------------------
class TNomSchListForm : public TForm
 {
 __published: //IDE-managed Components
 TPanel *BtnPanel;
  TcxButton *         OkBtn;
  TcxButton *         CancelBtn;
  TcxTreeList *       SchemaTL;
  TcxTreeListColumn * SchMIBPCol;
  TcxTreeListColumn * SchNameCol;
  TcxTreeListColumn * SchValCol;
  void __fastcall OkBtnClick(TObject * Sender);
 private: //User declarations
  TdsNomCommon * FDM;
 public: //User declarations
  TTagNode * Result;
  __fastcall TNomSchListForm(TComponent * Owner, TdsNomCommon * ADM, TTagNode * AIE, bool isVac);
 };
//---------------------------------------------------------------------------
extern PACKAGE TNomSchListForm * NomSchListForm;
//---------------------------------------------------------------------------
#endif
