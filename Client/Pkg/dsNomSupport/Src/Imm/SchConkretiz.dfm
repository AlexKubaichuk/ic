object SchForm: TSchForm
  Left = 647
  Top = 338
  BorderStyle = bsToolWindow
  Caption = #1042#1099#1073#1086#1088' '#1089#1093#1077#1084' '#1087#1083#1072#1085#1080#1088#1086#1074#1072#1085#1080#1103
  ClientHeight = 150
  ClientWidth = 414
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 110
    Width = 414
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      414
      40)
    object OkBtn: TcxButton
      Left = 249
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      OnClick = OkBtnClick
    end
    object CancelBtn: TcxButton
      Left = 330
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object CompPanel: TPanel
    Left = 0
    Top = 0
    Width = 414
    Height = 110
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object InfGB: TcxGroupBox
      AlignWithMargins = True
      Left = 3
      Top = 61
      Align = alTop
      Caption = #1057#1093#1077#1084#1072
      StyleDisabled.TextColor = clBtnShadow
      TabOrder = 0
      Height = 47
      Width = 408
      object VakCBE: TcxButtonEdit
        AlignWithMargins = True
        Left = 5
        Top = 21
        Align = alClient
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Style.Color = clInfoBk
        TabOrder = 0
        Width = 398
      end
    end
    object cxGroupBox1: TcxGroupBox
      AlignWithMargins = True
      Left = 3
      Top = 3
      Align = alTop
      Caption = #1048#1085#1092#1077#1082#1094#1080#1103
      StyleDisabled.TextColor = clBtnShadow
      TabOrder = 1
      Height = 52
      Width = 408
      object InfCB: TcxComboBox
        AlignWithMargins = True
        Left = 5
        Top = 21
        Align = alTop
        Properties.DropDownListStyle = lsEditFixedList
        TabOrder = 0
        Width = 398
      end
    end
  end
end
