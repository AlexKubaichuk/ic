object PlanItemForm: TPlanItemForm
  Left = 490
  Top = 307
  HelpContext = 4012
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = #1054#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1103' ...'
  ClientHeight = 242
  ClientWidth = 371
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 202
    Width = 371
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 395
    ExplicitWidth = 374
    DesignSize = (
      371
      40)
    object OkBtn: TcxButton
      Left = 204
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      OnClick = OkBtnClick
      ExplicitLeft = 207
    end
    object CancelBtn: TcxButton
      Left = 285
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      ExplicitLeft = 288
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 371
    Height = 202
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 1
    ExplicitTop = -1
    ExplicitWidth = 374
    ExplicitHeight = 395
    object CheckGB: TcxGroupBox
      Left = 1
      Top = 146
      Align = alTop
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072': '
      StyleDisabled.TextColor = clBtnShadow
      TabOrder = 0
      ExplicitLeft = 2
      ExplicitTop = 217
      ExplicitWidth = 372
      Height = 51
      Width = 369
      object CheckCB: TcxComboBox
        AlignWithMargins = True
        Left = 5
        Top = 21
        Align = alTop
        Properties.DropDownListStyle = lsEditFixedList
        TabOrder = 0
        ExplicitWidth = 362
        Width = 359
      end
    end
    object TypeRG: TcxRadioGroup
      Left = 1
      Top = 1
      Align = alTop
      Caption = #1040#1085#1072#1083#1080#1079#1080#1088#1086#1074#1072#1090#1100
      Properties.Columns = 3
      Properties.Items = <
        item
          Caption = #1055#1088#1080#1074#1080#1074#1082#1091
          Value = '1'
        end
        item
          Caption = #1055#1088#1086#1073#1091
          Value = '2'
        end
        item
          Caption = #1055#1088#1086#1074#1077#1088#1082#1091
          Value = '3'
        end>
      Properties.OnChange = TypeRGPropertiesChange
      ItemIndex = 0
      TabOrder = 1
      ExplicitWidth = 372
      Height = 47
      Width = 369
    end
    object PrivGB: TcxGroupBox
      Left = 1
      Top = 48
      Align = alTop
      Caption = #1052#1048#1041#1055':'
      StyleDisabled.TextColor = clBtnShadow
      TabOrder = 2
      ExplicitLeft = -1
      ExplicitTop = 42
      ExplicitWidth = 372
      Height = 47
      Width = 369
      object VacCB: TcxComboBox
        AlignWithMargins = True
        Left = 5
        Top = 21
        Align = alTop
        Properties.DropDownListStyle = lsEditFixedList
        TabOrder = 0
        ExplicitTop = 8
        ExplicitWidth = 362
        Width = 359
      end
    end
    object TestGB: TcxGroupBox
      Left = 1
      Top = 95
      Align = alTop
      Caption = #1055#1088#1086#1073#1072': '
      StyleDisabled.TextColor = clBtnShadow
      TabOrder = 3
      ExplicitLeft = 2
      ExplicitTop = 128
      ExplicitWidth = 372
      Height = 51
      Width = 369
      object TestCB: TcxComboBox
        AlignWithMargins = True
        Left = 5
        Top = 21
        Align = alTop
        Properties.DropDownListStyle = lsEditFixedList
        TabOrder = 0
        ExplicitTop = 8
        ExplicitWidth = 362
        Width = 359
      end
    end
  end
end
