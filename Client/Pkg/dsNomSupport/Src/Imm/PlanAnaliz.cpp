//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "PlanAnaliz.h"
#include "dsRegEDTemplateDataProvider.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxGroupBox"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxRadioGroup"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
#define __CLCODE(a)  (__int64)(a->Properties->Items->Objects[a->ItemIndex])
#define __CLSTR(a)   a->Properties->Items->Strings[a->ItemIndex]
//TPlanAnalizForm *PlanAnalizForm;
//---------------------------------------------------------------------------
__fastcall TPlanAnalizForm::TPlanAnalizForm(TComponent *Owner, TdsNomCommon *ADM, TTagNode *ParamRoot, bool isReg, bool isNeedDef)
        : TForm(Owner)
{
//  IsReg = isReg;
  FDM = ADM;
  FXMLList = FDM->XMLList;
  Result = ParamRoot;
  RegNode = FXMLList->GetXML("40381E23-92155860-4448");
  FillClassCB(FDM->OnGetValById, "l.003A", (TStringList*)InfCB->Properties->Items, -1);
  FillClassCB(FDM->OnGetValById, "l.0035", (TStringList*)VacCB->Properties->Items, -1);
  FillClassCB(FDM->OnGetValById, "l.002A", (TStringList*)TestCB->Properties->Items, -1);
  FillClassCB(FDM->OnGetValById, "l.013E", (TStringList*)CheckCB->Properties->Items, -1);
//!!!  FInvadeClassCB(InfCB->Properties->Items,  0, true ,RegNode->GetTagByUID("003A"), true, (TDataSet*)FDM->quFreeA, "", "", RegNode);
//!!!  FInvadeClassCB(VacCB->Properties->Items,  0, true ,RegNode->GetTagByUID("0035"), true, (TDataSet*)FDM->quFreeA, "", "", RegNode);
//!!!  FInvadeClassCB(ProbCB->Properties->Items, 0, true ,RegNode->GetTagByUID("002A"), true, (TDataSet*)FDM->quFreeA, "", "", RegNode);
  TypeRGPropertiesChange(TypeRG);

  TTagNode *sText;

  sText = Result->GetChildByAV("text","ref","type");   // �������������
  if (sText)
   TypeRG->ItemIndex = sText->AV["value"].ToIntDef(1) - 1;
  else
   {
     sText = Result->GetChildByAV("text","ref","2");
     if (sText)
      {
        if (sText->AV["value"].ToIntDef(0)) TypeRG->ItemIndex = 0;
        else                                TypeRG->ItemIndex = 1;
      }
   }

  sText = Result->GetChildByAV("text","ref","inf");   // ��������
  if (sText)
   SetIdx(sText, InfCB);
  else
   {
     sText = Result->GetChildByAV("text","ref","1");
     if (sText)
      SetIdx(sText, InfCB);
   }

  sText = Result->GetChildByAV("text","ref","mibp");  // ����
  if (sText)
   SetIdx(sText, VacCB);
  else
   {
     sText = Result->GetChildByAV("text","ref","3");
     if (sText)
      SetIdx(sText, VacCB);
   }
  sText = Result->GetChildByAV("text","ref","vtype");  // ��� ����������
  if (sText)
   VacTypeED->Text = sText->AV["value"];
  else
   {
     sText = Result->GetChildByAV("text","ref","4");  // V
     if (sText)
      VacTypeED->Text = sText->AV["value"];
     else
      {
        sText = Result->GetChildByAV("text","ref","5"); // RV
        if (sText)
          VacTypeED->Text = sText->AV["value"];
        else
         {
           sText = Result->GetChildByAV("text","ref","6"); // ���.
           if (sText)
             VacTypeED->Text = "���.";
         }
      }
   }
  sText = Result->GetChildByAV("text","ref","test");  // ��� �����
  if (sText)
   SetIdx(sText, TestCB);
  else
   {
     sText = Result->GetChildByAV("text","ref","7");
     if (sText)
      SetIdx(sText, TestCB);
   }

  sText = Result->GetChildByAV("text","ref","check");  // ��� ��������
  if (sText)
   SetIdx(sText, CheckCB);

  sText = Result->GetChildByAV("text","ref","notexec");  // ��������� ����������
  if (sText)
    NotExecCBChB->Checked = sText->AV["value"].ToIntDef(0);
  else
   {
     sText = Result->GetChildByAV("text","ref","10");
     if (sText) NotExecCBChB->Checked = sText->AV["value"].ToIntDef(0);
   }

  sText = Result->GetChildByAV("text","ref","exec");  // ����������
  if (sText)
    ExecCB->ItemIndex = sText->AV["value"].ToIntDef(0);
  else
   {
     sText = Result->GetChildByAV("text","ref","8");
     if (sText) ExecCB->ItemIndex = sText->AV["value"].ToIntDef(0);
   }

  sText = Result->GetChildByAV("text","ref","notsrc");  // ��������� ���������
  if (sText)
    NotPlanSrcCBChB->Checked = sText->AV["value"].ToIntDef(0);
  else
   {
     sText = Result->GetChildByAV("text","ref","11");
     if (sText) NotPlanSrcCBChB->Checked = sText->AV["value"].ToIntDef(0);
   }

  sText = Result->GetChildByAV("text","ref","src");  // ��������
  if (sText)
    PlanSrcCB->ItemIndex = sText->AV["value"].ToIntDef(0);
  else
   {
     sText = Result->GetChildByAV("text","ref","9");
     if (sText) PlanSrcCB->ItemIndex = sText->AV["value"].ToIntDef(0);
   }
}
//---------------------------------------------------------------------------
void __fastcall TPlanAnalizForm::SetIdx(TTagNode *ANode, TcxComboBox *ACB)
{
  int FIdx = ACB->Properties->Items->IndexOfObject((TObject*)ANode->AV["value"].ToIntDef(0));
  if (FIdx != -1)
   ACB->ItemIndex = FIdx;
}
//---------------------------------------------------------------------------
bool __fastcall TPlanAnalizForm::CheckInput()
{
  bool RC = false;
  try
   {
     if (!TypeRG->ItemIndex != -1)
      {
        switch (TypeRG->ItemIndex)
         {
           case 0:
           {
             VacTypeED->Text = VacTypeED->Text.Trim();
             if ((InfCB->ItemIndex == -1) && (VacCB->ItemIndex  == -1) && !VacTypeED->Text.Length())
              {
                _MSG_ERR("���������� ������� ��������� ������������� ��������.","������");
                ActiveControl = TypeRG;
              }
             else
              RC = true;
             break;
           }
           case 1:
           {
             if ((InfCB->ItemIndex == -1) && (TestCB->ItemIndex  == -1))
              {
                _MSG_ERR("���������� ������� ��������� ������������� �����.","������");
                ActiveControl = TypeRG;
              }
             else
              RC = true;
             break;
           }
           case 2:
           {
             if ((CheckCB->ItemIndex == -1))
              {
                _MSG_ERR("���������� ������� ������������� ��������.","������");
                ActiveControl = TypeRG;
              }
             else
              RC = true;
             break;
           }
         }
      }
     else
      {
        _MSG_ERR("���������� ������� ��� �������������� ��������","������");
        ActiveControl = TypeRG;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TPlanAnalizForm::OkBtnClick(TObject *Sender)
{
  if (CheckInput())
   {
     TTagNode *__tmp;
     UnicodeString FSQL = "";
     Result->DeleteChild();
     __tmp = Result->AddChild("text");
     __tmp->AV["ref"] = "type";
     __tmp->AV["value"] = IntToStr(TypeRG->ItemIndex +1);
     __tmp->AV["name"] = TypeRG->Caption +": "+ TypeRG->Properties->Items->Items[TypeRG->ItemIndex]->Caption;
     if (!TypeRG->ItemIndex || (TypeRG->ItemIndex == 1))
      {
        if (InfCB->ItemIndex != -1)
         {
           __tmp = Result->AddChild("text");
           __tmp->AV["ref"] = "inf";
           __tmp->AV["value"] = IntToStr(__CLCODE(InfCB));
           __tmp->AV["name"] = "��������="+__CLSTR(InfCB);
         }
        if (VacCB->ItemIndex != -1)
         {
           __tmp = Result->AddChild("text");
           __tmp->AV["ref"] = "mibp";
           __tmp->AV["value"] = IntToStr(__CLCODE(VacCB));
           __tmp->AV["name"] = "����="+__CLSTR(VacCB);
         }
        if (VacTypeED->Text.Length())
         {
           __tmp = Result->AddChild("text");
           __tmp->AV["ref"] = "vtype";
           __tmp->AV["value"] = VacTypeED->Text;
           __tmp->AV["name"] = "���="+VacTypeED->Text;
         }
        if (TestCB->ItemIndex != -1)
         {
           __tmp = Result->AddChild("text");
           __tmp->AV["ref"] = "test";
           __tmp->AV["value"] = IntToStr(__CLCODE(TestCB));
           __tmp->AV["name"] = "�����="+__CLSTR(TestCB);
         }
      }
     else if (TypeRG->ItemIndex == 2)
      {
        if (CheckCB->ItemIndex != -1)
         {
           __tmp = Result->AddChild("text");
           __tmp->AV["ref"] = "check";
           __tmp->AV["value"] = IntToStr(__CLCODE(CheckCB));
           __tmp->AV["name"] = "��������="+__CLSTR(CheckCB);
         }
      }
     if (ExecCB->ItemIndex >= 0)
      {
        if (NotExecCBChB->Checked)
         {
           __tmp = Result->AddChild("text");
           __tmp->AV["name"] = "";
           __tmp->AV["value"] = "1";
           __tmp->AV["ref"] = "notexec";
         }

        __tmp = Result->AddChild("text");
        if (NotExecCBChB->Checked) __tmp->AV["name"] = "���� ����������=�� "+__CLSTR(ExecCB);
        else                       __tmp->AV["name"] = "���� ����������="+__CLSTR(ExecCB);
        __tmp->AV["value"] = IntToStr(ExecCB->ItemIndex);
        __tmp->AV["ref"] = "exec";
      }
     if (PlanSrcCB->ItemIndex >= 0)
      {
        if (NotPlanSrcCBChB->Checked)
         {
           __tmp = Result->AddChild("text");
           __tmp->AV["name"] = "";
           __tmp->AV["value"] = "1";
           __tmp->AV["ref"] = "notsrc";
         }

        __tmp = Result->AddChild("text");
        if (NotPlanSrcCBChB->Checked) __tmp->AV["name"] = "�������� �����=�� "+__CLSTR(PlanSrcCB);
        else                          __tmp->AV["name"] = "�������� �����="+__CLSTR(PlanSrcCB);
        __tmp->AV["value"] = IntToStr(PlanSrcCB->ItemIndex);
        __tmp->AV["ref"] = "src";
      }
     ModalResult = mrOk;
   }
}
//---------------------------------------------------------------------------
void __fastcall TPlanAnalizForm::TypeRGPropertiesChange(TObject *Sender)
{
  InfGB->Enabled = false;
  PrivGB->Enabled = false;
  TestGB->Enabled = false;
  CheckGB->Enabled = false;
  switch (TypeRG->ItemIndex)
   {
     case 0:
     {
       InfGB->Enabled = true;
       PrivGB->Enabled = true;
       break;
     }
     case 1:
     {
       InfGB->Enabled = true;
       TestGB->Enabled = true;
       break;
     }
     case 2:
     {
       CheckGB->Enabled = true;
       break;
     }
   }
}
//---------------------------------------------------------------------------
/*
void __fastcall TRegPPConForm::OkBtnClick(TObject *Sender)
{
  if (CheckInput())
   {
     TTagNode *__tmp;
     UnicodeString FSQL = "";
     Result->DeleteChild();
     if (PrivRB->Checked)
      { //������ ��������
         __tmp = Result->AddChild("text");
         __tmp->AddAttr("name","������ ��������");
         __tmp->AddAttr("value",IntToStr((int)PrivRB->Checked));
         __tmp->AddAttr("ref","2");
        if (VacCB->ItemIndex > 0)
         {
            __tmp = Result->AddChild("text");
            __tmp->AddAttr("name","����="+__CLSTR(VacCB));
            __tmp->AddAttr("value",__CLCODE(VacCB));
            __tmp->AddAttr("ref","3");
            FSQL += " and PLN.R1086=0 and PLN.R1089="+IntToStr((int)__CLCODE(VacCB));
         }
        else
         {
            if (InfCB->ItemIndex > 0)
             {
               __tmp = Result->AddChild("text");
               __tmp->AddAttr("name","���������� ��="+__CLSTR(InfCB));
               __tmp->AddAttr("value",__CLCODE(InfCB));
               __tmp->AddAttr("ref","1");
   //            FSQL += " and PLN.R1086=0 and PLN.VACTYPE LIKE '%#"+IntToStr(__CLCODE(InfCB))+".%'";
               FSQL += " and PLN.R1086=0 and PLN.R1089 in (Select CL.R003B From CLASS_002D CL Where CL.R003E="+IntToStr((int)__CLCODE(InfCB))+") ";
             }
            else
             FSQL += " and PLN.R1086=0";
         }
        switch (VacTypeRG->ItemIndex)
         {
           case 1:
            {
               __tmp = Result->AddChild("text");
               __tmp->AddAttr("name","��� ����������=V"+VacType1ED->Text);
               __tmp->AddAttr("value","V"+VacType1ED->Text);
               __tmp->AddAttr("ref","4");
              FSQL += " and PLN.VACTYPE LIKE '%.V"+VacType1ED->Text+"#%'";
              break;
            }
           case 2:
            {
               __tmp = Result->AddChild("text");
               __tmp->AddAttr("name","��� ����������=RV"+VacType1ED->Text+"/"+VacType2ED->Text);
               __tmp->AddAttr("value","RV"+VacType1ED->Text+"/"+VacType2ED->Text);
               __tmp->AddAttr("ref","5");
              if ((VacType1ED->Text == "")&&(VacType2ED->Text == ""))
               FSQL += " and PLN.VACTYPE LIKE '%.RV%#%'";
              else if ((VacType1ED->Text != "")&&(VacType2ED->Text != ""))
               FSQL += " and PLN.VACTYPE LIKE '%.RV"+VacType1ED->Text+"/"+VacType2ED->Text+"#%'";
              else if (VacType1ED->Text != "")
               FSQL += " and PLN.VACTYPE LIKE '%.RV"+VacType1ED->Text+"/%#%'";
              else
               FSQL += " and PLN.VACTYPE LIKE '%.RV%/"+VacType2ED->Text+"#%'";
              break;
            }
           case 3:
            {
               __tmp = Result->AddChild("text");
               __tmp->AddAttr("name","��� ����������=���.");
               __tmp->AddAttr("value","D");
               __tmp->AddAttr("ref","6");
               FSQL += " and PLN.VACTYPE LIKE '%.D#%'";
              break;
            }
         }
      }
     else
      { //������ �����
         __tmp = Result->AddChild("text");
         __tmp->AddAttr("name","������ �����");
         __tmp->AddAttr("value",IntToStr((int)PrivRB->Checked));
         __tmp->AddAttr("ref","2");
        if (ProbCB->ItemIndex > 0)
         {
            __tmp = Result->AddChild("text");
            __tmp->AddAttr("name","�����="+__CLSTR(ProbCB));
            __tmp->AddAttr("value",__CLCODE(ProbCB));
            __tmp->AddAttr("ref","7");
            FSQL += " and PLN.R1086=1 and PLN.R108A="+IntToStr((int)__CLCODE(ProbCB));
         }
        else
         {
            if (InfCB->ItemIndex > 0)
             {
               __tmp = Result->AddChild("text");
               __tmp->AddAttr("name","���������� ��="+__CLSTR(InfCB));
               __tmp->AddAttr("value",__CLCODE(InfCB));
               __tmp->AddAttr("ref","1");
               FSQL += " and PLN.R1086=1 and PLN.VACTYPE LIKE '%#"+IntToStr((int)__CLCODE(InfCB))+".%'";
             }
            else
             FSQL += " and PLN.R1086=1";
         }
      }
     if (ApplyCB->ItemIndex > 0)
      {
         switch (ApplyCB->ItemIndex)
          {
            case 1:
             { //����������
               if (NotApplyCBChB->Checked)
               FSQL += " and PLN.F_RESULT = 0";
               else
               FSQL += " and PLN.F_RESULT <> 0";
               break;
             }
            case 2:
             { //���.������������
               if (NotApplyCBChB->Checked)
               FSQL += " and PLN.F_RESULT not in (3,4)";
               else
               FSQL += " and PLN.F_RESULT in (3,4)";
               break;
             }
            case 3:
             { //���.�� �����
               if (NotApplyCBChB->Checked)
               FSQL += " and PLN.F_RESULT <> 3";
               else
               FSQL += " and PLN.F_RESULT = 3";
               break;
             }
            case 4:
             { //���. �� �� �����
               if (NotApplyCBChB->Checked)
               FSQL += " and PLN.F_RESULT <> 4";
               else
               FSQL += " and PLN.F_RESULT = 4";
               break;
             }
            case 5:
             { //���.� ����� ���.
               if (NotApplyCBChB->Checked)
               FSQL += " and PLN.F_RESULT <> 2";
               else
               FSQL += " and PLN.F_RESULT = 2";
               break;
             }
            case 6:
             { //���.�� ��.����.
               if (NotApplyCBChB->Checked)
               FSQL += " and PLN.F_RESULT <> 1";
               else
               FSQL += " and PLN.F_RESULT = 1";
               break;
             }
          }
         __tmp = Result->AddChild("text");
         if (NotApplyCBChB->Checked)
           __tmp->AddAttr("name","���� ����������=�� "+__CLSTR(ApplyCB));
         else
           __tmp->AddAttr("name","���� ����������="+__CLSTR(ApplyCB));
         __tmp->AddAttr("value",IntToStr(ApplyCB->ItemIndex));
         __tmp->AddAttr("ref","8");
      }
     if (PlanTypeCB->ItemIndex > 0)
      {
         switch (PlanTypeCB->ItemIndex)
          {
            case 1:
             { //��������
               if (NotPlanTypeCBChB->Checked)
               FSQL += " and PLN.F_PLAN = 0";
               else
               FSQL += " and PLN.F_PLAN <> 0";
               break;
             }
            case 2:
             { //������ ����������
               if (NotPlanTypeCBChB->Checked)
               FSQL += " and PLN.F_PLAN <> 1";
               else
               FSQL += " and PLN.F_PLAN = 1";
               break;
             }
            case 3:
             { //����������� �� ���������
               if (NotPlanTypeCBChB->Checked)
               FSQL += " and PLN.F_PLAN <> 2";
               else
               FSQL += " and PLN.F_PLAN = 2";
               break;
             }
            case 4:
             { //����������� �� �� ���������
               if (NotPlanTypeCBChB->Checked)
               FSQL += " and PLN.F_PLAN <> 3";
               else
               FSQL += " and PLN.F_PLAN = 3";
               break;
             }
            case 5:
             { //����������� ��� ���������������
               if (NotPlanTypeCBChB->Checked)
               FSQL += " and PLN.F_PLAN <> 4";
               else
               FSQL += " and PLN.F_PLAN = 4";
               break;
             }
            case 6:
             { //����������� �� ����
               if (NotPlanTypeCBChB->Checked)
               FSQL += " and PLN.F_PLAN <> 5";
               else
               FSQL += " and PLN.F_PLAN = 5";
               break;
             }
            case 7:
             { //����������� �� ���� ��� ���������������
               if (NotPlanTypeCBChB->Checked)
               FSQL += " and PLN.F_PLAN <> 6";
               else
               FSQL += " and PLN.F_PLAN = 6";
               break;
             }
          }
         __tmp = Result->AddChild("text");
         if (NotPlanTypeCBChB->Checked)
           __tmp->AddAttr("name","�������� �����=�� "+__CLSTR(PlanTypeCB));
         else
           __tmp->AddAttr("name","�������� �����="+__CLSTR(PlanTypeCB));
         __tmp->AddAttr("value",IntToStr(PlanTypeCB->ItemIndex));
         __tmp->AddAttr("ref","9");
      }
     if (NotApplyCBChB->Checked)
      {
        __tmp = Result->AddChild("text");
        __tmp->AddAttr("name","");
        __tmp->AddAttr("value","1");
        __tmp->AddAttr("ref","10");
      }
     if (NotPlanTypeCBChB->Checked)
      {
        __tmp = Result->AddChild("text");
        __tmp->AddAttr("name","");
        __tmp->AddAttr("value","1");
        __tmp->AddAttr("ref","11");
      }

     if (FSQL != "")
      {
         __tmp = Result->AddChild("text");
         __tmp->AddAttr("name","sql");
         __tmp->AddAttr("value",FSQL.SubString(6,FSQL.Length()-5));
      }
     ModalResult = mrOk;
   }
}
//---------------------------------------------------------------------------
*/
/*
  FCtrlOwner->FillClass("d."+_UID_+":"+FDepVals, (TStringList*)ED->Properties->Items, -1);
  FCtrlOwner->FillClass("l."+FNode->AV["ref"], (TStringList*)ED->Properties->Items);
*/
