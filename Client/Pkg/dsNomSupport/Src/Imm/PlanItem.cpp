//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "PlanItem.h"
#include "dsRegEDTemplateDataProvider.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxGroupBox"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxRadioGroup"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
#define __CLCODE(a)  (__int64)(a->Properties->Items->Objects[a->ItemIndex])
#define __CLSTR(a)   a->Properties->Items->Strings[a->ItemIndex]
//---------------------------------------------------------------------------
__fastcall TPlanItemForm::TPlanItemForm(TComponent * Owner, TdsNomCommon * ADM, TTagNode * ParamRoot, bool isReg, bool isNeedDef)
    : TForm(Owner)
 {
  //IsReg = isReg;
  FDM      = ADM;
  FXMLList = FDM->XMLList;
  Result   = ParamRoot;
  RegNode  = FXMLList->GetXML("40381E23-92155860-4448");
  FillClassCB(FDM->OnGetValById, "l.0035", (TStringList *)VacCB->Properties->Items, -1);
  FillClassCB(FDM->OnGetValById, "l.002A", (TStringList *)TestCB->Properties->Items, -1);
  FillClassCB(FDM->OnGetValById, "l.013E", (TStringList *)CheckCB->Properties->Items, -1);
  TypeRGPropertiesChange(TypeRG);

  TTagNode * sText;

  sText = Result->GetChildByAV("text", "ref", "type"); //�������������
  if (sText)
   TypeRG->ItemIndex = sText->AV["value"].ToIntDef(1) - 1;

  sText = Result->GetChildByAV("text", "ref", "objref"); //����
  if (sText)
   {
    switch (TypeRG->ItemIndex)
     {
     case 0:
      SetIdx(sText, VacCB);
      break;
     case 1:
      SetIdx(sText, TestCB);
      break;
     case 2:
      SetIdx(sText, CheckCB);
      break;
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanItemForm::SetIdx(TTagNode * ANode, TcxComboBox * ACB)
 {
  int FIdx = ACB->Properties->Items->IndexOfObject((TObject *)ANode->AV["value"].ToIntDef(0));
  if (FIdx != -1)
   ACB->ItemIndex = FIdx;
 }
//---------------------------------------------------------------------------
bool __fastcall TPlanItemForm::CheckInput()
 {
  bool RC = false;
  try
   {
    if (!TypeRG->ItemIndex != -1)
     {
      switch (TypeRG->ItemIndex)
       {
       case 0:
         {
          if (VacCB->ItemIndex == -1)
           {
            _MSG_ERR("���������� ������� ����.", "������");
            ActiveControl = VacCB;
           }
          else
           RC = true;
          break;
         }
       case 1:
         {
          if (TestCB->ItemIndex == -1)
           {
            _MSG_ERR("���������� ������� �����.", "������");
            ActiveControl = TestCB;
           }
          else
           RC = true;
          break;
         }
       case 2:
         {
          if (CheckCB->ItemIndex == -1)
           {
            _MSG_ERR("���������� ������� ��������.", "������");
            ActiveControl = CheckCB;
           }
          else
           RC = true;
          break;
         }
       }
     }
    else
     {
      _MSG_ERR("���������� ������� ��� �������������� ��������", "������");
      ActiveControl = TypeRG;
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TPlanItemForm::OkBtnClick(TObject * Sender)
 {
  if (CheckInput())
   {
    TTagNode * __tmp;
    UnicodeString FSQL = "";
    Result->DeleteChild();
    __tmp              = Result->AddChild("text");
    __tmp->AV["ref"]   = "type";
    __tmp->AV["value"] = IntToStr(TypeRG->ItemIndex + 1);
    __tmp->AV["name"]  = TypeRG->Caption + ": " + TypeRG->Properties->Items->Items[TypeRG->ItemIndex]->Caption;

    __tmp            = Result->AddChild("text");
    __tmp->AV["ref"] = "objref";
    switch (TypeRG->ItemIndex)
     {
     case 0:
      __tmp->AV["value"] = IntToStr(__CLCODE(VacCB));
      __tmp->AV["name"] = "����=" + __CLSTR(VacCB);
      break;
     case 1:
      __tmp->AV["value"] = IntToStr(__CLCODE(TestCB));
      __tmp->AV["name"] = "�����=" + __CLSTR(TestCB);
      break;
     case 2:
      __tmp->AV["value"] = IntToStr(__CLCODE(CheckCB));
      __tmp->AV["name"] = "��������=" + __CLSTR(CheckCB);
      break;
     }
    ModalResult = mrOk;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanItemForm::TypeRGPropertiesChange(TObject * Sender)
 {
  PrivGB->Enabled  = false;
  TestGB->Enabled  = false;
  CheckGB->Enabled = false;
  switch (TypeRG->ItemIndex)
   {
   case 0:
     {
      PrivGB->Enabled = true;
      break;
     }
   case 1:
     {
      TestGB->Enabled = true;
      break;
     }
   case 2:
     {
      CheckGB->Enabled = true;
      break;
     }
   }
 }
//---------------------------------------------------------------------------
