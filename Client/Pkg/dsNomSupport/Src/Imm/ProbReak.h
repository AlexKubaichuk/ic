//---------------------------------------------------------------------------

#ifndef ProbReakH
#define ProbReakH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
//---------------------------------------------------------------------------
//#include "Dmf_Auto.h"
#include "dsRegTemplate.h"
#include "dsNomCommon.h"
//---------------------------------------------------------------------------
class TProbReakForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *CompPanel;
        TComboBox *ReakTypeCB;
        TLabel *ReakTypeLab;
        TEdit *FromED;
        TEdit *ToED;
        TLabel *FromLab;
        TLabel *ToLab;
        TCheckBox *AnalizChB;
        TBevel *Bevel1;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TLabel *ToLab2;
        TEdit *ToED2;
        void __fastcall AnalizChBClick(TObject *Sender);
        void __fastcall ReakTypeCBChange(TObject *Sender);
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
 void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
private:	// User declarations
        TAxeXMLContainer *FXMLList;
        TdsRegTemplateData *FTemplateData;
        bool __fastcall CheckInput();
        TTagNode* TmplNode;
        TdsRegTemplate *FTempl;
        TdsNomCommon *FDM;
//        TAutoFunc_DM *FDM;
public:		// User declarations
        TTagNode *Result;
        __fastcall TProbReakForm(TComponent* Owner, TdsNomCommon *ADM, TTagNode *AIE, bool isNeedDef, UnicodeString AProbCode);
};
//---------------------------------------------------------------------------
extern PACKAGE TProbReakForm *ProbReakForm;
//---------------------------------------------------------------------------
#endif
