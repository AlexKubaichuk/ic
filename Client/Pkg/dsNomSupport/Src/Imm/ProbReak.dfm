object ProbReakForm: TProbReakForm
  Left = 455
  Top = 230
  BorderStyle = bsToolWindow
  Caption = #1056#1077#1072#1082#1094#1080#1080' '#1085#1072' '#1087#1088#1086#1073#1091
  ClientHeight = 334
  ClientWidth = 308
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 294
    Width = 308
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      308
      40)
    object OkBtn: TcxButton
      Left = 139
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      OnClick = OkBtnClick
    end
    object CancelBtn: TcxButton
      Left = 222
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object CompPanel: TPanel
    Left = 0
    Top = 0
    Width = 308
    Height = 294
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object ReakTypeLab: TLabel
      Left = 9
      Top = 172
      Width = 110
      Height = 13
      Caption = #1061#1072#1088#1072#1082#1090#1077#1088' '#1080#1079#1084#1077#1085#1077#1085#1080#1081':'
      Enabled = False
    end
    object FromLab: TLabel
      Left = 9
      Top = 197
      Width = 165
      Height = 13
      Caption = #1060#1080#1082#1089#1080#1088#1091#1077#1084#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077' '#1087#1072#1087#1091#1083#1099':'
      Enabled = False
    end
    object ToLab: TLabel
      Left = 9
      Top = 241
      Width = 118
      Height = 13
      Caption = #1053#1072#1088#1072#1089#1090#1072#1085#1080#1077' '#1086#1090' '#1087#1072#1087#1091#1083#1099':'
      Enabled = False
    end
    object Bevel1: TBevel
      Left = -15
      Top = 146
      Width = 344
      Height = 3
      Shape = bsTopLine
    end
    object ToLab2: TLabel
      Left = 9
      Top = 219
      Width = 210
      Height = 13
      Caption = #1060#1080#1082#1089'. '#1079#1085#1072#1095'. '#1087#1088#1080' '#1087#1088#1077#1076#1099#1076#1091#1097#1077#1081' '#1075#1080#1087#1077#1088#1077#1084#1080#1080':'
      Enabled = False
    end
    object ReakTypeCB: TComboBox
      Left = 135
      Top = 167
      Width = 166
      Height = 21
      Style = csDropDownList
      Color = clBtnFace
      Enabled = False
      ItemIndex = 0
      TabOrder = 0
      Text = #1053#1072#1088#1072#1089#1090#1072#1085#1080#1077
      OnChange = ReakTypeCBChange
      Items.Strings = (
        #1053#1072#1088#1072#1089#1090#1072#1085#1080#1077
        #1042#1080#1088#1072#1078)
    end
    object FromED: TEdit
      Left = 228
      Top = 193
      Width = 73
      Height = 21
      Color = clBtnFace
      Enabled = False
      TabOrder = 1
    end
    object ToED: TEdit
      Left = 228
      Top = 237
      Width = 73
      Height = 21
      Color = clBtnFace
      Enabled = False
      TabOrder = 2
    end
    object AnalizChB: TCheckBox
      Left = 9
      Top = 148
      Width = 283
      Height = 17
      Caption = #1040#1085#1072#1083#1080#1079#1080#1088#1086#1074#1072#1090#1100' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
      TabOrder = 3
      OnClick = AnalizChBClick
    end
    object ToED2: TEdit
      Left = 228
      Top = 215
      Width = 73
      Height = 21
      Color = clBtnFace
      Enabled = False
      TabOrder = 4
    end
  end
end
