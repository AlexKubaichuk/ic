#include "dsICNomSupport.h"
#include <System.DateUtils.hpp>
#include "ProbReak.h"
#include "PlanAnaliz.h"
#include "PlanItem.h"
//#include "SchConkretiz.h"
#include "NomSchList.h"
//#include <Registry.hpp>

//#include "RegPPConkretiz.h"
//#include "SchConkretiz.h"
//#include "ProbReak.h"

#define flVal(a) AFields->Values[AFields->Names[a]]

//#define quFNStr(a) FDM->Query->FieldByName(a)->AsString
//#define quFNInt(a) FDM->Query->FieldByName(a)->AsInteger
//#define quFNFloat(a) FDM->Query->FieldByName(a)->AsFloat
//#define quFNDate(a) FDM->Query->FieldByName(a)->AsDateTime
//---------------------------------------------------------------------------
__fastcall TdsICNomSupport::TdsICNomSupport(TAxeXMLContainer * AXMLList, UnicodeString  ARegGUI)
    : TdsBaseNomSupport(AXMLList, ARegGUI)
 {
  PrepareData();
 }
//---------------------------------------------------------------------------
__fastcall TdsICNomSupport::~TdsICNomSupport()
 {
  /*
   VacList.clear();
   VacListCode.clear();
   ProbList.clear();
   ProbListCode.clear();
   InfList.clear();
   InfListCode.clear();
   */
 }
//---------------------------------------------------------------------------
void __fastcall TdsICNomSupport::PrepareData()
 {
  try
   {
   }
  catch (Exception & E)
   {
   }
 }
//---------------------------------------------------------------------------
TGetDefFunc __fastcall TdsICNomSupport::GetDefMethodAddr(UnicodeString  AFuncName)
 {
  TGetDefFunc RC = NULL;

  if (AFuncName == "DefDatePeriodLast")
   RC = DefDatePeriodLast;
  else if (AFuncName == "DefOtvodDatePeriodLast")
   RC = DefOtvodDatePeriodLast;
  else if (AFuncName == "DefPlanPrivReak")
   RC = DefPlanPrivReak;
  else if (AFuncName == "DefPlanProbReak")
   RC = DefPlanProbReak;
  else if (AFuncName == "DefPlanPres")
   RC = DefPlanPres;
  else if (AFuncName == "DefPlanItem")
   RC = DefPlanItem;
  else if (AFuncName == "DefPlanItemExec")
   RC = DefPlanItemExec;
  else if (AFuncName == "DefOrgPres")
   RC = DefOrgPres;
  else if (AFuncName == "DefExtEditValue")
   RC = DefExtEditValue;
  else if (AFuncName == "DefVacSch")
   RC = DefVacSch;
  else if (AFuncName == "DefTestSch")
   RC = DefTestSch;

  if (!RC)
   RC = TdsBaseNomSupport::GetDefMethodAddr(AFuncName);
  return RC;
 }
//---------------------------------------------------------------------------
/*
 int __fastcall TdsICNomSupport::CorrectDay( Word Year, Word Month, Word Day )
 {
 Word DaysCount = DaysInAMonth(Year,Month);
 if ( Day > DaysCount )
 Day = DaysCount;
 return Day;
 }
 */
//---------------------------------------------------------------------------
/*
 bool __fastcall TdsICNomSupport::CalcPlanPeriod(TDate *ADateBP, TDate *ADateEP, TDate *APlanMonth)
 {  //����������� ������� ������������ � ������ ������������
 // return: (������� ����� < ���� ������ ������������)

 TDate CurDate;
 Word DayNP,CurYear, CurMonth, CurDay, DayBP, MonthBP, YearBP, DayEP, MonthEP, YearEP;

 if ((int)(*APlanMonth)) CurDate = (*APlanMonth);
 else                    CurDate = Date();

 DecodeDate( CurDate, CurYear, CurMonth, CurDay );

 //TRegIniFile *IMMIni = new TRegIniFile ("IMM");
 //IMMIni->RootKey = HKEY_LOCAL_MACHINE;
 //IMMIni->OpenKey(FDM->RegKey,false);
 DayNP  = 25; //IMMIni->ReadInteger("LPUParam","DAY_PLAN",25);
 //IMMIni->CloseKey();
 //delete IMMIni;

 if ( CurDay < DayNP)
 {
 MonthEP = CurMonth;
 YearEP  = CurYear;

 MonthBP = Dateutils::MonthOf( Sysutils::IncMonth(CurDate,-1));
 YearBP  = ( MonthEP == 1 ) ? YearEP - 1 : YearEP;
 }
 else
 {
 MonthBP   = CurMonth;
 YearBP    = CurYear;

 MonthEP = Dateutils::MonthOf( Sysutils::IncMonth(CurDate));
 YearEP  = ( MonthBP == 12 ) ? CurYear + 1 : CurYear;
 }
 DayBP = CorrectDay(YearBP, MonthBP, DayNP);
 DayEP = CorrectDay(YearEP, MonthEP, 99);

 (*ADateBP) = EncodeDate(YearBP, MonthBP, DayBP);
 (*ADateEP) = EncodeDate(YearEP, MonthEP, DayEP);
 if ((*ADateEP) > CurDate) (*ADateEP) = CurDate;
 (*APlanMonth) = EncodeDate(YearEP, MonthEP, 1);
 return (Date().FormatString("dd").ToIntDef(1) < DayNP);
 }
 */
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#            ��������� ��������� �������� �������������� ��������� (Def...)  #
//#                                                                            #
//#            ��������� ������������ ������ SQL-�������             (SQL...)  #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
bool __fastcall TdsICNomSupport::DefDatePeriodLast(TTagNode * AIE, UnicodeString & AText, bool NeedKonkr, UnicodeString & AXMLIE)
 {
  //AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
  //����� �� ���, �� �� �� �� ����� �����
  //��� ���� "����"
  //{
  //1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
  //1,2 ��� �������; 0 - �������� ���
  //1 - ������ �� ������� ����
  //2 - ������ �� ����
  //2-� - ���� ��;   ��� ���� "�������� ���"
  //��� ����� "������ �� ..." �������� �������
  //3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
  //��� ���� "������ �� ������� ����"  - �� �������������;
  //4-� - �������� - ��� ���� "�������� ���"  - �� �������������
  //�������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
  if (!FDM)
   return false;
  bool iRC = false;
  UnicodeString tmp = "";
  TTagNode * __tmp;
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  if (NeedKonkr)
   {
    TDateConForm * Dlg = NULL;
    try
     {
      Dlg = new TDateConForm(Application->Owner);
      int FSubType;
      UnicodeString FSubTypeCapt = "";
      TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      if (FISCondRules->GetParent("IS") == FISCondRules->GetParent("domain")->GetFirstChild())//���� ��������
           FSubType = 0;
      else //���� ������������
       {
        if (!FISAttr->AV["add_ref"].Length())
         FSubTypeCapt = "������ ��������� ������������";
        else
         FSubTypeCapt = FISAttr->AV["add_ref"];
        FSubType = 1;
       }
      bool DoNotDop = FISAttr->CmpAV("ref", "40381E23-92155860-4448.1024");
      Dlg->SetType(_scDate, AIE, FSubType, FSubTypeCapt, DoNotDop);
      Dlg->MsgLab->Caption = FISCondRules->AV["name"];
      Dlg->ShowModal();
      iRC = (Dlg->ModalResult == mrOk);
     }
    __finally
     {
      if (Dlg)
       delete Dlg;
     }
   }
  if (AIE->GetFirstChild())
   {
    if (AIE->Count)
     {
      tmp   = "";
      __tmp = AIE->GetFirstChild();
      int DType = AIE->GetFirstChild()->GetAVDef("value", "0").ToInt();
      bool FLast = (bool)(DType & 1);
      DType = DType >> 1;
      if (FLast)
       {
        TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
        if (!FISAttr->AV["add_ref"].Length())
         tmp = "������ ��������� ������������";
        else
         tmp = FISAttr->AV["add_ref"];
       }
      TTagNode * DateType = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam1 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam2 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam3 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam4 = __tmp;
      if (DType == 0)
       {//�������� ���
        if ((DateParam1 && (DateParam1->AV["value"].Length())) ||
            (DateParam2 && (DateParam2->AV["value"].Length())))
         {
          tmp += " " + DateType->AV["name"];
          if (DateParam1->AV["value"].Length())
           {
            tmp += " " + DateParam1->AV["name"];
            tmp += "='" + DateParam1->AV["value"] + "'";
           }
          if (DateParam2->AV["value"].Length())
           {
            tmp += " " + DateParam2->AV["name"];
            tmp += "='" + DateParam2->AV["value"] + "'";
           }
         }
       }
      else if (DType == 3)
       {//������ ������������ ���������
        tmp += " " + DateType->AV["name"];
        if (DateParam3->AV["value"].Length())
         {
          tmp += " " + DateParam3->AV["name"];
          tmp += "='" + DateParam3->AV["value"] + "'";
         }
       }
      else
       {//������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
        tmp += " " + DateType->AV["name"];
        if (DateParam1->AV["value"].Length())
         {
          tmp += " " + DateParam1->AV["name"];
          tmp += "='" + DateParam1->AV["value"] + "'";
         }
        if (DateParam2->AV["value"].Length())
         {
          tmp += " " + DateParam2->AV["name"];
          tmp += "='" + DateParam2->AV["value"] + "'";
         }
        if (DateParam3->AV["value"].Length())
         {
          tmp += " " + DateParam3->AV["name"];
          tmp += "='" + DateParam3->AV["value"] + "'";
         }
       }
      if (DateParam4)
       {
        tmp += " " + DateParam4->AV["name"];
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsICNomSupport::DefOtvodDatePeriodLast(TTagNode * AIE, UnicodeString & AText, bool NeedKonkr, UnicodeString & AXMLIE)
 {
  //AIE - ������, ������ �� IE - ����� ����� 3-� ��� 4-� �����������
  //����� �� ���, �� �� �� �� ����� �����
  //��� ���� "����"
  //{
  //1-� -> ���: �������� 0-6 0-� ��� - ������ ��������� (��������,�����); �������� - 0 - ���, 1 - ��
  //1,2 ��� �������; 0 - �������� ���
  //1 - ������ �� ������� ����
  //2 - ������ �� ����
  //2-� - ���� ��;   ��� ���� "�������� ���"
  //��� ����� "������ �� ..." �������� �������
  //3-� - ���� �� -  ��� ����� "�������� ���" � "������ �� ������� ����"
  //��� ���� "������ �� ������� ����"  - �� �������������;
  //4-� - �������� - ��� ���� "�������� ���"  - �� �������������
  //�������    ��� ����� "������ �� ������� ����" � "������ �� ������� ����" - �������� �������;
  if (!FDM)
   return false;
  bool iRC = false;
  UnicodeString tmp = "";
  TTagNode * __tmp;
  TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
  if (NeedKonkr)
   {
    TDateConForm * Dlg = NULL;
    try
     {
      Dlg = new TDateConForm(Application->Owner);
      int FSubType;
      UnicodeString FSubTypeCapt = "";
      TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
      if (FISCondRules->GetParent("IS") == FISCondRules->GetParent("domain")->GetFirstChild())//���� ��������
           FSubType = 0;
      else //���� ������������
       {
        if (!FISAttr->AV["add_ref"].Length())
         FSubTypeCapt = "������ ��������� ������������";
        else
         FSubTypeCapt = FISAttr->AV["add_ref"];
        FSubType = 1;
       }
      bool DoNotDop = FISAttr->CmpAV("ref", "40381E23-92155860-4448.1024");
      Dlg->SetType(_scDate, AIE, FSubType, FSubTypeCapt, DoNotDop);
      Dlg->MsgLab->Caption = FISCondRules->AV["name"];
      Dlg->ShowModal();
      iRC = (Dlg->ModalResult == mrOk);
     }
    __finally
     {
      if (Dlg)
       delete Dlg;
     }
   }
  if (AIE->GetFirstChild())
   {
    if (AIE->Count)
     {
      tmp   = "";
      __tmp = AIE->GetFirstChild();
      int DType = AIE->GetFirstChild()->GetAVDef("value", "0").ToInt();
      bool FLast = (bool)(DType & 1);
      DType = DType >> 1;
      if (FLast)
       {
        TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
        if (!FISAttr->AV["add_ref"].Length())
         tmp = "������ ��������� ������������";
        else
         tmp = FISAttr->AV["add_ref"];
       }
      TTagNode * DateType = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam1 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam2 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam3 = __tmp;
      __tmp = __tmp->GetNext();
      TTagNode * DateParam4 = __tmp;
      if (DType == 0)
       {//�������� ���
        if ((DateParam1 && (DateParam1->AV["value"].Length())) ||
            (DateParam2 && (DateParam2->AV["value"].Length())))
         {
          tmp += " " + DateType->AV["name"];
          if (DateParam1->AV["value"].Length())
           {
            tmp += " " + DateParam1->AV["name"];
            tmp += "='" + DateParam1->AV["value"] + "'";
           }
          if (DateParam2->AV["value"].Length())
           {
            tmp += " " + DateParam2->AV["name"];
            tmp += "='" + DateParam2->AV["value"] + "'";
           }
         }
       }
      else if (DType == 3)
       {//������ ������������ ���������
        tmp += " " + DateType->AV["name"];
        if (DateParam3->AV["value"].Length())
         {
          tmp += " " + DateParam3->AV["name"];
          tmp += "='" + DateParam3->AV["value"] + "'";
         }
       }
      else
       {//������ �� ���� ��������� ������� ������������ ��������� // ������ �� ������� ����
        tmp += " " + DateType->AV["name"];
        if (DateParam1->AV["value"].Length())
         {
          tmp += " " + DateParam1->AV["name"];
          tmp += "='" + DateParam1->AV["value"] + "'";
         }
        if (DateParam2->AV["value"].Length())
         {
          tmp += " " + DateParam2->AV["name"];
          tmp += "='" + DateParam2->AV["value"] + "'";
         }
        if (DateParam3->AV["value"].Length())
         {
          tmp += " " + DateParam3->AV["name"];
          tmp += "='" + DateParam3->AV["value"] + "'";
         }
       }
      if (DateParam4)
       {
        tmp += " " + DateParam4->AV["name"];
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsICNomSupport::DefPlanPrivReak(TTagNode * AIE, UnicodeString & AText, bool NeedKonkr, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  UnicodeString tmp = "";
  TTagNode * __tmp;
  if (NeedKonkr)
   {
    TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
    TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
    __tmp = AIE->GetParent();
    __tmp = __tmp->GetChildByAV("IERef", "ref", "0E291426-00005882-2493." + FGetRefValue(FISAttr, "0070", "0086"));
    tmp   = "";
    if (__tmp)
     {
      __tmp = __tmp->GetChildByAV("text", "ref", "2");
      if (__tmp)
       {//���������� �������� ������� !!!
        tmp = __tmp->AV["value"];
       }
     }
    if (!tmp.Length())
     {
      ShowMessage("�� ����� ����!\n ������ ������ ������� ����������, \n�.�. ������ ������� ������� �� ���� ����");
      return false;
     }
    TTagNode * FRoot = FISCondRules->GetChildByName("root");
    if (FRoot)
     {
      TTagNode * FMIBPRef = FRoot->GetChildByAV("", "ref", "1020");
      if (FMIBPRef)
       {
        FMIBPRef->AV["def"] = tmp;
        FMIBPRef->AV["en"]  = "0";
       }
      tmp = FRoot->AsXML;
     }
   }
  return FDM->DefRegValue(AIE, AText, NeedKonkr, AXMLIE, tmp, "", false, false);
 }
//---------------------------------------------------------------------------
bool __fastcall TdsICNomSupport::DefPlanProbReak(TTagNode * AIE, UnicodeString & AText, bool NeedKonkr, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  UnicodeString tmp;
  TTagNode * __tmp;
  bool iRC = false;
  if (NeedKonkr)
   {
    TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
    TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
    __tmp = AIE->GetParent();
    __tmp = __tmp->GetChildByAV("IERef", "ref", "0E291426-00005882-2493." + FGetRefValue(FISAttr, "0044", "00B0"));
    tmp   = "";
    if (__tmp)
     {
      __tmp = __tmp->GetChildByAV("text", "ref", "0");
      if (__tmp)
       {//���������� �������� ����� !!!
        tmp = __tmp->AV["value"];
       }
     }
    if (!tmp.Length())
     {
      ShowMessage("�� ������ �����!\n ������ ������ ������� ����������, \n�.�. ������ ������� ������� �� ���� �����");
      return false;
     }
    TProbReakForm * Dlg = NULL;
    try
     {
      Dlg = new TProbReakForm(Application->Owner, FDM, AIE, NeedKonkr, tmp);
      Dlg->ShowModal();
      iRC = (Dlg->ModalResult == mrOk);
     }
    __finally
     {
      if (Dlg)
       delete Dlg;
     }
   }
  if (AIE->GetFirstChild())
   {
    if (AIE->Count > 1)
     {
      __tmp = AIE->GetFirstChild();
      tmp   = "";
      while (__tmp)
       {
        if (!__tmp->CmpAV("name", "sql"))
         tmp += " " + __tmp->AV["name"];
        __tmp = __tmp->GetNext();
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsICNomSupport::DefPlanPres(TTagNode * AIE, UnicodeString & AText, bool NeedKonkr, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  bool iRC = false;
  UnicodeString tmp;
  if (NeedKonkr)
   {
    TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
    TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
    TTagNode * FExtAttr = FDM->XMLList->GetRefer(FISAttr->AV["ref"]);
    TPlanAnalizForm * Dlg = NULL;
    try
     {
      Dlg = new TPlanAnalizForm(Application->Owner, FDM, AIE, FExtAttr->GetRoot()->CmpName("registr"), NeedKonkr);
      Dlg->ShowModal();
      iRC = (Dlg->ModalResult == mrOk);
     }
    __finally
     {
      if (Dlg)
       delete Dlg;
     }
   }
  if (AIE->GetFirstChild())
   {
    if (AIE->Count > 1)
     {
      TTagNode * __tmp = AIE->GetFirstChild();
      tmp = "";
      while (__tmp)
       {
        if (!__tmp->CmpAV("name", "sql"))
         tmp += " " + __tmp->AV["name"];
        __tmp = __tmp->GetNext();
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsICNomSupport::DefPlanItem(TTagNode * AIE, UnicodeString & AText, bool NeedKonkr, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  bool iRC = false;
  UnicodeString tmp;
  if (NeedKonkr)
   {
    TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
    TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
    TTagNode * FExtAttr = FDM->XMLList->GetRefer(FISAttr->AV["ref"]);
    TPlanItemForm * Dlg = NULL;
    try
     {
      Dlg = new TPlanItemForm(Application->Owner, FDM, AIE, FExtAttr->GetRoot()->CmpName("registr"), NeedKonkr);
      Dlg->ShowModal();
      iRC = (Dlg->ModalResult == mrOk);
     }
    __finally
     {
      if (Dlg)
       delete Dlg;
     }
   }
  if (AIE->GetFirstChild())
   {
    if (AIE->Count > 1)
     {
      TTagNode * __tmp = AIE->GetFirstChild();
      tmp = "";
      while (__tmp)
       {
        if (!__tmp->CmpAV("name", "sql"))
         tmp += " " + __tmp->AV["name"];
        __tmp = __tmp->GetNext();
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsICNomSupport::DefPlanItemExec(TTagNode * AIE, UnicodeString & AText, bool NeedKonkr, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;
  bool iRC = false;
  UnicodeString tmp;
  if (NeedKonkr)
   {
    TTagNode * FISCondRules = FDM->XMLList->GetRefer(AIE->AV["ref"]);
    TTagNode * FISAttr = FISCondRules->GetTagByUID(FISCondRules->AV["param"]);
    TTagNode * FExtAttr = FDM->XMLList->GetRefer(FISAttr->AV["ref"]);
    TPlanItemForm * Dlg = NULL;
    try
     {
      Dlg = new TPlanItemForm(Application->Owner, FDM, AIE, FExtAttr->GetRoot()->CmpName("registr"), NeedKonkr);
      Dlg->ShowModal();
      iRC = (Dlg->ModalResult == mrOk);
     }
    __finally
     {
      if (Dlg)
       delete Dlg;
     }
   }
  if (AIE->GetFirstChild())
   {
    if (AIE->Count > 1)
     {
      TTagNode * __tmp = AIE->GetFirstChild();
      tmp = "";
      while (__tmp)
       {
        if (!__tmp->CmpAV("name", "sql"))
         tmp += " " + __tmp->AV["name"];
        __tmp = __tmp->GetNext();
       }
      AText = tmp.Trim();
     }
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsICNomSupport::DefOrgPres(TTagNode * AIE, UnicodeString & AText, bool NeedKonkr, UnicodeString & AXMLIE)
 {
  if (!FDM) return false;
  bool RC = false;
  TStringList *FValues = NULL;
  try
   {
      FValues = new TStringList;

      FValues->AddObject("���������� �����������",(TObject*)0);
      FValues->AddObject("������� �����������",(TObject*)1);

      RC = FDM->DefChVal(AIE, AText, NeedKonkr, AXMLIE, FValues);
   }
  __finally
   {
     if (FValues) delete FValues;
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsICNomSupport::DefExtEditValue(TTagNode * AIE, UnicodeString & AText, bool NeedKonkr, UnicodeString & AXMLIE)
 {
  return false;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsICNomSupport::DefVacSch(TTagNode * AIE, UnicodeString & AText, bool NeedKonkr, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;

  bool iRC = false;
  if (NeedKonkr)
   {

    TNomSchListForm * Dlg = NULL;
    try
     {
      Dlg = new TNomSchListForm(Application->Owner, FDM, AIE, true);
      Dlg->ShowModal();
      iRC = (Dlg->ModalResult == mrOk);
     }
    __finally
     {
      if (Dlg)
       delete Dlg;
     }
   }
  if (AIE->Count)
   {
    AText = AIE->GetFirstChild()->AV["name"].Trim();
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsICNomSupport::DefTestSch(TTagNode * AIE, UnicodeString & AText, bool NeedKonkr, UnicodeString & AXMLIE)
 {
  if (!FDM)
   return false;

  bool iRC = false;
  if (NeedKonkr)
   {

    TNomSchListForm * Dlg = NULL;
    try
     {
      Dlg = new TNomSchListForm(Application->Owner, FDM, AIE, false);
      Dlg->ShowModal();
      iRC = (Dlg->ModalResult == mrOk);
     }
    __finally
     {
      if (Dlg)
       delete Dlg;
     }
   }
  if (AIE->Count)
   {
    AText = AIE->GetFirstChild()->AV["name"].Trim();
    iRC = true;
   }
  AXMLIE = AIE->AsXML;
  return iRC;
 }
//---------------------------------------------------------------------------
