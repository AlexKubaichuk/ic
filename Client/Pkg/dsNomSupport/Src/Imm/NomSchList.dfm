object NomSchListForm: TNomSchListForm
  Left = 420
  Top = 369
  HelpContext = 4008
  ActiveControl = SchemaTL
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1042#1099#1073#1086#1088' '#1089#1093#1077#1084#1099
  ClientHeight = 475
  ClientWidth = 537
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 435
    Width = 537
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 341
    ExplicitWidth = 318
    DesignSize = (
      537
      40)
    object OkBtn: TcxButton
      Left = 372
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      OnClick = OkBtnClick
      ExplicitLeft = 153
    end
    object CancelBtn: TcxButton
      Left = 453
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      ExplicitLeft = 234
    end
  end
  object SchemaTL: TcxTreeList
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 531
    Height = 429
    BorderStyle = cxcbsNone
    Align = alClient
    Bands = <
      item
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Navigator.Buttons.CustomButtons = <>
    OptionsBehavior.AlwaysShowEditor = True
    OptionsBehavior.ConfirmDelete = False
    OptionsBehavior.DragCollapse = False
    OptionsBehavior.DragExpand = False
    OptionsBehavior.MultiSort = False
    OptionsBehavior.ShowHourGlass = False
    OptionsCustomizing.BandCustomizing = False
    OptionsCustomizing.BandHorzSizing = False
    OptionsCustomizing.BandMoving = False
    OptionsCustomizing.BandVertSizing = False
    OptionsCustomizing.ColumnCustomizing = False
    OptionsCustomizing.ColumnHorzSizing = False
    OptionsCustomizing.ColumnMoving = False
    OptionsCustomizing.ColumnVertSizing = False
    OptionsData.CancelOnExit = False
    OptionsData.AnsiSort = True
    OptionsData.Deleting = False
    OptionsView.CellAutoHeight = True
    OptionsView.ScrollBars = ssVertical
    OptionsView.ShowEditButtons = ecsbFocused
    OptionsView.ColumnAutoWidth = True
    OptionsView.GridLineColor = clActiveCaption
    OptionsView.GridLines = tlglBoth
    OptionsView.ShowRoot = False
    OptionsView.TreeLineStyle = tllsNone
    ParentFont = False
    TabOrder = 1
    OnDblClick = OkBtnClick
    ExplicitLeft = -262
    ExplicitWidth = 692
    ExplicitHeight = 375
    Data = {
      00000500A00100000F00000044617461436F6E74726F6C6C6572310300000012
      000000546378537472696E6756616C7565547970651200000054637853747269
      6E6756616C75655479706512000000546378537472696E6756616C7565547970
      6509000000445855464D5401445855464D5401445855464D5401445855464D54
      01445855464D5401445855464D5401445855464D5401445855464D5401445855
      464D540103000000000000001200030000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFF0100000008000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF
      0200000008000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF030000000800
      0000000000000000FFFFFFFFFFFFFFFFFFFFFFFF040000001200030000000000
      000000000000FFFFFFFFFFFFFFFFFFFFFFFF0500000008000000000000000000
      FFFFFFFFFFFFFFFFFFFFFFFF0600000008000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFF0700000008000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF
      0800000008000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF1A0803000000}
    object SchMIBPCol: TcxTreeListColumn
      Caption.AlignHorz = taCenter
      Caption.Text = #1052#1048#1041#1055
      DataBinding.ValueType = 'String'
      Options.CellEndEllipsis = False
      Options.Customizing = False
      Options.Editing = False
      Options.Focusing = False
      Options.IncSearch = False
      Options.Moving = False
      Options.ShowEditButtons = eisbNever
      Options.TabStop = False
      Width = 291
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      SortOrder = soAscending
      SortIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object SchNameCol: TcxTreeListColumn
      Caption.AlignHorz = taCenter
      Caption.Text = #1042#1072#1088#1080#1072#1085#1090' '#1089#1093#1077#1084#1099
      DataBinding.ValueType = 'String'
      Options.Editing = False
      Width = 150
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object SchValCol: TcxTreeListColumn
      Visible = False
      DataBinding.ValueType = 'String'
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
end
