//---------------------------------------------------------------------------

#ifndef PlanItemH
#define PlanItemH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxGroupBox.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxRadioGroup.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
//#include "Dmf_Auto.h"
#include "dsNomCommon.h"
#include "dsRegEd.h"
//---------------------------------------------------------------------------
class TPlanItemForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *Panel1;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
 TcxGroupBox *CheckGB;
 TcxComboBox *CheckCB;
 TcxRadioGroup *TypeRG;
 TcxGroupBox *PrivGB;
 TcxComboBox *VacCB;
 TcxGroupBox *TestGB;
 TcxComboBox *TestCB;
        void __fastcall OkBtnClick(TObject *Sender);
 void __fastcall TypeRGPropertiesChange(TObject *Sender);
private:	// User declarations
        TTagNode *RegNode;
        TAxeXMLContainer *FXMLList;
  TdsNomCommon *FDM;
//        TAutoFunc_DM *FDM;
  void __fastcall SetIdx(TTagNode *ANode, TcxComboBox *ACB);
  bool __fastcall CheckInput();
public:		// User declarations
        TStringList *ValuesList;
        TTagNode *Result;
        __fastcall TPlanItemForm(TComponent *Owner, TdsNomCommon *ADM, TTagNode *ParamRoot, bool isReg, bool isNeedDef);
};
//---------------------------------------------------------------------------
#endif
