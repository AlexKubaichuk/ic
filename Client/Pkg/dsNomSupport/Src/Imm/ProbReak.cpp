//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ProbReak.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma resource "*.dfm"
TProbReakForm *ProbReakForm;
//---------------------------------------------------------------------------
__fastcall TProbReakForm::TProbReakForm(TComponent* Owner, TdsNomCommon *ADM, TTagNode *AIE, bool isNeedDef, UnicodeString AProbCode)
        : TForm(Owner)
{
/*

*/
  FDM = ADM;
//  FXMLList = AXMLList;
  Result = AIE;
  FTemplateData = new TdsRegTemplateData;
/*
*/
  UnicodeString tmp = "<root>\
                       <fl ref='1032' en='0'/>\
                       <fl ref='103B'/>\
                       <fl ref='10A7'/>\
                       <fl ref='103F'/><root/>";

  TTagNode *sText;

  FTemplateData->Value["1032"] = AProbCode;

  sText = AIE->GetChildByAV("text","ref","1");
  if (sText) FTemplateData->Value["103B"] = sText->AV["value"];

  sText = AIE->GetChildByAV("text","ref","2");
  if (sText) FTemplateData->Value["10A7"] = sText->AV["value"];

  sText = AIE->GetChildByAV("text","ref","3");
  if (sText) FTemplateData->Value["103F"] = sText->AV["value"];


  sText = AIE->GetChildByAV("text","ref","4");
  if (sText)
   {
     AnalizChB->Checked = true;
     ReakTypeCB->ItemIndex = sText->AV["value"].ToInt();
     sText = AIE->GetChildByAV("text","ref","5");
     if (sText)
      {
        AnalizChB->Checked = true;
        FromED->Text = sText->AV["value"];
      }
     sText = AIE->GetChildByAV("text","ref","6");
     if (sText&&(ReakTypeCB->ItemIndex == 0))
      {
        AnalizChB->Checked = true;
        ToED->Text = sText->AV["value"];
      }
     sText = AIE->GetChildByAV("text","ref","7");
     if (sText&&(ReakTypeCB->ItemIndex == 0))
      {
        AnalizChB->Checked = true;
        ToED2->Text = sText->AV["value"];
      }
   }
  TmplNode = FDM->XMLList->GetXML("40381E23-92155860-4448");

  FTempl = new TdsRegTemplate(TmplNode);
  FTempl->StyleController = FDM->StyleController;
  FTempl->TmplPanel = CompPanel;
  FTempl->DataPath        = FDM->DataPath;

  FTempl->DataProvider->OnGetValById  = FDM->OnGetValById;
  FTempl->OnExtBtnClick = FDM->OnExtBtnClick;
  FTempl->DataProvider->OnGetClassXML = FDM->OnGetXML;

  FTempl->CreateTemplate(tmp, FTemplateData);
  FTempl->OnKeyDown            = FormKeyDown;
}
//---------------------------------------------------------------------------
void __fastcall TProbReakForm::AnalizChBClick(TObject *Sender)
{
   if (AnalizChB->Checked)
    {
      ReakTypeLab->Enabled = true;
      ReakTypeCB->Enabled  = true;
      FromLab->Enabled     = true;
      FromED->Enabled      = true;
      ReakTypeCB->Color = clWindow;
      FromED->Color     = clWindow;
      ReakTypeCB->ItemIndex = 0;
      ReakTypeCBChange(ReakTypeCB);
    }
   else
    {
       ReakTypeLab->Enabled = false;
       ReakTypeCB->Enabled  = false;
       FromLab->Enabled     = false;
       FromED->Enabled      = false;
       FromED->Text         = "";
       ToLab->Enabled = false;
       ToLab2->Enabled = false;
       ToED->Enabled  = false;
       ToED2->Enabled  = false;
       ToED->Text     = "";
       ToED2->Text     = "";
       ToED->Color       = clBtnFace;
       ToED2->Color       = clBtnFace;
       ReakTypeCB->Color = clBtnFace;
       FromED->Color     = clBtnFace;
    }
}
//---------------------------------------------------------------------------
void __fastcall TProbReakForm::ReakTypeCBChange(TObject *Sender)
{
   if (ReakTypeCB->ItemIndex == -1) ReakTypeCB->ItemIndex = 0;
   if (ReakTypeCB->ItemIndex == 0)
    {
       ToLab->Enabled = true;
       ToLab2->Enabled = true;
       ToED->Enabled  = true;
       ToED2->Enabled  = true;
       ToED->Color    = clWindow;
       ToED2->Color    = clWindow;
    }
   else
    {
        ToLab->Enabled = false;
        ToLab2->Enabled = false;
        ToED->Enabled  = false;
        ToED2->Enabled  = false;
        ToED->Text     = "";
        ToED2->Text     = "";
        ToED->Color    = clBtnFace;
        ToED2->Color    = clBtnFace;
    }
}
//---------------------------------------------------------------------------
void __fastcall TProbReakForm::OkBtnClick(TObject *Sender)
{
  if (!CheckInput()) return;
  TTagNode *__tmp;
  TStringList *FFlList = new TStringList;
  FFlList->Text =  "1032\n103B\n10A7\n103F";
  Result->DeleteChild();
  for (int i = 0; i < FFlList->Count; i++)
   {
     if (FTempl->GetValue(FFlList->Strings[i]) != "")
      {
        __tmp = Result->AddChild("text");
        __tmp->AddAttr("name",FTempl->GetName(FFlList->Strings[i]));
        __tmp->AddAttr("value",FTempl->GetValue(FFlList->Strings[i]));
        __tmp->AddAttr("ref",IntToStr(i));
      }
   }
  delete FFlList;
  if (AnalizChB->Checked)
   {
     Result->AddChild("text","name=������ ���������:"+ReakTypeCB->Text+",value="+IntToStr(ReakTypeCB->ItemIndex)+",ref=4");
     Result->AddChild("text","name=����������� �������� ������:"+FromED->Text+",value="+FromED->Text+",ref=5");
     if (ReakTypeCB->ItemIndex == 0)
      {
        Result->AddChild("text","name=����������:"+ToED->Text+",value="+ToED->Text+",ref=6");
        if (ToED2->Text.Length())
         Result->AddChild("text","name=���������� �� ���������:"+ToED2->Text+",value="+ToED2->Text+",ref=7");
      }
   }
/*  if (!FTempl->IsEmpty())
   {
     __tmp = Result->AddChild("text","name=sql");
     UnicodeString FTabAlias = "";
     TTagNode *NomTag = FXMLList->GetRefer(Result->AV["ref"]);
     if (NomTag)
      {
        NomTag = NomTag->GetTagByUID(NomTag->AV["param"]);
        if (NomTag)
         {
           NomTag = NomTag->GetParent("is");
           if (NomTag)
            FTabAlias = NomTag->AV["tabalias"];
         }
      }
     __tmp->AddAttr("value",FTempl->GetWhere(FTabAlias));
   }
   */
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
bool __fastcall TProbReakForm::CheckInput()
{
  if (!FTempl->CheckInput()) return false;
  if (AnalizChB->Checked)
   {
     if (FromED->Text == "")
      {
        ShowMessage("�� ������ ����������� �������� ������");
        return false;
      }
     if (ReakTypeCB->ItemIndex == 0)
      {
        if (ToED->Text == "")
         {
           ShowMessage("�� ������ ����������");
           return false;
         }
      }
   }
  return true;
}
//---------------------------------------------------------------------------
void __fastcall TProbReakForm::FormDestroy(TObject *Sender)
{
  delete FTempl;
//  delete TmplNode;
}
//---------------------------------------------------------------------------
void __fastcall TProbReakForm::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
  if (Key == VK_RETURN)
   OkBtnClick(OkBtn);
}
//---------------------------------------------------------------------------

