//---------------------------------------------------------------------------

#ifndef dsICNomSupportH
#define dsICNomSupportH
//---------------------------------------------------------------------------
//#include <vcl.h>
#include "dsBaseNomSupport.h"
//---------------------------------------------------------------------------
class TdsICNomSupport : public TdsBaseNomSupport
{
private:
    // ������ ������
    TAnsiStrMap  VacList;
    TAnsiStrMap  VacListCode;
    // ������ ����
    TAnsiStrMap  ProbList;
    TAnsiStrMap  ProbListCode;
    // ������ ��������
    TAnsiStrMap  InfList;
    TAnsiStrMap  InfListCode;

//    int __fastcall CorrectDay( Word Year, Word Month, Word Day );
//    bool __fastcall CalcPlanPeriod(TDate *ADateBP, TDate *ADateEP, TDate *APlanMonth);


    bool __fastcall DefDatePeriodLast     (TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefOtvodDatePeriodLast(TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefPlanPrivReak       (TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefPlanProbReak       (TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefPlanPres           (TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefPlanItem           (TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefPlanItemExec       (TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefOrgPres            (TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefExtEditValue       (TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefVacSch             (TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);
    bool __fastcall DefTestSch            (TTagNode *AIE, UnicodeString& AText, bool NeedKonkr, UnicodeString& AXMLIE);

protected:
    virtual TGetDefFunc __fastcall GetDefMethodAddr(UnicodeString AFuncName);
    virtual void __fastcall PrepareData();
public:

    __fastcall TdsICNomSupport(TAxeXMLContainer *AXMLList, UnicodeString ARegGUI);
    __fastcall ~TdsICNomSupport();
};
#endif
