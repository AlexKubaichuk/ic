//---------------------------------------------------------------------------

#ifndef SchConkretizH
#define SchConkretizH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtonEdit.hpp"
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
#include "dsRegTemplate.h"
#include "dsNomCommon.h"
#include "cxDropDownEdit.hpp"
#include "cxGroupBox.hpp"
//---------------------------------------------------------------------------
class TSchForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *CompPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
 TcxGroupBox *InfGB;
 TcxButtonEdit *VakCBE;
 TcxGroupBox *cxGroupBox1;
 TcxComboBox *InfCB;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);


private:	// User declarations
        TdsNomCommon *FDM;
        bool __fastcall CheckInput();
        UnicodeString FInfCode,FVakSch,FProbSch;
        TTagNode *TmplNode;
        TdsRegTemplate *FTempl;
//        TAxeXMLContainer *FXMLList;
//        TAutoFunc_DM *FDM;
public:		// User declarations
        TTagNode *Result;
        __fastcall TSchForm(TComponent* Owner,TTagNode *AIE, bool isNeedDef, TdsNomCommon *ADM);
};
//---------------------------------------------------------------------------
extern PACKAGE TSchForm *SchForm;
//---------------------------------------------------------------------------
#endif
