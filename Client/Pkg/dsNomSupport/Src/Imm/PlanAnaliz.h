//---------------------------------------------------------------------------

#ifndef PlanAnalizH
#define PlanAnalizH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxGroupBox.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxRadioGroup.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
//#include "Dmf_Auto.h"
#include "dsNomCommon.h"
#include "dsRegEd.h"
//---------------------------------------------------------------------------
class TPlanAnalizForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *Panel1;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
 TcxGroupBox *ExecGB;
 TCheckBox *NotExecCBChB;
 TcxComboBox *ExecCB;
 TcxGroupBox *PlanSrcGB;
 TCheckBox *NotPlanSrcCBChB;
 TcxComboBox *PlanSrcCB;
 TcxGroupBox *cxGroupBox7;
 TcxGroupBox *PrivGB;
 TLabel *VacLab;
 TcxComboBox *VacCB;
 TcxMaskEdit *VacTypeED;
 TcxGroupBox *TestGB;
 TcxComboBox *TestCB;
 TLabel *Label1;
 TcxGroupBox *CheckGB;
 TcxComboBox *CheckCB;
 TcxRadioGroup *TypeRG;
 TcxGroupBox *InfGB;
 TcxComboBox *InfCB;
 TLabel *Label2;
        void __fastcall OkBtnClick(TObject *Sender);
 void __fastcall TypeRGPropertiesChange(TObject *Sender);
private:	// User declarations
        TTagNode *RegNode;
        TAxeXMLContainer *FXMLList;
  TdsNomCommon *FDM;
//        TAutoFunc_DM *FDM;
  void __fastcall SetIdx(TTagNode *ANode, TcxComboBox *ACB);
  bool __fastcall CheckInput();
public:		// User declarations
        TStringList *ValuesList;
        TTagNode *Result;
        __fastcall TPlanAnalizForm(TComponent *Owner, TdsNomCommon *ADM, TTagNode *ParamRoot, bool isReg, bool isNeedDef);
};
//---------------------------------------------------------------------------
//extern PACKAGE TPlanAnalizForm *PlanAnalizForm;
//---------------------------------------------------------------------------
#endif
