object PlanAnalizForm: TPlanAnalizForm
  Left = 490
  Top = 307
  HelpContext = 4012
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = #1054#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1103' ...'
  ClientHeight = 414
  ClientWidth = 366
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 374
    Width = 366
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      366
      40)
    object OkBtn: TcxButton
      Left = 199
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      OnClick = OkBtnClick
    end
    object CancelBtn: TcxButton
      Left = 280
      Top = 8
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 366
    Height = 374
    Align = alClient
    TabOrder = 0
    object ExecGB: TcxGroupBox
      Left = 1
      Top = 284
      Align = alTop
      Caption = #1060#1072#1082#1090' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103':'
      TabOrder = 0
      Height = 45
      Width = 364
      object NotExecCBChB: TCheckBox
        Left = 13
        Top = 19
        Width = 40
        Height = 17
        Caption = #1053#1045
        TabOrder = 0
      end
      object ExecCB: TcxComboBox
        Left = 47
        Top = 15
        Properties.DropDownListStyle = lsEditFixedList
        Properties.Items.Strings = (
          ''
          #1074#1099#1087#1086#1083#1085#1077#1085#1080#1077
          #1074#1099#1087#1086#1083#1085#1077#1085#1080#1077' '#1087#1086#1083#1080#1082#1083#1080#1085#1080#1082#1086#1081
          #1074#1099#1087#1086#1083#1085#1077#1085#1080#1077' '#1087#1086' '#1087#1083#1072#1085#1091
          #1074#1099#1087#1086#1083#1085#1077#1085#1080#1077' '#1085#1077' '#1087#1086' '#1087#1083#1072#1085#1091' ('#1086#1090#1084#1077#1090#1082#1072' '#1076#1088#1091#1075#1080#1084' '#1052#1048#1041#1055')'
          #1074#1099#1087#1086#1083#1085#1077#1085#1080#1077' '#1074' '#1095#1091#1078#1086#1084' '#1091#1095#1088#1077#1078#1076#1077#1085#1080#1080
          #1074#1099#1087#1086#1083#1085#1077#1085#1080#1077' '#1085#1072' '#1076#1088#1091#1075#1086#1081' '#1090#1077#1088#1088#1080#1090#1086#1088#1080#1080)
        TabOrder = 1
        Width = 310
      end
    end
    object PlanSrcGB: TcxGroupBox
      Left = 1
      Top = 329
      Align = alTop
      Caption = #1048#1089#1090#1086#1095#1085#1080#1082' '#1087#1083#1072#1085#1072':'
      TabOrder = 1
      Height = 46
      Width = 364
      object NotPlanSrcCBChB: TCheckBox
        Left = 13
        Top = 19
        Width = 38
        Height = 17
        Caption = #1053#1045
        TabOrder = 0
      end
      object PlanSrcCB: TcxComboBox
        Left = 47
        Top = 17
        Properties.DropDownListStyle = lsEditFixedList
        Properties.Items.Strings = (
          ''
          #1055#1083#1072#1085#1086#1074#1072#1103
          #1056#1091#1095#1085#1086#1077' '#1076#1086#1073#1072#1074#1083#1077#1085#1080#1077
          #1055#1083#1072#1085#1080#1088#1086#1074#1097#1080#1082' '#1087#1086' '#1082#1072#1083#1077#1085#1076#1072#1088#1102
          #1055#1083#1072#1085#1080#1088#1086#1074#1097#1080#1082' '#1085#1077' '#1087#1086' '#1082#1072#1083#1077#1085#1076#1072#1088#1102
          #1055#1083#1072#1085#1080#1088#1086#1074#1097#1080#1082' '#1082#1072#1082' '#1087#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1072#1103
          #1055#1083#1072#1085#1080#1088#1086#1074#1097#1080#1082' '#1087#1086' '#1090#1091#1088#1091
          #1055#1083#1072#1085#1080#1088#1086#1074#1097#1080#1082' '#1087#1086' '#1090#1091#1088#1091' '#1082#1072#1082' '#1087#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1072#1103)
        TabOrder = 1
        Width = 310
      end
    end
    object cxGroupBox7: TcxGroupBox
      Left = 1
      Top = 48
      Align = alTop
      TabOrder = 2
      Height = 185
      Width = 364
      object PrivGB: TcxGroupBox
        Left = 2
        Top = 57
        Align = alTop
        Caption = #1055#1088#1080#1074#1080#1074#1082#1072':'
        StyleDisabled.TextColor = clBtnShadow
        TabOrder = 0
        Height = 71
        Width = 360
        object VacLab: TLabel
          Left = 11
          Top = 23
          Width = 35
          Height = 13
          Caption = #1052#1048#1041#1055':'
        end
        object Label1: TLabel
          Left = 11
          Top = 46
          Width = 22
          Height = 13
          Caption = #1042#1080#1076':'
        end
        object Label2: TLabel
          Left = 124
          Top = 43
          Width = 221
          Height = 16
          Caption = '('#1060#1086#1088#1084#1072#1090': Vxx '#1080#1083#1080' RVxx/xx '#1080#1083#1080' '#1044#1086#1087'.)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object VacCB: TcxComboBox
          Left = 52
          Top = 16
          Properties.DropDownListStyle = lsEditFixedList
          TabOrder = 0
          Width = 305
        end
        object VacTypeED: TcxMaskEdit
          Left = 52
          Top = 42
          Properties.MaskKind = emkRegExprEx
          Properties.EditMask = '(V\d\d?)|(RV\d\d?)|(RV\d\d?\/\d\d?)|('#1044#1086#1087'\.)'
          Properties.MaxLength = 0
          TabOrder = 1
          Width = 66
        end
      end
      object TestGB: TcxGroupBox
        Left = 2
        Top = 128
        Align = alTop
        Caption = #1055#1088#1086#1073#1072': '
        StyleDisabled.TextColor = clBtnShadow
        TabOrder = 1
        Height = 51
        Width = 360
        object TestCB: TcxComboBox
          AlignWithMargins = True
          Left = 5
          Top = 21
          Align = alTop
          Properties.DropDownListStyle = lsEditFixedList
          TabOrder = 0
          Width = 350
        end
      end
      object InfGB: TcxGroupBox
        Left = 2
        Top = 5
        Align = alTop
        Caption = #1048#1085#1092#1077#1082#1094#1080#1103
        StyleDisabled.TextColor = clBtnShadow
        TabOrder = 2
        Height = 52
        Width = 360
        object InfCB: TcxComboBox
          AlignWithMargins = True
          Left = 5
          Top = 21
          Align = alTop
          Properties.DropDownListStyle = lsEditFixedList
          TabOrder = 0
          Width = 350
        end
      end
    end
    object CheckGB: TcxGroupBox
      Left = 1
      Top = 233
      Align = alTop
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072': '
      StyleDisabled.TextColor = clBtnShadow
      TabOrder = 3
      Height = 51
      Width = 364
      object CheckCB: TcxComboBox
        AlignWithMargins = True
        Left = 5
        Top = 21
        Align = alTop
        Properties.DropDownListStyle = lsEditFixedList
        TabOrder = 0
        Width = 354
      end
    end
    object TypeRG: TcxRadioGroup
      Left = 1
      Top = 1
      Align = alTop
      Caption = #1040#1085#1072#1083#1080#1079#1080#1088#1086#1074#1072#1090#1100
      Properties.Columns = 3
      Properties.Items = <
        item
          Caption = #1055#1088#1080#1074#1080#1074#1082#1091
          Value = '1'
        end
        item
          Caption = #1055#1088#1086#1073#1091
          Value = '2'
        end
        item
          Caption = #1055#1088#1086#1074#1077#1088#1082#1091
          Value = '3'
        end>
      Properties.OnChange = TypeRGPropertiesChange
      ItemIndex = 0
      TabOrder = 4
      Height = 47
      Width = 364
    end
  end
end
