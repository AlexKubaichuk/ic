//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "SchConkretiz.h"
#include "NomSchList.h"
#include "dsRegEDTemplateDataProvider.h"
//---------------------------------------------------------------------------

#pragma link "cxButtonEdit"
#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "cxDropDownEdit"
#pragma link "cxGroupBox"
#pragma resource "*.dfm"

#pragma package(smart_init)

TSchForm * SchForm;
//---------------------------------------------------------------------------
__fastcall TSchForm::TSchForm(TComponent * Owner, TTagNode * AIE, bool isNeedDef, TdsNomCommon * ADM)
    : TForm(Owner)
 {
  FDM      = ADM;
  FVakSch  = "";
  FProbSch = "";
  Result   = AIE;
  FillClassCB(FDM->OnGetValById, "l.003A", (TStringList *)InfCB->Properties->Items, -1);

  //FInfCode = AInfCode.Trim();
  UnicodeString tmp = "<root>"
      "</root>";
  TmplNode = new TTagNode(NULL); /*TODO -okab -c:) : ����� ����*/
  TmplNode->Assign(FDM->XMLList->GetXML("40381E23-92155860-4448"), true);

  FTempl                  = new TdsRegTemplate(TmplNode);
  FTempl->StyleController = ADM->StyleController;
  FTempl->TmplPanel       = CompPanel;
  FTempl->DataPath        = ADM->DataPath;
  FTempl->CreateTemplate(tmp);
 }
//---------------------------------------------------------------------------
void __fastcall TSchForm::OkBtnClick(TObject * Sender)
 {
  /*if (!CheckInput()) return;
   TTagNode *__tmp;
   TStringList *FFlList = new TStringList;
   FFlList->Text =  "1077";
   Result->DeleteChild();
   for (int i = 0; i < FFlList->Count; i++)
   {
   if (FTempl->GetValue(FFlList->Strings[i]) != "")
   {
   __tmp = Result->AddChild("text");
   __tmp->AddAttr("name",FTempl->GetName(FFlList->Strings[i]));
   __tmp->AddAttr("value",FTempl->GetValue(FFlList->Strings[i]));
   }
   }
   delete FFlList;
   if (VakRB->Checked)
   {
   TTagNode *tmp =  Result->AddChild("text");
   tmp->AV["name"] = "����� ��� �������="+VakCBE->Text;
   tmp->AV["value"] = FVakSch;
   }
   else
   {
   TTagNode *tmp =  Result->AddChild("text");
   tmp->AV["name"] = "����� ��� �����="+ProbCBE->Text;
   tmp->AV["value"] = FProbSch;
   }
   if (!FTempl->IsEmpty())
   {
   __tmp = Result->AddChild("text","name=sql");
   UnicodeString FTabAlias = "";
   TTagNode *NomTag = FXMLList->GetRefer(Result->AV["ref"]);
   if (NomTag)
   {
   NomTag = NomTag->GetTagByUID(NomTag->AV["param"]);
   if (NomTag)
   {
   NomTag = NomTag->GetParent("is");
   if (NomTag)
   FTabAlias = NomTag->AV["tabalias"];
   }
   }
   UnicodeString tmpSch = "";
   if (VakRB->Checked)
   {
   if (FVakSch.Length())
   {
   if (FVakSch == "_NULL_")
   tmpSch = FTabAlias+".R1078 = '"+FVakSch+"'";
   else
   tmpSch = "Upper("+FTabAlias+".R1078) LIKE '"+_GUI(FVakSch).UpperCase()+".%'";
   }
   }
   else
   {
   if (FProbSch.Length())
   {
   if (FProbSch == "_NULL_")
   tmpSch = FTabAlias+".R1079 = '"+FProbSch+"'";
   else
   tmpSch = "Upper("+FTabAlias+".R1079) LIKE '"+_GUI(FProbSch).UpperCase()+".%'";
   }
   }
   if (tmpSch.Length())
   __tmp->AddAttr("value",FTempl->GetWhere(FTabAlias)+" and "+tmpSch);
   else
   __tmp->AddAttr("value",FTempl->GetWhere(FTabAlias));
   }*/
  ModalResult = mrOk;
 }
//---------------------------------------------------------------------------
bool __fastcall TSchForm::CheckInput()
 {
  if (!FTempl->CheckInput())
   return false;
  if (VakCBE->Text == "")
   {
    MessageBox(NULL, L"�������� ����� ��� ������� ������ ���� ���������� !!!", L"������ �����", MB_ICONHAND);
    ActiveControl = VakCBE;
    return false;
   }
  return true;
 }
//---------------------------------------------------------------------------
void __fastcall TSchForm::FormDestroy(TObject * Sender)
 {
  delete FTempl;
  delete TmplNode;
 }
//---------------------------------------------------------------------------
