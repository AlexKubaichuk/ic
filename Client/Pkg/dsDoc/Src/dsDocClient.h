//---------------------------------------------------------------------------

#ifndef dsDocClientH
#define dsDocClientH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
//---------------------------------------------------------------------------
#include "dxBar.hpp"
//#include "ServerClientClasses.h"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "ICSNomenklator.h"
#include "dsRegEDFunc.h"
#include "dsRegExtFilterSetTypes.h"
#include "dsDocTypeDef.h"
#include "dsDocNomSupportTypeDef.h"
//#include "dsDocClientUnit.h"
//---------------------------------------------------------------------------
class TdsDocDM;
//---------------------------------------------------------------------------
class PACKAGE TdsDocClient : public TComponent
{
typedef map<UnicodeString, TForm*> TICDocMap;
private:
//        TAttr           *xAtt;
        UnicodeString      FDBUser,FDBPassword,FDBCharset, FDTFormat;
        bool            FActive;
        bool            FFetchAll;
        bool            FRefreshList;
        bool            FCallEvtForInvisebleExtEdit;
        TICDocMap       FDocMap;

        bool            FUseTemplate;
        bool            FUseClassDisControls;
        bool            FUseProgressBar;
        bool            FUseUnitGUID;
        bool            FUseClsGUID;
        bool            FUseAfterScroll;

        bool            FFullEdit,FFindPanel;
        int             FULEditWidth;
        int             FUListTop;
        int             FUListLeft;
        int             FDefaultListWidth;
        int             FDefaultClassWidth;

        UnicodeString      FClKeyName;
        UnicodeString      FDBName;
        UnicodeString      FULCaption;
        TExtLoadXML     FLoadExtXML;

        TExtBtnClick    FGetExtWhere;
        TExtBtnClick    FOnCtrlDataChange;
        TGetExtData     FGetExtData;
        TClassCallBack  FOnInit;
        TClassCallBack  FDesOnInit;
        TRegistryDemoMessageEvent FDemoMsg;
        TExtEditData    FExtEditExtData;
        TExtEditDependFieldEvent FExtEditGetDependField;
        TColor          FClassTreeColor;
//        TExtValidateData FOnExtValidate;
//        TExtValidateData FOnGetDefValues;
        TGetCompValue   FGetCompValue;
        TRegEditButtonClickEvent  FOnRegEditButtonClick;
        TExtBtnGetBitmapEvent FExtBtnGetBitmap;

        TStartDragEvent  FOnUnitStartDrag;
        TDragOverEvent   FOnUnitDragOver;
        TDragDropEvent   FOnUnitDragDrop;
        TEndDragEvent    FOnUnitEndDrag;

        TcxGridGetCellStyleEvent FListContentStyleEvent;
        TRegGetOrderEvent        FOnRegGetOrderEvent;
        TRegListGetColWidthEvent FOnGetListColumnWidth;

        TRegGetFormatEvent  FOnGetFormat;

//        TdsDocClassClient* __fastcall FGetConnection();
//        void __fastcall FSetConnection(TdsDocClassClient* AValue);

        TExtBtnClick __fastcall FGetExtBtnClick();
        void __fastcall FSetExtBtnClick(TExtBtnClick AValue);

        TcxEditStyleController* __fastcall FGetStyleController();
        void __fastcall FSetStyleController(TcxEditStyleController*AValue);

        TTagNode* __fastcall FGetRegDef();
        void __fastcall FSetRegDef(TTagNode* AValue);

        TdsGetClassXMLEvent __fastcall FGetOnGetClassXML();
        void __fastcall FSetOnGetClassXML(TdsGetClassXMLEvent AValue);

        TdsGetClassXMLEvent __fastcall FGetOnGetClassZIPXML();
        void __fastcall FSetOnGetClassZIPXML(TdsGetClassXMLEvent AValue);

        TdsGetCountEvent __fastcall FGetOnGetCount();
        void __fastcall FSetOnGetCount(TdsGetCountEvent AValue);

        TdsGetIdListEvent __fastcall FGetOnGetIdList();
        void __fastcall FSetOnGetIdList(TdsGetIdListEvent AValue);

        TdsGetValByIdEvent __fastcall FGetOnGetValById();
        void __fastcall FSetOnGetValById(TdsGetValByIdEvent AValue);

        TdsGetValById10Event __fastcall FGetOnGetValById10();
        void __fastcall FSetOnGetValById10(TdsGetValById10Event AValue);

        TdsFindEvent __fastcall FGetOnFind();
        void __fastcall FSetOnFind(TdsFindEvent AValue);

        TdsDocImportEvent __fastcall FGetOnImportData();
        void __fastcall FSetOnImportData(TdsDocImportEvent AValue);

        TdsInsertDataEvent __fastcall FGetOnInsertData();
        void __fastcall FSetOnInsertData(TdsInsertDataEvent AValue);

        TdsEditDataEvent __fastcall FGetOnEditData();
        void __fastcall FSetOnEditData(TdsEditDataEvent AValue);

        TdsDeleteDataEvent __fastcall FGetOnDeleteData();
        void __fastcall FSetOnDeleteData(TdsDeleteDataEvent AValue);

        TdsDocGetSpec __fastcall FGetOnGetSpec();
        void __fastcall FSetOnGetSpec(TdsDocGetSpec AValue);

        TdsDocGetDocById __fastcall FGetOnGetDocById();
        void __fastcall FSetOnGetDocById(TdsDocGetDocById AValue);

        TdsDocGetDocPreview __fastcall FGetOnGetDocPreview();
        void __fastcall FSetOnGetDocPreview(TdsDocGetDocPreview AValue);

        TdsDocLoadDocPreview __fastcall FGetOnLoadDocPreview();
        void __fastcall FSetOnLoadDocPreview(TdsDocLoadDocPreview AValue);

        TdsDocSaveDocEvent __fastcall FGetOnSaveDoc();
        void __fastcall FSetOnSaveDoc(TdsDocSaveDocEvent AValue);

        TdsDocCheckByPeriod __fastcall FGetOnCheckByPeriod();
        void __fastcall FSetOnCheckByPeriod(TdsDocCheckByPeriod AValue);

        TdsDocDocListClearByDate __fastcall FGetOnDocListClearByDate();
        void __fastcall FSetOnDocListClearByDate(TdsDocDocListClearByDate AValue);

        TdsDocGetDocCountBySpec __fastcall FGetOnGetDocCountBySpec();
        void __fastcall FSetOnGetDocCountBySpec(TdsDocGetDocCountBySpec AValue);

        TdsDocIsMainOwnerCode __fastcall FGetOnIsMainOwnerCode();
        void __fastcall FSetOnIsMainOwnerCode(TdsDocIsMainOwnerCode AValue);

        TdsDocCopySpec __fastcall FGetOnCopySpec();
        void __fastcall FSetOnCopySpec(TdsDocCopySpec AValue);

        TdsDocCheckSpecExists __fastcall FGetOnCheckSpecExists();
        void __fastcall FSetOnCheckSpecExists(TdsDocCheckSpecExists AValue);

        TdsDocSetTextDataEvent __fastcall FGetOnSetText();
        void __fastcall FSetOnSetText(TdsDocSetTextDataEvent AValue);

        TdsDocGetTextDataEvent __fastcall FGetOnGetText();
        void __fastcall FSetOnGetText(TdsDocGetTextDataEvent AValue);

        TdsDocCheckDocCreate __fastcall FGetOnCheckDocCreate();
        void __fastcall FSetOnCheckDocCreate(TdsDocCheckDocCreate AValue);

        TdsCheckExistDocCreate __fastcall FGetOnCheckExistDocCreate();
        void __fastcall FSetOnCheckExistDocCreate(TdsCheckExistDocCreate AValue);

        TdsDocDocCreate __fastcall FGetOnDocCreate();
        void __fastcall FSetOnDocCreate(TdsDocDocCreate AValue);

        TdsDocStartDocCreate __fastcall FGetOnStartDocCreate();
        void __fastcall FSetOnStartDocCreate(TdsDocStartDocCreate AValue);

        TdsDocDocCreateProgress __fastcall FGetOnDocCreateProgress();
        void __fastcall FSetOnDocCreateProgress(TdsDocDocCreateProgress AValue);


        TdsDocCreateCheckFilterEvent __fastcall FGetOnDocCreateCheckFilter();
        void __fastcall FSetOnDocCreateCheckFilter(TdsDocCreateCheckFilterEvent AValue);

        UnicodeString __fastcall FGetSubDir();
        void __fastcall FSetSubDir(UnicodeString AValue);

        TdsNomCallDefProcEvent __fastcall FGetOnCallDefProc();
        void __fastcall FSetOnCallDefProc(TdsNomCallDefProcEvent AValue);

        TdsNomCallDefProcEvent __fastcall FGetOnCallDocDefProc();
        void __fastcall FSetOnCallDocDefProc(TdsNomCallDefProcEvent AValue);

        TdsGetValByIdEvent __fastcall FGetOnMainAppGetValById();
        void __fastcall FSetOnMainAppGetValById(TdsGetValByIdEvent AValue);

  TAxeXMLContainer* __fastcall FGetXMLList();
  void __fastcall FSetXMLList(TAxeXMLContainer *AVal);

  UnicodeString __fastcall FGetMainOwnerCode();
  void __fastcall FSetMainOwnerCode(UnicodeString AVal);

  TdsGetValByIdEvent __fastcall FGetOnGetQList();
  void __fastcall FSetOnGetQList(TdsGetValByIdEvent AVal);

  TdsDocCanHandEdit __fastcall FGetOnCanHandEdit();
  void __fastcall FSetOnCanHandEdit(TdsDocCanHandEdit AVal);

  UnicodeString __fastcall FGetXMLPath();
  void __fastcall FSetXMLPath(UnicodeString AVal);

  UnicodeString __fastcall FGetDataPath();
  void __fastcall FSetDataPath(UnicodeString AVal);

  TColor __fastcall FGetReqColor();
  void __fastcall FSetReqColor(TColor AVal);

  TExtValidateData __fastcall FGetOnExtValidate();
  void __fastcall FSetOnExtValidate(TExtValidateData AVal);

  TExtValidateData __fastcall FGetOnGetDefValues();
  void __fastcall FSetOnGetDefValues(TExtValidateData AVal);

  TICSNomenklator* __fastcall FGetNom();
  TICSNomenklator* __fastcall FGetDocNom();
  TTagNode* __fastcall FGetNmkl();
  TTagNode* __fastcall FGetDocNmkl();


  bool __fastcall FGetSpecEnabled();
  void __fastcall FSetSpecEnabled(bool AVal);

  bool __fastcall FGetSpecEditable();
  void __fastcall FSetSpecEditable(bool AVal);


  bool __fastcall FGetDocEnabled();
  void __fastcall FSetDocEnabled(bool AVal);

  bool __fastcall FGetDocEditable();
  void __fastcall FSetDocEditable(bool AVal);

  bool __fastcall FGetDocHandEditable();
  void __fastcall FSetDocHandEditable(bool AVal);

  bool __fastcall FGetFilterEnabled();
  void __fastcall FSetFilterEnabled(bool AVal);

  bool __fastcall FGetFilterEditable();
  void __fastcall FSetFilterEditable(bool AVal);

  UnicodeString __fastcall FGetExternalView();
  void __fastcall FSetExternalView(UnicodeString AVal);

        void __fastcall FSetActive(bool AActive);
        void __fastcall FSetFullEdit(bool AFullEdit);
        void __fastcall FSetUListTop(int ATop);
        void __fastcall FSetUListLeft(int ALeft);
        void __fastcall FSetDBName(UnicodeString  ADBName);
        void __fastcall FSetInit(int APersent, UnicodeString AMess);
        void __fastcall FSetDTFormat(UnicodeString  AParam);
protected:
        TObject   *PrF;
        bool __fastcall Connect(UnicodeString ADBName, UnicodeString AXMLName);
        TdsDocDM     *FDM;


        bool           __fastcall FGetClassEditable();
        void           __fastcall FSetClassEditable(bool AVal);
        UnicodeString  __fastcall FGetClassShowExpanded();
        void           __fastcall FSetClassShowExpanded(UnicodeString AVal);

public:
        TAnsiStrMap  HelpContextList;

        void __fastcall ImportDoc();
        void __fastcall ExportDoc();
        void __fastcall DocList(bool ADocMode = false);
        void __fastcall FilterList();
        void __fastcall SubOrgList();
        void __fastcall SpecList();
  void __fastcall HTMLPreview(UnicodeString AHTML);
  void __fastcall PreviewDocument(UnicodeString AGUI, TkabProgress AProgressProc);
  void __fastcall GetQuickList(UnicodeString AId, TStringList *AList);
  UnicodeString __fastcall CreateDocument(TkabProgress AProgressProc, UnicodeString ASpecGUI, UnicodeString AFilter);

        __fastcall TdsDocClient(TComponent* Owner);
        __fastcall ~TdsDocClient();
__published:

        __property TRegGetFormatEvent  OnGetFormat  = {read=FOnGetFormat, write=FOnGetFormat};

        __property TcxEditStyleController *StyleController = {read=FGetStyleController,write=FSetStyleController};


        __property TTagNode *RegDef = {read=FGetRegDef, write=FSetRegDef};

        __property bool CallEvtForInvisebleExtEdit  = {read=FCallEvtForInvisebleExtEdit, write=FCallEvtForInvisebleExtEdit};

        __property TRegEditButtonClickEvent  OnRegEditButtonClick  = {read=FOnRegEditButtonClick, write=FOnRegEditButtonClick};
        __property TExtBtnGetBitmapEvent  OnGetExtBtnGetBitmap  = {read=FExtBtnGetBitmap, write=FExtBtnGetBitmap};

        __property int  DefaultListWidth  = {read=FDefaultListWidth, write=FDefaultListWidth};
        __property int  DefaultClassWidth  = {read=FDefaultClassWidth, write=FDefaultClassWidth};

        __property TGetExtData OnGetExtData    = {read=FGetExtData, write=FGetExtData};
        __property TExtBtnClick OnExtBtnClick  = {read=FGetExtBtnClick, write=FSetExtBtnClick};

        __property TRegistryDemoMessageEvent OnDemoMessage  = {read=FDemoMsg, write=FDemoMsg};
        __property TExtBtnClick OnGetExtWhere = {read=FGetExtWhere, write=FGetExtWhere};
        __property TGetCompValue OnGetCompValue = {read=FGetCompValue, write=FGetCompValue};

        __property TExtBtnClick OnCtrlDataChange    = {read=FOnCtrlDataChange, write=FOnCtrlDataChange};


        __property TExtEditData OnExtEditExtData     = {read=FExtEditExtData, write=FExtEditExtData};

        __property TStartDragEvent  OnUnitStartDrag = {read=FOnUnitStartDrag, write=FOnUnitStartDrag};
        __property TDragOverEvent   OnUnitDragOver = {read=FOnUnitDragOver, write=FOnUnitDragOver};
        __property TDragDropEvent   OnUnitDragDrop = {read=FOnUnitDragDrop, write=FOnUnitDragDrop};
        __property TEndDragEvent    OnUnitEndDrag = {read=FOnUnitEndDrag, write=FOnUnitEndDrag};

        __property TExtLoadXML OnLoadXML   = {read=FLoadExtXML, write=FLoadExtXML};


        __property bool UseTemplate        = {read=FUseTemplate,write=FUseTemplate};
        __property bool UseProgressBar  = {read=FUseProgressBar,write=FUseProgressBar};


        __property bool FullEdit        = {read=FFullEdit,write=FSetFullEdit};
        __property bool RefreshList     = {read=FRefreshList,write=FRefreshList};

        __property bool Active          = {read=FActive,write=FSetActive};
//        __property bool LargeIcons      = {read=FLargeIcons,write=FLargeIcons};
        __property bool FetchAll        = {read=FFetchAll,write=FFetchAll};
        __property bool UseAfterScroll  = {read=FUseAfterScroll,write=FUseAfterScroll};

        __property TcxGridGetCellStyleEvent OnGetListContentStyle = {read=FListContentStyleEvent,write=FListContentStyleEvent};
        __property TRegGetOrderEvent        OnGetOrder = {read=FOnRegGetOrderEvent, write=FOnRegGetOrderEvent};
        __property TRegListGetColWidthEvent OnGetListColumnWidth = {read=FOnGetListColumnWidth, write=FOnGetListColumnWidth};

        __property int ULEditWidth      = {read=FULEditWidth,write=FULEditWidth};
        __property int UnitListTop      = {read=FUListTop,write=FSetUListTop, default=0};
        __property int UnitListLeft     = {read=FUListLeft,write=FSetUListLeft, default=0};

        __property UnicodeString DBName    = {read=FDBName,write=FSetDBName};
        __property TClassCallBack OnInit    = {read=FOnInit,write=FOnInit};

        __property UnicodeString DTFormat         = {read=FDTFormat,write=FSetDTFormat};

        __property TColor RequiredColor  = {read=FGetReqColor,write=FSetReqColor};
        __property TExtEditDependFieldEvent OnExtEditGetDependField = {read=FExtEditGetDependField,write=FExtEditGetDependField};

        __property TExtValidateData OnExtValidate = {read=FGetOnExtValidate,write=FSetOnExtValidate};
        __property TExtValidateData OnGetDefValues = {read=FGetOnGetDefValues,write=FSetOnGetDefValues};

        __property UnicodeString DBUser  = {read=FDBUser,write=FDBUser};
        __property UnicodeString DBPassword  = {read=FDBPassword,write=FDBPassword};
        __property UnicodeString DBCharset  = {read=FDBCharset,write=FDBCharset};

        __property TdsGetClassXMLEvent OnGetClassXML  = {read=FGetOnGetClassXML,write=FSetOnGetClassXML};
        __property TdsGetClassXMLEvent OnGetClassZIPXML  = {read=FGetOnGetClassZIPXML,write=FSetOnGetClassZIPXML};
        __property TdsGetCountEvent OnGetCount  = {read=FGetOnGetCount,write=FSetOnGetCount};
        __property TdsGetIdListEvent OnGetIdList  = {read=FGetOnGetIdList,write=FSetOnGetIdList};
        __property TdsGetValByIdEvent OnGetValById  = {read=FGetOnGetValById,write=FSetOnGetValById};
        __property TdsGetValById10Event OnGetValById10  = {read=FGetOnGetValById10,write=FSetOnGetValById10};
        __property TdsFindEvent OnFind  = {read=FGetOnFind,write=FSetOnFind};
        __property TdsDocImportEvent OnImportData  = {read=FGetOnImportData,write=FSetOnImportData};
        __property TdsInsertDataEvent OnInsertData  = {read=FGetOnInsertData,write=FSetOnInsertData};
        __property TdsEditDataEvent OnEditData  = {read=FGetOnEditData,write=FSetOnEditData};
        __property TdsDeleteDataEvent OnDeleteData  = {read=FGetOnDeleteData,write=FSetOnDeleteData};

        __property TdsDocGetSpec       OnGetSpec        = {read=FGetOnGetSpec,write=FSetOnGetSpec};
        __property TdsDocGetDocById    OnGetDocById     = {read=FGetOnGetDocById,write=FSetOnGetDocById};
  __property TdsDocGetDocPreview OnGetDocPreview   = {read=FGetOnGetDocPreview,write=FSetOnGetDocPreview};
  __property TdsDocLoadDocPreview OnLoadDocPreview = {read=FGetOnLoadDocPreview,write=FSetOnLoadDocPreview};

        __property TdsDocCheckByPeriod OnCheckByPeriod  = {read=FGetOnCheckByPeriod,write=FSetOnCheckByPeriod};
        __property TdsDocDocListClearByDate OnDocListClearByDate  = {read=FGetOnDocListClearByDate,write=FSetOnDocListClearByDate};

  __property TdsDocGetDocCountBySpec OnGetDocCountBySpec   = {read=FGetOnGetDocCountBySpec,write=FSetOnGetDocCountBySpec};
  __property TdsDocIsMainOwnerCode OnIsMainOwnerCode       = {read=FGetOnIsMainOwnerCode,write=FSetOnIsMainOwnerCode};
  __property UnicodeString MainOwnerCode                   = {read=FGetMainOwnerCode,write=FSetMainOwnerCode};
  __property TdsDocCopySpec OnCopySpec                     = {read=FGetOnCopySpec,write=FSetOnCopySpec};
  __property TdsDocCheckSpecExists OnCheckSpecExists       = {read=FGetOnCheckSpecExists,write=FSetOnCheckSpecExists};
  __property TdsDocSetTextDataEvent OnSetText              = {read=FGetOnSetText,write=FSetOnSetText};
  __property TdsDocGetTextDataEvent OnGetText              = {read=FGetOnGetText,write=FSetOnGetText};


  __property TdsDocCheckDocCreate   OnCheckDocCreate       = {read=FGetOnCheckDocCreate,write=FSetOnCheckDocCreate};
  __property TdsCheckExistDocCreate OnCheckExistDocCreate  = {read=FGetOnCheckExistDocCreate,write=FSetOnCheckExistDocCreate};
  __property TdsDocDocCreate        OnDocCreate            = {read=FGetOnDocCreate,write=FSetOnDocCreate};

  __property TdsDocStartDocCreate     OnStartDocCreate     = {read=FGetOnStartDocCreate,write=FSetOnStartDocCreate};
  __property TdsDocDocCreateProgress  OnDocCreateProgress  = {read=FGetOnDocCreateProgress,write=FSetOnDocCreateProgress};

  __property TdsDocSaveDocEvent  OnSaveDoc                 = { read = FGetOnSaveDoc, write = FSetOnSaveDoc};

  __property TdsDocCreateCheckFilterEvent OnDocCreateCheckFilter = {read=FGetOnDocCreateCheckFilter,write=FSetOnDocCreateCheckFilter};
  __property UnicodeString SubDir = {read=FGetSubDir,write=FSetSubDir};

  __property TdsNomCallDefProcEvent OnCallDefProc          = { read = FGetOnCallDefProc, write = FSetOnCallDefProc};
  __property TdsNomCallDefProcEvent OnCallDocDefProc       = { read = FGetOnCallDocDefProc, write = FSetOnCallDocDefProc};

  __property TdsGetValByIdEvent OnMainAppGetValById        = {read=FGetOnMainAppGetValById,write=FSetOnMainAppGetValById};

  __property TAxeXMLContainer *XMLList = {read=FGetXMLList, write=FSetXMLList};

  __property TdsGetValByIdEvent OnGetQList = {read=FGetOnGetQList, write=FSetOnGetQList};
  __property TdsDocCanHandEdit OnCanHandEdit = {read=FGetOnCanHandEdit,write=FSetOnCanHandEdit};


  __property bool SpecEnabled             = {read=FGetSpecEnabled, write=FSetSpecEnabled};
  __property bool SpecEditable            = {read=FGetSpecEditable, write=FSetSpecEditable};

  __property bool DocEnabled              = {read=FGetDocEnabled, write=FSetDocEnabled};
  __property bool DocEditable             = {read=FGetDocEditable, write=FSetDocEditable};
  __property bool DocHandEditable         = {read=FGetDocHandEditable, write=FSetDocHandEditable};

  __property bool FilterEnabled           = {read=FGetFilterEnabled, write=FSetFilterEnabled};
  __property bool FilterEditable          = {read=FGetFilterEditable, write=FSetFilterEditable};


  __property TICSNomenklator*  Nom       = {read=FGetNom};
  __property TICSNomenklator*  DocNom    = {read=FGetDocNom};
//  __property UnicodeString BaseISRef = {read=FBaseISRef, write=FBaseISRef};
  __property TTagNode*     Nmkl = {read=FGetNmkl};
  __property TTagNode*     DocNmkl = {read=FGetDocNmkl};
  __property UnicodeString       XMLPath = {read=FGetXMLPath, write=FSetXMLPath};
  __property UnicodeString       DataPath = {read=FGetDataPath, write=FSetDataPath};
  __property UnicodeString ExternalView = {read=FGetExternalView, write=FSetExternalView};
};
//---------------------------------------------------------------------------
#endif

