//---------------------------------------------------------------------------

#ifndef dsDocGetCheckDocOrgUnitH
#define dsDocGetCheckDocOrgUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxRadioGroup.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
//#include <map>
#include "dsDocDMUnit.h"
//---------------------------------------------------------------------------
class TdsDocGetCheckDocOrgForm : public TForm
{
typedef map<int,UnicodeString> TIntStrMap;
__published:	// IDE-managed Components
        TcxRadioButton *ByTypeRB;
        TcxRadioButton *ByNameRB;
        TcxComboBox *OrgTypeCB;
        TPanel *Panel1;
        TcxButton *CancelBtn;
        TcxButton *ApplyBtn;
        TcxComboBox *OrgCB;
        void __fastcall ByNameRBClick(TObject *Sender);
        void __fastcall OrgCBPropertiesChange(TObject *Sender);
        void __fastcall ByTypeRBClick(TObject *Sender);
        void __fastcall OrgTypeCBPropertiesChange(TObject *Sender);
        void __fastcall ApplyBtnClick(TObject *Sender);
private:	// User declarations
        bool       FCreateByType;
        UnicodeString FOrgType, FOrgCode;
        TIntStrMap  FOrgTypeMap;
        TIntStrMap  FOrgCodeMap;
        TdsDocDM   *FDM;
public:		// User declarations
        __fastcall TdsDocGetCheckDocOrgForm(TComponent* Owner, TdsDocDM *ADM);
        __property bool CreateByType  = {read = FCreateByType};
        __property UnicodeString OrgType = {read = FOrgType};
        __property UnicodeString OrgCode = {read = FOrgCode};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocGetCheckDocOrgForm *dsDocGetCheckDocOrgForm;
//---------------------------------------------------------------------------
#endif
