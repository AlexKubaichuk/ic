//---------------------------------------------------------------------------
#ifndef dsDocFilterListUnitH
#define dsDocFilterListUnitH
//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxClasses.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxData.hpp"
#include "cxDataStorage.hpp"
#include "cxEdit.hpp"
#include "cxFilter.hpp"
#include "cxGraphics.hpp"
#include "cxGrid.hpp"
#include "cxGridCustomTableView.hpp"
#include "cxGridCustomView.hpp"
#include "cxGridLevel.hpp"
#include "cxGridTableView.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxNavigator.hpp"
#include "cxStyles.hpp"
#include "dxBar.hpp"
#include "dxStatusBar.hpp"
//---------------------------------------------------------------------------
#include "dsDocDMUnit.h"
#include "FilterEditEx.h"
//---------------------------------------------------------------------------
class PACKAGE TdsDocFilterListForm : public TForm
{
__published:	// IDE-managed Components
        TdxBarManager *ClassTBM;
        TActionList *ClassAL;
        TAction *actInsert;
        TAction *actEdit;
        TAction *actDelete;
        TPopupMenu *PMenu;
        TMenuItem *InsertItem;
        TMenuItem *ModifyItem;
        TMenuItem *DeleteItem;
        TImageList *RegIL;
        TdxStatusBar *StatusSB;
        TAction *actApply;
        TAction *actClose;
        TcxImageList *LMainIL;
        TdxBarLargeButton *dxBarLargeButton1;
        TdxBarLargeButton *dxBarLargeButton2;
        TdxBarLargeButton *dxBarLargeButton3;
        TdxBarLargeButton *dxBarLargeButton4;
 TcxStyleRepository *StyleRepos;
        TcxStyle *cxStyle1;
 TcxGrid *FltList;
 TcxGridTableView *FltListView;
 TcxGridColumn *ldvNameCol;
 TcxGridLevel *FltListLevel1;
 TTimer *ShowTimer;
 TdxStatusBarContainerControl *StatusSBContainer3;
 TdxStatusBarContainerControl *StatusSBContainer4;
 TProgressBar *PrBar;
 TLabel *MsgLab;
 TcxStyle *cxStyle2;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall actInsertExecute(TObject *Sender);
        void __fastcall actEditExecute(TObject *Sender);
        void __fastcall actDeleteExecute(TObject *Sender);
        void __fastcall actCloseExecute(TObject *Sender);
        void __fastcall actApplyExecute(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall FormConstrainedResize(TObject *Sender,
          int &MinWidth, int &MinHeight, int &MaxWidth, int &MaxHeight);
 void __fastcall ShowTimerTimer(TObject *Sender);
 void __fastcall FormShow(TObject *Sender);
 void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
 void __fastcall FltListViewFocusedRecordChanged(TcxCustomGridTableView *Sender,
          TcxCustomGridRecord *APrevFocusedRecord, TcxCustomGridRecord *AFocusedRecord,
          bool ANewItemRecordFocusingChanged);
private:	// User declarations
  TkabCustomDataSource *FDataSrc;
  TTagNode     *RootClass;
  TdsRegEDContainer *FCtrList;
  __int64    xpCount,xcTop;
  bool FCanSel;
  UnicodeString FInsertedRecId;
    TdsDocCanDeleteChoiceItemEvent FCanDelete;
    int    fltType;
    __int64   curCD;
    TTagNode *FSelFlt;
    TdsDocDM *FDM;

  void __fastcall FGetDataProgress(int AMax, int ACur, UnicodeString AMsg);
  void __fastcall RefreshById(UnicodeString ACode);
  void __fastcall CreateSrc();
  void __fastcall ChangeSrc();
  void __fastcall CheckCtrlState(bool AEnable);
  void __fastcall CheckCtrlVisible();
  bool __fastcall CreateListExt(TTagNode *itTag, UnicodeString &UID);
  TkabCustomDataSetRow* __fastcall CurRow();
  TkabCustomDataSetRow* __fastcall EditedRow();
  bool __fastcall FCanClose(TFilterEditExForm *AEditDlg, UnicodeString ACondName);
  bool __fastcall IsRecSelected();


    void __fastcall quFilterCommit();
    void __fastcall quFilterCancel();
public:		// User declarations
    __fastcall TdsDocFilterListForm(TComponent* Owner, UnicodeString ACapt, int AType, TdsDocDM *ADM, bool ACanSel  = false);
    UnicodeString    fltPath;

__published:	// IDE-managed Components
    __property TdsDocCanDeleteChoiceItemEvent OnCanDelete = {read=FCanDelete, write=FCanDelete};
    __property TTagNode *SelFlt = {read=FSelFlt, write=FSelFlt};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocFilterListForm *dsDocFilterListForm;
//---------------------------------------------------------------------------
#endif
