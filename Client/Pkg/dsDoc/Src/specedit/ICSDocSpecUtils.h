/*
  ����        - ICSDocSpecUtils.h
  ������      - ��������������� (ICS)
  ��������    - ������ ��� ������ �� ��������������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 10.01.2007
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  10.01.2007
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef ICSDocSpecUtilsH
#define ICSDocSpecUtilsH

//---------------------------------------------------------------------------

//#include "ICSDoc.h"	//��������� �������� � ���� ��������

#include <SysUtils.hpp>

#include "XMLContainer.h"

//###########################################################################
//##                                                                       ##
//##                                Publics                                ##
//##                                                                       ##
//###########################################################################

namespace ICSDocSpec
{

// ������ ������� �������������
extern PACKAGE const UnicodeString csSpecDefVersion;
// ������ ������� ������������� �� ���������
extern PACKAGE const UnicodeString csSpecDefDefaultVersion;

//---------------------------------------------------------------------------

// ��������� ������ ������� �������������
extern PACKAGE UnicodeString __fastcall GetSpecDefVersion(TTagNode *ASpec);
// ����������, �������������� �� ��������� ������������
extern PACKAGE bool __fastcall IsSpecSupported(TTagNode *ASpec);
// ���������, �������������� �� ��������� ������������. ���� ������������
// �� ��������������, �� ����������� ���������� EICSDocSpecError
extern PACKAGE void __fastcall CheckSpec(TTagNode *ASpec);
// ���������� �������������:
//   - ��������� �������������� ��������� � ��������� �� ���������� �� ���������
//   - ���������� �� ��������� ������ �������
// ���� ������������ �� ��������������, �� ����������� ���������� EICSDocSpecError
extern PACKAGE bool __fastcall UpdateSpec(TTagNode *ASpec);

//---------------------------------------------------------------------------

class PACKAGE EICSDocSpecError : public System::Sysutils::Exception
{
public:
    __fastcall EICSDocSpecError(const UnicodeString Msg);
};

//---------------------------------------------------------------------------

} // end of namespace ICSDocSpec

#endif
