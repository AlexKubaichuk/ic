//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SEPageSetup.h"
#include "icsDocPreviewConstDef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxGroupBox"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxRadioGroup"
#pragma link "cxSpinEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TSEPageSetupForm *SEPageSetupForm;
//---------------------------------------------------------------------------
__fastcall TSEPageSetupForm::TSEPageSetupForm(TComponent* Owner)
        : TForm(Owner)
{
  Caption = FMT(icsDocPageSetupCaption);
  OrientationGB->Caption = FMT(icsDocPageSetupOrientationGBCaption);
  portRB->Caption = FMT(icsDocPageSetupportRBCaption);
  landRB->Caption = FMT(icsDocPageSetuplandRBCaption);
  FieldsGB->Caption = FMT(icsDocPageSetupFieldsGBCaption);
  OkBtn->Caption = FMT(icsDocPageSetupOkBtnCaption);
  CancelBtn->Caption = FMT(icsDocPageSetupCancelBtnCaption);
  PrKoeffLab->Caption = FMT(icsDocPagePrintKoeff);

}
//---------------------------------------------------------------------------
void __fastcall TSEPageSetupForm::portRBClick(TObject *Sender)
{
  landRB->Checked = false;
  portRB->Checked = true;
  landIM->Visible = landRB->Checked;
  portIM->Visible = portRB->Checked;
}
//---------------------------------------------------------------------------
void __fastcall TSEPageSetupForm::landRBClick(TObject *Sender)
{
  landRB->Checked = true;
  portRB->Checked = false;
  landIM->Visible = landRB->Checked;
  portIM->Visible = portRB->Checked;
}
//---------------------------------------------------------------------------
