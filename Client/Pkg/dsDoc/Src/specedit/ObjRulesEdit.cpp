/*
  ����        - ObjRulesEdit.cpp
  ������      - ��������������� (ICS)
  ��������    - ���� ���������� ������, ����������� �������� �����
                ��� ������������ ������ ����������
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 17.11.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "ObjRulesEdit.h"
//#include "DKMBUtils.h"
#include "msgdef.h"
#include "ICSDocSpec.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "DKListBox"
#pragma resource "*.dfm"

//***************************************************************************
//********************** ���������� ��������� *******************************
//**************************** PRIVATE **************************************
//***************************************************************************

const int nTableNormalCrossType = 0;    //�������� ItemIndex'� ���������������� ������ cbCrossType,
                                        //��������������� �������� "normal" �������� "crosstype"
                                        //xml-���� "tabrules" �������������
const int nTableTransCrossType  = 1;    //�������� ItemIndex'� ���������������� ������ cbCrossType,
                                        //��������������� �������� "trans" �������� "crosstype"
                                        //xml-���� "tabrules" �������������

//***************************************************************************
//********************** ��������/��������/����� ����� **********************
//***************************************************************************

__fastcall TfmObjRulesEdit::TfmObjRulesEdit( TComponent* Owner, TTagNode *ASpec, TTagNode *AIntegrSpec )
        : TForm(Owner)
{
  if (!AIntegrSpec)
    throw DKClasses::ENullArgument(__FUNC__, "AIntegrSpec");
  if (!ASpec)
    throw DKClasses::ENullArgument(__FUNC__, "ASpec");
  FIntegrSpec = AIntegrSpec;
  FSpec = ASpec;
  FObjRules   = NULL;
  FOnGetDocElementsList      = NULL;
  FOnGetDocElementViewText   = NULL;
  FOnGetSpecMainName         = NULL;

  Caption = FMT(icsDocObjRulesEditCaption);
  ObjTitleLab->Caption = FMT(icsDocObjRulesEditObjTitleCaption);
  lbThisObjTitle->Caption = FMT(icsDocObjRulesEditThisObjTitle);
  lbRefObjTitle->Caption = FMT(icsDocObjRulesEditRefObjTitle);
  pPropHead->Caption = FMT(icsDocObjRulesEditPropHead);

  AssocTypeLab->Caption = FMT(icsDocObjRulesEditAssocType);
  cbCrossType->Properties->Items->Clear();
  cbCrossType->Properties->Items->Add(cFMT(icsDocObjRulesEditCrossTypeN));
  cbCrossType->Properties->Items->Add(cFMT(icsDocObjRulesEditCrossTypeC));

  actOk->Caption = FMT(icsDocObjRulesEditOkCaption);
  actOk->Hint = FMT(icsDocObjRulesEditOkHint);
  bCancel->Caption = FMT(icsDocObjRulesEditCancelBtnCaption);
  bHelp->Caption = FMT(icsDocObjRulesEditHelpBtnCaption);
}

//---------------------------------------------------------------------------

__fastcall TfmObjRulesEdit::~TfmObjRulesEdit()
{
  __DELETE_OBJ( FObjRules )
}

//---------------------------------------------------------------------------

void __fastcall TfmObjRulesEdit::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif

  //��������� caption'� ����� lbThisSpecTitle
  UnicodeString ThisSpecTitle = "";
  if ( FOnGetSpecMainName )
    FOnGetSpecMainName( this, ThisSpecTitle );
  if ( ThisSpecTitle == "" )
    ThisSpecTitle = "???";
  lbThisSpecTitle->Caption = "(" + ThisSpecTitle + ")";

  //��������� caption'� ����� lbRefSpecTitle
  UnicodeString RefSpecTitle = "";
  if ( FIntegrSpec->GetChildByName( "passport" ) )
    RefSpecTitle = FIntegrSpec->GetChildByName( "passport" )->AV["mainname"];
  if ( RefSpecTitle == "" )
    RefSpecTitle = "???";
  lbRefSpecTitle->Caption = "(" + RefSpecTitle + ")";

  //�������� ������ ��������
  if ( FOnGetDocElementsList )
  {
    lbxThisObj->DisableOnSelect();
    FOnGetDocElementsList( this, lbxThisObj->Items );
    lbxThisObj->EnableOnSelect();
   lbxThisObj->First();
  }
  if( !lbxThisObj->Items->Count )
    UpdateActionsEnabled();

  pProps->Top++;
}

//***************************************************************************
//********************** ������ ����� ***************************************
//***************************************************************************

TTagNode* __fastcall TfmObjRulesEdit::GetThisObj()
{
  return ( lbxThisObj->ItemIndex == -1 ) ? NULL : dynamic_cast<TTagNode*>( LBX_CUR_OBJECT( lbxThisObj ) );

}

//---------------------------------------------------------------------------

TTagNode* __fastcall TfmObjRulesEdit::GetRefObj()
{
  return ( lbxRefObj->ItemIndex == -1 ) ? NULL : dynamic_cast<TTagNode*>( LBX_CUR_OBJECT( lbxRefObj ) );
}

//---------------------------------------------------------------------------

//���������� ����������� ��������

void __fastcall TfmObjRulesEdit::UpdateActionsEnabled()
{
  bool bOkEn;
  bOkEn = false;

  if ( ( lbxThisObj->ItemIndex != -1 ) && ( lbxRefObj->ItemIndex != -1 ) )
    bOkEn = true;

  actOk->Enabled = bOkEn;
}

//---------------------------------------------------------------------------

//�������� ������ ���������� ( xml-���� insrules, listrules ��� tabrules ������������� )
// ������������ ��������:
// true  - Ok
// false - ���� ������� ���������� ��� �������, ���� �� ������ ������ ��� ������������� ������

bool __fastcall TfmObjRulesEdit::CreateObjRules()
{
  TTagNode *ndThisObj = GetThisObj();
  TTagNode *ndRefObj  = GetRefObj();
  if ( !ndThisObj || !ndRefObj )
    return false;
  /* TODO -okdv -cInsListRules : ���� �������� �������� ����� insrules � listrules */
  if ( FObjRules &&
       FObjRules->CmpName( "tabrules" ) &&
       FObjRules->CmpAV( "thisref", ndThisObj->AV["uid"] ) &&
       FObjRules->CmpAV( "ref"    , FIntegrSpec->GetChildByName( "passport" )->AV["GUI"] +
                                    "." +
                                    ndRefObj->AV["uid"] )
  )
  {
    if ( !( FObjRules->CmpAV( "crosstype", "normal" ) &&
            ( cbCrossType->ItemIndex == nTableNormalCrossType )
          ) &&
         !( FObjRules->CmpAV( "crosstype", "trans"  ) &&
            ( cbCrossType->ItemIndex == nTableTransCrossType  )
          )
    )
      switch( cbCrossType->ItemIndex )
      {
        case nTableNormalCrossType: FObjRules->AV["crosstype"] = "normal"; break;
        case nTableTransCrossType: FObjRules->AV["crosstype"]  = "trans" ; break;
      }
    return false;
  }

  __DELETE_OBJ( FObjRules )
  FObjRules = new TTagNode;

  if      ( ndThisObj->CmpName("table") )
  {
    FObjRules->Name = "tabrules";
    FObjRules->AV["thisref"]     = ndThisObj->AV["uid"];
    FObjRules->AV["thisrefmain"] = "";
    FObjRules->AV["ref"]         = FIntegrSpec->GetChildByName( "passport" )->AV["GUI"] +
                                       "." +
                                       ndRefObj->AV["uid"];
    FObjRules->AV["refmain"] = "";
    switch( cbCrossType->ItemIndex )
    {
      case nTableNormalCrossType: FObjRules->AV["crosstype"] = "normal"; break;
      case nTableTransCrossType : FObjRules->AV["crosstype"] = "trans"; break;
    }
    FObjRules->AddChild( "colrules" );
    FObjRules->AddChild( "rowrules" );
  }
  else if ( ndThisObj->CmpName("inscription") )
  {
    throw DKClasses::EMethodError(
      __FUNC__,
      FMT(icsDocRulesEditVersionLimit)
    );
  }
  else if ( ndThisObj->CmpName("list") )
  {
    throw DKClasses::EMethodError(
      __FUNC__,
      FMT(icsDocRulesEditVersionLimit)
    );
  }
  return true;
}

//---------------------------------------------------------------------------

bool __fastcall TfmObjRulesEdit::AreInputsCorrect()
{
  TTagNode *ndThisObj = GetThisObj();

  /* TODO -okdv -cInsListRules : 
���� ��� ���� ��������, ����� ����� ��������� ���������
insrules � listrules */
  if ( !ndThisObj->CmpName( "table" ) )
  {
    _MSG_INF(FMT(icsDocRulesEditVersionLimit), TICSDocSpec::ICSName);
    return false;
  }

  return true;
}

//***************************************************************************
//********************** ����������� �������*********************************
//***************************************************************************

void __fastcall TfmObjRulesEdit::actOkExecute(TObject *Sender)
{
  if ( !CheckDKFilterControls( bOk ) )	        //��� ����������� �� ������ DKComp,
    return;					//������������ TDKControlFilter

  if ( !AreInputsCorrect() )
    return;

  CreateObjRules();
      
  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TfmObjRulesEdit::bCancelClick(TObject *Sender)
{
  __DELETE_OBJ( FObjRules )
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TfmObjRulesEdit::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}

//---------------------------------------------------------------------------

void __fastcall TfmObjRulesEdit::lbxThisObjSelect(TObject *Sender,
      int ListItemIndex)
{
  if ( ListItemIndex == -1 )
  {
    UpdateActionsEnabled();
    return;
  }
  TTagNode *ndThisObj = GetThisObj();

  //�������� ������ ������������� ��������
  lbxRefObj->DisableOnSelect();
  lbxRefObj->Items->BeginUpdate();
  lbxRefObj->Clear();
  TTagNode *ndRefObj = FIntegrSpec->GetChildByName( "doccontent" )->GetFirstChild();
  while ( ndRefObj )
  {
    if ( ndRefObj->CmpName(ndThisObj->Name))
    {
      UnicodeString ViewText;
      if ( FOnGetDocElementViewText )
        FOnGetDocElementViewText( this, ndRefObj, ViewText );
      else
        ViewText = "???";
      lbxRefObj->AddItem( ViewText, ndRefObj );
    }
    ndRefObj = ndRefObj->GetNext();
  }
  lbxRefObj->Items->EndUpdate();
  lbxRefObj->EnableOnSelect();
  lbxRefObj->First();

  UpdateActionsEnabled();
  /* TODO -okdv -cInsListRules : ���� �������� �������������� ������� insrules � listrules */
  if      ( ndThisObj->CmpName("inscription") ) pInsRulesProps->BringToFront();
  else if ( ndThisObj->CmpName("list")        ) pListRulesProps->BringToFront();
  else if ( ndThisObj->CmpName("table")       ) pTabRulesProps->BringToFront();
}

//---------------------------------------------------------------------------

