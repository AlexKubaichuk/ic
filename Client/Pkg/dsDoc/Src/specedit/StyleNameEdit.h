/*
  ����        - StyleNameEdit.h
  ������      - ��������������� (ICS)
  ��������    - ���� ���������� ������, ����������� �������� �����
                ��� ����� ������������ �����
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 22.09.2004
*/
//---------------------------------------------------------------------------

/*
  ������� ���������:

  16.03.2006
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  27.01.2006
    [+] ��������� �������� TICSDocSpec::ReqEdBkColor
    [*] �������� ������������ ��������� ����������

  22.09.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef StyleNameEditH
#define StyleNameEditH

//---------------------------------------------------------------------------

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
//#include "ICSDoc.h"  //��������� �������� � ���� ��������
#include "DKUtils.h"
//---------------------------------------------------------------------------

namespace StyleNameEdit
{

//***************************************************************************
//********************** ������� ********************************************
//***************************************************************************

//***************************************************************************
//********************** ���� ***********************************************
//***************************************************************************

class TfmStyleEdit;

typedef bool __fastcall (__closure *TValidateEvent)( TfmStyleEdit *Sender );

//***************************************************************************
//********************** ��������� ���������� ������� ***********************
//***************************************************************************

//---------------------------------------------------------------------------

//����� ��� ����� ������������ �����

class PACKAGE TfmStyleEdit : public TForm
{
__published:	// IDE-managed Components
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
        TGroupBox *GroupBox1;
        TLabel *StyleNameLab;
  TcxMaskEdit *edStyleName;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bOkClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);


private:	// User declarations
        UnicodeString FStyleName;
        TFormUpdateMode FFUM;
        TValidateEvent FOnValidate;

        bool __fastcall AreInputsCorrect();


protected:
        TColor __fastcall GetReqEdBkColor() const;
        void __fastcall SetStyleName( UnicodeString  AValue );

        
public:		// User declarations
        __fastcall TfmStyleEdit(TComponent* Owner, TFormUpdateMode AFUM);
        __property UnicodeString StyleName = { read = FStyleName, write = SetStyleName };
        __property TFormUpdateMode FUM = { read = FFUM };
        __property TValidateEvent OnValidate = { read = FOnValidate, write = FOnValidate };
};

//---------------------------------------------------------------------------

} //end of namespace StyleNameEdit
using namespace StyleNameEdit;

#endif
