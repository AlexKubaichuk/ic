/*
  ����        - ParametersEdit.cpp
  ������      - ��������������� (ICS)
  ��������    - ������ �������������� ���������� �������������.
                ���� ����������
                (������������ ����������� TICSDocSpec)
  ����������� - ������� �.�.
  ����        - 20.09.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "ParametersEdit.h"
#include "ICSDocSpec.h"
//#include "DKMBUtils.h"
#include "msgdef.h"
#include "DKRusMB.h"
#include "DKUtils.h"
#include "ICSTL.h"
#include <functional>
#include <cderr.h>
#include "SEPageSetup.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxButtonEdit"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxSpinEdit"
#pragma link "cxTextEdit"
#pragma link "DKDialogs"
#pragma link "DKListBox"
#pragma link "cxDropDownEdit"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                                 Globals                               ##
//##                                                                       ##
//###########################################################################

namespace ParametersEdit
{

UINT_PTR CALLBACK PaintHook( HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
  LPRECT lprc;
  COLORREF crMargRect;
  HDC hdc, hdcOld;

  switch (uMsg)
  {
    // Drawing the margin rectangle.
    case WM_PSD_MARGINRECT:
      hdc = (HDC) wParam;
      lprc = (LPRECT) lParam;

      // Get the system highlight color.
      crMargRect = GetSysColor(COLOR_HIGHLIGHT);

      // Create a dash-dot pen of the system highlight color and
      // select it into the DC of the sample page.
      hdcOld = (HDC)SelectObject(hdc, CreatePen(PS_DASHDOT, .5, crMargRect));

      // Draw the margin rectangle.
      Rectangle(hdc, lprc->left, lprc->top, lprc->right, lprc->bottom);

      // Restore the previous pen to the DC.
      SelectObject(hdc, hdcOld);
    return TRUE;

    default:
    return FALSE;
  }
}

//---------------------------------------------------------------------------

#define MM_FROM_HMM( AttrVal ) FloatToStr ( StrToFloat( AttrVal ) / 100. )

//---------------------------------------------------------------------------

enum TImageListInd { iiDefBoth = 0, iiDefH, iiDefB, iiBold, iiItalic, iiUnderline, iiStrike };

} //end of namespace ParametersEdit

//###########################################################################
//##                                                                       ##
//##                             TfmParametersEdit                         ##
//##                                                                       ##
//###########################################################################

//***************************************************************************
//**                    Initialization / Finalization                      **
//***************************************************************************

__fastcall TfmParametersEdit::TfmParametersEdit(TComponent* Owner, TTagNode* APassport, TTagNode* ACondRules, TTagNode* ADocRules, TTagNode* ASpec) :
  TForm(Owner),
  csValueSet(cFMT(icsDocSpecNotEmptyEllipsiseEditor)),
  csValueUnset(cFMT(icsDocSpecEmptyEllipsiseEditor))
{
  Caption = FMT(icsDocParametersEditfmParametersEditCaption);
  bApply->Caption = FMT(icsDocParametersEditbApplyBtnCaption);
  bCancel->Caption = FMT(icsDocParametersEditbCancelBtnCaption);
  bHelp->Caption = FMT(icsDocParametersEditbHelpBtnCaption);
  tsCommon->Caption = FMT(icsDocParametersEdittsCommonTSCaption);
  SpecNameLab->Caption = FMT(icsDocParametersEditSpecNameLabLabCaption);
  Label3->Caption = FMT(icsDocParametersEditLabel3LabCaption);
  Label4->Caption = FMT(icsDocParametersEditLabel4LabCaption);
  Label5->Caption = FMT(icsDocParametersEditLabel5LabCaption);
  Label6->Caption = FMT(icsDocParametersEditLabel6LabCaption);
  lbTimeStamp->Caption = FMT(icsDocParametersEditlbTimeStampLabCaption);
  lbAutor->Caption = FMT(icsDocParametersEditlbAutorLabCaption);
  CondRulesLb->Caption = FMT(icsDocParametersEditCondRulesLbLabCaption);
  DocRulesLb->Caption = FMT(icsDocParametersEditDocRulesLbLabCaption);
  GroupBox1->Caption = FMT(icsDocParametersEditGroupBox1GBCaption);
  chbPersonal->Caption = FMT(icsDocParametersEditchbPersonalCHBCaption);
  chbGroup->Caption = FMT(icsDocParametersEditchbGroupCHBCaption);
//---  PeriodDefChBx->Caption = FMT(icsDocParametersEditPeriodDefChBxCHBCaption);
  SaveBDChBx->Caption = FMT(icsDocParametersEditSaveBDChBxCHBCaption);
  tsPageSetup->Caption = FMT(icsDocParametersEdittsPageSetupTSCaption);
  Label8->Caption = FMT(icsDocParametersEditLabel8LabCaption);
  Label9->Caption = FMT(icsDocParametersEditLabel9LabCaption);
  Label10->Caption = FMT(icsDocParametersEditLabel10LabCaption);
  Label11->Caption = FMT(icsDocParametersEditLabel11LabCaption);
  lbOrientation->Caption = FMT(icsDocParametersEditlbOrientationLabCaption);
  Label13->Caption = FMT(icsDocParametersEditLabel13LabCaption);
  Label14->Caption = FMT(icsDocParametersEditLabel14LabCaption);
  Label15->Caption = FMT(icsDocParametersEditLabel15LabCaption);
  Label16->Caption = FMT(icsDocParametersEditLabel16LabCaption);
  lbPageSize->Caption = FMT(icsDocParametersEditlbPageSizeLabCaption);
  lbLeftMargin->Caption = FMT(icsDocParametersEditlbLeftMarginLabCaption);
  lbTopMargin->Caption = FMT(icsDocParametersEditlbTopMarginLabCaption);
  lbRightMargin->Caption = FMT(icsDocParametersEditlbRightMarginLabCaption);
  lbBottomMargin->Caption = FMT(icsDocParametersEditlbBottomMarginLabCaption);
  bChangePageSetup->Caption = FMT(icsDocParametersEditbChangePageSetupBtnCaption);
  tsStyles->Caption = FMT(icsDocParametersEdittsStylesTSCaption);
  Label1->Caption = FMT(icsDocParametersEditLabel1LabCaption);
  Label22->Caption = FMT(icsDocParametersEditLabel22LabCaption);
  lbFontInfo->Caption = FMT(icsDocParametersEditlbFontInfoLabCaption);
  chbUseBDef->Caption = FMT(icsDocParametersEditchbUseBDefCHBCaption);
  chbUseHDef->Caption = FMT(icsDocParametersEditchbUseHDefCHBCaption);
  bAddStyle->Caption = FMT(icsDocParametersEditbAddStyleBtnCaption);
  bDeleteStyle->Caption = FMT(icsDocParametersEditbDeleteStyleBtnCaption);
  chbWordWrap->Caption = FMT(icsDocParametersEditchbWordWrapCHBCaption);
  bChangeFont->Caption = FMT(icsDocParametersEditbChangeFontBtnCaption);
  Button1->Caption = FMT(icsDocParametersEditButton1BtnCaption);
  actAddStyle->Caption = FMT(icsDocParametersEditactAddStyleActCaption);
  actEditStyle->Caption = FMT(icsDocParametersEditactEditStyleActCaption);
  actDeleteStyle->Caption = FMT(icsDocParametersEditactDeleteStyleActCaption);
  if (!Owner->InheritsFrom(__classid(TICSDocSpec)))
    throw DKClasses::EInvalidArgument(
      __FUNC__,
      "Owner",
      FMT(icsDocErrorCreateOwnerNotValid)
    );
  if (!APassport)
    throw DKClasses::ENullArgument(__FUNC__, "APassport");
  if (!ASpec)
    throw DKClasses::ENullArgument(__FUNC__, "ASpec");
  FPassport = new TTagNode;
  FPassport->Assign( APassport, true );
  if ( ACondRules )
  {
    FCondRules = new TTagNode;
    FCondRules->Assign( ACondRules, true );
  }
  else
    FCondRules = NULL;
  if ( ADocRules )
  {
    FDocRules = new TTagNode;
    FDocRules->Assign( ADocRules, true );
  }
  else
    FDocRules = NULL;
  FSpec = ASpec;
  FNewStylesCount = 0;
  FDocByDocIcon = NULL;
  FOnGetSpecOwner  = NULL;
  FOnEditCondRules = NULL;
  FOnCheckStyleExistence = NULL;
  FOnGetSpecsList = NULL;
  FOnGetOrgsList  = NULL;
  FOnGetSpecNode  = NULL;
  FOnGetDocElementsList  = NULL;
  FOnGetThisDocElementStruct = NULL;
  FOnGetRefDocElementStruct  = NULL;
  FOnGetDocElementViewText   = NULL;
  FOnGetDocElementStruct     = NULL;
}

//---------------------------------------------------------------------------

__fastcall TfmParametersEdit::~TfmParametersEdit()
{
  delete FPassport;
  __DELETE_OBJ(FCondRules)
  __DELETE_OBJ(FDocRules)  
}

//---------------------------------------------------------------------------
UnicodeString __fastcall TfmParametersEdit::PeriodIdxToId(int AIdx)
{
  UnicodeString RC = "0";
  try
   {
     switch (AIdx)
      {
        //  <choicevalue name='���' value='0'/>
        case 0: RC = "0"; break;
        //  <choicevalue name='�����' value='2'/>
        case 1: RC = "2"; break;
        //  <choicevalue name='���' value='5'/>
        case 2: RC = "5"; break;
        //  <choicevalue name='����������� ��� ������������' value='1'/>
        case 3: RC = "1"; break;
        //  <choicevalue name='���������' value='4'/>
        case 4: RC = "4"; break;
        //  <choicevalue name='�������' value='3'/>
        case 5: RC = "3"; break;
        default : RC = "0";
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
int __fastcall TfmParametersEdit::PeriodIdToIdx(UnicodeString AIdx)
{
  int RC = -1;
  try
   {
     switch (AIdx.ToIntDef(-1))
      {
        //  <choicevalue name='���' value='0'/>
        case 0: RC = 0; break;
        //  <choicevalue name='�����' value='2'/>
        case 2: RC = 1; break;
        //  <choicevalue name='���' value='5'/>
        case 5: RC = 2; break;
        //  <choicevalue name='����������� ��� ������������' value='1'/>
        case 1: RC = 3; break;
        //  <choicevalue name='���������' value='4'/>
        case 4: RC = 4; break;
        //  <choicevalue name='�������' value='3'/>
        case 3: RC = 5; break;
        default : RC = -1;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TfmParametersEdit::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif

  PageControl->ActivePage = tsCommon;
  edTitle->SetFocus();

  edTitle->Style->Color     = GetReqEdBkColor();
  edVersion->Style->Color   = GetReqEdBkColor();
  edRelease->Style->Color   = GetReqEdBkColor();
  PeriodDefCB->Style->Color = GetReqEdBkColor();

  // passport
  edTitle->Text    = FPassport->AV["mainname"];
  UnicodeString Autor = "";
  if ( FOnGetSpecOwner )
    FOnGetSpecOwner( this, FPassport->AV["autor"], Autor );
  lbAutor->Caption = Autor;
  edVersion->Value = StrToInt( FPassport->AV["version"] );
  edRelease->Value = StrToInt( FPassport->AV["release"] );


  PeriodDefCB->ItemIndex = PeriodIdToIdx(FPassport->AV["perioddef"]);
  SaveBDChBx->Checked    = FPassport->CmpAV("save_bd",   "1");

  UnicodeString OrientationStr = FPassport->AV["orientation"];
  if      ( OrientationStr == "personal" )
    chbPersonal->Checked = true;
  else if ( OrientationStr == "group" )
    chbGroup->Checked = true;
  else
  {
    chbPersonal->Checked = true;
    chbGroup->Checked    = true;
  }

  UnicodeString DTStr = FPassport->AV["timestamp"];
  lbTimeStamp->Caption = DTStr.SubString( 7, 2 ) + "." + DTStr.SubString( 5, 2 )  + "." + DTStr.SubString( 1, 4 ) +
                         "  " +
                         DTStr.SubString( 9, 2 ) + ":" + DTStr.SubString( 11, 2 ) + ":" + DTStr.SubString( 13, 2 );

  // condrules
  UpdateCondRulesEdit();

  // docrules
  UpdateDocRulesEdit();
  // �������� ������ ������������ ��������� �� ��������� �������� ������ ����
  // ������������ �������� �������� ���� �� ����� �������
  if (!FSpec->GetChildByName("doccontent")->GetCount(false, "table"))
  {
    DocRulesLb->Enabled = false;
    DocRulesEd->Enabled = false;
//    DocRulesEd->Color = clBtnFace;
  }

  // pagesetup
  UpdatePageSetupControls();

  // styles
  TTagNode* ndStyles = FPassport->GetChildByName( "styles", false );
  TTagNode* ndNode   = ndStyles->GetFirstChild();
  lbxStyles->DisableOnSelect();
  while( ndNode )
  {
    lbxStyles->AddItem( ndNode->AV["name"], ndNode );
    ndNode = ndNode->GetNext();
  }
  lbxStyles->EnableOnSelect();
  lbxStyles->First();
  actDeleteStyle->Enabled = ( lbxStyles->Items->Count > 1 );

  //------
  bApply->Enabled = false;
}

//***************************************************************************
//**                               Methods                                 **
//***************************************************************************

TColor __fastcall TfmParametersEdit::GetReqEdBkColor() const
{
  return dynamic_cast<TICSDocSpec*>(Owner)->ReqEdBkColor;
}

//---------------------------------------------------------------------------

bool __fastcall TfmParametersEdit::AreInputsCorrect()
{
  if ( edTitle->Text == "" )
  {
    PageControl->ActivePage = tsCommon;
    edTitle->SetFocus();
    _MSG_ERR(FMT1(icsDocErrorReqFieldCaption,FMT(icsDocParametersEditSpecName)), FMT(icsDocErrorInputErrorCaption));
    return false;
  }
 else if (PeriodDefCB->ItemIndex == -1)
  {
    PageControl->ActivePage = tsCommon;
    PeriodDefCB->SetFocus();
    _MSG_ERR(FMT1(icsDocErrorReqFieldCaption, "�������������"), FMT(icsDocErrorInputErrorCaption));
    return false;
  }

  return true;
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::UpdateRulesEdit(TTagNode *ndRules, TcxButtonEdit *AEdit)
{
  AEdit->Text = (ndRules) ? csValueSet : csValueUnset;
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::UpdateCondRulesEdit()
{
  UpdateRulesEdit(FCondRules, CondRulesEd);
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::UpdateDocRulesEdit()
{
  UpdateRulesEdit(FDocRules, DocRulesEd);
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::UpdatePageSetupControls()
{
  TTagNode* ndPageSetup = FPassport->GetChildByName( "pagesetup", false );
  lbPageSize->Caption = MM_FROM_HMM( ndPageSetup->AV["width"] ) +
                        " x " +
                        MM_FROM_HMM( ndPageSetup->AV["height"] ) +
                        " ��";
  lbOrientation->Caption = ndPageSetup->CmpAV( "orientation", "0" )
                           ? cFMT(icsDocParametersEditOrientation1)
                           : cFMT(icsDocParametersEditOrientation2);

  lbLeftMargin->Caption   = MM_FROM_HMM( ndPageSetup->AV["leftfield" ] );
  lbTopMargin->Caption    = MM_FROM_HMM( ndPageSetup->AV["topfield"] );
  lbRightMargin->Caption  = MM_FROM_HMM( ndPageSetup->AV["rightfield" ] );
  lbBottomMargin->Caption = MM_FROM_HMM( ndPageSetup->AV["bottomfield"] );
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::UpdateFontControls( TTagNode* ndNode )
{
  lbFontInfo->Caption = ndNode->AV["fontname"] + ",  " + ndNode->AV["fontsize"];

  pbStrike->Tag    = StrToInt( ndNode->AV["strike"] );
  pbStrike->Repaint();
  pbUnderline->Tag = StrToInt( ndNode->AV["underline"] );
  pbUnderline->Repaint();
  pbBold->Tag      = StrToInt( ndNode->AV["bold"] );
  pbBold->Repaint();
  pbItalic->Tag    = StrToInt( ndNode->AV["italic"] );
  pbItalic->Repaint();
}

//---------------------------------------------------------------------------

bool __fastcall TfmParametersEdit::ValidateStyleEditData( TfmStyleEdit *Sender )
{
  int nNewStyleNameInd = lbxStyles->Items->IndexOf( Sender->StyleName );

  bool bResult = ( ( nNewStyleNameInd == -1 ) ||
                   ( ( Sender->FUM == fumEDIT ) && ( nNewStyleNameInd == lbxStyles->ItemIndex ) ) );
  if ( !bResult )
    _MSG_ERR(FMT1(icsDocParametersEditStyleExists,Sender->StyleName), FMT(icsDocErrorInputErrorCaption));
  return bResult;
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::SetCurStyleAlign( UnicodeString  AValue )
{
  TTagNode* ndNode = dynamic_cast<TTagNode*>( LBX_CUR_OBJECT(lbxStyles) );
  ndNode->AV["align"] = AValue;
  bApply->Enabled = true;
}

//---------------------------------------------------------------------------

bool __fastcall TfmParametersEdit::ChangeDefStyle( TCheckBox* Sender, UnicodeString  DefStyleAttrName )
{
  TTagNode* ndNode = dynamic_cast<TTagNode*>( LBX_CUR_OBJECT(lbxStyles) );
  UnicodeString DefStyleUID = ndNode->GetParent()->AV[DefStyleAttrName];
  UnicodeString CurNodeUID = ndNode->AV["uid"];
  if ( DefStyleUID == CurNodeUID )
  {
    Sender->Checked = true;
    return false;
  }
  else
  {
    ndNode->GetParent()->AV[DefStyleAttrName] = CurNodeUID;
    int nPrevDefInd = lbxStyles->Items->IndexOfObject( FPassport->GetTagByUID( DefStyleUID ) );
    int nCurDefInd  = lbxStyles->ItemIndex;
    TRect rect;
    rect = lbxStyles->ItemRect( nCurDefInd );
    InvalidateRect( lbxStyles->Handle, &rect, true);
    UpdateWindow( lbxStyles->Handle );
    rect = lbxStyles->ItemRect( nPrevDefInd );
    InvalidateRect( lbxStyles->Handle, &rect, true);
    UpdateWindow( lbxStyles->Handle );
    return true;
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::ParametersEditGetSpecMainName( TObject *Sender, UnicodeString &SpecMainName )
{
  SpecMainName = edTitle->Text;
}

//***************************************************************************
//**                            Event handlers                             **
//***************************************************************************

void __fastcall TfmParametersEdit::bApplyClick(TObject *Sender)
{
  if ( !CheckDKFilterControls( Sender ) )       //��� ����������� �� ������ DKComp,
    return;                                     //������������ TDKControlFilter

  if ( !AreInputsCorrect() )
    return;

  FPassport->AV["mainname"]  = edTitle->Text;
  FPassport->AV["version"]   = IntToStr((int)edVersion->Value);
  FPassport->AV["release"]   = IntToStr((int)edRelease->Value);
  FPassport->AV["save_bd"]   = (SaveBDChBx->Checked) ? "1" : "0";
  FPassport->AV["perioddef"] = PeriodIdxToId(PeriodDefCB->ItemIndex);

  if      ( chbPersonal->Checked && chbGroup->Checked )
    FPassport->AV["orientation"] = "both";
  else if ( chbPersonal->Checked )
    FPassport->AV["orientation"] = "personal";
  else
    FPassport->AV["orientation"] = "group";

  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}

//---------------------------------------------------------------------------



void __fastcall TfmParametersEdit::lbxStylesSelect(TObject *Sender,
      int ListItemIndex)
{
  TTagNode* ndNode = dynamic_cast<TTagNode*>( lbxStyles->Items->Objects[ListItemIndex] );
  UpdateFontControls( ndNode );

  UnicodeString AlignVal = ndNode->AV["align"];
  if      ( AlignVal == "left" )
    sbAlignLeft->Down = true;
  else if ( AlignVal == "center" )
    sbAlignCenter->Down = true;
  else if ( AlignVal == "right" )
    sbAlignRight->Down = true;
  else if ( AlignVal == "justify" )
    sbAlignJustify->Down = true;

  SET_CHB_CHECKED_ONLY( chbWordWrap, ndNode->CmpAV( "wordwrap",  "1" ) );
  SET_CHB_CHECKED_ONLY( chbUseBDef , ndNode->GetParent()->CmpAV( "defaultbstyle", ndNode->AV["uid"] ) );
  SET_CHB_CHECKED_ONLY( chbUseHDef , ndNode->GetParent()->CmpAV( "defaulthstyle", ndNode->AV["uid"] ) );
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::lbxStylesDrawListItemImage(
      TObject *Sender, int ListItemIndex, int &ItemImageIndex)
{
  TTagNode* ndNode = dynamic_cast<TTagNode*>( lbxStyles->Items->Objects[ListItemIndex] );

  UnicodeString DefHUID  = ndNode->GetParent()->AV["defaulthstyle"];
  UnicodeString DefBUID  = ndNode->GetParent()->AV["defaultbstyle"];
  UnicodeString StyleUID = ndNode->AV["uid"];

  if      ( ( DefHUID == StyleUID ) && ( DefBUID == StyleUID ) )
    ItemImageIndex = iiDefBoth;
  else if ( DefHUID == StyleUID )
    ItemImageIndex = iiDefH;
  else if ( DefBUID == StyleUID )
    ItemImageIndex = iiDefB;
  else
    ItemImageIndex = -1;
}
//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::EditorChange(TObject *Sender)
{
  bApply->Enabled = true;
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::bChangePageSetupClick(TObject *Sender)
{
//  PrinterSetupDlg->Execute();
  TSEPageSetupForm  *Dlg = new TSEPageSetupForm(this);
  try
   {
     TTagNode* ndPageSetup = FPassport->GetChildByName( "pagesetup", false );

     UnicodeString pW = "0";
     UnicodeString pH = "0";
     if (ndPageSetup->AV["orientation"].ToIntDef(0))
      { // ���������
        pW = ndPageSetup->AV["height"];
        pH = ndPageSetup->AV["width"];
        Dlg->landRB->Checked = true;
      }
     else
      { // ����������
        pW = ndPageSetup->AV["width"];
        pH = ndPageSetup->AV["height"];
        Dlg->portRB->Checked = true;
      }

     Dlg->TopMargED->Value    = ndPageSetup->AV["topfield"].ToIntDef(10)/100;
     Dlg->LeftMargED->Value   = ndPageSetup->AV["leftfield"].ToIntDef(10)/100;
     Dlg->RightMargED->Value  = ndPageSetup->AV["rightfield"].ToIntDef(10)/100;
     Dlg->BottomMargED->Value = ndPageSetup->AV["bottomfield"].ToIntDef(10)/100;

     if (!ndPageSetup->AV["prk"].Length()) ndPageSetup->AV["prk"] = "1";
     Dlg->PrKoeffSBE->Value = StrToFloat(ndPageSetup->AV["prk"]);
     Dlg->ShowModal();
     if (Dlg->ModalResult == mrOk)
      {
        ndPageSetup->AV["topfield"]    = IntToStr(((int)Dlg->TopMargED->Value)*100);
        ndPageSetup->AV["leftfield"]   = IntToStr(((int)Dlg->LeftMargED->Value)*100);
        ndPageSetup->AV["rightfield"]  = IntToStr(((int)Dlg->RightMargED->Value)*100);
        ndPageSetup->AV["bottomfield"] = IntToStr(((int)Dlg->BottomMargED->Value)*100);

        ndPageSetup->AV["prk"] =  FloatToStr((double)Dlg->PrKoeffSBE->Value);

        if (Dlg->portRB->Checked)
         {
           ndPageSetup->AV["orientation"] = "0";
           ndPageSetup->AV["width"] = pW;
           ndPageSetup->AV["height"] = pH;
         }
        else
         {
           ndPageSetup->AV["orientation"] = "1";
           ndPageSetup->AV["width"] = pH;
           ndPageSetup->AV["height"] = pW;
         }
      }
     UpdatePageSetupControls();
     bApply->Enabled = true;
   }
  __finally
   {
     delete Dlg;
   }
/*
     // Initialize DEVMODE ( contains information about the device initialization and environment of a printer )
     HGLOBAL hDM = GlobalAlloc( GHND, sizeof(DEVMODE) );
     DEVMODE* pDM = (DEVMODE*)GlobalLock( hDM );
     pDM->dmSize = sizeof(DEVMODE);
     pDM->dmFields = DM_ORIENTATION | DM_PAPERLENGTH | DM_PAPERWIDTH;
     pDM->dmOrientation = ndPageSetup->CmpAV( "orientation", "0" ) ? DMORIENT_PORTRAIT : DMORIENT_LANDSCAPE;
     pDM->dmPaperLength = StrToFloat( ndPageSetup->AV["height"] ) / 10.;
     pDM->dmPaperWidth  = StrToFloat( ndPageSetup->AV["width"] ) / 10.;
     GlobalUnlock( hDM );

     // Initialize PAGESETUPDLG ( common dialog box structure )
     PAGESETUPDLG psd;
     ZeroMemory(&psd, sizeof(PAGESETUPDLG));
     psd.lStructSize = sizeof(PAGESETUPDLG);
     psd.hwndOwner   = Handle;
     psd.hDevMode    = hDM;  // Don't forget to free or store hDevMode
     psd.hDevNames   = NULL; // Don't forget to free or store hDevNames
     psd.Flags       = PSD_INHUNDREDTHSOFMILLIMETERS | PSD_MARGINS | PSD_NOWARNING | PSD_MINMARGINS |
                       PSD_ENABLEPAGEPAINTHOOK | PSD_DISABLEPRINTER | PSD_ENABLEPAGESETUPTEMPLATE;
     psd.rtMinMargin.top    = 0;
     psd.rtMinMargin.left   = 0;
     psd.rtMinMargin.right  = 0;
     psd.rtMinMargin.bottom = 0;
     psd.rtMargin.top    = StrToInt( ndPageSetup->AV["topfield"] );
     psd.rtMargin.left   = StrToInt( ndPageSetup->AV["leftfield"] );
     psd.rtMargin.right  = StrToInt( ndPageSetup->AV["rightfield"] );
     psd.rtMargin.bottom = StrToInt( ndPageSetup->AV["bottomfield"] );
     psd.hInstance = HInstance;
     psd.lpfnPagePaintHook = PaintHook;
     psd.lpPageSetupTemplateName = L"PAGESETUPDLGORD";
     if ( PageSetupDlg(&psd) )
     {
       ndPageSetup->AV["topfield"]    = IntToStr((int) psd.rtMargin.top    );
       ndPageSetup->AV["leftfield"]   = IntToStr((int) psd.rtMargin.left   );
       ndPageSetup->AV["rightfield"]  = IntToStr((int) psd.rtMargin.right  );
       ndPageSetup->AV["bottomfield"] = IntToStr((int) psd.rtMargin.bottom );

       pDM = (DEVMODE*)GlobalLock( hDM );
       ndPageSetup->AV["orientation"] = ( pDM->dmOrientation == DMORIENT_PORTRAIT ) ? "0" : "1";
       ndPageSetup->AV["width"]  = IntToStr( ( pDM->dmOrientation == DMORIENT_PORTRAIT ) ? (int)psd.ptPaperSize.x : (int)psd.ptPaperSize.y );
       ndPageSetup->AV["height"] = IntToStr( ( pDM->dmOrientation == DMORIENT_PORTRAIT ) ? (int)psd.ptPaperSize.y : (int)psd.ptPaperSize.x );
       GlobalUnlock( hDM );

       UpdatePageSetupControls();
       bApply->Enabled = true;
     }
     else
       if ( DWORD dwErrCode = CommDlgExtendedError() )     //PageSetupDlg fails
       {
         UnicodeString Msg;
         switch ( dwErrCode )
         {
           case CDERR_DIALOGFAILURE   : Msg = FMT(icsDocParametersEditCDERR_DIALOGFAILURE); break;
           case CDERR_FINDRESFAILURE  : Msg = FMT(icsDocParametersEditCDERR_FINDRESFAILURE); break;
           case CDERR_INITIALIZATION  : Msg = FMT(icsDocParametersEditCDERR_INITIALIZATION); break;
           case CDERR_LOADRESFAILURE  : Msg = FMT(icsDocParametersEditCDERR_LOADRESFAILURE); break;
           case CDERR_LOADSTRFAILURE  : Msg = FMT(icsDocParametersEditCDERR_LOADSTRFAILURE); break;
           case CDERR_LOCKRESFAILURE  : Msg = FMT(icsDocParametersEditCDERR_LOCKRESFAILURE); break;
           case CDERR_MEMALLOCFAILURE : Msg = FMT(icsDocParametersEditCDERR_MEMALLOCFAILURE); break;
           case CDERR_MEMLOCKFAILURE  : Msg = FMT(icsDocParametersEditCDERR_MEMLOCKFAILURE); break;
           case CDERR_NOHINSTANCE     : Msg = FMT(icsDocParametersEditCDERR_NOHINSTANCE); break;
           case CDERR_NOHOOK          : Msg = FMT(icsDocParametersEditCDERR_NOHOOK); break;
           case CDERR_NOTEMPLATE      : Msg = FMT(icsDocParametersEditCDERR_NOTEMPLATE); break;
           case CDERR_REGISTERMSGFAIL : Msg = FMT(icsDocParametersEditCDERR_REGISTERMSGFAIL); break;
           case CDERR_STRUCTSIZE      : Msg = FMT(icsDocParametersEditCDERR_STRUCTSIZE); break;
           case PDERR_CREATEICFAILURE : Msg = FMT(icsDocParametersEditPDERR_CREATEICFAILURE); break;
           case PDERR_DEFAULTDIFFERENT: Msg = FMT(icsDocParametersEditPDERR_DEFAULTDIFFERENT); break;
           case PDERR_DNDMMISMATCH    : Msg = FMT(icsDocParametersEditPDERR_DNDMMISMATCH); break;
           case PDERR_GETDEVMODEFAIL  : Msg = FMT(icsDocParametersEditPDERR_GETDEVMODEFAIL); break;
           case PDERR_INITFAILURE     : Msg = FMT(icsDocParametersEditPDERR_INITFAILURE); break;
           case PDERR_LOADDRVFAILURE  : Msg = FMT(icsDocParametersEditPDERR_LOADDRVFAILURE); break;
           case PDERR_NODEFAULTPRN    : Msg = FMT(icsDocParametersEditPDERR_NODEFAULTPRN); break;
           case PDERR_NODEVICES       : Msg = FMT(icsDocParametersEditPDERR_NODEVICES); break;
           case PDERR_PARSEFAILURE    : Msg = FMT(icsDocParametersEditPDERR_PARSEFAILURE); break;
           case PDERR_PRINTERNOTFOUND : Msg = FMT(icsDocParametersEditPDERR_PRINTERNOTFOUND); break;
           case PDERR_RETDEFFAILURE   : Msg = FMT(icsDocParametersEditPDERR_RETDEFFAILURE); break;
           case PDERR_SETUPFAILURE    : Msg = FMT(icsDocParametersEditPDERR_SETUPFAILURE); break;
         }
         TStringList* dopinfo = new TStringList;
         dopinfo->Text = FMT2(icsDocErrorCommonDop,IntToStr((int)dwErrCode),Msg);
         RMB_SHOW_ERROREX_CPT( cFMT(icsDocParametersEditErrorCreateDlg ), TICSDocSpec::ICSName.c_str(), dopinfo );
         delete dopinfo;

       }
     GlobalFree( hDM );
*/
}
//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::PaintBoxPaint(TObject *Sender)
{
  TPaintBox* pbSender = dynamic_cast<TPaintBox*>( Sender );
  bool bEnabled = static_cast<bool>(pbSender->Tag);
  int  nImgInd;
  if      ( pbSender->Name == "pbBold" )
    nImgInd = iiBold;
  else if ( pbSender->Name == "pbItalic" )
    nImgInd = iiItalic;
  else if ( pbSender->Name == "pbUnderline" )
    nImgInd = iiUnderline;
  else if ( pbSender->Name == "pbStrike" )
    nImgInd = iiStrike;
  pbSender->Canvas->Brush->Color = clBtnFace;
  pbSender->Canvas->FillRect( Rect( 0, 0, pbSender->Width - 1, pbSender->Height - 1 ) );
  if ( bEnabled )
  {
    pbSender->Canvas->Pen->Color = clBtnShadow;
    pbSender->Canvas->MoveTo( 0, 0 );
    pbSender->Canvas->LineTo( pbSender->Width - 1 , 0 );
    pbSender->Canvas->MoveTo( 0, 0 );
    pbSender->Canvas->LineTo( 0, pbSender->Height - 1 );
    pbSender->Canvas->Pen->Color = clWhite;
    int nOffset = 0;
    for ( int i = 1; i < pbSender->Height - 1; i++ )
    {
      for( int j = 1; j < pbSender->Width - 1; j+=2 )
      {
        pbSender->Canvas->MoveTo( nOffset + j, i );
        pbSender->Canvas->LineTo( nOffset + j + 1, i );
      }
      nOffset ^= 1;
    }
    pbSender->Canvas->MoveTo( pbSender->Width - 1, pbSender->Height - 1 );
    pbSender->Canvas->LineTo( pbSender->Width - 1 , -1 );
    pbSender->Canvas->MoveTo( pbSender->Width - 1, pbSender->Height - 1 );
    pbSender->Canvas->LineTo( -1, pbSender->Height - 1 );
  }
  TBImageList->Draw( pbSender->Canvas, ( pbSender->Width - TBImageList->Width ) / 2, ( pbSender->Height - TBImageList->Height ) / 2, nImgInd, true );
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::actAddStyleExecute(TObject *Sender)
{
  TfmStyleEdit *dlg;
  try
  {
    dlg = new TfmStyleEdit( Owner, fumADD );
    if (dlg)
    {
      dlg->StyleName = FMT1(icsDocParametersEditNewStyle,IntToStr((int) ++FNewStylesCount ));
      dlg->OnValidate = ValidateStyleEditData;
      if ( dlg->ShowModal() == mrOk )
      {
        TTagNode* ndNode  = FPassport->GetChildByName("styles", false)->AddChild("style");
        ndNode->AV["name"]      = dlg->StyleName;
        ndNode->AV["uid"]       = FSpec->NewUID();
        ndNode->AV["align"]     = "left";
        ndNode->AV["fontname"]  = "Times New Roman";
        ndNode->AV["fontsize"]  = "12";
        ndNode->AV["wordwrap"]  = "1";
        ndNode->AV["strike"]    = "0";
        ndNode->AV["underline"] = "0";
        ndNode->AV["bold"]      = "0";
        ndNode->AV["italic"]    = "0";
        lbxStyles->AddItem( ndNode->AV["name"], ndNode );
        bApply->Enabled = true;
      }
    }
  }
  __finally
  {
    if (dlg) delete dlg;
  }
  actDeleteStyle->Enabled = ( lbxStyles->Items->Count > 1 );
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::actEditStyleExecute(TObject *Sender)
{
  TfmStyleEdit *dlg;
  try
  {
    dlg = new TfmStyleEdit( Owner, fumEDIT );
    if (dlg)
    {
      TTagNode* ndNode = dynamic_cast<TTagNode*>( LBX_CUR_OBJECT(lbxStyles) );
      dlg->StyleName = ndNode->AV["name"];
      dlg->OnValidate = ValidateStyleEditData;
      if ( dlg->ShowModal() == mrOk )
      {
        ndNode->AV["name"] = dlg->StyleName;
        lbxStyles->EditItem( dlg->StyleName );
        bApply->Enabled = true;
      }
    }
  }
  __finally
  {
    if (dlg) delete dlg;
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::actDeleteStyleExecute(TObject *Sender)
{
  TTagNode* ndNode = dynamic_cast<TTagNode*>( LBX_CUR_OBJECT(lbxStyles) );
  if ( FOnCheckStyleExistence )
    if ( FOnCheckStyleExistence( this, ndNode->AV["uid"] ) )
      if ( _MSG_QUE(FMT1(icsDocParametersEditDeleteStyleConf,ndNode->AV["name"]),
                                 TICSDocSpec::ICSName
           ) == mrNo
      )
        return;
  bool bDefH = ndNode->GetParent()->CmpAV( "defaulthstyle", ndNode->AV["uid"] );
  bool bDefB = ndNode->GetParent()->CmpAV( "defaultbstyle", ndNode->AV["uid"] );
  FDropedStyles.push_back( ndNode->AV["uid"] );
  delete ndNode;
  lbxStyles->DisableOnSelect();
  lbxStyles->DeleteSelected();
  ndNode = dynamic_cast<TTagNode*>( LBX_CUR_OBJECT(lbxStyles) );
  if ( bDefH )
    ndNode->GetParent()->AV["defaulthstyle"] = ndNode->AV["uid"];
  if ( bDefB )
    ndNode->GetParent()->AV["defaultbstyle"] = ndNode->AV["uid"];
  if ( bDefH || bDefB )
  {
    TRect rect = lbxStyles->ItemRect( lbxStyles->ItemIndex );
    InvalidateRect( lbxStyles->Handle, &rect, true);
    UpdateWindow( lbxStyles->Handle );
  }
  lbxStyles->EnableOnSelect();
  lbxStyles->CallOnSelect();  
  actDeleteStyle->Enabled = ( lbxStyles->Items->Count > 1 );
  bApply->Enabled = true;
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::sbAlignLeftClick(TObject *Sender)
{
  SetCurStyleAlign( "left" );
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::sbAlignCenterClick(TObject *Sender)
{
  SetCurStyleAlign( "center" );
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::sbAlignRightClick(TObject *Sender)
{
  SetCurStyleAlign( "right" );        
}
//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::sbAlignJustifyClick(TObject *Sender)
{
  SetCurStyleAlign( "justify" );
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::chbWordWrapClick(TObject *Sender)
{
  TTagNode* ndNode = dynamic_cast<TTagNode*>( LBX_CUR_OBJECT(lbxStyles) );
  ndNode->AV["wordwrap"] = IntToStr((int) chbWordWrap->Checked );
  bApply->Enabled = true;
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::chbUseBDefClick(TObject *Sender)
{
  if ( ChangeDefStyle( dynamic_cast<TCheckBox*>(Sender), "defaultbstyle" ) )
    bApply->Enabled = true;
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::chbUseHDefClick(TObject *Sender)
{
  if ( ChangeDefStyle( dynamic_cast<TCheckBox*>(Sender), "defaulthstyle" ) )
    bApply->Enabled = true;
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::bChangeFontClick(TObject *Sender)
{
  TTagNode* ndNode = dynamic_cast<TTagNode*>( LBX_CUR_OBJECT(lbxStyles) );
  FontDlg->Font->Name = ndNode->AV["fontname"];//DKFontDialog
  FontDlg->Font->Size = StrToInt( ndNode->AV["fontsize"] );
  TFontStyles FontStyles;
  if ( ndNode->CmpAV( "bold", "1" ) )
    FontStyles << fsBold;
  if ( ndNode->CmpAV( "italic", "1" ) )
    FontStyles << fsItalic;
  if ( ndNode->CmpAV( "underline", "1" ) )
    FontStyles << fsUnderline;
  if ( ndNode->CmpAV( "strike", "1" ) )
    FontStyles << fsStrikeOut;
  FontDlg->Font->Style = FontStyles;
  if ( FontDlg->Execute() )
  {
    ndNode->AV["fontname"]  = FontDlg->Font->Name;
    ndNode->AV["fontsize"]  = IntToStr((int) FontDlg->Font->Size );
    ndNode->AV["bold"]      = IntToStr((int) FontDlg->Font->Style.Contains( fsBold ) );
    ndNode->AV["italic"]    = IntToStr((int) FontDlg->Font->Style.Contains( fsItalic ) );
    ndNode->AV["underline"] = IntToStr((int) FontDlg->Font->Style.Contains( fsUnderline ) );
    ndNode->AV["strike"]    = IntToStr((int) FontDlg->Font->Style.Contains( fsStrikeOut ) );
    UpdateFontControls( ndNode );
    bApply->Enabled = true;
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::chbOrientationClick(TObject *Sender)
{
  if ( !chbPersonal->Checked && !chbGroup->Checked )
    dynamic_cast<TCheckBox*>(Sender)->Checked = true;
  bApply->Enabled = true;
}

//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::CondRulesEdPropertiesButtonClick(TObject *Sender,
          int AButtonIndex)
{
/*get filter dlg*/
  if (FOnEditCondRules)
    if (FOnEditCondRules(this, edTitle->Text, &FCondRules))
    {
      UpdateCondRulesEdit();
      bApply->Enabled = true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TfmParametersEdit::DocRulesEdPropertiesButtonClick(TObject *Sender,
          int AButtonIndex)
{
  TfmDocRulesEdit *dlg = new TfmDocRulesEdit(Owner, FDocRules, FSpec);
  try
  {
    if (FDocByDocIcon)
      dlg->Icon = FDocByDocIcon;
    dlg->OnGetSpecificatorsList    = FOnGetSpecsList;
    dlg->OnGetOrgsList             = FOnGetOrgsList;
    dlg->OnGetDocElementsList      = FOnGetDocElementsList;
    dlg->OnGetThisDocElementStruct = FOnGetThisDocElementStruct;
    dlg->OnGetRefDocElementStruct  = FOnGetRefDocElementStruct;
    dlg->OnGetSpecNode             = FOnGetSpecNode;
    dlg->OnGetSpecMainName         = ParametersEditGetSpecMainName;
    dlg->OnGetDocElementViewText   = FOnGetDocElementViewText;
    dlg->OnGetDocElementStruct     = FOnGetDocElementStruct;
    if (dlg->ShowModal() == mrOk)
    {
      if (dlg->DocRules)
      {
        if (!FDocRules)
          FDocRules = new TTagNode;
        FDocRules->Assign(dlg->DocRules, true);
      }
      else
      {
        if (FDocRules)
          __DELETE_OBJ(FDocRules)
      }
      UpdateDocRulesEdit();
      bApply->Enabled = true;
    }
  }
  __finally
  {
    delete dlg;
  }
}
//---------------------------------------------------------------------------

