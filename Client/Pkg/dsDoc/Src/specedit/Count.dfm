object fmCount: TfmCount
  Left = 467
  Top = 322
  BorderStyle = bsDialog
  BorderWidth = 8
  Caption = #1055#1086#1076#1089#1095#1077#1090' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1072
  ClientHeight = 110
  ClientWidth = 369
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gbClient: TGroupBox
    Left = 0
    Top = 0
    Width = 369
    Height = 77
    Align = alClient
    TabOrder = 0
    object CountLab: TLabel
      Left = 8
      Top = 20
      Width = 130
      Height = 13
      Caption = #1055#1086#1076#1089#1095#1077#1090' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1072' '#1076#1083#1103' :'
    end
    object FilterRulesLab: TLabel
      Left = 8
      Top = 52
      Width = 85
      Height = 13
      Caption = #1055#1088#1072#1074#1080#1083#1072' '#1086#1090#1073#1086#1088#1072':'
    end
    object cbISs: TcxComboBox
      Left = 144
      Top = 17
      Properties.DropDownListStyle = lsEditFixedList
      Properties.OnChange = cbISsPropertiesChange
      TabOrder = 0
      Width = 217
    end
    object edCondRules: TcxButtonEdit
      Left = 144
      Top = 50
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = edCondRulesPropertiesButtonClick
      TabOrder = 1
      Width = 217
    end
  end
  object pBottom: TPanel
    Left = 0
    Top = 77
    Width = 369
    Height = 33
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 1
    object pCtrls: TPanel
      Left = 45
      Top = 0
      Width = 324
      Height = 33
      Align = alRight
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 0
      object bOk: TButton
        Left = 0
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Ok'
        Default = True
        TabOrder = 0
        OnClick = bOkClick
      end
      object bCancel: TButton
        Left = 166
        Top = 8
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
        OnClick = bCancelClick
      end
      object bHelp: TButton
        Left = 249
        Top = 8
        Width = 75
        Height = 25
        Caption = '&'#1055#1086#1084#1086#1097#1100
        TabOrder = 2
        OnClick = bHelpClick
      end
      object bClear: TButton
        Left = 83
        Top = 8
        Width = 75
        Height = 25
        Caption = #1057#1073#1088#1086#1089#1080#1090#1100
        TabOrder = 3
        OnClick = bClearClick
      end
    end
  end
end
