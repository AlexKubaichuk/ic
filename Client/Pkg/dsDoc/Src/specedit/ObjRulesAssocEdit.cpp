/*
 ����        - ObjRulesAssocEdit.cpp
 ������      - ��������������� (ICS)
 ��������    - ���� ���������� ������, ����������� �������� �����
 ��� ��������� ������������ ����� ����������
 ������� ������������� � ���������� �������������� �������
 ( ������������ ����������� TICSDocSpec )
 ����������� - ������� �.�.
 ����        - 19.11.2004
 */
// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "icsDocSpecConstDef.h"
#include "ObjRulesAssocEdit.h"
#include "ICSDocSpec.h"
// #include "DKMBUtils.h"
#include "msgdef.h"
// #include "DKRusMB.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
// ***************************************************************************
// ********************** ��������/��������/����� ����� **********************
// ***************************************************************************
__fastcall TfmObjRulesAssocEdit::TfmObjRulesAssocEdit(TComponent * Owner, TTagNode * AObjRulesNode,
  TTagNode * AThisSpec, TTagNode * ARefSpec) : TForm(Owner)
 {
  if (!AObjRulesNode)
   throw DKClasses::ENullArgument(__FUNC__, "AObjRulesNode");
  if (!AThisSpec)
   throw DKClasses::ENullArgument(__FUNC__, "AThisSpec");
  if (!ARefSpec)
   throw DKClasses::ENullArgument(__FUNC__, "ARefSpec");
  FObjRulesNode = new TTagNode;
  FObjRulesNode->Assign(AObjRulesNode, true);
  FThisSpec                        = AThisSpec;
  FRefSpec                         = ARefSpec;
  FThisObj                         = FThisSpec->GetTagByUID(FObjRulesNode->AV["thisref"]);
  FRefObj                          = FRefSpec->GetTagByUID(_UID(FObjRulesNode->AV["ref"]));
  FLoadingStructures               = false;
  FOnGetDocElementViewText         = NULL;
  FOnGetThisDocElementStruct       = NULL;
  FOnGetRefDocElementStruct        = NULL;
  FOnCheckAssociationExistence     = NULL;
  Caption                          = FMT(icsDocObjRulesAssocEditCaption);
  lbThisStructTitle->Caption       = FMT(icsDocObjRulesAssocEditThisStructTitle);
  lbRefStructTitle->Caption        = FMT(icsDocObjRulesAssocEditRefStructTitle);
  chbThisStructAutoExpand->Caption = FMT(icsDocObjRulesAssocEditAutoExpand);
  chbRefStructAutoExpand->Caption  = FMT(icsDocObjRulesAssocEditAutoExpand);
  actSetAssoc->Caption             = FMT(icsDocObjRulesAssocEditSetAssocCaption);
  actSetAssoc->Hint                = FMT(icsDocObjRulesAssocEditSetAssocHint);
  actOk->Caption                   = FMT(icsDocObjRulesAssocEditOkCaption);
  actOk->Hint                      = FMT(icsDocObjRulesAssocEditOkHint);
  actClearAssoc->Caption           = FMT(icsDocObjRulesAssocEditClearAssocCaption);
  actClearAssoc->Hint              = FMT(icsDocObjRulesAssocEditClearAssocHint);
  bCancel->Caption                 = FMT(icsDocObjRulesAssocEditCancelBtnCaption);
  bHelp->Caption                   = FMT(icsDocObjRulesAssocEditHelpBtnCaption);
 }
// ---------------------------------------------------------------------------
__fastcall TfmObjRulesAssocEdit::~TfmObjRulesAssocEdit()
 {
  delete FObjRulesNode;
 }
// ---------------------------------------------------------------------------
void __fastcall TfmObjRulesAssocEdit::FormShow(TObject * Sender)
 {
#ifdef HIDE_HELP_BUTTONS
  bHelp->Visible = false;
#endif
  /* TODO -okdv -cInsListRules : ���� �������� ��������� ������������ ��� insrules � listrules */
  if (FObjRulesNode->CmpName("tabrules"))
   {
    // ��������� caption'� ������ pTitle
    if (FObjRulesNode->CmpAV("crosstype", "normal"))
     pTitle->Caption = "  " + FMT(icsDocObjRulesAssocEditTitleCaptionNormal);
    else
     pTitle->Caption = "  " + FMT(icsDocObjRulesAssocEditTitleCaptionCross);
    UnicodeString ViewText;
    // ��������� caption'� ����� lbThisObjTitle
    if (FOnGetDocElementViewText)
     FOnGetDocElementViewText(this, FThisObj, ViewText);
    else
     ViewText = "???";
    lbThisObjTitle->Caption = "(" + ViewText + ")";
    // ��������� caption'� ����� lbRefObjTitle
    if (FOnGetDocElementViewText)
     FOnGetDocElementViewText(this, FRefObj, ViewText);
    else
     ViewText = "???";
    lbRefObjTitle->Caption = "(" + ViewText + ")";
    FLoadingStructures     = true;
    // �������� ��������� �������
    if (FOnGetThisDocElementStruct)
     {
      tvThisStruct->Items->BeginUpdate();
      tvThisStruct->Items->Clear();
      FOnGetThisDocElementStruct(this, FThisObj, tvThisStruct->Items);
      tvThisStruct->Selected = tvThisStruct->Items->GetFirstNode();
      TTreeNode * TreeNode = tvThisStruct->Selected;
      while (TreeNode)
       {
        TreeNode->Expand(true);
        TreeNode = TreeNode->getNextSibling();
       }
      tvThisStruct->Selected->MakeVisible();
      tvThisStruct->Items->EndUpdate();
     }
    TTreeNode * TreeNode = tvThisStruct->Items->GetFirstNode();
    while (TreeNode)
     {
      if (TreeNode->Data)
       {
        TTagNode * TagNode = reinterpret_cast<TTagNode *>(TreeNode->Data);
        if (TICSDocSpec::IsTableColSingle(TagNode) || TICSDocSpec::IsTableRowSingle(TagNode))
         {
          TTagNode * ndRefPair = FObjRulesNode->GetChildByAV("refpair", "thisref", TagNode->AV["uid"], true);
          if (ndRefPair && FOnGetDocElementViewText)
           {
            UnicodeString ViewText;
            FOnGetDocElementViewText(this, FRefObj->GetTagByUID(_UID(ndRefPair->AV["ref"])), ViewText);
            TreeNode->Text += (" -> " + ViewText);
           }
         }
       }
      TreeNode = TreeNode->GetNext();
     }
    // �������� ��������� �������������� �������
    if (FOnGetRefDocElementStruct)
     {
      tvRefStruct->Items->BeginUpdate();
      tvRefStruct->Items->Clear();
      FOnGetRefDocElementStruct(this, FRefObj, tvRefStruct->Items);
      tvRefStruct->Selected = tvRefStruct->Items->GetFirstNode();
      TTreeNode * TreeNode = tvRefStruct->Selected;
      while (TreeNode)
       {
        TreeNode->Expand(true);
        TreeNode = TreeNode->getNextSibling();
       }
      tvRefStruct->Selected->MakeVisible();
      tvRefStruct->Items->EndUpdate();
     }
    FLoadingStructures = false;
    tvThisStruct->OnChange(tvThisStruct, tvThisStruct->Selected);
   }
  else if (FObjRulesNode->CmpName("insrules"))
   {
    throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
   }
  else if (FObjRulesNode->CmpName("listrules"))
   {
    throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
   }
 }
// ***************************************************************************
// ********************** ������ ����� ***************************************
// ***************************************************************************
// ���������� ����������� ��������
void __fastcall TfmObjRulesAssocEdit::UpdateActionsEnabled()
 {
  bool bSetAssocEn, bClearAssocEn, bOkEn;
  bSetAssocEn = bClearAssocEn = bOkEn = false;
  bOkEn       = true;
  /* TODO -okdv -cInsListRules :
   ���� �������� ���������� ����������� ��������
   ��� insrules � listrules */
  if (tvThisStruct->Selected && tvRefStruct->Selected)
   if (FObjRulesNode->CmpName("tabrules"))
    {
     bool bTableNormalCrossType = FObjRulesNode->CmpAV("crosstype", "normal");
     TTagNode * ndThisStructSel = reinterpret_cast<TTagNode *>(tvThisStruct->Selected->Data);
     TTagNode * ndRefStructSel = reinterpret_cast<TTagNode *>(tvRefStruct->Selected->Data);
     if (ndThisStructSel && ndRefStructSel)
      {
       if ((bTableNormalCrossType && ((TICSDocSpec::IsTableColSingle(ndThisStructSel) && TICSDocSpec::IsTableColSingle
         (ndRefStructSel)) || (TICSDocSpec::IsTableRowSingle(ndThisStructSel) && TICSDocSpec::IsTableRowSingle
         (ndRefStructSel)))) || (!bTableNormalCrossType && ((TICSDocSpec::IsTableColSingle(ndThisStructSel)
         && TICSDocSpec::IsTableRowSingle(ndRefStructSel)) || (TICSDocSpec::IsTableRowSingle(ndThisStructSel)
         && TICSDocSpec::IsTableColSingle(ndRefStructSel)))))
        bSetAssocEn = true;
      }
    }
   else if (FObjRulesNode->CmpName("insrules"))
    {
     throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
    }
   else if (FObjRulesNode->CmpName("listrules"))
    {
     throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
    }
  if (tvThisStruct->Selected)
   if (FObjRulesNode->CmpName("tabrules"))
    {
     TTagNode * ndThisStructSel = reinterpret_cast<TTagNode *>(tvThisStruct->Selected->Data);
     if (ndThisStructSel && (TICSDocSpec::IsTableColSingle(ndThisStructSel) || TICSDocSpec::IsTableRowSingle
       (ndThisStructSel)) && FObjRulesNode->GetChildByAV("refpair", "thisref", ndThisStructSel->AV["uid"], true))
      bClearAssocEn = true;
    }
   else if (FObjRulesNode->CmpName("insrules"))
    {
     throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
    }
   else if (FObjRulesNode->CmpName("listrules"))
    {
     throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
    }
  actSetAssoc->Enabled = bSetAssocEn;
  actClearAssoc->Enabled = bClearAssocEn;
  actOk->Enabled         = bOkEn;
 }
// ---------------------------------------------------------------------------
/*
 * ���������� tvThisStruct ����� ���������� �������� actSetAssoc ��� actClearAssoc
 *
 *
 * ���������:
 *   [in]  AAction                 - ����������� ��������
 *   [in]  bNeedSelectedNodeUpdate - ������� ������������� ���������� ���������� ���� tvThisStruct
 *
 */
void __fastcall TfmObjRulesAssocEdit::UpdateThisStructTreeView(const TAction * AAction,
  const bool bNeedSelectedNodeUpdate)
 {
  // �������� AAction ���� ��������� ��� ������� ���������������� button'�
  if (AAction->ActionComponent && AAction->ActionComponent->ClassNameIs("TButton"))
   {
    tvThisStruct->SetFocus();
   }
  // �������� AAction ���� ��������� ��� ������ ���������������� ������ ������������
  // ���� ThisStructPM ��� ��� ������� ��������������� ������� ����������
  else
   {
    if (bNeedSelectedNodeUpdate)
     {
      TRect SelectedNodeRect;
      if (tvThisStruct->Selected->IsVisible)
       {
        SelectedNodeRect = tvThisStruct->Selected->DisplayRect(false);
        InvalidateRect(tvThisStruct->Handle, & SelectedNodeRect, true);
        UpdateWindow(tvThisStruct->Handle);
       }
     }
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TfmObjRulesAssocEdit::AreInputsCorrect()
 {
  /* TODO -okdv -cInsListRules :
   ���� �������� �������� ������������ ������������
   ��� insrules � listrules */
  if (FObjRulesNode->CmpName("tabrules"))
   {
    UnicodeString TableElementName;
    bool bBadRules = false;
    if (!FObjRulesNode->GetChildByName("colrules")->Count)
     {
      TableElementName = FMT(icsDocObjRulesAssocEditTableElementNameCol);
      bBadRules        = true;
     }
    else if (!FObjRulesNode->GetChildByName("rowrules")->Count)
     {
      TableElementName = FMT(icsDocObjRulesAssocEditTableElementNameRow);
      bBadRules        = true;
     }
    if (bBadRules)
     {
      _MSG_ERR(FMT1(icsDocRulesEditErrorConf, TableElementName), TICSDocSpec::ICSName);
      ActiveControl          = tvThisStruct;
      tvThisStruct->Selected = tvThisStruct->Items->GetFirstNode();
      return false;
     }
   }
  else if (FObjRulesNode->CmpName("insrules"))
   {
    throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
   }
  else if (FObjRulesNode->CmpName("listrules"))
   {
    throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
   }
  return true;
 }
// ***************************************************************************
// ********************** ����������� �������*********************************
// ***************************************************************************
void __fastcall TfmObjRulesAssocEdit::actOkExecute(TObject * Sender)
 {
  if (!CheckDKFilterControls(bOk)) // ��� ����������� �� ������ DKComp,
     return; // ������������ TDKControlFilter
  if (!AreInputsCorrect())
   return;
  ModalResult = mrOk;
 }
// ---------------------------------------------------------------------------
void __fastcall TfmObjRulesAssocEdit::bCancelClick(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
// ---------------------------------------------------------------------------
void __fastcall TfmObjRulesAssocEdit::bHelpClick(TObject * Sender)
 {
  Application->HelpContext(HelpContext);
 }
// ---------------------------------------------------------------------------
void __fastcall TfmObjRulesAssocEdit::actSetAssocExecute(TObject * Sender)
 {
  TTagNode * ndThisStructSel = reinterpret_cast<TTagNode *>(tvThisStruct->Selected->Data);
  TTagNode * ndRefStructSel = reinterpret_cast<TTagNode *>(tvRefStruct->Selected->Data);
  // �������� ������� ������������ ��� �������� ��������, ������������� �� ������ ������ ���
  // �� ������ �����, �� ��� ������� �������
  if (FOnCheckAssociationExistence)
   {
    UnicodeString Msg;
    if (FOnCheckAssociationExistence(this, FObjRulesNode, ndThisStructSel->AV["uid"], Msg))
     {
      TModalResult MR = _MSG_QUE(FMT1(icsDocObjRulesAssocEditSetAssocConf, tvThisStruct->Selected->Text) + "\n" + Msg,
        TICSDocSpec::ICSName);
      if (MR == mrNo)
       return;
     }
   }
  bool bNeedSelectedNodeUpdate = false;
  if (FObjRulesNode->CmpName("tabrules"))
   {
    TTagNode * ndRefPair = FObjRulesNode->GetChildByAV("refpair", "thisref", ndThisStructSel->AV["uid"], true);
    if (!ndRefPair)
     if (ndThisStructSel->CmpName("tcol"))
      ndRefPair = FObjRulesNode->GetChildByName("colrules")->AddChild("refpair");
     else if (ndThisStructSel->CmpName("trow"))
      ndRefPair = FObjRulesNode->GetChildByName("rowrules")->AddChild("refpair");
    if (ndRefPair)
     {
      ndRefPair->AV["thisref"] = ndThisStructSel->AV["uid"];
      ndRefPair->AV["ref"]     = FRefSpec->GetChildByName("passport")->AV["GUI"] + "." + ndRefStructSel->AV["uid"];
      if (FOnGetDocElementViewText)
       {
        UnicodeString ViewText;
        FOnGetDocElementViewText(this, ndThisStructSel, ViewText);
        tvThisStruct->Selected->Text = ViewText;
        FOnGetDocElementViewText(this, FRefObj->GetTagByUID(_UID(ndRefPair->AV["ref"])), ViewText);
        tvThisStruct->Selected->Text += (" -> " + ViewText);
        bNeedSelectedNodeUpdate = true;
       }
     }
   }
  else if (FObjRulesNode->CmpName("insrules"))
   {
    throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
   }
  else if (FObjRulesNode->CmpName("listrules"))
   {
    throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
   }
  UpdateThisStructTreeView(dynamic_cast<TAction *>(Sender), bNeedSelectedNodeUpdate);
  UpdateActionsEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TfmObjRulesAssocEdit::actClearAssocExecute(TObject * Sender)
 {
  bool bNeedSelectedNodeUpdate = false;
  if (FObjRulesNode->CmpName("tabrules"))
   {
    TTagNode * ndThisStructSel = reinterpret_cast<TTagNode *>(tvThisStruct->Selected->Data);
    TTagNode * ndRefPair = FObjRulesNode->GetChildByAV("refpair", "thisref", ndThisStructSel->AV["uid"], true);
    if (ndRefPair)
     delete ndRefPair;
    if (FOnGetDocElementViewText)
     {
      UnicodeString ViewText;
      FOnGetDocElementViewText(this, ndThisStructSel, ViewText);
      tvThisStruct->Selected->Text = ViewText;
      bNeedSelectedNodeUpdate      = true;
     }
   }
  else if (FObjRulesNode->CmpName("insrules"))
   {
    throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
   }
  else if (FObjRulesNode->CmpName("listrules"))
   {
    throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
   }
  UpdateThisStructTreeView(dynamic_cast<TAction *>(Sender), bNeedSelectedNodeUpdate);
  UpdateActionsEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TfmObjRulesAssocEdit::tvThisStructChange(TObject * Sender, TTreeNode * Node)
 {
  if (FLoadingStructures)
   return;
  if (FObjRulesNode->CmpName("tabrules"))
   {
    // ��������� � tvRefStruct � �������� ������� ����� ����, ����������������
    // ��������� �����, ���� � tvThisStruct ������ ���� �� ��������� ����� � ������
    // crosstype == "normal" ��� ������ ���� �� ��������� �������� � ������
    // crosstype == "trans"; ��� ���������������� ��������� ��������, ���� �
    // tvThisStruct ������ ���� �� ��������� �������� � ������ crosstype == "normal"
    // ��� ������ ���� �� ��������� ����� � ������ crosstype == "trans"
    // ��� ���� ��������� ����� � tvRefStruct ���������� ������ ������, ������ ������
    // ��������� ������������� � ���������� ��� ���������, ������ ������ ���������
    // ���������������
    TTagNode * ndCurNode;
    TTreeNode * CurNode = Node;
    do
     {
      ndCurNode = reinterpret_cast<TTagNode *>(CurNode->Data);
      CurNode   = CurNode->Parent;
     }
    while (ndCurNode && !ndCurNode->CmpName("rows,columns"));
    TTagNode * ndTreeViewFirst = reinterpret_cast<TTagNode *>(tvRefStruct->Items->GetFirstNode()->Data);
    UnicodeString NormalCrossTypeAttrNames[] =
     {
      "columns",
      "rows"
     };
    UnicodeString TransCrossTypeAttrNames[] =
     {
      "rows",
      "columns"
     };
    UnicodeString * pCrossTypeAttrNames = FObjRulesNode->CmpAV("crosstype", "normal") ? NormalCrossTypeAttrNames :
      TransCrossTypeAttrNames;
    int nInd = (ndCurNode->CmpName("columns")) ? 0 : 1;
    if (ndTreeViewFirst->Name != pCrossTypeAttrNames[nInd])
     {
      bool bOldAutoExpand = tvRefStruct->AutoExpand;
      tvRefStruct->AutoExpand = false;
      tvRefStruct->Items->GetFirstNode()->getNextSibling()->MoveTo(tvRefStruct->Items->GetFirstNode(), naInsert);
      tvRefStruct->Items->GetFirstNode()->Expand(true);
      tvRefStruct->Items->GetFirstNode()->getNextSibling()->Collapse(true);
      tvRefStruct->Selected   = tvRefStruct->Items->GetFirstNode();
      tvRefStruct->AutoExpand = bOldAutoExpand;
     }
   }
  else if (FObjRulesNode->CmpName("insrules"))
   {
    throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
   }
  else if (FObjRulesNode->CmpName("listrules"))
   {
    throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
   }
  UpdateActionsEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TfmObjRulesAssocEdit::tvThisStructContextPopup(TObject * Sender, TPoint & MousePos, bool & Handled)
 {
  // ������� ������������� ��� ������� ������ ������ ����
  if (!((MousePos.x == -1) && (MousePos.y == -1)))
   if (tvThisStruct->Selected)
    {
     tvThisStruct->Selected->Selected = true;
     tvThisStruct->Selected->Focused  = true;
    }
 }
// ---------------------------------------------------------------------------
// Ins - ���������� ������������
// Del - �������� ������������
void __fastcall TfmObjRulesAssocEdit::tvThisStructKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  switch (Key)
   {
   case VK_INSERT:
    actSetAssoc->Execute();
    break;
   case VK_DELETE:
    actClearAssoc->Execute();
    break;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TfmObjRulesAssocEdit::tvRefStructChange(TObject * Sender, TTreeNode * Node)
 {
  UpdateActionsEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TfmObjRulesAssocEdit::tvRefStructExpanding(TObject * Sender, TTreeNode * Node, bool & AllowExpansion)
 {
  if (FObjRulesNode->CmpName("tabrules"))
   {
    // ������ ������������� ������ �������� ����, ��� ����, �����
    // �� ���� �������� ���������� ������������ ����� �������� � ������� ��� ������� �
    // �������� � ������ crosstype == "normal" � ����� �������� � �������� ��� ������� �
    // ������� � ������ crosstype == "trans"
    AllowExpansion = !((Node->Level == 0) && (Node->getPrevSibling()));
   }
  else if (FObjRulesNode->CmpName("insrules"))
   {
    throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
   }
  else if (FObjRulesNode->CmpName("listrules"))
   {
    throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TfmObjRulesAssocEdit::chbThisStructAutoExpandClick(TObject * Sender)
 {
  tvThisStruct->AutoExpand = chbThisStructAutoExpand->Checked;
  tvThisStruct->SetFocus();
 }
// ---------------------------------------------------------------------------
void __fastcall TfmObjRulesAssocEdit::chbRefStructAutoExpandClick(TObject * Sender)
 {
  tvRefStruct->AutoExpand = chbRefStructAutoExpand->Checked;
  tvRefStruct->SetFocus();
 }
// ---------------------------------------------------------------------------
void __fastcall TfmObjRulesAssocEdit::EditCtrlsPnResize(TObject * Sender)
 {
  int nSpaceY = bClearAssoc->Top - (bSetAssoc->Top + bSetAssoc->Height);
  bSetAssoc->Top   = (EditCtrlsPn->Height - (bSetAssoc->Height + nSpaceY + bClearAssoc->Height)) / 2;
  bClearAssoc->Top = bSetAssoc->Top + bSetAssoc->Height + nSpaceY;
 }
// ---------------------------------------------------------------------------
void __fastcall TfmObjRulesAssocEdit::ClientPnResize(TObject * Sender)
 {
  ThisPn->Width = (ClientPn->Width - EditCtrlsPn->Width) / 2;
  RefPn->Left   = ThisPn->Width;
  RefPn->Width  = ClientPn->Width - ThisPn->Width;
 }
// ---------------------------------------------------------------------------
