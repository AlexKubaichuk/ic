object fmObjRulesEdit: TfmObjRulesEdit
  Left = 60
  Top = 107
  BorderStyle = bsDialog
  BorderWidth = 8
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1087#1088#1072#1074#1080#1083' '#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1103' '#1086#1073#1098#1077#1082#1090#1072
  ClientHeight = 490
  ClientWidth = 603
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 301
    Top = 17
    Width = 5
    Height = 387
    Align = alRight
    Beveled = True
    ExplicitLeft = 230
    ExplicitHeight = 178
  end
  object pBottom: TPanel
    Left = 0
    Top = 457
    Width = 603
    Height = 33
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 0
    object pCtrls: TPanel
      Left = 362
      Top = 0
      Width = 241
      Height = 33
      Align = alRight
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 0
      object bOk: TButton
        Left = 0
        Top = 8
        Width = 75
        Height = 25
        Action = actOk
        Caption = 'Ok'
        Default = True
        TabOrder = 0
      end
      object bCancel: TButton
        Left = 83
        Top = 8
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
        OnClick = bCancelClick
      end
      object bHelp: TButton
        Left = 166
        Top = 8
        Width = 75
        Height = 25
        Caption = '&'#1055#1086#1084#1086#1097#1100
        TabOrder = 2
        OnClick = bHelpClick
      end
    end
    object pDopCtrls: TPanel
      Left = 0
      Top = 0
      Width = 153
      Height = 33
      Align = alLeft
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 1
    end
  end
  object ObjTitleLab: TPanel
    Left = 0
    Top = 0
    Width = 603
    Height = 17
    Align = alTop
    Alignment = taLeftJustify
    Caption = '  '#1054#1073#1098#1077#1082#1090
    Color = clMedGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
  end
  object pProps: TPanel
    Left = 0
    Top = 404
    Width = 603
    Height = 53
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object pListRulesProps: TPanel
      Left = 0
      Top = 17
      Width = 603
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 3
    end
    object pInsRulesProps: TPanel
      Left = 0
      Top = 17
      Width = 603
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
    end
    object pTabRulesProps: TPanel
      Left = 0
      Top = 17
      Width = 603
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object AssocTypeLab: TLabel
        Left = 8
        Top = 12
        Width = 94
        Height = 13
        Caption = #1058#1080#1087' '#1089#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1103':'
      end
      object cbCrossType: TcxComboBox
        Left = 108
        Top = 9
        Properties.DropDownListStyle = lsEditFixedList
        Properties.Items.Strings = (
          #1087#1088#1103#1084#1086#1077
          #1080#1085#1074#1077#1088#1089#1085#1086#1077)
        TabOrder = 0
        Text = #1087#1088#1103#1084#1086#1077
        Width = 143
      end
    end
    object pPropHead: TPanel
      Left = 0
      Top = 0
      Width = 603
      Height = 17
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  '#1055#1072#1088#1072#1084#1077#1090#1088#1099
      Color = clMedGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 1
    end
  end
  object pThisObj: TPanel
    Left = 0
    Top = 17
    Width = 301
    Height = 387
    Align = alClient
    BevelOuter = bvNone
    Constraints.MinHeight = 50
    TabOrder = 3
    object Panel7: TPanel
      Left = 0
      Top = 0
      Width = 301
      Height = 40
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object lbThisObjTitle: TLabel
        Left = 8
        Top = 8
        Width = 49
        Height = 13
        Caption = #1054#1073#1098#1077#1082#1090':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbThisSpecTitle: TLabel
        Left = 8
        Top = 24
        Width = 73
        Height = 13
        Caption = 'lbThisSpecTitle'
      end
    end
    object lbxThisObj: TDKListBox
      Left = 0
      Top = 40
      Width = 301
      Height = 347
      ListItemHeight = 16
      ActionAdd.ExecKey = 45
      ActionEdit.ExecKey = 8205
      ActionDelete.ExecKey = 46
      ControlColors.EnabledColor = clWindow
      ControlColors.EnabledFontColor = clWindowText
      OnSelect = lbxThisObjSelect
      Align = alClient
      Constraints.MinHeight = 132
      Constraints.MinWidth = 140
      TabOrder = 1
    end
  end
  object pRefObj: TPanel
    Left = 306
    Top = 17
    Width = 297
    Height = 387
    Align = alRight
    BevelOuter = bvNone
    Constraints.MinHeight = 50
    TabOrder = 4
    object Panel8: TPanel
      Left = 0
      Top = 0
      Width = 297
      Height = 40
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object lbRefObjTitle: TLabel
        Left = 8
        Top = 8
        Width = 145
        Height = 13
        Caption = #1048#1085#1090#1077#1075#1088#1080#1088#1091#1077#1084#1099#1081' '#1086#1073#1098#1077#1082#1090':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbRefSpecTitle: TLabel
        Left = 8
        Top = 24
        Width = 70
        Height = 13
        Caption = 'lbRefSpecTitle'
      end
    end
    object lbxRefObj: TDKListBox
      Left = 0
      Top = 40
      Width = 297
      Height = 347
      ListItemHeight = 16
      ActionAdd.ExecKey = 45
      ActionEdit.ExecKey = 8205
      ActionDelete.ExecKey = 46
      ControlColors.EnabledColor = clWindow
      ControlColors.EnabledFontColor = clWindowText
      Align = alClient
      Constraints.MinHeight = 132
      Constraints.MinWidth = 140
      TabOrder = 1
    end
  end
  object ActionList1: TActionList
    Left = 380
    Top = 31
    object actOk: TAction
      Caption = 'O&k'
      HelpType = htContext
      Hint = 'Ok'
      OnExecute = actOkExecute
    end
  end
end
