/*
  ����        - ICSDocParsers.cpp
  ������      - ��������������� (ICS)
  ��������    - ���� ���������� ������, ����������� ������
                ��� ������� ������ ���������� ������/������� ������� �
                ������ �������� ������� ����� ������ � �������
  ����������� - ������� �.�.
  ����        - 26.10.2004
*/
//---------------------------------------------------------------------------

#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "ICSDocParsers.h"
#include "DKUtils.h"
#include "DKClasses.h"
#include "XMLContainer.h"

#include <algorithm>
#include <functional>
#include "ICSTL.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------

namespace ICSDocParsers
{

//***************************************************************************
//********************** ������� ********************************************
//***************************************************************************

//#define icsIS_DIGIT(ch)    ( ( (ch) >= '0' ) && ( (ch) <= '9' ) )
#define EOE             0
#define EMPTY_CHAR      0

//***************************************************************************
//********************** ���� ***********************************************
//***************************************************************************

//���� ������
typedef stack< LEX_LIST_ITEM, vector<LEX_LIST_ITEM> > TLexListItemStack;

typedef TLexMap::iterator TLexMapIt;            //�������� ��� ������� ������
typedef TLexList::iterator TLexListIt;          //�������� ��� ������ ������
typedef TLexList::reverse_iterator TLexListRIt; //�������� �������� ��� ������ ������

//***************************************************************************
//********************** ���������� ���������� ******************************
//***************************************************************************

//���� ������
const UnicodeString LexTypes[]  =
{
  cFMT(icsDocParsersLexTypes01),
  cFMT(icsDocParsersLexTypes02),
  cFMT(icsDocParsersLexTypes03),
  cFMT(icsDocParsersLexTypes04),
  cFMT(icsDocParsersLexTypes05),
  cFMT(icsDocParsersLexTypes06),
  cFMT(icsDocParsersLexTypes07),
  cFMT(icsDocParsersLexTypes08),
  cFMT(icsDocParsersLexTypes09),
  cFMT(icsDocParsersLexTypes10),
  cFMT(icsDocParsersLexTypes11),
  cFMT(icsDocParsersLexTypes12),
  cFMT(icsDocParsersLexTypes13),
  cFMT(icsDocParsersLexTypes14),
  cFMT(icsDocParsersLexTypes15),
  cFMT(icsDocParsersLexTypes16),
  cFMT(icsDocParsersLexTypes17),
  cFMT(icsDocParsersLexTypes18),
  cFMT(icsDocParsersLexTypes19),
  cFMT(icsDocParsersLexTypes20),
  cFMT(icsDocParsersLexTypes21)
};

//������ ������
const UnicodeString LexClasses[]  =
{
  cFMT(icsDocParsersLexClasses1),
  cFMT(icsDocParsersLexClasses2),
  cFMT(icsDocParsersLexClasses3),
  cFMT(icsDocParsersLexClasses4),
  cFMT(icsDocParsersLexClasses5),
  cFMT(icsDocParsersLexClasses6),
  cFMT(icsDocParsersLexClasses7),
  cFMT(icsDocParsersLexClasses8),
  cFMT(icsDocParsersLexClasses9)
};

//��������� �� ������� ������������ �����������
const UnicodeString LexErrors[] =
{
  cFMT(icsDocParsersLexErrors1),
  cFMT(icsDocParsersLexErrors2),
  cFMT(icsDocParsersLexErrors3),
  cFMT(icsDocParsersLexErrors4),
  cFMT(icsDocParsersLexErrors5),
  cFMT(icsDocParsersLexErrors6),
  cFMT(icsDocParsersLexErrors7),
  cFMT(icsDocParsersLexErrors8),
  cFMT(icsDocParsersLexErrors9)
};

const int LexErrCodeOffset = 0;

//��������� �� ������� ��������������� �����������
const UnicodeString SyntErrors[] =
{
  cFMT(icsDocParsersSyntErrors01),
  cFMT(icsDocParsersSyntErrors02),
  cFMT(icsDocParsersSyntErrors03),
  cFMT(icsDocParsersSyntErrors04),
  cFMT(icsDocParsersSyntErrors05),
  cFMT(icsDocParsersSyntErrors06),
  cFMT(icsDocParsersSyntErrors07),
  cFMT(icsDocParsersSyntErrors08),
  cFMT(icsDocParsersSyntErrors09),
  cFMT(icsDocParsersSyntErrors10),
  cFMT(icsDocParsersSyntErrors11),
  cFMT(icsDocParsersSyntErrors12),
  cFMT(icsDocParsersSyntErrors13)
};

const int SyntErrCodeOffset = 20;

//��������� �� ������� �������������� �����������
const UnicodeString SemErrors[]  =
{
  cFMT(icsDocParsersSemErrors01),
  cFMT(icsDocParsersSemErrors02),
  cFMT(icsDocParsersSemErrors03),
  cFMT(icsDocParsersSemErrors04),
  cFMT(icsDocParsersSemErrors05),
  cFMT(icsDocParsersSemErrors06),
  cFMT(icsDocParsersSemErrors07),
  cFMT(icsDocParsersSemErrors08),
  cFMT(icsDocParsersSemErrors09),
  cFMT(icsDocParsersSemErrors10),
  cFMT(icsDocParsersSemErrors11)
};

const int SemErrCodeOffset = 20;

//***************************************************************************
//********************** �������������� ������� *****************************
//***************************************************************************

struct SelectLexMapLexData : public unary_function<pair<int,LEX_TABLE_REC>, UnicodeString>
{
  UnicodeString operator()(const pair<int,LEX_TABLE_REC> x) const
  {
    return x.second.Data;
  }
};

//---------------------------------------------------------------------------

} // end of namespace ICSDocParsers

//***************************************************************************
//********************** TLexTable ******************************************
//******************* ������� ������ ****************************************
//***************************************************************************

__fastcall TLexTable::TLexTable() : TObject()
{
  FID = 0;
  FModified = false;
}

//---------------------------------------------------------------------------
__fastcall TLexTable::~TLexTable()
{
}
//---------------------------------------------------------------------------

void __fastcall TLexTable::Clear()
{
  FID = 0;
  FModified = false;
  FLexMap.clear();
}

//---------------------------------------------------------------------------

bool __fastcall TLexTable::Empty() const
{
  return FLexMap.empty();
}

//---------------------------------------------------------------------------
const TLexMap* __fastcall TLexTable::GetLexMap() const
{
  return &FLexMap;
}
//---------------------------------------------------------------------------
int __fastcall TLexTable::NewID()
{
  return ++FID;
}
//---------------------------------------------------------------------------

LEX_TABLE_REC __fastcall TLexTable::GetRec( LEX_LIST_ITEM LexListItem )
{
  return GetRec( LexListItem.LexTableId );
}

//---------------------------------------------------------------------------

LEX_TABLE_REC __fastcall TLexTable::GetRec( int nId )
{
  return FLexMap[nId];
}

//---------------------------------------------------------------------------

int __fastcall TLexTable::FindRec( UnicodeString AData, TLexType AType, TLexClass AClass ) const
{
  int nID = -1;
  for ( TLexMap::const_iterator i = FLexMap.begin(); i != FLexMap.end(); i++ )
    if ( ( (*i).second.Data == AData ) && ( (*i).second.Type == AType ) && ( (*i).second.Class == AClass ) )
    {
      nID = (*i).first;
      break;
    }
  return nID;
}

//---------------------------------------------------------------------------

bool __fastcall TLexTable::IsOperand     ( LEX_LIST_ITEM LexListItem )
{
  return IsOperand( GetRec( LexListItem ) );
}

//---------------------------------------------------------------------------

bool __fastcall TLexTable::IsOperation   ( LEX_LIST_ITEM LexListItem )
{
  return IsOperation( GetRec( LexListItem ) );
}

//---------------------------------------------------------------------------

int  __fastcall TLexTable::GetOpPriority ( LEX_LIST_ITEM LexListItem )
{
  return GetOpPriority( GetRec( LexListItem ) );
}

//---------------------------------------------------------------------------

bool __fastcall TLexTable::IsLOp         ( LEX_LIST_ITEM LexListItem )
{
  return IsLOp( GetRec( LexListItem ) );
}

//---------------------------------------------------------------------------

bool __fastcall TLexTable::IsAOp         ( LEX_LIST_ITEM LexListItem )
{
  return IsAOp( GetRec( LexListItem ) );
}

//---------------------------------------------------------------------------

bool __fastcall TLexTable::IsRelOp       ( LEX_LIST_ITEM LexListItem )
{
  return IsRelOp( GetRec( LexListItem ) );
}

//---------------------------------------------------------------------------

bool __fastcall TLexTable::IsOperand     ( LEX_TABLE_REC LexTableRec )
{
  return ( ( LexTableRec.Class == lcUnknown ) ||
           ( LexTableRec.Class == lcNumber  ) ||
           ( LexTableRec.Class == lcString  ) );

}

//---------------------------------------------------------------------------

bool __fastcall TLexTable::IsOperation   ( LEX_TABLE_REC LexTableRec )
{
  return ( ( LexTableRec.Class == lcSingleOp ) ||
           ( LexTableRec.Class == lcMulOp    ) ||
           ( LexTableRec.Class == lcAddOp    ) ||
           ( LexTableRec.Class == lcRelOp    ) );
}

//---------------------------------------------------------------------------

int  __fastcall TLexTable::GetOpPriority( LEX_TABLE_REC LexTableRec )
{
  int nPriority = -1;
  switch ( LexTableRec.Class )
  {
    case lcSingleOp: nPriority = 4; break;
    case lcMulOp   : nPriority = 3; break;
    case lcAddOp   : nPriority = 2; break;
    case lcRelOp   : nPriority = 1; break;
  }
  return nPriority;
}

//---------------------------------------------------------------------------

bool __fastcall TLexTable::IsLOp         ( LEX_TABLE_REC LexTableRec )
{
  bool Result;
  Result = ( ( LexTableRec.Type  == ltNOT   ) ||
             ( LexTableRec.Type  == ltAND   ) ||
             ( LexTableRec.Type  == ltOR    ) );
  return Result;
}

//---------------------------------------------------------------------------

bool __fastcall TLexTable::IsAOp         ( LEX_TABLE_REC LexTableRec )
{
  bool Result;
  Result = ( ( LexTableRec.Type  == ltADD   ) ||
             ( LexTableRec.Type  == ltSUB   ) ||
             ( LexTableRec.Type  == ltMUL   ) ||
             ( LexTableRec.Type  == ltDIV   ) );
  return Result;
}

//---------------------------------------------------------------------------

bool __fastcall TLexTable::IsRelOp       ( LEX_TABLE_REC LexTableRec )
{
  bool Result;
  Result = ( LexTableRec.Class == lcRelOp );
  return Result;
}

//***************************************************************************
//********************** TSyntTreeNode **************************************
//******************** ���� ��������������� ������ **************************
//***************************************************************************

__fastcall TSyntTreeNode::TSyntTreeNode( TSyntTreeNode *AParent, LEX_LIST_ITEM ANode ) : TObject()
{
  FParent = AParent;
  FLex = ANode;
  FLeft = NULL;
  FRight = NULL;
  FLevel = ( FParent ) ? FParent->Level + 1 : 0;
}

//---------------------------------------------------------------------------

__fastcall TSyntTreeNode::~TSyntTreeNode()
{
  if ( FParent )
  {
    if ( FParent->Left == this )
      FParent->Left  = NULL;
    else
      FParent->Right = NULL;
  }
  if ( FLeft  ) delete FLeft;
  if ( FRight ) delete FRight;
}

//---------------------------------------------------------------------------

TSyntTreeNode* __fastcall TSyntTreeNode::AddLeftChild ( LEX_LIST_ITEM ANode )
{
  FLeft = new TSyntTreeNode( this, ANode );
  return FLeft;
}

//---------------------------------------------------------------------------

TSyntTreeNode* __fastcall TSyntTreeNode::AddRightChild( LEX_LIST_ITEM ANode )
{
  FRight = new TSyntTreeNode( this, ANode );
  return FRight;
}

//---------------------------------------------------------------------------

bool __fastcall TSyntTreeNode::Iterate( TSyntTreeIterate ItItem )
{
  if ( ItItem(this) )
    return true;

  if ( Left )
  {
    if ( Left->Iterate( ItItem ) )
      return true;
  }

  if ( Right )
  {
    if ( Right->Iterate( ItItem ) )
      return true;
  }

  return false;
}

//---------------------------------------------------------------------------

bool __fastcall TSyntTreeNode::Iterate1( TSyntTreeIterate1 ItItem, void *p1 )
{
  if ( ItItem(this, p1) )
    return true;

  if ( Left )
  {
    if ( Left->Iterate1( ItItem, p1 ) )
      return true;
  }

  if ( Right )
  {
    if ( Right->Iterate1( ItItem, p1 ) )
      return true;
  }

  return false;
}

//***************************************************************************
//********************** TSyntTree ******************************************
//******************** �������������� ������ ********************************
//***************************************************************************

__fastcall TSyntTree::TSyntTree( PLEX_LIST_ITEM ARoot )
{
  if ( ARoot )
    CreateRoot( *ARoot );
  else
    FRoot = NULL;
}

//---------------------------------------------------------------------------

__fastcall TSyntTree::~TSyntTree()
{
  if (FRoot) delete FRoot;
}

//---------------------------------------------------------------------------

TSyntTreeNode* __fastcall TSyntTree::CreateRoot( LEX_LIST_ITEM ARoot )
{
  FRoot = new TSyntTreeNode( NULL, ARoot );
  return FRoot;
}

//---------------------------------------------------------------------------

void __fastcall TSyntTree::Clear()
{
  if (FRoot)
  {
    delete FRoot;
    FRoot = NULL;
  }
}

//***************************************************************************
//************************** TDKParser **************************************
//******************** ����������� ���������� *******************************
//***************************************************************************

__fastcall TDKParser::TDKParser( TObject *AOwner ) : TObject()
{
  FOwner  = AOwner;
  FErrPos = 0;
  FErrLen = 0;
}

//---------------------------------------------------------------------------
__fastcall TDKParser::~TDKParser()
{
}
//---------------------------------------------------------------------------

void __fastcall TDKParser::Init()
{
  FErrPos = 0;
  FErrLen = 0;
}

//---------------------------------------------------------------------------

bool __fastcall TDKParser::Parse()
{
  Init();
  return true;
}

//***************************************************************************
//************************** TExprLex ***************************************
//******************** ����������� ���������� *******************************
//***************************************************************************

__fastcall TExprLex::TExprLex( TObject *AOwner, TLexTable* ALexTable, TLexList* ALexList, UnicodeString AExpr ) : TDKParser(AOwner)
{
  if (!AOwner)
    throw DKClasses::ENullArgument(__FUNC__, "AOwner");
  if (!ALexTable)
    throw DKClasses::ENullArgument(__FUNC__, "ALexTable");
  if (!ALexList)
    throw DKClasses::ENullArgument(__FUNC__, "ALexList");

  FExpr    = AExpr;
  FLex     = "";
  FState   = stS;
  FLexStartPos = 0;
  FPos     = 0;
  FLexTable = ALexTable;
  FLexList  = ALexList;

  FErrType = etNone;
}

//---------------------------------------------------------------------------
__fastcall TExprLex::~TExprLex()
{
}
//---------------------------------------------------------------------------

void __fastcall TExprLex::Init()
{
  inherited :: Init();
  FState = stS;
  FLexStartPos = 0;
  FPos = 0;
  FLex = "";
  FErrType = etNone;
}

//---------------------------------------------------------------------------
char __fastcall TExprLex::LookUpNextChar()
{
  return GetChar(1);
}
//---------------------------------------------------------------------------
void __fastcall TExprLex::GoToState( TExprLexState NewState )
{
  FState = NewState;
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TExprLex::GetErrMsg()
{
  UnicodeString ErrMsg = LexErrors[FErrType];
  if ( FErrType != etNone )
    ErrMsg += FMT1(icsDocParsersErrorPos,IntToStr((int)FErrPos));
  return ErrMsg;
}

//---------------------------------------------------------------------------

int __fastcall TExprLex::GetErrCode()
{
  int ErrCode = 0;
  if ( FErrType != etNone )
    ErrCode = FErrType + LexErrCodeOffset;
  return ErrCode;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TExprLex::GetLexStrType( TLexType LexType )
{
  return LexTypes[LexType];
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TExprLex::GetLexStrClass( TLexClass LexClass )
{
  return LexClasses[LexClass];
}

//---------------------------------------------------------------------------

void __fastcall TExprLex::SetExpr( UnicodeString AExpr )
{
  FExpr = AExpr;
  Init();
}

//---------------------------------------------------------------------------

//����� ���������� �������, �� ��������� �������� ��� ����������, ����� �� ��������

char __fastcall TExprLex::LookUpPrevNotBlankChar()
{
  char Result = EMPTY_CHAR;
  int nCurPos = FPos;
  if ( nCurPos > 1 )
  {
    nCurPos--;
    while ( ( nCurPos > 1 ) && IsBlankSep( FExpr[nCurPos] ) )
      nCurPos--;
    if ( !IsBlankSep( FExpr[nCurPos] ) )
      Result = FExpr[nCurPos];
  }
  return Result;
}

//---------------------------------------------------------------------------

char __fastcall TExprLex::GetChar( int nOffset )
{
  if ( FPos + nOffset <= FExpr.Length() )
    return FExpr[FPos + nOffset];
  else
    return EOE;
}

//---------------------------------------------------------------------------

char __fastcall TExprLex::GetNextChar()
{
  FPos++;
  return GetChar();
}

//---------------------------------------------------------------------------

char __fastcall TExprLex::GetPrevChar()
{
  FPos--;
  if ( FPos < 1 )
    FPos = 1;
  return GetChar();
}

//---------------------------------------------------------------------------

bool __fastcall TExprLex::IsLET( const char c )
{
  bool result = false;

  if      ( ( c <= 'Z' ) && ( c >= 'A' ) ) result = true;
  else if ( ( c <= 'z' ) && ( c >= 'a' ) ) result = true;
  else if ( ( c <= '�' ) && ( c >= '�' ) ) result = true;
  else if ( ( c <= '�' ) && ( c >= '�' ) ) result = true;
  else if ( ( c == '�' ) || ( c == '�' ) ) result = true;

  return result;
}

//---------------------------------------------------------------------------

bool __fastcall TExprLex::IsID_CHAR( const char c )
{
  return IsLET(c) || IS_DIGIT(c) || ( c == '_' );
}

//---------------------------------------------------------------------------

bool __fastcall TExprLex::IsChSep( const char c )
{
  return ( FChSeps.find( c ) != FChSeps.end() );
}

//---------------------------------------------------------------------------

bool __fastcall TExprLex::IsBlankSep( const char c )
{
  return ( FBlankSeps.find( c ) != FBlankSeps.end() );
}

//---------------------------------------------------------------------------

bool __fastcall TExprLex::IsSignSep( const char c )
{
  return ( FSignSeps.find( c ) != FSignSeps.end() );
}

//---------------------------------------------------------------------------

bool __fastcall TExprLex::IsChOp( const char c )
{
  return ( FChOp.find( c ) != FChOp.end() );
}

//---------------------------------------------------------------------------

bool __fastcall TExprLex::IsEOEOrBlankSep( const char c )
{
  return IsBlankSep(c) || ( c == EOE );
}

//---------------------------------------------------------------------------

void __fastcall TExprLex::AddChar( const char c )
{
  if ( FLex == "" )
    FLexStartPos = FPos;
  FLex += c;
}

//---------------------------------------------------------------------------

void __fastcall TExprLex::ChangeState( const char c, TExprLexState NewState )
{
  AddChar(c);
  GoToState( NewState );
}

//***************************************************************************
//***************************** TExprSynt ***********************************
//******************** �������������� ���������� ****************************
//***************************************************************************

__fastcall TExprSynt::TExprSynt( TObject*   AOwner,
                                 TLexTable* ALexTable,
                                 TLexList*  ALexList,
                                 TLexList*  APolandLexList,
                                 TSyntTree* ASyntTree ) : TDKParser(AOwner)
{
  if (!AOwner)
    throw DKClasses::ENullArgument(__FUNC__, "AOwner");
  if (!ALexTable)
    throw DKClasses::ENullArgument(__FUNC__, "ALexTable");
  if (!ALexList)
    throw DKClasses::ENullArgument(__FUNC__, "ALexList");
  if (!APolandLexList)
    throw DKClasses::ENullArgument(__FUNC__, "APolandLexList");
  if (!ASyntTree)
    throw DKClasses::ENullArgument(__FUNC__, "ASyntTree");

  FLexTable      = ALexTable ;
  FLexList       = ALexList;
  FPolandLexList = APolandLexList;
  FSyntTree      = ASyntTree;
  FErrType       = stNone;
}

//---------------------------------------------------------------------------

void __fastcall TExprSynt::Init()
{
  inherited::Init();
  FErrType = stNone;
}

//---------------------------------------------------------------------------

//�������� ������ ������ � ���� �����'� �� ��������� ��������

bool __fastcall TExprSynt::CreatePoland( LEX_LIST_ITEM &LexListItem )
{
  LEX_TABLE_REC      LexTableRec;
  TLexListItemStack  LexStack;

  //������� ������ ������ ����� �������
  for ( TLexListIt CurLex = FLexList->begin(); CurLex != FLexList->end(); CurLex++ )
  {
    LexListItem = (*CurLex);
    LexTableRec = FLexTable->GetRec( LexListItem );

    if ( FLexTable->IsOperand( LexTableRec ) )  //�������
      FPolandLexList->push_back( LexListItem ); //��������� ������� � ����� ��������� ������
    else                                        //��������
    {
      if ( LexStack.empty() )                   //���� ���� ����
      {
        if ( LexTableRec.Class == lcRight )     //������
        {
          FErrType = stLeftRightDis;
          break;
        }
        else
          LexStack.push( LexListItem );         //��������� �������� � ����
      }
      else                                      //���� ���� �� ����
      {
        if ( LexTableRec.Class == lcLeft )      // '('
          LexStack.push( LexListItem );         //��������� ������� � ����
        else
        {
          if ( LexTableRec.Class == lcRight )   // ')'
          {                                     //���������� ��� �������� �� ��������� '(' � �����
            bool bLeftFound = false;            // ��������� ������ ( ������ � ������ �� ���������� )
            while ( !LexStack.empty() )
            {
              if ( FLexTable->GetRec( LexStack.top() ).Class == lcLeft )
              {
                LexStack.pop();
                bLeftFound = true;
                break;
              }
              else
              {
                FPolandLexList->push_back( LexStack.top() );
                LexStack.pop();
              }
            }
            if ( !bLeftFound )                  //������
            {
              FErrType = stLeftRightDis;
              break;
            }
          }
          else                                  //��������
          {
            while ( !LexStack.empty() )         //���������� �� ������ ��� �������� � ������� ��� ������
            {                                   //�����������
              LEX_TABLE_REC StackTopLexTableRec = FLexTable->GetRec( LexStack.top() );
              if ( FLexTable->IsOperation( StackTopLexTableRec ) )
              {
                if ( FLexTable->GetOpPriority( LexTableRec ) <= FLexTable->GetOpPriority( StackTopLexTableRec ) )
                {
                  FPolandLexList->push_back( LexStack.top() );
                  LexStack.pop();
                }
                else
                  break;
              }
              else
                break;
            }
            LexStack.push( LexListItem );       //��������� �������� � ����
          }
        }
      }
    }
  }

  if ( FErrType == stNone )
  {
    while ( !LexStack.empty() )                 //���������� �� ����� ���������� ��������
    {                                           //� ����� ��������� ������
      if ( FLexTable->IsOperation( FLexTable->GetRec( LexStack.top() ) ) )
      {
        FPolandLexList->push_back( LexStack.top() );
        LexStack.pop();
      }
      else                                      //���� � ����� ���� ������, �� ������
      {
        FErrType = stLeftRightDis;
        break;
      }
    }

    if ( FErrType == stNone )
      if ( FPolandLexList->empty() )            //���� �������� ������ ����, �� ������
        FErrType = stEmptyExpr;
  }

  return ( FErrType == stNone );
}

//---------------------------------------------------------------------------

//�������� ��������������� ������ �� ������ ������ � ���� �����'�
bool __fastcall TExprSynt::CreateSyntTree( LEX_LIST_ITEM &LexListItem )
{
  TSyntTreeNode* CurNode = NULL;        //���� ������, ��������������� ������� ��������
  LEX_TABLE_REC      LexTableRec;
  for ( TLexListRIt i = FPolandLexList->rbegin(); i != FPolandLexList->rend(); i++ )
  {
    LexListItem = (*i);
    LexTableRec = FLexTable->GetRec( LexListItem );

    //�������� �����
    if ( i == FPolandLexList->rbegin() )
      CurNode = FSyntTree->CreateRoot( LexListItem );
    else
    {
      //���������� �������� � ������ ���������
      if      ( !CurNode->Right )
      {
        CurNode->AddRightChild( LexListItem );
        if ( FLexTable->IsOperation(LexTableRec) )
          CurNode = CurNode->Right;
      }

      //���������� �������� � ����� ���������
      else if ( ( !CurNode->Left ) /*&& ( FLexTable->GetRec(CurNode->Lex).Class != lcSingleOp ) */)
      {
        CurNode->AddLeftChild( LexListItem );
        if ( FLexTable->IsOperation(LexTableRec) )
          CurNode = CurNode->Left;
      }

      //����� �� ������ ����� ��������� �������� ��������, � ������� �� ���������� �������,
      //��������������� ������ ���������
      else
      {
        TSyntTreeNode* NewCurNode = NULL;
        while( CurNode->Parent )
        {
          CurNode = CurNode->Parent;
          if ( ( !CurNode->Left ) && ( FLexTable->GetRec(CurNode->Lex).Class != lcSingleOp ) )
          {
            NewCurNode = CurNode;
            break;
          }
        }
        if ( NewCurNode )
        {
          NewCurNode->AddLeftChild( LexListItem );
          if ( FLexTable->IsOperation(LexTableRec) )
            CurNode = NewCurNode->Left;
        }
        else
        {
          FErrType = ( FLexTable->IsOperand(LexTableRec) ) ? stBadOperand : stBadOp;
          break;
        }
      }
    }
  }

  return ( FErrType == stNone );
}

//---------------------------------------------------------------------------

bool __fastcall TExprSynt::ParseTree( LEX_LIST_ITEM &LexListItem )
{
  bool bWasError = !FSyntTree->Root->Iterate1( CheckOp, &LexListItem );
  return bWasError;
}

//---------------------------------------------------------------------------

bool __fastcall TExprSynt::Parse()
{
  inherited::Parse();

  LEX_LIST_ITEM      LexListItem;

  if ( CreatePoland( LexListItem ) )
    if ( CreateSyntTree ( LexListItem ) )
      ParseTree( LexListItem );


  if ( FErrType != stNone )
  {
    FErrPos  = LexListItem.ExprPos;
    FErrLen  = FLexTable->GetRec( LexListItem ).Data.Length();
    return false;
  }
  else
    return true;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TExprSynt::GetErrMsg()
{
  UnicodeString ErrMsg = SyntErrors[FErrType];
  if ( FErrType != stNone )
    ErrMsg += FMT1(icsDocParsersErrorPos,IntToStr((int)FErrPos));
  return ErrMsg;
}

//---------------------------------------------------------------------------

int __fastcall TExprSynt::GetErrCode()
{
  int ErrCode = 0;
  if ( FErrType != stNone )
    ErrCode = FErrType + SyntErrCodeOffset;
  return ErrCode;
}

//***************************************************************************
//**************************** TExprSem *************************************
//******************** ������������� ���������� *****************************
//***************************************************************************

__fastcall TExprSem::TExprSem( TObject* AOwner, TLexTable* ALexTable, TSyntTree* ASyntTree ) : TDKParser(AOwner)
{
  if (!AOwner)
    throw DKClasses::ENullArgument(__FUNC__, "AOwner");
  if (!ALexTable)
    throw DKClasses::ENullArgument(__FUNC__, "ALexTable");
  if (!ASyntTree)
    throw DKClasses::ENullArgument(__FUNC__, "ASyntTree");

  FLexTable = ALexTable;
  FSyntTree = ASyntTree;
  FErrType  = mtNone;
}

//---------------------------------------------------------------------------

void __fastcall TExprSem::Init()
{
  inherited::Init();
  FErrType  = mtNone;
}

//---------------------------------------------------------------------------

bool __fastcall TExprSem::ParseTree( LEX_LIST_ITEM &LexListItem )
{
  bool bWasError = !FSyntTree->Root->Iterate1( CheckOp, &LexListItem );
  return bWasError;
}

//---------------------------------------------------------------------------

bool __fastcall TExprSem::Parse()
{
  inherited::Parse();

  LEX_LIST_ITEM      LexListItem;

  ParseTree( LexListItem );


  if ( FErrType != mtNone )
  {
    FErrPos  = LexListItem.ExprPos;
    FErrLen  = FLexTable->GetRec( LexListItem ).Data.Length();
    return false;
  }
  else
    return true;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TExprSem::GetErrMsg()
{
  UnicodeString ErrMsg = SemErrors[FErrType];
  if ( FErrType != mtNone )
    ErrMsg += FMT1(icsDocParsersErrorPos,IntToStr((int)FErrPos));
  return ErrMsg;
}

//---------------------------------------------------------------------------

int __fastcall TExprSem::GetErrCode()
{
  int ErrCode = 0;
  if ( FErrType != mtNone )
    ErrCode = FErrType + SemErrCodeOffset;
  return ErrCode;
}

//***************************************************************************
//************************** TExprParser ************************************
//********************* ���������� ��������� ********************************
//***************************************************************************

__fastcall TExprParser::TExprParser( UnicodeString AExpr ) : TDKParser(NULL)
{
  FExpr = AExpr;
  FLexTable = new TLexTable;
  FSyntTree = new TSyntTree;
  FExprLex  = NULL;
  FExprSynt = NULL;
  FExprSem  = NULL;
  InitParsers();
  FPolandStr = "";
  FErrType = peNone;
}

//---------------------------------------------------------------------------

__fastcall TExprParser::~TExprParser()
{
  delete FLexTable;
  delete FSyntTree;
  if (FExprLex)  delete FExprLex;
  if (FExprSynt) delete FExprSynt;
  if (FExprSem)  delete FExprSem;
}

//---------------------------------------------------------------------------

void __fastcall TExprParser::ThrowIfParsersMissing( UnicodeString AMethodName ) const
{
  if (!FExprLex)
    throw DKClasses::EMethodError(AMethodName, cFMT(icsDocParsersErrorExprLex));
  if (!FExprSynt)
    throw DKClasses::EMethodError(AMethodName, cFMT(icsDocParsersErrorExprSynt));
  if (!FExprSem)
    throw DKClasses::EMethodError(AMethodName, cFMT(icsDocParsersErrorExprSem));
}

//---------------------------------------------------------------------------

void __fastcall TExprParser::SetExpr( UnicodeString AExpr )
{
  if ( FExpr != AExpr )
  {
    FExpr = AExpr;
    FExprLex->Expr = AExpr;
    Init();
  }
}

//---------------------------------------------------------------------------

void __fastcall TExprParser::Init()
{
  ThrowIfParsersMissing(__FUNC__);
  inherited::Init();
  FExprLex ->Init();
  FExprSynt->Init();
  FExprSem ->Init();

  FLexTable     ->Clear();
  FLexList       .clear();
  FPolandLexList .clear();
  FSyntTree     ->Clear();

  FPolandStr = "";
  FNameMap.clear();
  FErrType = peNone;
}

//---------------------------------------------------------------------------

bool __fastcall TExprParser::Parse()
{
  ThrowIfParsersMissing(__FUNC__);
  inherited::Parse();

  bool Result;

  #pragma option push -w-pia
  if ( Result = FExprLex->Parse() )                     //����������� ������
    if ( Result = FExprSynt->Parse() )                  //�������������� ������
      if ( Result = FExprSem->Parse() )                 //������������� ������
  #pragma option pop
      {
        //���������� ������� ���� ( FNameMap )
        int nNameNum = 0;
        for ( TLexMap::const_iterator i = FLexTable->LexMap->begin(); i != FLexTable->LexMap->end(); i++ )
          if ( (*i).second.Type == ltName )
            FNameMap[ nNameNum++ ] = (*i).second;

        //���������� ���� FPolandStr ���������� � ���� �����'� � ������������ ' ' ����� ���������,
        //�������� ��, �, ��� ���������� ��������� ~, &, |;
        //����� ���������� �������������������� ���� @<������> ���
        //<������> - ������ ����� � ������ ���� FNameMap
        for ( TLexList::const_iterator i = FPolandLexList.begin(); i != FPolandLexList.end(); i++ )
        {
          if ( FPolandStr != "" )
            FPolandStr += " ";
          LEX_TABLE_REC LexTableRec = FLexTable->GetRec( *i );
          if      ( LexTableRec.Type == ltName )
          {
            TLexMap::const_iterator nNameIt = std::find_if( FNameMap.begin(),
                                                       FNameMap.end(),
                                                       ICUtils::compose1( bind2nd( equal_to<UnicodeString>(),
                                                                          LexTableRec.Data
                                                                 ),
                                                                 SelectLexMapLexData()
                                                       )
            );
            FPolandStr += "@" + IntToStr((int) (*nNameIt).first );
          }
          else if ( LexTableRec.Type == ltNOT )
            FPolandStr += "~";
          else if ( LexTableRec.Type == ltAND )
            FPolandStr += "&";
          else if ( LexTableRec.Type == ltOR )
            FPolandStr += "|";
          else
            FPolandStr += LexTableRec.Data;
        }
      }
      else
        FErrType = peSem;
    else
      FErrType = peSynt;
  else
    FErrType = peLex;

  if ( !Result )
  {
    FErrPos = GetErrPos();
    FErrLen = GetErrLen();
  }
  return Result;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TExprParser::GetErrMsg()
{
  ThrowIfParsersMissing(__FUNC__);
  UnicodeString Result = "";
  switch ( FErrType )
  {
    case peNone: Result = FMT(icsDocParsersOk);     break;
    case peLex : Result = FExprLex ->GetErrMsg(); break;
    case peSynt: Result = FExprSynt->GetErrMsg(); break;
    case peSem : Result = FExprSem->GetErrMsg();  break;
  }
  return Result;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TExprParser::GetErrMsg( bool ShowErrType )
{
  UnicodeString Result = "";
  if ( ShowErrType )
    switch ( FErrType )
    {
      case peLex : Result = FMT(icsDocParsersLexError);    break;
      case peSynt: Result = FMT(icsDocParsersSyntError); break;
      case peSem : Result = FMT(icsDocParsersSemError);  break;
    }
  return Result + GetErrMsg();
}

//---------------------------------------------------------------------------

int __fastcall TExprParser::GetErrCode()
{
  ThrowIfParsersMissing(__FUNC__);
  int Result = 0;
  switch ( FErrType )
  {
    case peLex  : Result = FExprLex ->GetErrCode(); break;
    case peSynt : Result = FExprSynt->GetErrCode(); break;
    case peSem  : Result = FExprSem ->GetErrCode(); break;
  }
  return Result;
}

//---------------------------------------------------------------------------

int __fastcall TExprParser::GetErrPos()
{
  ThrowIfParsersMissing(__FUNC__);
  int Result = -1;
  switch ( FErrType )
  {
    case peLex  : Result = FExprLex ->ErrPos; break;
    case peSynt : Result = FExprSynt->ErrPos; break;
    case peSem  : Result = FExprSem ->ErrPos; break;
  }
  return Result;
}

//---------------------------------------------------------------------------

int __fastcall TExprParser::GetErrLen()
{
  ThrowIfParsersMissing(__FUNC__);
  int Result = -1;
  switch ( FErrType )
  {
    case peLex  : Result = FExprLex ->ErrLen; break;
    case peSynt : Result = FExprSynt->ErrLen; break;
    case peSem  : Result = FExprSem ->ErrLen; break;
  }
  return Result;
}

//***************************************************************************
//************************** TCalcRulesLex **********************************
//******************** ����������� ���������� *******************************
//**************** ������ ���������� ������/������� ������� *****************
//***************************************************************************

__fastcall TCalcRulesLex::TCalcRulesLex( TObject *AOwner, TLexTable* ALexTable, TLexList* ALexList, UnicodeString AExpr )
        : TExprLex( AOwner, ALexTable, ALexList, AExpr )
{
  FSignSeps.insert('+');
  FSignSeps.insert('-');

  FChOp = FSignSeps;
  FChOp.insert('*');
  FChOp.insert('/');

  FChSeps = FChOp;
  FChSeps.insert('(');
  FChSeps.insert(')');

  FBlankSeps.insert(' ');
  FBlankSeps.insert('\t');
}

//---------------------------------------------------------------------------

bool __fastcall TCalcRulesLex::Parse()
{
  inherited::Parse();

  bool bParsing = true;
  char CurChar;
  char cTmpNext, cTmpPrev;

  //����� ����� �������
  while( bParsing )
  {
    if ( ( FState != stSepAn          ) &&
         ( FState != stError          ) &&
         ( FState != stPossibleAConst )
    )
      CurChar = GetNextChar();
    switch ( FState )
    {
      case stS:
        #ifdef CALCRULES_SUPPORT_NAMES
        if      ( IsLET(CurChar)      ) ChangeState( CurChar, stIdent );
        #else
        if      ( IsLET(CurChar)      ) { FErrType = etUnexpectedChar; GoToState ( stError ); }
        #endif
        else if ( IsSignSep(CurChar)  ) GoToState  ( stPossibleAConst );
        else if ( CurChar == '.'      ) ChangeState( CurChar, stDrob );
        else if ( IS_DIGIT(CurChar)   ) ChangeState( CurChar, stEndDec_DecFloat );
        else if ( IsBlankSep(CurChar) ) ;
        else if ( CurChar == '#'      ) ChangeState( CurChar, stRowColNum_CellNum );
        else if ( IsChSep(CurChar)    ) ChangeState( CurChar, stSepAn );
        else if ( CurChar == EOE      )
        {
          if ( FLexTable->Empty() )
          {
            FErrType = etEmptyExpr;
            GoToState  ( stError );
          }
          else
            GoToState  ( stEnd   );
        }
        else                            { FErrType = etUnexpectedChar; GoToState ( stError ); }
      break;

      #ifdef CALCRULES_SUPPORT_NAMES
      case stIdent:
        if      ( IsID_CHAR(CurChar)       ) AddChar( CurChar );
        else if ( IsChSep(CurChar)         ) { FillLexTable(); ChangeState( CurChar, stSepAn ); }
        else if ( IsEOEOrBlankSep(CurChar) ) { FillLexTable(); GoToState( stS ); }
        else                                 { FErrType = etUnexpectedChar; GoToState  ( stError ); }
      break;
      #endif      

      case stPossibleAConst:
        cTmpNext = LookUpNextChar();
        if ( IS_DIGIT(cTmpNext) || ( cTmpNext == '.') )
        {
          cTmpPrev = LookUpPrevNotBlankChar();
            if ( (cTmpPrev == EMPTY_CHAR) || (cTmpPrev == '(') || IsChOp(cTmpPrev) )
            {
              AddChar(CurChar);
              GoToState( stS );
            }
        }
        if ( FState == stPossibleAConst )
          ChangeState( CurChar, stSepAn );
      break;

      case stDrob:
        if ( IS_DIGIT(CurChar) )    ChangeState( CurChar, stDigits );
        else                        { FErrType = etDigExpected; GoToState( stError ); }
      break;

      case stDigits:
        if      ( IS_DIGIT(CurChar)        ) AddChar( CurChar );
        else if ( IsChSep(CurChar)         ) { FillLexTable(); ChangeState( CurChar, stSepAn ); }
        else if ( IsEOEOrBlankSep(CurChar) ) { FillLexTable(); GoToState( stS ); }
        else                                 { FErrType = etDigSepExpected; GoToState( stError ); }
      break;

      case stEndDec_DecFloat:
        if      ( CurChar == '.'           ) ChangeState( CurChar, stDrob );
        else if ( IS_DIGIT(CurChar)        ) AddChar( CurChar );
        else if ( IsChSep(CurChar)         ) { FillLexTable(); ChangeState( CurChar, stSepAn ); }
        else if ( IsEOEOrBlankSep(CurChar) ) { FillLexTable(); GoToState( stS ); }
        else                                 { FErrType = etDigSepDotExpected; GoToState( stError ); }
      break;

      case stRowColNum_CellNum:
        if      ( IS_DIGIT(CurChar)        ) ChangeState( CurChar, stEndRowColNum_CellNum );
        else                                 { FErrType = etDigExpected; GoToState( stError ); }
      break;

      case stEndRowColNum_CellNum:
        if      ( IS_DIGIT(CurChar)        ) AddChar( CurChar );
        else if ( CurChar == ':'           ) ChangeState( CurChar, stCellNum );
        else if ( IsChSep(CurChar)         ) { FillLexTable(); ChangeState( CurChar, stSepAn ); }
        else if ( IsEOEOrBlankSep(CurChar) ) { FillLexTable(); GoToState( stS ); }
        else                                 { FErrType = etDigSepColonExpected; GoToState( stError ); }
      break;

      case stCellNum:
        if      ( IS_DIGIT(CurChar)        ) ChangeState( CurChar, stEndCellNum );
        else                                 { FErrType = etDigExpected; GoToState( stError ); }
      break;

      case stEndCellNum:
        if      ( IS_DIGIT(CurChar)        ) AddChar( CurChar );
        else if ( IsChSep(CurChar)         ) { FillLexTable(); ChangeState( CurChar, stSepAn ); }
        else if ( IsEOEOrBlankSep(CurChar) ) { FillLexTable(); GoToState( stS ); }
        else                                 { FErrType = etDigSepExpected; GoToState( stError ); }
      break;

      case stSepAn:
        FillLexTable();
        GoToState( stS );
      break;

      case stError:
        bParsing = false;
      break;

      case stEnd:
        bParsing = false;
      break;
    }
  }

  if ( FState != stEnd )
  {
    FErrPos = FPos;
    FErrLen = 1;
    return false;
  }
  else
    return true;
}

//---------------------------------------------------------------------------

//��������� ������� ������� � ������� ������ (���� �� ��� ���) � ��������� ����
//������� � ����� ������ ������

void __fastcall TCalcRulesLex::FillLexTable()
{
  TLexType   tmpLexType;
  TLexClass  tmpLexClass;
  switch ( FState )
  {
    #ifdef CALCRULES_SUPPORT_NAMES
    case stIdent:
      tmpLexType = ltName; tmpLexClass = lcNumber;
    break;
    #endif

    case stDigits:
      tmpLexType  = ltFConst;
      tmpLexClass = lcNumber;
    break;

    case stEndDec_DecFloat:
      tmpLexType  = ltDConst;
      tmpLexClass = lcNumber;
    break;

    case stEndRowColNum_CellNum:
      tmpLexType  = ltRowColNum;
      tmpLexClass = lcNumber;
    break;

    case stEndCellNum:
      tmpLexType  = ltCellNum;
      tmpLexClass = lcNumber;
    break;

    case stSepAn:
      if      ( FLex == "+"  ) { tmpLexType = ltADD;   tmpLexClass = lcAddOp; }
      else if ( FLex == "-"  ) { tmpLexType = ltSUB;   tmpLexClass = lcAddOp; }
      else if ( FLex == "*"  ) { tmpLexType = ltMUL;   tmpLexClass = lcMulOp; }
      else if ( FLex == "/"  ) { tmpLexType = ltDIV;   tmpLexClass = lcMulOp; }
      else if ( FLex == "("  ) { tmpLexType = ltLeft;  tmpLexClass = lcLeft;  }
      else if ( FLex == ")"  ) { tmpLexType = ltRight; tmpLexClass = lcRight; }
    break;
  }

  //��������� ������� ������� � ������� ������
  int nLexTableID =  FLexTable->FindRec( FLex, tmpLexType, tmpLexClass );
  if ( nLexTableID == -1 )
  {
    nLexTableID = FLexTable->NewID();
    (*FLexTable->LexMap)[nLexTableID].Data  = FLex;
    (*FLexTable->LexMap)[nLexTableID].Type  = tmpLexType;
    (*FLexTable->LexMap)[nLexTableID].Class = tmpLexClass;
  }

  //��������� ������� ������� � ����� ������ ������
  LEX_LIST_ITEM LexListItem;
  LexListItem.LexTableId = nLexTableID;
  LexListItem.ExprPos    = FLexStartPos;
  FLexList->push_back( LexListItem );
  FLex = "";
  FLexStartPos = 0;
}

//***************************************************************************
//***************************** TCalcRulesSynt ******************************
//******************** �������������� ���������� ****************************
//**************** ������ ���������� ������/������� ������� *****************
//***************************************************************************
__fastcall TCalcRulesSynt::TCalcRulesSynt( TObject*   AOwner,
                                   TLexTable* ALexTable,
                                   TLexList*  ALexList,
                                   TLexList*  APolandLexList,
                                   TSyntTree* ASyntTree )
                : TExprSynt( AOwner, ALexTable, ALexList, APolandLexList, ASyntTree )
{
}
//---------------------------------------------------------------------------
//�������� ��������������� ������

bool __fastcall TCalcRulesSynt::CheckOp(TSyntTreeNode* SyntTreeNode, void *p1 )
{
  bool Result = false;
  LEX_LIST_ITEM &LexListItem = *((PLEX_LIST_ITEM)p1);
  LEX_TABLE_REC LexTableRec = FLexTable->GetRec( SyntTreeNode->Lex );

  //�������� �� ������� ������������ ����� ��������� ��� ������ ��������
  if ( FLexTable->IsOperation( SyntTreeNode->Lex ) )
    if ( !( SyntTreeNode->Right && SyntTreeNode->Left ) )
    {
      Result = true;
      LexListItem = SyntTreeNode->Lex;
      FErrType = stBadBiOp;
    }
  return Result;
}

//---------------------------------------------------------------------------

bool __fastcall TCalcRulesSynt::ParseTree( LEX_LIST_ITEM &LexListItem )
{
  bool bWasError = inherited::ParseTree( LexListItem );
  if ( !bWasError )
  {
    if ( FLexTable->IsOperand( FSyntTree->Root->Lex ) &&
         ( FSyntTree->Root->Left || FSyntTree->Root->Right )
    )
    {
      bWasError = true;
      LexListItem = FSyntTree->Root->Lex;
      FErrType = stBadCalcRulesExpr;
    }
  }
  return bWasError;
}

//***************************************************************************
//**************************** TCalcRulesSem ********************************
//******************** ������������� ���������� *****************************
//**************** ������ ���������� ������/������� ������� *****************
//***************************************************************************

//---------------------------------------------------------------------------
__fastcall TCalcRulesSem::TCalcRulesSem( TObject* AOwner, TLexTable* ALexTable, TSyntTree* ASyntTree )
 : TExprSem( AOwner, ALexTable, ASyntTree )
{
}
//---------------------------------------------------------------------------

// �����������:
//   1. ���������� ������� �� ����;
//   2. ���������� ������� ������� ���������� ��������;
//   3. ���������� ������� ������� ������� �����/��������;
//   4. ������ �����/�������� ������ ���������� � 1;
//   5. ���������� ������� ������� ������� ������ � ������� � ������ ������;
//   6. ������ ������ � ������� � ������ ������ ������ ���������� � 1;

bool __fastcall TCalcRulesSem::CheckOp(TSyntTreeNode* SyntTreeNode, void *p1 )
{
  bool Result = false;
  LEX_LIST_ITEM &LexListItem = *((PLEX_LIST_ITEM)p1);
  LEX_TABLE_REC LexTableRec = FLexTable->GetRec( SyntTreeNode->Lex );

  int nIntVal;
  bool bIsZero = false;
  TSemErrType SemErrType = mtNone;
  //�������� �� ������������ ���������� ���������, ������ ������/������� � ������ ������
  switch ( LexTableRec.Type )
  {
    case ltRowColNum:
      try
      {
        nIntVal = StrToInt( GetRowColNumLexNumber( LexTableRec.Data ) );
        if ( nIntVal < 1 )
          SemErrType = mtRowColNumBaseOne;
      }
      catch ( EConvertError& )
      {
        SemErrType = mtRowColNumTooBig;
      }
    break;

    case ltCellNum:
      try
      {
        nIntVal = StrToInt( GetCellNumLexRowNumber( LexTableRec.Data ) );
        if ( nIntVal < 1 )
        {
          SemErrType = mtCellRowNumBaseOne;
          break;
        }
      }
      catch ( EConvertError& )
      {
        SemErrType = mtCellRowNumTooBig;
        break;
      }
      try
      {
        nIntVal = StrToInt( GetCellNumLexColNumber( LexTableRec.Data ) );
        if ( nIntVal < 1 )
          SemErrType = mtCellColNumBaseOne;
      }
      catch ( EConvertError& )
      {
        SemErrType = mtCellColNumTooBig;
      }
    break;

    case ltDConst:
      try
      {
        if ( !StrToInt( LexTableRec.Data ) )
          bIsZero = true;
      }
      catch ( EConvertError& )
      {
        SemErrType = mtConstTooBig;
      }
    break;

    case ltFConst:
      try
      {
        if ( !DKStrToFloat( LexTableRec.Data, '.' ) )
          bIsZero = true;
      }
      catch ( EConvertError& )
      {
        SemErrType = mtConstTooBig;
      }
    break;
  }
  if ( SemErrType != mtNone )
  {
    Result = true;
    LexListItem = SyntTreeNode->Lex;
    FErrType = SemErrType;
  }
  //�������� ���������� ������� �� ����
  else if ( bIsZero &&
            SyntTreeNode->Parent &&
            ( FLexTable->GetRec( SyntTreeNode->Parent->Lex ).Type == ltDIV ) &&
            ( SyntTreeNode->Parent->Right == SyntTreeNode )

  )
  {
    Result = true;
    LexListItem = SyntTreeNode->Lex;
    FErrType = mtDivByZero;
  }
  return Result;
}

//***************************************************************************
//************************** TCalcRulesParser *******************************
//******************** ���������� ������ ���������� *************************
//*********************** ������/������� ������� ****************************
//***************************************************************************

//---------------------------------------------------------------------------
__fastcall TCalcRulesParser::TCalcRulesParser( UnicodeString AExpr)
 : TExprParser( AExpr )
{
}
//---------------------------------------------------------------------------
void __fastcall TCalcRulesParser::InitParsers()
{
  FExprLex  = new TCalcRulesLex ( this, FLexTable, &FLexList, FExpr);
  FExprSynt = new TCalcRulesSynt( this, FLexTable, &FLexList, &FPolandLexList, FSyntTree );
  FExprSem  = new TCalcRulesSem ( this, FLexTable, FSyntTree );
}

//***************************************************************************
//************************** THandRulesLex **********************************
//******************** ����������� ���������� *******************************
//********** ������ �������� ������� ����� ������ � ������� *****************
//***************************************************************************

__fastcall THandRulesLex::THandRulesLex( TObject *AOwner, TLexTable* ALexTable, TLexList* ALexList, UnicodeString AExpr )
        : TExprLex( AOwner, ALexTable, ALexList, AExpr )
{
  FSignSeps.insert('+');
  FSignSeps.insert('-');

  FChOp = FSignSeps;
  FChOp.insert('*');
  FChOp.insert('/');
  FChOp.insert('=');
  FChOp.insert('<');
  FChOp.insert('>');

  FChSeps = FChOp;
  FChSeps.insert('(');
  FChSeps.insert(')');

  FBlankSeps.insert(' ');
  FBlankSeps.insert('\t');
}

//---------------------------------------------------------------------------

bool __fastcall THandRulesLex::Parse()
{
  inherited::Parse();

  bool bParsing = true;
  char CurChar;
  char cTmpNext, cTmpPrev;
  int nErrPos = -1;

  //����� ����� �������
  while( bParsing )
  {
    if ( ( FState != stSepAn          ) &&
         ( FState != stError          ) &&
         ( FState != stPossibleAConst ) )
      CurChar = GetNextChar();
    switch ( FState )
    {
      case stS:
        if      ( IsLET(CurChar)      ) ChangeState( CurChar, stIdent );
        #ifdef HANDRULES_SUPPORT_STRCONSTS
        else if ( CurChar == '"'      ) ChangeState( CurChar, stLiteral );
        #else
        else if ( CurChar == '"'      ) { FErrType = etUnexpectedChar; GoToState ( stError ); }
        #endif
        else if ( IsSignSep(CurChar)  ) GoToState  ( stPossibleAConst );
        else if ( CurChar == '.'      ) ChangeState( CurChar, stDrob );
        else if ( IS_DIGIT(CurChar)   ) ChangeState( CurChar, stEndDec_DecFloat );
        else if ( IsBlankSep(CurChar) ) ;
        else if ( CurChar == '#'      ) ChangeState( CurChar, stRowColNum_CellNum );        
        else if ( IsChSep(CurChar)    ) GoToState  ( stSepAn );
        else if ( CurChar == EOE      )
        {
          if ( FLexTable->Empty() )
          {
            FErrType = etEmptyExpr;
            GoToState  ( stError );
          }
          else
            GoToState  ( stEnd   );
        }
        else                            { FErrType = etUnexpectedChar; GoToState ( stError ); }
      break;

      case stIdent:
        if      ( IsID_CHAR(CurChar)       ) AddChar( CurChar );
        else if ( IsChSep(CurChar) ||
                  IsEOEOrBlankSep(CurChar)
        )
        {
          #ifndef HANDRULES_SUPPORT_NAMES
            if ( ( FLex != FMT(icsDocParsersNotConst)  ) && ( FLex != FMT(icsDocParsersAndConst) ) && ( FLex != FMT(icsDocParsersOrConst) ) )
            {
              FErrType = etUnexpectedChar;
              nErrPos = FPos - FLex.Length();
              GoToState ( stError );
              break;
            }
          #endif
          FillLexTable();
          if ( IsChSep(CurChar) )
            GoToState( stSepAn );
          else
            GoToState( stS );
        }
        else                                 { FErrType = etUnexpectedChar; GoToState  ( stError ); }
      break;

      #ifdef HANDRULES_SUPPORT_STRCONSTS
      case stLiteral:
        if      ( CurChar == '"' ) ChangeState( CurChar, stSep );
        else if ( CurChar == EOE ) { FErrType = etUnexpectedEOE; GoToState( stError ); }
        else                       AddChar( CurChar );
      break;

      case stSep:
        if      ( IsChSep(CurChar)         ) { FillLexTable(); GoToState( stSepAn ); }
        else if ( IsEOEOrBlankSep(CurChar) ) { FillLexTable(); GoToState( stS ); }
        else                                 { FErrType = etSepExpected; GoToState( stError ); }
      break;
      #endif

      case stPossibleAConst:
        cTmpNext = LookUpNextChar();
        if ( IS_DIGIT(cTmpNext) || ( cTmpNext == '.') )
        {
          cTmpPrev = LookUpPrevNotBlankChar();
            if ( (cTmpPrev == EMPTY_CHAR) || (cTmpPrev == '(') || IsChOp(cTmpPrev) )
            {
              AddChar(CurChar);
              GoToState( stS );
            }
        }
        if ( FState == stPossibleAConst )
          GoToState( stSepAn );
      break;

      case stDrob:
        if ( IS_DIGIT(CurChar) )    ChangeState( CurChar, stDigits );
        else                        { FErrType = etDigExpected; GoToState( stError ); }
      break;

      case stDigits:
        if      ( IS_DIGIT(CurChar)        ) AddChar( CurChar );
        else if ( IsChSep(CurChar)         ) { FillLexTable(); GoToState( stSepAn ); }
        else if ( IsEOEOrBlankSep(CurChar) ) { FillLexTable(); GoToState( stS ); }
        else                                 { FErrType = etDigSepExpected; GoToState( stError ); }
      break;

      case stEndDec_DecFloat:
        if      ( CurChar == '.'           ) ChangeState( CurChar, stDrob );
        else if ( IS_DIGIT(CurChar)        ) AddChar( CurChar );
        else if ( IsChSep(CurChar)         ) { FillLexTable(); GoToState( stSepAn ); }
        else if ( IsEOEOrBlankSep(CurChar) ) { FillLexTable(); GoToState( stS ); }
        else                                 { FErrType = etDigSepDotExpected; GoToState( stError ); }
      break;

      case stRowColNum_CellNum:
        if      ( IS_DIGIT(CurChar)        ) ChangeState( CurChar, stEndRowColNum_CellNum );
        else                                 { FErrType = etDigExpected; GoToState( stError ); }
      break;

      case stEndRowColNum_CellNum:
        if      ( IS_DIGIT(CurChar)        ) AddChar( CurChar );
        else if ( CurChar == ':'           ) ChangeState( CurChar, stCellNum );
        else if ( IsChSep(CurChar)         ) { FillLexTable(); GoToState( stSepAn ); }
        else if ( IsEOEOrBlankSep(CurChar) ) { FillLexTable(); GoToState( stS ); }
        else                                 { FErrType = etDigSepColonExpected; GoToState( stError ); }
      break;

      case stCellNum:
        if      ( IS_DIGIT(CurChar)        ) ChangeState( CurChar, stEndCellNum );
        else                                 { FErrType = etDigExpected; GoToState( stError ); }
      break;

      case stEndCellNum:
        if      ( IS_DIGIT(CurChar)        ) AddChar( CurChar );
        else if ( IsChSep(CurChar)         ) { FillLexTable(); GoToState( stSepAn ); }
        else if ( IsEOEOrBlankSep(CurChar) ) { FillLexTable(); GoToState( stS ); }
        else                                 { FErrType = etDigSepExpected; GoToState( stError ); }
      break;

      case stSepAn:
        AddChar( CurChar );
        if      ( CurChar == '<' )
        {
          CurChar = GetNextChar();
          if ( ( CurChar == '>' ) || ( CurChar == '=' ) ) AddChar( CurChar );
          else                                            CurChar = GetPrevChar();
        }
        else if ( CurChar == '>' )
        {
          CurChar = GetNextChar();
          if ( CurChar == '=' ) AddChar( CurChar );
          else                  CurChar = GetPrevChar();
        }
        FillLexTable();
        GoToState( stS );
      break;

      case stError:
        bParsing = false;
      break;

      case stEnd:
        bParsing = false;
      break;
    }
  }

  if ( FState != stEnd )
  {
    FErrPos = ( nErrPos == -1 ) ? FPos : nErrPos;
    FErrLen = 1;
    return false;
  }
  else
    return true;
}

//---------------------------------------------------------------------------

//��������� ������� ������� � ������� ������ (���� �� ��� ���) � ��������� ����
//������� � ����� ������ ������

void __fastcall THandRulesLex::FillLexTable()
{
  TLexType   tmpLexType;
  TLexClass  tmpLexClass;
  switch ( FState )
  {
    case stIdent:
      if      ( FLex == FMT(icsDocParsersNotConst)  ) { tmpLexType = ltNOT;  tmpLexClass = lcSingleOp; }
      else if ( FLex == FMT(icsDocParsersAndConst)   ) { tmpLexType = ltAND;  tmpLexClass = lcMulOp;    }
      else if ( FLex == FMT(icsDocParsersOrConst) ) { tmpLexType = ltOR;   tmpLexClass = lcAddOp;    }
      #ifdef HANDRULES_SUPPORT_NAMES
      else                      { tmpLexType = ltName; tmpLexClass = lcUnknown;  }
      #endif
    break;

    #ifdef HANDRULES_SUPPORT_STRCONSTS
    case stSep:
      tmpLexType  = ltSConst;
      tmpLexClass = lcString;
    break;
    #endif

    case stDigits:
      tmpLexType  = ltFConst;
      tmpLexClass = lcNumber;
    break;

    case stEndDec_DecFloat:
      tmpLexType  = ltDConst;
      tmpLexClass = lcNumber;
    break;

    case stEndRowColNum_CellNum:
      tmpLexType  = ltRowColNum;
      tmpLexClass = lcNumber;
    break;

    case stEndCellNum:
      tmpLexType  = ltCellNum;
      tmpLexClass = lcNumber;
    break;

    case stSepAn:
      if      ( FLex == "+"  ) { tmpLexType = ltADD;   tmpLexClass = lcAddOp; }
      else if ( FLex == "-"  ) { tmpLexType = ltSUB;   tmpLexClass = lcAddOp; }
      else if ( FLex == "*"  ) { tmpLexType = ltMUL;   tmpLexClass = lcMulOp; }
      else if ( FLex == "/"  ) { tmpLexType = ltDIV;   tmpLexClass = lcMulOp; }
      else if ( FLex == "="  ) { tmpLexType = ltEQ;    tmpLexClass = lcRelOp; }
      else if ( FLex == "<>" ) { tmpLexType = ltNE;    tmpLexClass = lcRelOp; }
      else if ( FLex == "<"  ) { tmpLexType = ltLT;    tmpLexClass = lcRelOp; }
      else if ( FLex == ">"  ) { tmpLexType = ltGT;    tmpLexClass = lcRelOp; }
      else if ( FLex == "<=" ) { tmpLexType = ltLE;    tmpLexClass = lcRelOp; }
      else if ( FLex == ">=" ) { tmpLexType = ltGE;    tmpLexClass = lcRelOp; }
      else if ( FLex == "("  ) { tmpLexType = ltLeft;  tmpLexClass = lcLeft;  }
      else if ( FLex == ")"  ) { tmpLexType = ltRight; tmpLexClass = lcRight; }
    break;
  }

  //��������� ������� ������� � ������� ������
  int nLexTableID =  FLexTable->FindRec( FLex, tmpLexType, tmpLexClass );
  if ( nLexTableID == -1 )
  {
    nLexTableID = FLexTable->NewID();
    (*FLexTable->LexMap)[nLexTableID].Data  = FLex;
    (*FLexTable->LexMap)[nLexTableID].Type  = tmpLexType;
    (*FLexTable->LexMap)[nLexTableID].Class = tmpLexClass;
  }

  //��������� ������� ������� � ����� ������ ������
  LEX_LIST_ITEM LexListItem;
  LexListItem.LexTableId = nLexTableID;
  LexListItem.ExprPos    = FLexStartPos;
  FLexList->push_back( LexListItem );
  FLex = "";
  FLexStartPos = 0;
}

//***************************************************************************
//***************************** THandRulesSynt ******************************
//******************** �������������� ���������� ****************************
//********** ������ �������� ������� ����� ������ � ������� *****************
//***************************************************************************

//---------------------------------------------------------------------------
__fastcall THandRulesSynt::THandRulesSynt( TObject*   AOwner,
                                   TLexTable* ALexTable,
                                   TLexList*  ALexList,
                                   TLexList*  APolandLexList,
                                   TSyntTree* ASyntTree )
                : TExprSynt( AOwner, ALexTable, ALexList, APolandLexList, ASyntTree )
{
}
//---------------------------------------------------------------------------
__fastcall THandRulesSynt::~THandRulesSynt()
{
}
//---------------------------------------------------------------------------

//�������� ��������������� ������ �� ������ ������ � ���� �����'�

bool __fastcall THandRulesSynt::CreateSyntTree( LEX_LIST_ITEM &LexListItem )
{
  LEX_TABLE_REC LexTableRec = FLexTable->GetRec( *(FPolandLexList->rbegin()) );
  if      ( !FLexTable->IsOperation(LexTableRec) )       //���� - �� ��������, �� ������
  {
    FErrType = stNoneOp;
    return false;
  }
  else if ( !FLexTable->IsLOp(LexTableRec) &&           //���� - �� ���������� �������� � �� ��������
            !FLexTable->IsRelOp(LexTableRec)            //���������, �� ������
  )
  {
    FErrType = stBadHandRulesExpr;
    return false;
  }
  else
    return inherited::CreateSyntTree( LexListItem );
}

//---------------------------------------------------------------------------

//�������� ��������������� ������

bool __fastcall THandRulesSynt::CheckOp(TSyntTreeNode* SyntTreeNode, void *p1 )
{
  bool Result = false;
  LEX_LIST_ITEM &LexListItem = *((PLEX_LIST_ITEM)p1);
  LEX_TABLE_REC LexTableRec = FLexTable->GetRec( SyntTreeNode->Lex );

  //�������� �� ������� ������������ ����� ��������� ��� ������ ��������
  switch ( LexTableRec.Class )
  {
    case lcSingleOp:
      if ( !( SyntTreeNode->Right && ( !SyntTreeNode->Left ) ) )
      {
        Result = true;
        LexListItem = SyntTreeNode->Lex;
        FErrType = stBadSingleOp;
      }
    break;
    case lcMulOp:
    case lcAddOp:
    case lcRelOp:
      if ( !( SyntTreeNode->Right && SyntTreeNode->Left ) )
      {
        Result = true;
        LexListItem = SyntTreeNode->Lex;
        FErrType = stBadBiOp;
      }
    break;
  }

  //�������� �� ������������ ���� �������� � ����� ���������. ��� ���������� �������� -
  //���������� �������� (���������� ������ ���������� �������� ��� �������� ���������),
  //��� �������������� �������� - �������������� �������� ( �����, �������������� ���������,
  //������ �����/�������� ��� ���������� ������ �������������� ��������),
  //��� �������� ��������� - �����, ���������, ������ �����/�������� ��� ����������
  //�������������� ��������
  if ( !Result )
  {
    //���������� ��������
    if      ( FLexTable->IsLOp(LexTableRec) )
    {
      if ( !( FLexTable->IsRelOp( SyntTreeNode->Right->Lex ) ||
              FLexTable->IsLOp  ( SyntTreeNode->Right->Lex ) ) )
      {
        LexListItem = SyntTreeNode->Lex;
        FErrType = stBadLOp;
        return true;
      }
      if ( LexTableRec.Class != lcSingleOp )
        if ( !( FLexTable->IsRelOp( SyntTreeNode->Left->Lex ) ||
                FLexTable->IsLOp  ( SyntTreeNode->Left->Lex ) ) )
        {
          LexListItem = SyntTreeNode->Lex;
          FErrType = stBadLOp;
          return true;
        }
    }

    //�������������� ��������
    else if ( FLexTable->IsAOp  (LexTableRec) )
    {
      if ( !( ( FLexTable->IsAOp ( SyntTreeNode->Right->Lex ) ||
                ( FLexTable->GetRec( SyntTreeNode->Right->Lex ).Type  == ltName   ) ||
                ( FLexTable->GetRec( SyntTreeNode->Right->Lex ).Class == lcNumber ) ) &&
              ( FLexTable->IsAOp ( SyntTreeNode->Left ->Lex ) ||
                ( FLexTable->GetRec( SyntTreeNode->Left ->Lex ).Type  == ltName   ) ||
                ( FLexTable->GetRec( SyntTreeNode->Left ->Lex ).Class == lcNumber ) ) ) )
      {
        LexListItem = SyntTreeNode->Lex;
        FErrType = stBadAOp;
        return true;
      }
    }

    //�������� ���������
    else if ( FLexTable->IsRelOp(LexTableRec) )
    {
      if ( !( ( FLexTable->IsAOp ( SyntTreeNode->Right->Lex ) ||
                ( FLexTable->GetRec( SyntTreeNode->Right->Lex ).Type  == ltName   ) ||
                ( FLexTable->GetRec( SyntTreeNode->Right->Lex ).Class == lcNumber ) ||
                ( FLexTable->GetRec( SyntTreeNode->Right->Lex ).Class == lcString ) ) &&
              ( FLexTable->IsAOp ( SyntTreeNode->Left ->Lex ) ||
                ( FLexTable->GetRec( SyntTreeNode->Left ->Lex ).Type  == ltName   ) ||
                ( FLexTable->GetRec( SyntTreeNode->Left ->Lex ).Class == lcNumber ) ||
                ( FLexTable->GetRec( SyntTreeNode->Left ->Lex ).Class == lcString ) ) ) )
      {
        LexListItem = SyntTreeNode->Lex;
        FErrType = stBadRelOp;
        return true;
      }
    }
  }

  return Result;
}

//***************************************************************************
//**************************** THandRulesSem ********************************
//******************** ������������� ���������� *****************************
//********** ������ �������� ������� ����� ������ � ������� *****************
//***************************************************************************

//---------------------------------------------------------------------------
__fastcall THandRulesSem::THandRulesSem( TObject* AOwner, TLexTable* ALexTable, TSyntTree* ASyntTree )
 : TExprSem( AOwner, ALexTable, ASyntTree )
{
}
//---------------------------------------------------------------------------
bool __fastcall THandRulesSem::ParseTree( LEX_LIST_ITEM &LexListItem )
{
  bool bWasError = !FSyntTree->Root->Iterate1( CheckOp, &LexListItem );
  if ( !bWasError )
    do  //��������� �������������� ������ ���� ��� ������ � ����������� ������� ������
    {
      FLexTable->Modified = false;
      bWasError = !FSyntTree->Root->Iterate1( CheckOpNames, &LexListItem );
    }
    while( FLexTable->Modified && (!bWasError) );

  return bWasError;
}

//---------------------------------------------------------------------------

// �����������:
//   1. ���������� ������� �� ����;
//   2. ���������� ������� ������� ���������� ��������;
//   3. ���������� ������� ������� ������� �����/��������;
//   4. ������ �����/�������� ������ ���������� � 1;
//   5. ���������� ������� ������� ������� ������ � ������� � ������ ������;
//   6. ������ ������ � ������� � ������ ������ ������ ���������� � 1;

bool __fastcall THandRulesSem::CheckOp(TSyntTreeNode* SyntTreeNode, void *p1 )
{
  bool Result = false;
  LEX_LIST_ITEM &LexListItem = *((PLEX_LIST_ITEM)p1);
  LEX_TABLE_REC LexTableRec = FLexTable->GetRec( SyntTreeNode->Lex );

  int nIntVal;
  bool bIsZero = false;
  TSemErrType SemErrType = mtNone;
  //�������� �� ������������ ���������� ���������, ������ ������/������� � ������ ������
  switch ( LexTableRec.Type )
  {
    case ltRowColNum:
      try
      {
        nIntVal = StrToInt( GetRowColNumLexNumber( LexTableRec.Data ) );
        if ( nIntVal < 1 )
          SemErrType = mtRowColNumBaseOne;
      }
      catch ( EConvertError& )
      {
        SemErrType = mtRowColNumTooBig;
      }
    break;

    case ltCellNum:
      try
      {
        nIntVal = StrToInt( GetCellNumLexRowNumber( LexTableRec.Data ) );
        if ( nIntVal < 1 )
        {
          SemErrType = mtCellRowNumBaseOne;
          break;
        }
      }
      catch ( EConvertError& )
      {
        SemErrType = mtCellRowNumTooBig;
        break;
      }
      try
      {
        nIntVal = StrToInt( GetCellNumLexColNumber( LexTableRec.Data ) );
        if ( nIntVal < 1 )
          SemErrType = mtCellColNumBaseOne;
      }
      catch ( EConvertError& )
      {
        SemErrType = mtCellColNumTooBig;
      }
    break;
    
    case ltDConst:
      try
      {
        if ( !StrToInt( LexTableRec.Data ) )
          bIsZero = true;
      }
      catch ( EConvertError& )
      {
        SemErrType = mtConstTooBig;
      }
    break;

    case ltFConst:
      try
      {
        if ( !DKStrToFloat( LexTableRec.Data, '.' ) )
          bIsZero = true;
      }
      catch ( EConvertError& )
      {
        SemErrType = mtConstTooBig;
      }
    break;
  }
  if ( SemErrType != mtNone )
  {
    Result = true;
    LexListItem = SyntTreeNode->Lex;
    FErrType = SemErrType;
  }
  //�������� ���������� ������� �� ����
  else if ( bIsZero &&
            SyntTreeNode->Parent &&
            ( FLexTable->GetRec( SyntTreeNode->Parent->Lex ).Type == ltDIV ) &&
            ( SyntTreeNode->Parent->Right == SyntTreeNode )

  )
  {
    Result = true;
    LexListItem = SyntTreeNode->Lex;
    FErrType = mtDivByZero;
  }
  return Result;
}

//---------------------------------------------------------------------------

// �����������:
//   1. ��� ������ ���� ltName ������������ (���� ��������) �� ���
//      ( �������������� ��� ���������� ), � ������� ������ ����������� ���������� � ����
//      ���� ����� �� ����������

bool __fastcall THandRulesSem::CheckOpNames(TSyntTreeNode* SyntTreeNode, void *p1 )
{
  return false;
}

//***************************************************************************
//************************** THandRulesParser *******************************
//******************** ���������� ������ �������� ***************************
//****************** ������� ����� ������ � ������� *************************
//***************************************************************************

//---------------------------------------------------------------------------
__fastcall THandRulesParser::THandRulesParser( UnicodeString AExpr)
 : TExprParser( AExpr )
{
}
//---------------------------------------------------------------------------

void __fastcall THandRulesParser::InitParsers()
{
  FExprLex  = new THandRulesLex ( this, FLexTable, &FLexList, FExpr);
  FExprSynt = new THandRulesSynt( this, FLexTable, &FLexList, &FPolandLexList, FSyntTree );
  FExprSem  = new THandRulesSem ( this, FLexTable, FSyntTree );
}

//***************************************************************************
//********************** TBaseRulesCalc *************************************
//***************************************************************************

__fastcall TBaseRulesCalc::TBaseRulesCalc( UnicodeString APolandStr  ) : TObject()
{
  FPolandStr = Trim(APolandStr);
}


//---------------------------------------------------------------------------

void __fastcall TBaseRulesCalc::SetPolandStr( UnicodeString AValue )
{
  FPolandStr = Trim(AValue);
}

//---------------------------------------------------------------------------

double __fastcall TBaseRulesCalc::Calc()
{
  if ( FPolandStr == "" )
    throw DKClasses::EMethodError(__FUNC__, cFMT(icsDocParsersErrorPolandStr));
  return 0;
}

//***************************************************************************
//************************** THandRulesCalc *********************************
//******************** ���������� ���������, ������������ *******************
//******************* ������� �������� ������� ����� ������ *****************
//***************************************************************************

//---------------------------------------------------------------------------
__fastcall THandRulesCalc::THandRulesCalc( UnicodeString APolandStr)
: TBaseRulesCalc( APolandStr )
{
}
//---------------------------------------------------------------------------

double __fastcall THandRulesCalc::Calc()
{
  inherited::Calc();
  double fResult = -1;
  TStringList *SL;
  try
  {
    SL = new TStringList;
    if ( SL )
    {
      SL->Text = StringReplace( PolandStr, " ", "\n", TReplaceFlags() << rfReplaceAll );
      bool IsCond;
      try
      {
        fResult = CalcCondExp( SL, &IsCond );
      }
      catch(EZeroDivide&)
      {
        fResult = 0;
      }
    }
  }
  __finally
  {
    if ( SL ) delete SL;
  }
  return fResult;
}

//***************************************************************************
//************************** TCalcRulesCalc *********************************
//******************** ���������� ���������, ������������ *******************
//************************** ������� ���������� *****************************
//***************************************************************************

//---------------------------------------------------------------------------
__fastcall TCalcRulesCalc::TCalcRulesCalc( UnicodeString APolandStr)
  : TBaseRulesCalc( APolandStr )
{
}
//---------------------------------------------------------------------------
__fastcall TCalcRulesCalc::~TCalcRulesCalc()
{
}
//---------------------------------------------------------------------------
double __fastcall TCalcRulesCalc::Calc()
{
  inherited::Calc();
  double fResult = 0;
  TStringList *SL;
  try
  {
    SL = new TStringList;
    if ( SL )
    {
      SL->Text = StringReplace( PolandStr, " ", "\n", TReplaceFlags() << rfReplaceAll );
      try
      {
        fResult = CalcExpression( SL );
      }
      catch(EZeroDivide&)
      {
        fResult = 0;
      }
    }
  }
  __finally
  {
    if ( SL ) delete SL;
  }
  return fResult;
}

//***************************************************************************
//********************** ���������� ������� *********************************
//***************************************************************************

//���������� ����� ������/������� �� ��������������� ������� (�.�. ��� #)

UnicodeString __fastcall ICSDocParsers::GetRowColNumLexNumber ( UnicodeString ARowColNumLex )
{
  return ARowColNumLex.SubString( 2, ARowColNumLex.Length() - 1 );
}

//---------------------------------------------------------------------------

//���������� ����� ������ �� �������, ��������������� ������ ������

UnicodeString __fastcall ICSDocParsers::GetCellNumLexRowNumber( UnicodeString ACellNumLex   )
{
  return ACellNumLex.SubString( 2, ACellNumLex.Pos(":") - 2 );
}

//---------------------------------------------------------------------------

//���������� ����� ������� �� �������, ��������������� ������ ������

UnicodeString __fastcall ICSDocParsers::GetCellNumLexColNumber( UnicodeString ACellNumLex   )
{
  return ACellNumLex.SubString( ACellNumLex.Pos(":") + 1, ACellNumLex.Length() - ACellNumLex.Pos(":") );
}

//---------------------------------------------------------------------------

