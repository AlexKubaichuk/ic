//---------------------------------------------------------------------------
#ifndef dsDocSpecEditUnitH
#define dsDocSpecEditUnitH

//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
//---------------------------------------------------------------------------
#include "ICSDocBaseEdit.h"
#include "ICSDocSpec.h"
#include "dsDocDMUnit.h"
//---------------------------------------------------------------------------
class PACKAGE TdsDocSpecEditForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *Panel1;
        TcxButton *SaveBtn;
        TcxButton *CancelBtn;
        void __fastcall CancelBtnClick(TObject *Sender);
        void __fastcall SaveBtnClick(TObject *Sender);
        void __fastcall SpecEditorGetOrgsList(TObject *Sender,
          TUnicodeStringMap &SpecsList);
        void __fastcall SpecEditorGetSpecificatorsList(TObject *Sender,
          TUnicodeStringMap &SpecsList);
        bool __fastcall SpecEditorGetSpecNode(TObject *Sender,
          const UnicodeString SpecGUI, TTagNode *&SpecTagNode);
        void __fastcall SpecEditorGetSpecOwner(TObject *Sender,
          UnicodeString AAutorCode, UnicodeString &AAutorName);
        bool __fastcall SpecEditorLoadFilter(UnicodeString ACondName,
          TTagNode *Flt);
 void __fastcall FormDestroy(TObject *Sender);
private:	// User declarations
        TTagNode *FSpec;
        bool IsEdit;
        TAxeXMLContainer *XMList;
        TdsDocDM *FDM;
        TICSDocSpec *SpecEditor;
//  TOnCondCanCloseEvent   FCanClose;
public:		// User declarations
        __fastcall TdsDocSpecEditForm(TComponent* Owner, TTagNode *ASpec, TAxeXMLContainer *AXMList, UnicodeString AAutorCode, UnicodeString APeriodDef, bool AIsEdit,TdsDocDM *ADM, bool ADoc);
//  __property TOnCondCanCloseEvent   OnCanClose       = { read = FCanClose, write = FCanClose};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocSpecEditForm *dsDocSpecEditForm;
//---------------------------------------------------------------------------
#endif
