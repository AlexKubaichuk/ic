/*
  ����        - PeriodEdit.h
  ������      - ��������������� (ICS)
  ��������    - ������ �������������� �������� �������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 29.12.2006
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  29.12.2006
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef PeriodEditH
#define PeriodEditH

//---------------------------------------------------------------------------

#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxCalendar.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDateUtils.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxSpinEdit.hpp"
#include "cxTextEdit.hpp"
#include "dxCore.hpp"
//---------------------------------------------------------------------------
//#include "ICSDoc.h"  //��������� �������� � ���� ��������
#include <map>
//---------------------------------------------------------------------------


using namespace std;

namespace PeriodEdit
{

//###########################################################################
//##                                                                       ##
//##                                 Globals                               ##
//##                                                                       ##
//###########################################################################

enum TPeriodDef
{
  pdUnDef    = 0,
  pdYear     = 1,
  pdHalfYear = 2,
  pdQuoter   = 3,
  pdMonth    = 4,
  pdUnique   = 5
};

//###########################################################################
//##                                                                       ##
//##                             TPeriodEditFM                             ##
//##                                                                       ##
//###########################################################################

class PACKAGE TPeriodEditFM : public TForm
{
__published:	// IDE-managed Components
    TPanel *BottomPn;
    TButton *OkBtn;
    TButton *CancelBtn;
    TGroupBox *ClientGBx;
    TRadioButton *YearRBtn;
    TRadioButton *HalfYearRBtn;
    TRadioButton *QuoterRBtn;
    TRadioButton *MonthRBtn;
    TRadioButton *UniqueRBtn;
    TRadioButton *UnDefRBtn;
    TLabel *HalfYearYearLb;
    TLabel *QuoterYearLb;
    TLabel *MonthYearLb;
    TLabel *UniqueSepLb;
  TcxSpinEdit *YearEd;
  TcxSpinEdit *HalfYearYearEd;
  TcxSpinEdit *QuoterYearEd;
  TcxSpinEdit *MonthYearEd;
  TcxComboBox *HalfYearCBx;
  TcxComboBox *QuoterCBx;
  TcxComboBox *MonthCBx;
  TcxDateEdit *UniqueBegEd;
  TcxDateEdit *UniqueEndEd;
    void __fastcall DefRBtnClick(TObject *Sender);
    void __fastcall OkBtnClick(TObject *Sender);
    void __fastcall CancelBtnClick(TObject *Sender);


private:	// User declarations
    typedef map<TPeriodDef, TRadioButton*> TPeriodDefMap;
    typedef map<TRadioButton*, TPeriodDef> TPeriodDefCodeMap;

    TPeriodDefMap     FPeriodDefMap;
    TPeriodDefCodeMap FPeriodDefCodeMap;
    TPeriodDef FPeriodDef;


protected:
    void __fastcall SetPeriodDef(TPeriodDef AValue);
    UnicodeString __fastcall GetPeriodDefXMLStr() const;
    void __fastcall SetPeriodDefXMLStr(const UnicodeString AValue);
    UnicodeString __fastcall GetPeriodValueXMLStr() const;
    void __fastcall SetPeriodValueXMLStr(const UnicodeString AValue);
    void __fastcall SetYearCtrlsValue(unsigned short AYear);
    void __fastcall UpdateCtrlsEnabled(TPeriodDef ADef, bool AEnabled);
    bool __fastcall ValidateInputs(bool ASilent) const;


public:		// User declarations
    __fastcall TPeriodEditFM(TComponent* Owner);

    // ��� �������
    __property TPeriodDef PeriodDef = {read = FPeriodDef, write = SetPeriodDef};
    /*
      ��� ������� � ������� xml:
        year     - ���
        halfyear - ���������
        quarter  - �������
        month    - �����
        unique   - �������� ���
        undef    - �� ���������
    */
    __property UnicodeString PeriodDefXMLStr = {read = GetPeriodDefXMLStr, write = SetPeriodDefXMLStr};
    /*
      �������� ������� � ������� xml:
        <������>-<���������>

        <������>, <���������> ::= DD.MM.YYYY
        DD   - ����
        MM   - �����
        YYYY - ���
        <������> <= <���������>
    */
    __property UnicodeString PeriodValueXMLStr = {read = GetPeriodValueXMLStr, write = SetPeriodValueXMLStr};
};

//---------------------------------------------------------------------------

} // end of namespace PeriodEdit
using namespace PeriodEdit;

#endif
