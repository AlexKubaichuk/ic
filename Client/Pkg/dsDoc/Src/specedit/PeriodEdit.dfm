object PeriodEditFM: TPeriodEditFM
  Left = 477
  Top = 526
  BorderStyle = bsDialog
  BorderWidth = 8
  Caption = #1055#1077#1088#1080#1086#1076
  ClientHeight = 221
  ClientWidth = 330
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BottomPn: TPanel
    Left = 0
    Top = 188
    Width = 330
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object OkBtn: TButton
      Left = 174
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Ok'
      Default = True
      TabOrder = 0
      OnClick = OkBtnClick
    end
    object CancelBtn: TButton
      Left = 255
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = CancelBtnClick
    end
  end
  object ClientGBx: TGroupBox
    Left = 0
    Top = 0
    Width = 330
    Height = 188
    Align = alClient
    Caption = #1047#1085#1072#1095#1077#1085#1080#1077' '#1087#1077#1088#1080#1086#1076#1072
    TabOrder = 0
    object HalfYearYearLb: TLabel
      Left = 208
      Top = 76
      Width = 19
      Height = 13
      Caption = #1043#1086#1076
    end
    object QuoterYearLb: TLabel
      Left = 208
      Top = 105
      Width = 19
      Height = 13
      Caption = #1043#1086#1076
    end
    object MonthYearLb: TLabel
      Left = 208
      Top = 134
      Width = 19
      Height = 13
      Caption = #1043#1086#1076
    end
    object UniqueSepLb: TLabel
      Left = 208
      Top = 163
      Width = 5
      Height = 13
      Caption = '-'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object YearRBtn: TRadioButton
      Left = 8
      Top = 45
      Width = 113
      Height = 17
      Caption = #1043#1086#1076
      TabOrder = 1
      OnClick = DefRBtnClick
    end
    object HalfYearRBtn: TRadioButton
      Left = 8
      Top = 74
      Width = 113
      Height = 17
      Caption = #1055#1086#1083#1091#1075#1086#1076#1080#1077
      TabOrder = 2
      OnClick = DefRBtnClick
    end
    object QuoterRBtn: TRadioButton
      Left = 8
      Top = 103
      Width = 113
      Height = 17
      Caption = #1050#1074#1072#1088#1090#1072#1083
      TabOrder = 3
      OnClick = DefRBtnClick
    end
    object MonthRBtn: TRadioButton
      Left = 8
      Top = 132
      Width = 113
      Height = 17
      Caption = #1052#1077#1089#1103#1094
      TabOrder = 4
      OnClick = DefRBtnClick
    end
    object UniqueRBtn: TRadioButton
      Left = 8
      Top = 161
      Width = 113
      Height = 17
      Caption = #1059#1085#1080#1082#1072#1083#1100#1085#1099#1081
      TabOrder = 5
      OnClick = DefRBtnClick
    end
    object UnDefRBtn: TRadioButton
      Left = 8
      Top = 16
      Width = 113
      Height = 17
      Caption = #1053#1077' '#1086#1087#1088#1077#1076#1077#1083#1077#1085
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = DefRBtnClick
    end
    object HalfYearCBx: TcxComboBox
      Left = 102
      Top = 70
      Properties.DropDownListStyle = lsEditFixedList
      Properties.Items.Strings = (
        #1055#1077#1088#1074#1086#1077
        #1042#1090#1086#1088#1086#1077)
      TabOrder = 6
      Text = #1055#1077#1088#1074#1086#1077
      Width = 90
    end
  end
  object YearEd: TcxSpinEdit
    Left = 102
    Top = 43
    Properties.MaxValue = 9999.000000000000000000
    Properties.MinValue = 1.000000000000000000
    TabOrder = 1
    Value = 1
    Width = 90
  end
  object HalfYearYearEd: TcxSpinEdit
    Left = 232
    Top = 70
    Properties.MaxValue = 9999.000000000000000000
    Properties.MinValue = 1.000000000000000000
    TabOrder = 3
    Value = 1
    Width = 90
  end
  object QuoterYearEd: TcxSpinEdit
    Left = 232
    Top = 103
    Properties.MaxValue = 9999.000000000000000000
    Properties.MinValue = 1.000000000000000000
    TabOrder = 4
    Value = 1
    Width = 90
  end
  object MonthYearEd: TcxSpinEdit
    Left = 232
    Top = 132
    Properties.MaxValue = 9999.000000000000000000
    Properties.MinValue = 1.000000000000000000
    TabOrder = 5
    Value = 1
    Width = 90
  end
  object QuoterCBx: TcxComboBox
    Left = 102
    Top = 103
    Properties.DropDownListStyle = lsEditFixedList
    Properties.Items.Strings = (
      #1055#1077#1088#1074#1099#1081
      #1042#1090#1086#1088#1086#1081
      #1058#1088#1077#1090#1080#1081
      #1063#1077#1090#1074#1077#1088#1090#1099#1081)
    TabOrder = 6
    Text = #1055#1077#1088#1074#1099#1081
    Width = 90
  end
  object MonthCBx: TcxComboBox
    Left = 102
    Top = 132
    Properties.DropDownListStyle = lsEditFixedList
    TabOrder = 7
    Width = 90
  end
  object UniqueBegEd: TcxDateEdit
    Left = 102
    Top = 159
    Properties.ButtonGlyph.Data = {
      3E020000424D3E0200000000000036000000280000000D0000000D0000000100
      1800000000000802000000000000000000000000000000000000C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C000C56E35D6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAA
      D6ADAAD6ADAAD6ADAA00C56E36FFFFFFC0C0C0FFFFFFD3A9A7FFFFFFC0C0C0FF
      FFFFD3A6A6FFFFFFC0C0C0FFFFFFC1838300C56E36FFFFFFFFFFFFFFFFFFD6AE
      ACFFFFFFFFFFFFFFFFFFD5ABABFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7
      D7B2B7D6AEACD6ADAA0000FF0000FF0000FFD6ADAAD6AEACD7AFAFD6AEACC387
      8700C56E36FFFFFFC0C0C0FFFFFF0000FFFFFFFFC0C0C0FFFFFF0000FFFFFFFF
      C0C0C0FFFFFFC2868600C56E36FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
      FFFF0000FFFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7D7B2B7D7B2B7D6AD
      AA0000FF0000FF0000FFD6ADAAD7AFAFD7AFAFDBB7B7C3878700C56E36FFFFFF
      C0C0C0FFFFFFD6ADAAFFFFFFC0C0C0FFFFFFD5AAA9FFFFFFFFFFFFFFFFFFC386
      8500C56E36FFFFFFFFFFFFFFFFFFD8B7BEFFFFFFFFFFFFFFFFFFD7B4BCFFFFFF
      C0C0C0FFFFFFC38C9400C36C33C36C33C46D34C46D34C36D34C46D34C46D34C4
      6D34C36D34C46D34C46D34C46D34C36C3400C56C33C56C33C56C33C56C33CD69
      00CD6900CD6900CD6900CD6900CD6900CD6600CA5F00CD640000C36C33C36C33
      C36C33C46D33C46D34C46D33C46D33C46D33C46D33C46D33C46C33C36C33C36C
      3300}
    Style.Color = clWindow
    TabOrder = 8
    Width = 90
  end
  object UniqueEndEd: TcxDateEdit
    Left = 232
    Top = 159
    Properties.ButtonGlyph.Data = {
      3E020000424D3E0200000000000036000000280000000D0000000D0000000100
      1800000000000802000000000000000000000000000000000000C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C000C56E35D6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAA
      D6ADAAD6ADAAD6ADAA00C56E36FFFFFFC0C0C0FFFFFFD3A9A7FFFFFFC0C0C0FF
      FFFFD3A6A6FFFFFFC0C0C0FFFFFFC1838300C56E36FFFFFFFFFFFFFFFFFFD6AE
      ACFFFFFFFFFFFFFFFFFFD5ABABFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7
      D7B2B7D6AEACD6ADAA0000FF0000FF0000FFD6ADAAD6AEACD7AFAFD6AEACC387
      8700C56E36FFFFFFC0C0C0FFFFFF0000FFFFFFFFC0C0C0FFFFFF0000FFFFFFFF
      C0C0C0FFFFFFC2868600C56E36FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
      FFFF0000FFFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7D7B2B7D7B2B7D6AD
      AA0000FF0000FF0000FFD6ADAAD7AFAFD7AFAFDBB7B7C3878700C56E36FFFFFF
      C0C0C0FFFFFFD6ADAAFFFFFFC0C0C0FFFFFFD5AAA9FFFFFFFFFFFFFFFFFFC386
      8500C56E36FFFFFFFFFFFFFFFFFFD8B7BEFFFFFFFFFFFFFFFFFFD7B4BCFFFFFF
      C0C0C0FFFFFFC38C9400C36C33C36C33C46D34C46D34C36D34C46D34C46D34C4
      6D34C36D34C46D34C46D34C46D34C36C3400C56C33C56C33C56C33C56C33CD69
      00CD6900CD6900CD6900CD6900CD6900CD6600CA5F00CD640000C36C33C36C33
      C36C33C46D33C46D34C46D33C46D33C46D33C46D33C46D33C46C33C36C33C36C
      3300}
    Style.Color = clWindow
    TabOrder = 9
    Width = 90
  end
end
