/*
 ����        - Count.cpp
 ������      - ��������������� (ICS)
 ��������    - ���� ���������� ������, ����������� �������� �����
 ��� �������� ��������, ���������� ����������� �������
 ���������� ����������
 ( ������������ ����������� TICSDocSpec )
 ����������� - ������� �.�.
 ����        - 11.10.2004
 */
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "Count.h"
#include "ICSDocSpec.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxButtonEdit"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"

// ***************************************************************************
// ********************** ��������/��������/����� ����� **********************
// ***************************************************************************

__fastcall TfmCount::TfmCount(TComponent * Owner, TTagNode * ACountNode, TStrings * AISList)
    : TForm(Owner)
 {
  if (!Owner->InheritsFrom(__classid(TICSDocSpec)))
   throw DKClasses::EInvalidArgument(
      __FUNC__,
      "Owner",
      FMT(icsDocErrorCreateOwnerNotValid)
       );
  FCountNode = new TTagNode;
  if (ACountNode)
   FCountNode->Assign(ACountNode, true);
  FISList = AISList;

  CountLab->Caption       = FMT(icsDocCountCountCaption);
  FilterRulesLab->Caption = FMT(icsDocCountFilterRulesCaption);
  bOk->Caption            = FMT(icsDocCountOkBtnCaption);
  bCancel->Caption        = FMT(icsDocCountCancelBtnCaption);
  bHelp->Caption          = FMT(icsDocCountHelpBtnCaption);
  bClear->Caption         = FMT(icsDocCountClearBtnCaption);

 }
//---------------------------------------------------------------------------
__fastcall TfmCount::~TfmCount()
 {
  delete FCountNode;
 }
//---------------------------------------------------------------------------
void __fastcall TfmCount::FormShow(TObject * Sender)
 {
  cbISs->Properties->OnChange = NULL;

#ifdef HIDE_HELP_BUTTONS
  bHelp->Visible = false;
#endif

  cbISs->Style->Color = GetReqEdBkColor();

  cbISs->Properties->Items->Assign(FISList);
  if (cbISs->Properties->Items->Count)
   {
    cbISs->ItemIndex = 0;
    bOk->Enabled     = true;
   }
  else
   {
    cbISs->ItemIndex = -1;
    bOk->Enabled     = false;
   }

  if (FCountNode->CmpName("count"))
   {
    for (int i = 0; i < cbISs->Properties->Items->Count; i++)
     {
      if (GetISExtRef(i) == FCountNode->AV["ref"])
       {
        cbISs->ItemIndex = i;
        break;
       }
     }
    UpdateCondRulesEditorText();
   }
  else
   {
    FCountNode->Name      = "count";
    FCountNode->AV["ref"] = GetISExtRef(cbISs->ItemIndex);
    edCondRules->Text     = csEmptyEllipsiseEditor;
   }
  cbISs->Properties->OnChange = cbISsPropertiesChange;
 }

// ***************************************************************************
// ********************** ������ ����� ***************************************
// ***************************************************************************

UnicodeString __fastcall TfmCount::GetISExtRef(int nItemIndex)
 {
  TTagNode * nd = reinterpret_cast<TTagNode *>(cbISs->Properties->Items->Objects[nItemIndex]);
  return nd->GetRoot()->GetChildByName("passport")->AV["GUI"] + "." + nd->AV["uid"];
 }
//---------------------------------------------------------------------------
TColor __fastcall TfmCount::GetReqEdBkColor() const
 {
  return dynamic_cast<TICSDocSpec *>(Owner)->ReqEdBkColor;
 }
//---------------------------------------------------------------------------
void __fastcall TfmCount::UpdateCondRulesEditorText()
 {
  edCondRules->Text = (FCountNode->GetChildByName("condrules"))
      ? csNotEmptyEllipsiseEditor
      : csEmptyEllipsiseEditor;
 }

// ***************************************************************************
// ********************** ����������� �������*********************************
// ***************************************************************************
void __fastcall TfmCount::bOkClick(TObject * Sender)
 {
  FCountNode->AV["ref"] = GetISExtRef(cbISs->ItemIndex);
  ModalResult           = mrOk;
 }
//---------------------------------------------------------------------------
void __fastcall TfmCount::bClearClick(TObject * Sender)
 {
  ModalResult = mrAbort;
 }
//---------------------------------------------------------------------------
void __fastcall TfmCount::bCancelClick(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
//---------------------------------------------------------------------------
void __fastcall TfmCount::bHelpClick(TObject * Sender)
 {
  Application->HelpContext(HelpContext);
 }
//---------------------------------------------------------------------------
void __fastcall TfmCount::cbISsPropertiesChange(TObject * Sender)
 {
  if (FCountNode->GetChildByName("condrules"))
   {
    delete FCountNode->GetChildByName("condrules");
    edCondRules->Text = csEmptyEllipsiseEditor;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TfmCount::edCondRulesPropertiesButtonClick(TObject * Sender, int AButtonIndex)
 {
  /*get filter dlg*/
  if (FOnEditCondRules)
   if (
       FOnEditCondRules(
       this,
       FMT1(icsDocCountEvtCaption, cbISs->Text),
       FCountNode,
       FCountNode->GetChildByName("condrules"),
       reinterpret_cast<TTagNode *>(cbISs->Properties->Items->Objects[cbISs->ItemIndex])->AV["uid"]
       )
       )
    UpdateCondRulesEditorText();
 }
//---------------------------------------------------------------------------
