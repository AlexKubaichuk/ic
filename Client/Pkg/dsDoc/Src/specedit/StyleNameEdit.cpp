/*
  ����        - StyleNameEdit.cpp
  ������      - ��������������� (ICS)
  ��������    - ���� ���������� ������, ����������� �������� �����
                ��� ����� ������������ �����
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 22.09.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "StyleNameEdit.h"
//#include "DKMBUtils.h"
#include "msgdef.h"
#include "ICSDocSpec.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"

//***************************************************************************
//********************** ���������� ������� *********************************
//***************************************************************************

//***************************************************************************
//********************** ��������/��������/����� ����� **********************
//***************************************************************************

__fastcall TfmStyleEdit::TfmStyleEdit(TComponent* Owner, TFormUpdateMode AFUM)
        : TForm(Owner)
{
  if (!Owner->InheritsFrom(__classid(TICSDocSpec)))
    throw DKClasses::EInvalidArgument(
      __FUNC__,
      "Owner",
      FMT(icsDocErrorCreateOwnerNotValid)
    );
  bOk->Caption = FMT(icsDocStyleEditbOkBtnCaption);
  bCancel->Caption = FMT(icsDocStyleEditbCancelBtnCaption);
  bHelp->Caption = FMT(icsDocStyleEditbHelpBtnCaption);
  StyleNameLab->Caption = FMT(icsDocStyleEditStyleNameLabLabCaption);

  FStyleName = "";
  FFUM = AFUM;
  FOnValidate = NULL;
}

//---------------------------------------------------------------------------

void __fastcall TfmStyleEdit::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif
  switch( FFUM )
  {
    case fumADD : Caption = FMT(icsDocStyleEditfumADDCaption); break;
    case fumEDIT: Caption = FMT(icsDocStyleEditfumEDITCaption); break;
  }
  edStyleName->Style->Color = GetReqEdBkColor();
}

//***************************************************************************
//********************** ������ ����� ***************************************
//***************************************************************************

TColor __fastcall TfmStyleEdit::GetReqEdBkColor() const
{
  return dynamic_cast<TICSDocSpec*>(Owner)->ReqEdBkColor;
}

//---------------------------------------------------------------------------

void __fastcall TfmStyleEdit::SetStyleName( UnicodeString  AValue )
{
  edStyleName->Text = FStyleName = AValue;
}

//---------------------------------------------------------------------------

bool __fastcall TfmStyleEdit::AreInputsCorrect()
{
  if ( edStyleName->Text == "" )
  {
    edStyleName->SetFocus();
    _MSG_ERR(FMT1(icsDocErrorReqFieldCaption,FMT(icsDocStyleEditStyleNameCaption)), FMT(icsDocErrorInputErrorCaption));
    return false;
  }

  if ( FOnValidate )
    return FOnValidate( this );

  return true;
}

//***************************************************************************
//********************** ����������� �������*********************************
//***************************************************************************

void __fastcall TfmStyleEdit::bOkClick(TObject *Sender)
{
  if ( !CheckDKFilterControls( Sender ) )	//��� ����������� �� ������ DKComp,
    return;					//������������ TDKControlFilter

  FStyleName = edStyleName->Text;

  if ( !AreInputsCorrect() )
    return;

  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TfmStyleEdit::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TfmStyleEdit::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}

//---------------------------------------------------------------------------


