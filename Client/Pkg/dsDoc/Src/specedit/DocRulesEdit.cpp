/*
 ����        - DocRulesEdit.cpp
 ������      - ��������������� (ICS)
 ��������    - �������� ������ ������������ ��������� �� ���������.
 ���� ����������
 (������������ ����������� TICSDocSpec)
 ����������� - ������� �.�.
 ����        - 15.11.2004
 */
// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "icsDocSpecConstDef.h"
#include "DocRulesEdit.h"
// #include "DKMBUtils.h"
#include "msgdef.h"
#include "ICSTL.h"
#include "ICSDocSpec.h"
#include "PeriodEdit.h"
#include "DurationEdit.h"
#include <algorithm>
#include <functional>
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "DKListBox"
#pragma link "dxBar"
#pragma resource "*.dfm"
// using namespace std;
// ###########################################################################
// ##                                                                       ##
// ##                              TfmDocRulesEdit                          ##
// ##                                                                       ##
// ###########################################################################
// ***************************************************************************
// ********************** ��������/��������/����� ����� **********************
// ***************************************************************************
__fastcall TfmDocRulesEdit::TfmDocRulesEdit(TComponent * Owner, TTagNode * ADocRules, TTagNode * ASpec)
  : TForm(Owner), csCreateStepViewName("����"), csUndefPeriodVal("")
 {
  if (!Owner->InheritsFrom(__classid(TICSDocSpec)))
   throw DKClasses::EInvalidArgument(__FUNC__, "Owner", FMT(icsDocErrorCreateOwnerNotValid));
  if (!ASpec)
   throw DKClasses::ENullArgument(__FUNC__, "ASpec");
  FSpec = new TTagNode;
  FSpec->Assign(ASpec, true);
  if (ADocRules)
   {
    FDocRules = new TTagNode;
    FDocRules->Assign(ADocRules, true);
   }
  else
   FDocRules = NULL;
  FStepPropertiesEnabled     = true;
  FSettingPropertiesCtrls    = false;
  FIntegrSpec                = NULL;
  FOnGetDocElementsList      = NULL;
  FOnGetThisDocElementStruct = NULL;
  FOnGetRefDocElementStruct  = NULL;
  FOnGetSpecsList            = NULL;
  FOnGetOrgsList             = NULL;
  FOnGetSpecNode             = NULL;
  FOnGetSpecMainName         = NULL;
  FOnGetDocElementViewText   = NULL;
  FOnGetDocElementStruct     = NULL;
 }
// ---------------------------------------------------------------------------
__fastcall TfmDocRulesEdit::~TfmDocRulesEdit()
 {
  __DELETE_OBJ(FDocRules) __DELETE_OBJ(FSpec)
  if (FIntegrSpec)
   if (FOnGetSpecNode)
    FOnGetSpecNode(this, "", FIntegrSpec);
   else
    __DELETE_OBJ(FIntegrSpec)
  }
 // ---------------------------------------------------------------------------
 void __fastcall TfmDocRulesEdit::FormShow(TObject * Sender)
  {
#ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
#endif
  Caption                     = FMT(icsDocRulesEditCaption);
  pStepsHead->Caption         = FMT(icsDocRulesEditCreateStep);
  pPropHead->Caption          = FMT(icsDocRulesEditStepParam);
  pObjHead->Caption           = FMT(icsDocRulesEditStepObjects);
  gbDoc->Caption              = FMT(icsDocRulesEditIntDocCaption);
  DefLab->Caption             = FMT(icsDocRulesEditIntDocDef);
  PerTypeLab->Caption         = FMT(icsDocRulesEditIntDocPerType);
  PeriodValueTitleLb->Caption = FMT(icsDocRulesEditIntDocPeriod);
  PeriodisityLab->Caption     = FMT(icsDocRulesEditIntDocPeriodisity);
  gbOrg->Caption              = FMT(icsDocRulesEditOrg);
  OrgTypeLab->Caption         = FMT(icsDocRulesEditOrgType);
  lbOrgCodeTitle->Caption     = FMT(icsDocRulesEditOrgName);
  // actAddStep->Caption = FMT(icsDocRulesEditAddStepCaption);
  actAddStep->Hint = FMT(icsDocRulesEditAddStepHint);
  // actMoveStepUp->Caption = FMT(icsDocRulesEditMoveStepUpCaption);
  actMoveStepUp->Hint = FMT(icsDocRulesEditMoveStepUpHint);
  // actMoveStepDown->Caption = FMT(icsDocRulesEditMoveStepDownCaption);
  actMoveStepDown->Hint = FMT(icsDocRulesEditMoveStepDownHint);
  // actDelete->Caption = FMT(icsDocRulesEditDeleteCaption);
  actDelete->Hint = FMT(icsDocRulesEditDeleteHint);
  // actAddObjRules->Caption = FMT(icsDocRulesEditAddObjRulesCaption);
  actAddObjRules->Hint = FMT(icsDocRulesEditAddObjRulesHint);
  // actSetAssoc->Caption = FMT(icsDocRulesEditSetAssocCaption);
  actSetAssoc->Hint = FMT(icsDocRulesEditSetAssocHint);
  // actEditProp->Caption = FMT(icsDocRulesEditEditPropCaption);
  actEditProp->Hint          = FMT(icsDocRulesEditEditPropHint);
  bOk->Caption               = FMT(icsDocRulesEditOkBtnCaption);
  bCancel->Caption           = FMT(icsDocRulesEditCancelBtnCaption);
  bHelp->Caption             = FMT(icsDocRulesEditHelpBtnCaption);
  actAddStep->Caption        = actAddStep->Caption;
  actAddStep->Hint           = actAddStep->Hint;
  actMoveStepUp->Caption     = actMoveStepUp->Caption;
  actMoveStepUp->Hint        = actMoveStepUp->Hint;
  actMoveStepDown->Caption   = actMoveStepDown->Caption;
  actMoveStepDown->Hint      = actMoveStepDown->Hint;
  actDelete->Caption         = actDelete->Caption;
  actDelete->Hint            = actDelete->Hint;
  actAddObjRules->Caption    = actAddObjRules->Caption;
  actAddObjRules->Hint       = actAddObjRules->Hint;
  actSetAssoc->Caption       = actSetAssoc->Caption;
  actSetAssoc->Hint          = actSetAssoc->Hint;
  cbSpec->Style->Color       = GetReqEdBkColor();
  cbPeriodType->Style->Color = GetReqEdBkColor();
  // PeriodValueEd->Color                        = GetReqEdBkColor();
  PeriodicityCBx->Style->Color = GetReqEdBkColor();
  cbOrgType->Style->Color      = GetReqEdBkColor();
  cbOrgCode->Style->Color      = GetReqEdBkColor();
  /* TODO -okdv -cInsListRules :
   ���� ���������� �������������� ������� ��� insrules ���
   listrules, �� ���� ������� �������� actEditProp */
  actEditProp->Visible = false;
  // ������������� cbSpec
  if (FOnGetSpecsList)
   {
    FSpecs.clear();
    cbSpec->Clear();
    TUnicodeStringMap SpecsList;
    FOnGetSpecsList(this, SpecsList);
    TUnicodeStringMap::const_iterator i;
    int j;
    for (i = SpecsList.begin(), j = 1; i != SpecsList.end(); i++ , j++)
     {
      FSpecs[j] = (*i).first;
      cbSpec->Properties->Items->AddObject((* i).second, reinterpret_cast<TObject *>(j));
     }
    cbSpec->ItemIndex = -1;
    cbSpec->Text = "";
   }
  // ������������� cbOrgCode
  if (FOnGetOrgsList)
   {
    FOrgs.clear();
    cbOrgCode->Clear();
    TUnicodeStringMap OrgsList;
    FOnGetOrgsList(this, OrgsList);
    TUnicodeStringMap::const_iterator i;
    int j;
    for (i = OrgsList.begin(), j = 1; i != OrgsList.end(); i++ , j++)
     {
      FOrgs[j] = (*i).first;
      cbOrgCode->Properties->Items->AddObject((* i).second, reinterpret_cast<TObject *>(j));
     }
    cbOrgCode->ItemIndex = -1;
    cbOrgCode->Text = "";
   }
  // �������� ������ ������������ � lbxSteps
  LoadCreateSteps();
 }
// ***************************************************************************
// ********************** ������ ����� ***************************************
// ***************************************************************************
TColor __fastcall TfmDocRulesEdit::GetReqEdBkColor() const
 {
  return dynamic_cast<TICSDocSpec *>(Owner)->ReqEdBkColor;
 }
// ---------------------------------------------------------------------------
// -------------- ������ ��� ������ � �������� pProps � ppObjs ---------------
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::HidePropsPanel()
 {
  if (pProps->Visible)
   {
    pProps->Visible    = false;
    bPropHide->Caption = "+";
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::ShowPropsPanel()
 {
  if (!pProps->Visible)
   {
    pProps->Visible    = true;
    bPropHide->Caption = "�";
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::TogglePropsPanel()
 {
  pProps->Visible ^= true;
  bPropHide->Caption = (pProps->Visible) ? "�" : "+";
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::HideObjsPanel()
 {
  if (pObjs->Visible)
   {
    pObjs->Visible    = false;
    bObjHide->Caption = "+";
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::ShowObjsPanel()
 {
  if (!pObjs->Visible)
   {
    pObjs->Visible    = true;
    bObjHide->Caption = "�";
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::ToggleObjsPanel()
 {
  pObjs->Visible ^= true;
  bObjHide->Caption = (pObjs->Visible) ? "�" : "+";
 }
// ---------------------------------------------------------------------------
// -------------- ������ ��� ������ � control'��� - ����������� --------------
// -------------- ���������� ����� ������������ ------------------------------
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::InitStepPropertiesControls()
 {
  cbSpec->ItemIndex         = -1;
  cbSpec->Text              = "";
  cbPeriodType->ItemIndex   = 0;
  PeriodValueEd->Text       = csUndefPeriodVal;
  PeriodicityCBx->ItemIndex = 0;
  cbOrgType->ItemIndex      = 0;
  cbOrgCode->ItemIndex      = -1;
  cbOrgCode->Text           = "";
  tvObj->Items->Clear();
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::SetStepPropertiesControlsEnabled(bool bDoInit, bool bEnabled)
 {
  if (bDoInit)
   InitStepPropertiesControls();
  // TColor CtrlColor = ( bEnabled ) ? GetReqEdBkColor() : clBtnFace;
  cbSpec->Enabled        = bEnabled;
  cbPeriodType->Enabled  = bEnabled;
  PeriodValueEd->Enabled = bEnabled;
  // PeriodValueEd->Style->Color    = (bEnabled) ? clWindow : clBtnFace; //CtrlColor;
  PeriodicityCBx->Enabled = bEnabled;
  cbOrgType->Enabled      = bEnabled;
  cbOrgCode->Enabled      = bEnabled;
  tvObj->Enabled          = bEnabled;
  tvObj->Color            = (bEnabled) ? clWindow : clBtnFace;
  FStepPropertiesEnabled  = bEnabled;
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::EnableStepPropertiesControls(bool bDoInit)
 {
  SetStepPropertiesControlsEnabled(bDoInit, true);
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::DisableStepPropertiesControls(bool bDoInit)
 {
  SetStepPropertiesControlsEnabled(bDoInit, false);
 }
// ---------------------------------------------------------------------------
// ���������� ����������� ��������
void __fastcall TfmDocRulesEdit::UpdateActionsEnabled()
 {
  bool bMoveStepUpEn, bMoveStepDownEn, bDeleteEn, bAddObjRulesEn, bSetAssocEn, bEditPropEn;
  bMoveStepUpEn = bMoveStepDownEn = bDeleteEn = bAddObjRulesEn = bSetAssocEn = bEditPropEn = false;
  if (lbxSteps->ItemIndex != -1)
   {
    bMoveStepUpEn   = (lbxSteps->ItemIndex > 0);
    bMoveStepDownEn = (lbxSteps->ItemIndex < lbxSteps->Items->Count - 1);
    bAddObjRulesEn  = true;
   }
  if (ActiveControl == lbxSteps)
   bDeleteEn = (lbxSteps->ItemIndex != -1);
  else if (ActiveControl == tvObj)
   {
    bDeleteEn = ((tvObj->Selected) && (tvObj->Selected->Level == 0) && (tvObj->Selected->getPrevSibling() ||
      tvObj->Selected->getNextSibling()));
    bSetAssocEn = bEditPropEn = tvObj->Selected;
   }
  actMoveStepUp->Enabled = bMoveStepUpEn;
  actMoveStepDown->Enabled = bMoveStepDownEn;
  actDelete->Enabled       = bDeleteEn;
  actAddObjRules->Enabled  = bAddObjRulesEn;
  actSetAssoc->Enabled     = bSetAssocEn;
  actEditProp->Enabled     = bEditPropEn;
 }
// ---------------------------------------------------------------------------
// �������� ������������� ������������� ���������� ( ���� FIntegrSpec )
void __fastcall TfmDocRulesEdit::LoadIntegrSpec()
 {
  UnicodeString IntegrSpecGUI = FSpecs[reinterpret_cast<int>(cbSpec->Properties->Items->Objects[cbSpec->ItemIndex])];
  if (!FIntegrSpec || (FIntegrSpec && FIntegrSpec->GetChildByName("passport") && FIntegrSpec->GetChildByName("passport")
    ->GetAttrByName("GUI") && FIntegrSpec->GetChildByName("passport")->AV["GUI"] != IntegrSpecGUI))
   if (FOnGetSpecNode)
    if (!FOnGetSpecNode(this, IntegrSpecGUI, FIntegrSpec))
     _MSG_ERR(FMT(icsDocRulesEditAccessDeny), TICSDocSpec::ICSName);
 }
// ---------------------------------------------------------------------------
// ���������� ��������� tvObj - ����������� ������ ����������
void __fastcall TfmDocRulesEdit::UpdateDocElementIntegrInfo(TTreeNode * ANode)
 {
  if (!FOnGetDocElementViewText)
   return;
  TTagNode * TagNode = reinterpret_cast<TTagNode *>(ANode->Data);
  /* TODO -okdv -cInsListRules :
   ���� �������� ����������� �������� � ������������
   ��� insrules � listrules */
  if (TagNode)
    // ����������� �������� � ������������ ������
   if (TagNode->CmpName("tabrules"))
    {
     FObjRules = TagNode;
     UnicodeString ViewText;
     FOnGetDocElementViewText(this, FSpec->GetTagByUID(TagNode->AV["thisref"]), ViewText);
     ANode->Text = ViewText + "  ->  ";
     FOnGetDocElementViewText(this, FIntegrSpec->GetTagByUID(_UID(TagNode->AV["ref"])), ViewText);
     ANode->Text = ANode->Text + ViewText;
    }
   else if (TagNode->CmpName("insrules"))
    {
     throw DKClasses::EInvalidArgument(__FUNC__, "ANode", FMT(icsDocRulesEditVersionLimit));
    }
   else if (TagNode->CmpName("listrules"))
    {
     throw DKClasses::EInvalidArgument(__FUNC__, "ANode", FMT(icsDocRulesEditVersionLimit));
    }
   // ����������� �������� � ������������ �����/�������� ������
   else if (TICSDocSpec::IsTableColSingle(TagNode) || TICSDocSpec::IsTableRowSingle(TagNode))
    {
     UnicodeString ViewText;
     FOnGetDocElementViewText(this, TagNode, ViewText);
     ANode->Text = ViewText;
     TTagNode * ndRefPair = FObjRules->GetChildByAV("refpair", "thisref", TagNode->AV["uid"], true);
     if (ndRefPair)
      {
       FOnGetDocElementViewText(this, FIntegrSpec->GetTagByUID(_UID(ndRefPair->AV["ref"])), ViewText);
       ANode->Text = ANode->Text + "  ->  " + ViewText;
      }
    }
  for (int i = 0; i < ANode->Count; i++)
   UpdateDocElementIntegrInfo(ANode->Item[i]);
 }
// ---------------------------------------------------------------------------
// �������� ������ ������������ � lbxSteps
void __fastcall TfmDocRulesEdit::LoadCreateSteps()
 {
  if (FDocRules)
   {
    lbxSteps->DisableOnSelect();
    TTagNode * ndCreateStep = FDocRules->GetFirstChild();
    int i = 1;
    while (ndCreateStep)
     {
      lbxSteps->AddItem(csCreateStepViewName + " " + IntToStr(i++), ndCreateStep);
      ndCreateStep = ndCreateStep->GetNext();
     }
    lbxSteps->EnableOnSelect();
    lbxSteps->First();
   }
  else
   {
    DisableStepPropertiesControls();
    UpdateActionsEnabled();
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TfmDocRulesEdit::GetPeriodValViewText(TTagNode * ADocRulesStep)
 {
  UnicodeString sPeriodVal = csUndefPeriodVal;
  UnicodeString sPeriodType = ADocRulesStep->AV["periodtype"];
  if (sPeriodType == "defined_value")
   {
    if (ADocRulesStep->AV["periodvalue"].Length())
     {
      try
       {
        TDate dtBeg, dtEnd;
        XMLStrToPeriod(ADocRulesStep->AV["periodvalue"], & dtBeg, & dtEnd);
        sPeriodVal = DateToStr(dtBeg) + " - " + DateToStr(dtEnd);
       }
      catch (EConvertError &)
       {
        sPeriodVal = csUndefPeriodVal;
       }
     }
   }
  else if (sPeriodType == "abstruct_value")
   {
    sPeriodVal = ADocRulesStep->AV["periodvalue"];
   }
  return sPeriodVal;
 }
// ---------------------------------------------------------------------------
/*
 * ����� �������� � ��������������� ������ cbSpec ��� cbOrgCode, � �����������
 * �� �������� �������� "docref" ��� "org_type" xml-���� "createstep" �������������
 * ��������������
 *
 *
 * ���������:
 *   [in]  AttrName  - ������������ �������� ( "docref" ��� "org_type" )
 *   [in]  AMap      - ������� �������������� ��� �����������
 *   [out] AComboBox - ��������������� ������ cbSpec ��� cbOrgCode
 *
 */
void __fastcall TfmDocRulesEdit::SetMapComboText(UnicodeString AttrName, const TClassMap & AMap,
  TcxComboBox * AComboBox)
 {
  TTagNode * ndStep = dynamic_cast<TTagNode *>(LBX_CUR_OBJECT(lbxSteps));
  bool bClearCB = true;
  if (ndStep->AV[AttrName] != "")
   {
    TClassMap::const_iterator i;
    for (i = AMap.begin(); i != AMap.end(); i++)
     {
      if (ndStep->CmpAV(AttrName, i->second))
       break;
     }
    if (i != AMap.end())
     {
      AComboBox->ItemIndex = AComboBox->Properties->Items->IndexOfObject(reinterpret_cast<TObject *>(i->first));
      bClearCB             = false;
     }
   }
  if (bClearCB)
   {
    AComboBox->ItemIndex = -1;
    AComboBox->Text      = "";
   }
 }
// ---------------------------------------------------------------------------
/*
 * ���������� ������ ������������ �������� �������������
 *
 *
 * ���������:
 *   [in]  AObj       - ������� �������������
 *   [in]  AObjRules  - ������� ������������ �������� �������������
 *   [out] ATreeNodes - ���� TreeView, � ������� ���������� ��������� ����������� ������ ������������
 *
 */
void __fastcall TfmDocRulesEdit::AppendObjRules(TTagNode * AObj, TTagNode * AObjRules, TTreeNodes * ATreeNodes)
 {
  if (FOnGetDocElementStruct)
   {
    TTagNode * ndObjRules = dynamic_cast<TTagNode *>(LBX_CUR_OBJECT(lbxSteps))->AddChild();
    ndObjRules->Assign(AObjRules, true);
    AppendObjRulesView(AObj, ndObjRules, ATreeNodes);
   }
 }
// ---------------------------------------------------------------------------
/*
 * ���������� ����������� ������ ������������ �������� �������������
 *
 *
 * ���������:
 *   [in]  AObj       - ������� �������������
 *   [in]  AObjRules  - ������� ������������ �������� �������������
 *   [out] ATreeNodes - ���� TreeView, � ������� ���������� ��������� ����������� ������ ������������
 *
 */
void __fastcall TfmDocRulesEdit::AppendObjRulesView(TTagNode * AObj, TTagNode * AObjRules, TTreeNodes * ATreeNodes)
 {
  if (FOnGetDocElementStruct)
   {
    FOnGetDocElementStruct(this, AObj, ATreeNodes);
    tvObj->Selected->Data = reinterpret_cast<void *>(AObjRules);
    UpdateDocElementIntegrInfo(dynamic_cast<TTreeView *>(ATreeNodes->Owner)->Selected);
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TfmDocRulesEdit::DocRulesEditCheckAssociationExistence(TObject * Sender, const TTagNode * ndObjRules,
  const UnicodeString ThisRef, UnicodeString & Msg)
 {
  return false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TfmDocRulesEdit::AreInputsCorrect()
 {
  if (lbxSteps->ItemIndex == -1)
   return true;
  if (!dynamic_cast<TTagNode *>(LBX_CUR_OBJECT(lbxSteps))->Count)
   {
    ActiveControl = tvObj;
    _MSG_ERR(FMT(icsDocRulesEditErrorEmptyDoc), TICSDocSpec::ICSName);
    return false;
   }
  TTreeNode * RulesNode = tvObj->Items->GetFirstNode();
  while (RulesNode)
   {
    TTagNode * ndRules = reinterpret_cast<TTagNode *>(RulesNode->Data);
    /* TODO -okdv -cInsListRules : ���� �������� �������� ������������ insrules � listrules */
    if (ndRules->CmpName("tabrules"))
     {
      UnicodeString TableElementName;
      bool bBadRules = false;
      if (!ndRules->GetChildByName("colrules")->Count)
       {
        TableElementName = FMT(icsDocRulesEditConfCol);
        bBadRules        = true;
       }
      else if (!ndRules->GetChildByName("rowrules")->Count)
       {
        TableElementName = FMT(icsDocRulesEditConfRow);
        bBadRules        = true;
       }
      if (bBadRules)
       {
        _MSG_ERR(FMT1(icsDocRulesEditErrorConf, TableElementName), TICSDocSpec::ICSName);
        ActiveControl   = tvObj;
        tvObj->Selected = RulesNode;
        return false;
       }
     }
    else if (ndRules->CmpName("insrules"))
     {
      throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
     }
    else if (ndRules->CmpName("listrules"))
     {
      throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
     }
    RulesNode = RulesNode->getNextSibling();
   }
  if (FStepPropertiesEnabled)
   {
    if (cbSpec->ItemIndex == -1)
     {
      ShowPropsPanel();
      ActiveControl = cbSpec;
      cbSpec->SelectAll();
      _MSG_ERR(FMT(icsDocRulesEditErrorIntDefDoc), TICSDocSpec::ICSName);
      return false;
     }
    if (PeriodValueEd->Enabled && !PeriodValueEd->Text.Length())
     {
      ShowPropsPanel();
      ActiveControl = PeriodValueEd;
      PeriodValueEd->SelectAll();
      _MSG_ERR(FMT(icsDocRulesEditErrorPeriodDef), TICSDocSpec::ICSName);
      return false;
     }
    if (cbOrgCode->Enabled && (cbOrgCode->ItemIndex == -1))
     {
      ShowPropsPanel();
      ActiveControl = cbOrgCode;
      cbOrgCode->SelectAll();
      _MSG_ERR(FMT(icsDocRulesEditErrorOrgDef), TICSDocSpec::ICSName);
      return false;
     }
   }
  return true;
 }
// ***************************************************************************
// ********************** ����������� �������*********************************
// ***************************************************************************
void __fastcall TfmDocRulesEdit::bOkClick(TObject * Sender)
 {
  if (!CheckDKFilterControls(Sender)) // ��� ����������� �� ������ DKComp,
     return; // ������������ TDKControlFilter
  if (!AreInputsCorrect())
   return;
  if (FDocRules && !FDocRules->Count)
   __DELETE_OBJ(FDocRules) ModalResult = mrOk;
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::bCancelClick(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::bHelpClick(TObject * Sender)
 {
  Application->HelpContext(HelpContext);
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::bPropHideClick(TObject * Sender)
 {
  TogglePropsPanel();
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::bObjHideClick(TObject * Sender)
 {
  ToggleObjsPanel();
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::actAddStepExecute(TObject * Sender)
 {
  if (!FDocRules)
   {
    FDocRules       = new TTagNode;
    FDocRules->Name = "docrules";
   }
  TTagNode * ndNewCreateStep = FDocRules->AddChild("createstep");
  ndNewCreateStep->AV["docref"]      = "";
  ndNewCreateStep->AV["docrefmain"]  = "";
  ndNewCreateStep->AV["periodtype"]  = "this";
  ndNewCreateStep->AV["perioddef"]   = "undef";
  ndNewCreateStep->AV["periodvalue"] = "";
  ndNewCreateStep->AV["periodicity"] = "equal";
  ndNewCreateStep->AV["org_type"]    = "child";
  ndNewCreateStep->AV["org_code"]    = "";
  lbxSteps->AddItem(csCreateStepViewName + " " + IntToStr((int)FDocRules->Count), ndNewCreateStep);
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::actMoveStepUpExecute(TObject * Sender)
 {
  UnicodeString Down = LBX_CUR_ITEM(lbxSteps);
  UnicodeString Up = lbxSteps->Items->Strings[lbxSteps->ItemIndex - 1];
  lbxSteps->Items->BeginUpdate();
  lbxSteps->Items->Exchange(lbxSteps->ItemIndex, lbxSteps->ItemIndex - 1);
  lbxSteps->Items->Strings[lbxSteps->ItemIndex]     = Up;
  lbxSteps->Items->Strings[lbxSteps->ItemIndex + 1] = Down;
  lbxSteps->Items->EndUpdate();
  UpdateActionsEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::actMoveStepDownExecute(TObject * Sender)
 {
  UnicodeString Up = LBX_CUR_ITEM(lbxSteps);
  UnicodeString Down = lbxSteps->Items->Strings[lbxSteps->ItemIndex + 1];
  lbxSteps->Items->BeginUpdate();
  lbxSteps->Items->Exchange(lbxSteps->ItemIndex, lbxSteps->ItemIndex + 1);
  lbxSteps->Items->Strings[lbxSteps->ItemIndex]     = Down;
  lbxSteps->Items->Strings[lbxSteps->ItemIndex - 1] = Up;
  lbxSteps->Items->EndUpdate();
  UpdateActionsEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::actDeleteExecute(TObject * Sender)
 {
  if (ActiveControl == lbxSteps)
   {
    lbxSteps->Items->BeginUpdate();
    delete dynamic_cast<TTagNode *>(LBX_CUR_OBJECT(lbxSteps));
    lbxSteps->DeleteSelected();
    for (int i = 0; i < lbxSteps->Items->Count; i++)
     lbxSteps->Items->Strings[i] = csCreateStepViewName + " " + IntToStr(i + 1);
    lbxSteps->Items->EndUpdate();
   }
  else if (ActiveControl == tvObj)
   {
    tvObj->Items->BeginUpdate();
    delete reinterpret_cast<TTagNode *>(tvObj->Selected->Data);
    tvObj->Selected->Delete();
    tvObj->Items->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::actAddObjRulesExecute(TObject * Sender)
 {
  // �������� ������������
  if (cbSpec->ItemIndex == -1)
   {
    ShowPropsPanel();
    ActiveControl = cbSpec;
    cbSpec->SelectAll();
    _MSG_ERR(FMT(icsDocRulesEditErrorIntDefDoc), TICSDocSpec::ICSName);
    return;
   }
  LoadIntegrSpec();
  // ������ ������� ������������ ������ ����������
  TfmObjRulesEdit * dlg;
  try
   {
    dlg = new TfmObjRulesEdit(this, FSpec, FIntegrSpec);
    if (dlg)
     {
      dlg->OnGetDocElementsList    = FOnGetDocElementsList;
      dlg->OnGetSpecMainName       = FOnGetSpecMainName;
      dlg->OnGetDocElementViewText = FOnGetDocElementViewText;
      if (dlg->ShowModal() == mrOk)
       AppendObjRules(dlg->ThisObj, dlg->ObjRules, tvObj->Items);
     }
   }
  __finally
   {
    if (dlg)
     delete dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::actSetAssocExecute(TObject * Sender)
 {
  LoadIntegrSpec();
  // ����� ����� ���������, ���������������� �������� ������������� ( �������, ������� ��� ������)
  TTreeNode * TreeNode = tvObj->Selected;
  TTagNode * TagNode = reinterpret_cast<TTagNode *>(TreeNode->Data);
  while (TagNode && !TagNode->CmpName("insrules,listrules,tabrules"))
   {
    TreeNode = TreeNode->Parent;
    TagNode  = reinterpret_cast<TTagNode *>(TreeNode->Data);
   }
  // ������ ������� ��������� ������������
  TfmObjRulesAssocEdit * dlg;
  try
   {
    dlg = new TfmObjRulesAssocEdit(this, TagNode, FSpec, FIntegrSpec);
    if (dlg)
     {
      dlg->OnGetDocElementViewText     = FOnGetDocElementViewText;
      dlg->OnGetThisDocElementStruct   = FOnGetThisDocElementStruct;
      dlg->OnGetRefDocElementStruct    = FOnGetRefDocElementStruct;
      dlg->OnCheckAssociationExistence = DocRulesEditCheckAssociationExistence;
      if (dlg->ShowModal() == mrOk)
       {
        TagNode->Assign(dlg->ObjRules, true);
        UpdateDocElementIntegrInfo(TreeNode);
       }
     }
   }
  __finally
   {
    if (dlg)
     delete dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::actEditPropExecute(TObject * Sender)
 {
  throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::ListsEnter(TObject * Sender)
 {
  UpdateActionsEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::tvObjContextPopup(TObject * Sender, TPoint & MousePos, bool & Handled)
 {
  // ������� ������������� ��� ������� ������ ������ ����
  if (!((MousePos.x == -1) && (MousePos.y == -1)))
   if (tvObj->Selected)
    {
     tvObj->Selected->Selected = true;
     tvObj->Selected->Focused  = true;
    }
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::lbxStepsSelect(TObject * Sender, int ListItemIndex)
 {
  UpdateActionsEnabled();
  if (ListItemIndex == -1)
   {
    DisableStepPropertiesControls();
    return;
   }
  if (!FStepPropertiesEnabled)
   EnableStepPropertiesControls();
  TTagNode * ndStep = dynamic_cast<TTagNode *>(LBX_CUR_OBJECT(lbxSteps));
  FSettingPropertiesCtrls = true;
  // docref
  SetMapComboText("docref", FSpecs, cbSpec);
  // periodtype
  UnicodeString PeriodType = ndStep->AV["periodtype"];
  if (PeriodType == "this")
   cbPeriodType->ItemIndex = 0;
  else if (PeriodType == "defined_value")
   cbPeriodType->ItemIndex = 1;
  else if (PeriodType == "abstruct_value")
   cbPeriodType->ItemIndex = 2;
  cbPeriodType->Properties->OnChange(cbPeriodType);
  // periodvalue
  PeriodValueEd->Text = GetPeriodValViewText(ndStep);
  // periodicity
  UnicodeString Periodicity = ndStep->AV["periodicity"];
  if (Periodicity == "equal")
   PeriodicityCBx->ItemIndex = 0;
  else if (Periodicity == "any")
   PeriodicityCBx->ItemIndex = 1;
  else if (Periodicity == "no")
   PeriodicityCBx->ItemIndex = 2;
  else if (Periodicity == "year")
   PeriodicityCBx->ItemIndex = 3;
  else if (Periodicity == "halfyear")
   PeriodicityCBx->ItemIndex = 4;
  else if (Periodicity == "quarter")
   PeriodicityCBx->ItemIndex = 5;
  else if (Periodicity == "month")
   PeriodicityCBx->ItemIndex = 6;
  else if (Periodicity == "unique")
   PeriodicityCBx->ItemIndex = 7;
  // org_type
  UnicodeString OrgType = ndStep->AV["org_type"];
  if (OrgType == "defined")
   cbOrgType->ItemIndex = 0;
  else if (OrgType == "child")
   cbOrgType->ItemIndex = 1;
  else if (OrgType == "this")
   cbOrgType->ItemIndex = 2;
  cbOrgType->Properties->OnChange(cbOrgType);
  // org_code
  SetMapComboText("org_code", FOrgs, cbOrgCode);
  FSettingPropertiesCtrls = false;
  // rules
  tvObj->Items->BeginUpdate();
  tvObj->Items->Clear();
  if (cbSpec->ItemIndex != -1)
   {
    LoadIntegrSpec();
    TTagNode * ndRules = ndStep->GetFirstChild();
    while (ndRules)
     {
      TTagNode * ndObj = NULL;
      /* TODO -okdv -cInsListRules : ���� �������� ����������� ndObj ��� insrules � listrules */
      if (ndRules->CmpName("tabrules"))
       {
        ndObj = FSpec->GetTagByUID(ndRules->AV["thisref"]);
       }
      else if (ndRules->CmpName("insrules"))
       {
        throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
       }
      else if (ndRules->CmpName("listrules"))
       {
        throw DKClasses::EMethodError(__FUNC__, FMT(icsDocRulesEditVersionLimit));
       }
      AppendObjRulesView(ndObj, ndRules, tvObj->Items);
      ndRules = ndRules->GetNext();
     }
   }
  tvObj->Items->EndUpdate();
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::tvObjChange(TObject * Sender, TTreeNode * Node)
 {
  UpdateActionsEnabled();
 }
// ---------------------------------------------------------------------------
// Insert     - �������� ����
// Ctrl+����� - ����������� �����
// Ctrl+����  - ����������� ����
// Del        - �������
void __fastcall TfmDocRulesEdit::lbxStepsKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_INSERT)
   actAddStep->Execute();
  else if ((Key == VK_UP) && Shift.Contains(ssCtrl))
   actMoveStepUp->Execute();
  else if ((Key == VK_DOWN) && Shift.Contains(ssCtrl))
   actMoveStepDown->Execute();
  else if (Key == VK_DELETE)
   actDelete->Execute();
 }
// ---------------------------------------------------------------------------
// Ctrl+Insert - �������� ������� ������������ ���-����
// Shift+Enter - ���������� ������������
void __fastcall TfmDocRulesEdit::tvObjKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if ((Key == VK_INSERT) && Shift.Contains(ssCtrl))
   actAddObjRules->Execute();
  else if ((Key == VK_RETURN) && Shift.Contains(ssShift))
   actSetAssoc->Execute();
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::cbSpecPropertiesChange(TObject * Sender)
 {
  if (!FSettingPropertiesCtrls && (lbxSteps->ItemIndex != -1))
   {
    TTagNode * ndStep = dynamic_cast<TTagNode *>(LBX_CUR_OBJECT(lbxSteps));
    if (tvObj->Items->Count)
     {
      if (_MSG_QUE(FMT(icsDocRulesEditDefChangeConf), TICSDocSpec::ICSName) == mrNo)
       {
        SetMapComboText("docref", FSpecs, cbSpec);
        return;
       }
      ndStep->DeleteChild();
      tvObj->Items->Clear();
     }
    if (cbSpec->ItemIndex != -1)
     ndStep->AV["docref"] = FSpecs[reinterpret_cast<int>(cbSpec->Properties->Items->Objects[cbSpec->ItemIndex])];
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::cbPeriodTypePropertiesChange(TObject * Sender)
 {
  if (!FSettingPropertiesCtrls && (lbxSteps->ItemIndex != -1))
   {
    TTagNode * ndStep = dynamic_cast<TTagNode *>(LBX_CUR_OBJECT(lbxSteps));
    switch (cbPeriodType->ItemIndex)
     {
     case 0:
      ndStep->AV["periodtype"] = "this";
      ndStep->AV["perioddef"]   = "undef";
      ndStep->AV["periodvalue"] = "";
      break;
     case 1:
      ndStep->AV["periodtype"] = "defined_value";
      ndStep->AV["perioddef"]   = "undef";
      ndStep->AV["periodvalue"] = "";
      break;
     case 2:
      ndStep->AV["periodtype"] = "abstruct_value";
      ndStep->AV["perioddef"]   = "undef";
      ndStep->AV["periodvalue"] = "0/0/0";
      break;
     }
   }
  if (cbPeriodType->ItemIndex != 0)
   {
    PeriodValueEd->Enabled = true;
    // PeriodValueEd->Color   = GetReqEdBkColor();
    // PeriodValueEd->Style->Color   = clWindow;
    // PeriodValueTitleLb->Font->Color = clWindowText;
   }
  else
   {
    PeriodValueEd->Enabled = false;
    // PeriodValueEd->Style->Color   = clBtnFace;
    // PeriodValueTitleLb->Font->Color = clGrayText;
   }
  if (lbxSteps->ItemIndex != -1)
   {
    TTagNode * ndStep = dynamic_cast<TTagNode *>(LBX_CUR_OBJECT(lbxSteps));
    PeriodValueEd->Text = GetPeriodValViewText(ndStep);
   }
  else
   PeriodValueEd->Text = csUndefPeriodVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::PeriodValueEdPropertiesButtonClick(TObject * Sender, int AButtonIndex)
 {
  if (lbxSteps->ItemIndex != -1)
   {
    TTagNode * ndStep = dynamic_cast<TTagNode *>(LBX_CUR_OBJECT(lbxSteps));
    if (cbPeriodType->ItemIndex == 1)
     {
      TPeriodEditFM * dlg = new TPeriodEditFM(this);
      try
       {
        dlg->PeriodDefXMLStr   = ndStep->AV["perioddef"];
        dlg->PeriodValueXMLStr = ndStep->AV["periodvalue"];
        if (dlg->ShowModal() == mrOk)
         {
          ndStep->AV["perioddef"]   = dlg->PeriodDefXMLStr;
          ndStep->AV["periodvalue"] = dlg->PeriodValueXMLStr;
         }
       }
      __finally
       {
        delete dlg;
       }
     }
    else if (cbPeriodType->ItemIndex == 2)
     {
      TDurationEditFM * dlg = new TDurationEditFM(this);
      try
       {
        dlg->DurationXMLStr = ndStep->AV["periodvalue"];
        if (dlg->ShowModal() == mrOk)
         ndStep->AV["periodvalue"] = dlg->DurationXMLStr;
       }
      __finally
       {
        delete dlg;
       }
     }
    PeriodValueEd->Text = GetPeriodValViewText(ndStep);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::PeriodicityCBxPropertiesChange(TObject * Sender)
 {
  if (!FSettingPropertiesCtrls && (lbxSteps->ItemIndex != -1))
   {
    TTagNode * ndStep = dynamic_cast<TTagNode *>(LBX_CUR_OBJECT(lbxSteps));
    switch (PeriodicityCBx->ItemIndex)
     {
     case 0:
      ndStep->AV["periodicity"] = "equal";
      break;
     case 1:
      ndStep->AV["periodicity"] = "any";
      break;
     case 2:
      ndStep->AV["periodicity"] = "no";
      break;
     case 3:
      ndStep->AV["periodicity"] = "year";
      break;
     case 4:
      ndStep->AV["periodicity"] = "halfyear";
      break;
     case 5:
      ndStep->AV["periodicity"] = "quarter";
      break;
     case 6:
      ndStep->AV["periodicity"] = "month";
      break;
     case 7:
      ndStep->AV["periodicity"] = "unique";
      break;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::cbOrgTypePropertiesChange(TObject * Sender)
 {
  if (!FSettingPropertiesCtrls && (lbxSteps->ItemIndex != -1))
   {
    TTagNode * ndStep = dynamic_cast<TTagNode *>(LBX_CUR_OBJECT(lbxSteps));
    switch (cbOrgType->ItemIndex)
     {
     case 0:
      ndStep->AV["org_type"] = "defined";
      break;
     case 1:
      ndStep->AV["org_type"] = "child";
      break;
     case 2:
      ndStep->AV["org_type"] = "this";
      break;
     }
    ndStep->AV["org_code"] = "";
   }
  if (cbOrgType->ItemIndex == 0)
   {
    cbOrgCode->Enabled          = true;
    lbOrgCodeTitle->Font->Color = clWindowText;
   }
  else
   {
    cbOrgCode->Enabled          = false;
    lbOrgCodeTitle->Font->Color = clGrayText;
   }
  cbOrgCode->ItemIndex = -1;
  cbOrgCode->Text = "";
 }
// ---------------------------------------------------------------------------
void __fastcall TfmDocRulesEdit::cbOrgCodePropertiesChange(TObject * Sender)
 {
  if (!FSettingPropertiesCtrls && (cbOrgCode->ItemIndex != -1) && (lbxSteps->ItemIndex != -1))
   {
    TTagNode * ndStep = dynamic_cast<TTagNode *>(LBX_CUR_OBJECT(lbxSteps));
    ndStep->AV["org_code"] = FOrgs[reinterpret_cast<int>(cbOrgCode->Properties->Items->Objects[cbOrgCode->ItemIndex])];
   }
 }
// ---------------------------------------------------------------------------
