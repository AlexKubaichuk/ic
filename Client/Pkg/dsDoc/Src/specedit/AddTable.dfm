object fmAddTable: TfmAddTable
  Left = 459
  Top = 288
  BorderStyle = bsDialog
  BorderWidth = 8
  Caption = #1053#1086#1074#1072#1103' '#1090#1072#1073#1083#1080#1094#1072
  ClientHeight = 169
  ClientWidth = 281
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gbClient: TGroupBox
    Left = 0
    Top = 0
    Width = 281
    Height = 136
    Align = alClient
    TabOrder = 0
    object ListSrcLab: TLabel
      Left = 8
      Top = 12
      Width = 111
      Height = 13
      Caption = #1044#1072#1085#1085#1099#1077' '#1074#1099#1073#1080#1088#1072#1090#1100' '#1080#1079':'
    end
    object RowCountLab: TLabel
      Left = 8
      Top = 108
      Width = 67
      Height = 13
      Caption = #1063#1080#1089#1083#1086' '#1089#1090#1088#1086#1082':'
    end
    object Bevel1: TBevel
      Left = 8
      Top = 62
      Width = 265
      Height = 9
      Shape = bsTopLine
    end
    object ColCountLab: TLabel
      Left = 8
      Top = 76
      Width = 85
      Height = 13
      Caption = #1063#1080#1089#1083#1086' '#1089#1090#1086#1083#1073#1094#1086#1074':'
    end
    object edColCount: TcxSpinEdit
      Left = 150
      Top = 73
      Properties.MinValue = 1.000000000000000000
      TabOrder = 0
      Value = 1
      Width = 121
    end
    object cbISs: TcxComboBox
      Left = 6
      Top = 31
      Properties.DropDownListStyle = lsEditFixedList
      TabOrder = 1
      Width = 265
    end
  end
  object pBottom: TPanel
    Left = 0
    Top = 136
    Width = 281
    Height = 33
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 1
    object pCtrls: TPanel
      Left = 40
      Top = 0
      Width = 241
      Height = 33
      Align = alRight
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 0
      object bOk: TButton
        Left = 0
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Ok'
        Default = True
        TabOrder = 0
        OnClick = bOkClick
      end
      object bCancel: TButton
        Left = 83
        Top = 8
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
        OnClick = bCancelClick
      end
      object bHelp: TButton
        Left = 166
        Top = 8
        Width = 75
        Height = 25
        Caption = '&'#1055#1086#1084#1086#1097#1100
        TabOrder = 2
        OnClick = bHelpClick
      end
    end
  end
  object edRowCount: TcxSpinEdit
    Left = 150
    Top = 100
    Properties.MinValue = 1.000000000000000000
    TabOrder = 2
    Value = 1
    Width = 121
  end
end
