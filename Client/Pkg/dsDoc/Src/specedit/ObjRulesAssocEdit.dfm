object fmObjRulesAssocEdit: TfmObjRulesAssocEdit
  Left = 192
  Top = 139
  BorderIcons = [biSystemMenu, biMaximize]
  BorderWidth = 8
  Caption = #1059#1089#1090#1072#1085#1086#1074#1082#1072' '#1089#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1103
  ClientHeight = 448
  ClientWidth = 463
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 483
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pBottom: TPanel
    Left = 0
    Top = 415
    Width = 463
    Height = 33
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 0
    object pCtrls: TPanel
      Left = 222
      Top = 0
      Width = 241
      Height = 33
      Align = alRight
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 0
      object bOk: TButton
        Left = 0
        Top = 8
        Width = 75
        Height = 25
        Action = actOk
        Caption = 'Ok'
        Default = True
        TabOrder = 0
      end
      object bCancel: TButton
        Left = 83
        Top = 8
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
        OnClick = bCancelClick
      end
      object bHelp: TButton
        Left = 166
        Top = 8
        Width = 75
        Height = 25
        Caption = '&'#1055#1086#1084#1086#1097#1100
        TabOrder = 2
        OnClick = bHelpClick
      end
    end
  end
  object ClientPn: TPanel
    Left = 0
    Top = 0
    Width = 463
    Height = 415
    Align = alClient
    BevelOuter = bvNone
    Constraints.MinHeight = 334
    Constraints.MinWidth = 452
    TabOrder = 1
    OnResize = ClientPnResize
    DesignSize = (
      463
      415)
    object pTitle: TPanel
      Left = 0
      Top = 0
      Width = 463
      Height = 17
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  pTitle'
      Color = clMedGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
    end
    object ThisPn: TPanel
      Left = 0
      Top = 17
      Width = 192
      Height = 398
      Anchors = [akLeft, akTop, akBottom]
      BevelOuter = bvNone
      Constraints.MinHeight = 317
      Constraints.MinWidth = 192
      TabOrder = 1
      object tvThisStruct: TTreeView
        Left = 0
        Top = 40
        Width = 192
        Height = 334
        Align = alClient
        HideSelection = False
        Indent = 19
        PopupMenu = ThisStructPM
        ReadOnly = True
        RightClickSelect = True
        TabOrder = 0
        OnChange = tvThisStructChange
        OnContextPopup = tvThisStructContextPopup
        OnKeyDown = tvThisStructKeyDown
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 192
        Height = 40
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        DesignSize = (
          192
          40)
        object lbThisStructTitle: TLabel
          Left = 8
          Top = 8
          Width = 49
          Height = 13
          Caption = #1054#1073#1098#1077#1082#1090':'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbThisObjTitle: TLabel
          Left = 8
          Top = 24
          Width = 184
          Height = 13
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          Caption = 'lbThisObjTitle'
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 374
        Width = 192
        Height = 24
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object chbThisStructAutoExpand: TCheckBox
          Left = 8
          Top = 8
          Width = 89
          Height = 17
          Caption = #1040#1074#1090#1086#1089#1074#1077#1088#1090#1082#1072
          TabOrder = 0
          OnClick = chbThisStructAutoExpandClick
        end
      end
    end
    object RefPn: TPanel
      Left = 192
      Top = 17
      Width = 268
      Height = 398
      Anchors = [akLeft, akTop, akBottom]
      BevelOuter = bvNone
      Constraints.MinHeight = 317
      Constraints.MinWidth = 268
      TabOrder = 2
      object Panel2: TPanel
        Left = 76
        Top = 0
        Width = 192
        Height = 398
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object tvRefStruct: TTreeView
          Left = 0
          Top = 40
          Width = 192
          Height = 334
          Align = alClient
          AutoExpand = True
          Constraints.MinHeight = 253
          Constraints.MinWidth = 192
          HideSelection = False
          Indent = 19
          ReadOnly = True
          TabOrder = 0
          OnChange = tvRefStructChange
          OnExpanding = tvRefStructExpanding
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 192
          Height = 40
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            192
            40)
          object lbRefStructTitle: TLabel
            Left = 8
            Top = 8
            Width = 145
            Height = 13
            Caption = #1048#1085#1090#1077#1075#1088#1080#1088#1091#1077#1084#1099#1081' '#1086#1073#1098#1077#1082#1090':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lbRefObjTitle: TLabel
            Left = 8
            Top = 24
            Width = 184
            Height = 13
            Anchors = [akLeft, akTop, akRight]
            AutoSize = False
            Caption = 'lbRefObjTitle'
          end
        end
        object Panel7: TPanel
          Left = 0
          Top = 374
          Width = 192
          Height = 24
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object chbRefStructAutoExpand: TCheckBox
            Left = 8
            Top = 8
            Width = 97
            Height = 17
            Caption = #1040#1074#1090#1086#1089#1074#1077#1088#1090#1082#1072
            Checked = True
            State = cbChecked
            TabOrder = 0
            OnClick = chbRefStructAutoExpandClick
          end
        end
      end
      object EditCtrlsPn: TPanel
        Left = 0
        Top = 0
        Width = 76
        Height = 398
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        OnResize = EditCtrlsPnResize
        object bSetAssoc: TButton
          Left = 8
          Top = 117
          Width = 60
          Height = 25
          Action = actSetAssoc
          Caption = '<'
          TabOrder = 0
          TabStop = False
        end
        object bClearAssoc: TButton
          Left = 8
          Top = 150
          Width = 60
          Height = 25
          Action = actClearAssoc
          Caption = '>'
          TabOrder = 1
          TabStop = False
        end
      end
    end
  end
  object ActionList1: TActionList
    Left = 14
    Top = 76
    object actSetAssoc: TAction
      Caption = '&'#1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
      HelpType = htContext
      Hint = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
      OnExecute = actSetAssocExecute
    end
    object actOk: TAction
      Caption = 'O&k'
      HelpType = htContext
      Hint = 'Ok'
      OnExecute = actOkExecute
    end
    object actClearAssoc: TAction
      Caption = '&'#1057#1073#1088#1086#1089#1080#1090#1100
      HelpType = htContext
      Hint = #1057#1073#1088#1086#1089#1080#1090#1100
      OnExecute = actClearAssocExecute
    end
  end
  object ThisStructPM: TPopupMenu
    Left = 104
    Top = 197
    object N1: TMenuItem
      Action = actSetAssoc
    end
    object N2: TMenuItem
      Action = actClearAssoc
    end
  end
end
