object DurationEditFM: TDurationEditFM
  Left = 244
  Top = 324
  BorderStyle = bsDialog
  BorderWidth = 8
  Caption = #1044#1083#1080#1090#1077#1083#1100#1085#1086#1089#1090#1100
  ClientHeight = 97
  ClientWidth = 209
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BottomPn: TPanel
    Left = 0
    Top = 64
    Width = 209
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object OkBtn: TButton
      Left = 53
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Ok'
      Default = True
      TabOrder = 0
      OnClick = OkBtnClick
    end
    object CancelBtn: TButton
      Left = 134
      Top = 6
      Width = 75
      Height = 25
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = CancelBtnClick
    end
  end
  object ClientGBx: TGroupBox
    Left = 0
    Top = 0
    Width = 209
    Height = 64
    Align = alClient
    TabOrder = 0
    object TitleLb: TLabel
      Left = 8
      Top = 16
      Width = 127
      Height = 13
      Caption = #1047#1085#1072#1095#1077#1085#1080#1077' '#1076#1083#1080#1090#1077#1083#1100#1085#1086#1089#1090#1080':'
    end
    object Ed: TcxMaskEdit
      Left = 3
      Top = 35
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d?\d?\d\/(0?\d|1?[0-1])\/([0-2]?\d|3?[0-1])'
      Properties.MaxLength = 0
      Properties.OnValidate = EdPropertiesValidate
      TabOrder = 0
      Width = 203
    end
  end
end
