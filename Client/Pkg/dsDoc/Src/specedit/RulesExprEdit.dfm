object fmRulesExprEdit: TfmRulesExprEdit
  Left = 396
  Top = 337
  BorderStyle = bsDialog
  BorderWidth = 8
  Caption = #1055#1088#1072#1074#1080#1083#1072
  ClientHeight = 94
  ClientWidth = 377
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gbClient: TGroupBox
    Left = 0
    Top = 0
    Width = 377
    Height = 61
    Align = alClient
    TabOrder = 0
    object lbTitle: TLabel
      Left = 8
      Top = 16
      Width = 44
      Height = 13
      Caption = #1055#1088#1072#1074#1080#1083#1072
    end
    object edExpr: TcxMaskEdit
      Left = 8
      Top = 34
      TabOrder = 0
      Text = 'edExpr'
      Width = 357
    end
  end
  object pBottom: TPanel
    Left = 0
    Top = 61
    Width = 377
    Height = 33
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 1
    object pCtrls: TPanel
      Left = 136
      Top = 0
      Width = 241
      Height = 33
      Align = alRight
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 0
      object bOk: TButton
        Left = 0
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Ok'
        Default = True
        TabOrder = 0
        OnClick = bOkClick
      end
      object bCancel: TButton
        Left = 83
        Top = 8
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
        OnClick = bCancelClick
      end
      object bHelp: TButton
        Left = 166
        Top = 8
        Width = 75
        Height = 25
        Caption = '&'#1055#1086#1084#1086#1097#1100
        TabOrder = 2
        OnClick = bHelpClick
      end
    end
  end
end
