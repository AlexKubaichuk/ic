/*
  ����        - ICSDocBaseEdit.h
  ������      - ��������������� (ICS)
  ��������    - ������� ����� ��� ��������� ������� � ��������� �������������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 15.09.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  vX.X.X - 26.02.2015
    �������������� �� ��������� :)
  v1.1.1 - 27.12.2006
    [-] ����� UpdateImpDefAttrs
    [*] �����������

  v1.1.0 - 16.03.2006
    [+] protected-�������� ParentXML
    [+] public-����� NewUID
    [-] ��������� ��������� uid
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  v1.0.15 - 27.01.2006
    [+] protected-�������� ReqEdBkColor

  v1.0.14 - 04.04.2005
    1. ���������� ������ DKSetVersion �������� �� ������� �������� ������
    2. ����� ������������ inline ��� ������� TICSDocBaseEdit::UpdateTreeNodeData �
       TICSDocBaseEdit::UpdateTagNodeCtrl, �.�.
       ���� �������� � ��������� ��� ������ ������� �� ���������

  v1.0.13 - 03.03.2005
    1. ����� TICSDocBaseEdit::OnCurrentNodeDelete() ������������ �
       ����� ����������� TICSDocBaseEdit::BeforeCurrentNodeDelete()
    2. �������� ����� ����������� ����� TICSDocFilter::AfterCurrentNodeDelete()

  v1.0.12 - 09.02.2005
    1. ����� GetXMLNodeViewText ��������� � ������ public
    2. ����� CanDelete ������ ���� �����������
    3. �������� ����� OnCurrentNodeDelete()

  v1.0.11 - 22.11.2004
    1. ��������� protected-�������� Title
    2. �������� public-����� UpdateTreeNodeData()
    3. ��������� ���� � ������� MoveTreeNodeUp() � MoveTreeNodeDown():
       ���� ����������� TreeNode � ��������� ������ ( �.�. ���� ������ ),
       �� ��� �������� ����� Data ��������� �� ��������� TagNode'� - � �����
       ��� �������
    4. ���������� ����� DropTreeNodes - ������ � TagNode'��
       �������� ������ �� ��������� TreeNode'�

  v1.0.10 - 22.11.2004
    1. �������� ����� TICSDocControlProps
    2. ��������� protected-�������� Splitter

  v1.0.9  - 17.11.2004
    1. ��������� ����� � ������ LoadXML - ��� ���������� �� �������� ���������
       ARootNode ������ Expand � MakeVisible ���������� ��� FEditTV

  v1.0.8  - 18.10.2004
    1. ��������� ������������� ����������� IDKComponentVersion, IDKComponentComment

  v1.0.7  - 14.10.2004
    1. ��������� ������ MoveTreeNodeUp() � MoveTreeNodeDown()
    2. ����� ��� ����� ��������� TTreeNodes::BeginUpdate() � TTreeNodes::EndUpdate()
    3. ������ ��� ��������� ���'�� ������������ TDKXMLUIDGenerator

  v1.0.6  - 08.10.2004
    1. ��������� ������ GetDomainNode(),  InsertTreeNodeAfter(), AddChildTreeNodeFirst()
       � DeleteCurrentNode()

  v1.0.5  - 07.10.2004
    1. �������� ����� GetCurrentTagNode()

  v1.0.4  - 04.10.2004
    1. ��������� read only �������� EditTV

  v1.0.3  - 25.09.2004
    1. �������� ����� UpdateUIDs
    2. �������� ����� TreeNodesAssign
    3. ��������� �������� BaseUID
    4. ��������� ������� BeforeDelete
    5. ��������� ������ NewUID, SetUIDGen, InitUIDGen

  v1.0.2  - 24.09.2004
    1. ��������� ������ CheckEditTVExistence(), IsEmpty(), IsNone()
    2. �������� protected �������� CommonTBVisible

  v1.0.1  - 23.09.2004
    1. ��������� protected �������� CommonTBVisible
    2. �������� ����� FocusEditTVFirst()

  v1.0    - 15.09.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef ICSDocBaseEditH
#define ICSDocBaseEditH

//---------------------------------------------------------------------------

#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
//#include "TB2Dock.hpp"
//#include "TB2Toolbar.hpp"
#include "XMLContainer.h"
#include "DKClasses.h"
#include "cxClasses.hpp"
#include "dxBar.hpp"
#include "dsDocNomSupportTypeDef.h"

namespace ICSDocBaseEdit
{
typedef void __fastcall (__closure *TOnCopyItem)(TTagNode *TagNode, TTreeNode *TreeNode);

//###########################################################################
//##                                                                       ##
//##                                 Globals                               ##
//##                                                                       ##
//###########################################################################


//---------------------------------------------------------------------------

typedef bool __fastcall (__closure *TBeforeDeleteElementEvent)( TObject* Sender, TTagNode* RootNode );

//###########################################################################
//##                                                                       ##
//##                            TICSDocControlProps                        ##
//##                                                                       ##
//###########################################################################

// ��������� ��� �������������� ������� ���������� control'�

class PACKAGE TICSDocControlProps : public TPersistent
{
    typedef TPersistent inherited;
private:
    TControl *FControl;


protected:
    virtual void __fastcall AssignTo( Classes::TPersistent* Dest );
    virtual void __fastcall DefineProperties( TFiler* Filer );

    void __fastcall LoadAlignProperty(TReader *Reader);
    void __fastcall StoreAlignProperty(TWriter *Writer);

    int __fastcall GetWidth();
    int __fastcall GetHeight();
    TSizeConstraints* __fastcall GetConstraints();
    TAlign __fastcall GetAlign();
    void __fastcall SetWidth( const int AValue );
    void __fastcall SetHeight( const int AValue );
    void __fastcall SetConstraints( const TSizeConstraints* AValue );
    void __fastcall SetAlign( const TAlign AValue );

    __property TControl *Control = { read = FControl };


public:
    __fastcall TICSDocControlProps( TControl *AControl );


__published:
    __property int               Width       = { read = GetWidth      , write = SetWidth      , nodefault };
    __property int               Height      = { read = GetHeight     , write = SetHeight     , nodefault };
    __property TSizeConstraints* Constraints = { read = GetConstraints, write = SetConstraints, nodefault };
    __property TAlign            Align       = { read = GetAlign      , write = SetAlign      , stored = false };
};

//###########################################################################
//##                                                                       ##
//##                              TICSDocBaseEdit                          ##
//##                                                                       ##
//###########################################################################

// ������� ����� ��� ��������� ������� � ��������� �������������

class PACKAGE TICSDocBaseEdit : public TCustomPanel/*, IDKComponentVersion, IDKComponentComment*/
{
    typedef TCustomPanel inherited;
private:
    /*����*/
//    Variant             FExtNOM_DLL;                 // ���������� ����������� nom_dll.dll
    TAxeXMLContainer*   FXMLContainer;               // XML-��������� ( ��. AxeUtil.h )
    TTagNode*           FParentXML;                  // �������� �������������� XML-���������
    UnicodeString          FNmklGUI;                    // ��� ������������
    int                 FDomainNumber;               // ����� ������

    UnicodeString          FComment;                    // �����������

    TICSDocControlProps* FSplitterProps;             // �������� ������� FSplitter

    TBeforeDeleteElementEvent  FBeforeDelete;               // ������� "����� ���������" ��������
                                                     // ������������� �������
    int                 FBeforeDeleteDisableCount;   // ������� ���������� ������ DisableBeforeDelete

    /*��������� ����*/
    TTreeNode*   __treeNode;

    /*��������������� ������*/
    static void       __fastcall FGetLinkedIS( TTagNode *ALinkIS, TStringList *ASearchPath, TStringList *AISList, UnicodeString TagName = "rulesunit" );


protected:
    /*����*/
    TImageList*        FImageList;                  //������ �������� ��� �������� ( TAction )
    TActionList*       FActionList;                 //��������� ��������
    TdxBarManager*     FMainBM;                  //������� �������� ��� Toolbar'��
//    TTBDock*           FTBDockLeft;                 //������� �������� ��� Toolbar'��
    TdxBar*            FCommonTB;                   //Toolbar "�����"
    TdxBar*            FEditTB;                     //Toolbar "������"
    TPopupMenu*        FEditPM;                     //PopupMenu
    TSplitter*         FSplitter;                   //Splitter
    TTreeView*         FEditTV;                     //TreeView

    TTagNode*          FXML;                        //������������� xml
    bool               FCreated;                    //������� �������� xml'�
                                                    // FCreated == true  - New(), Load()
                                                    // FCreated == false - Set()
    bool               FModified;                   //������� ����������� FXML
    UnicodeString         FXMLTitle;                   //��������� xml-������
    TColor             FReqEdBkColor;               //���� ���� ��� "������������" ����� �����
    TdsNomCallDefProcEvent FOnCallDefProc;

    /*�������������� ��������*/
    // �������� �������������� XML-���������
    __property TTagNode* ParentXML = {read = FParentXML, write = FParentXML};
    __property TBeforeDeleteElementEvent BeforeDelete = { read = FBeforeDelete, write = FBeforeDelete };
    __property TTreeView* EditTV = { read = FEditTV };
    __property TICSDocControlProps* Splitter = { read = FSplitterProps, write = SetSplitterProps };
    //��������� xml-������ ( �� ��������� "XML" )
    __property UnicodeString Title = { read = FXMLTitle, write = SetXMLTitle };
    //���� ���� ��� "������������" ����� �����
    __property TColor ReqEdBkColor = { read = FReqEdBkColor, write = SetReqEdBkColor, default = clWindow };

    /*IDKComponentVersion*/
    virtual UnicodeString __fastcall DKGetVersion() const;
    virtual void       __fastcall DKSetVersion( const UnicodeString AValue );
    /*IDKComponentComment*/
    virtual UnicodeString __fastcall DKGetComment() const;
    virtual void       __fastcall DKSetComment( const UnicodeString AValue );

    /*TComponent*/
    virtual void __fastcall SetName(const UnicodeString Value);

    /*TICSDocBaseEdit*/
    //�������������, ���������������
    void __fastcall InitCommonTB();
    void __fastcall InitEditTB();
    void __fastcall InitSplitter();
    void __fastcall InitEditTV( TWinControl* AParent );
    void __fastcall InitAction( TAction*& AAction, UnicodeString  ACaption,
                                                   UnicodeString  AHint,
                                                   UnicodeString  AShortCut,
                                                   int AImageIndex,
                                                   TNotifyEvent AExecuter );
    void __fastcall LoadImageListBitmap( UnicodeString  ResName );
    void __fastcall CreateTBItem(TdxBarItemLinks *AParent, TdxBarLargeButton* &ATBItem, TAction *AAction = NULL, bool ACanBeginGroup = false);
    void __fastcall AddTBItem(TdxBarItemLinks *AParent, TdxBarLargeButton *ATBItem, bool ACanBeginGroup = false);
    void __fastcall CreateTBSubMenuItem(TdxBarItemLinks *AParent, TdxBarSubItem* &ATBItem, bool ACanBeginGroup = false);
    void __fastcall CreateTBSubMenuCleckedItem(TdxBarItemLinks *AParent, TdxBarLargeButton* &ATBItem, TdxBarPopupMenu* &ATBMenuItem, TAction *AAction = NULL, bool ACanBeginGroup = false);
    void __fastcall CreatePMItem(TPopupMenu *AParent, TMenuItem* &AItem, TAction *AAction = NULL);

    void __fastcall CheckEditTVExistence();
    virtual void __fastcall FreeXML();

    //��������� �������� �������
//    void __fastcall SetExtNOM_DLL ( const Variant &AValue );
    void __fastcall SetXMLContainer( TAxeXMLContainer* AValue );
    void __fastcall SetNmklGUI( UnicodeString  AValue );
    void __fastcall SetDomainNumber( int AValue );
    void __fastcall SetSplitterProps( const TICSDocControlProps *AValue );
    virtual void __fastcall SetXMLTitle( const UnicodeString AValue );
    virtual void __fastcall SetReqEdBkColor( TColor AValue );

    //������ �������� �������
    TTagNode* __fastcall GetNmkl();

    //������
    TAxeXMLContainer* __fastcall GetXMLContainer();
    TTagNode* __fastcall GetDomainNode();
    void __fastcall EnableBeforeDeleteEvent();
    void __fastcall DisableBeforeDeleteEvent();
    TTagNode* __fastcall GetCurrentTagNode();
    static UnicodeString __fastcall GetGUI(TTagNode *ANode);
    static void       __fastcall GetLinkedIS(TTagNode *ABaseIS, TStringList *AISList, UnicodeString TagName = "rulesunit" );
    int __fastcall GetISList( TTagNode *MainIS, TTreeNodes* TreeNodes, UnicodeString TagName = "rulesunit" );
    int __fastcall GetLinkedISAttrs( UnicodeString LinkISExtRef, TTreeNodes* TreeNodes );
    static void __fastcall TreeNodesAssign( TTreeNode* Dest, const TTreeNode* Src, bool IsRecurse = true );
    TTreeNode* __fastcall InsertTreeNodeAfter( TTreeNode* AInsTreeNode, UnicodeString  AInsTreeText, UnicodeString  ATagNodeName );
    TTreeNode* __fastcall AddChildTreeNodeFirst( TTreeNode* AParentTreeNode, UnicodeString  AChildTreeText, UnicodeString  ATagNodeName );
    virtual bool __fastcall CanDelete( TTreeNode* TreeNode );
    virtual void __fastcall BeforeCurrentNodeDelete() = 0;
    virtual void __fastcall DeleteCurrentNode();
    virtual void __fastcall AfterCurrentNodeDelete( UnicodeString DeletedNodeName ) = 0;
    virtual void __fastcall LoadXML( TTagNode* XMLLogicRoot, TTreeNode* ARootNode = NULL );
    virtual void __fastcall XMLToTree( TTagNode *TagNode, TTreeNode *TreeNode );
    virtual void __fastcall DropTreeNodes( TTreeNode* XMLLogicRoot, UnicodeString NodesNames );
    virtual void __fastcall DropTreeNodesAfterLoadXML( TTreeNode* XMLLogicRoot ) = 0;
    virtual void __fastcall EditTVContextPopup( TObject *Sender,
                    const TPoint &MousePos, bool &Handled );
    virtual void __fastcall EditTVChange( TObject *Sender, TTreeNode *Node ) = 0;
    virtual void __fastcall EditTVDrawItem(TCustomTreeView *Sender,
                    TTreeNode *Node, TCustomDrawState State, bool &DefaultDraw) = 0;
    bool __fastcall ISToTree(TTagNode *ItTag, void *Src1 = NULL, void *Src2 = NULL, void *Src3 = NULL, void *Src4 = NULL);

    void __fastcall ToTree(TTagNode* ANode, TTreeNode *ATreeNode);
    void __fastcall FToTree(TTagNode* ANode, TTreeNodes *Src, TTreeNode *xNode, TOnCopyItem AOnCopy);


public:
    __fastcall TICSDocBaseEdit(TComponent* Owner);
    __fastcall virtual ~TICSDocBaseEdit();

    //�������������� ��������
    __property DockManager ;

    //�������� ��������
    __property TdsNomCallDefProcEvent OnCallDefProc = { read = FOnCallDefProc, write = FOnCallDefProc};
//    Variant            ExtNOM_DLL;
    __property TAxeXMLContainer*  XMLContainer = { read = FXMLContainer, write = SetXMLContainer, default = NULL };
    __property UnicodeString         NmklGUI      = { read = FNmklGUI,      write = SetNmklGUI };
    __property TTagNode*          Nmkl         = { read = GetNmkl };

    //�������� ������
    virtual TTagNode* __fastcall GetPassport();
    virtual bool      __fastcall SavePassport( TTagNode* APassport );
    virtual TTagNode* __fastcall Get();
    virtual void      __fastcall Save( TTagNode* AXML );
    virtual bool __fastcall Delete();
    virtual void __fastcall FocusEditTVFirst();
    virtual bool __fastcall IsEmpty();
    virtual bool __fastcall IsNone();
    virtual UnicodeString __fastcall NewUID();

    //��������������� ������
    virtual UnicodeString __fastcall GetXMLNodeViewText( TTagNode *TagNode ) = 0;
    static UnicodeString __fastcall GetExtRef(TTagNode *ANode);
    static void __fastcall UpdateTreeNodeData( TTagNode* TagNode, bool bClear = false );
    static void __fastcall UpdateTagNodeCtrl( TTreeNode* TreeNode, bool bClear = false );
    static bool __fastcall MoveTreeNodeUp  ( TTreeNode* ATreeNode, bool bGenerateOnChange, bool bMoveTagNode = true );
    static bool __fastcall MoveTreeNodeDown( TTreeNode* ATreeNode, bool bGenerateOnChange, bool bMoveTagNode = true );


__published:
    //�������������� ��������
    __property UnicodeString Comment = { read = DKGetComment, write = DKSetComment};
    __property UnicodeString About = { read = DKGetVersion, write = DKSetVersion, stored = false };

    //�������� ��������
    __property int        DomainNumber = { read = FDomainNumber, write = SetDomainNumber, default = 0 };
    __property bool       Modified = { read = FModified, default = false };

    //�������������� ��������
    __property Align  = {default=0};
    __property Alignment  = {default=2};
    __property Anchors  = {default=3};
    __property AutoSize  = {default=0};
    __property BevelInner  = {default=0};
    __property BevelOuter  = {default=bvNone};
    __property BevelWidth  = {default=1};
    __property BiDiMode ;
    __property BorderWidth  = {default=0};
    __property BorderStyle  = {default=0};
    __property Color  = {default=-2147483633};
    __property Constraints ;
    __property Ctl3D ;
    __property UseDockManager  = {default=1};
    __property DockSite  = {default=0};
    __property DragCursor  = {default=-12};
    __property DragKind  = {default=0};
    __property DragMode  = {default=0};
    __property Enabled  = {default=1};
    __property FullRepaint  = {default=1};
    __property Font ;
    __property Locked  = {default=0};
    __property ParentBiDiMode  = {default=1};
    __property ParentColor  = {default=0};
    __property ParentCtl3D  = {default=1};
    __property ParentFont  = {default=1};
    __property ParentShowHint  = {default=1};
    __property ShowHint ;
    __property TabOrder  = {default=-1};
    __property TabStop  = {default=0};
    __property Visible  = {default=1};
    __property OnCanResize ;
    __property OnConstrainedResize ;
    __property OnEnter ;
    __property OnExit ;
    __property OnGetSiteInfo ;
    __property OnResize ;
};

//---------------------------------------------------------------------------

} // end of namespace ICSDocBaseEdit
using namespace ICSDocBaseEdit;

#endif
