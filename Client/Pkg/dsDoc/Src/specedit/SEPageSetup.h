//---------------------------------------------------------------------------

#ifndef SEPageSetupH
#define SEPageSetupH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxGroupBox.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxRadioGroup.hpp"
#include "cxSpinEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
class PACKAGE TSEPageSetupForm : public TForm
{
__published:	// IDE-managed Components
        TcxGroupBox *OrientationGB;
        TcxGroupBox *FieldsGB;
        TcxRadioButton *portRB;
        TcxRadioButton *landRB;
        TcxSpinEdit *BottomMargED;
        TcxSpinEdit *LeftMargED;
        TcxSpinEdit *TopMargED;
        TcxSpinEdit *RightMargED;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TImage *landIM;
        TImage *portIM;
        TcxSpinEdit *PrKoeffSBE;
        TLabel *PrKoeffLab;
        void __fastcall portRBClick(TObject *Sender);
        void __fastcall landRBClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TSEPageSetupForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TSEPageSetupForm *SEPageSetupForm;
//---------------------------------------------------------------------------
#endif
