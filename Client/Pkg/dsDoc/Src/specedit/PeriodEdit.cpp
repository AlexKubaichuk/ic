/*
  ����        - PeriodEdit.cpp
  ������      - ��������������� (ICS)
  ��������    - ������ �������������� �������� �������.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 29.12.2006
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "PeriodEdit.h"
#include <DateUtils.hpp>
#include "ICSTL.h"
//#include "DKMBUtils.h"
#include "msgdef.h"
#include "ICSDateutil.hpp"
#include "DKClasses.h"
#include "ICSDocGlobals.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxCalendar"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDateUtils"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxSpinEdit"
#pragma link "cxTextEdit"
#pragma link "dxCore"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                             TPeriodEditFM                             ##
//##                                                                       ##
//###########################################################################

__fastcall TPeriodEditFM::TPeriodEditFM(TComponent* Owner)
    : TForm(Owner)
{
  Caption = FMT(icsDocPeriodEditCaption);
  OkBtn->Caption = FMT(icsDocPeriodEditOkBtnBtnCaption);
  CancelBtn->Caption = FMT(icsDocPeriodEditCancelBtnBtnCaption);
  ClientGBx->Caption = FMT(icsDocPeriodEditClientGBxGBCaption);
  HalfYearYearLb->Caption = FMT(icsDocPeriodEditHalfYearYearLbLabCaption);
  QuoterYearLb->Caption = FMT(icsDocPeriodEditQuoterYearLbLabCaption);
  MonthYearLb->Caption = FMT(icsDocPeriodEditMonthYearLbLabCaption);
  UniqueSepLb->Caption = FMT(icsDocPeriodEditUniqueSepLbLabCaption);
  YearRBtn->Caption = FMT(icsDocPeriodEditYearRBtnRBCaption);
  HalfYearRBtn->Caption = FMT(icsDocPeriodEditHalfYearRBtnRBCaption);
  QuoterRBtn->Caption = FMT(icsDocPeriodEditQuoterRBtnRBCaption);
  MonthRBtn->Caption = FMT(icsDocPeriodEditMonthRBtnRBCaption);
  UniqueRBtn->Caption = FMT(icsDocPeriodEditUniqueRBtnRBCaption);
  UnDefRBtn->Caption = FMT(icsDocPeriodEditUnDefRBtnRBCaption);

  HalfYearCBx->Properties->Items->Clear();
  HalfYearCBx->Properties->Items->Add(FMT(icsDocPeriodEditHalfYearCBxItems1));
  HalfYearCBx->Properties->Items->Add(FMT(icsDocPeriodEditHalfYearCBxItems2));
  HalfYearCBx->Text = FMT(icsDocPeriodEditHalfYearCBxItems1);
  QuoterCBx->Properties->Items->Clear();
  QuoterCBx->Properties->Items->Add(FMT(icsDocPeriodEditQuoterCBxItems1));
  QuoterCBx->Properties->Items->Add(FMT(icsDocPeriodEditQuoterCBxItems2));
  QuoterCBx->Properties->Items->Add(FMT(icsDocPeriodEditQuoterCBxItems3));
  QuoterCBx->Properties->Items->Add(FMT(icsDocPeriodEditQuoterCBxItems4));
  QuoterCBx->Text = FMT(icsDocPeriodEditQuoterCBxItems1);

  FPeriodDef = pdUnDef;
  FPeriodDefMap[pdUnDef]    = UnDefRBtn;     FPeriodDefCodeMap[UnDefRBtn]    = pdUnDef;
  FPeriodDefMap[pdYear]     = YearRBtn;      FPeriodDefCodeMap[YearRBtn]     = pdYear;
  FPeriodDefMap[pdHalfYear] = HalfYearRBtn;  FPeriodDefCodeMap[HalfYearRBtn] = pdHalfYear;
  FPeriodDefMap[pdQuoter]   = QuoterRBtn;    FPeriodDefCodeMap[QuoterRBtn]   = pdQuoter;
  FPeriodDefMap[pdMonth]    = MonthRBtn;     FPeriodDefCodeMap[MonthRBtn]    = pdMonth;
  FPeriodDefMap[pdUnique]   = UniqueRBtn;    FPeriodDefCodeMap[UniqueRBtn]   = pdUnique;

  for (int i = 0; i < 12; i++)
    MonthCBx->Properties->Items->Add(FormatSettings.LongMonthNames[i]);  //LongMonthNames
  MonthCBx->ItemIndex = 0;
  SetYearCtrlsValue(YearOf(Date()));
  UniqueEndEd->Date = Date();

  YearEd->Enabled         = false;
  HalfYearCBx->Enabled    = false;
  HalfYearYearLb->Enabled = false;
  HalfYearYearEd->Enabled = false;
  QuoterCBx->Enabled      = false;
  QuoterYearLb->Enabled   = false;
  QuoterYearEd->Enabled   = false;
  MonthCBx->Enabled       = false;
  MonthYearLb->Enabled    = false;
  MonthYearEd->Enabled    = false;
  UniqueBegEd->Enabled    = false;
  UniqueSepLb->Enabled    = false;
  UniqueEndEd->Enabled    = false;
  UniqueBegEd->Style->Color = clBtnFace;
  UniqueEndEd->Style->Color = clBtnFace;
}

//---------------------------------------------------------------------------

void __fastcall TPeriodEditFM::SetPeriodDef(TPeriodDef AValue)
{
  FPeriodDefMap[AValue]->Checked = true;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TPeriodEditFM::GetPeriodDefXMLStr() const
{
  UnicodeString sXMLStr;
  switch (FPeriodDef)
  {
    case pdUnDef   : sXMLStr = "undef";    break;
    case pdYear    : sXMLStr = "year";     break;
    case pdHalfYear: sXMLStr = "halfyear"; break;
    case pdQuoter  : sXMLStr = "quarter";  break;
    case pdMonth   : sXMLStr = "month";    break;
    case pdUnique  : sXMLStr = "unique";   break;
    default:
      throw DKClasses::EMethodError(__FUNC__, cFMT1(icsDocPeriodEditGetPeriodDefXMLStr,IntToStr((int)FPeriodDef)));
  }
  return sXMLStr;
}

//---------------------------------------------------------------------------

void __fastcall TPeriodEditFM::SetPeriodDefXMLStr(const UnicodeString AValue)
{
  TPeriodDef nPeriodDef;
  if      (AValue == "undef")    nPeriodDef = pdUnDef;
  else if (AValue == "year")     nPeriodDef = pdYear;
  else if (AValue == "halfyear") nPeriodDef = pdHalfYear;
  else if (AValue == "quarter")  nPeriodDef = pdQuoter;
  else if (AValue == "month")    nPeriodDef = pdMonth;
  else if (AValue == "unique")   nPeriodDef = pdUnique;
  else
    throw DKClasses::EMethodError(__FUNC__, cFMT1(icsDocPeriodEditSetPeriodDefXMLStr,AValue));
  PeriodDef = nPeriodDef;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TPeriodEditFM::GetPeriodValueXMLStr() const
{
  if (!ValidateInputs(true)) // silent validation
    throw DKClasses::EMethodError(__FUNC__, cFMT(icsDocPeriodEditGetPeriodValueXMLStr));

  UnicodeString sXMLStr;
  if (FPeriodDef != pdUnDef)
  {
    TDate dtBeg, dtEnd;
    unsigned short usYear, usMonth, usDay;
    switch (FPeriodDef)
    {
      case pdYear    :
        usYear = YearEd->Value;
        usDay = 1;  usMonth = 1;  dtBeg = EncodeDate(usYear, usMonth, usDay);
        usDay = 31; usMonth = 12; dtEnd = EncodeDate(usYear, usMonth, usDay);
      break;

      case pdHalfYear:
        usYear = HalfYearYearEd->Value;
        switch (HalfYearCBx->ItemIndex)
        {
          case 0:
            usDay = 1;  usMonth = 1;  dtBeg = EncodeDate(usYear, usMonth, usDay);
            usDay = 30; usMonth = 6;  dtEnd = EncodeDate(usYear, usMonth, usDay);
          break;

          case 1:
            usDay = 1;  usMonth = 7;  dtBeg = EncodeDate(usYear, usMonth, usDay);
            usDay = 31; usMonth = 12; dtEnd = EncodeDate(usYear, usMonth, usDay);
          break;

          default:
            throw DKClasses::EMethodError(__FUNC__, cFMT1(icsDocPeriodEditGetPeriodValueXMLStrHalfYear,IntToStr((int)HalfYearCBx->ItemIndex)));
        }
      break;

      case pdQuoter  :
        usYear = QuoterYearEd->Value;
        switch (QuoterCBx->ItemIndex)
        {
          case 0:
            usDay = 1;  usMonth = 1;  dtBeg = EncodeDate(usYear, usMonth, usDay);
            usDay = 31; usMonth = 3;  dtEnd = EncodeDate(usYear, usMonth, usDay);
          break;

          case 1:
            usDay = 1;  usMonth = 4;  dtBeg = EncodeDate(usYear, usMonth, usDay);
            usDay = 30; usMonth = 6;  dtEnd = EncodeDate(usYear, usMonth, usDay);
          break;

          case 2:
            usDay = 1;  usMonth = 7;  dtBeg = EncodeDate(usYear, usMonth, usDay);
            usDay = 30; usMonth = 9;  dtEnd = EncodeDate(usYear, usMonth, usDay);
          break;

          case 3:
            usDay = 1;  usMonth = 10; dtBeg = EncodeDate(usYear, usMonth, usDay);
            usDay = 31; usMonth = 12; dtEnd = EncodeDate(usYear, usMonth, usDay);
          break;

          default:
            throw DKClasses::EMethodError(__FUNC__, cFMT1(icsDocPeriodEditGetPeriodValueXMLStrQuart,IntToStr((int)QuoterCBx->ItemIndex)));
        }
      break;

      case pdMonth   :
        usMonth = MonthCBx->ItemIndex + 1;
        usYear = MonthYearEd->Value;
        dtBeg = EncodeDate(usYear, usMonth, 1);
        dtEnd = EncodeDate(usYear, usMonth, DaysInAMonth(usYear, usMonth));
      break;

      case pdUnique  :
        dtBeg = UniqueBegEd->Date;
        dtEnd = UniqueEndEd->Date;
      break;

      default:
        throw DKClasses::EMethodError(__FUNC__, cFMT1(icsDocPeriodEditGetPeriodDefXMLStr,IntToStr((int)FPeriodDef)));
    } // switch(FPeriodDef)
    sXMLStr = PeriodToXMLStr(dtBeg, dtEnd);
  } // if (FPeriodDef != pdUnDef)

  return sXMLStr;
}

//---------------------------------------------------------------------------

void __fastcall TPeriodEditFM::SetPeriodValueXMLStr(const UnicodeString AValue)
{
  if (FPeriodDef != pdUnDef)
  {
    TDate dtBeg, dtEnd;
    unsigned short usYear, usMonth, usDay;
    XMLStrToPeriod(AValue, &dtBeg, &dtEnd);
    switch (FPeriodDef)
    {
      case pdYear    :
        DecodeDate(dtBeg, usYear, usMonth, usDay);
        SetYearCtrlsValue(usYear);
      break;

      case pdHalfYear:
        DecodeDate(dtBeg, usYear, usMonth, usDay);
        switch (usMonth)
        {
          case 1: HalfYearCBx->ItemIndex = 0; break;
          case 7: HalfYearCBx->ItemIndex = 1; break;
          default:
            throw DKClasses::EMethodError(__FUNC__, cFMT1(icsDocPeriodEditSetPeriodValueXMLStrHalfYear,AValue));
        }
        SetYearCtrlsValue(usYear);
      break;

      case pdQuoter  :
        DecodeDate(dtBeg, usYear, usMonth, usDay);
        switch (usMonth)
        {
          case 1 : QuoterCBx->ItemIndex = 0; break;
          case 4 : QuoterCBx->ItemIndex = 1; break;
          case 7 : QuoterCBx->ItemIndex = 2; break;
          case 10: QuoterCBx->ItemIndex = 3; break;
          default:
            throw DKClasses::EMethodError(__FUNC__, cFMT1(icsDocPeriodEditSetPeriodValueXMLStrQuart,AValue));
        }
        SetYearCtrlsValue(usYear);
      break;

      case pdMonth   :
        DecodeDate(dtBeg, usYear, usMonth, usDay);
        MonthCBx->ItemIndex = usMonth - 1;
        if (MonthCBx->ItemIndex != usMonth - 1)
          throw DKClasses::EMethodError(__FUNC__, cFMT1(icsDocPeriodEditSetPeriodValueXMLStrMonth,AValue));
        SetYearCtrlsValue(usYear);
      break;

      case pdUnique  :
        UniqueBegEd->Date = dtBeg;
        UniqueEndEd->Date = dtEnd;
      break;

      default:
        throw DKClasses::EMethodError(__FUNC__, cFMT1(icsDocPeriodEditGetPeriodDefXMLStr,IntToStr((int)FPeriodDef)));
    } // switch(FPeriodDef)
  } // if (FPeriodDef != pdUnDef)
}

//---------------------------------------------------------------------------

void __fastcall TPeriodEditFM::SetYearCtrlsValue(unsigned short AYear)
{
  YearEd->Value         = AYear;
  HalfYearYearEd->Value = AYear;
  QuoterYearEd->Value   = AYear;
  MonthYearEd->Value    = AYear;
}

//---------------------------------------------------------------------------

void __fastcall TPeriodEditFM::UpdateCtrlsEnabled(TPeriodDef ADef, bool AEnabled)
{
  switch (ADef)
  {
    case pdUnDef:
    break;

    case pdYear:
      YearEd->Enabled = AEnabled;
    break;

    case pdHalfYear:
      HalfYearCBx->Enabled    = AEnabled;
      HalfYearYearLb->Enabled = AEnabled;
      HalfYearYearEd->Enabled = AEnabled;
    break;

    case pdQuoter:
      QuoterCBx->Enabled    = AEnabled;
      QuoterYearLb->Enabled = AEnabled;
      QuoterYearEd->Enabled = AEnabled;
    break;

    case pdMonth:
      MonthCBx->Enabled    = AEnabled;
      MonthYearLb->Enabled = AEnabled;
      MonthYearEd->Enabled = AEnabled;
    break;

    case pdUnique:
      UniqueBegEd->Enabled = AEnabled;
      UniqueSepLb->Enabled = AEnabled;
      UniqueEndEd->Enabled = AEnabled;
    break;
  } // switch(ADef)
}

//---------------------------------------------------------------------------

bool __fastcall TPeriodEditFM::ValidateInputs(bool ASilent) const
{
  if (FPeriodDef == pdUnique)
  {
    TDate dtBeg, dtEnd;
    if ((int)UniqueBegEd->Date == Cxdateutils::NullDate)
     {
       if (!ASilent)
       {
         UniqueBegEd->SetFocus();
         UniqueBegEd->SelectAll();
         _MSG_ERRA(FMT(icsDocPeriodEditValidateInputsBegin),"");
       }
       return false;
     }
    if ((int)UniqueEndEd->Date == Cxdateutils::NullDate)
     {
       if (!ASilent)
       {
         UniqueEndEd->SetFocus();
         UniqueEndEd->SelectAll();
         _MSG_ERRA(FMT(icsDocPeriodEditValidateInputsEnd), "");
       }
       return false;
     }

    if (dtBeg > dtEnd)
    {
      if (!ASilent)
      {
        UniqueBegEd->SetFocus();
        UniqueBegEd->SelectAll();
        _MSG_ERRA(FMT(icsDocPeriodEditValidateInputsNotEq), "");
      }
      return false;
    }
  }
  return true;
}

//---------------------------------------------------------------------------
#include <algorithm>
void __fastcall TPeriodEditFM::DefRBtnClick(TObject *Sender)
{
  UpdateCtrlsEnabled(FPeriodDef, false);
  TPeriodDefCodeMap::iterator i = FPeriodDefCodeMap.find(dynamic_cast<TRadioButton*>(Sender));
  if (i == FPeriodDefCodeMap.end())
    throw DKClasses::EMethodError(__FUNC__, cFMT(icsDocPeriodEditDefRBtnClick));
  FPeriodDef = (*i).second;
  UpdateCtrlsEnabled(FPeriodDef, true);
}

//---------------------------------------------------------------------------

void __fastcall TPeriodEditFM::OkBtnClick(TObject *Sender)
{
  // ����� ����������� OnExit ��������� control'� �� ������ ����
  // ������ ��������� ��� ������ �� ���������
  dynamic_cast<TWinControl*>(Sender)->SetFocus();
  if (ValidateInputs(false)) // non-silent validation
    ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TPeriodEditFM::CancelBtnClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------


