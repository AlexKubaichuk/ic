/*
  ����        - ListSortOrder.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� �������� �����
                ��� �������� ������� ���������� ��� ������
  ����������� - ������� �.�.
  ����        - 12.10.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  27.01.2006
    [*] �������� ������������ ��������� ����������

  23.05.2005
    1. ��������� �������� ������� ���������� �� Shift+Enter
    2. ��������� hint'� ��� ������

  12.10.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef ListSortOrderH
#define ListSortOrderH

//---------------------------------------------------------------------------


#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>

//#include "ICSDoc.h"	//��������� �������� � ���� ��������
#include "XMLContainer.h"
//#include "TB2Item.hpp"

namespace ListSortOrder
{

//***************************************************************************
//********************** ���� ***********************************************
//***************************************************************************

typedef int __fastcall (__closure *TOnColListLoadEvent)( TObject* Sender, const TTagNode* ndList, TTreeNodes* TreeNodes );
typedef void __fastcall (__closure *TOnListColUpdateEvent)( TObject* Sender, const TTagNode* ndListCol, TTreeNode* ListColNode );

//---------------------------------------------------------------------------

//����� ��� �������� ������� ���������� ��� ������

class PACKAGE TfmListSortOrder : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *gbClient;
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
        TLabel *ColLab;
        TTreeView *tvCols;
        TButton *bAddCols;
        TButton *bDelete;
        TButton *bClear;
        TButton *bMoveUp;
        TButton *bMoveDown;
        TTreeView *tvSortBy;
        TImageList *ImageList;
        TLabel *SortOrderLab;
  TPopupMenu *SortPM;
  TMenuItem *AscSortTBItem;
  TMenuItem *DescSortTBItem;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bOkClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall tvColsCustomDrawItem(TCustomTreeView *Sender,
          TTreeNode *Node, TCustomDrawState State, bool &DefaultDraw);
        void __fastcall tvColsChange(TObject *Sender, TTreeNode *Node);
        void __fastcall bAddColsClick(TObject *Sender);
        void __fastcall tvSortByDblClick(TObject *Sender);
        void __fastcall tvSortByChange(TObject *Sender, TTreeNode *Node);
        void __fastcall bMoveUpClick(TObject *Sender);
        void __fastcall bMoveDownClick(TObject *Sender);
        void __fastcall bClearClick(TObject *Sender);
        void __fastcall bDeleteClick(TObject *Sender);
        void __fastcall tvSortByContextPopup(TObject *Sender,
          TPoint &MousePos, bool &Handled);
        void __fastcall AscSortTBItemClick(TObject *Sender);
        void __fastcall DescSortTBItemClick(TObject *Sender);
        void __fastcall tvColsKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall tvSortByKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);


private:	// User declarations
        TTagNode *FListTagNode;                         //TagNode, ��������������� "list"
        TTagNode *FSortOrderTagNode;                    //TagNode, ��������������� "sortorder"
        TOnColListLoadEvent FOnColListLoad;             //������� "��� �������� ������ ������� ������"
        TOnListColUpdateEvent FOnListColUpdate;         //������� "��� ���������� ������� ������"

        bool __NeedUpdateSortByList;


protected:
        void __fastcall SetSortOrderTagNode( const TTagNode* AValue );
        void __fastcall UpdateSortByList( bool bReloadColList = true );
        void __fastcall InvertSortType( TTreeNode *ATreeNode );

        
public:		// User declarations
        __fastcall TfmListSortOrder(TComponent* Owner, const TTagNode* AListTagNode);
        __fastcall ~TfmListSortOrder();

        __property TTagNode *SortOrderTagNode = { read = FSortOrderTagNode, write = SetSortOrderTagNode };
        __property TOnColListLoadEvent OnColListLoad = { read = FOnColListLoad, write = FOnColListLoad };
        __property TOnListColUpdateEvent OnListColUpdate = { read = FOnListColUpdate, write = FOnListColUpdate};
};

//---------------------------------------------------------------------------

} //end of namespace ListSortOrder
using namespace ListSortOrder;

#endif
