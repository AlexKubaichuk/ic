/*
  ����        - ParametersEdit.h
  ������      - ��������������� (ICS)
  ��������    - ������ �������������� ���������� �������������.
                ������������ ����
                (������������ ����������� TICSDocSpec)
  ����������� - ������� �.�.
  ����        - 20.09.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  16.03.2006
    [*] ��� ���������� ����� �������� �� ��������� ��� �������� wordwrap == 1
    [*] ��������� uid �������������� ���������� TTagNode
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  27.01.2006
    [+] ��������� �������� TICSDocSpec::ReqEdBkColor
    [*] ���� ����� "����������" ������������� � "��������" 
    [*] �������� ������������ ��������� ����������

  02.12.2004
    1. ������� OnGetSpecPath ���������� � OnGetSpecNode

  25.11.2004
    1. ��������� �������� DocByDocIcon

  15.11.2004
    1. �������� ����� ������� �������������� ������ ������������ ���������
       �� ���������

  20.09.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef ParametersEditH
#define ParametersEditH

//---------------------------------------------------------------------------

#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtonEdit.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxSpinEdit.hpp"
#include "cxTextEdit.hpp"
#include "DKDialogs.hpp"
#include "DKListBox.h"
//---------------------------------------------------------------------------
//#include "ICSDoc.h"  //��������� �������� � ���� ��������
#include "XMLContainer.h"
#include "StyleNameEdit.h"
#include "ICSDocGlobals.h"
#include "DocRulesEdit.h"
#include "cxDropDownEdit.hpp"
//---------------------------------------------------------------------------

namespace ParametersEdit
{

//###########################################################################
//##                                                                       ##
//##                                 Globals                               ##
//##                                                                       ##
//###########################################################################

typedef void __fastcall (__closure *TOnGetSpecOwnerEvent)( TObject *Sender, UnicodeString AAutorCode, UnicodeString &AAutorName );
typedef bool __fastcall (__closure *TOnEditCondRulesEvent)( TObject *Sender, UnicodeString ATitle, TTagNode **ACondRules );
typedef bool __fastcall (__closure *TOnCheckStyleExistenceEvent)( TObject *Sender, UnicodeString AStyleUID );

//###########################################################################
//##                                                                       ##
//##                             TfmParametersEdit                         ##
//##                                                                       ##
//###########################################################################

// ������ �������������� ���������� �������������

class PACKAGE TfmParametersEdit : public TForm
{
__published:	// IDE-managed Components
    TPanel *pBottom;
    TPanel *pCtrls;
    TButton *bApply;
    TButton *bCancel;
    TButton *bHelp;
    TPageControl *PageControl;
    TTabSheet *tsCommon;
    TTabSheet *tsPageSetup;
    TTabSheet *tsStyles;
    TDKListBox *lbxStyles;
    TLabel *Label1;
    TBevel *Bevel1;
        TLabel *SpecNameLab;
    TLabel *Label3;
    TLabel *Label4;
    TLabel *Label5;
    TBevel *Bevel2;
    TLabel *Label6;
    TLabel *lbTimeStamp;
    TLabel *Label8;
    TBevel *Bevel3;
    TLabel *Label9;
    TBevel *Bevel4;
    TLabel *Label10;
    TBevel *Bevel5;
    TLabel *Label11;
    TLabel *lbOrientation;
    TLabel *Label13;
    TLabel *Label14;
    TLabel *Label15;
    TLabel *Label16;
    TButton *bChangePageSetup;
    TLabel *lbPageSize;
    TLabel *lbLeftMargin;
    TLabel *lbTopMargin;
    TLabel *lbRightMargin;
    TLabel *lbBottomMargin;
    TLabel *Label22;
    TBevel *Bevel6;
    TLabel *lbFontInfo;
    TCheckBox *chbUseBDef;
    TBevel *Bevel7;
    TCheckBox *chbUseHDef;
    TButton *bAddStyle;
    TButton *bDeleteStyle;
    TActionList *ActionList;
    TAction *actAddStyle;
    TAction *actDeleteStyle;
    TSpeedButton *sbAlignLeft;
    TSpeedButton *sbAlignCenter;
    TSpeedButton *sbAlignRight;
    TSpeedButton *sbAlignJustify;
    TCheckBox *chbWordWrap;
    TPaintBox *pbBold;
    TPaintBox *pbItalic;
    TPaintBox *pbUnderline;
    TPaintBox *pbStrike;
    TButton *bChangeFont;
    TButton *Button1;
    TAction *actEditStyle;
    TDKFontDialog *DKFontDialog;
    TLabel *lbAutor;
    TBevel *Bevel8;
    TLabel *CondRulesLb;
    TLabel *DocRulesLb;
    TGroupBox *GroupBox1;
    TCheckBox *chbPersonal;
    TCheckBox *chbGroup;
    TCheckBox *SaveBDChBx;
  TcxSpinEdit *edRelease;
  TcxSpinEdit *edVersion;
  TcxMaskEdit *edTitle;
  TcxButtonEdit *DocRulesEd;
  TcxButtonEdit *CondRulesEd;
  TPopupMenu *TBPopupMenu;
  TMenuItem *N1;
  TMenuItem *N2;
  TMenuItem *N3;
  TcxImageList *TBImageList;
 TPrinterSetupDialog *PrinterSetupDlg;
 TFontDialog *FontDlg;
 TLabel *Label2;
 TLabel *Label7;
 TcxComboBox *PeriodDefCB;
    void __fastcall bCancelClick(TObject *Sender);
    void __fastcall bApplyClick(TObject *Sender);
    void __fastcall bHelpClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall lbxStylesSelect(TObject *Sender,
      int ListItemIndex);
    void __fastcall lbxStylesDrawListItemImage(TObject *Sender,
      int ListItemIndex, int &ItemImageIndex);
    void __fastcall EditorChange(TObject *Sender);
    void __fastcall bChangePageSetupClick(TObject *Sender);
    void __fastcall PaintBoxPaint(TObject *Sender);
    void __fastcall actAddStyleExecute(TObject *Sender);
    void __fastcall actDeleteStyleExecute(TObject *Sender);
    void __fastcall actEditStyleExecute(TObject *Sender);
    void __fastcall sbAlignLeftClick(TObject *Sender);
    void __fastcall sbAlignCenterClick(TObject *Sender);
    void __fastcall sbAlignRightClick(TObject *Sender);
    void __fastcall sbAlignJustifyClick(TObject *Sender);
    void __fastcall chbWordWrapClick(TObject *Sender);
    void __fastcall chbUseBDefClick(TObject *Sender);
    void __fastcall chbUseHDefClick(TObject *Sender);
    void __fastcall bChangeFontClick(TObject *Sender);
    void __fastcall chbOrientationClick(TObject *Sender);
  void __fastcall CondRulesEdPropertiesButtonClick(TObject *Sender, int AButtonIndex);
  void __fastcall DocRulesEdPropertiesButtonClick(TObject *Sender, int AButtonIndex);




private:	// User declarations
    const UnicodeString csValueSet;
    const UnicodeString csValueUnset;

    TTagNode* FPassport;
    TTagNode* FCondRules;
    TTagNode* FDocRules;
    TTagNode* FSpec;

    int FNewStylesCount;
    TUnicodeStringList FDropedStyles;

    TIcon*          FDocByDocIcon;          //������ ��� ������� ��������������
                                            //������ ������������ ��������� �� ���������

    TOnGetSpecOwnerEvent          FOnGetSpecOwner;
    TOnEditCondRulesEvent         FOnEditCondRules;
    TOnCheckStyleExistenceEvent   FOnCheckStyleExistence;
    TOnGetSpecificatorsListEvent  FOnGetSpecsList;
    TOnGetOrgsListEvent           FOnGetOrgsList;
    TOnGetSpecNodeEvent           FOnGetSpecNode;
    TOnGetDocElementsListEvent    FOnGetDocElementsList;
    TOnGetDocElementStructEvent   FOnGetThisDocElementStruct;
    TOnGetDocElementStructEvent   FOnGetRefDocElementStruct;
    TOnGetDocElementViewTextEvent FOnGetDocElementViewText;
    TOnGetDocElementStructEvent   FOnGetDocElementStruct;

    TColor __fastcall GetReqEdBkColor() const;
    bool __fastcall AreInputsCorrect();
    void __fastcall UpdateRulesEdit(TTagNode *ndRules, TcxButtonEdit *AEdit);
    void __fastcall UpdateCondRulesEdit();
    void __fastcall UpdateDocRulesEdit();
    void __fastcall UpdatePageSetupControls();
    void __fastcall UpdateFontControls( TTagNode* ndNode );
    bool __fastcall ValidateStyleEditData( TfmStyleEdit *Sender );
    void __fastcall SetCurStyleAlign( UnicodeString  AValue );
    bool __fastcall ChangeDefStyle( TCheckBox* Sender, UnicodeString  DefStyleAttrName );
    void __fastcall ParametersEditGetSpecMainName( TObject *Sender, UnicodeString &SpecMainName );

    UnicodeString __fastcall PeriodIdxToId(int AIdx);
    int __fastcall PeriodIdToIdx(UnicodeString AIdx);


public:		// User declarations
    __fastcall TfmParametersEdit(TComponent* Owner, TTagNode* APassport, TTagNode* ACondRules, TTagNode* ADocRules, TTagNode* ASpec );
    __fastcall ~TfmParametersEdit();

    __property TTagNode *Passport  = { read = FPassport  };
    __property TTagNode *CondRules = { read = FCondRules };
    __property TTagNode *DocRules = { read = FDocRules };
    __property TUnicodeStringList DropedStyles = { read = FDropedStyles };
    //������ ��� ������� �������������� ������ ������������ ��������� �� ���������
    __property TIcon*          DocByDocIcon = { read = FDocByDocIcon, write = FDocByDocIcon };

    //�������
    __property TOnGetSpecOwnerEvent          OnGetSpecOwner  = { read = FOnGetSpecOwner,  write = FOnGetSpecOwner  };
    __property TOnEditCondRulesEvent         OnEditCondRules = { read = FOnEditCondRules, write = FOnEditCondRules };
    __property TOnCheckStyleExistenceEvent   OnCheckStyleExistence = { read = FOnCheckStyleExistence, write = FOnCheckStyleExistence };
    __property TOnGetSpecificatorsListEvent  OnGetSpecificatorsList = { read = FOnGetSpecsList, write = FOnGetSpecsList };
    __property TOnGetOrgsListEvent           OnGetOrgsList          = { read = FOnGetOrgsList,  write = FOnGetOrgsList  };
    __property TOnGetSpecNodeEvent           OnGetSpecNode          = { read = FOnGetSpecNode,  write = FOnGetSpecNode  };
    __property TOnGetDocElementsListEvent    OnGetDocElementsList   = { read = FOnGetDocElementsList,  write = FOnGetDocElementsList  };
    __property TOnGetDocElementStructEvent   OnGetThisDocElementStruct  = { read = FOnGetThisDocElementStruct, write = FOnGetThisDocElementStruct };
    __property TOnGetDocElementStructEvent   OnGetRefDocElementStruct   = { read = FOnGetRefDocElementStruct,  write = FOnGetRefDocElementStruct  };
    __property TOnGetDocElementViewTextEvent OnGetDocElementViewText    = { read = FOnGetDocElementViewText,   write = FOnGetDocElementViewText   };
    __property TOnGetDocElementStructEvent   OnGetDocElementStruct     = { read = FOnGetDocElementStruct,     write = FOnGetDocElementStruct };
};

//---------------------------------------------------------------------------

} //end of namespace ParametersEdit
using namespace ParametersEdit;

#endif
