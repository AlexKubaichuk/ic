/*
  ����        - AddTable.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� �������� �����
                ��� ���������� ������� ( ��� "table" )
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 10.10.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  31.10.2006
    [*] ��������������� ������ "������ �������� ��" �������� ����� �����

  16.03.2006
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  27.01.2006
    [+] ��������� �������� TICSDocSpec::ReqEdBkColor
    [*] ��� ���� ����� �������� ��� "������������"
    [*] �������� ������������ ��������� ����������

  10.10.2004
          �������������� �������

*/
//---------------------------------------------------------------------------

#ifndef AddTableH
#define AddTableH

//---------------------------------------------------------------------------

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxSpinEdit.hpp"
#include "cxTextEdit.hpp"

//---------------------------------------------------------------------------
//#include "ICSDoc.h" //��������� �������� � ���� ��������
#include "XMLContainer.h"
//---------------------------------------------------------------------------

namespace AddTable
{

//����� ��� ���������� ������� ( ��� "table" )

class PACKAGE TfmAddTable : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *gbClient;
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
        TLabel *ListSrcLab;
        TLabel *RowCountLab;
        TBevel *Bevel1;
        TLabel *ColCountLab;
  TcxSpinEdit *edColCount;
  TcxSpinEdit *edRowCount;
  TcxComboBox *cbISs;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bOkClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);


protected:
        void __fastcall SetISList( TStrings* AValue );
        TStrings* __fastcall GetISList();
        TTagNode* __fastcall GetMainIS();
        int __fastcall GetColCount();
        int __fastcall GetRowCount();
        TColor __fastcall GetReqEdBkColor() const;
        

public:		// User declarations
        __fastcall TfmAddTable(TComponent* Owner);

        //������ �������������� ��������� ������������, ��� ������� ��������
        //���������� ������� ( ��� "table" )
        __property TStrings* ISList = { read = GetISList, write = SetISList };

        //�������� �������������� �������� ������������ ( ��� �� �������� �������
        // ( ��� "table" ) )
        __property TTagNode* MainIS = { read = GetMainIS };

        //����������� ���������� �������� ��� ����������� ������� ( ��� "table" )
        __property int ColCount = { read = GetColCount };

        //����������� ���������� ����� ��� ����������� ������� ( ��� "table" )
        __property int RowCount = { read = GetRowCount };
};

//---------------------------------------------------------------------------

} //end of namespace AddTable
using namespace AddTable;

#endif
