object fmAddList: TfmAddList
  Left = 602
  Top = 263
  BorderStyle = bsDialog
  BorderWidth = 8
  Caption = #1053#1086#1074#1099#1081' '#1089#1087#1080#1089#1086#1082
  ClientHeight = 541
  ClientWidth = 538
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gbClient: TGroupBox
    Left = 0
    Top = 0
    Width = 538
    Height = 508
    Align = alClient
    TabOrder = 0
    object lbISListTitle: TLabel
      Left = 8
      Top = 20
      Width = 111
      Height = 13
      Caption = #1044#1072#1085#1085#1099#1077' '#1074#1099#1073#1080#1088#1072#1090#1100' '#1080#1079':'
    end
    object lbISTreeTitle: TLabel
      Left = 8
      Top = 48
      Width = 49
      Height = 13
      Caption = #1054#1073#1098#1077#1082#1090#1099':'
    end
    object lbColsTitle: TLabel
      Left = 308
      Top = 45
      Width = 47
      Height = 13
      Caption = #1057#1090#1086#1083#1073#1094#1099':'
    end
    object lbMainIS: TLabel
      Left = 280
      Top = 20
      Width = 41
      Height = 13
      Caption = 'lbMainIS'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object tvISs: TTreeView
      Left = 8
      Top = 64
      Width = 228
      Height = 432
      HideSelection = False
      Indent = 19
      MultiSelect = True
      MultiSelectStyle = [msControlSelect, msShiftSelect]
      ReadOnly = True
      TabOrder = 0
      OnChange = tvISsChange
      OnCustomDrawItem = tvISsCustomDrawItem
      OnDblClick = tvISsDblClick
    end
    object bAddCols: TButton
      Left = 242
      Top = 97
      Width = 60
      Height = 25
      Action = actAddCols
      TabOrder = 2
    end
    object bDelete: TButton
      Left = 242
      Top = 161
      Width = 60
      Height = 25
      Action = actDelete
      TabOrder = 4
    end
    object tvCols: TTreeView
      Left = 308
      Top = 64
      Width = 227
      Height = 432
      HideSelection = False
      Indent = 19
      TabOrder = 1
      OnChange = tvColsChange
      OnDblClick = tvColsDblClick
      OnEdited = tvColsEdited
      OnEditing = tvColsEditing
      OnKeyDown = tvColsKeyDown
    end
    object bMoveUp: TButton
      Left = 242
      Top = 241
      Width = 60
      Height = 25
      Action = actUp
      TabOrder = 6
    end
    object bMoveDown: TButton
      Left = 242
      Top = 272
      Width = 60
      Height = 25
      Action = actDown
      TabOrder = 7
    end
    object bAddGCols: TButton
      Left = 242
      Top = 129
      Width = 60
      Height = 25
      Action = actAddGCols
      TabOrder = 3
    end
    object bClear: TButton
      Left = 242
      Top = 192
      Width = 60
      Height = 25
      Action = actClear
      TabOrder = 5
    end
    object cbISs: TcxComboBox
      Left = 125
      Top = 17
      Properties.DropDownListStyle = lsEditFixedList
      Properties.OnChange = cbISsPropertiesChange
      TabOrder = 8
      Width = 149
    end
  end
  object pBottom: TPanel
    Left = 0
    Top = 508
    Width = 538
    Height = 33
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 1
    object pCtrls: TPanel
      Left = 297
      Top = 0
      Width = 241
      Height = 33
      Align = alRight
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 0
      object bOk: TButton
        Left = 0
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Ok'
        Default = True
        TabOrder = 0
        OnClick = bOkClick
      end
      object bCancel: TButton
        Left = 83
        Top = 8
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
        OnClick = bCancelClick
      end
      object bHelp: TButton
        Left = 166
        Top = 8
        Width = 75
        Height = 25
        Caption = '&'#1055#1086#1084#1086#1097#1100
        TabOrder = 2
        OnClick = bHelpClick
      end
    end
  end
  object ActionList1: TActionList
    Left = 349
    Top = 8
    object actAddCols: TAction
      Caption = '>'
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1089#1090#1086#1083#1073#1094#1099
      ShortCut = 45
      OnExecute = actAddColsExecute
    end
    object actAddGCols: TAction
      Caption = '+>'
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' '#1089#1090#1086#1083#1073#1094#1086#1074
      ShortCut = 16429
      OnExecute = actAddGColsExecute
    end
    object actDelete: TAction
      Caption = 'X'
      Hint = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 46
      OnExecute = actDeleteExecute
    end
    object actClear: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
      Hint = #1054#1095#1080#1089#1090#1080#1090#1100
      ShortCut = 16430
      OnExecute = actClearExecute
    end
    object actUp: TAction
      Caption = #1042#1074#1077#1088#1093
      Hint = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100' '#1074#1074#1077#1088#1093' '
      ShortCut = 16422
      OnExecute = actUpExecute
    end
    object actDown: TAction
      Caption = #1042#1085#1080#1079
      Hint = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100' '#1074#1085#1080#1079
      ShortCut = 16424
      OnExecute = actDownExecute
    end
  end
end
