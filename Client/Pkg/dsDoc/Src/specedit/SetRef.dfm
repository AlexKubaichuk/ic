object fmSetRef: TfmSetRef
  Left = 421
  Top = 224
  BorderStyle = bsDialog
  BorderWidth = 8
  Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1089#1089#1099#1083#1082#1091
  ClientHeight = 355
  ClientWidth = 382
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pBottom: TPanel
    Left = 0
    Top = 322
    Width = 382
    Height = 33
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 1
    object pCtrls: TPanel
      Left = 58
      Top = 0
      Width = 324
      Height = 33
      Align = alRight
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 0
      object bOk: TButton
        Left = 0
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Ok'
        Default = True
        TabOrder = 0
        OnClick = bOkClick
      end
      object bCancel: TButton
        Left = 166
        Top = 8
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 2
        OnClick = bCancelClick
      end
      object bHelp: TButton
        Left = 249
        Top = 8
        Width = 75
        Height = 25
        Caption = '&'#1055#1086#1084#1086#1097#1100
        TabOrder = 3
        OnClick = bHelpClick
      end
      object bClear: TButton
        Left = 83
        Top = 8
        Width = 75
        Height = 25
        Caption = #1057#1073#1088#1086#1089#1080#1090#1100
        TabOrder = 1
        OnClick = bClearClick
      end
    end
  end
  object tv: TTreeView
    Left = 0
    Top = 0
    Width = 382
    Height = 322
    Align = alClient
    AutoExpand = True
    Indent = 19
    ReadOnly = True
    TabOrder = 0
    OnChange = tvChange
    OnDblClick = tvDblClick
  end
end
