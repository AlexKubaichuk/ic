/*
  ����        - Count.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� �������� �����
                ��� �������� ��������, ���������� ����������� �������
                ���������� ����������
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 11.10.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  16.03.2006
    [*] ��������� uid �������������� ���������� TTagNode
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  27.01.2006
    [+] ��������� �������� TICSDocSpec::ReqEdBkColor
    [*] ���� ����� "������� ���������� ���" �������� ��� "������������"
    [*] �������� ������������ ��������� ����������

  11.10.2004
    �������������� �������

*/
//---------------------------------------------------------------------------

#ifndef CountH
#define CountH

//---------------------------------------------------------------------------

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtonEdit.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"

//---------------------------------------------------------------------------
//#include "ICSDoc.h"  //��������� �������� � ���� ��������
#include "XMLContainer.h"
//---------------------------------------------------------------------------

namespace Count
{

//***************************************************************************
//********************** ���� ***********************************************
//***************************************************************************

typedef bool __fastcall (__closure *TOnEditCondRulesEvent)(
  TObject* Sender,
  UnicodeString ATitle,
  TTagNode *AParentNode,
  TTagNode *ACondRulesNode,
  UnicodeString AMainISUID
);

//---------------------------------------------------------------------------

//����� ��� �������� ��������, ���������� ����������� ������� ���������� ����������

class PACKAGE TfmCount : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *gbClient;
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
        TLabel *CountLab;
        TLabel *FilterRulesLab;
        TButton *bClear;
  TcxComboBox *cbISs;
  TcxButtonEdit *edCondRules;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bOkClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall bClearClick(TObject *Sender);
 void __fastcall cbISsPropertiesChange(TObject *Sender);
 void __fastcall edCondRulesPropertiesButtonClick(TObject *Sender, int AButtonIndex);





private:	// User declarations
        TTagNode* FCountNode;                   //TagNode "count"
        TStrings* FISList;                      //������ ��������� ������������
        TOnEditCondRulesEvent FOnEditCondRules; //�������, ����������� ��� �������������
                                                //������������� "condrules" ��� "count"


protected:
        UnicodeString __fastcall GetISExtRef( int nItemIndex );
        TColor __fastcall GetReqEdBkColor() const;
        void __fastcall UpdateCondRulesEditorText();


public:		// User declarations
        __fastcall TfmCount(TComponent* Owner, TTagNode* ACountNode, TStrings* AISList );
        __fastcall ~TfmCount();

        __property TTagNode* CountNode = { read = FCountNode };
        __property TOnEditCondRulesEvent OnEditCondRules = { read = FOnEditCondRules, write = FOnEditCondRules };
};

//---------------------------------------------------------------------------

} //end of namespace Count
using namespace Count;

#endif
