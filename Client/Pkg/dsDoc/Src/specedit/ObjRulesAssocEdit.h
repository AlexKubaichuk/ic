/*
  ����        - ObjRulesAssocEdit.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� �������� �����
                ��� ��������� ������������ ����� ����������
                ������� ������������� � ���������� �������������� �������
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 19.11.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  31.05.2006
    [+] ����������� �������� ������ �������

  16.03.2006
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  27.01.2006
    [*] �������� ������������ ��������� ����������

  19.11.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef ObjRulesAssocEditH
#define ObjRulesAssocEditH

//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <ActnList.hpp>
#include <Menus.hpp>
#include <System.Actions.hpp>

//#include "ICSDoc.h"  //��������� �������� � ���� ��������
#include "XMLContainer.h"


namespace ObjRulesAssocEdit
{

//***************************************************************************
//********************** ���� ***********************************************
//***************************************************************************

/*
 * ��� ����������� ������� OnGetDocElementViewText - ���������, ����� ���������
 * �������� ������������ ����� ��� �������� �������������. ������������ �����������
 * TICSDocSpec � ������� ��������� ������ ������������ ��������� �� ���������
 * ��� ��������� ������������
 *
 *
 * ���������:
 *   [in]  DocElement - ������� �������������
 *   [out] ViewText   - ������������ �����
 *
 */
typedef void __fastcall (__closure *TOnGetDocElementViewTextEvent)( TObject *Sender, const TTagNode *DocElement, UnicodeString &ViewText );

/*
 * ��� ����������� ������� OnGetDocElementStruct - ���������, ����� ���������
 * �������� ��������� �������� �������������. ������������ �����������
 * TICSDocSpec � ������� ��������� ������ ������������ ��������� �� ���������
 * ��� ��������� ������������
 *
 *
 * ���������:
 *   [in]  DocElement - ������� �������������
 *   [out] ViewText   - ���� TreeView, � ������� ���������� ��������� ����������� ���������
 *
 */
typedef void __fastcall (__closure *TOnGetDocElementStructEvent)( TObject *Sender, const TTagNode *DocElement, TTreeNodes *Struct );

/*
 * ��� ����������� ������� OnCheckAssociationExistence - ���������, ����� ���������
 * ���������� ������� ������������(�) ��� �������� �������������, ��������������(��) ��
 * ������ ������ ��� �� ������ �����, �� ��� ������� �������. ������������ �����������
 * TICSDocSpec � ������� ��������� ������ ������������ ��������� �� ���������
 * ��� ��������� ������������
 *
 *
 * ���������:
 *   [in]  ndObjRules - ������� ����������
 *   [in]  ThisRef    - ������ �� ������� �������������, ��� �������� ���������������
 *                      ������������
 *   [out] Msg        - ��������� ��� ������ ������������
 *
 * ������������ ��������:
 *   true  - ���� ������������(�)
 *   false - ��� ������������
 *
 */
typedef bool __fastcall (__closure *TOnCheckAssociationExistenceEvent)( TObject *Sender, const TTagNode *ndObjRules, const UnicodeString ThisRef, UnicodeString &Msg );

//---------------------------------------------------------------------------

//����� ��� ������������ ������ ����������

class PACKAGE TfmObjRulesAssocEdit : public TForm
{
__published:	// IDE-managed Components
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
        TActionList *ActionList1;
        TAction *actSetAssoc;
        TAction *actOk;
        TAction *actClearAssoc;
        TPanel *ClientPn;
        TPanel *pTitle;
        TPanel *ThisPn;
        TTreeView *tvThisStruct;
        TPanel *RefPn;
        TPanel *Panel2;
        TTreeView *tvRefStruct;
        TPanel *EditCtrlsPn;
        TButton *bSetAssoc;
        TButton *bClearAssoc;
        TPanel *Panel4;
        TLabel *lbThisStructTitle;
        TLabel *lbThisObjTitle;
        TPanel *Panel5;
        TCheckBox *chbThisStructAutoExpand;
        TPanel *Panel6;
        TLabel *lbRefStructTitle;
        TLabel *lbRefObjTitle;
        TPanel *Panel7;
        TCheckBox *chbRefStructAutoExpand;
  TPopupMenu *ThisStructPM;
  TMenuItem *N1;
  TMenuItem *N2;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall tvThisStructContextPopup(TObject *Sender,
          TPoint &MousePos, bool &Handled);
        void __fastcall actSetAssocExecute(TObject *Sender);
        void __fastcall actOkExecute(TObject *Sender);
        void __fastcall actClearAssocExecute(TObject *Sender);
        void __fastcall chbThisStructAutoExpandClick(TObject *Sender);
        void __fastcall chbRefStructAutoExpandClick(TObject *Sender);
        void __fastcall tvThisStructKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall tvThisStructChange(TObject *Sender,
          TTreeNode *Node);
        void __fastcall tvRefStructChange(TObject *Sender,
          TTreeNode *Node);
        void __fastcall tvRefStructExpanding(TObject *Sender,
          TTreeNode *Node, bool &AllowExpansion);
        void __fastcall EditCtrlsPnResize(TObject *Sender);
        void __fastcall ClientPnResize(TObject *Sender);


private:	// User declarations
        /*����*/
        bool              FLoadingStructures;   //������� ���������� ��������� ��������
                                                //�������� ������� � �������������� �������
        TTagNode*         FObjRulesNode;        //������� ����������
        TTagNode*         FThisSpec;            //������������� ������������
        TTagNode*         FRefSpec;             //������������ ������������� ����������
        TTagNode*         FThisObj;             //������
        TTagNode*         FRefObj;              //������������� ������

        /*������� (�������� ��. � ������ public)*/
        TOnGetDocElementViewTextEvent     FOnGetDocElementViewText;
        TOnGetDocElementStructEvent       FOnGetThisDocElementStruct;
        TOnGetDocElementStructEvent       FOnGetRefDocElementStruct;
        TOnCheckAssociationExistenceEvent FOnCheckAssociationExistence;


protected:
        void __fastcall UpdateActionsEnabled();
        void __fastcall UpdateThisStructTreeView( const TAction *AAction, const bool bNeedSelectedNodeUpdate );
        bool __fastcall AreInputsCorrect();


public:		// User declarations
        __fastcall TfmObjRulesAssocEdit( TComponent* Owner, TTagNode *AObjRulesNode, TTagNode *AThisSpec, TTagNode* ARefSpec );
        virtual __fastcall ~TfmObjRulesAssocEdit();

        /*��������*/
        //������� ����������
        __property TTagNode* ObjRules = { read = FObjRulesNode};

        /*�������*/
        //��������� ��� ������������� �������� ������������ ����� ��� �������� �������������
        __property TOnGetDocElementViewTextEvent OnGetDocElementViewText   = { read = FOnGetDocElementViewText,   write = FOnGetDocElementViewText   };
        //��������� ��� ������������� �������� ��������� ��������� �������������� �������������
        __property TOnGetDocElementStructEvent   OnGetThisDocElementStruct = { read = FOnGetThisDocElementStruct, write = FOnGetThisDocElementStruct };
        //��������� ��� ������������� �������� ��������� ��������� ������������� ������������� ����������
        __property TOnGetDocElementStructEvent   OnGetRefDocElementStruct  = { read = FOnGetRefDocElementStruct,  write = FOnGetRefDocElementStruct  };
        //��������� ��� ������������� ���������� ������� ������������(�) ��� �������� �������������,
        //��������������(��) �� ������ ������ ��� �� ������ �����, �� ��� ������� �������
        __property TOnCheckAssociationExistenceEvent OnCheckAssociationExistence = { read = FOnCheckAssociationExistence, write = FOnCheckAssociationExistence };
};

//---------------------------------------------------------------------------

} //end of namespace ObjRulesAssocEdit
using namespace ObjRulesAssocEdit;

#endif
