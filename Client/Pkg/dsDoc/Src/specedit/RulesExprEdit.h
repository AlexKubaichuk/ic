/*
  ����        - RulesExprEdit.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� �������� �����
                ��� �������������� ������� ���������� ���
                ������ ��� ������� ������� ( ��� "table" ) �
                ��� �������������� ������ �������� ������� �����
                ������ � �������
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 11.10.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  16.03.2006
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  27.01.2006
    [*] �������� ������������ ��������� ����������

  21.06.2005
    1. ��������� ��������� ������� ����� � ������� #<�����_������>:<�����_�������>

  12.11.2004
    1. ������ ������������ �� CalcRulesEdit � RulesExprEdit
    2. ��������� ��������� �������������� ������ �������� ������� �����
       ������ � �������

  11.10.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef RulesExprEditH
#define RulesExprEditH

//---------------------------------------------------------------------------

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"

//---------------------------------------------------------------------------
//#include "ICSDoc.h"  //��������� �������� � ���� ��������
#include "ICSDocParsers.h"
//---------------------------------------------------------------------------

namespace RulesExprEdit
{

//***************************************************************************
//********************** ���� ***********************************************
//***************************************************************************

//��� ������
enum TRulesType
{
  rtUnknown,    //����������, ��� � ��� ����� �������
  rtCalc,       //������� ���������� ������/������� ������� ( xml-��� "calcrules" ������������� )
  rtHand        //������� �������� ������� ����� ������ � ������� ( xml-��� "handrules" ������������� )
};

//��� "���������" ������
enum TRulesOwnerType
{
  otTableRow,   //������� ��� ������ �������
  otTableCol    //������� ��� ������� �������
};

//---------------------------------------------------------------------------

//����� ��� �������������� ������� ���������� ��� ������ ��� ������� �������

class PACKAGE TfmRulesExprEdit : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *gbClient;
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
        TLabel *lbTitle;
  TcxMaskEdit *edExpr;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bOkClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);


private:	// User declarations
        UnicodeString                  FCaptionDopInfo;   //����������� ��������� ��� ����� lbTitle
        ICSDocParsers::TExprParser* FExprParser;       //������ ��� ������
        TRulesType                  FRulesType;        //��� ������
        TRulesOwnerType             FRulesOwnerType;   //��� "���������" ������
        int                         FRowNumConstraint; //����. ���������� �������� ������ ������
        int                         FColNumConstraint; //����. ���������� �������� ������ �������

        void __fastcall ThrowIfParserMissing( UnicodeString MethodName ) const;


protected:
        UnicodeString __fastcall GetExpression() const;
        void       __fastcall SetExpression( UnicodeString  AValue );
        UnicodeString __fastcall GetPolExp    () const;
        TLexMap    __fastcall GetNameMap   () const;
        void       __fastcall SetRulesType( TRulesType AValue );
        bool __fastcall AreInputsCorrect();


public:		// User declarations
        __fastcall TfmRulesExprEdit(TComponent* Owner, UnicodeString  ACaptionDopInfo = "");
        virtual __fastcall ~TfmRulesExprEdit();

        //������������� �������
        __property UnicodeString Expression = { read = GetExpression, write = SetExpression };

        //��������� ������� �������
        __property UnicodeString PolExp     = { read = GetPolExp  };

        //������ ����, ����������� � ��������
        __property TLexMap    NameMap    = { read = GetNameMap };

        //��� ������
        __property TRulesType      RulesType = { read = FRulesType, write = SetRulesType };

        //��� "���������" ������
        __property TRulesOwnerType RulesOwnerType = { read = FRulesOwnerType, write = FRulesOwnerType };

        //����. ���������� �������� ������ ������
        __property int            RowNumConstraint = { read = FRowNumConstraint, write = FRowNumConstraint };
        //����. ���������� �������� ������ �������
        __property int            ColNumConstraint = { read = FColNumConstraint, write = FColNumConstraint };
};

//---------------------------------------------------------------------------

} //end of namespace RulesExprEdit
using namespace RulesExprEdit;

#endif
