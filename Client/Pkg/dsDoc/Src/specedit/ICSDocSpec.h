/*
  ����        - ICSDocSpec.h
  ������      - ��������������� (ICS)
  ��������    - �������� �������������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 15.09.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  v1.1.5 - 15.03.2007
    [!] � ����������� �������� ������, ��� �������� ������� ������� ������ ��� ������� ����������

  v1.1.4 - 26.12.2006 - 11.01.2007
    [+] ��������� specificator.dtd ������ 2.1.5
    [+] �������� �� ������ ������� ������������� � �������
        TICSDocSpec::Set � TICSDocSpec::Load
        � ����������� �� ������ 2.1.5 ��� �������������
    [+] #include "ICSDocSpecUtils.h"
    [*] �������� �� ��������� ��� ��������� APeriodDef ������ TICSDocSpec::New
        �������� � "month" �� "1"
    [*] �����������

  v1.1.3 - 31.10.2006
    [!] ��� ��������������� ��������� � ������ pmSystemDlg
    [!] ��� ����������� �������� �������� "������� ������" ��� ��������
        ���������� ����������� ��������� ��������
    [!] ��� �������������� ��������� ��������� � TreeView �� F2: ��� �����
        ������� �������� ����� ��������� �� ������ ��� ����� � �������� �������
        �� ������������ ������, � ��� �������� ���������� ����������� ���������
        �������� - ����� "(������� ����������)"
    [+] public-����� bool IsListColSingle1Main(const TTagNode *ndListEl)
    [+] public-����� bool IsListColSingle1Count(const TTagNode *ndListEl)
    [+] ����������� � TreeView ��������� ����� "(������� ����������)"
        ��� �������� ���������� ����������� ��������� ��������
    [*] ��������� �� ��������� ��� �������� specificator/passport/@save_bd
        ��������� 0 (�� ��������� � ����� ����������)
    [*] �����������

  v1.1.2 - 31.05.2006
    [*] ��� �������� ��������� ��������� ��� ��������� ������������� ��
        ��������� �������� 

  v1.1.1 - 21.03.2006
    [!] ��� ��������� ��������� �������� ���������, ���� ���������� �������
        ������ �������� <= ������� ���������� �������� ����������� ��������
        ���������

  v1.1.0 - 16.03.2006
    [+] ��������� TICSDocBaseEdit v1.1.0
    [*] ��������� uid �������������� ���������� TTagNode
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  v1.0.18 - 27.01.2006
    [+] __published-�������� ReqEdBkColor

  v1.0.17 - 20.10.2005 - 21.10.2005
    1. ��������� ���� � ������� docrules ��� �������� ������� �
       ������/������� �������

  v1.0.16 - 21.06.2005 - 22.06.2005
    1. ��������� ��������� ������� ����� ������� � �������� ����������
       � � �������� �������� ������� �����
    2. ��������� ��������� specificator.dtd ������ 2.1.4:
            2.1. �������� �� ��������� ��� �������� showzval ����� list � table
                 �������� �� 1

  v1.0.15 - 08.06.2005
    1. ��������� ������ ���
    2. ��������� ���� BCB Update Pack 4 � ����������, ��� ������
       � TDKPropertyEditor

  v1.0.14 - 23.05.2005 - 02.06.2005
    1. ��������� ��������� � ���� "������", ��������������� ��������� "����������� �����" � "����������� ����"
    2. ��� �������� style �������� ������� ��������� �������� "�����" � �� "����� �� ���������"
    3. ����� ����, ��������� � ������������ ���������� shortcut'�� ��� �������������� ������ ���� EditTV
    4. ����� ����, ��������� � ������������� �������� ������������������� ������ ������ ��� ��������� ��������� ���������
    5. ��������� ��������� specificator.dtd ������ 2.1.3:
            5.1. ��������� ��������� �������� width ��� list, listcol, table, tcol
    6. �������� ����������� public-������:
         IsListColLinkISGroup
         IsListColLinkISGroup
         GetListColSingle2Index
         GetSizeAttrVal
         InsertListColSingle2SizeAttr
         DeleteListColSingle2SizeAttr
         SwapListColSingle2SizeAttrVals

  v1.0.13 - 04.04.2005
    1. ��������� ��������� specificator.dtd ������ 2.1.2:
            1.1. ��������� ��������� �������� rownumbers ��� ���� list
    2. ����� ������������ inline ��� ������� TICSDocSpec::IsListColGroup,
       TICSDocSpec::IsListColSingle1 � TICSDocSpec::IsListColSingle, �.�.
       ���� �������� � ��������� ��� ������ ������� �� ���������

  v1.0.12 - 21.03.2005
    1. ��������� ShortCut'�

  v1.0.11 - 05.03.2005
    1. ��������� ����������� ShortCut'�� � hint'�� ��� ������ toolbar'��

  v1.0.10 - 04.03.2005
    1. ��������� ���� ��������� ShortCut'��

  v1.0.9  - 03.03.2005
    1. ��������� ��������� ��������� ����� � �������� �������� �������
    2. ��������� ��������� ShortCut'��

  v1.0.8  - 09.02.2005
    1. ��������� ��������� specificator.dtd ������ 2.1.1:
            1.1. ������� ��������� periodfields ��� ��������� ��� ���� passport
            1.2. ��������� ���� periodicity ��� ��������� ��� ���� passport
            1.3. ��������� �������� refperiodparams ��� ����� list, link_is � table
    2. ����� GetXMLNodeViewText ��������� � ������ public
    3. ��������� ���������� ���������
    4. ����������������� ��������� �������� �������� �������� ���������

  v1.0.7  - 01.02.2005
    1. ��������� ��������� specificator.dtd ������ 2.0.2:
            1.1 ��������� condrules ��� rows � columns
            1.2 ��������� periodfields
            1.3 ��������� �������� orientation ���� passport
    2. ����������� �������� � ������ UpdateImpDefAttrs (�����-�� � ���� if
       ��������� � "name" �������������� 2 ����, ������ ������)
    3. ����������� �������� � ������ UpdateActions (������ ������ ������ �������
       ���� rows � columns �������)
    4. ������ ��� �������������� ������ ������ ����� �������� ������ �������

  v1.0.6  - 02.12.2004
    1. ������� OnGetSpecPath ���������� � OnGetSpecNode

  v1.0.5  - 25.11.2004
    1. �������� public-����� Check()
    2. ��������� __published-�������� DocByDocIcon

  v1.0.4  - 24.11.2004
    1. ��������� __published-�������� Title
    2. ��������� �����: ��� ������� ������, ����������������
       link_is( condrules?, is_attr+ ) � ������� ������� ��������
       "������� ������" ����������� ������ ��� ������� condrules
    3. ��������� �����: ��� ����������� ����� � ������ ��������
       ��������� ��� �������� ����� ������������� ���� �� �����������
       ������ �� TagNode'� - � ����� ������� ������� ���� ������
    4. ���������� ����� DropTreeNodesAfterLoadXML - ������ � TagNode'��
       �������� ������ �� ��������� TreeNode'�

  v1.0.3  - 23.11.2004
    1. ��������� ������ docrules ��� �������� ����, �� �������
       � ���� docrules ���� ������

  v1.0.2  - 22.11.2004
    1. ��������� �����
    2. ��������� __published-��������:
            PropsPanel
            SpecPanel
            Splitter

  v1.0.1  - 15.11.2004
    1. ����� ������ �������������� ������ ������������ ��������� �� ���������

  v1.0    - 15.09.2004
    �������������� �������
*/
//---------------------------------------------------------------------------
/*
  ���� �� �������:
        1. ��� ��������� ������������ ��� �������� ������� �������������
           � ������� ��������� ������ ������������ ��������� �� ��������� ����������
           ��������� ������� ������������� ������������ ��� ����� �������� �� ������
           ������ ��� �� ������ ����� �� ��� ������� �������
        2. ���� ������� ���������� �������� ���������� ��������, ��� ��� ����������
           �������� ����������� ������ �� �����������
*/
//---------------------------------------------------------------------------

#ifndef ICSDocSpecH
#define ICSDocSpecH

//---------------------------------------------------------------------------

#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <ActnList.hpp>
#include <Menus.hpp>
#include <set>
//#include "TB2Item.hpp"
#include "XMLContainer.h"
#include "DKPropertyEditor.h"

#include "ICSDocSpecUtils.h"
#include "ICSDocBaseEdit.h"
#include "ICSDocViewer.h"
#include "ParametersEdit.h"
#include "FilterEditEx.h"

namespace ICSDocSpec
{
using namespace std;

//###########################################################################
//##                                                                       ##
//##                                 Globals                               ##
//##                                                                       ##
//###########################################################################

//��������� �������� �������� ���������

enum TCheckDocSpecResult {
  frOk = 0,     //Ok
  frEmpty = 1,  //� xml-���� "doccontent" ��� �������� ���������
  frNone = 2    //�������� ��������� �� �������
};

//---------------------------------------------------------------------------

//��������� ���������� ������ TICSDocSpec::New

enum TICSDocSpecNewResult
{
  nrCanNotDelete,               //�������� ������ ������������� ���������� �����
                                //������������� ������� �������������� �������������
  nrParametersApplied,          //� ����������� ��� �������� ������ ������������� ������� "���������"
                                //������������ ����� "���������"
  nrParametersCanceled          //� ����������� ��� �������� ������ ������������� ������� "���������"
                                //������������ ����� "������", ��� ���� Enabled ���������� ��������������� �
                                //false
};

//---------------------------------------------------------------------------

// ��� �������� �������������
#define COUNT_VIEW_TEXT         csCountViewText.c_str()
#define EMPTY_ELLIPSISEDITOR    csEmptyEllipsiseEditor.c_str()
#define NOTEMPTY_ELLIPSISEDITOR csNotEmptyEllipsiseEditor.c_str()
#define EMPTY_INSTEXT           csEmptyInsText.c_str()

//---------------------------------------------------------------------------

extern const UnicodeString csCountViewText;
extern const UnicodeString csEmptyEllipsiseEditor;
extern const UnicodeString csNotEmptyEllipsiseEditor;
extern const UnicodeString csEmptyInsText;

//extern const UnicodeString InscriptionViewText;
//extern const UnicodeString ListViewText;
//extern const UnicodeString TableViewText;

//###########################################################################
//##                                                                       ##
//##                              TICSDocSpec                              ##
//##                                                                       ##
//###########################################################################

// �������� �������������

class PACKAGE TICSDocSpec : public TICSDocBaseEdit
{
    typedef TICSDocBaseEdit inherited;
private:
    UnicodeString FBaseMainISUID;
    typedef map<UnicodeString,TTagNode*> TUIDsMap;
    typedef set<UnicodeString> TUIDsSet;
    typedef struct tagVALIDATE_ERROR_INFO
    {
      TTagNode* TagNode;
      UnicodeString AttrName;
      UnicodeString AttrValue;
      int Row;
    }VALIDATE_ERROR_INFO, *PVALIDATE_ERROR_INFO;
    typedef map<TTagNode*,int> TTableRowColNumMap;
    typedef map<TTagNode*,TTableRowColNumMap> TTableRowColNumMaps;
    enum TTableElement      //������� �������� �������
    {
      teRow,        //������
      teCol,        //�������
      teBoth        //������ � �������
    };

    TdxBar*            FMenuBar;
    TPanel*            FPropertyPanel;
    TStaticText*       FStaticText;
    TDKPropertyEditor* FPropertyEditor;

    TAction*     FParametersAction;
    TAction*     FPreViewAction;
    TAction*     FCutAction;
    TAction*     FCopyAction;
    TAction*     FPasteAction;
    TAction*     FDeleteAction;
    TAction*     FAddInscAction;
    TAction*     FAddInsTextAction;
    TAction*     FAddListAction;
    TAction*     FAddLColAction;
    TAction*     FAddTableAction;
    TAction*     FAddTColAction;
    TAction*     FAddTRowAction;
    TAction*     FMoveUpAction;
    TAction*     FMoveDownAction;
//******************************************************************************
    TdxBarSubItem*         FCommonItem;
    TdxBarLargeButton*          FParametersItem;//!!!
    TdxBarLargeButton*          FPreViewItem;

    TdxBarSubItem*         FEditItem;
    TdxBarLargeButton*          FCutItem;
    TdxBarLargeButton*          FCopyItem;
    TdxBarLargeButton*          FPasteItem;
    TdxBarLargeButton*          FDeleteItem;
    TdxBarLargeButton*          FMoveUpItem;
    TdxBarLargeButton*          FMoveDownItem;

    TdxBarSubItem*         FObjectItem;
    TdxBarLargeButton*          FAddInscItem;
    TdxBarPopupMenu*       FAddInscMenuItem;
    TdxBarLargeButton*          FAddInsTextItem;

    TdxBarLargeButton*          FAddListItem;
    TdxBarPopupMenu*       FAddListMenuItem;
    TdxBarLargeButton*          FAddLColItem;

    TdxBarLargeButton*          FAddTableItem;
    TdxBarPopupMenu*       FAddTableMenuItem;
    TdxBarLargeButton*          FAddTColItem;
    TdxBarLargeButton*          FAddTRowItem;
//*************************************************************
  TMenuItem *FAddInscriptionMItem;
  TMenuItem *FAddInsTextMItem;
  TMenuItem *FSep1MItem;
  TMenuItem *FAddListMItem;
  TMenuItem *FAddLColMItem;
  TMenuItem *FSep2MItem;
  TMenuItem *FAddTableMItem;
  TMenuItem *FAddTColMItem;
  TMenuItem *FAddTRowMItem;
  TMenuItem *FSep3MItem;
  TMenuItem *FCutMItem;
  TMenuItem *FCopyMItem;
  TMenuItem *FPasteMItem;
  TMenuItem *FDeleteMItem;
  TMenuItem *FSep4MItem;
  TMenuItem *FMoveUpMItem;
  TMenuItem *FMoveDownMItem;
//*************************************************************
    TICSDocViewer*         FViewer;
    bool                   FPreviewOnFly;
    TICSDocViewerDocFormat FPreviewDocFormat;
    bool                   FUseCSSOnHTMLPreview;
    bool                   FUseWordCSSOnHTMLPreview;
    TICSDocViewerDlgKind   FPreviewDlgKind;
    TIcon*                 FDefDocByDocIcon;
    TIcon*                 FDocByDocIcon;           //������ ��� ������� ��������������
                                                    //������ ������������ ���������
                                                    //�� ���������
    bool                   FEnumTableRowsCols;      //������� ��������� ����� � �������� �������

    TICSDocControlProps* FPropsPanelProps;          //�������� ������� FPropertyPanel
    TICSDocControlProps* FSpecPanelProps;           //�������� ������� FEditTV

    bool                FValidateErrorOnTVChanging;
    TTableRowColNumMaps FTableRowNumMaps;           //������� ������� ����� ������ �������������
    TTableRowColNumMaps FTableColNumMaps;           //������� ������� �������� ������ �������������

    TOnFltCustomLoadEvent        FOnLoadFilter;
    TOnGetSpecOwnerEvent         FOnGetSpecOwner;
    TOnGetSpecificatorsListEvent FOnGetSpecsList;
    TOnGetOrgsListEvent          FOnGetOrgsList;
    TOnGetSpecNodeEvent          FOnGetSpecNode;

    VALIDATE_ERROR_INFO __ValidateErrorInfo;
    TUIDsMap __Refers;
    TUIDsMap __RefObjects;

    void __fastcall CreateTableRowColNumMaps( TTagNode *ndTable, TTableElement ATE = teBoth );
    bool __fastcall FillRefObjects( TTagNode *ItTag, void *Src1 = NULL, void *Src2 = NULL, void *Src3 = NULL, void *Src4 = NULL );
    static bool __fastcall IsTableFirstEl( const TTagNode *ndTableEl, UnicodeString AElName );
    static bool __fastcall IsTableElSingle( const TTagNode *ndTableEl, UnicodeString AElName );
    bool __fastcall GetListColToDelete( TTagNode *ItTag, void *ListColToDelete = NULL, void *Src2 = NULL, void *Src3 = NULL, void *Src4 = NULL );
    bool __fastcall HasListColGroup(TTagNode *ItTag, void *Src1 = NULL, void *Src2 = NULL, void *Src3 = NULL, void *Src4 = NULL );
    bool __fastcall _IterateHasListColGroup(TTagNode *ItTag, void *Src1 = NULL, void *Src2 = NULL, void *Src3 = NULL, void *Src4 = NULL );
    TPPreviewDemoMessageEvent FPPDemoMsg;
    void __fastcall FSetPPDemoMsg(TPPreviewDemoMessageEvent APPDemoMsg);

protected:
    typedef struct tagINS_VIEW_TEXT
    {
      UnicodeString Value;
      UnicodeString Ref;
      UnicodeString Count;
    } INS_VIEW_TEXT, *PINS_VIEW_TEXT;

    virtual UnicodeString __fastcall DKGetVersion() const;

    void __fastcall SetPropsPanelProps ( const TICSDocControlProps *AValue );
    void __fastcall SetSpecPanelProps  ( const TICSDocControlProps *AValue );
    void __fastcall SetDocByDocIcon( const TIcon *AValue );
    virtual void __fastcall SetReqEdBkColor( TColor AValue );

    bool __fastcall EditCondRules(
      UnicodeString  ATitle,
      TTagNode *AParentNode,
      TTagNode *AInsNode,
      bool IsBefore,
      TTagNode **ACondRulesNode,
      UnicodeString  AMainISUID,
      bool NeedKonkr
    );
    bool __fastcall ClearDropedStyleRef( TTagNode *ItTag, void *ADropedStyles = NULL, void *Src2 = NULL, void *Src3 = NULL, void *Src4 = NULL );
    bool __fastcall ItDocSpecCheckStyleExistence( TTagNode *ItTag, UnicodeString &AStyleUID );
    bool __fastcall DocSpecCheckStyleExistence( TObject *Sender, UnicodeString AStyleUID );
    void __fastcall DocSpecGetDocElementsList( TObject *Sender, TStrings *DocElements );
    void __fastcall DocSpecGetThisDocElementStruct( TObject *Sender, const TTagNode *DocElement, TTreeNodes *Struct );
    void __fastcall DocSpecGetRefDocElementStruct( TObject *Sender, const TTagNode *DocElement, TTreeNodes *Struct );
    void __fastcall DocSpecGetDocElementStruct( TObject *Sender, const TTagNode *DocElement, TTreeNodes *Struct );
    void __fastcall DocSpecGetDocElementViewText( TObject *Sender, const TTagNode *DocElement, UnicodeString &ViewText );
    bool __fastcall EditParameters();
    void __fastcall FillStyleList( TStrings* S );
           UnicodeString __fastcall UpdateStyleProp    ( TTagNode* TagNode, UnicodeString  AAttrName, bool UpdatePropValue = false );
     void       __fastcall AppendStyleProp    ( TTagNode* TagNode, UnicodeString  AAttrName, UnicodeString  APropName );
           UnicodeString __fastcall UpdateBinProp      ( TTagNode* TagNode, UnicodeString  AAttrName, bool UpdatePropValue = false );
     void       __fastcall AppendBinProp      ( TTagNode* TagNode, UnicodeString  AAttrName, UnicodeString  APropName );
     void       __fastcall AppendUnionProp    ( TTagNode* TagNode, UnicodeString  AAttrName, UnicodeString  APropName );
           UnicodeString __fastcall UpdateTabPrtyProp  ( TTagNode* TagNode, UnicodeString  AAttrName, bool UpdatePropValue = false );
     void       __fastcall AppendTabPrtyProp  ( TTagNode* TagNode, UnicodeString  AAttrName, UnicodeString  APropName );
           UnicodeString __fastcall UpdateTextProp     ( TTagNode* TagNode, UnicodeString  AAttrName, bool UpdatePropValue = false );
     void       __fastcall AppendTextProp     ( TTagNode* TagNode, UnicodeString  AAttrName, UnicodeString  APropName, TDKPropertyEditorRowReq ARequared );
     void       __fastcall AppendIntProp      ( TTagNode* TagNode, UnicodeString  AAttrName, UnicodeString  APropName, TDKPropertyEditorRowReq ARequared );
           UnicodeString __fastcall UpdateChildNodeProp( TTagNode* TagNode, UnicodeString  AChildNodeName, bool UpdatePropValue = false );
     void       __fastcall AppendChildNodeProp( TTagNode* TagNode, UnicodeString  AChildNodeName, UnicodeString  APropName, TDKPropertyEditorRowEn AEnabled );
           UnicodeString __fastcall UpdateAttrExtProp  ( TTagNode* TagNode, UnicodeString  AAttrName, bool UpdatePropValue = false );
     void       __fastcall AppendAttrExtProp  ( TTagNode* TagNode, UnicodeString  AAttrName, UnicodeString  APropName, TDKPropertyEditorRowEn AEnabled );
           UnicodeString __fastcall UpdateValRefProp   ( UnicodeString  AAttrName, UnicodeString  AValue, bool UpdatePropValue = false );
     void       __fastcall AppendValRefProp   ( TTagNode* TagNode, UnicodeString  AAttrName, UnicodeString  APropName, UnicodeString  AValue );
           UnicodeString __fastcall UpdateImpRefProp   ( TTagNode* TagNode, UnicodeString  AAttrName, bool UpdatePropValue = false );
     void       __fastcall AppendImpRefProp   ( TTagNode* TagNode, UnicodeString  AAttrName, UnicodeString  APropName );
           UnicodeString __fastcall UpdateReqRefProp   ( TTagNode* TagNode, UnicodeString  AAttrName, bool UpdatePropValue = false );
     void       __fastcall AppendReqRefProp   ( TTagNode* TagNode, UnicodeString  AAttrName, UnicodeString  APropName );
           UnicodeString __fastcall UpdateCalcRulesProp( TTagNode* TagNode, UnicodeString  AChildNodeName, bool UpdatePropValue = false );
     void       __fastcall AppendCalcRulesProp( TTagNode* TagNode, UnicodeString  AChildNodeName, UnicodeString  APropName, TDKPropertyEditorRowEn AEnabled );
           UnicodeString __fastcall GetSizeViewText    ( UnicodeString  AAttrName, UnicodeString  AAttrVal );
           void       __fastcall SetSizeAttrVal     ( TTagNode* TagNode, UnicodeString  AAttrName, UnicodeString  AAttrVal );
           UnicodeString __fastcall UpdateHandRulesProp( TTagNode* TagNode, UnicodeString  AChildNodeName, bool UpdatePropValue = false );
     void       __fastcall AppendHandRulesProp( TTagNode* TagNode, UnicodeString  AChildNodeName, UnicodeString  APropName, TDKPropertyEditorRowEn AEnabled );
           UnicodeString __fastcall UpdateSizeProp     ( TTagNode* TagNode, UnicodeString  AAttrName, bool UpdatePropValue = false );
     void       __fastcall AppendSizeProp     ( TTagNode* TagNode, UnicodeString  AAttrName, UnicodeString  APropName, TDKPropertyEditorRowEn AEnabled );
           void       __fastcall ApplyStyleProp     ( TTagNode* TagNode, UnicodeString  AttrName, int ARow, UnicodeString  AttrValue );
     void       __fastcall ApplyBinProp       ( TTagNode* TagNode, UnicodeString  AttrName, int ARow, UnicodeString  AttrValue );
           void       __fastcall ApplyUnionProp     ( TTagNode* TagNode, UnicodeString  AttrName, int ARow, UnicodeString  AttrValue );
           void       __fastcall ApplyTextProp      ( TTagNode* TagNode, UnicodeString  AttrName, int ARow, UnicodeString  AttrValue );
           void       __fastcall ApplyIntProp       ( TTagNode* TagNode, UnicodeString  AttrName, int ARow, UnicodeString  AttrValue );
     void       __fastcall ApplyTabPrtyProp   ( TTagNode* TagNode, UnicodeString  AttrName, int ARow, UnicodeString  AttrValue );
           void       __fastcall ApplyRefProp       ( TTagNode* TagNode, UnicodeString  KeyName, UnicodeString  AttrValue );
           void __fastcall UpdateActions();
     void __fastcall UpdateCurTreeNodeText();
    TTreeNode* __fastcall AppendDocElement( UnicodeString  AName );
    TTreeNode* __fastcall AddInsText( TTreeNode* TreeNode );
    TStringList* __fastcall CreateNmklISList();

    bool __fastcall CanDeleteTableRowCol( TTagNode* NodeToDelete );
    bool __fastcall CanDeleteDocRulesRefObj( TTagNode* NodeToDelete );
    virtual bool __fastcall CanDelete( TTreeNode* TreeNode );
    virtual void __fastcall BeforeCurrentNodeDelete();
    virtual void __fastcall AfterCurrentNodeDelete( UnicodeString DeletedNodeName );
    virtual void __fastcall DropTreeNodesAfterLoadXML( TTreeNode* XMLLogicRoot );
    virtual void __fastcall XMLToTree( TTagNode *TagNode, TTreeNode *TreeNode );

    virtual void __fastcall EditTVChange( TObject *Sender, TTreeNode *Node );
    virtual void __fastcall EditTVDrawItem(TCustomTreeView *Sender,
                    TTreeNode *Node, TCustomDrawState State, bool &DefaultDraw);

    void __fastcall PropertyEditorEditButtonClick(TObject *Sender);
    bool __fastcall SetRefCanRefUpdate( TObject* Sender, TTagNode* ndRef );
    void __fastcall SetRefLoadRefObjects( TObject* Sender, TTreeView* tv );
    bool __fastcall EditCountCondRules(
      TObject* Sender,
      UnicodeString ATitle,
      TTagNode *AParentNode,
      TTagNode *ACondRulesNode,
      UnicodeString AMainISUID
    );
    int __fastcall ListSortOrderColListLoad( TObject* Sender, const TTagNode* ndList, TTreeNodes* TreeNodes );
    void __fastcall ListSortOrderListColUpdate( TObject* Sender, const TTagNode* ndListCol, TTreeNode* ListColNode );
    void __fastcall PropertyEditorValidate(TObject *Sender, int ACol,
                    int ARow, const UnicodeString KeyName, const UnicodeString KeyValue);
    void __fastcall PropertyEditorValidateError( TObject *Sender, Exception& E );
    void __fastcall PropertyEditorMouseDown ( TObject* Sender, TMouseButton Button, Classes::TShiftState Shift, int X, int Y );
    void __fastcall EditTVChanging(TObject* Sender, TTreeNode* Node, bool &AllowChange);
    void __fastcall EditTVEnter(TObject *Sender);
    void __fastcall EditTVEditing( TObject *Sender, TTreeNode *Node, bool &AllowEdit );
    void __fastcall EditTVEdited( TObject *Sender, TTreeNode *Node, UnicodeString &S );
    void __fastcall EditTVKeyDown( TObject *Sender, Word &Key, TShiftState Shift );
    bool __fastcall DocSpecEditCondRules( TObject *Sender, UnicodeString ATitle, TTagNode **ACondRules );
    void __fastcall ParametersActionExecute(TObject *Sender);
    bool __fastcall DocFilterBeforeDelete( TObject* Sender, TTagNode* RootNode );
    void __fastcall PreViewActionExecute(TObject *Sender);
    void __fastcall CutActionExecute(TObject *Sender);
    void __fastcall CopyActionExecute(TObject *Sender);
    void __fastcall PasteActionExecute(TObject *Sender);
    void __fastcall DeleteActionExecute(TObject *Sender);
    void __fastcall AddInscActionExecute(TObject *Sender);
    void __fastcall AddInsTextActionExecute(TObject *Sender);
    int __fastcall AddListObjListLoad( TObject* Sender, TTreeNodes* TreeNodes, TTagNode* MainIS );
    void __fastcall AddListActionExecute(TObject *Sender);
    void __fastcall AddLColActionExecute(TObject *Sender);
    void __fastcall AddTableActionExecute(TObject *Sender);
    void __fastcall AddTColActionExecute(TObject *Sender);
    void __fastcall AddTRowActionExecute(TObject *Sender);
    void __fastcall MoveUpActionExecute(TObject *Sender);
    void __fastcall MoveDownActionExecute(TObject *Sender);
    void __fastcall CxToTreeNodesAssign( TTreeNode* Dest, TcxTreeListNode* Src);
    void __fastcall FEditTVDblClick(TObject *Sender);
    TICSNomenklator* FNom;

public:
    __fastcall TICSDocSpec(TComponent* Owner);
    virtual __fastcall ~TICSDocSpec();

    static const UnicodeString ICSName;

    /*�������� ������*/
    TICSDocSpecNewResult __fastcall New(UnicodeString AAutor, UnicodeString APeriodDef = "1");
    virtual bool __fastcall Delete();
    void __fastcall Set(TTagNode* ASpec);
    void __fastcall Load(TTagNode* ASpec);
    TCheckDocSpecResult __fastcall Check();

    /*��������������� ������*/
    virtual UnicodeString __fastcall GetXMLNodeViewText( TTagNode *TagNode );
    static UnicodeString __fastcall GetInsTagViewText( TTagNode* ndInsText, TAxeXMLContainer *AXMLContainer, PINS_VIEW_TEXT = NULL );
    static TAttr* __fastcall GetListColGroupUnion( const TTagNode *ndListEl );
    static bool __fastcall IsListColGroup( const TTagNode *ndListEl );
    static bool __fastcall IsListColLinkISGroup( const TTagNode *ndListEl );
    static bool __fastcall IsListColSingle1(const TTagNode *ndListEl);
    static bool __fastcall IsListColSingle1Main(const TTagNode *ndListEl);
    static bool __fastcall IsListColSingle1Count(const TTagNode *ndListEl);
    static bool __fastcall IsListColSingle2( const TTagNode *ndListEl );
    static bool __fastcall IsListColSingle( const TTagNode *ndListEl );
    static bool __fastcall IsListCol( const TTagNode *ndListEl );
    static bool __fastcall IsListFirstCol( const TTagNode *ndListCol );
    static int __fastcall GetListColSingle2Index( const TTagNode* TagNode );
    static UnicodeString __fastcall GetSizeAttrVal( TTagNode* TagNode, UnicodeString  AAttrName );
    static bool __fastcall InsertListColSingle2SizeAttr( TTagNode* TagNode, UnicodeString  AAttrName, UnicodeString  AAttrVal );
    static bool __fastcall DeleteListColSingle2SizeAttr( TTagNode* TagNode, UnicodeString  AAttrName );
    static bool __fastcall SwapListColSingle2SizeAttrVals( TTagNode* TagNode1, TTagNode* TagNode2, UnicodeString  AAttrName );
    static bool __fastcall IsTableFirstCol( const TTagNode *ndTableCol );
    static bool __fastcall IsTableFirstRow( const TTagNode *ndTableRow );
    static bool __fastcall IsTableColSingle( const TTagNode *ndTableCol );
    static bool __fastcall IsTableRowSingle( const TTagNode *ndTableRow );
    static void __fastcall GetTableSingleColsList( TTagNode *ANode, TTagNodeList *ATableSingleCols );
    static void __fastcall GetTableSingleRowsList( TTagNode *ANode, TTagNodeList *ATableSingleRows );
    static void __fastcall GetListSingleColsList( TTagNode *ANode, TTagNodeList *AListSingleCols );


__published:
    /*�������������� ��������*/
    __property bool                   PreviewOnFly            = { read    = FPreviewOnFly,
                                                                  write   = FPreviewOnFly,
                                                                  default = false
    };
    __property TICSDocViewerDocFormat PreviewDocFormat        = { read    = FPreviewDocFormat,
                                                                  write   = FPreviewDocFormat,
                                                                  default = dfHTML
    };
    __property bool                   UseCSSOnHTMLPreview     = { read    = FUseCSSOnHTMLPreview,
                                                                  write   = FUseCSSOnHTMLPreview,
                                                                  default = true
    };
    __property bool                   UseWordCSSOnHTMLPreview = { read    = FUseWordCSSOnHTMLPreview,
                                                                  write   = FUseWordCSSOnHTMLPreview,
                                                                  default = true
    };
    __property TICSDocViewerDlgKind   PreviewDlgKind          = { read    = FPreviewDlgKind,
                                                                  write   = FPreviewDlgKind,
                                                                  default = pmInternalDlg };

    //�������� ������� ��� ������ � �������� �������
    __property TICSDocControlProps* PropsPanel = { read = FPropsPanelProps, write = SetPropsPanelProps };
    //�������� ������� ��� ������ � ������� �������������
    __property TICSDocControlProps* SpecPanel = { read = FSpecPanelProps, write = SetSpecPanelProps };
    //�������� ������� ��� splitter'�
    __property Splitter;
    //��������� ������ ������������� ( �� ��������� "�������� ���������" )
    __property Title;
    //���� ���� ��� "������������" ����� ����� ( �� ��������� clWindow )
    __property ReqEdBkColor;
    //������ ��� ������� �������������� ������ ������������ ��������� �� ���������
    __property TIcon*               DocByDocIcon = { read = FDocByDocIcon, write = SetDocByDocIcon };

    //������� �������� ���������
    __property UnicodeString   BaseISUID = { read = FBaseMainISUID, write = FBaseMainISUID};

    __property TICSNomenklator*   Nom = {read=FNom,write=FNom};

    /*��������������� �������� �� ��������� ��� �������������� �������*/
    __property Width       = { default = 700 };
    __property Height      = { default = 500 };

    /*�������*/
    __property TOnFltCustomLoadEvent        OnLoadFilter   = { read = FOnLoadFilter, write = FOnLoadFilter };
    __property TOnGetSpecOwnerEvent         OnGetSpecOwner = { read = FOnGetSpecOwner, write = FOnGetSpecOwner };
    __property TOnGetSpecificatorsListEvent OnGetSpecificatorsList = { read = FOnGetSpecsList, write = FOnGetSpecsList };
    __property TOnGetOrgsListEvent          OnGetOrgsList          = { read = FOnGetOrgsList,  write = FOnGetOrgsList  };
    __property TOnGetSpecNodeEvent          OnGetSpecNode          = { read = FOnGetSpecNode,  write = FOnGetSpecNode  };
    __property TPPreviewDemoMessageEvent    OnDemoMessage = {read=FPPDemoMsg,write=FSetPPDemoMsg};
};

//---------------------------------------------------------------------------

} // end of namespace ICSDocSpec
using namespace ICSDocSpec;

#endif
