/*
  ����        - ICSDocFilterUtils.cpp
  ������      - ��������������� (ICS)
  ��������    - ������ ��� ������ � ��������.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 10.01.2007
*/
//---------------------------------------------------------------------------

#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "ICSDocFilterUtils.h"

#include "DKClasses.h"
#include "DKVCLVerInfo.h"
#include "DKXMLUtils.h"

#include "ICSDocGlobals.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


namespace ICSDocFilter
{

//---------------------------------------------------------------------------

const UnicodeString csFilterDefVersion        = "2.0.1";
const UnicodeString csFilterDefDefaultVersion = "2.0.0";

//---------------------------------------------------------------------------
__fastcall EICSDocFilterError::EICSDocFilterError(const UnicodeString Msg)
: Exception(Msg)
{
}
//---------------------------------------------------------------------------
inline void __fastcall CheckFilterParam(UnicodeString AFuncName, TTagNode *AFilter)
{
  if (!AFilter)
    throw DKClasses::ENullArgument(AFuncName, "AFilter");
  if (AFilter->Name != "region")
    throw DKClasses::EInvalidArgument(
      AFuncName,
      "AFilter",
      FMT1(icsDocErrorTagEq,"region")
    );
}

//---------------------------------------------------------------------------

struct UpdateFO : public unary_function<TTagNode*, void>
{
  bool* FModified;

  UpdateFO(bool* AModified) : FModified(AModified) {}
};

struct UpdateCondRulesFO : public UpdateFO
{
  bool  FIsFilter;

  UpdateCondRulesFO(bool* AModified, bool AIsFilter) :
    UpdateFO(AModified),
    FIsFilter(AIsFilter)
  {
  }
};

struct UpdateFilterAttrs_2_0_1FO : public UpdateFO
{
  UpdateFilterAttrs_2_0_1FO(bool* AModified) : UpdateFO(AModified) {}
  void operator() (TTagNode *TagNode);
};

struct UpdateFilterAttrs_2_0_0FO : public UpdateFO
{
  UpdateFilterAttrs_2_0_0FO(bool* AModified) : UpdateFO(AModified) {}
  void operator() (TTagNode *TagNode);
};

struct UpdateFilter_2_0_1FO : public UpdateFO
{
  UpdateFilter_2_0_1FO(bool* AModified) : UpdateFO(AModified) {}
  void operator() (TTagNode *TagNode);
};

struct UpdateCondRules_2_0_1FO : public UpdateCondRulesFO
{
  UpdateCondRules_2_0_1FO(bool* AModified, bool AIsFilter) :
    UpdateCondRulesFO(AModified, AIsFilter)
  {
  }
  void operator() (TTagNode *TagNode);
};

//---------------------------------------------------------------------------

void UpdateFilterAttrs_2_0_1FO::operator() (TTagNode *TagNode)
{
  if      (TagNode->CmpName("passport"))
  {
    UpdateXMLAttr(TagNode, "defversion", "2.0.1", FModified);
  }
  else
    UpdateCondRules_2_0_1FO(FModified, true)(TagNode);
}

//---------------------------------------------------------------------------

void UpdateFilterAttrs_2_0_0FO::operator() (TTagNode *TagNode)
{
  UpdateCondRules_2_0_1FO(FModified, true)(TagNode);
}

//---------------------------------------------------------------------------

void UpdateFilter_2_0_1FO::operator() (TTagNode *TagNode)
{
  if      (TagNode->CmpName("passport"))
  {
    TagNode->AV["defversion"] = "2.0.1";
    *FModified = true;
  }
}

//---------------------------------------------------------------------------

void UpdateCondRules_2_0_1FO::operator() (TTagNode *TagNode)
{
  if      (TagNode->CmpName("condrules"))
  {
    UpdateXMLAttr(TagNode, "uniontype", "or", FModified);
  }
  else if (TagNode->CmpName("IERef"))
  {
    UpdateXMLAttr(TagNode, "not", "0", FModified);
    if (FIsFilter)
      UpdateXMLAttr(TagNode, "incltoname", "0", FModified);
  }
  else if (TagNode->CmpName("IAND"))
  {
    UpdateXMLAttr(TagNode, "not"      , "0"  , FModified);
    UpdateXMLAttr(TagNode, "uniontype", "and", FModified);
    if (FIsFilter)
      UpdateXMLAttr(TagNode, "incltoname", "0", FModified);
  }
  else if (TagNode->CmpName("IOR"))
  {
    UpdateXMLAttr(TagNode, "not", "0", FModified);
    if (FIsFilter)
      UpdateXMLAttr(TagNode, "incltoname", "0", FModified);
  }
  else if (TagNode->CmpName("IMOR"))
  {
    UpdateXMLAttr(TagNode, "not"      , "0"  , FModified);
    UpdateXMLAttr(TagNode, "uniontype", "and", FModified);
    if (FIsFilter)
      UpdateXMLAttr(TagNode, "incltoname", "0", FModified);
  }
  else if (TagNode->CmpName("RAND"))
  {
    UpdateXMLAttr(TagNode, "not"    , "0", FModified);
    UpdateXMLAttr(TagNode, "pure"   , "1", FModified);
    UpdateXMLAttr(TagNode, "comment", "" , FModified);
  }
  else if (TagNode->CmpName("ROR"))
  {
    UpdateXMLAttr(TagNode, "not"    , "0", FModified);
    UpdateXMLAttr(TagNode, "pure"   , "1", FModified);
    UpdateXMLAttr(TagNode, "comment", "" , FModified);
  }
}

}// end of namespace ICSDocFilter

//---------------------------------------------------------------------------

void __fastcall ICSDocFilter::CheckCondRulesParam(UnicodeString AFuncName, TTagNode *ACondRules)
{
  if (!ACondRules)
    throw DKClasses::ENullArgument(AFuncName, "ACondRules");
  if (ACondRules->Name != "condrules")
    throw DKClasses::EInvalidArgument(
      AFuncName,
      "ACondRules",
      FMT1(icsDocErrorTagEq,"condrules")
    );
}

//---------------------------------------------------------------------------

UnicodeString __fastcall ICSDocFilter::GetFilterDefVersion(TTagNode *AFilter)
{
  CheckFilterParam(__FUNC__, AFilter);
  return AFilter->GetChildByName("passport")->GetAVDef(
    "defversion",
    csFilterDefDefaultVersion
  );
}

//---------------------------------------------------------------------------

bool __fastcall ICSDocFilter::IsFilterSupported(TTagNode *AFilter)
{
  CheckFilterParam(__FUNC__, AFilter);
  return (VersionCmp(GetFilterDefVersion(AFilter), csFilterDefVersion) <= 0);
}

//---------------------------------------------------------------------------

void __fastcall ICSDocFilter::CheckFilter(TTagNode *AFilter)
{
  if (!IsFilterSupported(AFilter))
    throw EICSDocFilterError(
      FMT2(icsDocFilterUtilsFilterVersion,
           GetFilterDefVersion(AFilter),
           csFilterDefVersion)
    );
}

//---------------------------------------------------------------------------

bool __fastcall ICSDocFilter::UpdateFilter(TTagNode *AFilter)
{
  CheckFilter(AFilter);
  bool fModified = false;
  UnicodeString sFilterDefCurVersion = GetFilterDefVersion(AFilter);
  if (VersionCmp(sFilterDefCurVersion, csFilterDefVersion) == 0)
    ForEachTagNode(AFilter, UpdateFilterAttrs_2_0_1FO(&fModified));
  else
  {
    while (VersionCmp(sFilterDefCurVersion, csFilterDefVersion) < 0)
    {
      if (sFilterDefCurVersion == "2.0.0")
      {
        ForEachTagNode(AFilter, UpdateFilterAttrs_2_0_0FO(&fModified));
        ForEachTagNode(AFilter, UpdateFilter_2_0_1FO(&fModified));
      }
      sFilterDefCurVersion = GetFilterDefVersion(AFilter);
    }
  }
  return fModified;
}

//---------------------------------------------------------------------------

bool __fastcall ICSDocFilter::UpdateCondRules_2_0_1(TTagNode *ACondRules, bool AIsFilter)
{
  CheckCondRulesParam(__FUNC__, ACondRules);
  bool fModified = false;
  ForEachTagNode(ACondRules, UpdateCondRules_2_0_1FO(&fModified, AIsFilter));
  return fModified;
}

//---------------------------------------------------------------------------

