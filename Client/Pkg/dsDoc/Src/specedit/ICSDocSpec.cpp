/*
 ����        - ICSDocSpec.cpp
 ������      - ��������������� (ICS)
 ��������    - �������� �������������.
 ���� ����������
 ����������� - ������� �.�.
 ����        - 15.09.2004
 */
// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "icsDocSpecConstDef.h"
#include "ICSDocSpec.h"
#include <functional>
#include <iterator>
// #include "DKMBUtils.h"
#include "msgdef.h"
#include "DKRusMB.h"
#include "DKAxeUtils.h"
// #include "FilterEdit.h"
// #include "ICSDocFilter.h"
#include "SetRef.h"
#include "AddList.h"
#include "AddTable.h"
#include "RulesExprEdit.h"
#include "Count.h"
#include "ListSortOrder.h"
#include "DocElSizeEdit.h"
#include "ICSTL.h"
#pragma package(smart_init)
// ---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
static inline void ValidCtrCheck(TICSDocSpec *)
 {
  new TICSDocSpec(NULL);
 }
// ---------------------------------------------------------------------------
namespace ICSDocSpec
 {
 // ###########################################################################
 // ##                                                                       ##
 // ##                                 Globals                               ##
 // ##                                                                       ##
 // ###########################################################################
#define ONE_AV     "1"
#define ZERO_AV    "0"
#define ONE_VAV    cFMT(icsDocSpecONE_VAVCaption)
#define ZERO_VAV   cFMT(icsDocSpecZERO_VAVCaption)
#define TRUE_AV    "yes"
#define FALSE_AV   "no"
#define TRUE_VAV   cFMT(icsDocSpecTRUE_VAVCaption)
#define FALSE_VAV  cFMT(icsDocSpecFALSE_VAVCaption)
#define TAB_PRTY_ROW_AV   "row"
#define TAB_PRTY_COL_AV   "col"
#define TAB_PRTY_ROW_VAV  cFMT(icsDocSpecTabPrtyColCaption)
#define TAB_PRTY_COL_VAV  cFMT(icsDocSpecTabPrtyRowCaption)
#define EMPTY_LCOL        cFMT(icsDocSpecEmptyColCaption)
#define GET_BIN_AV( BinVAV )   ( ( (BinVAV)  == ONE_VAV  ) ? ONE_AV    : ZERO_AV   )
#define GET_BIN_VAV( BinAV )   ( ( (BinAV)   == ONE_AV   ) ? ONE_VAV   : ZERO_VAV  )
#define GET_BOOL_AV( BoolVAV ) ( ( (BoolVAV) == TRUE_VAV ) ? TRUE_AV   : FALSE_AV  )
#define GET_BOOL_VAV( BoolAV ) ( ( (BoolAV)  == TRUE_AV  ) ? TRUE_VAV  : FALSE_VAV )
#define GET_BOOL_AV( BoolVAV ) ( ( (BoolVAV) == TRUE_VAV ) ? TRUE_AV   : FALSE_AV  )
#define GET_TAB_PRTY_AV( TabPrtyVAV ) ( ( (TabPrtyVAV)  == TAB_PRTY_ROW_VAV  ) ? TAB_PRTY_ROW_AV   : TAB_PRTY_COL_AV  )
#define GET_TAB_PRTY_VAV( TabPrtyAV ) ( ( (TabPrtyAV)   == TAB_PRTY_ROW_AV   ) ? TAB_PRTY_ROW_VAV  : TAB_PRTY_COL_VAV )
 // ---------------------------------------------------------------------------
#pragma resource "ICSDoc.res"
 // ---------------------------------------------------------------------------
 const UnicodeString csCountViewText = cFMT(icsDocSpecCountViewText);
 const UnicodeString csEmptyEllipsiseEditor = cFMT(icsDocSpecEmptyEllipsiseEditor);
 const UnicodeString csNotEmptyEllipsiseEditor = cFMT(icsDocSpecNotEmptyEllipsiseEditor);
 const UnicodeString csEmptyInsText = cFMT(icsDocSpecEmptyInsCaption);
 // ---------------------------------------------------------------------------
 // �������������� ������ ��� ������ � ��������� TUIDsSet
 struct SelectPareTagNodeRefAttrVal : public unary_function<pair<UnicodeString, TTagNode *>, UnicodeString>
  {
   UnicodeString operator()(const pair<UnicodeString, TTagNode *>x) const ;
  };
 UnicodeString SelectPareTagNodeRefAttrVal:: operator()(const pair<UnicodeString, TTagNode *>x) const
  {
   return x.second->AV["ref"];
  }
 // ---------------------------------------------------------------------------
 // �������������� ������ ��� ������ � ������ TTagNode'��
 struct SelectTagNodeUID : public unary_function<TTagNode *, UnicodeString>
  {
   const UnicodeString operator()(const TTagNode * x) const ;
  };
 const UnicodeString SelectTagNodeUID:: operator()(const TTagNode * x) const
  {
   return const_cast<TTagNode *>(x)->AV["uid"];
  }
 // ---------------------------------------------------------------------------
 enum TImageListInd
  {
   iiParameters = 0, iiPreView, iiCut, iiCopy, iiPaste, iiDelete, iiInscription, iiInsText, iiList, iiLCol, iiTable,
   iiTCol, iiTRow, iiMoveUp, iiMoveDown
  };
 // ---------------------------------------------------------------------------
 struct TTagNodeListPair
  {
   TTagNodeList * first;
   TTagNodeList * second;
  };
 // ---------------------------------------------------------------------------
 } // end of namespace ICSDocSpec
// ###########################################################################
// ##                                                                       ##
// ##                              TICSDocSpec                              ##
// ##                                                                       ##
// ###########################################################################
const UnicodeString TICSDocSpec::ICSName = cFMT(icsDocSpecCaption);
// ---------------------------------------------------------------------------
// ---------------------- IDKComponentVersion --------------------------------
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::DKGetVersion() const
 {
  return "1.1.5";
 }
// ***************************************************************************
// ********************** ��������/�������� ���������� ***********************
// ***************************************************************************
__fastcall TICSDocSpec::TICSDocSpec(TComponent * Owner) : TICSDocBaseEdit(Owner)
 {
  Width              = 700;
  Height             = 500;
  FNom               = NULL;
  FBaseMainISUID     = "";
  FImageList->Height = 16;
  FImageList->Width  = 16;
  LoadImageListBitmap("ICSDS_PARAMETERS16");
  LoadImageListBitmap("ICSDS_PREVIEW16");
  LoadImageListBitmap("ICSDS_CUT16");
  LoadImageListBitmap("ICSDS_COPY16");
  LoadImageListBitmap("ICSDS_PASTE16");
  LoadImageListBitmap("ICSDS_DELETE16");
  LoadImageListBitmap("ICSDS_INSCRIPTION16");
  LoadImageListBitmap("ICSDS_INSTEXT16");
  LoadImageListBitmap("ICSDS_LIST16");
  LoadImageListBitmap("ICSDS_LCOL16");
  LoadImageListBitmap("ICSDS_TABLE16");
  LoadImageListBitmap("ICSDS_TCOL16");
  LoadImageListBitmap("ICSDS_TROW16");
  LoadImageListBitmap("ICSDS_MOVEUP16");
  LoadImageListBitmap("ICSDS_MOVEDOWN16");
  Parent                          = (TWinControl *)Owner;
  FMenuBar                        = FMainBM->AddToolBar(true);
  FMenuBar->Name                  = "MenuBar";
  FMenuBar->Caption               = FMT(icsDocSpecMainMenuCaption);
  FMenuBar->AllowClose            = false;
  FMenuBar->AllowCustomizing      = false;
  FMenuBar->AllowQuickCustomizing = false;
  FMenuBar->AllowReset            = false;
  InitEditTB();
  InitCommonTB();
  FPropertyPanel = new TPanel(this);
  FPropertyPanel->SetSubComponent(true);
  FPropertyPanel->Parent      = this;
  FPropertyPanel->Align       = alRight;
  FPropertyPanel->BevelInner  = bvNone;
  FPropertyPanel->BevelOuter  = bvNone;
  FPropertyPanel->BorderStyle = bsSingle;
  FPropertyPanel->Width       = 235;
  FPropertyPanel->Name        = "PropertyPanel";
  FPropertyPanel->Caption     = "";
  InitSplitter();
  FSplitter->Align = alRight;
  InitEditTV(this);
  FEditTV->ReadOnly   = false;
  FEditTV->BevelKind  = bkFlat;
  FEditTV->OnChanging = EditTVChanging;
  FEditTV->OnEnter    = EditTVEnter;
  FEditTV->OnEditing  = EditTVEditing;
  FEditTV->OnEdited   = EditTVEdited;
  FEditTV->OnKeyDown  = EditTVKeyDown;
  FStaticText         = new TStaticText(this);
  FStaticText->SetSubComponent(true);
  FStaticText->Parent      = FPropertyPanel;
  FStaticText->Align       = alTop;
  FStaticText->BorderStyle = sbsSingle;
  FStaticText->Color       = clMedGray;
  FStaticText->Font->Color = clWhite;
  FStaticText->Font->Style = TFontStyles() << fsBold;
  FStaticText->Caption     = "   " + FMT(icsDocSpecPropPanelCaption);
  FStaticText->Name        = "StaticText";
  FPropertyEditor          = new TDKPropertyEditor(this);
  FPropertyEditor->SetSubComponent(true);
  FPropertyEditor->Parent            = FPropertyPanel;
  FPropertyEditor->Align             = alClient;
  FPropertyEditor->Visible           = false;
  FPropertyEditor->ReqEdBkColor      = FReqEdBkColor;
  FPropertyEditor->OnEditButtonClick = PropertyEditorEditButtonClick;
  FPropertyEditor->OnValidate        = PropertyEditorValidate;
  FPropertyEditor->OnValidateError   = PropertyEditorValidateError;
  FPropertyEditor->OnMouseDown       = PropertyEditorMouseDown;
  FPropertyEditor->Name              = "PropertyEditor";
  InitAction(FParametersAction, FMT(icsDocSpecParametersCaption), FMT(icsDocSpecParametersHint),
    FMT(icsDocSpecParametersShortCut), iiParameters, ParametersActionExecute);
  InitAction(FPreViewAction, FMT(icsDocSpecPreViewCaption), FMT(icsDocSpecPreViewHint), FMT(icsDocSpecPreViewShortCut),
    iiPreView, PreViewActionExecute);
  InitAction(FCutAction, FMT(icsDocSpecCutCaption), FMT(icsDocSpecCutHint), FMT(icsDocSpecCutShortCut), iiCut,
    CutActionExecute);
  InitAction(FCopyAction, FMT(icsDocSpecCopyCaption), FMT(icsDocSpecCopyHint), FMT(icsDocSpecCopyShortCut), iiCopy,
    CopyActionExecute);
  InitAction(FPasteAction, FMT(icsDocSpecPasteCaption), FMT(icsDocSpecPasteHint), FMT(icsDocSpecPasteShortCut), iiPaste,
    PasteActionExecute);
  InitAction(FDeleteAction, FMT(icsDocSpecDeleteCaption), FMT(icsDocSpecDeleteHint), FMT(icsDocSpecDeleteShortCut),
    iiDelete, DeleteActionExecute);
  InitAction(FAddInscAction, FMT(icsDocSpecAddInscCaption), FMT(icsDocSpecAddInscHint), FMT(icsDocSpecAddInscShortCut),
    iiInscription, AddInscActionExecute);
  InitAction(FAddInsTextAction, FMT(icsDocSpecAddInsTextCaption), FMT(icsDocSpecAddInsTextHint),
    FMT(icsDocSpecAddInsTextShortCut), iiInsText, AddInsTextActionExecute);
  InitAction(FAddListAction, FMT(icsDocSpecAddListCaption), FMT(icsDocSpecAddListHint), FMT(icsDocSpecAddListShortCut),
    iiList, AddListActionExecute);
  InitAction(FAddLColAction, FMT(icsDocSpecAddLColCaption), FMT(icsDocSpecAddLColHint), FMT(icsDocSpecAddLColShortCut),
    iiLCol, AddLColActionExecute);
  InitAction(FAddTableAction, FMT(icsDocSpecAddTableCaption), FMT(icsDocSpecAddTableHint),
    FMT(icsDocSpecAddTableShortCut), iiTable, AddTableActionExecute);
  InitAction(FAddTColAction, FMT(icsDocSpecAddTColCaption), FMT(icsDocSpecAddTColHint), FMT(icsDocSpecAddTColShortCut),
    iiTCol, AddTColActionExecute);
  InitAction(FAddTRowAction, FMT(icsDocSpecAddTRowCaption), FMT(icsDocSpecAddTRowHint), FMT(icsDocSpecAddTRowShortCut),
    iiTRow, AddTRowActionExecute);
  InitAction(FMoveUpAction, FMT(icsDocSpecMoveUpCaption), FMT(icsDocSpecMoveUpHint), FMT(icsDocSpecMoveUpShortCut),
    iiMoveUp, MoveUpActionExecute);
  InitAction(FMoveDownAction, FMT(icsDocSpecMoveDownCaption), FMT(icsDocSpecMoveDownHint),
    FMT(icsDocSpecMoveDownShortCut), iiMoveDown, MoveDownActionExecute);
  FCutAction->Visible   = false;
  FCopyAction->Visible  = false;
  FPasteAction->Visible = false;
  /*
   TdxBarSubItem*         FCommonItem;
   TdxBarButton*          FParametersItem;
   TdxBarButton*          FPreViewItem;

   TdxBarSubItem*         FEditItem;
   TdxBarButton*          FCutItem;
   TdxBarButton*          FCopyItem;
   TdxBarButton*          FPasteItem;
   TdxBarButton*          FDeleteItem;
   TdxBarButton*          FMoveUpItem;
   TdxBarButton*          FMoveDownItem;

   TdxBarSubItem*         FObjectItem;
   */
  // �������� ����
  CreateTBSubMenuItem(FMenuBar->ItemLinks, FCommonItem);
  CreateTBItem(FCommonItem->ItemLinks, FParametersItem, FParametersAction);
  CreateTBItem(FCommonItem->ItemLinks, FPreViewItem, FPreViewAction);
  CreateTBSubMenuItem(FMenuBar->ItemLinks, FEditItem);
  CreateTBItem(FEditItem->ItemLinks, FCutItem, FCutAction);
  CreateTBItem(FEditItem->ItemLinks, FCopyItem, FCopyAction);
  CreateTBItem(FEditItem->ItemLinks, FPasteItem, FPasteAction);
  CreateTBItem(FEditItem->ItemLinks, FDeleteItem, FDeleteAction);
  CreateTBItem(FEditItem->ItemLinks, FMoveUpItem, FMoveUpAction, true);
  CreateTBItem(FEditItem->ItemLinks, FMoveDownItem, FMoveDownAction);
  CreateTBSubMenuItem(FMenuBar->ItemLinks, FObjectItem);
  CreatePMItem(FEditPM, FAddInscriptionMItem, FAddInscAction);
  CreatePMItem(FEditPM, FAddInsTextMItem, FAddInsTextAction);
  CreatePMItem(FEditPM, FSep1MItem);
  CreatePMItem(FEditPM, FAddListMItem, FAddListAction);
  CreatePMItem(FEditPM, FAddLColMItem, FAddLColAction);
  CreatePMItem(FEditPM, FSep2MItem);
  CreatePMItem(FEditPM, FAddTableMItem, FAddTableAction);
  CreatePMItem(FEditPM, FAddTColMItem, FAddTColAction);
  CreatePMItem(FEditPM, FAddTRowMItem, FAddTRowAction);
  CreatePMItem(FEditPM, FSep3MItem);
  CreatePMItem(FEditPM, FCutMItem, FCutAction);
  CreatePMItem(FEditPM, FCopyMItem, FCopyAction);
  CreatePMItem(FEditPM, FPasteMItem, FPasteAction);
  CreatePMItem(FEditPM, FDeleteMItem, FDeleteAction);
  CreatePMItem(FEditPM, FSep4MItem);
  CreatePMItem(FEditPM, FMoveUpMItem, FMoveUpAction);
  CreatePMItem(FEditPM, FMoveDownMItem, FMoveDownAction);
  CreateTBSubMenuCleckedItem(FObjectItem->ItemLinks, FAddInscItem, FAddInscMenuItem, FAddInscAction);
  CreateTBItem(FAddInscMenuItem->ItemLinks, FAddInsTextItem, FAddInsTextAction);
  CreateTBSubMenuCleckedItem(FObjectItem->ItemLinks, FAddListItem, FAddListMenuItem, FAddListAction);
  CreateTBItem(FAddListMenuItem->ItemLinks, FAddLColItem, FAddLColAction);
  CreateTBSubMenuCleckedItem(FObjectItem->ItemLinks, FAddTableItem, FAddTableMenuItem, FAddTableAction);
  CreateTBItem(FAddTableMenuItem->ItemLinks, FAddTColItem, FAddTColAction);
  CreateTBItem(FAddTableMenuItem->ItemLinks, FAddTRowItem, FAddTRowAction);
  FCommonItem->Caption = FMT(icsDocSpecCommonItemCaption);
  FEditItem->Caption   = FMT(icsDocSpecEditItemCaption);
  FObjectItem->Caption = FMT(icsDocSpecObjectItemCaption);
  // �������� ������ ������������
  AddTBItem(FCommonTB->ItemLinks, FParametersItem);
  AddTBItem(FCommonTB->ItemLinks, FPreViewItem);
  AddTBItem(FEditTB->ItemLinks, FCutItem, FCutAction);
  AddTBItem(FEditTB->ItemLinks, FCopyItem, FCopyAction);
  AddTBItem(FEditTB->ItemLinks, FPasteItem, FPasteAction);
  AddTBItem(FEditTB->ItemLinks, FDeleteItem, FDeleteAction);
  AddTBItem(FEditTB->ItemLinks, FAddInscItem, true);
  AddTBItem(FEditTB->ItemLinks, FAddListItem);
  AddTBItem(FEditTB->ItemLinks, FAddTableItem);
  AddTBItem(FEditTB->ItemLinks, FMoveUpItem, true);
  AddTBItem(FEditTB->ItemLinks, FMoveDownItem);
  FPPDemoMsg               = NULL;
  FViewer                  = new TICSDocViewer(this);
  FViewer->OnDemoMessage   = FPPDemoMsg;
  FPreviewOnFly            = false;
  FPreviewDocFormat        = dfHTML;
  FUseCSSOnHTMLPreview     = true;
  FUseWordCSSOnHTMLPreview = true;
  FPreviewDlgKind          = pmInternalDlg;
  FXMLTitle                = FMT(icsDocSpecXMLTitle);
  FDefDocByDocIcon         = new TIcon;
  FDefDocByDocIcon->ReleaseHandle();
  FDefDocByDocIcon->Handle   = LoadIcon(HInstance, L"ICSDS_DOCBYDOC16i");
  FDocByDocIcon              = new TIcon;
  FEnumTableRowsCols         = true;
  FPropsPanelProps           = new TICSDocControlProps(FPropertyPanel);
  FSpecPanelProps            = new TICSDocControlProps(FEditTV);
  FValidateErrorOnTVChanging = false;
  FOnLoadFilter              = NULL;
  FOnGetSpecOwner            = NULL;
  FOnGetSpecsList            = NULL;
  FOnGetOrgsList             = NULL;
  FOnGetSpecNode             = NULL;
  // FEditTV->OnDblClick = FEditTVDblClick;
 }
// ---------------------------------------------------------------------------
__fastcall TICSDocSpec::~TICSDocSpec()
 {
  delete FDefDocByDocIcon;
  delete FDocByDocIcon;
  delete FPropsPanelProps;
  delete FSpecPanelProps;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::FSetPPDemoMsg(TPPreviewDemoMessageEvent APPDemoMsg)
 {
  FPPDemoMsg = APPDemoMsg;
  if (FViewer)
   FViewer->OnDemoMessage = FPPDemoMsg;
 }
// ***************************************************************************
// ********************** ��������������� ������ *****************************
// ***************************************************************************
void __fastcall TICSDocSpec::SetPropsPanelProps(const TICSDocControlProps * AValue)
 {
  FPropsPanelProps->Assign(const_cast<TICSDocControlProps *>(AValue));
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::SetSpecPanelProps(const TICSDocControlProps * AValue)
 {
  FSpecPanelProps->Assign(const_cast<TICSDocControlProps *>(AValue));
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::SetDocByDocIcon(const TIcon * AValue)
 {
  FDocByDocIcon->Assign(const_cast<TIcon *>(AValue));
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::SetReqEdBkColor(TColor AValue)
 {
  if (FReqEdBkColor != AValue)
   {
    FReqEdBkColor                 = AValue;
    FPropertyEditor->ReqEdBkColor = AValue;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::FillStyleList(TStrings * S)
 {
  if (!S)
   throw DKClasses::ENullArgument(__FUNC__, "S");
  if (!FXML)
   throw DKClasses::EMethodError(__FUNC__, cFMT(icsDocSpecErrorCreateXML));
  S->Clear();
  TTagNode * ndStyle = FXML->GetChildByName("styles", true)->GetFirstChild();
  while (ndStyle)
   {
    S->AddObject(ndStyle->AV["name"], reinterpret_cast<TObject *>(UIDInt(ndStyle->AV["uid"])));
    ndStyle = ndStyle->GetNext();
   }
 }
// ---------------------------------------------------------------------------
// ������ �� �������� �����/�������� �������, �� ������� ���� ������
bool __fastcall TICSDocSpec::CanDeleteTableRowCol(TTagNode * NodeToDelete)
 {
  bool bCanDelete = true;
  TTagNodeList TableSingleRows; // ������ "��������" ����� �������
  TTagNodeList TableSingleCols; // ������ "��������" �������� �������
  TTagNodeList TableSingleLinesToDelete; // ������ "��������" �����/�������� ������� ����������
  // ��������
  UnicodeString LineName00, LineName0, LineName1, LineName2;
  GetTableSingleRowsList(NodeToDelete->GetParent("table"), & TableSingleRows);
  GetTableSingleColsList(NodeToDelete->GetParent("table"), & TableSingleCols);
  map<TTagNode *, int>TableSingleNumRows; // ������� "��������" ����� ������� � ����������
  map<TTagNode *, int>TableSingleNumCols; // ������� "��������" �������� ������� � ����������
  map<TTagNode *, int> *pTableSingleNumLines; // ������� "��������" �����/�������� ������� � ����������
  if (NodeToDelete->CmpName("tcol"))
   {
    GetTableSingleColsList(NodeToDelete, & TableSingleLinesToDelete);
    LineName0            = FMT(icsDocSpecLineName0Col);
    LineName00           = FMT(icsDocSpecLineName00Col);
    LineName1            = FMT(icsDocSpecLineName1Col);
    LineName2            = FMT(icsDocSpecLineName2Col);
    pTableSingleNumLines = &TableSingleNumCols;
   }
  else // "trow"
   {
    GetTableSingleRowsList(NodeToDelete, & TableSingleLinesToDelete);
    LineName0            = FMT(icsDocSpecLineName0Row);
    LineName00           = FMT(icsDocSpecLineName00Row);
    LineName1            = FMT(icsDocSpecLineName1Row);
    LineName2            = FMT(icsDocSpecLineName2Row);
    pTableSingleNumLines = &TableSingleNumRows;
   }
  int nNum = 1;
  for (TTagNodeList::const_iterator i = TableSingleRows.begin(); i != TableSingleRows.end(); i++)
   TableSingleNumRows[*i] = nNum++ ;
  nNum = 1;
  for (TTagNodeList::const_iterator i = TableSingleCols.begin(); i != TableSingleCols.end(); i++)
   TableSingleNumCols[*i] = nNum++ ;
  map<TTagNode *, TTagNodeListPair>Refers; // ������� ����� � ������� �������, ����������� �� ���������
  // ������/������� �������
  for (TTagNodeList::const_iterator i = TableSingleLinesToDelete.begin(); i != TableSingleLinesToDelete.end(); i++)
   {
    Refers[*i].first  = new TTagNodeList;
    Refers[*i].second = new TTagNodeList;
   }
  bool bRefersExist = false;
  // ���������� �������
  for (int nTableSingleLineTypeNo = 0; nTableSingleLineTypeNo < 2; nTableSingleLineTypeNo++)
   {
    TTagNodeList * pTableSingleLines;
    map<TTagNode *, int> *pTableSingleNumLines;
    switch (nTableSingleLineTypeNo)
     {
     case 0:
      pTableSingleLines = &TableSingleRows;
      pTableSingleNumLines = &TableSingleNumRows;
      break;
     case 1:
      pTableSingleLines = &TableSingleCols;
      pTableSingleNumLines = &TableSingleNumCols;
      break;
     }
    for (TTagNodeList::const_iterator i = pTableSingleLines->begin(); i != pTableSingleLines->end(); i++)
     {
      bool bLineNotFound = true;
      if (((nTableSingleLineTypeNo == 0) && (NodeToDelete->CmpName("trow"))) ||
        ((nTableSingleLineTypeNo == 1) && (NodeToDelete->CmpName("tcol"))))
       if (find(TableSingleLinesToDelete.begin(), TableSingleLinesToDelete.end(),
         (* i)) != TableSingleLinesToDelete.end())
        bLineNotFound = false;
      if (bLineNotFound)
       for (TTagNodeList::const_iterator j = TableSingleLinesToDelete.begin(); j != TableSingleLinesToDelete.end(); j++)
        {
         TTagNode * ndRules = NULL;
         UnicodeString PolExp = "";
         for (int nRulesNo = 0; nRulesNo < 2; nRulesNo++)
          {
           switch (nRulesNo)
            {
            case 0:
             ndRules = (*i)->GetChildByName("calcrules");
             if (ndRules)
              PolExp = ndRules->GetChildByName("calc")->AV["pol_exp"];
             break;
            case 1:
             ndRules = (*i)->GetChildByName("handrules");
             if (ndRules)
              PolExp = ndRules->AV["pol_cond"];
             break;
            }
           if (ndRules)
            {
             wchar_t * szPolExp = new wchar_t[PolExp.Length() + 1];
             szPolExp[0] = '\0';
             wcscpy(szPolExp, PolExp.c_str());
             bool bRefExist = false;
             wchar_t * p = wcstok(szPolExp, L" ");
             while (p && !bRefExist)
              {
               if (p[0] == '#')
                {
                 UnicodeString Num = p;
                 if (Num.Pos(":")) // ����� ������
                  {
                   if (NodeToDelete->CmpName("trow"))
                    {
                     int nCellRowNo = StrToInt(GetCellNumLexRowNumber(Num));
                     if (nCellRowNo == TableSingleNumRows[* j])
                      bRefExist = true;
                    }
                   else // "tcol"
                    {
                     int nCellColNo = StrToInt(GetCellNumLexColNumber(Num));
                     if (nCellColNo == TableSingleNumCols[* j])
                      bRefExist = true;
                    }
                  }
                 else // ����� ������ �������
                  {
                   if (((nTableSingleLineTypeNo == 0) && (NodeToDelete->CmpName("trow"))) ||
                     ((nTableSingleLineTypeNo == 1) && (NodeToDelete->CmpName("tcol"))))
                    {
                     int nRowColNum = StrToInt(GetRowColNumLexNumber(Num));
                     if (nRowColNum == (* pTableSingleNumLines)[* j])
                      bRefExist = true;
                    }
                  }
                }
               p = wcstok(NULL, L" ");
              }
             delete[]szPolExp;
             if (bRefExist)
              {
               switch (nTableSingleLineTypeNo)
                {
                case 0:
                 if (find(Refers[* j].first->begin(), Refers[* j].first->end(), (* i)) == Refers[* j].first->end())
                  Refers[* j].first->push_back(* i);
                 break;
                case 1:
                 if (find(Refers[* j].second->begin(), Refers[* j].second->end(), (* i)) == Refers[* j].second->end())
                  Refers[* j].second->push_back(* i);
                 break;
                }
               bRefersExist = true;
              }
            }
          }
        }
     }
   }
  if (bRefersExist)
   {
    TStringList * dopinfo;
    try
     {
      dopinfo = new TStringList;
      if (dopinfo)
       {
        for (map<TTagNode *, TTagNodeListPair>::const_iterator i = Refers.begin(); i != Refers.end(); i++)
         {
          if ((*i).second.first->size())
           {
            dopinfo->Text = dopinfo->Text + FMT3(icsDocSpecCanDelColRowDopInfo, LineName1,
              IntToStr((int)(*pTableSingleNumLines)[(*i).first]), "������:\n");
            UnicodeString RefNums = "";
            for (TTagNodeList::const_iterator j = (*i).second.first->begin(); j != (*i).second.first->end(); j++)
             {
              if (RefNums != "")
               RefNums += ",";
              RefNums += IntToStr((int) TableSingleNumRows[*j]);
             }
            dopinfo->Text = dopinfo->Text + RefNums + "\n";
           }
          if ((*i).second.second->size())
           {
            dopinfo->Text = dopinfo->Text + FMT3(icsDocSpecCanDelColRowDopInfo, LineName1,
              IntToStr((int)(*pTableSingleNumLines)[(*i).first]), "�������:\n");
            UnicodeString RefNums = "";
            for (TTagNodeList::const_iterator j = (*i).second.second->begin(); j != (*i).second.second->end(); j++)
             {
              if (RefNums != "")
               RefNums += ",";
              RefNums += IntToStr((int) TableSingleNumCols[*j]);
             }
            dopinfo->Text = dopinfo->Text + RefNums + "\n";
           }
         }
        RMB_SHOW_INFOEX_CPT(cFMT3(icsDocSpecErrorCanDelColRow, LineName0, LineName1, LineName00), ICSName.c_str(),
          dopinfo);
       }
     }
    __finally
     {
      delete dopinfo;
     }
    bCanDelete = false;
   }
  for (TTagNodeList::const_iterator i = TableSingleLinesToDelete.begin(); i != TableSingleLinesToDelete.end(); i++)
   {
    delete Refers[*i].first;
    delete Refers[*i].second;
   }
  return bCanDelete;
 }
// ---------------------------------------------------------------------------
// ����������� ����������� �������� ��������, �� ������� � ��������
// ������������ ��������� �� ��������� ���� ������
bool __fastcall TICSDocSpec::CanDeleteDocRulesRefObj(TTagNode * NodeToDelete)
 {
  bool bCanDelete = true;
  /* TODO -okdv -cInsListRules : ���� �������� ��������� docrules ��� insrules � listrules */
  TTagNode * ndDocRules = FXML->GetChildByName("docrules");
  if (ndDocRules)
   {
    bool bRefersExist = false;
    if (NodeToDelete->CmpName("table")) // tabrules
     {
      TTagNode * ndTabRules = ndDocRules->GetChildByAV("tabrules", "thisref", NodeToDelete->AV["uid"], true);
      if (ndTabRules)
       bRefersExist = true;
     }
    else if (NodeToDelete->CmpName("tcol,trow")) // refpair
     {
      TTagNode * ndRefPair = ndDocRules->GetChildByAV("refpair", "thisref", NodeToDelete->AV["uid"], true);
      if (ndRefPair)
       bRefersExist = true;
     }
    if (bRefersExist)
     {
      if (_MSG_QUE(FMT(icsDocSpecMsgCanDeleteDocRulesRefObj), ICSName) == mrNo)
       bCanDelete = false;
     }
   }
  return bCanDelete;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::CanDelete(TTreeNode * TreeNode)
 {
  bool bCanDelete = inherited::CanDelete(TreeNode);
  if (bCanDelete)
   {
    TTagNode * ndNodeToDelete = reinterpret_cast<TTagNode *>(TreeNode->Data);
    // ������ �� �������� �����/�������� �������, �� ������� ���� ������
    if (ndNodeToDelete->CmpName("tcol,trow"))
     bCanDelete = CanDeleteTableRowCol(ndNodeToDelete);
    if (bCanDelete)
     {
      // ����������� ����������� �������� ��������, �� ������� � ��������
      // ������������ ��������� �� ��������� ���� ������
      bCanDelete = CanDeleteDocRulesRefObj(ndNodeToDelete);
     }
   }
  return bCanDelete;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::BeforeCurrentNodeDelete()
 {
  TTagNode * ndNodeToDelete = GetCurrentTagNode();
  if (ndNodeToDelete->GetParent("list"))
   {
    // ������ "sortorder"
    TTagNode * ndSortOrder = ndNodeToDelete->GetParent("list")->GetChildByName("sortorder");
    if (ndSortOrder)
     {
      TUnicodeStringList ListColToDelete;
      ndNodeToDelete->_Iterate(GetListColToDelete, reinterpret_cast<void *>(& ListColToDelete));
      if (ListColToDelete.size())
       {
        TTagNode * ndSortEl = ndSortOrder->GetFirstChild();
        while (ndSortEl)
         {
          TUnicodeStringList::const_iterator i = find(ListColToDelete.begin(), ListColToDelete.end(),
            ndSortEl->AV["ref"]);
          TTagNode * ndNextSortEl = ndSortEl->GetNext();
          if (i != ListColToDelete.end())
           {
            delete ndSortEl;
            FModified = true;
           }
          ndSortEl = ndNextSortEl;
         }
        if (!ndSortOrder->Count)
         {
          delete ndSortOrder;
          FModified = true;
         }
       }
     }
   }
  // ������ docrules
  /* TODO -okdv -cInsListRules : ���� �������� ������ docrules ��� insrules � listrules */
  TTagNode * ndDocRules = FXML->GetChildByName("docrules");
  if (ndDocRules)
   {
    if (ndNodeToDelete->CmpName("table")) // ������ tabrules
     {
      TTagNode * ndCreateStep = ndDocRules->GetFirstChild();
      while (ndCreateStep)
       {
        TTagNode * ndTabRules;
#pragma option push -w-pia
        while (ndTabRules = ndCreateStep->GetChildByAV("tabrules", "thisref", ndNodeToDelete->AV["uid"], false))
#pragma option pop
         {
          delete ndTabRules;
          FModified = true;
         }
        if (!ndCreateStep->Count)
         {
          TTagNode * tmp = ndCreateStep;
          ndCreateStep = ndCreateStep->GetNext();
          delete tmp;
          FModified = true;
         }
        else
         ndCreateStep = ndCreateStep->GetNext();
       }
     }
    else if (ndNodeToDelete->CmpName("tcol,trow")) // ������ refpair
     {
      TTagNodeList TableSingleLinesToDelete; // ������ "��������" �����/�������� ������� ����������
      // ��������
      if (ndNodeToDelete->CmpName("tcol"))
       GetTableSingleColsList(ndNodeToDelete, & TableSingleLinesToDelete);
      else // "trow"
         GetTableSingleRowsList(ndNodeToDelete, & TableSingleLinesToDelete);
      if (TableSingleLinesToDelete.size())
       {
        TTagNode * ndCreateStep = ndDocRules->GetFirstChild();
        while (ndCreateStep)
         {
          TTagNode * ndObjRules = ndCreateStep->GetFirstChild();
          while (ndObjRules)
           {
            bool bDeleteObjRules = false;
            if (ndObjRules->CmpName("tabrules"))
             {
              UnicodeString RefPairParentNames[] =
               {
                "colrules",
                "rowrules"
               };
              for (int i = 0; i < 2; i++)
               {
                TTagNode * ndRefPair = ndObjRules->GetChildByName(RefPairParentNames[i])->GetFirstChild();
                while (ndRefPair)
                 {
                  TTagNodeList::iterator i = find_if(TableSingleLinesToDelete.begin(), TableSingleLinesToDelete.end(),
                    ICUtils::compose1(bind2nd(equal_to<UnicodeString>(), ndRefPair->AV["thisref"]), SelectTagNodeUID()));
                  if (i != TableSingleLinesToDelete.end())
                   {
                    TTagNode * tmp = ndRefPair;
                    ndRefPair = ndRefPair->GetNext();
                    delete tmp;
                    FModified = true;
                   }
                  else
                   ndRefPair = ndRefPair->GetNext();
                 }
                if (!ndObjRules->GetChildByName(RefPairParentNames[i])->Count)
                 {
                  bDeleteObjRules = true;
                  break;
                 }
               }
             }
            if (bDeleteObjRules)
             {
              TTagNode * tmp = ndObjRules;
              ndObjRules = ndObjRules->GetNext();
              delete tmp;
              FModified = true;
             }
            else
             ndObjRules = ndObjRules->GetNext();
           }
          if (!ndCreateStep->Count)
           {
            TTagNode * tmp = ndCreateStep;
            ndCreateStep = ndCreateStep->GetNext();
            delete tmp;
            FModified = true;
           }
          else
           ndCreateStep = ndCreateStep->GetNext();
         }
       }
     }
    if (!ndDocRules->Count)
     {
      delete ndDocRules;
      FModified = true;
     }
   }
  // ������ �������� ��������� "������" ��� listcol ���� ListColSingle2
  if (IsListColSingle2(ndNodeToDelete))
   if (DeleteListColSingle2SizeAttr(ndNodeToDelete, "width"))
    FModified = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AfterCurrentNodeDelete(UnicodeString DeletedNodeName)
 {
  // ���������� ��������� ����� � �������� ������� ������� �������������
  if ((DeletedNodeName == "trow") || (DeletedNodeName == "tcol"))
   if (FEnumTableRowsCols)
    {
     TTagNode * ndTE;
     if (DeletedNodeName == "trow")
      ndTE = (GetCurrentTagNode()->CmpName("rows")) ? GetCurrentTagNode() : GetCurrentTagNode()->GetParent("rows");
     else
      ndTE = (GetCurrentTagNode()->CmpName("columns")) ? GetCurrentTagNode() :
        GetCurrentTagNode()->GetParent("columns");
     TTreeNode * TENode = reinterpret_cast<TTreeNode *>(ndTE->Ctrl);
     TTagNode * ndCur = GetCurrentTagNode();
     FEditTV->Items->BeginUpdate();
     CreateTableRowColNumMaps(ndTE->GetParent("table"), (DeletedNodeName == "trow") ? teRow : teCol);
     TENode->DeleteChildren();
     LoadXML(ndTE, TENode);
     FEditTV->Selected = reinterpret_cast<TTreeNode *>(ndCur->Ctrl);
     FEditTV->Items->EndUpdate();
    }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::DropTreeNodesAfterLoadXML(TTreeNode * XMLLogicRoot)
 {
  TTreeView * TreeView = dynamic_cast<TTreeView *>(XMLLogicRoot->TreeView);
  TreeView->Items->BeginUpdate();
  TTreeNode * TreeNode = XMLLogicRoot;
  while (TreeNode && ((TreeNode == XMLLogicRoot) || ((TreeNode != XMLLogicRoot) && (TreeNode->Level >
    XMLLogicRoot->Level))))
   {
    TTagNode * TagNode = reinterpret_cast<TTagNode *>(TreeNode->Data);
    TTreeNode * NextNode;
    if (TagNode->CmpName("condrules,sortorder,count,calcceillist,calcrules,handrules,periodfields") ||
      ((TagNode->CmpName("is_attr")) && (TagNode->GetParent()->CmpName("listcol"))))
     {
      TTreeNode * PrevNode = TreeNode->GetPrev();
      UpdateTagNodeCtrl(TreeNode, true);
      if (PrevNode)
       {
        TreeNode->Delete();
        NextNode = PrevNode->GetNext();
       }
      else
       {
        NextNode = TreeNode->getNextSibling();
        TreeNode->Delete();
       }
     }
    else
     NextNode = TreeNode->GetNext();
    TreeNode = NextNode;
   }
  TreeNode = XMLLogicRoot;
  while (TreeNode && ((TreeNode == XMLLogicRoot) || ((TreeNode != XMLLogicRoot) && (TreeNode->Level >
    XMLLogicRoot->Level))))
   {
    TTagNode * TagNode = reinterpret_cast<TTagNode *>(TreeNode->Data);
    if ((TagNode->CmpName("list")) || ((TagNode->CmpName("listcol")) && (TagNode->GetFirstChild()->CmpName("link_is"))))
     {
      for (int i = 0; i < TreeNode->getFirstChild()->Count; i++)
       TreeNodesAssign(dynamic_cast<TTreeView *>(TreeNode->TreeView)->Items->AddChild(TreeNode, ""),
        TreeNode->getFirstChild()->Item[i]);
      UpdateTagNodeCtrl(TreeNode->getFirstChild(), true);
      TreeNode->getFirstChild()->Delete();
     }
    TreeNode = TreeNode->GetNext();
   }
  TreeView->Items->EndUpdate();
 }
// ---------------------------------------------------------------------------
/*
 * ������������ �������� ������� ����� � �������� ������ �������������
 *
 *
 * ���������:
 *   [in]  ndTable - �������� �������, ��� ������� ���������� ��������� �������
 *   [in]  ATE     - ���������� ��� ����� ��������� ������� ������� ������� �������
 *                   [default = teBoth]
 *
 */
void __fastcall TICSDocSpec::CreateTableRowColNumMaps(TTagNode * ndTable, TTableElement ATE)
 {
  if (ndTable && (ndTable->CmpName("table")) && FEnumTableRowsCols)
   {
    if ((ATE == teRow) || (ATE == teBoth))
     {
      TTagNodeList TableSingleRows;
      GetTableSingleRowsList(ndTable->GetChildByName("rows"), & TableSingleRows);
      TTableRowColNumMap TableRowNumMap;
      int j = 1;
      for (TTagNodeList::iterator i = TableSingleRows.begin(); i != TableSingleRows.end(); i++)
       TableRowNumMap[*i] = j++ ;
      FTableRowNumMaps[ndTable] = TableRowNumMap;
     }
    if ((ATE == teCol) || (ATE == teBoth))
     {
      TTagNodeList TableSingleCols;
      GetTableSingleColsList(ndTable->GetChildByName("columns"), & TableSingleCols);
      TTableRowColNumMap TableColNumMap;
      int j = 1;
      for (TTagNodeList::iterator i = TableSingleCols.begin(); i != TableSingleCols.end(); i++)
       TableColNumMap[*i] = j++ ;
      FTableColNumMaps[ndTable] = TableColNumMap;
     }
   }
 }
// ---------------------------------------------------------------------------
// ����������� ������������� �� TreeView
void __fastcall TICSDocSpec::XMLToTree(TTagNode * TagNode, TTreeNode * TreeNode)
 {
  inherited::XMLToTree(TagNode, TreeNode);
  // ������������ �������� ������� ����� � �������� ������ �������������
  if (TagNode->CmpName("table"))
   CreateTableRowColNumMaps(TagNode);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::GetXMLNodeViewText(TTagNode * TagNode)
 {
  UnicodeString tmpText = TagNode->Name;
  if (TagNode->CmpName("doccontent")) // ���� ��������
   {
    tmpText = FXMLTitle;
   }
  else if (TagNode->CmpName("inscription")) // �������
   {
    tmpText = cFMT(icsDocSpecInscViewText);
   }
  else if (TagNode->CmpName("instext")) // ������� �������
   {
    tmpText = GetInsTagViewText(TagNode, GetXMLContainer());
   }
  else if (TagNode->CmpName("list")) // ������
   {
    tmpText = FMT1(icsDocSpecListViewText, GetXMLContainer()->GetRefer(TagNode->AV["ref"])->AV["name"]);
   }
  else if (TagNode->CmpName("table")) // �������
   {
    tmpText = FMT1(icsDocSpecTableViewText, GetXMLContainer()->GetRefer(TagNode->AV["ref"])->AV["name"]);
   }
  // ������ �������� ������, ������� ������
  else if (TagNode->CmpName("listcolgroup,listcol"))
   {
    tmpText = TagNode->AV["name"];
    if (IsListColSingle1Count(TagNode))
     tmpText += csCountViewText;
   }
  // ������ �������, ������� �������
  else if (TagNode->CmpName("trow,tcol"))
   {
    tmpText = "";
    if (FEnumTableRowsCols)
     {
      const TTableRowColNumMap & TableRowColNumMap = (TagNode->CmpName("trow")) ?
        FTableRowNumMaps[TagNode->GetParent("table")] : FTableColNumMaps[TagNode->GetParent("table")];
      TTableRowColNumMap::const_iterator i = TableRowColNumMap.find(TagNode);
      if (i != TableRowColNumMap.end())
       tmpText += "[" + IntToStr((int)(*i).second) + "] ";
     }
    tmpText += TagNode->AV["name"];
   }
  // ������� ��������� ��������
  else if ((TagNode->CmpName("is_attr")) && (TagNode->GetParent()->CmpName("link_is")))
   {
    if (TTagNode * ndRef = (TagNode->AV["ref"] == "") ? NULL : GetXMLContainer()->GetRefer(TagNode->AV["ref"]))
     tmpText = ndRef->GetParent()->GetParent()->AV["name"] + " (" + ndRef->AV["name"] + ")";
    else
     tmpText = EMPTY_LCOL;
   }
  else if (TagNode->CmpName("rows")) // ������
   {
    tmpText = FMT(icsDocSpecGetXMLNodeViewTextRow);
   }
  else if (TagNode->CmpName("columns")) // �������
   {
    tmpText = FMT(icsDocSpecGetXMLNodeViewTextCol);
   }
  return tmpText;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::GetInsTagViewText(TTagNode * ndInsText, TAxeXMLContainer * AXMLContainer,
  PINS_VIEW_TEXT pVT)
 {
  UnicodeString Result = "";
  if (!ndInsText)
   throw DKClasses::ENullArgument(__FUNC__, "ndInsText");
  if (!AXMLContainer)
   throw DKClasses::ENullArgument(__FUNC__, "AXMLContainer");
  if (pVT)
   {
    pVT->Value = "";
    pVT->Ref   = "";
    pVT->Count = "";
   }
  // ����������� �����
  if (ndInsText->AV["value"] != "")
   {
    Result = ndInsText->AV["value"];
    if (pVT)
     pVT->Value = ndInsText->AV["value"];
   }
  // ������
  if (ndInsText->AV["ref"] != "")
   {
    if (Result != "")
     Result += "   +   ";
    TTagNode * ndRef = (_GUI(ndInsText->AV["ref"]) == "") ? ndInsText->GetRefer("ref") :
      AXMLContainer->GetRefer(ndInsText->AV["ref"]);
    // ������ �� ������� �������� TechIS ��� DocIS
    if (ndRef)
     {
      UnicodeString tmp = "";
      if (ndRef->CmpName("ATTR") && ndRef->GetParent()->GetParent()->CmpName("TechIS,DocIS"))
       tmp = (ndRef->GetParent()->GetParent()->AV["name"] + " (" + ndRef->AV["name"] + ")");
      // ������ �� ������� ������� ( ����������� condrules ������������� ) IERef
      else if (ndRef->CmpName("IERef"))
       tmp = AXMLContainer->GetRefer(ndRef->AV["ref"])->AV["name"];
      if (tmp != "")
       {
        Result += tmp;
        if (pVT)
         pVT->Ref = tmp;
       }
     }
   }
  // ������� ����������
  else
   {
    TTagNode * ndCount = ndInsText->GetChildByName("count");
    if (ndCount)
     {
      if (Result != "")
       Result += "   +   ";
      UnicodeString tmp = AXMLContainer->GetRefer(ndCount->AV["ref"])->AV["name"];
      Result += (FMT1(icsDocSpecGetCountCaption, tmp));
      if (pVT)
       pVT->Count = tmp;
     }
   }
  // �����������
  if (Result == "")
   Result = csEmptyInsText;
  return Result;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::UpdateStyleProp(TTagNode * TagNode, UnicodeString AAttrName, bool UpdatePropValue)
 {
  TTagNode * ndStyle = TagNode->GetRefer(AAttrName);
  UnicodeString Text = (ndStyle) ? ndStyle->AV["name"] : static_cast<UnicodeString>("");
  if (UpdatePropValue)
   FPropertyEditor->Values[AAttrName] = Text;
  return Text;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AppendStyleProp(TTagNode * TagNode, UnicodeString AAttrName, UnicodeString APropName)
 {
  int nRowInd;
  nRowInd = FPropertyEditor->InsertRow(AAttrName, APropName, UpdateStyleProp(TagNode, AAttrName), true, reEnabled,
    rrImplied, roNormal, esPickList);
  FillStyleList(FPropertyEditor->ItemProps[Variant(nRowInd)]->PickList);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::UpdateBinProp(TTagNode * TagNode, UnicodeString AAttrName, bool UpdatePropValue)
 {
  UnicodeString Text = GET_BIN_VAV(TagNode->AV[AAttrName]);
  if (UpdatePropValue)
   FPropertyEditor->Values[AAttrName] = Text;
  return Text;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AppendBinProp(TTagNode * TagNode, UnicodeString AAttrName, UnicodeString APropName)
 {
  int nRowInd;
  nRowInd = FPropertyEditor->InsertRow(AAttrName, APropName, UpdateBinProp(TagNode, AAttrName), true, reEnabled,
    rrRequared, roReadOnly, esPickList);
  FPropertyEditor->ItemProps[Variant(nRowInd)]->PickList->Add(ONE_VAV);
  FPropertyEditor->ItemProps[Variant(nRowInd)]->PickList->Add(ZERO_VAV);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AppendUnionProp(TTagNode * TagNode, UnicodeString AAttrName, UnicodeString APropName)
 {
  AppendBinProp(TagNode, AAttrName, APropName);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::UpdateTabPrtyProp(TTagNode * TagNode, UnicodeString AAttrName,
  bool UpdatePropValue)
 {
  UnicodeString Text = GET_TAB_PRTY_VAV(TagNode->AV[AAttrName]);
  if (UpdatePropValue)
   FPropertyEditor->Values[AAttrName] = Text;
  return Text;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AppendTabPrtyProp(TTagNode * TagNode, UnicodeString AAttrName, UnicodeString APropName)
 {
  int nRowInd;
  nRowInd = FPropertyEditor->InsertRow(AAttrName, APropName, UpdateTabPrtyProp(TagNode, AAttrName), true, reEnabled,
    rrRequared, roReadOnly, esPickList);
  FPropertyEditor->ItemProps[Variant(nRowInd)]->PickList->Add(TAB_PRTY_ROW_VAV);
  FPropertyEditor->ItemProps[Variant(nRowInd)]->PickList->Add(TAB_PRTY_COL_VAV);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::UpdateTextProp(TTagNode * TagNode, UnicodeString AAttrName, bool UpdatePropValue)
 {
  UnicodeString Text = TagNode->AV[AAttrName];
  if (UpdatePropValue)
   FPropertyEditor->Values[AAttrName] = Text;
  return Text;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AppendTextProp(TTagNode * TagNode, UnicodeString AAttrName, UnicodeString APropName,
  TDKPropertyEditorRowReq ARequared)
 {
  FPropertyEditor->InsertRow(AAttrName, APropName, UpdateTextProp(TagNode, AAttrName), true, reEnabled, ARequared,
    roNormal, esSimple);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AppendIntProp(TTagNode * TagNode, UnicodeString AAttrName, UnicodeString APropName,
  TDKPropertyEditorRowReq ARequared)
 {
  AppendTextProp(TagNode, AAttrName, APropName, ARequared);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::UpdateChildNodeProp(TTagNode * TagNode, UnicodeString AChildNodeName,
  bool UpdatePropValue)
 {
  UnicodeString Text = (TagNode && TagNode->GetChildByName(AChildNodeName)) ? csNotEmptyEllipsiseEditor :
    csEmptyEllipsiseEditor;
  if (UpdatePropValue)
   FPropertyEditor->Values[AChildNodeName] = Text;
  return Text;
  /*
   � ������� TagNode && TagNode->GetChildByName( AChildNodeName )
   ������������ "TagNode &&", �.�. ��������, ��������������� ��������� ���� ( child node property )
   �� ����� ���� ����� ���� ��������� � ��������� ���� ��������� ���� TagNode'�
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AppendChildNodeProp(TTagNode * TagNode, UnicodeString AChildNodeName,
  UnicodeString APropName, TDKPropertyEditorRowEn AEnabled)
 {
  FPropertyEditor->InsertRow(AChildNodeName, APropName, UpdateChildNodeProp(TagNode, AChildNodeName), true, AEnabled,
    rrImplied, roReadOnly, esEllipsis);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::UpdateAttrExtProp(TTagNode * TagNode, UnicodeString AAttrName,
  bool UpdatePropValue)
 {
  UnicodeString Text = (TagNode->AV[AAttrName] == "") ? csEmptyEllipsiseEditor : csNotEmptyEllipsiseEditor;
  if (UpdatePropValue)
   FPropertyEditor->Values[AAttrName] = Text;
  return Text;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AppendAttrExtProp(TTagNode * TagNode, UnicodeString AAttrName, UnicodeString APropName,
  TDKPropertyEditorRowEn AEnabled)
 {
  FPropertyEditor->InsertRow(AAttrName, APropName, UpdateAttrExtProp(TagNode, AAttrName), true, AEnabled, rrImplied,
    roReadOnly, esEllipsis);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::UpdateValRefProp(UnicodeString AAttrName, UnicodeString AValue,
  bool UpdatePropValue)
 {
  UnicodeString Text = (AValue == "") ? csEmptyEllipsiseEditor : AValue;
  if (UpdatePropValue)
   FPropertyEditor->Values[AAttrName] = Text;
  return Text;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AppendValRefProp(TTagNode * TagNode, UnicodeString AAttrName, UnicodeString APropName,
  UnicodeString AValue)
 {
  FPropertyEditor->InsertRow(AAttrName, APropName, UpdateValRefProp(AAttrName, AValue), true, reEnabled, rrImplied,
    roReadOnly, esEllipsis);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::UpdateImpRefProp(TTagNode * TagNode, UnicodeString AAttrName,
  bool UpdatePropValue)
 {
  TTagNode * ndRef = NULL;
  if (TagNode->AV["ref"] != "")
   ndRef = GetXMLContainer()->GetRefer(TagNode->AV["ref"]);
  UnicodeString Text = (ndRef) ? ndRef->AV["name"] : csEmptyEllipsiseEditor;
  if (UpdatePropValue)
   FPropertyEditor->Values[AAttrName] = Text;
  return Text;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AppendImpRefProp(TTagNode * TagNode, UnicodeString AAttrName, UnicodeString APropName)
 {
  FPropertyEditor->InsertRow(AAttrName, APropName, UpdateImpRefProp(TagNode, AAttrName), true, reEnabled, rrImplied,
    roReadOnly, esEllipsis);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::UpdateReqRefProp(TTagNode * TagNode, UnicodeString AAttrName,
  bool UpdatePropValue)
 {
  UnicodeString Text = GetXMLContainer()->GetRefer(TagNode->AV["ref"])->AV["name"];
  if (UpdatePropValue)
   FPropertyEditor->Values[AAttrName] = Text;
  return Text;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AppendReqRefProp(TTagNode * TagNode, UnicodeString AAttrName, UnicodeString APropName)
 {
  FPropertyEditor->InsertRow(AAttrName, APropName, UpdateReqRefProp(TagNode, AAttrName), true, reDisabled, rrRequared,
    roReadOnly, esEllipsis);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::UpdateCalcRulesProp(TTagNode * TagNode, UnicodeString AChildNodeName,
  bool UpdatePropValue)
 {
  TTagNode * CalcRulesNode = TagNode->GetChildByName(AChildNodeName);
  UnicodeString Text = (CalcRulesNode) ? CalcRulesNode->GetChildByName("calc")->AV["expression"] :
    static_cast<UnicodeString>("");
  if (UpdatePropValue)
   FPropertyEditor->Values[AChildNodeName] = Text;
  return Text;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AppendCalcRulesProp(TTagNode * TagNode, UnicodeString AChildNodeName,
  UnicodeString APropName, TDKPropertyEditorRowEn AEnabled)
 {
  FPropertyEditor->InsertRow(AChildNodeName, APropName, UpdateCalcRulesProp(TagNode, AChildNodeName), true, AEnabled,
    rrImplied, roReadOnly, esEllipsis);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::UpdateHandRulesProp(TTagNode * TagNode, UnicodeString AChildNodeName,
  bool UpdatePropValue)
 {
  TTagNode * HandRulesNode = TagNode->GetChildByName(AChildNodeName);
  UnicodeString Text = (HandRulesNode) ? HandRulesNode->AV["condition"] : static_cast<UnicodeString>("");
  if (UpdatePropValue)
   FPropertyEditor->Values[AChildNodeName] = Text;
  return Text;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AppendHandRulesProp(TTagNode * TagNode, UnicodeString AChildNodeName,
  UnicodeString APropName, TDKPropertyEditorRowEn AEnabled)
 {
  FPropertyEditor->InsertRow(AChildNodeName, APropName, UpdateHandRulesProp(TagNode, AChildNodeName), true, AEnabled,
    rrImplied, roReadOnly, esEllipsis);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::GetSizeViewText(UnicodeString AAttrName, UnicodeString AAttrVal)
 {
  UnicodeString ViewText = "";
  if (AAttrVal == "")
   ViewText = FMT(icsDocSpecGetSizeViewTextAuto);
  else if (AAttrVal == "100%")
   {
    if (AAttrName.Pos("width"))
     ViewText = FMT(icsDocSpecGetSizeViewTextFullWidth);
    else if (AAttrName.Pos("height"))
     ViewText = FMT(icsDocSpecGetSizeViewTextFullHeight);
   }
  else
   ViewText = FloatToStrF(AAttrVal.ToInt() / 10., ffNumber, 10, 1) + " ��.";
  return ViewText;
 }
// ---------------------------------------------------------------------------
int __fastcall TICSDocSpec::GetListColSingle2Index(const TTagNode * TagNode)
 {
  int nIndex = -1;
  if (IsListColSingle2(TagNode))
   {
    TTagNode * ndISAttr = CC_TNode(TagNode);
    while (ndISAttr && (ndISAttr->CmpName("is_attr")))
     {
      nIndex++ ;
      ndISAttr = ndISAttr->GetPrev();
     }
   }
  return nIndex;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::GetSizeAttrVal(TTagNode * TagNode, UnicodeString AAttrName)
 {
  UnicodeString SizeAttrVal = "";
  if (TagNode->CmpName("list,table,tcol") || IsListColSingle1(TagNode))
   SizeAttrVal = TagNode->AV[AAttrName];
  else if (IsListColSingle2(TagNode))
   {
    int nLColNo = GetListColSingle2Index(TagNode);
    TStringList * SizeValues = NULL;
    try
     {
      SizeValues       = new TStringList;
      SizeValues->Text = StringReplace(TagNode->GetParent()->GetParent()->AV[AAttrName], ";", "\n",
        TReplaceFlags() << rfReplaceAll);
      if ((nLColNo >= 0) && (nLColNo < SizeValues->Count))
       SizeAttrVal = SizeValues->Strings[nLColNo];
     }
    __finally
     {
      if (SizeValues)
       delete SizeValues;
     }
   }
  return SizeAttrVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::SetSizeAttrVal(TTagNode * TagNode, UnicodeString AAttrName, UnicodeString AAttrVal)
 {
  if (TagNode->CmpName("list,table,tcol") || IsListColSingle1(TagNode))
   TagNode->AV[AAttrName] = AAttrVal;
  else if (IsListColSingle2(TagNode))
   {
    int nLColNo = GetListColSingle2Index(TagNode);
    TStringList * SizeValues = NULL;
    try
     {
      SizeValues       = new TStringList;
      SizeValues->Text = StringReplace(TagNode->GetParent()->GetParent()->AV[AAttrName], ";", "\n",
        TReplaceFlags() << rfReplaceAll);
      if ((nLColNo >= 0) && (nLColNo < SizeValues->Count))
       {
        SizeValues->Strings[nLColNo] = AAttrVal;
        TagNode->GetParent()->GetParent()->AV[AAttrName] =
          StringReplace(SizeValues->Text, "\r\n", ";", TReplaceFlags() << rfReplaceAll);
        FModified = true;
       }
     }
    __finally
     {
      if (SizeValues)
       delete SizeValues;
     }
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::InsertListColSingle2SizeAttr(TTagNode * TagNode, UnicodeString AAttrName,
  UnicodeString AAttrVal)
 {
  bool bModified = false;
  if (IsListColSingle2(TagNode))
   {
    int nLColNo = GetListColSingle2Index(TagNode);
    TStringList * SizeValues = NULL;
    try
     {
      SizeValues       = new TStringList;
      SizeValues->Text = StringReplace(TagNode->GetParent()->GetParent()->AV[AAttrName], ";", "\n",
        TReplaceFlags() << rfReplaceAll);
      if (nLColNo >= 0)
       {
        if (nLColNo > SizeValues->Count)
         {
          int nCount = nLColNo - SizeValues->Count + 1;
          for (int i = 0; i < nCount; i++)
           SizeValues->Insert(SizeValues->Count, "");
         }
        SizeValues->Insert(nLColNo, AAttrVal);
        TagNode->GetParent()->GetParent()->AV[AAttrName] =
          StringReplace(SizeValues->Text, "\r\n", ";", TReplaceFlags() << rfReplaceAll);
        bModified = true;
       }
     }
    __finally
     {
      if (SizeValues)
       delete SizeValues;
     }
   }
  return bModified;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::DeleteListColSingle2SizeAttr(TTagNode * TagNode, UnicodeString AAttrName)
 {
  bool bModified = false;
  if (IsListColSingle2(TagNode))
   {
    int nLColNo = GetListColSingle2Index(TagNode);
    TStringList * SizeValues = NULL;
    try
     {
      SizeValues       = new TStringList;
      SizeValues->Text = StringReplace(TagNode->GetParent()->GetParent()->AV[AAttrName], ";", "\n",
        TReplaceFlags() << rfReplaceAll);
      if ((nLColNo >= 0) && (nLColNo < SizeValues->Count))
       {
        SizeValues->Delete(nLColNo);
        TagNode->GetParent()->GetParent()->AV[AAttrName] =
          StringReplace(SizeValues->Text, "\r\n", ";", TReplaceFlags() << rfReplaceAll);
        bModified = true;
       }
     }
    __finally
     {
      if (SizeValues)
       delete SizeValues;
     }
   }
  return bModified;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::SwapListColSingle2SizeAttrVals(TTagNode * TagNode1, TTagNode * TagNode2,
  UnicodeString AAttrName)
 {
  bool bModified = false;
  if (IsListColSingle2(TagNode1) && IsListColSingle2(TagNode2) && (TagNode1->GetParent()->GetParent()
    == TagNode2->GetParent()->GetParent()))
   {
    int nLCol1No = GetListColSingle2Index(TagNode1);
    int nLCol2No = GetListColSingle2Index(TagNode2);
    TStringList * SizeValues = NULL;
    try
     {
      SizeValues       = new TStringList;
      SizeValues->Text = StringReplace(TagNode1->GetParent()->GetParent()->AV[AAttrName], ";", "\n",
        TReplaceFlags() << rfReplaceAll);
      if (((nLCol1No >= 0) && (nLCol1No < SizeValues->Count)) && ((nLCol2No >= 0) && (nLCol2No < SizeValues->Count)))
       {
        SizeValues->Exchange(nLCol1No, nLCol2No);
        TagNode1->GetParent()->GetParent()->AV[AAttrName] =
          StringReplace(SizeValues->Text, "\r\n", ";", TReplaceFlags() << rfReplaceAll);
        bModified = true;
       }
     }
    __finally
     {
      if (SizeValues)
       delete SizeValues;
     }
   }
  return bModified;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocSpec::UpdateSizeProp(TTagNode * TagNode, UnicodeString AAttrName, bool UpdatePropValue)
 {
  UnicodeString Text = GetSizeViewText(AAttrName, GetSizeAttrVal(TagNode, AAttrName));
  if (UpdatePropValue)
   FPropertyEditor->Values[AAttrName] = Text;
  return Text;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AppendSizeProp(TTagNode * TagNode, UnicodeString AAttrName, UnicodeString APropName,
  TDKPropertyEditorRowEn AEnabled)
 {
  FPropertyEditor->InsertRow(AAttrName, APropName, UpdateSizeProp(TagNode, AAttrName), true, AEnabled, rrImplied,
    roReadOnly, esEllipsis);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::ApplyRefProp(TTagNode * TagNode, UnicodeString KeyName, UnicodeString AttrValue)
 {
  TTagNode * ndAttrOwner = TagNode;
  UnicodeString AttrName = KeyName;
  if (KeyName == "is_attr")
   {
    ndAttrOwner = TagNode->GetFirstChild();
    AttrName    = "ref";
    TTagNode * ndRef = (AttrValue == "") ? NULL : GetXMLContainer()->GetRefer(AttrValue);
    FPropertyEditor->Values[KeyName] = (ndRef) ? ndRef->AV["name"] : csEmptyEllipsiseEditor;
    ndAttrOwner->AV[AttrName]        = AttrValue;
    FModified                        = true;
   }
  else // ( KeyName == "ref" )
   {
    ndAttrOwner->AV[AttrName] = AttrValue;
    FModified                 = true;
    if ((TagNode->CmpName("is_attr")) && (TagNode->GetParent()->CmpName("link_is")))
     {
      TTagNode * ndRef = (AttrValue == "") ? NULL : GetXMLContainer()->GetRefer(AttrValue);
      FPropertyEditor->Values[KeyName] = (ndRef) ? ndRef->AV["name"] : csEmptyEllipsiseEditor;
     }
    else // ( TagNode->CmpName("instext") )
     {
      INS_VIEW_TEXT VT;
      GetInsTagViewText(TagNode, GetXMLContainer(), & VT);
      UpdateValRefProp(KeyName, VT.Ref, true);
     }
   }
  UpdateCurTreeNodeText();
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::UpdateCurTreeNodeText()
 {
  FEditTV->Selected->Text = GetXMLNodeViewText(GetCurrentTagNode());
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::ApplyStyleProp(TTagNode * TagNode, UnicodeString AttrName, int ARow,
  UnicodeString AttrValue)
 {
  UnicodeString TrimedAttrValue = Trim(AttrValue);
  FPropertyEditor->Values[AttrName] = TrimedAttrValue;
  TStrings * PickList = FPropertyEditor->ItemProps[Variant(ARow)]->PickList;
  if ((TrimedAttrValue != "") && (PickList->IndexOf(TrimedAttrValue) == -1))
   {
    __ValidateErrorInfo.TagNode   = TagNode;
    __ValidateErrorInfo.AttrName  = AttrName;
    __ValidateErrorInfo.AttrValue = AttrValue;
    __ValidateErrorInfo.Row       = ARow;
    throw DKCustomException(FMT(icsDocSpecErrorApplyStyle), 0, reinterpret_cast<void *>(& __ValidateErrorInfo));
   }
  if (TrimedAttrValue == "")
   TagNode->AV[AttrName] = TrimedAttrValue;
  else
   TagNode->AV[AttrName] = UIDStr(reinterpret_cast<int>(PickList->Objects[PickList->IndexOf(TrimedAttrValue)]));
  FModified = true;
  UpdateCurTreeNodeText();
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::ApplyBinProp(TTagNode * TagNode, UnicodeString AttrName, int ARow, UnicodeString AttrValue)
 {
  TagNode->AV[AttrName] = GET_BIN_AV(AttrValue);
  FModified             = true;
  UpdateCurTreeNodeText();
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::HasListColGroup(TTagNode * ItTag, void * Src1, void * Src2, void * Src3, void * Src4)
 {
  return IsListColGroup(ItTag);
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::_IterateHasListColGroup(TTagNode * ItTag, void * Src1, void * Src2, void * Src3,
  void * Src4)
 {
  return ItTag->_Iterate(HasListColGroup);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::ApplyUnionProp(TTagNode * TagNode, UnicodeString AttrName, int ARow,
  UnicodeString AttrValue)
 {
  UnicodeString BinAV = GET_BIN_AV(AttrValue);
  if ((BinAV == ONE_AV) && TagNode->_Iterate1st(_IterateHasListColGroup))
   {
    __ValidateErrorInfo.TagNode   = TagNode;
    __ValidateErrorInfo.AttrName  = AttrName;
    __ValidateErrorInfo.AttrValue = AttrValue;
    __ValidateErrorInfo.Row       = ARow;
    throw DKCustomException(FMT(icsDocSpecErrorApplyUnion), 0, reinterpret_cast<void *>(& __ValidateErrorInfo));
   }
  ApplyBinProp(TagNode, AttrName, ARow, AttrValue);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::ApplyTextProp(TTagNode * TagNode, UnicodeString AttrName, int ARow,
  UnicodeString AttrValue)
 {
  UnicodeString TrimedAttrValue = Trim(AttrValue);
  FPropertyEditor->Values[AttrName] = TrimedAttrValue;
  if ((TrimedAttrValue == "") && (FPropertyEditor->ItemProps[Variant(ARow)]->Requared))
   {
    __ValidateErrorInfo.TagNode   = TagNode;
    __ValidateErrorInfo.AttrName  = AttrName;
    __ValidateErrorInfo.AttrValue = AttrValue;
    __ValidateErrorInfo.Row       = ARow;
    throw DKCustomException(FMT(icsDocSpecErrorApplyText), 0, reinterpret_cast<void *>(& __ValidateErrorInfo));
   }
  TagNode->AV[AttrName] = TrimedAttrValue;
  FModified = true;
  UpdateCurTreeNodeText();
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::ApplyIntProp(TTagNode * TagNode, UnicodeString AttrName, int ARow, UnicodeString AttrValue)
 {
  UnicodeString TrimedAttrValue = Trim(AttrValue);
  FPropertyEditor->Values[AttrName] = TrimedAttrValue;
  if ((TrimedAttrValue == "") && (FPropertyEditor->ItemProps[Variant(ARow)]->Requared))
   {
    __ValidateErrorInfo.TagNode   = TagNode;
    __ValidateErrorInfo.AttrName  = AttrName;
    __ValidateErrorInfo.AttrValue = AttrValue;
    __ValidateErrorInfo.Row       = ARow;
    throw DKCustomException(FMT(icsDocSpecErrorApplyText), 0, reinterpret_cast<void *>(& __ValidateErrorInfo));
   }
  int n;
  bool bBadInt = false;
  try
   {
    n = StrToInt(TrimedAttrValue);
    if (n < 0)
     bBadInt = true;
   }
  catch (EConvertError &)
   {
    bBadInt = true;
   }
  if (bBadInt)
   {
    __ValidateErrorInfo.TagNode   = TagNode;
    __ValidateErrorInfo.AttrName  = AttrName;
    __ValidateErrorInfo.AttrValue = AttrValue;
    __ValidateErrorInfo.Row       = ARow;
    throw DKCustomException(FMT(icsDocSpecErrorApplyInt), 0, reinterpret_cast<void *>(& __ValidateErrorInfo));
   }
  TagNode->AV[AttrName] = TrimedAttrValue;
  FModified = true;
  UpdateCurTreeNodeText();
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::ApplyTabPrtyProp(TTagNode * TagNode, UnicodeString AttrName, int ARow,
  UnicodeString AttrValue)
 {
  TagNode->AV[AttrName] = GET_TAB_PRTY_AV(AttrValue);
  FModified             = true;
  UpdateCurTreeNodeText();
 }
// ---------------------------------------------------------------------------
/*
 * ������ ������� �������������� ������ ������ (xml-��� condrules)
 *
 *
 * ���������:
 *   [in]     ATitle         - ��������� ��� ������� �������������� ������ ������
 *   [in\out] AParentNode    - ���-�������� ��� condrules'� (����� ���� NULL)
 *   [in]     AInsNode       - ���, ����� ��� ����� �������� ���������� ����������������� condrules (����� ���� NULL)
 *   [in]     IsBefore       - ������� ����, ����� ��� ����� AInsNode �������� ����������������� condrules
 *   [in\out] ACondRulesNode - xml-��� condrules
 *   [in]     AMainISUID     - ��� �������� �������� ������������
 *   [in]     NeedKonkr      - ������� ������������� ������ ������������� ��� ������ ������
 *
 * ������������ ��������:
 *   true  - ������ ������ ������� ���������������
 *   false - �������������� ������ ������ �������� �������������
 *
 */
bool __fastcall TICSDocSpec::EditCondRules(UnicodeString ATitle, TTagNode * AParentNode, TTagNode * AInsNode,
  bool IsBefore, TTagNode ** ACondRulesNode, UnicodeString AMainISUID, bool NeedKonkr)
 {
  bool bResult = false;
  TTagNode * __tmpFlt = NULL;
  TFilterEditExForm * Dlg = NULL;
  try
   {
    __tmpFlt = new TTagNode(NULL);
    Dlg      = new TFilterEditExForm(this);
    UnicodeString flName;
    long Code;
    if (Dlg)
     {
      if (ATitle != "")
       Dlg->Caption = Dlg->Caption + " - " + ATitle;
      Dlg->OnCallDefProc = FOnCallDefProc;
      Dlg->EditType      = fetCondition;
      Dlg->DomainNumber  = DomainNumber;
      Dlg->MainISUID     = AMainISUID;
      Dlg->ParentXML     = FXML;
      Dlg->BeforeDelete  = DocFilterBeforeDelete;
      Dlg->XMLContainer  = XMLContainer;
      if (FNom)
       Dlg->Nom = FNom;
      else
       Dlg->NomSrc = XMLContainer->GetXML(NmklGUI);
      Dlg->CanKonkr = NeedKonkr;
      if (*ACondRulesNode)
       {
        Dlg->Filter = *ACondRulesNode; // edit
       }
      else
       Dlg->New(); // new
      Dlg->OnCustomLoad = FOnLoadFilter;
      Dlg->ShowModal();
      if (Dlg->ModalResult == mrOk)
       {
        // *********************
        if (Dlg->IsEmpty())
         {
          if (*ACondRulesNode)
           {
            if (AParentNode)
             AParentNode->DeleteChild(* ACondRulesNode);
            else
             delete * ACondRulesNode;
            *ACondRulesNode = NULL;
            bResult         = true;
           }
         }
        else
         {
          if (!(*ACondRulesNode))
           {
            if (AParentNode)
             {
              if (AInsNode)
               * ACondRulesNode = AParentNode->InsertChild(AInsNode, "condrules", IsBefore);
              else
               * ACondRulesNode = AParentNode->AddChild("condrules");
             }
            else
             * ACondRulesNode = new TTagNode;
           }
          (* ACondRulesNode)->AssignEx(Dlg->Filter, true);
          bResult = true;
         }
       }
     }
   }
  __finally
   {
    if (Dlg)
     delete Dlg;
    if (__tmpFlt)
     delete __tmpFlt;
   }
  return bResult;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::ClearDropedStyleRef(TTagNode * ItTag, void * ADropedStyles, void * Src2, void * Src3,
  void * Src4)
 {
  TUnicodeStringList * DropedStyles = reinterpret_cast<TUnicodeStringList *>(ADropedStyles);
  UnicodeString StyleAttrNames[] =
   {
    "style",
    "hstyle",
    "bstyle"
   };
  for (int i = 0; i < 3; i++)
   if (ItTag->GetAttrByName(StyleAttrNames[i]))
    {
     TUnicodeStringList::iterator j = find(DropedStyles->begin(), DropedStyles->end(), ItTag->AV[StyleAttrNames[i]]);
     if (j != DropedStyles->end())
      ItTag->AV[StyleAttrNames[i]] = "";
    }
  return false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::ItDocSpecCheckStyleExistence(TTagNode * ItTag, UnicodeString & AStyleUID)
 {
  UnicodeString StyleAttrNames[] =
   {
    "style",
    "hstyle",
    "bstyle"
   };
  for (int i = 0; i < 3; i++)
   if (ItTag->GetAttrByName(StyleAttrNames[i]))
    if (ItTag->CmpAV(StyleAttrNames[i], AStyleUID))
     return true;
  return false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::DocSpecCheckStyleExistence(TObject * Sender, UnicodeString AStyleUID)
 {
  UnicodeString StyleUID = AStyleUID;
  return FXML->GetChildByName("doccontent")->Iterate(ItDocSpecCheckStyleExistence, StyleUID);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::DocSpecGetDocElementsList(TObject * Sender, TStrings * DocElements)
 {
  DocElements->BeginUpdate();
  DocElements->Clear();
  TTreeNode * DocElementNode = FEditTV->Items->GetFirstNode()->getFirstChild();
  while (DocElementNode)
   {
    DocElements->AddObject(DocElementNode->Text, reinterpret_cast<TObject *>(DocElementNode->Data));
    DocElementNode = DocElementNode->getNextSibling();
   }
  DocElements->EndUpdate();
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::DocSpecGetThisDocElementStruct(TObject * Sender, const TTagNode * DocElement,
  TTreeNodes * Struct)
 {
  DocSpecGetRefDocElementStruct(Sender, DocElement, Struct);
  /* Struct->BeginUpdate();
   TTreeNode *DocElementNode = FEditTV->Items->GetFirstNode()->getFirstChild();
   while( DocElementNode )
   {
   if ( reinterpret_cast<TTagNode*>(DocElementNode->Data) == DocElement )
   {
   TTreeNode *ChildNode = DocElementNode->getFirstChild();
   while( ChildNode )
   {
   TTreeNode *NewNode = Struct->AddChild( NULL, "" );
   TICSDocBaseEdit::TreeNodesAssign( NewNode, ChildNode );
   ChildNode = ChildNode->getNextSibling();
   }
   }
   DocElementNode = DocElementNode->getNextSibling();
   }
   Struct->EndUpdate(); */
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::DocSpecGetRefDocElementStruct(TObject * Sender, const TTagNode * DocElement,
  TTreeNodes * Struct)
 {
  Struct->BeginUpdate();
  TTreeNode * FakeNode = Struct->AddChild(NULL, "");
  LoadXML(CC_TNode(DocElement), FakeNode);
  TTreeNode * TreeNode = FakeNode->getFirstChild();
  while (TreeNode)
   {
    TICSDocBaseEdit::TreeNodesAssign(Struct->AddChild(NULL, ""), TreeNode);
    TreeNode = TreeNode->getNextSibling();
   }
  FakeNode->Delete();
  Struct->EndUpdate();
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::DocSpecGetDocElementStruct(TObject * Sender, const TTagNode * DocElement,
  TTreeNodes * Struct)
 {
  Struct->BeginUpdate();
  LoadXML(CC_TNode(DocElement), Struct->AddChild(NULL, ""));
  Struct->EndUpdate();
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::DocSpecGetDocElementViewText(TObject * Sender, const TTagNode * DocElement,
  UnicodeString & ViewText)
 {
  ViewText = GetXMLNodeViewText(CC_TNode(DocElement));
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::EditParameters()
 {
  bool bResult = false;
  TfmParametersEdit * dlg;
  TTagNode * ndPassport = FXML->GetChildByName("passport");
  TTagNode * ndCondRules = FXML->GetChildByName("condrules");
  TTagNode * ndDocRules = FXML->GetChildByName("docrules");
  try
   {
    dlg = new TfmParametersEdit(this, ndPassport, ndCondRules, ndDocRules, FXML);
    if (dlg)
     {
      dlg->DocByDocIcon              = (FDocByDocIcon->Handle) ? FDocByDocIcon : FDefDocByDocIcon;
      dlg->OnGetSpecOwner            = FOnGetSpecOwner;
      dlg->OnEditCondRules           = DocSpecEditCondRules;
      dlg->OnCheckStyleExistence     = DocSpecCheckStyleExistence;
      dlg->OnGetDocElementsList      = DocSpecGetDocElementsList;
      dlg->OnGetThisDocElementStruct = DocSpecGetThisDocElementStruct;
      dlg->OnGetRefDocElementStruct  = DocSpecGetRefDocElementStruct;
      dlg->OnGetDocElementViewText   = DocSpecGetDocElementViewText;
      dlg->OnGetDocElementStruct     = DocSpecGetDocElementStruct;
      dlg->OnGetSpecificatorsList    = FOnGetSpecsList;
      dlg->OnGetOrgsList             = FOnGetOrgsList;
      dlg->OnGetSpecNode             = FOnGetSpecNode;
      if (dlg->ShowModal() == mrOk)
       {
        bResult = true;
        ndPassport->Assign(dlg->Passport, true);
        if (dlg->CondRules)
         {
          if (!ndCondRules)
           ndCondRules = FXML->InsertChild(ndPassport, "condrules", false);
          ndCondRules->Assign(dlg->CondRules, true); // AssignEx
         }
        else
         {
          if (ndCondRules)
           delete ndCondRules;
         }
        if (dlg->DocRules)
         {
          if (!ndDocRules)
           ndDocRules = FXML->InsertChild(FXML->GetChildByName("doccontent"), "docrules", true);
          ndDocRules->Assign(dlg->DocRules, true);
         }
        else
         {
          if (ndDocRules)
           delete ndDocRules;
         }
        // ������ ������ �� ��������� �����
        TUnicodeStringList DropedStyles = dlg->DropedStyles;
        FXML->_Iterate(ClearDropedStyleRef, & DropedStyles);
        FModified = true;
        if (FEditTV->Selected)
         EditTVChange(FEditTV, FEditTV->Selected);
       }
     }
   }
  __finally
   {
    if (dlg)
     delete dlg;
   }
  return bResult;
 }
// ***************************************************************************
// ********************** ����������� ������� ********************************
// ***************************************************************************
void __fastcall TICSDocSpec::FEditTVDblClick(TObject * Sender)
 {
  int t, l;
  TWinControl * FP = FEditTV;
  while (FP->Parent)
   FP = FP->Parent;
  TTreeNode * clkNode = FEditTV->GetNodeAt(Mouse->CursorPos.x - FP->Left - FEditTV->Left,
    Mouse->CursorPos.y - FP->Top - (FP->Height - FP->ClientHeight) - FEditTV->Top);
  if (clkNode)
   {
    if (clkNode->Data)
     {
      ShowMessage(((TTagNode *)clkNode->Data)->Name);
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::PropertyEditorEditButtonClick(TObject * Sender)
 {
  TTagNode * TagNode;
  UnicodeString KeyName = "";
#pragma option push -w-pia
  if (TagNode = GetCurrentTagNode())
#pragma option pop
   KeyName = FPropertyEditor->Keys[FPropertyEditor->Row];
  if (!TagNode || (KeyName == ""))
   return;
  if (KeyName == "condrules") // AppendChildNodeProp
   {
    TTagNode * ndParent = TagNode;
    TTagNode * ndIns = NULL;
    UnicodeString AMainISUID = "";
    UnicodeString ATitle = GetXMLNodeViewText(TagNode);
    if (TagNode->CmpName("list"))
     {
      ndIns = TagNode->GetChildByName("sortorder");
      if (!ndIns)
       ndIns = TagNode->GetChildByName("listcont");
      AMainISUID = _UID(TagNode->AV["ref"]);
     }
    else if (TagNode->CmpName("listcol"))
     {
      if (TTagNode * CountNode = TagNode->GetChildByName("link_is")->GetChildByName("count"))
       {
        ndParent   = CountNode;
        AMainISUID = _UID(CountNode->AV["ref"]);
       }
      else
       {
        ndParent   = TagNode->GetFirstChild();
        ndIns      = TagNode->GetFirstChild()->GetChildByName("is_attr");
        AMainISUID = _UID(TagNode->GetFirstChild()->AV["ref"]);
       }
     }
    else if (TagNode->CmpName("table"))
     {
      ndIns      = TagNode->GetChildByName("rows");
      AMainISUID = _UID(TagNode->AV["ref"]);
     }
    else if (TagNode->CmpName("rows,columns"))
     {
      ndIns      = TagNode->GetChildByName("trow");
      AMainISUID = _UID(TagNode->GetParent("table")->AV["ref"]);
     }
    else // trow,tcol
     {
      ndIns = TagNode->GetChildByName("calcrules");
      if (!ndIns)
       ndIns = TagNode->GetChildByName("handrules");
      if (!ndIns)
       ndIns = TagNode->GetChildByName("trow");
      AMainISUID = _UID(TagNode->GetParent("table")->AV["ref"]);
     }
    TTagNode * ndCondRules = ndParent->GetChildByName("condrules");
    if (EditCondRules(ATitle, // ��������� ��� ������� �������������� ������ ������
      ndParent, // ���-�������� ��� condrules'�
      ndIns, // ���, ����� ������� ���������� ����������������� condrules
      true, // ������� ����, ��� ����������������� condrules ���������� ����� AInsNode
      &ndCondRules, // xml-��� condrules
      AMainISUID, // ��� �������� �������� ������������
      true)) // ������� ������������� ������ ������������� ��� ������ ������
     {
      UpdateChildNodeProp(ndParent, KeyName, true);
      FModified = true;
     }
   }
  else if (KeyName == "sortorder") // AppendChildNodeProp
   {
    TfmListSortOrder * dlg;
    try
     {
      dlg = new TfmListSortOrder(this, TagNode);
      if (dlg)
       {
        TTagNode * ndSortOrder = TagNode->GetChildByName("sortorder");
        dlg->SortOrderTagNode = ndSortOrder;
        dlg->OnColListLoad    = ListSortOrderColListLoad;
        dlg->OnListColUpdate  = ListSortOrderListColUpdate;
        if (dlg->ShowModal() == mrOk)
         {
          if (dlg->SortOrderTagNode)
           {
            if (!ndSortOrder)
             ndSortOrder = TagNode->InsertChild(TagNode->GetChildByName("listcont"), "", true);
            ndSortOrder->Assign(dlg->SortOrderTagNode, true);
            FModified = true;
            UpdateChildNodeProp(TagNode, "sortorder", true);
           }
          else if (ndSortOrder)
           {
            TagNode->DeleteChild(ndSortOrder);
            FModified = true;
            UpdateChildNodeProp(TagNode, "sortorder", true);
           }
         }
       }
     }
    __finally
     {
      if (dlg)
       delete dlg;
     }
   }
  else if ((KeyName == "calcrules") || (KeyName == "handrules")) // AppendChildNodeProp
   {
    UnicodeString CaptionDopInfo;
    TRulesType RulesType;
    TRulesOwnerType RulesOwnerType;
    if (TagNode->CmpName("tcol"))
     {
      CaptionDopInfo = "�������";
      RulesOwnerType = otTableCol;
     }
    else if (TagNode->CmpName("trow"))
     {
      CaptionDopInfo = "������";
      RulesOwnerType = otTableRow;
     }
    RulesType = (KeyName == "calcrules") ? rtCalc : rtHand;
    CaptionDopInfo += (" \"" + TagNode->AV["name"] + "\"");
    TfmRulesExprEdit * dlg;
    try
     {
      dlg = new TfmRulesExprEdit(this, CaptionDopInfo);
      if (dlg)
       {
        dlg->RulesType        = RulesType;
        dlg->RulesOwnerType   = RulesOwnerType;
        dlg->RowNumConstraint = CountTagNodes(TagNode->GetParent("table")->GetChildByName("rows"), IsTableRowSingle);
        dlg->ColNumConstraint = CountTagNodes(TagNode->GetParent("table")->GetChildByName("columns"), IsTableColSingle);
        TTagNode * RulesNode = TagNode->GetChildByName(KeyName);
        if (RulesNode)
         dlg->Expression = (KeyName == "calcrules") ? RulesNode->GetChildByName("calc")->AV["expression"] :
           RulesNode->AV["condition"];
        if (dlg->ShowModal() == mrOk)
         {
          if (dlg->Expression != "")
           {
            if (!RulesNode)
             {
              if (KeyName == "calcrules")
               {
                if (TagNode->GetChildByName("condrules"))
                 RulesNode = TagNode->GetChildByName("condrules")->Insert("calcrules", false);
                else if (TagNode->GetChildByName("handrules"))
                 RulesNode = TagNode->GetChildByName("handrules")->Insert("calcrules", true);
                else
                 RulesNode = TagNode->AddChild("calcrules");
                RulesNode->AV["prty"] = "1";
                RulesNode->AddChild("calc", "expression=,pol_exp=");
               }
              else // handrules
               {
                if (TagNode->GetChildByName("calcrules"))
                 RulesNode = TagNode->GetChildByName("calcrules")->Insert("handrules", false);
                else if (TagNode->GetChildByName("condrules"))
                 RulesNode = TagNode->GetChildByName("condrules")->Insert("handrules", false);
                else
                 RulesNode = TagNode->AddChild("handrules");
                RulesNode->AV["condition"] = "";
                RulesNode->AV["pol_cond"]  = "";
               }
             }
            if (KeyName == "calcrules")
             {
              RulesNode->GetChildByName("calc")->AV["expression"] = dlg->Expression;
              RulesNode->GetChildByName("calc")->AV["pol_exp"] = dlg->PolExp;
              UpdateCalcRulesProp(TagNode, "calcrules", true);
             }
            else
             {
              RulesNode->AV["condition"] = dlg->Expression;
              RulesNode->AV["pol_cond"]  = dlg->PolExp;
              UpdateHandRulesProp(TagNode, "handrules", true);
             }
            FModified = true;
            UpdateActions();
           }
          else if (RulesNode)
           {
            delete RulesNode;
            if (KeyName == "calcrules")
             UpdateCalcRulesProp(TagNode, "calcrules", true);
            else
             UpdateHandRulesProp(TagNode, "handrules", true);
            FModified = true;
            UpdateActions();
           }
         }
       }
     }
    __finally
     {
      if (dlg)
       delete dlg;
     }
   }
  else if ((KeyName == "ref") || (KeyName == "is_attr"))
   { /* get filter dlg */
    if ((TagNode->CmpName("instext")) && TagNode->GetChildByName("count"))
     {
      if (_MSG_ERR(FMT(icsDocSpecCalcCountRefConf), ICSName) == mrNo)
       return;
      delete TagNode->GetChildByName("count");
      UpdateValRefProp("count", "", true);
      UpdateCurTreeNodeText();
      FModified = true;
     }
    TfmSetRef * dlg;
    try
     {
      TTagNode * ndAttrOwner = TagNode;
      UnicodeString AttrName = KeyName;
      dlg                   = new TfmSetRef(this, FPropertyEditor->ItemProps[Variant(FPropertyEditor->Row)]->Requared);
      dlg->OnCanRefUpdate   = SetRefCanRefUpdate;
      dlg->OnLoadRefObjects = SetRefLoadRefObjects;
      dlg->LoadRefObjects();
      if (TagNode->CmpName("is_attr,listcol"))
       {
        dlg->Caption      = FMT(icsDocSpecSetRefDlgCaption);
        dlg->ObjTreeTitle = FMT(icsDocSpecSetRefDlTreegCaption);
       }
      if (KeyName == "is_attr") // ( TagNode->CmpName("listcol") )
       {
        ndAttrOwner = TagNode->GetFirstChild();
        AttrName    = "ref";
       }
      dlg->CurDocGUI = FXML->GetChildByName("passport", false)->AV["GUI"];
      dlg->Ref = ndAttrOwner->AV[AttrName];
      if (dlg->ShowModal() == mrOk)
       ApplyRefProp(TagNode, KeyName, dlg->Ref);
     }
    __finally
     {
      __DELETE_OBJ(dlg)
     }
   }
  else if (KeyName == "count") // AppendValRefProp
   {
    if (TagNode->AV["ref"] != "")
     {
      if (_MSG_QUE(FMT(icsDocSpecCalcCountRefConf), ICSName) == mrNo)
       return;
      ApplyRefProp(TagNode, "ref", "");
     }
    TStringList * ISList = NULL;
    TfmCount * dlg = NULL;
    try
     {
      TTagNode * CountNode = TagNode->GetChildByName("count");
      ISList               = CreateNmklISList();
      dlg                  = new TfmCount(this, CountNode, ISList);
      dlg->OnEditCondRules = EditCountCondRules;
      INS_VIEW_TEXT VT;
      switch (dlg->ShowModal())
       {
       case mrOk:
        if (!CountNode)
         CountNode = TagNode->AddChild();
        CountNode->Assign(dlg->CountNode, true);
        FModified = true;
        GetInsTagViewText(CountNode->GetParent("instext"), GetXMLContainer(), & VT);
        UpdateValRefProp("count", VT.Count, true);
        UpdateCurTreeNodeText();
        break;
       case mrAbort:
        if (CountNode)
         {
          delete CountNode;
          FModified = true;
          UpdateValRefProp("count", "", true);
          UpdateCurTreeNodeText();
         }
        break;
       }
     }
    __finally
     {
      if (ISList)
       delete ISList;
      if (dlg)
       delete dlg;
     }
   }
  else if (KeyName == "width")
   {
    TfmDocElSizeEdit * dlg = NULL;
    try
     {
      dlg           = new TfmDocElSizeEdit(this);
      dlg->SizeType = stWidth;
      if (TagNode->CmpName("list,table"))
       dlg->SizeValidValues = SV_ALL;
      else if ((TagNode->CmpName("tcol")) || IsListCol(TagNode))
       dlg->SizeValidValues = SV_AUTOSIZE | SV_CUSTOM;
      dlg->SizeCustomValueUnit = vuCM;
      UnicodeString KeyVal = GetSizeAttrVal(TagNode, KeyName);
      if (KeyVal == "")
       dlg->SizeValueType = vtAutoSize;
      else if (KeyVal == "100%")
       dlg->SizeValueType = vtFullSize;
      else
       {
        dlg->SizeValueType       = vtCustom;
        dlg->SizeCustomValueAsMM = KeyVal.ToInt();
       }
      if (dlg->ShowModal() == mrOk)
       {
        switch (dlg->SizeValueType)
         {
         case vtAutoSize:
          KeyVal = "";
          break;
         case vtFullSize:
          KeyVal = "100%";
          break;
         case vtCustom:
          KeyVal = IntToStr((int) dlg->SizeCustomValueAsMM);
          break;
         }
        SetSizeAttrVal(TagNode, KeyName, KeyVal);
        UpdateSizeProp(TagNode, "width", true);
        FModified = true;
       }
     }
    __finally
     {
      if (dlg)
       delete dlg;
     }
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::SetRefCanRefUpdate(TObject * Sender, TTagNode * ndRef)
 {
  if (ndRef->CmpName("condrules,RAND,ROR,IAND,IOR,IMOR"))
   return false;
  else if (ndRef->CmpName("IERef"))
   {
    bool IsChildOfISec = false;
    TTagNode * ndParent = ndRef->GetParent();
    while (ndParent)
     {
      if (ndParent->CmpName("IAND,IOR,IMOR"))
       {
        IsChildOfISec = true;
        break;
       }
      ndParent = ndParent->GetParent();
     }
    return (!IsChildOfISec);
   }
  else
   return (ndRef->GetAttrByName("uid"));
 }
// ---------------------------------------------------------------------------
// �������� �������� ��� ������ � TreeView ����� TfmSetRef
void __fastcall TICSDocSpec::SetRefLoadRefObjects(TObject * Sender, TTreeView * tv)
 {
  TTagNode * TagNode = GetCurrentTagNode();
  UnicodeString KeyName = FPropertyEditor->Keys[FPropertyEditor->Row];
  // ������
  TTreeNode * RootNode = tv->Items->AddObject(NULL, cFMT(icsDocSpecSetRefRefObjCaption), NULL);
  if (KeyName == "ref")
   {
    // ������� �������
    if (TagNode->CmpName("instext"))
     {
      // �������� ��������� ������������
      if (TTagNode * ndTechnounit = GetNmkl()->GetChildByName("technounit", false))
       {
        // �������� �������� ������������, ����������� ��������� ���������
        TTreeNode * DocISNode = tv->Items->AddChildObject(RootNode, cFMT(icsDocSpecSetRefDocParamCaption), NULL);
        TTagNode * ndAttr = ndTechnounit->GetChildByName("DocIS")->GetChildByName("ATTRS")->GetFirstChild();
        while (ndAttr)
         {
          tv->Items->AddChildObject(DocISNode, ndAttr->AV["name"], ndAttr);
          ndAttr = ndAttr->GetNext();
         }
        // �������� ��������� ������������ �� ������� ��������������� ��������
        TTreeNode * TechISNodes = tv->Items->AddChildObject(RootNode, cFMT(icsDocSpecSetRefTechInfoCaption), NULL);
        TTagNode * ndTechIS = ndTechnounit->GetChildByName("TechIS");
        while (ndTechIS)
         {
          TTreeNode * TechISNode = tv->Items->AddChildObject(TechISNodes, ndTechIS->AV["name"], NULL);
          TTagNode * ndAttr = ndTechIS->GetChildByName("ATTRS")->GetFirstChild();
          while (ndAttr)
           {
            tv->Items->AddChildObject(TechISNode, ndAttr->AV["name"], ndAttr);
            ndAttr = ndAttr->GetNext();
           }
          ndTechIS = ndTechIS->GetNext();
         }
       }
      // �������� ����������� ������� �������������
      if (FXML->GetChildByName("condrules", false))
       {
        TFilterEditExForm * Dlg = NULL;
        try
         {
          Dlg                = new TFilterEditExForm(this);
          Dlg->OnCallDefProc = FOnCallDefProc;
          Dlg->EditType      = fetCondition;
          Dlg->DomainNumber  = DomainNumber;
          Dlg->MainISUID     = "";
          Dlg->ParentXML     = FXML;
          Dlg->BeforeDelete  = DocFilterBeforeDelete;
          Dlg->XMLContainer  = XMLContainer;
          if (FNom)
           Dlg->Nom = FNom;
          else
           Dlg->NomSrc = XMLContainer->GetXML(NmklGUI);
          Dlg->CanKonkr = false;
          Dlg->Filter   = FXML->GetChildByName("condrules", false); // edit
          TTreeNode * CondRulesNodes = tv->Items->AddChildObject(RootNode, "", NULL);
          CxToTreeNodesAssign(CondRulesNodes, Dlg->FRulesTV->Root->getFirstChild());
          CondRulesNodes->Text = "������";
         }
        __finally
         {
          if (Dlg)
           delete Dlg;
         }
       }
     }
    else // ( TagNode->CmpName("is_attr") )
     {
      TTagNode * ndAttr = GetXMLContainer()->GetRefer(TagNode->GetParent()->AV["ref"])->GetChildByName("ATTRS")
        ->GetFirstChild();
      while (ndAttr)
       {
        tv->Items->AddChildObject(RootNode, ndAttr->AV["name"], ndAttr);
        ndAttr = ndAttr->GetNext();
       }
     }
   }
  else // ( KeyName == "is_attr" ) && ( TagNode->CmpName("listcol") )
   {
    TTagNode * ndAttr = GetXMLContainer()->GetRefer(TagNode->GetParent("list")->AV["ref"])->GetChildByName("ATTRS")
      ->GetFirstChild();
    while (ndAttr)
     {
      tv->Items->AddChildObject(RootNode, ndAttr->AV["name"], ndAttr);
      ndAttr = ndAttr->GetNext();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::CxToTreeNodesAssign(TTreeNode * Dest, TcxTreeListNode * Src)
 {
  if (!Dest)
   throw DKClasses::ENullArgument(__FUNC__, "Dest");
  if (!Src)
   throw DKClasses::ENullArgument(__FUNC__, "Src");
  TTreeView * TreeView = dynamic_cast<TTreeView *>(Dest->TreeView);
  TreeView->Items->BeginUpdate();
  Dest->Text = Src->Texts[0];
  Dest->Data = NULL;
  if (Src->Data)
   {
    if (((TTagNode *)Src->Data)->AV["uid"].Length())
     Dest->Data = FXML->GetTagByUID(((TTagNode *)Src->Data)->AV["uid"]);
   }
  Dest->StateIndex = Src->StateIndex;
  UpdateTagNodeCtrl(Dest);
  Dest->DeleteChildren();
  for (int i = 0; i < Src->Count; i++)
   {
    CxToTreeNodesAssign(dynamic_cast<TTreeView *>(Dest->TreeView)->Items->AddChild(Dest, ""), Src->Items[i]);
   }
  TreeView->Items->EndUpdate();
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::EditCountCondRules(TObject * Sender, UnicodeString ATitle, TTagNode * AParentNode,
  TTagNode * ACondRulesNode, UnicodeString AMainISUID)
 {
  return EditCondRules(ATitle, // ��������� ��� ������� �������������� ������ ������
    AParentNode, // ���-�������� ��� condrules'�
    NULL, // xml-��� condrules ���������� � ����� ������ �������� ����� ��� AParentNode
    false, // ������������
    & ACondRulesNode, // xml-��� condrules
    AMainISUID, // ��� �������� �������� ������������
    true // ������� ������������� ������ ������������� ��� ������ ������
    );
 }
// ---------------------------------------------------------------------------
int __fastcall TICSDocSpec::ListSortOrderColListLoad(TObject * Sender, const TTagNode * ndList, TTreeNodes * TreeNodes)
 {
  TreeNodes->Clear();
  LoadXML(const_cast<TTagNode *>(ndList), TreeNodes->AddObject(NULL, "", NULL));
  return TreeNodes->Count;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::ListSortOrderListColUpdate(TObject * Sender, const TTagNode * ndListCol,
  TTreeNode * ListColNode)
 {
  LoadXML(const_cast<TTagNode *>(ndListCol), ListColNode);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::PropertyEditorValidateError(TObject * Sender, Exception & E)
 {
  if (!E.ClassNameIs("DKCustomException"))
   throw E;
  Application->ShowException(& E);
  PVALIDATE_ERROR_INFO pVEI = reinterpret_cast<PVALIDATE_ERROR_INFO>(dynamic_cast<DKCustomException &>(E).DopInfo);
  UnicodeString OldAttrValue = pVEI->TagNode->AV[pVEI->AttrName];
  if (((pVEI->AttrName == "style") || (pVEI->AttrName == "bstyle") || (pVEI->AttrName == "hstyle")) &&
    (OldAttrValue != ""))
   {
    TStringList * tmp = new TStringList;
    FillStyleList(tmp);
    OldAttrValue = tmp->Strings[tmp->IndexOfObject(reinterpret_cast<TObject *>(UIDInt(OldAttrValue)))];
    delete tmp;
   }
  else if (pVEI->AttrName == "union")
   OldAttrValue = GET_BIN_VAV(OldAttrValue);
  FPropertyEditor->Values[pVEI->AttrName] = OldAttrValue;
  FPropertyEditor->EditList->SelectAll();
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::PropertyEditorValidate(TObject * Sender, int ACol, int ARow, const UnicodeString KeyName,
  const UnicodeString KeyValue)
 {
  TTagNode * TagNode = NULL;
  if (KeyName != "")
   TagNode = GetCurrentTagNode();
  if (!TagNode)
   return;
  if (TagNode->CmpName("inscription")) // �������
   {
    if (KeyName == "style")
     ApplyStyleProp(TagNode, KeyName, ARow, KeyValue);
    else if (KeyName == "newline")
     ApplyBinProp(TagNode, KeyName, ARow, KeyValue);
   }
  else if (TagNode->CmpName("instext")) // ������� �������
   {
    if (KeyName == "style")
     ApplyStyleProp(TagNode, KeyName, ARow, KeyValue);
    else if (KeyName == "newline")
     ApplyBinProp(TagNode, KeyName, ARow, KeyValue);
    else if (KeyName == "value")
     ApplyTextProp(TagNode, KeyName, ARow, KeyValue);
   }
  else if (TagNode->CmpName("list")) // ������
   {
    if (KeyName == "showzval")
     ApplyBinProp(TagNode, KeyName, ARow, KeyValue);
    else if (KeyName == "border")
     ApplyIntProp(TagNode, KeyName, ARow, KeyValue);
    else if (KeyName == "rownumbers")
     ApplyBinProp(TagNode, KeyName, ARow, KeyValue);
    else if ((KeyName == "hstyle") || (KeyName == "bstyle"))
     ApplyStyleProp(TagNode, KeyName, ARow, KeyValue);
   }
  else if (TagNode->CmpName("listcolgroup")) // ������ ������� ������
   {
    if (KeyName == "name")
     ApplyTextProp(TagNode, KeyName, ARow, KeyValue);
    else if (KeyName == "union")
     ApplyUnionProp(TagNode, KeyName, ARow, KeyValue);
    else if (KeyName == "style")
     ApplyStyleProp(TagNode, KeyName, ARow, KeyValue);
   }
  else if (TagNode->CmpName("listcol")) // ������� ������
   {
    if (KeyName == "name")
     ApplyTextProp(TagNode, KeyName, ARow, KeyValue);
    else if ((KeyName == "hstyle") || (KeyName == "bstyle"))
     ApplyStyleProp(TagNode, KeyName, ARow, KeyValue);
    else if (KeyName == "union")
     ApplyBinProp(TagNode->GetFirstChild(), KeyName, ARow, KeyValue);
   }
  else if (TagNode->CmpName("table")) // �������
   {
    if ((KeyName == "hstyle") || (KeyName == "bstyle"))
     ApplyStyleProp(TagNode, KeyName, ARow, KeyValue);
    else if (KeyName == "border")
     ApplyIntProp(TagNode, KeyName, ARow, KeyValue);
    else if ((KeyName == "rownumbers") || (KeyName == "colnumbers") || (KeyName == "showzval"))
     ApplyBinProp(TagNode, KeyName, ARow, KeyValue);
    else if (KeyName == "tablepriority")
     ApplyTabPrtyProp(TagNode, KeyName, ARow, KeyValue);
   }
  else if (TagNode->CmpName("trow,tcol")) // ������ �������, ������� �������
   {
    if (KeyName == "name")
     ApplyTextProp(TagNode, KeyName, ARow, KeyValue);
    else if ((KeyName == "hstyle") || (KeyName == "bstyle"))
     ApplyStyleProp(TagNode, KeyName, ARow, KeyValue);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::PropertyEditorMouseDown(TObject * Sender, TMouseButton Button, Classes::TShiftState Shift,
  int X, int Y)
 {
  FPropertyEditor->Validate();
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::DocSpecEditCondRules(TObject * Sender, UnicodeString ATitle, TTagNode ** ACondRules)
 {
  return EditCondRules(ATitle, // ��������� ��� ������� �������������� ������ ������
    NULL, // xml-��� condrules ������������� ���������������� xml-������
    NULL, // ������������
    false, // ������������
    ACondRules, // xml-��� condrules
    FBaseMainISUID, // ��� �������� �������� ������������
    false); // ������� ������������� ������ ������������� ��� ������ ������
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::ParametersActionExecute(TObject * Sender)
 {
  EditParameters();
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::FillRefObjects(TTagNode * ItTag, void * Src1, void * Src2, void * Src3, void * Src4)
 {
  if (SetRefCanRefUpdate(NULL, ItTag))
   __RefObjects[ItTag->AV["uid"]] = ItTag;
  return false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::DocFilterBeforeDelete(TObject * Sender, TTagNode * RootNode)
 {
  // ���������� ������� ��������� �������, �� ������� ����� ��������� �������� �������������
  __RefObjects.clear();
  UnicodeString msg1, msg2;
  RootNode->_Iterate(FillRefObjects);
  if (__RefObjects.size())
   {
    // ���������� ������� ��������� �������������, ������� ����� ��������� �� �������� �������
    __Refers.clear();
    TTagNode * ndInsc = FXML->GetChildByName("doccontent")->GetFirstChild();
    while (ndInsc)
     {
      if (ndInsc->CmpName("inscription"))
       {
        TTagNode * ndInsText = ndInsc->GetFirstChild();
        while (ndInsText)
         {
          UnicodeString RefAV = ndInsText->AV["ref"];
          if ((RefAV != "") && (_GUI(RefAV) == ""))
           __Refers[ndInsText->AV["uid"]] = ndInsText;
          ndInsText = ndInsText->GetNext();
         }
       }
      ndInsc = ndInsc->GetNext();
     }
    if (__Refers.size())
     {
      TUIDsSet RefObjectsUS, RefersUS, HitsUS;
      TUIDsMap::iterator i;
      for (i = __RefObjects.begin(); i != __RefObjects.end(); i++)
       RefObjectsUS.insert((* i).first);
      for (i = __Refers.begin(); i != __Refers.end(); i++)
       RefersUS.insert((* i).second->AV["ref"]);
      // ����������� ������ ��������� ������������� �� �������� �������
      set_intersection(RefObjectsUS.begin(), RefObjectsUS.end(), RefersUS.begin(), RefersUS.end(),
        inserter(HitsUS, HitsUS.begin()));
      if (HitsUS.size())
       {
        TStringList * dopinfo = new TStringList;
        for (TUIDsSet::iterator i = HitsUS.begin(); i != HitsUS.end(); i++)
         {
          TUIDsMap::iterator j = find_if(__Refers.begin(), __Refers.end(),
            ICUtils::compose1(bind2nd(equal_to<UnicodeString>(), *i), SelectPareTagNodeRefAttrVal()));
          if (j != __Refers.end())
           {
            msg1 = "";
            msg2 = "";
            if ((*j).second->Ctrl)
             msg1 = reinterpret_cast<TTreeNode *>((*j).second->Ctrl)->Text;
            if (__RefObjects[* i]->Ctrl)
             reinterpret_cast<TTreeNode *>(__RefObjects[* i]->Ctrl)->Text;
            dopinfo->Add(cFMT2(icsDocSpecErrorSetRefDopInfo, msg1, msg2));
           }
         }
        RMB_SHOW_ERROREX_CPT(cFMT(icsDocSpecErrorSetRefMsg), ICSName.c_str(), dopinfo);
        delete dopinfo;
        return false;
       }
     }
   }
  return true;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::PreViewActionExecute(TObject * Sender)
 {
  if (FPreviewOnFly)
   FViewer->Mode = vmOnFly;
  FViewer->DocFormat            = FPreviewDocFormat;
  FViewer->UseCSS               = FUseCSSOnHTMLPreview;
  FViewer->UseWordCSS           = FUseWordCSSOnHTMLPreview;
  FViewer->PreviewDlgKind       = FPreviewDlgKind;
  FViewer->AsyncPreviewCreation = false;
  FViewer->Specificator         = FXML;
  FViewer->XMLContainer         = XMLContainer;
  FViewer->Preview(ptSpecificator);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::CutActionExecute(TObject * Sender)
 {
  _MSG_ATT(FMT(icsDocSpecErrorOperation), ICSName);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::CopyActionExecute(TObject * Sender)
 {
  _MSG_ATT(FMT(icsDocSpecErrorOperation), ICSName);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::PasteActionExecute(TObject * Sender)
 {
  _MSG_ATT(FMT(icsDocSpecErrorOperation), ICSName);
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::GetListColToDelete(TTagNode * ItTag, void * ListColToDelete, void * Src2, void * Src3,
  void * Src4)
 {
  if (ItTag->CmpName("listcol"))
   reinterpret_cast<TUnicodeStringList *>(ListColToDelete)->push_back(ItTag->AV["uid"]);
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::DeleteActionExecute(TObject * Sender)
 {
  DeleteCurrentNode();
 }
// ---------------------------------------------------------------------------
TTreeNode * __fastcall TICSDocSpec::AppendDocElement(UnicodeString AName)
 {
  TTagNode * ndDocElement = GetCurrentTagNode();
  if (!ndDocElement->CmpName("inscription,list,table"))
#pragma option push -w-pia
   if (!(ndDocElement = GetCurrentTagNode()->GetParent("inscription")))
    if (!(ndDocElement = GetCurrentTagNode()->GetParent("list")))
#pragma option pop
     ndDocElement = GetCurrentTagNode()->GetParent("table");
  return (ndDocElement) ? InsertTreeNodeAfter(reinterpret_cast<TTreeNode *>(ndDocElement->Ctrl), "", AName) :
    AddChildTreeNodeFirst(FEditTV->Items->GetFirstNode(), "", AName);
 }
// ---------------------------------------------------------------------------
TTreeNode * __fastcall TICSDocSpec::AddInsText(TTreeNode * TreeNode)
 {
  TTreeView * TreeView = dynamic_cast<TTreeView *>(TreeNode->TreeView);
  TreeView->Items->BeginUpdate();
  TTreeNode * InsTextNode = (reinterpret_cast<TTagNode *>(TreeNode->Data)->CmpName("inscription")) ?
    AddChildTreeNodeFirst(TreeNode, "", "instext") : InsertTreeNodeAfter(TreeNode, "", "instext");
  TTagNode * ndInsText = reinterpret_cast<TTagNode *>(InsTextNode->Data);
  ndInsText->AV["uid"]     = NewUID();
  ndInsText->AV["uidmain"] = "";
  ndInsText->AV["ref"]     = "";
  ndInsText->AV["refmain"] = "";
  ndInsText->AV["style"]   = "";
  ndInsText->AV["newline"] = "0";
  ndInsText->AV["value"]   = "";
  InsTextNode->Text        = GetXMLNodeViewText(ndInsText);
  TreeView->Items->EndUpdate();
  return InsTextNode;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AddInscActionExecute(TObject * Sender)
 {
  FEditTV->Items->BeginUpdate();
  TTreeNode * InscNode = AppendDocElement("inscription");
  TTagNode * ndInsc = reinterpret_cast<TTagNode *>(InscNode->Data);
  ndInsc->AV["uid"]     = NewUID();
  ndInsc->AV["uidmain"] = "";
  ndInsc->AV["style"]   = "";
  ndInsc->AV["newline"] = "0";
  InscNode->Text        = GetXMLNodeViewText(ndInsc);
  AddInsText(InscNode);
  FEditTV->Selected = InscNode;
  FEditTV->Selected->MakeVisible();
  FEditTV->Selected->Expand(true);
  FEditTV->Items->EndUpdate();
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AddInsTextActionExecute(TObject * Sender)
 {
  FEditTV->Items->BeginUpdate();
  FEditTV->Selected = AddInsText(FEditTV->Selected);
  FEditTV->Selected->MakeVisible();
  FEditTV->Selected->Expand(true);
  FEditTV->Items->EndUpdate();
 }
// ---------------------------------------------------------------------------
TStringList * __fastcall TICSDocSpec::CreateNmklISList()
 {
  TStringList * ISList = new TStringList;
  TTagNode * ISNode = GetDomainNode()->GetFirstChild();
  while (ISNode)
   {
    ISList->AddObject(ISNode->AV["name"], ISNode);
    ISNode = ISNode->GetNext();
   }
  return ISList;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AddListActionExecute(TObject * Sender)
 {
  TfmAddList * dlg;
  try
   {
    dlg = new TfmAddList(this, true);
    if (dlg)
     {
      dlg->OnObjListLoad = AddListObjListLoad;
      TStringList * ISList = CreateNmklISList();
      dlg->ISList = ISList;
      delete ISList;
      if (dlg->ShowModal() == mrOk)
       {
        FEditTV->Items->BeginUpdate();
        TTreeNode * ListNode = AppendDocElement("list");
        TTagNode * ndList = reinterpret_cast<TTagNode *>(ListNode->Data);
        TTagNode * ndListCont = ndList->AddChild("listcont");
        TTreeNode * CurNode = dlg->ListCols->GetFirstNode();
        TTagNodeList AddedListCols;
        while (CurNode)
         {
          TTagNode * ndListCol = ndListCont->AddChild();
          ndListCol->Assign(reinterpret_cast<TTagNode *>(CurNode->Data), true);
          AddedListCols.push_back(ndListCol);
          CurNode = CurNode->getNextSibling();
         }
        ndList->AV["uid"] = NewUID();
        ndList->AV["uidmain"]    = "";
        ndList->AV["ref"]        = GetExtRef(dlg->MainIS);
        ndList->AV["showzval"]   = "1";
        ndList->AV["border"]     = "1";
        ndList->AV["rownumbers"] = "1";
        ndList->AV["hstyle"]     = "";
        ndList->AV["bstyle"]     = "";
        ndList->AV["width"]      = "100%";
        LoadXML(ndList, ListNode);
        FEditTV->Items->EndUpdate();
       }
     }
   }
  __finally
   {
    if (dlg)
     delete dlg;
   }
 }
// ---------------------------------------------------------------------------
int __fastcall TICSDocSpec::AddListObjListLoad(TObject * Sender, TTreeNodes * TreeNodes, TTagNode * MainIS)
 {
  int nResult;
  if (dynamic_cast<TfmAddList *>(Sender)->AddListMode == alNewLinkISAttrsListCols)
   {
    TTagNode * ndLinkIS = GetCurrentTagNode();
    if (ndLinkIS->CmpName("is_attr"))
     ndLinkIS = ndLinkIS->GetParent();
    else
     ndLinkIS = ndLinkIS->GetChildByName("link_is");
    nResult = GetLinkedISAttrs(ndLinkIS->AV["ref"], TreeNodes);
   }
  else
   {
    nResult = GetISList(MainIS, TreeNodes, "ATTRS");
    TTreeNode * TreeNode = TreeNodes->GetFirstNode();
    while (TreeNode)
     {
      if (reinterpret_cast<TTagNode *>(TreeNode->Data) != MainIS)
       TreeNode->Text = TreeNode->Text + csCountViewText;
      TreeNode = TreeNode->getNextSibling();
     }
   }
  return nResult;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AddLColActionExecute(TObject * Sender)
 {
  TTagNode * TagNode = GetCurrentTagNode();
  bool fCanAddGroup = true;
  if (TagNode->CmpName("listcolgroup") && TagNode->CmpAV("union", "1"))
   fCanAddGroup = false;
  else
   {
    TTagNode * ndParent = TagNode->GetParent("listcolgroup");
    while (ndParent)
     {
      if (ndParent->CmpAV("union", "1"))
       {
        fCanAddGroup = false;
        break;
       }
      ndParent = ndParent->GetParent("listcolgroup");
     }
   }
  TAddListMode nAddListMode = (IsListColSingle2(TagNode) || IsListColLinkISGroup(TagNode)) ? alNewLinkISAttrsListCols :
    alNewListCols;
  TfmAddList * dlg = new TfmAddList(this, fCanAddGroup, nAddListMode);
  try
   {
    dlg->OnObjListLoad = AddListObjListLoad;
    TTagNode * ndList = (TagNode->CmpName("list")) ? TagNode : TagNode->GetParent("list");
    dlg->MainIS = GetXMLContainer()->GetRefer(ndList->AV["ref"]);
    if (dlg->ShowModal() == mrOk)
     {
      bool fAddChildFirst = (dlg->AddListMode == alNewListCols) ? TagNode->CmpName("list,listcolgroup") :
        TagNode->CmpName("listcol");
      TTagNode * ndParent;
      if (fAddChildFirst)
       {
        if (TagNode->CmpName("list"))
         ndParent = TagNode->GetChildByName("listcont");
        else if (IsListColLinkISGroup(TagNode))
         ndParent = TagNode->GetFirstChild();
        else
         ndParent = TagNode;
       }
      else
       ndParent = TagNode->GetParent();
      TTagNode * ndIns = (fAddChildFirst) ? NULL : TagNode;
      TTreeNode * CurNode = dlg->ListCols->GetFirstNode();
      int i = 0;
      FEditTV->Items->BeginUpdate();
      TTagNodeList AddedListCols;
      while (CurNode)
       {
        TTagNode * ndCur = reinterpret_cast<TTagNode *>(CurNode->Data);
        TTagNode * ndNew;
        if (fAddChildFirst && !i)
         ndNew = (ndParent->Count) ? ndParent->GetFirstChild()->Insert("", true) : ndParent->AddChild();
        else
         ndNew = ndIns->Insert("", false);
        ndNew->Assign(ndCur, true);
        TICSDocSpec::InsertListColSingle2SizeAttr(ndNew, "width", "");
        AddedListCols.push_back(ndNew);
        ndIns   = ndNew;
        CurNode = CurNode->getNextSibling();
        i++ ;
       }
      if (ndParent->CmpName("listcont,link_is"))
       ndParent = ndParent->GetParent();
      TTreeNode * ParentNode = reinterpret_cast<TTreeNode *>(ndParent->Ctrl);
      ParentNode->DeleteChildren();
      LoadXML(ndParent, ParentNode);
      FEditTV->Selected = reinterpret_cast<TTreeNode *>(ndIns->Ctrl);
      FEditTV->Selected->MakeVisible();
      FEditTV->Items->EndUpdate();
     }
   }
  __finally
   {
    delete dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AddTableActionExecute(TObject * Sender)
 {
  TfmAddTable * dlg;
  try
   {
    dlg = new TfmAddTable(this);
    if (dlg)
     {
      TStringList * ISList = CreateNmklISList();
      dlg->ISList = ISList;
      delete ISList;
      if (dlg->ShowModal() == mrOk)
       {
        FEditTV->Items->BeginUpdate();
        TTreeNode * TableNode = AppendDocElement("table");
        TTagNode * ndTable = reinterpret_cast<TTagNode *>(TableNode->Data);
        ndTable->AV["uid"]           = NewUID();
        ndTable->AV["ref"]           = GetExtRef(dlg->MainIS);
        ndTable->AV["hstyle"]        = "";
        ndTable->AV["bstyle"]        = "";
        ndTable->AV["border"]        = "1";
        ndTable->AV["rownumbers"]    = "1";
        ndTable->AV["colnumbers"]    = "1";
        ndTable->AV["showzval"]      = "1";
        ndTable->AV["tablepriority"] = "row";
        ndTable->AV["width"]         = "100%";
        TTagNode * ndRows = ndTable->AddChild("rows");
        for (int i = 0; i < dlg->RowCount; i++)
         {
          TTagNode * TRowNode = ndRows->AddChild("trow");
          TRowNode->AV["name"]   = FMT1(icsDocSpecAddTableRowNum, IntToStr(i + 1));
          TRowNode->AV["uid"]    = NewUID();
          TRowNode->AV["ref"]    = "";
          TRowNode->AV["hstyle"] = "";
          TRowNode->AV["bstyle"] = "";
         }
        TTagNode * ndCols = ndTable->AddChild("columns");
        for (int i = 0; i < dlg->ColCount; i++)
         {
          TTagNode * TColNode = ndCols->AddChild("tcol");
          TColNode->AV["name"]   = FMT1(icsDocSpecAddTableColNum, IntToStr(i + 1));
          TColNode->AV["uid"]    = NewUID();
          TColNode->AV["ref"]    = "";
          TColNode->AV["hstyle"] = "";
          TColNode->AV["bstyle"] = "";
          TColNode->AV["width"]  = "";
         }
        LoadXML(ndTable, TableNode);
        FEditTV->Items->EndUpdate();
       }
     }
   }
  __finally
   {
    if (dlg)
     delete dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AddTColActionExecute(TObject * Sender)
 {
  TTagNode * ndTCol = GetCurrentTagNode()->AddChild("tcol");
  ndTCol->AV["name"]   = FMT(icsDocSpecAddTableNewCol);
  ndTCol->AV["uid"]    = NewUID();
  ndTCol->AV["ref"]    = "";
  ndTCol->AV["hstyle"] = "";
  ndTCol->AV["bstyle"] = "";
  ndTCol->AV["width"]  = "";
  if (GetCurrentTagNode()->CmpName("tcol"))
   GetCurrentTagNode()->AV["width"] = "";
  if (FEnumTableRowsCols)
   {
    TTagNode * ndColumns = ndTCol->GetParent("columns");
    TTreeNode * ColumnsNode = reinterpret_cast<TTreeNode *>(ndColumns->Ctrl);
    FEditTV->Items->BeginUpdate();
    CreateTableRowColNumMaps(ndColumns->GetParent("table"), teCol);
    ColumnsNode->DeleteChildren();
    LoadXML(ndColumns, ColumnsNode);
    FEditTV->Selected = reinterpret_cast<TTreeNode *>(ndTCol->Ctrl);
    FEditTV->Items->EndUpdate();
   }
  else
   FEditTV->Selected = FEditTV->Items->AddChildObject(FEditTV->Selected, cFMT(icsDocSpecAddTableNewCol),
    reinterpret_cast<TTagNode *>(ndTCol));
  FModified = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::AddTRowActionExecute(TObject * Sender)
 {
  TTagNode * ndTRow = GetCurrentTagNode()->AddChild("trow");
  ndTRow->AV["name"]   = FMT(icsDocSpecAddTableNewRow);
  ndTRow->AV["uid"]    = NewUID();
  ndTRow->AV["ref"]    = "";
  ndTRow->AV["hstyle"] = "";
  ndTRow->AV["bstyle"] = "";
  if (FEnumTableRowsCols)
   {
    TTagNode * ndRows = ndTRow->GetParent("rows");
    TTreeNode * RowsNode = reinterpret_cast<TTreeNode *>(ndRows->Ctrl);
    FEditTV->Items->BeginUpdate();
    CreateTableRowColNumMaps(ndRows->GetParent("table"), teRow);
    RowsNode->DeleteChildren();
    LoadXML(ndRows, RowsNode);
    FEditTV->Selected = reinterpret_cast<TTreeNode *>(ndTRow->Ctrl);
    FEditTV->Items->EndUpdate();
   }
  else
   FEditTV->Selected = FEditTV->Items->AddChildObject(FEditTV->Selected, cFMT(icsDocSpecAddTableNewRow),
    reinterpret_cast<TTagNode *>(ndTRow));
  FModified = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::MoveUpActionExecute(TObject * Sender)
 {
  if (IsListColSingle2(GetCurrentTagNode()))
   SwapListColSingle2SizeAttrVals(reinterpret_cast<TTagNode *>(FEditTV->Selected->Data),
    reinterpret_cast<TTagNode *>(FEditTV->Selected->getPrevSibling()->Data), "width");
  MoveTreeNodeUp(FEditTV->Selected, true);
  // ���������� ��������� ����� � �������� �������� �������� ������� �������������
  if (GetCurrentTagNode()->CmpName("trow,tcol") && FEnumTableRowsCols)
   {
    TTagNode * ndTE = (GetCurrentTagNode()->CmpName("trow")) ? GetCurrentTagNode()->GetParent("rows") :
      GetCurrentTagNode()->GetParent("columns");
    TTreeNode * TENode = reinterpret_cast<TTreeNode *>(ndTE->Ctrl);
    TTagNode * ndCur = GetCurrentTagNode();
    FEditTV->Items->BeginUpdate();
    CreateTableRowColNumMaps(ndTE->GetParent("table"), (GetCurrentTagNode()->CmpName("trow")) ? teRow : teCol);
    TENode->DeleteChildren();
    LoadXML(ndTE, TENode);
    FEditTV->Selected = reinterpret_cast<TTreeNode *>(ndCur->Ctrl);
    FEditTV->Items->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::MoveDownActionExecute(TObject * Sender)
 {
  if (IsListColSingle2(GetCurrentTagNode()))
   SwapListColSingle2SizeAttrVals(reinterpret_cast<TTagNode *>(FEditTV->Selected->Data),
    reinterpret_cast<TTagNode *>(FEditTV->Selected->getNextSibling()->Data), "width");
  MoveTreeNodeDown(FEditTV->Selected, true);
  // ���������� ��������� ����� � �������� �������� �������� ������� �������������
  if (GetCurrentTagNode()->CmpName("trow,tcol") && FEnumTableRowsCols)
   {
    TTagNode * ndTE = (GetCurrentTagNode()->CmpName("trow")) ? GetCurrentTagNode()->GetParent("rows") :
      GetCurrentTagNode()->GetParent("columns");
    TTreeNode * TENode = reinterpret_cast<TTreeNode *>(ndTE->Ctrl);
    TTagNode * ndCur = GetCurrentTagNode();
    FEditTV->Items->BeginUpdate();
    CreateTableRowColNumMaps(ndTE->GetParent("table"), (GetCurrentTagNode()->CmpName("trow")) ? teRow : teCol);
    TENode->DeleteChildren();
    LoadXML(ndTE, TENode);
    FEditTV->Selected = reinterpret_cast<TTreeNode *>(ndCur->Ctrl);
    FEditTV->Items->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::EditTVEnter(TObject * Sender)
 {
  if (FValidateErrorOnTVChanging)
   {
    FValidateErrorOnTVChanging = false;
    FPropertyEditor->SetFocus();
   }
  else
   FValidateErrorOnTVChanging = false;
 }
// ---------------------------------------------------------------------------
// ������ ����������� � ���������� � �������������� ������������ ������ ��������
// �������, ������� ��������� ������� ������ ��� ������� ��������� ������/�������
// ������� � inplaceeditor'� �������� FEditTV
void __fastcall TICSDocSpec::EditTVEditing(TObject * Sender, TTreeNode * Node, bool & AllowEdit)
 {
  TTagNode * TagNode = reinterpret_cast<TTagNode *>(Node->Data);
  AllowEdit = (TagNode && (TagNode->CmpName("instext,tcol,trow") ||
    (IsListCol(TagNode) && !IsListColSingle2(TagNode))));
  if (AllowEdit && (TagNode->CmpName("instext,trow,tcol") || IsListColSingle1Count(TagNode)))
   {
    HWND hEditControl = TreeView_GetEditControl(FEditTV->Handle);
    if (hEditControl)
     if (TagNode->CmpName("instext"))
      SendMessage(hEditControl, WM_SETTEXT, 0, (LPARAM)TagNode->AV["value"].c_str());
     else
      SendMessage(hEditControl, WM_SETTEXT, 0, (LPARAM)TagNode->AV["name"].c_str());
    else
     AllowEdit = false;
   }
 }
// ---------------------------------------------------------------------------
// �������� ���������, ��������� � inplaceeditor'� �������� FEditTV
void __fastcall TICSDocSpec::EditTVEdited(TObject * Sender, TTreeNode * Node, UnicodeString & S)
 {
  S = Trim(S);
  TTagNode * TagNode = reinterpret_cast<TTagNode *>(Node->Data);
  if (TagNode->CmpName("instext"))
   {
    TagNode->AV["value"] = S;
    FModified            = true;
    UpdateTextProp(TagNode, "value", true);
    S = GetInsTagViewText(TagNode, GetXMLContainer(), NULL);
   }
  else // ( TagNode->CmpName( "tcol,trow" ) || !IsListColSingle2( TagNode ) )
    if (S == "")
   {
    S = GetXMLNodeViewText(TagNode);
    _MSG_ERR(FMT(icsDocSpecErrorApplyText), ICSName);
   }
  else
   {
    TagNode->AV["name"] = S;
    FModified           = true;
    UpdateTextProp(TagNode, "name", true);
    S = GetXMLNodeViewText(TagNode);
   }
 }
// ---------------------------------------------------------------------------
// F2 - ������� � ����� �������������� � inplaceeditor'� �������� FEditTV
// ��������� ���� ���������� ShortCut'��
void __fastcall TICSDocSpec::EditTVKeyDown(TObject * Sender, Word & Key, TShiftState Shift)
 {
  if (FEditTV->IsEditing())
   return;
  if (FEditTV->Selected)
   {
    switch (Key)
     {
     case VK_F2:
      TreeView_EditLabel(FEditTV->Handle, FEditTV->Selected->ItemId);
      break;
     case VK_DELETE:
      if (Shift.Empty())
       FDeleteAction->Execute();
      break;
     case VK_UP:
      if (Shift.Contains(ssShift) && Shift.Contains(ssCtrl))
       FMoveUpAction->Execute();
      break;
     case VK_DOWN:
      if (Shift.Contains(ssShift) && Shift.Contains(ssCtrl))
       FMoveDownAction->Execute();
      break;
     }
    // ��������� ���������� - �������
    if ((unsigned short)GetKeyboardLayout(0) == 0x0419)
     switch (Key)
      {
      case 'Y': // Shift+Ctrl+'�'
       if (Shift.Contains(ssShift) && Shift.Contains(ssCtrl))
        FAddInscAction->Execute();
       break;
      case VK_OEM_7: // Shift+Ctrl+'�'
       if (Shift.Contains(ssShift) && Shift.Contains(ssCtrl))
        FAddInsTextAction->Execute();
       break;
      case 'C': // Shift+Ctrl+'�'
       if (Shift.Contains(ssShift) && Shift.Contains(ssCtrl))
        FAddListAction->Execute();
       break;
      case VK_OEM_COMMA: // Shift+Ctrl+'�'
       if (Shift.Contains(ssShift) && Shift.Contains(ssCtrl))
        FAddLColAction->Execute();
       break;
      case 'N': // Shift+Ctrl+'�'
       if (Shift.Contains(ssShift) && Shift.Contains(ssCtrl))
        FAddTableAction->Execute();
       break;
      case 'J': // Shift+Ctrl+'�'
       if (Shift.Contains(ssShift) && Shift.Contains(ssCtrl))
        FAddTColAction->Execute();
       break;
      case 'H': // Shift+Ctrl+'�'
       if (Shift.Contains(ssShift) && Shift.Contains(ssCtrl))
        FAddTRowAction->Execute();
       break;
      }
   }
 }
// ---------------------------------------------------------------------------
// �������� ������ � ��������� �������
// ( ��� ���������, ��������� ������������� ��������� ������ � ��������� �������
// �������������� ��� ������ ������ �����, � ������������������ ������� ��� �����
// �� �������� TreeView ������: EditTVChanging -> EditTVChange -> PropertyEditorExit ->
// EditTVEnter )
void __fastcall TICSDocSpec::EditTVChanging(TObject * Sender, TTreeNode * Node, bool & AllowChange)
 {
  bool tmp = true;
  if (FPropertyEditor->Visible)
   if (!(tmp = FPropertyEditor->Validate()))
    FValidateErrorOnTVChanging = true;
  AllowChange = tmp;
 }
// ---------------------------------------------------------------------------
// ����������� ����������� ��������
void __fastcall TICSDocSpec::UpdateActions()
 {
  TTagNode * TagNode = GetCurrentTagNode();
  bool eDel, eInsText, eLCol, eTCol, eTRow;
  eDel = eInsText = eLCol = eTCol = eTRow = false;
  if (TagNode->CmpName("inscription,list,table"))
   eDel = true;
  if (TagNode->CmpName("inscription,instext"))
   {
    eInsText = true;
    if ((TagNode->CmpName("instext")) && (TagNode->GetParent()->Count > 1))
     eDel = true;
   }
  if (TagNode->CmpName("listcol,listcolgroup,is_attr,list"))
   {
    eLCol = true;
    if (TagNode->Name != "list")
     {
      if (TagNode->CmpName("is_attr"))
       {
        if (TagNode->GetParent()->GetChildByName("condrules"))
         {
          if (TagNode->GetParent()->Count > 2)
           eDel = true;
         }
        else if (TagNode->GetParent()->Count > 1)
         eDel = true;
       }
      else if (TagNode->GetParent()->Count > 1)
       eDel = true;
     }
   }
  if ((TagNode->CmpName("rows")) || ((TagNode->CmpName("trow")) && !TagNode->GetChildByName("calcrules")
    && !TagNode->GetChildByName("handrules")))
   eTRow = true;
  else if ((TagNode->CmpName("columns")) || ((TagNode->CmpName("tcol")) && !TagNode->GetChildByName("calcrules")
    && !TagNode->GetChildByName("handrules")))
   eTCol = true;
  if (((TagNode->CmpName("trow")) && !((TagNode->GetParent()->CmpName("rows")) && (TagNode->GetParent()->GetCount(false,
    "trow") == 1))) || ((TagNode->CmpName("tcol")) && !((TagNode->GetParent()->CmpName("columns")) &&
    (TagNode->GetParent()->GetCount(false, "tcol") == 1))))
   eDel = true;
  FDeleteAction->Enabled     = eDel;
  FAddInsTextAction->Enabled = eInsText;
  FAddLColAction->Enabled    = eLCol;
  FAddTColAction->Enabled    = eTCol;
  FAddTRowAction->Enabled    = eTRow;
  FMoveUpAction->Enabled     = (FEditTV->Selected->getPrevSibling() && (!TagNode->CmpName("rows,columns")));
  FMoveDownAction->Enabled   = (FEditTV->Selected->getNextSibling() && (!TagNode->CmpName("rows,columns")));
 }
// ---------------------------------------------------------------------------
// ����������� ����������� �������� (TAction) � ���������� ������� �������
// ��� ��������� �������� ���� EditTV
void __fastcall TICSDocSpec::EditTVChange(TObject * Sender, TTreeNode * Node)
 {
  TTagNode * TagNode = reinterpret_cast<TTagNode *>(Node->Data);
  // ���������� ������� �������
  bool bHidePropertyEditor = false;
  FPropertyEditor->Clear();
  FPropertyEditor->BeginUpdate();
  if (TagNode->CmpName("inscription")) // �������
   {
    FPropertyEditor->Visible = true;
    AppendStyleProp(TagNode, "style", FMT(icsDocSpecDefaultStyleCaption));
    AppendBinProp(TagNode, "newline", FMT(icsDocSpecNewRowCaption));
   }
  else if (TagNode->CmpName("instext")) // ������� �������
   {
    FPropertyEditor->Visible = true;
    INS_VIEW_TEXT VT;
    GetInsTagViewText(TagNode, GetXMLContainer(), & VT);
    AppendStyleProp(TagNode, "style", FMT(icsDocSpecStyleCaption));
    AppendBinProp(TagNode, "newline", FMT(icsDocSpecNewRowCaption));
    AppendTextProp(TagNode, "value", FMT(icsDocSpecStaticTextCaption), rrImplied);
    AppendValRefProp(TagNode, "ref", FMT(icsDocSpecRefCaption), VT.Ref);
    AppendValRefProp(TagNode, "count", FMT(icsDocSpecCalcCountCaption), VT.Count);
   }
  else if (TagNode->CmpName("list")) // ������
   {
    FPropertyEditor->Visible = true;
    AppendChildNodeProp(TagNode, "condrules", FMT(icsDocSpecRulesCaption), reEnabled);
    AppendChildNodeProp(TagNode, "sortorder", FMT(icsDocSpecOrderCaption), reEnabled);
    AppendReqRefProp(TagNode, "ref", FMT(icsDocSpecSelectDataFromCaption));
    AppendBinProp(TagNode, "showzval", FMT(icsDocSpecShowZeroValsCaption));
    AppendIntProp(TagNode, "border", FMT(icsDocSpecThickRectCaption), rrRequared);
    AppendBinProp(TagNode, "rownumbers", FMT(icsDocSpecRowNumCaption));
    AppendStyleProp(TagNode, "hstyle", FMT(icsDocSpecDefaultHeaderStyleCaption));
    AppendStyleProp(TagNode, "bstyle", FMT(icsDocSpecDefaultDataStyleCaption));
    AppendSizeProp(TagNode, "width", FMT(icsDocSpecWidthCaption), reEnabled);
   }
  else if (TagNode->CmpName("listcolgroup")) // ������ ������� ������
   {
    FPropertyEditor->Visible = true;
    AppendTextProp(TagNode, "name", FMT(icsDocSpecHeaderCaption), rrRequared);
    AppendUnionProp(TagNode, "union", FMT(icsDocSpecColUnionCaption));
    AppendStyleProp(TagNode, "style", FMT(icsDocSpecStyleCaption));
   }
  else if (TagNode->CmpName("listcol")) // ������� ������
   {
    FPropertyEditor->Visible = true;
    AppendTextProp(TagNode, "name", FMT(icsDocSpecHeaderCaption), rrRequared);
    AppendStyleProp(TagNode, "hstyle", FMT(icsDocSpecHeaderStyleCaption));
    AppendStyleProp(TagNode, "bstyle", FMT(icsDocSpecDataStyleCaption));
    TTagNode * ndColType = TagNode->GetFirstChild();
    if (ndColType->CmpName("is_attr"))
     {
      AppendImpRefProp(ndColType, "is_attr", FMT(icsDocSpecShowingCaption));
      AppendSizeProp(TagNode, "width", FMT(icsDocSpecWidthCaption), reEnabled);
     }
    else // "link_is"
     {
      AppendReqRefProp(ndColType, "link_is", FMT(icsDocSpecSelectDataFromCaption));
      TTagNode * ndLinkISType = ndColType->GetFirstChild();
      if (ndLinkISType->CmpName("count"))
       {
        FPropertyEditor->InsertRow("count", FMT(icsDocSpecCalcCountCaption), cFMT(icsDocSpecTRUE_VAVCaption), true,
          reDisabled, rrRequared, roReadOnly, esSimple);
        AppendChildNodeProp(ndLinkISType, "condrules", FMT(icsDocSpecRulesCaption), reEnabled);
        AppendSizeProp(TagNode, "width", FMT(icsDocSpecWidthCaption), reEnabled);
       }
      else
       {
        AppendBinProp(ndColType, "union", FMT(icsDocSpecColUnionCaption));
        AppendChildNodeProp(ndColType, "condrules", FMT(icsDocSpecRulesCaption), reEnabled);
       }
     }
   }
  // ������� ��������� ��������
  else if ((TagNode->CmpName("is_attr")) && (TagNode->GetParent()->CmpName("link_is")))
   {
    FPropertyEditor->Visible = true;
    AppendImpRefProp(TagNode, "ref", FMT(icsDocSpecShowingCaption));
    AppendSizeProp(TagNode, "width", FMT(icsDocSpecWidthCaption), reEnabled);
   }
  else if (TagNode->CmpName("table")) // �������
   {
    FPropertyEditor->Visible = true;
    AppendChildNodeProp(TagNode, "condrules", FMT(icsDocSpecRulesCaption), reEnabled);
    AppendReqRefProp(TagNode, "ref", FMT(icsDocSpecSelectDataFromCaption));
    AppendStyleProp(TagNode, "hstyle", FMT(icsDocSpecDefaultHeaderStyleCaption));
    AppendStyleProp(TagNode, "bstyle", FMT(icsDocSpecDefaultDataStyleCaption));
    AppendIntProp(TagNode, "border", FMT(icsDocSpecThickRectCaption), rrRequared);
    AppendBinProp(TagNode, "rownumbers", FMT(icsDocSpecRowNumCaption));
    AppendBinProp(TagNode, "colnumbers", FMT(icsDocSpecColNumCaption));
    AppendBinProp(TagNode, "showzval", FMT(icsDocSpecShowZeroValsCaption));
    AppendTabPrtyProp(TagNode, "tablepriority", FMT(icsDocSpecPriorityCaption));
    AppendSizeProp(TagNode, "width", FMT(icsDocSpecWidthCaption), reEnabled);
   }
  else if (TagNode->CmpName("rows,columns")) // ������ �������, ������� �������
   {
    FPropertyEditor->Visible = true;
    AppendChildNodeProp(TagNode, "condrules", FMT(icsDocSpecRulesCaption), reEnabled);
   }
  else if (TagNode->CmpName("trow,tcol")) // ������ �������, ������� �������
   {
    FPropertyEditor->Visible = true;
    AppendTextProp(TagNode, "name", FMT(icsDocSpecHeaderCaption), rrRequared);
    AppendStyleProp(TagNode, "hstyle", FMT(icsDocSpecHeaderStyleCaption));
    AppendStyleProp(TagNode, "bstyle", FMT(icsDocSpecDataStyleCaption));
    AppendChildNodeProp(TagNode, "condrules", FMT(icsDocSpecRulesCaption), reEnabled);
    AppendCalcRulesProp(TagNode, "calcrules", FMT(icsDocSpecCalcRulesCaption),
      static_cast<TDKPropertyEditorRowEn>(!TagNode->GetChildByName(TagNode->Name)));
    AppendHandRulesProp(TagNode, "handrules", FMT(icsDocSpecCheckRulesCaption),
      static_cast<TDKPropertyEditorRowEn>(!TagNode->GetChildByName(TagNode->Name)));
    if (TagNode->CmpName("tcol"))
     if (TagNode->GetChildByName("tcol"))
      AppendSizeProp(TagNode, "width", FMT(icsDocSpecWidthCaption), reDisabled);
     else
      AppendSizeProp(TagNode, "width", FMT(icsDocSpecWidthCaption), reEnabled);
   }
  else
   bHidePropertyEditor = true;
  if (bHidePropertyEditor)
   FPropertyEditor->Visible = false;
  FPropertyEditor->EndUpdate();
  if (FPropertyEditor->Visible)
   FPropertyEditor->SelectRow(0);
  // ����������� ����������� ��������
  UpdateActions();
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::EditTVDrawItem(TCustomTreeView * Sender, TTreeNode * Node, TCustomDrawState State,
  bool & DefaultDraw)
 {
  DefaultDraw = true;
 }
// ***************************************************************************
// ********************** �������� ������ ************************************
// ***************************************************************************
/*
 * �������� ������ �������������
 *
 *
 * ���������:
 *   [in]  AAutor     - ����� ������������� (��� ���)
 *   [in]  APeriodDef - ������� �������������� ���������:
 *                        0 - �� �������������
 *                        1 - �������������
 *                      [default = "1"]
 *                      �������� ��� �������� �������������:
 *                      year     == 1
 *                      halfyear == 1
 *                      quarter  == 1
 *                      month    == 1
 *                      unique   == 1
 *                      no       == 0
 *
 * ������������ ��������:
 *   ��. �������� TICSDocSpecNewResult
 *
 * ����������:
 *   ��� �������� ������� �������� nrParametersCanceled Enabled ���������� ��������������� � false
 *
 */
TICSDocSpecNewResult __fastcall TICSDocSpec::New(UnicodeString AAutor, UnicodeString APeriodDef)
 {
  if (!Delete())
   return nrCanNotDelete;
  FCreated   = true;
  FXML       = new TTagNode;
  FXML->Name = "specificator";
  TTagNode * CurNode = FXML->AddChild("passport");
  CurNode->AV["GUI"]      = NewGUI();
  CurNode->AV["mainname"] = "";
  CurNode->AV["autor"]    = AAutor;
  CurNode->AV["version"]  = "1";
  CurNode->AV["release"]  = "0";
  CurNode->AV["save_bd"]  = "0";
  if ((APeriodDef == "no") || (APeriodDef == "0"))
   CurNode->AV["perioddef"] = "0";
  else
   CurNode->AV["perioddef"] = "1";
  CurNode->AV["orientation"] = "group";
  CurNode->AV["timestamp"]   = Now().FormatString("yyyymmddhhnnss");
  CurNode->AV["defversion"]  = csSpecDefVersion;
  FXML->AddChild("doccontent");
  TTagNode * StylesNode = CurNode->AddChild("styles");
  CurNode                         = StylesNode->AddChild("style");
  CurNode->AV["name"]             = "�������";
  CurNode->AV["uid"]              = NewUID();
  CurNode->AV["align"]            = "left";
  CurNode->AV["fontname"]         = "Times New Roman";
  CurNode->AV["fontsize"]         = "12";
  CurNode->AV["wordwrap"]         = "1";
  CurNode->AV["strike"]           = "0";
  CurNode->AV["underline"]        = "0";
  CurNode->AV["bold"]             = "0";
  CurNode->AV["italic"]           = "0";
  StylesNode->AV["defaultbstyle"] = CurNode->AV["uid"];
  CurNode                         = StylesNode->AddChild("style");
  CurNode->AV["name"]             = FMT(icsDocSpecHeaderCaption);
  CurNode->AV["uid"]              = NewUID();
  CurNode->AV["align"]            = "left";
  CurNode->AV["fontname"]         = "Times New Roman";
  CurNode->AV["fontsize"]         = "12";
  CurNode->AV["wordwrap"]         = "1";
  CurNode->AV["strike"]           = "0";
  CurNode->AV["underline"]        = "0";
  CurNode->AV["bold"]             = "1";
  CurNode->AV["italic"]           = "0";
  StylesNode->AV["defaulthstyle"] = CurNode->AV["uid"];
  CurNode                         = FXML->GetFirstChild()->AddChild("pagesetup");
  CurNode->AV["topfield"]         = "2000";
  CurNode->AV["bottomfield"]      = "2000";
  CurNode->AV["leftfield"]        = "3000";
  CurNode->AV["rightfield"]       = "1500";
  CurNode->AV["orientation"]      = "0";
  CurNode->AV["width"]            = "21000";
  CurNode->AV["height"]           = "29700";
  FModified                       = true;
  FEditTV->Selected               = FEditTV->Items->AddObject(NULL, FXMLTitle,
    reinterpret_cast<void *>(FXML->GetChildByName("doccontent")));
  EditTVChange(FEditTV, FEditTV->Selected);
  FParametersAction->Enabled = true;
  FPreViewAction->Enabled    = true;
  FAddInscAction->Enabled    = true;
  FAddListAction->Enabled    = true;
  FAddTableAction->Enabled   = true;
  if (EditParameters())
   {
    Enabled = true;
    return nrParametersApplied;
   }
  else
   {
    Enabled = false;
    return nrParametersCanceled;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::Delete()
 {
  if (inherited::Delete())
   {
    FPropertyEditor->Clear();
    FPropertyEditor->Visible = false;
    Enabled                  = true;
    return true;
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::Set(TTagNode * ASpec)
 {
  CheckSpec(ASpec);
  if (Delete())
   {
    FCreated = false;
    FXML     = ASpec;
    if (UpdateSpec(FXML))
     FModified = true;
    LoadXML(FXML->GetChildByName("doccontent"));
    FParametersAction->Enabled = true;
    FPreViewAction->Enabled    = true;
    FAddInscAction->Enabled    = true;
    FAddListAction->Enabled    = true;
    FAddTableAction->Enabled   = true;
    Enabled                    = true;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocSpec::Load(TTagNode * ASpec)
 {
  CheckSpec(ASpec);
  if (Delete())
   {
    FCreated = true;
    FXML     = new TTagNode;
    FXML->Assign(ASpec, true);
    if (UpdateSpec(FXML))
     FModified = true;
    LoadXML(FXML->GetChildByName("doccontent"));
    FParametersAction->Enabled = true;
    FPreViewAction->Enabled    = true;
    FAddInscAction->Enabled    = true;
    FAddListAction->Enabled    = true;
    FAddTableAction->Enabled   = true;
    Enabled                    = true;
   }
 }
// ---------------------------------------------------------------------------
/*
 * �������� ������������ �������� ���������
 *
 *
 * ������������ ��������:
 *   ��. �������� TCheckDocSpecResult
 *
 */
TCheckDocSpecResult __fastcall TICSDocSpec::Check()
 {
  if (IsNone())
   return frNone;
  if (IsEmpty())
   return frEmpty;
  return frOk;
 }
// ---------------------------------------------------------------------------
TAttr * __fastcall TICSDocSpec::GetListColGroupUnion(const TTagNode * ndListEl)
 {
  TAttr * UnionAttr = NULL;
  if (IsListColGroup(ndListEl))
   if (CC_TNode(ndListEl)->CmpName("listcolgroup"))
    UnionAttr = CC_TNode(ndListEl)->GetAttrByName("union");
   else // listcol
      UnionAttr = CC_TNode(ndListEl)->GetChildByName("link_is")->GetAttrByName("union");
  return UnionAttr;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsListColGroup(const TTagNode * ndListEl)
 {
  return CC_TNode(ndListEl)->CmpName("listcolgroup") || IsListColLinkISGroup(ndListEl);
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsListColLinkISGroup(const TTagNode * ndListEl)
 {
  bool bResult = false;
  if (CC_TNode(ndListEl)->CmpName("listcol"))
   {
    TTagNode * ndListColType = CC_TNode(ndListEl)->GetFirstChild();
    if (ndListColType->CmpName("link_is") && ndListColType->GetChildByName("is_attr"))
     bResult = true;
   }
  return bResult;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsListColSingle1(const TTagNode * ndListEl)
 {
  return IsListColSingle1Main(ndListEl) || IsListColSingle1Count(ndListEl);
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsListColSingle1Main(const TTagNode * ndListEl)
 {
  bool bResult = false;
  if (CC_TNode(ndListEl)->CmpName("listcol"))
   {
    TTagNode * ndListColType = CC_TNode(ndListEl)->GetFirstChild();
    bResult = (ndListColType && ndListColType->CmpName("is_attr"));
   }
  return bResult;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsListColSingle1Count(const TTagNode * ndListEl)
 {
  bool bResult = false;
  if (CC_TNode(ndListEl)->CmpName("listcol"))
   {
    TTagNode * ndListColType = CC_TNode(ndListEl)->GetFirstChild();
    bResult = (ndListColType && ndListColType->CmpName("link_is") && ndListColType->GetChildByName("count"));
   }
  return bResult;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsListColSingle2(const TTagNode * ndListEl)
 {
  return CC_TNode(ndListEl)->CmpName("is_attr") && CC_TNode(ndListEl)->GetParent()->CmpName("link_is");
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsListColSingle(const TTagNode * ndListEl)
 {
  bool bResult;
  if (IsListColSingle1(ndListEl))
   {
    TAttr * attUnion = GetListColGroupUnion(CC_TNode(ndListEl)->GetParent());
    bResult = !(attUnion && attUnion->Value == "1");
   }
  else if (IsListColSingle2(ndListEl))
   {
    TAttr * attUnion = GetListColGroupUnion(CC_TNode(ndListEl)->GetParent("listcol"));
    bResult = !(attUnion && attUnion->Value == "1");
   }
  else
   {
    TAttr * attUnion = GetListColGroupUnion(ndListEl);
    bResult = (attUnion && attUnion->Value == "1");
   }
  return bResult;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsListCol(const TTagNode * ndListEl)
 {
  return IsListColGroup(ndListEl) || IsListColSingle(ndListEl);
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsListFirstCol(const TTagNode * ndListCol)
 {
  bool bIsListFirstCol;
  if (TICSDocSpec::IsListColSingle1(ndListCol) || TICSDocSpec::IsListColGroup(ndListCol))
   bIsListFirstCol = (!CC_TNode(ndListCol)->GetPrev());
  else if (TICSDocSpec::IsListColSingle2(ndListCol))
   bIsListFirstCol =
     ((!CC_TNode(ndListCol)->GetPrev() || !TICSDocSpec::IsListColSingle2(CC_TNode(ndListCol)->GetPrev())));
  else
   bIsListFirstCol = false;
  return bIsListFirstCol;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsTableFirstEl(const TTagNode * ndTableEl, UnicodeString AElName)
 {
  return (CC_TNode(ndTableEl)->CmpName(AElName) && (!CC_TNode(ndTableEl)->GetPrev() || !CC_TNode(ndTableEl)->GetPrev()
    ->CmpName(AElName)));
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsTableFirstCol(const TTagNode * ndTableCol)
 {
  return IsTableFirstEl(ndTableCol, "tcol");
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsTableFirstRow(const TTagNode * ndTableRow)
 {
  return IsTableFirstEl(ndTableRow, "trow");
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsTableElSingle(const TTagNode * ndTableEl, UnicodeString AElName)
 {
  return (CC_TNode(ndTableEl)->CmpName(AElName) && !CC_TNode(ndTableEl)->GetChildByName(AElName));
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsTableColSingle(const TTagNode * ndTableCol)
 {
  return IsTableElSingle(ndTableCol, "tcol");
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocSpec::IsTableRowSingle(const TTagNode * ndTableRow)
 {
  return IsTableElSingle(ndTableRow, "trow");
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ������ "��������"-����� ����� ��� �������� ������� ( xml-��� "table" ������������� ),
 * �.�. ����� �����, � ������� ��� ����������� ��� �������� �����-����������
 *
 *
 * ���������:
 *   [in]  ANode            - ������ �������������� xml-���������
 *   [out] ATableSingleCols - ������ "��������"-����� ( ����� ������� ������� ������ *ATableSingleCols
 *                            ���������� �������� )
 *
 *
 */
void __fastcall TICSDocSpec::GetTableSingleColsList(TTagNode * ANode, TTagNodeList * ATableSingleCols)
 {
  if (IsTableColSingle(ANode))
   ATableSingleCols->push_back(ANode);
  TTagNode * ndChild = ANode->GetFirstChild();
  while (ndChild)
   {
    GetTableSingleColsList(ndChild, ATableSingleCols);
    ndChild = ndChild->GetNext();
   }
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ������ "��������"-����� ����� ��� ����� ������� ( xml-��� "table" ������������� ),
 * �.�. ����� �����, � ������� ��� ����������� ��� �������� �����-����������
 *
 *
 * ���������:
 *   [in]  ANode            - ������ �������������� xml-���������
 *   [out] ATableSingleCols - ������ "��������"-����� ( ����� ������� ������� ������ *ATableSingleRows
 *                            ���������� �������� )
 *
 *
 */
void __fastcall TICSDocSpec::GetTableSingleRowsList(TTagNode * ANode, TTagNodeList * ATableSingleRows)
 {
  if (IsTableRowSingle(ANode))
   ATableSingleRows->push_back(ANode);
  TTagNode * ndChild = ANode->GetFirstChild();
  while (ndChild)
   {
    GetTableSingleRowsList(ndChild, ATableSingleRows);
    ndChild = ndChild->GetNext();
   }
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ������ "��������"-����� ����� ��� �������� ������ ( xml-��� "list" ������������� ),
 * �.�. ����� �����, � ������� ��� ����������� ��� �������� �����-����������
 *
 *
 * ���������:
 *   [in]  ANode           - ������ �������������� xml-���������
 *   [out] AListSingleCols - ������ "��������"-����� ( ����� ������� ������� ������ *AListSingleCols
 *                           ���������� �������� )
 *
 *
 */
void __fastcall TICSDocSpec::GetListSingleColsList(TTagNode * ANode, TTagNodeList * AListSingleCols)
 {
  if (IsListColSingle(ANode))
   AListSingleCols->push_back(ANode);
  TTagNode * ndChild = ANode->GetFirstChild();
  while (ndChild)
   {
    GetListSingleColsList(ndChild, AListSingleCols);
    ndChild = ndChild->GetNext();
   }
 }
// ---------------------------------------------------------------------------
