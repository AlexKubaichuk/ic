/*
  ����        - DurationEdit.cpp
  ������      - ��������������� (ICS)
  ��������    - ������ �������������� �������� ������������.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 29.12.2006
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "DurationEdit.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                            TDurationEditFM                            ##
//##                                                                       ##
//###########################################################################

__fastcall TDurationEditFM::TDurationEditFM(TComponent* Owner)
    : TForm(Owner)
{
  Caption            = FMT(icsDocDurationEditCaption);
  TitleLb->Caption   = FMT(icsDocDurationEditTitle);
  OkBtn->Caption     = FMT(icsDocDurationEditOkBtnCaption);
  CancelBtn->Caption = FMT(icsDocDurationEditCancelBtnCaption);

//  Ed->ControlFilter->Filter->Text = csPeriodLikeReqCtrlFlt; //?!!!
  Ed->Text = "0/0/0";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TDurationEditFM::GetDurationXMLStr() const
{
  return Ed->Text;
}
//---------------------------------------------------------------------------
void __fastcall TDurationEditFM::SetDurationXMLStr(const UnicodeString AValue)
{
  Ed->Text = AValue;
}
//---------------------------------------------------------------------------
void __fastcall TDurationEditFM::OkBtnClick(TObject *Sender)
{
  // ����� ����������� OnExit ��������� control'� �� ������ ����
  // ������ ��������� ��� ������ �� ���������
  dynamic_cast<TWinControl*>(Sender)->SetFocus();
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TDurationEditFM::CancelBtnClick(TObject *Sender)
{
  ModalResult = mrCancel;
}
//---------------------------------------------------------------------------
void __fastcall TDurationEditFM::EdPropertiesValidate(TObject *Sender, Variant &DisplayValue,
          TCaption &ErrorText, bool &Error)
{
  if (Error)
   {
     UnicodeString tmpVal = UnicodeString((wchar_t*)DisplayValue).Trim();
     Error = false;
     if (!tmpVal.LastDelimiter("/"))
      { // ����� ���
        DisplayValue = Variant(tmpVal+"/0/0");
      }
     else
      {
        if (tmpVal.Pos("/") == tmpVal.LastDelimiter("/"))
         { // ����� ���+"/"+�������?
           if (tmpVal.LastDelimiter("/") == tmpVal.Length())
            { // ����� ���+"/"
              DisplayValue = Variant(tmpVal+"0/0");
            }
           else
            { // ����� ���+"/"+�������
              DisplayValue = Variant(tmpVal+"/0");
            }
         }
        else
         {
           if (tmpVal.LastDelimiter("/") == tmpVal.Length())
            { // ����� ���+"/"+�������+"/"
              DisplayValue = Variant(tmpVal+"0");
            }
           else
            { // �������, �� �� ������ ���� ���������
              Error = true;
              ErrorText = FMT(icsDocDurationEditError);
            }
         }
      }
   }
}
//---------------------------------------------------------------------------

