/*
 ����        - ICSDocBaseEdit.cpp
 ������      - ��������������� (ICS)
 ��������    - ������� ����� ��� ��������� ������� � ��������� �������������.
 ���� ����������
 ����������� - ������� �.�.
 ����        - 15.09.2004
 */
//---------------------------------------------------------------------------

#include <vcl.h>

#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "ICSDocBaseEdit.h"
#pragma package(smart_init)

//###########################################################################
//##                                                                       ##
//##                                TFakeObj                               ##
//##                                                                       ##
//###########################################################################

/*
 ���� ����� ����� ������ ��� ����, ����� �� ����������� �������
 TICSDocBaseEdit::MoveTreeNodeUp() � TICSDocBaseEdit::MoveTreeNodeDown() �����
 ���� ���������� TTagNode::_Iterate().
 ���� � ���, ��� � TTagNode::_Iterate() �������� ItItem - ��� ��������� �� �� ����������� �����,
 ( ��������� ������ � __closure, � ��� �������� ��� ������, �� ������� ��������� ���������
 ������ ������ ������������ �������� this ). � �� ����������� ������� ����� �������� ������
 ����������� ������, �.�. ������ �������� TagNode->_Iterate( InternalUpdateTreeNodeData ), ���
 InternalUpdateTreeNodeData - ����� TICSDocBaseEdit, �� ����� �������� ���:
 TFakeObj FakeObj;
 TagNode->_Iterate( FakeObj.InternalUpdateTreeNodeData )
 ����������, � ��� ������ :)
 */

namespace ICSDocBaseEdit
 {

 class TFakeObj
  {
  public:
   bool __fastcall InternalUpdateTreeNodeData(TTagNode * ItTag, void * Src1 = NULL, void * Src2 = NULL, void * Src3 = NULL, void * Src4 = NULL);
  };

 }//end of namespace ICSDocBaseEdit

//---------------------------------------------------------------------------

bool __fastcall TFakeObj::InternalUpdateTreeNodeData(TTagNode * ItTag, void * Src1, void * Src2, void * Src3, void * Src4)
 {
  TICSDocBaseEdit::UpdateTreeNodeData(ItTag);
  return false;
 }

//###########################################################################
//##                                                                       ##
//##                           TICSDocControlProps                         ##
//##                                                                       ##
//###########################################################################

__fastcall TICSDocControlProps::TICSDocControlProps(TControl * AControl) : TPersistent()
 {
  if (!AControl)
   throw DKClasses::ENullArgument(__FUNC__, "AControl");
  FControl = AControl;
 }

//---------------------------------------------------------------------------
int __fastcall TICSDocControlProps::GetWidth()
 {
  return FControl->Width;
 }
//---------------------------------------------------------------------------
int __fastcall TICSDocControlProps::GetHeight()
 {
  return FControl->Height;
 }
//---------------------------------------------------------------------------
TSizeConstraints * __fastcall TICSDocControlProps::GetConstraints()
 {
  return FControl->Constraints;
 }
//---------------------------------------------------------------------------
TAlign __fastcall TICSDocControlProps::GetAlign()
 {
  return FControl->Align;
 }
//---------------------------------------------------------------------------
void __fastcall TICSDocControlProps::SetWidth(const int AValue)
 {
  FControl->Width = AValue;
 }
//---------------------------------------------------------------------------
void __fastcall TICSDocControlProps::SetHeight(const int AValue)
 {
  FControl->Height = AValue;
 }
//---------------------------------------------------------------------------
void __fastcall TICSDocControlProps::SetConstraints(const TSizeConstraints * AValue)
 {
  FControl->Constraints = const_cast<TSizeConstraints *>(AValue);
 }
//---------------------------------------------------------------------------
void __fastcall TICSDocControlProps::SetAlign(const TAlign AValue)
 {
  FControl->Align = AValue;
 }
//---------------------------------------------------------------------------

void __fastcall TICSDocControlProps::AssignTo(Classes::TPersistent * Dest)
 {
  if (Dest->InheritsFrom(__classid(TICSDocControlProps)))
   {
    TICSDocControlProps * DestObj = dynamic_cast<TICSDocControlProps *>(Dest);
    DestObj->Width       = Width;
    DestObj->Height      = Height;
    DestObj->Constraints = Constraints;
    DestObj->Align       = Align;
   }
  else
   inherited::AssignTo(Dest);
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocControlProps::LoadAlignProperty(TReader * Reader)

 {
  UnicodeString Val = Reader->ReadIdent();
  if (Val == "alNone")
   Align = alNone;
  else if (Val == "alTop")
   Align = alTop;
  else if (Val == "alBottom")
   Align = alBottom;
  else if (Val == "alLeft")
   Align = alLeft;
  else if (Val == "alRight")
   Align = alRight;
  else if (Val == "alClient")
   Align = alClient;
  else if (Val == "alCustom")
   Align = alCustom;
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocControlProps::StoreAlignProperty(TWriter * Writer)
 {
  switch (Align)
   {
   case alNone:
    Writer->WriteIdent("alNone");
    break;
   case alTop:
    Writer->WriteIdent("alTop");
    break;
   case alBottom:
    Writer->WriteIdent("alBottom");
    break;
   case alLeft:
    Writer->WriteIdent("alLeft");
    break;
   case alRight:
    Writer->WriteIdent("alRight");
    break;
   case alClient:
    Writer->WriteIdent("alClient");
    break;
   case alCustom:
    Writer->WriteIdent("alCustom");
    break;
   }
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocControlProps::DefineProperties(TFiler * Filer)
 {
  inherited::DefineProperties(Filer);
  Filer->DefineProperty("Align", LoadAlignProperty, StoreAlignProperty, true);
 }

//###########################################################################
//##                                                                       ##
//##                             TICSDocBaseEdit                           ##
//##                                                                       ##
//###########################################################################

//---------------------------------------------------------------------------
TTagNode * __fastcall TICSDocBaseEdit::Get()
 {
  return FXML;
 }
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------- IDKComponentVersion --------------------------------
//---------------------------------------------------------------------------

UnicodeString __fastcall TICSDocBaseEdit::DKGetVersion() const
 {
  return "1.1.1";
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::DKSetVersion(const UnicodeString AValue)
 {
 }

//---------------------------------------------------------------------------
//---------------------- IDKComponentComment --------------------------------
//---------------------------------------------------------------------------

UnicodeString __fastcall TICSDocBaseEdit::DKGetComment() const
 {
  return FComment;
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::DKSetComment(const UnicodeString AValue)
 {
  if (FComment != AValue)
   FComment = AValue;
 }

// ***************************************************************************
// ********************** ��������/�������� ���������� ***********************
// ***************************************************************************

__fastcall TICSDocBaseEdit::TICSDocBaseEdit(TComponent * Owner)
    : TCustomPanel(Owner)
 {
  BevelOuter       = bvNone;
  Caption          = "";
  FImageList       = new TImageList(this);
  FImageList->Name = "ImageList";

  FActionList         = new TActionList(this);
  FActionList->Images = FImageList;
  /*
   BarMngr = new TdxBarManager(this);
   Bar1 = BarMngr->AddToolBar();
   Bar2 = BarMngr->AddToolBar();
   BarIL = Bar1->ItemLinks->Add();
   BarBtn = new TdxBarLargeButton(this);
   BarBtn->Caption = "FFF";
   BarIL->Item = BarBtn;

   */

  FMainBM = new TdxBarManager(this); //+!!!
  //FMainBM->Parent   = this;
  FMainBM->Name                      = "FMainBarManager";
  FMainBM->ImageOptions->Images      = FImageList;
  FMainBM->ImageOptions->LargeImages = FImageList;
  //FTBDockTop->Position = dpTop;

  //FTBDockLeft = new TTBDock( this );
  //FTBDockLeft->Parent   = this;
  //FTBDockLeft->Name     = "TBDockLeft";
  //FTBDockLeft->Position = dpLeft;

  FEditPM         = new TPopupMenu(this);
  FEditPM->Images = FImageList;

  //ExtNOM_DLL   = Unassigned;
  FXMLContainer = NULL;
  FParentXML    = NULL;
  FNmklGUI      = "";
  FDomainNumber = 0;
  FXML          = NULL;
  FCreated      = false;
  FModified     = false;
  FXMLTitle     = "XML";

  FBeforeDelete             = NULL;
  FBeforeDeleteDisableCount = 0;
  FReqEdBkColor             = clWindow;

  __treeNode = NULL;
 }

//---------------------------------------------------------------------------

__fastcall TICSDocBaseEdit::~TICSDocBaseEdit()
 {
  FEditTV->PopupMenu = NULL;
  delete FEditPM;
  delete FMainBM;
  FreeXML();
  if (FSplitterProps)
   delete FSplitterProps;
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::InitCommonTB()
 {
  FCommonTB                        = FMainBM->AddToolBar(); //+!!!
  FCommonTB->Name                  = "CommonTB";
  FCommonTB->Caption               = FMT(icsDocBaseEditCommonTBCaption);
  FCommonTB->AllowClose            = false;
  FCommonTB->AllowCustomizing      = false;
  FCommonTB->AllowQuickCustomizing = false;
  FCommonTB->AllowReset            = false;
  FCommonTB->OneOnRow              = false;
  FCommonTB->WholeRow              = false;
  FCommonTB->Row                   = 1;
  FCommonTB->DockedLeft            = 0;

  //FCommonTB->Parent          = this;
  //FCommonTB->DragHandleStyle = dhDouble;
  //FCommonTB->CurrentDock     = FMainBM;
  //FCommonTB->DefaultDock     = FMainBM;
  //FCommonTB->LastDock        = FMainBM;
  //FCommonTB->CloseButton     = false;
  //FCommonTB->DockableTo      = TTBDockableTo() << dpTop;
  //FCommonTB->Images          = FImageList;
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::InitEditTB()
 {
  FEditTB                        = FMainBM->AddToolBar(); //+!!!
  FEditTB->Name                  = "EditTB";
  FEditTB->Caption               = FMT(icsDocBaseEditEditTBCaption);
  FEditTB->AllowClose            = false;
  FEditTB->AllowCustomizing      = false;
  FEditTB->AllowQuickCustomizing = false;
  FEditTB->AllowReset            = false;
  FEditTB->OneOnRow              = false;
  FEditTB->WholeRow              = false;
  FEditTB->Row                   = 1;
  FEditTB->DockedLeft            = 46;
  //FEditTB->Parent          = this;
  //FEditTB->DragHandleStyle = dhDouble;
  //FEditTB->CurrentDock     = FMainBM;
  //FEditTB->DefaultDock     = FMainBM;
  //FEditTB->LastDock        = FMainBM;
  //FEditTB->DockableTo      = TTBDockableTo() << dpTop << dpLeft;
  //FEditTB->CloseButton     = false;
  //FEditTB->Images          = FImageList;
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::InitSplitter()
 {
  FSplitter          = new TSplitter(this);
  FSplitter->Parent  = this;
  FSplitter->Beveled = true;
  FSplitter->Width   = 5;
  FSplitter->Name    = "Splitter";
  FSplitterProps     = new TICSDocControlProps(FSplitter);
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::InitEditTV(TWinControl * AParent)
 {
  FEditTV                   = new TTreeView(this);
  FEditTV->Parent           = AParent;
  FEditTV->Align            = alClient;
  FEditTV->Name             = "EditTV";
  FEditTV->HideSelection    = false;
  FEditTV->ReadOnly         = true;
  FEditTV->StateImages      = FImageList;
  FEditTV->RightClickSelect = true;
  FEditTV->PopupMenu        = FEditPM;
  FEditTV->OnContextPopup   = EditTVContextPopup;
  FEditTV->OnChange         = EditTVChange;
  FEditTV->OnCustomDrawItem = EditTVDrawItem;
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::InitAction(TAction *& AAction,
    UnicodeString  ACaption,
    UnicodeString  AHint,
    UnicodeString  AShortCut,
    int AImageIndex,
    TNotifyEvent AExecuter)
 {
  AAction             = new TAction(this);
  AAction->ActionList = FActionList;
  AAction->Caption    = ACaption;
  AAction->Hint       = AHint;
  if (AShortCut.Length())
   AAction->ShortCut = TextToShortCut(AShortCut);
  AAction->ImageIndex = AImageIndex;
  AAction->Enabled    = false;
  AAction->HelpType   = htContext;
  AAction->OnExecute  = AExecuter;
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::LoadImageListBitmap(UnicodeString  ResName)
 {
  bool RC = FImageList->GetInstRes(reinterpret_cast<int>(HInstance),
      rtBitmap,
      ResName,
      FImageList->Width,
      TLoadResources(),
      clFuchsia);
  if (!RC)
   ShowMessage("������ �������� ������� \"" + ResName + "\"");
 }

//---------------------------------------------------------------------------
void __fastcall TICSDocBaseEdit::CreateTBItem(TdxBarItemLinks * AParent, TdxBarLargeButton *& ATBItem, TAction * AAction, bool ACanBeginGroup)
 {//�������� ������� ������ TdxBarLargeButton
  ATBItem = new TdxBarLargeButton(this);
  AParent->Add()->Item = ATBItem;
  ATBItem->Action = AAction;
  if (ATBItem->LinkCount)
   ATBItem->Links[0]->BeginGroup = ACanBeginGroup;
 }
//---------------------------------------------------------------------------
void __fastcall TICSDocBaseEdit::AddTBItem(TdxBarItemLinks * AParent, TdxBarLargeButton * ATBItem, bool ACanBeginGroup)
 {//���������� ������� ������ TdxBarLargeButton
  AParent->Add()->Item = ATBItem;
  if (ATBItem->LinkCount)
   ATBItem->Links[0]->BeginGroup = ACanBeginGroup;
 }
//---------------------------------------------------------------------------
void __fastcall TICSDocBaseEdit::CreateTBSubMenuItem(TdxBarItemLinks * AParent, TdxBarSubItem *& ATBItem, bool ACanBeginGroup)
 {//�������� ������� ������ TdxBarSubItem
  ATBItem = new TdxBarSubItem(this);
  AParent->Add()->Item = ATBItem;
  if (ATBItem->LinkCount)
   ATBItem->Links[0]->BeginGroup = ACanBeginGroup;
 }
//---------------------------------------------------------------------------
void __fastcall TICSDocBaseEdit::CreateTBSubMenuCleckedItem(TdxBarItemLinks * AParent, TdxBarLargeButton *& ATBItem, TdxBarPopupMenu *& ATBMenuItem, TAction * AAction, bool ACanBeginGroup)
 {//�������� ������� ������ TdxBarLargeButton + TdxBarPopupMenu
  CreateTBItem(AParent, ATBItem, AAction, ACanBeginGroup);
  ATBItem->ButtonStyle    = bsDropDown;
  ATBMenuItem             = new TdxBarPopupMenu(this);
  ATBMenuItem->BarManager = AParent->BarManager;
  ATBItem->DropDownMenu   = ATBMenuItem;
 }
//---------------------------------------------------------------------------
void __fastcall TICSDocBaseEdit::CreatePMItem(TPopupMenu * AParent, TMenuItem *& AItem, TAction * AAction)
 {//�������� ������� PM
  AItem = new TMenuItem(AParent);
  if (!AAction)
   {
   AItem->Caption = "-";
   AItem->Enabled = false;
   }
  else
   AItem->Action = AAction;
  AParent->Items->Add(AItem);
 }
//---------------------------------------------------------------------------
void __fastcall TICSDocBaseEdit::CheckEditTVExistence()
 {
  if (!FEditTV)
   throw DKClasses::EMethodError(__FUNC__, "FEditTV not created.");
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::FreeXML()
 {
  if (FCreated)
   {
    DELETE_OBJ(FXML);
   }
  else
   FXML = NULL;
 }

// ***************************************************************************
// ********************** ��������������� ������ *****************************
// ***************************************************************************

void __fastcall TICSDocBaseEdit::SetName(const UnicodeString Value)
 {
  inherited::SetName(Value);
  Caption = "";
 }

//---------------------------------------------------------------------------

//void __fastcall TICSDocBaseEdit::SetExtNOM_DLL ( const Variant &AValue )
//{
//FExtNOM_DLL = AValue;
//}

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::SetXMLContainer(TAxeXMLContainer * AValue)
 {
  FXMLContainer = AValue;
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::SetNmklGUI(UnicodeString  AValue)
 {
  FNmklGUI = AValue;
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::SetDomainNumber(int AValue)
 {
  if (AValue < 0)
   throw DKClasses::EInvalidArgument(
      __FUNC__,
      "AValue",
      FMT1(icsDocBaseEditErrorSetDomainNum, 0)
       );
  FDomainNumber = AValue;
 }

//---------------------------------------------------------------------------

TAxeXMLContainer * __fastcall TICSDocBaseEdit::GetXMLContainer()
 {
  if (!FXMLContainer)
   throw DKClasses::EMethodError(__FUNC__, FMT(icsDocErrorXMLContainerNotDefined));
  return FXMLContainer;
 }

//---------------------------------------------------------------------------

TTagNode * __fastcall TICSDocBaseEdit::GetNmkl()
 {
  TTagNode * Result = GetXMLContainer()->GetXML(FNmklGUI);
  if (!Result)
   throw DKClasses::EMethodError(
      __FUNC__,
      FMT1(icsDocBaseEditErrorGetNom, FNmklGUI)
       );
  return Result;
 }

//---------------------------------------------------------------------------

//����� ������ � ��������� ������� � ������������

TTagNode * __fastcall TICSDocBaseEdit::GetDomainNode()
 {
  int i = 0;
  TTagNode * DomainNode = Nmkl->GetChildByName("domain");
  while ((i++ < DomainNumber) && DomainNode)
   DomainNode = DomainNode->GetNext();
  if (!DomainNode)
   throw DKClasses::EMethodError(
      __FUNC__,
      FMT1(icsDocBaseEditErrorGetDomain, IntToStr((int)DomainNumber))
       );
  return DomainNode;
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::SetSplitterProps(const TICSDocControlProps * AValue)
 {
  FSplitterProps->Assign(const_cast<TICSDocControlProps *>(AValue));
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::SetXMLTitle(const UnicodeString AValue)
 {
  if (FXMLTitle != AValue)
   {
    FXMLTitle = AValue;
    if (FEditTV && FEditTV->Items->GetFirstNode())
     FEditTV->Items->GetFirstNode()->Text = FXMLTitle;
   }
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::SetReqEdBkColor(TColor AValue)
 {
  if (FReqEdBkColor != AValue)
   FReqEdBkColor = AValue;
 }

//---------------------------------------------------------------------------

//���������� ����������� �������� �������� ������������� ������� ����� ���������
//������� BeforeDelete

bool __fastcall TICSDocBaseEdit::CanDelete(TTreeNode * TreeNode)
 {
  return (FBeforeDelete && (!FBeforeDeleteDisableCount))
      ? FBeforeDelete(this, reinterpret_cast<TTagNode *>(TreeNode->Data))
      : true;
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::EnableBeforeDeleteEvent()
 {
  if (FBeforeDeleteDisableCount)
   FBeforeDeleteDisableCount-- ;
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::DisableBeforeDeleteEvent()
 {
  FBeforeDeleteDisableCount++ ;
 }

//---------------------------------------------------------------------------

TTagNode * __fastcall TICSDocBaseEdit::GetCurrentTagNode()
 {
  return (FEditTV->Selected) ? reinterpret_cast<TTagNode *>(FEditTV->Selected->Data) : NULL;
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::UpdateTreeNodeData(TTagNode * TagNode, bool bClear)
 {
  if (TagNode->Ctrl)
   reinterpret_cast<TTreeNode *>(TagNode->Ctrl)->Data = (bClear)
       ? NULL
       : reinterpret_cast<void *>(TagNode);
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::UpdateTagNodeCtrl(TTreeNode * TreeNode, bool bClear)
 {
  if (TreeNode->Data && reinterpret_cast<TObject *>(TreeNode->Data)->InheritsFrom(__classid(TTagNode)))
   reinterpret_cast<TTagNode *>(TreeNode->Data)->Ctrl = (bClear)
       ? NULL
       : reinterpret_cast<TWinControl *>(TreeNode);
 }

//---------------------------------------------------------------------------

//����������� xml-��������� �� ������ TreeView

void __fastcall TICSDocBaseEdit::LoadXML(TTagNode * XMLLogicRoot, TTreeNode * ARootNode)
 {
  CheckEditTVExistence();
  TTreeView * TreeView;
  TTreeNode * RootNode;
  if (ARootNode)
   {
    TreeView = dynamic_cast<TTreeView *>(ARootNode->TreeView);
    TreeView->Items->BeginUpdate();
    RootNode = ARootNode;
   }
  else
   {
    TreeView = FEditTV;
    TreeView->Items->BeginUpdate();
    RootNode = FEditTV->Items->AddObject(NULL, "", NULL);
   }
  //XMLLogicRoot->OnCopyItem = XMLToTree;
  ToTree(XMLLogicRoot, RootNode);
  //XMLLogicRoot->OnCopyItem = NULL;

  DropTreeNodesAfterLoadXML(RootNode);

  TreeView->Selected = RootNode;
  TreeView->Selected->Expand(true);
  TreeView->Selected->MakeVisible();
  TreeView->Items->EndUpdate();
 }

//---------------------------------------------------------------------------
void __fastcall TICSDocBaseEdit::ToTree(TTagNode * ANode, TTreeNode * ATreeNode)
 {
  if (!this)
   throw ETagNodeError("Error in " + AnsiString(__FUNC__) + " -> object is NULL");
  TTreeNodes * Src = ((TTreeView *)ATreeNode->TreeView)->Items;
  ATreeNode->Text = ANode->Name;
  ATreeNode->Data = NULL;
  XMLToTree(ANode, ATreeNode);
  FToTree(ANode, Src, ATreeNode, XMLToTree);
 }
//---------------------------------------------------------------------------
void __fastcall TICSDocBaseEdit::FToTree(TTagNode * ANode, TTreeNodes * Src, TTreeNode * xNode, TOnCopyItem AOnCopy)
 {
  if (!this)
   throw ETagNodeError("Error in " + AnsiString(__FUNC__) + " -> object is NULL");
  TTagNode * ItNode = ANode->GetFirstChild();
  TTreeNode * xSrc;
  void * xData;
  AnsiString xName;
  for (int i = 0; i < ANode->Count; i++)
   {
    xName = ItNode->Name;
    xData = NULL;
    xSrc  = Src->AddChildObject(xNode, xName, xData);
    if (AOnCopy)
     AOnCopy(ItNode, xSrc);
    FToTree(ItNode, Src, xSrc, AOnCopy);
    ItNode = ItNode->GetNext();
   }
  return;
 }
//---------------------------------------------------------------------------

//��������� �������� xml-���� ��� ����������� xml-��������� �� ������ TreeView

void __fastcall TICSDocBaseEdit::XMLToTree(TTagNode * TagNode, TTreeNode * TreeNode)
 {
  TreeNode->Data = reinterpret_cast<void *>(TagNode);
  UpdateTagNodeCtrl(TreeNode);

  //��������� �������� ����
  TreeNode->Text = GetXMLNodeViewText(TagNode);
 }

//---------------------------------------------------------------------------

//�������� ����� ������ TreeView, ��������������� xml-����� � ���������� �������
//( � ��������� NodesNames ����� ����������� ����� ',' )

void __fastcall TICSDocBaseEdit::DropTreeNodes(TTreeNode * XMLLogicRoot, UnicodeString NodesNames)
 {
  CheckEditTVExistence();
  TTreeView * TreeView = dynamic_cast<TTreeView *>(XMLLogicRoot->TreeView);
  TreeView->Items->BeginUpdate();
  TTreeNode * TreeNode = XMLLogicRoot;
  while (TreeNode &&
      ((TreeNode == XMLLogicRoot) ||
      ((TreeNode != XMLLogicRoot) &&
      (TreeNode->Level > XMLLogicRoot->Level)
      )
      )
      )
   {
    TTagNode * TagNode = reinterpret_cast<TTagNode *>(TreeNode->Data);
    TTreeNode * NextNode;
    if (TagNode->CmpName(NodesNames))
     {
      TTreeNode * PrevNode = TreeNode->GetPrev();
      UpdateTagNodeCtrl(TreeNode, true);
      if (PrevNode)
       {
        TreeNode->Delete();
        NextNode = PrevNode->GetNext();
       }
      else
       {
        NextNode = TreeNode->getNextSibling();
        TreeNode->Delete();
       }
     }
    else
     NextNode = TreeNode->GetNext();
    TreeNode = NextNode;
   }
  TreeView->Items->EndUpdate();
 }

// ***************************************************************************
// ********************** ����������� ������� ********************************
// ***************************************************************************

//��������� ���� TreeView ��� ������� ������ ������ ����

void __fastcall TICSDocBaseEdit::EditTVContextPopup(TObject * Sender, const TPoint & MousePos, bool & Handled)
 {
  //������� ������������� ��� ������� ������ ������ ����
  if (!((MousePos.x == -1) && (MousePos.y == -1)))
   if (FEditTV->Selected)
    {
     FEditTV->Selected->Selected = true;
     FEditTV->Selected->Focused  = true;
    }
 }

//---------------------------------------------------------------------------

//����������� ������ ������ �� TreeNodes

bool __fastcall TICSDocBaseEdit::ISToTree(TTagNode * ItTag, void * Src1, void * Src2, void * Src3, void * Src4)
 {
  reinterpret_cast<TTreeNodes *>(Src1)->AddChildObject(__treeNode,
      ItTag->AV["name"],
      (TObject *)ItTag);
  return false;
 }

// ***************************************************************************
// ********************** �������� ������ ************************************
// ***************************************************************************

//�������� �������

TTagNode * __fastcall TICSDocBaseEdit::GetPassport()
 {
  if (!FXML)
   throw DKClasses::EMethodError(__FUNC__, FMT(icsDocBaseEditErrorNoXML));
  return FXML->GetFirstChild();
 }

//---------------------------------------------------------------------------

//��������� �������

bool __fastcall TICSDocBaseEdit::SavePassport(TTagNode * APassport)
 {
  if (!APassport)
   throw DKClasses::ENullArgument(__FUNC__, "APassport");
  APassport->Assign(GetPassport(), true);
  return true;
 }

//---------------------------------------------------------------------------

//��������� ������������� xml

void __fastcall TICSDocBaseEdit::Save(TTagNode * AXML)
 {
  if (!AXML)
   throw DKClasses::ENullArgument(__FUNC__, "AXML");
  if (!FXML)
   throw DKClasses::EMethodError(__FUNC__, FMT(icsDocBaseEditErrorNoXML));
  AXML->Assign(FXML, true);
 }

//---------------------------------------------------------------------------

//������� ������������� xml �� ���������

bool __fastcall TICSDocBaseEdit::Delete()
 {
  CheckEditTVExistence();
  bool bResult;
  TTreeNode * RootNode = FEditTV->Items->GetFirstNode();
#pragma option push -w-pia
  if (bResult = (RootNode) ? CanDelete(RootNode) : true)
#pragma option pop
   {
    FEditTV->Items->Clear();
    FreeXML();
    for (int i = 0; i < FActionList->ActionCount; i++)
     dynamic_cast<TAction *>(FActionList->Actions[i])->Enabled = false;
    FModified = false;
    FCreated  = false;
   }
  return bResult;
 }

//---------------------------------------------------------------------------

//�������� ������ ���� ������ TreeView

void __fastcall TICSDocBaseEdit::FocusEditTVFirst()
 {
  CheckEditTVExistence();
  FEditTV->SetFocus();
  FEditTV->Selected = FEditTV->Items->GetFirstNode();
 }

//---------------------------------------------------------------------------

bool __fastcall TICSDocBaseEdit::IsEmpty()
 {
  CheckEditTVExistence();
  return (FEditTV->Items->Count == 1);
 }

//---------------------------------------------------------------------------

bool __fastcall TICSDocBaseEdit::IsNone()
 {
  CheckEditTVExistence();
  return (FEditTV->Items->Count == 0);
 }

//---------------------------------------------------------------------------

UnicodeString __fastcall TICSDocBaseEdit::NewUID()
 {
  if (FParentXML)
   return FParentXML->NewUID();
  else if (FXML)
   return FXML->NewUID();
  else
   throw DKClasses::EMethodError(
      __FUNC__,
      FMT(icsDocBaseEditErrorGenUID)
       );
 }

// ***************************************************************************
// ********************** �������������� ������ ******************************
// ***************************************************************************

//����������� ������ ��������� � �������� �������������� ���������

void __fastcall TICSDocBaseEdit::GetLinkedIS(TTagNode * ABaseIS, TStringList * AISList, UnicodeString TagName)
 {
  if (ABaseIS->GetChildByName(TagName))
   AISList->AddObject("", ABaseIS);
  TTagNode * itNode = ABaseIS->GetChildByName("refers");
  if (!itNode)
   return;
  itNode = itNode->GetFirstChild();
  TStringList * FSearchPath;
  try
   {
    FSearchPath = new TStringList;
    //��������� ������ �� �������� � ������ ����������� ����
    FSearchPath->Add(GetGUI(ABaseIS) + "." + ABaseIS->AV["uid"]);
    while (itNode)
     {
      //�������� � ��� �������� �������� � �����
      FGetLinkedIS(itNode->GetRefer("ref"), FSearchPath, AISList);
      itNode = itNode->GetNext();
     }
   }
  __finally
   {
    if (FSearchPath)
     delete FSearchPath;
   }
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::FGetLinkedIS(TTagNode * ALinkIS, TStringList * ASearchPath, TStringList * AISList, UnicodeString TagName)
 {
  if (ASearchPath->IndexOf(GetGUI(ALinkIS) + "." + ALinkIS->AV["uid"]) == -1)
   {
    //��������� ������ �� �������� � ������ ����������� ����
    ASearchPath->Add(GetGUI(ALinkIS) + "." + ALinkIS->AV["uid"]);
    if (ALinkIS->GetChildByName(TagName))
     AISList->AddObject("", (TObject *)ALinkIS);
   }
  else
   return;
  TTagNode * itNode = ALinkIS->GetChildByName("refers");
  if (!itNode)
   return;
  itNode = itNode->GetFirstChild();
  while (itNode)
   {//�������� � ��� �������� �������� � �����
    FGetLinkedIS(itNode->GetRefer("ref"), ASearchPath, AISList);
    itNode = itNode->GetNext();
   }
 }

//---------------------------------------------------------------------------

UnicodeString __fastcall TICSDocBaseEdit::GetGUI(TTagNode * ANode)
 {
  return ANode->GetRoot()->GetChildByName("passport")->AV["gui"];
 }

//---------------------------------------------------------------------------

UnicodeString __fastcall TICSDocBaseEdit::GetExtRef(TTagNode * ANode)
 {
  return GetGUI(ANode) + "." + ANode->AV["uid"];
 }

//---------------------------------------------------------------------------

int __fastcall TICSDocBaseEdit::GetISList(TTagNode * MainIS, TTreeNodes * TreeNodes, UnicodeString TagName)
 {
  //��������� ������ �������������� ���������, ��������� � ��������
  TStringList * ISList = new TStringList;
  GetLinkedIS(MainIS, ISList, TagName);
  TreeNodes->BeginUpdate();
  for (int i = 0; i < ISList->Count; i++)
   {
    TTagNode * ISNode = dynamic_cast<TTagNode *>(ISList->Objects[i]);
    if (ISNode->GetChildByName(TagName))
     TreeNodes->AddObject(NULL, ISNode->AV["name"], reinterpret_cast<void *>(ISNode));
   }
  delete ISList;

  //����������� �������������� ��������� � ��������������� ������ ������ �� RulesTV
  __treeNode = TreeNodes->GetFirstNode();
  while (__treeNode)
   {
    reinterpret_cast<TTagNode *>(__treeNode->Data)->GetChildByName(TagName)->_Iterate1st(ISToTree,
        reinterpret_cast<void *>(TreeNodes)
        );
    __treeNode = __treeNode->getNextSibling();
   }
  TreeNodes->EndUpdate();
  return TreeNodes->Count;
 }

//---------------------------------------------------------------------------

int __fastcall TICSDocBaseEdit::GetLinkedISAttrs(UnicodeString  LinkISExtRef, TTreeNodes * TreeNodes)
 {
  TreeNodes->BeginUpdate();
  TTagNode * ISNode = GetXMLContainer()->GetRefer(LinkISExtRef);
  if (ISNode && ISNode->GetChildByName("ATTRS"))
   TreeNodes->AddObject(NULL, ISNode->AV["name"], reinterpret_cast<void *>(ISNode));

  __treeNode = TreeNodes->GetFirstNode();
  while (__treeNode)
   {
    reinterpret_cast<TTagNode *>(__treeNode->Data)->GetChildByName("ATTRS")->_Iterate1st(ISToTree,
        reinterpret_cast<void *>(TreeNodes)
        );
    __treeNode = __treeNode->getNextSibling();
   }
  TreeNodes->EndUpdate();
  return TreeNodes->Count;
 }

//---------------------------------------------------------------------------

/*
 * Assign ��� ����� TTreeNode
 *
 *
 * ���������:
 *   [in]  Dest      - TreeNode - ��������
 *   [in]  Src       - TreeNode - ��������
 *   [in]  IsRecurse - ������� ������������ ����������
 *                     [default = true ]
 *
 * ����������:
 *   ���������� Text, Data, ImageIndex, SelectedIndex, OverlayIndex, Focused, DropTarget, Cut,
 *   HasChildren � StateIndex
 *
 */

void __fastcall TICSDocBaseEdit::TreeNodesAssign(TTreeNode * Dest, const TTreeNode * Src, bool IsRecurse)
 {
  if (!Dest)
   throw DKClasses::ENullArgument(__FUNC__, "Dest");
  if (!Src)
   throw DKClasses::ENullArgument(__FUNC__, "Src");
  TTreeView * TreeView = dynamic_cast<TTreeView *>(Dest->TreeView);
  TreeView->Items->BeginUpdate();
  Dest->Assign(const_cast<TTreeNode *>(Src));
  Dest->StateIndex = Src->StateIndex;
  UpdateTagNodeCtrl(Dest);
  if (IsRecurse)
   {
    Dest->DeleteChildren();
    for (int i = 0; i < const_cast<TTreeNode *>(Src)->Count; i++)
     {
      TreeNodesAssign(dynamic_cast<TTreeView *>(Dest->TreeView)->Items->AddChild(Dest, ""),
          const_cast<TTreeNode *>(Src)->Item[i],
          IsRecurse
          );
     }
   }
  TreeView->Items->EndUpdate();
 }

//---------------------------------------------------------------------------

TTreeNode * __fastcall TICSDocBaseEdit::InsertTreeNodeAfter(TTreeNode * AInsTreeNode, UnicodeString  AInsTreeText, UnicodeString  ATagNodeName)
 {
  TTreeNode * NewTreeNode;
  TTagNode * NewTagNode;
  FEditTV->Items->BeginUpdate();
  if (TTreeNode * InsNextNode = AInsTreeNode->getNextSibling())
   {
    NewTagNode  = reinterpret_cast<TTagNode *>(AInsTreeNode->Data)->Insert(ATagNodeName, false);
    NewTreeNode = FEditTV->Items->InsertObject(InsNextNode,
        AInsTreeText,
        reinterpret_cast<void *>(NewTagNode)
        );
   }
  else
   {
    NewTagNode  = reinterpret_cast<TTagNode *>(AInsTreeNode->Parent->Data)->AddChild(ATagNodeName);
    NewTreeNode = FEditTV->Items->AddChildObject(AInsTreeNode->Parent,
        AInsTreeText,
        reinterpret_cast<void *>(NewTagNode)
        );
   }
  FEditTV->Items->EndUpdate();
  UpdateTagNodeCtrl(NewTreeNode);
  FModified = true;
  return NewTreeNode;
 }

//---------------------------------------------------------------------------

TTreeNode * __fastcall TICSDocBaseEdit::AddChildTreeNodeFirst(TTreeNode * AParentTreeNode, UnicodeString  AChildTreeText, UnicodeString  ATagNodeName)
 {
  TTreeNode * NewTreeNode;
  TTagNode * ParentTagNode = reinterpret_cast<TTagNode *>(AParentTreeNode->Data);
  TTagNode * NewTagNode = (ParentTagNode->Count)
      ? ParentTagNode->InsertChild(ParentTagNode->GetFirstChild(), ATagNodeName, true)
      : ParentTagNode->AddChild(ATagNodeName);
  FEditTV->Items->BeginUpdate();
  NewTreeNode = FEditTV->Items->AddChildObjectFirst(AParentTreeNode,
      AChildTreeText,
      reinterpret_cast<void *>(NewTagNode)
      );
  FEditTV->Items->EndUpdate();
  UpdateTagNodeCtrl(NewTreeNode);
  FModified = true;
  return NewTreeNode;
 }

//---------------------------------------------------------------------------

void __fastcall TICSDocBaseEdit::DeleteCurrentNode()
 {
  if (CanDelete(FEditTV->Selected))
   {
    UnicodeString DeletedNodeName = GetCurrentTagNode()->Name;
    BeforeCurrentNodeDelete();
    delete GetCurrentTagNode();
    FModified = true;
    FEditTV->Items->BeginUpdate();
    FEditTV->Selected->Delete();
    FEditTV->Selected->Text = GetXMLNodeViewText(GetCurrentTagNode());
    AfterCurrentNodeDelete(DeletedNodeName);
    FEditTV->Items->EndUpdate();
   }
 }

//---------------------------------------------------------------------------

bool __fastcall TICSDocBaseEdit::MoveTreeNodeUp(TTreeNode * ATreeNode, bool bGenerateOnChange, bool bMoveTagNode)
 {
  if (ATreeNode && ATreeNode->getPrevSibling())
   {
    if (bMoveTagNode)
     {
      TTagNode * ndCur = reinterpret_cast<TTagNode *>(ATreeNode->Data);
      TTagNode * ndPrev = reinterpret_cast<TTagNode *>(ATreeNode->getPrevSibling()->Data);
      ATreeNode->Data = reinterpret_cast<void *>(ndPrev->Insert("", true));
      TTagNode * TempNode = NULL;
      try
       {
        TempNode = new TTagNode;
        TempNode->Assign(ndCur, true);
        delete ndCur;
        reinterpret_cast<TTagNode *>(ATreeNode->Data)->Assign(TempNode, true);
       }
      __finally
       {
        if (TempNode)
         delete TempNode;
       }
      TFakeObj FakeObj;
      reinterpret_cast<TTagNode *>(ATreeNode->Data)->_Iterate(FakeObj.InternalUpdateTreeNodeData);
     }
    TTreeView * TreeView = dynamic_cast<TTreeView *>(ATreeNode->TreeView);
    if (TreeView)
     TreeView->Items->BeginUpdate();
    ATreeNode->MoveTo(ATreeNode->getPrevSibling(), naInsert);
    //-----��������� VCL ����
    int nStateIndex = ATreeNode->StateIndex;
    ATreeNode->StateIndex = 0;
    ATreeNode->StateIndex = nStateIndex;
    //-----------------------
    if (TreeView)
     TreeView->Items->EndUpdate();
    if (bGenerateOnChange && TreeView && TreeView->OnChange)
     TreeView->OnChange(TreeView, ATreeNode);
    return true;
   }
  else
   return false;
 }

//---------------------------------------------------------------------------

bool __fastcall TICSDocBaseEdit::MoveTreeNodeDown(TTreeNode * ATreeNode, bool bGenerateOnChange, bool bMoveTagNode)
 {
  if (ATreeNode && ATreeNode->getNextSibling())
   {
    if (bMoveTagNode)
     {
      TTagNode * ndCur = reinterpret_cast<TTagNode *>(ATreeNode->Data);
      TTagNode * ndNext = reinterpret_cast<TTagNode *>(ATreeNode->getNextSibling()->Data);
      ATreeNode->Data = reinterpret_cast<void *>(ndNext->Insert("", false));
      TTagNode * TempNode = NULL;
      try
       {
        TempNode = new TTagNode;
        TempNode->Assign(ndCur, true);
        delete ndCur;
        reinterpret_cast<TTagNode *>(ATreeNode->Data)->Assign(TempNode, true);
       }
      __finally
       {
        if (TempNode)
         delete TempNode;
       }
      TFakeObj FakeObj;
      reinterpret_cast<TTagNode *>(ATreeNode->Data)->_Iterate(FakeObj.InternalUpdateTreeNodeData);
     }
    TTreeView * TreeView = dynamic_cast<TTreeView *>(ATreeNode->TreeView);
    if (TreeView)
     TreeView->Items->BeginUpdate();
    if (ATreeNode->getNextSibling()->getNextSibling())
     ATreeNode->MoveTo(ATreeNode->getNextSibling()->getNextSibling(), naInsert);
    else
     ATreeNode->MoveTo(ATreeNode->getNextSibling(), naAdd);
    //-----��������� VCL ����
    int nStateIndex = ATreeNode->StateIndex;
    ATreeNode->StateIndex = 0;
    ATreeNode->StateIndex = nStateIndex;
    //-----------------------
    if (TreeView)
     TreeView->Items->EndUpdate();
    if (bGenerateOnChange && TreeView && TreeView->OnChange)
     TreeView->OnChange(TreeView, ATreeNode);
    return true;
   }
  else
   return false;
 }

//---------------------------------------------------------------------------
