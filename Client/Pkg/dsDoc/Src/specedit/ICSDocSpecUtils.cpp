/*
  ����        - ICSDocSpecUtils.cpp
  ������      - ��������������� (ICS)
  ��������    - ������ ��� ������ �� ��������������.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 10.01.2007
*/
//---------------------------------------------------------------------------

#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "ICSDocSpecUtils.h"

#include "ICSDateUtil.hpp"
#include "DKClasses.h"
#include "DKVCLVerInfo.h"
#include "DKXMLUtils.h"

#include "ICSDocGlobals.h"
#include "ICSDocSpec.h"
#include "ICSDocFilterUtils.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

namespace ICSDocSpec
{

//---------------------------------------------------------------------------

const UnicodeString csSpecDefVersion        = "3.0.0";
const UnicodeString csSpecDefDefaultVersion = "3.0.0";

//---------------------------------------------------------------------------

inline void __fastcall CheckSpecParam(UnicodeString AFuncName, TTagNode *ASpec)
{
  if (!ASpec)
    throw DKClasses::ENullArgument(AFuncName, "ASpec");
  if (ASpec->Name != "specificator")
    throw DKClasses::EInvalidArgument(
      AFuncName,
      "ASpec",
      FMT1(icsDocErrorTagEq,"specificator")
    );
}

//---------------------------------------------------------------------------

struct UpdateFO : public unary_function<TTagNode*, void>
{
  bool* FModified;
  UpdateFO(bool* AModified) : FModified(AModified) {}
};

struct UpdateSpecAttrs_2_1_4FO : public UpdateFO
{
  UpdateSpecAttrs_2_1_4FO(bool* AModified) : UpdateFO(AModified) {}
  void operator() (TTagNode *TagNode);
};

struct UpdateSpecAttrs_2_1_5FO : public UpdateFO
{
  UpdateSpecAttrs_2_1_5FO(bool* AModified) : UpdateFO(AModified) {}
  void operator() (TTagNode *TagNode);
};

struct UpdateSpec_2_1_5FO : public UpdateFO
{
  UpdateSpec_2_1_5FO(bool* AModified) : UpdateFO(AModified) {}
  void operator() (TTagNode *TagNode);
};

struct UpdateSpec_3_0_0FO : public UpdateFO
{
  UpdateSpec_3_0_0FO(bool* AModified) : UpdateFO(AModified) {}
  void operator() (TTagNode *TagNode);
};

//---------------------------------------------------------------------------

void UpdateSpecAttrs_2_1_4FO::operator() (TTagNode *TagNode)
{
  if      (TagNode->CmpName("condrules"))
  {
    if (ICSDocFilter::UpdateCondRules_2_0_1(TagNode, false))
      *FModified = true;
  }
  else if (TagNode->CmpName("passport"))
  {
    UpdateXMLAttr(TagNode, "save_bd"    , "0"    , FModified);
    UpdateXMLAttr(TagNode, "perioddef"  , "month", FModified);
    UpdateXMLAttr(TagNode, "orientation", "group", FModified);
  }
  else if (TagNode->CmpName("style"))
  {
    UpdateXMLAttr(TagNode, "align"    , "left"           , FModified);
    UpdateXMLAttr(TagNode, "fontname" , "Times New Roman", FModified);
    UpdateXMLAttr(TagNode, "fontsize" , "12"             , FModified);
    UpdateXMLAttr(TagNode, "wordwrap" , "1"              , FModified);
    UpdateXMLAttr(TagNode, "strike"   , "0"              , FModified);
    UpdateXMLAttr(TagNode, "underline", "0"              , FModified);
    UpdateXMLAttr(TagNode, "bold"     , "0"              , FModified);
    UpdateXMLAttr(TagNode, "italic"   , "0"              , FModified);
  }
  else if (TagNode->CmpName("pagesetup"))
  {
    UpdateXMLAttr(TagNode, "topfield"   , "2000" , FModified);
    UpdateXMLAttr(TagNode, "bottomfield", "2000" , FModified);
    UpdateXMLAttr(TagNode, "leftfield"  , "3000" , FModified);
    UpdateXMLAttr(TagNode, "rightfield" , "1500" , FModified);
    UpdateXMLAttr(TagNode, "orientation", "0"    , FModified);
    UpdateXMLAttr(TagNode, "width"      , "21000", FModified);
    UpdateXMLAttr(TagNode, "height"     , "29700", FModified);
  }
  else if (TagNode->CmpName("createstep"))
  {
    UpdateXMLAttr(TagNode, "docrefmain"  , ""     , FModified);
    UpdateXMLAttr(TagNode, "periodtype"  , "this" , FModified);
    UpdateXMLAttr(TagNode, "periodvalue" , ""     , FModified);
    UpdateXMLAttr(TagNode, "org_type"    , "child", FModified);
    UpdateXMLAttr(TagNode, "org_code"    , ""     , FModified);
  }
  else if (TagNode->CmpName("insrules,listrules"))
  {
    UpdateXMLAttr(TagNode, "ref"    , "", FModified);
    UpdateXMLAttr(TagNode, "refmain", "", FModified);
  }
  else if (TagNode->CmpName("tabrules"))
  {
    UpdateXMLAttr(TagNode, "thisrefmain", ""      , FModified);
    UpdateXMLAttr(TagNode, "refmain"    , ""      , FModified);
    UpdateXMLAttr(TagNode, "crosstype"  , "normal", FModified);
  }
  else if (TagNode->CmpName("instext"))
  {
    UpdateXMLAttr(TagNode, "uidmain", "" , FModified);
    UpdateXMLAttr(TagNode, "ref"    , "" , FModified);
    UpdateXMLAttr(TagNode, "refmain", "" , FModified);
    UpdateXMLAttr(TagNode, "style"  , "" , FModified);
    UpdateXMLAttr(TagNode, "newline", "0", FModified);
    UpdateXMLAttr(TagNode, "value"  , "" , FModified);
  }
  else if (TagNode->CmpName("inscription"))
  {
    UpdateXMLAttr(TagNode, "uidmain", "" , FModified);
    UpdateXMLAttr(TagNode, "style"  , "" , FModified);
    UpdateXMLAttr(TagNode, "newline", "0", FModified);
  }
  else if (TagNode->CmpName("list"))
  {
    UpdateXMLAttr(TagNode, "uidmain"        , "" ,    FModified);
    UpdateXMLAttr(TagNode, "refperiodparams", "" ,    FModified);
    UpdateXMLAttr(TagNode, "showzval"       , "1",    FModified);
    UpdateXMLAttr(TagNode, "border"         , "1",    FModified);
    UpdateXMLAttr(TagNode, "rownumbers"     , "1",    FModified);
    UpdateXMLAttr(TagNode, "hstyle"         , "" ,    FModified);
    UpdateXMLAttr(TagNode, "bstyle"         , "" ,    FModified);
    UpdateXMLAttr(TagNode, "width"          , "100%", FModified);
  }
  else if (TagNode->CmpName("sortel"))
  {
    UpdateXMLAttr(TagNode, "sorttype"   , "incr", FModified);
    UpdateXMLAttr(TagNode, "numberlevel", ""    , FModified);
    UpdateXMLAttr(TagNode, "numbertype" , "no"  , FModified);
    UpdateXMLAttr(TagNode, "doublevalue", "yes" , FModified);
  }
  else if (TagNode->CmpName("listcol"))
  {
    UpdateXMLAttr(TagNode, "uidmain", "", FModified);
    UpdateXMLAttr(TagNode, "hstyle" , "", FModified);
    UpdateXMLAttr(TagNode, "bstyle" , "", FModified);
    if (TICSDocSpec::IsListColLinkISGroup(TagNode))
    {
      UpdateXMLAttr(
        TagNode,
        "width",
        UnicodeString::StringOfChar(';', TagNode->GetChildByName("link_is")->GetCount(false, "is_attr")),
        FModified
      );
    }
    else
    {
      UpdateXMLAttr(TagNode, "width"  , "", FModified);
    }
  }
  else if (TagNode->CmpName("is_attr"))
  {
    UpdateXMLAttr(TagNode, "ref", "", FModified);
  }
  else if (TagNode->CmpName("link_is"))
  {
    UpdateXMLAttr(TagNode, "refperiodparams", "" , FModified);
    UpdateXMLAttr(TagNode, "union"          , "0", FModified);
  }
  else if (TagNode->CmpName("listcolgroup"))
  {
    UpdateXMLAttr(TagNode, "union", "0", FModified);
    UpdateXMLAttr(TagNode, "style", "" , FModified);
  }
  else if (TagNode->CmpName("table"))
  {
    UpdateXMLAttr(TagNode, "refperiodparams", ""   ,  FModified);
    UpdateXMLAttr(TagNode, "hstyle"         , ""   ,  FModified);
    UpdateXMLAttr(TagNode, "bstyle"         , ""   ,  FModified);
    UpdateXMLAttr(TagNode, "border"         , "1"  ,  FModified);
    UpdateXMLAttr(TagNode, "rownumbers"     , "1"  ,  FModified);
    UpdateXMLAttr(TagNode, "colnumbers"     , "1"  ,  FModified);
    UpdateXMLAttr(TagNode, "showzval"       , "1"  ,  FModified);
    UpdateXMLAttr(TagNode, "tablepriority"  , "row",  FModified);
    UpdateXMLAttr(TagNode, "width"          , "100%", FModified);
  }
  else if (TagNode->CmpName("trow,tcol"))
  {
    UpdateXMLAttr(TagNode, "ref"   , "", FModified);
    UpdateXMLAttr(TagNode, "hstyle", "", FModified);
    UpdateXMLAttr(TagNode, "bstyle", "", FModified);
    if (TagNode->CmpName("tcol"))
      UpdateXMLAttr(TagNode, "width" , "", FModified);
  }
  else if (TagNode->CmpName("name"))
  {
    UpdateXMLAttr(TagNode, "refmain", "", FModified);
    UpdateXMLAttr(TagNode, "comment", "", FModified);
  }
}

//---------------------------------------------------------------------------

void UpdateSpecAttrs_2_1_5FO::operator() (TTagNode *TagNode)
{
  if      (TagNode->CmpName("condrules"))
  {
    if (ICSDocFilter::UpdateCondRules_2_0_1(TagNode, false))
      *FModified = true;
  }
  else if (TagNode->CmpName("passport"))
  {
    UpdateXMLAttr(TagNode, "save_bd"    , "0"    , FModified);
    UpdateXMLAttr(TagNode, "perioddef"  , "1"    , FModified);
    UpdateXMLAttr(TagNode, "orientation", "group", FModified);
    UpdateXMLAttr(TagNode, "defversion" , "2.1.5", FModified);
  }
  else if (TagNode->CmpName("style"))
  {
    UpdateXMLAttr(TagNode, "align"    , "left"           , FModified);
    UpdateXMLAttr(TagNode, "fontname" , "Times New Roman", FModified);
    UpdateXMLAttr(TagNode, "fontsize" , "12"             , FModified);
    UpdateXMLAttr(TagNode, "wordwrap" , "1"              , FModified);
    UpdateXMLAttr(TagNode, "strike"   , "0"              , FModified);
    UpdateXMLAttr(TagNode, "underline", "0"              , FModified);
    UpdateXMLAttr(TagNode, "bold"     , "0"              , FModified);
    UpdateXMLAttr(TagNode, "italic"   , "0"              , FModified);
  }
  else if (TagNode->CmpName("pagesetup"))
  {
    UpdateXMLAttr(TagNode, "topfield"   , "2000" , FModified);
    UpdateXMLAttr(TagNode, "bottomfield", "2000" , FModified);
    UpdateXMLAttr(TagNode, "leftfield"  , "3000" , FModified);
    UpdateXMLAttr(TagNode, "rightfield" , "1500" , FModified);
    UpdateXMLAttr(TagNode, "orientation", "0"    , FModified);
    UpdateXMLAttr(TagNode, "width"      , "21000", FModified);
    UpdateXMLAttr(TagNode, "height"     , "29700", FModified);
  }
  else if (TagNode->CmpName("createstep"))
  {
    UpdateXMLAttr(TagNode, "docrefmain"  , ""     , FModified);
    UpdateXMLAttr(TagNode, "periodtype"  , "this" , FModified);
    UpdateXMLAttr(TagNode, "perioddef"   , "undef", FModified);
    UpdateXMLAttr(TagNode, "periodvalue" , ""     , FModified);
    UpdateXMLAttr(TagNode, "periodicity" , "equal", FModified);
    UpdateXMLAttr(TagNode, "org_type"    , "child", FModified);
    UpdateXMLAttr(TagNode, "org_code"    , ""     , FModified);
  }
  else if (TagNode->CmpName("insrules,listrules"))
  {
    UpdateXMLAttr(TagNode, "ref"    , "", FModified);
    UpdateXMLAttr(TagNode, "refmain", "", FModified);
  }
  else if (TagNode->CmpName("tabrules"))
  {
    UpdateXMLAttr(TagNode, "thisrefmain", ""      , FModified);
    UpdateXMLAttr(TagNode, "refmain"    , ""      , FModified);
    UpdateXMLAttr(TagNode, "crosstype"  , "normal", FModified);
  }
  else if (TagNode->CmpName("inscription"))
  {
    UpdateXMLAttr(TagNode, "uidmain", "" , FModified);
    UpdateXMLAttr(TagNode, "style"  , "" , FModified);
    UpdateXMLAttr(TagNode, "newline", "0", FModified);
  }
  else if (TagNode->CmpName("instext"))
  {
    UpdateXMLAttr(TagNode, "uidmain", "" , FModified);
    UpdateXMLAttr(TagNode, "ref"    , "" , FModified);
    UpdateXMLAttr(TagNode, "refmain", "" , FModified);
    UpdateXMLAttr(TagNode, "style"  , "" , FModified);
    UpdateXMLAttr(TagNode, "newline", "0", FModified);
    UpdateXMLAttr(TagNode, "value"  , "" , FModified);
  }
  else if (TagNode->CmpName("list"))
  {
    UpdateXMLAttr(TagNode, "uidmain"        , "" ,    FModified);
    UpdateXMLAttr(TagNode, "showzval"       , "1",    FModified);
    UpdateXMLAttr(TagNode, "border"         , "1",    FModified);
    UpdateXMLAttr(TagNode, "rownumbers"     , "1",    FModified);
    UpdateXMLAttr(TagNode, "hstyle"         , "" ,    FModified);
    UpdateXMLAttr(TagNode, "bstyle"         , "" ,    FModified);
    UpdateXMLAttr(TagNode, "width"          , "100%", FModified);
  }
  else if (TagNode->CmpName("sortel"))
  {
    UpdateXMLAttr(TagNode, "sorttype"   , "incr", FModified);
    UpdateXMLAttr(TagNode, "numberlevel", ""    , FModified);
    UpdateXMLAttr(TagNode, "numbertype" , "no"  , FModified);
    UpdateXMLAttr(TagNode, "doublevalue", "yes" , FModified);
  }
  else if (TagNode->CmpName("listcol"))
  {
    UpdateXMLAttr(TagNode, "uidmain", "", FModified);
    UpdateXMLAttr(TagNode, "hstyle" , "", FModified);
    UpdateXMLAttr(TagNode, "bstyle" , "", FModified);
    if (TICSDocSpec::IsListColLinkISGroup(TagNode))
    {
      UpdateXMLAttr(
        TagNode,
        "width",
        UnicodeString::StringOfChar(';', TagNode->GetChildByName("link_is")->GetCount(false, "is_attr")),
        FModified
      );
    }
    else
    {
      UpdateXMLAttr(TagNode, "width"  , "", FModified);
    }
  }
  else if (TagNode->CmpName("is_attr"))
  {
    UpdateXMLAttr(TagNode, "ref", "", FModified);
  }
  else if (TagNode->CmpName("link_is"))
  {
    UpdateXMLAttr(TagNode, "union"          , "0", FModified);
  }
  else if (TagNode->CmpName("listcolgroup"))
  {
    UpdateXMLAttr(TagNode, "union", "0", FModified);
    UpdateXMLAttr(TagNode, "style", "" , FModified);
  }
  else if (TagNode->CmpName("table"))
  {
    UpdateXMLAttr(TagNode, "hstyle"         , ""   ,  FModified);
    UpdateXMLAttr(TagNode, "bstyle"         , ""   ,  FModified);
    UpdateXMLAttr(TagNode, "border"         , "1"  ,  FModified);
    UpdateXMLAttr(TagNode, "rownumbers"     , "1"  ,  FModified);
    UpdateXMLAttr(TagNode, "colnumbers"     , "1"  ,  FModified);
    UpdateXMLAttr(TagNode, "showzval"       , "1"  ,  FModified);
    UpdateXMLAttr(TagNode, "tablepriority"  , "row",  FModified);
    UpdateXMLAttr(TagNode, "width"          , "100%", FModified);
  }
  else if (TagNode->CmpName("trow,tcol"))
  {
    UpdateXMLAttr(TagNode, "ref"   , "", FModified);
    UpdateXMLAttr(TagNode, "hstyle", "", FModified);
    UpdateXMLAttr(TagNode, "bstyle", "", FModified);
    if (TagNode->CmpName("tcol"))
      UpdateXMLAttr(TagNode, "width" , "", FModified);
  }
  else if (TagNode->CmpName("name"))
  {
    UpdateXMLAttr(TagNode, "refmain", "", FModified);
    UpdateXMLAttr(TagNode, "comment", "", FModified);
  }
}

//---------------------------------------------------------------------------
void UpdateSpec_2_1_5FO::operator() (TTagNode *TagNode)
{
  if      (TagNode->CmpName("passport"))
  {
    if (TagNode->CmpAV("perioddef", "no"))
      TagNode->AV["perioddef"] = "0";
    else
      TagNode->AV["perioddef"] = "1";
    TagNode->AV["defversion"] = "2.1.5";
    TTagNode *ndPeriodicity = TagNode->GetChildByName("periodicity");
    if (ndPeriodicity)
      TagNode->DeleteChild(ndPeriodicity);
    *FModified = true;
  }
  else if (TagNode->CmpName("createstep"))
  {
    UnicodeString sPeriodType = TagNode->AV["periodtype"];
    if      (sPeriodType == "this")
    {
      TagNode->AV["perioddef"]   = "undef";
    }
    else if (sPeriodType == "defined_value")
    {
      TagNode->AV["perioddef"]   = "unique";
    }
    else if (sPeriodType == "abstruct_value")
    {
      TagNode->AV["perioddef"]   = "undef";
      TDateTime dtBeg, dtEnd;
      Word wYears, wMonths, wDays;
      XMLStrToPeriod(TagNode->AV["periodvalue"], &dtBeg, &dtEnd);
      DateDiff(dtBeg, dtEnd, wDays, wMonths, wYears);
      TagNode->AV["periodvalue"] = DurationToXMLStr(wYears, wMonths, wDays);
    }
    TagNode->AV["periodicity"] = "equal";
    *FModified = true;
  }
  else if (TagNode->CmpName("list,link_is,table"))
  {
    TagNode->DeleteAttr("refperiodparams");
    *FModified = true;
  }
}

//---------------------------------------------------------------------------
void UpdateSpec_3_0_0FO::operator() (TTagNode *TagNode)
{
  if      (TagNode->CmpName("passport"))
  {
    TagNode->AV["defversion"] = "3.0.0";
    *FModified = true;
  }
}
//---------------------------------------------------------------------------
}// end of namespace ICSDocSpec

//---------------------------------------------------------------------------
__fastcall EICSDocSpecError::EICSDocSpecError(const UnicodeString Msg)
: Exception(Msg)
{
}
//---------------------------------------------------------------------------
UnicodeString __fastcall ICSDocSpec::GetSpecDefVersion(TTagNode *ASpec)
{
  CheckSpecParam(__FUNC__, ASpec);
  return ASpec->GetChildByName("passport")->GetAVDef(
    "defversion",
    csSpecDefDefaultVersion
  );
}

//---------------------------------------------------------------------------

bool __fastcall ICSDocSpec::IsSpecSupported(TTagNode *ASpec)
{
  CheckSpecParam(__FUNC__, ASpec);
  return (VersionCmp(GetSpecDefVersion(ASpec), csSpecDefVersion) <= 0);
}

//---------------------------------------------------------------------------

void __fastcall ICSDocSpec::CheckSpec(TTagNode *ASpec)
{
  if (!IsSpecSupported(ASpec))
    throw EICSDocSpecError(
        cFMT2(icsDocSpecUtilsReqVersion,
              GetSpecDefVersion(ASpec),
              csSpecDefVersion)
    );
}

//---------------------------------------------------------------------------

bool __fastcall ICSDocSpec::UpdateSpec(TTagNode *ASpec)
{
  CheckSpec(ASpec);
  bool fModified = false;
  UnicodeString sSpecDefCurVersion = GetSpecDefVersion(ASpec);
    while (VersionCmp(sSpecDefCurVersion, csSpecDefVersion) < 0)
    {
      if (sSpecDefCurVersion == "2.1.4")
       {
         ForEachTagNode(ASpec, UpdateSpecAttrs_2_1_4FO(&fModified));
         ForEachTagNode(ASpec, UpdateSpec_2_1_5FO(&fModified));
       }
      sSpecDefCurVersion = GetSpecDefVersion(ASpec);
      if (sSpecDefCurVersion == "2.1.5")
       ForEachTagNode(ASpec, UpdateSpecAttrs_2_1_5FO(&fModified));
      sSpecDefCurVersion = GetSpecDefVersion(ASpec);
      ForEachTagNode(ASpec, UpdateSpec_3_0_0FO(&fModified));

      sSpecDefCurVersion = GetSpecDefVersion(ASpec);
    }
  return fModified;
}

//---------------------------------------------------------------------------
