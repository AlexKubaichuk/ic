/*
  ����        - SetRef.cpp
  ������      - ��������������� (ICS)
  ��������    - ���� ���������� ������, ����������� �������� �����
                ��� ��������� ������
  ����������� - ������� �.�.
  ����        - 04.10.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "SetRef.h"
#include "DKClasses.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"

//***************************************************************************
//********************** ���������� ������� *********************************
//***************************************************************************

//***************************************************************************
//********************** ��������/��������/����� ����� **********************
//***************************************************************************

__fastcall TfmSetRef::TfmSetRef(TComponent* Owner, bool AIsRefRequared )
        : TForm(Owner)
{
  Caption = FMT(icsDocSetRefCaption);
  bOk->Caption = FMT(icsDocSetRefbOkBtnCaption);
  bCancel->Caption = FMT(icsDocSetRefbCancelBtnCaption);
  bHelp->Caption = FMT(icsDocSetRefbHelpBtnCaption);
  bClear->Caption = FMT(icsDocSetRefbClearBtnCaption);

  FRef = "";
  FCurDocGUI = "";
  FIsRefRequared = AIsRefRequared;
  FAreRefObjectsLoaded = false;
  FOnCanRefUpdate = NULL;
  FOnLoadRefObjects = NULL;
}

//---------------------------------------------------------------------------

void __fastcall TfmSetRef::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif
  bClear->Enabled = !FIsRefRequared;
  if ( !FAreRefObjectsLoaded )
    LoadRefObjects();
}

//***************************************************************************
//********************** ������ ����� ***************************************
//***************************************************************************

//---------------------------------------------------------------------------

UnicodeString __fastcall TfmSetRef::GetObjTreeTitle()
{
  if (!tv->Items->GetFirstNode())
    throw DKClasses::EMethodError(
      __FUNC__,
      cFMT(icsDocSetRefGetObjTreeTitle)
    );
  return tv->Items->GetFirstNode()->Text;
}

//---------------------------------------------------------------------------

void __fastcall TfmSetRef::SetObjTreeTitle( UnicodeString  AValue )
{
  if (!tv->Items->GetFirstNode())
    throw DKClasses::EMethodError(
      __FUNC__,
      cFMT(icsDocSetRefSetObjTreeTitle)
    );
  tv->Items->GetFirstNode()->Text = AValue;
}

//---------------------------------------------------------------------------

void __fastcall TfmSetRef::SetRef( UnicodeString  AValue )
{
  if ( FRef != AValue )
  {
    UnicodeString FVal = AValue;
    if (!_GUI(AValue).Length())
     {
       FVal = FCurDocGUI+"."+FVal;
     }
    for ( int i = 0; i < tv->Items->Count; i++ )
      if ( tv->Items->Item[i]->Data )
      {
        TTagNode* ndCur = reinterpret_cast<TTagNode*>(tv->Items->Item[i]->Data);

        UnicodeString CurUID = ndCur->GetAVDef( "uid" );
        UnicodeString CurGUI = ndCur->GetRoot()->GetChildByName("passport", false)->AV["GUI"];
        if ((CurGUI+"."+CurUID).UpperCase() == FVal.UpperCase())
         {
           tv->Select( tv->Items->Item[i] );
           tv->Items->Item[i]->MakeVisible();
           FRef = AValue;
           return;
         }
      }
    throw DKClasses::EInvalidArgument(
      __FUNC__,
      "AValue",
      cFMT1(icsDocSetRefSetRef,AValue)
    );
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmSetRef::LoadRefObjects()
{
  if ( FOnLoadRefObjects )
  {
    tv->Items->BeginUpdate();
    FOnLoadRefObjects( this, tv );
    FAreRefObjectsLoaded = true;
    if ( tv->Items->GetFirstNode() )
    {
      tv->Select( tv->Items->GetFirstNode() );
      tv->Selected->Expand( false );
      tv->Selected->MakeVisible();
    }
    else
      bOk->Enabled = false;
    tv->Items->EndUpdate();      
  }
  else
    bOk->Enabled = false;
}

//***************************************************************************
//********************** ����������� �������*********************************
//***************************************************************************

void __fastcall TfmSetRef::bOkClick(TObject *Sender)
{
  TTagNode* Node = reinterpret_cast<TTagNode*>(tv->Selected->Data);
  UnicodeString GUI = Node->GetRoot()->GetChildByName( "passport", false )->GetAVDef( "GUI" );
  FRef = Node->AV["uid"];
  if ( FCurDocGUI != GUI )
    FRef = GUI + "." + FRef;
  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TfmSetRef::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TfmSetRef::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}

//---------------------------------------------------------------------------

void __fastcall TfmSetRef::bClearClick(TObject *Sender)
{
  FRef = "";
  ModalResult = mrOk;  
}

//---------------------------------------------------------------------------

void __fastcall TfmSetRef::tvChange(TObject *Sender, TTreeNode *Node)
{
  bOk->Enabled = ( FOnCanRefUpdate && Node->Data )
                 ? FOnCanRefUpdate( this, reinterpret_cast<TTagNode*>(Node->Data) )
                 : false;
}

//---------------------------------------------------------------------------

void __fastcall TfmSetRef::tvDblClick(TObject *Sender)
{
  if ( bOk->Enabled )
    bOk->Click();
}

//---------------------------------------------------------------------------

