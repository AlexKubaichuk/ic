/*
  ����        - ObjRulesEdit.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� �������� �����
                ��� ������������ ������ ����������
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 17.11.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  16.03.2006
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  27.01.2006
    [*] � ���� ����� "��� ������������" ����� ������� ��� "�������������"
    [*] �������� ������������ ��������� ����������

  17.11.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef ObjRulesEditH
#define ObjRulesEditH

//---------------------------------------------------------------------------

#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "DKListBox.h"
//---------------------------------------------------------------------------
//#include "ICSDoc.h"  //��������� �������� � ���� ��������
#include "ObjRulesAssocEdit.h"
//---------------------------------------------------------------------------

namespace ObjRulesEdit
{

//***************************************************************************
//********************** ���� ***********************************************
//***************************************************************************

/*
 * ��� ����������� ������� OnGetDocElementsList - ���������, ����� ��������� �������� ������
 * ��������� �������������� �������������. ������������ ����������� TICSDocSpec � ������� ���������
 * ������ ������������ ��������� �� ��������� ��� ���������� ������ ����������
 *
 *
 * ���������:
 *   [out] DocElements - ������ ��������� �������������� �������������
 *
 */
typedef void __fastcall (__closure *TOnGetDocElementsListEvent)( TObject *Sender, TStrings *DocElements );

/*
 * ��� ����������� ������� OnGetSpecMainName - ���������, ����� ��������� �������� ������������
 * �������������� �������������. ������������ ����������� TICSDocSpec � ������� ���������
 * ������ ������������ ��������� �� ��������� ��� ���������� ������ ����������
 *
 *
 * ���������:
 *   [out] SpecMainName - ������������ �������������� �������������
 *
 */
typedef void __fastcall (__closure *TOnGetSpecMainNameEvent)( TObject *Sender, UnicodeString &SpecMainName );

//---------------------------------------------------------------------------

//����� ��� ������������ ������ ����������

class PACKAGE TfmObjRulesEdit : public TForm
{
__published:	// IDE-managed Components
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
        TSplitter *Splitter1;
        TActionList *ActionList1;
        TAction *actOk;
        TPanel *ObjTitleLab;
        TPanel *pProps;
        TPanel *pThisObj;
        TPanel *Panel7;
        TLabel *lbThisObjTitle;
        TDKListBox *lbxThisObj;
        TPanel *pRefObj;
        TPanel *Panel8;
        TLabel *lbRefObjTitle;
        TDKListBox *lbxRefObj;
        TLabel *lbThisSpecTitle;
        TLabel *lbRefSpecTitle;
        TPanel *pTabRulesProps;
        TLabel *AssocTypeLab;
        TPanel *pPropHead;
        TPanel *pInsRulesProps;
        TPanel *pListRulesProps;
        TPanel *pDopCtrls;
  TcxComboBox *cbCrossType;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall lbxThisObjSelect(TObject *Sender,
          int ListItemIndex);
        void __fastcall actOkExecute(TObject *Sender);


private:	// User declarations
        /*����*/
        TTagNode*         FSpec;        //������������� ������������
        TTagNode*         FIntegrSpec;  //������������� ������������� ����������
        TTagNode*         FObjRules;    //������� ����������

        /*������� (�������� ��. � ������ public)*/
        TOnGetDocElementsListEvent    FOnGetDocElementsList;
        TOnGetDocElementViewTextEvent FOnGetDocElementViewText;
        TOnGetSpecMainNameEvent       FOnGetSpecMainName;


protected:
        TTagNode* __fastcall GetThisObj();
        TTagNode* __fastcall GetRefObj();
        void __fastcall UpdateActionsEnabled();
        bool __fastcall CreateObjRules();
        bool __fastcall AreInputsCorrect();


public:		// User declarations
        __fastcall TfmObjRulesEdit( TComponent* Owner, TTagNode *ASpec, TTagNode *AIntegrSpec );
        virtual __fastcall ~TfmObjRulesEdit();

        /*��������*/
        //������� ����������
        __property TTagNode* ObjRules = { read = FObjRules  };
        //������ �������������� �������������
        __property TTagNode* ThisObj  = { read = GetThisObj };
        //������������� ������
        __property TTagNode* RefObj   = { read = GetRefObj  };

        /*�������*/
        //��������� ��� ������������� �������� ������ ��������� �������������� �������������
        __property TOnGetDocElementsListEvent    OnGetDocElementsList      = { read = FOnGetDocElementsList,      write = FOnGetDocElementsList      };
        //��������� ��� ������������� �������� ������������ ����� ��� �������� �������������
        __property TOnGetDocElementViewTextEvent OnGetDocElementViewText   = { read = FOnGetDocElementViewText,   write = FOnGetDocElementViewText   };
        //��������� ��� ������������� �������� ������������ ������������� ( ������� "mainname" �������� )
        __property TOnGetSpecMainNameEvent       OnGetSpecMainName         = { read = FOnGetSpecMainName,         write = FOnGetSpecMainName         };
};

//---------------------------------------------------------------------------

} //end of namespace ObjRulesEdit
using namespace ObjRulesEdit;

#endif
