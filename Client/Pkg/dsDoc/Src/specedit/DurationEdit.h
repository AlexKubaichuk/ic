/*
  ����        - DurationEdit.h
  ������      - ��������������� (ICS)
  ��������    - ������ �������������� �������� ������������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 29.12.2006
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  29.12.2006
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef DurationEditH
#define DurationEditH

//---------------------------------------------------------------------------

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
//#include "ICSDoc.h"  //��������� �������� � ���� ��������
//---------------------------------------------------------------------------


using namespace std;

namespace DurationEdit
{

//###########################################################################
//##                                                                       ##
//##                            TDurationEditFM                            ##
//##                                                                       ##
//###########################################################################

class PACKAGE TDurationEditFM : public TForm
{
__published:    // IDE-managed Components
    TPanel *BottomPn;
    TButton *OkBtn;
    TButton *CancelBtn;
    TGroupBox *ClientGBx;
    TLabel *TitleLb;
  TcxMaskEdit *Ed;
    void __fastcall OkBtnClick(TObject *Sender);
    void __fastcall CancelBtnClick(TObject *Sender);
  void __fastcall EdPropertiesValidate(TObject *Sender, Variant &DisplayValue, TCaption &ErrorText,
          bool &Error);

    
protected:
    UnicodeString __fastcall GetDurationXMLStr() const;
    void __fastcall SetDurationXMLStr(const UnicodeString AValue);


public:         // User declarations
    __fastcall TDurationEditFM(TComponent* Owner);

    /*
      �������� ������������ � ������� xml:
        YC/MC/DC

        YC - ���������� ���     [0; 999]
        MC - ���������� ������� [0; 12]
        DC - ���������� ����    [0; 31]
    */
    __property UnicodeString DurationXMLStr = {read = GetDurationXMLStr, write = SetDurationXMLStr};
};

//---------------------------------------------------------------------------

} // end of namespace DurationEdit
using namespace DurationEdit;

#endif
