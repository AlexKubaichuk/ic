/*
  ����        - AddTable.cpp
  ������      - ��������������� (ICS)
  ��������    - ���� ���������� ������, ����������� �������� �����
                ��� ���������� ������� ( ��� "table" )
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 10.10.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AddTable.h"

#include "icsDocSpecConstDef.h"
#include "ICSDocSpec.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxSpinEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"

//***************************************************************************
//********************** ��������/��������/����� ����� **********************
//***************************************************************************

__fastcall TfmAddTable::TfmAddTable(TComponent* Owner)
        : TForm(Owner)
{
  if (!Owner->InheritsFrom(__classid(TICSDocSpec)))
    throw DKClasses::EInvalidArgument(
      __FUNC__,
      "Owner",
      FMT(icsDocErrorCreateOwnerNotValid)
    );
}

//---------------------------------------------------------------------------

void __fastcall TfmAddTable::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif

  ListSrcLab->Caption  = FMT(icsDocAddTableListSrcLabCaption);
  ColCountLab->Caption = FMT(icsDocAddTableColCountLabCaption);
  RowCountLab->Caption = FMT(icsDocAddTableRowCountLabCaption);
  bOk->Caption         = FMT(icsDocAddTableOkBtnCaption);
  bCancel->Caption     = FMT(icsDocAddTableCancelBtnCaption);
  bHelp->Caption       = FMT(icsDocAddTableHelpBtnCaption);

  cbISs->Style->Color      = GetReqEdBkColor();
  edColCount->Style->Color = GetReqEdBkColor();
  edRowCount->Style->Color = GetReqEdBkColor();
}

//***************************************************************************
//********************** ������ ����� ***************************************
//***************************************************************************

TStrings* __fastcall TfmAddTable::GetISList()
{
  return cbISs->Properties->Items;
}

//---------------------------------------------------------------------------

void __fastcall TfmAddTable::SetISList( TStrings* AValue )
{
  cbISs->Properties->Items->Assign( AValue );
  if ( cbISs->Properties->Items->Count )
  {
    cbISs->ItemIndex = 0;
    bOk->Enabled = true;
  }
  else
  {
    cbISs->ItemIndex = -1;
    bOk->Enabled = false;
  }
}

//---------------------------------------------------------------------------

TTagNode* __fastcall TfmAddTable::GetMainIS()
{
  return dynamic_cast<TTagNode*>(cbISs->Properties->Items->Objects[cbISs->ItemIndex]);
}

//---------------------------------------------------------------------------

int __fastcall TfmAddTable::GetColCount()
{
  return edColCount->Value;
}

//---------------------------------------------------------------------------

int __fastcall TfmAddTable::GetRowCount()
{
  return edRowCount->Value;
}

//---------------------------------------------------------------------------

TColor __fastcall TfmAddTable::GetReqEdBkColor() const
{
  return dynamic_cast<TICSDocSpec*>(Owner)->ReqEdBkColor;
}

//***************************************************************************
//********************** ����������� �������*********************************
//***************************************************************************

void __fastcall TfmAddTable::bOkClick(TObject *Sender)
{
  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TfmAddTable::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TfmAddTable::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}

//---------------------------------------------------------------------------


