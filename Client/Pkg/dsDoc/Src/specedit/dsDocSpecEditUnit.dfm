object dsDocSpecEditForm: TdsDocSpecEditForm
  Left = 549
  Top = 0
  Caption = 'dsDocSpecEditForm'
  ClientHeight = 586
  ClientWidth = 753
  Color = clBtnFace
  Constraints.MinHeight = 360
  Constraints.MinWidth = 480
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  HelpFile = 'wss.hlp'
  OldCreateOrder = False
  Position = poDesigned
  ShowHint = True
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 545
    Width = 753
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      753
      41)
    object Panel1: TPanel
      Left = 486
      Top = 1
      Width = 275
      Height = 39
      Anchors = [akRight]
      BevelOuter = bvNone
      TabOrder = 0
      object SaveBtn: TcxButton
        Left = 104
        Top = 5
        Width = 75
        Height = 25
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
        TabOrder = 0
        OnClick = SaveBtnClick
      end
      object CancelBtn: TcxButton
        Left = 185
        Top = 5
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
        OnClick = CancelBtnClick
      end
    end
  end
end
