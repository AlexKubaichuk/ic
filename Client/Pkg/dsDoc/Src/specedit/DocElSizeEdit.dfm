object fmDocElSizeEdit: TfmDocElSizeEdit
  Left = 699
  Top = 439
  BorderStyle = bsDialog
  BorderWidth = 8
  Caption = 'fmDocElSizeEdit'
  ClientHeight = 117
  ClientWidth = 341
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gbSize: TGroupBox
    Left = 0
    Top = 0
    Width = 257
    Height = 117
    Align = alClient
    Caption = 'gbSize'
    TabOrder = 0
    object lbSize: TLabel
      Left = 24
      Top = 92
      Width = 28
      Height = 13
      Caption = 'lbSize'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object lbSizeUnit: TLabel
      Left = 144
      Top = 92
      Width = 48
      Height = 13
      Caption = #1045#1076#1080#1085#1080#1094#1099':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object rbFullSize: TRadioButton
      Left = 10
      Top = 17
      Width = 241
      Height = 17
      Caption = 'rbFullSize'
      TabOrder = 0
      OnClick = rbFullSizeClick
    end
    object rbAutoSize: TRadioButton
      Left = 8
      Top = 40
      Width = 241
      Height = 17
      Caption = #1040#1074#1090#1086#1086#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077
      TabOrder = 1
      OnClick = rbAutoSizeClick
    end
    object rbCustomValue: TRadioButton
      Left = 8
      Top = 64
      Width = 241
      Height = 17
      Caption = #1047#1085#1072#1095#1077#1085#1080#1077
      TabOrder = 2
      OnClick = rbCustomValueClick
    end
    object cbSizeUnit: TcxComboBox
      Left = 198
      Top = 87
      Properties.Items.Strings = (
        #1084#1084
        #1089#1084)
      Properties.OnChange = cbSizeUnitPropertiesChange
      TabOrder = 3
      Text = 'cbSizeUnit'
      Width = 51
    end
    object edSize: TcxMaskEdit
      Left = 76
      Top = 87
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '(\d+{1,6})|(\d+{1,6}[.|,]\d+{1,6})'
      Properties.MaxLength = 0
      Properties.ValidateOnEnter = False
      Properties.ValidationOptions = []
      Properties.OnChange = edSizePropertiesChange
      TabOrder = 4
      OnExit = edSizeExit
      Width = 62
    end
  end
  object pRight: TPanel
    Left = 257
    Top = 0
    Width = 84
    Height = 117
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 84
      Height = 97
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object bOk: TButton
        Left = 8
        Top = 5
        Width = 75
        Height = 25
        Caption = 'Ok'
        Default = True
        TabOrder = 0
        OnClick = bOkClick
      end
      object bCancel: TButton
        Left = 8
        Top = 37
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
        OnClick = bCancelClick
      end
      object bHelp: TButton
        Left = 8
        Top = 69
        Width = 75
        Height = 25
        Caption = '&'#1055#1086#1084#1086#1097#1100
        TabOrder = 2
        OnClick = bHelpClick
      end
    end
  end
end
