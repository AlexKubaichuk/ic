/*
  ����        - DocElSizeEdit.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� �������� ����� ���
                �������������� �������� ������ ��� ������ �������� ���������
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 25.05.2005
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  16.03.2006
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������
  
  27.01.2006
    [+] ��������� �������� TICSDocSpec::ReqEdBkColor
    [*] ���� ����� "lbSize" � "�������" �������� ��� "������������"
    [*] �������� ������������ ��������� ����������

  25.05.2005 - 02.06.2005
    �������������� �������

*/
//---------------------------------------------------------------------------

#ifndef DocElSizeEditH
#define DocElSizeEditH

//---------------------------------------------------------------------------

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"

//---------------------------------------------------------------------------
//#include "ICSDoc.h" //��������� �������� � ���� ��������
//---------------------------------------------------------------------------


namespace DocElSizeEdit
{

//***************************************************************************
//********************** ������� ********************************************
//***************************************************************************

#define SV_NONE         0x00
#define SV_FULLSIZE     0x01
#define SV_AUTOSIZE     0x02
#define SV_CUSTOM       0x04
#define SV_ALL          SV_FULLSIZE | SV_AUTOSIZE | SV_CUSTOM

//***************************************************************************
//********************** ���� ***********************************************
//***************************************************************************

//��� �������
enum TSizeType
{
  stHeight,     //������
  stWidth       //������
};

//�������� �������
enum TSizeValueType
{
  vtNone     = -1,
  vtFullSize = 0,   //�� ���� ������\������ ���������� ������� view'���
  vtAutoSize = 1,   //���������������
  vtCustom   = 2    //���������� ��������
};

//������� ��������� ��� ����������� �������� ������\������
enum TSizeCustomValueUnit
{
  vuMM = 0,     //��
  vuCM = 1      //��
};

//***************************************************************************
//********************** ��������� ���������� ������� ***********************
//***************************************************************************

//---------------------------------------------------------------------------

//����� ��� �������������� �������� ������ ��� ������

class TfmDocElSizeEdit : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *gbSize;
        TRadioButton *rbFullSize;
        TRadioButton *rbAutoSize;
        TRadioButton *rbCustomValue;
        TLabel *lbSize;
        TLabel *lbSizeUnit;
        TPanel *pRight;
        TPanel *Panel1;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
  TcxComboBox *cbSizeUnit;
  TcxMaskEdit *edSize;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bOkClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall rbCustomValueClick(TObject *Sender);
        void __fastcall rbFullSizeClick(TObject *Sender);
        void __fastcall rbAutoSizeClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
 void __fastcall cbSizeUnitPropertiesChange(TObject *Sender);
 void __fastcall edSizePropertiesChange(TObject *Sender);
 void __fastcall edSizeExit(TObject *Sender);


private:	// User declarations
        TSizeType FSizeType;
        BYTE FSizeValidValues;
        TSizeValueType FSizeValueType;
        TSizeCustomValueUnit FSizeCustomValueUnit;
        float FSizeCustomValue;
        int FMaxSizeMM;

        const TSizeType FSizeTypeDefVal;
        const BYTE FSizeValidValuesDefVal;
        const TSizeValueType FSizeValueTypeDefVal;
        const TSizeCustomValueUnit FSizeCustomValueUnitDefVal;
        const float FSizeCustomValueDefVal;


protected:
        bool __fastcall AreInputsCorrect();
        void __fastcall UpdateSizeTypeControls();
        void __fastcall UpdateSizeValidValuesControls();
        void __fastcall UpdateSizeValueTypeControls();
        void __fastcall UpdateSizeCustomValueUnitControls();
        void __fastcall UpdateSizeCustomValueControls();
        void __fastcall SetSizeType( TSizeType AValue );
        void __fastcall SetSizeValidValues( BYTE AValue );
        void __fastcall SetSizeValueType( TSizeValueType AValue );
        void __fastcall SetSizeCustomValueUnit( TSizeCustomValueUnit AValue );
        void __fastcall SetSizeCustomValue( float AValue );
        int __fastcall GetSizeCustomValueAsMM();
        void __fastcall SetSizeCustomValueAsMM( int AValue );
        void __fastcall SetMaxSizeMM ( int AValue );

        TColor __fastcall GetReqEdBkColor() const;

        void __fastcall EnableSizeCustomValueEditors();
        void __fastcall DisableSizeCustomValueEditors();

        UnicodeString __fastcall SizeCustomValueAsText();


public:		// User declarations
        __fastcall TfmDocElSizeEdit( TComponent* Owner );
        __property TSizeType SizeType = { read = FSizeType, write = SetSizeType };
        __property BYTE SizeValidValues = { read = FSizeValidValues, write = SetSizeValidValues };
        __property TSizeValueType SizeValueType = { read = FSizeValueType, write = SetSizeValueType };
        __property TSizeCustomValueUnit SizeCustomValueUnit = { read = FSizeCustomValueUnit, write = SetSizeCustomValueUnit };
        __property float SizeCustomValue = { read = FSizeCustomValue, write = SetSizeCustomValue };
        __property int SizeCustomValueAsMM = { read = GetSizeCustomValueAsMM, write = SetSizeCustomValueAsMM };
        __property int MaxSizeMM = { read = FMaxSizeMM, write = SetMaxSizeMM };

        static const int MaxWidthMMDefVal;
        static const int MaxHeightMMDefVal;
};

//---------------------------------------------------------------------------

} //end of namespace DocElSizeEdit
using namespace DocElSizeEdit;

#endif
