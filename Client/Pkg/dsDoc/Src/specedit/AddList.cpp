/*
  ����        - AddList.cpp
  ������      - ��������������� (ICS)
  ��������    - ���� ���������� ������, ����������� �������� �����
                ��� ���������� ������ ( ��� "list" ) ���
                �������� ������ ( ��� "list" )
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 08.10.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AddList.h"
//#include "DKMBUtils.h"
#include "msgdef.h"
#include "ICSDocSpec.h"
#include "icsDocSpecConstDef.h"
#include <functional>
#include "ICSTL.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"

//***************************************************************************
//********************** ���� ***********************************************
//***************************************************************************

//�������������� ������ ��� ���������� ������ TTreeNodeList �� ����������� �������

struct TreeNodeLessAbsoluteIndex : public binary_function<TTreeNode*, TTreeNode*, bool>
{
  bool operator() (TTreeNode* x, TTreeNode* y) { return x->AbsoluteIndex < y->AbsoluteIndex; }
};

//---------------------------------------------------------------------------

//�������������� ������ ��� ������ � ������ TTreeNodeList
struct SelectTreeNodeDataRefAttrVal : public unary_function<TTreeNode*, UnicodeString>
{
  UnicodeString operator()(const TTreeNode* x) const;
};

UnicodeString SelectTreeNodeDataRefAttrVal::operator()(const TTreeNode* x) const
{
  return reinterpret_cast<TTagNode*>(x->Data)->GetChildByName( "link_is" )->AV["ref"];
}

//***************************************************************************
//********************** ��������/��������/����� ����� **********************
//***************************************************************************

__fastcall TfmAddList::TfmAddList(TComponent* Owner, bool ACanAddGroup, TAddListMode AAddListMode )
        : TForm(Owner)
{
  if (!Owner)
    throw DKClasses::ENullArgument(__FUNC__, "Owner");
  FDocSpec = dynamic_cast<TICSDocSpec*>(Owner);
  if (!FDocSpec)
    throw DKClasses::EInvalidArgument(
      __FUNC__,
      "Owner",
      cFMT(icsDocAddListErrorInvalidArgument)
    );
  FAddListMode = AAddListMode;
  FCanAddGroup = ACanAddGroup;
  FMainISTagNode = NULL;
  FFakeTag = new TTagNode;
  FOnObjListLoad = NULL;
  FOldGBClientWndProc = gbClient->WindowProc;
  gbClient->WindowProc = GBClientWndProc;
}

//---------------------------------------------------------------------------

__fastcall TfmAddList::~TfmAddList()
{
  delete FFakeTag;
}

//---------------------------------------------------------------------------

void __fastcall TfmAddList::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif

  lbISListTitle->Caption = FMT(icsDocAddListISListTitle);
  lbISTreeTitle->Caption = FMT(icsDocAddListISTreeTitle);
  lbColsTitle->Caption = FMT(icsDocAddListColsTitle);

  actAddCols->Caption = FMT(icsDocAddListAddColsCaption);
  actAddCols->Hint = FMT(icsDocAddListAddColsHint);

  actAddGCols->Caption = FMT(icsDocAddListAddGColsCaption);
  actAddGCols->Hint = FMT(icsDocAddListAddGColsHint);

  actDelete->Caption = FMT(icsDocAddListDeleteCaption);
  actDelete->Hint = FMT(icsDocAddListDeleteHint);

  actClear->Caption = FMT(icsDocAddListClearCaption);
  actClear->Hint = FMT(icsDocAddListClearHint);

  actUp->Caption = FMT(icsDocAddListUpCaption);
  actUp->Hint = FMT(icsDocAddListUpHint);

  actDown->Caption = FMT(icsDocAddListDownCaption);
  actDown->Hint = FMT(icsDocAddListDownHint);

  bOk->Caption = FMT(icsDocAddListOkBtnCaption);
  bCancel->Caption = FMT(icsDocAddListCancelBtnCaption);
  bHelp->Caption = FMT(icsDocAddListHelpBtnCaption);

  if ( FAddListMode == alNewList )
    lbMainIS->Visible = false;
  else
  {
    cbISs->Visible = false;
    lbMainIS->Left = cbISs->Left;
    Caption = FMT(icsDocAddListCaption);
    if ( FAddListMode == alNewLinkISAttrsListCols )
    {
      tvCols->ShowButtons = false;
      tvCols->ShowLines   = false;        
    }
  }
  bOk->Enabled       = false;
  actDelete->Enabled   = false;
  actClear->Enabled    = false;  
  actUp->Enabled   = false;
  actDown->Enabled = false;
}

//---------------------------------------------------------------------------

void __fastcall TfmAddList::GBClientWndProc(Messages::TMessage &Message)
{
  FOldGBClientWndProc(Message);
  switch( Message.Msg )
  {
    case WM_NOTIFY:
      if ( ( ((LPNMHDR)Message.LParam)->code == TVN_ENDLABELEDIT ) &&
           ( ((LPNMHDR)Message.LParam)->hwndFrom == tvCols->Handle )
      )
      {
        bOk->Default    = true;
        bCancel->Cancel = true;
      }
    break;
  }
}

//***************************************************************************
//********************** ������ ����� ***************************************
//***************************************************************************

TStrings* __fastcall TfmAddList::GetISList()
{
  return cbISs->Properties->Items;
}

//---------------------------------------------------------------------------

void __fastcall TfmAddList::SetISList( TStrings* AValue )
{
  cbISs->Properties->Items->Assign( AValue );
  if ( cbISs->Properties->Items->Count )
  {
    cbISs->ItemIndex = 0;
    cbISs->Properties->OnChange( cbISs );
  }
  else
    cbISs->ItemIndex = -1;
}

//---------------------------------------------------------------------------

TTreeNodes* __fastcall TfmAddList::GetListCols()
{
  return tvCols->Items;
}

//---------------------------------------------------------------------------

TTagNode* __fastcall TfmAddList::GetMainIS()
{
  return FMainISTagNode;
}

//---------------------------------------------------------------------------

void __fastcall TfmAddList::SetMainIS( const TTagNode *AValue )
{
  if ( FMainISTagNode != AValue )
  {
    if ( FAddListMode == alNewList )
    {
      int nInd = cbISs->Properties->Items->IndexOfObject( const_cast<TTagNode*>(AValue) );
      if ( nInd == -1 )
        throw DKClasses::EInvalidArgument(
          __FUNC__,
          "AValue",
          FMT(icsDocAddListErrorSetMainIS)
        );
      if ( cbISs->ItemIndex != nInd )
      {
        cbISs->ItemIndex = nInd;
        cbISs->Properties->OnChange( cbISs );
      }
    }
    FMainISTagNode = const_cast<TTagNode*>(AValue);
    if ( FMainISTagNode && ( FAddListMode != alNewList ) )
      lbMainIS->Caption = FMainISTagNode->AV["name"];
    tvISs->Items->BeginUpdate();
    tvISs->Items->Clear();
    ClearCols();
    if ( FOnObjListLoad && FOnObjListLoad( this, tvISs->Items, FMainISTagNode ) )
    {
      tvISs->Selected = tvISs->Items->GetFirstNode();
      tvISs->Selected->MakeVisible();
      tvISs->Selected->Expand( true );
    }
    tvISs->Items->EndUpdate();
  }
}

//---------------------------------------------------------------------------

//������� ������ ����������� ��������

void __fastcall TfmAddList::ClearCols()
{
  tvCols->Items->Clear();
  FFakeTag->DeleteChild();
  bOk->Enabled       = false;
  actDelete->Enabled   = false;
  actClear->Enabled    = false;
  actUp->Enabled   = false;
  actDown->Enabled = false;
}

//---------------------------------------------------------------------------

//���������� ���� � ������ ����������� ��������

TTreeNode* __fastcall TfmAddList::AddColTreeNode( TTreeNode* ColParentNode, UnicodeString  Text, TTagNode* Object )
{
  return ( ColParentNode )
         ? tvCols->Items->AddChildObject( ColParentNode,
                                          Text,
                                          reinterpret_cast<void*>(Object) )
         : tvCols->Items->AddObject     ( NULL,
                                          Text,
                                          reinterpret_cast<void*>(Object) );
}

//---------------------------------------------------------------------------

//���������� ���� "listcol"

TTagNode* __fastcall TfmAddList::AddListColTagNode( TTagNode* ndColParent, UnicodeString  AName )
{
  TTagNode* ndListCol = ndColParent->AddChild("listcol");
  ndListCol->AV["name"]    = AName;
  ndListCol->AV["uid"]     = FDocSpec->NewUID();
  ndListCol->AV["uidmain"] = "";
  ndListCol->AV["hstyle"]  = "";
  ndListCol->AV["bstyle"]  = "";
  ndListCol->AV["width"]   = "";
  return ndListCol;
}

//---------------------------------------------------------------------------

//���������� �������

TTreeNode* __fastcall TfmAddList::AddCol( TTreeNode* ObjNode, TTreeNode* ColParentNode )
{
  TTagNode* ndObj    = reinterpret_cast<TTagNode*>( ObjNode->Data );
  TTagNode* ndColParent = ( ColParentNode ) ? reinterpret_cast<TTagNode*>( ColParentNode->Data ) : FFakeTag;
  if ( ObjNode->Parent )
  {
    //������� �������� ��������
    if ( reinterpret_cast<TTagNode*>(ObjNode->Parent->Data) == FMainISTagNode )
    {
      TTagNode* ndListCol = AddListColTagNode( ndColParent, ObjNode->Text );
      ndListCol->AddChild( "is_attr", "ref=" + TICSDocBaseEdit::GetExtRef( ndObj ) );
      return AddColTreeNode( ColParentNode, ObjNode->Text, ndListCol );
    }
    //������� ��������� ��������
    else
    {
      if ( !FCanAddGroup )
      {
        if ( !__CanNotAddLinkISAttrColMsgShown )
        {
          _MSG_INF( FMT(icsDocAddListErrorAddCol), TICSDocSpec::ICSName);
          __CanNotAddLinkISAttrColMsgShown = true;
        }
        return NULL;
      }
      else if ( FAddListMode == alNewLinkISAttrsListCols )
      {
        TTagNode* ndIsAttr = ndColParent->AddChild( "is_attr", "ref=" + TICSDocBaseEdit::GetExtRef( ndObj ) );
        return AddColTreeNode( ColParentNode, ObjNode->Text, ndIsAttr );
      }
      else
      {
        TTreeNode* ListColNode;
        //����� � ������ �����, ��������������� ����������� ����� link_is
        // ( ���� � ���, ��� ��� ��������� ���������� ��������� ����� � ��� �� ��������� ��������, ���
        // �������� ������ ���������� ��� �������� ���� "is_attr" ��� ���� "link_is" )
        TTreeNodeList::iterator i = find_if( FAddedLinkIS.begin(),
                                             FAddedLinkIS.end(),
                                             ICUtils::compose1( bind2nd( equal_to<UnicodeString>(),
                                                                TICSDocBaseEdit::GetExtRef( reinterpret_cast<TTagNode*>(ObjNode->Parent->Data) )
                                                       ),
                                                       SelectTreeNodeDataRefAttrVal()
                                             )
                                    );

        if ( i != FAddedLinkIS.end() )
          ListColNode = (*i);
        else
        {
          UnicodeString ListColName = ObjNode->Parent->Text.SubString( 1, ObjNode->Parent->Text.Pos(csCountViewText) );
          TTagNode* ndListCol = AddListColTagNode( ndColParent, ListColName );
          TTagNode* ndLinkIS = ndListCol->AddChild( "link_is" );
          ndLinkIS->AV["ref"]   = TICSDocBaseEdit::GetExtRef( reinterpret_cast<TTagNode*>(ObjNode->Parent->Data) );
          ndLinkIS->AV["union"] = "0";
          ListColNode = AddColTreeNode( ColParentNode, ListColName, ndListCol );
          FAddedLinkIS.push_back( ListColNode );
        }
        TTagNode* ndLinkIS = reinterpret_cast<TTagNode*>(ListColNode->Data)->GetChildByName( "link_is" );
        TTagNode* ndIsAttr = ndLinkIS->AddChild( "is_attr", "ref=" + TICSDocBaseEdit::GetExtRef( ndObj ) );
        TICSDocSpec::InsertListColSingle2SizeAttr( ndIsAttr, "width", "" );
        return AddColTreeNode( ListColNode, ObjNode->Text, ndIsAttr );
      }
    }
  }
  //������� ���������� ��� ��������� ��������
  else
  {
    TTagNode* ndListCol = AddListColTagNode( ndColParent, ObjNode->Text.SubString( 1, ObjNode->Text.Pos(csCountViewText) ) );
    TTagNode* ndLinkIS = ndListCol->AddChild( "link_is" );
    ndLinkIS->AV["ref"]   = TICSDocBaseEdit::GetExtRef( ndObj );
    ndLinkIS->AV["union"] = "0";
    ndLinkIS->AddChild( "count", "ref=" + TICSDocBaseEdit::GetExtRef( ndObj ) );
    return AddColTreeNode( ColParentNode, ObjNode->Text, ndListCol );
  }
}

//---------------------------------------------------------------------------

//���������� ��������

void __fastcall TfmAddList::AddCols( TTreeNode* ColsParentNode )
{
  TTreeNode* LastAddedCol = NULL;
  FAddedLinkIS.clear();
  __CanNotAddLinkISAttrColMsgShown = false;
  tvCols->Items->BeginUpdate();
  if ( tvISs->SelectionCount > 1 )
  {
    //���������� ������ ��������� ����� ������ tvISs, ��������������� �� ����������� �������
    TTreeNodeList SelectionsList;
    for ( unsigned i = 0; i < tvISs->SelectionCount; i++ )
      SelectionsList.push_back( tvISs->Selections[i] );
    SelectionsList.sort( TreeNodeLessAbsoluteIndex() );
    for ( TTreeNodeList::iterator i = SelectionsList.begin(); i != SelectionsList.end(); i++ )
    {
      TTagNode *TagNode = reinterpret_cast<TTagNode*>((*i)->Data);
      TTreeNode *tmp;
      if ( ( TagNode != FMainISTagNode ) &&
          !( ( FAddListMode == alNewLinkISAttrsListCols ) && ( TagNode->CmpName("IS") ) )
      )
        #pragma option push -w-pia
        if ( tmp = AddCol( *i, ColsParentNode ) )
        #pragma option pop        
          LastAddedCol = tmp;
    }
  }
  else
    LastAddedCol = AddCol( tvISs->Selected, ColsParentNode );
  if ( LastAddedCol )
  {
    tvCols->Selected = LastAddedCol;
    tvCols->Selected->MakeVisible();
    tvCols->Selected->Expand( true );
    bOk->Enabled = true;
    actClear->Enabled = true;
  }
  tvCols->Items->EndUpdate();  
}

//***************************************************************************
//********************** ����������� �������*********************************
//***************************************************************************

void __fastcall TfmAddList::bOkClick(TObject *Sender)
{
  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TfmAddList::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TfmAddList::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}

//---------------------------------------------------------------------------


void __fastcall TfmAddList::tvISsChange(TObject *Sender, TTreeNode *Node)
{
  bool eAddCols = false;
  for ( unsigned i = 0; i < tvISs->SelectionCount; i++ )
  {
    TTagNode *TagNode = reinterpret_cast<TTagNode*>(tvISs->Selections[i]->Data);
    if ( ( TagNode != FMainISTagNode ) &&
         !( ( FAddListMode == alNewLinkISAttrsListCols ) && ( TagNode->CmpName("IS") ) )
    )
    {
      eAddCols = true;
      break;
    }
  }
  actAddCols->Enabled  = eAddCols;
  actAddGCols->Enabled = eAddCols;
}

//---------------------------------------------------------------------------

void __fastcall TfmAddList::tvColsChange(TObject *Sender, TTreeNode *Node)
{
//  TTagNode* TagNode = reinterpret_cast<TTagNode*>(Node->Data);
  bool eDel = true;
  if ( Node->Parent /*&& ( reinterpret_cast<TTagNode*>(Node->Parent->Data)->CmpName("listcolgroup") ) */ )
    if ( Node->Parent->Count == 1 )
      eDel = false;
  actDelete->Enabled   = eDel;
  actUp->Enabled   = Node->getPrevSibling();
  actDown->Enabled = Node->getNextSibling();
}

//---------------------------------------------------------------------------

void __fastcall TfmAddList::tvColsEditing(TObject *Sender, TTreeNode *Node,
      bool &AllowEdit)
{
  TTagNode* TagNode = reinterpret_cast<TTagNode*>(Node->Data);
  #pragma option push -w-pia
  if ( AllowEdit = ( TagNode->CmpName("listcolgroup") ) )
  #pragma option pop  
  {
    bOk->Default    = false;
    bCancel->Cancel = false;
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmAddList::tvColsEdited(TObject *Sender, TTreeNode *Node,
      UnicodeString &S)
{
  reinterpret_cast<TTagNode*>(Node->Data)->AV["name"] = S;
}

//---------------------------------------------------------------------------




//Del            - ������� ������� �������
//Ctrl + Del     - �������� ������ ����������� ��������
//Ctrl + VK_UP   - ����������� ������� ������� �����
//Ctrl + VK_DOWN - ����������� ������� ������� ����
//F2             - �������������� ������������ ������ ��������

void __fastcall TfmAddList::tvColsKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if ( tvCols->IsEditing() )
    return;
  if ( ( Key == VK_F2 ) && tvCols->Selected )
    TreeView_EditLabel( tvCols->Handle, tvCols->Selected->ItemId );
}

//---------------------------------------------------------------------------
void __fastcall TfmAddList::tvISsCustomDrawItem(TCustomTreeView *Sender,
      TTreeNode *Node, TCustomDrawState State, bool &DefaultDraw)
{
  TTagNode *TagNode = reinterpret_cast<TTagNode*>(Node->Data);
  if ( ( TagNode == FMainISTagNode ) ||
       ( ( FAddListMode == alNewLinkISAttrsListCols ) && ( TagNode->CmpName("IS") ) )
  )
    Sender->Canvas->Font->Color = clGrayText;
  DefaultDraw = true;
}

//---------------------------------------------------------------------------

void __fastcall TfmAddList::tvISsDblClick(TObject *Sender)
{
  if (actAddCols->Enabled) actAddColsExecute(actAddCols);
}

//---------------------------------------------------------------------------

void __fastcall TfmAddList::tvColsDblClick(TObject *Sender)
{
  if (actDelete->Enabled) actDeleteExecute(actDelete);
}

//---------------------------------------------------------------------------
void __fastcall TfmAddList::actAddColsExecute(TObject *Sender)
{
  AddCols( NULL );
}
//---------------------------------------------------------------------------
void __fastcall TfmAddList::actAddGColsExecute(TObject *Sender)
{
  if ( !FCanAddGroup )
  {
    _MSG_INF(FMT(icsDocAddListErrorAddCols), TICSDocSpec::ICSName);
    return;
  }
  TTagNode* ndColsParent = FFakeTag->AddChild( "listcolgroup" );
  ndColsParent->AV["name"]  = FMT(icsDocAddListColGroupCaption);
  ndColsParent->AV["union"] = "0";
  ndColsParent->AV["style"] = "";
  tvCols->Items->BeginUpdate();
  TTreeNode* ColsParentNode = tvCols->Items->AddObject( NULL,
                                                        FMT(icsDocAddListColGroupCaption),
                                                        reinterpret_cast<void*>(ndColsParent) );
  AddCols( ColsParentNode );
  tvCols->Items->EndUpdate();
}
//---------------------------------------------------------------------------
void __fastcall TfmAddList::actDeleteExecute(TObject *Sender)
{
  delete reinterpret_cast<TTagNode*>(tvCols->Selected->Data);
  tvCols->Items->BeginUpdate();
  tvCols->Selected->Delete();
  tvCols->Items->EndUpdate();
  if (!tvCols->Items->Count)
  {
    actDelete->Enabled = false;
    bOk->Enabled     = false;
    actClear->Enabled  = false;
  }
}
//---------------------------------------------------------------------------
void __fastcall TfmAddList::actClearExecute(TObject *Sender)
{
  ClearCols();
}
//---------------------------------------------------------------------------
void __fastcall TfmAddList::actUpExecute(TObject *Sender)
{
  TICSDocBaseEdit::MoveTreeNodeUp( tvCols->Selected, true, false );
}
//---------------------------------------------------------------------------
void __fastcall TfmAddList::actDownExecute(TObject *Sender)
{
  TICSDocBaseEdit::MoveTreeNodeDown( tvCols->Selected, true, false );
}
//---------------------------------------------------------------------------

void __fastcall TfmAddList::cbISsPropertiesChange(TObject *Sender)
{
  MainIS = dynamic_cast<TTagNode*>( (cbISs->Properties->Items->Objects[cbISs->ItemIndex]));
}
//---------------------------------------------------------------------------

