/*
  ����        - DocElSizeEdit.cpp
  ������      - ��������������� (ICS)
  ��������    - ���� ���������� ������, ����������� �������� ����� ���
                �������������� �������� ������ ��� ������ �������� ���������
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 25.05.2005
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "DocElSizeEdit.h"
//#include "DKMBUtils.h"
#include "msgdef.h"
#include "Math.hpp"
#include "ICSDocSpec.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"

//***************************************************************************
//********************** ���������� ������� *********************************
//***************************************************************************

//***************************************************************************
//********************** ��������/��������/����� ����� **********************
//***************************************************************************

const int TfmDocElSizeEdit::MaxWidthMMDefVal = 100000000; //840;
const int TfmDocElSizeEdit::MaxHeightMMDefVal = 100000000; //1188;

//---------------------------------------------------------------------------

__fastcall TfmDocElSizeEdit::TfmDocElSizeEdit(TComponent* Owner)
: TForm(Owner),
  FSizeTypeDefVal(stHeight),
  FSizeValidValuesDefVal(SV_ALL),
  FSizeValueTypeDefVal(vtNone),
  FSizeCustomValueUnitDefVal(vuCM),
  FSizeCustomValueDefVal(0)
{
  if (!Owner->InheritsFrom(__classid(TICSDocSpec)))
    throw DKClasses::EInvalidArgument(
      __FUNC__,
      "Owner",
      FMT(icsDocErrorCreateOwnerNotValid)
    );

  FSizeType            = FSizeTypeDefVal;
  FSizeValidValues     = FSizeValidValuesDefVal;
  FSizeValueType       = FSizeValueTypeDefVal;
  FSizeCustomValueUnit = FSizeCustomValueUnitDefVal;
  FSizeCustomValue     = FSizeCustomValueDefVal;
  switch ( FSizeType )
  {
    case stHeight: FMaxSizeMM = MaxHeightMMDefVal; break;
    case stWidth : FMaxSizeMM = MaxWidthMMDefVal;  break;
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::FormCreate(TObject *Sender)
{
  edSize->Style->Color     = GetReqEdBkColor();
  cbSizeUnit->Style->Color = GetReqEdBkColor();
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif

  rbAutoSize->Caption    = FMT(icsDocElSizeEditAutoSizeCaption);
  rbCustomValue->Caption = FMT(icsDocElSizeEditValueCaption);
  lbSizeUnit->Caption    = FMT(icsDocElSizeEditUnitsCaption);
  bOk->Caption           = FMT(icsDocElSizeEditOkBtnCaption);
  bCancel->Caption       = FMT(icsDocElSizeEditCancelBtnCaption);
  bHelp->Caption         = FMT(icsDocElSizeEditHelpBtnCaption);

  if ( SizeType == FSizeTypeDefVal )
    UpdateSizeTypeControls();

  if ( SizeValidValues == FSizeValidValuesDefVal )
    UpdateSizeValidValuesControls();

  if ( SizeValueType == FSizeValueTypeDefVal )
    UpdateSizeValueTypeControls();

  if ( SizeCustomValueUnit == FSizeCustomValueUnitDefVal )
    UpdateSizeCustomValueUnitControls();

  if ( SizeCustomValue == FSizeCustomValueDefVal )
    UpdateSizeCustomValueControls();
}

//***************************************************************************
//********************** ������ ����� ***************************************
//***************************************************************************

bool __fastcall TfmDocElSizeEdit::AreInputsCorrect()
{
  if ( FSizeValueType == vtNone )
  {
    _MSG_ERR( FMT(icsDocElSizeEditAreInputsCorrectMsg), FMT(icsDocErrorInputErrorCaption) );
    return false;
  }

  return true;
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::UpdateSizeTypeControls()
{
  switch( FSizeType )
  {
    case stHeight:
      Caption             = FMT(icsDocElSizeEditValHeightCaption);
      gbSize->Caption     = FMT(icsDocElSizeEditHeightCaption);
      rbFullSize->Caption = FMT(icsDocElSizeEditFullHeightCaption);
      lbSize->Caption     = FMT(icsDocElSizeEditHeightCaption)+":";
    break;

    case stWidth:
      Caption             = FMT(icsDocElSizeEditValWidthCaption);
      gbSize->Caption     = FMT(icsDocElSizeEditWidthCaption);
      rbFullSize->Caption = FMT(icsDocElSizeEditFullWidthCaption);
      lbSize->Caption     = FMT(icsDocElSizeEditWidthCaption)+":";
    break;
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::UpdateSizeValidValuesControls()
{
  if ( !(FSizeValidValues & SV_FULLSIZE) )
  {
    rbFullSize->Checked = false;
    rbFullSize->Enabled = false;
  }
  if ( !(FSizeValidValues & SV_AUTOSIZE) )
  {
    rbAutoSize->Checked = false;
    rbAutoSize->Enabled = false;
  }
  if ( !(FSizeValidValues & SV_CUSTOM) )
  {
    rbCustomValue->Checked = false;
    rbCustomValue->Enabled = false;
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::UpdateSizeValueTypeControls()
{
  switch( FSizeValueType )
  {
    case vtNone:
      rbFullSize->Checked    = false;
      rbAutoSize->Checked    = false;
      rbCustomValue->Checked = false;
      DisableSizeCustomValueEditors();
    break;

    case vtFullSize:
      rbFullSize->Checked = true;
    break;

    case vtAutoSize:
      rbAutoSize->Checked = true;
    break;

    case vtCustom:
      rbCustomValue->Checked = true;
    break;
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::UpdateSizeCustomValueUnitControls()
{
  cbSizeUnit->ItemIndex = FSizeCustomValueUnit;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TfmDocElSizeEdit::SizeCustomValueAsText()
{
  UnicodeString Result = "";
  switch ( FSizeCustomValueUnit )
  {
    case vuMM:
      Result = FloatToStrF( FSizeCustomValue, ffNumber, 10, 0 );
    break;

    case vuCM:
      Result = FloatToStrF( FSizeCustomValue, ffNumber, 10, 1 );
    break;
  }
  return Result;
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::UpdateSizeCustomValueControls()
{
  edSize->Text = SizeCustomValueAsText();
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::SetSizeType( TSizeType AValue )
{
  if ((AValue < stHeight) || (AValue > stWidth))
    throw DKClasses::EInvalidArgument(
      __FUNC__,
      "AValue",
      FMT1(icsDocElSizeEditErrorSizeType,IntToStr((int)AValue))
    );
  if ( FSizeType != AValue )
  {
    FSizeType = AValue;
    switch ( FSizeType )
    {
      case stHeight: FMaxSizeMM = MaxHeightMMDefVal; break;
      case stWidth : FMaxSizeMM = MaxWidthMMDefVal;  break;
    }
    UpdateSizeTypeControls();
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::SetSizeValidValues( BYTE AValue )
{
  if ( FSizeValidValues != AValue )
  {
    if ( ( ( FSizeValueType == vtFullSize ) && ( !( AValue & SV_FULLSIZE ) ) ) ||
         ( ( FSizeValueType == vtAutoSize ) && ( !( AValue & SV_AUTOSIZE ) ) ) ||
         ( ( FSizeValueType == vtCustom   ) && ( !( AValue & SV_CUSTOM   ) ) ) )
      SizeValueType = vtNone;
    FSizeValidValues = AValue;
    UpdateSizeValidValuesControls();
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::SetSizeValueType( TSizeValueType AValue )
{
  if ((AValue < vtNone) || (AValue > vtCustom))
    throw DKClasses::EInvalidArgument(
      __FUNC__,
      "AValue",
      FMT1(icsDocElSizeEditErrorSizeType,IntToStr((int)AValue))
    );

  if ( FSizeValueType != AValue )
  {
    if ((AValue > vtNone) && (!((0x01 << static_cast<int>(AValue)) & FSizeValidValues)))
      throw DKClasses::EInvalidArgument(
        __FUNC__,
        "AValue",
        FMT1(icsDocElSizeEditErrorSizeType,IntToStr((int)AValue))
      );
    FSizeValueType = AValue;
    UpdateSizeValueTypeControls();
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::SetSizeCustomValueUnit(TSizeCustomValueUnit AValue)
{
  if ((AValue < vuMM) || (AValue > vuCM))
    throw DKClasses::EInvalidArgument(
      __FUNC__,
      "AValue",
      FMT1(icsDocElSizeEditErrorUnitsType,IntToStr((int)AValue))
    );
  if ( FSizeCustomValueUnit != AValue )
  {
    FSizeCustomValueUnit = AValue;
    UpdateSizeCustomValueUnitControls();
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::SetSizeCustomValue(float AValue)
{
  if (AValue < 0)
    throw DKClasses::EInvalidArgument(
      __FUNC__,
      "AValue",
      FMT(icsDocElSizeEditErrorSizeVal)
    );
  float fMaxMMVal;
  switch ( FSizeCustomValueUnit )
  {
    case vuMM: fMaxMMVal = FMaxSizeMM;          break;
    case vuCM: fMaxMMVal = FMaxSizeMM / 10.;    break;
  }
  if (AValue > fMaxMMVal)
    throw DKClasses::EInvalidArgument(
      __FUNC__,
      "AValue",
      FMT2(icsDocElSizeEditErrorSizeValRange,
           FloatToStrF(AValue, ffNumber, 7, 1),
           FloatToStrF(fMaxMMVal, ffNumber, 7, 1))
    );
  FSizeCustomValue = AValue;
  UpdateSizeCustomValueControls();
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::SetMaxSizeMM(int AValue)
{
  if (AValue < 0)
    throw DKClasses::EInvalidArgument(
      __FUNC__,
      "AValue",
      FMT(icsDocElSizeEditErrorMaxSizeVal)
    );
  FMaxSizeMM = AValue;
}

//---------------------------------------------------------------------------

int __fastcall TfmDocElSizeEdit::GetSizeCustomValueAsMM()
{
  int nResult = 0;
  switch ( FSizeCustomValueUnit )
  {
    case vuMM: nResult = FSizeCustomValue;                      break;
    case vuCM: nResult = RoundTo( FSizeCustomValue, -1 ) * 10;  break;
  }
  return nResult;
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::SetSizeCustomValueAsMM( int AValue )
{
  switch ( FSizeCustomValueUnit )
  {
    case vuMM: SizeCustomValue = AValue;        break;
    case vuCM: SizeCustomValue = AValue / 10.;  break;
  }
}

//---------------------------------------------------------------------------

TColor __fastcall TfmDocElSizeEdit::GetReqEdBkColor() const
{
  return dynamic_cast<TICSDocSpec*>(Owner)->ReqEdBkColor;
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::EnableSizeCustomValueEditors()
{
  lbSize->Font->Color     = clWindowText;
  lbSizeUnit->Font->Color = clWindowText;
  edSize->Enabled     = true;
  cbSizeUnit->Enabled = true;
}

//---------------------------------------------------------------------------

void __fastcall TfmDocElSizeEdit::DisableSizeCustomValueEditors()
{
  lbSize->Font->Color     = clGrayText;
  lbSizeUnit->Font->Color = clGrayText;
  edSize->Enabled     = false;
  cbSizeUnit->Enabled = false;
}

//***************************************************************************
//********************** ����������� �������*********************************
//***************************************************************************

void __fastcall TfmDocElSizeEdit::bOkClick(TObject *Sender)
{
  if ( !AreInputsCorrect() )
    return;

  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TfmDocElSizeEdit::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}
//---------------------------------------------------------------------------
void __fastcall TfmDocElSizeEdit::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}
//---------------------------------------------------------------------------
void __fastcall TfmDocElSizeEdit::rbFullSizeClick(TObject *Sender)
{
  DisableSizeCustomValueEditors();
  FSizeValueType = vtFullSize;
}
//---------------------------------------------------------------------------
void __fastcall TfmDocElSizeEdit::rbAutoSizeClick(TObject *Sender)
{
  DisableSizeCustomValueEditors();
  FSizeValueType = vtAutoSize;
}
//---------------------------------------------------------------------------
void __fastcall TfmDocElSizeEdit::rbCustomValueClick(TObject *Sender)
{
  EnableSizeCustomValueEditors();
  FSizeValueType = vtCustom;
}
//---------------------------------------------------------------------------
void __fastcall TfmDocElSizeEdit::cbSizeUnitPropertiesChange(TObject *Sender)
{
  if ( FSizeCustomValueUnit != cbSizeUnit->ItemIndex )
  {
    FSizeCustomValueUnit = static_cast<TSizeCustomValueUnit>(cbSizeUnit->ItemIndex);
    switch ( static_cast<TSizeCustomValueUnit>(cbSizeUnit->ItemIndex) )
    {
      case vuMM:
        SizeCustomValue *= 10;
      break;

      case vuCM:
        SizeCustomValue /= 10;
      break;
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TfmDocElSizeEdit::edSizePropertiesChange(TObject *Sender)
{
  SizeCustomValue = DKStrToFloatDef(edSize->Text, ',', 0);
}
//---------------------------------------------------------------------------
void __fastcall TfmDocElSizeEdit::edSizeExit(TObject *Sender)
{
  SizeCustomValue = DKStrToFloatDef(edSize->Text, ',', 0);
}
//---------------------------------------------------------------------------

