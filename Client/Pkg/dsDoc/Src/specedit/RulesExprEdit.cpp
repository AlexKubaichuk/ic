/*
  ����        - RulesExprEdit.cpp
  ������      - ��������������� (ICS)
  ��������    - ���� ���������� ������, ����������� �������� �����
                ��� �������������� ������� ���������� ���
                ������ ��� ������� ������� ( ��� "table" ) �
                ��� �������������� ������ �������� ������� �����
                ������ � �������
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 11.10.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "RulesExprEdit.h"
#include "ICSDocSpec.h"
//#include "DKMBUtils.h"
#include "msgdef.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"

//***************************************************************************
//********************** ��������/��������/����� ����� **********************
//***************************************************************************

__fastcall TfmRulesExprEdit::TfmRulesExprEdit(TComponent* Owner, UnicodeString  ACaptionDopInfo)
        : TForm(Owner)
{
  Caption = FMT(icsDocRulesExprEditCaption);
  lbTitle->Caption = FMT(icsDocRulesExprEditlbTitleLabCaption);
  bOk->Caption = FMT(icsDocRulesExprEditbOkBtnCaption);
  bCancel->Caption = FMT(icsDocRulesExprEditbCancelBtnCaption);
  bHelp->Caption = FMT(icsDocRulesExprEditbHelpBtnCaption);

  FCaptionDopInfo  = ACaptionDopInfo;
  FExprParser      = NULL;
  FRulesType       = rtUnknown;
  FRulesOwnerType  = otTableRow;
  FRowNumConstraint = 0;        //��� �����������
  FColNumConstraint = 0;        //��� �����������
}

//---------------------------------------------------------------------------

__fastcall TfmRulesExprEdit::~TfmRulesExprEdit()
{
  __DELETE_OBJ( FExprParser )
}

//---------------------------------------------------------------------------

void __fastcall TfmRulesExprEdit::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif

  switch ( FRulesType )
  {
    case rtUnknown:
      Caption = FMT(icsDocRulesExprEditrtUnknown);
    break;

    case rtCalc   :
      Caption = FMT(icsDocRulesExprEditrtCalcCaption);
      lbTitle->Caption =  FMT(icsDocRulesExprEditrtCalclbTitleCaption);
    break;

    case rtHand   :
      Caption = FMT(icsDocRulesExprEditrtHandCaption);
      lbTitle->Caption = FMT(icsDocRulesExprEditrtHandlbTitleCaption);
    break;
  }
  if ( FCaptionDopInfo != "" )
    lbTitle->Caption = lbTitle->Caption + " " + FCaptionDopInfo;
  lbTitle->Caption = lbTitle->Caption + ":";
}

//***************************************************************************
//********************** ������ ����� ***************************************
//***************************************************************************

void __fastcall TfmRulesExprEdit::ThrowIfParserMissing( UnicodeString MethodName ) const
{
  if (!FExprParser)
    throw DKClasses::EMethodError(MethodName, "����������� ���������� ������.");
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TfmRulesExprEdit::GetExpression() const
{
  return edExpr->Text;
}

//---------------------------------------------------------------------------

void __fastcall TfmRulesExprEdit::SetExpression( UnicodeString  AValue )
{
  edExpr->Text = AValue;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TfmRulesExprEdit::GetPolExp() const
{
  ThrowIfParserMissing(__FUNC__);
  return FExprParser->PolandStr;
}

//---------------------------------------------------------------------------

TLexMap __fastcall TfmRulesExprEdit::GetNameMap() const
{
  ThrowIfParserMissing(__FUNC__);
  return FExprParser->NameMap;
}

//---------------------------------------------------------------------------

void __fastcall TfmRulesExprEdit::SetRulesType( TRulesType AValue )
{
  if (AValue == rtUnknown)
    throw DKClasses::EInvalidArgument(
      __FUNC__,
      "AValue",
      "�������� \"rtUnknown\" ������������ ������ ��� ���������� �����."
    );
  FRulesType = AValue;
  __DELETE_OBJ( FExprParser )
  switch( FRulesType )
  {
    case rtCalc: FExprParser = new TCalcRulesParser; break;
    case rtHand: FExprParser = new THandRulesParser; break;
  }
}

//---------------------------------------------------------------------------

bool __fastcall TfmRulesExprEdit::AreInputsCorrect()
{
  ThrowIfParserMissing(__FUNC__);
  FExprParser->Init();
  if ( Expression != "" )
  {
    FExprParser->Expr = Expression;
    if ( FExprParser->Parse() )
    {
      for ( TLexList::const_iterator i = FExprParser->LexList.begin(); i != FExprParser->LexList.end(); i++ )
      {
        LEX_TABLE_REC ltRec = FExprParser->LexTable->GetRec( *i );
        if ( ltRec.Type == ltRowColNum )
        {
          int nNumConstraint = 0;
          switch ( FRulesOwnerType )
          {
            case otTableRow: nNumConstraint = FRowNumConstraint; break;
            case otTableCol: nNumConstraint = FColNumConstraint; break;
          }
          if ( nNumConstraint )
            if ( StrToInt( GetRowColNumLexNumber( ltRec.Data ) ) > nNumConstraint )
            {
              UnicodeString ErrMsg;
              switch ( FRulesOwnerType )
              {
                case otTableRow:
                  ErrMsg = static_cast<UnicodeString>("������ ����� �������������� ������. ������� ") +
                           (*i).ExprPos +
                           ".";
                break;

                case otTableCol:
                  ErrMsg = static_cast<UnicodeString>("������ ����� ��������������� �������. ������� ") +
                           (*i).ExprPos +
                           ".";
                break;
              }
             _MSG_ERR( ErrMsg, TICSDocSpec::ICSName );
              edExpr->SetFocus();
              edExpr->SelStart  = (*i).ExprPos - 1;
              edExpr->SelLength = ltRec.Data.Length();
              return false;
            }
        }
        else if ( ltRec.Type == ltCellNum )
        {
          bool bError = false;
          UnicodeString ErrMsg;
          if ( FRowNumConstraint )
            if ( StrToInt( GetCellNumLexRowNumber( ltRec.Data ) ) > FRowNumConstraint )
            {
              bError = true;
              ErrMsg = static_cast<UnicodeString>("������ ����� �������������� ������. ������� ") +
                       (*i).ExprPos +
                       ".";
            }
          if ( !bError && FColNumConstraint )
            if ( StrToInt( GetCellNumLexColNumber( ltRec.Data ) ) > FColNumConstraint )
            {
              bError = true;
              ErrMsg = static_cast<UnicodeString>("������ ����� ��������������� �������. ������� ") +
                       (*i).ExprPos +
                       ".";
            }
          if ( bError )
          {
            _MSG_ERR(ErrMsg, TICSDocSpec::ICSName);
            edExpr->SetFocus();
            edExpr->SelStart  = (*i).ExprPos - 1;
            edExpr->SelLength = ltRec.Data.Length();
            return false;
          }
        }
      }
    }
    else
    {
      _MSG_ERR(FExprParser->GetErrMsg( true ), TICSDocSpec::ICSName);
      edExpr->SetFocus();
      edExpr->SelStart  = FExprParser->ErrPos - 1;
      edExpr->SelLength = FExprParser->ErrLen;
      return false;
    }
  }
  return true;
}

//***************************************************************************
//********************** ����������� �������*********************************
//***************************************************************************

void __fastcall TfmRulesExprEdit::bOkClick(TObject *Sender)
{
  if ( !CheckDKFilterControls( Sender ) )	//��� ����������� �� ������ DKComp,
    return;					//������������ TDKControlFilter

  if ( !AreInputsCorrect() )
    return;

  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TfmRulesExprEdit::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TfmRulesExprEdit::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}

//---------------------------------------------------------------------------


