/*
  ����        - SetRef.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� �������� �����
                ��� ��������� ������
  ����������� - ������� �.�.
  ����        - 04.10.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  16.03.2006
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  27.01.2006
    [*] �������� ������������ ��������� ����������

  04.10.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef SetRefH
#define SetRefH

//---------------------------------------------------------------------------

//#include "ICSDoc.h"	//��������� �������� � ���� ��������

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include "XMLContainer.h"

namespace SetRef
{

//***************************************************************************
//********************** ������� ********************************************
//***************************************************************************

//***************************************************************************
//********************** ���� ***********************************************
//***************************************************************************

typedef bool __fastcall (__closure *TOnCanRefUpdate)( TObject* Sender, TTagNode* ndRef );
typedef void __fastcall (__closure *TOnLoadRefObjects)( TObject* Sender, TTreeView* tv );

//***************************************************************************
//********************** ��������� ���������� ������� ***********************
//***************************************************************************

//---------------------------------------------------------------------------

//����� ��� ��������� ������

class PACKAGE TfmSetRef : public TForm
{
__published:	// IDE-managed Components
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
        TTreeView *tv;
        TButton *bClear;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bOkClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall tvChange(TObject *Sender, TTreeNode *Node);
        void __fastcall bClearClick(TObject *Sender);
        void __fastcall tvDblClick(TObject *Sender);
private:	// User declarations
        UnicodeString FRef;
        UnicodeString FCurDocGUI;
        bool FIsRefRequared;
        bool FAreRefObjectsLoaded;
        TOnCanRefUpdate FOnCanRefUpdate;
        TOnLoadRefObjects FOnLoadRefObjects;


protected:
        UnicodeString __fastcall GetRef();
        void __fastcall SetRef( UnicodeString  AValue );
        UnicodeString __fastcall GetObjTreeTitle();
        void __fastcall SetObjTreeTitle( UnicodeString  AValue );

public:		// User declarations
        __fastcall TfmSetRef( TComponent* Owner, bool AIsRefRequared = true );
        __property UnicodeString ObjTreeTitle = { read = GetObjTreeTitle, write = SetObjTreeTitle };
        __property UnicodeString Ref = { read = FRef, write = SetRef };
        __property UnicodeString CurDocGUI = { read = FCurDocGUI, write = FCurDocGUI };
        __property TOnCanRefUpdate OnCanRefUpdate = { read = FOnCanRefUpdate, write = FOnCanRefUpdate };
        __property TOnLoadRefObjects OnLoadRefObjects = { read = FOnLoadRefObjects, write = FOnLoadRefObjects };
        void __fastcall LoadRefObjects();
};

//---------------------------------------------------------------------------

} //end of namespace SetRef
using namespace SetRef;

#endif
