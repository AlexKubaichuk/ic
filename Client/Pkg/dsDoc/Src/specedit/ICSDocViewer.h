/*
  ����        - ICSDocViewer.h
  ������      - ��������������� (ICS)
  ��������    - ����������� ���������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 18.10.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  v1.2.2  - 20.04.2007
    [!] ��� ��������� �������� "���������� ������� ��������" ��������� ��������� "������" � "�������"

  v1.2.1  - 18.04.2007
    [+] �������� UseExcelCSS, ������������ ������������� ��� ������������
        HTML-��������� CSS ��� ������������� ���������� ��������
        � MS Excel 2000/XP/2003

  v1.2.0  - 15.03.2007
    [!] ��� ���������� ����� ������������ ����������� csCreateHTML

  v1.1.9  - 11.01.2007
    [+] �������� �� ������ ������� ������������� ��� ��������
        TICSDocViewer::Specificator
    [*] �����������

  v1.1.8  - 31.10.2006
    [*] ��������� ������������ �������� � ������ ClearIfNoShowZVal
    [*] ����� ClearIfNoShowZVal ������������ � NormalizeNumericalValue

  v1.1.7  - 16.03.2006
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  v1.1.6  - 06.12.2005
    [!] ��� ���������� ������� ��� ������������ ����������� ���������
        ��� ���������� ������ � ������������ �������

  v1.1.5  - 29.08.2005
    1. ��������� ���� ��� �������� ��������������� ����������� ���������
       � MS Excel

  v1.1.4  - 15.07.2005
    1. ��������� ���� ��� ���������� ������� ��� ������������ ����������� ���������
       ��� ���������� ������

  v1.1.3  - 12.07.2005 - 13.07.2005
    1. ��������� ������ �� ���������� - ���� API-������� GetTempFileName ������������ � �� ������
       ������� ���������� ���, �� ��� �, ������ ��� ��� ��������� ������ ��������� �� ���������
       ���������� �������
    2. �������� ���������� ������� ��� ������������ ����������� ���������
    3. ��������� ����, ��������� � ������������ ������ ������� ���� listcol(link_is(is_attr+)) �
       ������������� ��������� ����������� �������

  v1.1.2  - 23.06.2005
    1. ��������� public-�������� MSWordTemplate � ICSWordTemplateFN
    2. ��������� ����������� ������������ ������ ����� ����������
       ������� MS Word
    3. ����������:
       ���������� ������� ��� ������������ ����������� ���������

  v1.1.1  - 21.06.2005
    1. ��� ������� ���������������� ��������� ���� pmExternalDlgExcel �����
       �������� ������ �������� ���������� ���������, ���� Excel �� ���� �� ��������� ��� ��������

  v1.1.0  - 08.06.2005
    1. ������ ��� ������� ��������������� ����������� � ������� HTML ���������
       ��������� ��������� ���� � ���������� ������
    2. ������� public-�������� TmpFileName

  v1.0.9  - 03.06.2005
    1. ��������� ��������� specificator.dtd ������ 2.1.3
    2. �������� ��� ������� ���������������� ��������� pmExternalDlgWord

  v1.0.8  - 24.05.2005
    1. �������� ��� ������� ���������������� ��������� pmExternalDlgExcel
    2. ��������� ���� � ������������� ������ �������� �������

  v1.0.7  - 04.04.2005
    1. ���������� ������ DKSetVersion �������� �� ������� �������� ������
    2. ��������� ��������� ��������� ����� ������ (xml-��� "list" �������������)
       ��� ���������� TICSDocViewer

  v1.0.6  - 03.03.2005
    1. ��������� ��� � ������ TICSDocViewer::AddHTMLInsText - ���������
       ������� ������� ������ ����������� � ����� ������

  v1.0.5  - 25.10.2004
    1. ��������� __published-�������� PreviewDlgIcon

  v1.0.4  - 25.10.2004
    1. IESetup ������ ��������������� ����� � ������� � �����������
       �������� Internet Explorer'� ������ ���� ��� ����� ��������
       ��������, ���������� ���� IESetup'��
    2. ������ �������� "���������" ��� �������������� HTML-����� � ������
    3. � ������ PreviewDlgKind == pmSystemDlg ��� ������������ �����������
       � ������� HTML IESetup ��������� �������� Internet Explorer'� �� �����������

  v1.0.3  - 24.10.2004
    1. ��������� __published-�������� IESetup

  v1.0.2  - 23.10.2004
    1. ��������� ����: ��� ������������ HTML-���������
       � �������� title'� ������ ���������� mainname �� �������� �������������.
       ������ ��� ������������ ����������� ������������� ������� ���
       ( ������������� ) mainname, � ��� ������������ ����������� ��������� -
       mainname ���������

  v1.0.1  - 18.10.2004
    1. ���������� ����������� � ������ InternalPreviewCreate()
    2. ��������� �������� DocAssign
    3. ��������� �������� ListZeroValCheckCountOnly

  v1.0    - 18.10.2004
    �������������� �������
*/
//---------------------------------------------------------------------------
/*
  ���� �� �������:
*/
//---------------------------------------------------------------------------

#ifndef ICSDocViewerH
#define ICSDocViewerH

//---------------------------------------------------------------------------

#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Registry.hpp>
#include "DKClasses.h"
#include "XMLContainer.h"
#include "IcsXMLDoc.h"
#include "Preview.h"

#include "ICSDocGlobals.h"

namespace ICSDocViewer
{

//###########################################################################
//##                                                                       ##
//##                                 Globals                               ##
//##                                                                       ##
//###########################################################################

// ������ ������������ ���������
enum TICSDocViewerDocFormat
{
  dfHTML,       // ������ HTML
  dfMSWord      // ������ MS Word
};

//---------------------------------------------------------------------------

// ��� ������� ��� �����������
enum TICSDocViewerPreviewType
{
  ptUnknown,            // �� ������ ����, ���� �� ���� ����� :)
  ptSpecificator,       // ���������� ������������
  ptDocument            // ���������� ��������
};

//---------------------------------------------------------------------------

// ����� ������ ������������
enum TICSDocViewerMode
{
  vmOnFly,              // ������������ ��������� "�� ����"
  vmSaveToDisk          // ������������ ��������� � ��������������� ����������� �� ����
};

//---------------------------------------------------------------------------

// ��� ������� ���������������� ���������
enum TICSDocViewerDlgKind
{
  pmInternalDlg,        // ������������ ���������� ������ ����������
  pmSystemDlg,          // ������������ "���������" ������
  pmExternalDlgExcel,   // ������������ "�������" ������ (MS Excel)
  pmExternalDlgWord     // ������������ "�������" ������ (MS Word)
};
/*
  ��� DocFormat == dfHTML
    pmInternalDlg - ����������� ����� � ����������� TCppWebBrowser
    pmSystemDlg   - ����������� browser �� ���������
                    (��� ���� IESetup ��������� �������� Internet Explorer'� �� �����������)
    pmExternalDlgExcel - �������������� html-���� ����������� MS Excel
                         ( MS Excel ����������� ��� ��������������� ����������)
    pmExternalDlgWord  - �������������� html-���� ����������� MS Word
                         ( MS Word ����������� ��� ��������������� ����������)
*/

//---------------------------------------------------------------------------

// ���� ������������ ����������� �������������/���������
enum TICSDocViewerCreateStage
{
  csNone,               // ����������� ��������� � ��������� ���������� �
                        // ������������ ����������� �������������/���������
  csCreateInit,         // ������������� ����� ������������� ����������� �������������/���������
  csCreateHTMLTags,     // ������������ HTML-�����
  csCreateHTML,         // �������������� HTML-����� � ������
  csFillMSWordTemplate, // ���������� ������� MS Word
  csPreviewDlgStart     // ������ ������� ���������������� ���������
};

//---------------------------------------------------------------------------

typedef void __fastcall (__closure *TOnChangeCreateStageEvent)( TObject* Sender, const TICSDocViewerCreateStage ACreateStage );
typedef void __fastcall (__closure *TOnPreviewCreationFinishedEvent)( TObject* Sender, const bool bNormallyTerminated );

//###########################################################################
//##                                                                       ##
//##                              TICSDocIESetup                           ##
//##                                                                       ##
//###########################################################################

// ����� ��� ��������� ���������� �������� Internet Explorer'�

class PACKAGE TICSDocIESetup : public TPersistent
{
    typedef TPersistent inherited;
private:
    bool        FUpdateIEPageSetup;    //������� ���������� ��������
                                       //InternetExplorer'� � ������������
                                       //� ����������� �������� ���������
                                       //��� ������� ������� ����������������
                                       //��������� ��� ������������ �����������
                                       //� ������� HTML
    UnicodeString  FIEFooter;             //������ ����������, ���������� InternetExplorer'��
    UnicodeString  FIEHeader;             //������� ����������, ���������� InternetExplorer'��
    UnicodeString  FBasePageSetupRegPath; //���� � ����� � �������, ��������� ��������� ���������� �������� InternetExplorer'�
    UnicodeString  FFooterKey;
    UnicodeString  FHeaderKey;
    UnicodeString  FMarginBottomKey;
    UnicodeString  FMarginLeftKey;
    UnicodeString  FMarginRightKey;
    UnicodeString  FMarginTopKey;

    bool __PrevCallToIEPageSetupResult;
    //���������� �������� ������ �������, ���������������
    //���������� �������� InternetExplorer'�
    HKEY __RootKey;
    UnicodeString __OldIEFooter;
    UnicodeString __OldIEHeader;
    UnicodeString __OldIEMarginBottom;
    UnicodeString __OldIEMarginLeft;
    UnicodeString __OldIEMarginRight;
    UnicodeString __OldIEMarginTop;

    //����� �������� ������ �������, ���������������
    //���������� �������� InternetExplorer'�
    UnicodeString __NewIEFooter;
    UnicodeString __NewIEHeader;
    UnicodeString __NewIEMarginBottom;
    UnicodeString __NewIEMarginLeft;
    UnicodeString __NewIEMarginRight;
    UnicodeString __NewIEMarginTop;


protected:
    virtual void __fastcall AssignTo(Classes::TPersistent* Dest);

    UnicodeString __fastcall HundredMMToInches(UnicodeString AHMM);
    bool __fastcall InternalReadIEPageSetup(
      HKEY ARootKey,
      UnicodeString &AIEFooter,
      UnicodeString &AIEHeader,
      UnicodeString &AIEMarginBottom,
      UnicodeString &AIEMarginLeft,
      UnicodeString &AIEMarginRight,
      UnicodeString &AIEMarginTop
    );
    bool __fastcall ReadIEPageSetup(
      HKEY &ARootKey,
      UnicodeString &AIEFooter,
      UnicodeString &AIEHeader,
      UnicodeString &AIEMarginBottom,
      UnicodeString &AIEMarginLeft,
      UnicodeString &AIEMarginRight,
      UnicodeString &AIEMarginTop
    );
    bool __fastcall WriteIEPageSetup(
      HKEY ARootKey,
      UnicodeString AIEFooter,
      UnicodeString AIEHeader,
      UnicodeString AIEMarginBottom,
      UnicodeString AIEMarginLeft,
      UnicodeString AIEMarginRight,
      UnicodeString AIEMarginTop
    );


public:
    __fastcall TICSDocIESetup();

    bool __fastcall SetIEPageSetup(
      UnicodeString AIEMarginBottom,
      UnicodeString AIEMarginLeft,
      UnicodeString AIEMarginRight,
      UnicodeString AIEMarginTop
    );
    bool __fastcall RestoreIEPageSetup();


__published:
    //������� ���������� �������� InternetExplorer'� � ������������ � �����������
    //�������� ��������� ��� ������� ������� ���������������� ��������� ���
    //������������ ����������� � ������� HTML
    __property bool                   UpdateIEPageSetup = { read = FUpdateIEPageSetup, write = FUpdateIEPageSetup, default = true };
    //������ ����������, ���������� InternetExplorer'�� ( �� ��������� "&b&d" )
    __property UnicodeString             IEFooter = { read = FIEFooter, write = FIEFooter };
    //������� ����������, ���������� InternetExplorer'�� ( �� ��������� "&b���. &p �� &P" )
    __property UnicodeString             IEHeader = { read = FIEHeader, write = FIEHeader };
    //���� � ����� � �������, ��������� ��������� ���������� �������� InternetExplorer'�
    //(�� ��������� "Software\\Microsoft\\Internet Explorer\\PageSetup")
    __property UnicodeString  BasePageSetupRegPath = { read = FBasePageSetupRegPath, write = FBasePageSetupRegPath };
    //(�� ��������� "footer")
    __property UnicodeString  FooterKey = { read = FFooterKey, write = FFooterKey };
    //(�� ��������� "header")
    __property UnicodeString  HeaderKey = { read = FHeaderKey, write = FHeaderKey };
    //(�� ��������� "margin_bottom")
    __property UnicodeString  MarginBottomKey = { read = FMarginBottomKey, write = FMarginBottomKey };
    //(�� ��������� "margin_left")
    __property UnicodeString  MarginLeftKey = { read = FMarginLeftKey, write = FMarginLeftKey };
    //(�� ��������� "margin_right")
    __property UnicodeString  MarginRightKey = { read = FMarginRightKey, write = FMarginRightKey };
    //(�� ��������� "margin_top")
    __property UnicodeString  MarginTopKey = { read = FMarginTopKey, write = FMarginTopKey };
};

//###########################################################################
//##                                                                       ##
//##                               TICSDocViewer                           ##
//##                                                                       ##
//###########################################################################

// ����������� ���������

class TICSDocViewerThread;
class PACKAGE TICSDocViewer : public TComponent/*, IDKComponentVersion, IDKComponentComment*/
{
    typedef TComponent inherited;
    friend class TICSDocViewerThread;
private:
    UnicodeString             FComment;        //����������� ��� ����������
    TICSDocViewerDocFormat FDocFormat;      //������ ������������ ���������
    TICSDocViewerMode      FMode;           //����� ������ ������������
    bool                   FUseCSS;         //���������� ������������ ��� ������������ HTML-��������� CSS
    bool                   FUseWordCSS;     //���������� ������������� ��� ������������ HTML-��������� CSS
                                            //��� ������������� ���������� �������� �
                                            //MS Word 2000/XP/2003
    bool                   FUseExcelCSS;    //���������� ������������� ��� ������������ HTML-��������� CSS
                                            //��� ������������� ���������� �������� �
                                            //MS Excel 2000/XP/2003
    TICSDocViewerDlgKind   FPreviewDlgKind; //��� ������� ���������������� ���������
    bool                   FAsyncPreviewCreation; //������� ������ ������������ ������������
                                                  //�����������
    TICSDocViewerCreateStage FState;        //��������� ������������ - ���� ������������ �����������
                                            //�������������/���������
    TTagNode*              FSpec;           //������������ ( xml-������ � ������ "specificator" )
    TTagNode*              FDoc;            //�������� ( xml-������ � ������ "docum" )
    UnicodeString             FTmpPath;        //���� � �������� ��� ������������ ��������� HTML-������
                                            //� ������ vmSaveToDisk
    list<UnicodeString>       FTmpFileNames;   //����� ��������� ������ ��� ������ vmSaveToDisk
    UnicodeString             FDefSavePath;    //���� � �������� �� ��������� ��� ����������
                                            //���������������� HTML-�����
    UnicodeString             FHTML;           //HTML-���� � ���� ������
    TTagNode*              FndHTML;         //������ html-�����
    TAxeXMLContainer*      FXMLContainer;   //XML-��������� ( ��. AxeUtil.h ) � �������������
    TICSDocViewerPreviewType FPreviewType;  //��� ������� ��� �����������
    bool                   FDocAssign;      //������� ������������� assign'� ���
                                            //���������
    bool                   FListZeroValCheckCountOnly; //������� ���������� �������
                                                       //����������� ������� ��������
                                                       //��� ������ ������ ���
                                                       //��������, ��������������� count'�
    TICSDocIESetup*        FIESetup;        //��������� ���������� �������� Internet Explorer'�
    TIcon*                 FDefPreviewDlgIcon;
    TIcon*                 FPreviewDlgIcon; //������ ��� ������� ����������������
                                            //��������� ( ��� PreviewDlgKind == pmInternalDlg )

    TICSDocViewerThread*   FThrd;           //����� ��� ������ ������������ ������������ �����������
    bool                   FLocked;         //���������� ����� ���������� ������������ ��� �����������
                                            //������������ �����������
    bool                   FThrdNormalTerminated; //������� ����������� ���������� ������ FThrd
    TTagNode*              FDocXMLTemplate; //������ MS Word, �� ������ �������� �������� �����������
    void                   __fastcall FSetDocXMLTemplate(TTagNode *ATmpl);
//    UnicodeString             FICSWordTemplateFN; //������ ��� ����� ICS_Word.dot

    //�������
    TOnChangeCreateStageEvent FOnChangeCreateStage; //��������� ��� ����� ����� ������������
                                                    //����������� �������������/���������

    TPPreviewDemoMessageEvent FPPDemoMsg;


    TOnPreviewCreationFinishedEvent FOnPreviewCreationFinished;   //��������� ��� ���������� ��������
                                                                  //������������ �����������
                                                                  //�������������/��������� ��� �����������
                                                                  //������ ������������ �����������

    //���� ��� ���������� ���� ( ������������ ��� �������� � ������ ������� ����� ������������ �������� )
    UnicodeString    __BorderWidth;    //������ ����� ������� ( xml-��� "table" ������������� )
                                    //��� ������ ( xml-��� "list" ������������� )
    UnicodeString    __Width;
    bool          __RowNumbers;     //������� ������������� ��������� ����� ������� ( xml-��� "table" ������������� )
                                    //��� ����� ������ ( xml-��� "list" ������������� )
    bool          __ColNumbers;     //������� ������������� ��������� �������� ������� ( xml-��� "table" ������������� )
    int           __RowNum;         //������� ����� ������ ������� ( xml-��� "table" ������������� )
    int           __ColNum;         //������� ����� ������� ������� ( xml-��� "table" ������������� )
    //���� ��� ���������� ����
    TProgressBar* FPB;              //"���������"
    TStringList*  __DopInfo;
    int        __PBMax;             //������������ ��� �������� ������ � ����� InternalSetPBMax()
    int        __PBPosition;        //������������ ��� �������� ������ � ����� InternalSetPBPosition()
    WideString __URL;               //������������ ��� �������� ������ � ����� HTMLPreview()
//    UnicodeString __DocName;           //������������ ��� �������� ������ � ����� ByXMLTemplatePreview()
    bool       __OnFly;
    bool       __CanShowPreviewDlg;
    TICSDocViewerCreateStage __CreateStage; //������������ ��� �������� ������ � ����� GenerateOnChangeCreateStageEvent()

    //*****************************

    //�������� ������ � ������� ������/������� ����������� ������� ( xml-��� "table" ������������� )
    //��� ������ � ������� ������ ����������� ������ ( xml-��� "list" ������������� )
    const UnicodeString FNumTCellAlign;        //������������ ( left | center | right | justify )
    const UnicodeString FNumTCellFontName;     //������������ ������
    const int        FNumTCellFontSize;     //������ ������ ( � ������� )
    const UnicodeString FNumTCellLTitle;       //��������� ������ � ������� ������ ����������� ������

    //*****************************

    /*�������������*/
    TTagNode* __fastcall InitNDHTML( bool bCreateBodyTag = true );
    UnicodeString __fastcall GetTempPath() const;
    /*��������������� ������*/
    void __fastcall CreateHTMLProgressInit(int AMax, UnicodeString AMsg);
    void __fastcall CreateHTMLProgressChange(int APercent, UnicodeString AMsg);
    void __fastcall CreateHTMLProgressComplite(UnicodeString AMsg);

    void __fastcall InternalInitPB();
    void __fastcall InternalSetPBMax();
    void __fastcall InternalIncPB();
    void __fastcall InternalSetPBPosition();
    void __fastcall InitPB();
    void __fastcall SetPBMax(const int nMax);
    void __fastcall IncPB();
    void __fastcall SetPBPosition(const int nPosition);
    void __fastcall InternalGenerateOnChangeCreateStageEvent();
    void __fastcall GenerateOnChangeCreateStageEvent( TICSDocViewerCreateStage ACreateStage );
    void __fastcall ChangeState( TICSDocViewerCreateStage AState, bool bGenerateOnChangeCreateStageEvent = true );
    //*****************************
    void __fastcall ThrowIfLocked( UnicodeString AMethodName );
    //*****************************
    UnicodeString __fastcall GetTableCellStyle( UnicodeString AStyleAttrName, TTagNode *ndCellTagNode, TTagNode *ndAltCellTagNode = NULL );
    UnicodeString __fastcall GetListStyle( UnicodeString AStyleAttrName, TTagNode *ndList );
    UnicodeString __fastcall GetListCellStyle( UnicodeString AStyleAttrName, TTagNode *ndCellTagNode );
    UnicodeString __fastcall GetInsTextStyle( TTagNode *ndInsText );
    UnicodeString __fastcall GetInsTextAlign( TTagNode *ndInsText );
    void __fastcall NormalizeNumericalValue(UnicodeString &ACellText, const bool fShowZVal);
    void __fastcall HiMetricToPixels(long *pnWidth, long *pnHeight);
    /*��������������� ������ ��� ������� HTML*/
    TTagNode* __fastcall AddHTMLTableTag( TTagNode *htmlParent );
    void __fastcall GetHTML_DIVAlignAttrVal( UnicodeString AAlign, UnicodeString &AAttrName, UnicodeString &AAttrVal );
    int __fastcall GetHTMLFontSizeMSOLike( const int nFontSize );
    TTagNode* __fastcall CreateHTMLTextFormat( TTagNode *htmlTag, UnicodeString AStyleUID );
    bool __fastcall SetHTMLTableCellBorderWidth( TTagNode *htmlTableCell, UnicodeString ABorderWidth );
    bool __fastcall SetHTMLBoxWidthValues( TTagNode *htmlTableCell, UnicodeString ABorderWidth, UnicodeString AWidth );
    TTagNode* __fastcall CreateHTMLTableNumCellTextFormat( TTagNode *htmlNumTableCell );


protected:
    /*IDKComponentVersion*/
    virtual UnicodeString __fastcall DKGetVersion() const;
    virtual void       __fastcall DKSetVersion( const UnicodeString AValue );
    /*IDKComponentComment*/
    virtual UnicodeString __fastcall DKGetComment() const;
    virtual void       __fastcall DKSetComment( const UnicodeString AValue );
    /*TICSDocViewer*/
    /*��������������� ������*/
    void __fastcall SetTmpPath( UnicodeString AValue );
    void __fastcall SetDefSavePath( UnicodeString AValue );
    void __fastcall SetXMLContainer( const TAxeXMLContainer *AValue );
    void __fastcall SetMode( const TICSDocViewerMode AValue );
    void __fastcall SetUseCSS( const bool AValue );
    void __fastcall SetUseWordCSS( const bool AValue );
    void __fastcall SetUseExcelCSS( const bool AValue );
    void __fastcall SetPreviewDlgKind( const TICSDocViewerDlgKind AValue );
    void __fastcall SetAsyncPreviewCreation( const bool AValue );
    void __fastcall SetOnChangeCreateStage( const TOnChangeCreateStageEvent AValue );
    void __fastcall SetOnPreviewCreationFinished( const TOnPreviewCreationFinishedEvent AValue );
    void __fastcall SetDocFormat( const TICSDocViewerDocFormat AValue );
    void __fastcall SetSpecificator( const TTagNode *AValue );
    void __fastcall SetDocument( const TTagNode *AValue );
    void __fastcall SetDocAssign( const bool AValue );
    void __fastcall SetListZeroValCheckCountOnly( const bool AValue );
    void __fastcall SetIESetup( const TICSDocIESetup* AValue );
    void __fastcall SetPreviewDlgIcon( const TIcon* AValue );

    TAxeXMLContainer* __fastcall GetXMLContainer() const;
    //*****************************
    void __fastcall ThreadTerminate( TObject *Sender );
    bool __fastcall CheckIfTerminated();
    //*****************************
    UnicodeString __fastcall GetListColHeaderText( TTagNode *ndListCol );
    UnicodeString __fastcall GetTableHeaderCellText( TTagNode *ndHeaderCell );
    UnicodeString __fastcall GetDocListCellText( TTagNode* &ndV, const TTagNode *ndListCol, const bool bShowZVal );
    long __fastcall GetListColsMaxLevel( const TTagNode *ANode ) const;
    int __fastcall GetMaxPBForSpec();
    int __fastcall GetMaxPBForDoc();
    int __fastcall GetMaxPBForDocByMSWordTemplate();
    void __fastcall InternalPreviewCreate();
    void __fastcall InternalPreviewDlg();
    /*��������������� ������ ��� ������� HTML*/
    bool __fastcall ReadIEPageSetup( HKEY ARootKey );
    bool __fastcall WriteIEPageSetup( HKEY ARootKey,
                                      UnicodeString AIEFooter,
                                      UnicodeString AIEHeader,
                                      UnicodeString AIEMarginBottom,
                                      UnicodeString AIEMarginLeft,
                                      UnicodeString AIEMarginRight,
                                      UnicodeString AIEMarginTop );

    bool __fastcall SetIEPageSetup();
    bool __fastcall RestoreIEPageSetup();
    UnicodeString __fastcall GetHTMLStyleDefinition( UnicodeString  AStyleClass,
                                                  UnicodeString  AStyleClassStrPrefix,
                                                  UnicodeString  AStylePropStrPrefix,
                                                  TTagNode*         ndStyle );
    UnicodeString __fastcall GetHTMLSTYLESDefinition( UnicodeString  AStyleClassStrPrefix,
                                                   UnicodeString  AStylePropStrPrefix,
                                                   TTagNode*         ndStyles );
    UnicodeString __fastcall GetMSOWordHTMLSTYLESDefinition(
      UnicodeString  AStyleClassStrPrefix,
      UnicodeString  AStylePropStrPrefix,
      TTagNode*         ndPageSetup
    );
    UnicodeString __fastcall GetMSOExcelHTMLSTYLESDefinition(
      UnicodeString  AStyleClassStrPrefix,
      UnicodeString  AStylePropStrPrefix,
      TTagNode*         ndPageSetup
    );
    UnicodeString __fastcall GetTableHTMLSTYLESDefinition( UnicodeString  AStyleClassStrPrefix,
                                                        UnicodeString  AStylePropStrPrefix );
    void __fastcall CreateHTML( TICSDocViewerPreviewType APreviewType );
    void __fastcall CreateHTMLListHeaderCell( TTagNode *ndListEl, TTagNode *htmlList, TTagNode *htmlRow = NULL );
    void __fastcall CreateHTMLListHeader( TTagNode *ndListEl, TTagNode *htmlList, TTagNodeList *AListSingleCols = NULL );
    void __fastcall CreateHTMLListBodyRow( const TTagNodeList *AListSingleCols, TTagNode *htmlList, TTagNodeList *ADataCells = NULL );
    TTagNode* __fastcall CreateHTMLTableHeaderCell( UnicodeString  ACellTagNodeName,
                                                    TTagNode*         ndCellTagNode,
                                                    TTagNode*         htmlRow );
    void __fastcall CreateHTMLTableHeaderCols( TTagNode *ndCol, TTagNode *htmlTable, TTagNode *htmlRow = NULL );
    void __fastcall CreateHTMLTableCols( TTagNode *ndColumns, TTagNode *htmlTable, TTagNodeList *ATableSingleCols = NULL );
    void __fastcall CreateHTMLTableRows( TTagNode *ndRow, TTagNode *htmlTable, const TTagNodeList &ATableSingleCols, TTagNodeList2 *DataCells, TTagNode *htmlRow = NULL );
    TTagNode* __fastcall AddHTMLInscription( TTagNode *ndInscription, TTagNode *htmlParent );
    TTagNode* __fastcall AddHTMLInsText( TTagNode *ndInsText, TTagNode **htmlParent );
    TTagNode* __fastcall AddHTMLList( TTagNode *ndList, TTagNode *htmlParent, TTagNodeList *AListSingleCols = NULL, int nRowCount = 1 );
    TTagNode* __fastcall AddHTMLTable( TTagNode *ndTable, TTagNode *htmlParent, TTagNodeList2 *DataCells = NULL );
    void __fastcall CreateSpecHTML();
    void __fastcall CreateDocHTML();
    void __fastcall InitHTMLPreview();
    void __fastcall HTMLPreview();
    /*��������������� ������ ��� ������� MSWord*/
    void __fastcall CreateSpecMSWord();
    void __fastcall CreateDocMSWord();
    void __fastcall InitMSWordPreview();
    void __fastcall MSWordPreview();
    /*��������������� ������ ��� ��� ������������ ������ �� ������� MSWord*/
    bool __fastcall CreateMSWordDocByTemplate();
    void __fastcall CreateSpecByMSWordTemplate();
    void __fastcall CreateDocByMSWordTemplate();
    void __fastcall ByXMLTemplatePreview(TXMLDoc *ADoc);


public:
    __fastcall TICSDocViewer(TComponent* Owner);
    virtual __fastcall ~TICSDocViewer();

    //*****************************

    //������������ ������������ ����������
    static const UnicodeString ICSName;

    //*****************************

    //������������ ( xml-������ � ������ "specificator" ) - ��� ��������� �������� �������� Assign
    __property TTagNode*  Specificator = { read = FSpec,        write = SetSpecificator };
    //�������� ( xml-������ � ������ "docum" ) - ��� ��������� �������� � ����������� ��
    //�������� DocAssign �������� ��� �� �������� Assign
    __property TTagNode*  Document     = { read = FDoc,         write = SetDocument     };
    //���� � �������� ��� ������������ ��������� ������ � ������ vmSaveToDisk (�� ��������� ���� �
    //���������� ���������� ��������)
    __property UnicodeString TmpPath      = { read = FTmpPath,     write = SetTmpPath      };
    //���� � �������� �� ��������� ��� ���������� ���������������� HTML-�����
    __property UnicodeString DefSavePath  = { read = FDefSavePath, write = SetDefSavePath  };
    //XML-��������� ( ��. AxeUtil.h ) � ������������� !!! SetXMLContainer �� ������ Assign !!!
    __property TAxeXMLContainer*  XMLContainer = { read = FXMLContainer, write = SetXMLContainer };
    //������ MS Word, �� ������ �������� �������� ����������� - ��� ��������� �������� ��������
    //�� �������� Assign
    __property TTagNode* XMLTemplate = { read = FDocXMLTemplate, write = FSetDocXMLTemplate };
    //������ ��� ����� ICS_Word.dot
//    __property UnicodeString  ICSWordTemplateFN = { read = FICSWordTemplateFN, write = FICSWordTemplateFN };

    //*****************************

    //������������ ����������� ���������/������������� � ������ �������
    //���������������� ���������
    void __fastcall Preview( TICSDocViewerPreviewType APreviewType, TProgressBar *APB = NULL );
    //���������� �������� ������������ ����������� ��� ����������� ������
    void __fastcall StopAsyncPreview();
    //����������� ������, ������������ �����������t ����������� ��� ����������� ������
    bool __fastcall KillAsyncPreview();


__published:
    /*��������*/
    //������ ����������
    __property UnicodeString             About      = { read = DKGetVersion, write = DKSetVersion,   stored = false         };
    //����������� ��� ����������
    __property UnicodeString             Comment    = { read = DKGetComment, write = DKSetComment                           };
    //������ ������������ ���������
    __property TICSDocViewerDocFormat DocFormat  = { read = FDocFormat,   write = SetDocFormat,   default = dfHTML       };
    //����� ������ ������������
    __property TICSDocViewerMode      Mode       = { read = FMode,        write = SetMode,        default = vmSaveToDisk };
    //���������� ������������� ��� ������������ HTML-��������� CSS
    __property bool                   UseCSS     = { read = FUseCSS,      write = SetUseCSS,      default = true         };
    //���������� ������������� ��� ������������ HTML-��������� CSS ��� ������������� ���������� �������� �
    //MS Word 2000/XP/2003
    __property bool                   UseWordCSS = { read = FUseWordCSS,  write = SetUseWordCSS,  default = true         };
    //���������� ������������� ��� ������������ HTML-��������� CSS ��� ������������� ���������� �������� �
    //MS Excel 2000/XP/2003
    __property bool                   UseExcelCSS = { read = FUseExcelCSS,  write = SetUseExcelCSS,  default = true         };
    //��� ������� ���������������� ���������
    __property TICSDocViewerDlgKind   PreviewDlgKind = { read = FPreviewDlgKind, write = SetPreviewDlgKind, default = pmInternalDlg };
    //������� ������ ������������ ������������ �����������
    __property bool                   AsyncPreviewCreation = { read = FAsyncPreviewCreation, write = SetAsyncPreviewCreation, default = true };
    //���������� ����� ���������� ������������ ��� ����������� ������������ �����������
    __property bool                   Locked = { read = FLocked };
    //��������� ������������ - ���� ������������ ����������� �������������/���������
    __property TICSDocViewerCreateStage State = { read = FState };
    //������� ������������� assign'� ��� ��������� ( �������� Document )
    __property bool                   DocAssign = { read = FDocAssign, write = SetDocAssign, default = false };
    //������� ���������� ������� ����������� ������� �������� ��� ������ ������ ���
    //��������, ��������������� count'�
    __property bool                   ListZeroValCheckCountOnly = { read = FListZeroValCheckCountOnly, write = SetListZeroValCheckCountOnly, default = true };
    //��������� ���������� �������� Internet Explorer'�
    __property TICSDocIESetup*        IESetup = { read = FIESetup, write = SetIESetup };
    //������ ��� ������� ���������������� ��������� ( ��� PreviewDlgKind == pmInternalDlg )
    __property TIcon*                 PreviewDlgIcon = { read = FPreviewDlgIcon, write = SetPreviewDlgIcon };

    /*�������*/
    //��������� ��� ����� ����� ������������ ����������� �������������/���������
    __property TOnChangeCreateStageEvent OnChangeCreateStage = { read = FOnChangeCreateStage, write = SetOnChangeCreateStage };

    __property TPPreviewDemoMessageEvent OnDemoMessage = {read=FPPDemoMsg,write=FPPDemoMsg};

    //��������� ��� ���������� �������� ������������ ����������� �������������/��������� ��� �����������
    //������ ������������ �����������
    __property TOnPreviewCreationFinishedEvent OnPreviewCreationFinished = { read = FOnPreviewCreationFinished, write = SetOnPreviewCreationFinished };
};

//---------------------------------------------------------------------------

} // end of namespace ICSDocViewer
using namespace ICSDocViewer;

#endif
