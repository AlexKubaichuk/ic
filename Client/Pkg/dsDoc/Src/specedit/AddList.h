/*
  ����        - AddList.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� �������� �����
                ��� ���������� ������ ( ��� "list" ) ���
                �������� ������ ( ��� "list" )
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 08.10.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  15.03.2007
    [-] ������������� �������� �� ������ ����������� ��������

  16.03.2006
    [*] ��������� uid �������������� ���������� TTagNode
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  27.01.2006
    [*] � ���� ����� "������ �������� ��" ����� ������� ��� "�������������"
    [*] �������� ������������ ��������� ����������

  11.07.2005
    1. �� �������� ������ �� �������� ������ �������� ������������ �������� "�������� �������"
    2. �� �������� ������ �� �������� ��������� �������� ������ ������������ �������� "�������"

  02.06.2005
    1. ������� ��������������� ��������� ��� ��������� specificator.dtd ������ 2.1.3

  23.05.2005
    1. ��������� hint'� ��� ������
    2. ��������� ���������� ������ ������������ ������ �������� �� F2

  08.10.2004
    �������������� �������

*/
//---------------------------------------------------------------------------

#ifndef AddListH
#define AddListH

//---------------------------------------------------------------------------

#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
//#include "ICSDoc.h"  //��������� �������� � ���� ��������
#include "ICSDocGlobals.h"
//---------------------------------------------------------------------------

namespace ICSDocSpec
{
class PACKAGE TICSDocSpec;
}

namespace AddList
{

//***************************************************************************
//********************** ���� ***********************************************
//***************************************************************************

typedef int __fastcall (__closure *TOnObjListLoadEvent)( TObject* Sender, TTreeNodes* TreeNodes, TTagNode* MainIS );

//����� ������ �����
enum TAddListMode {
  alNewList,                    //���������� ������ ( ��� "list" )
  alNewListCols,                //���������� �������� ������ ( ��� "list" )
  alNewLinkISAttrsListCols      //���������� �������� ������ ( ��� "list" ),
                                //��������������� ��������� ��������� �������� 
};

//---------------------------------------------------------------------------

//����� ��� ���������� ������ ( ��� "list" ) ��� �������� ������ ( ��� "list" )

class PACKAGE TfmAddList : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *gbClient;
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
        TLabel *lbISListTitle;
        TTreeView *tvISs;
        TLabel *lbISTreeTitle;
        TButton *bAddCols;
        TButton *bDelete;
        TTreeView *tvCols;
        TLabel *lbColsTitle;
        TButton *bMoveUp;
        TButton *bMoveDown;
        TButton *bAddGCols;
        TButton *bClear;
        TLabel *lbMainIS;
        TActionList *ActionList1;
        TAction *actAddCols;
        TAction *actAddGCols;
        TAction *actDelete;
        TAction *actClear;
        TAction *actUp;
        TAction *actDown;
  TcxComboBox *cbISs;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bOkClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall tvISsChange(TObject *Sender, TTreeNode *Node);
        void __fastcall tvColsChange(TObject *Sender, TTreeNode *Node);
        void __fastcall tvColsEdited(TObject *Sender, TTreeNode *Node,
          UnicodeString &S);
        void __fastcall tvColsKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall tvISsCustomDrawItem(TCustomTreeView *Sender,
          TTreeNode *Node, TCustomDrawState State, bool &DefaultDraw);
        void __fastcall tvColsEditing(TObject *Sender, TTreeNode *Node,
          bool &AllowEdit);
        void __fastcall tvISsDblClick(TObject *Sender);
        void __fastcall tvColsDblClick(TObject *Sender);
        void __fastcall actAddColsExecute(TObject *Sender);
        void __fastcall actAddGColsExecute(TObject *Sender);
        void __fastcall actDeleteExecute(TObject *Sender);
        void __fastcall actClearExecute(TObject *Sender);
        void __fastcall actUpExecute(TObject *Sender);
        void __fastcall actDownExecute(TObject *Sender);
  void __fastcall cbISsPropertiesChange(TObject *Sender);


private:	// User declarations
        TWndMethod    FOldGBClientWndProc;
        TAddListMode  FAddListMode;             //����� ������ �����
        bool          FCanAddGroup;             //�������, ������������ ������������
                                                //���������� ����� �������� � ��������,
                                                //��������������� ��������� ��������� ��������
        TTagNode*     FMainISTagNode;           //TagNode, ��������������� �������� ��������
        TTagNode*     FFakeTag;                 //TagNode ��� ������������ ������
                                                //����������� ��������
        TTreeNodeList FAddedLinkIS;             //������ TTreeNode*, ���������������
                                                //����������� ����� link_is ��� ��������� ����������
                                                //��������
        ICSDocSpec::TICSDocSpec*  FDocSpec;     //�������� �������������
        TOnObjListLoadEvent FOnObjListLoad;     //������� "��� �������� ������ �������� ��� ������"

        //���� ��� ���������� ����
        bool __CanNotAddLinkISAttrColMsgShown;


protected:
        void __fastcall GBClientWndProc(Messages::TMessage &Message);
        void __fastcall SetISList( TStrings* AValue );
        TStrings* __fastcall GetISList();
        TTreeNodes* __fastcall GetListCols();
        TTagNode* __fastcall GetMainIS();
        void __fastcall SetMainIS( const TTagNode *AValue );
        void __fastcall ClearCols();
        TTreeNode* __fastcall AddColTreeNode( TTreeNode* ColParentNode, UnicodeString  Text, TTagNode* Object );
        TTagNode* __fastcall AddListColTagNode( TTagNode* ndColParent, UnicodeString  AName );
        TTreeNode* __fastcall AddCol( TTreeNode* ObjNode, TTreeNode* ColParentNode );
        void __fastcall AddCols( TTreeNode* ColsParentNode );


public:		// User declarations
        __fastcall TfmAddList(TComponent* Owner, bool ACanAddGroup, TAddListMode AAddListMode = alNewList );
        virtual __fastcall ~TfmAddList();

        //����� ������ �����
        __property TAddListMode AddListMode = { read = FAddListMode };

        //������ �������������� ��������� ������������, ��� ������� ��������
        //���������� ������ ( ��� "list" )
        __property TStrings*    ISList      = { read = GetISList,  write = SetISList  };

        //������ �������� ������ ( ��� "list" ) ��� ����������
        __property TTreeNodes*  ListCols    = { read = GetListCols };

        //�������� �������������� �������� ������������ ( ��� �� �������� ������
        // ( ��� "list" ) )
        __property TTagNode*    MainIS      = { read = GetMainIS, write = SetMainIS };

        //������� "��� �������� ������ �������� ��� ������",
        //��������� ��� ��������� �������� �������� MainIS
        __property TOnObjListLoadEvent OnObjListLoad = { read = FOnObjListLoad, write = FOnObjListLoad };
};

//---------------------------------------------------------------------------

} //end of namespace AddList
using namespace AddList;

#endif
