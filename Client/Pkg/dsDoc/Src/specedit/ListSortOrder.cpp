/*
  ����        - ListSortOrder.cpp
  ������      - ��������������� (ICS)
  ��������    - ���� ���������� ������, ����������� �������� �����
                ��� �������� ������� ���������� ��� ������
  ����������� - ������� �.�.
  ����        - 12.10.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "ListSortOrder.h"
#include "DKUtils.h"
#include "ICSDocBaseEdit.h"
#include "ICSDocGlobals.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"

//***************************************************************************
//********************** ���� ***********************************************
//***************************************************************************

//��� ����������
enum TSortType
{
  stAsc = 1,    //�� �����������
  stDesc = 2    //�� ��������
};

//***************************************************************************
//********************** ��������/��������/����� ����� **********************
//***************************************************************************

__fastcall TfmListSortOrder::TfmListSortOrder(TComponent* Owner, const TTagNode* AListTagNode)
        : TForm(Owner)
{
  FOnColListLoad = NULL;
  FOnListColUpdate = NULL;
  FSortOrderTagNode = NULL;
  FListTagNode = new TTagNode;
  FListTagNode->Assign( const_cast<TTagNode*>(AListTagNode), true );
  __NeedUpdateSortByList = false;

  Caption               = FMT(icsDocListSortOrderCaption);
  ColLab->Caption       = FMT(icsDocListSortOrderColLabCaption);
  SortOrderLab->Caption = FMT(icsDocListSortOrderSortOrderLabCaption);
  bAddCols->Caption     = FMT(icsDocListSortOrderAddColsCaption);
  bAddCols->Hint        = FMT(icsDocListSortOrderAddColsHint);
  bDelete->Caption      = FMT(icsDocListSortOrderDeleteCaption);
  bDelete->Hint         = FMT(icsDocListSortOrderDeleteHint);
  bClear->Caption       = FMT(icsDocListSortOrderClearCaption);
  bClear->Hint          = FMT(icsDocListSortOrderClearHint);
  bMoveUp->Caption      = FMT(icsDocListSortOrderMoveUpCaption);
  bMoveUp->Hint         = FMT(icsDocListSortOrderMoveUpHint);
  bMoveDown->Caption    = FMT(icsDocListSortOrderMoveDownCaption);
  bMoveDown->Hint       = FMT(icsDocListSortOrderMoveDownHint);
  AscSortTBItem->Caption  = FMT(icsDocListSortOrderAscSortCaption);
  AscSortTBItem->Hint     = FMT(icsDocListSortOrderAscSortHint);
  DescSortTBItem->Caption = FMT(icsDocListSortOrderDescSortCaption);
  DescSortTBItem->Hint    = FMT(icsDocListSortOrderDescSortHint);
  bOk->Caption            = FMT(icsDocListSortOrderOkBtnCaption);
  bCancel->Caption        = FMT(icsDocListSortOrderCancelBtnCaption);
  bHelp->Caption          = FMT(icsDocListSortOrderHelpBtnCaption);

}

//---------------------------------------------------------------------------

__fastcall TfmListSortOrder::~TfmListSortOrder()
{
  delete FListTagNode;
  __DELETE_OBJ( FSortOrderTagNode )
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif

  bDelete->Enabled   = false;
  bClear->Enabled    = false;
  bMoveUp->Enabled   = false;
  bMoveDown->Enabled = false;
  AscSortTBItem->Enabled  = false;
  DescSortTBItem->Enabled = false;
  if ( FOnColListLoad )
  {
    tvCols->Items->BeginUpdate();
    if ( FOnColListLoad( this, FListTagNode, tvCols->Items ) )
    {
      tvCols->Selected = tvCols->Items->GetFirstNode();
      tvCols->Selected->MakeVisible();
      tvCols->Selected->Expand( true );
    }
    tvCols->Items->EndUpdate();
  }
  if ( __NeedUpdateSortByList )
    UpdateSortByList( false );
}

//***************************************************************************
//********************** ������ ����� ***************************************
//***************************************************************************

void __fastcall TfmListSortOrder::SetSortOrderTagNode( const TTagNode* AValue )
{
  __DELETE_OBJ( FSortOrderTagNode )
  if ( AValue )
  {
    FSortOrderTagNode = new TTagNode;
    FSortOrderTagNode->Assign( const_cast<TTagNode*>(AValue), true );
  }
  if ( Showing )
    UpdateSortByList();
  else
    __NeedUpdateSortByList = true;
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::UpdateSortByList( bool bReloadColList )
{
  tvSortBy->Items->Clear();
  TTagNode *ndList = NULL;
  if ( TTreeNode *TreeNode = tvCols->Items->GetFirstNode() )
    ndList = reinterpret_cast<TTagNode*>(TreeNode->Data);

  tvCols->Items->BeginUpdate();
  if ( bReloadColList && FOnColListLoad && FOnColListLoad( this, FListTagNode, tvCols->Items ) )
  {
    tvCols->Selected = tvCols->Items->GetFirstNode();
    tvCols->Selected->MakeVisible();
    tvCols->Selected->Expand( true );
  }

  tvSortBy->Items->BeginUpdate();
  if ( FSortOrderTagNode )
  {
    TTagNode *ndSortEl = FSortOrderTagNode->GetFirstChild();
    while( ndSortEl )
    {
      TTagNode  *ndRef = ( ndList ) ? ndList->GetTagByUID( ndSortEl->AV["ref"] ) : NULL;
      TTreeNode* TreeNode = tvSortBy->Items->AddObject( NULL,
                                                        ( ndRef ) ? ndRef->AV["name"] : static_cast<UnicodeString>("????"),
                                                        reinterpret_cast<void*>(ndSortEl)
      );
      TreeNode->StateIndex = ( ndSortEl->CmpAV( "sorttype", "incr" ) ) ? stAsc : stDesc;
      if ( ndRef )
        reinterpret_cast<TTreeNode*>(ndRef->Ctrl)->Delete();
      ndSortEl = ndSortEl->GetNext();
    }
  }
  tvCols->Items->EndUpdate();
  if ( tvSortBy->Items->GetFirstNode() )
  {
    tvSortBy->Selected = tvSortBy->Items->GetFirstNode();
    tvSortBy->Selected->MakeVisible();
    bClear->Enabled = true;
  }
  else
  {
    bDelete->Enabled   = false;
    bClear->Enabled    = false;
    bMoveUp->Enabled   = false;
    bMoveDown->Enabled = false;
  }
  tvSortBy->Items->EndUpdate();
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::InvertSortType( TTreeNode *ATreeNode )
{
  if ( ATreeNode )
  {
    TTagNode* ndNode = reinterpret_cast<TTagNode*>(ATreeNode->Data);
    if ( ndNode->CmpAV( "sorttype", "incr" ) )
    {
      ndNode->AV["sorttype"] = "decr";
      ATreeNode->StateIndex = stDesc;
    }
    else
    {
      ndNode->AV["sorttype"] = "incr";
      ATreeNode->StateIndex = stAsc;
    }
  }
}

//***************************************************************************
//********************** ����������� �������*********************************
//***************************************************************************

void __fastcall TfmListSortOrder::bOkClick(TObject *Sender)
{
  if ( FSortOrderTagNode && !FSortOrderTagNode->Count )
    __DELETE_OBJ( FSortOrderTagNode );

  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::tvColsCustomDrawItem(
      TCustomTreeView *Sender, TTreeNode *Node, TCustomDrawState State,
      bool &DefaultDraw)
{
  TTagNode *TagNode = reinterpret_cast<TTagNode*>(Node->Data);
  if ( TagNode && ( TagNode->Name != "listcol" ) )
    Sender->Canvas->Font->Color = clGrayText;
  DefaultDraw = true;
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::tvColsChange(TObject *Sender,
      TTreeNode *Node)
{
  bool eAddCols = false;
  for ( unsigned i = 0; i < tvCols->SelectionCount; i++ )
    if ( reinterpret_cast<TTagNode*>(tvCols->Selections[i]->Data)->CmpName("listcol") )
    {
      eAddCols = true;
      break;
    }
  bAddCols->Enabled  = eAddCols;
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::bAddColsClick(TObject *Sender)
{
  TTreeNodeList TreeNodesToDelete;
  TTreeNode *LastAddedNode = NULL;
  tvSortBy->Items->BeginUpdate();
  for ( unsigned i = 0; i < tvCols->SelectionCount; i++ )
  {
    TTagNode *ndListCol = reinterpret_cast<TTagNode*>(tvCols->Selections[i]->Data);
    if ( ndListCol->CmpName("listcol") )
    {
      if ( !FSortOrderTagNode )
      {
        FSortOrderTagNode = new TTagNode;
        FSortOrderTagNode->Name = "sortorder";
      }
      TTagNode *ndSortEl = FSortOrderTagNode->AddChild("sortel");
      ndSortEl->AV["ref"]         = ndListCol->AV["uid"];
      ndSortEl->AV["sorttype"]    = "incr";
      ndSortEl->AV["numberlevel"] = "";
      ndSortEl->AV["numbertype"]  = "no";
      ndSortEl->AV["doublevalue"] = "yes";
      LastAddedNode = tvSortBy->Items->AddObject( NULL,
                                                  ndListCol->AV["name"],
                                                  reinterpret_cast<void*>(ndSortEl)
      );
      LastAddedNode->StateIndex = stAsc;
      TreeNodesToDelete.push_back( reinterpret_cast<TTreeNode*>(ndListCol->Ctrl) );
    }
  }
  tvCols->Items->BeginUpdate();
  for ( TTreeNodeList::iterator i = TreeNodesToDelete.begin(); i != TreeNodesToDelete.end(); i++ )
  {
    TICSDocBaseEdit::UpdateTagNodeCtrl( *i, true );
    (*i)->Delete();
  }
  tvCols->Items->EndUpdate();
  if ( LastAddedNode )
  {
    tvSortBy->Selected = LastAddedNode;
    tvSortBy->Selected->MakeVisible();
    bClear->Enabled = true;
    bDelete->Enabled = true;
  }
  tvSortBy->Items->EndUpdate();
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::tvSortByDblClick(TObject *Sender)
{
  TPoint CursorPos = tvSortBy->ScreenToClient( Mouse->CursorPos );
  if ( TTreeNode *ClickedNode = tvSortBy->GetNodeAt( CursorPos.x, CursorPos.y ) )
    InvertSortType( ClickedNode );
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::tvSortByChange(TObject *Sender, TTreeNode *Node)
{
  bMoveUp->Enabled   = Node->getPrevSibling();
  bMoveDown->Enabled = Node->getNextSibling();
  bool bAscSorted = reinterpret_cast<TTagNode*>( Node->Data )->CmpAV( "sorttype", "incr" );
  AscSortTBItem->Enabled  = !bAscSorted;
  DescSortTBItem->Enabled = bAscSorted;
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::bMoveUpClick(TObject *Sender)
{
  TICSDocBaseEdit::MoveTreeNodeUp( tvSortBy->Selected, true );
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::bMoveDownClick(TObject *Sender)
{
  TICSDocBaseEdit::MoveTreeNodeDown( tvSortBy->Selected, true );
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::bClearClick(TObject *Sender)
{
  __DELETE_OBJ(FSortOrderTagNode)
  UpdateSortByList();
  bDelete->Enabled   = false;
  bClear->Enabled    = false;
  bMoveUp->Enabled   = false;
  bMoveDown->Enabled = false;
  AscSortTBItem->Enabled  = false;
  DescSortTBItem->Enabled = false;
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::bDeleteClick(TObject *Sender)
{
  TTreeNodeList TreeNodesToDelete;
  for ( unsigned i = 0; i < tvSortBy->SelectionCount; i++ )
    TreeNodesToDelete.push_back( tvSortBy->Selections[i] );
  tvSortBy->Items->BeginUpdate();
  tvCols->Items->BeginUpdate();
  for ( TTreeNodeList::iterator i = TreeNodesToDelete.begin(); i != TreeNodesToDelete.end(); i++ )
  {
    TTreeNode *TreeNode = *i;
    TTagNode  *TagNode  = reinterpret_cast<TTagNode*>(TreeNode->Data);
    if ( FOnListColUpdate )
    {
      TTagNode *ndListCol = FListTagNode->GetTagByUID( TagNode->AV["ref"] );
      TTreeNode *ListColNode = ( ndListCol->GetNext() && ndListCol->GetNext()->Ctrl )
                               ? tvCols->Items->InsertObject( reinterpret_cast<TTreeNode*>(ndListCol->GetNext()->Ctrl),
                                                              "",
                                                              NULL
                                 )
                               : tvCols->Items->AddChildObject( reinterpret_cast<TTreeNode*>(ndListCol->GetParent()->Ctrl),
                                                                "",
                                                                NULL
                                 );
      FOnListColUpdate( this, ndListCol, ListColNode );
      tvCols->Selected = ListColNode;
      ListColNode->MakeVisible();
      ListColNode->Expand( true );
    }
    TreeNode->Delete();
    delete TagNode;
  }
  tvCols->Items->EndUpdate();
  tvSortBy->Items->EndUpdate();
  if ( !tvSortBy->Items->Count )
  {
    bDelete->Enabled   = false;
    bClear->Enabled    = false;
    bMoveUp->Enabled   = false;
    bMoveDown->Enabled = false;
    AscSortTBItem->Enabled  = false;
    DescSortTBItem->Enabled = false;
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::tvSortByContextPopup(TObject *Sender,
      TPoint &MousePos, bool &Handled)
{
  //������� ������������� ��� ������� ������ ������ ����
  if ( !( ( MousePos.x == -1 ) && ( MousePos.y == -1 ) ) )
    if ( tvSortBy->Selected )
    {
      tvSortBy->Selected->Selected = true;
      tvSortBy->Selected->Focused  = true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::AscSortTBItemClick(TObject *Sender)
{
  if ( tvSortBy->Selected )
  {
    TTagNode* ndCurSel = reinterpret_cast<TTagNode*>(tvSortBy->Selected->Data);
    ndCurSel->AV["sorttype"] = "incr";
    tvSortBy->Selected->StateIndex = stAsc;
    AscSortTBItem->Enabled = false;
    DescSortTBItem->Enabled = true;
  }
}
//---------------------------------------------------------------------------

void __fastcall TfmListSortOrder::DescSortTBItemClick(TObject *Sender)
{
  if ( tvSortBy->Selected )
  {
    TTagNode* ndCurSel = reinterpret_cast<TTagNode*>(tvSortBy->Selected->Data);
    ndCurSel->AV["sorttype"] = "decr";
    tvSortBy->Selected->StateIndex = stDesc;
    AscSortTBItem->Enabled = true;
    DescSortTBItem->Enabled = false;
  }
}
//---------------------------------------------------------------------------

//Ins - �������� ������� ��� ����������

void __fastcall TfmListSortOrder::tvColsKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if ( Key == VK_INSERT )
    if ( bAddCols->Enabled )
      bAddCols->Click();
}

//---------------------------------------------------------------------------

//Del            - ������� ������� ������� ��� ����������
//Ctrl + Del     - �������� ������ �������� ��� ����������
//Ctrl + VK_UP   - ����������� ������� ������� ��� ���������� �����
//Ctrl + VK_DOWN - ����������� ������� ������� ��� ���������� ����
//Shift+Enter    - �������� ������� ����������

void __fastcall TfmListSortOrder::tvSortByKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
  if ( Key == VK_DELETE )
  {
    if ( Shift.Contains( ssCtrl ) )
    {
      if ( bClear->Enabled )
        bClear->Click();
    }
    else
      if ( bDelete->Enabled )
        bDelete->Click();
  }
  else if ( ( Key == VK_UP   ) && ( Shift.Contains( ssCtrl ) ) )
  {
    if ( bMoveUp->Enabled )
      bMoveUp->Click();
  }
  else if ( ( Key == VK_DOWN ) && ( Shift.Contains( ssCtrl ) ) )
  {
    if ( bMoveDown->Enabled )
      bMoveDown->Click();
  }
  else if ( ( Key == VK_RETURN ) && ( Shift.Contains( ssShift ) ) )
  {
    if ( tvSortBy->Selected )
      InvertSortType( tvSortBy->Selected );
  }
}
//---------------------------------------------------------------------------

