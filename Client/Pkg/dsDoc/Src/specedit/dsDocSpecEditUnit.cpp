//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsDocSpecEditUnit.h"
#include "msgdef.h"
#include "dsDocClientConstDef.h"
#include "dsDocExtFunc.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma resource "*.dfm"
TdsDocSpecEditForm *dsDocSpecEditForm;
//---------------------------------------------------------------------------
__fastcall TdsDocSpecEditForm::TdsDocSpecEditForm(TComponent* Owner, TTagNode *ASpec, TAxeXMLContainer *AXMList, UnicodeString AAutorCode, UnicodeString APeriodDef,bool AIsEdit,TdsDocDM *ADM, bool ADoc)
        : TForm(Owner)
{
  FDM = ADM;
  XMList = AXMList;
  FSpec = ASpec;
  IsEdit = AIsEdit;
  SpecEditor = new TICSDocSpec(this);
  SpecEditor->Parent = this;
  SpecEditor->Align = alClient;

  SpecEditor->Color = clBtnFace;
  SpecEditor->TabOrder = 0;
  SpecEditor->PropsPanel->Width = 235;
  SpecEditor->PropsPanel->Height = 492;
  SpecEditor->PropsPanel->Align = alRight;
  SpecEditor->SpecPanel->Width = 513;
  SpecEditor->SpecPanel->Height = 492;
  SpecEditor->SpecPanel->Align = alClient;
  SpecEditor->Splitter->Width = 5;
  SpecEditor->Splitter->Height = 492;
  SpecEditor->Splitter->Align = alRight;
  SpecEditor->Title = "�������� ���������";
  SpecEditor->OnLoadFilter           = SpecEditorLoadFilter;
  SpecEditor->OnGetSpecOwner         = SpecEditorGetSpecOwner;
  SpecEditor->OnGetSpecificatorsList = SpecEditorGetSpecificatorsList;
  SpecEditor->OnGetOrgsList          = SpecEditorGetOrgsList;
  SpecEditor->OnGetSpecNode          = SpecEditorGetSpecNode;

  if (FSpec->GetChildByName("passport"))
   SpecEditor->BaseISUID = FSpec->GetChildByName("passport")->AV["baseisuid"];

  SpecEditor->ReqEdBkColor = FDM->ReqColor;
  SpecEditor->DomainNumber = 0;
  SpecEditor->XMLContainer = FDM->XMLList;
  TTagNode *tmp = NULL;
  if (ADoc)
   {
     SpecEditor->OnCallDefProc = FDM->OnCallDocDefProc;
     SpecEditor->Nom = FDM->DocNom;
     tmp = FDM->ICS_DATA_DEF->GetChildByAV("item","xmltype","nomenklatordoc",true);
   }
  else
   {
     SpecEditor->OnCallDefProc = FDM->OnCallDefProc;
     SpecEditor->Nom = FDM->Nom;
     tmp = FDM->ICS_DATA_DEF->GetChildByAV("item","xmltype","nomenklator",true);
   }
  if (tmp)
   SpecEditor->NmklGUI = tmp->AV["xmlref"];
  SpecEditor->OnDemoMessage = FDM->DocViewDemoMessage;
  if (ASpec->Count)
   {
     Caption = FMT1(dsDocDocSpecEditEditCaption,GetSpecName(ASpec));
     SpecEditor->Set(ASpec);
   }
  else
   {
     TICSDocSpecNewResult RC = SpecEditor->New(AAutorCode, APeriodDef);
     if (RC == nrParametersCanceled) {ModalResult = mrCancel; }
     ASpec->Assign(SpecEditor->Get(),true);
     Caption = FMT1(dsDocDocSpecEditNewCaption,GetSpecName(ASpec));
   }
  SaveBtn->Caption = FMT(dsDocDocSpecEditSaveBtnCaption);
  CancelBtn->Caption = FMT(dsDocDocSpecEditCancelBtnCaption);
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecEditForm::SaveBtnClick(TObject *Sender)
{
   if (SpecEditor->IsEmpty()) {_MSG_INF(FMT(dsDocDocSpecEditSaveMsg),FMT(dsDocDocSpecEditSaveCaption));return;}
   FSpec->Assign(SpecEditor->Get(),true);
//   if (FCanClose)
//    {
//     if (FCanClose(this, ""))
//      ModalResult = mrOk;
//    }
//   else
    ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecEditForm::CancelBtnClick(TObject *Sender)
{
   if (SpecEditor->Modified)
    {
      int RC = _MSG_QUE(FMT(dsDocDocSpecEditCancelWarn),FMT(dsDocCommonMsgCaption));
      if (RC == ID_YES)
       SaveBtnClick(SaveBtn);
      else
       ModalResult = mrCancel;
    }
   else
    ModalResult = mrCancel;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecEditForm::SpecEditorGetOrgsList(TObject *Sender,
      TUnicodeStringMap &SpecsList)
{
   TStringList *DDD = new TStringList;
   FDM->GetChildSpecOwnerList(DDD,true);
   for (int i = 0; i < DDD->Count; i++)
    SpecsList[GetPart1(DDD->Strings[i],'=')] = GetPart2(DDD->Strings[i],'=');
   delete DDD;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecEditForm::SpecEditorGetSpecificatorsList(
      TObject *Sender, TUnicodeStringMap &SpecsList)
{
   TStringList *DDD = new TStringList;
   FDM->GetIntegredSpecList(DDD);
   for (int i = 0; i < DDD->Count; i++)
    SpecsList[GetPart1(DDD->Strings[i],'=')] = GetPart2(DDD->Strings[i],'=');
   delete DDD;
}
//---------------------------------------------------------------------------
bool __fastcall TdsDocSpecEditForm::SpecEditorGetSpecNode(TObject *Sender,
      const UnicodeString SpecGUI, TTagNode *&SpecTagNode)
{
  if (SpecTagNode)
   {
     delete SpecTagNode;
     SpecTagNode = NULL;
   }
  if ( SpecGUI == "" ) return false;
  else
   {
     SpecTagNode = new TTagNode(NULL);
     if (!FDM->GetSpecByGUI(SpecGUI,SpecTagNode)) return false;
     return true;
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecEditForm::SpecEditorGetSpecOwner(TObject *Sender,
      UnicodeString AAutorCode, UnicodeString &AAutorName)
{
//   AAutorCode = "000.0000000.000";
   AAutorName = FDM->GetOwnerName(AAutorCode);
}
//---------------------------------------------------------------------------
bool __fastcall TdsDocSpecEditForm::SpecEditorLoadFilter(
      UnicodeString ACondName, TTagNode *Flt)
{
  _MSG_DBG("������");
}
//---------------------------------------------------------------------------

void __fastcall TdsDocSpecEditForm::FormDestroy(TObject *Sender)
{
  SpecEditor->Parent = NULL;
  delete SpecEditor;
}
//---------------------------------------------------------------------------

