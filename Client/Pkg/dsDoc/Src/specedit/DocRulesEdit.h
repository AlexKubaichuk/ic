/*
  ����        - DocRulesEdit.h
  ������      - ��������������� (ICS)
  ��������    - �������� ������ ������������ ��������� �� ���������.
                ������������ ����
                (������������ ����������� TICSDocSpec)
  ����������� - ������� �.�.
  ����        - 15.11.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  16.03.2006
    [-] ����������� ��������� uid
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  27.01.2006
    [+] ��������� �������� TICSDocSpec::ReqEdBkColor
    [*] ��� ���� ����� �������� ��� "������������"
    [*] �������� ������������ ��������� ����������

  23.05.2005
    1. ��������� shortcut'� ��� ��������
    2. ������� "���" ��� ������������ ����������� �������� �� "������������"
    3. Gray'���� �������, ��������������� control'� ��� ������� ��gray'���

  21.02.2005
    1. �������� �� �������� ���������� � ��������� ImageList

  02.12.2004
    1. ������� OnGetSpecPath ���������� � OnGetSpecNode

  15.11.2004
    �������������� �������

*/
//---------------------------------------------------------------------------

#ifndef DocRulesEditH
#define DocRulesEditH

//---------------------------------------------------------------------------

#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtonEdit.hpp"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "DKListBox.h"
#include "dxBar.hpp"
//---------------------------------------------------------------------------
//#include "ICSDoc.h"  //��������� �������� � ���� ��������
#include "XMLContainer.h"
#include <map>
#include "ObjRulesEdit.h"
//---------------------------------------------------------------------------

using namespace std;

namespace DocRulesEdit
{

//###########################################################################
//##                                                                       ##
//##                                 Globals                               ##
//##                                                                       ##
//###########################################################################

typedef map<UnicodeString,UnicodeString> TUnicodeStringMap;

/*
 * ��� ����������� ������� OnGetSpecificatorsList - ���������, ����� ��������� �������� ������
 * ��������� ��������������, ����������� ������������� ���������, �.�. ���������, �� �������
 * ����� ������������� ������ ��������. ������������ ����������� TICSDocSpec � ������� ���������
 * ������ ������������ ��������� �� ��������� ��� �������� ���������� ����� ������������
 *
 *
 * ���������:
 *   [out] SpecsList - ������� �������������� ( ����   - GUI �������������,
 *                                              ������ - ������������ ������������� )
 */
typedef void __fastcall (__closure *TOnGetSpecificatorsListEvent)( TObject *Sender, TUnicodeStringMap &SpecsList );

/*
 * ��� ����������� ������� OnGetOrgsList - ���������, ����� ��������� �������� ������
 * ��������� �����������. ������������ ����������� TICSDocSpec � ������� ���������
 * ������ ������������ ��������� �� ��������� ��� �������� ���������� ����� ������������
 *
 *
 * ���������:
 *   [out] OrgsList - ������� ����������� ( ����   - ��� �����������,
 *                                          ������ - ������������ ����������� )
 */
typedef void __fastcall (__closure *TOnGetOrgsListEvent)( TObject *Sender, TUnicodeStringMap &OrgsList );

/*
 * ��� ����������� ������� OnGetSpecNode - ���������, ����� ��������� xml-������
 * ������������� � ��������� GUI. ������������ ����������� TICSDocSpec � ������� ���������
 * ������ ������������ ��������� �� ��������� ��� ���������� ������ ���������� ��� ��� ���������
 * ������������
 *
 *
 * ���������:
 *   [in]     SpecGUI      - GUI �������������
 *   [in\out] SpecTagNode  - ������ xml-������ �������������
 *
 * ������������ ��������:
 *   true  - ��������� ������ � xml-������� �������������
 *   false - ��������� ������ � xml-������� �������������, ��� ���� ���� SpecGUI != ""
 *           �������� ��������� "� ������� ��������."
 *
 * ����������:
 *   ������������ ������� ������� ���:
     if ( SpecTagNode )
     {
       delete SpecTagNode;
       SpecTagNode = NULL;
     }

     if ( SpecGUI == "" )
       return false;
     else
     {
       SpecTagNode = new TTagNode;
       < �������� xml-������ ������������� � GUI <SpecGUI> >
       return true;
     }
 *
 */
typedef bool __fastcall (__closure *TOnGetSpecNodeEvent)( TObject *Sender, const UnicodeString SpecGUI, TTagNode* &SpecTagNode );

//###########################################################################
//##                                                                       ##
//##                              TfmDocRulesEdit                          ##
//##                                                                       ##
//###########################################################################

// ����� ��� �������������� ������ ������������ ��������� �� ���������

class PACKAGE TfmDocRulesEdit : public TForm
{
__published:	// IDE-managed Components
    TPanel *pBottom;
    TPanel *pCtrls;
    TButton *bOk;
    TButton *bCancel;
    TButton *bHelp;
    TPanel *pStepProp;
    TSplitter *Splitter1;
    TPanel *pPropHead;
    TPanel *pProps;
    TPanel *pObjHead;
    TGroupBox *gbDoc;
        TLabel *DefLab;
        TLabel *PerTypeLab;
    TLabel *PeriodValueTitleLb;
    TGroupBox *gbOrg;
        TLabel *OrgTypeLab;
    TLabel *lbOrgCodeTitle;
    TButton *bPropHide;
    TPanel *pObjs;
    TButton *bObjHide;
    TTreeView *tvObj;
    TPanel *pSpeps;
    TDKListBox *lbxSteps;
    TPanel *pStepsHead;
    TActionList *ActionList;
  TImageList *ImageList11;
    TAction *actAddStep;
    TAction *actMoveStepUp;
    TAction *actMoveStepDown;
    TAction *actDelete;
    TAction *actAddObjRules;
    TAction *actSetAssoc;
    TAction *actEditProp;
        TLabel *PeriodisityLab;
  TdxBarManager *MainBM;
  TdxBar *dxBarManager1Bar1;
  TdxBarButton *dxBarButton1;
  TdxBarButton *dxBarButton2;
  TdxBarButton *dxBarButton3;
  TdxBarButton *dxBarButton4;
  TdxBarButton *dxBarButton5;
  TdxBarButton *dxBarButton6;
  TdxBarButton *dxBarButton7;
  TcxImageList *ImageList;
  TPopupMenu *StepsPM;
  TPopupMenu *ObjsPM;
  TMenuItem *N1;
  TMenuItem *N2;
  TMenuItem *N3;
  TMenuItem *N4;
  TMenuItem *N5;
  TMenuItem *N6;
  TMenuItem *N7;
  TcxComboBox *cbPeriodType;
  TcxComboBox *PeriodicityCBx;
  TcxComboBox *cbOrgType;
  TcxComboBox *cbOrgCode;
  TcxButtonEdit *PeriodValueEd;
 TdxBarLargeButton *dxBarLargeButton1;
 TdxBarLargeButton *dxBarLargeButton2;
 TdxBarLargeButton *dxBarLargeButton3;
 TdxBarLargeButton *dxBarLargeButton4;
 TdxBarLargeButton *dxBarLargeButton5;
 TdxBarLargeButton *dxBarLargeButton6;
 TdxBarLargeButton *dxBarLargeButton7;
 TcxComboBox *cbSpec;
    void __fastcall bCancelClick(TObject *Sender);
    void __fastcall bOkClick(TObject *Sender);
    void __fastcall bHelpClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall bPropHideClick(TObject *Sender);
    void __fastcall bObjHideClick(TObject *Sender);
    void __fastcall actAddStepExecute(TObject *Sender);
    void __fastcall actMoveStepUpExecute(TObject *Sender);
    void __fastcall actMoveStepDownExecute(TObject *Sender);
    void __fastcall actDeleteExecute(TObject *Sender);
    void __fastcall actAddObjRulesExecute(TObject *Sender);
    void __fastcall ListsEnter(TObject *Sender);
    void __fastcall tvObjContextPopup(TObject *Sender,
      TPoint &MousePos, bool &Handled);
    void __fastcall lbxStepsSelect(TObject *Sender, int ListItemIndex);
    void __fastcall actEditPropExecute(TObject *Sender);
    void __fastcall actSetAssocExecute(TObject *Sender);
    void __fastcall tvObjChange(TObject *Sender, TTreeNode *Node);
    void __fastcall lbxStepsKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift);
    void __fastcall tvObjKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift);
  void __fastcall cbSpecPropertiesChange(TObject *Sender);
  void __fastcall cbPeriodTypePropertiesChange(TObject *Sender);
  void __fastcall PeriodValueEdPropertiesButtonClick(TObject *Sender, int AButtonIndex);
  void __fastcall PeriodicityCBxPropertiesChange(TObject *Sender);
  void __fastcall cbOrgTypePropertiesChange(TObject *Sender);
  void __fastcall cbOrgCodePropertiesChange(TObject *Sender);




private:	// User declarations
    /*����*/
    typedef map<int,UnicodeString> TClassMap;

    /*����*/
    TTagNode* FDocRules;            //������� ������������ ��������� �� ���������
                                    //( xml-��� "docrules" ������������� )
    TClassMap FSpecs;               //������� �������������� ( ���� - ���������� ���,
                                    //                         ������ - GUI ������������� )
    TClassMap FOrgs;                //������� ����������� ( ���� - ���������� ���,
                                    //                      ������ - ��� ����������� )
    bool FStepPropertiesEnabled;    //������� ����������� control'�� - ���������� ����������
                                    //����� ������������
    bool FSettingPropertiesCtrls;   //������� ���������� ��������� �������������
                                    //control'�� - ���������� ���������� ����� ������������
    TTagNode* FSpec;                //������������
    TTagNode *FIntegrSpec;          //������������ ������������� ����������

    TTagNode* FObjRules;            //������� ���������� ( ��� ������ ������� ����� ������������ �������� )

    /*������� (�������� ��. � ������ public)*/
    TOnGetDocElementsListEvent    FOnGetDocElementsList;
    TOnGetDocElementStructEvent   FOnGetThisDocElementStruct;
    TOnGetDocElementStructEvent   FOnGetRefDocElementStruct;
    TOnGetSpecificatorsListEvent  FOnGetSpecsList;
    TOnGetOrgsListEvent           FOnGetOrgsList;
    TOnGetSpecNodeEvent           FOnGetSpecNode;
    TOnGetSpecMainNameEvent       FOnGetSpecMainName;
    TOnGetDocElementViewTextEvent FOnGetDocElementViewText;
    TOnGetDocElementStructEvent   FOnGetDocElementStruct;

    void __fastcall SetStepPropertiesControlsEnabled( bool bDoInit, bool bEnabled );


protected:
    const UnicodeString csCreateStepViewName;
    const UnicodeString csUndefPeriodVal;

    TColor __fastcall GetReqEdBkColor() const;
    void __fastcall HidePropsPanel();
    void __fastcall ShowPropsPanel();
    void __fastcall TogglePropsPanel();
    void __fastcall HideObjsPanel();
    void __fastcall ShowObjsPanel();
    void __fastcall ToggleObjsPanel();
    void __fastcall InitStepPropertiesControls();
    void __fastcall EnableStepPropertiesControls( bool bDoInit = false );
    void __fastcall DisableStepPropertiesControls( bool bDoInit = true );
    void __fastcall UpdateActionsEnabled();
    void __fastcall LoadIntegrSpec();
    void __fastcall UpdateDocElementIntegrInfo( TTreeNode *ANode );
    void __fastcall LoadCreateSteps();
    UnicodeString __fastcall GetPeriodValViewText(TTagNode *ADocRulesStep);
    void __fastcall SetMapComboText( UnicodeString AttrName,
                                     const TClassMap &AMap,
                                     TcxComboBox *AComboBox );
    void __fastcall AppendObjRules( TTagNode *AObj, TTagNode *AObjRules, TTreeNodes *ATreeNodes );
    void __fastcall AppendObjRulesView( TTagNode *AObj, TTagNode *AObjRules, TTreeNodes *ATreeNodes );
    bool __fastcall DocRulesEditCheckAssociationExistence( TObject *Sender, const TTagNode *ndObjRules, const UnicodeString ThisRef, UnicodeString &Msg );
    bool __fastcall AreInputsCorrect();


public:		// User declarations
    __fastcall TfmDocRulesEdit(TComponent* Owner, TTagNode* ADocRules, TTagNode* ASpec );
    virtual __fastcall ~TfmDocRulesEdit();

    /*��������*/
    //������� ������������ ��������� �� ��������� ( xml-��� "docrules" ������������� )
    __property TTagNode* DocRules = { read = FDocRules };

    /*�������*/
    //��������� ��� ������������� �������� ������ ��������� �������������� �������������
    __property TOnGetDocElementsListEvent    OnGetDocElementsList      = { read = FOnGetDocElementsList,      write = FOnGetDocElementsList  };
    //��������� ��� ������������� �������� ��������� ��������� �������������� �������������
    __property TOnGetDocElementStructEvent   OnGetThisDocElementStruct = { read = FOnGetThisDocElementStruct, write = FOnGetThisDocElementStruct };
    //��������� ��� ������������� �������� ��������� ��������� ������������� ������������� ����������
    __property TOnGetDocElementStructEvent   OnGetRefDocElementStruct  = { read = FOnGetRefDocElementStruct,  write = FOnGetRefDocElementStruct };
    //��������� ��� ������������� �������� ������ ��������������
    __property TOnGetSpecificatorsListEvent  OnGetSpecificatorsList    = { read = FOnGetSpecsList,            write = FOnGetSpecsList };
    //��������� ��� ������������� �������� ������ �����������
    __property TOnGetOrgsListEvent           OnGetOrgsList             = { read = FOnGetOrgsList,             write = FOnGetOrgsList  };
    //��������� ��� ������������� �������� xml-������ �������������
    __property TOnGetSpecNodeEvent           OnGetSpecNode             = { read = FOnGetSpecNode,             write = FOnGetSpecNode  };
    //��������� ��� ������������� �������� ������������ ������������� ( ������� "mainname" �������� )
    __property TOnGetSpecMainNameEvent       OnGetSpecMainName         = { read = FOnGetSpecMainName,         write = FOnGetSpecMainName };
    //��������� ��� ������������� �������� ������������ ����� ��� �������� �������������
    __property TOnGetDocElementViewTextEvent OnGetDocElementViewText   = { read = FOnGetDocElementViewText,   write = FOnGetDocElementViewText };
    //��������� ��� ������������� �������� ��������� �������� �������������
    __property TOnGetDocElementStructEvent   OnGetDocElementStruct     = { read = FOnGetDocElementStruct,     write = FOnGetDocElementStruct };
};

//---------------------------------------------------------------------------

} //end of namespace DocRulesEdit
using namespace DocRulesEdit;

#endif
