/*
 ����        - ICSDocViewer.cpp
 ������      - ��������������� (ICS)
 ��������    - ����������� ���������.
 ���� ����������
 ����������� - ������� �.�.
 ����        - 18.10.2004
 */
// ---------------------------------------------------------------------------
#include <basepch.h>
#pragma hdrstop
#include "icsDocSpecConstDef.h"
#include "ICSDocViewer.h"
#include <math.h>
#include "DKFileUtils.h"
//#include "DKMBUtils.h"
#include "msgdef.h"
#include "DKRusMB.h"
#include "DKAxeUtils.h"
// #include "ICSWord.h"
// #include "HTMLPreviewForm.h"
#include "ICSDocSpec.h"
#pragma package(smart_init)
// ###########################################################################
// ##                                                                       ##
// ##                              TICSDocIESetup                           ##
// ##                                                                       ##
// ###########################################################################
__fastcall TICSDocIESetup::TICSDocIESetup() : TPersistent()
 {
  FUpdateIEPageSetup    = true;
  FIEFooter             = "&b&d";
  FIEHeader             = FMT(icsDocViewerIEHeader);
  FBasePageSetupRegPath = "Software\\Microsoft\\Internet Explorer\\PageSetup";
  FFooterKey            = "footer";
  FHeaderKey            = "header";
  FMarginBottomKey      = "margin_bottom";
  FMarginLeftKey        = "margin_left";
  FMarginRightKey       = "margin_right";
  FMarginTopKey         = "margin_top";
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocIESetup::AssignTo(Classes::TPersistent * Dest)
 {
  if (Dest->InheritsFrom(__classid(TICSDocIESetup)))
   {
    TICSDocIESetup * DestObj = dynamic_cast<TICSDocIESetup *>(Dest);
    DestObj->UpdateIEPageSetup    = UpdateIEPageSetup;
    DestObj->IEFooter             = IEFooter;
    DestObj->IEHeader             = IEHeader;
    DestObj->BasePageSetupRegPath = BasePageSetupRegPath;
    DestObj->FooterKey            = FooterKey;
    DestObj->HeaderKey            = HeaderKey;
    DestObj->MarginBottomKey      = MarginBottomKey;
    DestObj->MarginLeftKey        = MarginLeftKey;
    DestObj->MarginRightKey       = MarginRightKey;
    DestObj->MarginTopKey         = MarginTopKey;
   }
  else
   inherited::AssignTo(Dest);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocIESetup::HundredMMToInches(UnicodeString AHMM)
 {
  return DKFloatToStrF((DKStrToFloat((AHMM), '.') / 100.) / 25.4, ffFixed, 7, 5, '.');
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocIESetup::InternalReadIEPageSetup(HKEY ARootKey, UnicodeString & AIEFooter,
  UnicodeString & AIEHeader, UnicodeString & AIEMarginBottom, UnicodeString & AIEMarginLeft,
  UnicodeString & AIEMarginRight, UnicodeString & AIEMarginTop)
 {
  bool bResult = false;
  TRegistry * Registry = new TRegistry(KEY_READ);
  try
   {
    try
     {
      Registry->RootKey = ARootKey;
#pragma option push -w-pia
      if (bResult = Registry->OpenKey(FBasePageSetupRegPath, false))
#pragma option pop
       {
        AIEFooter       = Registry->ReadString(FFooterKey);
        AIEHeader       = Registry->ReadString(FHeaderKey);
        AIEMarginBottom = Registry->ReadString(FMarginBottomKey);
        AIEMarginLeft   = Registry->ReadString(FMarginLeftKey);
        AIEMarginRight  = Registry->ReadString(FMarginRightKey);
        AIEMarginTop    = Registry->ReadString(FMarginTopKey);
        Registry->CloseKey();
       }
     }
    catch (ERegistryException &)
     {
      bResult = false;
     }
   }
  __finally
   {
    delete Registry;
   }
  return bResult;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocIESetup::ReadIEPageSetup(HKEY & ARootKey, UnicodeString & AIEFooter, UnicodeString & AIEHeader,
  UnicodeString & AIEMarginBottom, UnicodeString & AIEMarginLeft, UnicodeString & AIEMarginRight,
  UnicodeString & AIEMarginTop)
 {
  bool bIsReadOk;
  ARootKey = HKEY_CURRENT_USER;
  if (!(bIsReadOk = InternalReadIEPageSetup(ARootKey, AIEFooter, AIEHeader, AIEMarginBottom, AIEMarginLeft,
    AIEMarginRight, AIEMarginTop)))
   {
    ARootKey  = HKEY_LOCAL_MACHINE;
    bIsReadOk = InternalReadIEPageSetup(ARootKey, AIEFooter, AIEHeader, AIEMarginBottom, AIEMarginLeft, AIEMarginRight,
      AIEMarginTop);
   }
  return bIsReadOk;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocIESetup::WriteIEPageSetup(HKEY ARootKey, UnicodeString AIEFooter, UnicodeString AIEHeader,
  UnicodeString AIEMarginBottom, UnicodeString AIEMarginLeft, UnicodeString AIEMarginRight, UnicodeString AIEMarginTop)
 {
  bool bResult = false;
  TRegistry * Registry = new TRegistry;
  try
   {
    try
     {
      Registry->RootKey = ARootKey;
#pragma option push -w-pia
      if (bResult = Registry->OpenKey(FBasePageSetupRegPath, false))
#pragma option pop
       {
        Registry->WriteString(FFooterKey, AIEFooter);
        Registry->WriteString(FHeaderKey, AIEHeader);
        Registry->WriteString(FMarginBottomKey, AIEMarginBottom);
        Registry->WriteString(FMarginLeftKey, AIEMarginLeft);
        Registry->WriteString(FMarginRightKey, AIEMarginRight);
        Registry->WriteString(FMarginTopKey, AIEMarginTop);
        Registry->CloseKey();
       }
     }
    catch (ERegistryException &)
     {
      bResult = false;
     }
   }
  __finally
   {
    delete Registry;
   }
  return bResult;
 }
// ---------------------------------------------------------------------------
// Margin'� ����������� � ������ ����������
bool __fastcall TICSDocIESetup::SetIEPageSetup(UnicodeString AIEMarginBottom, UnicodeString AIEMarginLeft,
  UnicodeString AIEMarginRight, UnicodeString AIEMarginTop)
 {
  bool bResult = false;
  if (FUpdateIEPageSetup)
   {
    try
     {
#pragma option push -w-pia
      if (bResult = (ReadIEPageSetup(__RootKey, __OldIEFooter, __OldIEHeader, __OldIEMarginBottom, __OldIEMarginLeft,
        __OldIEMarginRight, __OldIEMarginTop) && WriteIEPageSetup(__RootKey, FIEFooter, FIEHeader,
        HundredMMToInches(AIEMarginBottom), HundredMMToInches(AIEMarginLeft), HundredMMToInches(AIEMarginRight),
        HundredMMToInches(AIEMarginTop))))
#pragma option pop
       {
        __NewIEFooter       = FIEFooter;
        __NewIEHeader       = FIEHeader;
        __NewIEMarginBottom = HundredMMToInches(AIEMarginBottom);
        __NewIEMarginLeft   = HundredMMToInches(AIEMarginLeft);
        __NewIEMarginRight  = HundredMMToInches(AIEMarginRight);
        __NewIEMarginTop    = HundredMMToInches(AIEMarginTop);
       }
     }
    catch (...)
     {
      bResult = false;
     }
   }
  return (__PrevCallToIEPageSetupResult = bResult);
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocIESetup::RestoreIEPageSetup()
 {
  bool bResult = false;
  if (FUpdateIEPageSetup && __PrevCallToIEPageSetupResult)
   {
    try
     {
      UnicodeString tmpIEFooter, tmpIEHeader, tmpIEMarginBottom, tmpIEMarginLeft, tmpIEMarginRight, tmpIEMarginTop;
      if (ReadIEPageSetup(__RootKey, tmpIEFooter, tmpIEHeader, tmpIEMarginBottom, tmpIEMarginLeft, tmpIEMarginRight,
        tmpIEMarginTop) && (tmpIEFooter == __NewIEFooter) && (tmpIEHeader == __NewIEHeader) &&
        (tmpIEMarginBottom == __NewIEMarginBottom) && (tmpIEMarginLeft == __NewIEMarginLeft) &&
        (tmpIEMarginRight == __NewIEMarginRight) && (tmpIEMarginTop == __NewIEMarginTop))
       bResult = WriteIEPageSetup(__RootKey, __OldIEFooter, __OldIEHeader, __OldIEMarginBottom, __OldIEMarginLeft,
        __OldIEMarginRight, __OldIEMarginTop);
     }
    catch (...)
     {
      bResult = false;
     }
   }
  return bResult;
 }
// ###########################################################################
// ##                                                                       ##
// ##                           TICSDocViewerThread                         ##
// ##                                                                       ##
// ###########################################################################
// ����� ��� ����������� ������ ���������� "����������� ���������"
class ICSDocViewer::TICSDocViewerThread : public TThread
 {
  typedef TThread inherited;
 private:
  TICSDocViewer * FICSDocViewer;
  TProgressBar * FPB;
  int __PBMax;
  int __PBPosition;
  TICSDocViewerCreateStage __CreateStage;
 protected:
  void __fastcall Execute();
  void __fastcall InternalSetPBMax();
  void __fastcall InternalSetPBPosition();
  void __fastcall InternalGenerateOnChangeCreateStageEvent();
 public:
  __fastcall TICSDocViewerThread(bool CreateSuspended, TICSDocViewer * AICSDocViewer);
  void __fastcall InitPB();
  void __fastcall SetPBMax(const int nMax);
  void __fastcall IncPB();
  void __fastcall SetPBPosition(const int nPosition);
  void __fastcall GenerateOnChangeCreateStageEvent(TICSDocViewerCreateStage ACreateStage);
  __property Terminated;
 };
// ---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
static inline void ValidCtrCheck(TICSDocViewer *)
 {
  new TICSDocViewer(NULL);
 }
// ###########################################################################
// ##                                                                       ##
// ##                               TICSDocViewer                           ##
// ##                                                                       ##
// ###########################################################################
// ---------------------------------------------------------------------------
// ---------------------- ����������� ����� ----------------------------------
// ---------------------------------------------------------------------------
const UnicodeString TICSDocViewer::ICSName = cFMT(icsDocViewerCaption);
// ---------------------------------------------------------------------------
// ---------------------- IDKComponentVersion --------------------------------
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocViewer::DKGetVersion() const
 {
  return "1.2.2";
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::DKSetVersion(const UnicodeString AValue)
 {
 }
// ---------------------------------------------------------------------------
// ---------------------- IDKComponentComment --------------------------------
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocViewer::DKGetComment() const
 {
  return FComment;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::DKSetComment(const UnicodeString AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (FComment != AValue)
   FComment = AValue;
 }
// ***************************************************************************
// ********************** ��������/�������� ���������� ***********************
// ***************************************************************************
__fastcall TICSDocViewer::TICSDocViewer(TComponent * Owner) : TComponent(Owner), FNumTCellAlign("center"),
  FNumTCellFontName("Times New Roman"), FNumTCellFontSize(8), FNumTCellLTitle(cFMT(icsDocViewerNumTCellLTitle))
 {
  FComment                   = "";
  FDocFormat                 = dfHTML;
  FMode                      = vmSaveToDisk;
  FUseCSS                    = true;
  FUseWordCSS                = true;
  FUseExcelCSS               = true;
  PreviewDlgKind             = pmInternalDlg;
  FAsyncPreviewCreation      = true;
  FSpec                      = NULL;
  FDoc                       = NULL;
  FTmpPath                   = GetTempPath();
  FDefSavePath               = "";
  FHTML                      = "";
  FndHTML                    = NULL;
  FXMLContainer              = NULL;
  FPreviewType               = ptUnknown;
  FThrd                      = NULL;
  FThrdNormalTerminated      = true;
  FLocked                    = false;
  FState                     = csNone;
  FDocAssign                 = false;
  FListZeroValCheckCountOnly = true;
  FIESetup                   = new TICSDocIESetup;
  FDefPreviewDlgIcon         = new TIcon;
  FDefPreviewDlgIcon->ReleaseHandle();
  FDefPreviewDlgIcon->Handle = LoadIcon(HInstance, L"ICSDV_PREVIEW16i");
  FPreviewDlgIcon            = new TIcon;
  FDocXMLTemplate            = NULL;
  // FICSWordTemplateFN = "ICS_Word.dot";
  __BorderWidth = "";
  __RowNumbers  = true;
  __ColNumbers  = true;
  __RowNum      = 1;
  __ColNum      = 1;
  FPB           = NULL;
  __DopInfo     = new TStringList;
  __PBMax       = 0;
  __PBPosition  = 0;
  __URL         = "";
  // __DocName =  "";
  __OnFly                    = false;
  __CanShowPreviewDlg        = false;
  FOnChangeCreateStage       = NULL;
  FOnPreviewCreationFinished = NULL;
  FPPDemoMsg                 = NULL;
 }
// ---------------------------------------------------------------------------
__fastcall TICSDocViewer::~TICSDocViewer()
 {
  if (FDocXMLTemplate)
   {
    delete FDocXMLTemplate;
    FDocXMLTemplate = NULL;
   }
  __DELETE_OBJ(FndHTML);
  delete __DopInfo;
  delete FIESetup;
  delete FDefPreviewDlgIcon;
  delete FPreviewDlgIcon;
  if (FAsyncPreviewCreation && FThrd)
   {
    if (!FThrd->Terminated)
     FThrd->Terminate();
    FThrd->WaitFor(); // ��������� ���������� ����������� ������
   }
  // �������� ��������� ������
  for (list<UnicodeString>::iterator i = FTmpFileNames.begin(); i != FTmpFileNames.end(); i++)
   DeleteFile((* i).c_str());
  UnicodeString OldTmpFileName = AppendFileName(GetTempPath(), "ics_doc_viewer.htm");
  // �������� �������� ����������� ���������� �����, ���������� ����������� ������ < 1.1.0
  if (FileExists(OldTmpFileName))
   DeleteFile(OldTmpFileName.c_str());
 }
// ---------------------------------------------------------------------------
// ������������� ���� FndHTML
TTagNode * __fastcall TICSDocViewer::InitNDHTML(bool bCreateBodyTag)
 {
  __DELETE_OBJ(FndHTML);
  FndHTML = new TTagNode;
  if (bCreateBodyTag)
   {
    FndHTML->Name = "body";
    return (FUseWordCSS) ? FndHTML->AddChild("div", "class=Section1") : FndHTML;
   }
  return NULL;
 }
// ---------------------------------------------------------------------------
// ����������� ���� � �������� ��� ������������ ���������� HTML-����� � ������ vmSaveToDisk �� ���������
UnicodeString __fastcall TICSDocViewer::GetTempPath() const
 {
  wchar_t szTempPath[MAX_PATH];
  szTempPath[0] = '\0';
  ::GetTempPath(MAX_PATH, szTempPath);
  return static_cast<UnicodeString>(szTempPath);
 }
// ***************************************************************************
// ********************** ��������������� ������ *****************************
// ***************************************************************************
void __fastcall TICSDocViewer::ThrowIfLocked(UnicodeString AMethodName)
 {
  if (FLocked)
   throw DKClasses::EMethodError(AMethodName, FMT(icsDocViewerErrorObjectLocked));
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::InternalInitPB()
 {
  if (FPB)
   {
    FPB->Min      = 0;
    FPB->Position = 0;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::InternalSetPBMax()
 {
  if (FPB)
   FPB->Max = __PBMax;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::InternalIncPB()
 {
  if (FPB)
   FPB->Position = FPB->Position + 1;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::InternalSetPBPosition()
 {
  if (FPB)
   FPB->Position = __PBPosition;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::InitPB()
 {
  if (FAsyncPreviewCreation)
   {
    if (FThrd)
     FThrd->InitPB();
   }
  else
   InternalInitPB();
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetPBMax(const int nMax)
 {
  if (FAsyncPreviewCreation)
   {
    if (FThrd)
     FThrd->SetPBMax(nMax);
   }
  else
   {
    __PBMax = nMax;
    InternalSetPBMax();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::IncPB()
 {
  if (FAsyncPreviewCreation)
   {
    if (FThrd)
     FThrd->IncPB();
   }
  else
   InternalIncPB();
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetPBPosition(const int nPosition)
 {
  if (FAsyncPreviewCreation)
   {
    if (FThrd)
     FThrd->SetPBPosition(nPosition);
   }
  else
   {
    __PBPosition = nPosition;
    InternalSetPBPosition();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::InternalGenerateOnChangeCreateStageEvent()
 {
  if (FOnChangeCreateStage)
   FOnChangeCreateStage(this, __CreateStage);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::GenerateOnChangeCreateStageEvent(TICSDocViewerCreateStage ACreateStage)
 {
  if (FAsyncPreviewCreation)
   {
    if (FThrd)
     FThrd->GenerateOnChangeCreateStageEvent(ACreateStage);
   }
  else
   {
    __CreateStage = ACreateStage;
    InternalGenerateOnChangeCreateStageEvent();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::ChangeState(TICSDocViewerCreateStage AState, bool bGenerateOnChangeCreateStageEvent)
 {
  FState = AState;
  if (bGenerateOnChangeCreateStageEvent)
   GenerateOnChangeCreateStageEvent(AState);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetTmpPath(UnicodeString AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (FTmpPath != AValue)
   FTmpPath = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetDefSavePath(UnicodeString AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (FDefSavePath != AValue)
   FDefSavePath = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetXMLContainer(const TAxeXMLContainer * AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (FXMLContainer != AValue)
   FXMLContainer = const_cast<TAxeXMLContainer *>(AValue);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetMode(const TICSDocViewerMode AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (FMode != AValue)
   FMode = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetUseCSS(const bool AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (FUseCSS != AValue)
   FUseCSS = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetUseWordCSS(const bool AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (FUseWordCSS != AValue)
   FUseWordCSS = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetUseExcelCSS(const bool AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (FUseExcelCSS != AValue)
   FUseExcelCSS = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetPreviewDlgKind(const TICSDocViewerDlgKind AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (FPreviewDlgKind != AValue)
   FPreviewDlgKind = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetAsyncPreviewCreation(const bool AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (FAsyncPreviewCreation != AValue)
   FAsyncPreviewCreation = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetOnChangeCreateStage(const TOnChangeCreateStageEvent AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (FOnChangeCreateStage != AValue)
   FOnChangeCreateStage = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetOnPreviewCreationFinished(const TOnPreviewCreationFinishedEvent AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (FOnPreviewCreationFinished != AValue)
   FOnPreviewCreationFinished = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetDocFormat(const TICSDocViewerDocFormat AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (FDocFormat != AValue)
   {
    if (AValue == dfMSWord)
     throw DKClasses::EInvalidArgument(__FUNC__, "AValue", FMT(icsDocViewerErrorSetDocFormat));
    FDocFormat = AValue;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetSpecificator(const TTagNode * AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (AValue)
   CheckSpec(CC_TNode(AValue));
  if (FSpec)
   __DELETE_OBJ(FSpec)
   if (AValue)
    {
     FSpec = new TTagNode;
     FSpec->Assign(CC_TNode(AValue), true);
    }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetDocument(const TTagNode * AValue)
 {
  ThrowIfLocked(__FUNC__);
  if (FDoc != AValue)
   {
    if (AValue && (AValue->Name != "docum"))
     throw DKClasses::EInvalidArgument(__FUNC__, "AValue", FMT(icsDocErrorDocNotDefined));
    if (FDocAssign)
     FDoc->Assign(CC_TNode(AValue), true);
    else
     FDoc = CC_TNode(AValue);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetDocAssign(const bool AValue)
 {
  ThrowIfLocked(__FUNC__);
  FDocAssign = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetListZeroValCheckCountOnly(const bool AValue)
 {
  ThrowIfLocked(__FUNC__);
  FListZeroValCheckCountOnly = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetIESetup(const TICSDocIESetup * AValue)
 {
  ThrowIfLocked(__FUNC__);
  FIESetup->Assign(const_cast<TICSDocIESetup *>(AValue));
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::SetPreviewDlgIcon(const TIcon * AValue)
 {
  ThrowIfLocked(__FUNC__);
  FPreviewDlgIcon->Assign(const_cast<TIcon *>(AValue));
 }
// ---------------------------------------------------------------------------
TAxeXMLContainer * __fastcall TICSDocViewer::GetXMLContainer() const
 {
  if (!FXMLContainer)
   throw DKClasses::EMethodError(__FUNC__, FMT(icsDocErrorXMLContainerNotDefined));
  return FXMLContainer;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::ThreadTerminate(TObject * Sender)
 {
  FLocked = false;
  FThrd   = NULL;
  if (FOnPreviewCreationFinished)
   FOnPreviewCreationFinished(this, FThrdNormalTerminated);
  if (FThrdNormalTerminated)
   InternalPreviewDlg();
  ChangeState(csNone, false);
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocViewer::CheckIfTerminated()
 {
  if (FAsyncPreviewCreation && FThrd && FThrd->Terminated)
   {
    FThrdNormalTerminated = false;
    return true;
   }
  return false;
 }
// ---------------------------------------------------------------------------
// ����������� ���'� ����� ��� �������� ������� ndInsText ( xml-��� "instext" ������������� )
UnicodeString __fastcall TICSDocViewer::GetInsTextStyle(TTagNode * ndInsText)
 {
  UnicodeString Result;
  if ((Result = ndInsText->AV["style"]) == "")
   if ((Result = ndInsText->GetParent("inscription")->AV["style"]) == "")
    Result = ndInsText->GetRoot()->GetChildByName("styles", true)->AV["defaultbstyle"];
  return Result;
 }
// ---------------------------------------------------------------------------
// ����������� ������������ ��� �������� ������� ndInsText ( xml-��� "instext" ������������� )
UnicodeString __fastcall TICSDocViewer::GetInsTextAlign(TTagNode * ndInsText)
 {
  UnicodeString StyleUID = GetInsTextStyle(ndInsText);
  TTagNode * ndStyles = ndInsText->GetRoot()->GetChildByName("styles", true);
  return ndStyles->GetChildByAV("style", "uid", StyleUID)->AV["align"];
 }
// ---------------------------------------------------------------------------
/*
 * ����������� ���'� ����� ������������� ��� ������ ������� ( xml-��� "table" ������������� ) ndCellTagNode
 *
 *
 * ���������:
 *   [in]  AStyleAttrName   - ��� ��������, ������������� ������������ ����� ( "hstyle" ��� "bstyle" )
 *   [in]  ndCellTagNode    - ������ ������� ( xml-��� "trow" ��� "tcol" ������������� )
 *   [in]  ndAltCellTagNode - �������������� ������ ������� ( xml-��� "trow" ��� "tcol" ������������� )
 *                            [default = NULL]
 *
 * ������������ ��������:
 *   ��� ����� ������������� ��� ������ ������� ( xml-��� "table" ������������� ) ndCellTagNode
 *
 */
UnicodeString __fastcall TICSDocViewer::GetTableCellStyle(UnicodeString AStyleAttrName, TTagNode * ndCellTagNode,
  TTagNode * ndAltCellTagNode)
 {
  UnicodeString Result;
  if ((Result = ndCellTagNode->AV[AStyleAttrName]) == "")
   if (!ndAltCellTagNode || (ndAltCellTagNode && ((Result = ndAltCellTagNode->AV[AStyleAttrName]) == "")))
    if ((Result = ndCellTagNode->GetParent("table")->AV[AStyleAttrName]) == "")
     Result = ndCellTagNode->GetRoot()->GetChildByName("styles", true)->AV[(AStyleAttrName == "hstyle") ?
      "defaulthstyle" : "defaultbstyle"];
  return Result;
 }
// ---------------------------------------------------------------------------
/*
 * ����������� ���'� ����� ������������� ��� ������ ( xml-��� "list" ������������� ) ndList
 *
 *
 * ���������:
 *   [in]  AStyleAttrName - ��� ��������, ������������� ������������ ����� ( "hstyle" ��� "bstyle" )
 *   [in]  ndList         - ������ ( xml-��� "list" ������������� )
 *
 * ������������ ��������:
 *   ��� ����� ������������� ��� ������ ( xml-��� "list" ������������� ) ndList
 *
 */
UnicodeString __fastcall TICSDocViewer::GetListStyle(UnicodeString AStyleAttrName, TTagNode * ndList)
 {
  UnicodeString Result;
  if ((Result = ndList->AV[AStyleAttrName]) == "")
   Result = ndList->GetRoot()->GetChildByName("styles", true)->AV[(AStyleAttrName == "hstyle") ? "defaulthstyle" :
    "defaultbstyle"];
  return Result;
 }
// ---------------------------------------------------------------------------
/*
 * ����������� ���'� ����� ������������� ��� ������ ������ ( xml-��� "list" ������������� ) ndCellTagNode
 *
 *
 * ���������:
 *   [in]  AStyleAttrName   - ��� ��������, ������������� ������������ ����� ( "hstyle" ��� "bstyle" )
 *   [in]  ndCellTagNode    - ������ ������ ( xml-��� "list" ������������� )
 *
 * ������������ ��������:
 *   ��� ����� ������������� ��� ������ ������ ( xml-��� "list" ������������� ) ndCellTagNode
 *
 */
UnicodeString __fastcall TICSDocViewer::GetListCellStyle(UnicodeString AStyleAttrName, TTagNode * ndCellTagNode)
 {
  UnicodeString Result = "";
  if (TICSDocSpec::IsListColSingle1(ndCellTagNode))
   Result = ndCellTagNode->AV[AStyleAttrName];
  else if (TICSDocSpec::IsListColSingle2(ndCellTagNode))
   Result = ndCellTagNode->GetParent("listcol")->AV[AStyleAttrName];
  else if (TICSDocSpec::IsListColGroup(ndCellTagNode))
   if (ndCellTagNode->CmpName("listcolgroup"))
    {
     if (AStyleAttrName == "hstyle")
      Result = ndCellTagNode->AV["style"];
    }
   else // listcol
      Result = ndCellTagNode->AV[AStyleAttrName];
  if (Result == "")
   Result = GetListStyle(AStyleAttrName, ndCellTagNode->GetParent("list"));
  return Result;
 }
// ---------------------------------------------------------------------------
// ������������ �������������� �������� � ������ �������� ����������� ������� ��������
// ������������ ������������, ��� 0.0 ����� ������������ ��� 0
void __fastcall TICSDocViewer::NormalizeNumericalValue(UnicodeString & ACellText, const bool fShowZVal)
 {
  try
   {
    const double cdblEps = 1E-8;
    double dblValue = DKStrToFloat(ACellText, '.');
    if (fabs(dblValue - 0.0) < cdblEps)
     ACellText = (fShowZVal) ? "0" : "";
   }
  catch (EConvertError &)
   {;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::HiMetricToPixels(long * pnWidth, long * pnHeight)
 {
  // ��������� ��� ��������������
  const float nHiMetricPerInch = 25.4;
  HDC hDC = GetDC(0);
  // ���������� ���������� �������� � ����������
  // ����� �� ����������� � �� ���������
  int dpiX = GetDeviceCaps(hDC, LOGPIXELSX);
  int dpiY = GetDeviceCaps(hDC, LOGPIXELSY);
  // ��������� ��������������
  if (pnWidth)
   * pnWidth = *pnWidth * dpiX / nHiMetricPerInch;
  if (pnHeight)
   * pnHeight = *pnHeight * dpiY / nHiMetricPerInch;
  ReleaseDC(0, hDC);
 }
// ---------------------------------------------------------------------------
// ������������ ������ ��� ��������� ������� ������ ( xml-��� "list" ������������� )
UnicodeString __fastcall TICSDocViewer::GetListColHeaderText(TTagNode * ndListCol)
 {
  UnicodeString HeaderText = "";
  if (TICSDocSpec::IsListCol(ndListCol))
   {
    if (TICSDocSpec::IsListColSingle2(ndListCol))
     {
      if (ndListCol->AV["ref"] != "")
       HeaderText = GetXMLContainer()->GetRefer(ndListCol->AV["ref"])->AV["name"];
     }
    else
     HeaderText = ndListCol->AV["name"];
    if ((HeaderText == "") && (FDocFormat == dfHTML))
     HeaderText = "&nbsp;";
   }
  return HeaderText;
 }
// ---------------------------------------------------------------------------
// ������������ ������ ��� ��������� ������/������� ������� ( xml-��� "table" ������������� )
UnicodeString __fastcall TICSDocViewer::GetTableHeaderCellText(TTagNode * ndHeaderCell)
 {
  UnicodeString HeaderText = ndHeaderCell->AV["name"];
  if ((HeaderText == "") && (FDocFormat == dfHTML))
   HeaderText = "&nbsp;";
  return HeaderText;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ������ ��� ������ � ������� ������ ������ ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in/out]  ndV       - ������� xml-��� "iv" ���������
 *   [in]      ndListCol - xml-��� ������������� ( "listcol", "listcolgroup" ��� "is_attr" ),
 *                         ��������������� �������� ������� ����������� ������
 *                         ( xml-��� "list" ������������� )
 *   [in]      bShowZVal - ������� "���������� ������� ��������"
 *
 * ������������ ��������:
 *   ����� ��� ������ � ������� ������/������� ������� ( xml-��� "table" ������������� )
 *
 */
UnicodeString __fastcall TICSDocViewer::GetDocListCellText(TTagNode *& ndV, const TTagNode * ndListCol,
  const bool bShowZVal)
 {
  UnicodeString CellText = "";
  if (TICSDocSpec::IsListColSingle1(ndListCol) || TICSDocSpec::IsListColSingle2(ndListCol))
   CellText = ndV->AV["val"];
  else // ������ �������� � ��������� "���������� �������"
   {
    int nChildColCount = (CC_TNode(ndListCol)->CmpName("listcolgroup"))
      // ��� listcolgroup
      ? ndListCol->Count
      // ��� listcol( link_is( is_attr+ ) )
      : GetTagNodeChildrenCount(CC_TNode(ndListCol)->GetFirstChild(), "is_attr", false);
    for (int i = 0; i < nChildColCount; i++)
     {
      if (CellText != "")
       CellText += " ";
      CellText += ndV->AV["val"];
      if (i < nChildColCount - 1)
       ndV = ndV->GetNext();
     }
   }
  // ������ ����������� ������� ��������
  if (!FListZeroValCheckCountOnly || (FListZeroValCheckCountOnly && CC_TNode(ndListCol)->CmpName("listcol") && CC_TNode
    (ndListCol)->GetChildByName("link_is") && CC_TNode(ndListCol)->GetChildByName("link_is")->GetChildByName("count")))
   NormalizeNumericalValue(CellText, bShowZVal);
  return CellText;
 }
// ---------------------------------------------------------------------------
/*
 * ����������� ������������� �������� ������ �����, ��������������� �������� ������
 * ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in]  ANode  - ������ xml-���������, � ������� �������������� ����� �������������
 *                  �������� ������
 *
 * ������������ ��������:
 *   ������������ �������� ������
 *
 */
long __fastcall TICSDocViewer::GetListColsMaxLevel(const TTagNode * ANode) const
 {
  long nLevel = 0;
  if (TICSDocSpec::IsListColSingle(ANode))
   {
    int nNodeLevel = ANode->Level;
    if (TICSDocSpec::IsListColSingle2(ANode))
     nNodeLevel-- ;
    if (nNodeLevel > nLevel)
     nLevel = nNodeLevel;
   }
  TTagNode * ndChild = CC_TNode(ANode)->GetFirstChild();
  while (ndChild)
   {
    long nChildNodesMaxLevel = GetListColsMaxLevel(ndChild);
    if (nChildNodesMaxLevel > nLevel)
     nLevel = nChildNodesMaxLevel;
    ndChild = ndChild->GetNext();
   }
  return nLevel;
 }
// ---------------------------------------------------------------------------
// ����������� ������������� �������� ��� "����������" ��� ������������ �������������
int __fastcall TICSDocViewer::GetMaxPBForSpec()
 {
  TTagNode * ndDocContent = FSpec->GetChildByName("doccontent");
  int nPBMax = ndDocContent->Count;
  TTagNode * ndDocElement = ndDocContent->GetFirstChild();
  while (ndDocElement)
   {
    if (ndDocElement->CmpName("inscription"))
     nPBMax += ndDocElement->Count;
    else if (ndDocElement->CmpName("list"))
     nPBMax += CountTagNodes(ndDocElement, TICSDocSpec::IsListColSingle);
    else if (ndDocElement->CmpName("table"))
     {
      nPBMax += CountTagNodes(ndDocElement->GetChildByName("columns"), "tcol");
      nPBMax += CountTagNodes(ndDocElement->GetChildByName("rows"), "trow");
     }
    ndDocElement = ndDocElement->GetNext();
   }
  return nPBMax;
 }
// ---------------------------------------------------------------------------
// ����������� ������������� �������� ��� "����������" ��� ������������ ���������
int __fastcall TICSDocViewer::GetMaxPBForDoc()
 {
  int nPBMax = GetMaxPBForSpec();
  TTagNode * ndObj = FDoc->GetChildByName("contens")->GetFirstChild();
  while (ndObj)
   {
    TTagNode * ndObjRef = FSpec->GetTagByUID(ndObj->AV["uidmain"]);
    if (ndObjRef->CmpName("list,table"))
     nPBMax += ndObj->Count;
    ndObj = ndObj->GetNext();
   }
  return nPBMax;
 }
// ---------------------------------------------------------------------------
// ����������� ������������� �������� ��� "����������" ��� ������������ ���������
// �� ������� MS Word
int __fastcall TICSDocViewer::GetMaxPBForDocByMSWordTemplate()
 {
  TTagNode * ndDocContent = FSpec->GetChildByName("doccontent");
  int nPBMax = ndDocContent->Count;
  TTagNode * ndDocElement = ndDocContent->GetFirstChild();
  while (ndDocElement)
   {
    if (ndDocElement->CmpName("inscription"))
     nPBMax += ndDocElement->Count;
    ndDocElement = ndDocElement->GetNext();
   }
  TTagNode * ndObj = FDoc->GetChildByName("contens")->GetFirstChild();
  while (ndObj)
   {
    TTagNode * ndObjRef = FSpec->GetTagByUID(ndObj->AV["uidmain"]);
    if (ndObjRef->CmpName("list,table"))
     nPBMax += ndObj->Count;
    ndObj = ndObj->GetNext();
   }
  return nPBMax;
 }
// ---------------------------------------------------------------------------
// ������������ ����������� ���������/������������� (���������� �����)
void __fastcall TICSDocViewer::InternalPreviewCreate()
 {
  // �������������
  ChangeState(csCreateInit);
  InitPB();
  // ������������ �����������
  switch (FPreviewType)
   {
   case ptSpecificator:
    if (!FSpec)
     throw DKClasses::EMethodError(__FUNC__, FMT(icsDocViewerErrorNoSpecDef));
    if (FDocXMLTemplate)
     CreateSpecByMSWordTemplate();
    else
     switch (FDocFormat)
      {
      case dfHTML:
       CreateSpecHTML();
       InitHTMLPreview();
       break;
      case dfMSWord:
       CreateSpecMSWord();
       InitMSWordPreview();
       break;
      }
    break;
   case ptDocument:
    if (!FSpec)
     throw DKClasses::EMethodError(__FUNC__, FMT(icsDocViewerErrorNoSpecDef));
    if (!FDoc)
     throw DKClasses::EMethodError(__FUNC__, FMT(icsDocViewerErrorNoDocDef));
    if (FDoc->GetChildByName("passport")->AV["GUIspecificator"].UpperCase() != FSpec->GetChildByName("passport")
      ->AV["GUI"].UpperCase())
     throw DKClasses::EMethodError(__FUNC__, FMT(icsDocErrorGUINotValid));
    if (FDocXMLTemplate)
     CreateDocByMSWordTemplate();
    else
     switch (FDocFormat)
      {
      case dfHTML:
       CreateDocHTML();
       InitHTMLPreview();
       break;
      case dfMSWord:
       CreateDocMSWord();
       InitMSWordPreview();
       break;
      }
    break;
   }
 }
// ---------------------------------------------------------------------------
// ������ ������� ���������������� ��������� (���������� �����)
void __fastcall TICSDocViewer::InternalPreviewDlg()
 {
  if (!__CanShowPreviewDlg)
   return;
  ChangeState(csPreviewDlgStart);
  if (FDocXMLTemplate)
   {
    TXMLDoc * FDoc = NULL;
    try
     {
      FDoc = new TXMLDoc();
      FDoc->Load(FDocXMLTemplate);
      ByXMLTemplatePreview(FDoc);
     }
    __finally
     {
      if (FDoc)
       delete FDoc;
     }
   }
  else
   switch (FDocFormat)
    {
    case dfHTML:
     HTMLPreview();
     break;
    case dfMSWord:
     MSWordPreview();
     break;
    }
 }
// ***************************************************************************
// ********************** ��������������� ������ *****************************
// ********************** ��� ������� HTML ***********************************
// ***************************************************************************
// ���������� � htmlParent ��������� html-���� "table"
// ( ������������ ��������� ���� __BorderWidth ��� �������� ������� ����� � __Width ��� �������� ������ )
TTagNode * __fastcall TICSDocViewer::AddHTMLTableTag(TTagNode * htmlParent)
 {
  TTagNode * htmlTableTag = htmlParent->AddChild("table");
  SetHTMLBoxWidthValues(htmlTableTag, __BorderWidth, __Width);
  htmlTableTag->AV["bordercolor"] = "black";
  htmlTableTag->AV["cellspacing"] = "0";
  htmlTableTag->AV["cellpadding"] = "3";
  return htmlTableTag;
 }
// ---------------------------------------------------------------------------
// ������������ ����� � �������� �������� ��� ��������� ������������ ��� html-���� "div"
void __fastcall TICSDocViewer::GetHTML_DIVAlignAttrVal(UnicodeString AAlign, UnicodeString & AAttrName,
  UnicodeString & AAttrVal)
 {
  if (FUseCSS)
   {
    AAttrName = "style";
    AAttrVal  = "vertical-align:top; text-align : " + AAlign; /* change kab 20.06.07 "vertical-align:top;" */
   }
  else
   {
    AAttrName = "align";
    AAttrVal  = AAlign;
   }
 }
// ---------------------------------------------------------------------------
/*
 * ����������� ������������ ����� �������� ������ � ������� � �������� ������ �
 * ���������� ��������, � ������� ����������� ������ ������ ��� html-���� "font".
 * ������� ������������ ��������� �� ������ ������� html-������, �����������
 * ���������� MS Word v11 ( MS Office 2003 ).
 *
 *
 * ���������:
 *   [in]  nFontSize - ������ ������ � �������
 *
 * ������������ ��������:
 *   ������ ������ � ���������� ��������
 *
 */
int __fastcall TICSDocViewer::GetHTMLFontSizeMSOLike(const int nFontSize)
 {
  if (nFontSize <= 9)
   return 1;
  else if (nFontSize <= 11)
   return 2;
  else if (nFontSize == 12)
   return 3;
  else if (nFontSize <= 15)
   return 4;
  else if (nFontSize <= 20)
   return 5;
  else if (nFontSize <= 29)
   return 6;
  else
   return 7;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ html-����� ��� �������������� ������ � ������������ � ���������
 * ������
 *
 *
 * ���������:
 *   [in]  htmlTag   - html-��� � ������� ( �������� <div>, <span>, <th>, <td> )
 *   [in]  AStyleUID - ��� ����� ������������� ��� ��������������
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������������� �����
 *
 *
 */
TTagNode * __fastcall TICSDocViewer::CreateHTMLTextFormat(TTagNode * htmlTag, UnicodeString AStyleUID)
 {
  TTagNode * ndResult = htmlTag;
  TTagNode * ndStyles = FSpec->GetChildByName("styles", true);
  TTagNode * ndStyle = ndStyles->GetChildByAV("style", "uid", AStyleUID);
  if (FUseCSS)
   {
    htmlTag->AV["class"] = AStyleUID;
    if (ndStyle->CmpAV("wordwrap", "0"))
     if (htmlTag->CmpName("th,td"))
      htmlTag->AV["nowrap"] = "";
     else
      ndResult = htmlTag->AddChild("nobr");
   }
  else
   {
    if (htmlTag->CmpName("th,td"))
     htmlTag->AV["align"] = ndStyle->AV["align"];
    ndResult             = htmlTag->AddChild("font");
    ndResult->AV["face"] = ndStyle->AV["fontname"];
    ndResult->AV["size"] = IntToStr((int) GetHTMLFontSizeMSOLike(StrToInt(ndStyle->AV["fontsize"])));
    if (ndStyle->CmpAV("wordwrap", "0"))
     ndResult = ndResult->AddChild("nobr");
    if (ndStyle->CmpAV("strike", "1"))
     ndResult = ndResult->AddChild("strike");
    if (ndStyle->CmpAV("underline", "1"))
     ndResult = ndResult->AddChild("u");
    if (ndStyle->CmpAV("bold", "1"))
     ndResult = ndResult->AddChild("b");
    if (ndStyle->CmpAV("italic", "1"))
     ndResult = ndResult->AddChild("i");
   }
  return ndResult;
 }
// ---------------------------------------------------------------------------
/*
 * ��������� ������ ����� ��� ������ html-������� ( ������ ��� �������������� CSS )
 *
 *
 * ���������:
 *   [in]  htmlTableCell - html-���, ��������������� ������ html-�������
 *   [in]  ABorderWidth  - ������ �����
 *
 * ������������ ��������:
 *   true  - �������� ���������
 *   false - ��������� ����������� �� ����, ����� ���������� ������������� CSS
 *
 */
bool __fastcall TICSDocViewer::SetHTMLTableCellBorderWidth(TTagNode * htmlTableCell, UnicodeString ABorderWidth)
 {
  if (FUseCSS)
   {
    htmlTableCell->AV["style"] = "border-width:" + ABorderWidth + "px";
    return true;
   }
  else
   return false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocViewer::SetHTMLBoxWidthValues(TTagNode * htmlTag, UnicodeString ABorderWidth,
  UnicodeString AWidth)
 {
  UnicodeString AWidthHTMLVal = "";
  if (AWidth != "")
   {
    if (AWidth.Pos("%"))
     AWidthHTMLVal = AWidth;
    else
     {
      long nWidthPX = AWidth.ToInt();
      HiMetricToPixels(& nWidthPX, NULL);
      AWidthHTMLVal = IntToStr((int) nWidthPX) + "px";
     }
   }
  if (FUseCSS)
   {
    UnicodeString AStyleAttrVal = "border-width:" + ABorderWidth + "px";
    if (AWidthHTMLVal != "")
     AStyleAttrVal += "; width:" + AWidthHTMLVal;
    htmlTag->AV["style"] = AStyleAttrVal;
   }
  else
   {
    if (AWidthHTMLVal != "")
     htmlTag->AV["width"] = AWidthHTMLVal;
    if (htmlTag->CmpName("table"))
     htmlTag->AV["border"] = ABorderWidth;
   }
  return true;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ html-����� ��� �������������� ������ ������ html-�������,
 * ������������ ��� ��������� �����/��������
 *
 *
 * ���������:
 *   [in]  htmlNumTableCell - html-���, ��������������� ������ html-�������,
 *                            ������������ ��� ��������� �����/�������� ( html-��� "th" )
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������������� �����
 *
 *
 */
TTagNode * __fastcall TICSDocViewer::CreateHTMLTableNumCellTextFormat(TTagNode * htmlNumTableCell)
 {
  TTagNode * ndResult = htmlNumTableCell;
  if (FUseCSS)
   htmlNumTableCell->AV["class"] = "num";
  else
   {
    htmlNumTableCell->AV["align"] = FNumTCellAlign;
    ndResult                      = htmlNumTableCell->AddChild("font");
    ndResult->AV["face"]          = FNumTCellFontName;
    ndResult->AV["size"]          = IntToStr((int) GetHTMLFontSizeMSOLike(StrToInt(FNumTCellFontSize)));
   }
  return ndResult;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ �������� HTML-�����
 *
 *
 * ���������:
 *   [in]  AStyleClass          - ������������ ������ HTML-�����
 *   [in]  AStyleClassStrPrefix - ������� ����� ������������� ������ HTML-�����
 *   [in]  AStylePropStrPrefix  - ������� ����� ��������
 *   [in]  ndStyle              - xml-��� "style" �������������
 *
 * ������������ ��������:
 *   �������� HTML-����� � ���� ������
 *
 *
 */
UnicodeString __fastcall TICSDocViewer::GetHTMLStyleDefinition(UnicodeString AStyleClass,
  UnicodeString AStyleClassStrPrefix, UnicodeString AStylePropStrPrefix, TTagNode * ndStyle)
 {
  UnicodeString STYLE = AStyleClassStrPrefix + "." + AStyleClass + "\n";
  STYLE += AStyleClassStrPrefix + "{\n";
  STYLE += AStylePropStrPrefix + "text-align     : " + ndStyle->AV["align"] + ";\n";
  STYLE += AStylePropStrPrefix + "vertical-align:top;\n"; /* change kab 20.06.07 ������������ ������������ */
  STYLE += AStylePropStrPrefix + "mso-number-format:\"\@\";"; /* change kab 20.06.07 ��� excel-� ��� ������ "�����" */
  STYLE += AStylePropStrPrefix + "font-family    : \"" + ndStyle->AV["fontname"] + "\";\n";
  STYLE += AStylePropStrPrefix + "font-size      : " + ndStyle->AV["fontsize"] + "pt;\n";
  STYLE += AStylePropStrPrefix + "text-decoration:";
  if (ndStyle->CmpAV("strike", "1") || ndStyle->CmpAV("underline", "1"))
   {
    if (ndStyle->CmpAV("strike", "1"))
     STYLE += " line-through";
    if (ndStyle->CmpAV("underline", "1"))
     STYLE += " underline";
   }
  else
   STYLE += " none";
  STYLE += ";\n";
  STYLE += AStylePropStrPrefix + "font-weight    :";
  STYLE += (ndStyle->CmpAV("bold", "1")) ? " bold" : " normal";
  STYLE += ";\n";
  STYLE += AStylePropStrPrefix + "font-style     :";
  STYLE += (ndStyle->CmpAV("italic", "1")) ? " italic" : " normal";
  STYLE += ";\n";
  STYLE += AStyleClassStrPrefix + "}\n";
  return STYLE;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ������� HTML-������
 *
 *
 * ���������:
 *   [in]  AStyleClassStrPrefix - ������� ����� ������������� ������ HTML-�����
 *   [in]  AStylePropStrPrefix  - ������� ����� ��������
 *   [in]  ndStyle              - xml-��� "styles" �������������
 *
 * ������������ ��������:
 *   �������� ������� HTML-������ � ���� ������
 *
 *
 */
UnicodeString __fastcall TICSDocViewer::GetHTMLSTYLESDefinition(UnicodeString AStyleClassStrPrefix,
  UnicodeString AStylePropStrPrefix, TTagNode * ndStyles)
 {
  UnicodeString STYLES = "<!-- Document style sheet -->\n" "<style type='text/css'>\n" "<!--\n";
  TTagNode * ndStyle = ndStyles->GetFirstChild();
  while (ndStyle)
   {
    STYLES += GetHTMLStyleDefinition(ndStyle->AV["uid"], AStyleClassStrPrefix, AStylePropStrPrefix, ndStyle);
    ndStyle = ndStyle->GetNext();
   }
  STYLES += "-->\n</style>";
  return STYLES;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ������� HTML-������ ��� ������������� ���������� �������� ������������ HTML-��������� �
 * MS Word 2000/XP/2003
 *
 *
 * ���������:
 *   [in]  AStyleClassStrPrefix - ������� ����� ������������� ������ HTML-�����
 *   [in]  AStylePropStrPrefix  - ������� ����� ��������
 *   [in]  ndStyle              - xml-��� "pagesetup" �������������
 *
 * ������������ ��������:
 *   �������� ������� HTML-������ � ���� ������
 *
 *
 */
UnicodeString __fastcall TICSDocViewer::GetMSOWordHTMLSTYLESDefinition(UnicodeString AStyleClassStrPrefix,
  UnicodeString AStylePropStrPrefix, TTagNode * ndPageSetup)
 {
  UnicodeString STYLES =
    "<!-- Style sheet for Microsoft Word v9 (MS Office 2000), v10 (MS Office XP ), v11 (MS Office 2003) -->\n" "<!--[if gte mso 9]>\n"
    "<style>\n";
  STYLES += AStyleClassStrPrefix + "@page Section1\n";
  STYLES += AStyleClassStrPrefix + "{\n";
  STYLES += AStylePropStrPrefix + "size                : ";
  UnicodeString MSOWidth, MSOHeight;
  if (ndPageSetup->CmpAV("orientation", "0")) // ������� ����������
   {
    MSOWidth  = DKFloatToStrF(StrToFloat(ndPageSetup->AV["width"]) / 100, ffFixed, 7, 2, '.');
    MSOHeight = DKFloatToStrF(StrToFloat(ndPageSetup->AV["height"]) / 100, ffFixed, 7, 2, '.');
   }
  else // ��������� ����������
   {
    MSOWidth  = DKFloatToStrF(StrToFloat(ndPageSetup->AV["height"]) / 100, ffFixed, 7, 2, '.');
    MSOHeight = DKFloatToStrF(StrToFloat(ndPageSetup->AV["width"]) / 100, ffFixed, 7, 2, '.');
   }
  STYLES += MSOWidth + "mm " + MSOHeight + "mm;\n";
  STYLES += AStylePropStrPrefix + "mso-page-orientation: ";
  STYLES += ndPageSetup->CmpAV("orientation", "0") ? "portrait" : "landscape";
  STYLES += ";\n";
  UnicodeString MSOMarginLeft, MSOMarginRight, MSOMarginTop, MSOMarginBottom;
  MSOMarginLeft   = DKFloatToStrF(StrToFloat(ndPageSetup->AV["leftfield"]) / 100, ffFixed, 7, 2, '.');
  MSOMarginRight  = DKFloatToStrF(StrToFloat(ndPageSetup->AV["rightfield"]) / 100, ffFixed, 7, 2, '.');
  MSOMarginTop    = DKFloatToStrF(StrToFloat(ndPageSetup->AV["topfield"]) / 100, ffFixed, 7, 2, '.');
  MSOMarginBottom = DKFloatToStrF(StrToFloat(ndPageSetup->AV["bottomfield"]) / 100, ffFixed, 7, 2, '.');
  STYLES += AStylePropStrPrefix + "margin              : " + MSOMarginTop + "mm " + MSOMarginRight + "mm " +
    MSOMarginBottom + "mm " + MSOMarginLeft + "mm;\n";
  STYLES += AStyleClassStrPrefix + "}\n";
  STYLES += AStyleClassStrPrefix + "div.Section1\n" + AStyleClassStrPrefix + "{\n" + AStylePropStrPrefix +
    "page: Section1;\n" + AStyleClassStrPrefix + "}\n";
  STYLES += "</style>\n" "<![endif]-->";
  return STYLES;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ������� HTML-������ ��� ������������� ���������� �������� ������������ HTML-��������� �
 * MS Excel 2000/XP/2003
 *
 *
 * ���������:
 *   [in]  AStyleClassStrPrefix - ������� ����� ������������� ������ HTML-�����
 *   [in]  AStylePropStrPrefix  - ������� ����� ��������
 *   [in]  ndStyle              - xml-��� "pagesetup" �������������
 *
 * ������������ ��������:
 *   �������� ������� HTML-������ � ���� ������
 *
 *
 */
UnicodeString __fastcall TICSDocViewer::GetMSOExcelHTMLSTYLESDefinition(UnicodeString AStyleClassStrPrefix,
  UnicodeString AStylePropStrPrefix, TTagNode * ndPageSetup)
 {
  // Style sheet
  UnicodeString STYLES =
    "<!-- Style sheet for Microsoft Excel v9 (MS Office 2000), v10 (MS Office XP ), v11 (MS Office 2003) -->\n" "<!--[if gte mso 9]>\n"
    "<style>\n";
  STYLES += AStyleClassStrPrefix + "@page \n";
  STYLES += AStyleClassStrPrefix + "{\n";
  STYLES += AStylePropStrPrefix + "mso-page-orientation: ";
  STYLES += ndPageSetup->CmpAV("orientation", "0") ? "portrait" : "landscape";
  STYLES += ";\n";
  UnicodeString MSOMarginLeft, MSOMarginRight, MSOMarginTop, MSOMarginBottom;
  MSOMarginLeft   = DKFloatToStrF(StrToFloat(ndPageSetup->AV["leftfield"]) / 100 / 25.4, ffFixed, 7, 2, '.');
  MSOMarginRight  = DKFloatToStrF(StrToFloat(ndPageSetup->AV["rightfield"]) / 100 / 25.4, ffFixed, 7, 2, '.');
  MSOMarginTop    = DKFloatToStrF(StrToFloat(ndPageSetup->AV["topfield"]) / 100 / 25.4, ffFixed, 7, 2, '.');
  MSOMarginBottom = DKFloatToStrF(StrToFloat(ndPageSetup->AV["bottomfield"]) / 100 / 25.4, ffFixed, 7, 2, '.');
  STYLES += AStylePropStrPrefix + "margin              : " + MSOMarginTop + "in " + MSOMarginRight + "in " +
    MSOMarginBottom + "in " + MSOMarginLeft + "in;\n";
  STYLES += AStyleClassStrPrefix + "}\n";
  STYLES += "</style>\n" "<![endif]-->\n";
  // Spread sheet
  STYLES +=
    "<!-- Spread sheet for Microsoft Excel v9 (MS Office 2000), v10 (MS Office XP ), v11 (MS Office 2003) -->\n" "<!--[if gte mso 9]>\n"
    "<xml>\n" "  <x:ExcelWorkbook>\n" "    <x:ExcelWorksheets>\n" "      <x:ExcelWorksheet>\n"
    "        <x:WorksheetOptions>\n" "          <x:Print>\n" "            <x:ValidPrinterInfo/>\n"
    "            <x:PaperSizeIndex>" + IntToStr((int)DMPAPER_A4) +
    "</x:PaperSizeIndex>\n" "          </x:Print>\n" "        </x:WorksheetOptions>\n" "      </x:ExcelWorksheet>\n"
    "    </x:ExcelWorksheets>\n" "  </x:ExcelWorkbook>\n" "</xml>\n" "<![endif]-->";
  return STYLES;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ������� HTML-������ ��� �������������� ������ HTML-���������
 *
 *
 * ���������:
 *   [in]  AStyleClassStrPrefix - ������� ����� ������������� ������ HTML-�����
 *   [in]  AStylePropStrPrefix  - ������� ����� ��������
 *
 * ������������ ��������:
 *   �������� ������� HTML-������ � ���� ������
 *
 *
 */
UnicodeString __fastcall TICSDocViewer::GetTableHTMLSTYLESDefinition(UnicodeString AStyleClassStrPrefix,
  UnicodeString AStylePropStrPrefix)
 {
  UnicodeString STYLES = "<!-- Style sheet for tables -->\n" "<style type='text/css'>\n" "<!--\n";
  STYLES += AStyleClassStrPrefix + "table\n";
  STYLES += AStyleClassStrPrefix + "{\n";
  STYLES += AStylePropStrPrefix + "border-collapse: collapse;\n";
  STYLES += AStylePropStrPrefix + "border-style   : solid;\n";
  STYLES += AStylePropStrPrefix + "border-color   : black;\n";
  STYLES += AStyleClassStrPrefix + "}\n";
  STYLES += AStyleClassStrPrefix + "th, td\n";
  STYLES += AStyleClassStrPrefix + "{\n";
  STYLES += AStylePropStrPrefix + "border-collapse: collapse;\n";
  STYLES += AStylePropStrPrefix + "border-style   : solid;\n";
  STYLES += AStylePropStrPrefix + "border-color   : black;\n";
  STYLES += AStyleClassStrPrefix + "}\n";
  STYLES += AStyleClassStrPrefix + "th.num, td.num\n";
  STYLES += AStyleClassStrPrefix + "{\n";
  STYLES += AStylePropStrPrefix + "border-collapse: collapse;\n";
  STYLES += AStylePropStrPrefix + "border-style   : solid;\n";
  STYLES += AStylePropStrPrefix + "border-color   : black;\n";
  STYLES += AStylePropStrPrefix + "text-align     : " + FNumTCellAlign + ";\n";
  STYLES += AStylePropStrPrefix + "mso-number-format:\"\@\";"; /* change kab 20.06.07  ��� excel-� ��� ������ "�����" */
  STYLES += AStylePropStrPrefix + "vertical-align:top;\n"; /* change kab 20.06.07 ������������ ������������ */
  STYLES += AStylePropStrPrefix + "font-family    : \"" + FNumTCellFontName + "\";\n";
  STYLES += AStylePropStrPrefix + "font-size      : " + IntToStr((int) FNumTCellFontSize) + "pt;\n";
  STYLES += AStylePropStrPrefix + "text-decoration: none;\n";
  STYLES += AStylePropStrPrefix + "font-weight    : normal;\n";
  STYLES += AStylePropStrPrefix + "font-style     : normal;\n";
  STYLES += AStyleClassStrPrefix + "}\n";
  STYLES += "-->\n</style>";
  return STYLES;
 }
// ---------------------------------------------------------------------------
// ������������ HTML-����� � ���� ������
void __fastcall TICSDocViewer::CreateHTML(TICSDocViewerPreviewType APreviewType)
 {
  ChangeState(csCreateHTML);
  UnicodeString TITLE = "";
  UnicodeString META = "<meta name='Generator' content='" + ClassName() + " V" + About +
    "'>\n" "<meta name='Description' content='";
  switch (APreviewType)
   {
   case ptSpecificator:
    TITLE = FSpec->GetChildByName("passport")->AV["mainname"];
    META += "specificator view";
    break;
   case ptDocument:
    TITLE = FDoc->GetChildByName("passport")->AV["mainname"];
    META += "document view";
    break;
   }
  META += "'>";
  UnicodeString STYLES = "";
  if (FUseCSS || FUseWordCSS || FUseExcelCSS)
   META += "\n";
  if (FUseCSS)
   {
    STYLES += GetHTMLSTYLESDefinition("\t", "\t  ", FSpec->GetChildByName("styles", true)) + "\n" +
      GetTableHTMLSTYLESDefinition("\t", "\t  ");
    if (FUseWordCSS || FUseExcelCSS)
     STYLES += "\n";
   }
  if (FUseWordCSS)
   {
    STYLES += GetMSOWordHTMLSTYLESDefinition("\t", "\t  ", FSpec->GetChildByName("pagesetup", true));
    if (FUseExcelCSS)
     STYLES += "\n";
   }
  if (FUseExcelCSS)
   STYLES += GetMSOExcelHTMLSTYLESDefinition("\t", "\t  ", FSpec->GetChildByName("pagesetup", true));
  try
   {
    FndHTML->OnProgressInit     = CreateHTMLProgressInit;
    FndHTML->OnProgress         = CreateHTMLProgressChange;
    FndHTML->OnProgressComplite = CreateHTMLProgressComplite;
    FHTML                       = FndHTML->AsHTML(TITLE, META + STYLES, false, 2, false);
    if (FUseExcelCSS)
     FHTML = StringReplace(FHTML, "<html>",
      "<html\n" "  xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n"
      "  xmlns:x=\"urn:schemas-microsoft-com:office:excel\"\n" "  xmlns=\"http://www.w3.org/TR/REC-html40\"\n" ">",
      TReplaceFlags() << rfIgnoreCase);
   }
  __finally
   {
    FndHTML->OnProgressInit     = NULL;
    FndHTML->OnProgress         = NULL;
    FndHTML->OnProgressComplite = NULL;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::CreateHTMLProgressInit(int AMax, UnicodeString AMsg)
 {
  InitPB();
  SetPBMax(AMax);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::CreateHTMLProgressChange(int APercent, UnicodeString AMsg)
 {
  SetPBPosition(APercent);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::CreateHTMLProgressComplite(UnicodeString AMsg)
 {
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����� ��� �������� ������ ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in]      ndListEl - xml-��� "listcont" �������������
 *   [in\out]  htmlList - ������ ( html-��� "table", ��������������� xml-���� "list" ������������� )
 *   [int]     htmlRow  - ������� ������ html-������� ( html-��� "tr" )
 *                        ������������ ��� ������ ������� ����� ������������ ��������, ��� ������
 *                        ������� ������ ���� NULL
 *                        [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *
 */
void __fastcall TICSDocViewer::CreateHTMLListHeaderCell(TTagNode * ndListEl, TTagNode * htmlList, TTagNode * htmlRow)
 {
  if (CheckIfTerminated())
   return;
  if (TICSDocSpec::IsListCol(ndListEl))
   {
    TAttr * attUnion = TICSDocSpec::GetListColGroupUnion(ndListEl);
    // �������� ������-���������
    TTagNode * htmlTH = htmlRow->AddChild("th");
    // ��������� ������ ����� � ������ �������
    if (TICSDocSpec::IsListColSingle1(ndListEl) || TICSDocSpec::IsListColSingle2(ndListEl))
     SetHTMLBoxWidthValues(htmlTH, __BorderWidth, TICSDocSpec::GetSizeAttrVal(ndListEl, "width"));
    else if (attUnion && (attUnion->Value == "1"))
     {
      int nWidth = 0;
      bool bNeedWidth = false;
      if (TICSDocSpec::IsListColLinkISGroup(ndListEl))
       {
        UnicodeString AWidth = ndListEl->AV["width"];
        if (AWidth != "")
         {
          TStringList * sl = NULL;
          try
           {
            sl       = new TStringList;
            sl->Text = StringReplace(AWidth, ";", "\n", TReplaceFlags() << rfReplaceAll);
            for (int i = 0; i < sl->Count; i++)
             if (sl->Strings[i] != "")
              {
               nWidth += sl->Strings[i].ToInt();
               bNeedWidth = true;
              }
           }
          __finally
           {
            if (sl)
             delete sl;
           }
         }
       }
      else
       {
        TTagNode * ndLCol = ndListEl->GetFirstChild();
        while (ndLCol)
         {
          UnicodeString AWidth = ndLCol->AV["width"];
          if (AWidth != "")
           {
            nWidth += AWidth.ToInt();
            bNeedWidth = true;
           }
          ndLCol = ndLCol->GetNext();
         }
       }
      if (bNeedWidth)
       SetHTMLBoxWidthValues(htmlTH, __BorderWidth, IntToStr((int)nWidth));
      else
       SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
     }
    else
     SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
    TTagNode * htmlTHData = CreateHTMLTextFormat(htmlTH, GetListCellStyle("hstyle", ndListEl));
    TAttr * attPCDATA = htmlTHData->AddAttr("PCDATA", GetListColHeaderText(ndListEl));
    attPCDATA->Ref = ndListEl;
    // ����������� "rowspan" ��� ��������� ������
    if (TICSDocSpec::IsListColSingle(ndListEl))
     {
      int nListElLevel = ndListEl->Level;
      if (TICSDocSpec::IsListColSingle2(ndListEl))
       nListElLevel-- ;
      int nListColMaxLevel = GetListColsMaxLevel(ndListEl->GetParent("listcont"));
      int nMaxDepth = nListColMaxLevel - nListElLevel + 1;
      if (nMaxDepth > 1)
       htmlTH->AV["rowspan"] = IntToStr((int) nMaxDepth);
      IncPB();
     }
    // ����������� "colspan" ��� ��������� ������
    else if (attUnion && (attUnion->Value == "0"))
     htmlTH->AV["colspan"] = IntToStr((int) CountTagNodes(ndListEl, TICSDocSpec::IsListColSingle));
    if (attUnion && (attUnion->Value == "1"))
     return;
   }
  TTagNode * ndChild = ndListEl->GetFirstChild();
  while (ndChild)
   {
    // ���������� htmlRow ��� ������ ������� ������ ��� ����� ������ �������
    if (TICSDocSpec::IsListFirstCol(ndChild))
     {
      int nRowInd = ndChild->Level - ndChild->GetParent("listcont")->Level;
      if (TICSDocSpec::IsListColSingle2(ndChild))
       nRowInd-- ;
      int i = 0;
      TTagNode * htmlTR = htmlList->GetFirstChild();
      while (htmlTR && (++i < nRowInd))
       htmlTR = htmlTR->GetNext();
      // ����������� ����� ������ ��� ������������ ������������
      htmlRow = (htmlTR) ? htmlTR : htmlList->AddChild("tr");
     }
    CreateHTMLListHeaderCell(ndChild, htmlList, htmlRow);
    ndChild = ndChild->GetNext();
   }
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����� ��� �������� ������ ( xml-��� "list" ������������� )
 * � ����������� ������ "��������" ��������
 *
 *
 * ���������:
 *   [in]      ndListCont      - xml-��� "listcont" �������������
 *   [in\out]  htmlList        - ������ ( html-��� "table", ��������������� xml-���� "list" ������������� )
 *   [out]     AListSingleCols - ������ "��������" ��������
 *                               [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *     __RowNumbers
 *     __BorderWidth
 *
 */
void __fastcall TICSDocViewer::CreateHTMLListHeader(TTagNode * ndListCont, TTagNode * htmlList,
  TTagNodeList * AListSingleCols)
 {
  if (CheckIfTerminated())
   return;
  CreateHTMLListHeaderCell(ndListCont, htmlList);
  // ���������� ��������� ��� ������� � �������� ����� ������
  if (__RowNumbers)
   {
    TTagNode * htmlFirstRow = htmlList->GetFirstChild();
    TTagNode * htmlTH = htmlFirstRow->InsertChild(htmlFirstRow->GetFirstChild(), "th", true);
    SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
    TTagNode * htmlTHData = CreateHTMLTextFormat(htmlTH, GetListStyle("hstyle", ndListCont->GetParent("list")));
    htmlTHData->AV["PCDATA"] = FNumTCellLTitle;
    // ����������� "rowspan" ��� ��������� ������
    int nMaxDepth = GetListColsMaxLevel(ndListCont) - ndListCont->Level;
    if (nMaxDepth > 1)
     htmlTH->AV["rowspan"] = IntToStr((int) nMaxDepth);
   }
  TTagNodeList ListSingleCols;
  if (AListSingleCols)
   {
    TICSDocSpec::GetListSingleColsList(ndListCont, & ListSingleCols);
    *AListSingleCols = ListSingleCols;
   }
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ������ ��� ������ ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in]      AListSingleCols - ������ "��������" ��������
 *   [in\out]  htmlList        - ������ ( html-��� "table", ��������������� xml-���� "list" ������������� )
 *   [out]     ADataCells      - ������ html-����� "td" html-������� - ������ � �������
 *                               [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *
 */
void __fastcall TICSDocViewer::CreateHTMLListBodyRow(const TTagNodeList * AListSingleCols, TTagNode * htmlList,
  TTagNodeList * ADataCells)
 {
  if (ADataCells)
   ADataCells->clear();
  TTagNode * htmlRow = htmlList->AddChild("tr");
  // ���������� ������ ��� ������ ������ ������
  if (__RowNumbers)
   {
    TTagNode * htmlTD = htmlRow->AddChild("td");
    SetHTMLTableCellBorderWidth(htmlTD, __BorderWidth);
    TTagNode * htmlTDData = CreateHTMLTableNumCellTextFormat(htmlTD);
    TAttr * pcdata = htmlTDData->AddAttr("PCDATA", "&nbsp;");
    pcdata->Ref = NULL;
    if (ADataCells)
     ADataCells->push_back(htmlTDData);
   }
  for (TTagNodeList::const_iterator i = AListSingleCols->begin(); i != AListSingleCols->end(); i++)
   {
    if (CheckIfTerminated())
     return;
    TTagNode * htmlTD = htmlRow->AddChild("td");
    SetHTMLTableCellBorderWidth(htmlTD, __BorderWidth);
    TTagNode * htmlTDData = CreateHTMLTextFormat(htmlTD, GetListCellStyle("bstyle", *i));
    TAttr * pcdata = htmlTDData->AddAttr("PCDATA", "&nbsp;");
    pcdata->Ref = *i;
    if (ADataCells)
     ADataCells->push_back(htmlTDData);
   }
  IncPB();
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ������-��������� ��� ������� ( xml-��� "table" ������������� )
 * ( ������������� ����������� ��������� ����� html-�������, ���� ������� ���������� )
 *
 *
 * ���������:
 *   [in]  ACellTagNodeName - ��� �������������� xml-����� ( "tcol" ��� "trow" )
 *   [in]  ndCellTagNode    - xml-���, ��������������� ������-��������� �������
 *   [int] htmlRow          - ������� ������ html-������� ( html-��� "tr" )
 *
 * ������������ ��������:
 *   ������-��������� ( html-��� "th" ) �������
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *      __RowNum
 *
 */
TTagNode * __fastcall TICSDocViewer::CreateHTMLTableHeaderCell(UnicodeString ACellTagNodeName, TTagNode * ndCellTagNode,
  TTagNode * htmlRow)
 {
  if (CheckIfTerminated())
   return NULL;
  TTagNode * htmlTH = NULL;
  if (ndCellTagNode->CmpName(ACellTagNodeName))
   {
    // ���������� ������ ��������� �������/������
    htmlTH = htmlRow->AddChild("th");
    // ��������� ������ ����� � ������ �������
    if ((ACellTagNodeName == "tcol") && !ndCellTagNode->GetChildByName("tcol"))
     SetHTMLBoxWidthValues(htmlTH, __BorderWidth, TICSDocSpec::GetSizeAttrVal(ndCellTagNode, "width"));
    else
     SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
    TTagNode * htmlTHData = CreateHTMLTextFormat(htmlTH, GetTableCellStyle("hstyle", ndCellTagNode));
    TAttr * attPCDATA = htmlTHData->AddAttr("PCDATA", GetTableHeaderCellText(ndCellTagNode));
    attPCDATA->Ref = ndCellTagNode;
    // ������������ ��� ���������� ����� ��� ����� ������� � ������� HTML
    ndCellTagNode->Ctrl = reinterpret_cast<TWinControl *>(htmlRow);
    // ������ �������/�����
    if (ndCellTagNode->GetChildByName(ACellTagNodeName))
     {
      int nTableSingleElCount;
      UnicodeString SpanAttrName;
      if (ACellTagNodeName == "tcol")
       {
        nTableSingleElCount = CountTagNodes(ndCellTagNode, TICSDocSpec::IsTableColSingle);
        SpanAttrName        = "colspan";
       }
      else // trow
       {
        nTableSingleElCount = CountTagNodes(ndCellTagNode, TICSDocSpec::IsTableRowSingle);
        SpanAttrName        = "rowspan";
       }
      htmlTH->AV[SpanAttrName] = IntToStr((int) nTableSingleElCount);
     }
    // �������/������
    else
     {
      UnicodeString ElTopParentName, SpanAttrName;
      if (ACellTagNodeName == "tcol")
       {
        ElTopParentName = "columns";
        SpanAttrName    = "rowspan";
       }
      else
       {
        ElTopParentName = "rows";
        SpanAttrName    = "colspan";
       }
      int nTableElMaxLevel = GetTagNodesMaxLevel(ndCellTagNode->GetParent(ElTopParentName), ACellTagNodeName);
      int nMaxDepth = nTableElMaxLevel - ndCellTagNode->Level + 1;
      if (nMaxDepth > 1)
       htmlTH->AV[SpanAttrName] = IntToStr((int) nMaxDepth);
      // ���������� ������ � ������� ������
      if ((ACellTagNodeName == "trow") && __RowNumbers)
       {
        TTagNode * htmlTH = htmlRow->AddChild("th");
        SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
        TTagNode * htmlTHData = CreateHTMLTableNumCellTextFormat(htmlTH);
        UnicodeString S;
        int nSingleRowCount = CountTagNodes(ndCellTagNode->GetParent("rows"), TICSDocSpec::IsTableRowSingle);
        S.printf(("%0" + IntToStr((int) IntToStr((int) nSingleRowCount).Length()) + "u").c_str(), __RowNum++);
        htmlTHData->AV["PCDATA"] = S;
       }
     }
    IncPB();
   }
  return htmlTH;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����� ��� �������� ������� ( xml-��� "table" ������������� )
 *
 *
 * ���������:
 *   [in]      ndCol     - xml-��� "columns" �������������
 *   [in\out]  htmlTable - ������� ( html-��� "table", ��������������� xml-���� "table" ������������� )
 *   [int]     htmlRow   - ������� ������ html-������� ( html-��� "tr" )
 *                         ������������ ��� ������ ������� ����� ������������ ��������, ��� ������
 *                         ������� ������ ���� NULL
 *                         [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *      __ColNumbers
 *
 */
void __fastcall TICSDocViewer::CreateHTMLTableHeaderCols(TTagNode * ndCol, TTagNode * htmlTable, TTagNode * htmlRow)
 {
  if (CheckIfTerminated())
   return;
  CreateHTMLTableHeaderCell("tcol", ndCol, htmlRow);
  TTagNode * ndChild = ndCol->GetChildByName("tcol");
  while (ndChild && ndChild->CmpName("tcol"))
   {
    // ��� ������ ������� � ������ ( ��� ��� ����� ������ ������� )
    if (TICSDocSpec::IsTableFirstCol(ndChild))
     {
      // ��� ����� ������ �������
      if (ndChild->GetParent()->CmpName("columns"))
       {
        // ���������� ����� ������ ��� HTML-�������
        htmlRow = htmlTable->AddChild("tr");
        // ���������� ����� ����� ������� ������ ����� ������� ( ��� ������ �������� ������ )
        TTagNode * htmlTH = htmlRow->AddChild("th");
        SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
        TTagNode * ndTableSection = ndChild->GetParent("table")->GetChildByName("columns");
        int nTableElMaxLevel = GetTagNodesMaxLevel(ndTableSection, "tcol");
        htmlTH->AV["rowspan"] = nTableElMaxLevel - ndTableSection->Level +static_cast<int>(__ColNumbers);
        ndTableSection        = ndChild->GetParent("table")->GetChildByName("rows");
        nTableElMaxLevel      = GetTagNodesMaxLevel(ndTableSection, "trow");
        htmlTH->AV["colspan"] = nTableElMaxLevel - ndTableSection->Level +static_cast<int>(__RowNumbers);
        htmlTH->AV["PCDATA"]  = "&nbsp;";
       }
      // ���������� htmlRow
      else
       {
        int nRowInd = ndChild->Level - ndChild->GetParent("columns")->Level;
        int i = 0;
        TTagNode * htmlTR = htmlTable->GetFirstChild();
        while (htmlTR && (++i < nRowInd))
         htmlTR = htmlTR->GetNext();
        htmlRow = (htmlTR) ? htmlTR : htmlTable->AddChild("tr");
       }
     }
    CreateHTMLTableHeaderCols(ndChild, htmlTable, htmlRow);
    ndChild = ndChild->GetNext();
   }
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����� ��� �������� ������� ( xml-��� "table" ������������� ) � ���������� ��������
 *
 *
 * ���������:
 *   [in]      ndColumns         - xml-��� "columns" �������������
 *   [in\out]  htmlTable         - ������� ( html-��� "table", ��������������� xml-���� "table" ������������� )
 *   [out]     ATableSingleCols  - ������ "��������"-����� ����� ��� ��������
 *                                 ������� ( xml-��� "table" ������������� ), �.�. ����� �����,
 *                                 � ������� ��� ����������� ��� �������� �����-����������
 *                                 [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __ColNumbers
 *      __ColNum
 *
 */
void __fastcall TICSDocViewer::CreateHTMLTableCols(TTagNode * ndColumns, TTagNode * htmlTable,
  TTagNodeList * ATableSingleCols)
 {
  if (CheckIfTerminated())
   return;
  CreateHTMLTableHeaderCols(ndColumns, htmlTable);
  TTagNodeList TableSingleCols;
  TICSDocSpec::GetTableSingleColsList(ndColumns, & TableSingleCols);
  // ���������� ����� � �������� ��������
  if (__ColNumbers)
   {
    TTagNode * htmlRow = htmlTable->AddChild("tr");
    for (TTagNodeList::const_iterator i = TableSingleCols.begin(); i != TableSingleCols.end(); i++)
     {
      TTagNode * htmlTH = htmlRow->AddChild("th");
      SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
      TTagNode * htmlTHData = CreateHTMLTableNumCellTextFormat(htmlTH);
      UnicodeString S;
      S.printf(("%0" + IntToStr(IntToStr((int) TableSingleCols.size()).Length()) + "u").c_str(), __ColNum++);
      htmlTHData->AV["PCDATA"] = S;
     }
   }
  if (ATableSingleCols)
   * ATableSingleCols = TableSingleCols;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����� ��� ����� ������� ( xml-��� "table" ������������� )
 * � ����� ������ � ���� �������
 *
 *
 * ���������:
 *   [in]     ndRow            - xml-��� "rows" �������������
 *   [in\out] htmlTable        - ������� ( html-��� "table", ��������������� xml-���� "table" ������������� )
 *   [in]     ATableSingleCols - ������ "��������"-����� ����� ��� ��������
 *                               ������� ( xml-��� "table" ������������� ), �.�. ����� �����,
 *                               � ������� ��� ����������� ��� �������� �����-����������
 *   [out]    DataCells        - ������ ������� ( ���� 2-������� ������� ) ���������� �� ������
 *                               ������ �������
 *   [int]    htmlRow          - ������� ������ html-������� ( html-��� "tr" )
 *                               ������������ ��� ������ ������� ����� ������������ ��������, ��� ������
 *                               ������� ������ ���� NULL
 *                               [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *
 */
void __fastcall TICSDocViewer::CreateHTMLTableRows(TTagNode * ndRow, TTagNode * htmlTable,
  const TTagNodeList & ATableSingleCols, TTagNodeList2 * DataCells, TTagNode * htmlRow)
 {
  if (CheckIfTerminated())
   return;
  if (CreateHTMLTableHeaderCell("trow", ndRow, htmlRow) && !ndRow->GetChildByName("trow"))
   {
    // ���������� ����� ��� ������
    TTagNodeList DataCellStr;
    for (TTagNodeList::const_iterator i = ATableSingleCols.begin(); i != ATableSingleCols.end(); i++)
     {
      TTagNode * htmlTD = htmlRow->AddChild("td");
      SetHTMLTableCellBorderWidth(htmlTD, __BorderWidth);
      TTagNode * htmlTDData = CreateHTMLTextFormat(htmlTD, GetTableCellStyle("bstyle", *i, ndRow));
      htmlTDData->AV["PCDATA"] = "&nbsp;";
      if (DataCells)
       DataCellStr.push_back(htmlTDData);
     }
    if (DataCells)
     DataCells->push_back(DataCellStr);
   }
  TTagNode * ndChild = ndRow->GetChildByName("trow");
  while (ndChild && ndChild->CmpName("trow"))
   {
    // ���������� htmlRow
    if (!htmlRow || !TICSDocSpec::IsTableFirstRow(ndChild))
     htmlRow = htmlTable->AddChild("tr");
    else if (TICSDocSpec::IsTableFirstRow(ndChild))
     htmlRow = reinterpret_cast<TTagNode *>(ndChild->GetParent()->Ctrl);
    CreateHTMLTableRows(ndChild, htmlTable, ATableSingleCols, DataCells, htmlRow);
    ndChild = ndChild->GetNext();
   }
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����������� ������ ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in]  ndList     - xml-��� "list" �������������
 *   [in]  htmlParent - html-��� - ������ ��� ��������������� html-������
 *   [in]  nRowCount  - ���������� ����� � ����������� ������
 *                      [default = 1]
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������ ( xml-��� "list" ������������� )
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __Width
 *      __RowNumbers
 *
 */
TTagNode * __fastcall TICSDocViewer::AddHTMLList(TTagNode * ndList, TTagNode * htmlParent,
  TTagNodeList * AListSingleCols, int nRowCount)
 {
  __BorderWidth = ndList->AV["border"];
  __Width       = ndList->AV["width"];
  __RowNumbers  = ndList->CmpAV("rownumbers", 1);
  TTagNode * htmlList = AddHTMLTableTag(htmlParent);
  TTagNode * ndListCont = ndList->GetChildByName("listcont");
  TTagNodeList ListSingleCols;
  CreateHTMLListHeader(ndListCont, htmlList, & ListSingleCols);
  for (int i = 0; i < nRowCount; i++)
   {
    if (CheckIfTerminated())
     break;
    CreateHTMLListBodyRow(& ListSingleCols, htmlList);
   }
  if (AListSingleCols)
   * AListSingleCols = ListSingleCols;
  return htmlList;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����������� ������� ( xml-��� "table" ������������� )
 *
 *
 * ���������:
 *   [in]  ndTable    - xml-��� "table" �������������
 *   [in]  htmlParent - html-��� - ������ ��� �������������� html-�������
 *   [out] DataCells  - ������ ������� ( ���� 2-������� ������� ) ���������� �� ������
 *                      ������ �������
 *                      [default = NULL]
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������� ( xml-��� "table" ������������� )
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *      __ColNumbers
 *      __RowNum
 *      __ColNum
 *
 */
TTagNode * __fastcall TICSDocViewer::AddHTMLTable(TTagNode * ndTable, TTagNode * htmlParent, TTagNodeList2 * DataCells)
 {
  __BorderWidth = ndTable->AV["border"];
  __Width       = ndTable->AV["width"];
  __RowNumbers  = ndTable->CmpAV("rownumbers", 1);
  __ColNumbers  = ndTable->CmpAV("colnumbers", 1);
  __RowNum      = 1;
  __ColNum      = 1;
  TTagNode * htmlTable = AddHTMLTableTag(htmlParent);
  TTagNodeList TableSingleCols;
  CreateHTMLTableCols(ndTable->GetChildByName("columns"), htmlTable, & TableSingleCols);
  if (DataCells)
   DataCells->clear();
  CreateHTMLTableRows(ndTable->GetChildByName("rows"), htmlTable, TableSingleCols, DataCells);
  return htmlTable;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����������� ������� ( xml-��� "inscription" ������������� )
 *
 *
 * ���������:
 *   [in]  ndInscription - xml-��� "inscription" �������������
 *   [in]  htmlParent    - html-��� - ������ ��� ��������������� �����������
 *
 * ������������ ��������:
 *   html-���, ��������������� ����������� ������� ( xml-��� "inscription" ������������� )
 *
 */
TTagNode * __fastcall TICSDocViewer::AddHTMLInscription(TTagNode * ndInscription, TTagNode * htmlParent)
 {
  if (ndInscription->CmpAV("newline", "1"))
   htmlParent->AddChild("br");
  return htmlParent->AddChild("div");
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����������� �������� ������� ( xml-��� "instext" ������������� )
 *
 *
 * ���������:
 *   [in]     ndInsText  - xml-��� "instext" �������������
 *   [in\out] htmlParent - html-��� - ������ ��� ��������������� �����������
 *
 * ������������ ��������:
 *   html-���, ��������������� ����������� �������� ������� ( xml-��� "instext" ������������� )
 *
 */
TTagNode * __fastcall TICSDocViewer::AddHTMLInsText(TTagNode * ndInsText, TTagNode ** htmlParent)
 {
  if (ndInsText->CmpAV("newline", "1"))
   * htmlParent = (*htmlParent)->AddChild("div");
  UnicodeString AlignAttrName, AlignAttrVal;
  GetHTML_DIVAlignAttrVal(GetInsTextAlign(ndInsText), AlignAttrName, AlignAttrVal);
  (*htmlParent)->AV[AlignAttrName] = AlignAttrVal;
  TTagNode * htmlInsText = (*htmlParent)->AddChild("span");
  return CreateHTMLTextFormat(htmlInsText, GetInsTextStyle(ndInsText));
 }
// ---------------------------------------------------------------------------
// ������������ ����������� ������������� � ������� HTML
void __fastcall TICSDocViewer::CreateSpecHTML()
 {
  ChangeState(csCreateHTMLTags);
  SetPBMax(GetMaxPBForSpec());
  TTagNode * htmlBodyRoot = InitNDHTML();
  TTagNode * ndDocElement = FSpec->GetChildByName("doccontent")->GetFirstChild();
  while (ndDocElement)
   {
    if (CheckIfTerminated())
     return;
    if (ndDocElement->CmpName("inscription")) // �������
     {
      TTagNode * htmlInsTextCurParent = AddHTMLInscription(ndDocElement, htmlBodyRoot);
      TTagNode * ndInsText = ndDocElement->GetFirstChild();
      while (ndInsText) // ������� �������
       {
        if (CheckIfTerminated())
         return;
        TTagNode * htmlInsText = AddHTMLInsText(ndInsText, &htmlInsTextCurParent);
        UnicodeString InsViewText = TICSDocSpec::GetInsTagViewText(ndInsText, GetXMLContainer());
        if (InsViewText != csEmptyInsText)
         htmlInsText->AV["PCDATA"] = InsViewText;
        ndInsText = ndInsText->GetNext();
        IncPB();
       }
     }
    else if (ndDocElement->CmpName("list")) // ������
       AddHTMLList(ndDocElement, htmlBodyRoot);
    else if (ndDocElement->CmpName("table")) // �������
       AddHTMLTable(ndDocElement, htmlBodyRoot);
    htmlBodyRoot->AddChild("br");
    ndDocElement = ndDocElement->GetNext();
    IncPB();
   }
  CreateHTML(ptSpecificator);
 }
// ---------------------------------------------------------------------------
// ������������ ����������� ��������� � ������� HTML
void __fastcall TICSDocViewer::CreateDocHTML()
 {
  ChangeState(csCreateHTMLTags);
  SetPBMax(GetMaxPBForDoc());
  TTagNode * htmlBodyRoot = InitNDHTML();
  TTagNode * ndObj = FDoc->GetChildByName("contens")->GetFirstChild();
  while (ndObj)
   {
    if (CheckIfTerminated())
     return;
    // ��������� ����� "lv" ������� ������ - ��������� ���������
    TTagNode * ndObjRef = FSpec->GetTagByUID(ndObj->AV["uidmain"]);
    if (ndObjRef->CmpName("inscription")) // �������
     {
      TTagNode * htmlInsTextCurParent = AddHTMLInscription(ndObjRef, htmlBodyRoot);
      TTagNode * ndInsElement = ndObj->GetFirstChild();
      while (ndInsElement)
       {
        if (CheckIfTerminated())
         return;
        // ��������� ����� "lv" ������� ������ - ��������� �������
        TTagNode * ndInsElementRef = FSpec->GetTagByUID(ndInsElement->AV["uidmain"]);
        TTagNode * htmlInsText = AddHTMLInsText(ndInsElementRef, &htmlInsTextCurParent);
        UnicodeString InsViewText = ndInsElementRef->AV["value"];
        UnicodeString RefText = ndInsElement->GetFirstChild()->AV["val"];
        UnicodeString CntClcRlsText = ndInsElement->GetFirstChild()->GetNext()->AV["val"];
        if (RefText != "")
         {
          if (InsViewText != "")
           InsViewText += " ";
          InsViewText += RefText;
         }
        if (CntClcRlsText != "")
         {
          if (InsViewText != "")
           InsViewText += " ";
          InsViewText += CntClcRlsText;
         }
        if (InsViewText != "")
         htmlInsText->AV["PCDATA"] = InsViewText;
        ndInsElement = ndInsElement->GetNext();
        IncPB();
       }
     }
    else if (ndObjRef->CmpName("list")) // ������
     {
      TTagNodeList ListSingleCols;
      TTagNode * htmlList = AddHTMLList(ndObjRef, htmlBodyRoot, &ListSingleCols, 0);
      TTagNode * ndListRow = ndObj->GetFirstChild();
      int nRowNo = 1;
      while (ndListRow)
       {
        if (CheckIfTerminated())
         return;
        // ��������� ����� "lv" ������� ������ - ����� ������
        TTagNodeList DataCells;
        CreateHTMLListBodyRow(& ListSingleCols, htmlList, & DataCells);
        TTagNodeList::iterator itListCell = DataCells.begin();
        // ���������� ������ ������� ������
        if (__RowNumbers)
         {
          UnicodeString RowNo = IntToStr((int) nRowNo);
          RowNo = UnicodeString::StringOfChar('0', IntToStr((int) ndObj->Count).Length() - RowNo.Length()) + RowNo;
          (*itListCell)->AV["PCDATA"] = RowNo;
          itListCell++ ;
         }
        TTagNode * ndListData = ndListRow->GetFirstChild();
        while (ndListData)
         {
          if (CheckIfTerminated())
           return;
          // ��������� ����� "lv", "iv" �������� ������ - ����� ������� ������ ������
          if (ndListData->CmpName("iv")) // ������� �������� �������� ��� ������� ����������
           { // ��� ��������� ��������
            TTagNode * ndListCol = reinterpret_cast<TTagNode *>((*itListCell)->GetAttrByName("PCDATA")->Ref);
            UnicodeString CellText = GetDocListCellText(ndListData, ndListCol, ndObjRef->CmpAV("showzval", "1"));
            if (CellText != "")
             (*itListCell)->AV["PCDATA"] = CellText;
           }
          else // �������� ��������� ��������
           {
            TTagNodeList::iterator itListCellInit = itListCell;
            TTagNode * ndListLinkISRows = ndListData->GetFirstChild();
            if (ndListLinkISRows)
             {
              while (ndListLinkISRows)
               {
                if (CheckIfTerminated())
                 return;
                // ��������� ����� "lv" ���������� ������ - ������ ��������� ��������� ��������
                itListCell = itListCellInit;
                TTagNode * ndListLinkISData = ndListLinkISRows->GetFirstChild();
                while (ndListLinkISData)
                 {
                  if (CheckIfTerminated())
                   return;
                  // ��������� ����� "iv" ������ ������ - ������ ����� ��������� ��������� ��������
                  TAttr * attPCDATA = (*itListCell)->GetAttrByName("PCDATA");
                  TTagNode * ndListCol = reinterpret_cast<TTagNode *>(attPCDATA->Ref);
                  UnicodeString CellText = GetDocListCellText(ndListLinkISData, ndListCol,
                    ndObjRef->CmpAV("showzval", "1"));
                  // if ( CellText != "" )
                  // { /*BUGFIX kab 20.06.07 - ���������� ������ �������� � ������*/
                  attPCDATA->Value = "";
                  if ((* itListCell)->Count)
                   (* itListCell)->AddChild("br");
                  (*itListCell)->AddChild("span")->AV["PCDATA"] = CellText;
                  // }
                  itListCell++ ;
                  ndListLinkISData = ndListLinkISData->GetNext();
                 }
                ndListLinkISRows = ndListLinkISRows->GetNext();
               }
             }
            else
             {
              TTagNode * ndListLinkISRef = FSpec->GetTagByUID(ndListData->AV["uidmain"]);
              if (ndListLinkISRef)
               {
                if (CheckIfTerminated())
                 return;
                // ��������� ����� "lv" ���������� ������ - ������ ��������� ��������� ��������
                itListCell = itListCellInit;
                TTagNode * ndListISAttrRef = ndListLinkISRef->GetFirstChild();
                if (ndListISAttrRef)
                 ndListISAttrRef = ndListISAttrRef->GetFirstChild();
                while (ndListISAttrRef)
                 {
                  if (CheckIfTerminated())
                   return;
                  if (ndListISAttrRef->CmpName("is_attr"))
                   {
                    // ��������� ����� "iv" ������ ������ - ������ ����� ��������� ��������� ��������
                    TAttr * attPCDATA = (*itListCell)->GetAttrByName("PCDATA");
                    attPCDATA->Value = "";
                    (* itListCell)->AddChild("span", "PCDATA=");
                    itListCell++ ;
                   }
                  ndListISAttrRef = ndListISAttrRef->GetNext();
                 }
               }
             }
            if (itListCell != itListCellInit)
             itListCell-- ;
           }
          itListCell++ ;
          ndListData = ndListData->GetNext();
         }
        ndListRow = ndListRow->GetNext();
        nRowNo++ ;
       }
     }
    else if (ndObjRef->CmpName("table")) // �������
     {
      TTagNodeList2 DataCells;
      AddHTMLTable(ndObjRef, htmlBodyRoot, & DataCells);
      TTagNode * ndTableRow = ndObj->GetFirstChild();
      TTagNodeList2::iterator itDataCells = DataCells.begin();
      while (ndTableRow)
       {
        if (CheckIfTerminated())
         return;
        // ��������� ����� "lv" ������� ������ - ����� �������
        TTagNode * ndTableData = ndTableRow->GetFirstChild();
        TTagNodeList::iterator itDataCellStr = (*itDataCells).begin();
        while (ndTableData)
         {
          if (CheckIfTerminated())
           return;
          // ��������� ����� "iv" - ����� ������ �������
          UnicodeString CellText = ndTableData->AV["val"];
          // ������ ����������� ������� ��������
          NormalizeNumericalValue(CellText, ndObjRef->CmpAV("showzval", "1"));
          if (CellText != "")
           (*itDataCellStr)->AV["PCDATA"] = CellText;
          ndTableData = ndTableData->GetNext();
          itDataCellStr++ ;
         }
        ndTableRow = ndTableRow->GetNext();
        itDataCells++ ;
        IncPB();
       }
     }
    htmlBodyRoot->AddChild("br");
    ndObj = ndObj->GetNext();
    IncPB();
   }
  CreateHTML(ptDocument);
 }
// ---------------------------------------------------------------------------
// ������������� ����� �������� ������� ���������������� ��������� ��� ��������������� ��������� � ������� HTML
void __fastcall TICSDocViewer::InitHTMLPreview()
 {
  // TfmHTMLPreview *dlg;
  WideString URL;
  bool bOnFly;
  __CanShowPreviewDlg = false;
  switch (FMode)
   {
   case vmOnFly:
    bOnFly = true;
    URL = FHTML;
    break;
   case vmSaveToDisk:
    bOnFly = false;
    wchar_t szTmpFileName[MAX_PATH];
    UnicodeString HTMLTmpFileName =
      AppendFileName(FTmpPath, "idv" + TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss") + ".htm");
    FTmpFileNames.push_back(HTMLTmpFileName);
    TFileStream * fs = NULL;
    try
     {
      fs = new TFileStream(HTMLTmpFileName, fmCreate | fmShareExclusive);
      fs->Seek(0, soFromBeginning);
      AnsiStringT<1251>tmpFLR = AnsiStringT<1251>(FHTML);
      fs->Write(tmpFLR.c_str(), tmpFLR.Length());
     }
    __finally
     {
      if (fs)
       delete fs;
     }
    URL = HTMLTmpFileName;
    break;
   }
  __CanShowPreviewDlg = true;
  __URL   = URL;
  __OnFly = bOnFly;
 }
// ---------------------------------------------------------------------------
// ������ ������� ���������������� ��������� ��� ��������������� ��������� � ������� HTML
void __fastcall TICSDocViewer::HTMLPreview()
 {
  if (FPreviewDlgKind == pmInternalDlg)
   {
    TPreviewForm * dlg = NULL;
    TStringList * FDoc = NULL;
    try
     {
      FDoc               = new TStringList;
      dlg                = new TPreviewForm(this);
      dlg->OnDemoMessage = FPPDemoMsg;
      FDoc->LoadFromFile(__URL);
      TTagNode * ndPageSetup = FSpec->GetChildByName("pagesetup", true);
      if (!ndPageSetup->AV["prk"].Length())
       ndPageSetup->AV["prk"] = "1";
      TPreviewParam param = TPreviewParam(ndPageSetup->AV["topfield"].ToIntDef(0) / 100.,
        ndPageSetup->AV["leftfield"].ToIntDef(0) / 100., ndPageSetup->AV["rightfield"].ToIntDef(0) / 100.,
        ndPageSetup->AV["bottomfield"].ToIntDef(0) / 100., StrToFloat(ndPageSetup->AV["prk"]),
        (Printers::TPrinterOrientation)ndPageSetup->AV["orientation"].ToIntDef(0),
        (ndPageSetup->AV["orientation"].ToIntDef(0)) ? pzHeight : pzWidth);
      dlg->Preview(FDoc, param);
     }
    __finally
     {
      if (dlg)
       delete dlg;
      if (FDoc)
       delete FDoc;
     }
    /*
     TfmHTMLPreview *dlg;
     try
     {
     dlg = new TfmHTMLPreview( this, __URL, __OnFly, FDefSavePath );
     TTagNode *ndPageSetup = FSpec->GetChildByName( "pagesetup", true );
     FIESetup->SetIEPageSetup( ndPageSetup->AV["bottomfield"],
     ndPageSetup->AV["leftfield" ],
     ndPageSetup->AV["rightfield" ],
     ndPageSetup->AV["topfield"]
     );
     dlg->Icon = ( FPreviewDlgIcon->Handle ) ? FPreviewDlgIcon : FDefPreviewDlgIcon;
     dlg->ShowModal();
     FIESetup->RestoreIEPageSetup();
     }
     __finally
     {
     if ( dlg ) delete dlg;
     }
     */
   }
  else if (FPreviewDlgKind == pmSystemDlg)
   {
    /*
     UnicodeString AURL = ( __OnFly ) ? "about:" + static_cast<UnicodeString>(__URL) : static_cast<UnicodeString>(__URL);
     if ( reinterpret_cast<int>( ShellExecute ( NULL, NULL, AURL.c_str(), NULL, NULL, SW_RESTORE ) ) <= 32 )
     {
     STARTUPINFO StartUpInfo;
     PROCESS_INFORMATION ProcessInformation;
     ZeroMemory( &StartUpInfo, sizeof( STARTUPINFO ) );
     StartUpInfo.cb = sizeof(STARTUPINFO);
     ZeroMemory( &ProcessInformation, sizeof( PROCESS_INFORMATION ) );
     if ( !CreateProcess( "explorer",
     AURL.c_str(),
     NULL,
     NULL,
     false,
     NORMAL_PRIORITY_CLASS,
     NULL,
     NULL,
     &StartUpInfo,
     &ProcessInformation
     )
     )
     if ( WinExec( ( "explorer " + AURL ).c_str(), SW_RESTORE ) <= 31 )
     {
     int ErrCode;
     UnicodeString ErrMsg = GetAPILastErrorFormatMessage( ErrCode );
     __DopInfo->Text = FMT2(icsDocErrorCommonDop,IntToStr((int)ErrCode),ErrMsg);
     throw DKCommonExceptionEx(
     FMT(icsDocViewerErrorOpenTmpFile),
     0,
     __DopInfo
     );
     }
     }
     */
   }
  else if ((FPreviewDlgKind == pmExternalDlgExcel) || (FPreviewDlgKind == pmExternalDlgWord))
   {
    /*
     UnicodeString MSOAppName      = FMT(icsDocViewerErrorApp);
     UnicodeString MSOAppClassName = FMT(icsDocViewerErrorClass);
     if      ( FPreviewDlgKind == pmExternalDlgExcel )
     {
     MSOAppName      = "Microsoft Excel";
     MSOAppClassName = "Excel.Application";
     }
     else if ( FPreviewDlgKind == pmExternalDlgWord )
     {
     MSOAppName      = "Microsoft Word";
     MSOAppClassName = "Word.Application";
     }

     if ( __OnFly )
     {
     MB_SHOW_ERROR_CPT( cFMT1(icsDocViewerErrorOnFly,MSOAppName), cFMT(icsDocViewerErrorMsgCaption) );
     return;
     }
     Variant vMSOApp;
     try
     {
     try
     {
     vMSOApp  = Variant::GetActiveObject(MSOAppClassName);
     }
     catch( EOleSysError &E )
     {
     try
     {
     vMSOApp = Variant::CreateObject(MSOAppClassName);
     }
     catch( ... )
     {
     vMSOApp = Unassigned;
     throw;
     }
     }
     if ( !vMSOApp.IsEmpty() && !vMSOApp.IsNull())
     {
     vMSOApp.OlePropertySet("Visible", true);
     if ( FPreviewDlgKind == pmExternalDlgExcel )
     {
     vMSOApp.OlePropertyGet("Workbooks").OleFunction(
     "Open",
     UnicodeString(__URL).c_str()
     );
     vMSOApp.OlePropertyGet("ActiveWorkbook").OleProcedure( "Save"     );
     vMSOApp.OlePropertyGet("ActiveWorkbook").OleProcedure( "Activate" );
     }
     else if ( FPreviewDlgKind == pmExternalDlgWord )
     {
     vMSOApp.OlePropertyGet("Documents").OleFunction(
     "Open",
     UnicodeString(__URL).c_str()
     );
     vMSOApp.OlePropertyGet("ActiveDocument").OleProcedure( "Save"     );
     vMSOApp.OlePropertyGet("ActiveDocument").OleProcedure( "Activate" );
     vMSOApp.OleProcedure( "Activate" );
     }
     }
     }
     catch( EOleSysError &E )
     {
     TStringList *sl = NULL;
     try
     {
     sl = new TStringList;
     sl->Text = FMT3(icsDocViewerErrorRepCreate1,
     E.ClassName(),
     IntToStr((int)E.ErrorCode),
     E.Message);
     RMB_SHOW_ERROREX_CPT( cFMT(icsDocViewerErrorRepCreate2), cFMT(icsDocViewerErrorMsgCaption), sl );
     }
     __finally
     {
     if ( sl ) delete sl;
     }
     }
     catch( Exception &E ) //EOleError � ��� �����
     {
     TStringList *sl = NULL;
     try
     {
     sl = new TStringList;
     sl->Text = FMT3(icsDocViewerErrorRepCreate1,
     E.ClassName(),
     "",
     E.Message);
     RMB_SHOW_ERROREX_CPT( cFMT(icsDocViewerErrorRepCreate2), cFMT(icsDocViewerErrorMsgCaption), sl );
     }
     __finally
     {
     if ( sl ) delete sl;
     }
     }
     catch( ... )
     {
     MB_SHOW_ERROR_CPT( cFMT(icsDocViewerErrorRepCreate3), cFMT(icsDocViewerErrorMsgCaption) );
     }
     */
   }
 }
// ***************************************************************************
// ********************** ��������������� ������ *****************************
// ********************** ��� ������� MSWord *********************************
// ***************************************************************************
// ������������ ����������� ��������� � ������� MSWord
void __fastcall TICSDocViewer::CreateDocMSWord()
 {
  throw DKClasses::EMethodError(__FUNC__, FMT(icsDocViewerErrorCreateMSWord));
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewer::FSetDocXMLTemplate(TTagNode * ATmpl)
 {
  if (ATmpl)
   {
    if (!FDocXMLTemplate)
     FDocXMLTemplate = new TTagNode(NULL);
    FDocXMLTemplate->Assign(ATmpl, true);
   }
  else
   {
    if (FDocXMLTemplate)
     {
      delete FDocXMLTemplate;
      FDocXMLTemplate = NULL;
     }
   }
 }
// ---------------------------------------------------------------------------
// ������������ ����������� ������������� � ������� MS Word
void __fastcall TICSDocViewer::CreateSpecMSWord()
 {
  throw DKClasses::EMethodError(__FUNC__, FMT(icsDocViewerErrorCreateMSWord));
 }
// ---------------------------------------------------------------------------
// ������������� ����� �������� ������� ���������������� ��������� ��� ��������������� ��������� � ������� MS Word
void __fastcall TICSDocViewer::InitMSWordPreview()
 {
  throw DKClasses::EMethodError(__FUNC__, FMT(icsDocViewerErrorCreateMSWord));
 }
// ---------------------------------------------------------------------------
// ������ ������� ���������������� ��������� ��� ��������������� ��������� � ������� MS Word
void __fastcall TICSDocViewer::MSWordPreview()
 {
  throw DKClasses::EMethodError(__FUNC__, FMT(icsDocViewerErrorCreateMSWord));
 }
// ***************************************************************************
// ********************** ��������������� ������ *****************************
// ********************** ��� ������������ ������ ****************************
// ********************** �� ������� MSWord **********************************
// ***************************************************************************
bool __fastcall TICSDocViewer::CreateMSWordDocByTemplate()
 {
  /*
   wchar_t szTmpFileName[MAX_PATH];
   UnicodeString MSWordTmpFileName = AppendFileName(
   FTmpPath,
   "idv" + TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss")+".doc"
   );
   FTmpFileNames.push_back(MSWordTmpFileName);
   TFileStream *fs = NULL;
   try
   {
   fs = new TFileStream( MSWordTmpFileName, fmCreate | fmShareExclusive );
   fs->Seek( 0, soFromBeginning );
   fs->CopyFrom( FDocXMLTemplate, FDocXMLTemplate->Size);
   }
   __finally
   {
   if ( fs ) delete fs;
   }
   __DocName = MSWordTmpFileName;
   */
  return true;
 }
// ---------------------------------------------------------------------------
// ������������ ����������� ������������� �� ������� MSWord
void __fastcall TICSDocViewer::CreateSpecByMSWordTemplate()
 {
  __CanShowPreviewDlg = false; // ��� ����
  __CanShowPreviewDlg = CreateMSWordDocByTemplate();
 }
// ---------------------------------------------------------------------------
// ������������ ����������� ��������� �� ������� MSWord
void __fastcall TICSDocViewer::CreateDocByMSWordTemplate()
 {
  __CanShowPreviewDlg = false; // ��� ����
#pragma option push -w-pia
  if (__CanShowPreviewDlg = CreateMSWordDocByTemplate())
#pragma option pop
   {
    TXMLDoc * WordDoc = NULL;
    TXMLTab * CurTab = NULL;
    int nCurTabInd = 0;
    try
     {
      WordDoc = new TXMLDoc();
      CoInitialize(NULL); // ������-�� ����� ����� ���� ???
      WordDoc->Load(FDocXMLTemplate);
      ChangeState(csFillMSWordTemplate);
      SetPBMax(GetMaxPBForDocByMSWordTemplate());
      TTagNode * ndObj = FDoc->GetChildByName("contens")->GetFirstChild();
      while (ndObj)
       {
        if (CheckIfTerminated())
         return;
        // ��������� ����� "lv" ������� ������ - ��������� ���������
        TTagNode * ndObjRef = FSpec->GetTagByUID(ndObj->AV["uidmain"]);
        // ��������� ������ �� ������� �� ������� ��� ����������
        CurTab = WordDoc->GetTab(++nCurTabInd);
        // ##########################################################################
        // #                              �������                                   #
        // ##########################################################################
        if (ndObjRef->CmpName("inscription"))
         {
          TTagNode * ndInsElement = ndObj->GetFirstChild();
          int nRowInd = 1;
          int nColInd = 0;
          // ��� ������� �������� ������� ���� ������
          while (ndInsElement)
           {
            if (CheckIfTerminated())
             return;
            // ��������� ����� "lv" ������� ������ - ��������� �������
            nColInd++ ;
            TTagNode * ndInsElementRef = FSpec->GetTagByUID(ndInsElement->AV["uidmain"]);
            if (ndInsElementRef->GetPrev() && ndInsElementRef->CmpAV("newline", "1"))
             {
              nRowInd++ ;
              nColInd = 1;
             }
            // ����������� �����
            UnicodeString InsViewText = ndInsElementRef->AV["value"];
            // ����� �� ������
            UnicodeString RefText = ndInsElement->GetFirstChild()->AV["val"];
            // ����� �� �������� ���-�� ��� ��������� ����������
            UnicodeString CntClcRlsText = ndInsElement->GetFirstChild()->GetNext()->AV["val"];
            if (RefText != "")
             {
              if (InsViewText != "")
               InsViewText += " ";
              InsViewText += RefText;
             }
            if (CntClcRlsText != "")
             {
              if (InsViewText != "")
               InsViewText += " ";
              InsViewText += CntClcRlsText;
             }
            if (InsViewText != "")
             CurTab->SetText(nRowInd, nColInd, InsViewText);
            ndInsElement = ndInsElement->GetNext();
            IncPB();
           }
         }
        // ##########################################################################
        // #                              ������                                    #
        // ##########################################################################
        else if (ndObjRef->CmpName("list"))
         {
          TTagNodeList ListSingleCols;
          TICSDocSpec::GetListSingleColsList(ndObjRef, & ListSingleCols);
          TTagNodeList::iterator itListSingleCols;
          // ������ ������ ������
          int nRowInd = GetListColsMaxLevel(ndObjRef) - ndObjRef->Level;
          // ������ ������� ������
          int nColInd;
          int nRowNo = 1;
          TTagNode * ndListRow = ndObj->GetFirstChild();
          CurTab->AppendRow(ndObj->Count - 1);
          while (ndListRow)
           {
            if (CheckIfTerminated())
             return;
            // ��������� ����� "lv" ������� ������ - ����� ������
            nColInd          = 1;
            itListSingleCols = ListSingleCols.begin();
            // ���������� ������ ������� ������
            if (ndObjRef->CmpAV("rownumbers", 1))
             {
              UnicodeString RowNo = IntToStr((int) nRowNo);
              RowNo = UnicodeString::StringOfChar('0', IntToStr((int) ndObj->Count).Length() - RowNo.Length()) + RowNo;
              CurTab->SetText(nRowInd, nColInd, RowNo);
              nColInd++ ;
             }
            TTagNode * ndListData = ndListRow->GetFirstChild();
            while (ndListData)
             {
              if (CheckIfTerminated())
               return;
              // ��������� ����� "lv", "iv" �������� ������ - ����� ������� ������ ������
              if (ndListData->CmpName("iv")) // ������� �������� �������� ��� ������� ����������
               { // ��� ��������� ��������
                UnicodeString CellText = GetDocListCellText(ndListData, *itListSingleCols,
                  ndObjRef->CmpAV("showzval", "1"));
                CurTab->SetText(nRowInd, nColInd, CellText);
                nColInd++ ;
               }
              else // �������� ��������� ��������
               {
                TTagNodeList::iterator itListSingleColsInit = itListSingleCols;
                int nSubColInd = nColInd;
                TTagNode * ndListLinkISRows = ndListData->GetFirstChild();
                while (ndListLinkISRows)
                 {
                  if (CheckIfTerminated())
                   return;
                  // ��������� ����� "lv" ���������� ������ - ������ ��������� ��������� ��������
                  itListSingleCols = itListSingleColsInit;
                  nSubColInd       = nColInd;
                  TTagNode * ndListLinkISData = ndListLinkISRows->GetFirstChild();
                  while (ndListLinkISData)
                   {
                    if (CheckIfTerminated())
                     return;
                    // ��������� ����� "iv" ������ ������ - ������ ����� ��������� ��������� ��������
                    UnicodeString CellText = GetDocListCellText(ndListLinkISData, *itListSingleCols,
                      ndObjRef->CmpAV("showzval", "1"));
                    if (ndListLinkISRows->GetPrev())
                     CurTab->SetText(nRowInd, nSubColInd, "\n");
                    CurTab->SetText(nRowInd, nSubColInd, CellText);
                    itListSingleCols++ ;
                    nSubColInd++ ;
                    ndListLinkISData = ndListLinkISData->GetNext();
                   }
                  ndListLinkISRows = ndListLinkISRows->GetNext();
                 }
                nColInd = nSubColInd;
                if (itListSingleCols != itListSingleColsInit)
                 itListSingleCols-- ;
               }
              itListSingleCols++ ;
              ndListData = ndListData->GetNext();
             }
            ndListRow = ndListRow->GetNext();
            nRowNo++ ;
            nRowInd++ ;
            IncPB();
           }
         }
        // ##########################################################################
        // #                              �������                                   #
        // ##########################################################################
        else if (ndObjRef->CmpName("table"))
         {
          TTagNodeList TableSingleRows;
          TICSDocSpec::GetTableSingleRowsList(ndObjRef->GetChildByName("rows"), & TableSingleRows);
          TTagNodeList::iterator itTableSingleRows = TableSingleRows.begin();
          int nRowInd; // ������ ������ ������
          int nColInd; // ������ ������� ������
          nRowInd = GetTagNodesMaxLevel(ndObjRef, "tcol") - ndObjRef->Level;
          if (ndObjRef->CmpAV("colnumbers", "1"))
           nRowInd++ ;
          TTagNode * ndTableRow = ndObj->GetFirstChild();
          while (ndTableRow)
           {
            if (CheckIfTerminated())
             return;
            // ��������� ����� "lv" ������� ������ - ����� �������
            TTagNode * ndTableData = ndTableRow->GetFirstChild();
            nColInd = (*itTableSingleRows)->Level - ndObjRef->Level;
            if (ndObjRef->CmpAV("rownumbers", "1"))
             nColInd++ ;
            while (ndTableData)
             {
              if (CheckIfTerminated())
               return;
              // ��������� ����� "iv" - ����� ������ �������
              UnicodeString CellText = ndTableData->AV["val"];
              // ������ ����������� ������� ��������
              NormalizeNumericalValue(CellText, ndObjRef->CmpAV("showzval", "1"));
              CurTab->SetText(nRowInd, nColInd, CellText);
              nColInd++ ;
              ndTableData = ndTableData->GetNext();
             }
            ndTableRow = ndTableRow->GetNext();
            itTableSingleRows++ ;
            nRowInd++ ;
            IncPB();
           }
         }
        ndObj = ndObj->GetNext();
        IncPB();
       }
      // WordDoc->Save();
      // WordDoc->Close();
      FDocXMLTemplate = WordDoc->AsTagNode();
     }
    __finally
     {
      if (WordDoc)
       delete WordDoc;
     }
   }
 }
// ---------------------------------------------------------------------------
// ������ ������� ���������������� ��������� ��� ��������������� ��������� �� ������� MS Word
void __fastcall TICSDocViewer::ByXMLTemplatePreview(TXMLDoc * ADoc)
 {
  TPreviewForm * pf = NULL;
  TStringList * DDD = NULL;
  try
   {
    pf                = new TPreviewForm(this);
    pf->OnDemoMessage = FPPDemoMsg;
    DDD               = new TStringList;
    ADoc->AsStrings(DDD);
    TPreviewParam param = TPreviewParam(ADoc->TopMargin, ADoc->LeftMargin, ADoc->RightMargin, ADoc->BottomMargin,
      ADoc->PrintScale, (Printers::TPrinterOrientation)ADoc->Orientation, (ADoc->Orientation) ? pzHeight : pzWidth);
    pf->Preview(DDD, param);
   }
  __finally
   {
    if (DDD)
     delete DDD;
    if (pf)
     delete pf;
    // if (WordDoc) delete WordDoc;
   }
 }
// ***************************************************************************
// ********************** �������� ������ ************************************
// ***************************************************************************
/*
 * ������������ ����������� ���������/������������� � ������ ������� ���������������� ���������
 *
 *
 * ���������:
 *   [in]  APreviewType - ��� ������� ��� �����������
 *   [in]  APreviewType - "���������"
 *                        [default = NULL]
 *
 */
void __fastcall TICSDocViewer::Preview(TICSDocViewerPreviewType APreviewType, TProgressBar * APB)
 {
  ThrowIfLocked(__FUNC__);
  if (APreviewType == ptUnknown)
   throw DKClasses::EInvalidArgument(__FUNC__, "APreviewType", FMT(icsDocViewerErrorPreviewTypeVal));
  // �������������
  FPreviewType = APreviewType;
  FPB          = APB;
  if (FAsyncPreviewCreation)
   {
    FThrd = new TICSDocViewerThread(true, this);
    if (FThrd)
     {
      FThrd->FreeOnTerminate = true;
      FLocked                = true;
      FThrdNormalTerminated  = true;
      FThrd->Start(); // Resume();
     }
   }
  else
   {
    InternalPreviewCreate();
    InternalPreviewDlg();
    ChangeState(csNone, false);
   }
 }
// ---------------------------------------------------------------------------
// ���������� �������� ������������ ����������� ��� ����������� ������
void __fastcall TICSDocViewer::StopAsyncPreview()
 {
  if (FAsyncPreviewCreation && FThrd && !FThrd->Terminated)
   FThrd->Terminate();
 }
// ---------------------------------------------------------------------------
// ����������� ������, ������������ ������������ ����������� ��� ����������� ������, ��� ���� ��������
// ������ windows'��� ����� ���� ��������������� �������� � �.�.
// ������������ ��������:
// true  - Ok
// false - ���� ��������� �� ��������� � ��������� ������������ ����������� � ����������� ������
// ���� ����� ����� �����-���������� �� ������� :)
bool __fastcall TICSDocViewer::KillAsyncPreview()
 {
  if (FAsyncPreviewCreation && FThrd)
   {
    DWORD dwExitCode;
    if (GetExitCodeThread(reinterpret_cast<HANDLE>(FThrd->Handle), &dwExitCode))
     if (TerminateThread(reinterpret_cast<HANDLE>(FThrd->Handle), dwExitCode))
      return true;
   }
  return false;
 }
// ***************************************************************************
// ********************** TICSDocViewerThread ********************************
// ***************************************************************************
__fastcall TICSDocViewerThread::TICSDocViewerThread(bool CreateSuspended, TICSDocViewer * AICSDocViewer)
  : TThread(CreateSuspended)
 {
  if (!AICSDocViewer)
   throw DKClasses::ENullArgument(__FUNC__, "AICSDocViewer");
  FICSDocViewer = AICSDocViewer;
  FPB           = NULL;
  __PBMax       = 0;
  __PBPosition  = 0;
  OnTerminate   = FICSDocViewer->ThreadTerminate;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewerThread::InternalSetPBMax()
 {
  FICSDocViewer->__PBMax = __PBMax;
  FICSDocViewer->InternalSetPBMax();
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewerThread::InternalSetPBPosition()
 {
  FICSDocViewer->__PBPosition = __PBPosition;
  FICSDocViewer->InternalSetPBPosition();
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewerThread::InternalGenerateOnChangeCreateStageEvent()
 {
  FICSDocViewer->__CreateStage = __CreateStage;
  Synchronize(& FICSDocViewer->InternalGenerateOnChangeCreateStageEvent);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewerThread::Execute()
 {
  try
   {
    FPB = FICSDocViewer->FPB;
    FICSDocViewer->InternalPreviewCreate();
   }
  catch (Exception & E)
   {
    Application->ShowException(& E);
   }
  catch (...)
   {
    try
     {
      throw Exception(cFMT(icsDocViewerErrorIllegalOperation));
     }
    catch (Exception & E)
     {
      Application->ShowException(& E);
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewerThread::InitPB()
 {
  Synchronize(& FICSDocViewer->InternalInitPB);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewerThread::SetPBMax(const int nMax)
 {
  __PBMax = nMax;
  Synchronize(& InternalSetPBMax);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewerThread::IncPB()
 {
  Synchronize(& FICSDocViewer->InternalIncPB);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewerThread::SetPBPosition(const int nPosition)
 {
  __PBPosition = nPosition;
  Synchronize(& InternalSetPBPosition);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocViewerThread::GenerateOnChangeCreateStageEvent(TICSDocViewerCreateStage ACreateStage)
 {
  __CreateStage = ACreateStage;
  Synchronize(& InternalGenerateOnChangeCreateStageEvent);
 }
// ---------------------------------------------------------------------------
