/*
  ����        - ICSDocFilterUtils.h
  ������      - ��������������� (ICS)
  ��������    - ������ ��� ������ � ��������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 10.01.2007
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  10.01.2007
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef ICSDocFilterUtilsH
#define ICSDocFilterUtilsH

//---------------------------------------------------------------------------

#include <SysUtils.hpp>

//#include "ICSDoc.h"	//��������� �������� � ���� ��������


#include "XMLContainer.h"

//###########################################################################
//##                                                                       ##
//##                               Internals                               ##
//##                                                                       ##
//###########################################################################

namespace ICSDocFilter
{

extern PACKAGE void __fastcall CheckCondRulesParam(UnicodeString AFuncName, TTagNode *ACondRules);
extern PACKAGE bool __fastcall UpdateCondRules_2_0_1(TTagNode *ACondRules, bool AIsFilter);

} // end of namespace ICSDocFilter

//###########################################################################
//##                                                                       ##
//##                                Publics                                ##
//##                                                                       ##
//###########################################################################

namespace ICSDocFilter
{

// ������ ������� �������
extern PACKAGE const UnicodeString csFilterDefVersion;
// ������ ������� ������� �� ���������
extern PACKAGE const UnicodeString csFilterDefDefaultVersion;

//---------------------------------------------------------------------------

// ��������� ������ ������� �������
extern PACKAGE UnicodeString __fastcall GetFilterDefVersion(TTagNode *AFilter);
// ����������, �������������� �� ��������� ������
extern PACKAGE bool __fastcall IsFilterSupported(TTagNode *AFilter);
// ���������, �������������� �� ��������� ������. ���� ������
// �� ��������������, �� ����������� ���������� EICSDocFilterError
extern PACKAGE void __fastcall CheckFilter(TTagNode *AFilter);
// ���������� �������:
//   - ��������� �������������� ��������� � ��������� �� ���������� �� ���������
//   - ���������� �� ��������� ������ �������
// ���� ������ �� ��������������, �� ����������� ���������� EICSDocFilterError
extern PACKAGE bool __fastcall UpdateFilter(TTagNode *AFilter);

//---------------------------------------------------------------------------

class PACKAGE EICSDocFilterError : public System::Sysutils::Exception
{
public:
    __fastcall EICSDocFilterError(const UnicodeString Msg);
};

//---------------------------------------------------------------------------

} // end of namespace ICSDocFilter

#endif
