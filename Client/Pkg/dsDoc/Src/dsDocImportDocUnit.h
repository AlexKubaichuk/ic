//---------------------------------------------------------------------------
#ifndef dsDocImportDocUnitH
#define dsDocImportDocUnitH
//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include "cxCheckBox.hpp"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLabel.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxPC.hpp"
#include "cxProgressBar.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxStatusBar.hpp"
//---------------------------------------------------------------------------
#include "dsDocDMUnit.h"
//---------------------------------------------------------------------------
class PACKAGE TdsDocImportDocForm : public TForm
{
        typedef map<int, UnicodeString> TIntMap;
__published:	// IDE-managed Components
        TOpenDialog *OpenDlg;
        TcxPageControl *ImportPC;
        TcxTabSheet *SpecTS;
        TPanel *SpecExtPanel;
        TcxTabSheet *DocTS;
        TPanel *DocExtPanel;
        TdxStatusBar *StatusBar;
        TdxStatusBarContainerControl *dxStatusBar1Container0;
        TcxProgressBar *PrBar;
 TcxStyleRepository *StyleRepos;
        TcxStyle *DisabledStyle;
        TcxStyle *EnabledStyle;
        TcxTreeList *SpecChList;
        TcxTreeListColumn *SpecCh;
        TcxTreeListColumn *SpecName;
        TcxTreeListColumn *SpecPerid;
        TcxTreeListColumn *SpecOwner;
        TcxTreeList *DocChList;
        TcxTreeListColumn *DocCh;
        TcxTreeListColumn *DocName;
        TcxTreeListColumn *DocPeriod;
        TcxTreeListColumn *DocOwner;
        TdxStatusBarContainerControl *StatusBarContainer1;
        TcxLabel *MsgLab;
        TdxBarManager *ListTBM;
        TdxBarLargeButton *dxBarLargeButton1;
        TdxBarLargeButton *dxBarLargeButton9;
        TdxBarLargeButton *dxBarLargeButton10;
        TdxBarCombo *PackCB;
        TImageList *MainIL;
        TActionList *MainAL;
        TAction *actOpen;
        TAction *actSaveToDB;
        TAction *actCheckByPack;
        TcxImageList *LMainIL;
        TAction *actClose;
 TdxBarPopupMenu *dxBarPopupMenu1;
 TAction *actSelectAll;
 TAction *actUnselect;
 TdxBarButton *dxBarButton1;
 TdxBarButton *dxBarButton2;
        void __fastcall SpecChPropertiesEditValueChanged(TObject *Sender);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall actOpenExecute(TObject *Sender);
        void __fastcall actSaveToDBExecute(TObject *Sender);
        void __fastcall actCheckByPackExecute(TObject *Sender);
        void __fastcall PackCBChange(TObject *Sender);
        void __fastcall actCloseExecute(TObject *Sender);
 void __fastcall SpecChListEditing(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          bool &Allow);
 void __fastcall DocChListEditing(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          bool &Allow);
 void __fastcall SpecChListStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
 void __fastcall DocChListStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
 void __fastcall FormDestroy(TObject *Sender);
 void __fastcall FormShow(TObject *Sender);
 void __fastcall actSelectAllExecute(TObject *Sender);
 void __fastcall actUnselectExecute(TObject *Sender);
private:	// User declarations

  TTagNode *FSpecDefNode, *FDocDefNode;
  TkabCustomDataSource *FSpecDS;
  TkabCustomDataSource *FDocDS;

        //        TStringList *DOSDocList;
        UnicodeString xPath, WrkPath, _ES_ ;
        map <int,bool> FDisSpec;
        map <int,bool> FDisDoc;
        TTagNode     *EITag;
        TIntMap  FSpecGUIMap;
        TStringList *FMissingSpecDoc;

        bool __fastcall CanProgress();
        void __fastcall FCheckDocItem(UnicodeString ASpecName, UnicodeString ASpecGUI, int APerType, TDate ADateFr, TDate ADateTo);
        void __fastcall FCheckSpecItem(UnicodeString ASpecName, UnicodeString ASpecGUI, int APerType, TDate ADateFr, TDate ADateTo);
        void __fastcall FLoadSpec();
        void __fastcall FProgressInit(int AMax, UnicodeString AMsg);
        void __fastcall FProgressChange(int APercent, UnicodeString AMsg);
        void __fastcall FProgressComplite(UnicodeString AMsg);
        bool FInProgress;
        void __fastcall SetInProgress(bool AVal);
        bool __fastcall CheckByHandRules(TTagNode *ADoc);
        void __fastcall SetEnabled(bool AEnabled);
        bool __fastcall SetUID(TTagNode *ANode, UnicodeString &AUID);
        TdsDocDM *FDM;
//        UnicodeString FUpperFunc;
protected:
        virtual void __fastcall WndProc(Messages::TMessage &Message);
public:		// User declarations
        __fastcall TdsDocImportDocForm(TComponent* Owner, TdsDocDM *ADM, bool ASpec = true, bool ADoc = true);
        __property bool InProgress = {read=FInProgress, write=SetInProgress};
//        __property UnicodeString UpperFunc = {read=FUpperFunc, write=FUpperFunc};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocImportDocForm *dsDocImportDocForm;
//---------------------------------------------------------------------------
#endif


