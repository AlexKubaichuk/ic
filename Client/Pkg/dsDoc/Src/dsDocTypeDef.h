//---------------------------------------------------------------------------
#ifndef dsDocTypeDefH
#define dsDocTypeDefH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
//#include "XMLContainer.h"
#include "AxeUtil.h"
#include "KabCustomDS.h"
#include "dsRegExtFilterSetTypes.h"
#include "dsRegTypeDef.h"
//---------------------------------------------------------------------------
//typedef bool __fastcall (__closure *TdsGetF63Event)(System::UnicodeString ARef, System::UnicodeString &RC);
typedef void __fastcall (__closure *TICSCheckPackItem) (UnicodeString ASpecName, UnicodeString ASpecGUI, int APerType, TDate ADateFr, TDate ADateTo);
typedef void __fastcall (__closure *TFillLPUData) (TStrings *ADest, __int64 ACode);
typedef void __fastcall (__closure *TSetGetLPUData) (__int64 ACode, TTagNode *ADest);

typedef bool __fastcall (__closure *TdsDocGetSpec)(System::UnicodeString AId, int  &ASpecType, System::UnicodeString &ARC);
typedef bool __fastcall (__closure *TdsDocGetDocPreview)(System::UnicodeString AId, System::UnicodeString &ARC, int &Al, int &At, int &Ar, int &Ab, double &Aps, int &Ao);
typedef UnicodeString __fastcall (__closure *TdsDocLoadDocPreview)(System::UnicodeString &AMsg);

typedef bool __fastcall (__closure *TdsDocGetDocById)(System::UnicodeString AId, System::UnicodeString &ARC);
typedef void __fastcall (__closure *TdsDocSaveDocEvent)(System::UnicodeString AId);

typedef bool __fastcall (__closure *TdsDocCheckByPeriod)(bool FCreateByType, UnicodeString ACode, UnicodeString ASpecGUI, System::UnicodeString &ARC);
typedef bool __fastcall (__closure *TdsDocDocListClearByDate)(TDate ADate);
typedef int  __fastcall (__closure *TdsDocGetDocCountBySpec)(UnicodeString ASpecOWNER, UnicodeString ASpecGUI, UnicodeString &ARCMsg);
typedef bool __fastcall (__closure *TdsDocIsMainOwnerCode)(UnicodeString ASpecOwner);
typedef bool __fastcall (__closure *TdsDocCopySpec)(UnicodeString AName, UnicodeString &ANewGUI, System::UnicodeString &ARC);
typedef int  __fastcall (__closure *TdsDocCheckSpecExists)(UnicodeString AName, UnicodeString &AGUI, bool AIsEdit);

typedef bool __fastcall (__closure *TdsDocCheckDocCreate)(UnicodeString ASpecGUI, int &ACreateType, int ADefCreateType, int &AGetCreateType, int &AGetPeriod);
typedef bool __fastcall (__closure *TdsCheckExistDocCreate)(UnicodeString ASpecGUI, UnicodeString AAuthor, int APerType, TDate APeriodDateFr, TDate FPeriodDateTo, System::UnicodeString &ADocGUI);
typedef bool __fastcall (__closure *TdsDocDocCreate)       (UnicodeString ASpecGUI, UnicodeString AAuthor, UnicodeString AFilter, bool AConcr, int ACreateType, int APerType, TDate APeriodDateFr, TDate FPeriodDateTo, System::UnicodeString &ADocGUI);
typedef bool __fastcall (__closure *TdsDocCanHandEdit)     (UnicodeString ASpecGUI);

typedef UnicodeString __fastcall (__closure *TdsDocImportEvent)(UnicodeString AID, TJSONObject *AData, UnicodeString &ARecId);

typedef bool __fastcall (__closure *TdsDocStartDocCreate) (UnicodeString ASpecGUI, UnicodeString AAuthor, UnicodeString AFilter, UnicodeString AFilterNameGUI, bool AConcr, int ACreateType, int APerType, TDate APeriodDateFr, TDate FPeriodDateTo, bool ANoSave);
typedef UnicodeString __fastcall (__closure *TdsDocDocCreateProgress) (System::UnicodeString &ADocGUI);

typedef void __fastcall (__closure *TdsDocCanDeleteChoiceItemEvent)(__int64 ACode, bool &ACanDelete, UnicodeString &AMsg);
typedef void __fastcall (__closure *TdsDocSetTextDataEvent)(System::UnicodeString ATabId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString AData, System::UnicodeString &ARC);
typedef void __fastcall (__closure *TdsDocGetTextDataEvent)(System::UnicodeString ATabId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString &AData, System::UnicodeString &ARC);
typedef UnicodeString __fastcall (__closure *TdsDocCreateCheckFilterEvent)(System::UnicodeString ASpecGUI, System::UnicodeString AFilter, UnicodeString &AFilterName, UnicodeString &AFilterGUI);

//---------------------------------------------------------------------------
#endif

