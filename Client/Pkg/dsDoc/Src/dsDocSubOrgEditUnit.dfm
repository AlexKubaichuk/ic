object dsDocSubOrgEditForm: TdsDocSubOrgEditForm
  Left = 615
  Top = 149
  BorderStyle = bsSingle
  Caption = 'dsDocSubOrgEditForm'
  ClientHeight = 333
  ClientWidth = 511
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 296
    Width = 511
    Height = 37
    Align = alBottom
    TabOrder = 0
    ExplicitTop = 338
    DesignSize = (
      511
      37)
    object OkBtn: TcxButton
      Left = 337
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = OkBtnClick
    end
    object CancelBtn: TcxButton
      Left = 425
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object LPUClassPanel: TPanel
    Left = 0
    Top = 46
    Width = 511
    Height = 250
    Align = alClient
    TabOrder = 1
    ExplicitTop = 69
    ExplicitHeight = 269
    object OrgCodeLab: TLabel
      Left = 9
      Top = 16
      Width = 90
      Height = 13
      Caption = #1050#1086#1076' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080':'
      Enabled = False
    end
    object NameLab: TLabel
      Left = 8
      Top = 43
      Width = 150
      Height = 13
      Caption = #1057#1086#1082#1088#1072#1097#1105#1085#1085#1086#1077' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077':'
      Enabled = False
    end
    object FullNameLab: TLabel
      Left = 9
      Top = 70
      Width = 118
      Height = 13
      Caption = #1055#1086#1083#1085#1086#1077' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077':'
      Enabled = False
    end
    object AddrLab: TLabel
      Left = 9
      Top = 97
      Width = 34
      Height = 13
      Caption = #1040#1076#1088#1077#1089':'
      Color = clBtnFace
      Enabled = False
      ParentColor = False
    end
    object AddrValLab: TLabel
      Left = 8
      Top = 121
      Width = 494
      Height = 28
      AutoSize = False
      Color = clCream
      Enabled = False
      ParentColor = False
      Transparent = False
      WordWrap = True
    end
    object FIOLab: TLabel
      Left = 9
      Top = 155
      Width = 112
      Height = 13
      Caption = #1060'.'#1048'.'#1054'. '#1088#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1103':'
      Enabled = False
    end
    object EMailLab: TLabel
      Left = 9
      Top = 212
      Width = 31
      Height = 13
      Caption = 'E-mail:'
      Enabled = False
    end
    object PhoneLab: TLabel
      Left = 9
      Top = 185
      Width = 48
      Height = 13
      Caption = #1058#1077#1083#1077#1092#1086#1085':'
      Enabled = False
    end
    object OrgCodeED: TcxMaskEdit
      Left = 164
      Top = 13
      Enabled = False
      Properties.MaskKind = emkRegExpr
      Properties.MaxLength = 50
      Properties.ValidateOnEnter = False
      Properties.OnValidate = OrgCodeEDPropertiesValidate
      Style.Color = clInfoBk
      TabOrder = 0
      Width = 338
    end
    object NameED: TcxTextEdit
      Left = 164
      Top = 40
      Enabled = False
      Properties.MaxLength = 25
      Style.Color = clInfoBk
      TabOrder = 1
      Width = 338
    end
    object FullNameED: TcxTextEdit
      Left = 164
      Top = 67
      Enabled = False
      Properties.MaxLength = 255
      Style.Color = clInfoBk
      TabOrder = 2
      Width = 338
    end
    object OwnerFIOED: TcxMaskEdit
      Left = 165
      Top = 155
      Enabled = False
      Properties.MaxLength = 255
      Properties.ValidateOnEnter = False
      Properties.OnValidate = OrgCodeEDPropertiesValidate
      Style.Color = clWindow
      TabOrder = 3
      Width = 338
    end
    object PhoneED: TcxMaskEdit
      Left = 165
      Top = 182
      Enabled = False
      Properties.MaxLength = 20
      Properties.ValidateOnEnter = False
      TabOrder = 4
      Width = 265
    end
    object EMailED: TcxMaskEdit
      Left = 164
      Top = 209
      Enabled = False
      Properties.MaskKind = emkRegExpr
      Properties.MaxLength = 0
      Properties.ValidateOnEnter = False
      TabOrder = 5
      Width = 265
    end
    object AddrED: TcxTextEdit
      Left = 164
      Top = 94
      Enabled = False
      Properties.MaxLength = 255
      Style.Color = clInfoBk
      TabOrder = 6
      Width = 338
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 511
    Height = 46
    Align = alTop
    TabOrder = 2
    object LPULab: TLabel
      Left = 9
      Top = 16
      Width = 27
      Height = 13
      Caption = #1051#1055#1059':'
      FocusControl = LPUCB
    end
    object LPUCB: TcxComboBox
      Left = 124
      Top = 12
      Properties.DropDownListStyle = lsEditFixedList
      Properties.OnChange = LPUCBPropertiesChange
      Style.Color = clInfoBk
      TabOrder = 0
      Width = 378
    end
  end
end
