#ifndef dsDocClientConstDefHPP
#define dsDocClientConstDefHPP

#include "fmtdef.h"

// Common
 #define dsDocCommonErrorCaption "������"
 #define dsDocCommonInputErrorCaption "������"
 #define dsDocCommonMsgCaption "���������"

 #define dsDocCommonErrorDBWorkMsg "������ ������ � ��"

 #define dsDocCommonConfRecDelCaption "������������� �������� ������"
 #define dsDocCommonDelConfMsg "������������� �������� %s \r\n%s ?"
 #define dsDocCommonErrorDelCaption "������ ��������"
 #define dsDocCommonErrorDelMsg "������ �������� %s"

 #define dsDocCommonErrorCreateDocCaption  "������ ������������ ���������"


//SQLCreator
 #define dsDocSQLCreatorErrorCondStructure  "������ � ��������� ������� ������"
 #define dsDocSQLCreatorErrorFuncMissing  "����������� ������� \"%s\""



//DocCreateProgress
 #define dsDocDocCreateProgressCaption  "������������ ���������"
 #define dsDocDocCreateProgressStageFrom  "���� : %s �� %s"

//DocCreateType
// #define dsDocDocCreateType  ""
 #define dsDocDocCreateTypeCaption "������������ ���������"
 #define dsDocDocCreateTypeQuestLabLabCaption "�������� ����������� ��:"
 #define dsDocDocCreateTypeOkBtnBtnCaption "���������"
 #define dsDocDocCreateTypeCancelBtnBtnCaption "������"
 #define dsDocDocCreateTypeDocRBRBCaption "����������"
 #define dsDocDocCreateTypeBaseRBRBCaption "���� ������"

//DocCreator
 #define dsDocDocCreatorDocLoadConf  "������ �������� �� ��������� ������ ��� ����������!!!\r\n ��������� ����������� ��������?"
 #define dsDocDocCreatorDocLoadConfCaption  "������������� �������� ���������"
 #define dsDocDocCreatorInitCaption  "�������������..."
 #define dsDocDocCreatorInscCreateCaption  "������������ ������ �������"
 #define dsDocDocCreatorListCreateCaption  "������������ ������ (����� �����: \"%s\")"
 #define dsDocDocCreatorErrorISLink  "����������� ����� ����� ����������"
 #define dsDocDocCreatorTableCreateGetCount  "������������ ������� (������ ���������� �����/��������)"
 #define dsDocDocCreatorTableCreateRows  "������������ ������� (������������ �����)"
 #define dsDocDocCreatorTableCreateRow  "������"
 #define dsDocDocCreatorTableCreateCols  "������������ ������� (������������ �������)"
 #define dsDocDocCreatorTableCreateCol  "�������"
 #define dsDocDocCreatorTableCreateCeills  "������������ ������� (������������ �����)"
 #define dsDocDocCreatorTableCreateCeill  "������������ �������, ������(%s,%s)"
 #define dsDocDocCreatorTableCreateCeillMsg  "������������ ������� (%s � %s)"
 #define dsDocDocCreatorErrorMissingColRowDef  "����������� �������� ����� ��� �������"
 #define dsDocDocCreatorErrorCeilCalc  "������ ��� ���������� ������.\r\n������ - %s ������� - %s; (%s,%s)"

 #define dsDocDocCreatorPeroidFromCaption  "�"
 #define dsDocDocCreatorPeroidTo1Caption  "��"
 #define dsDocDocCreatorPeroidTo2Caption  "��"
 #define dsDocDocCreatorUndefPeroidCaption  "�� ������"

 #define dsDocDocCreatorPeriodicityEqual  "��������� � �������������� ������������ ���������"
 #define dsDocDocCreatorPeriodicityYear  "���"
 #define dsDocDocCreatorPeriodicityHalfYear  "���������"
 #define dsDocDocCreatorPeriodicityQuarter  "�������"
 #define dsDocDocCreatorPeriodicityMonth  "�����"
 #define dsDocDocCreatorPeriodicityUnique  "����������"
 #define dsDocDocCreatorPeriodicityNo  "�����������"
 #define dsDocDocCreatorPeriodicityAny  "�����"

 #define dsDocDocCreatorAuthorDefined  ""
 #define dsDocDocCreatorAuthorThis  "����"
 #define dsDocDocCreatorAuthorAllChild  "��� �����������"

 #define dsDocDocCreatorErrorNoIntDocs  "���� %s\r\n����������� ��������� ��� ����������.\r\n\r\n������������� ���������:\r\n��������:\t\"%s\"\r\n������:\t\t\"%s\"\r\n�������������:\t\"%s\"\r\n�����������:\t\"%s\"."#13#10;

 #define dsDocDocCreatorErrorCreateRowColNamesNotValid  "������������ ������ �����/�������� �� ������������� ""rows,columns""."
 #define dsDocDocCreatorErrorRowColReferNotValid  "������� ������������ ��������� �� ���������,\r\n ������� ������ �� ������������� %s, ���=""%s""."

 #define dsDocDocCreatorSpecPrepare  "���������� �������� ���������"

//DocList
 #define dsDocDocListDocCaption  "���������"

 #define dsDocDocListCaption "������ ����������"
 #define dsDocDocListLabel2Caption "��� ���������:"
 #define dsDocDocListLabel4Caption "���� ������������:"
 #define dsDocDocListSpecGUICaption "��� ��������:"
 #define dsDocDocListLabel3Caption "����� ��������:"
 #define dsDocDocListLabel1Caption "����� ���������:"
 #define dsDocDocListPeriodLabCaption "������ ������������:"
 #define dsDocDocListLabel6Caption "�������������:"
 #define dsDocDocListLabel7Caption "����� ���������:"
 #define dsDocDocListLabel8Caption "��������� ������ ����������"

 #define dsDocDocListFilterPanelBottomBtnHint "�������� ������ ���������� ������ ����������"
 #define dsDocDocListFilterPanelTopBtnHint "������ ������ ���������� ������ ����������"

 #define dsDocDocListPeriodicityCBItems1 ""
 #define dsDocDocListPeriodicityCBItems2 "�����������"
 #define dsDocDocListPeriodicityCBItems3 "����������"
 #define dsDocDocListPeriodicityCBItems4 "�����"
 #define dsDocDocListPeriodicityCBItems5 "�������"
 #define dsDocDocListPeriodicityCBItems6 "���������"
 #define dsDocDocListPeriodicityCBItems7 "���"

 #define dsDocDocListResetBtnHint "�������� ������ ���������� ������ �������� ����������"
 #define dsDocDocListResetBtnCaption "�������� ��������� ������"

 #define dsDocDocListactDeleteDocCaption "�������"
 #define dsDocDocListactDeleteDocHint "������� ��������� ��������"
 #define dsDocDocListactPreviewCaption "�������� ���������"
 #define dsDocDocListactPreviewHint "�������� ���������..."
 #define dsDocDocListactFindDocCaption "����� ..."
 #define dsDocDocListactFindDocHint "����� ��������"
 #define dsDocDocListactCreateDocCaption "����������� ��������"
 #define dsDocDocListactCreateDocHint "������������ ���������"
 #define dsDocDocListactAboutCaption "�������"
 #define dsDocDocListactAboutHint "�������"

 #define dsDocDocListStatusSBPanels1 "�����:"
 #define dsDocDocListStatusSBPanels2 "����� �������"
 #define dsDocDocListStatusSBPanels3 "�� �������"
 #define dsDocDocListStatusSBPanels4 "�� �������"

 #define dsDocDocListListTBMBars1Caption "��������������"

 #define dsDocDocListDocListColumnNAMECaption "������������ ���������"
 #define dsDocDocListDocListColumnPERIOD_VALUECaption "������"

//DocOrgEdit
 #define dsDocDocOrgEditSetCaption  "��������� ���������� �����������"
 #define dsDocDocOrgEditEditCaption  "�������������� ���������� �����������"
 #define dsDocDocOrgEditInsertCaption  "���������� �����������"

 #define dsDocDocOrgEditChLPUErrMsg  "�� ������� ���"
 #define dsDocDocOrgEditChISubordErrMsg  "�� ������ ��� �������������"
 #define dsDocDocOrgEditChIOrgCodeErrMsg  "�� ����� ��� �����������"
 #define dsDocDocOrgEditChIOrgCodeResErrMsg  "�������� ��� ����������� �������� �����������������,\r\n������� ������ ���."
 #define dsDocDocOrgEditChIOrgCodeFormatErrMsg  "�������� ��� ����������� �� ������������� �������.\r\n ������ \\"XXX.XXXXXXX.XXX\\",\r\n��� X - ����� �� 0 �� 9.\r\n������� ������ ���."
 #define dsDocDocOrgEditChIOrgShortNameErrMsg  "�� ������� ����������� ������������ �����������"
 #define dsDocDocOrgEditChIOrgFullNameErrMsg  "�� ������� ������ ������������ �����������"
 #define dsDocDocOrgEditChIAddrErrMsg  "�� ����� ����� �����������"
 #define dsDocDocOrgEditChIOrgExistErrMsg  "����������� � ����� ����� ��� ����������, \r\n ������� ������ ���."

 #define dsDocDocOrgEditSubordLabCaption "�������������:"
 #define dsDocDocOrgEditOrgCodeLabCaption "��� �����������:"
 #define dsDocDocOrgEditNameLabCaption "����������� ������������:"
 #define dsDocDocOrgEditAddrLabCaption "�����:"
 #define dsDocDocOrgEditEMailLabCaption "E-mail:"
 #define dsDocDocOrgEditPhoneLabCaption "�������:"
 #define dsDocDocOrgEditFullNameLabCaption "������ ������������:"
 #define dsDocDocOrgEditOkBtnCaption "���������"
 #define dsDocDocOrgEditCancelBtnCaption "������"

 #define dsDocDocOrgEditSubordCBItem1 "�����������"
 #define dsDocDocOrgEditSubordCBItem2 "�����������"
 #define dsDocDocOrgEditSubordCBItem3 "����"

//DocOrgList
 #define dsDocDocOrgListErrorInsertRecMsg  "������ ���������� ������"
 #define dsDocDocOrgListErrorEditRecMsg  "������ ����������� ������"
 #define dsDocDocOrgListErrorDelDocDef  "���������� �������� ���������� ��������� ������ ������������"
 #define dsDocDocOrgListErrorDelDoc  "���������� ��������� �������������� ������ ������������"
 #define dsDocDocOrgListOrgCaption  "�����������"
 #define dsDocDocOrgListRecCaption  "������"

 #define dsDocDocOrgListCaption "����������� ���"
 #define dsDocDocOrgListLabel1Caption "��� �����������"
 #define dsDocDocOrgListLabel2Caption "�������������"
 #define dsDocDocOrgListLabel3Caption "�����"
 #define dsDocDocOrgListLabel4Caption "E-mail"
 #define dsDocDocOrgListLabel5Caption "�.�.�. ������������"
 #define dsDocDocOrgListLabel7Caption "����������� ������������"
 #define dsDocDocOrgListLabel8Caption "�������"

 #define dsDocDocOrgListactInsertCaption "��������"
 #define dsDocDocOrgListactInsertHint "��������"
 #define dsDocDocOrgListactEditCaption "��������"
 #define dsDocDocOrgListactEditHint "��������"
 #define dsDocDocOrgListactDeleteCaption "�������"
 #define dsDocDocOrgListactDeleteHint "�������"
 #define dsDocDocOrgListactSetTemplateCaption "���������� &������"
 #define dsDocDocOrgListactSetTemplateHint "���������� ������"
 #define dsDocDocOrgListactReSetTemplateCaption "���&�� ������"
 #define dsDocDocOrgListactReSetTemplateHint "����� ������"
 #define dsDocDocOrgListactRefreshCaption "��������"
 #define dsDocDocOrgListactRefreshHint "��������"
 #define dsDocDocOrgListactFindCaption "�����..."
 #define dsDocDocOrgListactFindHint "�����..."
 #define dsDocDocOrgListactFindNextCaption "����� �����..."
 #define dsDocDocOrgListactFindNextHint "����� �����..."

 #define dsDocDocOrgListMainTBMBar1Caption "��������������"
 #define dsDocDocOrgListMainTBMBar2Caption "������"
 #define dsDocDocOrgListClassGridColumnCaption "������ ������������"

//DocPeriod

 #define dsDocDocPeriodErrorYearFormat  "��������� ������ ����"
 #define dsDocDocPeriodErrorDateFormat  "��������� ������ ����"

// #define dsDocDocPeriodCaption "������ ������������"
 #define dsDocDocPeriodCaptionLabCaption "������� ������ ������������ ��������"
 #define dsDocDocPeriodLabCaption "������:"
 #define dsDocDocPeriodDateFrLabCaption "��"
 #define dsDocDocPeriodDateToLabCaption "��"
 #define dsDocDocPeriodYearRBCaption "���"
 #define dsDocDocPeriodQuarterRBCaption "�������"
 #define dsDocDocPeriodMonsRBCaption "�����"
 #define dsDocDocPeriodPeriodRBCaption "�������� ���"
 #define dsDocDocPeriodHalfYearRBCaption "���������"
 #define dsDocDocPeriodOkBtnCaption "���������"
 #define dsDocDocPeriodCancelBtnCaption "������"

 #define dsDocDocPeriodHalfYearCBItems1 "1-� ���������"
 #define dsDocDocPeriodHalfYearCBItems2 "2-� ���������"
 #define dsDocDocPeriodQuarterCBItems1 "1-� �������"
 #define dsDocDocPeriodQuarterCBItems2 "2-� �������"
 #define dsDocDocPeriodQuarterCBItems3 "3-� �������"
 #define dsDocDocPeriodQuarterCBItems4 "4-� �������"
 #define dsDocDocPeriodMonsCBItems01 "������"
 #define dsDocDocPeriodMonsCBItems02 "�������"
 #define dsDocDocPeriodMonsCBItems03 "����"
 #define dsDocDocPeriodMonsCBItems04 "������"
 #define dsDocDocPeriodMonsCBItems05 "���"
 #define dsDocDocPeriodMonsCBItems06 "����"
 #define dsDocDocPeriodMonsCBItems07 "����"
 #define dsDocDocPeriodMonsCBItems08 "������"
 #define dsDocDocPeriodMonsCBItems09 "��������"
 #define dsDocDocPeriodMonsCBItems10 "�������"
 #define dsDocDocPeriodMonsCBItems11 "������"
 #define dsDocDocPeriodMonsCBItems12 "�������"

//ExtFunc

 #define dsDocExtFuncDocPeriodLocStr1  "���"
 #define dsDocExtFuncDocPeriodLocStr2  "���������"
 #define dsDocExtFuncDocPeriodLocStr3  "�������"
 #define dsDocExtFuncDocPeriodLocStr4  "�����"
 #define dsDocExtFuncDocPeriodLocStr5  "����������"
 #define dsDocExtFuncDocPeriodLocStr6  "�����������"

 #define dsDocExtFuncDocPeriodValStrUniq1  "%s.%s.%s �. - %s.%s.%s �."
 #define dsDocExtFuncDocPeriodValStrUniq2  "� %s.%s.%s �."
 #define dsDocExtFuncDocPeriodValStrUniq3  "�� %s.%s.%s �."

 #define dsDocExtFuncDocPeriodValStrMonth  "%s %s �."

 #define dsDocExtFuncDocPeriodValStrQuart1  "I ������� %s �."
 #define dsDocExtFuncDocPeriodValStrQuart2  "II ������� %s �."
 #define dsDocExtFuncDocPeriodValStrQuart3  "III ������� %s �."
 #define dsDocExtFuncDocPeriodValStrQuart4  "IV ������� %s �."

 #define dsDocExtFuncDocPeriodValStrHalfYear1  "I ���������  %s �."
 #define dsDocExtFuncDocPeriodValStrHalfYear2  "II ��������� %s �."

 #define dsDocExtFuncDocPeriodValStrYear  "%s �."

 #define dsDocExtFuncErrorReadSettingCaption  "������ ������ ��������"
 #define dsDocExtFuncErrorReadSettingMsg  "� ���������� ��������� �� ������ ��������� ���"

 #define dsDocExtFuncCreateDocViewMsg  "������������ ����������� ���������"
 #define dsDocExtFuncErrorCreateDocView  "������ ������������ ��������� (����������� �������� ���������)"

 #define dsDocExtFuncReplaceDocMsg  "�������� - \"%s\"\r\n �� ������: %s\r\n ��� ����������.\r\n\r\n�������� ������������ ��������?"
 #define dsDocExtFuncReplaceDocCaption  "������������� ������ ���������"
 #define dsDocExtFuncMissingDocDef  "����������� �������� ���������, ��� �������� \"%s\""
 #define dsDocExtFuncNoDocDef  "�������� �����������"
 #define dsDocExtFuncFileVersionString  "StringFileInfo\041904E3\FileVersion"

 #define dsDocExtFuncRecCount1   "������"
 #define dsDocExtFuncRecCount234 "������"
 #define dsDocExtFuncRecCount    "�������"

//ExtSpecList

 #define dsDocExtSpecListAddDocDefCaption  "������ �������� ����������"

 #define dsDocExtSpecListCaption "������ �������� ����������"
 #define dsDocExtSpecListSpecCodeLabCaption "��� �������� ���������:"
 #define dsDocExtSpecListCreateDateLabCaption "  ���� ������������:"
 #define dsDocExtSpecListAutorLabCaption "����� ��������:"
 #define dsDocExtSpecListExtSpecListColumn1Caption "������������ ��������"
 #define dsDocExtSpecListListTBMBar1Caption "��������������"

 #define dsDocExtSpecListactAddSpecCaption1  "�������"
 #define dsDocExtSpecListactAddSpecCaption2  "��������"
 #define dsDocExtSpecListactDeleteSpecCaption1  "������"
 #define dsDocExtSpecListactDeleteSpecCaption2  "�������"

//SpecEdit

 #define dsDocDocSpecEditEditCaption "�������������� �������� ���������: \"%s\""
 #define dsDocDocSpecEditNewCaption "����� �������� ���������: \"%s\""
 #define dsDocDocSpecEditSaveMsg "�������� ��������� �� �������� �� ������ ��������!\r\n���������� ����������"
 #define dsDocDocSpecEditSaveCaption "������ ���������� ���������"
 #define dsDocDocSpecEditCancelWarn "� �������� ��������� ������� ���������,\r\n��������� ���������?"

 #define dsDocDocSpecEditSaveBtnCaption "���������"
 #define dsDocDocSpecEditCancelBtnCaption "������"

//SpecList

 #define dsDocDocSpecListNoDefContent "��������� ��������� �������� ���������!!!\r\n����������� �������� �������� ���������"
 #define dsDocDocSpecListErrorFindSpec "������, �� �� ������ ����� !"
 #define dsDocDocSpecListDocDefCaption "�������� ���������"
 #define dsDocDocSpecListErrorDelDocDef "���������� ��������� �������������� �� ������� ��������,\r\n�������� ����������."
 #define dsDocDocSpecListErrorReplaceDocDef "�������� ���������: \"%s\" ��� ����������!\r\n������������ �������� ������� �� ����, �������� �� ��������.\r\n��������� �������� ��������� ��� ����� ?"
 #define dsDocDocSpecListConfReplaceDocDef "�������� ���������: \"%s\" ��� ����������!\r\n�������� ?"
 #define dsDocDocSpecListCopyCaption "�����"
 #define dsDocDocSpecListErrorOpenFile "������ �������� ����� \"%s\""
 #define dsDocDocSpecListErrorFileWork "������ ���������� ����� �� �������� ��������: Open, Seek, Read, Close."
 #define dsDocDocSpecListOpenDlgMask "xml-�����|*.xml"

 #define dsDocDocSpecListCaption "������ �������� ����������"
 #define dsDocDocSpecListFilterParamLabCaption "��������� ������ ��������"
 #define dsDocDocSpecListPeriodicityLabCaption "�������������:"
 #define dsDocDocSpecListOwnerLabCaption "�����:"
 #define dsDocDocSpecListFilterPanelTopBtnHint "������ ������ ���������� ������ �������� ����������"
 #define dsDocDocSpecListFilterPanelBottomBtnHint "�������� ������ ���������� ������ �������� ����������"
 #define dsDocDocSpecListResetBtnHint "�������� ������ ���������� ������ �������� ����������"
 #define dsDocDocSpecListResetBtnCaption "�������� ��������� ������"
 #define dsDocDocSpecListSpecCodeLabCaption "��� �������� ���������:"
 #define dsDocDocSpecListCreateDateLabCaption "  ���� ������������:"
 #define dsDocDocSpecListAutorLabCaption "����� ��������:"
 #define dsDocDocSpecListLabel1Caption "��� �����������, ������ ��������:"
 #define dsDocDocSpecListactNewSpecCaption "����� ��������"
 #define dsDocDocSpecListactNewSpecHint "������� ����� �������� ..."
 #define dsDocDocSpecListactEditSpecCaption "������������� ��������"
 #define dsDocDocSpecListactEditSpecHint "������������� ��������� �������� ..."
 #define dsDocDocSpecListactCopySpecCaption "���������� ��������"
 #define dsDocDocSpecListactCopySpecHint "���������� ��������� �������� ..."
 #define dsDocDocSpecListactDeleteSpecCaption "������� ��������"
 #define dsDocDocSpecListactDeleteSpecHint "������� ��������� �������� ..."
 #define dsDocDocSpecListactFindSpecCaption "����� ..."
 #define dsDocDocSpecListactFindSpecHint "����� �������� ��������� ..."
 #define dsDocDocSpecListactCreateDocCaption "����������� ��������"
 #define dsDocDocSpecListactCreateDocHint "������������ ���������"
 #define dsDocDocSpecListactAboutCaption "�������"
 #define dsDocDocSpecListactAboutHint "�������"
 #define dsDocDocSpecListactHandCreateCaption "������ ���� ���������"
 #define dsDocDocSpecListactHandCreateHint "������ ���� ��������� ..."
 #define dsDocDocSpecListactAddTemplateCaption "�������� ������ ���������"
 #define dsDocDocSpecListactAddTemplateHint "�������� ������ ���������"
 #define dsDocDocSpecListactClearTemplateCaption "������� ������ ���������"
 #define dsDocDocSpecListactClearTemplateHint "������� ������ ���������"
 #define dsDocDocSpecListactSaveToXMLCaption "��������� �������� ��������� � ����"
 #define dsDocDocSpecListactSaveToXMLHint "��������� �������� ��������� � ����"
 #define dsDocDocSpecListactLoadFromXMLCaption "��������� �������� ��������� �� �����"
 #define dsDocDocSpecListactLoadFromXMLHint "��������� �������� ��������� �� �����"

 #define dsDocDocSpecListNoDataSrc "�� ����� �������� ������"

 #define dsDocDocSpecListFltLabCaption "������: "
 #define dsDocDocSpecListFltLabHint "������: "

 #define dsDocDocSpecListPeriodicityCBItem1 ""
 #define dsDocDocSpecListPeriodicityCBItem2 "���������������"
 #define dsDocDocSpecListPeriodicityCBItem3 "�������������"

 #define dsDocDocSpecListDBTableView1NAMECaption "������������ ��������"
 #define dsDocDocSpecListDBTableView1F_PERIODCaption "�������������"

 #define dsDocDocSpecListStatusSBItem1Caption "�����:"
 #define dsDocDocSpecListStatusSBItem2Caption "����� �������"
 #define dsDocDocSpecListStatusSBItem3Caption "�� �������"
 #define dsDocDocSpecListStatusSBItem4Caption "�� �������"

//DocExDFM

 #define dsDocDocDocExDFMPeriodicyty "��"
 #define dsDocDocDocExDFMNoPeriodicyty "���"

//ChoiceList

 #define dsDocDocChoiceListConfReplaceChoice "������: \"%s\" ��� ����������!\r\n�������� ?"

 #define dsDocDocChoiceListErrorAddChoiceRecMissing "������ ���������� ������� (������ �� �������)"
 #define dsDocDocChoiceListErrorEditChoice "������ ����������� �������"
 #define dsDocDocChoiceListErrorAddChoice "������ ���������� �������"
 #define dsDocDocChoiceListChioceCaption "�������"

 #define dsDocDocChoiceListCaption "���������� ��������"
 #define dsDocDocChoiceListFltListColumns1Caption "������������"
 #define dsDocDocChoiceListClassTBMBar1Caption "��������������"
 #define dsDocDocChoiceListactInsertCaption "��������"
 #define dsDocDocChoiceListactInsertHint "��������"
 #define dsDocDocChoiceListactEditCaption "��������"
 #define dsDocDocChoiceListactEditHint "��������"
 #define dsDocDocChoiceListactDeleteCaption "�������"
 #define dsDocDocChoiceListactDeleteHint "�������"

 #define dsDocDocChoiceListCountCaption "�����:"

//Choice

 #define dsDocDocChoiceCaption "������������ �������"
 #define dsDocDocChoiceOkBtnCaption "���������"
 #define dsDocDocChoiceCancelBtnCaption "������"

//ImportDoc

 #define dsDocDocImportDocFileFilter1 "����� ������|*.zxml|����� ������ DOS|*.let"
 #define dsDocDocImportDocFileFilter2 "����� ������|*.zxml"
 #define dsDocDocImportDocSourceCaption "��������: %s"
 #define dsDocDocImportDocLoadCaption "�������� ����� �������"
 #define dsDocDocImportDocOrgMissing "����������� [%s]"
 #define dsDocDocImportDocImportDataDefFileMissing "����������� ���� �������� ������������� ������"
 #define dsDocDocImportDocImportFileMissing "�� �������� ���� �������"
 #define dsDocDocImportDocDocDefCaption "�������� ���������� ..."
 #define dsDocDocImportDocReplaceDocDefConf1 "�������� ���������, ��� - \"%s\", ��� ����������.\r\n\r\n������ ������������ - \"%s\",\r\n�����  ������������ - \"%s\".\r\n\r\n����������� - \"%s\",\r\n�������� ������������ ��������?"
 #define dsDocDocImportDocReplaceDocDefConf2 "�������� ���������, ������������ - \"%s\", ��� ����������.\r\n\r\n������ ��� - \"%s\",\r\n����� ��� - \"%s\".\r\n\r\n����������� - \"%s\",\r\n�������� ������������ ��������?"
 #define dsDocDocImportDocDocCaption "���������..."
 #define dsDocDocImportDocReplaceDocConf1 "��������, ��� - \"%s\", ��� ����������.\r\n\r\n������������ ��������:\r\n%s\r\n����������� ��������:\r\n%s\r\n�������� ������������ ��������?"
 #define dsDocDocImportDocReplaceDocParam1 "\t������������\t\"%s\"\r\n\t�����������\t\"%s\"\r\n\t������\t\t\"%s\"\r\n"
 #define dsDocDocImportDocReplaceDocConf2 "��������, ������������ - \"%s\", ��� ����������.\r\n\r\n������������ ��������:\r\n%s\r\n����������� ��������:\r\n%s\r\n�������� ������������ ��������?"
 #define dsDocDocImportDocReplaceDocParam2 "\t���\t\t\"%s\"\r\n\t�����������\t\"%s\"\r\n\t������\t\t\"%s\"\r\n"

 #define dsDocDocImportDocBreakImportConf "�������� ���� ������?"

 #define dsDocDocImportDocCaption "����� �������� ����������, ����������"
 #define dsDocDocImportDocSpecTSCaption "�������� ����������"
 #define dsDocDocImportDocSpecNameCaption "�������� ���������"
 #define dsDocDocImportDocSpecOwnerCaption "�����"
 #define dsDocDocImportDocSpecPeridCaption "�������������"
 #define dsDocDocImportDocDocTSCaption "���������"
 #define dsDocDocImportDocDocNameCaption "��������"
 #define dsDocDocImportDocDocOwnerCaption "�����"
 #define dsDocDocImportDocDocPeriodCaption "������"
 #define dsDocDocImportDocOpenBtnCaption "�������"
 #define dsDocDocImportDocApplyBtnCaption "��������� � ��"
 #define dsDocDocImportDocCancelBtnCaption "�������"
 #define dsDocDocImportDocOperLabCaption "������� ������ \"�������\" � ������� �������� ������"

//ExportDoc

 #define dsDocDocExportDoc ""
 #define dsDocDocExportStatusSBPanels2 "��������:"

 #define dsDocDocExportDocFileFilter1 "����� ������|*.zxml|����� ������ DOS|*.let"
 #define dsDocDocExportDocFileFilter2 "����� ������|*.zxml"
 #define dsDocDocExportDocDocDefCaption "�������� ���������� ..."
 #define dsDocDocExportDocDocCaption "���������..."
 #define dsDocDocExportDocSaveCaption "���������� ������ �������� ..."
 #define dsDocDocExportDocBreakExportConf "�������� �������� ������?"

 #define dsDocDocExportDocCaption "�������� �������� ����������, ����������"
 #define dsDocDocExportDocExpSpecTSCaption "�������� ����������"
 #define dsDocDocExportDocLabel2Caption "��������� ������ ��������"
 #define dsDocDocExportDocLabel6Caption "�������������:"
 #define dsDocDocExportDocLabel1Caption "�����:"
 #define dsDocDocExportDocFilterPanelTopBtnHint "������ ������ ���������� ������ �������� ����������"
 #define dsDocDocExportDocFilterPanelBottomBtnHint "�������� ������ ���������� ������ �������� ����������"
 #define dsDocDocExportDocResetBtnHint "�������� ������ ���������� ������ �������� ����������"
 #define dsDocDocExportDocResetBtnCaption "�������� ��������� ������"
 #define dsDocDocExportDocSpecNameCaption "�������� ���������"
 #define dsDocDocExportDocSpecPeridCaption "�������������"
 #define dsDocDocExportDocExpDocTSCaption "���������"
 #define dsDocDocExportDocPeriodLabCaption "������ ������������:"
 #define dsDocDocExportDocLabel3Caption "�������������:"
 #define dsDocDocExportDocLabel7Caption "����� ���������:"
 #define dsDocDocExportDocLabel8Caption "��������� ������ ����������"
 #define dsDocDocExportDocDocFilterPanelBottomBtnHint "�������� ������ ���������� ������ ����������"
 #define dsDocDocExportDocDocFilterPanelTopBtnHint "������ ������ ���������� ������ ����������"
 #define dsDocDocExportDocDocResetBtnHint "�������� ������ ���������� ������ �������� ����������"
 #define dsDocDocExportDocDocResetBtnCaption "�������� ��������� ������"
 #define dsDocDocExportDocDocNameCaption "��������"
 #define dsDocDocExportDocDocPeriodCaption "������"
 #define dsDocDocExportDocSaveBtnCaption "��������"
 #define dsDocDocExportDocCancelBtnCaption "�������"


 #define dsDocDocExportDocPeriodicityCBItem1 ""
 #define dsDocDocExportDocPeriodicityCBItem2 "�� �������������"
 #define dsDocDocExportDocPeriodicityCBItem3 "�������������"

 #define dsDocDocExportDocDocPeriodicityCBItem1 ""
 #define dsDocDocExportDocDocPeriodicityCBItem2 ="�����������"
 #define dsDocDocExportDocDocPeriodicityCBItem3 ="����������"
 #define dsDocDocExportDocDocPeriodicityCBItem4 ="�����"
 #define dsDocDocExportDocDocPeriodicityCBItem5 ="�������"
 #define dsDocDocExportDocDocPeriodicityCBItem6 ="���������"
 #define dsDocDocExportDocDocPeriodicityCBItem7 ="���"

// HandEdit

 #define dsDocMatrixErrorParam "�������� ������ ����"
 #define dsDocMatrixErrorIndex "������ %s ������� �� ������� �������."
 #define dsDocHandEditErrorRow "������"
 #define dsDocHandEditErrorCol "�������"
 #define dsDocHandEditCheckRulesMsg "������ ������ (� ������ - %s; � ������� - %s)\r\n�� ������������� �������� �������� ��� %s � %s.\r\n������� ��������: %s"
 #define dsDocHandEditCheckRulesCpt "��������� ������ �� ������������� �������� ��������."

 #define dsDocHandEditStructHeaderCaption "  ��������� ���������"
 #define dsDocHandEditmiDocCaption      "��������"
 #define dsDocHandEditmiViewCaption     "���"
 #define dsDocHandEditmiJumpCaption     "�������"

 #define dsDocHandEditSaveCaption "&���������"
 #define dsDocHandEditSaveHint "���������"
 #define dsDocHandEditReCalcCaption "&�������� ����������� ��������"
 #define dsDocHandEditReCalcHint "�������� ����������� ��������"
 #define dsDocHandEditFirstDocElementCaption "�&����� ������� ���������"
 #define dsDocHandEditFirstDocElementHint "������ ������� ���������"
 #define dsDocHandEditPrevDocElementCaption "�&��������� ������� ���������"
 #define dsDocHandEditPrevDocElementHint "���������� ������� ���������"
 #define dsDocHandEditNextDocElementCaption "�&�������� ������� ���������"
 #define dsDocHandEditNextDocElementHint "��������� ������� ���������"
 #define dsDocHandEditLastDocElementCaption "�&�������� ������� ���������"
 #define dsDocHandEditLastDocElementHint "��������� ������� ���������"
 #define dsDocHandEditViewStructCaption "&��������� ���������"
 #define dsDocHandEditViewStructHint "��������� ���������"
 #define dsDocHandEditViewDocElementInscCaption "&������� ��� ��������� ���������"
 #define dsDocHandEditViewDocElementInscHint "������� ��� ��������� ���������"
 #define dsDocHandEditExitCaption "&�������"
 #define dsDocHandEditExitHint "�������"


#endif	
