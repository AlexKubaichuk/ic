//---------------------------------------------------------------------------
#ifndef dsDocDocListUnitH
#define dsDocDocListUnitH
//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxClasses.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxData.hpp"
#include "cxDataStorage.hpp"
#include "cxEdit.hpp"
#include "cxFilter.hpp"
#include "cxGraphics.hpp"
#include "cxGrid.hpp"
#include "cxGridCustomTableView.hpp"
#include "cxGridCustomView.hpp"
#include "cxGridLevel.hpp"
#include "cxGridTableView.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxNavigator.hpp"
#include "cxStyles.hpp"
#include "dxBar.hpp"
#include "dxStatusBar.hpp"
//---------------------------------------------------------------------------
#include "dsDocDMUnit.h"
//---------------------------------------------------------------------------
class PACKAGE TdsDocListForm : public TForm
{
        typedef map<int, UnicodeString> TIntMap;
__published:	// IDE-managed Components
        TActionList *MainAL;
        TAction *actDeleteDoc;
        TAction *actHandCreate;
        TAction *actPreview;
        TdxBarManager *ListTBM;
        TPopupMenu *MainPM;
        TMenuItem *N1;
        TMenuItem *N2;
        TdxStatusBar *StatusSB;
        TdxStatusBarContainerControl *dxStatusBar1Container0;
        TProgressBar *PrBar;
        TdxStatusBarContainerControl *dxStatusBar1Container1;
        TLabel *MsgLab;
 TPanel *ExtCompPanel;
        TMenuItem *N3;
 TcxStyleRepository *Style;
        TcxStyle *cxStyle1;
        TcxStyle *cxStyle2;
        TAction *actCheckByPeriod;
        TcxImageList *LMainIL;
        TdxBarLargeButton *dxBarLargeButton1;
        TdxBarLargeButton *dxBarLargeButton2;
        TdxBarLargeButton *dxBarLargeButton4;
        TdxBarLargeButton *dxBarLargeButton5;
        TAction *actClear;
        TdxBarLargeButton *dxBarLargeButton3;
        TdxBarCombo *SpecCB;
        TdxBarLargeButton *dxBarLargeButton6;
        TAction *actCreateDoc;
        TAction *actCreateDocByPeriod;
        TAction *actChoiceLibOpen;
 TdxBarPopupMenu *CreateDocByPerPM;
        TdxBarLargeButton *dxBarLargeButton7;
        TdxBarCombo *FltCB;
        TcxStyle *cxStyle3;
 TcxGrid *DocList;
 TcxGridTableView *DocListView;
 TcxGridLevel *DocListLevel1;
 TcxGridColumn *ldvNameCol;
 TcxGridColumn *ldvPeriodCol;
 TcxGridColumn *ldvAuthorCol;
 TcxGridColumn *ldvCreateDateCol;
 TTimer *ShowTimer;
 TAction *actClearFilterCB;
 TAction *actClearSpecCB;
 TdxBarLargeButton *dxBarLargeButton8;
 TdxBarLargeButton *dxBarLargeButton9;
 TdxBarLargeButton *dxBarLargeButton10;
 TcxStyle *cxStyle4;
 TAction *actImportDoc;
 TAction *actExportDoc;
 TdxBarLargeButton *dxBarLargeButton11;
 TdxBarLargeButton *dxBarLargeButton12;
 TdxBarLargeButton *dxBarLargeButton13;
 TAction *actRefresh;
 TdxBarPopupMenu *CreateDocPM;
 TAction *actCreateForHandEdit;
 TdxBarLargeButton *dxBarLargeButton14;
        void __fastcall actPreviewExecute(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall actDeleteDocExecute(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall actHandCreateExecute(TObject *Sender);
        void __fastcall DocListDBTableView1TcxGridDBDataControllerTcxDataSummaryDefaultGroupSummaryItems0GetText(
          TcxDataSummaryItem *Sender, const Variant &AValue,
          bool AIsFooter, UnicodeString &AText);
        void __fastcall actCheckByPeriodExecute(TObject *Sender);
        void __fastcall actClearExecute(TObject *Sender);
        void __fastcall actCreateDocExecute(TObject *Sender);
        void __fastcall actCreateDocByPeriodExecute(TObject *Sender);
        void __fastcall actChoiceLibOpenExecute(TObject *Sender);
        void __fastcall SpecCBChange(TObject *Sender);
 void __fastcall DocListViewTcxGridDataControllerTcxDataSummaryFooterSummaryItems0GetText(TcxDataSummaryItem *Sender,
          const Variant &AValue, bool AIsFooter, UnicodeString &AText);
 void __fastcall ShowTimerTimer(TObject *Sender);
 void __fastcall FormShow(TObject *Sender);
 void __fastcall DocListViewFocusedRecordChanged(TcxCustomGridTableView *Sender,
          TcxCustomGridRecord *APrevFocusedRecord, TcxCustomGridRecord *AFocusedRecord,
          bool ANewItemRecordFocusingChanged);
 void __fastcall actClearFilterCBExecute(TObject *Sender);
 void __fastcall actClearSpecCBExecute(TObject *Sender);
 void __fastcall actExportDocExecute(TObject *Sender);
 void __fastcall actImportDocExecute(TObject *Sender);
 void __fastcall actRefreshExecute(TObject *Sender);
 void __fastcall actCreateForHandEditExecute(TObject *Sender);

private:	// User declarations
        TdsDocDM *FDM;
        TIntMap  FSpecGUIMap;
  TkabCustomDataSource *FDataSrc;
  TTagNode     *RootClass;
  TdsRegEDContainer *FCtrList;
  UnicodeString FSaveCaption;
        TStringList *FMissingSpecDoc;

        UnicodeString curCD, delCode, FCheckOwnerCode;//,FPeriodWhere,FDateWhere, FOwnerWhere;
  __int64    xpCount,xcTop;
        TStringList *FltList;


  bool __fastcall CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg);
  void __fastcall FGetDataProgress(int AMax, int ACur, UnicodeString AMsg);
  void __fastcall RefreshById(UnicodeString ACode);
  void __fastcall CreateSrc();
  void __fastcall ChangeSrc();
  void __fastcall CheckCtrlState(bool AEnable);
  void __fastcall CheckCtrlVisible();
  bool __fastcall CreateListExt(TTagNode *itTag, UnicodeString &UID);
  TkabCustomDataSetRow* __fastcall CurRow();
  TkabCustomDataSetRow* __fastcall EditedRow();
  bool __fastcall IsRecSelected();

        void __fastcall FLoadChoice();
        void __fastcall FLoadSpec();
        UnicodeString __fastcall FGetSpecGUI();
        int __fastcall FGetFilterCode();
        int __fastcall FGetSpecType();
        void __fastcall quDocAfterScroll(TDataSet *DataSet);

public:		// User declarations
        __fastcall TdsDocListForm(TComponent* Owner, TdsDocDM *ADM, bool AHandEdit = false);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocListForm *dsDocListForm;
//---------------------------------------------------------------------------
#endif
