object dsDocPeriodForm: TdsDocPeriodForm
  Left = 842
  Top = 507
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1059#1082#1072#1078#1080#1090#1077' '#1087#1077#1088#1080#1086#1076' '#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1103' '#1076#1086#1082#1091#1084#1077#1090#1072':'
  ClientHeight = 129
  ClientWidth = 368
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  GlassFrame.Left = 3
  GlassFrame.Top = 3
  GlassFrame.Right = 3
  GlassFrame.Bottom = 45
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  DesignSize = (
    368
    129)
  PixelsPerInch = 96
  TextHeight = 13
  object DateFrLab: TLabel
    Left = 8
    Top = 57
    Width = 20
    Height = 16
    Caption = #1054#1090
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object DateToLab: TLabel
    Left = 162
    Top = 57
    Width = 20
    Height = 16
    Caption = #1044#1086
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object PeriodTypeLabel: TLabel
    Left = 8
    Top = 12
    Width = 61
    Height = 16
    Caption = #1055#1077#1088#1080#1086#1076':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object YearED: TcxSpinEdit
    Left = 305
    Top = 8
    ParentFont = False
    Properties.MaxValue = 9999.000000000000000000
    Properties.MinValue = 1900.000000000000000000
    Style.Color = clWindow
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 0
    Value = 1900
    OnKeyDown = FormKeyDown
    Width = 55
  end
  object DateFrDE: TcxDateEdit
    Left = 52
    Top = 53
    ParentFont = False
    Properties.ButtonGlyph.Data = {
      3E020000424D3E0200000000000036000000280000000D0000000D0000000100
      1800000000000802000000000000000000000000000000000000C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C000C56E35D6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAA
      D6ADAAD6ADAAD6ADAA00C56E36FFFFFFC0C0C0FFFFFFD3A9A7FFFFFFC0C0C0FF
      FFFFD3A6A6FFFFFFC0C0C0FFFFFFC1838300C56E36FFFFFFFFFFFFFFFFFFD6AE
      ACFFFFFFFFFFFFFFFFFFD5ABABFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7
      D7B2B7D6AEACD6ADAA0000FF0000FF0000FFD6ADAAD6AEACD7AFAFD6AEACC387
      8700C56E36FFFFFFC0C0C0FFFFFF0000FFFFFFFFC0C0C0FFFFFF0000FFFFFFFF
      C0C0C0FFFFFFC2868600C56E36FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
      FFFF0000FFFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7D7B2B7D7B2B7D6AD
      AA0000FF0000FF0000FFD6ADAAD7AFAFD7AFAFDBB7B7C3878700C56E36FFFFFF
      C0C0C0FFFFFFD6ADAAFFFFFFC0C0C0FFFFFFD5AAA9FFFFFFFFFFFFFFFFFFC386
      8500C56E36FFFFFFFFFFFFFFFFFFD8B7BEFFFFFFFFFFFFFFFFFFD7B4BCFFFFFF
      C0C0C0FFFFFFC38C9400C36C33C36C33C46D34C46D34C36D34C46D34C46D34C4
      6D34C36D34C46D34C46D34C46D34C36C3400C56C33C56C33C56C33C56C33CD69
      00CD6900CD6900CD6900CD6900CD6900CD6600CA5F00CD640000C36C33C36C33
      C36C33C46D33C46D34C46D33C46D33C46D33C46D33C46D33C46C33C36C33C36C
      3300}
    Properties.OnChange = DateFrDEPropertiesChange
    Style.Color = clWindow
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 1
    OnKeyDown = FormKeyDown
    Width = 90
  end
  object DateToDE: TcxDateEdit
    Left = 203
    Top = 53
    ParentFont = False
    Properties.ButtonGlyph.Data = {
      3E020000424D3E0200000000000036000000280000000D0000000D0000000100
      1800000000000802000000000000000000000000000000000000C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C000C56E35D6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAAD6ADAA
      D6ADAAD6ADAAD6ADAA00C56E36FFFFFFC0C0C0FFFFFFD3A9A7FFFFFFC0C0C0FF
      FFFFD3A6A6FFFFFFC0C0C0FFFFFFC1838300C56E36FFFFFFFFFFFFFFFFFFD6AE
      ACFFFFFFFFFFFFFFFFFFD5ABABFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7
      D7B2B7D6AEACD6ADAA0000FF0000FF0000FFD6ADAAD6AEACD7AFAFD6AEACC387
      8700C56E36FFFFFFC0C0C0FFFFFF0000FFFFFFFFC0C0C0FFFFFF0000FFFFFFFF
      C0C0C0FFFFFFC2868600C56E36FFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFF
      FFFF0000FFFFFFFFFFFFFFFFFFFFC3878700C56E36D7B2B7D7B2B7D7B2B7D6AD
      AA0000FF0000FF0000FFD6ADAAD7AFAFD7AFAFDBB7B7C3878700C56E36FFFFFF
      C0C0C0FFFFFFD6ADAAFFFFFFC0C0C0FFFFFFD5AAA9FFFFFFFFFFFFFFFFFFC386
      8500C56E36FFFFFFFFFFFFFFFFFFD8B7BEFFFFFFFFFFFFFFFFFFD7B4BCFFFFFF
      C0C0C0FFFFFFC38C9400C36C33C36C33C46D34C46D34C36D34C46D34C46D34C4
      6D34C36D34C46D34C46D34C46D34C36C3400C56C33C56C33C56C33C56C33CD69
      00CD6900CD6900CD6900CD6900CD6900CD6600CA5F00CD640000C36C33C36C33
      C36C33C46D33C46D34C46D33C46D33C46D33C46D33C46D33C46C33C36C33C36C
      3300}
    Properties.OnChange = DateFrDEPropertiesChange
    Style.Color = clWindow
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 2
    OnKeyDown = FormKeyDown
    Width = 90
  end
  object OkBtn: TcxButton
    Left = 194
    Top = 100
    Width = 80
    Height = 25
    Anchors = [akRight]
    Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
    Enabled = False
    TabOrder = 3
    OnClick = OkBtnClick
  end
  object CancelBtn: TcxButton
    Left = 280
    Top = 100
    Width = 80
    Height = 25
    Anchors = [akRight]
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 4
  end
  object PerTypeCB: TcxComboBox
    Left = 100
    Top = 8
    ParentFont = False
    Properties.DropDownListStyle = lsEditFixedList
    Properties.Items.Strings = (
      #1071#1085#1074#1072#1088#1100
      #1060#1077#1074#1088#1072#1083#1100
      #1052#1072#1088#1090
      #1040#1087#1088#1077#1083#1100
      #1052#1072#1081
      #1048#1102#1085#1100
      #1048#1102#1083#1100
      #1040#1074#1075#1091#1089#1090
      #1057#1077#1085#1090#1103#1073#1088#1100
      #1054#1082#1090#1103#1073#1088#1100
      #1053#1086#1103#1073#1088#1100
      #1044#1077#1082#1072#1073#1088#1100
      '------------------'
      #1043#1086#1076
      '------------------'
      #1048#1085#1090#1077#1088#1074#1072#1083' '#1076#1072#1090
      '------------------'
      '1-'#1077' '#1087#1086#1083#1091#1075#1086#1076#1080#1077
      '2-'#1077' '#1087#1086#1083#1091#1075#1086#1076#1080#1077
      '------------------'
      '1-'#1081' '#1082#1074#1072#1088#1090#1072#1083
      '2-'#1081' '#1082#1074#1072#1088#1090#1072#1083
      '3-'#1081' '#1082#1074#1072#1088#1090#1072#1083
      '4-'#1081' '#1082#1074#1072#1088#1090#1072#1083' ')
    Properties.OnChange = PerTypeCBPropertiesChange
    Style.Color = clWindow
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 5
    OnKeyDown = FormKeyDown
    Width = 193
  end
end
