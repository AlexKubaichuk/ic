//---------------------------------------------------------------------------
#ifndef dsDocDocCreateTypeUnitH
#define dsDocDocCreateTypeUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxRadioGroup.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGroupBox.hpp"
//---------------------------------------------------------------------------
class PACKAGE TdsDocDocCreateTypeForm : public TForm
{
__published:	// IDE-managed Components
 TPanel *Panel1;
 TcxButton *OkBtn;
 TcxButton *CancelBtn;
 TcxRadioGroup *CreateTypeRG;
 void __fastcall OkBtnClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        int  CreateType;
        __fastcall TdsDocDocCreateTypeForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocDocCreateTypeForm *dsDocDocCreateTypeForm;
//---------------------------------------------------------------------------
#endif
