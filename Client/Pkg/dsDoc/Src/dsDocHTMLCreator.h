/*
  ����        - ICSDocViewer.h
  ������      - ��������������� (ICS)
  ��������    - ����������� ���������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 18.10.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef dsDocHTMLCreatorH
#define dsDocHTMLCreatorH

//---------------------------------------------------------------------------

#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
//#include <Registry.hpp>

#include "DKClasses.h"
#include "XMLContainer.h"
#include "IcsXMLDoc.h"
#include "Preview.h"

#include "ICSDocGlobals.h"

namespace ICSDocViewer
{

//###########################################################################
//##                                                                       ##
//##                                 Globals                               ##
//##                                                                       ##
//###########################################################################

//---------------------------------------------------------------------------

// ��� ������� ��� �����������
enum TICSDocHTMLCreatorPreviewType
{
  ptUnknownEx,            // �� ������ ����, ���� �� ���� ����� :)
  ptSpecificatorEx,       // ���������� ������������
  ptDocumentEx            // ���������� ��������
};

//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                               TdsDocHTMLCreator                         ##
//##                                                                       ##
//###########################################################################

// ����������� ���������

class PACKAGE TdsDocHTMLCreator : public TObject
{
private:
    TTagNode*              FSpec;           //������������ ( xml-������ � ������ "specificator" )
    TTagNode*              FDoc;            //�������� ( xml-������ � ������ "docum" )
                                            //� ������ vmSaveToDisk
    UnicodeString             FHTML;           //HTML-���� � ���� ������
    TTagNode*              FndHTML;         //������ html-�����
    TAxeXMLContainer*      FXMLContainer;   //XML-��������� ( ��. AxeUtil.h ) � �������������
    bool                   FListZeroValCheckCountOnly; //������� ���������� �������
                                                       //����������� ������� ��������
                                                       //��� ������ ������ ���
                                                       //��������, ��������������� count'�
    //�������
                                                    //����������� �������������/���������

    //���� ��� ���������� ���� ( ������������ ��� �������� � ������ ������� ����� ������������ �������� )
    UnicodeString    __BorderWidth;    //������ ����� ������� ( xml-��� "table" ������������� )
                                    //��� ������ ( xml-��� "list" ������������� )
    UnicodeString    __Width;
    bool          __RowNumbers;     //������� ������������� ��������� ����� ������� ( xml-��� "table" ������������� )
                                    //��� ����� ������ ( xml-��� "list" ������������� )
    bool          __ColNumbers;     //������� ������������� ��������� �������� ������� ( xml-��� "table" ������������� )
    int           __RowNum;         //������� ����� ������ ������� ( xml-��� "table" ������������� )
    int           __ColNum;         //������� ����� ������� ������� ( xml-��� "table" ������������� )
    //���� ��� ���������� ����
    int        __PBMax;             //������������ ��� �������� ������ � ����� InternalSetPBMax()
    int        __PBPosition;        //������������ ��� �������� ������ � ����� InternalSetPBPosition()
    int           FTabNum;          //������� ����� �������
    int           FTabRowNum;       //������� ����� ������ �������
    int           FTabColNum;       //������� ����� ������� �������

    /*�������������*/
    TTagNode* __fastcall InitNDHTML( bool bCreateBodyTag = true );
    /*��������������� ������*/
    void __fastcall CreateHTMLProgressInit(int AMax, UnicodeString AMsg);
    void __fastcall CreateHTMLProgressChange(int APercent, UnicodeString AMsg);
    void __fastcall CreateHTMLProgressComplite(UnicodeString AMsg);

    //*****************************
    //*****************************
    UnicodeString __fastcall GetTableCellStyle( UnicodeString AStyleAttrName, TTagNode *ndCellTagNode, TTagNode *ndAltCellTagNode = NULL );
    UnicodeString __fastcall GetListStyle( UnicodeString AStyleAttrName, TTagNode *ndList );
    UnicodeString __fastcall GetListCellStyle( UnicodeString AStyleAttrName, TTagNode *ndCellTagNode );
    UnicodeString __fastcall GetInsTextStyle( TTagNode *ndInsText );
    UnicodeString __fastcall GetInsTextAlign( TTagNode *ndInsText );
    void       __fastcall NormalizeNumericalValue(UnicodeString &ACellText, const bool fShowZVal);
    void       __fastcall HiMetricToPixels(long *pnWidth, long *pnHeight);
    /*��������������� ������ ��� ������� HTML*/
    TTagNode* __fastcall AddHTMLTableTag( TTagNode *htmlParent );
    void             __fastcall GetHTML_DIVAlignAttrVal( UnicodeString AAlign, UnicodeString &AAttrName, UnicodeString &AAttrVal );
    TTagNode*        __fastcall CreateHTMLTextFormat( TTagNode *htmlTag, UnicodeString AStyleUID );
    bool      __fastcall SetHTMLTableCellBorderWidth( TTagNode *htmlTableCell, UnicodeString ABorderWidth );
    bool             __fastcall SetHTMLBoxWidthValues( TTagNode *htmlTableCell, UnicodeString ABorderWidth, UnicodeString AWidth );
    TTagNode*        __fastcall CreateHTMLTableNumCellTextFormat( TTagNode *htmlNumTableCell );


protected:
    /*TdsDocHTMLCreator*/
    bool __fastcall CheckIfTerminated();
    //*****************************
    UnicodeString __fastcall GetListColHeaderText( TTagNode *ndListCol );
    UnicodeString __fastcall GetTableHeaderCellText( TTagNode *ndHeaderCell );
    UnicodeString __fastcall GetDocListCellText( TTagNode* &ndV, const TTagNode *ndListCol, const bool bShowZVal );
    long __fastcall GetListColsMaxLevel( const TTagNode *ANode ) const;
    int __fastcall GetMaxPBForSpec();
    int __fastcall GetMaxPBForDoc();
    /*��������������� ������ ��� ������� HTML*/
    UnicodeString __fastcall GetHTMLStyleDefinition( UnicodeString  AStyleClass,
                                                  UnicodeString  AStyleClassStrPrefix,
                                                  UnicodeString  AStylePropStrPrefix,
                                                  TTagNode*         ndStyle );
    UnicodeString __fastcall GetHTMLSTYLESDefinition( UnicodeString  AStyleClassStrPrefix,
                                                   UnicodeString  AStylePropStrPrefix,
                                                   TTagNode*         ndStyles );
    void __fastcall CreateHTML( TICSDocHTMLCreatorPreviewType APreviewType );
    void __fastcall CreateHTMLListHeaderCell( TTagNode *ndListEl, TTagNode *htmlList, TTagNode *htmlRow = NULL );
    void __fastcall CreateHTMLListHeader( TTagNode *ndListEl, TTagNode *htmlList, ICSDocGlobals::TTagNodeList *AListSingleCols = NULL );
    void __fastcall CreateHTMLListBodyRow( const ICSDocGlobals::TTagNodeList *AListSingleCols, TTagNode *htmlList, ICSDocGlobals::TTagNodeList *ADataCells = NULL );
    TTagNode* __fastcall CreateHTMLTableHeaderCell( UnicodeString  ACellTagNodeName,
                                                    TTagNode*         ndCellTagNode,
                                                    TTagNode*         htmlRow );
    void __fastcall CreateHTMLTableHeaderCols( TTagNode *ndCol, TTagNode *htmlTable, TTagNode *htmlRow = NULL );
    void __fastcall CreateHTMLTableCols( TTagNode *ndColumns, TTagNode *htmlTable, ICSDocGlobals::TTagNodeList *ATableSingleCols = NULL );
    void __fastcall CreateHTMLTableRows( TTagNode *ndRow, TTagNode *htmlTable, const ICSDocGlobals::TTagNodeList &ATableSingleCols, ICSDocGlobals::TTagNodeList2 *DataCells, TTagNode *htmlRow = NULL );
    TTagNode* __fastcall AddHTMLInscription( TTagNode *ndInscription, TTagNode *htmlParent );
    TTagNode* __fastcall AddHTMLInsText( TTagNode *ndInsText, TTagNode **htmlParent );
    TTagNode* __fastcall AddHTMLList( TTagNode *ndList, TTagNode *htmlParent, ICSDocGlobals::TTagNodeList *AListSingleCols = NULL, int nRowCount = 1 );
    TTagNode* __fastcall AddHTMLTable( TTagNode *ndTable, TTagNode *htmlParent, ICSDocGlobals::TTagNodeList2 *DataCells = NULL );
    void __fastcall CreateSpecHTML();
    void __fastcall CreateDocHTML();

public:
    __fastcall TdsDocHTMLCreator();
    virtual __fastcall ~TdsDocHTMLCreator();

    //������������ ����������� ���������/������������� � ������ �������
    //���������������� ���������
    UnicodeString __fastcall Preview(TAxeXMLContainer*  AXMLContainer, TTagNode*  ASpecificator, TTagNode*  ADocument = NULL);
};

//---------------------------------------------------------------------------

} // end of namespace ICSDocViewer
using namespace ICSDocViewer;

#endif
