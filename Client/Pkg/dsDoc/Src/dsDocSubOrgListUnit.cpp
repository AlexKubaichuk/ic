//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsDocSubOrgListUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "pkgsetting.h"
#ifndef _CONFSETTTING
 #error _CONFSETTTING using are not defined
#endif

#include "dsDocSubOrgEditUnit.h"
#include "msgdef.h"
#include "dsDocClientConstDef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxClasses"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxData"
#pragma link "cxDataStorage"
#pragma link "cxEdit"
#pragma link "cxFilter"
#pragma link "cxGraphics"
#pragma link "cxGrid"
#pragma link "cxGridCustomTableView"
#pragma link "cxGridCustomView"
#pragma link "cxGridLevel"
#pragma link "cxGridTableView"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxNavigator"
#pragma link "cxStyles"
#pragma link "dxBar"
#pragma link "dxStatusBar"
#pragma resource "*.dfm"

#ifndef _CONFSETTTING
 #error _CONFSETTTING using are not defined
#endif
TdsDocSubOrgForm *dsDocSubOrgForm;
//---------------------------------------------------------------------------
__fastcall TdsDocSubOrgForm::TdsDocSubOrgForm(TComponent* Owner, TdsDocDM *ADM, bool AUseLPUClass)
        : TForm(Owner)
{
  FDM = ADM;
  FDataSrc = NULL;

  RootClass = FDM->RegDef->GetTagByUID("0004");
//  if (RootClass) RootClass = RootClass->GetFirstChild();

  FCtrList = new TdsRegEDContainer(this, this/*ExtCompPanel*/);
  FCtrList->DefXML           = RootClass;
  FCtrList->StyleController  = FDM->StyleController;

  FCtrList->DataProvider = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider*)FCtrList->DataProvider)->DefXML = RootClass;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  FCtrList->DataPath        = FDM->DataPath;

//  FLastTop = 0;
//  FLastHeight = 0;

  curCD = 0;
  CheckCtrlVisible();
  CreateSrc();
  FUseLPUClass = AUseLPUClass;

  Caption = FMT(dsDocDocOrgListCaption);

  MainTBM->Bars->Items[0]->Caption = FMT(dsDocDocOrgListMainTBMBar1Caption);
  MainTBM->Bars->Items[1]->Caption = FMT(dsDocDocOrgListMainTBMBar2Caption);

  StatusSB->Panels->Items[0]->Text = FMT(dsDocDocChoiceListCountCaption);

  //  ChangeSrc();
/*
  SubordOrgTV->DataController->DataSource = FDM->srcOrg;


  FDM->quOrg->Filtered = false;
  FDM->quOrg->Filter = "SUBORD<>2";
  FDM->quOrg->Filtered = true;
  ChangeSrc();
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmSubordOrg"); //# ������ ����������������� �����������.
#endif
*/
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgForm::CreateSrc()
{
  Application->ProcessMessages();
  try
   {
     try
      {
        Caption = RootClass->AV["name"];
        FDataSrc = new TkabCustomDataSource(RootClass);

        FDataSrc->DataSet->OnGetCount   = FDM->kabCDSGetCount;
        FDataSrc->DataSet->OnGetIdList  = FDM->kabCDSGetCodes;
        FDataSrc->DataSet->OnGetValById = FDM->kabCDSGetValById;
        FDataSrc->DataSet->OnGetValById10 = FDM->kabCDSGetValById10;

        FDataSrc->DataSet->OnInsert = FDM->kabCDSInsert;
        FDataSrc->DataSet->OnEdit   = FDM->kabCDSEdit;
        FDataSrc->DataSet->OnDelete = FDM->kabCDSDelete;

        FDataSrc->SetColumns(SubordOrgListView, "0022:������������\n002A:�������\n0029:��. �����\n0023:�����\n000D:���\n002B:������ ������������\n002D:�.�.�. ������������");

        SubordOrgListView->DataController->CustomDataSource = FDataSrc;
        ((TdsRegEDkabDSDataProvider*)FCtrList->DataProvider)->DataSource = FDataSrc;
      }
     catch (Exception &E)
      {
        MessageBox(Handle,cFMT1(icsErrorMsgCaptionSys,E.Message),cFMT(icsErrorMsgCaption),MB_ICONSTOP);
      }
     catch (...)
      {
        MessageBox(Handle,cFMT(icsRegistryErrorCommDBErrorMsgCaption),cFMT(icsErrorMsgCaption),MB_ICONSTOP);
      }
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgForm::ChangeSrc()
{
  CheckCtrlState(false);
  TTime FBegTime = Time();
  UnicodeString _ES_ = "";
//  UnicodeString TimeMsg = "";
  Application->ProcessMessages();
  try
   {
     try
      {
        SubordOrgListView->BeginUpdate();
        try
         {
           FDataSrc->DataSet->GetData("{\"0024\":\"0\"}", TdsRegFetch::Auto, 20, FGetDataProgress);
           FBegTime = Time();
           FDataSrc->DataChanged();
         }
        __finally
         {
           SubordOrgListView->EndUpdate();
           MsgLab->Caption = MsgLab->Caption+" ���������: "+(Time() - FBegTime).FormatString("ss.zzz");
         }

        if(curCD.Length())
         {
           TLocateOptions opt;
//          if(!quClass()->Locate("CODE",Variant(curCD),opt))
//           quClass()->First();
         }
//        else
//          quClass()->First();

//        RootClass->Iterate(CreateListExt,_ES_);

      }
     catch (Exception &E)
      {
        MessageBox(Handle,cFMT1(icsErrorMsgCaptionSys,E.Message),cFMT(icsErrorMsgCaption),MB_ICONSTOP);
      }
     catch (...)
      {
        MessageBox(Handle,cFMT(icsRegistryErrorCommDBErrorMsgCaption),cFMT(icsErrorMsgCaption),MB_ICONSTOP);
      }
   }
  __finally
   {
     CheckCtrlState(true);
     Application->ProcessMessages();
     ActiveControl = SubordOrgList;
//    _MSG_DBG(TimeMsg);
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgForm::FGetDataProgress(int AMax, int ACur, UnicodeString AMsg)
{
  if (AMax == -1)
   {
     PrBar->Position = 0;
     MsgLab->Caption = "";
   }
  else if (!(AMax+ACur))
   {
     PrBar->Position = 0;
     MsgLab->Caption = AMsg;
   }
  else
   {
     MsgLab->Caption = AMsg+ " ["+IntToStr(ACur)+"/"+IntToStr(AMax)+"]";
     if (PrBar->Max != AMax) PrBar->Max = AMax;
     PrBar->Position = ACur;
   }
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgForm::ShowEditDlg()
{
  TdsDocSubOrgEditForm *Dlg = new TdsDocSubOrgEditForm(this, FDataSrc, FDM);
  try
   {
     try
      {
        Dlg->OnGetFillLPUData = FDM->FillLPUData;
        Dlg->OnGetLPUData = FDM->GetLPUData;

        if(Dlg->ShowModal() == mrOk)
         {
           curCD = Dlg->RecId;
           RefreshById(curCD);
         }
      }
     catch (...)
      {
        _MSG_ERR(FMT(dsDocDocOrgListErrorInsertRecMsg),FMT(dsDocCommonErrorCaption));
      }
   }
  __finally
   {
     ActiveControl = SubordOrgList;
     delete Dlg;
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgForm::actInsertExecute(TObject *Sender)
{
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmSubordOrgInsert"); //# ������ ����������������� �����������. ����������.
#else
  ShowEditDlg();
#endif
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgForm::actDeleteExecute(TObject *Sender)
{
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmSubordOrgDelete");  //# ������ ����������������� �����������. ��������.
#else
   try
    {
      try
       {
         /*
         try
          {
             FDM->qtExecDoc("Select GUI From SPECIFIKATORS Where F_OWNER='"+FDM->quOrgCODE->AsString+"'",false);
             if (FDM->quFreeDoc->RecordCount)
              {
                _MSG_ERR(FMT(dsDocDocOrgListErrorDelDocDef),FMT(dsDocCommonErrorDelCaption));
                ActiveControl = SubordOrgList;
                return;
              }
             FDM->qtExecDoc("Select GUI From DOCUMENTS Where F_OWNER='"+FDM->quOrgCODE->AsString+"'",false);
             if (FDM->quFreeDoc->RecordCount)
              {
                _MSG_ERR(FMT(dsDocDocOrgListErrorDelDoc),FMT(dsDocCommonErrorDelCaption));
                ActiveControl = SubordOrgList;
                return;
              }
          }
         __finally
          {                                              VAriant
            FDM->qtDocCommit();
          }      */    ///!!!
         UnicodeString FOrgCode = VarToStr(CurRow()->StrValue[SubOrgCODE]).UpperCase();
         if (!(
                    (FOrgCode == "812.2741381.000") ||
                    (FOrgCode == "812.1107704.000") ||
                    (FOrgCode == "812.7107704.000") ||
                    (FOrgCode == "812.7177704.000") ||
                    (FOrgCode == "812.7170425.000") ||
                    (FOrgCode == "812.7100425.000") ||
                    (FOrgCode == "812.1100425.000") ||
                    (FOrgCode == "812.7171319.000")
         ))
          {
            if(_MSG_QUE(FMT2(dsDocCommonDelConfMsg,
                              FMT(dsDocDocOrgListOrgCaption),
                              "\"" + CurRow()->Value[SubOrgNAME] + "\""),
                        FMT(dsDocCommonConfRecDelCaption)) == IDYES)
             {
               curCD = CurRow()->Value[SubOrgCODE];
               FDataSrc->DataSet->Delete();
             }
          }
       }
      catch(Exception &E)
       {
         _MSG_ERR(E.Message,FMT(dsDocCommonErrorDelCaption));
//         FDM->quOrg->Cancel();
       }
      catch (...)
       {
         _MSG_ERR(FMT1(dsDocCommonErrorDelMsg,
                         FMT(dsDocDocOrgListRecCaption)),
                  FMT(dsDocCommonErrorCaption));
//         FDM->trOrg->Rollback();
       }
    }
   __finally
    {
      ActiveControl = SubordOrgList;
//      FDM->quOrg->EnableControls();
      ChangeSrc();
    }
#endif
}
//---------------------------------------------------------------------------
/*
void __fastcall TdsDocSubOrgForm::ChangeSrc()
{
 actInsert->Enabled = false;
 actEdit->Enabled   = false;
 actDelete->Enabled = false;
 try
  {
    if (FDM->trOrg->Active) FDM->quOrg->Cancel();
    FDM->quOrg->DisableControls();
    FDM->quOrg->AfterScroll = NULL;

    FDM->quOrg->Close();
    if (!FDM->trOrg->Active)  FDM->trOrg->StartTransaction();
    FDM->quOrg->Prepare();
    FDM->quOrg->Open();
    if(curCD.Length())
     {
      TLocateOptions opt;
      if(!FDM->quOrg->Locate("CODE",Variant(curCD),opt))
       FDM->quOrg->First();
     }
    else
      FDM->quOrg->First();
    int FRecCount = FDM->GetCount("SUBORDORG",FDM->quOrg->Filter);
    StatusSB->Panels->Items[1]->Text = IntToStr(FRecCount);
    if (FRecCount)
     {
       actInsert->Enabled = true;
       actEdit->Enabled   = true;
       actDelete->Enabled = true;
     }
    else
     {
       actInsert->Enabled = true;
       actEdit->Enabled   = false;
       actDelete->Enabled = false;
     }
  }
 catch (...)
  {
   _MSG_ERR(FMT(dsDocCommonErrorDBWorkMsg),cFMT(dsDocCommonErrorCaption));
   FDM->trOrg->Rollback();
  }
 ActiveControl = SubordOrgList;
 FDM->quOrg->EnableControls();
}
 */
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgForm::actCloseExecute(TObject *Sender)
{
  ModalResult = mrCancel;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgForm::CheckCtrlState(bool AEnable)
{
  if (!AEnable)
   {
     actDelete->Enabled = false;
   }
  else if (SubordOrgListView->DataController->CustomDataSource && FDataSrc)
   {
     if (IsRecSelected())
      {
        actDelete->Enabled = AEnable && FDM->ClassEditable && FDM->FullEdit;
        UnicodeString FOrgCode = VarToStr(CurRow()->StrValue[SubOrgCODE]).UpperCase();
        actDelete->Enabled = !(
                   (FOrgCode == "812.2741381.000") ||
                   (FOrgCode == "812.1107704.000") ||
                   (FOrgCode == "812.7107704.000") ||
                   (FOrgCode == "812.7177704.000") ||
                   (FOrgCode == "812.7170425.000") ||
                   (FOrgCode == "812.7100425.000") ||
                   (FOrgCode == "812.1100425.000") ||
                   (FOrgCode == "812.7171319.000")
        );
      }
     else
      {
        actDelete->Enabled = false;
      }
   }


/*
  actInsert->Enabled = AEnable && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("insertlabel"));
  actRefresh->Enabled           = AEnable;
  actFind->Enabled              = AEnable && actFind->Visible;

  actTemplate->Enabled          = AEnable && actTemplate->Visible && actInsert->Enabled&&actEdit->Enabled&&actDelete->Enabled;
  actSetFilter->Enabled         = AEnable && actSetFilter->Visible;
  actReSetFilter->Enabled       = AEnable && actReSetFilter->Visible;
  actFilterSetting->Enabled     = AEnable && actFilterSetting->Visible;

  actOpen->Enabled              = AEnable && actOpen->Visible;
  actExt->Enabled               = AEnable && actExt->Visible;
  actSelAll->Enabled            = AEnable && actSelAll->Visible;

  PrintBI->Enabled              = AEnable;
  actPrintListSetting->Enabled  = AEnable && actPrintListSetting->Visible;
  StatusSB->Enabled             = AEnable;
  FltList->Enabled            = AEnable;
*/

/*  actInsert->Enabled            = AEnable && FDM->ClassEditable && (SaveEnInsert&&RootClass);
  actEdit->Enabled              = AEnable && FDM->ClassEditable && (SaveEnEdit&&RootClass);
  actDelete->Enabled            = AEnable && FDM->ClassEditable && (SaveEnDelete&&RootClass);
  actRefresh->Enabled           = AEnable && SaveEnRefresh&&RootClass;
  actFind->Enabled              = AEnable && SaveEnFind&&RootClass;
  actTemplate->Enabled          = AEnable && SaveEnSetTemplate&&RootClass&&actInsert->Enabled&&actEdit->Enabled&&actDelete->Enabled;
  actSetFilter->Enabled         = AEnable && SaveEnSetTemplate&&RootClass;
  actReSetFilter->Enabled       = AEnable && SaveEnReSetTemplate&&RootClass;
  actOpen->Enabled              = AEnable && SaveEnReSetTemplate&&RootClass;
  actSelAll->Enabled            = AEnable && SaveEnReSetTemplate&&RootClass;
  actFilterSetting->Enabled     = AEnable && SaveEnReSetTemplate&&RootClass;
  actPrintListSetting->Enabled  = AEnable && SaveEnReSetTemplate&&RootClass;
  Application->ProcessMessages();       */
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgForm::CheckCtrlVisible()
{
/*
  actOpen->Visible             = FDM->OnOpenCard;
  actSelAll->Visible           = false;
  actFilterSetting->Visible    = FDM->OnQuickFilterList;

  actPrintListSetting->Visible = FDM->OnReportList && FDM->OnPrintReportClick;
  if (!actPrintListSetting->Visible)
   PrintBI->ButtonStyle = bsDefault;

  bool CanFind = false;
  TTagNode *UnitNode = RootClass->GetChildByName("description")->GetFirstChild()->GetFirstChild();
  while (UnitNode)
   {
     if (UnitNode->GetTagByUID(UnitNode->AV["ref"]))
      {
        if (UnitNode->GetTagByUID(UnitNode->AV["ref"])->CmpName("text,digit"))
         {
           CanFind = true;
           break;
         }
      }
     UnitNode = UnitNode->GetNext();
   }
  actFind->Visible     = CanFind;
  if (!RootClass->GetChildByName("description")->GetChildByName("maskfields"))
   {
     actTemplate->Visible = false;
     actSetFilter->Visible = false;
     actReSetFilter->Visible = false;
   }

   */
/*  actTemplate->Visible          = AEnable && SaveEnSetTemplate&&RootClass&&actInsert->Enabled&&actEdit->Enabled&&actDelete->Enabled;
  actSetFilter->Visible         = AEnable && SaveEnSetTemplate&&RootClass;
  actReSetFilter->Visible       = AEnable && SaveEnReSetTemplate&&RootClass;

  */
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
bool __fastcall TdsDocSubOrgForm::CreateListExt(TTagNode *itTag, UnicodeString &UID)
{
   TdsRegLabItem *tmpItem;
   if (!itTag->CmpName("class,description,actuals"))
    {
      if (itTag->CmpAV("inlist","e"))
       {
//         tmpItem = FCtrList->AddLabItem(itTag, ExtCompPanel->Width, ExtCompPanel);
//         tmpItem->Top = xcTop; xcTop += tmpItem->Height;
//         if (itTag->CmpName("extedit"))
//          tmpItem->OnGetExtData = FDM->OnGetExtData;
       }
    }
  return false;
}
//---------------------------------------------------------------------------

void __fastcall TdsDocSubOrgForm::ShowTimerTimer(TObject *Sender)
{
  ShowTimer->Enabled = false;
  Application->ProcessMessages();
  ChangeSrc();
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgForm::FormShow(TObject *Sender)
{
  SubordOrgListView->DataController->CustomDataSource = FDataSrc;
  ShowTimer->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgForm::FormClose(TObject *Sender, TCloseAction &Action)
{
  SubordOrgListView->DataController->CustomDataSource = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgForm::FormDestroy(TObject *Sender)
{
  SubordOrgListView->DataController->CustomDataSource = NULL;
  delete FCtrList->DataProvider;
  delete FCtrList; FCtrList = NULL;
  delete FDataSrc; FDataSrc = NULL;
}
//---------------------------------------------------------------------------
bool __fastcall TdsDocSubOrgForm::IsRecSelected()
 {
  return SubordOrgListView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgForm::SubordOrgListViewFocusedRecordChanged(TcxCustomGridTableView *Sender,
          TcxCustomGridRecord *APrevFocusedRecord, TcxCustomGridRecord *AFocusedRecord,
          bool ANewItemRecordFocusingChanged)
{
  if (IsRecSelected())
   FDataSrc->DataSet->GetDataById(VarToStr(FDataSrc->DataSet->CurRow->Value[SubOrgCODE]), FGetDataProgress);
  CheckCtrlState(true);
//  FCtrList->LabDataChange();

//  actEdit->Enabled   = ClassGridView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1) && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("editlabel"));
//  actDelete->Enabled = ClassGridView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1) && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("deletelabel"));
}
//---------------------------------------------------------------------------
TkabCustomDataSetRow* __fastcall TdsDocSubOrgForm::CurRow()
{
  TkabCustomDataSetRow* RC = NULL;
  try
   {
     if (FDataSrc)
      RC = FDataSrc->DataSet->CurRow;
     else
      throw Exception(FMT(dsDocDocSpecListNoDataSrc));
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TkabCustomDataSetRow* __fastcall TdsDocSubOrgForm::EditedRow()
{
  TkabCustomDataSetRow* RC = NULL;
  try
   {
     if (FDataSrc)
      if (FDataSrc->DataSet->EditedRow)
        RC = FDataSrc->DataSet->EditedRow;

     if (!RC)
      throw Exception(FMT(dsDocDocSpecListNoDataSrc));
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgForm::RefreshById(UnicodeString ACode)
{
  SubordOrgListView->BeginUpdate();
  FDataSrc->DataSet->GetDataById(ACode, FGetDataProgress);
  FDataSrc->DataChanged();
  StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
//  __int64 FCodeIdx = FDataSrc->DataSet->GetRowIdxByCode(ACode);
//  SubordOrgListView->DataController->FocusedRowIndex = FDataSrc->DataController->GetRowIndexByRecordIndex(FCodeIdx, true);
  SubordOrgListView->EndUpdate();
  SubordOrgListView->DataController->FocusedRecordIndex = FDataSrc->DataSet->GetRowIdxByCode(ACode);
}
//---------------------------------------------------------------------------

