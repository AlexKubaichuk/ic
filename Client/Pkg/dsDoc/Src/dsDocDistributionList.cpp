//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsDocDistributionList.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma resource "*.dfm"
TExtDistForm *ExtDistForm;
//---------------------------------------------------------------------------
__fastcall TExtDistForm::TExtDistForm(TComponent* Owner, UnicodeString ACaption, TStringList *ADest, TStringList *ACommSrc, bool AUseDef)
 : TForm(Owner)
{
  FUseDef = AUseDef;
  Caption = ACaption;
  DefCB->Properties->Items->Clear();
  UnicodeString FCode, FStr;
  TcxTreeListNode* tmpNode;
  FIdx = 1;
  for (int i = 0; i < ADest->Count; i++)
   {
     FCode = GetLPartB(ADest->Strings[i],'=');
     FStr  = GetRPartB(ADest->Strings[i],'=');
     if (FUseDef)
      {
        FDefCodes[FIdx] = FCode;
        DefCB->Properties->Items->AddObject(FStr, (TObject*)FIdx);
      }
     FExistMap[FCode.UpperCase()] = FStr;

     tmpNode = CurTL->Root->AddChild();
     tmpNode->Texts[0] = FStr;
     tmpNode->Texts[1] = "0";
     tmpNode->Texts[2] = FCode.UpperCase();
     FIdx++;
   }
  if (FUseDef)
   {
     ((TStringList*)DefCB->Properties->Items)->Sort();
     int FItemIdx = DefCB->Properties->Items->IndexOfObject((TObject*)1);
     if (FItemIdx != -1)
       DefCB->ItemIndex = FItemIdx;
   }
  else
   {
     DefPanel->Visible = false;
   }
  TAnsiStrMap::iterator FRC;
  for (int i = 0; i < ACommSrc->Count; i++)
   {
     FCode = GetLPartB(ACommSrc->Strings[i],'=');
     FStr  = GetRPartB(ACommSrc->Strings[i],'=');
     FRC = FExistMap.find(FCode.UpperCase());
     if (FRC == FExistMap.end())
      {
        FCommonMap[FCode.UpperCase()] = FStr;

        tmpNode = ComTL->Root->AddChild();
        tmpNode->Texts[0] = FStr;
        tmpNode->Texts[1] = "0";
        tmpNode->Texts[2] = FCode.UpperCase();
      }
   }
  FDefIdx = 1;
}
//---------------------------------------------------------------------------
void __fastcall TExtDistForm::ComTLDragDrop(TObject *Sender, TObject *Source, int X,
          int Y)
{
  if (((TcxTreeList*)Source)->Name == "CurTL")
   actDeleteSpecExecute(actDeleteSpec);
}
//---------------------------------------------------------------------------
void __fastcall TExtDistForm::ComTLDragOver(TObject *Sender, TObject *Source, int X,
          int Y, TDragState State, bool &Accept)
{
  Accept = (((TcxTreeList *)Source)->Name == "CurTL");
}
//---------------------------------------------------------------------------
void __fastcall TExtDistForm::CurTLDragDrop(TObject *Sender, TObject *Source, int X,
          int Y)
{
  if (((TcxTreeList*)Source)->Name == "ComTL")
   actAddSpecExecute(actAddSpec);
}
//---------------------------------------------------------------------------
void __fastcall TExtDistForm::CurTLDragOver(TObject *Sender, TObject *Source, int X,
          int Y, TDragState State, bool &Accept)
{
  Accept = (((TcxTreeList *)Source)->Name == "ComTL");
}
//---------------------------------------------------------------------------
void __fastcall TExtDistForm::actAddSpecExecute(TObject *Sender)
{
  if (ComTL->SelectionCount)
   {
     TcxTreeListNode* FSrc  = ComTL->Selections[0];
     TcxTreeListNode* FDest = CurTL->Root->AddChild();
     FDest->Texts[0] = FSrc->Texts[0];
     FDest->Texts[1] = "1";
     FDest->Texts[2] = FSrc->Texts[2];
     if (FUseDef)
      {
        FDefCodes[FIdx] = FSrc->Texts[2];
        DefCB->Properties->Items->AddObject(FSrc->Texts[0], (TObject*)FIdx);
        FIdx++;
      }
     FExistMap[FSrc->Texts[2]] = FSrc->Texts[0];
     FCommonMap.erase(FSrc->Texts[2]);
     FSrc->Delete();
   }
}
//---------------------------------------------------------------------------
void __fastcall TExtDistForm::actDeleteSpecExecute(TObject *Sender)
{
  if (CurTL->SelectionCount)
   {
     TcxTreeListNode* FSrc  = CurTL->Selections[0];
     TcxTreeListNode* FDest = ComTL->Root->AddChild();
     FDest->Texts[0] = FSrc->Texts[0];
     FDest->Texts[1] = "1";
     FDest->Texts[2] = FSrc->Texts[2];
     if (FUseDef)
      {
        int FindIdx = -1;
        for (TGUIIdxMap::iterator i = FDefCodes.begin(); (i != FDefCodes.end()) && (FindIdx == -1); i++)
         {
           if (i->second == FSrc->Texts[2])
            FindIdx = i->first;
         }
        if (FindIdx != -1)
         {
           FDefCodes.erase(FindIdx);
           FindIdx = DefCB->Properties->Items->IndexOfObject((TObject*)FindIdx);
           if (FindIdx != -1)
            DefCB->Properties->Items->Delete(FindIdx);
         }
      }
     FCommonMap[FSrc->Texts[2]] = FSrc->Texts[0];
     FExistMap.erase(FSrc->Texts[2]);

     FSrc->Delete();
   }
}
//---------------------------------------------------------------------------
void __fastcall TExtDistForm::actApplyExecute(TObject *Sender)
{
  if (FUseDef)
   {
     if (DefCB->ItemIndex != -1)
      ModalResult = mrOk;
     else
      _MSG_ERR("���������� ������� �������� �� ���������.","������");
   }
  else
   ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TExtDistForm::actCancelExecute(TObject *Sender)
{
  ModalResult = mrCancel;
}
//---------------------------------------------------------------------------
TJSONObject* __fastcall TExtDistForm::FGetList()
{
  TJSONObject* RC = new TJSONObject;
  try
   {
     // ���� ���� �������� �� ���������, �� ����� ��� ���������� ������ � ������
     UnicodeString FDefCode = "";
     if (FUseDef)
      {
        if (DefCB->ItemIndex != -1)
         {
           TGUIIdxMap::iterator FCodeRC = FDefCodes.find((int)DefCB->Properties->Items->Objects[DefCB->ItemIndex]);
           if (FCodeRC != FDefCodes.end())
            {
              TAnsiStrMap::iterator FRC = FExistMap.find(FCodeRC->second);
              if (FRC != FExistMap.end())
               {
                 RC->AddPair(FRC->first, FRC->second);
                 FDefCode = FRC->first;
               }
            }
         }
      }
     for (TAnsiStrMap::iterator i = FExistMap.begin();i != FExistMap.end(); i++)
      {
        if (!(FUseDef && (FDefCode == i->first)))
         RC->AddPair(i->first, i->second);
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

