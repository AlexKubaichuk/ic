//---------------------------------------------------------------------------
#ifndef dsDocPackPeriodUnitH
#define dsDocPackPeriodUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxCalendar.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDateUtils.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "dxCore.hpp"
//---------------------------------------------------------------------------
/*
#include "cxButtons.hpp"
#include "cxCalendar.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDateUtils.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "dxCore.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
#include "cxClasses.hpp"
*/
//---------------------------------------------------------------------------
class PACKAGE TdsDocPackPeriodForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel3;
        TcxComboBox *PerDataCB;
        TLabel *DateFrLab;
        TcxDateEdit *DateFrDE;
        TLabel *DateToLab;
        TcxDateEdit *DateToDE;
        TcxComboBox *PerTypeCB;
        TLabel *Label1;
        TcxMaskEdit *YearED;
        TcxCheckBox *YearChB;
        TLabel *Label2;
        TBevel *Bevel1;
        TLabel *ToFormatLab;
        TcxMaskEdit *ShiftED;
        TPanel *Panel1;
        TLabel *Label3;
        TcxComboBox *CreateTypeCB;
        TBevel *Bevel2;
 TcxButton *OkBtn;
 TcxButton *CancelBtn;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall PerDataCBPropertiesChange(TObject *Sender);
        void __fastcall PerTypeCBPropertiesChange(TObject *Sender);
        void __fastcall YearEDPropertiesValidate(TObject *Sender,
          Variant &DisplayValue, TCaption &ErrorText, bool &Error);
        void __fastcall YearChBPropertiesChange(TObject *Sender);
        void __fastcall ShiftEDPropertiesValidate(TObject *Sender,
          Variant &DisplayValue, TCaption &ErrorText, bool &Error);
private:	// User declarations

        unsigned short FCY,FCM,FCD;
//        int       FpdType;  // "year" 5, "halfyear 4; "quarter 3;  "month" 2;  "unique" 1;  ERR 0
        UnicodeString __fastcall GetLastDay(int AMons, int AYear);
//        TdsDocDM *FDM;

        int __fastcall FGetPerType();
        int  __fastcall FGetPerSubType();
        int  __fastcall FGetCreateType();

        UnicodeString  __fastcall FGetPerTypeStr();
        UnicodeString  __fastcall FGetPerSubTypeStr();
        UnicodeString  __fastcall FGetCreateTypeStr();

        UnicodeString  __fastcall FGetPerYear();

        UnicodeString  __fastcall FGetPerDateFr();
        UnicodeString  __fastcall FGetPerDateTo();

        UnicodeString  __fastcall FGetPerOffset();

public:		// User declarations
        __fastcall TdsDocPackPeriodForm(TComponent* Owner, bool ADocRules);

        __property int Type = {read=FGetPerType};
        __property int SubType = {read=FGetPerSubType};
        __property int CreateType = {read=FGetCreateType};

        __property UnicodeString TypeStr = {read=FGetPerTypeStr};
        __property UnicodeString SubTypeStr = {read=FGetPerSubTypeStr};
        __property UnicodeString CreateTypeStr = {read=FGetCreateTypeStr};

        __property UnicodeString Year = {read=FGetPerYear};

        __property UnicodeString DateFr = {read=FGetPerDateFr};
        __property UnicodeString DateTo = {read=FGetPerDateTo};

        __property UnicodeString Offset = {read=FGetPerOffset};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocPackPeriodForm *dsDocPackPeriodForm;
//---------------------------------------------------------------------------
extern PACKAGE int __fastcall GetPeriodValue(int AType, int ASubType, UnicodeString AYear,
                              UnicodeString ADateFr, UnicodeString ADateTo,
                              UnicodeString AOffset,
                              int ADocPerType, TDate &ADocPerDateFr, TDate &ADocPerDateTo);
#endif
