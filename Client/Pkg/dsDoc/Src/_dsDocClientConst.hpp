﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'dsDocClientConst.pas' rev: 28.00 (Windows)

#ifndef DsdocclientconstHPP
#define DsdocclientconstHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Dsdocclientconst
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE System::ResourceString _dsDocCommonErrorCaption;
#define Dsdocclientconst_dsDocCommonErrorCaption System::LoadResourceString(&Dsdocclientconst::_dsDocCommonErrorCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocCommonInputErrorCaption;
#define Dsdocclientconst_dsDocCommonInputErrorCaption System::LoadResourceString(&Dsdocclientconst::_dsDocCommonInputErrorCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocCommonMsgCaption;
#define Dsdocclientconst_dsDocCommonMsgCaption System::LoadResourceString(&Dsdocclientconst::_dsDocCommonMsgCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocCommonErrorDBWorkMsg;
#define Dsdocclientconst_dsDocCommonErrorDBWorkMsg System::LoadResourceString(&Dsdocclientconst::_dsDocCommonErrorDBWorkMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocCommonConfRecDelCaption;
#define Dsdocclientconst_dsDocCommonConfRecDelCaption System::LoadResourceString(&Dsdocclientconst::_dsDocCommonConfRecDelCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocCommonDelConfMsg;
#define Dsdocclientconst_dsDocCommonDelConfMsg System::LoadResourceString(&Dsdocclientconst::_dsDocCommonDelConfMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocCommonErrorDelCaption;
#define Dsdocclientconst_dsDocCommonErrorDelCaption System::LoadResourceString(&Dsdocclientconst::_dsDocCommonErrorDelCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocCommonErrorDelMsg;
#define Dsdocclientconst_dsDocCommonErrorDelMsg System::LoadResourceString(&Dsdocclientconst::_dsDocCommonErrorDelMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocCommonErrorCreateDocCaption;
#define Dsdocclientconst_dsDocCommonErrorCreateDocCaption System::LoadResourceString(&Dsdocclientconst::_dsDocCommonErrorCreateDocCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocSQLCreatorErrorCondStructure;
#define Dsdocclientconst_dsDocSQLCreatorErrorCondStructure System::LoadResourceString(&Dsdocclientconst::_dsDocSQLCreatorErrorCondStructure)
extern DELPHI_PACKAGE System::ResourceString _dsDocSQLCreatorErrorFuncMissing;
#define Dsdocclientconst_dsDocSQLCreatorErrorFuncMissing System::LoadResourceString(&Dsdocclientconst::_dsDocSQLCreatorErrorFuncMissing)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreateProgressCaption;
#define Dsdocclientconst_dsDocDocCreateProgressCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreateProgressCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreateProgressStageFrom;
#define Dsdocclientconst_dsDocDocCreateProgressStageFrom System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreateProgressStageFrom)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreateTypeCaption;
#define Dsdocclientconst_dsDocDocCreateTypeCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreateTypeCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreateTypeQuestLabLabCaption;
#define Dsdocclientconst_dsDocDocCreateTypeQuestLabLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreateTypeQuestLabLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreateTypeOkBtnBtnCaption;
#define Dsdocclientconst_dsDocDocCreateTypeOkBtnBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreateTypeOkBtnBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreateTypeCancelBtnBtnCaption;
#define Dsdocclientconst_dsDocDocCreateTypeCancelBtnBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreateTypeCancelBtnBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreateTypeDocRBRBCaption;
#define Dsdocclientconst_dsDocDocCreateTypeDocRBRBCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreateTypeDocRBRBCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreateTypeBaseRBRBCaption;
#define Dsdocclientconst_dsDocDocCreateTypeBaseRBRBCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreateTypeBaseRBRBCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorDocLoadConf;
#define Dsdocclientconst_dsDocDocCreatorDocLoadConf System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorDocLoadConf)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorDocLoadConfCaption;
#define Dsdocclientconst_dsDocDocCreatorDocLoadConfCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorDocLoadConfCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorInitCaption;
#define Dsdocclientconst_dsDocDocCreatorInitCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorInitCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorInscCreateCaption;
#define Dsdocclientconst_dsDocDocCreatorInscCreateCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorInscCreateCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorListCreateCaption;
#define Dsdocclientconst_dsDocDocCreatorListCreateCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorListCreateCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorErrorISLink;
#define Dsdocclientconst_dsDocDocCreatorErrorISLink System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorErrorISLink)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateGetCount;
#define Dsdocclientconst_dsDocDocCreatorTableCreateGetCount System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorTableCreateGetCount)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateRows;
#define Dsdocclientconst_dsDocDocCreatorTableCreateRows System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorTableCreateRows)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateRow;
#define Dsdocclientconst_dsDocDocCreatorTableCreateRow System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorTableCreateRow)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateCols;
#define Dsdocclientconst_dsDocDocCreatorTableCreateCols System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorTableCreateCols)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateCol;
#define Dsdocclientconst_dsDocDocCreatorTableCreateCol System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorTableCreateCol)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateCeills;
#define Dsdocclientconst_dsDocDocCreatorTableCreateCeills System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorTableCreateCeills)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateCeill;
#define Dsdocclientconst_dsDocDocCreatorTableCreateCeill System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorTableCreateCeill)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorTableCreateCeillMsg;
#define Dsdocclientconst_dsDocDocCreatorTableCreateCeillMsg System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorTableCreateCeillMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorErrorMissingColRowDef;
#define Dsdocclientconst_dsDocDocCreatorErrorMissingColRowDef System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorErrorMissingColRowDef)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorErrorCeilCalc;
#define Dsdocclientconst_dsDocDocCreatorErrorCeilCalc System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorErrorCeilCalc)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeroidFromCaption;
#define Dsdocclientconst_dsDocDocCreatorPeroidFromCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorPeroidFromCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeroidTo1Caption;
#define Dsdocclientconst_dsDocDocCreatorPeroidTo1Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorPeroidTo1Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeroidTo2Caption;
#define Dsdocclientconst_dsDocDocCreatorPeroidTo2Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorPeroidTo2Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorUndefPeroidCaption;
#define Dsdocclientconst_dsDocDocCreatorUndefPeroidCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorUndefPeroidCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityEqual;
#define Dsdocclientconst_dsDocDocCreatorPeriodicityEqual System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorPeriodicityEqual)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityYear;
#define Dsdocclientconst_dsDocDocCreatorPeriodicityYear System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorPeriodicityYear)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityHalfYear;
#define Dsdocclientconst_dsDocDocCreatorPeriodicityHalfYear System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorPeriodicityHalfYear)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityQuarter;
#define Dsdocclientconst_dsDocDocCreatorPeriodicityQuarter System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorPeriodicityQuarter)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityMonth;
#define Dsdocclientconst_dsDocDocCreatorPeriodicityMonth System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorPeriodicityMonth)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityUnique;
#define Dsdocclientconst_dsDocDocCreatorPeriodicityUnique System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorPeriodicityUnique)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityNo;
#define Dsdocclientconst_dsDocDocCreatorPeriodicityNo System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorPeriodicityNo)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorPeriodicityAny;
#define Dsdocclientconst_dsDocDocCreatorPeriodicityAny System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorPeriodicityAny)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorAuthorDefined;
#define Dsdocclientconst_dsDocDocCreatorAuthorDefined System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorAuthorDefined)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorAuthorThis;
#define Dsdocclientconst_dsDocDocCreatorAuthorThis System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorAuthorThis)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorAuthorAllChild;
#define Dsdocclientconst_dsDocDocCreatorAuthorAllChild System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorAuthorAllChild)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorErrorNoIntDocs;
#define Dsdocclientconst_dsDocDocCreatorErrorNoIntDocs System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorErrorNoIntDocs)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorErrorCreateRowColNamesNotValid;
#define Dsdocclientconst_dsDocDocCreatorErrorCreateRowColNamesNotValid System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorErrorCreateRowColNamesNotValid)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorErrorRowColReferNotValid;
#define Dsdocclientconst_dsDocDocCreatorErrorRowColReferNotValid System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorErrorRowColReferNotValid)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocCreatorSpecPrepare;
#define Dsdocclientconst_dsDocDocCreatorSpecPrepare System::LoadResourceString(&Dsdocclientconst::_dsDocDocCreatorSpecPrepare)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListDocCaption;
#define Dsdocclientconst_dsDocDocListDocCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListDocCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListCaption;
#define Dsdocclientconst_dsDocDocListCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListLabel2Caption;
#define Dsdocclientconst_dsDocDocListLabel2Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListLabel2Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListLabel4Caption;
#define Dsdocclientconst_dsDocDocListLabel4Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListLabel4Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListSpecGUICaption;
#define Dsdocclientconst_dsDocDocListSpecGUICaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListSpecGUICaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListLabel3Caption;
#define Dsdocclientconst_dsDocDocListLabel3Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListLabel3Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListLabel1Caption;
#define Dsdocclientconst_dsDocDocListLabel1Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListLabel1Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListPeriodLabCaption;
#define Dsdocclientconst_dsDocDocListPeriodLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListPeriodLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListLabel6Caption;
#define Dsdocclientconst_dsDocDocListLabel6Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListLabel6Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListLabel7Caption;
#define Dsdocclientconst_dsDocDocListLabel7Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListLabel7Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListLabel8Caption;
#define Dsdocclientconst_dsDocDocListLabel8Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListLabel8Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListFilterPanelBottomBtnHint;
#define Dsdocclientconst_dsDocDocListFilterPanelBottomBtnHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocListFilterPanelBottomBtnHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListFilterPanelTopBtnHint;
#define Dsdocclientconst_dsDocDocListFilterPanelTopBtnHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocListFilterPanelTopBtnHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListPeriodicityCBItems1;
#define Dsdocclientconst_dsDocDocListPeriodicityCBItems1 System::LoadResourceString(&Dsdocclientconst::_dsDocDocListPeriodicityCBItems1)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListPeriodicityCBItems2;
#define Dsdocclientconst_dsDocDocListPeriodicityCBItems2 System::LoadResourceString(&Dsdocclientconst::_dsDocDocListPeriodicityCBItems2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListPeriodicityCBItems3;
#define Dsdocclientconst_dsDocDocListPeriodicityCBItems3 System::LoadResourceString(&Dsdocclientconst::_dsDocDocListPeriodicityCBItems3)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListPeriodicityCBItems4;
#define Dsdocclientconst_dsDocDocListPeriodicityCBItems4 System::LoadResourceString(&Dsdocclientconst::_dsDocDocListPeriodicityCBItems4)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListPeriodicityCBItems5;
#define Dsdocclientconst_dsDocDocListPeriodicityCBItems5 System::LoadResourceString(&Dsdocclientconst::_dsDocDocListPeriodicityCBItems5)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListPeriodicityCBItems6;
#define Dsdocclientconst_dsDocDocListPeriodicityCBItems6 System::LoadResourceString(&Dsdocclientconst::_dsDocDocListPeriodicityCBItems6)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListPeriodicityCBItems7;
#define Dsdocclientconst_dsDocDocListPeriodicityCBItems7 System::LoadResourceString(&Dsdocclientconst::_dsDocDocListPeriodicityCBItems7)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListResetBtnHint;
#define Dsdocclientconst_dsDocDocListResetBtnHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocListResetBtnHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListResetBtnCaption;
#define Dsdocclientconst_dsDocDocListResetBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListResetBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListactDeleteDocCaption;
#define Dsdocclientconst_dsDocDocListactDeleteDocCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListactDeleteDocCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListactDeleteDocHint;
#define Dsdocclientconst_dsDocDocListactDeleteDocHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocListactDeleteDocHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListactPreviewCaption;
#define Dsdocclientconst_dsDocDocListactPreviewCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListactPreviewCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListactPreviewHint;
#define Dsdocclientconst_dsDocDocListactPreviewHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocListactPreviewHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListactFindDocCaption;
#define Dsdocclientconst_dsDocDocListactFindDocCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListactFindDocCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListactFindDocHint;
#define Dsdocclientconst_dsDocDocListactFindDocHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocListactFindDocHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListactCreateDocCaption;
#define Dsdocclientconst_dsDocDocListactCreateDocCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListactCreateDocCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListactCreateDocHint;
#define Dsdocclientconst_dsDocDocListactCreateDocHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocListactCreateDocHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListactAboutCaption;
#define Dsdocclientconst_dsDocDocListactAboutCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListactAboutCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListactAboutHint;
#define Dsdocclientconst_dsDocDocListactAboutHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocListactAboutHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListStatusSBPanels1;
#define Dsdocclientconst_dsDocDocListStatusSBPanels1 System::LoadResourceString(&Dsdocclientconst::_dsDocDocListStatusSBPanels1)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListStatusSBPanels2;
#define Dsdocclientconst_dsDocDocListStatusSBPanels2 System::LoadResourceString(&Dsdocclientconst::_dsDocDocListStatusSBPanels2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListStatusSBPanels3;
#define Dsdocclientconst_dsDocDocListStatusSBPanels3 System::LoadResourceString(&Dsdocclientconst::_dsDocDocListStatusSBPanels3)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListStatusSBPanels4;
#define Dsdocclientconst_dsDocDocListStatusSBPanels4 System::LoadResourceString(&Dsdocclientconst::_dsDocDocListStatusSBPanels4)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListListTBMBars1Caption;
#define Dsdocclientconst_dsDocDocListListTBMBars1Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListListTBMBars1Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListDocListColumnNAMECaption;
#define Dsdocclientconst_dsDocDocListDocListColumnNAMECaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListDocListColumnNAMECaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocListDocListColumnPERIOD_VALUECaption;
#define Dsdocclientconst_dsDocDocListDocListColumnPERIOD_VALUECaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocListDocListColumnPERIOD_VALUECaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditSetCaption;
#define Dsdocclientconst_dsDocDocOrgEditSetCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditSetCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditEditCaption;
#define Dsdocclientconst_dsDocDocOrgEditEditCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditEditCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditInsertCaption;
#define Dsdocclientconst_dsDocDocOrgEditInsertCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditInsertCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditChLPUErrMsg;
#define Dsdocclientconst_dsDocDocOrgEditChLPUErrMsg System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditChLPUErrMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditChISubordErrMsg;
#define Dsdocclientconst_dsDocDocOrgEditChISubordErrMsg System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditChISubordErrMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditChIOrgCodeErrMsg;
#define Dsdocclientconst_dsDocDocOrgEditChIOrgCodeErrMsg System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditChIOrgCodeErrMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditChIOrgCodeResErrMsg;
#define Dsdocclientconst_dsDocDocOrgEditChIOrgCodeResErrMsg System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditChIOrgCodeResErrMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditChIOrgCodeFormatErrMsg;
#define Dsdocclientconst_dsDocDocOrgEditChIOrgCodeFormatErrMsg System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditChIOrgCodeFormatErrMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditChIOrgShortNameErrMsg;
#define Dsdocclientconst_dsDocDocOrgEditChIOrgShortNameErrMsg System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditChIOrgShortNameErrMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditChIOrgFullNameErrMsg;
#define Dsdocclientconst_dsDocDocOrgEditChIOrgFullNameErrMsg System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditChIOrgFullNameErrMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditChIAddrErrMsg;
#define Dsdocclientconst_dsDocDocOrgEditChIAddrErrMsg System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditChIAddrErrMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditChIOrgExistErrMsg;
#define Dsdocclientconst_dsDocDocOrgEditChIOrgExistErrMsg System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditChIOrgExistErrMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditSubordLabCaption;
#define Dsdocclientconst_dsDocDocOrgEditSubordLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditSubordLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditOrgCodeLabCaption;
#define Dsdocclientconst_dsDocDocOrgEditOrgCodeLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditOrgCodeLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditNameLabCaption;
#define Dsdocclientconst_dsDocDocOrgEditNameLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditNameLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditAddrLabCaption;
#define Dsdocclientconst_dsDocDocOrgEditAddrLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditAddrLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditEMailLabCaption;
#define Dsdocclientconst_dsDocDocOrgEditEMailLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditEMailLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditPhoneLabCaption;
#define Dsdocclientconst_dsDocDocOrgEditPhoneLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditPhoneLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditFullNameLabCaption;
#define Dsdocclientconst_dsDocDocOrgEditFullNameLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditFullNameLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditOkBtnCaption;
#define Dsdocclientconst_dsDocDocOrgEditOkBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditCancelBtnCaption;
#define Dsdocclientconst_dsDocDocOrgEditCancelBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditSubordCBItem1;
#define Dsdocclientconst_dsDocDocOrgEditSubordCBItem1 System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditSubordCBItem1)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditSubordCBItem2;
#define Dsdocclientconst_dsDocDocOrgEditSubordCBItem2 System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditSubordCBItem2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgEditSubordCBItem3;
#define Dsdocclientconst_dsDocDocOrgEditSubordCBItem3 System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgEditSubordCBItem3)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListErrorInsertRecMsg;
#define Dsdocclientconst_dsDocDocOrgListErrorInsertRecMsg System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListErrorInsertRecMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListErrorEditRecMsg;
#define Dsdocclientconst_dsDocDocOrgListErrorEditRecMsg System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListErrorEditRecMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListErrorDelDocDef;
#define Dsdocclientconst_dsDocDocOrgListErrorDelDocDef System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListErrorDelDocDef)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListErrorDelDoc;
#define Dsdocclientconst_dsDocDocOrgListErrorDelDoc System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListErrorDelDoc)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListOrgCaption;
#define Dsdocclientconst_dsDocDocOrgListOrgCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListOrgCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListRecCaption;
#define Dsdocclientconst_dsDocDocOrgListRecCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListRecCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListCaption;
#define Dsdocclientconst_dsDocDocOrgListCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListLabel1Caption;
#define Dsdocclientconst_dsDocDocOrgListLabel1Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListLabel1Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListLabel2Caption;
#define Dsdocclientconst_dsDocDocOrgListLabel2Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListLabel2Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListLabel3Caption;
#define Dsdocclientconst_dsDocDocOrgListLabel3Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListLabel3Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListLabel4Caption;
#define Dsdocclientconst_dsDocDocOrgListLabel4Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListLabel4Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListLabel5Caption;
#define Dsdocclientconst_dsDocDocOrgListLabel5Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListLabel5Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListLabel7Caption;
#define Dsdocclientconst_dsDocDocOrgListLabel7Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListLabel7Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListLabel8Caption;
#define Dsdocclientconst_dsDocDocOrgListLabel8Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListLabel8Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactInsertCaption;
#define Dsdocclientconst_dsDocDocOrgListactInsertCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactInsertCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactInsertHint;
#define Dsdocclientconst_dsDocDocOrgListactInsertHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactInsertHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactEditCaption;
#define Dsdocclientconst_dsDocDocOrgListactEditCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactEditCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactEditHint;
#define Dsdocclientconst_dsDocDocOrgListactEditHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactEditHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactDeleteCaption;
#define Dsdocclientconst_dsDocDocOrgListactDeleteCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactDeleteCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactDeleteHint;
#define Dsdocclientconst_dsDocDocOrgListactDeleteHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactDeleteHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactSetTemplateCaption;
#define Dsdocclientconst_dsDocDocOrgListactSetTemplateCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactSetTemplateCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactSetTemplateHint;
#define Dsdocclientconst_dsDocDocOrgListactSetTemplateHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactSetTemplateHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactReSetTemplateCaption;
#define Dsdocclientconst_dsDocDocOrgListactReSetTemplateCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactReSetTemplateCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactReSetTemplateHint;
#define Dsdocclientconst_dsDocDocOrgListactReSetTemplateHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactReSetTemplateHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactRefreshCaption;
#define Dsdocclientconst_dsDocDocOrgListactRefreshCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactRefreshCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactRefreshHint;
#define Dsdocclientconst_dsDocDocOrgListactRefreshHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactRefreshHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactFindCaption;
#define Dsdocclientconst_dsDocDocOrgListactFindCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactFindCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactFindHint;
#define Dsdocclientconst_dsDocDocOrgListactFindHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactFindHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactFindNextCaption;
#define Dsdocclientconst_dsDocDocOrgListactFindNextCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactFindNextCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListactFindNextHint;
#define Dsdocclientconst_dsDocDocOrgListactFindNextHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListactFindNextHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListMainTBMBar1Caption;
#define Dsdocclientconst_dsDocDocOrgListMainTBMBar1Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListMainTBMBar1Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListMainTBMBar2Caption;
#define Dsdocclientconst_dsDocDocOrgListMainTBMBar2Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListMainTBMBar2Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocOrgListClassGridColumnCaption;
#define Dsdocclientconst_dsDocDocOrgListClassGridColumnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocOrgListClassGridColumnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodErrorYearFormat;
#define Dsdocclientconst_dsDocDocPeriodErrorYearFormat System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodErrorYearFormat)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodErrorDateFormat;
#define Dsdocclientconst_dsDocDocPeriodErrorDateFormat System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodErrorDateFormat)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodCaptionLabCaption;
#define Dsdocclientconst_dsDocDocPeriodCaptionLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodCaptionLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodLabCaption;
#define Dsdocclientconst_dsDocDocPeriodLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodDateFrLabCaption;
#define Dsdocclientconst_dsDocDocPeriodDateFrLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodDateFrLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodDateToLabCaption;
#define Dsdocclientconst_dsDocDocPeriodDateToLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodDateToLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodYearRBCaption;
#define Dsdocclientconst_dsDocDocPeriodYearRBCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodYearRBCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodQuarterRBCaption;
#define Dsdocclientconst_dsDocDocPeriodQuarterRBCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodQuarterRBCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodMonsRBCaption;
#define Dsdocclientconst_dsDocDocPeriodMonsRBCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodMonsRBCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodPeriodRBCaption;
#define Dsdocclientconst_dsDocDocPeriodPeriodRBCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodPeriodRBCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodHalfYearRBCaption;
#define Dsdocclientconst_dsDocDocPeriodHalfYearRBCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodHalfYearRBCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodOkBtnCaption;
#define Dsdocclientconst_dsDocDocPeriodOkBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodCancelBtnCaption;
#define Dsdocclientconst_dsDocDocPeriodCancelBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodHalfYearCBItems1;
#define Dsdocclientconst_dsDocDocPeriodHalfYearCBItems1 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodHalfYearCBItems1)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodHalfYearCBItems2;
#define Dsdocclientconst_dsDocDocPeriodHalfYearCBItems2 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodHalfYearCBItems2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodQuarterCBItems1;
#define Dsdocclientconst_dsDocDocPeriodQuarterCBItems1 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodQuarterCBItems1)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodQuarterCBItems2;
#define Dsdocclientconst_dsDocDocPeriodQuarterCBItems2 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodQuarterCBItems2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodQuarterCBItems3;
#define Dsdocclientconst_dsDocDocPeriodQuarterCBItems3 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodQuarterCBItems3)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodQuarterCBItems4;
#define Dsdocclientconst_dsDocDocPeriodQuarterCBItems4 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodQuarterCBItems4)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodMonsCBItems01;
#define Dsdocclientconst_dsDocDocPeriodMonsCBItems01 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodMonsCBItems01)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodMonsCBItems02;
#define Dsdocclientconst_dsDocDocPeriodMonsCBItems02 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodMonsCBItems02)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodMonsCBItems03;
#define Dsdocclientconst_dsDocDocPeriodMonsCBItems03 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodMonsCBItems03)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodMonsCBItems04;
#define Dsdocclientconst_dsDocDocPeriodMonsCBItems04 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodMonsCBItems04)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodMonsCBItems05;
#define Dsdocclientconst_dsDocDocPeriodMonsCBItems05 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodMonsCBItems05)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodMonsCBItems06;
#define Dsdocclientconst_dsDocDocPeriodMonsCBItems06 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodMonsCBItems06)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodMonsCBItems07;
#define Dsdocclientconst_dsDocDocPeriodMonsCBItems07 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodMonsCBItems07)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodMonsCBItems08;
#define Dsdocclientconst_dsDocDocPeriodMonsCBItems08 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodMonsCBItems08)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodMonsCBItems09;
#define Dsdocclientconst_dsDocDocPeriodMonsCBItems09 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodMonsCBItems09)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodMonsCBItems10;
#define Dsdocclientconst_dsDocDocPeriodMonsCBItems10 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodMonsCBItems10)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodMonsCBItems11;
#define Dsdocclientconst_dsDocDocPeriodMonsCBItems11 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodMonsCBItems11)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocPeriodMonsCBItems12;
#define Dsdocclientconst_dsDocDocPeriodMonsCBItems12 System::LoadResourceString(&Dsdocclientconst::_dsDocDocPeriodMonsCBItems12)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodLocStr1;
#define Dsdocclientconst_dsDocExtFuncDocPeriodLocStr1 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodLocStr1)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodLocStr2;
#define Dsdocclientconst_dsDocExtFuncDocPeriodLocStr2 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodLocStr2)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodLocStr3;
#define Dsdocclientconst_dsDocExtFuncDocPeriodLocStr3 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodLocStr3)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodLocStr4;
#define Dsdocclientconst_dsDocExtFuncDocPeriodLocStr4 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodLocStr4)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodLocStr5;
#define Dsdocclientconst_dsDocExtFuncDocPeriodLocStr5 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodLocStr5)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodLocStr6;
#define Dsdocclientconst_dsDocExtFuncDocPeriodLocStr6 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodLocStr6)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrUniq1;
#define Dsdocclientconst_dsDocExtFuncDocPeriodValStrUniq1 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodValStrUniq1)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrUniq2;
#define Dsdocclientconst_dsDocExtFuncDocPeriodValStrUniq2 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodValStrUniq2)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrUniq3;
#define Dsdocclientconst_dsDocExtFuncDocPeriodValStrUniq3 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodValStrUniq3)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrMonth;
#define Dsdocclientconst_dsDocExtFuncDocPeriodValStrMonth System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodValStrMonth)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrQuart1;
#define Dsdocclientconst_dsDocExtFuncDocPeriodValStrQuart1 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodValStrQuart1)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrQuart2;
#define Dsdocclientconst_dsDocExtFuncDocPeriodValStrQuart2 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodValStrQuart2)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrQuart3;
#define Dsdocclientconst_dsDocExtFuncDocPeriodValStrQuart3 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodValStrQuart3)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrQuart4;
#define Dsdocclientconst_dsDocExtFuncDocPeriodValStrQuart4 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodValStrQuart4)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrHalfYear1;
#define Dsdocclientconst_dsDocExtFuncDocPeriodValStrHalfYear1 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodValStrHalfYear1)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrHalfYear2;
#define Dsdocclientconst_dsDocExtFuncDocPeriodValStrHalfYear2 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodValStrHalfYear2)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncDocPeriodValStrYear;
#define Dsdocclientconst_dsDocExtFuncDocPeriodValStrYear System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncDocPeriodValStrYear)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncErrorReadSettingCaption;
#define Dsdocclientconst_dsDocExtFuncErrorReadSettingCaption System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncErrorReadSettingCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncErrorReadSettingMsg;
#define Dsdocclientconst_dsDocExtFuncErrorReadSettingMsg System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncErrorReadSettingMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncCreateDocViewMsg;
#define Dsdocclientconst_dsDocExtFuncCreateDocViewMsg System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncCreateDocViewMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncErrorCreateDocView;
#define Dsdocclientconst_dsDocExtFuncErrorCreateDocView System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncErrorCreateDocView)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncReplaceDocMsg;
#define Dsdocclientconst_dsDocExtFuncReplaceDocMsg System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncReplaceDocMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncReplaceDocCaption;
#define Dsdocclientconst_dsDocExtFuncReplaceDocCaption System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncReplaceDocCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncMissingDocDef;
#define Dsdocclientconst_dsDocExtFuncMissingDocDef System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncMissingDocDef)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncNoDocDef;
#define Dsdocclientconst_dsDocExtFuncNoDocDef System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncNoDocDef)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncFileVersionString;
#define Dsdocclientconst_dsDocExtFuncFileVersionString System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncFileVersionString)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncRecCount1;
#define Dsdocclientconst_dsDocExtFuncRecCount1 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncRecCount1)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncRecCount234;
#define Dsdocclientconst_dsDocExtFuncRecCount234 System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncRecCount234)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtFuncRecCount;
#define Dsdocclientconst_dsDocExtFuncRecCount System::LoadResourceString(&Dsdocclientconst::_dsDocExtFuncRecCount)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtSpecListAddDocDefCaption;
#define Dsdocclientconst_dsDocExtSpecListAddDocDefCaption System::LoadResourceString(&Dsdocclientconst::_dsDocExtSpecListAddDocDefCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtSpecListCaption;
#define Dsdocclientconst_dsDocExtSpecListCaption System::LoadResourceString(&Dsdocclientconst::_dsDocExtSpecListCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtSpecListSpecCodeLabCaption;
#define Dsdocclientconst_dsDocExtSpecListSpecCodeLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocExtSpecListSpecCodeLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtSpecListCreateDateLabCaption;
#define Dsdocclientconst_dsDocExtSpecListCreateDateLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocExtSpecListCreateDateLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtSpecListAutorLabCaption;
#define Dsdocclientconst_dsDocExtSpecListAutorLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocExtSpecListAutorLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtSpecListExtSpecListColumn1Caption;
#define Dsdocclientconst_dsDocExtSpecListExtSpecListColumn1Caption System::LoadResourceString(&Dsdocclientconst::_dsDocExtSpecListExtSpecListColumn1Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtSpecListListTBMBar1Caption;
#define Dsdocclientconst_dsDocExtSpecListListTBMBar1Caption System::LoadResourceString(&Dsdocclientconst::_dsDocExtSpecListListTBMBar1Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtSpecListactAddSpecCaption1;
#define Dsdocclientconst_dsDocExtSpecListactAddSpecCaption1 System::LoadResourceString(&Dsdocclientconst::_dsDocExtSpecListactAddSpecCaption1)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtSpecListactAddSpecCaption2;
#define Dsdocclientconst_dsDocExtSpecListactAddSpecCaption2 System::LoadResourceString(&Dsdocclientconst::_dsDocExtSpecListactAddSpecCaption2)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtSpecListactDeleteSpecCaption1;
#define Dsdocclientconst_dsDocExtSpecListactDeleteSpecCaption1 System::LoadResourceString(&Dsdocclientconst::_dsDocExtSpecListactDeleteSpecCaption1)
extern DELPHI_PACKAGE System::ResourceString _dsDocExtSpecListactDeleteSpecCaption2;
#define Dsdocclientconst_dsDocExtSpecListactDeleteSpecCaption2 System::LoadResourceString(&Dsdocclientconst::_dsDocExtSpecListactDeleteSpecCaption2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecEditEditCaption;
#define Dsdocclientconst_dsDocDocSpecEditEditCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecEditEditCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecEditNewCaption;
#define Dsdocclientconst_dsDocDocSpecEditNewCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecEditNewCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecEditSaveMsg;
#define Dsdocclientconst_dsDocDocSpecEditSaveMsg System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecEditSaveMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecEditSaveCaption;
#define Dsdocclientconst_dsDocDocSpecEditSaveCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecEditSaveCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecEditCancelWarn;
#define Dsdocclientconst_dsDocDocSpecEditCancelWarn System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecEditCancelWarn)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecEditSaveBtnCaption;
#define Dsdocclientconst_dsDocDocSpecEditSaveBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecEditSaveBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecEditCancelBtnCaption;
#define Dsdocclientconst_dsDocDocSpecEditCancelBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecEditCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListNoDefContent;
#define Dsdocclientconst_dsDocDocSpecListNoDefContent System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListNoDefContent)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListErrorFindSpec;
#define Dsdocclientconst_dsDocDocSpecListErrorFindSpec System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListErrorFindSpec)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListDocDefCaption;
#define Dsdocclientconst_dsDocDocSpecListDocDefCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListDocDefCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListErrorDelDocDef;
#define Dsdocclientconst_dsDocDocSpecListErrorDelDocDef System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListErrorDelDocDef)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListErrorReplaceDocDef;
#define Dsdocclientconst_dsDocDocSpecListErrorReplaceDocDef System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListErrorReplaceDocDef)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListConfReplaceDocDef;
#define Dsdocclientconst_dsDocDocSpecListConfReplaceDocDef System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListConfReplaceDocDef)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListCopyCaption;
#define Dsdocclientconst_dsDocDocSpecListCopyCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListCopyCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListErrorOpenFile;
#define Dsdocclientconst_dsDocDocSpecListErrorOpenFile System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListErrorOpenFile)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListErrorFileWork;
#define Dsdocclientconst_dsDocDocSpecListErrorFileWork System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListErrorFileWork)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListOpenDlgMask;
#define Dsdocclientconst_dsDocDocSpecListOpenDlgMask System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListOpenDlgMask)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListCaption;
#define Dsdocclientconst_dsDocDocSpecListCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListFilterParamLabCaption;
#define Dsdocclientconst_dsDocDocSpecListFilterParamLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListFilterParamLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListPeriodicityLabCaption;
#define Dsdocclientconst_dsDocDocSpecListPeriodicityLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListPeriodicityLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListOwnerLabCaption;
#define Dsdocclientconst_dsDocDocSpecListOwnerLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListOwnerLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListFilterPanelTopBtnHint;
#define Dsdocclientconst_dsDocDocSpecListFilterPanelTopBtnHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListFilterPanelTopBtnHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListFilterPanelBottomBtnHint;
#define Dsdocclientconst_dsDocDocSpecListFilterPanelBottomBtnHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListFilterPanelBottomBtnHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListResetBtnHint;
#define Dsdocclientconst_dsDocDocSpecListResetBtnHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListResetBtnHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListResetBtnCaption;
#define Dsdocclientconst_dsDocDocSpecListResetBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListResetBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListSpecCodeLabCaption;
#define Dsdocclientconst_dsDocDocSpecListSpecCodeLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListSpecCodeLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListCreateDateLabCaption;
#define Dsdocclientconst_dsDocDocSpecListCreateDateLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListCreateDateLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListAutorLabCaption;
#define Dsdocclientconst_dsDocDocSpecListAutorLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListAutorLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListLabel1Caption;
#define Dsdocclientconst_dsDocDocSpecListLabel1Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListLabel1Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactNewSpecCaption;
#define Dsdocclientconst_dsDocDocSpecListactNewSpecCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactNewSpecCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactNewSpecHint;
#define Dsdocclientconst_dsDocDocSpecListactNewSpecHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactNewSpecHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactEditSpecCaption;
#define Dsdocclientconst_dsDocDocSpecListactEditSpecCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactEditSpecCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactEditSpecHint;
#define Dsdocclientconst_dsDocDocSpecListactEditSpecHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactEditSpecHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactCopySpecCaption;
#define Dsdocclientconst_dsDocDocSpecListactCopySpecCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactCopySpecCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactCopySpecHint;
#define Dsdocclientconst_dsDocDocSpecListactCopySpecHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactCopySpecHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactDeleteSpecCaption;
#define Dsdocclientconst_dsDocDocSpecListactDeleteSpecCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactDeleteSpecCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactDeleteSpecHint;
#define Dsdocclientconst_dsDocDocSpecListactDeleteSpecHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactDeleteSpecHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactFindSpecCaption;
#define Dsdocclientconst_dsDocDocSpecListactFindSpecCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactFindSpecCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactFindSpecHint;
#define Dsdocclientconst_dsDocDocSpecListactFindSpecHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactFindSpecHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactCreateDocCaption;
#define Dsdocclientconst_dsDocDocSpecListactCreateDocCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactCreateDocCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactCreateDocHint;
#define Dsdocclientconst_dsDocDocSpecListactCreateDocHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactCreateDocHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactAboutCaption;
#define Dsdocclientconst_dsDocDocSpecListactAboutCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactAboutCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactAboutHint;
#define Dsdocclientconst_dsDocDocSpecListactAboutHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactAboutHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactHandCreateCaption;
#define Dsdocclientconst_dsDocDocSpecListactHandCreateCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactHandCreateCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactHandCreateHint;
#define Dsdocclientconst_dsDocDocSpecListactHandCreateHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactHandCreateHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactAddTemplateCaption;
#define Dsdocclientconst_dsDocDocSpecListactAddTemplateCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactAddTemplateCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactAddTemplateHint;
#define Dsdocclientconst_dsDocDocSpecListactAddTemplateHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactAddTemplateHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactClearTemplateCaption;
#define Dsdocclientconst_dsDocDocSpecListactClearTemplateCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactClearTemplateCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactClearTemplateHint;
#define Dsdocclientconst_dsDocDocSpecListactClearTemplateHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactClearTemplateHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactSaveToXMLCaption;
#define Dsdocclientconst_dsDocDocSpecListactSaveToXMLCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactSaveToXMLCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactSaveToXMLHint;
#define Dsdocclientconst_dsDocDocSpecListactSaveToXMLHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactSaveToXMLHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactLoadFromXMLCaption;
#define Dsdocclientconst_dsDocDocSpecListactLoadFromXMLCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactLoadFromXMLCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListactLoadFromXMLHint;
#define Dsdocclientconst_dsDocDocSpecListactLoadFromXMLHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListactLoadFromXMLHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListNoDataSrc;
#define Dsdocclientconst_dsDocDocSpecListNoDataSrc System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListNoDataSrc)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListFltLabCaption;
#define Dsdocclientconst_dsDocDocSpecListFltLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListFltLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListFltLabHint;
#define Dsdocclientconst_dsDocDocSpecListFltLabHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListFltLabHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListPeriodicityCBItem1;
#define Dsdocclientconst_dsDocDocSpecListPeriodicityCBItem1 System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListPeriodicityCBItem1)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListPeriodicityCBItem2;
#define Dsdocclientconst_dsDocDocSpecListPeriodicityCBItem2 System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListPeriodicityCBItem2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListPeriodicityCBItem3;
#define Dsdocclientconst_dsDocDocSpecListPeriodicityCBItem3 System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListPeriodicityCBItem3)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListDBTableView1NAMECaption;
#define Dsdocclientconst_dsDocDocSpecListDBTableView1NAMECaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListDBTableView1NAMECaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListDBTableView1F_PERIODCaption;
#define Dsdocclientconst_dsDocDocSpecListDBTableView1F_PERIODCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListDBTableView1F_PERIODCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListStatusSBItem1Caption;
#define Dsdocclientconst_dsDocDocSpecListStatusSBItem1Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListStatusSBItem1Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListStatusSBItem2Caption;
#define Dsdocclientconst_dsDocDocSpecListStatusSBItem2Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListStatusSBItem2Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListStatusSBItem3Caption;
#define Dsdocclientconst_dsDocDocSpecListStatusSBItem3Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListStatusSBItem3Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocSpecListStatusSBItem4Caption;
#define Dsdocclientconst_dsDocDocSpecListStatusSBItem4Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocSpecListStatusSBItem4Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocDocExDFMPeriodicyty;
#define Dsdocclientconst_dsDocDocDocExDFMPeriodicyty System::LoadResourceString(&Dsdocclientconst::_dsDocDocDocExDFMPeriodicyty)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocDocExDFMNoPeriodicyty;
#define Dsdocclientconst_dsDocDocDocExDFMNoPeriodicyty System::LoadResourceString(&Dsdocclientconst::_dsDocDocDocExDFMNoPeriodicyty)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListConfReplaceChoice;
#define Dsdocclientconst_dsDocDocChoiceListConfReplaceChoice System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListConfReplaceChoice)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListErrorAddChoiceRecMissing;
#define Dsdocclientconst_dsDocDocChoiceListErrorAddChoiceRecMissing System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListErrorAddChoiceRecMissing)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListErrorEditChoice;
#define Dsdocclientconst_dsDocDocChoiceListErrorEditChoice System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListErrorEditChoice)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListErrorAddChoice;
#define Dsdocclientconst_dsDocDocChoiceListErrorAddChoice System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListErrorAddChoice)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListChioceCaption;
#define Dsdocclientconst_dsDocDocChoiceListChioceCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListChioceCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListCaption;
#define Dsdocclientconst_dsDocDocChoiceListCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListFltListColumns1Caption;
#define Dsdocclientconst_dsDocDocChoiceListFltListColumns1Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListFltListColumns1Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListClassTBMBar1Caption;
#define Dsdocclientconst_dsDocDocChoiceListClassTBMBar1Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListClassTBMBar1Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListactInsertCaption;
#define Dsdocclientconst_dsDocDocChoiceListactInsertCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListactInsertCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListactInsertHint;
#define Dsdocclientconst_dsDocDocChoiceListactInsertHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListactInsertHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListactEditCaption;
#define Dsdocclientconst_dsDocDocChoiceListactEditCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListactEditCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListactEditHint;
#define Dsdocclientconst_dsDocDocChoiceListactEditHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListactEditHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListactDeleteCaption;
#define Dsdocclientconst_dsDocDocChoiceListactDeleteCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListactDeleteCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListactDeleteHint;
#define Dsdocclientconst_dsDocDocChoiceListactDeleteHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListactDeleteHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceListCountCaption;
#define Dsdocclientconst_dsDocDocChoiceListCountCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceListCountCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceCaption;
#define Dsdocclientconst_dsDocDocChoiceCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceOkBtnCaption;
#define Dsdocclientconst_dsDocDocChoiceOkBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocChoiceCancelBtnCaption;
#define Dsdocclientconst_dsDocDocChoiceCancelBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocChoiceCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocFileFilter1;
#define Dsdocclientconst_dsDocDocImportDocFileFilter1 System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocFileFilter1)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocFileFilter2;
#define Dsdocclientconst_dsDocDocImportDocFileFilter2 System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocFileFilter2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocSourceCaption;
#define Dsdocclientconst_dsDocDocImportDocSourceCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocSourceCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocLoadCaption;
#define Dsdocclientconst_dsDocDocImportDocLoadCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocLoadCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocOrgMissing;
#define Dsdocclientconst_dsDocDocImportDocOrgMissing System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocOrgMissing)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocImportDataDefFileMissing;
#define Dsdocclientconst_dsDocDocImportDocImportDataDefFileMissing System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocImportDataDefFileMissing)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocImportFileMissing;
#define Dsdocclientconst_dsDocDocImportDocImportFileMissing System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocImportFileMissing)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocDocDefCaption;
#define Dsdocclientconst_dsDocDocImportDocDocDefCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocDocDefCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocReplaceDocDefConf1;
#define Dsdocclientconst_dsDocDocImportDocReplaceDocDefConf1 System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocReplaceDocDefConf1)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocReplaceDocDefConf2;
#define Dsdocclientconst_dsDocDocImportDocReplaceDocDefConf2 System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocReplaceDocDefConf2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocDocCaption;
#define Dsdocclientconst_dsDocDocImportDocDocCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocDocCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocReplaceDocConf1;
#define Dsdocclientconst_dsDocDocImportDocReplaceDocConf1 System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocReplaceDocConf1)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocReplaceDocParam1;
#define Dsdocclientconst_dsDocDocImportDocReplaceDocParam1 System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocReplaceDocParam1)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocReplaceDocConf2;
#define Dsdocclientconst_dsDocDocImportDocReplaceDocConf2 System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocReplaceDocConf2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocReplaceDocParam2;
#define Dsdocclientconst_dsDocDocImportDocReplaceDocParam2 System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocReplaceDocParam2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocBreakImportConf;
#define Dsdocclientconst_dsDocDocImportDocBreakImportConf System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocBreakImportConf)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocCaption;
#define Dsdocclientconst_dsDocDocImportDocCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocSpecTSCaption;
#define Dsdocclientconst_dsDocDocImportDocSpecTSCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocSpecTSCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocSpecNameCaption;
#define Dsdocclientconst_dsDocDocImportDocSpecNameCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocSpecNameCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocSpecOwnerCaption;
#define Dsdocclientconst_dsDocDocImportDocSpecOwnerCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocSpecOwnerCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocSpecPeridCaption;
#define Dsdocclientconst_dsDocDocImportDocSpecPeridCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocSpecPeridCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocDocTSCaption;
#define Dsdocclientconst_dsDocDocImportDocDocTSCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocDocTSCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocDocNameCaption;
#define Dsdocclientconst_dsDocDocImportDocDocNameCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocDocNameCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocDocOwnerCaption;
#define Dsdocclientconst_dsDocDocImportDocDocOwnerCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocDocOwnerCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocDocPeriodCaption;
#define Dsdocclientconst_dsDocDocImportDocDocPeriodCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocDocPeriodCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocOpenBtnCaption;
#define Dsdocclientconst_dsDocDocImportDocOpenBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocOpenBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocApplyBtnCaption;
#define Dsdocclientconst_dsDocDocImportDocApplyBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocApplyBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocCancelBtnCaption;
#define Dsdocclientconst_dsDocDocImportDocCancelBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocImportDocOperLabCaption;
#define Dsdocclientconst_dsDocDocImportDocOperLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocImportDocOperLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDoc;
#define Dsdocclientconst_dsDocDocExportDoc System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDoc)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportStatusSBPanels2;
#define Dsdocclientconst_dsDocDocExportStatusSBPanels2 System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportStatusSBPanels2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocFileFilter1;
#define Dsdocclientconst_dsDocDocExportDocFileFilter1 System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocFileFilter1)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocFileFilter2;
#define Dsdocclientconst_dsDocDocExportDocFileFilter2 System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocFileFilter2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocDefCaption;
#define Dsdocclientconst_dsDocDocExportDocDocDefCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocDefCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocCaption;
#define Dsdocclientconst_dsDocDocExportDocDocCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocSaveCaption;
#define Dsdocclientconst_dsDocDocExportDocSaveCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocSaveCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocBreakExportConf;
#define Dsdocclientconst_dsDocDocExportDocBreakExportConf System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocBreakExportConf)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocCaption;
#define Dsdocclientconst_dsDocDocExportDocCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocExpSpecTSCaption;
#define Dsdocclientconst_dsDocDocExportDocExpSpecTSCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocExpSpecTSCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocLabel2Caption;
#define Dsdocclientconst_dsDocDocExportDocLabel2Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocLabel2Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocLabel6Caption;
#define Dsdocclientconst_dsDocDocExportDocLabel6Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocLabel6Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocLabel1Caption;
#define Dsdocclientconst_dsDocDocExportDocLabel1Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocLabel1Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocFilterPanelTopBtnHint;
#define Dsdocclientconst_dsDocDocExportDocFilterPanelTopBtnHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocFilterPanelTopBtnHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocFilterPanelBottomBtnHint;
#define Dsdocclientconst_dsDocDocExportDocFilterPanelBottomBtnHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocFilterPanelBottomBtnHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocResetBtnHint;
#define Dsdocclientconst_dsDocDocExportDocResetBtnHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocResetBtnHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocResetBtnCaption;
#define Dsdocclientconst_dsDocDocExportDocResetBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocResetBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocSpecNameCaption;
#define Dsdocclientconst_dsDocDocExportDocSpecNameCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocSpecNameCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocSpecPeridCaption;
#define Dsdocclientconst_dsDocDocExportDocSpecPeridCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocSpecPeridCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocExpDocTSCaption;
#define Dsdocclientconst_dsDocDocExportDocExpDocTSCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocExpDocTSCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocPeriodLabCaption;
#define Dsdocclientconst_dsDocDocExportDocPeriodLabCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocPeriodLabCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocLabel3Caption;
#define Dsdocclientconst_dsDocDocExportDocLabel3Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocLabel3Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocLabel7Caption;
#define Dsdocclientconst_dsDocDocExportDocLabel7Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocLabel7Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocLabel8Caption;
#define Dsdocclientconst_dsDocDocExportDocLabel8Caption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocLabel8Caption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocFilterPanelBottomBtnHint;
#define Dsdocclientconst_dsDocDocExportDocDocFilterPanelBottomBtnHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocFilterPanelBottomBtnHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocFilterPanelTopBtnHint;
#define Dsdocclientconst_dsDocDocExportDocDocFilterPanelTopBtnHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocFilterPanelTopBtnHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocResetBtnHint;
#define Dsdocclientconst_dsDocDocExportDocDocResetBtnHint System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocResetBtnHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocResetBtnCaption;
#define Dsdocclientconst_dsDocDocExportDocDocResetBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocResetBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocNameCaption;
#define Dsdocclientconst_dsDocDocExportDocDocNameCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocNameCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocPeriodCaption;
#define Dsdocclientconst_dsDocDocExportDocDocPeriodCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocPeriodCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocSaveBtnCaption;
#define Dsdocclientconst_dsDocDocExportDocSaveBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocSaveBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocCancelBtnCaption;
#define Dsdocclientconst_dsDocDocExportDocCancelBtnCaption System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocPeriodicityCBItem1;
#define Dsdocclientconst_dsDocDocExportDocPeriodicityCBItem1 System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocPeriodicityCBItem1)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocPeriodicityCBItem2;
#define Dsdocclientconst_dsDocDocExportDocPeriodicityCBItem2 System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocPeriodicityCBItem2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocPeriodicityCBItem3;
#define Dsdocclientconst_dsDocDocExportDocPeriodicityCBItem3 System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocPeriodicityCBItem3)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocPeriodicityCBItem1;
#define Dsdocclientconst_dsDocDocExportDocDocPeriodicityCBItem1 System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocPeriodicityCBItem1)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocPeriodicityCBItem2;
#define Dsdocclientconst_dsDocDocExportDocDocPeriodicityCBItem2 System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocPeriodicityCBItem2)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocPeriodicityCBItem3;
#define Dsdocclientconst_dsDocDocExportDocDocPeriodicityCBItem3 System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocPeriodicityCBItem3)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocPeriodicityCBItem4;
#define Dsdocclientconst_dsDocDocExportDocDocPeriodicityCBItem4 System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocPeriodicityCBItem4)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocPeriodicityCBItem5;
#define Dsdocclientconst_dsDocDocExportDocDocPeriodicityCBItem5 System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocPeriodicityCBItem5)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocPeriodicityCBItem6;
#define Dsdocclientconst_dsDocDocExportDocDocPeriodicityCBItem6 System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocPeriodicityCBItem6)
extern DELPHI_PACKAGE System::ResourceString _dsDocDocExportDocDocPeriodicityCBItem7;
#define Dsdocclientconst_dsDocDocExportDocDocPeriodicityCBItem7 System::LoadResourceString(&Dsdocclientconst::_dsDocDocExportDocDocPeriodicityCBItem7)
extern DELPHI_PACKAGE System::ResourceString _dsDocMatrixErrorParam;
#define Dsdocclientconst_dsDocMatrixErrorParam System::LoadResourceString(&Dsdocclientconst::_dsDocMatrixErrorParam)
extern DELPHI_PACKAGE System::ResourceString _dsDocMatrixErrorIndex;
#define Dsdocclientconst_dsDocMatrixErrorIndex System::LoadResourceString(&Dsdocclientconst::_dsDocMatrixErrorIndex)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditErrorRow;
#define Dsdocclientconst_dsDocHandEditErrorRow System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditErrorRow)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditErrorCol;
#define Dsdocclientconst_dsDocHandEditErrorCol System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditErrorCol)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditCheckRulesMsg;
#define Dsdocclientconst_dsDocHandEditCheckRulesMsg System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditCheckRulesMsg)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditCheckRulesCpt;
#define Dsdocclientconst_dsDocHandEditCheckRulesCpt System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditCheckRulesCpt)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditStructHeaderCaption;
#define Dsdocclientconst_dsDocHandEditStructHeaderCaption System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditStructHeaderCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditmiDocCaption;
#define Dsdocclientconst_dsDocHandEditmiDocCaption System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditmiDocCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditmiViewCaption;
#define Dsdocclientconst_dsDocHandEditmiViewCaption System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditmiViewCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditmiJumpCaption;
#define Dsdocclientconst_dsDocHandEditmiJumpCaption System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditmiJumpCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditSaveCaption;
#define Dsdocclientconst_dsDocHandEditSaveCaption System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditSaveCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditSaveHint;
#define Dsdocclientconst_dsDocHandEditSaveHint System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditSaveHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditReCalcCaption;
#define Dsdocclientconst_dsDocHandEditReCalcCaption System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditReCalcCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditReCalcHint;
#define Dsdocclientconst_dsDocHandEditReCalcHint System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditReCalcHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditFirstDocElementCaption;
#define Dsdocclientconst_dsDocHandEditFirstDocElementCaption System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditFirstDocElementCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditFirstDocElementHint;
#define Dsdocclientconst_dsDocHandEditFirstDocElementHint System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditFirstDocElementHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditPrevDocElementCaption;
#define Dsdocclientconst_dsDocHandEditPrevDocElementCaption System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditPrevDocElementCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditPrevDocElementHint;
#define Dsdocclientconst_dsDocHandEditPrevDocElementHint System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditPrevDocElementHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditNextDocElementCaption;
#define Dsdocclientconst_dsDocHandEditNextDocElementCaption System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditNextDocElementCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditNextDocElementHint;
#define Dsdocclientconst_dsDocHandEditNextDocElementHint System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditNextDocElementHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditLastDocElementCaption;
#define Dsdocclientconst_dsDocHandEditLastDocElementCaption System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditLastDocElementCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditLastDocElementHint;
#define Dsdocclientconst_dsDocHandEditLastDocElementHint System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditLastDocElementHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditViewStructCaption;
#define Dsdocclientconst_dsDocHandEditViewStructCaption System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditViewStructCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditViewStructHint;
#define Dsdocclientconst_dsDocHandEditViewStructHint System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditViewStructHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditViewDocElementInscCaption;
#define Dsdocclientconst_dsDocHandEditViewDocElementInscCaption System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditViewDocElementInscCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditViewDocElementInscHint;
#define Dsdocclientconst_dsDocHandEditViewDocElementInscHint System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditViewDocElementInscHint)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditExitCaption;
#define Dsdocclientconst_dsDocHandEditExitCaption System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditExitCaption)
extern DELPHI_PACKAGE System::ResourceString _dsDocHandEditExitHint;
#define Dsdocclientconst_dsDocHandEditExitHint System::LoadResourceString(&Dsdocclientconst::_dsDocHandEditExitHint)
}	/* namespace Dsdocclientconst */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_DSDOCCLIENTCONST)
using namespace Dsdocclientconst;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// DsdocclientconstHPP
