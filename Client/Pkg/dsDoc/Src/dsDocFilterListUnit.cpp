// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsDocFilterListUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "pkgsetting.h"
#ifndef _CONFSETTTING
#error _CONFSETTTING using are not defined
#endif
#include "msgdef.h"
#include "dsDocClientConstDef.h"
#include "dsDocExtFunc.h"
// #include "DMF.h"
// ---------------------------------------------------------------------------
#define FilterCODE        "code"
#define FilterGUI         "000C"
#define FilterNAME        "001E"
#define FilterF_OWNER     "002E"
#define FilterFLTTEXT     "001F"
#define FilterCHTYPE      "0020"
#define FilterMODULE_TYPE "0021"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxClasses"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxData"
#pragma link "cxDataStorage"
#pragma link "cxEdit"
#pragma link "cxFilter"
#pragma link "cxGraphics"
#pragma link "cxGrid"
#pragma link "cxGridCustomTableView"
#pragma link "cxGridCustomView"
#pragma link "cxGridLevel"
#pragma link "cxGridTableView"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxNavigator"
#pragma link "cxStyles"
#pragma link "dxBar"
#pragma link "dxStatusBar"
#pragma resource "*.dfm"
TdsDocFilterListForm * dsDocFilterListForm;
// ---------------------------------------------------------------------------
__fastcall TdsDocFilterListForm::TdsDocFilterListForm(TComponent * Owner, UnicodeString ACapt, int AType,
  TdsDocDM * ADM, bool ACanSel) : TForm(Owner)
 {
  FDM                       = ADM;
  FDataSrc                  = NULL;
  FCanSel                   = ACanSel;
  RootClass                 = FDM->RegDef->GetTagByUID("0003");
  FCtrList                  = new TdsRegEDContainer(this, this /* ExtCompPanel */);
  FCtrList->DefXML          = RootClass;
  FCtrList->StyleController = FDM->StyleController;
  FCtrList->DataProvider    = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DefXML = RootClass;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  FCtrList->DataPath                    = FDM->DataPath;
  curCD                                 = 0;
  CheckCtrlVisible();
  CreateSrc();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::CreateSrc()
 {
  Application->ProcessMessages();
  try
   {
    try
     {
      Caption                         = RootClass->AV["name"];
      FDataSrc                        = new TkabCustomDataSource(RootClass);
      FDataSrc->DataSet->OnGetCount   = FDM->kabCDSGetCount;
      FDataSrc->DataSet->OnGetIdList  = FDM->kabCDSGetCodes;
      FDataSrc->DataSet->OnGetValById = FDM->kabCDSGetValById;
      FDataSrc->DataSet->OnInsert     = FDM->kabCDSInsert;
      FDataSrc->DataSet->OnEdit       = FDM->kabCDSEdit;
      FDataSrc->DataSet->OnDelete     = FDM->kabCDSDelete;
      FDataSrc->SetColumns(FltListView, "001E:������������");
      FltListView->DataController->CustomDataSource = FDataSrc;
      ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DataSource = FDataSrc;
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsErrorMsgCaptionSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegistryErrorCommDBErrorMsgCaption), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::ChangeSrc()
 {
  CheckCtrlState(false);
  TTime FBegTime = Time();
  UnicodeString _ES_ = "";
  // UnicodeString TimeMsg = "";
  Application->ProcessMessages();
  try
   {
    try
     {
      FltListView->BeginUpdate();
      try
       {
        FDataSrc->DataSet->GetData(NULL, TdsRegFetch::Auto, 20, FGetDataProgress);
        FBegTime = Time();
        FDataSrc->DataChanged();
       }
      __finally
       {
        FltListView->EndUpdate();
        MsgLab->Caption = MsgLab->Caption + " ���������: " + (Time() - FBegTime).FormatString("ss.zzz");
       }
      if (curCD)
       {
        TLocateOptions opt;
        // if(!quClass()->Locate("CODE",Variant(curCD),opt))
        // quClass()->First();
       }
      // else
      // quClass()->First();
      RootClass->Iterate(CreateListExt, _ES_);
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsErrorMsgCaptionSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegistryErrorCommDBErrorMsgCaption), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
    CheckCtrlState(true);
    Application->ProcessMessages();
    ActiveControl = FltList;
    // _MSG_DBG(TimeMsg);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::FGetDataProgress(int AMax, int ACur, UnicodeString AMsg)
 {
  if (AMax == -1)
   {
    PrBar->Position = 0;
    MsgLab->Caption = "";
   }
  else if (!(AMax + ACur))
   {
    PrBar->Position = 0;
    MsgLab->Caption = AMsg;
   }
  else
   {
    MsgLab->Caption = AMsg + " [" + IntToStr(ACur) + "/" + IntToStr(AMax) + "]";
    if (PrBar->Max != AMax)
     PrBar->Max = AMax;
    PrBar->Position = ACur;
   }
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_INSERT)
   actInsertExecute(actInsert);
  else if ((Key == VK_RETURN) && Shift.Contains(ssShift) && actEdit->Enabled)
   actEditExecute(actEdit);
  else if ((Key == VK_DELETE) && actDelete->Enabled)
   actDeleteExecute(actDelete);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocFilterListForm::FCanClose(TFilterEditExForm * AEditDlg, UnicodeString ACondName)
 {
  bool RC = false;
  TTagNode * tmpFlt = new TTagNode(NULL);
  try
   {
    if (AEditDlg)
     {
      try
       {
        bool FCont = false;
        tmpFlt->Assign(AEditDlg->Filter, true);
        if (FDataSrc->DataSet->State == dsInsert)
         {
          EditedRow()->Value[FilterGUI] = GetSpecGUI(tmpFlt);
          EditedRow()->Value[FilterCHTYPE] = 0;
          FCont = true;
         }
        else if (FDataSrc->DataSet->State == dsEdit)
         {
          FCont = true;
         }
        if (FCont)
         {
          EditedRow()->Value[FilterNAME] = GetSpecName(tmpFlt);
          EditedRow()->Value[FilterMODULE_TYPE] = FDM->SubDir;
          UnicodeString PRC = FDataSrc->DataSet->Post(FInsertedRecId, true);
          if (!SameText(PRC, "ok"))
           _MSG_ERR(PRC, "������");
          else
           RC = true;
         }
       }
      catch (EkabCustomDataSetError & E)
       {
        _MSG_ERR(E.Message, "������");
       }
     }
   }
  __finally
   {
    delete tmpFlt;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::actInsertExecute(TObject * Sender)
 {
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmFilterListInsert"); // # ���������� ��������. ����������.
#else
  if (FDM->FilterEditable)
   {
    TTagNode * __tmpFlt = new TTagNode(NULL);
    TFilterEditExForm * EditDlg = new TFilterEditExForm(this);
    try
     {
      // UnicodeString flName;
      // __int64 Code;
      FDataSrc->DataSet->Insert();
      // Dlg->LargeIcon = true;
      EditDlg->OnCallDefProc = FDM->OnCallDefProc;
      EditDlg->EditType      = fetFilter;
      EditDlg->Nom           = FDM->Nom; // !!!
      EditDlg->XMLContainer  = FDM->XMLList;
      EditDlg->CanKonkr      = false;
      EditDlg->New();
      EditDlg->OnCanClose = FCanClose;
      EditDlg->ShowModal();
      if (EditDlg->ModalResult == mrOk)
       {
        __tmpFlt->Assign(EditDlg->Filter, true);
        // flName = GetSpecName(__tmpFlt);
        try
         {
          ////            EditedRow()->Value[FilterCODE]        = 1;  //FDM->qu->AsInteger
          // EditedRow()->Value[FilterGUI]         = GetSpecGUI(__tmpFlt); //FDM->qu->AsString
          ////            EditedRow()->Value[FilterFLTTEXT]     = __tmpFlt->AsXML;  //FDM->qu->AsString
          // EditedRow()->Value[FilterNAME]        = flName; //FDM->qu->AsString
          // EditedRow()->Value[FilterCHTYPE]      = 0;//FDM->qu->AsInteger
          // EditedRow()->Value[FilterMODULE_TYPE] = FDM->SubDir; //FDM->qu->AsString
          // UnicodeString CodeRC = "";
          // UnicodeString PRC = FDataSrc->DataSet->Post(CodeRC);
          // if (!SameText(PRC,"ok"))
          // _MSG_ERR(PRC,"������");
          // else
          // {
          // curCD = FInsertedRecId.ToIntDef(0);
          RefreshById(FInsertedRecId);
          FDM->SetTextData(ctFlt, FilterFLTTEXT, FInsertedRecId, __tmpFlt->AsXML);
          // }
         }
        catch (System::Sysutils::Exception & E)
         {
          _MSG_ERR(E.Message, FMT(dsDocCommonErrorCaption));
          // quFilterCancel();
         }
       }
      ChangeSrc();
     }
    __finally
     {
      delete __tmpFlt;
      delete EditDlg;
     }
    ActiveControl = FltList;
   }
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::actEditExecute(TObject * Sender)
 {
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmFilterListEdit"); // # ���������� ��������. ��������������.
#endif
  // if (actApply->Visible)
  // actApplyExecute(actApply);
  // else
  // {
  // if(FDM->GetCount("FILTERS",FDM->quFilter->Filter) < 1)
  // return;
  if (FDM->FilterEditable)
   {
    TTagNode * __tmpFlt = new TTagNode(NULL);
    TFilterEditExForm * EditDlg = new TFilterEditExForm(this);
    try
     {
      // UnicodeString flName;
      FDataSrc->DataSet->Edit();
      __tmpFlt->AsXML = FDM->GetTextData(ctFlt, FilterFLTTEXT, CurRow()->Value[FilterCODE]);
      // __int64 Code;
      // Dlg->LargeIcon = true;
      // Dlg->NomDef = FDM->ICSNom;
      EditDlg->OnCallDefProc = FDM->OnCallDefProc;
      EditDlg->EditType      = fetFilter;
      EditDlg->Nom           = FDM->Nom; // !!!
      EditDlg->XMLContainer  = FDM->XMLList;
      EditDlg->CanKonkr      = false;
      // Dlg->New();
      EditDlg->Filter     = __tmpFlt;
      EditDlg->OnCanClose = FCanClose;
      EditDlg->ShowModal();
      if (EditDlg->ModalResult == mrOk)
       {
        __tmpFlt->Assign(EditDlg->Filter, true);
        // flName = GetSpecName(__tmpFlt);
        try
         {
          ////              curCD = CurRow()->Value[FilterCODE];
          ////              EditedRow()->Value[FilterFLTTEXT]     = __tmpFlt->AsXML;  //FDM->qu->AsString
          // EditedRow()->Value[FilterNAME]        = flName; //FDM->qu->AsString
          // EditedRow()->Value[FilterMODULE_TYPE] = FDM->SubDir; //FDM->qu->AsString
          // UnicodeString CodeRC = "";
          // UnicodeString PRC = FDataSrc->DataSet->Post(CodeRC);
          // if (!SameText(PRC,"ok"))
          // _MSG_ERR(PRC,"������");
          // else
          // {
          // curCD = FInsertedRecId.ToIntDef(0);
          RefreshById(FInsertedRecId);
          FDM->SetTextData(ctFlt, FilterFLTTEXT, FInsertedRecId, __tmpFlt->AsXML);
          // }
         }
        catch (System::Sysutils::Exception & E)
         {
          _MSG_ERR(E.Message, FMT(dsDocCommonErrorCaption));
          // quFilterCancel();
         }
       }
      ChangeSrc();
     }
    __finally
     {
      delete __tmpFlt;
      delete EditDlg;
     }
    ActiveControl = FltList;
    // }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::actDeleteExecute(TObject * Sender)
 {
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmFilterListDelete"); // # ���������� ��������. ��������.
#else
  if (FDM->FilterEditable)
   {
    __int64 delCode;
    // if(FDM->GetCount("FILTERS",FDM->quFilter->Filter) < 1)
    // return;
    try
     {
      if (FCanDelete)
       {
        bool FDelete = true;
        UnicodeString FMsg;
        FCanDelete(CurRow()->Value[FilterCODE], FDelete, FMsg);
        if (!FDelete)
         {
          _MSG_ERR(FMsg.c_str(), cFMT(dsDocCommonErrorCaption));
          return;
         }
       }
      if (_MSG_QUE(FMT2(dsDocCommonDelConfMsg, FMT(dsDocDocChoiceListChioceCaption),
        "\"" + CurRow()->Value[FilterNAME] + "\""), FMT(dsDocCommonConfRecDelCaption)) != IDYES)
       return;
      // delCode = CurRow()->Value[FilterCODE];
      // FDM->quFilter->Next();
      curCD = CurRow()->Value[FilterCODE];
      // TLocateOptions opt;
      // FDM->quFilter->Locate("CODE",Variant(delCode),opt);
      FDataSrc->DataSet->Delete();
      // quFilterCommit();
     }
    catch (...)
     {
      _MSG_ERR(FMT1(dsDocCommonErrorDelMsg, FMT(dsDocDocChoiceListChioceCaption)),
      FMT(dsDocCommonErrorCaption));
      // quFilterCancel();
     }
    ChangeSrc();
    ActiveControl = FltList;
   }
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::quFilterCommit()
 {
  // FDM->trFilter->Commit();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::quFilterCancel()
 {
  // FDM->quFilter->Cancel();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::actCloseExecute(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::actApplyExecute(TObject * Sender)
 {
  // if(FDM->GetCount("FILTERS",FDM->quFilter->Filter) < 1)
  // return;
  TTagNode * __tmpFlt = NULL;
  try
   {
    FSelFlt = new TTagNode(NULL);
    UnicodeString flName;
    UnicodeString sFltName = VarToStr(CurRow()->Value[FilterNAME]).UpperCase();
    FSelFlt->AsXML = CurRow()->Value[FilterFLTTEXT];
   }
  __finally
   {
   }
  ModalResult = mrOk;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::FormDestroy(TObject * Sender)
 {
  if (FSelFlt)
   delete FSelFlt;
  FltListView->DataController->CustomDataSource = NULL;
  delete FCtrList->DataProvider;
  delete FCtrList;
  FCtrList = NULL;
  delete FDataSrc;
  FDataSrc = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::FormConstrainedResize(TObject * Sender, int & MinWidth, int & MinHeight,
  int & MaxWidth, int & MaxHeight)
 {
  ((TcxGridDBTableView *)FltList->ActiveView)->Columns[0]->Width =
    FltList->Width - ::GetSystemMetrics(SM_CXVSCROLL) - 6;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::CheckCtrlState(bool AEnable)
 {
  actInsert->Enabled = AEnable && FDM->FilterEditable && FDM->FullEdit;
  actEdit->Enabled   = false;
  actDelete->Enabled = false;
  if (AEnable && FltListView->DataController->CustomDataSource && FDataSrc)
   {
    if (IsRecSelected())
     {
      actEdit->Enabled   = AEnable && FDM->FilterEditable && FDM->FullEdit;
      actDelete->Enabled = AEnable && FDM->FilterEditable && FDM->FullEdit;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::CheckCtrlVisible()
 {
  actApply->Visible  = FCanSel;
  actInsert->Visible = !FCanSel;
  actEdit->Visible   = !FCanSel;
  actDelete->Visible = !FCanSel;
  actApply->Enabled  = actApply->Visible;
  actInsert->Enabled = actInsert->Visible;
  actEdit->Enabled   = actEdit->Visible;
  actDelete->Enabled = actDelete->Visible;
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocFilterListForm::CreateListExt(TTagNode * itTag, UnicodeString & UID)
 {
  TdsRegLabItem * tmpItem;
  if (!itTag->CmpName("class,description,actuals"))
   {
    if (itTag->CmpAV("inlist", "e"))
     {
      // tmpItem = FCtrList->AddLabItem(itTag, ExtCompPanel->Width, ExtCompPanel);
      tmpItem->Top = xcTop;
      xcTop += tmpItem->Height;
      // if (itTag->CmpName("extedit"))
      // tmpItem->OnGetExtData = FDM->OnGetExtData;
     }
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::ShowTimerTimer(TObject * Sender)
 {
  ShowTimer->Enabled = false;
  Application->ProcessMessages();
  ChangeSrc();
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::FormShow(TObject * Sender)
 {
  FltListView->DataController->CustomDataSource = FDataSrc;
  ShowTimer->Enabled                            = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::FormClose(TObject * Sender, TCloseAction & Action)
 {
  FltListView->DataController->CustomDataSource = NULL;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocFilterListForm::IsRecSelected()
 {
  return FltListView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::FltListViewFocusedRecordChanged(TcxCustomGridTableView * Sender,
  TcxCustomGridRecord * APrevFocusedRecord, TcxCustomGridRecord * AFocusedRecord, bool ANewItemRecordFocusingChanged)
 {
  if (IsRecSelected())
   FDataSrc->DataSet->GetDataById(VarToStr(FDataSrc->DataSet->CurRow->Value[FilterCODE]), FGetDataProgress);
  // FCtrList->LabDataChange();
  CheckCtrlState(true);
  // FCtrList->LabDataChange();
  // actEdit->Enabled   = ClassGridView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1) && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("editlabel"));
  // actDelete->Enabled = ClassGridView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1) && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("deletelabel"));
 }
// ---------------------------------------------------------------------------
TkabCustomDataSetRow * __fastcall TdsDocFilterListForm::CurRow()
 {
  TkabCustomDataSetRow * RC = NULL;
  try
   {
    if (FDataSrc)
     RC = FDataSrc->DataSet->CurRow;
    else
     throw Exception(FMT(dsDocDocSpecListNoDataSrc));
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TkabCustomDataSetRow * __fastcall TdsDocFilterListForm::EditedRow()
 {
  TkabCustomDataSetRow * RC = NULL;
  try
   {
    if (FDataSrc)
     if (FDataSrc->DataSet->EditedRow)
      RC = FDataSrc->DataSet->EditedRow;
    if (!RC)
     throw Exception(FMT(dsDocDocSpecListNoDataSrc));
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocFilterListForm::RefreshById(UnicodeString ACode)
 {
  FltListView->BeginUpdate();
  FDataSrc->DataSet->GetDataById(ACode, FGetDataProgress);
  FDataSrc->DataChanged();
  StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
  // __int64 FCodeIdx = FDataSrc->DataSet->GetRowIdxByCode(ACode);
  // FltListView->DataController->FocusedRowIndex = FDataSrc->DataController->GetRowIndexByRecordIndex(FCodeIdx, true);
  FltListView->EndUpdate();
  FltListView->DataController->FocusedRecordIndex = FDataSrc->DataSet->GetRowIdxByCode(ACode);
 }
// ---------------------------------------------------------------------------
