object dsDocGetCheckDocOrgForm: TdsDocGetCheckDocOrgForm
  Left = 725
  Top = 315
  ActiveControl = ByTypeRB
  BorderIcons = [biSystemMenu]
  BorderWidth = 3
  Caption = #1042#1099#1073#1086#1088' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
  ClientHeight = 106
  ClientWidth = 501
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object ByTypeRB: TcxRadioButton
    Left = 13
    Top = 14
    Width = 120
    Height = 17
    Caption = #1055#1086' '#1090#1080#1087#1091
    Checked = True
    TabOrder = 0
    TabStop = True
    OnClick = ByTypeRBClick
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
  end
  object ByNameRB: TcxRadioButton
    Left = 13
    Top = 36
    Width = 121
    Height = 17
    Caption = #1055#1086' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1102
    TabOrder = 1
    OnClick = ByNameRBClick
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
  end
  object OrgTypeCB: TcxComboBox
    Left = 145
    Top = 9
    Properties.DropDownListStyle = lsEditFixedList
    Properties.OnChange = OrgTypeCBPropertiesChange
    TabOrder = 2
    Width = 251
  end
  object Panel1: TPanel
    Left = 0
    Top = 65
    Width = 501
    Height = 41
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 4
    DesignSize = (
      501
      41)
    object CancelBtn: TcxButton
      Left = 421
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
    object ApplyBtn: TcxButton
      Left = 334
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = ApplyBtnClick
    end
  end
  object OrgCB: TcxComboBox
    Left = 144
    Top = 36
    Enabled = False
    Properties.DropDownListStyle = lsEditFixedList
    Properties.OnChange = OrgCBPropertiesChange
    TabOrder = 3
    Width = 349
  end
end
