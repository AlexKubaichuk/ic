// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsDocImportDocUnit.h"
#include "pkgsetting.h"
#include "msgdef.h"
#ifndef _CONFSETTTING
#error _CONFSETTTING using are not defined
#endif
#include "dsDocClientConstDef.h"
#include "ExtUtils.h"
#include "dsDocExtFunc.h"
#include "dcDocHandEdit.h"
#include "JSONUtils.h"
// ---------------------------------------------------------------------------
#define SpecGUI         "0005"
#define SpecNAME        "0006"
#define SpecSPECTEXT    "000E"
#define SpecF_OWNER     "0007"
#define SpecF_PERIOD    "0008"
#define SpecCREATEDATE  "0010"
#define SpecINTEGRATED  "0009"
#define SpecWTEMPLATE   "000F"
#define SpecSPECTYPE    "000A"
#define SpecMODULE_TYPE "0011"
#define DocGUI         "000B"
#define DocSPECGUI     "0012"
#define DocNAME        "0013"
#define DocDOCTEXT     "0014"
#define DocF_OWNER     "0015"
#define DocF_SPECOWNER "0016"
#define DocF_PERIOD    "0017"
#define DocCREATEDATE  "0018"
#define DocF_FROMDATE  "0019"
#define DocF_TODATE    "001A"
#define DocINTEGRATED  "001B"
#define DocHANDCREATE  "001C"
#define DocMODULE_TYPE "001D"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxCheckBox"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLabel"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxPC"
#pragma link "cxProgressBar"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxStatusBar"
#pragma resource "*.dfm"
TdsDocImportDocForm * dsDocImportDocForm;
// ---------------------------------------------------------------------------
__fastcall TdsDocImportDocForm::TdsDocImportDocForm(TComponent * Owner, TdsDocDM * ADM, bool ASpec, bool ADoc)
  : TForm(Owner)
 {
  FDM     = ADM;
  _ES_    = "";
  Caption = FMT(dsDocDocImportDocCaption);
  // SpecTS->Caption = FMT(dsDocDocImportDocSpecTSCaption);
  // SpecName->Caption->Text = FMT(dsDocDocImportDocSpecNameCaption);
  // SpecOwner->Caption->Text = FMT(dsDocDocImportDocSpecOwnerCaption);
  // SpecPerid->Caption->Text = FMT(dsDocDocImportDocSpecPeridCaption);
  // DocTS->Caption = FMT(dsDocDocImportDocDocTSCaption);
  // DocName->Caption->Text = FMT(dsDocDocImportDocDocNameCaption);
  // DocOwner->Caption->Text = FMT(dsDocDocImportDocDocOwnerCaption);
  // DocPeriod->Caption->Text = FMT(dsDocDocImportDocDocPeriodCaption);
  // OpenBtn->Caption = FMT(dsDocDocImportDocOpenBtnCaption);
  // ApplyBtn->Caption = FMT(dsDocDocImportDocApplyBtnCaption);
  // CancelBtn->Caption = FMT(dsDocDocImportDocCancelBtnCaption);
  // OperLab->Caption = FMT(dsDocDocImportDocOperLabCaption);
  FSpecDefNode       = FDM->RegDef->GetTagByUID("0001");
  FDocDefNode        = FDM->RegDef->GetTagByUID("0002");
  FSpecDS            = NULL;
  FDocDS             = NULL;
  ImportPC->HideTabs = true;
  // FDocI  = FDM->AppOpt->Vals["EIDocI"];
  // FSpecI = FDM->AppOpt->Vals["EISpecI"];
  SpecTS->TabVisible = ASpec;
  DocTS->TabVisible  = ADoc;
  if (SpecTS->TabVisible)
   {
    ImportPC->ActivePage = SpecTS;
    try
     {
      FSpecDS                          = new TkabCustomDataSource(FSpecDefNode);
      FSpecDS->DataSet->OnGetCount     = FDM->kabCDSGetCount;
      FSpecDS->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
      FSpecDS->DataSet->OnGetValById   = FDM->kabCDSGetValById;
      FSpecDS->DataSet->OnGetValById10 = FDM->kabCDSGetValById10;
      FSpecDS->DataSet->OnInsert       = FDM->kabCDSInsert;
      FSpecDS->DataSet->OnEdit         = FDM->kabCDSEdit;
      FSpecDS->DataSet->OnDelete       = FDM->kabCDSDelete;
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsErrorMsgCaptionSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegistryErrorCommDBErrorMsgCaption), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  if (DocTS->TabVisible)
   {
    ImportPC->ActivePage = DocTS;
    try
     {
      FDocDS                          = new TkabCustomDataSource(FDocDefNode);
      FDocDS->DataSet->OnGetCount     = FDM->kabCDSGetCount;
      FDocDS->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
      FDocDS->DataSet->OnGetValById   = FDM->kabCDSGetValById;
      FDocDS->DataSet->OnGetValById10 = FDM->kabCDSGetValById10;
      FDocDS->DataSet->OnInsert       = FDM->kabCDSInsert;
      FDocDS->DataSet->OnEdit         = FDM->kabCDSEdit;
      FDocDS->DataSet->OnDelete       = FDM->kabCDSDelete;
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsErrorMsgCaptionSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegistryErrorCommDBErrorMsgCaption), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  FInProgress = false;
  FLoadSpec();
  SetEnabled(true);
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmDocImport"); // # ������ ����������.
#endif
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocImportDocForm::SetUID(TTagNode * ANode, UnicodeString & AUID)
 {
  if (ANode->CmpName("IERef"))
   if (!ANode->AV["uid"].Length())
    ANode->AV["uid"] = ANode->NewUID();
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::FProgressInit(int AMax, UnicodeString AMsg)
 {
  if (!InProgress)
   throw EAbort("");
  PrBar->Properties->Max = 2 * AMax;
  PrBar->Position        = 0;
  PrBar->Update();
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::FProgressChange(int APercent, UnicodeString AMsg)
 {
  if (!InProgress)
   throw EAbort("");
  if (APercent == -1)
   PrBar->Position++ ;
  else if (APercent == -2)
   MsgLab->Caption = AMsg; // !!!
  else
   PrBar->Position = APercent;
  PrBar->Update();
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::FProgressComplite(UnicodeString AMsg)
 {
  if (!InProgress)
   throw EAbort("");
  PrBar->Position = 0;
  MsgLab->Caption = AMsg; // !!!
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::SetEnabled(bool AEnabled)
 {
  ImportPC->Enabled = AEnabled;
  actOpen->Enabled  = AEnabled;
  bool isCheck = EITag && (SpecChList->Root->Count || DocChList->Root->Count);
  actSaveToDB->Enabled             = AEnabled && isCheck;
  PackCB->Enabled                  = AEnabled && isCheck;
  SpecChList->Enabled              = AEnabled;
  DocChList->Enabled               = AEnabled;
  SpecChList->Styles->Content      = (AEnabled) ? EnabledStyle : DisabledStyle;
  DocChList->Styles->Content       = (AEnabled) ? EnabledStyle : DisabledStyle;
  SpecChList->Styles->ColumnHeader = (AEnabled) ? EnabledStyle : DisabledStyle;
  DocChList->Styles->ColumnHeader  = (AEnabled) ? EnabledStyle : DisabledStyle;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocImportDocForm::CanProgress()
 {
  bool RC = false;
  if (InProgress)
   {
    if (MessageBox(Handle, cFMT(dsDocDocImportDocBreakImportConf), cFMT(dsDocCommonMsgCaption),
      MB_ICONQUESTION | MB_YESNO) == ID_YES)
     {
      InProgress = false;
      RC         = true;
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::WndProc(Messages::TMessage & Message)
 {
  switch (Message.Msg)
   {
   case WM_CLOSE:
    if (CanProgress())
     {
      Message.Result = 0;
      return;
     }
    else
     inherited::WndProc(Message);
    break;
   }
  inherited::WndProc(Message);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::SpecChPropertiesEditValueChanged(TObject * Sender)
 {
  bool Import = false;
  if (SpecChList->IsEditing && SpecChList->SelectionCount && SpecChList->Selections[0])
   SpecChList->Selections[0]->EndEdit(false);
  for (int i = 0; i < SpecChList->Root->Count; i++)
   {
    /* if (SpecChList->SelectionCount && SpecChList->Selections[0] == SpecChList->Root->Items[i])
     {
     if (SpecChList->IsEditing)
     if (SpecChList->InplaceEditor->EditValue)//VarToWideStrDef(SpecChList->InplaceEditor->EditValue, "").UpperCase() == "TRUE")
     {
     Import = true;
     break;
     }
     }
     else */ if (SpecChList->Root->Items[i]->Values[0])
      // VarToWideStrDef(SpecChList->Root->Items[i]->Values[0],"").UpperCase() == "TRUE")
     {
      Import = true;
      break;
     }
   }
  if (!Import)
   {
    if (DocChList->IsEditing && DocChList->SelectionCount && DocChList->Selections[0])
     DocChList->Selections[0]->EndEdit(false);
    for (int i = 0; i < DocChList->Root->Count; i++)
     {
      /* if (DocChList->SelectionCount && DocChList->Selections[0] == DocChList->Root->Items[i])
       {
       if (DocChList->IsEditing)
       if (DocChList->InplaceEditor->EditValue)//VarToWideStrDef(DocChList->InplaceEditor->EditValue, "").UpperCase() == "TRUE")
       {
       Import = true;
       break;
       }
       }
       else */ if (DocChList->Root->Items[i]->Values[0])
        // VarToWideStrDef(DocChList->Root->Items[i]->Values[0],"").UpperCase() == "TRUE")
       {
        Import = true;
        break;
       }
     }
   }
  actSaveToDB->Enabled = Import;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::FormCloseQuery(TObject * Sender, bool & CanClose)
 {
  CanClose = !CanProgress();
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocImportDocForm::CheckByHandRules(TTagNode * ADoc)
 {
  bool RC = false;
  try
   {
    if (FDM->DocHandEditable)
     {
      TTagNode * FSpec = new TTagNode(NULL);
      TTagNode * FDoc = new TTagNode(NULL);
      TdsDocHandEditForm * Dlg = NULL;
      int FSpecType;
      try
       {
        if (FDM->GetSpec(GetDocSpecGUI(ADoc), FSpec, FSpecType, "������ �������� �������� ���������."))
         {
          FDoc->Assign(ADoc, true);
          Dlg = new TdsDocHandEditForm(this, FSpec, FDoc, FDM->XMLList);
          try
           {
            // Dlg->actReCalc->Visible = true;
            if (!Dlg->CheckByHandRules())
             {
              Dlg->ShowModal();
              if (Dlg->Modified)
               { // ��������� �������� � ������������� ������� ������ �����������
                ADoc->Assign(FDoc, true);
                RC = true;
               }
             }
            else
             RC = true;
           }
          __finally
           {
            if (Dlg)
             delete Dlg;
           }
         }
       }
      __finally
       {
        delete FSpec;
        delete FDoc;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::FLoadSpec()
 {
  FSpecGUIMap.clear();
  PackCB->Items->Clear();
  try
   {
    TJSONObject * RetData;
    TJSONString * tmpData;
    UnicodeString FClCode, FClStr;
    RetData = FDM->kabCDSGetValById
      ("doc.s2.Select GUI as ps1, NAME as ps2 From SPECIFIKATORS Where SPECTYPE = 10 Order By NAME", 0);
    TJSONPairEnumerator * itPair = RetData->GetEnumerator();
    PackCB->Items->Add("");
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      PackCB->Items->Add(FClStr);
      FSpecGUIMap[PackCB->Items->Count - 1] = FClCode;
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::actOpenExecute(TObject * Sender)
 {
  TcxTreeListNode * FNode;
  UnicodeString FOwnerName, FOwnerCode;
  // UnicodeString  FChValue;
  TTagNode * tmpS, *tmpD;
  // ImportPC->HideTabs = true;
  // SpecTS->TabVisible = false;
  // DocTS->TabVisible  = false;
  Application->ProcessMessages();
  try
   {
    if (OpenDlg->Execute())
     {
      InProgress = true;
      // ApplyBtn->Enabled = true;
      Application->ProcessMessages();
      if (ExtractFileExt(OpenDlg->FileName).LowerCase() == ".zxml")
       { // win data file
        if (EITag)
         delete EITag;
        EITag           = new TTagNode(NULL);
        MsgLab->Caption = FMT(dsDocDocImportDocLoadCaption); // !!!
        Application->ProcessMessages();
        EITag->OnProgressInit = FProgressInit;
        EITag->OnProgress     = FProgressChange;
        EITag->LoadFromZIPXMLFile(icsPrePath(OpenDlg->FileName));
        EITag->OnProgressInit   = NULL;
        EITag->OnProgress       = NULL;
        MsgLab->Caption         = ""; // !!!
        ImportPC->Pages[0]->Tag = 0;
        ImportPC->Pages[1]->Tag = 0;
        SpecChList->BeginUpdate();
        SpecChList->Clear();
        if (EITag->GetChildByName("specs") && FSpecDS)
         { // ��������� ������� �������� ����������
          ImportPC->Pages[0]->Tag = 1;
          tmpS                    = EITag->GetChildByName("specs")->GetFirstChild();
          // FChValue = "True";
          while (tmpS)
           {
            FOwnerCode                     = tmpS->GetChildByName("passport")->AV["autor"];
            FOwnerName                     = FDM->GetOwnerName(FOwnerCode);
            FNode                          = SpecChList->Root->AddChild();
            FNode->Values[0]               = true;
            FDisSpec[FNode->AbsoluteIndex] = true;
            if (!FOwnerName.Length() /* || (FChValue.UpperCase() == "FALSE") */)
             {
              // FChValue         = False;
              FNode->Values[0]               = false;
              FDisSpec[FNode->AbsoluteIndex] = false;
              FOwnerName                     = FMT1(dsDocDocImportDocOrgMissing, FOwnerCode);
             }
            FNode->Values[1] = tmpS->GetChildByName("passport")->AV["mainname"];
            FNode->Values[2] = FOwnerName;
            if (tmpS->GetChildByName("passport")->AV["perioddef"].ToIntDef(-1) == -1)
             {
              if (tmpS->GetChildByName("passport")->GetAVDef("perioddef", "no").UpperCase() == "NO")
               FNode->Values[3] = SpecPeriodRuStr(0);
              else
               FNode->Values[3] = SpecPeriodRuStr(1);
             }
            else
             FNode->Values[3] = SpecPeriodRuStr(tmpS->GetChildByName("passport")->AV["perioddef"].ToIntDef(0));
            FNode->Data = (void *)tmpS;
            tmpS        = tmpS->GetNext();
           }
         }
        SpecChList->EndUpdate();
        DocChList->BeginUpdate();
        DocChList->Clear();
        if (EITag->GetChildByName("docs") && FDocDS)
         { // ��������� ������� ����������
          ImportPC->Pages[1]->Tag = 1;
          tmpD                    = EITag->GetChildByName("docs")->GetFirstChild();
          UnicodeString DocPeriodicity, FFrDate, FToDate;
          // FChValue = "True";
          while (tmpD)
           {
            FOwnerCode                    = tmpD->GetChildByName("passport")->AV["autor"];
            FOwnerName                    = FDM->GetOwnerName(FOwnerCode);
            FNode                         = DocChList->Root->AddChild();
            FNode->Values[0]              = true;
            FDisDoc[FNode->AbsoluteIndex] = true;
            if (!FOwnerName.Length() /* || (FChValue.UpperCase() == "FALSE") */)
             {
              FDisDoc[FNode->AbsoluteIndex] = false;
              FOwnerName                    = FMT1(dsDocDocImportDocOrgMissing, FOwnerCode);
              // FChValue = "False";
              FNode->Values[0] = false;
             }
            FNode->Values[1] = tmpD->GetChildByName("passport")->AV["mainname"];
            FNode->Values[2] = FOwnerName;
            DocPeriodicity   = GetDocPeriod(tmpD, false);
            if (DocPeriodicity.ToIntDef(-1) == -1)
             {
              if (DocPeriodicity.LowerCase() == "year")
               DocPeriodicity = "5";
              else if (DocPeriodicity.LowerCase() == "halfyear")
               DocPeriodicity = "4";
              else if (DocPeriodicity.LowerCase() == "quarter")
               DocPeriodicity = "3";
              else if (DocPeriodicity.LowerCase() == "month")
               DocPeriodicity = "2";
              else if (DocPeriodicity.LowerCase() == "unique")
               DocPeriodicity = "1";
              else if (DocPeriodicity.LowerCase() == "no")
               DocPeriodicity = "0";
             }
            FFrDate = GetDocPeriodValFrom(tmpD, false);
            FToDate          = GetDocPeriodValTo(tmpD, false);
            FNode->Values[3] = GetDocPeriodValStr(DocPeriodicity, FFrDate, FToDate);
            FNode->Data      = (void *)tmpD;
            tmpD             = tmpD->GetNext();
           }
         }
        DocChList->EndUpdate();
       }
      else if (ExtractFileExt(OpenDlg->FileName).LowerCase() == ".let")
       {
        _MSG_ERR("DOS �� ��������������", "������");
       }
      SpecChPropertiesEditValueChanged(NULL);
     }
   }
  __finally
   {
    InProgress = false;
   }
  actSaveToDB->Enabled = SpecChList->Root->Count || DocChList->Root->Count;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::actSaveToDBExecute(TObject * Sender)
 {
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmDocImportExecute"); // # ������ ����������. ���������� � ��.
#else
  if (InProgress)
   {
    CanProgress();
    return;
   }
  InProgress = true;
  try
   {
    // ��������� ������
    UnicodeString OldName, NewName, OldGUI, NewGUI;
    TTagNode * SpecDoc = NULL;
    TTagNode * FSpec = NULL;
    // !!!      FDM->quDoc->DisableControls();
    TJSONObject * tmpDocValues = NULL;
    try
     {
      bool FCont;
      TReplaceFlags fRepl;
      fRepl << rfReplaceAll << rfIgnoreCase;
      if (EITag)
       {
        int isCheck = 0;
        if (SpecChList->IsEditing)
         SpecChList->Root->EndEdit(false);
        for (int i = 0; i < SpecChList->Root->Count; i++)
         if ((bool)SpecChList->Root->Items[i]->Values[0])
          isCheck++ ;
        if (isCheck)
         {
          MsgLab->Caption        = FMT(dsDocDocImportDocDocDefCaption); // !!!
          PrBar->Properties->Max = isCheck;
          PrBar->Position        = 0;
          PrBar->Update();
          for (int i = 0; i < SpecChList->Root->Count; i++)
           {
            if ((bool)SpecChList->Root->Items[i]->Values[0])
             {
              if (tmpDocValues)
               delete tmpDocValues;
              tmpDocValues = new TJSONObject;
              if (SpecDoc)
               delete SpecDoc;
              SpecDoc        = new TTagNode(NULL);
              SpecDoc->AsXML = StringReplace(((TTagNode *)SpecChList->Root->Items[i]->Data)->AsXML, "repl_uid",
                "uid", fRepl);
              UnicodeString tmpGUI = GetSpecGUI(SpecDoc);
              TExistsType RC = (TExistsType)FDM->CheckSpecExists(GetSpecName(SpecDoc), tmpGUI, false);
              // CheckExists(SpecDoc, tmpGUI, false);
              FCont = false;
              if (RC == etOk)
               FCont = true;
              else
               {
                UnicodeString FQueMsg = "������";
                if (RC == etExistByName)
                 FQueMsg = "�������� ��������� \"" + GetSpecName(SpecDoc) + "\"";
                else if (RC == etExistByCode)
                 FQueMsg = "�������� ��������� � ����� \"" + tmpGUI + "\" (\"" + GetSpecName(SpecDoc) + "\")";
                FQueMsg += " ��� ����������.\n�������� ������������ �������� ���������?";
                if (_MSG_QUE(FQueMsg, "�������������") == ID_YES)
                 FCont = true;
               }
              if (FCont)
               {
                tmpDocValues->AddPair(SpecGUI, tmpGUI);
                tmpDocValues->AddPair(SpecNAME, GetSpecName(SpecDoc));
                tmpDocValues->AddPair(SpecF_OWNER, (new TJSONObject)->AddPair(GetSpecOwner(SpecDoc, false), "au"));
                tmpDocValues->AddPair(SpecCREATEDATE, Date());
                tmpDocValues->AddPair(SpecF_PERIOD, (new TJSONObject)->AddPair(GetIntSpecPeriod(SpecDoc), "per"));
                tmpDocValues->AddPair(SpecINTEGRATED, (new TJSONObject)->AddPair((int)FDM->CanIntegrated(SpecDoc),
                  "int"));
                tmpDocValues->AddPair(SpecSPECTYPE, (new TJSONObject)->AddPair(0, "type"));
                tmpDocValues->AddPair(SpecMODULE_TYPE, FDM->SubDir);
                try
                 {
                  UnicodeString curCD = "0";
                  // UnicodeString PRC = FSpecDS->DataSet->Post(curCD, true);
                  UnicodeString PRC =
                    FDM->kabCDSImport("0001",
                    (TJSONObject *)TJSONObject::ParseJSONValue(tmpDocValues->ToString()), curCD);
                  if (!SameText(PRC, "ok"))
                   _MSG_ERR(PRC, "������.");
                  else
                   {
                    // GetPassport(SpecDoc)->AV["defversion"];
                    FDM->SetTextData(ctSpec, SpecSPECTEXT, curCD, StringReplace(SpecDoc->AsXML, "UpCase", "Upper",
                      fRepl));
                    SpecChList->Root->Items[i]->Values[0] = false;
                   }
                 }
                catch (System::Sysutils::Exception & E)
                 {
                  _MSG_ERR(E.Message, "������");
                 }
               }
              PrBar->Position++ ;
              Application->ProcessMessages();
              if (!InProgress)
               throw EAbort("");
             }
           }
         }
        isCheck = 0;
        if (DocChList->IsEditing)
         DocChList->Root->EndEdit(false);
        for (int i = 0; i < DocChList->Root->Count; i++)
         if ((bool)DocChList->Root->Items[i]->Values[0])
          isCheck++ ;
        if (isCheck)
         {
          MsgLab->Caption        = FMT(dsDocDocImportDocDocCaption); // !!!
          PrBar->Properties->Max = isCheck;
          PrBar->Position        = 0;
          PrBar->Update();
          for (int i = 0; i < DocChList->Root->Count; i++)
           {
            if ((bool)DocChList->Root->Items[i]->Values[0])
             {
              if (tmpDocValues)
               delete tmpDocValues;
              tmpDocValues = new TJSONObject;
              if (SpecDoc)
               delete SpecDoc;
              SpecDoc = new TTagNode(NULL);
              SpecDoc->Assign((TTagNode *)DocChList->Root->Items[i]->Data, true);
              if (FSpec)
               delete FSpec;
              FSpec = new TTagNode(NULL);
              bool FExist = false;
              if (FDM->GetSpecByGUI(GetDocSpecGUI(SpecDoc), FSpec))
               { // ������������ ��������� ����������
                TExistsType RC = FDM->CheckDocExists(GetDocOwner(SpecDoc), GetDocSpecGUI(SpecDoc),
                  GetDocPeriod(SpecDoc).ToIntDef(0), GetDocPeriodValFrom(SpecDoc).Trim() + ";" +
                  GetDocPeriodValTo(SpecDoc).Trim());
                FCont = false;
                if (RC == etOk)
                 FCont = true;
                else
                 {
                  UnicodeString FQueMsg = "������";
                  FExist  = true;
                  FQueMsg = "��������\n\"" + GetDocName(SpecDoc) + "\"\n������ - " +
                    GetDocPeriodValStr(GetDocPeriod(SpecDoc), GetDocPeriodValFrom(SpecDoc), GetDocPeriodValTo(SpecDoc));
                  FQueMsg += "\n��� ����������.\n�������� ������������ ��������?";
                  if (_MSG_QUE(FQueMsg, "�������������") == ID_YES)
                   FCont = true;
                 }
                if (FCont)
                 {
                  // ���������
                  SpecDoc->AsXML = StringReplace(SpecDoc->AsXML, "repl_uid", "uid", fRepl);
                  if (CheckByHandRules(SpecDoc))
                   {
                    tmpDocValues->AddPair(DocGUI, GetDocGUI(SpecDoc));
                    tmpDocValues->AddPair(DocSPECGUI, GetDocSpecGUI(SpecDoc));
                    tmpDocValues->AddPair(DocNAME, GetDocName(SpecDoc));
                    // FDocDS->DataSet->EditedRow->Value[DocDOCTEXT] = FDM->SubDir;
                    tmpDocValues->AddPair(DocF_OWNER, (new TJSONObject)->AddPair(GetDocOwner(SpecDoc), "au"));
                    tmpDocValues->AddPair(DocF_SPECOWNER, (new TJSONObject)->AddPair(GetSpecOwner(FSpec), "so"));
                    tmpDocValues->AddPair(DocF_PERIOD, (new TJSONObject)->AddPair(GetDocPeriod(SpecDoc).ToIntDef(0),
                      "per"));
                    tmpDocValues->AddPair(DocCREATEDATE, TSToDate(GetPassport(SpecDoc)->AV["timestamp"]));
                    if (GetDocPeriodValFrom(SpecDoc).Trim().Length())
                     tmpDocValues->AddPair(DocF_FROMDATE, TDate(GetDocPeriodValFrom(SpecDoc).Trim()));
                    if (GetDocPeriodValTo(SpecDoc).Trim().Length())
                     tmpDocValues->AddPair(DocF_TODATE, TDate(GetDocPeriodValTo(SpecDoc).Trim()));
                    tmpDocValues->AddPair(DocINTEGRATED, (new TJSONObject)->AddPair((int)FDM->CanIntegrated(FSpec),
                      "int"));
                    tmpDocValues->AddPair(DocHANDCREATE, (new TJSONObject)->AddPair(0, "hc"));
                    tmpDocValues->AddPair(DocMODULE_TYPE, FDM->SubDir);
                    try
                     {
                      UnicodeString curCD = "0";
                      // UnicodeString PRC = FDocDS->DataSet->Post(curCD, true);
                      UnicodeString PRC =
                        FDM->kabCDSImport("0002",
                        (TJSONObject *)TJSONObject::ParseJSONValue(tmpDocValues->ToString()), curCD);
                      if (!SameText(PRC, "ok"))
                       _MSG_ERR(PRC, "������.");
                      else
                       {
                        FDM->SetTextData(ctDoc, DocDOCTEXT, curCD, SpecDoc->AsXML);
                       }
                     }
                    catch (System::Sysutils::Exception & E)
                     {
                      _MSG_ERR(E.Message, "������");
                     }
                   }
                 }
               }
              PrBar->Position++ ;
              Application->ProcessMessages();
              if (!InProgress)
               throw EAbort("");
             }
           }
         }
       }
      else
       {
        _MSG_ERR(cFMT(dsDocDocImportDocImportFileMissing), "������");
       }
      PrBar->Position = 0;
      MsgLab->Caption = ""; // !!!
     }
    __finally
     {
      if (tmpDocValues)
       delete tmpDocValues;
      if (SpecDoc)
       delete SpecDoc;
      if (FSpec)
       delete FSpec;
     }
    // FDM->quDoc->EnableControls();
    MsgLab->Caption = "";
    MsgLab->Update();
    PrBar->Visible = false;
   }
  __finally
   {
    InProgress           = false;
    actSaveToDB->Enabled = true;
   }
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::FCheckDocItem(UnicodeString ASpecName, UnicodeString ASpecGUI, int APerType,
  TDate ADateFr, TDate ADateTo)
 {
  TTagNode * FDocNode;
  bool Exist = false;
  for (int i = 0; (i < DocChList->Root->Count) && !Exist; i++)
   {
    FDocNode = ((TTagNode *)DocChList->Root->Items[i]->Data);
    // FDocEl = (TDocEl*)FDocListData->Objects[i];
    if ((GetDocSpecGUI(FDocNode, false).UpperCase() == ASpecGUI.UpperCase()) && (GetDocPeriod(FDocNode,
      false).UpperCase() == APerType) && (GetDocPeriodValFrom(FDocNode, false) == ADateFr) &&
      (GetDocPeriodValTo(FDocNode, false) == ADateTo))
     {
      Exist                                = true;
      DocChList->Root->Items[i]->Values[0] = FDisSpec[DocChList->Root->Items[i]->AbsoluteIndex];
     }
   }
  if (!Exist)
   {
    FMissingSpecDoc->Add("<tr><td>" + ASpecName + "</td><td>" + ASpecGUI + "</td><td style='border-right: none;'>" +
      GetDocPeriodValStr(IntToStr(APerType), ADateFr.FormatString("dd.mm.yyyy"), ADateTo.FormatString("dd.mm.yyyy")) +
      "</td></tr>");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::FCheckSpecItem(UnicodeString ASpecName, UnicodeString ASpecGUI, int APerType,
  TDate ADateFr, TDate ADateTo)
 {
  TTagNode * FSpecNode;
  bool Exist = false;
  for (int i = 0; (i < SpecChList->Root->Count) && !Exist; i++)
   {
    FSpecNode = ((TTagNode *)SpecChList->Root->Items[i]->Data);
    // FDocEl = (TDocEl*)FDocListData->Objects[i];
    if (GetSpecGUI(FSpecNode, false).UpperCase() == ASpecGUI.UpperCase())
     {
      Exist                                 = true;
      SpecChList->Root->Items[i]->Values[0] = true; // "True";
     }
   }
  if (!Exist)
   {
    FMissingSpecDoc->Add("<tr><td>" + ASpecName + "</td><td style='border-right: none;'>" + ASpecGUI + "</td></tr>");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::actCheckByPackExecute(TObject * Sender)
 {
  if (PackCB->ItemIndex != -1)
   {
    FMissingSpecDoc = new TStringList;
    TTagNode * FPackDef = new TTagNode(NULL);
    int FSpecType;
    try
     {
      FMissingSpecDoc->Clear();
      if (ImportPC->ActivePage == DocTS)
       {
        if (FDM->GetSpec(FSpecGUIMap[PackCB->ItemIndex], FPackDef, FSpecType))
         {
          for (int i = 0; i < DocChList->Root->Count; i++)
           DocChList->Root->Items[i]->Values[0] = false; // "False";
          FDM->CheckPackage(FCheckDocItem, FPackDef);
         }
        // DocTV->DataController->CustomDataSource->DataChanged();
        if (FMissingSpecDoc->Count)
         {
          FMissingSpecDoc->Insert(0, "<html>" + FDM->StyleDef +
            "<body><h2>����������� ��������� ���������</h2><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><th>������������</th><th>���</th><th style='border-right: none;'>������</th></tr>");
          FMissingSpecDoc->Add("</table></body></html>");
          FDM->HTMLPreview(FMissingSpecDoc->Text);
         }
       }
      else
       {
        if (FDM->GetSpec(FSpecGUIMap[PackCB->ItemIndex], FPackDef, FSpecType))
         {
          for (int i = 0; i < SpecChList->Root->Count; i++)
           SpecChList->Root->Items[i]->Values[0] = false; // "False";
          FDM->CheckPackage(FCheckSpecItem, FPackDef);
         }
        // SpecTV->DataController->CustomDataSource->DataChanged();
        if (FMissingSpecDoc->Count)
         {
          FMissingSpecDoc->Insert(0, "<html>" + FDM->StyleDef +
            "<body><h2>����������� ��������� ��������</h2><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><th>������������</th><th style='border-right: none;'>���</th></tr>");
          FMissingSpecDoc->Add("</table></body></html>");
          FDM->HTMLPreview(FMissingSpecDoc->Text);
         }
       }
     }
    __finally
     {
      delete FMissingSpecDoc;
      FMissingSpecDoc = NULL;
      delete FPackDef;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::PackCBChange(TObject * Sender)
 {
  actCheckByPack->Enabled = (PackCB->ItemIndex > 0);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::actCloseExecute(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::SpecChListEditing(TcxCustomTreeList * Sender, TcxTreeListColumn * AColumn,
  bool & Allow)
 {
  if (SpecChList->SelectionCount && SpecChList->Selections[0])
   {
    Allow = FDisSpec[SpecChList->Selections[0]->AbsoluteIndex];
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::DocChListEditing(TcxCustomTreeList * Sender, TcxTreeListColumn * AColumn,
  bool & Allow)
 {
  if (DocChList->SelectionCount && DocChList->Selections[0])
   {
    Allow = FDisDoc[DocChList->Selections[0]->AbsoluteIndex];
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::SpecChListStylesGetContentStyle(TcxCustomTreeList * Sender,
  TcxTreeListColumn * AColumn, TcxTreeListNode * ANode, TcxStyle *& AStyle)
 {
  if (ANode)
   AStyle = (FDisSpec[ANode->AbsoluteIndex]) ? EnabledStyle : DisabledStyle;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::DocChListStylesGetContentStyle(TcxCustomTreeList * Sender,
  TcxTreeListColumn * AColumn, TcxTreeListNode * ANode, TcxStyle *& AStyle)
 {
  if (ANode)
   AStyle = (FDisDoc[ANode->AbsoluteIndex]) ? EnabledStyle : DisabledStyle;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::SetInProgress(bool AVal)
 {
  actSaveToDB->Caption = (AVal) ? "��������" : "�������";
  actSaveToDB->Enabled = AVal;
  FInProgress          = AVal;
  PrBar->Visible       = AVal;
  if (AVal)
   PrBar->Position = 0;
  else
   StatusBar->Panels->Items[1]->Text = "";
  SetEnabled(!AVal);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::FormDestroy(TObject * Sender)
 {
  if (FSpecDS)
   delete FSpecDS;
  if (FDocDS)
   delete FDocDS;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::FormShow(TObject * Sender)
 {
  actOpenExecute(actOpen);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::actSelectAllExecute(TObject * Sender)
 {
  if (SpecTS->TabVisible && ImportPC->ActivePage == SpecTS)
   {
    for (int i = 0; i < SpecChList->Root->Count; i++)
     SpecChList->Root->Items[i]->Values[0] = true;
   }
  else if (DocTS->TabVisible && ImportPC->ActivePage == DocTS)
   {
    for (int i = 0; i < DocChList->Root->Count; i++)
     DocChList->Root->Items[i]->Values[0] = true;
   }
  SpecChPropertiesEditValueChanged(NULL);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocImportDocForm::actUnselectExecute(TObject * Sender)
 {
  if (SpecTS->TabVisible && ImportPC->ActivePage == SpecTS)
   {
    for (int i = 0; i < SpecChList->Root->Count; i++)
     SpecChList->Root->Items[i]->Values[0] = false;
   }
  else if (DocTS->TabVisible && ImportPC->ActivePage == DocTS)
   {
    for (int i = 0; i < DocChList->Root->Count; i++)
     DocChList->Root->Items[i]->Values[0] = false;
   }
  SpecChPropertiesEditValueChanged(NULL);
 }
// ---------------------------------------------------------------------------
