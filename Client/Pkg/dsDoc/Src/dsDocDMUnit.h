//---------------------------------------------------------------------------
#ifndef dsDocDMUnitH
#define dsDocDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
//#include "ServerClientClasses.h"
#include "dsRegED.h"
#include "dsRegConstDef.h"
#include "dsDocTypeDef.h"
//---------------------------------------------------------------------------
//#include "AppOptions.h"
#include "ExtUtils.h"
#include "ICSNomenklator.h"
#include "Preview.h"
//#include "XMLContainer.h"
//#include "AppOptions.h"
//#include "ICSNomenklator.h"
//#include "KLADR.h"
//#include "icsDocExEventsDef.h"
//#include "DemoMessage.h"
#include "dsDocDocPeriodUnit.h"
#include "dsDocCustomGridUnit.h"
#include "dsDocNomSupportTypeDef.h"
#include "cxClasses.hpp"
#include "cxLookAndFeels.hpp"
//#include "cxGridTableView.hpp"
//namespace IcsReg
//{
//---------------------------------------------------------------------------
#define ctSpec   "0001"
#define ctDoc    "0002"
#define ctFlt    "0003"
#define ctSubOrg "0004"

//---------------------------------------------------------------------------
enum TExistsType { etOk=0, etExistByName=1, etExistByCode=2};
//---------------------------------------------------------------------------
enum TDocCreateType { dctNone = 0, dctEmpty = 1, dctBase = 2, dctDoc = 3, dctBaseDoc = 4};
//---------------------------------------------------------------------------
struct TDocEl
{
  UnicodeString SpecGUI, GUI, Owner, SOwner, Period, Code, /*DocXML, SpecXML, */FromDate, ToDate, SpecType;
  TDateTime CreateDate;
  __fastcall TDocEl():
  SpecGUI(""),
  GUI(""),
  Owner(""),
  SOwner(""),
  Period(""),
  Code(""),
/*  DocXML(""),
  SpecXML(""),*/
  FromDate(""),
  ToDate(""),
  SpecType(""),
  CreateDate(0)
  {};
  UnicodeString __fastcall ToString()
  {
    return ", SpecGUI: "+SpecGUI+
    "\n GUI: "+GUI+
    "\n Owner: "+Owner+
    "\n SOwner: "+SOwner+
    "\n Period: "+Period+
    "\n Code: "+Code+
/*    "\n DocXML: "+DocXML.SubString(1,200)+
    "\n SpecXML: "+SpecXML.SubString(1,200)+*/
    "\n FromDate: "+FromDate+
    "\n ToDate: "+ToDate+
    "\n SpecType: "+SpecType+
    "\n CreateDate: "+CreateDate.FormatString("dd.mm.yyyy");
  };
};
//---------------------------------------------------------------------------
class PACKAGE TdsDocDM : public TDataModule
{
__published:	// IDE-managed Components
        void __fastcall DataModuleCreate(TObject *Sender);
        void __fastcall DataModuleDestroy(TObject *Sender);

private:
  int           FDefaultClassWidth,FULEditWidth;
  bool          FFullEdit;
  bool          FClassEditable;
  UnicodeString FClassShowExpanded;
  TICSNomenklator *FNom;
  TICSNomenklator *FDocNom;

//  Variant      FICSNom;
//  Variant      FICSDocNom;

  UnicodeString         FMainOwnerCode;
  UnicodeString         FSubDir;
  void __fastcall FSetMainOwnerCode(UnicodeString AVal);

  TdxSkinName FSkinName;

//  TColor         FRequiredColor;
  UnicodeString  FXMLPath;
  UnicodeString FDataPath;

  UnicodeString FTablePrefix;
  UnicodeString FRegistryKey;
  TTagNode      *FRegDefNode;
  TTagNode      *FClassDefNode;
  TAxeXMLContainer *FXMLList;
  TDocCreateType FDefDocCreateType;

//  TGetCompValue         FOnGetCompValue;
//  TExtBtnGetBitmapEvent FOnGetExtBtnGetBitmap;
//  TRegGetFormatEvent    FOnGetFormat;
//  TExtBtnClick          FExtBtnClick;
  TExtValidateData      FOnGetDefValues;
  TExtValidateData      FOnExtValidate;
//  TExtBtnClick          FOnCtrlDataChange;

//  TExtEditData          FExtInsertData;
//  TExtEditData          FExtEditData;

//  TClassEditEvent       FOnClassAfterInsert;
//  TClassEditEvent       FOnClassAfterEdit;
//  TClassEditEvent       FOnClassAfterDelete;

//  TUnitEvent            FOnOpenCard;
//  TUnitEvent            FPrintReportClick;
  TcxEditStyleController *FStyleController;

  TdsGetClassXMLEvent FOnGetClassXML;
  TdsGetCountEvent    FOnGetCount;
  TdsGetIdListEvent   FOnGetIdList;
  TdsGetValByIdEvent  FOnGetValById;
  TdsGetValByIdEvent  FOnMainAppGetValById;
  TdsDocSaveDocEvent  FOnSaveDoc;

  TdsGetValById10Event  FOnGetValById10;
  TdsFindEvent        FOnFind;

  TdsDocImportEvent   FOnImportData;
  TdsInsertDataEvent  FOnInsertData;
  TdsEditDataEvent    FOnEditData;
  TdsDeleteDataEvent  FOnDeleteData;
  TdsDocSetTextDataEvent FOnSetText;
  TdsDocGetTextDataEvent FOnGetText;

  TdsGetClassXMLEvent FOnGetClassZIPXML;
  TdsDocGetSpec       FOnGetSpec;
  TdsDocGetDocPreview FOnGetDocPreview;
  TdsDocLoadDocPreview FOnLoadDocPreview;

  TdsDocGetDocById    FOnGetDocById;
  TdsDocCheckByPeriod FOnCheckByPeriod;
  TdsDocDocListClearByDate FOnDocListClearByDate;
  TdsDocGetDocCountBySpec  FOnGetDocCountBySpec;
  TdsDocIsMainOwnerCode    FOnIsMainOwnerCode;
  TdsDocCopySpec           FOnCopySpec;
  TdsDocCheckSpecExists    FOnCheckSpecExists;
  TdsDocCheckDocCreate     FOnCheckDocCreate;
  TdsCheckExistDocCreate   FOnCheckExistDocCreate;
  TdsDocDocCreate          FOnDocCreate;
  TdsDocStartDocCreate     FOnStartDocCreate;
  TdsDocDocCreateProgress  FOnDocCreateProgress;
  TdsGetValByIdEvent       FOnGetQList;

  TdsDocCanHandEdit            FOnCanHandEdit;
  TdsDocCreateCheckFilterEvent FOnDocCreateCheckFilter;
  TdsNomCallDefProcEvent   FOnCallDefProc;
  TdsNomCallDefProcEvent   FOnCallDocDefProc;
  UnicodeString            FExternalView;


  bool FSpecEnabled;
  bool FSpecEditable;

  bool FDocEnabled;
  bool FDocEditable;
  bool FDocHandEditable;

  bool FFilterEnabled;
  bool FFilterEditable;

  bool FIMM_DEBUG;
  bool FUseKLADRAddr;

  TColor  FReqColor;

//  TdsDocClassClient  *FdsDocClient;
//  TAppOptions        *FAppOpt;
  UnicodeString      FBaseISRef;
  TTagNode*     FNmkl;
  TTagNode*     FDocNmkl;

//  TTagNode* FICS_DATA_DEF;
  TTagNode* __fastcall FGetICS_DATA_DEF();

  TICSNomenklator* __fastcall FGetNom();

//  Variant         __fastcall FGetICSNom();
//  void             __fastcall FSetICSNom(Variant AVal);

  TICSNomenklator* __fastcall FGetDocNom();

//  Variant         __fastcall FGetICSDocNom();
//  void             __fastcall FSetICSDocNom(Variant AVal);

  //    TClassHashTable *FClassHash;
  TTagNode* __fastcall FGetClassDef();
//  TTagNode* __fastcall FGetRegDefNode();
  UnicodeString __fastcall JSONStrVal(TJSONObject *AData, UnicodeString AFlName);
  UnicodeString __fastcall JSONObjectVal(TJSONObject *AData, UnicodeString AFlName);

  void __fastcall GetQList(System::UnicodeString AId, TStringList *AList);
  void __fastcall SetQList(System::UnicodeString AId, TJSONObject* AList);
  UnicodeString __fastcall GetTempPath() const;

//  UnicodeString __fastcall FCreateDocument(UnicodeString ASpecGUI, int ACreateType, UnicodeString AFilter, bool ADocNom, UnicodeString AAuthor = "", int ADefCreateType = 0, bool ACheckDefDate = true);

public:
//  TICSClassData *FClassData;
  UnicodeString   DTFormats[3];
  TObject      *UnitList;
  TObject      *UnitCard;
  TTagNode     *SelRegUnit;
  UnicodeString    StyleDef;

  int CurDocCreateMonth, CurDocCreateYear;

  UnicodeString __fastcall NewGUID();

  __fastcall TdsDocDM(TComponent* Owner);

  bool __fastcall GetSpec(UnicodeString AGUI, TTagNode *ASpec, int &ASpecType, UnicodeString AMsg = "");
  UnicodeString __fastcall GetSpecTxt(UnicodeString AGUI);
  UnicodeString __fastcall GetOwnerName(UnicodeString AOwnerCode, UnicodeString AFieldName = "NAME");
  void __fastcall CheckPackage(TICSCheckPackItem ACheckItemEvt, TTagNode *APackDef);
  TDate __fastcall GetLastMonthDate(int AMons, int AYear);
  TDate __fastcall GetFirstMonthDate(int AMons, int AYear);
  UnicodeString __fastcall GetThisSpecOwner();

  bool __fastcall FillClass(TStrings *AValues, UnicodeString ARef, UnicodeString ASQL);
  void __fastcall GetOwnerList(TStringList *AChildList);
  void __fastcall GetChildSpecOwnerList(TStringList *AChildList, bool AIsName);
  void __fastcall GetIntegredSpecList(TStringList *AList);
  bool __fastcall GetSpecByGUI(UnicodeString AGUI,TTagNode *ASpec);

  bool __fastcall CreatePackage(TTagNode *ASpec, UnicodeString AFilter, bool ACheckDefDate = true);
  UnicodeString __fastcall StartCreateDocument(TTagNode *ASpec, int ACreateType, UnicodeString AFilter, bool ADocNom, UnicodeString AAuthor = "", int ADefCreateType = 0, bool ACheckDefDate = true, bool ANoSave = false);
  UnicodeString __fastcall CreateDocumentProgress(UnicodeString &ADocGUI);

  UnicodeString __fastcall InternalCreateDocument(TkabProgress AProgressProc, TTagNode *ASpec, int ACreateType,
                                                  UnicodeString AFilter, bool ADocNom, UnicodeString AAuthor = "",
                                                  int ADefCreateType = 0, bool ACheckDefDate = true, bool ANoSave = false);

  void __fastcall HTMLPreview(UnicodeString AHTML);
  void __fastcall PreviewDocument(UnicodeString AGUI, TkabProgress AProgressProc);
  void __fastcall SaveDocument(UnicodeString ADocGUI);
  bool __fastcall CanIntegrated(TTagNode *ASpec);

  __property TAxeXMLContainer *XMLList    = {read=FXMLList, write=FXMLList};
//  __property TAppOptions      *AppOpt     = {read=FAppOpt, write=FAppOpt};

  __property bool SpecEnabled             = {read=FSpecEnabled, write=FSpecEnabled};
  __property bool SpecEditable            = {read=FSpecEditable, write=FSpecEditable};

  __property bool DocEnabled              = {read=FDocEnabled, write=FDocEnabled};
  __property bool DocEditable             = {read=FDocEditable, write=FDocEditable};
  __property bool DocHandEditable         = {read=FDocHandEditable, write=FDocHandEditable};

  __property bool FilterEnabled           = {read=FFilterEnabled, write=FFilterEnabled};
  __property bool FilterEditable          = {read=FFilterEditable, write=FFilterEditable};

//  __property bool SpecEditable        = {read=FSpecEditable, write=FSpecEditable};
  __property bool IMM_DEBUG               = {read=FIMM_DEBUG, write=FIMM_DEBUG};
  __property UnicodeString  MainOwnerCode = {read=FMainOwnerCode, write=FSetMainOwnerCode};
  __property UnicodeString SubDir         = {read=FSubDir, write=FSubDir};
  __property TTagNode* ICS_DATA_DEF       = {read=FGetICS_DATA_DEF};
  void __fastcall DocViewDemoMessage(TPPreviewDemoMessage AMsgType);




  __property TDocCreateType DefDocCreateType = {read=FDefDocCreateType, write=FDefDocCreateType};


  void __fastcall OnGetXML(UnicodeString AId, TTagNode *& ADef);
  void __fastcall SetTextData(System::UnicodeString ATabId, System::UnicodeString AFlId, System::UnicodeString ARecId, System::UnicodeString AData);
  UnicodeString __fastcall GetTextData(System::UnicodeString ATabId, System::UnicodeString AFlId, System::UnicodeString ARecId);
  __property TColor  ReqColor = {read=FReqColor, write=FReqColor};

  void __fastcall FillLPUData(TStrings *ADest, __int64 ACode);
  void __fastcall GetLPUData(__int64 ACode, TTagNode *ADest);

  __int64       __fastcall kabCDSGetCount(UnicodeString AId, UnicodeString ATTH, UnicodeString AFilterParam);
  TJSONObject*  __fastcall kabCDSGetCodes(UnicodeString AId, int AMax, UnicodeString ATTH, UnicodeString AFilterParam);
  TJSONObject*  __fastcall kabCDSMainAppGetValById(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  __fastcall kabCDSGetValById(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  __fastcall kabCDSGetValById10(UnicodeString AId, UnicodeString ARecId);
  UnicodeString __fastcall kabCDSImport(UnicodeString AId, TJSONObject* AValues, UnicodeString &ARecId);
  UnicodeString __fastcall kabCDSInsert(UnicodeString AId, TJSONObject* AValues, UnicodeString &ARecId);
  UnicodeString __fastcall kabCDSEdit(UnicodeString AId, TJSONObject* AValues, UnicodeString &ARecId);
  UnicodeString __fastcall kabCDSDelete(UnicodeString AId, UnicodeString ARecId);
  UnicodeString __fastcall kabCDSGetClassXML(UnicodeString AId);
  UnicodeString __fastcall kabCDSGetClassZIPXML(UnicodeString AId);
  UnicodeString __fastcall kabCDSCheckByPeriod(bool FCreateByType,UnicodeString ACode, UnicodeString ASpecGUI);
  bool          __fastcall kabCDSDocListClearByDate(TDate ADate);
  int           __fastcall GetDocCountBySpec(UnicodeString ASpecOWNER, UnicodeString ASpecGUI, UnicodeString &ARCMsg);
  bool          __fastcall IsMainOwnerCode(UnicodeString ASpecOwner);
  UnicodeString __fastcall CopySpec(UnicodeString AGUI, UnicodeString &ANewGUI);
  TExistsType   __fastcall CheckSpecExists(UnicodeString AName, UnicodeString &AGUI, bool AIsEdit);
  TExistsType   __fastcall CheckDocExists(UnicodeString AAuthor, UnicodeString ASpecGUI, int APeriodType, UnicodeString APeriodVal);
  UnicodeString __fastcall GetDocById(UnicodeString AId);
  UnicodeString __fastcall DocCreateCheckFilter(UnicodeString ASpecGUI, UnicodeString AFilter, UnicodeString &AFilterName, UnicodeString &AFilterGUI);
  bool          __fastcall CanHandEdit(System::UnicodeString ASpecGUID);

  void __fastcall GetQuickList(UnicodeString AId, TStringList *AList);
//  void __fastcall GetQFilterList(TStringList *AList);

  __property TICSNomenklator*  Nom       = {read=FGetNom};
//  __property Variant          ICSNom    = {read=FGetICSNom, write=FSetICSNom};
  __property TICSNomenklator*  DocNom    = {read=FGetDocNom};
//  __property Variant          ICSDocNom = {read=FGetICSDocNom, write=FSetICSDocNom};
  __property UnicodeString BaseISRef = {read=FBaseISRef, write=FBaseISRef};
  __property TTagNode*     Nmkl = {read=FNmkl};
  __property TTagNode*     DocNmkl = {read=FDocNmkl};

  __property int DefaultClassWidth = {read=FDefaultClassWidth, write=FDefaultClassWidth};
  __property int ULEditWidth = {read=FULEditWidth, write=FULEditWidth};

  __property bool FullEdit = {read=FFullEdit, write=FFullEdit};
  __property UnicodeString TablePrefix = {read=FTablePrefix, write=FTablePrefix};
  __property UnicodeString RegistryKey = {read=FRegistryKey, write=FRegistryKey};
  __property TTagNode* ClassDef = {read=FGetClassDef};
  __property TTagNode* RegDef = {read=FRegDefNode, write = FRegDefNode};

//  __property TdsDocClassClient  *Connection = {read=FdsDocClient, write=FdsDocClient};

  __property bool ClassEditable = {read=FClassEditable, write=FClassEditable};
  __property UnicodeString ClassShowExpanded = {read=FClassShowExpanded, write=FClassShowExpanded};

  __property TcxEditStyleController *StyleController = {read=FStyleController,write=FStyleController};
  __property TdxSkinName SkinName                    = {read=FSkinName,write=FSkinName};
//  __property TColor RequiredColor                    = {read=FRequiredColor,  write=FRequiredColor};

  __property TdsGetClassXMLEvent OnGetClassXML             = {read=FOnGetClassXML,write=FOnGetClassXML};
  __property TdsGetClassXMLEvent OnGetClassZIPXML          = {read=FOnGetClassZIPXML,write=FOnGetClassZIPXML};
  __property TdsGetCountEvent OnGetCount                   = {read=FOnGetCount,write=FOnGetCount};
  __property TdsGetIdListEvent OnGetIdList                 = {read=FOnGetIdList,write=FOnGetIdList};
  __property TdsGetValByIdEvent OnGetValById               = {read=FOnGetValById,write=FOnGetValById};
  __property TdsGetValByIdEvent OnMainAppGetValById        = {read=FOnMainAppGetValById,write=FOnMainAppGetValById};

  __property TdsGetValById10Event OnGetValById10           = {read=FOnGetValById10,write=FOnGetValById10};
  __property TdsFindEvent OnFind                           = {read=FOnFind,write=FOnFind};

  __property TdsDocImportEvent OnImportData               = {read=FOnImportData,write=FOnImportData};
  __property TdsInsertDataEvent OnInsertData               = {read=FOnInsertData,write=FOnInsertData};
  __property TdsEditDataEvent OnEditData                   = {read=FOnEditData,write=FOnEditData};
  __property TdsDeleteDataEvent OnDeleteData               = {read=FOnDeleteData,write=FOnDeleteData};
  __property TdsDocGetSpec OnGetSpec                       = {read=FOnGetSpec,write=FOnGetSpec};
  __property TdsDocGetDocById    OnGetDocById              = {read=FOnGetDocById,write=FOnGetDocById};
  __property TdsDocGetDocPreview OnGetDocPreview           = {read=FOnGetDocPreview,write=FOnGetDocPreview};
  __property TdsDocLoadDocPreview OnLoadDocPreview           = {read=FOnLoadDocPreview,write=FOnLoadDocPreview};

  __property TdsDocCheckByPeriod OnCheckByPeriod           = {read=FOnCheckByPeriod,write=FOnCheckByPeriod};
  __property TdsDocDocListClearByDate OnDocListClearByDate = {read=FOnDocListClearByDate,write=FOnDocListClearByDate};
  __property TdsDocGetDocCountBySpec OnGetDocCountBySpec   = {read=FOnGetDocCountBySpec,write=FOnGetDocCountBySpec};
  __property TdsDocIsMainOwnerCode OnIsMainOwnerCode       = {read=FOnIsMainOwnerCode,write=FOnIsMainOwnerCode};
  __property TdsDocCopySpec OnCopySpec                     = {read=FOnCopySpec,write=FOnCopySpec};
  __property TdsDocCheckSpecExists OnCheckSpecExists       = {read=FOnCheckSpecExists,write=FOnCheckSpecExists};
  __property TdsDocSetTextDataEvent OnSetText              = {read=FOnSetText,write=FOnSetText};
  __property TdsDocGetTextDataEvent OnGetText              = {read=FOnGetText,write=FOnGetText};
  __property TdsGetValByIdEvent OnGetQList                 = {read=FOnGetQList, write=FOnGetQList};

  __property TdsDocCheckDocCreate     OnCheckDocCreate     = {read=FOnCheckDocCreate,write=FOnCheckDocCreate};
  __property TdsCheckExistDocCreate OnCheckExistDocCreate  = {read=FOnCheckExistDocCreate,write=FOnCheckExistDocCreate};
  __property TdsDocDocCreate        OnDocCreate            = {read=FOnDocCreate,write=FOnDocCreate};

  __property TdsDocStartDocCreate     OnStartDocCreate     = {read=FOnStartDocCreate,write=FOnStartDocCreate};
  __property TdsDocDocCreateProgress  OnDocCreateProgress  = {read=FOnDocCreateProgress,write=FOnDocCreateProgress};

  __property TdsDocCreateCheckFilterEvent OnDocCreateCheckFilter = {read=FOnDocCreateCheckFilter,write=FOnDocCreateCheckFilter};
  __property bool UseKLADRAddr                             = {read=FUseKLADRAddr};

  __property TdsNomCallDefProcEvent OnCallDefProc          = { read = FOnCallDefProc, write = FOnCallDefProc};
  __property TdsNomCallDefProcEvent OnCallDocDefProc       = { read = FOnCallDocDefProc, write = FOnCallDocDefProc};


  __property TdsDocSaveDocEvent  OnSaveDoc                 = { read = FOnSaveDoc, write = FOnSaveDoc};
  __property UnicodeString       XMLPath = {read=FXMLPath, write=FXMLPath};
  __property UnicodeString DataPath = {read=FDataPath, write=FDataPath};
  __property TExtValidateData OnExtValidate = {read=FOnExtValidate,write=FOnExtValidate};
  __property TExtValidateData OnGetDefValues = {read=FOnGetDefValues,write=FOnGetDefValues};
  __property TdsDocCanHandEdit OnCanHandEdit = {read=FOnCanHandEdit,write=FOnCanHandEdit};
  __property UnicodeString ExternalView = {read=FExternalView, write=FExternalView};

};
//}; // end of namespace DataExchange
//using namespace IcsReg;
//---------------------------------------------------------------------------
extern PACKAGE TdsDocDM *dsDocDM;
//---------------------------------------------------------------------------
#endif

