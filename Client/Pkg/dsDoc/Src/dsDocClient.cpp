// ---------------------------------------------------------------------------
#include <basepch.h>
#pragma hdrstop
#include "dsDocClient.h"
#include "dsDocDMUnit.h"
#include "dsDocImportDocUnit.h"
#include "dsDocExportDocUnit.h"
#include "dsDocDocListUnit.h"
#include "dsDocFilterListUnit.h"
#include "dsDocSpecListUnit.h"
#include "dsDocSubOrgListUnit.h"
#pragma package(smart_init)
// ---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//
static inline void ValidCtrCheck(TdsDocClient *)
 {
  new TdsDocClient(NULL);
 }
// ---------------------------------------------------------------------------
__fastcall TdsDocClient::TdsDocClient(TComponent * Owner) : TComponent(Owner)
 {
  FUseClassDisControls = false;
  FDBUser              = "";
  FDBPassword          = "";
  FDBCharset           = "";
  FOnCtrlDataChange    = NULL;
  FRefreshList         = false;
  FClassTreeColor      = clBtnFace;
  FDTFormat            = "dd.mm.yyyy";
  FDM                  = new TdsDocDM(this);
  FActive              = true;
 }
// ---------------------------------------------------------------------------
__fastcall TdsDocClient::~TdsDocClient()
 {
  for (TICDocMap::iterator i = FDocMap.begin(); i != FDocMap.end(); i++)
   delete i->second;
  FDocMap.clear();
  Active = false;
  delete FDM;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetFullEdit(bool AFullEdit)
 {
  FFullEdit = AFullEdit;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetActive(bool AActive)
 {
  FActive = AActive;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetUListTop(int ATop)
 {
  FUListTop = ATop;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetUListLeft(int ALeft)
 {
  FUListLeft = ALeft;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetDBName(UnicodeString ADBName)
 {
  FDBName = ADBName;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::ImportDoc()
 {
  if (!FActive)
   return;
  TICDocMap::iterator FRC = FDocMap.find("import");
  if (FRC == FDocMap.end())
   {
    FDocMap["import"] = new TdsDocImportDocForm(Application->Owner, FDM);
   }
  FDocMap["import"]->Show();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::ExportDoc()
 {
  if (!FActive)
   return;
  TICDocMap::iterator FRC = FDocMap.find("export");
  if (FRC == FDocMap.end())
   {
    FDocMap["export"] = new TdsDocExportDocForm(Application->Owner, FDM);
   }
  FDocMap["export"]->Show();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::DocList(bool ADocMode)
 {
  if (FActive && FDM)
   {
    if (FDM->DocEnabled)
     {
      TICDocMap::iterator FRC = FDocMap.find("doclist");
      if (FRC == FDocMap.end())
       FDocMap["doclist"] = new TdsDocListForm(Application->Owner, FDM, true);
      FDocMap["doclist"]->Show();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FilterList()
 {
  if (FActive && FDM)
   {
    if (FDM->FilterEnabled)
     {
      TICDocMap::iterator FRC = FDocMap.find("filterlist");
      if (FRC == FDocMap.end())
       FDocMap["filterlist"] = new TdsDocFilterListForm(Application->Owner, "���������� ��������", 0, FDM);
      FDocMap["filterlist"]->Show();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::SubOrgList()
 {
  if (!FActive)
   return;
  TICDocMap::iterator FRC = FDocMap.find("suborglist");
  if (FRC == FDocMap.end())
   {
    FDocMap["suborglist"] = new TdsDocSubOrgForm(Application->Owner, FDM, false);
   }
  FDocMap["suborglist"]->Show();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::SpecList()
 {
  if (FActive && FDM)
   {
    if (FDM->SpecEnabled)
     {
      TICDocMap::iterator FRC = FDocMap.find("speclist");
      if (FRC == FDocMap.end())
       FDocMap["speclist"] = new TdsDocSpecListForm(Application->Owner, FDM);
      FDocMap["speclist"]->Show();
     }
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocClient::Connect(UnicodeString ADBName, UnicodeString AXMLName)
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetInit(int APersent, UnicodeString AMess)
 {
  /*
   if (APersent == 0) ((TICSRegProgressForm*)PrF)->SetCaption(AMess);
   ((TICSRegProgressForm*)PrF)->SetPercent(APersent);
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetDTFormat(UnicodeString AParam)
 {
  FDTFormat = AParam;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocClient::FGetClassEditable()
 {
  bool RC = false;
  try
   {
    if (FDM)
     RC = FDM->ClassEditable;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetClassEditable(bool AVal)
 {
  if (FDM)
   FDM->ClassEditable = AVal;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocClient::FGetClassShowExpanded()
 {
  UnicodeString RC = "";
  try
   {
    if (FDM)
     RC = FDM->ClassShowExpanded;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetClassShowExpanded(UnicodeString AVal)
 {
  if (FDM)
   FDM->ClassShowExpanded = AVal;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsDocClient::FGetRegDef()
 {
  if (FDM)
   {
    return FDM->RegDef;
   }
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetRegDef(TTagNode * AValue)
 {
  if (FDM)
   FDM->RegDef = AValue;
 }
// ---------------------------------------------------------------------------
TExtBtnClick __fastcall TdsDocClient::FGetExtBtnClick()
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetExtBtnClick(TExtBtnClick AValue)
 {
 }
// ---------------------------------------------------------------------------
TcxEditStyleController * __fastcall TdsDocClient::FGetStyleController()
 {
  if (FDM)
   return FDM->StyleController;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetStyleController(TcxEditStyleController * AValue)
 {
  if (FDM)
   FDM->StyleController = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetClassXMLEvent __fastcall TdsDocClient::FGetOnGetClassXML()
 {
  if (FDM)
   return FDM->OnGetClassXML;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnGetClassXML(TdsGetClassXMLEvent AValue)
 {
  if (FDM)
   FDM->OnGetClassXML = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetClassXMLEvent __fastcall TdsDocClient::FGetOnGetClassZIPXML()
 {
  if (FDM)
   return FDM->OnGetClassZIPXML;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnGetClassZIPXML(TdsGetClassXMLEvent AValue)
 {
  if (FDM)
   FDM->OnGetClassZIPXML = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetCountEvent __fastcall TdsDocClient::FGetOnGetCount()
 {
  if (FDM)
   return FDM->OnGetCount;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnGetCount(TdsGetCountEvent AValue)
 {
  if (FDM)
   FDM->OnGetCount = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetIdListEvent __fastcall TdsDocClient::FGetOnGetIdList()
 {
  if (FDM)
   return FDM->OnGetIdList;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnGetIdList(TdsGetIdListEvent AValue)
 {
  if (FDM)
   FDM->OnGetIdList = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetValByIdEvent __fastcall TdsDocClient::FGetOnGetValById()
 {
  if (FDM)
   return FDM->OnGetValById;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnGetValById(TdsGetValByIdEvent AValue)
 {
  if (FDM)
   FDM->OnGetValById = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetValById10Event __fastcall TdsDocClient::FGetOnGetValById10()
 {
  if (FDM)
   return FDM->OnGetValById10;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnGetValById10(TdsGetValById10Event AValue)
 {
  if (FDM)
   FDM->OnGetValById10 = AValue;
 }
// ---------------------------------------------------------------------------
TdsFindEvent __fastcall TdsDocClient::FGetOnFind()
 {
  if (FDM)
   return FDM->OnFind;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnFind(TdsFindEvent AValue)
 {
  if (FDM)
   FDM->OnFind = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocImportEvent __fastcall TdsDocClient::FGetOnImportData()
 {
  if (FDM)
   return FDM->OnImportData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnImportData(TdsDocImportEvent AValue)
 {
  if (FDM)
   FDM->OnImportData = AValue;
 }
// ---------------------------------------------------------------------------
TdsInsertDataEvent __fastcall TdsDocClient::FGetOnInsertData()
 {
  if (FDM)
   return FDM->OnInsertData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnInsertData(TdsInsertDataEvent AValue)
 {
  if (FDM)
   FDM->OnInsertData = AValue;
 }
// ---------------------------------------------------------------------------
TdsEditDataEvent __fastcall TdsDocClient::FGetOnEditData()
 {
  if (FDM)
   return FDM->OnEditData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnEditData(TdsEditDataEvent AValue)
 {
  if (FDM)
   FDM->OnEditData = AValue;
 }
// ---------------------------------------------------------------------------
TdsDeleteDataEvent __fastcall TdsDocClient::FGetOnDeleteData()
 {
  if (FDM)
   return FDM->OnDeleteData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnDeleteData(TdsDeleteDataEvent AValue)
 {
  if (FDM)
   FDM->OnDeleteData = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocGetSpec __fastcall TdsDocClient::FGetOnGetSpec()
 {
  if (FDM)
   return FDM->OnGetSpec;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnGetSpec(TdsDocGetSpec AValue)
 {
  if (FDM)
   FDM->OnGetSpec = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocGetDocById __fastcall TdsDocClient::FGetOnGetDocById()
 {
  if (FDM)
   return FDM->OnGetDocById;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnGetDocById(TdsDocGetDocById AValue)
 {
  if (FDM)
   FDM->OnGetDocById = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocGetDocPreview __fastcall TdsDocClient::FGetOnGetDocPreview()
 {
  if (FDM)
   return FDM->OnGetDocPreview;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnGetDocPreview(TdsDocGetDocPreview AValue)
 {
  if (FDM)
   FDM->OnGetDocPreview = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocLoadDocPreview __fastcall TdsDocClient::FGetOnLoadDocPreview()
 {
  if (FDM)
   return FDM->OnLoadDocPreview;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnLoadDocPreview(TdsDocLoadDocPreview AValue)
 {
  if (FDM)
   FDM->OnLoadDocPreview = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocCheckByPeriod __fastcall TdsDocClient::FGetOnCheckByPeriod()
 {
  if (FDM)
   return FDM->OnCheckByPeriod;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnCheckByPeriod(TdsDocCheckByPeriod AValue)
 {
  if (FDM)
   FDM->OnCheckByPeriod = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocDocListClearByDate __fastcall TdsDocClient::FGetOnDocListClearByDate()
 {
  if (FDM)
   return FDM->OnDocListClearByDate;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnDocListClearByDate(TdsDocDocListClearByDate AValue)
 {
  if (FDM)
   FDM->OnDocListClearByDate = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocGetDocCountBySpec __fastcall TdsDocClient::FGetOnGetDocCountBySpec()
 {
  if (FDM)
   return FDM->OnGetDocCountBySpec;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnGetDocCountBySpec(TdsDocGetDocCountBySpec AValue)
 {
  if (FDM)
   FDM->OnGetDocCountBySpec = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocIsMainOwnerCode __fastcall TdsDocClient::FGetOnIsMainOwnerCode()
 {
  if (FDM)
   return FDM->OnIsMainOwnerCode;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnIsMainOwnerCode(TdsDocIsMainOwnerCode AValue)
 {
  if (FDM)
   FDM->OnIsMainOwnerCode = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocCopySpec __fastcall TdsDocClient::FGetOnCopySpec()
 {
  if (FDM)
   return FDM->OnCopySpec;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnCopySpec(TdsDocCopySpec AValue)
 {
  if (FDM)
   FDM->OnCopySpec = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocCheckSpecExists __fastcall TdsDocClient::FGetOnCheckSpecExists()
 {
  if (FDM)
   return FDM->OnCheckSpecExists;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnCheckSpecExists(TdsDocCheckSpecExists AValue)
 {
  if (FDM)
   FDM->OnCheckSpecExists = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocSetTextDataEvent __fastcall TdsDocClient::FGetOnSetText()
 {
  if (FDM)
   return FDM->OnSetText;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnSetText(TdsDocSetTextDataEvent AValue)
 {
  if (FDM)
   FDM->OnSetText = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocGetTextDataEvent __fastcall TdsDocClient::FGetOnGetText()
 {
  if (FDM)
   return FDM->OnGetText;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnGetText(TdsDocGetTextDataEvent AValue)
 {
  if (FDM)
   FDM->OnGetText = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocCheckDocCreate __fastcall TdsDocClient::FGetOnCheckDocCreate()
 {
  if (FDM)
   return FDM->OnCheckDocCreate;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnCheckDocCreate(TdsDocCheckDocCreate AValue)
 {
  if (FDM)
   FDM->OnCheckDocCreate = AValue;
 }
// ---------------------------------------------------------------------------
TdsCheckExistDocCreate __fastcall TdsDocClient::FGetOnCheckExistDocCreate()
 {
  if (FDM)
   return FDM->OnCheckExistDocCreate;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnCheckExistDocCreate(TdsCheckExistDocCreate AValue)
 {
  if (FDM)
   FDM->OnCheckExistDocCreate = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocDocCreate __fastcall TdsDocClient::FGetOnDocCreate()
 {
  if (FDM)
   return FDM->OnDocCreate;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnDocCreate(TdsDocDocCreate AValue)
 {
  if (FDM)
   FDM->OnDocCreate = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocStartDocCreate __fastcall TdsDocClient::FGetOnStartDocCreate()
 {
  if (FDM)
   return FDM->OnStartDocCreate;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnStartDocCreate(TdsDocStartDocCreate AValue)
 {
  if (FDM)
   FDM->OnStartDocCreate = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocDocCreateProgress __fastcall TdsDocClient::FGetOnDocCreateProgress()
 {
  if (FDM)
   return FDM->OnDocCreateProgress;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnDocCreateProgress(TdsDocDocCreateProgress AValue)
 {
  if (FDM)
   FDM->OnDocCreateProgress = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocCreateCheckFilterEvent __fastcall TdsDocClient::FGetOnDocCreateCheckFilter()
 {
  if (FDM)
   return FDM->OnDocCreateCheckFilter;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnDocCreateCheckFilter(TdsDocCreateCheckFilterEvent AValue)
 {
  if (FDM)
   FDM->OnDocCreateCheckFilter = AValue;
 }
// ---------------------------------------------------------------------------
TdsNomCallDefProcEvent __fastcall TdsDocClient::FGetOnCallDefProc()
 {
  if (FDM)
   return FDM->OnCallDefProc;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnCallDefProc(TdsNomCallDefProcEvent AValue)
 {
  if (FDM)
   FDM->OnCallDefProc = AValue;
 }
// ---------------------------------------------------------------------------
TdsNomCallDefProcEvent __fastcall TdsDocClient::FGetOnCallDocDefProc()
 {
  if (FDM)
   return FDM->OnCallDocDefProc;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnCallDocDefProc(TdsNomCallDefProcEvent AValue)
 {
  if (FDM)
   FDM->OnCallDocDefProc = AValue;
 }
// ---------------------------------------------------------------------------
TAxeXMLContainer * __fastcall TdsDocClient::FGetXMLList()
 {
  if (FDM)
   return FDM->XMLList;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetXMLList(TAxeXMLContainer * AVal)
 {
  if (FDM)
   FDM->XMLList = AVal;
 }
// ---------------------------------------------------------------------------
TICSNomenklator * __fastcall TdsDocClient::FGetNom()
 {
  if (FDM)
   return FDM->Nom;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
TICSNomenklator * __fastcall TdsDocClient::FGetDocNom()
 {
  if (FDM)
   return FDM->DocNom;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsDocClient::FGetNmkl()
 {
  if (FDM)
   return FDM->Nmkl;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsDocClient::FGetDocNmkl()
 {
  if (FDM)
   return FDM->DocNmkl;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocClient::FGetMainOwnerCode()
 {
  if (FDM)
   return FDM->MainOwnerCode;
  else
   return "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetMainOwnerCode(UnicodeString AVal)
 {
  if (FDM)
   FDM->MainOwnerCode = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::HTMLPreview(UnicodeString AHTML)
 {
  if (FDM)
   FDM->HTMLPreview(AHTML);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::PreviewDocument(UnicodeString AGUI, TkabProgress AProgressProc)
 {
  if (FDM)
   FDM->PreviewDocument(AGUI, AProgressProc);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocClient::FGetSubDir()
 {
  if (FDM)
   return FDM->SubDir;
  else
   return "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetSubDir(UnicodeString AValue)
 {
  if (FDM)
   FDM->SubDir = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetValByIdEvent __fastcall TdsDocClient::FGetOnMainAppGetValById()
 {
  if (FDM)
   return FDM->OnMainAppGetValById;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnMainAppGetValById(TdsGetValByIdEvent AValue)
 {
  if (FDM)
   FDM->OnMainAppGetValById = AValue;
 }
// ---------------------------------------------------------------------------
TdsDocSaveDocEvent __fastcall TdsDocClient::FGetOnSaveDoc()
 {
  if (FDM)
   return FDM->OnSaveDoc;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnSaveDoc(TdsDocSaveDocEvent AValue)
 {
  if (FDM)
   FDM->OnSaveDoc = AValue;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::GetQuickList(UnicodeString AId, TStringList * AList)
 {
  FDM->GetQuickList(AId, AList);
 }
// ---------------------------------------------------------------------------
TdsGetValByIdEvent __fastcall TdsDocClient::FGetOnGetQList()
 {
  if (FDM)
   return FDM->OnGetQList;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnGetQList(TdsGetValByIdEvent AVal)
 {
  if (FDM)
   FDM->OnGetQList = AVal;
 }
// ---------------------------------------------------------------------------
TdsDocCanHandEdit __fastcall TdsDocClient::FGetOnCanHandEdit()
 {
  if (FDM)
   return FDM->OnCanHandEdit;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnCanHandEdit(TdsDocCanHandEdit AVal)
 {
  if (FDM)
   FDM->OnCanHandEdit = AVal;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocClient::CreateDocument(TkabProgress AProgressProc, UnicodeString ASpecGUI,
  UnicodeString AFilter)
 {
  UnicodeString RC = "";
  if (FDM)
   {
    TTagNode * FSpec = new TTagNode(NULL);
    int FSpecType;
    try
     {
      if (FDM->GetSpec(ASpecGUI, FSpec, FSpecType, "������ �������� �������� ���������."))
       RC = FDM->InternalCreateDocument(AProgressProc, FSpec, 1, AFilter, false, FDM->GetThisSpecOwner());
     }
    __finally
     {
      delete FSpec;
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocClient::FGetXMLPath()
 {
  if (FDM)
   return FDM->XMLPath;
  else
   return "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetXMLPath(UnicodeString AVal)
 {
  if (FDM)
   FDM->XMLPath = AVal;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocClient::FGetDataPath()
 {
  if (FDM)
   return FDM->DataPath;
  else
   return "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetDataPath(UnicodeString AVal)
 {
  if (FDM)
   FDM->DataPath = AVal;
 }
// ---------------------------------------------------------------------------
TColor __fastcall TdsDocClient::FGetReqColor()
 {
  if (FDM)
   return FDM->ReqColor;
  else
   return clInfoBk;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetReqColor(TColor AVal)
 {
  if (FDM)
   FDM->ReqColor = AVal;
 }
// ---------------------------------------------------------------------------
TExtValidateData __fastcall TdsDocClient::FGetOnExtValidate()
 {
  if (FDM)
   return FDM->OnExtValidate;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnExtValidate(TExtValidateData AVal)
 {
  if (FDM)
   FDM->OnExtValidate = AVal;
 }
// ---------------------------------------------------------------------------
TExtValidateData __fastcall TdsDocClient::FGetOnGetDefValues()
 {
  if (FDM)
   return FDM->OnGetDefValues;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetOnGetDefValues(TExtValidateData AVal)
 {
  if (FDM)
   FDM->OnGetDefValues = AVal;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocClient::FGetSpecEnabled()
 {
  if (FDM)
   return FDM->SpecEnabled;
  else
   return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetSpecEnabled(bool AVal)
 {
  if (FDM)
   FDM->SpecEnabled = AVal;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocClient::FGetSpecEditable()
 {
  if (FDM)
   return FDM->SpecEditable;
  else
   return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetSpecEditable(bool AVal)
 {
  if (FDM)
   FDM->SpecEditable = AVal;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocClient::FGetDocEnabled()
 {
  if (FDM)
   return FDM->DocEnabled;
  else
   return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetDocEnabled(bool AVal)
 {
  if (FDM)
   FDM->DocEnabled = AVal;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocClient::FGetDocEditable()
 {
  if (FDM)
   return FDM->DocEditable;
  else
   return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetDocEditable(bool AVal)
 {
  if (FDM)
   FDM->DocEditable = AVal;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocClient::FGetDocHandEditable()
 {
  if (FDM)
   return FDM->DocHandEditable;
  else
   return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetDocHandEditable(bool AVal)
 {
  if (FDM)
   FDM->DocHandEditable = AVal;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocClient::FGetFilterEnabled()
 {
  if (FDM)
   return FDM->FilterEnabled;
  else
   return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetFilterEnabled(bool AVal)
 {
  if (FDM)
   FDM->FilterEnabled = AVal;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocClient::FGetFilterEditable()
 {
  if (FDM)
   return FDM->FilterEditable;
  else
   return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetFilterEditable(bool AVal)
 {
  if (FDM)
   FDM->FilterEditable = AVal;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocClient::FGetExternalView()
 {
  if (FDM)
   return FDM->ExternalView;
  else
   return "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocClient::FSetExternalView(UnicodeString AVal)
 {
  if (FDM)
   FDM->ExternalView = AVal;
 }
// ---------------------------------------------------------------------------

