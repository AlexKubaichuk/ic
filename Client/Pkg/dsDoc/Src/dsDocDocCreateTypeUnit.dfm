object dsDocDocCreateTypeForm: TdsDocDocCreateTypeForm
  Left = 615
  Top = 397
  ActiveControl = CreateTypeRG
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1060#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
  ClientHeight = 173
  ClientWidth = 338
  Color = clBtnFace
  TransparentColorValue = clNone
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  GlassFrame.Bottom = 45
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 132
    Width = 338
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      338
      41)
    object OkBtn: TcxButton
      Left = 163
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      ModalResult = 1
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = OkBtnClick
    end
    object CancelBtn: TcxButton
      Left = 249
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object CreateTypeRG: TcxRadioGroup
    Left = 0
    Top = 0
    Align = alClient
    Caption = #1060#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1076#1086#1082#1091#1084#1077#1085#1090
    ParentBackground = False
    ParentFont = False
    Properties.Items = <
      item
        Caption = #1087#1086' '#1073#1072#1079#1077' '#1076#1072#1085#1085#1099#1093
        Value = '1'
      end
      item
        Caption = #1087#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1084
        Value = '0'
      end
      item
        Caption = #1087#1091#1089#1090#1086#1081
        Value = '2'
      end>
    ItemIndex = 0
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'MS Sans Serif'
    Style.Font.Style = []
    Style.TextStyle = [fsBold]
    Style.IsFontAssigned = True
    TabOrder = 0
    ExplicitLeft = 8
    ExplicitTop = 8
    Height = 132
    Width = 338
  end
end
