//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FltPassportEdit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxSpinEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TFilterPassportEditForm *FilterPassportEditForm;
//---------------------------------------------------------------------------
__fastcall TFilterPassportEditForm::TFilterPassportEditForm(TComponent* Owner, TICSFilterPassport* APasp)
        : TForm(Owner)
{
  FPasp = APasp;
  NameED->Text   = FPasp->MainName;
  AuthorED->Text = FPasp->Author;
  VerED->Value   = FPasp->Version.ToIntDef(1);
  RelED->Value   = FPasp->Release.ToIntDef(0);
  UnicodeString DTStr     = FPasp->Timestamp;
  DTCreateLab->Caption = "���� � ����� ��������: "+DTStr.SubString( 7, 2 ) + "." + DTStr.SubString( 5, 2 )  + "." + DTStr.SubString( 1, 4 ) +
                         "  " +
                         DTStr.SubString( 9, 2 ) + ":" + DTStr.SubString( 11, 2 ) + ":" + DTStr.SubString( 13, 2 );
}
//---------------------------------------------------------------------------
void __fastcall TFilterPassportEditForm::ApplyBtnClick(TObject *Sender)
{
  FPasp->MainName = NameED->Text.Trim();
  FPasp->Author   = AuthorED->Text.Trim();
  FPasp->Version  = IntToStr((int)VerED->Value);
  FPasp->Release  = IntToStr((int)RelED->Value);
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------

