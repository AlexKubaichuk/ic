/*
 ����        - FilterEdit.cpp
 ������      - ��������������� (ICS)
 ��������    - ���� ���������� ������, ����������� �������� �����
 ��� �������������� �������, � ���������� -
 ���� "condrules"
 ( ������������ ����������� TICSDocSpec )
 ����������� - ������� �.�.
 ����        - 23.09.2004
 */
// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

// #include "ChoiceList.h"
#include "FilterEditEx.h"
#include "IGroup.h"
#include "FltPassportEdit.h"
#include "ExtUtils.h"
// #include "pgmsetting.h"
#include "DKUtils.h"
#define Str2Ole(a) StringToOleStr(a)
// ---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxClasses"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxSplitter"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma link "dxStatusBar"
#pragma resource "*.dfm"
#pragma resource "curres.RES"
// ---------------------------------------------------------------------------
#define _OR_    "[���]"
#define _AND_   "[�]"
#define _GOR_   "(���)"
#define _GAND_  "(�)"
#define _NOT_   " [��]"
#define _CondNodeName_   "condrules"

//#define NOM_DLL_CSTR_MAXLEN             255
#define NOM_DLL_DEF_FUNC_PREFIX         "Def"
// ---------------------------------------------------------------------------
typedef bool(*TDefExtFunc)(TTagNode * AIE, char* AText, bool NeedKonkr, char* AXMLIE);
// ---------------------------------------------------------------------------
UnicodeString MessageTexts[] =
 { "�������� �������", // femtCaption
  "������", // femtRootNameF,
  "�������", // femtRootNameC,
  "��������� ������", // femtRootNameT,
  "����� ������", // femtNewFileName,
  "������ ���������", // femtCheckOk,
  "������ �����������", // femtCheckNone,
  "������ ������", // femtCheckEmpty,
  "����� �������" // femtSelectCondName
 } ;
TFilterEditExForm* FilterEditExForm;

const crFltDragAdd       = 5;
const crFltDragInsBefore = 6;
const crFltDragInsAfter  = 7;
// ---------------------------------------------------------------------------
__fastcall EFilterEditError::EFilterEditError(const UnicodeString Msg)
: Sysutils::Exception(Msg)
{
}
// ---------------------------------------------------------------------------
__fastcall TFilterEditExForm::TFilterEditExForm(TComponent* Owner) : TForm(Owner)
 {
  ValidateClipboard = false;
//  NomDef = Unassigned;
  FParentXML = NULL;
  FOnCustomLoad = NULL;
  FFltRet = new TTagNode(NULL);
  FFilter = new TICSFilter();
  FDTDNode = new TTagNode(NULL);
  FDTDNode->AsXML =
    "<?xml version='1.0' encoding='windows-1251'?>" "<list>" "<node uid='0001' name='region' childs='passport,condrules'/>"
    "<node uid='0002' name='passport' childs=''>" " <attr name='GUI'       values='' def='' req='1'/>" " <attr name='mainname'  values='' def='' req='1'/>"
    " <attr name='autor'     values='' def='' req='1'/>" " <attr name='version'   values='' def='' req='1'/>"
    " <attr name='release'   values='' def='' req='1'/>" " <attr name='timestamp' values='' def='' req='1'/>" "</node>"
    "<node uid='0003' name='condrules' childs='IERef,IAND,IOR,IMOR,RAND,ROR'>" " <attr name='uniontype' values='or,and' def='or' req='1'/>" "</node>"
    "<node uid='0004' name='IERef' childs='text'>" " <attr name='uid' values='' def='' req='1'/>" " <attr name='ref' values='' def='' req='1'/>"
    " <attr name='not' values='1,0' def='0' req='1'/>" " <attr name='incltoname' values='1,0' def='0' req='1'/>" "</node>"
    "<node uid='0005' name='IAND' childs='IERef,IAND,IOR,IMOR'>" " <attr name='uid' values='' def='' req='1'/>" " <attr name='name' values='' def='' req='1'/>"
    " <attr name='not' values='1,0' def='0' req='1'/>" " <attr name='uniontype' values='or,and' def='and' req='1'/>"
    " <attr name='incltoname' values='1,0' def='0' req='1'/>" "</node>" "<node uid='0006' name='IOR' childs='IERef,IAND,IOR,IMOR'>"
    " <attr name='uid' values='' def='' req='1'/>" " <attr name='name' values='' def='' req='1'/>" " <attr name='not' values='1,0' def='0' req='1'/>"
    " <attr name='incltoname' values='1,0' def='0' req='1'/>" "</node>" "<node uid='0007' name='IMOR' childs='IERef,IAND,IOR,IMOR'>"
    " <attr name='uid' values='' def='' req='1'/>" " <attr name='name' values='' def='' req='1'/>" " <attr name='not' values='1,0' def='0' req='1'/>"
    " <attr name='uniontype' values='or,and' def='and' req='1'/>" " <attr name='incltoname' values='1,0' def='0' req='1'/>" "</node>"
    "<node uid='0008' name='RAND' childs='IERef,IAND,IOR,IMOR,RAND,ROR'>" " <attr name='not' values='1,0' def='0' req='1'/>"
    " <attr name='pure' values='1,0' def='1' req='1'/>" " <attr name='comment' values='' def='' req='0'/>" "</node>"
    "<node uid='0009' name='ROR' childs='IERef,IAND,IOR,IMOR,RAND,ROR'>" " <attr name='not' values='1,0' def='0' req='1'/>"
    " <attr name='pure' values='1,0' def='1' req='1'/>" " <attr name='comment' values='' def='' req='0'/>" "</node>" "<node uid='000A' name='text' childs=''>"
    " <attr name='name' values='' def='' req='1'/>" " <attr name='value' values='' def='' req='1'/>" " <attr name='ref' values='' def='' req='0'/>"
    " <attr name='show' values='' def='1' req='0'/>" "</node>" "</list>";
  FFilter->OnCopyNode = FOnCopyNodes;
  FFilter->OnCheckNodeRef = FOnNodeCheckRef;
  FFilter->DTD = (TICSDTDChecker *)FDTDNode;

  FNom = NULL;
  NomCreated = false;
  FCanExtAdd = false;
  FRootName = "";
  FReg = NULL;
  FNomGUI = "";
  FMainISUID = "";
  FBaseNom = NULL;
  FEditType = fetNone;
  SetActVisible();
  FGroupNames["IOR"] = "(���)";
  FGroupNames["IMOR"] = "(���)";
  FGroupNames["IAND"] = "(�)";
  FGroupNames["ROR"] = "[���]";
  FGroupNames["RAND"] = "[�]";
  FCanKonkr = false;
  FInclToNameChB = new TcxCheckBox(this);
  FInclToNameChB->Properties->NullStyle = nssUnchecked;
  FInclToNameChB->Properties->ValueChecked = 1;
  FInclToNameChB->Properties->ValueGrayed = 0;
  FInclToNameChB->Properties->ValueUnchecked = 0;
  FDragAdd = NULL;
  FDragInsBefore = NULL;
  FDragInsAfter = NULL;
  FDragedNode = NULL;

  FDragAdd = LoadCursor(HInstance, L"DRAGADD");
  FDragInsBefore = LoadCursor(HInstance, L"DRAGINSAFTER");
  FDragInsAfter = LoadCursor(HInstance, L"DRAGINSBEFORE");
  Screen->Cursors[crFltDragAdd] = FDragAdd;
  Screen->Cursors[crFltDragInsBefore] = FDragInsBefore;
  Screen->Cursors[crFltDragInsAfter] = FDragInsAfter;
  Caption = MsgTxt(femtCaption);
 }
// ---------------------------------------------------------------------------
__fastcall TFilterEditExForm::~TFilterEditExForm()
 {
  if (FFilter)
   delete FFilter;
  if (NomCreated && FNom)
   delete FNom;
  delete FFltRet;
  delete FInclToNameChB;
  delete FDTDNode;
 }
// ---------------------------------------------------------------------------

// ##############################################################################
// #                                                                            #
// #                           ��������/��������                                #
// #                                                                            #
// ##############################################################################

// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::New()
 {
  FModify = false;
  ModifyOnIm->Visible = false;
  ModifyOffIm->Visible = true;
  LoadNom();
  if (!FFilter->Condition)
   throw EFilterEditError("�� ������ ������ �������");
  if (!FNom || !FNom->Valid)
   throw EFilterEditError("�� �������� �����������");
  if ((FEditType == fetFilter) || (FEditType == fetFilteredTemplate) || (FEditType == fetKonkr))
   FNewFilter();
  else if (FEditType == fetCondition)
   FNewCondition();
  else if (FEditType == fetTemplate)
   FNewTemplate();
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FNewFilter()
 {
  try
   {
    FPaintNom();
    FRulesTV->BeginUpdate();
    FFilter->Condition->DeleteChild();
    FFilter->NomGUI = FNomGUI;
    FFilter->Condition->AV["uniontype"] = "and";
    TcxTreeListNode* FRootNode;
    FRulesTV->Clear();
    FRootNode = FRulesTV->Root->AddChild();
    FRootName = MsgTxt(femtRootNameF);
    // if (FFilter->Passport)
    // FRootName += " ("+FFilter->Passport->MainName+") ";
    FRootNode->Data = (void*)FFilter->Condition;
    FRootNode->Texts[0] = GetXMLNodeViewText(FFilter->Condition);
    SetInclToName(FRootNode, FFilter->Condition);
   }
  __finally
   {
    FRulesTV->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FNewCondition()
 {
  try
   {
    FPaintNom();
    FRulesTV->BeginUpdate();
    FFilter->Condition->DeleteChild();
    FFilter->NomGUI = FNomGUI;
    FFilter->Condition->AV["uniontype"] = "and";
    TcxTreeListNode* FRootNode;
    FRulesTV->Clear();
    FRootNode = FRulesTV->Root->AddChild();
    FRootName = MsgTxt(femtRootNameC);
    FRootNode->Data = (void*)FFilter->Condition;
    FRootNode->Texts[0] = GetXMLNodeViewText(FFilter->Condition);
    SetInclToName(FRootNode, FFilter->Condition);
   }
  __finally
   {
    FRulesTV->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FNewTemplate()
 {
  try
   {
    FRootName = MsgTxt(femtRootNameT);
    FRulesTV->BeginUpdate();
    FFilter->Condition->DeleteChild();
    FFilter->NomGUI = FNomGUI;
    FFilter->Condition->AV["uniontype"] = "and";
    FFilter->Condition->DeleteChild();
    TTagNode* FDomain = FNom->Nmkl->GetChildByName("domain", true);
    if (FDomain)
     {
      TcxTreeListNode* FISNode;
      TcxTreeListNode* FISRuleNode;
      TcxTreeListNode* tmpNode;
      TTagNode* itISNode = FDomain->GetFirstChild();
      TTagNode* itISRulesNode;
      TDTDTagNode* FISRefNode;
      TDTDTagNode* FISRefRuleNode;
      FRulesTV->Clear();
      while (itISNode)
       {
        itISRulesNode = itISNode->GetChildByName("rulesunit", true);
        if (itISRulesNode)
         {
          FISRefNode = FFilter->Condition->DTDAddChild("RAND");
          FISRefNode->AV["comment"] = itISNode->AV["name"];
          FISNode = FRulesTV->Root->AddChild();
          FISNode->Texts[0] = itISNode->AV["name"];
          FISNode->Data = (void*)FISRefNode;
          itISRulesNode = itISRulesNode->GetFirstChild();
          while (itISRulesNode)
           {
            if (itISRulesNode->CmpName("IE"))
             {
              FISRuleNode = FISNode->AddChild();
              FISRefRuleNode = FISRefNode->DTDAddChild("IERef", itISRulesNode);
              if (GetPart1(itISRulesNode->AV["name"], ':').Trim().UpperCase() == itISNode->AV["name"].Trim().UpperCase())
               FISRuleNode->Texts[0] = GetPart2(itISRulesNode->AV["name"], ':').Trim();
              else
               FISRuleNode->Texts[0] = itISRulesNode->AV["name"];
              FISRuleNode->Data = (void*)FISRefRuleNode;
             }
            itISRulesNode = itISRulesNode->GetNext();
           }
         }
        itISNode = itISNode->GetNext();
       }
      FRulesTV->Root->Expand(true);
     }
   }
  __finally
   {
    FRulesTV->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
TTagNode* __fastcall TFilterEditExForm::FGetFlt()
 {
  TTagNode* RC = NULL;
  try
   {
    if ((FEditType == fetFilter) || (FEditType == fetFilteredTemplate) || (FEditType == fetKonkr))
     {
      RC = FFltRet;
      RC->Assign(FFilter->AsFilter, true);
     }
    else if ((FEditType == fetCondition) || (FEditType == fetTemplate))
     {
      RC = FFltRet;
      RC->Assign(FFilter->AsCondition, true);
      if (FEditType == fetTemplate)
       {
        FDelEmptyNode(RC);
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TTagNode* __fastcall TFilterEditExForm::AsFilter()
 {
  TTagNode* RC = NULL;
  try
   {
    RC = FFltRet;
    RC->Assign(FFilter->AsFilter, true);
    if (FEditType == fetTemplate)
     FDelEmptyNode(RC->GetChildByName(_CondNodeName_));
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FDelEmptyNode(TTagNode* ASrc)
 {
  TTagNode* itGr = ASrc->GetFirstChild();
  TTagNode* itIERef;
  TTagNode* tmp;
  while (itGr)
   {
    itIERef = itGr->GetFirstChild();
    while (itIERef)
     {
      if (!itIERef->Count)
       {
        tmp = itIERef;
        itIERef = itIERef->GetNext();
        delete tmp;
       }
      else
       itIERef = itIERef->GetNext();
     }
    if (!itGr->Count)
     {
      tmp = itGr;
      itGr = itGr->GetNext();
      delete tmp;
     }
    else
     itGr = itGr->GetNext();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FLoadTemplate(TDTDTagNode* ADest, TTagNode* ASrc)
 {
  TTagNode* itGr = ASrc->GetFirstChild();
  TTagNode* itIERef;
  TTagNode* FIEDef;
  TDTDTagNode* tmpIERef;
  if (itGr)
   {
    if (itGr->CmpName("RAND"))
     {
      while (itGr)
       {
        itIERef = itGr->GetFirstChild();
        while (itIERef)
         {
          if (itIERef->CmpName("IERef"))
           {
            FIEDef = FNom->Nmkl->GetTagByUID(_UID(itIERef->AV["ref"]));
            if (FIEDef)
             {
              tmpIERef = (TDTDTagNode *)ADest->GetChildByAV("IERef", "ref", itIERef->AV["ref"], true);
              if (tmpIERef)
               {
                if (itGr->CmpAV("not", "1"))
                 {
                  if (((TTagNode*)tmpIERef)->GetParent("RAND"))
                   ((TTagNode*)tmpIERef)->GetParent("RAND")->AV["not"] = "1";
                 }
                tmpIERef->Copy(itIERef);
               }
             }
            else
             {
              New();
              throw EFilterEditError("��������� ��������� ������, ����������� �������� �������.");
             }
           }
          else
           {
            New();
            throw EFilterEditError("��������� ��������� ������, ��������� �������.");
           }
          itIERef = itIERef->GetNext();
         }
        itGr = itGr->GetNext();
       }
     }
    else
     {
      New();
      throw EFilterEditError("��������� ��������� ������, ��������� ������.");
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FSetFlt(TTagNode* ASrc)
 {
  TTagNode* FNodeRef = NULL;
  try
   {
    New();
    if ((FEditType == fetFilter) || (FEditType == fetFilteredTemplate) || (FEditType == fetKonkr))
     {
      if (ASrc->CmpName("region"))
       FFilter->AsFilter = ASrc;
      else if (ASrc->CmpName(_CondNodeName_))
       FFilter->AsCondition = ASrc;
      else
       throw EFilterEditError("��������� ��������� ������, ��������� ������ ��� �������.");
     }
    else if (FEditType == fetCondition)
     {
      if (ASrc->CmpName("region"))
       {
        if (ASrc->GetChildByName(_CondNodeName_))
         FFilter->AsCondition = ASrc->GetChildByName(_CondNodeName_);
        else
         throw EFilterEditError("��������� ��������� ������, ��������� ������ ��� �������.");
       }
      else if (ASrc->CmpName(_CondNodeName_))
       FFilter->AsCondition = ASrc;
      else
       throw EFilterEditError("��������� ��������� ������, ��������� ������ ��� �������.");
     }
    else if (FEditType == fetTemplate)
     {
      if (ASrc->CmpName(_CondNodeName_))
       FLoadTemplate(FFilter->Condition, ASrc);
      else if (ASrc->CmpName("region"))
       {
        if (intload)
         {
          if (ASrc->GetChildByName(_CondNodeName_))
           FLoadTemplate(FFilter->Condition, ASrc->GetChildByName(_CondNodeName_));
         }
        else
         {
          EditType = fetFilteredTemplate;
          FNewFilter();
          FFilter->AsFilter = ASrc;
         }
        intload = false;
       }
      else
       throw EFilterEditError("��������� ��������� ������, ��������� ������ ��� �������.");
     }
   }
  __finally
   {
    if (FEditType == fetTemplate)
     {
      if (FFilter->Condition)
       {
        for (int i = 0; i < FRulesTV->Root->Count; i++)
         {
          FSetModify(false, FRulesTV->Root->Items[i], true);
         }
        FRulesTV->Root->Items[0]->MakeVisible();
        FRulesTV->Root->Items[0]->Focused = true;
       }
     }
    else if ((FEditType == fetFilter) || (FEditType == fetCondition) || (FEditType == fetFilteredTemplate) || (FEditType == fetKonkr))
     {
      if (FFilter->AsCondition)
       {
        try
         {
          FRulesTV->BeginUpdate();
          TcxTreeListNode* FNewNode;
          FNewNode = FRulesTV->Root->getFirstChild();
          TagNodeToTreeNode(FNewNode, FFilter->Condition);
          FSetModify(true, FNewNode, true);
         }
        __finally
         {
          FRulesTV->EndUpdate();
         }
       }
     }
   }
  FModify = false;
 }
// ---------------------------------------------------------------------------

// ##############################################################################
// #                                                                            #
// #                          ����������� ��������                              #
// #                                                                            #
// ##############################################################################

// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FAddRulesNode(TTagNode* ANomData, TDTDTagNode* ARuleData, UnicodeString ANodeName, bool ACanCopy)
 {
  try
   {
    if (ARuleData)
     {
      if (CanInfGroup(ARuleData->GetParent(), false))
       return;
      TDTDTagNode* FNodeRef = NULL;
      if (ANomData && ACanCopy)
       {
        if (ANomData->CmpName("IE"))
         FNodeRef = ARuleData->DTDAddChild("IERef", ANomData);
        else if (ANomData->CmpName("IAND"))
         {
          if (ANomData->Count == 1)
           FNodeRef = ARuleData->DTDAddChild("IERef", ANomData->GetFirstChild());
          else
           {
            FNodeRef = ARuleData->DTDAddChild("RAND", ANomData);
            FNodeRef->AV["pure"] = "0";
           }
         }
        else if (ANomData->CmpName("IOR,IMOR"))
         {
          IGroupKonkr(ANomData, ARuleData, atAdd);
         }
        else
         throw EFilterEditError("��������� ��� ��������.");
       }
      else
       FNodeRef = ARuleData->DTDAddChild(ANodeName);
      if (FNodeRef)
       {
        TcxTreeListNode* FNewNode;
        TcxTreeListNode* foNode = FGetRulesFocusedNode();
        FNewNode = foNode->AddChild();
        TagNodeToTreeNode(FNewNode, FNodeRef);
        FSetModify(true, foNode, false);
        FSetModify(true, FNewNode, true);
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FInsertRulesNode(TTagNode* ANomData, TDTDTagNode* ARuleData, UnicodeString ANodeName, bool ACanCopy, bool ACanBefore)
 {
  try
   {
    if (ARuleData)
     {
      if (CanInfGroup(ARuleData->GetParent(), false))
       return;
      TDTDTagNode* FNodeRef = NULL;
      if (ANomData && ACanCopy)
       {
        if (ANomData->CmpName("IE"))
         FNodeRef = ARuleData->DTDInsert("IERef", ACanBefore, ANomData);
        else if (ANomData->CmpName("IAND"))
         {
          if (ANomData->Count == 1)
           FNodeRef = ARuleData->DTDInsert("IERef", ACanBefore, ANomData->GetFirstChild());
          else
           {
            FNodeRef = ARuleData->DTDInsert("RAND", ACanBefore, ANomData);
            FNodeRef->AV["pure"] = "0";
           }
         }
        else if (ANomData->CmpName("IOR,IMOR"))
         {
          IGroupKonkr(ANomData, ARuleData, (ACanBefore) ? atInsBefore : atInsAfter);
         }
        else
         throw EFilterEditError("��������� ��� ��������.");
       }
      else
       FNodeRef = ARuleData->DTDInsert(ANodeName, ACanBefore);
      if (FNodeRef)
       {
        TcxTreeListNode* FNewNode;
        TcxTreeListNode* FFocusNode = FGetRulesFocusedNode();
        if (ACanBefore)
         FNewNode = FFocusNode->Parent->InsertChild(FFocusNode);
        else
         {
          FNewNode = FFocusNode->getNextSibling();
          if (FNewNode)
           FNewNode = FFocusNode->Parent->InsertChild(FNewNode);
          else
           FNewNode = FFocusNode->Parent->AddChild();
         }
        TagNodeToTreeNode(FNewNode, FNodeRef);
        FSetModify(true, FFocusNode, false);
        FSetModify(true, FNewNode, true);
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actAddConditionExecute(TObject* Sender)
 {
  if (!actAddCondition->Enabled)
   return;
  try
   {
    FAddRulesNode(FGetNomData(), FGetRulesData(), "", true);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actInsertCondBeforeExecute(TObject* Sender)
 {
  if (!actInsertCondBefore->Enabled)
   return;
  try
   {
    FInsertRulesNode(FGetNomData(), FGetRulesData(), "", true, true);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actInsertCondAfterExecute(TObject* Sender)
 {
  if (!actInsertCondAfter->Enabled)
   return;
  try
   {
    FInsertRulesNode(FGetNomData(), FGetRulesData(), "", true, false);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actAddORGroupExecute(TObject* Sender)
 {
  if (!actAddORGroup->Enabled)
   return;
  try
   {
    FAddRulesNode(NULL, FGetRulesData(), "ROR", false);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actInsertORBeforeExecute(TObject* Sender)
 {
  if (!actInsertORBefore->Enabled)
   return;
  try
   {
    FInsertRulesNode(NULL, FGetRulesData(), "ROR", false, true);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actInsertORAfterExecute(TObject* Sender)
 {
  if (!actInsertORAfter->Enabled)
   return;
  try
   {
    FInsertRulesNode(NULL, FGetRulesData(), "ROR", false, false);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actAddANDGroupExecute(TObject* Sender)
 {
  if (!actAddANDGroup->Enabled)
   return;
  try
   {
    FAddRulesNode(NULL, FGetRulesData(), "RAND", false);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actInsertANDBeforeExecute(TObject* Sender)
 {
  if (!actInsertANDBefore->Enabled)
   return;
  try
   {
    FInsertRulesNode(NULL, FGetRulesData(), "RAND", false, true);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actInsertANDAfterExecute(TObject* Sender)
 {
  if (!actInsertANDAfter->Enabled)
   return;
  try
   {
    FInsertRulesNode(NULL, FGetRulesData(), "RAND", false, false);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actSwapGroupTypeExecute(TObject* Sender)
 {
  TDTDTagNode* ndData = FGetRulesData();
  bool FUpdate        = false;
  if (ndData)
   {
    if (ndData->CmpName(_CondNodeName_))
     {
      if (ndData->DTD->AttrExist(ndData, "uniontype"))
       {
        ndData->AV["uniontype"] = (ndData->CmpAV("uniontype", "and")) ? "or" : "and";
        FUpdate = true;
       }
     }
    else
     {
      if (!CanInfGroup(ndData, true))
       {
        ndData->SwapTo(UnicodeString((ndData->CmpName("ROR")) ? "RAND" : "ROR"));
        FUpdate = true;
       }
     }
    if (FUpdate)
     FSetModify(true, FGetRulesFocusedNode(), true);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actSetNOTPrefExecute(TObject* Sender)
 {
  TDTDTagNode* ndData = FGetRulesData();
  if (ndData)
   if (ndData->DTD->AttrExist(ndData, "not"))
    {
     ndData->AV["not"] = IntToStr((int) !ndData->AV["not"].ToIntDef(0));
     FSetModify(true, FGetRulesFocusedNode(), true);
    }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actDeleteExecute(TObject* Sender)
 {
  try
   {
    TDTDTagNode* FRuleData = FGetRulesData();
    if (FRuleData)
     {
      bool FCanDelete = true;
      if (FBeforeDelete)
       FCanDelete = FBeforeDelete(this, (TTagNode *)FRuleData);

      if (FCanDelete)
       {
        if (FRuleData->CmpName(_CondNodeName_))
         {
          FGetRulesFocusedNode()->DeleteChildren();
          FRuleData->DeleteChild();
         }
        else
         {
          TcxTreeListNode* newFocusedNode = FGetRulesFocusedNode();
          TcxTreeListNode* curFocusedNode = FGetRulesFocusedNode();
          if (newFocusedNode->getPrevSibling())
           newFocusedNode = newFocusedNode->getPrevSibling();
          else
           newFocusedNode = newFocusedNode->Parent;
          newFocusedNode->Focused = true;
          curFocusedNode->Delete();
          delete FRuleData;
         }
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actSetValueExecute(TObject* Sender)
 {
  if (!actSetValue->Enabled)
   return;
  TTagNode* CurNode = (TTagNode *)FRulesTV->Selections[0]->Data;
  if (CurNode->CmpName("IERef"))
   {
    UnicodeString FViewText = "";
    try
     {
      if (CallDefProc(CurNode, FViewText, true))
       {
        TcxTreeListNode* FNode = FGetRulesFocusedNode();
        TDTDTagNode* ndData    = FGetRulesData(FNode);
        FSetModify(true, FGetRulesFocusedNode(), true);
        if (ndData && InclToNameCol->Visible && ndData->DTD->AttrExist(ndData->Name, "incltoname"))
         {
          ndData->AV["incltoname"] = "1";
          FNode->Values[1] = 1;
         }
       }
     }
    catch (Exception& E)
     {
      ShowMessage(E.Message);
     }
   }
  else // IOR,IMOR,IAND (ROR RAND � ��������� pure=0)
   {
    if (CanInfGroup((TDTDTagNode*)CurNode, false))
     IGroupKonkr(FGetNomData(), FGetRulesData(), atNone);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actResetValueExecute(TObject* Sender)
 {
  TDTDTagNode* CurNode = FGetRulesData();
  if (CurNode->CmpName("IERef"))
   {
    CurNode->DeleteChild();
    FSetModify(true, FGetRulesFocusedNode(), true);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actEditCommentExecute(TObject* Sender)
 {
  try
   {
    UnicodeString tmp;
    TDTDTagNode* ndData = FGetRulesData();
    if (ndData)
     {
      tmp = InputBox(ndData->AV["name"], "�����������:", ndData->AV["comment"]);
      ndData->AV["comment"] = tmp;
      FSetModify(true, FGetRulesFocusedNode(), true);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actPropertuesExecute(TObject* Sender)
 {
  TFilterPassportEditForm* Dlg = NULL;
  try
   {
    Dlg = new TFilterPassportEditForm(this, FFilter->Passport);
    if (Dlg)
     {
      Dlg->ShowModal();
      if (Dlg->ModalResult == mrOk)
       FSetModify(true, FRulesTV->Root->Items[0], false);
     }

   }
  __finally
   {
    if (Dlg)
     delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actSaveExecute(TObject* Sender)
 {
  if (FFilter->Passport)
   SaveDlg->FileName = FFilter->Passport->MainName + ".xflt";
  else
   SaveDlg->FileName = MsgTxt(femtNewFileName) + ".xflt";
  if (!SaveDlg->FileName.Length())
   SaveDlg->FileName = MsgTxt(femtNewFileName) + ".xflt";
  if (SaveDlg->Execute())
   {
    try
     {
      Filter->SaveToXMLFile(icsPrePath(SaveDlg->FileName), "");
     }
    __finally
     {
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actCheckExecute(TObject* Sender)
 {
  if (Check() == fchOk)
   MessageBox(Handle, MsgTxt(femtCheckOk).c_str(), MsgTxt(femtCaption).c_str(), MB_ICONINFORMATION);
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actLoadExecute(TObject* Sender)
{
  if (OpenDlg->Execute())
   {
     TTagNode* tmp = NULL;
     try
      {
        tmp = new TTagNode(NULL);
        Application->ProcessMessages();
        tmp->LoadFromXMLFile(icsPrePath(OpenDlg->FileName));
        Application->ProcessMessages();
        intload = true;
        Application->ProcessMessages();
        TTagNode* FPass = tmp->GetChildByName("passport");
        if (FPass)
         FPass->AV["GUI"] = FFilter->Passport->GUI;
        Filter = tmp;
        Application->ProcessMessages();
      }
     __finally
      {
        if (tmp)
         delete tmp;
      }
   }
}
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actApplyExecute(TObject* Sender)
 {
  if (Check() == fchOk)
   {
    FModify = false;
    if (FCanClose)
     {
      if (FCanClose(this, FFilter->Passport->MainName))
       ModalResult = mrOk;
     }
    else
     ModalResult = mrOk;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actCopyExecute(TObject* Sender)
 {
  try
   {
    UnicodeString tmp;
    TDTDTagNode* ndData = FGetRulesData();
    if (ndData)
     {
      Clipboard()->AsText = ndData->AsXML;
      if (FGetRulesFocusedNode())
       FSetActEnabled(FGetRulesFocusedNode(), true, true);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actCutExecute(TObject* Sender)
 {
  try
   {
    UnicodeString tmp;
    TDTDTagNode* ndData = FGetRulesData();
    if (ndData)
     {
      Clipboard()->AsText = ndData->AsXML;
      actDeleteExecute(actDelete);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actPasteExecute(TObject* Sender)
 {
  if (!actPaste->Enabled)
   return;
  try
   {
    TInfGroupAddType FAddRulesNodeType = atNone;
    TDTDTagNode* FRuleData             = FGetRulesData();
    if (FRuleData)
     {
      if (CanInfGroup(FRuleData->GetParent(), false))
       return;
      UnicodeString fPasteData = Clipboard()->AsText;
      TDTDTagNode* FNodeRef    = NULL;
      if (fPasteData.Trim().Length())
       {
        TTagNode* tstNode = NULL;
        try
         {
          tstNode = new TTagNode(NULL);
          try
           {
            tstNode->AsXML = Clipboard()->AsText.Trim();
            if ((tstNode->CmpName(_CondNodeName_) && tstNode->Count) || !tstNode->CmpName(_CondNodeName_))
             { // ���� ���� ���������
              try
               {
                if (FRuleData->CmpName(_CondNodeName_))
                 { // �������� ���-�� ��������� � �����
                  if (FRuleData->Count)
                   { // ������ �� ������, ������� ����� ����� ������ �����������
                    FSwapCondGr(tstNode);
                    FNodeRef = ((TDTDTagNode *)FRuleData->GetFirstChild())->InsertEx(true, tstNode);
                    FAddRulesNodeType = atInsAfter; // ������� ����� ������ ������
                   }
                  else
                   { // ������ ������, ���� ���������
                    if (tstNode->CmpName(_CondNodeName_))
                     {
                      FNodeRef = FRuleData;
                      FAddRulesNodeType = atNone;
                      TTagNode* itTest = tstNode->GetFirstChild();
                      while (itTest)
                       {
                        FRuleData->AddChildEx(itTest);
                        itTest = itTest->GetNext();
                       }
                     }
                    else
                     {
                      FAddRulesNodeType = atAdd;
                      FNodeRef = FRuleData->AddChildEx(tstNode);
                     }
                   }
                 }
                else if (FRuleData->CmpName("ror,rand"))
                 {
                  if (FRuleData->CmpAV("pure", "1"))
                   { // ������ ����������
                    FSwapCondGr(tstNode);
                    if (FRuleData->Count)
                     { // ������ �� ������, ������� ����� ����� �������
                      FAddRulesNodeType = atInsBefore;
                      FNodeRef = FRuleData->InsertEx(true, tstNode);
                     }
                    else
                     { // ������ ������, ���� ���������
                      FAddRulesNodeType = atAdd;
                      FNodeRef = FRuleData->AddChildEx(tstNode);
                     }
                   }
                  else
                   { // �� ���, �������� �������� �����
                    FAddRulesNodeType = atInsBefore;
                    FSwapCondGr(tstNode);
                    FNodeRef = FRuleData->InsertEx(true, tstNode);
                   }
                 }
                else
                 {
                  FAddRulesNodeType = atInsBefore;
                  FSwapCondGr(tstNode);
                  FNodeRef = FRuleData->InsertEx(true, tstNode);
                 }
               }
              catch (EICSCondNodeError& E)
               {
                MessageBox(Handle, E.Message.c_str(), L"������", MB_ICONSTOP);
               }
             }
           }
          catch (...)
           {
           }
         }
        __finally
         {
          if (tstNode)
           delete tstNode;
         }
       }
      if (FNodeRef)
       {
        TcxTreeListNode* FNewNode;
        TcxTreeListNode* FFocusNode = FGetRulesFocusedNode();

        if (FAddRulesNodeType == atNone)
         FNewNode = FFocusNode;
        else if (FAddRulesNodeType == atAdd)
         FNewNode = FFocusNode->AddChild();
        else if (FAddRulesNodeType == atInsBefore)
         FNewNode = FFocusNode->Parent->InsertChild(FFocusNode);
        else // atInsAfter
           FNewNode = FFocusNode->InsertChild(FFocusNode->getFirstChild());

        TagNodeToTreeNode(FNewNode, FNodeRef);
        FSetModify(true, FNewNode, true);
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FSwapCondGr(TTagNode* ASrc)
 {
  if (ASrc->CmpName(_CondNodeName_))
   {
    ASrc->Name = "R" + UnicodeString((ASrc->CmpAV("uniontype", "and") ? "and" : "or")).UpperCase();
    ASrc->DeleteAttr("uniontype");
    ASrc->AV["pure"] = "1";
    ASrc->AV["not"] = "0";
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actLoadFromLibExecute(TObject* Sender)
 {
  if (FOnCustomLoad)
   {
    TTagNode* tmpFlt = NULL;
    try
     {
      tmpFlt = new TTagNode(NULL);
      if (FOnCustomLoad(MsgTxt(femtSelectCondName), tmpFlt))
       {
        intload = true;
        TTagNode* FPass = tmpFlt->GetChildByName("passport");
        if (FPass)
         FPass->AV["GUI"] = FFilter->Passport->GUI;
        Filter = tmpFlt;
       }
     }
    __finally
     {
      if (tmpFlt)
       delete tmpFlt;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actChangeModeExecute(TObject* Sender)
 {
  TTagNode* tmpFlt = Filter;
  if (FEditType == fetTemplate)
   EditType = fetFilteredTemplate;
  else
   EditType = fetTemplate;
  intload = true;
  Filter = tmpFlt;
 }
// ---------------------------------------------------------------------------

// ##############################################################################
// #                                                                            #
// #                  ����������� ��� �������                                   #
// #                                                                            #
// ##############################################################################

// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FSetEditType(TFilterEditType AEditType)
 {
  FEditType = AEditType;
  if (AEditType == fetKonkr)
   FCanKonkr = true;
  SetActVisible();
 }
// ---------------------------------------------------------------------------

// ##############################################################################
// #                                                                            #
// #                  ����������� ����������� ��������                          #
// #                                                                            #
// ##############################################################################

// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::SetActVisible()
 {
  MainBM->Bars->BeginUpdate();
  FRulesTV->BeginUpdate();
  FNomTV->BeginUpdate();
  try
   {
    actPropertues->Visible = false;
    actPropertues->Enabled = false;
    actApply->Visible = false;
    actApply->Enabled = false;
    actLoad->Visible = false;
    actLoad->Enabled = false;
    actSave->Visible = false;
    actSave->Enabled = false;

    actCopy->Visible = false;
    actCopy->Enabled = false;
    actCut->Visible = false;
    actCut->Enabled = false;
    actPaste->Visible = false;
    actPaste->Enabled = false;

    actAddCondition->Visible = false;
    actAddCondition->Enabled = false;
    actAddANDGroup->Visible = false;
    actAddANDGroup->Enabled = false;
    actAddORGroup->Visible = false;
    actAddORGroup->Enabled = false;

    actInsertCondBefore->Visible = false;
    actInsertCondBefore->Enabled = false;
    actInsertCondAfter->Visible = false;
    actInsertCondAfter->Enabled = false;
    actInsertANDBefore->Visible = false;
    actInsertANDBefore->Enabled = false;
    actInsertANDAfter->Visible = false;
    actInsertANDAfter->Enabled = false;
    actInsertORBefore->Visible = false;
    actInsertORBefore->Enabled = false;
    actInsertORAfter->Visible = false;
    actInsertORAfter->Enabled = false;

    actSwapGroupType->Visible = false;
    actSwapGroupType->Enabled = false;
    actSetNOTPref->Visible = false;
    actSetNOTPref->Enabled = false;

    actDelete->Visible = false;
    actDelete->Enabled = false;
    actSetValue->Visible = false;
    actSetValue->Enabled = false;
    actResetValue->Visible = false;
    actResetValue->Enabled = false;
    actEditComment->Visible = false;
    actEditComment->Enabled = false;
    actCheck->Visible = false;
    actCheck->Enabled = false;
    TreeSpl->Visible = false;
    FNomTV->Visible = false;
    InclToNameCol->Visible = (FEditType != fetKonkr);

    actLoadFromLib->Visible = false;
    actLoadFromLib->Enabled = false;
    actChangeMode->Visible = false;
    actChangeMode->Enabled = false;
    if (FEditType == fetKonkr)
     {
      actApply->Visible = true;
      actApply->Enabled = true;
      actSetValue->Visible = true;
      actResetValue->Visible = true;
      actCheck->Visible = true;
      actCheck->Enabled = true;
     }
    else if (FEditType == fetFilter)
     {
      // Width = 720;
      if (FOnCustomLoad)
       {
        actLoadFromLib->Visible = true;
        actLoadFromLib->Enabled = true;
       }
      // MainBM->Bars->Items[0]->DockingStyle = dsLeft;
      FNomTV->Visible = true;
      TreeSpl->Visible = true;
      actPropertues->Visible = true;
      actPropertues->Enabled = true;
      actApply->Visible = true;
      actApply->Enabled = true;
      actCopy->Visible = true;
      actCut->Visible = true;
      actPaste->Visible = true;

      actAddCondition->Visible = true;
      actAddANDGroup->Visible = true;
      actAddORGroup->Visible = true;

      actLoad->Visible = true;
      actLoad->Enabled = true;
      actSave->Visible = true;
      actSave->Enabled = true;

      actInsertCondBefore->Visible = true;
      actInsertCondAfter->Visible = true;
      actInsertANDBefore->Visible = true;
      actInsertANDAfter->Visible = true;
      actInsertORBefore->Visible = true;
      actInsertORAfter->Visible = true;

      actSwapGroupType->Visible = true;
      actSetNOTPref->Visible = true;
      actDelete->Visible = true;
      actSetValue->Visible = true;
      actResetValue->Visible = true;
      actEditComment->Visible = true;
      actCheck->Visible = true;
      actCheck->Enabled = true;
     }
    else if (FEditType == fetFilteredTemplate)
     {
      // Width = 720;
      if (FOnCustomLoad)
       {
        actLoadFromLib->Visible = true;
        actLoadFromLib->Enabled = true;
       }
      actChangeMode->Visible = true;
      actChangeMode->Enabled = true;
      // MainBM->Bars->Items[0]->DockingStyle = dsLeft;
      FNomTV->Visible = true;
      TreeSpl->Visible = true;
      // actPropertues->Visible    = true; actPropertues->Enabled = true;
      actApply->Visible = true;
      actApply->Enabled = true;
      actCopy->Visible = true;
      actCut->Visible = true;
      actPaste->Visible = true;

      actLoad->Visible = true;
      actLoad->Enabled = true;
      actSave->Visible = true;
      actSave->Enabled = true;

      actAddCondition->Visible = true;
      actAddANDGroup->Visible = true;
      actAddORGroup->Visible = true;

      actInsertCondBefore->Visible = true;
      actInsertCondAfter->Visible = true;
      actInsertANDBefore->Visible = true;
      actInsertANDAfter->Visible = true;
      actInsertORBefore->Visible = true;
      actInsertORAfter->Visible = true;

      actSwapGroupType->Visible = true;
      actSetNOTPref->Visible = true;
      actDelete->Visible = true;
      actSetValue->Visible = true;
      actResetValue->Visible = true;
      actEditComment->Visible = true;
      actCheck->Visible = true;
      actCheck->Enabled = true;
     }
    // ||(FEditType == fetFilteredTemplate)
    else if (FEditType == fetCondition)
     {
      // Width = 720;
      InclToNameCol->Visible = false;
      // MainBM->Bars->Items[0]->DockingStyle = dsLeft;
      FNomTV->Visible = true;
      TreeSpl->Visible = true;
      actApply->Visible = true;
      actApply->Enabled = true;
      // actLoad->Visible          = true;
      // actSave->Visible          = true;
      actCopy->Visible = true;
      actCut->Visible = true;
      actPaste->Visible = true;

      actLoad->Visible = true;
      actLoad->Enabled = true;
      actSave->Visible = true;
      actSave->Enabled = true;

      actAddCondition->Visible = true;
      actAddANDGroup->Visible = true;
      actAddORGroup->Visible = true;
      actInsertCondBefore->Visible = true;
      actInsertCondAfter->Visible = true;
      actInsertANDBefore->Visible = true;
      actInsertANDAfter->Visible = true;
      actInsertORBefore->Visible = true;
      actInsertORAfter->Visible = true;

      actSwapGroupType->Visible = true;
      actSetNOTPref->Visible = true;
      actDelete->Visible = true;
      actSetValue->Visible = true;
      actResetValue->Visible = true;
      actEditComment->Visible = true;
      actCheck->Visible = true;
      actCheck->Enabled = true;
     }
    else if (FEditType == fetTemplate)
     {
      // Width = 400;
      actChangeMode->Visible = true;
      actChangeMode->Enabled = true;
      // MainBM->Bars->Items[0]->DockingStyle = dsTop;
      // MainBM->Bars->Items[1]->DockedLeft = 2;
      MainBM->Bars->Items[0]->DockedTop = 0;
      MainBM->Bars->Items[1]->DockedTop = 0;
      MainBM->Bars->Items[1]->Row = 0;
      actApply->Visible = true;
      actApply->Enabled = true;

      actLoad->Visible = true;
      actLoad->Enabled = true;
      actSave->Visible = true;
      actSave->Enabled = true;

      actSetNOTPref->Visible = true;
      actSetValue->Visible = true;
      actResetValue->Visible = true;
      actCheck->Visible = true;
      actCheck->Enabled = true;
     }
   }
  __finally
   {
    MainBM->Bars->EndUpdate();
    FRulesTV->EndUpdate();
    FNomTV->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FSetActEnabled(TcxTreeListNode* AFocusedNode, bool AAllEn, bool IsRulesTV)
 {
  if (AAllEn)
   {
    if (!IsRulesTV)
     {
      FSetActEnabled(actAddCondition, FCheckAddAction(FGetNomData(AFocusedNode), FGetRulesData()));
      FSetActEnabled(actInsertCondBefore, FCheckInsertAction(FGetNomData(AFocusedNode), FGetRulesData()));
      FSetActEnabled(actInsertCondAfter, FCheckInsertAction(FGetNomData(AFocusedNode), FGetRulesData()));
     }
    else
     {
      TDTDTagNode* ndRData = FGetRulesData(AFocusedNode);
      TTagNode* ndNData    = FGetNomData();

      FSetActEnabled(actAddCondition, FCheckAddAction(ndNData, ndRData));

      bool tmp = FCheckInsertAction(ndNData, ndRData);
      FSetActEnabled(actInsertCondBefore, tmp);
      FSetActEnabled(actInsertCondAfter, tmp);

      tmp = FCheckAddGrAction(ndNData, ndRData);
      FSetActEnabled(actAddORGroup, tmp);
      FSetActEnabled(actAddANDGroup, tmp);

      tmp = FCheckInsertGrAction(ndNData, ndRData);
      FSetActEnabled(actInsertANDBefore, tmp);
      FSetActEnabled(actInsertANDAfter, tmp);
      FSetActEnabled(actInsertORBefore, tmp);
      FSetActEnabled(actInsertORAfter, tmp);

      FSetActEnabled(actSwapGroupType, FCheckSwapAction(ndNData, ndRData));
      FSetActEnabled(actSetNOTPref, FCheckSetNOTAction(ndNData, ndRData));

      FSetActEnabled(actDelete, FCheckDelAction(ndNData, ndRData));
      FSetActEnabled(actSetValue, FCheckSetValAction(ndNData, ndRData));
      FSetActEnabled(actResetValue, FCheckResetValAction(ndNData, ndRData));
      FSetActEnabled(actEditComment, FCheckEditComAction(ndNData, ndRData));

      FValidateClipboard(ndRData);

      FSetActEnabled(actCut, actDelete->Enabled);
      FSetActEnabled(actCopy, actDelete->Enabled);
     }
   }
  else
   {
    actAddORGroup->Enabled = false;
    actAddANDGroup->Enabled = false;
    actSwapGroupType->Enabled = false;
    actSetNOTPref->Enabled = false;
    actAddCondition->Enabled = false;
    actDelete->Enabled = false;
    actSetValue->Enabled = false;
    actResetValue->Enabled = false;
    actEditComment->Enabled = false;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::FCheckAddComm(TDTDTagNode* ARuleTag)
 {
  bool RC = false;
  try
   {
    if (ARuleTag)
     {
      RC |= ARuleTag->CmpName(_CondNodeName_);
      RC |= (!CanInfGroup(ARuleTag, false) && !ARuleTag->CmpName("IERef"));
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::FCheckAddAction(TTagNode* ANomTag, TDTDTagNode* ARuleTag)
 {
  bool RC = false;
  try
   {
    if (ANomTag && ARuleTag)
     {
      if (ANomTag->CmpName("IE,IAND,IOR,IMOR"))
       {
        RC = FCheckAddComm(ARuleTag);
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::FCheckInsertAction(TTagNode* ANomTag, TDTDTagNode* ARuleTag)
 {
  bool RC = false;
  try
   {
    if (ANomTag && ARuleTag)
     {
      if (ANomTag->CmpName("IE,IAND,IOR,IMOR"))
       {
        RC = FCheckAddComm(ARuleTag->GetParent()) && (!ARuleTag->CmpName(_CondNodeName_));
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::FCheckAddGrAction(TTagNode* ANomTag, TDTDTagNode* ARuleTag)
 {
  bool RC = false;
  try
   {
    RC = FCheckAddComm(ARuleTag);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::FCheckInsertGrAction(TTagNode* ANomTag, TDTDTagNode* ARuleTag)
 {
  bool RC = false;
  try
   {
    if (ARuleTag)
     RC = FCheckAddComm(ARuleTag->GetParent()) && (!ARuleTag->CmpName(_CondNodeName_));
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::FCheckSwapAction(TTagNode* ANomTag, TDTDTagNode* ARuleTag)
 {
  bool RC = false;
  try
   {
    if (ARuleTag)
     {
      RC |= ARuleTag->CmpName(_CondNodeName_);
      RC |= ARuleTag->CmpName("ROR,RAND") && !CanInfGroup(ARuleTag, false);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::FCheckSetNOTAction(TTagNode* ANomTag, TDTDTagNode* ARuleTag)
 {
  bool RC = false;
  try
   {
    if (ARuleTag)
     RC = ARuleTag->DTD->AttrExist(ARuleTag, "not");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::FCheckDelAction(TTagNode* ANomTag, TDTDTagNode* ARuleTag)
 {
  bool RC = false;
  try
   {
    if (ARuleTag)
     {
      if (ARuleTag->CmpName("condrules,IERef,IOR,IAND,IMOR,ROR,RAND"))
       {
        if (ARuleTag->CmpName("IERef"))
         {
          bool CanLogic       = true;
          TDTDTagNode* itNode = ARuleTag;
          while (itNode->GetParent() && CanLogic)
           {
            itNode = itNode->GetParent();
            if (itNode->DTD->AttrExist(itNode, "pure"))
             CanLogic &= itNode->CmpAV("pure", "1");
           }
          RC = CanLogic;
         }
        else
         RC = true;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::FCheckSetValAction(TTagNode* ANomTag, TDTDTagNode* ARuleTag)
 {
  bool RC = false;
  try
   {
    if (ARuleTag)
     {
      if (FEditType == fetKonkr)
       {
        if (ARuleTag->CmpName("IERef"))
         RC = !ARuleTag->Count;
        else
         RC = ARuleTag->CmpName("IAND,IOR,IMOR");
       }
      else
       RC = ARuleTag->CmpName("IERef,IAND,IOR,IMOR");
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::FCheckResetValAction(TTagNode* ANomTag, TDTDTagNode* ARuleTag)
 {
  bool RC = false;
  try
   {
    if (ARuleTag)
     {
      if (FEditType == fetKonkr)
       {
        if (ARuleTag->CmpName("IERef"))
         RC = !ARuleTag->Count;
       }
      else
       RC = ARuleTag->CmpName("IERef");
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::FCheckEditComAction(TTagNode* ANomTag, TDTDTagNode* ARuleTag)
 {
  bool RC = false;
  try
   {
    if (ARuleTag)
     {
      RC = ARuleTag->DTD->AttrExist(ARuleTag, "comment");
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FSetActEnabled(TAction* AAct, bool AVal)
 {
  if (!AVal)
   AAct->Enabled = AVal;
  else
   {
    if (AAct->Visible)
     AAct->Enabled = true;
    else
     AAct->Enabled = false;
   }
 }
// ---------------------------------------------------------------------------

// ##############################################################################
// #                                                                            #
// #                          ��������� �������� �� ��������                    #
// #                                                                            #
// ##############################################################################

// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FPaintNom()
 { // ��������� ������������ �� ��������
  try
   {
    FNomTV->BeginUpdate();
    TTagNode* FDomain = FNom->Nmkl->GetChildByName("domain", true);
    if (FDomain)
     {
      TcxTreeListNode* FISNode;
      TcxTreeListNode* FISRuleNode;
      TcxTreeListNode* tmpNode;
      TTagNode* itISNode = FDomain->GetFirstChild();
      TTagNode* itISRulesNode;
      FNomTV->Clear();
      while (itISNode)
       {
        itISRulesNode = itISNode->GetChildByName("rulesunit", true);
        if (itISRulesNode)
         {
          FISNode = FNomTV->Root->AddChild();
          FISNode->Texts[0] = itISNode->AV["name"];
          FISNode->Data = (void*)itISNode;
          itISRulesNode = itISRulesNode->GetFirstChild();
          while (itISRulesNode)
           {
            FPaintNomGroup(itISRulesNode, FISNode);
            itISRulesNode = itISRulesNode->GetNext();
           }
         }
        itISNode = itISNode->GetNext();
       }
      FNomTV->Root->Expand(true);
     }
   }
  __finally
   {
    FNomTV->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FPaintNomGroup(TTagNode* ASrc, TcxTreeListNode* AParent)
 { // ��������� ������������ �� ��������
  try
   {
    TcxTreeListNode* FRuleNode;
    TTagNode* itNode;
    if (ASrc)
     {
      if (ASrc->CmpName("IE,IAND,IOR,IMOR"))
       {
        FRuleNode = AParent->AddChild();
        if (GetPart1(ASrc->AV["name"], ':').Trim().UpperCase() == ASrc->AV["name"].Trim().UpperCase())
         FRuleNode->Texts[0] = GetPart2(ASrc->AV["name"], ':').Trim();
        else
         FRuleNode->Texts[0] = ASrc->AV["name"];
        FRuleNode->Data = (void*)ASrc;
        if (ASrc->CmpName("IAND,IOR,IMOR"))
         {
          itNode = ASrc->GetFirstChild();
          while (itNode)
           {
            FPaintNomGroup(itNode, FRuleNode);
            itNode = itNode->GetNext();
           }
         }
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::SetInclToName(TcxTreeListNode* ANode, TDTDTagNode* ANodeData)
 {
  try
   {
    if (ANodeData && ANode)
     {
      if (InclToNameCol->Visible && ANodeData->DTD->AttrExist(ANodeData->Name, "incltoname"))
       ANode->Values[1] = ANodeData->AV["incltoname"].ToIntDef(0);
     }
   }
  __finally
   {
   }
  return true;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TFilterEditExForm::GetXMLNodeViewText(TDTDTagNode* TagNode)
 {
  /*
   if (FEditType == fetFilter)         FNewFilter();
   else if (FEditType == fetCondition) FNewCondition();
   else if (FEditType != fetTemplate)  FNewTemplate();
   */

  UnicodeString tmpText = TagNode->Name;
  if (TagNode->CmpName(_CondNodeName_))
   {
    tmpText = FRootName;
    if (FFilter->Passport)
     tmpText += " (" + FFilter->Passport->MainName + ") ";
   }
  else
   {
    TTagNode* ndParent = TagNode->GetParent();
    tmpText = "";
    if (ndParent->CmpName(_CondNodeName_) && (FEditType != fetTemplate))
     tmpText = (ndParent->CmpAV("uniontype", "or")) ? _OR_ : _AND_;
    else if (ndParent->CmpName("RAND") && (FEditType != fetTemplate))
     tmpText = _AND_;
    else if (ndParent->CmpName("ROR") && (FEditType != fetTemplate))
     tmpText = _OR_;
    if (TagNode->CmpAV("not", "1"))
     tmpText += _NOT_;
    if (TagNode->CmpName("IERef"))
     {
      tmpText += (" " + FXMLContainer->GetRefer(TagNode->AV["ref"])->AV["name"] + " = ");
      // tmpText += ( " " + FXMLContainer->GetRefer(TagNode->AV["ref"])->AV["name"] + " = " );
      UnicodeString FViewText = "?";
      if (TagNode->Count)
       {
        FViewText = "������";
        try
         {
           CallDefProc(TagNode, FViewText, false);
         }
        catch (...)
         {
          ShowMessage("������ ����������� �������� �������.");
         }
       }
      tmpText += ("{" + FViewText + "}");
     }
    else if (TagNode->CmpName("IAND,IOR,IMOR"))
     {
      tmpText += (" " + TagNode->AV["name"]);
     }
    else // RAND,ROR
     {
      tmpText += " ���������������";
      if (TagNode->AV["comment"].Length())
       tmpText += (" " + TagNode->AV["comment"]);
     }
   }
  if (TagNode->CmpName("condrules,RAND,ROR"))
   if (!TagNode->Count)
    {
     if (TagNode->CmpName(_CondNodeName_))
      tmpText += (TagNode->CmpAV("uniontype", "or")) ? " " _GOR_ : " " _GAND_;
     else if (TagNode->CmpName("RAND"))
      tmpText += " " _GAND_;
     else // ROR
        tmpText += " " _GOR_;
    }
  return tmpText;
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::LoadNom()
 {
  if (!FNom)
   throw EFilterEditError("����������� ����������� �����������.");

  if (!FNom->Valid)
   FNom->SrcNom = FBaseNom;
  FNom->DomainNumber = FDomainNumber;
  if (!FMainISUID.Trim().Length())
   {
    TTagNode* tmpDNode = FNom->GetDomainNode(FDomainNumber);
    if (tmpDNode)
     {
      if (tmpDNode->GetFirstChild())
       {
        FMainISUID = tmpDNode->GetFirstChild()->AV["uid"];
       }
     }
   }
  FNom->MainISUID = FMainISUID;
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::ClearNom(){ FNomTV->Clear();}
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::SetDomainNumber(int AValue)
 {
  if (AValue < 0)
   throw EFilterEditError("����� ������ ������ ���� ������ 0.");
  FDomainNumber = AValue;
 }
// ---------------------------------------------------------------------------

// ##############################################################################
// #                                                                            #
// #                          DragDrop                                          #
// #                                                                            #
// ##############################################################################

// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FRulesTVDragDrop(TObject* Sender, TObject* Source, int X, int Y)
 {
  if (((TcxTreeList *)Source)->Name == "FNomTV")
   {
    bool FAdd = actAddCondition->Enabled && actAddCondition->Visible;
    bool FIns = actInsertCondBefore->Enabled && actInsertCondBefore->Visible;
    SHORT CtrlKeyPress;
    CtrlKeyPress = GetAsyncKeyState(VK_LCONTROL);
    CtrlKeyPress += GetAsyncKeyState(VK_RCONTROL);

    if (FAdd && FIns)
     {
      if (CtrlKeyPress)
       actInsertCondBeforeExecute(actInsertCondBefore);
      else
       actAddConditionExecute(actAddCondition);
     }
    else if (FIns)
     {
      if (CtrlKeyPress)
       actInsertCondAfterExecute(actInsertCondAfter);
      else
       actInsertCondBeforeExecute(actInsertCondBefore);
     }
    else if (FAdd)
     actAddConditionExecute(actAddCondition);
   }

 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FRulesTVDragOver(TObject* Sender, TObject* Source, int X, int Y, TDragState State, bool& Accept)
 {
  if (((TcxTreeList *)Source)->Name == "FNomTV")
   {
    FNomTV->SetFocus();
    bool FAdd = FCheckAddAction(FGetNomData(), FGetRulesData());
    bool FIns = FCheckInsertAction(FGetNomData(), FGetRulesData());
    SHORT CtrlKeyPress;
    CtrlKeyPress = GetAsyncKeyState(VK_LCONTROL);
    CtrlKeyPress += GetAsyncKeyState(VK_RCONTROL);
    if (FAdd && FIns)
     {
      Accept = true;
      if (CtrlKeyPress)
       ((TcxTreeList*)Source)->DragCursor = (TCursor)crFltDragInsBefore;
      else
       ((TcxTreeList*)Source)->DragCursor = (TCursor)crFltDragAdd;
     }
    else if (FIns)
     {
      Accept = true;
      if (CtrlKeyPress)
       ((TcxTreeList*)Source)->DragCursor = (TCursor)crFltDragInsAfter;
      else
       ((TcxTreeList*)Source)->DragCursor = (TCursor)crFltDragInsBefore;
     }
    else if (FAdd)
     {
      Accept = true;
      ((TcxTreeList*)Source)->DragCursor = (TCursor)crFltDragAdd;
     }
    else
     Accept = false;
   }
  // else
  // Accept = false;
  else if (((TcxTreeList *)Source)->Name == "FRulesTV")
   {
    Accept = false;
    TDTDTagNode* FSelNode        = FGetRulesData();
    TDTDTagNode* FDragedNodeData = FGetRulesData(FDragedNode);
    if (FSelNode && FDragedNodeData)
     {
      bool FIns = FSelNode->DTD->CanAddChild(FSelNode->GetParent()->Name, FDragedNodeData->Name);
      FIns &= !CanInfGroup(FSelNode->GetParent(), false) && !FSelNode->CmpName(_CondNodeName_);
      SHORT CtrlKeyPress;
      CtrlKeyPress = GetAsyncKeyState(VK_LCONTROL);
      CtrlKeyPress += GetAsyncKeyState(VK_RCONTROL);
      if (FIns)
       {
        Accept = true;
        if (CtrlKeyPress)
         ((TcxTreeList*)Source)->DragCursor = (TCursor)crFltDragInsAfter;
        else
         ((TcxTreeList*)Source)->DragCursor = (TCursor)crFltDragInsBefore;
       }
      else
       Accept = false;
     }
    else
     Accept = false;
   }
  else
   Accept = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FNomTVMouseDown(TObject* Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
 {
  TTagNode* nomData = FGetNomData(FNomTV->HitTest->HitNode);
  if (nomData)
   {
    if (nomData->GetParent("IAND") || nomData->CmpName("IS"))
     {
      FNomTV->DragMode = dmManual;
     }
    else
     FNomTV->DragMode = dmAutomatic;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FRulesTVMouseDown(TObject* Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
 {
  TDTDTagNode* ndData = FGetRulesData(FRulesTV->HitTest->HitNode);
  FRulesTV->DragMode = dmManual;
  if (ndData)
   {
    if (!CanInfGroup(ndData->GetParent(), false) && !ndData->CmpName(_CondNodeName_) && (FEditType != fetTemplate))
     FRulesTV->DragMode = dmAutomatic;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FRulesTVStartDrag(TObject* Sender, TDragObject*& DragObject){ FDragedNode = FGetRulesFocusedNode();}
// ---------------------------------------------------------------------------

// ##############################################################################
// #                                                                            #
// #                          ��������������� �������                           #
// #                                                                            #
// ##############################################################################

// ---------------------------------------------------------------------------
TcxTreeListNode* __fastcall TFilterEditExForm::FGetRulesFocusedNode()
 {
  TcxTreeListNode* RC = NULL;
  try
   {
    if (FRulesTV->SelectionCount)
     RC = FRulesTV->Selections[0];
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TcxTreeListNode* __fastcall TFilterEditExForm::FGetNomFocusedNode()
 {
  TcxTreeListNode* RC = NULL;
  try
   {
    if (FNomTV->SelectionCount)
     RC = FNomTV->Selections[0];
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TTagNode* __fastcall TFilterEditExForm::FGetNomData(TcxTreeListNode* ATreeNode)
 {
  TTagNode* RC = NULL;
  try
   {
    TcxTreeListNode* FNd;
    if (ATreeNode)
     FNd = ATreeNode;
    else
     FNd = FGetNomFocusedNode();
    if (FNd)
     if (FNd->Data)
      RC = (TTagNode *)FNd->Data;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TDTDTagNode* __fastcall TFilterEditExForm::FGetRulesData(TcxTreeListNode* ATreeNode)
 {
  TDTDTagNode* RC = NULL;
  try
   {
    TcxTreeListNode* FNd;
    if (ATreeNode)
     FNd = ATreeNode;
    else
     FNd = FGetRulesFocusedNode();
    if (FNd)
     if (FNd->Data)
      RC = (TDTDTagNode *)FNd->Data;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::IGroupKonkr(TTagNode* ANomData, TDTDTagNode* ARuleData, TInfGroupAddType AAddType)
 {
  TIGroupKonkrForm* Dlg = NULL;
  bool FSuccess         = false;
  TDTDTagNode* FGroup   = NULL;
  try
   {
    if (ARuleData)
     {
      if (AAddType == atNone)
       FGroup = ARuleData;
      else
       {
        if (ANomData)
         {
          if (!CanInfGroup(ARuleData->GetParent(), false))
           {
            if (AAddType == atAdd)
             FGroup = ARuleData->DTDAddChild(ANomData->Name, ANomData);
            else if (AAddType == atInsBefore)
             FGroup = ARuleData->DTDInsert(ANomData->Name, true, ANomData);
            else if (AAddType == atInsAfter)
             FGroup = ARuleData->DTDInsert(ANomData->Name, false, ANomData);
           }
         }
       }
      Dlg = new TIGroupKonkrForm(this, FGroup, (TTagNode *)FNom->Nmkl, false);
      if (Dlg && FGroup)
       {
        Dlg->ShowModal();
        FSuccess = Dlg->ModalResult != mrCancel;
       }
     }
   }
  __finally
   {
    if (Dlg)
     delete Dlg;
    if (FGroup)
     {
      if (FSuccess)
       {
        TcxTreeListNode* FNewNode = NULL;
        try
         {
          TcxTreeListNode* FFocusNode = FGetRulesFocusedNode();
          // FRulesTV->BeginUpdate();
          if (AAddType == atNone)
           FNewNode = FFocusNode;
          else
           {
            if (AAddType == atAdd)
             FNewNode = FFocusNode->AddChild();
            else if (AAddType == atInsBefore)
             FNewNode = FFocusNode->Parent->InsertChild(FFocusNode);
            else if (AAddType == atInsAfter)
             {
              FNewNode = FFocusNode->getNextSibling();
              if (FNewNode)
               FNewNode = FFocusNode->Parent->InsertChild(FNewNode);
              else
               FNewNode = FFocusNode->Parent->AddChild();
             }
           }
          TagNodeToTreeNode(FNewNode, FGroup);
          FSetModify(true, FNewNode, true);
         }
        __finally
         {
          // FRulesTV->EndUpdate();
         }
       }
      else
       {
        if (AAddType != atNone)
         delete FGroup;
       }
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::TagNodeToTreeNode(TcxTreeListNode* ADest, TTagNode* ASrc)
 {
  ADest->DeleteChildren();
  ADest->Data = (void*)ASrc;
  TTagNode* itNode = ASrc->GetFirstChild();
  while (itNode)
   {
    if (!itNode->CmpName("text"))
     TagNodeToTreeNode(ADest->AddChild(), itNode);
    itNode = itNode->GetNext();
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::CallDefProc(TTagNode* AIE, UnicodeString &AText, bool NeedKonkr)
 {
  bool RC = false;
  UnicodeString ProcName = NOM_DLL_DEF_FUNC_PREFIX + FXMLContainer->GetRefer(AIE->AV["ref"])->AV["procname"];
  try
   {
     if (FOnCallDefProc)
      {
        RC = FOnCallDefProc(ProcName, AIE, NeedKonkr, AText);
      }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FSetModify(bool AVal, TcxTreeListNode* ANode, bool ACanChild)
 {
  FModify |= AVal;
  if (FModify)
   {
    ModifyOnIm->Visible = true;
    ModifyOffIm->Visible = false;
   }
  else
   {
    ModifyOnIm->Visible = false;
    ModifyOffIm->Visible = true;
   }
  try
   {
    FRulesTV->BeginUpdate();
    TDTDTagNode* ndData = FGetRulesData(ANode);
    // ANode->MakeVisible();
    // FNewNode->Repaint(true);
    if (ndData)
     {
      if (ANode->StateIndex == 2)
       ANode->StateIndex = 0;
      else
       ANode->StateIndex = -1;
      if ((FEditType == fetFilter) || (FEditType == fetCondition) || (FEditType == fetFilteredTemplate) || (FEditType == fetKonkr))
       {
        ANode->Texts[0] = GetXMLNodeViewText(ndData);
        SetInclToName(ANode, ndData);
        if (ndData->CmpName("IOR,IMOR,IAND"))
         ANode->StateIndex = 1;
        else if (ndData->CmpName("ROR,RAND"))
         {
          if (ndData->CmpAV("pure", "0"))
           ANode->StateIndex = 1;
         }
        if (ACanChild)
         {
          for (int i = 0; i < ANode->Count; i++)
           {
            FSetNodeText(ANode->Items[i]);
           }
         }
       }
      else if (FEditType == fetTemplate)
       {
        if (ndData->CmpName("IERef") && ndData->Count)
         ANode->StateIndex = 2;
        if (!ndData->CmpName("RAND"))
         ANode->Texts[0] = GetXMLNodeViewText(ndData);
        else if (ndData->CmpName("RAND"))
         {
          if (ndData->CmpAV("not", "1"))
           ANode->Texts[0] = ndData->AV["comment"] + " " + UnicodeString(_NOT_) + " �������������";
          else
           ANode->Texts[0] = ndData->AV["comment"];
         }
        if (ACanChild)
         {
          for (int i = 0; i < ANode->Count; i++)
           {
            FSetNodeText(ANode->Items[i]);
           }
         }
       }
     }
   }
  __finally
   {
    FRulesTV->EndUpdate();
    ANode->MakeVisible();
    ANode->Focused = true;
    // ANode->Selected = true;
    if (ANode->Count)
     ANode->Expand(true);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FSetNodeText(TcxTreeListNode* ANode)
 {
  TDTDTagNode* ndData;
  ndData = FGetRulesData(ANode);
  if (ndData)
   {
    if (ANode->StateIndex == 2)
     ANode->StateIndex = 0;
    else
     ANode->StateIndex = -1;
    if (FEditType == fetTemplate)
     {
      if (ndData->Count)
       {
        if (ndData->CmpName("IERef"))
         ANode->StateIndex = 2;
        ANode->Texts[0] = GetXMLNodeViewText(ndData);
       }
     }
    else
     ANode->Texts[0] = GetXMLNodeViewText(ndData);
    SetInclToName(ANode, ndData);
   }
  for (int i = 0; i < ANode->Count; i++)
   FSetNodeText(ANode->Items[i]);
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::FOnNodeCheckRef(TTagNode* ASrc)
 {
  bool RC = true;
  try
   {
    if (ASrc->GetAttrByName("ref") && !ASrc->CmpName("text"))
     {
      #ifdef IMM_DEBUG
      if (_GUI(ASrc->AV["ref"]).UpperCase() != FNomGUI.UpperCase())
       RC = false;
      #else
      if ((_GUI(ASrc->AV["ref"]).UpperCase() != FNomGUI.UpperCase()) || (!FNom->Nmkl->GetTagByUID(_UID(ASrc->AV["ref"]))))
       RC = false;
      #endif
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FOnCopyNodes(TTagNode* ASrc, TDTDTagNode* ADest)
 {
  TTagNode* itNode;
  if (ASrc->CmpName("IE,IAND,IOR,IMOR"))
   {
    if (ADest->DTD->AttrExist(ADest, "uid"))
     ADest->AV["uid"] = (FParentXML) ? FParentXML->NewUID() : ADest->NewUID();
    if (ADest->DTD->AttrExist(ADest, "name"))
     ADest->AV["name"] = ASrc->AV["name"];
    if (ADest->DTD->AttrExist(ADest, "comment"))
     ADest->AV["comment"] = ASrc->AV["comment"];
    if (ASrc->CmpName("IE"))
     {
      ADest->AV["ref"] = NomGUI + "." + ASrc->AV["uid"];
     }
    else if (ASrc->CmpName("IAND,IOR,IMOR"))
     {
      if (ADest->CmpName("RAND,ROR"))
       ADest->AV["comment"] = ASrc->AV["name"];
      itNode = ASrc->GetFirstChild();
      while (itNode)
       {
        if (itNode->CmpName("IE"))
         ADest->DTDAddChild("IERef", itNode);
        else
         ADest->DTDAddChild(itNode->Name, itNode);
        itNode = itNode->GetNext();
       }
      if (ASrc->CmpName("IAND,IMOR"))
       {
        if (ASrc->GetAttrByName("uniontype"))
         ADest->AV["uniontype"] = ASrc->AV["uniontype"];
       }
     }
   }
  else if (ASrc->CmpName("IERef,text"))
   {
    UnicodeString FAName;
    if (ADest->CmpName(ASrc->Name))
     {
      for (int i = 0; i < ASrc->AttrCount; i++)
       {
        FAName = ASrc->Attrs(i)->Name;
        if (!ADest->GetNodeData()->IsUID(FAName)) // �������� ��� �������� ����� uid-a
         {
          if (ADest->DTD->AttrExist(ADest, FAName))
           ADest->AV[FAName] = ASrc->AV[FAName];
         }
       }
     }
    TTagNode* itNode = ASrc->GetFirstChild();
    while (itNode)
     {
      ADest->DTDAddChild(itNode->Name, itNode);
      itNode = itNode->GetNext();
     }
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::CanInfGroup(TDTDTagNode* ASrc, bool CanKonkrOnly)
 {
  bool RC = false;
  try
   {
    if (!ASrc)
     throw EFilterEditError(UnicodeString(__FUNC__) + ": ASrc is NULL");

    if (!CanKonkrOnly)
     {
      if (ASrc->CmpName("IOR,IMOR,IAND"))
       RC = true;
     }
    if (ASrc->CmpName("ROR,RAND"))
     {
      RC |= ASrc->CmpAV("pure", "0");
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::InclToNameColGetEditProperties(TcxTreeListColumn* Sender, TcxTreeListNode* ANode, TcxCustomEditProperties*& EditProperties)
 {
  EditProperties = NULL;
  try
   {
    if (Sender)
     {
      if (Sender->Position->ColIndex == 1)
       {
        TDTDTagNode* ndData = FGetRulesData(ANode);
        if (ndData)
         {
          if (InclToNameCol->Visible && ndData->DTD->AttrExist(ndData->Name, "incltoname"))
           EditProperties = FInclToNameChB->Properties;
          else
           EditProperties = NULL;
         }
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FRulesTVDblClick(TObject* Sender)
 {
  if (FRulesTV->HitTest->HitColumn)
   {
    if ((FRulesTV->HitTest->HitColumn->Position->ColIndex == 0) && (!FRulesTV->HitTest->HitAtColumnHeader))
     actSetValueExecute(actSetValue);
   }
 }
// ---------------------------------------------------------------------------
TCheckFilterResult __fastcall TFilterEditExForm::Check()
 {
  if (IsNone())
   {
    if (FEditType == fetCondition)
     return fchOk;
    MessageBox(Handle, MsgTxt(femtCheckNone).c_str(), MsgTxt(femtCaption).c_str(), MB_ICONHAND);
    return fchNone;
   }
  if (IsEmpty())
   {
    if (FEditType == fetCondition)
     return fchOk;
    FRulesTV->SetFocus();
    FRulesTV->Root->Focused = true;
    MessageBox(Handle, MsgTxt(femtCheckEmpty).c_str(), MsgTxt(femtCaption).c_str(), MB_ICONHAND);
    return fchEmpty;
   }

  bool ChTemplate = false;
  for (int i = 0; i < FRulesTV->AbsoluteCount; i++)
   {
    TcxTreeListNode* TreeNode = FRulesTV->AbsoluteItems[i];
    TTagNode* TagNode         = reinterpret_cast<TTagNode *>(TreeNode->Data);

    // �������� �� ������� ������ ������
    if ((TagNode->Name != "IERef") && (!TagNode->Count) && (FEditType != fetTemplate))
     {
      FRulesTV->SetFocus();
      TreeNode->MakeVisible();
      TreeNode->Focused = true;
      MessageBox(Handle, L"������� ������ ����� ������� �����������.", MsgTxt(femtCaption).c_str(), MB_ICONHAND);
      return fchBad;
     }

    // �������� �� ������� �������������������� ������
    if ((FEditType == fetTemplate) && TagNode->CmpName("IERef"))
     ChTemplate |= (bool)TagNode->Count;
    if (FCanKonkr)
     if (TagNode->CmpName("IOR,IMOR,IAND") || ((TagNode->CmpName("IERef")) && (!TagNode->Count) && (FEditType != fetTemplate)))
      {
       FRulesTV->SetFocus();
       TreeNode->MakeVisible();
       TreeNode->Focused = true;
       if (TagNode->CmpName("IERef"))
        MessageBox(Handle, L"������� ������� c �������������� ���������� �����������.", MsgTxt(femtCaption).c_str(), MB_ICONHAND);
       else
        MessageBox(Handle, L"������� ������������� ����� ������� �����������.", MsgTxt(femtCaption).c_str(), MB_ICONHAND);
       return fchBad;
      }
   }
  if ((FEditType == fetTemplate) && !ChTemplate)
   {
    MessageBox(Handle, L"���������� ������� �������� ��� ������� ��� ������ ��������� ������.", MsgTxt(femtCaption).c_str(), MB_ICONHAND);
    return fchBad;
   }
  return fchOk;
 }

// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::IsEmpty()
 {
  if (!FRulesTV)
   throw EFilterEditError("FRulesTV not created.");
  bool RC = true;
  try
   {
    TTagNode* tmp = Filter;
    if (!tmp->CmpName(_CondNodeName_))
     tmp = tmp->GetChildByName(_CondNodeName_);
    if (tmp)
     {
      if (tmp->CmpName(_CondNodeName_))
       RC = (!tmp->Count);
      else
       throw EFilterEditError("FRulesTV not created.");
     }
    else
     throw EFilterEditError("FRulesTV not created.");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TFilterEditExForm::IsNone()
 {
  if (!FRulesTV)
   throw EFilterEditError("FRulesTV not created.");
  return (FRulesTV->Count == 0);
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::actCloseExecute(TObject* Sender){ ModalResult = mrCancel;}
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FormClose(TObject* Sender, TCloseAction& Action)
 {
  if (FModify)
   {
    if ((FEditType == fetFilter) || (FEditType == fetCondition) || (FEditType == fetFilteredTemplate) || (FEditType == fetKonkr))
     {
      if (MessageBox(Handle, L"      ��������!!!\n���� ������� ���������.\n��������� ���������?", L"", MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) == ID_YES)
       {
        if (Check() == fchOk)
         {
          FModify = false;
          if (FCanClose)
           {
            if (FCanClose(this, FFilter->Passport->MainName))
             {
              Action = caHide;
              ModalResult = mrOk;
             }
            else
             Action = caNone;
           }
          else
           {
            Action = caHide;
            ModalResult = mrOk;
           }
         }
        else
         Action = caNone;
       }
      else
       Action = caHide;
     }
    else
     Action = caHide;
   }
  else
   Action = caHide;
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FSetCanKonkr(bool AVal)
 {
  if (FEditType == fetKonkr)
   FCanKonkr = true;
  else
   FCanKonkr = AVal;
  SetActVisible();
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FNomTVDblClick(TObject* Sender)
 {
  if (actAddCondition->Enabled)
   actAddConditionExecute(actAddCondition);
  else if (actInsertCondBefore->Enabled)
   actInsertCondBeforeExecute(actInsertCondBefore);
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FormShow(TObject* Sender)
{
  ValidateClipboard = true;
  FRulesTV->SetFocus();
  FRulesTV->Root->Focused = true;
  if (FRulesTV->Root->Count)
   {
     FRulesTV->Root->Items[0]->MakeVisible();
     FRulesTV->Root->Items[0]->Focused = true;
     FRulesTV->Root->Items[0]->Selected = true;
   }
}
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FormActivate(TObject* Sender)
{
  TDTDTagNode* ndRData = FGetRulesData();
  FValidateClipboard(ndRData);
}
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FValidateClipboard(TDTDTagNode* AndRData)
 {
  bool FCanPaste = false;
  if (ValidateClipboard)
   {
    if (Clipboard()->AsText.Trim().Length() && actPaste->Visible && AndRData)
     {
      TTagNode* tstNode = NULL;
      try
       {
        tstNode = new TTagNode(NULL);
        try
         {
          tstNode->AsXML = Clipboard()->AsText.Trim();
          // bool __fastcall TFilterEditExForm::FCheckAddAction(TTagNode *ANomTag, TDTDTagNode *ARuleTag)
          // bool __fastcall TFilterEditExForm::FCheckInsertAction(TTagNode *ANomTag, TDTDTagNode *ARuleTag)
          // bool __fastcall TFilterEditExForm::FCheckAddGrAction(TTagNode *ANomTag, TDTDTagNode *ARuleTag)
          // bool __fastcall TFilterEditExForm::FCheckInsertGrAction(TTagNode *ANomTag, TDTDTagNode *ARuleTag)
          /*
           bool FCanPaste = false;
           */
          if ((tstNode->CmpName(_CondNodeName_) && tstNode->Count) || !tstNode->CmpName(_CondNodeName_))
           { // ���� ���� ���������
            FCanPaste = FCheckAddGrAction(NULL, AndRData) || FCheckInsertGrAction(NULL, AndRData);
            // FCanPaste = FCheckAddGrAction(NULL, AndRData) || FCheckInsertGrAction(NULL, AndRData);
            if (tstNode->CmpName(_CondNodeName_))
             { // ��, ��� �������� �������� �������, ������� ���� ����� ������������ � ������
              if (FCanPaste)
               {
                TTagNode* itTest = tstNode->GetFirstChild();
                while (itTest && FCanPaste)
                 {
                  FCanPaste &= AndRData->CheckPaste(itTest, "ROR");
                  itTest = itTest->GetNext();
                 }
               }
             }
            else
             {
              if (AndRData->CmpName(_CondNodeName_))
               FCanPaste = AndRData->CheckPaste(tstNode, _CondNodeName_);
              else
               FCanPaste = AndRData->CheckPaste(tstNode);
             }
           }
         }
        catch (...)
         {
         }
       }
      __finally
       {
        if (tstNode)
         delete tstNode;
       }
     }
   }
  FSetActEnabled(actPaste, FCanPaste);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TFilterEditExForm::MsgTxt(TFilterEditMsgType AType)
 {
  UnicodeString RC = "";
  try
   {
    RC = MessageTexts[AType];
    if (FGetMessageText)
     RC = FGetMessageText(AType, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FSetCustomLoad(TOnFltCustomLoadEvent AOnCustomLoad)
 {
  FOnCustomLoad = AOnCustomLoad;
  SetActVisible();
 }
// ---------------------------------------------------------------------------
TTagNode* __fastcall TFilterEditExForm::FGetDTD(){ return (TTagNode *)FFilter->DTD;}
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FSetNomSrc(TTagNode* ANomSrc)
 {
  if (FNom)
   throw EFilterEditError("����������� ����������, ���������� �������� ��� �������� Nom");
  else
   {
    TTagNode* FPassport = ANomSrc->GetChildByName("passport", true);
    if (!FPassport)
     {
      FNomGUI = "";
      throw EFilterEditError("����������� ����� ��������� ���������");
     }
    FBaseNom = ANomSrc;
    FNomGUI = FPassport->AV["GUI"];
    FNom = new TICSNomenklator();
    NomCreated = true;
    FNom->SrcNom = FBaseNom;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FSetNom(TICSNomenklator* ANom)
 {
  if (NomCreated && FNom)
   delete FNom;
  FNom = ANom;
  FBaseNom = ANom->SrcNom;
  TTagNode* FPassport = ANom->SrcNom->GetChildByName("passport", true);
  if (!FPassport)
   {
    FNomGUI = "";
    throw EFilterEditError("����������� ����� ��������� ���������");
   }
  FNomGUI = FPassport->AV["GUI"];
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FRulesTVEditValueChanged(TcxCustomTreeList* Sender, TcxTreeListColumn* AColumn)
 {
  TcxTreeListNode* FNode = FGetRulesFocusedNode();
  if (FNode)
   {
    if (AColumn)
     {
      if (FRulesTV->InplaceEditor)
       {
        TDTDTagNode* ndData = FGetRulesData();
        if (ndData)
         {
          if (InclToNameCol->Visible && ndData->DTD->AttrExist(ndData->Name, "incltoname"))
           {
            ndData->AV["incltoname"] = IntToStr((int)FRulesTV->InplaceEditor->EditValue);
            FModify = true;
            ModifyOnIm->Visible = true;
            ModifyOffIm->Visible = false;
           }
         }
       }
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FRulesTVFocusedNodeChanged(TcxCustomTreeList* Sender, TcxTreeListNode* APrevFocusedNode, TcxTreeListNode* AFocusedNode)

 {
  if (Sender)
   FSetActEnabled(AFocusedNode, true, (((TcxTreeList*)Sender)->Name == "FRulesTV"));
  else
   FSetActEnabled(AFocusedNode, false, false);
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FRulesTVMoveTo(TcxCustomTreeList* Sender, TcxTreeListNode* AttachNode, TcxTreeListNodeAttachMode AttachMode, TList* Nodes,
  bool& IsCopy, bool& Done)
 {
  Done = false;
  if (!Nodes->Count)
   return;
  if (FDragedNode && (FDragedNode != AttachNode))
   {
    try
     {
      UnicodeString tmp;
      TDTDTagNode* ndData    = FGetRulesData(FDragedNode);
      TDTDTagNode* FRuleData = FGetRulesData(AttachNode);
      TDTDTagNode* FNodeRef  = NULL;
      UnicodeString ndXML    = "";
      if (ndData && FRuleData)
       {
        if (CanInfGroup(ndData->GetParent(), false))
         return;
        ndXML = ndData->AsXML;
        FDragedNode->Data = NULL;
        delete ndData;
        ndData = NULL;
        TDTDTagNode* FNodeRef = NULL;
        if (ndXML.Trim().Length())
         {
          try
           {
            FNodeRef = FRuleData->InsertEx(true, ndXML.Trim());
           }
          catch (EICSCondNodeError& E)
           {
            MessageBox(Handle, E.Message.c_str(), L"������", MB_ICONSTOP);
           }
         }
        if (FNodeRef)
         {
          FDragedNode->MoveTo(AttachNode, tlamInsert);
          TagNodeToTreeNode(FDragedNode, FNodeRef);
          FSetModify(true, FDragedNode, true);
          Done = true;
         }
       }
     }
    __finally
     {
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FRulesTVStylesGetContentStyle(TcxCustomTreeList* Sender, TcxTreeListColumn* AColumn, TcxTreeListNode* ANode,
  TcxStyle*& AStyle)

 {
  try
   {
    TDTDTagNode* ndData = FGetRulesData(ANode);
    if (ndData)
     {
      if (FEditType == fetKonkr)
       {
        if (ndData->CmpName("IERef") && ndData->Count)
         AStyle = grStyle;
       }
      else
       {
        if (ndData->CmpName("IOR,IAND,IMOR") || (ndData->CmpName("ROR,RAND") && !ndData->Count))
         AStyle = bStyle;
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TFilterEditExForm::FRulesTVEditing(TcxCustomTreeList* Sender, TcxTreeListColumn* AColumn, bool& Allow)
 {
  try
   {
    if (AColumn)
     {
      if (AColumn->Position->ColIndex == 1)
       {
        TDTDTagNode* ndData = FGetRulesData();
        if (ndData)
         Allow = ndData->DTD->AttrExist(ndData->Name, "incltoname");
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
namespace ICSDocFilter
 {
  TCheckDocFilterResult __fastcall InternalCheckCondRules(TTagNode* ACondRules)
   {
    // �������� �� ������� ������ ������
    if (!ACondRules->CmpName("IERef,text") && !ACondRules->Count)
     return frBad;
    // �������� �� ������� �������������������� ������
    if (ACondRules->CmpName("IOR,IMOR,IAND") || (ACondRules->CmpName("IERef") && !ACondRules->Count))
     return frBad;
    TTagNode* ndChild = ACondRules->GetFirstChild();
    while (ndChild)
     {
      TCheckDocFilterResult Res = InternalCheckCondRules(ndChild);
      if (Res != frOk)
       return Res;
      ndChild = ndChild->GetNext();
     }
    return frOk;
   }
  // ---------------------------------------------------------------------------
  TCheckDocFilterResult __fastcall CheckCondRules(TTagNode* ACondRules)
   {
    if (!ACondRules)
     return frNone;
    if (!ACondRules->Count)
     return frEmpty;
    return InternalCheckCondRules(ACondRules);
   }
  // ---------------------------------------------------------------------------
 } // end of namespace ICSDocFilter
