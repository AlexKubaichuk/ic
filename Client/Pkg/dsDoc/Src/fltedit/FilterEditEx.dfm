object FilterEditExForm: TFilterEditExForm
  Left = 347
  Top = 257
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1060#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1077' '#1092#1080#1083#1100#1090#1088#1072
  ClientHeight = 588
  ClientWidth = 841
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object FRulesTV: TcxTreeList
    Left = 0
    Top = 88
    Width = 552
    Height = 480
    Align = alClient
    Bands = <
      item
      end>
    Constraints.MinHeight = 100
    Constraints.MinWidth = 200
    DefaultRowHeight = 18
    DragCursor = crDrag
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Navigator.Buttons.CustomButtons = <>
    OptionsBehavior.AlwaysShowEditor = True
    OptionsBehavior.CellHints = True
    OptionsBehavior.DragDropText = True
    OptionsBehavior.DragFocusing = True
    OptionsBehavior.ExpandOnDblClick = False
    OptionsBehavior.MultiSort = False
    OptionsBehavior.Sorting = False
    OptionsData.Deleting = False
    OptionsView.CellEndEllipsis = True
    OptionsView.ShowEditButtons = ecsbAlways
    OptionsView.ColumnAutoWidth = True
    OptionsView.ExtPaintStyle = True
    OptionsView.GridLines = tlglVert
    OptionsView.UseNodeColorForIndent = False
    ParentFont = False
    PopupMenu = EditPM
    StateImages = StateIL
    Styles.OnGetContentStyle = FRulesTVStylesGetContentStyle
    Styles.ContentOdd = cxStyle3
    Styles.HotTrack = cxStyle4
    TabOrder = 1
    OnDblClick = FRulesTVDblClick
    OnDragDrop = FRulesTVDragDrop
    OnDragOver = FRulesTVDragOver
    OnEditing = FRulesTVEditing
    OnEditValueChanged = FRulesTVEditValueChanged
    OnFocusedNodeChanged = FRulesTVFocusedNodeChanged
    OnMouseDown = FRulesTVMouseDown
    OnMoveTo = FRulesTVMoveTo
    OnStartDrag = FRulesTVStartDrag
    Data = {
      000005006A0100000F00000044617461436F6E74726F6C6C6572310200000012
      000000546378537472696E6756616C7565547970651300000054637856617269
      616E7456616C75655479706508000000445855464D5401445855464D54014458
      55464D5401445855464D5401445855464D5401445855464D5401445855464D54
      01445855464D540101000000000000000200010000000000000000000000FFFF
      FFFFFFFFFFFFFFFFFFFF010000001200060000000000000000000000FFFFFFFF
      FFFFFFFFFFFFFFFF0200000008000000000000000000FFFFFFFFFFFFFFFFFFFF
      FFFF0300000008000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF04000000
      08000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF05000000080000000000
      00000000FFFFFFFFFFFFFFFFFFFFFFFF0600000008000000000000000000FFFF
      FFFFFFFFFFFFFFFFFFFF0700000008000000000000000000FFFFFFFFFFFFFFFF
      FFFFFFFF0A0001000000}
    object CondNodesCol: TcxTreeListColumn
      Caption.AlignHorz = taRightJustify
      DataBinding.ValueType = 'String'
      MinWidth = 240
      Options.Customizing = False
      Options.Editing = False
      Options.Focusing = False
      Options.Moving = False
      Options.Sorting = False
      Width = 240
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object InclToNameCol: TcxTreeListColumn
      Caption.AlignHorz = taRightJustify
      Caption.Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        0800000000000001000000000000000000000001000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00090909001212
        12001F1F1F002C2C2C003939390045454500525252005F5F5F006C6C6C007878
        780085858500929292009F9F9F00ABABAB00B8B8B800C5C5C500D2D2D200DEDE
        DE00EBEBEB00F8F8F800F0FBFF00A4A0A000C0DCC000F0CAA60000003E000000
        5D0000007C0000009B000000BA000000D9000000F0002424FF004848FF006C6C
        FF009090FF00B4B4FF0000143E00001E5D0000287C0000329B00003CBA000046
        D9000055F000246DFF004885FF006C9DFF0090B5FF00B4CDFF00002A3E00003F
        5D0000547C0000699B00007EBA000093D90000AAF00024B6FF0048C2FF006CCE
        FF0090DAFF00B4E6FF00003E3E00005D5D00007C7C00009B9B0000BABA0000D9
        D90000F0F00024FFFF0048FFFF006CFFFF0090FFFF00B4FFFF00003E2A00005D
        3F00007C5400009B690000BA7E0000D9930000F0AA0024FFB60048FFC2006CFF
        CE0090FFDA00B4FFE600003E1400005D1E00007C2800009B320000BA3C0000D9
        460000F0550024FF6D0048FF85006CFF9D0090FFB500B4FFCD00003E0000005D
        0000007C0000009B000000BA000000D9000000F0000024FF240048FF48006CFF
        6C0090FF9000B4FFB400143E00001E5D0000287C0000329B00003CBA000046D9
        000055F000006DFF240085FF48009DFF6C00B5FF9000CDFFB4002A3E00003F5D
        0000547C0000699B00007EBA000093D90000AAF00000B6FF2400C2FF4800CEFF
        6C00DAFF9000E6FFB4003E3E00005D5D00007C7C00009B9B0000BABA0000D9D9
        0000F0F00000FFFF2400FFFF4800FFFF6C00FFFF9000FFFFB4003E2A00005D3F
        00007C5400009B690000BA7E0000D9930000F0AA0000FFB62400FFC24800FFCE
        6C00FFDA9000FFE6B4003E1400005D1E00007C2800009B320000BA3C0000D946
        0000F0550000FF6D2400FF854800FF9D6C00FFB59000FFCDB4003E0000005D00
        00007C0000009B000000BA000000D9000000F0000000FF242400FF484800FF6C
        6C00FF909000FFB4B4003E0014005D001E007C0028009B003200BA003C00D900
        4600F0005500FF246D00FF488500FF6C9D00FF90B500FFB4CD003E002A005D00
        3F007C0054009B006900BA007E00D9009300F000AA00FF24B600FF48C200FF6C
        CE00FF90DA00FFB4E6003E003E005D005D007C007C009B009B00BA00BA00D900
        D900F000F000FF24FF00FF48FF00FF6CFF00FF90FF00FFB4FF002A003E003F00
        5D0054007C0069009B007E00BA009300D900AA00F000B624FF00C248FF00CE6C
        FF00DA90FF00E6B4FF0014003E001E005D0028007C0032009B003C00BA004600
        D9005500F0006D24FF008548FF009D6CFF00B590FF00CDB4FF000D0D0D0D0D0D
        0D0D0D0D0D0D0D0D0D0D0D0D0D0DA30D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D
        B7B7B7B7B7B7B7B7B70D0D0D0D0DA30DB723232323232323B70D0D0D0D0D0D0D
        B723B5B5B5B5B523B70D0D0D0D0DA30DB723232323232323B70D0D0D0D0DA30D
        B723237474747423B70D0D0D0DA3A3A3B723237474232323B70D0D0D0DA3A3A3
        B723237423742323B70D0D0DA3A3A3A3A323237423237423B70D0DA3A3A3A3A3
        A3A3232323B7B774B70DA3A3A3A3A3A3A3A3A32323B723B7740D0D0D0D0D0D0D
        B723232323B7B70D0D740DA30D0DA30DB7A3B7B7B7B70D0D0D0D0DA30D0DA30D
        0DA30D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D0D}
      Caption.GlyphAlignHorz = taCenter
      Caption.Text = '   '#1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1077' '#1091#1089#1083#1086#1074#1080#1103' '#1074' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1080' '
      DataBinding.ValueType = 'Variant'
      Options.CellEndEllipsis = False
      Options.Sizing = False
      Options.Customizing = False
      Options.IncSearch = False
      Options.Moving = False
      Options.ShowEditButtons = eisbAlways
      Options.Sorting = False
      Width = 22
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
      OnGetEditProperties = InclToNameColGetEditProperties
    end
  end
  object StatusSB: TdxStatusBar
    Left = 0
    Top = 568
    Width = 841
    Height = 20
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 20
      end
      item
        PanelStyleClassName = 'TdxStatusBarContainerPanelStyle'
        PanelStyle.Container = StatusSBContainer7
        MinWidth = 16
        Width = 16
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    object StatusSBContainer7: TdxStatusBarContainerControl
      Left = 24
      Top = 2
      Width = 18
      Height = 18
      object ModifyOffIm: TImage
        Left = 0
        Top = 0
        Width = 16
        Height = 16
        AutoSize = True
        Picture.Data = {
          07544269746D6170F6000000424DF60000000000000076000000280000001000
          0000100000000100040000000000800000000000000000000000100000000000
          0000000000000000800000800000008080008000000080008000808000008080
          8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
          FF00DDDDDDDDDDDDDDDDDD8888888888888DD8FF888888FF8F8DD8FF888888FF
          8F8DD8FF888888FF8F8DD8FF888888888F8DD8FFFFFFFFFFFF8DD8FF88888888
          FF8DD8F8FFFFFFFF8F8DD8F8FFFFFFFF8F8DD8F8FFFFFFFF8F8DD8F8FFFFFFFF
          8F8DD8F8FFFFFFFF888DD8F8FFFFFFFF8F8DD88888888888888DDDDDDDDDDDDD
          DDDD}
        Transparent = True
      end
      object ModifyOnIm: TImage
        Left = 1
        Top = -1
        Width = 16
        Height = 16
        AutoSize = True
        Picture.Data = {
          07544269746D6170F6000000424DF60000000000000076000000280000001000
          0000100000000100040000000000800000000000000000000000100000000000
          0000000000000000800000800000008080008000000080008000808000008080
          8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
          FF00DDDDDDDDDDDDDDDDDD0000000000000DD03300000088030DD03300000088
          030DD03300000088030DD03300000000030DD03333333333330DD03300000000
          330DD03088888888030DD03088888888030DD03088888888030DD03088888888
          030DD03088888888000DD03088888888080DD00000000000000DDDDDDDDDDDDD
          DDDD}
        Transparent = True
      end
    end
  end
  object TreeSpl: TcxSplitter
    Left = 552
    Top = 88
    Width = 8
    Height = 480
    Cursor = crHSplit
    HotZoneClassName = 'TcxSimpleStyle'
    HotZone.SizePercent = 15
    HotZone.ArrowColor = clBtnHighlight
    HotZone.ArrowHighlightColor = clMenuHighlight
    HotZone.LightColor = cl3DLight
    HotZone.DotsColor = clBtnShadow
    AlignSplitter = salRight
    NativeBackground = False
    MinSize = 200
    ResizeUpdate = True
    Control = FNomTV
    Color = clBtnFace
    ParentColor = False
  end
  object FNomTV: TcxTreeList
    Left = 560
    Top = 88
    Width = 281
    Height = 480
    Align = alRight
    Bands = <
      item
      end>
    DragCursor = crCross
    DragMode = dmAutomatic
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Navigator.Buttons.CustomButtons = <>
    OptionsBehavior.DragDropText = True
    OptionsBehavior.ExpandOnDblClick = False
    OptionsSelection.CellSelect = False
    OptionsView.ColumnAutoWidth = True
    OptionsView.GridLines = tlglVert
    OptionsView.Headers = False
    ParentFont = False
    PopupMenu = dxBarPopupMenu2
    TabOrder = 7
    OnDblClick = FNomTVDblClick
    OnFocusedNodeChanged = FRulesTVFocusedNodeChanged
    OnMouseDown = FNomTVMouseDown
    object cxTreeListColumn1: TcxTreeListColumn
      Caption.AlignHorz = taCenter
      DataBinding.ValueType = 'String'
      MinWidth = 240
      Options.Editing = False
      Options.Focusing = False
      Options.Moving = False
      Width = 240
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object MainAL: TActionList
    Images = MainIL
    Left = 106
    Top = 95
    object actPropertues: TAction
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
      Hint = #1055#1072#1088#1072#1084#1077#1090#1088#1099
      ImageIndex = 22
      OnExecute = actPropertuesExecute
    end
    object actApply: TAction
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Hint = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ImageIndex = 15
      OnExecute = actApplyExecute
    end
    object actLoad: TAction
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1079' '#1092#1072#1081#1083#1072
      Hint = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1079' '#1092#1072#1081#1083#1072
      ImageIndex = 16
      ShortCut = 16463
      OnExecute = actLoadExecute
    end
    object actSave: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
      Hint = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
      ImageIndex = 17
      ShortCut = 16467
      OnExecute = actSaveExecute
    end
    object actAddANDGroup: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' ['#1048'] '
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' ['#1048'] '
      ImageIndex = 4
      OnExecute = actAddANDGroupExecute
    end
    object actInsertANDBefore: TAction
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' ['#1048'] '#1087#1077#1088#1077#1076' ...'
      Hint = #1042#1089#1090#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' ['#1048'] '#1087#1077#1088#1077#1076' ...'
      ImageIndex = 5
      OnExecute = actInsertANDBeforeExecute
    end
    object actInsertANDAfter: TAction
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' ['#1048'] '#1087#1086#1089#1083#1077' ...'
      Hint = #1042#1089#1090#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' ['#1048'] '#1087#1086#1089#1083#1077' ...'
      ImageIndex = 6
      OnExecute = actInsertANDAfterExecute
    end
    object actAddORGroup: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' ['#1048#1051#1048']'
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' ['#1048#1051#1048']'
      ImageIndex = 7
      OnExecute = actAddORGroupExecute
    end
    object actInsertORBefore: TAction
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' ['#1048#1051#1048'] '#1087#1077#1088#1077#1076' ...'
      Hint = #1042#1089#1090#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' ['#1048#1051#1048'] '#1087#1077#1088#1077#1076' ...'
      ImageIndex = 8
      OnExecute = actInsertORBeforeExecute
    end
    object actInsertORAfter: TAction
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' ['#1048#1051#1048'] '#1087#1086#1089#1083#1077' ...'
      Hint = #1042#1089#1090#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' ['#1048#1051#1048'] '#1087#1086#1089#1083#1077' ...'
      ImageIndex = 9
      OnExecute = actInsertORAfterExecute
    end
    object actSwapGroupType: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1090#1080#1087' '#1075#1088#1091#1087#1087#1099
      Hint = #1048#1079#1084#1077#1085#1080#1090#1100' '#1090#1080#1087' '#1075#1088#1091#1087#1087#1099
      ImageIndex = 0
      OnExecute = actSwapGroupTypeExecute
    end
    object actSetNOTPref: TAction
      Caption = #1054#1090#1088#1080#1094#1072#1085#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1103
      Hint = #1054#1090#1088#1080#1094#1072#1085#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1103
      ImageIndex = 11
      OnExecute = actSetNOTPrefExecute
    end
    object actAddCondition: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1091#1089#1083#1086#1074#1080#1077
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1091#1089#1083#1086#1074#1080#1077
      ImageIndex = 1
      ShortCut = 45
      OnExecute = actAddConditionExecute
    end
    object actInsertCondBefore: TAction
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1091#1089#1083#1086#1074#1080#1077' '#1087#1077#1088#1077#1076' ...'
      Hint = #1042#1089#1090#1072#1074#1080#1090#1100' '#1091#1089#1083#1086#1074#1080#1077' '#1087#1077#1088#1077#1076' ...'
      ImageIndex = 3
      OnExecute = actInsertCondBeforeExecute
    end
    object actInsertCondAfter: TAction
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1091#1089#1083#1086#1074#1080#1077' '#1087#1086#1089#1083#1077' ...'
      Hint = #1042#1089#1090#1072#1074#1080#1090#1100' '#1091#1089#1083#1086#1074#1080#1077' '#1087#1086#1089#1083#1077' ...'
      ImageIndex = 2
      OnExecute = actInsertCondAfterExecute
    end
    object actDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' ...'
      Hint = #1059#1076#1072#1083#1080#1090#1100' ...'
      ImageIndex = 10
      ShortCut = 46
      OnExecute = actDeleteExecute
    end
    object actSetValue: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1077
      Hint = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1077
      ImageIndex = 12
      ShortCut = 8205
      OnExecute = actSetValueExecute
    end
    object actResetValue: TAction
      Caption = #1057#1073#1088#1086#1089#1080#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1077
      Hint = #1057#1073#1088#1086#1089#1080#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1077
      ImageIndex = 13
      ShortCut = 8238
      OnExecute = actResetValueExecute
    end
    object actEditComment: TAction
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1082#1086#1084#1084#1077#1085#1090#1072#1088#1080#1080
      Hint = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1082#1086#1084#1084#1077#1085#1090#1072#1088#1080#1080
      ImageIndex = 14
      OnExecute = actEditCommentExecute
    end
    object actCheck: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1080#1090#1100
      Hint = #1055#1088#1086#1074#1077#1088#1080#1090#1100
      ImageIndex = 21
      OnExecute = actCheckExecute
    end
    object actCopy: TAction
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      Hint = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 18
      ShortCut = 16451
      Visible = False
      OnExecute = actCopyExecute
    end
    object actCut: TAction
      Caption = #1042#1099#1088#1077#1079#1072#1090#1100
      Hint = #1042#1099#1088#1077#1079#1072#1090#1100
      ImageIndex = 19
      ShortCut = 16472
      Visible = False
      OnExecute = actCutExecute
    end
    object actPaste: TAction
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100
      Hint = #1042#1089#1090#1072#1074#1080#1090#1100
      ImageIndex = 20
      ShortCut = 16470
      Visible = False
      OnExecute = actPasteExecute
    end
    object actClose: TAction
      Caption = 'actClose'
      ShortCut = 27
      OnExecute = actCloseExecute
    end
    object actLoadFromLib: TAction
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1079' '#1073#1080#1073#1083#1080#1086#1090#1077#1082#1080' '#1092#1080#1083#1100#1090#1088#1086#1074
      Hint = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1079' '#1073#1080#1073#1083#1080#1086#1090#1077#1082#1080' '#1092#1080#1083#1100#1090#1088#1086#1074
      ImageIndex = 24
      OnExecute = actLoadFromLibExecute
    end
    object actChangeMode: TAction
      AutoCheck = True
      Caption = #1056#1077#1078#1080#1084' '#1092#1080#1083#1100#1090#1088#1072
      Hint = #1056#1077#1078#1080#1084' '#1092#1080#1083#1100#1090#1088#1072
      ImageIndex = 25
      OnExecute = actChangeModeExecute
    end
  end
  object Style: TcxStyleRepository
    Left = 77
    Top = 208
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Microsoft Sans Serif'
      Font.Style = []
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor]
      Color = clWindow
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor]
      Color = 16775675
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor]
      Color = clGradientActiveCaption
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor]
      Color = clRed
    end
    object bStyle: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object grStyle: TcxStyle
      AssignedValues = [svTextColor]
      TextColor = clGrayText
    end
  end
  object MainBM: TdxBarManager
    AutoDockColor = False
    AutoHideEmptyBars = True
    Scaled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      #1054#1073#1097#1077#1077
      #1056#1077#1076#1072#1082#1090#1086#1088#1086#1074#1072#1085#1080#1077' '#1076#1077#1088#1077#1074#1072' '#1091#1089#1083#1086#1074#1080#1081
      #1050#1086#1087#1080#1088#1086#1074#1072#1085#1080#1077'/'#1074#1089#1090#1072#1074#1082#1072
      #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1079#1085#1072#1095#1077#1085#1080#1103)
    Categories.ItemsVisibles = (
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True)
    DockColor = clBtnFace
    ImageOptions.ImageListBkColor = clBtnFace
    ImageOptions.Images = MainIL
    ImageOptions.LargeImages = MainLIL
    ImageOptions.LargeIcons = True
    ImageOptions.MakeDisabledImagesFaded = True
    ImageOptions.StretchGlyphs = False
    ImageOptions.UseLargeImagesForLargeIcons = True
    MenuAnimations = maFade
    MenusShowRecentItemsFirst = False
    PopupMenuLinks = <
      item
      end
      item
      end
      item
      end>
    ShowShortCutInHint = True
    UseFullReset = True
    UseSystemFont = True
    Left = 235
    Top = 101
    DockControlHeights = (
      0
      0
      88
      0)
    object MainBMBar1: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      Caption = #1054#1073#1097#1077#1077
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 0
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton13'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton31'
        end
        item
          Visible = True
          ItemName = 'dxBarButton12'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarButton23'
        end
        item
          Visible = True
          ItemName = 'dxBarButton24'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton14'
        end
        item
          Visible = True
          ItemName = 'dxBarButton7'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton8'
        end
        item
          Visible = True
          ItemName = 'dxBarButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton10'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton11'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton32'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton1'
        end>
      NotDocking = [dsNone, dsRight, dsBottom]
      OldName = 'Custom 1'
      OneOnRow = True
      RotateWhenVertical = False
      Row = 0
      SizeGrip = False
      UseOwnFont = False
      UseRecentItems = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object MainBMBar2: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 44
      DockingStyle = dsTop
      FloatLeft = 0
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarButton20'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton16'
        end
        item
          Visible = True
          ItemName = 'dxBarButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarButton21'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton4'
        end>
      NotDocking = [dsNone, dsRight, dsBottom]
      OldName = 'Custom 2'
      OneOnRow = True
      RotateWhenVertical = False
      Row = 1
      SizeGrip = False
      UseOwnFont = False
      UseRecentItems = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object dxBarButton13: TdxBarButton
      Action = actApply
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = actLoad
      Category = 0
    end
    object dxBarButton12: TdxBarButton
      Action = actSave
      Category = 0
    end
    object dxBarButton11: TdxBarButton
      Action = actCheck
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Action = actPropertues
      Category = 0
      ShortCut = 120
    end
    object dxBarButton17: TdxBarButton
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1088#1072#1079#1084#1077#1088' '#1082#1085#1086#1087#1086#1082
      Category = 0
      Hint = #1048#1079#1084#1077#1085#1080#1090#1100' '#1088#1072#1079#1084#1077#1088' '#1082#1085#1086#1087#1086#1082
      Visible = ivAlways
      AllowAllUp = True
      ButtonStyle = bsChecked
      ImageIndex = 23
    end
    object dxBarButton31: TdxBarButton
      Action = actLoadFromLib
      Category = 0
    end
    object dxBarButton32: TdxBarButton
      Action = actChangeMode
      Category = 0
      AllowAllUp = True
      ButtonStyle = bsChecked
    end
    object dxBarButton3: TdxBarButton
      Action = actAddCondition
      Category = 1
    end
    object dxBarButton15: TdxBarButton
      Action = actInsertCondBefore
      Category = 1
      PaintStyle = psCaptionInMenu
    end
    object dxBarButton16: TdxBarButton
      Action = actInsertCondAfter
      Category = 1
      PaintStyle = psCaptionInMenu
    end
    object dxBarButton5: TdxBarButton
      Action = actAddANDGroup
      Category = 1
    end
    object dxBarButton18: TdxBarButton
      Action = actInsertANDBefore
      Category = 1
    end
    object dxBarButton19: TdxBarButton
      Action = actInsertANDAfter
      Category = 1
    end
    object dxBarButton6: TdxBarButton
      Action = actAddORGroup
      Category = 1
    end
    object dxBarButton20: TdxBarButton
      Action = actInsertORBefore
      Category = 1
    end
    object dxBarButton21: TdxBarButton
      Action = actInsertORAfter
      Category = 1
    end
    object dxBarButton4: TdxBarButton
      Action = actDelete
      Category = 1
    end
    object dxBarButton22: TdxBarButton
      Action = actCopy
      Category = 2
    end
    object dxBarButton23: TdxBarButton
      Action = actCut
      Category = 2
    end
    object dxBarButton24: TdxBarButton
      Action = actPaste
      Category = 2
    end
    object dxBarButton7: TdxBarButton
      Action = actSetNOTPref
      Category = 3
    end
    object dxBarButton14: TdxBarButton
      Action = actSwapGroupType
      Category = 3
    end
    object dxBarButton10: TdxBarButton
      Action = actEditComment
      Category = 3
    end
    object dxBarButton8: TdxBarButton
      Action = actSetValue
      Category = 3
    end
    object dxBarButton9: TdxBarButton
      Action = actResetValue
      Category = 3
    end
  end
  object EditPM: TdxBarPopupMenu
    BarManager = MainBM
    Images = MainIL
    ItemLinks = <
      item
        Visible = True
        ItemName = 'dxBarButton3'
      end
      item
        Visible = True
        ItemName = 'dxBarButton5'
      end
      item
        Visible = True
        ItemName = 'dxBarButton6'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxBarButton15'
      end
      item
        Visible = True
        ItemName = 'dxBarButton18'
      end
      item
        Visible = True
        ItemName = 'dxBarButton20'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxBarButton16'
      end
      item
        Visible = True
        ItemName = 'dxBarButton19'
      end
      item
        Visible = True
        ItemName = 'dxBarButton21'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxBarButton4'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxBarButton22'
      end
      item
        Visible = True
        ItemName = 'dxBarButton23'
      end
      item
        Visible = True
        ItemName = 'dxBarButton24'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxBarButton14'
      end
      item
        Visible = True
        ItemName = 'dxBarButton7'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxBarButton8'
      end
      item
        Visible = True
        ItemName = 'dxBarButton9'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxBarButton10'
      end>
    UseOwnFont = False
    Left = 174
    Top = 246
  end
  object dxBarPopupMenu2: TdxBarPopupMenu
    BarManager = MainBM
    ItemLinks = <
      item
        Visible = True
        ItemName = 'dxBarButton3'
      end>
    UseOwnFont = False
    Left = 387
    Top = 112
  end
  object MainIL: TcxImageList
    DrawingStyle = dsTransparent
    FormatVersion = 1
    DesignInfo = 9765098
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000090FF900000000000000000000000FF000000FF00000000000000
          0000000000000000000000000000000000000000FF000000FF00000000000000
          0000000000000000000000800000000000000000FF0000000000000000000000
          000000000000000000000000000000000000000000000000FF0000000000B4FF
          B400008000000080000000800000008000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000080
          0000B4FFB400000000000080000000000000FF00000000000000FF0000000000
          0000FF00000000000000FF000000FF00000000000000FF000000000000000080
          00000000000090FF90000000000000000000FF000000FF000000FF0000000000
          0000FF00000000000000FF000000FF000000FF000000FF000000B4FFB4000080
          000000000000000000000000000000000000FF00000000000000FF0000000000
          000000000000FF000000FF000000FF00000000000000FF000000B4FFB4000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000FF000000
          FF00000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF000000FF000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000FF000000FF00000000000000
          000000000000FF0000000000000000000000FF00000000000000000000000000
          00000000000000000000000000000000000000000000B4FFB400000000000000
          000000000000FF000000FF00000000000000FF00000000000000000000000000
          0000000000000000000090FF90000000000000800000B4FFB400000000000000
          000000000000FF00000000000000FF000000FF00000000000000000000000000
          0000000000000080000000000000B4FFB4000080000000000000000000000000
          000000000000FF0000000000000000000000FF00000000000000000000000000
          000000800000008000000080000000800000B4FFB40000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000800000000000000000000000000000000000000000FF000000
          0000000000000000000000000000000000000000000000000000000000000000
          FF00000000000000000090FF90000000000000000000000000000000FF000000
          FF000000000000000000000000000000000000000000000000000000FF000000
          FF00000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00ECFC0000F5FE000083FF000095520000AC5000003D6200007FFF00003FFE
          00007FFC0000EDFE0000E5F40000E9E90000EDC10000FFEF00007FB700003F3F
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C54000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000007C540000000000007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000000000
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF007C540000000000000000
          00007C540000000000000000000000000000000000007C540000000000007C54
          000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF007C540000000000000000
          00007C5400000000000000000000000000007C54000000000000000000007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000000000
          00007C5400000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000000000
          00000000000000000000000000000000000000000000000000007C54000000FF
          000000FF000000FF000000FF000000FF000000FF00007C540000000000000000
          000000000000FF000000FF0000000000000000000000000000007C54000000FF
          000000FF000000FF000000FF000000FF000000FF00007C540000000000000000
          000000000000FF000000FF0000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000000000
          000000000000FF000000FF000000000000000000000000000000000000000000
          00007C5400000000000000000000000000000000000000000000FF000000FF00
          0000FF000000FF000000FF000000FF000000FF000000FF000000000000000000
          00007C5400000000000000000000000000000000000000000000FF000000FF00
          0000FF000000FF000000FF000000FF000000FF000000FF000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000FF000000FF000000000000000000000000000000000000000000
          00007C5400000000000000000000000000000000000000000000000000000000
          000000000000FF000000FF000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000FF000000FF000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FDFF0000FE800000C0000000DE800000DD800000DFFF000000FF
          000000E7000000E7000000E70000DF000000DF000000FFE70000DFE70000FFE7
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          00000000000000000000000000007C5400000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000007C540000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C54000000000000000000007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C54000000FF
          FF0000FFFF0000FFFF0000FFFF0000FFFF007C54000000000000000000007C54
          0000000000000000000000000000000000007C540000000000007C54000000FF
          FF0000FFFF0000FFFF0000FFFF0000FFFF007C54000000000000000000007C54
          00000000000000000000000000007C54000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C54000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C54000000FF
          000000FF000000FF000000FF000000FF000000FF00007C540000000000007C54
          00000000000000000000000000000000000000000000000000007C54000000FF
          000000FF000000FF000000FF000000FF000000FF00007C540000000000007C54
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000BFFF0000FFFF0000BBFF0000BD01000080010000BD010000BB01
          0000BFFF0000BF00000080000000BF000000BF000000FFFF0000BFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C54000000FF
          000000FF000000FF000000FF000000FF000000FF00007C540000000000007C54
          00000000000000000000000000000000000000000000000000007C54000000FF
          000000FF000000FF000000FF000000FF000000FF00007C540000000000007C54
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000007C54
          00000000000000000000000000007C5400000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000007C540000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C54000000000000000000007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C54000000FF
          FF0000FFFF0000FFFF0000FFFF0000FFFF007C54000000000000000000007C54
          0000000000000000000000000000000000007C540000000000007C54000000FF
          FF0000FFFF0000FFFF0000FFFF0000FFFF007C54000000000000000000007C54
          00000000000000000000000000007C54000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C54000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000BFFF0000FFFF0000BFFF0000BF00000080000000BF000000BF00
          0000BBFF0000BD01000080010000BD010000BB010000FFFF0000BFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          FF000000FF000000FF0000000000000000000000000000000000000000000000
          00000000000000000000000000000000FF000000FF000000FF00000000000000
          FF00000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          FF00000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C54000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C5400007C540000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C540000000000007C5400007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C54000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C54000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C54000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          FF00000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          FF00000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          FF000000FF000000FF0000000000000000000000000000000000000000000000
          00000000000000000000000000000000FF000000FF000000FF00}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF008FF80000BFFE0000BFFE0000FFFF0000FFFF0000FDBF0000FCBF0000FD3F
          0000FDBF0000FDBF0000FDBF0000FFFF0000FFFF0000BFFE0000BFFE00008FF8
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C54000000FF
          000000FF000000FF000000FF000000FF000000FF00007C540000000000007C54
          00000000000000000000000000000000000000000000000000007C54000000FF
          000000FF000000FF000000FF000000FF000000FF00007C540000000000007C54
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000FF000000FF00000000000000
          0000000000000000000000000000000000000000FF000000FF00000000007C54
          0000000000007C54000000000000000000000000FF0000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000007C54
          000000000000000000007C54000000000000000000000000000000000000FF00
          00000000000000000000FF000000000000000000000000000000000000007C54
          00007C5400007C5400007C5400007C540000000000000000000000000000FF00
          0000FF00000000000000FF000000000000000000000000000000000000007C54
          000000000000000000007C54000000000000000000000000000000000000FF00
          000000000000FF000000FF000000000000000000000000000000000000007C54
          0000000000007C5400000000000000000000000000000000000000000000FF00
          00000000000000000000FF000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000FF0000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          0000000000000000000000000000000000000000FF000000FF00000000000000
          0000000000000000000000000000000000000000FF000000FF00}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000BFFF0000FF00000080000000BF000000BF000000BFFF0000BCFC
          0000ADFE0000B7B7000083970000B7A70000AFB70000FFFF0000BDFE0000FCFC
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000FF000000FF00000000000000
          0000000000000000000000000000000000000000FF000000FF00000000007C54
          0000000000000000000000000000000000000000FF0000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          000000000000000000000000000000000000000000000000000000000000FF00
          00000000000000000000FF000000000000000000000000000000000000007C54
          0000000000007C5400000000000000000000000000000000000000000000FF00
          0000FF00000000000000FF000000000000000000000000000000000000007C54
          000000000000000000007C54000000000000000000000000000000000000FF00
          000000000000FF000000FF000000000000000000000000000000000000007C54
          00007C5400007C5400007C5400007C540000000000000000000000000000FF00
          00000000000000000000FF000000000000000000000000000000000000007C54
          000000000000000000007C540000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000007C54000000000000000000000000FF0000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000007C54
          0000000000000000000000000000000000000000FF000000FF00000000000000
          0000000000000000000000000000000000000000FF000000FF00000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C54000000FF
          000000FF000000FF000000FF000000FF000000FF00007C540000000000007C54
          00000000000000000000000000000000000000000000000000007C54000000FF
          000000FF000000FF000000FF000000FF000000FF00007C540000000000000000
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FCFC0000BDFE0000FFB70000AF970000B7A7000083B70000B7FF0000ADFE
          0000BCFC0000BFFF0000BF00000080000000BF000000FF000000BFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          FF000000FF000000FF0000000000000000000000000000000000000000000000
          00000000000000000000000000000000FF000000FF000000FF00000000000000
          FF00000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          FF00000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          000000000000000000007C540000000000007C54000000000000000000000000
          00007C540000000000007C54000000000000000000007C540000000000007C54
          00007C540000000000007C54000000000000000000007C540000000000000000
          00007C540000000000007C5400007C540000000000007C540000000000007C54
          0000000000007C5400007C54000000000000000000007C540000000000000000
          00007C540000000000007C540000000000007C5400007C540000000000007C54
          000000000000000000007C54000000000000000000007C540000000000000000
          00007C540000000000007C54000000000000000000007C540000000000007C54
          000000000000000000007C54000000000000000000007C540000000000000000
          00007C540000000000007C54000000000000000000007C540000000000007C54
          000000000000000000007C5400000000000000000000000000007C5400007C54
          00007C540000000000007C54000000000000000000007C540000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          FF00000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          FF00000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          FF000000FF000000FF0000000000000000000000000000000000000000000000
          00000000000000000000000000000000FF000000FF000000FF00}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF008FF80000BFFE0000BFFE0000FFFF0000FFFF0000B5D6000096D20000A6D4
          0000B6D60000B6D60000B7160000FFFF0000FFFF0000BFFE0000BFFE00008FF8
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C54000000FF
          000000FF000000FF000000FF000000FF000000FF00007C540000000000007C54
          00000000000000000000000000000000000000000000000000007C54000000FF
          000000FF000000FF000000FF000000FF000000FF00007C540000000000007C54
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000FF000000FF00000000000000
          0000000000000000000000000000000000000000FF000000FF00000000007C54
          0000000000007C54000000000000000000000000FF0000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000007C54
          000000000000000000007C540000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          00007C5400007C5400007C5400007C540000FF00000000000000FF0000000000
          0000FF00000000000000FF000000FF00000000000000FF000000000000007C54
          000000000000000000007C54000000000000FF000000FF000000FF0000000000
          0000FF00000000000000FF000000FF000000FF000000FF000000000000007C54
          0000000000007C5400000000000000000000FF00000000000000FF0000000000
          000000000000FF000000FF000000FF00000000000000FF000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000FF0000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          0000000000000000000000000000000000000000FF000000FF00000000000000
          0000000000000000000000000000000000000000FF000000FF00}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000BFFF0000FF00000080000000BF000000BF000000BFFF0000BCFC
          0000ADFE0000B7FF000081520000B4500000AD620000FFFF0000BDFE0000FCFC
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000FF000000FF00000000000000
          0000000000000000000000000000000000000000FF000000FF00000000007C54
          0000000000000000000000000000000000000000FF0000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000007C5400000000000000000000FF00000000000000FF0000000000
          0000FF00000000000000FF000000FF00000000000000FF000000000000007C54
          000000000000000000007C54000000000000FF000000FF000000FF0000000000
          0000FF00000000000000FF000000FF000000FF000000FF000000000000007C54
          00007C5400007C5400007C5400007C540000FF00000000000000FF0000000000
          000000000000FF000000FF000000FF00000000000000FF000000000000007C54
          000000000000000000007C540000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000007C54000000000000000000000000FF0000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000007C54
          0000000000000000000000000000000000000000FF000000FF00000000000000
          0000000000000000000000000000000000000000FF000000FF00000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C54000000FF
          000000FF000000FF000000FF000000FF000000FF00007C540000000000007C54
          00000000000000000000000000000000000000000000000000007C54000000FF
          000000FF000000FF000000FF000000FF000000FF00007C540000000000000000
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C540000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FCFC0000BDFE0000FFFF0000AD520000B450000081620000B7FF0000ADFE
          0000BCFC0000BFFF0000BF00000080000000BF000000FF000000BFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000FF000000FF00000000000000000000000000000000000000
          0000000000000000FF000000FF00000000000000000000000000000000007C54
          0000000000000000FF000000FF000000FF0000000000000000007C5400007C54
          00000000FF000000FF000000FF007C5400007C54000000000000000000007C54
          00007C5400007C5400000000FF000000FF000000FF007C5400007C5400000000
          FF000000FF000000FF0000FF000000FF00007C54000000000000000000007C54
          00000000000000000000000000000000FF000000FF000000FF000000FF000000
          FF000000FF0000FF000000FF000000FF00007C54000000000000000000007C54
          0000000000000000000000000000000000000000FF000000FF000000FF000000
          FF007C5400007C5400007C5400007C5400007C54000000000000000000007C54
          0000000000000000000000000000000000000000FF000000FF000000FF000000
          FF00000000000000000000000000000000000000000000000000000000007C54
          00000000000000000000000000000000FF000000FF000000FF000000FF000000
          FF000000FF007C5400007C5400007C5400007C54000000000000000000007C54
          00007C5400007C5400000000FF000000FF000000FF007C5400007C5400000000
          FF000000FF000000FF0000FF000000FF00007C54000000000000000000007C54
          0000000000000000FF000000FF000000FF0000000000000000007C54000000FF
          00000000FF000000FF000000FF0000FF00007C54000000000000000000007C54
          0000000000000000FF000000FF000000000000000000000000007C5400007C54
          00007C5400000000FF000000FF007C5400007C54000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00BFFF0000FFFF0000A7E70000A301000080010000B8010000BC010000BC3F
          0000B801000080010000A3010000A7010000BFFF0000BFFF0000FFFF0000BFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          FF000000FF000000FF0000000000000000000000000000000000000000000000
          00000000000000000000000000000000FF000000FF000000FF00000000000000
          FF00000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          FF00000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007C54000000000000000000007C540000000000000000
          00007C5400007C5400007C5400007C5400000000000000000000000000000000
          000000000000000000007C54000000000000000000007C540000000000000000
          00007C5400000000000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C5400007C5400007C540000000000000000
          00007C5400007C5400007C540000000000000000000000000000000000000000
          000000000000000000007C54000000000000000000007C540000000000000000
          00007C5400000000000000000000000000000000000000000000000000000000
          000000000000000000007C54000000000000000000007C540000000000000000
          00007C5400000000000000000000000000000000000000000000000000000000
          000000000000000000007C54000000000000000000007C540000000000000000
          00007C5400007C5400007C5400007C5400000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          FF00000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          FF00000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          FF000000FF000000FF0000000000000000000000000000000000000000000000
          00000000000000000000000000000000FF000000FF000000FF00}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF008FF80000BFFE0000BFFE0000FFFF0000FFFF0000F6C30000F6DF0000F0C7
          0000F6DF0000F6DF0000F6C30000FFFF0000FFFF0000BFFE0000BFFE00008FF8
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000E421C11FF31140CE1190A06980304
          07420000000C0000000200000000000000000000000000000000000000000000
          00000000000000000000000000000000000D663C2BDCB9C7D2FF7889A2FF2441
          82FF051033960000000F00000002000000000000000000000000000000020000
          00090000000E0000000F0000001065656576775D52CB879AB2FFC8E3F5FF1F66
          B6FF2B6BA8FF051236950000000E000000020000000000000000000000088C66
          57C0C38C7AFFC38C79FFC28B78FFCFA697FFBAA9A4FF488BC3FFDEFEFDFF51B4
          E3FF1F68B7FF3173AEFF061538940000000D00000002000000000000000CC591
          7EFFFDFBFAFFFCF8F6FFFBF7F5FFFAF7F4FFF5F3F1FF8EB4D1FF479FD2FFDEFE
          FDFF59BFE9FF216BB9FF367BB3FF07173A920000000C000000020000000CC794
          81FFFEFBFAFFF9F0EAFFF8F0EAFFF8EFE9FFF9F2EDFFF3EEEBFF8EB4CEFF4BA5
          D5FFDEFEFDFF61CAEFFF246FBCFF3B83B9FF08193D900000000A0000000BC997
          86FFFEFCFBFFF9F2EDFFE1C8BDFFAC6C56FFF9F0EAFFF9F3EEFFCEADA2FF8AAB
          C6FF4EAAD7FFDEFEFDFF68D4F4FF2875BEFF3F8BBEFF091B3F8E0000000ACB9C
          8BFFFEFDFCFFFAF3EFFFB0725BFFFAF3EEFFFAF1ECFFF9F1EBFFF9F3EFFFCEAF
          A3FF92B8D3FF51AEDAFFDEFEFDFF6EDDF8FF2C7BC2FF18448BFF00000009CFA0
          8DFFFEFEFDFFF4E8E2FFB3765DFFFBF4F0FFFAF3EFFFFAF3EFFFF9F3EDFFBF8F
          7DFFF2EBE7FF95BBD7FF54B1DCFFDEFEFDFF4FA6D4FF112B4E8800000009D0A3
          93FFFEFEFDFFB77A63FFDABAADFFFBF5F1FFFBF5F0FFFBF5F0FFFAF4EFFFD6B3
          A5FFC08F7DFFF9F7F6FF8AA7BFFF357FBCFF173A59860000000500000008D3A8
          97FFFEFEFEFFF4EAE6FFB77A64FFFCF6F3FFFCF6F3FFFCF4F2FFFBF5F1FFB070
          59FFF2E7E0FFFCFAF9FFE5CEC5FF00000011000000030000000100000007D3AB
          9AFFFFFEFEFFFCF8F6FFBA7E69FFFCF7F5FFFBF6F4FFFBF6F4FFFCF6F3FFB273
          5CFFFBF6F1FFFDFBF9FFD0A493FF0000000C000000000000000000000006D8AE
          9DFFFFFFFEFFFDF9F7FFE8D3CBFFBB7F6AFFFCF8F6FFFCF7F5FFB67862FFE4CE
          C5FFFCF7F3FFFDFCFAFFD2A897FF0000000B000000000000000000000006D8B0
          A0FFFFFFFFFFFDFAF9FFFDFAF8FFFDFAF8FFFDF9F7FFFCF8F7FFFBF8F6FFFBF7
          F6FFFCF7F5FFFEFCFCFFD5AA9AFF0000000B000000000000000000000005D9B3
          A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFEFFFEFEFEFFFFFEFEFFFEFE
          FEFFFEFEFEFFFEFEFDFFD7AE9EFF00000009000000000000000000000003A386
          7AC0DBB5A5FFDAB5A4FFDAB5A4FFDAB4A4FFD9B3A3FFD9B3A3FFD9B3A2FFD9B2
          A2FFD8B2A2FFD8B1A1FFA08277C2000000060000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00020000000900000012000000180000001A0000001A00000018000000100000
          0005000000010000000000000000000000000000000000000000000000020000
          000D3524146A936338E5A56B3AFFA36938FFA16736FF9D6233FB633E20B70805
          022800000006000000010000000000000000000000000000000000000008442F
          1D78C18B59FEE1AC76FFE4C296FFB5793BFFB5793CFFB5793CFFAD7239FF7E50
          2AD80302042A00000006000000010000000000000000000000000000000DB07D
          4EF3E6B17AFFE9B47DFFE9B47DFFE7C79DFFB67A3DFFB57A3DFFB57A3DFF6953
          7BFF090E5ED50001052800000006000000010000000000000000000000086A4E
          329DEFD7B3FFE9B47DFFE9B47DFFE9B47DFFEACDA4FFB57B3EFF735C86FF313F
          CCFF2935B8FF0B1161D501010627000000050000000100000000000000010000
          000C745538A5F2DDBBFFE9B47DFFE9B47DFFE9B47DFFD1CEE1FF3443CEFF3443
          CDFF3443CEFF2C39BAFF0D1463D4010106260000000500000001000000000000
          00020000000B76583BA4F5E2C1FFE9B47DFFB5A9B8FF829FF1FFB1C9F5FF3949
          D1FF3A4AD1FF3A49D1FF303FBDFF111767D30101062500000005000000000000
          0000000000010000000B785B3DA3E9E1D2FF87A3F2FF87A4F1FF87A3F2FFB9D0
          F7FF3E50D5FF3E50D5FF3F50D5FF3545C2FF141B6AD201010622000000000000
          000000000000000000010000000A2C386FA2C9E2F9FF8CA8F3FF8DA8F3FF8CA8
          F3FFC0D8F9FF4457D9FF4356D9FF4456D9FF3949C2FF141A61C2000000000000
          000000000000000000000000000100000009303D74A1CFE7FBFF92ADF3FF91AD
          F4FF92ADF4FFC6DEFAFF495EDBFF495DDCFF475AD7FF232F8BF0000000000000
          00000000000000000000000000000000000100000008334177A0D4ECFCFF97B2
          F5FF97B2F4FF97B3F5FFCCE4FBFF4A5FDAFF3141A4F6090C214A000000000000
          000000000000000000000000000000000000000000010000000736457A9FD8F0
          FDFF9DB7F5FF9CB7F5FFD9F1FEFF6B81CAF50B0E234700000006000000000000
          0000000000000000000000000000000000000000000000000001000000063947
          7D9EDBF3FEFFDBF3FFFF677FCFF513192C440000000500000001000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          00053543728E4F63AACD151A2D40000000040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000030000000400000002000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00310000003400000036000000380000003B0000003D00000040000000430000
          0044000000470000004A0000004C000000500000005200000000000000000000
          001400000016000000190000001B0000001D0000001E00000021000000230000
          0026000000280000002A0000002D0000002F0000003200000000000000000000
          00030000000400000005000000060000000700000008000000090000000B0000
          000C0000000D0000000F00000011000000130000001500000000000000000000
          0000000000000000000000000000000000001721AAFF0E1385FF0505338B0000
          00150000000C0000000300000000000000000000000000000000000000000000
          0000000000000000000000000000000000002D43D4FF445FF4FF503A31FF4934
          2CFF221814990000001200000003000000000000000000000000000000040000
          00090000000B0000000C0000000C0000000C19256F8B5D463CFF78594DFF7151
          45FF45396CFF04062EA000000016000000030000000000000000122533553776
          A2EA3C81B2FF397FB0FF377EB0FF7FACCCFFC2D3DEFF654D41FFA39596FF6C5D
          99FF5E61E3FF242792FF202858CB0000001500000002000000003F81AEF37BB7
          D8FF87D3F2FF83D1F0FF7ECEF0FF7ACCEFFFD8ECF5FF999594FF7E7EA6FF9EA7
          F2FF686CE6FF696CE6FF282B98FF070A389C0000000F00000002478BBAFFB7E6
          F7FF8ED7F3FF818F92FF786D63FF73675EFFADDEF5FFD2CFCDFF6E71AAFF6E78
          C6FFA7B1F4FF7279E9FF7278E9FF2B309EFF0A0D3F990000000E4B8EBCFFBFE9
          F9FF97DBF4FF80776CFF8ED7F3FF786E64FF87D3F2FFA49D96FFD9ECF4FF7478
          B3FF747FCEFFB0BAF6FF7D85ECFF7D83ECFF3238A4FF0A0E3E8C4E93BFFFC7ED
          FAFF9DDEF5FF8B9898FF80776DFF7B7268FF8ED7F3FF74685EFFB4E3F5FFD4D1
          CFFF7E8DCDFF7B86D5FFBAC5F8FF8990EFFF8D95EBFF181F85F05296C1FFCFF0
          FBFFA5E2F6FFA1E0F6FF9FDEF5FF80766CFF96DBF4FF796E63FF74675EFFAAB2
          B4FFDDEFF6FF8492D4FF7B86D8FFC8D5FAFFA7B3EBFF171F7CCC5599C4FFD6F3
          FBFFBEEBFAFF93A1A2FF867D74FF92AEB5FF9FDEF5FF7C7268FF96DBF4FF93D9
          F4FFBAE7F8FFDEF0F8FF8897D5FF5F69C9F4333C99CD0406162B5A9CC6FF9CC9
          E1FFD7F3FBFFD3F1FBFFCEF0FBFFC9EEFAFFC5ECF9FFACC7CFFFBAE8F8FFB5E5
          F8FFAFE3F7FFA6CFE5FFC6DAE8FF0000000A000000040000000217273144487C
          9DCC599BC5FF569AC4FF5498C3FF5296C1FF4F94C0FF4D91BEFF4B90BCFF498D
          BBFF478CBAFF39749AD910212D47000000020000000000000000000000010000
          0003000000040000000400000004000000040000000500000005000000050000
          0005000000050000000400000002000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000002000000070000000C0000001000000012000000110000
          000E000000080000000200000000000000000000000000000000000000000000
          000100000004000101120D2A1D79184E36C6216B4BFF216B4BFF216C4BFF1A53
          3AD20F2F21840001011500000005000000010000000000000000000000000000
          0005050F0A351C5B40DC24805CFF29AC7EFF2CC592FF2DC894FF2DC693FF2AAE
          80FF258560FF1A563DD405110C3D00000007000000010000000000000003040E
          0A31206548ED299D74FF2FC896FF2EC996FF56D4ACFF68DAB5FF3BCD9DFF30C9
          96FF32CA99FF2BA479FF227050F805110C3D00000005000000000000000A1A57
          3DD02EA57CFF33CA99FF2EC896FF4CD2A8FF20835CFF00673BFF45BE96FF31CB
          99FF31CB98FF34CC9CFF31AD83FF1B5C41D300010113000000020B23185E2E8A
          66FF3BCD9EFF30CA97FF4BD3A9FF349571FF87AF9DFFB1CFC1FF238A60FF45D3
          A8FF36CF9FFF33CD9BFF3ED0A3FF319470FF0F32237F00000007184D37B63DB3
          8CFF39CD9FFF4BD5A9FF43A382FF699782FFF8F1EEFFF9F3EEFF357F5DFF56C4
          A1FF43D5A8FF3ED3A4FF3CD1A4FF41BC95FF1B5C43CD0000000B1C6446DF4BCA
          A4FF44D2A8FF4FB392FF4E826AFFF0E9E6FFC0C3B5FFEFE3DDFFCEDDD4FF1B75
          4FFF60DCB8FF48D8ACFF47D6AAFF51D4ACFF247A58F80000000E217050F266D9
          B8FF46D3A8FF0B6741FFD2D2CBFF6A8F77FF116B43FF73967EFFF1E8E3FF72A2
          8BFF46A685FF5EDFBAFF4CD9AFFF6BE2C2FF278460FF020604191E684ADC78D9
          BEFF52DAB1FF3DBA92FF096941FF2F9C76FF57DEB8FF2D9973FF73967EFFF0EA
          E7FF4F886CFF5ABB9AFF5BDEB9FF7FE2C7FF27835FF80000000C19523BAB77C8
          B0FF62E0BCFF56DDB7FF59DFBAFF5CE1BDFF5EE2BEFF5FE4C1FF288C67FF698E
          76FFE6E1DCFF176B47FF5FD8B4FF83D5BDFF1E674CC60000000909201747439C
          7BFF95ECD6FF5ADFBAFF5EE2BDFF61E4BFFF64E6C1FF67E6C5FF67E8C7FF39A1
          7EFF1F6D4AFF288B64FF98EFD9FF4DAC8CFF1036286D00000004000000041C5F
          46B578C6ADFF9AEED9FF65E5C0FF64E7C3FF69E7C6FF6BE8C8FF6CE9C9FF6BEA
          C9FF5ED6B6FF97EDD7FF86D3BBFF237759D20102010C0000000100000001030A
          0718247B5BDA70C1A8FFB5F2E3FF98F0DAFF85EDD4FF75EBCEFF88EFD6FF9CF2
          DDFFBAF4E7FF78CDB3FF2A906DEA0615102E0000000200000000000000000000
          0001030A07171E694FB844AB87FF85D2BBFFA8E6D6FFC5F4EBFFABE9D8FF89D8
          C1FF4BB692FF237F60CB05130E27000000030000000000000000000000000000
          000000000001000000030A241B411B60489D258464CF2C9D77EE258867CF1F71
          56B00E3226560000000600000002000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000090000
          000E000000100000001000000010000000100000001000000011000000110000
          001100000011000000100000000B00000003000000000000000019427ACA245A
          A5FF255CA7FF255BA7FF245AA6FF2459A6FF2358A5FF2358A4FF2356A4FF2256
          A4FF2255A3FF2154A3FF2153A1FF1C468AE303080F2900000002255DA5FF316B
          AEFF6DA6D5FF86CAF0FF46A6E4FF44A3E4FF41A1E3FF3FA0E2FF3C9EE2FF3B9C
          E1FF389BE0FF369AE0FF3498DFFF2C77C1FF10284D8B000000082B68AEFF4984
          BEFF4B8BC5FFB2E3F8FF68BBECFF55B0E8FF52AEE8FF4EACE7FF4CA9E6FF49A8
          E5FF47A6E4FF44A4E4FF41A2E3FF3A92D6FF1C4885D50000000D2F6FB4FF6CA7
          D2FF3F87C4FFAED9F0FF9AD8F5FF66BDEEFF63BBEDFF60B9EBFF5DB6EBFF5BB5
          EAFF57B2EAFF55B0E9FF51AEE7FF4FABE7FF2D69B1FF040B142F3276B9FF8FC7
          E6FF509FD4FF86BCE0FFC5EFFCFF78CAF2FF74C8F1FF72C5F0FF6FC4F0FF6DC2
          EFFF69C0EEFF66BDEEFF63BBEDFF60B9EBFF448BC9FF122D4D81357CBCFFAFE3
          F5FF75C8EDFF59A2D4FFDDF7FDFFDFF8FEFFDDF7FEFFDBF7FEFFD8F5FEFFD4F4
          FDFFD0F2FDFFCCEFFCFFC7EDFBFFC1EBFBFF9ACBE9FF215187CB3882C1FFC7F5
          FEFF97E5FCFF64BAE5FF4D9FD3FF4D9DD2FF4B9BD1FF4A99CFFF4998CFFF4896
          CEFF4694CCFF4592CBFF3073B7FF3072B6FF2F71B5FF2A65A4EA3A88C5FFCDF7
          FEFFA6ECFEFF9CE8FDFF93E4FBFF8EE1FBFF89DFFBFF86DEFAFF81DAFAFF7ED8
          F9FF7BD7F9FF79D6F9FF2A6BB0FF000000140000000A000000073D8EC8FFD0F8
          FEFFAEF0FEFFAAEEFEFFA6EDFEFFA5EBFDFFBBF2FDFFD4F9FEFFD5F9FEFFD3F8
          FEFFD1F8FEFFCEF7FDFF3680BFFF0000000800000000000000003F92CBFFD3F9
          FEFFB6F3FEFFB3F1FDFFB0F1FEFFB8EDFAFF4895CBFF3B8CC6FF3B8AC6FF3A89
          C5FF3A88C5FF3A87C3FF2A6391C20000000500000000000000004197CEFFE2FC
          FEFFE2FCFEFFE1FCFEFFD4F3FAFF458FBFEC040A0E1B00000006000000060000
          000600000006000000060000000400000001000000000000000031739ABF429A
          D0FF4299D0FF4299D0FF4297CFFF153244590000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          0003000000030000000400000003000000020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000040000
          00130000001E0000002000000020000000200000002100000021000000210000
          002200000022000000220000002300000021000000160000000500000012281E
          16CB37291EFF463A31FFBD8150FFBC7E4DFFB97949FFB67646FFB37141FFB06D
          3DFFAD6839FFAB6535FF42362EFF3D3026FF241A13CE00000015000000193B2C
          21FF685C54FF483C34FFE8C28BFFE7C088FFE6BD85FFE5BB81FFE4B87CFFE3B5
          79FFE2B276FFE2B273FF443931FF51433AFF34261DFF0000001E000000183E2F
          24FF6C6057FF4A3F37FFD9B27DFFD8B07BFFD7AE77FFD7AB74FFD6A970FFD5A6
          6DFFD4A56AFFD4A268FF473B33FF5B4F47FF37291EFF0000001D000000164031
          26FF6F645CFF4C4038FFFFFFFFFFF7F1EBFFF7F0EBFFF7F0EBFFF7EFEBFFF6EF
          EAFFF6EFEAFFF6EFE9FF463B34FF5D5249FF3A2C21FF0000001B000000144434
          29FF73675FFF4F443CFFFFFFFFFFF8F2EDFFF8F1EDFFF7F1EDFFF7F0EDFFF8F1
          EBFFF7F0EBFFF7F0ECFF4A4037FF5F534BFF3D2E23FF00000019000000124637
          2CFF776B63FF50453DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF4E433BFF61544CFF403126FF0000001700000010493A
          2FFF796E66FF50453DFF61564EFF60564EFF60554DFF5F544CFF5E544CFF5E53
          4BFF5D524AFF5C5249FF5B5149FF61554DFF433429FF000000150000000E4C3D
          32FF7C706AFF674E44FF654B42FF634A41FF61473FFF5F473EFF5C443CFF5B43
          3AFF594139FF584038FF573F37FF63574FFF46362DFF000000130000000D4E3F
          35FF80746DFF6B5248FFF4ECE6FFE9DACEFFE9D8CDFFE9D8CCFFE9D8CBFFE8D7
          CAFFF3EAE2FFF3E9E2FF5A4139FF645850FF483A2FFF000000110000000B5142
          36FF827770FF70564DFFF9F5F2FFF4EAE4FFF1E6DEFFEBDCD2FFE9D9CCFF4E41
          3DFF60534CFFF3EAE3FF5D453CFF655951FF4C3D32FF0000000F000000095344
          39FF857A73FF755A50FFFAF6F3FFF5EDE7FFF4EDE6FFF4ECE6FFEFE2DAFF493D
          38FF5A4D46FFF4EBE4FF60483FFF655A52FF4F3F34FF0000000D000000075545
          3AFF887D76FF795E54FFFAF6F4FFF5EEE9FFF5EDE7FFF4EDE7FFF4ECE6FF473A
          36FF483D36FFE9D9CDFF644C43FF675A52FF514137FF0000000B000000065748
          3DFF898079FF7C6157FFFAF7F4FFFAF6F4FFFAF6F4FFFAF6F3FFFAF6F3FFFAF5
          F2FFF5EEE9FFF4ECE6FF695046FF82776FFF534439FF00000009000000034235
          2EC058493DFF7F645AFF998178FF967F75FF937A72FF8E786DFF8B7269FF866E
          64FF82695FFF7D645BFF6E544AFF56453BFF3F332BC200000005000000000000
          0002000000030000000400000004000000040000000400000005000000050000
          0005000000050000000500000006000000060000000400000001}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0002000000090000000E0000000F0000000F0000001000000010000000110000
          0011000000100000000B00000003000000000000000000000000000000000000
          00087C5345C0AD725EFFAC725DFFAC715DFFAC6F5BFFAB705CFFAB705CFFAB6E
          5CFFAB6E5AFF7A4E41C30000000B000000000000000000000000000000000000
          000BAF7462FFFDFBF9FFFBF6F2FFFBF5F2FFFAF5F1FFFBF4EFFFF9F3EEFFF9F2
          EEFFFAF2ECFFAC715DFF0000000F000000000000000000000000000000000000
          000BB17964FFFDFBFAFFF7EEE7FFF8EDE7FFF7EDE7FFF7EDE6FFF6ECE5FFF6EC
          E5FFFAF2EEFFAE7260FF01010120010101100101010B00000003000000000000
          000BB37C69FFFDFCFBFFF8EFE8FFF7EEE8FFF7EEE8FFF8EEE7FFF7EEE7FFF7EC
          E6FFFAF3EFFFB07863FFC19D92FFB57D6AFF815A4EC30101010B000000000000
          000AB57F6CFFFEFCFBFFF9F0EAFFF8F0EAFFF8EFE9FFF8EFE8FFF8EEE9FFF8EE
          E7FFFBF5F1FFB27A66FFEBE6E2FFFAF3EDFFB6806DFF0101010F000000000000
          0009B98270FFFEFDFCFFF9F2EDFFF9F2EDFFF9F0EBFFF9F0EAFFF8F0EAFFF8F0
          E9FFFBF6F3FFB37D6AFFE9E1DAFFFAF3EFFFB88170FF01010110000000000000
          0008BB8775FFFEFDFDFFFAF3EFFFFAF4EEFFFAF3EEFFFAF1ECFFF9F1EBFFF9F0
          EBFFFCF8F5FFB6806DFFEAE1DBFFFAF4F0FFB98673FF0101010F000000000000
          0007BF8B78FFFEFEFDFFFBF5F1FFFBF5F0FFFBF4F0FFFAF3EFFFFAF3EFFFF9F3
          EDFFFCF9F7FFBA8471FFECE4DDFFFBF5F2FFBB8876FF0101010E000000000000
          0007C18E7EFFFEFEFDFFFAF5F3FFFBF6F2FFFBF5F1FFFBF5F0FFFBF5F0FFFAF4
          EFFFFDFAF8FFBC8978FFEDE7E0FFFBF6F4FFBC8B7AFF0101010D000000000000
          0006C49382FFFEFEFEFFFBF6F4FFFBF6F4FFFCF6F3FFFCF6F3FFFCF4F2FFFBF5
          F1FFFDFBF9FFBF8C7CFFEFE8E3FFFCF8F5FFBF8E7CFF0101010D000000000000
          0005C49785FFFFFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFDFFFEFDFDFFFEFD
          FDFFFEFDFCFFC2907FFFF0EBE6FFFCF9F7FFC29180FF0101010C000000000000
          0003967265C0C89988FFC99887FFC79887FFC59786FFC79785FFC79784FFC596
          84FFC59683FFCDA79AFFF4EFEAFFFDFAF8FFC49686FF0101010B000000000000
          000100000003000000040000000BD8BBB0FFF8F8F8FFF5F0EFFFF5F0EFFFF5EF
          EDFFF5EFEDFFF7F0EEFFFAF4F1FFFDFBF9FFC7998AFF0000000A000000000000
          0000000000000000000000000005CCA392FFFFFEFEFFFEFEFEFFFEFEFEFFFEFE
          FEFFFEFEFDFFFEFDFDFFFEFDFDFFFEFDFCFFCA9D8DFF00000009000000000000
          00000000000000000000000000039A7B6FC0CEA495FFCFA494FFCDA494FFCCA3
          93FFCDA392FFCDA391FFCCA291FFCCA290FF97776BC200000006}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00030000000E00000018000000180000000F0000000400000000000000000000
          00030000000E00000018000000180000000F0000000400000000000000030402
          011A512408A5833A0BEE81380AEF4F2105A70301001C00000003000000030302
          001A512408A5833A0BEE81380AEF4F2105A70301001C00000003000000095931
          159BB8763BFFF1BF6EFFF1BD6BFFB56F33FF52280DA10000000C00000009552C
          129BB76F34FFEEA457FFEDA354FFB3672CFF52280DA10000000C0000000CA868
          37EEF6CE8FFFC08B57FFD2A26DFFF3C983FF985525F00000001400000010A05E
          2FEFF4B775FFB57A47FFC8905BFFF0B06AFF995425EF0000001000000009B77C
          48EDFAE3BFFFA26638FFAE7545FFF8DEB5FFAC6A38FF0000002A0000002AB777
          44FFF9D4A9FF97592FFFA3663AFFF7CFA0FFA56735EE0000000D000000047251
          3290DFAE7EFFF4DDC1FFF3DDC0FFDEB58BFFCB9C76FF67372BFF603226FFD0A1
          7AFFE1B58AFFF4D5B3FFF3D4B1FFD49F6EFF68462A9300000006000000010504
          030B7356378ECD9762F2D69F69FFE0BA94FFE6CBB4FFF0DED4FF9E796DFFCAAB
          94FFDDB591FFD29964FFC28C5AF36D4E33910504020D00000001000000000000
          0000000000030000000A35241F66AD8577FBF9F4F2FFF2E2D9FFE4CDC4FF9772
          68FF865B4EFB23120D6F00000010000000040000000100000000000000000000
          000000000000000000010000000C5B3B32B2B58B7EFFFAF5F3FFF4E5DCFFB799
          8EFF43241CB40000000F00000001000000000000000000000000000000000000
          000000000000000000031E141142A07567FDDECAC2FFB89388FFF8F3F1FFF3E7
          DFFF8A6054FD150B093F00000003000000000000000000000000000000000000
          0000000000010403020F82584BD8E6D1C9FFEFE5E1FF795144E0885E53DFF6F1
          EEFFDFCFC6FF61382ED70201010E000000010000000000000000000000000000
          0000000000045339318ACCAEA3FFF0E5E1FF734D41C80805041E0A0605197550
          45C5F4EEECFFB99B93FF39201988000000040000000000000000000000000000
          00022219153BB68D7EFBEEE2DDFF68473DB00402021000000002000000010403
          020C644237ACF2EAE7FF92695DFD170D0A390000000200000000000000010403
          030B946C5FD6EBDED9FF5C413894000000070000000200000000000000000000
          00010000000454372E91ECE3E0FF683F34D50301010A00000001000000025E45
          3B85EBDCD6FF4B362E7200000005000000010000000000000000000000000000
          000000000000000000023F2A236FE9DFDCFF3E231D8400000002000000028965
          58BB3D2D27590000000300000001000000000000000000000000000000000000
          000000000000000000000000000131201B57654236BA00000001}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000030000000C00000010000000110000
          0011000000120000001200000013000000130000001000000004000000000000
          00000000000000000000000000000000000BA97563FFA87462FFA77261FFA571
          5FFFA5705EFFA36F5DFFA36D5CFFA26D5BFFA26C5AFF0000000F000000000000
          00000000000000000000000000000000000DAB7866FFFDFBF8FFF7EFE8FFF6EF
          E6FFF6EEE6FFF5EEE5FFF5EDE4FFF5EDE4FFA36D5CFF00000013000000000000
          0000265080B7336CB1FF326BB0FF2E63A6FFAD7C6AFFFDFCFAFFF7F0E9FFF7F0
          E8FFF7EFE7FFF6EFE7FFF6EEE6FFF5EEE5FFA46F5DFF00000012000000000000
          00003976B8FF91D2F4FF66BEEEFF60B5E4FFB07F6DFFFEFDFCFFF7F2EAFFF8F1
          E9FFF7F0E9FFF6F0E8FFF6EFE7FFF6EFE6FFA67260FF00000011000000000000
          00003F7DBCFF9AD6F5FF6CC3F0FF66B8E5FFB28070FFFEFEFDFFF8F3ECFFF8F2
          EBFFF7F1EAFFF7F1E9FFF7F0E8FFF7EFE8FFA87563FF00000010000000000000
          00004384C1FFA4DBF6FF73C7F1FF6CBEE8FFB48473FFFFFFFEFFF9F3EEFFF9F3
          EDFFF9F2ECFFF8F2EBFFF7F1EAFFF7F0E9FFAB7766FF0000000F000000000000
          0000488BC5FFADE1F8FF7ACCF2FF73C3E9FFB68676FFFFFFFFFFF9F5EEFFF9F4
          EEFFF9F3EDFFF8F3ECFFF8F2EBFFF7F2EBFFAC7A6AFF0000000E000000000000
          00004C92CAFFB6E6FAFF81D2F4FF7AC9EBFFB88878FFFFFFFFFFFAF5F0FFF9F5
          EFFFF9F5EFFFF9F4EEFFF9F3EDFFF9F3ECFFAF7E6CFF0000000D000000000000
          00005098CDFFBEE9FAFF88D6F6FF81CEEEFFBB8B7BFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFCFAFFB1816FFF0000000C000000000000
          0000539DD1FFC5EDFBFF8FDBF7FF89D5F2FFBD8D7DFFBB8C7CFFBA8B7BFFB989
          79FFB98977FFB78876FFB68674FFB58373FFB38371FF00000009000000000000
          000056A1D4FFCBF0FCFF95E0F8FF82CBE9FF72B7D9FF71B7D9FF70B7DAFF70B8
          DBFF6FB7DBFF4385BBFF00000009000000090000000800000002000000000000
          000058A5D7FFCBEDF8FF73B2D0FF5792B7FF5893B7FF5995BAFF5B99BDFF62A3
          C9FF69AFD4FF4184BBFF00000000000000000000000000000000000000000000
          000059A8D9FFBCDBE7FF8E7875FF9B7061FF946A5BFF8F6456FF885D50FF7B65
          63FF97BCD3FF488EC4FF00000000000000000000000000000000000000000000
          0000437EA2BD4A90BFFFB48E7FFFF3EDE7FFDEC9B8FFDDC8B5FFDBC4B2FF9770
          60FF3B7BAEFF40799EBD00000000000000000000000000000000000000000000
          00000000000000000000876B60BDB69181FFB69080FFB58E7FFFB28C7DFF7D60
          54BD000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00020000000A0000001000000011000000120000001200000013000000130000
          001300000014000000130000000D000000030000000000000000000000000000
          000981584AC2B27B66FFB27A66FFB27864FFB17965FFB17965FFB17765FFB177
          63FFB07664FFB07664FF7E5548C50000000C0000000000000000000000000000
          000EB57C6BFFFBF6F2FFFBF5F2FFFAF5F1FFFBF4EFFFF9F3EEFFF9F2EEFFFAF2
          ECFFF8F0EBFFF9F0EAFFB17866FF000000120000000000000000000000000000
          000EB77F6EFFFBF7F4FFF8EDE7FF887FB1FF40388EFFC7BDCEFF3E368CFF867C
          ADFFF6EBE4FFF9F1ECFFB37A68FF000000130000000000000000000000000000
          000DB88371FFFCF8F6FFF7EEE8FF5047A9FF6172DCFF35349DFF5D6BD7FF3B34
          8FFFF6ECE5FFFAF2EEFFB57D6BFF000000120000000000000000000000000000
          000CBB8775FFFDF9F7FFF8F0EAFFC9C1D9FF4C4CB9FF7186EAFF4B4AB8FFC9BF
          D6FFF8EDE7FFFAF4EFFFB7816EFF000000110000000000000000000000000000
          000BBE8A78FFFDFBF9FFF9F2EDFF5752AAFF9CA6E6FF5758B5FF9AA4E5FF524E
          A5FFF8EFE8FFFAF5F1FFBA8571FF000000100000000000000000000000000000
          000BC08F7CFFFDFBFAFFFAF4EEFF9C98D0FF605CBCFFC8C1DCFF5F5BBBFF9A94
          CDFFF8F0EAFFFBF6F3FFBB8975FF0000000F0000000000000000000000000000
          000AC39281FFFEFCFBFFFBF5F0FFFBF4F0FFFAF3EFFFFAF3EFFFF9F3EDFFF9F2
          EDFFF9F1EBFFFCF7F4FFBE8B79FF0000000F0000000000000000000000000000
          0009C59685FFFEFDFCFFFBF6F2FFFBF5F1FFDCE1D8FF376851FFDFE0D9FFFAF4
          EEFFFAF3EFFFFDF9F7FFC18E7DFF0000000E0000000000000000000000000000
          0008C89989FFFEFDFDFFFBF6F4FFDDE3DCFF3D8769FF46B28EFF347057FFE0E2
          DAFFFAF5F0FFFDFAF8FFC39382FF0000000D0000000000000000000000000000
          0007CB9E8CFFFEFEFDFFFCF7F5FF438C6EFF8BD4BFFF3E8E6FFF54BC9CFF3776
          5CFFE0E3DCFFFDFBF9FFC69786FF0000000C0000000000000000000000000000
          0007CCA090FFFFFEFEFFFDF9F7FF85B8A3FF479476FFDCE5DEFF429576FF8CD3
          BFFF4B9073FFFDFCFAFFC89B8AFF0000000B0000000000000000000000000000
          0006CEA393FFFFFEFEFFFDFAF8FFFDFAF8FFFDF9F7FFFCF8F7FFDBE7E0FF4394
          74FF8AB9A5FFFEFCFCFFCB9D8DFF0000000B0000000000000000000000000000
          0005D0A696FFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFEFFFEFEFEFFFFFEFEFFFEFE
          FEFFFEFEFEFFFEFEFDFFCDA191FF000000090000000000000000000000000000
          00039C7C71C0D1A897FFD1A897FFD1A797FFD0A696FFD0A696FFD0A695FFD0A5
          95FFCFA595FFCFA494FF98796EC2000000060000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000030000
          000B000000110000000B00000002000000000000000000000000000000000000
          00020000000A000000100000000C0000000300000000000000000000000A171D
          50A529328BFF1419459500000010000000020000000000000000000000030000
          0010362A2699685149FF4A3D38AD0000000B00000000000000000000000D2734
          86F15DB8EAFF255EA0FF06082E9C0000001000000002000000030000000F2616
          11A3A28B7FFFE8DAD0FF7B655DFD0000000E000000000000000000000007151D
          468BB8DFF0FF64C4F1FF2B69ABFF060B339B0000001100000010281812A1987E
          72FFE1CDC1FFB8A79EFF43383498000000080000000000000000000000010000
          000B18245397BAE2F2FF6BC9F3FF2F6FB1FF070E389F221510B29A8175FFE2D1
          C4FFBFAFA6FF4A3F399F0000000C000000020000000000000000000000000000
          00010000000B18285797BCE4F3FF74CFF3FF3676B7FF2A2E57FF9E938CFFBFB0
          A8FF4D413B9E0000000B00000002000000000000000000000000000000010000
          000400000009000000151A2E5B9BBDE5F4FF7CD3F5FF3C7FBFFF313B68FF4037
          32AE0000000B0000000200000002000000080000000C00000006000000062B25
          225D6A5A52CC867168FF4D362DEA606E97FFBEE7F5FF85DAF7FF4284C3FF0D1C
          49930000000800000000000000083D322A90796254FF231D195E2C2522589A86
          7CFFD5CAC2FFEEE4DDFFE4D3C9FFDCCCC2FF6E81ABFFBDE7F5FF9FE0F6FF1F44
          9CFA0102041A0000000D16120F418F7B6FFFF9F7F6FF9B806DFF7A6960D6A695
          8BFF927C71FFAC9B91FFEFE6DFFFE4D6CCFF7D6A61EB1A355D893365B4EFE1D3
          CDFF7F6859FF7F6759FF7E6759FF958175FFAF998AFF4E41388B9F8E84F94C43
          407F0000000940363180A19086FFE8DBD2FF8D796FF90000000A0000000DB198
          89FFE8DFD9FFE5DBD5FFF1ECE9FFA78E7DFF1915133600000006000000030000
          000200000002060504147D685DFFDCD1CBFF7B6A62D40000000500000009B59D
          8FFFEAE2DDFFF4EFECFFCABFB7FF4A3E37870000000500000000000000000000
          000000000002382D287F89746CFFB1A098FF37322E58000000053A2F2887AD9E
          95FFF6F3F0FFCCC0B9FF4C403986000000050000000100000000000000000000
          000000000002AC9B92F9887A73C8312C2A4C000000053A2E2786B9AEA7FFFFFF
          FFFFCEC2BBFF4E423A8400000004000000010000000000000000000000000000
          000000000000000000010000000100000001C0A89CFFC0A89CFFC0A89CFFBFA7
          9BFF4F433C820000000300000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000002000000020000
          0002000000010000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          0000000000000000000000000000000000000000000000000000000000000000
          00007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D99300007C54
          0000000000000000000000000000000000000000000000000000000000000000
          00007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D99300007C54
          00007C5400007C5400007C5400007C5400000000000000000000000000000000
          00007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D99300007C54
          0000F0FBFF00F0FBFF00D99300007C5400000000000000000000000000000000
          00007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D99300007C54
          0000F0FBFF00F0FBFF00D99300007C5400000000000000000000000000000000
          00007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D99300007C54
          0000F0FBFF00F0FBFF00D99300007C5400000000000000000000000000000000
          00007C540000D9930000D9930000D9930000D9930000D9930000D99300007C54
          0000F0FBFF00F0FBFF00D99300007C5400000000000000000000000000000000
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          0000F0FBFF00F0FBFF00D99300007C5400000000000000000000000000000000
          0000000000007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00D99300007C5400000000000000000000000000000000
          0000000000007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00D99300007C5400000000000000000000000000000000
          0000000000007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00D99300007C5400000000000000000000000000000000
          0000000000007C540000D9930000D9930000D9930000D9930000D9930000D993
          0000D9930000D9930000D99300007C5400000000000000000000000000000000
          0000000000007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C5400007C5400007C5400000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000C03F0000C03F0000C0030000C0030000C0030000C0030000C003
          0000C0030000E0030000E0030000E0030000E0030000E0030000FFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000090000
          000E000000100000001000000010000000100000001000000011000000110000
          001100000011000000100000000B00000003000000000000000017417CCA2159
          A8FF225BAAFF225AAAFF2159A9FF2158A9FF2057A8FF2057A7FF2055A7FF1F55
          A7FF1F54A6FF1E53A6FF1E52A4FF1A458DE303080F2900000002225DA8FF2F6B
          B0FF579AD3FF71BEECFF46A6E4FF44A3E4FF41A1E3FF3FA0E2FF3C9EE2FF3B9C
          E1FF389BE0FF369AE0FF3498DFFF2875C1FF0F284E8B000000082868B1FF4884
          BFFF4489C7FF9CD8F5FF63B9EBFF55B0E8FF52AEE8FF4EACE7FF4CA9E6FF49A8
          E5FF47A6E4FF44A4E4FF41A2E3FF3991D7FF1B4787D50000000D2C6FB7FF6CA7
          D2FF3C87C6FFA0D4EFFF94D5F4FF66BDEEFF63BBEDFF60B9EBFF5DB6EBFF5BB5
          EAFF57B2EAFF55B0E9FF51AEE7FF4FABE7FF2967B4FF040B142F2F75BCFF8FC7
          E6FF4FA0D5FF7FBCE2FFC3EEFCFF78CAF2FF74C8F1FF72C5F0FF6FC4F0FF6DC2
          EFFF69C0EEFF66BDEEFF63BBEDFF60B9EBFF408ACAFF112C4E81327CBFFFAFE3
          F5FF75C8EDFF55A3D7FFD2F5FDFFD4F6FEFFD2F4FEFFCDF3FDFFC8F1FDFFC2EE
          FCFFBCEBFBFFB5E7FAFFADE3F9FFA5DFF8FF82C0E6FF1E5189CB3582C4FFC7F5
          FEFF97E5FCFF62BBE7FF4AA0D6FF4A9DD5FF91C3E3FF8DBCDCFF8FBDE0FF5FA3
          D6FF4394CFFF4292CEFF2D73BAFF2D72B9FF2C71B8FF2765A7EA3688C8FFCDF7
          FEFFA6ECFEFF9CE8FDFF93E4FBFF8EE1FBFFBBE6F3FFA68E78FFA6BABDFFB2E1
          F3FFB9E6F7FFB3E0F0FF8EADD1FF000000180000000B00000007398ECBFFD0F8
          FEFFAEF0FEFFAAEEFEFFA6EDFEFFA5EBFDFFD3EDF3FFAD744CFF9A6841FFBEB8
          ADFFD1E1E3FF975D32FF8A6E59FF0C07032E00000007000000013B92CEFFD3F9
          FEFFB6F3FEFFB3F1FDFFB0F1FEFFB8EDFAFF9CC3E0FFAD886CFFDBB891FFB07F
          53FF9B643AFF9F663DFFDBB67EFF8E5D33EF311D0F67000000063D97D1FFE2FC
          FEFFE2FCFEFFE1FCFEFFD3F3FAFF428FC1EC0B111523382A1E58C49870FFEAD2
          B1FFFCF0D0FFFEF2D3FFFEE9BBFFF7E6C5FFA36C43FF000000082E739DBF3E9A
          D3FF3E99D3FF3E99D3FF3E97D2FF143245590000000200000003382C204E9877
          58CABF946DFAC3966DFFEAD3B4FFB7906CEE3D2E215B00000003000000020000
          0003000000030000000400000003000000020000000000000000000000010000
          000200000005CAA27AFF846A50AC110D0A1B0000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000100000001000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000020000000A0000
          000E0000000A000000050000000A0000000F0000000B00000003000000000000
          000000000000000000000000000000000000000000000000000913543BC11974
          52FF12533AC10000001314553BC21B7653FF13543BC20000000A000000000000
          00000000000000000000000000040000000D00000011000000131D7A56FF58D6
          B3FF1C7955FF0000001A1D7957FF55D4B1FF1C7856FF0000000E000000000000
          000000000000000000000000000C4F413BBB755E59FF31282577207E5BFF5ED8
          B8FF1F7D5AFF00000019207D5AFF58D6B3FF1F7C59FF0000000D000000000000
          000000000000000000000000000F8B756CFFEBE8E7FF4F4A4776248461FF9EEC
          DBFF23835FFF0000001622815DFF5CD7B6FF217F5CFF0000000C000000000000
          000000000000000000000000000E877169FFE5E0DDFF5B5450802A6F54DD2889
          66FF1D654BC300000010258461FF5FD9B9FF248360FF0000000B000000000000
          000000000000000000010000000E846E67FFE4DDDAFFA79A93DC3F322F900000
          000E000000060000000A278865FF62DABBFF268663FF00000009000000000000
          000000000001000000061A151449836C64FFDDD5D0FFC2B4ACFF775F59FF1A15
          144A0000000700000008298B68FFA1EDDDFF288A67FF00000007000000000000
          000100000006292220637E6861FB96837BFFB3A49DFF9A877EFF9A857BFF7F69
          62FB28211F630000000B206A4FC12C8E6BFF20684FC100000004000000000000
          000429221F5E86716AFFA89890FFD9D1CCFFCBBEB7FFAFA096FFA39086FFAB97
          8AFF846F68FF27201E6000000008000000040000000300000001000000010504
          041374625CE8AC9A92FFD5CDC7FFF0EDEAFFD7CFC9FFC0B5AEFFA7968DFFAD99
          8CFF9C887EFF725F58E80404031400000001000000000000000000000003322A
          276A94817AFFC2B6AEFFEEEBE8FFFBFAF9FFE0DBD7FFD2C9C3FFB1A29BFFAD99
          8CFFA69387FF8B766FFF3027256C000000040000000000000000000000055A4B
          47B2B5A79FFFE4DFD9FFF9F8F6FFF8F8F5FFF8F8F5FFF8F8F5FFF6F6F3FFE7E3
          DEFFD5CCC4FFA6958DFF564844B300000006000000000000000000000005816E
          67F4EEECE8FFCCC4C0FFAB9D98FF79635CFF77615AFF77615AFF79635CFFAB9D
          98FFCCC4C0FFEBE8E4FF7A665FEF00000006000000000000000000000003433A
          3773A99791F9D8D1CBFFEDEBE7FFF5F5F2FFF8F8F5FFF8F8F5FFF5F4F1FFEAE7
          E3FFD2CAC4FF9E8E86F93D333174000000030000000000000000000000010000
          0003110E0E214A403C7E71625DBE7F6E68D598847DFF97837BFF7C6A64D56C5D
          58BF453C387F110F0E2500000003000000010000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008000000080000000800000000000
          0000000000000000000000000000000000000000000000000000000000008000
          00008000000080000000800000000000000080000000E6B78900800000000000
          0000000000000000000000000000000000000000000000000000000000008000
          0000000000000000000000000000000000008000000080000000800000000000
          0000000000000000000000000000000000000000000000000000000000008000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000008000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000080000000800000008000000000000000000000008000
          0000000000000000000000000000000000000000000080000000800000008000
          0000800000000000000080000000E6B789008000000000000000000000008000
          0000000000000000000000000000000000000000000080000000000000000000
          0000000000000000000080000000800000008000000000000000000000008000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000008000
          0000000000000000000000000000000000008000000080000000800000000000
          0000000000000000000000000000000000000000000000000000000000008000
          00008000000080000000800000000000000080000000E6B78900800000000000
          0000000000000000000000000000000000000000000000000000000000008000
          0000000000000000000000000000000000008000000080000000800000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000FF000000
          FF000000FF000000FF000000FF000000FF000000FF000000FF00800000008000
          00008000000000000000000000000000000000000000000000000000FF000000
          FF000000FF000000FF000000FF000000FF000000FF000000FF0080000000E6B7
          8900800000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000800000008000
          0000800000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FC7F0000847F0000BC7F0000BFFF0000BFF10000BE110000BEF10000BFFF
          0000BC7F0000847F0000BC7F0000FF0000001F0000001FFF00001FFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000FF00FFFFFF0000000000000000000000
          0000000000000000FF00FFFFFF00000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000FF000000FF000000FF00FFFFFF000000000000000000000000000000
          000000000000000000000000FF00FFFFFF000000000000000000000000000000
          00000000FF000000FF000000FF00FFFFFF000000000000000000000000000000
          0000000000000000FF00FFFFFF00000000000000000000000000000000000000
          0000000000000000FF000000FF000000FF00FFFFFF0000000000000000000000
          00000000FF000000FF00FFFFFF00000000000000000000000000000000000000
          000000000000000000000000FF000000FF000000FF00FFFFFF00000000000000
          FF000000FF00FFFFFF0000000000000000000000000000000000000000000000
          00000000000000000000000000000000FF000000FF000000FF000000FF000000
          FF00FFFFFF000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000FF000000FF000000FF00FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000FF000000FF000000FF000000FF000000
          FF00FFFFFF000000000000000000000000000000000000000000000000000000
          000000000000000000000000FF000000FF000000FF00FFFFFF00000000000000
          FF00FFFFFF000000000000000000000000000000000000000000000000000000
          00000000FF000000FF000000FF000000FF00FFFFFF0000000000000000000000
          00000000FF000000FF00FFFFFF00000000000000000000000000000000000000
          FF000000FF000000FF000000FF00FFFFFF000000000000000000000000000000
          0000000000000000FF000000FF00FFFFFF000000000000000000000000000000
          FF000000FF00FFFFFF0000000000000000000000000000000000000000000000
          000000000000000000000000FF000000FF00FFFFFF0000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF0000FFF90000E7FF0000C3F30000C3E70000E1C70000F08F
          0000F81F0000FC3F0000F81F0000F09F0000C1C7000083E300008FF10000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000FF000000FF00
          0000000000000000000000000000000000000000000000000000008080000080
          800000000000000000000000000000000000008080000000000000000000FF00
          0000FF0000000000000000000000000000000000000000000000008080000080
          8000000000000000000000000000000000000080800000000000000000000000
          0000FF000000FF00000000000000000000000000000000000000008080000080
          8000008080000080800000808000008080000080800000000000000000000000
          000000000000FF000000FF000000000000000000000000000000008080000080
          8000000000000000000000000000008080000080800000000000000000000000
          00000000000000000000FF000000FF0000000000000000000000008080000000
          0000000000000000000000000000000000000080800000000000000000000000
          0000000000000000000000000000FF000000FF00000000000000008080000000
          0000000000000000000000000000000000000080800000000000000000000000
          000000000000000000000000000000000000FF00000000000000008080000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000008080000080800000808000008080000080800000000000FF000000FF00
          00000000000000000000000000000000000000000000000000000000000000FF
          FF0000000000008080000080800000808000008080000080800000000000FF00
          0000FF000000000000000000000000000000000000000000000000000000FFFF
          FF0000FFFF000000000000808000008080000080800000808000008080000000
          0000FF000000FF000000000000000000000000000000000000000000000000FF
          FF00FFFFFF0000FFFF0000000000000000000000000000000000000000000000
          000000000000FF000000FF00000000000000000000000000000000000000FFFF
          FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF00000000000000
          00000000000000000000FF000000FF00000000000000000000000000000000FF
          FF00FFFFFF0000FFFF0000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF00000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FF000000FF000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FF0000003E0800009E000000CE000000E6000000F2380000F8380000FC3A
          000000000000003F0000001F0000000F00000007000000730000007900008FFC
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000808080000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000080000000808080008080
          8000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FF000000FF000000800000008080
          8000808080000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FF0000008000
          0000808080008080800000000000000000000000000000000000000000000000
          00000000000000000000FF000000FF000000FF000000FF000000FF000000FF00
          0000800000008080800080808000000000000000000000000000000000000000
          000000000000FF000000FF000000FF000000FF000000FF000000FF0000008000
          0000800000008000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FF0000008000
          0000808080000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FF0000008000
          0000808080000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FF0000008000
          0000808080000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FF0000008000
          0000808080000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FF0000008000
          0000808080000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FF0000008000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF0000FF7F0000FE3F0000FC1F0000F80F0000F0070000E00F
          0000F81F0000F81F0000F81F0000F81F0000F81F0000F83F0000FFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008080800080808000808080008080
          8000808080000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000800000008000000080000000800000008000
          0000808080000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FF0000008000
          0000808080000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FF0000008000
          0000808080000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FF0000008000
          0000808080000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FF0000008000
          0000808080000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FF0000008000
          0000808080008080800080808000000000000000000000000000000000000000
          000000000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
          0000FF000000FF00000000000000000000000000000000000000000000000000
          00000000000000000000FF000000FF000000FF000000FF000000FF000000FF00
          0000FF0000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FF000000FF00
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FF000000FF000000FF0000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF0000FC1F0000F81F0000F81F0000F81F0000F81F0000F81F
          0000F8070000E00F0000F01F0000F83F0000FC7F0000FEFF0000FFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF000000FF0000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF000000FF0000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF000000FF0000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000FF000000FF00
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FF00
          0000FF0000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF0000000000000000000000FF00
          0000FF0000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF0000000000000000000000FF00
          0000FF0000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FF000000FF000000FF000000FF00
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FE7F0000FE7F0000FFFF
          0000FE7F0000FF3F0000FF9F0000F99F0000F99F0000FC3F0000FFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000FF00000000000000000000000000000000000000FF0000000000
          0000000000000000000000000000FF0000000000000000000000000000000000
          0000FF000000FF000000FF0000000000000000000000FF000000FF000000FF00
          00000000000000000000FF000000FF000000FF00000000000000000000000000
          000000000000FF00000000000000000000000000000000000000FF0000000000
          0000000000000000000000000000FF0000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000EF7B
          0000C6310000EF7B0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000006C6CFF0000000000000000000000000000000000FF0000000000
          0000FF000000FF00000000000000FF00000000000000FF00000000000000FF00
          00006C6CFF006C6CFF0000000000000000000000000000000000FF000000FF00
          0000FF00000000000000FF000000FF00000000000000FF000000FF000000FF00
          00006C6CFF006C6CFF0000000000000000000000000000000000FF0000000000
          0000FF00000000000000FF000000FF00000000000000FF0000006C6CFF00FF00
          00006C6CFF006C6CFF006C6CFF006C6CFF000000000000000000FF0000000000
          0000FF00000000000000FF000000FF00000000000000FF0000006C6CFF00FF00
          00006C6CFF006C6CFF006C6CFF006C6CFF006C6CFF0000000000000000000000
          000000000000000000006C6CFF00000000000000000000000000000000006C6C
          FF006C6CFF006C6CFF00000000006C6CFF006C6CFF006C6CFF00000000000000
          000000000000000000006C6CFF00000000000000000000000000000000000000
          00006C6CFF006C6CFF0000000000000000006C6CFF006C6CFF00000000000000
          000000000000000000006C6CFF006C6CFF0000000000000000006C6CFF000000
          0000000000006C6CFF0000000000000000006C6CFF006C6CFF00000000000000
          000000000000000000006C6CFF006C6CFF0000000000000000006C6CFF006C6C
          FF0000000000000000000000000000000000000000006C6CFF00000000000000
          000000000000000000006C6CFF006C6CFF006C6CFF00000000006C6CFF006C6C
          FF006C6CFF00000000000000000000000000000000006C6CFF00000000000000
          00000000000000000000000000006C6CFF006C6CFF006C6CFF006C6CFF006C6C
          FF006C6CFF006C6CFF0000000000000000000000000000000000000000000000
          0000000000000000000000000000000000006C6CFF006C6CFF006C6CFF006C6C
          FF006C6CFF006C6CFF00FF00000000000000FF00000000000000000000000000
          00000000000000000000000000000000000000000000000000006C6CFF006C6C
          FF006C6CFF0000000000FF000000FF000000FF00000000000000000000000000
          00000000000000000000000000000000000000000000000000006C6CFF006C6C
          FF000000000000000000FF00000000000000FF00000000000000000000000000
          00000000000000000000000000000000000000000000000000006C6CFF000000
          00000000000000000000FF00000000000000FF00000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFEF00004A8F0000120F00005203000052010000F7880000F7CC0000F36C
          0000F33E0000F11E0000F80F0000FC050000FF110000FF350000FF750000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000009090FF0000000000000000000000000000000000FF0000000000
          0000FF000000FF00000000000000FF00000000000000FF00000000000000FF00
          00006C6CFF006C6CFF0000000000000000000000000000000000FF000000FF00
          0000FF00000000000000FF000000FF00000000000000FF000000FF0000006C6C
          FF006C6CFF004848FF0000000000000000000000000000000000FF0000000000
          0000FF00000000000000FF000000FF00000000000000FF0000006C6CFF006C6C
          FF004848FF002424FF002424FF004848FF000000000000000000FF0000000000
          0000FF00000000000000FF000000FF00000000000000FF0000006C6CFF004848
          FF002424FF000000FF000000FF002424FF004848FF0000000000000000000000
          00000000000000000000B4B4FF00000000000000000000000000000000004848
          FF002424FF000000FF00000000002424FF004848FF006C6CFF00000000000000
          00000000000000000000B4B4FF00000000000000000000000000000000000000
          00004848FF002424FF0000000000000000006C6CFF006C6CFF00000000000000
          000000000000000000009090FF006C6CFF0000000000000000004848FF000000
          0000000000004848FF0000000000000000006C6CFF009090FF00000000000000
          000000000000000000006C6CFF006C6CFF0000000000000000002424FF004848
          FF000000000000000000000000000000000000000000B4B4FF00000000000000
          000000000000000000006C6CFF004848FF002424FF00000000000000FF002424
          FF004848FF0000000000000000000000000000000000B4B4FF00000000000000
          00000000000000000000000000004848FF002424FF000000FF000000FF002424
          FF004848FF006C6CFF0000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004848FF002424FF002424FF004848
          FF006C6CFF006C6CFF00FF00000000000000FF00000000000000000000000000
          00000000000000000000000000000000000000000000000000004848FF006C6C
          FF006C6CFF0000000000FF000000FF000000FF00000000000000000000000000
          00000000000000000000000000000000000000000000000000006C6CFF006C6C
          FF000000000000000000FF00000000000000FF00000000000000000000000000
          00000000000000000000000000000000000000000000000000009090FF000000
          00000000000000000000FF00000000000000FF00000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFEF00004A8F0000120F00005203000052010000F7880000F7CC0000F36C
          0000F33E0000F11E0000F80F0000FC050000FF110000FF350000FF750000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008000000080000000800000000000
          0000000000000000000000000000000000000000000000000000000000008000
          00008000000080000000800000000000000080000000E6B78900800000000000
          0000000000000000000000000000000000000000000000000000000000008000
          0000000000000000000000000000000000008000000080000000800000000000
          0000000000000000000000000000000000000000000000000000000000008000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000008000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000080000000800000008000000000000000000000008000
          0000000000000000000000000000000000000000000080000000800000008000
          0000800000000000000080000000E6B789008000000000000000000000008000
          0000000000000000000000000000000000000000000080000000000000000000
          0000000000000000000080000000800000008000000000000000000000008000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000008000
          0000000000000000000000000000000000008000000080000000800000000000
          000000000000FF000000FF000000000000000000000000000000000000008000
          00008000000080000000800000000000000080000000E6B78900800000000000
          000000000000FF000000FF000000000000000000000000000000000000008000
          0000000000000000000000000000000000008000000080000000800000000000
          000000000000FF000000FF000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000FF000000FF00
          0000FF000000FF000000FF000000FF000000FF000000FF000000800000008000
          0000800000000000000000000000000000000000000000000000FF000000FF00
          0000FF000000FF000000FF000000FF000000FF000000FF00000080000000E6B7
          8900800000000000000000000000000000000000000000000000000000000000
          000000000000FF000000FF000000000000000000000000000000800000008000
          0000800000000000000000000000000000000000000000000000000000000000
          000000000000FF000000FF000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000FF000000FF000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FC7F0000847F0000BC7F0000BFFF0000BFF10000BE110000BEF10000BFFF
          0000BC67000084670000BC670000FF0000001F0000001FE700001FE70000FFE7
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000000000FF
          000000FF000000FF000000FF000000FF000000FF000000000000000000000000
          00000000000000000000000000000000000000000000000000000000000000FF
          000000FF000000FF000000FF000000FF000000FF000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FF00000000000000FF000000FF00
          000000000000FF00000000000000FF00000000000000FF000000000000000000
          000000000000000000000000000000000000FF000000FF000000FF0000000000
          0000FF000000FF00000000000000FF000000FF000000FF000000000000000000
          000000000000000000000000000000000000FF00000000000000FF0000000000
          0000FF000000FF00000000000000FF00000000000000FF000000000000000000
          000000000000000000000000000000000000FF00000000000000FF0000000000
          0000FF000000FF00000000000000FF00000000000000FF000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FDFF0000FE800000C0000000DE800000DD800000DFFF000000FF
          000000FF000000FF000000FF0000DD2A0000DC480000FD4A0000DD4A0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000000000FF
          000000FF000000FF000000FF000000FF000000FF000000000000000000000000
          00000000000000000000000000000000000000000000000000000000000000FF
          000000FF000000FF000000FF000000FF000000FF000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FF00
          00000000000000000000FF00000000000000FF00000000000000000000000000
          0000FF00000000000000FF0000000000000000000000FF00000000000000FF00
          0000FF00000000000000FF0000000000000000000000FF000000000000000000
          0000FF00000000000000FF000000FF00000000000000FF00000000000000FF00
          000000000000FF000000FF0000000000000000000000FF000000000000000000
          0000FF00000000000000FF00000000000000FF000000FF00000000000000FF00
          00000000000000000000FF0000000000000000000000FF000000000000000000
          0000FF00000000000000FF0000000000000000000000FF00000000000000FF00
          00000000000000000000FF0000000000000000000000FF000000000000000000
          0000FF00000000000000FF0000000000000000000000FF00000000000000FF00
          00000000000000000000FF000000000000000000000000000000FF000000FF00
          0000FF00000000000000FF0000000000000000000000FF000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FDFF0000FE800000C0000000DE800000DD800000DFFF000000FF
          000000FF000000FF000000D6000096D20000A6D40000B6D60000B6D60000B716
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000080000000800000008000000080
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000800000008000000080000000800000008000000080
          0000008000000080000000000000000000000000000000000000000000000000
          0000000000000080000000800000008000000A840A0001800100008000000080
          0000008000000080000000800000000000000000000000000000000000000000
          00000080000000800000008000002D962D00FFFFFF00FFFFFF000A840A000080
          0000008000000080000000800000008000000000000000000000000000000000
          000000800000008000002D962D00FFFFFF00FFFFFF00E3F1E300FFFFFF002D96
          2D00008000000080000000800000008000000000000000000000000000000080
          0000008000002D962D00FFFFFF00FFFFFF0087C3870087C38700FFFFFF00FFFF
          FF002D962D000080000000800000008000000080000000000000000000000080
          00000A840A00FFFFFF00FFFFFF0087C38700008000000080000087C38700FFFF
          FF00FFFFFF002D962D0000800000008000000080000000000000000000000080
          000001800100C5E2C50087C387000080000000800000008000000080000087C3
          8700FFFFFF00FFFFFF002D962D00008000000080000000000000000000000080
          0000008000000080000000800000008000000080000000800000008000000080
          000087C38700FFFFFF00FFFFFF000A840A000080000000000000000000000000
          0000008000000080000000800000008000000080000000800000008000000080
          00000080000087C38700C5E2C500018001000000000000000000000000000000
          0000008000000080000000800000008000000080000000800000008000000080
          0000008000000080000000800000008000000000000000000000000000000000
          0000000000000080000000800000008000000080000000800000008000000080
          0000008000000080000000800000000000000000000000000000000000000000
          0000000000000000000000800000008000000080000000800000008000000080
          0000008000000080000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000080000000800000008000000080
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FC3F0000F00F0000E0070000C0030000C0030000800100008001
          00008001000080010000C0030000C0030000E0070000F00F0000FC3F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000080000000800000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000008000000000000000000000008000000000000000000000008000
          0000800000000000000000000000000000000000000000000000000000000000
          0000000000008000000000000000000000008000000000000000800000000000
          0000000000008000000000000000000000000000000000000000000000000000
          0000000000008000000000000000000000008000000000000000800000000000
          0000000000008000000000000000000000000000000000000000000000000000
          0000000000000000000080000000800000008000000000000000800000000000
          0000000000008000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008000000000000000800000008000
          0000800000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008000000000000000800000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000F3FF0000ED9F0000ED6F0000ED6F0000F16F0000FD1F0000FC7F
          0000FEFF0000FC7F0000FD7F0000F93F0000FBBF0000FBBF0000FBBF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000D9930000D9930000D9930000D993
          0000D9930000D9930000D9930000D9930000D993000000000000000000000000
          000000000000000000000000000000000000BA7E0000FFB62400FFB62400FFB6
          2400FFB62400FFB62400FFB62400FFB62400BA7E000000000000000000000000
          000000000000000000000000000000000000BA7E0000FFC248007C5400007C54
          00007C5400007C5400007C540000FFC24800BA7E000000000000D9930000D993
          0000D9930000D9930000D9930000D9930000BA7E0000FFCE6C00FFCE6C00FFCE
          6C00FFCE6C00FFCE6C00FFCE6C00FFCE6C00BA7E000000000000BA7E0000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BA7E0000FFCE6C007C5400007C54
          00007C5400007C5400007C540000FFCE6C00BA7E000000000000BA7E0000FFFF
          FF007C5400007C5400007C5400007C540000BA7E0000FFDA9000FFDA9000FFDA
          9000FFDA9000FFDA9000FFDA9000FFDA9000BA7E0000000000009B690000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009B690000FFDA90009B6900009B69
          0000FFDA90009B6900009B6900009B6900009B690000000000009B690000FFFF
          FF007C5400007C5400007C5400007C5400009B690000EBEBEB00EBEBEB00EBEB
          EB00EBEBEB009B690000FFFFFF009B69000000000000000000009B690000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009B690000EBEBEB00EBEBEB00EBEB
          EB00EBEBEB009B6900009B6900000000000000000000000000007C540000FFFF
          FF007C5400007C540000FFFFFF007C5400009B6900009B6900009B6900009B69
          00009B6900009B690000000000000000000000000000000000007C540000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF007C540000FFFFFF007C540000000000000000
          00000000000000000000000000000000000000000000000000007C540000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF007C5400007C54000000000000000000000000
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF0000FC010000FC010000FC01000000010000000100000001
          0000000100000003000000070000000F000000FF000001FF000003FF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008000000080000000800000008000
          0000800000008000000080000000800000008000000080000000000000007C54
          00007C5400007C5400007C5400007C54000080000000FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00800000007C5400000069
          9B0000699B0000699B0000699B0000699B0080000000FFFFFF00800000008000
          000080000000800000008000000080000000FFFFFF00800000007C5400000069
          9B0000699B0000699B0000699B0000699B0080000000FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00800000007C540000007E
          BA00007EBA00007EBA00007EBA00007EBA0080000000FFFFFF00800000008000
          000080000000FFFFFF00800000008000000080000000800000007C540000007E
          BA00007EBA00007EBA00007EBA00007EBA0080000000FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF0080000000FFFFFF0080000000000000007C540000007E
          BA00007EBA00007EBA00007EBA00007EBA0080000000FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00800000008000000000000000000000007C5400000093
          D9000093D9000093D9000093D9000093D9008000000080000000800000008000
          00008000000080000000800000007C54000000000000000000007C5400000093
          D9000093D9000093D9000093D9000093D9000093D9000093D9000093D9000093
          D9000093D9000093D9000093D9007C54000000000000000000007C5400000093
          D9000093D9007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400000093D9000093D9007C54000000000000000000007C54000000AA
          F00000AAF0007C5400000093D9000093D9000093D9000093D9000093D9000093
          D9007C54000000AAF00000AAF0007C54000000000000000000007C54000000AA
          F00000AAF00000AAF0007C54000000FFFF007C5400007C54000000FFFF007C54
          000000AAF00000AAF00000AAF0007C5400000000000000000000000000007C54
          00007C5400007C5400007C5400007C54000000FFFF0000FFFF007C5400007C54
          00007C5400007C5400007C540000000000000000000000000000000000000000
          00000000000000000000000000007C5400007C5400007C5400007C5400000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FC00000080000000000000000000000000000000000100000003
          0000000300000003000000030000000300000003000080070000F87F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000007C54
          0000000000007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400000000000000000000000000000000000000000000000000007C54
          00007C5400007C5400004848FF004848FF004848FF004848FF004848FF004848
          FF007C5400000000000000000000000000000000000000000000000000007C54
          0000000000007C5400004848FF004848FF004848FF004848FF004848FF004848
          FF007C5400000000000000000000000000000000000000000000000000007C54
          0000000000007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400000000000000000000000000000000000000000000000000007C54
          00007C5400007C54000048FF850048FF850048FF850048FF850048FF850048FF
          85007C5400000000000000000000009B00000000000000000000000000007C54
          0000000000007C54000048FF850048FF850048FF850048FF850048FF850048FF
          85007C54000000000000009B000024FF6D00009B000000000000000000007C54
          0000000000007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C540000009B000024FF6D000000000024FF6D00009B0000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400000000000000000000000000000000000000000000000000007C54
          00007C5400007C54000048FF850048FF850048FF850048FF850048FF850048FF
          85007C5400000000000000000000009B00000000000000000000000000007C54
          0000000000007C54000048FF850048FF850048FF850048FF850048FF850048FF
          85007C54000000000000009B000024FF6D00009B000000000000000000007C54
          0000000000007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C540000009B000024FF6D000000000024FF6D00009B0000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00A01F0000801F0000A01F0000A01F0000FFFF0000A01F0000801B0000A011
          0000A0040000FFFF0000A01F0000801B0000A0110000A0040000FFFF0000BFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000BA7E0000BA7E
          0000BA7E0000BA7E0000BA7E0000BA7E0000BA7E0000BA7E0000BA7E00005D3F
          00005D3F00005D3F00005D3F0000000000000000000000000000BA7E0000DEDE
          DE00DEDEDE00DEDEDE00DEDEDE00DEDEDE005D3F00005D3F0000C0C0C0005D3F
          0000008080005D3F00005D3F00005D3F00005D3F000000000000BA7E0000DEDE
          DE00DEDEDE00FFFFFF00FFFFFF00FFFFFF0000808000008080005D3F00005D3F
          000000FFFF00008080005D3F00000080800000FFFF005D3F0000BA7E0000DEDE
          DE00DEDEDE00808080005D3F0000C0C0C0005D3F000000FFFF00008080005D3F
          0000FFFFFF00008080000080800000FFFF005D3F000000000000BA7E0000DEDE
          DE00DEDEDE00808080005D3F00005D3F0000008080005D3F000000FFFF00FFFF
          FF0000FFFF00FFFFFF0000FFFF005D3F0000008080005D3F0000BA7E0000DEDE
          DE00DEDEDE00808080005D3F00005D3F00000080800000FFFF00FFFFFF005D3F
          00005D3F00005D3F0000FFFFFF0000FFFF00008080005D3F0000BA7E0000DEDE
          DE00DEDEDE008080800080808000008080005D3F0000FFFFFF005D3F00008080
          8000FFFFFF005D3F00005D3F0000008080005D3F00005D3F0000BA7E0000DEDE
          DE00DEDEDE00FFFFFF00FFFFFF00FFFFFF005D3F00000080800000FFFF008080
          8000FFFFFF005D3F00000080800000FFFF005D3F000000000000BA7E0000DEDE
          DE00DEDEDE00808080005D3F0000C0C0C0000080800000FFFF005D3F00008080
          8000FFFFFF005D3F00005D3F0000008080005D3F000000000000BA7E0000DEDE
          DE00DEDEDE00808080005D3F0000FFFFFF000080800000808000DEDEDE008080
          800080808000DEDEDE005D3F0000000000000000000000000000BA7E0000DEDE
          DE00DEDEDE00808080005D3F00005D3F0000C0C0C000FFFFFF00DEDEDE00DEDE
          DE00DEDEDE00DEDEDE005D3F0000000000000000000000000000BA7E0000DEDE
          DE00DEDEDE0080808000808080008080800080808000FFFFFF00DEDEDE00DEDE
          DE00DEDEDE00DEDEDE005D3F0000000000000000000000000000BA7E0000DEDE
          DE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDE
          DE00DEDEDE00DEDEDE005D3F0000000000000000000000000000BA7E0000DEDE
          DE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDE
          DE00DEDEDE00DEDEDE005D3F0000000000000000000000000000BA7E0000BA7E
          0000BA7E0000BA7E0000BA7E0000BA7E0000BA7E0000BA7E0000BA7E0000BA7E
          0000BA7E0000BA7E00005D3F0000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF00000007000000010000000000000001000000000000000000000000
          0000000100000001000000070000000700000007000000070000000700000007
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000007C54
          0000000000007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400000000000000000000000000000000000000000000000000007C54
          00007C5400007C54000000FF000000FF000000FF000000FF000000FF000000FF
          00007C5400000000000000000000000000000000000000000000000000007C54
          0000000000007C54000000FF000000FF000000FF000000FF000000FF000000FF
          00007C5400000000000000000000000000000000000000000000000000007C54
          0000000000007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          000000000000FF00000000000000FF00000000000000FF000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          00007C5400000000000000000000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000007C54
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007C54
          000000FFFF00808080007C540000000000000000000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000007C54
          000000FFFF00C0C0C0007C540000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00007C54000000FFFF00808080007C5400000000000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          00007C54000000FFFF00C0C0C0007C5400000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000007C54000000FFFF00808080007C54000000000000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000007C54000000FFFF007C5400007C54000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007C54000000008000000080007C540000000000007C54
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C5400007C54000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00A01F0000801F0000A01F0000A01F0000FFFF0000AABF0000FF9F0000BF8F
          0000FF870000BF870000FFC30000BFC30000FFE10000BFE10000FFF00000BFF1
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000080000000800000000000
          0000800000008000000000000000800000008000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000FF000000FF0000000000000000000000000000007C5400000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000FF000000FF0000000000000000000000000000007C5400007C54
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400000000000000000000000000000000000000000000000000000000
          000000000000FF000000FF0000000000000000000000000000007C54000000FF
          FF00808080007C54000000000000000000000000000000000000000000000000
          00000000000000000000FF000000FF00000000000000000000007C54000000FF
          FF00C0C0C0007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF00000000000000000000007C54
          000000FFFF00808080007C54000000000000000000000000000000000000FF00
          0000FF0000000000000000000000FF000000FF00000000000000000000007C54
          000000FFFF00C0C0C0007C54000000000000000000000000000000000000FF00
          0000FF0000000000000000000000FF000000FF00000000000000000000000000
          00007C54000000FFFF00808080007C5400000000000000000000000000000000
          0000FF000000FF000000FF000000FF0000000000000000000000000000000000
          00007C54000000FFFF007C5400007C5400000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000007C54000000008000000080007C54000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000007C5400007C5400007C5400000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FE490000FFFF0000FFFF0000E77F0000E73F0000FF1F0000E70F
          0000F30F0000F98700009987000099C30000C3C30000FFE10000FFE30000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000000000FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
          FF000000000000000000FFFFFF00FFFFFF00000000000000000000000000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
          0000FFFFFF007C5400007C5400007C5400007C5400007C5400007C5400007C54
          0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
          00007C540000FF854800FF854800FFC24800FFCE6C00FFCE6C00FFDA9000FFE6
          B4007C540000FFFFFF0000000000000000000000000000000000000000000000
          00007C540000FF854800FFC24800FFCE6C00FFCE6C00FFDA9000FFE6B400FFE6
          B400FFDA90007C54000000000000000000000000000000000000000000000000
          00007C540000FFC24800FFCE6C00FFCE6C00FFDA9000FFDA9000FFE6B400EBEB
          EB00FFE6B400009B00007C540000000000000000000000000000000000000000
          00007C540000FFCE6C00FFCE6C00FFDA9000FFDA9000FFE6B400EBEBEB00F8F8
          F800FFE6B40000BA3C00009B00007C5400000000000000000000000000000000
          00007C540000FFCE6C00FFDA9000FFDA9000FFE6B400EBEBEB00F8F8F800F8F8
          F800FFFFB40048FF480000BA3C00009B00007C54000000000000000000000000
          0000000000007C540000EBEBEB00FFFFB400FFE6B400FFE6B400FFE6B400FFDA
          9000F8F8F8006CFF6C0048FF480000BA3C007C54000000000000000000000000
          000000000000000000007C540000B4FFB40090FF90006CFF6C0048FF480000BA
          3C00009B0000B4FFB4006CFF6C0048FF48007C54000000000000000000000000
          00000000000000000000000000007C540000B4FFB40090FF90006CFF6C0048FF
          480000BA3C00009B0000B4FFB4006CFF6C007C54000000000000000000000000
          0000000000000000000000000000000000007C540000B4FFB40090FF90006CFF
          6C0048FF480000BA3C00009B0000B4FFB4007C54000000000000000000000000
          000000000000000000000000000000000000000000007C5400007C5400007C54
          00007C5400007C5400007C5400007C5400000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00E93300008000000080010000C0030000C00F0000C00F0000C0070000C003
          0000C0010000E0010000F0010000F8010000FC010000FE030000FFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000C0C0C000C0C0C00000000000008080000000000000000000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000C0C0C000C0C0C00000000000008080000000000000000000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000C0C0C000C0C0C00000000000008080000000000000000000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000000000008080000000000000000000000000000000
          0000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000000000000000000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000000000000000000000000000000
          00000080800000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C00000000000008080000000000000000000000000000000
          00000080800000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C00000000000008080000000000000000000000000000000
          00000080800000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C00000000000008080000000000000000000000000000000
          00000080800000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C00000000000008080000000000000000000000000000000
          00000080800000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C00000000000000000000000000000000000000000000000
          00000080800000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C00000000000C0C0C0000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000C001000080010000800100008001000080010000800100008001
          000080010000800100008001000080010000800100008001000080010000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FF000000FFB62400000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FFB624000000
          0000000000000000000000000000000000000000000000000000005D5D00005D
          5D00005D5D00005D5D00FF000000FF000000FF000000FF000000FFB62400005D
          5D00005D5D000000000000000000000000000000000000000000005D5D00005D
          5D0000808000FF000000FF000000FF000000FFB62400FF000000FF000000FFB6
          240000808000005D5D0000000000000000000000000000000000005D5D006CFF
          FF00FF000000FF000000FF000000FFB624000080800000808000FF000000FF00
          0000FFB6240000808000005D5D00000000000000000000000000005D5D006CFF
          FF006CFFFF00FF000000FFB6240000808000008080000080800000808000FF00
          0000FFB624000080800000808000005D5D000000000000000000005D5D006CFF
          FF006CFFFF006CFFFF00005D5D00008080000080800000808000008080000080
          8000FF000000FFB624000080800000808000005D5D0000000000005D5D006CFF
          FF006CFFFF006CFFFF006CFFFF00005D5D00005D5D00005D5D00005D5D00005D
          5D00005D5D00FF000000FFB62400005D5D00005D5D00005D5D00005D5D006CFF
          FF006CFFFF006CFFFF006CFFFF006CFFFF006CFFFF006CFFFF006CFFFF006CFF
          FF00005D5D0000000000FF000000FFB624000000000000000000005D5D006CFF
          FF006CFFFF006CFFFF006CFFFF006CFFFF006CFFFF006CFFFF006CFFFF006CFF
          FF00005D5D000000000000000000FF000000FFB6240000000000005D5D006CFF
          FF006CFFFF006CFFFF00005D5D00005D5D00005D5D00005D5D00005D5D00005D
          5D00005D5D00000000000000000000000000FF000000FFB6240000000000005D
          5D00005D5D00005D5D0000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000FF000000FF000000FF0000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000FF000000FF0000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF000000000000000000
          000000000000FF00000000000000FF0000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000FF000000FF00
          0000FF0000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FCFF0000F87F0000001F0000000F00000007000000030000000100000000
          00000013000000190000001C00008FFE0000FFE30000FFF30000FEEB0000FF1F
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000008080000080800000808000008080000080800000808000008080000080
          80000080800000000000000000000000000000000000000000000000000000FF
          FF00000000000080800000808000008080000080800000808000008080000080
          800000808000008080000000000000000000000000000000000000000000FFFF
          FF0000FFFF000000000000808000008080000080800000808000008080000080
          80000080800000808000008080000000000000000000000000000000000000FF
          FF00FFFFFF0000FFFF0000000000008080000080800000808000008080000080
          800000808000008080000080800000808000000000000000000000000000FFFF
          FF0000FFFF00FFFFFF0000FFFF00000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000000000FF
          FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
          FF0000000000000000000000000000000000000000000000000000000000FFFF
          FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
          FF000000000000000000000000000000000000000000000000000000000000FF
          FF00FFFFFF0000FFFF0000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF0000001F0000000F00000007000000030000000100000000
          0000001F0000001F0000001F00008FF10000FFF90000FF750000FF8F0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C540000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C54000000000000000000000000
          000000000000FF00000000000000000000007C5400007C5400007C540000FFB6
          2400FFB62400FFB62400FFB62400FFB624007C54000000000000000000000000
          000000000000FF000000FF000000000000007C540000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C54000000000000FF000000FF00
          0000FF000000FF000000FF000000FF0000007C54000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000FF000000FF000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000FF00000000000000000000007C540000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C54000000000000000000000000
          0000000000000000000000000000000000007C5400007C5400007C540000FFB6
          2400FFB62400FFB62400FFB62400FFB624007C54000000000000000000000000
          0000000000000000000000000000000000007C540000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C54000000000000000000000000
          0000000000000000000000000000000000007C54000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000FF00000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000FF000000FF000000000000007C540000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C54000000000000FF000000FF00
          0000FF000000FF000000FF000000FF0000007C5400007C5400007C540000FFB6
          2400FFB62400FFB62400FFB62400FFB624007C54000000000000000000000000
          000000000000FF000000FF000000000000007C540000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C54000000000000000000000000
          000000000000FF00000000000000000000007C54000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C54000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FD010000EC010000E501000001FF0000E7FF0000ED010000FC010000FD01
          0000FDFF0000EFFF0000E501000000010000E5010000EDFF0000FFFF0000FDFF
          0000}
      end>
  end
  object StateIL: TcxImageList
    DrawingStyle = dsTransparent
    FormatVersion = 1
    DesignInfo = 17367128
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
          0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF000000FF0000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF000000FF0000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FF000000FF0000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000FF000000FF00
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FF00
          0000FF0000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF0000000000000000000000FF00
          0000FF0000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF0000000000000000000000FF00
          0000FF0000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FF000000FF000000FF000000FF00
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FE7F0000FE7F0000FFFF
          0000FE7F0000FF3F0000FF9F0000F99F0000F99F0000FC3F0000FFFF0000FFFF
          0000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000D94600000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000BA3C0000BA3C000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000BA3C0000BA3C0000BA3C0000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000BA3C0000BA3C0000BA3C0000BA3C00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000BA3C0000BA3C0000BA3C0000BA3C0000BA3C000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009B3200009B3200009B320000BA3C0000BA3C0000BA
          3C00000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009B3200009B3200009B3200009B320000BA3C0000BA
          3C0000BA3C000000000000000000000000000000000000000000000000000000
          00000000000000000000009B32000080000000800000009B320000BA3C0000BA
          3C0000BA3C0000BA3C0000000000000000000000000000000000000000000000
          00000000000000000000009B3200009B3200009B3200009B320000BA3C0000BA
          3C0000BA3C000000000000000000000000000000000000000000000000000000
          00000000000000000000009B3200009B3200009B320000BA3C0000BA3C0000BA
          3C00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000BA3C0000BA3C0000BA3C0000BA3C0000BA3C000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000BA3C0000BA3C0000BA3C0000BA3C00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000BA3C0000BA3C0000BA3C0000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000BA3C0000BA3C000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000D94600000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          7E000000424D7E000000000000003E0000002800000010000000100000000100
          010000000000400000000000000000000000020000000000000000000000FFFF
          FF00FFFF0000F7FF0000F3FF0000F1FF0000F0FF0000F07F0000F03F0000F01F
          0000F00F0000F01F0000F03F0000F07F0000F0FF0000F1FF0000F3FF0000F7FF
          0000}
      end>
  end
  object MainLIL: TcxImageList
    DrawingStyle = dsTransparent
    Height = 32
    Width = 32
    FormatVersion = 1
    DesignInfo = 13238508
    ImageInfo = <
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000FF000000
          FF000000FF000000FF0000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000FF000000FF000000FF000000FF00000000000000
          00000000000000000000000000000000000000000000000000000000FF000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF00000000000000
          00000000000000000000008000000000000000000000000000000000FF000000
          0000FF000000FF0000000000000000000000FF000000FF00000000000000FF00
          0000FF0000000000000000000000FF000000FF00000000000000FF000000FF00
          00000000000000000000FF000000FF000000000000000000FF00000000000000
          00000000000000000000008000000080000000000000000000000000FF000000
          0000FF000000FF000000FF00000000000000FF000000FF000000000000000000
          0000FF000000FF00000000000000FF000000FF00000000000000FF000000FF00
          0000FF00000000000000FF000000FF000000000000000000FF00000000000000
          0000000000000000000000800000008000000080000000000000000000000000
          0000FF000000FF000000FF00000000000000FF000000FF000000000000000000
          0000FF000000FF00000000000000FF000000FF00000000000000FF000000FF00
          0000FF00000000000000FF000000FF0000000000000000000000000000000000
          000000D9000000BA000000800000008000000080000000800000000000000000
          0000FF000000FF000000FF000000FF000000FF000000FF000000000000000000
          0000FF000000FF00000000000000FF000000FF00000000000000FF000000FF00
          0000FF000000FF000000FF000000FF00000000000000000000000000000000BA
          0000008000000080000000800000008000000080000000800000008000000000
          0000FF000000FF00000000000000FF000000FF000000FF000000000000000000
          0000FF000000FF00000000000000FF000000FF00000000000000FF000000FF00
          000000000000FF000000FF000000FF000000000000000000000000BA00000080
          00000080000000BA000000800000008000000080000000800000000000000000
          0000FF000000FF00000000000000FF000000FF000000FF000000000000000000
          0000FF000000FF00000000000000FF000000FF00000000000000FF000000FF00
          000000000000FF000000FF000000FF0000000000000000000000008000000080
          0000000000000000000000800000008000000080000000000000000000000000
          0000FF000000FF0000000000000000000000FF000000FF000000000000000000
          0000FF000000FF00000000000000FF000000FF00000000000000FF000000FF00
          00000000000000000000FF000000FF0000000000000000000000008000000080
          00000000000000000000008000000080000000000000000000000000FF000000
          0000FF000000FF0000000000000000000000FF000000FF000000000000000000
          0000FF000000FF00000000000000FF000000FF00000000000000FF000000FF00
          00000000000000000000FF000000FF000000000000000000FF00008000000080
          00000000000000000000008000000000000000000000000000000000FF000000
          0000FF000000FF0000000000000000000000FF000000FF000000000000000000
          000000000000FF000000FF000000FF000000FF00000000000000FF000000FF00
          00000000000000000000FF000000FF000000000000000000FF00008000000080
          000000D9000000000000000000000000000000000000000000000000FF000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FF00008000000080
          00000080000000000000000000000000000000000000000000000000FF000000
          FF000000FF000000FF0000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000FF000000FF000000FF000000FF00000000000080
          0000008000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000080
          0000008000000000000000000000000000000000000000000000000000000000
          FF000000FF000000FF000000FF00000000000000000000000000000000000000
          0000000000000000FF000000FF000000FF000000FF0000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000080
          0000008000000000000000000000000000000000000000000000000000000000
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000FF0000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000080
          0000008000000000000000000000000000000000000000000000000000000000
          FF00000000000000000000000000FF000000FF0000000000000000000000FF00
          0000FF0000000000000000000000000000000000FF0000000000000000000000
          0000000000000080000000000000000000000000000000000000000000000080
          0000008000000000000000000000000000000000000000000000000000000000
          FF00000000000000000000000000FF000000FF000000FF00000000000000FF00
          0000FF0000000000000000000000000000000000FF0000000000000000000000
          0000008000000080000000000000000000000000000000000000000000000080
          0000008000000000000000000000000000000000000000000000000000000000
          FF00000000000000000000000000FF000000FF000000FF00000000000000FF00
          0000FF0000000000000000000000000000000000000000000000000000000080
          0000008000000080000000000000000000000000000000000000008000000080
          000000BA00000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF000000FF000000FF000000FF00
          0000FF0000000000000000000000000000000000000000000000008000000080
          0000008000000080000000000000000000000000000000800000008000000080
          000000BA00000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF00000000000000FF000000FF00
          0000FF0000000000000000000000000000000000000000800000008000000080
          00000080000000800000008000000080000000800000008000000080000000BA
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF00000000000000FF000000FF00
          0000FF0000000000000000000000000000000000000000000000008000000080
          0000008000000080000000800000008000000080000000800000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000FF000000FF0000000000000000000000FF00
          0000FF0000000000000000000000000000000000000000000000000000000080
          0000008000000080000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          FF00000000000000000000000000FF000000FF0000000000000000000000FF00
          0000FF0000000000000000000000000000000000FF0000000000000000000000
          0000008000000080000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          FF00000000000000000000000000FF000000FF0000000000000000000000FF00
          0000FF0000000000000000000000000000000000FF0000000000000000000000
          0000000000000080000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000FF0000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          FF000000FF000000FF000000FF00000000000000000000000000000000000000
          0000000000000000FF000000FF000000FF000000FF0000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFFF0FFFF0FF7FFFFEF74C9932F344C912F1C4C913C0C0
          C9038048C92300C8C92331CCC933334CC932374CE1321F7FFFFE1F0FFFF09FFF
          FFFFFFFFFFFFFFFFFF9F87E1FF9FBFFDFF9FB99DEF9FB89DCF9FB89F8F1FF81F
          0E1FF91E003FF91F00FFF99F8FFFB99DCFFFB99DEFFFBFFDFFFF87E1FFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000004000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000004000000000000000000000000000000000000
          0000FFEED2FF725246FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000004000000000000000000000000000000000000
          000000000000FFEED2FF725246FF00000000000000004293D0FF3287CEFF3085
          CDFF2F82CBFF2D80C9FF2B7FC8FF2A7BC5FF2B7CC7FF2B7FC8FF2D80C9FF2F82
          CBFF3085CDFF3287CEFF4293D0FF000000000000000000000000000000000000
          0000000000000000000000000003000000000000000000000000000000000000
          00000000000000000000725246FF725246FF000000004A9ED8FF6FC8F3FF6EC7
          F3FF6CC6F3FF6CC5F3FF6AC4F2FF66C2F2FF68C3F2FF6AC4F2FF6CC5F3FF6CC6
          F3FF6EC7F3FF6FC8F3FF4A9ED8FF000000000000000000000000000000000000
          000000000000000000000000000300000000725246FF725246FF725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF4EA1D9FF79CDF4FF57BC
          F0FF56BBF0FF56BAF0FF54BAF0FF53B8F0FF54B9F0FF54BAF0FF56BAF0FF56BB
          F0FF57BCF0FF79CDF4FF4EA1D9FF000000000000000000000000000000000000
          000000000000000000000000000300000000725246FFFFEED2FFFFEED2FFFFEE
          D2FFFFEED2FFFFEED2FF725246FF725246FFFFEED2FF51A4DBFF80D1F6FF5BBF
          F1FF5BBFF1FF5BBEF1FF5ABEF1FF59BCF1FF59BDF1FF5ABEF1FF5BBEF1FF5BBF
          F1FF5BBFF1FF80D1F6FF51A4DBFF000000000000000000000000000000000000
          000000000000000000000000000000000000725246FFFFEED2FF000000000000
          000000000000FFEED2FF725246FF000000000000000053A6DCFF89D5F7FF87D4
          F7FF86D4F6FF86D4F6FF85D3F6FF82D1F5FF83D3F5FF85D3F6FF86D4F6FF86D4
          F6FF87D4F7FF89D5F7FF53A6DCFF000000000000000000000000000000000000
          000000000000000000000000000000000000725246FFFFEED2FF000000000000
          0000FFEED2FF725246FF00000000000000000000000056A9DCFF55A8DCFF55A7
          DCFF54A6DBFF52A6DBFF52A5DAFF51A3D9FF52A4D9FF52A5DAFF52A6DBFF54A6
          DBFF55A7DCFF55A8DCFF56A9DCFF000000000000000000000000000000000000
          000000000000000000000000000000000000725246FFFFEED2FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000725246FFFFEED2FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000725246FFFFEED2FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000725246FFFFEED2FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000725246FFFFEED2FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001F5942BE2C79
          5CFF2B765AFF297558FF297356FF297255FF277153FF266E52FF266D52FF266C
          4FFF246A4FFF23694DFF23674DFF23664BFF216348FF206147FF206145FF2060
          45FF174531C00000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000034886AFF7ED9
          BDFF6BD1B3FF69D0B1FF68D0B0FF66D0B0FF66D0B0FF64D0AFFF63D0AEFF62CE
          ADFF61CEADFF60CEABFF5ECDABFF5ECDAAFF5BCCA7FF5ACBA7FF5ACAA7FF59CA
          A6FF226548FF00000000000000000000000000000000F97B2BFFCE4D00FFCC4E
          00FFCB4B00FF903600B800000000000000000000000000000000368C6DFF8BDC
          C3FF4FC6A0FF4EC69FFF4CC59FFF4CC59DFF4BC49BFF49C29BFF48C29AFF47C1
          99FF45C199FF44C197FF43C096FF42BF95FF3FBE94FF3EBD92FF3DBD91FF5CCD
          A9FF24694DFF00000000000000000000000000000000D55200FFFFCB7BFFFFB1
          51FFFFB04FFFCE4D00FF00000000000000000000000000000000389071FF98E1
          CBFF52C8A2FF51C7A1FF50C7A1FF50C7A1FF4FC5A0FF4EC59FFF4DC69EFF4BC5
          9CFF4AC39CFF49C29AFF47C299FF46C298FF43C097FF42BF96FF41BF95FF60CE
          ACFF287053FF00000000000000000000000000000000DC5600FFFFCF81FFFFB3
          55FFFFB253FFD25000FF000000000000000000000000000000003C9778FFB2E9
          D9FF5CCDAAFF5BCDA9FF59CDA8FF58CCA6FF57CAA6FF56CAA5FF55CAA5FF54C8
          A4FF52C8A3FF50C8A2FF4FC7A1FF4FC6A0FF4CC59DFF4BC49CFF4AC39BFF69D0
          B0FF2E7C5EFF00000000000000000000000000000000E25900FFFFD38AFFFFB7
          5AFFFFB559FFD85400FF000000000000000000000000000000003D9B7BFFBDED
          DFFF60CFADFF5FCFACFF5ECFABFF5CCFAAFF5CCEAAFF5ACCA9FF59CBA7FF59CB
          A6FF57CAA6FF56C9A5FF54C9A4FF53C9A3FF50C7A0FF4FC6A0FF4EC6A0FF6DD4
          B4FF318364FFF79044FFF26602FFF06400FFEE6200FFEC5C00FFFFBC5FFFFFBA
          5FFFFFB95DFFDF5700FFDC5600FFDA5500FFD75600FF973A00B73F9F7FFFC8F0
          E4FFC7EFE3FFC5EFE2FFC2EEE1FFC0EDE1FFBEEDDFFFBDEDDEFFBBECDDFFB9EB
          DCFFB6EADBFFB4EADAFFB2E9D8FFAFE8D6FFA7E5D3FFA5E5D2FFA3E4D1FFA1E3
          CFFF34896AFFF96C05FFFFE1A2FFFFC269FFFFBF68FFFFBD68FFFFBF65FFFFBE
          63FFFFBB63FFFFBB61FFFFBA5FFFFFB85FFFFFB95CFFDB5600FF30785FC140A1
          81FF40A080FF3F9F7FFF3F9E7EFF3F9D7DFF3D9B7CFF389575FF3D9A7AFF3D99
          79FF3B9878FF3B9777FF3B9577FF3A9576FF389272FF389171FF388F70FF388E
          6FFF286751C2FC700AFFFFE5A7FFFFC36FFFFFC16FFFFFC36CFFFFC26AFFFFC0
          69FFFFBF68FFFFBF67FFFFBF64FFFFBD64FFFFBA63FFE25800FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000FC7812FFFFE8ACFFFFE8A9FFFFE7A7FFFFE5A6FFFFE4A4FFFFC3
          6FFFFFC26DFFFFC36CFFFFE09BFFFFDC99FFFFDA95FFE95D00FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000FB9D51FFFD7F16FFFD7913FFFB7512FFFB760FFFFFE7A9FFFFC7
          74FFFFC573FFF96B04FFF76902FFF46702FFF36301FFB24600BD000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FC7E15FFFFE9AEFFFFCA
          79FFFFC977FFFB6F0AFF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FD821CFFFFECB2FFFFCC
          7EFFFFCC7CFFFC7611FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FD8B24FFFFEEB6FFFFED
          B5FFFFECB3FFFC7C18FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FEA55FFFFE8B28FFFE88
          26FFFD8623FFBC6216BD00000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000001F5942BE2C795CFF2B76
          5AFF297558FF297356FF297255FF277153FF266E52FF266D52FF266C4FFF246A
          4FFF23694DFF23674DFF23664BFF226649FF226449FF216348FF206147FF2061
          45FF206045FF174531C000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000034886AFF7ED9BDFF6BD1
          B3FF69D0B1FF68D0B0FF66D0B0FF66D0B0FF64D0AFFF63D0AEFF62CEADFF61CE
          ADFF60CEABFF5ECDABFF5ECDAAFF5DCDA9FF5DCCA9FF5BCCA7FF5ACBA7FF5ACA
          A7FF59CAA6FF226548FF00000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000000000000000000000000000368C6DFF8BDCC3FF4FC6
          A0FF4EC69FFF4CC59FFF4CC59DFF4BC49BFF49C29BFF48C29AFF47C199FF45C1
          99FF44C197FF43C096FF42BF95FF41BF95FF3FBF95FF3FBE94FF3EBD92FF3DBD
          91FF5CCDA9FF24694DFF00000000000000000000000000000000725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF3A9374FFA5E5D2FF57CB
          A6FF56CAA5FF55CAA4FF53CAA4FF53C9A3FF51C8A2FF50C8A2FF4FC7A0FF4FC6
          9FFF4DC69EFF4CC59EFF4BC59CFF49C49AFF48C39AFF48C29AFF46C199FF45C1
          98FF64D0AEFF2A7659FF00000000000000000000000000000000725246FFAA9F
          8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAA3C9778FFB2E9D9FF5CCD
          AAFF5BCDA9FF59CDA8FF58CCA6FF57CAA6FF56CAA5FF55CAA5FF54C8A4FF52C8
          A3FF50C8A2FF4FC7A1FF4FC6A0FF4EC69FFF4DC59FFF4CC59DFF4BC49CFF4AC3
          9BFF69D0B0FF2E7C5EFF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003D9B7BFFBDEDDFFF60CF
          ADFF5FCFACFF5ECFABFF5CCFAAFF5CCEAAFF5ACCA9FF59CBA7FF59CBA6FF57CA
          A6FF56C9A5FF54C9A4FF53C9A3FF52C8A2FF50C8A1FF50C7A0FF4FC6A0FF4EC6
          A0FF6DD4B4FF318364FF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003F9F7FFFC8F0E4FFC7EF
          E3FFC5EFE2FFC2EEE1FFC0EDE1FFBEEDDFFFBDEDDEFFBBECDDFFB9EBDCFFB6EA
          DBFFB4EADAFFB2E9D8FFAFE8D6FFADE7D5FFAAE6D4FFA7E5D3FFA5E5D2FFA3E4
          D1FFA1E3CFFF34896AFF00000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000030785FC140A181FF40A0
          80FF3F9F7FFF3F9E7EFF3F9D7DFF3D9B7CFF389575FF3D9A7AFF3D9979FF3B98
          78FF3B9777FF3B9577FF3A9576FF3A9475FF399374FF389272FF389171FF388F
          70FF388E6FFF286751C200000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000AA9F8CAA725246FF0000000000000000000000004293
          D0FF3287CEFF3085CDFF2F82CBFF2D80C9FF2B7FC8FF2A7BC5FF2B7CC7FF2B7F
          C8FF2D80C9FF2F82CBFF3085CDFF3287CEFF4293D0FF00000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000AA9F8CAA725246FF00000000000000004A9E
          D8FF6FC8F3FF6EC7F3FF6CC6F3FF6CC5F3FF6AC4F2FF66C2F2FF68C3F2FF6AC4
          F2FF6CC5F3FF6CC6F3FF6EC7F3FF6FC8F3FF4A9ED8FF00000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000725246FF725246FF000000004EA1
          D9FF79CDF4FF57BCF0FF56BBF0FF56BAF0FF54BAF0FF53B8F0FF54B9F0FF54BA
          F0FF56BAF0FF56BBF0FF57BCF0FF79CDF4FF4EA1D9FF00000000000000000000
          0000000000000000000000000000000000000000000000000000725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF725246FF725246FF51A4
          DBFF80D1F6FF5BBFF1FF5BBFF1FF5BBEF1FF5ABEF1FF59BCF1FF59BDF1FF5ABE
          F1FF5BBEF1FF5BBFF1FF5BBFF1FF80D1F6FF51A4DBFF00000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAA725246FF725246FFAA9F8CAA53A6
          DCFF89D5F7FF87D4F7FF86D4F6FF86D4F6FF85D3F6FF82D1F5FF83D3F5FF85D3
          F6FF86D4F6FF86D4F6FF87D4F7FF89D5F7FF53A6DCFF00000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000AA9F8CAA725246FF000000000000000056A9
          DCFF55A8DCFF55A7DCFF54A6DBFF52A6DBFF52A5DAFF51A3D9FF52A4D9FF52A5
          DAFF52A6DBFF54A6DBFF55A7DCFF55A8DCFF56A9DCFF00000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000AA9F8CAA725246FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000AA9F8CAA725246FF0000000000000000000000004293
          D0FF3287CEFF3085CDFF2F82CBFF2D80C9FF2B7FC8FF2A7BC5FF2B7CC7FF2B7F
          C8FF2D80C9FF2F82CBFF3085CDFF3287CEFF4293D0FF00000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000AA9F8CAA725246FF00000000000000004A9E
          D8FF6FC8F3FF6EC7F3FF6CC6F3FF6CC5F3FF6AC4F2FF66C2F2FF68C3F2FF6AC4
          F2FF6CC5F3FF6CC6F3FF6EC7F3FF6FC8F3FF4A9ED8FF00000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000725246FF725246FF000000004EA1
          D9FF79CDF4FF57BCF0FF56BBF0FF56BAF0FF54BAF0FF53B8F0FF54B9F0FF54BA
          F0FF56BAF0FF56BBF0FF57BCF0FF79CDF4FF4EA1D9FF00000000000000000000
          0000000000000000000000000000000000000000000000000000725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF725246FF725246FF51A4
          DBFF80D1F6FF5BBFF1FF5BBFF1FF5BBEF1FF5ABEF1FF59BCF1FF59BDF1FF5ABE
          F1FF5BBEF1FF5BBFF1FF5BBFF1FF80D1F6FF51A4DBFF00000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAA725246FF725246FFAA9F8CAA53A6
          DCFF89D5F7FF87D4F7FF86D4F6FF86D4F6FF85D3F6FF82D1F5FF83D3F5FF85D3
          F6FF86D4F6FF86D4F6FF87D4F7FF89D5F7FF53A6DCFF00000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000AA9F8CAA725246FF000000000000000056A9
          DCFF55A8DCFF55A7DCFF54A6DBFF52A6DBFF52A5DAFF51A3D9FF52A4D9FF52A5
          DAFF52A6DBFF54A6DBFF55A7DCFF55A8DCFF56A9DCFF00000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000AA9F8CAA725246FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000001F5942BE2C795CFF2B76
          5AFF297558FF297356FF297255FF277153FF266E52FF266D52FF266C4FFF246A
          4FFF23694DFF23674DFF23664BFF226649FF226449FF216348FF206147FF2061
          45FF206045FF174531C000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000034886AFF7ED9BDFF6BD1
          B3FF69D0B1FF68D0B0FF66D0B0FF66D0B0FF64D0AFFF63D0AEFF62CEADFF61CE
          ADFF60CEABFF5ECDABFF5ECDAAFF5DCDA9FF5DCCA9FF5BCCA7FF5ACBA7FF5ACA
          A7FF59CAA6FF226548FF00000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000000000000000000000000000368C6DFF8BDCC3FF4FC6
          A0FF4EC69FFF4CC59FFF4CC59DFF4BC49BFF49C29BFF48C29AFF47C199FF45C1
          99FF44C197FF43C096FF42BF95FF41BF95FF3FBF95FF3FBE94FF3EBD92FF3DBD
          91FF5CCDA9FF24694DFF00000000000000000000000000000000725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF3A9374FFA5E5D2FF57CB
          A6FF56CAA5FF55CAA4FF53CAA4FF53C9A3FF51C8A2FF50C8A2FF4FC7A0FF4FC6
          9FFF4DC69EFF4CC59EFF4BC59CFF49C49AFF48C39AFF48C29AFF46C199FF45C1
          98FF64D0AEFF2A7659FF00000000000000000000000000000000725246FFAA9F
          8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAA3C9778FFB2E9D9FF5CCD
          AAFF5BCDA9FF59CDA8FF58CCA6FF57CAA6FF56CAA5FF55CAA5FF54C8A4FF52C8
          A3FF50C8A2FF4FC7A1FF4FC6A0FF4EC69FFF4DC59FFF4CC59DFF4BC49CFF4AC3
          9BFF69D0B0FF2E7C5EFF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003D9B7BFFBDEDDFFF60CF
          ADFF5FCFACFF5ECFABFF5CCFAAFF5CCEAAFF5ACCA9FF59CBA7FF59CBA6FF57CA
          A6FF56C9A5FF54C9A4FF53C9A3FF52C8A2FF50C8A1FF50C7A0FF4FC6A0FF4EC6
          A0FF6DD4B4FF318364FF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003F9F7FFFC8F0E4FFC7EF
          E3FFC5EFE2FFC2EEE1FFC0EDE1FFBEEDDFFFBDEDDEFFBBECDDFFB9EBDCFFB6EA
          DBFFB4EADAFFB2E9D8FFAFE8D6FFADE7D5FFAAE6D4FFA7E5D3FFA5E5D2FFA3E4
          D1FFA1E3CFFF34896AFF00000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000030785FC140A181FF40A0
          80FF3F9F7FFF3F9E7EFF3F9D7DFF3D9B7CFF389575FF3D9A7AFF3D9979FF3B98
          78FF3B9777FF3B9577FF3A9576FF3A9475FF399374FF389272FF389171FF388F
          70FF388E6FFF286751C200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000FFFF0000
          FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000
          FFFF00008E8E0000555500005555000055550000555500005555000055550000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000055550000
          55550000555500005555000055550000555500008E8E0000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000002010006010100040000000000000001000000000000
          0000010100050101000400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000231200863A1D00DD0603001600000000020100070000
          00002B1600A2301800B600000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000023120087432200FF361B00CD0704001B020100060000
          00002D1700AB381D00D600000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000028140099331A00C42A15009F381D00D7000000000100
          00022C1600A7361B00CC00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000027140096381D00D70A0500263F2000EE190D00600000
          00002D1700AD361B00CD00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000271400943B1E00E2000000000C06002E3D1F00E71008
          003D2915009B371C00D000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000271400943A1D00DD010000020000000027140094391D
          00D828140097371C00D000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000281400983C1E00E40000000001010005010100042B16
          00A3432200FF311900BB00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000024120088351B00CB0000000001000003010000020000
          00012F1800B2301800B500000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000100000200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000001010100050000000000000000000000000101
          0005010000020000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF00008E8E0000555500005555000055550000555500005555000055550000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000055550000
          55550000555500005555000055550000555500008E8E0000FFFF000000000000
          FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000FFFF0000
          FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000001F5942BE2C795CFF2B76
          5AFF297558FF297356FF297255FF277153FF266E52FF266D52FF266C4FFF246A
          4FFF23694DFF23674DFF23664BFF226649FF226449FF216348FF206147FF2061
          45FF206045FF174531C000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000034886AFF7ED9BDFF6BD1
          B3FF69D0B1FF68D0B0FF66D0B0FF66D0B0FF64D0AFFF63D0AEFF62CEADFF61CE
          ADFF60CEABFF5ECDABFF5ECDAAFF5DCDA9FF5DCCA9FF5BCCA7FF5ACBA7FF5ACA
          A7FF59CAA6FF226548FF00000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000000000000000000000000000368C6DFF8BDCC3FF4FC6
          A0FF4EC69FFF4CC59FFF4CC59DFF4BC49BFF49C29BFF48C29AFF47C199FF45C1
          99FF44C197FF43C096FF42BF95FF41BF95FF3FBF95FF3FBE94FF3EBD92FF3DBD
          91FF5CCDA9FF24694DFF00000000000000000000000000000000725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF3A9374FFA5E5D2FF57CB
          A6FF56CAA5FF55CAA4FF53CAA4FF53C9A3FF51C8A2FF50C8A2FF4FC7A0FF4FC6
          9FFF4DC69EFF4CC59EFF4BC59CFF49C49AFF48C39AFF48C29AFF46C199FF45C1
          98FF64D0AEFF2A7659FF00000000000000000000000000000000725246FFAA9F
          8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAA3C9778FFB2E9D9FF5CCD
          AAFF5BCDA9FF59CDA8FF58CCA6FF57CAA6FF56CAA5FF55CAA5FF54C8A4FF52C8
          A3FF50C8A2FF4FC7A1FF4FC6A0FF4EC69FFF4DC59FFF4CC59DFF4BC49CFF4AC3
          9BFF69D0B0FF2E7C5EFF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003D9B7BFFBDEDDFFF60CF
          ADFF5FCFACFF5ECFABFF5CCFAAFF5CCEAAFF5ACCA9FF59CBA7FF59CBA6FF57CA
          A6FF56C9A5FF54C9A4FF53C9A3FF52C8A2FF50C8A1FF50C7A0FF4FC6A0FF4EC6
          A0FF6DD4B4FF318364FF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003F9F7FFFC8F0E4FFC7EF
          E3FFC5EFE2FFC2EEE1FFC0EDE1FFBEEDDFFFBDEDDEFFBBECDDFFB9EBDCFFB6EA
          DBFFB4EADAFFB2E9D8FFAFE8D6FFADE7D5FFAAE6D4FFA7E5D3FFA5E5D2FFA3E4
          D1FFA1E3CFFF34896AFF00000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000030785FC140A181FF40A0
          80FF3F9F7FFF3F9E7EFF3F9D7DFF3D9B7CFF389575FF3D9A7AFF3D9979FF3B98
          78FF3B9777FF3B9577FF3A9576FF3A9475FF399374FF389272FF389171FF388F
          70FF388E6FFF286751C200000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          00000000FFFF0000FFFF0000FFFF0000FFFF0000000101000002000000000000
          00000000000001000002000000000000FEFF0000FEFF0000FFFF0000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          00000000FFFF0000000000000000000000000000000000000000010000030000
          00000000000000000000000000000000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          00000000FFFF000000000000000000000000060300150E070035000000000100
          0002000000000E070035060300180000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000AA9F8CAA725246FF0000000000000000000000000000
          0000000000000000000000000000000000001C0E006A432200FF1C0E006A0000
          000000000000432200FF261300910000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000AA9F8CAA725246FF00000000000000000000
          0000000000000000000000000000000000001A0D0062422100FB432200FF1B0E
          006600000000422200FC27140094000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000725246FF725246FF000000000000
          0000000000000000000000000000000000001B0D0065432200FF371C00D1341A
          00C600000000412100F927140096000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF725246FF725246FF0000
          0000000000000000000000000000000000001B0D0065432200FF020100093018
          00B52D1700AA412100F627140094000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAA725246FF725246FFAA9F8CAA0000
          000000000000000000000000000000000000190D0061412100F900000000130A
          004A432200FF422100FA24120089000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000AA9F8CAA725246FF00000000000000000000
          0000000000000000000000000000000000001D0F006D432200FF0703001A0101
          000520100079432200FF2915009C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000AA9F8CAA725246FF0000000000000000000000000000
          00000000FFFF0000000000000000000000000C06002C1E0F0072020100080000
          0000000000001E0F0072120900430000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          00000000FFFF0000000000000000000000000000000000000000000000000000
          00000100000300000000000000000000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000FFFF0000000000000000000000000000000101010005000000000000
          00000000000001010005010000020000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          00000000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000000000
          00000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000FFFF0000FFFF0000FFFF0000FFFF0000000101000002000000000000
          00000000000001000002000000000000FEFF0000FEFF0000FFFF0000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          00000000FFFF0000000000000000000000000000000000000000010000030000
          00000000000000000000000000000000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          00000000FFFF000000000000000000000000060300150E070035000000000100
          0002000000000E070035060300180000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000AA9F8CAA725246FF0000000000000000000000000000
          0000000000000000000000000000000000001C0E006A432200FF1C0E006A0000
          000000000000432200FF261300910000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000AA9F8CAA725246FF00000000000000000000
          0000000000000000000000000000000000001A0D0062422100FB432200FF1B0E
          006600000000422200FC27140094000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000725246FF725246FF000000000000
          0000000000000000000000000000000000001B0D0065432200FF371C00D1341A
          00C600000000412100F927140096000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF725246FF725246FF0000
          0000000000000000000000000000000000001B0D0065432200FF020100093018
          00B52D1700AA412100F627140094000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAA725246FF725246FFAA9F8CAA0000
          000000000000000000000000000000000000190D0061412100F900000000130A
          004A432200FF422100FA24120089000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000AA9F8CAA725246FF00000000000000000000
          0000000000000000000000000000000000001D0F006D432200FF0703001A0101
          000520100079432200FF2915009C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000AA9F8CAA725246FF0000000000000000000000000000
          00000000FFFF0000000000000000000000000C06002C1E0F0072020100080000
          0000000000001E0F0072120900430000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          00000000FFFF0000000000000000000000000000000000000000000000000000
          00000100000300000000000000000000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          00000000FFFF0000000000000000000000000000000101010005000000000000
          00000000000001010005000000000100000200000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          00000000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000000000
          00000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000001F5942BE2C795CFF2B76
          5AFF297558FF297356FF297255FF277153FF266E52FF266D52FF266C4FFF246A
          4FFF23694DFF23674DFF23664BFF226649FF226449FF216348FF206147FF2061
          45FF206045FF174531C000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000034886AFF7ED9BDFF6BD1
          B3FF69D0B1FF68D0B0FF66D0B0FF66D0B0FF64D0AFFF63D0AEFF62CEADFF61CE
          ADFF60CEABFF5ECDABFF5ECDAAFF5DCDA9FF5DCCA9FF5BCCA7FF5ACBA7FF5ACA
          A7FF59CAA6FF226548FF00000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000000000000000000000000000368C6DFF8BDCC3FF4FC6
          A0FF4EC69FFF4CC59FFF4CC59DFF4BC49BFF49C29BFF48C29AFF47C199FF45C1
          99FF44C197FF43C096FF42BF95FF41BF95FF3FBF95FF3FBE94FF3EBD92FF3DBD
          91FF5CCDA9FF24694DFF00000000000000000000000000000000725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF3A9374FFA5E5D2FF57CB
          A6FF56CAA5FF55CAA4FF53CAA4FF53C9A3FF51C8A2FF50C8A2FF4FC7A0FF4FC6
          9FFF4DC69EFF4CC59EFF4BC59CFF49C49AFF48C39AFF48C29AFF46C199FF45C1
          98FF64D0AEFF2A7659FF00000000000000000000000000000000725246FFAA9F
          8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAA3C9778FFB2E9D9FF5CCD
          AAFF5BCDA9FF59CDA8FF58CCA6FF57CAA6FF56CAA5FF55CAA5FF54C8A4FF52C8
          A3FF50C8A2FF4FC7A1FF4FC6A0FF4EC69FFF4DC59FFF4CC59DFF4BC49CFF4AC3
          9BFF69D0B0FF2E7C5EFF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003D9B7BFFBDEDDFFF60CF
          ADFF5FCFACFF5ECFABFF5CCFAAFF5CCEAAFF5ACCA9FF59CBA7FF59CBA6FF57CA
          A6FF56C9A5FF54C9A4FF53C9A3FF52C8A2FF50C8A1FF50C7A0FF4FC6A0FF4EC6
          A0FF6DD4B4FF318364FF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003F9F7FFFC8F0E4FFC7EF
          E3FFC5EFE2FFC2EEE1FFC0EDE1FFBEEDDFFFBDEDDEFFBBECDDFFB9EBDCFFB6EA
          DBFFB4EADAFFB2E9D8FFAFE8D6FFADE7D5FFAAE6D4FFA7E5D3FFA5E5D2FFA3E4
          D1FFA1E3CFFF34896AFF00000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000030785FC140A181FF40A0
          80FF3F9F7FFF3F9E7EFF3F9D7DFF3D9B7CFF389575FF3D9A7AFF3D9979FF3B98
          78FF3B9777FF3B9577FF3A9576FF3A9475FF399374FF389272FF389171FF388F
          70FF388E6FFF286751C200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000FFFF0000
          FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000
          FFFF00008E8E0000555500005555000055550000555500005555000055550000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000055550000
          55550000555500005555000055550000555500008E8E0000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010000020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000002010006010100040000000000000001000000000000
          000001010005010100040302000C050300130000000000000000000000000000
          0000000000000201000700000001010000030201000700000000000000010000
          0000000000000100000302010007000000000000000000000000000000000101
          00040000000000000000231200863A1D00DD0603001600000000020100070000
          00002B1600A2301800B6160B0052311900BB2312008500000000010000020000
          0000020100083F2000F10D070033140A004B3F2000F10A050026000000000201
          0007000000001E0F00723C1F00E5000000000000000000000000000000000101
          0005000000000000000023120087432200FF361B00CD0704001B020100060000
          00002D1700AB381D00D60000000011090041432200FF04020011000000000000
          000002010009432200FF0E070037140A004D432200FF3A1E00DE0D0600300201
          0007000000002010007A412100F6000000000000000000000000000000000101
          0004000000000000000028140099331A00C42A15009F381D00D7000000000100
          00022C1600A7361B00CC0100000201010005301800B5311900BA000000000000
          000002010008432200FE0E070035170C00593E1F00EC2010007B422200FD0000
          000001000002201000783F2000F1000000000000000100000000000000000101
          0005000000000000000027140096381D00D70A0500263F2000EE190D00600000
          00002D1700AD361B00CD0000000000000000301800B5341A00C5000000000000
          000002010009432200FF0E070035160B0055412100F90704001C391D00D82211
          0082000000002010007B402000F3000000000000000000000000000000000101
          00050000000000000000271400943B1E00E2000000000C06002E3D1F00E71008
          003D2915009B371C00D00000000000000000301800B6331A00C2000000000000
          000002010009432200FF0E070036160B0053432200FF0000000004020010391D
          00DA190D00611C0E006A402100F5000000000100000200000000000000000101
          00040000000000000000271400943A1D00DD010000020000000027140094391D
          00D828140097371C00D00000000000000000301800B5331A00C2010000020100
          00020301000B432200FE0E070035160B0053422200FD00000001000000001A0D
          0062422100FB1F1000773F2000F1000000000000000100000000000000000101
          00050000000000000000281400983C1E00E40000000001010005010100042B16
          00A3432200FF311900BB0000000000000000311900BB331A00C4000000000000
          000000000000432200FF0E070037160B0055432200FF00000000010000020101
          000422110082432200FF3B1E00DF000000000000000100000000000000000101
          0004000000000000000024120088351B00CB0000000001000003010000020000
          00012F1800B3301800B500000000000000002915009D3A1D00DB2C1600A72D17
          00AA2B1600A33D1F00E90D060030140A004C3D1F00E900000000000000000100
          00030000000027140094381D00D7000000000100000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000004020010040200100402
          0010000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF00008E8E0000555500005555000055550000555500005555000055550000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000055550000
          55550000555500005555000055550000555500008E8E0000FFFF000000000000
          FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000FFFF0000
          FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000001F5942BE2C795CFF2B76
          5AFF297558FF297356FF297255FF277153FF266E52FF266D52FF266C4FFF246A
          4FFF23694DFF23674DFF23664BFF226649FF226449FF216348FF206147FF2061
          45FF206045FF174531C000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000034886AFF7ED9BDFF6BD1
          B3FF69D0B1FF68D0B0FF66D0B0FF66D0B0FF64D0AFFF63D0AEFF62CEADFF61CE
          ADFF60CEABFF5ECDABFF5ECDAAFF5DCDA9FF5DCCA9FF5BCCA7FF5ACBA7FF5ACA
          A7FF59CAA6FF226548FF00000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000000000000000000000000000368C6DFF8BDCC3FF4FC6
          A0FF4EC69FFF4CC59FFF4CC59DFF4BC49BFF49C29BFF48C29AFF47C199FF45C1
          99FF44C197FF43C096FF42BF95FF41BF95FF3FBF95FF3FBE94FF3EBD92FF3DBD
          91FF5CCDA9FF24694DFF00000000000000000000000000000000725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF3A9374FFA5E5D2FF57CB
          A6FF56CAA5FF55CAA4FF53CAA4FF53C9A3FF51C8A2FF50C8A2FF4FC7A0FF4FC6
          9FFF4DC69EFF4CC59EFF4BC59CFF49C49AFF48C39AFF48C29AFF46C199FF45C1
          98FF64D0AEFF2A7659FF00000000000000000000000000000000725246FFAA9F
          8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAA3C9778FFB2E9D9FF5CCD
          AAFF5BCDA9FF59CDA8FF58CCA6FF57CAA6FF56CAA5FF55CAA5FF54C8A4FF52C8
          A3FF50C8A2FF4FC7A1FF4FC6A0FF4EC69FFF4DC59FFF4CC59DFF4BC49CFF4AC3
          9BFF69D0B0FF2E7C5EFF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003D9B7BFFBDEDDFFF60CF
          ADFF5FCFACFF5ECFABFF5CCFAAFF5CCEAAFF5ACCA9FF59CBA7FF59CBA6FF57CA
          A6FF56C9A5FF54C9A4FF53C9A3FF52C8A2FF50C8A1FF50C7A0FF4FC6A0FF4EC6
          A0FF6DD4B4FF318364FF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003F9F7FFFC8F0E4FFC7EF
          E3FFC5EFE2FFC2EEE1FFC0EDE1FFBEEDDFFFBDEDDEFFBBECDDFFB9EBDCFFB6EA
          DBFFB4EADAFFB2E9D8FFAFE8D6FFADE7D5FFAAE6D4FFA7E5D3FFA5E5D2FFA3E4
          D1FFA1E3CFFF34896AFF00000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000030785FC140A181FF40A0
          80FF3F9F7FFF3F9E7EFF3F9D7DFF3D9B7CFF389575FF3D9A7AFF3D9979FF3B98
          78FF3B9777FF3B9577FF3A9576FF3A9475FF399374FF389272FF389171FF388F
          70FF388E6FFF286751C200000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000000000000000000000000000000000000000FFFF0000
          FEFF0100FDFF0000FFFF00000000000000000101000201000001010100020101
          0002000000000000000000000000010100020000000000000000010100020000
          000000000000000000000100FDFF0100FDFF0000FFFF0000FFFF725246FFAA9F
          8CAA0000000000000000000000000000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000020100030000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FFFF725246FFAA9F
          8CAA0000000000000000000000000000000000000000000000000000FFFF0B06
          0015000000000000000001010004000000010000000001010004010000020101
          0004000000000000000001000003010000020100000201000002000000000100
          000300000001000000000000000000000000000000000100FDFF725246FFAA9F
          8CAA0000000000000000AA9F8CAA725246FF00000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          000000000000000000000000000000000000000000000502F4FF725246FFAA9F
          8CAA000000000000000000000000AA9F8CAA725246FF00000000000000000000
          00000000000000000000190D005F0703001900000000180C005C1008003D1D0F
          006E0402000E00000000130A004A0C06002D120900450E07003500000000180C
          005D0B06002B000000000000000000000000000000000503000A725246FFAA9F
          8CAA00000000000000000000000000000000725246FF725246FF000000000000
          000000000000000000003B1E00DF3B1E00E1010100053D1F00E80A0500262915
          009C2714009500000000301800B61E0F007127140093432200FE05020012361B
          00CD1D0F006D000000000000000000000000000000000603000B725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF725246FF725246FF0000
          00000000000000000000311900BC361B00CE1D0F006E351B00CB0301000B1F10
          00752A1500A1010000022C1600A71B0E006622110081361C00CF2010007A3019
          00B81B0D0065000000000000000000000000000000000603000B725246FFAA9F
          8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAA725246FF725246FFAA9F8CAA0000
          00000000000000000000381D00D70D070032341A00C6432200FF010100042312
          00852915009D000000002D1700AA1C0E00692A1500A0180C005B271400934322
          00FF130A0048000000000000000000000000000000000603000B725246FFAA9F
          8CAA000000000000000000000000AA9F8CAA725246FF00000000000000000000
          00000000000000000000311900B90D0700310804001F371C00D3020100091B0E
          0066301800B71D0F0070311900BC150B005024120089190D00600301000B351B
          00C8130A0049000000000000000000000000000000000503000A725246FFAA9F
          8CAA0000000000000000AA9F8CAA725246FF00000000000000000000FFFF0000
          0000000000000000000000000000000000010000000000000000000000010000
          0000020100080503001302010007000000000000000000000001000000000000
          000001010003000000000000000000000000000000000502F4FF725246FFAA9F
          8CAA0000000000000000000000000000000000000000000000000000FFFF0905
          0012000000000000000000000000020100030000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000100000000010100020000000000000000000000000100FDFF000000000000
          00000000000000000000000000000000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000000000000100
          0001000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FFFF725246FFAA9F
          8CAA0000000000000000000000000000000000000000000000000000FFFF0000
          FFFF0100FDFF0000FFFF00000000000000000101000201000001000000000000
          0000000000000101000201000001010100020000000000000000010100020000
          000000000000000000000000FEFF0000FEFF0000FFFF0000FFFF725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000FFFF0000
          FFFF0000FFFF0000FFFF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF725246FFAA9F
          8CAA0000000000000000000000000000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FFFF725246FFAA9F
          8CAA0000000000000000000000000000000000000000000000000000FFFF0000
          0000000000000000000001010004000000010000000001010004010000020101
          0004000000000000000001000003010000020100000201000002000000000100
          000300000001000000000000000000000000000000000000FFFF725246FFAA9F
          8CAA0000000000000000AA9F8CAA725246FF00000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          000000000000000000000000000000000000000000000000FFFF725246FFAA9F
          8CAA000000000000000000000000AA9F8CAA725246FF00000000000000000000
          00000000000000000000190D005F0703001900000000180C005C1008003D1D0F
          006E0402000E00000000130A004A0C06002D120900450E07003500000000180C
          005D0B06002B0000000000000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000725246FF725246FF000000000000
          000000000000000000003B1E00DF3B1E00E1010100053D1F00E80A0500262915
          009C2714009500000000301800B61E0F007127140093432200FE05020012361B
          00CD1D0F006D0000000000000000000000000000000000000000725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF725246FF725246FF0000
          00000000000000000000311900BC361B00CE1D0F006E351B00CB0301000B1F10
          00752A1500A1010000022C1600A71B0E006622110081361C00CF2010007A3019
          00B81B0D00650000000000000000000000000000000000000000725246FFAA9F
          8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAA725246FF725246FFAA9F8CAA0000
          00000000000000000000381D00D70D070032341A00C6432200FF010100042312
          00852915009D000000002D1700AA1C0E00692A1500A0180C005B271400934322
          00FF130A00480000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000AA9F8CAA725246FF00000000000000000000
          00000000000000000000311900B90D0700310804001F371C00D3020100091B0E
          0066301800B71D0F0070311900BC150B005024120089190D00600301000B351B
          00C8130A00490000000000000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000AA9F8CAA725246FF00000000000000000000FFFF0000
          0000000000000000000000000000000000010000000000000000000000010000
          0000020100080503001302010007000000000000000000000001000000000000
          000001000002000000000000000000000000000000000000FFFF725246FFAA9F
          8CAA0000000000000000000000000000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000100000000000000000000000000000000000000000000FFFF725246FFAA9F
          8CAA0000000000000000000000000000000000000000000000000000FFFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000FFFF725246FFAA9F
          8CAA0000000000000000000000000000000000000000000000000000FFFF0000
          FFFF0000FFFF0000FFFF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000001F5942BE2C795CFF2B76
          5AFF297558FF297356FF297255FF277153FF266E52FF266D52FF266C4FFF246A
          4FFF23694DFF23674DFF23664BFF226649FF226449FF216348FF206147FF2061
          45FF206045FF174531C000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000034886AFF7ED9BDFF6BD1
          B3FF69D0B1FF68D0B0FF66D0B0FF66D0B0FF64D0AFFF63D0AEFF62CEADFF61CE
          ADFF60CEABFF5ECDABFF5ECDAAFF5DCDA9FF5DCCA9FF5BCCA7FF5ACBA7FF5ACA
          A7FF59CAA6FF226548FF00000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000000000000000000000000000368C6DFF8BDCC3FF4FC6
          A0FF4EC69FFF4CC59FFF4CC59DFF4BC49BFF49C29BFF48C29AFF47C199FF45C1
          99FF44C197FF43C096FF42BF95FF41BF95FF3FBF95FF3FBE94FF3EBD92FF3DBD
          91FF5CCDA9FF24694DFF00000000000000000000000000000000725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF3A9374FFA5E5D2FF57CB
          A6FF56CAA5FF55CAA4FF53CAA4FF53C9A3FF51C8A2FF50C8A2FF4FC7A0FF4FC6
          9FFF4DC69EFF4CC59EFF4BC59CFF49C49AFF48C39AFF48C29AFF46C199FF45C1
          98FF64D0AEFF2A7659FF00000000000000000000000000000000725246FFAA9F
          8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAA3C9778FFB2E9D9FF5CCD
          AAFF5BCDA9FF59CDA8FF58CCA6FF57CAA6FF56CAA5FF55CAA5FF54C8A4FF52C8
          A3FF50C8A2FF4FC7A1FF4FC6A0FF4EC69FFF4DC59FFF4CC59DFF4BC49CFF4AC3
          9BFF69D0B0FF2E7C5EFF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003D9B7BFFBDEDDFFF60CF
          ADFF5FCFACFF5ECFABFF5CCFAAFF5CCEAAFF5ACCA9FF59CBA7FF59CBA6FF57CA
          A6FF56C9A5FF54C9A4FF53C9A3FF52C8A2FF50C8A1FF50C7A0FF4FC6A0FF4EC6
          A0FF6DD4B4FF318364FF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003F9F7FFFC8F0E4FFC7EF
          E3FFC5EFE2FFC2EEE1FFC0EDE1FFBEEDDFFFBDEDDEFFBBECDDFFB9EBDCFFB6EA
          DBFFB4EADAFFB2E9D8FFAFE8D6FFADE7D5FFAAE6D4FFA7E5D3FFA5E5D2FFA3E4
          D1FFA1E3CFFF34896AFF00000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000030785FC140A181FF40A0
          80FF3F9F7FFF3F9E7EFF3F9D7DFF3D9B7CFF389575FF3D9A7AFF3D9979FF3B98
          78FF3B9777FF3B9577FF3A9576FF3A9475FF399374FF389272FF389171FF388F
          70FF388E6FFF286751C200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000101
          0819060624740201094800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000010104112127
          77B5475AF1FF242FA8EC0101062F000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000001F5942BE2A5768FF6073
          FAFF4A62E9FF2F48EBFF214870FF277153FF266E52FF266D52FF266C4FFF246A
          4FFF23694DFF23674DFF23664BFF226649FF226449FF216348FF206147FF2061
          45FF1A4541FF13392DCB00000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000034886AFF567FB1FF5F73
          F7FF3E52DFFF2640DDFF3055BDFF5EBFA4FF64D0AFFF63D0AEFF62CEADFF61CE
          ADFF60CEABFF5ECDABFF5ECDAAFF5DCDA9FF5DCCA9FF5BCCA7FF5ACBA7FF4BA4
          90FF2B42A9FF1F584CFF00000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000000000000000000000000000368C6DFF6189CFFF5469
          E8FF384EDEFF223BD8FF1C2FC9FF41A689FF49C29BFF48C29AFF47C199FF45C1
          99FF44C197FF43C096FF42BF95FF41BF95FF3FBF95FF3FBE94FF3EBD92FF2854
          99FF2D50A0FF24694DFF00000000000000000000000000000000725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF3A9374FF7EA7D4FF4E63
          E5FF344BDCFF2039D4FF1B31D5FF377E7FFF51C8A2FF50C8A2FF4FC7A0FF4FC6
          9FFF4DC69EFF4CC59EFF4BC59CFF49C49AFF48C39AFF48C29AFF378E85FF2430
          DFFF44888DFF2A7659FF00000000000000000000000000000000725246FFAA9F
          8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAA3C9778FFA8DBD9FF4C6C
          DBFF2F48DCFF1E37D1FF1833DAFF20399DFF4EB595FF55CAA5FF54C8A4FF52C8
          A3FF50C8A2FF4FC7A1FF4FC6A0FF4EC69FFF4DC59FFF45B091FF1C3DC0FF1839
          BDFF63C4A7FF2E7C5EFF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003D9B7BFFBDEDDFFF55A0
          ABFF384EDDFF1931D1FF1630D3FF1930D7FF30647AFF59CBA7FF59CBA6FF57CA
          A6FF56C9A5FF54C9A4FF53C9A3FF52C8A2FF50C8A1FF2D6096FF102FFDFF3278
          86FF6DD4B4FF318364FF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003F9F7FFFC8F0E4FFC7EF
          E3FF849ADBFF233DD6FF152ECEFF1732D9FF2131BCFF86A8A9FFB9EBDCFFB6EA
          DBFFB4EADAFFB2E9D8FFAFE8D6FFADE7D5FF678AA0FF1D39F5FF2C47BBFF9AD7
          C7FFA1E3CFFF34896AFF00000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000030785FC140A181FF40A0
          80FF419C83FF4A6EC1FF1C35D0FF122CCEFF1732DEFF1B3195FF348168FF3B98
          78FF3B9777FF3B9577FF3A9576FF2B6764FF263EDEFF1935E9FF2C6866FF388F
          70FF388E6FFF286751C200000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000F0F1F233943AFCC0F2ACCFF0D28CEFF2038E2FF11155EBC0000
          00130000000000000000070820512334C3EE1836F2FF11196BA7000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          000000000000000000000F0F2125323DB0D00722C9FF142FD3FF2D42E1FF0E10
          42A30000000C040514522234C2EC1231EEFF0E21A8E203030C23000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000D0D1E21303AA3C10A24CAFF1934D7FF2437
          CEFF0F145EBC1D2FC1F0102EE7FF0923D3FF090A255B00000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000B0B18182D3489A20B22BDFB0C24
          CAFF102AD5FF0823D0FF071EC3F812185D840000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000001F5942BE2C795CFF2B76
          5AFF297558FF297356FF297255FF277153FF266E52FF26555CFF0E23BCFF0017
          C3FF0019BEFF0019C9FF132786FF226249FF226449FF216348FF206147FF2061
          45FF206045FF174531C000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000034886AFF7ED9BDFF6BD1
          B3FF69D0B1FF68D0B0FF66D0B0FF5BB89EFF2D5B88FF0A25C1FF001DD5FF001A
          C9FF0019C7FF0017C7FF081DB0FF2E5F72FF5AC4A3FF5BCCA7FF5ACBA7FF5ACA
          A7FF59CAA6FF226548FF00000000000000000000000000000000725246FFAA9F
          8CAA0000000000000000000000000000000000000000368C6DFF8BDCC3FF4FC6
          A0FF4EC69FFF4ABC9BFF398288FF324DB2FF2942F1FF122FEEFF0321DFFF021F
          DBFF274CBBFF3152C2FF041BCCFF0317CCFF1D4A77FF39AC86FF3EBD92FF3DBD
          91FF5CCDA9FF24694DFF00000000000000000000000000000000725246FF7252
          46FF725246FF725246FF725246FF725246FF725246FF3A9374FFA5E5D2FF57CB
          A6FF479B8CFF466A9CFF6373EEFF596EF5FF4358E9FF2841E7FF1634F4FF2952
          B7FF4BB59DFF4EBAA4FF4878C4FF122CE3FF0C28EAFF21428DFF389A80FF45C1
          98FF64D0AEFF2A7659FF00000000000000000000000000000000725246FFE3D4
          BBE3AA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAAAA9F8CAA3C9778FF9BC9BEFF4D79
          91FF7183D7FF8998FEFF7485F5FF6E80F4FF576BEEFF4159F5FF3B66AFFF4EB8
          9EFF50C8A2FF4FC7A1FF4FC6A0FF5199BBFF405DE5FF4158F6FF3144ADFF317D
          76FF69D0B0FF2E7C5EFF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000003A756CFF6775CBFF7A8B
          F8FF8F9DF7FF8B99F0FF7F8EF0FF7A8AF2FF6379F0FF4981A6FF57C5A3FF57CA
          A6FF56C9A5FF54C9A4FF53C9A3FF52C8A2FF53B5ACFF5D8AD1FF7285FCFF5D69
          D7FF3C6980FF2C765CFF00000000000000000000000000000000725246FFAA9F
          8CAA00000000000000000000000000000000000000006B989FFF7B8DFCFF566A
          E0FF7A8AEAFF8291ECFF8595FAFF7E90DBFF95B6C7FFBBECDDFFB9EBDCFFB6EA
          DBFFB4EADAFFB2E9D8FFAFE8D6FFADE7D5FFAAE6D4FFA7E5D3FF8FB8D7FF879D
          ECFF7F8AF2FF354991FF0102115F000000090000000000000000725246FFAA9F
          8CAA0000000000000000000000000000000000000000357963C586B9BEFF798E
          DFFF6E82E4FF8499E9FF58859CFF3E927CFF389575FF3D9A7AFF3D9979FF3B98
          78FF3B9777FF3B9577FF3A9576FF3A9475FF399374FF389272FF389171FF3A8B
          79FF4B849BFF6980D5F24A53C7EF161856B202020A3B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000725246FFAA9F
          8CAA000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000FFFF0000
          FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000
          FFFF00008E8E0000555500005555000055550000555500005555000055550000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000055550000
          55550000555500005555000055550000555500008E8E0000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001020100070000000000000000000000000000000001000002020100070000
          0000010100040302000C06030018060300180603001806030018020100060000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001008
          003B3F2000F104020010000000000100000300000000140A004D3F2000F10000
          00002C1700A9391D00DA2A15009F2B1600A32A15009F2B1600A20B0500280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001000002000000001108
          003F432200FF05020012000000000101000500000000160B0053432200FF0000
          0000331A00C4301800B500000000000000000000000000000000000000000000
          0000000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001000003000000001008
          003E432200FE02010009000000000000000000000000140A004C432200FE0000
          0000321900BF301800B500000000000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001000002000000001108
          003F432200FF0C06002F0904002109050024080400201B0E0068432200FE0000
          0000321900BD321A00C0080400200A0500250804002009040021020100080000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001000003000000001108
          003F432200FF2A1500A12915009D2A15009E2915009C311900BC422100FB0000
          00002F1800B43C1E00E32815009A2A15009F2815009A2915009D0A0500260000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001000003000000001008
          003E432200FE0000000000000000000000000000000012090044432200FE0000
          0000321A00C02F1800B100000000000000000000000000000000000000000000
          0000000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001000002000000001109
          0040432200FF06030016010100050201000901010005170C0057432200FF0000
          0000341A00C5311900B900000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001000003000000000F08
          00393D1F00E90402000F000000000100000300000000130A004A3D1F00E90000
          00002B1600A3381C00D52A1500A12B1600A52A1500A12B1600A40B0500280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000101000504020010040200100402001004020010010100040000
          0000000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF000055550000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000055550000FFFF000000000000
          FFFF00008E8E0000555500005555000055550000555500005555000055550000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000055550000
          55550000555500005555000055550000555500008E8E0000FFFF000000000000
          FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000FFFF0000
          FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000030000
          000B000000160000001A000000140000000D0000000700000004000000020000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000010000000100000002000000060000
          0016452E26FF241510DA100A0788020101350000001C00000011000000090000
          0004000000020000000100000001000000010000000100000001000000010000
          0001000000000000000000000000000000000000000000000000000000000000
          00000000000100000002000000040000000600000007000000080000000D0000
          001E4E382FD8A07C6FFF544649FF292C47F716254FB5070C1A5C0000001E0000
          00120000000B0000000800000007000000080000000800000008000000070000
          0005000000020000000100000000000000000000000000000000000000000000
          000000000002000000070000000F00000016000000190000001B0000001E0000
          002B281D188D82706CFFADCAE6FF7492BFFF425D99FF2E478AFF101D3FA50000
          003500000027000000200000001E0000001D0000001D0000001D0000001A0000
          0012000000080000000200000000000000000000000000000000000000000000
          0001000000040000000F78554AC1A57666FFA57565FFA57465FFA47363FFAB85
          78FFAF968DFF5C5F79FFDBE3EFFFA2C2E2FF7896C2FF536DA2FF315194FF575E
          84FFB59C94FFAC897CFF9D6D5DFF9D6E5EFF9E6D5DFF9E6D5DFF9E6C5CFF714E
          42C2000000130000000400000001000000000000000000000000000000000000
          00010000000500000014A77868FFFCF8F6FFFBF8F6FFFBF8F5FFFAF6F3FFF5F1
          EEFFE7E5E4FF7D8FB5FF8B9FC7FFCED9E9FF2084DBFF51A9E2FF1552AAFF154B
          9EFF6D7EA6FFE1DFDCFFEFEAE8FFF4EEEAFFF7F0EBFFF8F1EBFFF7EFEAFF9F6D
          5DFF000000190000000700000001000000000000000000000000000000000000
          00010000000600000016AA796AFFFCF9F7FFF6ECE6FFF6ECE6FFF5EBE5FFF3E9
          E2FFEBE5E1FFB9BEC9FF4B6CABFFFFFFFFFF4DA3E5FF2389DFFF56ADE3FF1658
          ACFF174FA0FF6C7FA6FFE0DBD7FFECE5DFFFF0E5DEFFF2E8DEFFF8F1EBFF9F6F
          5FFF0000001B0000000700000001000000000000000000000000000000000000
          00010000000500000015AA7C6CFFFCFAF8FFF7EDE8FFF7EDE8FFF6ECE6FFF3EC
          E5FFF0E9E3FFE7E3E0FF8C9BBBFF6990C2FFC2E5F8FF50A7E6FF2791E0FF5DB1
          E5FF1A5DAFFF1953A4FF6E82A9FFE1DCD9FFECE5DFFFF0E6DFFFF7F1EDFFA170
          60FF0000001B0000000700000001000000000000000000000000000000000000
          00010000000500000015AC7D6FFFFDFAF8FFF7EFE9FFF7EDE8FFF7EDE9FFF5EC
          E7FFF4EBE4FFF1EAE5FFE6E1DEFF82A1C1FF4590D0FFC7E8F8FF56AEE7FF2C97
          E1FF62B5E6FF1B62B2FF1B57A6FF7185ACFFE4DEDAFFEDE6E1FFF5EFEBFFA272
          62FF0000001A0000000700000001000000000000000000000000000000000000
          00000000000500000014AD7F70FFFDFAF9FFF7F0EAFFF7EFE9FFF7EFE9FFF7EF
          E9FFF6EEE7FFF4EBE6FFF1EAE6FFE7E3DFFF84A4C5FF4895D2FFCBEBFAFF5AB4
          EBFF319DE4FF67BAE7FF1E65B6FF1D5DA9FF738AAFFFE4DFDBFFF1EDEAFFA272
          62FF0000001A0000000600000001000000000000000000000000000000000000
          00000000000500000013AE8172FFFDFBFAFFF7F0EAFFF7F0EAFFF7F0E9FFF6EF
          E9FFF7EFE9FFF6EEE7FFF5EDE7FFF1EAE6FFE8E4E1FF86A7C7FF4C9AD4FFCFEE
          FBFF60B9EDFF36A5E9FF6EBEE8FF206BB9FF2061ADFF758DB2FFE9E6E5FFB38E
          83FF0000001A0000000700000001000000000000000000000000000000000000
          00000000000400000012AF8475FFFDFCFAFFF8F1EBFFF8F1EBFFF8F0EBFFF7F0
          EBFFF7F0EAFFF9F4F0FFFBF8F4FFF7F1ECFFF2ECE8FFEAE6E3FF88AACAFF4F9F
          D7FFD3F0FCFF64BEEFFF3BABEBFF72C2EAFF2371BCFF2266AFFF7992B8FFBFA5
          9DFF0000001E0000000800000002000000000000000000000000000000000000
          00000000000400000011B18676FFFDFCFBFFF8F1EDFFF8F2ECFFF8F1EBFFF7F1
          EBFFCCB0A5FFA37363FFD1B8ADFFF7F0EAFFF5EDE8FFF3ECE9FFEBE7E5FF8BAE
          CCFF52A3D8FFD6F2FCFF6AC5F0FF42B2EBFF77C6ECFF2677C0FF256BB4FF6877
          9AFF000000260000000D00000003000000010000000000000000000000000000
          00000000000400000010B48878FFFDFCFCFFF9F2EDFFF8F2ECFFF8F2ECFFCCB0
          A5FFA57565FFD2BBB2FFF7F0EBFFF8F1EAFFF8F0EAFFF5EEE8FFF4EEE9FFEDEA
          E7FF8DB1CFFF56A8DAFFD9F4FDFF89D5F6FF47BBEFFF7BCBECFF2A7DC1FF2871
          B7FF191C26980000001400000007000000020000000000000000000000000000
          0000000000040000000FB58979FFFEFDFCFFF9F3F0FFF8F2EDFFF8F2EDFFB790
          83FFAC8071FFFCF8F6FFF8F1EDFFF8F1ECFFF8F1EBFFF8F0EBFFF7EFE9FFF5EE
          EBFFEEEBE8FF7A95B0FF5AADDCFFDCF6FDFF74CFF6FF4CC0F0FF80D0EEFF7877
          7BFF794A3AFF29110B900000000F000000060000000200000000000000000000
          0000000000030000000EB78C7DFFFEFDFCFFF9F4F0FFF9F3F0FFF9F3EFFFB993
          85FFA87A6AFFF6F0EDFFF9F4EFFFF8F1ECFFF8F1EDFFF7F1ECFFF8F1ECFFF6EF
          EAFFF5EFEBFFC5ACA4FF7E9BB4FF5DB0DEFFDEF8FEFF7AD6F7FF90B9C5FFD8C1
          B3FF996B59FF815141FF190D228D0000000C0000000500000001000000000000
          0000000000030000000DB78E7FFFFEFDFDFFF9F4F1FFF9F4F0FFF9F3F1FFCAAC
          A1FFAA7B6BFFE6D7D2FFFAF3F1FFF8F3EFFFF8F2EFFFF8F1EDFFF8F1ECFFF8F1
          ECFFE6D7D0FFB68F83FFD4C2BBFF94BAD7FF60B4E0FFE7F7F9FFDBC3B1FFCFB3
          9FFFDBC5B8FF584B8BFF212095FF0A0A3E8B0000000B00000004000000000000
          0000000000030000000CB98F80FFFEFEFDFFFAF4F1FFFAF6F1FFFAF4F1FFD0B6
          ACFFAB7E6EFFEBDED9FFF9F3EFFFF9F3EFFFF9F3EFFFF8F2EFFFF9F2EFFFF8F2
          EFFFEDE1DBFFA37665FFCCB2A8FFF5F3F1FFA9AEB7FFB58F7EFFFAF0E2FFDFC8
          B6FF9A97C5FF8791E3FF2B2EAAFF25259CFF0F0F458900000006000000000000
          0000000000030000000BBB9182FFFEFEFDFFFAF6F3FFF9F6F2FFEFE7E1FFB891
          83FFD1B8AEFFFAF4F0FFF9F4F0FFF9F4F0FFF9F3F0FFF9F3EFFFF9F3EFFFF9F2
          EFFFF9F3EDFFD2B9AEFFAA7E6FFFF0E7E2FFF6F4F3FFC2A69EFFBC9788FFF6EE
          E7FF8B9FEDFF6275E1FF98A2E7FF7B85D0FF272A96FF00000007000000000000
          0000000000030000000BBC9384FFFEFEFDFFFAF7F3FFFBF7F3FFB58C7CFFD3BA
          AEFFFCFAF8FFFCF9F6FFFAF4F1FFF9F4F1FFF9F4F0FFF9F3F1FFF9F4F0FFF9F3
          F0FFF9F3F0FFF9F3EFFFD4BCB3FFA57867FFF4EDE9FFF5F2EFFFB1A6BEFF6B78
          D4FFDBE7FCFF8BA0EEFFACC0F3FF4E57C3FF21235A8400000004000000000000
          0000000000020000000ABD9685FFFEFEFEFFFBF8F4FFFAF7F4FFF5EFEBFFBB95
          86FFC9AB9FFFFDFBF9FFFAF7F4FFFAF6F2FFFAF6F2FFFAF6F1FFF9F4F1FFF9F4
          F1FFF9F4F1FFC4A498FFB48B7DFFF0E6E1FFF8F3EFFFF8F3F0FFFAF8F8FF8E86
          B8FF7180DAFFDCE8FCFF6675D7FF262A60830000000500000002000000000000
          00000000000200000009BF9787FFFFFEFEFFFBF8F6FFFBF8F4FFFAF7F6FFD4BC
          B2FFB28676FFE9DCD6FFFAF8F4FFFAF6F3FFFAF6F2FFF9F6F3FFFAF6F2FFF9F4
          F2FFEADDD7FFAB7E6DFFC6A69BFFFBF7F6FFF9F3F0FFF9F3F0FFFCFBFAFFD2BA
          B2FF2B3166885761CEFF2C316681000000040000000200000001000000000000
          00000000000200000008C09989FFFFFEFEFFFBF8F7FFFBF8F6FFFBF8F6FFCCAF
          A3FFB48979FFF3ECE8FFFAF7F4FFFBF7F4FFFAF7F4FFFAF6F3FFFAF6F3FFFAF6
          F2FFF5EEE9FFAD806FFFC19F93FFFCF9F8FFF9F4F1FFF9F4F0FFFDFBFAFFC6A4
          98FF0000000F0000000500000002000000010000000000000000000000000000
          00000000000200000007C19A8BFFFFFFFEFFFBF9F7FFFBF9F7FFFBF9F6FFC3A3
          94FFBA9383FFFDFCFBFFFCF9F8FFFBF8F4FFFAF7F4FFFBF7F4FFFAF7F4FFFAF7
          F3FFFAF7F3FFB38A7AFFB3897AFFFBF8F7FFFAF4F1FFF9F4F1FFFDFCFAFFA576
          65FF0000000C0000000300000000000000000000000000000000000000000000
          00000000000200000007C19B8CFFFFFFFFFFFBF9F8FFFBF9F8FFFDF9F7FFCEB3
          A6FFB78D7DFFE5D6CFFFFDFCFBFFFCFAF8FFFBF8F6FFFAF8F4FFFBF8F6FFFBF7
          F4FFE3D2CBFFB08474FFBE9B8DFFFAF7F2FFFAF6F2FFFAF6F2FFFDFCFBFF9A6A
          5BFF0000000B0000000300000000000000000000000000000000000000000000
          00000000000100000006C39D8DFFFFFFFFFFFDFAF8FFFDFAF9FFFDF9F8FFFDFA
          F8FFD4BAAFFFB78F7EFFDBC7BEFFFBF9F7FFFBF9F6FFFBF8F6FFFBF8F6FFE3D3
          CCFFB28877FFCAACA0FFF5F0ECFFFAF7F4FFFAF6F3FFFAF7F2FFFEFDFCFFA26F
          60FF0000000B0000000300000000000000000000000000000000000000000000
          00000000000100000005C39D8FFFFFFFFFFFFEFAF9FFFDFAFAFFFDFBF9FFFDFA
          F9FFFDFAF8FFFDFAF9FFFDF9F8FFFBF9F7FFFBF9F8FFFBF9F7FFFBF9F7FFFBF9
          F6FFFBF9F6FFFAF8F6FFFBF8F4FFFBF7F4FFFBF8F4FFFAF7F3FFFEFDFCFFA677
          67FF0000000A0000000200000000000000000000000000000000000000000000
          00000000000100000004C49E90FFFFFFFFFFFDFBFAFFFDFBFAFFFDFBF9FFFDFB
          F9FFFEFAF9FFFDFAF9FFFDFAF8FFFDFAF8FFFBFAF9FFFDFAF8FFFDF9F8FFFBF9
          F7FFFBF9F7FFFBF9F7FFFDF9F6FFFBF8F6FFFBF8F4FFFBF7F6FFFEFDFCFFAA7C
          6CFF000000090000000200000000000000000000000000000000000000000000
          00000000000100000004C5A190FFFFFFFFFFFEFDFBFFFDFBFBFFFDFDFBFFFEFB
          FAFFFEFBFAFFFDFBF9FFFDFBF9FFFDFAF9FFFDFBF9FFFDFAF8FFFDFAF8FFFDFA
          F8FFFDFAF8FFFBF9F8FFFDF9F7FFFBF8F7FFFBF8F6FFFBF8F6FFFEFDFDFFB084
          75FF000000080000000200000000000000000000000000000000000000000000
          00000000000100000003C6A191FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFEFEFFFFFEFEFFFFFEFEFFFEFEFEFFFFFEFEFFFEFEFDFFFFFEFDFFB58B
          7AFF000000060000000200000000000000000000000000000000000000000000
          0000000000000000000293776CBEC6A291FFC6A192FFC6A191FFC59F91FFC69F
          92FFC59F91FFC59F90FFC59F91FFC49F90FFC49E90FFC49D8FFFC49E8EFFC39D
          8EFFC39D8EFFBC9284FFBC9284FFBC9183FFBC9182FFBC9182FFBA9082FF8A6A
          5FBF000000040000000100000000000000000000000000000000000000000000
          0000000000000000000100000001000000020000000200000003000000030000
          0003000000030000000300000003000000030000000400000004000000040000
          0004000000040000000500000005000000050000000500000005000000050000
          0004000000020000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010000000100000004000000070000000A0000000B0000000B0000
          000B0000000B0000000C0000000B0000000A0000000800000004000000020000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00010000000300000007000000100000001B0000002200000027000000280000
          0028000000280000002800000028000000240000001C00000011000000080000
          0003000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000200000007000000121D130A476D4626BA986234F2A36937FFA26836FF9E6C
          41FF91857CFF9A704EFF9E6332FF985E30F8653E1FB71D12094D000000150000
          0009000000030000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          0007000000135239237CCF935BFBE2A76EFFE9B47DFFD0965AFFB5783BFFB679
          3AFFB98D63FFC3B9B3FFBB9168FFB57839FFA46A31FF8F5525FD3E230F8D0000
          00190000000A0000000300000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000060000
          0012523A237CDA9E67FFEBB37CFFECB47DFFECB47CFFEEB984FFD1975CFFB578
          3BFFB5783BFFBA8E64FFC4BAB7FFBB926AFFB4783BFFB4783BFF9D622DFF4427
          1095000000180000000900000002000000010000000000000000000000000000
          00000000000000000000000000000000000000000000000000000000000C5138
          2375DA9F68FFEDB77FFFECB67FFFEDB67FFFEDB67FFFECB67FFFEFBD89FFD198
          5DFFB5793BFFB5793BFFBB8F65FFC6BCB9FFBB926BFFB5783BFFB5783BFFA064
          2FFF44281194000000180000000A000000020000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000010C38A
          57F1ECB781FFEDB882FFEDB882FFEDB981FFEDB882FFEDB882FFEFB882FFEFC0
          8EFFD2995FFFB6793CFFB6793CFFBB9066FFC8C0BAFFBC946DFFB77A3BFFB77A
          3BFF2229A5FF02064B9600000017000000090000000200000000000000000000
          00000000000000000000000000000000000000000000000000000000000FCA99
          6AF0F1C594FFEFBA85FFEFBA84FFEFBA84FFEFBA85FFEFBA84FFEFBA84FFEFBA
          84FFF1C492FFD39A61FFB67A3DFFB6793DFFBD9168FFCBC2BCFFBE956EFF4147
          B7FF3140C8FF1B28AFFF02074C96000000170000000900000002000000000000
          00000000000000000000000000000000000000000000000000000000000A5541
          2D73DFAB77FFF3CB9DFFF0BB87FFEFBB87FFF0BC87FFF0BB87FFF0BC87FFF0BC
          87FFF0BB87FFF2C797FFD39B63FFB67A3EFFB67A3EFFBE9469FFCBC4C2FF6D74
          C7FF3342C9FF3342C9FF1C29B1FF02084D950000001600000009000000020000
          0000000000000000000000000000000000000000000000000000000000040000
          000D543D2877E0AD7BFFF5D0A2FFF0BF8BFFF1BE8BFFF1BE8BFFF1BF8BFFF1BF
          8BFFF1BE8BFFF0BF8BFFF3CB9EFFD49D65FFB67B3FFF464BBAFF6870C9FFCEC6
          C5FF6F76C8FF3545CBFF3645CBFF1E2BB2FF03094D9400000015000000080000
          0002000000000000000000000000000000000000000000000000000000010000
          00050000000D543E2976E1B07FFFF6D4A9FFF1C08DFFF1C08DFFF1C18DFFF1C0
          8DFFF1C18DFFF2C18DFFF1C18EFFF5CEA2FF6374D7FF3748CDFF3747CCFF6A73
          CBFFCFC9C7FF717ACBFF3748CDFF3747CDFF1F2EB3FF030A4D94000000140000
          0008000000020000000000000000000000000000000000000000000000000000
          0001000000050000000D553F2A75E1B384FFF7D7AEFFF2C290FFF2C290FFF2C2
          90FFF2C290FFF2C290FFF2C290FF8EA1E5FF99B1F4FF576FDFFF394AD0FF3A4A
          CFFF6C75CDFFD3CCC9FF747CCDFF394ACFFF394ACFFF212FB6FF040A4E930000
          0014000000080000000200000000000000000000000000000000000000000000
          000000000001000000050000000C55402C74E3B687FFF8DBB4FFF3C492FFF3C4
          93FFF3C493FFF3C493FF95A8E6FF7C99F0FF7B98F0FF9EB7F4FF5B72E0FF3C4D
          D1FF3C4DD1FF6F78D0FFD4CECEFF767ECFFF3C4DD1FF3C4DD1FF2332B7FF050B
          5092000000130000000700000002000000000000000000000000000000000000
          00000000000000000001000000040000000B56412D74E3B88BFFF9DEB7FFF4C6
          96FFF3C695FF9BADE8FF839EF1FF829DF1FF7F9CF1FF7F9BF0FFA4BDF6FF5E76
          E2FF4050D3FF4050D3FF727BD2FFD6D0D0FF7A81D2FF4050D3FF4050D3FF2534
          B9FF050B50920000001200000007000000020000000000000000000000000000
          0000000000000000000000000001000000040000000B56422E73E4BA8FFFFAE1
          BCFFA1B2E8FF88A4F2FF87A2F2FF87A2F2FF85A0F1FF839FF2FF829EF1FFAAC1
          F7FF6178E3FF4355D5FF4355D6FF757FD4FFD9D4D2FF7E87D4FF4355D5FF4355
          D5FF2737BBFF060C519100000011000000070000000100000000000000000000
          000000000000000000000000000000000001000000040000000A56432E728B99
          D6FFB9D1F9FF8DA7F3FF8CA6F2FF8AA5F2FF89A4F2FF88A3F2FF86A2F2FF86A1
          F2FFB0C6F8FF657BE4FF4657D7FF4657D7FF7781D7FFDBD6D6FF808AD7FF4658
          D7FF4657D7FF293ABDFF060D51900000000F0000000500000001000000000000
          00000000000000000000000000000000000000000001000000040000000A2029
          52717A8EDEFFBED4FAFF91AAF3FF8EA9F3FF8DA9F3FF8CA7F3FF8BA6F3FF8AA5
          F3FF89A4F2FFB5CAF8FF687FE6FF475BDAFF475BDAFF7985DAFFDED9D9FF838D
          D9FF475AD9FF485BDAFF2333B7FF050A3E6E0000000900000002000000000000
          0000000000000000000000000000000000000000000000000001000000030000
          0009212B52717F94DEFFC5DBFAFF9DB6F5FF9CB7F5FF9AB3F4FF97B2F4FF95AE
          F4FF8FABF4FF8DA8F3FFB9CEF9FF6A82E7FF4A5EDBFF4A5EDBFF7B88DBFFDFDC
          DCFF8590DBFF4A5EDBFF4357D2FF0A0F70CD0000000A00000002000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          000300000008252F53708CA1E3FFC9DFFBFFA1BAF5FFA0BAF5FF9FB8F5FF9DB7
          F5FF9CB5F5FF9BB4F5FF95AFF4FFBED4F9FF6C85E8FF4B61DCFF4B61DCFF7D8B
          DEFFE2DEDFFF8A97DEFF3444C1FF090D51890000000800000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000100000003000000082C36567093A8E5FFCCE2FBFFA5BEF6FFA4BCF6FFA3BB
          F5FFA1BBF6FFA0BAF5FF9FB8F5FF9EB7F5FFC5D8F9FF6E86E9FF4D63DEFF4D63
          DFFF8694E1FFDBD8D7FF3B3F77B4000002100000000400000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000100000003000000072E38576F98ADE7FFCFE4FCFFA8C1F7FFA7C0
          F7FFA6BEF6FFA5BEF6FFA4BDF6FFA2BBF6FFA1BAF5FFC9DDFBFF738BEAFF5B71
          E3FF5162D3FF353978B402020210000000050000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000100000003000000072F39576E9BB0E7FFD0E5FDFFABC5
          F8FFAAC3F7FFA9C2F7FFA8C2F7FFA7BFF6FFA6BEF6FFA5BEF6FFD7EAFDFF8398
          E6FF121B77B30000020F00000005000000020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000010000000200000006313B586D9DB3E7FFD1E7
          FDFFAEC7F8FFADC6F8FFACC5F8FFABC4F7FFAAC2F7FFC6DDFBFFAABEEEFF4858
          90B20101020E0000000500000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000200000006333D586DA0B5
          E8FFD2E7FDFFB1C8F8FFAFC7F8FFAFC7F8FFCBE2FCFFB2C5F0FF4E5F93B20102
          030D000000040000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000010000000200000005343E
          586DA3B7E8FFD5E9FDFFBAD0F9FFD1E7FDFFB7C9F0FF536595B10102030B0000
          0004000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          0005343F596B9DB2E6FFC9DDF7FFA7BCEBFF576896B00102030A000000030000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000200000004293044535B6D9AB23B4663760202030800000002000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010000000200000003000000030000000200000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000010000
          0002000000050000000700000008000000080000000800000009000000090000
          000900000009000000090000000A0000000A0000000A0000000A0000000A0000
          000B0000000B0000000B0000000B0000000B0000000B0000000C0000000C0000
          000C0000000C0000000C0000000B000000090000000400000001000000020000
          000800000012000000180000001B0000001C0000001D0000001D0000001E0000
          001F0000001F0000002000000021000000220000002300000023000000240000
          00250000002500000026000000270000002800000028000000290000002A0000
          002B0000002B0000002B000000290000001F0000000E00000003000000030000
          000E000000320000003300000034000000350000003600000037000000380000
          00390000003A0000003B0000003C0000003D0000003E0000003F000000400000
          0041000000420000004300000044000000450000004600000047000000480000
          00490000004A0000004B0000004C0000004D0000001C00000007000000030000
          000E000000230000002400000025000000260000002700000028000000280000
          00290000002A0000002B0000002C0000002D0000002E0000002F000000300000
          0031000000310000003200000033000000340000003500000036000000370000
          0038000000390000003A0000003B0000003C0000002000000008000000020000
          000900000016000000170000001700000018000000190000001A0000001A0000
          001B0000001C0000001D0000001E0000001E0000001F00000020000000210000
          0022000000230000002300000024000000250000002600000027000000280000
          0028000000290000002A0000002B0000002C0000001C00000007000000010000
          00040000000B0000000B0000000C0000000C0000000D0000000E0000000E0000
          0010000000110000001100000011000000120000001200000013000000140000
          00140000001500000016000000170000001700000018000000190000001A0000
          001A0000001B0000001C0000001D0000001E0000001100000005000000000000
          0001000000030000000400000004000000040000000500000007000000090000
          000D000000100000000E0000000B0000000A000000090000000A0000000B0000
          000B0000000C0000000C0000000D0000000E0000000E0000000F000000100000
          00100000001100000012000000110000000D0000000600000002000000000000
          00000000000000000000000000000000000000000000000000020000000B0000
          001A000000220000001C0000000E000000050000000100000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000005000000191E21
          5A9D2E30A3FF03022CA20000002A000000140000000800000004000000020000
          0001000000010000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000001000000070000001E4951
          BEFF6E88F7FF2836ACFF03022CA90000003500000021000000160000000F0000
          000C000000080000000300000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000010000000500000017494D
          719C6F81E9FF4E6BF3FF37326AFF392627F3211513A70A07055D000000330000
          002C000000210000001200000007000000020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000020000000B0000
          0022494D71A15D5B8AFF684C41FF7B5C4FFF64473DFF463029FF3E2822FF3C27
          21FF2E1C1AD90201013C00000016000000070000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000002000000050101
          01110101012D725E58F0B0988BFF876558FF745346FF5B3B30FF56382DFF5737
          2EFF2F1A15FF130C2DDD0101073F010101180000000800000002000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0003000000030000000400000004000000050000000500000005000000070000
          000C0D0D0D2B443730A6806254FFBAA193FF82604FFF67473BFF634338FF3A21
          1AFF483977FF1F1E9FFF0A0753E009090E490404041E00000009000000030000
          0000000000000000000000000000000000000000000000000000000000060000
          000A0000000D0000000F00000010000000110000001200000013000000150000
          00191B1B1B3E3F3A37887C6153FFBAA08EFFA17D67FF876655FF4D3027FF5545
          85FF4647CDFF3636C4FF1E1CA1FF0F0C5BE716161C5E07070722000000080000
          0001000000000000000000000000000000000000000000000000112533513775
          A2E9397FB0FF377EB0FF357BAEFF347AADFF3278ABFF3077ABFF2E76A9FF2D74
          A7FF3D7AA9FF82A0B6FF7F6556FFAC8D7FFFA88672FF755243FF5D4C8CFF4F50
          CFFF4040CAFF4041CCFF3A3AC6FF2120A4FF141465F016161B5B0505051C0000
          00070000000100000000000000000000000000000000000000003E7EACF26DB2
          D6FF92D8F3FF8DD6F2FF89D4F2FF87D2F2FF83D0F0FF80CDF0FF7BCBF0FF79C8
          EFFF74C3E7FFBCD3DFFF816657FF9C7D6FFF886756FF645492FF575BD2FF4243
          CCFF4242CBFF4343CBFF4445CDFF3E3EC6FF2323A8FF110F60E609090F450101
          0115000000070000000100000000000000000000000000000000458AB8FF9ADB
          F4FF9ADBF5FF96DAF4FF91D8F3FF8DD5F2FF8BD4F2FF87D2F2FF83CEF1FF80CC
          F0FF7DC9EAFFCADBE4FF8A796DFF826355FF70629DFF6164D6FF4546CDFF4546
          CCFF4546CDFF4546CCFF4647CDFF4949CFFF4142CAFF2626AAFF0E0C5EDF0202
          0839000000120000000600000001000000000000000000000000488DBBFFA5DF
          F6FFA0DEF6FF9BDCF5FF9ADBF4FF8CB8C8FF756A61FF73685FFF72675EFF87D1
          F1FF71645DFFBEBAB7FFBCB9B8FF635F95FF8189DAFF5256D1FF474ACEFF474A
          CFFF474ACEFF474ACEFF474ACEFF484BD0FF4C50D1FF4347CBFF282AACFF0F0C
          61DC0101083500000011000000060000000100000000000000004C91BDFFA9E1
          F6FFA9E2F7FFA5DFF6FFA0DDF6FF776D64FF9ADBF4FF96DAF3FF736860FF8DD6
          F3FF71655CFF8BD1EFFFC4E1EEFFBBB9BFFF5F67C1FF7B82D9FF585CD5FF4B4E
          D1FF4B4ED1FF4B4ED1FF4B4ED1FF4B4ED2FF4D51D2FF5156D4FF484CCEFF2C2D
          B0FF100F64DB01010833000000100000000600000001000000005093C0FFACE2
          F5FFADE5F8FFACE3F7FFA7E1F6FF97BDCBFF776D65FF756B63FF756961FF96D9
          F3FF71665DFF8DD5F2FF8DD2F0FFBDB8B6FFC2D3E1FF646DC6FF858DDEFF5C63
          D7FF4F53D2FF4F53D3FF4F53D3FF4E53D3FF4F53D3FF5056D4FF555BD6FF4B50
          D1FF2F32B5FF121067DA010108320000000F00000005000000015397C2FFB5E7
          F9FFB3E7F8FFB1E6F8FFADE5F8FFACE3F7FFA7E1F6FFA4DFF6FF756B62FF9BDC
          F5FF73685FFF96DAF4FF91D7F2FF756A61FFC9E4F0FFC5D4E3FF6B76CAFF9099
          E2FF6A71DCFF5F67DBFF6169DCFF636BDDFF6169DCFF5E66DBFF5C63D9FF5C63
          D9FF5056D2FF3437B7FF15136CDA020109300000000E00000005589BC5FFBDE9
          F9FFB9EAFAFFB6E8FAFFB2E7F9FFB1E6F8FF797067FF786F65FF99BECAFFA4DF
          F6FF746A60FF72685FFF71675DFF8AB6C5FF96D7F1FFCBE6F1FFC7D8E6FF727D
          D1FF9EAAE8FF7782E3FF6972DFFF6972DFFF6872DFFF6872DFFF6872DFFF6973
          E0FF666FDEFF555DD4FF383DBBFF181871DB0202092D0000000C5B9DC7FFD6F2
          FBFFBEECFAFFBCEBFAFFB9EAFAFFB6E8FAFFB3E7F9FFB1E6F8FFADE5F8FFABE3
          F7FF756B62FFA4DFF6FFA0DEF5FF9DDCF5FF99DAF4FF99D9F2FFCCE8F2FFCADC
          E8FF7883D4FFA7B4ECFF7F8BE6FF707BE2FF6F7BE2FF6F7BE2FF6F7BE2FF6F7B
          E2FF707CE2FF707CE2FF5B63D7FF3D42BFFF1B1972D80101031C5998BEF1AACF
          E4FFEFFBFEFFECF9FDFFE9F8FDFFE6F7FDFFE1F6FDFFDEF5FDFFD9F3FCFFD4F1
          FBFF929592FFCCEEFAFFC8ECF9FFC1EAF9FFBCE7F8FFB6E5F8FFB4E2F5FFD3EB
          F4FFCCDDEAFF7D89D8FFB0BDEFFF8795E9FF7885E5FF7785E6FF7885E6FF7885
          E6FF7885E5FF7986E6FF7885E6FF6069D9FF363AB0FF12124891182832414C7F
          9FCA5EA0C9FF5C9EC8FF5A9CC6FF589AC5FF5598C3FF5397C2FF5194C0FF4F92
          BFFF4C90BDFF4A8EBCFF488DBAFF468AB8FF4388B7FF4186B6FF3F84B4FF4385
          B5FF89B1CCFF92B1CDFF7686D6FFB6C4F1FF8F9EEBFF7F8DE8FF7F8DE8FF7F8E
          E8FF7F8EE8FF7F8EE8FF808FE8FF7A88E6FF5E66D5FF2F2F94EB000000010000
          0002000000030000000400000004000000050000000600000006000000070000
          000800000008000000090000000A0000000B0000000B0000000C0000000D0000
          000E040404152E2E2E3F393A415A6872BDDEBCCAF3FF96A6EDFF8797EAFF8797
          EBFF8797EAFF8797EAFF8697EAFF8899EAFF8591E6FF3A3BA5F7000000000000
          0000000000010000000100000001000000010000000100000001000000020000
          0002000000020000000200000002000000020000000300000003000000030000
          000300000003010101070B0B0B1312121928656FB8D4C1CFF5FF9EAFF0FF8E9F
          EDFF8EA0EDFF8EA0EDFF8EA0EDFF98AAEFFF8D99E2FF343688C9000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000101010105090A111C6772B8D2C6D4F6FFB9CA
          F6FFA0B3F2FF98ABF1FFA3B6F3FFBECEF6FF727BCFFD15163251000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000100000003090A1019636DB3CDABB9
          EFFFCCDBF7FFD5E4FAFFBDCBF2FF7E8AD9FD282B526F00000008000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000001000000020303050B3E45
          71836671BBD4727CD1EE5860A5C0191B2E3E0000000600000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0003000000040000000400000004000000030000000100000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000010000000200000004000000050000000600000007000000070000
          0006000000050000000400000002000000010000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000010000
          0003000000060000000B0000001000000015000000180000001A0000001A0000
          001800000016000000110000000C000000070000000400000001000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000003000000070000
          000E0000001707140E4D133324951D4E38D11D513BD9246448FF246448FF1E51
          3BD91C4E38D11233249607140F4F000000190000001000000008000000040000
          0001000000010000000000000000000000000000000000000000000000000000
          000000000000000000000000000100000001000000040000000B000000150C21
          176B1F563FE1257151FF278963FF299D72FF2AA176FF2BB07FFF2BAF80FF2AA2
          76FF2A9E72FF278964FF267151FF20573FE20C21186D000000180000000D0000
          0005000000010000000100000000000000000000000000000000000000000000
          0000000000000000000100000001000000050000000D040C09361C503ACF2678
          56FF2AA074FF2CB180FF2BB180FF2CB081FF2CB081FF2CB180FF2CB181FF2CB1
          80FF2CB080FF2CB081FF2CB180FF2AA074FF277957FF1D5039D1040C093A0000
          0010000000060000000100000001000000000000000000000000000000000000
          00000000000000000001000000050000000E09191255236248F029946BFF2CB1
          81FF2CB181FF2CB181FF2CB282FF2CB282FF2CB282FF2CB282FF2DB282FF2DB2
          82FF2CB182FF2CB281FF2CB181FF2CB181FF2CB181FF29956CFF246248F10919
          135A000000120000000600000001000000000000000000000000000000000000
          000000000000000000040000000C09191253256C4EFA2A9F74FF2CB181FF2DB1
          82FF2CB283FF2DB283FF2DB283FF2EB283FF2EB284FF2EB384FF2EB383FF2EB3
          84FF2EB384FF2EB283FF2EB383FF2EB383FF2DB282FF2DB182FF2A9F74FF256D
          4FFA091A12590000001000000005000000010000000000000000000000000000
          00000000000200000009050F0B38246549EE2EA47AFF2EB383FF2DB283FF2EB3
          83FF2EB384FF2EB385FF2EB484FF51C7A2FF60CFAEFF37B98DFF2EB485FF2FB5
          85FF2FB485FF2EB485FF2EB485FF2EB384FF2EB383FF2EB384FF2FB384FF2FA5
          7AFF23644AEF050F0B3E0000000C000000030000000100000000000000000000
          000100000005000000101D513BCB2E9770FF30B385FF2EB384FF2EB484FF2EB4
          85FF2FB586FF2FB586FF50C6A1FF32916EFF1E7652FF4AB995FF30B587FF31B6
          87FF30B686FF30B587FF30B587FF2FB586FF2EB486FF2EB485FF2EB485FF30B4
          86FF2E9871FF1D533CD000000016000000070000000100000000000000000000
          00020000000A0C231A652D7D5CFF34B689FF2EB384FF2EB485FF2FB586FF30B5
          86FF31B688FF4EC59FFF389774FF7CAE9AFFA2C4B6FF2F8C6AFF41BF95FF32B7
          89FF32B789FF31B688FF31B689FF31B688FF30B588FF30B686FF2FB486FF2EB4
          85FF34B68AFF2D7E5EFF0D241A6B0000000E0000000300000001000000010000
          00040000000F205B43DD32A67DFF30B587FF2FB586FF30B587FF31B688FF31B7
          88FF4BC49DFF3E9F7CFF6BA28BFFF9F5F1FFF5EFEAFF45896CFF4CB491FF35BB
          8DFF34B98BFF34B98AFF33B98AFF32B989FF32B789FF32B688FF31B688FF30B5
          87FF31B688FF33A77DFF215E44DF000000160000000700000001000000020000
          0007091B144E308061FF37B98BFF30B587FF30B688FF31B688FF33B78AFF48C4
          9BFF46A786FF5A967DFFF6F4F0FFF3E8DFFFF3E8DFFFC8D9D0FF247A58FF4FC5
          9EFF35BB8DFF35BB8DFF34BA8CFF34BA8BFF34BA8BFF33B98AFF32B989FF31B7
          88FF31B688FF37B98CFF308162FF0A1D15570000000B00000002000000020000
          0009153B2B923A9B78FF35B98BFF32B788FF32B989FF33B98BFF46C39AFF4CAF
          8EFF4A8A6FFFF4F4F1FFF4EAE2FFF4E9E0FFF3E8E0FFF7EDE7FF699D87FF409D
          7CFF3FC094FF37BC8FFF37BC8EFF36BB8EFF36BB8DFF35BA8DFF34BA8BFF34B9
          8BFF32B78AFF36BA8CFF3B9C79FF153C2C980000000E00000003000000020000
          000B1E553FC63DAC86FF35B88CFF33B78AFF33B98BFF45C398FF54B797FF4084
          67FFEAEFEAFFF5EBE3FFF2E6DEFFEDDFD6FFF4E9E1FFF4E9E0FFE3E8E1FF2570
          50FF56C19EFF39BE91FF39BE90FF38BD90FF37BC8FFF37BC8EFF35BB8DFF35BB
          8CFF33B98BFF35B98DFF3EAD87FF1E5640CA0000001100000004000000030000
          000C256A4EEC3EB88EFF34BA8CFF35B98CFF3FBF94FF57BD9DFF347B5CFFE5EC
          E8FFF6EDE6FFF1E6DDFFCAC3B6FF9DAA97FFEFE3DCFFF4EAE2FFF6EBE5FF9DBD
          AFFF318464FF4FC8A2FF3ABF94FF3ABF92FF39BE91FF38BD90FF37BD8FFF36BB
          8EFF35BB8CFF35BB8CFF3FB98FFF256D50ED0000001300000005000000030000
          000C287455FA43C096FF35BA8BFF36BB8DFF37B488FF1F704EFFC9D0C8FFF7ED
          E7FFF1E5DEFFBEBCB0FF2E7354FF246F4EFFB5B7A8FFF1E6DFFFF4EAE3FFF6F1
          ECFF548B72FF4EAD8DFF42C49AFF3CC195FF3BC094FF3BBF92FF39BE90FF39BD
          90FF37BC8EFF35BB8DFF44C197FF287657FA0000001300000005000000030000
          000B287656FA4CC49BFF35BC8DFF37BC8EFF37BC8EFF24805CFF608A72FFE4D5
          CCFFB2B5A7FF2C7757FF3CBD96FF3BBB93FF2B7151FFC8C3B6FFF4E9E2FFF5EB
          E4FFE1E8E2FF2D7355FF5FCAABFF40C59AFF3EC197FF3CC195FF3BC094FF3ABF
          92FF38BD90FF38BC8EFF4EC59DFF297859FA0000001200000005000000020000
          000A267052EC55C39FFF39BD91FF38BE90FF3ABE92FF3CC096FF257E5BFF4A7C
          61FF2B7F5EFF41CBA3FF45D3ACFF46D3ADFF39B28DFF3C7457FFDACFC5FFF5EB
          E4FFF7EDE8FFADC7BAFF2E8061FF5ED8B7FF43CDA4FF40C99FFF3DC399FF3CBF
          94FF3ABF92FF3BBF92FF56C5A0FF277256ED0000001100000004000000020000
          0008205D46C559BD9DFF3DC094FF3CC296FF43CDA6FF45D2ACFF44CEA8FF319D
          7AFF46D2ACFF48D5B0FF49D5B0FF49D5B0FF49D5B1FF34A481FF4C7B60FFE0D2
          CAFFF5ECE5FFF9F3EFFF699882FF459D7FFF57D6B3FF44CFA6FF43CDA4FF41C8
          A0FF3DC196FF3FC196FF5BBF9FFF205F47C80000000E00000003000000010000
          00061643328F58B194FF49CDA6FF47D2ACFF48D3AEFF49D5AFFF49D5B0FF4AD7
          B2FF4BD7B2FF4CD8B4FF4DD7B4FF4DD8B4FF4CD8B4FF4DD7B4FF329B78FF4B7B
          61FFDFD2C9FFF6EBE5FFEFF0ECFF468066FF58B699FF55D6B1FF45CFA7FF45CE
          A5FF43CAA3FF48C9A2FF58B295FF174433940000000B00000003000000010000
          00040B201848409E80FF63DEC0FF4BD4B1FF4CD7B2FF4DD7B3FF4ED7B4FF4FD8
          B4FF4FD9B7FF50DAB7FF50DAB7FF51DAB8FF51DAB7FF50DAB7FF51D9B7FF38A6
          84FF47795EFFDDD0C7FFF6ECE7FFF0F2EFFF478368FF61C1A5FF56D5B3FF47CF
          A8FF46CDA6FF62D8B8FF409C7DFF0B20184E0000000700000002000000000000
          0002000000072D7D62DA6CD4BBFF55D9B7FF51D8B5FF51D9B6FF53DAB8FF53DB
          BAFF54DCB9FF55DBBAFF55DCBBFF56DCBBFF55DCBAFF56DDBBFF56DDBAFF55DC
          BAFF40B090FF487A60FFD9CCC4FFF3E9E3FFEDF1EDFF4B876DFF67C7ACFF57D6
          B3FF4ED2ADFF6ECFB6FF29785DDC0000000E0000000400000001000000000000
          0001000000041231275B48A98CFF72E5CAFF56DBB9FF56DBBAFF58DCBCFF58DC
          BCFF59DDBDFF59DEBDFF5ADFBEFF5ADFBFFF5BDFBFFF5ADFBEFF5ADFBEFF59DE
          BDFF59DDBCFF47BB9AFF367559FFBAB9ADFFECDFD8FFDDDFDAFF237150FF45BA
          96FF70DFC3FF46A487FF10302561000000080000000200000000000000000000
          000000000002000000062B765EC66CCEB6FF6AE3C6FF5CDDBDFF5DDEBFFF5EDE
          C0FF5EDFC0FF5FE1C2FF60E1C2FF5FE1C2FF60E1C2FF5FE0C2FF5EE1C1FF5EE0
          C1FF5DDFBFFF5CDFBEFF55CFAFFF2E8464FF72937DFF8FA392FF2D8463FF63D8
          B9FF6DCBB1FF287259C80000000C000000040000000100000000000000000000
          00000000000100000003081410283B9679EC7EDFCBFF6FE4C9FF63E0C3FF63E0
          C3FF64E1C4FF65E2C4FF64E3C5FF64E3C5FF64E3C5FF65E3C5FF64E2C5FF63E2
          C4FF63E2C3FF61E0C1FF5FDFBFFF5EDEBDFF48B797FF2A8362FF61D2B5FF80DB
          C6FF379274ED07140F2D00000006000000020000000000000000000000000000
          00000000000000000001000000041028204644A689F982DFCBFF7EEAD3FF69E2
          C6FF69E3C7FF6AE4C7FF6AE4C8FF6AE5C9FF6AE4C8FF6AE4C8FF6AE4C8FF69E4
          C7FF68E3C6FF66E2C4FF64E0C2FF62DFC1FF61DDBEFF7AE5CDFF82DCC7FF40A2
          83FA0E271F4A0000000700000002000000000000000000000000000000000000
          000000000000000000000000000100000004102921453F9E81EE77D5BEFF93F1
          DFFF7BE9D1FF6FE5CAFF6FE6CBFF70E6CBFF70E6CBFF6FE7CCFF6EE6CBFF6DE6
          CAFF6CE4C8FF6BE3C6FF69E2C5FF75E5CBFF92EEDAFF75D2BAFF3B9B7CEE0F28
          204A000000070000000200000001000000000000000000000000000000000000
          000000000000000000000000000000000001000000030814102335856DC85ABF
          A3FF8BE4D2FF9DF4E5FF8DEFDCFF82EBD5FF7EEBD4FF75E8CFFF74E8CEFF7DEA
          D2FF7FEAD3FF8CEDDAFF9DF2E2FF8BE4D0FF58BEA1FF318469CA071410280000
          0006000000020000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000002000000051738
          2E563D987CDA54BD9EFF75D4BCFF8EE6D3FF94EAD9FFA7F5E8FFA7F5E8FF95EA
          D9FF8DE6D3FF73D3BAFF52BB9CFF399679DA16392E5B00000007000000040000
          0002000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000010000
          0003000000040D201A32255A4A82388D73C53B957ACE49B896FC49BA98FC3A95
          7ACF378D74C6235A4A840C201A34000000060000000400000002000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000010000000200000003000000040000000500000005000000050000
          0005000000040000000400000003000000020000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000010000000100000001000000010000
          0001000000010000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000000000000000000000000000000000000000000020000
          0004000000060000000600000006000000060000000600000006000000060000
          0006000000060000000600000006000000070000000700000007000000070000
          0007000000070000000700000007000000070000000700000007000000070000
          0006000000040000000200000001000000000000000000000000000000060000
          000E000000150000001700000018000000180000001800000018000000190000
          001900000019000000190000001900000019000000190000001A0000001A0000
          001A0000001A0000001A0000001A0000001A0000001B0000001B0000001A0000
          001700000010000000070000000200000000000000000000000000000010173B
          7ACD2557A4FF2859A5FF2758A6FF2757A5FF2558A5FF2656A3FF2656A4FF2456
          A4FF2455A4FF2455A3FF2354A2FF2353A2FF2252A2FF2253A2FF2151A1FF2151
          A1FF2051A1FF2050A0FF1F4FA0FF1F4F9FFF1F4E9FFF1E4D9FFF1D4D9EFF1C4B
          9CFF173F8AEE040A143D0000000B000000020000000000000000000000151D50
          9FFF2559A5FF4F95CFFF64BBEDFF47A7E7FF46A5E6FF44A4E6FF42A3E5FF40A2
          E4FF3FA1E4FF3E9FE3FF3C9EE3FF3B9DE2FF3A9CE1FF389BE1FF389AE1FF359A
          E1FF3598E0FF3497E0FF3397DEFF3296E0FF3195DEFF3095DEFF3095DEFF2E90
          DAFF297DCAFF12326BBF00000011000000040000000000000000000000162154
          A3FF3166ADFF3C7BBCFF7ECBF2FF4FADE9FF4BAAE8FF49AAE8FF47A7E6FF45A6
          E6FF43A5E5FF43A3E4FF40A2E5FF40A1E4FF3FA0E4FF3D9FE3FF3B9DE3FF3A9D
          E2FF399CE2FF379BE1FF379AE2FF3599E1FF3398E1FF3298DFFF3197DFFF3096
          DFFF3295DEFF1A4A98F80103051D00000005000000010000000000000014245C
          A9FF447ABAFF2862ACFF8CD1F2FF64BCEEFF52B0EBFF51AFEAFF50AEE9FF4DAD
          E8FF4CAAE7FF4AA9E7FF48A8E7FF47A7E6FF45A5E6FF43A4E6FF42A3E5FF41A1
          E4FF3FA1E4FF3D9FE4FF3C9FE3FF3B9EE3FF3A9DE2FF399CE1FF379AE1FF3699
          E0FF389CE1FF2363B1FF08162E5D000000080000000100000000000000132661
          AFFF548AC4FF2866AEFF7CB9E1FF82CFF3FF59B6EDFF57B5EDFF57B4ECFF54B3
          ECFF53B1EAFF51B0EAFF51AEE8FF4FADE8FF4DABE8FF4EACE8FF4CAAE8FF4BAA
          E7FF47A6E5FF44A6E6FF43A4E5FF41A3E5FF40A2E4FF3FA1E3FF3E9FE2FF3C9F
          E2FF3D9FE3FF2F7AC4FF0F2A56970000000A0000000200000000000000122A67
          B1FF649BCEFF3272B8FF619DCFFF9FDEF8FF5FBAF0FF5EBAEFFF5EB9EEFF5DBA
          EDFF62BBEEFF68BFEFFF6ABEEFFF6CC0EEFF69BDEDFF66BBEDFF63B9EDFF60B9
          ECFF5EB6ECFF5BB4EBFF56B1E9FF51ADE9FF4CA9E8FF47A6E6FF45A5E5FF43A4
          E5FF44A4E4FF4198D9FF164385D80000000D0000000300000001000000112D6C
          B5FF74AAD7FF4287C6FF4083C0FFB5E8FBFF71C6F3FF6FC4F2FF76C8F3FF7ACA
          F4FF7ACAF2FF78C8F3FF77C7F2FF73C6F1FF72C4F1FF70C2F0FF6DC0EFFF6ABF
          EEFF68BDEEFF65BAEEFF62B9ECFF5FB7EBFF5DB6EBFF5AB4EAFF54B0E9FF4EAC
          E7FF49A9E6FF50ADE8FF225EAAFF030A132C0000000400000001000000103071
          B7FF84B9DFFF5399D3FF3A7EBEFFADDCF1FFA5E1F9FF86D1F6FF85D0F6FF83CF
          F6FF82CEF5FF7FCDF3FF7DCBF3FF7CCBF3FF7ACAF2FF79C8F2FF76C6F1FF73C6
          F2FF70C4F0FF6DC2F0FF6CC1EFFF68BDEEFF66BCEDFF63B9ECFF60B8ECFF5EB6
          EBFF59B4EAFF5AB3EAFF357CC1FF0C2344740000000700000002000000103176
          BAFF93C6E7FF61ABDDFF4590CBFF8DC0E0FFC2EDFCFF8BD5F7FF8AD5F7FF89D3
          F6FF87D2F6FF86D1F6FF85D1F5FF83D0F5FF81CFF4FF7FCDF4FF7ECDF3FF7DCB
          F3FF7AC9F2FF77C8F2FF74C6F0FF72C5F0FF70C3F0FF6CC1F0FF6ABFEEFF66BD
          EEFF64BBEDFF66BCEDFF53A0D8FF153F75BA00000009000000020000000F347B
          BDFFA1D3EEFF70BBE6FF5AA8DBFF66A5D2FFD7F6FDFF95DCF9FF90D8F8FF8FD8
          F8FF8DD7F8FF8CD5F8FF8BD5F7FF88D3F6FF88D3F6FF86D1F6FF85D1F4FF82D0
          F4FF81CFF4FF80CEF4FF7DCCF4FF7BCBF3FF78C9F1FF75C7F1FF72C6F1FF70C5
          F1FF6DC1EFFF6CC1EFFF73C1EDFF215DA4F701030614000000030000000E357E
          BFFFAEDFF4FF7ECAEFFF73C2ECFF4C97CDFFCAEBF7FFDEF8FEFFDEF8FEFFDDF8
          FEFFDCF8FEFFDCF8FEFFDBF7FEFFD9F6FEFFD7F5FCFFBDECFBFF8FD7F7FF89D3
          F7FF87D2F6FF85D3F6FF84D1F5FF82D0F5FF80CEF5FF7ECDF4FF7CCBF3FF79CA
          F2FF76C8F2FF73C6F1FF7CCAF1FF3A7EBFFF091C3255000000050000000D3782
          C1FFBBE7F8FF8DD8F5FF84D2F4FF6ABDE7FF51A1D3FF4C99CCFF4B98CCFF4996
          CBFF4995CAFF4693CBFF4591CAFF4490C8FF75B0D8FFD0F0F9FFD0F4FCFF9BDE
          F9FF8DD7F8FF8CD6F7FF8AD4F7FF88D5F7FF87D3F7FF85D2F5FF83D1F5FF80CF
          F5FF7ECDF3FF7BCCF3FF81CEF3FF5CA4D5FF133A659D000000060000000C3985
          C3FFC5F0FCFF97E3FBFF93E1FAFF8DDCFAFF87D9F9FF83D7F9FF7FD5F8FF7CD2
          F8FF79D1F8FF77CEF6FF73CCF6FF6EC9F5FF5BB4E7FF559DD1FFACD6ECFFE0F9
          FEFFDFF9FEFFDEF9FEFFDEF8FEFFDDF7FEFFDDF7FEFFDBF7FEFFDAF6FEFFDAF6
          FEFFD8F5FDFFD6F5FDFFD5F4FDFFBDE2F2FF1D5897E0000000070000000B3B89
          C5FFCDF6FFFFA2ECFEFF9FEAFEFF9CE8FEFF99E7FEFF96E6FDFF93E4FDFF90E2
          FDFF8EDFFCFF8CDDFCFF89DCFCFF84D9FBFF7DD5FAFF6FCAF5FF52A7DAFF4694
          CAFF4592CAFF4492CAFF4290C9FF418FC8FF3F8DC7FF3E8BC7FF3D8AC6FF3B89
          C5FF226BB1FF226AB1FF216AB0FF2064A6F415416DA4000000050000000B3C8C
          C7FFD1F7FFFFA6EFFFFFA4EEFFFFA2ECFFFFA0EBFEFF9EE9FEFF9CE8FEFF99E7
          FEFF97E5FDFF95E3FDFF92E1FDFF8FE0FCFF8ADDFCFF83D9FBFF7DD4FAFF77D1
          F9FF73CFF9FF70CCF9FF6DC9F8FF6AC7F6FF68C5F6FF64C2F5FF62C0F4FF7CCC
          F6FF2C6CB1FF000000170000000D0000000800000006000000030000000A3D8F
          C9FFD4F9FFFFAAF1FFFFA8F0FFFFA6EFFFFFA4EEFFFFA2EDFFFFA0EBFFFF9EEA
          FEFF9CE8FEFF9AE7FEFF97E5FDFF95E3FDFF92E2FDFF8FDFFCFF8CDDFCFF89DA
          FBFF84D9FBFF81D7FBFF7ED5FAFF7CD3FAFF79D1F9FF76CEF9FF73CDF8FF8DD6
          F9FF357DBEFF0000000F00000005000000020000000100000001000000093E91
          CBFFD6FAFFFFADF3FFFFACF2FFFFAAF1FFFFA8F0FFFFA6EFFFFFA4EDFFFFA2ED
          FFFFA0EBFEFF9EEAFEFF9CE9FEFF9AE6FEFF97E6FEFF95E4FDFF93E2FDFF90E0
          FCFF8EDFFCFF8BDDFCFF88DBFBFF85D9FBFF83D7FAFF81D5FAFF7DD3F9FF95DC
          FBFF3985C4FF0000000C00000003000000000000000000000000000000084095
          CDFFDAFAFFFFB0F5FFFFAFF4FFFFADF3FFFFACF2FFFFAAF1FFFFA8F0FFFFA6EF
          FFFFA4EEFFFFABEFFEFFC0F6FEFFCEF8FEFFCDF8FEFFCDF8FEFFCDF8FEFFCCF8
          FEFFCCF8FEFFCBF8FEFFCBF8FEFFCBF8FEFFCAF8FEFFCAF8FEFFC9F8FEFFA7DE
          F1FF367CB3E90000000A00000003000000000000000000000000000000084297
          CEFFDBFBFFFFB3F6FFFFB2F6FFFFB0F5FFFFAFF4FFFFADF3FFFFACF2FFFFAAF1
          FFFFAEF2FEFF92D2ECFF3F93CBFF3F92CBFF3F92CBFF3F91CBFF3F91CAFF3F91
          CAFF3E90CAFF3D90CAFF3E8FC9FF3D8FC9FF3D8EC9FF3D8DC8FF3D8DC8FF306F
          9ECD0F23314A0000000700000002000000000000000000000000000000074299
          CFFFDDFCFFFFB5F7FFFFB4F7FFFFB3F6FFFFB1F6FFFFB0F5FFFFAFF4FFFFB3F4
          FFFF8ED2ECFF2A6082A80000000B000000090000000900000009000000090000
          0009000000090000000900000009000000090000000A0000000A0000000A0000
          0009000000060000000300000001000000000000000000000000000000053D8D
          BDE8B2E3F3FFD5F9FEFFD4F9FEFFD4F9FEFFD4F9FEFFD3F9FEFFD3F9FEFFC5F0
          FAFF4893C1EA050B0F1A00000004000000020000000200000002000000020000
          0002000000020000000200000002000000020000000200000002000000020000
          0002000000010000000100000000000000000000000000000000000000031127
          3445357CA6CB449DD2FF449DD2FF449DD2FF449DD1FF449DD1FF449CD1FF449C
          D1FF163345590000000400000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0003000000050000000500000005000000050000000500000006000000060000
          0005000000030000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000000000000000000000000000000000
          0001000000010000000300000005000000070000000700000007000000070000
          0007000000070000000700000007000000070000000700000007000000070000
          0007000000070000000700000007000000070000000700000007000000070000
          0007000000070000000600000004000000010000000100000000000000000000
          0001000000060000000E000000160000001A0000001B0000001B0000001B0000
          001B0000001B0000001B0000001B0000001B0000001B0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001D0000001D0000
          001D0000001C0000001800000010000000070000000100000000000000010000
          00030000000E2D1F198E583E33FD593D34FF583C32FF583C32FF5A3F37FFC58D
          5DFFC18656FFBD8151FFBB7D4DFFB97A4BFFB77748FFB57444FFB37141FFB06D
          3DFFAF6B3AFFAC6837FFAB6535FFA96333FFA76030FF50342DFF4F342CFF4F34
          2CFF4F342DFF4E342DFD2519158B000000100000000400000001000000010000
          0005000000145C3F36FD806357FF745449FF73534AFF735349FF61473EFFE8C5
          94FFE5BE89FFE3B981FFE3B87FFFE3B67DFFE2B57AFFE1B278FFE0B175FFDFAF
          73FFDEAD71FFDEAC6FFFDDAB6DFFDDAA6CFFDDA86AFF523731FF6B4C42FF6B4C
          42FF6B4C42FF6B4D43FF4E342BFB000000170000000600000001000000010000
          0006000000175F4337FF83655AFF77574CFF76574BFF76574BFF654B41FFEAC8
          98FFE6C08CFFE5BC85FFE3B981FFE2B77EFFE2B57CFFE1B479FFE0B277FFE0B0
          75FFDFAF72FFDEAC70FFDEAC6FFFDDAA6CFFDCA96BFF553932FF6B4C43FF6C4D
          42FF6B4C42FF6D4F45FF50362DFF0000001B0000000700000001000000010000
          000600000017624539FF87695DFF79594EFF795A4EFF795A4EFF684E44FFE1C2
          96FFDCB988FFDBB684FFD7B17BFFD7AF79FFD6AD76FFD6AC75FFD6AA72FFD5A8
          70FFD4A66EFFD4A56BFFD3A369FFD3A267FFD3A166FF583D35FF6D4F44FF6D4E
          45FF6C4D44FF6E5047FF51362FFF0000001B0000000700000001000000010000
          00060000001766483CFF8C6E63FF7D5D51FF7D5D50FF7C5C50FF6C5247FFFAF5
          F2FFF9F3F0FFF9F2EFFFF7F1ECFFF7F0EBFFF6F0EAFFF6EEE9FFF5EEE9FFF5EE
          E8FFF4EDE7FFF4ECE7FFF3ECE6FFF4EBE5FFF3EBE4FF5A3F38FF6F5046FF6E50
          46FF6E4F45FF715348FF523830FF0000001A0000000700000001000000010000
          000500000016684B3EFF917366FF816153FF816153FF7F6154FF6E554AFFFAF7
          F3FFF9F4F0FFF9F3EFFFF8F3EDFFF7F0ECFFF6F0EBFFF6EFEBFFF6EFEAFFF5EE
          E9FFF5EDE8FFF5EDE7FFF4ECE7FFF4EBE6FFF4EBE5FF5E4339FF705248FF7052
          48FF6F5147FF72554BFF523A31FF000000190000000600000001000000010000
          0005000000156C4F42FF95786AFF846456FF836557FF836456FF72584DFFFAF7
          F5FFFAF5F2FFD5B8A9FFD2B4A6FFCCA899FFC9A492FFC6A090FFC39C8BFFC097
          87FFBE9283FFBA8F7FFFB88C7BFFF4EDE6FFF4ECE6FF60463DFF72544BFF7053
          4BFF705248FF75574DFF543A32FF000000190000000600000001000000010000
          0005000000156F5346FF997D6FFF87675AFF876859FF876859FF755C50FFFBF8
          F6FFFAF6F2FFFAF5F1FFF9F5F1FFF9F3EFFFF7F2EDFFF7F1EDFFF6F0EBFFF6EF
          EAFFF5EFEAFFF5EEE9FFF5EDE8FFF5ECE8FFF4EDE6FF634940FF73564CFF7355
          4CFF72544BFF775A4FFF563D34FF000000180000000600000001000000000000
          000500000014735649FF9E8275FF8B6B5CFF8A6D5EFF8B6C5DFF796053FFFCF9
          F7FFFAF7F3FFD9BEB1FFD8BCAEFFD5BAABFFCDAC9DFFCBA797FFC8A392FFC6A0
          8FFFC39B8BFFC09787FFBD9283FFF5EEE8FFF4EDE8FF664C43FF75594EFF7458
          4DFF74574CFF795C53FF573D35FF000000180000000600000001000000000000
          00050000001376594BFFA28878FF8E7061FF8F7061FF8E7062FF7C6155FFFCF9
          F8FFFBF7F4FFFAF7F4FFF9F6F3FFF9F5F2FFF9F4F1FFF8F2EEFFF7F1EDFFF7F0
          ECFFF6F0ECFFF6F0EBFFF5EFEAFFF6EEE9FFF4EEE8FF694F46FF775B50FF7759
          4FFF75594EFF7B6056FF594037FF000000170000000600000001000000000000
          0005000000137B5D4EFFA68D7FFF937464FF937464FF917465FF7F6659FFFCFB
          F9FFFCF9F8FFFCF9F8FFFCF9F7FFFBF9F6FFFCF8F5FFF9F6F3FFF9F5F2FFF9F5
          F1FFF9F4F1FFF8F4F0FFF7F2EFFFF7F2EDFFF7F1EDFF6C5248FF785E52FF775D
          51FF775A50FF7F6359FF5C4239FF000000160000000600000001000000000000
          0005000000127E6152FFAB9182FF967767FF957A67FF957968FF876D5EFF8268
          5BFF82695AFF82685AFF81685AFF80675AFF7F6759FF7A6155FF755D51FF745B
          50FF735B50FF725A4FFF72594FFF71594EFF70574CFF72594EFF7B6055FF7A60
          54FF795E52FF81675BFF5D433AFF000000160000000500000001000000000000
          000400000012836654FFB09687FF997B6BFF9A7D6BFF9A7E6BFF9A7D6CFF997D
          6BFF997E6CFF997E6BFF997E6CFF987E6DFF997E6CFF967B6BFF82675BFF7F66
          5AFF806659FF7F6659FF7F655AFF7F6559FF7F6458FF7D6257FF7C6255FF7C61
          55FF7A6054FF826A5EFF5E463CFF000000150000000500000001000000000000
          000400000011866958FFB39B8CFF9D816FFF9D7F6EFF9D816EFF9D816FFF9D81
          70FF9D826FFF9D8270FF9D8270FF9C8170FF9B8170FF9C826FFF8D7364FF8369
          5CFF82695DFF82685CFF81675CFF81675BFF7F6659FF7F6559FF7F6458FF7D63
          58FF7D6257FF866D61FF61483EFF000000140000000500000001000000000000
          0004000000108A6C5BFFB79F91FFA08471FFA08471FFA08571FFA08573FFA085
          73FFA08574FFA08673FFA18673FFA08574FFA08673FF9F8574FF9C8271FF836A
          60FF836B60FF836A5FFF836A5DFF83695DFF82695DFF81685BFF81675AFF7F65
          59FF7F6359FF886E64FF624940FF000000140000000500000000000000000000
          0004000000108D705EFFBBA494FFA48774FFA48874FFAA927FFFAC9582FFAD94
          83FFAD9482FFAC9382FFAC9482FFAC9483FFAB9381FFAA9282FFAA9080FF9880
          71FF8E766BFF8D756AFF8D7568FF8C7468FF897167FF887065FF866E61FF8167
          5CFF806659FF8B7267FF634A40FF000000130000000500000000000000000000
          00040000000F907462FFBFA898FFA68A77FFA78B79FFB09785FF6E5449FF5439
          31FF543831FF60463EFF644A42FF634A40FF61483FFF5F473CFF5E453CFF5C44
          3BFF5B4339FF5A4139FF594137FF583F36FF563E36FF654D43FF887164FF836A
          5DFF82685CFF8D766AFF654C42FF000000120000000500000000000000000000
          00040000000E957764FFC2AD9DFFA98D7BFFAA8F7BFFB29986FF563A33FF5944
          3DFF644E47FF75594EFFE8DAD0FFDCC5B5FFDBC4B5FFDBC4B3FFDAC3B3FFDAC3
          B2FFD9C2B1FFE3D2C6FFE2D1C5FFE2D0C3FFE1CFC2FF573F37FF8A7266FF856C
          5EFF836A5EFF8F796EFF664F44FF000000120000000400000000000000000000
          00030000000E977A67FFC5B0A0FFAC907CFFAD927DFFB39C88FF573B34FF5A45
          3EFF654F48FF775B50FFEBDFD5FFDEC9BBFFDEC9B9FFDDC8B8FFDDC7B7FFDCC6
          B6FF584139FF705448FF705347FF6F5246FFE4D3C7FF594138FF8B7368FF856D
          62FF856B5FFF937B70FF685146FF000000110000000400000000000000000000
          00030000000D9B7D6AFFC8B4A3FFAE947FFFAF9480FFB79E8BFF583C34FF5B46
          3FFF655049FF795D53FFEFE4DBFFE1CEC0FFE1CDBFFFE1CDBDFFE0CBBBFFDFCA
          BBFF554038FF61473FFF654B42FF715548FFE7D8CDFF5B433AFF8C7468FF876F
          62FF876D60FF957E72FF695147FF000000100000000400000000000000000000
          00030000000C9C7F6BFFCAB6A7FFB29681FFB29782FFB89F8CFF593C35FF5B46
          40FF66514AFF7B5F55FFF2E8E0FFE5D3C5FFE4D3C4FFE3D1C2FFE3D0C1FFE2CF
          C0FF523E36FF5B413AFF5E433BFF73564BFFEBDDD4FF5E463CFF8E7469FF8871
          65FF866E64FF968075FF6B5348FF000000100000000400000000000000000000
          00030000000C9E826CFFCDBAAAFFB39983FFB49984FFBBA18DFF593D35FF5C47
          40FF68524BFF7D6157FFF4ECE5FFE8D8CAFFE8D7C9FFE7D6C8FFE6D5C7FFE6D4
          C6FF4E3C34FF553B34FF583E36FF75584DFFEEE2D9FF60483EFF8F766AFF8A72
          66FF896F65FF998377FF6C544AFF0000000F0000000400000000000000000000
          00030000000BA2836EFFCFBCACFFB69B86FFB59B86FFBBA28FFF5A3E36FF5C47
          41FF68524BFF7E6459FFF6F0EAFFEBDCCFFFEADCCEFFEADBCDFFE9DACCFFE8D9
          CBFF4B3A33FF523731FF533832FF765A4FFFF1E6DEFF624A41FF8E786AFF8A73
          67FF897165FF9B8579FF6D564BFF0000000E0000000400000000000000000000
          00030000000AA3876FFFD1BEB0FFB89D89FFB89E87FFBDA390FF5B3E37FF5C48
          42FF69524CFF80665CFFF8F3EEFFEEE1D4FFEDE0D3FFEDDFD2FFECDED1FFEBDD
          D0FF473832FF493832FF493932FF493832FFF3EBE4FF654D43FF90766CFF8D73
          68FF8A7166FF9E877DFF6D564BFF0000000D0000000300000000000000000000
          000200000008A1866EF9D2BFB0FFD3C0B2FFD3C2B1FFD6C5B6FF5B3E37FF5D48
          42FF69534CFF82675DFFF9F5F1FFF9F5F0FFF9F4EFFFF9F4EFFFF8F3EEFFF8F2
          EDFFF7F2ECFFF7F1EBFFF7F1EBFFF6F0EAFFF6EEE9FF664F46FFB4A399FFB3A0
          97FFB19D95FFB09C93FF6E564CFC0000000B0000000300000000000000000000
          00010000000552443984A48873FCA88C75FFA98D76FFA88F78FF836859FF765B
          4DFF765A4DFF80655BFFA9948BFFA89289FFA59087FFA48E85FFA28C82FFA08A
          80FF9E877EFF9C847BFF998278FF987F76FF947C73FF695148FF745F52FF745E
          51FF725B50FF6F584DFC453730A5000000070000000200000000000000000000
          0000000000020000000500000007000000090000000900000009000000090000
          000A0000000A0000000A0000000A0000000A0000000A0000000A0000000A0000
          000B0000000B0000000B0000000B0000000B0000000B0000000B0000000B0000
          000B0000000B0000000A00000007000000030000000100000000000000000000
          0000000000000000000100000002000000020000000200000002000000020000
          0002000000020000000200000002000000020000000200000002000000020000
          0002000000030000000300000003000000030000000300000003000000030000
          0003000000030000000200000002000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000010000000200000004000000060000000600000006000000060000
          0007000000070000000700000007000000070000000700000007000000070000
          0007000000070000000700000007000000070000000700000004000000020000
          0001000000000000000000000000000000000000000000000000000000000000
          000000000002000000070000000E000000150000001800000019000000190000
          0019000000190000001A0000001A0000001A0000001A0000001B0000001B0000
          001B0000001B0000001C0000001C0000001B0000001900000011000000080000
          0002000000000000000000000000000000000000000000000000000000000000
          0001000000030000000E78564BC0A77868FFA77867FFA77666FFA67766FFA675
          65FFA67666FFA57565FFA47464FFA47464FFA47363FFA37363FFA37262FFA371
          61FFA27161FFA17061FFA17160FFA1705FFF9F705FFF725044C2000000110000
          0004000000010000000000000000000000000000000000000000000000000000
          00010000000500000013AA7A6CFFFCF9F7FFFCF8F6FFFBF8F6FFFBF8F5FFFBF7
          F5FFFBF7F4FFFAF7F3FFFAF6F3FFFAF6F3FFFAF6F2FFFAF5F2FFF9F4F1FFF9F4
          F0FFF9F3F0FFF8F3EFFFF8F3EFFFF8F2EEFFF8F2EEFFA17060FF000000180000
          0006000000010000000000000000000000000000000000000000000000000000
          00010000000500000014AB7D6DFFFCF9F8FFF7EDE9FFF6EDE8FFF6EDE6FFF6ED
          E6FFF6ECE6FFF6ECE5FFF6ECE5FFF6ECE4FFF4EBE4FFF4EBE4FFF4EBE4FFF4EA
          E3FFF4EAE3FFF4EAE3FFF4EAE3FFF3E9E3FFF8F2EEFFA37262FF0000001A0000
          0008000000020000000100000001000000000000000000000000000000000000
          00000000000500000014AB7F6FFFFDFAF8FFF7EFE9FFF7EFE9FFF7EFE8FFF6ED
          E8FFF6EDE8FFF6EDE6FFF6EDE6FFF4ECE6FFF6ECE6FFF4ECE5FFF4EBE5FFF4EB
          E4FFF4EBE4FFF4EBE4FFF4EBE4FFF4EAE3FFF9F3EFFFA37363FF0000001F0000
          000D000000080000000700000004000000020000000100000000000000000000
          00000000000500000013AD8071FFFDFBF9FFF7F0E9FFF6EFE9FFF7EFE9FFF7EF
          E8FFF7EFE9FFF6EDE8FFF7EDE8FFF7EDE6FFF6EDE6FFF6EDE6FFF6ECE5FFF4EC
          E5FFF6ECE5FFF4EBE5FFF4EBE4FFF4EBE4FFF9F4F0FFA57565FF010101310101
          01210101011C0101011901010111000000080000000200000000000000000000
          00000000000500000012AF8273FFFDFBFAFFF8F0EBFFF7F0EBFFF7F0EAFFF7F0
          EAFFF7F0E9FFF7EFE9FFF6EFE9FFF7EDE8FFF6EDE8FFF6EDE8FFF6EDE8FFF6EC
          E6FFF6EDE6FFF4ECE5FFF6EBE5FFF4EBE4FFF9F5F1FFA67667FFB89D94FFAD84
          74FFB08576FFB08676FF7F6055C2010101110000000400000001000000000000
          00000000000400000011B08375FFFDFCFAFFF8F1EBFFF7F1EBFFF7F1EBFFF7F0
          EBFFF8F0EAFFF7F0EAFFF7EFEAFFF7EFEAFFF7EDE9FFF6EFE8FFF6EFE8FFF6ED
          E8FFF7EDE8FFF6EDE6FFF4EDE6FFF6EDE6FFFAF5F2FFA87969FFE4E0DDFFF3EF
          ECFFF8F4F0FFFAF5F1FFB18677FF010101180000000600000001000000000000
          00000000000400000010B18677FFFDFCFAFFF8F2ECFFF8F1ECFFF8F1ECFFF7F1
          ECFFF7F0EBFFF8F1EAFFF8F0EAFFF7F0EAFFF7F0E9FFF7F0E9FFF6EFE9FFF6ED
          E9FFF7EDE9FFF7EFE9FFF6EDE8FFF7EDE8FFFBF6F4FFAA7A6AFFE2DAD5FFF1E9
          E3FFF4ECE7FFF9F5F2FFB38879FF010101190000000700000001000000000000
          0000000000040000000FB48878FFFDFCFBFFF8F2EDFFF8F2EDFFF8F2EDFFF8F1
          EDFFF8F1ECFFF8F1ECFFF8F1EBFFF8F0EBFFF8F0EAFFF7EFEAFFF7F0EAFFF7EF
          EAFFF7F0E9FFF7EFE9FFF6EDE9FFF7EDE8FFFBF7F4FFAA7D6DFFE2DBD6FFF1EA
          E4FFF5EDE7FFFAF5F3FFB3897AFF010101190000000600000001000000000000
          0000000000040000000FB5897AFFFEFDFCFFF9F3EFFFF8F2EFFFF8F2EDFFF8F2
          EDFFF8F2EDFFF8F1ECFFF8F1EDFFF7F1ECFFF8F1ECFFF7F0EBFFF7F0EAFFF7F0
          EBFFF7EFEAFFF7EFEAFFF7EFEAFFF7EFE9FFFBF8F5FFAC7F6FFFE3DCD8FFF1EA
          E4FFF5EEE8FFFAF5F4FFB58B7BFF010101180000000600000001000000000000
          0000000000030000000EB68D7EFFFEFDFCFFF9F3F1FFF9F3F0FFF8F3EFFFF8F2
          EFFFF9F2EFFFF8F3EFFFF8F2EFFFF8F1EDFFF8F1ECFFF8F1ECFFF8F1ECFFF8F1
          EBFFF8F1EBFFF7F0EBFFF7F0EBFFF7EFEAFFFCF8F6FFAD8071FFE4DED9FFF2EA
          E5FFF6EFE9FFFAF6F4FFB68C7DFF010101170000000600000001000000000000
          0000000000030000000DB88F80FFFEFDFDFFFAF4F1FFF9F4F1FFF9F3F1FFF9F3
          F0FFF9F3EFFFF9F3EFFFF9F3EFFFF8F2EFFFF9F2EFFFF8F2EFFFF8F2EDFFF8F1
          ECFFF8F1ECFFF7F1ECFFF7F0EBFFF7F0EBFFFAF6F3FFAE8373FFE6E0DAFFF2ED
          E7FFF7F1EBFFFBF7F5FFB88F7FFF010101160000000600000001000000000000
          0000000000030000000CB99081FFFEFEFDFFF9F6F2FFF9F4F1FFF9F4F1FFFAF4
          F0FFF9F4F0FFF9F4F0FFF9F3F0FFF9F3EFFFF9F3EFFFF9F2EFFFF9F3EDFFF9F2
          EDFFF8F1EDFFF8F1ECFFF8F1ECFFF6EFE9FFF8F4F2FFB08374FFE7E2DDFFF3ED
          E8FFF8F1ECFFFBF8F5FFB89080FF010101150000000500000001000000000000
          0000000000030000000BBC9283FFFEFEFDFFFAF6F2FFFAF7F2FFFAF6F2FFFAF6
          F2FFFAF4F1FFF9F4F1FFF9F4F0FFF9F3F1FFF9F4F0FFF9F3F0FFF9F3F0FFF9F3
          EFFFF9F3EFFFF8F2EFFFF6F0EAFFF5EDE7FFF6F1EEFFB38576FFE8E3DEFFF3ED
          E9FFF8F1ECFFFBF8F6FFB89183FF010101150000000500000001000000000000
          0000000000020000000ABD9384FFFEFEFEFFFAF7F3FFFAF7F3FFFAF7F3FFFAF6
          F3FFF9F6F2FFFAF6F2FFFAF6F2FFFAF6F1FFF9F4F1FFF8F3F0FFF9F4F1FFF8F3
          EFFFF8F2EEFFF6F0EBFFF4EDE8FFF2E9E5FFF3ECE9FFB38978FFE9E4E0FFF4EE
          EAFFF8F2EDFFFBF9F7FFBA9385FF010101140000000500000001000000000000
          00000000000200000009BE9686FFFFFEFEFFFAF7F6FFFAF7F4FFFAF7F3FFFAF7
          F3FFFAF7F3FFFAF6F3FFFAF6F2FFF9F6F3FFFAF6F2FFF8F2EFFFF6EFEBFFF5ED
          E9FFF3EAE6FFF0E5E2FFEEE2DDFFEBDED9FFECE1DDFFB5897AFFEAE6E2FFF4EF
          EBFFF8F2EEFFFCF9F7FFBB9487FF010101130000000500000000000000000000
          00000000000200000008BF9787FFFFFEFEFFFBF8F6FFFBF8F6FFFAF8F4FFFBF7
          F4FFFAF7F4FFFBF7F4FFFAF7F4FFF9F5F2FFF9F5F1FFF5EEE9FFEEE2DCFFE6D8
          D0FFE1D2CAFFE0CEC7FFDECAC2FFDBC7BEFFDCC8C2FFB78C7DFFEBE8E4FFF5F0
          ECFFF8F3EFFFFAF7F5FFBC9789FF010101120000000400000000000000000000
          00000000000200000008C09989FFFFFEFEFFFBF9F6FFFDF9F6FFFBF8F6FFFAF8
          F6FFFBF8F6FFFBF8F4FFFAF7F4FFFBF7F4FFF8F2EFFFEFE6DFFFB38B7CFFA577
          66FFA47564FFA47464FFA47363FFA37363FFCEB3AAFFB88F7EFFEFEAE7FFF6F1
          EDFFF7F2EDFFF9F5F4FFBE978AFF010101110000000400000000000000000000
          00000000000200000007C19A8BFFFFFFFEFFFDF9F7FFFBFAF7FFFBF9F7FFFBF9
          F7FFFBF8F6FFFBF8F6FFFBF8F6FFFAF8F4FFF7F2EFFFECDFDAFFAB7E6DFFFFFF
          FFFFFFFEFEFFFFFDFCFFFEFCFAFFFCF9F7FFCAAFA6FFC2A9A0FFF3EFECFFF5F1
          ECFFF6F0EBFFF7F3F0FFC0998CFF010101100000000400000000000000000000
          00000000000100000006C39B8CFFFFFFFFFFFDF9F8FFFDFAF8FFFDF9F7FFFDF9
          F8FFFBF9F7FFFBF9F7FFFBF9F6FFFBF8F6FFF6F1EDFFEBDFDBFFB08574FFFFFE
          FEFFFEFBFAFFFDF9F7FFFCF6F3FFCEB2A8FFC5ACA3FFF1EDE9FFF4F0EBFFF4EF
          EAFFF4EBE7FFF4EEEBFFC09D8EFF0101010F0000000400000000000000000000
          00000000000100000005C39D8EFFFFFFFFFFFDFBF9FFFDFAF9FFFDFAF8FFFDFA
          F9FFFDF9F8FFFBF9F7FFFBF9F8FFF9F6F4FFF6F0ECFFECE1DBFFB68C7DFFFFFE
          FEFFFDF9F6FFFBF6F3FFD1B5ACFFC8AFA5FFEEE8E5FFF2EAE4FFF1E7E3FFF0E5
          E1FFEDE1DDFFEDE2DFFFC29D90FF0101010E0000000400000000000000000000
          00000000000100000005C49E8FFFFFFFFFFFFDFBF9FFFDFBF9FFFEFAF9FFFDFA
          F9FFFDFAF8FFFDFAF8FFF9F7F6FFF9F4F2FFF5EDEBFFEBE1DDFFBC9584FFFFFE
          FEFFFBF6F3FFD4BAAFFFCBB2A7FFE9DEDAFFE5D8D2FFE2D4CCFFE2D1CBFFE1CE
          C7FFDECBC2FFDECCC5FFC49F91FF0101010D0000000300000000000000000000
          00000000000100000004C59F90FFFFFFFFFFFDFDFBFFFEFBFAFFFEFBFAFFFDFB
          F9FFFDFBF9FFFBF7F6FFF9F5F3FFF7F1EEFFF3EBE7FFEDE1DCFFC19B8BFFFFFE
          FEFFD6BCB2FFD1B8B0FFEBE3DDFFBA9689FFAF8476FFAF8474FFAF8374FFAF82
          73FFAE8273FFD1B8AFFFC49F91FF0000000A0000000300000000000000000000
          00000000000100000003C6A191FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFBF9F9FFF9F6F4FFF6F1F0FFF2ECE9FFEEE3E0FFE3D2CBFFDBC5
          BDFFD4BFB5FFF3EFECFFECDFDCFFB48B7CFFFFFFFFFFFFFEFEFFFFFDFCFFFEFC
          FAFFFCF9F7FFD3BAB2FF55403886000000060000000200000000000000000000
          0000000000000000000293776CBEC6A191FFC59F91FFC69F92FFC59F91FFC59F
          90FFC59F91FFC49F90FFC49E90FFC49D8FFFC49E8EFFC39D8EFFC39D8EFFD8C3
          BAFFF8F5F3FFF5F1EDFFECE0DCFFB99283FFFFFEFEFFFEFBFAFFFDF9F7FFFCF6
          F4FFD6BDB4FF57423B8600000008000000030000000100000000000000000000
          000000000000000000010000000100000002000000030000000400000008DAC6
          BDFFFCFCFCFFFAF9F7FFF9F7F6FFF9F7F5FFF9F7F6FFF9F6F5FFF8F6F4FFF9F7
          F6FFF9F6F4FFF6F2EEFFEEE3DEFFBF998BFFFFFEFEFFFDF9F6FFFBF6F4FFD8BF
          B8FF5A463F860000000800000003000000010000000000000000000000000000
          000000000000000000000000000000000000000000010000000200000006CDAE
          A1FFFEFEFEFFFCFBF9FFFCFBF9FFFDFAF9FFFCFAF9FFFCFAF8FFFCFAF8FFF9F7
          F6FFF9F5F4FFF5EFEDFFEDE3DFFFC4A191FFFFFEFEFFFBF6F4FFDBC4BAFF5D49
          4285000000070000000300000001000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000100000004CFB0
          A3FFFFFFFFFFFDFDFCFFFEFCFBFFFEFCFBFFFDFCFAFFFDFCFAFFFBF8F7FFF9F6
          F5FFF7F3F0FFF4EDE9FFEEE3DFFFC9A698FFFFFEFEFFDDC6BDFF5F4C45840000
          0006000000020000000100000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000100000003D0B1
          A4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF9F9FFF9F6
          F5FFF6F2F1FFF3EDEAFFEFE4E1FFE5D4CFFFE1CEC7FF604F4883000000050000
          0002000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000029A83
          7ABED0B1A4FFCFB0A4FFD0B0A4FFCFB0A4FFCFB0A3FFCFB0A4FFCEB0A3FFCEAF
          A3FFCEAEA2FFCEAFA1FFCDAEA1FFCDAEA1FF63524A8200000004000000020000
          0001000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000001000000010000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000001000000010000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000100000002000000030000000400000005000000050000
          0003000000020000000100000000000000000000000000000000000000000000
          0000000000000000000100000002000000030000000400000005000000050000
          0003000000020000000100000000000000000000000000000000000000000000
          00000000000100000003000000070000000D0000001200000014000000130000
          000F000000080000000300000001000000000000000000000000000000000000
          00000000000100000003000000070000000D0000001200000014000000130000
          000F000000080000000300000001000000000000000000000000000000000000
          000000000002000000080F0802303A1D088A5F2E0BCE77390FF9602D0BCF3C1C
          078D100702330000000B00000003000000010000000000000000000000000000
          000000000002000000080F0702303A1C088A5E2C0CCE76380FF95F2D0BCF3B1B
          078D0F0702330000000B00000003000000010000000000000000000000000000
          000100000005110A042D5C3917B0B47B42FFDD984BFFF1B773FFDEA56AFFB274
          3CFF53270AB21007023200000008000000020000000000000000000000000000
          0001000000051009032D583415B0AB6C35FFD08B49FFE39B55FFCF8847FFAA68
          31FF52260BB21007023200000008000000020000000000000000000000000000
          000200000008482C1486CE9C6AFFECB16AFFDE9C56FFD28E4EFFDD9C55FFEBA7
          5AFFB2723DFF411F098C0000000D000000030000000100000000000000000000
          00020000000842271186BC8756FFDE9B57FFCE9053FFC0834BFFCD8F52FFDD99
          55FFAA6C3CFF3F1E0A8B0000000D000000030000000100000000000000000000
          00020000000A81542CCBE8C18FFFD29353FF6945229D170F08396B45239FD293
          52FFDB9D5AFF713C17CE00000011000000040000000100000000000000000000
          00020000000A734927CBD7985AFFC1854DFF5E3F229D150E08395F40239FC286
          4CFFD08C4FFF6B3616CE00000010000000040000000100000000000000000000
          000200000009B27A45F9FAE3C2FFAB6830FF150D063500000015150D0638AF6A
          30FFF7CB93FF995826F900000013000000060000000200000001000000010000
          0003000000099E6A3EF9EFBB85FF9B5E30FF120C063500000015130C06389E61
          32FFEDB880FF8D4E24F900000010000000040000000100000000000000000000
          000200000007966B42CAEED6BAFFC18A59FF623C1B9A150D0634623C1C9CC487
          4FFFEAC9A3FF845228D20000001A0000000D0000000800000008000000060000
          00060000000B865E3ACEE5C19BFFB77643FF55351C9A130C063456361D9CB877
          44FFDFB790FF774725CC0000000D000000040000000000000000000000000000
          00010000000462482F81DDBA95FFEBD5BEFFC08A5FFFA45B2BFFC18757FFEDD1
          B1FFE2BE9DFF764B2FCF0000002E000000270000002B0000002D0000002A0000
          002300000023745038CEE2BF9AFFE4BE9BFFB87B4CFF9A572AFFB87C4CFFE4BE
          99FFC49974FF4F321D8500000009000000020000000000000000000000000000
          0000000000021A140D25856543AADFBD99FFF0DDC9FFFCF4EAFFF2E1CFFFECD1
          B3FFEBD2B8FFB9A093FF875849FF875648FF865446FF845244FF815042FF6D41
          35FF6C3F34FFA48271FFE2C29FFFE3C29EFFEBD4BAFFFCEEDAFFEAD2B8FFCDA7
          84FF714D2FAC150E082A00000004000000010000000000000000000000000000
          000000000001000000021B140E24654D3580A27B53C9CA9767F9A0784FCE8B65
          48CACBB1A4FFFAF9F9FFE9D7C9FFEAD8CCFFEAD8CCFFEAD8CCFFE7D8D3FF8E67
          5CFFB59D93FFCBB6ABFFA88677FF77553CCA926E4ACCB68459F88E6744CA583F
          298217100A280000000500000001000000000000000000000000000000000000
          00000000000000000001000000020000000300000005000000070000000D0000
          001950383093CFB7AFFFFFFEFEFFEBD9D0FFBE9F92FF95685BFFBD9E92FFC6B2
          ACFF97766BFF95756AFF38211B98000000130000000600000005000000050000
          0005000000030000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000002000000030000
          000800000018593F36AAD1B8B1FFFFFEFEFFB18D81FFE1D0C8FFB08D81FFEEDE
          D6FF936E63FF3E251EAF00000019000000050000000200000001000000010000
          0001000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0003000000125F4035D0B89D92FFDECAC3FFE1D3CEFFC5A9A0FFD9C5BCFFEFE2
          D8FFDCCEC9FF58352BCF00000017000000070000000200000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000532221D6BAA8D82FFE0D9D3FFC7B1A8FFDECBC3FFFFFFFEFFF2E4DEFFF2E6
          DEFFF2E4DCFFA68981FF2919156E0000000D0000000400000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020B08
          061F8C675BEBE6DBD4FFE9E2DCFFE1D9D4FFCEBBB3FFD8C2BBFFFFFFFFFFF5E8
          E0FFF3E9E2FFEEE2DEFF775045EE080504260000000800000002000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000075A41
          38A0CCB5ABFFF0E8E2FFEDE6E0FFF6F4F3FFB8A49DFF704F44CBD2BBB2FFFFFF
          FFFFF5EDE6FFF5EDE6FFC8B5AFFF462B24A20000000E00000005000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000032018153FAF8B
          7FFDF0E8E1FFF1E8E3FFFCFAF8FFC6B1A9FF4530288D00000016523B338BD2BB
          B2FFFFFFFFFFF8F0EAFFF7EFEAFF947065FD180F0C4500000009000000020000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000010202010B87675AD5E3D5
          CEFFF1E9E5FFFCFAF9FFCAB6AEFF4B352D8A00000009000000050000000C533C
          3489D2BBB3FFFFFFFFFFF9F3EEFFE5DBD7FF674136D501010112000000050000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000347373072C9ADA4FFF2EB
          E6FFFCFAF9FFCFBAB3FF4E383289000000070000000100000001000000040000
          000B533C3488D3BCB3FFFFFFFFFFF9F5F3FFB49990FF33211B75000000090000
          0003000000010000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000001130F0D22AE8D80F1EFE6DFFFFCFB
          F9FFD3BEB7FF513D368700000006000000010000000000000000000000010000
          000400000009533D3487D3BCB4FFFFFFFFFFF4EEEBFF855F52F10D0807280000
          0005000000010000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000004775E54AADECCC3FFFDFBFAFFD7C2
          BBFF55413A860000000600000001000000000000000000000000000000000000
          00010000000300000008543D3686D3BDB4FFFFFFFFFFD3C3BDFF52362CAC0000
          0009000000030000000100000000000000000000000000000000000000000000
          00000000000000000000000000012D232042C9AA9EFFFDFBFAFFDAC6BFFF5944
          3E85000000050000000100000000000000000000000000000000000000000000
          0000000000010000000300000007543E3585D4BDB5FFFFFFFFFF9F7F73FF1E13
          1046000000050000000100000000000000000000000000000000000000000000
          000000000000000000000504030AA7887CDEF4EDEAFFDCC9C2FF5B4841840000
          0004000000010000000000000000000000000000000000000000000000000000
          000000000000000000010000000200000006543E3684D4BDB5FFEEE8E6FF7350
          42DE0302020E0000000300000001000000000000000000000000000000000000
          000000000000000000015C4B457BDECAC1FFDFCCC5FF5D4B4483000000030000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000010000000200000005553E3683D5BEB6FFBFA8
          A0FF3F2A227D0000000500000001000000000000000000000000000000000000
          0000000000001A151324C5A79DF5E1CFC8FF604E478200000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000200000004553F3782D5BE
          B6FF926E63F5130C0A2900000002000000010000000000000000000000000000
          0000000000018D746AB5E3D1CAFF625049810000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000035540
          3881D5BFB7FF604137B600000003000000010000000000000000000000000000
          000000000001CDAC9EFF63514B80000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          000256403880AD8071FF00000002000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000010000000100000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010101010302020206040202090403020A0403030B0403030B0403
          030B0403030B0503030C0503030C0503030C0503030C0503030C0503030C0503
          030D0403030C0503030B03020208010101030000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000010000020402020A0A0505180D0808230F090928100A0A2A100A0A2B100A
          0A2B110B0A2C110B0A2D110B0A2D110B0A2E110B0A2E120B0B2F120B0B30120B
          0B30120B0B30100A0A2B0C07071E0503030D0101010300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000002020206080605177B584CC6A47564FFA37464FFA37463FFA37363FFA272
          62FFA27261FFA2705FFFA26F5FFFA06F5FFFA06E5DFF9F6E5DFF9F6D5CFF9E6D
          5CFF9E6D5CFF9C6B5BFF775145CA0B07071E0302020801000001000000000000
          0000000000000000000000000001000000010000000100000001000000010100
          0002030202090C080721A67767FFFEFDFCFFFCFAF8FFFCFAF8FFFCFAF7FFFCF9
          F7FFFCF9F7FFFCF8F6FFFBF8F6FFFBF7F5FFFBF7F5FFFBF6F4FFFBF7F3FFFAF6
          F3FFFBF5F2FFF9F5F1FF9F6D5CFF100A0A2A0402020A01000001000000000000
          0000000000010000000100000002000000040000000500000005000000060000
          00070302020E0D080827A77A69FFFEFDFCFFF7F1EBFFF7F0EBFFF8F0EAFFF7F0
          EAFFF7EFEAFFF7EFEAFFF7EDE9FFF6EFE8FFF6EFE8FFF6EDE8FFF7EDE8FFF6ED
          E6FFF4EDE6FFFBF6F3FFA06F5EFF110B0A2D0503030B01000001000000000000
          000000000001000000040000000A000000100000001400000015000000150100
          00160302021D0C080734A97A6AFFFEFDFCFFF8F1ECFFF7F1ECFFF7F0EBFFF8F1
          EAFFF8F0EAFFF7F0EAFFF7F0E9FFF7F0E9FFF6EFE9FFF6EDE9FFF7EDE9FFF7EF
          E9FFF6EDE8FFFBF7F4FFA27061FF110B0A2D0403030B01000001000000000000
          000000000002000000090E1F32592B62A1EA2E6AB1FF2E6AB0FF2D69AFFF2D67
          ADFF2E66ABFF6E8CB6FFAC7D6EFFFEFDFDFFF8F2EDFFF8F1EDFFF8F1ECFFF8F1
          ECFFF8F1EBFFF8F0EBFFF8F0EAFFF7EFEAFFF7F0EAFFF7EFEAFFF7F0E9FFF7EF
          E9FFF6EDE9FFFBF7F5FFA37363FF100A0A2B0403030B01000001000000000000
          0000000000040000000F2D65A1E869AFDEFF66BAEBFF65B9ECFF62B8EBFF61B7
          EAFF60B3E6FF8CBCDBFFAD7F70FFFEFDFDFFF8F2EDFFF8F2EDFFF8F2EDFFF8F1
          ECFFF8F1EDFFF7F1ECFFF8F1ECFFF7F0EBFFF7F0EAFFF7F0EBFFF7EFEAFFF7EF
          EAFFF7EFEAFFFCF8F5FFA47564FF100A092A0403030B01000001000000000000
          000000000004000000113573B6FF82CAF1FF58B2EAFF51AEE8FF4FADE8FF4DAB
          E6FF4EA8E2FF82B6DAFFAF8172FFFEFEFDFFF8F3EFFFF8F2EFFFF9F2EFFFBB91
          7FFFBB907EFFBA8E7DFFB98E7CFFB78C7AFFB78A7AFFB68978FFF8F1EBFFF7F0
          EBFFF7F0EBFFFCF8F6FFA57667FF0F0A09280403030A01000001000000000000
          000000000004000000123777B8FF88CCF1FF54B1E9FF53B0E9FF52AFE8FF51AE
          E8FF51AAE3FF84B8DCFFB08474FFFEFEFEFFF9F3F1FFF9F3F0FFF9F3EFFFF4EB
          E6FFF4EBE6FFF3EAE5FFF4EAE5FFF2E9E5FFF2E9E3FFF2E8E2FFF8F1ECFFF7F1
          ECFFF7F0EBFFFCF9F7FFA77968FF0F0909270403030A01000001000000000000
          000000000004000000113A7BBAFF8ED0F3FF58B3EAFF56B2E9FF55B1EAFF54B1
          E8FF53ACE4FF86BBDDFFB18576FFFEFEFEFFF9F4F1FFFAF4F0FFF9F4F0FFBF96
          85FFBE9684FFBD9483FFBC9281FFBB9180FFBB8F7EFFBA8E7DFFF8F1EDFFF8F1
          ECFFF8F1ECFFFCFAF8FFA87B6AFF0E0908250402020901000001000000000000
          000000000004000000103C7EBDFF93D3F3FF5BB6EAFF59B5EAFF58B4EAFF58B2
          E8FF56AFE5FF88BEDFFFB48778FFFEFEFEFFFAF6F2FFFAF6F2FFFAF4F1FFF5ED
          E8FFF4ECE7FFF4EBE8FFF4ECE7FFF4EBE6FFF4EAE6FFF3EAE5FFF9F3EFFFF8F2
          EFFFF8F2EDFFFCFAF8FFA97C6DFF0E0908240402020901000001000000000000
          0000000000040000000F3E82BFFF98D5F3FF5EB8ECFF5DB8ECFF5BB6EBFF5BB4
          E9FF59B3E7FF8ABFE0FFB58979FFFFFEFEFFFAF7F3FFFAF6F3FFF9F6F2FFC39C
          8DFFC19B8AFFC19989FFBF9886FFBE9685FFBE9484FFBD9382FFF9F3EFFFF9F3
          EFFFF8F3EFFFFDFBF9FFAD7E6FFF0D0808220402020901000001000000000000
          0000000000040000000F4285C1FF9DD9F5FF62BBECFF62BAECFF5FB9ECFF5EB8
          ECFF5DB5E8FF8CC1E2FFB78A7BFFFFFEFEFFFAF7F3FFFAF7F3FFFAF7F3FFF6EF
          EBFFF6EFEAFFF5EFEAFFF5EEE9FFF4ECE9FFF4ECE8FFF4ECE7FFFAF4F1FFF9F3
          F0FFF9F3F0FFFDFBFAFFAE8272FF0C0807210302020801000001000000000000
          0000000000030000000E4388C3FFA3DCF5FF66BEEDFF65BDEDFF64BBEDFF62BB
          ECFF61B8E9FF8FC4E3FFB78D7CFFFFFEFEFFFAF8F4FFFBF7F4FFFAF7F4FFC7A4
          93FFC6A291FFC6A090FFC39E8EFFC29C8DFFC29A8AFFC19988FFF9F6F1FFF9F4
          F1FFF9F4F1FFFDFCFAFFAF8374FF0C07071F0302020801000001000000000000
          0000000000030000000D468BC5FFA8DEF6FF6AC1EEFF69C0EEFF68BFEEFF66BD
          EDFF65BBEAFF91C7E6FFB98F7EFFFFFFFEFFFBF8F6FFFAF8F6FFFBF8F6FFF7F1
          ECFFF6F0ECFFF7F0ECFFF6F0ECFFF6F0EAFFF5EFEAFFF5EFEAFFFAF6F2FFF9F4
          F2FFFAF4F1FFFDFCFBFFB18575FF0B07061D0302020801000001000000000000
          0000000000030000000C488FC7FFADE1F7FF6EC3EFFF6CC2EEFF6BC2EEFF6AC0
          EEFF68BDEAFF94C9E7FFBA9081FFFFFFFFFFFBF9F7FFFBF9F7FFFBF8F6FFCBA9
          9AFFCAA798FFC9A696FFC9A594FFC7A392FFC7A292FFC5A08FFFFAF7F3FFFAF7
          F2FFFAF6F2FFFEFDFBFFB28677FF0B07061C0302020701000001000000000000
          0000000000030000000C4A92C9FFB1E3F7FF72C6F0FF70C5EFFF6FC4EFFF6DC3
          EEFF6CC0EBFF96CBE7FFBB9282FFFFFFFFFFFDF9F7FFFDF9F8FFFBF9F7FFFBF9
          F7FFFBF9F6FFFBF8F6FFFBF8F6FFFBF8F6FFFBF8F4FFFBF7F4FFFAF8F4FFFAF7
          F4FFFAF6F3FFFEFDFCFFB48879FF0A06061A0302020701000001000000000000
          0000000000030000000B4D95CAFFB7E6F8FF76C9F0FF74C8F1FF74C7F0FF72C6
          EFFF70C3EDFF9ACFEAFFBD9383FFFFFFFFFFFDFAF8FFFDFAF9FFFDF9F8FFFBF9
          F7FFFBF9F8FFFBF9F7FFFBF9F7FFFBF9F6FFFBF9F6FFFAF8F6FFFBF8F4FFFBF7
          F4FFFBF8F4FFFEFDFCFFB6897AFF090606190202020601000001000000000000
          0000000000030000000A4F98CCFFBBE8F9FF7ACCF2FF78CBF2FF77CAF1FF75CA
          F0FF74C6EEFF9DD1EBFFBD9384FFFFFFFFFFFEFAF9FFFDFAF9FFFDFAF8FFFDFA
          F8FFFBFAF9FFFDFAF8FFFDF9F8FFFBF9F7FFFBF9F7FFFBF9F7FFFDF9F6FFFBF8
          F6FFFBF8F4FFFEFDFDFFB78D7CFF090605180202020601000001000000000000
          0000000000020000000A529ACEFFBFE9F9FF7ECFF2FF7CCEF2FF7BCDF2FF79CD
          F1FF78CAEFFF9FD5EDFFBE9585FFFFFFFFFFFEFBFAFFFDFBF9FFFDFBF9FFFDFA
          F9FFFDFBF9FFFDFAF8FFFDFAF8FFFDFAF8FFFDFAF8FFFBF9F8FFFDF9F7FFFBF8
          F7FFFBF8F6FFFEFEFDFFB98F7DFF080505150201010501000001000000000000
          00000000000200000009539DD0FFC4EDFAFF83D2F3FF81D1F3FF7FCFF4FF7DCF
          F3FF7CCDF0FFA3D7EFFFBF9687FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFEFFFFFE
          FEFFFFFEFEFFFFFEFEFFBA907FFF070404120201010401000001000000000000
          0000000000020000000856A0D1FFC7EEFBFF86D5F4FF85D4F4FF84D2F4FF82D2
          F4FF80D0F3FFA6DAF3FFB8A6A1FFC09688FFBF9687FFBF9686FFBF9687FFBE96
          86FFBE9586FFBE9485FFBE9584FFBD9484FFBD9484FFBD9384FFBD9384FFBD92
          83FFBD9282FFBD9282FF8D6D62C40403030C0101010300000000000000000000
          0000000000020000000858A2D3FFCBEFFBFF8AD8F6FF89D8F5FF88D6F5FF86D5
          F4FF84D3F3FFAADFF5FFA2D5EBFF9DCBE4FF9CCBE3FF9DCAE2FF9BCBE3FF9BCC
          E3FF9BCBE4FF9ACCE5FF9BCDE6FF9BCEE8FF9CCEE8FFC1E0EEFF82ACCFFF0604
          0419060404130603030F0402020B020101050000000100000000000000000000
          000000000002000000075AA4D4FFCFF1FCFF8EDBF6FF8CDBF6FF8BD8F4FF8AD5
          F3FF87D2F0FF79BFDDFF6CADCDFF6BABCBFF6BA9CAFF6AAACBFF6BACCCFF6BAC
          CEFF6CB0D2FF6EB5D6FF70B8DBFF72BEE3FF72C0E7FFADDCF1FF4D90C5FF0201
          010F010101060101010301010102000000010000000000000000000000000000
          000000000002000000065BA7D5FFD1F3FCFF92DDF7FF90DCF6FF8ED8F2FF9AD7
          ECFF8EBCD0FF7EA0B5FF7A98ACFF7D95A7FF7C94A4FF7C93A4FF7B96A8FF7F9D
          AEFF7DA2B7FF76A8C2FF6CADCBFF70B5D6FF73BFE2FFAFDDEFFF4F94C9FF0000
          0009000000020000000000000000000000000000000000000000000000000000
          000000000001000000065DA9D7FFD5F4FCFF96E1F8FF9AE0F5FFB5DDECFF99B4
          C1FF7D7D83FF7E6963FF806458FF845C4FFF84594BFF7F584BFF7A5A50FF765E
          59FF777274FF8394A1FF87A5B5FF74A9C4FF73B6D4FFB3DCEEFF5398CAFF0000
          0008000000020000000000000000000000000000000000000000000000000000
          0000000000010000000560ACD8FFD8F6FDFFACEAFAFFBEE6F3FF9AACB5FF8F71
          68FFAF8D7DFFC3A898FFCCB3A2FFD4BDACFFD7C0AFFFCFB6A5FFC3A794FFB597
          85FF9B7666FF7D5D51FF7F888EFF8FA9B8FF85BBD1FFB8DEECFF559ACBFF0000
          0007000000020000000000000000000000000000000000000000000000000000
          00000000000100000003589CC4E8BBE4F3FFD8F6FDFFDFEDF0FF9D7C6FFFE6DA
          D3FFF4EFEAFFF4EDE7FFF2EBE4FFE9DDD3FFDDC9BAFFDBC5B6FFE3D1C4FFE8DA
          D0FFE5D6CAFFD1BCAFFF825D51FFB2BCBFFFB4D1DBFFA2CEE2FF4F8EB9E80000
          0005000000010000000000000000000000000000000000000000000000000000
          00000000000000000002182B35424E88AACA62ADD8FFA9CDE3FFAC8F83FFB087
          78FFAF8877FFAD8576FFAD8475FFEDE1D9FFE4D5C8FFE0D0C2FF9E7564FF9E75
          62FF9D7462FF9B7361FF997669FF9CB7CAFF5899C5FF477DA1CD162833450000
          0003000000010000000000000000000000000000000000000000000000000000
          00000000000000000001000000020000000300000003000000060000000B0000
          000E0000000E0000001281645AC2E2D1C9FFF3EDE7FFDCCBC0FF795B4FC90000
          0017000000130000001300000011000000080000000500000004000000020000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000001000000010000
          00010000000100000002130F0D227E6358B7AF897CFA75584DBA120D0C230000
          0002000000010000000100000001000000010000000100000001000000010000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000010000000200000004000000060000000700000007000000070000
          0007000000070000000800000008000000080000000800000008000000080000
          0008000000080000000800000008000000080000000800000008000000080000
          0008000000070000000500000003000000010000000000000000000000000000
          0000000000020000000800000011000000180000001B0000001C0000001D0000
          001D0000001D0000001D0000001D0000001E0000001E0000001E0000001E0000
          001F0000001F0000001F0000001F0000001F0000002000000020000000200000
          001F0000001C0000001400000009000000020000000000000000000000000000
          000100000004000000117E5E52C1AF8271FFAE8172FFAE8171FFAE8070FFAD80
          70FFAD7F70FFAC7F6FFFAC7E6EFFAC7E6EFFAB7E6DFFAB7D6DFFAB7D6CFFAB7D
          6CFFAA7C6CFFAA7B6BFFA97C6BFFA97A6AFFA97A6AFFA97A6AFFA87969FFA879
          69FFA87969FF78564AC300000014000000050000000100000000000000000000
          00010000000600000016B08374FFFDFCFAFFFBF8F6FFFBF8F5FFFBF7F5FFFBF7
          F4FFFAF7F4FFFBF6F3FFFBF6F3FFFAF6F2FFFAF5F2FFFAF5F1FFFAF4F1FFF9F4
          F1FFF9F4F0FFF9F3EFFFF8F2EEFFF8F3EEFFF8F2EDFFF8F2ECFFF8F1ECFFF7F0
          ECFFF7F0EBFFA8796AFF0000001B000000070000000100000000000000000000
          00010000000600000018B18576FFFDFCFBFFF6EEE8FFF6EEE8FFF6EEE8FFF6ED
          E7FFF6EDE7FFF5EDE6FFF5ECE6FFF5ECE6FFF6ECE6FFF5ECE5FFF5EBE5FFF5EC
          E5FFF4EBE4FFF5EBE4FFF5EBE4FFF5EAE4FFF4EBE3FFF5EAE3FFF4EAE3FFF4EA
          E3FFF8F1ECFFA97B6AFF0000001E000000080000000100000000000000000000
          00010000000600000018B38777FFFEFCFBFFF6EEE9FFF7EFE8FFF6EFE9FFF6EE
          E8FFF6EDE8FFF6EDE7FFF6EDE7FFF5EDE7FFF6EDE7FFF5ECE6FFF5EDE6FFF5EC
          E6FFF5ECE5FFF5ECE5FFF5ECE5FFF4ECE5FFF4EBE4FFF5EBE4FFF5EAE4FFF5EA
          E3FFF9F2EDFFAA7D6CFF0000001E000000070000000100000000000000000000
          00010000000600000017B48979FFFEFCFBFFF6F0EAFFF7EFEAFFF5EDE8FFF5ED
          E7FFF6ECE6FFF6ECE6FFF5ECE6FFF5ECE5FFF5ECE5FFF5EBE5FFF4EBE4FFF5EB
          E4FFF5EAE4FFF4EBE4FFF4EAE3FFF4E9E3FFF4E9E3FFF3E9E3FFF5EBE4FFF5EB
          E4FFF8F2EDFFAC7D6DFF0000001D000000070000000100000000000000000000
          00010000000500000016B58B7CFFFEFDFCFFF8F0EBFFF7EFEAFFB6ACD2FF392A
          A7FF847FA2FFF7EFE9FFF6EEE9FF7B769BFF3828A5FFBAB0D3FFF6EEE8FFF6ED
          E7FFF5EDE7FFF6EDE7FFF6ECE6FFF5ECE6FFF5ECE5FFF4EAE3FFF5EBE5FFF5EC
          E4FFF9F3EEFFAC7F6FFF0000001C000000070000000100000000000000000000
          00010000000500000015B68C7EFFFEFDFCFFF7F1ECFFF7F1EBFF392BABFF6675
          E5FF29319FFF7F7AA0FF76729BFF262F9DFF5F6EE2FF3426A6FFF6EEE8FFF6EE
          E8FFF6EEE7FFF6EDE8FFF6EDE7FFF6EDE6FFF5ECE7FFF4EAE4FFF5ECE6FFF5EC
          E6FFFAF4F0FFAD8070FF0000001B000000070000000100000000000000000000
          00000000000500000014B78E80FFFEFDFDFFF8F1EDFFF8F1ECFF9086CAFF6467
          D0FF7083EFFF2E37A7FF2D36A7FF6C80EDFF5D5FCDFF9086C7FFF7EFE9FFF7EF
          E8FFF6EEE8FFF6EEE8FFF6EEE8FFF6EDE7FFF6EDE7FFF5EBE5FFF6EDE7FFF5EC
          E7FFF9F4F1FFAF8272FF0000001A000000070000000100000000000000000000
          00000000000500000013B88F82FFFEFDFDFFF8F2EEFFF8F1EDFFF7F1EBFF8F87
          CCFF5B61D3FF5A70ECFF596DECFF575CD1FF8F87CBFFF7F0EAFFF7F0EAFFF6EF
          EAFFF7EFEAFFF6EFE9FFF6EEE9FFF6EEE8FFF6EEE8FFF5ECE6FFF6EEE8FFF6ED
          E7FFFAF5F2FFAF8373FF00000019000000060000000100000000000000000000
          00000000000400000012BB9184FFFEFEFDFFF8F3EEFFF8F2EEFFF7F1EBFF847F
          BEFF444DC4FF8094F3FF7F92F2FF3E49C1FF8A84BDFFF7F0EBFFF8F0EBFFF8F0
          EAFFB38878FFB38878FFB28677FFB28677FFB28676FFB28575FFB18575FFF6EE
          E8FFFAF5F3FFB18575FF00000018000000060000000100000000000000000000
          00000000000400000011BB9485FFFEFEFDFFF9F4F0FFF9F3EFFF8D88C6FF4A56
          C8FF899EF6FF656DCBFF656BCAFF8599F5FF454FC4FF8E89C3FFF7F1ECFFF8F0
          EBFFF7F0EBFFF7F0EBFFF7F0EBFFF7EFEAFFF7F0EAFFF5EDE7FFF7EEE9FFF6EE
          E8FFFAF6F3FFB28677FF00000017000000060000000100000000000000000000
          00000000000400000010BC9788FFFEFEFDFFF9F3F0FFF9F4EFFF5C5BCAFFC1CB
          F7FF8B91E0FFA19ED9FF9A97D7FF888DDEFFBBC6F7FF5855C5FFF8F1EDFFF8F1
          EDFFF8F0ECFFF7F1EBFFF7F1EBFFF7F0EBFFF8F0EBFFF6EFE8FFF6F0EAFFF7F0
          EAFFFBF7F4FFB48979FF00000016000000060000000100000000000000000000
          0000000000040000000FBF988AFFFEFEFEFFFAF5F1FFF9F4F1FFC5C2E5FF6567
          D1FFAAA8DFFFF9F3EFFFF9F3EFFFA4A2DCFF6261CDFFC8C3E2FFF9F2EDFFF8F2
          EDFFF8F2ECFFF8F2ECFFF7F1ECFFF7F0ECFFF8F0ECFFF7F0EAFFF7F0EBFFF8F0
          EBFFFBF8F5FFB58A7AFF00000015000000050000000100000000000000000000
          0000000000040000000EC0998BFFFEFEFEFFFAF5F2FFFAF5F1FFF9F4F1FFF8F3
          EFFFF3EEEAFFEAE6E2FFEEE9E6FFF7F1EEFFF9F3EFFFF9F3EFFFF9F3EEFFF9F2
          EEFFF9F2EDFFF8F1EDFFF8F2EDFFF8F1EDFFF8F2ECFFF7F0EAFFF7F0ECFFF8F0
          EBFFFBF8F6FFB58C7DFF00000015000000050000000100000000000000000000
          0000000000030000000EC19C8DFFFFFEFEFFFBF6F3FFFAF6F2FFF8F4F0FFF3EE
          EBFFADBAAFFF135839FF698977FFECE8E4FFF8F3EFFFF9F3EFFFF9F3F0FFF8F3
          EFFFF9F3EFFFF8F2EEFFF8F2EDFFF8F2EEFFF8F1EDFFF7F0EBFFF8F1EDFFF8F1
          ECFFFBF9F7FFB78E7FFF00000014000000050000000100000000000000000000
          0000000000030000000DC29D8FFFFFFEFEFFFBF7F4FFFAF6F4FFF4F0EDFFB2C1
          B6FF1D7D57FF2EE1AAFF146E4BFFAAB5ADFFF3EDEAFFF8F4F0FFF8F3F0FFF8F2
          EFFFF8F2EDFFF8F2EDFFF8F2EDFFF8F2ECFFF8F2ECFFF7F1ECFFF8F2EEFFF8F1
          EDFFFCF9F7FFB88F81FF00000013000000050000000000000000000000000000
          0000000000030000000CC49F90FFFFFEFEFFFBF7F5FFF8F4F1FFB9C8BDFF2483
          5EFF40ECB8FF38ECB7FF2BD09CFF256447FFDCDCD6FFF7F2EEFFFAF5F1FFF9F4
          F1FFFAF4F1FFF9F4F1FFF9F4F0FFF9F3EFFFF9F3EFFFF7F2EDFFF8F2EEFFF8F2
          EEFFFAF7F5FFB99283FF00000012000000040000000000000000000000000000
          0000000000030000000BC4A192FFFFFFFEFFFAF6F4FFBFD0C5FF328A68FF5EF2
          C9FF71F9D7FF77F7D7FF41EBB9FF22AE7FFF517B66FFEFEBE8FFF8F3F0FFF9F4
          F0FFBE9789FFBD9687FFBD9487FFBC9587FFBC9485FFBC9385FFBA9385FFF7F1
          ECFFF9F4F3FFBB9284FF00000011000000040000000000000000000000000000
          0000000000020000000AC6A294FFFFFFFEFFFBF7F5FFACCBBBFF4DAD8CFF78DC
          C0FF419E7EFF237956FF6ADFC1FF41E9B8FF1C966CFF799887FFF4F0ECFFF9F4
          F2FFFAF6F2FFFAF5F2FFFAF5F2FFFAF5F1FFFAF5F1FFF8F3F0FFF7F2EDFFF6EF
          EAFFF7F2EFFFBD9486FF00000010000000040000000000000000000000000000
          00000000000200000009C8A495FFFFFFFFFFFCF9F6FFFAF8F5FFA8C9B9FF589D
          81FF9FBEAEFFD1DBD3FF479577FF6DE2C4FF48ECBDFF178058FFA3B6AAFFF8F4
          F0FFFAF6F3FFF9F5F2FFFAF6F3FFF9F5F1FFF9F4F0FFF6F1ECFFF5EFEAFFF3EA
          E6FFF3EDEAFFBD9888FF0000000F000000040000000000000000000000000000
          00000000000200000008C8A597FFFFFFFFFFFCFAF8FFFCFAF7FFFCF8F6FFFBF7
          F4FFFAF6F5FFFAF6F4FFE0E7E0FF449273FF62D0B3FF66EFCCFF2A8362FFF5F2
          F0FFFBF6F3FFF8F3F0FFF6F0ECFFF5EEEAFFF4EBE6FFF0E6E3FFEFE4DFFFECE0
          DBFFECE1DDFFBF988AFF0000000E000000040000000000000000000000000000
          00000000000200000008C9A698FFFFFFFFFFFCFAF9FFFCFAF8FFFCF9F8FFFCF9
          F7FFFCF9F7FFFCF9F7FFFBF9F6FFEBEDE9FF58997EFF379675FFC1D0C7FFF9F5
          F3FFFAF6F3FFF6EFEAFFEFE3DEFFE7D9D2FFE2D3CBFFE1CFC8FFDFCCC4FFDCC8
          BFFFDCC9C2FFC19A8CFF0000000D000000030000000000000000000000000000
          00000000000200000007CBA899FFFFFFFFFFFDFAF9FFFDFAF9FFFCF9F7FFFCF9
          F7FFFCF9F6FFFDF9F6FFFCF8F6FFFBF8F6FFFBF7F5FFFBF7F4FFFAF6F4FFFCF7
          F5FFF9F2F0FFF0E7E1FFB89284FFAC7F6FFFAB7E6DFFAB7D6DFFAB7C6CFFAA7C
          6CFFD1B8AFFFC29D8DFF0000000A000000030000000000000000000000000000
          00000000000100000006CBA99BFFFFFFFFFFFDFBFAFFFDFAFAFFFCF9F8FFFCFA
          F9FFFDFAF8FFFCFBF8FFFCFAF8FFFCFAF8FFFCF9F7FFFCF9F7FFFCF9F7FFFBF9
          F6FFF8F2EFFFEDE0DCFFB18676FFFFFFFFFFFFFEFEFFFFFDFCFFFEFCFAFFFCF9
          F7FFD1B7AEFF533C358600000006000000020000000000000000000000000000
          00000000000100000005CBAA9DFFFFFFFFFFFDFBFAFFFDFBFAFFFDFBF8FFFDFB
          FAFFFDFAF9FFFDFBF9FFFDFAF8FFFDFAF9FFFCFAF8FFFCFAF8FFFCFAF7FFFCF9
          F7FFF7F1EDFFECE0DBFFB68D7DFFFFFEFEFFFEFBFAFFFDF9F7FFFCF6F3FFD4BA
          B0FF553F38860000000800000003000000010000000000000000000000000000
          00000000000100000005CCAB9DFFFFFFFFFFFEFCFBFFFEFCFAFFFEFBF9FFFDFB
          FBFFFDFCF9FFFDFBF9FFFDFBF8FFFDFBF9FFFDF9F8FFFCF9F7FFFCF9F8FFFAF6
          F4FFF6F0ECFFEDE2DCFFBC9485FFFFFEFEFFFDF9F6FFFBF6F3FFD6BCB4FF5843
          3B86000000080000000300000001000000000000000000000000000000000000
          00000000000100000004CDAC9FFFFFFFFFFFFDFCFCFFFDFDFCFFFDFCFBFFFDFC
          FBFFFDFCFAFFFDFCFAFFFEFBFAFFFDFBFAFFFDFBF9FFFDFBF9FFFAF8F7FFF9F5
          F3FFF5EEECFFECE2DDFFC19C8CFFFFFEFEFFFBF6F3FFD9C1B7FF5B463F850000
          0007000000030000000100000000000000000000000000000000000000000000
          00000000000100000003CEAD9FFFFFFFFFFFFEFCFCFFFEFDFCFFFEFDFCFFFDFC
          FCFFFDFDFCFFFEFCFBFFFEFCFBFFFDFCFAFFFDFCFAFFFBF8F7FFF9F6F4FFF7F2
          EFFFF3ECE8FFEDE2DDFFC6A293FFFFFEFEFFDBC3BAFF5D494284000000060000
          0002000000010000000000000000000000000000000000000000000000000000
          00000000000100000002CEADA0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF9F9FFF9F6F4FFF6F1
          F0FFF2ECE9FFEEE3E0FFE5D4CEFFE0CCC4FF5F4D458300000005000000020000
          0001000000000000000000000000000000000000000000000000000000000000
          00000000000000000001998076BECEAEA0FFCEADA0FFCEAE9FFFCEADA0FFCEAD
          9FFFCDAC9FFFCEACA0FFCDAC9FFFCDAC9EFFCDAC9FFFCCAC9EFFCCAB9EFFCCAA
          9DFFCCAB9CFFCBAA9CFFCBAA9CFF614F48820000000400000002000000010000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000010000000100000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000001000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010000000200000004000000060000000600000004000000020000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000100000002000000040000000600000006000000040000
          0002000000010000000000000000000000000000000000000000000000000000
          0001000000020000000800000010000000170000001700000012000000090000
          0003000000010000000000000000000000000000000000000000000000000000
          0000000000010000000300000009000000120000001800000018000000110000
          0008000000020000000100000000000000000000000000000000000000000000
          0001000000070000001509123688112168E0102169E50A143E990001031F0000
          000C000000040000000100000000000000000000000000000000000000000000
          0001000000040000000C0201011F2B1E199949322AE5473129E0261A16890000
          0017000000080000000200000000000000000000000000000000000000010000
          00030000000F091237863F68ADFF2478CDFF4599DDFF1E58ADFF0F1E58C10101
          03210000000C0000000400000001000000000000000000000000000000010000
          00040000000C020101213B2823C1877269FFC9BBB1FFC6B6ACFF765E55FF261A
          1689000000110000000400000001000000000000000000000000000000010000
          0005000000141F4C8DDF65A8E0FF2B8DDEFF2B8CDEFF4BA5E6FF236CBFFF0F20
          59C1010103210000000C00000004000000010000000000000001000000030000
          000C020101213B2923C08E7C72FFCFBDB1FFBAA495FFBAA495FFCBBBB1FF4731
          29E0000000170000000500000001000000000000000000000000000000010000
          000400000014235598E993C7ECFF3192E0FF3092E0FF3091E0FF4EA9E8FF2671
          C2FF11215BC1010103200000000B0000000300000002000000030000000B0202
          01203D2B25C088736AFFCDBCB2FFBAA395FFB8A192FFBAA395FFCEBFB5FF5E44
          3BEA000000170000000500000001000000000000000000000000000000010000
          00030000000E173C639888B8DEFF8ECBF1FF3598E2FF3497E2FF3496E1FF52AC
          E8FF2974C3FF11225CC10001031F0000000C000000060000000C0201011F3F2C
          27BF8B776DFFD0BEB5FFBBA599FFBAA396FFBAA293FFCDBCB0FF917970FF3729
          249B000000110000000400000001000000000000000000000000000000000000
          0002000000070103051B245788C0A6D0EAFF91CDF2FF3C9EE3FF3B9EE3FF3A9E
          E3FF56AEE9FF2E79C4FF12235DC00101032100000015020202223C2C2AC48975
          6DFFCFC0B6FFBEA79CFFBCA79AFFBCA598FFCDBCB2FF988178FF4B3630C00202
          021E000000090000000200000000000000000000000000000000000000000000
          000100000003000000090103051B255B8BBFA6D1EBFF94D0F3FF40A5E7FF3FA4
          E7FF3FA3E7FF5AB4EBFF327FC7FF12245EC10404073C3C3641DE675B60FFBBAD
          A9FFBAA79CFFBEA99DFFBEA99BFFCFBEB3FFA59188FF4C3932BF030202200000
          000B000000030000000100000000000000000000000000000000000000000000
          00000000000100000003000000090103051B265E8EBFA7D2ECFF97D5F4FF48AD
          EAFF47AAE8FF46A9E8FF5DB8ECFF3785C9FF1E3272FA2F334FFF6D6C7CFF8F83
          84FFAE9C94FFBCA69BFFD0BFB5FFA69389FF4F3B33BF0302021F0000000B0000
          0003000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000002000000080103051A276090BEA8D4EEFF9AD8
          F5FF4EB3EBFF4EB2EBFF4DB0EAFF62BCEEFF3C8BCDFF1D3476FF3E425CFF6E6A
          77FF928687FFBEB1ACFF9C877FFF503D36BE0302021E0000000B000000030000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000020000000801030519276291BEA8D5
          EFFF9DDBF6FF57BAEDFF56B9EDFF55BAECFF67C1EFFF4391CEFF1F3978FF4649
          61FF7C7A86FF807273FF4F3D39C10302021C0000000A00000003000000010000
          0002000000020000000200000000000000000000000000000000000000000000
          000000000000000000010000000200000002000000060000000C0103051C2865
          94BFA8D6F0FFA1DFF7FF5DC2EFFF5CC0F1FF5BC0EFFF6BC6F1FF4797D1FF253F
          7DFF4C4D62FF4D434AD50302021D0000000A0000000300000002000000020000
          0005000000080000000600000002000000020000000000000000000000000000
          00000000000100000003000000060000000A0000000E00000011000000180507
          0831447FADF2A9D7F1FFA5E1F9FF65C9F2FF64C8F2FF63C8F1FF70CCF3FF498B
          C5FF2B3D6EF40504052B0000000C0000000300000002000000040000000B0000
          00160000001D000000170000000C000000050000000100000000000000000000
          0001000000040000000A140F0D3E47352EB360473EEA694E44FF60463EEB7966
          61EDA9A19DFF5E9CCBFFA9D7F1FFA8E6FAFF6DCFF4FF6DCEF4FF69CEF4FF7BC9
          EDFF1E4284F30000001A0000000A00000003000000030000000B0000001D2D20
          1B9B5D453BFF160C099D000000230000000F0000000500000001000000010000
          000403020211382A258E7D655BFCB9A9A2FFDED5CFFFEAE2DCFFDDD3CCFFC7BC
          B5FFDFD7D2FFC7C0BDFF5D9BC9FFA9D8F2FFB9ECFCFF91E0F8FF88DCF6FF9ED9
          F1FF3974AAFF0301017F0000000D000000060000000700000016382D299A8871
          63FFC5B4A7FF746056FF160C09A0000000230000000F00000005000000030000
          000942332DA1958177FFE5DCD6FFE0D4CDFFDACCC4FFD7C9BFFFD8CAC1FFD9CE
          C4FFD1C3BAFFD5CAC3FFC8C3BEFF5F9FCEFF8CC8ECFFC8EBF9FFC4E9F8FF82B7
          DDFF849BB0FF584E48FF0301017D000000100000001100000023755F57FFC6B5
          A9FFBBA799FFC5B3A6FF756157FF160D099F000000210000000B000000051B15
          1247846D62FFEDE7E0FFDCCEC6FFDACCC4FFD9CAC2FFD8CAC0FFD7C9BFFFD6C7
          BDFFD5C6BCFFD1C2BAFFDFD7D2FFB5ADA9FF527FA0E83184BFEC3888C1FE8FAB
          C1FFC8B5AAFFC5B4A7FF5A4F4AFF0302018200000025000000337B665DFFDFD5
          CCFFC6B5A8FFBCA89AFFC6B4A8FF766258FF170E0A9A00000016000000074C3B
          34ABC6BBB0FFE7DCD5FFE6DDD6FFF6F2E9FFFDFBF1FFFCFAEFFFECE4DAFFD8CA
          C0FFD6C7BDFFDACCC2FFD0C3BDFF84716AEA0505042100000014150F0D567A6A
          64F9D7CBC3FFC8B7ADFFC1B1A6FF645047FF674F47FF654D46FF796259FFBFAD
          A1FFDFD5CBFFC7B6AAFFBEA99CFFE3D8CEFF7C665DFF00000019000000086B54
          4AE6F1EEE3FFE7DFD9FFBFB0A5FF988175FF957F72FFC2B4A9FFFDFCF1FFEBE4
          DAFFD7C9BFFFD8CAC1FFDFD5CDFF766056E80000001000000008000000091C16
          145481726AF9D2C7BFFFC9B6AAFFCDBCB1FFCFBFB5FFCEBDB1FFC6B4A9FFB19B
          8DFFEDE6DDFFFDFCF6FFFCFAF1FFC4B8AFFF463B36960000001400000007785F
          54F9F8F5EAFFAF9C90FF6A5951B90907071C0504031753433BAAC0B2A7FFFCFA
          F0FFD8CAC1FFD6CAC0FFE7DFD9FF866E62FA0000000D00000004000000030000
          00081F1A1758745F56FCD7C8BDFFD4C6BBFFD0C1B6FFCFBEB3FFCCBBB0FFC8B7
          ACFF8A766CFF9E887EFF9C877DFF4D423E94000000190000000900000006715A
          50E5B3A196FF6F5E57B60403030F000000060000000704040317887166FCFEFC
          F2FFD9CBC2FFDACDC3FFE0D7D1FF7E695EE70000000B00000003000000010000
          000500000011503F37CBC4B7ADFFD7CABFFFD5C6BCFFD3C4B9FFD1C1B6FFD1C2
          B8FF7D655DFF0000002D0000001D000000120000000900000003000000045444
          3BA872635CB40403030D0000000400000003000000050A0807217E655AFCF7F4
          EBFFDACCC3FFE1D6CEFFC6B8AFFF5F5048AC0000000800000002000000020000
          00050000001030262183A5958BFFDDD0C7FFD9CBC1FFD7C8BEFFD5C6BCFFE9E1
          D7FF856E66FF0000001A0000000B000000040000000100000001000000010000
          0003000000030000000200000002000000030302020E523F37BCA7968CFFE7DE
          D9FFDCCFC6FFE6DCD7FFA48F84FF241E1B470000000700000005000000050000
          000A0000001559463FC7CDC1B9FFE7DDD6FFE2D8D1FFDCD1C9FFEEE7DDFFC4B8
          AEFF453B36900000000F00000004000000000000000000000000000000000000
          00010000000100000001000000020302020C4B3932B5897369FFE4DCD6FFE6DD
          D6FFEEE8E1FFB8A79CFF5B4F479900000009000000091612103B0706051E100D
          0B334B3D36A5C3B5AEFFEDE5DFFFEAE3DCFFEBE4DDFFF6F1E8FFC3B5AAFF4B40
          3B8F000000120000000700000001000000000000000000000000000000000000
          000000000000000000010000000349362FB3836D63FFF4F2E7FFF1EDE3FFDAD0
          C5FFAB998EFA4F443F820403030D00000006463A349099867BFF8A7469FF937F
          74FFC7B9B2FFF0E8E2FFF2ECE5FFF6F1EAFFF2EDE3FFBCACA1FD443A367C0000
          0010000000070000000300000000000000000000000000000000000000000000
          000000000000000000010000000373665DAD99867CE5AA968BFF98857AE57164
          5CAF1E1A18340000000500000003000000042A2522429A877EDCD3C6BAFFEAE3
          D8FFFCF9EFFFFBF8EEFFE9E2D7FFD0C3B8FF9D8B7FE8322B285B0000000C0000
          0005000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000030000000400000005000000050000
          000400000003000000010000000100000001000000040404030E5147427A8A7A
          71C7B49E92FFB19B90FC88786FC84F46417D0807071900000008000000040000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000001000000010000
          0001000000010000000000000000000000000000000100000003000000040000
          0007000000090000000900000009000000060000000400000002000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0001000000020000000200000002000000010000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C54000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C54000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C540000D9930000D9930000D9930000D993
          0000D9930000D9930000D9930000D9930000D9930000D9930000D9930000D993
          00007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C540000D9930000D9930000D9930000D993
          0000D9930000D9930000D9930000D9930000D9930000D9930000D9930000D993
          00007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          000000000000000000007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C540000F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C5400007C540000F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C5400007C540000F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C5400007C540000F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C5400007C540000F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C5400007C540000F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C5400007C540000F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FB
          FF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00F0FBFF00D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C5400007C540000D9930000D993
          0000D9930000D9930000D9930000D9930000D9930000D9930000D9930000D993
          0000D9930000D9930000D9930000D9930000D9930000D9930000D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C5400007C540000D9930000D993
          0000D9930000D9930000D9930000D9930000D9930000D9930000D9930000D993
          0000D9930000D9930000D9930000D9930000D9930000D9930000D9930000D993
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C5400007C5400007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C5400007C5400007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000
          000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000
          000FF000000FF000000FFC00000FFC00000FFC00000FFC00000FFC00000FFC00
          000FFC00000FFC00000FFC00000FFC00000FFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000000000000000000000000000000000000000000020000
          0004000000060000000600000006000000060000000600000006000000060000
          0006000000060000000600000006000000070000000700000007000000070000
          0007000000070000000700000007000000070000000700000007000000070000
          0006000000040000000200000001000000000000000000000000000000060000
          000E000000150000001700000018000000180000001800000018000000190000
          001900000019000000190000001900000019000000190000001A0000001A0000
          001A0000001A0000001A0000001A0000001A0000001B0000001B0000001A0000
          001700000010000000070000000200000000000000000000000000000010173B
          7ACD2557A4FF2859A5FF2758A6FF2757A5FF2558A5FF2656A3FF2656A4FF2456
          A4FF2455A4FF2455A3FF2354A2FF2353A2FF2252A2FF2253A2FF2151A1FF2151
          A1FF2051A1FF2050A0FF1F4FA0FF1F4F9FFF1F4E9FFF1E4D9FFF1D4D9EFF1C4B
          9CFF173F8AEE040A143D0000000B000000020000000000000000000000151D50
          9FFF2359A5FF4F95CFFF64BBEDFF47A7E7FF46A5E6FF44A4E6FF42A3E5FF40A2
          E4FF3FA1E4FF3E9FE3FF3C9EE3FF3B9DE2FF3A9CE1FF389BE1FF389AE1FF359A
          E1FF3598E0FF3497E0FF3397DEFF3296E0FF3195DEFF3095DEFF3095DEFF2E90
          DAFF297DCAFF12326BBF00000011000000040000000000000000000000162154
          A3FF2E65ADFF3D7BBCFF7ECBF2FF4FADE9FF4BAAE8FF49AAE8FF47A7E6FF45A6
          E6FF43A5E5FF43A3E4FF40A2E5FF40A1E4FF3FA0E4FF3D9FE3FF3B9DE3FF3A9D
          E2FF399CE2FF379BE1FF379AE2FF3599E1FF3398E1FF3298DFFF3197DFFF3096
          DFFF3295DEFF1A4A98F80103051D00000005000000010000000000000014245C
          A9FF3E76B9FF2862ADFF8CD1F2FF64BCEEFF52B0EBFF51AFEAFF50AEE9FF4DAD
          E8FF4CAAE7FF4AA9E7FF48A8E7FF47A7E6FF45A5E6FF43A4E6FF42A3E5FF41A1
          E4FF3FA1E4FF3D9FE4FF3C9FE3FF3B9EE3FF3A9DE2FF399CE1FF379AE1FF3699
          E0FF389CE1FF2363B1FF08162E5D000000080000000100000000000000132661
          AFFF4D86C3FF2765AEFF7CBAE1FF82CFF3FF59B6EDFF57B5EDFF57B4ECFF54B3
          ECFF53B1EAFF51B0EAFF51AEE8FF4FADE8FF4CAAE8FF4AAAE7FF49A8E7FF48A8
          E6FF46A5E5FF44A6E6FF43A4E5FF41A3E5FF40A2E4FF3FA1E3FF3E9FE2FF3C9F
          E2FF3D9FE3FF2F7AC4FF0F2A56970000000A0000000200000000000000122A67
          B1FF5C95CEFF2F6FB7FF619DCFFF9FDEF8FF5FBAF0FF5EBAEFFF5EB9EEFF5CB9
          EDFF5BB7EDFF59B7EDFF58B4ECFF57B4EBFF55B2EAFF53B0EAFF51AFEAFF4FAF
          E9FF4EADE9FF4CABE8FF4BAAE7FF49A8E7FF48A7E7FF46A6E6FF45A5E5FF43A4
          E5FF44A4E4FF4198D9FF164385D80000000D0000000300000001000000112D6C
          B5FF6BA5D6FF387FC4FF4183C0FFB5E8FBFF70C6F3FF65BFF1FF64BFF1FF62BE
          F1FF62BDEFFF61BCF0FF60BBEFFF5DBAEEFF5DB9EEFF5CB7EDFF5AB6ECFF58B5
          EBFF57B3EBFF55B1EBFF53B0E9FF51AFE9FF50AEE9FF4EADE8FF4CABE8FF4AAA
          E6FF49A9E6FF50ADE8FF225EAAFF030A132C0000000400000001000000103071
          B7FF79B4DFFF4791D1FF387EBEFFA7DBF1FF91D9F8FF6BC4F3FF6BC3F3FF69C3
          F3FF69C2F2FF67C1F0FF66C0F0FF66C0F0FF64BFEFFF64BEEFFF62BCEEFF60BC
          EFFF5EBAEDFF5CB9EDFF5CB8EDFF59B5ECFF58B4EBFF56B2EAFF54B1EAFF53B0
          E9FF50AEE8FF56B1E9FF357CC1FF0C2344740000000700000002000000103176
          BAFF88C0E7FF53A2DCFF418CCAFF87BFE0FFB1E8FBFF70C9F5FF70C9F5FF6FC7
          F4FF6EC7F4FF6EC6F3FF6DC6F3FF6CC5F3FF6BC4F1FF6AC3F1FF6AC3F1FF69C1
          F1FF67C0F0FF65BFF0FF63BDEEFF62BDEEFF61BBEEFF5EBAEEFF5DB8ECFF5AB6
          ECFF59B5EBFF5CB7EBFF4D9DD7FF153F75BA00000009000000020000000F347B
          BDFF98CDEDFF61B1E4FF509FDAFF65A6D3FFCBF4FDFF7BD1F7FF75CDF6FF75CD
          F6FF74CCF6FF73CAF6FF73CAF5FF71C9F4FF71C9F4FF70C7F4FF70C7F2FF6EC7
          F2FF6EC6F2FF6DC5F2FF6BC4F2FF6AC3F1FF68C1EFFF66BFEFFF64BFEFFF63BE
          EFFF61BBEDFF61BBEDFF69BDECFF215DA4F701030614000000030000000E357E
          BFFFA3D9F3FF71C1EDFF63B7EAFF4C98CDFFC1E9F7FFD4F6FEFFD4F6FEFFD3F6
          FEFFD2F6FEFFD2F6FEFFD2F5FEFFD0F4FEFFCDF3FCFFAEE8FAFF7ACEF5FF74CA
          F5FF73C9F4FF72CAF4FF72C9F3FF71C8F3FF6FC6F3FF6EC6F2FF6DC4F1FF6BC3
          F0FF69C2F0FF67C0EFFF71C5F0FF377DBFFF091C3255000000050000000D3782
          C1FFB0E2F8FF7CCDF4FF73C7F2FF5FB5E6FF50A0D3FF4D9ACDFF4C98CCFF4B98
          CBFF4996CBFF4894CBFF4693CAFF4591CAFF73B1D9FFC8EEF9FFC5F1FCFF88D6
          F7FF78CEF6FF78CDF6FF77CCF6FF76CDF5FF76CCF5FF75CBF4FF73CAF3FF72C9
          F3FF70C7F2FF6EC6F2FF76C9F2FF56A1D5FF133A659D000000060000000C3985
          C3FFBAEBFCFF87D9FAFF82D6F9FF7BD1F9FF74CEF8FF70CBF8FF6DC7F6FF6AC6
          F5FF68C5F5FF65C1F4FF63BFF4FF5DBAF2FF50AAE5FF529DD2FFA7D5ECFFD9F7
          FEFFD8F7FEFFD7F7FEFFD7F7FEFFD6F6FEFFD6F6FEFFD5F6FEFFD4F5FEFFD4F5
          FEFFD2F4FDFFD1F4FDFFD0F3FDFFB8E1F2FF1D5897E0000000070000000B3B89
          C5FFC3F1FEFF92E2FEFF8EE0FEFF8BDEFEFF88DDFDFF85DAFCFF82D8FCFF7FD6
          FCFF7CD3FBFF7AD2FAFF77D1FAFFADE0FAFFA5D9F4FF9ED5F2FF7EBCE2FF4796
          CBFF4694CBFF4592CAFF4491C9FF418FC8FF418FC8FF3F8CC7FF3E8BC7FF3D8A
          C6FF3B88C5FF226AB1FF216AB0FF2064A6F415416DA4000000050000000B3C8C
          C7FFC7F3FFFF96E6FEFF94E4FFFF92E3FEFF90E1FEFF8DDFFEFF8BDDFDFF88DC
          FDFF86DAFCFF83D9FCFF80D6FCFFB3E2F6FFAA744EFF9D8C7EFFAEDBF2FF8FD3
          F7FF62C3F6FF5EBFF6FF5CBDF5FF5ABAF4FF58B8F4FF55B7F3FF53B5F3FF6EC4
          F5FF2C6CB1FF000000170000000D0000000800000006000000030000000A3D8F
          C9FFCCF5FFFF9BE8FFFF98E7FFFF96E6FFFF94E4FEFF92E3FEFF90E0FEFF8DE0
          FEFF8BDEFEFF89DDFDFF86DBFCFFB5E3F5FFAE734AFFCB9D6AFF9F8D7DFFB4DE
          F3FFA1DCF9FF77CFF9FF6DCAF9FFADDEF8FFA8D9F3FFA9DAF6FF7CCAF6FF7FCE
          F8FF357DBEFF0000000F00000005000000020000000100000001000000093E91
          CBFFCEF5FFFFA0EBFFFF9DEAFFFF9BE8FFFF99E7FFFF97E6FFFF94E4FFFF92E3
          FEFF90E1FEFF8EE0FEFF8CDFFEFFBBE6F8FFB18B6DFFE4C18FFFCB9D66FF9E80
          6AFFB1CEDAFFB4E1F5FF9DDAF8FFB3DDF2FFAC7046FF9DA3A3FFAEDAF0FFAEE0
          F8FF448BC6FF0000000C00000003000000000000000000000000000000084095
          CDFFD2F7FFFFA3EDFFFFA1ECFFFF9FEAFFFF9DE9FFFF9BE8FFFF99E7FFFF96E6
          FFFF94E5FFFFA1EAFEFFC0F6FEFFDDF8FBFFC9BCABFFD1A67BFFF5D498FFD7AE
          75FFA46D44FFAE917AFFC2BFB5FFC8D2D0FFB16F40FFAF7848FFA07B5FFFBECF
          D2FF91B5D2F30000000C00000003000000000000000000000000000000084297
          CEFFD5F8FFFFA7EEFFFFA5EEFFFFA3EDFFFFA1EBFFFFA0EBFFFF9DE9FFFF9BE9
          FFFFA5EDFEFF92D2ECFF3F93CBFF67A8D5FF9CC0D8FFB78B65FFF1DAAEFFF5D6
          99FFF2D295FFCFA671FFB48152FFA66D42FFB47749FFF1D094FFD3A972FF9E67
          3EFE857266CD0000001300000005000000010000000000000000000000074299
          CFFFD7F9FFFFA9F0FFFFA8EFFFFFA7EEFFFFA5EEFFFFA3EDFFFFA1ECFFFFA7ED
          FFFF8ED2ECFF2A6082A80000000B000000090000000C3A2A1C5DCFA67DFFFAE7
          BDFFF6DCA3FFF6DA9DFFF4D79DFFF4D79CFFF4D69AFFF3D59AFFF2D498FFEDCB
          92FFBD8B5AFF734526C4140C0732000000040000000000000000000000053D8D
          BDE8B2E3F3FFD5F9FEFFD4F9FEFFD4F9FEFFD4F9FEFFD3F9FEFFD3F9FEFFC5F0
          FAFF4893C1EA050B0F1A00000004000000020000000200000006543E2B79D2AC
          84FFF5E3C1FFFBEDC8FFFBF0CAFFFBF2CDFFFCF2CCFFFBF0C8FFF9EABEFFF6E0
          AEFFF7E2B7FFE8CDA8FFA9764BEC000000070000000000000000000000031127
          3445357CA6CB449DD2FF449DD2FF449DD2FF449DD1FF449DD1FF449CD1FF449C
          D1FF163345590000000400000001000000000000000000000001000000043E2F
          2259B18962E7DABD9CFFECDCC2FFF6EDD8FFFEFBE9FFFDF6D3FFFCF7D9FFF9F3
          DDFFD6B590FF8F6846C81B130D32000000030000000000000000000000010000
          0003000000050000000500000005000000050000000500000006000000060000
          0005000000030000000100000000000000000000000000000000000000000000
          0002070504104F3E2C6E8B6C4EBAAB845EE1B48157FFFEFBE5FFEBDBC2FFBF97
          6FF34B3928700000000900000003000000000000000000000000000000000000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000000000000000000000000000000000000000000000000
          00000000000100000002000000050000000BBB8B62FFD9BC9BFF886A4DB51510
          0C26000000050000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000706040EAF8863E94235275D000000060000
          0002000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000400000003000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00010000000100000003000000090000000D0000000D00000009000000050000
          00090000000E0000000E0000000900000005000000090000000E0000000E0000
          0009000000050000000A0000000F0000000F0000000A00000002000000000000
          0000000000000000000000000000000000000000000000000001000000010000
          0003000000050000000B155C41C11D7C59FF1D7A57FF14553DC0000000111357
          3EC01A7855FF1A7854FF12543CC10000001211523AC117724FFF17714EFF114F
          36C100000013114F36C1166D4BFF156B49FF0E4D33C100000009000000000000
          0000000000000000000000000000000000000000000000000001000000050000
          000D0000001500000016258966FF5BCFADFF49C79FFF238864FF000000182084
          61FF4FC9A3FF45C59CFF208460FF000000191F835FFF55CBA7FF43C39AFF1F83
          5FFF0000001A218661FF65D2B2FF45C59CFF208461FF0000000E000000000000
          00000000000000000000000000000000000000000001000000030000000D231C
          1A695A4742E22A221F74288C69FF65D3B2FF4ECAA5FF268A67FF000000182387
          63FF54CCA8FF49C7A0FF228663FF00000019208561FF5ACEAAFF45C49CFF2084
          60FF0000001A238763FF6CD6B7FF48C69EFF228663FF0000000E000000000000
          0000000000000000000000000000000000000000000100000006000000156450
          4AF0C6B8AEFF5B5754732B8F6CFF6CD6B8FF55CDAAFF288D6AFF00000017258A
          67FF5BD0ACFF4DC9A3FF258965FF00000018218662FF60D0AFFF47C69EFF2185
          62FF00000019268966FF74D9BCFF4CC8A2FF248865FF0000000D000000000000
          0000000000000000000000000000000000000000000100000007070505276E59
          53FFD5C9BEFF5C5856732D926FFFAAECDCFFA9ECDCFF2B8F6DFF00000015298C
          69FF64D3B2FF52CCA6FF278B68FF00000016238764FF65D3B3FF49C7A0FF2387
          63FF00000017288B69FF7ADCC1FF50CBA5FF278A67FF0000000C000000000000
          0000000000000000000000000000000000000000000200000009130F0E427C65
          5FFFD5C8BDFF6965627E357F64DD2F9371FF2F9370FF226C53C5000000112B8F
          6CFF6AD5B7FF55CEAAFF298E6BFF00000015258966FF6CD5B8FF4CC8A2FF2488
          65FF000000162A8E6BFF82DFC5FF53CDA8FF298D6AFF0000000C000000000000
          000000000000000000000000000000000001000000020000000A1F1A185C846E
          67FFD4C7BCFFBFB7B1DB6359528F312B29720D0A0A300000000C0000000E2D91
          6FFF70D9BCFF59D0AEFF2D906DFF00000013268B67FF73D8BCFF4ECAA5FF2689
          66FF000000142D906EFF8AE1C9FF57CEABFF2B8F6CFF0000000B000000000000
          000000000000000000000000000000000001000000030000000C2F2623778E77
          6FFFD3C5BBFFE0D7D0FFBBA89DFF7A6D69FF2D2522790000000E0000000B3093
          71FFACECDDFFACECDDFF2E9370FF00000012288C69FF79DBBFFF52CCA7FF288C
          68FF000000132F9270FF8FE4CEFF5BD1AEFF2D916EFF0000000A000000000000
          000000000000000000000000000000000001000000030000000E3C322F90957F
          76FFD2C4BAFFE0D7D0FFBEACA0FF7E716DFF3C312E940000000F00000009256E
          55C2319572FF319572FF246E54C20000000E298D6BFF7EDEC4FF54CDA9FF298D
          6AFF00000011309472FFC1F2E7FFC1F2E7FF2F9371FF00000009000000000000
          00000000000000000000000000000000000200000006000000134D403CAB9D87
          7CFFD1C3B9FFE0D7D0FFC1AEA3FF837570FF4A3E3AAD00000015000000080000
          0007000000080000000700000005000000092B8F6CFF84E0C7FF57CEABFF2B8F
          6CFF0000000D256E55C2329573FF319573FF246E54C200000006000000000000
          0000000000000000000000000002000000060000000F0B0909316C5A55E0A48D
          82FFD0C2B7FFE0D7D0FFC3B1A6FF847772FF695753E00B090835000000110000
          0007000000030000000100000000000000072C916EFF8BE2CBFF59D0ADFF2C90
          6DFF000000090000000500000007000000070000000500000001000000000000
          0000000000010000000300000008030302173E33318D79645DF8877169FF9D84
          79FF9A8376FF91776BFF8F7669FF806B60FF79645BFF6F5C55F93A302C8F0302
          021B0000000A0000000300000001000000062E926FFF90E4CFFF5BD1B0FF2D92
          6FFF000000070000000000000000000000000000000000000000000000000000
          0000000000020000000809080724665751D189736BFFB29E94FFAE988EFFA38C
          80FF9E8678FF9A8173FF8F7668FF80685BFF7E6358FF80685DFF755E56FF5E4D
          48D2080706270000000A00000003000000062F9371FFC1F2E7FFC1F2E7FF2E93
          70FF000000060000000000000000000000000000000000000000000000000000
          0002000000060807061F6F5F5AD99F8B80FFC4B3A9FFC5B6ACFFB6A398FFAF9A
          8DFFAA9487FFA48E7EFF987F71FF876B5FFF83685AFF8A7163FF8F7769FF7D65
          5AFF63524CDB070605240000000900000005246E55C1309572FF309472FF236E
          54C0000000040000000000000000000000000000000000000000000000010000
          00040000000C5B4E49AEA08B82FFC9BAB1FFD4C8C0FFD1C3BBFFC1AFA4FFBAA7
          9BFFB39F92FFAE978AFFA28B7CFF8E7465FF856B5CFF8F7667FF998274FF957B
          6EFF7D655BFF4F423EB100000011000000060000000400000004000000040000
          0003000000010000000000000000000000000000000000000000000000020000
          00072C26245B958279FFC9B9AFFFE6E3DAFFEAE8E1FFDCD1C9FFC8B8AEFFC3B1
          A6FFBCA79BFFB69F94FFAB9486FF977D6EFF836C5CFF8D7465FFA0887AFF9F88
          79FF937A6BFF7A635CFF26201E600000000A0000000300000001000000000000
          0000000000000000000000000000000000000000000000000000000000030000
          000C756761CEBDABA2FFDED7CFFFEFF0E8FFF0F1E9FFE6E2DAFFCFC1B7FFC9B8
          AEFFC2B0A3FFBAA79BFFAF998EFF9C8474FF876E5EFF8C7263FF9F887BFFA38D
          7FFF9B8172FF887063FF64534ED0000000110000000500000001000000000000
          000000000000000000000000000000000000000000000000000000000005221E
          1D459E8C84FFCFC2B8FFECEBE4FFF5FAF3FFF4F8F2FFECECE4FFD7CBC1FFCFBF
          B4FFC6B4A9FFBEABA0FFB49F94FFA0877AFF866E5FFF887061FF9A8475FFA68F
          83FF9F8677FF927968FF7F6962FF1D18174B0000000800000002000000000000
          0000000000000000000000000000000000000000000000000000000000074C43
          4084B2A098FFDBD3CAFFF2F6EFFFF6FBF4FFF6FBF4FFF0F2EAFFDFD7CEFFD4C4
          BCFFCBB9AEFFC1AEA4FFB7A498FFA28B7FFF886F61FF866C5EFF968072FFA992
          86FFA38A7DFF967B6EFF866D63FF463B37940000000B00000003000000000000
          0000000000000000000000000000000000000000000000000000000000087A6D
          68C5C1B2A9FFE5E2DAFFF6FAF3FFF7FBF4FFF8FAF3FFF7F7F0FFF6F4ECFFF2EE
          E7FFF9F7F0FFFAF8F1FFF9F7F0FFE8E2DAFFE3DCD4FFD2C8BFFFBCADA3FFB5A3
          96FFA48B80FF997F72FF8A7163FF655651CA0000000E00000003000000000000
          0000000000000000000000000000000000000000000000000000000000098F80
          7BDECABCB2FFF0F0E7FFF9F9F2FFE0DED6FFC2BEB4FF9F978CFF877C70FF8477
          6CFF635444FF6C5E4EFF756658FF928678FFACA297FFC2BBB1FFDAD7CEFFEDEB
          E3FFEDE8E0FFBAA89CFF8C7063FF7B6A63EA0000000F00000004000000000000
          000000000000000000000000000000000000000000000000000000000009A393
          8CF7E9E3DBFFD3CEC5FF786A5DFF685747FF6F5F51FF725F50FF776353FF7E6B
          5AFF8C7869FF98887AFFA29184FFAA9C90FFB3A699FFBCB1A7FFCDCAC0FFD1D2
          C6FFDADAD0FFEDEBE2FFD0C3B9FF86746DFA0000000E00000004000000000000
          000000000000000000000000000000000000000000000000000000000007A596
          90F5F4F2EBFF776355FF7F6B5AFF8C7968FF8F7D6DFF8D7968FF8C7767FF907A
          6DFF9D897AFFA79588FFB19F92FFBAAA9EFFC4B6ACFFCDBFB7FFDDD7CEFFE5E4
          DDFFE9EAE2FFE6E3DBFFF2EFE8FF897770FA0000000D00000003000000000000
          000000000000000000000000000000000000000000000000000000000005796E
          69B2D7CFC8FFDFD9CFFFA18D7EFF947E6FFF968173FF927F6FFF907B6AFF937F
          6EFF9E897AFFA89688FFB09E91FFB9A99BFFC3B4A8FFCCBFB4FFD9CFC6FFE7E3
          DCFFF2F1EAFFF6F4ECFFCFC5BEFF6B5F59B50000000900000002000000000000
          0000000000000000000000000000000000000000000000000000000000020D0C
          0B19796E6AAFCBC1BAFFEEEAE3FFF2EEE6FFE3DCD3FFCDC3B7FFB7A89BFFB7A9
          9CFF9F8B7BFFA59283FFAD9B8DFFC5B5AAFFD5CAC0FFE0D7CEFFEFE9E2FFF6F3
          ECFFECE7E0FFC1B5AEFF6D605CB20D0C0B200000000500000001000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          000300000006383331558F827ECCBEB1AAFFCFC5BDFFE1DBD4FFECE7E0FFEBE7
          E0FFF7F4EDFFFAF8F1FFF6F4EDFFEAE6DFFFEAE5DEFFDFD8D1FFC9BEB7FFB4A5
          9EFF847771CD332E2B580000000A000000050000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000100000002000000030000000611100F1F433D3B636D64609E8E827ECC9084
          7FCFA4968FEBB2A29BFFA2938DEB8D807AD08B7D78CD685F5A9F3F3A3666100F
          0E22000000090000000500000003000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000100000001000000020000000300000005000000060000
          0006000000070000000700000008000000070000000700000006000000050000
          0003000000020000000100000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000010000000200000004000000060000000600000006000000060000
          0007000000070000000700000007000000070000000700000007000000070000
          0007000000070000000700000007000000070000000700000004000000020000
          0001000000000000000000000000000000000000000000000000000000000000
          000000000002000000070000000E000000150000001800000019000000190000
          0019000000190000001A0000001A0000001A0000001A0000001B0000001B0000
          001B0000001B0000001C0000001C0000001B0000001900000011000000080000
          0002000000000000000000000000000000000000000000000000000000000000
          0001000000030000000E78564BC0A77868FFA77867FFA77666FFA67766FFA675
          65FFA67666FFA57565FFA47464FFA47464FFA47363FFA37363FFA37262FFA371
          61FFA27161FFA17061FFA17160FFA1705FFF9F705FFF725044C2000000110000
          0004000000010000000000000000000000000000000000000000000000000000
          00010000000500000013AA7A6CFFFCF9F7FFFCF8F6FFFBF8F6FFFBF8F5FFFBF7
          F5FFFBF7F4FFFAF7F3FFFAF6F3FFFAF6F3FFFAF6F2FFFAF5F2FFF9F4F1FFF9F4
          F0FFF9F3F0FFF8F3EFFFF8F3EFFFF8F2EEFFF8F2EEFFA17060FF000000180000
          0006000000010000000000000000000000000000000000000000000000000000
          00010000000500000014AB7D6DFFFCF9F8FFF7EDE9FFF6EDE8FFF6EDE6FFF6ED
          E6FFF6ECE6FFF6ECE5FFF6ECE5FFF6ECE4FFF4EBE4FFF4EBE4FFF4EBE4FFF4EA
          E3FFF4EAE3FFF4EAE3FFF4EAE3FFF3E9E3FFF8F2EEFFA37262FF0000001A0000
          0008000000020000000100000001000000000000000000000000000000000000
          00000000000500000014AB7F6FFFFDFAF8FFF7EFE9FFF7EFE9FFF7EFE8FFF6ED
          E8FFF6EDE8FFF6EDE6FFF6EDE6FFF4ECE6FFF6ECE6FFF4ECE5FFF4EBE5FFF4EB
          E4FFF4EBE4FFF4EBE4FFF4EBE4FFF4EAE3FFF9F3EFFFA37363FF0000001F0000
          000D000000080000000700000004000000020000000100000000000000000000
          00000000000500000013AD8071FFFDFBF9FFF7F0E9FFF6EFE9FFF7EFE9FFF7EF
          E8FFF7EFE9FFF6EDE8FFF7EDE8FFF7EDE6FFF6EDE6FFF6EDE6FFF6ECE5FFF4EC
          E5FFF6ECE5FFF4EBE5FFF4EBE4FFF4EBE4FFF9F4F0FFA57565FF010101310101
          01210101011C0101011901010111000000080000000200000000000000000000
          00000000000500000012AF8273FFFDFBFAFFF8F0EBFFF7F0EBFFF7F0EAFFF7F0
          EAFFF7F0E9FFF7EFE9FFF6EFE9FFF7EDE8FFF6EDE8FFF6EDE8FFF6EDE8FFF6EC
          E6FFF6EDE6FFF4ECE5FFF6EBE5FFF4EBE4FFF9F5F1FFA67667FFB89D94FFAD84
          74FFB08576FFB08676FF7F6055C2010101110000000400000001000000000000
          00000000000400000011B08375FFFDFCFAFFF8F1EBFFF7F1EBFFF7F1EBFFF7F0
          EBFFF8F0EAFFF7F0EAFFF7EFEAFFF7EFEAFFF7EDE9FFF6EFE8FFF6EFE8FFF6ED
          E8FFF7EDE8FFF6EDE6FFF4EDE6FFF6EDE6FFFAF5F2FFA87969FFE4E0DDFFF3EF
          ECFFF8F4F0FFFAF5F1FFB18677FF010101180000000600000001000000000000
          00000000000400000010B18677FFFDFCFAFFF8F2ECFFF8F1ECFFF8F1ECFFF7F1
          ECFFF7F0EBFFF8F1EAFFF8F0EAFFF7F0EAFFF7F0E9FFF7F0E9FFF6EFE9FFF6ED
          E9FFF7EDE9FFF7EFE9FFF6EDE8FFF7EDE8FFFBF6F4FFAA7A6AFFE2DAD5FFF1E9
          E3FFF4ECE7FFF9F5F2FFB38879FF010101190000000700000001000000000000
          0000000000040000000FB48878FFFDFCFBFFF8F2EDFFF8F2EDFFF8F2EDFFF8F1
          EDFFF8F1ECFFF8F1ECFFF8F1EBFFF8F0EBFFF8F0EAFFF7EFEAFFF7F0EAFFF7EF
          EAFFF7F0E9FFF7EFE9FFF6EDE9FFF7EDE8FFFBF7F4FFAA7D6DFFE2DBD6FFF1EA
          E4FFF5EDE7FFFAF5F3FFB3897AFF010101190000000600000001000000000000
          0000000000040000000FB5897AFFFEFDFCFFF9F3EFFFF8F2EFFFF8F2EDFFF8F2
          EDFFF8F2EDFFF8F1ECFFF8F1EDFFF7F1ECFFF8F1ECFFF7F0EBFFF7F0EAFFF7F0
          EBFFF7EFEAFFF7EFEAFFF7EFEAFFF7EFE9FFFBF8F5FFAC7F6FFFE3DCD8FFF1EA
          E4FFF5EEE8FFFAF5F4FFB58B7BFF010101180000000600000001000000000000
          0000000000030000000EB68D7EFFFEFDFCFFF9F3F1FFF9F3F0FFF8F3EFFFF8F2
          EFFFF9F2EFFFF8F3EFFFF8F2EFFFF8F1EDFFF8F1ECFFF8F1ECFFF8F1ECFFF8F1
          EBFFF8F1EBFFF7F0EBFFF7F0EBFFF7EFEAFFFCF8F6FFAD8071FFE4DED9FFF2EA
          E5FFF6EFE9FFFAF6F4FFB68C7DFF010101170000000600000001000000000000
          0000000000030000000DB88F80FFFEFDFDFFFAF4F1FFF9F4F1FFF9F3F1FFF9F3
          F0FFF9F3EFFFF9F3EFFFF9F3EFFFF8F2EFFFF9F2EFFFF8F2EFFFF8F2EDFFF8F1
          ECFFF8F1ECFFF7F1ECFFF7F0EBFFF7F0EBFFFAF6F3FFAE8373FFE6E0DAFFF2ED
          E7FFF7F1EBFFFBF7F5FFB88F7FFF010101160000000600000001000000000000
          0000000000030000000CB99081FFFEFEFDFFF9F6F2FFF9F4F1FFF9F4F1FFFAF4
          F0FFF9F4F0FFF9F4F0FFF9F3F0FFF9F3EFFFF9F3EFFFF9F2EFFFF9F3EDFFF9F2
          EDFFF8F1EDFFF8F1ECFFF8F1ECFFF6EFE9FFF8F4F2FFB08374FFE7E2DDFFF3ED
          E8FFF8F1ECFFFBF8F5FFB89080FF010101150000000500000001000000000000
          0000000000030000000BBC9283FFFEFEFDFFFAF6F2FFFAF7F2FFFAF6F2FFFAF6
          F2FFFAF4F1FFF9F4F1FFF9F4F0FFF9F3F1FFF9F4F0FFF9F3F0FFF9F3F0FFF9F3
          EFFFF9F3EFFFF8F2EFFFF6F0EAFFF5EDE7FFF6F1EEFFB38576FFE8E3DEFFF3ED
          E9FFF8F1ECFFFBF8F6FFB89183FF010101150000000500000001000000000000
          0000000000020000000ABD9384FFFEFEFEFFFAF7F3FFFAF7F3FFFAF7F3FFFAF6
          F3FFF9F6F2FFFAF6F2FFFAF6F2FFFAF6F1FFF9F4F1FFF8F3F0FFF9F4F1FFF8F3
          EFFFF8F2EEFFF6F0EBFFF4EDE8FFF2E9E5FFF3ECE9FFB38978FFE9E4E0FFF4EE
          EAFFF8F2EDFFFBF9F7FFBA9385FF010101140000000500000001000000000000
          00000000000200000009BE9686FFFFFEFEFFFAF7F6FFFAF7F4FFFAF7F3FFFAF7
          F3FFFAF7F3FFFAF6F3FFFAF6F2FFF9F6F3FFFAF6F2FFF8F2EFFFF6EFEBFFF5ED
          E9FFF3EAE6FFF0E5E2FFEEE2DDFFEBDED9FFECE1DDFFB5897AFFEAE6E2FFF4EF
          EBFFF8F2EEFFFCF9F7FFBB9487FF010101130000000500000000000000000000
          00000000000200000008BF9787FFFFFEFEFFFBF8F6FFFBF8F6FFFAF8F4FFFBF7
          F4FFFAF7F4FFFBF7F4FFFAF7F4FFF9F5F2FFF9F5F1FFF5EEE9FFEEE2DCFFE6D8
          D0FFE1D2CAFFE0CEC7FFDECAC2FFDBC7BEFFDCC8C2FFB78C7DFFEBE8E4FFF5F0
          ECFFF8F3EFFFFAF7F5FFBC9789FF010101120000000400000000000000000000
          00000000000200000008C09989FFFFFEFEFFFBF9F6FFFDF9F6FFFBF8F6FFFAF8
          F6FFFBF8F6FFFBF8F4FFFAF7F4FFFBF7F4FFF8F2EFFFEFE6DFFFB38B7CFFA577
          66FFA47564FFA47464FFA47363FFA37363FFCEB3AAFFB88F7EFFEFEAE7FFF6F1
          EDFFF7F2EDFFF9F5F4FFBE978AFF010101110000000400000000000000000000
          00000000000200000007C19A8BFFFFFFFEFFFDF9F7FFFBFAF7FFFBF9F7FFFBF9
          F7FFFBF8F6FFFBF8F6FFFBF8F6FFFAF8F4FFF7F2EFFFECDFDAFFAB7E6DFFFFFF
          FFFFFFFEFEFFFFFDFCFFFEFCFAFFFCF9F7FFCAAFA6FFC2A9A0FFF3EFECFFF5F1
          ECFFF6F0EBFFF7F3F0FFC0998CFF010101100000000400000000000000000000
          00000000000100000006C39B8CFFFFFFFFFFFDF9F8FFFDFAF8FFFDF9F7FFFDF9
          F8FFFBF9F7FFFBF9F7FFFBF9F6FFFBF8F6FFF6F1EDFFEBDFDBFFB08574FFFFFE
          FEFFFEFBFAFFFDF9F7FFFCF6F3FFCEB2A8FFC5ACA3FFF1EDE9FFF4F0EBFFF4EF
          EAFFF4EBE7FFF4EEEBFFC09D8EFF0101010F0000000400000000000000000000
          00000000000100000005C39D8EFFFFFFFFFFFDFBF9FFFDFAF9FFFDFAF8FFFDFA
          F9FFFDF9F8FFFBF9F7FFFBF9F8FFF9F6F4FFF6F0ECFFECE1DBFFB68C7DFFFFFE
          FEFFFDF9F6FFFBF6F3FFD1B5ACFFC8AFA5FFEEE8E5FFF2EAE4FFF1E7E3FFF0E5
          E1FFEDE1DDFFEDE2DFFFC29D90FF0101010E0000000400000000000000000000
          00000000000100000005C49E8FFFFFFFFFFFFDFBF9FFFDFBF9FFFEFAF9FFFDFA
          F9FFFDFAF8FFFDFAF8FFF9F7F6FFF9F4F2FFF5EDEBFFEBE1DDFFBC9584FFFFFE
          FEFFFBF6F3FFD4BAAFFFCBB2A7FFE9DEDAFFE5D8D2FFE2D4CCFFE2D1CBFFE1CE
          C7FFDECBC2FFDECCC5FFC49F91FF0101010D0000000300000000000000000000
          00000000000100000004C59F90FFFFFFFFFFFDFDFBFFFEFBFAFFFEFBFAFFFDFB
          F9FFFDFBF9FFFBF7F6FFF9F5F3FFF7F1EEFFF3EBE7FFEDE1DCFFC19B8BFFFFFE
          FEFFD6BCB2FFD1B8B0FFEBE3DDFFBA9689FFAF8476FFAF8474FFAF8374FFAF82
          73FFAE8273FFD1B8AFFFC49F91FF0000000A0000000300000000000000000000
          00000000000100000003C6A191FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFBF9F9FFF9F6F4FFF6F1F0FFF2ECE9FFEEE3E0FFE3D2CBFFDBC5
          BDFFD4BFB5FFF3EFECFFECDFDCFFB48B7CFFFFFFFFFFFFFEFEFFFFFDFCFFFEFC
          FAFFFCF9F7FFD3BAB2FF55403886000000060000000200000000000000000000
          0000000000000000000293776CBEC6A191FFC59F91FFC69F92FFC59F91FFC59F
          90FFC59F91FFC49F90FFC49E90FFC49D8FFFC49E8EFFC39D8EFFC39D8EFFD8C3
          BAFFF8F5F3FFF5F1EDFFECE0DCFFB99283FFFFFEFEFFFEFBFAFFFDF9F7FFFCF6
          F4FFD6BDB4FF57423B8600000008000000030000000100000000000000000000
          000000000000000000010000000100000002000000030000000400000008DAC6
          BDFFFCFCFCFFFAF9F7FFF9F7F6FFF9F7F5FFF9F7F6FFF9F6F5FFF8F6F4FFF9F7
          F6FFF9F6F4FFF6F2EEFFEEE3DEFFBF998BFFFFFEFEFFFDF9F6FFFBF6F4FFD8BF
          B8FF5A463F860000000800000003000000010000000000000000000000000000
          000000000000000000000000000000000000000000010000000200000006CDAE
          A1FFFEFEFEFFFCFBF9FFFCFBF9FFFDFAF9FFFCFAF9FFFCFAF8FFFCFAF8FFF9F7
          F6FFF9F5F4FFF5EFEDFFEDE3DFFFC4A191FFFFFEFEFFFBF6F4FFDBC4BAFF5D49
          4285000000070000000300000001000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000100000004CFB0
          A3FFFFFFFFFFFDFDFCFFFEFCFBFFFEFCFBFFFDFCFAFFFDFCFAFFFBF8F7FFF9F6
          F5FFF7F3F0FFF4EDE9FFEEE3DFFFC9A698FFFFFEFEFFDDC6BDFF5F4C45840000
          0006000000020000000100000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000100000003D0B1
          A4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF9F9FFF9F6
          F5FFF6F2F1FFF3EDEAFFEFE4E1FFE5D4CFFFE1CEC7FF604F4883000000050000
          0002000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000029A83
          7ABED0B1A4FFCFB0A4FFD0B0A4FFCFB0A4FFCFB0A3FFCFB0A4FFCEB0A3FFCEAF
          A3FFCEAEA2FFCEAFA1FFCDAEA1FFCDAEA1FF63524A8200000004000000020000
          0001000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000001000000010000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000001000000010000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000100000002000000030000000400000005000000050000
          0003000000020000000100000000000000000000000000000000000000000000
          0000000000000000000100000002000000030000000400000005000000050000
          0003000000020000000100000000000000000000000000000000000000000000
          00000000000100000003000000070000000D0000001200000014000000130000
          000F000000080000000300000001000000000000000000000000000000000000
          00000000000100000003000000070000000D0000001200000014000000130000
          000F000000080000000300000001000000000000000000000000000000000000
          000000000002000000080F0802303A1D088A5F2E0BCE77390FF9602D0BCF3C1C
          078D100702330000000B00000003000000010000000000000000000000000000
          000000000002000000080F0702303A1C088A5E2C0CCE76380FF95F2D0BCF3B1B
          078D0F0702330000000B00000003000000010000000000000000000000000000
          000100000005110A042D5C3917B0B47B42FFDD984BFFF1B773FFDEA56AFFB274
          3CFF53270AB21007023200000008000000020000000000000000000000000000
          0001000000051009032D583415B0AB6C35FFD08B49FFE39B55FFCF8847FFAA68
          31FF52260BB21007023200000008000000020000000000000000000000000000
          000200000008482C1486CE9C6AFFECB16AFFDE9C56FFD28E4EFFDD9C55FFEBA7
          5AFFB2723DFF411F098C0000000D000000030000000100000000000000000000
          00020000000842271186BC8756FFDE9B57FFCE9053FFC0834BFFCD8F52FFDD99
          55FFAA6C3CFF3F1E0A8B0000000D000000030000000100000000000000000000
          00020000000A81542CCBE8C18FFFD29353FF6945229D170F08396B45239FD293
          52FFDB9D5AFF713C17CE00000011000000040000000100000000000000000000
          00020000000A734927CBD7985AFFC1854DFF5E3F229D150E08395F40239FC286
          4CFFD08C4FFF6B3616CE00000010000000040000000100000000000000000000
          000200000009B27A45F9FAE3C2FFAB6830FF150D063500000015150D0638AF6A
          30FFF7CB93FF995826F900000013000000060000000200000001000000010000
          0003000000099E6A3EF9EFBB85FF9B5E30FF120C063500000015130C06389E61
          32FFEDB880FF8D4E24F900000010000000040000000100000000000000000000
          000200000007966B42CAEED6BAFFC18A59FF623C1B9A150D0634623C1C9CC487
          4FFFEAC9A3FF845228D20000001A0000000D0000000800000008000000060000
          00060000000B865E3ACEE5C19BFFB77643FF55351C9A130C063456361D9CB877
          44FFDFB790FF774725CC0000000D000000040000000000000000000000000000
          00010000000462482F81DDBA95FFEBD5BEFFC08A5FFFA45B2BFFC18757FFEDD1
          B1FFE2BE9DFF764B2FCF0000002E000000270000002B0000002D0000002A0000
          002300000023745038CEE2BF9AFFE4BE9BFFB87B4CFF9A572AFFB87C4CFFE4BE
          99FFC49974FF4F321D8500000009000000020000000000000000000000000000
          0000000000021A140D25856543AADFBD99FFF0DDC9FFFCF4EAFFF2E1CFFFECD1
          B3FFEBD2B8FFB9A093FF875849FF875648FF865446FF845244FF815042FF6D41
          35FF6C3F34FFA48271FFE2C29FFFE3C29EFFEBD4BAFFFCEEDAFFEAD2B8FFCDA7
          84FF714D2FAC150E082A00000004000000010000000000000000000000000000
          000000000001000000021B140E24654D3580A27B53C9CA9767F9A0784FCE8B65
          48CACBB1A4FFFAF9F9FFE9D7C9FFEAD8CCFFEAD8CCFFEAD8CCFFE7D8D3FF8E67
          5CFFB59D93FFCBB6ABFFA88677FF77553CCA926E4ACCB68459F88E6744CA583F
          298217100A280000000500000001000000000000000000000000000000000000
          00000000000000000001000000020000000300000005000000070000000D0000
          001950383093CFB7AFFFFFFEFEFFEBD9D0FFBE9F92FF95685BFFBD9E92FFC6B2
          ACFF97766BFF95756AFF38211B98000000130000000600000005000000050000
          0005000000030000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000002000000030000
          000800000018593F36AAD1B8B1FFFFFEFEFFB18D81FFE1D0C8FFB08D81FFEEDE
          D6FF936E63FF3E251EAF00000019000000050000000200000001000000010000
          0001000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0003000000125F4035D0B89D92FFDECAC3FFE1D3CEFFC5A9A0FFD9C5BCFFEFE2
          D8FFDCCEC9FF58352BCF00000017000000070000000200000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000532221D6BAA8D82FFE0D9D3FFC7B1A8FFDECBC3FFFFFFFEFFF2E4DEFFF2E6
          DEFFF2E4DCFFA68981FF2919156E0000000D0000000400000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020B08
          061F8C675BEBE6DBD4FFE9E2DCFFE1D9D4FFCEBBB3FFD8C2BBFFFFFFFFFFF5E8
          E0FFF3E9E2FFEEE2DEFF775045EE080504260000000800000002000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000075A41
          38A0CCB5ABFFF0E8E2FFEDE6E0FFF6F4F3FFB8A49DFF704F44CBD2BBB2FFFFFF
          FFFFF5EDE6FFF5EDE6FFC8B5AFFF462B24A20000000E00000005000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000032018153FAF8B
          7FFDF0E8E1FFF1E8E3FFFCFAF8FFC6B1A9FF4530288D00000016523B338BD2BB
          B2FFFFFFFFFFF8F0EAFFF7EFEAFF947065FD180F0C4500000009000000020000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000010202010B87675AD5E3D5
          CEFFF1E9E5FFFCFAF9FFCAB6AEFF4B352D8A00000009000000050000000C533C
          3489D2BBB3FFFFFFFFFFF9F3EEFFE5DBD7FF674136D501010112000000050000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000347373072C9ADA4FFF2EB
          E6FFFCFAF9FFCFBAB3FF4E383289000000070000000100000001000000040000
          000B533C3488D3BCB3FFFFFFFFFFF9F5F3FFB49990FF33211B75000000090000
          0003000000010000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000001130F0D22AE8D80F1EFE6DFFFFCFB
          F9FFD3BEB7FF513D368700000006000000010000000000000000000000010000
          000400000009533D3487D3BCB4FFFFFFFFFFF4EEEBFF855F52F10D0807280000
          0005000000010000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000004775E54AADECCC3FFFDFBFAFFD7C2
          BBFF55413A860000000600000001000000000000000000000000000000000000
          00010000000300000008543D3686D3BDB4FFFFFFFFFFD3C3BDFF52362CAC0000
          0009000000030000000100000000000000000000000000000000000000000000
          00000000000000000000000000012D232042C9AA9EFFFDFBFAFFDAC6BFFF5944
          3E85000000050000000100000000000000000000000000000000000000000000
          0000000000010000000300000007543E3585D4BDB5FFFFFFFFFF9F7F73FF1E13
          1046000000050000000100000000000000000000000000000000000000000000
          000000000000000000000504030AA7887CDEF4EDEAFFDCC9C2FF5B4841840000
          0004000000010000000000000000000000000000000000000000000000000000
          000000000000000000010000000200000006543E3684D4BDB5FFEEE8E6FF7350
          42DE0302020E0000000300000001000000000000000000000000000000000000
          000000000000000000015C4B457BDECAC1FFDFCCC5FF5D4B4483000000030000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000010000000200000005553E3683D5BEB6FFBFA8
          A0FF3F2A227D0000000500000001000000000000000000000000000000000000
          0000000000001A151324C5A79DF5E1CFC8FF604E478200000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000200000004553F3782D5BE
          B6FF926E63F5130C0A2900000002000000010000000000000000000000000000
          0000000000018D746AB5E3D1CAFF625049810000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000035540
          3881D5BFB7FF604137B600000003000000010000000000000000000000000000
          000000000001CDAC9EFF63514B80000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          000256403880AD8071FF00000002000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000010000000100000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010101010302020206040202090403020A0403030B0403030B0403
          030B0403030B0503030C0503030C0503030C0503030C0503030C0503030C0503
          030D0403030C0503030B03020208010101030000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000010000020402020A0A0505180D0808230F090928100A0A2A100A0A2B100A
          0A2B110B0A2C110B0A2D110B0A2D110B0A2E110B0A2E120B0B2F120B0B30120B
          0B30120B0B30100A0A2B0C07071E0503030D0101010300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000002020206080605177B584CC6A47564FFA37464FFA37463FFA37363FFA272
          62FFA27261FFA2705FFFA26F5FFFA06F5FFFA06E5DFF9F6E5DFF9F6D5CFF9E6D
          5CFF9E6D5CFF9C6B5BFF775145CA0B07071E0302020801000001000000000000
          0000000000000000000000000001000000010000000100000001000000010100
          0002030202090C080721A67767FFFEFDFCFFFCFAF8FFFCFAF8FFFCFAF7FFFCF9
          F7FFFCF9F7FFFCF8F6FFFBF8F6FFFBF7F5FFFBF7F5FFFBF6F4FFFBF7F3FFFAF6
          F3FFFBF5F2FFF9F5F1FF9F6D5CFF100A0A2A0402020A01000001000000000000
          0000000000010000000100000002000000040000000500000005000000060000
          00070302020E0D080827A77A69FFFEFDFCFFF7F1EBFFF7F0EBFFF8F0EAFFF7F0
          EAFFF7EFEAFFF7EFEAFFF7EDE9FFF6EFE8FFF6EFE8FFF6EDE8FFF7EDE8FFF6ED
          E6FFF4EDE6FFFBF6F3FFA06F5EFF110B0A2D0503030B01000001000000000000
          000000000001000000040000000A000000100000001400000015000000150100
          00160302021D0C080734A97A6AFFFEFDFCFFF8F1ECFFF7F1ECFFF7F0EBFFF8F1
          EAFFF8F0EAFFF7F0EAFFF7F0E9FFF7F0E9FFF6EFE9FFF6EDE9FFF7EDE9FFF7EF
          E9FFF6EDE8FFFBF7F4FFA27061FF110B0A2D0403030B01000001000000000000
          000000000002000000090E1F32592B62A1EA2E6AB1FF2E6AB0FF2D69AFFF2D67
          ADFF2E66ABFF6E8CB6FFAC7D6EFFFEFDFDFFF8F2EDFFF8F1EDFFF8F1ECFFF8F1
          ECFFF8F1EBFFF8F0EBFFF8F0EAFFF7EFEAFFF7F0EAFFF7EFEAFFF7F0E9FFF7EF
          E9FFF6EDE9FFFBF7F5FFA37363FF100A0A2B0403030B01000001000000000000
          0000000000040000000F2D65A1E869AFDEFF66BAEBFF65B9ECFF62B8EBFF61B7
          EAFF60B3E6FF8CBCDBFFAD7F70FFFEFDFDFFF8F2EDFFF8F2EDFFF8F2EDFFF8F1
          ECFFF8F1EDFFF7F1ECFFF8F1ECFFF7F0EBFFF7F0EAFFF7F0EBFFF7EFEAFFF7EF
          EAFFF7EFEAFFFCF8F5FFA47564FF100A092A0403030B01000001000000000000
          000000000004000000113573B6FF82CAF1FF58B2EAFF51AEE8FF4FADE8FF4DAB
          E6FF4EA8E2FF82B6DAFFAF8172FFFEFEFDFFF8F3EFFFF8F2EFFFF9F2EFFFBB91
          7FFFBB907EFFBA8E7DFFB98E7CFFB78C7AFFB78A7AFFB68978FFF8F1EBFFF7F0
          EBFFF7F0EBFFFCF8F6FFA57667FF0F0A09280403030A01000001000000000000
          000000000004000000123777B8FF88CCF1FF54B1E9FF53B0E9FF52AFE8FF51AE
          E8FF51AAE3FF84B8DCFFB08474FFFEFEFEFFF9F3F1FFF9F3F0FFF9F3EFFFF4EB
          E6FFF4EBE6FFF3EAE5FFF4EAE5FFF2E9E5FFF2E9E3FFF2E8E2FFF8F1ECFFF7F1
          ECFFF7F0EBFFFCF9F7FFA77968FF0F0909270403030A01000001000000000000
          000000000004000000113A7BBAFF8ED0F3FF58B3EAFF56B2E9FF55B1EAFF54B1
          E8FF53ACE4FF86BBDDFFB18576FFFEFEFEFFF9F4F1FFFAF4F0FFF9F4F0FFBF96
          85FFBE9684FFBD9483FFBC9281FFBB9180FFBB8F7EFFBA8E7DFFF8F1EDFFF8F1
          ECFFF8F1ECFFFCFAF8FFA87B6AFF0E0908250402020901000001000000000000
          000000000004000000103C7EBDFF93D3F3FF5BB6EAFF59B5EAFF58B4EAFF58B2
          E8FF56AFE5FF88BEDFFFB48778FFFEFEFEFFFAF6F2FFFAF6F2FFFAF4F1FFF5ED
          E8FFF4ECE7FFF4EBE8FFF4ECE7FFF4EBE6FFF4EAE6FFF3EAE5FFF9F3EFFFF8F2
          EFFFF8F2EDFFFCFAF8FFA97C6DFF0E0908240402020901000001000000000000
          0000000000040000000F3E82BFFF98D5F3FF5EB8ECFF5DB8ECFF5BB6EBFF5BB4
          E9FF59B3E7FF8ABFE0FFB58979FFFFFEFEFFFAF7F3FFFAF6F3FFF9F6F2FFC39C
          8DFFC19B8AFFC19989FFBF9886FFBE9685FFBE9484FFBD9382FFF9F3EFFFF9F3
          EFFFF8F3EFFFFDFBF9FFAD7E6FFF0D0808220402020901000001000000000000
          0000000000040000000F4285C1FF9DD9F5FF62BBECFF62BAECFF5FB9ECFF5EB8
          ECFF5DB5E8FF8CC1E2FFB78A7BFFFFFEFEFFFAF7F3FFFAF7F3FFFAF7F3FFF6EF
          EBFFF6EFEAFFF5EFEAFFF5EEE9FFF4ECE9FFF4ECE8FFF4ECE7FFFAF4F1FFF9F3
          F0FFF9F3F0FFFDFBFAFFAE8272FF0C0807210302020801000001000000000000
          0000000000030000000E4388C3FFA3DCF5FF66BEEDFF65BDEDFF64BBEDFF62BB
          ECFF61B8E9FF8FC4E3FFB78D7CFFFFFEFEFFFAF8F4FFFBF7F4FFFAF7F4FFC7A4
          93FFC6A291FFC6A090FFC39E8EFFC29C8DFFC29A8AFFC19988FFF9F6F1FFF9F4
          F1FFF9F4F1FFFDFCFAFFAF8374FF0C07071F0302020801000001000000000000
          0000000000030000000D468BC5FFA8DEF6FF6AC1EEFF69C0EEFF68BFEEFF66BD
          EDFF65BBEAFF91C7E6FFB98F7EFFFFFFFEFFFBF8F6FFFAF8F6FFFBF8F6FFF7F1
          ECFFF6F0ECFFF7F0ECFFF6F0ECFFF6F0EAFFF5EFEAFFF5EFEAFFFAF6F2FFF9F4
          F2FFFAF4F1FFFDFCFBFFB18575FF0B07061D0302020801000001000000000000
          0000000000030000000C488FC7FFADE1F7FF6EC3EFFF6CC2EEFF6BC2EEFF6AC0
          EEFF68BDEAFF94C9E7FFBA9081FFFFFFFFFFFBF9F7FFFBF9F7FFFBF8F6FFCBA9
          9AFFCAA798FFC9A696FFC9A594FFC7A392FFC7A292FFC5A08FFFFAF7F3FFFAF7
          F2FFFAF6F2FFFEFDFBFFB28677FF0B07061C0302020701000001000000000000
          0000000000030000000C4A92C9FFB1E3F7FF72C6F0FF70C5EFFF6FC4EFFF6DC3
          EEFF6CC0EBFF96CBE7FFBB9282FFFFFFFFFFFDF9F7FFFDF9F8FFFBF9F7FFFBF9
          F7FFFBF9F6FFFBF8F6FFFBF8F6FFFBF8F6FFFBF8F4FFFBF7F4FFFAF8F4FFFAF7
          F4FFFAF6F3FFFEFDFCFFB48879FF0A06061A0302020701000001000000000000
          0000000000030000000B4D95CAFFB7E6F8FF76C9F0FF74C8F1FF74C7F0FF72C6
          EFFF70C3EDFF9ACFEAFFBD9383FFFFFFFFFFFDFAF8FFFDFAF9FFFDF9F8FFFBF9
          F7FFFBF9F8FFFBF9F7FFFBF9F7FFFBF9F6FFFBF9F6FFFAF8F6FFFBF8F4FFFBF7
          F4FFFBF8F4FFFEFDFCFFB6897AFF090606190202020601000001000000000000
          0000000000030000000A4F98CCFFBBE8F9FF7ACCF2FF78CBF2FF77CAF1FF75CA
          F0FF74C6EEFF9DD1EBFFBD9384FFFFFFFFFFFEFAF9FFFDFAF9FFFDFAF8FFFDFA
          F8FFFBFAF9FFFDFAF8FFFDF9F8FFFBF9F7FFFBF9F7FFFBF9F7FFFDF9F6FFFBF8
          F6FFFBF8F4FFFEFDFDFFB78D7CFF090605180202020601000001000000000000
          0000000000020000000A529ACEFFBFE9F9FF7ECFF2FF7CCEF2FF7BCDF2FF79CD
          F1FF78CAEFFF9FD5EDFFBE9585FFFFFFFFFFFEFBFAFFFDFBF9FFFDFBF9FFFDFA
          F9FFFDFBF9FFFDFAF8FFFDFAF8FFFDFAF8FFFDFAF8FFFBF9F8FFFDF9F7FFFBF8
          F7FFFBF8F6FFFEFEFDFFB98F7DFF080505150201010501000001000000000000
          00000000000200000009539DD0FFC4EDFAFF83D2F3FF81D1F3FF7FCFF4FF7DCF
          F3FF7CCDF0FFA3D7EFFFBF9687FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFEFFFFFE
          FEFFFFFEFEFFFFFEFEFFBA907FFF070404120201010401000001000000000000
          0000000000020000000856A0D1FFC7EEFBFF86D5F4FF85D4F4FF84D2F4FF82D2
          F4FF80D0F3FFA6DAF3FFB8A6A1FFC09688FFBF9687FFBF9686FFBF9687FFBE96
          86FFBE9586FFBE9485FFBE9584FFBD9484FFBD9484FFBD9384FFBD9384FFBD92
          83FFBD9282FFBD9282FF8D6D62C40403030C0101010300000000000000000000
          0000000000020000000858A2D3FFCBEFFBFF8AD8F6FF89D8F5FF88D6F5FF86D5
          F4FF84D3F3FFAADFF5FFA2D5EBFF9DCBE4FF9CCBE3FF9DCAE2FF9BCBE3FF9BCC
          E3FF9BCBE4FF9ACCE5FF9BCDE6FF9BCEE8FF9CCEE8FFC1E0EEFF82ACCFFF0604
          0419060404130603030F0402020B020101050000000100000000000000000000
          000000000002000000075AA4D4FFCFF1FCFF8EDBF6FF8CDBF6FF8BD8F4FF8AD5
          F3FF87D2F0FF79BFDDFF6CADCDFF6BABCBFF6BA9CAFF6AAACBFF6BACCCFF6BAC
          CEFF6CB0D2FF6EB5D6FF70B8DBFF72BEE3FF72C0E7FFADDCF1FF4D90C5FF0201
          010F010101060101010301010102000000010000000000000000000000000000
          000000000002000000065BA7D5FFD1F3FCFF92DDF7FF90DCF6FF8ED8F2FF9AD7
          ECFF8EBCD0FF7EA0B5FF7A98ACFF7D95A7FF7C94A4FF7C93A4FF7B96A8FF7F9D
          AEFF7DA2B7FF76A8C2FF6CADCBFF70B5D6FF73BFE2FFAFDDEFFF4F94C9FF0000
          0009000000020000000000000000000000000000000000000000000000000000
          000000000001000000065DA9D7FFD5F4FCFF96E1F8FF9AE0F5FFB5DDECFF99B4
          C1FF7D7D83FF7E6963FF806458FF845C4FFF84594BFF7F584BFF7A5A50FF765E
          59FF777274FF8394A1FF87A5B5FF74A9C4FF73B6D4FFB3DCEEFF5398CAFF0000
          0008000000020000000000000000000000000000000000000000000000000000
          0000000000010000000560ACD8FFD8F6FDFFACEAFAFFBEE6F3FF9AACB5FF8F71
          68FFAF8D7DFFC3A898FFCCB3A2FFD4BDACFFD7C0AFFFCFB6A5FFC3A794FFB597
          85FF9B7666FF7D5D51FF7F888EFF8FA9B8FF85BBD1FFB8DEECFF559ACBFF0000
          0007000000020000000000000000000000000000000000000000000000000000
          00000000000100000003589CC4E8BBE4F3FFD8F6FDFFDFEDF0FF9D7C6FFFE6DA
          D3FFF4EFEAFFF4EDE7FFF2EBE4FFE9DDD3FFDDC9BAFFDBC5B6FFE3D1C4FFE8DA
          D0FFE5D6CAFFD1BCAFFF825D51FFB2BCBFFFB4D1DBFFA2CEE2FF4F8EB9E80000
          0005000000010000000000000000000000000000000000000000000000000000
          00000000000000000002182B35424E88AACA62ADD8FFA9CDE3FFAC8F83FFB087
          78FFAF8877FFAD8576FFAD8475FFEDE1D9FFE4D5C8FFE0D0C2FF9E7564FF9E75
          62FF9D7462FF9B7361FF997669FF9CB7CAFF5899C5FF477DA1CD162833450000
          0003000000010000000000000000000000000000000000000000000000000000
          00000000000000000001000000020000000300000003000000060000000B0000
          000E0000000E0000001281645AC2E2D1C9FFF3EDE7FFDCCBC0FF795B4FC90000
          0017000000130000001300000011000000080000000500000004000000020000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000001000000010000
          00010000000100000002130F0D227E6358B7AF897CFA75584DBA120D0C230000
          0002000000010000000100000001000000010000000100000001000000010000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000010000
          0002000000050000000700000008000000080000000800000009000000090000
          000900000009000000090000000A0000000A0000000A0000000A0000000A0000
          000B0000000B0000000B0000000B0000000B0000000B0000000C0000000C0000
          000C0000000C0000000C0000000B000000090000000400000001000000020000
          000800000012000000180000001B0000001C0000001D0000001D0000001E0000
          001F0000001F0000002000000021000000220000002300000023000000240000
          00250000002500000026000000270000002800000028000000290000002A0000
          002B0000002B0000002B000000290000001F0000000E00000003000000030000
          000E000000320000003300000034000000350000003600000037000000380000
          00390000003A0000003B0000003C0000003D0000003E0000003F000000400000
          0041000000420000004300000044000000450000004600000047000000480000
          00490000004A0000004B0000004C0000004D0000001C00000007000000030000
          000E000000230000002400000025000000260000002700000028000000280000
          00290000002A0000002B0000002C0000002D0000002E0000002F000000300000
          0031000000310000003200000033000000340000003500000036000000370000
          0038000000390000003A0000003B0000003C0000002000000008000000020000
          000900000016000000170000001700000018000000190000001A0000001A0000
          001B0000001C0000001D0000001E0000001E0000001F00000020000000210000
          0022000000230000002300000024000000250000002600000027000000280000
          0028000000290000002A0000002B0000002C0000001C00000007000000010000
          00040000000B0000000B0000000D0000000D0000000E0000000F0000000F0000
          0010000000110000001100000011000000120000001200000013000000140000
          001400000015000000170000001800000018000000190000001A0000001B0000
          001B0000001C0000001D0000001E0000001F0000001100000005000000000000
          000200000005000000080000000A0000000B0000000C0000000D0000000E0000
          000E0000000E0000000E0000000C0000000A000000090000000A0000000C0000
          000E000000110000001300000015000000170000001700000018000000190000
          00190000001B0000001B00000017000000100000000700000002000000000000
          0000000000000000000000000000000000000000000000000001000000030000
          0006000000070000000500000003000000010000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000001000000030000000B0000
          00160000001A000000140000000D000000070000000400000002000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000362822FF372822FF362721FF362821FF362821FF00000015452E
          26FF241510DA100A0788020101340000001B0000001000000008000000030000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000372922FF00000000000000000000000100000006000000184E38
          2ED7A07C6FFF544649FF2A2D47F716254FB3070C1A57000000180000000B0000
          0004000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000382923FF0000000000000000000000010000000400000012281D
          187F82706CFFADCAE6FF7492BFFF425D99FF2E478AFF101E3F9A0000001C0000
          000B000000030000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000392A23FF000000000000000000000000000000020000000B0504
          032B555974F6DBE3EFFFA2C2E2FF7896C2FF536DA2FF315194FF071A439A0000
          001B0000000B0000000300000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000003B2C24FF00000000000000000000000000000001000000060000
          0015293B63AA8B9FC7FFCED9E9FF2084DBFF51A9E2FF1552AAFF154B9EFF081C
          44990000001A0000000B00000003000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000003C2C25FF00000000000000000000000000000001000000030000
          000C0B14224D4B6CABFFFFFFFFFF4DA3E5FF2389DFFF56ADE3FF1658ACFF174F
          A0FF081D4598000000190000000A000000030000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000003E2D25FF00000000000000000000000000000000000000010000
          000500000012213254926990C2FFC2E5F8FF50A7E6FF2791E0FF5DB1E5FF1A5D
          AFFF1953A4FF091F4797000000180000000A0000000300000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000003E2E26FF00000000000000000000000000000000000000010000
          0002000000070000001417385A924590D0FFC7E8F8FF56AEE7FF2C97E1FF62B5
          E6FF1B62B2FF1B57A6FF0A204995000000160000000900000003000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000403027FF00000000000000000000000000000000000000000000
          0001000000020000000700000013173A5C904895D2FFCBEBFAFF5AB4EBFF319D
          E4FF67BAE7FF1E65B6FF1D5DA9FF0B234A940000001500000009000000020000
          0001000000000000000000000000000000000000000000000000000000000000
          000000000000413028FF00000000000000000000000000000000000000000000
          000000000001000000020000000700000012183B5D8F4C9AD4FFCFEEFBFF60B9
          EDFF36A5E9FF6EBEE8FF206BB9FF2061ADFF0C254D9300000013000000080000
          0002000000010000000000000000000000000000000000000000000000000000
          000000000000433229FF00000000000000000000000000000000000000000000
          00000000000000000000000000020000000600000010193D5E8E4F9FD7FFD3F0
          FCFF64BEEFFF3BABEBFF72C2EAFF2371BCFF2266AFFF0D284E92000000120000
          0008000000020000000100000000000000000000000000000000000000000000
          00000000000043332AFF00000000000000000000000000000000000000000000
          000000000000000000000000000000000002000000060000000F1A3F5F8C52A3
          D8FFD6F2FCFF6AC5F0FF42B2EBFF77C6ECFF2677C0FF256BB4FF0E2950900000
          0011000000070000000200000001000000000000000000000000000000000000
          00000000000046342BFF00000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000002000000050000000D1B41
          608B56A8DAFFD9F4FDFF89D5F6FF47BBEFFF7BCBECFF2A7DC1FF2871B7FF191C
          278F0000000F0000000600000002000000000000000000000000000000000000
          00000000000046352CFF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000040000
          000C1C43618A5AADDCFFDCF6FDFF74CFF6FF4CC0F0FF80D0EEFF78777BFF794A
          3AFF2A110B8E0000000E00000006000000020000000000000000000000000000
          00000000000048362CFF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          00040000000B1C4462885DB0DEFFDEF8FEFF7AD6F7FF90B9C5FFD8C1B3FF996B
          59FF815141FF190D228C0000000C000000050000000100000000000000000000
          00000000000049372CFF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000100000003000000091D45638760B4E0FFE7F7F9FFDBC3B1FFCFB39FFFDBC5
          B8FF584B8BFF212095FF0A0A3E8B0000000B0000000400000001000000000000
          0000000000004A382EFF4A372DFF4A382DFF4A382DFF49372DFF48372DFF4936
          2DFF48372DFF48362CFF48362CFF47362CFF47352CFF46352CFF47352CFF4735
          2BFF46352BFF45342BFF0000000831384386B58F7EFFFAF0E2FFDFC8B6FF9A97
          C5FF8791E3FF2B2EAAFF25259CFF0F0F45890000000600000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000100000002000000074A2F2885BC9788FFF6EEE7FF8B9F
          EDFF6275E1FF98A2E7FF7B85D0FF272A96FF0000000700000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000010000000200000006372C44846B78D4FFDBE7
          FCFF8BA0EEFFACC0F3FF4E57C3FF21235A840000000400000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000200000004262A60827180
          DAFFDCE8FCFF6675D7FF262A6083000000050000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000032B31
          66815761CEFF2C31668100000004000000020000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0002000000020000000200000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00000000000000
          0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
          0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F8F8F800F8F8F800F8F8F800F8F8
          F800F8F8F800F8F8F800F8F8F800F8F8F800F8F8F800F8F8F800F8F8F800F8F8
          F800F8F8F800F8F8F800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
          0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
          000000000000FFFFF800F8F8F800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
          0000000000000000000000000000FFFFFF00FFFFFF00A8581000A8601000A868
          1000A8701000A8781800A8782000A8782000A8782000A8782000A8802800A880
          3000A8803800B0884000C0A87800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
          000000000000000000000000000000000000E0783800E8804000E8804000E880
          4000F0A84000F0B84800F0B85800F0B85800F0B85800F0C05800E8C87800F0C8
          8800F0D09800D8C08000A8884000A08048000000000000000000FFFFFF000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B0601800E8804000FF904800FF984800FFA8
          5000FFC05800FFC86000FFD06800FFD07800FFD07800FFD88800FFE09800FFE0
          A800FFE8B800F0D09800C0986800A8884000C0A8780000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B0601800E8804000FF984800FFB04800FFC0
          5800FFC86000FFD06800FFD06800FFD07800FFD88000FFD89000FFE0A800FFE8
          B000FFE8B800FFE0A800F0D08800D8B86800B088400000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B0681800E8804000FFA85000FFC05800FFC8
          5800FFD06800FFD06800FFD07800FFD07800FFD88800FFE09800FFE0A800FFE8
          B800F8E8C000F8E8C000FFE8B000E8D080008890300058600000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B0781800F0A84000FFC05800FFC86000FFD0
          6800FFD06800FFD07800FFD88000FFD88800FFD89000FFE09800FFE0A800F8E8
          C000F0E8D800F0E8D800FFE8C000D8D8900060A8380028800000586000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000A8802000F0B84800FFC86000FFD06800FFD0
          6800FFD07800FFD88000FFD88800FFD89000FFD89800FFE0A000F8E8B800F8E8
          D000F0F0E800F0F0E800FFE8C800D0D8980050B8480008981000308008005868
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B0802800F0C05800FFD06800FFD06800FFD0
          7800FFD88000FFD88800FFD89000FFE09800FFE0A000F8E8B800F0E8D000F0F0
          E800F0F0E800F8F0E800FFE8C800D0D8980058C8600000B02800109810002888
          0000586800000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000B0882800F0C05800FFD07800FFD07800FFD0
          7800FFD88800FFD89000FFD89800FFE0A000F8E8B800F8E8D000F0F0E800F0F0
          E800F8F8F800F8F8F000FFE8C800E0E8A00068D8680010C8380000B028000898
          1000388008005868000000000000000000000000000000000000000000000000
          0000000000000000000000000000B0882800F0C05800FFD87800FFD88000FFD8
          8800FFD89000FFD89800FFE0A000F8E8B800F0E8D000F0F0E800F0F0E800F8F8
          F800F8F8F800F8F8F000FFE8C800E0E8A00080F0680030E8400010C8300000B0
          2800109810002888000058680000000000000000000000000000000000000000
          000000000000000000000000000000000000D8A84800E8C87800FFD89800FFE0
          A000FFE09800FFE0A000FFE8B000F8E8C000F0E8D800F0E8E000F8F0E800FFF0
          E800FFF0E800FFF0E800F8E8D000E8E8A80090FF780048F8500020E0480010C8
          380000B028000898100040780800000000000000000000000000000000000000
          000000000000000000000000000000000000A0782000C0A06800E0E0C000F8E8
          D000FFE8B000FFE8B000FFE8B000FFE8B800FFE8C000FFE8C800FFE8C800FFE8
          C000FFE8B800FFE8B800FFF0E800F0E8D80098FF900058FF600040F8480030E8
          400010C8380008A8280040781000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000098804800C8C0A000E0E0
          C000F8E8B800F8E8B800E8E8A800E8E8A800E8E8A800E0E8A000E0E8A000E0E8
          A000D0D88800D0D88800E0E0C000E0E0C00098F8980078FF780058FF600048F8
          500020E0480018C0380048881800000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000098784000C0A8
          7800D0D89800E0E8A000A8F89000A8F89000A0F8880090F8780088F8700070E8
          600058D0580050C0480050B8480058C0580088E8880098FF980080FF800058FF
          600040F8500038E0400058902000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000008880
          3000A0C88000A8E8980098F8980088FF900078FF780068FF680058FF580040F0
          480020D8400008C0300000A8100018A8200060D0680098F0900088F8880070FF
          780058FF600050F0480068A02000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00008880300098B06000A8E8980098FF980088FF880078FF780060FF680050F8
          580038F0480020D8380008B8280000B0280028B0300050C8480080E8800098FF
          980078FF800060F0580070A02800000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000088803000A0C88000A8E8980098F8980088FF900078FF780068FF
          680058FF580040F8480020D8400008C0300000A8180018A8200060D0680098F0
          900088F8900078F0680078A03000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009088380098B06000A8E8980098FF980088FF900078FF
          800068FF680058FF600038F8500020D8400008C0300000B0280028B8380048C8
          500078F0800098F0900088A04800000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000088803000A0C87800A8E8980098E8880088E8
          780078E8680068E8580058E8480040E0400020C0380010A830001098100020A0
          180088A0480088A0480000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000090984800889840008098
          3800789830007898280070982000689820005888180048781800487000005070
          0800788828000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00F8C30F07C0000000800000008000000080000001C0000003E0000007F800
          000FFC00037FF80001FFF80001FFF80000FFF800007FF800003FF800001FF800
          000FF8000007FC000007FC000007FE000007FF000007FF800007FFC00007FFE0
          0007FFF00007FFF8000FFFFE001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000800000008000
          0000800000008000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000800000008000
          0000800000008000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008000000080000000000000000000
          0000000000000000000080000000800000000000000000000000000000000000
          0000800000008000000080000000800000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008000000080000000000000000000
          0000000000000000000080000000800000000000000000000000000000000000
          0000800000008000000080000000800000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008000000080000000000000000000
          0000000000000000000080000000800000000000000000000000800000008000
          0000000000000000000000000000000000008000000080000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008000000080000000000000000000
          0000000000000000000080000000800000000000000000000000800000008000
          0000000000000000000000000000000000008000000080000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008000000080000000000000000000
          0000000000000000000080000000800000000000000000000000800000008000
          0000000000000000000000000000000000008000000080000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008000000080000000000000000000
          0000000000000000000080000000800000000000000000000000800000008000
          0000000000000000000000000000000000008000000080000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000800000008000
          0000800000008000000080000000800000000000000000000000800000008000
          0000000000000000000000000000000000008000000080000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000800000008000
          0000800000008000000080000000800000000000000000000000800000008000
          0000000000000000000000000000000000008000000080000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000080000000800000000000000000000000800000008000
          0000800000008000000080000000800000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000080000000800000000000000000000000800000008000
          0000800000008000000080000000800000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000080000000800000000000000000000000800000008000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000080000000800000000000000000000000800000008000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFFF0FFFFFFF0FFFFFFCF3C3FFFCF3C3FFFCF33CFFFCF3
          3CFFFCF33CFFFCF33CFFFF033CFFFF033CFFFFF303FFFFF303FFFFF03FFFFFF0
          3FFFFFFCFFFFFFFCFFFFFFF03FFFFFF03FFFFFF33FFFFFF33FFFFFC30FFFFFC3
          0FFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFCFCFFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000008080000080
          8000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000008080000080
          8000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000000000000000000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000000000000000000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000000000000000000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000000000000000000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000000000000000000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000000000000000000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000000000000000000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000000000000000000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000008080000080800000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000000000000000000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000000000000000000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000000000000000000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000C0C0C000C0C0C00000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000000000000000000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
          0000C0C0C000C0C0C00000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFF0000003F0000003C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003C0000003C0000003C000
          0003C0000003C0000003C0000003C0000003C0000003C0000003FFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000FFFF0000FFFF0000000000000000000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000FFFF0000FFFF0000000000000000000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000FFFFFF00FFFFFF0000FFFF0000FFFF000000000000000000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000000000000000000000000000000000000000000000000000000000000000
          0000FFFFFF00FFFFFF0000FFFF0000FFFF000000000000000000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000000000000000000000000000000000000000000000000000000000000000
          000000FFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FFFF00000000000000
          0000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000000000000000000000000000000000000000000000
          000000FFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FFFF00000000000000
          0000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000000000000000000000000000000000000000000000
          0000FFFFFF00FFFFFF0000FFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000FFFFFF00FFFFFF0000FFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000FFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FFFF00FFFFFF00FFFF
          FF0000FFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FFFF00FFFFFF00FFFF
          FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000FFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FFFF00FFFFFF00FFFF
          FF0000FFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FFFF00FFFFFF00FFFF
          FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000FFFFFF00FFFFFF0000FFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FF
          FF00FFFFFF00FFFFFF0000FFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FF
          FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000FFFFFF00FFFFFF0000FFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FF
          FF00FFFFFF00FFFFFF0000FFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FF
          FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000FFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FFFF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000FFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FFFF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000003FF000003FF000000FF0000
          00FF0000003F0000003F0000000F0000000F0000000300000003000000000000
          0000000003FF000003FF000003FF000003FF000003FF000003FFC0FFFF03C0FF
          FF03FFFFFFC3FFFFFFC3FFFF3F33FFFF3F33FFFFC0FFFFFFC0FFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000D9930000D9930000D9930000D9930000D9930000D993
          0000D9930000D9930000D9930000D9930000D9930000D9930000D9930000D993
          0000D9930000D9930000D9930000D99300000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000D9930000D9930000D9930000D9930000D9930000D993
          0000D9930000D9930000D9930000D9930000D9930000D9930000D9930000D993
          0000D9930000D9930000D9930000D99300000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000BA7E0000BA7E0000FFB62400FFB62400FFB62400FFB6
          2400FFB62400FFB62400FFB62400FFB62400FFB62400FFB62400FFB62400FFB6
          2400FFB62400FFB62400BA7E0000BA7E00000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000BA7E0000BA7E0000FFB62400FFB62400FFB62400FFB6
          2400FFB62400FFB62400FFB62400FFB62400FFB62400FFB62400FFB62400FFB6
          2400FFB62400FFB62400BA7E0000BA7E00000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000BA7E0000BA7E0000FFC24800FFC248007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          0000FFC24800FFC24800BA7E0000BA7E00000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000BA7E0000BA7E0000FFC24800FFC248007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          0000FFC24800FFC24800BA7E0000BA7E00000000000000000000D9930000D993
          0000D9930000D9930000D9930000D9930000D9930000D9930000D9930000D993
          0000D9930000D9930000BA7E0000BA7E0000FFCE6C00FFCE6C00FFCE6C00FFCE
          6C00FFCE6C00FFCE6C00FFCE6C00FFCE6C00FFCE6C00FFCE6C00FFCE6C00FFCE
          6C00FFCE6C00FFCE6C00BA7E0000BA7E00000000000000000000D9930000D993
          0000D9930000D9930000D9930000D9930000D9930000D9930000D9930000D993
          0000D9930000D9930000BA7E0000BA7E0000FFCE6C00FFCE6C00FFCE6C00FFCE
          6C00FFCE6C00FFCE6C00FFCE6C00FFCE6C00FFCE6C00FFCE6C00FFCE6C00FFCE
          6C00FFCE6C00FFCE6C00BA7E0000BA7E00000000000000000000BA7E0000BA7E
          0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00BA7E0000BA7E0000FFCE6C00FFCE6C007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          0000FFCE6C00FFCE6C00BA7E0000BA7E00000000000000000000BA7E0000BA7E
          0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00BA7E0000BA7E0000FFCE6C00FFCE6C007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          0000FFCE6C00FFCE6C00BA7E0000BA7E00000000000000000000BA7E0000BA7E
          0000FFFFFF00FFFFFF007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C540000BA7E0000BA7E0000FFDA9000FFDA9000FFDA9000FFDA
          9000FFDA9000FFDA9000FFDA9000FFDA9000FFDA9000FFDA9000FFDA9000FFDA
          9000FFDA9000FFDA9000BA7E0000BA7E00000000000000000000BA7E0000BA7E
          0000FFFFFF00FFFFFF007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C540000BA7E0000BA7E0000FFDA9000FFDA9000FFDA9000FFDA
          9000FFDA9000FFDA9000FFDA9000FFDA9000FFDA9000FFDA9000FFDA9000FFDA
          9000FFDA9000FFDA9000BA7E0000BA7E000000000000000000009B6900009B69
          0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF009B6900009B690000FFDA9000FFDA90009B6900009B69
          00009B6900009B690000FFDA9000FFDA90009B6900009B6900009B6900009B69
          00009B6900009B6900009B6900009B69000000000000000000009B6900009B69
          0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF009B6900009B690000FFDA9000FFDA90009B6900009B69
          00009B6900009B690000FFDA9000FFDA90009B6900009B6900009B6900009B69
          00009B6900009B6900009B6900009B69000000000000000000009B6900009B69
          0000FFFFFF00FFFFFF007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C5400009B6900009B690000EBEBEB00EBEBEB00EBEBEB00EBEB
          EB00EBEBEB00EBEBEB00EBEBEB00EBEBEB009B6900009B690000FFFFFF00FFFF
          FF009B6900009B690000000000000000000000000000000000009B6900009B69
          0000FFFFFF00FFFFFF007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C5400009B6900009B690000EBEBEB00EBEBEB00EBEBEB00EBEB
          EB00EBEBEB00EBEBEB00EBEBEB00EBEBEB009B6900009B690000FFFFFF00FFFF
          FF009B6900009B690000000000000000000000000000000000009B6900009B69
          0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF009B6900009B690000EBEBEB00EBEBEB00EBEBEB00EBEB
          EB00EBEBEB00EBEBEB00EBEBEB00EBEBEB009B6900009B6900009B6900009B69
          00000000000000000000000000000000000000000000000000009B6900009B69
          0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF009B6900009B690000EBEBEB00EBEBEB00EBEBEB00EBEB
          EB00EBEBEB00EBEBEB00EBEBEB00EBEBEB009B6900009B6900009B6900009B69
          00000000000000000000000000000000000000000000000000007C5400007C54
          0000FFFFFF00FFFFFF007C5400007C5400007C5400007C540000FFFFFF00FFFF
          FF007C5400007C5400009B6900009B6900009B6900009B6900009B6900009B69
          00009B6900009B6900009B6900009B6900009B6900009B690000000000000000
          00000000000000000000000000000000000000000000000000007C5400007C54
          0000FFFFFF00FFFFFF007C5400007C5400007C5400007C540000FFFFFF00FFFF
          FF007C5400007C5400009B6900009B6900009B6900009B6900009B6900009B69
          00009B6900009B6900009B6900009B6900009B6900009B690000000000000000
          00000000000000000000000000000000000000000000000000007C5400007C54
          0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF007C5400007C540000FFFFFF00FFFFFF007C5400007C540000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007C5400007C54
          0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF007C5400007C540000FFFFFF00FFFFFF007C5400007C540000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007C5400007C54
          0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF007C5400007C5400007C5400007C5400000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007C5400007C54
          0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF007C5400007C5400007C5400007C5400000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007C5400007C54
          00007C5400007C5400007C5400007C5400007C5400007C5400007C5400007C54
          00007C5400007C54000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00003FFF00003FFF00003FFF0
          0003FFF00003FFF0000300000003000000030000000300000003000000030000
          000300000003000000030000000F0000000F0000003F0000003F000000FF0000
          00FF0000FFFF0000FFFF0003FFFF0003FFFF000FFFFF000FFFFFFFFFFFFFFFFF
          FFFF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010000000100000001000000040000000600000006000000040000
          0002000000010000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          00020000000300000005000000080000000E00000016000000160000000F0000
          0008000000050000000400000002000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000030000
          00080000000F000000110000001628201D7070594FFF6F584FFF201816710000
          001800000014000000110000000A000000040000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000002000000070404
          031C594842C63E302AB30E0B0A4B53423CC4B7A69DFFAD998EFF46352FC50E0B
          0A4F372925B040302ACC040302240000000A0000000200000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000020000000B604F
          48C7CABCB5FFAB9990FF745E56FF7A635AFECAB8ADFFC7B4A8FF6B544BFE755F
          56FE9F8B80FFAA968AFF41312BC9000000100000000400000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000030000000C5344
          3EABC2B4ADFFE2D8D2FFD0C1B7FFC2B1A5FFCEBDB2FFCCBAB1FFBEAAA0FFC9B6
          AAFFCAB5A9FFA28E83FF3C2F29B1000000120000000500000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000001000000040000000E120F
          0D3B927F77FCE9E1DDFFD7C9C1FFD5C7BEFFE1D6D0FFE6DDD6FFDFD3CBFFD0C0
          B5FFCBB8AEFF7A6359FD0E0B0A47000000140000000700000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000002000000072D24216C5242
          3BBB8B756BFED8CDC7FFE1D7D0FFCFC2BBFF998179FF9F887EFFD2C5BEFFE1D6
          CFFFC3B1A6FF755E55FF4F3E38C2281F1C720000000C00000003000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000020000000A957D72FFD9D0
          CCFFEBE4E1FFE7E0DCFFE7DDD9FF897167FF251D1B64261E1C65987F77FFE8E0
          DBFFD2C2B9FFCFC0B4FFBBA79CFF836C62FF0000001000000004000000000000
          0000000000000000000000000000000000010000000100000001000000010000
          0001000000000000000100000001000000010000000300000009998276FFDBD2
          CEFFF6F3F1FFF9F6F5FFEBE5E1FF7E625BFF1F17155C2018165D8A7068FFEDE6
          E2FFDACAC3FFD2C4BCFFC9BBB5FF877066FF0000000F00000004000000000000
          0000000000000000000000000001000000020000000400000005000000040000
          0002000000010000000200000004000000060000000500000007352E2A606A5B
          52B59A8379FEECE7E4FFEDE6E3FFCEC1BCFF8B736CFF8C736DFFD1C6C0FFE7DE
          D9FFD4C8C1FF8E776CFD60514AB92E2623620201010C00000002000000000000
          0000000000000000000100000003000000080000000F0504031E020202160000
          000800000005000000090101011305040320000000110000000B0000000B1713
          1232AB978EFDF4F0EEFFF0EAE7FFEEE8E4FFF0EAE7FFECE5E1FFEBE3DFFFEBE5
          E1FFE7DEDAFF8A756BFD130F0E3C0000000D0000000400000001000000000000
          0000000000000000000200000007100C0B373B2D289F675248F342332DB20000
          0018000000110000001841322CB2644E46F4382B25A10F0B0A3A0000000D695A
          53AADDD5D1FFF5F2F0FFF4F1EFFFEBE5E2FFFCFAFAFFF0E9E7FFE0D7D1FFF9F7
          F6FFF7F4F2FFC8BCB5FF54453DAF000000090000000200000000000000000000
          000000000001000000030000000B392C2795B1A39CFFD9CFC7FFA18E84FF3529
          24960000002535292599A5958DFFCABBB0FFA3928AFF35292497000000117868
          5FBDE5DFDCFFDED4D1FFAF9C92FDA28B80FDFAF8F8FFF2EDEBFF9E887EFDA997
          8DFBD9CFCBFFE1DAD5FF73625AC1000000070000000200000000000000000000
          000100000002000000050000000E352A258CAE9F99FFE1D7D0FFD6C9BFFF8D78
          6FFF6B5349FF8D786FFFD3C4BBFFD0C2B6FFA29188FF32262290000000130605
          05147B6B62C06A5B54A61814132E72625AB2E2DAD6FFE1DAD6FF705F58B31512
          112D645750A47A6A61C606050412000000030000000100000000000000020000
          0005000000090000000D000000163329248AA19189FFDCD1C9FFD1C1B6FFD9CD
          C2FFDCD1C7FFD7C9BFFFCDBFB2FFD5C7BDFF96837AFF332723910000001B0000
          0013000000100000000B000000073B332F5FA89186FFA89085FF3B332F5F0000
          0005000000050000000400000003000000010000000000000000000000050F0C
          0B2D372D28842F252176362B26897A665DF4D2C6BEFFD6C8BFFFD2C5B9FFD2C4
          B8FFD1C3B7FFD1C2B7FFD1C2B6FFD2C3B8FFCBBDB4FF7A6259F834282493382C
          279940312CA6100D0B3800000009000000040000000300000003000000020000
          0001000000010000000100000000000000000000000000000000000000094336
          3197B4A59FFFA7968EFFA6958DFFD5C8C4FFDCCFC8FFD7C8BEFFD5C7BDFFD5C7
          BDFFD3C6BBFFD3C5BBFFD2C5BBFFD2C4B9FFD3C6BBFFCFC3B9FF9C8A80FFA898
          8EFFAF9F95FF40322CA40000000E000000040000000200000001000000000000
          0000000000000000000000000000000000000000000000000000040403137964
          5AF0F0EDEAFFEEE8E5FFE9E1DCFFE2D7D1FFE0D3CCFFDDCFC7FFDCD0C7FFE5DD
          D7FFF0EBE8FFF1EBE7FFE6DBD6FFD8CAC0FFD5C6BCFFD5C8BEFFDBD0C8FFD9CD
          C3FFE1DAD4FF6F5951F50504041B000000040000000100000000000000000000
          00000000000000000000000000000000000000000000000000000302020E4F41
          3AA7BEB1ACFFF9F7F5FFECE5E0FFE2D8D0FFE1D8D0FFE1D6D0FFDED6D0FFAD9D
          96FF948078FFB09F99FFE6DFDBFFE5DCD7FFD7C8BFFFD6C8BEFFD6C7BEFFEBE3
          DFFFAA9B95FF41332E9F0101010F000000030000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000040000
          000B4236308CAA9A92FFF6F3F1FFE6DDD6FFE5DAD3FFE4DAD3FFA3928CFF3C32
          2F830000001B40363389A4938DFFECE4E0FFDACBC3FFD8CBC2FFE3D9D4FF9987
          7FFF3227237F0000001100000006000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          000700000012806A60F9F3F0EFFFE8E0DAFFE7DFD8FFE6DDD8FF7E6A64FC0303
          021D0000001306050524836E66FFE9E1DCFFDBCEC6FFDBCDC6FFECE5E1FF7861
          57FF0000001A0000000B00000003000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000030000
          000940352F82AA9992FFF3EEEAFFE9E3DDFFE9E2DDFFE9E1DCFF907D77FF382D
          2B86000000173C312E8C917F7AFFE0D5CDFFDDD2C9FFDDD1C8FFE9E2DCFF9A86
          7EFF332A267D0000000F00000005000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000020101085647
          41A1C0B4AEFFF6F3F1FFEDE6E1FFECE5E1FFECE4DFFFEBE4DFFFDAD1CBFF8C79
          74FF735D57FF8C7872FFD4CBC4FFE0D6CDFFE0D5CDFFDFD3CCFFDFD3CCFFEBE4
          DDFFAE9E97FF4437319400000009000000020000000000000000000000000000
          00000000000000000000000000000000000000000000000000000504040F8A75
          6CF1F6F4F4FFFAF8F7FFFAF9F7FFF5F2EEFFEFE9E5FFEEE7E3FFEDE7E3FFECE7
          E2FFECE6E0FFECE5DFFFECE5DFFFE7DED8FFE2D8D1FFE3DCD5FFF1EDEAFFF2EC
          E9FFF0EDEAFF826E64F507060519000000030000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000045245
          3F94CBBFBAFFBCAEA7FFBBAEA7FFEEEAE8FFF9F6F4FFF1EBE8FFEFE9E5FFEFE8
          E5FFEFE8E4FFEEE8E4FFEEE8E4FFEEE7E3FFEBE4E1FFEDE9E7FFB6A8A1FFCCC2
          BEFFCCC2BDFF4E403AA100000008000000020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000021310
          0F254E433C8B3B322D6D4539347F948076F5E7E2DFFFF7F3F1FFF1EBE8FFF1EB
          E8FFF0EBE7FFF0EAE7FFF0EAE6FFF1EBE8FFEFEAE9FF98867DFB4438338A5143
          3DA151433C9F1612103300000004000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0002000000030000000400000007493E3885C3B6B0FFFAF7F6FFF3EDEBFFFBF9
          F9FFFEFEFDFFFDFBFAFFF6F3F0FFF7F4F2FFBFB3ACFF4A3F38910000000C0000
          0008000000060000000400000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000001000000010000000453474091D0C5C0FFF9F8F5FFFAFAF8FFB09F
          97FF937C71FFAF9E96FFFCFAFAFFFAF7F5FFCCC1BCFF4F423D93000000070000
          0003000000010000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000001000000025A4C4698D4CBC6FFFAFAF9FFC3B5AFFF4036
          3172000000093F353172C1B4ADFFFAF9F9FFD3CAC6FF584A429D000000040000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000011D19173261514BA19C887DF75A4C46990000
          00040000000300000005584B4499998479F75D4F48A21C181535000000020000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000001000000020B090915020201050000
          00010000000100000001020201060B0908160000000300000002000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end>
  end
  object SaveDlg: TSaveDialog
    DefaultExt = '.xflt'
    Filter = #1060#1072#1081#1083#1099' '#1092#1080#1083#1100#1090#1088#1086#1074'|*.xflt'
    Left = 164
    Top = 108
  end
  object OpenDlg: TOpenDialog
    DefaultExt = '.xflt'
    Filter = #1060#1072#1081#1083#1099' '#1092#1080#1083#1100#1090#1088#1086#1074'|*.xflt'
    Left = 138
    Top = 183
  end
end
