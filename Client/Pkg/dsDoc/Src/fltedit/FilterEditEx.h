/*
  ����        - FilterEdit.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� �������� �����
                ��� �������������� �������, � ���������� -
                ���� "condrules"
                ( ������������ ����������� TICSDocSpec )
  ����������� - ������� �.�.
  ����        - 23.09.2004
*/
//---------------------------------------------------------------------------
#ifndef FilterEditExH
#define FilterEditExH
//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>
#include <Vcl.ImgList.hpp>
#include "cxClasses.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxSplitter.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"
#include "dxStatusBar.hpp"
//---------------------------------------------------------------------------
#include "ICSNomenklator.h"
#include "ICSFilter.h"
#include "dsDocNomSupportTypeDef.h"
//---------------------------------------------------------------------------
class TFilterEditExForm;
typedef bool __fastcall (__closure *TOnCondCanCloseEvent)(TFilterEditExForm *Sender, UnicodeString ACondName);
typedef bool __fastcall (__closure *TOnFltCustomLoadEvent)(UnicodeString ACondName, TTagNode *Flt);
typedef bool __fastcall (__closure *TBeforeDeleteEvent)( TObject* Sender, TTagNode* RootNode );
//---------------------------------------------------------------------------
enum TFilterEditMsgType
{
  femtCaption,
  femtRootNameF,
  femtRootNameC,
  femtRootNameT,
  femtNewFileName,
  femtCheckOk,
  femtCheckNone,
  femtCheckEmpty,
  femtSelectCondName
};
//---------------------------------------------------------------------------
typedef UnicodeString __fastcall (__closure *TOnGetMessageTextEvent)(TFilterEditMsgType AType, UnicodeString ADefMsg);
//---------------------------------------------------------------------------

enum TFilterEditType{fetNone, fetFilter, fetCondition, fetTemplate, fetFilteredTemplate, fetKonkr};
enum TInfGroupAddType{atNone,atAdd,atInsBefore,atInsAfter};
enum TCheckFilterResult
{
  fchOk = 0,     // Ok
  fchBad = 1,    // ������� ������ �������� ������ ��� �������������������� ������
  fchEmpty = 2,  // � xml-���� "condrules" ��� �������� ���������
  fchNone = 3    // ������ �� ������
};
//---------------------------------------------------------------------------
class PACKAGE EFilterEditError : public System::Sysutils::Exception
{
//        typedef Exception inherited;
public:
     __fastcall EFilterEditError(const UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE TFilterEditExForm : public TForm
{
__published:	// IDE-managed Components
        TActionList *MainAL;
        TcxTreeList *FRulesTV;
        TcxTreeListColumn *CondNodesCol;
        TdxStatusBar *StatusSB;
        TcxSplitter *TreeSpl;
        TAction *actPropertues;
        TAction *actSave;
        TAction *actAddORGroup;
        TAction *actAddANDGroup;
        TAction *actSwapGroupType;
        TAction *actSetNOTPref;
        TAction *actAddCondition;
        TAction *actDelete;
        TAction *actSetValue;
        TAction *actResetValue;
        TAction *actEditComment;
        TAction *actCheck;
 TcxStyleRepository *Style;
        TcxStyle *cxStyle1;
        TcxStyle *cxStyle2;
        TcxStyle *cxStyle3;
        TdxBarManager *MainBM;
        TdxBarButton *dxBarButton1;
        TdxBarButton *dxBarButton2;
        TdxBarButton *dxBarButton3;
        TdxBarButton *dxBarButton4;
        TdxBarButton *dxBarButton5;
        TdxBarButton *dxBarButton6;
        TdxBarButton *dxBarButton7;
        TdxBarButton *dxBarButton9;
        TdxBarButton *dxBarButton10;
        TdxBarButton *dxBarButton11;
        TAction *actLoad;
        TAction *actApply;
        TdxBarButton *dxBarButton12;
        TdxBarButton *dxBarButton13;
 TdxBarPopupMenu *EditPM;
        TdxBarButton *dxBarButton14;
        TdxBarButton *dxBarButton8;
        TcxTreeList *FNomTV;
        TcxTreeListColumn *cxTreeListColumn1;
        TdxBarPopupMenu *dxBarPopupMenu2;
        TdxStatusBarContainerControl *StatusSBContainer7;
        TImage *ModifyOffIm;
        TImage *ModifyOnIm;
        TcxStyle *cxStyle4;
        TcxStyle *cxStyle5;
        TcxStyle *bStyle;
        TAction *actInsertCondBefore;
        TAction *actInsertCondAfter;
        TAction *actInsertANDBefore;
        TAction *actInsertANDAfter;
        TAction *actInsertORBefore;
        TAction *actInsertORAfter;
        TdxBarButton *dxBarButton15;
        TdxBarButton *dxBarButton16;
        TdxBarButton *dxBarButton18;
        TdxBarButton *dxBarButton19;
        TdxBarButton *dxBarButton20;
        TdxBarButton *dxBarButton21;
        TAction *actCopy;
        TAction *actCut;
        TAction *actPaste;
        TdxBarButton *dxBarButton22;
        TdxBarButton *dxBarButton23;
        TdxBarButton *dxBarButton24;
        TcxImageList *MainIL;
        TcxImageList *StateIL;
        TdxBarButton *dxBarButton17;
        TcxTreeListColumn *InclToNameCol;
        TcxImageList *MainLIL;
        TAction *actClose;
        TAction *actChangeMode;
        TAction *actLoadFromLib;
        TdxBarButton *dxBarButton31;
        TdxBarButton *dxBarButton32;
        TSaveDialog *SaveDlg;
        TOpenDialog *OpenDlg;
        TcxStyle *grStyle;
        void __fastcall FAddRulesNode(TTagNode *ANomData,TDTDTagNode *ARuleData,UnicodeString ANodeName, bool ACanCopy);
        void __fastcall FInsertRulesNode(TTagNode *ANomData, TDTDTagNode *ARuleData, UnicodeString ANodeName, bool ACanCopy, bool ACanBefore);
        void __fastcall actPropertuesExecute(TObject *Sender);
        void __fastcall actSaveExecute(TObject *Sender);
        void __fastcall actAddORGroupExecute(TObject *Sender);
        void __fastcall actAddANDGroupExecute(TObject *Sender);
        void __fastcall actSwapGroupTypeExecute(TObject *Sender);
        void __fastcall actSetNOTPrefExecute(TObject *Sender);
        void __fastcall actAddConditionExecute(TObject *Sender);
        void __fastcall actDeleteExecute(TObject *Sender);
        void __fastcall actSetValueExecute(TObject *Sender);
        void __fastcall actResetValueExecute(TObject *Sender);
        void __fastcall actEditCommentExecute(TObject *Sender);
        void __fastcall actCheckExecute(TObject *Sender);
        void __fastcall actLoadExecute(TObject *Sender);
        void __fastcall actApplyExecute(TObject *Sender);
        void __fastcall FRulesTVDragDrop(TObject *Sender, TObject *Source,
          int X, int Y);
        void __fastcall FRulesTVDragOver(TObject *Sender, TObject *Source,
          int X, int Y, TDragState State, bool &Accept);
        void __fastcall actInsertANDBeforeExecute(TObject *Sender);
        void __fastcall actInsertANDAfterExecute(TObject *Sender);
        void __fastcall actInsertORBeforeExecute(TObject *Sender);
        void __fastcall actInsertORAfterExecute(TObject *Sender);
        void __fastcall actInsertCondBeforeExecute(TObject *Sender);
        void __fastcall actInsertCondAfterExecute(TObject *Sender);
        void __fastcall actCopyExecute(TObject *Sender);
        void __fastcall actCutExecute(TObject *Sender);
        void __fastcall actPasteExecute(TObject *Sender);
        void __fastcall InclToNameColGetEditProperties(
          TcxTreeListColumn *Sender, TcxTreeListNode *ANode,
          TcxCustomEditProperties *&EditProperties);
        void __fastcall FRulesTVDblClick(TObject *Sender);
        void __fastcall actCloseExecute(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall actLoadFromLibExecute(TObject *Sender);
        void __fastcall actChangeModeExecute(TObject *Sender);
        void __fastcall FNomTVDblClick(TObject *Sender);
        void __fastcall FNomTVMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall FRulesTVMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall FRulesTVStartDrag(TObject *Sender,
          TDragObject *&DragObject);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall FormActivate(TObject *Sender);
  void __fastcall FRulesTVEditValueChanged(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn);
  void __fastcall FRulesTVFocusedNodeChanged(TcxCustomTreeList *Sender, TcxTreeListNode *APrevFocusedNode,
          TcxTreeListNode *AFocusedNode);
  void __fastcall FRulesTVMoveTo(TcxCustomTreeList *Sender, TcxTreeListNode *AttachNode,
          TcxTreeListNodeAttachMode AttachMode, TList *Nodes, bool &IsCopy,
          bool &Done);
  void __fastcall FRulesTVStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
  void __fastcall FRulesTVEditing(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          bool &Allow);



private:	// User declarations
        /*��������� ����*/
        TdsNomCallDefProcEvent FOnCallDefProc;
        TTreeNode*   __treeNode;
        TTagNode*   FParentXML;
        /*����*/
        TOnCondCanCloseEvent FCanClose;
        TOnFltCustomLoadEvent FOnCustomLoad;
        TICSFilter *FFilter;
        bool intload;
        bool ValidateClipboard;
        UnicodeString FNomGUI;
        TTagNode *FBaseNom;
        TcxCheckBox      *FInclToNameChB;
//        bool FMPress;
        TcxTreeListNode  *FDragedNode;
        void __fastcall FSetCustomLoad(TOnFltCustomLoadEvent AOnCustomLoad);

        TTagNode* __fastcall FGetDTD();
        TTagNode *FReg;
        TTagNode *FDTDNode;
        bool NomCreated;
        void __fastcall FSetNomSrc(TTagNode *ANomSrc);
        void __fastcall FSetNom(TICSNomenklator *ANom);
        void __fastcall FValidateClipboard(TDTDTagNode *AndRData);
        bool __fastcall FOnNodeCheckRef(TTagNode *ASrc);
        void __fastcall FSwapCondGr(TTagNode *ASrc);
        void __fastcall FSetNodeText(TcxTreeListNode *ANode);
        void __fastcall FSetEditType(TFilterEditType   AEditType);
        void __fastcall FPaintNomGroup(TTagNode *ASrc, TcxTreeListNode *AParent);
        UnicodeString __fastcall GetXMLNodeViewText(TDTDTagNode *TagNode);
        void __fastcall FSetActEnabled(TAction *AAct, bool AVal);
        TcxTreeListNode* __fastcall FGetRulesFocusedNode();
        TcxTreeListNode* __fastcall FGetNomFocusedNode();
        TTagNode* __fastcall FGetNomData(TcxTreeListNode *ATreeNode = NULL);
        TDTDTagNode* __fastcall FGetRulesData(TcxTreeListNode *ATreeNode = NULL);
        bool __fastcall FCheckAddAction(TTagNode *ANomTag, TDTDTagNode *ARuleTag);
        bool __fastcall FCheckInsertAction(TTagNode *ANomTag, TDTDTagNode *ARuleTag);
        bool __fastcall FCheckAddComm(TDTDTagNode *ARuleTag);
        bool __fastcall FCheckAddGrAction(TTagNode *ANomTag, TDTDTagNode *ARuleTag);
        bool __fastcall FCheckInsertGrAction(TTagNode *ANomTag, TDTDTagNode *ARuleTag);
        bool __fastcall FCheckSwapAction(TTagNode *ANomTag, TDTDTagNode *ARuleTag);
        bool __fastcall FCheckSetNOTAction(TTagNode *ANomTag, TDTDTagNode *ARuleTag);
        bool __fastcall FCheckDelAction(TTagNode *ANomTag, TDTDTagNode *ARuleTag);
        bool __fastcall FCheckSetValAction(TTagNode *ANomTag, TDTDTagNode *ARuleTag);
        bool __fastcall FCheckResetValAction(TTagNode *ANomTag, TDTDTagNode *ARuleTag);
        bool __fastcall FCheckEditComAction(TTagNode *ANomTag, TDTDTagNode *ARuleTag);

        void __fastcall FSetActEnabled(TcxTreeListNode *AFocusedNode, bool AAllEn, bool IsRulesTV);
        bool __fastcall SetInclToName(TcxTreeListNode *ANode, TDTDTagNode *ANodeData);
        void __fastcall FOnCopyNodes(TTagNode *ASrc, TDTDTagNode *ADest);

        void __fastcall SetActVisible();
        HCURSOR             FDragAdd;
        HCURSOR             FDragInsBefore;
        HCURSOR             FDragInsAfter;

        TAxeXMLContainer*   FXMLContainer;               // XML-��������� ( ��. AxeUtil.h )
        TFilterEditType     FEditType;
        TOnGetMessageTextEvent FGetMessageText;
        UnicodeString          FMainISUID;                  //��� �������� �������� ������������
        UnicodeString          FRootName;
        TAnsiStrMap         FGroupNames;
        TCursor             FSaveCursor;
        int                 FDomainNumber;               // ����� ������
        TICSNomenklator     *FNom;
        bool                FModify;
        bool                FCanExtAdd;
        bool                FCanKonkr;
        UnicodeString __fastcall MsgTxt(TFilterEditMsgType AType);
        void __fastcall FSetCanKonkr(bool AVal);
        void __fastcall FDelEmptyNode(TTagNode *ASrc);
        TTagNode* __fastcall FGetFlt();
        void      __fastcall FSetFlt(TTagNode* ASrc);
        void      __fastcall FLoadTemplate(TDTDTagNode *ADest, TTagNode *ASrc);
        void      __fastcall FSetModify(bool AVal, TcxTreeListNode *ANode, bool ACanChild);
        void      __fastcall FPaintNom();
        void      __fastcall FNewFilter();
        void      __fastcall FNewCondition();
        void      __fastcall FNewTemplate();
        void      __fastcall SetDomainNumber( int AValue );
        void      __fastcall IGroupKonkr(TTagNode *ANomData, TDTDTagNode *ARuleData, TInfGroupAddType AAddType);
        void      __fastcall TagNodeToTreeNode(TcxTreeListNode *ADest, TTagNode *ASrc);
        void      __fastcall ClearNom();
        void      __fastcall LoadNom();
        bool      __fastcall CanInfGroup(TDTDTagNode *ASrc, bool CanKonkrOnly);
        TTagNode  *FFltRet;
        TBeforeDeleteEvent FBeforeDelete;
        /*��������������� ������*/
        bool __fastcall CallDefProc( TTagNode* AIE, UnicodeString &AText, bool NeedKonkr );
public:		// User declarations
        bool __fastcall IsEmpty();
        bool __fastcall IsNone();
//        Variant    NomDef;
        __fastcall TFilterEditExForm(TComponent* Owner);
        virtual __fastcall ~TFilterEditExForm();
        void    __fastcall New();
        TTagNode* __fastcall AsFilter();
        TCheckFilterResult __fastcall Check();
        __property TTagNode*         NomSrc       = { read = FBaseNom,write=FSetNomSrc};
        __property TICSNomenklator*  Nom          = { read = FNom,write=FSetNom};
        __property TTagNode*         DTD          = { read = FGetDTD};
        __property TTagNode*         Filter       = { read = FGetFlt,write=FSetFlt};
        __property bool              CanExtAdd    = { read = FCanExtAdd,write=FCanExtAdd};
        __property bool              Modify       = { read = FModify};
        __property bool              CanKonkr     = { read = FCanKonkr,write=FSetCanKonkr};
        __property UnicodeString     NomGUI       = { read = FNomGUI};
        __property TFilterEditType   EditType     = { read = FEditType, write = FSetEditType};
        __property TAxeXMLContainer* XMLContainer = { read = FXMLContainer, write = FXMLContainer};
        __property int               DomainNumber = { read = FDomainNumber, write = SetDomainNumber, default = 0 };
        __property UnicodeString     MainISUID    = { read = FMainISUID, write = FMainISUID};
        __property TTagNode*         ParentXML    = { read = FParentXML, write = FParentXML};

        __property TBeforeDeleteEvent     BeforeDelete     = { read = FBeforeDelete, write = FBeforeDelete};
        __property TOnCondCanCloseEvent   OnCanClose       = { read = FCanClose, write = FCanClose};
        __property TOnGetMessageTextEvent OnGetMessageText = { read = FGetMessageText, write = FGetMessageText};
        __property TOnFltCustomLoadEvent  OnCustomLoad     = { read = FOnCustomLoad, write = FSetCustomLoad};
  __property TdsNomCallDefProcEvent       OnCallDefProc    = { read = FOnCallDefProc, write = FOnCallDefProc};
};

//---------------------------------------------------------------------------
namespace ICSDocFilter
{
enum TCheckDocFilterResult
{
  frOk = 0,     // Ok
  frBad = 1,    // ������� ������ �������� ������ ��� �������������������� ������
  frEmpty = 2,  // � xml-���� "condrules" ��� �������� ���������
  frNone = 3    // ������ �� ������
};
extern PACKAGE TCheckDocFilterResult __fastcall CheckCondRules( TTagNode *ACondRules );
} //end of namespace ICSDocFilter
using namespace ICSDocFilter;

#endif
