//---------------------------------------------------------------------------

#ifndef ICSFilterH
#define ICSFilterH
//---------------------------------------------------------------------------
#include "XMLContainer.h"
//---------------------------------------------------------------------------
class PACKAGE EICSFilterError : public System::Sysutils::Exception
{
//        typedef Exception inherited;
public:
     __fastcall EICSFilterError(const UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE EICSCondNodeError : public System::Sysutils::Exception
{
//        typedef Exception inherited;
public:
     __fastcall EICSCondNodeError(const UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE EICSDTDError : public System::Sysutils::Exception
{
//        typedef Exception inherited;
public:
     __fastcall EICSDTDError(const UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE TICSDTDChecker: public TTagNode
{
protected:
     TTagNode* __fastcall GetTag(UnicodeString AName);
private:
     TAnsiStrMap    FNameUIDMap;
     void       __fastcall FFSetXML(UnicodeString AXML);
//     void       __fastcall FFSetZIPXML(UnicodeString AXML);
     TTagNode* __fastcall FCheckNode(UnicodeString AParentName, UnicodeString AName);
public:
     __fastcall TICSDTDChecker();

     TTagNode*  __fastcall SwapTo(TTagNode *ANode, UnicodeString ANewNodeName);

     void __fastcall Assign(TTagNode *Src, bool IsRecurse);
     bool       __fastcall LoadFromFile(UnicodeString  FileName);
     void       __fastcall LoadFromXMLFile(UnicodeString AName);
     void       __fastcall LoadFromZIPXMLFile(UnicodeString AName);
     void       __fastcall FCreateNames();

     bool __fastcall CheckRef(TTagNode *ANode, UnicodeString ANomGUI);
     bool __fastcall TagExist(TTagNode* ATag);
     bool __fastcall TagExist(UnicodeString AName);
     bool __fastcall AttrExist(TTagNode *ANode, UnicodeString AAttrName);
     bool __fastcall AttrExist(UnicodeString ANodeName, UnicodeString AAttrName);
     bool __fastcall CheckAttrVal(TTagNode *ANode, UnicodeString AAttrName,UnicodeString AAttrValue);
     bool __fastcall CheckAttrVal(UnicodeString ANodeName, UnicodeString AAttrName,UnicodeString AAttrValue);
     TTagNode* __fastcall DTDCHAddChild(TTagNode *AParent, UnicodeString AName, UnicodeString AAttrList);
     TTagNode* __fastcall DTDCHInsert(TTagNode *ANode, UnicodeString AName, bool ABefore);
     bool __fastcall CanAddChild(TTagNode *AParent, UnicodeString AName);
     bool __fastcall CanAddChild(UnicodeString AParentName, UnicodeString AName);
     __property UnicodeString AsXML    = {write=FFSetXML};
//     __property UnicodeString AsZIPXML = {write=FFSetZIPXML};

};
//---------------------------------------------------------------------------
class PACKAGE TICSFilterPassport: public TTagNode
{
typedef TICSFilterPassport* pTPassp ;
private:
     UnicodeString __fastcall FGetGUI();
     void       __fastcall FSetGUI(UnicodeString ASrc);
     UnicodeString __fastcall FGetName();
     void       __fastcall FSetName(UnicodeString ASrc);
     UnicodeString __fastcall FGetAuthor();
     void       __fastcall FSetAuthor(UnicodeString ASrc);
     UnicodeString __fastcall FGetVersion();
     void       __fastcall FSetVersion(UnicodeString ASrc);
     UnicodeString __fastcall FGetRelease();
     void       __fastcall FSetRelease(UnicodeString ASrc);
     UnicodeString __fastcall FGetTimestamp();
     void       __fastcall FSetTimestamp(UnicodeString ASrc);
     UnicodeString __fastcall FGetDefVersion();
     void       __fastcall FSetDefVersion(UnicodeString ASrc);
public:
     __fastcall TICSFilterPassport(TTagNode *AParent);
     void __fastcall New();
__published:
     __property UnicodeString GUI        = {read=FGetGUI       ,write=FSetGUI       };
     __property UnicodeString MainName   = {read=FGetName      ,write=FSetName      };
     __property UnicodeString Author     = {read=FGetAuthor    ,write=FSetAuthor    };
     __property UnicodeString Version    = {read=FGetVersion   ,write=FSetVersion   };
     __property UnicodeString Release    = {read=FGetRelease   ,write=FSetRelease   };
     __property UnicodeString Timestamp  = {read=FGetTimestamp ,write=FSetTimestamp };
     __property UnicodeString DefVersion = {read=FGetDefVersion,write=FSetDefVersion};
};
//---------------------------------------------------------------------------
class PACKAGE TDTDTagNode;
typedef void __fastcall (__closure *TDTDNodeCopy)(TTagNode *ASrc, TDTDTagNode *ADest);
typedef bool __fastcall (__closure *TDTDNodeCheckRef)(TTagNode *ASrc);
//---------------------------------------------------------------------------
class PACKAGE TDTDTagNode: public TTagNode
{
private:
     TICSDTDChecker* __fastcall FGetDTD();
     UnicodeString      __fastcall FGetNomGUI();
     UnicodeString      __fastcall FGetAV(UnicodeString AName);
     void            __fastcall FSetAV(UnicodeString AName, UnicodeString AVal);
     TDTDTagNode*    __fastcall FGetCondRules();
     TDTDTagNode*    __fastcall DTDAddChild();
     TDTDNodeCopy      __fastcall GetOnCopyNode();
     TDTDNodeCheckRef  __fastcall GetOnCheckNodeRef();

public:
      __fastcall TDTDTagNode();
      __fastcall TDTDTagNode(TDTDTagNode *AParent);
     bool __fastcall CheckNodeRef(TTagNode* ASrc);
__published:
     TDTDTagNode*  __fastcall DTDAddChild(UnicodeString AName, TTagNode *ASrc = NULL);
     TDTDTagNode*  __fastcall DTDAddChild(UnicodeString AName,UnicodeString AAttrs, TTagNode *ASrc = NULL);
     TDTDTagNode*  __fastcall DTDInsert(UnicodeString AName, bool ABefore = true, TTagNode *ASrc = NULL);
     TDTDTagNode*  __fastcall DTDInsertChild(TTagNode* AInsTag, UnicodeString AName, bool ABefore, TTagNode *ASrc = NULL);
     TDTDTagNode*  __fastcall InsertEx(bool ABefore, UnicodeString ASrc);
     TDTDTagNode*  __fastcall InsertEx(bool ABefore, TTagNode *ASrc);
     TDTDTagNode*  __fastcall AddChildEx(TTagNode *ASrc);
     TDTDTagNode*  __fastcall SwapTo(UnicodeString ANewNodeName);
     TDTDTagNode*  __fastcall GetParent();
     bool          __fastcall Check(TTagNode *ASrc);
     bool          __fastcall CheckPaste(TTagNode *ASrc, UnicodeString ADestTagName = "");
     void          __fastcall Copy(TTagNode* ASrc);
     __property TICSDTDChecker*     DTD    = {read=FGetDTD};
     __property UnicodeString          NomGUI = {read=FGetNomGUI};
     __property UnicodeString AV[UnicodeString AName] = {read=FGetAV ,write=FSetAV};
};
//---------------------------------------------------------------------------
class PACKAGE TICSFilter : public TTagNode
{
private:
     TICSFilterPassport* FPassport;
     TDTDTagNode*        FSource;
     TICSDTDChecker*     FDTD;
     TDTDNodeCopy        FOnCopyNode;
     TDTDNodeCheckRef    FOnCheckNodeRef;
     void     __fastcall FSetDTD(TICSDTDChecker *ASrc);

     UnicodeString          FNomGUI;
     void         __fastcall FSetNomGUI(UnicodeString AGUI);
     TDTDTagNode* __fastcall FCreateCondrules();
     TTagNode*    __fastcall FGetCondition();
     void         __fastcall FSetCondition(TTagNode *ASrc);
     TTagNode*    __fastcall FGetFilter();
     void         __fastcall FSetFilter(TTagNode *ASrc);
public:
     __fastcall TICSFilter();
     __fastcall ~TICSFilter();
__published:
     __property TICSFilterPassport* Passport    = {read=FPassport};
     __property TDTDTagNode*        Condition   = {read=FSource};
     __property TICSDTDChecker*     DTD         = {read=FDTD,write=FSetDTD};
     __property UnicodeString          NomGUI      = {read=FNomGUI,write=FSetNomGUI};
     __property TTagNode*           AsCondition = {read=FGetCondition,write=FSetCondition};
     __property TTagNode*           AsFilter    = {read=FGetFilter,write=FSetFilter};
     __property TDTDNodeCopy        OnCopyNode  = {read=FOnCopyNode,write=FOnCopyNode};
     __property TDTDNodeCheckRef    OnCheckNodeRef  = {read=FOnCheckNodeRef,write=FOnCheckNodeRef};

};
//---------------------------------------------------------------------------
#endif
