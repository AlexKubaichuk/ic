object IGroupKonkrForm: TIGroupKonkrForm
  Left = 475
  Top = 233
  BorderStyle = bsDialog
  BorderWidth = 8
  Caption = 'IGroupKonkrForm'
  ClientHeight = 373
  ClientWidth = 442
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pBottom: TPanel
    Left = 0
    Top = 332
    Width = 442
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object ApplyBtn: TcxButton
      Left = 78
      Top = 11
      Width = 75
      Height = 25
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      TabOrder = 0
      OnClick = ApplyBtnClick
    end
    object IgnoreBtn: TcxButton
      Left = 156
      Top = 11
      Width = 206
      Height = 25
      Caption = #1054#1089#1090#1072#1074#1080#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1077' '#1085#1077#1086#1087#1088#1077#1076#1077#1083#1077#1085#1085#1099#1084
      ModalResult = 5
      TabOrder = 1
    end
    object CancelBtn: TcxButton
      Left = 366
      Top = 11
      Width = 75
      Height = 25
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 2
    end
  end
  object clbElements: TcxCheckListBox
    Left = 0
    Top = 0
    Width = 442
    Height = 332
    Align = alClient
    EditValueFormat = cvfIndices
    Items = <>
    PopupMenu = PopupMenu1
    TabOrder = 1
    OnClickCheck = clbElementsClickCheck
  end
  object ActionList: TActionList
    Left = 359
    Top = 8
    object actSelAll: TAction
      Caption = #1042#1099#1073#1088#1072#1090#1100' &'#1074#1089#1077
      HelpType = htContext
      Hint = #1042#1099#1073#1088#1072#1090#1100' '#1074#1089#1077
      OnExecute = actSelAllExecute
    end
    object actClearSel: TAction
      Caption = '&'#1057#1073#1088#1086#1089
      HelpType = htContext
      Hint = #1057#1073#1088#1086#1089
      OnExecute = actClearSelExecute
    end
    object actInvSel: TAction
      Caption = '&'#1048#1085#1074#1077#1088#1089#1080#1103
      HelpType = htContext
      Hint = #1048#1085#1074#1077#1088#1089#1080#1103
      OnExecute = actInvSelExecute
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 298
    Top = 66
    object N1: TMenuItem
      Action = actSelAll
    end
    object N2: TMenuItem
      Action = actClearSel
    end
    object N3: TMenuItem
      Action = actInvSel
    end
  end
end
