//---------------------------------------------------------------------------


#include <vcl.h>
#pragma hdrstop

#include "ICSNomenklator.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
//---------------------------------------------------------------------------

//******************************************************************************
//*                                                                            *
//*                       TICSBaseNomenklator                                  *
//*                                                                            *
//******************************************************************************
//---------------------------------------------------------------------------
__fastcall EICSNomenklatorError::EICSNomenklatorError(const UnicodeString Msg)
: Sysutils::Exception(Msg)
{
}
//---------------------------------------------------------------------------
__fastcall EICSNomDomainError::EICSNomDomainError(const UnicodeString Msg)
: Sysutils::Exception(Msg)
{
}
//---------------------------------------------------------------------------
__fastcall TICSBaseNomenklator::TICSBaseNomenklator()
                           : TTagNode(NULL)
{
//  FSrcNom = new TTagNode(NULL);
  FValid = false;
}
//---------------------------------------------------------------------------
__fastcall TICSBaseNomenklator::~TICSBaseNomenklator()
{
  FValid = false;
//  delete FSrcNom;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomenklator::AssignNom(TTagNode *Src)
{
  FValid = false;
  FSrcNom = Src;
  FCreateNom();
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TICSBaseNomenklator::FGetSrcNom()
{
  if (FValid)
   return FSrcNom;
  else
   return NULL;
}
//---------------------------------------------------------------------------

void __fastcall TICSBaseNomenklator::FCreateNom()
{
  //����� ������ � ��������� �������
  TTagNode* DomainNode = GetDomainNode();
  //����� �������� �������������� �������� � ������
  TTagNode* ISNode = ( FMainISUID == "" ) ? DomainNode->GetFirstChild() : FSrcNom->GetTagByUID( FMainISUID );
  if ( !ISNode )
    throw EICSNomenklatorError("�������� �������������� �������� � UID'��  '"+FMainISUID+"' �� �������.");
  if ( ISNode->GetParent() != DomainNode )
    throw EICSNomDomainError("�������� �������������� �������� � UID'�� '"+FMainISUID+"' �� ����������� ������ � ������� "+IntToStr((int)FDomainNumber)+".");

  this->Assign(FSrcNom,false);
  this->AddChild()->Assign(FSrcNom->GetChildByName("passport"),true);
  if (FSrcNom->GetChildByName("technounit"))
   this->AddChild()->Assign(FSrcNom->GetChildByName("technounit"),true);
  TTagNode *FDomNodes = this->AddChild();
  FDomNodes->Assign(DomainNode,false);
  GetISList(ISNode, FDomNodes);
  FValid = true;
}

//---------------------------------------------------------------------------
int __fastcall TICSBaseNomenklator::GetISList( TTagNode *MainIS, TTagNode *ANewNom, UnicodeString TagName )
{
  //��������� ������ �������������� ���������, ��������� � ��������
  TStringList *ISList = new TStringList;
  GetLinkedIS( MainIS, ISList, TagName );
  for (int i = 0; i < ISList->Count; i++)
   {
     TTagNode* ISNode = dynamic_cast<TTagNode*>( ISList->Objects[i] );
     if( ISNode->GetChildByName(TagName) )
      ANewNom->AddChild()->Assign(ISNode,true);
   }
  delete ISList;
  return ANewNom->Count;
}

//---------------------------------------------------------------------------
void __fastcall TICSBaseNomenklator::GetLinkedIS(TTagNode *ABaseIS, TStringList *AISList, UnicodeString TagName)
{
  if ( ABaseIS->GetChildByName(TagName) )
    AISList->AddObject( "", ABaseIS );
  TTagNode *itNode = ABaseIS->GetChildByName("refers");
  if (!itNode) return;
  itNode = itNode->GetFirstChild();
  TStringList *FSearchPath;
  try
  {
    FSearchPath = new TStringList;
    // ��������� ������ �� �������� � ������ ����������� ����
    FSearchPath->Add(GetGUI(ABaseIS)+"."+ABaseIS->AV["uid"]);
    while(itNode)
    {
      // �������� � ��� �������� �������� � �����
      FGetLinkedIS(itNode->GetRefer("ref"),FSearchPath,AISList);
      itNode =itNode->GetNext();
    }
  }
  __finally
  {
    if (FSearchPath) delete FSearchPath;
  }
}

//---------------------------------------------------------------------------
void __fastcall TICSBaseNomenklator::FGetLinkedIS(TTagNode *ALinkIS, TStringList *ASearchPath, TStringList *AISList, UnicodeString TagName)
{
  if (ASearchPath->IndexOf(GetGUI(ALinkIS)+"."+ALinkIS->AV["uid"]) == -1)
   {
     // ��������� ������ �� �������� � ������ ����������� ����
     ASearchPath->Add(GetGUI(ALinkIS)+"."+ALinkIS->AV["uid"]);
     if (ALinkIS->GetChildByName(TagName))
       AISList->AddObject("",(TObject*)ALinkIS);
   }
  else
    return;
  TTagNode *itNode = ALinkIS->GetChildByName("refers");
  if (!itNode) return;
  itNode = itNode->GetFirstChild();
  while(itNode)
   { // �������� � ��� �������� �������� � �����
     FGetLinkedIS(itNode->GetRefer("ref"),ASearchPath,AISList);
     itNode = itNode->GetNext();
   }
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TICSBaseNomenklator::GetDomainNode()
{
  int i = 0;
  TTagNode* DomainNode = FSrcNom->GetChildByName( "domain" );
  while ( ( i++ < FDomainNumber ) && DomainNode )
    DomainNode = DomainNode->GetNext();
  if ( !DomainNode )
    throw EICSNomDomainError("����� � ������� "+IntToStr((int)FDomainNumber)+" � ������������ �� ������.");
  return DomainNode;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSBaseNomenklator::GetGUI(TTagNode *ANode)
{
  return ANode->GetRoot()->GetChildByName("passport")->AV["gui"];
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomenklator::FSetDomainNumber(int ADomNum)
{
  FValid = false;
  if ( ADomNum < 0 )
    throw EICSNomDomainError("����� ������ ������ ���������� � 0.");
  FDomainNumber = ADomNum;
}
//---------------------------------------------------------------------------
void __fastcall TICSBaseNomenklator::FSetMainISUID(UnicodeString ASrc)
{
  FValid = false;
  FMainISUID = ASrc;
}
//---------------------------------------------------------------------------

//******************************************************************************
//*                                                                            *
//*                          TICSNomenklator                                   *
//*                                                                            *
//******************************************************************************

//---------------------------------------------------------------------------
__fastcall TICSNomenklator::TICSNomenklator()
{
  FValid = false;
  FSrcNom = NULL;
  FNomMap.clear();
  FDomainNumber = 0;
  FMainISUID = "";
}
//---------------------------------------------------------------------------
__fastcall TICSNomenklator::~TICSNomenklator()
{
  ClearNoms();
  FValid = false;
}
//---------------------------------------------------------------------------
void __fastcall TICSNomenklator::FSetSrcNom(TTagNode *ASrc)
{
  FValid = false;
  FSrcNom = ASrc;
  if (!FSrcNom)
    throw EICSNomenklatorError("������ �������� ������������.");
  if (!FSrcNom->CmpName("mais_ontology"))
    throw EICSNomenklatorError("�� ���������� ��������� ������������ (�������� ���).");
  if (!FSrcNom->GetChildByName("passport"))
    throw EICSNomenklatorError("�� ���������� ��������� ������������ (�������).");
  if (!FSrcNom->GetChildByName("domain"))
    throw EICSNomenklatorError("�� ���������� ��������� ������������ (�����).");
  ClearNoms();
  TTagNode *itDom = FSrcNom->GetChildByName("domain");
  TTagNode *itIS;
  int itDomNum = 0;
  UnicodeString ISUID;
  TICSBaseNomenklator *itNom;
  if (FPrInit)
   FPrInit(FSrcNom->GetCount(true,"IS"),"���������� ������������...");
  while (itDom)
   {
     itIS = itDom->GetChildByName("IS");
     while (itIS)
      {
        if (FPrCh)
         FPrCh(1, "���������� ������������...");
        Application->ProcessMessages();
        ISUID = itIS->AV["uid"];
        itNom = new TICSBaseNomenklator();
        itNom->DomainNumber = itDomNum;
        itNom->MainISUID    = ISUID;
        itNom->AssignNom(FSrcNom);
        FNomMap[IntToStr((int)itDomNum)+"_"+ISUID] = itNom;
        itIS = itIS->GetNext();
      }
     itDomNum++;
     itDom = itDom->GetNext();
   }
  FValid = true;
}
//---------------------------------------------------------------------------
TICSBaseNomenklator* __fastcall TICSNomenklator::FGetCurrNom()
{
  TICSBaseNomenklator* RC = NULL;
  try
   {
     TTagNode* DomainNode = GetDomainNode(FDomainNumber);
     if (!DomainNode)
       throw EICSNomenklatorError("����� � "+IntToStr((int)FDomainNumber)+" �� ������.");
     TTagNode* ISNode = ( FMainISUID == "" ) ? DomainNode->GetFirstChild() : FSrcNom->GetTagByUID( FMainISUID );
     if ( !ISNode )
       throw EICSNomenklatorError("�������� �������������� �������� � UID'��  '"+FMainISUID+"' �� �������.");
     if ( ISNode->GetParent() != DomainNode )
       throw EICSNomDomainError("�������� �������������� �������� � UID'�� '"+FMainISUID+"' �� ����������� ������ � ������� "+IntToStr((int)FDomainNumber)+".");
     FMainISUID = ISNode->AV["uid"];
     TICSNomMap::iterator FRC = FNomMap.find(IntToStr((int)FDomainNumber)+"_"+FMainISUID);
     if (FRC != FNomMap.end())  RC = FRC->second;
     else                       throw EICSNomenklatorError("�������� � uid=\""+FMainISUID+"\" � ������ � "+IntToStr((int)FDomainNumber)+" �����������.");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSNomenklator::ClearNoms()
{
  for (TICSNomMap::iterator i = FNomMap.begin(); i != FNomMap.end(); i++)
   delete i->second;
  FNomMap.clear();
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TICSNomenklator::GetDomainNode(int ADomainNumber)
{
  int i = 0;
  TTagNode* DomainNode = FSrcNom->GetChildByName( "domain" );
  while ( ( i++ < ADomainNumber ) && DomainNode )
    DomainNode = DomainNode->GetNext();
  if ( !DomainNode )
    throw EICSNomDomainError("����� � ������� "+IntToStr((int)FDomainNumber)+" � ������������ �� ������.");
  return DomainNode;
}
//---------------------------------------------------------------------------

