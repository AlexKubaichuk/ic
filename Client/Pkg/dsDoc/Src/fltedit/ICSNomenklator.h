//---------------------------------------------------------------------------

#ifndef ICSNomenklatorH
#define ICSNomenklatorH
//---------------------------------------------------------------------------
#include "XMLContainer.h"
//---------------------------------------------------------------------------
class PACKAGE EICSNomenklatorError : public System::Sysutils::Exception
{
        typedef System::Sysutils::Exception inherited;
public:
     __fastcall EICSNomenklatorError(const UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE EICSNomDomainError : public System::Sysutils::Exception
{
        typedef System::Sysutils::Exception inherited;
public:
     __fastcall EICSNomDomainError(const UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE TICSBaseNomenklator : public TTagNode
{
private:
     bool        FValid;
     TTagNode    *FSrcNom;
     int         FDomainNumber;
     UnicodeString  FMainISUID;
     void        __fastcall FSetDomainNumber(int ADomNum);
     void        __fastcall FSetMainISUID(UnicodeString ASrc);
     void        __fastcall FCreateNom();
     int         __fastcall GetISList( TTagNode *MainIS, TTagNode *ANewNom, UnicodeString TagName = "rulesunit" );
     void        __fastcall GetLinkedIS(TTagNode *ABaseIS, TStringList *AISList, UnicodeString TagName = "rulesunit" );
     void        __fastcall FGetLinkedIS(TTagNode *ALinkIS, TStringList *ASearchPath, TStringList *AISList, UnicodeString TagName = "rulesunit" );
     TTagNode*   __fastcall FGetSrcNom();
     UnicodeString  __fastcall GetGUI(TTagNode *ANode);
public:
     __fastcall TICSBaseNomenklator();
     __fastcall ~TICSBaseNomenklator();
     TTagNode*  __fastcall GetDomainNode();
     void       __fastcall AssignNom(TTagNode *Src);
__published:

     __property bool       Valid        = { read = FValid};
     __property int        DomainNumber = { read = FDomainNumber, write = FSetDomainNumber, default = 0 };
     __property UnicodeString MainISUID    = { read = FMainISUID, write = FSetMainISUID};
     __property TTagNode*  BaseNom      = { read = FGetSrcNom};
};
//---------------------------------------------------------------------------
typedef map<UnicodeString, TICSBaseNomenklator*> TICSNomMap;
//---------------------------------------------------------------------------
class PACKAGE TICSNomenklator : public TObject
{
private:
     bool        FValid;
     TTagNode    *FSrcNom;
     TICSNomMap  FNomMap;
     int         FDomainNumber;
     UnicodeString  FMainISUID;

     void                 __fastcall ClearNoms();
     TICSBaseNomenklator* __fastcall FGetCurrNom();
     void                 __fastcall FSetSrcNom(TTagNode *ASrc);

     TProgressInit     FPrInit;
     TProgressChange   FPrCh;
     TProgressComplite FPrComp;
public:
     __fastcall TICSNomenklator();
     __fastcall ~TICSNomenklator();
__published:
     TTagNode* __fastcall GetDomainNode(int ADomainNumber);

     __property TProgressInit     OnPrInit = {read = FPrInit, write = FPrInit};
     __property TProgressChange   OnPrCh   = {read = FPrCh,   write = FPrCh};
     __property TProgressComplite OnPrComp = {read = FPrComp, write = FPrComp};

     __property bool                 Valid        = {read = FValid};
     __property int                  DomainNumber = {read = FDomainNumber, write = FDomainNumber, default = 0 };
     __property UnicodeString           MainISUID    = {read = FMainISUID,    write = FMainISUID};
     __property TTagNode*            SrcNom       = {read = FSrcNom,       write = FSetSrcNom};
     __property TICSBaseNomenklator* Nmkl         = {read = FGetCurrNom};
};
//---------------------------------------------------------------------------
#endif
