/*
  ����        - IGroup.cpp
  ������      - ��������������� (ICS)
  ��������    - ���� ���������� ������, ����������� ��������
                ������� ������������� �������������� ������
                ( ������������ ����������� TICSDocFilter )
  ����������� - ������� �.�.
  ����        - 02.09.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "IGroup.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxCheckListBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TIGroupKonkrForm::TIGroupKonkrForm(TComponent* Owner, TDTDTagNode* ISection, TTagNode* ANmkl, bool AIsFilter )
        : TForm(Owner)
{
  if (!ISection)
    throw Exception("");
  if (!ISection->CmpName("IOR,IMOR"))
    throw Exception("������ ������������ ��� ������������� ������ ������ IOR � IMOR.");
  if (!ANmkl)
    throw Exception("");
  if (ANmkl->Name != "mais_ontology")
    Exception("��������� xml-�������� �� �������� �������������.");
  FIsFilter = AIsFilter;
  FISec     = ISection;
  FNmkl     = ANmkl;

  if ( FISec->CmpName("IMOR") )
    Caption = "����� ���������� ���������� �� ...";
  else
  {
    Caption = "����� ������ ��������� �� ...";
    clbElements->PopupMenu = NULL;
  }
  TTagNode* CurNode = FISec->GetFirstChild();
  TcxCheckListBoxItem *tmpItem;
  while ( CurNode )
  {
    UnicodeString ViewText = "������!!!";
    if      ( CurNode->CmpName("IERef") )
      ViewText = FNmkl->GetTagByUID( _UID( CurNode->AV["ref"] ) )->AV["name"];
    else if ( CurNode->CmpName("ROR,RAND") )
      ViewText = CurNode->AV["comment"];
    else if ( CurNode->CmpName("IAND,IOR,IMOR") )
      ViewText = CurNode->AV["name"];
    tmpItem = clbElements->Items->Add();

    tmpItem->Text = ViewText;
    tmpItem->ItemObject = CurNode;
    CurNode = CurNode->GetNext();
  }
  clbElements->ItemIndex = 0;
  ApplyBtn->Enabled = false;
}
//---------------------------------------------------------------------------
//***************************************************************************
//********************** ������ ����� ***************************************
//***************************************************************************
bool __fastcall TIGroupKonkrForm::AreInputsCorrect()
{
        //---- ����� ���������� ��������� ���� �������� ----

  return true;
}

//***************************************************************************
//********************** ����������� �������*********************************
//***************************************************************************
void __fastcall TIGroupKonkrForm::actSelAllExecute(TObject *Sender)
{
  for ( int i = 0; i < clbElements->Count; i++ )
    clbElements->Items->Items[i]->Checked  = true;
  ApplyBtn->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TIGroupKonkrForm::actClearSelExecute(TObject *Sender)
{
  for ( int i = 0; i < clbElements->Count; i++ )
    clbElements->Items->Items[i]->Checked  = false;
  ApplyBtn->Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall TIGroupKonkrForm::actInvSelExecute(TObject *Sender)
{
  bool bEn = false;
  for ( int i = 0; i < clbElements->Count; i++ )
   {
     clbElements->Items->Items[i]->Checked  = !clbElements->Items->Items[i]->Checked;
     bEn |= clbElements->Items->Items[i]->Checked;
   }
  ApplyBtn->Enabled = bEn;
}
//---------------------------------------------------------------------------
void __fastcall TIGroupKonkrForm::ApplyBtnClick(TObject *Sender)
{
  if ( !AreInputsCorrect() )
    return;

  if ( FISec->CmpName("IMOR") )
  {
/*
    FISec->Name = ( ;
    if ( FIsFilter )
      FISec->DeleteAttr( "incltoname" );
    FISec->DeleteAttr( "uniontype" );
    FISec->DeleteAttr( "uid" );
    FISec->AV["pure"] = "0";
    FISec->AV["comment"] = FISec->AV["name"];
    FISec->DeleteAttr( "name" );
*/
    UnicodeString FSecName = FISec->AV["name"];
    for ( int i = 0; i < clbElements->Count; i++ )
      if ( !clbElements->Items->Items[i]->Checked )
        FISec->DeleteChild( dynamic_cast<TTagNode*>(clbElements->Items->Items[i]->ItemObject) );
    FISec->SwapTo(UnicodeString((FISec->CmpAV("uniontype","or") ) ? "ROR" : "RAND"));
    FISec->AV["pure"] = "0";
    FISec->AV["comment"] = FSecName;
  }
  else
  {
    TTagNode* ChoosenNode = new TTagNode;
    for ( int i = 0; i < clbElements->Count; i++ )
      if ( clbElements->Items->Items[i]->Checked)
      {
        ChoosenNode->Assign( dynamic_cast<TTagNode*>(clbElements->Items->Items[i]->ItemObject), true );
        break;
      }
    FISec->Assign( ChoosenNode, true );
    delete ChoosenNode;
  }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TIGroupKonkrForm::clbElementsClickCheck(TObject *Sender,
      int AIndex, TcxCheckBoxState APrevState, TcxCheckBoxState ANewState)
{
  try
   {
//     clbElements->BeginUpdate();
     if (FISec->CmpName("IOR"))
      {
        int  nCheckedItemIndex;                               //������ ���������� �������� ������
        TcxCheckBoxState bSelectedItemChecked;                            //���������� ������ �� ���������� �������

        bSelectedItemChecked = ANewState;

        //����� ���� ��������� ���������, ����� ���������� (������� ����� ������ ���� �������)
        for ( int i=0; i < clbElements->Count; i++ )
          if ((clbElements->Items->Items[i]->State == cbsChecked) &&
               (i != AIndex) &&
               (bSelectedItemChecked == cbsChecked))
            clbElements->Items->Items[i]->State = cbsUnchecked;
      }
     bool bEn = false;
     for (int i = 0; i < clbElements->Count; i++)
       if (clbElements->Items->Items[i]->Checked)
       {
         bEn = true;
         break;
       }
     ApplyBtn->Enabled = bEn;
   }
  __finally
   {
//     clbElements->EndUpdate();
   }
}
//---------------------------------------------------------------------------

