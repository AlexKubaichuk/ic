object FilterPassportEditForm: TFilterPassportEditForm
  Left = 286
  Top = 166
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
  ClientHeight = 152
  ClientWidth = 326
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 10
    Top = 12
    Width = 79
    Height = 13
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077':'
  end
  object Label2: TLabel
    Left = 10
    Top = 39
    Width = 33
    Height = 13
    Caption = #1040#1074#1090#1086#1088':'
  end
  object Label3: TLabel
    Left = 10
    Top = 66
    Width = 40
    Height = 13
    Caption = #1042#1077#1088#1089#1080#1103':'
  end
  object Label4: TLabel
    Left = 152
    Top = 66
    Width = 52
    Height = 13
    Caption = #1056#1077#1076#1072#1082#1094#1080#1103':'
  end
  object DTCreateLab: TLabel
    Left = 10
    Top = 95
    Width = 64
    Height = 13
    Caption = 'DTCreateLab'
  end
  object NameED: TcxTextEdit
    Left = 95
    Top = 8
    TabOrder = 0
    Width = 225
  end
  object AuthorED: TcxTextEdit
    Left = 94
    Top = 35
    TabOrder = 1
    Width = 226
  end
  object VerED: TcxSpinEdit
    Left = 94
    Top = 62
    Properties.AssignedValues.MinValue = True
    Properties.MaxValue = 50.000000000000000000
    TabOrder = 2
    Value = 1
    Width = 47
  end
  object RelED: TcxSpinEdit
    Left = 223
    Top = 62
    Properties.MaxValue = 9999.000000000000000000
    TabOrder = 3
    Width = 47
  end
  object BtnPanel: TPanel
    Left = 0
    Top = 111
    Width = 326
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 4
    DesignSize = (
      326
      41)
    object ApplyBtn: TcxButton
      Left = 164
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      OnClick = ApplyBtnClick
    end
    object CancelBtn: TcxButton
      Left = 245
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
end
