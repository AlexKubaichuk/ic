/*
  ����        - IGroup.cpp
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� ��������
                ������� ������������� �������������� ������
                ( ������������ ����������� TICSDocFilter )
  ����������� - �������� �.�.
  ����        - 02.09.2004
*/
//---------------------------------------------------------------------------
/*
*/
//---------------------------------------------------------------------------

#ifndef IGroupH
#define IGroupH

//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxCheckListBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
//---------------------------------------------------------------------------
//#include "ICSDoc.h"  //��������� �������� � ���� ��������
#include "XMLContainer.h"
#include "ICSFilter.h"
//---------------------------------------------------------------------------
namespace IGroup
{

//�����-������ ������������� �������������� ������

class PACKAGE TIGroupKonkrForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *pBottom;
        TActionList *ActionList;
        TAction *actSelAll;
        TAction *actClearSel;
        TAction *actInvSel;
        TcxCheckListBox *clbElements;
        TcxButton *ApplyBtn;
        TcxButton *IgnoreBtn;
        TcxButton *CancelBtn;
        TPopupMenu *PopupMenu1;
        TMenuItem *N1;
        TMenuItem *N2;
        TMenuItem *N3;
        void __fastcall actSelAllExecute(TObject *Sender);
        void __fastcall actClearSelExecute(TObject *Sender);
        void __fastcall actInvSelExecute(TObject *Sender);
        void __fastcall ApplyBtnClick(TObject *Sender);
        void __fastcall clbElementsClickCheck(TObject *Sender, int AIndex,
          TcxCheckBoxState APrevState, TcxCheckBoxState ANewState);


private:	// User declarations
        bool __fastcall AreInputsCorrect();

        TDTDTagNode* FISec;        //���������������� �������������� ������
        TTagNode* FNmkl;        //�����������
        bool FIsFilter;         //������� �������������� �������


public:		// User declarations
        __fastcall TIGroupKonkrForm(TComponent* Owner, TDTDTagNode* ISection, TTagNode* ANmkl, bool AIsFilter );

        __property TDTDTagNode* ISec = { read = FISec };
};

//***************************************************************************
//********************** ��������� ���������� ������� ***********************
//***************************************************************************


} //end of namespace IGroup
using namespace IGroup;

#endif
