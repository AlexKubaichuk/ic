//---------------------------------------------------------------------------


#pragma hdrstop

#include "ICSFilter.h"
#include <Dialogs.hpp>

#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall EICSFilterError::EICSFilterError(const UnicodeString Msg)
: Sysutils::Exception(Msg)
{
}
//---------------------------------------------------------------------------
__fastcall EICSCondNodeError::EICSCondNodeError(const UnicodeString Msg)
: Sysutils::Exception(Msg)
{
}
//---------------------------------------------------------------------------
__fastcall EICSDTDError::EICSDTDError(const UnicodeString Msg)
: Sysutils::Exception(Msg)
{
}
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#                        TICSDTDChecker                                       #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
__fastcall TICSDTDChecker::TICSDTDChecker()
: TTagNode(NULL)
{
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TICSDTDChecker::FCheckNode(UnicodeString AParentName, UnicodeString AName)
{
  TTagNode *RC = NULL;
  try
   {
     int idx;
     UnicodeString pUID, cUID;
     TAnsiStrMap::iterator FRC;
     // UID ��������
     FRC = FNameUIDMap.find(AParentName);
     if (FRC == FNameUIDMap.end())
      throw EICSDTDError("������������ ��� ������������� ���� ("+AParentName+").");
     pUID = FRC->second;
     // UID ������������
     FRC = FNameUIDMap.find(AName);
     if (FRC == FNameUIDMap.end())
      throw EICSDTDError("������������ ��� ���� ("+AParentName+").");
     cUID = FRC->second;
     TTagNode *FParentDef = GetTagByUID(pUID);
     if (!FParentDef)
      throw EICSDTDError("������������ ��� ������������� ���� ("+AParentName+").");
     if (!AName.Length())
      throw EICSDTDError("������������ ��� ���� ("+AName+").");
     if (!StrInList(FParentDef->AV["childs"], AName, ',', false))
      throw EICSDTDError("��� "+AName+" �� �������� ����������� ����� ��� ���� ("+AParentName+").");
     TTagNode *FChildDef = GetTagByUID(cUID);
     if (!FChildDef)
      throw EICSDTDError("������������ ��� ���� ("+AParentName+").");
     RC = FChildDef;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TICSDTDChecker::DTDCHAddChild(TTagNode *AParent, UnicodeString AName, UnicodeString AAttrList)
{
  TTagNode *RC = NULL;
  try
   {
     int idx;
     TTagNode *FChildDef = FCheckNode(AParent->Name,AName);
     if (FChildDef)
      {
        TTagNode *FCh = AParent->AddChild(AName);
        TStringList *FAddAttrList = NULL;
        try
         {
           FAddAttrList = new TStringList;
           FAddAttrList->Delimiter = ',';
           FAddAttrList->Clear();
           if (AAttrList.Trim().Length())
            FAddAttrList->DelimitedText = AAttrList.Trim();
           TTagNode *itAttr = FChildDef->GetFirstChild();
           while (itAttr)
            {
              FCh->AV[itAttr->AV["name"]] = itAttr->AV["def"];
              if (FAddAttrList->Count)
               {
                 idx = FAddAttrList->IndexOfName(itAttr->AV["name"]);
                 if (idx != -1)
                  FCh->AV[itAttr->AV["name"]] = GetPart2(FAddAttrList->Strings[idx],'=');
               }
              itAttr = itAttr->GetNext();
            }
         }
        __finally
         {
           if (FAddAttrList) delete FAddAttrList;
         }
        RC = FCh;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TICSDTDChecker::SwapTo(TTagNode *ANode, UnicodeString ANewNodeName)
{
  TTagNode *RC = NULL;
  try
   {
     int idx;
     TTagNode *ndParent = ANode->GetParent();
     if (ndParent)
      {
        TTagNode *FChildDef = FCheckNode(ndParent->Name,ANewNodeName);
        if (FChildDef)
         {
           ANode->Name = FChildDef->AV["name"];
           TTagNode *itNode = ANode->GetFirstChild();
           TTagNode *tmpNode;
           while (itNode)
            {
              if (FCheckNode(ANode->Name,itNode->Name))
               itNode = itNode->GetNext();
              else
               {
                 tmpNode = itNode->GetNext();
                 delete itNode;
                 itNode = tmpNode;
               }
            }
           try
            {
              TTagNode *itAttr = FChildDef->GetFirstChild();
              UnicodeString FAttrList = "";
              while (itAttr)
               {
                 if (!FAttrList.Length()) FAttrList =  itAttr->AV["name"];
                 else                     FAttrList += ","+itAttr->AV["name"];
                 if (!ANode->GetAttrByName(itAttr->AV["name"]))
                  ANode->AV[itAttr->AV["name"]] = itAttr->AV["def"];
                 itAttr = itAttr->GetNext();
               }
              TAttr *tmpAttr;
              for (int i = 0; i < ANode->AttrCount; i++)
               {
                 tmpAttr = ANode->Attrs(i);
                 if (!tmpAttr->Name.Length())
                  throw EICSDTDError("������������ ��� ���� ("+tmpAttr->Name+").");
                 if (!StrInList(FAttrList, tmpAttr->Name, ',', false))
                  {
                    ANode->DeleteAttr(tmpAttr->Name);
                    i--;
                  }
               }
            }
           __finally
            {
            }
           RC = ANode;
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TICSDTDChecker::DTDCHInsert(TTagNode *ANode, UnicodeString AName, bool ABefore)
{
  TTagNode *RC = NULL;
  try
   {
     int idx;
     if (!ANode->GetParent())
      throw EICSDTDError("������ ������� ���� ("+AName+").");
     TTagNode *FChildDef = FCheckNode(ANode->GetParent()->Name,AName);
     if (FChildDef)
      {
        TTagNode *FCh = ANode->Insert(AName,ABefore);
        TTagNode *itAttr = FChildDef->GetFirstChild();
        while (itAttr)
         {
           FCh->AV[itAttr->AV["name"]] = itAttr->AV["def"];
           itAttr = itAttr->GetNext();
         }
        RC = FCh;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TICSDTDChecker::CheckRef(TTagNode *ANode, UnicodeString ANomGUI)
{
  bool RC = true;
  try
   {
     if (AttrExist(ANode, "ref"))
      if (!ANode->CmpName("text")) /* TODO -okab -cbagfix : ����� ���� �� ��������, �� ���� ���� ������� ICSNom */
        if (_GUI(ANode->AV["ref"]).UpperCase() != ANomGUI.UpperCase())
          RC = false;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TICSDTDChecker::CanAddChild(UnicodeString AParentName, UnicodeString AName)
{
  bool RC = false;
  try
   {
     UnicodeString pUID, cUID;
     TAnsiStrMap::iterator FRC;
     // UID ��������
     FRC = FNameUIDMap.find(AParentName);
     if (FRC != FNameUIDMap.end())
      {
        pUID = FRC->second;
        // UID ������������
        FRC = FNameUIDMap.find(AName);
        if (FRC != FNameUIDMap.end())
         {
           TTagNode *FParentDef = GetTagByUID(pUID);
           if (FParentDef)
            {
              if (!AName.Length())
               throw EICSDTDError("������������ ��� ���� ("+AName+").");
              if (StrInList(FParentDef->AV["childs"], AName, ',', false))
               {
                 if (GetTagByUID(FRC->second))
                   RC = true;
               }
            }
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TICSDTDChecker::TagExist(TTagNode* ATag)
{
  return (bool)GetTag(ATag->Name);
}
//---------------------------------------------------------------------------
bool __fastcall TICSDTDChecker::TagExist(UnicodeString AName)
{
  return (bool)GetTag(AName);
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TICSDTDChecker::GetTag(UnicodeString AName)
{
  TTagNode *RC = NULL;
  try
   {
     TAnsiStrMap::iterator FRC = FNameUIDMap.find(AName);
     if (FRC != FNameUIDMap.end())
      RC = GetTagByUID(FRC->second);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TICSDTDChecker::CanAddChild(TTagNode *AParent, UnicodeString AName)
{
  return CanAddChild(AParent->Name, AName);
}
//---------------------------------------------------------------------------
bool __fastcall TICSDTDChecker::AttrExist(TTagNode *ANode, UnicodeString AAttrName)
{
  return AttrExist(ANode->Name, AAttrName);
}
//---------------------------------------------------------------------------
bool __fastcall TICSDTDChecker::AttrExist(UnicodeString ANodeName, UnicodeString AAttrName)
{
  bool RC = false;
  try
   {
     TTagNode *FNode = GetTag(ANodeName);
     if (FNode)
      {
        if (FNode->GetChildByAV("","name",AAttrName))
         RC = true;
      }
     else
      throw EICSDTDError("��� '"+ANodeName+"' ����������� � ������ ���������� �����.");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TICSDTDChecker::CheckAttrVal(TTagNode *ANode, UnicodeString AAttrName,UnicodeString AAttrValue)
{
  return CheckAttrVal(ANode->Name, AAttrName, AAttrValue);
}
//---------------------------------------------------------------------------
bool __fastcall TICSDTDChecker::CheckAttrVal(UnicodeString ANodeName, UnicodeString AAttrName,UnicodeString AAttrValue)
{
  bool RC = false;
  try
   {
     TTagNode *FNode = GetTag(ANodeName);
     if (FNode)
      {
        TTagNode *FAttrDef = FNode->GetChildByAV("","name",AAttrName);
        if (FAttrDef)
         {
           if (FAttrDef->AV["values"].Length())
            {
              if (!AAttrValue.Length())
               throw EICSDTDError("������������ ��� ���� ("+AAttrValue+").");
              if (StrInList(FAttrDef->AV["values"], AAttrValue, ',', false))
               RC = true;
              else
               throw EICSDTDError("�������� �������� '"+AAttrName+"' ����� �� ���������� �������� '"+AAttrValue+"', ("+FAttrDef->AV["values"]+").");
            }
           else
            RC = true;
         }
        else
         throw EICSDTDError("������� '"+AAttrName+"' ����������� � ������ ��������� ���� "+ANodeName+".");
      }
     else
      throw EICSDTDError("��� '"+ANodeName+"' ����������� � ������ ���������� �����.");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSDTDChecker::FFSetXML(UnicodeString AXML)
{
  TTagNode::AsXML = AXML; FCreateNames();
}
//---------------------------------------------------------------------------
void __fastcall TICSDTDChecker::Assign(TTagNode *Src, bool IsRecurse)
{
  TTagNode::Assign(Src, IsRecurse); FCreateNames();
}
//---------------------------------------------------------------------------
bool __fastcall TICSDTDChecker::LoadFromFile(UnicodeString  FileName)
{
  bool RC = TTagNode::LoadFromFile(FileName); FCreateNames(); return RC;
}
//---------------------------------------------------------------------------
void __fastcall TICSDTDChecker::LoadFromXMLFile(UnicodeString AName)
{
  TTagNode::LoadFromXMLFile(AName); FCreateNames();
}
//---------------------------------------------------------------------------
void __fastcall TICSDTDChecker::LoadFromZIPXMLFile(UnicodeString AName)
{
  TTagNode::LoadFromZIPXMLFile(AName); FCreateNames();
}
//---------------------------------------------------------------------------
void __fastcall TICSDTDChecker::FCreateNames()
{
  FNameUIDMap.clear();
  TTagNode *it = this->GetFirstChild();
  while (it)
   {
     FNameUIDMap[it->AV["name"]] = it->AV["uid"];
     it = it->GetNext();
   }
}
//---------------------------------------------------------------------------
//void __fastcall TICSDTDChecker::FFSetZIPXML(UnicodeString AXML)
//{
//  TTagNode::AsZIPXML = AXML;
//  FCreateNames();
//}
//---------------------------------------------------------------------------

//##############################################################################
//#                                                                            #
//#                        TDTDTagNode                                        #
//#                                                                            #
//##############################################################################

__fastcall TDTDTagNode::TDTDTagNode(TDTDTagNode *AParent)
:TTagNode(AParent)
{
}
//---------------------------------------------------------------------------
__fastcall TDTDTagNode::TDTDTagNode()
                        : TTagNode(NULL)
{
}
//---------------------------------------------------------------------------
TDTDTagNode* __fastcall TDTDTagNode::GetParent()
{
  return (TDTDTagNode*)(TTagNode::GetParent());
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TDTDTagNode::FGetNomGUI()
{
  try
   {
     TICSFilter *tmp = (TICSFilter*)GetRoot();
     if (tmp)
      {
        return tmp->NomGUI;
      }
     else
      throw EICSCondNodeError("DTD not found, code=1.");
   }
  __finally
   {
   }
  return "";
}
//---------------------------------------------------------------------------
TICSDTDChecker* __fastcall TDTDTagNode::FGetDTD()
{
  TICSDTDChecker *RC = NULL;
  try
   {
     TICSFilter *tmp = (TICSFilter*)GetRoot();
     if (tmp)
      {
        RC = tmp->DTD;
        if (RC)
         {
           if (!RC->ClassNameIs("TICSDTDChecker"))
            {
              RC = NULL;
              throw EICSCondNodeError("DTD not found, code=3.");
            }
         }
      }
     else
      throw EICSCondNodeError("DTD not found, code=1.");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TDTDNodeCheckRef __fastcall TDTDTagNode::GetOnCheckNodeRef()
{
  TDTDNodeCheckRef RC = NULL;
  try
   {
     TICSFilter *tmp = (TICSFilter*)GetRoot();
     if (tmp)
      {
        RC = tmp->OnCheckNodeRef;
      }
     else
      throw EICSCondNodeError("OnCheckNodeRef event not found, code=112.");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TDTDNodeCopy __fastcall TDTDTagNode::GetOnCopyNode()
{
  TDTDNodeCopy RC = NULL;
  try
   {
     TICSFilter *tmp = (TICSFilter*)GetRoot();
     if (tmp)
      {
        RC = tmp->OnCopyNode;
      }
     else
      throw EICSCondNodeError("OnCopyNode event not found, code=12.");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TDTDTagNode* __fastcall TDTDTagNode::DTDAddChild()
{
   if (!this) throw EICSCondNodeError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (this->FChild == NULL)
    {
      this->FChild = new TDTDTagNode(this);
      this->FLastChild = this->FChild;
    }
   else
    {
      ((TDTDTagNode*)this->FLastChild)->FNext = new TDTDTagNode(this);
      ((TDTDTagNode*)((TDTDTagNode*)this->FLastChild)->FNext)->FPrev = this->FLastChild;
      this->FLastChild = ((TDTDTagNode*)this->FLastChild)->FNext;
    }
   this->FCount++;
   return ((TDTDTagNode*)this->FLastChild);
}
//---------------------------------------------------------------------------
TDTDTagNode* __fastcall TDTDTagNode::SwapTo(UnicodeString ANewNodeName)
{
  TDTDTagNode *RC = NULL;
  try
   {
      RC = (TDTDTagNode*)DTD->SwapTo(this,ANewNodeName);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TDTDTagNode* __fastcall TDTDTagNode::DTDAddChild(UnicodeString AName, TTagNode *ASrc)
{
  TDTDTagNode *RC = NULL;
  try
   {
      RC = (TDTDTagNode*)DTD->DTDCHAddChild(this,AName,"");
//      RC->NomGUI = FNomGUI;
      if (ASrc) RC->Copy(ASrc);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TDTDTagNode* __fastcall TDTDTagNode::DTDAddChild(UnicodeString AName, UnicodeString AAttrs, TTagNode *ASrc)
{
  TDTDTagNode *RC = NULL;
  try
   {
      RC = (TDTDTagNode*)DTD->DTDCHAddChild(this,AName,AAttrs);
//      RC->NomGUI = FNomGUI;
      if (ASrc) RC->Copy(ASrc);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TDTDTagNode* __fastcall TDTDTagNode::DTDInsert(UnicodeString AName, bool ABefore, TTagNode *ASrc)
{
  TDTDTagNode *RC = NULL;
  try
   {
      RC = (TDTDTagNode*)DTD->DTDCHInsert(this,AName,ABefore);
//      RC->NomGUI = FNomGUI;
      if (ASrc) RC->Copy(ASrc);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TDTDTagNode* __fastcall TDTDTagNode::InsertEx(bool ABefore, UnicodeString ASrc)
{
  TDTDTagNode *RC = NULL;
  TTagNode *tmpNode = NULL;
  try
   {
     tmpNode = new TTagNode(NULL);
     tmpNode->AsXML = ASrc;
     if (DTD->CanAddChild(this->GetParent()->Name, tmpNode->Name))
      {
        if (this->Check(tmpNode))
         {
           RC = (TDTDTagNode*)DTD->DTDCHInsert(this,tmpNode->Name,true);
           RC->AssignEx(tmpNode,true);
         }
      }
   }
  __finally
   {
     if (tmpNode) delete tmpNode;
   }
  return RC;
}
//---------------------------------------------------------------------------
TDTDTagNode* __fastcall TDTDTagNode::AddChildEx(TTagNode *ASrc)
{
  TDTDTagNode *RC = NULL;
  try
   {
     if (DTD->CanAddChild(this->Name, ASrc->Name))
      {
        if (this->Check(ASrc))
         {
           RC = (TDTDTagNode*)DTD->DTDCHAddChild(this,ASrc->Name,"");
           RC->AssignEx(ASrc,true);
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TDTDTagNode* __fastcall TDTDTagNode::InsertEx(bool ABefore, TTagNode *ASrc)
{
  TDTDTagNode *RC = NULL;
  try
   {
     if (DTD->CanAddChild(this->GetParent()->Name, ASrc->Name))
      {
        if (this->Check(ASrc))
         {
           RC = (TDTDTagNode*)DTD->DTDCHInsert(this,ASrc->Name,true);
           RC->AssignEx(ASrc,true);
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TDTDTagNode::CheckPaste(TTagNode *ASrc, UnicodeString ADestTagName)
{
  bool RC = false;
  try
   {
     try
      {
        if (!ADestTagName.Trim().Length())
         {
           if (DTD->CanAddChild(this->GetParent()->Name, ASrc->Name))
             RC = this->Check(ASrc);
         }
        else
         {
           if (DTD->CanAddChild(ADestTagName.Trim(), ASrc->Name))
             RC = this->Check(ASrc);
         }
      }
     catch (...)
      {
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TDTDTagNode::Check(TTagNode *ASrc)
{
  bool RC = true;
  try
   {
      TTagNode *iNode = ASrc->GetFirstChild();
      if (ASrc->GetParent())
       RC &= this->DTD->CanAddChild(ASrc->GetParent()->Name,ASrc->Name);
      RC &= this->DTD->CheckRef(ASrc,NomGUI);
      if (RC)
       RC &= CheckNodeRef(ASrc);
      while (iNode&&RC)
       {
         RC =  this->Check(iNode);
         iNode = iNode->GetNext();
       }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TDTDTagNode* __fastcall TDTDTagNode::DTDInsertChild(TTagNode* AInsTag, UnicodeString AName, bool ABefore, TTagNode *ASrc)
{
  TDTDTagNode *RC = NULL;
  try
   {
      throw EICSCondNodeError("�� �����������.");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TDTDTagNode::Copy(TTagNode* ASrc)
{
  TDTDNodeCopy ndCopyEvt = GetOnCopyNode();
  if (ndCopyEvt)
   ndCopyEvt(ASrc, this);
}
//---------------------------------------------------------------------------
bool __fastcall TDTDTagNode::CheckNodeRef(TTagNode* ASrc)
{
  bool RC = false;
  try
   {
     TDTDNodeCheckRef ndCheckRefEvt = GetOnCheckNodeRef();
     if (ndCheckRefEvt)
      RC = ndCheckRefEvt(ASrc);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TDTDTagNode::FGetAV(UnicodeString AName)
{
  if (DTD->AttrExist(this, AName))
   return TTagNode::AV[AName];
  else
   return "";
}
//---------------------------------------------------------------------------
void __fastcall TDTDTagNode::FSetAV(UnicodeString AName, UnicodeString AVal)
{
  if (DTD->CheckAttrVal(this, AName, AVal))
   TTagNode::AV[AName] = AVal;
}
//---------------------------------------------------------------------------
/*
<?xml version='1.0' encoding='windows-1251'?>

<!-- ������. ������ 2.0.0 �� 04.02.2005. -->

<!ENTITY % allitems    ' '>
<!ENTITY % req         'CDATA #REQUIRED'>
<!ENTITY % imp         'CDATA #IMPLIED'>
<!ENTITY % title       'uid %req; name %req;'>

<!-- ������ -->

<!ELEMENT region  (passport,condrules)>

<!-- 1. ������� -->
<!--
     GUI       - ���
     mainname  - ������������
     autor     - �����
     version   - ������
     release   - ����������
     timestamp - ���� � ����� ��������
-->
<!ELEMENT passport EMPTY>
<!ATTLIST passport GUI       %req;
                   mainname  %req;
                   autor     %req;
                   version   %req;
                   release   %req;
                   timestamp %req;>

<!-- 2. ������� ������ -->
<!ELEMENT condrules (IERef|IAND|IOR|IMOR|RAND|ROR)+>
<!ATTLIST condrules uniontype (or|and) 'or'>

<!-- 2.1. ������ �� �������������� ������� -->
<!ELEMENT IERef (text)*>
<!ATTLIST IERef uid        %req;
                ref        %req;      - ���.��� ��������������� �������� ������ ������ �������� ������������
                not        (1|0) '0'  - ������� ���������
                incltoname (1|0) '0'> - ������� ������������� �������� �������� ��������� ������ � ������������ �������

<!-- 2.2. �������������� ������ "�" ("��� ��") -->
<!ELEMENT IAND ( IERef|IAND|IOR|IMOR )+>
<!ATTLIST IAND 'uid %req;
                name %req;'
               not        (1|0) '0'
               uniontype  (or|and) 'and' - ��� ������������� or  - � ���������� ������������� ������ ����������������� � ������ ROR (pure = 0) and - � ���������� ������������� ������ ����������������� � ������ RAND (pure = 0)
               incltoname (1|0) '0'>     - ������� ������������� �������� �������� ��������� ������ � ������������ �������

<!-- 2.3. �������������� ������ "���" ("���� ��") -->
<!ELEMENT IOR  ( IERef|IAND|IOR|IMOR )+>
<!ATTLIST IOR 'uid %req; name %req;'
              not        (1|0) '0'  - ������� ���������
              incltoname (1|0) '0'> - ������� ������������� �������� �������� ��������� ������ � ������������ �������

<!-- 2.4. �������������� ������ "����" ("��������� ��") -->
<!ELEMENT IMOR ( IERef|IAND|IOR|IMOR )+>
<!ATTLIST IMOR 'uid %req; name %req;'
               not        (1|0) '0'      - ������� ���������
               uniontype  (or|and) 'and' - ��� ������������� or  - � ���������� ������������� ������ ����������������� � ������ ROR (pure = 0) and - � ���������� ������������� ������ ����������������� � ������ RAND (pure = 0)
               incltoname (1|0) '0'> - ������� ������������� �������� �������� ��������� ������ � ������������ �������

<!-- 2.5. ���������� ������ "�" / ������������������ �������������� ������ -->
<!ELEMENT RAND (IERef|IAND|IOR|IMOR|RAND|ROR)+>
<!ATTLIST RAND not     (1|0) '0' - ������� ���������
               pure    (1|0) '1' - ��� ������  0 - ������������������ �������������� 1 - ����������
               comment %imp;>    - ����������

<!-- 2.6. ���������� ������ "���" / ������������������ �������������� ������ -->
<!ELEMENT ROR (IERef|IAND|IOR|IMOR|RAND|ROR)+>
<!ATTLIST ROR not     (1|0) '0' - ������� ���������
              pure    (1|0) '1' - ��� ������  0 - ������������������ �������������� 1 - ����������
              comment %imp;>    - ����������

<!-- 2.7. ��������� ������� -->
<!ELEMENT text EMPTY>
<!ATTLIST text name  %req;
               value %req;>
*/

/*
<?xml version='1.0' encoding='windows-1251'?>
<region>
 <passport GUI='0B390137-00007DD9-2BA9' mainname='���������� ������ �� ...' autor='����� �������' version='1' release='0' timestamp='20060314171625' defversion='2.0.1'/>
 <condrules uniontype='and'>
  <RAND not='0' pure='1' comment=''>
   <IERef uid='0001' ref='0E291426-00005882-2493.004F' not='0' incltoname='0'/>
   <IERef uid='0002' ref='0E291426-00005882-2493.004E' not='0' incltoname='0'/>
  </RAND>
 </condrules>
</region>*/
//##############################################################################
//#                                                                            #
//#                        TDTDTagNode                                        #
//#                                                                            #
//##############################################################################

//---------------------------------------------------------------------------
__fastcall TICSFilterPassport::TICSFilterPassport(TTagNode *AParent):TTagNode(AParent)
{
  if (((pTPassp)AParent)->FChild == NULL)
   {
     ((pTPassp)AParent)->FChild = this;
     ((pTPassp)AParent)->FLastChild = this;
   }
  else
   {
     ((pTPassp)((pTPassp)AParent)->FLastChild)->FNext = this;
     ((pTPassp)((pTPassp)((pTPassp)AParent)->FLastChild)->FNext)->FPrev = ((pTPassp)AParent)->FLastChild;
     ((pTPassp)AParent)->FLastChild = ((pTPassp)((pTPassp)AParent)->FLastChild)->FNext;
   }
  ((pTPassp)AParent)->FCount++;

  this->New();
}
//---------------------------------------------------------------------------
void __fastcall TICSFilterPassport::New()
{
  this->Name = "passport";
  this->AV["GUI"] = NewGUI();
  this->AV["mainname"] = "����� ������";
  this->AV["autor"] = "";
  this->AV["version"] = "1";
  this->AV["release"] = "0";
  this->AV["timestamp"] = (Date()+Time()).FormatString("yyyymmddhhnnss");
  this->AV["defversion"] = "3.0.0";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSFilterPassport::FGetGUI()
{
  return this->AV["GUI"];
}
//---------------------------------------------------------------------------
void __fastcall TICSFilterPassport::FSetGUI(UnicodeString ASrc)
{
  this->AV["GUI"] = ASrc;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSFilterPassport::FGetName()
{
  return this->AV["mainname"];
}
//---------------------------------------------------------------------------
void __fastcall TICSFilterPassport::FSetName(UnicodeString ASrc)
{
  this->AV["mainname"] = ASrc;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSFilterPassport::FGetAuthor()
{
  return this->AV["autor"];
}
//---------------------------------------------------------------------------
void __fastcall TICSFilterPassport::FSetAuthor(UnicodeString ASrc)
{
  this->AV["autor"] = ASrc;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSFilterPassport::FGetVersion()
{
  return this->AV["version"];
}
//---------------------------------------------------------------------------
void __fastcall TICSFilterPassport::FSetVersion(UnicodeString ASrc)
{
  this->AV["version"] = ASrc;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSFilterPassport::FGetRelease()
{
  return this->AV["release"];
}
//---------------------------------------------------------------------------
void __fastcall TICSFilterPassport::FSetRelease(UnicodeString ASrc)
{
  this->AV["release"] = ASrc;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSFilterPassport::FGetTimestamp()
{
  return this->AV["timestamp"];
}
//---------------------------------------------------------------------------
void __fastcall TICSFilterPassport::FSetTimestamp(UnicodeString ASrc)
{
  this->AV["timestamp"] = ASrc;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TICSFilterPassport::FGetDefVersion()
{
  return this->AV["defversion"];
}
//---------------------------------------------------------------------------
void __fastcall TICSFilterPassport::FSetDefVersion(UnicodeString ASrc)
{
  this->AV["defversion"] = ASrc;
}
//---------------------------------------------------------------------------
__fastcall TICSFilter::TICSFilter()
                      :TTagNode(NULL)
{
  FOnCopyNode = NULL;
  Name = "region";
  FPassport = new TICSFilterPassport(this);
  FSource   = FCreateCondrules();
  FSource->Name = "condrules";
  FDTD = new TICSDTDChecker;
//  FSource->DTD = FDTD;
  FNomGUI = "";
}
//---------------------------------------------------------------------------
__fastcall TICSFilter::~TICSFilter()
{
  delete FSource;
  delete FPassport;
  delete FDTD;
}
//---------------------------------------------------------------------------
void __fastcall TICSFilter::FSetNomGUI(UnicodeString AGUI)
{
  FNomGUI = AGUI;
}
//---------------------------------------------------------------------------
void __fastcall TICSFilter::FSetDTD(TICSDTDChecker *ASrc)
{
  FDTD->Assign(ASrc,true);
}
//---------------------------------------------------------------------------
TDTDTagNode* __fastcall TICSFilter::FCreateCondrules()
{
   if (this->FChild == NULL)
    {
      this->FChild = new TDTDTagNode((TDTDTagNode*)this);
      this->FLastChild = this->FChild;
    }
   else
    {
      ((TICSFilter*)this->FLastChild)->FNext = new TDTDTagNode((TDTDTagNode*)this);
      ((TICSFilter*)((TICSFilter*)this->FLastChild)->FNext)->FPrev = this->FLastChild;
      this->FLastChild = ((TICSFilter*)this->FLastChild)->FNext;
    }
   this->FCount++;
   return ((TDTDTagNode*)this->FLastChild);
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TICSFilter::FGetCondition()
{
  return (TTagNode*)FSource;
}
//---------------------------------------------------------------------------
void __fastcall TICSFilter::FSetCondition(TTagNode *ASrc)
{
  FSource->Assign((TDTDTagNode*)ASrc,true);
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TICSFilter::FGetFilter()
{
  return (TTagNode*)this;
}
//---------------------------------------------------------------------------
void __fastcall TICSFilter::FSetFilter(TTagNode *ASrc)
{
  TTagNode *FCond = ASrc->GetChildByName("condrules");
  if (FCond)
   {
     FSource->Assign((TDTDTagNode*)FCond,true);
     TTagNode *FPasp = ASrc->GetChildByName("passport");
     if (FPasp)
      {
        FPassport->Assign(FPasp,false);
      }
     else
      FPassport->New();
   }
}
//---------------------------------------------------------------------------

