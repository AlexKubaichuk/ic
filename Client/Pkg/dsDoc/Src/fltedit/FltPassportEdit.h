//---------------------------------------------------------------------------

#ifndef FltPassportEditH
#define FltPassportEditH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxSpinEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
#include "ICSFilter.h"
//---------------------------------------------------------------------------
class TFilterPassportEditForm : public TForm
{
__published:	// IDE-managed Components
        TcxTextEdit *NameED;
        TcxTextEdit *AuthorED;
        TcxSpinEdit *VerED;
        TcxSpinEdit *RelED;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *DTCreateLab;
        TPanel *BtnPanel;
        TcxButton *ApplyBtn;
        TcxButton *CancelBtn;
        void __fastcall ApplyBtnClick(TObject *Sender);
private:	// User declarations
        TICSFilterPassport* FPasp;
public:		// User declarations
        __fastcall TFilterPassportEditForm(TComponent* Owner, TICSFilterPassport* APasp);
};
//---------------------------------------------------------------------------
extern PACKAGE TFilterPassportEditForm *FilterPassportEditForm;
//---------------------------------------------------------------------------
#endif
