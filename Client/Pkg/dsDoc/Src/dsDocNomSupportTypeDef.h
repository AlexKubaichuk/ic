//---------------------------------------------------------------------------
#ifndef dsDocNomSupportTypeDefH
#define dsDocNomSupportTypeDefH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include "XMLContainer.h"
//---------------------------------------------------------------------------
typedef bool __fastcall (__closure *TdsNomCallDefProcEvent)(System::UnicodeString AProcName, TTagNode* AIENode, bool ANeedKonkr, System::UnicodeString &AText);
//---------------------------------------------------------------------------
#endif

