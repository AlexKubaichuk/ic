/*
  ����        - ICSDocViewer.h
  ������      - ��������������� (ICS)
  ��������    - ����������� ���������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 18.10.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  v1.2.2  - 20.04.2007
    [!] ��� ��������� �������� "���������� ������� ��������" ��������� ��������� "������" � "�������"

  v1.2.1  - 18.04.2007
    [+] �������� UseExcelCSS, ������������ ������������� ��� ������������
        HTML-��������� CSS ��� ������������� ���������� ��������
        � MS Excel 2000/XP/2003

  v1.2.0  - 15.03.2007
    [!] ��� ���������� ����� ������������ ����������� csCreateHTML

  v1.1.9  - 11.01.2007
    [+] �������� �� ������ ������� ������������� ��� ��������
        TICSDocHTMLCreator::Specificator
    [*] �����������

  v1.1.8  - 31.10.2006
    [*] ��������� ������������ �������� � ������ ClearIfNoShowZVal
    [*] ����� ClearIfNoShowZVal ������������ � NormalizeNumericalValue

  v1.1.7  - 16.03.2006
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  v1.1.6  - 06.12.2005
    [!] ��� ���������� ������� ��� ������������ ����������� ���������
        ��� ���������� ������ � ������������ �������

  v1.1.5  - 29.08.2005
    1. ��������� ���� ��� �������� ��������������� ����������� ���������
       � MS Excel

  v1.1.4  - 15.07.2005
    1. ��������� ���� ��� ���������� ������� ��� ������������ ����������� ���������
       ��� ���������� ������

  v1.1.3  - 12.07.2005 - 13.07.2005
    1. ��������� ������ �� ���������� - ���� API-������� GetTempFileName ������������ � �� ������
       ������� ���������� ���, �� ��� �, ������ ��� ��� ��������� ������ ��������� �� ���������
       ���������� �������
    2. �������� ���������� ������� ��� ������������ ����������� ���������
    3. ��������� ����, ��������� � ������������ ������ ������� ���� listcol(link_is(is_attr+)) �
       ������������� ��������� ����������� �������

  v1.1.2  - 23.06.2005
    1. ��������� public-�������� MSWordTemplate � ICSWordTemplateFN
    2. ��������� ����������� ������������ ������ ����� ����������
       ������� MS Word
    3. ����������:
       ���������� ������� ��� ������������ ����������� ���������

  v1.1.1  - 21.06.2005
    1. ��� ������� ���������������� ��������� ���� pmExternalDlgExcel �����
       �������� ������ �������� ���������� ���������, ���� Excel �� ���� �� ��������� ��� ��������

  v1.1.0  - 08.06.2005
    1. ������ ��� ������� ��������������� ����������� � ������� HTML ���������
       ��������� ��������� ���� � ���������� ������
    2. ������� public-�������� TmpFileName

  v1.0.9  - 03.06.2005
    1. ��������� ��������� specificator.dtd ������ 2.1.3
    2. �������� ��� ������� ���������������� ��������� pmExternalDlgWord

  v1.0.8  - 24.05.2005
    1. �������� ��� ������� ���������������� ��������� pmExternalDlgExcel
    2. ��������� ���� � ������������� ������ �������� �������

  v1.0.7  - 04.04.2005
    1. ���������� ������ DKSetVersion �������� �� ������� �������� ������
    2. ��������� ��������� ��������� ����� ������ (xml-��� "list" �������������)
       ��� ���������� TICSDocHTMLCreator

  v1.0.6  - 03.03.2005
    1. ��������� ��� � ������ TICSDocHTMLCreator::AddHTMLInsText - ���������
       ������� ������� ������ ����������� � ����� ������

  v1.0.5  - 25.10.2004
    1. ��������� __published-�������� PreviewDlgIcon

  v1.0.4  - 25.10.2004
    1. IESetup ������ ��������������� ����� � ������� � �����������
       �������� Internet Explorer'� ������ ���� ��� ����� ��������
       ��������, ���������� ���� IESetup'��
    2. ������ �������� "���������" ��� �������������� HTML-����� � ������
    3. � ������ PreviewDlgKind == pmSystemDlg ��� ������������ �����������
       � ������� HTML IESetup ��������� �������� Internet Explorer'� �� �����������

  v1.0.3  - 24.10.2004
    1. ��������� __published-�������� IESetup

  v1.0.2  - 23.10.2004
    1. ��������� ����: ��� ������������ HTML-���������
       � �������� title'� ������ ���������� mainname �� �������� �������������.
       ������ ��� ������������ ����������� ������������� ������� ���
       ( ������������� ) mainname, � ��� ������������ ����������� ��������� -
       mainname ���������

  v1.0.1  - 18.10.2004
    1. ���������� ����������� � ������ InternalPreviewCreate()
    2. ��������� �������� DocAssign
    3. ��������� �������� ListZeroValCheckCountOnly

  v1.0    - 18.10.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef ICSDocHandHTMLCreatorH
#define ICSDocHandHTMLCreatorH

//---------------------------------------------------------------------------

#include <SysUtils.hpp>
#include <Classes.hpp>
#include <Registry.hpp>
//#include "DKClasses.h"
#include "XMLContainer.h"
#include "IcsXMLDoc.h"
#include "Preview.h"

#include "ICSDocGlobals.h"

namespace ICSDocViewer
{

//###########################################################################
//##                                                                       ##
//##                                 Globals                               ##
//##                                                                       ##
//###########################################################################

//---------------------------------------------------------------------------

// ��� ������� ��� �����������
enum TICSDocHTMLCreatorPreviewType
{
  ptUnknownEx,            // �� ������ ����, ���� �� ���� ����� :)
  ptSpecificatorEx,       // ���������� ������������
  ptDocumentEx            // ���������� ��������
};

//---------------------------------------------------------------------------

//###########################################################################
//##                                                                       ##
//##                               TICSDocHTMLCreator                         ##
//##                                                                       ##
//###########################################################################

// ����������� ���������

class PACKAGE TICSDocHTMLCreator : public TObject
{
private:
    TTagNode*              FSpec;           //������������ ( xml-������ � ������ "specificator" )
    TTagNode*              FDoc;            //�������� ( xml-������ � ������ "docum" )
                                            //� ������ vmSaveToDisk
    UnicodeString             FHTML;           //HTML-���� � ���� ������
    TTagNode*              FndHTML;         //������ html-�����
    TAxeXMLContainer*      FXMLContainer;   //XML-��������� ( ��. AxeUtil.h ) � �������������
    bool                   FListZeroValCheckCountOnly; //������� ���������� �������
                                                       //����������� ������� ��������
                                                       //��� ������ ������ ���
                                                       //��������, ��������������� count'�
    //�������
                                                    //����������� �������������/���������

    //���� ��� ���������� ���� ( ������������ ��� �������� � ������ ������� ����� ������������ �������� )
    UnicodeString    __BorderWidth;    //������ ����� ������� ( xml-��� "table" ������������� )
                                    //��� ������ ( xml-��� "list" ������������� )
    UnicodeString    __Width;
    bool          __RowNumbers;     //������� ������������� ��������� ����� ������� ( xml-��� "table" ������������� )
                                    //��� ����� ������ ( xml-��� "list" ������������� )
    bool          __ColNumbers;     //������� ������������� ��������� �������� ������� ( xml-��� "table" ������������� )
    int           __RowNum;         //������� ����� ������ ������� ( xml-��� "table" ������������� )
    int           __ColNum;         //������� ����� ������� ������� ( xml-��� "table" ������������� )
    //���� ��� ���������� ����
    int        __PBMax;             //������������ ��� �������� ������ � ����� InternalSetPBMax()
    int        __PBPosition;        //������������ ��� �������� ������ � ����� InternalSetPBPosition()
    int           FTabNum;          //������� ����� �������
    int           FTabRowNum;       //������� ����� ������ �������
    int           FTabColNum;       //������� ����� ������� �������

    /*�������������*/
    inline TTagNode* __fastcall InitNDHTML(UnicodeString AClass = "");
    /*��������������� ������*/
    void __fastcall CreateHTMLProgressInit(int AMax, UnicodeString AMsg);
    void __fastcall CreateHTMLProgressChange(int APercent, UnicodeString AMsg);
    void __fastcall CreateHTMLProgressComplite(UnicodeString AMsg);

    //*****************************
    //*****************************
    UnicodeString __fastcall GetTableCellStyle( const UnicodeString &AStyleAttrName, TTagNode *ndCellTagNode, TTagNode *ndAltCellTagNode = NULL );
    UnicodeString __fastcall GetListStyle( const UnicodeString &AStyleAttrName, TTagNode *ndList );
    UnicodeString __fastcall GetInsTextStyle( TTagNode *ndInsText );
    UnicodeString __fastcall GetInsTextAlign( TTagNode *ndInsText );
    void       __fastcall NormalizeNumericalValue(UnicodeString &ACellText, const bool fShowZVal);
    void       __fastcall HiMetricToPixels(long *pnWidth, long *pnHeight);
    /*��������������� ������ ��� ������� HTML*/
    inline TTagNode* __fastcall AddHTMLTableTag( TTagNode *htmlParent, UnicodeString ADop = "" );
    void             __fastcall GetHTML_DIVAlignAttrVal( const UnicodeString &AAlign, UnicodeString &AAttrName, UnicodeString &AAttrVal );
    TTagNode*        __fastcall CreateHTMLTextFormat( TTagNode *htmlTag, const UnicodeString &AStyleUID );
    inline bool      __fastcall SetHTMLTableCellBorderWidth( TTagNode *htmlTableCell, const UnicodeString &ABorderWidth );
    bool             __fastcall SetHTMLBoxWidthValues( TTagNode *htmlTableCell, const UnicodeString &ABorderWidth, const UnicodeString &AWidth );

    TTagNode*        __fastcall CreateHTMLTableNumCellTextFormat( TTagNode *htmlNumTableCell );
    TTagNode*        __fastcall CreateHTMLTableNumColTextFormat( TTagNode *htmlNumTableCell );
    TTagNode*        __fastcall CreateHTMLTableNumRowTextFormat( TTagNode *htmlNumTableCell );


protected:
    /*TICSDocHTMLCreator*/
    bool __fastcall CheckIfTerminated();
    //*****************************
    UnicodeString __fastcall GetListColHeaderText( TTagNode *ndListCol );
    UnicodeString __fastcall GetTableHeaderCellText( TTagNode *ndHeaderCell );
    long __fastcall GetListColsMaxLevel( const TTagNode *ANode ) const;
    int __fastcall GetMaxPBForSpec();
    int __fastcall GetMaxPBForDoc();
    /*��������������� ������ ��� ������� HTML*/
    UnicodeString __fastcall GetHTMLStyleDefinition( const UnicodeString& AStyleClass,
                                                  const UnicodeString& AStyleClassStrPrefix,
                                                  const UnicodeString& AStylePropStrPrefix,
                                                  TTagNode*         ndStyle );
    UnicodeString __fastcall GetHTMLSTYLESDefinition( const UnicodeString& AStyleClassStrPrefix,
                                                   const UnicodeString& AStylePropStrPrefix,
                                                   TTagNode*         ndStyles );
    void __fastcall CreateHTML( TICSDocHTMLCreatorPreviewType APreviewType );
    TTagNode* __fastcall CreateHTMLTableHeaderCell( const UnicodeString& ACellTagNodeName, TTagNode* ndCellTagNode, TTagNode* htmlRow );
    TTagNode* __fastcall CreateHTMLTableHeaderCellEx( const UnicodeString& ACellTagNodeName, TTagNode* ndCellTagNode, TTagNode* htmlRow );
    void __fastcall CreateHTMLTableHeaderCols( TTagNode *ndCol, TTagNode *htmlTable, TTagNode *htmlRow = NULL );
    void __fastcall CreateHTMLTableCols( TTagNode *ndColumns, TTagNode *htmlTable, TTagNodeList *ATableSingleCols = NULL );
    void __fastcall CreateHTMLTableHeadCols( TTagNode *ndColumns, TTagNode *htmlTable, TTagNodeList *ATableSingleCols = NULL );
    void __fastcall CreateHTMLTableDataCols( TTagNode *ndColumns, TTagNode *htmlTable, TTagNodeList *ATableSingleCols = NULL );
    void __fastcall CreateHTMLTableRows( TTagNode *ndRow, TTagNode *htmlTable, const TTagNodeList &ATableSingleCols, TTagNodeList2 *DataCells, TTagNode *htmlRow = NULL );
    void __fastcall CreateHTMLTableDataRows( TTagNode *ndRow, TTagNode *htmlTable, const TTagNodeList &ATableSingleCols, TTagNodeList2 *DataCells, TTagNode *htmlRow = NULL );
    TTagNode* __fastcall AddHTMLInscription( TTagNode *ndInscription, TTagNode *htmlParent );
    TTagNode* __fastcall AddHTMLInsText( TTagNode *ndInsText, TTagNode **htmlParent );
//    TTagNode* __fastcall AddHTMLList( TTagNode *ndList, TTagNode *htmlParent, TTagNodeList *AListSingleCols = NULL, int nRowCount = 1 );
    TTagNode* __fastcall AddHTMLTable( TTagNode *ndTable, TTagNode *htmlParent, TTagNodeList2 *DataCells = NULL );
    TTagNode* __fastcall AddHTMLTableNoHead( TTagNode *ndTable, TTagNode *htmlParent, TTagNodeList2 *DataCells );
//    void __fastcall CreateSpecHTML();
    void __fastcall GetTableCalcRows( TTagNode *ndRow, const TTagNodeList &ATableSingleCols, TStringList *ACalcList);
    void __fastcall CreateSpecFirstTabHTML();
    void __fastcall CreateFirstInscSpecHTML();
    void __fastcall CreateDocFirstTabHTML();
    void __fastcall CreateTabColHeadHTML();
    void __fastcall CreateTabRowHeadHTML();
    void __fastcall CreateFirstInscDocHTML();
    void __fastcall GetTableRowsStr( TTagNode *ARow, TStringList *ARowList, const UnicodeString &ACapt);
    void __fastcall GetTableColsStr( TTagNode *ACol, TStringList *AColList, const UnicodeString &ACapt);

public:
    __fastcall TICSDocHTMLCreator();
    virtual __fastcall ~TICSDocHTMLCreator();

    //������������ ����������� ���������/������������� � ������ �������
    //���������������� ���������
    UnicodeString __fastcall PreviewFirstTab(TAxeXMLContainer*  AXMLContainer, TTagNode*  ASpecificator, TTagNode*  ADocument);
    void       __fastcall GetFirstTabCalcCell(TAxeXMLContainer*  AXMLContainer, TTagNode*  ASpecificator, TStringList*  AList);
    void       __fastcall GetTabHeaderStr(TAxeXMLContainer*  AXMLContainer, TTagNode*  ASpecificator, TStringList*  ARowList, TStringList*  AColList);
    UnicodeString __fastcall PreviewInsc(TAxeXMLContainer*  AXMLContainer, TTagNode*  ASpecificator, TTagNode*  ADocument);
    UnicodeString __fastcall PreviewTabColHead(TAxeXMLContainer*  AXMLContainer, TTagNode*  ASpecificator);
    UnicodeString __fastcall PreviewTabRowHead(TAxeXMLContainer*  AXMLContainer, TTagNode*  ASpecificator);
};

//---------------------------------------------------------------------------

} // end of namespace ICSDocViewer
using namespace ICSDocViewer;

#endif
