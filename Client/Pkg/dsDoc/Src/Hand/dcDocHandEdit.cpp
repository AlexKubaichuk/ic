/*
 ����        - DocHandEdit.cpp
 ������      - ��������������� (ICS)
 ��������    - ���� ���������� ������, ����������� ��������
 ����� ��� ������� ����� ������ � ��������
 ����������� - ������� �.�.
 ����        - 22.11.2004
 */
// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dcDocHandEdit.h"
#include "icsDocConstDef.h"
#include "DKRusMB.h"
#include "ICSDocParsers.h"
#include "msgdef.h"
// #include "LabEdit.hpp"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLabel"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma link "dxBarExtItems"
#pragma link "Htmlview"
#pragma link "cxImage"
#pragma link "dxGDIPlusClasses"
#pragma resource "*.dfm"
namespace Dcdochandedit
 {
 // ***************************************************************************
 // ********************** ���� ***********************************************
 // ***************************************************************************
 // enum TDocElementImageListInd { iiNone = 0, iiInscription, iiInsText, iiList, iiTable };
 // ---------------------------------------------------------------------------
 // ������ �� ������-��������� ������ � ������� ��� ������ �������
 struct TTCellTagNodes
  {
   TTagNode * RowNode; // ������ �� ������-��������� ������
   TTagNode * ColNode; // ������ �� ������-��������� �������
   TCellEDData ED;
   // TcxMaskEdit    *ED;         //������ �� ��������
   // TFormControlObj *FO;
  };
 // ---------------------------------------------------------------------------
 /* struct TTCellTagNodesEx
  {
  TTagNode *RowNode;    //������ �� ������-��������� ������
  TTagNode *ColNode;    //������ �� ������-��������� �������
  }; */
 // ---------------------------------------------------------------------------
 // ������ (���������� ������)
 template<class DHVData>
  class TDHVector
  {
  private:
   DHVData * FVector; // ������
   int FColCount; // ���������� ��������
  protected:
   void __fastcall SetColCount(int AColCount);
  public:
   __fastcall TDHVector(int AColCount = 0);
   virtual __fastcall ~TDHVector();
   __property int ColCount =
    {
     read = FColCount,
     write = SetColCount
    };
   DHVData & __fastcall operator[](int i);
  };
 // ---------------------------------------------------------------------------
 // ������� (��������� ������)
 template<class DHMData>
  class TDHMatrix
  {
  private:
   TDHVector<DHMData> *FMatrix; // �������
   int FRowCount; // ���������� �����
   int FColCount; // ���������� ��������
  public:
   __fastcall TDHMatrix(int ARowCount, int AColCount);
   virtual __fastcall ~TDHMatrix();
   __property int RowCount =
    {
     read = FRowCount
    };
   __property int ColCount =
    {
     read = FColCount
    };
   TDHVector<DHMData> & __fastcall operator[](int i);
  };
 } // end of namespace DocHandEdit
// ***************************************************************************
// ********************** TDHVector ********************************************
// ***************************************************************************
template<class DHVData>
 __fastcall TDHVector<DHVData>::TDHVector(int AColCount)
 {
  if (AColCount < 0)
   throw DKClasses::EInvalidArgument(__FUNC__, "AColCount", FMT(icsDocMatrixErrorParam) + " > 0.");
  FColCount = AColCount;
  FVector   = (FColCount) ? new DHVData[FColCount] : NULL;
 }
// ---------------------------------------------------------------------------
template<class DHVData>
 __fastcall TDHVector<DHVData>::~TDHVector()
 {
  if (FVector)
   delete[]FVector;
 }
// ---------------------------------------------------------------------------
template<class DHVData>
 void __fastcall TDHVector<DHVData>::SetColCount(int AColCount)
 {
  if (AColCount < 0)
   throw DKClasses::EInvalidArgument(__FUNC__, "AColCount", FMT(icsDocMatrixErrorParam) + " > 0.");
  if (FColCount != AColCount)
   {
    FColCount = AColCount;
    FVector   = (FColCount) ? new DHVData[FColCount] : NULL;
   }
 }
// ---------------------------------------------------------------------------
template<class DHVData>
 DHVData & __fastcall TDHVector<DHVData>:: operator[](int i)
 {
  if ((i < 0) || (i >= FColCount))
   throw DKClasses::EInvalidArgument(__FUNC__, "i", FMT1(icsDocMatrixErrorIndex, IntToStr(i)));
  return FVector[i];
 }
// ***************************************************************************
// ********************** TDHMatrix ********************************************
// ***************************************************************************
template<class DHMData>
 __fastcall TDHMatrix<DHMData>::TDHMatrix(int ARowCount, int AColCount)
 {
  if (ARowCount <= 0)
   throw DKClasses::EInvalidArgument(__FUNC__, "ARowCount", FMT(icsDocMatrixErrorParam) + " >= 0.");
  if (AColCount <= 0)
   throw DKClasses::EInvalidArgument(__FUNC__, "AColCount", FMT(icsDocMatrixErrorParam) + " >= 0.");
  FRowCount = ARowCount;
  FColCount = AColCount;
  FMatrix   = new TDHVector<DHMData>[FRowCount];
  for (int i = 0; i < FRowCount; i++)
   FMatrix[i].ColCount = FColCount;
 }
// ---------------------------------------------------------------------------
template<class DHMData>
 __fastcall TDHMatrix<DHMData>::~TDHMatrix()
 {
  delete[]FMatrix;
 }
// ---------------------------------------------------------------------------
template<class DHMData>
 TDHVector<DHMData> & __fastcall TDHMatrix<DHMData>:: operator[](int i)
 {
  if ((i < 0) || (i >= FRowCount))
   throw DKClasses::EInvalidArgument(__FUNC__, "i", FMT1(icsDocMatrixErrorIndex, IntToStr(i)));
  return FMatrix[i];
 }
// ***************************************************************************
// ********************** ��������/��������/����� ����� **********************
// ***************************************************************************
__fastcall TDocTableData::TDocTableData(TTagNode * ATabDef, TTagNode * ATabDoc)
 {
  FTabDef = ATabDef;
  FTabDoc = ATabDoc;
  TICSDocSpec::GetTableSingleRowsList(FTabDef->GetChildByName("rows"), & FSingleRows);
  TICSDocSpec::GetTableSingleColsList(FTabDef->GetChildByName("columns"), & FSingleCols);
  FRowMax = FSingleRows.size();
  FColMax = FSingleCols.size();
  FTCells = new TDHMatrix<TTCellTagNodes>(FRowMax, FColMax);
  TTagNodeList::iterator itRow, itCol;
  int nRow, nCol;
  for (itRow = FSingleRows.begin(), nRow = 0; itRow != FSingleRows.end(); itRow++ , nRow++)
   {
    for (itCol = FSingleCols.begin(), nCol = 0; itCol != FSingleCols.end(); itCol++ , nCol++)
     {
      (*FTCells)[nRow][nCol].RowNode = (*itRow);
      (*FTCells)[nRow][nCol].ColNode = (*itCol);
     }
   }
 }
// ---------------------------------------------------------------------------
__fastcall TDocTableData::~TDocTableData()
 {
  __DELETE_OBJ(FTCells);
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TDocTableData::GetTabColNode(int ARow, int ACol)
 {
  return (* FTCells)[ARow][ACol].ColNode;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TDocTableData::GetTabRowNode(int ARow, int ACol)
 {
  return (* FTCells)[ARow][ACol].RowNode;
 }
// ---------------------------------------------------------------------------
TCellEDData & __fastcall TDocTableData::GetED(int ARow, int ACol)
 {
  return ((* FTCells)[ARow][ACol]).ED;
 }
// ---------------------------------------------------------------------------
void __fastcall TDocTableData::SetED(int ARow, int ACol, TCellEDData AED)
 {
  ((* FTCells)[ARow][ACol]).ED = AED;
 }
// ---------------------------------------------------------------------------
__fastcall TdsDocHandEditForm::TdsDocHandEditForm(TComponent * Owner, TTagNode * ASpec, TTagNode * ADoc,
  TAxeXMLContainer * AXMLContainer) : TForm(Owner)
 {
  FEdXLast      = 0;
  FEdYLast      = 0;
  FEDCurTop     = 0;
  FEDCurLeft    = 0;
  FDefColWidth  = 107;
  FDefRowHeight = 34;
  FXMLContainer = AXMLContainer;
  FSpec         = ASpec;
  FDoc          = ADoc;
  FTabPresent   = false;
  FEditDoc      = new TTagNode;
  FEditDoc->Assign(FDoc, true);
  TTagNode * ErrNode = FDoc->GetChildByName("errcontent", true);
  if (ErrNode)
   delete ErrNode;

  FModified        = false;
  FCurGridModified = false;
  FViewer          = new TICSDocHTMLCreator;
  FLRDirection     = true;
  FColHeaders      = new TStringList;
  FRowHeaders      = new TStringList;
  FLoadCodes();
 }
// ---------------------------------------------------------------------------
__fastcall TdsDocHandEditForm::~TdsDocHandEditForm()
 {
  __DELETE_OBJ(FEditDoc) delete FViewer;
  delete FColHeaders;
  delete FRowHeaders;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FormShow(TObject * Sender)
 {
  Caption            = /* FMT(dsDocHandDocCaption) + */ " - " + FDoc->GetChildByName("passport")->AV["mainname"];
  actSave->Enabled   = false;
  actSave->Caption   = FMT(icsDocHandEditSaveCaption);
  actSave->Hint      = FMT(icsDocHandEditSaveHint);
  actExit->Caption   = FMT(icsDocHandEditExitCaption);
  actExit->Hint      = FMT(icsDocHandEditExitHint);
  FRecalcMode        = false;
  ShowTimer->Enabled = true;
  if (FTabPresent)
   FocusFirstTabEd(0);
 }
// ***************************************************************************
// ********************** ������ ����� ***************************************
// ***************************************************************************
// ---------------------------------------------------------------------------
// ��������� TagNode'�, ���������������� �������� ���������� �������� ���������
TTagNode * __fastcall TdsDocHandEditForm::GetDocElementDesc(TTagNode * ADocElement)
 {
  return (ADocElement) ? FSpec->GetTagByUID(ADocElement->AV["uidmain"]) : NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocHandEditForm::DocDataToStr(const double AData)
 {
  if (AData == static_cast<long>(AData))
   return DKFloatToStr(AData, '.');
  else
   return DKFloatToStrF(AData, ffFixed, 18, 2, '.');
 }
// ---------------------------------------------------------------------------
double __fastcall TdsDocHandEditForm::DisplayTextToDocData(const UnicodeString & AVal)
 {
  double RC = 0;
  try
   {
    UnicodeString FSrc = AVal;
    int idx = FSrc.LastDelimiter(",.");
    if (idx)
     FSrc[idx] = FormatSettings.DecimalSeparator;
    TryStrToFloat(FSrc, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocHandEditForm::DocDataToDisplayText(const double AData)
 {
  if (AData == static_cast<long>(AData))
   return FloatToStr(AData);
  else
   return FloatToStrF(AData, ffFixed, 18, 2);
 }
// ---------------------------------------------------------------------------
/*
 * �������� ��������� ������ grid'� ��� �������� �������� ���������
 *
 *
 * ������������ ��������:
 *   true  - �������� ��������
 *   false - ������� ������������ ������
 *
 */
bool __fastcall TdsDocHandEditForm::ApplyCurrentGridUpdates(bool ARecalc)
 {
  bool fResult = true;
  if (FCurGridModified)
   {
    if (ARecalc)
     {
      ReCalcCurrentGridData();
      fResult = CheckByHandRules();
     }
    if (fResult)
     {
      int i = 0;
      TTagNode * ndLV2 = FTable[FEdTab]->TabDoc->GetFirstChild(); // ������ �������
      while (ndLV2)
       {
        int j = 0;
        TTagNode * ndIV = ndLV2->GetFirstChild(); // ������ �������
        while (ndIV)
         {
          ndIV->AV["val"] = DocDataToStr(DisplayTextToDocData(FTable[FEdTab]->GetED(i, j).Text.Trim()));
          ndIV            = ndIV->GetNext();
          j++ ;
         }
        ndLV2 = ndLV2->GetNext();
        i++ ;
       }
      FCurGridModified = false;
     }
   }
  return fResult;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::AssignIVValAV(TTagNode * ADestNode, TTagNode * ASrcNode)
 {
  if (ADestNode->CmpName("iv") && ASrcNode->CmpName("iv") && ADestNode->GetAttrByName("val")
    && ASrcNode->GetAttrByName("val"))
   ADestNode->AV["val"] = ASrcNode->AV["val"];
  ADestNode = ADestNode->GetFirstChild();
  ASrcNode  = ASrcNode->GetFirstChild();
  while (ADestNode && ASrcNode)
   {
    AssignIVValAV(ADestNode, ASrcNode);
    ADestNode = ADestNode->GetNext();
    ASrcNode  = ASrcNode->GetNext();
   }
 }
// ---------------------------------------------------------------------------
// �������� ���������
bool __fastcall TdsDocHandEditForm::ApplyUpdates()
 {
  bool RC = ApplyCurrentGridUpdates();
  if (RC)
   {
    AssignIVValAV(FDoc, FEditDoc);
    FModified        = true;
    actSave->Enabled = false;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
/*
 * ������ ������� �����/�������� � ������� ����� ������� ������� ���������������
 * ����� ������� � ��������������� ����� ������� ��������� ������ ��� ����������
 * ������� � ��������� � ���� �����'� ������� ��� ���������
 *
 *
 * ���������:
 *   [in]  APoland - ������� ��� ���������
 *   [in]  AType   - ��� ������� ��� ���������
 *   [in]  nRow    - ����� ������ ������� ������ grid'� ��� �������� �������� ���������
 *   [in]  nCol    - ����� ������� ������� ������ grid'� ��� �������� �������� ���������
 *
 * ������������ ��������:
 *   ������� ��� ��������� � ���� �����'� � ����������� �������� �����/�������� � ��������
 * ����� ������� ������� ��������������� ����� ������� � ��������������� ����� �������
 * ��������� ������ ��� ���������� �������
 *
 */
UnicodeString __fastcall TdsDocHandEditForm::ReplaceTRowColNum(UnicodeString APoland, TTRulesType AType, int nRow,
  int nCol)
 {
  UnicodeString Result = "";
  TStringList * SL;
  try
   {
    SL       = new TStringList;
    SL->Text = StringReplace(APoland, " ", "\n", TReplaceFlags() << rfReplaceAll);
    for (int i = 0; i < SL->Count; i++)
     if (SL->Strings[i].Length() && (SL->Strings[i][1] == '#')) // ����� ������/������� ��� ����� ������
      {
       if (SL->Strings[i].Pos(":")) // ����� ������
        {
         int nCellRowNo = StrToInt(GetCellNumLexRowNumber(SL->Strings[i]));
         int nCellColNo = StrToInt(GetCellNumLexColNumber(SL->Strings[i]));
         SL->Strings[i] =
           DKFloatToStr(DisplayTextToDocData(FTable[FEdTab]->GetED(nCellRowNo - 1, nCellColNo - 1).Text.Trim()), '.');
        }
       else // ����� ������/�������
        {
         int nRowColNum = StrToInt(GetRowColNumLexNumber(SL->Strings[i]));
         switch (AType)
          {
          case rtRow:
           SL->Strings[i] =
             DKFloatToStr(DisplayTextToDocData(FTable[FEdTab]->GetED(nRowColNum - 1, nCol).Text.Trim()), '.');
           break;
          case rtCol:
           SL->Strings[i] =
             DKFloatToStr(DisplayTextToDocData(FTable[FEdTab]->GetED(nRow, nRowColNum - 1).Text.Trim()), '.');
           break;
          }
        }
      }
    // Result = StringReplace(SL->Text, "\n", " ", TReplaceFlags() << rfReplaceAll);
    Result = SL->Text;
   }
  __finally
   {
    if (SL)
     delete SL;
   }
  return Result;
 }
// ---------------------------------------------------------------------------
/*
 * �������� ������������ ������ ������� �������� �������� ������� �����
 * ��������� ������ ��� ���������� �������
 *
 *
 * ���������:
 *   [in]  ndHandRules - ������� �������� ������� ����� (xml-��� "handrules" �������������)
 *   [in]  AType       - ��� ������ ��������
 *   [in]  nRow        - ����� ������ ������� ������ grid'� ��� �������� �������� ���������
 *   [in]  nCol        - ����� ������� ������� ������ grid'� ��� �������� �������� ���������
 *
 * ������������ ��������:
 *   true  - ������ ������� ������������� �������� �������� ������� �����
 *   false - ������ ������� �� ������������� �������� �������� ������� �����
 *
 */
bool __fastcall TdsDocHandEditForm::THandRulesCheck(TTagNode * ndHandRules, TTRulesType AType, int nRow, int nCol)
 {
  bool bResult = false;
  // ������ ������� �����/�������� ������� ��������������� ����� �������
  UnicodeString AHandRules = ReplaceTRowColNum(ndHandRules->AV["pol_cond"], AType, nRow, nCol);
  // �������� ���������� �������, ������������ ������� �������� ������� �����
  THandRulesCalc * HandRulesCalc;
  try
   {
    HandRulesCalc = new THandRulesCalc(AHandRules);
    bResult       = (HandRulesCalc->Calc() == 1);
   }
  __finally
   {
    if (HandRulesCalc)
     delete HandRulesCalc;
   }
  return bResult;
 }
// ---------------------------------------------------------------------------
// �������� ������������ ��������� ������ � ������������ � ��������� ��������
// ������� �����. ��� ���� ������� ��������� �� ������ � ������������
// ������ ����� � ������������� �������
bool __fastcall TdsDocHandEditForm::CheckByHandRules()
 {
  bool RC = false;
  try
   {
    // ����� �����               FTable[FEdTab]->GetED(nRow,nRowColNum - 1)->Text;
    FErrInCol = false;
    bool bBadHandRules = false;
    for (int i = 0; (i < FTable[FEdTab]->RowCount) && !bBadHandRules; i++)
     {
      for (int j = 0; (j < FTable[FEdTab]->ColCount) && !bBadHandRules; j++)
       {
        TTagNode * ndRowHandRules = FTable[FEdTab]->GetTabRowNode(i, j)->GetChildByName("handrules");
        TTagNode * ndColHandRules = FTable[FEdTab]->GetTabColNode(i, j)->GetChildByName("handrules");
        // �������� ������������
        if (ndRowHandRules || ndColHandRules)
         {
          UnicodeString S2, S3;
          TTagNode * ndBadHandRules = NULL;
          int nNum;
          if (FTable[FEdTab]->TabDef->CmpAV("tablepriority", "row"))
           {
            if (ndRowHandRules && !THandRulesCheck(ndRowHandRules, rtRow, i, j))
             {
              S2             = FMT(icsDocHandEditErrorRow);
              nNum           = i;
              S3             = " ������ �";
              ndBadHandRules = ndRowHandRules;
              bBadHandRules  = true;
             }
            if (ndColHandRules && !THandRulesCheck(ndColHandRules, rtCol, i, j))
             {
              S2             = FMT(icsDocHandEditErrorCol);
              nNum           = j;
              S3             = " ������� �";
              FErrInCol      = true;
              ndBadHandRules = ndColHandRules;
              bBadHandRules  = true;
             }
           }
          else
           {
            if (ndColHandRules && !THandRulesCheck(ndColHandRules, rtCol, i, j))
             {
              S2             = FMT(icsDocHandEditErrorCol);
              nNum           = i;
              S3             = " ������� �";
              FErrInCol      = true;
              ndBadHandRules = ndColHandRules;
              bBadHandRules  = true;
             }
            if (ndRowHandRules && !THandRulesCheck(ndRowHandRules, rtRow, i, j))
             {
              S2             = FMT(icsDocHandEditErrorRow);
              nNum           = j;
              S3             = " ������ �";
              ndBadHandRules = ndRowHandRules;
              bBadHandRules  = true;
             }
           }
          if (bBadHandRules)
           {
            // ����������� ������ ����� � ������������� �������
            FErrRow = i;
            FErrCol = j;
            // EDToCellPos(FEdYLast, FEdXLast);
            // FED->SelectAll();
            /* if (FTable[FEdTab]->GetED(i,j).Enabled)
             {
             FED->SelectAll();
             } */
            EDToCellPos(i, j);
            TReplaceFlags fRepFl;
            fRepFl << rfReplaceAll << rfIgnoreCase;
            S3 = StringReplace(ndBadHandRules->AV["condition"], "#", S3, fRepFl);
            // ����� ��������� �� ������
            ErrMsgLab->Caption = "������ ������ [������ �" + IntToStr(i + 1) + " : ������� �" + IntToStr(j + 1) +
              "] �� ������������� �������� �������� ��� " + S2 + " �" + IntToStr(nNum + 1);
            ErrRulesLab->Caption = S3;
            // ErrBasePanel->Height = 84;
            // ErrPanel->Visible = true;
            ErrPanelContainer->Visible = ivAlways;
           }
         }
       }
     }
    RC = !bBadHandRules;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
/*
 * �������� ������ ������ �������
 *
 *
 * ���������:
 *   [in]  ndCalcRules - ������� ���������� (xml-��� "calcrules" �������������)
 *   [in]  AType       - ��� ������ ����������
 *   [in]  nRow        - ����� ������ ������� ������ grid'�
 *   [in]  nCol        - ����� ������� ������� ������ grid'�
 *
 */
void __fastcall TdsDocHandEditForm::ReCalcTCell(TTagNode * ndCalcRules, TTRulesType AType, int nRow, int nCol)
 {
  if (ndCalcRules)
   {
    // ������ ������� �����/�������� ������� ��������������� ����� �������
    UnicodeString ACalcRules = ReplaceTRowColNum(ndCalcRules->GetChildByName("calc")->AV["pol_exp"], AType, nRow, nCol);
    // �������� ���������� �������, ������������ ������� �������� ������� �����
    TCalcRulesCalc * CalcRulesCalc;
    try
     {
      CalcRulesCalc = new TCalcRulesCalc(ACalcRules);
      FTable[FEdTab]->GetED(nRow, nCol).Text = DocDataToDisplayText(CalcRulesCalc->Calc());
      Application->ProcessMessages();
     }
    __finally
     {
      if (CalcRulesCalc)
       delete CalcRulesCalc;
     }
   }
 }
// ---------------------------------------------------------------------------
// �������� ����������� ������
void __fastcall TdsDocHandEditForm::ReCalcCurrentGridData()
 {
  FRecalcMode = true;
  try
   {
    // ����� �����               FTable[FEdTab]->GetTabRowNode(i,j)->
    bool FRowPrty = FTable[FEdTab]->TabDef->CmpAV("tablepriority", "row");
    // �������� ����������� �����
    for (int i = 0; i < FTable[FEdTab]->RowCount; i++)
     {
      for (int j = 0; j < FTable[FEdTab]->ColCount; j++)
       {
        Application->ProcessMessages();
        if (FRowPrty)
         ReCalcTCell(FTable[FEdTab]->GetTabRowNode(i, j)->GetChildByName("calcrules"), rtRow, i, j);
        else
         ReCalcTCell(FTable[FEdTab]->GetTabColNode(i, j)->GetChildByName("calcrules"), rtCol, i, j);
        PrBar->Position++ ;
       }
     }
    for (int i = 0; i < FTable[FEdTab]->RowCount; i++)
     {
      for (int j = 0; j < FTable[FEdTab]->ColCount; j++)
       {
        Application->ProcessMessages();
        if (FRowPrty)
         ReCalcTCell(FTable[FEdTab]->GetTabColNode(i, j)->GetChildByName("calcrules"), rtCol, i, j);
        else
         ReCalcTCell(FTable[FEdTab]->GetTabRowNode(i, j)->GetChildByName("calcrules"), rtRow, i, j);
        PrBar->Position++ ;
       }
     }
    bool FSet;
    for (int i = 0; i < FTable[FEdTab]->RowCount; i++)
     {
      for (int j = 0; j < FTable[FEdTab]->ColCount; j++)
       {
        /*
         FSet = !(TabEdit->IsEditing && TabEdit->SelectionCount && TabEdit->InplaceEditor);
         if (!FSet)
         FSet = (i != TabEdit->Selections[0]->AbsoluteIndex) && (j != TabEdit->InplaceColumnIndex);

         if (FSet)
         */
        Application->ProcessMessages();
        TabEdit->Root->Items[i]->Texts[j] = FTable[FEdTab]->GetED(i, j).Text;
        PrBar->Position++ ;
       }
     }
   }
  __finally
   {
    FRecalcMode = false;
   }
 }
// ---------------------------------------------------------------------------
// ***************************************************************************
// ********************** ����������� �������*********************************
// ***************************************************************************
void __fastcall TdsDocHandEditForm::actSaveExecute(TObject * Sender)
 {
  ApplyUpdates();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::actExitExecute(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocHandEditForm::GetNextCell()
 {
  bool RC = true;
  try
   {
    if (FLRDirection)
     {
      RC = IncX();
      if (!RC)
       RC = IncY();
     }
    else
     {
      RC = IncY();
      if (!RC)
       RC = IncX();
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocHandEditForm::IncY()
 {
  bool RC = true;
  try
   {
    FEdYLast = FEdY;
    if (FEdY < FTable[FEdTab]->RowCount - 1)
     FEdY++ ;
    else
     {
      FEdY = 0;
      RC   = false;
     }
    while (!FTable[FEdTab]->GetED(FEdY, FEdX).Enabled && (FEdY != FEdYLast))
     {
      if (FEdY < FTable[FEdTab]->RowCount - 1)
       FEdY++ ;
      else
       {
        FEdY = 0;
        RC   = false;
       }
     }
    if (FEdY == FEdYLast)
     FocusFirstTabEd(0, FEdY, 0);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocHandEditForm::DecY()
 {
  bool RC = true;
  try
   {
    FEdYLast = FEdY;
    if (FEdY > 0)
     FEdY-- ;
    else
     FEdY = FTable[FEdTab]->RowCount - 1;
    while (!FTable[FEdTab]->GetED(FEdY, FEdX).Enabled)
     {
      if (FEdY > 0)
       FEdY-- ;
      else
       FEdY = FTable[FEdTab]->RowCount - 1;
     }
    RC = (FEdY != FTable[FEdTab]->RowCount - 1);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocHandEditForm::IncX()
 {
  bool RC = true;
  try
   {
    FEdXLast = FEdX;
    if (FEdX < FTable[FEdTab]->ColCount - 1)
     FEdX++ ;
    else
     {
      FEdX = 0;
      RC   = false;
     }
    while (!FTable[FEdTab]->GetED(FEdY, FEdX).Enabled && (FEdX != FEdXLast))
     {
      if (FEdX < FTable[FEdTab]->ColCount - 1)
       FEdX++ ;
      else
       {
        FEdX = 0;
        RC   = false;
       }
     }
    if (FEdX == FEdXLast)
     FocusFirstTabEd(0, FEdY, 0);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocHandEditForm::DecX()
 {
  bool RC = false;
  try
   {
    FEdXLast = FEdX;
    if (FEdX > 0)
     FEdX-- ;
    else
     FEdX = FTable[FEdTab]->ColCount - 1;
    while (!FTable[FEdTab]->GetED(FEdY, FEdX).Enabled && (FEdX != FEdXLast))
     {
      if (FEdX > 0)
       FEdX-- ;
      else
       FEdX = FTable[FEdTab]->ColCount - 1;
     }
    if (FEdX == FEdXLast)
     FocusFirstTabEd(0, FEdX, 0);
    RC = (FEdX != FTable[FEdTab]->ColCount - 1);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FocusFirstTabEd(int ATab, int AStartRow, int AStartCol)
 {
  FEdTab = ATab;
  bool FCont = true;
  for (int y = AStartRow; (y < FTable[FEdTab]->RowCount) && FCont; y++)
   {
    for (int x = AStartCol; (x < FTable[FEdTab]->ColCount) && FCont; x++)
     {
      FCont = !FTable[FEdTab]->GetED(y, x).Enabled;
      if (!FCont)
       {
        FEdYLast = FEdY;
        FEdXLast = FEdX;
        FEdY     = y;
        FEdX     = x;
        EDToCellPos(y, x);
       }
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FocusLastTabEd(int ATab)
 {
  FEdTab = ATab;
  bool FCont = true;
  for (int y = FTable[FEdTab]->RowCount - 1; (y >= 0) && FCont; y--)
   {
    for (int x = FTable[FEdTab]->ColCount - 1; (x >= 0) && FCont; x--)
     {
      FCont = !FTable[FEdTab]->GetED(y, x).Enabled;
      if (!FCont)
       {
        FEdYLast = FEdY;
        FEdXLast = FEdX;
        FEdY     = y;
        FEdX     = x;
        EDToCellPos(y, x);
       }
     }
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocHandEditForm::FindLink(TList * ALinkList, const UnicodeString & ALinkSrc)
 {
  bool RC = false;
  try
   {
    UnicodeString FLinkSrc = ALinkSrc.UpperCase();
    for (int i = 0; (i < ALinkList->Count) && !RC; i++)
     {
      RC = (((TFontObj *)ALinkList->Items[i])->Url.UpperCase() == FLinkSrc);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FLoadCodes()
 {
  FErrRow = -1;
  FErrCol = -1;
  TStringList * CalcList = new TStringList;
  try
   {
    TStringList * SaveList = new TStringList;
    UnicodeString DataStr;
    try
     {
      DataStr = FViewer->PreviewInsc(FXMLContainer, FSpec, FEditDoc);
      FDocViewInsc->LoadFromString(DataStr);
      // SaveList->Text = DataStr;
      // SaveList->SaveToFile("_FDocViewInsc.htm");
      DataStr = FViewer->PreviewTabColHead(FXMLContainer, FSpec);
      ColView->LoadFromString(DataStr);
      // SaveList->Text = DataStr;
      // SaveList->SaveToFile("_ColView.htm");
      DataStr = FViewer->PreviewTabRowHead(FXMLContainer, FSpec);
      RowView->LoadFromString(DataStr);
      // SaveList->Text = DataStr;
      // SaveList->SaveToFile("_RowView.htm");
      DataStr = FViewer->PreviewFirstTab(FXMLContainer, FSpec, FEditDoc);
      FDocView->LoadFromString(DataStr);
      // SaveList->Text = DataStr;
      // SaveList->SaveToFile("_FDocView.htm");
     }
    __finally
     {
      delete SaveList;
     }
    FViewer->GetFirstTabCalcCell(FXMLContainer, FSpec, CalcList);
    FViewer->GetTabHeaderStr(FXMLContainer, FSpec, FRowHeaders, FColHeaders);
    TTagNode * itTab = FSpec->GetChildByName("doccontent");
    TTagNode * itTabDoc, *itRow, *itCol;
    if (itTab)
     {
      itTab = itTab->GetFirstChild();
      int FCurTabNum = 0;
      int FCRow, FCCol;
      while (itTab)
       {
        if (itTab->CmpName("table"))
         {
          FTabPresent = true;
          itTabDoc    = FEditDoc->GetChildByAV("lv", "uidmain", itTab->AV["uid"], true);
          if (itTabDoc)
           {
            FTable[FCurTabNum] = new TDocTableData(itTab, itTabDoc);
            FCRow              = 0;
            itRow              = itTabDoc->GetFirstChild();
            while (itRow)
             {
              FCCol = 0;
              itCol = itRow->GetFirstChild();
              while (itCol)
               {
                FTable[FCurTabNum]->SetED(FCRow, FCCol, TCellEDData(itCol->AV["val"],
                  CalcList->IndexOf(IntToStr(FCRow) + ":" + IntToStr(FCCol)) == -1));
                FCCol++ ;
                itCol = itCol->GetNext();
               }
              FCRow++ ;
              itRow = itRow->GetNext();
             }
            FCurTabNum++ ;
           }
          else
           throw Exception("������������ ��������� ���������.");
         }
        itTab = itTab->GetNext();
       }
      TabEdit->DeleteAllColumns();
      TabEdit->Clear();
      TcxTreeListColumn * tmpCol;
      TcxTreeListNode * tmpNode;
      TcxLabelProperties * tmpColProp;
      for (int i = 0; i < FTable[FEdTab]->ColCount; i++)
       {
        tmpCol                           = TabEdit->CreateColumn();
        tmpCol->Width                    = FDefColWidth;
        tmpCol->MinWidth                 = FDefColWidth;
        tmpCol->Options->ShowEditButtons = eisbAlways;
        tmpCol->PropertiesClassName      = "TcxLabelProperties";
        tmpColProp                       = (TcxLabelProperties *)tmpCol->Properties;
        tmpColProp->Alignment->Horz      = taRightJustify;
        tmpColProp->Alignment->Vert      = taVCenter;
        tmpCol->OnGetEditingProperties   = TabEditColGetEditingProperties;
       }
      for (int i = 0; i < FTable[FEdTab]->RowCount; i++)
       {
        tmpNode = TabEdit->Root->AddChild();
        for (int j = 0; j < FTable[FEdTab]->ColCount; j++)
         tmpNode->Texts[j] = FTable[FEdTab]->GetED(i, j).Text;
       }
      TabEdit->DefaultRowHeight = FDefRowHeight;
     }
   }
  __finally
   {
    delete CalcList;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FormClose(TObject * Sender, TCloseAction & Action)
 {
  if (FCurGridModified)
   {
    int RC = MessageBox(Handle, L"��������� ��������� ���������?", L"���������",
      MB_YESNOCANCEL | MB_ICONQUESTION | MB_DEFBUTTON1);
    if (RC == ID_YES)
     {
      if (ApplyUpdates())
       Action = caHide;
      else
       Action = caNone;
     }
    else if (RC == ID_CANCEL)
     Action = caNone;
    else
     Action = caHide;
   }
  else
   Action = caHide;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  // bool FChLR = false;
  // bool FChTB = false;
  if (Key == VK_HOME)
   {
    FocusFirstTabEd(0);
   }
  else if (Key == VK_END)
   {
    FocusLastTabEd(0);
   }
  else if (Key == VK_RETURN)
   {
    if (TabEdit->IsEditing || !FTable[FEdTab]->GetED(FEdY, FEdX).Enabled)
     {
      TabEdit->Root->EndEdit(false);
      if (!GetNextCell())
       {
        TabEdit->Root->Items[0]->MakeVisible();
        TabEdit->Columns[0]->Focused     = true;
        TabEdit->Root->Items[0]->Focused = true;
        FocusFirstTabEd(0);
       }
      else
       {
        EDToCellPos(FEdY, FEdX);
        if (!FTable[FEdTab]->GetED(FEdYLast, FEdXLast).Enabled)
         Key = 0;
       }
     }
   }
  /*
   if (Key == VK_DOWN)
   {
   FChTB = true;
   IncY();
   }
   else if (Key == VK_UP)
   {
   FChTB = true;
   DecY();
   }
   else if (Key == VK_RIGHT)
   {
   if (TabEdit->IsEditing)
   {
   if (FED->SelStart >= FED->Text.Trim().Length())
   {
   FChLR = true;
   IncX();
   }
   }
   else
   {
   FChLR = true;
   IncX();
   }
   }
   else if (Key == VK_LEFT)
   {
   if (TabEdit->IsEditing)
   {
   if (FED->SelStart <= 0)
   {
   FChLR = true;
   DecX();
   }
   }
   else
   {
   FChLR = true;
   DecX();
   }
   }
   if (FChLR || FChTB)
   {
   TabEdit->Root->EndEdit(false);
   EDToCellPos(FEdY, FEdX);
   Key = 0;
   } */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::ShowTimerTimer(TObject * Sender)
 {
  ShowTimer->Enabled   = false;
  FDocViewInsc->Height = FDocViewInsc->VScrollBar->Max;
  if (FDocView->VScrollBar->Visible)
   HPanel->Width = FDocView->VScrollBar->Width;
  else
   HPanel->Width = 0;
  // RightScrollPanel->Width = ColView->Width - ColView->FullDisplaySize(1).cx;// + 20;
  ColumnPanel->Height = ColView->VScrollBar->Max;
  // RowPanel->Width     = RowView->FullDisplaySize(1).cx;// + 35; //300;//RowView->HScrollBar->Max;
  EmptyPanel->Width = RowPanel->Width;
  int FVisRowCount = ViewerClientPanel->Height / (FDefRowHeight);
  ViewerSizePanel->Height = FVisRowCount * (FDefRowHeight) - 16;
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::EDToCellPos(int ARow, int ACol)
 {
  TabEdit->SetFocus();
  TabEdit->Root->Items[ARow]->MakeVisible();
  TabEdit->Columns[ACol]->MakeVisible();
  TabEdit->Columns[ACol]->Focused     = true;
  TabEdit->Root->Items[ARow]->Focused = true;
  // FED->SetFocus();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::TabEditStylesGetContentStyle(TcxCustomTreeList * Sender,
  TcxTreeListColumn * AColumn, TcxTreeListNode * ANode, TcxStyle *& AStyle)
 {
  if (AColumn && ANode)
   {
    bool FDis = !FTable[FEdTab]->GetED(ANode->AbsoluteIndex, AColumn->Position->ColIndex).Enabled;
    if (FDis)
     AStyle = dStyle;
    if ((FErrRow != -1) && (ANode->AbsoluteIndex == FErrRow))
     { // ������
      // if (FDis)
      // AStyle = errDStyle;
      // else
      AStyle = comErrStyle;
     }
    if ((FErrCol != -1) && (AColumn->Position->ColIndex == FErrCol))
     { // �������
      // if (FDis)
      // AStyle = errDStyle;
      // else
      AStyle = comErrStyle;
     }
    if ((FErrRow != -1) && (FErrCol != -1))
     { // ������
      if ((ANode->AbsoluteIndex == FErrRow) && (AColumn->Position->ColIndex == FErrCol))
       AStyle = errStyle;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::TabEditEditing(TcxCustomTreeList * Sender, TcxTreeListColumn * AColumn,
  bool & Allow)
 {
  if (AColumn && TabEdit->SelectionCount)
   {
    Allow = FTable[FEdTab]->GetED(TabEdit->Selections[0]->AbsoluteIndex, AColumn->Position->ColIndex).Enabled;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FormResize(TObject * Sender)
 {
  ShowTimerTimer(ShowTimer);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::TabEditEditValueChanged(TcxCustomTreeList * Sender, TcxTreeListColumn * AColumn)
 {
  // TTime Beg = TTime::CurrentTime();
  // TTime t1, t2, t3, t4;
  // t1 = t2 = t3 = t4 = 0;
  if (TabEdit->IsEditing && TabEdit->SelectionCount && TabEdit->InplaceEditor)
   {
    FTable[FEdTab]->GetED(TabEdit->Selections[0]->AbsoluteIndex, AColumn->Position->ColIndex).Text =
      TabEdit->InplaceEditor->EditingValue;
    // t1 = TTime::CurrentTime();
    if (!FRecalcMode)
     {
      // ((TcxMaskEdit*)Sender)->Hint = ((TcxMaskEdit*)Sender)->Text;
      ErrMsgLab->Caption         = "";
      ErrRulesLab->Caption       = "";
      ErrPanelContainer->Visible = ivNever;
      if ((FErrRow != -1) && (FErrCol != -1))
       {
        FErrRow = -1;
        FErrCol = -1;
       }
      FCurGridModified = true;
      actSave->Enabled = false;
      // t2                 = TTime::CurrentTime();
      // ReCalcCurrentGridData();
      // t3 = TTime::CurrentTime();
      // CheckByHandRules();
      // t4 = TTime::CurrentTime();
      Application->ProcessMessages();
     }
    // ShowMessage("\nset val:\t\t\t " + (t1 - Beg).FormatString("ss.zzz") + "\nbefore recalc:\t " +
    // (t2 - t1).FormatString("ss.zzz") + "\nReCalc:\t\t " + (t3 - t2).FormatString("ss.zzz") + "\nCheckByHand:\t " +
    // (t4 - t3).FormatString("ss.zzz") + "\nRecalc+CheckByHand: " + (t4 - t2).FormatString("ss.zzz") + "\ncom:\t\t\t " +
    // (t4 - Beg).FormatString("ss.zzz"));
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::TabEditColGetEditingProperties(TcxTreeListColumn * Sender, TcxTreeListNode * ANode,
  TcxCustomEditProperties *& EditProperties)
 {
  EditProperties = FED->Properties;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::actLRDirectionExecute(TObject * Sender)
 {
  FLRDirection = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::actTBDirectionExecute(TObject * Sender)
 {
  FLRDirection = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::TabEditFocusedNodeChanged(TcxCustomTreeList * Sender,
  TcxTreeListNode * APrevFocusedNode, TcxTreeListNode * AFocusedNode)
 {
  if (AFocusedNode)
   {
    FEdYLast = FEdY;
    FEdY     = AFocusedNode->AbsoluteIndex;
    CurRowColHeaderUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::TabEditFocusedColumnChanged(TcxCustomTreeList * Sender,
  TcxTreeListColumn * APrevFocusedColumn, TcxTreeListColumn * AFocusedColumn)
 {
  if (AFocusedColumn)
   {
    FEdXLast = FEdX;
    FEdX     = AFocusedColumn->Position->ColIndex;
    CurRowColHeaderUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::CurRowColHeaderUpdate()
 {
  UnicodeString FLab = "";
  TTagNode * ndRowCalcRules = FTable[FEdTab]->GetTabRowNode(FEdY, FEdX)->GetChildByName("calc", true);
  TTagNode * ndColCalcRules = FTable[FEdTab]->GetTabColNode(FEdY, FEdX)->GetChildByName("calc", true);
  if ((FEdY >= 0) && (FEdY < FRowHeaders->Count))
   {
    FLab += "������ [" + FRowHeaders->Strings[FEdY] + "]";
   }
  if ((FEdX >= 0) && (FEdX < FColHeaders->Count))
   {
    FLab += "  ������� [" + FColHeaders->Strings[FEdX] + "]";
   }
  CurColRowLab->Caption = FLab;
  FLab = "";
  if (ndRowCalcRules)
   RowCalcRulesLab->Caption = "������ �" + IntToStr(FEdY + 1) + " = " + ndRowCalcRules->AV["expression"];
  else
   RowCalcRulesLab->Caption = "";
  if (ndColCalcRules)
   ColCalcRulesLab->Caption = "������� �" + IntToStr(FEdX + 1) + " = " + ndColCalcRules->AV["expression"];
  else
   ColCalcRulesLab->Caption = "";
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::TabEditLeftPosChanged(TObject * Sender)
 {
  if (Visible && TabEdit->VisibleColumnCount && TabEdit->VisibleCount)
   {
    int Pos = -1;
    int LPos = 0;
    for (int i = 0; (i < TabEdit->VisibleColumnCount) && (Pos == -1); i++)
     {
      LPos = TabEdit->CellRect(TabEdit->TopVisibleNode, TabEdit->VisibleColumns[i]).Left;
      if (LPos)
       Pos = i + 1;
     }
    LPos = (FDefColWidth + 1) - TabEdit->CellRect(TabEdit->TopVisibleNode, TabEdit->VisibleColumns[Pos]).Left;
    ColView->HScrollBarPosition = LPos + (FDefColWidth + 1) * (Pos - 1) + TabEdit->OptionsView->IndicatorWidth -
      TabEdit->OptionsView->FixedSeparatorWidth;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::TabEditTopRecordIndexChanged(TObject * Sender)
 {
  if (Visible && TabEdit->VisibleColumnCount && TabEdit->VisibleCount)
   {
    int YPos = (FDefRowHeight + 1) * TabEdit->TopVisibleNode->Index;
    RowView->VScrollBarPosition = YPos;
   }
 }
// ---------------------------------------------------------------------------
/*
 // if (!AViewInfo->Selected)
 //  {
 //paint the focused node
 //    ACanvas->Brush->Color = 0x80FFFF;
 //    ACanvas->FillRect(AViewInfo->EditRect, clRed);
 //TcxBorders fff;
 // fff = cxBordersAll;
 //ACanvas->FrameRect(AViewInfo->BoundsRect);//, clDefault, 1, fff);
 ACanvas->MoveTo(AViewInfo->BoundsRect.left-1,AViewInfo->BoundsRect.top);
 ACanvas->LineTo(AViewInfo->BoundsRect.right-1,AViewInfo->BoundsRect.top);
 ACanvas->LineTo(AViewInfo->BoundsRect.right-1,AViewInfo->BoundsRect.bottom*2);
 ACanvas->LineTo(AViewInfo->BoundsRect.left-1,AViewInfo->BoundsRect.bottom*2);
 ACanvas->LineTo(AViewInfo->BoundsRect.left-1,AViewInfo->BoundsRect.top);
 //    ACanvas->FillRect(AViewInfo->EditRect, clRed);
 UnicodeString DDD = "wewerw wer wer wer werwe rwer";
 ACanvas->DrawTexT("wewerw wer wer wer werwe rwer", AViewInfo->BoundsRect, cxWordBreak);
 //  }
 ADone = true;
 */
void __fastcall TdsDocHandEditForm::actCheckExecute(TObject * Sender)
 {
  PrTimer->Enabled       = true;
  PrBar->Position        = 0;
  PrBar->Max             = FTable[FEdTab]->RowCount * FTable[FEdTab]->ColCount * 3;
  ProgressPanel->Left    = 0;
  ProgressPanel->Top     = 0;
  ProgressPanel->Width   = ClientWidth;
  ProgressPanel->Height  = ClientHeight - 45;
  ProgressPanel->Visible = true;
  ProgressPanel->BringToFront();
  try
   {
    if (TabEdit->IsEditing)
     TabEdit->HideEdit();
    ReCalcCurrentGridData();
    actSave->Enabled = CheckByHandRules();
   }
  __finally
   {
    PrTimer->Enabled       = false;
    ProgressPanel->Visible = false;
    ProgressPanel->Width   = 0;
    ProgressPanel->Height  = 0;
    Application->ProcessMessages();
    PrBar->Position = 0;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::PrTimerTimer(TObject * Sender)
 {
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
