/*
 ����        - ICSDocViewer.cpp
 ������      - ��������������� (ICS)
 ��������    - ����������� ���������.
 ���� ����������
 ����������� - ������� �.�.
 ����        - 18.10.2004
 */
// ---------------------------------------------------------------------------
#include <basepch.h>
#pragma hdrstop
#include "ICSDocHandHTMLCreator.h"
#include <math.h>
#include "DKAxeUtils.h"
#include "ICSDocSpec.h"
#include "ExtUtils.h"
#pragma package(smart_init)
// ###########################################################################
// ##                                                                       ##
// ##                               TICSDocHTMLCreator                      ##
// ##                                                                       ##
// ###########################################################################
// ***************************************************************************
// ********************** ��������/�������� ���������� ***********************
// ***************************************************************************
const UnicodeString icsDocViewerExNumTCellLTitle = "� �/�";
const UnicodeString icsDocViewerExErrorNoSpecDef = "������ ���� ������ ������������.";
const UnicodeString icsDocViewerExErrorGUINotValid =
  "��� ������������� ���������� ��������� �� ��������� � ��� ���������� �������������";
__fastcall TICSDocHTMLCreator::TICSDocHTMLCreator()
 {
  FSpec                      = NULL;
  FDoc                       = NULL;
  FHTML                      = "";
  FndHTML                    = NULL;
  FXMLContainer              = NULL;
  FListZeroValCheckCountOnly = true;
  __BorderWidth              = "";
  __RowNumbers               = true;
  __ColNumbers               = true;
  __RowNum                   = 1;
  __ColNum                   = 1;
  __PBMax                    = 0;
  __PBPosition               = 0;
 }
// ---------------------------------------------------------------------------
__fastcall TICSDocHTMLCreator::~TICSDocHTMLCreator()
 {
  __DELETE_OBJ(FndHTML);
 }
// ---------------------------------------------------------------------------
// ������������� ���� FndHTML
TTagNode * __fastcall TICSDocHTMLCreator::InitNDHTML(UnicodeString AClass)
 {
  __DELETE_OBJ(FndHTML);
  FndHTML = new TTagNode;
  FndHTML->Name = "body";
  if (AClass.Length())
  FndHTML->AV["class"] = AClass;

  return FndHTML;
 }
// ---------------------------------------------------------------------------
// ***************************************************************************
// ********************** ��������������� ������ *****************************
// ***************************************************************************
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
bool __fastcall TICSDocHTMLCreator::CheckIfTerminated()
 {
  return false;
 }
// ---------------------------------------------------------------------------
// ����������� ���'� ����� ��� �������� ������� ndInsText ( xml-��� "instext" ������������� )
UnicodeString __fastcall TICSDocHTMLCreator::GetInsTextStyle(TTagNode * ndInsText)
 {
  UnicodeString Result;
  if ((Result = ndInsText->AV["style"]) == "")
   if ((Result = ndInsText->GetParent("inscription")->AV["style"]) == "")
    Result = ndInsText->GetRoot()->GetChildByName("styles", true)->AV["defaultbstyle"];
  return Result;
 }
// ---------------------------------------------------------------------------
// ����������� ������������ ��� �������� ������� ndInsText ( xml-��� "instext" ������������� )
UnicodeString __fastcall TICSDocHTMLCreator::GetInsTextAlign(TTagNode * ndInsText)
 {
  UnicodeString StyleUID = GetInsTextStyle(ndInsText);
  TTagNode * ndStyles = ndInsText->GetRoot()->GetChildByName("styles", true);
  return ndStyles->GetChildByAV("style", "uid", StyleUID)->AV["align"];
 }
// ---------------------------------------------------------------------------
/*
 * ����������� ���'� ����� ������������� ��� ������ ������� ( xml-��� "table" ������������� ) ndCellTagNode
 *
 *
 * ���������:
 *   [in]  AStyleAttrName   - ��� ��������, ������������� ������������ ����� ( "hstyle" ��� "bstyle" )
 *   [in]  ndCellTagNode    - ������ ������� ( xml-��� "trow" ��� "tcol" ������������� )
 *   [in]  ndAltCellTagNode - �������������� ������ ������� ( xml-��� "trow" ��� "tcol" ������������� )
 *                            [default = NULL]
 *
 * ������������ ��������:
 *   ��� ����� ������������� ��� ������ ������� ( xml-��� "table" ������������� ) ndCellTagNode
 *
 */
UnicodeString __fastcall TICSDocHTMLCreator::GetTableCellStyle(const UnicodeString & AStyleAttrName,
  TTagNode * ndCellTagNode, TTagNode * ndAltCellTagNode)
 {
  UnicodeString Result;
  if ((Result = ndCellTagNode->AV[AStyleAttrName]) == "")
   if (!ndAltCellTagNode || (ndAltCellTagNode && ((Result = ndAltCellTagNode->AV[AStyleAttrName]) == "")))
    if ((Result = ndCellTagNode->GetParent("table")->AV[AStyleAttrName]) == "")
     Result = ndCellTagNode->GetRoot()->GetChildByName("styles", true)->AV[(AStyleAttrName == "hstyle") ?
      "defaulthstyle" : "defaultbstyle"];
  return Result;
 }
// ---------------------------------------------------------------------------
/*
 * ����������� ���'� ����� ������������� ��� ������ ( xml-��� "list" ������������� ) ndList
 *
 *
 * ���������:
 *   [in]  AStyleAttrName - ��� ��������, ������������� ������������ ����� ( "hstyle" ��� "bstyle" )
 *   [in]  ndList         - ������ ( xml-��� "list" ������������� )
 *
 * ������������ ��������:
 *   ��� ����� ������������� ��� ������ ( xml-��� "list" ������������� ) ndList
 *
 */
UnicodeString __fastcall TICSDocHTMLCreator::GetListStyle(const UnicodeString & AStyleAttrName, TTagNode * ndList)
 {
  UnicodeString Result;
  if ((Result = ndList->AV[AStyleAttrName]) == "")
   Result = ndList->GetRoot()->GetChildByName("styles", true)->AV[(AStyleAttrName == "hstyle") ? "defaulthstyle" :
    "defaultbstyle"];
  return Result;
 }
// ---------------------------------------------------------------------------
// ������������ �������������� �������� � ������ �������� ����������� ������� ��������
// ������������ ������������, ��� 0.0 ����� ������������ ��� 0
void __fastcall TICSDocHTMLCreator::NormalizeNumericalValue(UnicodeString & ACellText, const bool fShowZVal)
 {
  try
   {
    const double cdblEps = 1E-8;
    double dblValue = DKStrToFloat(ACellText, '.');
    if (fabs(dblValue - 0.0) < cdblEps)
     ACellText = (fShowZVal) ? "0" : "";
   }
  catch (EConvertError &)
   {;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::HiMetricToPixels(long * pnWidth, long * pnHeight)
 {
  // ��������� ��� ��������������
  const float nHiMetricPerInch = 25.4;
  HDC hDC = GetDC(0);
  // ���������� ���������� �������� � ����������
  // ����� �� ����������� � �� ���������
  int dpiX = GetDeviceCaps(hDC, LOGPIXELSX);
  int dpiY = GetDeviceCaps(hDC, LOGPIXELSY);
  // ��������� ��������������
  if (pnWidth)
   * pnWidth = *pnWidth * dpiX / nHiMetricPerInch;
  if (pnHeight)
   * pnHeight = *pnHeight * dpiY / nHiMetricPerInch;
  ReleaseDC(0, hDC);
 }
// ---------------------------------------------------------------------------
// ������������ ������ ��� ��������� ������� ������ ( xml-��� "list" ������������� )
UnicodeString __fastcall TICSDocHTMLCreator::GetListColHeaderText(TTagNode * ndListCol)
 {
  UnicodeString HeaderText = "";
  if (TICSDocSpec::IsListCol(ndListCol))
   {
    if (TICSDocSpec::IsListColSingle2(ndListCol))
     {
      if (ndListCol->AV["ref"] != "")
       HeaderText = FXMLContainer->GetRefer(ndListCol->AV["ref"])->AV["name"];
     }
    else
     HeaderText = ndListCol->AV["name"];
    if (!HeaderText.Length())
     HeaderText = "&nbsp;";
   }
  return HeaderText;
 }
// ---------------------------------------------------------------------------
// ������������ ������ ��� ��������� ������/������� ������� ( xml-��� "table" ������������� )
UnicodeString __fastcall TICSDocHTMLCreator::GetTableHeaderCellText(TTagNode * ndHeaderCell)
 {
  UnicodeString HeaderText = ndHeaderCell->AV["name"];
  if (!HeaderText.Length())
   HeaderText = "&nbsp;";
  return HeaderText;
 }
// ---------------------------------------------------------------------------
/*
 * ����������� ������������� �������� ������ �����, ��������������� �������� ������
 * ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in]  ANode  - ������ xml-���������, � ������� �������������� ����� �������������
 *                  �������� ������
 *
 * ������������ ��������:
 *   ������������ �������� ������
 *
 */
long __fastcall TICSDocHTMLCreator::GetListColsMaxLevel(const TTagNode * ANode) const
 {
  long nLevel = 0;
  if (TICSDocSpec::IsListColSingle(ANode))
   {
    int nNodeLevel = ANode->Level;
    if (TICSDocSpec::IsListColSingle2(ANode))
     nNodeLevel-- ;
    if (nNodeLevel > nLevel)
     nLevel = nNodeLevel;
   }
  TTagNode * ndChild = const_cast<TTagNode *>(ANode)->GetFirstChild();
  while (ndChild)
   {
    long nChildNodesMaxLevel = GetListColsMaxLevel(ndChild);
    if (nChildNodesMaxLevel > nLevel)
     nLevel = nChildNodesMaxLevel;
    ndChild = ndChild->GetNext();
   }
  return nLevel;
 }
// ---------------------------------------------------------------------------
// ����������� ������������� �������� ��� "����������" ��� ������������ �������������
int __fastcall TICSDocHTMLCreator::GetMaxPBForSpec()
 {
  TTagNode * ndDocContent = FSpec->GetChildByName("doccontent");
  int nPBMax = ndDocContent->Count;
  TTagNode * ndDocElement = ndDocContent->GetFirstChild();
  while (ndDocElement)
   {
    if (ndDocElement->CmpName("inscription"))
     nPBMax += ndDocElement->Count;
    else if (ndDocElement->CmpName("list"))
     nPBMax += CountTagNodes(ndDocElement, TICSDocSpec::IsListColSingle);
    else if (ndDocElement->CmpName("table"))
     {
      nPBMax += CountTagNodes(ndDocElement->GetChildByName("columns"), "tcol");
      nPBMax += CountTagNodes(ndDocElement->GetChildByName("rows"), "trow");
     }
    ndDocElement = ndDocElement->GetNext();
   }
  return nPBMax;
 }
// ---------------------------------------------------------------------------
// ����������� ������������� �������� ��� "����������" ��� ������������ ���������
int __fastcall TICSDocHTMLCreator::GetMaxPBForDoc()
 {
  int nPBMax = GetMaxPBForSpec();
  TTagNode * ndObj = FDoc->GetChildByName("contens")->GetFirstChild();
  while (ndObj)
   {
    TTagNode * ndObjRef = FSpec->GetTagByUID(ndObj->AV["uidmain"]);
    if (ndObjRef->CmpName("list,table"))
     nPBMax += ndObj->Count;
    ndObj = ndObj->GetNext();
   }
  return nPBMax;
 }
// ---------------------------------------------------------------------------
// ***************************************************************************
// ********************** ��������������� ������ *****************************
// ********************** ��� ������� HTML ***********************************
// ***************************************************************************
// ���������� � htmlParent ��������� html-���� "table"
// ( ������������ ��������� ���� __BorderWidth ��� �������� ������� ����� � __Width ��� �������� ������ )
TTagNode * __fastcall TICSDocHTMLCreator::AddHTMLTableTag(TTagNode * htmlParent, UnicodeString ADop)
 {
  TTagNode * htmlTableTag = htmlParent->AddChild("table");
  SetHTMLBoxWidthValues(htmlTableTag, __BorderWidth, __Width);
  htmlTableTag->AV["bordercolor"] = "black";
  htmlTableTag->AV["cellspacing"] = "0";
  htmlTableTag->AV["cellpadding"] = "0";
  if (ADop.Length())
   htmlTableTag->AV["id"] = ADop;
  return htmlTableTag;
 }
// ---------------------------------------------------------------------------
// ������������ ����� � �������� �������� ��� ��������� ������������ ��� html-���� "div"
void __fastcall TICSDocHTMLCreator::GetHTML_DIVAlignAttrVal(const UnicodeString & AAlign, UnicodeString & AAttrName,
  UnicodeString & AAttrVal)
 {
  AAttrName = "style";
  AAttrVal  = "vertical-align:top; text-align : " + AAlign; /* change kab 20.06.07 "vertical-align:top;" */
 }
// ---------------------------------------------------------------------------
/*
 * ������������ html-����� ��� �������������� ������ � ������������ � ���������
 * ������
 *
 *
 * ���������:
 *   [in]  htmlTag   - html-��� � ������� ( �������� <div>, <span>, <th>, <td> )
 *   [in]  AStyleUID - ��� ����� ������������� ��� ��������������
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������������� �����
 *
 *
 */
TTagNode * __fastcall TICSDocHTMLCreator::CreateHTMLTextFormat(TTagNode * htmlTag, const UnicodeString & AStyleUID)
 {
  TTagNode * ndResult = htmlTag;
  TTagNode * ndStyles = FSpec->GetChildByName("styles", true);
  TTagNode * ndStyle = ndStyles->GetChildByAV("style", "uid", AStyleUID);
  htmlTag->AV["class"] = AStyleUID;
  if (ndStyle->CmpAV("wordwrap", "0"))
   {
    if (htmlTag->CmpName("th,td"))
     htmlTag->AV["nowrap"] = "";
    else
     ndResult = htmlTag->AddChild("nobr");
   }
  return ndResult;
 }
// ---------------------------------------------------------------------------
/*
 * ��������� ������ ����� ��� ������ html-������� ( ������ ��� �������������� CSS )
 *
 *
 * ���������:
 *   [in]  htmlTableCell - html-���, ��������������� ������ html-�������
 *   [in]  ABorderWidth  - ������ �����
 *
 * ������������ ��������:
 *   true  - �������� ���������
 *   false - ��������� ����������� �� ����, ����� ���������� ������������� CSS
 *
 */
bool __fastcall TICSDocHTMLCreator::SetHTMLTableCellBorderWidth(TTagNode * htmlTableCell,
  const UnicodeString & ABorderWidth)
 {
  //htmlTableCell->AV["style"] = "border-width:" + ABorderWidth + "px";
  return true;
 }
// ---------------------------------------------------------------------------
bool __fastcall TICSDocHTMLCreator::SetHTMLBoxWidthValues(TTagNode * htmlTag, const UnicodeString & ABorderWidth,
  const UnicodeString & AWidth)
 {
  UnicodeString AWidthHTMLVal = "";
  if (AWidth != "")
   {
    if (AWidth.Pos("%"))
     AWidthHTMLVal = AWidth;
    else
     {
      long nWidthPX = AWidth.ToInt();
      HiMetricToPixels(& nWidthPX, NULL);
      AWidthHTMLVal = IntToStr((int)nWidthPX) + "px";
     }
   }
//  UnicodeString AStyleAttrVal = "border-width:" + ABorderWidth + "px";
//  if (AWidthHTMLVal != "")
//   AStyleAttrVal += ";"; // width:" + AWidthHTMLVal;
//  htmlTag->AV["style"] = AStyleAttrVal;
  return true;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ html-����� ��� �������������� ������ ������ html-�������,
 * ������������ ��� ��������� �����/��������
 *
 *
 * ���������:
 *   [in]  htmlNumTableCell - html-���, ��������������� ������ html-�������,
 *                            ������������ ��� ��������� �����/�������� ( html-��� "th" )
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������������� �����
 *
 *
 */
// ---------------------------------------------------------------------------
TTagNode * __fastcall TICSDocHTMLCreator::CreateHTMLTableNumCellTextFormat(TTagNode * htmlNumTableCell)
 {
  TTagNode * ndResult = htmlNumTableCell;
  htmlNumTableCell->AV["class"] = "num";
  return ndResult;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TICSDocHTMLCreator::CreateHTMLTableNumColTextFormat(TTagNode * htmlNumTableCell)
 {
  TTagNode * ndResult = htmlNumTableCell;
  htmlNumTableCell->AV["class"] = "numcol";
  return ndResult;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TICSDocHTMLCreator::CreateHTMLTableNumRowTextFormat(TTagNode * htmlNumTableCell)
 {
  TTagNode * ndResult = htmlNumTableCell;
  htmlNumTableCell->AV["class"] = "numrow";
  return ndResult;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ �������� HTML-�����
 *
 *
 * ���������:
 *   [in]  AStyleClass          - ������������ ������ HTML-�����
 *   [in]  AStyleClassStrPrefix - ������� ����� ������������� ������ HTML-�����
 *   [in]  AStylePropStrPrefix  - ������� ����� ��������
 *   [in]  ndStyle              - xml-��� "style" �������������
 *
 * ������������ ��������:
 *   �������� HTML-����� � ���� ������
 *
 *
 */
UnicodeString __fastcall TICSDocHTMLCreator::GetHTMLStyleDefinition(const UnicodeString & AStyleClass,
  const UnicodeString & AStyleClassStrPrefix, const UnicodeString & AStylePropStrPrefix, TTagNode * ndStyle)
 {
  UnicodeString STYLE = AStyleClassStrPrefix + "." + AStyleClass + "\n";
  STYLE += AStyleClassStrPrefix + "{\n";
  STYLE += AStylePropStrPrefix + "text-align     : " + ndStyle->AV["align"] + ";\n";
  STYLE += AStylePropStrPrefix + "vertical-align:top;\n"; /* change kab 20.06.07 ������������ ������������ */
  STYLE += AStylePropStrPrefix + "font-family    : \"" + ndStyle->AV["fontname"] + "\";\n";
  // !!!  STYLE += AStylePropStrPrefix  + "font-size      : "   + ndStyle->AV["fontsize"] + "pt;\n";
  STYLE += AStylePropStrPrefix + "text-decoration:";
  if (ndStyle->CmpAV("strike", "1") || ndStyle->CmpAV("underline", "1"))
   {
    if (ndStyle->CmpAV("strike", "1"))
     STYLE += " line-through";
    if (ndStyle->CmpAV("underline", "1"))
     STYLE += " underline";
   }
  else
   STYLE += " none";
  STYLE += ";\n";
  STYLE += AStylePropStrPrefix + "font-weight    :";
  STYLE += (ndStyle->CmpAV("bold", "1")) ? " bold" : " normal";
  STYLE += ";\n";
  STYLE += AStylePropStrPrefix + "font-style     :";
  STYLE += (ndStyle->CmpAV("italic", "1")) ? " italic" : " normal";
  STYLE += ";\n";
  STYLE += AStyleClassStrPrefix + "}\n";
  return STYLE;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ������� HTML-������
 *
 *
 * ���������:
 *   [in]  AStyleClassStrPrefix - ������� ����� ������������� ������ HTML-�����
 *   [in]  AStylePropStrPrefix  - ������� ����� ��������
 *   [in]  ndStyle              - xml-��� "styles" �������������
 *
 * ������������ ��������:
 *   �������� ������� HTML-������ � ���� ������
 *
 *
 */
UnicodeString __fastcall TICSDocHTMLCreator::GetHTMLSTYLESDefinition(const UnicodeString & AStyleClassStrPrefix,
  const UnicodeString & AStylePropStrPrefix, TTagNode * ndStyles)
 {
  UnicodeString STYLES = "<!-- Document style sheet -->\n" "<style type='text/css'>\n" "<!--\n";
  TTagNode * ndStyle = ndStyles->GetFirstChild();
  while (ndStyle)
   {
    STYLES += GetHTMLStyleDefinition(ndStyle->AV["uid"], AStyleClassStrPrefix, AStylePropStrPrefix, ndStyle);
    ndStyle = ndStyle->GetNext();
   }
  UnicodeString cssPath = icsPrePath(GetEnvironmentVariable("APPDATA") + "\\ic\\css\\hand_edit_style.css");
  if (FileExists(cssPath))
   {
    TStringList * FlLoad = new TStringList;
    try
     {
      FlLoad->LoadFromFile(cssPath);
      STYLES += FlLoad->Text;
     }
    __finally
     {
      delete FlLoad;
     }
   }
  else
   {
    // td.num
    STYLES += "\n\
 table \n\
 {\n\
   border-collapse:  collapse;\n\
   border: 1px solid #606A9D;\n\
 }\n\
 .firstth \n\
 {\n\
   width: 16px;\n\
 }\n\
 .rowfirstth \n\
 {\n\
   width: 1px;\n\
 }\n\
 td\n\
 {\n\
   position: fixed;\n\
   height: 25px;\n\
   width: 100px;\n\
   border-collapse:  collapse;\n\
   border: 1px solid #99A0C1;\n\
   padding-right: 3px;\n\
   padding-left: 3px;\n\
   text-align: center;\n\
   vertical-align: middle;\n\
   font-weight: bold;\n\
   font-family: Verdana, Geneva, sans-serif;\n\
   background-color: #F5FFF0;\n\
   white-space: nowrap;\n\
 }\n\
 th\n\
 {\n\
   border-collapse:  collapse;\n\
   border: 1px solid #99A0C1;\n\
   padding-right: 3px;\n\
   padding-left: 3px;\n\
   text-align: center;\n\
   vertical-align: middle;\n\
   color: #000000;\n\
   font-weight: bold;                                \
   font-family: Verdana, Geneva, sans-serif;         \
   background-color: #F5FFF0;                        \
   white-space: nowrap;                      \
   position: fixed;\n\
   height: 25px;\n\
 }                                                         \
 th.num \n\
 { \n\
   border-collapse:  collapse;                       \
   border: 1px solid #99A0C1;                        \
   padding-right: 3px;                               \
   padding-left: 3px;                                \
   text-align: center;                               \
   vertical-align: middle;                           \
   font-weight: bold;                                \
   color: #FF0000;                                   \
   width: 100px;                                       \
   font-family: Verdana, Geneva, sans-serif;         \
   background-color: #F5FFF0;                        \
   white-space: nowrap;                      \
   position: fixed;\n\
   height: 25px;\n\
 }                                                         \
 th.numcol\n\
 {                                                      \
   border-collapse:  collapse;                       \
   border: 1px solid #99A0C1;                        \
   padding-right: 3px;                               \
   padding-left: 3px;                                \
   text-align: center;                               \
   vertical-align: middle;                           \
   font-weight: bold;                                \
   color: #FF0000;                                   \
   width: 100px;                                       \
   font-family: Verdana, Geneva, sans-serif;         \
   background-color: #F5FFF0;                        \
   position: fixed;\n\
   height: 25px;\n\
 }                                                         \
 th.numrow \n\
 {                                                      \
   border-collapse:  collapse;                       \
   border: 1px solid #99A0C1;                        \
   padding-right: 3px;                               \
   padding-left: 3px;                                \
   text-align: center;                               \
   vertical-align: middle;                           \
   font-weight: bold;                                \
   color: #FF0000;                                   \
//           width: 100px !impotent;                                       \
   font-family: Verdana, Geneva, sans-serif;         \
   background-color: #F5FFF0;                        \
 }                                                         \
 td input\n\
 {                                                   \
   vertical-align: middle;                           \
   border: 1px solid #36F;                           \
   text-align: right;                                \
   font-family: Verdana, Geneva, sans-serif;\n         \
   display: none;\n                                    \
   white-space: nowrap;                      \
 }                                                         \
 span input\n\
 {                                                   \
   vertical-align: middle;                           \
   border: 0px solid #F5FFF0;                           \
   background-color: #F5FFF0;                        \
   text-align: right;                                \
   font-family: Verdana, Geneva, sans-serif;         \
   display: none;                                     \
   white-space: nowrap;                      \
 }                                                         \
 .disinput \n\
 {                                                   \
   vertical-align: middle;                           \
   border: 0px solid #F5FFF0;                           \
   background-color: #F5FFF0;                        \
   text-align: right;                                \
   font-family: Verdana, Geneva, sans-serif;         \
   display: none;                                     \
   white-space: nowrap;                      \
 }                                                         \
 .cLM \n\
 {                                                    \
   text-align: left;                                 \
   vertical-align: middle;                           \
   font-weight: bold;                                \
   font-family: Verdana, Geneva, sans-serif;         \
   background-color: #DCE2ED;                        \
 }                                                         \
 .cCMW \n\
 {                                                   \
   text-align: center;                               \
   vertical-align: middle;                           \
   color: white;                                     \
   font-weight: bold;                                \
   font-family: Verdana, Geneva, sans-serif;         \
   background-color: #DCE2ED;                        \
 }";
   }
  STYLES += "-->\n</style>";
  return STYLES;
 }
// ---------------------------------------------------------------------------
// ������������ HTML-����� � ���� ������
void __fastcall TICSDocHTMLCreator::CreateHTML(TICSDocHTMLCreatorPreviewType APreviewType)
 {
  UnicodeString TITLE = "";
  UnicodeString META = "<meta name='Generator' content='" + ClassName() +
    " V1'>\n" "<meta name='Description' content='";
  switch (APreviewType)
   {
   case ptSpecificatorEx:
    TITLE = FSpec->GetChildByName("passport")->AV["mainname"];
    META += "specificator view";
    break;
   case ptDocumentEx:
    TITLE = FDoc->GetChildByName("passport")->AV["mainname"];
    META += "document view";
    break;
   }
  META += "'>";
  UnicodeString STYLES = "";
  META += "\n";
  STYLES += GetHTMLSTYLESDefinition("\t", "\t  ", FSpec->GetChildByName("styles", true));
  try
   {
    FndHTML->OnProgressInit     = CreateHTMLProgressInit;
    FndHTML->OnProgress         = CreateHTMLProgressChange;
    FndHTML->OnProgressComplite = CreateHTMLProgressComplite;
    FHTML                       = FndHTML->AsHTML(TITLE, META + STYLES, false, 2, false);
   }
  __finally
   {
    FndHTML->OnProgressInit     = NULL;
    FndHTML->OnProgress         = NULL;
    FndHTML->OnProgressComplite = NULL;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::CreateHTMLProgressInit(int AMax, UnicodeString AMsg)
 {
  // InitPB();
  __PBMax = AMax;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::CreateHTMLProgressChange(int APercent, UnicodeString AMsg)
 {
  __PBPosition = APercent;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::CreateHTMLProgressComplite(UnicodeString AMsg)
 {
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ������-��������� ��� ������� ( xml-��� "table" ������������� )
 * ( ������������� ����������� ��������� ����� html-�������, ���� ������� ���������� )
 *
 *
 * ���������:
 *   [in]  ACellTagNodeName - ��� �������������� xml-����� ( "tcol" ��� "trow" )
 *   [in]  ndCellTagNode    - xml-���, ��������������� ������-��������� �������
 *   [int] htmlRow          - ������� ������ html-������� ( html-��� "tr" )
 *
 * ������������ ��������:
 *   ������-��������� ( html-��� "th" ) �������
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *      __RowNum
 *
 */
TTagNode * __fastcall TICSDocHTMLCreator::CreateHTMLTableHeaderCell(const UnicodeString & ACellTagNodeName,
  TTagNode * ndCellTagNode, TTagNode * htmlRow)
 {
  if (CheckIfTerminated())
   return NULL;
  TTagNode * htmlTH = NULL;
  if (ndCellTagNode->CmpName(ACellTagNodeName))
   {
    // ���������� ������ ��������� �������/������
    htmlTH = htmlRow->AddChild("th");
    // ��������� ������ ����� � ������ �������
    if ((ACellTagNodeName == "tcol") && !ndCellTagNode->GetChildByName("tcol"))
     SetHTMLBoxWidthValues(htmlTH, __BorderWidth, TICSDocSpec::GetSizeAttrVal(ndCellTagNode, "width"));
    else
     SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
    TTagNode * htmlTHData = CreateHTMLTextFormat(htmlTH, GetTableCellStyle("hstyle", ndCellTagNode));
    TAttr * attPCDATA = htmlTHData->AddAttr("PCDATA", GetTableHeaderCellText(ndCellTagNode));
    attPCDATA->Ref = ndCellTagNode;
    // ������������ ��� ���������� ����� ��� ����� ������� � ������� HTML
    ndCellTagNode->Ctrl = reinterpret_cast<TWinControl *>(htmlRow);
    // ������ �������/�����
    if (ndCellTagNode->GetChildByName(ACellTagNodeName))
     {
      int nTableSingleElCount;
      UnicodeString SpanAttrName;
      if (ACellTagNodeName == "tcol")
       {
        nTableSingleElCount = CountTagNodes(ndCellTagNode, TICSDocSpec::IsTableColSingle);
        SpanAttrName        = "colspan";
       }
      else // trow
       {
        nTableSingleElCount = CountTagNodes(ndCellTagNode, TICSDocSpec::IsTableRowSingle);
        SpanAttrName        = "rowspan";
       }
      htmlTH->AV[SpanAttrName] = IntToStr(nTableSingleElCount);
     }
    // �������/������
    else
     {
      UnicodeString ElTopParentName, SpanAttrName;
      if (ACellTagNodeName == "tcol")
       {
        ElTopParentName = "columns";
        SpanAttrName    = "rowspan";
       }
      else
       {
        ElTopParentName = "rows";
        SpanAttrName    = "colspan";
       }
      int nTableElMaxLevel = GetTagNodesMaxLevel(ndCellTagNode->GetParent(ElTopParentName), ACellTagNodeName);
      int nMaxDepth = nTableElMaxLevel - ndCellTagNode->Level + 1;
      if (nMaxDepth > 1)
       htmlTH->AV[SpanAttrName] = IntToStr(nMaxDepth);
      // ���������� ������ � ������� ������
      if ((ACellTagNodeName == "trow") && __RowNumbers)
       {
        TTagNode * htmlTH = htmlRow->AddChild("th");
        htmlTH->AV["class"] = "throw";
        SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
        TTagNode * htmlTHData = CreateHTMLTableNumRowTextFormat(htmlTH);
        UnicodeString S;
        int nSingleRowCount = CountTagNodes(ndCellTagNode->GetParent("rows"), TICSDocSpec::IsTableRowSingle);
        S.printf(("%0" + IntToStr(IntToStr(nSingleRowCount).Length()) + "u").c_str(), __RowNum++);
        htmlTHData->AV["PCDATA"] = S;
       }
     }
    // IncPB();
   }
  return htmlTH;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TICSDocHTMLCreator::CreateHTMLTableHeaderCellEx(const UnicodeString & ACellTagNodeName,
  TTagNode * ndCellTagNode, TTagNode * htmlRow)
 {
  if (CheckIfTerminated())
   return NULL;
  TTagNode * htmlTH = NULL;
  if (ndCellTagNode->CmpName(ACellTagNodeName))
   {
    // ���������� ������ ��������� �������/������
    htmlTH              = htmlRow->AddChild("th");
    htmlTH->AV["class"] = "throw";
    // ��������� ������ ����� � ������ �������
    if ((ACellTagNodeName == "tcol") && !ndCellTagNode->GetChildByName("tcol"))
     SetHTMLBoxWidthValues(htmlTH, __BorderWidth, TICSDocSpec::GetSizeAttrVal(ndCellTagNode, "width"));
    else
     SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
    TTagNode * htmlTHData = CreateHTMLTextFormat(htmlTH, GetTableCellStyle("hstyle", ndCellTagNode));
    htmlTHData->AV["class"] = "rowfirstth";
    TAttr * attPCDATA = htmlTHData->AddAttr("PCDATA", "" /* GetTableHeaderCellText( ndCellTagNode ) */);
    attPCDATA->Ref = ndCellTagNode;
    // ������������ ��� ���������� ����� ��� ����� ������� � ������� HTML
    ndCellTagNode->Ctrl = reinterpret_cast<TWinControl *>(htmlRow);
    // ������ �������/�����
    if (ndCellTagNode->GetChildByName(ACellTagNodeName))
     {
      int nTableSingleElCount;
      UnicodeString SpanAttrName;
      if (ACellTagNodeName == "tcol")
       {
        nTableSingleElCount = CountTagNodes(ndCellTagNode, TICSDocSpec::IsTableColSingle);
        SpanAttrName        = "colspan";
       }
      else // trow
       {
        nTableSingleElCount = CountTagNodes(ndCellTagNode, TICSDocSpec::IsTableRowSingle);
        SpanAttrName        = "rowspan";
       }
      htmlTH->AV[SpanAttrName] = IntToStr(nTableSingleElCount);
     }
    // �������/������
    else
     {
      UnicodeString ElTopParentName, SpanAttrName;
      if (ACellTagNodeName == "tcol")
       {
        ElTopParentName = "columns";
        SpanAttrName    = "rowspan";
       }
      else
       {
        ElTopParentName = "rows";
        SpanAttrName    = "colspan";
       }
      int nTableElMaxLevel = GetTagNodesMaxLevel(ndCellTagNode->GetParent(ElTopParentName), ACellTagNodeName);
      int nMaxDepth = nTableElMaxLevel - ndCellTagNode->Level + 1;
      if (nMaxDepth > 1)
       htmlTH->AV[SpanAttrName] = IntToStr(nMaxDepth);
     }
    // IncPB();
   }
  return htmlTH;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����� ��� �������� ������� ( xml-��� "table" ������������� )
 *
 *
 * ���������:
 *   [in]      ndCol     - xml-��� "columns" �������������
 *   [in\out]  htmlTable - ������� ( html-��� "table", ��������������� xml-���� "table" ������������� )
 *   [int]     htmlRow   - ������� ������ html-������� ( html-��� "tr" )
 *                         ������������ ��� ������ ������� ����� ������������ ��������, ��� ������
 *                         ������� ������ ���� NULL
 *                         [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *      __ColNumbers
 *
 */
void __fastcall TICSDocHTMLCreator::CreateHTMLTableHeaderCols(TTagNode * ndCol, TTagNode * htmlTable,
  TTagNode * htmlRow)
 {
  if (CheckIfTerminated())
   return;
  CreateHTMLTableHeaderCell("tcol", ndCol, htmlRow);
  TTagNode * ndChild = ndCol->GetChildByName("tcol");
  while (ndChild && ndChild->CmpName("tcol"))
   {
    // ��� ������ ������� � ������ ( ��� ��� ����� ������ ������� )
    if (TICSDocSpec::IsTableFirstCol(ndChild))
     {
      // ��� ����� ������ �������
      if (ndChild->GetParent()->CmpName("columns"))
       {
        // ���������� ����� ������ ��� HTML-�������
        htmlRow = htmlTable->AddChild("tr");
        // ���������� ����� ����� ������� ������ ����� ������� ( ��� ������ �������� ������ )
        TTagNode * htmlTH = htmlRow->AddChild("th");
        htmlTH->AV["class"] = "throw";
        SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
        TTagNode * ndTableSection = ndChild->GetParent("table")->GetChildByName("columns");
        int nTableElMaxLevel = GetTagNodesMaxLevel(ndTableSection, "tcol");
        htmlTH->AV["rowspan"] = nTableElMaxLevel - ndTableSection->Level +static_cast<int>(__ColNumbers);
        ndTableSection        = ndChild->GetParent("table")->GetChildByName("rows");
        nTableElMaxLevel      = GetTagNodesMaxLevel(ndTableSection, "trow");
        htmlTH->AV["colspan"] = nTableElMaxLevel - ndTableSection->Level +static_cast<int>(__RowNumbers);
        htmlTH->AV["PCDATA"]  = "&nbsp;";
        htmlTH->AV["class"]   = "firstth";
       }
      // ���������� htmlRow
      else
       {
        int nRowInd = ndChild->Level - ndChild->GetParent("columns")->Level;
        int i = 0;
        TTagNode * htmlTR = htmlTable->GetFirstChild();
        while (htmlTR && (++i < nRowInd))
         htmlTR = htmlTR->GetNext();
        htmlRow = (htmlTR) ? htmlTR : htmlTable->AddChild("tr");
       }
     }
    CreateHTMLTableHeaderCols(ndChild, htmlTable, htmlRow);
    ndChild = ndChild->GetNext();
   }
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����� ��� �������� ������� ( xml-��� "table" ������������� ) � ���������� ��������
 *
 *
 * ���������:
 *   [in]      ndColumns         - xml-��� "columns" �������������
 *   [in\out]  htmlTable         - ������� ( html-��� "table", ��������������� xml-���� "table" ������������� )
 *   [out]     ATableSingleCols  - ������ "��������"-����� ����� ��� ��������
 *                                 ������� ( xml-��� "table" ������������� ), �.�. ����� �����,
 *                                 � ������� ��� ����������� ��� �������� �����-����������
 *                                 [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __ColNumbers
 *      __ColNum
 *
 */
void __fastcall TICSDocHTMLCreator::CreateHTMLTableCols(TTagNode * ndColumns, TTagNode * htmlTable,
  TTagNodeList * ATableSingleCols)
 {
  if (CheckIfTerminated())
   return;
  CreateHTMLTableHeaderCols(ndColumns, htmlTable);
  TTagNodeList TableSingleCols;
  TICSDocSpec::GetTableSingleColsList(ndColumns, & TableSingleCols);
  // ���������� ����� � �������� ��������
  if (__ColNumbers)
   {
    TTagNode * htmlRow = htmlTable->AddChild("tr");
    for (TTagNodeList::const_iterator i = TableSingleCols.begin(); i != TableSingleCols.end(); i++)
     {
      TTagNode * htmlTH = htmlRow->AddChild("th");
      htmlTH->AV["class"] = "thcol";
      SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
      TTagNode * htmlTHData = CreateHTMLTableNumColTextFormat(htmlTH);
      UnicodeString S;
      S.printf(("%0" + IntToStr(IntToStr((int)TableSingleCols.size()).Length()) + "u").c_str(), __ColNum++);
      htmlTHData->AV["PCDATA"] = S;
     }
   }
  if (ATableSingleCols)
   * ATableSingleCols = TableSingleCols;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::CreateHTMLTableDataCols(TTagNode * ndColumns, TTagNode * htmlTable,
  TTagNodeList * ATableSingleCols)
 {
  if (CheckIfTerminated())
   return;
  TTagNodeList TableSingleCols;
  TICSDocSpec::GetTableSingleColsList(ndColumns, & TableSingleCols);
  if (ATableSingleCols)
   * ATableSingleCols = TableSingleCols;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����� ��� ����� ������� ( xml-��� "table" ������������� )
 * � ����� ������ � ���� �������
 *
 *
 * ���������:
 *   [in]     ndRow            - xml-��� "rows" �������������
 *   [in\out] htmlTable        - ������� ( html-��� "table", ��������������� xml-���� "table" ������������� )
 *   [in]     ATableSingleCols - ������ "��������"-����� ����� ��� ��������
 *                               ������� ( xml-��� "table" ������������� ), �.�. ����� �����,
 *                               � ������� ��� ����������� ��� �������� �����-����������
 *   [out]    DataCells        - ������ ������� ( ���� 2-������� ������� ) ���������� �� ������
 *                               ������ �������
 *   [int]    htmlRow          - ������� ������ html-������� ( html-��� "tr" )
 *                               ������������ ��� ������ ������� ����� ������������ ��������, ��� ������
 *                               ������� ������ ���� NULL
 *                               [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *
 */
void __fastcall TICSDocHTMLCreator::CreateHTMLTableRows(TTagNode * ndRow, TTagNode * htmlTable,
  const TTagNodeList & ATableSingleCols, TTagNodeList2 * DataCells, TTagNode * htmlRow)
 {
  if (CheckIfTerminated())
   return;
  if (CreateHTMLTableHeaderCell("trow", ndRow, htmlRow) && !ndRow->GetChildByName("trow"))
   {
    // ���������� ����� ��� ������
    TTagNodeList DataCellStr;
    UnicodeString FEDName;
    FTabColNum = -1;
    for (TTagNodeList::const_iterator i = ATableSingleCols.begin(); i != ATableSingleCols.end(); i++)
     {
      FTabColNum++ ;
      TTagNode * htmlTD = htmlRow->AddChild("td");
      SetHTMLTableCellBorderWidth(htmlTD, __BorderWidth);
      TTagNode * htmlTDData = CreateHTMLTextFormat(htmlTD, GetTableCellStyle("bstyle", *i, ndRow));
      TTagNode * htmlInputData = NULL;
      FEDName = IntToStr(FTabNum) + "." + IntToStr(FTabRowNum) + ":" + IntToStr(FTabColNum) + "#";
      // name = Ntab.Nrow:NCol#(in|dis)%uid
      if ((*i)->GetChildByName("calcrules") || ndRow->GetChildByName("calcrules"))
       {
        htmlInputData             = htmlTDData->AddChild("span");
        htmlInputData             = htmlInputData->AddChild("input");
        htmlInputData->AV["name"] = FEDName + "dis%" + (*i)->AV["uid"];
        htmlInputData->AV["id"]   = htmlInputData->AV["name"];
       }
      else
       {
        htmlInputData                  = htmlTDData->AddChild("input");
        htmlInputData->AV["name"]      = FEDName + "in%" + (*i)->AV["uid"];
        htmlInputData->AV["type"]      = "text";
        htmlInputData->AV["id"]        = htmlInputData->AV["name"];
        htmlInputData->AV["value"]     = "0";
        htmlInputData->AV["size"]      = "11";
        htmlInputData->AV["maxlength"] = "8";
       }
      if (DataCells && htmlInputData)
       DataCellStr.push_back(htmlInputData);
     }
    if (DataCells)
     DataCells->push_back(DataCellStr);
   }
  TTagNode * ndChild = ndRow->GetChildByName("trow");
  while (ndChild && ndChild->CmpName("trow"))
   {
    // ���������� htmlRow
    if (!htmlRow || !TICSDocSpec::IsTableFirstRow(ndChild))
     {
      FTabRowNum++ ;
      htmlRow = htmlTable->AddChild("tr");
     }
    else if (TICSDocSpec::IsTableFirstRow(ndChild))
     htmlRow = reinterpret_cast<TTagNode *>(ndChild->GetParent()->Ctrl);
    CreateHTMLTableRows(ndChild, htmlTable, ATableSingleCols, DataCells, htmlRow);
    ndChild = ndChild->GetNext();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::CreateHTMLTableDataRows(TTagNode * ndRow, TTagNode * htmlTable,
  const TTagNodeList & ATableSingleCols, TTagNodeList2 * DataCells, TTagNode * htmlRow)
 {
  if (CheckIfTerminated())
   return;
  if (CreateHTMLTableHeaderCellEx("trow", ndRow, htmlRow) && !ndRow->GetChildByName("trow"))
   {
    // ���������� ����� ��� ������
    TTagNodeList DataCellStr;
    UnicodeString FEDName;
    FTabColNum = -1;
    for (TTagNodeList::const_iterator i = ATableSingleCols.begin(); i != ATableSingleCols.end(); i++)
     {
      FTabColNum++ ;
      TTagNode * htmlTD = htmlRow->AddChild("td");
      // SetHTMLTableCellBorderWidth( htmlTD, __BorderWidth );
      TTagNode * htmlTDData = CreateHTMLTextFormat(htmlTD, GetTableCellStyle("bstyle", *i, ndRow));
      TTagNode * htmlInputData = NULL;
      // FEDName = IntToStr(FTabNum)+"."+IntToStr(FTabRowNum)+":"+IntToStr(FTabColNum)+"#";
      // name = Ntab.Nrow:NCol#(in|dis)%uid
      if ((*i)->GetChildByName("calcrules") || ndRow->GetChildByName("calcrules"))
       {
        htmlInputData = htmlTDData; // ->AddChild("span");
        // htmlInputData->AV["name"]   = FEDName+"dis%"+(*i)->AV["uid"];
        htmlInputData->AV["class"] = "distabval";
       }
      else
       {
        htmlTDData->AV["class"]    = "entabtd";
        htmlInputData              = htmlTDData->AddChild("a");
        htmlInputData->AV["href"]  = IntToStr(FTabRowNum) + ":" + IntToStr(FTabColNum);
        htmlInputData->AV["class"] = "entabval";
       }
      htmlInputData->AV["PCDATA"] = "0";
      if (DataCells && htmlInputData)
       DataCellStr.push_back(htmlInputData);
     }
    if (DataCells)
     DataCells->push_back(DataCellStr);
   }
  TTagNode * ndChild = ndRow->GetChildByName("trow");
  // TTagNode *itRow = NULL;
  while (ndChild && ndChild->CmpName("trow"))
   {
    // ���������� htmlRow
    if (!htmlRow || !TICSDocSpec::IsTableFirstRow(ndChild))
     {
      FTabRowNum++ ;
      htmlRow = htmlTable->AddChild("tr");
     }
    else if (TICSDocSpec::IsTableFirstRow(ndChild))
     htmlRow = reinterpret_cast<TTagNode *>(ndChild->GetParent()->Ctrl);
    CreateHTMLTableDataRows(ndChild, htmlTable, ATableSingleCols, DataCells, htmlRow);
    ndChild = ndChild->GetNext();
   }
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����������� ������� ( xml-��� "table" ������������� )
 *
 *
 * ���������:
 *   [in]  ndTable    - xml-��� "table" �������������
 *   [in]  htmlParent - html-��� - ������ ��� �������������� html-�������
 *   [out] DataCells  - ������ ������� ( ���� 2-������� ������� ) ���������� �� ������
 *                      ������ �������
 *                      [default = NULL]
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������� ( xml-��� "table" ������������� )
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *      __ColNumbers
 *      __RowNum
 *      __ColNum
 *
 */
TTagNode * __fastcall TICSDocHTMLCreator::AddHTMLTable(TTagNode * ndTable, TTagNode * htmlParent,
  TTagNodeList2 * DataCells)
 {
  __BorderWidth = ndTable->AV["border"];
  __Width       = ndTable->AV["width"];
  __RowNumbers  = ndTable->CmpAV("rownumbers", 1);
  __ColNumbers  = ndTable->CmpAV("colnumbers", 1);
  __RowNum      = 1;
  __ColNum      = 1;
  TTagNode * htmlTable = AddHTMLTableTag(htmlParent);
  TTagNodeList TableSingleCols;
  CreateHTMLTableCols(ndTable->GetChildByName("columns"), htmlTable, & TableSingleCols);
  if (DataCells)
   DataCells->clear();
  FTabRowNum = -1;
  CreateHTMLTableRows(ndTable->GetChildByName("rows"), htmlTable, TableSingleCols, DataCells);
  FTabNum++ ;
  return htmlTable;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TICSDocHTMLCreator::AddHTMLTableNoHead(TTagNode * ndTable, TTagNode * htmlParent,
  TTagNodeList2 * DataCells)
 {
  __BorderWidth = ndTable->AV["border"];
  __Width       = ndTable->AV["width"];
  __RowNumbers  = ndTable->CmpAV("rownumbers", 1);
  __ColNumbers  = ndTable->CmpAV("colnumbers", 1);
  __RowNum      = 1;
  __ColNum      = 1;
  TTagNode * htmlTable = AddHTMLTableTag(htmlParent);
  TTagNodeList TableSingleCols;
  CreateHTMLTableDataCols(ndTable->GetChildByName("columns"), htmlTable, & TableSingleCols);
  if (DataCells)
   DataCells->clear();
  FTabRowNum = -1;
  CreateHTMLTableDataRows(ndTable->GetChildByName("rows"), htmlTable, TableSingleCols, DataCells);
  FTabNum++ ;
  return htmlTable;
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����������� ������� ( xml-��� "inscription" ������������� )
 *
 *
 * ���������:
 *   [in]  ndInscription - xml-��� "inscription" �������������
 *   [in]  htmlParent    - html-��� - ������ ��� ��������������� �����������
 *
 * ������������ ��������:
 *   html-���, ��������������� ����������� ������� ( xml-��� "inscription" ������������� )
 *
 */
TTagNode * __fastcall TICSDocHTMLCreator::AddHTMLInscription(TTagNode * ndInscription, TTagNode * htmlParent)
 {
  if (ndInscription->CmpAV("newline", "1"))
   htmlParent->AddChild("br");
  return htmlParent->AddChild("div");
 }
// ---------------------------------------------------------------------------
/*
 * ������������ ����������� �������� ������� ( xml-��� "instext" ������������� )
 *
 *
 * ���������:
 *   [in]     ndInsText  - xml-��� "instext" �������������
 *   [in\out] htmlParent - html-��� - ������ ��� ��������������� �����������
 *
 * ������������ ��������:
 *   html-���, ��������������� ����������� �������� ������� ( xml-��� "instext" ������������� )
 *
 */
TTagNode * __fastcall TICSDocHTMLCreator::AddHTMLInsText(TTagNode * ndInsText, TTagNode ** htmlParent)
 {
  if (ndInsText->CmpAV("newline", "1"))
   * htmlParent = (*htmlParent)->AddChild("div");
  UnicodeString AlignAttrName, AlignAttrVal;
  GetHTML_DIVAlignAttrVal(GetInsTextAlign(ndInsText), AlignAttrName, AlignAttrVal);
  (*htmlParent)->AV[AlignAttrName] = AlignAttrVal;
  TTagNode * htmlInsText = (*htmlParent)->AddChild("span");
  return CreateHTMLTextFormat(htmlInsText, GetInsTextStyle(ndInsText));
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::CreateSpecFirstTabHTML()
 {
  CreateHTMLProgressInit(GetMaxPBForSpec(), "");
  TTagNode * htmlBodyRoot = InitNDHTML();
  TTagNode * ndDocElement = FSpec->GetChildByName("doccontent")->GetFirstChild();
  while (ndDocElement)
   {
    if (CheckIfTerminated())
     return;
    if (ndDocElement->CmpName("table")) // �������
     {
      AddHTMLTable(ndDocElement, htmlBodyRoot);
      ndDocElement = NULL;
     }
    else
     ndDocElement = ndDocElement->GetNext();
    // IncPB();
   }
  CreateHTML(ptSpecificatorEx);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::CreateTabRowHeadHTML()
 {
  CreateHTMLProgressInit(GetMaxPBForSpec(), "");
  TTagNode * htmlBodyRoot = InitNDHTML("rowbody");
  TTagNode * ndDocElement = FSpec->GetChildByName("doccontent")->GetFirstChild();
  while (ndDocElement)
   {
    if (CheckIfTerminated())
     return;
    if (ndDocElement->CmpName("table"))
     {
      __BorderWidth = ndDocElement->AV["border"];
      __Width       = ndDocElement->AV["width"];
      __RowNumbers  = ndDocElement->CmpAV("rownumbers", 1);
      __ColNumbers  = ndDocElement->CmpAV("colnumbers", 1);
      __RowNum      = 1;
      __ColNum      = 1;
      TTagNode * htmlTable = AddHTMLTableTag(htmlBodyRoot, "rowtab");
      TTagNodeList TableSingleCols;
      FTabRowNum = -1;
      TableSingleCols.clear();
      CreateHTMLTableRows(ndDocElement->GetChildByName("rows"), htmlTable, TableSingleCols, NULL);
      ndDocElement = NULL;
      htmlBodyRoot->AddChild("div")->AV["class"] = "rowdiv";
     }
    else
     ndDocElement = ndDocElement->GetNext();
    // IncPB();
   }
  CreateHTML(ptSpecificatorEx);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::CreateDocFirstTabHTML()
 {
  CreateHTMLProgressInit(GetMaxPBForDoc(), "");
  TTagNode * htmlBodyRoot = InitNDHTML();
  TTagNode * ndObj = FDoc->GetChildByName("contens")->GetFirstChild();
  while (ndObj)
   {
    if (CheckIfTerminated())
     return;
    // ��������� ����� "lv" ������� ������ - ��������� ���������
    TTagNode * ndObjRef = FSpec->GetTagByUID(ndObj->AV["uidmain"]);
    if (ndObjRef->CmpName("table")) // �������
     {
      TTagNodeList2 DataCells;
      AddHTMLTableNoHead(ndObjRef, htmlBodyRoot, & DataCells);
      // ����������� �������� �� ���������
      TTagNode * ndTableRow = ndObj->GetFirstChild();
      TTagNodeList2::iterator itDataCells = DataCells.begin();
      while (ndTableRow)
       {
        if (CheckIfTerminated())
         return;
        // ��������� ����� "lv" ������� ������ - ����� �������
        TTagNode * ndTableData = ndTableRow->GetFirstChild();
        TTagNodeList::iterator itDataCellStr = (*itDataCells).begin();
        while (ndTableData)
         {
          if (CheckIfTerminated())
           return;
          // ��������� ����� "iv" - ����� ������ �������
          UnicodeString CellText = ndTableData->AV["val"];
          // ������ ����������� ������� ��������
          NormalizeNumericalValue(CellText, true /* ndObjRef->CmpAV("showzval", "1") */);
          if ((*itDataCellStr))
           (*itDataCellStr)->AV["PCDATA"] = CellText;
          ndTableData = ndTableData->GetNext();
          itDataCellStr++ ;
         }
        ndTableRow = ndTableRow->GetNext();
        itDataCells++ ;
        // IncPB();
       }
      // htmlBodyRoot->AddChild( "br" );
      // htmlBodyRoot->AddChild( "span","id=sss,name=sss");
      ndObj = NULL;
     }
    else
     ndObj = ndObj->GetNext();
    // IncPB();
   }
  CreateHTML(ptDocumentEx);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocHTMLCreator::PreviewFirstTab(TAxeXMLContainer * AXMLContainer, TTagNode * ASpecificator,
  TTagNode * ADocument)
 {
  UnicodeString RC = "";
  try
   {
    // �������������
    // InitPB();
    FTabNum = 0; // ������� ����� �������
    // ������������ �����������
    FXMLContainer = AXMLContainer;
    FSpec         = ASpecificator;
    FDoc          = ADocument;
    if (!FSpec)
     throw DKClasses::EMethodError(__FUNC__, icsDocViewerExErrorNoSpecDef);
    if (FDoc)
     { // ptDocumentEx
      if (FDoc->GetChildByName("passport")->AV["GUIspecificator"].UpperCase() != FSpec->GetChildByName("passport")
        ->AV["GUI"].UpperCase())
       throw DKClasses::EMethodError(__FUNC__, icsDocViewerExErrorGUINotValid);
      CreateDocFirstTabHTML();
     }
    else
     { // ptSpecificatorEx
      CreateSpecFirstTabHTML();
     }
    RC = FHTML;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::GetFirstTabCalcCell(TAxeXMLContainer * AXMLContainer, TTagNode * ASpecificator,
  TStringList * AList)
 {
  try
   {
    FXMLContainer = AXMLContainer;
    FSpec         = ASpecificator;
    if (!FSpec)
     throw DKClasses::EMethodError(__FUNC__, icsDocViewerExErrorNoSpecDef);
    TTagNode * ndTable = FSpec->GetChildByName("table", true);
    if (ndTable) // �������
     {
      TTagNodeList TableSingleCols;
      TICSDocSpec::GetTableSingleColsList(ndTable->GetChildByName("columns"), & TableSingleCols);
      FTabRowNum = -1;
      GetTableCalcRows(ndTable->GetChildByName("rows"), TableSingleCols, AList);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::GetTabHeaderStr(TAxeXMLContainer * AXMLContainer, TTagNode * ASpecificator,
  TStringList * ARowList, TStringList * AColList)
 {
  try
   {
    FXMLContainer = AXMLContainer;
    FSpec         = ASpecificator;
    ARowList->Clear();
    AColList->Clear();
    if (!FSpec)
     throw DKClasses::EMethodError(__FUNC__, icsDocViewerExErrorNoSpecDef);
    TTagNode * ndTable = FSpec->GetChildByName("table", true);
    if (ndTable) // �������
     {
      TTagNode * itNode;
      itNode = ndTable->GetChildByName("rows");
      if (itNode)
       {
        itNode = itNode->GetChildByName("trow");
        while (itNode && itNode->CmpName("trow"))
         {
          GetTableRowsStr(itNode, ARowList, "");
          itNode = itNode->GetNext();
         }
       }
      itNode = ndTable->GetChildByName("columns");
      if (itNode)
       {
        itNode = itNode->GetChildByName("tcol");
        while (itNode && itNode->CmpName("tcol"))
         {
          GetTableColsStr(itNode, AColList, "");
          itNode = itNode->GetNext();
         }
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::GetTableRowsStr(TTagNode * ARow, TStringList * ARowList,
  const UnicodeString & ACapt)
 {
  if (CheckIfTerminated())
   return;
  UnicodeString FCapt = ACapt;
  if (FCapt.Length())
   FCapt += " | " + ARow->AV["name"];
  else
   FCapt += ARow->AV["name"];
  if (!ARow->GetChildByName("trow"))
   {
    ARowList->Add(FCapt);
   }
  TTagNode * ndChild = ARow->GetChildByName("trow");
  while (ndChild && ndChild->CmpName("trow"))
   {
    GetTableRowsStr(ndChild, ARowList, FCapt);
    ndChild = ndChild->GetNext();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::GetTableColsStr(TTagNode * ACol, TStringList * AColList,
  const UnicodeString & ACapt)
 {
  if (CheckIfTerminated())
   return;
  UnicodeString FCapt = ACapt;
  if (FCapt.Length())
   FCapt += " | " + ACol->AV["name"];
  else
   FCapt += ACol->AV["name"];
  if (!ACol->GetChildByName("tcol"))
   {
    AColList->Add(FCapt);
   }
  TTagNode * ndChild = ACol->GetChildByName("tcol");
  while (ndChild && ndChild->CmpName("tcol"))
   {
    GetTableColsStr(ndChild, AColList, FCapt);
    ndChild = ndChild->GetNext();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::GetTableCalcRows(TTagNode * ndRow, const TTagNodeList & ATableSingleCols,
  TStringList * ACalcList)
 {
  if (CheckIfTerminated())
   return;
  if (!ndRow->GetChildByName("trow"))
   {
    // ���������� ����� ��� ������
    FTabRowNum++ ;
    FTabColNum = -1;
    for (TTagNodeList::const_iterator i = ATableSingleCols.begin(); i != ATableSingleCols.end(); i++)
     {
      FTabColNum++ ;
      if ((*i)->GetChildByName("calcrules") || ndRow->GetChildByName("calcrules"))
       {
        ACalcList->Add(IntToStr(FTabRowNum) + ":" + IntToStr(FTabColNum));
       }
     }
   }
  TTagNode * ndChild = ndRow->GetChildByName("trow");
  while (ndChild && ndChild->CmpName("trow"))
   {
    GetTableCalcRows(ndChild, ATableSingleCols, ACalcList);
    ndChild = ndChild->GetNext();
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocHTMLCreator::PreviewTabRowHead(TAxeXMLContainer * AXMLContainer,
  TTagNode * ASpecificator)
 {
  UnicodeString RC = "";
  try
   {
    // �������������
    // InitPB();
    FTabNum = 0; // ������� ����� �������
    // ������������ �����������
    FXMLContainer = AXMLContainer;
    FSpec         = ASpecificator;
    FDoc          = NULL;
    if (!FSpec)
     throw DKClasses::EMethodError(__FUNC__, icsDocViewerExErrorNoSpecDef);
    CreateTabRowHeadHTML();
    RC = FHTML;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #             ������������ ���� �������� �� ������ �������                   #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocHTMLCreator::PreviewInsc(TAxeXMLContainer * AXMLContainer, TTagNode * ASpecificator,
  TTagNode * ADocument)
 {
  UnicodeString RC = "";
  try
   {
    // �������������
    // InitPB();
    FTabNum = 0; // ������� ����� �������
    // ������������ �����������
    FXMLContainer = AXMLContainer;
    FSpec         = ASpecificator;
    FDoc          = ADocument;
    if (!FSpec)
     throw DKClasses::EMethodError(__FUNC__, icsDocViewerExErrorNoSpecDef);
    if (FDoc)
     { // ptDocumentEx
      if (FDoc->GetChildByName("passport")->AV["GUIspecificator"].UpperCase() != FSpec->GetChildByName("passport")
        ->AV["GUI"].UpperCase())
       throw DKClasses::EMethodError(__FUNC__, icsDocViewerExErrorGUINotValid);
      CreateFirstInscDocHTML();
     }
    else
     { // ptSpecificatorEx
      CreateFirstInscSpecHTML();
     }
    RC = FHTML;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::CreateFirstInscDocHTML()
 {
  CreateHTMLProgressInit(GetMaxPBForDoc(), "");
  TTagNode * htmlBodyRoot = InitNDHTML();
  TTagNode * ndObj = FDoc->GetChildByName("contens")->GetFirstChild();
  while (ndObj)
   {
    if (CheckIfTerminated())
     return;
    // ��������� ����� "lv" ������� ������ - ��������� ���������
    TTagNode * ndObjRef = FSpec->GetTagByUID(ndObj->AV["uidmain"]);
    if (ndObjRef->CmpName("inscription")) // �������
     {
      TTagNode * htmlInsTextCurParent = AddHTMLInscription(ndObjRef, htmlBodyRoot);
      TTagNode * ndInsElement = ndObj->GetFirstChild();
      while (ndInsElement)
       {
        if (CheckIfTerminated())
         return;
        // ��������� ����� "lv" ������� ������ - ��������� �������
        TTagNode * ndInsElementRef = FSpec->GetTagByUID(ndInsElement->AV["uidmain"]);
        TTagNode * htmlInsText = AddHTMLInsText(ndInsElementRef, &htmlInsTextCurParent);
        UnicodeString InsViewText = ndInsElementRef->AV["value"];
        UnicodeString RefText = ndInsElement->GetFirstChild()->AV["val"];
        UnicodeString CntClcRlsText = ndInsElement->GetFirstChild()->GetNext()->AV["val"];
        if (RefText != "")
         {
          if (InsViewText != "")
           InsViewText += " ";
          InsViewText += RefText;
         }
        if (CntClcRlsText != "")
         {
          if (InsViewText != "")
           InsViewText += " ";
          InsViewText += CntClcRlsText;
         }
        if (InsViewText != "")
         htmlInsText->AV["PCDATA"] = InsViewText;
        ndInsElement = ndInsElement->GetNext();
        // IncPB();
       }
      htmlBodyRoot->AddChild("br");
      htmlBodyRoot->AddChild("span", "id=sss,name=sss");
      ndObj = ndObj->GetNext();
     }
    else if (ndObjRef->CmpName("list,table"))
     ndObj = NULL;
   }
  CreateHTML(ptDocumentEx);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::CreateFirstInscSpecHTML()
 {
  CreateHTMLProgressInit(GetMaxPBForSpec(), "");
  TTagNode * htmlBodyRoot = InitNDHTML();
  TTagNode * ndDocElement = FSpec->GetChildByName("doccontent")->GetFirstChild();
  while (ndDocElement)
   {
    if (CheckIfTerminated())
     return;
    if (ndDocElement->CmpName("inscription")) // �������
     {
      TTagNode * htmlInsTextCurParent = AddHTMLInscription(ndDocElement, htmlBodyRoot);
      TTagNode * ndInsText = ndDocElement->GetFirstChild();
      while (ndInsText) // ������� �������
       {
        if (CheckIfTerminated())
         return;
        TTagNode * htmlInsText = AddHTMLInsText(ndInsText, &htmlInsTextCurParent);
        UnicodeString InsViewText = TICSDocSpec::GetInsTagViewText(ndInsText, FXMLContainer);
        if (InsViewText != "� � � � � � �  � � � � � � �  -  � � � � � � � �  � �  � � � � � � � � � � �")
         htmlInsText->AV["PCDATA"] = InsViewText;
        ndInsText = ndInsText->GetNext();
        // IncPB();
       }
      htmlBodyRoot->AddChild("br");
      ndDocElement = ndDocElement->GetNext();
     }
    else if (ndDocElement->CmpName("list,table")) // ������
       ndDocElement = NULL;
   }
  CreateHTML(ptSpecificatorEx);
 }
// ---------------------------------------------------------------------------
// ##############################################################################
// #                                                                            #
// #                     ������������ "�����" ��� �������                       #
// #                                                                            #
// ##############################################################################
// ---------------------------------------------------------------------------
UnicodeString __fastcall TICSDocHTMLCreator::PreviewTabColHead(TAxeXMLContainer * AXMLContainer,
  TTagNode * ASpecificator)
 {
  UnicodeString RC = "";
  try
   {
    // �������������
    // InitPB();
    FTabNum = 0; // ������� ����� �������
    // ������������ �����������
    FXMLContainer = AXMLContainer;
    FSpec         = ASpecificator;
    FDoc          = NULL;
    if (!FSpec)
     throw DKClasses::EMethodError(__FUNC__, icsDocViewerExErrorNoSpecDef);
    CreateTabColHeadHTML();
    RC = FHTML;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::CreateTabColHeadHTML()
 {
  CreateHTMLProgressInit(GetMaxPBForSpec(), "");
  TTagNode * htmlBodyRoot = InitNDHTML("colbody");
  TTagNode * ndDocElement = FSpec->GetChildByName("doccontent")->GetFirstChild();
  while (ndDocElement)
   {
    if (CheckIfTerminated())
     return;
    if (ndDocElement->CmpName("table")) // �������
     {
      // AddHTMLTable( ndDocElement, htmlBodyRoot );
      __BorderWidth = ndDocElement->AV["border"];
      __Width       = ndDocElement->AV["width"];
      __RowNumbers  = ndDocElement->CmpAV("rownumbers", 1);
      __ColNumbers  = ndDocElement->CmpAV("colnumbers", 1);
      __RowNum      = 1;
      __ColNum      = 1;
      TTagNode * htmlTable = AddHTMLTableTag(htmlBodyRoot, "coltab");
      TTagNodeList TableSingleCols;
      CreateHTMLTableHeadCols(ndDocElement->GetChildByName("columns"), htmlTable, & TableSingleCols);
      htmlBodyRoot->AddChild("div")->AV["class"] = "coldiv";
      ndDocElement = NULL;
     }
    else
     ndDocElement = ndDocElement->GetNext();
    // IncPB();
   }
  CreateHTML(ptSpecificatorEx);
 }
// ---------------------------------------------------------------------------
void __fastcall TICSDocHTMLCreator::CreateHTMLTableHeadCols(TTagNode * ndColumns, TTagNode * htmlTable,
  TTagNodeList * ATableSingleCols)
 {
  if (CheckIfTerminated())
   return;
  CreateHTMLTableHeaderCols(ndColumns, htmlTable);
  TTagNodeList TableSingleCols;
  TICSDocSpec::GetTableSingleColsList(ndColumns, & TableSingleCols);
  // ���������� ����� � �������� ��������
  if (__ColNumbers)
   {
    TTagNode * htmlRow = htmlTable->AddChild("tr");
    for (TTagNodeList::const_iterator i = TableSingleCols.begin(); i != TableSingleCols.end(); i++)
     {
      TTagNode * htmlTH = htmlRow->AddChild("th");
      SetHTMLTableCellBorderWidth(htmlTH, __BorderWidth);
      TTagNode * htmlTHData = CreateHTMLTableNumColTextFormat(htmlTH);
      UnicodeString S;
      S.printf(("%0" + IntToStr(IntToStr((int)TableSingleCols.size()).Length()) + "u").c_str(), __ColNum++);
      htmlTHData->AV["PCDATA"] = S;
     }
   }
  if (ATableSingleCols)
   * ATableSingleCols = TableSingleCols;
 }
// ---------------------------------------------------------------------------
