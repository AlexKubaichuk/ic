/*
  ����        - DocHandEdit.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� ��������
                ����� ��� ������� ����� ������ � ��������
  ����������� - ������� �.�.
  ����        - 22.11.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  23.04.2007
    [+] ��������� ������������ ����� �� 3-�� ����� ����� �������

  16.03.2006
    [+] ������ ������������ "�������"
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �� ��������� ������ ������������ "�������" � ������ "��������� ���������"
        ��������
    [*] �����������

  22.06.2005
    1. ��������� ��������� ������� ����� ������� � �������� �������� ������� �����

  20.04.2005
    1. ������������ ����� � grid'� ������������ � ������������ DecimalSeparator
    2. ������� �� ������ ���� "������ ������������" ��������� �������� "������� ����"
    3. ��������� ���� � ����������� �������� ������ ��� ������������ ���������

  09.02.2005
    1. �������� viewer ��������� ���������, ������� ���� �
       �������� ��� ��������� �� ���������

  30.11.2004
    1. ��� ������ ��������� �� ������, �����
       ��������� ������ �� ������������� �������� ��������
       ������� �����, ������ ��������� � ��� ������� ��������
    2. �������� �������� ����������� ������ ����� ��������� ������� �����

  25.11.2004
    1. ��������� ������ ��������� �� ������, �����
       ��������� ������ �� ������������� �������� ��������
       ������� �����

  24.11.2004
    1. �� �������� ���������� ������� ������� ��� �����

  23.11.2004
    1. ��������� ������ ��������� ��� ����� �����, ����������������
       �������, �������� � ������� ������ �������������

  22.11.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef dcDocHandEditH
#define dcDocHandEditH

//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLabel.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"
#include "dxBarExtItems.hpp"
#include "Htmlview.hpp"

//---------------------------------------------------------------------------
#include "ICSDocHandHTMLCreator.h"
#include "ICSDocSpec.h"
#include "cxImage.hpp"
#include "dxGDIPlusClasses.hpp"
#include <Vcl.ComCtrls.hpp>

//#include "ICSDoc.h"	//��������� �������� � ���� ��������

//---------------------------------------------------------------------------
/*
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ActnList.hpp>
#include <ImgList.hpp>
#include <ExtCtrls.hpp>
#include <list.h>
#include <Menus.hpp>
#include <ComCtrls.hpp>

#include "cxGraphics.hpp"
#include "dxBar.hpp"
#include "dxBarExtItems.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGroupBox.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxButtons.hpp"
#include "Htmlview.hpp"
#include "cxCustomData.hpp"
#include "cxInplaceContainer.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxSplitter.hpp"
#include "cxMaskEdit.hpp"
#include "cxLabel.hpp"
#include "cxClasses.hpp"
#include "cxLookAndFeels.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include <System.Actions.hpp>
*/
namespace Dcdochandedit
{

typedef struct tagCellEDData
{
public:
  bool Enabled;
  UnicodeString Text;
  __fastcall tagCellEDData()
  {
    Enabled = false;
    Text = "";
  };
  __fastcall tagCellEDData(const UnicodeString &AText, bool AEnabled)
  {
    Enabled = AEnabled;
    Text = AText;
  };
} TCellEDData;

struct TTCellTagNodes;
template <class DHMData> class TDHMatrix;

//---------------------------------------------------------------------------

class PACKAGE TDocTableData : public TObject
{
private:
    TTagNode *FTabDef;
    TTagNode *FTabDoc;
    TDHMatrix<TTCellTagNodes> *FTCells;  //������� ������ �� ������-���������
    TDHMatrix<TTCellTagNodes> __fastcall FGetTabCells(){return (*FTCells);};  //������� ������ �� ������-���������
    TTagNodeList FSingleRows;
    TTagNodeList FSingleCols;
    int FRowMax, FColMax;
public:
   __fastcall TDocTableData(TTagNode *ATabDef, TTagNode *ATabDoc);
   __fastcall ~TDocTableData();

    TCellEDData& __fastcall GetED(int ARow, int ACol);         //������� ������ �� ������-���������
    void   __fastcall SetED(int ARow, int ACol, TCellEDData AED);

    TTagNode* __fastcall GetTabColNode(int ARow, int ACol);         //������� ������ �� ������-���������
    TTagNode* __fastcall GetTabRowNode(int ARow, int ACol);         //������� ������ �� ������-���������

   __property TDHMatrix<TTCellTagNodes> TabCells = {read = FGetTabCells};
   __property int RowCount = {read = FRowMax};
   __property int ColCount = {read = FColMax};
   __property TTagNode *TabDef = {read = FTabDef};
   __property TTagNode *TabDoc = {read = FTabDoc};
};
//---------------------------------------------------------------------------
typedef map<int, TDocTableData*> TDocTableDataMap;
typedef map<TcxMaskEdit*, UnicodeString> TEDAnsiMap;

//---------------------------------------------------------------------------
class PACKAGE TDocHandTreeList : public TcxTreeList
{
public:

};
//---------------------------------------------------------------------------

//����� ��� ������� ����� ������ � ��������

class PACKAGE TdsDocHandEditForm : public TForm
{
    typedef TForm inherited;

__published:	// IDE-managed Components
    TActionList *ActionList;
    TAction *actSave;
    TAction *actExit;
        TTimer *ShowTimer;
        TcxStyleRepository *cxStyleRepository1;
        TcxStyle *dStyle;
        TPanel *RightScrollPanel;
        TcxStyle *errStyle;
        TcxStyle *comErrStyle;
        TdxBarManager *MainTBM;
        TcxImageList *LMainIL32;
        TdxBarLargeButton *dxBarLargeButton1;
        TdxBarControlContainerItem *ErrPanelContainer;
        TAction *actLRDirection;
        TdxBarLargeButton *dxBarLargeButton2;
        TdxBarLargeButton *dxBarLargeButton3;
        TPanel *ErrPanel;
        TLabel *ErrMsgLab;
        TPanel *Panel3;
        TLabel *ErrRulesLab;
        TLabel *Label1;
        TAction *actTBDirection;
        TdxBarStatic *dxBarStatic1;
        TcxStyle *errDStyle;
        TcxStyle *cxStyle1;
        TPanel *DocViewerPanel;
        TPanel *TabViewerPanel;
        TPanel *ViewerPanel;
        TPanel *TopSizePanel;
        TcxTreeList *TabEdit;
        TcxTreeListColumn *cxTreeList1cxTreeListColumn1;
        TcxMaskEdit *FED;
        THTMLViewer *FDocView;
        TPanel *ColumnPanel;
        THTMLViewer *ColView;
        TPanel *EmptyPanel;
        TPanel *HPanel;
        TPanel *RowPanel;
        THTMLViewer *RowView;
        THTMLViewer *FDocViewInsc;
        TPanel *ViewerClientPanel;
        TPanel *ViewerSizePanel;
        TLabel *CurColRowLab;
        TPanel *CurRowColPanel;
        TPanel *CRPanel;
        TLabel *RowCalcRulesLab;
        TLabel *ColCalcRulesLab;
 TAction *actCheck;
 TcxImage *ProgressPanel;
 TdxBarLargeButton *dxBarLargeButton4;
 TTimer *PrTimer;
 TProgressBar *PrBar;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall actSaveExecute(TObject *Sender);
    void __fastcall actExitExecute(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall ShowTimerTimer(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall actLRDirectionExecute(TObject *Sender);
        void __fastcall actTBDirectionExecute(TObject *Sender);
 void __fastcall TabEditEditing(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          bool &Allow);
 void __fastcall TabEditEditValueChanged(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn);
 void __fastcall TabEditFocusedColumnChanged(TcxCustomTreeList *Sender, TcxTreeListColumn *APrevFocusedColumn,
          TcxTreeListColumn *AFocusedColumn);
 void __fastcall TabEditFocusedNodeChanged(TcxCustomTreeList *Sender, TcxTreeListNode *APrevFocusedNode,
          TcxTreeListNode *AFocusedNode);
 void __fastcall TabEditStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
 void __fastcall TabEditLeftPosChanged(TObject *Sender);
 void __fastcall TabEditTopRecordIndexChanged(TObject *Sender);
 void __fastcall actCheckExecute(TObject *Sender);
 void __fastcall PrTimerTimer(TObject *Sender);


private:	// User declarations
    /*����*/
    enum TTRulesType {                              //��� ������ ������� ��� �������� ��� �������
      rtRow,                                        //handrules ��� calcrules ��� ������ �������
      rtCol                                         //handrules ��� calcrules ��� ������� �������
    };
    TICSDocHTMLCreator *FViewer;

    int FEdX, FEdY, FEdXLast, FEdYLast, FEdXMax, FEdYMax, FEdTab;
    int FEDCurTop, FEDCurLeft;
    TDocHandTreeList * FHandTL;

    /*����*/
    bool FErrInCol;
    int  FErrRow, FErrCol;
    TTagNode*              FSpec;           //������������ ( xml-������ � ������ "specificator" )
    TTagNode*              FDoc;            //�������� ( xml-������ � ������ "docum" )
    TTagNode*              FEditDoc;        //�������� ����� ���������
    bool                   FTabPresent;
    bool                   FModified;       //������� ����������� ���������
    bool                   FCurGridModified;//������� ����������� grid'� ��� �������� �������� ���������
                                            //��� �������� �������� ��������� � ������ �����������
                                            //�����
                                            //������ � ������� ��� ������ �������� �������� ��������� "�������"

    bool                   FLRDirection;
    TDocTableDataMap       FTable;
    TEDAnsiMap             FEDNameMap;
    bool   FRecalcMode;
//    int FTabColCount;
//    int FTabRowCount;
    int FDefColWidth;
    int FDefRowHeight;
    TStringList *FColHeaders;
    TStringList *FRowHeaders;
    bool __fastcall IncY();
    bool __fastcall DecY();
    bool __fastcall IncX();
    bool __fastcall DecX();
    bool __fastcall GetNextCell();

    void __fastcall FocusFirstTabEd(int ATab, int AStartRow = 0, int AStartCol = 0);
    void __fastcall FocusLastTabEd(int ATab);
    void __fastcall FLoadCodes();
    void __fastcall EDToCellPos(int ARow, int ACol);
    bool __fastcall FindLink(TList *ALinkList, const UnicodeString &ALinkSrc);
    void __fastcall TabEditColGetEditingProperties(TcxTreeListColumn *Sender, TcxTreeListNode *ANode, TcxCustomEditProperties *&EditProperties);
    void __fastcall CurRowColHeaderUpdate();

protected:

    TAxeXMLContainer *FXMLContainer;

    inline TTagNode* __fastcall GetDocElementDesc(TTagNode *ADocElement);
    UnicodeString __fastcall ReplaceTRowColNum(UnicodeString APoland, TTRulesType AType, int nRow, int nCol);
    bool __fastcall THandRulesCheck(TTagNode *ndHandRules,TTRulesType AType,int nRow,int nCol);
    void __fastcall ReCalcTCell(TTagNode *ndCalcRules, TTRulesType AType, int nRow, int nCol);
    void __fastcall ReCalcCurrentGridData();
    UnicodeString __fastcall DocDataToStr(const double AData);
    UnicodeString __fastcall DocDataToDisplayText(const double AData);
    double __fastcall DisplayTextToDocData(const UnicodeString &AVal);
    bool __fastcall ApplyCurrentGridUpdates(bool ARecalc = true);
    void __fastcall AssignIVValAV(TTagNode *ADestNode, TTagNode *ASrcNode);
    bool __fastcall ApplyUpdates();


public:		// User declarations
    __fastcall TdsDocHandEditForm(TComponent* Owner, TTagNode* ASpec, TTagNode* ADoc, TAxeXMLContainer *AXMLContainer);
    virtual __fastcall ~TdsDocHandEditForm();

    bool __fastcall CheckByHandRules();

    /*��������*/
    //������� ����������� ��������� � ������� ��������������
    __property bool       Modified = { read = FModified };
};

//---------------------------------------------------------------------------

} //end of namespace DocHandEdit
using namespace Dcdochandedit;

#endif
