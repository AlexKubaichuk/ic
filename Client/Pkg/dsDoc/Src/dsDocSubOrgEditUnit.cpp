//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsDocSubOrgEditUnit.h"
#include "pkgsetting.h"

#ifndef _CONFSETTTING
 #error _CONFSETTTING using are not defined
#endif

#include "msgdef.h"
#include "dsDocClientConstDef.h"
#include "dsDocExtFunc.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TdsDocSubOrgEditForm *dsDocSubOrgEditForm;
//---------------------------------------------------------------------------
__fastcall TdsDocSubOrgEditForm::TdsDocSubOrgEditForm(TComponent* Owner, TkabCustomDataSource *ADataSrc, TdsDocDM *ADM)
        : TForm(Owner)
{
  FDataSrc = ADataSrc;
  FExtLPUCode = -1;
  FDM = ADM;
  FDataSrc->DataSet->Insert();
  FRow = FDataSrc->DataSet->EditedRow;
//  SubordCB->Properties->Items->Clear();

//  SubordCB->Properties->Items->Add(FMT(dsDocDocOrgEditSubordCBItem1));
//  SubordCB->Properties->Items->Add(FMT(dsDocDocOrgEditSubordCBItem2));
//  SubordCB->Properties->Items->Add(FMT(dsDocDocOrgEditSubordCBItem3));
  Caption = FMT(dsDocDocOrgEditInsertCaption);
  FRecId = "";

  OrgCodeLab->Caption = FMT(dsDocDocOrgEditOrgCodeLabCaption);
  NameLab->Caption = FMT(dsDocDocOrgEditNameLabCaption);
  AddrLab->Caption = FMT(dsDocDocOrgEditAddrLabCaption);
  EMailLab->Caption = FMT(dsDocDocOrgEditEMailLabCaption);
  PhoneLab->Caption = FMT(dsDocDocOrgEditPhoneLabCaption);
  FullNameLab->Caption = FMT(dsDocDocOrgEditFullNameLabCaption);
  OkBtn->Caption = FMT(dsDocDocOrgEditOkBtnCaption);
  CancelBtn->Caption = FMT(dsDocDocOrgEditCancelBtnCaption);
}
//---------------------------------------------------------------------------
bool __fastcall TdsDocSubOrgEditForm::CheckInput()
{
   try
    {
      if (!(LPUCB->ItemIndex >= 0))
       {
         _MSG_ERR(FMT(dsDocDocOrgEditChLPUErrMsg),FMT(dsDocCommonInputErrorCaption));
         ActiveControl = LPUCB;
         return false;
       }
      if (!OrgCodeED->Text.Trim().Length())
       {
         _MSG_ERR(FMT(dsDocDocOrgEditChIOrgCodeErrMsg),FMT(dsDocCommonInputErrorCaption));
         ActiveControl = OrgCodeED;
         return false;
       }
      else if (
               (OrgCodeED->Text == "000.0000000.000") ||
                    (OrgCodeED->Text == "812.2741381.000") ||
                    (OrgCodeED->Text == "812.1107704.000") ||
                    (OrgCodeED->Text == "812.7107704.000") ||
                    (OrgCodeED->Text == "812.7177704.000") ||
                    (OrgCodeED->Text == "812.7170425.000") ||
                    (OrgCodeED->Text == "812.7100425.000") ||
                    (OrgCodeED->Text == "812.1100425.000") ||
                    (OrgCodeED->Text == "812.7171319.000")
              )
       {
         _MSG_ERR(FMT(dsDocDocOrgEditChIOrgCodeResErrMsg),FMT(dsDocCommonInputErrorCaption));
         ActiveControl = OrgCodeED;
         return false;
       }
      if (!NameED->Text.Trim().Length())
       {
         _MSG_ERR(FMT(dsDocDocOrgEditChIOrgShortNameErrMsg),FMT(dsDocCommonInputErrorCaption));
         ActiveControl = NameED;
         return false;
       }
      if (!FullNameED->Text.Trim().Length())
       {
         _MSG_ERR(FMT(dsDocDocOrgEditChIOrgFullNameErrMsg),FMT(dsDocCommonInputErrorCaption));
         ActiveControl = FullNameED;
         return false;
       }
    }
   __finally
    {
//      if (FDM->trFreeDoc->Active) FDM->trFreeDoc->Commit();
    }
   return true;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgEditForm::OkBtnClick(TObject *Sender)
{
   if (!CheckInput()) return;

   FRow->Value[SubOrgCODE]     = OrgCodeED->Text.Trim();
   FRow->Value[SubOrgNAME]     = NameED->Text;
   FRow->Value[SubOrgFULLNAME] = FullNameED->Text;
   FRow->Value[SubOrgADDR]     = AddrED->Text;
   FRow->Value[SubOrgSUBORD]   = 0; // �����������
   FRow->Value[SubOrgEMAIL]    = EMailED->Text;
   FRow->Value[SubOrgPHONE]    = PhoneED->Text;
   FRow->Value[SubOrgOWNERFIO] = OwnerFIOED->Text;

   try
    {
      UnicodeString PRC = FDataSrc->DataSet->Post(FRecId, true);
      if (!SameText(PRC,"ok"))
        _MSG_ERR(PRC,"������");
      else
        ModalResult = mrOk;
    }
   catch (EkabCustomDataSetError &E)
    {
      _MSG_ERR(E.Message,"������");
    }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgEditForm::OrgCodeEDPropertiesValidate(
      TObject *Sender, Variant &DisplayValue, TCaption &ErrorText,
      bool &Error)
{
   Error =  false;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgEditForm::AddrEDPropertiesButtonClick(
      TObject *Sender, int AButtonIndex)
{
/*
  if (AButtonIndex == 0)
   {
     FDM->KLADR->DomVisible = true;
     if (FDM->KLADR->Execute(false,AddrED->Text))
      {
        AddrED->Text = FDM->KLADR->AddrCode;
        AddrValLab->Caption = FDM->KLADR->codeToText(FDM->KLADR->AddrCode);
      }
   }
  else
   {
     AddrED->Text = "";
     AddrValLab->Caption = "";
   }
*/
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgEditForm::FormDestroy(TObject *Sender)
{
/*
  if (FDM->UseKLADRAddr)
   FDM->KLADR->DomVisible = false;
*/
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgEditForm::ClearLPUData()
{
  OrgCodeED->Text = "";
  FullNameED->Text = "";
  NameED->Text = "";
  OwnerFIOED->Text = "";
  PhoneED->Text = "";
  EMailED->Text = "";
  AddrED->Text = "";
  AddrValLab->Caption = "";
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgEditForm::SetLPUData(TTagNode *AData)
{
  ClearLPUData();
  if (AData->GetChildByName("Code"))
   OrgCodeED->Text = AData->GetChildByName("Code")->AV["PCDATA"];
  if (AData->GetChildByName("FullName"))
   FullNameED->Text = AData->GetChildByName("FullName")->AV["PCDATA"];
  if (AData->GetChildByName("Name"))
   NameED->Text = AData->GetChildByName("Name")->AV["PCDATA"];
  if (AData->GetChildByName("OwnerFIO"))
   OwnerFIOED->Text = AData->GetChildByName("OwnerFIO")->AV["PCDATA"];
  if (AData->GetChildByName("Phone"))
   PhoneED->Text = AData->GetChildByName("Phone")->AV["PCDATA"];
  if (AData->GetChildByName("EMail"))
   EMailED->Text = AData->GetChildByName("EMail")->AV["PCDATA"];
  if (AData->GetChildByName("Addr"))
   {
     UnicodeString FAddr = AData->GetChildByName("Addr")->AV["PCDATA"];
     AddrED->Text = GetLPartB(FAddr,':');
     AddrValLab->Caption = GetRPartE(FAddr,':');
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgEditForm::GetLPUData(TTagNode *AData)
{
  AData->Name = "LPUData";
  AData->DeleteChild();
  AData->AddChild("Code")->AV["PCDATA"]     = OrgCodeED->Text;
  AData->AddChild("FullName")->AV["PCDATA"] = FullNameED->Text;
  AData->AddChild("Name")->AV["PCDATA"]     = NameED->Text;
  AData->AddChild("OwnerFIO")->AV["PCDATA"] = OwnerFIOED->Text;
  AData->AddChild("Phone")->AV["PCDATA"]    = PhoneED->Text;
  AData->AddChild("EMail")->AV["PCDATA"]    = EMailED->Text;
  AData->AddChild("Addr")->AV["PCDATA"]    = AddrED->Text;
  if (FDM->UseKLADRAddr)
   AData->AddChild("AddrStr")->AV["PCDATA"] = AddrValLab->Caption;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgEditForm::LPUCBPropertiesChange(TObject *Sender)
{
  if (OnGetFillLPUData)
   {
     if (LPUCB->ItemIndex != -1)
      {
        long FLPUCode = (long)LPUCB->Properties->Items->Objects[LPUCB->ItemIndex];
        if (OnGetLPUData)
         {
           TTagNode *FLPUData = new TTagNode(NULL);
           GetLPUData(FLPUData);
           try
            {
              OnGetLPUData(FLPUCode, FLPUData);
              SetLPUData(FLPUData);
            }
           __finally
            {
              delete FLPUData;
            }
         }
      }
     else
      {
        ClearLPUData();
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSubOrgEditForm::FormShow(TObject *Sender)
{
  if (OnGetFillLPUData)
   {
     OnGetFillLPUData(LPUCB->Properties->Items, -1);
     if (FExtLPUCode)
      {
        int idx = LPUCB->Properties->Items->IndexOfObject((TObject*)FExtLPUCode);
        if (idx != -1)
          LPUCB->ItemIndex = idx;
      }
   }
}
//---------------------------------------------------------------------------

