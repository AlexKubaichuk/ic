//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsDocGetCheckDocOrgUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxRadioGroup"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TdsDocGetCheckDocOrgForm *dsDocGetCheckDocOrgForm;
//---------------------------------------------------------------------------
__fastcall TdsDocGetCheckDocOrgForm::TdsDocGetCheckDocOrgForm(TComponent* Owner, TdsDocDM *ADM)
        : TForm(Owner)
{
  FDM = ADM;
  FCreateByType = false;
  FOrgType   = "";
  FOrgCode   = "";

  OrgTypeCB->Properties->Items->Clear();

  OrgTypeCB->Properties->Items->Add("�������");
  OrgTypeCB->Properties->Items->Add("����");
  OrgTypeCB->Properties->Items->Add("�����������");
  OrgTypeCB->ItemIndex = 0;

  FOrgCodeMap.clear();
  OrgCB->Properties->Items->Clear();
  try
   {
     TJSONObject *RetData;
     TJSONObject *tmpPair;
     TJSONString *tmpData;
     UnicodeString FClCode, FClStr1, FClStr2;
     RetData = FDM->kabCDSGetValById("doc.s3.Select CODE as ps1, NAME as ps2, FULLNAME as ps3 from SUBORDORG order by name", 0);
     TJSONPairEnumerator *itPair = RetData->GetEnumerator();
     while (itPair->MoveNext())
      {
        FClCode = ((TJSONString*)itPair->Current->JsonString)->Value();
        FClStr1 = ((TJSONString*)itPair->Current->JsonValue)->Value();
        FClStr2 = GetRPartB(FClStr1,':');
        FClStr1 = GetLPartB(FClStr1,':');


        OrgCB->Properties->Items->Add(FClStr1);
        FOrgCodeMap[OrgCB->Properties->Items->Count-1] = FClCode;
      }
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocGetCheckDocOrgForm::ByTypeRBClick(TObject *Sender)
{
  ApplyBtn->Enabled  = OrgTypeCB->ItemIndex != -1;
  OrgTypeCB->Enabled = FCreateByType;
  OrgCB->Enabled     = !FCreateByType;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocGetCheckDocOrgForm::ByNameRBClick(TObject *Sender)
{
  ApplyBtn->Enabled  = OrgCB->ItemIndex != -1;
  OrgTypeCB->Enabled = FCreateByType;
  OrgCB->Enabled     = !FCreateByType;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocGetCheckDocOrgForm::OrgTypeCBPropertiesChange(
      TObject *Sender)
{
  ApplyBtn->Enabled = OrgTypeCB->ItemIndex != -1;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocGetCheckDocOrgForm::OrgCBPropertiesChange(TObject *Sender)
{
  ApplyBtn->Enabled = OrgCB->ItemIndex != -1;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocGetCheckDocOrgForm::ApplyBtnClick(TObject *Sender)
{
  FCreateByType = ByTypeRB->Checked;
  FOrgType = "";
  FOrgCode = "";
  if (FCreateByType)
   {
     if (OrgTypeCB->ItemIndex == 0)
      {
        FCreateByType = false;
        FOrgCode = FDM->GetThisSpecOwner();
      }
     else if (OrgTypeCB->ItemIndex == 1)
      {
        FOrgType = "3";
      }
     else if (OrgTypeCB->ItemIndex == 2)
      {
        FOrgType = "0";
      }
   }
  else
   {
     if (OrgCB->ItemIndex != -1)
      {
        FOrgCode = FOrgCodeMap[OrgCB->ItemIndex];
      }
   }
}
//---------------------------------------------------------------------------

