﻿// ---------------------------------------------------------------------------
#include <vcl.h>
// #include <SysUtils.hpp>
#pragma hdrstop
#include "dsDocSpecListUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "pkgsetting.h"
#include <ShellAnimations.hpp>
#ifndef _CONFSETTTING
#error _CONFSETTTING using are not defined
#endif
#define SpecPass(a) GetPassport(FSpec)->AV[a]
#define DocPass(a)  GetPassport(FDoc)->AV[a]
#include "dsDocClientConstDef.h"
#include "msgdef.h"
#include "dsDocFilterListUnit.h"
#include "dsDocSpecEditUnit.h"
#include "dsDocSpecPackEditUnit.h"
// #include "DocCreator.h"
#include "dsDocExtFunc.h"
#include "dcDocHandEdit.h"
// #include "dcDocHandEdit.h"
#include "dsDocExportDocUnit.h"
#include "dsDocImportDocUnit.h"
// ---------------------------------------------------------------------------
#define SpecGUI         "0005"
#define SpecNAME        "0006"
#define SpecSPECTEXT    "000E"
#define SpecF_OWNER     "0007"
#define SpecF_PERIOD    "0008"
#define SpecCREATEDATE  "0010"
#define SpecINTEGRATED  "0009"
#define SpecWTEMPLATE   "000F"
#define SpecSPECTYPE    "000A"
#define SpecMODULE_TYPE "0011"
#define DocDOCTEXT     "0014"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxClasses"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxData"
#pragma link "cxDataStorage"
#pragma link "cxEdit"
#pragma link "cxFilter"
#pragma link "cxGraphics"
#pragma link "cxGrid"
#pragma link "cxGridCustomTableView"
#pragma link "cxGridCustomView"
#pragma link "cxGridLevel"
#pragma link "cxGridTableView"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxNavigator"
#pragma link "cxStyles"
#pragma link "dxBar"
#pragma link "dxStatusBar"
#pragma resource "*.dfm"
TdsDocSpecListForm * dsDocSpecListForm;
// ---------------------------------------------------------------------------
__fastcall TdsDocSpecListForm::TdsDocSpecListForm(TComponent * Owner, TdsDocDM * ADM) : TForm(Owner)
 {
  FDM      = ADM;
  FDataSrc = NULL;
  // FPrForm = NULL;
  RootClass = FDM->RegDef->GetTagByUID("0001");
  // if (RootClass) RootClass = RootClass->GetFirstChild();
  FCtrList                  = new TdsRegEDContainer(this, this);
  FCtrList->DefXML          = RootClass;
  FCtrList->StyleController = FDM->StyleController;
  FCtrList->DataProvider    = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DefXML = RootClass;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  FCtrList->DataPath                    = FDM->DataPath;
  // FLastTop = 0;
  // FLastHeight = 0;
  curCD = 0;
  CheckCtrlVisible();
  CreateSrc();
  if (FDM->DocEditable && FDM->FilterEnabled && (FDM->DefDocCreateType == dctBaseDoc))
   FLoadChoice();
  TStringList * FSpecOwnerList = new TStringList;
  try
   {
    FDM->GetOwnerList(FSpecOwnerList);
    FOwnerNameMap.clear();
    for (int i = 0; i < FSpecOwnerList->Count; i++)
     FOwnerNameMap[GetPart1(FSpecOwnerList->Strings[i], '=')] = GetPart2(FSpecOwnerList->Strings[i], '=');
   }
  __finally
   {
    delete FSpecOwnerList;
   }
  FSpecOwner = FDM->GetThisSpecOwner();
  actNewPackDoc->Enabled = false;
  actNewPackDoc->Visible = false;
  if (!FSpecOwner.Length())
   return;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::CreateSrc()
 {
  Application->ProcessMessages();
  try
   {
    try
     {
      Caption                           = RootClass->AV["name"];
      FSaveCaption                      = RootClass->AV["name"];
      FDataSrc                          = new TkabCustomDataSource(RootClass);
      FDataSrc->DataSet->OnGetCount     = FDM->kabCDSGetCount;
      FDataSrc->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
      FDataSrc->DataSet->OnGetValById   = FDM->kabCDSGetValById;
      FDataSrc->DataSet->OnGetValById10 = FDM->kabCDSGetValById10;
      FDataSrc->DataSet->OnInsert       = FDM->kabCDSInsert;
      FDataSrc->DataSet->OnEdit         = FDM->kabCDSEdit;
      FDataSrc->DataSet->OnDelete       = FDM->kabCDSDelete;
      FDataSrc->SetColumns(SpecListView,
        "000A:Тип\n0006:Наименование\n0008:Периодичность\n0007:Автор\n0010:Дата создания\n0005:Код описания\n");
      SpecListView->DataController->CustomDataSource = FDataSrc;
      ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DataSource = FDataSrc;
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsErrorMsgCaptionSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegistryErrorCommDBErrorMsgCaption), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::Reopen()
 {
  CheckCtrlState(false);
  TTime FBegTime = Time();
  UnicodeString _ES_ = "";
  // UnicodeString TimeMsg = "";
  Application->ProcessMessages();
  try
   {
    try
     {
      SpecListView->BeginUpdate();
      try
       {
        FDataSrc->DataSet->GetData(NULL, TdsRegFetch::Auto, 20, FGetDataProgress);
        FBegTime = Time();
        FDataSrc->DataChanged();
       }
      __finally
       {
        SpecListView->EndUpdate();
        MsgLab->Caption = MsgLab->Caption + " отрисовка: " + (Time() - FBegTime).FormatString("ss.zzz");
       }
      if (curCD.Length())
       {
        TLocateOptions opt;
        // if(!quClass()->Locate("CODE",Variant(curCD),opt))
        // quClass()->First();
       }
      // else
      // quClass()->First();
      RootClass->Iterate(CreateListExt, _ES_);
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsErrorMsgCaptionSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegistryErrorCommDBErrorMsgCaption), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
    CheckCtrlState(true);
    Application->ProcessMessages();
    ActiveControl = SpecList;
    // _MSG_DBG(TimeMsg);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::FGetDataProgress(int AMax, int ACur, UnicodeString AMsg)
 {
  if (AMax == -1)
   {
    PrBar->Position = 0;
    MsgLab->Caption = "";
   }
  else if (!(AMax + ACur))
   {
    PrBar->Position = 0;
    MsgLab->Caption = AMsg;
   }
  else
   {
    MsgLab->Caption = AMsg + " [" + IntToStr(ACur) + "/" + IntToStr(AMax) + "]";
    if (PrBar->Max != AMax)
     PrBar->Max = AMax;
    PrBar->Position = ACur;
   }
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actNewExecute(TObject * Sender)
 {
  actNewSpecExecute(actNewSpec);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actNewSpecExecute(TObject * Sender)
 {
  FNewSpec(0);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actNewPackExecute(TObject * Sender)
 {
  FNewSpec(10);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actNewPackDocExecute(TObject * Sender)
 {
  FNewSpec(11);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::FNewSpec(int ASpecType)
 {
  if (FDM->SpecEditable)
   {
    TForm * Dlg = NULL;
    TTagNode * FSpec = NULL;
    if (IsRecSelected())
     curCD = CurRow()->Value[SpecGUI];
    bool IsIntegr = false;
    CheckCtrlState(false);
    try
     {
      FSpec = new TTagNode(NULL);
      if (ASpecType == 10)
       {
        Dlg = new TdsDocSpecPackEditForm(this, "Создание пакета описаний", FSpecOwner,
          /* FDM->quFreeDoc, */ false, FDM);
        ((TdsDocSpecPackEditForm *)Dlg)->PackDef = FSpec;
       }
      else
       Dlg = new TdsDocSpecEditForm(this, FSpec, FDM->XMLList, FSpecOwner, "", false, FDM, (ASpecType == 11));
      if (Dlg)
       {
        if (Dlg->ModalResult == mrCancel)
         return;
        Dlg->ShowModal();
        if (Dlg->ModalResult == mrOk)
         {
          if (ASpecType == 10)
           FSpec->Assign(((TdsDocSpecPackEditForm *)Dlg)->PackDef, true);
          TTagNode * __tmp = FSpec->GetChildByName("doccontent");
          if (__tmp)
           {
            IsIntegr = FDM->CanIntegrated(FSpec);
           }
          else
           {
            _MSG_ERR(FMT(dsDocDocSpecListNoDefContent), FMT(dsDocCommonErrorCreateDocCaption));
            return;
           }
          UnicodeString tmpGUI = GetSpecGUI(FSpec);
          TExistsType RC = CheckExists(FSpec, tmpGUI, false);
          if (RC == etOk)
           {
            FDataSrc->DataSet->Insert();
            // FDM->quSpec->Insert();
            EditedRow()->Value[SpecGUI] = tmpGUI;
            // EditedRow()->Value[SpecSPECTEXT]    = FSpec->AsXML;
            EditedRow()->Value[SpecNAME] = GetSpecName(FSpec);
            EditedRow()->Value[SpecF_OWNER] = FSpecOwner;
            EditedRow()->Value[SpecCREATEDATE] = Date();
            EditedRow()->Value[SpecF_PERIOD] = GetIntSpecPeriod(FSpec);
            EditedRow()->Value[SpecINTEGRATED] = (int)IsIntegr;
            EditedRow()->Value[SpecSPECTYPE] = ASpecType;
            EditedRow()->Value[SpecMODULE_TYPE] = FDM->SubDir;
            try
             {
              UnicodeString PRC = FDataSrc->DataSet->Post(curCD, true);
              if (!SameText(PRC, "ok"))
               _MSG_ERR(PRC, "Ошибка");
              else
               {
                RefreshById(curCD);
                FDM->SetTextData(ctSpec, SpecSPECTEXT, curCD, FSpec->AsXML);
               }
             }
            catch (System::Sysutils::Exception & E)
             {
              _MSG_ERR(E.Message, "Ошибка");
             }
           }
          else
           {
            // enum TExistsType { etOk=0, etExistByName=1, etExistByCode=2};
            // if (RC == etCancel)
            // return;
           }
          /* else if (RC == etReplace)
           {
           TLocateOptions opt;
           if(!FDM->quSpec->Locate("GUI",Variant(tmpGUI),opt))
           {// какая-то лажа
           _MSG_ERR(FMT(dsDocDocSpecListErrorFindSpec),FMT(dsDocCommonErrorCaption));
           return;
           }
           FDataSrc->DataSet->Edit();
           //                    FDM->quSpec->Edit();
           CurRow()->Value["0005"] FDM->quSpecGUI->AsString = GetSpecGUI(FSpec);
           CurRow()->Value["0005"] FDM->quSpecSPECTEXT->AsString = FSpec->AsXML;
           CurRow()->Value["0005"] FDM->quSpecCREATEDATE->AsDateTime = TDateTime::CurrentDate();
           CurRow()->Value["0005"] FDM->quSpecINTEGRATED->AsInteger = (int)IsIntegr;
           } */
          // curCD = EditedRow()->Value[SpecGUI];
         }
       }
     }
    __finally
     {
      if (Dlg)
       delete Dlg;
      if (FSpec)
       delete FSpec;
      CheckCtrlState(true);
      ActiveControl = SpecList;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actEditSpecExecute(TObject * Sender)
 {
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmSpecListEdit"); // # Список описаний документов. Редактирование описания.
#else
  if (FDM->SpecEditable && FDM->IsMainOwnerCode(CurRow()->Value[SpecF_OWNER]))
   {
    TForm * Dlg = NULL;
    TTagNode * FSpec = NULL;
    curCD = CurRow()->Value[SpecGUI];
    bool IsIntegr = false;
    CheckCtrlState(false);
    try
     {
      FSpec           = new TTagNode(NULL);
      FSpec->AsXML    = FDM->GetSpecTxt(CurRow()->Value[SpecGUI]);
      FSpec->Encoding = "utf-8";
      int _ST = CurRow()->Value[SpecSPECTYPE];
      if (_ST == 10)
       {
        Dlg = new TdsDocSpecPackEditForm(this, "Редактирование пакета описаний", FSpecOwner,
          /* FDM->quFreeDoc, */ true, FDM);
        ((TdsDocSpecPackEditForm *)Dlg)->PackDef = FSpec;
       }
      else
       Dlg = new TdsDocSpecEditForm(this, FSpec, FDM->XMLList, FSpecOwner, "", true, FDM, (_ST == 11));
      if (Dlg)
       {
        // _MSG_DBG("ShowModal");
        Dlg->ShowModal();
        if (Dlg->ModalResult == mrOk)
         {
          if (_ST == 10)
           FSpec->Assign(((TdsDocSpecPackEditForm *)Dlg)->PackDef, true);
          TTagNode * __tmp = FSpec->GetChildByName("doccontent");
          if (__tmp)
           {
            IsIntegr = FDM->CanIntegrated(FSpec);
           }
          else
           {
            _MSG_ERR(FMT(dsDocDocSpecListNoDefContent), FMT(dsDocCommonErrorCreateDocCaption));
            return;
           }
          UnicodeString tmpGUI = GetSpecGUI(FSpec);
          TExistsType RC = CheckExists(FSpec, tmpGUI, true);
          if (true /* RC == etOk */)
           {
            FDataSrc->DataSet->Edit();
            // EditedRow()->Value[SpecSPECTEXT]    = FSpec->AsXML;
            EditedRow()->Value[SpecF_PERIOD] = GetIntSpecPeriod(FSpec);
            EditedRow()->Value[SpecNAME] = GetSpecName(FSpec);
            EditedRow()->Value[SpecINTEGRATED] = (int)IsIntegr;
            EditedRow()->Value[SpecMODULE_TYPE] = FDM->SubDir;
            try
             {
              UnicodeString PRC = FDataSrc->DataSet->Post(curCD, true);
              if (!SameText(PRC, "ok"))
               _MSG_ERR(PRC, "Ошибка");
              else
               {
                RefreshById(curCD);
                FDM->SetTextData(ctSpec, SpecSPECTEXT, curCD, FSpec->AsXML);
               }
             }
            catch (System::Sysutils::Exception & E)
             {
              _MSG_ERR(E.Message, "Ошибка");
             }
           }
          else
           {
            // if (RC == etCancel)
            // return;
            // else if (RC == etOk)
           }
          /* else if (RC == etReplace)
           {
           TLocateOptions opt;
           if(!FDM->quSpec->Locate("GUI",Variant(tmpGUI),opt))
           {// какая-то лажа
           _MSG_ERR(FMT(dsDocDocSpecListErrorFindSpec),FMT(dsDocCommonErrorCaption));
           return;
           }
           FDM->quSpec->Edit();
           CurRow()->Value[SpecGUI] = GetSpecGUI(FSpec);
           FDM->quSpecSPECTEXT->AsString = FSpec->AsXML;
           FDM->quSpecCREATEDATE->AsDateTime = TDateTime::CurrentDate();
           FDM->quSpecINTEGRATED->AsInteger = (int)IsIntegr;
           } */
         }
       }
     }
    __finally
     {
      if (Dlg)
       delete Dlg;
      if (FSpec)
       delete FSpec;
      CheckCtrlState(true);
      ActiveControl = SpecList;
     }
   }
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actCopySpecExecute(TObject * Sender)
 {
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmSpecListCopy"); // # Список описаний документов. Создание копии описания.
#else
  if (FDM->SpecEditable)
   {
    try
     {
      UnicodeString FNewGUI = "";
      UnicodeString FPostRC = FDM->CopySpec(CurRow()->Value[SpecGUI], FNewGUI);
      if (SameText(FPostRC, "ok"))
       {
        RefreshById(FNewGUI);
       }
      else
       _MSG_ERR(FPostRC, "Ошибка");
     }
    __finally
     {
      CheckCtrlState(true);
      ActiveControl = SpecList;
     }
   }
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actDeleteSpecExecute(TObject * Sender)
 {
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmSpecListDelete"); // # Список описаний документов. Удаление описания.
#else
  // FDM->quSpec->DisableControls();
  CheckCtrlState(false);
  try
   {
    UnicodeString FMsg = "";
    int FDocCount = FDM->GetDocCountBySpec(CurRow()->Value[SpecF_OWNER], CurRow()->Value[SpecGUI], FMsg);
    if (FDocCount)
     { // Существуют документы сформированные по данному описанию, удаление невозможно.
      _MSG_ERR(FMsg, FMT(dsDocCommonErrorDelCaption));
      ActiveControl = SpecList;
     }
    else
     { // пробуем удалить
      // if(FDM->GetCount("SPECIFIKATORS",FDM->quSpec->Filter) < 1)
      // return;
      try
       {
        if (_MSG_QUE(FMT2(dsDocCommonDelConfMsg, FMT(dsDocDocSpecListDocDefCaption),
          "\"" + CurRow()->Value[SpecNAME] + "\""), FMT(dsDocCommonConfRecDelCaption)) == IDYES)
         {
          /* delCode = CurRow()->Value[SpecGUI];
           FDM->quSpec->Next();
           curCD = CurRow()->Value[SpecGUI];
           TLocateOptions opt;
           FDM->quSpec->Locate("GUI",Variant(delCode),opt);
           if (!FDM->trSpec->Active) FDM->trSpec->StartTransaction(); */
          FDataSrc->DataSet->Delete();
          Reopen();
         }
       }
      catch (...)
       {
        _MSG_ERR(FMT1(dsDocCommonErrorDelMsg, FMT(dsDocDocSpecListDocDefCaption)),
          cFMT(dsDocCommonErrorCaption));
        // FDataSrc->DataSet->Cancel();
       }
     }
   }
  __finally
   {
    // FDM->quSpec->EnableControls();
   }
  // ActiveControl = SpecList;
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::quSpecAfterScroll(TDataSet * DataSet)
 {
  CheckCtrlState(true);
 }
// ---------------------------------------------------------------------------
TExistsType __fastcall TdsDocSpecListForm::CheckExists(TTagNode * ANode, UnicodeString & AGUI, bool AIsEdit)
 {
  return (TExistsType)FDM->CheckSpecExists(GetSpecName(ANode), AGUI, AIsEdit);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actCreateDocExecute(TObject * Sender)
 {
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmSpecListCreateDoc"); // # Список описаний документов. Формирование документа.
#endif
  if (FDM->DocEditable)
   {
    TTagNode * FSpec = NULL;
    TTagNode * FDoc = NULL;
    TReplaceFlags repF;
    repF << rfReplaceAll;
    UnicodeString FSQL;
    CheckCtrlState(false);
    // FDM->quSpec->DisableControls();
    try
     {
      // if (FDM->GetCount("SPECIFIKATORS",FDM->quSpec->Filter) > 0)
      // {
      UnicodeString FltCode = "";
      bool FNeedRefresh = false;
      FSpec        = new TTagNode(NULL);
      FDoc         = new TTagNode(NULL);
      FSpec->AsXML = FDM->GetSpecTxt(CurRow()->Value[SpecGUI]);
      if (FDM->FilterEnabled)
       {
        if (FltCB->ItemIndex != -1)
         FltCode = IntToStr((__int64)FltCB->Items->Objects[FltCB->ItemIndex]);
       }
      int _ST = CurRow()->Value[SpecSPECTYPE];
      if (_ST == 10)
       { // пакет
        FDM->CreatePackage(FSpec, FltCode);
       }
      else
       {
        UnicodeString FDocGUI = FDM->InternalCreateDocument(CheckProgress, FSpec, 1, FltCode, (_ST == 11),
          FDM->GetThisSpecOwner());
        if (!SameText(FDocGUI, "cancel") && FDocGUI.Length())
         {
          MsgLab->Caption = "Формирование предварительного просмотра";
          FDM->PreviewDocument(FDocGUI, CheckProgress);
          FDM->SaveDocument(FDocGUI);
         }
       }
     }
    __finally
     {
      CheckCtrlState(true);
      // FDM->quSpec->EnableControls();
      if (FSpec)
       delete FSpec;
      if (FDoc)
       delete FDoc;
      MsgLab->Caption = "";
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actCreateDocByPeriodExecute(TObject * Sender)
 {
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmSpecListCreateDoc"); // # Список описаний документов. Формирование документа.
#endif
  if (FDM->DocEditable)
   {
    TTagNode * FSpec = NULL;
    TTagNode * FDoc = NULL;
    TReplaceFlags repF;
    repF << rfReplaceAll;
    UnicodeString FSQL;
    CheckCtrlState(false);
    // FDM->quSpec->DisableControls();
    try
     {
      // if (FDM->GetCount("SPECIFIKATORS",FDM->quSpec->Filter) > 0)
      // {
      UnicodeString FltCode = "";
      bool FNeedRefresh = false;
      FSpec        = new TTagNode(NULL);
      FDoc         = new TTagNode(NULL);
      FSpec->AsXML = FDM->GetSpecTxt(CurRow()->Value[SpecGUI]);
      if (FDM->FilterEnabled)
       {
        if (FltCB->ItemIndex != -1)
         FltCode = IntToStr((__int64)FltCB->Items->Objects[FltCB->ItemIndex]);
       }
      int _ST = CurRow()->Value[SpecSPECTYPE];
      if (_ST == 10)
       { // пакет
        FDM->CreatePackage(FSpec, FltCode, false);
       }
      else
       {
        UnicodeString FDocGUI = FDM->InternalCreateDocument(CheckProgress, FSpec, 1, FltCode, (_ST == 11),
          FDM->GetThisSpecOwner());
        if (!SameText(FDocGUI, "cancel") && FDocGUI.Length())
         {
          MsgLab->Caption = "Формирование предварительного просмотра";
          FDM->PreviewDocument(FDocGUI, CheckProgress);
          FDM->SaveDocument(FDocGUI);
         }
       }
      // }
     }
    __finally
     {
      CheckCtrlState(true);
      // FDM->quSpec->EnableControls();
      if (FSpec)
       delete FSpec;
      if (FDoc)
       delete FDoc;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actHandCreateExecute(TObject * Sender)
 {
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmSpecListHandEdit"); // # Список описаний документов. Ручное редактирование документа.
#else
  if (FDM->DocHandEditable)
   {
    TTagNode * FSpec = NULL;
    TTagNode * FDoc = NULL;
    UnicodeString FSQL;
    TReplaceFlags repF;
    repF << rfReplaceAll;
    CheckCtrlState(false);
    try
     {
      // if (FDM->GetCount("SPECIFIKATORS",FDM->quSpec->Filter) > 0)
      // {
      bool FNeedRefresh = false;
      FSpec        = new TTagNode(NULL);
      FDoc         = new TTagNode(NULL);
      FSpec->AsXML = FDM->GetSpecTxt(CurRow()->Value[SpecGUI]);
      bool CanEdit = false;
      int _ST = CurRow()->Value[SpecSPECTYPE];
      UnicodeString FDocGUI = "";
      if (_ST != 10)
       { // не пакет
        FDocGUI = FDM->InternalCreateDocument(CheckProgress, FSpec, 0, "", (_ST == 11), FDM->GetThisSpecOwner());
       }
      if (!SameText(FDocGUI, "cancel") && FDocGUI.Length())
       {
        FDoc->AsXML = FDM->GetDocById(FDocGUI);
        // TdsDocHandEditForm * Dlg = new TdsDocHandEditForm(this, FSpec, FDoc, FDM->XMLList);
        TdsDocHandEditForm * Dlg = new TdsDocHandEditForm(this, FSpec, FDoc, FDM->XMLList);
        try
         {
          try
           {
//            Dlg->actReCalc->Visible = true;
            Dlg->ShowModal();
            if (Dlg->Modified && GetPassport(FSpec)->CmpAV("save_bd", "1"))
             { // сохраняем документ и устанавливаем признак ручной модификации
              // curCD = FDocGUI;
              FDM->SetTextData(ctDoc, DocDOCTEXT, FDocGUI, FDoc->AsXML);
              // RefreshById(curCD);
             }
           }
          catch (Exception & E)
           {
            _MSG_ERR(E.Message, "Ошибка");
           }
         }
        __finally
         {
          delete Dlg;
         }
       }
      // }
     }
    __finally
     {
      CheckCtrlState(true);
      if (FSpec)
       delete FSpec;
      if (FDoc)
       delete FDoc;
     }
   }
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actAddTemplateExecute(TObject * Sender)
 {
  /*
   if (OpenDlg->Execute())
   {
   TFileStream *xFile = NULL;
   long xLength,xRead;
   CheckCtrlState(false);
   try
   {
   try
   {
   xFile = new TFileStream(OpenDlg->FileName, fmOpenRead);
   if (!xFile)
   {
   Application->MessageBox(cFMT1(dsDocDocSpecListErrorOpenFile,OpenDlg->FileName), L"File Error", IDOK);
   }
   if (FDM->trFreeDoc->Active) FDM->trFreeDoc->Commit();
   FDM->trFreeDoc->StartTransaction();
   FDM->quFreeDoc->Close();
   FDM->quFreeDoc->SQL->Clear();
   FDM->quFreeDoc->SQL->Add("Update SPECIFIKATORS Set ");
   FDM->quFreeDoc->SQL->Add(" WTEMPLATE = ?BINDATA");
   FDM->quFreeDoc->SQL->Add((" Where GUI = '"+CurRow()->Value[SpecGUI]+"'").c_str());
   FDM->quFreeDoc->ParamByName("BINDATA")->LoadFromStream(xFile);
   FDM->quFreeDoc->Prepare();
   FDM->quFreeDoc->ExecQuery();
   FDM->trFreeDoc->Commit();
   }
   catch(...)
   {
   Application->MessageBox(cFMT(dsDocDocSpecListErrorFileWork), L"File Error", IDOK);
   }
   }
   __finally
   {
   CheckCtrlState(true);
   if (xFile) delete xFile;
   }
   }
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actClearTemplateExecute(TObject * Sender)
 {
  /*
   if (FDM->trFreeDoc->Active) FDM->trFreeDoc->Commit();
   FDM->trFreeDoc->StartTransaction();
   FDM->quFreeDoc->Close();
   FDM->quFreeDoc->SQL->Clear();
   FDM->quFreeDoc->SQL->Add("Update SPECIFIKATORS Set ");
   FDM->quFreeDoc->SQL->Add(" WTEMPLATE = NULL");
   FDM->quFreeDoc->SQL->Add((" Where GUI = '"+CurRow()->Value[SpecGUI]+"'").c_str());
   FDM->quFreeDoc->Prepare();
   FDM->quFreeDoc->ExecQuery();
   FDM->trFreeDoc->Commit();
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actSaveToXMLExecute(TObject * Sender)
 {
  /*
   SaveDlg->Filter = "xml-файлы|*.xml";
   SaveDlg->DefaultExt = ".xml";
   if (SaveDlg->Execute())
   {
   TTagNode *FSpec = NULL;
   try
   {
   if (FDM->GetCount("SPECIFIKATORS",FDM->quSpec->Filter) > 0)
   {
   FSpec = new TTagNode(NULL);
   FSpec->AsXML = FDM->quSpecSPECTEXT->AsString;
   FSpec->SaveToXMLFile(SaveDlg->FileName,"");
   }
   }
   __finally
   {
   if (FSpec) delete FSpec;
   }
   }
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actLoadFromXMLExecute(TObject * Sender)
 {
  /*
   OpenDlg->Filter = FMT(dsDocDocSpecListOpenDlgMask);
   if (OpenDlg->Execute())
   {
   TTagNode *FSpec = NULL;
   curCD = CurRow()->Value[SpecGUI];
   bool IsIntegr = false;
   try
   {
   FSpec = new TTagNode(NULL);
   FSpec->LoadFromXMLFile(OpenDlg->FileName);
   TTagNode *__tmp = FSpec->GetChildByName("doccontent");
   if (__tmp)
   {
   if (__tmp->Count == 2)
   {
   if ( __tmp->GetFirstChild()->CmpName("inscription")&&
   __tmp->GetFirstChild()->GetNext()->CmpName("table")
   )
   {
   IsIntegr = true;
   }
   }
   }
   else
   {
   _MSG_ERR(FMT(dsDocDocSpecListNoDefContent),FMT(dsDocCommonErrorCreateDocCaption));
   return;
   }
   UnicodeString tmpGUI = GetSpecGUI(FSpec);
   TExistsType RC = CheckExists(FSpec, tmpGUI, false);
   if (RC == etCancel)
   return;
   else if (RC == etOk)
   {
   FDM->quSpec->Insert();
   CurRow()->Value[SpecGUI] = tmpGUI;
   FDM->quSpecSPECTEXT->AsString = FSpec->AsXML;
   FDM->quSpecNAME->AsString = GetSpecName(FSpec);
   FDM->quSpecF_OWNER->AsString = FSpecOwner;
   FDM->quSpecCREATEDATE->AsDateTime = TDateTime::CurrentDate();
   FDM->quSpecF_PERIOD->AsInteger = GetIntSpecPeriod(FSpec);
   FDM->quSpecINTEGRATED->AsInteger = (int)IsIntegr;
   }
   else if (RC == etReplace)
   {
   TLocateOptions opt;
   if(!FDM->quSpec->Locate("GUI",Variant(tmpGUI),opt))
   {// какая-то лажа
   _MSG_ERR(FMT(dsDocDocSpecListErrorFindSpec),FMT(dsDocCommonErrorCaption));
   return;
   }
   FDM->quSpec->Edit();
   CurRow()->Value[SpecGUI] = GetSpecGUI(FSpec);
   FDM->quSpecSPECTEXT->AsString = FSpec->AsXML;
   FDM->quSpecCREATEDATE->AsDateTime = TDateTime::CurrentDate();
   FDM->quSpecINTEGRATED->AsInteger = (int)IsIntegr;
   }
   curCD = CurRow()->Value[SpecGUI];
   FDM->quSpecMODULE_TYPE->AsString = FDM->SubDir;
   FDM->quSpec->Post();
   FDM->trSpec->Commit();
   }
   __finally
   {
   if (FSpec) delete FSpec;
   Reopen();
   ActiveControl = SpecList;
   }
   }
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_ESCAPE)
   {
    Close();
    Key = 0;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actCreateDebugDocExecute(TObject * Sender)
 {
  if (FDM->IMM_DEBUG && FDM->DocEditable)
   {
    TTagNode * FSpec = NULL;
    TTagNode * FDoc = NULL;
    TReplaceFlags repF;
    repF << rfReplaceAll;
    UnicodeString FSQL;
    CheckCtrlState(false);
    // FDM->quSpec->DisableControls();
    try
     {
      // if (FDM->GetCount("SPECIFIKATORS",FDM->quSpec->Filter) > 0)
      // {
      UnicodeString FltCode = "";
      bool FNeedRefresh = false;
      FSpec        = new TTagNode(NULL);
      FDoc         = new TTagNode(NULL);
      FSpec->AsXML = CurRow()->Value[SpecSPECTEXT];
      if (FDM->FilterEnabled)
       {
        if (FltCB->ItemIndex != -1)
         FltCode = IntToStr((__int64)FltCB->Items->Objects[FltCB->ItemIndex]);
       }
      int _ST = CurRow()->Value[SpecSPECTYPE];
      if (!FDM->InternalCreateDocument(CheckProgress, FSpec, 1, FltCode, (_ST == 11), FDM->GetThisSpecOwner()).Length())
       _MSG_ERR("Ошибка формирования документа", "Ошибка");
      // }
     }
    __finally
     {
      CheckCtrlState(true);
      // FDM->quSpec->EnableControls();
      if (FSpec)
       delete FSpec;
      if (FDoc)
       delete FDoc;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actChoiceLibOpenExecute(TObject * Sender)
 {
  if (FDM->FilterEnabled)
   {
    TdsDocFilterListForm * Dlg = NULL;
    CheckCtrlState(false);
    try
     {
      Dlg = new TdsDocFilterListForm(this, "Библиотека фильтров", 0, FDM);
      Dlg->ShowModal();
      FLoadChoice();
     }
    __finally
     {
      CheckCtrlState(true);
      if (Dlg)
       delete Dlg;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::FLoadChoice()
 {
  if (FDM->DocEditable && FDM->FilterEnabled /* && FDM->AppOpt->Vals["rtDocLibEdit"].AsBoolDef(false) */ &&
    (FDM->DefDocCreateType == dctBaseDoc))
   {
    try
     {
      FltCB->Items->Clear();
      FDM->FillClass(FltCB->Items, "",
        "Select CODE as ps1, NAME as ps2 From FILTERS Where CHTYPE = 0 and Upper(MODULE_TYPE)='" +
        FDM->SubDir.UpperCase() + "' Order By NAME");
      // FltCB->Items->Insert(0,"");
      /*
       FltCB->Items->Clear();
       FDM->qtExecDoc(, false);
       FltCB->Items->Add("");
       while (!FDM->quFreeDoc->Eof)
       {
       FltCB->Items->AddObject(FDM->quFreeDoc->Value("NAME")->AsString.c_str(),(TObject*)FDM->quFreeDoc->Value("CODE")->AsInteger);
       FDM->quFreeDoc->Next();
       }
       FDM->qtDocCommit();
       */
     }
    __finally
     {
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::FormDestroy(TObject * Sender)
 {
  SpecListView->DataController->CustomDataSource = NULL;
  if (FDataSrc)
   delete FDataSrc;
  FDataSrc = NULL;
  delete FCtrList->DataProvider;
  delete FCtrList;
  // FDM->quSpec->AfterScroll = NULL;
  // FDM->quSpecF_OWNERSTR->OnGetText = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::quSpecF_OWNERGetText(TField * Sender, UnicodeString & Text, bool DisplayText)
 {
  if (Sender->IsNull)
   Text = "";
  else
   Text = FOwnerNameMap[Sender->AsString];
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::CheckCtrlState(bool AEnable)
 {
  actNew->Enabled        = false;
  actNewPack->Enabled    = false;
  actNewPackDoc->Enabled = false;
  actNewSpec->Enabled    = false;
  actEditSpec->Enabled   = false;
  actDeleteSpec->Enabled = false;
  actCopySpec->Enabled   = false;
  actFindSpec->Enabled   = false;
  actCreateDoc->Enabled  = false;
  // actCreateDocByPeriod->Enabled = false;
  actCreateDebugDoc->Enabled = false;
  actHandCreate->Enabled     = false;
  actChoiceLibOpen->Enabled  = false;
  FltCB->Enabled             = false;
  actExportSpec->Enabled     = false;
  actImportSpec->Enabled     = false;
  if (AEnable)
   {
    if (SpecListView->DataController->CustomDataSource && FDataSrc)
     {
      bool FRCount = FDataSrc->DataSet->RecordCount;
      if (FRCount && IsRecSelected())
       {
        actEditSpec->Enabled = FDM->SpecEditable && FRCount && FDM->IsMainOwnerCode(CurRow()->Value[SpecF_OWNER])
          && actEditSpec->Visible;
        actDeleteSpec->Enabled = FDM->SpecEditable && FRCount && actDeleteSpec->Visible;
        actCreateDoc->Enabled  = FDM->DocEditable && actCreateDoc->Visible;
        // actCreateDocByPeriod->Enabled = actCreateDocByPeriod->Visible;
        actCreateDebugDoc->Enabled = FDM->DocEditable && actCreateDebugDoc->Visible;
        actHandCreate->Enabled     = FDM->DocHandEditable && actHandCreate->Visible && FDM->CanHandEdit
          (CurRow()->Value[SpecGUI]);
        actCopySpec->Enabled = FDM->SpecEditable && FRCount && actCopySpec->Visible;
       }
      // actAddTemplate->Enabled = FRCount && actAddTemplate->Visible;
      // actClearTemplate->Enabled = FRCount && actClearTemplate->Visible;
      // actSaveToXML->Enabled     = FRCount && actSaveToXML->Visible;
      // actLoadFromXML->Enabled   = FRCount && actLoadFromXML->Visible;
      actNew->Enabled = FDM->SpecEditable && actNew->Visible;
      actNewPack->Enabled       = FDM->SpecEditable && actNewPack->Visible;
      actNewPackDoc->Enabled    = FDM->SpecEditable && actNewPackDoc->Visible;
      actNewSpec->Enabled       = FDM->SpecEditable && actNewSpec->Visible;
      actChoiceLibOpen->Enabled = FDM->FilterEnabled && actChoiceLibOpen->Visible;
      FltCB->Enabled            = FDM->FilterEnabled && actChoiceLibOpen->Visible;
      actExportSpec->Enabled    = FRCount && FDM->SpecEditable;
      actImportSpec->Enabled    = FDM->SpecEditable;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::CheckCtrlVisible()
 {
  if (FDM->IMM_DEBUG)
   actCreateDebugDoc->Visible = true;
  ListTBM->Bars->Items[1]->Visible = false;
  if (FDM->DocEditable && FDM->FilterEnabled && (FDM->DefDocCreateType == dctBaseDoc))
   {
    ListTBM->Bars->Items[1]->Visible = true;
    FltCB->Visible                   = ivAlways;
    actChoiceLibOpen->Visible        = true;
   }
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocSpecListForm::CreateListExt(TTagNode * itTag, UnicodeString & UID)
 {
  TdsRegLabItem * tmpItem;
  if (!itTag->CmpName("class,description,actuals"))
   {
    if (itTag->CmpAV("inlist", "e"))
     {
      // tmpItem = FCtrList->AddLabItem(itTag, ExtCompPanel->Width, ExtCompPanel);
      tmpItem->Top = xcTop;
      xcTop += tmpItem->Height;
      // if (itTag->CmpName("extedit"))
      // tmpItem->OnGetExtData = FDM->OnGetExtData;
     }
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::ShowTimerTimer(TObject * Sender)
 {
  ShowTimer->Enabled = false;
  Application->ProcessMessages();
  Reopen();
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::FormShow(TObject * Sender)
 {
  SpecListView->DataController->CustomDataSource = FDataSrc;
  CheckCtrlState(true);
  ShowTimer->Enabled = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::FormClose(TObject * Sender, TCloseAction & Action)
 {
  SpecListView->DataController->CustomDataSource = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::SpecListViewStylesGetContentStyle(TcxCustomGridTableView * Sender,
  TcxCustomGridRecord * ARecord, TcxCustomGridTableItem * AItem, TcxStyle *& AStyle)
 {
  if (ARecord->DisplayTexts[0].UpperCase() == "ПАКЕТ")
   AStyle = PackStyle;
  else if (ARecord->DisplayTexts[0].UpperCase() == "СБОР")
   AStyle = DocStyle;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocSpecListForm::IsRecSelected()
 {
  return SpecListView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::SpecListViewFocusedRecordChanged(TcxCustomGridTableView * Sender,
  TcxCustomGridRecord * APrevFocusedRecord, TcxCustomGridRecord * AFocusedRecord, bool ANewItemRecordFocusingChanged)
 {
  if (IsRecSelected())
   FDataSrc->DataSet->GetDataById(VarToStr(FDataSrc->DataSet->CurRow->Value[SpecGUI]), FGetDataProgress);
  if (AFocusedRecord)
   CheckCtrlState(true);
  // FCtrList->LabDataChange();
 }
// ---------------------------------------------------------------------------
TkabCustomDataSetRow * __fastcall TdsDocSpecListForm::CurRow()
 {
  TkabCustomDataSetRow * RC = NULL;
  try
   {
    if (FDataSrc)
     RC = FDataSrc->DataSet->CurRow;
    else
     throw Exception(FMT(dsDocDocSpecListNoDataSrc));
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TkabCustomDataSetRow * __fastcall TdsDocSpecListForm::EditedRow()
 {
  TkabCustomDataSetRow * RC = NULL;
  try
   {
    if (FDataSrc)
     if (FDataSrc->DataSet->EditedRow)
      RC = FDataSrc->DataSet->EditedRow;
    if (!RC)
     throw Exception(FMT(dsDocDocSpecListNoDataSrc));
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::RefreshById(UnicodeString ACode)
 {
  if (ACode.Trim().Length())
   {
    SpecListView->BeginUpdate();
    FDataSrc->DataSet->GetDataById(ACode, FGetDataProgress);
    FDataSrc->DataChanged();
    StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
    SpecListView->EndUpdate();
    // __int64 FCodeIdx = FDataSrc->DataSet->GetRowIdxByCode(ACode);
    // __int64 FIdx = FDataSrc->DataController->GetRowIndexByRecordIndex(FCodeIdx, true);
    // SpecListView->DataController->FocusedRowIndex = FIdx;
    SpecListView->DataController->FocusedRecordIndex = FDataSrc->DataSet->GetRowIdxByCode(ACode);
   }
  else
   _MSG_DBG("Потеряшка");
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actClearFilterCBExecute(TObject * Sender)
 {
  FltCB->ItemIndex = -1;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocSpecListForm::CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg)
 {
  switch (AType)
   {
   case TkabProgressType::InitCommon:
     {
      PrBar->Max = AVal;
      break;
     }
   case TkabProgressType::StageInc:
     {
      MsgLab->Caption = AMsg;
      break;
     }
   case TkabProgressType::CommInc:
     {
      Caption         = FSaveCaption + "  [" + AMsg + "]";
      PrBar->Position = AVal;
      break;
     }
   case TkabProgressType::CommComplite:
     {
      MsgLab->Caption = "";
      PrBar->Position = 0;
      Caption         = FSaveCaption;
      break;
     }
   }
  return true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actExportSpecExecute(TObject * Sender)
 {
  TdsDocExportDocForm * Dlg = new TdsDocExportDocForm(Application->Owner, FDM, true, false);
  try
   {
    Dlg->ShowModal();
   }
  __finally
   {
    delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actImportSpecExecute(TObject * Sender)
 {
  TdsDocImportDocForm * Dlg = new TdsDocImportDocForm(Application->Owner, FDM, true, false);
  try
   {
    Dlg->ShowModal();
   }
  __finally
   {
    delete Dlg;
    Reopen();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actRefreshExecute(TObject * Sender)
 {
  Reopen();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocSpecListForm::actViewDocExecute(TObject * Sender)
 {
  if (FDM->DocEditable)
   {
    TTagNode * FSpec = NULL;
    TTagNode * FDoc = NULL;
    TReplaceFlags repF;
    repF << rfReplaceAll;
    UnicodeString FSQL;
    CheckCtrlState(false);
    try
     {
      UnicodeString FltCode = "";
      bool FNeedRefresh = false;
      FSpec        = new TTagNode(NULL);
      FDoc         = new TTagNode(NULL);
      FSpec->AsXML = FDM->GetSpecTxt(CurRow()->Value[SpecGUI]);
      if (FDM->FilterEnabled)
       {
        if (FltCB->ItemIndex != -1)
         FltCode = IntToStr((__int64)FltCB->Items->Objects[FltCB->ItemIndex]);
       }
      int _ST = CurRow()->Value[SpecSPECTYPE];
      if (_ST == 10)
       { // пакет
        FDM->CreatePackage(FSpec, FltCode);
       }
      else
       {
        UnicodeString FDocGUI = FDM->InternalCreateDocument(CheckProgress, FSpec, 1, FltCode, (_ST == 11),
          FDM->GetThisSpecOwner(), 0, true, true);
        if (!SameText(FDocGUI, "cancel") && FDocGUI.Length())
         {
          MsgLab->Caption = "Формирование предварительного просмотра";
          FDM->PreviewDocument(FDocGUI, CheckProgress);
         }
       }
     }
    __finally
     {
      CheckCtrlState(true);
      if (FSpec)
       delete FSpec;
      if (FDoc)
       delete FDoc;
      MsgLab->Caption = "";
     }
   }
 }
// ---------------------------------------------------------------------------
