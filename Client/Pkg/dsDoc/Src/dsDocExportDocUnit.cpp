//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsDocExportDocUnit.h"
#include "pkgsetting.h"
#include "msgdef.h"

#ifndef _CONFSETTTING
#error _CONFSETTTING using are not defined
#endif
#include "dsDocClientConstDef.h"
#include "ExtUtils.h"
#include "dsDocExtFunc.h"
#include "dsDocDocPeriodUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxCheckBox"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxPC"
#pragma link "cxProgressBar"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxStatusBar"
#pragma resource "*.dfm"
TdsDocExportDocForm * dsDocExportDocForm;
//---------------------------------------------------------------------------
const UnicodeString DOSConvertErrMsg[] =
 {
  "������� �� ������.", //"������� \"%s\" �� ������.",
  "���������� ������� �������� windows-������.",
  "���������� ������� ������������ ��������� windows-������.",
  "���������� ������� ������������ windows-������.",
  "����������� �������� ������ ��������� windows-������, ���������� ������� � �������.",
  "�� ������ �������������� �������� windows-������.",
  "�� ������ ������������ ��������������� ��������� windows-������.",
  "�� ������ ������������ windows-������ ��������������� ��������� DOS-������.",
  "��� ���������� ������������� windows-������ �� ��������� � ��� ������������� ���������� ��������� windows-������.",
  "������� ������������ ������������� ���������.", //"������� ������������ ������������� ���������: \"%s\".",
  "�������� DOS-������ �� ������.",                //"�������� DOS-������ � ����� %d �� ������.",
  "�������� DOS-������ ������� �����.",
  "�������� DOS-������ ������� ���.",
  "����� ��������� DOS-������ ������ � ������������ �������.", //"%s - ����� ��������� DOS-������ ������ � ������������ �������.",
  "� ������������� windows-������ ��������� �������.",
  "� ������������� windows-������ ��������� �������.",
  "� ������������� windows-������ ������ ������� �������.",
  "� ������������� windows-������ ��������� ������� �������.",
  "� ��������� windows-������ ��������� �������.",
  "� ��������� windows-������ ��������� �������.",
  "����� �� ����������.", //"����� \"%s\" �� ����������.",
  " ",
  "�������� �� ����� ���� �������� � DOS-�������.", //"�������� \"%s\" \n�� ����� ���� �������� � DOS-�������.",
  "�������� �� ����� ���� �������� � DOS-�������.\n������ �� ������������� ���������� � �������� ���������."//"�������� \"%s\" \n�� ����� ���� �������� � DOS-�������.\n������ �� ������������� ���������� � �������� ���������.\n�������� ����������� �� \"%s\", ��������� ������ \"%s\"."
 };
//---------------------------------------------------------------------------
__fastcall TdsDocExportDocForm::TdsDocExportDocForm(TComponent * Owner, TdsDocDM * ADM, bool ASpec, bool ADoc)
    : TForm(Owner)
 {
  FDM          = ADM;
  Caption      = FMT(dsDocDocExportDocCaption);
  FSpecDefNode = FDM->RegDef->GetTagByUID("0001");
  FDocDefNode  = FDM->RegDef->GetTagByUID("0002");

  FDocDS  = NULL;
  FSpecDS = NULL;

  FExpTagName           = "docdata";
  FPeriodWhere          = "";
  FOwnerWhere           = "";
  FDocPeriodWhere       = "";
  FDocDateWhere         = "";
  FDocOwnerWhere        = "";
  FDocSpecOwnerWhere    = "";
  ExportPC->HideTabs    = true;
  ExportPC->OnChange    = NULL;
  ExpSpecTS->TabVisible = ASpec;
  ExpDocTS->TabVisible  = ADoc;
  if (ExpSpecTS->TabVisible)
   {
    ExportPC->ActivePage = ExpSpecTS;
    try
     {
      FSpecDS = new TkabCustomDataSource(FSpecDefNode);

      FSpecDS->DataSet->OnGetCount     = FDM->kabCDSGetCount;
      FSpecDS->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
      FSpecDS->DataSet->OnGetValById   = FDM->kabCDSGetValById;
      FSpecDS->DataSet->OnGetValById10 = FDM->kabCDSGetValById10;

      FSpecDS->DataSet->OnInsert = FDM->kabCDSInsert;
      FSpecDS->DataSet->OnEdit   = FDM->kabCDSEdit;
      FSpecDS->DataSet->OnDelete = FDM->kabCDSDelete;
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsErrorMsgCaptionSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegistryErrorCommDBErrorMsgCaption), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }

  if (ExpDocTS->TabVisible)
   {
    ExportPC->ActivePage = ExpDocTS;
    try
     {
      FDocDS = new TkabCustomDataSource(FDocDefNode);

      FDocDS->DataSet->OnGetCount     = FDM->kabCDSGetCount;
      FDocDS->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
      FDocDS->DataSet->OnGetValById   = FDM->kabCDSGetValById;
      FDocDS->DataSet->OnGetValById10 = FDM->kabCDSGetValById10;

      FDocDS->DataSet->OnInsert = FDM->kabCDSInsert;
      FDocDS->DataSet->OnEdit   = FDM->kabCDSEdit;
      FDocDS->DataSet->OnDelete = FDM->kabCDSDelete;
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsErrorMsgCaptionSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegistryErrorCommDBErrorMsgCaption), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }

  FInProgress = false;
  FLoadSpec();

  actSaveDOS->Visible = false;
  UnicodeString CondDll = icsPrePath(ExtractFilePath(ParamStr(0)) + "\\icdosconv.dll");
  ConvDoc = NULL;

  if (FileExists(CondDll))
   {
    try
     {
      ConvDoc = LoadLibrary(CondDll.c_str());
      if (ConvDoc)
       {
        FOpenProc           = (TOpenConvertProc)GetProcAddress(ConvDoc, "OpenConvert");
        FCloseProc          = (TCloseConvertProc)GetProcAddress(ConvDoc, "CloseConvert");
        FConvertProc        = (TConvertDocProc)GetProcAddress(ConvDoc, "ConvertDoc");
        actSaveDOS->Visible = true;
       }
     }
    catch (...)
     {
     }
   }
  SetEnabled(true);
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmDocExport"); //# ������� ����������.
#endif
 }
//---------------------------------------------------------------------------
TDocEl * __fastcall TdsDocExportDocForm::SpecEl(int Idx)
 {
  return ((TDocEl *)SpecChList->Root->Items[Idx]->Data);
 }
//---------------------------------------------------------------------------
TDocEl * __fastcall TdsDocExportDocForm::DocEl(int Idx)
 {
  return ((TDocEl *)DocChList->Root->Items[Idx]->Data);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::ExportXML(TcxTreeListNode * AItems, TTagNode * AExpTag, bool ACanSpec)
 {
  TTagNode * tmpSpec;
  TDocEl * expDef;
  TcxTreeListNode * FNode;
  for (int i = 0; i < AItems->Count; i++)
   {
    FNode = AItems->Items[i];
    if (FNode->Values[0])
     {
      TReplaceFlags fRepl;
      fRepl << rfReplaceAll << rfIgnoreCase;
      expDef = (TDocEl *)FNode->Data;
      if (ACanSpec)
       {
        tmpSpec                 = AExpTag->AddChild("sdoc");
        tmpSpec->AsXML          = StringReplace(FDM->GetTextData(ctSpec, "000E", expDef->Code), "uid", "repl_uid", fRepl);
        tmpSpec->AV["spectype"] = expDef->SpecType;
       }
      else
       {
        //_MSG_DBG(expDef->Code);
        AExpTag->AddChild("sdoc")->AsXML = StringReplace(FDM->GetDocById(expDef->Code), "uid", "repl_uid", fRepl);
       }
      AExpTag->Encoding = "utf-8";
     }
    PrBar->Position++ ;
    Application->ProcessMessages();
    if (!InProgress)
     throw EAbort("");
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::FProgressInit(int AMax, UnicodeString AMsg)
 {
  if (!InProgress)
   throw EAbort("");
  PrBar->Properties->Max = AMax;
  PrBar->Position        = 0;
  PrBar->Update();
  //StatusBar->Panels->Items[4]->Text = AMsg;
  Application->ProcessMessages();
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::FProgressChange(int APercent, UnicodeString AMsg)
 {
  if (!InProgress)
   throw EAbort("");
  if (APercent == -1)
   PrBar->Position++ ;
  else if (APercent == -2)
   StatusBar->Panels->Items[4]->Text = AMsg;
  else
   PrBar->Position = APercent;
  PrBar->Update();
  Application->ProcessMessages();
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::FProgressComplite(UnicodeString AMsg)
 {
  if (!InProgress)
   throw EAbort("");
  PrBar->Position                   = 0;
  StatusBar->Panels->Items[4]->Text = AMsg;
  Application->ProcessMessages();
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::SetEnabled(bool AEnabled)
 {
  ExportPC->Enabled   = AEnabled;
  SpecChList->Enabled = AEnabled;
  DocChList->Enabled  = AEnabled;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDocExportDocForm::CanProgress()
 {
  bool RC = false;
  if (InProgress)
   {
    if (MessageBox(Handle, cFMT(dsDocDocExportDocBreakExportConf), cFMT(dsDocCommonMsgCaption), MB_ICONQUESTION | MB_YESNO) == ID_YES)
     {
      InProgress = false;
      RC         = true;
     }
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::WndProc(Messages::TMessage & Message)
 {
  switch (Message.Msg)
   {
   case WM_CLOSE:
    if (CanProgress())
     {
      Message.Result = 0;
      return;
     }
    else
     inherited::WndProc(Message);
    break;
   }
  inherited::WndProc(Message);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::FLoadSpec()
 {
  FSpecGUIMap.clear();
  PackCB->Items->Clear();
  PackCB->Items->Add("");
  try
   {
    TJSONObject * RetData;
    TJSONObject * tmpPair;
    TJSONString * tmpData;
    UnicodeString FClCode, FClStr;
    RetData = FDM->kabCDSGetValById("doc.s2.Select GUI as ps1, NAME as ps2 From SPECIFIKATORS Where SPECTYPE = 10 Order By NAME", 0);
    TJSONPairEnumerator * itPair = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();

      PackCB->Items->Add(FClStr);
      FSpecGUIMap[PackCB->Items->Count - 1] = FClCode;
     }
   }
  __finally
   {
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::FormDestroy(TObject * Sender)
 {
  if (FSpecDS)
   {
    for (int i = 0; i < SpecChList->Root->Count; i++)
     delete(TDocEl *)SpecChList->Root->Items[i]->Data;
    delete FSpecDS;
   }
  if (FDocDS)
   {
    for (int i = 0; i < DocChList->Root->Count; i++)
     delete(TDocEl *)DocChList->Root->Items[i]->Data;
    delete FDocDS;
   }
  if (ConvDoc)
   {
    FreeLibrary(ConvDoc);
    ConvDoc = NULL;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::actSaveExecute(TObject * Sender)
 {
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmDocExportExecute"); //# ������� ����������. ��������.
#else
  if (InProgress)
   {
    CanProgress();
    return;
   }
  TTagNode * EITag = NULL;
  TReplaceFlags rFlag;
  rFlag << rfReplaceAll;
  try
   {
    SaveDlg->FileName = "DocExport_" + TDateTime::CurrentDateTime().FormatString("yyyymmdd_hhnnss");
    SaveDlg->Filter   = "����� ������|*.zxml";

    if (SaveDlg->Execute())
     {
      InProgress      = true;
      EITag           = new TTagNode(NULL);
      EITag->Name     = FExpTagName + "-ei";
      EITag->Encoding = "utf-8";
      int isCheck = 0;
      if (SpecChList->IsEditing)
       SpecChList->Root->EndEdit(false);
      for (int i = 0; i < SpecChList->Root->Count; i++)
       isCheck += (int)SpecChList->Root->Items[i]->Values[0];

      if (isCheck)
       {
        StatusBar->Panels->Items[4]->Text = FMT(dsDocDocExportDocDocDefCaption);
        PrBar->Properties->Max            = isCheck;
        PrBar->Position                   = 0;
        PrBar->Update();
        ExportXML(SpecChList->Root, EITag->AddChild("specs"), true);
       }
      isCheck = 0;
      if (DocChList->IsEditing)
       DocChList->Root->EndEdit(false);
      for (int i = 0; i < DocChList->Root->Count; i++)
       isCheck += (int)DocChList->Root->Items[i]->Values[0];
      if (isCheck)
       {
        StatusBar->Panels->Items[4]->Text = FMT(dsDocDocExportDocDocCaption);
        PrBar->Properties->Max            = isCheck;
        PrBar->Position                   = 0;
        PrBar->Update();
        ExportXML(DocChList->Root, EITag->AddChild("docs"), false);
       }
      StatusBar->Panels->Items[4]->Text = FMT(dsDocDocExportDocSaveCaption);
      PrBar->Position       = 0;
      EITag->OnProgressInit = FProgressInit;
      EITag->OnProgress     = FProgressChange;
      EITag->SaveToZIPXMLFile(icsPrePath(SaveDlg->FileName) + ".zxml", "");
      EITag->OnProgressInit = NULL;
      EITag->OnProgress     = NULL;
      PrBar->Position       = 0;
     }
   }
  __finally
   {
    InProgress                        = false;
    StatusBar->Panels->Items[1]->Text = "";
    StatusBar->Panels->Items[4]->Text = "";
    if (EITag)
     delete EITag;
   }
#endif
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::actSaveDOSExecute(TObject * Sender)
 {
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmDocExportExecute"); //# ������� ����������. ��������.
#else
  if (InProgress)
   {
    CanProgress();
    return;
   }
  //TReplaceFlags rFlag;
  //rFlag << rfReplaceAll;
  try
   {
    SaveDlg->FileName = "mail.let";
    SaveDlg->Filter   = "����� ������ DOS|mail.let";

    if (SaveDlg->Execute())
     {
      InProgress = true;
      int isCheck = 0;

      if (SpecChList->IsEditing)
       SpecChList->Root->EndEdit(false);
      if (DocChList->IsEditing)
       DocChList->Root->EndEdit(false);

      isCheck = 0;
      for (int i = 0; i < DocChList->Root->Count; i++)
       isCheck += (int)DocChList->Root->Items[i]->Values[0];
      if (isCheck)
       {
        StatusBar->Panels->Items[4]->Text = FMT(dsDocDocExportDocDocCaption);
        PrBar->Properties->Max            = isCheck;
        PrBar->Position                   = 0;
        PrBar->Update();
        if (FOpenProc(AnsiString(FDM->GetThisSpecOwner()).c_str()))
         {
          TTagNode * spec = new TTagNode;
          TTagNode * doc = new TTagNode;
          int ConvRC = 0;
          try
           {
            TDocEl * expDef;
            TcxTreeListNode * FNode;
            for (int i = 0; i < DocChList->Root->Count; i++)
             {
              FNode = DocChList->Root->Items[i];
              if (FNode->Values[0] && !ConvRC)
               {
                TReplaceFlags fRepl;
                fRepl << rfReplaceAll << rfIgnoreCase;
                expDef         = (TDocEl *)FNode->Data;
                spec->AsXML    = FDM->GetTextData(ctSpec, "000E", expDef->SpecGUI);
                spec->Encoding = "windows-1251";
                doc->AsXML     = FDM->GetDocById(expDef->Code);
                doc->Encoding  = "windows-1251";
                ConvRC |= FConvertProc(AnsiString(doc->AsXML).c_str(), AnsiString(spec->AsXML).c_str());
                if (ConvRC)
                 _MSG_ERR(DOSConvertErrMsg[ConvRC] + "\n" + FNode->Texts[1], "������");
               }
              PrBar->Position++ ;
              Application->ProcessMessages();
              if (!InProgress)
               throw EAbort("");
             }
           }
          __finally
           {
            delete spec;
            delete doc;
            if (!ConvRC)
             FCloseProc(AnsiString(SaveDlg->FileName).c_str());
           }
         }
        else
         _MSG_ERR(
            "������ ���� ����� ����������� �� �������������\n������� ���������� ��� �������� ����������\n� ������� DOS.\n\n��������� ������ ���� �����������:\n\"XXX.XXXXXXX.XXX\".\n\n��� ��������� ���� ����������� ���������\n� ������: \"���������\"->\"��������� ���\".",
            "������");
       }
      StatusBar->Panels->Items[4]->Text = FMT(dsDocDocExportDocSaveCaption);
      PrBar->Position = 0;
     }
   }
  __finally
   {
    InProgress                        = false;
    StatusBar->Panels->Items[1]->Text = "";
    StatusBar->Panels->Items[4]->Text = "";
   }
#endif
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::PackCBChange(TObject * Sender)
 {
  actCheckByPack->Enabled = (PackCB->ItemIndex > 0);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::FCheckDocItem(UnicodeString  ASpecName, UnicodeString  ASpecGUI, int APerType, TDate ADateFr, TDate ADateTo)
 {
  TDocEl * FDocEl;
  bool Exist = false;
  for (int i = 0; (i < DocChList->Root->Count) && !Exist; i++)
   {
    FDocEl = DocEl(i);
    if ((FDocEl->SpecGUI == ASpecGUI.UpperCase()) &&
        (FDocEl->Period == APerType) &&
        (FDocEl->FromDate == ADateFr) &&
        (FDocEl->ToDate == ADateTo)
        )
     {
      Exist                                = true;
      DocChList->Root->Items[i]->Values[0] = true;
     }
   }
  if (!Exist)
   {
    FMissingSpecDoc->Add("<tr><td>" + ASpecName + "</td><td>" + ASpecGUI + "</td><td style='border-right: none;'>" + GetDocPeriodValStr(IntToStr(APerType), ADateFr.FormatString("dd.mm.yyyy"),
        ADateTo.FormatString("dd.mm.yyyy")) + "</td></tr>");
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::FCheckSpecItem(UnicodeString  ASpecName, UnicodeString  ASpecGUI, int APerType, TDate ADateFr, TDate ADateTo)
 {
  TDocEl * FDocEl;
  bool Exist = false;
  for (int i = 0; (i < SpecChList->Root->Count) && !Exist; i++)
   {
    FDocEl = SpecEl(i);
    if ((FDocEl->SpecGUI == ASpecGUI.UpperCase()))
     {
      Exist                                 = true;
      SpecChList->Root->Items[i]->Values[0] = true;
     }
   }
  if (!Exist)
   {
    FMissingSpecDoc->Add("<tr><td>" + ASpecName + "</td><td style='border-right: none;'>" + ASpecGUI + "</td></tr>");
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::actCheckByPackExecute(TObject * Sender)
 {
  if (PackCB->ItemIndex != -1)
   {
    FMissingSpecDoc = new TStringList;
    TTagNode * FPackDef = new TTagNode(NULL);
    int FSpecType;
    try
     {
      FMissingSpecDoc->Clear();
      if (ExportPC->ActivePage == ExpDocTS)
       {
        if (FDM->GetSpec(FSpecGUIMap[PackCB->ItemIndex], FPackDef, FSpecType))
         {
          ClearDocCheck();
          FDM->CheckPackage(FCheckDocItem, FPackDef);
         }
        //DocTV->DataController->CustomDataSource->DataChanged();
        if (FMissingSpecDoc->Count)
         {
          FMissingSpecDoc->Insert(0, "<html>" + FDM->StyleDef +
              "<body><h2>����������� ��������� ���������</h2><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><th>������������</th><th>���</th><th style='border-right: none;'>������</th></tr>");
          FMissingSpecDoc->Add("</table></body></html>");
          FDM->HTMLPreview(FMissingSpecDoc->Text);
         }
       }
      else
       {
        if (FDM->GetSpec(FSpecGUIMap[PackCB->ItemIndex], FPackDef, FSpecType))
         {
          ClearSpecCheck();
          FDM->CheckPackage(FCheckSpecItem, FPackDef);
         }
        //SpecTV->DataController->CustomDataSource->DataChanged();
        if (FMissingSpecDoc->Count)
         {
          FMissingSpecDoc->Insert(0, "<html>" + FDM->StyleDef +
              "<body><h2>����������� ��������� ��������</h2><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr><th>������������</th><th style='border-right: none;'>���</th></tr>");
          FMissingSpecDoc->Add("</table></body></html>");
          FDM->HTMLPreview(FMissingSpecDoc->Text);
         }
       }
     }
    __finally
     {
      delete FMissingSpecDoc;
      FMissingSpecDoc = NULL;
      delete FPackDef;
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::SpecListColChBPropertiesChange(
    TObject * Sender)
 {
  bool Export = false;
  for (int i = 0; (i < SpecChList->Root->Count) && !Export; i++)
   {
    Export = SpecChList->Root->Items[i]->Values[0];
   }
  if (!Export)
   {
    for (int i = 0; (i < DocChList->Root->Count) && !Export; i++)
     {
      Export = DocChList->Root->Items[i]->Values[0];
     }
   }
  actSave->Enabled = Export;
  if (actSaveDOS->Visible)
   actSaveDOS->Enabled = Export;
  else
   actSaveDOS->Enabled = false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::actCloseExecute(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::ShowTimerTimer(TObject * Sender)
 {
  ShowTimer->Enabled = false;
  Application->ProcessMessages();

  UnicodeString _ES_ = "";
  Application->ProcessMessages();
  //DocTV->BeginUpdate();
  //SpecTV->BeginUpdate();
  DocChList->BeginUpdate();
  SpecChList->BeginUpdate();
  try
   {
    InProgress = true;
    try
     {
      if (FDocDS)
       {
        FDocDS->DataSet->GetData(NULL, TdsRegFetch::All, 20, FGetDataProgress);
        FDocDS->DataChanged();
       }

      if (FSpecDS)
       {
        FSpecDS->DataSet->GetData(NULL, TdsRegFetch::All, 20, FGetDataProgress);
        FSpecDS->DataChanged();
       }
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsErrorMsgCaptionSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegistryErrorCommDBErrorMsgCaption), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    //������ ������ ��������������
    TDocEl * tmpEl;
    SpecChList->Clear();
    TcxTreeListNode * FRecNode;
    if (ExpSpecTS->TabVisible)
     {
      for (int i = 0; i < FSpecDS->DataSet->RecordCount; i++)
       {
        FRecNode          = SpecChList->Root->AddChild();
        tmpEl             = new TDocEl;
        tmpEl->GUI        = "";
        tmpEl->SpecGUI    = VarToStr(FSpecDS->DataSet->Row[i]->Value["0005"]).UpperCase();
        tmpEl->Owner      = FSpecDS->DataSet->Row[i]->Value["0007"];
        tmpEl->SOwner     = FSpecDS->DataSet->Row[i]->Value["0007"];
        tmpEl->Period     = (FSpecDS->DataSet->Row[i]->Value["0008"] ? "1" : "0");
        tmpEl->Code       = VarToStr(FSpecDS->DataSet->Row[i]->Value["code"]);
        tmpEl->CreateDate = VarToDateTime(FSpecDS->DataSet->Row[i]->Value["0010"]);
        tmpEl->SpecType   = FSpecDS->DataSet->Row[i]->Value["000A"];

        FRecNode->Data      = tmpEl;
        FRecNode->Values[0] = false;
        FRecNode->Texts[1]  = VarToStr(FSpecDS->DataSet->Row[i]->Value["0006"]);
        FRecNode->Texts[2]  = VarToStr(FSpecDS->DataSet->Row[i]->StrValue["0007"]);
        FRecNode->Texts[3]  = VarToStr(FSpecDS->DataSet->Row[i]->StrValue["0008"]);

        Application->ProcessMessages();
       }
     }
    DocChList->Clear();
    //������ ������ ����������
    if (ExpDocTS->TabVisible)
     {
      for (int i = 0; i < FDocDS->DataSet->RecordCount; i++)
       {
        FRecNode          = DocChList->Root->AddChild();
        tmpEl             = new TDocEl;
        tmpEl->GUI        = VarToStr(FDocDS->DataSet->Row[i]->Value["000B"]).UpperCase();
        tmpEl->SpecGUI    = VarToStr(FDocDS->DataSet->Row[i]->Value["0012"]).UpperCase();
        tmpEl->Owner      = FDocDS->DataSet->Row[i]->Value["0015"];
        tmpEl->SOwner     = FDocDS->DataSet->Row[i]->Value["0016"];
        tmpEl->Period     = FDocDS->DataSet->Row[i]->Value["0017"];
        tmpEl->FromDate   = FDocDS->DataSet->Row[i]->Value["0019"];
        tmpEl->ToDate     = FDocDS->DataSet->Row[i]->Value["001A"];
        tmpEl->Code       = VarToStr(FDocDS->DataSet->Row[i]->Value["code"]);
        tmpEl->CreateDate = VarToDateTime(FDocDS->DataSet->Row[i]->Value["0018"]);

        FRecNode->Data = tmpEl;

        FRecNode->Values[0] = false;
        FRecNode->Texts[1]  = VarToStr(FDocDS->DataSet->Row[i]->Value["0013"]);
        FRecNode->Texts[2]  = VarToStr(FDocDS->DataSet->Row[i]->StrValue["0017"]);
        FRecNode->Texts[3]  = VarToStr(FDocDS->DataSet->Row[i]->StrValue["0015"]);

        Application->ProcessMessages();
       }
     }
   }
  __finally
   {
    InProgress = false;

    SpecChList->EndUpdate();
    DocChList->EndUpdate();

    //DocTV->EndUpdate();
    //SpecTV->EndUpdate();
   }
  Application->ProcessMessages();
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::FormShow(TObject * Sender)
 {
  ShowTimer->Enabled = true;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::FGetDataProgress(int AMax, int ACur, UnicodeString  AMsg)
 {
  if (AMax == -1)
   {
    PrBar->Position                   = 0;
    StatusBar->Panels->Items[4]->Text = "";
   }
  else if (!(AMax + ACur))
   {
    PrBar->Position                   = 0;
    StatusBar->Panels->Items[4]->Text = AMsg;
   }
  else
   {
    StatusBar->Panels->Items[4]->Text = AMsg + " [" + IntToStr(ACur) + "/" + IntToStr(AMax) + "]";
    if (PrBar->Properties->Max != AMax)
     PrBar->Properties->Max = AMax;
    PrBar->Position = ACur;
   }
  Application->ProcessMessages();
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::SetInProgress(bool AVal)
 {
  //SaveBtn->Caption = (AVal)? "��������":"��������";
  FInProgress    = AVal;
  PrBar->Visible = AVal;
  if (AVal)
   PrBar->Position = 0;
  else
   StatusBar->Panels->Items[1]->Text = "";
  SetEnabled(!AVal);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::ClearCheck()
 {
  ClearDocCheck();
  ClearSpecCheck();
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::ClearDocCheck()
 {
  for (int i = 0; i < DocChList->Root->Count; i++)
   DocChList->Root->Items[i]->Values[0] = false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::ClearSpecCheck()
 {
  for (int i = 0; i < SpecChList->Root->Count; i++)
   SpecChList->Root->Items[i]->Values[0] = false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::dxBarButton1Click(TObject * Sender)
 {
  ClearCheck();
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::SpecChListStylesGetContentStyle(TcxCustomTreeList * Sender,
    TcxTreeListColumn * AColumn, TcxTreeListNode * ANode, TcxStyle *& AStyle)
 {
  if (AColumn && ANode)
   {
    if (ANode->Values[0])
     AStyle = selStyle;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::SpecChListDblClick(TObject * Sender)
 {
  if (SpecChList->Selections[0]->Values[0])
   SpecChList->Selections[0]->Values[0] = false;
  else
   SpecChList->Selections[0]->Values[0] = true;
  SpecChPropertiesEditValueChanged(NULL);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::DocChListDblClick(TObject * Sender)
 {
  if (DocChList->Selections[0]->Values[0])
   DocChList->Selections[0]->Values[0] = false;
  else
   DocChList->Selections[0]->Values[0] = true;
  DocChPropertiesEditValueChanged(NULL);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::SpecChPropertiesEditValueChanged(TObject * Sender)
 {
  bool En = false;

  if (SpecChList->SelectionCount && SpecChList->Selections[0] && SpecChList->IsEditing)
   En = (bool)SpecChList->InplaceEditor->EditValue;

  for (int i = 0; (i < SpecChList->Root->Count) && !En; i++)
   En |= (bool)SpecChList->Root->Items[i]->Values[0];
  actSave->Enabled    = En;
  actSaveDOS->Enabled = false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocExportDocForm::DocChPropertiesEditValueChanged(TObject * Sender)
 {
  bool En = false;

  if (DocChList->SelectionCount && DocChList->Selections[0] && DocChList->IsEditing)
   En = (bool)DocChList->InplaceEditor->EditValue;

  for (int i = 0; (i < DocChList->Root->Count) && !En; i++)
   En |= (bool)DocChList->Root->Items[i]->Values[0];
  actSave->Enabled = En;
  if (actSaveDOS->Visible)
   actSaveDOS->Enabled = En;
  else
   actSaveDOS->Enabled = false;
 }
//---------------------------------------------------------------------------
/*
 __fastcall TForm28::TForm28(TComponent * Owner)
 : TForm(Owner)
 {
 ConvDoc = NULL;
 try
 {
 try
 {
 ConvDoc = LoadLibrary(L"icdosconv.dll");
 }
 catch (...)
 {
 }
 if (ConvDoc)
 {
 FOpenProc    = (TOpenConvertProc )GetProcAddress(ConvDoc, "OpenConvert");
 FCloseProc   = (TCloseConvertProc )GetProcAddress(ConvDoc, "CloseConvert");
 FConvertProc = (TConvertDocProc )GetProcAddress(ConvDoc, "ConvertDoc");
 }
 else
 {
 }
 }
 catch (Exception & E)
 {
 MessageBox(NULL, E.Message.c_str(), L"icdosconv.dll", 0);
 }
 }
 //---------------------------------------------------------------------------
 void __fastcall TForm28::FormDestroy(TObject * Sender)
 {
 }
 //---------------------------------------------------------------------------
 void __fastcall TForm28::Button1Click(TObject * Sender)
 {
 //extern "C" bool __declspec(dllexport) __stdcall OpenConvert(char * AOrgCode)
 //extern "C" int __declspec(dllexport) __stdcall CloseConvert(char * AFileName)
 //extern "C" bool __declspec(dllexport) __stdcall ConvertDoc(char * ADoc, char * ASpec)
 TTagNode *spec = new TTagNode;
 TTagNode *doc = new TTagNode;
 int RC;
 if (FOpenProc("812.2741381.000"))
 {
 spec->LoadFromZIPXMLFile("D:\\work\\imm\\icv7\\Client\\test\\spec.zxml");
 spec->Encoding = "windows-1251";
 doc->LoadFromZIPXMLFile("D:\\work\\imm\\icv7\\Client\\test\\doc.zxml");
 doc->Encoding = "windows-1251";
 RC = FConvertProc(AnsiString(doc->AsXML).c_str(), AnsiString(spec->AsXML).c_str());
 if (RC)
 FCloseProc("D:\\work\\imm\\icv7\\Client\\test\\Win32\\Debug\\mail.let");
 }
 }
 */
