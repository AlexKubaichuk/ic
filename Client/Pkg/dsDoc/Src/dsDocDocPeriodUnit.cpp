//---------------------------------------------------------------------------

#include <vcl.h>
#include <DateUtils.hpp>
#pragma hdrstop

#include "dsDocDocPeriodUnit.h"
//#include "reportdef.h"
#include "msgdef.h"
#include "dsDocClientConstDef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//#ifndef _CONFSETTTING
// #error _CONFSETTTING using are not defined
//#endif

#pragma link "cxButtons"
#pragma link "cxCalendar"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDateUtils"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxRadioGroup"
#pragma link "cxSpinEdit"
#pragma link "cxTextEdit"
#pragma link "dxCore"
#pragma resource "*.dfm"
TdsDocPeriodForm *dsDocPeriodForm;
//---------------------------------------------------------------------------
__fastcall TdsDocPeriodForm::TdsDocPeriodForm(TComponent* Owner, TColor AReqColor, short int APeriodDef)
        : TForm(Owner)
{
  FPeriodDef = APeriodDef;
  PerTypeCB->Style->Color = AReqColor;
//  Caption = FMT(dsDocDocPeriodCaption);
  Caption = FMT(dsDocDocPeriodCaptionLabCaption);
  DateFrLab->Caption = FMT(dsDocDocPeriodDateFrLabCaption);
  DateToLab->Caption = FMT(dsDocDocPeriodDateToLabCaption);
//  YearRB->Caption = FMT(dsDocDocPeriodYearRBCaption);
//  QuarterRB->Caption = FMT(dsDocDocPeriodQuarterRBCaption);
//  MonsRB->Caption = FMT(dsDocDocPeriodMonsRBCaption);
//  PeriodRB->Caption = FMT(dsDocDocPeriodPeriodRBCaption);
//  HalfYearRB->Caption = FMT(dsDocDocPeriodHalfYearRBCaption);
  OkBtn->Caption = FMT(dsDocDocPeriodOkBtnCaption);
  CancelBtn->Caption = FMT(dsDocDocPeriodCancelBtnCaption);
  PerTypeCB->Properties->Items->Clear();

  unsigned short YY,MM,DD;
  TDateTime CDate = TDateTime::CurrentDate();
  CDate.DecodeDate(&YY, &MM, &DD);
  PeriodTypeLabel->Caption = FMT(dsDocDocPeriodLabCaption);
  switch(FPeriodDef)
  {
    case 1:
    { // <choicevalue name='����������� ��� ������������' value='1'/>
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems01), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems02), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems03), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems04), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems05), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems06), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems07), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems08), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems09), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems10), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems11), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems12), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject("------------------", (TObject*)0);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodYearRBCaption), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject("------------------", (TObject*)0);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodPeriodRBCaption), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject("------------------", (TObject*)0);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodHalfYearCBItems1), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodHalfYearCBItems2), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject("------------------", (TObject*)0);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodQuarterCBItems1), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodQuarterCBItems2), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodQuarterCBItems3), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodQuarterCBItems4), (TObject*)1);
      PerTypeCB->ItemIndex = -1;
      break;
    }
    case 2:
    { // <choicevalue name='�����' value='2'/>
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems01), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems02), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems03), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems04), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems05), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems06), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems07), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems08), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems09), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems10), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems11), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodMonsCBItems12), (TObject*)1);
      if (DD > 5)
       PerTypeCB->ItemIndex = MM-1;
      else
       {
         if (MM == 1)
          PerTypeCB->ItemIndex = 11; // �������
         else
          PerTypeCB->ItemIndex = MM-2;
       }
      break;
    }
    case 3:
    { // <choicevalue name='�������' value='3'/>
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodQuarterCBItems1), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodQuarterCBItems2), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodQuarterCBItems3), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodQuarterCBItems4), (TObject*)1);
      break;
    }
    case 4:
    { // <choicevalue name='���������' value='4'/>
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodHalfYearCBItems1), (TObject*)1);
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodHalfYearCBItems2), (TObject*)1);
      break;
    }
    case 5:
    { // <choicevalue name='���' value='5'/>
      PerTypeCB->Properties->Items->AddObject(FMT(dsDocDocPeriodYearRBCaption), (TObject*)1);
      PerTypeCB->ItemIndex = 0;
      PerTypeCB->Enabled = false;
      break;
    }
  }
  PerTypeCBPropertiesChange(NULL);
  DateFr = TDate(0);
  DateTo = TDate(0);
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPeriodForm::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if ((Key == VK_RETURN)&&OkBtn->Enabled && !PerTypeCB->DroppedDown)
   OkBtnClick(OkBtn);
}
//---------------------------------------------------------------------------
int __fastcall TdsDocPeriodForm::FGetPeriodValue(TDate &AFr, TDate &ATo, unsigned short AYear)
{
  int RC = 0;
  try
   {
     switch(FPeriodDef)
     {
       case 1: // <choicevalue name='����������� ��� ������������' value='1'/>
       case 2: // <choicevalue name='�����' value='2'/>
       {
         switch (PerTypeCB->ItemIndex)
         {
           case 0: // ������
           case 1: // �������
           case 2: // ����
           case 3: // ������
           case 4: // ���
           case 5: // ����
           case 6: // ����
           case 7: // ������
           case 8: // ��������
           {
             AFr = StrToDate("01.0"+IntToStr(PerTypeCB->ItemIndex+1)+"."+IntToStr(AYear));
             ATo = StrToDate(GetLastDay(PerTypeCB->ItemIndex+1, AYear)+".0"+IntToStr(PerTypeCB->ItemIndex+1)+"."+IntToStr(AYear));
             RC = 2;
             break;
           }
           case 9: //�������
           case 10: //������
           case 11: //�������
           {
             AFr = StrToDate("01."+IntToStr(PerTypeCB->ItemIndex+1)+"."+IntToStr(AYear));
             ATo = StrToDate(GetLastDay(PerTypeCB->ItemIndex+1, AYear)+"."+IntToStr(PerTypeCB->ItemIndex+1)+"."+IntToStr(AYear));
             RC = 2;
             break;
           }
           case 13:
           { // ���
             AFr = StrToDate("01.01."+IntToStr(AYear));
             ATo = StrToDate("31.12."+IntToStr(AYear));
             RC = 5;
             break;
           }
           case 15:
           { // �������� ���
             RC = 1;
             if (DateFrDE->Text.Length()) AFr = DateFrDE->Date;
             if (DateToDE->Text.Length()) ATo = DateToDE->Date;
             break;
           }
           case 17:
           { // 1-� ���������
             AFr = StrToDate("01.01."+IntToStr(AYear));
             ATo = StrToDate("30.06."+IntToStr(AYear));
             RC = 4;
             break;
           }
           case 18:
           { // 2-� ���������
             AFr = StrToDate("01.07."+IntToStr(AYear));
             ATo = StrToDate("31.12."+IntToStr(AYear));
             RC = 4;
             break;
           }
           case 20:
           { // 1-� �������
             AFr = StrToDate("01.01."+IntToStr(AYear));
             ATo = StrToDate("31.03."+IntToStr(AYear));
             RC = 3;
             break;
           }
           case 21:
           { // 2-� �������
             AFr = StrToDate("01.04."+IntToStr(AYear));
             ATo = StrToDate("30.06."+IntToStr(AYear));
             RC = 3;
             break;
           }
           case 22:
           { // 3-� �������
             AFr = StrToDate("01.07."+IntToStr(AYear));
             ATo = StrToDate("30.09."+IntToStr(AYear));
             RC = 3;
             break;
           }
           case 23:
           { // 4-� �������
             AFr = StrToDate("01.10."+IntToStr(AYear));
             ATo = StrToDate("31.12."+IntToStr(AYear));
             RC = 3;
             break;
           }
         }
         break;
       }
       case 3:
       { // <choicevalue name='�������' value='3'/>
         switch (PerTypeCB->ItemIndex)
         {
           case 0:
           { // 1-� �������
             AFr = StrToDate("01.01."+IntToStr(AYear));
             ATo = StrToDate("31.03."+IntToStr(AYear));
             break;
           }
           case 1:
           { // 2-� �������
             AFr = StrToDate("01.04."+IntToStr(AYear));
             ATo = StrToDate("30.06."+IntToStr(AYear));
             break;
           }
           case 2:
           { // 3-� �������
             AFr = StrToDate("01.07."+IntToStr(AYear));
             ATo = StrToDate("30.09."+IntToStr(AYear));
             break;
           }
           case 3:
           { // 4-� �������
             AFr = StrToDate("01.10."+IntToStr(AYear));
             ATo = StrToDate("31.12."+IntToStr(AYear));
             break;
           }
         }
         RC = 3;
         break;
       }
       case 4:
       { // <choicevalue name='���������' value='4'/>
         switch (PerTypeCB->ItemIndex)
         {
           case 0:
           { // 1-� ���������
             AFr = StrToDate("01.01."+IntToStr(AYear));
             ATo = StrToDate("30.06."+IntToStr(AYear));
             break;
           }
           case 1:
           { // 2-� ���������
             AFr = StrToDate("01.07."+IntToStr(AYear));
             ATo = StrToDate("31.12."+IntToStr(AYear));
             break;
           }
         }
         RC = 4;
         break;
       }
       case 5:
       { // <choicevalue name='���' value='5'/>
         AFr = StrToDate("01.01."+IntToStr(AYear));
         ATo = StrToDate("31.12."+IntToStr(AYear));
         RC = 5;
         break;
       }
     }
   }
  catch (...)
   {
     _MSG_ERR(FMT(dsDocDocPeriodErrorDateFormat),FMT(dsDocCommonErrorCaption));
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPeriodForm::OkBtnClick(TObject *Sender)
{
  ActiveControl = OkBtn;
  try
   {
     pdType = FGetPeriodValue(DateFr, DateTo, YearED->Value);
   }
  catch (...)
   {
      _MSG_ERR(FMT(dsDocDocPeriodErrorDateFormat),FMT(dsDocCommonErrorCaption));
      return;
   }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocPeriodForm::GetLastDay(int AMons, int AYear)
{
  if (IsValidDate((Word)AYear, (Word)AMons, (Word)1))
   {// ��� � ����� ����������
     return IntToStr(DaysInAMonth((Word)AYear, (Word)AMons));
   }
  else
   return "1";
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPeriodForm::PerTypeCBPropertiesChange(TObject *Sender)
{
  TDateTime FDateFr,FDateTo;
  unsigned short YY,MM,DD;
  TDateTime CDate = TDateTime::CurrentDate();
  CDate.DecodeDate(&YY, &MM, &DD);
  if (PerTypeCB->ItemIndex != -1)
   {
     if (PerTypeCB->ItemIndex == 15)
      { // ������
        YearED->Enabled = false;
        DateFrLab->Enabled = true;
        DateFrDE->Enabled = true;
        DateToLab->Enabled = true;
        DateToDE->Enabled = true;
        DateToDE->Date = TDateTime::CurrentDate();
      }
     else
      {

        YearED->Enabled = true;

        DateFrLab->Enabled = false;
        DateFrDE->Enabled = false;
        DateToLab->Enabled = false;
        DateToDE->Enabled = false;

        DateFrDE->Text = "";
        DateToDE->Text = "";

        if (FGetPeriodValue(FDateFr, FDateTo, YY) == 5)
         { // ������ "���"
           if (MM > 1)  YearED->Value  = YY;
           else         YearED->Value  = YY-1;
         }
        else
         { // ������ �� "���"
           if (FDateFr > CDate) YearED->Value  = YY-1;
           else                 YearED->Value  = YY;
         }
      }
   }
  else
   {
     DateFrLab->Enabled = false;
     DateFrDE->Enabled  = false;
     DateToLab->Enabled = false;
     DateToDE->Enabled  = false;
     DateFrDE->Text     = "";
     DateToDE->Text     = "";
     YearED->Enabled    = false;
   }
  CheckCtrl();
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPeriodForm::FormShow(TObject *Sender)
{
  if (FPeriodDef == 5)
    YearED->SetFocus();
  else
    PerTypeCB->SetFocus();
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPeriodForm::CheckCtrl()
{
  OkBtn->Enabled = false;

  if (PerTypeCB->ItemIndex != -1)
   {
     if ((FPeriodDef == 1) && (PerTypeCB->ItemIndex == 15) )
      OkBtn->Enabled = (DateFrDE->Text.Length() || DateToDE->Text.Length());
     else
      OkBtn->Enabled = (int)PerTypeCB->Properties->Items->Objects[PerTypeCB->ItemIndex];
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPeriodForm::DateFrDEPropertiesChange(TObject *Sender)
{
  CheckCtrl();
}
//---------------------------------------------------------------------------


