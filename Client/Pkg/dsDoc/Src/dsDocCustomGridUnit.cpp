//---------------------------------------------------------------------------
#include <vcl.h>
//#include <stdio.h>
//#include <clipbrd.hpp>
#pragma hdrstop

#include "dsDocCustomGridUnit.h"
//#include "dsDocDocPeriodUnit.h"
//#include "dsDocPackPeriodUnit.h"
//#include "dsDocClientConstDef.h"
//#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//###########################################################################
//#                                                                         #
//#                         TicsGridData                                    #
//#                                                                         #
//###########################################################################
//---------------------------------------------------------------------------
__fastcall TicsGridData::TicsGridData()
{
  FRecords = new TList;
  FDataRecords = new TList;
}
//---------------------------------------------------------------------------
__fastcall TicsGridData::~TicsGridData()
{
  Clear();
  delete FRecords;
  delete FDataRecords;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TicsGridData::Add()
{
  TTagNode *RC = new TTagNode(NULL);
  RC->Name = "r";
  FRecords->Add(RC);
  FDataRecords->Add(NULL);
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TicsGridData::Clear()
{
  while (FRecords->Count > 0)
  {
    delete FRecords->Items[FRecords->Count - 1];
    FRecords->Delete(FRecords->Count - 1);
  }
}
//---------------------------------------------------------------------------
int __fastcall TicsGridData::FGetCount()
{
  return FRecords->Count;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TicsGridData::FGetValues(int AIdx)
{
  return ((TTagNode*)FRecords->Items[AIdx]);
}
//---------------------------------------------------------------------------
TObject* __fastcall TicsGridData::FGetData(int AIdx)
{
  return ((TObject*)FDataRecords->Items[AIdx]);
}
//---------------------------------------------------------------------------
void __fastcall TicsGridData::FSetData(int AIdx, TObject* AData)
{
  FDataRecords->Items[AIdx] = (void*)AData;
}
//---------------------------------------------------------------------------
//###########################################################################
//#                                                                         #
//#                         TicsGridData                                    #
//#                                                                         #
//###########################################################################
//---------------------------------------------------------------------------
__fastcall TicsGridDataSource::TicsGridDataSource(TicsGridData *ADataOwner) : TcxCustomDataSource()
{
  FDataOwner = ADataOwner;
}
//---------------------------------------------------------------------------
//Returns the TcxGridColumn.DataBinding property for a specified visible index of a column. This property is initialized when a grid column is created.
TcxGridItemDataBinding* __fastcall TicsGridDataSource::GetDataBinding(int AItemIndex)
{
  return ((TcxCustomGridTableItem*)DataController->GetItem(AItemIndex))->DataBinding;
}
//---------------------------------------------------------------------------
//Returns TcxGridColumn.DataBinding as an item handle for a specified visible index of a column
void* __fastcall TicsGridDataSource::GetItemHandle(int AItemIndex)
{
  return (TcxDataItemHandle*)GetDataBinding(AItemIndex);
}
//---------------------------------------------------------------------------
//Returns a pointer to the memory address at which the value of the desired field resides. AItemHandle is an item handle retrieved by the GetItemHandle method.
bool __fastcall TicsGridDataSource::GetInfoForCompare(void * ARecordHandle, void * AItemHandle, char * &PValueBuffer)
{
/*  TcxGridItemDataBinding * ADataBinding = (TcxGridItemDataBinding*)AItemHandle;
  TTagNode *APEntry = (TTagNode*)(*FDataOwner->Records)[(int)ARecordHandle];
  UnicodeString FID = IntToStr(int(ADataBinding->Data));
  PValueBuffer = &*(APEntry->AV[FID]).c_str();
//  PValueBuffer = (char*)&( APEntry->AV[IntToStr(int(ADataBinding->Data))] );  */

  return true;
}
//---------------------------------------------------------------------------
//Returns the number of records to display in a grid control
int __fastcall TicsGridDataSource::GetRecordCount()
{
  return FDataOwner->Count;
}
//---------------------------------------------------------------------------
//Returns a specific field value from the TListEntry object. ARecordHandle identifies a record in the list. AItemHandle is an item (column) handle retrieved by the GetItemHandle method.
Variant __fastcall TicsGridDataSource::GetValue(void * ARecordHandle, void * AItemHandle)
{
  return FDataOwner->Values[(int)ARecordHandle]->AV[IntToStr(int(((TcxGridItemDataBinding*)AItemHandle)->Data))];

//  TcxGridItemDataBinding *ADataBinding = (TcxGridItemDataBinding*)AItemHandle;
//  TTagNode *APEntry = (TTagNode*)(*FDataOwner->Records)[(int)ARecordHandle];

//  return APEntry->AV[IntToStr(int(ADataBinding->Data))];
}
//---------------------------------------------------------------------------
void __fastcall TicsGridDataSource::SetValue(void * ARecordHandle, void * AItemHandle, const Variant &AValue)
{
  FDataOwner->Values[(int)ARecordHandle]->AV[IntToStr(int(((TcxGridItemDataBinding*)AItemHandle)->Data))] = VarToWideStrDef(AValue, "");

//  TcxGridItemDataBinding *ADataBinding = (TcxGridItemDataBinding*)AItemHandle;
//  TTagNode *APEntry = (TTagNode*)(*FDataOwner->Records)[(int)ARecordHandle];

//  APEntry->AV[IntToStr(int(ADataBinding->Data))] = UnicodeString(AValue);
}
//---------------------------------------------------------------------------
//Since the TicsGridDataSource provides the GetInfoForCompare procedure, the IsNativeCompare function should return True.
bool __fastcall TicsGridDataSource::IsNativeCompare()
{
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TicsGridDataSource::SetColumnID(TcxGridTableView *AView)
{
  for (int i = 0; i < AView->ColumnCount; i++)
   {
     AView->Columns[i]->DataBinding->Data = (TObject*)i;
   }
}
//---------------------------------------------------------------------------
