/*
  ����        - ICSDocGlobals.h
  ������      - ��������������� (ICS)
  ��������    - ������ ������ ����������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 11.11.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  27.12.2006
    [+] ������� UpdateXMLAttr
    [+] ��������� csXMLDateFmtStr
    [+] ��������� cchXMLDateSeparator
    [+] ��������� cchXMLPeriodSeparator
    [+] ��������� cchXMLDurationSeparator
    [+] ������� DateToXMLStr
    [+] ������� XMLStrToDate
    [+] ������� PeriodToXMLStr
    [+] ������� XMLStrToPeriod
    [+] ������� DurationToXMLStr
    [+] ������� XMLStrToDuration

  16.03.2006
    [-] ��������� XML-���������

  11.11.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef ICSDocGlobalsH
#define ICSDocGlobalsH

//---------------------------------------------------------------------------

//#include "ICSDoc.h"	//��������� �������� � ���� ��������
#include <System.hpp>
#include <Vcl.ComCtrls.hpp>
#include "XMLContainer.h"
#include <list>

namespace ICSDocGlobals
{
using namespace std;
//###########################################################################
//##                                                                       ##
//##                                Internals                              ##
//##                                                                       ##
//###########################################################################

// ���������� ��������������� �������� ��� �������� �� ��������� ��
// ���������, ���� �� �����������
void __fastcall UpdateXMLAttr(
  TTagNode *ANode,
  UnicodeString AName,
  UnicodeString AValue,
  bool *AModified
);

//###########################################################################
//##                                                                       ##
//##                                 Publics                               ##
//##                                                                       ##
//###########################################################################

typedef list<UnicodeString> TUnicodeStringList;
typedef list<TTagNode*> TTagNodeList;
typedef list<TTagNodeList> TTagNodeList2;
typedef list<TTreeNode*> TTreeNodeList;

//---------------------------------------------------------------------------

extern PACKAGE const UnicodeString csXMLDateFmtStr;
extern PACKAGE const char cchXMLDateSeparator;
extern PACKAGE const char cchXMLPeriodSeparator;
extern PACKAGE const char cchXMLDurationSeparator;

//---------------------------------------------------------------------------

extern PACKAGE UnicodeString __fastcall DateToXMLStr(const TDate &AValue);
// throw:
//   EConvertError - ������ ��������������
extern PACKAGE TDate __fastcall XMLStrToDate(UnicodeString AValue);
extern PACKAGE UnicodeString __fastcall PeriodToXMLStr(const TDate &ABeg, const TDate &AEnd);
// throw:
//   EConvertError - ������ ��������������
extern PACKAGE void __fastcall XMLStrToPeriod(UnicodeString AValue, TDate *ABeg, TDate *AEnd);
extern PACKAGE UnicodeString __fastcall DurationToXMLStr(const int AYears, const int AMonths, const int ADays);
// throw:
//   EConvertError - ������ ��������������
extern PACKAGE void __fastcall XMLStrToDuration(UnicodeString AValue, int *AYears, int *AMonths, int *ADays);

//---------------------------------------------------------------------------

} //end of namespace ICSDocGlobals
using namespace ICSDocGlobals;

#endif
