/*
  ����        - ICSDocGlobals.cpp
  ������      - ��������������� (ICS)
  ��������    - ������ ������ ����������.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 11.11.2004
*/
//---------------------------------------------------------------------------

#pragma hdrstop

#include "icsDocSpecConstDef.h"
#include "ICSDocGlobals.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------

const UnicodeString ICSDocGlobals::csXMLDateFmtStr   = "dd.mm.yyyy";
const char ICSDocGlobals::cchXMLDateSeparator     = '.';
const char ICSDocGlobals::cchXMLPeriodSeparator   = '-';
const char ICSDocGlobals::cchXMLDurationSeparator = '/';

//---------------------------------------------------------------------------
void __fastcall ICSDocGlobals::UpdateXMLAttr(  TTagNode *ANode,  UnicodeString AName,  UnicodeString AValue,  bool *AModified)
{
  if (!ANode->GetAttrByName(AName))
  {
    ANode->AV[AName] = AValue;
    *AModified = true;
  }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall ICSDocGlobals::DateToXMLStr(const TDate &AValue)
{
  return AValue.FormatString(csXMLDateFmtStr);
}

//---------------------------------------------------------------------------

TDate __fastcall ICSDocGlobals::XMLStrToDate(UnicodeString AValue)
{
  AValue = AValue.Trim();
  const UnicodeString csErrMsg = "\"" + AValue + "\" is not a valid date.";
  if (!AValue.Length())
    throw EConvertError(csErrMsg);

  unsigned short usYear = 0, usMonth = 0, usDay = 0;
  TStrings *sl = new TStringList;
  try
  {
    try
    {
      sl->Text = StringReplace(AValue, cchXMLDateSeparator, "\n", TReplaceFlags() << rfReplaceAll);
      if (sl->Count < 3)
        throw EConvertError(csErrMsg);
      #pragma warn -sig
      usDay   = sl->Strings[0].ToInt();
      usMonth = sl->Strings[1].ToInt();
      usYear  = sl->Strings[2].ToInt();
      #pragma warn .sig
    }
    catch(EConvertError&)
    {
      throw EConvertError(csErrMsg);
    }
  }
  __finally
  {
    delete sl;
  }
  return EncodeDate(usYear, usMonth, usDay);
}

//---------------------------------------------------------------------------

UnicodeString __fastcall ICSDocGlobals::PeriodToXMLStr(const TDate &ABeg, const TDate &AEnd)
{
  return ABeg.FormatString(csXMLDateFmtStr) +
         UnicodeString(cchXMLPeriodSeparator) +
         AEnd.FormatString(csXMLDateFmtStr);
}

//---------------------------------------------------------------------------

void __fastcall ICSDocGlobals::XMLStrToPeriod(UnicodeString AValue, TDate *ABeg, TDate *AEnd)
{
  AValue = AValue.Trim();
  const UnicodeString csErrMsg = "\"" + AValue + "\" is not a valid period.";
  if (!AValue.Length())
    throw EConvertError(csErrMsg);

  UnicodeString sBeg = GetPart1(AValue, cchXMLPeriodSeparator);
  UnicodeString sEnd = GetPart2(AValue, cchXMLPeriodSeparator);
  unsigned short usBegYear = 0, usBegMonth = 0, usBegDay = 0,
                 usEndYear = 0, usEndMonth = 0, usEndDay = 0;
  TStrings *sl = new TStringList;
  try
  {
    try
    {
      sl->Text = StringReplace(sBeg, cchXMLDateSeparator, "\n", TReplaceFlags() << rfReplaceAll);
      if (sl->Count < 3)
        throw EConvertError(csErrMsg);
      #pragma warn -sig
      usBegDay   = sl->Strings[0].ToInt();
      usBegMonth = sl->Strings[1].ToInt();
      usBegYear  = sl->Strings[2].ToInt();
      sl->Text = StringReplace(sEnd, cchXMLDateSeparator, "\n", TReplaceFlags() << rfReplaceAll);
      if (sl->Count < 3)
        throw EConvertError(csErrMsg);
      usEndDay   = sl->Strings[0].ToInt();
      usEndMonth = sl->Strings[1].ToInt();
      usEndYear  = sl->Strings[2].ToInt();
      #pragma warn .sig
    }
    catch(EConvertError&)
    {
      throw EConvertError(csErrMsg);
    }
  }
  __finally
  {
    delete sl;
  }
  *ABeg = EncodeDate(usBegYear, usBegMonth, usBegDay);
  *AEnd = EncodeDate(usEndYear, usEndMonth, usEndDay);
}

//---------------------------------------------------------------------------

UnicodeString __fastcall ICSDocGlobals::DurationToXMLStr(const int AYears, const int AMonths, const int ADays)
{
  return IntToStr((int)AYears) +
         UnicodeString(cchXMLDurationSeparator) +
         IntToStr((int)AMonths) +
         UnicodeString(cchXMLDurationSeparator) +
         IntToStr((int)ADays);
}

//---------------------------------------------------------------------------

void __fastcall ICSDocGlobals::XMLStrToDuration(UnicodeString AValue, int *AYears, int *AMonths, int *ADays)
{
  AValue = AValue.Trim();
  const UnicodeString csErrMsg = "\"" + AValue + "\" is not a valid duration.";
  if (!AValue.Length())
    throw EConvertError(csErrMsg);

  TStrings *sl = new TStringList;
  try
  {
    try
    {
      sl->Text = StringReplace(AValue, cchXMLDurationSeparator, "\n", TReplaceFlags() << rfReplaceAll);
      if (sl->Count < 3)
        throw EConvertError(csErrMsg);
      *AYears  = sl->Strings[0].ToInt();
      *AMonths = sl->Strings[1].ToInt();
      *ADays   = sl->Strings[2].ToInt();
    }
    catch(EConvertError&)
    {
      throw EConvertError(csErrMsg);
    }
  }
  __finally
  {
    delete sl;
  }
}

//---------------------------------------------------------------------------
