//---------------------------------------------------------------------------

#ifndef dsDocDistributionListH
#define dsDocDistributionListH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <System.JSON.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include "cxButtons.hpp"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"
/*
#include "cxButtons.hpp"
//#include "dsDocDMUnit.h"
*/
//---------------------------------------------------------------------------
#include "XMLContainer.h"
//---------------------------------------------------------------------------
class TExtDistForm : public TForm
{
__published:	// IDE-managed Components
 TPanel *RightPanel;
 TPanel *LeftPanel;
 TPanel *BtnPanel;
 TdxBarManager *ListTBM;
 TdxBar *ListTBMBar1;
 TdxBarLargeButton *dxBarLargeButton1;
 TdxBarLargeButton *dxBarLargeButton2;
 TcxImageList *MainIL16;
 TcxImageList *MainIL32;
 TPopupMenu *MainPM;
 TMenuItem *N1;
 TMenuItem *N2;
 TActionList *MainAL;
 TAction *actAddSpec;
 TAction *actDeleteSpec;
 TAction *actApply;
 TAction *actCancel;
 TLabel *ComLab;
 TLabel *CurLab;
 TPanel *DefPanel;
 TLabel *Label1;
 TcxTreeList *CurTL;
 TcxTreeListColumn *CurTLName;
 TcxTreeListColumn *CurTLModif;
 TcxTreeListColumn *CurTLGUI;
 TcxTreeList *ComTL;
 TcxTreeListColumn *ComTLName;
 TcxTreeListColumn *ComTLModif;
 TcxTreeListColumn *ComTLGUI;
 TcxButton *SaveBtn;
 TcxButton *CancelBtn;
 TcxComboBox *DefCB;
 void __fastcall ComTLDragDrop(TObject *Sender, TObject *Source, int X, int Y);
 void __fastcall ComTLDragOver(TObject *Sender, TObject *Source, int X, int Y, TDragState State,
          bool &Accept);
 void __fastcall CurTLDragDrop(TObject *Sender, TObject *Source, int X, int Y);
 void __fastcall CurTLDragOver(TObject *Sender, TObject *Source, int X, int Y, TDragState State,
          bool &Accept);
 void __fastcall actAddSpecExecute(TObject *Sender);
 void __fastcall actDeleteSpecExecute(TObject *Sender);
 void __fastcall actApplyExecute(TObject *Sender);
 void __fastcall actCancelExecute(TObject *Sender);
private:	// User declarations
  typedef map<int,UnicodeString> TGUIIdxMap;
  TAnsiStrMap FExistMap;
  TAnsiStrMap FCommonMap;
  TGUIIdxMap  FDefCodes;
  bool FUseDef;
  int FDefIdx;
  int FIdx;
  TJSONObject* __fastcall FGetList();

public:		// User declarations
  __fastcall TExtDistForm(TComponent* Owner, UnicodeString ACaption, TStringList *ADest, TStringList *ACommSrc, bool AUseDef);
  __property TJSONObject* List = {read=FGetList};
};
//---------------------------------------------------------------------------
extern PACKAGE TExtDistForm *ExtDistForm;
//---------------------------------------------------------------------------
#endif
