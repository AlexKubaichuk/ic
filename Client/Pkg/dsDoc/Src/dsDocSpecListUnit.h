//---------------------------------------------------------------------------
#ifndef dsDocSpecListUnitH
#define dsDocSpecListUnitH
//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxClasses.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxData.hpp"
#include "cxDataStorage.hpp"
#include "cxEdit.hpp"
#include "cxFilter.hpp"
#include "cxGraphics.hpp"
#include "cxGrid.hpp"
#include "cxGridCustomTableView.hpp"
#include "cxGridCustomView.hpp"
#include "cxGridLevel.hpp"
#include "cxGridTableView.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxNavigator.hpp"
#include "cxStyles.hpp"
#include "dxBar.hpp"
#include "dxStatusBar.hpp"
//---------------------------------------------------------------------------
#include "dsDocDMUnit.h"
//---------------------------------------------------------------------------
//#include "dsProgress.h"
class PACKAGE TdsDocSpecListForm : public TForm
{
__published:	// IDE-managed Components
 TActionList *MainAL;
        TAction *actNewSpec;
        TAction *actEditSpec;
        TAction *actDeleteSpec;
        TAction *actFindSpec;
        TAction *actCreateDoc;
        TAction *actHandCreate;
        TAction *actCopySpec;
        TOpenDialog *OpenDlg;
        TAction *actAddTemplate;
        TAction *actClearTemplate;
        TAction *actSaveToXML;
        TAction *actLoadFromXML;
        TSaveDialog *SaveDlg;
        TdxBarManager *ListTBM;
        TPopupMenu *MainPM;
        TMenuItem *N1;
        TMenuItem *N2;
        TdxBarCombo *FltCB;
        TdxBarButton *dxBarButton8;
        TdxBarButton *dxBarButton9;
        TdxBarButton *dxBarButton10;
        TdxBarButton *dxBarButton11;
        TMenuItem *N3;
        TMenuItem *N4;
        TMenuItem *N5;
        TMenuItem *N6;
        TdxStatusBar *StatusSB;
 TcxStyleRepository *Style;
        TcxStyle *stEnabled;
        TcxStyle *stDisabled;
        TcxStyle *cxStyle1;
        TcxStyle *cxStyle2;
        TAction *actCreateDebugDoc;
        TAction *actChoiceLibOpen;
        TcxImageList *LMainIL;
        TdxBarLargeButton *dxBarLargeButton1;
        TdxBarLargeButton *dxBarLargeButton2;
        TdxBarLargeButton *dxBarLargeButton3;
        TdxBarLargeButton *dxBarLargeButton4;
        TdxBarLargeButton *dxBarLargeButton6;
        TdxBarLargeButton *dxBarLargeButton7;
        TdxBarLargeButton *dxBarLargeButton8;
        TcxStyle *cxStyle3;
        TdxBarPopupMenu *CreateSpecPM;
        TAction *actNewPack;
        TAction *actNewPackDoc;
        TdxBarLargeButton *dxBarLargeButton9;
        TdxBarLargeButton *dxBarLargeButton10;
        TdxBarLargeButton *dxBarLargeButton11;
        TAction *actNew;
        TAction *actCreateDocByPeriod;
        TcxStyle *PackStyle;
        TcxStyle *DocStyle;
        TdxBarPopupMenu *CreateDocPM;
        TdxBarLargeButton *dxBarLargeButton12;
 TTimer *ShowTimer;
 TdxStatusBarContainerControl *StatusSBContainer0;
 TdxStatusBarContainerControl *StatusSBContainer1;
 TProgressBar *PrBar;
 TLabel *MsgLab;
 TcxGrid *SpecList;
 TcxGridTableView *SpecListView;
 TcxGridColumn *ldvTypeCol;
 TcxGridColumn *ldvNameCol;
 TcxGridColumn *ldvPeriodCol;
 TcxGridColumn *ldvAuthorCol;
 TcxGridColumn *ldvCreateDateCol;
 TcxGridLevel *SpecListLevel1;
 TAction *actClearFilterCB;
 TcxGridColumn *SpecListViewColumn1;
 TdxBarLargeButton *dxBarLargeButton13;
 TdxBarLargeButton *dxBarLargeButton14;
 TcxStyle *cxStyle4;
 TAction *actExportSpec;
 TAction *actImportSpec;
 TcxImageList *MainIL;
 TdxBarLargeButton *dxBarLargeButton5;
 TdxBarLargeButton *dxBarLargeButton15;
 TdxBarLargeButton *dxBarLargeButton16;
 TAction *actRefresh;
 TAction *actViewDoc;
 TdxBarLargeButton *dxBarLargeButton17;
        void        __fastcall actCreateDocExecute(TObject *Sender);
        void        __fastcall actNewSpecExecute(TObject *Sender);
        void        __fastcall actEditSpecExecute(TObject *Sender);
        void        __fastcall actDeleteSpecExecute(TObject *Sender);
        TExistsType __fastcall CheckExists(TTagNode *ANode, UnicodeString &AGUI, bool AIsEdit);
        void __fastcall actHandCreateExecute(TObject *Sender);
        void __fastcall actCopySpecExecute(TObject *Sender);
        void __fastcall actAddTemplateExecute(TObject *Sender);
        void __fastcall actClearTemplateExecute(TObject *Sender);
        void __fastcall actSaveToXMLExecute(TObject *Sender);
        void __fastcall actLoadFromXMLExecute(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall actCreateDebugDocExecute(TObject *Sender);
        void __fastcall actChoiceLibOpenExecute(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall actNewPackExecute(TObject *Sender);
        void __fastcall actNewPackDocExecute(TObject *Sender);
        void __fastcall actNewExecute(TObject *Sender);
        void __fastcall actCreateDocByPeriodExecute(TObject *Sender);
 void __fastcall ShowTimerTimer(TObject *Sender);
 void __fastcall FormShow(TObject *Sender);
 void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
 void __fastcall SpecListViewStylesGetContentStyle(TcxCustomGridTableView *Sender,
          TcxCustomGridRecord *ARecord, TcxCustomGridTableItem *AItem,
          TcxStyle *&AStyle);
 void __fastcall SpecListViewFocusedRecordChanged(TcxCustomGridTableView *Sender,
          TcxCustomGridRecord *APrevFocusedRecord, TcxCustomGridRecord *AFocusedRecord,
          bool ANewItemRecordFocusingChanged);
 void __fastcall actClearFilterCBExecute(TObject *Sender);
 void __fastcall actExportSpecExecute(TObject *Sender);
 void __fastcall actImportSpecExecute(TObject *Sender);
 void __fastcall actRefreshExecute(TObject *Sender);
 void __fastcall actViewDocExecute(TObject *Sender);
private:	// User declarations
  TkabCustomDataSource *FDataSrc;
  TTagNode     *RootClass;
  TdsRegEDContainer *FCtrList;
  __int64    xpCount,xcTop;
  UnicodeString FSaveCaption;
//  TdsProgressForm *FPrForm;
        UnicodeString curCD, FSpecOwner, NmklGUI;
        TTagNode *Spec;
        TTagNode *FDocCondRules;
        TTagNode *Doc;
        TdsDocDM *FDM;
        TAnsiStrMap FOwnerNameMap;
        TStringList *FltList;


  bool __fastcall CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg);
  void __fastcall FGetDataProgress(int AMax, int ACur, UnicodeString AMsg);
  void __fastcall RefreshById(UnicodeString ACode);
  void __fastcall CreateSrc();
  void __fastcall Reopen();
  void __fastcall CheckCtrlState(bool AEnable);
  void __fastcall CheckCtrlVisible();
  bool __fastcall CreateListExt(TTagNode *itTag, UnicodeString &UID);
  void __fastcall FLoadChoice();
  TkabCustomDataSetRow* __fastcall CurRow();
  TkabCustomDataSetRow* __fastcall EditedRow();
  bool __fastcall IsRecSelected();

        void __fastcall quSpecAfterScroll(TDataSet *DataSet);
        void __fastcall FNewSpec(int ASpecType);
        void __fastcall quSpecF_OWNERGetText(TField *Sender, UnicodeString &Text, bool DisplayText);
public:		// User declarations
        __fastcall TdsDocSpecListForm(TComponent* Owner, TdsDocDM *ADM);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocSpecListForm *dsDocSpecListForm;
//---------------------------------------------------------------------------
#endif
