﻿//---------------------------------------------------------------------------

#ifndef dsDocHandCreateTypeUnitH
#define dsDocHandCreateTypeUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxRadioGroup.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
//#include <map>
#include "dsDocDMUnit.h"
//---------------------------------------------------------------------------
class TdsDocCreateTypeForm : public TForm
{
typedef map<int,UnicodeString> TIntStrMap;
__published:	// IDE-managed Components
 TLabel *Label1;
 TPanel *Panel1;
 TcxComboBox *OrgCB;
 TcxButton *ApplyBtn;
 TcxButton *CancelBtn;
        void __fastcall OrgCBPropertiesChange(TObject *Sender);
private:	// User declarations
        UnicodeString FOrgCode;
        TIntStrMap  FOrgCodeMap;
        TdsDocDM   *FDM;
        int __fastcall FLoadSubOrg(UnicodeString AOwnerCode);
public:		// User declarations
        __fastcall TdsDocCreateTypeForm(TComponent* Owner, TdsDocDM *ADM);
        __property UnicodeString OrgCode = {read = FOrgCode};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocCreateTypeForm *dsDocCreateTypeForm;
//---------------------------------------------------------------------------
#endif
