//---------------------------------------------------------------------------
#include <vcl.h>

#pragma hdrstop

#include "dsDocExtFunc.h"
#include "dsDocClientConstDef.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------
TTagNode* __fastcall GetPassport(TTagNode *ANode, bool AFromRoot)
{
  if (AFromRoot)
   return ANode->GetRoot()->GetChildByName("passport");
  else
   return ANode->GetChildByName("passport");
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetSpecName(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = FPassport->AV["mainname"];
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocName(TTagNode *ANode, bool AFromRoot)
{
  return GetSpecName(ANode, AFromRoot);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetSpecGUI(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = FPassport->AV["GUI"];
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocGUI(TTagNode *ANode, bool AFromRoot)
{
   return GetSpecGUI(ANode, AFromRoot);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetPeriodDef(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = FPassport->GetAVDef("perioddef","0");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetStrSpecPeriod(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = FPassport->GetAVDef("perioddef","0");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
int __fastcall GetIntSpecPeriod(TTagNode *ANode, bool AFromRoot)
{
  return GetStrSpecPeriod(ANode, AFromRoot).ToIntDef(0);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocPeriod(TTagNode *ANode, bool AFromRoot)
{
   return GetPeriodDef(ANode, AFromRoot);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetSpecOwner(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = FPassport->AV["autor"];
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocOwner(TTagNode *ANode, bool AFromRoot)
{
   return GetSpecOwner(ANode, AFromRoot);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocSpecGUI(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = FPassport->AV["GUIspecificator"];
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall SetDocSpecGUI(TTagNode *ANode,UnicodeString AVal, bool AFromRoot)
{
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      FPassport->AV["GUIspecificator"] = AVal;
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall SetSpecName(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      FPassport->AV["mainname"] = AVal;
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall SetDocName(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
   SetSpecName(ANode, AVal, AFromRoot);
}
//---------------------------------------------------------------------------
void __fastcall SetSpecGUI(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      FPassport->AV["GUI"] = AVal;
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall SetDocGUI(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
   SetSpecGUI(ANode, AVal, AFromRoot);
}
//---------------------------------------------------------------------------
void __fastcall SetSpecPeriod(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      FPassport->AV["perioddef"] = AVal;
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall SetDocPeriod(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
   SetSpecPeriod(ANode, AVal, AFromRoot);
}
//---------------------------------------------------------------------------
void __fastcall SetSpecOwner(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      FPassport->AV["autor"] = AVal;
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall SetDocOwner(TTagNode *ANode, UnicodeString AVal, bool AFromRoot)
{
   SetSpecOwner(ANode, AVal, AFromRoot);
}
//---------------------------------------------------------------------------
int __fastcall DocPeriodInt(UnicodeString APeriod)
{
  return APeriod.ToIntDef(0);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall SpecPeriodRuStr(int APeriod)
{
  UnicodeString RC = "���";
  try
   {
     switch (APeriod)
      {
        //  <choicevalue name='���' value='0'/>
        case 0: RC = "���"; break;
        //  <choicevalue name='����������� ��� ������������' value='1'/>
        case 1: RC = "����������� ��� ������������"; break;
        //  <choicevalue name='�����' value='2'/>
        case 2: RC = "�����"; break;
        //  <choicevalue name='�������' value='3'/>
        case 3: RC = "�������"; break;
        //  <choicevalue name='���������' value='4'/>
        case 4: RC = "���������"; break;
        //  <choicevalue name='���' value='5'/>
        case 5: RC = "���"; break;
        default : RC = "���";
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall DocPeriodRuStr(int APeriod)
{
  switch (APeriod)
   {
     case 5  : return FMT(dsDocExtFuncDocPeriodLocStr1);
     case 4  : return FMT(dsDocExtFuncDocPeriodLocStr2);
     case 3  : return FMT(dsDocExtFuncDocPeriodLocStr3);
     case 2  : return FMT(dsDocExtFuncDocPeriodLocStr4);
     case 1  : return FMT(dsDocExtFuncDocPeriodLocStr5);
     default : return FMT(dsDocExtFuncDocPeriodLocStr6);
   }
}
//---------------------------------------------------------------------------
void __fastcall SetDocPeriodVal(TTagNode *ANode, UnicodeString AFVal, UnicodeString ATVal, bool AFromRoot)
{
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      FPassport->AV["periodval"] = AFVal+"-"+ATVal;
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocPeriodValFrom(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = GetPart1(FPassport->AV["periodval"],'-').Trim();
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocPeriodValTo(TTagNode *ANode, bool AFromRoot)
{
  UnicodeString RC = "";
  try
   {
     TTagNode *FPassport = GetPassport(ANode, AFromRoot);
     if (FPassport)
      RC = GetPart2(FPassport->AV["periodval"],'-').Trim();;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetDocPeriodValStr(UnicodeString APerType, UnicodeString AValFrom, UnicodeString AValTo)
{
   UnicodeString RC = "";
   UnicodeString FFrDay,FFrMonth,FFrYear,FToDay,FToMonth,FToYear,FFrMonthFull,FToMonthFull;
   FFrDay = "";FFrMonth = "";FFrYear = "";FToDay = "";FToMonth = "";FToYear = "";FFrMonthFull = "";FToMonthFull = "";
   TDateTime tmpDate;
   if (TryStrToDate(AValFrom,tmpDate))
    {
      FFrDay       = StrToDate(AValFrom).FormatString("dd");
      FFrMonth     = StrToDate(AValFrom).FormatString("mm");
      FFrMonthFull = StrToDate(AValFrom).FormatString("mmmm");
      FFrYear      = StrToDate(AValFrom).FormatString("yyyy");
    }
   if (TryStrToDate(AValTo,tmpDate))
    {
      FToDay       = StrToDate(AValTo).FormatString("dd");
      FToMonth     = StrToDate(AValTo).FormatString("mm");
      FToMonthFull = StrToDate(AValTo).FormatString("mmmm");
      FToYear      = StrToDate(AValTo).FormatString("yyyy");
    }


   int FPerType;
   if (APerType.ToIntDef(-1) == -1)
    FPerType = DocPeriodInt(APerType);
   else
    FPerType = APerType.ToInt();
   if (FPerType == 0)
    {//"�����������"
      return "";
    }
   else if (FPerType == 1)
    {//"����������"
      if (AValFrom.Length() && AValTo.Length())
       {
         RC =  FMT6(dsDocExtFuncDocPeriodValStrUniq1,
                      FFrDay,FFrMonth,FFrYear,FToDay,FToMonth,FToYear);
       }
      else if (AValFrom.Length())
       RC =  FMT3(dsDocExtFuncDocPeriodValStrUniq2,
                    FFrDay,FFrMonth,FFrYear);
      else if (AValTo.Length())
       RC =  FMT3(dsDocExtFuncDocPeriodValStrUniq3,
                    FToDay,FToMonth,FToYear);
    }
   else if (FPerType == 2)
    {//"�����"
      RC =  FMT2(dsDocExtFuncDocPeriodValStrMonth, FFrMonthFull,FFrYear);
    }
   else if (FPerType == 3)
    {//"�������"
      int quarter = StrToDate(AValFrom).FormatString("mm").ToInt();
      if ((quarter >=1)&&(quarter<=3))
       RC =  FMT1(dsDocExtFuncDocPeriodValStrQuart1, FFrYear);
      else if ((quarter >=4)&&(quarter<=6))
       RC =  FMT1(dsDocExtFuncDocPeriodValStrQuart2, FFrYear);
      else if ((quarter >=7)&&(quarter<=9))
       RC =  FMT1(dsDocExtFuncDocPeriodValStrQuart3, FFrYear);
      else if ((quarter >=10)&&(quarter<=12))
       RC =  FMT1(dsDocExtFuncDocPeriodValStrQuart4, FFrYear);
    }
   else if (FPerType == 4)
    {//"���������"
      int quarter = StrToDate(AValFrom).FormatString("mm").ToInt();
      if ((quarter >=1)&&(quarter<=6))
       RC =  FMT1(dsDocExtFuncDocPeriodValStrHalfYear1, FFrYear);
      else if ((quarter >=7)&&(quarter<=12))
       RC =  FMT1(dsDocExtFuncDocPeriodValStrHalfYear2, FFrYear);
    }
   else if (FPerType == 5)
    {//"���"
      RC =  FMT1(dsDocExtFuncDocPeriodValStrYear, FFrYear);
    }
   else
    RC = "";
   return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetGUI(TTagNode *ANode)
{
  return ANode->GetRoot()->GetChildByName("passport")->AV["gui"];
}
//---------------------------------------------------------------------------
TDateTime __fastcall TSToDate(UnicodeString ATS)
{//�������������� timestamp to datetime
   try
    {
      if (ATS != "")  return StrToDate(ATS.SubString(7,2)+"."+ATS.SubString(5,2)+"."+ATS.SubString(1,4));
      else            return NULL;
    }
   catch (...)
    {
      return NULL;
    }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall icsDocExCorrectCount(int ACount)
{
 UnicodeString RC;
 RC = IntToStr(ACount);
 int xpCount = StrToInt(RC[RC.Length()]);
 RC = IntToStr(ACount)+" ";
 if ((ACount == 0)||((ACount > 4)&&(ACount < 21))||((ACount > 20)&&(xpCount > 4)))
  RC += FMT(dsDocExtFuncRecCount);
 else
  {
    if (xpCount == 1)                    RC += FMT(dsDocExtFuncRecCount1);
    else if (xpCount > 1 && xpCount < 5) RC += FMT(dsDocExtFuncRecCount234);
    else                                 RC += FMT(dsDocExtFuncRecCount1);
  }
 return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall GetTemplateFields(TTagNode *ADescription)
{
  TTagNode *FTmpl = ADescription->GetChildByName("maskfields");
  UnicodeString tmpl = "";
  if (FTmpl)
   {
     FTmpl = FTmpl->GetFirstChild();
     if (FTmpl->CmpName("field"))
      { // old template set
        TTagNode *tmp = new TTagNode(NULL);
        tmp->Name = "root";
        tmp->AV["ref"] = "";
        while (FTmpl)
         {
           if (FTmpl->CmpName("field"))
            tmp->AddChild("fl","ref="+FTmpl->AV["ref"]);
           else
            tmp->AddChild("separator");
           FTmpl = FTmpl->GetNext();
         }
        tmpl = tmp->AsXML;
        delete tmp;
      }
     else
      tmpl = FTmpl->AsXML;
   }
  return tmpl;
}
//---------------------------------------------------------------------------

