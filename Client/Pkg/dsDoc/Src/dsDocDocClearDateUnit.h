//---------------------------------------------------------------------------

#ifndef dsDocDocClearDateUnitH
#define dsDocDocClearDateUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxCalendar.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDateUtils.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "dxCore.hpp"
//---------------------------------------------------------------------------
/*
#include "cxGraphics.hpp"
#include "cxLookAndFeels.hpp"
#include <Vcl.ComCtrls.hpp>
#include "cxClasses.hpp"
//---------------------------------------------------------------------------
*/
class TdsDocDocClearDateForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TPanel *Panel1;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TcxDateEdit *DateToDE;
private:	// User declarations
        TDate __fastcall FGetDate();
public:		// User declarations
        __fastcall TdsDocDocClearDateForm(TComponent* Owner);
        __property TDate ToDate = {read=FGetDate};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocDocClearDateForm *dsDocDocClearDateForm;
//---------------------------------------------------------------------------
#endif
