//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsDocDocCreateTypeUnit.h"
#include "dsDocClientConstDef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxRadioGroup"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGroupBox"
#pragma resource "*.dfm"
TdsDocDocCreateTypeForm *dsDocDocCreateTypeForm;
//---------------------------------------------------------------------------
__fastcall TdsDocDocCreateTypeForm::TdsDocDocCreateTypeForm(TComponent* Owner)
        : TForm(Owner)
{
//  Caption = FMT(dsDocExDocCreateTypeCaption);
//  QuestLab->Caption = FMT(dsDocExDocCreateTypeQuestLabLabCaption);
//  OkBtn->Caption = FMT(dsDocExDocCreateTypeOkBtnBtnCaption);
//  CancelBtn->Caption = FMT(dsDocExDocCreateTypeCancelBtnBtnCaption);
//  DocRB->Caption = FMT(dsDocExDocCreateTypeDocRBRBCaption);
//  BaseRB->Caption = FMT(dsDocExDocCreateTypeBaseRBRBCaption);
  CreateType = 1;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocDocCreateTypeForm::OkBtnClick(TObject *Sender)
{
  if (CreateTypeRG->ItemIndex == 0)
   CreateType = 1;
  else if (CreateTypeRG->ItemIndex == 1)
   CreateType = 0;
  else
   CreateType = 2;
}
//---------------------------------------------------------------------------

