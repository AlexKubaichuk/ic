// ---------------------------------------------------------------------------
#include <vcl.h>
#include <System.hpp>
// #include <stdio.h>
// #include <clipbrd.hpp>
#pragma hdrstop
#include "dsDocDMUnit.h"
#include "dsDocDocPeriodUnit.h"
#include "dsDocPackPeriodUnit.h"
#include "dsDocClientConstDef.h"
#include "dsDocDocCreateTypeUnit.h"
#include "dsDocExtFunc.h"
#include "FilterEditEx.h"
#include "dsDocDistributionList.h"
#include "msgdef.h"
#include "Winapi.TlHelp32.hpp"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TdsDocDM * dsDocDM;
// ---------------------------------------------------------------------------
__fastcall TdsDocDM::TdsDocDM(TComponent * Owner) : TDataModule(Owner)
 {
  FUseKLADRAddr = false;
  FSpecEnabled  = true;
  FSpecEditable = true;
  // FSpecEditable = true;
  FDocEnabled       = true;
  FDocEditable      = true;
  FDocHandEditable  = true;
  FFilterEnabled    = true;
  FFilterEditable   = true;
  FDefDocCreateType = dctBaseDoc;
  FMainOwnerCode    = "812.1107704.000";
  FSubDir           = "";
  // FICS_DATA_DEF = NULL;
  CurDocCreateMonth  = -1;
  FNom               = NULL;
  FDocNom            = NULL;
  FNmkl              = NULL;
  FDocNmkl           = NULL;
  FClassDefNode      = NULL;
  FRegDefNode        = new TTagNode;
  FClassEditable     = true;
  FFullEdit          = true;
  FClassShowExpanded = "";
  // FRequiredColor = clInfoBk;
  FULEditWidth = 700;
  FReqColor    = clInfoBk;
  FXMLPath     = icsPrePath(GetEnvironmentVariable("APPDATA") + "\\ic\\xml");
  StyleDef     = "<style>   \
              table {border: 1px solid black;}\
              th {border-right: 1px solid black; }\
              td {border-top: 1px solid black; border-right: 1px solid black;}\
              h2 {text-align: center;}\
              </style>";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::DataModuleCreate(TObject * Sender)
 {
  UnitList   = NULL;
  SelRegUnit = NULL;
  // DTFormats[0] = FMT(icsDTFormatsD);
  // DTFormats[1] = FMT(icsDTFormatsDT);
  // DTFormats[2] = FMT(icsDTFormatsT);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::DataModuleDestroy(TObject * Sender)
 {
  // delete FRegDefNode;
  // if (FICS_DATA_DEF) delete FICS_DATA_DEF;
  if (FNom)
   delete FNom;
  if (FDocNom)
   delete FDocNom;
  // if (FNmkl) delete FNmkl;
  // if (FDocNmkl) delete FDocNmkl;
  /*
   delete FClassData;
   if (SelRegUnit)   delete SelRegUnit;
   SelRegUnit = NULL;
   if (REG_DB->Connected) REG_DB->Close();
   */
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsDocDM::FGetICS_DATA_DEF()
 {
  return FXMLList->GetXML("ICS_DATA_DEF");
 }
// ---------------------------------------------------------------------------
// #include <rpc.h>
UnicodeString __fastcall TdsDocDM::NewGUID()
 {
  return icsNewGUID().UpperCase();
 }
// ---------------------------------------------------------------------------
// TTagNode* __fastcall TdsRegDM::FGetRegDefNode()
// {
// TTagNode *RC = FRegDefNode;
// return RC;
// }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::FSetMainOwnerCode(UnicodeString AVal)
 {
  FMainOwnerCode = AVal.UpperCase();
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsDocDM::FGetClassDef()
 {
  TTagNode * RC = FClassDefNode;
  try
   {
    if (!RC)
     {
      FClassDefNode = RegDef->GetChildByName("classes");
      RC            = FClassDefNode;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::OnGetXML(UnicodeString AId, TTagNode *& ARC)
 {
  try
   {
    if (!FXMLList->XMLExist(AId))
     {
      TTagNode * tmp = new TTagNode;
      try
       {
        UnicodeString FFN = icsPrePath(FXMLPath + "\\" + AId + ".xml");
        if (!FileExists(FFN))
         {
          ForceDirectories(FXMLPath);
          tmp->AsXML = kabCDSGetClassXML(AId);
          tmp->SaveToXMLFile(FFN, "");
         }
        FXMLList->Load(FFN, AId);
       }
      __finally
       {
        delete tmp;
       }
     }
    ARC = FXMLList->GetXML(AId);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::GetSpec(UnicodeString AGUI, TTagNode * ASpec, int & ASpecType, UnicodeString AMsg)
 {
  bool RC = false;
  try
   {
    ASpecType = -1;
    if (FOnGetSpec)
     {
      UnicodeString tmpRC = "";
      RC           = FOnGetSpec(AGUI, ASpecType, tmpRC);
      ASpec->AsXML = tmpRC;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::GetSpecTxt(UnicodeString AGUI)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetSpec)
     {
      int FSpecType = 0;
      FOnGetSpec(AGUI, FSpecType, RC);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::CheckPackage(TICSCheckPackItem ACheckItemEvt, TTagNode * APackDef)
 {
  TDocCreateType FCreateType;
  try
   {
    Application->ProcessMessages();
    FCreateType = dctBaseDoc;
    bool FPeriodisity = false;
    TTagNode * itSpec = APackDef->GetChildByName("doccontent", true)->GetFirstChild();
    while (itSpec)
     {
      FPeriodisity |= itSpec->AV["type"].ToIntDef(0);
      itSpec = itSpec->GetNext();
     }
    if (FCreateType != dctNone)
     { // ��������� �����
      TDate FPeriodDateFr = 0;
      TDate FPeriodDateTo = 0;
      short int FPerType = 0;
      if (FPeriodisity)
       {
        if (CurDocCreateMonth != -1)
         {
          FPeriodDateFr = GetFirstMonthDate(CurDocCreateMonth, CurDocCreateYear);
          FPeriodDateTo = GetLastMonthDate(CurDocCreateMonth, CurDocCreateYear);
          FPerType      = 2;
         }
        else
         {
          FPerType = 0;
          TdsDocPeriodForm * Dlg = new TdsDocPeriodForm(Application->Owner, ReqColor, 1);
          try
           {
            Dlg->ShowModal();
            if (Dlg->ModalResult == mrOk)
             {
              FPerType      = Dlg->pdType;
              FPeriodDateFr = Dlg->DateFr;
              FPeriodDateTo = Dlg->DateTo;
             }
            else
             FCreateType = dctNone;
           }
          __finally
           {
            delete Dlg;
           }
         }
       }
      if (FCreateType != dctNone)
       {
        itSpec = APackDef->GetChildByName("doccontent", true)->GetFirstChild();
        int FSpecPerType;
        int FDocPerType;
        TDate FDocPeriodDateFr, FDocPeriodDateTo;
        while (itSpec)
         { // ���� �� ���������
          try
           {
            try
             {
              if (FPeriodisity)
               { // ������������� ����, ���������� � ���
                FSpecPerType = itSpec->AV["type"].ToIntDef(0);
                if (FSpecPerType)
                 {
                  FDocPeriodDateFr = FPeriodDateFr;
                  FDocPeriodDateTo = FPeriodDateTo;
                  FDocPerType      = GetPeriodValue(FSpecPerType, itSpec->AV["stype"].ToIntDef(0),
                    ((FSpecPerType == 6) ? UnicodeString("") : itSpec->AV["dfrom"]).c_str(),
                    ((FSpecPerType == 6) ? itSpec->AV["dfrom"] : UnicodeString("")).c_str(), itSpec->AV["dto"].c_str(),
                    itSpec->AV["offset"].c_str(), FPerType, FDocPeriodDateFr, FDocPeriodDateTo);
                 }
               }
              ACheckItemEvt(itSpec->AV["name"], itSpec->AV["spec_gui"], FDocPerType, FDocPeriodDateFr,
              FDocPeriodDateTo);
             }
            __finally
             {
             }
           }
          catch (Exception & E)
           {
            _MSG_ERRA((FMT(dsDocCommonErrorCreateDocCaption) + "\n" + E.Message).c_str(),
              cFMT(dsDocCommonErrorCaption));
           }
          catch (...)
           {
            _MSG_ERRA(FMT(dsDocCommonErrorCreateDocCaption), cFMT(dsDocCommonErrorCaption));
           }
          itSpec = itSpec->GetNext();
         }
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
TDate __fastcall TdsDocDM::GetLastMonthDate(int AMons, int AYear)
 {
  if (IsValidDate((Word)AYear, (Word)AMons, (Word)1))
   { // ��� � ����� ����������
    return TDate(IntToStr(DaysInAMonth((Word)AYear, (Word)AMons)) + ((AMons < 10) ? ".0" : ".") + IntToStr(AMons) +
      "." + IntToStr(AYear));
   }
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
TDate __fastcall TdsDocDM::GetFirstMonthDate(int AMons, int AYear)
 {
  if (IsValidDate((Word)AYear, (Word)AMons, (Word)1))
   { // ��� � ����� ����������
    return TDate(((AMons < 10) ? "01.0" : "01.") + IntToStr(AMons) + "." + IntToStr(AYear));
   }
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::HTMLPreview(UnicodeString AHTML)
 {
  TPreviewForm * Dlg = new TPreviewForm(this);
  TStringList * FDoc = new TStringList;
  try
   {
    FDoc->Text = AHTML;
    TPreviewParam param = TPreviewParam(10, 10, 10, 10, 1, poPortrait, pzWidth);
    Dlg->Preview(FDoc, param);
   }
  __finally
   {
    delete Dlg;
    delete FDoc;
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::GetThisSpecOwner()
 {
  return FMainOwnerCode;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::CreatePackage(TTagNode * ASpec, UnicodeString AFilter, bool ACheckDefDate)
 {
  // DocCreateType - ctBase    - ������������ ��������� �� ��
  // - ctDoc     - ������������ ��������� �� ���������
  // - dctBaseDoc - ������������ ��������� �� �� � ���������
  bool RC = true; /*
   bool FCanCreateFlt = false;
   TTagNode *FFilter = NULL;
   TDocCreateType FCreateType;
   TDocCreateType FPackCreateType;
   TDocCreateProgressForm *ProgressDlg = NULL;
   TTagNode *FGuardSpec = NULL;
   char *grdBuf = NULL;
   try
   {
   ProgressDlg = new TDocCreateProgressForm(Application->Owner);
   ProgressDlg->Show();
   Application->ProcessMessages();
   ProgressDlg->SetProgress(100, FMT(dsDocDocCreatorSpecPrepare), ptInit);
   FGuardSpec = new TTagNode(NULL);

   FGuardSpec->AsXML = ASpec->AsXML;
   ProgressDlg->SetProgress(50, "", ptInc);

   FCreateType = dctBaseDoc;

   bool FDocOnly = true;
   bool FBaseOnly = true;
   bool FPeriodisity = false;
   TTagNode *itSpec = ASpec->GetChildByName("doccontent",true)->GetFirstChild();
   int FCTType;
   while(itSpec)
   {
   // 1  - ��������� � ����� ������������ ������
   // 2  - �� ����
   // 3  - �� ����������

   FPeriodisity |= itSpec->AV["type"].ToIntDef(0);
   FCTType = itSpec->AV["createtype"].ToIntDef(0);
   FBaseOnly &= (FCTType == 2);
   FDocOnly  &= (FCTType == 3);
   itSpec = itSpec->GetNext();
   }
   FPackCreateType = ctNone;
   if (FBaseOnly)                    FPackCreateType = ctBase;
   else if (FDocOnly)                FPackCreateType = ctDoc;
   else if (!FBaseOnly && !FDocOnly) FPackCreateType = dctBaseDoc;

   if (FDefDocCreateType == ctDoc)
   {
   if ((FPackCreateType == ctDoc) || (FPackCreateType == dctBaseDoc))
   FCreateType = ctDoc;
   else
   FCreateType = ctEmpty;
   }
   else if (FDefDocCreateType == ctBase)
   {
   if ((FPackCreateType == ctBase) || (FPackCreateType == dctBaseDoc))
   FCreateType = ctBase;
   else
   FCreateType = ctEmpty;
   }
   else if (FDefDocCreateType == dctBaseDoc)
   {
   if (FPackCreateType == ctDoc)
   FCreateType = ctDoc;
   else if (FPackCreateType == ctBase)
   FCreateType = ctBase;
   else if (FPackCreateType == dctBaseDoc)
   {
   TDocCreateTypeForm *Dlg = NULL;
   try
   {
   Dlg = new TDocCreateTypeForm(Application->Owner);
   Dlg->ShowModal();
   if (Dlg->ModalResult == mrOk)
   {
   if (Dlg->CreateType) FCreateType = ctBase;
   else                 FCreateType = ctDoc;
   }
   else
   {
   FCreateType = ctNone;
   RC = false;
   }
   }
   __finally
   {
   if (Dlg) delete Dlg;
   }
   }
   else
   FCreateType = ctNone;
   }
   else
   {
   FCreateType = ctNone;
   RC = false;
   }
   if (FCreateType != ctNone)
   { // ��������� �����
   TDate FPeriodDateFr = 0;
   TDate FPeriodDateTo = 0;
   short int FPerType = 0;
   if (FPeriodisity)
   {
   if ((int)ADefDateFr || (int)ADefDateTo || ADefPerType)
   {
   FPeriodDateFr = ADefDateFr;
   FPeriodDateTo = ADefDateTo;
   FPerType = ADefPerType;
   }
   else
   {
   TDocPeriodForm *Dlg = NULL;
   try
   {
   Dlg = new TDocPeriodForm(Application->Owner, ReqColor);
   FPerType = 0;
   Dlg->ShowModal();
   if (Dlg->ModalResult == mrOk)
   {
   FPerType = Dlg->pdType;
   FPeriodDateFr = Dlg->DateFr;
   FPeriodDateTo = Dlg->DateTo;
   }
   else
   FCreateType = ctNone;
   }
   __finally
   {
   if (Dlg) delete Dlg;
   }
   }
   }
   if (FCreateType != ctNone)
   {
   itSpec = ASpec->GetChildByName("doccontent",true)->GetFirstChild();
   TTagNode *FSpec;
   TTagNode *FDoc;
   int FSpecPerType;
   int FDocPerType;
   int FSpecType;
   TDate FDocPeriodDateFr, FDocPeriodDateTo;
   while(itSpec)
   { // ���� �� ���������
   try
   {
   FSpec = new TTagNode(NULL);
   FDoc = new TTagNode(NULL);
   try
   {
   if (GetSpec(itSpec->AV["spec_gui"], FSpec, FSpecType, "������ �������� �������� ���������."))
   {
   if (FPeriodisity)
   { // ������������� ����, ���������� � ���
   FSpecPerType = itSpec->AV["type"].ToIntDef(0);
   if (FSpecPerType)
   {
   FDocPeriodDateFr = FPeriodDateFr;
   FDocPeriodDateTo = FPeriodDateTo;
   FDocPerType =  GetPeriodValue(FSpecPerType, itSpec->AV["stype"].ToIntDef(0), ((FSpecPerType == 6)? UnicodeString(""):itSpec->AV["dfrom"]).c_str(),
   ((FSpecPerType == 6)? itSpec->AV["dfrom"]:UnicodeString("")).c_str(), itSpec->AV["dto"].c_str(),
   itSpec->AV["offset"].c_str(),
   FPerType, FDocPeriodDateFr, FDocPeriodDateTo);

   FCTType = itSpec->AV["createtype"].ToIntDef(0);

   if (FCTType == 2)       FCreateType = ctBase;
   else if (FCTType == 3)  FCreateType = ctDoc;

   InternalCreateDocument(itSpec->AV["spec_gui"], 1, AFilterCode, (FSpecType == 11), "", FCreateType, true);
   }
   else
   {
   FCTType = itSpec->GetChildByName("passport",true)->AV["createtype"].ToIntDef(0);

   if (FCTType == 2)       FCreateType = ctBase;
   else if (FCTType == 3)  FCreateType = ctDoc;

   InternalCreateDocument(itSpec->AV["spec_gui"], 1, AFilterCode, (FSpecType == 11), "", FCreateType, false); //!!! , 0, 0, 0
   }
   }
   else
   { // ������������� ��� � ��� :)
   FCTType = itSpec->AV["createtype"].ToIntDef(0);

   if (FCTType == 2)       FCreateType = ctBase;
   else if (FCTType == 3)  FCreateType = ctDoc;

   InternalCreateDocument(itSpec->AV["spec_gui"], 1, AFilterCode, (FSpecType == 11), "", FCreateType, false); //!!! , 0, 0, 0,
   }


   ///                        if (GetPassport(FSpec)->CmpAV("save_bd","1"))
   //                         {// ���������� ������� ���������� � ��
   //                           SaveDocument(FDoc, FSpec, false, false, 1); //!!! ��������� ���������������
   //                         }

   }
   }
   __finally
   {
   delete FSpec;
   delete FDoc;
   }
   }
   catch (Exception &E)
   {
   _MSG_ERRA((FMT(dsDocCommonErrorCreateDocCaption)+"\n"+E.Message).c_str(),cFMT(dsDocCommonErrorCaption));
   FCreateType = ctNone;
   RC = false;
   }
   catch (...)
   {
   _MSG_ERRA(FMT(dsDocCommonErrorCreateDocCaption),cFMT(dsDocCommonErrorCaption));
   FCreateType = ctNone;
   RC = false;
   }
   itSpec = itSpec->GetNext();
   }
   }
   }
   }
   __finally
   {
   if (grdBuf) delete[] grdBuf;
   if (ProgressDlg)
   {
   ProgressDlg->Close();
   delete ProgressDlg;
   }
   if (FFilter && FCanCreateFlt) delete FFilter;
   if (FGuardSpec)
   {
   delete FGuardSpec;
   }
   } */
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::DocCreateCheckFilter(UnicodeString ASpecGUI, UnicodeString AFilter,
  UnicodeString & AFilterName, UnicodeString & AFilterGUI)
 {
  UnicodeString RC = "error";
  try
   {
    if (FOnDocCreateCheckFilter)
     RC = FOnDocCreateCheckFilter(ASpecGUI, AFilter, AFilterName, AFilterGUI);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::StartCreateDocument(TTagNode * ASpec, int ACreateType, UnicodeString AFilter,
  bool ADocNom, UnicodeString AAuthor, int ADefCreateType, bool ACheckDefDate, bool ANoSave)
 {
  UnicodeString RC = "error";
  try
   {
    UnicodeString FFltName, FFltGUI, FSpecGUI;
    FSpecGUI = GetSpecGUI(ASpec);
    UnicodeString FFltRC = DocCreateCheckFilter(FSpecGUI, AFilter, FFltName, FFltGUI);
    bool ChRC = false;
    bool FConcr = false;
    if (SameText(FFltRC, "error"))
     { // ������ ������.
     }
    else if (SameText(FFltRC, "ok"))
     { // ������ �� ������� ������������� � ����������� �� ������ ������� �� �����
      ChRC   = true;
      FFltRC = AFilter;
     }
    else
     { // ������ ������� �������������.
      TFilterEditExForm * Dlg = new TFilterEditExForm(Application->Owner);
      TTagNode * __tmpFlt = new TTagNode;
      try
       {
        __tmpFlt->AsXML    = FFltRC;
        Dlg->OnCallDefProc = FOnCallDefProc;
        Dlg->EditType      = fetKonkr;
        Dlg->XMLContainer  = FXMLList;
        Dlg->Nom           = Nom;
        Dlg->Filter        = __tmpFlt; // edit
        Dlg->ShowModal();
        if (Dlg->ModalResult == mrOk)
         {
          if (Dlg->Filter->CmpName("region"))
           {
            if (Dlg->Filter->GetChildByName("condrules"))
             {
              FFltRC = Dlg->Filter->GetChildByName("condrules")->AsXML;
              ChRC   = true;
              FConcr = true;
             }
           }
          else if (Dlg->Filter->CmpName("condrules"))
           {
            FFltRC = Dlg->Filter->AsXML;
            ChRC   = true;
            FConcr = true;
           }
         }
       }
      __finally
       {
        delete Dlg;
        delete __tmpFlt;
       }
     }
    if (ChRC && FOnCheckDocCreate && FOnCheckExistDocCreate && FOnStartDocCreate)
     {
      TDate FFDate = 0;
      TDate FLDate = 0;
      int FDType = 0;
      if (CurDocCreateMonth != -1)
       {
        FFDate = GetFirstMonthDate(CurDocCreateMonth, CurDocCreateYear);
        FLDate = GetLastMonthDate(CurDocCreateMonth, CurDocCreateYear);
        FDType = 2;
       }
      TDocCreateType FCreateType = dctNone;
      bool FCont;
      int FGetCreateType, FGetPeriod, FGetFilterData;
      int FPerType = 0;
      TDate FPeriodDateFr, FPeriodDateTo;
      FCont = false;
      if (FOnCheckDocCreate(FSpecGUI, ACreateType, ADefCreateType, FGetCreateType, FGetPeriod))
       {
        FCreateType = (TDocCreateType)ACreateType;
        if (FGetCreateType)
         {
          TdsDocDocCreateTypeForm * Dlg = new TdsDocDocCreateTypeForm(this);
          try
           {
            Dlg->ShowModal();
            if (Dlg->ModalResult == mrOk)
             {
              switch (Dlg->CreateType)
               {
               case 0:
                 {
                  FCreateType = dctDoc;
                  break;
                 }
               case 1:
                 {
                  FCreateType = dctBase;
                  break;
                 }
               case 2:
                 {
                  FCreateType = dctEmpty;
                  break;
                 }
               }
              FCont = true;
             }
           }
          __finally
           {
            delete Dlg;
           }
         }
        else
         FCont = true;
        if (FCont)
         {
          FCont = false;
          if (FGetPeriod)
           {
            if (FDType)
             {
              FPerType      = FDType;
              FPeriodDateFr = FFDate;
              FPeriodDateTo = FLDate;
              FCont         = true;
             }
            else
             {
              TdsDocPeriodForm * Dlg = new TdsDocPeriodForm(this, ReqColor, GetIntSpecPeriod(ASpec));
              try
               {
                FPerType = 0;
                Dlg->ShowModal();
                if (Dlg->ModalResult == mrOk)
                 {
                  FPerType      = Dlg->pdType;
                  FPeriodDateFr = Dlg->DateFr;
                  FPeriodDateTo = Dlg->DateTo;
                  FCont         = true;
                 }
               }
              __finally
               {
                delete Dlg;
               }
             }
           }
          else
           FCont = true;
         }
        if (FCont)
         {
          FCont = false;
          if (FOnCheckExistDocCreate(FSpecGUI, AAuthor, FPerType, FPeriodDateFr, FPeriodDateTo, RC))
           {
            int DlgRC = _MSG_QUEC(FMT(dsDocDocCreatorDocLoadConf), FMT(dsDocDocCreatorDocLoadConfCaption));
            if (DlgRC == ID_NO)
             FCont = true;
            else if (DlgRC == ID_CANCEL)
             RC = "cancel";
           }
          else
           FCont = true;
         }
        if (FCont)
         {
          FCont = false;
          if (FOnStartDocCreate(FSpecGUI, AAuthor, FFltRC, FFltName + ":" + FFltGUI, FConcr, (int)FCreateType, FPerType,
            FPeriodDateFr, FPeriodDateTo, ANoSave))
           RC = "ok";
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::CreateDocumentProgress(UnicodeString & ADocGUI)
 {
  UnicodeString RC = "� � � � � �";
  try
   {
    if (FOnDocCreateProgress)
     RC = FOnDocCreateProgress(ADocGUI);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::InternalCreateDocument(TkabProgress AProgressProc, TTagNode * ASpec, int ACreateType,
  UnicodeString AFilter, bool ADocNom, UnicodeString AAuthor, int ADefCreateType, bool ACheckDefDate, bool ANoSave)
 {
  UnicodeString RC = "";
  TTagNode * PRC = new TTagNode;
  if (!AProgressProc)
   throw Exception("����������� ������� ��������� ��������� ������������.");
  try
   {
    AProgressProc(TkabProgressType::InitCommon, 1000, "");
    UnicodeString StartRC = StartCreateDocument(ASpec, ACreateType, AFilter, ADocNom, AAuthor, ADefCreateType,
      ACheckDefDate, ANoSave);
    if (SameText(StartRC, "ok"))
     {
      while (!RC.Length())
       {
        StartRC = CreateDocumentProgress(RC);
        if (SameText(StartRC, "ok"))
         {
          if (!RC.Length())
           RC = "error";
         }
        else if (SameText(StartRC, "error"))
         {
          RC = "error";
         }
        else
         {
          try
           {
            PRC->AsXML = StartRC;
            AProgressProc(TkabProgressType::StageInc, 0, PRC->AV["SMsg"]);
            AProgressProc(TkabProgressType::CommInc, (int)(PRC->AV["CP"].ToDouble() * 10.0), PRC->AV["CSMsg"]);
           }
          catch (Exception & E)
           {
           }
          Application->ProcessMessages();
         }
        Sleep(300);
       }
     }
    else if (!SameText(StartRC, "error"))
     RC = StartRC;
   }
  __finally
   {
    delete PRC;
    AProgressProc(TkabProgressType::CommComplite, 0, "");
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::GetTempPath() const
 {
  wchar_t szTempPath[MAX_PATH];
  szTempPath[0] = '\0';
  ::GetTempPath(MAX_PATH, szTempPath);
  return static_cast<UnicodeString>(szTempPath);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall GetAPILastErrorFormatMessage()
 {
  UnicodeString ErrMsg;
  LPVOID lpMsgBuf;
  int nErrorCode = GetLastError();
  FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL,
    nErrorCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
    (LPTSTR) & lpMsgBuf, 0, NULL);
  ErrMsg = static_cast<LPCTSTR>(lpMsgBuf);
  LocalFree(lpMsgBuf);
  return ErrMsg;
 }
// ---------------------------------------------------------------------------
HANDLE GetProcessHandle(wchar_t * szExeName)
 {
  Winapi::Tlhelp32::PROCESSENTRY32 Pc =
   {
    sizeof(Winapi::Tlhelp32::PROCESSENTRY32)
   };
  HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);
  if (Process32First(hSnapshot, &Pc))
   {
    do
     {
      if (!wcscmp(Pc.szExeFile, szExeName))
       {
        return OpenProcess(PROCESS_ALL_ACCESS, TRUE, Pc.th32ProcessID);
       }
     }
    while (Process32Next(hSnapshot, &Pc));
   }
  return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::PreviewDocument(UnicodeString AGUI, TkabProgress AProgressProc)
 {
  TPreviewForm * Dlg = new TPreviewForm(this);
  // TStringList * FDoc = new TStringList;
  try
   {
    UnicodeString DRC;
    if (FOnGetDocPreview && FOnLoadDocPreview)
     {
      int Fl, Ft, Fr, Fb, Fo;
      Fl = Ft = Fr = Fb = 10;
      double Fps = 1;
      Fo = (int)poPortrait;
      if (FOnGetDocPreview(AGUI, DRC, Fl, Ft, Fr, Fb, Fps, Fo))
       {
        if (SameText(DRC, "ok"))
         {
          UnicodeString FDocData = "";
          bool FCont = true;
          int ContLen = 0;
          UnicodeString HTMLTmpFileName =
            icsPrePath(GetTempPath() + "\\d" + AGUI + TDateTime::CurrentDateTime().FormatString("yymmddhhnnss") +
            ".htm");
          TFileStream * fs = new TFileStream(HTMLTmpFileName, fmCreate | fmShareExclusive);
          try
           {
            TByteDynArray FByteBuf = TByteDynArray();
            FByteBuf = (TEncoding::UTF8)->GetPreamble();
            fs->WriteBuffer(FByteBuf, FByteBuf.Length);
            int commCount = 0;
            int curPos = 0;
            while (FCont)
             {
              FDocData = FOnLoadDocPreview(DRC);
              FByteBuf = (TEncoding::UTF8)->GetBytes(FDocData);
              fs->WriteBuffer(FByteBuf, FByteBuf.Length);
              ContLen += FByteBuf.Length;
              FCont = false;
              if (SameText(GetLPartB(DRC, ':'), "progress"))
               {
                // progress:" + Position + "/" + Size;
                if (!commCount)
                 {
                  commCount = GetRPartE(DRC, '/').ToIntDef(0);
                  if (AProgressProc)
                   AProgressProc(TkabProgressType::InitCommon, 100, "�������� ���������");
                 }
                else
                 {
                  curPos = GetLPartE(GetRPartB(DRC, ':'), '/').ToIntDef(0) * 100;
                  if (AProgressProc)
                   AProgressProc(TkabProgressType::CommInc, curPos / commCount, "");
                 }
                FCont = true;
               }
              Application->ProcessMessages();
             }
           }
          __finally
           {
            delete fs;
           }
          if (SameText(DRC, "ok"))
           {
            if (ContLen)
             {
              // FDoc->Text = FDocData;
              // FDoc->SaveToFile("c:\\rep.htm");
              // fs->Seek(0, soFromBeginning);
              if (FExternalView.Length() && FileExists(FExternalView))
               {
                try
                 {
                  int x;
                  HANDLE kill = GetProcessHandle(L"DocPreview.exe");
                  DWORD fdwExit = 0;
                  GetExitCodeProcess(kill, & fdwExit);
                  TerminateProcess(kill, fdwExit);
                  x = CloseHandle(kill);
                  STARTUPINFO si;
                  PROCESS_INFORMATION pi;
                  ZeroMemory(& si, sizeof(si));
                  si.cb = sizeof(si);
                  ZeroMemory(& pi, sizeof(pi));
                  // Start the child process.
                  UnicodeString cmd = "\"" + FExternalView + "\"";
                  cmd += " \"" + HTMLTmpFileName + "\"";
                  cmd += " " + IntToStr(Fl);
                  cmd += " " + IntToStr(Ft);
                  cmd += " " + IntToStr(Fr);
                  cmd += " " + IntToStr(Fb);
                  cmd += " " + FloatToStr(Fps);
                  cmd += " " + IntToStr(Fo);
                  cmd += " " + IntToStr(pzWidth);
                  if (!CreateProcess(NULL,
                    // No module name (use command line)
                    cmd.c_str(), // Command line
                    NULL, // Process handle not inheritable
                    NULL, // Thread handle not inheritable
                    false, // Set handle inheritance to FALSE
                    NORMAL_PRIORITY_CLASS, // No creation flags
                    NULL, // Use parent's environment block
                    NULL, // Use parent's starting directory
                    &si, // Pointer to STARTUPINFO structure
                    &pi) // Pointer to PROCESS_INFORMATION structure
                    )
                   {
                    _MSG_ERRA(L"������ �������� ��������� ��������� ���������.\n��������� ���������:\n" +
                      ICUtils::GetAPILastErrorFormatMessage() + L".", L"������");
                   }
                 }
                catch (System::Sysutils::Exception & E)
                 {
                  _MSG_ERRA(E.Message, "������ 21");
                 }
               }
              else
               {
                TPreviewParam param = TPreviewParam(Fl, Ft, Fr, Fb, Fps, (Printers::TPrinterOrientation)Fo, pzWidth);
                Dlg->Preview(HTMLTmpFileName, param);
               }
             }
            else if (ContLen && SameText(DRC, "ok"))
             _MSG_ERRA("�������� ������.", "������");
           }
          else if (SameText(DRC, "error"))
           _MSG_ERRA("������ �������� ���������������� ���������.", "������");
         }
       }
      else
       _MSG_ERRA(DRC, "������ ������������ ���������������� ���������");
     }
    else
     _MSG_ERRA("�� ����� ���������� OnGetDoc", "������");
   }
  __finally
   {
    delete Dlg;
    // delete FDoc;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::SaveDocument(UnicodeString ADocGUI)
 {
  if (FOnSaveDoc)
   FOnSaveDoc(ADocGUI);
  else
   _MSG_ERRA("����������� OnSaveDoc", "������");
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::FillClass(TStrings * AValues, UnicodeString ARef, UnicodeString ASQL)
 {
  bool RC = false;
  try
   {
    TJSONObject * RetData;
    // TJSONObject *tmpPair;
    // TJSONString *tmpData;
    int FClCode;
    UnicodeString FClStr;
    if (ARef.Length())
     RetData = kabCDSGetValById("doc.l." + ARef, "0");
    else
     RetData = kabCDSGetValById("doc.s2." + ASQL, "0");
    TJSONPairEnumerator * itPair = RetData->GetEnumerator();
    AValues->Clear();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value().ToIntDef(0);
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      (TStringList *)AValues->AddObject(FClStr, (TObject *)FClCode);
     }
    RC = true;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::GetOwnerList(TStringList * AChildList)
 {
  // qtExecDoc("Select CODE, NAME, SUBORD From SUBORDORG Order By NAME",false);
  try
   {
    TJSONObject * RetData;
    // TJSONObject *tmpPair;
    // TJSONString *tmpData;
    UnicodeString FClCode, FClStr;
    RetData = kabCDSGetValById("doc.s2.Select CODE as ps1, NAME as ps2 From SUBORDORG Order By NAME", "0");
    TJSONPairEnumerator * itPair = RetData->GetEnumerator();
    AChildList->Clear();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      if (FMainOwnerCode.UpperCase() == FClCode.UpperCase())
       AChildList->Add(FClCode + "=���� �����������");
      else
       AChildList->Add(FClCode + "=" + FClStr);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::GetChildSpecOwnerList(TStringList * AChildList, bool AIsName)
 {
  // qtExecDoc("Select CODE, NAME From SUBORDORG Where SUBORD=0 Order By NAME",false);    //!!!
  try
   {
    TJSONObject * RetData;
    // TJSONObject *tmpPair;
    // TJSONString *tmpData;
    UnicodeString FClCode, FClStr;
    RetData = kabCDSGetValById("doc.s2.Select CODE as ps1, NAME as ps2, SUBORD From SUBORDORG Order By NAME", "0");
    TJSONPairEnumerator * itPair = RetData->GetEnumerator();
    AChildList->Clear();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      if (AIsName)
       AChildList->Add(FClCode + "=" + FClStr);
      else
       AChildList->Add(FClCode);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::GetOwnerName(UnicodeString AOwnerCode, UnicodeString AFieldName)
 {
  UnicodeString RC = "";
  try
   {
    // qtExecDoc("");
    // if (!quFreeDoc->RecordCount) return "";
    // else                         return quFreeDoc->FN(AFieldName)->AsString;
    TJSONObject * RetData;
    // TJSONObject *tmpPair;
    // TJSONString *tmpData;
    // UnicodeString FClCode, FClStr;
    RetData = kabCDSGetValById("doc.s2.Select code as ps1, " + AFieldName +
      " as ps2 From SUBORDORG Where Upper(CODE)='" + AOwnerCode.UpperCase() + "'", "0");
    TJSONPairEnumerator * itPair = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      RC = ((TJSONString *)itPair->Current->JsonValue)->Value();
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::CanIntegrated(TTagNode * ASpec)
 {
  bool RC = false;
  try
   {
    if (ASpec)
     {
      TTagNode * __tmp = ASpec->GetChildByName("doccontent");
      if (__tmp)
       {
        RC = (bool)(__tmp->GetCount(false, "table"));
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::DocViewDemoMessage(TPPreviewDemoMessage AMsgType)
 {
#ifdef _DEMOMODE
  switch (AMsgType)
   {
   case pdmPrint:
     {
      DemoMsg->ShowDemoById("pdmPrint");
      break;
     } // #��������������� ��������. ������. //#* ������ ������� �� �������� � ���������������� ������.
   case pdmSave:
     {
      DemoMsg->ShowDemoById("pdmSave");
      break;
     } // #��������������� ��������. ����������. //#* ������ ������� �� �������� � ���������������� ������.
   case pdmExport:
     {
      DemoMsg->ShowDemoById("pdmExport");
      break;
     } // #��������������� ��������. ������� � Excel. //#* ������ ������� �� �������� � ���������������� ������.
   }
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::GetIntegredSpecList(TStringList * AList)
 {
  // qtExecDoc("Select GUI,NAME From SPECIFIKATORS Where INTEGRATED=1 Order By NAME", false);  //!!!
  try
   {
    TJSONObject * RetData;
    // TJSONObject *tmpPair;
    // TJSONString *tmpData;
    UnicodeString FClCode, FClStr;
    RetData = kabCDSGetValById
      ("doc.s2.Select GUI as ps1, NAME as ps2 From SPECIFIKATORS Where INTEGRATED=1 Order By NAME", "0");
    TJSONPairEnumerator * itPair = RetData->GetEnumerator();
    AList->Clear();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      AList->Add(FClCode + "=" + FClStr);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::GetSpecByGUI(UnicodeString AGUI, TTagNode * ASpec)
 {
  // qtExecDoc("Select SPECTEXT From SPECIFIKATORS Where GUI='"+AGUI+"'", false);
  bool RC = false;
  ASpec->DeleteChild();
  try
   {
    TJSONObject * RetData;
    // TJSONObject *tmpPair;
    // TJSONString *tmpData;
    UnicodeString FClCode, FClStr;
    RetData = kabCDSGetValById("doc.s2.Select GUI as ps1, SPECTEXT as ps2 From SPECIFIKATORS Where Upper(GUI)='" +
      AGUI.UpperCase() + "'", "0");
    TJSONPairEnumerator * itPair = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FClCode      = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr       = ((TJSONString *)itPair->Current->JsonValue)->Value();
      ASpec->AsXML = FClStr;
      RC           = true;
     }
    if (!ASpec->Count)
     {
      // _MSG_ERRA(FMT1(icsDocExExtFuncMissingDocDef,AGUI),FMT(icsDocExCommonErrorCaption));
      RC = false;
     }
   }
  __finally
   {
   }
  /*
   qtExecDoc("Select SPECTEXT From SPECIFIKATORS Where GUI='"+AGUI+"'", false);
   if (quFreeDoc->RecordCount > 0)
   {
   ASpec->AsXML = quFreeDoc->FN("SPECTEXT")->AsString;
   return true;
   }
   else
   {
   return false;
   }
   */
  return RC;
 }
// ---------------------------------------------------------------------------
TICSNomenklator * __fastcall TdsDocDM::FGetNom()
 {
  TICSNomenklator * RC = FNom;
  if (!RC)
   {
    TTagNode * defNode = ICS_DATA_DEF->GetChildByAV("item", "xmltype", "nomenklator", true);
    if (defNode)
     {
      try
       {
        if (!FNmkl)
         FNmkl = FXMLList->GetXML(defNode->AV["xmlref"]);
        FNom = new TICSNomenklator();
        // FNom->OnPrInit = AOnPrInit;
        // FNom->OnPrCh   = AOnPrCh;
        // FNom->OnPrComp = AOnPrComp;
        FNom->SrcNom = FNmkl;
        RC           = FNom;
       }
      __finally
       {
        // FNom->OnPrInit  = NULL;
        // FNom->OnPrCh    = NULL;
        // FNom->OnPrComp  = NULL;
        FBaseISRef = (FNmkl->GetChildByName("passport", true)->AV["gui"] + "." + FNom->Nmkl->MainISUID).UpperCase();
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
// Variant __fastcall TdsDocDM::FGetICSNom()
// {
// Variant RC = Variant::Empty();
// try
// {
// }
// __finally
// {
// }
// return RC;
// }
// ---------------------------------------------------------------------------
// void __fastcall TdsDocDM::FSetICSNom(Variant AVal)
// {
// }
// ---------------------------------------------------------------------------
TICSNomenklator * __fastcall TdsDocDM::FGetDocNom()
 {
  TICSNomenklator * RC = FDocNom;
  if (!RC)
   {
    TTagNode * defNode = ICS_DATA_DEF->GetChildByAV("item", "xmltype", "nomenklatordoc", true);
    if (defNode)
     {
      try
       {
        if (!FDocNmkl)
         FDocNmkl = FXMLList->GetXML(defNode->AV["xmlref"]);
        FDocNom = new TICSNomenklator();
        // FDocNom->OnPrInit = AOnPrInit;
        // FDocNom->OnPrCh   = AOnPrCh;
        // FDocNom->OnPrComp = AOnPrComp;
        FDocNom->SrcNom = FDocNmkl;
        RC              = FDocNom;
       }
      __finally
       {
        // FDocNom->OnPrInit  = NULL;
        // FDocNom->OnPrCh    = NULL;
        // FDocNom->OnPrComp  = NULL;
        FBaseISRef = (FDocNmkl->GetChildByName("passport", true)->AV["gui"] + "." + FDocNom->Nmkl->MainISUID)
          .UpperCase();
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
// Variant __fastcall TdsDocDM::FGetICSDocNom()
// {
// Variant RC = Variant::Empty();
// try
// {
// }
// __finally
// {
// }
// return RC;
// }
// ---------------------------------------------------------------------------
// void __fastcall TdsDocDM::FSetICSDocNom(Variant AVal)
// {
// }
// ---------------------------------------------------------------------------
__int64 __fastcall TdsDocDM::kabCDSGetCount(UnicodeString AId, UnicodeString ATTH, UnicodeString AFilterParam)
 {
  __int64 RC = 0;
  try
   {
    if (FOnGetCount)
     RC = FOnGetCount(AId, AFilterParam);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsDocDM::kabCDSGetCodes(UnicodeString AId, int AMax, UnicodeString ATTH,
  UnicodeString AFilterParam)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetIdList)
     FOnGetIdList(AId, AMax, AFilterParam, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsDocDM::kabCDSGetValById(UnicodeString AId, UnicodeString ARecId)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetValById)
     FOnGetValById(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsDocDM::kabCDSMainAppGetValById(UnicodeString AId, UnicodeString ARecId)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnMainAppGetValById)
     FOnMainAppGetValById(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsDocDM::kabCDSGetValById10(UnicodeString AId, UnicodeString ARecId)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetValById10)
     FOnGetValById10(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::kabCDSImport(UnicodeString AId, TJSONObject * AValues, UnicodeString & ARecId)
 {
  UnicodeString RC = "error";
  try
   {
    if (FOnImportData)
     RC = FOnImportData(AId, AValues, ARecId);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::kabCDSInsert(UnicodeString AId, TJSONObject * AValues, UnicodeString & ARecId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnInsertData)
     FOnInsertData(AId, AValues, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::kabCDSEdit(UnicodeString AId, TJSONObject * AValues, UnicodeString & ARecId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnEditData)
     FOnEditData(AId, AValues, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::kabCDSDelete(UnicodeString AId, UnicodeString ARecId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnDeleteData)
     FOnDeleteData(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::kabCDSGetClassXML(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetClassXML)
     FOnGetClassXML(AId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::kabCDSCheckByPeriod(bool FCreateByType, UnicodeString ACode, UnicodeString ASpecGUI)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnCheckByPeriod)
     FOnCheckByPeriod(FCreateByType, ACode, ASpecGUI, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::kabCDSDocListClearByDate(TDate ADate)
 {
  bool RC = false;
  try
   {
    if (FOnDocListClearByDate)
     RC = FOnDocListClearByDate(ADate);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsDocDM::GetDocCountBySpec(UnicodeString ASpecOWNER, UnicodeString ASpecGUI, UnicodeString & ARCMsg)
 {
  int RC = 0;
  try
   {
    if (FOnGetDocCountBySpec)
     RC = FOnGetDocCountBySpec(ASpecOWNER, ASpecGUI, ARCMsg);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::IsMainOwnerCode(UnicodeString ASpecOwner)
 {
  bool RC = false;
  try
   {
    if (FOnIsMainOwnerCode)
     RC = FOnIsMainOwnerCode(ASpecOwner);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::CopySpec(UnicodeString AGUI, UnicodeString & ANewGUI)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnCopySpec)
     FOnCopySpec(AGUI, ANewGUI, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TExistsType __fastcall TdsDocDM::CheckSpecExists(UnicodeString AName, UnicodeString & AGUI, bool AIsEdit)
 {
  TExistsType RC = etOk;
  try
   {
    if (FOnCheckSpecExists)
     RC = (TExistsType)FOnCheckSpecExists(AName, AGUI, AIsEdit);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TExistsType __fastcall TdsDocDM::CheckDocExists(UnicodeString AAuthor, UnicodeString ASpecGUI, int APeriodType,
  UnicodeString APeriodVal)
 {
  TExistsType RC = etOk;
  try
   {
    TDate FPeriodDateFr, FPeriodDateTo;
    FPeriodDateFr = TDate(0);
    FPeriodDateTo = TDate(0);
    if (GetLPartB(APeriodVal, ';').Trim().Length())
     FPeriodDateFr = TDate(GetLPartB(APeriodVal, ';').Trim());
    if (GetRPartE(APeriodVal, ';').Trim().Length())
     FPeriodDateTo = TDate(GetRPartE(APeriodVal, ';').Trim());
    UnicodeString FDocGUI = "";
    if (FOnCheckExistDocCreate(ASpecGUI, AAuthor, APeriodType, FPeriodDateFr, FPeriodDateTo, FDocGUI))
     RC = etExistByName;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::GetDocById(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetDocById)
     FOnGetDocById(AId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::SetTextData(System::UnicodeString ATabId, System::UnicodeString AFlId,
  System::UnicodeString ARecId, System::UnicodeString AData)
 {
  try
   {
    if (FOnSetText)
     {
      UnicodeString RC = "ok";
      FOnSetText("doc." + ATabId, AFlId, ARecId, AData, RC);
      if (!SameText(RC, "ok"))
       throw Exception("������ ���������� ������ " + ATabId + " " + AFlId + ".");
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::GetTextData(System::UnicodeString ATabId, System::UnicodeString AFlId,
  System::UnicodeString ARecId)
 {
  UnicodeString RC = "";
  UnicodeString MRC = "";
  try
   {
    if (FOnGetText)
     {
      FOnGetText("doc." + ATabId, AFlId, ARecId, RC, MRC);
      if (!SameText(MRC, "ok"))
       throw Exception("������ ���������� ������ " + ATabId + " " + AFlId + ".");
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::FillLPUData(TStrings * ADest, __int64 ACode)
 {
  try
   {
    TJSONObject * RetData;
    UnicodeString FClCode, FClStr;
    if (ACode)
     RetData = kabCDSMainAppGetValById
       ("reg.s2.Select CODE as ps1, R00CA as ps2 From CLASS_00C7 Where R0104 = 0 and code = " + IntToStr(ACode) +
      " Order By R00CA", "0");
    else
     RetData = kabCDSMainAppGetValById
       ("reg.s2.Select CODE as ps1, R00CA as ps2 From CLASS_00C7 Where R0104 = 0 Order By R00CA", "0");
    TJSONPairEnumerator * itPair = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      ADest->AddObject(FClStr, (TObject *)FClCode.ToIntDef(0));
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::GetLPUData(__int64 ACode, TTagNode * ADest)
 {
  try
   {
    TJSONObject * RetData;
    RetData = kabCDSMainAppGetValById("reg.00C7", IntToStr(ACode));
    /*
     FFlId = itField->AV["uid"];
     tmpFlData = (TJSONObject*)RetData->Values[FFlId];
     if (tmpFlData)
     {
     tmpData = (TJSONString*)tmpFlData;
     if (itField->CmpName("binary,choice,choiceDB,choiceTree,extedit"))
     {
     FFlVal =((TJSONString*)tmpFlData->Pairs[0]->JsonString)->Value();
     if(FFlVal.Trim().Length())
     {
     if (itField->CmpName("binary,choice,choiceDB"))
     ARow->Value[FFlId] = FFlVal.ToInt();
     else
     ARow->Value[FFlId] = FFlVal;

     }
     else
     ARow->Value[FFlId] = Variant::Empty();

     ARow->StrValue[FFlId] = ((TJSONString*)tmpFlData->Pairs[0]->JsonValue)->Value();
     }
     else if (itField->CmpName("text,date,digit"))
     {
     if(!tmpData->Value().Trim().Length())
     ARow->Value[FFlId] = Variant::Empty();
     else
     {
     if (itField->CmpName("date"))
     ARow->Value[FFlId] = TDate(tmpData->Value());
     else
     ARow->Value[FFlId] = tmpData->Value();
     }
     }
     }



     FFlId = itField->AV["uid"];



     try
     {
     TJSONString *tmpData;
     UnicodeString FFlId;
     UnicodeString FFlVal;
     //     FFlVal = Row[ARowId]->Value["code"];
     //     tmpRowData = FOnGetValById(FDSId, FFlVal);
     TTagNode* itField = FDefNode->GetFirstChild();
     while (itField)
     {
     if (!itField->CmpName("code,data"))
     {
     }
     itField = itField->GetNext();
     }
     ARow->Fetched = true;
     RC = true;
     }
     __finally
     {
     }
     */
    // qtExec("Select * From "+TabName+" Where CODE="+IntToStr(ACode), false);
    ADest->DeleteChild();
    // if (quFree->RecordCount)
    // {
    ADest->AddChild("Code")->AV["PCDATA"] = JSONStrVal(RetData, "011E");
    ADest->AddChild("FullName")->AV["PCDATA"] = JSONStrVal(RetData, "00CB");
    ADest->AddChild("Name")->AV["PCDATA"] = JSONStrVal(RetData, "00CA");
    ADest->AddChild("OwnerFIO")->AV["PCDATA"] = JSONStrVal(RetData, "00CD");
    ADest->AddChild("Phone")->AV["PCDATA"] = JSONStrVal(RetData, "00CE");
    ADest->AddChild("EMail")->AV["PCDATA"] = JSONStrVal(RetData, "00CF");
    ADest->AddChild("Addr")->AV["PCDATA"] = JSONObjectVal(RetData, "00CC");
    // }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::JSONStrVal(TJSONObject * AData, UnicodeString AFlName)
 {
  UnicodeString RC = "";
  TJSONObject * tmpFlData;
  try
   {
    tmpFlData = (TJSONObject *)AData->Values[AFlName];
    if (tmpFlData)
     RC = ((TJSONString *)tmpFlData)->Value();
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocDM::JSONObjectVal(TJSONObject * AData, UnicodeString AFlName)
 {
  UnicodeString RC = ":";
  TJSONObject * tmpFlData;
  try
   {
    tmpFlData = (TJSONObject *)AData->Values[AFlName];
    if (tmpFlData)
     {
      if (tmpFlData->Count)
       RC = tmpFlData->Pairs[0]->JsonString->Value() + ":" + tmpFlData->Pairs[0]->JsonValue->Value();
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::GetQList(System::UnicodeString AId, TStringList * AList)
 {
  TJSONObject * FRC = NULL;
  try
   {
    if (FOnGetQList)
     {
      FOnGetQList(AId, "get", FRC);
      TJSONPairEnumerator * itPair = FRC->GetEnumerator();
      AList->Clear();
      while (itPair->MoveNext())
       AList->Add(((TJSONString *)itPair->Current->JsonString)->Value() + "=" +
        ((TJSONString *)itPair->Current->JsonValue)->Value());
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::SetQList(System::UnicodeString AId, TJSONObject * AList)
 {
  try
   {
    if (FOnGetQList)
     FOnGetQList(AId, "set", AList);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocDM::GetQuickList(UnicodeString AId, TStringList * AList)
 {
  if (AList->Count)
   {
    TStringList * FDest = new TStringList;
    TStringList * FCommSrc = new TStringList;
    try
     {
      UnicodeString FId = GetLPartB(AId, ':');
      if (SameText(AList->Strings[0], "set"))
       {
        GetQList(FId, FDest);
        GetQList("com" + FId, FCommSrc);
        TExtDistForm * Dlg = new TExtDistForm(this, GetRPartB(AId, ':'), FDest, FCommSrc,
          (FId.UpperCase() == "REPORT"));
        try
         {
          Dlg->ShowModal();
          if (Dlg->ModalResult == mrOk)
           { // ��������� ������
            SetQList(FId, Dlg->List);
           }
         }
        __finally
         {
          if (Dlg)
           delete Dlg;
         }
       }
      // ��������� ������
      GetQList(FId, AList);
     }
    __finally
     {
      delete FDest;
      delete FCommSrc;
     }
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocDM::CanHandEdit(System::UnicodeString ASpecGUID)
 {
  bool RC = false;
  try
   {
    if (FOnCanHandEdit)
     RC = FOnCanHandEdit(ASpecGUID);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
