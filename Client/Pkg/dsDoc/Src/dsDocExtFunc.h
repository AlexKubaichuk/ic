//---------------------------------------------------------------------------

#ifndef dsDocExtFuncH
#define dsDocExtFuncH
#include "XMLContainer.h"
//---------------------------------------------------------------------------
enum TDocPeriodicity {dpNo=0,dpUnique=1,dpMonth=2,dpQuarter=3,dpHalfYear=4,dpYear=5};
extern PACKAGE TTagNode* __fastcall GetPassport(TTagNode *ANode, bool AFromRoot = true);
extern PACKAGE UnicodeString __fastcall GetSpecName(TTagNode *ANode, bool AFromRoot = true);
extern PACKAGE UnicodeString __fastcall GetDocName(TTagNode *ANode, bool AFromRoot = true);
extern PACKAGE UnicodeString __fastcall GetSpecGUI(TTagNode *ANode, bool AFromRoot = true);
extern PACKAGE UnicodeString __fastcall GetDocGUI(TTagNode *ANode, bool AFromRoot = true);
extern PACKAGE UnicodeString __fastcall GetPeriodDef(TTagNode *ANode, bool AFromRoot = true); //+
extern PACKAGE UnicodeString __fastcall GetStrSpecPeriod(TTagNode *ANode, bool AFromRoot = true);
extern PACKAGE int           __fastcall GetIntSpecPeriod(TTagNode *ANode, bool AFromRoot = true);
extern PACKAGE UnicodeString __fastcall GetDocPeriod(TTagNode *ANode, bool AFromRoot = true);
extern PACKAGE UnicodeString __fastcall GetSpecOwner(TTagNode *ANode, bool AFromRoot = true);
extern PACKAGE UnicodeString __fastcall GetDocOwner(TTagNode *ANode, bool AFromRoot = true);
extern PACKAGE UnicodeString __fastcall GetDocSpecGUI(TTagNode *ANode, bool AFromRoot = true);
extern PACKAGE void __fastcall SetDocSpecGUI(TTagNode *ANode,UnicodeString AVal, bool AFromRoot = true);
extern PACKAGE void __fastcall SetSpecName(TTagNode *ANode, UnicodeString AVal, bool AFromRoot = true);
extern PACKAGE void __fastcall SetDocName(TTagNode *ANode, UnicodeString AVal, bool AFromRoot = true);
extern PACKAGE void __fastcall SetSpecGUI(TTagNode *ANode, UnicodeString AVal, bool AFromRoot = true);
extern PACKAGE void __fastcall SetDocGUI(TTagNode *ANode, UnicodeString AVal, bool AFromRoot = true);
extern PACKAGE void __fastcall SetSpecPeriod(TTagNode *ANode, UnicodeString AVal, bool AFromRoot = true);
extern PACKAGE void __fastcall SetDocPeriod(TTagNode *ANode, UnicodeString AVal, bool AFromRoot = true);
extern PACKAGE void __fastcall SetSpecOwner(TTagNode *ANode, UnicodeString AVal, bool AFromRoot = true);
extern PACKAGE void __fastcall SetDocOwner(TTagNode *ANode, UnicodeString AVal, bool AFromRoot = true);
extern PACKAGE int __fastcall DocPeriodInt(UnicodeString APeriod);
extern PACKAGE UnicodeString __fastcall SpecPeriodRuStr(int APeriod);
extern PACKAGE UnicodeString __fastcall DocPeriodRuStr(int APeriod);
extern PACKAGE void __fastcall SetDocPeriodVal(TTagNode *ANode, UnicodeString AFVal, UnicodeString ATVal, bool AFromRoot = true);
extern PACKAGE UnicodeString __fastcall GetDocPeriodValFrom(TTagNode *ANode, bool AFromRoot = true);
extern PACKAGE UnicodeString __fastcall GetDocPeriodValTo(TTagNode *ANode, bool AFromRoot = true);
extern PACKAGE UnicodeString __fastcall GetDocPeriodValStr(UnicodeString APerType, UnicodeString AValFrom, UnicodeString APerTo);

extern PACKAGE UnicodeString __fastcall GetGUI(TTagNode *ANode);
extern PACKAGE TDateTime __fastcall TSToDate(UnicodeString ATS);
extern PACKAGE UnicodeString __fastcall icsDocExCorrectCount(int ACount);
extern PACKAGE UnicodeString __fastcall GetTemplateFields(TTagNode *ADescription);


#endif
