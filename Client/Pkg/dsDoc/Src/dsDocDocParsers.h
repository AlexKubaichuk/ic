/*
  ����        - ICSDocParsers.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� ������
                ��� ������� ������ ���������� ������/������� ������� �
                ������ �������� ������� ����� ������ � �������
  ����������� - ������� �.�.
  ����        - 26.10.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  20.04.2007
    [*] ���� ��� ���������� ��������� ���������� THandRulesCalc � TCalcRulesCalc
        ���������� ������� �� ���� ��:
          ��� ������ �������� ������� ����� ������������ �������� 0 (������� �� ���������)
          ��� ������ ���������� ������������ �������� 0

  16.03.2006
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �����������

  12.07.2005
    1. ��������� ���� ��� ����������� ������� ������ �������� ������� �����

  21.06.2005
    1. ��������� ��������� ������� ����� � ������� #<�����_������>:<�����_�������>
    2. ��������� ���������� �������
       //���������� ����� ������/������� �� ��������������� ������� (�.�. ��� #)
       UnicodeString __fastcall GetRowColNumLexNumber ( UnicodeString ARowColNumLex )
       //���������� ����� ������ �� �������, ��������������� ������ ������
       UnicodeString __fastcall GetCellNumLexRowNumber( UnicodeString ACellNumLex   )
       //���������� ����� ������� �� �������, ��������������� ������ ������
       UnicodeString __fastcall GetCellNumLexColNumber( UnicodeString ACellNumLex   )

  30.11.2004
    1. �������� ����� TCalcRulesCalc
    2. ��� ������� THandRulesCalc � TCalcRulesCalc ������� ������� �����
       TBaseRulesCalc

  23.11.2004
    1. �������� ����� THandRulesCalc
    2. �������� ����� AxeUtil::CalcCondExp() ��� �����������
       �������, ������������ ������� �������� ������� �����

  13.11.2004
    1. ��������� ���� - ������ ������ ��������, ���� ��������� �� ����������

  12.11.2004
    1. ������ ������������ �� CalcRulesParser � ICSDocParsers
    2. ��������� �������� ����������� ������ �������� ������� �����
       ������ � �������

  29.10.2004
    1. � ������������� ���������� ��������� �������� ����, �����
       ����� ������ ������� ��������� � 1

  28.10.2004
    1. ����������� ������� SUPPORT_NAMES �������� � ICSDocCfg.h
    2. �������� #include "ICSDocCfg.h" � ������������ �����

  26.10.2004
    �������������� �������
*/
//---------------------------------------------------------------------------
/*
  ���� �� �������:
        1. ���� ����������� ��������� �������� �/��� ���� ���������� ���� THandRulesParser'��,
           �� ���� �������� ������������� ������ �� ������� ������������ ����� ��������� ������
           �������� � ����������� ������� ������ ��� ����������� ���� �����
           ( ��������� ��� ����������� )
*/
//---------------------------------------------------------------------------
/*
  �������� ������� ������:
                                   TObject
                                      |
             ------------------------------------------------------------------------
            |                  |            |                |                       |
        TLexTable       TSyntTreeNode   TSyntTree       TDKParser              TBaseRulesCalc
                                                             |                       |
          ----------------------------------------------------                --------------
          |                |                |                 |               |             |
       TExprLex        TExprSynt        TExprSem        TExprParser    THandRulesCalc TCalcRulesCalc
          |                |                |                 |
      --------        ---------         --------         ----------
      |       |       |        |        |       |        |         |
TCalcRulesLex |       |        |        |       |        |         |
        THandRulesLex |        |        |       |        |         |
                TCalcRulesSynt |        |       |        |         |
                         THandRulesSynt |       |        |         |
                                  TCalcRulesSem |        |         |
                                           THandRulesSem |         |
                                                  TCalcRulesParser |
                                                            THandRulesParser
*/
//---------------------------------------------------------------------------

#ifndef dsDocDocParsersH
#define dsDocDocParsersH

//#include "ICSDoc.h"

#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <set>
#include <map>
#include <list>
#include <vector>
#include <stack>

namespace Dsdocdocparsers
{
using namespace std;

//***************************************************************************
//********************** ���� ***********************************************
//***************************************************************************

//��������� ������������ ����������� (��. ����� ��������)
enum TExprLexState {
  stS,
  stIdent,
  stLiteral,
  stSep,
  stPossibleAConst,
  stDrob,
  stDigits,
  stEndDec_DecFloat,
  stRowColNum_CellNum,
  stEndRowColNum_CellNum,
  stCellNum,
  stEndCellNum,
  stSepAn,
  stError,
  stEnd
};

//��� ������ ������������ �����������
enum TLexErrType {
  etNone = 0,           //�������� ������
  etEmptyExpr,          //������ ���������
  etUnexpectedChar,     //�������� ������
  etUnexpectedEOE,      //����������� ����� �������
  etSepExpected,        //��������� ������-�����������
  etDigExpected,        //��������� ������-�����
  etDigSepExpected,     //��������� ������-����� ��� ������-�����������
  etDigSepDotExpected,  //��������� ������-�����, ������-����������� ��� ������ '.'
  etDigSepColonExpected //��������� ������-�����, ������-����������� ��� ������ ':'
};

//��� ������ ��������������� �����������
enum TSyntErrType {
  stNone = 0,           //�������� ������
  stLeftRightDis,       //��������� ������
  stEmptyExpr,          //������ ���������
  stNoneOp,             //��������� �� �������� ��������
  stBadOp,              //�������� �� ������ � ������ ���������
  stBadOperand,         //��� �������� �� ������� ��������
  stBadSingleOp,        //��� �������� ������ ���� ������ ���� � ������ ���� �������
  stBadBiOp,            //��� �������� ������ ���� ������� ��� ��������
  stBadLOp,             //��� ���������� �������� ������ ���� ������� ���������� ��������
  stBadAOp,             //��� �������������� �������� ������ ���� ������� �������������� ��������
  stBadRelOp,           //��� �������� ��������� � �������� ��������� ����� ��������� ���, ���������, �������������� ��������� ��� ����� ������ �������
  stBadCalcRulesExpr,   //������� ���������� ������ ���� ������� � ���� ��������������� ��������� ��� ��������������� ��������
  stBadHandRulesExpr    //������� �������� ������ ���� ������� � ���� ����������� ���������
};

//��� ������ �������������� �����������
enum TSemErrType {
  mtNone = 0,           //�������� ������
  mtConstTooBig,        //�������� ��������� ������� ������
  mtRowColNumTooBig,    //����� ������� �����
  mtRowColNumBaseOne,   //��������� ���������� � 1
  mtCellRowNumTooBig,   //����� ������ ������� �����
  mtCellRowNumBaseOne,  //��������� ����� ���������� � 1
  mtCellColNumTooBig,   //����� ������� ������� �����
  mtCellColNumBaseOne,  //��������� �������� ���������� � 1
  mtDivByZero,          //������� �� ����
  mtBadAOp,             //��� �������������� �������� ������ ���� ������� �������������� ��������
  mtBadRelOp            //��� �������� ��������� � �������� ��������� ����� ��������� ���, ���������, �������������� ��������� ��� ����� ������ �������
};

//��� �������
enum TLexType {
  ltName = 0,           //���
  ltSConst,             //��������� ���������
  ltDConst,             //���������� ����� ���������
  ltFConst,             //���������� ������������ ���������
  ltRowColNum,          //����� ������ �������
  ltCellNum,            //����� ������  
  ltADD,                //�������� ��������
  ltSUB,                //�������� ���������
  ltMUL,                //�������� ���������
  ltDIV,                //�������� �������
  ltEQ,                 //��������� �����
  ltNE,                 //��������� �� �����
  ltLT,                 //��������� ������
  ltGT,                 //��������� ������
  ltLE,                 //��������� ������ ��� �����
  ltGE,                 //��������� ������ ��� �����
  ltLeft,               //����������� ������
  ltRight,              //����������� ������
  ltNOT,                //�������� ��
  ltAND,                //�������� �
  ltOR                  //�������� ���
};

//����� �������
enum TLexClass {
  lcUnknown = 0,        //��� ������������ ����
  lcNumber,             //��� ��� ��������� ��������� ����, ����� ������/������� ��� ����� ������
  lcString,             //��� ��� ��������� ���������� ����
  lcSingleOp,           //������� ��������
  lcMulOp,              //����������������� ��������
  lcAddOp,              //���������� ��������
  lcRelOp,              //�������� ���������
  lcLeft,               //����������� ������
  lcRight               //����������� ������
};


//��� ������ ������� ���������
enum TExprParserErrType {
  peNone,               //�������� ������
  peLex,                //����������� ������
  peSynt,               //�������������� ������
  peSem                 //������������� ������
};

//������ ��� ������ ������� ������
typedef struct tagLEX_TABLE_REC {
  UnicodeString Data;                      //�������
  TLexType   Type;                      //��� �������
  TLexClass  Class;                     //����� �������
}LEX_TABLE_REC, *PLEX_TABLE_REC;

//������� ������ ������
typedef struct tagLEX_LIST_ITEM {
  int LexTableId;                       //��� ������ ������� ������
  int ExprPos;                          //������� ������ ������� � �������� ���������
}LEX_LIST_ITEM, *PLEX_LIST_ITEM;

typedef set<char> TCharSet;                     //��������� ��������
typedef map<int,LEX_TABLE_REC> TLexMap;         //������� ������
typedef list<LEX_LIST_ITEM> TLexList;           //������ ������

class TSyntTreeNode;
//��������� �� �������, ���������� ��� ������ ��������������� ������
typedef bool __fastcall (__closure *TSyntTreeIterate)(TSyntTreeNode* ItTag );
typedef bool __fastcall (__closure *TSyntTreeIterate1)(TSyntTreeNode* ItTag, void *p1 );

//***************************************************************************
//********************** TLexTable ******************************************
//***************************************************************************

//������� ������

class PACKAGE TLexTable : public TObject
{
        typedef TObject inherited;

  private:
        TLexMap FLexMap;
        int     FID;
        bool    FModified;


  protected:
        const TLexMap* __fastcall GetLexMap() const;


  public:
        __fastcall TLexTable();
        virtual __fastcall ~TLexTable();

        __property TLexMap* LexMap = { read = GetLexMap };
        __property bool Modified = { read = FModified, write = FModified };

        void __fastcall Clear();
        bool __fastcall Empty() const;
        int __fastcall NewID();
        LEX_TABLE_REC __fastcall GetRec( LEX_LIST_ITEM LexListItem );
        LEX_TABLE_REC __fastcall GetRec( int nId );
        int __fastcall FindRec( UnicodeString AData, TLexType AType, TLexClass AClass ) const;

        bool __fastcall IsOperand     ( LEX_TABLE_REC LexTableRec );
        bool __fastcall IsOperation   ( LEX_TABLE_REC LexTableRec );
        int  __fastcall GetOpPriority ( LEX_TABLE_REC LexTableRec );
        bool __fastcall IsLOp         ( LEX_TABLE_REC LexTableRec );
        bool __fastcall IsAOp         ( LEX_TABLE_REC LexTableRec );
        bool __fastcall IsRelOp       ( LEX_TABLE_REC LexTableRec );
        bool __fastcall IsOperand     ( LEX_LIST_ITEM LexListItem );
        bool __fastcall IsOperation   ( LEX_LIST_ITEM LexListItem );
        int  __fastcall GetOpPriority ( LEX_LIST_ITEM LexListItem );
        bool __fastcall IsLOp         ( LEX_LIST_ITEM LexListItem );
        bool __fastcall IsAOp         ( LEX_LIST_ITEM LexListItem );
        bool __fastcall IsRelOp       ( LEX_LIST_ITEM LexListItem );
};

//***************************************************************************
//********************** TSyntTreeNode **************************************
//***************************************************************************

//���� ��������������� ������

class PACKAGE TSyntTreeNode : public TObject
{
        typedef TObject inherited;

  private:
        LEX_LIST_ITEM FLex;
        TSyntTreeNode* FParent;
        TSyntTreeNode* FLeft;
        TSyntTreeNode* FRight;
        int FLevel;


  public:
        __fastcall TSyntTreeNode( TSyntTreeNode *AParent, LEX_LIST_ITEM ANode );
        virtual __fastcall ~TSyntTreeNode();

        __property LEX_LIST_ITEM Lex = { read = FLex, write = FLex };
        __property TSyntTreeNode* Left = { read = FLeft, write = FLeft };
        __property TSyntTreeNode* Right = { read = FRight, write = FRight };
        __property TSyntTreeNode* Parent = { read = FParent };
        __property int Level = { read = FLevel };

        TSyntTreeNode* __fastcall AddLeftChild ( LEX_LIST_ITEM ANode );
        TSyntTreeNode* __fastcall AddRightChild( LEX_LIST_ITEM ANode );

        bool __fastcall Iterate ( TSyntTreeIterate ItItem );
        bool __fastcall Iterate1( TSyntTreeIterate1 ItItem, void *p1 );
};

//***************************************************************************
//********************** TSyntTree ******************************************
//***************************************************************************

//�������������� ������

class PACKAGE TSyntTree : public TObject
{
        typedef TObject inherited;

  private:
        TSyntTreeNode* FRoot;


  public:
        __fastcall TSyntTree( PLEX_LIST_ITEM ARoot = NULL );
        virtual __fastcall ~TSyntTree();

        __property TSyntTreeNode* Root = { read = FRoot, write = FRoot };

        TSyntTreeNode* __fastcall CreateRoot( LEX_LIST_ITEM ANode );
        void __fastcall Clear();
};

//***************************************************************************
//********************** TDKParser ******************************************
//***************************************************************************

//����������� ����� "����������", �������� ������� ��� ������������, ��������������� �
//�������������� ������������

class PACKAGE TDKParser : public TObject
{
        typedef TObject inherited;

  protected:
        TObject *FOwner;
        int FErrPos;
        int FErrLen;


  public:
        __fastcall TDKParser( TObject *AOwner );
        __fastcall ~TDKParser();

        __property int ErrPos = { read = FErrPos };
        __property int ErrLen = { read = FErrLen };

        virtual void __fastcall Init();
        virtual bool __fastcall Parse();
        virtual UnicodeString __fastcall GetErrMsg() = 0;
        virtual int __fastcall GetErrCode() = 0;
};

//***************************************************************************
//********************** TExprLex *******************************************
//***************************************************************************

//����������� ���������� ���������
// ����  - ��������� � ���� ������ ��������
// ����� - ������ ������ � ������������ ( � ������ ������ ����� �� ������� ������ )
//         ��� ��� ������
// ��� ����������� ������� ����������� ������� ������

class PACKAGE TExprLex : public TDKParser
{
        typedef TDKParser inherited;

  protected:
        TExprLexState FState;
        UnicodeString    FExpr;
        UnicodeString    FLex;
        int           FLexStartPos;
        int           FPos;
        TLexErrType   FErrType;
        TCharSet      FChSeps;
        TCharSet      FBlankSeps;
        TCharSet      FSignSeps;
        TCharSet      FChOp;
        TLexTable*    FLexTable;
        TLexList*     FLexList;

        void __fastcall SetExpr( UnicodeString AExpr );
        char __fastcall GetChar( int nOffset = 0 );
        char __fastcall GetNextChar();
        char __fastcall LookUpNextChar();
        char __fastcall LookUpPrevNotBlankChar();
        char __fastcall GetPrevChar();
        char __fastcall GoToPrev();

        void __fastcall AddChar( const char c );
        void __fastcall GoToState( TExprLexState NewState );
        void __fastcall ChangeState( const char c, TExprLexState NewState );
        virtual void __fastcall FillLexTable() = 0;


  public:
        __fastcall TExprLex( TObject *AOwner, TLexTable* ALexTable, TLexList* ALexList, UnicodeString AExpr = "" );
        virtual __fastcall ~TExprLex();

        virtual void __fastcall Init();
        virtual UnicodeString __fastcall GetErrMsg();
        virtual int __fastcall GetErrCode();

        static UnicodeString __fastcall GetLexStrType( TLexType LexType );
        static UnicodeString __fastcall GetLexStrClass( TLexClass LexClass );
        static bool __fastcall IsLET( const char c );
        static bool __fastcall IsID_CHAR( const char c );
        bool __fastcall IsChSep( const char c );
        bool __fastcall IsBlankSep( const char c );
        bool __fastcall IsSignSep( const char c );
        bool __fastcall IsChOp( const char c );
        bool __fastcall IsEOEOrBlankSep( const char c );

        __property UnicodeString  Expr     = { read = FExpr, write = SetExpr };
        __property TLexErrType ErrType  = {read = FErrType};
};

//***************************************************************************
//********************** TExprSynt ******************************************
//***************************************************************************

//������������� ���������� ��������� ( �� ��������� �������� )
// ����  - ������ ������ � ������������ ( � ������ ������ ����� �� ������� ������ )
// ����� - ������ ������ � ������������ ( � ������ ������ ����� �� ������� ������ ) � ���� �����'� �
//         �������������� ������ ���������
//         ��� ��� ������

class PACKAGE TExprSynt : public TDKParser
{
        typedef TDKParser inherited;

  protected:
        TLexTable*   FLexTable;
        TLexList*    FLexList;
        TLexList*    FPolandLexList;
        TSyntTree*   FSyntTree;
        TSyntErrType FErrType;

        virtual bool __fastcall CreatePoland( LEX_LIST_ITEM &LexListItem );
        virtual bool __fastcall CreateSyntTree( LEX_LIST_ITEM &LexListItem );
        virtual bool __fastcall CheckOp(TSyntTreeNode* SyntTreeNode, void *p1 ) = 0;
        virtual bool __fastcall ParseTree( LEX_LIST_ITEM &LexListItem );


  public:
        __fastcall TExprSynt( TObject*   AOwner,
                              TLexTable* ALexTable,
                              TLexList*  ALexList,
                              TLexList*  APolandLexList,
                              TSyntTree* ASyntTree );

        virtual void __fastcall Init();
        virtual bool __fastcall Parse();
        virtual UnicodeString __fastcall GetErrMsg();
        virtual int __fastcall GetErrCode();
};

//***************************************************************************
//********************** TExprSem *******************************************
//***************************************************************************

//������������� ���������� ���������
// ����  - �������������� ������ ���������
// ����� - ������� ������������� ������������ ���������
//         ��� ��� ������

class PACKAGE TExprSem : public TDKParser
{
        typedef TDKParser inherited;

  protected:
        TLexTable*   FLexTable;
        TSyntTree*   FSyntTree;
        TSemErrType  FErrType;

        virtual bool __fastcall ParseTree( LEX_LIST_ITEM &LexListItem );
        virtual bool __fastcall CheckOp(TSyntTreeNode* SyntTreeNode, void *p1 ) = 0;


  public:
        __fastcall TExprSem( TObject* AOwner, TLexTable* ALexTable, TSyntTree* ASyntTree );

        virtual void __fastcall Init();
        virtual bool __fastcall Parse();
        virtual UnicodeString __fastcall GetErrMsg();
        virtual int __fastcall GetErrCode();
};

//***************************************************************************
//********************** TExprParser ****************************************
//***************************************************************************

//���������� ���������
// ����  - ��������� � ���� ������ ��������
//         ( �������� AExpr ������������ ��� �������� Expr )
// ����� - ��������� � ���� �����'� � ������������ ' ' ����� ���������
//           ( �������� PolandStr ) � ������ ���� � ��������� �� ����� ( �������� NameMap);
//           �������� ��, �, ��� ���������� ��������� ~, &, |;
//           ����� ���������� �������������������� ���� @<������> ���
//           <������> - ������ ����� � ������ ����
//         ��� ��� ������
// ����� ��� �������� ���������� ��� �������, ��� � �����������, ���������� � ����������
// ������������ �������.
// ��� ������������ ���������� ������, ������������ ��� ��������� ����� ������� �� ������
// � �������� �������� ������.

class PACKAGE TExprParser : public TDKParser
{
        typedef TDKParser inherited;

  private:
        void __fastcall ThrowIfParsersMissing( UnicodeString AMethodName ) const;

          
  protected:
        UnicodeString      FExpr;

        TLexTable*      FLexTable;
        TLexList        FLexList;
        TLexList        FPolandLexList;
        TSyntTree*      FSyntTree;

        TLexMap         FNameMap;
        UnicodeString      FPolandStr;

        TExprLex*       FExprLex;
        TExprSynt*      FExprSynt;
        TExprSem*       FExprSem;

        TExprParserErrType  FErrType;

        void __fastcall SetExpr( UnicodeString AExpr );
        int __fastcall GetErrPos();
        int __fastcall GetErrLen();
        virtual void __fastcall InitParsers() = 0;


  public:
        __fastcall TExprParser( UnicodeString AExpr = "" );
        virtual __fastcall ~TExprParser();

        __property UnicodeString Expr = { read = FExpr, write = SetExpr };

        __property TExprLex*     ExprLex       = { read = FExprLex       };
        __property TExprSynt*    ExprSynt      = { read = FExprSynt      };
        __property TExprSem*     ExprSem       = { read = FExprSem       };
        __property TLexTable*    LexTable      = { read = FLexTable      };
        __property TLexList      LexList       = { read = FLexList       };
        __property TLexList      PolandLexList = { read = FPolandLexList };
        __property TSyntTree*    SyntTree      = { read = FSyntTree      };
        __property TLexMap       NameMap       = { read = FNameMap       };
        __property UnicodeString    PolandStr     = { read = FPolandStr     };


        virtual void __fastcall Init();
        virtual bool __fastcall Parse();
        virtual UnicodeString __fastcall GetErrMsg();
        UnicodeString __fastcall GetErrMsg( bool ShowErrType );
        virtual int __fastcall GetErrCode();
};

//***************************************************************************
//********************** TCalcRulesLex **************************************
//***************************************************************************

//����������� ���������� ���������
//( � ������ - ������ ���������� ������/������� ������� )
// ����  - ��������� � ���� ������ ��������
// ����� - ������ ������ � ������������ ( � ������ ������ ����� �� ������� ������ )
//         ��� ��� ������
// ��� ����������� ������� ����������� ������� ������

class PACKAGE TCalcRulesLex : public TExprLex
{
        typedef TExprLex inherited;

  protected:
        virtual void __fastcall FillLexTable();


  public:
        __fastcall TCalcRulesLex( TObject *AOwner, TLexTable* ALexTable, TLexList* ALexList, UnicodeString AExpr = "" );

        virtual bool __fastcall Parse();
};

//***************************************************************************
//********************** TCalcRulesSynt *************************************
//***************************************************************************

//������������� ���������� ��������� ( �� ��������� �������� )
//( � ������ - ������ ���������� ������/������� ������� )
// ����  - ������ ������ � ������������ ( � ������ ������ ����� �� ������� ������ )
// ����� - ������ ������ � ������������ ( � ������ ������ ����� �� ������� ������ ) � ���� �����'� �
//         �������������� ������ ���������
//         ��� ��� ������

class PACKAGE TCalcRulesSynt : public TExprSynt
{
        typedef TExprSynt inherited;

  protected:
        virtual bool __fastcall CheckOp(TSyntTreeNode* SyntTreeNode, void *p1 );
        virtual bool __fastcall ParseTree( LEX_LIST_ITEM &LexListItem );


  public:
        __fastcall TCalcRulesSynt( TObject*   AOwner,
                                   TLexTable* ALexTable,
                                   TLexList*  ALexList,
                                   TLexList*  APolandLexList,
                                   TSyntTree* ASyntTree );
};

//***************************************************************************
//********************** TCalcRulesSem **************************************
//***************************************************************************

//������������� ���������� ���������
//( � ������ - ������ ���������� ������/������� ������� )
// ����  - �������������� ������ ���������
// ����� - ������� ������������� ������������ ���������
//         ��� ��� ������
// ��� ������������� ������� ��� �����������:
//   1. ���������� ������� �� ����;
//   2. ���������� ������� ������� ���������� ��������;
//   3. ���������� ������� ������� ������� �����/��������;
//   4. ������ �����/�������� ������ ���������� � 1;

class PACKAGE TCalcRulesSem : public TExprSem
{
        typedef TExprSem inherited;

  protected:
        virtual bool __fastcall CheckOp(TSyntTreeNode* SyntTreeNode, void *p1 );


  public:
        __fastcall TCalcRulesSem( TObject* AOwner, TLexTable* ALexTable, TSyntTree* ASyntTree );
};

//***************************************************************************
//********************** TCalcRulesParser ***********************************
//***************************************************************************

//���������� ������ ���������� ������/������� �������
// ����  - ������� ���������� ������/������� ������� � ���� ������ ��������
//         ( �������� AExpr ������������ ��� �������� Expr )
// ����� - ������� ���������� ������/������� ������� � ���� �����'� � ������������ ' ' ����� ���������
//           ( �������� PolandStr ) � ������ ���� � ��������� �� ����� ( �������� NameMap);
//           ����� ���������� �������������������� ���� @<������> ���
//           <������> - ������ ����� � ������ ����
//         ��� ��� ������
// ����� ��� �������� ���������� ��� �������, ��� � �����������, ���������� � ����������
// ������������ �������.
// ��� ������������ ���������� ������, ������������ ��� ��������� ����� ������� �� ������
// � �������� �������� ������.

class PACKAGE TCalcRulesParser : public TExprParser
{
        typedef TExprParser inherited;

  protected:
        virtual void __fastcall InitParsers();

        
  public:
        __fastcall TCalcRulesParser( UnicodeString AExpr = "" );
};

//***************************************************************************
//********************** THandRulesLex **************************************
//***************************************************************************

//����������� ���������� ���������
//( � ������ - ������ �������� ������� ����� ������ � ������� )
// ����  - ��������� � ���� ������ ��������
// ����� - ������ ������ � ������������ ( � ������ ������ ����� �� ������� ������ )
//         ��� ��� ������
// ��� ����������� ������� ����������� ������� ������

class PACKAGE THandRulesLex : public TExprLex
{
        typedef TExprLex inherited;

  protected:
        virtual void __fastcall FillLexTable();


  public:
        __fastcall THandRulesLex( TObject *AOwner, TLexTable* ALexTable, TLexList* ALexList, UnicodeString AExpr = "" );

        virtual bool __fastcall Parse();
};

//***************************************************************************
//********************** THandRulesSynt *************************************
//***************************************************************************

//������������� ���������� ��������� ( �� ��������� �������� )
//( � ������ - ������ �������� ������� ����� ������ � ������� )
// ����  - ������ ������ � ������������ ( � ������ ������ ����� �� ������� ������ )
// ����� - ������ ������ � ������������ ( � ������ ������ ����� �� ������� ������ ) � ���� �����'� �
//         �������������� ������ ���������
//         ��� ��� ������

class PACKAGE THandRulesSynt : public TExprSynt
{
        typedef TExprSynt inherited;

  protected:
        virtual bool __fastcall CreateSyntTree( LEX_LIST_ITEM &LexListItem );
        virtual bool __fastcall CheckOp(TSyntTreeNode* SyntTreeNode, void *p1 );


  public:
        __fastcall THandRulesSynt( TObject*   AOwner,
                                   TLexTable* ALexTable,
                                   TLexList*  ALexList,
                                   TLexList*  APolandLexList,
                                   TSyntTree* ASyntTree );
        virtual __fastcall ~THandRulesSynt();
};

//***************************************************************************
//********************** THandRulesSem **************************************
//***************************************************************************

//������������� ���������� ���������
//( � ������ - ������ �������� ������� ����� ������ � ������� )
// ����  - �������������� ������ ���������
// ����� - ������� ������������� ������������ ���������
//         ��� ��� ������
// ��� ������������� ������� ��� �����������:
//   1. ���������� ������� �� ����;
//   2. ���������� ������� ������� ���������� ��������;
//   3. ���������� ������� ������� ������� �����/��������;
//   4. ������ �����/�������� ������ ���������� � 1;
//   5. ��� ������ ���� ltName ������������ (���� ��������) �� ���
//      ( �������������� ��� ���������� ), � ������� ������ ����������� ���������� � ����
//      ���� ����� �� ����������

class PACKAGE THandRulesSem : public TExprSem
{
        typedef TExprSem inherited;

  protected:
        virtual bool __fastcall ParseTree( LEX_LIST_ITEM &LexListItem );
        virtual bool __fastcall CheckOp(TSyntTreeNode* SyntTreeNode, void *p1 );
        bool __fastcall CheckOpNames(TSyntTreeNode* SyntTreeNode, void *p1 );        


  public:
        __fastcall THandRulesSem( TObject* AOwner, TLexTable* ALexTable, TSyntTree* ASyntTree );
};

//***************************************************************************
//********************** THandRulesParser ***********************************
//***************************************************************************

//���������� ������ �������� ������� ����� ������ � �������
// ����  - ������� �������� ������� ����� ������ � ������� � ���� ������ ��������
//         ( �������� AExpr ������������ ��� �������� Expr )
// ����� - ������� �������� ������� ����� ������ � ������� � ���� �����'� � ������������ ' ' ����� ���������
//           ( �������� PolandStr ) � ������ ���� � ��������� �� ����� ( �������� NameMap);
//           �������� ��, �, ��� ���������� ��������� ~, &, |;
//           ����� ���������� �������������������� ���� @<������> ���
//           <������> - ������ ����� � ������ ����
//         ��� ��� ������
// ����� ��� �������� ���������� ��� �������, ��� � �����������, ���������� � ����������
// ������������ �������.
// ��� ������������ ���������� ������, ������������ ��� ��������� ����� ������� �� ������
// � �������� �������� ������.

class PACKAGE THandRulesParser : public TExprParser
{
        typedef TExprParser inherited;

  protected:
        virtual void __fastcall InitParsers();


  public:
        __fastcall THandRulesParser( UnicodeString AExpr = "" );
};

//***************************************************************************
//********************** TBaseRulesCalc *************************************
//***************************************************************************

//������� ����� ��� THandRulesCalc � TCalcRulesCalc

class PACKAGE TBaseRulesCalc : public TObject
{
        typedef TObject inherited;

  private:
        UnicodeString FPolandStr;


  protected:
        void __fastcall SetPolandStr( UnicodeString AValue );
        __property UnicodeString PolandStr = { read = FPolandStr, write = SetPolandStr };

  public:
        __fastcall TBaseRulesCalc( UnicodeString APolandStr = "" );

        virtual double __fastcall Calc();
};

//***************************************************************************
//********************** THandRulesCalc *************************************
//***************************************************************************

//������������ ���������� �������, ������������ ������� �������� ������� ����� ������
// ( xml-��� handrules ������������� )
// ����  - ������� �������� ������� ����� ������ � ���� �����'� � ������������
//         ' ' ����� ���������, ������ ������� �����, ��������, �����  � ������ ����
//         ������� ��������������� �������� (���� �������� �� ����������, ��
//         ����������� "NULL") ( �������� PolandStr )
// ����� - -1 - ������� �� ����������
//          0 - ������� �����
//          1 - ������� �������
// ����� ��� �������� ���������� ��� �������, ��� � �����������, ���������� � ����������
// ������������ �������.
// ��� ������������ ���������� ������, ������������ ��� ��������� ����� ������� �� ������
// � �������� �������� ������.

class PACKAGE THandRulesCalc : public TBaseRulesCalc
{
        typedef TBaseRulesCalc inherited;

  public:
        __fastcall THandRulesCalc( UnicodeString APolandStr = "" );

        __property PolandStr;

        virtual double __fastcall Calc();
};

//***************************************************************************
//********************** TCalcRulesCalc *************************************
//***************************************************************************

//������������ ���������� ���������, ������������ ������� ����������
// ( xml-��� calcrules ������������� )
// ����  - ������� ���������� � ���� �����'� � ������������
//         ' ' ����� ���������, ������ ������� �����, ��������, ����� � ������ ����
//          ������� ��������������� �������� (���� �������� �� ����������,
//          �� ����������� "NULL") ( �������� PolandStr )
// ����� - ����������� ��������
// ����� ��� �������� ���������� ��� �������, ��� � �����������, ���������� � ����������
// ������������ �������.
// ��� ������������ ���������� ������, ������������ ��� ��������� ����� ������� �� ������
// � �������� �������� ������.

class PACKAGE TCalcRulesCalc : public TBaseRulesCalc
{
        typedef TBaseRulesCalc inherited;

  public:
        __fastcall TCalcRulesCalc( UnicodeString APolandStr = "" );
        virtual __fastcall ~TCalcRulesCalc();

        __property PolandStr;

        virtual double __fastcall Calc();
};

//***************************************************************************
//********************** ���������� ������� *********************************
//***************************************************************************

extern PACKAGE UnicodeString __fastcall GetRowColNumLexNumber ( UnicodeString ARowColNumLex );
extern PACKAGE UnicodeString __fastcall GetCellNumLexRowNumber( UnicodeString ACellNumLex   );
extern PACKAGE UnicodeString __fastcall GetCellNumLexColNumber( UnicodeString ACellNumLex   );

//---------------------------------------------------------------------------

} // end of namespace ICSDocParsers
using namespace Dsdocdocparsers;

#endif
