object dsDocCreateTypeForm: TdsDocCreateTypeForm
  Left = 482
  Top = 195
  BorderIcons = [biSystemMenu]
  BorderWidth = 3
  ClientHeight = 120
  ClientWidth = 571
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 13
    Top = 20
    Width = 67
    Height = 13
    Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
  end
  object Panel1: TPanel
    Left = 0
    Top = 79
    Width = 571
    Height = 41
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      571
      41)
    object ApplyBtn: TcxButton
      Left = 404
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object CancelBtn: TcxButton
      Left = 491
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object OrgCB: TcxComboBox
    Left = 116
    Top = 20
    Properties.DropDownListStyle = lsEditFixedList
    Properties.OnChange = OrgCBPropertiesChange
    TabOrder = 1
    Width = 447
  end
end
