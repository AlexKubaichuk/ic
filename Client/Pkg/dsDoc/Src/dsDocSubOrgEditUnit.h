//---------------------------------------------------------------------------
#ifndef dsDocSubOrgEditUnitH
#define dsDocSubOrgEditUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
#include "dsDocDMUnit.h"
//---------------------------------------------------------------------------
#define SubOrgCODE        "000D"
#define SubOrgNAME        "0022"
#define SubOrgFULLNAME    "002B"
#define SubOrgADDR        "0023"
#define SubOrgSUBORD      "0024"
#define SubOrgEMAIL       "0029"
#define SubOrgPHONE       "002A"
#define SubOrgOWNERFIO    "002D"
//---------------------------------------------------------------------------
class PACKAGE TdsDocSubOrgEditForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
 TPanel *LPUClassPanel;
 TLabel *OrgCodeLab;
 TLabel *NameLab;
 TLabel *FullNameLab;
 TLabel *AddrLab;
 TLabel *AddrValLab;
 TcxMaskEdit *OrgCodeED;
 TcxTextEdit *NameED;
 TcxTextEdit *FullNameED;
 TLabel *FIOLab;
 TcxMaskEdit *OwnerFIOED;
 TcxMaskEdit *PhoneED;
 TcxMaskEdit *EMailED;
 TLabel *EMailLab;
 TLabel *PhoneLab;
 TPanel *Panel1;
 TLabel *LPULab;
 TcxComboBox *LPUCB;
 TcxTextEdit *AddrED;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall OrgCodeEDPropertiesValidate(TObject *Sender,
          Variant &DisplayValue, TCaption &ErrorText, bool &Error);
        void __fastcall AddrEDPropertiesButtonClick(TObject *Sender,
          int AButtonIndex);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall LPUCBPropertiesChange(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
        UnicodeString FRecId;
        bool __fastcall CheckInput();
        UnicodeString SaveCode;
        TdsDocDM *FDM;
        long FExtLPUCode;
        TFillLPUData    FOnGetFillLPUData;
//        TSetGetLPUData  FOnSetLPUData;
        TSetGetLPUData  FOnGetLPUData;
        void __fastcall ClearLPUData();
        void __fastcall SetLPUData(TTagNode *AData);
        void __fastcall GetLPUData(TTagNode *AData);
        TkabCustomDataSource *FDataSrc;
        TkabCustomDataSetRow *FRow;
public:		// User declarations
        __fastcall TdsDocSubOrgEditForm(TComponent* Owner, TkabCustomDataSource *ADataSrc, TdsDocDM *ADM);

        __property TFillLPUData  OnGetFillLPUData = {read=FOnGetFillLPUData, write=FOnGetFillLPUData};
//        __property TSetGetLPUData  OnSetLPUData = {read=FOnSetLPUData, write=FOnSetLPUData};
        __property TSetGetLPUData  OnGetLPUData = {read=FOnGetLPUData, write=FOnGetLPUData};

        __property long LPUCode = {read=FExtLPUCode, write=FExtLPUCode};
        __property UnicodeString RecId = {read=FRecId};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocSubOrgEditForm *dsDocSubOrgEditForm;
//---------------------------------------------------------------------------
#endif
