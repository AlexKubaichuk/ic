﻿unit dsDocClientConst;

interface

resourcestring
// Common
  dsDocCommonErrorCaption = 'Ошибка';
  dsDocCommonInputErrorCaption = 'Ошибка';
  dsDocCommonMsgCaption = 'Сообщение';

  dsDocCommonErrorDBWorkMsg = 'Ошибка работы с БД';

  dsDocCommonConfRecDelCaption = 'Подтверждение удаления записи';
  dsDocCommonDelConfMsg = 'Подтверждаете удаление %s '#13#10'%s ?';
  dsDocCommonErrorDelCaption = 'Ошибка удаления';
  dsDocCommonErrorDelMsg = 'Ошибка удаления %s';

  dsDocCommonErrorCreateDocCaption  = 'Ошибка формирования документа';


//SQLCreator
  dsDocSQLCreatorErrorCondStructure  = 'Ошибка в структуре условий отбора';
  dsDocSQLCreatorErrorFuncMissing  = 'Отсутствует функция "%s"';



//DocCreateProgress
  dsDocDocCreateProgressCaption  = 'Формирование документа';
  dsDocDocCreateProgressStageFrom  = 'Этап : %s из %s';

//DocCreateType
//  dsDocDocCreateType  = '';
  dsDocDocCreateTypeCaption = 'Формирование документа';
  dsDocDocCreateTypeQuestLabLabCaption = 'Документ формировать по:';
  dsDocDocCreateTypeOkBtnBtnCaption = 'Применить';
  dsDocDocCreateTypeCancelBtnBtnCaption = 'Отмена';
  dsDocDocCreateTypeDocRBRBCaption = 'Документам';
  dsDocDocCreateTypeBaseRBRBCaption = 'Базе данных';

//DocCreator
  dsDocDocCreatorDocLoadConf  = 'Данный документ за указанный период уже существует!!!'#13#10' Загрузить сохранённый документ?';
  dsDocDocCreatorDocLoadConfCaption  = 'Подтверждение загрузки документа';
  dsDocDocCreatorInitCaption  = 'Инициализация...';
  dsDocDocCreatorInscCreateCaption  = 'Формирование текста надписи';
  dsDocDocCreatorListCreateCaption  = 'Формирование списка (всего строк: "%s")';
  dsDocDocCreatorErrorISLink  = 'Отсутствует связь между сущностями';
  dsDocDocCreatorTableCreateGetCount  = 'Формирование таблицы (расчёт количества строк/столбцов)';
  dsDocDocCreatorTableCreateRows  = 'Формирование таблицы (формирование строк)';
  dsDocDocCreatorTableCreateRow  = 'строка';
  dsDocDocCreatorTableCreateCols  = 'Формирование таблицы (формирование колонок)';
  dsDocDocCreatorTableCreateCol  = 'колонка';
  dsDocDocCreatorTableCreateCeills  = 'Формирование таблицы (формирование ячеек)';
  dsDocDocCreatorTableCreateCeill  = 'Формирование таблицы, ячейка(%s,%s)';
  dsDocDocCreatorTableCreateCeillMsg  = 'Формирование таблицы (%s № %s)';
  dsDocDocCreatorErrorMissingColRowDef  = 'Отсутствует описание строк или колонок';
  dsDocDocCreatorErrorCeilCalc  = 'Ошибка при вычислении ячейки.'#13#10'строка - %s колонка - %s; (%s,%s)';

  dsDocDocCreatorPeroidFromCaption  = 'с';
  dsDocDocCreatorPeroidTo1Caption  = 'по';
  dsDocDocCreatorPeroidTo2Caption  = 'до';
  dsDocDocCreatorUndefPeroidCaption  = 'Не указан';

  dsDocDocCreatorPeriodicityEqual  = 'Совпадает с периодичностью формируемого документа';
  dsDocDocCreatorPeriodicityYear  = 'Год';
  dsDocDocCreatorPeriodicityHalfYear  = 'Полугодие';
  dsDocDocCreatorPeriodicityQuarter  = 'Квартал';
  dsDocDocCreatorPeriodicityMonth  = 'Месяц';
  dsDocDocCreatorPeriodicityUnique  = 'Уникальная';
  dsDocDocCreatorPeriodicityNo  = 'Отсутствует';
  dsDocDocCreatorPeriodicityAny  = 'Любая';

  dsDocDocCreatorAuthorDefined  = '';
  dsDocDocCreatorAuthorThis  = 'Своя';
  dsDocDocCreatorAuthorAllChild  = 'Все подчинённые';

  dsDocDocCreatorErrorNoIntDocs  = 'Этап %s'#13#10'Отсутствуют документы для интеграции.'#13#10#13#10'Интегрируемые документы:'#13#10'Описание:'#09'"%s";'#13#10'Период:'#09#09'"%s";'#13#10'Периодичность:'#09'"%s";'#13#10'Организация:'#09'"%s".'#13#10;

  dsDocDocCreatorErrorCreateRowColNamesNotValid  = 'Наименование списка строк/столбцов не соответствует ''rows,columns''.';
  dsDocDocCreatorErrorRowColReferNotValid  = 'Правила формирования документа по документу,'#13#10' указана ссылка на отсутствующую %s, идф=''%s''.';

  dsDocDocCreatorSpecPrepare  = 'Подготовка описания документа';

//DocList
  dsDocDocListDocCaption  = 'документа';

  dsDocDocListCaption = 'Список документов';
  dsDocDocListLabel2Caption = 'Код документа:';
  dsDocDocListLabel4Caption = 'Дата формирования:';
  dsDocDocListSpecGUICaption = 'Код описания:';
  dsDocDocListLabel3Caption = 'Автор описания:';
  dsDocDocListLabel1Caption = 'Автор документа:';
  dsDocDocListPeriodLabCaption = 'Период формирования:';
  dsDocDocListLabel6Caption = 'Периодичность:';
  dsDocDocListLabel7Caption = 'Автор документа:';
  dsDocDocListLabel8Caption = 'Параметры отбора документов';

  dsDocDocListFilterPanelBottomBtnHint = 'Показать панель параметров отбора документов';
  dsDocDocListFilterPanelTopBtnHint = 'Скрыть панель параметров отбора документов';

  dsDocDocListPeriodicityCBItems1 = '';
  dsDocDocListPeriodicityCBItems2 = 'Отсутствует';
  dsDocDocListPeriodicityCBItems3 = 'Уникальная';
  dsDocDocListPeriodicityCBItems4 = 'Месяц';
  dsDocDocListPeriodicityCBItems5 = 'Квартал';
  dsDocDocListPeriodicityCBItems6 = 'Полугодие';
  dsDocDocListPeriodicityCBItems7 = 'Год';

  dsDocDocListResetBtnHint = 'Показать панель параметров отбора описаний документов';
  dsDocDocListResetBtnCaption = 'Сбросить параметры отбора';

  dsDocDocListactDeleteDocCaption = 'Удалить';
  dsDocDocListactDeleteDocHint = 'Удалить выбранный документ';
  dsDocDocListactPreviewCaption = 'Просмотр документа';
  dsDocDocListactPreviewHint = 'Просмотр документа...';
  dsDocDocListactFindDocCaption = 'Найти ...';
  dsDocDocListactFindDocHint = 'Найти документ';
  dsDocDocListactCreateDocCaption = 'Формировать документ';
  dsDocDocListactCreateDocHint = 'Формирование документа';
  dsDocDocListactAboutCaption = 'Справка';
  dsDocDocListactAboutHint = 'Справка';

  dsDocDocListStatusSBPanels1 = 'Всего:';
  dsDocDocListStatusSBPanels2 = 'всего записей';
  dsDocDocListStatusSBPanels3 = 'по шаблону';
  dsDocDocListStatusSBPanels4 = 'по шаблону';

  dsDocDocListListTBMBars1Caption = 'Редактирование';

  dsDocDocListDocListColumnNAMECaption = 'Наименование документа';
  dsDocDocListDocListColumnPERIOD_VALUECaption = 'Период';

//DocOrgEdit
  dsDocDocOrgEditSetCaption  = 'Настройка параметров организации';
  dsDocDocOrgEditEditCaption  = 'Редактирование параметров организации';
  dsDocDocOrgEditInsertCaption  = 'Добавление организации';

  dsDocDocOrgEditChLPUErrMsg  = 'Не указано ЛПУ';
  dsDocDocOrgEditChISubordErrMsg  = 'Не выбран вид подчинённости';
  dsDocDocOrgEditChIOrgCodeErrMsg  = 'Не введён код организации';
  dsDocDocOrgEditChIOrgCodeResErrMsg  = 'Введённый код организации является зарезервировынным,'#13#10'введите другой код.';
  dsDocDocOrgEditChIOrgCodeFormatErrMsg  = 'Введённый код организации не соответствует формату.'#13#10' Формат \"XXX.XXXXXXX.XXX\",'#13#10'где X - цифра от 0 до 9.'#13#10'Введите другой код.';
  dsDocDocOrgEditChIOrgShortNameErrMsg  = 'Не введено сокращённое наименование организации';
  dsDocDocOrgEditChIOrgFullNameErrMsg  = 'Не введено полное наименование организации';
  dsDocDocOrgEditChIAddrErrMsg  = 'Не введён адрес организации';
  dsDocDocOrgEditChIOrgExistErrMsg  = 'Организация с таким кодом уже существует, '#13#10' введите другой код.';

  dsDocDocOrgEditSubordLabCaption = 'Подчинённость:';
  dsDocDocOrgEditOrgCodeLabCaption = 'Код организации:';
  dsDocDocOrgEditNameLabCaption = 'Сокращённое наименование:';
  dsDocDocOrgEditAddrLabCaption = 'Адрес:';
  dsDocDocOrgEditEMailLabCaption = 'E-mail:';
  dsDocDocOrgEditPhoneLabCaption = 'Телефон:';
  dsDocDocOrgEditFullNameLabCaption = 'Полное наименование:';
  dsDocDocOrgEditOkBtnCaption = 'Применить';
  dsDocDocOrgEditCancelBtnCaption = 'Отмена';

  dsDocDocOrgEditSubordCBItem1 = 'Подчинённая';
  dsDocDocOrgEditSubordCBItem2 = 'Вышестоящая';
  dsDocDocOrgEditSubordCBItem3 = 'Своя';

//DocOrgList
  dsDocDocOrgListErrorInsertRecMsg  = 'Ошибка добавления записи';
  dsDocDocOrgListErrorEditRecMsg  = 'Ошибка модификации записи';
  dsDocDocOrgListErrorDelDocDef  = 'Существуют описания документов созданные данной организацией';
  dsDocDocOrgListErrorDelDoc  = 'Существуют документы сформированные данной организацией';
  dsDocDocOrgListOrgCaption  = 'организации';
  dsDocDocOrgListRecCaption  = 'записи';

  dsDocDocOrgListCaption = 'Подчиненные ЛПУ';
  dsDocDocOrgListLabel1Caption = 'Код организации';
  dsDocDocOrgListLabel2Caption = 'Подчинённость';
  dsDocDocOrgListLabel3Caption = 'Адрес';
  dsDocDocOrgListLabel4Caption = 'E-mail';
  dsDocDocOrgListLabel5Caption = 'Ф.И.О. руководителя';
  dsDocDocOrgListLabel7Caption = 'Сокращённое наименование';
  dsDocDocOrgListLabel8Caption = 'Телефон';

  dsDocDocOrgListactInsertCaption = 'Добавить';
  dsDocDocOrgListactInsertHint = 'Добавить';
  dsDocDocOrgListactEditCaption = 'Изменить';
  dsDocDocOrgListactEditHint = 'Изменить';
  dsDocDocOrgListactDeleteCaption = 'Удалить';
  dsDocDocOrgListactDeleteHint = 'Удалить';
  dsDocDocOrgListactSetTemplateCaption = 'Установить &шаблон';
  dsDocDocOrgListactSetTemplateHint = 'Установить шаблон';
  dsDocDocOrgListactReSetTemplateCaption = 'Сня&ть шаблон';
  dsDocDocOrgListactReSetTemplateHint = 'Снять шаблон';
  dsDocDocOrgListactRefreshCaption = 'Обновить';
  dsDocDocOrgListactRefreshHint = 'Обновить';
  dsDocDocOrgListactFindCaption = 'Найти...';
  dsDocDocOrgListactFindHint = 'Найти...';
  dsDocDocOrgListactFindNextCaption = 'Найти далее...';
  dsDocDocOrgListactFindNextHint = 'Найти далее...';

  dsDocDocOrgListMainTBMBar1Caption = 'Редактирование';
  dsDocDocOrgListMainTBMBar2Caption = 'Шаблон';
  dsDocDocOrgListClassGridColumnCaption = 'Полное наименование';

//DocPeriod

  dsDocDocPeriodErrorYearFormat  = 'Ошибочный формат года';
  dsDocDocPeriodErrorDateFormat  = 'Ошибочный формат даты';

//  dsDocDocPeriodCaption = 'Период формирования';
  dsDocDocPeriodCaptionLabCaption = 'Укажите период формирования докумета';
  dsDocDocPeriodLabCaption = 'Период:';
  dsDocDocPeriodDateFrLabCaption = 'От';
  dsDocDocPeriodDateToLabCaption = 'До';
  dsDocDocPeriodYearRBCaption = 'Год';
  dsDocDocPeriodQuarterRBCaption = 'Квартал';
  dsDocDocPeriodMonsRBCaption = 'Месяц';
  dsDocDocPeriodPeriodRBCaption = 'Интервал дат';
  dsDocDocPeriodHalfYearRBCaption = 'Полугодие';
  dsDocDocPeriodOkBtnCaption = 'Применить';
  dsDocDocPeriodCancelBtnCaption = 'Отмена';

  dsDocDocPeriodHalfYearCBItems1 = '1-е полугодие';
  dsDocDocPeriodHalfYearCBItems2 = '2-е полугодие';
  dsDocDocPeriodQuarterCBItems1 = '1-й квартал';
  dsDocDocPeriodQuarterCBItems2 = '2-й квартал';
  dsDocDocPeriodQuarterCBItems3 = '3-й квартал';
  dsDocDocPeriodQuarterCBItems4 = '4-й квартал';
  dsDocDocPeriodMonsCBItems01 = 'Январь';
  dsDocDocPeriodMonsCBItems02 = 'Февраль';
  dsDocDocPeriodMonsCBItems03 = 'Март';
  dsDocDocPeriodMonsCBItems04 = 'Апрель';
  dsDocDocPeriodMonsCBItems05 = 'Май';
  dsDocDocPeriodMonsCBItems06 = 'Июнь';
  dsDocDocPeriodMonsCBItems07 = 'Июль';
  dsDocDocPeriodMonsCBItems08 = 'Август';
  dsDocDocPeriodMonsCBItems09 = 'Сентябрь';
  dsDocDocPeriodMonsCBItems10 = 'Октябрь';
  dsDocDocPeriodMonsCBItems11 = 'Ноябрь';
  dsDocDocPeriodMonsCBItems12 = 'Декабрь';

//ExtFunc

  dsDocExtFuncDocPeriodLocStr1  = 'Год';
  dsDocExtFuncDocPeriodLocStr2  = 'Полугодие';
  dsDocExtFuncDocPeriodLocStr3  = 'Квартал';
  dsDocExtFuncDocPeriodLocStr4  = 'Месяц';
  dsDocExtFuncDocPeriodLocStr5  = 'Уникальная';
  dsDocExtFuncDocPeriodLocStr6  = 'Отсутствует';

  dsDocExtFuncDocPeriodValStrUniq1  = '%s.%s.%s г. - %s.%s.%s г.';
  dsDocExtFuncDocPeriodValStrUniq2  = 'с %s.%s.%s г.';
  dsDocExtFuncDocPeriodValStrUniq3  = 'до %s.%s.%s г.';

  dsDocExtFuncDocPeriodValStrMonth  = '%s %s г.';

  dsDocExtFuncDocPeriodValStrQuart1  = 'I квартал %s г.';
  dsDocExtFuncDocPeriodValStrQuart2  = 'II квартал %s г.';
  dsDocExtFuncDocPeriodValStrQuart3  = 'III квартал %s г.';
  dsDocExtFuncDocPeriodValStrQuart4  = 'IV квартал %s г.';

  dsDocExtFuncDocPeriodValStrHalfYear1  = 'I полугодие  %s г.';
  dsDocExtFuncDocPeriodValStrHalfYear2  = 'II полугодие %s г.';

  dsDocExtFuncDocPeriodValStrYear  = '%s г.';

  dsDocExtFuncErrorReadSettingCaption  = 'Ошибка чтения настроек';
  dsDocExtFuncErrorReadSettingMsg  = 'В параметрах настройки не заданы параметры ЛПУ';

  dsDocExtFuncCreateDocViewMsg  = 'Формирование отображения документа';
  dsDocExtFuncErrorCreateDocView  = 'Ошибка формирования просмотра (отсутствует описание документа)';

  dsDocExtFuncReplaceDocMsg  = 'Документ - "%s"'#13#10' за период: %s'#13#10' уже существует.'#13#10#13#10'Заменить существующий документ?';
  dsDocExtFuncReplaceDocCaption  = 'Подтверждение замены документа';
  dsDocExtFuncMissingDocDef  = 'Отсутствует описание документа, код описания "%s"';
  dsDocExtFuncNoDocDef  = 'Описание отсутствует';
  dsDocExtFuncFileVersionString  = 'StringFileInfo\041904E3\FileVersion';

  dsDocExtFuncRecCount1   = 'запись';
  dsDocExtFuncRecCount234 = 'записи';
  dsDocExtFuncRecCount    = 'записей';

//ExtSpecList

  dsDocExtSpecListAddDocDefCaption  = 'Список описаний документов';

  dsDocExtSpecListCaption = 'Список описаний документов';
  dsDocExtSpecListSpecCodeLabCaption = 'Код описания документа:';
  dsDocExtSpecListCreateDateLabCaption = '  Дата формирования:';
  dsDocExtSpecListAutorLabCaption = 'Автор описания:';
  dsDocExtSpecListExtSpecListColumn1Caption = 'Наименование описания';
  dsDocExtSpecListListTBMBar1Caption = 'Редактирование';

  dsDocExtSpecListactAddSpecCaption1  = 'Выбрать';
  dsDocExtSpecListactAddSpecCaption2  = 'Добавить';
  dsDocExtSpecListactDeleteSpecCaption1  = 'Отмена';
  dsDocExtSpecListactDeleteSpecCaption2  = 'Удалить';

//SpecEdit

  dsDocDocSpecEditEditCaption = 'Редактирование описания документа: "%s"';
  dsDocDocSpecEditNewCaption = 'Новое описание документа: "%s"';
  dsDocDocSpecEditSaveMsg = 'Описание документа не содержит ни одного элемента!'#13#10'Сохранение невозможно';
  dsDocDocSpecEditSaveCaption = 'Ошибка сохранения документа';
  dsDocDocSpecEditCancelWarn = 'В описание документа внесены изменения,'#13#10'Сохранить изменения?';

  dsDocDocSpecEditSaveBtnCaption = 'Сохранить';
  dsDocDocSpecEditCancelBtnCaption = 'Отмена';

//SpecList

  dsDocDocSpecListNoDefContent = 'Ошибочная структура описания документа!!!'#13#10'Отсутствует описание стуктуры документа';
  dsDocDocSpecListErrorFindSpec = 'Искали, но не смогли найти !';
  dsDocDocSpecListDocDefCaption = 'описания документа';
  dsDocDocSpecListErrorDelDocDef = 'Существуют документы сформированные по данному описанию,'#13#10'удаление невозможно.';
  dsDocDocSpecListErrorReplaceDocDef = 'Описание документа: "%s" уже существует!'#13#10'Существующее описание создано не Вами, заменить не возможно.'#13#10'Сохранить описание документа как копию ?';
  dsDocDocSpecListConfReplaceDocDef = 'Описание документа: "%s" уже существует!'#13#10'Заменить ?';
  dsDocDocSpecListCopyCaption = 'копия';
  dsDocDocSpecListErrorOpenFile = 'Ошибка открытия файла "%s"';
  dsDocDocSpecListErrorFileWork = 'Ошибка выполнения одной из файловых операций: Open, Seek, Read, Close.';
  dsDocDocSpecListOpenDlgMask = 'xml-файлы|*.xml';

  dsDocDocSpecListCaption = 'Список описаний документов';
  dsDocDocSpecListFilterParamLabCaption = 'Параметры отбора описаний';
  dsDocDocSpecListPeriodicityLabCaption = 'Периодичность:';
  dsDocDocSpecListOwnerLabCaption = 'Автор:';
  dsDocDocSpecListFilterPanelTopBtnHint = 'Скрыть панель параметров отбора описаний документов';
  dsDocDocSpecListFilterPanelBottomBtnHint = 'Показать панель параметров отбора описаний документов';
  dsDocDocSpecListResetBtnHint = 'Показать панель параметров отбора описаний документов';
  dsDocDocSpecListResetBtnCaption = 'Сбросить параметры отбора';
  dsDocDocSpecListSpecCodeLabCaption = 'Код описания документа:';
  dsDocDocSpecListCreateDateLabCaption = '  Дата формирования:';
  dsDocDocSpecListAutorLabCaption = 'Автор описания:';
  dsDocDocSpecListLabel1Caption = 'Код организации, автора описания:';
  dsDocDocSpecListactNewSpecCaption = 'Новое описание';
  dsDocDocSpecListactNewSpecHint = 'Создать новое описание ...';
  dsDocDocSpecListactEditSpecCaption = 'Редактировать описание';
  dsDocDocSpecListactEditSpecHint = 'Редактировать выбранное описание ...';
  dsDocDocSpecListactCopySpecCaption = 'Копировать описание';
  dsDocDocSpecListactCopySpecHint = 'Копировать выбранное описание ...';
  dsDocDocSpecListactDeleteSpecCaption = 'Удалить описание';
  dsDocDocSpecListactDeleteSpecHint = 'Удалить выбранное описание ...';
  dsDocDocSpecListactFindSpecCaption = 'Найти ...';
  dsDocDocSpecListactFindSpecHint = 'Найти описание документа ...';
  dsDocDocSpecListactCreateDocCaption = 'Формировать документ';
  dsDocDocSpecListactCreateDocHint = 'Формирование документа';
  dsDocDocSpecListactAboutCaption = 'Справка';
  dsDocDocSpecListactAboutHint = 'Справка';
  dsDocDocSpecListactHandCreateCaption = 'Ручной ввод документа';
  dsDocDocSpecListactHandCreateHint = 'Ручной ввод документа ...';
  dsDocDocSpecListactAddTemplateCaption = 'Добавить шаблон документа';
  dsDocDocSpecListactAddTemplateHint = 'Добавить шаблон документа';
  dsDocDocSpecListactClearTemplateCaption = 'Удалить шаблон документа';
  dsDocDocSpecListactClearTemplateHint = 'Удалить шаблон документа';
  dsDocDocSpecListactSaveToXMLCaption = 'Сохранить описание документа в файл';
  dsDocDocSpecListactSaveToXMLHint = 'Сохранить описание документа в файл';
  dsDocDocSpecListactLoadFromXMLCaption = 'Загрузить описание документа из файла';
  dsDocDocSpecListactLoadFromXMLHint = 'Загрузить описание документа из файла';

  dsDocDocSpecListNoDataSrc = 'Не задан источник данных';

  dsDocDocSpecListFltLabCaption = 'Фильтр: ';
  dsDocDocSpecListFltLabHint = 'Фильтр: ';

  dsDocDocSpecListPeriodicityCBItem1 = '';
  dsDocDocSpecListPeriodicityCBItem2 = 'Непериодические';
  dsDocDocSpecListPeriodicityCBItem3 = 'Периодические';

  dsDocDocSpecListDBTableView1NAMECaption = 'Наименование описания';
  dsDocDocSpecListDBTableView1F_PERIODCaption = 'Периодичность';

  dsDocDocSpecListStatusSBItem1Caption = 'Всего:';
  dsDocDocSpecListStatusSBItem2Caption = 'всего записей';
  dsDocDocSpecListStatusSBItem3Caption = 'По фильтру';
  dsDocDocSpecListStatusSBItem4Caption = 'по фильтру';

//DocExDFM

  dsDocDocDocExDFMPeriodicyty = 'Да';
  dsDocDocDocExDFMNoPeriodicyty = 'Нет';

//ChoiceList

  dsDocDocChoiceListConfReplaceChoice = 'Фильтр: "%s" уже существует!'#13#10'Заменить ?';

  dsDocDocChoiceListErrorAddChoiceRecMissing = 'Ошибка добавления фильтра (запись не найдена)';
  dsDocDocChoiceListErrorEditChoice = 'Ошибка модификации фильтра';
  dsDocDocChoiceListErrorAddChoice = 'Ошибка добавления фильтра';
  dsDocDocChoiceListChioceCaption = 'фильтра';

  dsDocDocChoiceListCaption = 'Библиотека фильтров';
  dsDocDocChoiceListFltListColumns1Caption = 'Наименование';
  dsDocDocChoiceListClassTBMBar1Caption = 'Редактирование';
  dsDocDocChoiceListactInsertCaption = 'Добавить';
  dsDocDocChoiceListactInsertHint = 'Добавить';
  dsDocDocChoiceListactEditCaption = 'Изменить';
  dsDocDocChoiceListactEditHint = 'Изменить';
  dsDocDocChoiceListactDeleteCaption = 'Удалить';
  dsDocDocChoiceListactDeleteHint = 'Удалить';

  dsDocDocChoiceListCountCaption = 'Всего:';

//Choice

  dsDocDocChoiceCaption = 'Формирование фильтра';
  dsDocDocChoiceOkBtnCaption = 'Применить';
  dsDocDocChoiceCancelBtnCaption = 'Отмена';

//ImportDoc

  dsDocDocImportDocFileFilter1 = 'Файлы данных|*.zxml|Файлы данных DOS|*.let';
  dsDocDocImportDocFileFilter2 = 'Файлы данных|*.zxml';
  dsDocDocImportDocSourceCaption = 'Источник: %s';
  dsDocDocImportDocLoadCaption = 'Загрузка файла импорта';
  dsDocDocImportDocOrgMissing = 'Отсутствует [%s]';
  dsDocDocImportDocImportDataDefFileMissing = 'Отсутствует файл описания импортируемых данных';
  dsDocDocImportDocImportFileMissing = 'Не загружен файл импорта';
  dsDocDocImportDocDocDefCaption = 'Описания документов ...';
  dsDocDocImportDocReplaceDocDefConf1 = 'Описание документа, код - "%s", уже существует.'#13#10#13#10'Старое наименование - "%s",'#13#10'Новое  наименование - "%s".'#13#10#13#10'Организация - "%s",'#13#10'Заменить существующее описание?';
  dsDocDocImportDocReplaceDocDefConf2 = 'Описание документа, наименование - "%s", уже существует.'#13#10#13#10'Старый код - "%s",'#13#10'Новый код - "%s".'#13#10#13#10'Организация - "%s",'#13#10'Заменить существующее описание?';
  dsDocDocImportDocDocCaption = 'Документы...';
  dsDocDocImportDocReplaceDocConf1 = 'Документ, код - "%s", уже существует.'#13#10''#13#10'Существующий документ:'#13#10'%s'#13#10'Добавляемый документ:'#13#10'%s'#13#10'Заменить существующий документ?';
  dsDocDocImportDocReplaceDocParam1 = ''#09'Наименование'#09'"%s"'#13#10#09'Организация'#09'"%s"'#13#10#09'Период'#09#09'"%s"'#13#10'';
  dsDocDocImportDocReplaceDocConf2 = 'Документ, наименование - "%s", уже существует.'#13#10#13#10'Существующий документ:'#13#10'%s'#13#10'Добавляемый документ:'#13#10'%s'#13#10'Заменить существующий документ?';
  dsDocDocImportDocReplaceDocParam2 = ''#09'Код'#09#09'"%s"'#13#10''#09'Организация'#09'"%s"'#13#10''#09'Период'#09#09'"%s"'#13#10'';

  dsDocDocImportDocBreakImportConf = 'Прервать приём данных?';

  dsDocDocImportDocCaption = 'Прием описаний документов, документов';
  dsDocDocImportDocSpecTSCaption = 'Описания документов';
  dsDocDocImportDocSpecNameCaption = 'Описание документа';
  dsDocDocImportDocSpecOwnerCaption = 'Автор';
  dsDocDocImportDocSpecPeridCaption = 'Периодичность';
  dsDocDocImportDocDocTSCaption = 'Документы';
  dsDocDocImportDocDocNameCaption = 'Документ';
  dsDocDocImportDocDocOwnerCaption = 'Автор';
  dsDocDocImportDocDocPeriodCaption = 'Период';
  dsDocDocImportDocOpenBtnCaption = 'Принять';
  dsDocDocImportDocApplyBtnCaption = 'Сохранить в БД';
  dsDocDocImportDocCancelBtnCaption = 'Закрыть';
  dsDocDocImportDocOperLabCaption = 'Нажмите кнопку "Принять" и укажите источник данных';

//ExportDoc

  dsDocDocExportDoc = '';
  dsDocDocExportStatusSBPanels2 = 'Отобрано:';

  dsDocDocExportDocFileFilter1 = 'Файлы данных|*.zxml|Файлы данных DOS|*.let';
  dsDocDocExportDocFileFilter2 = 'Файлы данных|*.zxml';
  dsDocDocExportDocDocDefCaption = 'Описания документов ...';
  dsDocDocExportDocDocCaption = 'Документы...';
  dsDocDocExportDocSaveCaption = 'Сохранение данных экспорта ...';
  dsDocDocExportDocBreakExportConf = 'Прервать передачу данных?';

  dsDocDocExportDocCaption = 'Передача описаний документов, документов';
  dsDocDocExportDocExpSpecTSCaption = 'Описания документов';
  dsDocDocExportDocLabel2Caption = 'Параметры отбора описаний';
  dsDocDocExportDocLabel6Caption = 'Периодичность:';
  dsDocDocExportDocLabel1Caption = 'Автор:';
  dsDocDocExportDocFilterPanelTopBtnHint = 'Скрыть панель параметров отбора описаний документов';
  dsDocDocExportDocFilterPanelBottomBtnHint = 'Показать панель параметров отбора описаний документов';
  dsDocDocExportDocResetBtnHint = 'Показать панель параметров отбора описаний документов';
  dsDocDocExportDocResetBtnCaption = 'Сбросить параметры отбора';
  dsDocDocExportDocSpecNameCaption = 'Описание документа';
  dsDocDocExportDocSpecPeridCaption = 'Периодичность';
  dsDocDocExportDocExpDocTSCaption = 'Документы';
  dsDocDocExportDocPeriodLabCaption = 'Период формирования:';
  dsDocDocExportDocLabel3Caption = 'Периодичность:';
  dsDocDocExportDocLabel7Caption = 'Автор документа:';
  dsDocDocExportDocLabel8Caption = 'Параметры отбора документов';
  dsDocDocExportDocDocFilterPanelBottomBtnHint = 'Показать панель параметров отбора документов';
  dsDocDocExportDocDocFilterPanelTopBtnHint = 'Скрыть панель параметров отбора документов';
  dsDocDocExportDocDocResetBtnHint = 'Показать панель параметров отбора описаний документов';
  dsDocDocExportDocDocResetBtnCaption = 'Сбросить параметры отбора';
  dsDocDocExportDocDocNameCaption = 'Документ';
  dsDocDocExportDocDocPeriodCaption = 'Период';
  dsDocDocExportDocSaveBtnCaption = 'Передать';
  dsDocDocExportDocCancelBtnCaption = 'Закрыть';


  dsDocDocExportDocPeriodicityCBItem1 = '';
  dsDocDocExportDocPeriodicityCBItem2 = 'Не периодические';
  dsDocDocExportDocPeriodicityCBItem3 = 'Периодические';

  dsDocDocExportDocDocPeriodicityCBItem1 = '';
  dsDocDocExportDocDocPeriodicityCBItem2 ='Отсутствует';
  dsDocDocExportDocDocPeriodicityCBItem3 ='Уникальная';
  dsDocDocExportDocDocPeriodicityCBItem4 ='Месяц';
  dsDocDocExportDocDocPeriodicityCBItem5 ='Квартал';
  dsDocDocExportDocDocPeriodicityCBItem6 ='Полугодие';
  dsDocDocExportDocDocPeriodicityCBItem7 ='Год';

// HandEdit

  dsDocMatrixErrorParam = 'Параметр должен быть';
  dsDocMatrixErrorIndex = 'Индекс %s выходит за границы массива.';
  dsDocHandEditErrorRow = 'строки';
  dsDocHandEditErrorCol = 'столбца';
  dsDocHandEditCheckRulesMsg = 'Данные ячейки (№ строки - %s; № столбца - %s)'+#13#10+'не удовлетворяют правилам контроля для %s № %s.'+#13#10+'Правила контроля: %s';
  dsDocHandEditCheckRulesCpt = 'Введенные данные не соответствуют правилам контроля.';

  dsDocHandEditStructHeaderCaption = '  Структура документа';
  dsDocHandEditmiDocCaption      = 'Документ';
  dsDocHandEditmiViewCaption     = 'Вид';
  dsDocHandEditmiJumpCaption     = 'Переход';

  dsDocHandEditSaveCaption = '&Сохранить';
  dsDocHandEditSaveHint = 'Сохранить';
  dsDocHandEditReCalcCaption = '&Пересчет вычисляемых значений';
  dsDocHandEditReCalcHint = 'Пересчет вычисляемых значений';
  dsDocHandEditFirstDocElementCaption = 'П&ервый элемент документа';
  dsDocHandEditFirstDocElementHint = 'Первый элемент документа';
  dsDocHandEditPrevDocElementCaption = 'П&редыдущий элемент документа';
  dsDocHandEditPrevDocElementHint = 'Предыдущий элемент документа';
  dsDocHandEditNextDocElementCaption = 'С&ледующий элемент документа';
  dsDocHandEditNextDocElementHint = 'Следующий элемент документа';
  dsDocHandEditLastDocElementCaption = 'П&оследний элемент документа';
  dsDocHandEditLastDocElementHint = 'Последний элемент документа';
  dsDocHandEditViewStructCaption = '&Структура документа';
  dsDocHandEditViewStructHint = 'Структура документа';
  dsDocHandEditViewDocElementInscCaption = '&Надпись над элементом документа';
  dsDocHandEditViewDocElementInscHint = 'Надпись над элементом документа';
  dsDocHandEditExitCaption = '&Закрыть';
  dsDocHandEditExitHint = 'Закрыть';


implementation

end.
