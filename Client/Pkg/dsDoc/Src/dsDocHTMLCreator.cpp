/*
  ����        - ICSDocViewer.cpp
  ������      - ��������������� (ICS)
  ��������    - ����������� ���������.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 18.10.2004
*/
//---------------------------------------------------------------------------

#include <basepch.h>

#pragma hdrstop

#include "dsDocHTMLCreator.h"

#include <math.h>
#include "DKAxeUtils.h"
#include "ICSDocSpec.h"
#include "ExtUtils.h"

#pragma package(smart_init)


//###########################################################################
//##                                                                       ##
//##                               TdsDocHTMLCreator                      ##
//##                                                                       ##
//###########################################################################

//***************************************************************************
//********************** ��������/�������� ���������� ***********************
//***************************************************************************

const UnicodeString icsDocViewerExNumTCellLTitle = "� �/�";
const UnicodeString icsDocViewerExErrorNoSpecDef = "������ ���� ������ ������������.";
const UnicodeString icsDocViewerExErrorGUINotValid = "��� ������������� ���������� ��������� �� ��������� � ��� ���������� �������������";

__fastcall TdsDocHTMLCreator::TdsDocHTMLCreator()
{
  FSpec         = NULL;
  FDoc          = NULL;
  FHTML         = "";
  FndHTML       = NULL;
  FXMLContainer = NULL;
  FListZeroValCheckCountOnly = true;

  __BorderWidth = "";
  __RowNumbers  = true;
  __ColNumbers  = true;
  __RowNum      = 1;
  __ColNum      = 1;
  __PBMax = 0;
  __PBPosition = 0;
}
//---------------------------------------------------------------------------
__fastcall TdsDocHTMLCreator::~TdsDocHTMLCreator()
{
  __DELETE_OBJ( FndHTML );
}
//---------------------------------------------------------------------------
//������������� ���� FndHTML

TTagNode* __fastcall TdsDocHTMLCreator::InitNDHTML( bool bCreateBodyTag )
{
  __DELETE_OBJ( FndHTML );
  FndHTML = new TTagNode;
  if ( bCreateBodyTag )
  {
    FndHTML->Name = "body";
    return FndHTML;
  }
  return NULL;
}
//---------------------------------------------------------------------------
//***************************************************************************
//********************** ��������������� ������ *****************************
//***************************************************************************
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
bool __fastcall TdsDocHTMLCreator::CheckIfTerminated()
{
  return false;
}
//---------------------------------------------------------------------------

//����������� ���'� ����� ��� �������� ������� ndInsText ( xml-��� "instext" ������������� )

UnicodeString __fastcall TdsDocHTMLCreator::GetInsTextStyle( TTagNode *ndInsText )
{
  UnicodeString Result;
  if ( ( Result = ndInsText->AV["style"] ) == "" )
    if ( ( Result = ndInsText->GetParent( "inscription" )->AV["style"] ) == "" )
      Result = ndInsText->GetRoot()->GetChildByName( "styles", true )->AV["defaultbstyle"];
  return Result;
}

//---------------------------------------------------------------------------

//����������� ������������ ��� �������� ������� ndInsText ( xml-��� "instext" ������������� )

UnicodeString __fastcall TdsDocHTMLCreator::GetInsTextAlign( TTagNode *ndInsText )
{
  UnicodeString StyleUID = GetInsTextStyle( ndInsText );
  TTagNode *ndStyles = ndInsText->GetRoot()->GetChildByName( "styles", true );
  return ndStyles->GetChildByAV( "style", "uid", StyleUID )->AV["align"];
}

//---------------------------------------------------------------------------

/*
 * ����������� ���'� ����� ������������� ��� ������ ������� ( xml-��� "table" ������������� ) ndCellTagNode
 *
 *
 * ���������:
 *   [in]  AStyleAttrName   - ��� ��������, ������������� ������������ ����� ( "hstyle" ��� "bstyle" )
 *   [in]  ndCellTagNode    - ������ ������� ( xml-��� "trow" ��� "tcol" ������������� )
 *   [in]  ndAltCellTagNode - �������������� ������ ������� ( xml-��� "trow" ��� "tcol" ������������� )
 *                            [default = NULL]
 *
 * ������������ ��������:
 *   ��� ����� ������������� ��� ������ ������� ( xml-��� "table" ������������� ) ndCellTagNode
 *
 */

UnicodeString __fastcall TdsDocHTMLCreator::GetTableCellStyle( UnicodeString AStyleAttrName, TTagNode *ndCellTagNode, TTagNode *ndAltCellTagNode )
{
  UnicodeString Result;
  if ( ( Result = ndCellTagNode->AV[AStyleAttrName] ) == "" )
    if ( !ndAltCellTagNode || ( ndAltCellTagNode && ( ( Result = ndAltCellTagNode->AV[AStyleAttrName] ) == "" ) ) )
      if ( ( Result = ndCellTagNode->GetParent( "table" )->AV[AStyleAttrName] ) == "" )
        Result = ndCellTagNode->GetRoot()->GetChildByName( "styles", true )->AV[( AStyleAttrName == "hstyle" )
                                                                                    ? "defaulthstyle"
                                                                                    : "defaultbstyle"
      ];
  return Result;
}

//---------------------------------------------------------------------------

/*
 * ����������� ���'� ����� ������������� ��� ������ ( xml-��� "list" ������������� ) ndList
 *
 *
 * ���������:
 *   [in]  AStyleAttrName - ��� ��������, ������������� ������������ ����� ( "hstyle" ��� "bstyle" )
 *   [in]  ndList         - ������ ( xml-��� "list" ������������� )
 *
 * ������������ ��������:
 *   ��� ����� ������������� ��� ������ ( xml-��� "list" ������������� ) ndList
 *
 */

UnicodeString __fastcall TdsDocHTMLCreator::GetListStyle( UnicodeString AStyleAttrName, TTagNode *ndList )
{
  UnicodeString Result;
  if ( ( Result = ndList->AV[AStyleAttrName] ) == "" )
    Result = ndList->GetRoot()->GetChildByName( "styles", true )->AV[( AStyleAttrName == "hstyle" )
                                                                           ? "defaulthstyle"
                                                                           : "defaultbstyle"
    ];
  return Result;
}

//---------------------------------------------------------------------------

/*
 * ����������� ���'� ����� ������������� ��� ������ ������ ( xml-��� "list" ������������� ) ndCellTagNode
 *
 *
 * ���������:
 *   [in]  AStyleAttrName   - ��� ��������, ������������� ������������ ����� ( "hstyle" ��� "bstyle" )
 *   [in]  ndCellTagNode    - ������ ������ ( xml-��� "list" ������������� )
 *
 * ������������ ��������:
 *   ��� ����� ������������� ��� ������ ������ ( xml-��� "list" ������������� ) ndCellTagNode
 *
 */

UnicodeString __fastcall TdsDocHTMLCreator::GetListCellStyle( UnicodeString AStyleAttrName, TTagNode *ndCellTagNode )
{
  UnicodeString Result = "";
  if      ( TICSDocSpec::IsListColSingle1( ndCellTagNode ) )
    Result = ndCellTagNode->AV[AStyleAttrName];
  else if ( TICSDocSpec::IsListColSingle2( ndCellTagNode ) )
    Result = ndCellTagNode->GetParent( "listcol" )->AV[AStyleAttrName];
  else if ( TICSDocSpec::IsListColGroup( ndCellTagNode ) )
    if ( ndCellTagNode->CmpName( "listcolgroup" ) )
    {
      if ( AStyleAttrName == "hstyle" )
        Result = ndCellTagNode->AV["style"];
    }
    else //listcol
      Result = ndCellTagNode->AV[AStyleAttrName];
  if ( Result == "" )
    Result = GetListStyle( AStyleAttrName, ndCellTagNode->GetParent( "list" ) );
  return Result;
}

//---------------------------------------------------------------------------

// ������������ �������������� �������� � ������ �������� ����������� ������� ��������
// ������������ ������������, ��� 0.0 ����� ������������ ��� 0

void __fastcall TdsDocHTMLCreator::NormalizeNumericalValue(UnicodeString &ACellText, const bool fShowZVal)
{
  try
  {
    const double cdblEps = 1E-8;
    double dblValue = DKStrToFloat(ACellText, '.');

    if (fabs(dblValue - 0.0) < cdblEps)
        ACellText = (fShowZVal) ? "0" : "";
  }
  catch(EConvertError&) {;}
}

//---------------------------------------------------------------------------

void __fastcall TdsDocHTMLCreator::HiMetricToPixels(long *pnWidth, long *pnHeight)
{
  // ��������� ��� ��������������
  const float nHiMetricPerInch = 25.4;

  HDC hDC = GetDC(0);

  // ���������� ���������� �������� � ����������
  // ����� �� ����������� � �� ���������
  int dpiX = GetDeviceCaps(hDC, LOGPIXELSX);
  int dpiY = GetDeviceCaps(hDC, LOGPIXELSY);

  // ��������� ��������������
  if ( pnWidth )
    *pnWidth  = *pnWidth * dpiX / nHiMetricPerInch;

  if ( pnHeight )
    *pnHeight = *pnHeight * dpiY / nHiMetricPerInch;

  ReleaseDC(0, hDC);
}

//---------------------------------------------------------------------------

//������������ ������ ��� ��������� ������� ������ ( xml-��� "list" ������������� )

UnicodeString __fastcall TdsDocHTMLCreator::GetListColHeaderText( TTagNode *ndListCol )
{
  UnicodeString HeaderText = "";
  if ( TICSDocSpec::IsListCol( ndListCol ) )
  {
    if ( TICSDocSpec::IsListColSingle2( ndListCol ) )
    {
      if ( ndListCol->AV["ref"] != "" )
        HeaderText = FXMLContainer->GetRefer( ndListCol->AV["ref"] )->AV["name"];
    }
    else
      HeaderText = ndListCol->AV["name"];
    if ( !HeaderText.Length())
      HeaderText = "&nbsp;";
  }
  return HeaderText;
}

//---------------------------------------------------------------------------

//������������ ������ ��� ��������� ������/������� ������� ( xml-��� "table" ������������� )

UnicodeString __fastcall TdsDocHTMLCreator::GetTableHeaderCellText( TTagNode *ndHeaderCell )
{
  UnicodeString HeaderText = ndHeaderCell->AV["name"];
  if (!HeaderText.Length())
    HeaderText = "&nbsp;";
  return HeaderText;
}

//---------------------------------------------------------------------------

/*
 * ������������ ������ ��� ������ � ������� ������ ������ ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in/out]  ndV       - ������� xml-��� "iv" ���������
 *   [in]      ndListCol - xml-��� ������������� ( "listcol", "listcolgroup" ��� "is_attr" ),
 *                         ��������������� �������� ������� ����������� ������
 *                         ( xml-��� "list" ������������� )
 *   [in]      bShowZVal - ������� "���������� ������� ��������"
 *
 * ������������ ��������:
 *   ����� ��� ������ � ������� ������/������� ������� ( xml-��� "table" ������������� )
 *
 */

UnicodeString __fastcall TdsDocHTMLCreator::GetDocListCellText( TTagNode* &ndV, const TTagNode *ndListCol, const bool bShowZVal )
{
  UnicodeString CellText = "";
  if ( TICSDocSpec::IsListColSingle1( ndListCol ) || TICSDocSpec::IsListColSingle2( ndListCol ) )
    CellText = ndV->AV["val"];
  else                        //������ �������� � ��������� "���������� �������"
  {
    int nChildColCount = ( const_cast<TTagNode*>(ndListCol)->CmpName( "listcolgroup" ) )
                         //��� listcolgroup
                         ? ndListCol->Count
                         //��� listcol( link_is( is_attr+ ) )
                         : GetTagNodeChildrenCount( const_cast<TTagNode*>(ndListCol)->GetFirstChild(), "is_attr", false );
    for ( int i = 0; i < nChildColCount; i++ )
    {
      if ( CellText != "" )
        CellText += " ";
      CellText += ndV->AV["val"];
      if ( i < nChildColCount - 1 )
        ndV = ndV->GetNext();
    }
  }
  //������ ����������� ������� ��������
  if ( !FListZeroValCheckCountOnly ||
       ( FListZeroValCheckCountOnly &&
         const_cast<TTagNode*>(ndListCol)->CmpName( "listcol" ) &&
         const_cast<TTagNode*>(ndListCol)->GetChildByName( "link_is" ) &&
         const_cast<TTagNode*>(ndListCol)->GetChildByName( "link_is" )->GetChildByName( "count" ) )
  )
    NormalizeNumericalValue(CellText, true/*bShowZVal*/);
  return CellText;
}

//---------------------------------------------------------------------------

/*
 * ����������� ������������� �������� ������ �����, ��������������� �������� ������
 * ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in]  ANode  - ������ xml-���������, � ������� �������������� ����� �������������
 *                  �������� ������
 *
 * ������������ ��������:
 *   ������������ �������� ������
 *
 */

long __fastcall TdsDocHTMLCreator::GetListColsMaxLevel( const TTagNode *ANode ) const
{
  long nLevel = 0;
  if ( TICSDocSpec::IsListColSingle( ANode ) )
  {
    int nNodeLevel = ANode->Level;
    if ( TICSDocSpec::IsListColSingle2( ANode ) )
      nNodeLevel--;
    if ( nNodeLevel > nLevel )
      nLevel = nNodeLevel;
  }
  TTagNode *ndChild = const_cast<TTagNode*>(ANode)->GetFirstChild();
  while( ndChild )
  {
    long nChildNodesMaxLevel = GetListColsMaxLevel( ndChild );
    if ( nChildNodesMaxLevel > nLevel )
      nLevel = nChildNodesMaxLevel;
    ndChild = ndChild->GetNext();
  }
  return nLevel;
}

//---------------------------------------------------------------------------

//����������� ������������� �������� ��� "����������" ��� ������������ �������������

int __fastcall TdsDocHTMLCreator::GetMaxPBForSpec()
{
  TTagNode* ndDocContent = FSpec->GetChildByName( "doccontent" );
  int nPBMax = ndDocContent->Count;
  TTagNode *ndDocElement = ndDocContent->GetFirstChild();
  while ( ndDocElement )
  {
    if      ( ndDocElement->CmpName("inscription") )
      nPBMax += ndDocElement->Count;
    else if ( ndDocElement->CmpName("list") )   
      nPBMax += CountTagNodes( ndDocElement, TICSDocSpec::IsListColSingle );
    else if ( ndDocElement->CmpName("table") )
    {
      nPBMax += CountTagNodes( ndDocElement->GetChildByName( "columns" ), "tcol" );
      nPBMax += CountTagNodes( ndDocElement->GetChildByName( "rows"    ), "trow" );
    }
    ndDocElement = ndDocElement->GetNext();
  }
  return nPBMax;
}

//---------------------------------------------------------------------------

//����������� ������������� �������� ��� "����������" ��� ������������ ���������

int __fastcall TdsDocHTMLCreator::GetMaxPBForDoc()
{
  int nPBMax = GetMaxPBForSpec();
  TTagNode *ndObj = FDoc->GetChildByName( "contens" )->GetFirstChild();
  while ( ndObj )
  {
    TTagNode *ndObjRef = FSpec->GetTagByUID( ndObj->AV["uidmain"] );
    if ( ndObjRef->CmpName( "list,table" ) )
      nPBMax += ndObj->Count;
    ndObj = ndObj->GetNext();
  }
  return nPBMax;
}

//---------------------------------------------------------------------------

//***************************************************************************
//********************** ��������������� ������ *****************************
//********************** ��� ������� HTML ***********************************
//***************************************************************************

//���������� � htmlParent ��������� html-���� "table"
//( ������������ ��������� ���� __BorderWidth ��� �������� ������� ����� � __Width ��� �������� ������ )

TTagNode* __fastcall TdsDocHTMLCreator::AddHTMLTableTag( TTagNode *htmlParent )
{
  TTagNode *htmlTableTag = htmlParent->AddChild( "table" );
  SetHTMLBoxWidthValues( htmlTableTag, __BorderWidth, __Width );
  htmlTableTag->AV["bordercolor"] = "black";
  htmlTableTag->AV["cellspacing"] = "0";
  htmlTableTag->AV["cellpadding"] = "0";
  return htmlTableTag;
}

//---------------------------------------------------------------------------

//������������ ����� � �������� �������� ��� ��������� ������������ ��� html-���� "div"

void __fastcall TdsDocHTMLCreator::GetHTML_DIVAlignAttrVal( UnicodeString AAlign, UnicodeString &AAttrName, UnicodeString &AAttrVal )
{
  AAttrName = "style";
  AAttrVal  = "vertical-align:top; text-align : " + AAlign; /*change kab 20.06.07 "vertical-align:top;" */
}

//---------------------------------------------------------------------------

/*
 * ������������ html-����� ��� �������������� ������ � ������������ � ���������
 * ������
 *
 *
 * ���������:
 *   [in]  htmlTag   - html-��� � ������� ( �������� <div>, <span>, <th>, <td> )
 *   [in]  AStyleUID - ��� ����� ������������� ��� ��������������
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������������� �����
 *
 *
 */

TTagNode* __fastcall TdsDocHTMLCreator::CreateHTMLTextFormat( TTagNode *htmlTag, UnicodeString AStyleUID )
{
  TTagNode *ndResult = htmlTag;
  TTagNode *ndStyles = FSpec->GetChildByName( "styles", true );
  TTagNode *ndStyle  = ndStyles->GetChildByAV( "style", "uid", AStyleUID );
  htmlTag->AV["class"] = AStyleUID;
  if ( ndStyle->CmpAV( "wordwrap", "0" ) )
   {
     if ( htmlTag->CmpName( "th,td" ) )
       htmlTag->AV["nowrap"] = "";
     else
       ndResult = htmlTag->AddChild( "nobr" );
   }
  return ndResult;
}

//---------------------------------------------------------------------------

/*
 * ��������� ������ ����� ��� ������ html-������� ( ������ ��� �������������� CSS )
 *
 *
 * ���������:
 *   [in]  htmlTableCell - html-���, ��������������� ������ html-�������
 *   [in]  ABorderWidth  - ������ �����
 *
 * ������������ ��������:
 *   true  - �������� ���������
 *   false - ��������� ����������� �� ����, ����� ���������� ������������� CSS
 *
 */

bool __fastcall TdsDocHTMLCreator::SetHTMLTableCellBorderWidth( TTagNode *htmlTableCell, UnicodeString ABorderWidth )
{
  htmlTableCell->AV["style"] = "border-width:" + ABorderWidth + "px";
  return true;
}

//---------------------------------------------------------------------------

bool __fastcall TdsDocHTMLCreator::SetHTMLBoxWidthValues( TTagNode *htmlTag, UnicodeString ABorderWidth, UnicodeString AWidth )
{
  UnicodeString AWidthHTMLVal = "";
  if ( AWidth != "" )
   {
     if ( AWidth.Pos( "%" ) )
       AWidthHTMLVal = AWidth;
     else
     {
       long nWidthPX = AWidth.ToInt();
       HiMetricToPixels( &nWidthPX, NULL );
       AWidthHTMLVal = IntToStr((int) nWidthPX ) + "px";
     }
   }
  UnicodeString AStyleAttrVal = "border-width:" + ABorderWidth + "px";
  if ( AWidthHTMLVal != "" )
    AStyleAttrVal += ";";// width:" + AWidthHTMLVal;
  htmlTag->AV["style"] = AStyleAttrVal;
  return true;
}

//---------------------------------------------------------------------------

/*
 * ������������ html-����� ��� �������������� ������ ������ html-�������,
 * ������������ ��� ��������� �����/��������
 *
 *
 * ���������:
 *   [in]  htmlNumTableCell - html-���, ��������������� ������ html-�������,
 *                            ������������ ��� ��������� �����/�������� ( html-��� "th" )
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������������� �����
 *
 *
 */

TTagNode* __fastcall TdsDocHTMLCreator::CreateHTMLTableNumCellTextFormat( TTagNode *htmlNumTableCell )
{
  TTagNode *ndResult = htmlNumTableCell;
  htmlNumTableCell->AV["class"] = "num";
  return ndResult;
}

//---------------------------------------------------------------------------

/*
 * ������������ �������� HTML-�����
 *
 *
 * ���������:
 *   [in]  AStyleClass          - ������������ ������ HTML-�����
 *   [in]  AStyleClassStrPrefix - ������� ����� ������������� ������ HTML-�����
 *   [in]  AStylePropStrPrefix  - ������� ����� ��������
 *   [in]  ndStyle              - xml-��� "style" �������������
 *
 * ������������ ��������:
 *   �������� HTML-����� � ���� ������
 *
 *
 */

UnicodeString __fastcall TdsDocHTMLCreator::GetHTMLStyleDefinition( UnicodeString  AStyleClass,
                                                             UnicodeString  AStyleClassStrPrefix,
                                                             UnicodeString  AStylePropStrPrefix,
                                                             TTagNode*         ndStyle )
{
  UnicodeString STYLE = AStyleClassStrPrefix + "." + AStyleClass + "\n";
  STYLE += AStyleClassStrPrefix + "{\n";
  STYLE += AStylePropStrPrefix  + "text-align     : "   + ndStyle->AV["align" ] + ";\n";
  STYLE += AStylePropStrPrefix  + "vertical-align:top;\n";        /*change kab 20.06.07 ������������ ������������*/
  STYLE += AStylePropStrPrefix  + "font-family    : \"" + ndStyle->AV["fontname"] + "\";\n";
//!!!  STYLE += AStylePropStrPrefix  + "font-size      : "   + ndStyle->AV["fontsize"] + "pt;\n";
  STYLE += AStylePropStrPrefix  + "text-decoration:";
  if ( ndStyle->CmpAV( "strike"   , "1" ) ||
       ndStyle->CmpAV( "underline", "1" )
  )
  {
    if ( ndStyle->CmpAV( "strike",    "1" ) ) STYLE += " line-through";
    if ( ndStyle->CmpAV( "underline", "1" ) ) STYLE += " underline";
  }
  else
    STYLE += " none";
  STYLE += ";\n";
  STYLE += AStylePropStrPrefix  + "font-weight    :";
  STYLE += ( ndStyle->CmpAV( "bold", "1" ) ) ? " bold" : " normal";
  STYLE += ";\n";
  STYLE += AStylePropStrPrefix  + "font-style     :";
  STYLE += ( ndStyle->CmpAV( "italic", "1" ) ) ? " italic" : " normal";
  STYLE += ";\n";
  STYLE += AStyleClassStrPrefix + "}\n";
  return STYLE;
}

//---------------------------------------------------------------------------

/*
 * ������������ ������� HTML-������
 *
 *
 * ���������:
 *   [in]  AStyleClassStrPrefix - ������� ����� ������������� ������ HTML-�����
 *   [in]  AStylePropStrPrefix  - ������� ����� ��������
 *   [in]  ndStyle              - xml-��� "styles" �������������
 *
 * ������������ ��������:
 *   �������� ������� HTML-������ � ���� ������
 *
 *
 */

UnicodeString __fastcall TdsDocHTMLCreator::GetHTMLSTYLESDefinition( UnicodeString  AStyleClassStrPrefix,
                                                              UnicodeString  AStylePropStrPrefix,
                                                              TTagNode*         ndStyles )
{
  UnicodeString STYLES = "<!-- Document style sheet -->\n"
                         "<style type='text/css'>\n"
                         "<!--\n";
  TTagNode* ndStyle = ndStyles->GetFirstChild();
  while ( ndStyle )
   {
     STYLES += GetHTMLStyleDefinition(ndStyle->AV["uid"], AStyleClassStrPrefix, AStylePropStrPrefix, ndStyle );
     ndStyle = ndStyle->GetNext();
   }
  UnicodeString cssPath = icsPrePath(GetEnvironmentVariable("APPDATA")+"\\ic\\css\\hand_edit_style.css");
  if (FileExists(cssPath))
   {
     TStringList *FlLoad = new TStringList;
     try
      {
         FlLoad->LoadFromFile(cssPath);
         STYLES += FlLoad->Text;
      }
     __finally
      {
        delete FlLoad;
      }
   }
  else
   {
   // td.num
     STYLES += "\n\
     table {\
           border-collapse:  collapse;                       \
           border: 1px solid #606A9D;                        \
           width: 90% \
   }                                                         \
   td {                                                      \
           border-collapse:  collapse;                       \
           border: 1px solid #99A0C1;                        \
           padding-right: 3px;                               \
           padding-left: 3px;                                \
           text-align: center;                               \
           vertical-align: middle;                           \
           font-weight: bold;                                \
           font-family: Verdana, Geneva, sans-serif;         \
           background-color: #F5FFF0;                        \
   }                                                         \
   th {                                                      \
           border-collapse:  collapse;                       \
           border: 1px solid #99A0C1;                        \
           padding-right: 3px;                               \
           padding-left: 3px;                                \
           text-align: center;                               \
           vertical-align: middle;                           \
           color: #000000;                                \
           font-weight: bold;                                \
           font-family: Verdana, Geneva, sans-serif;         \
           background-color: #F5FFF0;                        \
   }                                                         \
   th.num, {                                                      \
           border-collapse:  collapse;                       \
           border: 1px solid #99A0C1;                        \
           padding-right: 3px;                               \
           padding-left: 3px;                                \
           text-align: center;                               \
           vertical-align: middle;                           \
           font-weight: bold;                                \
           color: #FF0000;                                \
           font-family: Verdana, Geneva, sans-serif;         \
           background-color: #F5FFF0;                        \
   }                                                         \
   td input {                                                   \
           vertical-align: middle;                           \
           border: 1px solid #36F;                           \
           text-align: right;                                \
           font-family: Verdana, Geneva, sans-serif;         \
           width: 80%;                                      \
   }                                                         \
   span input {                                                   \
           vertical-align: middle;                           \
           border: 0px solid #F5FFF0;                           \
           background-color: #F5FFF0;                        \
           text-align: right;                                \
           font-family: Verdana, Geneva, sans-serif;         \
           width: 80%;                                      \
   }                                                         \
   .cLM {                                                    \
           text-align: left;                                 \
           vertical-align: middle;                           \
           font-weight: bold;                                \
           font-family: Verdana, Geneva, sans-serif;         \
           background-color: #DCE2ED;                        \
   }                                                         \
   .cCMW {                                                   \
           text-align: center;                               \
           vertical-align: middle;                           \
           color: white;                                     \
           font-weight: bold;                                \
           font-family: Verdana, Geneva, sans-serif;         \
           background-color: #DCE2ED;                        \
   }";
   }
  STYLES += "-->\n</style>";

  return STYLES;
}
//---------------------------------------------------------------------------

//������������ HTML-����� � ���� ������

void __fastcall TdsDocHTMLCreator::CreateHTML( TICSDocHTMLCreatorPreviewType APreviewType )
{
  UnicodeString TITLE  = "";
  UnicodeString META   = "<meta name='Generator' content='" + ClassName() + " V1'>\n"
                      "<meta name='Description' content='";
  switch ( APreviewType )
  {
    case ptSpecificatorEx:
      TITLE = FSpec->GetChildByName( "passport" )->AV["mainname"];
      META += "specificator view";
    break;

    case ptDocumentEx    :
      TITLE = FDoc->GetChildByName( "passport" )->AV["mainname"];
      META += "document view";
    break;
  }
  META += "'>";
  UnicodeString STYLES = "";
  META += "\n";
  STYLES += GetHTMLSTYLESDefinition( "\t", "\t  ", FSpec->GetChildByName("styles", true));
  try
   {
     FndHTML->OnProgressInit     = CreateHTMLProgressInit;
     FndHTML->OnProgress         = CreateHTMLProgressChange;
     FndHTML->OnProgressComplite = CreateHTMLProgressComplite;
     FHTML = FndHTML->AsHTML(TITLE, META + STYLES, false, 2, false);
   }
  __finally
   {
     FndHTML->OnProgressInit     = NULL;
     FndHTML->OnProgress         = NULL;
     FndHTML->OnProgressComplite = NULL;
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocHTMLCreator::CreateHTMLProgressInit(int AMax, UnicodeString AMsg)
{
//  InitPB();
    __PBMax = AMax;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocHTMLCreator::CreateHTMLProgressChange(int APercent, UnicodeString AMsg)
{
    __PBPosition = APercent;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocHTMLCreator::CreateHTMLProgressComplite(UnicodeString AMsg)
{
}
//---------------------------------------------------------------------------
/*
 * ������������ ����� ��� �������� ������ ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in]      ndListEl - xml-��� "listcont" �������������
 *   [in\out]  htmlList - ������ ( html-��� "table", ��������������� xml-���� "list" ������������� )
 *   [int]     htmlRow  - ������� ������ html-������� ( html-��� "tr" )
 *                        ������������ ��� ������ ������� ����� ������������ ��������, ��� ������
 *                        ������� ������ ���� NULL
 *                        [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *
 */
void __fastcall TdsDocHTMLCreator::CreateHTMLListHeaderCell( TTagNode *ndListEl, TTagNode *htmlList, TTagNode *htmlRow )
{
  if ( CheckIfTerminated() ) return;
  if ( TICSDocSpec::IsListCol( ndListEl ) )
  {
    TAttr *attUnion = TICSDocSpec::GetListColGroupUnion( ndListEl );

    //�������� ������-���������
    TTagNode *htmlTH = htmlRow->AddChild( "th" );
    //��������� ������ ����� � ������ �������
    if ( TICSDocSpec::IsListColSingle1(ndListEl) || TICSDocSpec::IsListColSingle2(ndListEl) )
      SetHTMLBoxWidthValues( htmlTH, __BorderWidth, TICSDocSpec::GetSizeAttrVal( ndListEl, "width" ) );
    else if ( attUnion && ( attUnion->Value == "1" ) )
    {
      int nWidth = 0;
      bool bNeedWidth = false;
      if ( TICSDocSpec::IsListColLinkISGroup( ndListEl ) )
      {
        UnicodeString AWidth = ndListEl->AV["width"];
        if ( AWidth != "" )
        {
          TStringList *sl = NULL;
          try
          {
            sl = new TStringList;
            sl->Text = StringReplace( AWidth, ";", "\n", TReplaceFlags() << rfReplaceAll );
            for ( int i = 0; i < sl->Count; i++ )
              if ( sl->Strings[i] != "" )
              {
                nWidth += sl->Strings[i].ToInt();
                bNeedWidth = true;
              }
          }
          __finally
          {
            if ( sl ) delete sl;
          }
        }
      }
      else
      {
        TTagNode *ndLCol = ndListEl->GetFirstChild();
        while( ndLCol )
        {
          UnicodeString AWidth = ndLCol->AV["width"];
          if ( AWidth != "" )
          {
            nWidth += AWidth.ToInt();
            bNeedWidth = true;
          }
          ndLCol = ndLCol->GetNext();
        }
      }

      if ( bNeedWidth )
        SetHTMLBoxWidthValues( htmlTH, __BorderWidth, IntToStr((int)nWidth) );
      else
        SetHTMLTableCellBorderWidth( htmlTH, __BorderWidth );
    }
    else
      SetHTMLTableCellBorderWidth( htmlTH, __BorderWidth );
    TTagNode *htmlTHData = CreateHTMLTextFormat( htmlTH, GetListCellStyle( "hstyle", ndListEl ) );
    TAttr *attPCDATA = htmlTHData->AddAttr("PCDATA" , GetListColHeaderText( ndListEl ) );
    attPCDATA->Ref = ndListEl;

    //����������� "rowspan" ��� ��������� ������
    if      ( TICSDocSpec::IsListColSingle( ndListEl ) )
    {
      int nListElLevel = ndListEl->Level;
      if ( TICSDocSpec::IsListColSingle2( ndListEl ) )
        nListElLevel--;
      int nListColMaxLevel = GetListColsMaxLevel( ndListEl->GetParent( "listcont" ) );
      int nMaxDepth = nListColMaxLevel - nListElLevel + 1;
      if ( nMaxDepth > 1 )
        htmlTH->AV["rowspan"] = IntToStr((int)nMaxDepth );
//      IncPB();
    }
    //����������� "colspan" ��� ��������� ������
    else if ( attUnion && ( attUnion->Value == "0" ) )
      htmlTH->AV["colspan"] = IntToStr((int)CountTagNodes( ndListEl, TICSDocSpec::IsListColSingle ) );
    if ( attUnion && ( attUnion->Value == "1" ) )
      return;
  }
  TTagNode *ndChild = ndListEl->GetFirstChild();
  while ( ndChild )
  {
    //���������� htmlRow ��� ������ ������� ������ ��� ����� ������ �������
    if ( TICSDocSpec::IsListFirstCol( ndChild ) )
    {
      int nRowInd = ndChild->Level - ndChild->GetParent( "listcont" )->Level;
      if ( TICSDocSpec::IsListColSingle2( ndChild ) )
        nRowInd--;
      int i = 0;
      TTagNode *htmlTR = htmlList->GetFirstChild();
      while ( htmlTR && ( ++i < nRowInd ) )
        htmlTR = htmlTR->GetNext();
      //����������� ����� ������ ��� ������������ ������������
      htmlRow = ( htmlTR ) ? htmlTR : htmlList->AddChild( "tr" );
    }
    CreateHTMLListHeaderCell( ndChild, htmlList, htmlRow );
    ndChild = ndChild->GetNext();
  }
}
//---------------------------------------------------------------------------
/*
 * ������������ ����� ��� �������� ������ ( xml-��� "list" ������������� )
 * � ����������� ������ "��������" ��������
 *
 *
 * ���������:
 *   [in]      ndListCont      - xml-��� "listcont" �������������
 *   [in\out]  htmlList        - ������ ( html-��� "table", ��������������� xml-���� "list" ������������� )
 *   [out]     AListSingleCols - ������ "��������" ��������
 *                               [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *     __RowNumbers
 *     __BorderWidth
 *
 */
void __fastcall TdsDocHTMLCreator::CreateHTMLListHeader( TTagNode *ndListCont, TTagNode *htmlList, TTagNodeList *AListSingleCols )
{
  if ( CheckIfTerminated() ) return;
  CreateHTMLListHeaderCell( ndListCont, htmlList );
  //���������� ��������� ��� ������� � �������� ����� ������
  if ( __RowNumbers )
  {
    TTagNode *htmlFirstRow = htmlList->GetFirstChild();
    TTagNode *htmlTH = htmlFirstRow->InsertChild( htmlFirstRow->GetFirstChild(), "th", true );
    SetHTMLTableCellBorderWidth( htmlTH, __BorderWidth );
    TTagNode *htmlTHData = CreateHTMLTextFormat( htmlTH, GetListStyle( "hstyle", ndListCont->GetParent( "list" ) ) );


    htmlTHData->AV["PCDATA"] = icsDocViewerExNumTCellLTitle;
    //����������� "rowspan" ��� ��������� ������
    int nMaxDepth = GetListColsMaxLevel( ndListCont ) - ndListCont->Level;
    if ( nMaxDepth > 1 )
      htmlTH->AV["rowspan"] = IntToStr((int)nMaxDepth );
  }
  TTagNodeList ListSingleCols;
  if ( AListSingleCols )
  {
    TICSDocSpec::GetListSingleColsList( ndListCont, &ListSingleCols );
    *AListSingleCols = ListSingleCols;
  }
}
//---------------------------------------------------------------------------
/*
 * ������������ ������ ��� ������ ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in]      AListSingleCols - ������ "��������" ��������
 *   [in\out]  htmlList        - ������ ( html-��� "table", ��������������� xml-���� "list" ������������� )
 *   [out]     ADataCells      - ������ html-����� "td" html-������� - ������ � �������
 *                               [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *
 */
void __fastcall TdsDocHTMLCreator::CreateHTMLListBodyRow( const TTagNodeList *AListSingleCols, TTagNode *htmlList, TTagNodeList *ADataCells )
{
  if ( ADataCells )
    ADataCells->clear();
  TTagNode *htmlRow = htmlList->AddChild( "tr" );
  //���������� ������ ��� ������ ������ ������
  if ( __RowNumbers )
  {
    TTagNode *htmlTD = htmlRow->AddChild( "td" );
    SetHTMLTableCellBorderWidth( htmlTD, __BorderWidth );
    TTagNode *htmlTDData = CreateHTMLTableNumCellTextFormat( htmlTD );
    TAttr *pcdata = htmlTDData->AddAttr( "PCDATA", "&nbsp;" );
    pcdata->Ref = NULL;
    if ( ADataCells )
      ADataCells->push_back( htmlTDData );
  }
  for ( TTagNodeList::const_iterator i = AListSingleCols->begin(); i != AListSingleCols->end(); i++ )
  {
    if ( CheckIfTerminated() ) return;
    TTagNode *htmlTD = htmlRow->AddChild( "td" );
    SetHTMLTableCellBorderWidth( htmlTD, __BorderWidth );
    TTagNode *htmlTDData = CreateHTMLTextFormat( htmlTD, GetListCellStyle( "bstyle", *i ) );
    TAttr *pcdata = htmlTDData->AddAttr( "PCDATA", "&nbsp;" );
    pcdata->Ref = *i;
    if ( ADataCells )
      ADataCells->push_back( htmlTDData );
  }
//  IncPB();
}

//---------------------------------------------------------------------------

/*
 * ������������ ������-��������� ��� ������� ( xml-��� "table" ������������� )
 * ( ������������� ����������� ��������� ����� html-�������, ���� ������� ���������� )
 *
 *
 * ���������:
 *   [in]  ACellTagNodeName - ��� �������������� xml-����� ( "tcol" ��� "trow" )
 *   [in]  ndCellTagNode    - xml-���, ��������������� ������-��������� �������
 *   [int] htmlRow          - ������� ������ html-������� ( html-��� "tr" )
 *
 * ������������ ��������:
 *   ������-��������� ( html-��� "th" ) �������
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *      __RowNum
 *
 */
TTagNode* __fastcall TdsDocHTMLCreator::CreateHTMLTableHeaderCell( UnicodeString  ACellTagNodeName,
                                                               TTagNode*         ndCellTagNode,
                                                               TTagNode*         htmlRow )
{
  if ( CheckIfTerminated() ) return NULL;
  TTagNode *htmlTH = NULL;
  if ( ndCellTagNode->CmpName( ACellTagNodeName ) )
  {
    //���������� ������ ��������� �������/������
    htmlTH = htmlRow->AddChild( "th" );

    //��������� ������ ����� � ������ �������
    if ( ( ACellTagNodeName == "tcol" ) && !ndCellTagNode->GetChildByName( "tcol" ) )
      SetHTMLBoxWidthValues( htmlTH, __BorderWidth, TICSDocSpec::GetSizeAttrVal( ndCellTagNode, "width" ) );
    else
      SetHTMLTableCellBorderWidth( htmlTH, __BorderWidth );
    TTagNode *htmlTHData = CreateHTMLTextFormat( htmlTH, GetTableCellStyle( "hstyle", ndCellTagNode ) );
    TAttr *attPCDATA = htmlTHData->AddAttr( "PCDATA", GetTableHeaderCellText( ndCellTagNode ) );
    attPCDATA->Ref = ndCellTagNode;
    //������������ ��� ���������� ����� ��� ����� ������� � ������� HTML
    ndCellTagNode->Ctrl = reinterpret_cast<TWinControl*>(htmlRow);
    //������ �������/�����
    if ( ndCellTagNode->GetChildByName( ACellTagNodeName ) )
    {
      int nTableSingleElCount;
      UnicodeString SpanAttrName;
      if ( ACellTagNodeName == "tcol" )
      {
        nTableSingleElCount = CountTagNodes( ndCellTagNode, TICSDocSpec::IsTableColSingle );
        SpanAttrName = "colspan";
      }
      else //trow
      {
        nTableSingleElCount = CountTagNodes( ndCellTagNode, TICSDocSpec::IsTableRowSingle );
        SpanAttrName = "rowspan";
      }
      htmlTH->AV[SpanAttrName] = IntToStr((int)nTableSingleElCount );
    }
    //�������/������
    else
    {
      UnicodeString ElTopParentName, SpanAttrName;
      if ( ACellTagNodeName == "tcol" )
      {
        ElTopParentName = "columns";
        SpanAttrName    = "rowspan";
      }
      else
      {
        ElTopParentName = "rows";
        SpanAttrName    = "colspan";
      }
      int nTableElMaxLevel = GetTagNodesMaxLevel( ndCellTagNode->GetParent( ElTopParentName ), ACellTagNodeName );
      int nMaxDepth = nTableElMaxLevel - ndCellTagNode->Level + 1;
      if ( nMaxDepth > 1 )
        htmlTH->AV[SpanAttrName] = IntToStr((int)nMaxDepth );
      //���������� ������ � ������� ������
      if ( ( ACellTagNodeName == "trow" ) && __RowNumbers )
      {
        TTagNode *htmlTH = htmlRow->AddChild( "th" );
        SetHTMLTableCellBorderWidth( htmlTH, __BorderWidth );
        TTagNode *htmlTHData = CreateHTMLTableNumCellTextFormat( htmlTH );
        UnicodeString S;
        int nSingleRowCount = CountTagNodes( ndCellTagNode->GetParent( "rows" ), TICSDocSpec::IsTableRowSingle );
        S.printf( ( "%0" + IntToStr((int) IntToStr( (int)nSingleRowCount ).Length() ) + "u" ).c_str(),
                  __RowNum++
        );
        htmlTHData->AV["PCDATA"] = S;
      }
    }
//    IncPB();
  }
  return htmlTH;
}
//---------------------------------------------------------------------------
/*
 * ������������ ����� ��� �������� ������� ( xml-��� "table" ������������� )
 *
 *
 * ���������:
 *   [in]      ndCol     - xml-��� "columns" �������������
 *   [in\out]  htmlTable - ������� ( html-��� "table", ��������������� xml-���� "table" ������������� )
 *   [int]     htmlRow   - ������� ������ html-������� ( html-��� "tr" )
 *                         ������������ ��� ������ ������� ����� ������������ ��������, ��� ������
 *                         ������� ������ ���� NULL
 *                         [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *      __ColNumbers
 *
 */
void __fastcall TdsDocHTMLCreator::CreateHTMLTableHeaderCols( TTagNode *ndCol, TTagNode *htmlTable, TTagNode *htmlRow )
{
  if ( CheckIfTerminated() ) return;
  CreateHTMLTableHeaderCell( "tcol", ndCol, htmlRow );
  TTagNode *ndChild = ndCol->GetChildByName( "tcol" );
  while ( ndChild && ndChild->CmpName( "tcol" ) )
  {
    //��� ������ ������� � ������ ( ��� ��� ����� ������ ������� )
    if ( TICSDocSpec::IsTableFirstCol( ndChild ) )
    {
      //��� ����� ������ �������
      if ( ndChild->GetParent()->CmpName( "columns" ) )
      {
        //���������� ����� ������ ��� HTML-�������
        htmlRow = htmlTable->AddChild( "tr" );
        //���������� ����� ����� ������� ������ ����� ������� ( ��� ������ �������� ������ )
        TTagNode *htmlTH = htmlRow->AddChild( "th" );
        SetHTMLTableCellBorderWidth( htmlTH, __BorderWidth );
        TTagNode *ndTableSection = ndChild->GetParent( "table" )->GetChildByName( "columns" );
        int nTableElMaxLevel = GetTagNodesMaxLevel( ndTableSection, "tcol" );
        htmlTH->AV["rowspan"] = nTableElMaxLevel - ndTableSection->Level + static_cast<int>(__ColNumbers);
        ndTableSection = ndChild->GetParent( "table" )->GetChildByName( "rows" );
        nTableElMaxLevel = GetTagNodesMaxLevel( ndTableSection, "trow" );
        htmlTH->AV["colspan"] = nTableElMaxLevel - ndTableSection->Level + static_cast<int>(__RowNumbers);
        htmlTH->AV["PCDATA"] = "&nbsp;";
      }
      //���������� htmlRow
      else
      {
        int nRowInd = ndChild->Level - ndChild->GetParent( "columns" )->Level;
        int i = 0;
        TTagNode *htmlTR = htmlTable->GetFirstChild();
        while ( htmlTR && ( ++i < nRowInd ) )
          htmlTR = htmlTR->GetNext();
        htmlRow = ( htmlTR ) ? htmlTR : htmlTable->AddChild( "tr" );
      }
    }
    CreateHTMLTableHeaderCols( ndChild, htmlTable, htmlRow );
    ndChild = ndChild->GetNext();
  }
}

//---------------------------------------------------------------------------

/*
 * ������������ ����� ��� �������� ������� ( xml-��� "table" ������������� ) � ���������� ��������
 *
 *
 * ���������:
 *   [in]      ndColumns         - xml-��� "columns" �������������
 *   [in\out]  htmlTable         - ������� ( html-��� "table", ��������������� xml-���� "table" ������������� )
 *   [out]     ATableSingleCols  - ������ "��������"-����� ����� ��� ��������
 *                                 ������� ( xml-��� "table" ������������� ), �.�. ����� �����,
 *                                 � ������� ��� ����������� ��� �������� �����-����������
 *                                 [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __ColNumbers
 *      __ColNum
 *
 */

void __fastcall TdsDocHTMLCreator::CreateHTMLTableCols( TTagNode *ndColumns, TTagNode *htmlTable, TTagNodeList *ATableSingleCols )
{
  if ( CheckIfTerminated() ) return;
  CreateHTMLTableHeaderCols( ndColumns, htmlTable );
  TTagNodeList TableSingleCols;
  TICSDocSpec::GetTableSingleColsList( ndColumns, &TableSingleCols );
  //���������� ����� � �������� ��������
  if ( __ColNumbers )
  {
    TTagNode *htmlRow = htmlTable->AddChild( "tr" );
    for ( TTagNodeList::const_iterator i = TableSingleCols.begin(); i != TableSingleCols.end(); i++ )
    {
      TTagNode *htmlTH = htmlRow->AddChild( "th" );
      SetHTMLTableCellBorderWidth( htmlTH, __BorderWidth );
      TTagNode *htmlTHData = CreateHTMLTableNumCellTextFormat( htmlTH );
      UnicodeString S;
      S.printf( ( "%0" + IntToStr ( (int)IntToStr( (int)TableSingleCols.size() ).Length() ) + "u" ).c_str(),
                __ColNum++
      );
      htmlTHData->AV["PCDATA"] = S;
    }
  }
  if ( ATableSingleCols )
    *ATableSingleCols = TableSingleCols;
}

//---------------------------------------------------------------------------

/*
 * ������������ ����� ��� ����� ������� ( xml-��� "table" ������������� )
 * � ����� ������ � ���� �������
 *
 *
 * ���������:
 *   [in]     ndRow            - xml-��� "rows" �������������
 *   [in\out] htmlTable        - ������� ( html-��� "table", ��������������� xml-���� "table" ������������� )
 *   [in]     ATableSingleCols - ������ "��������"-����� ����� ��� ��������
 *                               ������� ( xml-��� "table" ������������� ), �.�. ����� �����,
 *                               � ������� ��� ����������� ��� �������� �����-����������
 *   [out]    DataCells        - ������ ������� ( ���� 2-������� ������� ) ���������� �� ������
 *                               ������ �������
 *   [int]    htmlRow          - ������� ������ html-������� ( html-��� "tr" )
 *                               ������������ ��� ������ ������� ����� ������������ ��������, ��� ������
 *                               ������� ������ ���� NULL
 *                               [default = NULL]
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *
 */

void __fastcall TdsDocHTMLCreator::CreateHTMLTableRows( TTagNode *ndRow,
                                                    TTagNode *htmlTable,
                                                    const TTagNodeList &ATableSingleCols,
                                                    TTagNodeList2 *DataCells,
                                                    TTagNode *htmlRow )
{
  if ( CheckIfTerminated() ) return;
  if ( CreateHTMLTableHeaderCell( "trow", ndRow, htmlRow ) && !ndRow->GetChildByName( "trow" ) )
  {
    //���������� ����� ��� ������
    TTagNodeList DataCellStr;
    UnicodeString FEDName;
    FTabColNum = -1;
    for ( TTagNodeList::const_iterator i = ATableSingleCols.begin(); i != ATableSingleCols.end(); i++ )
    {
      FTabColNum++;
      TTagNode *htmlTD = htmlRow->AddChild( "td" );
      SetHTMLTableCellBorderWidth( htmlTD, __BorderWidth );
      TTagNode *htmlTDData = CreateHTMLTextFormat( htmlTD, GetTableCellStyle( "bstyle", *i, ndRow ) );
      TTagNode *htmlInputData = NULL;
      FEDName = IntToStr((int)FTabNum)+"."+IntToStr((int)FTabRowNum)+":"+IntToStr((int)FTabColNum)+"#";
      //name = Ntab.Nrow:NCol#(in|dis)%uid
      if ((*i)->GetChildByName("calcrules") || ndRow->GetChildByName("calcrules"))
       {
//         htmlTDData->AV["PCDATA"] = "&nbsp;";
         htmlInputData    = htmlTDData->AddChild("span");
         htmlInputData    = htmlInputData->AddChild("input");
         htmlInputData->AV["name"]  = FEDName+"dis%"+(*i)->AV["uid"];
         htmlInputData->AV["type"]  = "text";
         htmlInputData->AV["id"]    = htmlInputData->AV["name"];
         htmlInputData->AV["value"] = "0";
         htmlInputData->AV["size"]  = "11";
         htmlInputData->AV["maxlength"] = "8";
       }
      else
       {
         htmlInputData    = htmlTDData->AddChild("input");
         htmlInputData->AV["name"]  = FEDName+"in%"+(*i)->AV["uid"];
         htmlInputData->AV["type"]  = "text";
         htmlInputData->AV["id"]    = htmlInputData->AV["name"];
         htmlInputData->AV["value"] = "0";
         htmlInputData->AV["size"]  = "11";
         htmlInputData->AV["maxlength"] = "8";
       }

      if ( DataCells && htmlInputData )
        DataCellStr.push_back( htmlInputData );
    }
    if ( DataCells )
      DataCells->push_back( DataCellStr );
  }
  TTagNode *ndChild = ndRow->GetChildByName( "trow" );
  while ( ndChild && ndChild->CmpName( "trow" ) )
  {
    //���������� htmlRow
    if      ( !htmlRow || !TICSDocSpec::IsTableFirstRow( ndChild ) )
     {
       FTabRowNum++;
       htmlRow = htmlTable->AddChild( "tr" );
     }
    else if ( TICSDocSpec::IsTableFirstRow( ndChild ) )
      htmlRow = reinterpret_cast<TTagNode*>(ndChild->GetParent()->Ctrl);
    CreateHTMLTableRows( ndChild, htmlTable, ATableSingleCols, DataCells, htmlRow );
    ndChild = ndChild->GetNext();
  }
}

//---------------------------------------------------------------------------

/*
 * ������������ ����������� ������ ( xml-��� "list" ������������� )
 *
 *
 * ���������:
 *   [in]  ndList     - xml-��� "list" �������������
 *   [in]  htmlParent - html-��� - ������ ��� ��������������� html-������
 *   [in]  nRowCount  - ���������� ����� � ����������� ������
 *                      [default = 1]
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������ ( xml-��� "list" ������������� )
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __Width
 *      __RowNumbers
 *
 */

TTagNode* __fastcall TdsDocHTMLCreator::AddHTMLList( TTagNode *ndList, TTagNode *htmlParent, TTagNodeList *AListSingleCols, int nRowCount )
{
  __BorderWidth = ndList->AV["border"];
  __Width       = ndList->AV["width"]; 
  __RowNumbers  = ndList->CmpAV( "rownumbers", 1 );
  TTagNode *htmlList   = AddHTMLTableTag( htmlParent );
  TTagNode *ndListCont = ndList->GetChildByName( "listcont" );
  TTagNodeList ListSingleCols;
  CreateHTMLListHeader( ndListCont, htmlList, &ListSingleCols );
  for ( int i = 0; i < nRowCount; i++ )
  {
    if ( CheckIfTerminated() )
      break;
    CreateHTMLListBodyRow( &ListSingleCols, htmlList );
  }
  if ( AListSingleCols )
    *AListSingleCols = ListSingleCols;
  return htmlList;
}

//---------------------------------------------------------------------------

/*
 * ������������ ����������� ������� ( xml-��� "table" ������������� )
 *
 *
 * ���������:
 *   [in]  ndTable    - xml-��� "table" �������������
 *   [in]  htmlParent - html-��� - ������ ��� �������������� html-�������
 *   [out] DataCells  - ������ ������� ( ���� 2-������� ������� ) ���������� �� ������
 *                      ������ �������
 *                      [default = NULL]
 *
 * ������������ ��������:
 *   html-���, ��������������� ���������� ������� ( xml-��� "table" ������������� )
 *
 * ����������:
 *   ������������ ��������� ����:
 *      __BorderWidth
 *      __RowNumbers
 *      __ColNumbers
 *      __RowNum
 *      __ColNum
 *
 */

TTagNode* __fastcall TdsDocHTMLCreator::AddHTMLTable( TTagNode *ndTable, TTagNode *htmlParent, TTagNodeList2 *DataCells )
{
  __BorderWidth = ndTable->AV["border"];
  __Width       = ndTable->AV["width"];
  __RowNumbers  = ndTable->CmpAV( "rownumbers", 1 );
  __ColNumbers  = ndTable->CmpAV( "colnumbers", 1 );
  __RowNum = 1;
  __ColNum = 1;


  TTagNode *htmlTable    = AddHTMLTableTag( htmlParent );
  TTagNodeList TableSingleCols;
  CreateHTMLTableCols   ( ndTable->GetChildByName( "columns" ), htmlTable, &TableSingleCols );
  if ( DataCells )
    DataCells->clear();
  FTabRowNum = -1;
  CreateHTMLTableRows   ( ndTable->GetChildByName( "rows" )   , htmlTable, TableSingleCols, DataCells );
  FTabNum++;
  return htmlTable;
}

//---------------------------------------------------------------------------

/*
 * ������������ ����������� ������� ( xml-��� "inscription" ������������� )
 *
 *
 * ���������:
 *   [in]  ndInscription - xml-��� "inscription" �������������
 *   [in]  htmlParent    - html-��� - ������ ��� ��������������� �����������
 *
 * ������������ ��������:
 *   html-���, ��������������� ����������� ������� ( xml-��� "inscription" ������������� )
 *
 */

TTagNode* __fastcall TdsDocHTMLCreator::AddHTMLInscription( TTagNode *ndInscription, TTagNode *htmlParent )
{
  if ( ndInscription->CmpAV( "newline", "1" ) )
    htmlParent->AddChild( "br" );
  return htmlParent->AddChild( "div" );
}

//---------------------------------------------------------------------------

/*
 * ������������ ����������� �������� ������� ( xml-��� "instext" ������������� )
 *
 *
 * ���������:
 *   [in]     ndInsText  - xml-��� "instext" �������������
 *   [in\out] htmlParent - html-��� - ������ ��� ��������������� �����������
 *
 * ������������ ��������:
 *   html-���, ��������������� ����������� �������� ������� ( xml-��� "instext" ������������� )
 *
 */

TTagNode* __fastcall TdsDocHTMLCreator::AddHTMLInsText( TTagNode *ndInsText, TTagNode **htmlParent )
{
  if ( ndInsText->CmpAV( "newline", "1" ) )
    *htmlParent = (*htmlParent)->AddChild( "div" );

  UnicodeString AlignAttrName, AlignAttrVal;
  GetHTML_DIVAlignAttrVal( GetInsTextAlign( ndInsText ), AlignAttrName, AlignAttrVal );
  (*htmlParent)->AV[AlignAttrName] = AlignAttrVal;

  TTagNode *htmlInsText = (*htmlParent)->AddChild( "span" );
  return CreateHTMLTextFormat( htmlInsText, GetInsTextStyle( ndInsText ) );
}

//---------------------------------------------------------------------------

//������������ ����������� ������������� � ������� HTML

void __fastcall TdsDocHTMLCreator::CreateSpecHTML()
{
  CreateHTMLProgressInit(GetMaxPBForSpec(),"");
  TTagNode *htmlBodyRoot = InitNDHTML();
  TTagNode* ndDocElement = FSpec->GetChildByName( "doccontent" )->GetFirstChild();
  while( ndDocElement )
  {
    if ( CheckIfTerminated() ) return;
    if      ( ndDocElement->CmpName("inscription") )             //�������
    {
      TTagNode* htmlInsTextCurParent = AddHTMLInscription( ndDocElement, htmlBodyRoot );
      TTagNode* ndInsText = ndDocElement->GetFirstChild();
      while ( ndInsText )                                       //������� �������
      {
        if ( CheckIfTerminated() ) return;
        TTagNode *htmlInsText = AddHTMLInsText( ndInsText, &htmlInsTextCurParent );
        UnicodeString InsViewText = TICSDocSpec::GetInsTagViewText( ndInsText, FXMLContainer );
        if ( InsViewText != "� � � � � � �  � � � � � � �  -  � � � � � � � �  � �  � � � � � � � � � � �")
          htmlInsText->AV["PCDATA"] = InsViewText;
        ndInsText = ndInsText->GetNext();
//        IncPB();
      }
    }
    else if ( ndDocElement->CmpName("list")        )             //������
      AddHTMLList( ndDocElement, htmlBodyRoot );
    else if ( ndDocElement->CmpName("table")       )             //�������
      AddHTMLTable( ndDocElement, htmlBodyRoot );
    htmlBodyRoot->AddChild( "br" );
    ndDocElement = ndDocElement->GetNext();
//    IncPB();
  }
  CreateHTML( ptSpecificatorEx );
}

//---------------------------------------------------------------------------

//������������ ����������� ��������� � ������� HTML

void __fastcall TdsDocHTMLCreator::CreateDocHTML()
{
  CreateHTMLProgressInit(GetMaxPBForDoc(),"");
  TTagNode *htmlBodyRoot = InitNDHTML();
  TTagNode* ndObj = FDoc->GetChildByName( "contens" )->GetFirstChild();
  while( ndObj )
  {
    if ( CheckIfTerminated() ) return;
    //��������� ����� "lv" ������� ������ - ��������� ���������
    TTagNode *ndObjRef = FSpec->GetTagByUID( ndObj->AV["uidmain"] );
    if      ( ndObjRef->CmpName("inscription") )                 //�������
    {
      TTagNode* htmlInsTextCurParent = AddHTMLInscription( ndObjRef, htmlBodyRoot );
      TTagNode *ndInsElement = ndObj->GetFirstChild();
      while ( ndInsElement )
      {
        if ( CheckIfTerminated() ) return;
        //��������� ����� "lv" ������� ������ - ��������� �������
        TTagNode *ndInsElementRef = FSpec->GetTagByUID( ndInsElement->AV["uidmain"] );
        TTagNode *htmlInsText = AddHTMLInsText( ndInsElementRef, &htmlInsTextCurParent );
        UnicodeString InsViewText = ndInsElementRef->AV["value"];
        UnicodeString RefText = ndInsElement->GetFirstChild()->AV["val"];
        UnicodeString CntClcRlsText = ndInsElement->GetFirstChild()->GetNext()->AV["val"];
        if ( RefText != "" )
        {
          if ( InsViewText != "" )
            InsViewText += " ";
          InsViewText += RefText;
        }
        if ( CntClcRlsText != "" )
        {
          if ( InsViewText != "" )
            InsViewText += " ";
          InsViewText += CntClcRlsText;
        }
        if ( InsViewText != "" )
          htmlInsText->AV["PCDATA"] = InsViewText;
        ndInsElement = ndInsElement->GetNext();
//        IncPB();
      }
    }
    else if ( ndObjRef->CmpName("list")        )                 //������
    {
      TTagNodeList ListSingleCols;
      TTagNode *htmlList = AddHTMLList( ndObjRef, htmlBodyRoot, &ListSingleCols, 0 );
      TTagNode *ndListRow = ndObj->GetFirstChild();
      int nRowNo = 1;
      while ( ndListRow )
      {
        if ( CheckIfTerminated() ) return;
        //��������� ����� "lv" ������� ������ - ����� ������
        TTagNodeList DataCells;
        CreateHTMLListBodyRow( &ListSingleCols, htmlList, &DataCells );
        TTagNodeList::iterator itListCell = DataCells.begin();
        //���������� ������ ������� ������
        if ( __RowNumbers )
        {
          UnicodeString RowNo = IntToStr( (int)nRowNo );
          RowNo = UnicodeString::StringOfChar( '0', IntToStr( (int)ndObj->Count ).Length() - RowNo.Length() ) + RowNo;
          (*itListCell)->AV["PCDATA"] = RowNo;
          itListCell++;
        }
        TTagNode *ndListData = ndListRow->GetFirstChild();
        while ( ndListData )
        {
          if ( CheckIfTerminated() ) return;
          //��������� ����� "lv", "iv" �������� ������ - ����� ������� ������ ������
          if ( ndListData->CmpName( "iv" ) )   //������� �������� �������� ��� ������� ����������
          {                                    //��� ��������� ��������
            TTagNode *ndListCol = reinterpret_cast<TTagNode*>((*itListCell)->GetAttrByName( "PCDATA" )->Ref);
            UnicodeString CellText = GetDocListCellText( ndListData, ndListCol, ndObjRef->CmpAV( "showzval", "1" ) );
            if ( CellText != "" )
              (*itListCell)->AV["PCDATA"] = CellText;
          }
          else                          //�������� ��������� ��������
          {
            TTagNodeList::iterator itListCellInit = itListCell;
            TTagNode *ndListLinkISRows = ndListData->GetFirstChild();
            if (ndListLinkISRows)
             {
               while ( ndListLinkISRows )
               {
                 if ( CheckIfTerminated() ) return;
                 //��������� ����� "lv" ���������� ������ - ������ ��������� ��������� ��������
                 itListCell = itListCellInit;
                 TTagNode *ndListLinkISData = ndListLinkISRows->GetFirstChild();
                 while ( ndListLinkISData )
                 {
                   if ( CheckIfTerminated() ) return;
                   //��������� ����� "iv" ������ ������ - ������ ����� ��������� ��������� ��������
                   TAttr *attPCDATA = (*itListCell)->GetAttrByName( "PCDATA" );
                   TTagNode *ndListCol = reinterpret_cast<TTagNode*>(attPCDATA->Ref);
                   UnicodeString CellText = GetDocListCellText( ndListLinkISData, ndListCol, ndObjRef->CmpAV( "showzval", "1" ) );
   //                if ( CellText != "" )
   //                { /*BUGFIX kab 20.06.07 - ���������� ������ �������� � ������*/
                     attPCDATA->Value = "";
                     if ( (*itListCell)->Count )
                      {
                       (*itListCell)->AddChild( "br" );
                      }

                     (*itListCell)->AddChild( "span")->AV["PCDATA"] = CellText;
   //                }
                   itListCell++;
                   ndListLinkISData = ndListLinkISData->GetNext();
                 }
                 ndListLinkISRows = ndListLinkISRows->GetNext();
               }
             }
            else
             {
               TTagNode *ndListLinkISRef = FSpec->GetTagByUID( ndListData->AV["uidmain"] );
               if (ndListLinkISRef)
                {
                  if ( CheckIfTerminated() ) return;
                  //��������� ����� "lv" ���������� ������ - ������ ��������� ��������� ��������
                  itListCell = itListCellInit;
                  TTagNode *ndListISAttrRef = ndListLinkISRef->GetFirstChild();
                  if (ndListISAttrRef) ndListISAttrRef = ndListISAttrRef->GetFirstChild();
                  while (ndListISAttrRef)
                   {
                     if ( CheckIfTerminated() ) return;
                     if (ndListISAttrRef->CmpName("is_attr"))
                      {
                        //��������� ����� "iv" ������ ������ - ������ ����� ��������� ��������� ��������
                        TAttr *attPCDATA = (*itListCell)->GetAttrByName( "PCDATA" );
                        attPCDATA->Value = "";
                        (*itListCell)->AddChild( "span", "PCDATA=");
                        itListCell++;
                      }
                     ndListISAttrRef = ndListISAttrRef->GetNext();
                   }
                }
             }
            if ( itListCell != itListCellInit )
              itListCell--;
          }
          itListCell++;
          ndListData = ndListData->GetNext();
        }
        ndListRow = ndListRow->GetNext();
        nRowNo++;
      }
    }
    else if ( ndObjRef->CmpName("table")       )                 //�������
    {
      TTagNodeList2 DataCells;
      AddHTMLTable( ndObjRef, htmlBodyRoot, &DataCells );
      // ����������� �������� �� ���������
      TTagNode *ndTableRow = ndObj->GetFirstChild();
      TTagNodeList2::iterator itDataCells = DataCells.begin();
      while ( ndTableRow )
      {
        if ( CheckIfTerminated() ) return;
        //��������� ����� "lv" ������� ������ - ����� �������
        TTagNode *ndTableData = ndTableRow->GetFirstChild();
        TTagNodeList::iterator itDataCellStr = (*itDataCells).begin();
        while( ndTableData )
        {
          if ( CheckIfTerminated() ) return;
          //��������� ����� "iv" - ����� ������ �������
          UnicodeString CellText = ndTableData->AV["val"];
          //������ ����������� ������� ��������
          NormalizeNumericalValue(CellText, true/*ndObjRef->CmpAV("showzval", "1")*/);
//          if ( CellText != "" )
//            (*itDataCellStr)->AV["PCDATA"] = CellText;
//          if ( CellText != "" )
          (*itDataCellStr)->AV["value"] = CellText;
          ndTableData = ndTableData->GetNext();
          itDataCellStr++;
        }
        ndTableRow = ndTableRow->GetNext();
        itDataCells++;
//        IncPB();
      }
    }
    htmlBodyRoot->AddChild( "br" );
    htmlBodyRoot->AddChild( "span","id=sss,name=sss");
    ndObj = ndObj->GetNext();
//    IncPB();
  }
  CreateHTML( ptDocumentEx );
}

//---------------------------------------------------------------------------

//***************************************************************************
//********************** �������� ������ ************************************
//***************************************************************************

/*
 * ������������ ����������� ���������/������������� � ������ ������� ���������������� ���������
 *
 *
 * ���������:
 *   [in]  APreviewType - ��� ������� ��� �����������
 *   [in]  APreviewType - "���������"
 *                        [default = NULL]
 *
 */

UnicodeString __fastcall TdsDocHTMLCreator::Preview(TAxeXMLContainer*  AXMLContainer, TTagNode*  ASpecificator, TTagNode*  ADocument)
{
  UnicodeString RC = "";
  try
   {
     //�������������
   //  InitPB();
     FTabNum = 0;          //������� ����� �������

     //������������ �����������
     FXMLContainer = AXMLContainer;
     FSpec         = ASpecificator;
     FDoc          = ADocument;
     if (!FSpec) throw DKClasses::EMethodError(__FUNC__, icsDocViewerExErrorNoSpecDef);
     if (FDoc)
      { //ptDocumentEx
        if ( FDoc->GetChildByName( "passport" )->AV["GUIspecificator"].UpperCase() != FSpec->GetChildByName( "passport" )->AV["GUI"].UpperCase())
          throw DKClasses::EMethodError(__FUNC__, icsDocViewerExErrorGUINotValid);
        CreateDocHTML();
      }
     else
      { // ptSpecificatorEx
        CreateSpecHTML();
      }
     RC = FHTML;
   }
  __finally
   {
   }
  return RC;
}

//---------------------------------------------------------------------------

