//---------------------------------------------------------------------------
#include <vcl.h>
//#include <SysUtils.hpp>
#pragma hdrstop

#include "dsDocSpecPackEditUnit.h"
#include "msgdef.h"
#include "dsDocClientConstDef.h"
#include "dsDocPackPeriodUnit.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma link "cxButtons"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma resource "*.dfm"

TdsDocSpecPackEditForm *dsDocSpecPackEditForm;
//---------------------------------------------------------------------------
__fastcall TdsDocSpecPackEditForm::TdsDocSpecPackEditForm(TComponent* Owner, UnicodeString ACaption, UnicodeString AOwnerCode, /*FIBQuery *AQuery,*/ bool AEdit, TdsDocDM *ADM)
        : TForm(Owner)
{
  FDM = ADM;
  FEdit  = AEdit;
//  FQuery = AQuery;
  FSpecOwner = AOwnerCode;
  if (!FSpecOwner.Length()) return;
  Caption = ACaption;
  FPackDef = NULL;
  ListTBM->Bars->Items[0]->Caption = FMT(dsDocExtSpecListListTBMBar1Caption);
  FPackDef = new TTagNode(NULL);
  FPackModif = false;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecPackEditForm::FSetPackDef(TTagNode *ANode)
{
  FPackDef->Assign(ANode, true);
  TcxTreeListNode *tmp;
  try
   {
     CurTL->BeginUpdate();
     ComTL->BeginUpdate();
     // ��������� ����� ������ ��������
     try
      {
        ComTL->Clear();
        TJSONObject *RetData;
        TJSONObject *tmpPair;
        TJSONString *tmpData;
        UnicodeString FClCode, FClStr1, FClStr2;
        RetData = FDM->kabCDSGetValById("doc.s3.Select GUI as ps1, NAME as ps2, F_PERIOD as ps3 From SPECIFIKATORS Where SPECTYPE <> 10  and Upper(GUI) <> '000-UNITS-001' Order By F_PERIOD, Name", 0);
        TJSONPairEnumerator *itPair = RetData->GetEnumerator();
        while (itPair->MoveNext())
         {
           FClCode = ((TJSONString*)itPair->Current->JsonString)->Value();
           FClStr1 = ((TJSONString*)itPair->Current->JsonValue)->Value();
           FClStr2 = GetRPartB(FClStr1,':');
           FClStr1 = GetLPartB(FClStr1,':');

           tmp = ComTL->Root->AddChild();
           tmp->Texts[0] = FClStr1;
           tmp->Texts[1] = "0";
           tmp->Texts[2] = FClCode;
           tmp->Texts[3] = FClStr2;
         }
      }
     __finally
      {
      }
     CurTL->Clear();
     if (FEdit)
      {
        TTagNode *tmpNode;
        tmpNode = FPackDef->GetChildByName("passport");
        if (tmpNode)
         PackNameED->Text = tmpNode->AV["mainname"];

        tmpNode = FPackDef->GetChildByName("doccontent");
        if (tmpNode)
         {
           TcxTreeListNode *specEdNode;
           tmpNode = tmpNode->GetFirstChild();
           while (tmpNode)
            {
              specEdNode = CurTL->Root->AddChild();
              specEdNode->Texts[0] = tmpNode->AV["name"];
              specEdNode->Texts[1] = tmpNode->AV["typestr"];
              specEdNode->Texts[2] = tmpNode->AV["stypestr"];
              specEdNode->Texts[3] = tmpNode->AV["dfrom"];
              specEdNode->Texts[4] = tmpNode->AV["dto"];
              specEdNode->Texts[5] = tmpNode->AV["offset"];
              specEdNode->Texts[6] = tmpNode->AV["createtypestr"];

              specEdNode->Texts[7] = tmpNode->AV["spec_gui"];
              specEdNode->Texts[8] = tmpNode->AV["type"];
              specEdNode->Texts[9] = tmpNode->AV["stype"];
              specEdNode->Texts[10] = tmpNode->AV["createtype"];

              tmpNode = tmpNode->GetNext();
            }
         }
      }
   }
  __finally
   {
     CurTL->EndUpdate();
     ComTL->EndUpdate();
//     if (FQuery->Transaction->Active) FQuery->Transaction->Commit();
   }
  FPackModif = false;
}
//---------------------------------------------------------------------------
bool __fastcall TdsDocSpecPackEditForm::CanSpecDocRules(UnicodeString AGUI)
{
  bool RC = false;
  TTagNode *FSpec = new TTagNode(NULL);
  try
   {
     int FType;
     if (FDM->GetSpec(AGUI, FSpec, FType,""))
      {
        RC = FSpec->GetChildByName("docrules", true);
      }
   }
  __finally
   {
     delete FSpec;
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecPackEditForm::CurTLDragOver(TObject *Sender,
      TObject *Source, int X, int Y, TDragState State, bool &Accept)
{
  Accept = (((TcxTreeList*)Source)->Name == "ComTL");
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecPackEditForm::CurTLDragDrop(TObject *Sender,
      TObject *Source, int X, int Y)
{
  if (((TcxTreeList*)Source)->Name == "ComTL")
   actAddSpecExecute(actAddSpec);
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecPackEditForm::ComTLDragOver(TObject *Sender,
      TObject *Source, int X, int Y, TDragState State, bool &Accept)
{
  Accept = (((TcxTreeList*)Source)->Name == "CurTL");
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecPackEditForm::ComTLDragDrop(TObject *Sender,
      TObject *Source, int X, int Y)
{
  if (((TcxTreeList*)Source)->Name == "CurTL")
   actDeleteSpecExecute(actDeleteSpec);
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecPackEditForm::actAddSpecExecute(TObject *Sender)
{
  if (ComTL->SelectionCount)
   {
     int FType = 0;
     int FSubType = 0;
     int FCreateType = 0;

     UnicodeString FTypeStr = "";
     UnicodeString FSubTypeStr = "";
     UnicodeString FCreateTypeStr = "";

     UnicodeString FYear = "";

     UnicodeString FDateFr = "";
     UnicodeString FDateTo = "";

     UnicodeString FOffset = "";

     TcxTreeListNode *FSrc = ComTL->Selections[0];
     bool FCont = true;
     if (FSrc->Texts[3].ToIntDef(0))
      {
        TdsDocPackPeriodForm *Dlg = new TdsDocPackPeriodForm(this, CanSpecDocRules(FSrc->Texts[2]));
        try
         {
           Dlg->ShowModal();
           if (Dlg->ModalResult == mrOk)
            {
              FType       = Dlg->Type;
              FSubType    = Dlg->SubType;
              FCreateType = Dlg->CreateType;
              FTypeStr       = Dlg->TypeStr;
              FSubTypeStr    = Dlg->SubTypeStr;
              FCreateTypeStr = Dlg->CreateTypeStr;
              FYear   = Dlg->Year;
              FDateFr = Dlg->DateFr;
              FDateTo = Dlg->DateTo;
              FOffset = Dlg->Offset;
            }
           else
            FCont = false;
         }
        __finally
         {
           delete Dlg;
         }
      }
     if (FCont)
      {
        TcxTreeListNode *FDest = CurTL->Root->AddChild();
        FPackModif = true;

        FDest->Texts[0] = FSrc->Texts[0];
        FDest->Texts[1] = FTypeStr;
        FDest->Texts[2] = FSubTypeStr;
        if (FType == 6) FDest->Texts[3] = FDateFr;
        else            FDest->Texts[3] = FYear;
        FDest->Texts[4] = FDateTo;
        FDest->Texts[5] = FOffset;
        FDest->Texts[6] = FCreateTypeStr;

        FDest->Texts[7]  = FSrc->Texts[2];
        FDest->Texts[8]  = IntToStr(FType);
        FDest->Texts[9]  = IntToStr(FSubType);
        FDest->Texts[10] = IntToStr(FCreateType);
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecPackEditForm::actDeleteSpecExecute(TObject *Sender)
{
  if (CurTL->SelectionCount)
   {
     if (_MSG_QUE("������� �������� �� ������?","�������������") == ID_YES)
      {
        FPackModif = true;
        CurTL->Selections[0]->Delete();
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecPackEditForm::actApplyExecute(TObject *Sender)
{
  bool FCont = true;
  if (FPackModif)
   {
     if (!PackNameED->Text.Trim().Length())
      {
        _MSG_ERR("�� ������� ������������ ������","������");
        PackNameED->SetFocus();
        FCont = false;
      }
     if (!CurTL->Root->Count)
      {
        _MSG_ERR("� ������ ������ ���� �� ����� ������ ��������","������");
        CurTL->SetFocus();
        FCont = false;
      }
     if (FCont)
      FSave();
   }
  if (FCont)
   ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecPackEditForm::FSave()
{
  TTagNode *tmpNode;
  TTagNode *ContentNode;
  TTagNode *PassNode;
  if (FEdit)
   {
     PassNode = FPackDef->GetChildByName("passport");
     ContentNode = FPackDef->GetChildByName("doccontent");
   }
  else
   {
     FPackDef->Name = "specificator";
     PassNode = FPackDef->AddChild("passport");
     PassNode->AV["autor"] = FSpecOwner;
     PassNode->AV["defversion"] = "3.0.0";
     PassNode->AV["dosperioddef"] = "no";
     PassNode->AV["GUI"] = NewGUI();
     PassNode->AV["mainname"] = PackNameED->Text.Trim();
     PassNode->AV["orientation"] = "group";
     PassNode->AV["release"] = "0";
     PassNode->AV["save_bd"] = "0";
     PassNode->AV["timestamp"] = Date().FormatString("yyyymmdd")+Time().FormatString("hhnnss");
     PassNode->AV["version"] = "1";
     ContentNode = FPackDef->AddChild("doccontent");
   }
  ContentNode->DeleteChild();
  bool FPeriodisity = false;
  TcxTreeListNode *specEdNode;
  for (int i = 0; i < CurTL->Root->Count; i++)
   {
     specEdNode = CurTL->Root->Items[i];
     FPeriodisity |= specEdNode->Texts[8].ToIntDef(0);
     tmpNode = ContentNode->AddChild("spec");
     tmpNode->AV["name"]          = specEdNode->Texts[0];
     tmpNode->AV["typestr"]       = specEdNode->Texts[1];
     tmpNode->AV["stypestr"]      = specEdNode->Texts[2];
     tmpNode->AV["dfrom"]         = specEdNode->Texts[3];
     tmpNode->AV["dto"]           = specEdNode->Texts[4];
     tmpNode->AV["offset"]        = specEdNode->Texts[5];
     tmpNode->AV["createtypestr"] = specEdNode->Texts[6];

     tmpNode->AV["spec_gui"]   = specEdNode->Texts[7];
     tmpNode->AV["type"]       = specEdNode->Texts[8];
     tmpNode->AV["stype"]      = specEdNode->Texts[9];
     tmpNode->AV["createtype"] = specEdNode->Texts[10];
   }
  PassNode->AV["perioddef"] = IntToStr((int)FPeriodisity);
  FPackModif = false;
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecPackEditForm::actCancelExecute(TObject *Sender)
{
  ModalResult = mrCancel;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecPackEditForm::FormDestroy(TObject *Sender)
{
  delete FPackDef;
  FPackDef = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecPackEditForm::PackNameEDPropertiesChange(
      TObject *Sender)
{
  FPackModif = true;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocSpecPackEditForm::FormCloseQuery(TObject *Sender,
      bool &CanClose)
{
  if (FPackModif)
   {
     if (_MSG_QUE("��������� ���������?","�������������") == ID_YES)
      {
        FPackModif = false;
      }
     else
      CanClose = true;
   }
  else
   CanClose = true;
}
//---------------------------------------------------------------------------

