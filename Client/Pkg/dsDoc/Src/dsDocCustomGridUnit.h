//---------------------------------------------------------------------------
#ifndef dsDocCustomGridUnitH
#define dsDocCustomGridUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
//#include "ServerClientClasses.h"
//#include "dsRegED.h"
//#include "icsRegConstDef.h"
//#include "dsRegTypeDef.h"
//---------------------------------------------------------------------------
//#include "AppOptions.h"
//#include "ExtUtils.h"
//#include "ICSNomenklator.h"
//#include "Preview.h"
#include "XMLContainer.h"
//#include "AppOptions.h"
//#include "ICSNomenklator.h"
//#include "KLADR.h"
//#include "icsDocExEventsDef.h"
//#include "DemoMessage.h"
//#include "dsDocDocPeriodUnit.h"

#include "cxGridTableView.hpp"
//---------------------------------------------------------------------------
class TicsGridData : public TObject
{
private:
  TList *FRecords;
  TList *FDataRecords;
  int __fastcall FGetCount();
  TTagNode* __fastcall FGetValues(int AIdx);
  TObject*  __fastcall FGetData(int AIdx);
  void      __fastcall FSetData(int AIdx, TObject* AData);
public:
  __fastcall TicsGridData();
  __fastcall ~TicsGridData();
  TTagNode* __fastcall Add();
  void __fastcall Clear();
  __property TList *Records = {read=FRecords};
  __property int Count = {read=FGetCount};
  __property TTagNode* Values[int AIdx] = {read=FGetValues};
  __property TObject* Objects[int AIdx] = {read=FGetData, write=FSetData};
};
//---------------------------------------------------------------------------
class TicsGridDataSource  : public TcxCustomDataSource
{
private:
  TicsGridData *FDataOwner;
  TcxGridItemDataBinding* __fastcall GetDataBinding(int AItemIndex);

protected:
  bool    __fastcall GetInfoForCompare(void * ARecordHandle, void * AItemHandle, char * &PValueBuffer);
  void*   __fastcall GetItemHandle(int AItemIndex);
  int     __fastcall GetRecordCount();
  Variant __fastcall GetValue(void * ARecordHandle, void * AItemHandle);
  void    __fastcall SetValue(void * ARecordHandle, void * AItemHandle, const Variant &AValue);
  bool    __fastcall IsNativeCompare();
public:
  static void __fastcall SetColumnID(TcxGridTableView *AView);
  __fastcall TicsGridDataSource(TicsGridData *ADataOwner);
};
//---------------------------------------------------------------------------
#endif

