object dsDocHandEditForm: TdsDocHandEditForm
  Left = 461
  Top = 148
  VertScrollBar.Smooth = True
  VertScrollBar.Tracking = True
  Caption = 'dsDocHandEditForm'
  ClientHeight = 737
  ClientWidth = 1079
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object RightScrollPanel: TPanel
    Left = 894
    Top = 54
    Width = 185
    Height = 642
    Align = alRight
    BevelOuter = bvNone
    Color = clWhite
    TabOrder = 0
  end
  object ErrPanel: TPanel
    Left = 323
    Top = 2
    Width = 750
    Height = 46
    BevelOuter = bvNone
    BorderWidth = 5
    Color = 10813439
    TabOrder = 1
    object ErrMsgLab: TLabel
      Left = 5
      Top = 27
      Width = 740
      Height = 14
      Align = alBottom
      Caption = 'ErrMsgLab'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Transparent = True
      ExplicitWidth = 52
    end
    object Panel3: TPanel
      Left = 5
      Top = 5
      Width = 740
      Height = 22
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object ErrRulesLab: TLabel
        Left = 159
        Top = 0
        Width = 581
        Height = 22
        Align = alClient
        Caption = 'ErrRulesLab'
        Color = clInfoBk
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        Transparent = True
        ExplicitWidth = 96
        ExplicitHeight = 19
      end
      object Label1: TLabel
        Left = 0
        Top = 0
        Width = 159
        Height = 22
        Align = alLeft
        Caption = #1055#1088#1072#1074#1080#1083#1072' '#1082#1086#1085#1090#1088#1086#1083#1103': '
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExplicitHeight = 19
      end
    end
  end
  object DocViewerPanel: TPanel
    Left = 0
    Top = 54
    Width = 894
    Height = 642
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 3
    Color = clWindow
    TabOrder = 2
    object TabViewerPanel: TPanel
      Left = 3
      Top = 81
      Width = 888
      Height = 558
      Align = alClient
      BevelOuter = bvNone
      Color = clWhite
      TabOrder = 0
      object ColumnPanel: TPanel
        Left = 0
        Top = 26
        Width = 888
        Height = 58
        Align = alTop
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 0
        object ColView: THTMLViewer
          Left = 180
          Top = 0
          Width = 692
          Height = 58
          Cursor = crHandPoint
          ViewImages = False
          TabOrder = 0
          Align = alClient
          DefBackground = clWindow
          BorderStyle = htNone
          HistoryMaxCount = 6
          DefFontName = 'Times New Roman'
          DefPreFontName = 'Courier New'
          DefFontSize = 10
          DefFontColor = clWindowText
          DefOverLinkColor = clFuchsia
          ImageCacheCount = 6
          NoSelect = True
          ScrollBars = ssNone
          CharSet = DEFAULT_CHARSET
          MarginHeight = 0
          MarginWidth = 0
          PrintMarginLeft = 1.000000000000000000
          PrintMarginRight = 1.000000000000000000
          PrintMarginTop = 1.000000000000000000
          PrintMarginBottom = 1.000000000000000000
          PrintScale = 1.000000000000000000
          htOptions = [htNoLinkUnderline, htNoLinkHilite]
          OnKeyDown = FormKeyDown
        end
        object EmptyPanel: TPanel
          Left = 0
          Top = 0
          Width = 180
          Height = 58
          Align = alLeft
          BevelOuter = bvNone
          Color = clWhite
          TabOrder = 1
        end
        object HPanel: TPanel
          Left = 872
          Top = 0
          Width = 16
          Height = 58
          Align = alRight
          BevelOuter = bvNone
          Color = clWhite
          TabOrder = 2
        end
      end
      object ViewerClientPanel: TPanel
        Left = 0
        Top = 84
        Width = 888
        Height = 474
        Align = alClient
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        object ViewerSizePanel: TPanel
          Left = 0
          Top = 0
          Width = 888
          Height = 458
          Align = alTop
          BevelOuter = bvNone
          Color = clWhite
          TabOrder = 0
          object FDocView: THTMLViewer
            Left = 330
            Top = 0
            Width = 44
            Height = 44
            Cursor = crHandPoint
            ViewImages = False
            TabOrder = 2
            DefBackground = clWindow
            BorderStyle = htNone
            Visible = False
            HistoryMaxCount = 6
            DefFontName = 'Times New Roman'
            DefPreFontName = 'Courier New'
            DefFontSize = 10
            DefFontColor = clWindowText
            DefHotSpotColor = clBlack
            DefOverLinkColor = clFuchsia
            ImageCacheCount = 6
            NoSelect = True
            CharSet = DEFAULT_CHARSET
            MarginHeight = 0
            MarginWidth = 0
            PrintMarginLeft = 1.000000000000000000
            PrintMarginRight = 1.000000000000000000
            PrintMarginTop = 1.000000000000000000
            PrintMarginBottom = 1.000000000000000000
            PrintScale = 1.000000000000000000
            htOptions = [htNoLinkUnderline, htPrintTableBackground, htPrintBackground, htNoLinkHilite]
            OnKeyDown = FormKeyDown
          end
          object ViewerPanel: TPanel
            Left = 331
            Top = 0
            Width = 557
            Height = 458
            Align = alClient
            BevelOuter = bvNone
            Caption = 'ViewerPanel'
            TabOrder = 0
            object TopSizePanel: TPanel
              Left = 0
              Top = 0
              Width = 557
              Height = 1
              Align = alTop
              Color = clWhite
              TabOrder = 0
            end
            object TabEdit: TcxTreeList
              Left = 0
              Top = 1
              Width = 557
              Height = 457
              BorderStyle = cxcbsNone
              Align = alClient
              Bands = <
                item
                end>
              DefaultRowHeight = 35
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              LookAndFeel.Kind = lfStandard
              LookAndFeel.NativeStyle = True
              Navigator.Buttons.CustomButtons = <>
              OptionsBehavior.CellHints = True
              OptionsBehavior.FocusCellOnCycle = True
              OptionsSelection.HideFocusRect = False
              OptionsSelection.InvertSelect = False
              OptionsView.Buttons = False
              OptionsView.ExtPaintStyle = True
              OptionsView.GridLineColor = clSkyBlue
              OptionsView.GridLines = tlglBoth
              OptionsView.Headers = False
              OptionsView.Indicator = True
              OptionsView.IndicatorWidth = 17
              OptionsView.ShowRoot = False
              OptionsView.TreeLineColor = clBackground
              ParentFont = False
              Styles.OnGetContentStyle = TabEditStylesGetContentStyle
              Styles.HotTrack = cxStyle1
              TabOrder = 1
              OnEditing = TabEditEditing
              OnEditValueChanged = TabEditEditValueChanged
              OnFocusedColumnChanged = TabEditFocusedColumnChanged
              OnFocusedNodeChanged = TabEditFocusedNodeChanged
              OnKeyDown = FormKeyDown
              OnLeftPosChanged = TabEditLeftPosChanged
              OnTopRecordIndexChanged = TabEditTopRecordIndexChanged
              Data = {
                00000500850000000F00000044617461436F6E74726F6C6C6572310100000012
                000000546378537472696E6756616C75655479706502000000445855464D5401
                445855464D5401020000000000000008000000000000000000FFFFFFFFFFFFFF
                FFFFFFFFFF0100000008000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF1A
                0802000000}
              object cxTreeList1cxTreeListColumn1: TcxTreeListColumn
                PropertiesClassName = 'TcxLabelProperties'
                DataBinding.ValueType = 'String'
                Options.ShowEditButtons = eisbAlways
                Position.ColIndex = 0
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
            end
            object FED: TcxMaskEdit
              Left = 24
              Top = 40
              ParentFont = False
              Properties.HideSelection = False
              Properties.MaskKind = emkRegExprEx
              Properties.EditMask = '(\d{1,15})([\.|\,]\d{1,3})?'
              Properties.MaxLength = 0
              Style.BorderStyle = ebs3D
              Style.Color = 14215888
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -13
              Style.Font.Name = 'MS Sans Serif'
              Style.Font.Style = []
              Style.Shadow = True
              Style.IsFontAssigned = True
              TabOrder = 2
              Visible = False
              Width = 100
            end
          end
          object RowPanel: TPanel
            Left = 0
            Top = 0
            Width = 331
            Height = 458
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object VPanel: TPanel
              Left = 0
              Top = 441
              Width = 331
              Height = 17
              Align = alBottom
              BevelOuter = bvNone
              Color = clWhite
              TabOrder = 0
            end
            object RowView: THTMLViewer
              Left = 0
              Top = 0
              Width = 331
              Height = 441
              Cursor = crHandPoint
              TabOrder = 1
              Align = alClient
              DefBackground = clWindow
              BorderStyle = htNone
              HistoryMaxCount = 6
              DefFontName = 'Times New Roman'
              DefPreFontName = 'Courier New'
              DefFontSize = 10
              DefFontColor = clWindowText
              DefOverLinkColor = clFuchsia
              ImageCacheCount = 6
              NoSelect = True
              ScrollBars = ssNone
              CharSet = DEFAULT_CHARSET
              MarginHeight = 0
              MarginWidth = 0
              PrintMarginLeft = 1.000000000000000000
              PrintMarginRight = 1.000000000000000000
              PrintMarginTop = 1.000000000000000000
              PrintMarginBottom = 1.000000000000000000
              PrintScale = 1.000000000000000000
              htOptions = [htNoLinkUnderline, htNoLinkHilite]
              OnKeyDown = FormKeyDown
            end
          end
        end
      end
      object CurRowColPanel: TPanel
        Left = 0
        Top = 0
        Width = 888
        Height = 26
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 3
        Color = 14215888
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        object CurColRowLab: TLabel
          Left = 3
          Top = 3
          Width = 882
          Height = 20
          Align = alClient
          Alignment = taCenter
          AutoSize = False
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
      end
    end
    object FDocViewInsc: THTMLViewer
      Left = 3
      Top = 3
      Width = 888
      Height = 78
      Cursor = crHandPoint
      TabOrder = 1
      Align = alTop
      DefBackground = clWindow
      BorderStyle = htNone
      HistoryMaxCount = 6
      DefFontName = 'Times New Roman'
      DefPreFontName = 'Courier New'
      DefFontSize = 10
      DefFontColor = clWindowText
      DefOverLinkColor = clFuchsia
      ImageCacheCount = 6
      NoSelect = True
      ScrollBars = ssNone
      CharSet = DEFAULT_CHARSET
      MarginHeight = 0
      MarginWidth = 0
      PrintMarginLeft = 1.000000000000000000
      PrintMarginRight = 1.000000000000000000
      PrintMarginTop = 1.000000000000000000
      PrintMarginBottom = 1.000000000000000000
      PrintScale = 1.000000000000000000
      htOptions = [htNoLinkUnderline, htNoLinkHilite]
      OnKeyDown = FormKeyDown
    end
  end
  object CRPanel: TPanel
    Left = 0
    Top = 696
    Width = 1079
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 3
    Color = clWhite
    TabOrder = 7
    object RowCalcRulesLab: TLabel
      Left = 3
      Top = 3
      Width = 1073
      Height = 16
      Align = alTop
      Caption = 'RowCalcRulesLab'
      Color = clBackground
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      ExplicitWidth = 128
    end
    object ColCalcRulesLab: TLabel
      Left = 3
      Top = 22
      Width = 1073
      Height = 16
      Align = alBottom
      Caption = 'CalcRulesLab'
      Color = clBackground
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      ExplicitWidth = 98
    end
  end
  object ActionList: TActionList
    Images = MainIL
    Left = 589
    Top = 78
    object actSave: TAction
      Caption = '&'#1057#1086#1093#1088#1072#1085#1080#1090#1100
      HelpType = htContext
      Hint = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      ImageIndex = 0
      ShortCut = 16467
      OnExecute = actSaveExecute
    end
    object actReCalc: TAction
      Caption = '&'#1055#1077#1088#1077#1089#1095#1077#1090' '#1074#1099#1095#1080#1089#1083#1103#1077#1084#1099#1093' '#1079#1085#1072#1095#1077#1085#1080#1081
      HelpType = htContext
      Hint = #1055#1077#1088#1077#1089#1095#1077#1090' '#1074#1099#1095#1080#1089#1083#1103#1077#1084#1099#1093' '#1079#1085#1072#1095#1077#1085#1080#1081
      ImageIndex = 1
      ShortCut = 16466
      OnExecute = actReCalcExecute
    end
    object actExit: TAction
      Caption = '&'#1047#1072#1082#1088#1099#1090#1100
      HelpType = htContext
      Hint = #1047#1072#1082#1088#1099#1090#1100
      ShortCut = 27
      OnExecute = actExitExecute
    end
    object actLRDirection: TAction
      AutoCheck = True
      Checked = True
      ImageIndex = 2
      ShortCut = 16455
      OnExecute = actLRDirectionExecute
    end
    object actTBDirection: TAction
      AutoCheck = True
      ImageIndex = 3
      ShortCut = 16473
      OnExecute = actTBDirectionExecute
    end
  end
  object ReCalcTimer: TTimer
    Enabled = False
    Interval = 700
    OnTimer = ReCalcTimerTimer
    Left = 28
    Top = 127
  end
  object ShowTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = ShowTimerTimer
    Left = 78
    Top = 126
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 88
    Top = 29
    PixelsPerInch = 96
    object dStyle: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clGray
    end
    object errStyle: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = 5987327
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object comErrStyle: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = 12713983
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clRed
    end
    object errDStyle: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = clBtnFace
      TextColor = clRed
    end
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clSkyBlue
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      TextColor = clBlue
    end
  end
  object MainTBM: TdxBarManager
    AllowCallFromAnotherForm = True
    AllowReset = False
    AutoDockColor = False
    AutoHideEmptyBars = True
    Scaled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    CanCustomize = False
    Categories.Strings = (
      #1054#1073#1097#1077#1077)
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockColor = clBtnFace
    ImageOptions.ImageListBkColor = clNone
    ImageOptions.Images = MainIL
    ImageOptions.LargeImages = LMainIL32
    ImageOptions.LargeIcons = True
    ImageOptions.StretchGlyphs = False
    ImageOptions.UseLargeImagesForLargeIcons = True
    LookAndFeel.Kind = lfStandard
    LookAndFeel.NativeStyle = True
    MenuAnimations = maSlide
    MenusShowRecentItemsFirst = False
    NotDocking = [dsNone, dsLeft, dsRight, dsBottom]
    PopupMenuLinks = <>
    ShowFullMenusAfterDelay = False
    ShowHelpButton = True
    ShowShortCutInHint = True
    UseFullReset = True
    UseSystemFont = True
    Left = 601
    Top = 1
    DockControlHeights = (
      0
      0
      54
      0)
    object MainTBMBar1: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      BorderStyle = bbsNone
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 724
      FloatTop = 369
      FloatClientWidth = 40
      FloatClientHeight = 40
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarStatic1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ErrPanelContainer'
        end>
      OldName = 'Custom 1'
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRecentItems = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = actSave
      Category = 0
      Glyph.Data = {
        36100000424D3610000000000000360000002800000020000000200000000100
        2000000000000010000000000000000000000000000000000000000000000000
        0000000000000000000100000001000000010000000100000001000000010000
        0001000000010000000100000001000000010000000100000001000000010000
        0001000000010000000100000001000000010000000100000001000000010000
        0001000000010000000100000001000000000000000000000000000000000000
        0001000000010000000300000005000000070000000700000007000000070000
        0007000000070000000700000007000000070000000700000007000000070000
        0007000000070000000700000007000000070000000700000007000000070000
        0007000000070000000600000004000000010000000100000000000000000000
        0001000000060000000E000000160000001A0000001B0000001B0000001B0000
        001B0000001B0000001B0000001B0000001B0000001B0000001C0000001C0000
        001C0000001C0000001C0000001C0000001C0000001C0000001D0000001D0000
        001D0000001C0000001800000010000000070000000100000000000000010000
        00030000000E2D1F198E573E33FD593D34FF583C32FF583C32FF5A3F37FFC58D
        5DFFC18656FFBD8151FFBB7D4DFFB97A4BFFB77748FFB57444FFB37141FFB06D
        3DFFAF6B3AFFAC6837FFAB6535FFA96333FFA76030FF50342DFF4F342CFF4F34
        2CFF4F342DFF4D342DFD2519158B000000100000000400000001000000010000
        0005000000145B3F36FD806357FF745449FF73534AFF735349FF61473EFFE8C5
        94FFE5BE89FFE3B981FFE3B87FFFE3B67DFFE2B57AFFE1B278FFE0B175FFDFAF
        73FFDEAD71FFDEAC6FFFDDAB6DFFDDAA6CFFDDA86AFF523731FF6B4C42FF6B4C
        42FF6B4C42FF6B4D43FF4E332AFB000000170000000600000001000000010000
        0006000000175F4337FF83655AFF77574CFF76574BFF76574BFF654B41FFEAC8
        98FFE6C08CFFE5BC85FFE3B981FFE2B77EFFE2B57CFFE1B479FFE0B277FFE0B0
        75FFDFAF72FFDEAC70FFDEAC6FFFDDAA6CFFDCA96BFF553932FF6B4C43FF6C4D
        42FF6B4C42FF6D4F45FF50362DFF0000001B0000000700000001000000010000
        000600000017624539FF87695DFF79594EFF795A4EFF795A4EFF684E44FFE1C2
        96FFDCB988FFDBB684FFD7B17BFFD7AF79FFD6AD76FFD6AC75FFD6AA72FFD5A8
        70FFD4A66EFFD4A56BFFD3A369FFD3A267FFD3A166FF583D35FF6D4F44FF6D4E
        45FF6C4D44FF6E5047FF51362FFF0000001B0000000700000001000000010000
        00060000001766483CFF8C6E63FF7D5D51FF7D5D50FF7C5C50FF6C5247FFFAF5
        F2FFF9F3F0FFF9F2EFFFF7F1ECFFF7F0EBFFF6F0EAFFF6EEE9FFF5EEE9FFF5EE
        E8FFF4EDE7FFF4ECE7FFF3ECE6FFF4EBE5FFF3EBE4FF5A3F38FF6F5046FF6E50
        46FF6E4F45FF715348FF523830FF0000001A0000000700000001000000010000
        000500000016684B3EFF917366FF816153FF816153FF7F6154FF6E554AFFFAF7
        F3FFF9F4F0FFF9F3EFFFF8F3EDFFF7F0ECFFF6F0EBFFF6EFEBFFF6EFEAFFF5EE
        E9FFF5EDE8FFF5EDE7FFF4ECE7FFF4EBE6FFF4EBE5FF5E4339FF705248FF7052
        48FF6F5147FF72554BFF523A31FF000000190000000600000001000000010000
        0005000000156C4F42FF95786AFF846456FF836557FF836456FF72584DFFFAF7
        F5FFFAF5F2FFD5B8A9FFD2B4A6FFCCA899FFC9A492FFC6A090FFC39C8BFFC097
        87FFBE9283FFBA8F7FFFB88C7BFFF4EDE6FFF4ECE6FF60463DFF72544BFF7053
        4BFF705248FF75574DFF543A32FF000000190000000600000001000000010000
        0005000000156F5346FF997D6FFF87675AFF876859FF876859FF755C50FFFBF8
        F6FFFAF6F2FFFAF5F1FFF9F5F1FFF9F3EFFFF7F2EDFFF7F1EDFFF6F0EBFFF6EF
        EAFFF5EFEAFFF5EEE9FFF5EDE8FFF5ECE8FFF4EDE6FF634940FF73564CFF7355
        4CFF72544BFF775A4FFF563D34FF000000180000000600000001000000000000
        000500000014735649FF9E8275FF8B6B5CFF8A6D5EFF8B6C5DFF796053FFFCF9
        F7FFFAF7F3FFD9BEB1FFD8BCAEFFD5BAABFFCDAC9DFFCBA797FFC8A392FFC6A0
        8FFFC39B8BFFC09787FFBD9283FFF5EEE8FFF4EDE8FF664C43FF75594EFF7458
        4DFF74574CFF795C53FF573D35FF000000180000000600000001000000000000
        00050000001376594BFFA28878FF8E7061FF8F7061FF8E7062FF7C6155FFFCF9
        F8FFFBF7F4FFFAF7F4FFF9F6F3FFF9F5F2FFF9F4F1FFF8F2EEFFF7F1EDFFF7F0
        ECFFF6F0ECFFF6F0EBFFF5EFEAFFF6EEE9FFF4EEE8FF694F46FF775B50FF7759
        4FFF75594EFF7B6056FF594037FF000000170000000600000001000000000000
        0005000000137B5D4EFFA68D7FFF937464FF937464FF917465FF7F6659FFFCFB
        F9FFFCF9F8FFFCF9F8FFFCF9F7FFFBF9F6FFFCF8F5FFF9F6F3FFF9F5F2FFF9F5
        F1FFF9F4F1FFF8F4F0FFF7F2EFFFF7F2EDFFF7F1EDFF6C5248FF785E52FF775D
        51FF775A50FF7F6359FF5C4239FF000000160000000600000001000000000000
        0005000000127E6152FFAB9182FF967767FF957A67FF957968FF876D5EFF8268
        5BFF82695AFF82685AFF81685AFF80675AFF7F6759FF7A6155FF755D51FF745B
        50FF735B50FF725A4FFF72594FFF71594EFF70574CFF72594EFF7B6055FF7A60
        54FF795E52FF81675BFF5D433AFF000000160000000500000001000000000000
        000400000012836654FFB09687FF997B6BFF9A7D6BFF9A7E6BFF9A7D6CFF997D
        6BFF997E6CFF997E6BFF997E6CFF987E6DFF997E6CFF967B6BFF82675BFF7F66
        5AFF806659FF7F6659FF7F655AFF7F6559FF7F6458FF7D6257FF7C6255FF7C61
        55FF7A6054FF826A5EFF5E463CFF000000150000000500000001000000000000
        000400000011866958FFB39B8CFF9D816FFF9D7F6EFF9D816EFF9D816FFF9D81
        70FF9D826FFF9D8270FF9D8270FF9C8170FF9B8170FF9C826FFF8D7364FF8369
        5CFF82695DFF82685CFF81675CFF81675BFF7F6659FF7F6559FF7F6458FF7D63
        58FF7D6257FF866D61FF61483EFF000000140000000500000001000000000000
        0004000000108A6C5BFFB79F91FFA08471FFA08471FFA08571FFA08573FFA085
        73FFA08574FFA08673FFA18673FFA08574FFA08673FF9F8574FF9C8271FF836A
        60FF836B60FF836A5FFF836A5DFF83695DFF82695DFF81685BFF81675AFF7F65
        59FF7F6359FF886E64FF624940FF000000140000000500000000000000000000
        0004000000108D705EFFBBA494FFA48774FFA48874FFAA927FFFAC9582FFAD94
        83FFAD9482FFAC9382FFAC9482FFAC9483FFAB9381FFAA9282FFAA9080FF9880
        71FF8E766BFF8D756AFF8D7568FF8C7468FF897167FF887065FF866E61FF8167
        5CFF806659FF8B7267FF634A40FF000000130000000500000000000000000000
        00040000000F907462FFBFA898FFA68A77FFA78B79FFB09785FF6E5449FF5439
        31FF543831FF60463EFF644A42FF634A40FF61483FFF5F473CFF5E453CFF5C44
        3BFF5B4339FF5A4139FF594137FF583F36FF563E36FF654D43FF887164FF836A
        5DFF82685CFF8D766AFF654C42FF000000120000000500000000000000000000
        00040000000E957764FFC2AD9DFFA98D7BFFAA8F7BFFB29986FF563A33FF5944
        3DFF644E47FF75594EFFE8DAD0FFDCC5B5FFDBC4B5FFDBC4B3FFDAC3B3FFDAC3
        B2FFD9C2B1FFE3D2C6FFE2D1C5FFE2D0C3FFE1CFC2FF573F37FF8A7266FF856C
        5EFF836A5EFF8F796EFF664F44FF000000120000000400000000000000000000
        00030000000E977A67FFC5B0A0FFAC907CFFAD927DFFB39C88FF573B34FF5A45
        3EFF654F48FF775B50FFEBDFD5FFDEC9BBFFDEC9B9FFDDC8B8FFDDC7B7FFDCC6
        B6FF584139FF705448FF705347FF6F5246FFE4D3C7FF594138FF8B7368FF856D
        62FF856B5FFF937B70FF685146FF000000110000000400000000000000000000
        00030000000D9B7D6AFFC8B4A3FFAE947FFFAF9480FFB79E8BFF583C34FF5B46
        3FFF655049FF795D53FFEFE4DBFFE1CEC0FFE1CDBFFFE1CDBDFFE0CBBBFFDFCA
        BBFF554038FF61473FFF654B42FF715548FFE7D8CDFF5B433AFF8C7468FF876F
        62FF876D60FF957E72FF695147FF000000100000000400000000000000000000
        00030000000C9C7F6BFFCAB6A7FFB29681FFB29782FFB89F8CFF593C35FF5B46
        40FF66514AFF7B5F55FFF2E8E0FFE5D3C5FFE4D3C4FFE3D1C2FFE3D0C1FFE2CF
        C0FF523E36FF5B413AFF5E433BFF73564BFFEBDDD4FF5E463CFF8E7469FF8871
        65FF866E64FF968075FF6B5348FF000000100000000400000000000000000000
        00030000000C9E826CFFCDBAAAFFB39983FFB49984FFBBA18DFF593D35FF5C47
        40FF68524BFF7D6157FFF4ECE5FFE8D8CAFFE8D7C9FFE7D6C8FFE6D5C7FFE6D4
        C6FF4E3C34FF553B34FF583E36FF75584DFFEEE2D9FF60483EFF8F766AFF8A72
        66FF896F65FF998377FF6C544AFF0000000F0000000400000000000000000000
        00030000000BA2836EFFCFBCACFFB69B86FFB59B86FFBBA28FFF5A3E36FF5C47
        41FF68524BFF7E6459FFF6F0EAFFEBDCCFFFEADCCEFFEADBCDFFE9DACCFFE8D9
        CBFF4B3A33FF523731FF533832FF765A4FFFF1E6DEFF624A41FF8E786AFF8A73
        67FF897165FF9B8579FF6D564BFF0000000E0000000400000000000000000000
        00030000000AA3876FFFD1BEB0FFB89D89FFB89E87FFBDA390FF5B3E37FF5C48
        42FF69524CFF80665CFFF8F3EEFFEEE1D4FFEDE0D3FFEDDFD2FFECDED1FFEBDD
        D0FF473832FF493832FF493932FF493832FFF3EBE4FF654D43FF90766CFF8D73
        68FF8A7166FF9E877DFF6D564BFF0000000D0000000300000000000000000000
        000200000008A0866DF9D2BFB0FFD3C0B2FFD3C2B1FFD6C5B6FF5B3E37FF5D48
        42FF69534CFF82675DFFF9F5F1FFF9F5F0FFF9F4EFFFF9F4EFFFF8F3EEFFF8F2
        EDFFF7F2ECFFF7F1EBFFF7F1EBFFF6F0EAFFF6EEE9FF664F46FFB4A399FFB3A0
        97FFB19D95FFB09C93FF6E564BFC0000000B0000000300000000000000000000
        00010000000552443984A38773FCA88C75FFA98D76FFA88F78FF836859FF765B
        4DFF765A4DFF80655BFFA9948BFFA89289FFA59087FFA48E85FFA28C82FFA08A
        80FF9E877EFF9C847BFF998278FF987F76FF947C73FF695148FF745F52FF745E
        51FF725B50FF6F584CFC453630A5000000070000000200000000000000000000
        0000000000020000000500000007000000090000000900000009000000090000
        000A0000000A0000000A0000000A0000000A0000000A0000000A0000000A0000
        000B0000000B0000000B0000000B0000000B0000000B0000000B0000000B0000
        000B0000000B0000000A00000007000000030000000100000000000000000000
        0000000000000000000100000002000000020000000200000002000000020000
        0002000000020000000200000002000000020000000200000002000000020000
        0002000000030000000300000003000000030000000300000003000000030000
        0003000000030000000200000002000000010000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
      AutoGrayScale = False
    end
    object ErrPanelContainer: TdxBarControlContainerItem
      Caption = #1054#1096#1080#1073#1082#1072
      Category = 0
      Hint = #1054#1096#1080#1073#1082#1072
      Visible = ivNever
      Control = ErrPanel
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = actLRDirection
      Category = 0
      ButtonStyle = bsChecked
      GroupIndex = 1
      PaintStyle = psCaptionInMenu
      AutoGrayScale = False
      ShowCaption = False
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = actTBDirection
      Category = 0
      ButtonStyle = bsChecked
      GroupIndex = 1
      PaintStyle = psCaptionInMenu
      AutoGrayScale = False
      ShowCaption = False
    end
    object dxBarStatic1: TdxBarStatic
      Caption = #1053#1072#1087#1088#1072#1074#1083#1077#1085#1080#1077' '#1086#1073#1093#1086#1076#1072' '#1103#1095#1077#1077#1082':'
      Category = 0
      Hint = #1053#1072#1087#1088#1072#1074#1083#1077#1085#1080#1077' '#1086#1073#1093#1086#1076#1072' '#1103#1095#1077#1077#1082':'
      Visible = ivAlways
    end
  end
  object MainIL: TcxImageList
    FormatVersion = 1
    DesignInfo = 197171
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000040000
          00130000001E0000002000000020000000200000002100000021000000210000
          002200000022000000220000002300000021000000160000000500000012281E
          16CB37291EFF463A31FFBD8150FFBC7E4DFFB97949FFB67646FFB37141FFB06D
          3DFFAD6839FFAB6535FF42362EFF3D3026FF241A13CE00000015000000193B2C
          21FF685C54FF483C34FFE8C28BFFE7C088FFE6BD85FFE5BB81FFE4B87CFFE3B5
          79FFE2B276FFE2B273FF443931FF51433AFF34261DFF0000001E000000183E2F
          24FF6C6057FF4A3F37FFD9B27DFFD8B07BFFD7AE77FFD7AB74FFD6A970FFD5A6
          6DFFD4A56AFFD4A268FF473B33FF5B4F47FF37291EFF0000001D000000164031
          26FF6F645CFF4C4038FFFFFFFFFFF7F1EBFFF7F0EBFFF7F0EBFFF7EFEBFFF6EF
          EAFFF6EFEAFFF6EFE9FF463B34FF5D5249FF3A2C21FF0000001B000000144434
          29FF73675FFF4F443CFFFFFFFFFFF8F2EDFFF8F1EDFFF7F1EDFFF7F0EDFFF8F1
          EBFFF7F0EBFFF7F0ECFF4A4037FF5F534BFF3D2E23FF00000019000000124637
          2CFF776B63FF50453DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF4E433BFF61544CFF403126FF0000001700000010493A
          2FFF796E66FF50453DFF61564EFF60564EFF60554DFF5F544CFF5E544CFF5E53
          4BFF5D524AFF5C5249FF5B5149FF61554DFF433429FF000000150000000E4C3D
          32FF7C706AFF674E44FF654B42FF634A41FF61473FFF5F473EFF5C443CFF5B43
          3AFF594139FF584038FF573F37FF63574FFF46362DFF000000130000000D4E3F
          35FF80746DFF6B5248FFF4ECE6FFE9DACEFFE9D8CDFFE9D8CCFFE9D8CBFFE8D7
          CAFFF3EAE2FFF3E9E2FF5A4139FF645850FF483A2FFF000000110000000B5142
          36FF827770FF70564DFFF9F5F2FFF4EAE4FFF1E6DEFFEBDCD2FFE9D9CCFF4E41
          3DFF60534CFFF3EAE3FF5D453CFF655951FF4C3D32FF0000000F000000095344
          39FF857A73FF755A50FFFAF6F3FFF5EDE7FFF4EDE6FFF4ECE6FFEFE2DAFF493D
          38FF5A4D46FFF4EBE4FF60483FFF655A52FF4F3F34FF0000000D000000075545
          3AFF887D76FF795E54FFFAF6F4FFF5EEE9FFF5EDE7FFF4EDE7FFF4ECE6FF473A
          36FF483D36FFE9D9CDFF644C43FF675A52FF514137FF0000000B000000065748
          3DFF898079FF7C6157FFFAF7F4FFFAF6F4FFFAF6F4FFFAF6F3FFFAF6F3FFFAF5
          F2FFF5EEE9FFF4ECE6FF695046FF82776FFF534439FF00000009000000034235
          2EC058493DFF7F645AFF998178FF967F75FF937A72FF8E786DFF8B7269FF866E
          64FF82695FFF7D645BFF6E544AFF56453BFF3F332BC200000005000000000000
          0002000000030000000400000004000000040000000400000005000000050000
          0005000000050000000500000006000000060000000400000001}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00020000000A0000000F00000011000000110000001200000012000000130000
          00130000001300000014000000130000000D0000000300000000000000000000
          000981594BC1B37B67FFB27B66FFB27A66FFB27864FFB17965FFB17965FFB177
          65FFB17763FFB07664FFB07664FF7E5548C50000000C00000000000000000000
          000DB57D6BFFFDFBF9FFFBF6F2FFFBF5F2FFFAF5F1FFFBF4EFFFF9F3EEFFF9F2
          EEFFFAF2ECFFF8F0EBFFF9F0EAFFB17866FF0000001200000000000000000000
          000DB7816DFFFDFBFAFFCEAFA1FFCEAFA2FFCDAFA1FFCDAEA1FFCDAFA1FFCEAF
          A1FFCDAFA1FFCDAEA1FFF9F1ECFFB37A68FF0000001300000000000000000000
          000DB98472FFFDFCFBFFCEB1A4FFCEB1A3FFCFB0A3FFCEB0A3FFCFB0A3FFCEB0
          A2FFCEB0A2FFCEAFA2FFFAF2EEFFB57D6BFF0000001200000000000000000000
          000CBB8775FFFEFCFBFFCFB2A4FFCFB2A5FFCFB2A4FFCFB2A4FFCFB2A5FFCEB1
          A4FFCFB2A4FFCFB1A4FFFAF4EFFFB7816EFF0000001100000000000000000000
          000BBE8A79FFFEFDFCFFF9F2EDFFF9F2EDFFF9F0EBFFF9F0EAFFF8F0EAFFF8F0
          E9FFF8EFE9FFF8EFE8FFFAF5F1FFBA8571FF0000001000000000000000000000
          000AC08F7EFFFEFDFDFFD2B6AAFFD2B6AAFFD2B5A9FFFAF1ECFFD2B6AAFFD2B6
          A8FFD1B5A8FFD1B5A8FFFBF6F3FFBB8975FF0000000F00000000000000000000
          0009C49380FFFEFEFDFFD4B9ABFFD4B8ABFFD2B8ABFFFAF3EFFFD2B8ABFFD2B8
          ABFFD3B7ABFFD2B7ABFFFCF7F4FFBE8B79FF0000000F00000000000000000000
          0009C69686FFFEFEFDFFD5BAAEFFD4BAAEFFD5BAAEFFFBF5F0FFD4BAADFFD4B9
          AEFFD4B9ADFFD4B9ADFFFDF9F7FFC18E7DFF0000000E00000000000000000000
          0008C99B8AFFFEFEFEFFFBF6F4FFFBF6F4FFFCF6F3FFFCF6F3FFFCF4F2FFFBF5
          F1FFFBF5F0FFFAF5F0FFFDFAF8FFC39382FF0000000D00000000000000000000
          0007C99E8DFFFFFEFEFFD8BEB2FFD7BEB2FFD7BEB2FFD7BDB2FFD7BDB2FFD7BD
          B1FFD7BDB1FFD6BDB0FFFDFBF9FFC69786FF0000000C00000000000000000000
          0006CEA190FFFFFFFEFFD8BFB4FFD8BFB4FFD8BFB3FFD8BFB4FFD7BEB3FFD8BE
          B3FFD7BFB3FFD8BEB3FFFDFCFAFFC89B8AFF0000000B00000000000000000000
          0006CEA393FFFFFFFFFFD9C0B5FFD9C0B5FFD9C0B5FFD9C0B5FFD9C0B5FFD9C0
          B5FFD8C0B5FFD8C0B5FFFEFCFCFFCB9D8DFF0000000B00000000000000000000
          0005D0A696FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFEFFFEFEFEFFFFFE
          FEFFFEFEFEFFFEFEFEFFFEFEFDFFCDA191FF0000000900000000000000000000
          00039C7C71C0D2A898FFD1A897FFD1A897FFD1A797FFD0A696FFD0A696FFD0A6
          95FFD0A595FFCFA595FFCFA494FF98796EC20000000600000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00040000000F0000000F00000007000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000EB0683FFF834324E131190D6A000000110000000600000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0011B37045FFDEAF77FFB47247FF773C20D2201009500000000F000000050000
          0001000000000000000000000000000000000000000000000000000000000000
          0010B6764BFFECC38FFFE8BA7DFFD8A66FFFAD693FFF6A371CBE170C06400000
          000D000000040000000100000000000000000000000000000000000000000000
          000FBB7C51FFEFCD9CFFE9BB7FFFE8BC80FFEABE83FFD39D6CFFA7623BFD5B30
          1AA90D07032C0000000B00000003000000010000000000000000000000000000
          000EBE8257FFF1D5ACFFEBC087FFE9BF85FFE8BD81FFE9BF82FFE9C087FFCD97
          65FF9F5933F74E29169408040221000000090000000200000000000000000000
          000DC0855EFFF5DEBBFFEDC68FFFEDC58DFFEBC289FFEAC084FFE8BC80FFE9BD
          82FFE8BB87FFC78E60FF965430EE4223137E0000000900000000000000000000
          000CC59067FFF9E9CFFFF3D4A3FFF2D09FFFF0CC98FFEEC890FFEBC088FFE9BE
          83FFEAC38AFFECC693FFE3B889FFA65B33FF0000000D00000000000000000000
          000BC9956EFFFAEFDAFFF4DAAEFFF5D8AAFFF2D5A6FFF1D1A1FFF1D0A2FFF1D2
          A8FFEDCDA1FFCC956BFF9A5D39E33A2314680000000700000000000000000000
          000ACC9B73FFFCF4E3FFF8E0B7FFF6DDB4FFF6DEB4FFF7E2C0FFF6E1C1FFD9B2
          8CFFB0754EF14D301F7E04030216000000060000000100000000000000000000
          0009CDA077FFFEF7E9FFFBE7C2FFFBEBCCFFFBEFD6FFE3C5A7FFC08B65F96544
          2E960B07051E0000000600000002000000000000000000000000000000000000
          0008D0A47CFFFEFBEFFFFDF4E0FFEEDAC1FFD1A07DFF78553CA717100B2C0000
          0007000000020000000000000000000000000000000000000000000000000000
          0007D2A77FFFF5EADBFFD8B291FF916D50BE261C143C00000007000000020000
          0000000000000000000000000000000000000000000000000000000000000000
          0005D5AA83FFA98462D3372B204F000000070000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000040000000500000002000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000003000000090000000A0000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000D552C19A64E2916A40000
          000D000000010000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000623130B54A77053FD9E6443FD2011
          0A54000000060000000000000000000000000000000000000000000000000000
          000000000000000000000000000204020117814A2EDEDFBF9BFFD4AC7EFF7942
          26DF030201180000000200000000000000000000000000000000000000000000
          00000000000000000001000000094D2D1C90C69D7FFFDFB987FFDFB785FFBA87
          5FFF472617910000000A00000001000000000000000000000000000000000000
          00000000000000000004180F0A3AA87252FBE5C89FFFDCB27AFFDCB17BFFE0BC
          8EFF9C6140FB160C073B00000004000000000000000000000000000000000000
          0000000000010000000C7E4E32CCDDBFA0FFDFB783FFDDB37DFFDDB37DFFDFB6
          82FFD2AB80FF714027CD0000000D000000020000000000000000000000000000
          000000000006452C1E78C69D7FFFE4C394FFDEB680FFDFB680FFDEB67FFFDEB5
          7FFFE3BF8FFFB8855FFF3E241679000000070000000000000000000000000000
          0002120C0828AA7758F3E9CEAAFFE4C290FFE5C794FFE8CA98FFE8CA99FFE6C5
          93FFE3C18DFFE2C193FF996141F31009062A0000000300000000000000000000
          0006795339B6E0C7ADFFF9F1E0FFFAF2E2FFFAF2E2FFEBD3A3FFECD2A3FFF1DD
          B7FFF0DDB7FFF9F1E0FFDBBEA6FF6B4028B80000000700000000000000000000
          0006C1906AFFC18E68FFC08C66FFBE8A64FFFBF5E5FFEFDAACFFEFD9ACFFF3E2
          BEFFB9805AFFB77E58FFB77C55FFB57A54FF0000000800000000000000000000
          000200000006000000070000000DC08D68FFFBF7E9FFF2E1B4FFF2E1B4FFF5E9
          C5FFB27753FF0000001000000009000000070000000200000000000000000000
          0000000000000000000000000007C3926CFFFCF8EBFFF5E8BCFFF5E7BCFFF8EE
          CCFFB47C56FF0000000800000000000000000000000000000000000000000000
          0000000000000000000000000005C59570FFFDFAEDFFFDFAEDFFFDFAEDFFFDFA
          EDFFB67F5AFF0000000600000000000000000000000000000000000000000000
          000000000000000000000000000393755CBACDA37EFFCBA17DFFCAA07BFFC99D
          79FF8C6A50BB0000000400000000000000000000000000000000000000000000
          0000000000000000000000000001000000020000000400000004000000040000
          0004000000030000000100000000000000000000000000000000}
      end>
  end
  object LMainIL32: TcxImageList
    Height = 32
    Width = 32
    FormatVersion = 1
    DesignInfo = 1049047
    ImageInfo = <
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C00050372C00583E3300593D3400583C3200583C32005A3F3700C58D
          5D00C1865600BD815100BB7D4D00B97A4B00B7774800B5744400B3714100B06D
          3D00AF6B3A00AC683700AB653500A9633300A760300050342D004F342C004F34
          2C004F342D004E342D00432D2600C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C0005C3F3600806357007454490073534A007353490061473E00E8C5
          9400E5BE8900E3B98100E3B87F00E3B67D00E2B57A00E1B27800E0B17500DFAF
          7300DEAD7100DEAC6F00DDAB6D00DDAA6C00DDA86A00523731006B4C42006B4C
          42006B4C42006B4D43004F342B00C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C0005F43370083655A0077574C0076574B0076574B00654B4100EAC8
          9800E6C08C00E5BC8500E3B98100E2B77E00E2B57C00E1B47900E0B27700E0B0
          7500DFAF7200DEAC7000DEAC6F00DDAA6C00DCA96B00553932006B4C43006C4D
          42006B4C42006D4F450050362D00C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C0006245390087695D0079594E00795A4E00795A4E00684E4400E1C2
          9600DCB98800DBB68400D7B17B00D7AF7900D6AD7600D6AC7500D6AA7200D5A8
          7000D4A66E00D4A56B00D3A36900D3A26700D3A16600583D35006D4F44006D4E
          45006C4D44006E50470051362F00C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C00066483C008C6E63007D5D51007D5D50007C5C50006C524700FAF5
          F200F9F3F000F9F2EF00F7F1EC00F7F0EB00F6F0EA00F6EEE900F5EEE900F5EE
          E800F4EDE700F4ECE700F3ECE600F4EBE500F3EBE4005A3F38006F5046006E50
          46006E4F45007153480052383000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000684B3E009173660081615300816153007F6154006E554A00FAF7
          F300F9F4F000F9F3EF00F8F3ED00F7F0EC00F6F0EB00F6EFEB00F6EFEA00F5EE
          E900F5EDE800F5EDE700F4ECE700F4EBE600F4EBE5005E433900705248007052
          48006F51470072554B00523A3100C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C0006C4F420095786A0084645600836557008364560072584D00FAF7
          F500FAF5F200D5B8A900D2B4A600CCA89900C9A49200C6A09000C39C8B00C097
          8700BE928300BA8F7F00B88C7B00F4EDE600F4ECE60060463D0072544B007053
          4B007052480075574D00543A3200C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C0006F534600997D6F0087675A008768590087685900755C5000FBF8
          F600FAF6F200FAF5F100F9F5F100F9F3EF00F7F2ED00F7F1ED00F6F0EB00F6EF
          EA00F5EFEA00F5EEE900F5EDE800F5ECE800F4EDE6006349400073564C007355
          4C0072544B00775A4F00563D3400C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000735649009E8275008B6B5C008A6D5E008B6C5D0079605300FCF9
          F700FAF7F300D9BEB100D8BCAE00D5BAAB00CDAC9D00CBA79700C8A39200C6A0
          8F00C39B8B00C0978700BD928300F5EEE800F4EDE800664C430075594E007458
          4D0074574C00795C5300573D3500C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C00076594B00A28878008E7061008F7061008E7062007C615500FCF9
          F800FBF7F400FAF7F400F9F6F300F9F5F200F9F4F100F8F2EE00F7F1ED00F7F0
          EC00F6F0EC00F6F0EB00F5EFEA00F6EEE900F4EEE800694F4600775B50007759
          4F0075594E007B60560059403700C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C0007B5D4E00A68D7F009374640093746400917465007F665900FCFB
          F900FCF9F800FCF9F800FCF9F700FBF9F600FCF8F500F9F6F300F9F5F200F9F5
          F100F9F4F100F8F4F000F7F2EF00F7F2ED00F7F1ED006C524800785E5200775D
          5100775A50007F6359005C423900C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C0007E615200AB91820096776700957A670095796800876D5E008268
          5B0082695A0082685A0081685A0080675A007F6759007A615500755D5100745B
          5000735B5000725A4F0072594F0071594E0070574C0072594E007B6055007A60
          5400795E520081675B005D433A00C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C00083665400B0968700997B6B009A7D6B009A7E6B009A7D6C00997D
          6B00997E6C00997E6B00997E6C00987E6D00997E6C00967B6B0082675B007F66
          5A00806659007F6659007F655A007F6559007F6458007D6257007C6255007C61
          55007A605400826A5E005E463C00C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C00086695800B39B8C009D816F009D7F6E009D816E009D816F009D81
          70009D826F009D8270009D8270009C8170009B8170009C826F008D7364008369
          5C0082695D0082685C0081675C0081675B007F6659007F6559007F6458007D63
          58007D625700866D610061483E00C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C0008A6C5B00B79F9100A0847100A0847100A0857100A0857300A085
          7300A0857400A0867300A1867300A0857400A08673009F8574009C827100836A
          6000836B6000836A5F00836A5D0083695D0082695D0081685B0081675A007F65
          59007F635900886E640062494000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C0008D705E00BBA49400A4877400A4887400AA927F00AC958200AD94
          8300AD948200AC938200AC948200AC948300AB938100AA928200AA9080009880
          71008E766B008D756A008D7568008C7468008971670088706500866E61008167
          5C00806659008B726700634A4000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C00090746200BFA89800A68A7700A78B7900B09785006E5449005439
          31005438310060463E00644A4200634A400061483F005F473C005E453C005C44
          3B005B4339005A41390059413700583F3600563E3600654D430088716400836A
          5D0082685C008D766A00654C4200C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C00095776400C2AD9D00A98D7B00AA8F7B00B2998600563A33005944
          3D00644E470075594E00E8DAD000DCC5B500DBC4B500DBC4B300DAC3B300DAC3
          B200D9C2B100E3D2C600E2D1C500E2D0C300E1CFC200573F37008A726600856C
          5E00836A5E008F796E00664F4400C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000977A6700C5B0A000AC907C00AD927D00B39C8800573B34005A45
          3E00654F4800775B5000EBDFD500DEC9BB00DEC9B900DDC8B800DDC7B700DCC6
          B6005841390070544800705347006F524600E4D3C700594138008B736800856D
          6200856B5F00937B700068514600C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C0009B7D6A00C8B4A300AE947F00AF948000B79E8B00583C34005B46
          3F0065504900795D5300EFE4DB00E1CEC000E1CDBF00E1CDBD00E0CBBB00DFCA
          BB005540380061473F00654B420071554800E7D8CD005B433A008C746800876F
          6200876D6000957E720069514700C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C0009C7F6B00CAB6A700B2968100B2978200B89F8C00593C35005B46
          400066514A007B5F5500F2E8E000E5D3C500E4D3C400E3D1C200E3D0C100E2CF
          C000523E36005B413A005E433B0073564B00EBDDD4005E463C008E7469008871
          6500866E6400968075006B534800C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C0009E826C00CDBAAA00B3998300B4998400BBA18D00593D35005C47
          400068524B007D615700F4ECE500E8D8CA00E8D7C900E7D6C800E6D5C700E6D4
          C6004E3C3400553B3400583E360075584D00EEE2D90060483E008F766A008A72
          6600896F6500998377006C544A00C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000A2836E00CFBCAC00B69B8600B59B8600BBA28F005A3E36005C47
          410068524B007E645900F6F0EA00EBDCCF00EADCCE00EADBCD00E9DACC00E8D9
          CB004B3A33005237310053383200765A4F00F1E6DE00624A41008E786A008A73
          6700897165009B8579006D564B00C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000A3876F00D1BEB000B89D8900B89E8700BDA390005B3E37005C48
          420069524C0080665C00F8F3EE00EEE1D400EDE0D300EDDFD200ECDED100EBDD
          D00047383200493832004939320049383200F3EBE400654D430090766C008D73
          68008A7166009E877D006D564B00C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000A4897000D2BFB000D3C0B200D3C2B100D6C5B6005B3E37005D48
          420069534C0082675D00F9F5F100F9F5F000F9F4EF00F9F4EF00F8F3EE00F8F2
          ED00F7F2EC00F7F1EB00F7F1EB00F6F0EA00F6EEE900664F4600B4A39900B3A0
          9700B19D9500B09C93006F574C00C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C0009E836E00A5897400A88C7500A98D7600A88F780083685900765B
          4D00765A4D0080655B00A9948B00A8928900A5908700A48E8500A28C8200A08A
          80009E877E009C847B0099827800987F7600947C730069514800745F5200745E
          5100725B500070594D006A544A00C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
          C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000}
        MaskColor = clSilver
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00020000000A0000000F00000011000000110000001200000012000000130000
          00130000001300000014000000130000000D0000000300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000981594BC1B37B67FFB27B66FFB27A66FFB27864FFB17965FFB17965FFB177
          65FFB17763FFB07664FFB07664FF7E5548C50000000C00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000DB57D6BFFFDFBF9FFFBF6F2FFFBF5F2FFFAF5F1FFFBF4EFFFF9F3EEFFF9F2
          EEFFFAF2ECFFF8F0EBFFF9F0EAFFB17866FF0000001200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000DB7816DFFFDFBFAFFCEAFA1FFCEAFA2FFCDAFA1FFCDAEA1FFCDAFA1FFCEAF
          A1FFCDAFA1FFCDAEA1FFF9F1ECFFB37A68FF0000001300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000DB98472FFFDFCFBFFCEB1A4FFCEB1A3FFCFB0A3FFCEB0A3FFCFB0A3FFCEB0
          A2FFCEB0A2FFCEAFA2FFFAF2EEFFB57D6BFF0000001200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000CBB8775FFFEFCFBFFCFB2A4FFCFB2A5FFCFB2A4FFCFB2A4FFCFB2A5FFCEB1
          A4FFCFB2A4FFCFB1A4FFFAF4EFFFB7816EFF0000001100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000BBE8A79FFFEFDFCFFF9F2EDFFF9F2EDFFF9F0EBFFF9F0EAFFF8F0EAFFF8F0
          E9FFF8EFE9FFF8EFE8FFFAF5F1FFBA8571FF0000001000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000AC08F7EFFFEFDFDFFD2B6AAFFD2B6AAFFD2B5A9FFFAF1ECFFD2B6AAFFD2B6
          A8FFD1B5A8FFD1B5A8FFFBF6F3FFBB8975FF0000000F00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0009C49380FFFEFEFDFFD4B9ABFFD4B8ABFFD2B8ABFFFAF3EFFFD2B8ABFFD2B8
          ABFFD3B7ABFFD2B7ABFFFCF7F4FFBE8B79FF0000000F00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0009C69686FFFEFEFDFFD5BAAEFFD4BAAEFFD5BAAEFFFBF5F0FFD4BAADFFD4B9
          AEFFD4B9ADFFD4B9ADFFFDF9F7FFC18E7DFF0000000E00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0008C99B8AFFFEFEFEFFFBF6F4FFFBF6F4FFFCF6F3FFFCF6F3FFFCF4F2FFFBF5
          F1FFFBF5F0FFFAF5F0FFFDFAF8FFC39382FF0000000D00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0007C99E8DFFFFFEFEFFD8BEB2FFD7BEB2FFD7BEB2FFD7BDB2FFD7BDB2FFD7BD
          B1FFD7BDB1FFD6BDB0FFFDFBF9FFC69786FF0000000C00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0006CEA190FFFFFFFEFFD8BFB4FFD8BFB4FFD8BFB3FFD8BFB4FFD7BEB3FFD8BE
          B3FFD7BFB3FFD8BEB3FFFDFCFAFFC89B8AFF0000000B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0006CEA393FFFFFFFFFFD9C0B5FFD9C0B5FFD9C0B5FFD9C0B5FFD9C0B5FFD9C0
          B5FFD8C0B5FFD8C0B5FFFEFCFCFFCB9D8DFF0000000B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0005D0A696FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFEFFFEFEFEFFFFFE
          FEFFFEFEFEFFFEFEFEFFFEFEFDFFCDA191FF0000000900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00039C7C71C0D2A898FFD1A897FFD1A897FFD1A797FFD0A696FFD0A696FFD0A6
          95FFD0A595FFCFA595FFCFA494FF98796EC20000000600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000004B79FFFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000004B79FFFF9DDBFFFF4B79
          FFFF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDB
          FFFF9DDBFFFF4B79FFFF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDB
          FFFF9DDBFFFF9DDBFFFF4B79FFFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000004B79FFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDB
          FFFF9DDBFFFF9DDBFFFF9DDBFFFF4B79FFFF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000004B79FFFF4B79FFFF4B79FFFF4B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000099
          00FF009900FF0000000000000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000099
          00FF33CC66FF009900FF00000000000000004B79FFFF9DDBFFFF9DDBFFFF9DDB
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000099
          00FF33CC66FF33CC66FF009900FF000000004B79FFFF4B79FFFF4B79FFFF4B79
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000099
          00FF009900FF009900FF009900FF009900FF009900FF009900FF009900FF0099
          00FF009900FF009900FF009900FF009900FF009900FF009900FF009900FF0099
          00FF33CC66FF33CC66FF33CC66FF009900FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000099
          00FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC
          66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC
          66FF33CC66FF33CC66FF33CC66FF33CC66FF009900FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000099
          00FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC
          66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC
          66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF009900FF000000000000
          0000000000000000000000000000000000000000000000000000000000000099
          00FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC
          66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC
          66FF33CC66FF33CC66FF33CC66FF33CC66FF009900FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000099
          00FF009900FF009900FF009900FF009900FF009900FF009900FF009900FF0099
          00FF009900FF009900FF009900FF009900FF009900FF009900FF009900FF0099
          00FF33CC66FF33CC66FF33CC66FF009900FF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000099
          00FF33CC66FF33CC66FF009900FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000099
          00FF33CC66FF009900FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000099
          00FF009900FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004B79
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004B79
          FFFF9DDBFFFF4B79FFFF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004B79
          FFFF9DDBFFFF9DDBFFFF4B79FFFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004B79
          FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79
          FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79
          FFFF9DDBFFFF9DDBFFFF9DDBFFFF4B79FFFF0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004B79
          FFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDB
          FFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDB
          FFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF4B79FFFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000004B79
          FFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDB
          FFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDB
          FFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF4B79FFFF000000000000
          0000000000000000000000000000000000000000000000000000000000004B79
          FFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDB
          FFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDB
          FFFF9DDBFFFF9DDBFFFF9DDBFFFF9DDBFFFF4B79FFFF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000004B79
          FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79
          FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79FFFF4B79
          FFFF9DDBFFFF9DDBFFFF9DDBFFFF4B79FFFF0000000000000000000000000000
          000000000000000000000000000000000000009900FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004B79
          FFFF9DDBFFFF9DDBFFFF4B79FFFF000000000000000000000000000000000000
          0000000000000000000000000000009900FF33CC66FF009900FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004B79
          FFFF9DDBFFFF4B79FFFF00000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000004B79
          FFFF4B79FFFF0000000000000000000000000000000000000000000000000000
          000000000000009900FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF0099
          00FF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000009900FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC
          66FF009900FF0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000099
          00FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC66FF33CC
          66FF33CC66FF009900FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000099
          00FF009900FF009900FF009900FF33CC66FF33CC66FF33CC66FF009900FF0099
          00FF009900FF009900FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF33CC66FF33CC66FF33CC66FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000009900FF009900FF009900FF009900FF009900FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0001000000010000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000020000
          0004000000040000000200000001000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000002000000070000
          000D0000000E0000000A00000005000000020000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000001000000040000000EA55E
          37FF5E321BA91008053300000011000000090000000400000002000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000500000013A662
          38FFC9996DFF9F5C36F9502A1896070402240000000F00000008000000040000
          0001000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000500000014AA65
          3BFFE7C99CFFE1BC8AFFC18D61FF93522FEC3E22137A020101190000000D0000
          0007000000030000000100000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000400000013AB6A
          3FFFE9CEA3FFDFB67FFFE1BD89FFDFB88AFFBB8359FF864929DC2C180E5F0000
          00140000000C0000000500000002000000010000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000400000011AF6E
          44FFEBD1ACFFE1BA83FFE1B983FFE1BA85FFE4C190FFDAB486FFB3784FFF7542
          25C51E110948000000120000000A000000050000000200000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000400000010B273
          48FFEED7B3FFE3BE8AFFE2BD89FFE2BC88FFE2BC87FFE2BC8AFFE6C394FFD5AA
          7EFFAF7048FF633820AB0F090530000000100000000900000004000000020000
          0001000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000040000000FB478
          4DFFF1DABBFFE5C290FFE5C28FFFE4C08EFFE4C08DFFE3BE8BFFE3BD89FFE4BF
          8CFFE7C596FFCD9F73FFA5663FF954301D95080402230000000E000000080000
          0003000000010000000100000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000030000000EB77B
          53FFF3E0C4FFE8C696FFE7C695FFE7C595FFE6C493FFE5C291FFE4C08EFFE3BF
          8BFFE2BD88FFE4C08DFFE4C294FFC7966AFF9A5E39EC40261677020101180000
          000B000000040000000100000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000030000000DBA82
          59FFF5E4CAFFEACB9DFFEACA9CFFE9C99BFFE9C999FFE7C697FFE7C494FFE5C2
          90FFE4BF8DFFE2BD89FFE1BB86FFE4BF8DFFDFBB8EFFBF895EFF8C5533DB2F1C
          1159000000090000000200000001000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000030000000CBD86
          5DFFF7E8D1FFEDD0A3FFECCFA2FFECCEA2FFEBCDA0FFEACB9DFFE9C999FFE8C6
          96FFE6C392FFE4C08EFFE2BD89FFE1B984FFE2BC8AFFE8CCA0FFDCB890FFAC6C
          42FF0000000C0000000300000001000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000030000000BC18D
          64FFF8EBD7FFF1D8AFFFF2DBB2FFF3DCB3FFF2DBB2FFF1D9AFFFEFD5AAFFECD1
          A4FFEACA9BFFE6C493FFE5C291FFEACEA6FFE5C9A6FFC39169FF855535C82115
          0D40000000080000000200000001000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000020000000AC491
          6AFFFBF2DBFFF6E4BEFFF6E4BEFFF5E2BCFFF5E0BAFFF4DFB7FFF2DCB3FFF0D9
          AFFFEFD7ACFFF1DDB6FFEFDBBCFFD0A581FF9D6A46E139261962000000100000
          0008000000030000000100000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000200000009C697
          6FFFFCF5DEFFF8E9C4FFF8E8C3FFF7E7C2FFF7E5C0FFF5E3BDFFF4E0B9FFF5E4
          BFFFF6EBCDFFDEBF9EFFB4805AF35238257F0403021600000009000000050000
          0002000000010000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000200000008C99B
          75FFFDF7E0FFFAEDC9FFFAECC8FFF9EBC6FFF9E9C5FFF8E9C6FFFAEFD3FFECD7
          BAFFC69972FD6F4F369F0D090620000000090000000500000002000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000200000007CBA0
          79FFFEF8E1FFFBEFCDFFFCEFCCFFFBEFCDFFFCF3D7FFF4E7CCFFD4AF8BFF916B
          4CC5221911390000000900000005000000020000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000100000006CDA4
          7EFFFEFAE1FFFDF2D1FFFEF5D9FFFBF2D8FFDFC2A1FFAC8360DF3E2F225B0000
          0009000000050000000200000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000100000005D0A8
          83FFFFFAE2FFFDF8DFFFE9D4B5FFC29A76F2594533790504030E000000050000
          0002000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000100000004D2AC
          87FFF1E2C6FFD2B08AFC785F479A0E0B09190000000500000003000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000100000002D4B0
          8BFF95785BB9221B152F00000004000000020000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0002000000020000000200000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000000000000000000000000000000000
          0001000000010000000300000005000000070000000700000007000000070000
          0007000000070000000700000007000000070000000700000007000000070000
          0007000000070000000700000007000000070000000700000007000000070000
          0007000000070000000600000004000000010000000100000000000000000000
          0001000000060000000E000000160000001A0000001B0000001B0000001B0000
          001B0000001B0000001B0000001B0000001B0000001B0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001D0000001D0000
          001D0000001C0000001800000010000000070000000100000000000000010000
          00030000000E50372C8E583E33FD593D34FF583C32FF583C32FF5A3F37FFC58D
          5DFFC18656FFBD8151FFBB7D4DFFB97A4BFFB77748FFB57444FFB37141FFB06D
          3DFFAF6B3AFFAC6837FFAB6535FFA96333FFA76030FF50342DFF4F342CFF4F34
          2CFF4F342DFF4E342DFD432D268B000000100000000400000001000000010000
          0005000000145C3F36FD806357FF745449FF73534AFF735349FF61473EFFE8C5
          94FFE5BE89FFE3B981FFE3B87FFFE3B67DFFE2B57AFFE1B278FFE0B175FFDFAF
          73FFDEAD71FFDEAC6FFFDDAB6DFFDDAA6CFFDDA86AFF523731FF6B4C42FF6B4C
          42FF6B4C42FF6B4D43FF4F342BFB000000170000000600000001000000010000
          0006000000175F4337FF83655AFF77574CFF76574BFF76574BFF654B41FFEAC8
          98FFE6C08CFFE5BC85FFE3B981FFE2B77EFFE2B57CFFE1B479FFE0B277FFE0B0
          75FFDFAF72FFDEAC70FFDEAC6FFFDDAA6CFFDCA96BFF553932FF6B4C43FF6C4D
          42FF6B4C42FF6D4F45FF50362DFF0000001B0000000700000001000000010000
          000600000017624539FF87695DFF79594EFF795A4EFF795A4EFF684E44FFE1C2
          96FFDCB988FFDBB684FFD7B17BFFD7AF79FFD6AD76FFD6AC75FFD6AA72FFD5A8
          70FFD4A66EFFD4A56BFFD3A369FFD3A267FFD3A166FF583D35FF6D4F44FF6D4E
          45FF6C4D44FF6E5047FF51362FFF0000001B0000000700000001000000010000
          00060000001766483CFF8C6E63FF7D5D51FF7D5D50FF7C5C50FF6C5247FFFAF5
          F2FFF9F3F0FFF9F2EFFFF7F1ECFFF7F0EBFFF6F0EAFFF6EEE9FFF5EEE9FFF5EE
          E8FFF4EDE7FFF4ECE7FFF3ECE6FFF4EBE5FFF3EBE4FF5A3F38FF6F5046FF6E50
          46FF6E4F45FF715348FF523830FF0000001A0000000700000001000000010000
          000500000016684B3EFF917366FF816153FF816153FF7F6154FF6E554AFFFAF7
          F3FFF9F4F0FFF9F3EFFFF8F3EDFFF7F0ECFFF6F0EBFFF6EFEBFFF6EFEAFFF5EE
          E9FFF5EDE8FFF5EDE7FFF4ECE7FFF4EBE6FFF4EBE5FF5E4339FF705248FF7052
          48FF6F5147FF72554BFF523A31FF000000190000000600000001000000010000
          0005000000156C4F42FF95786AFF846456FF836557FF836456FF72584DFFFAF7
          F5FFFAF5F2FFD5B8A9FFD2B4A6FFCCA899FFC9A492FFC6A090FFC39C8BFFC097
          87FFBE9283FFBA8F7FFFB88C7BFFF4EDE6FFF4ECE6FF60463DFF72544BFF7053
          4BFF705248FF75574DFF543A32FF000000190000000600000001000000010000
          0005000000156F5346FF997D6FFF87675AFF876859FF876859FF755C50FFFBF8
          F6FFFAF6F2FFFAF5F1FFF9F5F1FFF9F3EFFFF7F2EDFFF7F1EDFFF6F0EBFFF6EF
          EAFFF5EFEAFFF5EEE9FFF5EDE8FFF5ECE8FFF4EDE6FF634940FF73564CFF7355
          4CFF72544BFF775A4FFF563D34FF000000180000000600000001000000000000
          000500000014735649FF9E8275FF8B6B5CFF8A6D5EFF8B6C5DFF796053FFFCF9
          F7FFFAF7F3FFD9BEB1FFD8BCAEFFD5BAABFFCDAC9DFFCBA797FFC8A392FFC6A0
          8FFFC39B8BFFC09787FFBD9283FFF5EEE8FFF4EDE8FF664C43FF75594EFF7458
          4DFF74574CFF795C53FF573D35FF000000180000000600000001000000000000
          00050000001376594BFFA28878FF8E7061FF8F7061FF8E7062FF7C6155FFFCF9
          F8FFFBF7F4FFFAF7F4FFF9F6F3FFF9F5F2FFF9F4F1FFF8F2EEFFF7F1EDFFF7F0
          ECFFF6F0ECFFF6F0EBFFF5EFEAFFF6EEE9FFF4EEE8FF694F46FF775B50FF7759
          4FFF75594EFF7B6056FF594037FF000000170000000600000001000000000000
          0005000000137B5D4EFFA68D7FFF937464FF937464FF917465FF7F6659FFFCFB
          F9FFFCF9F8FFFCF9F8FFFCF9F7FFFBF9F6FFFCF8F5FFF9F6F3FFF9F5F2FFF9F5
          F1FFF9F4F1FFF8F4F0FFF7F2EFFFF7F2EDFFF7F1EDFF6C5248FF785E52FF775D
          51FF775A50FF7F6359FF5C4239FF000000160000000600000001000000000000
          0005000000127E6152FFAB9182FF967767FF957A67FF957968FF876D5EFF8268
          5BFF82695AFF82685AFF81685AFF80675AFF7F6759FF7A6155FF755D51FF745B
          50FF735B50FF725A4FFF72594FFF71594EFF70574CFF72594EFF7B6055FF7A60
          54FF795E52FF81675BFF5D433AFF000000160000000500000001000000000000
          000400000012836654FFB09687FF997B6BFF9A7D6BFF9A7E6BFF9A7D6CFF997D
          6BFF997E6CFF997E6BFF997E6CFF987E6DFF997E6CFF967B6BFF82675BFF7F66
          5AFF806659FF7F6659FF7F655AFF7F6559FF7F6458FF7D6257FF7C6255FF7C61
          55FF7A6054FF826A5EFF5E463CFF000000150000000500000001000000000000
          000400000011866958FFB39B8CFF9D816FFF9D7F6EFF9D816EFF9D816FFF9D81
          70FF9D826FFF9D8270FF9D8270FF9C8170FF9B8170FF9C826FFF8D7364FF8369
          5CFF82695DFF82685CFF81675CFF81675BFF7F6659FF7F6559FF7F6458FF7D63
          58FF7D6257FF866D61FF61483EFF000000140000000500000001000000000000
          0004000000108A6C5BFFB79F91FFA08471FFA08471FFA08571FFA08573FFA085
          73FFA08574FFA08673FFA18673FFA08574FFA08673FF9F8574FF9C8271FF836A
          60FF836B60FF836A5FFF836A5DFF83695DFF82695DFF81685BFF81675AFF7F65
          59FF7F6359FF886E64FF624940FF000000140000000500000000000000000000
          0004000000108D705EFFBBA494FFA48774FFA48874FFAA927FFFAC9582FFAD94
          83FFAD9482FFAC9382FFAC9482FFAC9483FFAB9381FFAA9282FFAA9080FF9880
          71FF8E766BFF8D756AFF8D7568FF8C7468FF897167FF887065FF866E61FF8167
          5CFF806659FF8B7267FF634A40FF000000130000000500000000000000000000
          00040000000F907462FFBFA898FFA68A77FFA78B79FFB09785FF6E5449FF5439
          31FF543831FF60463EFF644A42FF634A40FF61483FFF5F473CFF5E453CFF5C44
          3BFF5B4339FF5A4139FF594137FF583F36FF563E36FF654D43FF887164FF836A
          5DFF82685CFF8D766AFF654C42FF000000120000000500000000000000000000
          00040000000E957764FFC2AD9DFFA98D7BFFAA8F7BFFB29986FF563A33FF5944
          3DFF644E47FF75594EFFE8DAD0FFDCC5B5FFDBC4B5FFDBC4B3FFDAC3B3FFDAC3
          B2FFD9C2B1FFE3D2C6FFE2D1C5FFE2D0C3FFE1CFC2FF573F37FF8A7266FF856C
          5EFF836A5EFF8F796EFF664F44FF000000120000000400000000000000000000
          00030000000E977A67FFC5B0A0FFAC907CFFAD927DFFB39C88FF573B34FF5A45
          3EFF654F48FF775B50FFEBDFD5FFDEC9BBFFDEC9B9FFDDC8B8FFDDC7B7FFDCC6
          B6FF584139FF705448FF705347FF6F5246FFE4D3C7FF594138FF8B7368FF856D
          62FF856B5FFF937B70FF685146FF000000110000000400000000000000000000
          00030000000D9B7D6AFFC8B4A3FFAE947FFFAF9480FFB79E8BFF583C34FF5B46
          3FFF655049FF795D53FFEFE4DBFFE1CEC0FFE1CDBFFFE1CDBDFFE0CBBBFFDFCA
          BBFF554038FF61473FFF654B42FF715548FFE7D8CDFF5B433AFF8C7468FF876F
          62FF876D60FF957E72FF695147FF000000100000000400000000000000000000
          00030000000C9C7F6BFFCAB6A7FFB29681FFB29782FFB89F8CFF593C35FF5B46
          40FF66514AFF7B5F55FFF2E8E0FFE5D3C5FFE4D3C4FFE3D1C2FFE3D0C1FFE2CF
          C0FF523E36FF5B413AFF5E433BFF73564BFFEBDDD4FF5E463CFF8E7469FF8871
          65FF866E64FF968075FF6B5348FF000000100000000400000000000000000000
          00030000000C9E826CFFCDBAAAFFB39983FFB49984FFBBA18DFF593D35FF5C47
          40FF68524BFF7D6157FFF4ECE5FFE8D8CAFFE8D7C9FFE7D6C8FFE6D5C7FFE6D4
          C6FF4E3C34FF553B34FF583E36FF75584DFFEEE2D9FF60483EFF8F766AFF8A72
          66FF896F65FF998377FF6C544AFF0000000F0000000400000000000000000000
          00030000000BA2836EFFCFBCACFFB69B86FFB59B86FFBBA28FFF5A3E36FF5C47
          41FF68524BFF7E6459FFF6F0EAFFEBDCCFFFEADCCEFFEADBCDFFE9DACCFFE8D9
          CBFF4B3A33FF523731FF533832FF765A4FFFF1E6DEFF624A41FF8E786AFF8A73
          67FF897165FF9B8579FF6D564BFF0000000E0000000400000000000000000000
          00030000000AA3876FFFD1BEB0FFB89D89FFB89E87FFBDA390FF5B3E37FF5C48
          42FF69524CFF80665CFFF8F3EEFFEEE1D4FFEDE0D3FFEDDFD2FFECDED1FFEBDD
          D0FF473832FF493832FF493932FF493832FFF3EBE4FF654D43FF90766CFF8D73
          68FF8A7166FF9E877DFF6D564BFF0000000D0000000300000000000000000000
          000200000008A48970F9D2BFB0FFD3C0B2FFD3C2B1FFD6C5B6FF5B3E37FF5D48
          42FF69534CFF82675DFFF9F5F1FFF9F5F0FFF9F4EFFFF9F4EFFFF8F3EEFFF8F2
          EDFFF7F2ECFFF7F1EBFFF7F1EBFFF6F0EAFFF6EEE9FF664F46FFB4A399FFB3A0
          97FFB19D95FFB09C93FF6F574CFC0000000B0000000300000000000000000000
          0001000000059E836E84A58974FCA88C75FFA98D76FFA88F78FF836859FF765B
          4DFF765A4DFF80655BFFA9948BFFA89289FFA59087FFA48E85FFA28C82FFA08A
          80FF9E877EFF9C847BFF998278FF987F76FF947C73FF695148FF745F52FF745E
          51FF725B50FF70594DFC6A544AA5000000070000000200000000000000000000
          0000000000020000000500000007000000090000000900000009000000090000
          000A0000000A0000000A0000000A0000000A0000000A0000000A0000000A0000
          000B0000000B0000000B0000000B0000000B0000000B0000000B0000000B0000
          000B0000000B0000000A00000007000000030000000100000000000000000000
          0000000000000000000100000002000000020000000200000002000000020000
          0002000000020000000200000002000000020000000200000002000000020000
          0002000000030000000300000003000000030000000300000003000000030000
          0003000000030000000200000002000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000010000000200000004000000060000000700000007000000070000
          0007000000070000000800000008000000080000000800000008000000080000
          0008000000080000000800000008000000080000000800000008000000080000
          0008000000070000000500000003000000010000000000000000000000000000
          0000000000020000000800000011000000180000001B0000001C0000001D0000
          001D0000001D0000001D0000001D0000001E0000001E0000001E0000001E0000
          001F0000001F0000001F0000001F0000001F0000002000000020000000200000
          001F0000001C0000001400000009000000020000000000000000000000000000
          000100000004000000117E5E52C1AF8271FFAE8172FFAE8171FFAE8070FFAD80
          70FFAD7F70FFAC7F6FFFAC7E6EFFAC7E6EFFAB7E6DFFAB7D6DFFAB7D6CFFAB7D
          6CFFAA7C6CFFAA7B6BFFA97C6BFFA97A6AFFA97A6AFFA97A6AFFA87969FFA879
          69FFA87969FF78564AC300000014000000050000000100000000000000000000
          00010000000600000016B08374FFFDFCFAFFFBF8F6FFFBF8F5FFFBF7F5FFFBF7
          F4FFFAF7F4FFFBF6F3FFFBF6F3FFFAF6F2FFFAF5F2FFFAF5F1FFFAF4F1FFF9F4
          F1FFF9F4F0FFF9F3EFFFF8F2EEFFF8F3EEFFF8F2EDFFF8F2ECFFF8F1ECFFF7F0
          ECFFF7F0EBFFA8796AFF0000001B000000070000000100000000000000000000
          00010000000600000018B18576FFFDFCFBFFF6EEE8FFF6EEE8FFF6EEE8FFF6ED
          E7FFF6EDE7FFF5EDE6FFF5ECE6FFF5ECE6FFF6ECE6FFF5ECE5FFF5EBE5FFF5EC
          E5FFF4EBE4FFF5EBE4FFF5EBE4FFF5EAE4FFF4EBE3FFF5EAE3FFF4EAE3FFF4EA
          E3FFF8F1ECFFA97B6AFF0000001E000000080000000100000000000000000000
          00010000000600000018B38777FFFEFCFBFFF6EEE9FFF7EFE8FFF6EFE9FFF6EE
          E8FFF6EDE8FFF6EDE7FFF6EDE7FFF5EDE7FFF6EDE7FFF5ECE6FFF5EDE6FFF5EC
          E6FFF5ECE5FFF5ECE5FFF5ECE5FFF4ECE5FFF4EBE4FFF5EBE4FFF5EAE4FFF5EA
          E3FFF9F2EDFFAA7D6CFF0000001E000000070000000100000000000000000000
          00010000000600000017B48979FFFEFCFBFFF6F0EAFFF7EFEAFFCDB0A3FFCDB1
          A2FFCDB1A2FFCDB1A2FFCEB0A2FFCDB0A2FFCDB0A2FFCDB0A2FFCDB0A1FFCDB0
          A2FFCEB0A1FFCDB0A1FFCDB0A2FFCDB0A2FFCDAFA1FFCDB0A1FFF5EBE4FFF5EB
          E4FFF8F2EDFFAC7D6DFF0000001D000000070000000100000000000000000000
          00010000000500000016B58B7CFFFEFDFCFFF8F0EBFFF7EFEAFFCEB2A3FFD2B6
          A9FFD2B6A9FFD2B6A8FFD2B6A9FFD2B6A9FFD1B6A8FFD2B6A9FFD2B6A9FFD1B6
          A8FFD2B6A9FFD1B6A8FFD2B5A8FFD1B5A8FFD1B5A8FFCDB0A2FFF5EBE5FFF5EC
          E4FFF9F3EEFFAC7F6FFF0000001C000000070000000100000000000000000000
          00010000000500000015B68C7EFFFEFDFCFFF7F1ECFFF7F1EBFFCFB2A4FFD3B7
          A9FFD2B6A9FFD2B7A9FFD3B7A9FFD2B6A9FFD2B7A9FFD3B7A9FFD2B6A9FFD2B7
          A9FFD2B7A9FFD2B7A9FFD2B6A8FFD2B6A9FFD2B6A8FFCEB1A3FFF5ECE6FFF5EC
          E6FFFAF4F0FFAD8070FF0000001B000000070000000100000000000000000000
          00000000000500000014B78E80FFFEFDFDFFF8F1EDFFF8F1ECFFCFB2A4FFD3B7
          ABFFD3B7AAFFD3B7AAFFD3B7AAFFD3B7AAFFD2B7AAFFD2B7AAFFD3B7AAFFD2B7
          AAFFD3B7AAFFD2B7A9FFD2B7AAFFD3B7AAFFD2B6AAFFCFB1A3FFF6EDE7FFF5EC
          E7FFF9F4F1FFAF8272FF0000001A000000070000000100000000000000000000
          00000000000500000013B88F82FFFEFDFDFFF8F2EEFFF8F1EDFFD0B2A5FFD4B7
          ABFFD3B8ABFFD3B7ACFFD4B7ABFFD4B7ABFFD3B7ACFFD3B7ABFFD4B7ABFFD4B7
          ABFFD3B7AAFFD3B7AAFFD3B7ABFFD3B7AAFFD3B7AAFFCFB2A4FFF6EEE8FFF6ED
          E7FFFAF5F2FFAF8373FF00000019000000060000000100000000000000000000
          00000000000400000012BB9184FFFEFEFDFFF8F3EEFFF8F2EEFFD1B4A6FFD4B8
          ADFFD4B9ACFFD5B8ADFFD4B9ACFFD4B8ADFFD4B8ACFFD4B8ABFFD4B8ACFFD4B8
          ACFFD4B8ACFFD4B8ACFFD4B7ACFFD4B7ABFFD3B7ACFFCFB2A5FFF6EEE8FFF6EE
          E8FFFAF5F3FFB18575FF00000018000000060000000100000000000000000000
          00000000000400000011BB9485FFFEFEFDFFF9F4F0FFF9F3EFFFD1B4A7FFD4B9
          ADFFD4B9AEFFD5B9ADFFD4B8AEFFD5B9ADFFD4B9ADFFD4B9ADFFD4B9ADFFD4B9
          ADFFD4B9ACFFD5B8ADFFD4B9ADFFD5B8ADFFD4B8ACFFCFB3A6FFF7EEE9FFF6EE
          E8FFFAF6F3FFB28677FF00000017000000060000000100000000000000000000
          00000000000400000010BC9788FFFEFEFDFFF9F3F0FFF9F4EFFFD1B4A7FFD4B9
          ADFFD4B9AEFFD5B9ADFFD4B8AEFFD5B9ADFFD4B9ADFFD4B9ADFFD4B9ADFFD4B9
          ADFFD4B9ACFFD5B8ADFFD4B9ADFFD5B8ADFFD4B8ACFFCFB3A6FFF6F0EAFFF7F0
          EAFFFBF7F4FFB48979FF00000016000000060000000100000000000000000000
          0000000000040000000FBF988AFFFEFEFEFFFAF5F1FFF9F4F1FFD3B6A9FFD6BB
          AFFFD6BBAFFFD5BBAFFFD6BBAFFFD6BAAFFFD6BBAFFFD6BBAFFFD5BBAEFFD5BA
          AEFFD6BAAFFFD5BAAEFFD5BAAEFFD5BAAFFFD5BAAEFFD1B5A8FFF7F0EBFFF8F0
          EBFFFBF8F5FFB58A7AFF00000015000000050000000100000000000000000000
          0000000000040000000EC0998BFFFEFEFEFFFAF5F2FFFAF5F1FFD4B7AAFFD7BB
          B0FFD6BBB0FFD6BBB0FFD6BBB0FFD6BCB0FFD6BBAFFFD6BBB0FFD6BBAFFFD6BB
          AFFFD6BBAFFFD6BBAFFFD5BBAFFFD5BBB0FFD6BAAFFFD3B5A9FFF7F0ECFFF8F0
          EBFFFBF8F6FFB58C7DFF00000015000000050000000100000000000000000000
          0000000000030000000EC19C8DFFFFFEFEFFFBF6F3FFFAF6F2FFD4B8ABFFD7BC
          B1FFD7BDB1FFD7BCB1FFD6BDB1FFD7BCB1FFD6BCB1FFD6BCB1FFD6BCB1FFD7BC
          B0FFD7BBB1FFD6BCB0FFD6BCB0FFD6BCB0FFD6BCB0FFD3B6AAFFF8F1EDFFF8F1
          ECFFFBF9F7FFB78E7FFF00000014000000050000000100000000000000000000
          0000000000030000000DC29D8FFFFFFEFEFFFBF7F4FFFAF6F4FFD5B8AEFFD4B8
          ACFFD5B8ACFFD4B8ACFFD4B8ACFFD4B8ACFFD4B7ABFFD4B8ACFFD4B8ABFFD4B7
          ACFFD4B8ABFFD4B8ABFFD4B8ABFFD4B8ABFFD4B7ABFFD4B8ABFFF8F2EEFFF8F1
          EDFFFCF9F7FFB88F81FF00000013000000050000000000000000000000000000
          0000000000030000000CC49F90FFFFFEFEFFFBF7F5FFFBF7F4FFFBF6F3FFFBF7
          F3FFFBF6F3FFFAF6F3FFFAF5F3FFFAF5F2FFFAF5F1FFFAF5F1FFFAF5F1FFF9F4
          F1FFFAF4F1FFF9F4F1FFF9F4F0FFF9F3EFFFF9F3EFFFF7F2EDFFF8F2EEFFF8F2
          EEFFFAF7F5FFB99283FF00000012000000040000000000000000000000000000
          0000000000030000000BC4A192FFFFFFFEFFFBF7F5FFFBF8F5FFD6BAAFFFD6BA
          AFFFD5B9AFFFD6BAAFFFFAF6F3FFD6BAAEFFD5B9AEFFD6B9AEFFD5B9AEFFD5BA
          AEFFD5BAAEFFD5B9AEFFD5B9AEFFD5B9AEFFD5B9AEFFD5B9ACFFF9F3EFFFF7F1
          ECFFF9F4F3FFBB9284FF00000011000000040000000000000000000000000000
          0000000000020000000AC6A294FFFFFFFEFFFCF8F6FFFBF8F5FFD7BBB0FFD9C0
          B5FFD9BFB4FFD6BBB0FFFBF7F4FFD6BAAFFFD9BFB4FFD9BFB4FFD9BFB4FFD9BF
          B4FFD9BEB4FFD8BFB4FFD9BFB4FFD9BFB3FFD9BFB4FFD5B9AFFFF7F2EDFFF6EF
          EAFFF7F2EFFFBD9486FF00000010000000040000000000000000000000000000
          00000000000200000009C8A495FFFFFFFFFFFCF9F6FFFBF9F6FFD6BCB1FFDAC0
          B6FFDAC0B5FFD7BBB1FFFBF8F5FFD6BBB0FFD9C0B5FFDAC0B5FFD9C0B5FFD9C0
          B5FFD9C0B5FFD8BEB4FFDABFB4FFD8BFB4FFD8BFB4FFD4BAAEFFF5EFEAFFF3EA
          E6FFF3EDEAFFBD9888FF0000000F000000040000000000000000000000000000
          00000000000200000008C8A597FFFFFFFFFFFCFAF8FFFCFAF7FFD7BCB1FFD7BC
          B1FFD7BCB2FFD7BCB2FFFBF8F5FFD7BCB1FFD7BCB1FFD7BCB1FFD7BBB1FFD6BB
          B0FFD7BCB0FFD7BAAFFFD4B8AEFFD5B8ACFFD2B6ABFFD1B4A8FFEFE4DFFFECE0
          DBFFECE1DDFFBF988AFF0000000E000000040000000000000000000000000000
          00000000000200000008C9A698FFFFFFFFFFFCFAF9FFFCFAF8FFFCF9F8FFFCF9
          F7FFFCF9F7FFFCF9F7FFFBF9F6FFFCF8F6FFFBF8F6FFFCF8F6FFFBF8F6FFFAF6
          F4FFFAF6F3FFF6EFEAFFEFE3DEFFE7D9D2FFE2D3CBFFE1CFC8FFDFCCC4FFDCC8
          BFFFDCC9C2FFC19A8CFF0000000D000000030000000000000000000000000000
          00000000000200000007CBA899FFFFFFFFFFFDFAF9FFFDFAF9FFD7BDB2FFD7BD
          B2FFD7BDB2FFD7BDB2FFD7BDB2FFD7BDB2FFD7BDB2FFD7BDB2FFD7BDB2FFD7BD
          B2FFD6BAB0FFF0E7E1FFB89284FFAC7F6FFFAB7E6DFFAB7D6DFFAB7C6CFFAA7C
          6CFFD1B8AFFFC29D8DFF0000000A000000030000000000000000000000000000
          00000000000100000006CBA99BFFFFFFFFFFFDFBFAFFFDFAFAFFD8BEB3FFDBC3
          B7FFDAC3B8FFDAC3B7FFDAC3B7FFDBC2B8FFDAC3B7FFDBC3B7FFDAC3B7FFDAC3
          B7FFD5BAAFFFEDE0DCFFB18676FFFFFFFFFFFFFEFEFFFFFDFCFFFEFCFAFFFCF9
          F7FFD1B7AEFF533C358600000006000000020000000000000000000000000000
          00000000000100000005CBAA9DFFFFFFFFFFFDFBFAFFFDFBFAFFD8BEB3FFDBC4
          B8FFDBC3B8FFDBC3B8FFDBC3B8FFDBC3B8FFDBC3B8FFDBC4B8FFDAC3B8FFDBC3
          B8FFD5BBB0FFECE0DBFFB68D7DFFFFFEFEFFFEFBFAFFFDF9F7FFFCF6F3FFD4BA
          B0FF553F38860000000800000003000000010000000000000000000000000000
          00000000000100000005CCAB9DFFFFFFFFFFFEFCFBFFFEFCFAFFD8BFB4FFD8BF
          B4FFD8BFB4FFD8BFB4FFD8BFB3FFD9BFB4FFD8BFB3FFD8BFB4FFD8BFB4FFD7BD
          B3FFD5BAAEFFEDE2DCFFBC9485FFFFFEFEFFFDF9F6FFFBF6F3FFD6BCB4FF5843
          3B86000000080000000300000001000000000000000000000000000000000000
          00000000000100000004CDAC9FFFFFFFFFFFFDFCFCFFFDFDFCFFFDFCFBFFFDFC
          FBFFFDFCFAFFFDFCFAFFFEFBFAFFFDFBFAFFFDFBF9FFFDFBF9FFFAF8F7FFF9F5
          F3FFF5EEECFFECE2DDFFC19C8CFFFFFEFEFFFBF6F3FFD9C1B7FF5B463F850000
          0007000000030000000100000000000000000000000000000000000000000000
          00000000000100000003CEAD9FFFFFFFFFFFFEFCFCFFFEFDFCFFFEFDFCFFFDFC
          FCFFFDFDFCFFFEFCFBFFFEFCFBFFFDFCFAFFFDFCFAFFFBF8F7FFF9F6F4FFF7F2
          EFFFF3ECE8FFEDE2DDFFC6A293FFFFFEFEFFDBC3BAFF5D494284000000060000
          0002000000010000000000000000000000000000000000000000000000000000
          00000000000100000002CEADA0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF9F9FFF9F6F4FFF6F1
          F0FFF2ECE9FFEEE3E0FFE5D4CEFFE0CCC4FF5F4D458300000005000000020000
          0001000000000000000000000000000000000000000000000000000000000000
          00000000000000000001998076BECEAEA0FFCEADA0FFCEAE9FFFCEADA0FFCEAD
          9FFFCDAC9FFFCEACA0FFCDAC9FFFCDAC9EFFCDAC9FFFCCAC9EFFCCAB9EFFCCAA
          9DFFCCAB9CFFCBAA9CFFCBAA9CFF614F48820000000400000002000000010000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000000000000000000000000000000000
          0001000000010000000300000005000000070000000700000007000000070000
          0007000000070000000700000007000000070000000700000007000000070000
          0007000000070000000700000007000000070000000700000007000000070000
          0007000000070000000600000004000000010000000100000000000000000000
          0001000000060000000E000000160000001A0000001B0000001B0000001B0000
          001B0000001B0000001B0000001B0000001B0000001B0000001C0000001C0000
          001C0000001C0000001C0000001C0000001C0000001C0000001D0000001D0000
          001D0000001C0000001800000010000000070000000100000000000000010000
          00030000000E2D1F198E583E33FD593D34FF583C32FF583C32FF5A3F37FFC58D
          5DFFC18656FFBD8151FFBB7D4DFFB97A4BFFB77748FFB57444FFB37141FFB06D
          3DFFAF6B3AFFAC6837FFAB6535FFA96333FFA76030FF50342DFF4F342CFF4F34
          2CFF4F342DFF4E342DFD2519158B000000100000000400000001000000010000
          0005000000145C3F36FD806357FF745449FF73534AFF735349FF61473EFFE8C5
          94FFE5BE89FFE3B981FFE3B87FFFE3B67DFFE2B57AFFE1B278FFE0B175FFDFAF
          73FFDEAD71FFDEAC6FFFDDAB6DFFDDAA6CFFDDA86AFF523731FF6B4C42FF6B4C
          42FF6B4C42FF6B4D43FF4E342BFB000000170000000600000001000000010000
          0006000000175F4337FF83655AFF77574CFF76574BFF76574BFF654B41FFEAC8
          98FFE6C08CFFE5BC85FFE3B981FFE2B77EFFE2B57CFFE1B479FFE0B277FFE0B0
          75FFDFAF72FFDEAC70FFDEAC6FFFDDAA6CFFDCA96BFF553932FF6B4C43FF6C4D
          42FF6B4C42FF6D4F45FF50362DFF0000001B0000000700000001000000010000
          000600000017624539FF87695DFF79594EFF795A4EFF795A4EFF684E44FFE1C2
          96FFDCB988FFDBB684FFD7B17BFFD7AF79FFD6AD76FFD6AC75FFD6AA72FFD5A8
          70FFD4A66EFFD4A56BFFD3A369FFD3A267FFD3A166FF583D35FF6D4F44FF6D4E
          45FF6C4D44FF6E5047FF51362FFF0000001B0000000700000001000000010000
          00060000001766483CFF8C6E63FF7D5D51FF7D5D50FF7C5C50FF6C5247FFFAF5
          F2FFF9F3F0FFF9F2EFFFF7F1ECFFF7F0EBFFF6F0EAFFF6EEE9FFF5EEE9FFF5EE
          E8FFF4EDE7FFF4ECE7FFF3ECE6FFF4EBE5FFF3EBE4FF5A3F38FF6F5046FF6E50
          46FF6E4F45FF715348FF523830FF0000001A0000000700000001000000010000
          000500000016684B3EFF917366FF816153FF816153FF7F6154FF6E554AFFFAF7
          F3FFF9F4F0FFF9F3EFFFF8F3EDFFF7F0ECFFF6F0EBFFF6EFEBFFF6EFEAFFF5EE
          E9FFF5EDE8FFF5EDE7FFF4ECE7FFF4EBE6FFF4EBE5FF5E4339FF705248FF7052
          48FF6F5147FF72554BFF523A31FF000000190000000600000001000000010000
          0005000000156C4F42FF95786AFF846456FF836557FF836456FF72584DFFFAF7
          F5FFFAF5F2FFD5B8A9FFD2B4A6FFCCA899FFC9A492FFC6A090FFC39C8BFFC097
          87FFBE9283FFBA8F7FFFB88C7BFFF4EDE6FFF4ECE6FF60463DFF72544BFF7053
          4BFF705248FF75574DFF543A32FF000000190000000600000001000000010000
          0005000000156F5346FF997D6FFF87675AFF876859FF876859FF755C50FFFBF8
          F6FFFAF6F2FFFAF5F1FFF9F5F1FFF9F3EFFFF7F2EDFFF7F1EDFFF6F0EBFFF6EF
          EAFFF5EFEAFFF5EEE9FFF5EDE8FFF5ECE8FFF4EDE6FF634940FF73564CFF7355
          4CFF72544BFF775A4FFF563D34FF000000180000000600000001000000000000
          000500000014735649FF9E8275FF8B6B5CFF8A6D5EFF8B6C5DFF796053FFFCF9
          F7FFFAF7F3FFD9BEB1FFD8BCAEFFD5BAABFFCDAC9DFFCBA797FFC8A392FFC6A0
          8FFFC39B8BFFC09787FFBD9283FFF5EEE8FFF4EDE8FF664C43FF75594EFF7458
          4DFF74574CFF795C53FF573D35FF000000180000000600000001000000000000
          00050000001376594BFFA28878FF8E7061FF8F7061FF8E7062FF7C6155FFFCF9
          F8FFFBF7F4FFFAF7F4FFF9F6F3FFF9F5F2FFF9F4F1FFF8F2EEFFF7F1EDFFF7F0
          ECFFF6F0ECFFF6F0EBFFF5EFEAFFF6EEE9FFF4EEE8FF694F46FF775B50FF7759
          4FFF75594EFF7B6056FF594037FF000000170000000600000001000000000000
          0005000000137B5D4EFFA68D7FFF937464FF937464FF917465FF7F6659FFFCFB
          F9FFFCF9F8FFFCF9F8FFFCF9F7FFFBF9F6FFFCF8F5FFF9F6F3FFF9F5F2FFF9F5
          F1FFF9F4F1FFF8F4F0FFF7F2EFFFF7F2EDFFF7F1EDFF6C5248FF785E52FF775D
          51FF775A50FF7F6359FF5C4239FF000000160000000600000001000000000000
          0005000000127E6152FFAB9182FF967767FF957A67FF957968FF876D5EFF8268
          5BFF82695AFF82685AFF81685AFF80675AFF7F6759FF7A6155FF755D51FF745B
          50FF735B50FF725A4FFF72594FFF71594EFF70574CFF72594EFF7B6055FF7A60
          54FF795E52FF81675BFF5D433AFF000000160000000500000001000000000000
          000400000012836654FFB09687FF997B6BFF9A7D6BFF9A7E6BFF9A7D6CFF997D
          6BFF997E6CFF997E6BFF997E6CFF987E6DFF997E6CFF967B6BFF82675BFF7F66
          5AFF806659FF7F6659FF7F655AFF7F6559FF7F6458FF7D6257FF7C6255FF7C61
          55FF7A6054FF826A5EFF5E463CFF000000150000000500000001000000000000
          000400000011866958FFB39B8CFF9D816FFF9D7F6EFF9D816EFF9D816FFF9D81
          70FF9D826FFF9D8270FF9D8270FF9C8170FF9B8170FF9C826FFF8D7364FF8369
          5CFF82695DFF82685CFF81675CFF81675BFF7F6659FF7F6559FF7F6458FF7D63
          58FF7D6257FF866D61FF61483EFF000000140000000500000001000000000000
          0004000000108A6C5BFFB79F91FFA08471FFA08471FFA08571FFA08573FFA085
          73FFA08574FFA08673FFA18673FFA08574FFA08673FF9F8574FF9C8271FF836A
          60FF836B60FF836A5FFF836A5DFF83695DFF82695DFF81685BFF81675AFF7F65
          59FF7F6359FF886E64FF624940FF000000140000000500000000000000000000
          0004000000108D705EFFBBA494FFA48774FFA48874FFAA927FFFAC9582FFAD94
          83FFAD9482FFAC9382FFAC9482FFAC9483FFAB9381FFAA9282FFAA9080FF9880
          71FF8E766BFF8D756AFF8D7568FF8C7468FF897167FF887065FF866E61FF8167
          5CFF806659FF8B7267FF634A40FF000000130000000500000000000000000000
          00040000000F907462FFBFA898FFA68A77FFA78B79FFB09785FF6E5449FF5439
          31FF543831FF60463EFF644A42FF634A40FF61483FFF5F473CFF5E453CFF5C44
          3BFF5B4339FF5A4139FF594137FF583F36FF563E36FF654D43FF887164FF836A
          5DFF82685CFF8D766AFF654C42FF000000120000000500000000000000000000
          00040000000E957764FFC2AD9DFFA98D7BFFAA8F7BFFB29986FF563A33FF5944
          3DFF644E47FF75594EFFE8DAD0FFDCC5B5FFDBC4B5FFDBC4B3FFDAC3B3FFDAC3
          B2FFD9C2B1FFE3D2C6FFE2D1C5FFE2D0C3FFE1CFC2FF573F37FF8A7266FF856C
          5EFF836A5EFF8F796EFF664F44FF000000120000000400000000000000000000
          00030000000E977A67FFC5B0A0FFAC907CFFAD927DFFB39C88FF573B34FF5A45
          3EFF654F48FF775B50FFEBDFD5FFDEC9BBFFDEC9B9FFDDC8B8FFDDC7B7FFDCC6
          B6FF584139FF705448FF705347FF6F5246FFE4D3C7FF594138FF8B7368FF856D
          62FF856B5FFF937B70FF685146FF000000110000000400000000000000000000
          00030000000D9B7D6AFFC8B4A3FFAE947FFFAF9480FFB79E8BFF583C34FF5B46
          3FFF655049FF795D53FFEFE4DBFFE1CEC0FFE1CDBFFFE1CDBDFFE0CBBBFFDFCA
          BBFF554038FF61473FFF654B42FF715548FFE7D8CDFF5B433AFF8C7468FF876F
          62FF876D60FF957E72FF695147FF000000100000000400000000000000000000
          00030000000C9C7F6BFFCAB6A7FFB29681FFB29782FFB89F8CFF593C35FF5B46
          40FF66514AFF7B5F55FFF2E8E0FFE5D3C5FFE4D3C4FFE3D1C2FFE3D0C1FFE2CF
          C0FF523E36FF5B413AFF5E433BFF73564BFFEBDDD4FF5E463CFF8E7469FF8871
          65FF866E64FF968075FF6B5348FF000000100000000400000000000000000000
          00030000000C9E826CFFCDBAAAFFB39983FFB49984FFBBA18DFF593D35FF5C47
          40FF68524BFF7D6157FFF4ECE5FFE8D8CAFFE8D7C9FFE7D6C8FFE6D5C7FFE6D4
          C6FF4E3C34FF553B34FF583E36FF75584DFFEEE2D9FF60483EFF8F766AFF8A72
          66FF896F65FF998377FF6C544AFF0000000F0000000400000000000000000000
          00030000000BA2836EFFCFBCACFFB69B86FFB59B86FFBBA28FFF5A3E36FF5C47
          41FF68524BFF7E6459FFF6F0EAFFEBDCCFFFEADCCEFFEADBCDFFE9DACCFFE8D9
          CBFF4B3A33FF523731FF533832FF765A4FFFF1E6DEFF624A41FF8E786AFF8A73
          67FF897165FF9B8579FF6D564BFF0000000E0000000400000000000000000000
          00030000000AA3876FFFD1BEB0FFB89D89FFB89E87FFBDA390FF5B3E37FF5C48
          42FF69524CFF80665CFFF8F3EEFFEEE1D4FFEDE0D3FFEDDFD2FFECDED1FFEBDD
          D0FF473832FF493832FF493932FF493832FFF3EBE4FF654D43FF90766CFF8D73
          68FF8A7166FF9E877DFF6D564BFF0000000D0000000300000000000000000000
          000200000008A1866EF9D2BFB0FFD3C0B2FFD3C2B1FFD6C5B6FF5B3E37FF5D48
          42FF69534CFF82675DFFF9F5F1FFF9F5F0FFF9F4EFFFF9F4EFFFF8F3EEFFF8F2
          EDFFF7F2ECFFF7F1EBFFF7F1EBFFF6F0EAFFF6EEE9FF664F46FFB4A399FFB3A0
          97FFB19D95FFB09C93FF6E564CFC0000000B0000000300000000000000000000
          00010000000552443984A48873FCA88C75FFA98D76FFA88F78FF836859FF765B
          4DFF765A4DFF80655BFFA9948BFFA89289FFA59087FFA48E85FFA28C82FFA08A
          80FF9E877EFF9C847BFF998278FF987F76FF947C73FF695148FF745F52FF745E
          51FF725B50FF6F584DFC453730A5000000070000000200000000000000000000
          0000000000020000000500000007000000090000000900000009000000090000
          000A0000000A0000000A0000000A0000000A0000000A0000000A0000000A0000
          000B0000000B0000000B0000000B0000000B0000000B0000000B0000000B0000
          000B0000000B0000000A00000007000000030000000100000000000000000000
          0000000000000000000100000002000000020000000200000002000000020000
          0002000000020000000200000002000000020000000200000002000000020000
          0002000000030000000300000003000000030000000300000003000000030000
          0003000000030000000200000002000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000020000000500000007000000080000
          0008000000090000000900000009000000090000000900000009000000090000
          000900000009000000090000000A0000000A0000000A0000000A000000090000
          0006000000030000000100000000000000000000000000000000000000000000
          00000000000000000000000000020000000700000010000000180000001C0000
          001D0000001D0000001E0000001F0000001F0000002000000020000000210000
          0021000000220000002200000023000000230000002300000023000000200000
          00160000000A0000000300000000000000000000000000000000000000000000
          00000000000000000000000000030000000D1B4E3ABF276D51FF276D51FF276D
          50FF276E51FF276D51FF266D50FF276D50FF276D51FF276D51FF266D51FF266D
          50FF276D50FF266D51FF276E51FF276D51FF266D50FF276D51FF276D51FF1C4E
          3AC4000000150000000500000001000000000000000000000000000000000000
          00000000000000000000000000030000000D358869FF62D3B3FF3CC49AFF3CC4
          9AFF3CC49AFF3CC499FF3CC49AFF3CC499FF3CC399FF3CC399FF3DC39AFF3CC3
          9AFF3CC49AFF3CC499FF3CC49AFF3CC499FF3CC39AFF3CC39AFF3CC39AFF2C7B
          5EFF000000180000000600000001000000000000000000000000000000000000
          0000000000000000000000000002000000093B9778FF77E0C7FF76E0C7FF76E0
          C7FF76E0C7FF76E0C7FF76E0C7FF77E0C7FF76DFC6FF77DFC6FF76DFC6FF76DF
          C6FF76E0C7FF76E0C7FF76E0C7FF76E0C7FF76E1C7FF76E0C7FF76E0C7FF338B
          6CFF000000140000000500000001000000000000000000000000000000000000
          000000000000000000000000000100000004317B62BF42A686FF43A685FF43A6
          86FF42A686FF42A686FF43A685FF42A585FF41A584FF41A383FF41A383FF42A4
          84FF42A584FF43A685FF42A685FF42A685FF43A685FF42A685FF42A686FF2A73
          59C30000000B0000000300000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000200000002000000030000
          0004000000040000000500000006000000090000000F00000017000000180000
          00100000000C0000000900000009000000090000000A0000000A0000000A0000
          0007000000030000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000001000000020000000A0102011E14402BD9133F2AD90102
          011E0000000B0000000300000002000000010000000100000001000000010000
          0001000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000100000006000000140D291D982E8866FF298560FF0D28
          1B98000000140000000600000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000001000000040000000E06120C50266A4EFD39B98DFF37B78AFF2066
          49FD05110C510000000E00000004000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00010000000200000009010302201A4E37E03FB08AFF30B687FF31B687FF36AC
          83FF184833E00103022100000009000000030000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000100000005000000111132249D3B9675FF37BA8EFF30B788FF30B787FF36BA
          8DFF2F8F6CFF0F2E209E00000012000000060000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          00030000000C081811542F795DFD42C096FF31B88AFF31B88AFF31B889FF31B8
          89FF3DBD92FF266F52FD07150F560000000D0000000300000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          00080205031F215C44E44DBC98FF36BB8EFF32BA8CFF32BA8CFF33B98CFF33BA
          8BFF34BA8DFF3FB58DFF1B523BE4010403210000000900000002000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000050000
          000E143D2DA248A586FF3DC196FF34BC8EFF34BC8EFF34BB8FFF33BC8EFF34BC
          8EFF33BC8EFF3DC095FF379977FF113525A40000001000000006000000010000
          0001000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000030000000A0A1E
          16563B886DFF4DC9A2FF35BE91FF35BE91FF35BE91FF35BE90FF35BE90FF35BE
          91FF35BD90FF35BD90FF46C69DFF2C7C5DFF081912590000000C000000030000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000001000000060207051F2A6C
          53E85BC8A7FF3AC195FF37C093FF37C094FF37C093FF36BF93FF37BF93FF37C0
          93FF37BF92FF36C092FF39C095FF4ABF9AFF205D45E902060422000000070000
          0002000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000001000000040000000C1A4A38A858B4
          97FF45C79EFF3AC397FF45C79EFF4BCAA3FF4DCCA5FF51CCA8FF50CDA7FF4BCA
          A3FF48C8A0FF40C59BFF3AC296FF43C69DFF41A684FF153F2DAA0000000E0000
          0005000000010000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000002000000070E271E5D499C80FF56D1
          ADFF4CCDA4FF5CD2B0FF5DD4B0FF5CD3B0FF5CD3AFFF5BD2AFFF5AD1AEFF59D2
          AEFF57D1ADFF57D1ACFF54CFAAFF46C9A1FF4ECCA6FF34896AFF0B2017600000
          000A000000030000000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000040309071D347D62EB6BD2B4FF63D6
          B6FF69D9BAFF68D9BAFF67D9B9FF66D8B8FF65D7B7FF64D7B7FF63D6B6FF62D6
          B4FF61D6B4FF60D5B4FF5FD5B3FF5DD4B2FF58D1AEFF56C8A7FF25684DEB0207
          0521000000060000000100000000000000000000000000000000000000000000
          000000000000000000000000000100000006225A45B08BCEBAFFB9F6E9FFB8F6
          E9FFB8F6E9FFB7F5E9FFA2EFDEFF70DDC0FF6FDCBEFF6EDCBEFF6DDCBDFF6CDC
          BCFF6ADBBCFF80E3CBFF9AECDAFFB0F2E3FFAFF1E3FFAEF1E2FF7CC2AEFF1949
          35B2000000080000000200000000000000000000000000000000000000000000
          0000000000000000000000000001000000053C9678FF3D9577FF3C9576FF3B94
          75FF3C9374FF3C9273FFA8F2E2FF7AE2C7FF79E2C6FF78E1C5FF77E0C5FF75E0
          C3FF74DFC3FF89E8D1FF34886AFF348769FF338668FF348667FF338566FF3284
          66FF000000070000000200000000000000000000000000000000000000000000
          0000000000000000000000000001000000020000000500000007000000080000
          000A0000000F3E9878FFAEF5E6FF83E7CDFF82E6CDFF81E5CBFF80E5CBFF7FE5
          CBFF7EE4CAFF92ECD7FF277758FF000000120000000D0000000B0000000A0000
          0007000000030000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000002000000020000
          000400000009429C7FFFB2F7EAFF8DEBD4FF8CEAD3FF8AEAD3FF8AE9D2FF88E9
          D1FF87E9D0FF99EFDCFF297A5BFF0000000B0000000400000003000000020000
          0002000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00020000000745A283FFB6F9ECFF95EEDAFF95EED9FF94EED9FF93EDD8FF92ED
          D8FF91ECD7FFA0F2E1FF2B7D5EFF000000080000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00010000000549A788FFB9FAEFFF9EF2E0FF9DF1DFFF9CF1DFFF9BF1DEFF9AF0
          DDFF99F0DDFFA7F5E5FF2C8161FF000000070000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000044CAC8EFFBCFCF1FFA6F5E5FFA5F5E4FFA4F4E3FFA4F4E3FFA3F4
          E3FFA1F3E2FFADF8E9FF2D8363FF000000060000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00010000000350B193FFC3FEF4FFC3FEF4FFC3FEF4FFC3FEF4FFC3FEF4FFC3FE
          F4FFC3FDF4FFC3FDF4FF2E8766FF000000050000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000023E8671BE53B596FF53B496FF52B395FF52B395FF51B194FF51B2
          94FF50B093FF50B092FF32775FBF000000030000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010000000100000002000000020000000300000003000000030000
          0003000000030000000300000002000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0001000000010000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end>
  end
end
