/*
 ����        - DocHandEdit.cpp
 ������      - ��������������� (ICS)
 ��������    - ���� ���������� ������, ����������� ��������
 ����� ��� ������� ����� ������ � ��������
 ����������� - ������� �.�.
 ����        - 22.11.2004
 */
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dcDocHandEdit.h"
#include "dsDocHTMLCreator.h"
#include "dsDocClientConstDef.h"
#include "dsDocDocParsers.h"
#include "DKUtils.h"

#include "msgdef.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxGroupBox"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "Htmlview"
#pragma resource "*.dfm"

#define FCObj(a) ((TFormControlObj*)a)

// ***************************************************************************
// ********************** ���� ***********************************************
// ***************************************************************************

//enum TDocElementImageListInd { iiNone = 0, iiInscription, iiInsText, iiList, iiTable };

//---------------------------------------------------------------------------

namespace dsDocHandEdit
 {
 //������ �� ������-��������� ������ � ������� ��� ������ �������
 struct TTCellTagNodes
  {
   TTagNode *        RowNode; //������ �� ������-��������� ������
   TTagNode *        ColNode; //������ �� ������-��������� �������
   TcxMaskEdit *     ED;      //������ �� ��������
   TFormControlObj * FO;
  };
 //---------------------------------------------------------------------------
 /*struct TTCellTagNodesEx
  {
  TTagNode *RowNode;    //������ �� ������-��������� ������
  TTagNode *ColNode;    //������ �� ������-��������� �������
  };*/
 //---------------------------------------------------------------------------

 //������ (���������� ������)
 template<class DHVData>
  class TDHVector
  {
  private:
   DHVData * FVector;   //������
   int       FColCount; //���������� ��������

  protected:
   void __fastcall SetColCount(int AColCount);

  public:
   __fastcall TDHVector(int AColCount = 0);
   virtual __fastcall ~TDHVector();

   __property int ColCount =
    {read = FColCount, write = SetColCount};

   DHVData & __fastcall operator[](int i);
  };

 //---------------------------------------------------------------------------

 //������� (��������� ������)
 template<class DHMData>
  class TDHMatrix
  {
  private:
   TDHVector<DHMData> *FMatrix; //�������
   int FRowCount;               //���������� �����
   int FColCount;               //���������� ��������

  public:
   __fastcall TDHMatrix(int ARowCount, int AColCount);
   virtual __fastcall ~TDHMatrix();

   __property int RowCount =
    {read = FRowCount};
   __property int ColCount =
    {read = FColCount};

   TDHVector<DHMData> & __fastcall operator[](int i);
  };

 }//end of namespace DocHandEdit

// ***************************************************************************
// ********************** TDHVector ********************************************
// ***************************************************************************

template<class DHVData>
 __fastcall TDHVector<DHVData>::TDHVector(int AColCount)
 {
  if (AColCount < 0)
   throw System::Math::EInvalidArgument(UnicodeString(__FUNC__) + ": AColCount " + FMT(dsDocMatrixErrorParam) + " > 0.");
  FColCount = AColCount;
  FVector   = (FColCount) ? new DHVData[FColCount] : NULL;
 }

//---------------------------------------------------------------------------

template<class DHVData>
 __fastcall TDHVector<DHVData>::~TDHVector()
 {
  if (FVector)
   delete[]FVector;
 }

//---------------------------------------------------------------------------

template<class DHVData>
 void __fastcall TDHVector<DHVData>::SetColCount(int AColCount)
 {
  if (AColCount < 0)
   throw System::Math::EInvalidArgument(UnicodeString(__FUNC__) + ": AColCount " + FMT(dsDocMatrixErrorParam) + " > 0.");
  if (FColCount != AColCount)
   {
    FColCount = AColCount;
    FVector   = (FColCount) ? new DHVData[FColCount] : NULL;
   }
 }

//---------------------------------------------------------------------------

template<class DHVData>
 DHVData & __fastcall TDHVector<DHVData>:: operator[](int i)
 {
  if ((i < 0) || (i >= FColCount))
   throw System::Math::EInvalidArgument(UnicodeString(__FUNC__) + ": i " + FMT1(dsDocMatrixErrorIndex, IntToStr(i)));
  return FVector[i];
 }
// ***************************************************************************
// ********************** TDHMatrix ********************************************
// ***************************************************************************

template<class DHMData>
 __fastcall TDHMatrix<DHMData>::TDHMatrix(int ARowCount, int AColCount)
 {
  if (ARowCount <= 0)
   throw System::Math::EInvalidArgument(UnicodeString(__FUNC__) + ": ARowCount " + FMT(dsDocMatrixErrorParam) + " >= 0.");
  if (AColCount <= 0)
   throw System::Math::EInvalidArgument(UnicodeString(__FUNC__) + ": AColCount " + FMT(dsDocMatrixErrorParam) + " >= 0.");
  FRowCount = ARowCount;
  FColCount = AColCount;
  FMatrix   = new TDHVector<DHMData>[FRowCount];
  for (int i = 0; i < FRowCount; i++)
   FMatrix[i].ColCount = FColCount;
 }

//---------------------------------------------------------------------------

template<class DHMData>
 __fastcall TDHMatrix<DHMData>::~TDHMatrix()
 {
  delete[]FMatrix;
 }

//---------------------------------------------------------------------------

template<class DHMData>
 TDHVector<DHMData> & __fastcall TDHMatrix<DHMData>:: operator[](int i)
 {
  if ((i < 0) || (i >= FRowCount))
   throw System::Math::EInvalidArgument(UnicodeString(__FUNC__) + ": i " + FMT1(dsDocMatrixErrorIndex, IntToStr(i)));
  return FMatrix[i];
 }

// ***************************************************************************
// ********************** ��������/��������/����� ����� **********************
// ***************************************************************************

__fastcall TDocTableData::TDocTableData(TTagNode * ATabDef, TTagNode * ATabDoc)
 {
  FTabDef = ATabDef;
  FTabDoc = ATabDoc;
  TICSDocSpec::GetTableSingleRowsList(FTabDef->GetChildByName("rows"), & FSingleRows);
  TICSDocSpec::GetTableSingleColsList(FTabDef->GetChildByName("columns"), & FSingleCols);

  FRowMax = FSingleRows.size();
  FColMax = FSingleCols.size();
  FTCells = new TDHMatrix<TTCellTagNodes>(FRowMax, FColMax);

  TTagNodeList::iterator itRow, itCol;
  int nRow, nCol;
  for (itRow = FSingleRows.begin(), nRow = 0; itRow != FSingleRows.end(); itRow++ , nRow++)
   {
    for (itCol = FSingleCols.begin(), nCol = 0; itCol != FSingleCols.end(); itCol++ , nCol++)
     {
      (*FTCells)[nRow][nCol].RowNode = (*itRow);
      (*FTCells)[nRow][nCol].ColNode = (*itCol);
     }
   }
 }
//---------------------------------------------------------------------------
__fastcall TDocTableData::~TDocTableData()
 {
  delete FTCells;
  FTCells = NULL;
 }
//---------------------------------------------------------------------------
TDHMatrix<TTCellTagNodes>__fastcall TDocTableData::FGetTabCells()
 {
  return (*FTCells);
 }
//---------------------------------------------------------------------------
TTagNode * __fastcall TDocTableData::GetTabColNode(int ARow, int ACol)
 {
  return (* FTCells)[ARow][ACol].ColNode;
 }
//---------------------------------------------------------------------------
TTagNode * __fastcall TDocTableData::GetTabRowNode(int ARow, int ACol)
 {
  return (* FTCells)[ARow][ACol].RowNode;
 }
//---------------------------------------------------------------------------
TcxMaskEdit * __fastcall TDocTableData::GetTabEDCells(int ARow, int ACol)
 {
  return ((* FTCells)[ARow][ACol]).ED;
 }
//---------------------------------------------------------------------------
void __fastcall TDocTableData::SetTabEDCells(int ARow, int ACol, TcxMaskEdit * AED)
 {
  ((* FTCells)[ARow][ACol]).ED = AED;
 }
//---------------------------------------------------------------------------
TFormControlObj * __fastcall TDocTableData::GetTabFOCells(int ARow, int ACol)
 {
  return ((* FTCells)[ARow][ACol]).FO;
 }
//---------------------------------------------------------------------------
void __fastcall TDocTableData::SetTabFOCells(int ARow, int ACol, TFormControlObj * AFO)
 {
  ((* FTCells)[ARow][ACol]).FO = AFO;
 }
//---------------------------------------------------------------------------
__fastcall TdsDocHandEditForm::TdsDocHandEditForm(TComponent * Owner, TTagNode * ASpec, TTagNode * ADoc, TAxeXMLContainer * AXMLContainer)
    : TForm(Owner)
 {
  FXMLContainer = AXMLContainer;
  FSpec         = ASpec;
  FDoc          = ADoc;
  FTabPresent   = false;
  FEditDoc      = new TTagNode;
  FEditDoc->Assign(FDoc, true);
  TTagNode * ErrNode = FDoc->GetChildByName("errcontent", true);
  if (ErrNode)
   delete ErrNode;
  FModified        = false;
  FCurGridModified = false;

  FErrCellColor   = (TColor)0x009393FF;
  FErrRowColColor = (TColor)0x00E1E1FF;
  FNormalColor    = clWindow;
  FNormalDisColor = (TColor)0x00F0FFF5;

  FLoadCodes();
 }

//---------------------------------------------------------------------------
__fastcall TdsDocHandEditForm::~TdsDocHandEditForm()
 {
  delete FEditDoc;
  FEditDoc = NULL;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FormShow(TObject * Sender)
 {
  if (FTabPresent)
   FocusFirstTabEd(0);
  Caption = /*FMT(dsDocHandDocCaption) +*/" - " + FDoc->GetChildByName("passport")->AV["mainname"];

  actSave->Enabled   = false;
  actReCalc->Enabled = false;

  actSave->Caption   = FMT(dsDocHandEditSaveCaption);
  actSave->Hint      = FMT(dsDocHandEditSaveHint);
  actReCalc->Caption = FMT(dsDocHandEditReCalcCaption);
  actReCalc->Hint    = FMT(dsDocHandEditReCalcHint);
  actExit->Caption   = FMT(dsDocHandEditExitCaption);
  actExit->Hint      = FMT(dsDocHandEditExitHint);
  FRecalcMode        = false;
 }

// ***************************************************************************
// ********************** ������ ����� ***************************************
// ***************************************************************************

//---------------------------------------------------------------------------
//��������� TagNode'�, ���������������� �������� ���������� �������� ���������

TTagNode * __fastcall TdsDocHandEditForm::GetDocElementDesc(TTagNode * ADocElement)
 {
  return (ADocElement) ? FSpec->GetTagByUID(ADocElement->AV["uidmain"]) : NULL;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocHandEditForm::DocDataToStr(const double AData)
 {
  if (AData == static_cast<long>(AData))
   return DKFloatToStr(AData, '.');
  else
   return DKFloatToStrF(AData, ffFixed, 18, 2, '.');
 }

//---------------------------------------------------------------------------
double __fastcall TdsDocHandEditForm::DisplayTextToDocData(UnicodeString AVal)
 {
  double RC = 0;
  try
   {
    UnicodeString FSrc = AVal;
    int idx = FSrc.LastDelimiter(",.");
    if (idx)
     FSrc[idx] = FormatSettings.DecimalSeparator;
    TryStrToFloat(FSrc, RC);
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------

UnicodeString __fastcall TdsDocHandEditForm::DocDataToDisplayText(const double AData)
 {
  if (AData == static_cast<long>(AData))
   return FloatToStr(AData);
  else
   return FloatToStrF(AData, ffFixed, 18, 2);
 }

//---------------------------------------------------------------------------

/*
 * �������� ��������� ������ grid'� ��� �������� �������� ���������
 *
 *
 * ������������ ��������:
 *   true  - �������� ��������
 *   false - ������� ������������ ������
 *
 */

bool __fastcall TdsDocHandEditForm::ApplyCurrentGridUpdates()
 {
  bool fResult = true;
  if (FCurGridModified)
   {
    ReCalcCurrentGridData();
    UnicodeString tmpData;
    fResult = CheckByHandRules();
    if (fResult)
     {
      int i = 0;
      TTagNode * ndLV2 = FDocTabData[FEdTab]->TabDoc->GetFirstChild(); //������ �������
      while (ndLV2)
       {
        int j = 0;
        TTagNode * ndIV = ndLV2->GetFirstChild(); //������ �������
        while (ndIV)
         {
          tmpData         = FDocTabData[FEdTab]->GetTabEDCells(i, j)->Text.Trim();
          ndIV->AV["val"] = DocDataToStr(DisplayTextToDocData(tmpData));
          ndIV            = ndIV->GetNext();
          j++ ;
         }
        ndLV2 = ndLV2->GetNext();
        i++ ;
       }
      FCurGridModified = false;
     }
   }
  return fResult;
 }

//---------------------------------------------------------------------------

void __fastcall TdsDocHandEditForm::AssignIVValAV(TTagNode * ADestNode, TTagNode * ASrcNode)
 {
  if (ADestNode->CmpName("iv") &&
      ASrcNode->CmpName("iv") &&
      ADestNode->GetAttrByName("val") &&
      ASrcNode->GetAttrByName("val")
      )
   ADestNode->AV["val"] = ASrcNode->AV["val"];

  ADestNode = ADestNode->GetFirstChild();
  ASrcNode  = ASrcNode->GetFirstChild();
  while (ADestNode && ASrcNode)
   {
    AssignIVValAV(ADestNode, ASrcNode);
    ADestNode = ADestNode->GetNext();
    ASrcNode  = ASrcNode->GetNext();
   }
 }

//---------------------------------------------------------------------------

//�������� ���������

void __fastcall TdsDocHandEditForm::ApplyUpdates()
 {
  if (ApplyCurrentGridUpdates())
   {
    AssignIVValAV(FDoc, FEditDoc);
    FModified        = true;
    actSave->Enabled = false;
   }
 }

//---------------------------------------------------------------------------

/*
 * ������ ������� �����/�������� � ������� ����� ������� ������� ���������������
 * ����� ������� � ��������������� ����� ������� ��������� ������ ��� ����������
 * ������� � ��������� � ���� �����'� ������� ��� ���������
 *
 *
 * ���������:
 *   [in]  APoland - ������� ��� ���������
 *   [in]  AType   - ��� ������� ��� ���������
 *   [in]  nRow    - ����� ������ ������� ������ grid'� ��� �������� �������� ���������
 *   [in]  nCol    - ����� ������� ������� ������ grid'� ��� �������� �������� ���������
 *
 * ������������ ��������:
 *   ������� ��� ��������� � ���� �����'� � ����������� �������� �����/�������� � ��������
 * ����� ������� ������� ��������������� ����� ������� � ��������������� ����� �������
 * ��������� ������ ��� ���������� �������
 *
 */

UnicodeString __fastcall TdsDocHandEditForm::ReplaceTRowColNum(UnicodeString APoland,
    TTRulesType AType,
    int nRow,
    int nCol)
 {
  UnicodeString Result = "";
  UnicodeString tmpData;
  TStringList * SL;
  try
   {
    SL       = new TStringList;
    SL->Text = StringReplace(APoland, " ", "\n", TReplaceFlags() << rfReplaceAll);
    for (int i = 0; i < SL->Count; i++)
     if (SL->Strings[i].Length() && (SL->Strings[i][1] == '#'))//����� ������/������� ��� ����� ������
      {
       if (SL->Strings[i].Pos(":"))//����� ������
        {
         int nCellRowNo = StrToInt(GetCellNumLexRowNumber(SL->Strings[i]));
         int nCellColNo = StrToInt(GetCellNumLexColNumber(SL->Strings[i]));
         tmpData        = FDocTabData[FEdTab]->GetTabEDCells(nCellRowNo - 1, nCellColNo - 1)->Text.Trim();
         SL->Strings[i] = IntToStr(tmpData.ToInt());
        }
       else //����� ������/�������
        {
         int nRowColNum = StrToInt(GetRowColNumLexNumber(SL->Strings[i]));
         switch (AType)
          {
          case rtRow:
            {
             tmpData        = FDocTabData[FEdTab]->GetTabEDCells(nRowColNum - 1, nCol)->Text.Trim();
             SL->Strings[i] = IntToStr(tmpData.ToInt());
             break;
            }
          case rtCol:
            {
             tmpData        = FDocTabData[FEdTab]->GetTabEDCells(nRow, nRowColNum - 1)->Text.Trim();
             SL->Strings[i] = IntToStr(tmpData.ToInt());
             break;
            }
          }
        }
      }
    Result = StringReplace(SL->Text, "\n", " ", TReplaceFlags() << rfReplaceAll);
   }
  __finally
   {
    if (SL)
     delete SL;
   }
  return Result;
 }

//---------------------------------------------------------------------------

/*
 * �������� ������������ ������ ������� �������� �������� ������� �����
 * ��������� ������ ��� ���������� �������
 *
 *
 * ���������:
 *   [in]  ndHandRules - ������� �������� ������� ����� (xml-��� "handrules" �������������)
 *   [in]  AType       - ��� ������ ��������
 *   [in]  nRow        - ����� ������ ������� ������ grid'� ��� �������� �������� ���������
 *   [in]  nCol        - ����� ������� ������� ������ grid'� ��� �������� �������� ���������
 *
 * ������������ ��������:
 *   true  - ������ ������� ������������� �������� �������� ������� �����
 *   false - ������ ������� �� ������������� �������� �������� ������� �����
 *
 */

bool __fastcall TdsDocHandEditForm::THandRulesCheck(TTagNode * ndHandRules,
    TTRulesType AType,
    int nRow,
    int nCol)
 {
  bool bResult = false;
  //������ ������� �����/�������� ������� ��������������� ����� �������
  UnicodeString AHandRules = ReplaceTRowColNum(ndHandRules->AV["pol_cond"], AType, nRow, nCol);
  //�������� ���������� �������, ������������ ������� �������� ������� �����
  THandRulesCalc * HandRulesCalc;
  try
   {
    HandRulesCalc = new THandRulesCalc(AHandRules);
    bResult       = (HandRulesCalc->Calc() == 1);
   }
  __finally
   {
    if (HandRulesCalc)
     delete HandRulesCalc;
   }
  return bResult;
 }

//---------------------------------------------------------------------------
//�������� ������������ ��������� ������ � ������������ � ��������� ��������
//������� �����. ��� ���� ������� ��������� �� ������ � ������������
//������ ����� � ������������� �������
bool __fastcall TdsDocHandEditForm::CheckByHandRules()
 {
  bool RC = false;
  try
   {
    //����� �����               FDocTabData[FEdTab]->GetTabEDCells(nRow,nRowColNum - 1)->Text;
    FErrInCol = false;
    bool bBadHandRules = false;
    for (int i = 0; (i < FDocTabData[FEdTab]->RowCount) && !bBadHandRules; i++)
     {
      for (int j = 0; (j < FDocTabData[FEdTab]->ColCount) && !bBadHandRules; j++)
       {
        TTagNode * ndRowHandRules = FDocTabData[FEdTab]->GetTabRowNode(i, j)->GetChildByName("handrules");
        TTagNode * ndColHandRules = FDocTabData[FEdTab]->GetTabColNode(i, j)->GetChildByName("handrules");
        //�������� ������������
        if (ndRowHandRules || ndColHandRules)
         {
          UnicodeString S2, S3;
          TTagNode * ndBadHandRules = NULL;
          int nNum;
          if (FDocTabData[FEdTab]->TabDef->CmpAV("tablepriority", "row"))
           {
            if (ndRowHandRules && !THandRulesCheck(ndRowHandRules, rtRow, i, j))
             {
              S2             = FMT(dsDocHandEditErrorRow);
              nNum           = i;
              S3             = " ������ �";
              ndBadHandRules = ndRowHandRules;
              bBadHandRules  = true;
             }
            if (ndColHandRules && !THandRulesCheck(ndColHandRules, rtCol, i, j))
             {
              S2             = FMT(dsDocHandEditErrorCol);
              nNum           = j;
              S3             = " ������� �";
              FErrInCol      = true;
              ndBadHandRules = ndColHandRules;
              bBadHandRules  = true;
             }
           }
          else
           {
            if (ndColHandRules && !THandRulesCheck(ndColHandRules, rtCol, i, j))
             {
              S2             = FMT(dsDocHandEditErrorCol);
              nNum           = i;
              S3             = " ������� �";
              FErrInCol      = true;
              ndBadHandRules = ndColHandRules;
              bBadHandRules  = true;
             }
            if (ndRowHandRules && !THandRulesCheck(ndRowHandRules, rtRow, i, j))
             {
              S2             = FMT(dsDocHandEditErrorRow);
              nNum           = j;
              S3             = " ������ �";
              ndBadHandRules = ndRowHandRules;
              bBadHandRules  = true;
             }
           }
          if (bBadHandRules)
           {
            //����������� ������ ����� � ������������� �������
            FErrRow = i;
            FErrCol = j;
            if (FErrInCol)
             {
              for (int e = 0; e < FDocTabData[FEdTab]->RowCount; e++)
               {
                FDocTabData[FEdTab]->GetTabEDCells(e, FErrCol)->Style->Color = FErrRowColColor;
                FDocTabData[FEdTab]->GetTabEDCells(e, FErrCol)->StyleDisabled->Color = FErrRowColColor;
               }
             }
            else
             {
              for (int e = 0; e < FDocTabData[FEdTab]->ColCount; e++)
               {
                FDocTabData[FEdTab]->GetTabEDCells(FErrRow, e)->Style->Color = FErrRowColColor;
                FDocTabData[FEdTab]->GetTabEDCells(FErrRow, e)->StyleDisabled->Color = FErrRowColColor;
               }
             }

            FDocTabData[FEdTab]->GetTabEDCells(i, j)->Style->Color = FErrCellColor;
            FDocTabData[FEdTab]->GetTabEDCells(i, j)->StyleDisabled->Color = FErrCellColor;

            if (FDocTabData[FEdTab]->GetTabEDCells(i, j)->Enabled)
             {
              FDocTabData[FEdTab]->GetTabEDCells(i, j)->SetFocus();
              FDocTabData[FEdTab]->GetTabEDCells(i, j)->SelectAll();
             }
            else
             {
              FDocTabData[FEdTab]->GetTabEDCells(i, j)->Style->Font->Color = clRed;
              Application->ProcessMessages();
             }

            TReplaceFlags fRepFl;
            fRepFl << rfReplaceAll << rfIgnoreCase;
            S3 = StringReplace(ndBadHandRules->AV["condition"], "#", S3, fRepFl);
            //����� ��������� �� ������

            ErrMsgLab->Caption   = "������ ������ [������ �" + IntToStr(i + 1) + " : ������� �" + IntToStr(j + 1) + "] �� ������������� �������� �������� ��� " + S2 + " �" + IntToStr(nNum + 1);
            ErrRulesLab->Caption = S3;
            ErrPanel->Visible    = true;
           }
         }
       }
     }
    RC = !bBadHandRules;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
/*
 * �������� ������ ������ �������
 *
 *
 * ���������:
 *   [in]  ndCalcRules - ������� ���������� (xml-��� "calcrules" �������������)
 *   [in]  AType       - ��� ������ ����������
 *   [in]  nRow        - ����� ������ ������� ������ grid'�
 *   [in]  nCol        - ����� ������� ������� ������ grid'�
 *
 */

void __fastcall TdsDocHandEditForm::ReCalcTCell(TTagNode * ndCalcRules, TTRulesType AType, int nRow, int nCol)
 {
  if (ndCalcRules)
   {
    //������ ������� �����/�������� ������� ��������������� ����� �������
    UnicodeString ACalcRules = ReplaceTRowColNum(ndCalcRules->GetChildByName("calc")->AV["pol_exp"], AType, nRow, nCol);
    //�������� ���������� �������, ������������ ������� �������� ������� �����
    TCalcRulesCalc * CalcRulesCalc;
    try
     {
      CalcRulesCalc = new TCalcRulesCalc(ACalcRules);
      FDocTabData[FEdTab]->GetTabEDCells(nRow, nCol)->Text = DocDataToDisplayText(CalcRulesCalc->Calc());
      Application->ProcessMessages();
     }
    __finally
     {
      if (CalcRulesCalc)
       delete CalcRulesCalc;
     }
   }
 }

//---------------------------------------------------------------------------

//�������� ����������� ������
void __fastcall TdsDocHandEditForm::ReCalcCurrentGridData()
 {
  FRecalcMode = true;
  try
   {
    ReCalcTimer->Enabled = false;
    //����� �����               FDocTabData[FEdTab]->GetTabRowNode(i,j)->
    bool FRowPrty = FDocTabData[FEdTab]->TabDef->CmpAV("tablepriority", "row");
    //�������� ����������� �����
    for (int i = 0; i < FDocTabData[FEdTab]->RowCount; i++)
     {
      for (int j = 0; j < FDocTabData[FEdTab]->ColCount; j++)
       {
        if (FRowPrty)
         ReCalcTCell(FDocTabData[FEdTab]->GetTabRowNode(i, j)->GetChildByName("calcrules"), rtRow, i, j);
        else
         ReCalcTCell(FDocTabData[FEdTab]->GetTabColNode(i, j)->GetChildByName("calcrules"), rtCol, i, j);
       }
     }
    for (int i = 0; i < FDocTabData[FEdTab]->RowCount; i++)
     {
      for (int j = 0; j < FDocTabData[FEdTab]->ColCount; j++)
       {
        if (FRowPrty)
         ReCalcTCell(FDocTabData[FEdTab]->GetTabColNode(i, j)->GetChildByName("calcrules"), rtCol, i, j);
        else
         ReCalcTCell(FDocTabData[FEdTab]->GetTabRowNode(i, j)->GetChildByName("calcrules"), rtRow, i, j);
       }
     }
    actReCalc->Enabled = false;
   }
  __finally
   {
    FRecalcMode = false;
   }
 }
//---------------------------------------------------------------------------
// ***************************************************************************
// ********************** ����������� �������*********************************
// ***************************************************************************
void __fastcall TdsDocHandEditForm::actSaveExecute(TObject * Sender)
 {
  ApplyUpdates();
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::actReCalcExecute(TObject * Sender)
 {
  ReCalcCurrentGridData();
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::actExitExecute(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::IncY()
 {
  if (FEdY < FDocTabData[FEdTab]->RowCount - 1)
   FEdY++ ;
  else
   FEdY = 0;
  while (!FDocTabData[FEdTab]->GetTabEDCells(FEdY, FEdX)->Enabled)
   {
    if (FEdY < FDocTabData[FEdTab]->RowCount - 1)
     FEdY++ ;
    else
     FEdY = 0;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::DecY()
 {
  if (FEdY > 0)
   FEdY-- ;
  else
   FEdY = FDocTabData[FEdTab]->RowCount - 1;
  while (!FDocTabData[FEdTab]->GetTabEDCells(FEdY, FEdX)->Enabled)
   {
    if (FEdY > 0)
     FEdY-- ;
    else
     FEdY = FDocTabData[FEdTab]->RowCount - 1;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::IncX()
 {
  if (FEdX < FDocTabData[FEdTab]->ColCount - 1)
   FEdX++ ;
  else
   FEdX = 0;
  while (!FDocTabData[FEdTab]->GetTabEDCells(FEdY, FEdX)->Enabled)
   {
    if (FEdX < FDocTabData[FEdTab]->ColCount - 1)
     FEdX++ ;
    else
     FEdX = 0;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::DecX()
 {
  if (FEdX > 0)
   FEdX-- ;
  else
   FEdX = FDocTabData[FEdTab]->ColCount - 1;
  while (!FDocTabData[FEdTab]->GetTabEDCells(FEdY, FEdX)->Enabled)
   {
    if (FEdX > 0)
     FEdX-- ;
    else
     FEdX = FDocTabData[FEdTab]->ColCount - 1;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FEDExit(TObject * Sender)
 {
  ReCalcCurrentGridData();
  Application->ProcessMessages();
  ((TcxMaskEdit *)Sender)->Visible = false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FEDEnter(TObject * Sender)
 {
  int FCTab, FCTabRow, FCTabCol;
  GetTRC(FEDNameMap[((TcxMaskEdit *)Sender)], FCTab, FCTabRow, FCTabCol);
  FEdTab = FCTab;
  FEdY   = FCTabRow;
  FEdX   = FCTabCol;
  Application->ProcessMessages();
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FEDChange(TObject * Sender)
 {
  if (!FRecalcMode)
   {
    ((TcxMaskEdit *)Sender)->Hint = ((TcxMaskEdit *)Sender)->Text;

    ErrMsgLab->Caption   = "";
    ErrRulesLab->Caption = "";
    ErrPanel->Visible    = false;
    if ((FErrRow != -1) && (FErrCol != -1))
     {
      if (FErrInCol)
       {
        for (int e = 0; e < FDocTabData[FEdTab]->RowCount; e++)
         {
          FDocTabData[FEdTab]->GetTabEDCells(e, FErrCol)->Style->Color = FNormalColor;
          FDocTabData[FEdTab]->GetTabEDCells(e, FErrCol)->StyleDisabled->Color = FNormalDisColor;
         }
       }
      else
       {
        for (int e = 0; e < FDocTabData[FEdTab]->ColCount; e++)
         {
          FDocTabData[FEdTab]->GetTabEDCells(FErrRow, e)->Style->Color = FNormalColor;
          FDocTabData[FEdTab]->GetTabEDCells(FErrRow, e)->StyleDisabled->Color = FNormalDisColor;
         }
       }

      FDocTabData[FEdTab]->GetTabEDCells(FErrRow, FErrCol)->Style->Color = FNormalColor;
      FDocTabData[FEdTab]->GetTabEDCells(FErrRow, FErrCol)->StyleDisabled->Color = FNormalDisColor;
      FErrRow = -1;
      FErrCol = -1;
     }

    FCurGridModified = true;
    actReCalc->Enabled   = true;
    actSave->Enabled     = true;
    ReCalcTimer->Enabled = false;
    ReCalcTimer->Enabled = true;
    Application->ProcessMessages();
   }
  //UnicodeString AText = DocDataToDisplayText(AGrid->Cells[ACol - AGrid->FixedCols][ARow-AGrid->FixedRows]);
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FocusFirstTabEd(int ATab)
 {
  FEdTab = ATab;
  bool FCont = true;
  for (int y = 0; (y < FDocTabData[FEdTab]->RowCount) && FCont; y++)
   {
    for (int x = 0; (x < FDocTabData[FEdTab]->ColCount) && FCont; x++)
     {
      FCont = !FDocTabData[FEdTab]->GetTabEDCells(y, x)->Enabled;
      if (!FCont)
       {
        FEdY = y;
        FEdX = x;
        FDocTabData[FEdTab]->GetTabEDCells(y, x)->Visible = true;
        FDocTabData[FEdTab]->GetTabEDCells(y, x)->SetFocus();
       }
     }
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TdsDocHandEditForm::GetTRC(UnicodeString AName, int & ATab, int & ATabRow, int & ATabCol)
 {
  bool RC = false;
  try
   {
    ATab    = _GUI(AName).ToIntDef(0);
    ATabRow = GetPart1(GetPart1(_UID(AName), '#'), ':').ToIntDef(-1);
    ATabCol = GetPart2(GetPart1(_UID(AName), '#'), ':').ToIntDef(-1);
    RC      = (GetPart1(GetPart2(AName, '#'), '%').LowerCase() == "in");
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FLoadCodes()
 {
  FErrRow = -1;
  FErrCol = -1;
  TdsDocHTMLCreator * FViewer = new TdsDocHTMLCreator;
  try
   {
    FDocView->LoadFromString(FViewer->Preview(FXMLContainer, FSpec, FEditDoc));
   }
  __finally
   {
    delete FViewer;
   }
  TTagNode * itTab = FSpec->GetChildByName("doccontent");
  TTagNode * itTabDoc;
  if (itTab)
   {
    itTab = itTab->GetFirstChild();
    int FCurTabNum = 0;
    while (itTab)
     {
      if (itTab->CmpName("table"))
       {
        FTabPresent = true;
        itTabDoc    = FEditDoc->GetChildByAV("lv", "uidmain", itTab->AV["uid"], true);
        if (itTabDoc)
         {
          FDocTabData[FCurTabNum] = new TDocTableData(itTab, itTabDoc);
          FCurTabNum++ ;
         }
        else
         throw Exception("������������ ��������� ���������.");
       }
      itTab = itTab->GetNext();
     }
    TList * FValues = FDocView->FormControlList;
    UnicodeString FCtrlName;
    bool FEDEnabled;
    int FCTab, FCTabRow, FCTabCol;
    TcxMaskEdit * FED;
    TFormControlObj * FFO;
    for (int i = 0; i < FValues->Count; i++)
     {
      FFO = FCObj(FValues->Items[i]);
      if (FFO->TheControl->ClassNameIs("TcxMaskEdit"))
       {
        FED = ((TcxMaskEdit *)FFO->TheControl);
        //name = Ntab.Nrow:NCol#(in|dis)%uid
        FCtrlName                 = FCObj(FValues->Items[i])->Name;
        FEDEnabled                = GetTRC(FCtrlName, FCTab, FCTabRow, FCTabCol);
        FEDNameMap[FED]           = FCtrlName;
        FED->Visible              = false;
        FED->Enabled              = FEDEnabled;
        FED->Properties->MaskKind = emkRegExpr;
        FED->Properties->EditMask = "[-]?\\d?\\d?\\d?\\d?\\d?\\d?\\d?\\d?\\d?\\d?([\\.,]?\\d{1,4})";

        FED->Properties->Alignment->Horz           = taRightJustify;
        FED->Properties->Alignment->Vert           = taVCenter;
        FED->Properties->UseLeftAlignmentOnEditing = false;

        FED->StyleDisabled->TextColor = clWindowText;
        FED->StyleDisabled->Color     = FNormalDisColor;

        if (FEDEnabled)
         FED->OnExit = FEDExit;
        if (FEDEnabled)
         FED->OnEnter = FEDEnter;
        if (FEDEnabled)
         FED->Properties->OnChange = FEDChange;
        FED->OnKeyDown = FormKeyDown;

        FDocTabData[FCTab]->SetTabEDCells(FCTabRow, FCTabCol, FED);
        FDocTabData[FCTab]->SetTabFOCells(FCTabRow, FCTabCol, FFO);
       }
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::MMM(TObject * Sender,
    TShiftState Shift, int X, int Y)
 {
  _MSG_DBG("aa");
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::ReCalcTimerTimer(TObject * Sender)
 {
  ReCalcCurrentGridData();
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FormClose(TObject * Sender,
    TCloseAction & Action)
 {
  if (FCurGridModified)
   {
    int RC = MessageBox(Handle, L"��������� ��������� ���������?", L"���������", MB_YESNOCANCEL | MB_ICONQUESTION | MB_DEFBUTTON1);
    if (RC == ID_YES)
     {
      ApplyUpdates();
      Action = caFree;
     }
    else if (RC == ID_CANCEL)
     Action = caNone;
    else
     Action = caFree;
   }
  else
   Action = caFree;
 }
//---------------------------------------------------------------------------
void __fastcall TdsDocHandEditForm::FormKeyDown(TObject * Sender, WORD & Key,
    TShiftState Shift)
 {
  bool FChLR = false;
  bool FChTB = false;

  if (Key == VK_DOWN)
   {
    FChTB = true;
    IncY();
   }
  else if (Key == VK_UP)
   {
    FChTB = true;
    DecY();
   }
  else if (Key == VK_RIGHT)
   {
    TcxMaskEdit * ED = FDocTabData[FEdTab]->GetTabEDCells(FEdY, FEdX);
    if (ED->SelStart >= ED->Text.Trim().Length())
     {
      FChLR = true;
      IncX();
     }
   }
  else if (Key == VK_LEFT)
   {
    TcxMaskEdit * ED = FDocTabData[FEdTab]->GetTabEDCells(FEdY, FEdX);
    if (ED->SelStart <= 0)
     {
      FChLR = true;
      DecX();
     }
   }
  if (FChLR || FChTB)
   {
    TFormControlObj * FO = FDocTabData[FEdTab]->GetTabFOCells(FEdY, FEdX);
    TcxMaskEdit * ED = FDocTabData[FEdTab]->GetTabEDCells(FEdY, FEdX);
    if (ED)
     {
      if (FChLR)
       {
        int FXL = ED->Left + ED->Width;
        int FXR = FDocView->Width - FDocView->Left - 10;
        if (ED->Left <= 5)
         {
          FDocView->HScrollBar->Position += ED->Left - 10;
          FDocView->Repaint();
         }
        else if (FXL > FXR)
         {
          FDocView->HScrollBar->Position += FXL - FXR + 10;
          FDocView->Repaint();
         }
        Application->ProcessMessages();
       }

      if (FChTB)
       {
        int FYT = ED->Top + ED->Height;
        int FYB = FDocView->Height - FDocView->Top;

        if ((FO->YValue - FDocView->VScrollBar->Position) <= 5)
         {
          FDocView->VScrollBar->Position = FO->YValue;
          FDocView->Reformat();
         }
        else if (FYT > FYB)
         {
          FDocView->VScrollBar->Position = FO->YValue;
          FDocView->Reformat();
         }
       }
      Application->ProcessMessages();
      if (!ED->Visible)
       ED->Visible = true;
      Application->ProcessMessages();
      ED->SetFocus();
     }
   }
 }
//---------------------------------------------------------------------------
