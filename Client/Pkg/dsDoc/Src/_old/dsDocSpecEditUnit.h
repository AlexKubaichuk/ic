//---------------------------------------------------------------------------
#ifndef dsDocSpecEditUnitH
#define dsDocSpecEditUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "ICSDocBaseEdit.h"
#include "ICSDocSpec.h"
//---------------------------------------------------------------------------
#include "dsDocDMUnit.h"
#include "cxGraphics.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
//---------------------------------------------------------------------------
/*
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "ICSDocBaseEdit.h"
#include "ICSDocSpec.h"
#include <ExtCtrls.hpp>
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
*/
//---------------------------------------------------------------------------
class PACKAGE TdsDocSpecEditForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *Panel1;
        TcxButton *SaveBtn;
        TcxButton *CancelBtn;
        void __fastcall CancelBtnClick(TObject *Sender);
        void __fastcall SaveBtnClick(TObject *Sender);
        void __fastcall SpecEditorGetOrgsList(TObject *Sender,
          TUnicodeStringMap &SpecsList);
        void __fastcall SpecEditorGetSpecificatorsList(TObject *Sender,
          TUnicodeStringMap &SpecsList);
        bool __fastcall SpecEditorGetSpecNode(TObject *Sender,
          const UnicodeString SpecGUI, TTagNode *&SpecTagNode);
        void __fastcall SpecEditorGetSpecOwner(TObject *Sender,
          UnicodeString AAutorCode, UnicodeString &AAutorName);
        bool __fastcall SpecEditorLoadFilter(UnicodeString ACondName,
          TTagNode *Flt);
 void __fastcall FormDestroy(TObject *Sender);
private:	// User declarations
        TTagNode *FSpec;
        bool IsEdit;
        TAxeXMLContainer *XMList;
        TdsDocDM *FDM;
        TICSDocSpec *SpecEditor;
public:		// User declarations
        __fastcall TdsDocSpecEditForm(TComponent* Owner, TTagNode *ASpec, TAxeXMLContainer *AXMList, UnicodeString AAutorCode, UnicodeString APeriodDef, bool AIsEdit,TdsDocDM *ADM, bool ADoc);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocSpecEditForm *dsDocSpecEditForm;
//---------------------------------------------------------------------------
#endif
