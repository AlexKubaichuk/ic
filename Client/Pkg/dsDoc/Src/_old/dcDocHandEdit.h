/*
  ����        - DocHandEdit.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� ��������
                ����� ��� ������� ����� ������ � ��������
  ����������� - ������� �.�.
  ����        - 22.11.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  23.04.2007
    [+] ��������� ������������ ����� �� 3-�� ����� ����� �������

  16.03.2006
    [+] ������ ������������ "�������"
    [*] ��� ���������� ������� ������ Exception � DKException �����������
        EInvalidArgument, ENullArgument, EMethodError
    [*] �� ��������� ������ ������������ "�������" � ������ "��������� ���������"
        ��������
    [*] �����������

  22.06.2005
    1. ��������� ��������� ������� ����� ������� � �������� �������� ������� �����

  20.04.2005
    1. ������������ ����� � grid'� ������������ � ������������ DecimalSeparator
    2. ������� �� ������ ���� "������ ������������" ��������� �������� "������� ����"
    3. ��������� ���� � ����������� �������� ������ ��� ������������ ���������

  09.02.2005
    1. �������� viewer ��������� ���������, ������� ���� �
       �������� ��� ��������� �� ���������

  30.11.2004
    1. ��� ������ ��������� �� ������, �����
       ��������� ������ �� ������������� �������� ��������
       ������� �����, ������ ��������� � ��� ������� ��������
    2. �������� �������� ����������� ������ ����� ��������� ������� �����

  25.11.2004
    1. ��������� ������ ��������� �� ������, �����
       ��������� ������ �� ������������� �������� ��������
       ������� �����

  24.11.2004
    1. �� �������� ���������� ������� ������� ��� �����

  23.11.2004
    1. ��������� ������ ��������� ��� ����� �����, ����������������
       �������, �������� � ������� ������ �������������

  22.11.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef dcDocHandEditH
#define dcDocHandEditH
//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxGroupBox.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "Htmlview.hpp"
//#include "ICSDocSpec.h"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "ICSDocSpec.h"
#include "cxClasses.hpp"
/*
#include "dxSkinsCore.hpp"
#include "Htmlview.hpp"

//---------------------------------------------------------------------------
#include "XMLContainer.h"
//#include <list>
#include "ICSDocSpec.h"
#include "cxClasses.hpp"
*/
//#include "ICSDoc.h"	//��������� �������� � ���� ��������

namespace dsDocHandEdit
{

struct TTCellTagNodes;
template <class DHMData> class TDHMatrix;
//typedef list<TTagNode*> TTagNodeList;

//---------------------------------------------------------------------------

class PACKAGE TDocTableData : public TObject
{
private:
    TTagNode *FTabDef;
    TTagNode *FTabDoc;
    TDHMatrix<TTCellTagNodes> *FTCells;  //������� ������ �� ������-���������
    TDHMatrix<TTCellTagNodes> __fastcall FGetTabCells();  //������� ������ �� ������-���������
    TTagNodeList FSingleRows;
    TTagNodeList FSingleCols;
    int FRowMax, FColMax;
public:
   __fastcall TDocTableData(TTagNode *ATabDef, TTagNode *ATabDoc);
   __fastcall ~TDocTableData();

    TcxMaskEdit* __fastcall GetTabEDCells(int ARow, int ACol);         //������� ������ �� ������-���������
    void   __fastcall SetTabEDCells(int ARow, int ACol, TcxMaskEdit *AED);

    TFormControlObj* __fastcall GetTabFOCells(int ARow, int ACol);
    void   __fastcall SetTabFOCells(int ARow, int ACol, TFormControlObj *AFO);

    TTagNode* __fastcall GetTabColNode(int ARow, int ACol);         //������� ������ �� ������-���������
    TTagNode* __fastcall GetTabRowNode(int ARow, int ACol);         //������� ������ �� ������-���������

   __property TDHMatrix<TTCellTagNodes> TabCells = {read = FGetTabCells};
   __property int RowCount = {read = FRowMax};
   __property int ColCount = {read = FColMax};
   __property TTagNode *TabDef = {read = FTabDef};
   __property TTagNode *TabDoc = {read = FTabDoc};
};
//---------------------------------------------------------------------------
typedef map<int, TDocTableData*> TDocTableDataMap;
typedef map<TcxMaskEdit*, UnicodeString> TEDAnsiMap;

//---------------------------------------------------------------------------

//����� ��� ������� ����� ������ � ��������

class PACKAGE TdsDocHandEditForm : public TForm
{
    typedef TForm inherited;

__published:	// IDE-managed Components
    TActionList *ActionList;
    TAction *actSave;
    TAction *actExit;
    TAction *actReCalc;
        TImageList *MainIL;
        TcxImageList *LMainIL;
        TTimer *ReCalcTimer;
        TPanel *BtnPanel;
        TcxButton *ApplyBtn;
        TPanel *Panel2;
        TcxGroupBox *ErrPanel;
        TPanel *Panel1;
        TLabel *ErrMsgLab;
        TPanel *Panel3;
        TLabel *ErrRulesLab;
        TLabel *Label1;
 THTMLViewer *FDocView;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall actSaveExecute(TObject *Sender);
    void __fastcall actExitExecute(TObject *Sender);
    void __fastcall actReCalcExecute(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall ReCalcTimerTimer(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);

private:	// User declarations
    /*����*/
    enum TTRulesType {                              //��� ������ ������� ��� �������� ��� �������
      rtRow,                                        //handrules ��� calcrules ��� ������ �������
      rtCol                                         //handrules ��� calcrules ��� ������� �������
    };
    int FEdX,FEdY,FEdXMax,FEdYMax, FEdTab;

    TColor                 FErrCellColor, FErrRowColColor, FNormalColor, FNormalDisColor;
    /*����*/
    bool FErrInCol;
    int  FErrRow, FErrCol;
    TTagNode*              FSpec;           //������������ ( xml-������ � ������ "specificator" )
    TTagNode*              FDoc;            //�������� ( xml-������ � ������ "docum" )
    TTagNode*              FEditDoc;        //�������� ����� ���������
    bool                   FTabPresent;
    bool                   FModified;       //������� ����������� ���������
    bool                   FCurGridModified;//������� ����������� grid'� ��� �������� �������� ���������
                                            //��� �������� �������� ��������� � ������ �����������
                                            //�����
                                            //������ � ������� ��� ������ �������� �������� ��������� "�������"

    TDocTableDataMap       FDocTabData;
    TEDAnsiMap             FEDNameMap;
    bool   FRecalcMode;

    void __fastcall FEDExit(TObject *Sender);
    void __fastcall FEDEnter(TObject *Sender);
    void __fastcall FEDChange(TObject *Sender);

    void __fastcall IncY();
    void __fastcall DecY();
    void __fastcall IncX();
    void __fastcall DecX();

    void       __fastcall FocusFirstTabEd(int ATab);
    bool       __fastcall GetTRC(UnicodeString AName, int &ATab, int &ATabRow, int &ATabCol);
    void       __fastcall FLoadCodes();
    void __fastcall TdsDocHandEditForm::MMM(TObject *Sender, TShiftState Shift, int X, int Y);

protected:

    TAxeXMLContainer *FXMLContainer;

    TTagNode* __fastcall GetDocElementDesc(TTagNode *ADocElement);
    UnicodeString __fastcall ReplaceTRowColNum(UnicodeString APoland, TTRulesType AType, int nRow, int nCol);
    bool __fastcall THandRulesCheck(TTagNode *ndHandRules,TTRulesType AType,int nRow,int nCol);
    void __fastcall ReCalcTCell(TTagNode *ndCalcRules, TTRulesType AType, int nRow, int nCol);
    void __fastcall ReCalcCurrentGridData();
    UnicodeString __fastcall DocDataToStr(const double AData);
    UnicodeString __fastcall DocDataToDisplayText(const double AData);
    double __fastcall DisplayTextToDocData(UnicodeString AVal);
    bool __fastcall ApplyCurrentGridUpdates();
    void __fastcall AssignIVValAV(TTagNode *ADestNode, TTagNode *ASrcNode);
    void __fastcall ApplyUpdates();


public:		// User declarations
    __fastcall TdsDocHandEditForm(TComponent* Owner, TTagNode* ASpec, TTagNode* ADoc, TAxeXMLContainer *AXMLContainer);
    virtual __fastcall ~TdsDocHandEditForm();

    bool __fastcall CheckByHandRules();

    /*��������*/
    //������� ����������� ��������� � ������� ��������������
    __property bool       Modified = { read = FModified };
};

//---------------------------------------------------------------------------

} //end of namespace DocHandEdit
using namespace dsDocHandEdit;

#endif
