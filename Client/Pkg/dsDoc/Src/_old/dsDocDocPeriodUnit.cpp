//---------------------------------------------------------------------------

#include <vcl.h>
#include <DateUtils.hpp>
#pragma hdrstop

#include "dsDocDocPeriodUnit.h"
//#include "reportdef.h"
#include "msgdef.h"
#include "dsDocClientConstDef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//#ifndef _CONFSETTTING
// #error _CONFSETTTING using are not defined
//#endif

#pragma link "cxButtons"
#pragma link "cxCalendar"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDateUtils"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxRadioGroup"
#pragma link "cxSpinEdit"
#pragma link "cxTextEdit"
#pragma link "dxCore"
#pragma resource "*.dfm"
TdsDocPeriodForm *dsDocPeriodForm;
//---------------------------------------------------------------------------
__fastcall TdsDocPeriodForm::TdsDocPeriodForm(TComponent* Owner, TColor AReqColor)
        : TForm(Owner)
{
  FReqColor = AReqColor;
//  Caption = FMT(dsDocDocPeriodCaption);
  Caption = FMT(dsDocDocPeriodCaptionLabCaption);
  DateFrLab->Caption = FMT(dsDocDocPeriodDateFrLabCaption);
  DateToLab->Caption = FMT(dsDocDocPeriodDateToLabCaption);
  YearRB->Caption = FMT(dsDocDocPeriodYearRBCaption);
  QuarterRB->Caption = FMT(dsDocDocPeriodQuarterRBCaption);
  MonsRB->Caption = FMT(dsDocDocPeriodMonsRBCaption);
  PeriodRB->Caption = FMT(dsDocDocPeriodPeriodRBCaption);
  HalfYearRB->Caption = FMT(dsDocDocPeriodHalfYearRBCaption);
  OkBtn->Caption = FMT(dsDocDocPeriodOkBtnCaption);
  CancelBtn->Caption = FMT(dsDocDocPeriodCancelBtnCaption);

  HalfYearCB->Properties->Items->Clear();
  HalfYearCB->Properties->Items->Add(FMT(dsDocDocPeriodHalfYearCBItems1));
  HalfYearCB->Properties->Items->Add(FMT(dsDocDocPeriodHalfYearCBItems2));

  QuarterCB->Properties->Items->Clear();
  QuarterCB->Properties->Items->Add(FMT(dsDocDocPeriodQuarterCBItems1));
  QuarterCB->Properties->Items->Add(FMT(dsDocDocPeriodQuarterCBItems2));
  QuarterCB->Properties->Items->Add(FMT(dsDocDocPeriodQuarterCBItems3));
  QuarterCB->Properties->Items->Add(FMT(dsDocDocPeriodQuarterCBItems4));

  MonsCB->Properties->Items->Clear();
  MonsCB->Properties->Items->Add(FMT(dsDocDocPeriodMonsCBItems01));
  MonsCB->Properties->Items->Add(FMT(dsDocDocPeriodMonsCBItems02));
  MonsCB->Properties->Items->Add(FMT(dsDocDocPeriodMonsCBItems03));
  MonsCB->Properties->Items->Add(FMT(dsDocDocPeriodMonsCBItems04));
  MonsCB->Properties->Items->Add(FMT(dsDocDocPeriodMonsCBItems05));
  MonsCB->Properties->Items->Add(FMT(dsDocDocPeriodMonsCBItems06));
  MonsCB->Properties->Items->Add(FMT(dsDocDocPeriodMonsCBItems07));
  MonsCB->Properties->Items->Add(FMT(dsDocDocPeriodMonsCBItems08));
  MonsCB->Properties->Items->Add(FMT(dsDocDocPeriodMonsCBItems09));
  MonsCB->Properties->Items->Add(FMT(dsDocDocPeriodMonsCBItems10));
  MonsCB->Properties->Items->Add(FMT(dsDocDocPeriodMonsCBItems11));
  MonsCB->Properties->Items->Add(FMT(dsDocDocPeriodMonsCBItems12));

  YearRBClick(NULL);
  DateFrDE->Style->Color = FReqColor;
  DateToDE->Style->Color = FReqColor;
  DateFr = TDate(0);
  DateTo = TDate(0);
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPeriodForm::YearRBClick(TObject *Sender)
{
  YearED->Enabled = true;
  YearED->Style->Color = FReqColor;

  DateFrLab->Enabled = false;
  DateFrDE->Enabled = false;
  DateToLab->Enabled = false;
  DateToDE->Enabled = false;

  DateFrDE->Text = "";
  DateToDE->Text = "";

  TDateTime cDate = TDateTime::CurrentDate();
  unsigned short YY,MM,DD;
  cDate.DecodeDate(&YY, &MM, &DD);
  if (YearRB->Checked)
   {
     HalfYearCB->Enabled  = false;
     QuarterCB->Enabled   = false;
     MonsCB->Enabled      = false;
     MonsCB->Visible      = true;
     MonsCB->Style->Color = clBtnFace;

     HalfYearCB->ItemIndex = -1;
     QuarterCB->ItemIndex = -1;
     MonsCB->ItemIndex = -1;

     YearED->Value = YY;
   }
  else if (HalfYearRB->Checked)
   {
     QuarterCB->Visible  = false;
     MonsCB->Visible     = false;

     HalfYearCB->Visible       = true;
     HalfYearCB->Enabled       = true;
     HalfYearCB->Style->Color  = FReqColor;

     HalfYearCB->ItemIndex = (int)((MM-1)/6);
   }
  else if (QuarterRB->Checked)
   {
     HalfYearCB->Visible = false;
     MonsCB->Visible     = false;

     QuarterCB->Visible       = true;
     QuarterCB->Enabled       = true;
     QuarterCB->Style->Color  = FReqColor;
     QuarterCB->ItemIndex = (int)((MM-1)/3);
   }
  else if (MonsRB->Checked)
   {
     HalfYearCB->Visible = false;
     QuarterCB->Visible  = false;

     MonsCB->Visible       = true;
     MonsCB->Enabled       = true;
     MonsCB->Style->Color  = FReqColor;
     MonsCB->ItemIndex = MM - 1;
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPeriodForm::PeriodRBClick(TObject *Sender)
{
   YearED->Enabled = false;
   YearED->Style->Color = clBtnFace;

   HalfYearCB->Visible = false;
   QuarterCB->Visible  = false;
   MonsCB->Visible      = true;
   MonsCB->ItemIndex = -1;
   MonsCB->Enabled      = false;
   MonsCB->Style->Color = clBtnFace;

   DateFrLab->Enabled = true;
   DateFrDE->Enabled = true;
   DateToLab->Enabled = true;
   DateToDE->Enabled = true;
   DateToDE->Date = TDateTime::CurrentDate();
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPeriodForm::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if ((Key == VK_RETURN)&&OkBtn->Enabled) OkBtnClick(OkBtn);
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPeriodForm::OkBtnClick(TObject *Sender)
{
   pdType = 0;
   if (YearRB->Checked)
    {
      try
       {
         DateFr = StrToDate(("01.01."+YearED->Text).c_str());
         DateTo = StrToDate(("31.12."+YearED->Text).c_str());
         pdType = 5;
       }
      catch (...)
       {
          _MSG_ERR(FMT(dsDocDocPeriodErrorYearFormat),FMT(dsDocCommonErrorCaption));
          return;
       }
    }
   else if (HalfYearRB->Checked)
    {
      try
       {
         if (HalfYearCB->ItemIndex < 0) return;
         if (HalfYearCB->ItemIndex == 0)
          {
            DateFr = StrToDate(("01.01."+YearED->Text).c_str());
            DateTo = StrToDate(("30.06."+YearED->Text).c_str());
          }
         else
          {
            DateFr = StrToDate(("01.07."+YearED->Text).c_str());
            DateTo = StrToDate(("31.12."+YearED->Text).c_str());
          }
         pdType = 4;
       }
      catch (...)
       {
          _MSG_ERR(FMT(dsDocDocPeriodErrorDateFormat),FMT(dsDocCommonErrorCaption));
          return;
       }
    }
   else if (QuarterRB->Checked)
    {
      try
       {
         if (QuarterCB->ItemIndex == 0)
          {
            DateFr = StrToDate(("01.01."+YearED->Text).c_str());
            DateTo = StrToDate(("31.03."+YearED->Text).c_str());
          }
         else if (QuarterCB->ItemIndex == 1)
          {
            DateFr = StrToDate(("01.04."+YearED->Text).c_str());
            DateTo = StrToDate(("30.06."+YearED->Text).c_str());
          }
         else if (QuarterCB->ItemIndex == 2)
          {
            DateFr = StrToDate(("01.07."+YearED->Text).c_str());
            DateTo = StrToDate(("30.09."+YearED->Text).c_str());
          }
         else if (QuarterCB->ItemIndex == 3)
          {
            DateFr = StrToDate(("01.10."+YearED->Text).c_str());
            DateTo = StrToDate(("31.12."+YearED->Text).c_str());
          }
         else return;
         pdType = 3;
       }
      catch (...)
       {
          _MSG_ERR(FMT(dsDocDocPeriodErrorDateFormat),FMT(dsDocCommonErrorCaption));
          return;
       }
    }
   else if (MonsRB->Checked)
    {
      try
       {
         if (MonsCB->ItemIndex < 0) return;
         if (MonsCB->ItemIndex < 9)
          {
            DateFr = StrToDate(("01.0"+IntToStr(MonsCB->ItemIndex+1)+"."+YearED->Text).c_str());
            DateTo = StrToDate((GetLastDay(MonsCB->ItemIndex+1,YearED->Text.ToInt())+".0"+IntToStr(MonsCB->ItemIndex+1)+"."+YearED->Text).c_str());
          }
         else
          {
            DateFr = StrToDate(("01."+IntToStr(MonsCB->ItemIndex+1)+"."+YearED->Text).c_str());
            DateTo = StrToDate((GetLastDay(MonsCB->ItemIndex+1,YearED->Text.ToInt())+"."+IntToStr(MonsCB->ItemIndex+1)+"."+YearED->Text).c_str());
          }
         pdType = 2;
       }
      catch (...)
       {
          _MSG_ERR(FMT(dsDocDocPeriodErrorDateFormat),FMT(dsDocCommonErrorCaption));
          return;
       }
    }
   else if (PeriodRB->Checked)
    {
      try
       {
         pdType = 1;
         if (DateFrDE->Text.Length()) DateFr = DateFrDE->Date;
         if (DateToDE->Text.Length()) DateTo = DateToDE->Date;
       }
      catch (...)
       {
          _MSG_ERR(FMT(dsDocDocPeriodErrorYearFormat),FMT(dsDocCommonErrorCaption));
          return;
       }
    }
   ModalResult = mrOk;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocPeriodForm::GetLastDay(int AMons, int AYear)
{
  if (IsValidDate((Word)AYear, (Word)AMons, (Word)1))
   {// ��� � ����� ����������
     return IntToStr(DaysInAMonth((Word)AYear, (Word)AMons));
   }
  else
   return "1";
}
//---------------------------------------------------------------------------


