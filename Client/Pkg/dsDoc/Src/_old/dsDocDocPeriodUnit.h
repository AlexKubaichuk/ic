//---------------------------------------------------------------------------
#ifndef dsDocDocPeriodUnitH
#define dsDocDocPeriodUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxCalendar.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDateUtils.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxRadioGroup.hpp"
#include "cxSpinEdit.hpp"
#include "cxTextEdit.hpp"
#include "dxCore.hpp"
//---------------------------------------------------------------------------
class PACKAGE TdsDocPeriodForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *DateFrLab;
        TLabel *DateToLab;
        TcxSpinEdit *YearED;
        TcxRadioButton *YearRB;
        TcxRadioButton *QuarterRB;
        TcxRadioButton *MonsRB;
        TcxRadioButton *PeriodRB;
        TcxRadioButton *HalfYearRB;
        TcxComboBox *HalfYearCB;
        TcxComboBox *QuarterCB;
        TcxComboBox *MonsCB;
        TcxDateEdit *DateFrDE;
        TcxDateEdit *DateToDE;
 TcxButton *OkBtn;
 TcxButton *CancelBtn;
 TcxComboBox *cxComboBox1;
        void __fastcall YearRBClick(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall PeriodRBClick(TObject *Sender);
private:	// User declarations
        TColor   FReqColor;
        UnicodeString __fastcall GetLastDay(int AMons, int AYear);
//        TdsDocDM *FDM;
public:		// User declarations
        TDateTime DateFr,DateTo;
        int       pdType;  // "year" 5, "halfyear 4; "quarter 3;  "month" 2;  "unique" 1;  ERR 0
        __fastcall TdsDocPeriodForm(TComponent* Owner, TColor AReqColor);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocPeriodForm *dsDocPeriodForm;
//---------------------------------------------------------------------------
#endif
