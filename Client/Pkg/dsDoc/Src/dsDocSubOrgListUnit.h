//---------------------------------------------------------------------------
#ifndef dsDocSubOrgListUnitH
#define dsDocSubOrgListUnitH
//---------------------------------------------------------------------------

#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxClasses.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxData.hpp"
#include "cxDataStorage.hpp"
#include "cxEdit.hpp"
#include "cxFilter.hpp"
#include "cxGraphics.hpp"
#include "cxGrid.hpp"
#include "cxGridCustomTableView.hpp"
#include "cxGridCustomView.hpp"
#include "cxGridLevel.hpp"
#include "cxGridTableView.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxNavigator.hpp"
#include "cxStyles.hpp"
#include "dxBar.hpp"
#include "dxStatusBar.hpp"
//---------------------------------------------------------------------------
#include "dsDocDMUnit.h"
//---------------------------------------------------------------------------
class PACKAGE TdsDocSubOrgForm : public TForm
{
__published:	// IDE-managed Components
        TdxStatusBar *StatusSB;
        TdxBarManager *MainTBM;
        TActionList *MainAL;
        TAction *actInsert;
        TAction *actDelete;
        TAction *actRefresh;
        TAction *actFind;
        TPopupMenu *PMenu;
        TMenuItem *InsertItem;
        TMenuItem *ModifyItem;
        TMenuItem *DeleteItem;
        TImageList *RegIL;
        TAction *actClose;
        TcxImageList *LMainIL;
        TdxBarLargeButton *dxBarLargeButton1;
        TdxBarLargeButton *dxBarLargeButton3;
        TdxBarLargeButton *dxBarLargeButton4;
 TcxStyleRepository *Style;
        TcxStyle *cxStyle1;
        TcxStyle *cxStyle2;
 TcxGrid *SubordOrgList;
 TcxGridTableView *SubordOrgListView;
 TcxGridColumn *ldvNameCol;
 TcxGridColumn *ldvPhoneCol;
 TcxGridColumn *ldvEMailCol;
 TcxGridColumn *ldvAddrCol;
 TcxGridLevel *SubordOrgListLevel1;
 TcxGridColumn *ldvCodeCol;
 TcxGridColumn *ldvFullNameCol;
 TcxGridColumn *ldvOwnerNameCol;
 TTimer *ShowTimer;
 TdxStatusBarContainerControl *StatusSBContainer3;
 TdxStatusBarContainerControl *StatusSBContainer4;
 TProgressBar *PrBar;
 TLabel *MsgLab;
 TcxStyle *cxStyle3;
        void __fastcall actInsertExecute(TObject *Sender);
        void __fastcall actDeleteExecute(TObject *Sender);
        void __fastcall actCloseExecute(TObject *Sender);
 void __fastcall ShowTimerTimer(TObject *Sender);
 void __fastcall FormShow(TObject *Sender);
 void __fastcall FormDestroy(TObject *Sender);
 void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
 void __fastcall SubordOrgListViewFocusedRecordChanged(TcxCustomGridTableView *Sender,
          TcxCustomGridRecord *APrevFocusedRecord, TcxCustomGridRecord *AFocusedRecord,
          bool ANewItemRecordFocusingChanged);
private:	// User declarations
  TkabCustomDataSource *FDataSrc;
  TTagNode     *RootClass;
  TdsRegEDContainer *FCtrList;
  __int64    xpCount,xcTop;
//        void __fastcall ChangeSrc();
        bool FUseLPUClass;
        UnicodeString curCD;
        TdsDocDM *FDM;

  void __fastcall ShowEditDlg();
  void __fastcall FGetDataProgress(int AMax, int ACur, UnicodeString AMsg);
  void __fastcall RefreshById(UnicodeString ACode);
  void __fastcall CreateSrc();
  void __fastcall ChangeSrc();
  void __fastcall CheckCtrlState(bool AEnable);
  void __fastcall CheckCtrlVisible();
  bool __fastcall CreateListExt(TTagNode *itTag, UnicodeString &UID);
  TkabCustomDataSetRow* __fastcall CurRow();
  TkabCustomDataSetRow* __fastcall EditedRow();
  bool __fastcall IsRecSelected();

public:		// User declarations
        __fastcall TdsDocSubOrgForm(TComponent* Owner, TdsDocDM *ADM, bool AUseLPUClass = true);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocSubOrgForm *dsDocSubOrgForm;
//---------------------------------------------------------------------------
#endif
