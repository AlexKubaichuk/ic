//---------------------------------------------------------------------------
#ifndef dsDocExportDocUnitH
#define dsDocExportDocUnitH
//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include "cxCheckBox.hpp"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxPC.hpp"
#include "cxProgressBar.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxStatusBar.hpp"
//---------------------------------------------------------------------------
#include "dsDocDMUnit.h"
//---------------------------------------------------------------------------
typedef bool __stdcall(*TOpenConvertProc)(char * AOrgCode);
typedef int __stdcall(*TCloseConvertProc)(char * AFileName);
typedef bool __stdcall(*TConvertDocProc)(char * ADoc, char * ASpec);
/*
class TForm28 : public TForm
{
__published:	// IDE-managed Components
 TButton *Button1;
 void __fastcall FormDestroy(TObject *Sender);
 void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
*/
class PACKAGE TdsDocExportDocForm : public TForm
{
        typedef map<int, UnicodeString> TIntMap;
        typedef map<int, TDocEl*> TDocElMap;
__published:	// IDE-managed Components
        TSaveDialog *SaveDlg;
        TcxPageControl *ExportPC;
        TcxTabSheet *ExpSpecTS;
        TcxTabSheet *ExpDocTS;
        TdxStatusBar *StatusBar;
        TdxStatusBarContainerControl *dxStatusBar1Container0;
        TcxProgressBar *PrBar;
 TcxStyleRepository *StyleRepos;
        TcxStyle *DisabledStyle;
        TcxStyle *EnabledStyle;
        TcxStyle *BoldStyle;
        TdxStatusBarContainerControl *StatusBarContainer4;
        TImage *FilterOnImg;
        TImage *FilterOffImg;
        TdxBarManager *ListTBM;
        TdxBarLargeButton *dxBarLargeButton1;
        TdxBarLargeButton *dxBarLargeButton10;
        TcxImageList *LMainIL;
        TActionList *MainAL;
        TAction *actSave;
        TAction *actCheckByPack;
        TdxBarCombo *PackCB;
        TAction *actClose;
 TTimer *ShowTimer;
 TcxTreeList *SpecChList;
 TcxTreeListColumn *SpecCh;
 TcxTreeListColumn *SpecName;
 TcxTreeListColumn *SpecOwner;
 TcxTreeListColumn *SpecPerid;
 TcxStyle *selStyle;
 TcxTreeList *DocChList;
 TcxTreeListColumn *DocCh;
 TcxTreeListColumn *DocName;
 TcxTreeListColumn *DocOwner;
 TcxTreeListColumn *DocPeriod;
 TcxStyle *cxStyle1;
 TcxImageList *MainIL;
 TAction *actClear;
 TdxBarLargeButton *dxBarLargeButton2;
 TcxStyle *cxStyle2;
 TAction *actSaveDOS;
 TdxBarLargeButton *dxBarLargeButton3;
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall actSaveExecute(TObject *Sender);
        void __fastcall PackCBChange(TObject *Sender);
        void __fastcall actCheckByPackExecute(TObject *Sender);
        void __fastcall SpecListColChBPropertiesChange(TObject *Sender);
        void __fastcall actCloseExecute(TObject *Sender);
 void __fastcall ShowTimerTimer(TObject *Sender);
 void __fastcall FormShow(TObject *Sender);
 void __fastcall dxBarButton1Click(TObject *Sender);
 void __fastcall SpecChListStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
 void __fastcall SpecChListDblClick(TObject *Sender);
 void __fastcall DocChListDblClick(TObject *Sender);
 void __fastcall SpecChPropertiesEditValueChanged(TObject *Sender);
 void __fastcall DocChPropertiesEditValueChanged(TObject *Sender);
 void __fastcall actSaveDOSExecute(TObject *Sender);
private:	// User declarations
        bool __fastcall CanProgress();
  HINSTANCE ConvDoc;
  TOpenConvertProc  FOpenProc;
  TCloseConvertProc FCloseProc;
  TConvertDocProc   FConvertProc;

        TTagNode *FSpecDefNode, *FDocDefNode;
        TkabCustomDataSource *FSpecDS;
        TkabCustomDataSource *FDocDS;
//        TStringList *FSpecList;
//        TStringList *FDocList;
        UnicodeString xPath, WrkPath;
        TIntMap  FSpecGUIMap;
        void __fastcall FLoadSpec();

        TStringList *FMissingSpecDoc;

        void __fastcall FCheckDocItem(UnicodeString ASpecName, UnicodeString ASpecGUI, int APerType, TDate ADateFr, TDate ADateTo);
        void __fastcall FCheckSpecItem(UnicodeString ASpecName, UnicodeString ASpecGUI, int APerType, TDate ADateFr, TDate ADateTo);
        void __fastcall FGetDataProgress(int AMax, int ACur, UnicodeString AMsg);

        TDocEl* __fastcall SpecEl(int Idx);
        TDocEl* __fastcall DocEl(int Idx);
        void __fastcall ExportXML(TcxTreeListNode *AItems, TTagNode *AExpTag, bool ACanSpec);
        void __fastcall FProgressInit(int AMax, UnicodeString AMsg);
        void __fastcall FProgressChange(int APercent, UnicodeString AMsg);
        void __fastcall FProgressComplite(UnicodeString AMsg);
        bool FInProgress;
        void __fastcall SetInProgress(bool AVal);
        void __fastcall SetEnabled(bool AEnabled);
  void __fastcall ClearCheck();
  void __fastcall ClearDocCheck();
  void __fastcall ClearSpecCheck();
        UnicodeString  FExpTagName;
protected:
        virtual void __fastcall WndProc(Messages::TMessage &Message);
        UnicodeString FPeriodWhere,FOwnerWhere;
        UnicodeString FDocPeriodWhere,FDocDateWhere, FDocOwnerWhere, FDocSpecOwnerWhere;
        TdsDocDM *FDM;
public:		// User declarations
        __fastcall TdsDocExportDocForm(TComponent* Owner, TdsDocDM *ADM, bool ASpec = true, bool ADoc = true);
        __property bool InProgress = {read=FInProgress, write=SetInProgress};
        __property UnicodeString ExpTagName = {read=FExpTagName, write=FExpTagName};

};
//---------------------------------------------------------------------------
#endif

