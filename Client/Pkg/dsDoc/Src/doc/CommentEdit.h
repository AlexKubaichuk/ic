/*
  ����        - CommentEdit.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� ��������
                ����� "�������������� �����������"
                ( ������������ ����������� TICSDocFilter )
  ����������� - ������� �.�.
  ����        - 01.09.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  27.01.2006
    [*] �������� ������������ ��������� ����������

  10.11.2005
    [*] ����� ����� ��� ������ ����� ��������������� �� ���� �����

  01.09.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef CommentEditH
#define CommentEditH

//---------------------------------------------------------------------------

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxTextEdit.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"

#include "ICSDoc.h"  //��������� �������� � ���� ��������
/*
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxTextEdit.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinCaramel.hpp"
#include "dxSkinCoffee.hpp"
#include "dxSkinDarkRoom.hpp"
#include "dxSkinDarkSide.hpp"
#include "dxSkinFoggy.hpp"
#include "dxSkinGlassOceans.hpp"
#include "dxSkiniMaginary.hpp"
#include "dxSkinLilian.hpp"
#include "dxSkinLiquidSky.hpp"
#include "dxSkinLondonLiquidSky.hpp"
#include "dxSkinMcSkin.hpp"
#include "dxSkinMoneyTwins.hpp"
#include "dxSkinOffice2007Blue.hpp"
#include "dxSkinOffice2007Green.hpp"
#include "dxSkinOffice2007Pink.hpp"
#include "dxSkinOffice2007Silver.hpp"
#include "dxSkinOffice2010Blue.hpp"
#include "dxSkinOffice2010Silver.hpp"
#include "dxSkinPumpkin.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinsDefaultPainters.hpp"
#include "dxSkinSeven.hpp"
#include "dxSkinSharp.hpp"
#include "dxSkinSilver.hpp"
#include "dxSkinSpringTime.hpp"
#include "dxSkinStardust.hpp"
#include "dxSkinSummer2008.hpp"
#include "dxSkinValentine.hpp"
#include "dxSkinXmas2008Blue.hpp"*/

namespace CommentEdit
{

//�����, "�������������� �����������"

class PACKAGE TfmCommentEdit : public TForm
{
__published:	// IDE-managed Components
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
        TGroupBox *GroupBox1;
        TLabel *CommentLab;
  TcxTextEdit *edComment;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bOkClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);


private:	// User declarations
        bool __fastcall AreInputsCorrect();
        UnicodeString FComment;                    //������������� �����������


protected:
        void __fastcall SetComment( UnicodeString  AValue );


public:		// User declarations
        __fastcall TfmCommentEdit(TComponent* Owner,UnicodeString  AComment);

        //������������� �����������
        __property UnicodeString Comment = { read = FComment, write = SetComment };
};

//***************************************************************************
//********************** ��������� ���������� ������� ***********************
//***************************************************************************

extern PACKAGE int ShowModalCommentEdit( TComponent* Owner, UnicodeString& AComment );

} //end of namespace CommentEdit
using namespace CommentEdit;

#endif
