﻿// CodeGear C++Builder
// Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'icsDocConst.pas' rev: 28.00 (Windows)

#ifndef IcsdocconstHPP
#define IcsdocconstHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Icsdocconst
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
extern DELPHI_PACKAGE System::ResourceString _icsDocCaption;
#define Icsdocconst_icsDocCaption System::LoadResourceString(&Icsdocconst::_icsDocCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocErrorTagEq;
#define Icsdocconst_icsDocErrorTagEq System::LoadResourceString(&Icsdocconst::_icsDocErrorTagEq)
extern DELPHI_PACKAGE System::ResourceString _icsDocErrorXMLContainerNotDefined;
#define Icsdocconst_icsDocErrorXMLContainerNotDefined System::LoadResourceString(&Icsdocconst::_icsDocErrorXMLContainerNotDefined)
extern DELPHI_PACKAGE System::ResourceString _icsDocErrorDocNotDefined;
#define Icsdocconst_icsDocErrorDocNotDefined System::LoadResourceString(&Icsdocconst::_icsDocErrorDocNotDefined)
extern DELPHI_PACKAGE System::ResourceString _icsDocErrorGUINotValid;
#define Icsdocconst_icsDocErrorGUINotValid System::LoadResourceString(&Icsdocconst::_icsDocErrorGUINotValid)
extern DELPHI_PACKAGE System::ResourceString _icsDocErrorCommonDop;
#define Icsdocconst_icsDocErrorCommonDop System::LoadResourceString(&Icsdocconst::_icsDocErrorCommonDop)
extern DELPHI_PACKAGE System::ResourceString _icsDocErrorCreateOwnerNotValid;
#define Icsdocconst_icsDocErrorCreateOwnerNotValid System::LoadResourceString(&Icsdocconst::_icsDocErrorCreateOwnerNotValid)
extern DELPHI_PACKAGE System::ResourceString _icsDocErrorInputErrorCaption;
#define Icsdocconst_icsDocErrorInputErrorCaption System::LoadResourceString(&Icsdocconst::_icsDocErrorInputErrorCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocErrorReqFieldCaption;
#define Icsdocconst_icsDocErrorReqFieldCaption System::LoadResourceString(&Icsdocconst::_icsDocErrorReqFieldCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocBaseEditCommonTBCaption;
#define Icsdocconst_icsDocBaseEditCommonTBCaption System::LoadResourceString(&Icsdocconst::_icsDocBaseEditCommonTBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocBaseEditEditTBCaption;
#define Icsdocconst_icsDocBaseEditEditTBCaption System::LoadResourceString(&Icsdocconst::_icsDocBaseEditEditTBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocBaseEditErrorSetDomainNum;
#define Icsdocconst_icsDocBaseEditErrorSetDomainNum System::LoadResourceString(&Icsdocconst::_icsDocBaseEditErrorSetDomainNum)
extern DELPHI_PACKAGE System::ResourceString _icsDocBaseEditErrorGetNom;
#define Icsdocconst_icsDocBaseEditErrorGetNom System::LoadResourceString(&Icsdocconst::_icsDocBaseEditErrorGetNom)
extern DELPHI_PACKAGE System::ResourceString _icsDocBaseEditErrorGetDomain;
#define Icsdocconst_icsDocBaseEditErrorGetDomain System::LoadResourceString(&Icsdocconst::_icsDocBaseEditErrorGetDomain)
extern DELPHI_PACKAGE System::ResourceString _icsDocBaseEditErrorNoXML;
#define Icsdocconst_icsDocBaseEditErrorNoXML System::LoadResourceString(&Icsdocconst::_icsDocBaseEditErrorNoXML)
extern DELPHI_PACKAGE System::ResourceString _icsDocBaseEditErrorGenUID;
#define Icsdocconst_icsDocBaseEditErrorGenUID System::LoadResourceString(&Icsdocconst::_icsDocBaseEditErrorGenUID)
extern DELPHI_PACKAGE System::ResourceString _icsDocGlobals;
#define Icsdocconst_icsDocGlobals System::LoadResourceString(&Icsdocconst::_icsDocGlobals)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterCaption;
#define Icsdocconst_icsDocFilterCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterFilterTBCaption;
#define Icsdocconst_icsDocFilterFilterTBCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterFilterTBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterPassportCaption;
#define Icsdocconst_icsDocFilterPassportCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterPassportCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterPassportHint;
#define Icsdocconst_icsDocFilterPassportHint System::LoadResourceString(&Icsdocconst::_icsDocFilterPassportHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterCustomLoadSaveCaption;
#define Icsdocconst_icsDocFilterCustomLoadSaveCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterCustomLoadSaveCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterCustomLoadSaveHint;
#define Icsdocconst_icsDocFilterCustomLoadSaveHint System::LoadResourceString(&Icsdocconst::_icsDocFilterCustomLoadSaveHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterCustomLoadCaption;
#define Icsdocconst_icsDocFilterCustomLoadCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterCustomLoadCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterCustomLoadHint;
#define Icsdocconst_icsDocFilterCustomLoadHint System::LoadResourceString(&Icsdocconst::_icsDocFilterCustomLoadHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterORCaption;
#define Icsdocconst_icsDocFilterORCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterORCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterORHint;
#define Icsdocconst_icsDocFilterORHint System::LoadResourceString(&Icsdocconst::_icsDocFilterORHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterANDCaption;
#define Icsdocconst_icsDocFilterANDCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterANDCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterANDHint;
#define Icsdocconst_icsDocFilterANDHint System::LoadResourceString(&Icsdocconst::_icsDocFilterANDHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterLogicNOTCaption;
#define Icsdocconst_icsDocFilterLogicNOTCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterLogicNOTCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterLogicNOTHint;
#define Icsdocconst_icsDocFilterLogicNOTHint System::LoadResourceString(&Icsdocconst::_icsDocFilterLogicNOTHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterNOTCaption;
#define Icsdocconst_icsDocFilterNOTCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterNOTCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterNOTHint;
#define Icsdocconst_icsDocFilterNOTHint System::LoadResourceString(&Icsdocconst::_icsDocFilterNOTHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterAddCondCaption;
#define Icsdocconst_icsDocFilterAddCondCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterAddCondCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterAddCondHint;
#define Icsdocconst_icsDocFilterAddCondHint System::LoadResourceString(&Icsdocconst::_icsDocFilterAddCondHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterDeleteCaption;
#define Icsdocconst_icsDocFilterDeleteCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterDeleteCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterDeleteHint;
#define Icsdocconst_icsDocFilterDeleteHint System::LoadResourceString(&Icsdocconst::_icsDocFilterDeleteHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterKonkrCaption;
#define Icsdocconst_icsDocFilterKonkrCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterKonkrCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterKonkrHint;
#define Icsdocconst_icsDocFilterKonkrHint System::LoadResourceString(&Icsdocconst::_icsDocFilterKonkrHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterDeKonkrCaption;
#define Icsdocconst_icsDocFilterDeKonkrCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterDeKonkrCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterDeKonkrHint;
#define Icsdocconst_icsDocFilterDeKonkrHint System::LoadResourceString(&Icsdocconst::_icsDocFilterDeKonkrHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterCommentCaption;
#define Icsdocconst_icsDocFilterCommentCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterCommentCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterCommentHint;
#define Icsdocconst_icsDocFilterCommentHint System::LoadResourceString(&Icsdocconst::_icsDocFilterCommentHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterCheckCaption;
#define Icsdocconst_icsDocFilterCheckCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterCheckCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterCheckHint;
#define Icsdocconst_icsDocFilterCheckHint System::LoadResourceString(&Icsdocconst::_icsDocFilterCheckHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterXMLTitle;
#define Icsdocconst_icsDocFilterXMLTitle System::LoadResourceString(&Icsdocconst::_icsDocFilterXMLTitle)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterErrorSetIsFilter;
#define Icsdocconst_icsDocFilterErrorSetIsFilter System::LoadResourceString(&Icsdocconst::_icsDocFilterErrorSetIsFilter)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterErrorLoadNomDLL;
#define Icsdocconst_icsDocFilterErrorLoadNomDLL System::LoadResourceString(&Icsdocconst::_icsDocFilterErrorLoadNomDLL)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterErrorFreeNomDLL;
#define Icsdocconst_icsDocFilterErrorFreeNomDLL System::LoadResourceString(&Icsdocconst::_icsDocFilterErrorFreeNomDLL)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterErrorCallProc;
#define Icsdocconst_icsDocFilterErrorCallProc System::LoadResourceString(&Icsdocconst::_icsDocFilterErrorCallProc)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterErrorCaption;
#define Icsdocconst_icsDocFilterErrorCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterErrorCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterConformCaption;
#define Icsdocconst_icsDocFilterConformCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterConformCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterCheckOkCaption;
#define Icsdocconst_icsDocFilterCheckOkCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterCheckOkCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterCheckEmptyCaption;
#define Icsdocconst_icsDocFilterCheckEmptyCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterCheckEmptyCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterCheckNoBodyCaption;
#define Icsdocconst_icsDocFilterCheckNoBodyCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterCheckNoBodyCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterNewCaption;
#define Icsdocconst_icsDocFilterNewCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterNewCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterNewAuthorCaption;
#define Icsdocconst_icsDocFilterNewAuthorCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterNewAuthorCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterErrorEmptyGroup;
#define Icsdocconst_icsDocFilterErrorEmptyGroup System::LoadResourceString(&Icsdocconst::_icsDocFilterErrorEmptyGroup)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterErrorEmptyValue;
#define Icsdocconst_icsDocFilterErrorEmptyValue System::LoadResourceString(&Icsdocconst::_icsDocFilterErrorEmptyValue)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterErrorIS;
#define Icsdocconst_icsDocFilterErrorIS System::LoadResourceString(&Icsdocconst::_icsDocFilterErrorIS)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterErrorISDomain;
#define Icsdocconst_icsDocFilterErrorISDomain System::LoadResourceString(&Icsdocconst::_icsDocFilterErrorISDomain)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterEditCaption;
#define Icsdocconst_icsDocFilterEditCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterEditCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterEditOkBtnCaption;
#define Icsdocconst_icsDocFilterEditOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterEditOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterEditCancelBtnCaption;
#define Icsdocconst_icsDocFilterEditCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterEditCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterEditHelpBtnCaption;
#define Icsdocconst_icsDocFilterEditHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocFilterEditHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocFilterUtilsFilterVersion;
#define Icsdocconst_icsDocFilterUtilsFilterVersion System::LoadResourceString(&Icsdocconst::_icsDocFilterUtilsFilterVersion)
extern DELPHI_PACKAGE System::ResourceString _icsDocOptions;
#define Icsdocconst_icsDocOptions System::LoadResourceString(&Icsdocconst::_icsDocOptions)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes01;
#define Icsdocconst_icsDocParsersLexTypes01 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes01)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes02;
#define Icsdocconst_icsDocParsersLexTypes02 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes02)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes03;
#define Icsdocconst_icsDocParsersLexTypes03 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes03)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes04;
#define Icsdocconst_icsDocParsersLexTypes04 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes04)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes05;
#define Icsdocconst_icsDocParsersLexTypes05 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes05)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes06;
#define Icsdocconst_icsDocParsersLexTypes06 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes06)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes07;
#define Icsdocconst_icsDocParsersLexTypes07 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes07)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes08;
#define Icsdocconst_icsDocParsersLexTypes08 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes08)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes09;
#define Icsdocconst_icsDocParsersLexTypes09 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes09)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes10;
#define Icsdocconst_icsDocParsersLexTypes10 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes10)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes11;
#define Icsdocconst_icsDocParsersLexTypes11 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes11)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes12;
#define Icsdocconst_icsDocParsersLexTypes12 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes12)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes13;
#define Icsdocconst_icsDocParsersLexTypes13 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes13)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes14;
#define Icsdocconst_icsDocParsersLexTypes14 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes14)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes15;
#define Icsdocconst_icsDocParsersLexTypes15 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes15)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes16;
#define Icsdocconst_icsDocParsersLexTypes16 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes16)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes17;
#define Icsdocconst_icsDocParsersLexTypes17 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes17)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes18;
#define Icsdocconst_icsDocParsersLexTypes18 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes18)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes19;
#define Icsdocconst_icsDocParsersLexTypes19 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes19)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes20;
#define Icsdocconst_icsDocParsersLexTypes20 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes20)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexTypes21;
#define Icsdocconst_icsDocParsersLexTypes21 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexTypes21)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexClasses1;
#define Icsdocconst_icsDocParsersLexClasses1 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexClasses1)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexClasses2;
#define Icsdocconst_icsDocParsersLexClasses2 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexClasses2)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexClasses3;
#define Icsdocconst_icsDocParsersLexClasses3 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexClasses3)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexClasses4;
#define Icsdocconst_icsDocParsersLexClasses4 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexClasses4)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexClasses5;
#define Icsdocconst_icsDocParsersLexClasses5 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexClasses5)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexClasses6;
#define Icsdocconst_icsDocParsersLexClasses6 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexClasses6)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexClasses7;
#define Icsdocconst_icsDocParsersLexClasses7 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexClasses7)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexClasses8;
#define Icsdocconst_icsDocParsersLexClasses8 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexClasses8)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexClasses9;
#define Icsdocconst_icsDocParsersLexClasses9 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexClasses9)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexErrors1;
#define Icsdocconst_icsDocParsersLexErrors1 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexErrors1)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexErrors2;
#define Icsdocconst_icsDocParsersLexErrors2 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexErrors2)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexErrors3;
#define Icsdocconst_icsDocParsersLexErrors3 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexErrors3)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexErrors4;
#define Icsdocconst_icsDocParsersLexErrors4 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexErrors4)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexErrors5;
#define Icsdocconst_icsDocParsersLexErrors5 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexErrors5)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexErrors6;
#define Icsdocconst_icsDocParsersLexErrors6 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexErrors6)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexErrors7;
#define Icsdocconst_icsDocParsersLexErrors7 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexErrors7)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexErrors8;
#define Icsdocconst_icsDocParsersLexErrors8 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexErrors8)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexErrors9;
#define Icsdocconst_icsDocParsersLexErrors9 System::LoadResourceString(&Icsdocconst::_icsDocParsersLexErrors9)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSyntErrors01;
#define Icsdocconst_icsDocParsersSyntErrors01 System::LoadResourceString(&Icsdocconst::_icsDocParsersSyntErrors01)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSyntErrors02;
#define Icsdocconst_icsDocParsersSyntErrors02 System::LoadResourceString(&Icsdocconst::_icsDocParsersSyntErrors02)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSyntErrors03;
#define Icsdocconst_icsDocParsersSyntErrors03 System::LoadResourceString(&Icsdocconst::_icsDocParsersSyntErrors03)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSyntErrors04;
#define Icsdocconst_icsDocParsersSyntErrors04 System::LoadResourceString(&Icsdocconst::_icsDocParsersSyntErrors04)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSyntErrors05;
#define Icsdocconst_icsDocParsersSyntErrors05 System::LoadResourceString(&Icsdocconst::_icsDocParsersSyntErrors05)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSyntErrors06;
#define Icsdocconst_icsDocParsersSyntErrors06 System::LoadResourceString(&Icsdocconst::_icsDocParsersSyntErrors06)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSyntErrors07;
#define Icsdocconst_icsDocParsersSyntErrors07 System::LoadResourceString(&Icsdocconst::_icsDocParsersSyntErrors07)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSyntErrors08;
#define Icsdocconst_icsDocParsersSyntErrors08 System::LoadResourceString(&Icsdocconst::_icsDocParsersSyntErrors08)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSyntErrors09;
#define Icsdocconst_icsDocParsersSyntErrors09 System::LoadResourceString(&Icsdocconst::_icsDocParsersSyntErrors09)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSyntErrors10;
#define Icsdocconst_icsDocParsersSyntErrors10 System::LoadResourceString(&Icsdocconst::_icsDocParsersSyntErrors10)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSyntErrors11;
#define Icsdocconst_icsDocParsersSyntErrors11 System::LoadResourceString(&Icsdocconst::_icsDocParsersSyntErrors11)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSyntErrors12;
#define Icsdocconst_icsDocParsersSyntErrors12 System::LoadResourceString(&Icsdocconst::_icsDocParsersSyntErrors12)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSyntErrors13;
#define Icsdocconst_icsDocParsersSyntErrors13 System::LoadResourceString(&Icsdocconst::_icsDocParsersSyntErrors13)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSemErrors01;
#define Icsdocconst_icsDocParsersSemErrors01 System::LoadResourceString(&Icsdocconst::_icsDocParsersSemErrors01)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSemErrors02;
#define Icsdocconst_icsDocParsersSemErrors02 System::LoadResourceString(&Icsdocconst::_icsDocParsersSemErrors02)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSemErrors03;
#define Icsdocconst_icsDocParsersSemErrors03 System::LoadResourceString(&Icsdocconst::_icsDocParsersSemErrors03)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSemErrors04;
#define Icsdocconst_icsDocParsersSemErrors04 System::LoadResourceString(&Icsdocconst::_icsDocParsersSemErrors04)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSemErrors05;
#define Icsdocconst_icsDocParsersSemErrors05 System::LoadResourceString(&Icsdocconst::_icsDocParsersSemErrors05)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSemErrors06;
#define Icsdocconst_icsDocParsersSemErrors06 System::LoadResourceString(&Icsdocconst::_icsDocParsersSemErrors06)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSemErrors07;
#define Icsdocconst_icsDocParsersSemErrors07 System::LoadResourceString(&Icsdocconst::_icsDocParsersSemErrors07)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSemErrors08;
#define Icsdocconst_icsDocParsersSemErrors08 System::LoadResourceString(&Icsdocconst::_icsDocParsersSemErrors08)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSemErrors09;
#define Icsdocconst_icsDocParsersSemErrors09 System::LoadResourceString(&Icsdocconst::_icsDocParsersSemErrors09)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSemErrors10;
#define Icsdocconst_icsDocParsersSemErrors10 System::LoadResourceString(&Icsdocconst::_icsDocParsersSemErrors10)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSemErrors11;
#define Icsdocconst_icsDocParsersSemErrors11 System::LoadResourceString(&Icsdocconst::_icsDocParsersSemErrors11)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersErrorPos;
#define Icsdocconst_icsDocParsersErrorPos System::LoadResourceString(&Icsdocconst::_icsDocParsersErrorPos)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersErrorExprLex;
#define Icsdocconst_icsDocParsersErrorExprLex System::LoadResourceString(&Icsdocconst::_icsDocParsersErrorExprLex)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersErrorExprSynt;
#define Icsdocconst_icsDocParsersErrorExprSynt System::LoadResourceString(&Icsdocconst::_icsDocParsersErrorExprSynt)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersErrorExprSem;
#define Icsdocconst_icsDocParsersErrorExprSem System::LoadResourceString(&Icsdocconst::_icsDocParsersErrorExprSem)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersOk;
#define Icsdocconst_icsDocParsersOk System::LoadResourceString(&Icsdocconst::_icsDocParsersOk)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersLexError;
#define Icsdocconst_icsDocParsersLexError System::LoadResourceString(&Icsdocconst::_icsDocParsersLexError)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSyntError;
#define Icsdocconst_icsDocParsersSyntError System::LoadResourceString(&Icsdocconst::_icsDocParsersSyntError)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersSemError;
#define Icsdocconst_icsDocParsersSemError System::LoadResourceString(&Icsdocconst::_icsDocParsersSemError)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersNotConst;
#define Icsdocconst_icsDocParsersNotConst System::LoadResourceString(&Icsdocconst::_icsDocParsersNotConst)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersAndConst;
#define Icsdocconst_icsDocParsersAndConst System::LoadResourceString(&Icsdocconst::_icsDocParsersAndConst)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersOrConst;
#define Icsdocconst_icsDocParsersOrConst System::LoadResourceString(&Icsdocconst::_icsDocParsersOrConst)
extern DELPHI_PACKAGE System::ResourceString _icsDocParsersErrorPolandStr;
#define Icsdocconst_icsDocParsersErrorPolandStr System::LoadResourceString(&Icsdocconst::_icsDocParsersErrorPolandStr)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecCaption;
#define Icsdocconst_icsDocSpecCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecTabPrtyColCaption;
#define Icsdocconst_icsDocSpecTabPrtyColCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecTabPrtyColCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecTabPrtyRowCaption;
#define Icsdocconst_icsDocSpecTabPrtyRowCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecTabPrtyRowCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecEmptyColCaption;
#define Icsdocconst_icsDocSpecEmptyColCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecEmptyColCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecCountViewText;
#define Icsdocconst_icsDocSpecCountViewText System::LoadResourceString(&Icsdocconst::_icsDocSpecCountViewText)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecEmptyEllipsiseEditor;
#define Icsdocconst_icsDocSpecEmptyEllipsiseEditor System::LoadResourceString(&Icsdocconst::_icsDocSpecEmptyEllipsiseEditor)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecNotEmptyEllipsiseEditor;
#define Icsdocconst_icsDocSpecNotEmptyEllipsiseEditor System::LoadResourceString(&Icsdocconst::_icsDocSpecNotEmptyEllipsiseEditor)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecEmptyInsCaption;
#define Icsdocconst_icsDocSpecEmptyInsCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecEmptyInsCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecInscViewText;
#define Icsdocconst_icsDocSpecInscViewText System::LoadResourceString(&Icsdocconst::_icsDocSpecInscViewText)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecListViewText;
#define Icsdocconst_icsDocSpecListViewText System::LoadResourceString(&Icsdocconst::_icsDocSpecListViewText)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecTableViewText;
#define Icsdocconst_icsDocSpecTableViewText System::LoadResourceString(&Icsdocconst::_icsDocSpecTableViewText)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecMainMenuCaption;
#define Icsdocconst_icsDocSpecMainMenuCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecMainMenuCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecPropPanelCaption;
#define Icsdocconst_icsDocSpecPropPanelCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecPropPanelCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecParametersCaption;
#define Icsdocconst_icsDocSpecParametersCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecParametersCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecParametersHint;
#define Icsdocconst_icsDocSpecParametersHint System::LoadResourceString(&Icsdocconst::_icsDocSpecParametersHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecParametersShortCut;
#define Icsdocconst_icsDocSpecParametersShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecParametersShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecPreViewCaption;
#define Icsdocconst_icsDocSpecPreViewCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecPreViewCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecPreViewHint;
#define Icsdocconst_icsDocSpecPreViewHint System::LoadResourceString(&Icsdocconst::_icsDocSpecPreViewHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecPreViewShortCut;
#define Icsdocconst_icsDocSpecPreViewShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecPreViewShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecCutCaption;
#define Icsdocconst_icsDocSpecCutCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecCutCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecCutHint;
#define Icsdocconst_icsDocSpecCutHint System::LoadResourceString(&Icsdocconst::_icsDocSpecCutHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecCutShortCut;
#define Icsdocconst_icsDocSpecCutShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecCutShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecCopyCaption;
#define Icsdocconst_icsDocSpecCopyCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecCopyCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecCopyHint;
#define Icsdocconst_icsDocSpecCopyHint System::LoadResourceString(&Icsdocconst::_icsDocSpecCopyHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecCopyShortCut;
#define Icsdocconst_icsDocSpecCopyShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecCopyShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecPasteCaption;
#define Icsdocconst_icsDocSpecPasteCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecPasteCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecPasteHint;
#define Icsdocconst_icsDocSpecPasteHint System::LoadResourceString(&Icsdocconst::_icsDocSpecPasteHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecPasteShortCut;
#define Icsdocconst_icsDocSpecPasteShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecPasteShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecDeleteCaption;
#define Icsdocconst_icsDocSpecDeleteCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecDeleteCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecDeleteHint;
#define Icsdocconst_icsDocSpecDeleteHint System::LoadResourceString(&Icsdocconst::_icsDocSpecDeleteHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecDeleteShortCut;
#define Icsdocconst_icsDocSpecDeleteShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecDeleteShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddInscCaption;
#define Icsdocconst_icsDocSpecAddInscCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecAddInscCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddInscHint;
#define Icsdocconst_icsDocSpecAddInscHint System::LoadResourceString(&Icsdocconst::_icsDocSpecAddInscHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddInscShortCut;
#define Icsdocconst_icsDocSpecAddInscShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecAddInscShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddInsTextCaption;
#define Icsdocconst_icsDocSpecAddInsTextCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecAddInsTextCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddInsTextHint;
#define Icsdocconst_icsDocSpecAddInsTextHint System::LoadResourceString(&Icsdocconst::_icsDocSpecAddInsTextHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddInsTextShortCut;
#define Icsdocconst_icsDocSpecAddInsTextShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecAddInsTextShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddListCaption;
#define Icsdocconst_icsDocSpecAddListCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecAddListCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddListHint;
#define Icsdocconst_icsDocSpecAddListHint System::LoadResourceString(&Icsdocconst::_icsDocSpecAddListHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddListShortCut;
#define Icsdocconst_icsDocSpecAddListShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecAddListShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddLColCaption;
#define Icsdocconst_icsDocSpecAddLColCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecAddLColCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddLColHint;
#define Icsdocconst_icsDocSpecAddLColHint System::LoadResourceString(&Icsdocconst::_icsDocSpecAddLColHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddLColShortCut;
#define Icsdocconst_icsDocSpecAddLColShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecAddLColShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddTableCaption;
#define Icsdocconst_icsDocSpecAddTableCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecAddTableCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddTableHint;
#define Icsdocconst_icsDocSpecAddTableHint System::LoadResourceString(&Icsdocconst::_icsDocSpecAddTableHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddTableShortCut;
#define Icsdocconst_icsDocSpecAddTableShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecAddTableShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddTColCaption;
#define Icsdocconst_icsDocSpecAddTColCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecAddTColCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddTColHint;
#define Icsdocconst_icsDocSpecAddTColHint System::LoadResourceString(&Icsdocconst::_icsDocSpecAddTColHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddTColShortCut;
#define Icsdocconst_icsDocSpecAddTColShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecAddTColShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddTRowCaption;
#define Icsdocconst_icsDocSpecAddTRowCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecAddTRowCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddTRowHint;
#define Icsdocconst_icsDocSpecAddTRowHint System::LoadResourceString(&Icsdocconst::_icsDocSpecAddTRowHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddTRowShortCut;
#define Icsdocconst_icsDocSpecAddTRowShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecAddTRowShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecMoveUpCaption;
#define Icsdocconst_icsDocSpecMoveUpCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecMoveUpCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecMoveUpHint;
#define Icsdocconst_icsDocSpecMoveUpHint System::LoadResourceString(&Icsdocconst::_icsDocSpecMoveUpHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecMoveUpShortCut;
#define Icsdocconst_icsDocSpecMoveUpShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecMoveUpShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecMoveDownCaption;
#define Icsdocconst_icsDocSpecMoveDownCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecMoveDownCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecMoveDownHint;
#define Icsdocconst_icsDocSpecMoveDownHint System::LoadResourceString(&Icsdocconst::_icsDocSpecMoveDownHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecMoveDownShortCut;
#define Icsdocconst_icsDocSpecMoveDownShortCut System::LoadResourceString(&Icsdocconst::_icsDocSpecMoveDownShortCut)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecCommonItemCaption;
#define Icsdocconst_icsDocSpecCommonItemCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecCommonItemCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecEditItemCaption;
#define Icsdocconst_icsDocSpecEditItemCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecEditItemCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecObjectItemCaption;
#define Icsdocconst_icsDocSpecObjectItemCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecObjectItemCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecXMLTitle;
#define Icsdocconst_icsDocSpecXMLTitle System::LoadResourceString(&Icsdocconst::_icsDocSpecXMLTitle)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecErrorCreateXML;
#define Icsdocconst_icsDocSpecErrorCreateXML System::LoadResourceString(&Icsdocconst::_icsDocSpecErrorCreateXML)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecLineName0Col;
#define Icsdocconst_icsDocSpecLineName0Col System::LoadResourceString(&Icsdocconst::_icsDocSpecLineName0Col)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecLineName00Col;
#define Icsdocconst_icsDocSpecLineName00Col System::LoadResourceString(&Icsdocconst::_icsDocSpecLineName00Col)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecLineName1Col;
#define Icsdocconst_icsDocSpecLineName1Col System::LoadResourceString(&Icsdocconst::_icsDocSpecLineName1Col)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecLineName2Col;
#define Icsdocconst_icsDocSpecLineName2Col System::LoadResourceString(&Icsdocconst::_icsDocSpecLineName2Col)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecLineName0Row;
#define Icsdocconst_icsDocSpecLineName0Row System::LoadResourceString(&Icsdocconst::_icsDocSpecLineName0Row)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecLineName00Row;
#define Icsdocconst_icsDocSpecLineName00Row System::LoadResourceString(&Icsdocconst::_icsDocSpecLineName00Row)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecLineName1Row;
#define Icsdocconst_icsDocSpecLineName1Row System::LoadResourceString(&Icsdocconst::_icsDocSpecLineName1Row)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecLineName2Row;
#define Icsdocconst_icsDocSpecLineName2Row System::LoadResourceString(&Icsdocconst::_icsDocSpecLineName2Row)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecCanDelColRowDopInfo;
#define Icsdocconst_icsDocSpecCanDelColRowDopInfo System::LoadResourceString(&Icsdocconst::_icsDocSpecCanDelColRowDopInfo)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecErrorCanDelColRow;
#define Icsdocconst_icsDocSpecErrorCanDelColRow System::LoadResourceString(&Icsdocconst::_icsDocSpecErrorCanDelColRow)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecMsgCanDeleteDocRulesRefObj;
#define Icsdocconst_icsDocSpecMsgCanDeleteDocRulesRefObj System::LoadResourceString(&Icsdocconst::_icsDocSpecMsgCanDeleteDocRulesRefObj)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecGetXMLNodeViewTextRow;
#define Icsdocconst_icsDocSpecGetXMLNodeViewTextRow System::LoadResourceString(&Icsdocconst::_icsDocSpecGetXMLNodeViewTextRow)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecGetXMLNodeViewTextCol;
#define Icsdocconst_icsDocSpecGetXMLNodeViewTextCol System::LoadResourceString(&Icsdocconst::_icsDocSpecGetXMLNodeViewTextCol)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecGetCountCaption;
#define Icsdocconst_icsDocSpecGetCountCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecGetCountCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecGetSizeViewTextAuto;
#define Icsdocconst_icsDocSpecGetSizeViewTextAuto System::LoadResourceString(&Icsdocconst::_icsDocSpecGetSizeViewTextAuto)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecGetSizeViewTextFullWidth;
#define Icsdocconst_icsDocSpecGetSizeViewTextFullWidth System::LoadResourceString(&Icsdocconst::_icsDocSpecGetSizeViewTextFullWidth)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecGetSizeViewTextFullHeight;
#define Icsdocconst_icsDocSpecGetSizeViewTextFullHeight System::LoadResourceString(&Icsdocconst::_icsDocSpecGetSizeViewTextFullHeight)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecErrorApplyStyle;
#define Icsdocconst_icsDocSpecErrorApplyStyle System::LoadResourceString(&Icsdocconst::_icsDocSpecErrorApplyStyle)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecErrorApplyUnion;
#define Icsdocconst_icsDocSpecErrorApplyUnion System::LoadResourceString(&Icsdocconst::_icsDocSpecErrorApplyUnion)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecErrorApplyText;
#define Icsdocconst_icsDocSpecErrorApplyText System::LoadResourceString(&Icsdocconst::_icsDocSpecErrorApplyText)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecErrorApplyInt;
#define Icsdocconst_icsDocSpecErrorApplyInt System::LoadResourceString(&Icsdocconst::_icsDocSpecErrorApplyInt)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecCalcCountRefConf;
#define Icsdocconst_icsDocSpecCalcCountRefConf System::LoadResourceString(&Icsdocconst::_icsDocSpecCalcCountRefConf)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecSetRefDlgCaption;
#define Icsdocconst_icsDocSpecSetRefDlgCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecSetRefDlgCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecSetRefDlTreegCaption;
#define Icsdocconst_icsDocSpecSetRefDlTreegCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecSetRefDlTreegCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecSetRefRefObjCaption;
#define Icsdocconst_icsDocSpecSetRefRefObjCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecSetRefRefObjCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecSetRefDocParamCaption;
#define Icsdocconst_icsDocSpecSetRefDocParamCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecSetRefDocParamCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecSetRefTechInfoCaption;
#define Icsdocconst_icsDocSpecSetRefTechInfoCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecSetRefTechInfoCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecErrorSetRefDopInfo;
#define Icsdocconst_icsDocSpecErrorSetRefDopInfo System::LoadResourceString(&Icsdocconst::_icsDocSpecErrorSetRefDopInfo)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecErrorSetRefMsg;
#define Icsdocconst_icsDocSpecErrorSetRefMsg System::LoadResourceString(&Icsdocconst::_icsDocSpecErrorSetRefMsg)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecErrorOperation;
#define Icsdocconst_icsDocSpecErrorOperation System::LoadResourceString(&Icsdocconst::_icsDocSpecErrorOperation)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddTableRowNum;
#define Icsdocconst_icsDocSpecAddTableRowNum System::LoadResourceString(&Icsdocconst::_icsDocSpecAddTableRowNum)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddTableColNum;
#define Icsdocconst_icsDocSpecAddTableColNum System::LoadResourceString(&Icsdocconst::_icsDocSpecAddTableColNum)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddTableNewCol;
#define Icsdocconst_icsDocSpecAddTableNewCol System::LoadResourceString(&Icsdocconst::_icsDocSpecAddTableNewCol)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecAddTableNewRow;
#define Icsdocconst_icsDocSpecAddTableNewRow System::LoadResourceString(&Icsdocconst::_icsDocSpecAddTableNewRow)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecWidthCaption;
#define Icsdocconst_icsDocSpecWidthCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecWidthCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecRulesCaption;
#define Icsdocconst_icsDocSpecRulesCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecRulesCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecCalcRulesCaption;
#define Icsdocconst_icsDocSpecCalcRulesCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecCalcRulesCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecCheckRulesCaption;
#define Icsdocconst_icsDocSpecCheckRulesCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecCheckRulesCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecDataStyleCaption;
#define Icsdocconst_icsDocSpecDataStyleCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecDataStyleCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecHeaderStyleCaption;
#define Icsdocconst_icsDocSpecHeaderStyleCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecHeaderStyleCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecDefaultHeaderStyleCaption;
#define Icsdocconst_icsDocSpecDefaultHeaderStyleCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecDefaultHeaderStyleCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecDefaultDataStyleCaption;
#define Icsdocconst_icsDocSpecDefaultDataStyleCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecDefaultDataStyleCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecHeaderCaption;
#define Icsdocconst_icsDocSpecHeaderCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecHeaderCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecStyleCaption;
#define Icsdocconst_icsDocSpecStyleCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecStyleCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecDefaultStyleCaption;
#define Icsdocconst_icsDocSpecDefaultStyleCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecDefaultStyleCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecNewRowCaption;
#define Icsdocconst_icsDocSpecNewRowCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecNewRowCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecStaticTextCaption;
#define Icsdocconst_icsDocSpecStaticTextCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecStaticTextCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecRefCaption;
#define Icsdocconst_icsDocSpecRefCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecRefCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecCalcCountCaption;
#define Icsdocconst_icsDocSpecCalcCountCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecCalcCountCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecColUnionCaption;
#define Icsdocconst_icsDocSpecColUnionCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecColUnionCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecShowingCaption;
#define Icsdocconst_icsDocSpecShowingCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecShowingCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecSelectDataFromCaption;
#define Icsdocconst_icsDocSpecSelectDataFromCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecSelectDataFromCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecOrderCaption;
#define Icsdocconst_icsDocSpecOrderCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecOrderCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecShowZeroValsCaption;
#define Icsdocconst_icsDocSpecShowZeroValsCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecShowZeroValsCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecThickRectCaption;
#define Icsdocconst_icsDocSpecThickRectCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecThickRectCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecRowNumCaption;
#define Icsdocconst_icsDocSpecRowNumCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecRowNumCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecColNumCaption;
#define Icsdocconst_icsDocSpecColNumCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecColNumCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecPriorityCaption;
#define Icsdocconst_icsDocSpecPriorityCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecPriorityCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecONE_VAVCaption;
#define Icsdocconst_icsDocSpecONE_VAVCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecONE_VAVCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecZERO_VAVCaption;
#define Icsdocconst_icsDocSpecZERO_VAVCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecZERO_VAVCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecTRUE_VAVCaption;
#define Icsdocconst_icsDocSpecTRUE_VAVCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecTRUE_VAVCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecFALSE_VAVCaption;
#define Icsdocconst_icsDocSpecFALSE_VAVCaption System::LoadResourceString(&Icsdocconst::_icsDocSpecFALSE_VAVCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSpecUtilsReqVersion;
#define Icsdocconst_icsDocSpecUtilsReqVersion System::LoadResourceString(&Icsdocconst::_icsDocSpecUtilsReqVersion)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerIEHeader;
#define Icsdocconst_icsDocViewerIEHeader System::LoadResourceString(&Icsdocconst::_icsDocViewerIEHeader)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerCaption;
#define Icsdocconst_icsDocViewerCaption System::LoadResourceString(&Icsdocconst::_icsDocViewerCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerNumTCellLTitle;
#define Icsdocconst_icsDocViewerNumTCellLTitle System::LoadResourceString(&Icsdocconst::_icsDocViewerNumTCellLTitle)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorObjectLocked;
#define Icsdocconst_icsDocViewerErrorObjectLocked System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorObjectLocked)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorSetDocFormat;
#define Icsdocconst_icsDocViewerErrorSetDocFormat System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorSetDocFormat)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorNoSpecDef;
#define Icsdocconst_icsDocViewerErrorNoSpecDef System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorNoSpecDef)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorNoDocDef;
#define Icsdocconst_icsDocViewerErrorNoDocDef System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorNoDocDef)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorPreviewTypeVal;
#define Icsdocconst_icsDocViewerErrorPreviewTypeVal System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorPreviewTypeVal)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorOpenTmpFile;
#define Icsdocconst_icsDocViewerErrorOpenTmpFile System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorOpenTmpFile)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorApp;
#define Icsdocconst_icsDocViewerErrorApp System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorApp)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorClass;
#define Icsdocconst_icsDocViewerErrorClass System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorClass)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorOnFly;
#define Icsdocconst_icsDocViewerErrorOnFly System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorOnFly)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorMsgCaption;
#define Icsdocconst_icsDocViewerErrorMsgCaption System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorMsgCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorRepCreate1;
#define Icsdocconst_icsDocViewerErrorRepCreate1 System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorRepCreate1)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorRepCreate2;
#define Icsdocconst_icsDocViewerErrorRepCreate2 System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorRepCreate2)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorRepCreate3;
#define Icsdocconst_icsDocViewerErrorRepCreate3 System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorRepCreate3)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorCreateMSWord;
#define Icsdocconst_icsDocViewerErrorCreateMSWord System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorCreateMSWord)
extern DELPHI_PACKAGE System::ResourceString _icsDocViewerErrorIllegalOperation;
#define Icsdocconst_icsDocViewerErrorIllegalOperation System::LoadResourceString(&Icsdocconst::_icsDocViewerErrorIllegalOperation)
extern DELPHI_PACKAGE System::ResourceString _icsDocInclToNameEditCaption;
#define Icsdocconst_icsDocInclToNameEditCaption System::LoadResourceString(&Icsdocconst::_icsDocInclToNameEditCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocInclToNameEditOkBtnCaption;
#define Icsdocconst_icsDocInclToNameEditOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocInclToNameEditOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocInclToNameEditCancelBtnCaption;
#define Icsdocconst_icsDocInclToNameEditCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocInclToNameEditCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocInclToNameEditHelpBtnCaption;
#define Icsdocconst_icsDocInclToNameEditHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocInclToNameEditHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionErrorCreate1;
#define Icsdocconst_icsDocISectionErrorCreate1 System::LoadResourceString(&Icsdocconst::_icsDocISectionErrorCreate1)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionErrorCreate2;
#define Icsdocconst_icsDocISectionErrorCreate2 System::LoadResourceString(&Icsdocconst::_icsDocISectionErrorCreate2)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionCaption1;
#define Icsdocconst_icsDocISectionCaption1 System::LoadResourceString(&Icsdocconst::_icsDocISectionCaption1)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionCaption2;
#define Icsdocconst_icsDocISectionCaption2 System::LoadResourceString(&Icsdocconst::_icsDocISectionCaption2)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionErrorCaption;
#define Icsdocconst_icsDocISectionErrorCaption System::LoadResourceString(&Icsdocconst::_icsDocISectionErrorCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionLeaveAsIsBtnCaption;
#define Icsdocconst_icsDocISectionLeaveAsIsBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocISectionLeaveAsIsBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionOkBtnCaption;
#define Icsdocconst_icsDocISectionOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocISectionOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionCancelBtnCaption;
#define Icsdocconst_icsDocISectionCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocISectionCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionHelpBtnCaption;
#define Icsdocconst_icsDocISectionHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocISectionHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionSelAllCaption;
#define Icsdocconst_icsDocISectionSelAllCaption System::LoadResourceString(&Icsdocconst::_icsDocISectionSelAllCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionSelAllHint;
#define Icsdocconst_icsDocISectionSelAllHint System::LoadResourceString(&Icsdocconst::_icsDocISectionSelAllHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionClearSelCaption;
#define Icsdocconst_icsDocISectionClearSelCaption System::LoadResourceString(&Icsdocconst::_icsDocISectionClearSelCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionClearSelHint;
#define Icsdocconst_icsDocISectionClearSelHint System::LoadResourceString(&Icsdocconst::_icsDocISectionClearSelHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionInvSelCaption;
#define Icsdocconst_icsDocISectionInvSelCaption System::LoadResourceString(&Icsdocconst::_icsDocISectionInvSelCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocISectionInvSelHint;
#define Icsdocconst_icsDocISectionInvSelHint System::LoadResourceString(&Icsdocconst::_icsDocISectionInvSelHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderCaption;
#define Icsdocconst_icsDocListSortOrderCaption System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderColLabCaption;
#define Icsdocconst_icsDocListSortOrderColLabCaption System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderColLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderSortOrderLabCaption;
#define Icsdocconst_icsDocListSortOrderSortOrderLabCaption System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderSortOrderLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderAddColsCaption;
#define Icsdocconst_icsDocListSortOrderAddColsCaption System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderAddColsCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderAddColsHint;
#define Icsdocconst_icsDocListSortOrderAddColsHint System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderAddColsHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderDeleteCaption;
#define Icsdocconst_icsDocListSortOrderDeleteCaption System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderDeleteCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderDeleteHint;
#define Icsdocconst_icsDocListSortOrderDeleteHint System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderDeleteHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderClearCaption;
#define Icsdocconst_icsDocListSortOrderClearCaption System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderClearCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderClearHint;
#define Icsdocconst_icsDocListSortOrderClearHint System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderClearHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderMoveUpCaption;
#define Icsdocconst_icsDocListSortOrderMoveUpCaption System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderMoveUpCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderMoveUpHint;
#define Icsdocconst_icsDocListSortOrderMoveUpHint System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderMoveUpHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderMoveDownCaption;
#define Icsdocconst_icsDocListSortOrderMoveDownCaption System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderMoveDownCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderMoveDownHint;
#define Icsdocconst_icsDocListSortOrderMoveDownHint System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderMoveDownHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderAscSortCaption;
#define Icsdocconst_icsDocListSortOrderAscSortCaption System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderAscSortCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderAscSortHint;
#define Icsdocconst_icsDocListSortOrderAscSortHint System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderAscSortHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderDescSortCaption;
#define Icsdocconst_icsDocListSortOrderDescSortCaption System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderDescSortCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderDescSortHint;
#define Icsdocconst_icsDocListSortOrderDescSortHint System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderDescSortHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderOkBtnCaption;
#define Icsdocconst_icsDocListSortOrderOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderCancelBtnCaption;
#define Icsdocconst_icsDocListSortOrderCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocListSortOrderHelpBtnCaption;
#define Icsdocconst_icsDocListSortOrderHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocListSortOrderHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditTitleCaptionNormal;
#define Icsdocconst_icsDocObjRulesAssocEditTitleCaptionNormal System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditTitleCaptionNormal)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditTitleCaptionCross;
#define Icsdocconst_icsDocObjRulesAssocEditTitleCaptionCross System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditTitleCaptionCross)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditTableElementNameCol;
#define Icsdocconst_icsDocObjRulesAssocEditTableElementNameCol System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditTableElementNameCol)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditTableElementNameRow;
#define Icsdocconst_icsDocObjRulesAssocEditTableElementNameRow System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditTableElementNameRow)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditSetAssocConf;
#define Icsdocconst_icsDocObjRulesAssocEditSetAssocConf System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditSetAssocConf)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditCaption;
#define Icsdocconst_icsDocObjRulesAssocEditCaption System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditThisStructTitle;
#define Icsdocconst_icsDocObjRulesAssocEditThisStructTitle System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditThisStructTitle)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditRefStructTitle;
#define Icsdocconst_icsDocObjRulesAssocEditRefStructTitle System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditRefStructTitle)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditAutoExpand;
#define Icsdocconst_icsDocObjRulesAssocEditAutoExpand System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditAutoExpand)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditSetAssocCaption;
#define Icsdocconst_icsDocObjRulesAssocEditSetAssocCaption System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditSetAssocCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditSetAssocHint;
#define Icsdocconst_icsDocObjRulesAssocEditSetAssocHint System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditSetAssocHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditOkCaption;
#define Icsdocconst_icsDocObjRulesAssocEditOkCaption System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditOkCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditOkHint;
#define Icsdocconst_icsDocObjRulesAssocEditOkHint System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditOkHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditClearAssocCaption;
#define Icsdocconst_icsDocObjRulesAssocEditClearAssocCaption System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditClearAssocCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditClearAssocHint;
#define Icsdocconst_icsDocObjRulesAssocEditClearAssocHint System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditClearAssocHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditCancelBtnCaption;
#define Icsdocconst_icsDocObjRulesAssocEditCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesAssocEditHelpBtnCaption;
#define Icsdocconst_icsDocObjRulesAssocEditHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocObjRulesAssocEditHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesEditCaption;
#define Icsdocconst_icsDocObjRulesEditCaption System::LoadResourceString(&Icsdocconst::_icsDocObjRulesEditCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesEditObjTitleCaption;
#define Icsdocconst_icsDocObjRulesEditObjTitleCaption System::LoadResourceString(&Icsdocconst::_icsDocObjRulesEditObjTitleCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesEditThisObjTitle;
#define Icsdocconst_icsDocObjRulesEditThisObjTitle System::LoadResourceString(&Icsdocconst::_icsDocObjRulesEditThisObjTitle)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesEditRefObjTitle;
#define Icsdocconst_icsDocObjRulesEditRefObjTitle System::LoadResourceString(&Icsdocconst::_icsDocObjRulesEditRefObjTitle)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesEditPropHead;
#define Icsdocconst_icsDocObjRulesEditPropHead System::LoadResourceString(&Icsdocconst::_icsDocObjRulesEditPropHead)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesEditAssocType;
#define Icsdocconst_icsDocObjRulesEditAssocType System::LoadResourceString(&Icsdocconst::_icsDocObjRulesEditAssocType)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesEditCrossTypeN;
#define Icsdocconst_icsDocObjRulesEditCrossTypeN System::LoadResourceString(&Icsdocconst::_icsDocObjRulesEditCrossTypeN)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesEditCrossTypeC;
#define Icsdocconst_icsDocObjRulesEditCrossTypeC System::LoadResourceString(&Icsdocconst::_icsDocObjRulesEditCrossTypeC)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesEditOkCaption;
#define Icsdocconst_icsDocObjRulesEditOkCaption System::LoadResourceString(&Icsdocconst::_icsDocObjRulesEditOkCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesEditOkHint;
#define Icsdocconst_icsDocObjRulesEditOkHint System::LoadResourceString(&Icsdocconst::_icsDocObjRulesEditOkHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesEditCancelBtnCaption;
#define Icsdocconst_icsDocObjRulesEditCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocObjRulesEditCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocObjRulesEditHelpBtnCaption;
#define Icsdocconst_icsDocObjRulesEditHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocObjRulesEditHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditSpecName;
#define Icsdocconst_icsDocParametersEditSpecName System::LoadResourceString(&Icsdocconst::_icsDocParametersEditSpecName)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditOrientation1;
#define Icsdocconst_icsDocParametersEditOrientation1 System::LoadResourceString(&Icsdocconst::_icsDocParametersEditOrientation1)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditOrientation2;
#define Icsdocconst_icsDocParametersEditOrientation2 System::LoadResourceString(&Icsdocconst::_icsDocParametersEditOrientation2)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditStyleExists;
#define Icsdocconst_icsDocParametersEditStyleExists System::LoadResourceString(&Icsdocconst::_icsDocParametersEditStyleExists)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditCDERR_DIALOGFAILURE;
#define Icsdocconst_icsDocParametersEditCDERR_DIALOGFAILURE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditCDERR_DIALOGFAILURE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditCDERR_FINDRESFAILURE;
#define Icsdocconst_icsDocParametersEditCDERR_FINDRESFAILURE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditCDERR_FINDRESFAILURE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditCDERR_INITIALIZATION;
#define Icsdocconst_icsDocParametersEditCDERR_INITIALIZATION System::LoadResourceString(&Icsdocconst::_icsDocParametersEditCDERR_INITIALIZATION)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditCDERR_LOADRESFAILURE;
#define Icsdocconst_icsDocParametersEditCDERR_LOADRESFAILURE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditCDERR_LOADRESFAILURE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditCDERR_LOADSTRFAILURE;
#define Icsdocconst_icsDocParametersEditCDERR_LOADSTRFAILURE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditCDERR_LOADSTRFAILURE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditCDERR_LOCKRESFAILURE;
#define Icsdocconst_icsDocParametersEditCDERR_LOCKRESFAILURE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditCDERR_LOCKRESFAILURE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditCDERR_MEMALLOCFAILURE;
#define Icsdocconst_icsDocParametersEditCDERR_MEMALLOCFAILURE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditCDERR_MEMALLOCFAILURE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditCDERR_MEMLOCKFAILURE;
#define Icsdocconst_icsDocParametersEditCDERR_MEMLOCKFAILURE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditCDERR_MEMLOCKFAILURE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditCDERR_NOHINSTANCE;
#define Icsdocconst_icsDocParametersEditCDERR_NOHINSTANCE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditCDERR_NOHINSTANCE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditCDERR_NOHOOK;
#define Icsdocconst_icsDocParametersEditCDERR_NOHOOK System::LoadResourceString(&Icsdocconst::_icsDocParametersEditCDERR_NOHOOK)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditCDERR_NOTEMPLATE;
#define Icsdocconst_icsDocParametersEditCDERR_NOTEMPLATE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditCDERR_NOTEMPLATE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditCDERR_REGISTERMSGFAIL;
#define Icsdocconst_icsDocParametersEditCDERR_REGISTERMSGFAIL System::LoadResourceString(&Icsdocconst::_icsDocParametersEditCDERR_REGISTERMSGFAIL)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditCDERR_STRUCTSIZE;
#define Icsdocconst_icsDocParametersEditCDERR_STRUCTSIZE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditCDERR_STRUCTSIZE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditPDERR_CREATEICFAILURE;
#define Icsdocconst_icsDocParametersEditPDERR_CREATEICFAILURE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditPDERR_CREATEICFAILURE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditPDERR_DEFAULTDIFFERENT;
#define Icsdocconst_icsDocParametersEditPDERR_DEFAULTDIFFERENT System::LoadResourceString(&Icsdocconst::_icsDocParametersEditPDERR_DEFAULTDIFFERENT)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditPDERR_DNDMMISMATCH;
#define Icsdocconst_icsDocParametersEditPDERR_DNDMMISMATCH System::LoadResourceString(&Icsdocconst::_icsDocParametersEditPDERR_DNDMMISMATCH)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditPDERR_GETDEVMODEFAIL;
#define Icsdocconst_icsDocParametersEditPDERR_GETDEVMODEFAIL System::LoadResourceString(&Icsdocconst::_icsDocParametersEditPDERR_GETDEVMODEFAIL)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditPDERR_INITFAILURE;
#define Icsdocconst_icsDocParametersEditPDERR_INITFAILURE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditPDERR_INITFAILURE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditPDERR_LOADDRVFAILURE;
#define Icsdocconst_icsDocParametersEditPDERR_LOADDRVFAILURE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditPDERR_LOADDRVFAILURE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditPDERR_NODEFAULTPRN;
#define Icsdocconst_icsDocParametersEditPDERR_NODEFAULTPRN System::LoadResourceString(&Icsdocconst::_icsDocParametersEditPDERR_NODEFAULTPRN)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditPDERR_NODEVICES;
#define Icsdocconst_icsDocParametersEditPDERR_NODEVICES System::LoadResourceString(&Icsdocconst::_icsDocParametersEditPDERR_NODEVICES)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditPDERR_PARSEFAILURE;
#define Icsdocconst_icsDocParametersEditPDERR_PARSEFAILURE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditPDERR_PARSEFAILURE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditPDERR_PRINTERNOTFOUND;
#define Icsdocconst_icsDocParametersEditPDERR_PRINTERNOTFOUND System::LoadResourceString(&Icsdocconst::_icsDocParametersEditPDERR_PRINTERNOTFOUND)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditPDERR_RETDEFFAILURE;
#define Icsdocconst_icsDocParametersEditPDERR_RETDEFFAILURE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditPDERR_RETDEFFAILURE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditPDERR_SETUPFAILURE;
#define Icsdocconst_icsDocParametersEditPDERR_SETUPFAILURE System::LoadResourceString(&Icsdocconst::_icsDocParametersEditPDERR_SETUPFAILURE)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditErrorCreateDlg;
#define Icsdocconst_icsDocParametersEditErrorCreateDlg System::LoadResourceString(&Icsdocconst::_icsDocParametersEditErrorCreateDlg)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditNewStyle;
#define Icsdocconst_icsDocParametersEditNewStyle System::LoadResourceString(&Icsdocconst::_icsDocParametersEditNewStyle)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditDeleteStyleConf;
#define Icsdocconst_icsDocParametersEditDeleteStyleConf System::LoadResourceString(&Icsdocconst::_icsDocParametersEditDeleteStyleConf)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditfmParametersEditCaption;
#define Icsdocconst_icsDocParametersEditfmParametersEditCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditfmParametersEditCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditbApplyBtnCaption;
#define Icsdocconst_icsDocParametersEditbApplyBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditbApplyBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditbCancelBtnCaption;
#define Icsdocconst_icsDocParametersEditbCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditbCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditbHelpBtnCaption;
#define Icsdocconst_icsDocParametersEditbHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditbHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEdittsCommonTSCaption;
#define Icsdocconst_icsDocParametersEdittsCommonTSCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEdittsCommonTSCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditSpecNameLabLabCaption;
#define Icsdocconst_icsDocParametersEditSpecNameLabLabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditSpecNameLabLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditLabel3LabCaption;
#define Icsdocconst_icsDocParametersEditLabel3LabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditLabel3LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditLabel4LabCaption;
#define Icsdocconst_icsDocParametersEditLabel4LabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditLabel4LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditLabel5LabCaption;
#define Icsdocconst_icsDocParametersEditLabel5LabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditLabel5LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditLabel6LabCaption;
#define Icsdocconst_icsDocParametersEditLabel6LabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditLabel6LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditlbTimeStampLabCaption;
#define Icsdocconst_icsDocParametersEditlbTimeStampLabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditlbTimeStampLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditlbAutorLabCaption;
#define Icsdocconst_icsDocParametersEditlbAutorLabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditlbAutorLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditCondRulesLbLabCaption;
#define Icsdocconst_icsDocParametersEditCondRulesLbLabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditCondRulesLbLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditDocRulesLbLabCaption;
#define Icsdocconst_icsDocParametersEditDocRulesLbLabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditDocRulesLbLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditGroupBox1GBCaption;
#define Icsdocconst_icsDocParametersEditGroupBox1GBCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditGroupBox1GBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditchbPersonalCHBCaption;
#define Icsdocconst_icsDocParametersEditchbPersonalCHBCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditchbPersonalCHBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditchbGroupCHBCaption;
#define Icsdocconst_icsDocParametersEditchbGroupCHBCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditchbGroupCHBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditPeriodDefChBxCHBCaption;
#define Icsdocconst_icsDocParametersEditPeriodDefChBxCHBCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditPeriodDefChBxCHBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditSaveBDChBxCHBCaption;
#define Icsdocconst_icsDocParametersEditSaveBDChBxCHBCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditSaveBDChBxCHBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEdittsPageSetupTSCaption;
#define Icsdocconst_icsDocParametersEdittsPageSetupTSCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEdittsPageSetupTSCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditLabel8LabCaption;
#define Icsdocconst_icsDocParametersEditLabel8LabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditLabel8LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditLabel9LabCaption;
#define Icsdocconst_icsDocParametersEditLabel9LabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditLabel9LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditLabel10LabCaption;
#define Icsdocconst_icsDocParametersEditLabel10LabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditLabel10LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditLabel11LabCaption;
#define Icsdocconst_icsDocParametersEditLabel11LabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditLabel11LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditlbOrientationLabCaption;
#define Icsdocconst_icsDocParametersEditlbOrientationLabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditlbOrientationLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditLabel13LabCaption;
#define Icsdocconst_icsDocParametersEditLabel13LabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditLabel13LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditLabel14LabCaption;
#define Icsdocconst_icsDocParametersEditLabel14LabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditLabel14LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditLabel15LabCaption;
#define Icsdocconst_icsDocParametersEditLabel15LabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditLabel15LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditLabel16LabCaption;
#define Icsdocconst_icsDocParametersEditLabel16LabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditLabel16LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditlbPageSizeLabCaption;
#define Icsdocconst_icsDocParametersEditlbPageSizeLabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditlbPageSizeLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditlbLeftMarginLabCaption;
#define Icsdocconst_icsDocParametersEditlbLeftMarginLabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditlbLeftMarginLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditlbTopMarginLabCaption;
#define Icsdocconst_icsDocParametersEditlbTopMarginLabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditlbTopMarginLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditlbRightMarginLabCaption;
#define Icsdocconst_icsDocParametersEditlbRightMarginLabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditlbRightMarginLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditlbBottomMarginLabCaption;
#define Icsdocconst_icsDocParametersEditlbBottomMarginLabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditlbBottomMarginLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditbChangePageSetupBtnCaption;
#define Icsdocconst_icsDocParametersEditbChangePageSetupBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditbChangePageSetupBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEdittsStylesTSCaption;
#define Icsdocconst_icsDocParametersEdittsStylesTSCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEdittsStylesTSCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditLabel1LabCaption;
#define Icsdocconst_icsDocParametersEditLabel1LabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditLabel1LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditLabel22LabCaption;
#define Icsdocconst_icsDocParametersEditLabel22LabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditLabel22LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditlbFontInfoLabCaption;
#define Icsdocconst_icsDocParametersEditlbFontInfoLabCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditlbFontInfoLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditchbUseBDefCHBCaption;
#define Icsdocconst_icsDocParametersEditchbUseBDefCHBCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditchbUseBDefCHBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditchbUseHDefCHBCaption;
#define Icsdocconst_icsDocParametersEditchbUseHDefCHBCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditchbUseHDefCHBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditbAddStyleBtnCaption;
#define Icsdocconst_icsDocParametersEditbAddStyleBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditbAddStyleBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditbDeleteStyleBtnCaption;
#define Icsdocconst_icsDocParametersEditbDeleteStyleBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditbDeleteStyleBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditchbWordWrapCHBCaption;
#define Icsdocconst_icsDocParametersEditchbWordWrapCHBCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditchbWordWrapCHBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditbChangeFontBtnCaption;
#define Icsdocconst_icsDocParametersEditbChangeFontBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditbChangeFontBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditButton1BtnCaption;
#define Icsdocconst_icsDocParametersEditButton1BtnCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditButton1BtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditactAddStyleActCaption;
#define Icsdocconst_icsDocParametersEditactAddStyleActCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditactAddStyleActCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditactEditStyleActCaption;
#define Icsdocconst_icsDocParametersEditactEditStyleActCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditactEditStyleActCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocParametersEditactDeleteStyleActCaption;
#define Icsdocconst_icsDocParametersEditactDeleteStyleActCaption System::LoadResourceString(&Icsdocconst::_icsDocParametersEditactDeleteStyleActCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPassportEditErrorOwnreType;
#define Icsdocconst_icsDocPassportEditErrorOwnreType System::LoadResourceString(&Icsdocconst::_icsDocPassportEditErrorOwnreType)
extern DELPHI_PACKAGE System::ResourceString _icsDocPassportEditCaption;
#define Icsdocconst_icsDocPassportEditCaption System::LoadResourceString(&Icsdocconst::_icsDocPassportEditCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPassportEditLabel1LabCaption;
#define Icsdocconst_icsDocPassportEditLabel1LabCaption System::LoadResourceString(&Icsdocconst::_icsDocPassportEditLabel1LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPassportEditLabel2LabCaption;
#define Icsdocconst_icsDocPassportEditLabel2LabCaption System::LoadResourceString(&Icsdocconst::_icsDocPassportEditLabel2LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPassportEditLabel3LabCaption;
#define Icsdocconst_icsDocPassportEditLabel3LabCaption System::LoadResourceString(&Icsdocconst::_icsDocPassportEditLabel3LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPassportEditLabel4LabCaption;
#define Icsdocconst_icsDocPassportEditLabel4LabCaption System::LoadResourceString(&Icsdocconst::_icsDocPassportEditLabel4LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPassportEditLabel5LabCaption;
#define Icsdocconst_icsDocPassportEditLabel5LabCaption System::LoadResourceString(&Icsdocconst::_icsDocPassportEditLabel5LabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPassportEditlbTimeStampLabCaption;
#define Icsdocconst_icsDocPassportEditlbTimeStampLabCaption System::LoadResourceString(&Icsdocconst::_icsDocPassportEditlbTimeStampLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPassportEditbNameAdvancedBtnCaption;
#define Icsdocconst_icsDocPassportEditbNameAdvancedBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocPassportEditbNameAdvancedBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPassportEditbApplyBtnCaption;
#define Icsdocconst_icsDocPassportEditbApplyBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocPassportEditbApplyBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPassportEditbCancelBtnCaption;
#define Icsdocconst_icsDocPassportEditbCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocPassportEditbCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPassportEditbHelpBtnCaption;
#define Icsdocconst_icsDocPassportEditbHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocPassportEditbHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditGetPeriodDefXMLStr;
#define Icsdocconst_icsDocPeriodEditGetPeriodDefXMLStr System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditGetPeriodDefXMLStr)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditSetPeriodDefXMLStr;
#define Icsdocconst_icsDocPeriodEditSetPeriodDefXMLStr System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditSetPeriodDefXMLStr)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditGetPeriodValueXMLStr;
#define Icsdocconst_icsDocPeriodEditGetPeriodValueXMLStr System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditGetPeriodValueXMLStr)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditGetPeriodValueXMLStrHalfYear;
#define Icsdocconst_icsDocPeriodEditGetPeriodValueXMLStrHalfYear System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditGetPeriodValueXMLStrHalfYear)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditGetPeriodValueXMLStrQuart;
#define Icsdocconst_icsDocPeriodEditGetPeriodValueXMLStrQuart System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditGetPeriodValueXMLStrQuart)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditSetPeriodValueXMLStrHalfYear;
#define Icsdocconst_icsDocPeriodEditSetPeriodValueXMLStrHalfYear System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditSetPeriodValueXMLStrHalfYear)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditSetPeriodValueXMLStrQuart;
#define Icsdocconst_icsDocPeriodEditSetPeriodValueXMLStrQuart System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditSetPeriodValueXMLStrQuart)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditSetPeriodValueXMLStrMonth;
#define Icsdocconst_icsDocPeriodEditSetPeriodValueXMLStrMonth System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditSetPeriodValueXMLStrMonth)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditValidateInputsBegin;
#define Icsdocconst_icsDocPeriodEditValidateInputsBegin System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditValidateInputsBegin)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditValidateInputsEnd;
#define Icsdocconst_icsDocPeriodEditValidateInputsEnd System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditValidateInputsEnd)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditValidateInputsNotEq;
#define Icsdocconst_icsDocPeriodEditValidateInputsNotEq System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditValidateInputsNotEq)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditDefRBtnClick;
#define Icsdocconst_icsDocPeriodEditDefRBtnClick System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditDefRBtnClick)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditCaption;
#define Icsdocconst_icsDocPeriodEditCaption System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditOkBtnBtnCaption;
#define Icsdocconst_icsDocPeriodEditOkBtnBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditOkBtnBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditCancelBtnBtnCaption;
#define Icsdocconst_icsDocPeriodEditCancelBtnBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditCancelBtnBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditClientGBxGBCaption;
#define Icsdocconst_icsDocPeriodEditClientGBxGBCaption System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditClientGBxGBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditHalfYearYearLbLabCaption;
#define Icsdocconst_icsDocPeriodEditHalfYearYearLbLabCaption System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditHalfYearYearLbLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditQuoterYearLbLabCaption;
#define Icsdocconst_icsDocPeriodEditQuoterYearLbLabCaption System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditQuoterYearLbLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditMonthYearLbLabCaption;
#define Icsdocconst_icsDocPeriodEditMonthYearLbLabCaption System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditMonthYearLbLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditUniqueSepLbLabCaption;
#define Icsdocconst_icsDocPeriodEditUniqueSepLbLabCaption System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditUniqueSepLbLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditYearRBtnRBCaption;
#define Icsdocconst_icsDocPeriodEditYearRBtnRBCaption System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditYearRBtnRBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditHalfYearRBtnRBCaption;
#define Icsdocconst_icsDocPeriodEditHalfYearRBtnRBCaption System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditHalfYearRBtnRBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditQuoterRBtnRBCaption;
#define Icsdocconst_icsDocPeriodEditQuoterRBtnRBCaption System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditQuoterRBtnRBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditMonthRBtnRBCaption;
#define Icsdocconst_icsDocPeriodEditMonthRBtnRBCaption System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditMonthRBtnRBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditUniqueRBtnRBCaption;
#define Icsdocconst_icsDocPeriodEditUniqueRBtnRBCaption System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditUniqueRBtnRBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditUnDefRBtnRBCaption;
#define Icsdocconst_icsDocPeriodEditUnDefRBtnRBCaption System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditUnDefRBtnRBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditHalfYearCBxItems1;
#define Icsdocconst_icsDocPeriodEditHalfYearCBxItems1 System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditHalfYearCBxItems1)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditHalfYearCBxItems2;
#define Icsdocconst_icsDocPeriodEditHalfYearCBxItems2 System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditHalfYearCBxItems2)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditQuoterCBxItems1;
#define Icsdocconst_icsDocPeriodEditQuoterCBxItems1 System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditQuoterCBxItems1)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditQuoterCBxItems2;
#define Icsdocconst_icsDocPeriodEditQuoterCBxItems2 System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditQuoterCBxItems2)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditQuoterCBxItems3;
#define Icsdocconst_icsDocPeriodEditQuoterCBxItems3 System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditQuoterCBxItems3)
extern DELPHI_PACKAGE System::ResourceString _icsDocPeriodEditQuoterCBxItems4;
#define Icsdocconst_icsDocPeriodEditQuoterCBxItems4 System::LoadResourceString(&Icsdocconst::_icsDocPeriodEditQuoterCBxItems4)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesExprEditCaption;
#define Icsdocconst_icsDocRulesExprEditCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesExprEditCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesExprEditlbTitleLabCaption;
#define Icsdocconst_icsDocRulesExprEditlbTitleLabCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesExprEditlbTitleLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesExprEditbOkBtnCaption;
#define Icsdocconst_icsDocRulesExprEditbOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesExprEditbOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesExprEditbCancelBtnCaption;
#define Icsdocconst_icsDocRulesExprEditbCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesExprEditbCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesExprEditbHelpBtnCaption;
#define Icsdocconst_icsDocRulesExprEditbHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesExprEditbHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesExprEditrtUnknown;
#define Icsdocconst_icsDocRulesExprEditrtUnknown System::LoadResourceString(&Icsdocconst::_icsDocRulesExprEditrtUnknown)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesExprEditrtCalcCaption;
#define Icsdocconst_icsDocRulesExprEditrtCalcCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesExprEditrtCalcCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesExprEditrtCalclbTitleCaption;
#define Icsdocconst_icsDocRulesExprEditrtCalclbTitleCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesExprEditrtCalclbTitleCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesExprEditrtHandCaption;
#define Icsdocconst_icsDocRulesExprEditrtHandCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesExprEditrtHandCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesExprEditrtHandlbTitleCaption;
#define Icsdocconst_icsDocRulesExprEditrtHandlbTitleCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesExprEditrtHandlbTitleCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSetRefGetObjTreeTitle;
#define Icsdocconst_icsDocSetRefGetObjTreeTitle System::LoadResourceString(&Icsdocconst::_icsDocSetRefGetObjTreeTitle)
extern DELPHI_PACKAGE System::ResourceString _icsDocSetRefSetObjTreeTitle;
#define Icsdocconst_icsDocSetRefSetObjTreeTitle System::LoadResourceString(&Icsdocconst::_icsDocSetRefSetObjTreeTitle)
extern DELPHI_PACKAGE System::ResourceString _icsDocSetRefSetRef;
#define Icsdocconst_icsDocSetRefSetRef System::LoadResourceString(&Icsdocconst::_icsDocSetRefSetRef)
extern DELPHI_PACKAGE System::ResourceString _icsDocSetRefCaption;
#define Icsdocconst_icsDocSetRefCaption System::LoadResourceString(&Icsdocconst::_icsDocSetRefCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSetRefbOkBtnCaption;
#define Icsdocconst_icsDocSetRefbOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocSetRefbOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSetRefbCancelBtnCaption;
#define Icsdocconst_icsDocSetRefbCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocSetRefbCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSetRefbHelpBtnCaption;
#define Icsdocconst_icsDocSetRefbHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocSetRefbHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocSetRefbClearBtnCaption;
#define Icsdocconst_icsDocSetRefbClearBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocSetRefbClearBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocStyleEditStyleNameCaption;
#define Icsdocconst_icsDocStyleEditStyleNameCaption System::LoadResourceString(&Icsdocconst::_icsDocStyleEditStyleNameCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocStyleEditfumADDCaption;
#define Icsdocconst_icsDocStyleEditfumADDCaption System::LoadResourceString(&Icsdocconst::_icsDocStyleEditfumADDCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocStyleEditfumEDITCaption;
#define Icsdocconst_icsDocStyleEditfumEDITCaption System::LoadResourceString(&Icsdocconst::_icsDocStyleEditfumEDITCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocStyleEditbOkBtnCaption;
#define Icsdocconst_icsDocStyleEditbOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocStyleEditbOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocStyleEditbCancelBtnCaption;
#define Icsdocconst_icsDocStyleEditbCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocStyleEditbCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocStyleEditbHelpBtnCaption;
#define Icsdocconst_icsDocStyleEditbHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocStyleEditbHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocStyleEditStyleNameLabLabCaption;
#define Icsdocconst_icsDocStyleEditStyleNameLabLabCaption System::LoadResourceString(&Icsdocconst::_icsDocStyleEditStyleNameLabLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListErrorInvalidArgument;
#define Icsdocconst_icsDocAddListErrorInvalidArgument System::LoadResourceString(&Icsdocconst::_icsDocAddListErrorInvalidArgument)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListErrorSetMainIS;
#define Icsdocconst_icsDocAddListErrorSetMainIS System::LoadResourceString(&Icsdocconst::_icsDocAddListErrorSetMainIS)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListErrorAddCol;
#define Icsdocconst_icsDocAddListErrorAddCol System::LoadResourceString(&Icsdocconst::_icsDocAddListErrorAddCol)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListErrorAddCols;
#define Icsdocconst_icsDocAddListErrorAddCols System::LoadResourceString(&Icsdocconst::_icsDocAddListErrorAddCols)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListCaption;
#define Icsdocconst_icsDocAddListCaption System::LoadResourceString(&Icsdocconst::_icsDocAddListCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListColGroupCaption;
#define Icsdocconst_icsDocAddListColGroupCaption System::LoadResourceString(&Icsdocconst::_icsDocAddListColGroupCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListISListTitle;
#define Icsdocconst_icsDocAddListISListTitle System::LoadResourceString(&Icsdocconst::_icsDocAddListISListTitle)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListISTreeTitle;
#define Icsdocconst_icsDocAddListISTreeTitle System::LoadResourceString(&Icsdocconst::_icsDocAddListISTreeTitle)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListColsTitle;
#define Icsdocconst_icsDocAddListColsTitle System::LoadResourceString(&Icsdocconst::_icsDocAddListColsTitle)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListAddColsCaption;
#define Icsdocconst_icsDocAddListAddColsCaption System::LoadResourceString(&Icsdocconst::_icsDocAddListAddColsCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListAddColsHint;
#define Icsdocconst_icsDocAddListAddColsHint System::LoadResourceString(&Icsdocconst::_icsDocAddListAddColsHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListAddGColsCaption;
#define Icsdocconst_icsDocAddListAddGColsCaption System::LoadResourceString(&Icsdocconst::_icsDocAddListAddGColsCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListAddGColsHint;
#define Icsdocconst_icsDocAddListAddGColsHint System::LoadResourceString(&Icsdocconst::_icsDocAddListAddGColsHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListDeleteCaption;
#define Icsdocconst_icsDocAddListDeleteCaption System::LoadResourceString(&Icsdocconst::_icsDocAddListDeleteCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListDeleteHint;
#define Icsdocconst_icsDocAddListDeleteHint System::LoadResourceString(&Icsdocconst::_icsDocAddListDeleteHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListClearCaption;
#define Icsdocconst_icsDocAddListClearCaption System::LoadResourceString(&Icsdocconst::_icsDocAddListClearCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListClearHint;
#define Icsdocconst_icsDocAddListClearHint System::LoadResourceString(&Icsdocconst::_icsDocAddListClearHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListUpCaption;
#define Icsdocconst_icsDocAddListUpCaption System::LoadResourceString(&Icsdocconst::_icsDocAddListUpCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListUpHint;
#define Icsdocconst_icsDocAddListUpHint System::LoadResourceString(&Icsdocconst::_icsDocAddListUpHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListDownCaption;
#define Icsdocconst_icsDocAddListDownCaption System::LoadResourceString(&Icsdocconst::_icsDocAddListDownCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListDownHint;
#define Icsdocconst_icsDocAddListDownHint System::LoadResourceString(&Icsdocconst::_icsDocAddListDownHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListOkBtnCaption;
#define Icsdocconst_icsDocAddListOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocAddListOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListCancelBtnCaption;
#define Icsdocconst_icsDocAddListCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocAddListCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddListHelpBtnCaption;
#define Icsdocconst_icsDocAddListHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocAddListHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddTableListSrcLabCaption;
#define Icsdocconst_icsDocAddTableListSrcLabCaption System::LoadResourceString(&Icsdocconst::_icsDocAddTableListSrcLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddTableColCountLabCaption;
#define Icsdocconst_icsDocAddTableColCountLabCaption System::LoadResourceString(&Icsdocconst::_icsDocAddTableColCountLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddTableRowCountLabCaption;
#define Icsdocconst_icsDocAddTableRowCountLabCaption System::LoadResourceString(&Icsdocconst::_icsDocAddTableRowCountLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddTableOkBtnCaption;
#define Icsdocconst_icsDocAddTableOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocAddTableOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddTableCancelBtnCaption;
#define Icsdocconst_icsDocAddTableCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocAddTableCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocAddTableHelpBtnCaption;
#define Icsdocconst_icsDocAddTableHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocAddTableHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocCommentEditCommentLabCaption;
#define Icsdocconst_icsDocCommentEditCommentLabCaption System::LoadResourceString(&Icsdocconst::_icsDocCommentEditCommentLabCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocCommentEditOkBtnCaption;
#define Icsdocconst_icsDocCommentEditOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocCommentEditOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocCommentEditCancelBtnCaption;
#define Icsdocconst_icsDocCommentEditCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocCommentEditCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocCommentEditHelpBtnCaption;
#define Icsdocconst_icsDocCommentEditHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocCommentEditHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocCountEvtCaption;
#define Icsdocconst_icsDocCountEvtCaption System::LoadResourceString(&Icsdocconst::_icsDocCountEvtCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocCountCountCaption;
#define Icsdocconst_icsDocCountCountCaption System::LoadResourceString(&Icsdocconst::_icsDocCountCountCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocCountFilterRulesCaption;
#define Icsdocconst_icsDocCountFilterRulesCaption System::LoadResourceString(&Icsdocconst::_icsDocCountFilterRulesCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocCountOkBtnCaption;
#define Icsdocconst_icsDocCountOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocCountOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocCountCancelBtnCaption;
#define Icsdocconst_icsDocCountCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocCountCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocCountHelpBtnCaption;
#define Icsdocconst_icsDocCountHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocCountHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocCountClearBtnCaption;
#define Icsdocconst_icsDocCountClearBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocCountClearBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditAreInputsCorrectMsg;
#define Icsdocconst_icsDocElSizeEditAreInputsCorrectMsg System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditAreInputsCorrectMsg)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditValHeightCaption;
#define Icsdocconst_icsDocElSizeEditValHeightCaption System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditValHeightCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditHeightCaption;
#define Icsdocconst_icsDocElSizeEditHeightCaption System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditHeightCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditFullHeightCaption;
#define Icsdocconst_icsDocElSizeEditFullHeightCaption System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditFullHeightCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditValWidthCaption;
#define Icsdocconst_icsDocElSizeEditValWidthCaption System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditValWidthCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditWidthCaption;
#define Icsdocconst_icsDocElSizeEditWidthCaption System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditWidthCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditFullWidthCaption;
#define Icsdocconst_icsDocElSizeEditFullWidthCaption System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditFullWidthCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditErrorSizeType;
#define Icsdocconst_icsDocElSizeEditErrorSizeType System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditErrorSizeType)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditErrorUnitsType;
#define Icsdocconst_icsDocElSizeEditErrorUnitsType System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditErrorUnitsType)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditErrorSizeVal;
#define Icsdocconst_icsDocElSizeEditErrorSizeVal System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditErrorSizeVal)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditErrorMaxSizeVal;
#define Icsdocconst_icsDocElSizeEditErrorMaxSizeVal System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditErrorMaxSizeVal)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditErrorSizeValRange;
#define Icsdocconst_icsDocElSizeEditErrorSizeValRange System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditErrorSizeValRange)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditErrorValValidate;
#define Icsdocconst_icsDocElSizeEditErrorValValidate System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditErrorValValidate)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditAutoSizeCaption;
#define Icsdocconst_icsDocElSizeEditAutoSizeCaption System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditAutoSizeCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditValueCaption;
#define Icsdocconst_icsDocElSizeEditValueCaption System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditValueCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditUnitsCaption;
#define Icsdocconst_icsDocElSizeEditUnitsCaption System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditUnitsCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditOkBtnCaption;
#define Icsdocconst_icsDocElSizeEditOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditCancelBtnCaption;
#define Icsdocconst_icsDocElSizeEditCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocElSizeEditHelpBtnCaption;
#define Icsdocconst_icsDocElSizeEditHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocElSizeEditHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocMatrixErrorParam;
#define Icsdocconst_icsDocMatrixErrorParam System::LoadResourceString(&Icsdocconst::_icsDocMatrixErrorParam)
extern DELPHI_PACKAGE System::ResourceString _icsDocMatrixErrorIndex;
#define Icsdocconst_icsDocMatrixErrorIndex System::LoadResourceString(&Icsdocconst::_icsDocMatrixErrorIndex)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditErrorRow;
#define Icsdocconst_icsDocHandEditErrorRow System::LoadResourceString(&Icsdocconst::_icsDocHandEditErrorRow)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditErrorCol;
#define Icsdocconst_icsDocHandEditErrorCol System::LoadResourceString(&Icsdocconst::_icsDocHandEditErrorCol)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditCheckRulesMsg;
#define Icsdocconst_icsDocHandEditCheckRulesMsg System::LoadResourceString(&Icsdocconst::_icsDocHandEditCheckRulesMsg)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditCheckRulesCpt;
#define Icsdocconst_icsDocHandEditCheckRulesCpt System::LoadResourceString(&Icsdocconst::_icsDocHandEditCheckRulesCpt)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditStructHeaderCaption;
#define Icsdocconst_icsDocHandEditStructHeaderCaption System::LoadResourceString(&Icsdocconst::_icsDocHandEditStructHeaderCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditmiDocCaption;
#define Icsdocconst_icsDocHandEditmiDocCaption System::LoadResourceString(&Icsdocconst::_icsDocHandEditmiDocCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditmiViewCaption;
#define Icsdocconst_icsDocHandEditmiViewCaption System::LoadResourceString(&Icsdocconst::_icsDocHandEditmiViewCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditmiJumpCaption;
#define Icsdocconst_icsDocHandEditmiJumpCaption System::LoadResourceString(&Icsdocconst::_icsDocHandEditmiJumpCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditSaveCaption;
#define Icsdocconst_icsDocHandEditSaveCaption System::LoadResourceString(&Icsdocconst::_icsDocHandEditSaveCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditSaveHint;
#define Icsdocconst_icsDocHandEditSaveHint System::LoadResourceString(&Icsdocconst::_icsDocHandEditSaveHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditReCalcCaption;
#define Icsdocconst_icsDocHandEditReCalcCaption System::LoadResourceString(&Icsdocconst::_icsDocHandEditReCalcCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditReCalcHint;
#define Icsdocconst_icsDocHandEditReCalcHint System::LoadResourceString(&Icsdocconst::_icsDocHandEditReCalcHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditFirstDocElementCaption;
#define Icsdocconst_icsDocHandEditFirstDocElementCaption System::LoadResourceString(&Icsdocconst::_icsDocHandEditFirstDocElementCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditFirstDocElementHint;
#define Icsdocconst_icsDocHandEditFirstDocElementHint System::LoadResourceString(&Icsdocconst::_icsDocHandEditFirstDocElementHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditPrevDocElementCaption;
#define Icsdocconst_icsDocHandEditPrevDocElementCaption System::LoadResourceString(&Icsdocconst::_icsDocHandEditPrevDocElementCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditPrevDocElementHint;
#define Icsdocconst_icsDocHandEditPrevDocElementHint System::LoadResourceString(&Icsdocconst::_icsDocHandEditPrevDocElementHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditNextDocElementCaption;
#define Icsdocconst_icsDocHandEditNextDocElementCaption System::LoadResourceString(&Icsdocconst::_icsDocHandEditNextDocElementCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditNextDocElementHint;
#define Icsdocconst_icsDocHandEditNextDocElementHint System::LoadResourceString(&Icsdocconst::_icsDocHandEditNextDocElementHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditLastDocElementCaption;
#define Icsdocconst_icsDocHandEditLastDocElementCaption System::LoadResourceString(&Icsdocconst::_icsDocHandEditLastDocElementCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditLastDocElementHint;
#define Icsdocconst_icsDocHandEditLastDocElementHint System::LoadResourceString(&Icsdocconst::_icsDocHandEditLastDocElementHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditViewStructCaption;
#define Icsdocconst_icsDocHandEditViewStructCaption System::LoadResourceString(&Icsdocconst::_icsDocHandEditViewStructCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditViewStructHint;
#define Icsdocconst_icsDocHandEditViewStructHint System::LoadResourceString(&Icsdocconst::_icsDocHandEditViewStructHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditViewDocElementInscCaption;
#define Icsdocconst_icsDocHandEditViewDocElementInscCaption System::LoadResourceString(&Icsdocconst::_icsDocHandEditViewDocElementInscCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditViewDocElementInscHint;
#define Icsdocconst_icsDocHandEditViewDocElementInscHint System::LoadResourceString(&Icsdocconst::_icsDocHandEditViewDocElementInscHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditExitCaption;
#define Icsdocconst_icsDocHandEditExitCaption System::LoadResourceString(&Icsdocconst::_icsDocHandEditExitCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHandEditExitHint;
#define Icsdocconst_icsDocHandEditExitHint System::LoadResourceString(&Icsdocconst::_icsDocHandEditExitHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditVersionLimit;
#define Icsdocconst_icsDocRulesEditVersionLimit System::LoadResourceString(&Icsdocconst::_icsDocRulesEditVersionLimit)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditAccessDeny;
#define Icsdocconst_icsDocRulesEditAccessDeny System::LoadResourceString(&Icsdocconst::_icsDocRulesEditAccessDeny)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditErrorEmptyDoc;
#define Icsdocconst_icsDocRulesEditErrorEmptyDoc System::LoadResourceString(&Icsdocconst::_icsDocRulesEditErrorEmptyDoc)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditConfCol;
#define Icsdocconst_icsDocRulesEditConfCol System::LoadResourceString(&Icsdocconst::_icsDocRulesEditConfCol)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditConfRow;
#define Icsdocconst_icsDocRulesEditConfRow System::LoadResourceString(&Icsdocconst::_icsDocRulesEditConfRow)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditErrorConf;
#define Icsdocconst_icsDocRulesEditErrorConf System::LoadResourceString(&Icsdocconst::_icsDocRulesEditErrorConf)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditErrorIntDefDoc;
#define Icsdocconst_icsDocRulesEditErrorIntDefDoc System::LoadResourceString(&Icsdocconst::_icsDocRulesEditErrorIntDefDoc)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditErrorPeriodDef;
#define Icsdocconst_icsDocRulesEditErrorPeriodDef System::LoadResourceString(&Icsdocconst::_icsDocRulesEditErrorPeriodDef)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditErrorOrgDef;
#define Icsdocconst_icsDocRulesEditErrorOrgDef System::LoadResourceString(&Icsdocconst::_icsDocRulesEditErrorOrgDef)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditDefChangeConf;
#define Icsdocconst_icsDocRulesEditDefChangeConf System::LoadResourceString(&Icsdocconst::_icsDocRulesEditDefChangeConf)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditCaption;
#define Icsdocconst_icsDocRulesEditCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesEditCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditCreateStep;
#define Icsdocconst_icsDocRulesEditCreateStep System::LoadResourceString(&Icsdocconst::_icsDocRulesEditCreateStep)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditStepParam;
#define Icsdocconst_icsDocRulesEditStepParam System::LoadResourceString(&Icsdocconst::_icsDocRulesEditStepParam)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditStepObjects;
#define Icsdocconst_icsDocRulesEditStepObjects System::LoadResourceString(&Icsdocconst::_icsDocRulesEditStepObjects)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditIntDocCaption;
#define Icsdocconst_icsDocRulesEditIntDocCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesEditIntDocCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditIntDocDef;
#define Icsdocconst_icsDocRulesEditIntDocDef System::LoadResourceString(&Icsdocconst::_icsDocRulesEditIntDocDef)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditIntDocPerType;
#define Icsdocconst_icsDocRulesEditIntDocPerType System::LoadResourceString(&Icsdocconst::_icsDocRulesEditIntDocPerType)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditIntDocPeriod;
#define Icsdocconst_icsDocRulesEditIntDocPeriod System::LoadResourceString(&Icsdocconst::_icsDocRulesEditIntDocPeriod)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditIntDocPeriodisity;
#define Icsdocconst_icsDocRulesEditIntDocPeriodisity System::LoadResourceString(&Icsdocconst::_icsDocRulesEditIntDocPeriodisity)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditOrg;
#define Icsdocconst_icsDocRulesEditOrg System::LoadResourceString(&Icsdocconst::_icsDocRulesEditOrg)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditOrgType;
#define Icsdocconst_icsDocRulesEditOrgType System::LoadResourceString(&Icsdocconst::_icsDocRulesEditOrgType)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditOrgName;
#define Icsdocconst_icsDocRulesEditOrgName System::LoadResourceString(&Icsdocconst::_icsDocRulesEditOrgName)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditAddStepCaption;
#define Icsdocconst_icsDocRulesEditAddStepCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesEditAddStepCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditAddStepHint;
#define Icsdocconst_icsDocRulesEditAddStepHint System::LoadResourceString(&Icsdocconst::_icsDocRulesEditAddStepHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditMoveStepUpCaption;
#define Icsdocconst_icsDocRulesEditMoveStepUpCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesEditMoveStepUpCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditMoveStepUpHint;
#define Icsdocconst_icsDocRulesEditMoveStepUpHint System::LoadResourceString(&Icsdocconst::_icsDocRulesEditMoveStepUpHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditMoveStepDownCaption;
#define Icsdocconst_icsDocRulesEditMoveStepDownCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesEditMoveStepDownCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditMoveStepDownHint;
#define Icsdocconst_icsDocRulesEditMoveStepDownHint System::LoadResourceString(&Icsdocconst::_icsDocRulesEditMoveStepDownHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditDeleteCaption;
#define Icsdocconst_icsDocRulesEditDeleteCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesEditDeleteCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditDeleteHint;
#define Icsdocconst_icsDocRulesEditDeleteHint System::LoadResourceString(&Icsdocconst::_icsDocRulesEditDeleteHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditAddObjRulesCaption;
#define Icsdocconst_icsDocRulesEditAddObjRulesCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesEditAddObjRulesCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditAddObjRulesHint;
#define Icsdocconst_icsDocRulesEditAddObjRulesHint System::LoadResourceString(&Icsdocconst::_icsDocRulesEditAddObjRulesHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditSetAssocCaption;
#define Icsdocconst_icsDocRulesEditSetAssocCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesEditSetAssocCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditSetAssocHint;
#define Icsdocconst_icsDocRulesEditSetAssocHint System::LoadResourceString(&Icsdocconst::_icsDocRulesEditSetAssocHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditEditPropCaption;
#define Icsdocconst_icsDocRulesEditEditPropCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesEditEditPropCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditEditPropHint;
#define Icsdocconst_icsDocRulesEditEditPropHint System::LoadResourceString(&Icsdocconst::_icsDocRulesEditEditPropHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditOkBtnCaption;
#define Icsdocconst_icsDocRulesEditOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesEditOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditCancelBtnCaption;
#define Icsdocconst_icsDocRulesEditCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesEditCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocRulesEditHelpBtnCaption;
#define Icsdocconst_icsDocRulesEditHelpBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocRulesEditHelpBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocDurationEditCaption;
#define Icsdocconst_icsDocDurationEditCaption System::LoadResourceString(&Icsdocconst::_icsDocDurationEditCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocDurationEditTitle;
#define Icsdocconst_icsDocDurationEditTitle System::LoadResourceString(&Icsdocconst::_icsDocDurationEditTitle)
extern DELPHI_PACKAGE System::ResourceString _icsDocDurationEditOkBtnCaption;
#define Icsdocconst_icsDocDurationEditOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocDurationEditOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocDurationEditCancelBtnCaption;
#define Icsdocconst_icsDocDurationEditCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocDurationEditCancelBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocDurationEditError;
#define Icsdocconst_icsDocDurationEditError System::LoadResourceString(&Icsdocconst::_icsDocDurationEditError)
extern DELPHI_PACKAGE System::ResourceString _icsDocHTMLPreviewMsgCaption;
#define Icsdocconst_icsDocHTMLPreviewMsgCaption System::LoadResourceString(&Icsdocconst::_icsDocHTMLPreviewMsgCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHTMLPreviewErrorRewrite;
#define Icsdocconst_icsDocHTMLPreviewErrorRewrite System::LoadResourceString(&Icsdocconst::_icsDocHTMLPreviewErrorRewrite)
extern DELPHI_PACKAGE System::ResourceString _icsDocHTMLPreviewCaption;
#define Icsdocconst_icsDocHTMLPreviewCaption System::LoadResourceString(&Icsdocconst::_icsDocHTMLPreviewCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHTMLPreviewSaveDialogFilter;
#define Icsdocconst_icsDocHTMLPreviewSaveDialogFilter System::LoadResourceString(&Icsdocconst::_icsDocHTMLPreviewSaveDialogFilter)
extern DELPHI_PACKAGE System::ResourceString _icsDocHTMLPreviewSaveAsCaption;
#define Icsdocconst_icsDocHTMLPreviewSaveAsCaption System::LoadResourceString(&Icsdocconst::_icsDocHTMLPreviewSaveAsCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHTMLPreviewSaveAsHint;
#define Icsdocconst_icsDocHTMLPreviewSaveAsHint System::LoadResourceString(&Icsdocconst::_icsDocHTMLPreviewSaveAsHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocHTMLPreviewPrintPreviewCaption;
#define Icsdocconst_icsDocHTMLPreviewPrintPreviewCaption System::LoadResourceString(&Icsdocconst::_icsDocHTMLPreviewPrintPreviewCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHTMLPreviewPrintPreviewHint;
#define Icsdocconst_icsDocHTMLPreviewPrintPreviewHint System::LoadResourceString(&Icsdocconst::_icsDocHTMLPreviewPrintPreviewHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocHTMLPreviewPrintCaption;
#define Icsdocconst_icsDocHTMLPreviewPrintCaption System::LoadResourceString(&Icsdocconst::_icsDocHTMLPreviewPrintCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHTMLPreviewPrintHint;
#define Icsdocconst_icsDocHTMLPreviewPrintHint System::LoadResourceString(&Icsdocconst::_icsDocHTMLPreviewPrintHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocHTMLPreviewCloseCaption;
#define Icsdocconst_icsDocHTMLPreviewCloseCaption System::LoadResourceString(&Icsdocconst::_icsDocHTMLPreviewCloseCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocHTMLPreviewCloseHint;
#define Icsdocconst_icsDocHTMLPreviewCloseHint System::LoadResourceString(&Icsdocconst::_icsDocHTMLPreviewCloseHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewCommPageCount;
#define Icsdocconst_icsDocPreviewCommPageCount System::LoadResourceString(&Icsdocconst::_icsDocPreviewCommPageCount)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewZoomHeight;
#define Icsdocconst_icsDocPreviewZoomHeight System::LoadResourceString(&Icsdocconst::_icsDocPreviewZoomHeight)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewZoomWidth;
#define Icsdocconst_icsDocPreviewZoomWidth System::LoadResourceString(&Icsdocconst::_icsDocPreviewZoomWidth)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewZoomCustom;
#define Icsdocconst_icsDocPreviewZoomCustom System::LoadResourceString(&Icsdocconst::_icsDocPreviewZoomCustom)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactPrintCaption;
#define Icsdocconst_icsDocPreviewactPrintCaption System::LoadResourceString(&Icsdocconst::_icsDocPreviewactPrintCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactPrintHint;
#define Icsdocconst_icsDocPreviewactPrintHint System::LoadResourceString(&Icsdocconst::_icsDocPreviewactPrintHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactZoomHeightCaption;
#define Icsdocconst_icsDocPreviewactZoomHeightCaption System::LoadResourceString(&Icsdocconst::_icsDocPreviewactZoomHeightCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactZoomHeightHint;
#define Icsdocconst_icsDocPreviewactZoomHeightHint System::LoadResourceString(&Icsdocconst::_icsDocPreviewactZoomHeightHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactZoomWidthCaption;
#define Icsdocconst_icsDocPreviewactZoomWidthCaption System::LoadResourceString(&Icsdocconst::_icsDocPreviewactZoomWidthCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactZoomWidthHint;
#define Icsdocconst_icsDocPreviewactZoomWidthHint System::LoadResourceString(&Icsdocconst::_icsDocPreviewactZoomWidthHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactSet1PageCaption;
#define Icsdocconst_icsDocPreviewactSet1PageCaption System::LoadResourceString(&Icsdocconst::_icsDocPreviewactSet1PageCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactSet1PageHint;
#define Icsdocconst_icsDocPreviewactSet1PageHint System::LoadResourceString(&Icsdocconst::_icsDocPreviewactSet1PageHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactSet2PageCaption;
#define Icsdocconst_icsDocPreviewactSet2PageCaption System::LoadResourceString(&Icsdocconst::_icsDocPreviewactSet2PageCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactSet2PageHint;
#define Icsdocconst_icsDocPreviewactSet2PageHint System::LoadResourceString(&Icsdocconst::_icsDocPreviewactSet2PageHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactPrintSettingCaption;
#define Icsdocconst_icsDocPreviewactPrintSettingCaption System::LoadResourceString(&Icsdocconst::_icsDocPreviewactPrintSettingCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactPrintSettingHint;
#define Icsdocconst_icsDocPreviewactPrintSettingHint System::LoadResourceString(&Icsdocconst::_icsDocPreviewactPrintSettingHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactSaveCaption;
#define Icsdocconst_icsDocPreviewactSaveCaption System::LoadResourceString(&Icsdocconst::_icsDocPreviewactSaveCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactSaveHint;
#define Icsdocconst_icsDocPreviewactSaveHint System::LoadResourceString(&Icsdocconst::_icsDocPreviewactSaveHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactExcelCaption;
#define Icsdocconst_icsDocPreviewactExcelCaption System::LoadResourceString(&Icsdocconst::_icsDocPreviewactExcelCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewactExcelHint;
#define Icsdocconst_icsDocPreviewactExcelHint System::LoadResourceString(&Icsdocconst::_icsDocPreviewactExcelHint)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewSaveDlgFilter;
#define Icsdocconst_icsDocPreviewSaveDlgFilter System::LoadResourceString(&Icsdocconst::_icsDocPreviewSaveDlgFilter)
extern DELPHI_PACKAGE System::ResourceString _icsDocPreviewSaveDlgTitle;
#define Icsdocconst_icsDocPreviewSaveDlgTitle System::LoadResourceString(&Icsdocconst::_icsDocPreviewSaveDlgTitle)
extern DELPHI_PACKAGE System::ResourceString _icsDocPrnStatusStatusLabelCaption1;
#define Icsdocconst_icsDocPrnStatusStatusLabelCaption1 System::LoadResourceString(&Icsdocconst::_icsDocPrnStatusStatusLabelCaption1)
extern DELPHI_PACKAGE System::ResourceString _icsDocPrnStatusStatusLabelCaption2;
#define Icsdocconst_icsDocPrnStatusStatusLabelCaption2 System::LoadResourceString(&Icsdocconst::_icsDocPrnStatusStatusLabelCaption2)
extern DELPHI_PACKAGE System::ResourceString _icsDocPrnStatusCancelButtonCaption;
#define Icsdocconst_icsDocPrnStatusCancelButtonCaption System::LoadResourceString(&Icsdocconst::_icsDocPrnStatusCancelButtonCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPagePrintKoeff;
#define Icsdocconst_icsDocPagePrintKoeff System::LoadResourceString(&Icsdocconst::_icsDocPagePrintKoeff)
extern DELPHI_PACKAGE System::ResourceString _icsDocPageSetupCaption;
#define Icsdocconst_icsDocPageSetupCaption System::LoadResourceString(&Icsdocconst::_icsDocPageSetupCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPageSetupOrientationGBCaption;
#define Icsdocconst_icsDocPageSetupOrientationGBCaption System::LoadResourceString(&Icsdocconst::_icsDocPageSetupOrientationGBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPageSetupportRBCaption;
#define Icsdocconst_icsDocPageSetupportRBCaption System::LoadResourceString(&Icsdocconst::_icsDocPageSetupportRBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPageSetuplandRBCaption;
#define Icsdocconst_icsDocPageSetuplandRBCaption System::LoadResourceString(&Icsdocconst::_icsDocPageSetuplandRBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPageSetupFieldsGBCaption;
#define Icsdocconst_icsDocPageSetupFieldsGBCaption System::LoadResourceString(&Icsdocconst::_icsDocPageSetupFieldsGBCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPageSetupOkBtnCaption;
#define Icsdocconst_icsDocPageSetupOkBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocPageSetupOkBtnCaption)
extern DELPHI_PACKAGE System::ResourceString _icsDocPageSetupCancelBtnCaption;
#define Icsdocconst_icsDocPageSetupCancelBtnCaption System::LoadResourceString(&Icsdocconst::_icsDocPageSetupCancelBtnCaption)
}	/* namespace Icsdocconst */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ICSDOCCONST)
using namespace Icsdocconst;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// IcsdocconstHPP
