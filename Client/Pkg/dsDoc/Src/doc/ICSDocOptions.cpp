/*
  ����        - ICSDocOptions.cpp
  ������      - ��������������� (ICS)
  ��������    - ���� ���������� ������, ����������� ��������
                ���������� �����, ��������, ���������� � �������
  ����������� - ������� �.�.
  ����        - 01.12.2004
*/
//---------------------------------------------------------------------------

#pragma hdrstop

#include "icsDocConstDef.h"
#include "ICSDocOptions.h"

#include "DKFileUtils.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------

UnicodeString __fastcall ICSDocOptions::ICSDocVer()
{
  wchar_t szModuleFileName[_MAX_PATH + 1];
  GetModuleFileName( HInstance, szModuleFileName, _MAX_PATH + 1 );
  return GetFileVersionInfoValue( szModuleFileName, "ProductVersion" );
}

//---------------------------------------------------------------------------

namespace ICSDocOptions
{

int __fastcall ICSDocVerPart( const int nInd )
{
  int nVerPartVal = -1;
  UnicodeString ProductVersion = ICSDocVer();
  wchar_t *szProductVersion = new wchar_t[ ProductVersion.Length() + 1 ];
  szProductVersion[0] = '\0';
  wcscpy( szProductVersion, ProductVersion.c_str() );
  wchar_t *p = wcstok( szProductVersion, L".");
  int i = 0;
  while( p )
  {
    if ( i == nInd )
    {
      nVerPartVal = _wtoi(p);
      break;
    }
    p = wcstok( NULL, L"." );
    i++;
  }
  delete [] szProductVersion;
  return nVerPartVal;
}

} //end of namespace ICSDocOptions

//---------------------------------------------------------------------------

int __fastcall ICSDocOptions::ICSDocVerMaj()
{
  return ICSDocVerPart( 0 );
}

//---------------------------------------------------------------------------

int __fastcall ICSDocOptions::ICSDocVerMin()
{
  return ICSDocVerPart( 1 );
}

//---------------------------------------------------------------------------

int __fastcall ICSDocOptions::ICSDocRelease()
{
  return ICSDocVerPart( 2 );
}

//---------------------------------------------------------------------------

