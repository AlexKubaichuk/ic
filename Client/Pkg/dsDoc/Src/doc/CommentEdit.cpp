/*
  ����        - CommentEdit.cpp
  ������      - ��������������� (ICS)
  ��������    - ���� ���������� ������, ����������� ��������
                ����� "�������������� �����������"
                ( ������������ ����������� TICSDocFilter )
  ����������� - ������� �.�.
  ����        - 01.09.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "icsDocConstDef.h"
#include "CommentEdit.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxTextEdit"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma resource "*.dfm"

//***************************************************************************
//********************** ���������� ������� *********************************
//***************************************************************************

int CommentEdit::ShowModalCommentEdit( TComponent* Owner, UnicodeString& AComment )
{
  int nModalResult = mrNone;
  TfmCommentEdit *dlg;
  try
  {
    dlg = new TfmCommentEdit(Owner, AComment );
    if (dlg)
      if ( ( nModalResult = dlg->ShowModal() ) == mrOk )
        AComment = dlg->Comment;
  }
  __finally
  {
    if (dlg) delete dlg;
  }
  return nModalResult;
}

//***************************************************************************
//********************** ��������/��������/����� ����� **********************
//***************************************************************************

__fastcall TfmCommentEdit::TfmCommentEdit(TComponent* Owner, UnicodeString  AComment )
        : TForm(Owner)
{
  CommentLab->Caption = FMT(icsDocCommentEditCommentLabCaption);
  bOk->Caption        = FMT(icsDocCommentEditOkBtnCaption);
  bCancel->Caption    = FMT(icsDocCommentEditCancelBtnCaption);
  bHelp->Caption      = FMT(icsDocCommentEditHelpBtnCaption);
  Comment = AComment;
}

//---------------------------------------------------------------------------

void __fastcall TfmCommentEdit::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif
}

//***************************************************************************
//********************** ������ ����� ***************************************
//***************************************************************************

void __fastcall TfmCommentEdit::SetComment( UnicodeString  AValue )
{
  edComment->Text = FComment = AValue;
}

//---------------------------------------------------------------------------

bool __fastcall TfmCommentEdit::AreInputsCorrect()
{
        //---- ����� ���������� ��������� ���� �������� ----

  return true;
}

//***************************************************************************
//********************** ����������� �������*********************************
//***************************************************************************

void __fastcall TfmCommentEdit::bOkClick(TObject *Sender)
{
//  if ( !CheckDKFilterControls( Sender ) )//?!!!
//    return;

  if ( !AreInputsCorrect() )
    return;

  FComment = Trim( edComment->Text );
  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TfmCommentEdit::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TfmCommentEdit::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}

//---------------------------------------------------------------------------


