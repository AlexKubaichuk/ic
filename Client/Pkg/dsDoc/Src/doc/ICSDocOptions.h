/*
  ����        - ICSDocOptions.h
  ������      - ��������������� (ICS)
  ��������    - ������������ ���� ������, ����������� ��������
                ���������� �����, ��������, ���������� � �������
  ����������� - ������� �.�.
  ����        - 01.12.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

        04.04.2005
                1. ��������� ICSDocVerMaj, ICSDocVerMin, ICSDocRelease ��������
                   ������������ ���������
                2. ��������� ������� ICSDocVer

        01.12.2004
                �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef ICSDocOptionsH
#define ICSDocOptionsH

#include "ICSDoc.h"	//��������� �������� � ���� ��������

#include <Classes.hpp>

namespace ICSDocOptions
{

//---------------------------------------------------------------------------

extern PACKAGE int __fastcall ICSDocVerMaj();
extern PACKAGE int __fastcall ICSDocVerMin();
extern PACKAGE int __fastcall ICSDocRelease();
extern PACKAGE UnicodeString __fastcall ICSDocVer();

//---------------------------------------------------------------------------

} //end of namespace ICSDocOptions
using namespace ICSDocOptions;

#endif
