unit icsDocConst;

interface

resourcestring
// Common
  icsDocCaption = '������';
  icsDocErrorTagEq = '�������� ������ ��������������� ���� "%s".';
  icsDocErrorXMLContainerNotDefined = '�� ������ xml-���������.';
  icsDocErrorDocNotDefined = '��������� ������� ��������.';
  icsDocErrorGUINotValid = '��� ������������� ���������� ��������� �� ��������� � ��� ���������� �������������';
  icsDocErrorCommonDop = '��� ������ - %s,'+#13#10+'��������� - "%s".';
  icsDocErrorCreateOwnerNotValid   = '�������� ������ ���� �������� TICSDocSpec.';
  icsDocErrorInputErrorCaption  = '������ �����';
  icsDocErrorReqFieldCaption  = '���� "%s" ������ ���� ���������.';

// BaseEdit
  icsDocBaseEditCommonTBCaption = '�����';
  icsDocBaseEditEditTBCaption = '������';
  icsDocBaseEditErrorSetDomainNum = '����� ������ ������ ���������� � %s.';
  icsDocBaseEditErrorGetNom = '�� ������ ����������� (GUI = %s).';
  icsDocBaseEditErrorGetDomain = '����� � ������� "%s" � ������������ �� ������.';
  icsDocBaseEditErrorNoXML = '����������� xml.';
  icsDocBaseEditErrorGenUID = '��������� uid ����������. �� ����������� �� ���� �� ��������� �������: ParentXML, XML.';

// Globals
  icsDocGlobals = '';

//Filter
  icsDocFilterCaption = '�������� �������';
  icsDocFilterFilterTBCaption = '������';

  icsDocFilterPassportCaption       = '&���������';
  icsDocFilterPassportHint          = '���������';
  icsDocFilterCustomLoadSaveCaption = '&��������� / ���������';
  icsDocFilterCustomLoadSaveHint    = '��������� / ���������';
  icsDocFilterCustomLoadCaption     = '���&������';
  icsDocFilterCustomLoadHint        = '���������';
  icsDocFilterORCaption             = '�&��';
  icsDocFilterORHint                = '�������� ������ ������� ������������� �� {���}';
  icsDocFilterANDCaption            = '&�';
  icsDocFilterANDHint               = '�������� ������ ������� ������������� �� {�}';
  icsDocFilterLogicNOTCaption       = '���&/�';
  icsDocFilterLogicNOTHint          = '������� ��� ������ �������';
  icsDocFilterNOTCaption            = '&��';
  icsDocFilterNOTHint               = '��������� �������� �������';
  icsDocFilterAddCondCaption        = '&�������� �������';
  icsDocFilterAddCondHint           = '�������� �������';
  icsDocFilterDeleteCaption         = '&�������';
  icsDocFilterDeleteHint            = '�������';
  icsDocFilterKonkrCaption          = '&���������� �������� ...';
  icsDocFilterKonkrHint             = '���������� �������� ...';
  icsDocFilterDeKonkrCaption        = '������� � �&� ������������';
  icsDocFilterDeKonkrHint           = '������� � �� ������������';
  icsDocFilterCommentCaption        = '&�����������';
  icsDocFilterCommentHint           = '�����������';
  icsDocFilterCheckCaption          = '���&������';
  icsDocFilterCheckHint             = '���������';

  icsDocFilterXMLTitle = '������';
  icsDocFilterErrorSetIsFilter = '������ �������� ������� ������� ��� ���������� �������.';
  icsDocFilterErrorLoadNomDLL = '������ �������� "nom_dll.dll". ��� ������ - %s, ��������� - "%s".';
  icsDocFilterErrorFreeNomDLL = '������ �������� "nom_dll.dll". ��� ������ - %s, ��������� - "%s".';
  icsDocFilterErrorCallProc = '%s - ������ ������ "%s" �� "nom_dll.dll".';
  icsDocFilterErrorCaption = '������';
  icsDocFilterConformCaption = '���������������';
  icsDocFilterCheckOkCaption = '������ ���������';
  icsDocFilterCheckEmptyCaption = '������ ������';
  icsDocFilterCheckNoBodyCaption = '������ �����������';

  icsDocFilterNewCaption = '����� ������';
  icsDocFilterNewAuthorCaption = '����������';
  icsDocFilterErrorEmptyGroup = '������� ������ ����� ������� �����������.';
  icsDocFilterErrorEmptyValue = 'icsDocFilterErrorEmptyGroup';
  icsDocFilterErrorIS = '�������� �������������� �������� � UID''��  "%s" �� �������.';
  icsDocFilterErrorISDomain = '�������� �������������� �������� � UID''�� "%s" �� ����������� ������ � ������� "%s".';

//  FilterEdit
  icsDocFilterEditCaption = '������������ �������';
  icsDocFilterEditOkBtnCaption     = '���������';
  icsDocFilterEditCancelBtnCaption = '������';
  icsDocFilterEditHelpBtnCaption   = '&������';

// FilterUtils
  icsDocFilterUtilsFilterVersion = '������ ������� "%s" �� ��������������. ��������� ������ "%s" ��� ����.';

// Options
  icsDocOptions = '';

// Parsers
  icsDocParsersLexTypes01 = '���';
  icsDocParsersLexTypes02 = '��������� ���������';
  icsDocParsersLexTypes03 = '���������� ����� ���������';
  icsDocParsersLexTypes04 = '���������� ������������ ���������';
  icsDocParsersLexTypes05 = '����� ������ �������';
  icsDocParsersLexTypes06 = '����� ������';
  icsDocParsersLexTypes07 = '�������� ��������';
  icsDocParsersLexTypes08 = '�������� ���������';
  icsDocParsersLexTypes09 = '�������� ���������';
  icsDocParsersLexTypes10 = '�������� �������';
  icsDocParsersLexTypes11 = '��������� �����';
  icsDocParsersLexTypes12 = '��������� �� �����';
  icsDocParsersLexTypes13 = '��������� ������';
  icsDocParsersLexTypes14 = '��������� ������';
  icsDocParsersLexTypes15 = '��������� ������ ��� �����';
  icsDocParsersLexTypes16 = '��������� ������ ��� �����';
  icsDocParsersLexTypes17 = '����������� ������';
  icsDocParsersLexTypes18 = '����������� ������';
  icsDocParsersLexTypes19 = '�������� ��';
  icsDocParsersLexTypes20 = '�������� �';
  icsDocParsersLexTypes21 = '�������� ���';

  icsDocParsersLexClasses1 = '��� ������������ ����';
  icsDocParsersLexClasses2 = '��� ��� ��������� ��������� ����, ����� ������/������� ��� ����� ������';
  icsDocParsersLexClasses3 = '��� ��� ��������� ���������� ����';
  icsDocParsersLexClasses4 = '������� ��������';
  icsDocParsersLexClasses5 = '����������������� ��������';
  icsDocParsersLexClasses6 = '���������� ��������';
  icsDocParsersLexClasses7 = '�������� ���������';
  icsDocParsersLexClasses8 = '����������� ������';
  icsDocParsersLexClasses9 = '����������� ������';

  icsDocParsersLexErrors1 = '�������� ������.';
  icsDocParsersLexErrors2 = '������ ���������.';
  icsDocParsersLexErrors3 = '�������� ������.';
  icsDocParsersLexErrors4 = '����������� ����� �������.';
  icsDocParsersLexErrors5 = '��������� ������-�����������.';
  icsDocParsersLexErrors6 = '��������� ������-�����.';
  icsDocParsersLexErrors7 = '��������� ������-����� ��� ������-�����������.';
  icsDocParsersLexErrors8 = '��������� ������-�����, ������-����������� ��� ������ ''.''.';
  icsDocParsersLexErrors9 = '��������� ������-�����, ������-����������� ��� ������ '':''.';

  icsDocParsersSyntErrors01 = '�������� ������.';
  icsDocParsersSyntErrors02 = '��������� ������.';
  icsDocParsersSyntErrors03 = '������ ���������.';
  icsDocParsersSyntErrors04 = '��������� �� �������� ��������.';
  icsDocParsersSyntErrors05 = '�������� �� ������ � ������ ���������.';
  icsDocParsersSyntErrors06 = '��� �������� �� ������� ��������.';
  icsDocParsersSyntErrors07 = '��� �������� ������ ���� ������ ���� � ������ ���� �������.';
  icsDocParsersSyntErrors08 = '��� �������� ������ ���� ������� ��� ��������.';
  icsDocParsersSyntErrors09 = '��� ���������� �������� ������ ���� ������� ���������� ��������.';
  icsDocParsersSyntErrors10 = '��� �������������� �������� ������ ���� ������� �������������� ��������.';
  icsDocParsersSyntErrors11 = '��� �������� ��������� � �������� ��������� ����� ��������� ���, ���������, �������������� ��������� ��� ����� ������ �������.';
  icsDocParsersSyntErrors12 = '������� ���������� ������ ���� ������� � ���� ��������������� ��������� ��� ��������������� ��������.';
  icsDocParsersSyntErrors13 = '������� �������� ������ ���� ������� � ���� ����������� ���������.';

  icsDocParsersSemErrors01 = '�������� ������.';
  icsDocParsersSemErrors02 = '�������� ��������� ������� ������.';
  icsDocParsersSemErrors03 = '����� ������� �����.';
  icsDocParsersSemErrors04 = '��������� ���������� � "1".';
  icsDocParsersSemErrors05 = '����� ������ ������� �����.';
  icsDocParsersSemErrors06 = '��������� ����� ���������� � "1".';
  icsDocParsersSemErrors07 = '����� ������� ������� �����.';
  icsDocParsersSemErrors08 = '��������� �������� ���������� � "1".';
  icsDocParsersSemErrors09 = '������� �� ����.';
  icsDocParsersSemErrors10 = '��� �������������� �������� ������ ���� ������� �������������� ��������.';
  icsDocParsersSemErrors11 = '��� �������� ��������� � �������� ��������� ����� ��������� ���, ���������, �������������� ��������� ��� ����� ������ �������.';

  icsDocParsersErrorPos = ' ������� %s.';

  icsDocParsersErrorExprLex = '����������� ����������� ����������.';
  icsDocParsersErrorExprSynt = '����������� �������������� ����������.';
  icsDocParsersErrorExprSem = '����������� ������������� ����������.';

  icsDocParsersOk = '�������� ������.';

  icsDocParsersLexError = '����������� ������: ';
  icsDocParsersSyntError = '�������������� ������: ';
  icsDocParsersSemError = '������������� ������: ';

  icsDocParsersNotConst = '��';
  icsDocParsersAndConst = '�';
  icsDocParsersOrConst = '���';
  icsDocParsersErrorPolandStr = '�� ����������� �������� PolandStr.';

// Spec
  icsDocSpecCaption = '�������� �������� ���������';
  icsDocSpecTabPrtyColCaption = '������';
  icsDocSpecTabPrtyRowCaption = '�������';
  icsDocSpecEmptyColCaption = '� � � � � �  � � � � � � �';
  icsDocSpecCountViewText = ' (������� ����������)';
  icsDocSpecEmptyEllipsiseEditor = '(�� �����������)';
  icsDocSpecNotEmptyEllipsiseEditor = '(�����������)';
  icsDocSpecEmptyInsCaption = '� � � � � � �  � � � � � � �  -  � � � � � � � �  � �  � � � � � � � � � � �';
  icsDocSpecInscViewText = '�������';
  icsDocSpecListViewText = '������ -> ������ �������� �� "%s"';
  icsDocSpecTableViewText = '������� -> ������ �������� �� "%s"';

  icsDocSpecMainMenuCaption = '����';
  icsDocSpecPropPanelCaption = '��������';

  icsDocSpecParametersCaption = '&���������';
  icsDocSpecParametersHint = '���������';
  icsDocSpecParametersShortCut = '';

  icsDocSpecPreViewCaption = '����&����';
  icsDocSpecPreViewHint = '��������';
  icsDocSpecPreViewShortCut = '';

  icsDocSpecCutCaption = '&��������';
  icsDocSpecCutHint = '��������';
  icsDocSpecCutShortCut = '';

  icsDocSpecCopyCaption = '&����������';
  icsDocSpecCopyHint = '����������';
  icsDocSpecCopyShortCut = '';

  icsDocSpecPasteCaption = '���&�����';
  icsDocSpecPasteHint = '��������';
  icsDocSpecPasteShortCut = '';

  icsDocSpecDeleteCaption = '&�������';
  icsDocSpecDeleteHint = '�������';
  icsDocSpecDeleteShortCut = 'Del';

  icsDocSpecAddInscCaption = '&�������';
  icsDocSpecAddInscHint = '�������';
  icsDocSpecAddInscShortCut = 'Ctrl+I';

  icsDocSpecAddInsTextCaption = '&������� �������';
  icsDocSpecAddInsTextHint = '������� �������';
  icsDocSpecAddInsTextShortCut = 'Ctrl+Alt+I';

  icsDocSpecAddListCaption = '&������';
  icsDocSpecAddListHint = '������';
  icsDocSpecAddListShortCut = 'Ctrl+L';

  icsDocSpecAddLColCaption = '�����&�� ������';
  icsDocSpecAddLColHint = '������� ������';
  icsDocSpecAddLColShortCut = 'Ctrl+Alt+L';

  icsDocSpecAddTableCaption = '&�������';
  icsDocSpecAddTableHint = '�������';
  icsDocSpecAddTableShortCut = 'Ctrl+T';

  icsDocSpecAddTColCaption = '��&����� �������';
  icsDocSpecAddTColHint = '������� �������';
  icsDocSpecAddTColShortCut = 'Ctrl+Alt+T';

  icsDocSpecAddTRowCaption = '��&���� �������';
  icsDocSpecAddTRowHint = '������ �������';
  icsDocSpecAddTRowShortCut = 'Shift+Ctrl+T';

  icsDocSpecMoveUpCaption = '����������� ����&�';
  icsDocSpecMoveUpHint = '����������� �����';
  icsDocSpecMoveUpShortCut = 'Ctrl+Up';

  icsDocSpecMoveDownCaption = '����������� ���&�';
  icsDocSpecMoveDownHint = '����������� ����';
  icsDocSpecMoveDownShortCut = 'Ctrl+Down';

  icsDocSpecCommonItemCaption = '�&����';
  icsDocSpecEditItemCaption = '&������';
  icsDocSpecObjectItemCaption = '&������';

  icsDocSpecXMLTitle = '�������� ���������';
  icsDocSpecErrorCreateXML = '�� ������ xml.';

  icsDocSpecLineName0Col  = '���������';
  icsDocSpecLineName00Col = '����';
  icsDocSpecLineName1Col  = '�������';
  icsDocSpecLineName2Col  = '�������';
  icsDocSpecLineName0Row  = '���������';
  icsDocSpecLineName00Row = '��';
  icsDocSpecLineName1Row  = '������';
  icsDocSpecLineName2Row  = '������';

  icsDocSpecCanDelColRowDopInfo = '�� %s �%s ��������� ��������� %s';
  icsDocSpecErrorCanDelColRow = '%s %s ������ �������, �.�. �� %s ���� ������.';
  icsDocSpecMsgCanDeleteDocRulesRefObj = '�� ��������� ������� � �������� ������������ ��������� �� ��������� ���� ������. ����������?';
  icsDocSpecGetXMLNodeViewTextRow = '������';
  icsDocSpecGetXMLNodeViewTextCol = '�������';
  icsDocSpecGetCountCaption = '������� ���������� ��� "%s"';

  icsDocSpecGetSizeViewTextAuto = '����';
  icsDocSpecGetSizeViewTextFullWidth = '�� ���� ������';
  icsDocSpecGetSizeViewTextFullHeight = '�� ���� ������';
  icsDocSpecErrorApplyStyle = '���������� ������� ����� �� ������.';
  icsDocSpecErrorApplyUnion = '��� ����� �������� ���������� ������ ������ �������� ������ ������� ������� "���������� �������".';
  icsDocSpecErrorApplyText = '���������� ����������� ������� ��������.';
  icsDocSpecErrorApplyInt = '���������� ������� ����� ������������� ��������.';
  icsDocSpecCalcCountRefConf = '�������� "������� ����������" � "������" �����������������. �������� �������� "������� ����������" ����� ��������. ����������?';
  icsDocSpecSetRefDlgCaption = '� ������� ���������';
  icsDocSpecSetRefDlTreegCaption = '������� ��� �����������';
  icsDocSpecSetRefRefObjCaption = '������� ��� ������';
  icsDocSpecSetRefDocParamCaption = '��������� ���������';
  icsDocSpecSetRefTechInfoCaption = '��������������� ��������';
  icsDocSpecErrorSetRefDopInfo = '������� ������� "%s" ��������� �� "%s"';
  icsDocSpecErrorSetRefMsg = '������� �� ����� ���� ������, �.�. �� ���� ������� ������.';
  icsDocSpecErrorOperation = '������� ������ ��������� �� ������������ ������ ��������.';
  icsDocSpecAddTableRowNum = '������ %s';
  icsDocSpecAddTableColNum = '������� %s';
  icsDocSpecAddTableNewCol = '����� �������';
  icsDocSpecAddTableNewRow = '����� ������';
  icsDocSpecWidthCaption = '������';
  icsDocSpecRulesCaption = '������� ������';
  icsDocSpecCalcRulesCaption = '������� ����������';
  icsDocSpecCheckRulesCaption = '������� ��������';
  icsDocSpecDataStyleCaption = '����� ������';
  icsDocSpecHeaderStyleCaption = '����� ���������';
  icsDocSpecDefaultHeaderStyleCaption = '����� ��������� �� ���������';
  icsDocSpecDefaultDataStyleCaption = '����� ������ �� ���������';
  icsDocSpecHeaderCaption = '���������';
  icsDocSpecStyleCaption = '�����';
  icsDocSpecDefaultStyleCaption = '����� �� ���������';
  icsDocSpecNewRowCaption = '� ����� ������';
  icsDocSpecStaticTextCaption = '����������� �����';
  icsDocSpecRefCaption = '������';
  icsDocSpecCalcCountCaption = '������� ����������';
  icsDocSpecColUnionCaption = '���������� �������';
  icsDocSpecShowingCaption = '����������';
  icsDocSpecSelectDataFromCaption = '������ �������� ��';
  icsDocSpecOrderCaption = '������� ����������';
  icsDocSpecShowZeroValsCaption = '���������� ������� ��������';
  icsDocSpecThickRectCaption = '������� �����';
  icsDocSpecRowNumCaption = '��������� �����';
  icsDocSpecColNumCaption = '��������� ��������';
  icsDocSpecPriorityCaption = '��������������';

  icsDocSpecONE_VAVCaption = '��';
  icsDocSpecZERO_VAVCaption = '���';
  icsDocSpecTRUE_VAVCaption = '��';
  icsDocSpecFALSE_VAVCaption = '���';

// SpecUtils
  icsDocSpecUtilsReqVersion = '������ �������� ��������� "%s" �� ��������������. ��������� ������ "%s" ��� ����.';

// Viewer
  icsDocViewerIEHeader = '&b���. &p �� &P';
  icsDocViewerCaption = '�������� ���������';
  icsDocViewerNumTCellLTitle = '� �/�';
  icsDocViewerErrorObjectLocked = '������ ������������';
  icsDocViewerErrorSetDocFormat = '������ ������ ���������� �� ������������ ��������� �����.';
  icsDocViewerErrorNoSpecDef = '������ ���� ������ ������������.';
  icsDocViewerErrorNoDocDef = '������ ���� ������ ��������.';
  icsDocViewerErrorPreviewTypeVal = '�������� "ptUnknown" ��������� APreviewType ������������ ������ ��� ���������� �����.';
  icsDocViewerErrorOpenTmpFile = '������ �������� ���������� �����.';
  icsDocViewerErrorApp = '����������� ����������';
  icsDocViewerErrorClass = '����������� �����';
  icsDocViewerErrorOnFly = '��� ������ "�� ����" �������� � ������� %s �� ��������.';
  icsDocViewerErrorMsgCaption = '����. ��������';
  icsDocViewerErrorRepCreate1 = '���������� - %s;'+#13#10+'���        - %s;'+#13#10+'���������: %s';
  icsDocViewerErrorRepCreate2 = '��� ������������ ������ ��������� ������.';
  icsDocViewerErrorRepCreate3 = '������ ������������ ������.';
  icsDocViewerErrorCreateMSWord = '������ ������ ���������� �� ������������ ������������ ���������� � ������� MS Word.';
  icsDocViewerErrorIllegalOperation = '��������� ��������� ������������ ��������.';

// InclToNameEdit
  icsDocInclToNameEditCaption = '��������� ������, ����������� � ������������ �������';
  icsDocInclToNameEditOkBtnCaption     = '���������';
  icsDocInclToNameEditCancelBtnCaption = '������';
  icsDocInclToNameEditHelpBtnCaption   = '&������';

// ISection
  icsDocISectionErrorCreate1 = '������ ������������ ��� ������������� ������ ������ IOR � IMOR.';
  icsDocISectionErrorCreate2 = '��������� xml-�������� �� �������� �������������.';
  icsDocISectionCaption1 = '����� ���������� ���������� �� ...';
  icsDocISectionCaption2 = '����� ������ ��������� �� ...';
  icsDocISectionErrorCaption = '������!!!';

  icsDocISectionLeaveAsIsBtnCaption     = '�������� �������� ��������������';
  icsDocISectionOkBtnCaption     = '���������';
  icsDocISectionCancelBtnCaption = '������';
  icsDocISectionHelpBtnCaption   = '&������';

  icsDocISectionSelAllCaption = '������� &���';
  icsDocISectionSelAllHint = '������� ���';
  icsDocISectionClearSelCaption = '&�����';
  icsDocISectionClearSelHint = '�����';
  icsDocISectionInvSelCaption = '&��������';
  icsDocISectionInvSelHint = '��������';

// ListSortOrder
  icsDocListSortOrderCaption = '����� �������� ��� ����������';
  icsDocListSortOrderColLabCaption = '�������:';
  icsDocListSortOrderSortOrderLabCaption = '����������� ��:';
  icsDocListSortOrderAddColsCaption = '>';
  icsDocListSortOrderAddColsHint = '�������� ������� ��� ���������� (Ins)';
  icsDocListSortOrderDeleteCaption = 'X';
  icsDocListSortOrderDeleteHint = '������� (Del)';
  icsDocListSortOrderClearCaption = '��������';
  icsDocListSortOrderClearHint = '�������� (Ctrl+Del)';
  icsDocListSortOrderMoveUpCaption = '�����';
  icsDocListSortOrderMoveUpHint = '����������� ����� (Ctrl+�����)';
  icsDocListSortOrderMoveDownCaption = '����';
  icsDocListSortOrderMoveDownHint = '����������� ���� (Ctrl+����)';
  icsDocListSortOrderAscSortCaption = '���������� �� &�����������';
  icsDocListSortOrderAscSortHint = '���������� �� �����������';
  icsDocListSortOrderDescSortCaption = '���������� �� &��������';
  icsDocListSortOrderDescSortHint = '���������� �� ��������';
  icsDocListSortOrderOkBtnCaption     = '���������';
  icsDocListSortOrderCancelBtnCaption = '������';
  icsDocListSortOrderHelpBtnCaption   = '&������';

// ObjRulesAssocEdit

  icsDocObjRulesAssocEditTitleCaptionNormal = '������������ ����� ��������� (������)';
  icsDocObjRulesAssocEditTitleCaptionCross = '������������ ����� ��������� (���������)';
  icsDocObjRulesAssocEditTableElementNameCol = '������ �������';
  icsDocObjRulesAssocEditTableElementNameRow = '����� ������';
  icsDocObjRulesAssocEditSetAssocConf = '��� �������� "%s" ��� ����������� ������������. ����������?';
  icsDocObjRulesAssocEditCaption = '��������� ������������';
  icsDocObjRulesAssocEditThisStructTitle = '������:';
  icsDocObjRulesAssocEditRefStructTitle = '������������� ������:';
  icsDocObjRulesAssocEditAutoExpand = '�����������';
  icsDocObjRulesAssocEditSetAssocCaption = '&����������';
  icsDocObjRulesAssocEditSetAssocHint = '����������';
  icsDocObjRulesAssocEditOkCaption = '���������';
  icsDocObjRulesAssocEditOkHint = '���������';
  icsDocObjRulesAssocEditClearAssocCaption = '&��������';
  icsDocObjRulesAssocEditClearAssocHint = '��������';
  icsDocObjRulesAssocEditCancelBtnCaption = '������';
  icsDocObjRulesAssocEditHelpBtnCaption   = '&������';

// ObjRulesEdit
  icsDocObjRulesEditCaption = '��������� ������������';
  icsDocObjRulesEditObjTitleCaption = '  ������';
  icsDocObjRulesEditThisObjTitle = '������:';
  icsDocObjRulesEditRefObjTitle = '������������� ������:';
  icsDocObjRulesEditPropHead = '  ���������';
  icsDocObjRulesEditAssocType = '��� ������������:';
  icsDocObjRulesEditCrossTypeN = '������';
  icsDocObjRulesEditCrossTypeC = '���������';

  icsDocObjRulesEditOkCaption = '���������';
  icsDocObjRulesEditOkHint = '���������';
  icsDocObjRulesEditCancelBtnCaption = '������';
  icsDocObjRulesEditHelpBtnCaption   = '&������';

// ParametersEdit
  icsDocParametersEditSpecName = '������������';
  icsDocParametersEditOrientation1 = '�������';
  icsDocParametersEditOrientation2 = '���������';
  icsDocParametersEditStyleExists = '����� � ������������� "%s" ��� ����.';

  icsDocParametersEditCDERR_DIALOGFAILURE    = '������ �������� ����������� ����.';
  icsDocParametersEditCDERR_FINDRESFAILURE   = '�� ������ ��������� ������.';
  icsDocParametersEditCDERR_INITIALIZATION   = '������ ������������� ������� ����������� ����, �������� ��-�� �������� ������.';
  icsDocParametersEditCDERR_LOADRESFAILURE   = '������ �������� ���������� �������.';
  icsDocParametersEditCDERR_LOADSTRFAILURE   = '������ �������� ��������� ������.';
  icsDocParametersEditCDERR_LOCKRESFAILURE   = '������ ���������� ���������� �������.';
  icsDocParametersEditCDERR_MEMALLOCFAILURE  = '������ ��������� ������ ��� ���������� ��������.';
  icsDocParametersEditCDERR_MEMLOCKFAILURE   = '������ ���������� ������, ��������� � ������������.';
  icsDocParametersEditCDERR_NOHINSTANCE      = '�� ������ ��������������� ���������� ���������� ���������� ��� ������ ���������� �������.';
  icsDocParametersEditCDERR_NOHOOK           = '�� ������ ��������������� ��������� �� hook-���������.';
  icsDocParametersEditCDERR_NOTEMPLATE       = '�� ������ ������ ����������� ����.';
  icsDocParametersEditCDERR_REGISTERMSGFAIL  = '������ ����������� ������ ����������� ����.';
  icsDocParametersEditCDERR_STRUCTSIZE       = '������ ��� �������� ������� ����������������� ���������.';
  icsDocParametersEditPDERR_CREATEICFAILURE  = '������ �������� ��������������� ���������.';
  icsDocParametersEditPDERR_DEFAULTDIFFERENT = '������� �� ��������� �� ������������� ����������.';
  icsDocParametersEditPDERR_DNDMMISMATCH     = '�� ������������ ��������� ��������� �� ���������� ����������������� ����������.';
  icsDocParametersEditPDERR_GETDEVMODEFAIL   = '������ ������������� ���������� ���������.';
  icsDocParametersEditPDERR_INITFAILURE      = '������ �������������';
  icsDocParametersEditPDERR_LOADDRVFAILURE   = '������ �������� �������� ���������� ��������.';
  icsDocParametersEditPDERR_NODEFAULTPRN     = '����������� ������� �� ���������.';
  icsDocParametersEditPDERR_NODEVICES        = '�� ������� �������� ��������.';
  icsDocParametersEditPDERR_PARSEFAILURE     = '������ ������� ����� � ����������������� ���������.';
  icsDocParametersEditPDERR_PRINTERNOTFOUND  = '������ [devices] ����� WIN.INI �� �������� ������ ��� ���������� ��������.';
  icsDocParametersEditPDERR_RETDEFFAILURE    = '�������������� � ����������������� ���������.';
  icsDocParametersEditPDERR_SETUPFAILURE     = '������ �������� ���������� �������.';
  icsDocParametersEditErrorCreateDlg = '������ �������� ����������� ����.';
  icsDocParametersEditNewStyle = '����� ����� %s';
  icsDocParametersEditDeleteStyleConf = '����� "%s" ������������ ��������� ���������. �� ������� � ���, ��� ������ ������� ���?';

  icsDocParametersEditfmParametersEditCaption = '���������';
  icsDocParametersEditbApplyBtnCaption = '���������';
  icsDocParametersEditbCancelBtnCaption = '������';
  icsDocParametersEditbHelpBtnCaption = '&������';
  icsDocParametersEdittsCommonTSCaption = '&�����';
  icsDocParametersEditSpecNameLabLabCaption = '������������:';
  icsDocParametersEditLabel3LabCaption = '�����:';
  icsDocParametersEditLabel4LabCaption = '������:';
  icsDocParametersEditLabel5LabCaption = '��������:';
  icsDocParametersEditLabel6LabCaption = '���� � ����� ��������:';
  icsDocParametersEditlbTimeStampLabCaption = 'lbTimeStamp';
  icsDocParametersEditlbAutorLabCaption = 'lbAutor';
  icsDocParametersEditCondRulesLbLabCaption = '������� ������:' ;
  icsDocParametersEditDocRulesLbLabCaption = '������� ������������ ��������� �� ���������:';
  icsDocParametersEditGroupBox1GBCaption = '������� ���������� ���������';
  icsDocParametersEditchbPersonalCHBCaption = '������������';
  icsDocParametersEditchbGroupCHBCaption = '���������';
  icsDocParametersEditPeriodDefChBxCHBCaption = '�������������';
  icsDocParametersEditSaveBDChBxCHBCaption = '��������� � ����� ����������';
  icsDocParametersEdittsPageSetupTSCaption = '&��������� ��������';
  icsDocParametersEditLabel8LabCaption = '������';
  icsDocParametersEditLabel9LabCaption = '����������';
  icsDocParametersEditLabel10LabCaption = '���� (��)';
  icsDocParametersEditLabel11LabCaption = '������:';
  icsDocParametersEditlbOrientationLabCaption = 'lbOrientation';
  icsDocParametersEditLabel13LabCaption = '�����:';
  icsDocParametersEditLabel14LabCaption = '�������:';
  icsDocParametersEditLabel15LabCaption = '������:';
  icsDocParametersEditLabel16LabCaption = '������:';
  icsDocParametersEditlbPageSizeLabCaption = 'lbPageSize';
  icsDocParametersEditlbLeftMarginLabCaption = 'lbLeftMargin';
  icsDocParametersEditlbTopMarginLabCaption = 'lbTopMargin';
  icsDocParametersEditlbRightMarginLabCaption = 'lbRightMargin';
  icsDocParametersEditlbBottomMarginLabCaption = 'lbBottomMargin';
  icsDocParametersEditbChangePageSetupBtnCaption = '��������';
  icsDocParametersEdittsStylesTSCaption = '&�����';
  icsDocParametersEditLabel1LabCaption = '�����';
  icsDocParametersEditLabel22LabCaption = '��������������';
  icsDocParametersEditlbFontInfoLabCaption = 'lbFontInfo';
  icsDocParametersEditchbUseBDefCHBCaption = '������������ �� ��������� ��� ������';
  icsDocParametersEditchbUseHDefCHBCaption = '������������ �� ��������� ��� ����������';
  icsDocParametersEditbAddStyleBtnCaption = '��������';
  icsDocParametersEditbDeleteStyleBtnCaption = '�������';
  icsDocParametersEditchbWordWrapCHBCaption = '���������� �� ������';
  icsDocParametersEditbChangeFontBtnCaption = '�������� �����';
  icsDocParametersEditButton1BtnCaption = '�������� ������������';
  icsDocParametersEditactAddStyleActCaption = '&��������';
  icsDocParametersEditactEditStyleActCaption = '&�������� ������������';
  icsDocParametersEditactDeleteStyleActCaption = '&�������';
//  icsDocParametersEdit = '';

// PassportEdit
  icsDocPassportEditErrorOwnreType = '�������� ������ ���� �������� TICSDocFilter.';

  icsDocPassportEditCaption = '���������';
  icsDocPassportEditLabel1LabCaption = '������������:';
  icsDocPassportEditLabel2LabCaption = '�����:';
  icsDocPassportEditLabel3LabCaption = '������:';
  icsDocPassportEditLabel4LabCaption = '��������:';
  icsDocPassportEditLabel5LabCaption = '���� � ����� ��������:';
  icsDocPassportEditlbTimeStampLabCaption = 'lbTimeStamp';
  icsDocPassportEditbNameAdvancedBtnCaption = '������������� ...';
  icsDocPassportEditbApplyBtnCaption = '���������';
  icsDocPassportEditbCancelBtnCaption = '������';
  icsDocPassportEditbHelpBtnCaption = '������';

// PeriodEdit
  icsDocPeriodEditGetPeriodDefXMLStr = '������� "%s" ������������ TPeriodDef �� ��������������.';
  icsDocPeriodEditSetPeriodDefXMLStr = '"%s" - ������������ ��������.';
  icsDocPeriodEditGetPeriodValueXMLStr = '��������� ������ ���������.';

  icsDocPeriodEditGetPeriodValueXMLStrHalfYear = '�������� "%s" ��� ��������� �� ��������������.';
  icsDocPeriodEditGetPeriodValueXMLStrQuart = '�������� "%s" ��� �������� �� ��������������.';

  icsDocPeriodEditSetPeriodValueXMLStrHalfYear = '"%s" - ������������ �������� ������� ��� ���������.';
  icsDocPeriodEditSetPeriodValueXMLStrQuart = '"%s" - ������������ �������� ������� ��� ��������.';
  icsDocPeriodEditSetPeriodValueXMLStrMonth = '"%s" - ������������ �������� ������� ��� ������.';


  icsDocPeriodEditValidateInputsBegin = '���������� ������� ������ �������.';
  icsDocPeriodEditValidateInputsEnd = '���������� ������� ��������� �������.';
  icsDocPeriodEditValidateInputsNotEq = '������ ������� �� ������ ��������� ��� ���������.';
  icsDocPeriodEditDefRBtnClick = '�� ������ ������� ������� FPeriodDefMap.';

  icsDocPeriodEditCaption = '������';
  icsDocPeriodEditOkBtnBtnCaption = '���������';
  icsDocPeriodEditCancelBtnBtnCaption = '������';
  icsDocPeriodEditClientGBxGBCaption = '�������� �������';
  icsDocPeriodEditHalfYearYearLbLabCaption = '���';
  icsDocPeriodEditQuoterYearLbLabCaption = '���';
  icsDocPeriodEditMonthYearLbLabCaption = '���';
  icsDocPeriodEditUniqueSepLbLabCaption = '-';
  icsDocPeriodEditYearRBtnRBCaption = '���';
  icsDocPeriodEditHalfYearRBtnRBCaption = '���������';
  icsDocPeriodEditQuoterRBtnRBCaption = '�������';
  icsDocPeriodEditMonthRBtnRBCaption = '�����';
  icsDocPeriodEditUniqueRBtnRBCaption = '����������';
  icsDocPeriodEditUnDefRBtnRBCaption = '�� ���������';

  icsDocPeriodEditHalfYearCBxItems1 = '������';
  icsDocPeriodEditHalfYearCBxItems2 = '������';

  icsDocPeriodEditQuoterCBxItems1 = '������';
  icsDocPeriodEditQuoterCBxItems2 = '������';
  icsDocPeriodEditQuoterCBxItems3 = '������';
  icsDocPeriodEditQuoterCBxItems4 = '���������';

// RulesExprEdit

  icsDocRulesExprEditCaption = '�������';
  icsDocRulesExprEditlbTitleLabCaption = '�������';
  icsDocRulesExprEditbOkBtnCaption = '���������';
  icsDocRulesExprEditbCancelBtnCaption = '������';
  icsDocRulesExprEditbHelpBtnCaption = '&������';

  icsDocRulesExprEditrtUnknown = '???';
  icsDocRulesExprEditrtCalcCaption = '������� ����������';
  icsDocRulesExprEditrtCalclbTitleCaption =  '������� ���������� ���';
  icsDocRulesExprEditrtHandCaption = '������� �������� ������� �����';
  icsDocRulesExprEditrtHandlbTitleCaption =  '������� �������� ������� ����� ���';

// SetRef
  icsDocSetRefGetObjTreeTitle = '������ �������� ��� ������ �� ���������.';
  icsDocSetRefSetObjTreeTitle = '������ �������� ��� ������ �� ���������.';
  icsDocSetRefSetRef = '�� ������ ������ ������ � ���''�� "%s".';

  icsDocSetRefCaption = '���������� ������';
  icsDocSetRefbOkBtnCaption = '���������';
  icsDocSetRefbCancelBtnCaption = '������';
  icsDocSetRefbHelpBtnCaption = '&������';
  icsDocSetRefbClearBtnCaption = '��������';

// StyleEdit
  icsDocStyleEditStyleNameCaption   = '������������ �����';
  icsDocStyleEditfumADDCaption = '���������� �����';
  icsDocStyleEditfumEDITCaption = '�������������� ������������ �����';

  icsDocStyleEditbOkBtnCaption = '���������';
  icsDocStyleEditbCancelBtnCaption = '������';
  icsDocStyleEditbHelpBtnCaption = '&������';
  icsDocStyleEditStyleNameLabLabCaption = '������������ �����:';

// AddList
  icsDocAddListErrorInvalidArgument = '���������� ������ ���� �������� �������������.';
  icsDocAddListErrorSetMainIS = '��������� �������� �� ����������� ������, ������������ � cbISs.';
  icsDocAddListErrorAddCol =  '"��� ������ �������� � ��������� "���������� �������" ����������� ���������� ����� ��������. ������ �������� �� ����� ���������."';
  icsDocAddListErrorAddCols = '"��� ������ �������� � ��������� "���������� �������" ����������� ���������� ����� ��������."';
  icsDocAddListCaption = '����� ������� ������';
  icsDocAddListColGroupCaption = '� � � � � �  � � � � � � � �';

  icsDocAddListISListTitle = '������ �������� ��:';
  icsDocAddListISTreeTitle = '�������:';
  icsDocAddListColsTitle = '�������:';

  icsDocAddListAddColsCaption = '>';
  icsDocAddListAddColsHint = '�������� �������';

  icsDocAddListAddGColsCaption = '+>';
  icsDocAddListAddGColsHint = '�������� ������ ��������';

  icsDocAddListDeleteCaption = 'X';
  icsDocAddListDeleteHint = '�������';

  icsDocAddListClearCaption = '��������';
  icsDocAddListClearHint = '��������';

  icsDocAddListUpCaption = '�����';
  icsDocAddListUpHint = '����������� �����';

  icsDocAddListDownCaption = '����';
  icsDocAddListDownHint = '����������� ����';

  icsDocAddListOkBtnCaption     = '���������';
  icsDocAddListCancelBtnCaption = '������';
  icsDocAddListHelpBtnCaption   = '&������';

// AddTable
  icsDocAddTableListSrcLabCaption = '������ �������� ��:';
  icsDocAddTableColCountLabCaption = '����� ��������:';
  icsDocAddTableRowCountLabCaption = '����� �����:';
  icsDocAddTableOkBtnCaption     = '���������';
  icsDocAddTableCancelBtnCaption = '������';
  icsDocAddTableHelpBtnCaption   = '&������';

// CommentEdit
  icsDocCommentEditCommentLabCaption = '�����������:';
  icsDocCommentEditOkBtnCaption      = '���������';
  icsDocCommentEditCancelBtnCaption  = '������';
  icsDocCommentEditHelpBtnCaption    = '&������';

// Count
  icsDocCountEvtCaption = '������� ���������� ��� "%s"';
  icsDocCountCountCaption = '������� ���������� ��� :';
  icsDocCountFilterRulesCaption = '������� ������:';
  icsDocCountOkBtnCaption      = '���������';
  icsDocCountCancelBtnCaption  = '������';
  icsDocCountHelpBtnCaption    = '&������';
  icsDocCountClearBtnCaption    = '��������';

// ElSizeEdit
  icsDocElSizeEditAreInputsCorrectMsg = '���������� ������� ��������.';
  icsDocElSizeEditValHeightCaption = '��������� �������� ������';
  icsDocElSizeEditHeightCaption = '������';
  icsDocElSizeEditFullHeightCaption = '�� ���� ������';
  icsDocElSizeEditValWidthCaption = '��������� �������� ������';
  icsDocElSizeEditWidthCaption = '������';
  icsDocElSizeEditFullWidthCaption = '�� ���� ������';

  icsDocElSizeEditErrorSizeType = '"%s" - ������������ �������� ��� ���� �������.';
  icsDocElSizeEditErrorUnitsType = '"%s" - ������������ �������� ��� ������� ���������.';

  icsDocElSizeEditErrorSizeVal = '��������� ������� ������ ���� ��������������� �����.';
  icsDocElSizeEditErrorMaxSizeVal = '������������ �������� ������� ������ ���� ��������������� ������.';
  icsDocElSizeEditErrorSizeValRange = '"%s" - �������� �� ������ ��������� "%s".';
  icsDocElSizeEditErrorValValidate = '"%s" - ������������ ��������.';

  icsDocElSizeEditAutoSizeCaption  = '���������������';
  icsDocElSizeEditValueCaption     = '��������';
  icsDocElSizeEditUnitsCaption     = '�������:';
  icsDocElSizeEditOkBtnCaption     = '���������';
  icsDocElSizeEditCancelBtnCaption = '������';
  icsDocElSizeEditHelpBtnCaption   = '&������';

// HandEdit
  icsDocMatrixErrorParam = '�������� ������ ����';
  icsDocMatrixErrorIndex = '������ %s ������� �� ������� �������.';
  icsDocHandEditErrorRow = '������';
  icsDocHandEditErrorCol = '�������';
  icsDocHandEditCheckRulesMsg = '������ ������ (� ������ - %s; � ������� - %s)'+#13#10+'�� ������������� �������� �������� ��� %s � %s.'+#13#10+'������� ��������: %s';
  icsDocHandEditCheckRulesCpt = '��������� ������ �� ������������� �������� ��������.';

  icsDocHandEditStructHeaderCaption = '  ��������� ���������';
  icsDocHandEditmiDocCaption      = '��������';
  icsDocHandEditmiViewCaption     = '���';
  icsDocHandEditmiJumpCaption     = '�������';

  icsDocHandEditSaveCaption = '&���������';
  icsDocHandEditSaveHint = '���������';
  icsDocHandEditReCalcCaption = '&�������� ����������� ��������';
  icsDocHandEditReCalcHint = '�������� ����������� ��������';
  icsDocHandEditFirstDocElementCaption = '�&����� ������� ���������';
  icsDocHandEditFirstDocElementHint = '������ ������� ���������';
  icsDocHandEditPrevDocElementCaption = '�&��������� ������� ���������';
  icsDocHandEditPrevDocElementHint = '���������� ������� ���������';
  icsDocHandEditNextDocElementCaption = '�&�������� ������� ���������';
  icsDocHandEditNextDocElementHint = '��������� ������� ���������';
  icsDocHandEditLastDocElementCaption = '�&�������� ������� ���������';
  icsDocHandEditLastDocElementHint = '��������� ������� ���������';
  icsDocHandEditViewStructCaption = '&��������� ���������';
  icsDocHandEditViewStructHint = '��������� ���������';
  icsDocHandEditViewDocElementInscCaption = '&������� ��� ��������� ���������';
  icsDocHandEditViewDocElementInscHint = '������� ��� ��������� ���������';
  icsDocHandEditExitCaption = '&�������';
  icsDocHandEditExitHint = '�������';

// RulesEdit
  icsDocRulesEditVersionLimit  = '������ ������ ������������ ��������� ������ ������������ ������ ��� ������.';
  icsDocRulesEditAccessDeny    = '� ������� ��������.';
  icsDocRulesEditErrorEmptyDoc = '���������� ������� ���� �� ���� ������ ������������ ���������.';
  icsDocRulesEditConfCol   = '������ �������';
  icsDocRulesEditConfRow   = '����� ������';
  icsDocRulesEditErrorConf = '���������� ������� ������������ ���� �� ��� %s �������.';
  icsDocRulesEditErrorIntDefDoc = '���������� ������� �������� ������������� ���������� �� ������.';
  icsDocRulesEditErrorPeriodDef = '���������� ������� �������� �������.';
  icsDocRulesEditErrorOrgDef = '���������� ������� ��� ����������� �� ������.';
  icsDocRulesEditDefChangeConf = '����� �������� ������������� ���������� �������� � ������� ������ ��������. ����������?';

  icsDocRulesEditCaption     = '������� ������������ ��������� �� ���������';
  icsDocRulesEditCreateStep  = '  ����� ������������';
  icsDocRulesEditStepParam   = '     ���������';
  icsDocRulesEditStepObjects = '     �������';
  icsDocRulesEditIntDocCaption = '������������� ���������';
  icsDocRulesEditIntDocDef = '��������:';
  icsDocRulesEditIntDocPerType = '��� �������:';
  icsDocRulesEditIntDocPeriod = '������:';
  icsDocRulesEditIntDocPeriodisity = '�������������:';
  icsDocRulesEditOrg = '�����������';
  icsDocRulesEditOrgType = '���:';
  icsDocRulesEditOrgName = '������������:';

  icsDocRulesEditAddStepCaption = '�������� &����';
  icsDocRulesEditAddStepHint = '�������� ����';
  icsDocRulesEditMoveStepUpCaption = '����������� ����&�';
  icsDocRulesEditMoveStepUpHint = '����������� �����';
  icsDocRulesEditMoveStepDownCaption = '����������� ���&�';
  icsDocRulesEditMoveStepDownHint = '����������� ����';
  icsDocRulesEditDeleteCaption = '&�������';
  icsDocRulesEditDeleteHint = '�������';
  icsDocRulesEditAddObjRulesCaption = '�������� ������� ������������ &�������';
  icsDocRulesEditAddObjRulesHint = '�������� ������� ������������ �������';
  icsDocRulesEditSetAssocCaption = '���������� ����&��������';
  icsDocRulesEditSetAssocHint = '���������� ������������';
  icsDocRulesEditEditPropCaption = '&��������';
  icsDocRulesEditEditPropHint = '��������';

  icsDocRulesEditOkBtnCaption     = '���������';
  icsDocRulesEditCancelBtnCaption = '������';
  icsDocRulesEditHelpBtnCaption   = '&������';

// DurationEdit
  icsDocDurationEditCaption = '������������';
  icsDocDurationEditTitle = '�������� ������������:';
  icsDocDurationEditOkBtnCaption     = '���������';
  icsDocDurationEditCancelBtnCaption = '������';
  icsDocDurationEditError = '������� ������������ �������� ������������.';

// HTMLPreview
  icsDocHTMLPreviewMsgCaption = '���������� �����';
  icsDocHTMLPreviewErrorRewrite = '������ ���������� ����� "%s"';
  icsDocHTMLPreviewCaption = '��������';
  icsDocHTMLPreviewSaveDialogFilter = '���-�������� (*.htm;*.html;*.shtml)|*.htm;*.html;*.shtml|��� ����� (*.*)|*.*';
  icsDocHTMLPreviewSaveAsCaption = '&��������� ���...';
  icsDocHTMLPreviewSaveAsHint = '��������� ���...';
  icsDocHTMLPreviewPrintPreviewCaption = '�&������� ������';
  icsDocHTMLPreviewPrintPreviewHint = '�������� ������';
  icsDocHTMLPreviewPrintCaption = '&������';
  icsDocHTMLPreviewPrintHint = '������';
  icsDocHTMLPreviewCloseCaption = '&�������';
  icsDocHTMLPreviewCloseHint = '�������';

// Preview
  icsDocPreviewCommPageCount = '����� �������: %s ';
  icsDocPreviewZoomHeight = '�� ������';
  icsDocPreviewZoomWidth  = '�� ������';
  icsDocPreviewZoomCustom = '�����';
//  icsDocPreviewMainTBM: TdxBarManager
//    Bars = <
//      item
//        Caption = '��������� ���������'
//    Categories.Strings = (
//      '��������� ������')
  icsDocPreviewactPrintCaption = '������';
  icsDocPreviewactPrintHint = '������';
  icsDocPreviewactZoomHeightCaption = '�� ������';
  icsDocPreviewactZoomHeightHint = '�� ������';
  icsDocPreviewactZoomWidthCaption = '�� ������';
  icsDocPreviewactZoomWidthHint = '�� ������';
  icsDocPreviewactSet1PageCaption = '���� ��������';
  icsDocPreviewactSet1PageHint = '���� ��������';
  icsDocPreviewactSet2PageCaption = '��� ��������';
  icsDocPreviewactSet2PageHint = '��� ��������';
  icsDocPreviewactPrintSettingCaption = '��������� ��������';
  icsDocPreviewactPrintSettingHint = '��������� ��������';
  icsDocPreviewactSaveCaption = '���������';
  icsDocPreviewactSaveHint = '���������';
  icsDocPreviewactExcelCaption = '������� ������ � "MS Excel"';
  icsDocPreviewactExcelHint = '������� ������ � "MS Excel"';
  icsDocPreviewSaveDlgFilter = '���-�������� (*.htm)|*.htm|MS Word (*.doc)|*.doc';
  icsDocPreviewSaveDlgTitle = '��������� �����';

// PrnStatus
  icsDocPrnStatusStatusLabelCaption1 = '��������������';
  icsDocPrnStatusStatusLabelCaption2 = '�������� � %s';
  icsDocPrnStatusCancelButtonCaption = '������';

//icsDocPageSetup

  icsDocPagePrintKoeff = '����������� ������: ';
  icsDocPageSetupCaption = '��������� ��������';
  icsDocPageSetupOrientationGBCaption = ' ���������� ';
  icsDocPageSetupportRBCaption = '�������';
  icsDocPageSetuplandRBCaption = '���������';
  icsDocPageSetupFieldsGBCaption = ' ���� (��) ';
  icsDocPageSetupOkBtnCaption = '���������';
  icsDocPageSetupCancelBtnCaption = '������';

implementation

end.
