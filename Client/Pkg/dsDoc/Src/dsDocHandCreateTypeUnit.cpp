﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsDocHandCreateTypeUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxRadioGroup"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TdsDocCreateTypeForm * dsDocCreateTypeForm;
// ---------------------------------------------------------------------------
__fastcall TdsDocCreateTypeForm::TdsDocCreateTypeForm(TComponent * Owner, TdsDocDM * ADM) : TForm(Owner)
 {
  FDM              = ADM;
  FOrgCode         = "";
  OrgCB->ItemIndex = FLoadSubOrg(FDM->GetThisSpecOwner());
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocCreateTypeForm::OrgCBPropertiesChange(TObject * Sender)
 {
  ApplyBtn->Enabled = OrgCB->ItemIndex != -1;
  FOrgCode          = "";
  if (OrgCB->ItemIndex != -1)
   {
    FOrgCode = FOrgCodeMap[OrgCB->ItemIndex];
   }
 }
// ---------------------------------------------------------------------------
int __fastcall TdsDocCreateTypeForm::FLoadSubOrg(UnicodeString AOwnerCode)
 {
  int RC = -1;
  FOrgCodeMap.clear();
  OrgCB->Properties->Items->Clear();
  try
   {
    TJSONObject * RetData;
    TJSONObject * tmpPair;
    TJSONString * tmpData;
    UnicodeString FClCode, FClStr;
    RetData = FDM->kabCDSGetValById("doc.s2.Select CODE as ps1, FULLNAME as ps2 from SUBORDORG order by name", 0);
    // Select CODE, NAME, FULLNAME from SUBORDORG order by name
    TJSONPairEnumerator * itPair = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value().LowerCase();
      FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
      if (!((FClCode == "812.1107704.000") || (FClCode == "812.2741381.000") || (FClCode == "812.7171319.000")))
       {
        OrgCB->Properties->Items->Add(FClStr);
        FOrgCodeMap[OrgCB->Properties->Items->Count - 1] = FClCode;
        if (AOwnerCode.UpperCase() == FClCode.UpperCase())
         RC = OrgCB->Properties->Items->Count - 1;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
