//---------------------------------------------------------------------------

#include <vcl.h>
#include <DateUtils.hpp>
#pragma hdrstop

#include "dsDocPackPeriodUnit.h"
//#include "reportdef.h"
#include "msgdef.h"
#include "dsDocClientConstDef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
//#ifndef _CONFSETTTING
// #error _CONFSETTTING using are not defined
//#endif

#pragma link "cxButtons"
#pragma link "cxCalendar"
#pragma link "cxCheckBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDateUtils"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "dxCore"
#pragma resource "*.dfm"
TdsDocPackPeriodForm *dsDocPackPeriodForm;
//---------------------------------------------------------------------------
__fastcall TdsDocPackPeriodForm::TdsDocPackPeriodForm(TComponent* Owner, bool ADocRules)
        : TForm(Owner)
{
//  Caption = FMT(dsDocDocPeriodCaption);
  Caption = FMT(dsDocDocPeriodCaptionLabCaption);
  DateFrLab->Caption = FMT(dsDocDocPeriodDateFrLabCaption);
  DateToLab->Caption = FMT(dsDocDocPeriodDateToLabCaption);
  OkBtn->Caption = FMT(dsDocDocPeriodOkBtnCaption);
  CancelBtn->Caption = FMT(dsDocDocPeriodCancelBtnCaption);

  PerTypeCB->ItemIndex = 0;
  Date().DecodeDate(&FCY, &FCM, &FCD);
  CreateTypeCB->Enabled = ADocRules;
  if (!ADocRules)
   CreateTypeCB->ItemIndex = 1;
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPackPeriodForm::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if ((Key == VK_RETURN)&&OkBtn->Enabled) OkBtnClick(OkBtn);
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPackPeriodForm::OkBtnClick(TObject *Sender)
{
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocPackPeriodForm::GetLastDay(int AMons, int AYear)
{
  if (IsValidDate((Word)AYear, (Word)AMons, (Word)1))
   {// ��� � ����� ����������
     return IntToStr(DaysInAMonth((Word)AYear, (Word)AMons));
   }
  else
   return "1";
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPackPeriodForm::PerDataCBPropertiesChange(TObject *Sender)
{
  if (PerDataCB->ItemIndex > 0)
   {
     if (PerTypeCB->ItemIndex == 5)
      {
        DateFrLab->Enabled = true;
        DateFrDE->Enabled = true;
        DateToLab->Enabled = true;
        DateToDE->Enabled = true;
      }
     else
      {
         YearChB->Enabled = true;
         YearChB->Checked = false;
         YearED->Enabled = true;
         YearED->Text = "";
      }
   }
  else
   {
         YearChB->Enabled = false;
         YearED->Enabled = false;
         YearED->Text = "";
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPackPeriodForm::PerTypeCBPropertiesChange(TObject *Sender)
{
  PerDataCB->Enabled = false;
  YearED->Enabled = false;
  YearED->Text = "";
  DateFrLab->Enabled = false;
  YearChB->Enabled = false;
  YearChB->Checked = false;
  DateFrDE->Enabled = false;
  DateToLab->Enabled = false;
  DateToDE->Enabled = false;
  DateFrDE->Text = "";
  DateToDE->Text = "";

  PerDataCB->Properties->Items->Clear();
  if (PerTypeCB->ItemIndex == 0)
   {
   }
  else if (PerTypeCB->ItemIndex == 1)
   {
     YearChB->Enabled = true;
   }
  else if (PerTypeCB->ItemIndex == 2)
   {
     PerDataCB->Enabled = true;
     PerDataCB->Properties->Items->Add("��������� ���");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->ItemIndex = 0;
   }
  else if (PerTypeCB->ItemIndex == 3)
   {
     PerDataCB->Enabled = true;
     PerDataCB->Properties->Items->Add("������� ���");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("��������");
     PerDataCB->ItemIndex = 0;
   }
  else if (PerTypeCB->ItemIndex == 4)
   {
     PerDataCB->Enabled = true;

     PerDataCB->Properties->Items->Add("����� ���");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("�������");
     PerDataCB->Properties->Items->Add("����");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("���");
     PerDataCB->Properties->Items->Add("����");
     PerDataCB->Properties->Items->Add("����");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("��������");
     PerDataCB->Properties->Items->Add("�������");
     PerDataCB->Properties->Items->Add("������");
     PerDataCB->Properties->Items->Add("�������");
     PerDataCB->ItemIndex = 0;
   }
  else if (PerTypeCB->ItemIndex == 5)
   {
     PerDataCB->Enabled = true;
     PerDataCB->Properties->Items->Add("�������� ���");
     PerDataCB->Properties->Items->Add("��������� ��������");
     PerDataCB->ItemIndex = 0;
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPackPeriodForm::YearChBPropertiesChange(TObject *Sender)
{
  YearED->Enabled = YearChB->Checked;
  if (YearED->Enabled)
   YearED->Text = FCY;
  else
   YearED->Text = "";
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPackPeriodForm::ShiftEDPropertiesValidate(TObject *Sender,
      Variant &DisplayValue, TCaption &ErrorText, bool &Error)
{
  if (Error)
   {
     UnicodeString tmpVal = VarToWideStrDef(DisplayValue, "").Trim();
     Error = false;
     if (!tmpVal.LastDelimiter("/"))
      { // ����� ���
        DisplayValue = Variant(tmpVal+"/0/0");
      }
     else
      {
        if (tmpVal.Pos("/") == tmpVal.LastDelimiter("/"))
         { // ����� ���+"/"+�������?
           if (tmpVal.LastDelimiter("/") == tmpVal.Length())
            { // ����� ���+"/"
              DisplayValue = Variant(tmpVal+"0/0");
            }
           else
            { // ����� ���+"/"+�������
              DisplayValue = Variant(tmpVal+"/0");
            }
         }
        else
         {
           if (tmpVal.LastDelimiter("/") == tmpVal.Length())
            { // ����� ���+"/"+�������+"/"
              DisplayValue = Variant(tmpVal+"0");
            }
           else
            { // �������, �� �� ������ ���� ���������
              Error = true;
              ErrorText = "������� ������������ �������� ��������.";
            }
         }
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsDocPackPeriodForm::YearEDPropertiesValidate(TObject *Sender,
      Variant &DisplayValue, TCaption &ErrorText, bool &Error)
{
  if (Error)
   {
     UnicodeString tmpVal = VarToWideStrDef(DisplayValue,"").Trim();
     Error = false;
     switch (tmpVal.Length())
      {
        case 0: {DisplayValue = Variant(IntToStr(FCY)); break;}
        case 1: {DisplayValue = Variant(IntToStr(FCY).SubString(1,3)+tmpVal); break;}
        case 2: {DisplayValue = Variant(IntToStr(FCY).SubString(1,2)+tmpVal); break;}
        case 3: {DisplayValue = Variant(IntToStr(FCY).SubString(1,1)+tmpVal); break;}
        default: {
                   Error = true;
                   ErrorText = "������� ������������ �������� ����.";
                 }
      }
   }
}
//---------------------------------------------------------------------------
int __fastcall TdsDocPackPeriodForm::FGetPerType()
{
  if (PerTypeCB->ItemIndex != -1)
   {
     return PerTypeCB->ItemIndex+1;
   }
  else
    return 0;
}
//---------------------------------------------------------------------------
int __fastcall TdsDocPackPeriodForm::FGetPerSubType()
{
  if ((PerDataCB->ItemIndex != -1) && (PerTypeCB->ItemIndex > 1))
   {
     return PerDataCB->ItemIndex+1;
   }
  else
    return 0;
}
//---------------------------------------------------------------------------
int __fastcall TdsDocPackPeriodForm::FGetCreateType()
{
  if (CreateTypeCB->ItemIndex != -1)
   {
     return CreateTypeCB->ItemIndex+1;
   }
  else
    return 0;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocPackPeriodForm::FGetPerTypeStr()
{
  if (PerTypeCB->ItemIndex != -1)
   {
     return PerTypeCB->Properties->Items->Strings[PerTypeCB->ItemIndex];
   }
  else
    return "";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocPackPeriodForm::FGetPerSubTypeStr()
{
  if ((PerDataCB->ItemIndex != -1) && (PerTypeCB->ItemIndex > 1))
   {
     return PerDataCB->Properties->Items->Strings[PerDataCB->ItemIndex];
   }
  else
    return "";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocPackPeriodForm::FGetCreateTypeStr()
{
  if (CreateTypeCB->ItemIndex != -1)
   {
     return CreateTypeCB->Properties->Items->Strings[CreateTypeCB->ItemIndex];
   }
  else
    return "";
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocPackPeriodForm::FGetPerYear()
{
  return YearED->Text.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocPackPeriodForm::FGetPerDateFr()
{
  return DateFrDE->Text.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocPackPeriodForm::FGetPerDateTo()
{
  return DateToDE->Text.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocPackPeriodForm::FGetPerOffset()
{
  return ShiftED->Text.Trim();
}
//---------------------------------------------------------------------------
int __fastcall GetPeriodValue(int AType, int ASubType, UnicodeString AYear,
                              UnicodeString ADateFr, UnicodeString ADateTo,
                              UnicodeString AOffset,
                              int ADocPerType, TDate &ADocPerDateFr, TDate &ADocPerDateTo)
{
  int RC = -1; // �������������  - "year" 5, "halfyear 4; "quarter 3;  "month" 2;  "unique" 1;  ERR 0
  try
   {
     unsigned short FCY,FCM,FCD;
     int FQuart, FHalfYear;
     ADocPerDateTo.DecodeDate(&FCY, &FCM, &FCD);
     if (FCM < 7)
      {
        if (FCM < 4) FQuart = 1;
        else         FQuart = 2;
        FHalfYear = 1;
      }
     else
      {
        if (FCM < 10) FQuart = 3;
        else          FQuart = 4;
        FHalfYear = 2;
      }
     UnicodeString FYearStr;
     if (AYear.Length())  FYearStr = AYear;
     else                 FYearStr = IntToStr(FCY);
     try
      {
        switch (AType)
         {
           case 1:
           { // ��������� � ���
             RC = ADocPerType;
             break;
           }
           case 2:
           { // ��� // �������������  - "year" 5
             ADocPerDateFr = StrToDate("01.01."+FYearStr);
             ADocPerDateTo = StrToDate("31.12."+FYearStr);
             RC = 5;
             break;
           }
           case 3:
           { // ��������� // �������������  - "halfyear 4;
             if (ASubType > 1)
              { //��  "��������� ���"
                FHalfYear = ASubType - 1;
              }
             switch (FHalfYear)
              {
                case 1:
                { //"������");
                  ADocPerDateFr = StrToDate("01.01."+FYearStr);
                  ADocPerDateTo = StrToDate("30.06."+FYearStr);
                  break;
                }
                case 2:
                { //"������");
                  ADocPerDateFr = StrToDate("01.07."+FYearStr);
                  ADocPerDateTo = StrToDate("31.12."+FYearStr);
                  break;
                }
              }
             RC = 4;
             break;
           }
           case 4:
           { // ������� // �������������  - "quarter 3;
             if (ASubType > 1)
              { //��  "������� ���"
                FQuart = ASubType - 1;
              }
             switch (FQuart)
              {
                case 1:
                { //"������");
                  ADocPerDateFr = StrToDate("01.01."+FYearStr);
                  ADocPerDateTo = StrToDate("31.03."+FYearStr);
                  break;
                }
                case 2:
                { //"������");
                  ADocPerDateFr = StrToDate("01.04."+FYearStr);
                  ADocPerDateTo = StrToDate("30.06."+FYearStr);
                  break;
                }
                case 3:
                { //"������");
                  ADocPerDateFr = StrToDate("01.07."+FYearStr);
                  ADocPerDateTo = StrToDate("30.09."+FYearStr);
                  break;
                }
                case 4:
                { //"��������");
                  ADocPerDateFr = StrToDate("01.10."+FYearStr);
                  ADocPerDateTo = StrToDate("31.12."+FYearStr);
                  break;
                }
              }
             RC = 3;
             break;
           }
           case 5:
           { // ����� // �������������  - "month" 2;
             if (ASubType > 1)
              { //��  "����� ���"
                FCM = ASubType - 1;
              }
             UnicodeString FLastDay = "1";

             if (IsValidDate((Word)FYearStr.ToInt(), (Word)FCM, (Word)1))
              { // ��� � ����� ����������
                FLastDay = IntToStr(DaysInAMonth((Word)FYearStr.ToInt(), (Word)FCM));
              }

             if (FCM < 10)
              {
                ADocPerDateFr = StrToDate("01.0"+IntToStr(FCM)+"."+FYearStr);
                ADocPerDateTo = StrToDate(FLastDay+".0"+IntToStr(FCM)+"."+FYearStr);
              }
             else
              {
                ADocPerDateFr = StrToDate("01."+IntToStr(FCM)+"."+FYearStr);
                ADocPerDateTo = StrToDate(FLastDay+"."+IntToStr(FCM)+"."+FYearStr);
              }
             RC = 2;
             break;
           }
           case 6:
           { // �������� ��� // �������������  - "unique" 1;
             if (ASubType == 2)
              { //"��������� ��������";
                if (ADateFr.Length()) ADocPerDateFr = StrToDate(ADateFr);
                if (ADateTo.Length()) ADocPerDateTo = StrToDate(ADateTo);
              }
             RC = 1;
             break;
           }
         }
      }
     catch (...)
      {
         _MSG_ERRA(FMT(dsDocDocPeriodErrorYearFormat),FMT(dsDocCommonErrorCaption));
         throw;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------


