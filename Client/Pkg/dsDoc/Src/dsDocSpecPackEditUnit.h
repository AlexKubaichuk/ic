//---------------------------------------------------------------------------
#ifndef dsDocSpecPackEditUnitH
#define dsDocSpecPackEditUnitH
//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"
//---------------------------------------------------------------------------
//#include "XMLContainer.h"
#include "dsDocDMUnit.h"
//---------------------------------------------------------------------------
class PACKAGE TdsDocSpecPackEditForm : public TForm
{
__published:	// IDE-managed Components
        TActionList *MainAL;
        TAction *actAddSpec;
        TAction *actDeleteSpec;
        TdxBarManager *ListTBM;
        TImageList *MainIL;
        TPopupMenu *MainPM;
        TMenuItem *N1;
        TMenuItem *N2;
        TPanel *LeftPanel;
        TPanel *RightPanel;
        TPanel *InfoPanel;
        TLabel *CurLab;
        TPanel *Panel1;
        TLabel *ComLab;
        TdxBarButton *dxBarButton1;
        TAction *actApply;
        TPanel *Panel2;
        TcxButton *SaveBtn;
        TcxButton *CancelBtn;
        TcxTreeList *CurTL;
        TcxTreeList *ComTL;
        TAction *actCancel;
        TcxTreeListColumn *CurTLName;
        TcxTreeListColumn *CurTLSubType;
        TcxTreeListColumn *ComTLName;
        TcxTreeListColumn *ComTLModif;
        TcxTreeListColumn *CurTLGUI;
        TcxTreeListColumn *ComTLGUI;
        TcxImageList *LMainIL;
        TPanel *Panel3;
        TcxTextEdit *PackNameED;
        TLabel *Label1;
        TcxTreeListColumn *CurTLTypeStr;
        TcxTreeListColumn *CurTLSubTypeStr;
        TcxTreeListColumn *ComTLPeriod;
        TcxTreeListColumn *CurTLValue1;
        TcxTreeListColumn *CurTLValue2;
        TcxTreeListColumn *CurTLOffset;
        TcxTreeListColumn *CurTLType;
        TcxTreeListColumn *CurTLCreateType;
        TcxTreeListColumn *CurTLCreateTypeStr;
 TdxBarLargeButton *dxBarLargeButton1;
 TdxBarLargeButton *dxBarLargeButton2;
        void __fastcall CurTLDragDrop(TObject *Sender, TObject *Source, int X, int Y);
        void __fastcall CurTLDragOver(TObject *Sender, TObject *Source, int X, int Y, TDragState State, bool &Accept);
        void __fastcall ComTLDragDrop(TObject *Sender, TObject *Source, int X, int Y);
        void __fastcall ComTLDragOver(TObject *Sender, TObject *Source, int X, int Y, TDragState State, bool &Accept);
        void __fastcall actDeleteSpecExecute(TObject *Sender);
        void __fastcall actApplyExecute(TObject *Sender);
        void __fastcall actCancelExecute(TObject *Sender);
        void __fastcall actAddSpecExecute(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall PackNameEDPropertiesChange(TObject *Sender);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
private:	// User declarations
        TTagNode *FPackDef;
        void __fastcall FSetPackDef(TTagNode *ANode);

        bool FPackModif;
        bool FEdit;
        UnicodeString FSpecOwner;
        TdsDocDM *FDM;
//        TpFIBQuery *FQuery;

        void __fastcall FSave();
        bool __fastcall CanSpecDocRules(UnicodeString AGUI);
public:		// User declarations
        __fastcall TdsDocSpecPackEditForm(TComponent* Owner, UnicodeString ACaption, UnicodeString AOwnerCode, /*TpFIBQuery *AQuery, */bool AEdit, TdsDocDM *ADM);
        __property TTagNode* PackDef = {read=FPackDef, write=FSetPackDef};
        __property bool Modif = {read=FPackModif};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsDocSpecPackEditForm *dsDocSpecPackEditForm;
//---------------------------------------------------------------------------
#endif
