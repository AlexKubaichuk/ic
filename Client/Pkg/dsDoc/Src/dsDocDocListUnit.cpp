// ---------------------------------------------------------------------------
#include <vcl.h>
#include <System.SysUtils.hpp>
#include <System.DateUtils.hpp>
#pragma hdrstop
#include "dsDocDocListUnit.h"
#include "dsDocHandCreateTypeUnit.h"
// #include "dsDocWhoInDocListUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "dcDocHandEdit.h"
#include "dsDocDocClearDateUnit.h"
#include "pkgsetting.h"
#ifndef _CONFSETTTING
#error _CONFSETTTING using are not defined
#endif
// #include "DocCreator.h"
#include "dsDocDocPeriodUnit.h"
#include "msgdef.h"
#include "dsDocExtFunc.h"
#include "dsDocClientConstDef.h"
#include "dsDocFilterListUnit.h"
#include "dsDocGetCheckDocOrgUnit.h"
#include "dsDocExportDocUnit.h"
#include "dsDocImportDocUnit.h"
// ---------------------------------------------------------------------------
#define DocGUI         "000B"
#define DocSPECGUI     "0012"
#define DocNAME        "0013"
#define DocDOCTEXT     "0014"
#define DocF_OWNER     "0015"
#define DocF_SPECOWNER "0016"
#define DocF_PERIOD    "0017"
#define DocCREATEDATE  "0018"
#define DocF_FROMDATE  "0019"
#define DocF_TODATE    "001A"
#define DocINTEGRATED  "001B"
#define DocHANDCREATE  "001C"
#define DocMODULE_TYPE "001D"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxClasses"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxData"
#pragma link "cxDataStorage"
#pragma link "cxEdit"
#pragma link "cxFilter"
#pragma link "cxGraphics"
#pragma link "cxGrid"
#pragma link "cxGridCustomTableView"
#pragma link "cxGridCustomView"
#pragma link "cxGridLevel"
#pragma link "cxGridTableView"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxNavigator"
#pragma link "cxStyles"
#pragma link "dxBar"
#pragma link "dxStatusBar"
#pragma resource "*.dfm"
TdsDocListForm * dsDocListForm;
// ---------------------------------------------------------------------------
__fastcall TdsDocListForm::TdsDocListForm(TComponent * Owner, TdsDocDM * ADM, bool AHandEdit) : TForm(Owner)
 {
  FDM = ADM;
  if (AHandEdit)
   {
    actHandCreate->Visible        = FDM->DocHandEditable;
    actCreateForHandEdit->Visible = FDM->DocHandEditable;
    // actHandCreate->Visible    = FDM->DocHandEditable;
    actCheckByPeriod->Visible = FDM->DocHandEditable;
    // actCheckByPeriod->Visible = FDM->DocHandEditable;
   }
  FDataSrc = NULL;
  RootClass = FDM->RegDef->GetTagByUID("0002");
  // if (RootClass) RootClass = RootClass->GetFirstChild();
  FCtrList                  = new TdsRegEDContainer(this, ExtCompPanel);
  FCtrList->DefXML          = RootClass;
  FCtrList->StyleController = FDM->StyleController;
  FCtrList->DataProvider    = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DefXML = RootClass;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  FCtrList->DataPath                    = FDM->DataPath;
  // FLastTop = 0;
  // FLastHeight = 0;
  curCD = 0;
  CheckCtrlVisible();
  CreateSrc();
#ifndef _CONFSETTTING
#error _CONFSETTTING using are not defined
#endif
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmDocList"); // # ������ ����������.
#endif
  FLoadChoice();
  FLoadSpec();
  SpecCBChange(SpecCB);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::CreateSrc()
 {
  Application->ProcessMessages();
  try
   {
    try
     {
      Caption                           = RootClass->AV["name"];
      FSaveCaption                      = RootClass->AV["name"];
      FDataSrc                          = new TkabCustomDataSource(RootClass);
      FDataSrc->DataSet->OnGetCount     = FDM->kabCDSGetCount;
      FDataSrc->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
      FDataSrc->DataSet->OnGetValById   = FDM->kabCDSGetValById;
      FDataSrc->DataSet->OnGetValById10 = FDM->kabCDSGetValById10;
      FDataSrc->DataSet->OnInsert       = FDM->kabCDSInsert;
      FDataSrc->DataSet->OnEdit         = FDM->kabCDSEdit;
      FDataSrc->DataSet->OnDelete       = FDM->kabCDSDelete;
      FDataSrc->SetColumns(DocListView, "0013:������������\n0017:������\n0015:�����\n0018:���� ��������");
      DocListView->DataController->CustomDataSource = FDataSrc;
      ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DataSource = FDataSrc;
     }
    catch (Exception & E)
     {
      _MSG_ERR(FMT1(icsErrorMsgCaptionSys, E.Message), FMT(icsErrorMsgCaption));
     }
    catch (...)
     {
      _MSG_ERR(FMT(icsRegistryErrorCommDBErrorMsgCaption), FMT(icsErrorMsgCaption));
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::ChangeSrc()
 {
  CheckCtrlState(false);
  TTime FBegTime = Time();
  UnicodeString _ES_ = "";
  // UnicodeString TimeMsg = "";
  Application->ProcessMessages();
  try
   {
    try
     {
      DocListView->BeginUpdate();
      try
       {
        FDataSrc->DataSet->GetData(NULL, TdsRegFetch::Auto, 20, FGetDataProgress);
        FBegTime = Time();
        FDataSrc->DataChanged();
       }
      __finally
       {
        DocListView->EndUpdate();
        MsgLab->Caption = MsgLab->Caption + " ���������: " + (Time() - FBegTime).FormatString("ss.zzz");
       }
      if (curCD.Length())
       {
        TLocateOptions opt;
        // if(!quClass()->Locate("CODE",Variant(curCD),opt))
        // quClass()->First();
       }
      // else
      // quClass()->First();
      RootClass->Iterate(CreateListExt, _ES_);
     }
    catch (Exception & E)
     {
      _MSG_ERR(FMT1(icsErrorMsgCaptionSys, E.Message), FMT(icsErrorMsgCaption));
     }
    catch (...)
     {
      _MSG_ERR(FMT(icsRegistryErrorCommDBErrorMsgCaption), FMT(icsErrorMsgCaption));
     }
   }
  __finally
   {
    CheckCtrlState(true);
    Application->ProcessMessages();
    ActiveControl = DocList;
    // _MSG_DBG(TimeMsg);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::FGetDataProgress(int AMax, int ACur, UnicodeString AMsg)
 {
  if (AMax == -1)
   {
    PrBar->Position = 0;
    MsgLab->Caption = "";
   }
  else if (!(AMax + ACur))
   {
    PrBar->Position = 0;
    MsgLab->Caption = AMsg;
   }
  else
   {
    MsgLab->Caption = AMsg + " [" + IntToStr(ACur) + "/" + IntToStr(AMax) + "]";
    if (PrBar->Max != AMax)
     PrBar->Max = AMax;
    PrBar->Position = ACur;
   }
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::actPreviewExecute(TObject * Sender)
 {
  MsgLab->Caption = "������������ ���������������� ���������";
  FDM->PreviewDocument(VarToStr(CurRow()->Value[DocGUI]), CheckProgress);
  MsgLab->Caption = "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::FormClose(TObject * Sender, TCloseAction & Action)
 {
  DocListView->DataController->CustomDataSource = NULL;
  // FDM->quDoc->AfterScroll = NULL;
  // delete Spec;
  // delete FDocCondRules;
  // delete Doc;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::actDeleteDocExecute(TObject * Sender)
 {
  try
   {
    // if(FDM->GetCount("DOCUMENTS",FDM->quDoc->Filter) < 1)
    // return;
    try
     {
      if (_MSG_QUE(FMT2(dsDocCommonDelConfMsg, FMT(dsDocDocListDocCaption), "\"" + CurRow()->Value[DocNAME] + "\""),
        FMT(dsDocCommonConfRecDelCaption)) == IDYES)
       {
        /* delCode = FDM->quDocGUI->AsString;
         FDM->quDoc->Next();
         curCD = FDM->quDocGUI->AsString;
         TLocateOptions opt;
         FDM->quDoc->Locate("GUI",Variant(delCode),opt);
         if (!FDM->trDoc->Active) FDM->trDoc->StartTransaction(); */
        FDataSrc->DataSet->Delete();
        ChangeSrc();
       }
     }
    catch (Exception & E)
     {
      _MSG_ERR(FMT1(dsDocCommonErrorDelMsg, FMT(dsDocDocListDocCaption)) + "\n" + E.Message,
        FMT(dsDocCommonErrorCaption));
      FDataSrc->DataSet->Cancel();
     }
   }
  __finally
   {
   }
  ActiveControl = DocList;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_ESCAPE)
   {
    Close();
    Key = 0;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::FormDestroy(TObject * Sender)
 {
  DocListView->DataController->CustomDataSource = NULL;
  delete FCtrList->DataProvider;
  delete FCtrList;
  FCtrList = NULL;
  delete FDataSrc;
  FDataSrc = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::actHandCreateExecute(TObject * Sender)
 {
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmSpecListHandEdit"); // # ������ �������� ����������. ������ �������������� ���������.
#else
  if (FDM->DocHandEditable)
   {
    TTagNode * FSpec = NULL;
    TTagNode * FDoc = NULL;
    UnicodeString FSQL;
    TReplaceFlags repF;
    repF << rfReplaceAll;
    int FSpecType;
    try
     {
      FSpec = new TTagNode(NULL);
      FDoc  = new TTagNode(NULL);
      UnicodeString FDocGUI;
      if (FDM->GetSpec(VarToStr(CurRow()->Value[DocSPECGUI]), FSpec, FSpecType, "������ �������� �������� ���������."))
       {
        FDocGUI     = VarToStr(CurRow()->Value[DocGUI]);
        FDoc->AsXML = FDM->GetDocById(FDocGUI);
        TdsDocHandEditForm * Dlg = new TdsDocHandEditForm(this, FSpec, FDoc, FDM->XMLList);
        try
         {
          // Dlg->actReCalc->Visible = true;
          Dlg->ShowModal();
          if (Dlg->Modified)
           { // ��������� �������� � ������������� ������� ������ �����������
            curCD = CurRow()->Value[DocGUI];
            FDM->SetTextData(ctDoc, DocDOCTEXT, curCD, FDoc->AsXML);
           }
         }
        __finally
         {
          delete Dlg;
         }
       }
     }
    __finally
     {
      if (FSpec)
       delete FSpec;
      if (FDoc)
       delete FDoc;
     }
    ActiveControl = DocList;
   }
#endif
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::actCreateForHandEditExecute(TObject * Sender)
 {
#ifdef _DEMOMODE
  FDM->DocDemoMessage("ddmSpecListHandEdit"); // # ������ �������� ����������. ������ �������������� ���������.
#else
  if (FDM->DocHandEditable)
   {
    if (!FGetSpecGUI().Trim().Length())
     {
      _MSG_INF("��� ������������ ��������� ���������� ������� ��������.", "���������");
     }
    else
     {
      TTagNode * FSpec = NULL;
      TTagNode * FDoc = NULL;
      UnicodeString FSQL;
      TReplaceFlags repF;
      repF << rfReplaceAll;
      int FSpecType;
      TdsDocCreateTypeForm * DlgCT = new TdsDocCreateTypeForm(this, FDM);
      try
       {
        FSpec = new TTagNode(NULL);
        FDoc  = new TTagNode(NULL);
        if (DlgCT->ShowModal() == mrOk)
         {
          UnicodeString FDocGUI;
          // FSpec->AsXML = FDM->GetSpecTxt(CurRow()->Value[SpecGUI]);
          if (FDM->GetSpec(FGetSpecGUI(), FSpec, FSpecType, "������ �������� �������� ���������.")) // !!!
           {
            FDocGUI = FDM->InternalCreateDocument(CheckProgress, FSpec, 0, "", (FSpecType == 11), DlgCT->OrgCode);
            if (!SameText(FDocGUI, "cancel") && FDocGUI.Length())
             {
              FDoc->AsXML = FDM->GetDocById(FDocGUI);
              TdsDocHandEditForm * Dlg = new TdsDocHandEditForm(this, FSpec, FDoc, FDM->XMLList);
              try
               {
                // Dlg->actReCalc->Visible = true;
                Dlg->ShowModal();
                if (Dlg->Modified && GetPassport(FSpec)->CmpAV("save_bd", "1"))
                 { // ��������� �������� � ������������� ������� ������ �����������
                  curCD = FDocGUI;
                  FDM->SetTextData(ctDoc, DocDOCTEXT, curCD, FDoc->AsXML);
                  ChangeSrc();
                 }
               }
              __finally
               {
                delete Dlg;
               }
             }
           }
         }
       }
      __finally
       {
        if (FSpec)
         delete FSpec;
        if (FDoc)
         delete FDoc;
        delete DlgCT;
       }
      ActiveControl = DocList;
     }
   }
#endif
 }
// ---------------------------------------------------------------------------
/*
 void __fastcall TDocListFormEx::FormConstrainedResize(TObject *Sender,
 int &MinWidth, int &MinHeight, int &MaxWidth, int &MaxHeight)
 {
 DOC_NAME->Width = DocList->Width - DocListDBTableView1F_PERIOD->Width - ::GetSystemMetrics(SM_CXVSCROLL)-6;
 }
 //---------------------------------------------------------------------------
 */
void __fastcall TdsDocListForm::DocListDBTableView1TcxGridDBDataControllerTcxDataSummaryDefaultGroupSummaryItems0GetText
  (TcxDataSummaryItem * Sender, const Variant & AValue, bool AIsFooter, UnicodeString & AText)
 {
  if (!(AValue.IsNull() || AValue.IsEmpty()))
   AText = "���������� ����������: " + VarToWideStrDef(AValue, "");
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::actCheckByPeriodExecute(TObject * Sender)
 {
  FMissingSpecDoc = new TStringList;
  TTagNode * FPackDef = new TTagNode(NULL);
  UnicodeString OrgName;
  int FSpecType;
  int FMisCount;
  TdsDocGetCheckDocOrgForm * Dlg = new TdsDocGetCheckDocOrgForm(this, FDM);
  try
   {
    Dlg->ShowModal();
    if (Dlg->ModalResult == mrOk)
     {
      FDM->HTMLPreview(FDM->kabCDSCheckByPeriod(Dlg->CreateByType, (Dlg->CreateByType) ? Dlg->OrgType : Dlg->OrgCode,
        FSpecGUIMap[SpecCB->ItemIndex]));
     }
   }
  __finally
   {
    delete Dlg;
    delete FMissingSpecDoc;
    FMissingSpecDoc = NULL;
    delete FPackDef;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::actClearExecute(TObject * Sender)
 {
  TdsDocDocClearDateForm * Dlg = new TdsDocDocClearDateForm(this);
  try
   {
    Dlg->ShowModal();
    if (Dlg->ModalResult == mrOk)
     {
      FDM->kabCDSDocListClearByDate(Dlg->ToDate);
      ChangeSrc();
      ActiveControl = DocList;
     }
   }
  __finally
   {
    delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::actCreateDocExecute(TObject * Sender)
 {
  if (FDM->DocEditable)
   {
    if (!FGetSpecGUI().Trim().Length())
     {
      _MSG_INF("��� ������������ ��������� ���������� ������� ��������.", "���������");
     }
    else
     {
      TTagNode * FSpec = NULL;
      TTagNode * FDoc = NULL;
      TReplaceFlags repF;
      repF << rfReplaceAll;
      UnicodeString FSQL;
      int FSpecType;
      try
       {
        FltCB->Enabled = false;
        UnicodeString FltCode = "";
        bool FNeedRefresh = false;
        FSpec = new TTagNode(NULL);
        FDoc  = new TTagNode(NULL);
        if (FDM->GetSpec(FGetSpecGUI(), FSpec, FSpecType, "������ �������� �������� ���������."))
         {
          if (FDM->FilterEnabled)
           {
            FltCode = IntToStr(FGetFilterCode());
           }
          if (FGetSpecType() == 10)
           { // �����
            FDM->CreatePackage(FSpec, FltCode);
           }
          else
           {
            UnicodeString FDocGUI = FDM->InternalCreateDocument(CheckProgress, FSpec, 1, FltCode,
              (FGetSpecType() == 11), FDM->GetThisSpecOwner());
            if (!FDocGUI.Length())
             _MSG_ERR("������ ������������ ���������", "������");
            else
             {
              FDM->SaveDocument(FDocGUI); // !!! �������������?
              MsgLab->Caption = "������������ ���������������� ���������";
              FDM->PreviewDocument(FDocGUI, CheckProgress);
             }
           }
         }
       }
      __finally
       {
        FltCB->Enabled = true;
        if (FSpec)
         delete FSpec;
        if (FDoc)
         delete FDoc;
        ChangeSrc();
       }
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::actCreateDocByPeriodExecute(TObject * Sender)
 {
  if (FDM->DocEditable)
   {
    TTagNode * FSpec = NULL;
    TTagNode * FDoc = NULL;
    TReplaceFlags repF;
    repF << rfReplaceAll;
    UnicodeString FSQL;
    int FSpecType;
    try
     {
      FltCB->Enabled = false;
      UnicodeString FltCode = "";
      bool FNeedRefresh = false;
      FSpec = new TTagNode(NULL);
      FDoc  = new TTagNode(NULL);
      if (FDM->GetSpec(FGetSpecGUI(), FSpec, FSpecType, "������ �������� �������� ���������."))
       {
        if (FDM->FilterEnabled)
         {
          FltCode = IntToStr(FGetFilterCode());
         }
        if (FSpecType == 10)
         { // �����
          FDM->CreatePackage(FSpec, FltCode, false);
         }
        else
         {
          UnicodeString FDocGUI = FDM->InternalCreateDocument(CheckProgress, FSpec, 1, FltCode, (FSpecType == 11),
            FDM->GetThisSpecOwner());
          if (!FDocGUI.Length()) // !!!
             _MSG_ERR("������ ������������ ���������", "������");
          else
           FDM->SaveDocument(FDocGUI); // !!! �������������?
         }
       }
     }
    __finally
     {
      FltCB->Enabled = true;
      if (FSpec)
       delete FSpec;
      if (FDoc)
       delete FDoc;
      ChangeSrc();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::actChoiceLibOpenExecute(TObject * Sender)
 {
  if (FDM->DocEditable && FDM->FilterEnabled && /* FDM->AppOpt->Vals["rtDocLibEdit"].AsBoolDef(false) && */
    (FDM->DefDocCreateType == dctBaseDoc))
   {
    TdsDocFilterListForm * Dlg = NULL;
    try
     {
      Dlg = new TdsDocFilterListForm(this, "���������� ��������", 0, FDM);
      Dlg->ShowModal();
      FLoadChoice();
     }
    __finally
     {
      if (Dlg)
       delete Dlg;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::FLoadChoice()
 {
  if (FDM->DocEditable && FDM->FilterEnabled /* && FDM->AppOpt->Vals["rtDocLibEdit"].AsBoolDef(false) */ &&
    (FDM->DefDocCreateType == dctBaseDoc))
   {
    try
     {
      // FltCB->Items->Add("");
      FDM->FillClass(FltCB->Items, "",
        "Select CODE as ps1, NAME as ps2 From FILTERS Where CHTYPE = 0 and Upper(MODULE_TYPE)='" +
        FDM->SubDir.UpperCase() + "' Order By NAME");
      /*
       FltCB->Items->Clear();
       FDM->qtExecDoc("Select CODE, NAME From FILTERS Where CHTYPE = 0 and MODULE_TYPE='"+FDM->SubDir+"' Order By NAME", false);
       FltCB->Items->Add("");
       while (!FDM->quFreeDoc->Eof)
       {
       FltCB->Items->AddObject(FDM->quFreeDoc->Value("NAME")->AsString.c_str(),(TObject*)FDM->quFreeDoc->Value("CODE")->AsInteger);
       FDM->quFreeDoc->Next();
       }
       FDM->qtDocCommit();
       */
     }
    __finally
     {
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::FLoadSpec()
 {
  FSpecGUIMap.clear();
  SpecCB->Items->Clear();
  try
   {
    TJSONObject * RetData;
    TJSONObject * tmpPair;
    TJSONString * tmpData;
    UnicodeString FClCode, FClStr1, FClStr2;
    RetData = FDM->kabCDSGetValById
      ("doc.s3.Select GUI as ps1, NAME as ps2, SPECTYPE as ps3 From SPECIFIKATORS Where SPECTYPE <> 5 Order By SPECTYPE DESC, NAME",
      0);
    TJSONPairEnumerator * itPair = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
      FClStr1 = ((TJSONString *)itPair->Current->JsonValue)->Value();
      FClStr2 = GetRPartB(FClStr1, ':');
      FClStr1 = GetLPartB(FClStr1, ':');
      SpecCB->Items->AddObject(FClStr1, (TObject *)FClStr2.ToIntDef(0));
      FSpecGUIMap[SpecCB->Items->Count - 1] = FClCode;
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsDocListForm::FGetSpecGUI()
 {
  UnicodeString RC = "";
  try
   {
    TIntMap::iterator FRC = FSpecGUIMap.find(SpecCB->ItemIndex);
    if (FRC != FSpecGUIMap.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsDocListForm::FGetSpecType()
 {
  int RC = -1;
  try
   {
    if (SpecCB->ItemIndex != -1)
     RC = (int)SpecCB->Items->Objects[SpecCB->ItemIndex];
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsDocListForm::FGetFilterCode()
 {
  int RC = -1;
  try
   {
    if (FltCB->ItemIndex != -1)
     RC = (int)FltCB->Items->Objects[FltCB->ItemIndex];
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::SpecCBChange(TObject * Sender)
 {
  CheckCtrlState(true);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::DocListViewTcxGridDataControllerTcxDataSummaryFooterSummaryItems0GetText
  (TcxDataSummaryItem * Sender, const Variant & AValue, bool AIsFooter, UnicodeString & AText)
 {
  if (!(AValue.IsNull() || AValue.IsEmpty()))
   AText = "���������� ����������: " + VarToWideStrDef(AValue, "");
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::CheckCtrlState(bool AEnable)
 {
  actPreview->Enabled           = false;
  actDeleteDoc->Enabled         = false;
  actExportDoc->Enabled         = false;
  actImportDoc->Enabled         = false;
  actClear->Enabled             = false;
  actCreateDoc->Enabled         = false;
  actCreateDocByPeriod->Enabled = false;
  actHandCreate->Enabled        = false;
  actCreateForHandEdit->Enabled = false;
  actCheckByPeriod->Enabled     = false;
  if (DocListView->DataController->CustomDataSource && FDataSrc)
   {
    if (FDataSrc->DataSet->RecordCount && IsRecSelected())
     {
      actPreview->Enabled    = true;
      actDeleteDoc->Enabled  = FDM->DocEditable;
      actExportDoc->Enabled  = FDM->DocEditable;
      actClear->Enabled      = FDM->DocEditable;
      actHandCreate->Enabled = FDM->DocHandEditable && actHandCreate->Visible && FDM->CanHandEdit
        (CurRow()->Value[DocSPECGUI]);
     }
    actCreateDocByPeriod->Enabled = FDM->DocEditable;
    actImportDoc->Enabled = FDM->DocEditable;
   }
  if (SpecCB->ItemIndex != -1)
   {
    actCreateDoc->Enabled         = FDM->DocEditable;
    actCreateForHandEdit->Enabled = FDM->DocHandEditable && actCreateForHandEdit->Visible && FDM->CanHandEdit
      (FGetSpecGUI());
    actCreateDocByPeriod->Enabled = true;
    actCheckByPeriod->Enabled     = (FGetSpecType() == 10);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::CheckCtrlVisible()
 {
  if (FDM->DocEditable && FDM->FilterEnabled && (FDM->DefDocCreateType == dctBaseDoc))
   {
    FltCB->Enabled            = true;
    FltCB->Visible            = ivAlways;
    actChoiceLibOpen->Enabled = true;
    actChoiceLibOpen->Visible = true;
   }
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocListForm::CreateListExt(TTagNode * itTag, UnicodeString & UID)
 {
  TdsRegLabItem * tmpItem;
  if (!itTag->CmpName("class,description,actuals"))
   {
    if (itTag->CmpAV("inlist", "e"))
     {
      tmpItem      = FCtrList->AddLabItem(itTag, ExtCompPanel->Width, ExtCompPanel);
      tmpItem->Top = xcTop;
      xcTop += tmpItem->Height;
      // if (itTag->CmpName("extedit"))
      // tmpItem->OnGetExtData = FDM->OnGetExtData;
     }
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::ShowTimerTimer(TObject * Sender)
 {
  ShowTimer->Enabled = false;
  Application->ProcessMessages();
  ChangeSrc();
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::FormShow(TObject * Sender)
 {
  DocListView->DataController->CustomDataSource = FDataSrc;
  ShowTimer->Enabled                            = true;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocListForm::IsRecSelected()
 {
  return DocListView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::DocListViewFocusedRecordChanged(TcxCustomGridTableView * Sender,
  TcxCustomGridRecord * APrevFocusedRecord, TcxCustomGridRecord * AFocusedRecord, bool ANewItemRecordFocusingChanged)
 {
  if (IsRecSelected())
   FDataSrc->DataSet->GetDataById(VarToStr(FDataSrc->DataSet->CurRow->Value[DocGUI]), FGetDataProgress);
  CheckCtrlState(true);
  // FCtrList->LabDataChange();
 }
// ---------------------------------------------------------------------------
TkabCustomDataSetRow * __fastcall TdsDocListForm::CurRow()
 {
  TkabCustomDataSetRow * RC = NULL;
  try
   {
    if (FDataSrc)
     RC = FDataSrc->DataSet->CurRow;
    else
     throw Exception(FMT(dsDocDocSpecListNoDataSrc));
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TkabCustomDataSetRow * __fastcall TdsDocListForm::EditedRow()
 {
  TkabCustomDataSetRow * RC = NULL;
  try
   {
    if (FDataSrc)
     if (FDataSrc->DataSet->EditedRow)
      RC = FDataSrc->DataSet->EditedRow;
    if (!RC)
     throw Exception(FMT(dsDocDocSpecListNoDataSrc));
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::RefreshById(UnicodeString ACode)
 {
  DocListView->BeginUpdate();
  FDataSrc->DataSet->GetDataById(ACode, FGetDataProgress);
  FDataSrc->DataChanged();
  StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
  // __int64 FCodeIdx = FDataSrc->DataSet->GetRowIdxByCode(ACode);
  // DocListView->DataController->FocusedRowIndex = FDataSrc->DataController->GetRowIndexByRecordIndex(FCodeIdx, true);
  DocListView->EndUpdate();
  DocListView->DataController->FocusedRecordIndex = FDataSrc->DataSet->GetRowIdxByCode(ACode);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::actClearFilterCBExecute(TObject * Sender)
 {
  FltCB->ItemIndex = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::actClearSpecCBExecute(TObject * Sender)
 {
  SpecCB->ItemIndex = -1;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsDocListForm::CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg)
 {
  switch (AType)
   {
   case TkabProgressType::InitCommon:
     {
      PrBar->Max = AVal;
      break;
     }
   case TkabProgressType::StageInc:
     {
      MsgLab->Caption = AMsg;
      break;
     }
   case TkabProgressType::CommInc:
     {
      Caption         = FSaveCaption + "  [" + AMsg + "]";
      PrBar->Position = AVal;
      break;
     }
   case TkabProgressType::CommComplite:
     {
      MsgLab->Caption = "";
      PrBar->Position = 0;
      Caption         = FSaveCaption;
      break;
     }
   }
  return true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::actExportDocExecute(TObject * Sender)
 {
  TdsDocExportDocForm * Dlg = new TdsDocExportDocForm(Application->Owner, FDM, false, true);
  try
   {
    Dlg->ShowModal();
   }
  __finally
   {
    delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::actImportDocExecute(TObject * Sender)
 {
  TdsDocImportDocForm * Dlg = new TdsDocImportDocForm(Application->Owner, FDM, false, true);
  try
   {
    Dlg->ShowModal();
   }
  __finally
   {
    delete Dlg;
    ChangeSrc();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsDocListForm::actRefreshExecute(TObject * Sender)
 {
  ChangeSrc();
 }
// ---------------------------------------------------------------------------
