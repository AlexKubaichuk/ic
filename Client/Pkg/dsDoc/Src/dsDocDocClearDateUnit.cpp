//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsDocDocClearDateUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxCalendar"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDateUtils"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "dxCore"
#pragma resource "*.dfm"
TdsDocDocClearDateForm *dsDocDocClearDateForm;
//---------------------------------------------------------------------------
__fastcall TdsDocDocClearDateForm::TdsDocDocClearDateForm(TComponent* Owner)
        : TForm(Owner)
{
  DateToDE->Date = Dateutils::IncYear(Sysutils::IncMonth(Date(), -6),-1);
}
//---------------------------------------------------------------------------
TDate __fastcall TdsDocDocClearDateForm::FGetDate()
{
  return DateToDE->Date;
}
//---------------------------------------------------------------------------
