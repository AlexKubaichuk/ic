//---------------------------------------------------------------------------

#include <System.hpp>
#pragma hdrstop
USEFORM("..\Src\dsDocDMUnit.cpp", dsDocDM); /* TDataModule: File Type */
USEFORM("..\Src\dsDocDistributionList.cpp", ExtDistForm);
USEFORM("..\Src\dsDocDocClearDateUnit.cpp", dsDocDocClearDateForm);
USEFORM("..\Src\dsDocDocListUnit.cpp", dsDocListForm);
USEFORM("..\Src\dsDocDocCreateTypeUnit.cpp", dsDocDocCreateTypeForm);
USEFORM("..\Src\dsDocDocPeriodUnit.cpp", dsDocPeriodForm);
USEFORM("..\Src\dsDocSubOrgEditUnit.cpp", dsDocSubOrgEditForm);
USEFORM("..\Src\dsDocSpecPackEditUnit.cpp", dsDocSpecPackEditForm);
USEFORM("..\Src\dsDocSpecListUnit.cpp", dsDocSpecListForm);
USEFORM("..\Src\dsDocSubOrgListUnit.cpp", dsDocSubOrgForm);
USEFORM("..\Src\specedit\dsDocSpecEditUnit.cpp", dsDocSpecEditForm);
USEFORM("..\Src\Hand\dcDocHandEdit.cpp", dsDocHandEditForm);
USEFORM("..\Src\dsDocPackPeriodUnit.cpp", dsDocPackPeriodForm);
USEFORM("..\Src\dsDocFilterListUnit.cpp", dsDocFilterListForm);
USEFORM("..\Src\dsDocExportDocUnit.cpp", dsDocExportDocForm);
USEFORM("..\Src\dsDocGetCheckDocOrgUnit.cpp", dsDocGetCheckDocOrgForm);
USEFORM("..\Src\dsDocImportDocUnit.cpp", dsDocImportDocForm);
USEFORM("..\Src\dsDocHandCreateTypeUnit.cpp", dsDocCreateTypeForm);
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Package source.
//---------------------------------------------------------------------------


#pragma argsused
extern "C" int _libmain(unsigned long reason)
{
 return 1;
}
//---------------------------------------------------------------------------
