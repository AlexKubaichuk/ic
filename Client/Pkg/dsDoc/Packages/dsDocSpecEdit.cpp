//---------------------------------------------------------------------------

#include <System.hpp>
#pragma hdrstop
USEFORM("..\Src\specedit\DocElSizeEdit.cpp", fmDocElSizeEdit);
USEFORM("..\Src\specedit\Count.cpp", fmCount);
USEFORM("..\Src\specedit\AddTable.cpp", fmAddTable);
USEFORM("..\Src\specedit\DocRulesEdit.cpp", fmDocRulesEdit);
USEFORM("..\Src\specedit\DurationEdit.cpp", DurationEditFM);
USEFORM("..\Src\specedit\AddList.cpp", fmAddList);
USEFORM("..\Src\specedit\RulesExprEdit.cpp", fmRulesExprEdit);
USEFORM("..\Src\specedit\PeriodEdit.cpp", PeriodEditFM);
USEFORM("..\Src\specedit\ParametersEdit.cpp", fmParametersEdit);
USEFORM("..\Src\specedit\SEPageSetup.cpp", SEPageSetupForm);
USEFORM("..\Src\specedit\StyleNameEdit.cpp", fmStyleEdit);
USEFORM("..\Src\specedit\SetRef.cpp", fmSetRef);
USEFORM("..\Src\specedit\ObjRulesEdit.cpp", fmObjRulesEdit);
USEFORM("..\Src\specedit\ObjRulesAssocEdit.cpp", fmObjRulesAssocEdit);
USEFORM("..\Src\specedit\ListSortOrder.cpp", fmListSortOrder);
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Package source.
//---------------------------------------------------------------------------


#pragma argsused
extern "C" int _libmain(unsigned long reason)
{
 return 1;
}
//---------------------------------------------------------------------------
