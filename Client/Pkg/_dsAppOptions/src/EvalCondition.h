//---------------------------------------------------------------------------

#include <SysUtils.hpp>
//#include <dstring.h>
#include <map>
#include <AxeUtil.h>
#ifndef EvalConditionH
#define EvalConditionH
//---------------------------------------------------------------------------
class variable;
using namespace std;
enum operationType {NONE,LT,GT,LE,GE,EQ,NotEQ,AND,OR};

typedef map <UnicodeString, UnicodeString> TCondVarsMap;
typedef void __fastcall (__closure *TUnresolvedResult)(variable* firstVar, variable* secondVar, operationType type, bool &result);

const UnicodeString undefinedValue = "$0m3c4@ZyV@1u3";


class TEvalCond
{
private:
    TUnresolvedResult FUnresolvedResult;
    void __fastcall shrinkToResult     ( TStringList* lexems, int index, bool result );

    void __fastcall getLexems         ( UnicodeString expression, TStringList* lexems);
    void __fastcall getVars           ( TStringList* lexems, TCondVarsMap vars );
    bool __fastcall realEvaluate      ( TStringList* lexems );

    int  __fastcall expandParenthesis ( TStringList* lexems );
    void __fastcall expandArrows      ( TStringList* lexems );
    void __fastcall expandEQ          ( TStringList* lexems );
    void __fastcall expandNotEQ       ( TStringList* lexems );
    void __fastcall expandAND         ( TStringList* lexems );
    void __fastcall expandOR          ( TStringList* lexems );

    void __fastcall delObj            ( int index, TStringList* lexems );
    void __fastcall compare           ( int index, operationType type, TStringList* lexems );
protected:

public:
    __fastcall TEvalCond () {FUnresolvedResult = NULL;};

    bool evalCondition (const UnicodeString condition, const TCondVarsMap variablesMap);
    __property TUnresolvedResult OnUnresolvedResult = {read = FUnresolvedResult, write = FUnresolvedResult};
};

class TEvalCondError : public System::Sysutils::Exception
{
public:
    __fastcall TEvalCondError (const UnicodeString Msg) : System::Sysutils::Exception (Msg) {};
};

//-----------------------------------------------------------------------------
//           ����� ��������
//-----------------------------------------------------------------------------

enum varType {tNotVar, tUndefined, tDigit, tString};

class variable : public TObject
{
 private:
     friend        TEvalCond;
     double        digitVal;
     UnicodeString    strVal;
     varType       vType;
     Variant    __fastcall getVar ();
     void       __fastcall setVar ( Variant value );
     Double     __fastcall getDblVal ();
     UnicodeString __fastcall getStrVal ();
 public:
     __fastcall variable ();
     __fastcall variable (varType type, Variant value);
     __property Variant var = {read = getVar, write = setVar};
     __property Double asFloat       = { read = getDblVal };
     __property UnicodeString asString  = { read = getStrVal };
     __property varType variableType = { read = vType};

};

class TvariableError : public System::Sysutils::Exception
{
public:
    __fastcall TvariableError (const UnicodeString Msg) : System::Sysutils::Exception (Msg) {};
};
#endif
