#ifndef AppOptDialogCompsH
#define AppOptDialogCompsH

#include <SysUtils.hpp>
#include <Classes.hpp>
//#include <cxTL.hpp>
//#include <cxPC.hpp>
#include <cxCheckBox.hpp>
#include <cxButtonEdit.hpp>
#include <cxLabel.hpp>
//#include <cxCurrencyEdit.hpp>
#include <cxCalendar.hpp>
#include <cxRadioGroup.hpp>
//#include <cxGroupBox.hpp>
//#include <dxStatusBar.hpp>
//#include <cxSplitter.hpp>
//#include <cxControls.hpp>
//#include <cxStyles.hpp>
#include <AxeUtil.h>
//**************************************************************************
//                                                                         *
//                         "���" ��������� ��������                        *
//                                                                         *
//**************************************************************************
typedef void __fastcall (__closure *TTextResize)(TObject* Sender);

//---------------------------------------------------------------------------
class AOCheckBox : public TcxCheckBox
{
public:
    TTagNode* node;
    __fastcall AOCheckBox (TWinControl* AOwner, TRect rect);
};

//---------------------------------------------------------------------------
class AOLabelEditControl : public TControl
{
private:
    int labelWidth;
    int editWidth;
    UnicodeString getCaption();
    void setCaption (UnicodeString);
    void setLabelWidth (int);
    void setEditWidth  (int);
    virtual void __fastcall changeRects(TObject* Sender) =0;
public:
    TcxLabel*      label;
    __property int labelWidthPercent = {read = labelWidth, write = setLabelWidth};
    __property int editWidthPercent  = {read = editWidth , write = setEditWidth};
    __property UnicodeString caption    = {read = getCaption ,write = setCaption};

    virtual __fastcall AOLabelEditControl (TWinControl* AOwner, TRect rect) : TControl(AOwner) {};
    virtual __fastcall ~AOLabelEditControl () {};
};

// ������� ���� � ����������� ����������� ��� ����
class AOButtEdit : public TcxButtonEdit
{
public:
    TTagNode* node;
    AOButtEdit (TWinControl* AOwner) : TcxButtonEdit(AOwner) {};
};

// ������ ���� ��� ������������� ���������� "���������"
class AOButtonEdit : public AOLabelEditControl
{
private:
    void __fastcall changeRects(TObject* Sender);
public:
    AOButtEdit* edit;

    __fastcall AOButtonEdit (TWinControl* AOwner, TRect rect);
    __fastcall ~AOButtonEdit ();
};

//---------------------------------------------------------------------------
// ���� � ���� � ����������� ����������� ��� ����
class AODEdit : public TcxDateEdit
{
public:
    TTagNode* node;
    __fastcall AODEdit (TWinControl* AOwner) : TcxDateEdit (AOwner) {};
};

//---------------------------------------------------------------------------

class AODateEdit : public AOLabelEditControl
{
private:
    void __fastcall changeRects(TObject* Sender);
public:
    AODEdit* edit;
    __fastcall AODateEdit (TWinControl* AOwner, TRect rect);
    __fastcall ~AODateEdit();
};

//---------------------------------------------------------------------------

class AORadioGroup : public TcxRadioGroup
{
public:
    TTagNode* node;
    __fastcall AORadioGroup (TWinControl* AOwner) : TcxRadioGroup (AOwner) {};
};

//---------------------------------------------------------------------------

class AOButton : public TcxButton
{
public:
    TTagNode* node;
    __fastcall AOButton (TWinControl* AOwner) : TcxButton (AOwner) {};
};

//---------------------------------------------------------------------------

#endif
