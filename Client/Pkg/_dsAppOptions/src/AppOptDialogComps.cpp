//---------------------------------------------------------------------------


#pragma hdrstop

#include "AppOptDialogComps.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//****************************************************************************
//                             AOCheckBox                                    *
//****************************************************************************
__fastcall AOCheckBox::AOCheckBox (TWinControl* AOwner, TRect rect): TcxCheckBox (AOwner)
{
    Parent     = AOwner;
    BoundsRect = rect;
    Anchors << akLeft << akRight;
};

//****************************************************************************
//                            AOLabelEditControl                             *
//****************************************************************************
UnicodeString AOLabelEditControl::getCaption()
{
    return caption;
}
//---------------------------------------------------------------------------
void AOLabelEditControl::setCaption (UnicodeString newCap)
{
    if ( label )
         label->Caption = newCap;
}
//---------------------------------------------------------------------------
void AOLabelEditControl::setLabelWidth (int newLabelWidth)
{
    if ( newLabelWidth < 20 || newLabelWidth > 80 )
        return;
    labelWidth = newLabelWidth;
    editWidth  = 98 - labelWidth;
    changeRects(NULL);
}
//---------------------------------------------------------------------------
void AOLabelEditControl::setEditWidth  (int newEditWidth)
{
    if ( newEditWidth < 20 || newEditWidth > 80 )
        return;
    editWidth = newEditWidth;
    labelWidth  = 98 - editWidth;
    changeRects(NULL);
}

//****************************************************************************
//                             AOButtonEdit                                  *
//****************************************************************************

__fastcall AOButtonEdit::AOButtonEdit (TWinControl* AOwner, TRect rect) : AOLabelEditControl (AOwner, rect)
{
    Parent     = AOwner;
    BoundsRect = rect;
    Anchors << akLeft << akRight;

    label         = new TcxLabel (AOwner);
    edit          = new AOButtEdit (AOwner);
    label->Parent = Parent;
    edit->Parent  = Parent;

    label->BoundsRect = Rect ( rect.Left, rect.Top, (rect.Right - rect.Left)/2 - 5, rect.Bottom );
    edit->BoundsRect  = Rect ( (rect.Right - rect.Left)/2, rect.Top, rect.Right, rect.Bottom );

    label->Anchors << akLeft << akRight;
    edit->Anchors  << akRight;
    edit->Anchors  >> akLeft;
    OnResize = changeRects;

}
//---------------------------------------------------------------------------
__fastcall AOButtonEdit::~AOButtonEdit ()
{
    if ( label ) delete label;
    if ( edit  ) delete edit;
}
//---------------------------------------------------------------------------
void __fastcall AOButtonEdit::changeRects(TObject* Sender)
{
    label->Width = Width * labelWidthPercent / 100;
    edit->Width  = Width - label->Width - 5;
    edit->Left   = label->Width + 5;
}

//****************************************************************************
//                             AODateEdit                                   *
//****************************************************************************

__fastcall AODateEdit::AODateEdit (TWinControl* AOwner, TRect rect) : AOLabelEditControl (AOwner, rect)
{
    Parent     = AOwner;
    BoundsRect = rect;
    Anchors << akLeft << akRight;

    label         = new TcxLabel (AOwner);
    edit          = new AODEdit (AOwner);
    label->Parent = Parent;
    edit->Parent  = Parent;

    label->BoundsRect = Rect ( rect.Left, rect.Top, (rect.Right - rect.Left)/2 - 5, rect.Bottom );
    edit->BoundsRect  = Rect ( (rect.Right - rect.Left)/2, rect.Top, rect.Right, rect.Bottom );

    label->Anchors << akLeft << akRight;
    edit->Anchors  << akRight;
    edit->Anchors  >> akLeft;
    OnResize = changeRects;

}
//---------------------------------------------------------------------------
__fastcall AODateEdit::~AODateEdit ()
{
    if ( label ) delete label;
    if ( edit  ) delete edit;
}
//---------------------------------------------------------------------------
void __fastcall AODateEdit::changeRects(TObject* Sender)
{
    label->Width = Width * labelWidthPercent / 100;
    edit->Width  = Width - label->Width - 5;
    edit->Left   = label->Width + 5;
}
//---------------------------------------------------------------------------

