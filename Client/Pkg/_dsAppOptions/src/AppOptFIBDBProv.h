//---------------------------------------------------------------------------

#ifndef AppOptFIBDBProvH
#define AppOptFIBDBProvH
//---------------------------------------------------------------------------
//#include <SysUtils.hpp>
//#include <Classes.hpp>
//#include <Registry.hpp>
#include "FIBQuery.hpp"
#include "pFIBQuery.hpp"

#include "AppOptBaseProv.h"
//---------------------------------------------------------------------------
class EFIBDBProviderError : public Exception
{
public:
    __fastcall EFIBDBProviderError(const UnicodeString Msg) : Exception(Msg) {}
};
//---------------------------------------------------------------------------
class PACKAGE TAppOptFIBDBProv : public TAppOptBaseProv
{
private:
    TpFIBQuery *quFree;

    UnicodeString FTableName;
    bool __fastcall CheckConnection();
    UnicodeString __fastcall FReadValue (UnicodeString AKey);
    void __fastcall FWriteValue (UnicodeString AKey, UnicodeString AVal);
    void __fastcall QExec(UnicodeString ASQL, bool aCommit = false);
protected:
public:
    __fastcall TAppOptFIBDBProv(TComponent* Owner);
    __fastcall ~TAppOptFIBDBProv();

   void __fastcall writeValue (TAppOptionsVar* AVar);
   void __fastcall writeValue (const UnicodeString key, const UnicodeString value);
   void __fastcall readValue  (TAppOptionsVar* AVar);

__published:
   __property TpFIBQuery *Query = {read = quFree , write = quFree};
   __property UnicodeString TableName = {read = FTableName , write = FTableName};

};
//---------------------------------------------------------------------------
#endif
