#ifndef AppOptVarH
#define AppOptVarH

//---------------------------------------------------------------------------
#include <Classes.hpp>
#include "AxeUtil.h"
#include "ICSVariant.h"
#include "AppOptDialogConstDef.h"

//###########################################################################
//#                                                                         #
//#                        TAppOptionsVar                                   #
//#                                                                         #
//###########################################################################

//---------------------------------------------------------------------------
enum TAOVarType
{
  vtypeNone,
  vtypeInteger,
  vtypeDouble,
  vtypeString,
  vtypeDateTime,
  vtypeBool
};
//---------------------------------------------------------------------------
class ECustomAOVarError : public System::Sysutils::Exception
{
public:
    __fastcall ECustomAOVarError(const UnicodeString Msg) : System::Sysutils::Exception(Msg) {}
};
//---------------------------------------------------------------------------

class TAppOptionsVar : public TObject
{
private:
   TAOVarType varType;
   UnicodeString name;
   ICSVariant value;
   TTagNode*  node;

   void __fastcall writeVal (const ICSVariant &newVal);
   ICSVariant __fastcall getVal ();
   bool __fastcall FIsEmpty();
   double __fastcall TryStrToFloatDef(UnicodeString ASrc, bool &AValid);

public:

   __fastcall TAppOptionsVar(const UnicodeString AName, TTagNode* ANode, const ICSVariant &AVal = NULL);
   void __fastcall Clear();

   __property UnicodeString Name  = {read = name};
   __property ICSVariant Value = {read = getVal, write = writeVal};
   __property TTagNode*  Node  = {read = node};
   __property bool isEmpty     = {read = FIsEmpty};
};

#endif
