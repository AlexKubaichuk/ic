//---------------------------------------------------------------------------

#include <basepch.h>

#pragma hdrstop

#include "AppOptions.h"
#include "AppOptDialogConstDef.h"
#pragma package(smart_init)

//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TAppOptions *)
{
    new TAppOptions(NULL);
}

namespace Appoptions
{
    void __fastcall PACKAGE Register()
    {
         TComponentClass classes[1] = {__classid(TAppOptions)};
         RegisterComponents("ICS", classes, 0);
    }
}
//---------------------------------------------------------------------------


//===========================================================================
//                           TAppOptions
//===========================================================================

__fastcall TAppOptions::TAppOptions(TComponent* Owner)
    : TComponent(Owner)
{
    dataProv  = NULL;
    pathToXML = "";
    FOptDefNode = NULL;
}

//---------------------------------------------------------------------------
__fastcall TAppOptions::~TAppOptions()
{
    if (FOptDefNode) delete FOptDefNode;
    FOptDefNode = NULL;
    for (TAOVarMap::iterator i = values.begin(); i != values.end(); i++)
     delete i->second;
}
//---------------------------------------------------------------------------

void __fastcall TAppOptions::LoadXML ()
{

    if (ComponentState.Contains(csDesigning))
        return;

    if (FOptDefNode)
        delete FOptDefNode;

    FOptDefNode = new TTagNode;

    //�������� XML-��������
    if ( FLoadXML )
        FLoadXML ( FOptDefNode );
    else if ( !pathToXML.IsEmpty() && FileExists (pathToXML) )
    {
        try
        {
            FOptDefNode->LoadFromXMLFile(pathToXML);
        }
        catch (Exception &e)
        {
            throw EAOAppOptionsError ( FMT2 (icsAppOptOptionsXMLLoadError, pathToXML, e.Message) );
        }
    }
    else
        throw EAOAppOptionsError (FMT ( icsAppOptOptionsXMLNotLoaded ) );

    if ( FOptDefNode && FOptDefNode->GetChildByName("content",true) )
        readGroup ( FOptDefNode->GetChildByName("content",true) );
    else
        throw EAOAppOptionsError (FMT ( icsAppOptOptionsXMLNoContent ) );
}

//---------------------------------------------------------------------------

void __fastcall TAppOptions::readGroup (TTagNode* grNode)
{
    if ( grNode == NULL )
        return;

    TTagNode* childNode;
    childNode = grNode->GetFirstChild();

    TAppOptionsVar*  pVar;
    UnicodeString name;

    while ( childNode != NULL )
    {
        name = childNode->AV["name"];

        if ( childNode->Name == "group" ||  childNode->Name == "servicegroup" )
            readGroup( childNode );
        else if ( childNode->Name == "extern" )
            throwExternVarsEvent ( childNode );
        else if ( childNode->Name == "actuals" )
        {
        }
        else
        {
            values[name]   = new TAppOptionsVar(name, childNode);
            pVar           = values[name];
            dataProv->readValue( pVar );
/*
            try
            {
                if ( childNode->Name == "digit" )        pVar = StrToFloat (var);
                else if ( childNode->Name == "binary")   pVar = (var.IsEmpty() || var == "0" || var == "false") ? 0 : 1;
                else if ( childNode->Name == "choice")   pVar = var;
                else if ( childNode->Name == "text")     pVar = var;
                else if ( childNode->Name == "datetime") pVar = (TDateTime)var;
            }
            catch (EConvertError &e)
            {
                if ( childNode->Name == "digit")    throw ECustomAOVarError ( FMT1 (icsAppOptOptionsCantConvertToDigit, var ) );
                if ( childNode->Name == "datetime") throw ECustomAOVarError ( FMT1 (icsAppOptOptionsCantConvertToDate, var ) );
            }
*/
            if ( FAfterRead )
                 FAfterRead ( childNode, pVar );
        }
        childNode    = childNode->GetNext();
    }
}

//---------------------------------------------------------------------------
TAppOptionsVar* __fastcall TAppOptions::getOption (UnicodeString AName)
{
    return variableExist (AName) ? values [AName] : NULL;
}
//---------------------------------------------------------------------------
TAppOptionsVar* __fastcall TAppOptions::getOptionByUID (UnicodeString UID)
{
    UnicodeString name = getNameByUID (UID);
    return variableExist (name) ? values [name] : NULL;
}
//---------------------------------------------------------------------------
ICSVariant __fastcall TAppOptions::getVarVal(UnicodeString AName)
{
  TAppOptionsVar* tmp  = getOption (AName);
  if (tmp)
   {
     return tmp->Value;
   }
  else
    throw ( ECustomAOVarError ( FMT1 (icsAppOptOptionsOptionNotDefined, AName ) ) );
}
//---------------------------------------------------------------------------
void __fastcall TAppOptions::setVarVal(UnicodeString AName, const ICSVariant &AVal)
{
  TAppOptionsVar* tmp  = getOption (AName);
  if (tmp)
   {
     tmp->Value = AVal;
     dataProv->writeValue(tmp);
   }
  else
    throw ( ECustomAOVarError ( FMT1 (icsAppOptOptionsOptionNotDefined, AName ) ) );
}
//---------------------------------------------------------------------------
ICSVariant __fastcall TAppOptions::getVarValByUID(UnicodeString AUID)
{
  TAppOptionsVar* tmp  = getOptionByUID (AUID);
  if (tmp)
   {
     return tmp->Value;
   }
  else
    throw ( ECustomAOVarError ( FMT1 (icsAppOptOptionsOptionNotDefined, AUID ) ) );
}
//---------------------------------------------------------------------------
void __fastcall TAppOptions::setVarValByUID(UnicodeString AUID, const ICSVariant &AVal)
{
  TAppOptionsVar* tmp  = getOptionByUID (AUID);
  if (tmp)
   {
     tmp->Value = AVal;
     dataProv->writeValue(tmp);
   }
  else
    throw ( ECustomAOVarError ( FMT1 (icsAppOptOptionsOptionNotDefined, AUID ) ) );
}
//---------------------------------------------------------------------------
UnicodeString TAppOptions::getNameByUID ( UnicodeString UID )
{
  TTagNode *tmp = FOptDefNode->GetTagByUID(UID);
  if (tmp)
   return tmp->AV["name"];
  else
    throw ( ECustomAOVarError ( FMT1 (icsAppOptOptionsOptionNotDefined, UID ) ) );
}
//---------------------------------------------------------------------------
/*
void __fastcall TAppOptions::setOption (UnicodeString name, ICSVariant newValue)
{

    if ( ( values[name] == NULL ) || ( values[name]->Node == NULL ) )
        throw ( ECustomAOVarError ( FMT1 (icsAppOptOptionsOptionNotDefined, name ) ) );

    if      ( values[name]->Node->Name == "binary")       setBinaryOpt (name, newValue);
    else if ( values[name]->Node->Name == "digit")        setDigitOpt  (name, newValue);
    else if ( values[name]->Node->Name == "choice")       setChoiceOpt (name, newValue);
    else if ( values[name]->Node->Name == "text")         setTextOpt   (name, newValue);
    else if ( values[name]->Node->Name == "datetime")     setDateOpt   (name, newValue);
    else if ( values[name]->Node->Name == "extern")       throw ( ECustomAOVarError( FMT (icsAppOptOptionsExternAssign) ) );
    else if ( values[name]->Node->Name == "group")        throw ( ECustomAOVarError( FMT (icsAppOptOptionsGroupAssign ) ) );
    else if ( values[name]->Node->Name == "servicegroup") throw ( ECustomAOVarError( FMT (icsAppOptOptionsServGroupAssign ) ) );

}
*/
//---------------------------------------------------------------------------
/*
void __fastcall TAppOptions::setBinaryOpt (UnicodeString name, ICSVariant newValue)
{
    int valType = newValue.Type;
    int val2write;
    if (  valType >= 2 && valType <= 5 )
    {
        if ( newValue == 0 ) val2write = 0;
        else                 val2write = 1;
    }
    else if ( valType == 256 )
    {
        if ( newValue == (UnicodeString)"false" || newValue == (UnicodeString)"0" || newValue.IsEmpty() )
             val2write = 0;
        else val2write = 1;
    }
    else if ( valType == 11 )
    {
        if      ( newValue) val2write = 1;
        else     val2write = 0;
//        else throw ( ECustomAOVarError( FMT1 (icsAppOptOptionsConvertError , newValue ) ) );
    }
    else throw ( ECustomAOVarError( FMT1 (icsAppOptOptionsConvertError , newValue ) ) );

    dataProv->writeValue( values[name]->node->AV["uid"], IntToStr (val2write) );
    values[name]->Value = val2write;
}
*/
//---------------------------------------------------------------------------
/*
void __fastcall TAppOptions::setDigitOpt  (UnicodeString name, ICSVariant newValue)
{
    if ( newValue.Type() == 256 && newValue == (UnicodeString)"empty" )
    {
        values[name]->Value = "";
        dataProv->writeValue( values[name]->node->AV["uid"], "empty" );
        return;
    }

    int decimal = values[name]->node->AV["decimal"].IsEmpty() ? 0 : decimal = values[name]->node->AV["decimal"].ToInt();
    int digits  = values[name]->node->AV["digits"].IsEmpty() ? 0 : values[name]->node->AV["digits"].ToInt();
    double min  = values[name]->node->AV["min"].IsEmpty() ? 0 :values[name]->node->AV["min"].ToDouble();
    double max  = values[name]->node->AV["max"].IsEmpty() ? 0 :values[name]->node->AV["max"].ToDouble();

    UnicodeString formatStr = "";

    if ( decimal )
    {
        for ( int i=0 ; i < (digits - decimal - 2) ; i++)
            formatStr+= "#";

        formatStr += "0.0";

        for ( int i=0 ; i < (decimal-1) ; i++)
            formatStr+= "#";
    }
    else
    {
        for ( int i=0 ; i < (digits - 1) ; i++)
            formatStr+= "#";

        formatStr += "0";
    }

    UnicodeString val = FormatFloat( formatStr, (double)newValue );
    double resVal = StrToFloat( val );

    if ( ( resVal < min ) || ( resVal > max ) )
        throw ( ECustomAOVarError( FMT (icsAppOptOptionsNotInRange) ) );

    values[name]->Value = resVal;
    dataProv->writeValue( values[name]->node->AV["uid"], FloatToStr(resVal) );
}
*/
//---------------------------------------------------------------------------
/*
void __fastcall TAppOptions::setChoiceOpt (UnicodeString name, ICSVariant newValue)
{
    if ( newValue.Type() != 3 && newValue.Type() != 256 && newValue.Type() != 2 )
        throw ( ECustomAOVarError( FMT (icsAppOptOptionsChoicevalValMustBeIntegerOrStr) ) );

    if ( newValue.Type() != 256 && newValue == -1 )
    {
        values[name]->Value = -1;
        dataProv->writeValue( values[name]->node->AV["uid"], -1 );
        return;
    }

    TTagNode* child;
    child = values[name]->node->GetChildByAV("choicevalue","value",(UnicodeString)newValue);

    if ( child )
    {
      values[name]->Value = newValue;
      dataProv->writeValue( values[name]->node->AV["uid"], newValue );
      return;
    }
    else
     throw ( ECustomAOVarError ( FMT (icsAppOptOptionsNoMatch) ) );
}
*/
//---------------------------------------------------------------------------
/*
void __fastcall TAppOptions::setTextOpt (UnicodeString name, ICSVariant newValue)
{

    UnicodeString mask;
    mask = ANode->AV["mask"];

    if ( !mask.IsEmpty() )
    {
        if ( !validateText( (UnicodeString)newValue.pcVal , mask ) )
            throw ( ECustomAOVarError ( FMT (icsAppOptOptionsMaskError) ));
    }

    values[name]->Value = newValue;
    dataProv->writeValue( values[name]->node->AV["uid"], newValue );

}
*/
//---------------------------------------------------------------------------
/*
void __fastcall TAppOptions::setDateOpt (UnicodeString name, ICSVariant newValue)
{

    UnicodeString type;
    type = values[name]->node->AV["subtype"];

    TDateTime date = newValue.VDate;

    if ( type == "time")
        dataProv->writeValue( values[name]->node->AV["uid"], date.TimeString() );
    else if ( type == "datetime")
        dataProv->writeValue( values[name]->node->AV["uid"], date.DateTimeString() );
    else //if ( type == "date")
        dataProv->writeValue( values[name]->node->AV["uid"], date.DateString() );

    values[name]->Value = newValue;

}
*/
//---------------------------------------------------------------------------
/*
bool __fastcall TAppOptions::validateText (UnicodeString text, UnicodeString mask)
{

    int maskLength = mask.Length();
    int maskIndex=1;
    int textIndex=1;
    for ( ; maskIndex <= maskLength ; )
    {
        if ( mask[maskIndex] == '0')
        {
            if ( !isNumber( text[textIndex] ) ) return false;
        }

        else if ( mask[maskIndex] == '9')
        {
            if ( (!isNumber( text[textIndex] )) && (text[textIndex]!=' ') ) return false;
        }

        else if ( mask[maskIndex] == 'L')
        {
            if ( !isLetter( text[textIndex] ) ) return false;
        }

        else if ( mask[maskIndex] == 'l')
        {
            if ( (!isLetter(text[textIndex])) && (text[textIndex]!=' ') ) return false;
        }

        else if ( mask[maskIndex] == 'A')
        {
            if ( (!isNumber(text[textIndex])) && (!isLetter(text[textIndex])) ) return false;
        }

        else if ( mask[maskIndex] == 'a')
        {
            if ( (!isNumber(text[textIndex])) && (!isLetter(text[textIndex])) && (text[textIndex]!=' ') ) return false;
        }
        else if ( mask[maskIndex] == '\\' )
        {
            if ( mask[++maskIndex] != text[textIndex] ) return false;
        }
        maskIndex++;
        textIndex++;
    }
    //���� ����� ������� �����
    if ( textIndex < text.Length() ) return false;

    return true;

}
*/
//---------------------------------------------------------------------------
/*
bool __fastcall TAppOptions::isNumber (char symbol)
{
    //��������� �������� �� ������ ��������
    for (char i=0x30 ; i != 0x3a ; i++)
        if ( symbol == i ) return true;

    return false;
}

//---------------------------------------------------------------------------
*/
/*
bool __fastcall TAppOptions::isLetter (char symbol)
{
    //��������� �������� �� ������ ���������
    for (char i=0x41 ; i != 0x5b ; i++)
        if ( symbol == i ) return true;

    for (char i=0x61 ; i != 0x7b ; i++)
        if ( symbol == i ) return true;

    return false;
}
*/
//---------------------------------------------------------------------------

/************************************************************************************
Char  Dec  Oct  Hex | Char  Dec  Oct  Hex | Char  Dec  Oct  Hex | Char Dec  Oct   Hex
-------------------------------------------------------------------------------------
(nul)   0 0000 0x00 | (sp)   32 0040 0x20 | @      64 0100 0x40 | `      96 0140 0x60
(soh)   1 0001 0x01 | !      33 0041 0x21 | A      65 0101 0x41 | a      97 0141 0x61
(stx)   2 0002 0x02 | "      34 0042 0x22 | B      66 0102 0x42 | b      98 0142 0x62
(etx)   3 0003 0x03 | #      35 0043 0x23 | C      67 0103 0x43 | c      99 0143 0x63
(eot)   4 0004 0x04 | $      36 0044 0x24 | D      68 0104 0x44 | d     100 0144 0x64
(enq)   5 0005 0x05 | %      37 0045 0x25 | E      69 0105 0x45 | e     101 0145 0x65
(ack)   6 0006 0x06 | &      38 0046 0x26 | F      70 0106 0x46 | f     102 0146 0x66
(bel)   7 0007 0x07 | '      39 0047 0x27 | G      71 0107 0x47 | g     103 0147 0x67
(bs)    8 0010 0x08 | (      40 0050 0x28 | H      72 0110 0x48 | h     104 0150 0x68
(ht)    9 0011 0x09 | )      41 0051 0x29 | I      73 0111 0x49 | i     105 0151 0x69
(nl)   10 0012 0x0a | *      42 0052 0x2a | J      74 0112 0x4a | j     106 0152 0x6a
(vt)   11 0013 0x0b | +      43 0053 0x2b | K      75 0113 0x4b | k     107 0153 0x6b
(np)   12 0014 0x0c | ,      44 0054 0x2c | L      76 0114 0x4c | l     108 0154 0x6c
(cr)   13 0015 0x0d | -      45 0055 0x2d | M      77 0115 0x4d | m     109 0155 0x6d
(so)   14 0016 0x0e | .      46 0056 0x2e | N      78 0116 0x4e | n     110 0156 0x6e
(si)   15 0017 0x0f | /      47 0057 0x2f | O      79 0117 0x4f | o     111 0157 0x6f
(dle)  16 0020 0x10 | 0      48 0060 0x30 | P      80 0120 0x50 | p     112 0160 0x70
(dc1)  17 0021 0x11 | 1      49 0061 0x31 | Q      81 0121 0x51 | q     113 0161 0x71
(dc2)  18 0022 0x12 | 2      50 0062 0x32 | R      82 0122 0x52 | r     114 0162 0x72
(dc3)  19 0023 0x13 | 3      51 0063 0x33 | S      83 0123 0x53 | s     115 0163 0x73
(dc4)  20 0024 0x14 | 4      52 0064 0x34 | T      84 0124 0x54 | t     116 0164 0x74
(nak)  21 0025 0x15 | 5      53 0065 0x35 | U      85 0125 0x55 | u     117 0165 0x75
(syn)  22 0026 0x16 | 6      54 0066 0x36 | V      86 0126 0x56 | v     118 0166 0x76
(etb)  23 0027 0x17 | 7      55 0067 0x37 | W      87 0127 0x57 | w     119 0167 0x77
(can)  24 0030 0x18 | 8      56 0070 0x38 | X      88 0130 0x58 | x     120 0170 0x78
(em)   25 0031 0x19 | 9      57 0071 0x39 | Y      89 0131 0x59 | y     121 0171 0x79
(sub)  26 0032 0x1a | :      58 0072 0x3a | Z      90 0132 0x5a | z     122 0172 0x7a
(esc)  27 0033 0x1b | ;      59 0073 0x3b | [      91 0133 0x5b | {     123 0173 0x7b
(fs)   28 0034 0x1c | <      60 0074 0x3c | \      92 0134 0x5c | |     124 0174 0x7c
(gs)   29 0035 0x1d | =      61 0075 0x3d | ]      93 0135 0x5d | }     125 0175 0x7d
(rs)   30 0036 0x1e | >      62 0076 0x3e | ^      94 0136 0x5e | ~     126 0176 0x7e
(us)   31 0037 0x1f | ?      63 0077 0x3f | _      95 0137 0x5f | (del) 127 0177 0x7f
************************************************************************************/

//---------------------------------------------------------------------------

void __fastcall TAppOptions::throwExternVarsEvent ( TTagNode* ANode )
{
    // ��������� ������� �� ������ �� ������ �������� ��������������
    if ( ANode == NULL )
        return;

    TTagNode* childNode = ANode->GetFirstChild();

    TAOVarMap extVars;

    while ( childNode != NULL )
    {
        if ( childNode->Name == "actuals")
        {
            childNode = childNode->GetNext();
            continue;
        }

        values [childNode->AV["name"]] = new TAppOptionsVar(childNode->AV["name"], childNode);
        extVars[childNode->AV["name"]] = new TAppOptionsVar(childNode->AV["name"], childNode);
        childNode                      = childNode->GetNext();
    }

    if (FLoadExternVars) FLoadExternVars ( ANode, extVars );

    for (TAOVarMap::iterator i = extVars.begin(); i != extVars.end(); i++)
        values[i->first] = i->second;
}

//---------------------------------------------------------------------------

bool __fastcall TAppOptions::variableExist ( UnicodeString name )
{
    TAOVarMap::iterator rc = values.find (name);
    return rc == values.end() ? false:true;
}

//---------------------------------------------------------------------------
TAppOptBaseProv* __fastcall TAppOptions::getDataProv () {return dataProv;}
//---------------------------------------------------------------------------
void __fastcall TAppOptions::setDataProv (TAppOptBaseProv* newProv) {dataProv = newProv;}
//---------------------------------------------------------------------------

