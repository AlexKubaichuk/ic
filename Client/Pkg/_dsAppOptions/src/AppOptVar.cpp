//---------------------------------------------------------------------------


#pragma hdrstop

#include "AppOptVar.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall TAppOptionsVar::TAppOptionsVar(const UnicodeString AName, TTagNode* ANode, const ICSVariant &AVal)
{
    name = AName;
    node = ANode;

    if (ANode->CmpName("digit"))
     {
       if (!ANode->AV["decimal"].Length())
        varType = vtypeInteger;
       else
        varType = vtypeDouble;
     }
    else if ( ANode->CmpName("binary"))   varType = vtypeBool;
    else if ( ANode->CmpName("choice"))   varType = vtypeInteger;
    else if ( ANode->CmpName("text"))     varType = vtypeString;
    else if ( ANode->CmpName("datetime")) varType = vtypeDateTime;
    else
     throw  ECustomAOVarError(FMT1(icsAOVarWrongType,ANode->Name));

    if ( AVal )
        writeVal (AVal);
}
//---------------------------------------------------------------------------
void __fastcall TAppOptionsVar::writeVal (const ICSVariant &newVal)
{
  if ((newVal.Type == ivtNULL) && node->CmpAV("required","yes"))
   throw  ECustomAOVarError(FMT(icsAOVarNotReqNULL));
  switch (varType)
   {
     case vtypeInteger:
      {
        if (node->CmpName("digit"))
         {
           if (varType == vtypeInteger)
            {
              int FVal = newVal;
              int min  = node->AV["min"].ToIntDef(0);
              int max  = node->AV["max"].ToIntDef(0);
              if (!((FVal >= min)&&(FVal <= max)))
               throw  ECustomAOVarError(FMT3(icsAOVarOutOfRange,node->Name,node->AV["min"],node->AV["max"]));
            }
           value = newVal;
         }
        else if ( node->CmpName("choice"))
         {
           int FVal = newVal;
           TTagNode *itNode = node->GetFirstChild();
           bool FValid = false;
           while (itNode && !FValid)
            {
              if (itNode->AV["value"].ToIntDef(-7000) == FVal)
               FValid = true;
              itNode = itNode->GetNext();
            }
           if (FValid)
            value = newVal;
           else
            throw  ECustomAOVarError(FMT2(icsAOVarOutOfList,IntToStr(FVal),node->Name));
         }
        break;
      }
     case vtypeDouble:
      {
        double FVal = newVal;
        bool FValid = false;
        double min  = TryStrToFloatDef(node->AV["min"], FValid);
        double max  = TryStrToFloatDef(node->AV["max"], FValid);
        if (!((FVal >= min)&&(FVal <= max)))
         throw  ECustomAOVarError(FMT3(icsAOVarOutOfRange,node->Name,node->AV["min"],node->AV["max"]));
        value = newVal;
        break;
      }
     case vtypeBool:
      {
        value = newVal;
        break;
      }
     case vtypeString:
      {
        value = newVal;
        break;
      }
     case vtypeDateTime:
      {
        value = newVal;
        break;
      }
     default:  {break;}
   }
}
//---------------------------------------------------------------------------
ICSVariant __fastcall TAppOptionsVar::getVal ()
{
  return value;
}
//---------------------------------------------------------------------------
bool __fastcall TAppOptionsVar::FIsEmpty()
{
  return value.IsEmpty();
}
//---------------------------------------------------------------------------
void __fastcall TAppOptionsVar::Clear()
{
  value.Clear();
}
//---------------------------------------------------------------------------
double __fastcall TAppOptionsVar::TryStrToFloatDef(UnicodeString ASrc, bool &AValid)
{
  AValid = true;
  TReplaceFlags rFlag;
  rFlag << rfReplaceAll;
  double RC = 0;
  try
   {
     if (!TryStrToFloat(ASrc, RC))
      if (!TryStrToFloat(StringReplace(ASrc.Trim(),".", ",", rFlag), RC))
       {
         RC = 0;
         AValid = false;
       }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
