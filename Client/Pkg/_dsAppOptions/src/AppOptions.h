//---------------------------------------------------------------------------
#ifndef AppOptionsH
#define AppOptionsH
//---------------------------------------------------------------------------

#include <SysUtils.hpp>
#include <Classes.hpp>
#include "AppOptBaseProv.h"

//---------------------------------------------------------------------------

namespace Appoptions
{


typedef void __fastcall (__closure *TAOExtLoadXML)(TTagNode *AOptNode);
typedef map <UnicodeString, TAppOptionsVar*> TAOVarMap;
typedef void __fastcall (__closure *TAfterRead) (TTagNode* ANode, TAppOptionsVar* Var);
typedef void __fastcall (__closure *TExternVars)(TTagNode* ANode, TAOVarMap &vars);


//###########################################################################
//#                                                                         #
//#                        TAppOptions                                      #
//#                                                                         #
//###########################################################################


class PACKAGE TAppOptions : public TComponent
{
private:
    TAOExtLoadXML      FLoadXML;
    TExternVars      FLoadExternVars;
    TAfterRead       FAfterRead;
    TAppOptBaseProv* dataProv;
    TAOVarMap        values;
    UnicodeString       pathToXML;


//    void __fastcall setBinaryOpt (UnicodeString name, ICSVariant newValue);
//    void __fastcall setDigitOpt  (UnicodeString name, ICSVariant newValue);
//    void __fastcall setChoiceOpt (UnicodeString name, ICSVariant newValue);
//    void __fastcall setTextOpt   (UnicodeString name, ICSVariant newValue);
//    void __fastcall setDateOpt   (UnicodeString name, ICSVariant newValue);

//    bool __fastcall validateText (UnicodeString text, UnicodeString mask);
//    bool __fastcall isNumber     (char symbol);
//    bool __fastcall isLetter     (char symbol);
    void __fastcall readGroup    (TTagNode* grNode);

    void __fastcall throwExternVarsEvent ( TTagNode* ANode );

protected:
    UnicodeString getNameByUID ( UnicodeString UID );
    TAppOptionsVar* __fastcall getOption      (UnicodeString AName);
    TAppOptionsVar* __fastcall getOptionByUID (UnicodeString UID);
//    void            __fastcall setOption      (UnicodeString name, ICSVariant newValue);
    bool __fastcall variableExist ( UnicodeString name );

    ICSVariant __fastcall getVarVal(UnicodeString AName);
    void __fastcall setVarVal(UnicodeString AName, const ICSVariant &AVal);

    ICSVariant __fastcall getVarValByUID(UnicodeString AUID);
    void __fastcall setVarValByUID(UnicodeString AUID, const ICSVariant &AVal);
    TTagNode* FOptDefNode;

public:
    TTagNode* ANode;

    __fastcall TAppOptions(TComponent* Owner);
    __fastcall ~TAppOptions();
    
    void            __fastcall LoadXML ();

    __property TAppOptionsVar* Vars[UnicodeString AName]    = {read = getOption};
    __property TAppOptionsVar* VarsByUID[UnicodeString UID] = {read = getOptionByUID};
    __property ICSVariant     Vals[UnicodeString AName]    = {read = getVarVal, write=setVarVal};
    __property ICSVariant     ValsByUID[UnicodeString UID] = {read = getVarValByUID, write=setVarValByUID};

    __property TTagNode* OptDefNode = {read = FOptDefNode};

    TAppOptBaseProv* __fastcall getDataProv ();
    void __fastcall setDataProv (TAppOptBaseProv* newProv);

__published:

    __property TAOExtLoadXML OnLoadXML         = {read =FLoadXML  , write=FLoadXML};
    __property TExternVars OnExternVars      = {read = FLoadExternVars , write = FLoadExternVars};
    __property TAfterRead  AfterRead         = {read = FAfterRead , write = FAfterRead};
    __property UnicodeString  XMLPath           = {read=pathToXML  , write=pathToXML};
    __property TAppOptBaseProv* DataProvider = {read=getDataProv, write=setDataProv};

};
//---------------------------------------------------------------------------

}
using namespace Appoptions;
#endif
