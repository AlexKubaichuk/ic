unit icsAppOptionsConst;

interface

resourcestring

// AppOptions Dialog
ICSAPPOPTTEST = 'Cancel';
icsAppOptDialogOKBtnCaption = '���������';
icsAppOptDialogCancelBtnCaption = '������';
icsAppOptDialogDefWindowCaption = '���������';
icsAppOptDialogTooManyGroups = '��������� ���������� ������� ����������� �������� (�� ����� 5 �������)'+#13#10+'������ "%s" �������� �� �����';
icsAppOptDialogErrMsmCaption = '������';
icsAppOptDialogNeed2FillReqs = '��������� ��� ����������� (������������ ������) ����';
icsAppOptDialogConditionError = '������� ������������ �� ����� ��������� ������ �� ���������� � ������ �������.'+#13#10+'�������: "%s"';
icsAppOptDialogWrongLink = '������� ������������ ��������� �� ���������, ������� �� ����� ����� ��������'+#13#10+'������� ������ ��: "%s"'+#13#10+'�������: "%s"';
icsAppOptDialogSetVarErrorNoNode = '���������� ������ ����� �������� ���������� '+#13#10+'����������� ����������� ����';
icsAppOptDialogSetVarErrorNoComponent = '���������� ������ ����� �������� ���������� '+#13#10+'�� ������� ����� ���������';
icsAppOptDialogSetVarErrorArgumentNotADate = '���������� ������ ����� �������� ���������� '+#13#10+'����� �������� �����������';
icsAppOptDialogSetVarErrorArgumentNotAInt  = '���������� ������ ����� �������� ���������� '+#13#10+'����� �������� �����������';
icsAppOptDialogOutOfRangeLessThanMin = '�������� �������� ������� �� ��������� ��������� ��������'+#13#10+'����������� ��������: "%f"';
icsAppOptDialogOutOfRangeMoreThanMax = '�������� �������� ������� �� ��������� ��������� ��������'+#13#10+'������������ ��������: "%f"';

// AppOptions EvalCondition
icsAppOptDialogEvalCondUndefError = '������� ���������� ������������ �������� ��������� � ����� �������� �������������� ��������'+#13#10+'��������� ������ �������� == � !=';
icsAppOptDialogEvalCondUnknownOperation = '���������������� ��������:'+#13#10+'��������: "%s"'+#13#10+'���������: "%s"';
icsAppOptDialogEvalCondVarIsNotDigit    = '������: ������� ��������� ���������� �������� �������� ����������';
icsAppOptDialogEvalCondVarIsNotString   = '������: ������� ��������� ����������� �������� ��������� ����������';
icsAppOptDialogEvalCondVarsNotSameType  = '������: ������� ��������� ����� � �������';
icsAppOptDialogEvalCondVarNoValue       = '������: �� ������ �������� ���������� "%s"';

// AppOptions Options
icsAppOptOptionsXMLLoadError = '������ � �������� �������� XML ����� "%s"'+#13#10+'"%s".';
icsAppOptOptionsXMLNotLoaded = '������ � �������� �������� XML �����: '+#13#10+'�� ������ ���� � ����� � �� �������������� �������� XML ����� �����.';
icsAppOptOptionsXMLNoContent = '������ � �������� �������� XML �����: '+#13#10+' ����������� ��� content.';
icsAppOptOptionsExternAssign = '���������� ��������� �������� ���� \"������� ��������������\".';
icsAppOptOptionsGroupAssign  = '���������� ��������� �������� ���� \"������\".';
icsAppOptOptionsServGroupAssign = '���������� ��������� �������� ���� \"��������� ������\".';
icsAppOptOptionsConvertError = '�� ������� ���������������� �������� "%s" � ��������� ���.';
icsAppOptOptionsNotInRange   = '����� �� �������� � �������� ���������.';
icsAppOptOptionsNoMatch      = '�� ������� ���������� � ������ ��������� ��������.';
icsAppOptOptionsMaskError    = '������: ��������� ����� �� ������������� ����� �����.';
icsAppOptOptionsChoicevalValMustBeIntegerOrStr = '�������� �������� ������� ����� ������ ���� ����� ������ ��� �������.';
icsAppOptOptionsCantConvertToDigit = '������: �� ������� �������� �������� "%s" � ���������.';
icsAppOptOptionsCantConvertToDate  = '������: �� ������� �������� �������� "%s" � ����.';
icsAppOptOptionsOptionNotDefined   = '������: ���������� "%s" �� ����������.';
// AppOptions Data Providers

// Registry Data Provider
icsAppOptRegProvRootNotDefined = '������: �� ������� ����� �������';

// AO Base Provider
icsAppOptBaseProvValueNotDigit  = '������: ������� ���������� ����������� �������� �������� ����������.';
icsAppOptBaseProvValueNotString = '������: ������� ���������� ������������ �������� ��������� ����������.';
icsAppOptBaseProvValueNotDate   = '������: ������� ���������� ������������� �������� ���������� ���� "����".';
icsAppOptBaseProvVarTypeIsNone  = '������: ������� ��������� � ����������, �� ������� ����.';

// AO Var
icsAOVarWrongType   = '��� "%s" ����������� � ������ ���������� �����.';
icsAOVarOutOfRange  = '�������� "%s" ������� �� ������� ����������� ��������� (%s - %s).';
icsAOVarOutOfList   = '�������� "%s" ��� ��������� "%s" �� ������ � ������ ���������� ��������.';
icsAOVarNotReqNULL  = '������: ������� ���������� ������� �������� ������������ ����������.';

// DB Provider
icsAppOptFIBDBProvQueryNotDef = '�� ���������� �������� �������� "Query".';
icsAppOptFIBDBProvErrDBWork = '������ ������ � ��, ������/������ ����� ���������';
icsAppOptFIBDBProvErrTableNotDef = '�� ���������� �������� �������� "TableName".';
icsAppOptFIBDBProvKeyNotValid = '�� ���������� �������� ������������ ��������.';
icsAppOptFIBDBProvErrWriteValue = '������ ������ �������� ��� �������� "%s".';

// XML Provider
icsAppOptXMLProvNoDefNode = '�� ���������� �������� �������� ��� "FileName" ��� "DefNode".';
icsAppOptXMLProvKeyNotValid = '�� ���������� �������� ������������ ��������.';
icsAppOptXMLProvErrWriteValue = '������ ������ �������� ��� �������� "%s".';

implementation

end.
