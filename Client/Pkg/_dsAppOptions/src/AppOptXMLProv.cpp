//---------------------------------------------------------------------------

#pragma hdrstop

#include "AppOptXMLProv.h"

#include "AppOptDialogConstDef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TAppOptXMLProv *)
{
    new TAppOptXMLProv(NULL);
}
//---------------------------------------------------------------------------
__fastcall TAppOptXMLProv::TAppOptXMLProv(TComponent* Owner)
    : TAppOptBaseProv(Owner)
{
  FDefNode  = NULL;
  FFileName = "";
  FExternal = false;
}
//---------------------------------------------------------------------------
__fastcall TAppOptXMLProv::~TAppOptXMLProv()
{
  if (FDefNode && !FExternal)
   delete FDefNode;
}
//---------------------------------------------------------------------------
void __fastcall TAppOptXMLProv::FSetDefNode(TTagNode *AValue)
{
  FExternal = (AValue != NULL);
  FDefNode = AValue;
}
//---------------------------------------------------------------------------
void __fastcall TAppOptXMLProv::writeValue (TAppOptionsVar* AVar)
{
  try
   {
      UnicodeString UID = AVar->Node->AV["uid"];

      if ( AVar->Node->CmpName("digit") )
       {
          AVar->isEmpty ? FWriteValue (UID, "empty") : FWriteValue ( UID, AVar->Value.AsString );
       }
      else
       FWriteValue ( UID, AVar->Value.AsString );
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall TAppOptXMLProv::writeValue (UnicodeString key, UnicodeString value)
{
  FWriteValue(key, value);
}
//---------------------------------------------------------------------------
void __fastcall TAppOptXMLProv::readValue  (TAppOptionsVar* AVar)
{
  try
   {
      UnicodeString tmp = FReadValue(AVar->Node->AV["uid"]);
      bool FValid = true;
      if ( AVar->Node->CmpName("digit") )
       {
         if (tmp.LowerCase() == "empty")
          {
            AVar->Clear();
          }
         else
          AVar->Value = TryStrToFloatDef(tmp,FValid);
       }
      else if ( AVar->Node->CmpName("binary"))
       AVar->Value = (bool)tmp.ToIntDef(0);
      else if ( AVar->Node->CmpName("choice"))
       AVar->Value = tmp.ToIntDef(0);
      else if ( AVar->Node->CmpName("text"))
       AVar->Value = tmp;
      else if ( AVar->Node->CmpName("datetime"))
       AVar->Value = TryStrToDateTimeDef(tmp,FValid);
      if (!FValid)
       AVar->Clear();
   }
  catch (Exception &e)
   {
     AVar->Clear();
   }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAppOptXMLProv::FReadValue (UnicodeString AKey)
{
  UnicodeString RC = "";
  try
   {
     if (!AKey.Trim().Length())
       throw EXMLProviderError (FMT(icsAppOptXMLProvKeyNotValid));
     if (!FDefNode)
       throw EXMLProviderError (FMT(icsAppOptXMLProvNoDefNode));
     TTagNode *FRC = FDefNode->GetChildByAV("i","n", AKey, true);
     if (FRC)
       RC = FRC->AV["v"];
     else
       throw EXMLProviderError (FMT1(icsAppOptXMLProvErrWriteValue,AKey));
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TAppOptXMLProv::FWriteValue (UnicodeString AKey, UnicodeString AVal)
{
  if (!AKey.Trim().Length())
    throw EXMLProviderError (FMT(icsAppOptXMLProvKeyNotValid));
  if (!FDefNode)
    throw EXMLProviderError (FMT(icsAppOptXMLProvNoDefNode));
  TTagNode *FRC = FDefNode->GetChildByAV("i","n", AKey, true);
  if (!FRC)
   {
     FRC = FDefNode->AddChild("i");
     FRC->AV["n"] = AKey;
   }
  FRC->AV["v"] = AVal.Trim();
  if (!FExternal && FFileName.Length())
   {
     FDefNode->SaveToXMLFile(FFileName,"");
   }
}
//---------------------------------------------------------------------------
void __fastcall TAppOptXMLProv::FSetFileName(UnicodeString AValue)
{
  FFileName = AValue.Trim();
  if (FFileName.Length())
   {
     if (!FDefNode) FDefNode = new TTagNode;
     FDefNode->LoadFromFile(FFileName);
   }
}
//---------------------------------------------------------------------------

namespace Appoptxmlprov
{
    void __fastcall PACKAGE Register()
    {
         TComponentClass classes[1] = {__classid(TAppOptXMLProv)};
         RegisterComponents("ICS", classes, 0);
    }
}
//---------------------------------------------------------------------------