//---------------------------------------------------------------------------


#pragma hdrstop

#include "AppOptBaseProv.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall EAOAppOptionsError::EAOAppOptionsError(const UnicodeString Msg)
: Exception(Msg)
{
}
//---------------------------------------------------------------------------
__fastcall ECustomAOError::ECustomAOError(const UnicodeString Msg)
: Exception(Msg)
{
}
//---------------------------------------------------------------------------
__fastcall ECustomAOProviderError::ECustomAOProviderError(const UnicodeString Msg)
: Exception(Msg)
{
}
//---------------------------------------------------------------------------
__fastcall  TAppOptBaseProv::TAppOptBaseProv (TComponent* AOwner)
: TComponent(AOwner)
{
}
//---------------------------------------------------------------------------
double __fastcall TAppOptBaseProv::TryStrToFloatDef(UnicodeString ASrc, bool &AValid)
{
  AValid = true;
  TReplaceFlags rFlag;
  rFlag << rfReplaceAll;
  double RC = 0;
  try
   {
     if (!TryStrToFloat(ASrc, RC))
      if (!TryStrToFloat(StringReplace(ASrc.Trim(),".", ",", rFlag), RC))
       {
         RC = 0;
         AValid = false;
       }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TDateTime __fastcall TAppOptBaseProv::TryStrToDateTimeDef(UnicodeString ASrc, bool &AValid)
{
  AValid = true;
  TDateTime RC = TDateTime(0);
  try
   {
     if (!TryStrToDateTime(ASrc, RC))
      {
        RC = TDateTime(0);
        AValid = false;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

