//---------------------------------------------------------------------------
#include <basepch.h>

#pragma hdrstop

#include "AppOptDialog.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TAppOptDialog *)
{
    new TAppOptDialog(NULL);
}


//---------------------------------------------------------------------------
__fastcall EAppOptDialogError::EAppOptDialogError (const UnicodeString Msg)
: Exception(Msg)
{
}
//---------------------------------------------------------------------------
__fastcall TAppOptDialog::TAppOptDialog(TComponent* Owner)
    : TComponent(Owner)
{
    formHeight=420;
    formWidth=600;
    AOptions         = NULL;
    optForm          = NULL;
    optTreeList      = NULL;
    bottomPanel      = NULL;
//    bottomPanelSpl   = NULL;
    optPageCtrl      = NULL;
    splitter         = NULL;

    FAfterComponentCreate = NULL;
    FExtEdit              = NULL;
    FBtnEdit              = NULL; 
    FBeforeSave           = NULL;
    FUnresolvedRes        = NULL;

    requiredColor = clInfoBk;
    normalColor   = clWhite;
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::Execute(UnicodeString formCaption, int ASelIdx )
{
    pageIndex = 0;
    if ( AOptions == NULL)
        throw EAppOptDialogError ("AppOptions component is not set");

    if ( AOptions->OptDefNode == NULL )
        AOptions->LoadXML();

    TTagNode* contentNode = AOptions->OptDefNode->GetChildByName("content",true);

    if ( contentNode->GetFirstChild() == NULL )
        throw EAppOptDialogError ("XML file dont have any content");

    createForm ( formCaption );
    createTreeView();
    createPageControl ( optForm );
    createSplitter();
    createBottomPanel();

    fillData ( contentNode, optTreeList->Root );

    for ( TAODataCompMap::iterator i = components.begin(); i != components.end(); i++)
        workoutDependency ( i->first );

    for ( TAOVisualCompMap::iterator i = vcomponents.begin(); i != vcomponents.end(); i++)
        workoutDependency ( i->first );

    optTreeList->Root->Expand(true);
    if (ASelIdx < optTreeList->Root->Count)
     optTreeList->Root->Items[ASelIdx]->Focused = true;
    else
     optTreeList->TopNode->Focused = true;
    splitter->CloseSplitter();
    splitter->OpenSplitter();
    optForm->ShowModal();
    delete optForm;
}

//***************************************************************************
//                             ������ ��������                              *
//***************************************************************************

void __fastcall TAppOptDialog::setFormHeight( int pH )
{
  formHeight = (pH < 420) ? 420 : pH;
}
//---------------------------------------------------------------------------
void __fastcall TAppOptDialog::setFormWidth( int pW )
{
  formWidth  = (pW < 600) ? 600 : pW;
}
//---------------------------------------------------------------------------
void __fastcall TAppOptDialog::createForm ( UnicodeString formCaption )
{
    optForm           = new TForm(this);
    optForm->Caption  = formCaption;
    optForm->Constraints->MinHeight = 420;
    optForm->Constraints->MinWidth  = 600;
    optForm->Width    = formWidth;
    optForm->Height   = formHeight;
    optForm->Position = poScreenCenter;
//    optForm->Top      = formHeight > Screen->Height ? 0 : (Screen->Height - formHeight)/2;
//    optForm->Left     = formWidth > Screen->Width ? 0 : (Screen->Width - formWidth)/2;
    optForm->OnClose  = onFormClose;

    optForm->BorderIcons >> biMinimize;
    optForm->BorderIcons >> biMaximize;
    optForm->BorderStyle = bsSingle;//bsSizeable;

//    optForm->OnMouseWheelDown = onMouseWheelDown;
//    optForm->OnMouseWheelUp   = onMouseWheelUp;
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::createTreeView ()
{
    optTreeList = new TcxTreeList( optForm );

    optTreeList->Parent = optForm;
//    optTreeList->Top    = 1;
//    optTreeList->Left   = 1;
    optTreeList->Width  = 180;
//    optTreeList->Height = optForm->Height - 65;
    optTreeList->Align  = alLeft;
    optTreeList->Constraints->MinWidth = 150;
    optTreeList->Constraints->MaxWidth = 200;

    optTreeList->OptionsView->Headers     = false;
    optTreeList->LookAndFeel->Kind        = lfStandard;
    optTreeList->LookAndFeel->NativeStyle = true;
    optTreeList->OnFocusedNodeChanged     = focusedNodeChanged;
    optTreeList->OnResize                 = onTreeListResize;
    optTreeList->DeleteAllColumns();

//    optTreeList->OnMouseWheelDown = onMouseWheelDown;
//    optTreeList->OnMouseWheelUp   = onMouseWheelUp;

    TcxTreeListColumn* col;
    col                    = optTreeList->CreateColumn();
    col->Name              = "optionName";
    col->Width             = optTreeList->Width - 5;
    col->Options->Editing  = false;
    col->Options->Focusing = false;
    col->Options->Sorting  = false;
    col->Options->Sizing   = false;
    col->Options->Moving   = false;

    col                    = optTreeList->CreateColumn();
    col->Name              = "optionUID";
    col->Visible           = false;
    col                    = optTreeList->CreateColumn();
    col->Name              = "pageIndex";
    col->Visible           = false;
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::createSplitter()
{
    splitter = new TcxSplitter ( optForm );

    splitter->Parent       = optForm;
    splitter->Control      = optTreeList;
    splitter->AutoPosition = true;
    splitter->HotZone      = new TcxXPTaskBarStyle (splitter);
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::createBottomPanel()
{
    bottomPanel = new TPanel (optForm);

    bottomPanel->Parent = optForm;
    bottomPanel->Align  = alBottom;
//    bottomPanel->Color = clGray;
//    bottomPanel->Top    = 360;
//    bottomPanel->Left   = 0;
//    bottomPanel->Width  = 600;

    bottomPanel->Height = 41;
    bottomPanel->BevelOuter = bvNone;
    Application->ProcessMessages();
    TcxButton* btn                = new TcxButton (optForm);
    btn->Parent                   = bottomPanel;
    btn->Caption                  = FMT (icsAppOptDialogOKBtnCaption);
    btn->Width                    = 75;
    btn->Height                   = 25;
    btn->OnClick                  = onOkBtnClick;
    btn->LookAndFeel->Kind        = lfStandard;
    btn->LookAndFeel->NativeStyle = true;
    vcomponents["OkBtn"]          = btn;
    Application->ProcessMessages();
/*
    ((TcxButton*)vcomponents["OkBtn"])->Top          = 2;
    ((TcxButton*)vcomponents["OkBtn"])->Left         = bottomPanel->Width - 153;
    ((TcxButton*)vcomponents["CancelBtn"])->Top      = 2;
    ((TcxButton*)vcomponents["CancelBtn"])->Left     = bottomPanel->Width  - 76;
*/
    btn->Left = optForm->ClientWidth - btn->Width*2 - 5;
    btn->Top = (bottomPanel->Height - btn->Height)/2;
    btn->Anchors.Clear();
    btn->Anchors << akRight << akTop;
    Application->ProcessMessages();

    btn                           = new TcxButton (optForm);
    btn->Parent                   = bottomPanel;
    btn->Caption                  = FMT (icsAppOptDialogCancelBtnCaption);
    btn->Cancel                   = true;
    btn->Width                    = 75;
    btn->Height                   = 25;
    btn->OnClick                  = onCancelBtnClick;
    btn->LookAndFeel->Kind        = lfStandard;
    btn->LookAndFeel->NativeStyle = true;
    vcomponents["CancelBtn"]      = btn;

    btn->Left = optForm->ClientWidth - btn->Width-2;
    btn->Top = (bottomPanel->Height - btn->Height)/2;
    btn->Anchors.Clear();
    btn->Anchors << akRight << akTop;

//    bottomPanelSpl = new TPanel (optForm);
//    bottomPanelSpl->Parent = optForm;
//    bottomPanelSpl->Align  = alBottom;
//    bottomPanelSpl->Height = 3;
//    bottomPanelSpl->BevelOuter = bvRaised;
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::fillData (TTagNode* grNode, TcxTreeListNode* treeNode)
{
    // ��������� ������ �� ����
    if ( grNode == NULL )
        return;

    TTagNode* childNode;
    TcxTreeListNode* tlNode;
    grNode = grNode->GetFirstChild();

    TAppOptionsVar*  pVar;
    UnicodeString name;

    while ( grNode != NULL )
    {
        if ( grNode->Name != "servicegroup" && ( (grNode->GetParent()->Name != "group")  || (grNode->Name == "group" && grNode->Level == 2) || (grNode->Name == "group" && grNode->Level == 3 && grNode->AV["isEmbedded"] == "no") ) )
        {
            // ������� ���� � ������
            tlNode            = treeNode->AddChild();
            tlNode->Values[0] = grNode->AV["caption"];
            tlNode->Values[1] = grNode->AV["uid"];
            tlNode->Values[2] = IntToStr (pageIndex);

            // ���������� ��������� ���� ����� ������ ���������
            TcxTabSheet* parentSheet = createPage ( optPageCtrl, grNode->AV["caption"], pageIndex++ );
            pages[grNode->AV["caption"]] = parentSheet;

            TcxPageControl* childPageCtrl = createPageControl ( parentSheet );
            pageControls[grNode->AV["uid"]] = childPageCtrl;

            TcxTabSheet* childSheet  = createPage ( childPageCtrl, grNode->AV["caption"], 0, true );
            pagesLevel2[grNode->AV["caption"]] = childSheet;

            TWinControl* parent = createGroupBox ( grNode, childSheet, Rect (childSheet->Left + 10, childSheet->Top + 5, childSheet->Left + childSheet->Width - 20, 30) );
            TRect rect          = Rect(parent->Left+5, parent->Top + 15, parent->Left + parent->Width - 5, parent->Top + 40);

            createComponents ( grNode, parent, rect );

            if ( grNode->GetChildByName( "group", true ) )
                fillData( grNode, tlNode );
        }

        grNode = grNode->GetNext();
    }
}


//---------------------------------------------------------------------------
TcxPageControl* __fastcall TAppOptDialog::createPageControl(TWinControl* parent)
{

    if ( parent == (TWinControl*)optForm )
    {
        optPageCtrl = new TcxPageControl( parent );

        optPageCtrl->Parent                   = parent;
        optPageCtrl->LookAndFeel->NativeStyle = true;
        optPageCtrl->Align                    = alClient;
        optPageCtrl->Constraints->MinWidth    = 350;
        optPageCtrl->HideTabs                 = true;
        optPageCtrl->Style                    = 10;
        return NULL;
    }
    else
    {
        TcxPageControl* pageCtrl = new TcxPageControl (parent);
        pageCtrl->Parent                   = parent;
        pageCtrl->LookAndFeel->NativeStyle = true;
        pageCtrl->Align                    = alClient;
        pageCtrl->Constraints->MinWidth    = 350;
        pageCtrl->HideTabs                 = true;
        pageCtrl->Style                    = 10;
//        vcomponents["pagectrl"+IntToStr( ((TcxTabSheet*)parent)->PageIndex )] = pageCtrl;
        return pageCtrl;
    }


}

//---------------------------------------------------------------------------

TcxTabSheet* __fastcall TAppOptDialog::createPage ( TcxPageControl* pageCtrl, UnicodeString caption, int pageIndex, bool isTabVisible)
{
    TcxTabSheet* page = new TcxTabSheet ( pageCtrl );
    page->PageControl = pageCtrl;
    page->Caption     = caption;
//    page->PageIndex   = pageIndex;
    page->TabVisible  = isTabVisible;

    return page;
/*
    TWinControl* parent = createGroupBox ( ANode, page, Rect (page->Left + 10, page->Top + 5, page->Left + page->Width - 20, 30) );
    TRect rect          = Rect(parent->Left+5, parent->Top + 15, parent->Left + parent->Width - 5, parent->Top + 40);

    createComponents ( ANode, parent, rect );
*/
}

//***************************************************************************
//              �������� ����������� ����������                             *
//***************************************************************************

int __fastcall TAppOptDialog::createComponents (TTagNode* ANode, TWinControl* groupBox, TRect rect)
{
    if      ( ANode->Name == "binary")   return createBinary (ANode, groupBox, rect);
    else if ( ANode->Name == "digit")    return createDigit  (ANode, groupBox, rect);
    else if ( ANode->Name == "choice")   return createChoice (ANode, groupBox, rect);
    else if ( ANode->Name == "text")     return createText   (ANode, groupBox, rect);
    else if ( ANode->Name == "extern")   return createExtern (ANode, groupBox, rect);
    else if ( ANode->Name == "datetime") return createDate   (ANode, groupBox, rect);
    else if ( ANode->Name == "group")    return createGroup  (ANode, groupBox, rect);

    return 0;
}

//---------------------------------------------------------------------------

int __fastcall TAppOptDialog::createGroup (TTagNode* ANode, TWinControl* groupBox, TRect rect)
{
    int ctrlsHeight = 0;
    rect = Rect (5, 15 , groupBox->Width - 5, 40);

    getDependencies(ANode);

    ANode = ANode->GetFirstChild();

    while ( ANode )
    {
        UnicodeString name = ANode->AV["name"];
        if ( ANode->Name != "group" )
        {
            ctrlsHeight += createComponents (ANode, groupBox, rect);
            rect  = Rect ( 5, ctrlsHeight + 15, groupBox->Width - 5, ctrlsHeight + 40);
        }
        else
        {
            if ( ANode->Level >= 3 && !ANode->CmpAV ("isEmbedded","no") ) //|| ( ANode->Level > 3 && ANode->AV["isEmbedded"].IsEmpty()) )
            {
                TWinControl* newGrpBox = createGroupBox ( ANode, groupBox, Rect (5, ctrlsHeight + 15, groupBox->Width - 5, ctrlsHeight + 36) );
                int result             = createGroup (ANode, newGrpBox, Rect(newGrpBox->Left + 4, newGrpBox->Top + 15, newGrpBox->Left + newGrpBox->Width - 5, newGrpBox->Top + 40));
                ctrlsHeight           += result;
                groupBox->Height      += result;
                rect  = Rect (5, ctrlsHeight + 15, groupBox->Width - 5, ctrlsHeight + 40);
            }
            else if ( ANode->Level > 3 && ANode->AV["isEmbedded"] == "no")
            {
                //�������� �� ������� ������������ ����(���), ������� ���� ���������
                TTagNode* parentNode = ANode;
                while ( parentNode )
                {
                    if ( ! pageControlExist ( parentNode->AV["uid"] ) )
                        parentNode = parentNode->GetParent();
                    else break;
                }

                //�� ���� ��� �������� ������ �� ��������� ������� �� ����
                TcxPageControl* pageCtrl = pageControls [parentNode->AV["uid"]];
                pageCtrl->HideTabs       = false;

                TcxTabSheet* page  = createPage ( pageCtrl, ANode->AV["caption"], 0, true );
                pagesLevel2[ANode->AV["uid"]] = page;

                TWinControl* parent = createGroupBox ( ANode, page, Rect (page->Left + 5, 0, page->Left + page->Width - 5, 30) );
                TRect rect          = Rect(parent->Left+5, parent->Top + 15, parent->Left + parent->Width - 5, parent->Top + 40);
                createComponents ( ANode, parent, rect );
            }

        }

        ANode = ANode->GetNext();
    }

    return ctrlsHeight + 25;
}

//---------------------------------------------------------------------------

int __fastcall TAppOptDialog::createBinary (TTagNode* ANode, TWinControl* parent, TRect rect)
{
    getDependencies(ANode);
    return createCheckBox (ANode, parent, rect);
}
//---------------------------------------------------------------------------

int __fastcall TAppOptDialog::createDigit (TTagNode* ANode, TWinControl* parent, TRect rect)
{
    getDependencies(ANode);
    return createDigitMaskEdit(ANode, parent, Rect ( parent->Left+5, rect.Top, parent->ClientWidth-5, rect.Bottom) );
}
//---------------------------------------------------------------------------

int __fastcall TAppOptDialog::createChoice (TTagNode* ANode, TWinControl* parent, TRect rect)
{
    getDependencies(ANode);
    return createRadioButtons(ANode, parent, rect);
}
//---------------------------------------------------------------------------

int __fastcall TAppOptDialog::createText (TTagNode* ANode, TWinControl* parent, TRect rect)
{
    getDependencies(ANode);
    return createButtonEdit(ANode, parent, Rect ( parent->Left+5, rect.Top, parent->ClientWidth-5, rect.Bottom) );
}
//---------------------------------------------------------------------------

int __fastcall TAppOptDialog::createExtern (TTagNode* ANode, TWinControl* parent, TRect rect)
{
    int h = createButton(ANode, parent, rect);
    getDependencies(ANode);
    return h;
}
//---------------------------------------------------------------------------

int __fastcall TAppOptDialog::createDate (TTagNode* ANode, TWinControl* parent, TRect rect)
{
    getDependencies(ANode);
    return createDateEdit(ANode, parent, Rect ( parent->Left+5, rect.Top, parent->ClientWidth-5, rect.Bottom) );
}



//***************************************************************************
//              �������� ����������� �� �����                               *
//***************************************************************************

//---------------------------------------------------------------------------

int __fastcall TAppOptDialog::createButtonEdit (TTagNode* ANode, TWinControl* parent, TRect rect)
{
    AOButtonEdit* me = new AOButtonEdit (parent, rect);

    me->caption                           = ANode->AV["caption"] + ":";
//    me->edit->Text                        = AOptions->getOption(ANode->AV["name"])->isEmpty ? (UnicodeString)" ": (UnicodeString)AOptions->getOption(ANode->AV["name"])->Value;
    me->edit->Text                        = AOptions->Vals[ANode->AV["name"]].AsString;//getOption(ANode->AV["name"])->isEmpty ? (UnicodeString)" ": (UnicodeString)AOptions->getOption(ANode->AV["name"])->Value;
    me->edit->node                        = ANode;
    me->Anchors << akLeft << akRight;
    me->edit->Properties->Alignment->Horz = taRightJustify;
    me->edit->Properties->EditMask        = ANode->AV["mask"];
    me->edit->Properties->OnChange        = onButtonEditChange;

    me->edit->Style->LookAndFeel->Kind         = lfStandard;
    me->label->Style->LookAndFeel->Kind        = lfStandard;
    me->edit->Style->LookAndFeel->NativeStyle  = true;
    me->label->Style->LookAndFeel->NativeStyle = true;

    //���� ���� ����������� ������
    if ( ANode->AV["hasButton"] == "yes")
    {
        me->edit->Properties->OnButtonClick = onButtonEditBtnClick;
        if ( ANode->AV["readonly"] == "yes" )
        {
            //���� ��������� ���� ������� ������ ����� ������
            me->edit->Properties->ReadOnly  = true;
            me->edit->Properties->ViewStyle = vsHideCursor;
        }
    }
    else
        me->edit->Properties->Buttons->Clear();


    if ( ANode->AV["required"] == "yes")
        me->edit->Style->Color = requiredColor;
    else
        me->edit->Style->Color = normalColor;

    parent->Height              += me->Height;
    components[ANode->AV["uid"]] = me;

    // ����� AfterComponentCreate
    if ( FAfterComponentCreate )
        FAfterComponentCreate ( ANode, me );

    return me->Height;
}

//---------------------------------------------------------------------------

int __fastcall TAppOptDialog::createDigitMaskEdit (TTagNode* ANode, TWinControl* parent, TRect rect)
{ //[0-9]*[.][0-9]*

    AOButtonEdit* me = new AOButtonEdit (parent, rect);

    me->caption                           = ANode->AV["caption"] + ":";
    me->label->Update();
//    me->edit->Text                        = AOptions->getOption(ANode->AV["name"])->isEmpty ? (UnicodeString)" ": (UnicodeString)AOptions->getOption(ANode->AV["name"])->Value;
    me->edit->Text                        = AOptions->Vals[ANode->AV["name"]].AsString; //getOption(ANode->AV["name"])->isEmpty ? (UnicodeString)" ": (UnicodeString)AOptions->getOption(ANode->AV["name"])->Value;
    me->edit->node                        = ANode;
//    me->edit->Left = me->label->Left+me->label->Width+3;
    me->labelWidthPercent = 79;
    me->Anchors << akLeft << akRight;
    me->edit->Properties->Alignment->Horz = taRightJustify;
    me->edit->Properties->MaskKind        = emkRegExprEx;
    me->edit->Properties->EditMask        = makeDigitEditMask ( ANode );
    me->edit->Properties->OnChange        = onCurrencyEditChange;
    me->edit->Properties->OnValidate      = onCurrEditValidate;

    me->edit->Style->LookAndFeel->Kind         = lfStandard;
    me->label->Style->LookAndFeel->Kind        = lfStandard;
    me->edit->Style->LookAndFeel->NativeStyle  = true;
    me->label->Style->LookAndFeel->NativeStyle = true;

    me->edit->Properties->Buttons->Clear();


    if ( ANode->AV["required"] == "yes")
        me->edit->Style->Color = requiredColor;
    else
        me->edit->Style->Color = normalColor;

    parent->Height              += me->Height;
    components[ANode->AV["uid"]] = me;

    // ����� AfterComponentCreate
    if ( FAfterComponentCreate )
        FAfterComponentCreate ( ANode, me );

    return me->Height;

}

//---------------------------------------------------------------------------
UnicodeString TAppOptDialog::makeDigitEditMask ( TTagNode* ANode )
{
    int digits  = ANode->AV["digits"].IsEmpty()  ? 0 : ANode->AV["digits"].ToInt() ;
    int decimal = ANode->AV["decimal"].IsEmpty() ? 0 : ANode->AV["decimal"].ToInt();

    UnicodeString formatStr = "";

    if ( digits && digits <=9 )
         formatStr = "[0-9]{" + IntToStr(digits) + "}";
    else if ( digits )
        formatStr = "[0-9]{9}";
    else formatStr = "[0-9]{1,9}";

    if ( decimal && decimal <= 10 )
         formatStr += "[,][0-9]{" + IntToStr(decimal) + "}";
    else if ( decimal )
        formatStr += "[,][0-9]{10}";
    else
    {
        formatStr += "|"+formatStr;
        formatStr += "[,][0-9]{1,10}";
    }

    return formatStr;
}

//---------------------------------------------------------------------------

int __fastcall TAppOptDialog::createButton (TTagNode* ANode, TWinControl* parent, TRect rect)
{
    AOButton* btn = new AOButton (parent);

    btn->Parent                   = parent;
    btn->Caption                  = ANode->AV["caption"];
    btn->Top                      = rect.Top;
    btn->Left                     = rect.Left;
    btn->Width                    = parent->ClientWidth - 10;
    btn->Height                   = 21;//rect.Bottom - rect.Top - 5;
    btn->OnClick                  = onExtEditBtnClick;
    btn->node                     = ANode;
    btn->LookAndFeel->Kind        = lfStandard;
    btn->LookAndFeel->NativeStyle = true;
    btn->Anchors << akLeft;
    btn->Anchors << akRight;

    parent->Height += btn->Height;
    components[ANode->AV["uid"]] = btn;

    // ����� AfterComponentCreate
    if ( FAfterComponentCreate )
        FAfterComponentCreate ( ANode, btn );

    return btn->Height;
}

//---------------------------------------------------------------------------

int __fastcall TAppOptDialog::createDateEdit (TTagNode* ANode, TWinControl* parent, TRect rect)
{
    AODateEdit* de = new AODateEdit ( parent, rect );

    de->caption      = ANode->AV["caption"];
//    de->edit->Date   = AOptions->getOption(ANode->AV["name"])->isEmpty ? 0 : (TDateTime)AOptions->getOption(ANode->AV["name"])->Value;
    de->edit->Date   = AOptions->Vals[ANode->AV["name"]].AsDateTime; //getOption(ANode->AV["name"])->isEmpty ? 0 : (TDateTime)AOptions->getOption(ANode->AV["name"])->Value;
    de->edit->node   = ANode;
    de->edit->Properties->OnChange = onDateEditChange;

    de->label->Style->LookAndFeel->Kind        = lfStandard;
    de->label->Style->LookAndFeel->NativeStyle = true;
    de->edit->Style->LookAndFeel->Kind         = lfStandard;
    de->edit->Style->LookAndFeel->NativeStyle  = true;

    if ( ANode->AV["required"] == "yes" )
        de->edit->Style->Color = requiredColor;
    else
        de->edit->Style->Color = normalColor;

    parent->Height += de->Height;
    components[ANode->AV["uid"]] = de;

    // ����� AfterComponentCreate
    if ( FAfterComponentCreate )
        FAfterComponentCreate ( ANode, de );

    return de->Height;
}
//---------------------------------------------------------------------------

TWinControl* __fastcall TAppOptDialog::createGroupBox (TTagNode* ANode, TWinControl* parent, TRect rect)
{
    TcxGroupBox* grb = new TcxGroupBox (parent);

    grb->Parent  = parent;
    if ( ANode->Name != "group")
        grb->Style->BorderStyle = ebsNone;
    else
        grb->Caption = " " + ANode->AV["caption"] + " ";

    if ( ANode->Level == 2 || (ANode->Name == "group" && ANode->Level == 3 && ANode->AV["isEmbedded"] == "no") )
    {
        grb->Top     = parent->Top + 2;
        grb->Left    = parent->Left + 2;
        grb->Width   = parent->Width - 4;
        grb->Height  = parent->Height - 4;
        grb->Align   = alClient;
        Application->ProcessMessages ( );
        
    }
    else
    {
        grb->Top     = rect.Top;
        grb->Left    = rect.Left;
        grb->Width   = rect.Right - rect.Left;
        grb->Height  = rect.Bottom - rect.Top;
        grb->Anchors << akLeft << akRight;
    }

    vcomponents[ANode->AV["uid"]] = grb;

    // ����� AfterComponentCreate
    if ( FAfterComponentCreate )
        FAfterComponentCreate ( ANode, grb );

    return grb;
}

//---------------------------------------------------------------------------

int __fastcall TAppOptDialog::createCheckBox (TTagNode* ANode, TWinControl* parent, TRect rect)
{
    AOCheckBox* chBox = new AOCheckBox ( parent, rect );

    chBox->Caption = ANode->AV["caption"];
    chBox->node    = ANode;

    chBox->Style->LookAndFeel->Kind        = lfStandard;
    chBox->Style->LookAndFeel->NativeStyle = true;

    chBox->Properties->OnChange = onCheckBoxChange;
/*
    if ( AOptions->getOption(ANode->AV["name"])->Value )
        chBox->Checked = true;
    else
        chBox->Checked = false;
*/
    chBox->Checked = AOptions->Vals[ANode->AV["name"]].AsBool;

    parent->Height += chBox->Height;
    components[ANode->AV["uid"]] = chBox;

    // ����� AfterComponentCreate
    if ( FAfterComponentCreate )
        FAfterComponentCreate ( ANode, chBox );

    return chBox->Height;
}

//---------------------------------------------------------------------------

int __fastcall TAppOptDialog::createRadioButtons (TTagNode* ANode, TWinControl* parent, TRect rect)
{
    AORadioGroup* rg = new AORadioGroup ( parent );

    rg->Parent  = parent;
    rg->Top     = rect.Top;
    rg->Left    = rect.Left;
    rg->Width   = parent->ClientWidth - 10;
    rg->Height  = rect.Bottom - rect.Top;
    rg->Anchors << akLeft;
    rg->Anchors << akRight;
    rg->Caption = ANode->AV["caption"];
    rg->node    = ANode;

    rg->Style->LookAndFeel->Kind        = lfStandard;
    rg->Style->LookAndFeel->NativeStyle = true;

    rg->Properties->OnChange = onRadioGroupChange;
    components[ANode->AV["uid"]] = rg;

//    UnicodeString checkedVal = AOptions->getOption(ANode->AV["name"])->Value;
    UnicodeString checkedVal = AOptions->Vals[ANode->AV["name"]].AsString; //getOption(ANode->AV["name"])->Value;
    TcxRadioGroupItem* rgi;

    TTagNode* choiceNode = ANode->GetChildByName("choicevalue");

    while ( choiceNode != NULL )
    {
        rgi          = rg->Properties->Items->Add();
        rgi->Caption = choiceNode->AV["caption"];
        rgi->Value   = choiceNode->AV["value"];

        if ( rgi->Value == checkedVal )
             rg->ItemIndex = rgi->Index;
        rg->Height  += 15;

        choiceNode = choiceNode->GetNext("choicevalue");
    }

    if ( ANode->AV["required"] == "yes" && rg->ItemIndex == -1 )
    {
        rg->Style->BorderColor   = requiredColor;
        rg->Style->BorderStyle   = ebsThick;
    }

    parent->Height += rg->Height + 5;

    // ����� AfterComponentCreate
    if ( FAfterComponentCreate )
        FAfterComponentCreate ( ANode, rg );

    return rg->Height + 5;
}

//***************************************************************************
//                                                                          *
//                               �������                                    *
//                                                                          *
//***************************************************************************

void __fastcall TAppOptDialog::onTreeListResize   ( TObject* Sender )
{
    optTreeList->Columns[0]->Width = optTreeList->Width - 10;
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::onFormClose ( TObject* Sender, TCloseAction &Action )
{
    cleanup();
}

//---------------------------------------------------------------------------
void __fastcall TAppOptDialog::cleanup()
{
    // �������� ��������� �����������

    //���������� ���������� ������
    for (TAODataCompMap::iterator i = components.begin(); i != components.end(); i++)
    {
      if ( i->second )
       {
         delete i->second;
         i->second = NULL;
       }
    }
    components.clear();
    vcomponents.clear();

    //-- �������� ������� ������
    for (TAOPagesMap::iterator i = pagesLevel2.begin(); i != pagesLevel2.end(); i++)
    {
      if (i->second)
       {
         delete i->second;
         i->second = NULL;
       }
    }
    pagesLevel2.clear();

    //-- ���������� ������� ������� ������
    for (TAOPageCtrlMap::iterator i = pageControls.begin(); i != pageControls.end(); i++)
    {
      if (i->second)
       {
         delete i->second;
         i->second = NULL;
       }
    }
    pageControls.clear();

    //-- �������� ������� ������
    for (TAOPagesMap::iterator i = pages.begin(); i != pages.end(); i++)
    {
      if (i->second)
       {
         delete i->second;
         i->second = NULL;
       }
    }
    pages.clear();

    delete optTreeList;
    delete optPageCtrl;
    delete bottomPanel;
    delete splitter;
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::onOkBtnClick ( TObject* Sender )
{
    if ( !canSave() )
    {
        ShowMessage (FMT(icsAppOptDialogNeed2FillReqs));
        return;
    }

    if ( FBeforeSave )
    {
         bool canSave = true;
         FBeforeSave ( Options->OptDefNode->GetChildByName("content"), canSave );
         if ( !canSave )
            return;
    }

    saveChanges();
    optForm->ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::onCancelBtnClick ( TObject* Sender )
{
    optForm->ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::focusedNodeChanged(TcxCustomTreeList *Sender, TcxTreeListNode *APrevFocusedNode, TcxTreeListNode *AFocusedNode)
{
    if ( AFocusedNode != NULL )
        optPageCtrl->ActivePageIndex = AFocusedNode->Values[2];
}

//---------------------------------------------------------------------------
void __fastcall TAppOptDialog::onMouseWheelUp ( TObject *Sender, TShiftState Shift, const TPoint &MousePos, bool &Handled)
{
/*    TComponent * temp = vcomponents[(UnicodeString)optTreeList->FocusedNode->Values[1] + "scrollbox"];
    if ( ((TScrollBox*)temp)->VertScrollBar->Visible )
         ((TScrollBox*)temp)->VertScrollBar->Position -= 20;
*/
}
//---------------------------------------------------------------------------
void __fastcall TAppOptDialog::onMouseWheelDown ( TObject *Sender, TShiftState Shift, const TPoint &MousePos, bool &Handled)
{
/*
    TComponent * temp = vcomponents[(UnicodeString)optTreeList->FocusedNode->Values[1] + "scrollbox"];
    if ( ((TScrollBox*)temp)->VertScrollBar->Visible )
         ((TScrollBox*)temp)->VertScrollBar->Position += 20;
*/
}
//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::saveChanges ()
{
    //��������� ���������
    TTagNode* node;
    for ( TAODataCompMap::iterator i = components.begin(); i != components.end(); i++)
    {
        node = AOptions->OptDefNode->GetTagByUID ( i->first );
        if ( node )
            writeValue (node->Name, node->AV["name"], i->second);
    }
}

//---------------------------------------------------------------------------
bool __fastcall TAppOptDialog::canSave ()
{
    TTagNode* node;
    for ( TAODataCompMap::iterator i = components.begin(); i != components.end(); i++)
    {
        node = AOptions->OptDefNode->GetTagByUID ( i->first );
        if ( node )
        {
            if ( node->Name == "digit")
            {
                if (node->CmpAV("required", "yes") && ((AOButtonEdit*)i->second)->edit->Text.Trim().IsEmpty() && ((AOButtonEdit*)i->second)->edit->Enabled)
                    return false;
            }
            else if ( node->Name == "text")
            {
                if (node->CmpAV("required", "yes") && ((AOButtonEdit*)i->second)->edit->Text.Trim().IsEmpty() && ((AOButtonEdit*)i->second)->edit->Enabled )
                    return false;
            }
            else if ( node->Name == "choice")
            {
                if (node->CmpAV("required", "yes") && ((TcxRadioGroup*)i->second)->ItemIndex == -1 && ((TcxRadioGroup*)i->second)->Enabled)
                    return false;
             }
        }
    }
    return true;
}
//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::writeValue (UnicodeString tagType, UnicodeString optionName, TComponent* comp)
{
    if  ( tagType == "binary" )
    {
        /*
        int val = ((TcxCheckBox*)comp)->Checked?1:0;
        AOptions->setOption(optionName, val );
        */
        AOptions->Vals[optionName] = ((TcxCheckBox*)comp)->Checked;
    }
    else if  ( tagType == "digit" )
    {
/*
        UnicodeString text = ((AOButtonEdit*)comp)->edit->Text.IsEmpty() ? (UnicodeString)"empty" : ((AOButtonEdit*)comp)->edit->Text;
        AOptions->setOption(optionName, text );
*/
        AOptions->Vals[optionName] = ((AOButtonEdit*)comp)->edit->Text.Trim();
    }
    else if  ( tagType == "choice" )
    {
/*
        int index = ((TcxRadioGroup*)comp)->ItemIndex;
        if ( index >= 0 )
            AOptions->setOption(optionName, Variant((int)((TcxRadioGroup*)comp)->Properties->Items->Items[index]->Value) );
        else
            AOptions->setOption(optionName, -1 );
*/
        int index = ((TcxRadioGroup*)comp)->ItemIndex;
        if ( index >= 0 )
            AOptions->Vals[optionName] = (int)((TcxRadioGroup*)comp)->Properties->Items->Items[index]->Value; // setOption(optionName, Variant() );
        else
            AOptions->Vals[optionName] = -1;
    }
    else if  ( tagType == "text" )
    {
/*
        AOptions->setOption(optionName, ((AOButtonEdit*)comp)->edit->Text );
*/
        AOptions->Vals[optionName] = ((AOButtonEdit*)comp)->edit->Text.Trim();
    }
    else if  ( tagType == "datetime" )
    {
/*
        AOptions->setOption(optionName, ((AODateEdit*)comp)->edit->EditValue );
*/
        AOptions->Vals[optionName] = TDateTime((double)((AODateEdit*)comp)->edit->EditValue);
//'_fastcall System::TDateTime::TDateTime(const System::TDateTime &) at c:\ics\embarcadero\rad studio\8.0\include\windows\rtl\systdate.h:48'
//'_fastcall System::TDateTime::TDateTime(const double) at c:\ics\embarcadero\rad studio\8.0\include\windows\rtl\systdate.h:49'
    }
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::onExtEditBtnClick ( TObject* Sender)
{
    // ��������� ������� �� ������ �� ������ �������� ��������������
    if ( !FExtEdit )
        return;

    TTagNode* ANode = ((AOButton*)Sender)->node;
    TTagNode* childNode = ANode->GetFirstChild();

    TAOVarMap extVars;
    while ( childNode != NULL )
    {
        extVars[childNode->AV["name"]] = AOptions->Vars[childNode->AV["name"]];
        childNode = childNode->GetNext();
    }

    FExtEdit ( ANode, extVars );

/*
    for (TAOVarMap::iterator i = extVars.begin(); i != extVars.end(); i++)
        AOptions->setOption (i->first, i->second);
*/
    for (TAOVarMap::iterator i = extVars.begin(); i != extVars.end(); i++)
        AOptions->Vals[i->first] = i->second;
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::onButtonEditBtnClick ( TObject *Sender, int AButtonIndex)
{
    if ( !FBtnEdit )
        return;

    ((TcxButtonEdit*)Sender)->Text = FBtnEdit ( ((AOButtEdit*)Sender)->node, AButtonIndex, (AOButtonEdit*)Sender );
}

//---------------------------------------------------------------------------

//************************************************************************
//                   ������ �� ���������� � ���������                    *
//************************************************************************

void __fastcall TAppOptDialog::onButtonEditChange ( TObject* Sender )
{
    // �������� �� ������������ ����������� �����
    workoutDependencies ( ((AOButtEdit*)Sender)->node->AV["uid"] );
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::onCurrencyEditChange ( TObject *Sender )
{
    if ( !((AOButtEdit*)Sender)->Text.IsEmpty() )
    {
        if ( ((AOButtEdit*)Sender)->Text[((AOButtEdit*)Sender)->Text.Length()] == ',' )
           return;

        double val = StrToFloat ( ((AOButtEdit*)Sender)->Text );
        double min = StrToFloat ( ((AOButtEdit*)Sender)->node->AV["min"] );
        double max = StrToFloat ( ((AOButtEdit*)Sender)->node->AV["max"] );

        if ( val < min || val > max)
            return;
    }

    // �������� �� ������������ ����������� �����
    workoutDependencies ( ((AOButtEdit*)Sender)->node->AV["uid"] );
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::onCurrEditValidate   ( TObject *Sender, Variant &DisplayValue, TCaption &ErrorText, bool &Error)
{
    if ( DisplayValue.Type() == 256 ) //&& DisplayValue == NULL )
        if ( DisplayValue.VString == NULL )
            return;

    double min = StrToFloat ( ((AOButtEdit*)Sender)->node->AV["min"] );
    double max = StrToFloat ( ((AOButtEdit*)Sender)->node->AV["max"] );

    if ( DisplayValue < min )
    {
        ErrorText = FMT1(icsAppOptDialogOutOfRangeLessThanMin, min);
        Error     = true;
    }
    else if ( DisplayValue > max )
    {
        ErrorText = FMT1(icsAppOptDialogOutOfRangeMoreThanMax, max);
        Error     = true;
    }
    Error = false;
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::onDateEditChange ( TObject *Sender )
{
    // �������� �� ������������ ����������� �����
    workoutDependencies ( ((AODEdit*)Sender)->node->AV["uid"] );
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::onCheckBoxChange ( TObject *Sender )
{
    // �������� �� ������������ ����������� �����
    workoutDependencies ( ((AOCheckBox*)Sender)->node->AV["uid"] );
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::onRadioGroupChange ( TObject* Sender )
{
    // �������� �� ������������ ����������� �����
    workoutDependencies ( ((AORadioGroup*)Sender)->node->AV["uid"] );
}

//*************************************************************************
//                                                                        *
//                        ������� ������������                            *
//                                                                        *
//*************************************************************************

void __fastcall TAppOptDialog::getDependencies(TTagNode* ANode)
{
    // ����������� ������ ������������
    if ( !ANode ) return;

    UnicodeString slave  = ANode->AV["uid"];

    ANode = ANode->GetChildByName("actual",true);
    if ( !ANode ) return;

    UnicodeString master = ANode->AV["ref"];

    if ( dependencies[master].IsEmpty() )
        dependencies[master] = slave;
    else
        dependencies[master] += "," + slave;
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::workoutDependencies (UnicodeString masterUID)
{
    if ( masterUID.IsEmpty() )
        return;

    UnicodeString UIDS = dependencies[masterUID];
    if ( UIDS.IsEmpty() )
        return;

    int dotindex = UIDS.Pos(",");
    if ( dotindex )
    {
        while ( dotindex )
        {
            workoutDependency ( UIDS.SubString(1,dotindex-1) );
            UIDS.Delete (1,dotindex);
            dotindex = UIDS.Pos(",");
        }
    }

    workoutDependency ( UIDS );
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::workoutDependency ( UnicodeString UID )
{
    // �������� ��������� ������������ ���������� �� ��� ����

    TTagNode* node        = Options->OptDefNode->GetTagByUID(UID);

    if ( !node || node->GetChildByName("actuals") == NULL )
        return;

    TComponent* component = dataComponentExist(UID)?components[UID]:visualComponentExist(UID)?vcomponents[UID]:NULL;

    if( !component )
        return;
        
    UnicodeString condition  = node->GetChildByName("actuals")->AV["condition"];

    if ( !isValidCondition( node ) )
    {
        UnicodeString msg = FMT1(icsAppOptDialogConditionError, condition);
        Application->MessageBox (msg.c_str(), FMT(icsAppOptDialogErrMsmCaption).c_str(), MB_ICONERROR);
        return;
    }

    // ���������� ������ ����������

    TCondVarsMap  vars;
    UnicodeString    value;
    componentType type;
    TTagNode* varNode = node->GetChildByName("actuals")->GetFirstChild();

    while ( varNode )
    {
        TTagNode* referedNode = Options->OptDefNode->GetTagByUID(varNode->AV["ref"]);
        UnicodeString aType = referedNode->Name;
        if      ( aType == "text" )      type = tButtonEdit;
        else if ( aType == "digit" )     type = tCurrEdit;
        else if ( aType == "datetime" )  type = tDate;
        else if ( aType == "binary" )    type = tCheckBox;
        else if ( aType == "choice" )    type = tRadioButtonGroup;
        else
        {
            UnicodeString msg = FMT2( icsAppOptDialogWrongLink, aType, condition);
            Application->MessageBox (msg.c_str(), FMT(icsAppOptDialogErrMsmCaption).c_str(), MB_ICONERROR);
            return;
        }

        TComponent* referedComp = dataComponentExist(referedNode->AV["uid"])?components[referedNode->AV["uid"]]:NULL;
        if ( !referedComp )
        {   // ���� ��������� ��� �� ������
            varNode = varNode->GetNext("actual");
            continue;
        }

        switch ( type )
        {
            case tButtonEdit:       value = ((AOButtonEdit*)referedComp)->edit->Enabled ?((AOButtonEdit*)referedComp)->edit->Text:(UnicodeString)" ";
                                    break;
            case tCurrEdit:         value = ((AOButtonEdit*)referedComp)->edit->Text;
                                    break;
            case tDate:             value = ((AODateEdit*)referedComp)->edit->Date;
                                    break;
            case tCheckBox:         value = ((TcxCheckBox*)referedComp)->Enabled ? ((TcxCheckBox*)referedComp)->Checked?1:0 : NULL;
                                    break;
            case tRadioButtonGroup: int index = ((TcxRadioGroup*)referedComp)->ItemIndex;
                                    value = index>=0 ?((TcxRadioGroup*)referedComp)->Properties->Items->Items[index]->Value:Variant("");
                                    break;
        }

        vars[varNode->AV["name"]] = value.IsEmpty() ? undefinedValue : value;
        varNode = varNode->GetNext("actual");
    } //while ( varNode )

    // ������ ������� ������������
    TEvalCond* eval = new TEvalCond;
    if ( FUnresolvedRes )
        eval->OnUnresolvedResult = onUnresolvedResult;

    bool result     = eval->evalCondition ( condition, vars );

    type = getTypeByName ( node->Name );

    switch ( type )
    {
        case tButtonEdit:       ((AOButtonEdit*)component)->label->Enabled   = result;
                                ((AOButtonEdit*)component)->edit->Enabled    = result;
                                break;
        case tCurrEdit:         ((AOButtonEdit*)component)->label->Enabled = result;
                                ((AOButtonEdit*)component)->edit->Enabled  = result;
                                break;
        case tDate:             ((AODateEdit*)component)->label->Enabled     = result;
                                ((AODateEdit*)component)->edit->Enabled      = result;
                                break;
        case tCheckBox:         ((TcxCheckBox*)component)->Enabled     = result;
                                break;
        case tGroup:            ((TcxGroupBox*)component)->Enabled     = result;
                                changeGroupComponentsStatus ( (TcxGroupBox*)component, result );
                                break;
        case tRadioButtonGroup: ((TcxRadioGroup*)component)->Enabled   = result;
                                break;
        case tExtern:           ((TcxButton*)component)->Enabled       = result;
                                break;
        case tNone:             break;
    }

    // ������������ ����������, ��������� �� �������
    workoutDependencies (UID);
}

//---------------------------------------------------------------------------

bool __fastcall TAppOptDialog::isValidCondition( TTagNode* node )
{
    // �������� �� ��, ��� ��� ������������ ����������
    //��������� �� ��� �� ��������, ��� � ��������� ���������
    TTagNode* dependedNodeParent = node->GetParent("group");

    if ( dependedNodeParent )
    {
        while ( dependedNodeParent->GetParent("group") )
            dependedNodeParent = dependedNodeParent->GetParent("group");
    }

    TTagNode* referedNodes, *referedNodeParent;
    referedNodes = node->GetChildByName("actuals")->GetFirstChild();

    while ( referedNodes )
    {
        TTagNode* referedNode = Options->OptDefNode->GetTagByUID(referedNodes->AV["ref"]);
        referedNodeParent     = referedNode->GetParent ("group");

        if ( referedNodeParent )
        {
            while ( referedNodeParent->GetParent ("group") )
                referedNodeParent = referedNodeParent->GetParent ("group");
        }

        if ( dependedNodeParent != referedNodeParent )
            return false;
        else if ( dependedNodeParent == referedNodeParent && dependedNodeParent == NULL )
            return false;

        referedNodes = referedNodes->GetNext("actual");
    }

    return true;
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::changeGroupComponentsStatus ( TcxGroupBox* groupBox, bool result )
{
    // ��� ��������� ���\���� ������, �������� ��������������
    // ���\���� �� �����������

    for ( TAODataCompMap::iterator i = components.begin(); i != components.end(); i++)
    {
         // ������� ���\���� ��� ������� ����������
        TTagNode* node = AOptions->OptDefNode->GetTagByUID(i->first);

        if ( !node ) return;

        componentType type;
        type = getTypeByName ( node->Name );

        switch (type)
        {
            case tButtonEdit:       if (((AOButtonEdit*)i->second)->Parent     == groupBox )
                                    {
                                        ((AOButtonEdit*)i->second)->label->Enabled     = result;
                                        ((AOButtonEdit*)i->second)->edit->Enabled      = result;
                                    }
                                    break;
            case tCurrEdit:         if (((AOButtonEdit*)i->second)->Parent == groupBox )
                                    {
                                        ((AOButtonEdit*)i->second)->label->Enabled = result;
                                        ((AOButtonEdit*)i->second)->edit->Enabled  = result;
                                    }
                                    break;
            case tDate:             if (((AODateEdit*)i->second)->Parent     == groupBox )
                                    {
                                        ((AODateEdit*)i->second)->label->Enabled     = result;
                                        ((AODateEdit*)i->second)->edit->Enabled      = result;
                                    }
                                    break;
            case tCheckBox:         if (((TcxCheckBox*)i->second)->Parent     == groupBox )
                                        ((TcxCheckBox*)i->second)->Enabled     = result;
                                    break;
            case tRadioButtonGroup: if (((TcxRadioGroup*)i->second)->Parent   == groupBox )
                                        ((TcxRadioGroup*)i->second)->Enabled   = result;
                                    break;
            case tExtern:           if (((TcxButton*)i->second)->Parent       == groupBox )
                                        ((TcxButton*)i->second)->Enabled       = result;
                                    break;
            case tNone:             break;
        }
    } // for

    for ( TAOVisualCompMap::iterator i = vcomponents.begin(); i != vcomponents.end(); i++)
    {
        // ������� ���/���� ��� ��������� �����
        TTagNode* node = AOptions->OptDefNode->GetTagByUID(i->first);

        if ( !node || !node->CmpName("group") ) continue;

        if (((TcxGroupBox*)i->second)->Parent     == groupBox )
        {
            ((TcxGroupBox*)i->second)->Enabled     = result;
            changeGroupComponentsStatus ( (TcxGroupBox*)i->second, result );
        }
    }

}

//---------------------------------------------------------------------------

bool __fastcall TAppOptDialog::dataComponentExist ( UnicodeString UID )
{
    TAODataCompMap::iterator rc = components.find(UID);
    return rc == components.end() ? false:true;
}

//---------------------------------------------------------------------------

bool __fastcall TAppOptDialog::visualComponentExist ( UnicodeString UID )
{
    TAOVisualCompMap::iterator rc = vcomponents.find(UID);
    return rc == vcomponents.end() ? false:true;
}

//---------------------------------------------------------------------------

bool __fastcall TAppOptDialog::pageControlExist ( UnicodeString UID )
{
    TAOPageCtrlMap::iterator rc = pageControls.find(UID);
    return rc == pageControls.end() ? false:true;
}

//---------------------------------------------------------------------------

componentType TAppOptDialog::getTypeByName ( UnicodeString name )
{
    componentType type;

    if      ( name == "text" )      type = tButtonEdit;
    else if ( name == "digit" )     type = tCurrEdit;
    else if ( name == "datetime" )  type = tDate;
    else if ( name == "binary" )    type = tCheckBox;
    else if ( name == "group" )     type = tGroup;
    else if ( name == "choice" )    type = tRadioButtonGroup;
    else if ( name == "extern" )    type = tExtern;
    else                            type = tNone;

    return type;
}
//*************************************************************************
//                                                                        *
//                        ������ ����������                               *
//                                                                        *
//*************************************************************************

Variant __fastcall TAppOptDialog::getVarByName (UnicodeString name)
{
    TTagNode* node = Options->OptDefNode->GetChildByAV("", "name", name, true);
    return node == NULL ? Variant("") : getVarValue (node);
}

//---------------------------------------------------------------------------

Variant __fastcall TAppOptDialog::getVarByUID  (UnicodeString UID)
{
    TTagNode* node = Options->OptDefNode->GetTagByUID (UID);
    return node == NULL ? Variant("") : getVarValue (node);
}

//---------------------------------------------------------------------------

Variant __fastcall TAppOptDialog::getVarValue ( TTagNode* ANode )
{
    if ( !ANode ) return Variant("");

    componentType type = getTypeByName (ANode->Name);

    UnicodeString uid = ANode->AV["uid"];

    if ( !dataComponentExist (uid) )
        return Variant ("");

    switch (type)
    {
    case tButtonEdit:       return ((AOButtonEdit*)components[uid])->edit->Text;
    case tCurrEdit:         return ((AOButtonEdit*)components[uid])->edit->Text;
    case tDate:             return ((AODateEdit*)components[uid])->edit->Text;
    case tCheckBox:         return ((TcxCheckBox*)components[uid])->Checked;
    case tRadioButtonGroup: return ((TcxRadioGroup*)components[uid])->ItemIndex;
    default:                return Variant("");
    }
}

//---------------------------------------------------------------------------

void __fastcall    TAppOptDialog::setVarByName (UnicodeString name, Variant value)
{
    TTagNode* node = Options->OptDefNode->GetChildByAV("", "name", name, true);
    setVarValue (node, value);
}

//---------------------------------------------------------------------------

void __fastcall    TAppOptDialog::setVarByUID  (UnicodeString UID, Variant value)
{
    TTagNode* node = Options->OptDefNode->GetTagByUID (UID);
    setVarValue (node, value);
}

//---------------------------------------------------------------------------

void __fastcall TAppOptDialog::setVarValue ( TTagNode* ANode, Variant value )
{
/*
    if ( !ANode )
        throw EAppOptDialogError ( FMT(icsAppOptDialogSetVarErrorNoNode) );

    componentType type = getTypeByName (ANode->Name);
    UnicodeString uid     = ANode->AV["uid"];

    if ( !dataComponentExist (uid) )
        throw EAppOptDialogError ( FMT(icsAppOptDialogSetVarErrorNoComponent) );

    switch (type)
    {
    case tButtonEdit:       ((AOButtonEdit*)components[uid])->edit->Text   = value;
                            AOptions->setOption( ANode->AV["name"], ((AOButtonEdit*)components[uid])->edit->Text );
                            break;
    case tCurrEdit:         ((AOButtonEdit*)components[uid])->edit->Text   = value;
                            AOptions->setOption( ANode->AV["name"], ((AOButtonEdit*)components[uid])->edit->Text );
                            break;
    case tDate:             if (value.Type() != 7)
                                throw EAppOptDialogError ( FMT(icsAppOptDialogSetVarErrorArgumentNotADate) );
                            ((AODateEdit*)components[uid])->edit->Date     = value;
                            AOptions->setOption( ANode->AV["name"], ((AODateEdit*)components[uid])->edit->Date );
                            break;
    case tCheckBox:         if (value.Type() != 2 && value.Type() != 3 )
                                throw EAppOptDialogError ( FMT(icsAppOptDialogSetVarErrorArgumentNotAInt) );
                            ((TcxCheckBox*)components[uid])->Checked       = value;
                            AOptions->setOption( ANode->AV["name"], ((TcxCheckBox*)components[uid])->Checked ? 1:0 );
                            break;
    case tRadioButtonGroup: if (value.Type() != 2 && value.Type() != 3 )
                                throw EAppOptDialogError ( FMT(icsAppOptDialogSetVarErrorArgumentNotAInt) );
                            ((TcxRadioGroup*)components[uid])->ItemIndex   = value;
                            AOptions->setOption( ANode->AV["name"], ((TcxRadioGroup*)components[uid])->ItemIndex );
                            break;
    default:                break;
    }
*/
}

//---------------------------------------------------------------------------

namespace Appoptdialog
{
    void __fastcall PACKAGE Register()
    {
         TComponentClass classes[1] = {__classid(TAppOptDialog)};
         RegisterComponents("ICS", classes, 0);
    }
}

void __fastcall TAppOptDialog::onUnresolvedResult ( variable* firstVar, variable* secondVar, operationType type, bool &result )
{
    FUnresolvedRes ( firstVar, secondVar, type, result );
}
