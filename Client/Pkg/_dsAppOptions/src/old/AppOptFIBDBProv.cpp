//---------------------------------------------------------------------------

//#include <basepch.h>

#pragma hdrstop

#include "AppOptFIBDBProv.h"

#include "AppOptDialogConstDef.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TAppOptFIBDBProv *)
{
    new TAppOptFIBDBProv(NULL);
}
//---------------------------------------------------------------------------
__fastcall TAppOptFIBDBProv::TAppOptFIBDBProv(TComponent* Owner)
    : TAppOptBaseProv(Owner)
{
  quFree = NULL;
  FTableName = "";
}

//---------------------------------------------------------------------------
__fastcall TAppOptFIBDBProv::~TAppOptFIBDBProv()
{
}
//---------------------------------------------------------------------------
void __fastcall TAppOptFIBDBProv::writeValue (TAppOptionsVar* AVar)
{
  try
   {
      UnicodeString UID = AVar->Node->AV["uid"];

      if ( AVar->Node->CmpName("digit") )
       {
          AVar->isEmpty ? FWriteValue (UID, "empty") : FWriteValue ( UID, AVar->Value.AsString );
       }
      else
       FWriteValue ( UID, AVar->Value.AsString );
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
void __fastcall TAppOptFIBDBProv::writeValue (UnicodeString key, UnicodeString value)
{
  FWriteValue(key, value);
}
//---------------------------------------------------------------------------
void __fastcall TAppOptFIBDBProv::readValue  (TAppOptionsVar* AVar)
{
  try
   {
      UnicodeString tmp = FReadValue(AVar->Node->AV["uid"]);
      bool FValid = true;
      if ( AVar->Node->CmpName("digit") )
       {
         if (tmp.LowerCase() == "empty")
          {
            AVar->Clear();
          }
         else
          AVar->Value = TryStrToFloatDef(tmp,FValid);
       }
      else if ( AVar->Node->CmpName("binary"))
       AVar->Value = (bool)tmp.ToIntDef(0);
      else if ( AVar->Node->CmpName("choice"))
       AVar->Value = tmp.ToIntDef(0);
      else if ( AVar->Node->CmpName("text"))
       AVar->Value = tmp;
      else if ( AVar->Node->CmpName("datetime"))
       AVar->Value = TryStrToDateTimeDef(tmp,FValid);
      if (!FValid)
       AVar->Clear();
   }
  catch (Exception &e)
   {
     AVar->Clear();
   }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAppOptFIBDBProv::FReadValue (UnicodeString AKey)
{
  UnicodeString RC = "";
  try
   {
     if (!AKey.Trim().Length())
       throw EFIBDBProviderError (FMT(icsAppOptFIBDBProvKeyNotValid));
     if (CheckConnection())
      {
        try
         {
           QExec("Select VAL From "+FTableName.Trim().UpperCase()+" Where NAME='"+AKey.Trim().UpperCase()+"'");
           RC = quFree->FN("VAL")->AsString;
           quFree->Transaction->Commit();
         }
        catch(EFIBInterBaseError& E)
         {
           throw EFIBDBProviderError (FMT1(icsAppOptFIBDBProvErrWriteValue,AKey));
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TAppOptFIBDBProv::FWriteValue (UnicodeString AKey, UnicodeString AVal)
{
  if (!AKey.Trim().Length())
    throw EFIBDBProviderError (FMT(icsAppOptFIBDBProvKeyNotValid));
  if (CheckConnection())
   {
     try
      {
        QExec("Select Count(*) as MCOUNT From "+FTableName.Trim().UpperCase()+" Where NAME='"+AKey.Trim().UpperCase()+"'");
        bool FCanInsert = !(bool)quFree->FN("MCOUNT")->AsInteger;
        if (!quFree->Transaction->Active) quFree->Transaction->Rollback();
        quFree->Transaction->StartTransaction();
        quFree->Close();
        quFree->SQL->Clear();
        if (FCanInsert)
         quFree->SQL->Text = "Insert into "+FTableName.Trim().UpperCase()+" (NAME,VAL) values (?PNAME,?PVAL)";
        else
         quFree->SQL->Text = "Update "+FTableName.Trim().UpperCase()+" Set VAL = ?PVAL Where NAME=?PNAME";
        quFree->ParamByName("PNAME")->AsString = AKey.Trim().UpperCase();
        quFree->ParamByName("PVAL")->AsString = AVal.Trim();
        quFree->Prepare();
        quFree->ExecQuery();
        quFree->Transaction->Commit();
      }
     catch(EFIBInterBaseError& E)
      {
        throw EFIBDBProviderError (FMT1(icsAppOptFIBDBProvErrWriteValue,AKey));
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TAppOptFIBDBProv::CheckConnection()
{
  bool RC = false;
  try
   {
     try
      {
        if (!FTableName.Length())
         throw EFIBDBProviderError (FMT(icsAppOptFIBDBProvErrTableNotDef));
        QExec("Select Count(*) from "+FTableName.Trim().UpperCase());
        RC = true;
      }
     catch (EFIBDBProviderError &E)
      {
        throw E;
      }
     catch(EFIBInterBaseError& E)
      {
        if ((E.SQLCode == -204) && ((E.IBErrorCode == 335544569)||(E.IBErrorCode == 335544580)))
         { // ������� �� �������
           try
            {
               QExec("Create table "+FTableName.Trim().UpperCase()+" (NAME VARCHAR(150) NOT NULL, VAL VARCHAR(255), PRIMARY KEY (NAME))");
               quFree->Transaction->Commit();
            }
           catch(EFIBInterBaseError& E)
            {
             //
            }
         }
        else
         throw E;
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TAppOptFIBDBProv::QExec(UnicodeString ASQL, bool aCommit)
{
  if (quFree)
   {
     if (!quFree->Transaction->Active) quFree->Transaction->StartTransaction();
     quFree->Close();
     quFree->SQL->Clear();
     quFree->SQL->Text = ASQL;
     quFree->Prepare();
     quFree->ExecQuery();
     if (aCommit) quFree->Transaction->Commit();
   }
  else
    throw EFIBDBProviderError (FMT(icsAppOptFIBDBProvQueryNotDef));
}
//---------------------------------------------------------------------------

namespace Appoptfibdbprov
{
    void __fastcall PACKAGE Register()
    {
         TComponentClass classes[1] = {__classid(TAppOptFIBDBProv)};
         RegisterComponents("ICS", classes, 0);
    }
}
//---------------------------------------------------------------------------
