//---------------------------------------------------------------------------

#ifndef AppOptRegProviderH
#define AppOptRegProviderH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include "AppOptBaseProv.h"
#include <Registry.hpp>
//---------------------------------------------------------------------------
enum regRoot { LOCAL_MACHINE , CURRENT_USER , CURRENT_CONFIG };
class PACKAGE TAppOptRegProv : public TAppOptBaseProv
{
private:
    TRegistry* regContent;
    UnicodeString path;
    regRoot    root;

    void openReg();
protected:
public:

    __fastcall TAppOptRegProv(TComponent* Owner);// : TAppOptBaseProv(Owner) : TComponent(Owner);
    __fastcall ~TAppOptRegProv();

   void __fastcall writeValue (TAppOptionsVar* AVar);
   void __fastcall writeValue (const UnicodeString key, const UnicodeString value);
   void __fastcall readValue  (TAppOptionsVar* AVar);

__published:
   __property UnicodeString Path = {read = path , write = path};
   __property regRoot RootKey = {read = root , write = root};

};
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
class ERegProviderError : public Exception
{
public:
    __fastcall ERegProviderError(const UnicodeString Msg) : Exception(Msg) {}
};

#endif
 