//---------------------------------------------------------------------------

//#include <basepch.h>

#pragma hdrstop

#include "AppOptRegProv.h"
#include "AppOptDialogConstDef.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TAppOptRegProv *)
{
    new TAppOptRegProv(NULL);
}
//---------------------------------------------------------------------------
__fastcall TAppOptRegProv::TAppOptRegProv(TComponent* Owner)
    : TAppOptBaseProv(Owner)
{
    root = LOCAL_MACHINE;
}

//---------------------------------------------------------------------------
__fastcall TAppOptRegProv::~TAppOptRegProv()
{
    if ( regContent )
        delete regContent;
}
//---------------------------------------------------------------------------

void __fastcall TAppOptRegProv::writeValue (TAppOptionsVar* AVar)
{
    if ( !regContent )
        openReg();

    if ( regContent->OpenKey( path, true ))
    {
        try
        {
            UnicodeString UID = AVar->Node->AV["uid"];

            if ( AVar->Node->CmpName("digit") )
             {
                AVar->isEmpty ? regContent->WriteString (UID, "empty") : regContent->WriteString ( UID, AVar->Value.AsString );
             }
            else
             regContent->WriteString ( UID, AVar->Value.AsString );
        }
        __finally
        {
            regContent->CloseKey();
        }
    }

}

//---------------------------------------------------------------------------

void __fastcall TAppOptRegProv::writeValue (UnicodeString key, UnicodeString value)
{
    if ( !regContent )
        openReg();

    if ( regContent->OpenKey(path, true) )
    {
        try
        {
          regContent->WriteString(key,value);
        }
        __finally
        {
          regContent->CloseKey();
        }
      }
}

//---------------------------------------------------------------------------
void __fastcall TAppOptRegProv::readValue  (TAppOptionsVar* AVar)
{

    if ( !regContent )
        openReg();

    if ( regContent->OpenKey( path, false ))
    {   try
        {
          try
          {
              UnicodeString tmp = regContent->ReadString   ( AVar->Node->AV["uid"] );
              bool FValid = true;
              if ( AVar->Node->CmpName("digit") )
               {
                 if (tmp.LowerCase() == "empty")
                  {
                    AVar->Clear();
                  }
                 else
                  AVar->Value = TryStrToFloatDef(tmp,FValid);
               }
              else if ( AVar->Node->CmpName("binary"))
               AVar->Value = (bool)tmp.ToIntDef(0);
              else if ( AVar->Node->CmpName("choice"))
               AVar->Value = tmp.ToIntDef(0);
              else if ( AVar->Node->CmpName("text"))
               AVar->Value = tmp;
              else if ( AVar->Node->CmpName("datetime"))
               AVar->Value = TryStrToDateTimeDef(tmp,FValid);
              if (!FValid)
               AVar->Clear();
          }
          catch (ERegistryException &e)
          {
            AVar->Clear();
          }
        }
        __finally
        {
          regContent->CloseKey();
        }

    }
}

//---------------------------------------------------------------------------

void TAppOptRegProv::openReg()
{
  // ������������� ������ � ��������
  if ( regContent == NULL )
      regContent          = new TRegistry;

  switch ( root )
  {
        case LOCAL_MACHINE  : regContent->RootKey = HKEY_LOCAL_MACHINE;  break;
        case CURRENT_USER   : regContent->RootKey = HKEY_CURRENT_USER ;  break;
        case CURRENT_CONFIG : regContent->RootKey = HKEY_CURRENT_CONFIG; break;
        default: throw ERegProviderError (FMT(icsAppOptRegProvRootNotDefined));
  }
}

//---------------------------------------------------------------------------

namespace Appoptregprov
{
    void __fastcall PACKAGE Register()
    {
         TComponentClass classes[1] = {__classid(TAppOptRegProv)};
         RegisterComponents("ICS", classes, 0);
    }
}
//---------------------------------------------------------------------------
