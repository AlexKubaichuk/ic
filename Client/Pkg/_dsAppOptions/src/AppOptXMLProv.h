//---------------------------------------------------------------------------

#ifndef AppOptXMLProvH
#define AppOptXMLProvH
//---------------------------------------------------------------------------
//#include <SysUtils.hpp>
//#include <Classes.hpp>
//#include <Registry.hpp>
//#include "FIBQuery.hpp"
//#include "pFIBQuery.hpp"

#include "AppOptBaseProv.h"
#include "XMLContainer.h"
//---------------------------------------------------------------------------
class PACKAGE EXMLProviderError : public Exception
{
public:
    __fastcall EXMLProviderError(const UnicodeString Msg) : Exception(Msg) {}
};
//---------------------------------------------------------------------------
class PACKAGE TAppOptXMLProv : public TAppOptBaseProv
{
private:
    TTagNode *FDefNode;
    bool FExternal;

    UnicodeString FFileName;

    void __fastcall FSetDefNode(TTagNode *AValue);
    void __fastcall FSetFileName(UnicodeString AValue);

    UnicodeString __fastcall FReadValue (UnicodeString AKey);
    void          __fastcall FWriteValue (UnicodeString AKey, UnicodeString AVal);
protected:
public:
    __fastcall TAppOptXMLProv(TComponent* Owner);
    __fastcall ~TAppOptXMLProv();

   void __fastcall writeValue (TAppOptionsVar* AVar);
   void __fastcall writeValue (const UnicodeString key, const UnicodeString value);
   void __fastcall readValue  (TAppOptionsVar* AVar);

__published:
   __property UnicodeString FileName = {read = FFileName, write = FSetFileName};
   __property TTagNode * DefNode = {read = FDefNode , write = FSetDefNode};
};
//---------------------------------------------------------------------------
#endif
