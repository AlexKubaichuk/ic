//---------------------------------------------------------------------------

#ifndef AppOptDialogH
#define AppOptDialogH
//---------------------------------------------------------------------------
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <cxTL.hpp>
#include <cxPC.hpp>
#include <cxCheckBox.hpp>
#include "cxButtonEdit.hpp"
#include <cxLabel.hpp>
#include <cxCurrencyEdit.hpp>
#include <cxCalendar.hpp>
#include <cxRadioGroup.hpp>
#include <cxGroupBox.hpp>
#include <dxStatusBar.hpp>
#include <cxSplitter.hpp>
#include <cxControls.hpp>
#include <cxStyles.hpp>
#include "AppOptions.h"
#include "EvalCondition.h"
#include "AppOptDialogComps.h"
#include "AppOptDialogConstDef.h"

namespace Appoptions
{

typedef map <UnicodeString, TComponent*> TAODataCompMap;
typedef map <UnicodeString, TComponent*> TAOVisualCompMap;
typedef map <UnicodeString, TcxTabSheet*> TAOPagesMap;
typedef map <UnicodeString, TcxPageControl*> TAOPageCtrlMap;
typedef map <UnicodeString, UnicodeString> TAODependenciesMap;

typedef map <UnicodeString, TAppOptionsVar*> TAOVarMap;
typedef void __fastcall (__closure *TExtEdit)(TTagNode* ANode, TAOVarMap &vars);
typedef UnicodeString __fastcall (__closure *TBtnEdit)(TTagNode* ANode, int AButtonIndex, AOButtonEdit* control);
typedef void __fastcall (__closure *TAfterComponentCreate)(TTagNode* ANode, TComponent* component);
typedef void __fastcall (__closure *TBeforeSave) (TTagNode* ANode, bool &canSave);
typedef void __fastcall (__closure *TUnresolvedRes)(variable* firstVar, variable* secondVar, operationType type, bool &result);

enum componentType {tButtonEdit,tCurrEdit,tDate,tGroup,tCheckBox,tExtern,tRadioButtonGroup,tNone};
//---------------------------------------------------------------------------

class PACKAGE TAppOptDialog : public TComponent
{
private:
    int                   formHeight;
    int                   formWidth;
    TUnresolvedRes        FUnresolvedRes;
    TAfterComponentCreate FAfterComponentCreate;
    TExtEdit              FExtEdit;
    TBtnEdit              FBtnEdit;
    TBeforeSave           FBeforeSave;
    int                   pageIndex;
    TAppOptions*          AOptions;
    TForm*                optForm;
    TcxTreeList*          optTreeList;
    TPanel*               bottomPanel;
//    TPanel*               bottomPanelSpl;
    TcxPageControl*       optPageCtrl;
    TAOPagesMap           pages;
    TAOPagesMap           pagesLevel2;
    TAOPageCtrlMap        pageControls;
    TcxSplitter*          splitter;
    TAODataCompMap        components;
    TAOVisualCompMap      vcomponents;
    TAODependenciesMap    dependencies;


    // ����� ������������
    void __fastcall createForm                   ( UnicodeString formCaption );
    void __fastcall createTreeView               ( );
    void __fastcall createSplitter               ( );
    void __fastcall createBottomPanel            ( );
    TcxPageControl* __fastcall createPageControl ( TWinControl* parent);
    TcxTabSheet* __fastcall createPage           ( TcxPageControl* pageCtrl, UnicodeString caption, int pageIndex, bool isTabVisible =false);
    TWinControl* __fastcall createGroupBox       ( TTagNode* ANode, TWinControl* parent, TRect rect);

    // �������� ������������� ����������
    int __fastcall createComponents(TTagNode* ANode, TWinControl* parent, TRect rect);
    int __fastcall createBinary    (TTagNode* ANode, TWinControl* parent, TRect rect);
    int __fastcall createDigit     (TTagNode* ANode, TWinControl* parent, TRect rect);
    int __fastcall createChoice    (TTagNode* ANode, TWinControl* parent, TRect rect);
    int __fastcall createText      (TTagNode* ANode, TWinControl* parent, TRect rect);
    int __fastcall createExtern    (TTagNode* ANode, TWinControl* parent, TRect rect);
    int __fastcall createDate      (TTagNode* ANode, TWinControl* parent, TRect rect);
    int __fastcall createGroup     (TTagNode* ANode, TWinControl* groupBox, TRect rect);

    // �������� ����������� ��� ����������� ����������
    int __fastcall createButtonEdit    (TTagNode* ANode, TWinControl* parent, TRect rect);
    int __fastcall createDigitMaskEdit (TTagNode* ANode, TWinControl* parent, TRect rect);
    int __fastcall createButton        (TTagNode* ANode, TWinControl* parent, TRect rect);
    int __fastcall createDateEdit      (TTagNode* ANode, TWinControl* parent, TRect rect);
    int __fastcall createCheckBox      (TTagNode* ANode, TWinControl* parent, TRect rect);
    int __fastcall createRadioButtons  (TTagNode* ANode, TWinControl* parent, TRect rect);

    // ��������������� ������
    void __fastcall onUnresolvedResult          ( variable* firstVar, variable* secondVar, operationType type, bool &result );
    UnicodeString makeDigitEditMask                ( TTagNode* ANode );
    bool __fastcall isValidCondition            ( TTagNode* node );
    bool __fastcall dataComponentExist          ( UnicodeString UID );
    bool __fastcall visualComponentExist        ( UnicodeString UID );
    bool __fastcall pageControlExist            ( UnicodeString UID );
    void __fastcall changeGroupComponentsStatus ( TcxGroupBox* groupBox, bool result );
    void __fastcall fillData                    (TTagNode* grNode, TcxTreeListNode* treeNode);
    void __fastcall cleanup                     ( );
    void __fastcall saveChanges                 ( );
    bool __fastcall canSave                     ( );
    void __fastcall writeValue                  ( UnicodeString tagName, UnicodeString optionName, TComponent* comp );
    void __fastcall getDependencies             ( TTagNode* ANode );
    void __fastcall workoutDependencies         ( UnicodeString masterUID );
    void __fastcall workoutDependency           ( UnicodeString UID );

    // events
    void __fastcall focusedNodeChanged   ( TcxCustomTreeList *Sender, TcxTreeListNode *APrevFocusedNode, TcxTreeListNode *AFocusedNode);
    void __fastcall onTreeListResize     ( TObject* Sender );
    void __fastcall onFormClose          ( TObject* Sender, TCloseAction &Action );
    void __fastcall onOkBtnClick         ( TObject* Sender );
    void __fastcall onCancelBtnClick     ( TObject* Sender );
    void __fastcall onMouseWheelDown     ( TObject *Sender, TShiftState Shift, const TPoint &MousePos, bool &Handled);
    void __fastcall onMouseWheelUp       ( TObject *Sender, TShiftState Shift, const TPoint &MousePos, bool &Handled);
    void __fastcall onExtEditBtnClick    ( TObject *Sender );
    void __fastcall onButtonEditChange   ( TObject *Sender );
    void __fastcall onButtonEditBtnClick ( TObject *Sender, int AButtonIndex);
    void __fastcall onCurrencyEditChange ( TObject *Sender );
    void __fastcall onRadioGroupChange   ( TObject *Sender );
    void __fastcall onDateEditChange     ( TObject *Sender );
    void __fastcall onCheckBoxChange     ( TObject *Sender );
    void __fastcall onCurrEditValidate   ( TObject *Sender, Variant &DisplayValue, TCaption &ErrorText, bool &Error);

    componentType getTypeByName          ( UnicodeString name );
    Variant __fastcall getVarValue  (TTagNode* ANode);
    void __fastcall    setVarValue  (TTagNode* ANode, Variant value);


protected:
public:

    Variant __fastcall getVarByName (UnicodeString name);
    Variant __fastcall getVarByUID  (UnicodeString UID);
    void __fastcall setVarByName    (UnicodeString name, Variant value);
    void __fastcall setVarByUID     (UnicodeString UID,  Variant value);
    void __fastcall setFormHeight   ( int pH );
    void __fastcall setFormWidth    ( int pW );

    TColor requiredColor;
    TColor normalColor;
    __fastcall TAppOptDialog(TComponent* Owner);
    void __fastcall Execute(UnicodeString formCaption =FMT(icsAppOptDialogDefWindowCaption), int ASelIdx = 0 );
    __property Variant VarsByName [UnicodeString name] = {read = getVarByName, write = setVarByName};
    __property Variant VarsByUID  [UnicodeString UID]  = {read = getVarByUID,  write = setVarByUID};


__published:

    __property int FormHeight                              = {read = formHeight, write = setFormHeight, default=420};
    __property int FormWidth                               = {read = formWidth,  write = setFormWidth, default=600};
    __property TAppOptions* Options                        = {read = AOptions, write = AOptions};
    __property TExtEdit OnExternEditClick                  = {read = FExtEdit, write = FExtEdit};
    __property TBtnEdit OnButtonEditClick                  = {read = FBtnEdit, write = FBtnEdit};
    __property TBeforeSave BeforeOptionsSave               = {read = FBeforeSave, write = FBeforeSave};
    __property TAfterComponentCreate AfterComponentCreate  = {read = FAfterComponentCreate, write = FAfterComponentCreate};
    __property TUnresolvedRes OnActualsConditionUnresolved = {read = FUnresolvedRes, write = FUnresolvedRes};
};
//---------------------------------------------------------------------------

class PACKAGE EAppOptDialogError : public System::Sysutils::Exception
{
public:
    __fastcall EAppOptDialogError (const UnicodeString Msg);
};

} // ����� namespace
#endif
