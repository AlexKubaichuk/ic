#ifndef AppOptBaseProvH
#define AppOptBaseProvH

//---------------------------------------------------------------------------
#include <Classes.hpp>
#include "AppOptVar.h"

//###########################################################################
//#                                                                         #
//#                        TAppOptBaseProvider                              #
//#                                                                         #
//###########################################################################
class PACKAGE TAppOptBaseProv : public TComponent
{
protected:
   double __fastcall TryStrToFloatDef(UnicodeString ASrc, bool &AValid);
   TDateTime __fastcall TryStrToDateTimeDef(UnicodeString ASrc, bool &AValid);
public:

//   virtual __fastcall  TAppOptBaseProv (TComponent* AOwner):TComponent(AOwner){};
   __fastcall  TAppOptBaseProv (TComponent* AOwner);

   virtual void    __fastcall writeValue  (TAppOptionsVar* AVar) = 0;
   virtual void    __fastcall writeValue  (UnicodeString key, UnicodeString value) = 0;
   virtual void    __fastcall readValue   (TAppOptionsVar* AVar) = 0;
};

//---------------------------------------------------------------------------
// исключения
//---------------------------------------------------------------------------
class PACKAGE EAOAppOptionsError : public System::Sysutils::Exception
{
public:
    __fastcall EAOAppOptionsError(const UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE ECustomAOError : public System::Sysutils::Exception
{
public:
    __fastcall ECustomAOError(const UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE ECustomAOProviderError : public System::Sysutils::Exception
{
public:
    __fastcall ECustomAOProviderError(const UnicodeString Msg);
};
//---------------------------------------------------------------------------


#endif
