//---------------------------------------------------------------------------


#pragma hdrstop

#include "EvalCondition.h"
#include "AppOptDialogConstDef.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------

void __fastcall TEvalCond::getLexems (UnicodeString cond, TStringList* lexems)
{
    int i = 1;

    while ( i <= cond.Length() )
    {
        if ( cond[i] == '(' )
        {
            int index = lexems->Add ("(");
            lexems->Objects[index] = new variable (tNotVar,0);
            i++;
        }
        else if ( cond[i] == ')' )
        {
            int index = lexems->Add (")");
            lexems->Objects[index] = new variable (tNotVar,0);
            i++;
        }
        else if ( cond[i] == '!' && cond[i+1] != '=')
        {
            int index = lexems->Add ("!");
            lexems->Objects[index] = new variable (tNotVar,0);
            i++;
        }
        else if ( cond[i] == '!' && cond[i+1] == '=')
        {
            int index = lexems->Add ("!=");
            lexems->Objects[index] = new variable (tNotVar,0);
            i += 2;
        }
        else if ( cond[i] == '=' && cond[i+1] == '=')
        {
            int index = lexems->Add ("==");
            lexems->Objects[index] = new variable (tNotVar,0);
            i += 2;
        }
        else if ( cond[i] == '<' && cond[i+1] != '=' )
        {
            int index = lexems->Add ("<");
            lexems->Objects[index] = new variable (tNotVar,0);
            i++;
        }
        else if ( cond[i] == '<' && cond[i+1] == '=')
        {
            int index = lexems->Add ("<=");
            lexems->Objects[index] = new variable (tNotVar,0);
            i += 2;
        }
        else if ( cond[i] == '>' && cond[i+1] != '=' )
        {
            int index = lexems->Add (">");
            lexems->Objects[index] = new variable (tNotVar,0);
            i++;
        }
        else if ( cond[i] == '>' && cond[i+1] == '=')
        {
            int index = lexems->Add (">=");
            lexems->Objects[index] = new variable (tNotVar,0);
            i += 2;
        }
        else if ( cond[i] == '&' && cond[i+1] == '&' )
        {
            int index = lexems->Add ("&&");
            lexems->Objects[index] = new variable (tNotVar,0);
            i += 2;
        }
        else if ( cond[i] == '|' && cond[i+1] == '|' )
        {
            int index = lexems->Add ("||");
            lexems->Objects[index] = new variable (tNotVar,0);
            i += 2;
        }
        else if ( cond[i] == ' ')
        {
            i++;
            continue;
        }
        else
        {
            int len = 0;
            while ( i+len <= cond.Length() && !cond.IsDelimiter(" !=<>&|()",i+len) )
                len++;
            int index = lexems->Add ( cond.SubString (i,len) );
            lexems->Objects[index] = new variable (tNotVar,0);
            i += len;
        }
    }
}


//---------------------------------------------------------------------------

bool TEvalCond::evalCondition (UnicodeString condition, TCondVarsMap variablesMap)
{
    if ( condition.IsEmpty() || variablesMap.empty() )
        return false;

    TStringList* lexems = new TStringList;
    bool result;

    try
    {
        getLexems ( condition, lexems );
        getVars   ( lexems, variablesMap );

        result = realEvaluate ( lexems );
    }
    __finally
    {
        while ( lexems->Count )
        {
            delObj (0, lexems);
            lexems->Delete (0);
        }

        delete lexems;
    }
                   
    return result;
}

//---------------------------------------------------------------------------

void __fastcall TEvalCond::getVars ( TStringList* lexems, const TCondVarsMap vars )
{
    int index = 0;
    while ( index < lexems->Count )
    {
        if ( lexems->Strings[index].LastDelimiter("&|!><=#()") )
        {
            // �������
        }
        else if ( vars.find(lexems->Strings[index]) != vars.end() )
        {
            // ����������
            variable* var = (variable*)lexems->Objects[index];
            if ( vars[lexems->Strings[index]] == undefinedValue )
            {
                var->vType = tUndefined;
            }
            else
            {
                try
                {
                    double val = StrToFloat (vars[lexems->Strings[index]]);
                    var->vType = tDigit;
                    var->var   = val;
                }
                catch ( EConvertError &e )
                {
                    var->vType = tString;
                    var->var   = vars[lexems->Strings[index]];
                }
            }
        }

        else
        {
             try
             {
                variable* var = (variable*)lexems->Objects[index];
                if ( lexems->Strings[index][1] == '\"' && lexems->Strings[index][lexems->Strings[index].Length()] == '\"')
                {
                    // ��������� ���������
                    var->vType   = tString;
                    var->var     = lexems->Strings[index].SubString(2,lexems->Strings[index].Length()-2);

                }
                else if ( lexems->Strings[index] == "undefined" )
                {
                    // �������������� ��������
                    var->vType   = tUndefined;
                }
                else
                {
                    // �����
                    int num = StrToInt (lexems->Strings[index]);
                    var->vType = tDigit;
                    var->var   = num;
                }
             }
             catch (EConvertError &e)
             {
                throw TEvalCondError( FMT1 (icsAppOptDialogEvalCondVarNoValue, lexems->Strings[index]) );
             }
        }

        index++;
    }

}

//---------------------------------------------------------------------------

bool __fastcall TEvalCond::realEvaluate ( TStringList* lexems )
{
    while ( expandParenthesis ( lexems ) ) ;

    expandArrows ( lexems );
    expandEQ     ( lexems );
    expandNotEQ  ( lexems );
    expandAND    ( lexems );
    expandOR     ( lexems );

    return StrToInt(lexems->Strings[0]) == 1?true:false;
}

//---------------------------------------------------------------------------

int __fastcall TEvalCond::expandParenthesis( TStringList* lexems )
{
    if ( lexems->IndexOf("(") == -1 )
        return 0;

    int index    = lexems->Count - 1;
    int leftPar  = 0;
    int rightPar = 0;
    bool result;

    while ( index >= 0 )
    {
        if ( lexems->Strings[index] == ")" )
            rightPar = index;
        if ( lexems->Strings[index] == "(" )
        {
            leftPar  = index;
            break;
        }
        index--;
    }

    if ( leftPar >= rightPar )
        throw Exception ("error: no matching \")\" found");
    TStringList* temp = new TStringList;
    try
    {
        while ( ++index < rightPar )
            temp->Add ( lexems->Strings[index] );

        result = realEvaluate ( temp );
    }
    __finally
    {
        delete temp;
    }

    while ( leftPar != rightPar )
    {
        delObj (leftPar, lexems);
        lexems->Delete (leftPar);
        rightPar--;
    }

    lexems->Strings[leftPar] = result?1:0;
    ((variable*)lexems->Objects[leftPar])->vType = tDigit;
    ((variable*)lexems->Objects[leftPar])->var  = 1;

    return 1;
}

//---------------------------------------------------------------------------

void __fastcall TEvalCond::expandArrows ( TStringList* lexems )
{
    if ( lexems->IndexOf("<")  == -1 && lexems->IndexOf("<=") == -1 && lexems->IndexOf(">")  == -1 && lexems->IndexOf(">=") == -1 )
        return;

    operationType type;
    int index = lexems->IndexOf("<");
    while ( index != -1 )
    {
        compare ( index, LT, lexems );
        index = lexems->IndexOf("<");
    }

    index = lexems->IndexOf(">");
    while ( index != -1 )
    {
        compare ( index, GT, lexems );
        index = lexems->IndexOf(">");
    }

    index = lexems->IndexOf("<=");
    while ( index != -1 )
    {
        compare ( index, LE, lexems );
        index = lexems->IndexOf("<=");
    }

    index = lexems->IndexOf(">=");
    while ( index != -1 )
    {
        compare ( index, GE, lexems );
        index = lexems->IndexOf(">=");
    }
}

//---------------------------------------------------------------------------

void __fastcall TEvalCond::expandEQ          ( TStringList* lexems )
{
    int index = lexems->IndexOf("==");
    if ( index == -1 )
        return;

    while ( index != -1 )
    {
        compare ( index, EQ, lexems );
        index = lexems->IndexOf("==");
    }
}

//---------------------------------------------------------------------------

void __fastcall TEvalCond::expandNotEQ       ( TStringList* lexems )
{
    int index = lexems->IndexOf("!=");
    if ( index == -1 )
        return;

    while ( index != -1 )
    {
        compare ( index, NotEQ, lexems );
        index = lexems->IndexOf("!=");
    }

}

//---------------------------------------------------------------------------

void __fastcall TEvalCond::expandAND         ( TStringList* lexems )
{
    int index = lexems->IndexOf("&&");
    if ( index == -1 )
        return;

    while ( index != -1 )
    {
        compare ( index, AND, lexems );
        index = lexems->IndexOf("&&");
    }

}

//---------------------------------------------------------------------------

void __fastcall TEvalCond::expandOR          ( TStringList* lexems )
{
    int index = lexems->IndexOf("||");
    if ( index == -1 )
        return;

    while ( index != -1 )
    {
        compare ( index, OR, lexems );
        index = lexems->IndexOf("||");
    }
}

//---------------------------------------------------------------------------

void __fastcall TEvalCond::compare ( int index, operationType type, TStringList* lexems )
{
    //�������� ������������ �� ��� ��� ����

    //���� ��� �� ��������� ������������
    if ( ((variable*)lexems->Objects[index-1])->variableType == tUndefined && ((variable*)lexems->Objects[index+1])->variableType == tUndefined )
    {
        if ( type == NotEQ )
        {
            shrinkToResult ( lexems, index, false );
        }
        else if ( type == EQ )
        {
            shrinkToResult ( lexems, index, true );
        }
        else
            throw TEvalCondError ( FMT(icsAppOptDialogEvalCondUndefError) );
        return;
    }
    //���� ���� �� ��������� �����������
    else if ( ( ((variable*)lexems->Objects[index-1])->variableType == tUndefined || ((variable*)lexems->Objects[index+1])->variableType == tUndefined ) )
    {
        // ������� ����� �������� ���� ��������
        if ( type == NotEQ )
        {
            // ���� ��� �� �����, ������ ��� � �������
            shrinkToResult ( lexems, index, true );
            return;
        }
        else if ( type == OR )
        {
            // ���� ��� ���, �������� "������������" �� ���� � ���������
            // ������ � �������� �����������
            if ( ((variable*)lexems->Objects[index-1])->variableType == tUndefined )
            {
                lexems->Strings[index-1] = "0";
                ((variable*)lexems->Objects[index-1])->vType = tDigit;
                ((variable*)lexems->Objects[index-1])->var   = 0;
            }
            else
            {
                lexems->Strings[index+1] = "0";
                ((variable*)lexems->Objects[index+1])->vType = tDigit;
                ((variable*)lexems->Objects[index+1])->var   = 0;
            }
        }
        else
        {
            // �� ��� ��������� �������� ������ ���� � �������
            shrinkToResult ( lexems, index, false );
            return;
        }
    }

    // ��������� �������� ������������
    double leftVar, rightVar;
    UnicodeString strLeftVar, strRightVar;
    bool varsIsStrings = false;
    bool result;

    if ( ((variable*)lexems->Objects[index-1])->variableType == tDigit && ((variable*)lexems->Objects[index+1])->variableType == tDigit )
    {
        // ���� ��� �������� �����
        rightVar = ((variable*)lexems->Objects[index+1])->var;
        leftVar  = ((variable*)lexems->Objects[index-1])->var;
    }
    else if ( ((variable*)lexems->Objects[index-1])->variableType == tString && ((variable*)lexems->Objects[index+1])->variableType == tString )
    {
        // ���� ������
        strRightVar   = ((variable*)lexems->Objects[index+1])->var;
        strLeftVar    = ((variable*)lexems->Objects[index-1])->var;
        varsIsStrings = true;
    }
    else
    {
        if ( FUnresolvedResult )
        {
            // ���� ��� ��������� ������ (������ � �����), ��������� �������
            bool result;
            FUnresolvedResult ( (variable*)lexems->Objects[index-1], (variable*)lexems->Objects[index+1], type, result);

            shrinkToResult ( lexems, index, result );
            return;
        }
            // ���� ������� �� �������, ��������� �������� � �������
        else
        {
            strRightVar   = ((variable*)lexems->Objects[index+1])->asString;
            strLeftVar    = ((variable*)lexems->Objects[index-1])->asString;
            varsIsStrings = true;
        }
        //throw TEvalCondError ( FMT ( icsAppOptDialogEvalCondVarsNotSameType ) );
    }

    // ���������� ����������� ��������
    switch (type)
    {
        case LT:
                 if ( varsIsStrings ) result = strLeftVar < strRightVar?true:false;
                 else                 result = leftVar    < rightVar   ?true:false;
                 break;
        case GT:
                 if ( varsIsStrings ) result = strLeftVar > strRightVar?true:false;
                 else                 result = leftVar    > rightVar   ?true:false;
                 break;
        case LE:
                 if ( varsIsStrings ) result = strLeftVar <= strRightVar?true:false;
                 else                 result = leftVar    <= rightVar   ?true:false;
                 break;
        case GE:
                 if ( varsIsStrings ) result = strLeftVar >= strRightVar?true:false;
                 else                 result = leftVar    >= rightVar   ?true:false;
                 break;
        case EQ:
                 if ( varsIsStrings ) result = strLeftVar == strRightVar?true:false;
                 else                 result = leftVar    == rightVar   ?true:false;
                 break;
        case NotEQ:
                 if ( varsIsStrings ) result = strLeftVar != strRightVar?true:false;
                 else                 result = leftVar    != rightVar   ?true:false;
                 break;
        case AND:
                 if ( varsIsStrings ) result = !strLeftVar.IsEmpty() && !strRightVar.IsEmpty()?true:false;
                 else                 result = leftVar && rightVar?true:false;
                 break;
        case OR:
                 if ( varsIsStrings ) result = !strLeftVar.IsEmpty() || !strRightVar.IsEmpty()?true:false;
                 else                 result = leftVar || rightVar?true:false;
                 break;
        default: throw TEvalCondError ( FMT2(icsAppOptDialogEvalCondUnknownOperation, lexems->Strings[index], lexems->Strings[index-1] + lexems->Strings[index] + lexems->Strings[index+1]) );;
    }

    shrinkToResult ( lexems, index, result );
}

//---------------------------------------------------------------------------

void __fastcall TEvalCond::shrinkToResult ( TStringList* lexems, int index, bool result )
{
    delObj         ( index, lexems );
    lexems->Delete ( index );
    delObj         ( index, lexems );
    lexems->Delete ( index );

    lexems->Strings[index-1] = result?1:0;
    ((variable*)lexems->Objects[index-1])->vType = tDigit;
    ((variable*)lexems->Objects[index-1])->var   = result?1:0;
}
//*****************************************************************************
//                                                                            *
//                            class variable                                  *
//                                                                            *
//*****************************************************************************

__fastcall variable::variable ()
{
    vType    = tNotVar;
    digitVal = 0;
    strVal   = NULL;
}

//---------------------------------------------------------------------------

__fastcall variable::variable (varType type, Variant value)
{
    vType = type;
    setVar ( value );
}
//---------------------------------------------------------------------------

Variant __fastcall variable::getVar ()
{
    switch ( vType )
    {
        case tUndefined: return (UnicodeString)"undefined";
        case tDigit:     return digitVal;
        case tString:    return strVal;
        default:         return Variant ("");
    }
}

//---------------------------------------------------------------------------

void __fastcall variable::setVar ( Variant value )
{
    switch ( vType )
    {
        case tDigit:     if ( value.Type() >=2 && value.Type() <=6 ) digitVal = value;
                         else throw TvariableError ( FMT (icsAppOptDialogEvalCondVarIsNotDigit) );
                         break;
        case tString:    if ( value.Type() == 256 ) strVal   = value;
                         else throw TvariableError ( FMT (icsAppOptDialogEvalCondVarIsNotString) );
                         break;
        default:         digitVal = 0;
                         strVal   = NULL;
                         break;
    }

}

//---------------------------------------------------------------------------

Double __fastcall variable::getDblVal ()
{
        switch ( vType )
    {
        case tDigit:     return digitVal;
        case tString:    try
                        {
                            return StrToFloat ( digitVal );
                        }
                        catch (EConvertError &e)
                        {
                            return -1;
                        }
        default:         return -1;
    }
}

//---------------------------------------------------------------------------

UnicodeString __fastcall variable::getStrVal ()
{
    switch ( vType )
    {
        case tDigit:     return FloatToStr ( digitVal );
        case tString:    return strVal;
        default:         return "";
    }
}

//---------------------------------------------------------------------------
void __fastcall TEvalCond::delObj ( int index, TStringList* lexems )
{
    if ( index >= lexems->Count )
        return;

    if ( lexems->Objects[index] )
    {
        delete lexems->Objects[index];
        lexems->Objects[index] = NULL;
    }

}
