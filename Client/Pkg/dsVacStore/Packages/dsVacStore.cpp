//---------------------------------------------------------------------------

#include <System.hpp>
#pragma hdrstop
USEFORM("..\Src\dsStoreClientUnit.cpp", dsStoreClientForm);
USEFORM("..\Src\dsStorCancelEditUnit.cpp", dsStorCancelEditForm);
USEFORM("..\Src\dsStoreEditClientUnit.cpp", dsStoreEditClientForm);
USEFORM("..\Src\dsStorOutcomeEditUnit.cpp", dsStorOutcomeEditForm);
USEFORM("..\Src\dsStorIncomeEditUnit.cpp", dsStorIncomeEditForm);
USEFORM("..\Src\dsGroupCancellationUnit.cpp", dsGroupCancellationForm);
USEFORM("..\Src\dsICStoreDMUnit.cpp", dsICStoreDM); /* TDataModule: File Type */
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Package source.
//---------------------------------------------------------------------------


#pragma argsused
extern "C" int _libmain(unsigned long reason)
{
 return 1;
}
//---------------------------------------------------------------------------
