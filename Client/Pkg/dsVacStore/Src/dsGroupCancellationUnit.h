//---------------------------------------------------------------------------

#ifndef dsGroupCancellationUnitH
#define dsGroupCancellationUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "cxCalendar.hpp"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxProgressBar.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ImgList.hpp>
//---------------------------------------------------------------------------
#include <System.JSON.hpp>
#include "dsICStoreDMUnit.h"
#include "cxBarEditItem.hpp"
#include "cxCheckBox.hpp"
//---------------------------------------------------------------------------
class TdsGroupCancellationForm : public TForm
{
__published:	// IDE-managed Components
 TcxTreeList *MIBPTL;
 TcxTreeListColumn *TestCol;
 TcxTreeListColumn *VacTypeCol;
 TcxTreeListColumn *ReacTypeCol;
 TcxTreeListColumn *ReacValCol;
 TcxTreeListColumn *TestCodeCol;
 TcxTreeListColumn *TestReakCodeCol;
 TcxProgressBar *PBar;
 TcxTreeListColumn *TestTLColumn1;
 TcxTreeListColumn *TestTLColumn2;
 TdxBarManager *ClassTBM;
 TdxBar *ClassTBMBar1;
 TdxBarLargeButton *dxBarLargeButton2;
 TdxBarLargeButton *dxBarLargeButton3;
 TdxBarLargeButton *dxBarLargeButton4;
 TdxBarCombo *PeriodCB;
 TcxBarEditItem *MIBPInStoreChB;
 TcxImageList *ClassIL;
 TcxImageList *LClassIL32;
 TActionList *ClassAL;
 TAction *actApply;
 TAction *actDelete;
 void __fastcall actApplyExecute(TObject *Sender);
 void __fastcall actDeleteExecute(TObject *Sender);
 void __fastcall FormDestroy(TObject *Sender);
 void __fastcall MIBPInStoreChBPropertiesChange(TObject *Sender);
 void __fastcall ReacValColPropertiesChange(TObject *Sender);
private:	// User declarations
  TdsICStoreDM *FDM;
  TStringList *MIBPList;
  void __fastcall GetPrivTestData();
  UnicodeString __fastcall GetMIBPName(int ACode);
public:		// User declarations
 __fastcall TdsGroupCancellationForm(TComponent* Owner, TdsICStoreDM *ADM);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsGroupCancellationForm *dsGroupCancellationForm;
//---------------------------------------------------------------------------
#endif
