//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsVacStorIncomeEditUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TdsVacStorIncomeEditForm *dsVacStorIncomeEditForm;
//---------------------------------------------------------------------------
/*
// <extedit  name='��� ��������'                uid='017B' ref='1000' depend='017B.017B' isedit='0' fieldtype='integer'/>
// <date     name='���� ����������'             uid='1031' required='1' inlist='l' default='CurrentDate'/>
// <extedit  name='�������'                     uid='10B5' inlist='l' depend='1031' isedit='0' length='9'/>
// <choiceDB name='�����'                       uid='1032' ref='002A' required='1' inlist='l'/>
// <text     name='����� ���������'             uid='1033' inlist='l' cast='no' length='20'/>
// <choice   name='�����'                       uid='1034' required='1' inlist='e' default='0'>
// <binary   name='������� ���������'           uid='103B' required='1' inlist='e' default='check'/>
// <binary   name='���� �������'                uid='1041' inlist='e' invname='������� �������������'>
// <choiceDB name='������ �������'              uid='10A7' ref='0038' required='1' inlist='l' depend='1032.002B'>
// <digit    name='��������'                    uid='103F' ref='008E' required='1' inlist='l' depend='10A7.002B' digits='5' min='2' max='32767'>
// <choiceDB name='������ ������� 2'            uid='0183' ref='0038' required='1' inlist='e' depend='1032.002B'>
// <digit    name='�������� 2'                  uid='0184' inlist='e' digits='5' min='2' max='32767'>
// <choiceDB name='������ ������� 3'            uid='0185' ref='0038' required='1' inlist='e' depend='1032.002B'>
// <digit    name='�������� 3'                  uid='0186' inlist='e' digits='5' min='2' max='32767'>
// <choiceDB name='������ ������� 4'            uid='0187' ref='0038' required='1' inlist='e' depend='1032.002B'>
// <digit    name='�������� 4'                  uid='0188' inlist='e' digits='5' min='2' max='32767'>
// <choiceDB name='������ ������� 5'            uid='0189' ref='0038' required='1' inlist='e' depend='1032.002B'>
// <digit    name='�������� 5'                  uid='018A' inlist='e' digits='5' min='2' max='32767'>
// <extedit  name='�����'                       uid='1093' required='1' inlist='e' isedit='0' length='9'/>
// <choiceDB name='���'                         uid='018B' ref='00C7' inlist='e' linecount='2'/>
// <choiceDB name='�������������'               uid='018C' ref='000C' inlist='e' depend='018B.000C' linecount='2'/>
// <binary   name='��������� � ������ ���'      uid='10A5' required='1' inlist='e' invname='��������� � ������ �����������' default='check'/>
// <choiceDB name='����'                        uid='10B2' ref='00AA' inlist='e' depend='018B.00AA;018C.00AA' linecount='2'/>
// <choiceDB name='���. ������'                 uid='018D' ref='00AA' inlist='e' depend='018B.00AA;018C.00AA' linecount='2'/>
// <choiceDB name='�������� ��������������'     uid='10B3' ref='00AB' inlist='e' linecount='2'/>
// <choiceDB name='���. ����������'             uid='018E' ref='00BE' inlist='e' linecount='3'/>
// <digit    name='���������� �������� �������' uid='10C1' digits='2' min='0' max='99' isedit='0'/>
// <digit    name='���������� ������ �������'   uid='10C2' digits='2' min='1' max='99' isedit='0'/>
*/
#define _LPU_       "018B"
#define _PLPU_      "018C"
#define _InThisLPU_ "10A5"

//---------------------------------------------------------------------------
#define EDLEFT1  2
#define EDLEFT2  232
//---------------------------------------------------------------------------
const int LeftMargin = 2;
const int LevelCorrection = 5;
__fastcall TdsVacStorIncomeEditForm::TdsVacStorIncomeEditForm(TComponent* Owner, bool AIsAppend, UnicodeString AClassId, __int64 AUCode, TdsICCardDM *ADM, TTagNode* ARoot, TDate APatBirthday, TkabCustomDataSource *ADataSrc, TdsRegTemplateData *ATmplData, bool AOneCol)
    : TForm(Owner)
{
  FUCode = AUCode;
  FDM = ADM;
  FDataSrc = ADataSrc;
  FOneCol = AOneCol;
  FInsertedRecId = "";
  FPatBirthday = APatBirthday;

  OkBtn->Caption = FMT(icsRegClassifEditApplyBtnCaption);
  CancelBtn->Caption = FMT(icsRegClassifEditCancelBtnCaption);
  FIsAppend = AIsAppend;
  if (FIsAppend)
   FDataSrc->DataSet->Insert();
  else
   FDataSrc->DataSet->Edit();

  FClassDef = ARoot->GetTagByUID(AClassId);
  FClassDesc = FClassDef->GetChildByName("description");

  FCtrList                = new TdsRegEDContainer(this,CompPanel);
  FCtrList->UseHash       = true;
  FCtrList->DefXML        = ARoot;
  FCtrList->ReqColor      = FDM->RequiredColor;
  FCtrList->NativeStyle     = FDM->NativeStyle;
  FCtrList->LFKind          = FDM->LFKind;
  FCtrList->StyleController = FDM->StyleController;
  FCtrList->TemplateData    = ATmplData;

  FCtrList->DataProvider = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider*)FCtrList->DataProvider)->DefXML = ARoot;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  ((TdsRegEDkabDSDataProvider*)FCtrList->DataProvider)->DataSource    = FDataSrc;
  FCtrList->DataProvider->OnGetCompValue       = FDM->OnGetCompValue;

  FCtrList->OnDataChange         = FCtrlDataChange;
  FCtrList->OnGetExtBtnGetBitmap = FDM->OnGetExtBtnGetBitmap;
  FCtrList->OnGetFormat          = FDM->OnGetFormat;
  FCtrList->OnExtBtnClick        = FDM->OnExtBtnClick;


  ColWidth = FDM->ULEditWidth/2+FOneCol*50;

  int FGroupCount = 0;
  bool FGroupOnly = true;

  TTagNode *itNode = FClassDef->GetFirstChild();
  while (itNode)
   {
     if (!itNode->CmpName("description"))
      {
        if (itNode->CmpName("group"))
         FGroupCount++;
        else
         FGroupOnly = false;
      }
     itNode = itNode->GetNext();
   }


  FLastTop = 0;
  FLastHeight = 0;
  int xTop,xLeft;

  LastLeftCtrl = NULL;
  int MaxH = 0;
  TList *CntrlList = new TList;

  TTagNode *FDefNode = NULL;

  if (FGroupOnly)
   FDefNode = FClassDesc->GetNext();
  else
   FDefNode = FClassDef;
  SubLevel = 0;
  xTop = 5; xLeft = LeftMargin;
  LastCtrl = NULL;
  CreateUnitList(CompPanel, FDefNode, xTop, &xLeft, CntrlList);
  if (LastLeftCtrl)
   {
    if ((LastLeftCtrl->Top+LastLeftCtrl->Height) <= xTop)
     MaxH = xTop;
    else
     MaxH = LastLeftCtrl->Top+LastLeftCtrl->Height;
   }
  else
   MaxH = xTop;
  ClientHeight = MaxH+50;
//     Width = ColWidth+(!FOneCol)*ColWidth+(Width-CompPanel->Width)+LeftMargin*2;
//  FCtrList->SetOnCtrlDataChange(0);
  FClassDef->GetFirstChild();
  FClassDef->Iterate(FSetFocus,*&UnicodeString(""));
  delete CntrlList; CntrlList = NULL;
  if (FIsAppend)
   {
     bool valid;
     if (FDM->OnGetDefValues)
      FDM->OnGetDefValues(-1,FClassDef,FCtrList,valid);
     Caption = FClassDef->GetChildByName("description")->AV["insertlabel"];
   }
  else
   Caption = FClassDef->GetFirstChild()->AV["editlabel"];
   // ��������� 7.10.08 ��������� �.�.
   // ��������� ������� �� �������� ������ �����������
/*   if ( ARegComp->OnAfterClassEditCreate )
   {
       ARegComp->OnAfterClassEditCreate ( (TForm*)this, FCtrList, AClassDef, RegComp->DM->quReg->Value("CODE")->AsInteger );
   }*/
}
//---------------------------------------------------------------------------
bool __fastcall TdsVacStorIncomeEditForm::FSetFocus(TTagNode *itxTag, UnicodeString& tmp)
{
  TControl *tmpCtrl;
  if (itxTag->GetAttrByName("uid"))
   {
     if (FCtrList->GetEDControl(itxTag->AV["uid"]))
      {
        tmpCtrl = FCtrList->EditItems[itxTag->AV["uid"]]->GetFirstEnabledControl();
        if (tmpCtrl)
         {
           ActiveControl = (TWinControl*)tmpCtrl;
           return true;
         }
      }
   }
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsVacStorIncomeEditForm::CreateUnitList(TWinControl *AParent, TTagNode* ANode, int &ATop, int *ALeft, TList *GroupComp, int APIndex)
{
//  TypeCB->Style->Color = DM->ReqColor;
//   if (AIsEdit)
//    {
//      if (DSStr("R1093") != "_NULL_")
//       FCurrSch = _GUI(DSStr("R1093"));
//    }
// UnicodeString FFl = "";
// AClsNode->Iterate(GetCardFields,FFl);
// FFl = "<fllist>"+FFl+"</fllist>";

// TTagNode *flNode = new TTagNode(NULL);
// flNode->AsXML = FFl;
// TTagNode *itNode = flNode->GetFirstChild();

// <extedit name='��� ��������' uid='017B' ref='1000' depend='017B.017B' isedit='0' fieldtype='integer'/>
// <date name='���� ����������' uid='1031' required='1' inlist='l' default='CurrentDate'/>
// <extedit name='�������' uid='10B5' inlist='l' depend='1031' isedit='0' length='9'/>
// <choiceDB name='�����' uid='1032' ref='002A' required='1' inlist='l'/>
// <text name='����� ���������' uid='1033' inlist='l' cast='no' length='20'/>
// <choice name='�����' uid='1034' required='1' inlist='e' default='0'>
// <binary name='������� ���������' uid='103B' required='1' inlist='e' default='check'/>
// <binary name='���� �������' uid='1041' inlist='e' invname='������� �������������'>
// <choiceDB name='������ �������' uid='10A7' ref='0038' required='1' inlist='l' depend='1032.002B'>
// <digit name='��������' uid='103F' ref='008E' required='1' inlist='l' depend='10A7.002B' digits='5' min='2' max='32767'>
// <choiceDB name='������ ������� 2' uid='0183' ref='0038' required='1' inlist='e' depend='1032.002B'>
// <digit name='�������� 2' uid='0184' inlist='e' digits='5' min='2' max='32767'>
// <choiceDB name='������ ������� 3' uid='0185' ref='0038' required='1' inlist='e' depend='1032.002B'>
// <digit name='�������� 3' uid='0186' inlist='e' digits='5' min='2' max='32767'>
// <choiceDB name='������ ������� 4' uid='0187' ref='0038' required='1' inlist='e' depend='1032.002B'>
// <digit name='�������� 4' uid='0188' inlist='e' digits='5' min='2' max='32767'>
// <choiceDB name='������ ������� 5' uid='0189' ref='0038' required='1' inlist='e' depend='1032.002B'>
// <digit name='�������� 5' uid='018A' inlist='e' digits='5' min='2' max='32767'>
// <extedit name='�����' uid='1093' required='1' inlist='e' isedit='0' length='9'/>
// <choiceDB name='���' uid='018B' ref='00C7' inlist='e' linecount='2'/>
// <choiceDB name='�������������' uid='018C' ref='000C' inlist='e' depend='018B.000C' linecount='2'/>
// <binary name='��������� � ������ ���' uid='10A5' required='1' inlist='e' invname='��������� � ������ �����������' default='check'/>
// <choiceDB name='����' uid='10B2' ref='00AA' inlist='e' depend='018B.00AA;018C.00AA' linecount='2'/>
// <choiceDB name='���. ������' uid='018D' ref='00AA' inlist='e' depend='018B.00AA;018C.00AA' linecount='2'/>
// <choiceDB name='�������� ��������������' uid='10B3' ref='00AB' inlist='e' linecount='2'/>
// <choiceDB name='���. ����������' uid='018E' ref='00BE' inlist='e' linecount='3'/>
// <digit name='���������� �������� �������' uid='10C1' digits='2' min='0' max='99' isedit='0'/>
// <digit name='���������� ������ �������' uid='10C2' digits='2' min='1' max='99' isedit='0'/>






  TdsRegEDItem* FED;
  int FOrder = 0;
  int FReak = 1;
// <extedit name='��� ��������' uid='017B' ref='1000' depend='017B.017B' isedit='0' fieldtype='integer'/>
// <extedit name='�������' uid='10B5' inlist='l' depend='1031' isedit='0' length='9'/>
// <extedit name='�����' uid='1093' required='1' inlist='e' isedit='0' length='9'/>
// <digit name='���������� �������� �������' uid='10C1' digits='2' min='0' max='99' isedit='0'/>
// <digit name='���������� ������ �������' uid='10C2' digits='2' min='1' max='99' isedit='0'/>

  // <date name='���� ����������' uid='1031' required='1' inlist='l' default='CurrentDate'/>
  FED = AddRegED("1156");  FED->Visible = false; FED->SetValue(FPatBirthday.FormatString("dd.mm.yyyy"));
  FED = AddRegED("017B");  FED->Visible = false;
  FED->SetValue(FUCode);
  FED = AddRegED("1031");  SetAlign(FED, EDLEFT1, ATop, 200, FOrder++);
  // <choiceDB name='�����' uid='1032' ref='002A' required='1' inlist='l'/>
  FED = AddRegED("1032");  ATop += SetAlign(FED, EDLEFT2, ATop, 250, FOrder++);
  FED->OnDataChange = CBChange;
  FED = AddRegED("1200");  ATop += SetAlign(FED, EDLEFT2, ATop, 250, FOrder++);
  TypeCB->Top = ATop; TypeCB->Left = 80; TypeCB->TabOrder = FOrder++;
  TypeLab->Top = ATop; TypeLab->Left = 7;
  TypeCB->Height+4;
  // <text name='����� ���������' uid='1033' inlist='l' cast='no' length='20'/>
  FED = AddRegED("1033");  ATop += SetAlign(FED, EDLEFT2, ATop, 250, FOrder++);
  // <choice name='�����' uid='1034' required='1' inlist='e' default='0'>
  FED = AddRegED("1034");  ATop += SetAlign(FED, EDLEFT1, ATop, 200, FOrder++);
  FED->OnDataChange = CBChange;

  //********** ������� ��������� uid=103B          <binary name='' required='1' inlist='e' default='check'/>
  FED = AddRegED("103B");   ATop += SetAlign(FED, EDLEFT1, ATop, 300, FOrder++);
  // <binary name='���� �������' uid='1041' inlist='e' invname='������� �������������'>
//  FED = AddRegED("1041"); ATop += SetAlign(FED, EDLEFT2, ATop, 300, FOrder++);

  // <choiceDB name='������ �������' uid='10A7' ref='0038' required='1' inlist='l' depend='1032.002B'>
  FED = AddRegED("10A7");
  ((TcxComboBox*)FED->GetFirstEnabledControl())->Properties->Alignment->Horz = taRightJustify;
  SetAlign(FED, 5, ATop, 170, FOrder++);   FED->SetEDLeft(50); FED->SetLabel("������� "); //+IntToStr(FReak++)
  // <digit name='��������' uid='103F' ref='008E' required='1' inlist='l' depend='10A7.002B' digits='5' min='2' max='32767'>
  FED = AddRegED("103F"); /*ATop += */SetAlign(FED, 175, ATop, 80, FOrder++); FED->SetEDLeft(0);
  // <choiceDB name='������ ������� 2' uid='0183' ref='0038' required='1' inlist='e' depend='1032.002B'>
//  FED = AddRegED("0183");   FED->Visible = false;//      SetAlign(FED, 5, ATop, 250, FOrder++);   FED->SetEDLeft(70); FED->SetLabel("������� "+IntToStr(FReak++));
  // <digit name='�������� 2' uid='0184' inlist='e' digits='5' min='2' max='32767'>
  FED = AddRegED("0184"); /*ATop += */SetAlign(FED, 256, ATop, 80, FOrder++); FED->SetEDLeft(0);
  // <choiceDB name='������ ������� 3' uid='0185' ref='0038' required='1' inlist='e' depend='1032.002B'>
 // FED = AddRegED("0185");   FED->Visible = false;//      SetAlign(FED, 5, ATop, 250, FOrder++);   FED->SetEDLeft(70); FED->SetLabel("������� "+IntToStr(FReak++));
  // <digit name='�������� 3' uid='0186' inlist='e' digits='5' min='2' max='32767'>
  FED = AddRegED("0186"); /*ATop += */SetAlign(FED, 337, ATop, 80, FOrder++); FED->SetEDLeft(0);
  // <choiceDB name='������ ������� 4' uid='0187' ref='0038' required='1' inlist='e' depend='1032.002B'>
 // FED = AddRegED("0187");   FED->Visible = false;//      SetAlign(FED, 5, ATop, 250, FOrder++);   FED->SetEDLeft(70); FED->SetLabel("������� "+IntToStr(FReak++));
  // <digit name='�������� 4' uid='0188' inlist='e' digits='5' min='2' max='32767'>
  FED = AddRegED("0188"); /*ATop += */SetAlign(FED, 418, ATop, 80, FOrder++); FED->SetEDLeft(0);
  // <choiceDB name='������ ������� 5' uid='0189' ref='0038' required='1' inlist='e' depend='1032.002B'>
 // FED = AddRegED("0189");   FED->Visible = false;//     SetAlign(FED, 5, ATop, 250, FOrder++);   FED->SetEDLeft(70); FED->SetLabel("������� "+IntToStr(FReak++));
  // <digit name='�������� 5' uid='018A' inlist='e' digits='5' min='2' max='32767'>
  FED = AddRegED("018A"); ATop += SetAlign(FED, 499, ATop, 80, FOrder++); FED->SetEDLeft(0);
  FED->Visible = false;

  // <choiceDB name='���' uid='018B' ref='00C7' inlist='e' linecount='2'/>
  FED = AddRegED(_LPU_);          SetAlign(FED, EDLEFT1, ATop, 250, FOrder++); FED->SetEDLeft(73);
  // <choiceDB name='�������������' uid='018C' ref='000C' inlist='e' depend='018B.000C' linecount='2'/>
  FED = AddRegED(_PLPU_); ATop += SetAlign(FED, 290, ATop, 250, FOrder++);
  // <binary name='��������� � ������ ���' uid='10A5' required='yes' inlist='list_ext' invname='��������� � ������ �����������' default='check'/>
  FED = AddRegED(_InThisLPU_);  ATop += SetAlign(FED, EDLEFT1, ATop, 300, FOrder++);
  RegComp(_LPU_)->OnDataChange  = InThisLPUChBChange;
  RegComp(_PLPU_)->OnDataChange = InThisLPUChBChange;

  // <choiceDB name='����' uid='10B2' ref='00AA' inlist='e' depend='018B.00AA;018C.00AA' linecount='2'/>
  FED = AddRegED("10B2");  SetAlign(FED, 5, ATop, 250, FOrder++); FED->SetEDLeft(68); FED->SetLabel(FED->DefNode->AV["name"]);

  // <choiceDB name='���. ������' uid='018D' ref='00AA' inlist='e' depend='018B.00AA;018C.00AA' linecount='2'/>
  FED = AddRegED("018D");  ATop += SetAlign(FED, 290, ATop, 245, FOrder++);  FED->SetLabel(FED->DefNode->AV["name"]);
  // <choiceDB name='�������� ��������������' uid='10B3' ref='00AB' inlist='e' linecount='2'/>
  FED = AddRegED("10B3");  ATop += SetAlign(FED, EDLEFT1, ATop, 300, FOrder++);
  ((TWinControl*)FED->GetControl())->Width = 290;
  FED->SetEDLeft(155);
  // <choiceDB name='���. ����������' uid='018E' ref='00BE' inlist='e' linecount='3'/>
  FED = AddRegED("018E");  ATop += SetAlign(FED, EDLEFT1, ATop, 300, FOrder++);
  ((TWinControl*)FED->GetControl())->Width = 290;
  FED->SetEDLeft(155);

  Height = ATop + 60;
}
//---------------------------------------------------------------------------
void __fastcall TdsVacStorIncomeEditForm::AlignCtrls(TList *ACtrlList)
{
  int LabR,EDL;
  int xLabR,xEDL;
  LabR = 0; EDL = 9999;
  TdsRegEDItem *tmpED;
  for (int i = 0; i < ACtrlList->Count; i++)
   {
     // ���������� max ������ ������� ����� ���������
     // � min ����� ����� ���������
     tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
     tmpED->GetEDLeft(&xLabR,&xEDL);
     if (xLabR != -1) LabR = max(xLabR,LabR);
     if (xEDL != -1)  EDL  = min(xEDL,EDL);
   }
  if ((LabR+2) < EDL)
   {
     // max ������ ������� ����� ���������
     // � min ����� ����� ��������� �� ������������
     // ����������� �������� �� min ����� ������� ����� ���������
     for (int i = 0; i < ACtrlList->Count; i++)
      {
        tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
        tmpED->SetEDLeft(EDL);
      }
   }
  else
   {
     // ����� ����������� �� "max ������ ������� ����� ���������" + 2
     for (int i = 0; i < ACtrlList->Count; i++)
      {
        tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
        tmpED->SetEDLeft(LabR+2);
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsVacStorIncomeEditForm::FormKeyDown(TObject *Sender, WORD &Key,  TShiftState Shift)
{
  if (Key == VK_RETURN)
   {ActiveControl = OkBtn; OkBtnClick(this);}
}
//---------------------------------------------------------------------------
void __fastcall TdsVacStorIncomeEditForm::OkBtnClick(TObject *Sender)
{
  TWinControl *tmpCtrl = ActiveControl;
  ActiveControl = OkBtn;
  ActiveControl = tmpCtrl;
  if (CheckInput())
   { // ������ �� �������� ������
     try
      {
        UnicodeString PRC = FDataSrc->DataSet->Post(FInsertedRecId, true);
        if (!SameText(PRC,"ok"))
          _MSG_ERR(PRC,"������");
        else
         ModalResult = mrOk;
      }
     catch (EkabCustomDataSetError &E)
      {
        _MSG_ERR(E.Message,"������");
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TdsVacStorIncomeEditForm::CheckInput()
{
  bool RC = false;
  try
   {
     UnicodeString xValid = "";
     FClassDef->Iterate(GetInput,xValid);
     if (!xValid.Length())
      {
        RC = true;
        if (FDM->OnExtValidate)
         {
           if (FIsAppend)
            FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
           else
            FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsVacStorIncomeEditForm::GetInput(TTagNode *itTag, UnicodeString &UID)
{
   if (itTag)
    {
      if (FCtrList->GetEDControl(itTag->AV["uid"]))
       if (!FCtrList->EditItems[itTag->AV["uid"]]->CheckReqValue())
        {
           UID = "no";
           return true;
        }
    }
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsVacStorIncomeEditForm::FormDestroy(TObject *Sender)
{
   FCtrList->ClearOnCtrlDataChange(0);
   delete FCtrList->DataProvider;
   delete FCtrList;
}
//---------------------------------------------------------------------------
bool __fastcall TdsVacStorIncomeEditForm::FCtrlDataChange(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource)
{
  bool RC = false;
  if (FDM->OnCtrlDataChange)
   {
     RC = FDM->OnCtrlDataChange(ItTag, Src, ASource, FIsAppend, FCtrList, UIDInt(FClassDef->AV["uid"]));
   }
  return RC;
}
//---------------------------------------------------------------------------
int  __fastcall TdsVacStorIncomeEditForm::SetAlign(TdsRegEDItem* AItem, int ALeft, int ATop, int AWidth, int ATabOrder)
{
  AItem->Left = ALeft;
  AItem->Width = AWidth;
  AItem->Top = ATop;
  AItem->Anchors.Clear();
  AItem->Anchors << akLeft << akRight << akTop;
  AItem->TabOrder = ATabOrder;
  return AItem->Height+4;
}
//---------------------------------------------------------------------------
inline TdsRegEDItem* __fastcall TdsVacStorIncomeEditForm::RegComp(UnicodeString AUID)
{
  return FCtrList->EditItems[AUID];
}
//---------------------------------------------------------------------------
inline TdsRegEDItem* __fastcall TdsVacStorIncomeEditForm::AddRegED(UnicodeString AUID)
{
  return FCtrList->AddEditItem(FClassDef->GetTagByUID(AUID), FIsAppend);
}
//---------------------------------------------------------------------------
bool __fastcall TdsVacStorIncomeEditForm::InThisLPUChBChange(TTagNode *ItTag, UnicodeString &Src)
{
  if (FDM->LPUCode != -1)
   {
     if (RegComp(_LPU_)->GetValue("").ToIntDef(0) == FDM->LPUCode)
      {
        if (FDM->LPUPartCode != -1)
         {
           if (RegComp(_PLPU_)->GetValue("").ToIntDef(0) == FDM->LPUPartCode)
            FCtrList->EditItems[_InThisLPU_]->SetValue("1","");
           else
            FCtrList->EditItems[_InThisLPU_]->SetValue("0","");
         }
        else
         {
           FCtrList->EditItems[_InThisLPU_]->SetValue("1","");
         }
      }
     else
      FCtrList->EditItems[_InThisLPU_]->SetValue("0","");
   }
  return true;
}
//---------------------------------------------------------------------------
bool __fastcall TdsVacStorIncomeEditForm::CBChange(TTagNode *ItTag, UnicodeString &Src)
{
  bool RC = false;
  try
   {
     UnicodeString xProb,xOldProbVal,xOldProbFormat,xProbDate;
     xProb = RegComp("1032")->GetValue("002A.");
     TypeCB->Properties->Items->Clear();
     int FInfCode = -1;

     TIntListMap::iterator FRC = FDM->ProbInfList.find(xProb.ToIntDef(-1));
     if (FRC != FDM->ProbInfList.end())
      {
        if (FRC->second->size())
         FInfCode = FRC->second->begin()->first;
      }
     if (FInfCode != -1)
      RegComp("1200")->SetValue(IntToStr(FInfCode));


     if ((RegComp("1034")->GetValue("").ToIntDef(0) != 1) && (FInfCode != -1))
      {
//            DM->qtExec("Select R1079 From CARD_1076 Where UCODE="+IntToStr(FUCode)+"and  R1077="+FtmpInfCode,false);
//            if (!FCurrSch.Length())
//             FCurrSch = _GUI(quFNStr("R1079"));
        TTagNode *tmp;// = DM->xmlTestDef->GetTagByUID(FCurrSch);
//            if (tmp)
//             {
           TTagNodeMap SchList;
           FDM->GetSchemeList(FInfCode, SchList, false);
           for (TTagNodeMap::iterator i = SchList.begin(); i != SchList.end(); i++)
            {
              if (i->second->CmpAV("ref",xProb))
               {
                 tmp = i->second->GetFirstChild();
                 while (tmp)
                  {
                    if (tmp->CmpName("line"))
                     {
                       TypeCB->Properties->Items->AddObject(i->second->AV["name"]+"."+tmp->AV["name"],(TObject*)UIDInt(tmp->AV["uid"]));
                     }
                    tmp = tmp->GetNext();
                  }
               }
            }
//       }
      }
     else
      {
  //      FCurrSch = "";
        TypeCB->Properties->Items->AddObject("���.",(TObject*)0);
      }
     if (TypeCB->Properties->Items->Count == 1)
      TypeCB->ItemIndex = 0;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

