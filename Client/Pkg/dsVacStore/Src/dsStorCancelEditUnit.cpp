//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsStorCancelEditUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TdsStorCancelEditForm *dsStorCancelEditForm;
//---------------------------------------------------------------------------
/*
// <date     name='���� ��������'         uid='112A' required='1' inlist='l'/>
// <choiceDB name='����'                  uid='1151' ref='0035' required='1' inlist='l'/>
// <text     name='�����'                 uid='1152' required='1' inlist='l' cast='no' length='20'/>
// <digit    name='���������� (����)'     uid='1153' required='1' inlist='l' digits='10' min='0' max='99999999'/>
// <digit    name='���������� (��������)' uid='1154' digits='10' min='0' max='99999999'/>
*/

//---------------------------------------------------------------------------
#define EDLEFT1  2
#define EDLEFT2  232
//---------------------------------------------------------------------------
const int LeftMargin = 2;
const int LevelCorrection = 5;
__fastcall TdsStorCancelEditForm::TdsStorCancelEditForm(TComponent* Owner, bool AIsAppend, UnicodeString AClassId, TdsICStoreDM *ADM, TTagNode* ARoot, TkabCustomDataSource *ADataSrc, TdsRegTemplateData *ATmplData, bool AOneCol)
    : TForm(Owner)
{
  FDM = ADM;
  FDataSrc = ADataSrc;
  FOneCol = AOneCol;
  FInsertedRecId = "";

  OkBtn->Caption = FMT(icsRegClassifEditApplyBtnCaption);
  CancelBtn->Caption = FMT(icsRegClassifEditCancelBtnCaption);
  FIsAppend = AIsAppend;
  if (FIsAppend)
   FDataSrc->DataSet->Insert();
  else
   FDataSrc->DataSet->Edit();

  FClassDef = ARoot->GetTagByUID(AClassId);
  FClassDesc = FClassDef->GetChildByName("description");

  FCtrList                = new TdsRegEDContainer(this,CompPanel);
  FCtrList->UseHash       = true;
  FCtrList->DefXML        = ARoot;
  FCtrList->ReqColor      = FDM->RequiredColor;
  FCtrList->StyleController = FDM->StyleController;
  FCtrList->TemplateData    = ATmplData;
  FCtrList->DataPath        = FDM->DataPath;

  FCtrList->DataProvider = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider*)FCtrList->DataProvider)->DefXML = ARoot;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  ((TdsRegEDkabDSDataProvider*)FCtrList->DataProvider)->DataSource    = FDataSrc;
  FCtrList->DataProvider->OnGetCompValue       = FDM->OnGetCompValue;

  FCtrList->OnDataChange         = FCtrlDataChange;
  FCtrList->OnGetExtBtnGetBitmap = FDM->OnGetExtBtnGetBitmap;
  FCtrList->OnGetFormat          = FDM->OnGetFormat;
  FCtrList->OnExtBtnClick        = FDM->OnExtBtnClick;
  FCtrList->OnKeyDown            = FormKeyDown;


  ColWidth = FDM->ULEditWidth/2+FOneCol*50;

  int FGroupCount = 0;
  bool FGroupOnly = true;

  TTagNode *itNode = FClassDef->GetFirstChild();
  while (itNode)
   {
     if (!itNode->CmpName("description"))
      {
        if (itNode->CmpName("group"))
         FGroupCount++;
        else
         FGroupOnly = false;
      }
     itNode = itNode->GetNext();
   }


  FLastTop = 0;
  FLastHeight = 0;
  int xTop,xLeft;

  LastLeftCtrl = NULL;
  int MaxH = 0;
  TList *CntrlList = new TList;

  TTagNode *FDefNode = NULL;

  if (FGroupOnly)
   FDefNode = FClassDesc->GetNext();
  else
   FDefNode = FClassDef;
  SubLevel = 0;
  xTop = 5; xLeft = LeftMargin;
  LastCtrl = NULL;
  CreateUnitList(CompPanel, FDefNode, xTop, &xLeft, CntrlList);
  if (LastLeftCtrl)
   {
    if ((LastLeftCtrl->Top+LastLeftCtrl->Height) <= xTop)
     MaxH = xTop;
    else
     MaxH = LastLeftCtrl->Top+LastLeftCtrl->Height;
   }
  else
   MaxH = xTop;
  ClientHeight = MaxH+50;
//     Width = ColWidth+(!FOneCol)*ColWidth+(Width-CompPanel->Width)+LeftMargin*2;
//  FCtrList->SetOnCtrlDataChange(0);
  FClassDef->GetFirstChild();
  FClassDef->Iterate(FSetFocus,*&UnicodeString(""));
  delete CntrlList; CntrlList = NULL;
  if (FIsAppend)
   {
     bool valid;
     if (FDM->OnGetDefValues)
      FDM->OnGetDefValues(-1,FClassDef,FCtrList,valid);
     Caption = FClassDef->GetChildByName("description")->AV["insertlabel"];
   }
  else
   Caption = FClassDef->GetFirstChild()->AV["editlabel"];
   // ��������� 7.10.08 ��������� �.�.
   // ��������� ������� �� �������� ������ �����������
/*   if ( ARegComp->OnAfterClassEditCreate )
   {
       ARegComp->OnAfterClassEditCreate ( (TForm*)this, FCtrList, AClassDef, RegComp->DM->quReg->Value("CODE")->AsInteger );
   }*/
}
//---------------------------------------------------------------------------
bool __fastcall TdsStorCancelEditForm::FSetFocus(TTagNode *itxTag, UnicodeString& tmp)
{
  TControl *tmpCtrl;
  if (itxTag->GetAttrByName("uid"))
   {
     if (FCtrList->GetEDControl(itxTag->AV["uid"]))
      {
        tmpCtrl = FCtrList->EditItems[itxTag->AV["uid"]]->GetFirstEnabledControl();
        if (tmpCtrl)
         {
           ActiveControl = (TWinControl*)tmpCtrl;
           return true;
         }
      }
   }
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsStorCancelEditForm::CreateUnitList(TWinControl *AParent, TTagNode* ANode, int &ATop, int *ALeft, TList *GroupComp, int APIndex)
{
// <date     name='���� ��������'         uid='112A' required='1' inlist='l'/>
// <choiceDB name='����'                  uid='1151' ref='0035' required='1' inlist='l'/>
// <text     name='�����'                 uid='1152' required='1' inlist='l' cast='no' length='20'/>
// <digit    name='���������� (����)'     uid='1153' required='1' inlist='l' digits='10' min='0' max='99999999'/>
// <digit    name='���������� (��������)' uid='1154' digits='10' min='0' max='99999999'/>


  TdsRegEDItem* FED;
  int FOrder = 0;
  int FReak = 1;
  // <choiceDB name='����'                  uid='1151' ref='0035' required='1' inlist='l'/>
  FED = AddRegED("32E6");  FED->Visible = false;
  FED = AddRegED("1151");  ATop += SetAlign(FED, EDLEFT1, ATop, 250, FOrder++);
  // <date     name='���� ��������'         uid='112A' required='1' inlist='l'/>
  FED = AddRegED("112A");  SetAlign(FED, EDLEFT1, ATop, 250, FOrder++);
  if (FIsAppend)   FED->SetValue(Date().FormatString("dd.mm.yyyy"));
  // <text     name='�����'                 uid='1152' required='1' inlist='l' cast='no' length='20'/>
  FED = AddRegED("1152");  ATop += SetAlign(FED, EDLEFT2, ATop, 250, FOrder++);
  // <digit    name='���������� (����)'     uid='1153' required='1' inlist='l' digits='10' min='0' max='99999999'/>
  FED = AddRegED("1153");  ATop += SetAlign(FED, EDLEFT1, ATop, 200, FOrder++);
  // <digit    name='���������� (��������)' uid='1154' digits='10' min='0' max='99999999'/>
  FED = AddRegED("1154");  FED->Visible = false;

  Height = ATop + 60;
}
//---------------------------------------------------------------------------
void __fastcall TdsStorCancelEditForm::AlignCtrls(TList *ACtrlList)
{
  int LabR,EDL;
  int xLabR,xEDL;
  LabR = 0; EDL = 9999;
  TdsRegEDItem *tmpED;
  for (int i = 0; i < ACtrlList->Count; i++)
   {
     // ���������� max ������ ������� ����� ���������
     // � min ����� ����� ���������
     tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
     tmpED->GetEDLeft(&xLabR,&xEDL);
     if (xLabR != -1) LabR = max(xLabR,LabR);
     if (xEDL != -1)  EDL  = min(xEDL,EDL);
   }
  if ((LabR+2) < EDL)
   {
     // max ������ ������� ����� ���������
     // � min ����� ����� ��������� �� ������������
     // ����������� �������� �� min ����� ������� ����� ���������
     for (int i = 0; i < ACtrlList->Count; i++)
      {
        tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
        tmpED->SetEDLeft(EDL);
      }
   }
  else
   {
     // ����� ����������� �� "max ������ ������� ����� ���������" + 2
     for (int i = 0; i < ACtrlList->Count; i++)
      {
        tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
        tmpED->SetEDLeft(LabR+2);
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsStorCancelEditForm::FormKeyDown(TObject *Sender, WORD &Key,  TShiftState Shift)
{
  if (Key == VK_RETURN)
   {ActiveControl = OkBtn; OkBtnClick(this);}
}
//---------------------------------------------------------------------------
void __fastcall TdsStorCancelEditForm::OkBtnClick(TObject *Sender)
{
  TWinControl *tmpCtrl = ActiveControl;
  ActiveControl = OkBtn;
  ActiveControl = tmpCtrl;
  if (CheckInput())
   { // ������ �� �������� ������
     try
      {
        UnicodeString PRC = FDataSrc->DataSet->Post(FInsertedRecId, true);
        if (!SameText(PRC,"ok"))
          _MSG_ERR(PRC,"������");
        else
         ModalResult = mrOk;
      }
     catch (EkabCustomDataSetError &E)
      {
        _MSG_ERR(E.Message,"������");
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TdsStorCancelEditForm::CheckInput()
{
  bool RC = false;
  try
   {
     if (FIsAppend)
      FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
     else
      FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
     UnicodeString xValid = "";
     FClassDef->Iterate(GetInput,xValid);
     if (!xValid.Length())
      {
        RC = true;
        if (FDM->OnExtValidate)
         {
           if (FIsAppend)
            FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
           else
            FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsStorCancelEditForm::GetInput(TTagNode *itTag, UnicodeString &UID)
{
   if (itTag)
    {
      if (FCtrList->GetEDControl(itTag->AV["uid"]))
       if (!FCtrList->EditItems[itTag->AV["uid"]]->CheckReqValue())
        {
           UID = "no";
           return true;
        }
    }
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsStorCancelEditForm::FormDestroy(TObject *Sender)
{
   FCtrList->ClearOnCtrlDataChange(0);
   delete FCtrList->DataProvider;
   delete FCtrList;
}
//---------------------------------------------------------------------------
bool __fastcall TdsStorCancelEditForm::FCtrlDataChange(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource)
{
  bool RC = false;
  if (FDM->OnCtrlDataChange)
   {
     RC = FDM->OnCtrlDataChange(ItTag, Src, ASource, FIsAppend, FCtrList, UIDInt(FClassDef->AV["uid"]));
   }
  return RC;
}
//---------------------------------------------------------------------------
int  __fastcall TdsStorCancelEditForm::SetAlign(TdsRegEDItem* AItem, int ALeft, int ATop, int AWidth, int ATabOrder)
{
  AItem->Left = ALeft;
  AItem->Width = AWidth;
  AItem->Top = ATop;
  AItem->Anchors.Clear();
  AItem->Anchors << akLeft << akRight << akTop;
  AItem->TabOrder = ATabOrder;
  return AItem->Height+4;
}
//---------------------------------------------------------------------------
inline TdsRegEDItem* __fastcall TdsStorCancelEditForm::RegComp(UnicodeString AUID)
{
  return FCtrList->EditItems[AUID];
}
//---------------------------------------------------------------------------
inline TdsRegEDItem* __fastcall TdsStorCancelEditForm::AddRegED(UnicodeString AUID)
{
  return FCtrList->AddEditItem(FClassDef->GetTagByUID(AUID), FIsAppend);
}
//---------------------------------------------------------------------------

