//---------------------------------------------------------------------------
#ifndef dsStorCancelEditUnitH
#define dsStorCancelEditUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
//---------------------------------------------------------------------------
#include "KabCustomDS.h"
#include "dsICStoreDMUnit.h"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
class PACKAGE TdsStorCancelEditForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *CompPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
private:	// User declarations
    TdsICStoreDM *FDM;
    TkabCustomDataSource *FDataSrc;
    bool     FIsAppend, FOneCol;
//    TTagNode *Root;
    TTagNode *FClassDef,*FClassDesc;
    int      ColWidth, SubLevel;
    UnicodeString  FInsertedRecId;
    TdsRegEDItem   *LastLeftCtrl;
    TdsRegEDItem   *LastCtrl;
    TcxPageControl *Group1st;
    TcxTabSheet    *gPage;

    TdsRegEDContainer *FCtrList;
    int FLastTop,FLastHeight;

    void __fastcall AlignCtrls(TList *ACtrlList);
    bool __fastcall GetInput(TTagNode *uiTag, UnicodeString &UID);
    bool __fastcall CheckInput();
    bool __fastcall FCtrlDataChange(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource);
    bool __fastcall FSetFocus(TTagNode *itxTag, UnicodeString& tmp);
    void __fastcall CreateUnitList(TWinControl *AParent, TTagNode* ANode,int &ATop,int *ALeft,TList *GroupComp,int APIndex = 0);
    int  __fastcall SetAlign(TdsRegEDItem* AItem, int ALeft, int ATop, int AWidth, int ATabOrder);
    inline TdsRegEDItem* __fastcall RegComp(UnicodeString AUID);
    inline TdsRegEDItem* __fastcall AddRegED(UnicodeString AUID);

public:
    __property UnicodeString  InsertedRecId = {read=FInsertedRecId};
    __fastcall TdsStorCancelEditForm(TComponent* Owner, bool AIsAppend, UnicodeString AClassId, TdsICStoreDM *ADM, TTagNode* ARoot, TkabCustomDataSource *ADataSrc, TdsRegTemplateData *ATmplData, bool AOneCol = true);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsStorCancelEditForm *dsStorCancelEditForm;
//---------------------------------------------------------------------------
#endif
