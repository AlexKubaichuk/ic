// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsGroupCancellationUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxCalendar"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxProgressBar"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma link "cxBarEditItem"
#pragma link "cxCheckBox"
#pragma resource "*.dfm"
TdsGroupCancellationForm * dsGroupCancellationForm;
// ---------------------------------------------------------------------------
__fastcall TdsGroupCancellationForm::TdsGroupCancellationForm(TComponent * Owner, TdsICStoreDM * ADM) : TForm(Owner)
 {
  FDM      = ADM;
  MIBPList = new TStringList;
  GetPrivTestData();
  MIBPInStoreChBPropertiesChange(MIBPInStoreChB);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsGroupCancellationForm::GetPrivTestData()
 {
  // RC->AddPair(FQ->FieldByName("ps1")->AsString, FQ->FieldByName("ps2")->AsString + ":" + FQ->FieldByName("ps3")->AsString);
  TJSONObject * RetData = NULL;
  TJSONPairEnumerator * itPair;
  UnicodeString FVal1, FVal2, FVal3;
  TcxTreeListNode * itNode;
  TStringList * MIBPCount = new TStringList;
  try
   {
    RetData = FDM->kabCDSGetValById("reg.s2.Select Code as ps1, R0040 as ps2 From CLASS_0035", "0");
    itPair  = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FVal1 = ((TJSONString *)itPair->Current->JsonString)->Value();
      FVal2 = ((TJSONString *)itPair->Current->JsonValue)->Value();
      MIBPList->AddObject(FVal2, (TObject *)FVal1.ToIntDef(0));
     }
    RetData = FDM->kabCDSGetValById
      ("reg.s2.Select (cast(R113F as varchar(10))||'_'|| R1140) as ps1, R1141 as ps2 From CLASS_1121", "0");
    itPair = RetData->GetEnumerator();
    while (itPair->MoveNext())
     {
      FVal1 = ((TJSONString *)itPair->Current->JsonString)->Value();
      FVal2 = ((TJSONString *)itPair->Current->JsonValue)->Value();
      MIBPCount->AddObject(FVal1, (TObject *)FVal2.ToIntDef(0));
     }
    RetData = FDM->kabCDSGetValById
      ("reg.s3.Select count(R3020) as ps1, R3020 as ps2,  R3023  as ps3 From CLASS_3003 where R3309 is null Group By R3020, R3023",
      "0");
    itPair = RetData->GetEnumerator();
    int idx;
    while (itPair->MoveNext())
     {
      FVal1            = ((TJSONString *)itPair->Current->JsonString)->Value();
      FVal2            = ((TJSONString *)itPair->Current->JsonValue)->Value();
      FVal3            = GetRPartB(FVal2, ':');
      FVal2            = GetLPartB(FVal2, ':');
      itNode           = MIBPTL->Root->AddChild();
      itNode->Texts[0] = GetMIBPName(FVal2.ToIntDef(0));
      itNode->Texts[1] = FVal3;
      itNode->Texts[2] = FVal1;
      itNode->Texts[3] = 1;
      itNode->Texts[4] = FVal1;
      itNode->Texts[5] = "0";
      itNode->Texts[6] = "0";
      itNode->Texts[7] = FVal2;
      // itNode->Texts[7] = FVal2 + "_" + FVal3;
      idx = MIBPCount->IndexOf(FVal2 + "_" + FVal3);
      if (idx != -1)
       {
        itNode->Texts[5] = IntToStr((int)MIBPCount->Objects[idx]);
        itNode->Texts[6] = "1";
       }
     }
   }
  __finally
   {
    delete MIBPCount;
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsGroupCancellationForm::GetMIBPName(int ACode)
 {
  UnicodeString RC = "";
  try
   {
    int Idx = MIBPList->IndexOfObject((TObject *)ACode);
    if (Idx != -1)
     RC = MIBPList->Strings[Idx];
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsGroupCancellationForm::actApplyExecute(TObject * Sender)
 {
  if (MIBPTL->IsEditing)
   MIBPTL->HideEdit();
  MIBPTL->BeginUpdate();
  TcxTreeListNode * itNode;
  int PrivCount = 0;
  try
   {
    for (int i = 0; i < MIBPTL->Root->Count; i++)
     {
      if ((bool)MIBPTL->Root->Items[i]->Visible)
       PrivCount++ ;
     }
    PBar->Properties->Max = PrivCount * 2;
    PBar->Position = 0;
    for (int i = 0; i < MIBPTL->Root->Count; i++)
     {
      itNode = MIBPTL->Root->Items[i];
      if ((bool)itNode->Visible)
       {
        PBar->Position += 1;
        FDM->WriteOff(itNode->Texts[7].ToIntDef(0), itNode->Texts[1], itNode->Texts[4].ToIntDef(0),
          itNode->Texts[3].ToIntDef(0));
        Application->ProcessMessages();
        PBar->Position += 1;
       }
     }
   }
  __finally
   {
    MIBPTL->EndUpdate();
    PBar->Position = 0;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsGroupCancellationForm::actDeleteExecute(TObject * Sender)
 {
  TcxTreeListNode * delNode = NULL;
  try
   {
    if (MIBPTL->SelectionCount)
     {
      delNode = MIBPTL->Selections[0];
      if (delNode)
       delNode->Delete();
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsGroupCancellationForm::FormDestroy(TObject * Sender)
 {
  delete MIBPList;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsGroupCancellationForm::MIBPInStoreChBPropertiesChange(TObject * Sender)
 {
  MIBPTL->BeginUpdate();
  try
   {
    for (int i = 0; i < MIBPTL->Root->Count; i++)
     {
      if ((bool)MIBPInStoreChB->EditValue)
       MIBPTL->Root->Items[i]->Visible = ((bool)MIBPInStoreChB->EditValue) == (bool)MIBPTL->Root->Items[i]->Texts[6]
         .ToIntDef(0);
      else
       MIBPTL->Root->Items[i]->Visible = true;
     }
   }
  __finally
   {
    MIBPTL->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsGroupCancellationForm::ReacValColPropertiesChange(TObject * Sender)
 {
  if (MIBPTL->SelectionCount)
   {
    try
     {
      if (MIBPTL->IsEditing)
       MIBPTL->Selections[0]->Texts[4] = MIBPTL->Selections[0]->Texts[2].ToIntDef(0) * (int)
         MIBPTL->InplaceEditor->EditingValue;
      else
       MIBPTL->Selections[0]->Texts[4] = MIBPTL->Selections[0]->Texts[2].ToIntDef(0) * MIBPTL->Selections[0]->Texts[3]
         .ToIntDef(1);
     }
    catch (...)
     {
     }
   }
 }
// ---------------------------------------------------------------------------
