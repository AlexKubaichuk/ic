//---------------------------------------------------------------------------
#ifndef dsICStoreDMUnitH
#define dsICStoreDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Datasnap.DSClientRest.hpp>
//#include <IPPeerClient.hpp>
//---------------------------------------------------------------------------
#include "dsRegClient.h"
#include "dsRegED.h"
#include "dsRegConstDef.h"
//#include "ICSClassData.h"
//#include "ServerClientClasses.h"
#include "dsICStoreTypeDef.h"
#include "ExtUtils.h"
//---------------------------------------------------------------------------
/*#include <Classes.hpp>
#include <DB.hpp>
#include <DBCtrls.hpp>
#include <Registry.hpp>
#include <ADODB.hpp>
#include "FIBDatabase.hpp"
#include "FIBDataSet.hpp"
#include "FIBQuery.hpp"
#include "pFIBDatabase.hpp"
#include "pFIBDataSet.hpp"
#include "pFIBQuery.hpp"
//---------------------------------------------------------------------------
#include "RegED.h"
#include "icsRegConstDef.h"
#include "ICSClassData.h"  */
//---------------------------------------------------------------------------
//namespace IcsReg
//{
//---------------------------------------------------------------------------
class PACKAGE TdsICStoreDM : public TDataModule
{
__published:	// IDE-managed Components
        void __fastcall DataModuleCreate(TObject *Sender);
        void __fastcall DataModuleDestroy(TObject *Sender);

private:
  int           FDefaultClassWidth,FULEditWidth;
  bool          FFullEdit;
  bool          FClassEditable;
  UnicodeString FClassShowExpanded;

  TColor         FRequiredColor;

  UnicodeString FTablePrefix;
  UnicodeString FRegistryKey;
  TTagNode      *FRegDefNode;
  TTagNode      *FClassDefNode;
  TAxeXMLContainer *FXMLList;
  UnicodeString       FXMLPath;
  UnicodeString       FDataPath;
  TdsRegClient     *FRegComp;

  TGetCompValue         FOnGetCompValue;
  TExtBtnGetBitmapEvent FOnGetExtBtnGetBitmap;
  TRegGetFormatEvent    FOnGetFormat;
  TExtBtnClick          FExtBtnClick;
  TExtValidateData      FOnGetDefValues;
  TExtValidateData      FOnExtValidate;
  TExtBtnClick          FOnCtrlDataChange;

  TExtEditData          FExtInsertData;
  TExtEditData          FExtEditData;

  TClassEditEvent       FOnClassAfterInsert;
  TClassEditEvent       FOnClassAfterEdit;
  TClassEditEvent       FOnClassAfterDelete;

  TUnitEvent            FOnOpenCard;
  TGetReportListEvent   FGetQuickFilterList;
  TGetReportListEvent   FReportList;
  TdsRegReportClickEvent FPrintReportClick;
  TcxEditStyleController *FStyleController;

  TdsGetClassXMLEvent FOnGetClassXML;
  TdsGetCountEvent    FOnGetCount;
  TdsGetIdListEvent   FOnGetIdList;
  TdsGetValByIdEvent  FOnGetValById;
  TdsGetValById10Event  FOnGetValById10;
  TdsFindEvent        FOnFind;
  TdsInsertDataEvent  FOnInsertData;
  TdsEditDataEvent    FOnEditData;
  TdsDeleteDataEvent  FOnDeleteData;
  TdsGetMIBPFormEvent FOnGetMIBPForm;
  TdsStoreWriteOff    FOnWriteOff;


//  TdsICClassClient   *FAppClient;

  //    TClassHashTable *FClassHash;
  TTagNode* __fastcall FGetClassDef();
//  TTagNode* __fastcall FGetRegDefNode();

public:
//  TICSClassData *FClassData;
  UnicodeString   DTFormats[3];
//  __int64         *SearchList;
//  __int64         SearchIndex,SeachCount;
  TObject      *UnitList;
  TObject      *UnitCard;
  TTagNode     *SelRegUnit;
  UnicodeString __fastcall NewGUID();
  __fastcall TdsICStoreDM(TComponent* Owner);

  __property TAxeXMLContainer *XMLList = {read=FXMLList, write=FXMLList};
  void __fastcall OnGetXML(UnicodeString AId, TTagNode *& ADef);

  UnicodeString __fastcall kabCDSGetClassXML(UnicodeString AId);
  __int64       __fastcall kabCDSGetCount(UnicodeString AId, UnicodeString ATTH, UnicodeString AFilterParam);
  TJSONObject*  __fastcall kabCDSGetCodes(UnicodeString AId, int AMax, UnicodeString ATTH, UnicodeString AFilterParam);
  TJSONObject*  __fastcall kabCDSGetValById(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  __fastcall kabCDSGetValById10(UnicodeString AId, UnicodeString ARecId);
  UnicodeString __fastcall kabCDSInsert(UnicodeString AId, TJSONObject* AValues, UnicodeString &ARecId);
  UnicodeString __fastcall kabCDSEdit(UnicodeString AId, TJSONObject* AValues, UnicodeString &ARecId);
  UnicodeString __fastcall kabCDSDelete(UnicodeString AId, UnicodeString ARecId);
  UnicodeString __fastcall kabCDSGetMIBPForm();
  void          __fastcall WriteOff(int AMIBPCode, UnicodeString  ASer, int ADoseCount, int ADosePerPrivCount);


  __property int DefaultClassWidth = {read=FDefaultClassWidth, write=FDefaultClassWidth};
  __property int ULEditWidth = {read=FULEditWidth, write=FULEditWidth};

  __property bool FullEdit = {read=FFullEdit, write=FFullEdit};
  __property UnicodeString TablePrefix = {read=FTablePrefix, write=FTablePrefix};
  __property UnicodeString RegistryKey = {read=FRegistryKey, write=FRegistryKey};
  __property TTagNode* ClassDef = {read=FGetClassDef};
  __property TTagNode* RegDef = {read=FRegDefNode, write = FRegDefNode};

//  __property TdsICClassClient   *Connection = {read=FAppClient, write=FAppClient};

  __property bool ClassEditable = {read=FClassEditable, write=FClassEditable};
  __property UnicodeString ClassShowExpanded = {read=FClassShowExpanded, write=FClassShowExpanded};

  __property TcxEditStyleController *StyleController = {read=FStyleController,write=FStyleController};
  __property TColor RequiredColor                    = {read=FRequiredColor,  write=FRequiredColor};

  __property TGetCompValue         OnGetCompValue       = {read=FOnGetCompValue, write=FOnGetCompValue};
  __property TExtBtnGetBitmapEvent OnGetExtBtnGetBitmap = {read=FOnGetExtBtnGetBitmap, write=FOnGetExtBtnGetBitmap};
  __property TRegGetFormatEvent    OnGetFormat          = {read=FOnGetFormat, write=FOnGetFormat};
  __property TExtBtnClick          OnExtBtnClick        = {read=FExtBtnClick, write=FExtBtnClick};
  __property TExtValidateData      OnGetDefValues       = {read=FOnGetDefValues, write=FOnGetDefValues};
  __property TExtValidateData      OnExtValidate        = {read=FOnExtValidate, write=FOnExtValidate};
  __property TExtBtnClick          OnCtrlDataChange     = {read=FOnCtrlDataChange, write=FOnCtrlDataChange};

  __property TExtEditData          OnExtInsertData      = {read=FExtInsertData, write=FExtInsertData};
  __property TExtEditData          OnExtEditData        = {read=FExtEditData, write=FExtEditData};

  __property TClassEditEvent OnClassAfterInsert = {read=FOnClassAfterInsert,write=FOnClassAfterInsert};
  __property TClassEditEvent OnClassAfterEdit   = {read=FOnClassAfterEdit,write=FOnClassAfterEdit};
  __property TClassEditEvent OnClassAfterDelete = {read=FOnClassAfterDelete,write=FOnClassAfterDelete};


  __property TUnitEvent          OnOpenCard         = {read=FOnOpenCard,write=FOnOpenCard};
  __property TGetReportListEvent OnQuickFilterList  = {read=FGetQuickFilterList, write=FGetQuickFilterList};
  __property TGetReportListEvent OnReportList       = {read=FReportList, write=FReportList};
  __property TdsRegReportClickEvent OnPrintReportClick = {read=FPrintReportClick, write=FPrintReportClick};
  __property TdsStoreWriteOff OnWriteOff = {read=FOnWriteOff, write=FOnWriteOff};

  __property TdsGetClassXMLEvent OnGetClassXML  = {read=FOnGetClassXML,write=FOnGetClassXML};
  __property TdsGetCountEvent OnGetCount  = {read=FOnGetCount,write=FOnGetCount};
  __property TdsGetIdListEvent OnGetIdList  = {read=FOnGetIdList,write=FOnGetIdList};
  __property TdsGetValByIdEvent OnGetValById  = {read=FOnGetValById,write=FOnGetValById};
  __property TdsGetValById10Event OnGetValById10  = {read=FOnGetValById10,write=FOnGetValById10};
  __property TdsFindEvent OnFind  = {read=FOnFind,write=FOnFind};
  __property TdsInsertDataEvent OnInsertData  = {read=FOnInsertData,write=FOnInsertData};
  __property TdsEditDataEvent OnEditData  = {read=FOnEditData,write=FOnEditData};
  __property TdsDeleteDataEvent OnDeleteData  = {read=FOnDeleteData,write=FOnDeleteData};
  __property TdsGetMIBPFormEvent      OnGetMIBPForm  = {read=FOnGetMIBPForm,write=FOnGetMIBPForm};
  __property UnicodeString       XMLPath = {read=FXMLPath, write=FXMLPath};
  __property UnicodeString       DataPath = {read=FDataPath, write=FDataPath};
  __property TdsRegClient     *RegComp   = {read=FRegComp, write=FRegComp};
};
//}; // end of namespace DataExchange
//using namespace IcsReg;
//---------------------------------------------------------------------------
extern PACKAGE TdsICStoreDM *dsICStoreDM;
//---------------------------------------------------------------------------
#endif

