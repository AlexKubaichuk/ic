// ---------------------------------------------------------------------------
#include <vcl.h>
// #include <stdio.h>
// #include <clipbrd.hpp>
#pragma hdrstop
#include "dsICStoreDMUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TdsICStoreDM * dsICStoreDM;
// ---------------------------------------------------------------------------
__fastcall TdsICStoreDM::TdsICStoreDM(TComponent * Owner) : TDataModule(Owner)
 {
  FClassDefNode = NULL;
  // FRegDefNode = NULL;
  FClassEditable = true;
  // FFullEdit = true;
  FRegComp           = NULL;
  FClassShowExpanded = "";
  FRequiredColor     = clInfoBk;
  FULEditWidth       = 700;
  FXMLPath           = icsPrePath(GetEnvironmentVariable("APPDATA") + "\\ic\\xml");
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreDM::DataModuleCreate(TObject * Sender)
 {
  UnitList     = NULL;
  SelRegUnit   = NULL;
  DTFormats[0] = FMT(icsDTFormatsD);
  DTFormats[1] = FMT(icsDTFormatsDT);
  DTFormats[2] = FMT(icsDTFormatsT);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreDM::DataModuleDestroy(TObject * Sender)
 {
  /*
   delete FClassData;
   if (SelRegUnit)   delete SelRegUnit;
   SelRegUnit = NULL;
   if (REG_DB->Connected) REG_DB->Close();
   if (SearchList) delete[] SearchList;
   SearchList = NULL;
   */
 }
// ---------------------------------------------------------------------------
// #include <rpc.h>
UnicodeString __fastcall TdsICStoreDM::NewGUID()
 {
  return icsNewGUID().UpperCase();
 }
// ---------------------------------------------------------------------------
// TTagNode* __fastcall TdsRegDM::FGetRegDefNode()
// {
// TTagNode *RC = FRegDefNode;
// return RC;
// }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsICStoreDM::FGetClassDef()
 {
  TTagNode * RC = FClassDefNode;
  try
   {
    if (!RC)
     {
      FClassDefNode = RegDef->GetChildByName("classes");
      RC            = FClassDefNode;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TdsICStoreDM::kabCDSGetCount(UnicodeString AId, UnicodeString ATTH, UnicodeString AFilterParam)
 {
  __int64 RC = 0;
  try
   {
    if (FOnGetCount)
     RC = FOnGetCount(AId, AFilterParam);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsICStoreDM::kabCDSGetCodes(UnicodeString AId, int AMax, UnicodeString ATTH,
  UnicodeString AFilterParam)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetIdList)
     FOnGetIdList(AId, AMax, AFilterParam, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsICStoreDM::kabCDSGetValById(UnicodeString AId, UnicodeString ARecId)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetValById)
     FOnGetValById(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsICStoreDM::kabCDSGetValById10(UnicodeString AId, UnicodeString ARecId)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetValById10)
     FOnGetValById10(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICStoreDM::kabCDSInsert(UnicodeString AId, TJSONObject * AValues, UnicodeString & ARecId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnInsertData)
     FOnInsertData(AId, AValues, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICStoreDM::kabCDSEdit(UnicodeString AId, TJSONObject * AValues, UnicodeString & ARecId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnEditData)
     FOnEditData(AId, AValues, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICStoreDM::kabCDSDelete(UnicodeString AId, UnicodeString ARecId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnDeleteData)
     FOnDeleteData(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICStoreDM::kabCDSGetClassXML(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetClassXML)
     FOnGetClassXML(AId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICStoreDM::kabCDSGetMIBPForm()
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetMIBPForm)
     FOnGetMIBPForm(RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreDM::OnGetXML(UnicodeString AId, TTagNode *& ARC)
 {
  try
   {
    if (!FXMLList->XMLExist(AId))
     {
      TTagNode * tmp = new TTagNode;
      try
       {
        UnicodeString FFN = icsPrePath(FXMLPath + "\\" + AId + ".xml");
        if (!FileExists(FFN))
         {
          ForceDirectories(FXMLPath);
          tmp->AsXML = kabCDSGetClassXML(AId);
          tmp->SaveToXMLFile(FFN, "");
         }
        FXMLList->Load(FFN, AId);
       }
      __finally
       {
        delete tmp;
       }
     }
    ARC = FXMLList->GetXML(AId);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreDM::WriteOff(int AMIBPCode, UnicodeString ASer, int ADoseCount, int ADosePerPrivCount)
 {
  try
   {
    if (FOnWriteOff)
     FOnWriteOff(AMIBPCode, ASer, ADoseCount, ADosePerPrivCount);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
