//---------------------------------------------------------------------------
#ifndef dsICStoreTypeDefH
#define dsICStoreTypeDefH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
//#include "XMLContainer.h"
#include "AxeUtil.h"
#include "KabCustomDS.h"
#include "dsRegExtFilterSetTypes.h"
#include "dsRegTypeDef.h"

//---------------------------------------------------------------------------
typedef bool __fastcall (__closure *TdsGetMIBPFormEvent)(System::UnicodeString &RC);
typedef void __fastcall (__closure *TdsStoreWriteOff)(int AMIBPCode, UnicodeString  ASer, int ADoseCount, int ADosePerPrivCount);

//---------------------------------------------------------------------------
#endif

