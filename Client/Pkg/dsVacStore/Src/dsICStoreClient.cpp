// ---------------------------------------------------------------------------
#include <basepch.h>
#pragma hdrstop
#include "dsICStoreClient.h"
// #include "DxLocale.h"
#include "dsICStoreDMUnit.h"
// #include "ICSRegList.h"
// #include "ICSRegSelList.h"
// #include "Reg_DMF.h"
// #include "ICSRegProgress.h"
#pragma package(smart_init)
// ---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//
static inline void ValidCtrCheck(TdsICStoreClient *)
 {
  new TdsICStoreClient(NULL);
 }
// ---------------------------------------------------------------------------
__fastcall TdsICStoreClient::TdsICStoreClient(TComponent * Owner) : TComponent(Owner)
 {
  FUseClassDisControls = false;
  FUseReportSetting    = false;
  FDBUser              = "";
  FDBPassword          = "";
  FDBCharset           = "";
  RegistryKey          = "Software\\ICS Ltd.\\ClassifSize";
  FUnitListCreated     = NULL;
  FBuyCaption          = "";
  // FLargeIcons = false;
  // FFetchAll = true;
  FUnitListDeleted  = NULL;
  FOnExtFilterSet   = NULL;
  FOnCtrlDataChange = NULL;
  FRefreshList      = false;
  FClassTreeColor   = clBtnFace;
  FUpperFunc        = "Upper";
  FDTFormat         = "dd.mm.yyyy";
  FDM               = new TdsICStoreDM(this);
  FActive           = true;
  FClassEnabled     = true;
 }
// ---------------------------------------------------------------------------
__fastcall TdsICStoreClient::~TdsICStoreClient()
 {
  for (TICStoreMap::iterator i = FStoreMap.begin(); i != FStoreMap.end(); i++)
   delete i->second;
  FStoreMap.clear();
  Active = false;
  delete FDM;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetFullEdit(bool AFullEdit)
 {
  FFullEdit = AFullEdit;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetActive(bool AActive)
 {
  FActive = AActive;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetClassEnabled(bool AClassEnabled)
 {
  /*
   FClassEnabled = AClassEnabled;
   if (FDM)
   if (FDM->UnitList) ((TICSRegListForm*)FDM->UnitList)->CheckAccess();
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetListEditEnabled(bool AListEditEnabled)
 {
  /*
   FListEditEnabled = AListEditEnabled;
   if (FDM)
   if (FDM->UnitList) ((TICSRegListForm*)FDM->UnitList)->CheckAccess();
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOpenCardEnabled(bool AOpenCardEnabled)
 {
  /*
   FOpenCardEnabled = AOpenCardEnabled;
   if (FDM)
   if (FDM->UnitList) ((TICSRegListForm*)FDM->UnitList)->CheckAccess();
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetUListTop(int ATop)
 {
  FUListTop = ATop;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetUListLeft(int ALeft)
 {
  FUListLeft = ALeft;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetDBName(UnicodeString ADBName)
 {
  FDBName = ADBName;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetXMLName(UnicodeString AXMLName)
 {
  FXMLName = AXMLName;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::ShowStore()
 {
  if (!FActive)
   return;
  TICStoreMap::iterator FRC = FStoreMap.find("store");
  if (FRC == FStoreMap.end())
   {
    FStoreMap["store"] = new TdsStoreClientForm(Application->Owner, FDM);
   }
  FStoreMap["store"]->Show();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetInqComp(TControl * AInqComp)
 {
  FInqComp = AInqComp;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsICStoreClient::GetSelected(__int64 * ACode, UnicodeString & AUnitStr)
 {
  /*
   if (!IsListShowing()) return false;
   if (FDM->UnitList)
   {
   __int64 XXCode = ((TICSRegListForm*)FDM->UnitList)->GetSelCode();
   if (XXCode == -1) return false;
   *ACode   = XXCode;
   AUnitStr = ((TICSRegListForm*)FDM->UnitList)->GetSelStr();
   return true;
   }
   return false;
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::GetSelectedUnits(TStrings * ADest)
 {
  /*
   if (!IsListShowing()) return;
   if (FDM->UnitList)
   ((TICSRegListForm*)FDM->UnitList)->GetSelectedUnits(ADest);
   */
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsICStoreClient::Connect(UnicodeString ADBName, UnicodeString AXMLName)
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::GetTreeChoice(TList * AList)
 {
  tmpChTree = new TList;
  FDM->RegDef->Iterate(GetTreeCh, *& UnicodeString(""));
  AList->Assign(tmpChTree, laCopy, NULL);
  if (tmpChTree)
   delete tmpChTree;
  tmpChTree = NULL;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsICStoreClient::GetTreeCh(TTagNode * itTag, UnicodeString & UID)
 {
  if (itTag->CmpName("choiceTree"))
   tmpChTree->Add(itTag);
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetInit(int APersent, UnicodeString AMess)
 {
  /*
   if (APersent == 0) ((TICSRegProgressForm*)PrF)->SetCaption(AMess);
   ((TICSRegProgressForm*)PrF)->SetPercent(APersent);
   */
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICStoreClient::GetUnitTabName()
 {
  if (!FActive)
   return "";
  if (!FDM->RegDef)
   return "";
  if (!FDM->RegDef->GetChildByName("unit"))
   return "";
  return (TablePrefix + FDM->RegDef->GetChildByName("unit")->AV["tblname"]).UpperCase();
 }
// ---------------------------------------------------------------------------
TdxBar * __fastcall TdsICStoreClient::GetListExtTB(void)
 {
  /*
   if (FDM->UnitList) return ((TICSRegListForm*)FDM->UnitList)->GetExtTB();
   else              return NULL;
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetUpperFunc(UnicodeString AParam)
 {
  FUpperFunc = AParam;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetDTFormat(UnicodeString AParam)
 {
  FDTFormat = AParam;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsICStoreClient::FGetClassEditable()
 {
  bool RC = false;
  try
   {
    if (FDM)
     RC = FDM->ClassEditable;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetClassEditable(bool AVal)
 {
  if (FDM)
   FDM->ClassEditable = AVal;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICStoreClient::FGetClassShowExpanded()
 {
  UnicodeString RC = "";
  try
   {
    if (FDM)
     RC = FDM->ClassShowExpanded;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetClassShowExpanded(UnicodeString AVal)
 {
  if (FDM)
   FDM->ClassShowExpanded = AVal;
 }
// ---------------------------------------------------------------------------
TExtEditData __fastcall TdsICStoreClient::FGetExtInsertData()
 {
  if (FDM)
   return FDM->OnExtInsertData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetExtInsertData(TExtEditData AValue)
 {
  if (FDM)
   FDM->OnExtInsertData = AValue;
 }
// ---------------------------------------------------------------------------
TExtEditData __fastcall TdsICStoreClient::FGetExtEditData()
 {
  if (FDM)
   return FDM->OnExtEditData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetExtEditData(TExtEditData AValue)
 {
  if (FDM)
   FDM->OnExtEditData = AValue;
 }
// ---------------------------------------------------------------------------
TClassEditEvent __fastcall TdsICStoreClient::FGetClassAfterInsert()
 {
  if (FDM)
   return FDM->OnClassAfterInsert;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetClassAfterInsert(TClassEditEvent AValue)
 {
  if (FDM)
   FDM->OnClassAfterInsert = AValue;
 }
// ---------------------------------------------------------------------------
TClassEditEvent __fastcall TdsICStoreClient::FGetClassAfterEdit()
 {
  if (FDM)
   return FDM->OnClassAfterEdit;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetClassAfterEdit(TClassEditEvent AValue)
 {
  if (FDM)
   FDM->OnClassAfterEdit = AValue;
 }
// ---------------------------------------------------------------------------
TClassEditEvent __fastcall TdsICStoreClient::FGetClassAfterDelete()
 {
  if (FDM)
   return FDM->OnClassAfterDelete;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetClassAfterDelete(TClassEditEvent AValue)
 {
  if (FDM)
   FDM->OnClassAfterDelete = AValue;
 }
// ---------------------------------------------------------------------------
TUnitEvent __fastcall TdsICStoreClient::FGetOnOpenCard()
 {
  if (FDM)
   return FDM->OnOpenCard;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOnOpenCard(TUnitEvent AValue)
 {
  if (FDM)
   FDM->OnOpenCard = AValue;
 }
// ---------------------------------------------------------------------------
TGetReportListEvent __fastcall TdsICStoreClient::FGetQuickFilterList()
 {
  if (FDM)
   return FDM->OnQuickFilterList;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetQuickFilterList(TGetReportListEvent AValue)
 {
  if (FDM)
   FDM->OnQuickFilterList = AValue;
 }
// ---------------------------------------------------------------------------
TGetReportListEvent __fastcall TdsICStoreClient::FGetReportList()
 {
  if (FDM)
   return FDM->OnReportList;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetReportList(TGetReportListEvent AValue)
 {
  if (FDM)
   FDM->OnReportList = AValue;
 }
// ---------------------------------------------------------------------------
TdsRegReportClickEvent __fastcall TdsICStoreClient::FGetPrintReportClick()
 {
  if (FDM)
   return FDM->OnPrintReportClick;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetPrintReportClick(TdsRegReportClickEvent AValue)
 {
  if (FDM)
   FDM->OnPrintReportClick = AValue;
 }
// ---------------------------------------------------------------------------
// TdsICClassClient* __fastcall TdsICStoreClient::FGetConnection()
// {
// if (FDM)
// return FDM->Connection;
// else
// return NULL;
// }
// ---------------------------------------------------------------------------
// void __fastcall TdsICStoreClient::FSetConnection(TdsICClassClient* AValue)
// {
// if (FDM)
// FDM->Connection = AValue;
// }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsICStoreClient::FGetRegDef()
 {
  if (FDM)
   return FDM->RegDef;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetRegDef(TTagNode * AValue)
 {
  if (FDM)
   FDM->RegDef = AValue;
 }
// ---------------------------------------------------------------------------
TExtBtnClick __fastcall TdsICStoreClient::FGetExtBtnClick()
 {
  if (FDM)
   return FDM->OnExtBtnClick;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetExtBtnClick(TExtBtnClick AValue)
 {
  if (FDM)
   FDM->OnExtBtnClick = AValue;
 }
// ---------------------------------------------------------------------------
TcxEditStyleController * __fastcall TdsICStoreClient::FGetStyleController()
 {
  if (FDM)
   return FDM->StyleController;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetStyleController(TcxEditStyleController * AValue)
 {
  if (FDM)
   FDM->StyleController = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetClassXMLEvent __fastcall TdsICStoreClient::FGetOnGetClassXML()
 {
  if (FDM)
   return FDM->OnGetClassXML;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOnGetClassXML(TdsGetClassXMLEvent AValue)
 {
  if (FDM)
   FDM->OnGetClassXML = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetCountEvent __fastcall TdsICStoreClient::FGetOnGetCount()
 {
  if (FDM)
   return FDM->OnGetCount;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOnGetCount(TdsGetCountEvent AValue)
 {
  if (FDM)
   FDM->OnGetCount = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetIdListEvent __fastcall TdsICStoreClient::FGetOnGetIdList()
 {
  if (FDM)
   return FDM->OnGetIdList;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOnGetIdList(TdsGetIdListEvent AValue)
 {
  if (FDM)
   FDM->OnGetIdList = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetValByIdEvent __fastcall TdsICStoreClient::FGetOnGetValById()
 {
  if (FDM)
   return FDM->OnGetValById;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOnGetValById(TdsGetValByIdEvent AValue)
 {
  if (FDM)
   FDM->OnGetValById = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetValById10Event __fastcall TdsICStoreClient::FGetOnGetValById10()
 {
  if (FDM)
   return FDM->OnGetValById10;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOnGetValById10(TdsGetValById10Event AValue)
 {
  if (FDM)
   FDM->OnGetValById10 = AValue;
 }
// ---------------------------------------------------------------------------
TdsFindEvent __fastcall TdsICStoreClient::FGetOnFind()
 {
  if (FDM)
   return FDM->OnFind;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOnFind(TdsFindEvent AValue)
 {
  if (FDM)
   FDM->OnFind = AValue;
 }
// ---------------------------------------------------------------------------
TdsInsertDataEvent __fastcall TdsICStoreClient::FGetOnInsertData()
 {
  if (FDM)
   return FDM->OnInsertData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOnInsertData(TdsInsertDataEvent AValue)
 {
  if (FDM)
   FDM->OnInsertData = AValue;
 }
// ---------------------------------------------------------------------------
TdsEditDataEvent __fastcall TdsICStoreClient::FGetOnEditData()
 {
  if (FDM)
   return FDM->OnEditData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOnEditData(TdsEditDataEvent AValue)
 {
  if (FDM)
   FDM->OnEditData = AValue;
 }
// ---------------------------------------------------------------------------
TdsDeleteDataEvent __fastcall TdsICStoreClient::FGetOnDeleteData()
 {
  if (FDM)
   return FDM->OnDeleteData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOnDeleteData(TdsDeleteDataEvent AValue)
 {
  if (FDM)
   FDM->OnDeleteData = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetMIBPFormEvent __fastcall TdsICStoreClient::FGetOnGetMIBPForm()
 {
  if (FDM)
   return FDM->OnGetMIBPForm;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOnGetMIBPForm(TdsGetMIBPFormEvent AValue)
 {
  if (FDM)
   FDM->OnGetMIBPForm = AValue;
 }
// ---------------------------------------------------------------------------
TAxeXMLContainer * __fastcall TdsICStoreClient::FGetXMLList()
 {
  if (FDM)
   return FDM->XMLList;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetXMLList(TAxeXMLContainer * AVal)
 {
  if (FDM)
   FDM->XMLList = AVal;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICStoreClient::FGetXMLPath()
 {
  if (FDM)
   return FDM->XMLPath;
  else
   return "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetXMLPath(UnicodeString AVal)
 {
  if (FDM)
   FDM->XMLPath = AVal;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICStoreClient::FGetDataPath()
 {
  if (FDM)
   return FDM->DataPath;
  else
   return "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetDataPath(UnicodeString AVal)
 {
  if (FDM)
   FDM->DataPath = AVal;
 }
// ---------------------------------------------------------------------------
TdsRegClient * __fastcall TdsICStoreClient::FGetRegComp()
 {
  if (FDM)
   return FDM->RegComp;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetRegComp(TdsRegClient * AVal)
 {
  if (FDM)
   FDM->RegComp = AVal;
 }
// ---------------------------------------------------------------------------
TColor __fastcall TdsICStoreClient::FGetReqColor()
 {
  if (FDM)
   return FDM->RequiredColor;
  else
   return clInfoBk;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetReqColor(TColor AVal)
 {
  if (FDM)
   FDM->RequiredColor = AVal;
 }
// ---------------------------------------------------------------------------
TExtValidateData __fastcall TdsICStoreClient::FGetOnExtValidate()
 {
  if (FDM)
   return FDM->OnExtValidate;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOnExtValidate(TExtValidateData AVal)
 {
  if (FDM)
   FDM->OnExtValidate = AVal;
 }
// ---------------------------------------------------------------------------
TExtValidateData __fastcall TdsICStoreClient::FGetOnGetDefValues()
 {
  if (FDM)
   return FDM->OnGetDefValues;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOnGetDefValues(TExtValidateData AVal)
 {
  if (FDM)
   FDM->OnGetDefValues = AVal;
 }
// ---------------------------------------------------------------------------
TdsStoreWriteOff __fastcall TdsICStoreClient::FGetOnWriteOff()
 {
  if (FDM)
   return FDM->OnWriteOff;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICStoreClient::FSetOnWriteOff(TdsStoreWriteOff AVal)
 {
  if (FDM)
   FDM->OnWriteOff = AVal;
 }
// ---------------------------------------------------------------------------

