//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsStoreEditClientUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma resource "*.dfm"
//#define lComp ((TWinControl*)CList->Items[CList->Count-1])
TdsStoreEditClientForm *dsStoreEditClientForm;
//---------------------------------------------------------------------------
const int LeftMargin = 2;
const int LevelCorrection = 5;
__fastcall TdsStoreEditClientForm::TdsStoreEditClientForm(TComponent* Owner, bool AIsAppend, TTagNode* AClassDef, TdsICStoreDM *ADM, TTagNode* ARoot, TkabCustomDataSource *ADataSrc, TdsRegTemplateData *ATmplData, bool AOneCol)
    : TForm(Owner)
{
  FDM = ADM;
  FDataSrc = ADataSrc;
  FOneCol = AOneCol;
  FInsertedRecId = "";
  OkBtn->Caption = FMT(icsRegClassifEditApplyBtnCaption);
  CancelBtn->Caption = FMT(icsRegClassifEditCancelBtnCaption);
//  HelpContext = RegComp->HelpContextList[AClassDef->AV["uid"]+"_edit"+RegComp->TablePrefix].ToIntDef(0);
  FIsAppend = AIsAppend;
  if (FIsAppend)
   FDataSrc->DataSet->Insert();
  else
   FDataSrc->DataSet->Edit();

  FClassDef = AClassDef;
//  UnitNode = RegComp->DM->RegUnit->GetChildByName("unit");
  FClassDesc = FClassDef->GetChildByName("description");

  FCtrList                = new TdsRegEDContainer(this,CompPanel);
  FCtrList->DefXML        = ARoot;
  FCtrList->ReqColor      = FDM->RequiredColor;
  FCtrList->StyleController = FDM->StyleController;
  FCtrList->TemplateData    = ATmplData;
  FCtrList->DataPath        = FDM->DataPath;

  FCtrList->DataProvider = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider*)FCtrList->DataProvider)->DefXML = ARoot;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  ((TdsRegEDkabDSDataProvider*)FCtrList->DataProvider)->DataSource    = FDataSrc;
  FCtrList->DataProvider->OnGetCompValue       = FDM->OnGetCompValue;

  FCtrList->OnDataChange         = FCtrlDataChange;
  FCtrList->OnGetExtBtnGetBitmap = FDM->OnGetExtBtnGetBitmap;
  FCtrList->OnGetFormat          = FDM->OnGetFormat;
  FCtrList->OnExtBtnClick        = FDM->OnExtBtnClick;
  FCtrList->OnKeyDown            = FormKeyDown;


  ColWidth = FDM->ULEditWidth/2+FOneCol*50;

  int FGroupCount = 0;
  bool FGroupOnly = true;

  TTagNode *itNode = FClassDef->GetFirstChild();
  while (itNode)
   {
     if (!itNode->CmpName("description"))
      {
        if (itNode->CmpName("group"))
         FGroupCount++;
        else
         FGroupOnly = false;
      }
     itNode = itNode->GetNext();
   }


  FLastTop = 0;
  FLastHeight = 0;
  int xTop,xLeft;

  LastLeftCtrl = NULL;
  int MaxH = 0;
  TList *CntrlList = new TList;

  TTagNode *FDefNode = NULL;

  if (FGroupOnly)
   FDefNode = FClassDesc->GetNext();
  else
   FDefNode = FClassDef;
  if ((FGroupCount > 1) && FGroupOnly)
   {
     SubLevel = 1;
     Group1st = new TcxPageControl(this);
     Group1st->Parent = CompPanel;
     Group1st->Align = alClient;
     int xMaxH;
     int PIdx = 0;
     while (FDefNode)
      {
        xTop = 2; xLeft = LeftMargin;
        LastLeftCtrl = NULL;
        gPage = new TcxTabSheet(Group1st);
        gPage->PageControl = Group1st;
        gPage->Caption = FDefNode->AV["name"];
        LastCtrl = NULL;
        CreateUnitList(gPage, FDefNode, &xTop, &xLeft, CntrlList, PIdx);
        PIdx++;
        if (LastLeftCtrl)
         {
          if ((LastLeftCtrl->Top+LastLeftCtrl->Height) <= xTop)
           xMaxH = xTop;
          else
           xMaxH = LastLeftCtrl->Top+LastLeftCtrl->Height;
         }
        else
         xMaxH = xTop;
        if (MaxH < xMaxH) MaxH = xMaxH;
        FDefNode = FDefNode->GetNext();
      }
     ClientHeight = MaxH+80;
     Width = ColWidth+(!FOneCol)*ColWidth+(Width-Group1st->Width)+LeftMargin*4;
   }
  else
   {
     SubLevel = 0;
     xTop = 2; xLeft = LeftMargin;
     LastCtrl = NULL;
     CreateUnitList(CompPanel, FDefNode, &xTop, &xLeft, CntrlList);
     if (LastLeftCtrl)
      {
       if ((LastLeftCtrl->Top+LastLeftCtrl->Height) <= xTop)
        MaxH = xTop;
       else
        MaxH = LastLeftCtrl->Top+LastLeftCtrl->Height;
      }
     else
      MaxH = xTop;
     ClientHeight = MaxH+50;
     Width = ColWidth+(!FOneCol)*ColWidth+(Width-CompPanel->Width)+LeftMargin*2;
   }
  FCtrList->SetOnCtrlDataChange(0);
  FClassDef->GetFirstChild();
  FClassDef->Iterate(FSetFocus,*&UnicodeString(""));
  delete CntrlList; CntrlList = NULL;
  if (FIsAppend)
   {
     bool valid;
     if (FDM->OnGetDefValues)
      FDM->OnGetDefValues(-1,FClassDef,FCtrList,valid);
     Caption = FClassDef->GetChildByName("description")->AV["insertlabel"];
   }
  else
   Caption = FClassDef->GetFirstChild()->AV["editlabel"];
   // ��������� 7.10.08 ��������� �.�.
   // ��������� ������� �� �������� ������ �����������
/*   if ( ARegComp->OnAfterClassEditCreate )
   {
       ARegComp->OnAfterClassEditCreate ( (TForm*)this, FCtrList, AClassDef, RegComp->DM->quReg->Value("CODE")->AsInteger );
   }*/
}
//---------------------------------------------------------------------------
bool __fastcall TdsStoreEditClientForm::FSetFocus(TTagNode *itxTag, UnicodeString& tmp)
{
  TControl *tmpCtrl;
  if (itxTag->GetAttrByName("uid"))
   {
     if (FCtrList->GetEDControl(itxTag->AV["uid"]))
      {
        tmpCtrl = FCtrList->EditItems[itxTag->AV["uid"]]->GetFirstEnabledControl();
        if (tmpCtrl)
         {
           ActiveControl = (TWinControl*)tmpCtrl;
           return true;
         }
      }
   }
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsStoreEditClientForm::CreateUnitList(TWinControl *AParent, TTagNode* ANode, int *ATop, int *ALeft, TList *GroupComp, int APIndex)
{
  TTagNode *itNode = ANode->GetFirstChild();
  while (itNode)
   {
     if (!itNode->CmpName("description"))
      {
        TdsRegEDItem *uCtrl = FCtrList->AddEditItem(itNode, FIsAppend, AParent);
        uCtrl->Tag = APIndex;
        if (itNode->CmpAV("isedit","0")&& !itNode->CmpAV("depend",""))
         {
           uCtrl->Visible = false;
         }
        else
         {
           GroupComp->Add(uCtrl);

           uCtrl->Top = *ATop;
           uCtrl->Left = *ALeft;
           TTagNode *FParentNode = uCtrl->DefNode->GetParent();
           bool ParentFirstLvl = FParentNode->CmpName("class") || (FParentNode->CmpName("group") &&  FParentNode->GetParent()->CmpName("class"));
           if (ParentFirstLvl)
            { // ������� �������� ������ 1-�� ������ ��� ������, ���� ��� �����
              if (!LastLeftCtrl)
               { // ��������� ����� ������� �����������
                 LastLeftCtrl = uCtrl;
                 if (!FOneCol) *ALeft = ColWidth+LeftMargin;        // �������� ����� ������� ������
               }
              else
               { //������� �����-����, ������� ��������
                 if (!FOneCol && ((LastLeftCtrl->Top+LastLeftCtrl->Height) <= *ATop))
                  {
                    if ((!LastCtrl)||LastCtrl&&(LastCtrl->Left < ColWidth))
                     {
                       *ATop = LastLeftCtrl->Top;
                        uCtrl->Top = *ATop;
                     }
                  }
               }
            }
           uCtrl->Width = ColWidth-LeftMargin-(uCtrl->DefNode->Level-SubLevel)*2*LeftMargin;//LevelCorrection;  // ������ �������� "����� �������" - 15
           if (uCtrl->DefNode->CmpName("group"))
            {
              int xTop, xLeft; xTop = 13; xLeft = LeftMargin;
              TList *GrCtrl = new TList;
              try
               {
                 // ��������� ������ ��������� ������
                 CreateUnitList(uCtrl, uCtrl->DefNode, &xTop, &xLeft, GrCtrl, APIndex);
                 uCtrl->Height = xTop+5;
                 *ATop = *ATop+3;   //????
                 AlignCtrls(GrCtrl);
               }
              __finally
               {
                 delete GrCtrl;
               }
            }
           *ATop = *ATop + uCtrl->Height+1;
           if (ParentFirstLvl)
            { // ������� �������� ������ 1-�� ������ ��� ������, ���� ��� �����
              if (LastLeftCtrl && ((uCtrl->Top+uCtrl->Height) > (LastLeftCtrl->Top+LastLeftCtrl->Height)))
               {
                 if (!FOneCol && ((uCtrl->Top+uCtrl->Height) > (LastLeftCtrl->Top+LastLeftCtrl->Height+15)))
                  {
                    uCtrl->Top = LastLeftCtrl->Top+LastLeftCtrl->Height+2;
                    uCtrl->Left = 2;
                    LastLeftCtrl = uCtrl;
                    *ALeft = ColWidth+LeftMargin;
                    *ATop = uCtrl->Top;
                  }
                 else
                  {
                    LastLeftCtrl->Constraints->MaxHeight = uCtrl->Top+uCtrl->Height - LastLeftCtrl->Top;
                    LastLeftCtrl->Height = LastLeftCtrl->Constraints->MaxHeight;
                    *ATop = *ATop+3;
                  }
               }
            }
           LastCtrl = uCtrl;
         }
      }
     itNode = itNode->GetNext();
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsStoreEditClientForm::AlignCtrls(TList *ACtrlList)
{
  int LabR,EDL;
  int xLabR,xEDL;
  LabR = 0; EDL = 9999;
  TdsRegEDItem *tmpED;
  for (int i = 0; i < ACtrlList->Count; i++)
   {
     // ���������� max ������ ������� ����� ���������
     // � min ����� ����� ���������
     tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
     tmpED->GetEDLeft(&xLabR,&xEDL);
     if (xLabR != -1) LabR = max(xLabR,LabR);
     if (xEDL != -1)  EDL  = min(xEDL,EDL);
   }
  if ((LabR+2) < EDL)
   {
     // max ������ ������� ����� ���������
     // � min ����� ����� ��������� �� ������������
     // ����������� �������� �� min ����� ������� ����� ���������
     for (int i = 0; i < ACtrlList->Count; i++)
      {
        tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
        tmpED->SetEDLeft(EDL);
      }
   }
  else
   {
     // ����� ����������� �� "max ������ ������� ����� ���������" + 2
     for (int i = 0; i < ACtrlList->Count; i++)
      {
        tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
        tmpED->SetEDLeft(LabR+2);
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsStoreEditClientForm::FormKeyDown(TObject *Sender, WORD &Key,  TShiftState Shift)
{
  if (Key == VK_RETURN)
   {ActiveControl = OkBtn; OkBtnClick(this);}
}
//---------------------------------------------------------------------------
void __fastcall TdsStoreEditClientForm::OkBtnClick(TObject *Sender)
{
  TWinControl *tmpCtrl = ActiveControl;
  ActiveControl = OkBtn;
  ActiveControl = tmpCtrl;
  if (CheckInput())
   { // ������ �� �������� ������
     try
      {
        UnicodeString PRC = FDataSrc->DataSet->Post(FInsertedRecId, true);
        if (!SameText(PRC,"ok"))
          _MSG_ERR(PRC,"������");
        else
         ModalResult = mrOk;
      }
     catch (EkabCustomDataSetError &E)
      {
        _MSG_ERR(E.Message,"������");
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TdsStoreEditClientForm::CheckInput()
{
  bool RC = false;
  try
   {
     UnicodeString xValid = "";
     if (FIsAppend)
      FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
     else
      FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
     FClassDef->Iterate(GetInput,xValid);
     if (!xValid.Length())
      {
        RC = true;
        if (FDM->OnExtValidate)
         {
           if (FIsAppend)
            FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
           else
            FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsStoreEditClientForm::GetInput(TTagNode *itTag, UnicodeString &UID)
{
   if (itTag)
    {
      if (FCtrList->GetEDControl(itTag->AV["uid"]))
       if (!FCtrList->EditItems[itTag->AV["uid"]]->CheckReqValue())
        {
           UID = "no";
           return true;
        }
    }
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsStoreEditClientForm::FormDestroy(TObject *Sender)
{
   FCtrList->ClearOnCtrlDataChange(0);
   delete FCtrList->DataProvider;
   delete FCtrList;
}
//---------------------------------------------------------------------------
bool __fastcall TdsStoreEditClientForm::FCtrlDataChange(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource)
{
  bool RC = false;
  if (FDM->OnCtrlDataChange)
   {
     RC = FDM->OnCtrlDataChange(ItTag, Src, ASource, FIsAppend, FCtrList, UIDInt(FClassDef->AV["uid"]));
   }
  return RC;
}
//---------------------------------------------------------------------------

