//---------------------------------------------------------------------------
#ifndef dsStoreClientUnitH
#define dsStoreClientUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxData.hpp"
#include "cxDataStorage.hpp"
#include "cxEdit.hpp"
#include "cxFilter.hpp"
#include "cxGraphics.hpp"
#include "cxGrid.hpp"
#include "cxGridCustomTableView.hpp"
#include "cxGridCustomView.hpp"
#include "cxGridLevel.hpp"
#include "cxGridTableView.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxNavigator.hpp"
#include "cxPC.hpp"
#include "cxSplitter.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "dxBar.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxStatusBar.hpp"
#include "Htmlview.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "dsICStoreDMUnit.h"
#include "KabCustomDS.h"
#include "cxButtons.hpp"
#include <Vcl.StdCtrls.hpp>
#include "cxImage.hpp"
#include "dxGDIPlusClasses.hpp"
#include "cxGridInplaceEditForm.hpp"
#include "dxLayoutContainer.hpp"
#include "cxProgressBar.hpp"
//---------------------------------------------------------------------------
class PACKAGE TdsStoreClientForm : public TForm
{
__published:	// IDE-managed Components
        TPopupMenu *PMenu;
        TMenuItem *InsertItem;
        TMenuItem *ModifyItem;
        TMenuItem *DeleteItem;
        TdxBarManager *ClassTBM;
        TdxBarButton *InsertBI;
        TdxBarButton *ResetTemplateBI;
        TdxBarButton *DeleteBI;
        TdxBarButton *EditBI;
 TcxStyleRepository *Style;
        TcxStyle *BGStyle;
        TActionList *ClassAL;
        TAction *actInsert;
        TAction *actEdit;
        TAction *actDelete;
        TdxBarButton *SetFilterBI;
        TAction *actRefresh;
        TdxBarButton *RefreshBI;
        TdxBarButton *FindBI;
        TdxBarButton *FindNextBI;
        TdxBarButton *ClassParamBI;
        TdxBarButton *TemplateParamBI;
        TcxStyle *cxStyle2;
        TdxBarButton *dxBarButton1;
 TAction *actCardSelect;
        TAction *actExit;
        TdxBarButton *SetTemplateBI;
  TdxBarLargeButton *dxBarLargeButton1;
  TdxBarLargeButton *dxBarLargeButton2;
  TdxBarLargeButton *dxBarLargeButton3;
  TdxBarLargeButton *dxBarLargeButton4;
  TdxBarLargeButton *dxBarLargeButton5;
  TdxBarLargeButton *dxBarLargeButton6;
  TdxBarLargeButton *dxBarLargeButton7;
  TdxBarLargeButton *dxBarLargeButton8;
  TdxBarLargeButton *dxBarLargeButton9;
  TdxBarLargeButton *dxBarLargeButton10;
 TcxImageList *LClassIL32;
 TdxBarPopupMenu *QuickFilterMenu;
 TcxTabControl *CardTypePC;
 TcxImageList *CardIL;
 TcxPageControl *CardViewPC;
 TcxTabSheet *F63TS;
 TcxTabSheet *ComTS;
 TPanel *CompPanel;
 TcxPageControl *ViewParamPC;
 TcxTabSheet *ClassParamTS;
 TcxPageControl *ExtListPC;
 TcxSplitter *Split;
 TcxGrid *ClassGrid;
 TcxGridTableView *ClassGridView;
 TcxGridLevel *ClassGridLevel1;
 TdxBarDockControl *dxBarDockControl1;
 TPanel *Panel1;
 THTMLViewer *F63Viewer;
 TcxStyle *cxStyle1;
 TcxTabSheet *SettingTS;
 TcxButton *CoolListBtn;
 TcxButton *PostListBtn;
 TAction *actGetCoolList;
 TAction *actGetPostList;
 TcxButton *cxButton1;
 TAction *actGetPointList;
 TcxImage *ProgressPanel;
 TAction *actGroupCancellation;
 TdxBarLargeButton *dxBarLargeButton11;
 TAction *actPrintJornal;
 TdxBarLargeButton *dxBarLargeButton12;
 TdxStatusBar *StatusSB;
 TdxStatusBarContainerControl *StatusSBContainer0;
 TcxProgressBar *PrBar;
 TcxButton *cxButton2;
 TAction *actOutSrcList;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall actInsertExecute(TObject *Sender);
        void __fastcall actEditExecute(TObject *Sender);
        void __fastcall actDeleteExecute(TObject *Sender);
        void __fastcall actRefreshExecute(TObject *Sender);
        void __fastcall actCardSelectExecute(TObject *Sender);
        void __fastcall actExitExecute(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
 void __fastcall ClassGridViewFocusedRecordChanged(TcxCustomGridTableView *Sender,
          TcxCustomGridRecord *APrevFocusedRecord, TcxCustomGridRecord *AFocusedRecord,
          bool ANewItemRecordFocusingChanged);
 void __fastcall CardTypePCChange(TObject *Sender);
 void __fastcall actGetCoolListExecute(TObject *Sender);
 void __fastcall actGetPostListExecute(TObject *Sender);
 void __fastcall actGetPointListExecute(TObject *Sender);
 void __fastcall actGroupCancellationExecute(TObject *Sender);
 void __fastcall actPrintJornalExecute(TObject *Sender);
 void __fastcall actOutSrcListExecute(TObject *Sender);
private:	// User declarations
  TdsICStoreDM *FDM;
  TkabCustomDataSource *FDataSrc;
  int FExtPageCount;

  UnicodeString _UID_;
  TcxTabSheet *FCurPage;


        bool SaveEnInsert;
        bool SaveEnEdit;
        bool SaveEnDelete;
        bool SaveEnSetTemplate;
        bool SaveEnReSetTemplate;
        bool SaveEnRefresh;
        bool SaveEnFind;
        bool SaveEnFindNext;
        bool SaveEnViewClassParam;
        bool SaveEnViewTemplateParam;
    __int64    curCD,xpCount,xcTop;
    int     chBDInd;
//    TList   *CompList;
//    UnicodeString   SaveClassCapt;
//    UnicodeString   cCapt;
    TTagNode     *CardRoot;
//    TTreeNode    *TreeClassRoot;
    TcxTreeListNode *FRootNode;
//    TTagNode     *CallRootClass;
//    TAnsiStrMap  FGroups;
    TTagNode     *FSelectedNode, *FSelectedNodeDescr;
    TdsRegEDContainer *FCtrList;
    int FLastTop,FLastHeight;
    UnicodeString    SaveCaption;
  bool InProgress;


  void __fastcall FGetDataProgress(int AMax, int ACur, UnicodeString AMsg);
  void __fastcall GetData();
  void __fastcall RefreshById(UnicodeString ACode);
    bool __fastcall FSetEdit(TTagNode *itTag, UnicodeString &UID);
    void __fastcall DisableAfterScroll();
    void __fastcall EnableAfterScroll();
    TTagNode* __fastcall _CLASSNODE_();
  TTagNode * __fastcall _CLASSDESCR_();
    bool __fastcall GetBtnEn(UnicodeString AAttrName);
    void __fastcall ChangeSrc();
    void __fastcall CreateCardPages();
    bool __fastcall CreateClassTV(TTagNode *itTag, UnicodeString &UID);
    bool __fastcall CreateListExt(TTagNode *itTag);
    void __fastcall ListAfterScroll(TDataSet *DataSet);
    TcxGridColumn* __fastcall ClassCol(int AIdx);
  void __fastcall CheckCtrlState(bool AEnable);
  bool __fastcall IsRecSelected();
  bool __fastcall CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg);
public:		// User declarations
    __fastcall TdsStoreClientForm(TComponent* Owner, TdsICStoreDM *ADM);
    void __fastcall SetActiveCard(UnicodeString ACardId);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsStoreClientForm *dsStoreClientForm;
//---------------------------------------------------------------------------
#endif
