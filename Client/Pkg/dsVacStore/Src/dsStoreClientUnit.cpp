// ---------------------------------------------------------------------------
#include <vcl.h>
#include <SysUtils.hpp>
#pragma hdrstop
#include "dsStoreClientUnit.h"
#include "dsStoreEditClientUnit.h"
#include "dsStorIncomeEditUnit.h"
#include "dsStorOutcomeEditUnit.h"
#include "dsStorCancelEditUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "dsGroupCancellationUnit.h"
// #include "pkgsetting.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxData"
#pragma link "cxDataStorage"
#pragma link "cxEdit"
#pragma link "cxFilter"
#pragma link "cxGraphics"
#pragma link "cxGrid"
#pragma link "cxGridCustomTableView"
#pragma link "cxGridCustomView"
#pragma link "cxGridLevel"
#pragma link "cxGridTableView"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxNavigator"
#pragma link "cxPC"
#pragma link "cxSplitter"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "dxBar"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxStatusBar"
#pragma link "Htmlview"
#pragma link "cxButtons"
#pragma link "cxImage"
#pragma link "dxGDIPlusClasses"
#pragma link "cxGridInplaceEditForm"
#pragma link "dxLayoutContainer"
#pragma link "cxProgressBar"
#pragma resource "*.dfm"
TdsStoreClientForm * dsStoreClientForm;
// ---------------------------------------------------------------------------
__fastcall TdsStoreClientForm::TdsStoreClientForm(TComponent * Owner, TdsICStoreDM * ADM) : TForm(Owner)
 {
  FDM                     = ADM;
  SaveEnInsert            = false;
  SaveEnEdit              = false;
  SaveEnDelete            = false;
  SaveEnSetTemplate       = false;
  SaveEnReSetTemplate     = false;
  SaveEnRefresh           = false;
  SaveEnFind              = false;
  SaveEnFindNext          = false;
  SaveEnViewClassParam    = false;
  SaveEnViewTemplateParam = false;
  FDataSrc                = NULL;
  FExtPageCount           = 0;
  // FSettingNode = NULL;
  _UID_ = "";
  /* actInsert->Caption = FMT(icsRegClassifInsertCaption);
   actInsert->Hint = FMT(icsRegClassifInsertHint);
   if (FMT(icsRegClassifInsertShortCut).Length())
   actInsert->ShortCut = TextToShortCut(FMT(icsRegClassifInsertShortCut));

   actEdit->Caption = FMT(icsRegClassifEditCaption);
   actEdit->Hint = FMT(icsRegClassifEditHint);
   if (FMT(icsRegClassifEditShortCut).Length())
   actEdit->ShortCut = TextToShortCut(FMT(icsRegClassifEditShortCut));

   actDelete->Caption = FMT(icsRegClassifDeleteCaption);
   actDelete->Hint = FMT(icsRegClassifDeleteHint);
   if (FMT(icsRegClassifDeleteShortCut).Length())
   actDelete->ShortCut = TextToShortCut(FMT(icsRegClassifDeleteShortCut));

   //  actSetTemplate->Caption = FMT(icsRegClassifSetTemplateCaption);
   //  actSetTemplate->Hint = FMT(icsRegClassifSetTemplateHint);
   //  if (FMT(icsRegClassifSetTemplateShortCut).Length())
   //   actSetTemplate->ShortCut = TextToShortCut(FMT(icsRegClassifSetTemplateShortCut));

   //  actReSetTemplate->Caption = FMT(icsRegClassifReSetTemplateCaption);
   //  actReSetTemplate->Hint = FMT(icsRegClassifReSetTemplateHint);
   //  if (FMT(icsRegClassifReSetTemplateShortCut).Length())
   //   actReSetTemplate->ShortCut = TextToShortCut(FMT(icsRegClassifReSetTemplateShortCut));

   actRefresh->Caption = FMT(icsRegClassifRefreshCaption);
   actRefresh->Hint = FMT(icsRegClassifRefreshHint);
   if (FMT(icsRegClassifRefreshShortCut).Length())
   actRefresh->ShortCut = TextToShortCut(FMT(icsRegClassifRefreshShortCut));

   if (FMT(icsRegClassifFindShortCut).Length())

   if (FMT(icsRegClassifFindNextShortCut).Length())

   //  actViewClassParam->Caption = FMT(icsRegClassifViewClassParamCaption);
   //  actViewClassParam->Hint = FMT(icsRegClassifViewClassParamHint);
   //  if (FMT(icsRegClassifViewClassParamShortCut).Length())
   //   actViewClassParam->ShortCut = TextToShortCut(FMT(icsRegClassifViewClassParamShortCut));

   //  actViewTemplateParam->Caption = FMT(icsRegClassifViewTemplateParamCaption);
   //  actViewTemplateParam->Hint = FMT(icsRegClassifViewTemplateParamHint);
   //  if (FMT(icsRegClassifViewTemplateParamShortCut).Length())
   //   actViewTemplateParam->ShortCut = TextToShortCut(FMT(icsRegClassifViewTemplateParamShortCut)); */
  /* StatusSB->Panels->Items[0]->Text = FMT(icsCommonRecCount); */
  ViewParamPC->ActivePage = ClassParamTS;
  // FGroups["card"] = "1";
  // FGroups["0"] = "1";
  BGStyle->Color = clBtnShadow;
  if (FDM->DefaultClassWidth)
   Width = FDM->DefaultClassWidth;
  CardRoot                  = FDM->ClassDef->GetChildByAV("classform", "group", "storage", true);
  FCtrList                  = new TdsRegEDContainer(this, ExtListPC);
  FCtrList->DefXML          = FDM->ClassDef;
  FCtrList->StyleController = FDM->StyleController;
  FCtrList->DataProvider    = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DefXML = FDM->ClassDef;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  FCtrList->DataPath                    = FDM->DataPath;
  FLastTop                              = 0;
  FLastHeight                           = 0;
  // SaveCaption = FDM->PrefixCaption.Trim();
  CreateCardPages();
  curCD = 0;
  // ������� ������ ������
  // ClassTBM->Images = FDM->Images;
  // ClassTBM->LargeImages = FDM->LargeImages;
  // ClassTBM->HotImages = FDM->HotImages;
  // ClassTBM->DisabledImages = FDM->DisabledImages;
  // ClassTBM->LargeIcons = FDM->UseLargeImages;
  // ClassAL->Images = FDM->Images;
  // PMenu->Images = FDM->Images;
  int AllItemsCount = 0;
  for (int i = 0; i < ClassTBM->Bars->Count; i++)
   {
    AllItemsCount += ClassTBM->Bars->Items[i]->ItemLinks->Count;
   }
  int FCalcWidth = 0;
  // if (FDM->UseLargeImages)
  // FCalcWidth = (AllItemsCount+1) * ClassTBM->LargeImages->Width;
  // else
  // FCalcWidth = (AllItemsCount+1) * ClassTBM->Images->Width;
  // if (FCalcWidth > Width) Width = FCalcWidth;
  /* if (ClassTL->Count == 0)
   {
   actInsert->Enabled        = false;
   actEdit->Enabled          = false;
   actDelete->Enabled        = false;
   actRefresh->Enabled       = false;
   Caption = SaveCaption;
   //     if (FDM->UseClassDisControls)
   //      quClass()->DisableControls();
   DisableAfterScroll();
   //     quClass()->Fields->Clear();
   //     quClass()->Close();
   FCtrList->ClearLabItems();
   return;
   } */
  /* int ClassCounter = 0;
   for (int i = 0; i < ClassTL->Count; i++)
   {
   if (ClassTL->Items[i]->Level == 1)
   { */
  /* if (ClassCounter == AClassNum)
   {
   if (i < ClassTL->Count)
   {
   ClassTL->Items[i]->Focused = true;
   curCD = 0;
   FSelectedNode = (TTagNode*)ClassTL->Items[i]->Data;
   ChangeSrc();
   ActiveControl = ClassGrid;
   }
   break;
   } */
  /* ClassCounter++;
   }
   } */
  if (FDM->FullEdit)
   {
    // CardRoot->Iterate(FSetEdit,*&UnicodeString(""));
   }
  // if (FDM->FetchAll)
  // quClass()->Options << poFetchAll;
  // else
  // quClass()->Options >> poFetchAll;
  CardTypePC->TabIndex = 0;
  CardTypePCChange(CardTypePC);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsStoreClientForm::FSetEdit(TTagNode * itTag, UnicodeString & UID)
 {
  if (itTag->GetAttrByName("isedit"))
   itTag->AV["isedit"] = "1";
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::SetActiveCard(UnicodeString ACardId)
 {
  if (ACardId.Length())
   {
    UnicodeString FCardUID;
    for (int i = 0; i < CardTypePC->Properties->Tabs->Count; i++)
     {
      if (CardTypePC->Properties->Tabs->Objects[i])
       {
        FCardUID = UIDStr((int)CardTypePC->Properties->Tabs->Objects[i]).UpperCase();
        if (FCardUID == ACardId.UpperCase())
         {
          FSelectedNode        = CardRoot->GetTagByUID(FCardUID);
          CardTypePC->TabIndex = i;
          break;
         }
       }
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::ChangeSrc()
 {
  CheckCtrlState(false);
  Application->ProcessMessages();
  try
   {
    try
     {
      Caption = SaveCaption + " " + _CLASSNODE_()->AV["name"];
      FCtrList->ClearLabItems();
      bool CanFind = false;
      TTagNode * UnitNode = _CLASSDESCR_()->GetFirstChild()->GetFirstChild();
      while (UnitNode)
       {
        if (UnitNode->GetTagByUID(UnitNode->AV["ref"]))
         {
          if (UnitNode->GetTagByUID(UnitNode->AV["ref"])->CmpName("text,digit"))
           {
            CanFind = true;
            break;
           }
         }
        UnitNode = UnitNode->GetNext();
       }
      ClassGridView->DataController->CustomDataSource = NULL;
      if (FDataSrc)
       delete FDataSrc;
      FDataSrc                          = new TkabCustomDataSource(_CLASSNODE_());
      FDataSrc->DataSet->OnGetCount     = FDM->kabCDSGetCount;
      FDataSrc->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
      FDataSrc->DataSet->OnGetValById   = FDM->kabCDSGetValById;
      FDataSrc->DataSet->OnGetValById10 = FDM->kabCDSGetValById10;
      FDataSrc->DataSet->OnInsert       = FDM->kabCDSInsert;
      FDataSrc->DataSet->OnEdit         = FDM->kabCDSEdit;
      FDataSrc->DataSet->OnDelete       = FDM->kabCDSDelete;
      ClassGridView->BeginUpdate();
      try
       {
        GetData();
        FDataSrc->CreateColumns(ClassGridView);
        ClassGridView->DataController->CustomDataSource = FDataSrc;
        FDataSrc->DataChanged();
       }
      __finally
       {
        ClassGridView->EndUpdate();
       }
      if (curCD)
       {
        TLocateOptions opt;
        // if(!quClass()->Locate("CODE",Variant(curCD),opt))
        // quClass()->First();
       }
      // else
      // quClass()->First();
      ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DataSource = FDataSrc;
      xcTop = 1;
      int FPC = ExtListPC->PageCount - 1;
      for (int i = FPC; i >= 0; i--)
       {
        FCurPage              = ExtListPC->Pages[i];
        FCurPage->PageControl = NULL;
        delete FCurPage;
       }
      _CLASSNODE_()->Iterate(CreateListExt);
      if (!FCtrList->LabCount)
       Split->CloseSplitter();
      else
       Split->OpenSplitter();
      StatusSB->Panels->Items[1]->Text = CorrectCount(FDataSrc->DataSet->RecordCount);
      ActiveControl                    = ClassGrid;
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsErrorMsgCaptionSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegistryErrorCommDBErrorMsgCaption), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
    CheckCtrlState(true);
    Application->ProcessMessages();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::FGetDataProgress(int AMax, int ACur, UnicodeString AMsg)
 {
  if (AMax == -1)
   StatusSB->Panels->Items[2]->Text = "";
  else if (!(AMax + ACur))
   StatusSB->Panels->Items[2]->Text = AMsg;
  else
   StatusSB->Panels->Items[2]->Text = AMsg + " [" + IntToStr(ACur) + "/" + IntToStr(AMax) + "]";
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_INSERT && actInsert->Enabled)
   actInsert->OnExecute(this);
  if (Key == VK_RETURN && actEdit->Enabled && Shift.Contains(ssShift))
   actEdit->OnExecute(this);
  if (Key == VK_DELETE && actDelete->Enabled)
   actDelete->OnExecute(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::CreateCardPages()
 {
  CardTypePC->Properties->Tabs->Clear();
  /*

   ������ �������' uid='111F' ref='storage' inlist='l'>
   ������ �������' uid='1120' ref='storage' inlist='l'>
   ������ ������� ���� (�����)' uid='1121' ref='storage' inlist='l'>
   ������ ������� ���� (�����)' uid='1122' ref='storage' inlist='l'>
   ������ �������� t�c' uid='1123' ref='storage' inlist='l'>
   ������ ��������' uid='1124' ref='storage' inlist='l'>
   */
  TTagNode * itNode = CardRoot->GetFirstChild();
  while (itNode)
   {
    if (itNode->CmpAV("inlist", "l"))
     {
      CardTypePC->Properties->Tabs->AddObject(itNode->AV["name"], (TObject *)UIDInt(itNode->AV["uid"]));
     }
    itNode = itNode->GetNext();
   }
  CardTypePC->Properties->Tabs->AddObject("���������", (TObject *)0);
  // CardTypePC->Properties->Tabs->AddObject("������� �����",(TObject*)0);
  FExtPageCount = 1;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsStoreClientForm::CreateClassTV(TTagNode * itTag, UnicodeString & UID)
 {
  TcxTreeListNode * FNode;
  if (itTag->CmpName("class"))
   {
    UnicodeString InList = itTag->GetAVDef("inlist", "n").LowerCase();
    /*
     if (!FDM->FullEdit)
     {
     break;
     }
     */
    FNode           = FRootNode->AddChild();
    FNode->Texts[0] = itTag->AV["name"];
    FNode->Data     = itTag;
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::FormDestroy(TObject * Sender)
 {
  if (FCtrList)
   {
    if (FCtrList->DataProvider)
     delete FCtrList->DataProvider;
    delete FCtrList;
   }
  FCtrList = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::ListAfterScroll(TDataSet * DataSet)
 {
  FCtrList->LabDataChange();
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsStoreClientForm::CreateListExt(TTagNode * itTag)
 {
  if ((itTag->CmpName("group") && itTag->GetParent()->CmpName("unit")) || itTag->CmpName("class"))
   {
    // TcxTabSheet *Page;
    // ExtPanelList->Add((void*) new TPanel(this));
    // CurExtList = (TPanel*)ExtPanelList->Items[ExtPanelList->Count-1];
    FCurPage              = new TcxTabSheet(this);
    FCurPage->Caption     = itTag->AV["name"];
    FCurPage->PageControl = ExtListPC;
    FCurPage->Color       = clBtnFace;
    FCurPage->BorderWidth = 3;
    FCurPage->ImageIndex  = FCurPage->TabIndex;
    xcTop                 = 0;
   }
  TdsRegLabItem * tmpItem;
  if (!itTag->CmpName("class,description,actuals"))
   {
    if (itTag->AV["inlist"].LowerCase() == "e")
     {
      tmpItem      = FCtrList->AddLabItem(itTag, FCurPage->Width, FCurPage);
      tmpItem->Top = xcTop;
      xcTop += tmpItem->Height;
      // if (itTag->CmpName("extedit"))
      // tmpItem->OnGetExtData = FDM->OnGetExtData;
     }
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::actInsertExecute(TObject * Sender)
 {
  if (!actInsert->Enabled)
   return;
  TTagNode * EditNode = NULL;
  __int64 FSaveCode = 0;
  bool CanRefresh, CanCancel, CanCont;
  try
   {
    CanRefresh = true;
    CanCancel  = true;
    DisableAfterScroll();
    CanCont = true;
    bool CanReopen = false;
    try
     {
      if (FDM->OnExtInsertData)
       {
        __int64 FCode;
        FDM->OnExtInsertData(EditNode, FDataSrc, 0, CanCont, CanReopen, FCode);
        if (CanReopen)
         curCD = FCode;
       }
      if (CanCont)
       {
        if (_UID_ == "111F")
         { // ������ �������
          TdsStorIncomeEditForm * Dlg = new TdsStorIncomeEditForm(this, true, _CLASSNODE_()->AV["uid"], FDM,
            _CLASSNODE_()->GetRoot(), FDataSrc, FCtrList->TemplateData);
          try
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
          __finally
           {
            delete Dlg;
           }
         }
        else if (_UID_ == "1120")
         { // ������ �������
          TdsStorOutcomeEditForm * Dlg = new TdsStorOutcomeEditForm(this, true, _CLASSNODE_()->AV["uid"], FDM,
            _CLASSNODE_()->GetRoot(), FDataSrc, FCtrList->TemplateData);
          try
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
          __finally
           {
            delete Dlg;
           }
         }
        else if (_UID_ == "1124")
         { // ������ ��������
          TdsStorCancelEditForm * Dlg = new TdsStorCancelEditForm(this, true, _CLASSNODE_()->AV["uid"], FDM,
            _CLASSNODE_()->GetRoot(), FDataSrc, FCtrList->TemplateData);
          try
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
          __finally
           {
            delete Dlg;
           }
         }
        else
         { // ���������
          TdsStoreEditClientForm * Dlg = new TdsStoreEditClientForm(this, true, _CLASSNODE_(), FDM,
            _CLASSNODE_()->GetRoot(), FDataSrc, FCtrList->TemplateData, true);
          // TdsStoreEditClientForm(TComponent* Owner, bool AIsAppend, TTagNode* AClassDef,     TdsICStoreDM *ADM, TTagNode* ARoot, TkabCustomDataSource *ADataSrc, TdsRegTemplateData *ATmplData, bool AOneCol = true);
          try
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
          __finally
           {
            delete Dlg;
           }
         }
       }
      else
       {
        CanRefresh = CanReopen;
        CanCancel  = false;
       }
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsRegErrorInsertRecSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegErrorInsertRec), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
    if (CanCancel)
     {
      FDataSrc->DataSet->Cancel();
     }
    if (CanRefresh)
     {
      if (FDM->OnClassAfterInsert)
       FDM->OnClassAfterInsert(_CLASSNODE_());
      // if (_UID_ == "1003")
      // actRefreshExecute(actRefresh);
      // else
      RefreshById(curCD);
     }
    EnableAfterScroll();
   }
  ActiveControl = ClassGrid;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::actEditExecute(TObject * Sender)
 {
  if (!actEdit->Enabled)
   return;
  TTagNode * EditNode = NULL;
  TdsStoreEditClientForm * Dlg = NULL;
  bool CanRefresh, CanCancel, CanCont;
  try
   {
    CanRefresh = true;
    CanCancel  = true;
    DisableAfterScroll();
    EditNode = new TTagNode(NULL);
    EditNode->Assign(_CLASSNODE_(), true);
    try
     {
      CanCont = true;
      bool CanReopen = false;
      if (FDM->OnExtEditData)
       {
        __int64 FCode;
        FDM->OnExtEditData(EditNode, FDataSrc, FDataSrc->DataSet->CurRow->Value["CODE"], CanCont, CanReopen, FCode);
        if (CanReopen)
         curCD = FCode;
       }
      if (CanCont)
       {
        Dlg = new TdsStoreEditClientForm(this, false, EditNode, FDM, _CLASSNODE_()->GetRoot(), FDataSrc,
          FCtrList->TemplateData);
        if (Dlg)
         {
          if (Dlg->ShowModal() == mrOk)
           {
            // curCD = quClass()->Value("CODE")->AsInteger;
            curCD      = Dlg->InsertedRecId.ToIntDef(0);
            CanRefresh = true;
            CanCancel  = false;
           }
          else
           CanRefresh = CanReopen;
         }
       }
      else
       {
        CanRefresh = CanReopen;
        CanCancel  = false;
       }
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsRegErrorEditRecSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegErrorEditRec), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
    if (Dlg)
     delete Dlg;
    if (EditNode)
     delete EditNode;
    if (CanCancel)
     {
      /* if (quClass()->State != dsBrowse) */
      FDataSrc->DataSet->Cancel();
     }
    if (CanRefresh)
     {
      if (FDM->OnClassAfterEdit)
       FDM->OnClassAfterEdit(_CLASSNODE_());
      RefreshById(IntToStr(curCD));
     }
    EnableAfterScroll();
   }
  ActiveControl = ClassGrid;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::actDeleteExecute(TObject * Sender)
 {
  /*
   if (FDM->OnRegEditButtonClick)
   {
   if (!FDM->OnRegEditButtonClick(bcDelete)) return;
   }

   __int64 xfCode;
   TTagNode* EditNode = NULL;
   if (!quClass()->Active) return;
   */
  if (!actDelete->Enabled)
   return;
  /*
   if (!quClass()->Transaction->Active) return;
   if(quClass()->RecordCount < 1) return;
   */
  bool CanRefresh, CanCancel, CanCont;
  try
   {
    CanRefresh = true;
    CanCancel  = true;
    DisableAfterScroll();
    try
     {
      // xfCode = (__int64)quClass()->Value("CODE")->AsInteger;
      CanCont = true;
      /*
       if (FDM->OnExtDeleteData)
       {
       bool CanReopen = false;
       __int64 FCode;
       FDM->OnExtDeleteData(EditNode,FDM->DM->srcReg,xfCode,CanCont,CanReopen,FCode);
       if (CanReopen)
       {
       curCD = FCode;
       if (quClass()->Transaction->Active)
       quClass()->Transaction->Commit();
       ChangeSrc();
       if (FDM->OnClassAfterDelete) FDM->OnClassAfterDelete(_CLASSNODE_());

       }
       }
       */
      if (CanCont)
       {
        // curCD = quClass()->Value("CODE")->AsInteger;
        int FRecIdx = FDataSrc->CurrentRecIdx;
        UnicodeString Str = "";
        UnicodeString FFlId;
        if (FRecIdx >= 0)
         {
          if (_CLASSDESCR_()->AV["confirmlabel"].Trim().Length())
           { // ���� confirmlabel;
            TStringList * FConfMsgList = new TStringList;
            try
             {
              FConfMsgList->Delimiter     = '#';
              FConfMsgList->QuoteChar     = '~';
              FConfMsgList->DelimitedText = _CLASSDESCR_()->AV["confirmlabel"].Trim();
              for (int i = 0; i < FConfMsgList->Count; i++)
               {
                if (FConfMsgList->Strings[i].Length())
                 {
                  if (FConfMsgList->Strings[i][1] == '@')
                   {
                    FFlId = FConfMsgList->Strings[i].SubString(2, 4);
                    // if (_CLASSDESCR_()->GetTagByUID(FFlId)->CmpName("binary,choice,choiceDB,extedit,choiceTree"))
                    Str += " " + FDataSrc->DataSet->CurRow->StrValue[FFlId];
                    // else
                    // Str += " "+FDataSrc->DataSet->CurRow->Value[FFlId] ;
                   }
                  else
                   {
                    Str += " " + FConfMsgList->Strings[i];
                   }
                 }
               }
             }
            __finally
             {
              delete FConfMsgList;
             }
           }
          else
           {
            TTagNode * itFl = FDataSrc->DataSet->ListDefNode->GetFirstChild();
            while (itFl)
             {
              if (Str.Length())
               Str += ", ";
              // if (_CLASSDESCR_()->GetTagByUID(itFl->AV["uid"])->CmpName("binary,choice,choiceDB,extedit,choiceTree"))
              Str += FDataSrc->DataSet->CurRow->StrValue[itFl->AV["uid"]];
              // else
              // Str += FDataSrc->DataSet->CurRow->Value[itFl->AV["uid"]];
              itFl = itFl->GetNext();
             }
            Str = FMT(icsRegErrorConfirmDeleteRec) + " " + _CLASSDESCR_()->AV["deletelabel"].LowerCase() + "\n\n\" " +
              Str + "\"\n";
           }
         }
        if (MessageBox(Handle, Str.c_str(), cFMT(icsRegErrorConfirmDeleteRecMsgCaption),
          MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2) == IDYES)
         {
          FDataSrc->DataSet->Delete();
          CanRefresh = true;
          // if (quClass()->Active && quClass()->RecordCount)
          // curCD = quClass()->Value("CODE")->AsInteger;
          CanCancel = false;
         }
        else
         {
          CanRefresh = false;
          CanCancel  = false;
         }
       }
      else
       {
        CanRefresh = false;
        CanCancel  = false;
       }
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsRegErrorDeleteRecSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegErrorDeleteRec), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
    if (CanCancel)
     {
      FDataSrc->DataSet->Cancel();
     }
    if (CanRefresh)
     {
      if (FDM->OnClassAfterDelete)
       FDM->OnClassAfterDelete(_CLASSNODE_());
      actRefreshExecute(actRefresh);
     }
    EnableAfterScroll();
   }
  ActiveControl = ClassGrid;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::GetData()
 {
  // TJSONObject *Filters = new TJSONObject;
  // Filters->AddPair(_CLASSNODE_()->GetChildByAV("choiceDB","ref","1000", true)->AV["uid"],IntToStr(FUCode));
  try
   {
    FDataSrc->DataSet->GetData(NULL, TdsRegFetch::Auto, 20, FGetDataProgress);
   }
  __finally
   {
    // delete Filters;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::actRefreshExecute(TObject * Sender)
 {
  ClassGridView->BeginUpdate();
  CheckCtrlState(false);
  try
   {
    GetData();
    FDataSrc->DataChanged();
    StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
   }
  __finally
   {
    CheckCtrlState(true);
    ClassGridView->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::RefreshById(UnicodeString ACode)
 {
  ClassGridView->BeginUpdate();
  CheckCtrlState(false);
  try
   {
    FDataSrc->DataSet->GetDataById(ACode, FGetDataProgress);
    FDataSrc->DataChanged();
    StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
   }
  __finally
   {
    ClassGridView->EndUpdate();
    CheckCtrlState(true);
    ClassGridView->DataController->FocusedRecordIndex = FDataSrc->DataSet->GetRowIdxByCode(ACode);
   }
 }
// ---------------------------------------------------------------------------
TcxGridColumn * __fastcall TdsStoreClientForm::ClassCol(int AIdx)
 {
  return ClassGridView->Columns[AIdx];
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::DisableAfterScroll()
 {
  /*
   if (FDM->UseAfterScroll)
   quClass()->AfterScroll = NULL;
   else
   quClass()->OnEndScroll = NULL;
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::EnableAfterScroll()
 {
  /*
   if (quClass()->Active)
   {
   if (FDM->UseAfterScroll)
   quClass()->AfterScroll = ListAfterScroll;
   else
   quClass()->OnEndScroll = ListAfterScroll;
   }
   */
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsStoreClientForm::_CLASSNODE_()
 {
  return FSelectedNode;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsStoreClientForm::_CLASSDESCR_()
 {
  return FSelectedNodeDescr;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsStoreClientForm::GetBtnEn(UnicodeString AAttrName)
 {
  return ((bool)(_CLASSDESCR_()->AV[AAttrName].Length()));
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::actCardSelectExecute(TObject * Sender)
 {
  if (CardTypePC->Properties->Tabs->Count)
   {
    if (CardTypePC->TabIndex < (CardTypePC->Properties->Tabs->Count - FExtPageCount))
     {
      curCD         = 0;
      FSelectedNode = CardRoot->GetTagByUID(UIDStr((int)CardTypePC->Properties->Tabs->Objects[CardTypePC->TabIndex]));
      FSelectedNodeDescr = FSelectedNode->GetChildByName("description");
      CheckCtrlState(true);
      _UID_   = FSelectedNode->AV["uid"].UpperCase();
      Caption = SaveCaption + " " + FSelectedNode->AV["name"];
      Application->ProcessMessages();
      ChangeSrc();
      ActiveControl = ClassGrid;
     }
    else
     _UID_ = "";
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::actExitExecute(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::FormShow(TObject * Sender)
 {
  // if (ClassTBM->LargeIcons != FDM->UseLargeImages)
  // ClassTBM->LargeIcons = FDM->UseLargeImages;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::ClassGridViewFocusedRecordChanged(TcxCustomGridTableView * Sender,
  TcxCustomGridRecord * APrevFocusedRecord, TcxCustomGridRecord * AFocusedRecord, bool ANewItemRecordFocusingChanged)
 {
  if (IsRecSelected())
   FDataSrc->DataSet->GetDataById(VarToStr(FDataSrc->DataSet->CurRow->Value["CODE"]), FGetDataProgress);
  FCtrList->LabDataChange();
  actEdit->Enabled   = IsRecSelected() && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("editlabel"));
  actDelete->Enabled = IsRecSelected() && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("deletelabel"));
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsStoreClientForm::IsRecSelected()
 {
  return ClassGridView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::CheckCtrlState(bool AEnable)
 {
  InProgress = !AEnable;
  if (AEnable)
   {
    ProgressPanel->Visible = false;
    ProgressPanel->Width   = 0;
    ProgressPanel->Height  = 0;
   }
  else
   {
    ProgressPanel->Left    = 0;
    ProgressPanel->Top     = 0;
    ProgressPanel->Width   = ClientWidth;
    ProgressPanel->Height  = ClientHeight - 25;
    ProgressPanel->Visible = true;
    ProgressPanel->BringToFront();
   }
  if (_CLASSNODE_())
   {
    actInsert->Enabled = AEnable && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("insertlabel"));
    if (!AEnable)
     {
      actEdit->Enabled   = false;
      actDelete->Enabled = false;
     }
    else if (ClassGridView->DataController->CustomDataSource && FDataSrc)
     {
      if (FDataSrc->DataSet->RecordCount && (FDataSrc->CurrentRecIdx != -1))
       {
        actEdit->Enabled   = AEnable && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("editlabel"));
        actDelete->Enabled = AEnable && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("deletelabel"));
       }
      else
       {
        actEdit->Enabled   = false;
        actDelete->Enabled = false;
       }
     }
    actRefresh->Enabled = AEnable;
    actPrintJornal->Enabled = AEnable;
    // actTemplate->Enabled         = AEnable;
    // actSetFilter->Enabled        = AEnable;
    // actReSetFilter->Enabled      = AEnable;
    // actFind->Enabled             = AEnable;
    // actFindAndFilter->Enabled    = AEnable;
    // actClearSearchField->Enabled = AEnable;
   }
  else
   {
    actInsert->Enabled      = false;
    actEdit->Enabled        = false;
    actDelete->Enabled      = false;
    actRefresh->Enabled     = false;
    actPrintJornal->Enabled = false;
    // actTemplate->Enabled         = false;
    // actSetFilter->Enabled        = false;
    // actReSetFilter->Enabled      = false;
    // actFind->Enabled             = false;
    // actFindAndFilter->Enabled    = false;
    // actClearSearchField->Enabled = false;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::CardTypePCChange(TObject * Sender)
 {
  if (CardTypePC->TabIndex < (CardTypePC->Properties->Tabs->Count - FExtPageCount))
   {
    CardViewPC->ActivePage = ComTS;
    actCardSelectExecute(actCardSelect);
   }
  // else if (CardTypePC->TabIndex == (CardTypePC->Properties->Tabs->Count - FExtPageCount))
  // {
  // CardViewPC->ActivePage = F63TS;
  // Caption = SaveCaption+"������� �����";
  // F63Viewer->LoadFromString(FDM->kabCDSGetMIBPForm());
  // }
  else
   {
    Caption                = SaveCaption + " ���������";
    CardViewPC->ActivePage = SettingTS;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::actGetCoolListExecute(TObject * Sender)
 {
  if (FDM->RegComp)
   {
    FDM->RegComp->ShowClasses(ctList, "storage", "32E8");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::actGetPostListExecute(TObject * Sender)
 {
  if (FDM->RegComp)
   {
    FDM->RegComp->ShowClasses(ctList, "storage", "1131");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::actGetPointListExecute(TObject * Sender)
 {
  if (FDM->RegComp)
   {
    FDM->RegComp->ShowClasses(ctList, "storage", "32EE");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::actGroupCancellationExecute(TObject * Sender)
 {
  TdsGroupCancellationForm * Dlg = new TdsGroupCancellationForm(this, FDM);
  try
   {
    Dlg->ShowModal();
   }
  __finally
   {
    delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::actPrintJornalExecute(TObject * Sender)
 {
  CheckCtrlState(false);
  try
   {
    if (FDM->OnPrintReportClick  && _UID_.Length())
     {
        if (IsRecSelected())
         FDM->OnPrintReportClick(_UID_, (__int64)FDataSrc->DataSet->CurRow->Value["CODE"], "", CheckProgress);
        else
         FDM->OnPrintReportClick(_UID_, -1, "", CheckProgress);
     }
   }
  __finally
   {
    CheckCtrlState(true);
   }
  ActiveControl = ClassGrid;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsStoreClientForm::CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg)
 {
  Application->ProcessMessages();
  switch (AType)
   {
   case TkabProgressType::InitCommon:
     {
      SetFocus();
      PrBar->Properties->Max           = AVal;
      StatusSB->Panels->Items[2]->Text = AMsg;
      break;
     }
   case TkabProgressType::StageInc:
     {
      // MsgLab->Caption = AMsg;
      StatusSB->Panels->Items[2]->Text = AMsg;
      break;
     }
   case TkabProgressType::CommInc:
     {
      Caption                          = SaveCaption + " {" + FloatToStrF(AVal / 10.0, ffFixed, 2, 1) + "%}";
      StatusSB->Panels->Items[2]->Text = AMsg;
      PrBar->Position                  = AVal;
      break;
     }
   case TkabProgressType::CommComplite:
     {
      // MsgLab->Caption = "";
      // PrBar->Position = 0;
      StatusSB->Panels->Items[2]->Text = "";
      Caption                          = SaveCaption + " " + _CLASSNODE_()->AV["name"];
      break;
     }
   }
  Application->ProcessMessages();
  return InProgress;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsStoreClientForm::actOutSrcListExecute(TObject *Sender)
{
  if (FDM->RegComp)
   {
    FDM->RegComp->ShowClasses(ctList, "storage", "330F");
   }
}
//---------------------------------------------------------------------------

