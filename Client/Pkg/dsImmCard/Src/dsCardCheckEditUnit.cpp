﻿//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsCardCheckEditUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TdsCardCheckEditForm *dsCardCheckEditForm;
//---------------------------------------------------------------------------
#define _LPU_       "1108"
#define _InThisLPU_ "110A"

//---------------------------------------------------------------------------
#define EDLEFT1  2
#define EDLEFT2  232
//---------------------------------------------------------------------------
const int LeftMargin = 2;
const int LevelCorrection = 5;
__fastcall TdsCardCheckEditForm::TdsCardCheckEditForm(TComponent* Owner, bool AIsAppend, UnicodeString AClassId, __int64 AUCode, TdsICCardDM *ADM, TTagNode* ARoot, TDate APatBirthday, TkabCustomDataSource *ADataSrc, TdsRegTemplateData *ATmplData, bool AOneCol)
    : TForm(Owner)
{
  TypeCB->Style->StyleController = ADM->StyleController;

  FUCode = AUCode;
  FDM = ADM;
  FDataSrc = ADataSrc;
  FOneCol = AOneCol;
  FInsertedRecId = "";
  FPatBirthday = APatBirthday;

  OkBtn->Caption = FMT(icsRegClassifEditApplyBtnCaption);
  CancelBtn->Caption = FMT(icsRegClassifEditCancelBtnCaption);
  FIsAppend = AIsAppend;
  if (FIsAppend)
   FDataSrc->DataSet->Insert();
  else
   FDataSrc->DataSet->Edit();

  FClassDef = ARoot->GetTagByUID(AClassId);
  FClassDesc = FClassDef->GetChildByName("description");

  FCtrList                = new TdsRegEDContainer(this,CompPanel);
  FCtrList->UseHash       = true;
  FCtrList->DefXML        = ARoot;
  FCtrList->ReqColor      = FDM->RequiredColor;
  FCtrList->StyleController = FDM->StyleController;
  FCtrList->TemplateData    = ATmplData;
  FCtrList->DataPath        = FDM->DataPath;

  FCtrList->DataProvider = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider*)FCtrList->DataProvider)->DefXML = ARoot;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  ((TdsRegEDkabDSDataProvider*)FCtrList->DataProvider)->DataSource    = FDataSrc;
  FCtrList->DataProvider->OnGetCompValue       = FDM->OnGetCompValue;

  FCtrList->OnDataChange         = FCtrlDataChange;
  FCtrList->OnGetExtBtnGetBitmap = FDM->OnGetExtBtnGetBitmap;
  FCtrList->OnGetFormat          = FDM->OnGetFormat;
  FCtrList->OnExtBtnClick        = FDM->OnExtBtnClick;


  ColWidth = FDM->ULEditWidth/2+FOneCol*50;

  int FGroupCount = 0;
  bool FGroupOnly = true;

  TTagNode *itNode = FClassDef->GetFirstChild();
  while (itNode)
   {
     if (!itNode->CmpName("description"))
      {
        if (itNode->CmpName("group"))
         FGroupCount++;
        else
         FGroupOnly = false;
      }
     itNode = itNode->GetNext();
   }


  FLastTop = 0;
  FLastHeight = 0;
  int xTop,xLeft;

  LastLeftCtrl = NULL;
  int MaxH = 0;
  TList *CntrlList = new TList;

  TTagNode *FDefNode = NULL;

  if (FGroupOnly)
   FDefNode = FClassDesc->GetNext();
  else
   FDefNode = FClassDef;
  SubLevel = 0;
  xTop = 5; xLeft = LeftMargin;
  LastCtrl = NULL;
  CreateUnitList(CompPanel, FDefNode, xTop, &xLeft, CntrlList);
  if (LastLeftCtrl)
   {
    if ((LastLeftCtrl->Top+LastLeftCtrl->Height) <= xTop)
     MaxH = xTop;
    else
     MaxH = LastLeftCtrl->Top+LastLeftCtrl->Height;
   }
  else
   MaxH = xTop;
  ClientHeight = MaxH+50;
//     Width = ColWidth+(!FOneCol)*ColWidth+(Width-CompPanel->Width)+LeftMargin*2;
//  FCtrList->SetOnCtrlDataChange(0);
  FClassDef->GetFirstChild();
  FClassDef->Iterate(FSetFocus);
  delete CntrlList; CntrlList = NULL;
  if (FIsAppend)
   {
     bool valid;
     if (FDM->OnGetDefValues)
      FDM->OnGetDefValues(-1,FClassDef,FCtrList,valid);
     Caption = FClassDef->GetChildByName("description")->AV["insertlabel"];
   }
  else
   Caption = FClassDef->GetFirstChild()->AV["editlabel"];
   // добавлено 7.10.08 Евдокимов М.В.
   // поднимаем событие на передачу списка компонентов
/*   if ( ARegComp->OnAfterClassEditCreate )
   {
       ARegComp->OnAfterClassEditCreate ( (TForm*)this, FCtrList, AClassDef, RegComp->DM->quReg->Value("CODE")->AsInteger );
   }*/
}
//---------------------------------------------------------------------------
bool __fastcall TdsCardCheckEditForm::FSetFocus(TTagNode *itxTag)
{
  TControl *tmpCtrl;
  if (itxTag->GetAttrByName("uid"))
   {
     if (FCtrList->GetEDControl(itxTag->AV["uid"]))
      {
        tmpCtrl = FCtrList->EditItems[itxTag->AV["uid"]]->GetFirstEnabledControl();
        if (tmpCtrl)
         {
           ActiveControl = (TWinControl*)tmpCtrl;
           return true;
         }
      }
   }
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsCardCheckEditForm::CreateUnitList(TWinControl *AParent, TTagNode* ANode, int &ATop, int *ALeft, TList *GroupComp, int APIndex)
{

//<extedit name='Код пациента' uid='1101' ref='1000' depend='1101.1101' isedit='0' fieldtype='integer'/>
//<date name='Дата выполнения' uid='1104' required='1' inlist='l' default='CurrentDate'/>
//<extedit name='Возраст' uid='1105' depend='1104' isedit='0' length='9'/>
//<choiceDB name='Проверка' uid='1103' ref='013E' required='1' inlist='l'/>
//<text name='Значение' uid='110F' cast='no' length='50'/>
//<extedit name='Схема' uid='1107' required='1' inlist='e' isedit='0' length='9'/>
//<choiceDB name='ЛПУ' uid='1108' ref='00C7' inlist='e' linecount='2'/>
//<binary name='Выполнена в данном ЛПУ' uid='110A' required='1' inlist='e' invname='Выполнена в другой организации' default='check'/>
//<choiceDB name='Врач' uid='110B' ref='00AA' inlist='e' depend='1108.00AA;1109.00AA' linecount='2'/>
//<choiceDB name='Мед. сестра' uid='110C' ref='00AA' inlist='e' depend='1108.00AA;1109.00AA' linecount='2'/>
//<choiceDB name='Источник финансирования' uid='110D' ref='00AB' inlist='e' linecount='2'/>
//<choiceDB name='Доп. информация' uid='110E' ref='00BE' inlist='e' linecount='3'/>
//<date name='ДР пациента' uid='1160' isedit='0' default='patbd'/>

  TdsRegEDItem* FED;
  int FOrder = 0;
  int FReak = 1;

  //<date name='ДР пациента' uid='1160' isedit='0' default='patbd'/>
  FED = AddRegED("32D9");  FED->Visible = false;
  FED = AddRegED("1160");  FED->Visible = false; FED->SetValue(FPatBirthday.FormatString("dd.mm.yyyy"));
  //<extedit name='Код пациента' uid='1101' ref='1000' depend='1101.1101' isedit='0' fieldtype='integer'/>
  FED = AddRegED("1101");  FED->Visible = false; FED->SetValue(FUCode);
  //<date name='Дата выполнения' uid='1104' required='1' inlist='l' default='CurrentDate'/>
  FED = AddRegED("1104");  SetAlign(FED, EDLEFT1, ATop, 200, FOrder++);
  //<choiceDB name='Проверка' uid='1103' ref='013E' required='1' inlist='l'/>
  FED = AddRegED("1103");  ATop += SetAlign(FED, EDLEFT2, ATop, 250, FOrder++);
  FED->OnDataChange = CBChange;
  TypeCB->Top = ATop; TypeCB->Left = 80; TypeCB->TabOrder = FOrder++;
  TypeLab->Top = ATop; TypeLab->Left = 7;
  TypeCB->Height+4;
  ATop += TypeCB->Height+4;
//  // <text name='Серия препарата' uid='1033' inlist='l' cast='no' length='20'/>
//  FED = AddRegED("1033");  ATop += SetAlign(FED, EDLEFT2, ATop, 250, FOrder++);
//  // <choice name='Повод' uid='1034' required='1' inlist='e' default='0'>
//  FED = AddRegED("1034");  ATop += SetAlign(FED, EDLEFT1, ATop, 200, FOrder++);
//  FED->OnDataChange = CBChange;

//  //********** Реакция проверена uid=103B          <binary name='' required='1' inlist='e' default='check'/>
//  FED = AddRegED("103B");   ATop += SetAlign(FED, EDLEFT1, ATop, 300, FOrder++);
  // <binary name='Есть реакция' uid='1041' inlist='e' invname='Реакция отрицательная'>
//  FED = AddRegED("1041"); ATop += SetAlign(FED, EDLEFT2, ATop, 300, FOrder++);

//  // <choiceDB name='Формат реакции' uid='10A7' ref='0038' required='1' inlist='l' depend='1032.002B'>
//  FED = AddRegED("10A7");
//  ((TcxComboBox*)FED->GetFirstEnabledControl())->Properties->Alignment->Horz = taRightJustify;
//  SetAlign(FED, 5, ATop, 170, FOrder++);   FED->SetEDLeft(50); FED->SetLabel("Реакция "); //+IntToStr(FReak++)

  //<text name='Значение' uid='110F' cast='no' length='50'/>

  //        <choiceDB name='Результат' uid='110F' ref='0038' required='1' inlist='l' depend='1032.002B'/>
  //        <text name='Результат(значение)' uid='319B' inlist='l' cast='no' length='255'>
  //        <text name='№ ответа' uid='319C' inlist='l' cast='up' length='20'/>

  FED = AddRegED("110F"); SetAlign(FED, EDLEFT1, ATop, 250, FOrder++); //FED->SetEDLeft(70);
  FED = AddRegED("319B"); ATop += SetAlign(FED, 290, ATop, 200, FOrder++); FED->SetEDLeft(0);
  FED = AddRegED("319C"); ATop += SetAlign(FED, EDLEFT1, ATop, 250, FOrder++); //FED->SetEDLeft(70);


  // <choiceDB name='ЛПУ' uid='018B' ref='00C7' inlist='e' linecount='2'/>
  FED = AddRegED(_LPU_);          SetAlign(FED, EDLEFT1, ATop, 250, FOrder++); FED->SetEDLeft(73);
  // <binary name='Выполнена в данном ЛПУ' uid='10A5' required='yes' inlist='list_ext' invname='Выполнена в другой организации' default='check'/>
  FED = AddRegED(_InThisLPU_);  ATop += SetAlign(FED, EDLEFT1, ATop, 300, FOrder++);
  RegComp(_LPU_)->OnDataChange  = InThisLPUChBChange;

  //<choiceDB name='Врач' uid='110B' ref='00AA' inlist='e' depend='1108.00AA;1109.00AA' linecount='2'/>
  FED = AddRegED("110B");  SetAlign(FED, 5, ATop, 250, FOrder++); FED->SetEDLeft(68); FED->SetLabel(FED->DefNode->AV["name"]);

  //<choiceDB name='Мед. сестра' uid='110C' ref='00AA' inlist='e' depend='1108.00AA;1109.00AA' linecount='2'/>
  FED = AddRegED("110C");  ATop += SetAlign(FED, 290, ATop, 245, FOrder++);  FED->SetLabel(FED->DefNode->AV["name"]);
  //<choiceDB name='Источник финансирования' uid='110D' ref='00AB' inlist='e' linecount='2'/>
  FED = AddRegED("110D");  ATop += SetAlign(FED, EDLEFT1, ATop, 300, FOrder++);
  ((TWinControl*)FED->GetControl())->Width = 290;
  FED->SetEDLeft(155);
  //<choiceDB name='Доп. информация' uid='110E' ref='00BE' inlist='e' linecount='3'/>
  FED = AddRegED("110E");  ATop += SetAlign(FED, EDLEFT1, ATop, 300, FOrder++);
  ((TWinControl*)FED->GetControl())->Width = 290;
  FED->SetEDLeft(155);

  Height = ATop + 60;
}
//---------------------------------------------------------------------------
void __fastcall TdsCardCheckEditForm::AlignCtrls(TList *ACtrlList)
{
  int LabR,EDL;
  int xLabR,xEDL;
  LabR = 0; EDL = 9999;
  TdsRegEDItem *tmpED;
  for (int i = 0; i < ACtrlList->Count; i++)
   {
     // определяем max правую границу меток контролов
     // и min левую самих контролов
     tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
     tmpED->GetEDLeft(&xLabR,&xEDL);
     if (xLabR != -1) LabR = max(xLabR,LabR);
     if (xEDL != -1)  EDL  = min(xEDL,EDL);
   }
  if ((LabR+2) < EDL)
   {
     // max правая граница меток контролов
     // и min левая самих контролов не пересекаются
     // выравниваем контролы по min левой границе самих контролов
     for (int i = 0; i < ACtrlList->Count; i++)
      {
        tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
        tmpED->SetEDLeft(EDL);
      }
   }
  else
   {
     // иначе выравниваем по "max правая граница меток контролов" + 2
     for (int i = 0; i < ACtrlList->Count; i++)
      {
        tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
        tmpED->SetEDLeft(LabR+2);
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsCardCheckEditForm::FormKeyDown(TObject *Sender, WORD &Key,  TShiftState Shift)
{
  if (Key == VK_RETURN)
   {ActiveControl = OkBtn; OkBtnClick(this);}
}
//---------------------------------------------------------------------------
void __fastcall TdsCardCheckEditForm::OkBtnClick(TObject *Sender)
{
  TWinControl *tmpCtrl = ActiveControl;
  ActiveControl = OkBtn;
  ActiveControl = tmpCtrl;
  if (CheckInput())
   { // запрос на проверку данных
     try
      {
        UnicodeString PRC = FDataSrc->DataSet->Post(FInsertedRecId, true);
        if (!SameText(PRC,"ok"))
          _MSG_ERR(PRC,"Ошибка");
        else
         ModalResult = mrOk;
      }
     catch (EkabCustomDataSetError &E)
      {
        _MSG_ERR(E.Message,"Ошибка");
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TdsCardCheckEditForm::CheckInput()
{
  bool RC = false;
  try
   {
     if (FIsAppend)
      FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
     else
      FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
     UnicodeString xValid = "";
     FClassDef->Iterate(GetInput,xValid);
     if (!xValid.Length())
      {
        RC = true;
        if (FDM->OnExtValidate)
         {
           if (FIsAppend)
            FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
           else
            FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCardCheckEditForm::GetInput(TTagNode *itTag, UnicodeString &UID)
{
   if (itTag)
    {
      if (FCtrList->GetEDControl(itTag->AV["uid"]))
       if (!FCtrList->EditItems[itTag->AV["uid"]]->CheckReqValue())
        {
           UID = "no";
           return true;
        }
    }
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsCardCheckEditForm::FormDestroy(TObject *Sender)
{
   FCtrList->ClearOnCtrlDataChange(0);
   delete FCtrList->DataProvider;
   delete FCtrList;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCardCheckEditForm::FCtrlDataChange(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource)
{
  bool RC = false;
  if (FDM->OnCtrlDataChange)
   {
     RC = FDM->OnCtrlDataChange(ItTag, Src, ASource, FIsAppend, FCtrList, UIDInt(FClassDef->AV["uid"]));
   }
  return RC;
}
//---------------------------------------------------------------------------
int  __fastcall TdsCardCheckEditForm::SetAlign(TdsRegEDItem* AItem, int ALeft, int ATop, int AWidth, int ATabOrder)
{
  AItem->Left = ALeft;
  AItem->Width = AWidth;
  AItem->Top = ATop;
  AItem->Anchors.Clear();
  AItem->Anchors << akLeft << akRight << akTop;
  AItem->TabOrder = ATabOrder;
  return AItem->Height+4;
}
//---------------------------------------------------------------------------
inline TdsRegEDItem* __fastcall TdsCardCheckEditForm::RegComp(UnicodeString AUID)
{
  return FCtrList->EditItems[AUID];
}
//---------------------------------------------------------------------------
inline TdsRegEDItem* __fastcall TdsCardCheckEditForm::AddRegED(UnicodeString AUID)
{
  return FCtrList->AddEditItem(FClassDef->GetTagByUID(AUID), FIsAppend);
}
//---------------------------------------------------------------------------
bool __fastcall TdsCardCheckEditForm::InThisLPUChBChange(TTagNode *ItTag, UnicodeString &Src)
{
  if (FDM->LPUCode)
   {
     if (RegComp(_LPU_)->GetValue("").ToIntDef(0) == FDM->LPUCode)
      {
        FCtrList->EditItems[_InThisLPU_]->SetValue("1","");
      }
     else
      FCtrList->EditItems[_InThisLPU_]->SetValue("0","");
   }
  return true;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCardCheckEditForm::CBChange(TTagNode *ItTag, UnicodeString &Src)
{
  bool RC = false;
  try
   {
     UnicodeString xProb,xOldProbVal,xOldProbFormat,xProbDate;
     xProb = RegComp("1103")->GetValue("013E.");
     TypeCB->Properties->Items->Clear();
        TypeCB->Properties->Items->AddObject("Доп.",(TObject*)0);

/*
     if ((RegComp("1034")->GetValue("").ToIntDef(0) != 1) && (FInfCode != -1))
      {
//            DM->qtExec("Select R1079 From CARD_1076 Where UCODE="+IntToStr(FUCode)+"and  R1077="+FtmpInfCode,false);
//            if (!FCurrSch.Length())
//             FCurrSch = _GUI(quFNStr("R1079"));
        TTagNode *tmp;// = DM->xmlTestDef->GetTagByUID(FCurrSch);
//            if (tmp)
//             {
           TTagNodeMap SchList;
           FDM->GetSchemeList(FInfCode, SchList, false);
           for (TTagNodeMap::iterator i = SchList.begin(); i != SchList.end(); i++)
            {
              if (i->second->CmpAV("ref",xProb))
               {
                 tmp = i->second->GetFirstChild();
                 while (tmp)
                  {
                    if (tmp->CmpName("line"))
                     {
                       TypeCB->Properties->Items->AddObject(i->second->AV["name"]+"."+tmp->AV["name"],(TObject*)UIDInt(tmp->AV["uid"]));
                     }
                    tmp = tmp->GetNext();
                  }
               }
            }
//       }
      }
     else
      {
  //      FCurrSch = "";
        TypeCB->Properties->Items->AddObject("Доп.",(TObject*)0);
      }
*/
     if (TypeCB->Properties->Items->Count == 1)
      TypeCB->ItemIndex = 0;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

