﻿//---------------------------------------------------------------------------
#ifndef dsCardSchListUnitH
#define dsCardSchListUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxClasses.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
//---------------------------------------------------------------------------
#include "dsICCardDMUnit.h"
//---------------------------------------------------------------------------

class PACKAGE TSchemaListForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TcxTreeList *SchemaTL;
        TcxTreeListColumn *SchCol;
 TcxStyleRepository *Style;
        TcxStyle *BoldStyle;
        TcxStyle *NoBoldStyle;
        TcxStyle *cxStyle1;
        void __fastcall OkBtnClick(TObject *Sender);
  void __fastcall SchemaTLStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AItem, TcxTreeListNode *ANode, TcxStyle *&AStyle);
private:	// User declarations
//    TStringList *xValues;
  TdsICCardDM *FDM;
    bool FSelectSchOnly, FVac;
public:		// User declarations
    UnicodeString Value;
    __fastcall TSchemaListForm(TComponent* Owner, TdsICCardDM *ADM, long InfCode, bool isVac, UnicodeString AVal, bool ASelectSchOnly = false);
};
//---------------------------------------------------------------------------
extern PACKAGE TSchemaListForm *SchemaListForm;
//---------------------------------------------------------------------------
#endif
