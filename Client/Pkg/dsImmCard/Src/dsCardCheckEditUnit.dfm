object dsCardCheckEditForm: TdsCardCheckEditForm
  Left = 369
  Top = 357
  HelpContext = 1011
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = ':)'
  ClientHeight = 375
  ClientWidth = 571
  Color = clBtnFace
  Constraints.MinHeight = 110
  Constraints.MinWidth = 298
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = [fsBold]
  GlassFrame.Bottom = 45
  OldCreateOrder = True
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 334
    Width = 571
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      571
      41)
    object OkBtn: TcxButton
      Left = 403
      Top = 9
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = OkBtnClick
    end
    object CancelBtn: TcxButton
      Left = 487
      Top = 9
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object CompPanel: TPanel
    Left = 0
    Top = 0
    Width = 571
    Height = 334
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 5
    UseDockManager = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object TypeLab: TLabel
      Left = 8
      Top = 304
      Width = 73
      Height = 13
      Caption = #1042#1080#1076' '#1087#1088#1086#1074#1077#1088#1082#1080':'
    end
    object TypeCB: TcxComboBox
      Left = 71
      Top = 296
      Properties.DropDownListStyle = lsEditFixedList
      Style.Color = clWindow
      TabOrder = 0
      Width = 120
    end
  end
end
