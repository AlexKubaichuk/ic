﻿//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsCardOtvodEditUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxCheckListBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
#define _Type_       "3043"
#define _Inf_        "3083"
#define _Prich_      "3066"
#define _MKB_        "3082"
#define _BegDate_    "305F"
#define _EndDate_    "3065"
#define _PrivCancel_ "3110"
#define _TestCancel_ "3111"
//---------------------------------------------------------------------------
#define _EDLEFT  2
#define _EDLEFT2 232

TdsCardOtvodEditForm * dsCardOtvodEditForm;
//---------------------------------------------------------------------------
const int LeftMargin = 2;
const int LevelCorrection = 5;
__fastcall TdsCardOtvodEditForm::TdsCardOtvodEditForm(TComponent * Owner, bool AIsAppend, UnicodeString  AClassId, __int64 AUCode, TdsICCardDM * ADM, TTagNode * ARoot, TDate APatBirthday,
    TkabCustomDataSource * ADataSrc, TdsRegTemplateData * ATmplData, bool AOneCol)
    : TForm(Owner)
 {
  InfTypeCB->Style->StyleController = ADM->StyleController;
  InfChLB->Style->StyleController   = ADM->StyleController;

  FUCode             = AUCode;
  FDM                = ADM;
  FDataSrc           = ADataSrc;
  FOneCol            = AOneCol;
  FInsertedRecId     = "";
  FPatBirthday       = APatBirthday;
  FEditInfList       = new TStringList;
  OkBtn->Caption     = FMT(icsRegClassifEditApplyBtnCaption);
  CancelBtn->Caption = FMT(icsRegClassifEditCancelBtnCaption);
  FIsAppend          = AIsAppend;
  if (FIsAppend)
   FDataSrc->DataSet->Insert();
  else
   {
    FEditInfList->Delimiter     = ';';
    FEditInfList->DelimitedText = FDM->GetOtvodInf((__int64)FDataSrc->DataSet->CurRow->Value["code"]);
    FDataSrc->DataSet->Edit();
   }

  FClassDef = ARoot->GetTagByUID(AClassId);
  FClassDesc = FClassDef->GetChildByName("description");

  FCtrList                  = new TdsRegEDContainer(this, CompPanel);
  FCtrList->UseHash         = true;
  FCtrList->DefXML          = ARoot;
  FCtrList->ReqColor        = FDM->RequiredColor;
  FCtrList->StyleController = FDM->StyleController;
  FCtrList->TemplateData    = ATmplData;
  FCtrList->DataPath        = FDM->DataPath;

  FCtrList->DataProvider = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DefXML = ARoot;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DataSource = FDataSrc;
  FCtrList->DataProvider->OnGetCompValue = FDM->OnGetCompValue;

  FCtrList->OnDataChange         = FCtrlDataChange;
  FCtrList->OnGetExtBtnGetBitmap = FDM->OnGetExtBtnGetBitmap;
  FCtrList->OnGetFormat          = FDM->OnGetFormat;
  FCtrList->OnExtBtnClick        = FDM->OnExtBtnClick;
  FCtrList->OnKeyDown            = FormKeyDown;

  ColWidth = FDM->ULEditWidth / 2 + FOneCol * 50;

  int FGroupCount = 0;
  bool FGroupOnly = true;

  TTagNode * itNode = FClassDef->GetFirstChild();
  while (itNode)
   {
    if (!itNode->CmpName("description"))
     {
      if (itNode->CmpName("group"))
       FGroupCount++ ;
      else
       FGroupOnly = false;
     }
    itNode = itNode->GetNext();
   }

  FLastTop = 0;
  FLastHeight = 0;
  int xTop, xLeft;

  LastLeftCtrl = NULL;
  int MaxH = 0;
  TList * CntrlList = new TList;

  TTagNode * FDefNode = NULL;

  if (FGroupOnly)
   FDefNode = FClassDesc->GetNext();
  else
   FDefNode = FClassDef;
  SubLevel = 0;
  xTop     = 2;
  xLeft    = LeftMargin;
  LastCtrl = NULL;
  CreateUnitList(CompPanel, FDefNode, xTop, & xLeft, CntrlList);
  if (LastLeftCtrl)
   {
    if ((LastLeftCtrl->Top + LastLeftCtrl->Height) <= xTop)
     MaxH = xTop;
    else
     MaxH = LastLeftCtrl->Top + LastLeftCtrl->Height;
   }
  else
   MaxH = xTop;
  ClientHeight = MaxH + 50;
  //Width = ColWidth+(!FOneCol)*ColWidth+(Width-CompPanel->Width)+LeftMargin*2;
  //FCtrList->SetOnCtrlDataChange(0);
  FClassDef->GetFirstChild();
  //FClassDef->Iterate(FSetFocus);
  delete CntrlList;
  CntrlList = NULL;
  if (FIsAppend)
   {
    bool valid;
    if (FDM->OnGetDefValues)
     FDM->OnGetDefValues(-1, FClassDef, FCtrList, valid);
    Caption = FClassDef->GetChildByName("description")->AV["insertlabel"];
    for (int i = 0; i < InfChLB->Items->Count; i++)
     {
      InfChLB->Items->Items[i]->Checked = true;
     }

    RegComp(_BegDate_)->SetValue(Date().FormatString("dd.mm.yyyy"));
    RegComp(_EndDate_)->SetValue(IncMonth(Date(), 1).FormatString("dd.mm.yyyy"));
   }
  else
   {
    Caption = FClassDef->GetFirstChild()->AV["editlabel"];
    bool FFullInf = true;
    for (int i = 0; FFullInf && (i < InfChLB->Items->Count); i++)
     {
      if (FEditInfList->IndexOf(IntToStr((int)InfChLB->Items->Objects[i])) == -1)
       FFullInf = false;
     }
    if (FFullInf)
     {
      InfTypeCB->ItemIndex = 0;
      for (int i = 0; i < InfChLB->Items->Count; i++)
       {
        InfChLB->Items->Items[i]->Checked = true;
       }
     }
    else
     {
      InfTypeCB->ItemIndex = 1;
      for (int i = 0; i < InfChLB->Items->Count; i++)
       {
        if (FEditInfList->IndexOf(IntToStr((int)InfChLB->Items->Objects[i])) != -1)
         InfChLB->Items->Items[i]->Checked = true;
       }
     }
   }
  //((TWinControl*)RegComp(_Type_)->GetFirstEnabledControl())->SetFocus();

  //добавлено 7.10.08 Евдокимов М.В.
  //поднимаем событие на передачу списка компонентов
  /*if ( ARegComp->OnAfterClassEditCreate )
   {
   ARegComp->OnAfterClassEditCreate ( (TForm*)this, FCtrList, AClassDef, RegComp->DM->quReg->FN("CODE")->AsInteger );
   }*/
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCardOtvodEditForm::FSetFocus(TTagNode * itxTag)
 {
  TControl * tmpCtrl;
  if (itxTag->GetAttrByName("uid"))
   {
    if (FCtrList->GetEDControl(itxTag->AV["uid"]))
     {
      tmpCtrl = FCtrList->EditItems[itxTag->AV["uid"]]->GetFirstEnabledControl();
      if (tmpCtrl)
       {
        ActiveControl = (TWinControl *)tmpCtrl;
        return true;
       }
     }
   }
  return false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardOtvodEditForm::CreateUnitList(TWinControl * AParent, TTagNode * ANode, int & ATop, int * ALeft, TList * GroupComp, int APIndex)
 {
  TdsRegEDItem * tmpItem;
  int FTabOrder = 0;

  //<extedit name='Код пациента' uid='017C' ref='1000' depend='017C.017C' isedit='0' fieldtype='integer'/>
  //<binary name='Отвод прививки' uid='1110' default='check'/>
  //<binary name='Отвод пробы' uid='1111'/>

  //<extedit name='Код пациента' uid='317C' ref='1000' depend='1101.1101' isedit='0' fieldtype='integer'/>
  tmpItem          = AddRegED("32DA");
  tmpItem->Visible = false;
  tmpItem          = AddRegED("317C");
  tmpItem->Visible = false;
  tmpItem->SetValue(FUCode);

  //<choiceDB name='Инфекция' uid='1083' ref='003A' required='1' inlist='l'/>
  tmpItem          = AddRegED(_Inf_);
  tmpItem->Visible = false; //<choiceBD name='Вакцинация от' uid='1083' ref='003A' required='yes' inlist='list'/>

  //<choice name='Причина' uid='1066' required='1' inlist='e' default='0'>
  tmpItem = AddRegED(_Prich_);
  ATop += SetAlign(tmpItem, _EDLEFT, ATop, 460, FTabOrder++); //tmpItem->SetEDLeft(70);

  //<date name='Начало действия' uid='105F' required='1' inlist='l'/>
  tmpItem               = AddRegED(_BegDate_); /*ATop +=*/SetAlign(tmpItem, _EDLEFT, ATop, 230, FTabOrder++); //<date name='Начало действия' uid='105F' required='yes' inlist='list'/>
  tmpItem->OnDataChange = FTypeDateChange;

  //<date name='Окончание действия' uid='1065' required='1' inlist='l'>
  tmpItem = AddRegED(_EndDate_);
  ATop += SetAlign(tmpItem, _EDLEFT2, ATop, 230, FTabOrder++); //<date name='Окончание действия' uid='1065' required='yes' inlist='list'>
  tmpItem->OnDataChange = FTypeDateChange;

  //<choice name='Отвод' uid='1043' required='1' inlist='l' default='0'>
  tmpItem          = AddRegED(_Type_);
  tmpItem->Visible = false; //SetAlign(tmpItem, _EDLEFT, ATop, 190, FTabOrder++);// tmpItem->SetEDLeft(70);
  //tmpItem->OnDataChange = FTypeDateChange;

  OtvodAgeLab->Top = ATop;
  ATop += 21;

  //<choiceTree name='Заболевание' uid='1082' ref='011D' required='1' inlist='e' dlgtitle='МКБ-10' linecount='2'>
  tmpItem = AddRegED(_MKB_);
  ATop += SetAlign(tmpItem, _EDLEFT, ATop, 460, FTabOrder++);

  //<binary name='Отвод прививки' uid='1110' default='check'/>
  tmpItem = AddRegED(_PrivCancel_); /*ATop +=*/SetAlign(tmpItem, _EDLEFT, ATop, 230, FTabOrder++);
  //<binary name='Отвод пробы' uid='1111'/>
  tmpItem = AddRegED(_TestCancel_);
  ATop += SetAlign(tmpItem, _EDLEFT2, ATop, 230, FTabOrder++);

  InfPanel->Top = ATop + 3;
  ATop += InfPanel->Height + 3;
  InfPanel->TabOrder = FTabOrder++ ;
  //SelAllBtn->Top   = ATop+3;
  //DeSelAllBtn->Top = ATop+3;
  //ATop += 32;
  //SelAllBtn->TabOrder = FTabOrder++;  DeSelAllBtn->TabOrder = FTabOrder++;
  LoadInfList();
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardOtvodEditForm::AlignCtrls(TList * ACtrlList)
 {
  int LabR, EDL;
  int xLabR, xEDL;
  LabR = 0;
  EDL  = 9999;
  TdsRegEDItem * tmpED;
  for (int i = 0; i < ACtrlList->Count; i++)
   {
    //определяем max правую границу меток контролов
    //и min левую самих контролов
    tmpED = ((TdsRegEDItem *)ACtrlList->Items[i]);
    tmpED->GetEDLeft(& xLabR, & xEDL);
    if (xLabR != -1)
     LabR = max(xLabR, LabR);
    if (xEDL != -1)
     EDL = min(xEDL, EDL);
   }
  if ((LabR + 2) < EDL)
   {
    //max правая граница меток контролов
    //и min левая самих контролов не пересекаются
    //выравниваем контролы по min левой границе самих контролов
    for (int i = 0; i < ACtrlList->Count; i++)
     {
      tmpED = ((TdsRegEDItem *)ACtrlList->Items[i]);
      tmpED->SetEDLeft(EDL);
     }
   }
  else
   {
    //иначе выравниваем по "max правая граница меток контролов" + 2
    for (int i = 0; i < ACtrlList->Count; i++)
     {
      tmpED = ((TdsRegEDItem *)ACtrlList->Items[i]);
      tmpED->SetEDLeft(LabR + 2);
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardOtvodEditForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if ((Key == VK_RETURN) && !InfTypeCB->DroppedDown)
   {
    ActiveControl = OkBtn;
    OkBtnClick(this);
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardOtvodEditForm::OkBtnClick(TObject * Sender)
 {
  TWinControl * tmpCtrl = ActiveControl;
  bool FAllCheck = true;
  for (int i = 0; (i < InfChLB->Items->Count) && FAllCheck; i++)
   {
    FAllCheck &= InfChLB->Items->Items[i]->Checked;
   }
  if (FAllCheck)
   InfTypeCB->ItemIndex = 0;

  ActiveControl = OkBtn;
  ActiveControl = tmpCtrl;
  SetInfList();
  if (CheckInput())
   {//запрос на проверку данных
    try
     {
      UnicodeString PRC = FDataSrc->DataSet->Post(FInsertedRecId, true);
      if (!SameText(PRC, "ok"))
       _MSG_ERR(PRC, "Ошибка");
      else
       ModalResult = mrOk;
     }
    catch (EkabCustomDataSetError & E)
     {
      _MSG_ERR(E.Message, "Ошибка");
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardOtvodEditForm::SetInfList()
 {
  UnicodeString FInfList = "";
  if (InfTypeCB->ItemIndex)
   {
    for (int i = 0; i < InfChLB->Items->Count; i++)
     {
      if (InfChLB->Items->Items[i]->Checked)
       {
        if (FInfList.Length())
         FInfList += ",";
        FInfList += IntToStr((int)InfChLB->Items->Items[i]->ItemObject) + ":" + InfChLB->Items->Items[i]->Text;
       }
     }
   }
  else
   FInfList = "all:Все инфекции";
  RegComp("3083")->SetValue(FInfList);
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCardOtvodEditForm::CheckInput()
 {
  bool RC = false;
  try
   {
    if (FIsAppend)
     FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
    else
     FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
    /*
     TDateTime PBirthDay;
     DM->qtExec("Select R0031 From casebook1 Where CODE="+IntToStr(UCODE));
     if (DM->quFree->RecordCount)
     PBirthDay = quFNDate("R0031");
     else
     {
     MessageBox(Handle,"Ошибка определения даты рождения пациента.","Ошибка",0);
     return false;
     }
     AnsiString xValid = "";
     int OtvodType;
     TDateTime begDate, endDate;
     DM->xmlImmCardDef->GetTagByUID("1030")->Iterate(GetInput,xValid);
     if (!xValid.Length())
     {
     OtvodType = RegComp("1043")->GetValue("").ToIntDef(-1);
     if (OtvodType == -1)
     {
     MessageBox(Handle,"Ошибка определения вида отвода.","Ошибка",0);
     return false;
     }
     else if (OtvodType == 0)
     { // Временный
     if (!TryStrToDate(RegComp("105F")->GetValue("").c_str(), begDate))
     {
     MessageBox(Handle,"Введите корректную дату начала действия отвода","Ошибка",0);
     return false;
     }
     if (!TryStrToDate(RegComp("1065")->GetValue("").c_str(), endDate))
     {
     MessageBox(Handle,"Введите корректную дату окончания действия отвода","Ошибка",0);
     return false;
     }
     if (begDate < PBirthDay)
     {
     MessageBox(Handle,"Дата начала действия отвода не может быть меньше даты рождения пациента.","Ошибка",0);
     return false;
     }
     if (endDate < PBirthDay)
     {
     MessageBox(Handle,"Дата окончания действия отвода не может быть меньше даты рождения пациента.","Ошибка",0);
     return false;
     }
     if (endDate < begDate)
     {
     MessageBox(Handle,"Дата окончания действия отвода не может быть меньше даты начала действия отвода.","Ошибка",0);
     return false;
     }
     }
     else if (OtvodType == 1)
     { // Постоянный
     if (!TryStrToDate(RegComp("105F")->GetValue("").c_str(), begDate))
     {
     MessageBox(Handle,"Введите корректную дату начала действия отвода","Ошибка",0);
     return false;
     }
     if (begDate < PBirthDay)
     {
     MessageBox(Handle,"Дата начала действия отвода не может быть меньше даты рождения пациента.","Ошибка",0);
     return false;
     }
     }
     else
     {
     MessageBox(Handle,"Ошибка определения вида отвода.","Ошибка",0);
     return false;
     }
     return true;
     }
     else
     return false;
     */
    UnicodeString FValid = "";
    int OtvodType;
    TDateTime begDate, endDate;
    FDM->ClassDef->GetTagByUID("3030")->Iterate(GetInput, FValid);
    bool FCont = false;
    if (!FValid.Length())
     {
      RC = false;

      OtvodType = RegComp(_Type_)->GetValue("").ToIntDef(-1);
      if (OtvodType == -1)
       {
        _MSG_ERR("Ошибка определения вида отвода.", "Ошибка");
       }
      else if (OtvodType == 0)
       {//Временный
        if (!TryStrToDate(RegComp(_BegDate_)->GetValue("").c_str(), begDate))
         _MSG_ERR("Введите корректную дату начала действия отвода", "Ошибка");
        else if (!TryStrToDate(RegComp(_EndDate_)->GetValue("").c_str(), endDate))
         _MSG_ERR("Введите корректную дату окончания действия отвода", "Ошибка");
        else if (begDate < FPatBirthday)
         _MSG_ERR("Дата начала действия отвода не может быть меньше даты рождения пациента.", "Ошибка");
        else if (endDate < FPatBirthday)
         _MSG_ERR("Дата окончания действия отвода не может быть меньше даты рождения пациента.", "Ошибка");
        else if (endDate < begDate)
         _MSG_ERR("Дата окончания действия отвода не может быть меньше даты начала действия отвода.", "Ошибка");
        else
         FCont = true;
       }
      else if (OtvodType == 1)
       {//Постоянный
        if (!TryStrToDate(RegComp(_BegDate_)->GetValue("").c_str(), begDate))
         _MSG_ERR("Введите корректную дату начала действия отвода", "Ошибка");
        else if (begDate < FPatBirthday)
         _MSG_ERR("Дата начала действия отвода не может быть меньше даты рождения пациента.", "Ошибка");
        else
         FCont = true;
       }
      else
       _MSG_ERR("Ошибка определения вида отвода.", "Ошибка");

      if (FCont)
       {
        if (!RegComp(_PrivCancel_)->GetValue("").ToIntDef(0) && !RegComp(_TestCancel_)->GetValue("").ToIntDef(0))
         _MSG_ERR("Необходимо указать значение для одного из элементов 'Отвод прививки' или 'Отвод пробы'.", "Ошибка");
        else
         RC = true;
       }
     }
    /*
     UnicodeString xValid = "";
     FClassDef->Iterate(GetInput,xValid);
     if (!xValid.Length())
     {
     RC = true;
     if (FDM->OnExtValidate)
     {
     if (FIsAppend)
     FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
     else
     FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->FN["CODE"], FClassDef, FCtrList, RC);
     }
     }
     */
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCardOtvodEditForm::GetInput(TTagNode * itTag, UnicodeString & AValid)
 {
  if (itTag->CmpAV("uid", _Inf_))
   {
    bool IsSel = false;
    for (int i = 0; i < InfChLB->Items->Count; i++)
     IsSel |= InfChLB->Items->Items[i]->Checked;
    if (!IsSel)
     {
      _MSG_INF("Необходимо выбрать минимум одну инфекцию", "Сообщение");
      AValid = "no";
      return true;
     }
   }
  else
   {
    if (FCtrList->GetEDControl(itTag->AV["uid"]))
     if (!RegComp(itTag->AV["uid"])->CheckReqValue())
      {
       AValid = "no";
       return true;
      }
   }
  return false;
  /*
   if (itTag)
   {
   if (FCtrList->GetEDControl(itTag->AV["uid"]))
   if (!FCtrList->EditItems[itTag->AV["uid"]]->CheckReqValue())
   {
   UID = "no";
   return true;
   }
   }
   return false;
   */
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardOtvodEditForm::FormDestroy(TObject * Sender)
 {
  FCtrList->ClearOnCtrlDataChange(0);
  delete FCtrList->DataProvider;
  delete FCtrList;
  delete FEditInfList;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCardOtvodEditForm::FCtrlDataChange(TTagNode * ItTag, UnicodeString & Src, TkabCustomDataSource * ASource)
 {
  bool RC = false;
  if (FDM->OnCtrlDataChange)
   {
    RC = FDM->OnCtrlDataChange(ItTag, Src, ASource, FIsAppend, FCtrList, UIDInt(FClassDef->AV["uid"]));
   }
  return RC;
 }
//---------------------------------------------------------------------------
int __fastcall TdsCardOtvodEditForm::SetAlign(TdsRegEDItem * AItem, int ALeft, int ATop, int AWidth, int ATabOrder)
 {
  AItem->Left  = ALeft;
  AItem->Width = AWidth;
  AItem->Top   = ATop;
  AItem->Anchors.Clear();
  AItem->Anchors << akLeft << akRight << akTop;
  AItem->TabOrder = ATabOrder;
  return AItem->Height + 4;
 }
//---------------------------------------------------------------------------
inline TdsRegEDItem * __fastcall TdsCardOtvodEditForm::RegComp(UnicodeString  AUID)
 {
  return FCtrList->EditItems[AUID];
 }
//---------------------------------------------------------------------------
inline TdsRegEDItem * __fastcall TdsCardOtvodEditForm::AddRegED(UnicodeString  AUID)
 {
  return FCtrList->AddEditItem(FClassDef->GetTagByUID(AUID), FIsAppend);
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCardOtvodEditForm::FTypeDateChange(TTagNode * ItTag, UnicodeString & Src)
 {
  bool RC = false;
  try
   {
    /*
     //<choice name='Отвод' uid='1043' required='1' inlist='l' default='0'>
     tmpItem = AddRegED(_Type_); SetAlign(tmpItem, _EDLEFT, ATop, 190, FTabOrder++);// tmpItem->SetEDLeft(70);
     tmpItem->OnDataChange = FTypeDateChange;
     */
    OtvodAgeLab->Caption = "";
    TDateTime begDate, endDate;
    if (TryStrToDate(RegComp(_BegDate_)->GetValue(""), begDate))
     {
      if (TryStrToDate(RegComp(_EndDate_)->GetValue(""), endDate))
       {
        OtvodAgeLab->Caption = "Длительность отвода: " + FDM->AgeStr(endDate, begDate);
        RegComp(_Type_)->SetValue("0");
       }
      else
       RegComp(_Type_)->SetValue("1");
     }
    /*
     if (RegComp(_Type_)->GetValue("").ToIntDef(-1) == 0)
     { // Временный
     if (TryStrToDate(RegComp(_BegDate_)->GetValue(""), begDate) && TryStrToDate(RegComp(_EndDate_)->GetValue(""), endDate))
     OtvodAgeLab->Caption = "Длительность отвода: "+FDM->AgeStr(endDate, begDate);
     }
     */
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardOtvodEditForm::InfTypeCBPropertiesChange(TObject * Sender)
 {
  for (int i = 0; i < InfChLB->Items->Count; i++)
   {
    InfChLB->Items->Items[i]->Checked = (InfTypeCB->ItemIndex != 1);
   }
  InfChLB->Enabled = (InfTypeCB->ItemIndex == 1);
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardOtvodEditForm::LoadInfList()
 {
  TTagNode * FSettingNode = new TTagNode;
  TStringList * CalendInfList = new TStringList;
  TStringList * CommInfList = new TStringList;
  try
   {
    FSettingNode->AsXML = FDM->GetPatSetting(FUCode);
    TTagNode * FPersPlanList = FSettingNode->GetChildByName("vacplan", true);
    TTagNode * FPers = NULL;
    TTagNode * FComm = NULL;
    if (FPersPlanList)
     {
      FPers = FPersPlanList->GetChildByName("pers");
      FComm = FPersPlanList->GetChildByName("comm");
     }
    for (TAnsiStrMap::iterator i = FDM->InfList.begin(); i != FDM->InfList.end(); i++)
     {
      if (FDM->InfInPlan(FComm, FPers, i->first))
       CalendInfList->AddObject(i->second, (TObject *)i->first.ToIntDef(0));
      else
       CommInfList->AddObject(i->second, (TObject *)i->first.ToIntDef(0));
     }
    CalendInfList->Sort();
    CommInfList->Sort();
    TcxCheckListBoxItem * tmpChLBItem;
    InfChLB->Items->Clear();
    for (int i = 0; i < CalendInfList->Count; i++)
     {
      tmpChLBItem             = InfChLB->Items->Add();
      tmpChLBItem->Text       = CalendInfList->Strings[i];
      tmpChLBItem->ItemObject = CalendInfList->Objects[i];
     }
    for (int i = 0; i < CommInfList->Count; i++)
     {
      tmpChLBItem             = InfChLB->Items->Add();
      tmpChLBItem->Text       = CommInfList->Strings[i];
      tmpChLBItem->ItemObject = CommInfList->Objects[i];
     }
   }
  __finally
   {
    delete CalendInfList;
    delete CommInfList;
    delete FSettingNode;
   }
 }
//---------------------------------------------------------------------------

