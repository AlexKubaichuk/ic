﻿//---------------------------------------------------------------------------
#ifndef dsCardClientUnitH
#define dsCardClientUnitH

#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtonEdit.hpp"
#include "cxButtons.hpp"
#include "cxClasses.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxData.hpp"
#include "cxDataStorage.hpp"
#include "cxEdit.hpp"
#include "cxFilter.hpp"
#include "cxGraphics.hpp"
#include "cxGrid.hpp"
#include "cxGridCustomTableView.hpp"
#include "cxGridCustomView.hpp"
#include "cxGridLevel.hpp"
#include "cxGridTableView.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxNavigator.hpp"
#include "cxPC.hpp"
#include "cxSplitter.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxStatusBar.hpp"
#include "Htmlview.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "dsICCardDMUnit.h"
#include "KabCustomDS.h"
#include "cxDropDownEdit.hpp"
#include "cxGridCardView.hpp"
#include "cxGridCustomLayoutView.hpp"
#include "cxCalendar.hpp"
#include "cxMaskEdit.hpp"
//---------------------------------------------------------------------------
class PACKAGE TdsCardClientForm : public TForm
{
__published:	// IDE-managed Components
        TPopupMenu *PMenu;
        TMenuItem *InsertItem;
        TMenuItem *ModifyItem;
        TMenuItem *DeleteItem;
        TdxBarManager *ClassTBM;
        TdxBarButton *ResetTemplateBI;
 TcxStyleRepository *Style;
        TcxStyle *BGStyle;
        TdxStatusBar *StatusSB;
        TActionList *ClassAL;
        TAction *actInsert;
        TAction *actEdit;
        TAction *actDelete;
        TAction *actSetFilter;
        TAction *actReSetFilter;
        TdxBarButton *SetFilterBI;
        TAction *actRefresh;
        TAction *actFindNext;
        TdxBarButton *ClassParamBI;
        TdxBarButton *TemplateParamBI;
        TcxStyle *cxStyle2;
        TAction *actGetClassList;
        TdxBarButton *dxBarButton1;
 TAction *actCardSelect;
        TAction *actExit;
        TAction *actTemplate;
        TdxBarButton *SetTemplateBI;
  TdxBarLargeButton *dxBarLargeButton1;
  TdxBarLargeButton *dxBarLargeButton2;
  TdxBarLargeButton *dxBarLargeButton3;
  TdxBarLargeButton *dxBarLargeButton4;
  TdxBarLargeButton *dxBarLargeButton5;
  TdxBarLargeButton *dxBarLargeButton7;
  TdxBarLargeButton *dxBarLargeButton8;
  TdxBarLargeButton *dxBarLargeButton9;
  TdxBarLargeButton *dxBarLargeButton10;
  TcxImageList *LClassIL32;
  TcxImageList *ClassIL;
  TdxBarPopupMenu *QuickFilterMenu;
  TcxTabControl *CardTypePC;
  TcxImageList *CardIL;
  TcxPageControl *CardViewPC;
  TcxTabSheet *F63TS;
  TcxTabSheet *ComTS;
  TPanel *CompPanel;
  TcxPageControl *ViewParamPC;
  TcxTabSheet *ClassParamTS;
  TcxPageControl *ExtListPC;
  TcxSplitter *Split;
  TdxBarDockControl *dxBarDockControl1;
  TPanel *Panel1;
  THTMLViewer *F63Viewer;
  TcxTabSheet *SettingTS;
  TcxStyle *cxStyle1;
 TcxStyle *cxStyle3;
 TPanel *Panel2;
 TcxGrid *ClassGrid;
 TcxGridTableView *ClassGridView;
 TcxGridColumn *ClassGridViewColumn1;
 TcxGridColumn *ClassGridViewColumn2;
 TcxGridColumn *ClassGridViewColumn3;
 TcxGridColumn *ClassGridViewColumn5;
 TcxGridColumn *ClassGridViewColumn4;
 TcxGridLevel *ClassGridLevel1;
 TcxStyle *cxStyle4;
 TcxStyle *cxStyle5;
 TcxTabSheet *PlanTS;
 TcxStyle *FixedColStyle;
 TcxStyle *cxStyle7;
 TcxPageControl *UnitPlanSettingPC;
 TcxTabSheet *SchTS;
 TcxTabSheet *PersPlanTS;
 TcxTreeList *SchSettingTV;
 TcxTreeListColumn *InfNameSchCol;
 TcxTreeListColumn *VacSchCol;
 TcxTreeListColumn *TestSchCol;
 TcxTreeListColumn *TurVacSchCol;
 TcxTreeListColumn *TurTestSchCol;
 TAction *actSaveSchSetting;
 TAction *actLoadSchSetting;
 TPanel *SaveSettingBtnPanel;
 TcxButton *cxButton2;
 TcxTreeList *PersPlanTV;
 TdxStatusBar *UnitDataSB;
 TcxImageList *UnitDataIL;
 TcxTreeListColumn *PersPlanTVColumn1;
 TcxButton *cxButton1;
 TPanel *Panel3;
 TcxButton *cxButton3;
 TcxButton *cxButton4;
 TAction *actAddPlanOptions;
 TAction *actDelPlanOptions;
 TcxTreeListColumn *PersPlanTVColumn2;
 TcxStyle *HeadStyle;
 TcxTreeListColumn *NKSettingTVCol;
 TdxBarButton *dxBarButton2;
 TcxStyle *PlanStyle;
 TcxGrid *PrivGrid;
 TcxGridTableView *PrivGridView;
 TcxGridColumn *cxGridColumn1;
 TcxGridColumn *cxGridColumn2;
 TcxGridColumn *cxGridColumn3;
 TcxGridColumn *cxGridColumn4;
 TcxGridColumn *cxGridColumn5;
 TcxGridLevel *cxGridLevel1;
 TcxSplitter *PrivSplit;
 TButton *Button2;
 TAction *actPrintF63;
 TAction *actInsertF63;
 TdxBarLargeButton *dxBarLargeButton6;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall actInsertExecute(TObject *Sender);
        void __fastcall actEditExecute(TObject *Sender);
        void __fastcall actDeleteExecute(TObject *Sender);
        void __fastcall actRefreshExecute(TObject *Sender);
        void __fastcall actFindNextExecute(TObject *Sender);
        void __fastcall actCardSelectExecute(TObject *Sender);
        void __fastcall actExitExecute(TObject *Sender);
        void __fastcall actTemplateExecute(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
 void __fastcall ClassGridViewFocusedRecordChanged(TcxCustomGridTableView *Sender,
          TcxCustomGridRecord *APrevFocusedRecord, TcxCustomGridRecord *AFocusedRecord,
          bool ANewItemRecordFocusingChanged);
 void __fastcall CardTypePCChange(TObject *Sender);
 void __fastcall CardTypePCChanging(TObject *Sender, bool &AllowChange);
 void __fastcall ClassGridViewColumn1CompareRowValuesForCellMerging(TcxGridColumn *Sender,
          TcxGridDataRow *ARow1, TcxCustomEditProperties *AProperties1,
          const Variant &AValue1, TcxGridDataRow *ARow2,
          TcxCustomEditProperties *AProperties2, const Variant &AValue2,
          bool &AAreEqual);
 void __fastcall ClassGridViewGroupRowCollapsing(TcxGridTableView *Sender, TcxGridGroupRow *AGroup,
          bool &AAllow);
 void __fastcall actSaveSchSettingExecute(TObject *Sender);
 void __fastcall actLoadSchSettingExecute(TObject *Sender);
 void __fastcall VacSchColPropertiesButtonClick(TObject *Sender, int AButtonIndex);
 void __fastcall TestSchColPropertiesButtonClick(TObject *Sender, int AButtonIndex);
 void __fastcall TurVacSchColPropertiesButtonClick(TObject *Sender, int AButtonIndex);
 void __fastcall TurTestSchColPropertiesButtonClick(TObject *Sender, int AButtonIndex);
 void __fastcall actAddPlanOptionsExecute(TObject *Sender);
 void __fastcall actDelPlanOptionsExecute(TObject *Sender);
 void __fastcall PersPlanTVStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
 void __fastcall PersPlanTVCollapsing(TcxCustomTreeList *Sender, TcxTreeListNode *ANode,
          bool &Allow);
 void __fastcall dxBarButton2Click(TObject *Sender);
 void __fastcall SchSettingTVStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
 void __fastcall actPrintF63Execute(TObject *Sender);
 void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
 void __fastcall actInsertF63Execute(TObject *Sender);

private:	// User declarations
  TdsICCardDM *FDM;
  TkabCustomDataSource *FDataSrc;
  TkabCustomDataSource *FPrivDataSrc;
  TTagNode *FPrivNode;
  TAnsiStrMap   FUnitCardCode;
  __int64 FUCode;
  UnicodeString _UID_;
  void __fastcall FGetDataProgress(int AMax, int ACur, UnicodeString AMsg);
  void __fastcall RefreshById(UnicodeString ACode);
  void __fastcall GetData();
  TcxTabSheet *FCurPage;
  TTagNode* __fastcall GetSettingSelNode(TcxTreeList *ASrc);
  void __fastcall FSchButtonClick(int AButtonIndex, int ACol, UnicodeString AAttr);
  int __fastcall InfInPlan(TTagNode *AComRoot, TTagNode *APersRoot, UnicodeString AInf);
  TkabCustomDataSetRow *FUnitData;
  TTagNode *FSettingNode;
  UnicodeString FUnitStr;

  TNotifyEvent FOnCardClose;

  TDate FPatBirthday;
  bool FSetCardId;
        bool SaveEnInsert;
        bool SaveEnEdit;
        bool SaveEnDelete;
        bool SaveEnSetTemplate;
        bool SaveEnReSetTemplate;
        bool SaveEnRefresh;
        bool SaveEnFind;
        bool SaveEnFindNext;
        bool SaveEnViewClassParam;
        bool SaveEnViewTemplateParam;
    __int64    curCD,xpCount,xcTop;
    int     chBDInd;
    TTagNode     *CardRoot;
    TcxTreeListNode *FRootNode;
    TTagNode     *FSelectedNode, *FSelectedNodeDescr;
    TdsRegEDContainer *FCtrList;
    int FLastTop,FLastHeight;
  TList *MergeCompFLNum;

    bool __fastcall FSetEdit(TTagNode *itTag, UnicodeString &UID);
    void __fastcall SetFiltered(bool AVal);

    UnicodeString    SaveCaption;
    void __fastcall DisableAfterScroll();
    void __fastcall EnableAfterScroll();
    TTagNode* __fastcall _CLASSDESCR_();
    TTagNode* __fastcall _CLASSNODE_();
    bool __fastcall GetBtnEn(UnicodeString AAttrName);
    void __fastcall ChangeSrc();
    void __fastcall CreateCardPages();
    bool __fastcall CreateClassTV(TTagNode *itTag, UnicodeString &UID);
    bool __fastcall CreateListExt(TTagNode *itTag, UnicodeString &UID);
    void __fastcall ListAfterScroll(TDataSet *DataSet);
    TcxGridColumn* __fastcall ClassCol(int AIdx);
    void __fastcall ClearTemplate();

  bool __fastcall FGetUnitCode(TTagNode * itTag);
  UnicodeString __fastcall UnitCodeFromCard(UnicodeString AID);
  void __fastcall FGetUnitData();
public:		// User declarations
    __fastcall TdsCardClientForm(TComponent* Owner, TdsICCardDM *ADM, __int64 AUCode, UnicodeString AUnitStr);

  bool __fastcall GetSchData(int ACol, UnicodeString AAttr, UnicodeString &AVal, UnicodeString &AStrVal);
  void __fastcall SetSchData(int ACol, UnicodeString AAttr, UnicodeString AVal, UnicodeString AStrVal);


    void __fastcall SetActiveCard(UnicodeString ACardId);
  __property TNotifyEvent OnCardClose = {read=FOnCardClose, write=FOnCardClose};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsCardClientForm *dsCardClientForm;
//---------------------------------------------------------------------------
#endif
