﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsCardSchListUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxCustomData"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma resource "*.dfm"
TSchemaListForm * SchemaListForm;
// ---------------------------------------------------------------------------
__fastcall TSchemaListForm::TSchemaListForm(TComponent * Owner, TdsICCardDM * ADM, long InfCode, bool isVac,
  UnicodeString AVal, bool ASelectSchOnly) : TForm(Owner)
 {
  FDM            = ADM;
  FSelectSchOnly = ASelectSchOnly;
  FVac           = isVac;
  TTagNodeMap FSchList;
  FDM->GetSchemeList(InfCode, FSchList, isVac);
  TcxTreeListNode * itSchTreeNode;
  TcxTreeListNode * itLineTreeNode;
  TcxTreeListNode * FCurSel = NULL;
  TTagNode * itLine;
  SchemaTL->Clear();
  itSchTreeNode           = SchemaTL->Root->AddChild();
  itSchTreeNode->Data     = (void *)NULL;
  itSchTreeNode->Texts[0] = " Не определено";
  if (AVal == "_NULL_")
   FCurSel = itSchTreeNode;
  for (TTagNodeMap::iterator i = FSchList.begin(); i != FSchList.end(); i++)
   {
    itSchTreeNode       = SchemaTL->Root->AddChild();
    itSchTreeNode->Data = (void *)(i->second);
    if (isVac)
     itSchTreeNode->Texts[0] = FDM->VacList[i->second->AV["ref"]];
    else
     itSchTreeNode->Texts[0] = FDM->ProbList[i->second->AV["ref"]];
    itSchTreeNode->Texts[0] = itSchTreeNode->Texts[0] + " схема '" + i->second->AV["name"] + "'";
    itLine                  = i->second->GetFirstChild();
    while (itLine)
     {
      if (itLine->CmpName("line"))
       {
        itLineTreeNode           = itSchTreeNode->AddChild();
        itLineTreeNode->Data     = (void *)itLine;
        itLineTreeNode->Texts[0] = ((itLine->AV["name"][1] == 'V') ? " " : "") + itLine->AV["name"];
        if (!FSelectSchOnly)
         {
          if (AVal == i->second->AV["uid"] + "." + itLine->AV["uid"])
           FCurSel = itLineTreeNode;
         }
       }
      else if (itLine->CmpName("begline"))
       {
        itLineTreeNode           = itSchTreeNode->AddChild();
        itLineTreeNode->Data     = (void *)itLine;
        itLineTreeNode->Texts[0] = "Начало схемы";
        if (!FSelectSchOnly)
         {
          if (AVal == i->second->AV["uid"] + "." + itLine->AV["uid"])
           FCurSel = itLineTreeNode;
         }
       }
      itLine = itLine->GetNext();
     }
    if (FSelectSchOnly)
     {
      if (AVal == i->second->AV["uid"])
       FCurSel = itSchTreeNode;
     }
   }
  if (FCurSel)
   {
    FCurSel->Focused = true;
    FCurSel->MakeVisible();
   }
  else
   SchemaTL->Items[0]->Focused = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TSchemaListForm::OkBtnClick(TObject * Sender)
 {
  if (SchemaTL->SelectionCount)
   {
    TTagNode * tmpNode;
    if (!SchemaTL->Selections[0]->Data)
     Value = "_NULL_=-";
    else
     {
      if (FSelectSchOnly)
       {
        if (SchemaTL->Selections[0]->Level)
         return;
        tmpNode = (TTagNode *)SchemaTL->Selections[0]->Data;
        Value   = tmpNode->AV["uid"] + "=" + SchemaTL->Selections[0]->Texts[0];
       }
      else
       {
        if (!SchemaTL->Selections[0]->Level)
         return;
        tmpNode = (TTagNode *)SchemaTL->Selections[0]->Data;
        if (tmpNode->CmpName("begline"))
         Value = "{" + tmpNode->GetParent()->AV["name"] + ".Начало схемы} ";
        else
         Value = "{" + tmpNode->GetParent()->AV["name"] + "." + tmpNode->AV["name"] + "} ";
        if (FVac)
         Value += FDM->VacList[tmpNode->GetParent()->AV["ref"]];
        else
         Value += FDM->ProbList[tmpNode->GetParent()->AV["ref"]];
        Value = tmpNode->GetParent()->AV["uid"] + "." + tmpNode->AV["uid"] + "=" + Value;
       }
     }
    ModalResult = mrOk;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TSchemaListForm::SchemaTLStylesGetContentStyle(TcxCustomTreeList * Sender, TcxTreeListColumn * AItem,
  TcxTreeListNode * ANode, TcxStyle *& AStyle)
 {
  if (ANode)
   {
    if (!FSelectSchOnly)
     {
      if ((ANode->Level == 1) || !ANode->Data)
       AStyle = BoldStyle;
      else
       AStyle = NoBoldStyle;
     }
    else
     {
      if ((ANode->Level != 1) || !ANode->Data)
       AStyle = BoldStyle;
      else
       AStyle = NoBoldStyle;
     }
   }
 }
// ---------------------------------------------------------------------------
