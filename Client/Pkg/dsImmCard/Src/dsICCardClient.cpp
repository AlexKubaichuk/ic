﻿//---------------------------------------------------------------------------

#include <basepch.h>

#pragma hdrstop

#include "dsICCardClient.h"

//#include "DxLocale.h"

#include "dsICCardDMUnit.h"
//#include "ICSRegList.h"
//#include "ICSRegSelList.h"
//#include "Reg_DMF.h"
//#include "ICSRegProgress.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TdsICCardClient *)
{
  new TdsICCardClient(NULL);
}
//---------------------------------------------------------------------------
__fastcall TdsICCardClient::TdsICCardClient(TComponent* Owner)
        : TComponent(Owner)
{
  FUseClassDisControls = false;
  FUseReportSetting = false;
  FDBUser = "";
  FDBPassword ="";
  FDBCharset = "";
  RegistryKey = "Software\\ICS Ltd.\\ClassifSize";
  FUnitListCreated = NULL;
  FBuyCaption = "";
//  FLargeIcons = false;
//  FFetchAll = true;
  FUnitListDeleted = NULL;
  FOnCtrlDataChange = NULL;

  FRefreshList = false;
  FClassTreeColor = clBtnFace;
  FDTFormat = "dd.mm.yyyy";
  FDM = new TdsICCardDM(this);
  FActive = true;
  FClassEnabled = true;

}
//---------------------------------------------------------------------------
__fastcall TdsICCardClient::~TdsICCardClient()
{
  for (TICCardMap::iterator i = FCardMap.begin(); i != FCardMap.end(); i++)
   delete i->second;
  FCardMap.clear();
  Active = false;
  delete FDM;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetFullEdit(bool AFullEdit)
{
  FFullEdit = AFullEdit;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetActive(bool AActive)
{
 FActive = AActive;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetClassEnabled(bool AClassEnabled)
{
/*
  FClassEnabled = AClassEnabled;
  if (FDM)
   if (FDM->UnitList) ((TICSRegListForm*)FDM->UnitList)->CheckAccess();
*/
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetListEditEnabled(bool AListEditEnabled)
{
/*
  FListEditEnabled = AListEditEnabled;
  if (FDM)
   if (FDM->UnitList) ((TICSRegListForm*)FDM->UnitList)->CheckAccess();
*/
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOpenCardEnabled(bool AOpenCardEnabled)
{
/*
  FOpenCardEnabled = AOpenCardEnabled;
  if (FDM)
   if (FDM->UnitList) ((TICSRegListForm*)FDM->UnitList)->CheckAccess();
*/
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetUListTop(int ATop)
{
  FUListTop = ATop;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetUListLeft(int ALeft)
{
  FUListLeft = ALeft;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetDBName(UnicodeString  ADBName)
{
  FDBName = ADBName;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetXMLName(UnicodeString  AXMLName)
{
  FXMLName = AXMLName;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::ShowCard(__int64 AUCode, TNotifyEvent AProc, UnicodeString AUnitStr)
{
  if (!FActive) return;
  if (!FClassEnabled) return;
  TICCardMap::iterator FRC = FCardMap.find(AUCode);
  if (FRC == FCardMap.end())
   {
     FCardMap[AUCode] = new TdsCardClientForm(Application->Owner, FDM, AUCode, AUnitStr);
   }
  FCardMap[AUCode]->OnCardClose = AProc;
  FCardMap[AUCode]->SetActiveCard("1003");
  FCardMap[AUCode]->Show();
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::ShowPrePlan(__int64 AUCode, UnicodeString AUnitStr)
{
  if (!FActive) return;
  if (!FClassEnabled) return;
  TICCardMap::iterator FRC = FCardMap.find(AUCode);
  if (FRC == FCardMap.end())
   {
     FCardMap[AUCode] = new TdsCardClientForm(Application->Owner, FDM, AUCode, AUnitStr);
   }
  FCardMap[AUCode]->SetActiveCard("1112");
  FCardMap[AUCode]->Show();
}
//---------------------------------------------------------------------------
void  __fastcall TdsICCardClient::FSetInqComp(TControl *AInqComp)
{
  FInqComp  = AInqComp;
}
//---------------------------------------------------------------------------
/*
bool  __fastcall TdsICCardClient::GetSelected(__int64 *ACode, UnicodeString &AUnitStr)
{
  if (!IsListShowing()) return false;
  if (FDM->UnitList)
   {
     __int64 XXCode = ((TICSRegListForm*)FDM->UnitList)->GetSelCode();
     if (XXCode == -1) return false;
     *ACode   = XXCode;
     AUnitStr = ((TICSRegListForm*)FDM->UnitList)->GetSelStr();
     return true;
   }
  return false;
}
//---------------------------------------------------------------------------
*/
/*
void __fastcall TdsICCardClient::GetSelectedUnits(TStrings *ADest)
{
  if (!IsListShowing()) return;
  if (FDM->UnitList)
   ((TICSRegListForm*)FDM->UnitList)->GetSelectedUnits(ADest);
}
//---------------------------------------------------------------------------
*/
bool __fastcall TdsICCardClient::Connect(UnicodeString ADBName, UnicodeString AXMLName)
{
/*
   try
    {
      if (!FActive) return false;
      try
       {
         if (!FDM) FDM = new TdsRegDM(this);
       }
      catch (Exception &E)
       {
         MessageBox(NULL,FMT1(icsRegistryErrorLoadDM,E.Message).c_str(),FMT(icsErrorMsgCaption).c_str(),MB_ICONSTOP);
         throw;
       }
      if (!FDM) return false;
      FDM->RegDef  = new TTagNode(NULL);
      FDM->SelRegDef  = new TTagNode(NULL);
      UnicodeString fPar;
      FDM->REG_DB->DBParams->Clear();
      fPar = (FDBUser.Length())?FDBUser:UnicodeString("SYSDBA");
      FDM->REG_DB->DBParams->Add(("user_name="+fPar).LowerCase().c_str());
      fPar = (FDBPassword.Length())?FDBPassword:UnicodeString("MASTERKEY");
      FDM->REG_DB->DBParams->Add(("password="+fPar).LowerCase().c_str());
      FDM->REG_DB->DBParams->Add("sql_role_name=");
      if (FDBCharset.Length())
       FDM->REG_DB->DBParams->Add(("lc_ctype="+FDBCharset).LowerCase().c_str());

      FDM->REG_DB->DBName = ADBName;
      FDM->REG_DB->Open();

      if (FileExists(AXMLName))
       {
         FDM->RegDef->LoadFromFile(AXMLName);
         FDM->SelRegDef->LoadFromFile(AXMLName);
       }
      else
       {
         if (FLoadExtXML)
          {
            FLoadExtXML(FDM->RegDef, FDM->SelRegDef);
          }
         else
          return false;
       }
      return true;
    }
   catch(EFIBInterBaseError& E)
    {
      if (E.SQLCode == -902)
       {
         MessageBox(NULL,FMT1(icsRegistryErrorDBNotExist,ADBName).c_str(),FMT1(icsRegistryErrorDBErrorMsgCaption,IntToStr(E.SQLCode)).c_str(),MB_ICONSTOP);
         return false;
       }
      else if (E.SQLCode == -904)
       {
         MessageBox(NULL,FMT(icsRegistryErrorNoServer).c_str(),FMT1(icsRegistryErrorDBErrorMsgCaption,IntToStr(E.SQLCode)).c_str(),MB_ICONSTOP);
         return false;
       }
      else
       {
         MessageBox(NULL,E.Message.c_str(),FMT1(icsRegistryErrorDBErrorMsgCaption,IntToStr(E.SQLCode)).c_str(),MB_ICONSTOP);
         return false;
       }
    }
  catch (...)
    {
      MessageBox(NULL,FMT(icsRegistryErrorLoadRegDef).c_str(),FMT(icsErrorMsgCaption).c_str(),MB_ICONSTOP);
      return false;
    }
*/
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::GetTreeChoice(TList *AList)
{
  tmpChTree = new TList;
  FDM->RegDef->Iterate(GetTreeCh,*&UnicodeString(""));
  AList->Assign(tmpChTree,laCopy,NULL);
  if (tmpChTree) delete tmpChTree;
  tmpChTree = NULL;
}
//---------------------------------------------------------------------------
bool __fastcall TdsICCardClient::GetTreeCh(TTagNode *itTag, UnicodeString &UID)
{
   if (itTag->CmpName("choiceTree"))
    tmpChTree->Add(itTag);
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetInit(int APersent, UnicodeString AMess)
{
/*
  if (APersent == 0) ((TICSRegProgressForm*)PrF)->SetCaption(AMess);
  ((TICSRegProgressForm*)PrF)->SetPercent(APersent);
*/
}
//---------------------------------------------------------------------------
/*
TdxBar*  __fastcall TdsICCardClient::GetListExtTB(void)
{
  if (FDM->UnitList) return ((TICSRegListForm*)FDM->UnitList)->GetExtTB();
  else              return NULL;
}
//---------------------------------------------------------------------------
*/
void __fastcall TdsICCardClient::FSetDTFormat(UnicodeString  AParam)
{
  FDTFormat = AParam;
}
//---------------------------------------------------------------------------
bool __fastcall TdsICCardClient::FGetCardEditable()
{
  bool RC = false;
  try
   {
     if (FDM)
      RC = FDM->CardEditable;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetCardEditable(bool AVal)
{
  if (FDM)
    FDM->CardEditable = AVal;
}
//---------------------------------------------------------------------------
UnicodeString  __fastcall TdsICCardClient::FGetClassShowExpanded()
{
  UnicodeString RC = "";
  try
   {
     if (FDM)
      RC = FDM->ClassShowExpanded;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetClassShowExpanded(UnicodeString AVal)
{
  if (FDM)
    FDM->ClassShowExpanded = AVal;
}
//---------------------------------------------------------------------------
TExtEditData __fastcall TdsICCardClient::FGetExtInsertData()
{

  if (FDM)
   return FDM->OnExtInsertData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetExtInsertData(TExtEditData AValue)
{
  if (FDM)
   FDM->OnExtInsertData = AValue;
}
//---------------------------------------------------------------------------
TExtEditData __fastcall TdsICCardClient::FGetExtEditData()
{

  if (FDM)
   return FDM->OnExtEditData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetExtEditData(TExtEditData AValue)
{
  if (FDM)
   FDM->OnExtEditData = AValue;
}
//---------------------------------------------------------------------------
TClassEditEvent __fastcall TdsICCardClient::FGetClassAfterInsert()
{
  if (FDM)
   return FDM->OnClassAfterInsert;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetClassAfterInsert(TClassEditEvent AValue)
{
  if (FDM)
   FDM->OnClassAfterInsert = AValue;
}
//---------------------------------------------------------------------------
TClassEditEvent __fastcall TdsICCardClient::FGetClassAfterEdit()
{
  if (FDM)
   return FDM->OnClassAfterEdit;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetClassAfterEdit(TClassEditEvent AValue)
{
  if (FDM)
   FDM->OnClassAfterEdit = AValue;
}
//---------------------------------------------------------------------------
TClassEditEvent __fastcall TdsICCardClient::FGetClassAfterDelete()
{
  if (FDM)
   return FDM->OnClassAfterDelete;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetClassAfterDelete(TClassEditEvent AValue)
{
  if (FDM)
   FDM->OnClassAfterDelete = AValue;
}
//---------------------------------------------------------------------------
TUnitEvent __fastcall TdsICCardClient::FGetOnOpenCard()
{
  if (FDM)
   return FDM->OnOpenCard;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnOpenCard(TUnitEvent AValue)
{
  if (FDM)
   FDM->OnOpenCard = AValue;
}
//---------------------------------------------------------------------------
TGetReportListEvent __fastcall TdsICCardClient::FGetQuickFilterList()
{
  if (FDM)
   return FDM->OnQuickFilterList;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetQuickFilterList(TGetReportListEvent AValue)
{
  if (FDM)
   FDM->OnQuickFilterList = AValue;
}
//---------------------------------------------------------------------------
TGetReportListEvent __fastcall TdsICCardClient::FGetReportList()
{
  if (FDM)
   return FDM->OnReportList;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetReportList(TGetReportListEvent AValue)
{
  if (FDM)
   FDM->OnReportList = AValue;
}
//---------------------------------------------------------------------------
TUnitEvent __fastcall TdsICCardClient::FGetPrintReportClick()
{
  if (FDM)
   return FDM->OnPrintReportClick;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetPrintReportClick(TUnitEvent AValue)
{
  if (FDM)
   FDM->OnPrintReportClick = AValue;
}
//---------------------------------------------------------------------------
//TdsICClassClient* __fastcall TdsICCardClient::FGetConnection()
//{
//  if (FDM)
//   return FDM->Connection;
//  else
//   return NULL;
//}
//---------------------------------------------------------------------------
//void __fastcall TdsICCardClient::FSetConnection(TdsICClassClient* AValue)
//{
//  if (FDM)
//   FDM->Connection = AValue;
//}
//---------------------------------------------------------------------------
TTagNode* __fastcall TdsICCardClient::FGetRegDef()
{
  if (FDM)
   return FDM->RegDef;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetRegDef(TTagNode* AValue)
{
  if (FDM)
   FDM->RegDef = AValue;
}
//---------------------------------------------------------------------------
TExtBtnClick __fastcall TdsICCardClient::FGetExtBtnClick()
{
  if (FDM)
   return FDM->OnExtBtnClick;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetExtBtnClick(TExtBtnClick AValue)
{
  if (FDM)
   FDM->OnExtBtnClick = AValue;
}
//---------------------------------------------------------------------------
TcxEditStyleController* __fastcall TdsICCardClient::FGetStyleController()
{
  if (FDM)
   return FDM->StyleController;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetStyleController(TcxEditStyleController* AValue)
{
  if (FDM)
   FDM->StyleController = AValue;
}
//---------------------------------------------------------------------------
TdsGetClassXMLEvent __fastcall TdsICCardClient::FGetOnGetClassXML()
{
  if (FDM)
   return FDM->OnGetClassXML;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnGetClassXML(TdsGetClassXMLEvent AValue)
{
  if (FDM)
   FDM->OnGetClassXML = AValue;
}
//---------------------------------------------------------------------------
TdsGetCountEvent __fastcall TdsICCardClient::FGetOnGetCount()
{
  if (FDM)
   return FDM->OnGetCount;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnGetCount(TdsGetCountEvent AValue)
{
  if (FDM)
   FDM->OnGetCount = AValue;
}
//---------------------------------------------------------------------------
TdsGetIdListEvent __fastcall TdsICCardClient::FGetOnGetIdList()
{
  if (FDM)
   return FDM->OnGetIdList;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnGetIdList(TdsGetIdListEvent AValue)
{
  if (FDM)
   FDM->OnGetIdList = AValue;
}
//---------------------------------------------------------------------------
TdsGetValByIdEvent __fastcall TdsICCardClient::FGetOnGetValById()
{
  if (FDM)
   return FDM->OnGetValById;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnGetValById(TdsGetValByIdEvent AValue)
{
  if (FDM)
   FDM->OnGetValById = AValue;
}
//---------------------------------------------------------------------------
TdsGetValById10Event __fastcall TdsICCardClient::FGetOnGetValById10()
{
  if (FDM)
   return FDM->OnGetValById10;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnGetValById10(TdsGetValById10Event AValue)
{
  if (FDM)
   FDM->OnGetValById10 = AValue;
}
//---------------------------------------------------------------------------
TdsFindEvent __fastcall TdsICCardClient::FGetOnFind()
{
  if (FDM)
   return FDM->OnFind;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnFind(TdsFindEvent AValue)
{
  if (FDM)
   FDM->OnFind = AValue;
}
//---------------------------------------------------------------------------
TdsInsertDataEvent __fastcall TdsICCardClient::FGetOnInsertData()
{
  if (FDM)
   return FDM->OnInsertData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnInsertData(TdsInsertDataEvent AValue)
{
  if (FDM)
   FDM->OnInsertData = AValue;
}
//---------------------------------------------------------------------------
TdsEditDataEvent __fastcall TdsICCardClient::FGetOnEditData()
{
  if (FDM)
   return FDM->OnEditData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnEditData(TdsEditDataEvent AValue)
{
  if (FDM)
   FDM->OnEditData = AValue;
}
//---------------------------------------------------------------------------
TdsDeleteDataEvent __fastcall TdsICCardClient::FGetOnDeleteData()
{
  if (FDM)
   return FDM->OnDeleteData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnDeleteData(TdsDeleteDataEvent AValue)
{
  if (FDM)
   FDM->OnDeleteData = AValue;
}
//---------------------------------------------------------------------------
TdsGetF63Event __fastcall TdsICCardClient::FGetOnGetF63()
{
  if (FDM)
   return FDM->OnGetF63;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnGetF63(TdsGetF63Event AValue)
{
  if (FDM)
   FDM->OnGetF63 = AValue;
}
//---------------------------------------------------------------------------
TdsGetVacDefDozeEvent __fastcall TdsICCardClient::FGetOnGetVacDefDoze()
{
  if (FDM)
   return FDM->OnGetVacDefDoze;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnGetVacDefDoze(TdsGetVacDefDozeEvent AValue)
{
  if (FDM)
   FDM->OnGetVacDefDoze = AValue;
}
//---------------------------------------------------------------------------
TAxeXMLContainer* __fastcall TdsICCardClient::FGetXMLList()
{
  if (FDM)
   return FDM->XMLList;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetXMLList(TAxeXMLContainer *AVal)
{
  if (FDM)
   FDM->XMLList = AVal;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::PreloadData()
{
  if (FDM)
    FDM->PreloadData();
}
//---------------------------------------------------------------------------
TdsICCardGetUnitDataEvent __fastcall TdsICCardClient::FGetOnGetUnitData()
{
  if (FDM)
   return FDM->OnGetUnitData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnGetUnitData(TdsICCardGetUnitDataEvent AValue)
{
  if (FDM)
   FDM->OnGetUnitData = AValue;
}
//---------------------------------------------------------------------------
TdsGetUnitSettingEvent __fastcall TdsICCardClient::FGetOnGetPatSetting()
{
  if (FDM)
   return FDM->OnGetPatSetting;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnGetPatSetting(TdsGetUnitSettingEvent AValue)
{
  if (FDM)
   FDM->OnGetPatSetting = AValue;
}
//---------------------------------------------------------------------------
TdsSetUnitSettingEvent __fastcall TdsICCardClient::FGetOnSetPatSetting()
{
  if (FDM)
   return FDM->OnSetPatSetting;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnSetPatSetting(TdsSetUnitSettingEvent AValue)
{
  if (FDM)
   FDM->OnSetPatSetting = AValue;
}
//---------------------------------------------------------------------------
TdsGetUnitSettingEvent __fastcall TdsICCardClient::FGetOnGetPrivVar()
{
  if (FDM)
   return FDM->OnGetPrivVar;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnGetPrivVar(TdsGetUnitSettingEvent AValue)
{
  if (FDM)
   FDM->OnGetPrivVar = AValue;
}
//---------------------------------------------------------------------------
TdsGetUnitSettingEvent __fastcall TdsICCardClient::FGetOnGetOtvodInf()
{
  if (FDM)
   return FDM->OnGetOtvodInf;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnGetOtvodInf(TdsGetUnitSettingEvent AValue)
{
  if (FDM)
   FDM->OnGetOtvodInf = AValue;
}
//---------------------------------------------------------------------------
TdsCreateUnitPlanEvent __fastcall TdsICCardClient::FGetOnCreatePlan()
{
  if (FDM)
   return FDM->OnCreatePlan;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnCreatePlan(TdsCreateUnitPlanEvent AValue)
{
  if (FDM)
   FDM->OnCreatePlan = AValue;
}
//---------------------------------------------------------------------------
__int64 __fastcall TdsICCardClient::FGetLPUCode()
{
  if (FDM)
   return FDM->LPUCode;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetLPUCode(__int64 AValue)
{
  if (FDM)
   FDM->LPUCode = AValue;
}
//---------------------------------------------------------------------------
TExtValidateData __fastcall TdsICCardClient::FGetOnGetDefValues()
{
  if (FDM)
   return FDM->OnGetDefValues;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnGetDefValues(TExtValidateData AValue)
{
  if (FDM)
   FDM->OnGetDefValues = AValue;
}
//---------------------------------------------------------------------------
TdsHTMLPreviewEvent __fastcall TdsICCardClient::FGetOnHTMLPreview()
{
  if (FDM)
   return FDM->OnHTMLPreview;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnHTMLPreview(TdsHTMLPreviewEvent AValue)
{
  if (FDM)
   FDM->OnHTMLPreview = AValue;
}
//---------------------------------------------------------------------------
bool __fastcall TdsICCardClient::PlanPrivInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData *ATmplData, bool ASaveTmpl)
{
  bool RC = false;
  if (FDM)
   RC = FDM->PlanPrivInsert(AUCode, APatBirthday, ATmplData, ASaveTmpl);
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsICCardClient::PlanTestInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData *ATmplData, bool ASaveTmpl)
{
  bool RC = false;
  if (FDM)
   RC = FDM->PlanTestInsert(AUCode, APatBirthday, ATmplData, ASaveTmpl);
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsICCardClient::PlanCheckInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData *ATmplData, bool ASaveTmpl)
{
  bool RC = false;
  if (FDM)
   RC = FDM->PlanCheckInsert(AUCode, APatBirthday, ATmplData, ASaveTmpl);
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsICCardClient::PlanCancelInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData *ATmplData, bool ASaveTmpl)
{
  bool RC = false;
  if (FDM)
   RC = FDM->PlanCancelInsert(AUCode, APatBirthday, ATmplData, ASaveTmpl);
  return RC;
}
//---------------------------------------------------------------------------
//UnicodeString __fastcall TdsICCardClient::FGetXMLPath()
//{
//  if (FDM)
//   return FDM->XMLPath;
//  else
//   return "";
//}
////---------------------------------------------------------------------------
//void __fastcall TdsICCardClient::FSetXMLPath(UnicodeString AVal)
//{
//  if (FDM)
//   FDM->XMLPath = AVal;
//}
////---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardClient::FGetDataPath()
{
  if (FDM)
   return FDM->DataPath;
  else
   return "";
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetDataPath(UnicodeString AVal)
{
  if (FDM)
   FDM->DataPath = AVal;
}
//---------------------------------------------------------------------------
TdsGetListByIdEvent __fastcall TdsICCardClient::FGetOnGetListById()
{
  if (FDM)
   return FDM->OnGetListById;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnGetListById(TdsGetListByIdEvent AVal)
{
  if (FDM)
   FDM->OnGetListById = AVal;
}
//---------------------------------------------------------------------------
TColor __fastcall TdsICCardClient::FGetReqColor()
{
  if (FDM)
   return FDM->RequiredColor;
  else
   return clInfoBk;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetReqColor(TColor AVal)
{
  if (FDM)
   FDM->RequiredColor = AVal;
}
//---------------------------------------------------------------------------
TExtValidateData __fastcall TdsICCardClient::FGetOnExtValidate()
{
  if (FDM)
   return FDM->OnExtValidate;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnExtValidate(TExtValidateData AVal)
{
  if (FDM)
   FDM->OnExtValidate = AVal;
}
//---------------------------------------------------------------------------
TdsGetPlanVacVarEvent __fastcall TdsICCardClient::FGetOnGetPlanVacVar()
{
  if (FDM)
   return FDM->OnGetPlanVacVar;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICCardClient::FSetOnGetPlanVacVar(TdsGetPlanVacVarEvent AVal)
{
  if (FDM)
   FDM->OnGetPlanVacVar = AVal;
}
//---------------------------------------------------------------------------

