object dsCardPrivEditForm: TdsCardPrivEditForm
  Left = 369
  Top = 357
  HelpContext = 1011
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = ':)'
  ClientHeight = 467
  ClientWidth = 463
  Color = clBtnFace
  Constraints.MinHeight = 110
  Constraints.MinWidth = 298
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = [fsBold]
  GlassFrame.Bottom = 45
  OldCreateOrder = True
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 426
    Width = 463
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      463
      41)
    object OkBtn: TcxButton
      Left = 295
      Top = 9
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = OkBtnClick
    end
    object CancelBtn: TcxButton
      Left = 379
      Top = 9
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object CompPanel: TPanel
    Left = 0
    Top = 0
    Width = 463
    Height = 426
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 5
    UseDockManager = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object AdmBl: TBevel
      Left = 0
      Top = 328
      Width = 457
      Height = 3
      Shape = bsTopLine
    end
    object InfPanel: TPanel
      Left = 3
      Top = 41
      Width = 454
      Height = 187
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 0
      object NewVariantGrid: TcxTreeList
        Left = 2
        Top = 48
        Width = 450
        Height = 137
        Align = alClient
        Bands = <
          item
          end>
        Navigator.Buttons.CustomButtons = <>
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.ConfirmDelete = False
        OptionsBehavior.DragCollapse = False
        OptionsBehavior.DragExpand = False
        OptionsBehavior.ExpandOnDblClick = False
        OptionsBehavior.MultiSort = False
        OptionsBehavior.ShowHourGlass = False
        OptionsCustomizing.BandCustomizing = False
        OptionsCustomizing.BandHorzSizing = False
        OptionsCustomizing.BandMoving = False
        OptionsCustomizing.BandVertSizing = False
        OptionsCustomizing.ColumnHorzSizing = False
        OptionsCustomizing.ColumnMoving = False
        OptionsCustomizing.ColumnVertSizing = False
        OptionsData.CancelOnExit = False
        OptionsData.AnsiSort = True
        OptionsData.Deleting = False
        OptionsView.ScrollBars = ssVertical
        OptionsView.ShowEditButtons = ecsbFocused
        OptionsView.Buttons = False
        OptionsView.GridLines = tlglBoth
        OptionsView.Headers = False
        OptionsView.ShowRoot = False
        TabOrder = 1
        OnKeyDown = FormKeyDown
        Data = {
          00000500980100000F00000044617461436F6E74726F6C6C6572310300000012
          000000546378537472696E6756616C7565547970651200000054637853747269
          6E6756616C75655479706512000000546378537472696E6756616C7565547970
          6509000000445855464D5401445855464D5401445855464D5401445855464D54
          01445855464D5401445855464D5401445855464D5401445855464D5401445855
          464D5401090000000000000008080000000000000000FFFFFFFFFFFFFFFFFFFF
          FFFF0100000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF02000000
          08080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF03000000080800000000
          00000000FFFFFFFFFFFFFFFFFFFFFFFF0400000008080000000000000000FFFF
          FFFFFFFFFFFFFFFFFFFF0500000008080000000000000000FFFFFFFFFFFFFFFF
          FFFFFFFF0600000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0700
          000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0800000008080000
          000000000000FFFFFFFFFFFFFFFFFFFFFFFF1A0809000000}
        object InfCol: TcxTreeListColumn
          Caption.AlignHorz = taCenter
          Caption.Text = #1048#1085#1092#1077#1082#1094#1080#1103
          DataBinding.ValueType = 'String'
          Options.CellEndEllipsis = False
          Options.Sizing = False
          Options.Customizing = False
          Options.Editing = False
          Options.Focusing = False
          Options.IncSearch = False
          Options.Moving = False
          Options.ShowEditButtons = eisbNever
          Options.Sorting = False
          Options.TabStop = False
          Width = 150
          Position.ColIndex = 0
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object VacVarCol: TcxTreeListColumn
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.CaseInsensitive = False
          Properties.ClearKey = 16430
          Properties.DropDownListStyle = lsFixedList
          Properties.HideSelection = False
          Properties.ImmediatePost = True
          Properties.ImmediateUpdateText = True
          Properties.Items.Strings = (
            'V1'
            'V2'
            'V3'
            'RV1/1'
            #1044#1086#1087'.')
          Properties.OnCloseUp = VacVarColPropertiesCloseUp
          Caption.AlignHorz = taCenter
          Caption.ShowEndEllipsis = False
          Caption.Text = #1042#1080#1076' '#1074#1072#1082#1094#1080#1085#1072#1094#1080#1080
          DataBinding.ValueType = 'String'
          Options.CellEndEllipsis = False
          Options.Sizing = False
          Options.Customizing = False
          Options.Moving = False
          Options.ShowEditButtons = eisbAlways
          Options.Sorting = False
          Width = 275
          Position.ColIndex = 1
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object CurrSchCol: TcxTreeListColumn
          Visible = False
          Caption.AlignHorz = taCenter
          Caption.Text = #1058#1077#1082#1091#1097#1072#1103' '#1089#1093#1077#1084#1072
          DataBinding.ValueType = 'String'
          Options.CellEndEllipsis = False
          Options.Sizing = False
          Options.Customizing = False
          Options.Editing = False
          Options.Focusing = False
          Options.IncSearch = False
          Options.Moving = False
          Options.ShowEditButtons = eisbNever
          Options.Sorting = False
          Options.TabStop = False
          Width = 20
          Position.ColIndex = 2
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
      end
      object VacSchPanel: TPanel
        Left = 2
        Top = 2
        Width = 450
        Height = 46
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object SchLab: TLabel
          Left = 2
          Top = 6
          Width = 35
          Height = 13
          Caption = #1057#1093#1077#1084#1072':'
          Color = clCream
          ParentColor = False
        end
        object SchValLab: TLabel
          Left = 43
          Top = 6
          Width = 402
          Height = 13
          AutoSize = False
          Color = clCream
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object ExtSchChB: TcxCheckBox
          Left = 2
          Top = 21
          Caption = #1056#1072#1089#1096#1080#1088#1077#1085#1085#1072#1103' '#1091#1089#1090#1072#1085#1086#1074#1082#1072' '#1074#1080#1076#1086#1074' '#1074#1072#1082#1094#1080#1085#1072#1094#1080#1081
          TabOrder = 0
          OnClick = ExtSchChBClick
          OnKeyDown = FormKeyDown
          Width = 255
        end
      end
    end
  end
end
