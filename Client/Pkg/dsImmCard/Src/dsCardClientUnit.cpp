﻿//---------------------------------------------------------------------------
#include <vcl.h>
#include <SysUtils.hpp>
#pragma hdrstop
#include "dsCardClientUnit.h"
#include "dsCardEditClientUnit.h"
#include "dsCardPrivEditUnit.h"
#include "dsCardOtvodEditUnit.h"
#include "dsCardTestEditUnit.h"
#include "dsCardCheckEditUnit.h"
#include "dsCardSchListUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "dsInsertF63TestUnit.h"
//#include "pkgsetting.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxButtons"
#pragma link "cxClasses"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxData"
#pragma link "cxDataStorage"
#pragma link "cxEdit"
#pragma link "cxFilter"
#pragma link "cxGraphics"
#pragma link "cxGrid"
#pragma link "cxGridCustomTableView"
#pragma link "cxGridCustomView"
#pragma link "cxGridLevel"
#pragma link "cxGridTableView"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxNavigator"
#pragma link "cxPC"
#pragma link "cxSplitter"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxStatusBar"
#pragma link "Htmlview"
#pragma link "cxDropDownEdit"
#pragma link "cxGridCardView"
#pragma link "cxGridCustomLayoutView"
#pragma link "cxCalendar"
#pragma link "cxMaskEdit"
#pragma resource "*.dfm"
TdsCardClientForm * dsCardClientForm;
//---------------------------------------------------------------------------
__fastcall TdsCardClientForm::TdsCardClientForm(TComponent * Owner, TdsICCardDM * ADM, __int64 AUCode,
    UnicodeString AUnitStr) : TForm(Owner)
 {
  FDM                     = ADM;
  FUCode                  = AUCode;
  SaveEnInsert            = false;
  SaveEnEdit              = false;
  SaveEnDelete            = false;
  SaveEnSetTemplate       = false;
  SaveEnReSetTemplate     = false;
  SaveEnRefresh           = false;
  SaveEnFind              = false;
  SaveEnFindNext          = false;
  SaveEnViewClassParam    = false;
  SaveEnViewTemplateParam = false;
  FDataSrc                = NULL;
  FPrivDataSrc            = NULL;
  MergeCompFLNum          = new TList;
  FSettingNode            = NULL;
  _UID_                   = "";
  FPrivNode               = NULL;
  FUnitData               = NULL;
  FUnitStr                = AUnitStr;
  FSetCardId              = false;
  //FGetUnitData();

  /*actInsert->Caption = FMT(icsRegClassifInsertCaption);
   actInsert->Hint = FMT(icsRegClassifInsertHint);
   if (FMT(icsRegClassifInsertShortCut).Length())
   actInsert->ShortCut = TextToShortCut(FMT(icsRegClassifInsertShortCut));

   actEdit->Caption = FMT(icsRegClassifEditCaption);
   actEdit->Hint = FMT(icsRegClassifEditHint);
   if (FMT(icsRegClassifEditShortCut).Length())
   actEdit->ShortCut = TextToShortCut(FMT(icsRegClassifEditShortCut));

   actDelete->Caption = FMT(icsRegClassifDeleteCaption);
   actDelete->Hint = FMT(icsRegClassifDeleteHint);
   if (FMT(icsRegClassifDeleteShortCut).Length())
   actDelete->ShortCut = TextToShortCut(FMT(icsRegClassifDeleteShortCut));

   //  actSetTemplate->Caption = FMT(icsRegClassifSetTemplateCaption);
   //  actSetTemplate->Hint = FMT(icsRegClassifSetTemplateHint);
   //  if (FMT(icsRegClassifSetTemplateShortCut).Length())
   //   actSetTemplate->ShortCut = TextToShortCut(FMT(icsRegClassifSetTemplateShortCut));

   //  actReSetTemplate->Caption = FMT(icsRegClassifReSetTemplateCaption);
   //  actReSetTemplate->Hint = FMT(icsRegClassifReSetTemplateHint);
   //  if (FMT(icsRegClassifReSetTemplateShortCut).Length())
   //   actReSetTemplate->ShortCut = TextToShortCut(FMT(icsRegClassifReSetTemplateShortCut));

   actRefresh->Caption = FMT(icsRegClassifRefreshCaption);
   actRefresh->Hint = FMT(icsRegClassifRefreshHint);
   if (FMT(icsRegClassifRefreshShortCut).Length())
   actRefresh->ShortCut = TextToShortCut(FMT(icsRegClassifRefreshShortCut));

   actFind->Caption = FMT(icsRegClassifFindCaption);
   actFind->Hint = FMT(icsRegClassifFindHint);
   if (FMT(icsRegClassifFindShortCut).Length())
   actFind->ShortCut = TextToShortCut(FMT(icsRegClassifFindShortCut));

   actFindNext->Caption = FMT(icsRegClassifFindNextCaption);
   actFindNext->Hint = FMT(icsRegClassifFindNextHint);
   if (FMT(icsRegClassifFindNextShortCut).Length())
   actFindNext->ShortCut = TextToShortCut(FMT(icsRegClassifFindNextShortCut));

   //  actViewClassParam->Caption = FMT(icsRegClassifViewClassParamCaption);
   //  actViewClassParam->Hint = FMT(icsRegClassifViewClassParamHint);
   //  if (FMT(icsRegClassifViewClassParamShortCut).Length())
   //   actViewClassParam->ShortCut = TextToShortCut(FMT(icsRegClassifViewClassParamShortCut));

   //  actViewTemplateParam->Caption = FMT(icsRegClassifViewTemplateParamCaption);
   //  actViewTemplateParam->Hint = FMT(icsRegClassifViewTemplateParamHint);
   //  if (FMT(icsRegClassifViewTemplateParamShortCut).Length())
   //   actViewTemplateParam->ShortCut = TextToShortCut(FMT(icsRegClassifViewTemplateParamShortCut));*/
  /*StatusSB->Panels->Items[0]->Text = FMT(icsCommonRecCount);*/
  ViewParamPC->ActivePage = ClassParamTS;
  //FGroups["card"] = "1";
  //FGroups["0"] = "1";
  //BGStyle->Color = LtColor(clBtnFace,16);
  if (FDM->DefaultClassWidth)
   Width = FDM->DefaultClassWidth;
  CardRoot = FDM->ClassDef->GetChildByAV("classform", "group", "card", true);
  CardRoot->Iterate1st(FGetUnitCode);
  FCtrList         = new TdsRegEDContainer(this, ExtListPC);
  FCtrList->DefXML = FDM->ClassDef;
  //FCtrList->FIBTemplateQuery = FDM->xquFree;
  //FCtrList->ClassData        = FDM->FClassData;
  FCtrList->StyleController = FDM->StyleController;
  FCtrList->DataPath        = FDM->DataPath;
  FCtrList->DataProvider    = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DefXML = FDM->ClassDef;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  FLastTop                              = 0;
  FLastHeight                           = 0;
  //SaveCaption = FDM->PrefixCaption.Trim();
  CreateCardPages();
  curCD = 0;
  //ClassTBM->Images = FDM->Images;
  //ClassTBM->LargeImages = FDM->LargeImages;
  //ClassTBM->HotImages = FDM->HotImages;
  //ClassTBM->DisabledImages = FDM->DisabledImages;
  //ClassTBM->LargeIcons = FDM->UseLargeImages;
  //ClassAL->Images = FDM->Images;
  //PMenu->Images = FDM->Images;
  int AllItemsCount = 0;
  for (int i = 0; i < ClassTBM->Bars->Count; i++)
   {
    AllItemsCount += ClassTBM->Bars->Items[i]->ItemLinks->Count;
   }
  int FCalcWidth = 0;
  if (FDM->FullEdit)
   {
    //CardRoot->Iterate(FSetEdit,*&UnicodeString(""));
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::FGetUnitData()
 {
  FPatBirthday = Date();
  UnicodeString FPatFam = "ошибка";
  UnicodeString FPatName = "ошибка";
  UnicodeString FPatSName = "ошибка";
  if (FDM->OnGetUnitData)
   {
    if (FDM->OnGetUnitData(FUCode, FUnitData))
     {
      FPatBirthday = TDate(VarToStr(FUnitData->Value["0031"]));
      FPatFam      = VarToStr(FUnitData->Value["001F"]);
      FPatName     = VarToStr(FUnitData->Value["0020"]);
      FPatSName    = VarToStr(FUnitData->Value["002F"]);
     }
   }
  UnicodeString FPatFIO = FPatFam;
  if (FPatName.Length())
   FPatFIO += " " + UnicodeString(FPatName[1]) + ".";
  if (FPatSName.Length())
   FPatFIO += " " + UnicodeString(FPatSName[1]) + ".";
  if (FUnitStr.Length())
   SaveCaption = FUnitStr + " [" + IntToStr(FUCode) + "] - ";
  else
   SaveCaption = FPatFIO + " [" + IntToStr(FUCode) + "] - ";
  UnitDataSB->Panels->Items[0]->Text = FPatFIO;
  UnitDataSB->Panels->Items[1]->Text = FPatBirthday.FormatString("dd.mm.yyyy г.");
  UnitDataSB->Panels->Items[2]->Text = FDM->AgeStr(Date(), FPatBirthday);
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCardClientForm::FSetEdit(TTagNode * itTag, UnicodeString & UID)
 {
  if (itTag->GetAttrByName("isedit"))
   itTag->AV["isedit"] = "1";
  return false;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCardClientForm::FGetUnitCode(TTagNode * itTag)
 {
  if (itTag->CmpName("class"))
   {
    TTagNode * UnitRef = itTag->GetChildByAV("extedit", "ref", "1000", true);
    if (UnitRef)
     FUnitCardCode[itTag->AV["uid"].UpperCase()] = UnitRef->AV["uid"].UpperCase();
   }
  return false;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsCardClientForm::UnitCodeFromCard(UnicodeString AID)
 {
  UnicodeString RC = "";
  try
   {
    TAnsiStrMap::iterator FRC = FUnitCardCode.find(AID.UpperCase());
    if (FRC != FUnitCardCode.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::SetActiveCard(UnicodeString ACardId)
 {
  if (ACardId.Length())
   {
    UnicodeString FCardUID;
    for (int i = 0; i <= CardTypePC->Properties->Tabs->Count; i++)
     {
      if (CardTypePC->Properties->Tabs->Objects[i])
       {
        FCardUID = UIDStr((int)CardTypePC->Properties->Tabs->Objects[i]).UpperCase();
        if (FCardUID == ACardId.UpperCase())
         {
          FSelectedNode        = CardRoot->GetTagByUID(FCardUID);
          FSelectedNodeDescr   = FSelectedNode->GetChildByName("description");
          CardTypePC->TabIndex = i;
          FSetCardId           = true;
          break;
         }
       }
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::ChangeSrc()
 {
  //PrivView->BeginUpdate();
  try
   {
    try
     {
      actInsertF63->Enabled = false;
      Caption               = SaveCaption + " " + _CLASSNODE_()->AV["name"];
      FCtrList->ClearLabItems();
      bool IsCellMerging = false;
      MergeCompFLNum->Clear();
      IsCellMerging = _CLASSDESCR_()->GetChildByName("compfields");
      if (!_CLASSDESCR_()->GetChildByName("maskfields"))
       {
        actTemplate->Enabled    = false;
        actSetFilter->Enabled   = false;
        actReSetFilter->Enabled = false;
       }
      else
       {
        actTemplate->Enabled    = actInsert->Enabled && actEdit->Enabled && actDelete->Enabled;
        actSetFilter->Enabled   = true;
        actReSetFilter->Enabled = true;
       }
      bool CanFind = false;
      TTagNode * UnitNode = _CLASSDESCR_()->GetFirstChild()->GetFirstChild();
      while (UnitNode)
       {
        if (UnitNode->GetTagByUID(UnitNode->AV["ref"]))
         {
          if (UnitNode->GetTagByUID(UnitNode->AV["ref"])->CmpName("text,digit"))
           {
            CanFind = true;
            break;
           }
         }
        UnitNode = UnitNode->GetNext();
       }
      //actFind->Enabled     = CanFind;
      ClassGridView->DataController->CustomDataSource = NULL;
      PrivGridView->DataController->CustomDataSource = NULL;
      //if (_CLASSNODE_()->CmpAV("uid","1003"))
      //PrivView->DataController->CustomDataSource = NULL;
      if (FDataSrc)
       delete FDataSrc;
      if (_CLASSNODE_()->CmpAV("uid", "1003") && FPrivNode)
       {
        if (UnitCodeFromCard("3003").Length())
         {
          if (FPrivDataSrc)
           delete FPrivDataSrc;
          FPrivDataSrc                          = new TkabCustomDataSource(FPrivNode);
          FPrivDataSrc->DataSet->IdPref         = "card";
          FPrivDataSrc->DataSet->OnGetCount     = FDM->kabCDSGetCount;
          FPrivDataSrc->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
          FPrivDataSrc->DataSet->OnGetValById   = FDM->kabCDSGetValById;
          FPrivDataSrc->DataSet->OnGetValById10 = FDM->kabCDSGetValById10;
          FPrivDataSrc->DataSet->OnInsert       = FDM->kabCDSInsert;
          FPrivDataSrc->DataSet->OnEdit         = FDM->kabCDSEdit;
          FPrivDataSrc->DataSet->OnDelete       = FDM->kabCDSDelete;
          FPrivDataSrc->DataSet->GetData("{\"" + UnitCodeFromCard("3003") + "\":\"" + IntToStr(FUCode) + "\"}",
              TdsRegFetch::All, 20, FGetDataProgress);
         }
       }
      FDataSrc = new TkabCustomDataSource(_CLASSNODE_());
      FDataSrc->DataSet->IdPref         = "card";
      FDataSrc->DataSet->OnGetCount     = FDM->kabCDSGetCount;
      FDataSrc->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
      FDataSrc->DataSet->OnGetValById   = FDM->kabCDSGetValById;
      FDataSrc->DataSet->OnGetValById10 = FDM->kabCDSGetValById10;
      FDataSrc->DataSet->OnInsert       = FDM->kabCDSInsert;
      FDataSrc->DataSet->OnEdit         = FDM->kabCDSEdit;
      FDataSrc->DataSet->OnDelete       = FDM->kabCDSDelete;
      GetData();
      if (_CLASSNODE_()->CmpAV("uid", "1003"))
       {
        PrivGrid->Visible = true;
        PrivSplit->OpenSplitter();
        FDataSrc->CreateColumns(ClassGridView,
            "1024:Дата\n10B4:Возраст\n1021:Вид\n1020:МИБП\n1075\n1022:Доза\n1023:Серия\n10A8:Формат\n1027:Значение");
        //FPrivDataSrc->CreateColumns(PrivGridView, "3024:Дата\n30B4:Возраст\n3021:Вид\n3020:МИБП\n3185\n3022:Доза\n3023:Серия");
        FPrivDataSrc->CreateColumns(PrivGridView,
            "3024:Дата\n30B4:Возраст\n3021:Вид\n3020:МИБП\n3022:Доза\n3023:Серия\n30A8:Формат\n3027:Значение");
        PrivGridView->Columns[0]->SortOrder = soAscending;
        PrivGridView->Columns[0]->Options->SortByDisplayText = isbtOff;
        PrivGridView->DataController->CustomDataSource = FPrivDataSrc;
        FPrivDataSrc->DataChanged();
       }
      else if (_CLASSNODE_()->CmpAV("uid", "1112"))
       {
        //PrivGrid->Visible = false;
        PrivSplit->CloseSplitter();
        FDataSrc->CreateColumns(ClassGridView,
//            "1115:Дата\n31A2:Плановый период\n1116:Тип\n1117:Вид/препарат\n3187:Инфекция\n1118:По схеме\n1119:Приоритет\n111C:Источник");
            "1115:Дата\n31A2:Плановый период\n1116:Тип\n1117:Вид/препарат\n3187:Инфекция\n1118:По схеме\n111C:Источник");
        //ClassGridView->Columns[0]->SortOrder = soDescending;
        //ClassGridView->Columns[0]->Options->SortByDisplayText = isbtOff;
        ClassGridView->Columns[0]->SortOrder = soAscending;
        //ClassGridView->Columns[1]->Options->SortByDisplayText = isbtOff;
        //FPrivDataSrc->CreateColumns(PrivGridView, "3024:Дата\n30B4:Возраст\n3021:Вид\n3020:МИБП\n3185\n3022:Доза\n3023:Серия");
        //PrivGridView->Columns[0]->SortOrder = soAscending;
        //PrivGridView->DataController->CustomDataSource = FPrivDataSrc;
        //FPrivDataSrc->DataChanged();
       }
      else
       {
        PrivGrid->Visible = true;
        PrivSplit->CloseSplitter();
        FDataSrc->CreateColumns(ClassGridView);
        if (IsCellMerging)
         {
          for (int i = 0; i < ClassGridView->ColumnCount; i++)
           {
            TTagNode * FCompFields = _CLASSDESCR_()->GetChildByName("compfields");
            if (FCompFields->GetChildByAV("field", "ref", FDataSrc->GetColIdByIdx(i)))
             MergeCompFLNum->Add((void *)i);
            ClassGridView->Columns[i]->Options->CellMerging = true;
            ClassGridView->Columns[i]->OnCompareRowValuesForCellMerging =
                ClassGridViewColumn1CompareRowValuesForCellMerging;
           }
         }
       }
      ClassGridView->DataController->CustomDataSource = FDataSrc;
      FDataSrc->DataChanged();
      if (_CLASSNODE_()->CmpAV("uid", "1003"))
       {
        //FDataSrc->CreateColumns(PrivView, "1024\n10B4\n1020\n1021\n1075");
        //PrivView->DataController->CustomDataSource = FDataSrc;
        ClassGridView->Columns[4]->GroupIndex = 0;
        ClassGridView->Columns[4]->Visible    = false;
        //PrivView->ViewData->Expand(true);
       }
      else if (_CLASSNODE_()->CmpAV("uid", "1112"))
       {
        ClassGridView->Columns[1]->GroupIndex = 0;
        ClassGridView->Columns[1]->Visible    = false;
       }
      //if (!quClass()->Transaction->Active)
      //quClass()->Transaction->StartTransaction();
      //quClass()->Prepare();
      //quClass()->Open();
      if (curCD)
       {
        TLocateOptions opt;
        //if(!quClass()->Locate("CODE",Variant(curCD),opt))
        //quClass()->First();
       }
      //else
      //quClass()->First();
      ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DataSource = FDataSrc;
      actInsert->Enabled = FDM->CardEditable && GetBtnEn("insertlabel"); //(FDM->FullEdit | GetBtnEn("insertlabel"))
      if (FDataSrc->DataSet->RecordCount && (FDataSrc->CurrentRecIdx != -1))
       {
        actEdit->Enabled   = FDM->CardEditable && GetBtnEn("editlabel"); //(FDM->FullEdit | GetBtnEn("editlabel"))
        actDelete->Enabled = FDM->CardEditable && GetBtnEn("deletelabel"); //(FDM->FullEdit | GetBtnEn("deletelabel"))
       }
      else
       {
        actEdit->Enabled   = false;
        actDelete->Enabled = false;
       }
      xcTop = 1;
      int FPC = ExtListPC->PageCount - 1;
      for (int i = FPC; i >= 0; i--)
       {
        FCurPage              = ExtListPC->Pages[i];
        FCurPage->PageControl = NULL;
        delete FCurPage;
       }
      _CLASSNODE_()->Iterate(CreateListExt, *& UnicodeString(""));
      if (!FCtrList->LabCount)
       Split->CloseSplitter();
      else
       Split->OpenSplitter();
      if (CardTypePC->TabIndex == 1)
       {//прививки
        actInsertF63->Enabled = false;
       }
      else if (CardTypePC->TabIndex == 2)
       {//пробы
        actInsertF63->Enabled = true;
       }

      StatusSB->Panels->Items[0]->Text = CorrectCount(FDataSrc->DataSet->RecordCount);
      //if (SetTmpl->Visible)
      //StatusSB->Panels->Items[3]->Text = CorrectCount(FDataSrc->DataSet->RecordCount);
      //else
      //StatusSB->Panels->Items[3]->Text = "";
      ActiveControl = ClassGrid;
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsErrorMsgCaptionSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
      //if (quClass()->Transaction->Active)
      //quClass()->Transaction->Rollback();
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegistryErrorCommDBErrorMsgCaption), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
      //if (quClass()->Transaction->Active)
      //quClass()->Transaction->Rollback();
     }
   }
  __finally
   {
    Split->CloseSplitter();
    //PrivView->EndUpdate();
    if (_CLASSNODE_()->CmpAV("uid", "1003,1112"))
     {
      //ClassGridView->        PrivView->ViewData->Expand(true);
      ClassGridView->ViewData->Expand(true);
     }
    //if (quClass()->Active)
    //{
    //if (FDM->UseAfterScroll)
    //quClass()->AfterScroll  = ListAfterScroll;
    //else
    //quClass()->OnEndScroll  = ListAfterScroll;
    //ListAfterScroll(quClass());
    //}
    //if (FDM->UseClassDisControls)
    //quClass()->EnableControls();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::FGetDataProgress(int AMax, int ACur, UnicodeString AMsg)
 {
  if (AMax == -1)
   StatusSB->Panels->Items[1]->Text = "";
  else if (!(AMax + ACur))
   StatusSB->Panels->Items[1]->Text = AMsg;
  else
   StatusSB->Panels->Items[1]->Text = AMsg + " [" + IntToStr(ACur) + "/" + IntToStr(AMax) + "]";
  Application->ProcessMessages();
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_INSERT && actInsert->Enabled)
   actInsert->OnExecute(this);
  if (Key == VK_RETURN && actEdit->Enabled && Shift.Contains(ssShift))
   actEdit->OnExecute(this);
  if (Key == VK_DELETE && actDelete->Enabled)
   actDelete->OnExecute(this);
  if ((Key == VK_TAB) && Shift.Contains(ssCtrl))
   {
    if (CardTypePC->TabIndex < CardTypePC->Tabs->Count)
     CardTypePC->TabIndex++ ;
    else
     CardTypePC->TabIndex = 0;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::CreateCardPages()
 {
  CardTypePC->Properties->Tabs->Clear();
  CardTypePC->Properties->Tabs->AddObject("[&0] Форма № 63", (TObject *)0);
  CardTypePC->Properties->Tabs->AddObject("[&1] Прививки", (TObject *)UIDInt("1003"));
  CardTypePC->Properties->Tabs->AddObject("[&2] Пробы", (TObject *)UIDInt("102f"));
  CardTypePC->Properties->Tabs->AddObject("[&3] Проверки", (TObject *)UIDInt("1100"));
  CardTypePC->Properties->Tabs->AddObject("[&4] Отводы", (TObject *)UIDInt("3030"));
  CardTypePC->Properties->Tabs->AddObject("[&5] План", (TObject *)UIDInt("1112"));
  CardTypePC->Properties->Tabs->AddObject("[&6] Настройки", (TObject *)0);
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCardClientForm::CreateClassTV(TTagNode * itTag, UnicodeString & UID)
 {
  TcxTreeListNode * FNode;
  if (itTag->CmpName("class"))
   {
    UnicodeString InList = itTag->GetAVDef("inlist", "n").LowerCase();
    /*
     if (!FDM->FullEdit)
     {
     break;
     }
     */
    FNode           = FRootNode->AddChild();
    FNode->Texts[0] = itTag->AV["name"];
    FNode->Data     = itTag;
   }
  return false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::FormDestroy(TObject * Sender)
 {
  if (FSettingNode)
   delete FSettingNode;
  ClearTemplate();
  if (FCtrList)
   {
    if (FCtrList->DataProvider)
     delete FCtrList->DataProvider;
    delete FCtrList;
   }
  FCtrList = NULL;
  delete MergeCompFLNum;
  if (FDataSrc)
   delete FDataSrc;
  if (FPrivDataSrc)
   delete FPrivDataSrc;
  //if (quClass()->Active) quClass()->Close();
  //if (quClass()->Transaction->Active) quClass()->Transaction->Rollback();
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::ListAfterScroll(TDataSet * DataSet)
 {
  FCtrList->LabDataChange();
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCardClientForm::CreateListExt(TTagNode * itTag, UnicodeString & UID)
 {
  /*
   if (itTag->GetParent()->CmpName("unit"))
   {
   TcxTabSheet *Page;
   ExtPanelList->Add((void*) new TPanel(this));
   CurExtList = (TPanel*)ExtPanelList->Items[ExtPanelList->Count-1];
   Page = new TcxTabSheet(this);
   Page->Caption = itTag->AV["name"];
   Page->PageControl = ExtListPC;
   Page->Color = clInactiveCaption;
   Page->ImageIndex = Page->TabIndex;
   CurExtList->Parent = Page;
   CurExtList->BevelInner = bvNone;
   CurExtList->BevelOuter = bvNone;
   CurExtList->Width = ExtListPC->Width - 4;
   CurExtList->Align = alClient;
   xcTop = 0;
   }
   }*/
  if ((itTag->CmpName("group") && itTag->GetParent()->CmpName("unit")) || itTag->CmpName("class"))
   {
    //TcxTabSheet *Page;
    //ExtPanelList->Add((void*) new TPanel(this));
    //CurExtList = (TPanel*)ExtPanelList->Items[ExtPanelList->Count-1];
    FCurPage              = new TcxTabSheet(this);
    FCurPage->Caption     = itTag->AV["name"];
    FCurPage->PageControl = ExtListPC;
    FCurPage->Color       = clBtnFace;
    FCurPage->BorderWidth = 3;
    FCurPage->ImageIndex  = FCurPage->TabIndex;
    xcTop                 = 0;
   }
  TdsRegLabItem * tmpItem;
  if (!itTag->CmpName("class,description,actuals"))
   {
    if (itTag->AV["inlist"].LowerCase() == "e")
     {
      tmpItem      = FCtrList->AddLabItem(itTag, FCurPage->Width, FCurPage);
      tmpItem->Top = xcTop;
      xcTop += tmpItem->Height;
      //if (itTag->CmpName("extedit"))
      //tmpItem->OnGetExtData = FDM->OnGetExtData;
     }
   }
  return false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::actInsertExecute(TObject * Sender)
 {
  if (!actInsert->Enabled)
   return;
  //TdsCardEditClientForm *Dlg = NULL;
  TTagNode * EditNode = NULL;
  __int64 FSaveCode = 0;
  bool CanRefresh, CanCancel, CanCont;
  try
   {
    CanRefresh = true;
    CanCancel  = true;
    DisableAfterScroll();
    CanCont = true;
    bool CanReopen = false;
    try
     {
      if (FDM->OnExtInsertData)
       {
        __int64 FCode;
        FDM->OnExtInsertData(EditNode, FDataSrc, 0, CanCont, CanReopen, FCode);
        if (CanReopen)
         curCD = FCode;
       }
      if (CanCont)
       {
        if (_UID_ == "1003")
         {//прививки
          TdsCardPrivEditForm * Dlg = new TdsCardPrivEditForm(this, true, FUCode, FDM, _CLASSNODE_()->GetRoot(),
              FPatBirthday, FDataSrc, FCtrList->TemplateData);
          try
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
          __finally
           {
            delete Dlg;
           }
         }
        else if (_UID_ == "102F")
         {//пробы
          TdsCardTestEditForm * Dlg = new TdsCardTestEditForm(this, true, FUCode, FDM, _CLASSNODE_()->GetRoot(),
              FPatBirthday, FDataSrc, FCtrList->TemplateData);
          try
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
          __finally
           {
            delete Dlg;
           }
         }
        else if (_UID_ == "1100")
         {//проверки
          TdsCardCheckEditForm * Dlg = new TdsCardCheckEditForm(this, true, _CLASSNODE_()->AV["uid"], FUCode, FDM,
              _CLASSNODE_()->GetRoot(), FPatBirthday, FDataSrc, FCtrList->TemplateData);
          try
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
          __finally
           {
            delete Dlg;
           }
         }
        else if (_UID_ == "3030")
         {//отводы
          TdsCardOtvodEditForm * Dlg = new TdsCardOtvodEditForm(this, true, _CLASSNODE_()->AV["uid"], FUCode, FDM,
              _CLASSNODE_()->GetRoot(), FPatBirthday, FDataSrc, FCtrList->TemplateData);
          try
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
          __finally
           {
            delete Dlg;
           }
         }
        else
         {//остальное
          TdsCardEditClientForm * Dlg = new TdsCardEditClientForm(this, true, _CLASSNODE_()->AV["uid"], FUCode, FDM,
              _CLASSNODE_()->GetRoot(), FDataSrc, FCtrList->TemplateData);
          try
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
          __finally
           {
            delete Dlg;
           }
         }
       }
      else
       {
        CanRefresh = CanReopen;
        CanCancel  = false;
       }
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsRegErrorInsertRecSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegErrorInsertRec), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
    if (CanCancel)
     {
      FDataSrc->DataSet->Cancel();
     }
    if (CanRefresh)
     {
      if (FDM->OnClassAfterInsert)
       FDM->OnClassAfterInsert(_CLASSNODE_());
      if (_UID_ == "1003")
       actRefreshExecute(actRefresh);
      else
       RefreshById(curCD);
     }
    EnableAfterScroll();
   }
  if (_CLASSNODE_()->CmpAV("uid", "1003,1112"))
   {
    ClassGridView->ViewData->Expand(true);
   }
  ActiveControl = ClassGrid;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::actEditExecute(TObject * Sender)
 {
  if (!actEdit->Enabled)
   return;
  TTagNode * EditNode = NULL;
  //TdsCardEditClientForm *Dlg = NULL;
  bool CanRefresh, CanCancel, CanCont;
  try
   {
    CanRefresh = true;
    CanCancel  = true;
    DisableAfterScroll();
    EditNode = new TTagNode(NULL);
    EditNode->Assign(_CLASSNODE_(), true);
    try
     {
      CanCont = true;
      bool CanReopen = false;
      if (FDM->OnExtEditData)
       {
        __int64 FCode;
        FDM->OnExtEditData(EditNode, FDataSrc, FDataSrc->DataSet->CurRow->Value["CODE"], CanCont, CanReopen, FCode);
        if (CanReopen)
         curCD = FCode;
       }
      if (CanCont)
       {
        //Dlg = new TdsCardEditClientForm(this, false, _CLASSNODE_()->AV["uid"], FDM,_CLASSNODE_()->GetRoot(), FDataSrc, FCtrList->TemplateData);
        if (_UID_ == "1003")
         {//прививки
          TdsCardPrivEditForm * Dlg = new TdsCardPrivEditForm(this, false, FUCode, FDM, _CLASSNODE_()->GetRoot(),
              FPatBirthday, FDataSrc, FCtrList->TemplateData);
          try
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
          __finally
           {
            delete Dlg;
           }
         }
        else if (_UID_ == "102F")
         {//пробы
          TdsCardTestEditForm * Dlg = new TdsCardTestEditForm(this, false, FUCode, FDM, _CLASSNODE_()->GetRoot(),
              FPatBirthday, FDataSrc, FCtrList->TemplateData);
          try
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
          __finally
           {
            delete Dlg;
           }
         }
        else if (_UID_ == "1100")
         {//проверки
          TdsCardCheckEditForm * Dlg = new TdsCardCheckEditForm(this, false, _CLASSNODE_()->AV["uid"], FUCode, FDM,
              _CLASSNODE_()->GetRoot(), FPatBirthday, FDataSrc, FCtrList->TemplateData);
          try
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
          __finally
           {
            delete Dlg;
           }
         }
        else if (_UID_ == "3030")
         {//отводы
          TdsCardOtvodEditForm * Dlg = new TdsCardOtvodEditForm(this, false, _CLASSNODE_()->AV["uid"], FUCode, FDM,
              _CLASSNODE_()->GetRoot(), FPatBirthday, FDataSrc, FCtrList->TemplateData);
          try
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
          __finally
           {
            delete Dlg;
           }
         }
        else
         {//остальное
          TdsCardEditClientForm * Dlg = new TdsCardEditClientForm(this, false, _CLASSNODE_()->AV["uid"], FUCode, FDM,
              _CLASSNODE_()->GetRoot(), FDataSrc, FCtrList->TemplateData);
          try
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
          __finally
           {
            delete Dlg;
           }
         }
       }
      else
       {
        CanRefresh = CanReopen;
        CanCancel  = false;
       }
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsRegErrorEditRecSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegErrorEditRec), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
    //if (Dlg) delete Dlg;
    if (EditNode)
     delete EditNode;
    if (CanCancel)
     {
      FDataSrc->DataSet->Cancel();
     }
    if (CanRefresh)
     {
      if (FDM->OnClassAfterEdit)
       FDM->OnClassAfterEdit(_CLASSNODE_());
      if (_UID_ == "1003")
       {
        actRefreshExecute(actRefresh);
       }
      else
       RefreshById(curCD);
     }
    EnableAfterScroll();
   }
  if (_CLASSNODE_()->CmpAV("uid", "1003,1112"))
   {
    ClassGridView->ViewData->Expand(true);
   }
  ActiveControl = ClassGrid;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::actDeleteExecute(TObject * Sender)
 {
  /*
   if (FDM->OnRegEditButtonClick)
   {
   if (!FDM->OnRegEditButtonClick(bcDelete)) return;
   }

   __int64 xfCode;
   TTagNode* EditNode = NULL;
   if (!quClass()->Active) return;
   */
  if (!actDelete->Enabled)
   return;
  /*
   if (!quClass()->Transaction->Active) return;
   if(quClass()->RecordCount < 1) return;
   */
  bool CanRefresh, CanCancel, CanCont;
  try
   {
    CanRefresh = true;
    CanCancel  = true;
    DisableAfterScroll();
    try
     {
      //xfCode = (__int64)quClass()->Value("CODE")->AsInteger;
      CanCont = true;
      if (CanCont)
       {
        //curCD = quClass()->Value("CODE")->AsInteger;
        int FRecIdx = FDataSrc->CurrentRecIdx;
        UnicodeString FFlId;
        UnicodeString Str = "";
        if (FRecIdx >= 0)
         {
          if (_CLASSDESCR_()->AV["confirmlabel"].Trim().Length())
           {//есть confirmlabel;
            TStringList * FConfMsgList = new TStringList;
            try
             {
              FConfMsgList->Delimiter     = '#';
              FConfMsgList->QuoteChar     = '~';
              FConfMsgList->DelimitedText = _CLASSDESCR_()->AV["confirmlabel"].Trim();
              for (int i = 0; i < FConfMsgList->Count; i++)
               {
                if (FConfMsgList->Strings[i].Length())
                 {
                  if (FConfMsgList->Strings[i][1] == '@')
                   {
                    FFlId = FConfMsgList->Strings[i].SubString(2, 4);
                    //if (_CLASSDESCR_()->GetTagByUID(FFlId)->CmpName("binary,choice,choiceDB,extedit,choiceTree"))
                    Str += " " + FDataSrc->DataSet->CurRow->StrValue[FFlId];
                    //else
                    //Str += " "+FDataSrc->DataSet->CurRow->Value[FFlId] ;
                   }
                  else
                   {
                    Str += " " + FConfMsgList->Strings[i];
                   }
                 }
               }
             }
            __finally
             {
              delete FConfMsgList;
             }
           }
          else
           {
            TTagNode * itFl = FDataSrc->DataSet->ListDefNode->GetFirstChild();
            while (itFl)
             {
              if (Str.Length())
               Str += ", ";
              //if (_CLASSDESCR_()->GetTagByUID(itFl->AV["uid"])->CmpName("binary,choice,choiceDB,extedit,choiceTree"))
              Str += FDataSrc->DataSet->CurRow->StrValue[itFl->AV["uid"]];
              //else
              //Str += FDataSrc->DataSet->CurRow->Value[itFl->AV["uid"]];
              itFl = itFl->GetNext();
             }
            Str = FMT(icsRegErrorConfirmDeleteRec) + " " + _CLASSDESCR_()->AV["deletelabel"].LowerCase() + "\n\n\" " +
                Str + "\"\n";
           }
          /*
           TTagNode *itFl = FDataSrc->DataSet->ListDefNode->GetFirstChild();
           while (itFl)
           {
           if (Str.Length())
           Str += ", ";
           Str += FDataSrc->DataSet->CurRow->Value[itFl->AV["uid"]];
           itFl = itFl->GetNext();
           }
           */
         }
        //Str = FMT(icsRegErrorConfirmDeleteRec)+" "+_CLASSDESCR_()->AV["deletelabel"].LowerCase()+"\n\n\" "+Str+"\"\n";
        if (MessageBox(Handle, Str.c_str(), cFMT(icsRegErrorConfirmDeleteRecMsgCaption),
            MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2) == IDYES)
         {
          FDataSrc->DataSet->Delete();
          CanRefresh = true;
          //if (quClass()->Active && quClass()->RecordCount)
          //curCD = quClass()->Value("CODE")->AsInteger;
          CanCancel = false;
         }
        else
         {
          CanRefresh = false;
          CanCancel  = false;
         }
       }
      else
       {
        CanRefresh = false;
        CanCancel  = false;
       }
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsRegErrorDeleteRecSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegErrorDeleteRec), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
    if (CanCancel)
     {
      FDataSrc->DataSet->Cancel();
     }
    if (CanRefresh)
     {
      if (FDM->OnClassAfterDelete)
       FDM->OnClassAfterDelete(_CLASSNODE_());
      actRefreshExecute(actRefresh);
     }
    EnableAfterScroll();
   }
  if (_CLASSNODE_()->CmpAV("uid", "1003,1112"))
   {
    ClassGridView->ViewData->Expand(true);
   }
  ActiveControl = ClassGrid;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::SetFiltered(bool AVal)
 {
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::ClearTemplate()
 {
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::GetData()
 {
  //TJSONObject *Filters = new TJSONObject;
  //Filters->AddPair(_CLASSNODE_()->GetChildByAV("choiceDB","ref","1000", true)->AV["uid"],IntToStr(FUCode));
  try
   {
    FDataSrc->DataSet->GetData("{\"" + UnitCodeFromCard(_CLASSNODE_()->AV["UID"]) +
        "\":\"" + IntToStr(FUCode) + "\"}", TdsRegFetch::All, 20, FGetDataProgress);
   }
  __finally
   {
    //delete Filters;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::actRefreshExecute(TObject * Sender)
 {
  ClassGridView->BeginUpdate();
  PrivGridView->BeginUpdate();
  try
   {
    GetData();
    FDataSrc->DataChanged();
    StatusSB->Panels->Items[0]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
    if (_UID_ == "1003")
     {
      FPrivDataSrc->DataSet->GetData("{\"317A\":\"" + IntToStr(FUCode) + "\"}", TdsRegFetch::All, 20, FGetDataProgress);
      FPrivDataSrc->DataChanged();
     }
   }
  __finally
   {
    ClassGridView->EndUpdate();
    PrivGridView->EndUpdate();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::RefreshById(UnicodeString ACode)
 {
  ClassGridView->BeginUpdate();
  try
   {
    FDataSrc->DataSet->GetDataById(ACode, FGetDataProgress);
    FDataSrc->DataChanged();
    StatusSB->Panels->Items[0]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
   }
  __finally
   {
    ClassGridView->EndUpdate();
    ClassGridView->DataController->FocusedRecordIndex = FDataSrc->DataSet->GetRowIdxByCode(ACode);
   }
  //__int64 FCodeIdx =
  //int rxrx = FDataSrc->DataController->GetRowIndexByRecordIndex(FCodeIdx, true);
  //ClassGridView->DataController->FocusedRowIndex = rxrx;
  // = FDataSrc->DataController->GetRowIndexByRecordIndex(FCodeIdx, true);
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::actFindNextExecute(TObject * Sender)
 {
  /*
   try
   {
   if (FDM->DM->SearchList && (FDM->DM->SearchIndex != -1))
   {
   if (quClass()->Active)
   {
   if (FDM->DM->SeachCount == 1) return;
   if (FDM->DM->SearchIndex >= FDM->DM->SeachCount)
   {
   if (MessageBox(NULL,cFMT(icsRegSearchEndMsg),cFMT(icsMessageMsgCaption),MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1)==ID_YES) FDM->DM->SearchIndex = 0;
   else return;
   }
   __int64 unitCode = FDM->DM->SearchList[FDM->DM->SearchIndex];
   TLocateOptions opt;
   if (FDM->UseClassDisControls)
   quClass()->DisableControls();
   quClass()->Locate("CODE",Variant(unitCode),opt);
   if (FDM->UseClassDisControls)
   quClass()->EnableControls();
   FDM->DM->SearchIndex++;
   }
   }
   }
   catch(Exception &E)
   {
   MessageBox(NULL,(FMT(icsRegErrorSearch)+"  \n"+E.Message).c_str(),FMT(icsErrorMsgCaption).c_str(),0);
   }
   */
 }
//---------------------------------------------------------------------------
/*
 void __fastcall TClassifForm::actViewClassParamExecute(TObject *Sender)
 {
 ViewParamPC->ActivePage = ClassParamTS;
 }
 //---------------------------------------------------------------------------
 void __fastcall TClassifForm::actViewTemplateParamExecute(TObject *Sender)
 {
 ViewParamPC->ActivePage = FilterParamTS;
 }
 //---------------------------------------------------------------------------
 */
TcxGridColumn * __fastcall TdsCardClientForm::ClassCol(int AIdx)
 {
  return ClassGridView->Columns[AIdx];
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::DisableAfterScroll()
 {
  /*
   if (FDM->UseAfterScroll)
   quClass()->AfterScroll = NULL;
   else
   quClass()->OnEndScroll = NULL;
   */
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::EnableAfterScroll()
 {
  /*
   if (quClass()->Active)
   {
   if (FDM->UseAfterScroll)
   quClass()->AfterScroll = ListAfterScroll;
   else
   quClass()->OnEndScroll = ListAfterScroll;
   }
   */
 }
//---------------------------------------------------------------------------
TTagNode * __fastcall TdsCardClientForm::_CLASSDESCR_()
 {
  return FSelectedNodeDescr;
 }
//---------------------------------------------------------------------------
TTagNode * __fastcall TdsCardClientForm::_CLASSNODE_()
 {
  return FSelectedNode;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCardClientForm::GetBtnEn(UnicodeString AAttrName)
 {
  return ((bool)(_CLASSDESCR_()->AV[AAttrName].Length()));
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::actCardSelectExecute(TObject * Sender)
 {
  if ((CardTypePC->TabIndex != -1) && (int)CardTypePC->Properties->Tabs->Objects[CardTypePC->TabIndex])
   {
    FSelectedNode      = CardRoot->GetTagByUID(UIDStr((int)CardTypePC->Properties->Tabs->Objects[CardTypePC->TabIndex]));
    FSelectedNodeDescr = FSelectedNode->GetChildByName("description");
    _UID_              = FSelectedNode->AV["uid"].UpperCase();
    if (_UID_ == "1003")
     FPrivNode = CardRoot->GetTagByUID("3003");
    Caption            = SaveCaption + " " + _CLASSNODE_()->AV["name"];
    curCD              = 0;
    actInsert->Enabled = FDM->CardEditable && GetBtnEn("insertlabel") && (SaveEnInsert && _CLASSNODE_())
        /*FDM->CardEditable && (SaveEnInsert&&_CLASSNODE_())*/; //(FDM->FullEdit |
    actEdit->Enabled        = FDM->CardEditable && (SaveEnEdit && _CLASSNODE_());
    actDelete->Enabled      = FDM->CardEditable && (SaveEnDelete && _CLASSNODE_());
    actTemplate->Enabled    = SaveEnSetTemplate && _CLASSNODE_() && actInsert->Enabled;
    actSetFilter->Enabled   = SaveEnSetTemplate && _CLASSNODE_();
    actReSetFilter->Enabled = SaveEnReSetTemplate && _CLASSNODE_();
    //actRefresh->Enabled           = SaveEnRefresh&&_CLASSNODE_();
    //actFind->Enabled              = SaveEnFind&&_CLASSNODE_();
    actFindNext->Enabled = SaveEnFindNext && _CLASSNODE_();
    //actViewClassParam->Enabled    = SaveEnViewClassParam&&_CLASSNODE_();
    //actViewTemplateParam->Enabled = SaveEnViewTemplateParam&&_CLASSNODE_();
    ClearTemplate();
    ChangeSrc();
    ActiveControl = ClassGrid;
   }
  else
   _UID_ = "";
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::actExitExecute(TObject * Sender)
 {
  Close();
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::actTemplateExecute(TObject * Sender)
 {
  /*
   if (!actTemplate->Checked)
   {
   if (_CLASSNODE_())
   {
   UnicodeString tmpl = GetTemplateFields(_CLASSNODE_()->GetChildByName("description"));
   bool FResetDown = true;
   if (tmpl.Length())
   {
   TITemplateForm *TmpDlg = NULL;
   try
   {
   TmpDlg = new TITemplateForm(this,FDM,tmpl);
   TmpDlg->HelpContext = FDM->HelpContextList[_CLASSNODE_()->AV["uid"]+"_tmpl"+FDM->TablePrefix].ToIntDef(0);
   if (TmpDlg)
   {
   curCD = quClass()->Value("CODE")->AsInteger;
   TmpDlg->ShowModal();
   if (TmpDlg->ModalResult == mrOk)
   {
   if (!TmpDlg->IsEmpty())
   {
   FCtrList->LabDataChange();
   actTemplate->Checked = true;
   if (actSetFilter->Checked) actSetFilterExecute(actSetFilter);
   SetTemplateBI->Down = true;
   SetFilterBI->Down = false;
   FResetDown = false;
   }
   }
   }
   }
   __finally
   {
   if (TmpDlg) delete TmpDlg;
   if (FResetDown)
   SetTemplateBI->Down = false;
   }
   }
   ActiveControl = ClassGrid;
   }
   }
   else
   {
   try
   {
   if (quClass()->Active&&_CLASSNODE_())
   {
   curCD = quClass()->Value("CODE")->AsInteger;
   ClearTemplate();
   FCtrList->LabDataChange();
   actTemplate->Checked = false;
   SetTemplateBI->Down = false;
   }
   }
   __finally
   {
   ActiveControl = ClassGrid;
   }
   }
   */
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::FormShow(TObject * Sender)
 {
  FGetUnitData();
  if (!FSetCardId)
   CardTypePC->TabIndex = 1;
  CardTypePCChange(CardTypePC);
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::ClassGridViewFocusedRecordChanged(TcxCustomGridTableView * Sender,
    TcxCustomGridRecord * APrevFocusedRecord, TcxCustomGridRecord * AFocusedRecord, bool ANewItemRecordFocusingChanged)
 {
  if (ClassGridView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1))
   FDataSrc->DataSet->GetDataById(VarToStr(FDataSrc->DataSet->CurRow->Value["CODE"]), FGetDataProgress);
  FCtrList->LabDataChange();
  actEdit->Enabled = ClassGridView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1)
      && FDM->CardEditable && GetBtnEn("editlabel"); //(FDM->FullEdit | GetBtnEn("editlabel"))
  actDelete->Enabled = ClassGridView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1)
      && FDM->CardEditable && GetBtnEn("deletelabel"); //(FDM->FullEdit | GetBtnEn("deletelabel"))
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::CardTypePCChange(TObject * Sender)
 {
  if (CardTypePC->TabIndex != -1)
   {
    if ((int)CardTypePC->Properties->Tabs->Objects[CardTypePC->TabIndex])
     {
      CardViewPC->ActivePage = ComTS;
      actCardSelectExecute(actCardSelect);
     }
    else if (!CardTypePC->TabIndex)
     {
      CardViewPC->ActivePage = F63TS;
      Caption                = SaveCaption + "Форма №63";
      F63Viewer->LoadFromString(FDM->kabCDSGetF63(IntToStr(FUCode)));
     }
    else if (CardTypePC->TabIndex == 6)
     {
      actLoadSchSettingExecute(actLoadSchSetting);
      CardViewPC->ActivePage        = SettingTS;
      Caption                       = SaveCaption + "Настройки";
      UnitPlanSettingPC->ActivePage = SchTS;
      SchSettingTV->SetFocus();
     }
    /*else if (CardTypePC->TabIndex == 6)
     {

     CardViewPC->ActivePage = PlanTS;
     Caption = SaveCaption+"План";
     //   CardViewPC->ActivePage = F63TS;
     }*/
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::CardTypePCChanging(TObject * Sender, bool & AllowChange)
 {
  /*
   if (APrevFocusedNode&&_CLASSNODE_())
   {
   if (APrevFocusedNode->Level > 0)
   {
   RegIni->OpenKey(FDM->RegistryKey,true);
   int xxWidth;
   for (int i = 0; i < CastToTSL(_LISTFL_)->Count; i++)
   {
   // формат строки  таблица.поле:алиас_поля=текстовая_метка_поля,(TObject*)ширина колонки);
   UnicodeString XXX = CastToTSL(_LISTFL_)->Strings[i];
   RegIni->WriteInteger(_CLASSNODE_()->AV["uid"],GetPart1(XXX,':'),ClassCol(i)->Width);
   }
   RegIni->CloseKey();
   }
   }
   */
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::ClassGridViewColumn1CompareRowValuesForCellMerging(TcxGridColumn * Sender,
    TcxGridDataRow * ARow1, TcxCustomEditProperties * AProperties1, const Variant & AValue1, TcxGridDataRow * ARow2,
    TcxCustomEditProperties * AProperties2, const Variant & AValue2, bool & AAreEqual)
 {
  bool IsEqu = true;
  for (int i = 0; i < MergeCompFLNum->Count; i++)
   {
    IsEqu &= (ARow1->Values[(int)MergeCompFLNum->Items[i]] == ARow2->Values[(int)MergeCompFLNum->Items[i]]);
   }
  AAreEqual = (AValue1 == AValue2) && IsEqu;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::ClassGridViewGroupRowCollapsing(TcxGridTableView * Sender, TcxGridGroupRow * AGroup,
    bool & AAllow)
 {
  AAllow = false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::actSaveSchSettingExecute(TObject * Sender)
 {
  FDM->SavePatSetting(FUCode, FSettingNode->AsXML);
  SaveSettingBtnPanel->Visible = false;
  actSaveSchSetting->Enabled   = false;
  actLoadSchSetting->Enabled   = false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::actLoadSchSettingExecute(TObject * Sender)
 {
  SchSettingTV->BeginUpdate();
  PersPlanTV->BeginUpdate();
  try
   {
    if (!FSettingNode)
     FSettingNode = new TTagNode;
    FSettingNode->AsXML = FDM->GetPatSetting(FUCode);
    TTagNode * FSchList = FSettingNode->GetChildByName("sch", true);
    TTagNode * FPersPlanList = FSettingNode->GetChildByName("vacplan", true);
    TTagNode * itNode;
    TTagNode * FPers = NULL;
    TTagNode * FComm = NULL;
    if (FPersPlanList)
     {
      FPers = FPersPlanList->GetChildByName("pers");
      FComm = FPersPlanList->GetChildByName("comm");
     }
    TcxTreeListNode * tmpNode;
    if (FSchList)
     {
      SchSettingTV->Clear();
      TTagNode * itInf = FSchList->GetFirstChild();
      while (itInf)
       {
        tmpNode            = SchSettingTV->Root->AddChild();
        tmpNode->Values[0] = InfInPlan(FComm, FPers, itInf->AV["infref"]);
        tmpNode->Texts[1]  = FDM->InfName[itInf->AV["infref"]];
        tmpNode->Texts[2]  = FDM->GetSchName(itInf->AV["vac"], true);
        tmpNode->Texts[3]  = FDM->GetSchName(itInf->AV["test"], false);
        tmpNode->Texts[4]  = FDM->GetSchName(itInf->AV["tvac"], true);
        tmpNode->Texts[5]  = FDM->GetSchName(itInf->AV["ttest"], false);
        tmpNode->Data      = itInf;
        itInf              = itInf->GetNext();
       }
      NKSettingTVCol->SortOrder = soDescending;
      InfNameSchCol->SortOrder = soAscending;
     }
    PersPlanTV->Clear();
    if (FPersPlanList)
     {
      if (FComm)
       {
        TcxTreeListNode * FLpuGr = NULL;
        TcxTreeListNode * FOrgGr = NULL;
        TcxTreeListNode * FUchGr = NULL;
        TcxTreeListNode * FAddrGr = NULL;
        TcxTreeListNode * FProfGr = NULL;
        itNode = FComm->GetFirstChild();
        while (itNode)
         {
          tmpNode = NULL;
          if (itNode->CmpAV("objtype", "l"))
           {
            if (!FLpuGr)
             {
              FLpuGr           = PersPlanTV->Root->AddChild();
              FLpuGr->Texts[0] = "Календарное планирование";
             }
            tmpNode = FLpuGr->AddChild();
           }
          else if (itNode->CmpAV("objtype", "o"))
           {
            if (!FOrgGr)
             {
              FOrgGr           = PersPlanTV->Root->AddChild();
              FOrgGr->Texts[0] = "Планирование для организации";
             }
            tmpNode = FOrgGr->AddChild();
           }
          else if (itNode->CmpAV("objtype", "u"))
           {
            if (!FUchGr)
             {
              FUchGr           = PersPlanTV->Root->AddChild();
              FUchGr->Texts[0] = "Планирование для участка";
             }
            tmpNode = FUchGr->AddChild();
           }
          else if (itNode->CmpAV("objtype", "a"))
           {
            if (!FAddrGr)
             {
              FAddrGr           = PersPlanTV->Root->AddChild();
              FAddrGr->Texts[0] = "Планирование по адресу";
             }
            tmpNode = FAddrGr->AddChild();
           }
          else if (itNode->CmpAV("objtype", "p"))
           {
            if (!FProfGr)
             {
              FProfGr           = PersPlanTV->Root->AddChild();
              FProfGr->Texts[0] = "Планирование для профессии";
             }
            tmpNode = FProfGr->AddChild();
           }
          if (tmpNode)
           {
            tmpNode->Texts[0] = FDM->InfName[itNode->AV["infref"]];
           }
          itNode = itNode->GetNext();
         }
       }
      if (FPers)
       {
        TcxTreeListNode * FPersGr = NULL;
        itNode = FPers->GetFirstChild();
        while (itNode)
         {
          if (!FPersGr)
           {
            FPersGr           = PersPlanTV->Root->AddChild();
            FPersGr->Texts[0] = "Планирование для пациента";
           }
          tmpNode = FPersGr->AddChild();
          tmpNode->Texts[0] = FDM->InfName[itNode->AV["infref"]];
          itNode            = itNode->GetNext();
         }
       }
     }
    PersPlanTV->Root->Expand(true);
    SaveSettingBtnPanel->Visible = false;
    actSaveSchSetting->Enabled   = false;
    actLoadSchSetting->Enabled   = false;
   }
  __finally
   {
    SchSettingTV->EndUpdate();
    PersPlanTV->EndUpdate();
   }
 }
//---------------------------------------------------------------------------
int __fastcall TdsCardClientForm::InfInPlan(TTagNode * AComRoot, TTagNode * APersRoot, UnicodeString AInf)
 {
  return FDM->InfInPlan(AComRoot, APersRoot, AInf);
 }
//---------------------------------------------------------------------------
TTagNode * __fastcall TdsCardClientForm::GetSettingSelNode(TcxTreeList * ASrc)
 {
  TTagNode * RC = NULL;
  try
   {
    if (ASrc->SelectionCount)
     {
      if (ASrc->Selections[0]->Data)
       RC = (TTagNode *)ASrc->Selections[0]->Data;
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCardClientForm::GetSchData(int ACol, UnicodeString AAttr, UnicodeString & AVal,
    UnicodeString & AStrVal)
 {
  bool RC = false;
  TSchemaListForm * Dlg = NULL;
  try
   {
    TTagNode * FData = GetSettingSelNode(SchSettingTV);
    if (FData)
     {
      Dlg = new TSchemaListForm(this, FDM, FData->AV["infref"].ToIntDef(0), !(ACol % 2), FData->AV[AAttr], false);
      Dlg->ShowModal();
      if (Dlg->ModalResult == mrOk)
       {
        AVal    = GetLPartB(Dlg->Value, '=');
        AStrVal = GetRPartE(Dlg->Value, '=');
        RC      = true;
       }
     }
   }
  __finally
   {
    if (Dlg)
     delete Dlg;
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::SetSchData(int ACol, UnicodeString AAttr, UnicodeString AVal,
    UnicodeString AStrVal)
 {
  TTagNode * FData = GetSettingSelNode(SchSettingTV);
  if (FData)
   {
    SchSettingTV->Selections[0]->Texts[ACol] = AStrVal;
    if (SchSettingTV->InplaceEditor)
     {
      SchSettingTV->InplaceEditor->EditValue = AStrVal;
      SchSettingTV->Root->EndEdit(false);
     }
    FData->AV[AAttr] = AVal;
    FData->AV["ch"+AAttr] = "1";
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::FSchButtonClick(int AButtonIndex, int ACol, UnicodeString AAttr)
 {
  bool FModify = false;
  if (AButtonIndex)
   {
    SetSchData(ACol, AAttr, "", "");
    FModify = true;
   }
  else
   {
    UnicodeString FSch, FStrSch;
    if (GetSchData(ACol, AAttr, FSch, FStrSch))
     {
      SetSchData(ACol, AAttr, FSch, FStrSch);
      FModify = true;
     }
   }
  if (FModify)
   {
    SaveSettingBtnPanel->Visible = true;
    actSaveSchSetting->Enabled   = true;
    actLoadSchSetting->Enabled   = true;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::VacSchColPropertiesButtonClick(TObject * Sender, int AButtonIndex)
 {
  FSchButtonClick(AButtonIndex, 2, "vac");
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::TestSchColPropertiesButtonClick(TObject * Sender, int AButtonIndex)
 {
  FSchButtonClick(AButtonIndex, 3, "test");
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::TurVacSchColPropertiesButtonClick(TObject * Sender, int AButtonIndex)
 {
  FSchButtonClick(AButtonIndex, 4, "tvac");
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::TurTestSchColPropertiesButtonClick(TObject * Sender, int AButtonIndex)
 {
  FSchButtonClick(AButtonIndex, 5, "ttest");
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::actAddPlanOptionsExecute(TObject * Sender)
 {
  ShowMessage("Добавляем");
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::actDelPlanOptionsExecute(TObject * Sender)
 {
  ShowMessage("Удаляем");
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::PersPlanTVStylesGetContentStyle(TcxCustomTreeList * Sender,
    TcxTreeListColumn * AColumn, TcxTreeListNode * ANode, TcxStyle *& AStyle)
 {
  if (ANode)
   {
    if (!ANode->Level)
     AStyle = HeadStyle;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::PersPlanTVCollapsing(TcxCustomTreeList * Sender, TcxTreeListNode * ANode,
    bool & Allow)
 {
  Allow = false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::dxBarButton2Click(TObject * Sender)
 {
  if (FDM->OnCreatePlan)
   {
    FDM->OnCreatePlan(FUCode);
    ClassGridView->BeginUpdate();
    Application->ProcessMessages();
    Sleep(10000);
    Application->ProcessMessages();
    GetData();
    Application->ProcessMessages();
    FDataSrc->DataChanged();
    Application->ProcessMessages();
    StatusSB->Panels->Items[0]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
    ClassGridView->EndUpdate();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::SchSettingTVStylesGetContentStyle(TcxCustomTreeList * Sender,
    TcxTreeListColumn * AColumn, TcxTreeListNode * ANode, TcxStyle *& AStyle)
 {
  if (AColumn && ANode)
   {
    if (!AColumn->Position->BandIndex && (AColumn->Position->ColIndex == 1))
     {
      if (ANode->Texts[0].ToIntDef(0))
       AStyle = PlanStyle;
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::actPrintF63Execute(TObject * Sender)
 {
  FDM->DocPreview(FDM->kabCDSGetF63(IntToStr(FUCode)));
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::FormClose(TObject * Sender, TCloseAction & Action)
 {
  if (FOnCardClose)
   FOnCardClose((TObject *)FUCode);
 }
//---------------------------------------------------------------------------
void __fastcall TdsCardClientForm::actInsertF63Execute(TObject * Sender)
 {
  if (CardTypePC->TabIndex == 1)
   {//прививки
    ShowMessage("Временно не реализовано.");
   }
  else if (CardTypePC->TabIndex == 2)
   {//пробы
    TdsInsertF63TestForm * Dlg = new TdsInsertF63TestForm(this, FDM, FUCode, FPatBirthday);
    try
     {
      Dlg->ShowModal();
      if (Dlg->ModalResult == mrOk)
       {
        //TdsCardPrivEditForm * Dlg = new TdsCardPrivEditForm(this, true, FUCode, FDM, _CLASSNODE_()->GetRoot(),
       }
     }
    __finally
     {
      delete Dlg;
     }
   }
 }
//---------------------------------------------------------------------------
