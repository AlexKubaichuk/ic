﻿//---------------------------------------------------------------------------

#ifndef dsInsertF63TestUnitH
#define dsInsertF63TestUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ImgList.hpp>
#include "cxBarEditItem.hpp"
#include "cxClasses.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxDropDownEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLabel.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"
#include "cxCalendar.hpp"
//---------------------------------------------------------------------------
#include "dsICCardDMUnit.h"
#include "cxContainer.hpp"
#include "cxEdit.hpp"
#include "cxProgressBar.hpp"
#include "InsertCardItem.h"
#include "dxBarExtItems.hpp"
//---------------------------------------------------------------------------
class TdsInsertF63TestForm : public TForm
{
__published:	// IDE-managed Components
 TcxTreeList *TestTL;
 TcxTreeListColumn *BirthDayCol;
 TcxTreeListColumn *VacTypeCol;
 TcxTreeListColumn *ReacTypeCol;
 TcxTreeListColumn *ReacValCol;
 TcxTreeListColumn *TestCodeCol;
 TActionList *ClassAL;
 TdxBarManager *ClassTBM;
 TdxBar *ClassTBMBar1;
 TdxBarLargeButton *dxBarLargeButton2;
 TdxBarLargeButton *dxBarLargeButton3;
 TdxBarLargeButton *dxBarLargeButton4;
 TcxImageList *ClassIL;
 TcxImageList *LClassIL32;
 TAction *actApply;
 TAction *actRestore;
 TAction *actDelete;
 TAction *Action4;
 TdxBarCombo *PeriodCB;
 TcxTreeListColumn *TestCol;
 TcxTreeListColumn *TestReakCodeCol;
 TcxProgressBar *PBar;
 TdxBarControlContainerItem *dxBarControlContainerItem1;
 TdxBarStatic *dxBarStatic1;
 void __fastcall actRestoreExecute(TObject *Sender);
 void __fastcall actApplyExecute(TObject *Sender);
 void __fastcall actDeleteExecute(TObject *Sender);
 void __fastcall PeriodCBChange(TObject *Sender);
 void __fastcall ReacValColPropertiesValidate(TObject *Sender, Variant &DisplayValue,
          TCaption &ErrorText, bool &Error);
 void __fastcall FormShow(TObject *Sender);
 void __fastcall TestColPropertiesEditValueChanged(TObject *Sender);
 void __fastcall SerEDPropertiesValidate(TObject *Sender, Variant &DisplayValue,
          TCaption &ErrorText, bool &Error);
 void __fastcall ReacTypeColGetEditingProperties(TcxTreeListColumn *Sender, TcxTreeListNode *ANode,
          TcxCustomEditProperties *&EditProperties);
 void __fastcall ReacTypeColPropertiesEditValueChanged(TObject *Sender);
 void __fastcall VacTypeColPropertiesValidate(TObject *Sender, Variant &DisplayValue,
          TCaption &ErrorText, bool &Error);
 void __fastcall BirthDayColPropertiesEditValueChanged(TObject *Sender);
 void __fastcall TestTLEditing(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          bool &Allow);
 void __fastcall TestColPropertiesChange(TObject *Sender);
private:	// User declarations
  __int64 FUCode;
  TDateTime FBirthDay;
  TdsICCardDM  *FDM;
  TInsertCardItem * FInsTestExecutor;
  TcxTreeListNode * __fastcall GetSelNode();

  void __fastcall CreateList();
  bool __fastcall CheckInput();
  void __fastcall ButtonEnabled();
public:		// User declarations
  __fastcall TdsInsertF63TestForm(TComponent* Owner, TdsICCardDM  *ADM, __int64 AUCode, TDateTime ABirthDay);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsInsertF63TestForm *dsInsertF63TestForm;
//---------------------------------------------------------------------------
#endif
