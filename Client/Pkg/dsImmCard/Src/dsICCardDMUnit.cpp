﻿#include <vcl.h>
// #include <stdio.h>
// #include <clipbrd.hpp>
#pragma hdrstop
#include "dsICCardDMUnit.h"
#include "icsDateUtil.hpp"
#include "dsCardPrivEditUnit.h"
#include "dsCardTestEditUnit.h"
#include "dsCardOtvodEditUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
// kabCDSGetValById("reg.s3 - ps1 ps2:ps3;
// kabCDSGetValById("reg.s4 - ps1 ps2:ps3#ps4
TdsICCardDM * dsICCardDM;
// ---------------------------------------------------------------------------
__fastcall TdsICCardDM::TdsICCardDM(TComponent * Owner) : TDataModule(Owner)
 {
  FClassDefNode = NULL;
  // FRegDefNode = NULL;
  FCardEditable      = true;
  FFullEdit          = true;
  FClassShowExpanded = "";
  FRequiredColor     = clInfoBk;
  FULEditWidth       = 700;
  FExtSearchVacData  = new TStringList;
  // FXMLPath = icsPrePath(GetEnvironmentVariable("APPDATA")+"\\"+PRODFOLDER+"\\xml");
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICCardDM::DataModuleCreate(TObject * Sender)
 {
  DTFormats[0]      = FMT(icsDTFormatsD);
  DTFormats[1]      = FMT(icsDTFormatsDT);
  DTFormats[2]      = FMT(icsDTFormatsT);
  FExtLPUCode       = -1;
  FCardVacDataSrc   = NULL;
  FCardTestDataSrc  = NULL;
  FCardCheckDataSrc = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICCardDM::FLoadMapData(TJSONObject * AData, TAnsiStrMap & AMap /* , TAnsiStrMap &AMapCode */)
 {
  TJSONPairEnumerator * itPair;
  UnicodeString FClCode, FClStr;
  itPair = AData->GetEnumerator();
  while (itPair->MoveNext())
   {
    FClCode       = ((TJSONString *)itPair->Current->JsonString)->Value();
    FClStr        = ((TJSONString *)itPair->Current->JsonValue)->Value();
    AMap[FClCode] = FClStr;
    // AMapCode[FClStr] = FClCode;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICCardDM::FLoadDependMapData(TJSONObject * AData, TIntListMap & AMap, TIntListMap & AMapCode)
 {
  TJSONPairEnumerator * itPair;
  int FIntClCode, FIntClVal;
  TIntMap * vti;
  itPair = AData->GetEnumerator();
  while (itPair->MoveNext())
   {
    FIntClCode = ((TJSONString *)itPair->Current->JsonString)->Value().ToIntDef(-1);
    FIntClVal  = ((TJSONString *)itPair->Current->JsonValue)->Value().ToIntDef(-1);
    if (AMap.find(FIntClCode) == AMap.end())
     AMap[FIntClCode] = new TIntMap;
    vti = AMap[FIntClCode];
    (*vti)[FIntClVal] = 1;
    if (AMapCode.find(FIntClVal) == AMapCode.end())
     AMapCode[FIntClVal] = new TIntMap;
    vti = AMapCode[FIntClVal];
    (*vti)[FIntClCode] = 1;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICCardDM::PreloadData()
 {
  // TJSONObject *RetData;
  // UnicodeString FClCode, FClStr;
  // TJSONPairEnumerator *itPair;
  // int FIntClCode, FIntClVal;
  // TIntMap *vti;
  // TIntListMap::iterator RC;
  // ***** Список Инфекций ******
  FLoadMapData(kabCDSGetListById("reg.s2.Select CODE as ps1, R003C as ps2 From CLASS_003A"), InfList);
  /*
   RetData = kabCDSGetValById("reg.s2.Select CODE as ps1, R003C as ps2 From CLASS_003A", "0");
   itPair = RetData->GetEnumerator();
   while (itPair->MoveNext())
   {
   FClCode =((TJSONString*)itPair->Current->JsonString)->Value();
   FClStr  =((TJSONString*)itPair->Current->JsonValue)->Value();
   InfList[FClCode] = FClStr;
   //     InfListCode[FClStr] = FClCode;
   }
   */
  // ***** Список МИБП ******
  FLoadMapData(kabCDSGetListById("reg.s2.Select CODE as ps1, R0040 as ps2 From CLASS_0035"), VacList);
  /*
   RetData = kabCDSGetValById("reg.s2.Select CODE as ps1, R0040 as ps2 From CLASS_0035", "0");
   itPair = RetData->GetEnumerator();
   while (itPair->MoveNext())
   {
   FClCode =((TJSONString*)itPair->Current->JsonString)->Value();
   FClStr  =((TJSONString*)itPair->Current->JsonValue)->Value();
   VacList[FClCode] = FClStr;
   //VacListCode[FClStr] = FClCode;
   }
   */
  // ***** Список Проб ******
  FLoadMapData(kabCDSGetListById("reg.s2.Select CODE as ps1, R002C as ps2 From CLASS_002A"), ProbList);
  /*
   RetData = kabCDSGetValById("reg.s2.Select CODE as ps1, R002C as ps2 From CLASS_002A", "0");
   itPair = RetData->GetEnumerator();
   while (itPair->MoveNext())
   {
   FClCode =((TJSONString*)itPair->Current->JsonString)->Value();
   FClStr  =((TJSONString*)itPair->Current->JsonValue)->Value();
   ProbList[FClCode] = FClStr;
   //ProbListCode[FClStr] = FClCode;
   }
   */
  // ***** Инфекции по пробам и пробы по инфекциям ******
  FLoadDependMapData(kabCDSGetListById("reg.s2.Select R007B as ps1, R007A as ps2 From CLASS_0079"), InfProbList,
    ProbInfList);
  /*
   RetData = kabCDSGetValById("reg.s2.Select R007B as ps1, R007A as ps2 From CLASS_0079", "0");
   itPair = RetData->GetEnumerator();
   while (itPair->MoveNext())
   {
   FIntClCode =((TJSONString*)itPair->Current->JsonString)->Value().ToIntDef(-1);
   FIntClVal  =((TJSONString*)itPair->Current->JsonValue)->Value().ToIntDef(-1);

   if (InfProbList.find(FIntClCode) == InfProbList.end())
   InfProbList[FIntClCode] = new TIntMap;
   vti = InfProbList[FIntClCode];
   (*vti)[FIntClVal] = 1;

   if (ProbInfList.find(FIntClVal) == ProbInfList.end())
   ProbInfList[FIntClVal] = new TIntMap;
   vti = ProbInfList[FIntClVal];
   (*vti)[FIntClCode] = 1;
   }
   */
  // ***** Инфекции по вакцинам и вакцины по инфекциям ******
  FLoadDependMapData(kabCDSGetListById("reg.s2.Select R003E as ps1, R003B as ps2 From CLASS_002D"), InfVacList,
    VacInfList);
  /*
   RetData = kabCDSGetValById("reg.s2.Select R003E as ps1, R003B as ps2 From CLASS_002D", "0");
   itPair = RetData->GetEnumerator();
   while (itPair->MoveNext())
   {
   FIntClCode =((TJSONString*)itPair->Current->JsonString)->Value().ToIntDef(-1);
   FIntClVal  =((TJSONString*)itPair->Current->JsonValue)->Value().ToIntDef(-1);

   if (InfVacList.find(FIntClCode) == InfVacList.end())
   InfVacList[FIntClCode] = new TIntMap;
   vti = InfVacList[FIntClCode];
   (*vti)[FIntClVal] = 1;

   if (VacInfList.find(FIntClVal) == VacInfList.end())
   VacInfList[FIntClVal] = new TIntMap;
   vti = VacInfList[FIntClVal];
   (*vti)[FIntClCode] = 1;
   }
   */
  PreloadExtData();
  FCardVacNode                                = RegDef->GetTagByUID("1003");
  FCardTestNode                               = RegDef->GetTagByUID("102f");
  FCardCheckNode                              = RegDef->GetTagByUID("1100");
  FCardCancelNode                             = RegDef->GetTagByUID("3030");
  FCardVacDataSrc                             = new TkabCustomDataSource(FCardVacNode);
  FCardVacDataSrc->DataSet->IdPref            = "card";
  FCardVacDataSrc->DataSet->OnGetCount        = kabCDSGetCount;
  FCardVacDataSrc->DataSet->OnGetIdList       = kabCDSGetCodes;
  FCardVacDataSrc->DataSet->OnGetValById      = kabCDSGetValById;
  FCardVacDataSrc->DataSet->OnGetValById10    = kabCDSGetValById10;
  FCardVacDataSrc->DataSet->OnInsert          = kabCDSInsert;
  FCardVacDataSrc->DataSet->OnEdit            = kabCDSEdit;
  FCardVacDataSrc->DataSet->OnDelete          = kabCDSDelete;
  FCardTestDataSrc                            = new TkabCustomDataSource(FCardTestNode);
  FCardTestDataSrc->DataSet->IdPref           = "card";
  FCardTestDataSrc->DataSet->OnGetCount       = kabCDSGetCount;
  FCardTestDataSrc->DataSet->OnGetIdList      = kabCDSGetCodes;
  FCardTestDataSrc->DataSet->OnGetValById     = kabCDSGetValById;
  FCardTestDataSrc->DataSet->OnGetValById10   = kabCDSGetValById10;
  FCardTestDataSrc->DataSet->OnInsert         = kabCDSInsert;
  FCardTestDataSrc->DataSet->OnEdit           = kabCDSEdit;
  FCardTestDataSrc->DataSet->OnDelete         = kabCDSDelete;
  FCardCheckDataSrc                           = new TkabCustomDataSource(FCardCheckNode);
  FCardCheckDataSrc->DataSet->IdPref          = "card";
  FCardCheckDataSrc->DataSet->OnGetCount      = kabCDSGetCount;
  FCardCheckDataSrc->DataSet->OnGetIdList     = kabCDSGetCodes;
  FCardCheckDataSrc->DataSet->OnGetValById    = kabCDSGetValById;
  FCardCheckDataSrc->DataSet->OnGetValById10  = kabCDSGetValById10;
  FCardCheckDataSrc->DataSet->OnInsert        = kabCDSInsert;
  FCardCheckDataSrc->DataSet->OnEdit          = kabCDSEdit;
  FCardCheckDataSrc->DataSet->OnDelete        = kabCDSDelete;
  FCardCancelDataSrc                          = new TkabCustomDataSource(FCardCancelNode);
  FCardCancelDataSrc->DataSet->IdPref         = "card";
  FCardCancelDataSrc->DataSet->OnGetCount     = kabCDSGetCount;
  FCardCancelDataSrc->DataSet->OnGetIdList    = kabCDSGetCodes;
  FCardCancelDataSrc->DataSet->OnGetValById   = kabCDSGetValById;
  FCardCancelDataSrc->DataSet->OnGetValById10 = kabCDSGetValById10;
  FCardCancelDataSrc->DataSet->OnInsert       = kabCDSInsert;
  FCardCancelDataSrc->DataSet->OnEdit         = kabCDSEdit;
  FCardCancelDataSrc->DataSet->OnDelete       = kabCDSDelete;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICCardDM::DataModuleDestroy(TObject * Sender)
 {
  delete FExtSearchVacData;
  if (FCardVacDataSrc)
   delete FCardVacDataSrc;
  if (FCardTestDataSrc)
   delete FCardTestDataSrc;
  if (FCardCheckDataSrc)
   delete FCardCheckDataSrc;
  if (FCardCancelDataSrc)
   delete FCardCancelDataSrc;
  /*
   delete FClassData;
   if (REG_DB->Connected) REG_DB->Close();
   SearchList = NULL;
   */
 }
// ---------------------------------------------------------------------------
#include <rpc.h>
UnicodeString __fastcall TdsICCardDM::NewGUID()
 {
  UnicodeString RC = "";
  try
   {
    UUID FGUI;
    if (UuidCreate(&FGUI) == RPC_S_OK)
     {
      unsigned short * strGUID[1];
      if (UuidToString(&FGUI, strGUID) == RPC_S_OK)
       RC = UnicodeString(AnsiString((char *)(strGUID[0])));
     }
   }
  __finally
   {
   }
  if (RC.Trim().Length())
   return RC.Trim().UpperCase();
  else
   throw Exception("Ошибка формирования GUID");
 }
// ---------------------------------------------------------------------------
// TTagNode* __fastcall TdsRegDM::FGetRegDefNode()
// {
// TTagNode *RC = FRegDefNode;
// return RC;
// }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsICCardDM::FGetClassDef()
 {
  TTagNode * RC = FClassDefNode;
  try
   {
    if (!RC)
     {
      FClassDefNode = RegDef->GetChildByName("classes");
      RC            = FClassDefNode;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TdsICCardDM::kabCDSGetCount(UnicodeString AId, UnicodeString ATTH, UnicodeString AFilterParam)
 {
  __int64 RC = 0;
  try
   {
    if (FOnGetCount)
     RC = FOnGetCount(AId, AFilterParam);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsICCardDM::kabCDSGetCodes(UnicodeString AId, int AMax, UnicodeString ATTH,
  UnicodeString AFilterParam)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetIdList)
     FOnGetIdList(AId, AMax, AFilterParam, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsICCardDM::kabCDSGetListById(UnicodeString AId)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetListById)
     FOnGetListById(AId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsICCardDM::kabCDSGetValById(UnicodeString AId, UnicodeString ARecId)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetValById)
     FOnGetValById(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsICCardDM::kabCDSGetValById10(UnicodeString AId, UnicodeString ARecId)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetValById10)
     FOnGetValById10(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::kabCDSInsert(UnicodeString AId, TJSONObject * AValues, UnicodeString & ARecId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnInsertData)
     FOnInsertData(AId, AValues, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::kabCDSEdit(UnicodeString AId, TJSONObject * AValues, UnicodeString & ARecId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnEditData)
     FOnEditData(AId, AValues, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::kabCDSDelete(UnicodeString AId, UnicodeString ARecId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnDeleteData)
     FOnDeleteData(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::kabCDSGetClassXML(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetClassXML)
     FOnGetClassXML(AId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::kabCDSGetF63(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetF63)
     FOnGetF63(AId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICCardDM::OnGetXML(UnicodeString AId, TTagNode *& ARC)
 {
  ARC = FXMLList->GetXML(AId);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::GetVacDefDoze(TDate APrivDate, TDate APatBirthday, UnicodeString AVacCode)
 {
  UnicodeString RC = "0,0";
  try
   {
    if (FOnGetVacDefDoze)
     RC = FOnGetVacDefDoze(APrivDate, APatBirthday, AVacCode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::GetVacSch(TStringList * AInfList, UnicodeString AVacCode, __int64 AUCode,
  UnicodeString & AVacSch, UnicodeString & AVacSchTur)
 {
  UnicodeString RC = "";
  TTagNode * FSettingNode = new TTagNode;
  try
   {
    AVacSch             = "";
    AVacSchTur          = "";
    FSettingNode->AsXML = GetPatSetting(AUCode);
    TTagNode * FSchList = FSettingNode->GetChildByName("sch", true);
    UnicodeString FCurrSch, FCurrTurSch;
    TTagNode * FSchNode;
    if (FSchList)
     {
      FCurrSch    = "";
      FCurrTurSch = "";
      for (int i = 0; i < AInfList->Count; i++)
       {
        FSchNode = FSchList->GetChildByAV("spr", "infref", IntToStr((int)AInfList->Objects[i]));
        if (FSchNode)
         {
          if (!FCurrSch.Length())
           {
            FCurrSch = FSchNode->AV["vac"].UpperCase();
            AVacSch  = FCurrSch;
           }
          else
           {
            if (FCurrSch != FSchNode->AV["vac"].UpperCase())
             {
              FCurrSch = FSchNode->AV["vac"].UpperCase();
              AVacSch += ", " + FCurrSch;
             }
           }
          if (!FCurrTurSch.Length())
           {
            FCurrTurSch = FSchNode->AV["tvac"].UpperCase();
            AVacSchTur  = FCurrTurSch;
           }
          else
           {
            if (FCurrTurSch != FSchNode->AV["tvac"].UpperCase())
             {
              FCurrTurSch = FSchNode->AV["tvac"].UpperCase();
              AVacSchTur += ", " + FCurrTurSch;
             }
           }
         }
       }
     }
   }
  __finally
   {
    delete FSettingNode;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsICCardDM::FGetVacSchDef()
 {
  TTagNode * RC = NULL;
  try
   {
    RC = FXMLList->GetXML("12063611-00008CD7-CD89");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsICCardDM::FGetTestSchDef()
 {
  TTagNode * RC = NULL;
  try
   {
    RC = FXMLList->GetXML("001D3500-00005882-DC28");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICCardDM::GetSchemeList(int AInfCode, TTagNodeMap & ASchList, bool AVacSch)
 {
  TIntListMap::iterator RC = ((AVacSch) ? InfVacList.find(AInfCode) : InfProbList.find(AInfCode));
  if (RC != ((AVacSch) ? InfVacList.end() : InfProbList.end()))
   {
    TIntMap vti = *(RC->second);
    UnicodeString FVacCodes = "";
    for (TIntMap::iterator i = vti.begin(); i != vti.end(); i++)
     {
      if (!FVacCodes.Length())
       FVacCodes = IntToStr(i->first);
      else
       FVacCodes += "," + IntToStr(i->first);
     }
    TTagNode * itSch = ((AVacSch) ? VacSchDef->GetChildByName("schema", true) :
      TestSchDef->GetChildByName("schema", true));
    while (itSch)
     {
      if (itSch->CmpAV("ref", FVacCodes))
       ASchList[itSch->AV["uid"]] = itSch;
      itSch = itSch->GetNext();
     }
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::FGetInfName(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    TAnsiStrMap::iterator FRC = InfList.find(ACode);
    if (FRC != InfList.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::FGetVacName(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    TAnsiStrMap::iterator FRC = VacList.find(ACode);
    if (FRC != VacList.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::FGetTestName(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    TAnsiStrMap::iterator FRC = ProbList.find(ACode);
    if (FRC != ProbList.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICCardDM::GetTestList(TStrings * AList)
 {
  try
   {
    AList->Clear();
    for (TAnsiStrMap::iterator i = ProbList.begin(); i != ProbList.end(); i++)
     AList->AddObject(i->second, (TObject *)i->first.ToIntDef(0));
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::GetSchName(UnicodeString ASchId, bool AVacSch)
 {
  UnicodeString RC = "ошибка";
  try
   {
    if ((ASchId.UpperCase() == "_NULL_") || !ASchId.Length())
     {
      RC = "";
     }
    else
     {
      TTagNode * FSch = (AVacSch) ? VacSchDef->GetTagByUID(_GUI(ASchId)) : TestSchDef->GetTagByUID(_GUI(ASchId));
      TTagNode * FSchLine = (AVacSch) ? VacSchDef->GetTagByUID(_UID(ASchId)) : TestSchDef->GetTagByUID(_UID(ASchId));
      if (FSch && FSchLine)
       {
        if (FSch->CmpName("schema") && FSchLine->CmpName("begline, line, endline") && FSchLine->GetParent("schema")
          ->CmpAV("uid", FSch->AV["uid"]))
         { // _GUI(ASchId) указывает на схему, _UID(ASchId) указывает на строку схемы _GUI(ASchId) - остальное косяки
          if (FSchLine->CmpName("begline"))
           RC = "Начало схемы";
          else if (FSchLine->CmpName("endline"))
           RC = "Конец схемы";
          else
           RC = FSchLine->AV["name"];
          RC = "{" + FSch->AV["name"] + "." + RC + "} " + ((AVacSch) ? VacName[FSch->AV["ref"]] :
            TestName[FSch->AV["ref"]]);
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::AgeStr(TDateTime ACurDate, TDateTime ABirthDay)
 {
  UnicodeString RC = "";
  try
   {
    Word FYear, FMonth, FDay;
    DateDiff(ACurDate, ABirthDay, FDay, FMonth, FYear);
    RC = SetDay(FYear, FMonth, FDay);
   }
  __finally
   {
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::SetDay(int AYear, int AMonth, int ADay)
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString sm = IntToStr(AYear);
    sm = sm.SubString(sm.Length(), 1);
    if (((sm == "1") || (sm == "2") || (sm == "3") || (sm == "4")) && !((AYear < 15) && (AYear > 10)))
     sm = "г. ";
    else
     sm = "л. ";
    if (AYear)
     RC += IntToStr(AYear) + sm;
    if (AMonth)
     RC += IntToStr(AMonth) + "м. ";
    if (ADay)
     RC += IntToStr(ADay) + "д.";
    if (!(AYear + AMonth + ADay))
     RC += "0д.";
   }
  __finally
   {
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICCardDM::PreloadExtData()
 {
  TJSONObject * RetData, *RetDataInf;
  UnicodeString FClCode, FClStr, FClCodeInf, FClStrInf;
  TJSONPairEnumerator * itPair, *itPairInf;
  RetData = kabCDSGetValById("reg.s2.Select CODE as ps1, R008F as ps2 From CLASS_0035", "0");
  itPair  = RetData->GetEnumerator();
  FExtSearchVacData->Clear();
  while (itPair->MoveNext())
   {
    FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
    FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
    FExtSearchVacData->AddObject(FClStr.UpperCase(), (TObject *)FClCode.ToIntDef(0));
   }
  for (int i = 0; i < FExtSearchVacData->Count; i++)
   {
    FClCode    = IntToStr((int)FExtSearchVacData->Objects[i]);
    RetDataInf = kabCDSGetValById
      ("reg.s2.select c2.code as ps1, c2.R003c as ps2 from class_002d c1 join class_003a c2 on (c2.code = c1.r003e) where c1.r003b=" +
      FClCode, "0");
    itPairInf = RetDataInf->GetEnumerator();
    while (itPairInf->MoveNext())
     FExtSearchVacData->Strings[i] = FExtSearchVacData->Strings[i] + " " +
       ((TJSONString *)itPairInf->Current->JsonValue)->Value().UpperCase();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICCardDM::SavePatSetting(__int64 AUCode, UnicodeString AData)
 {
  if (FOnSetPatSetting)
   FOnSetPatSetting(AUCode, AData);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::GetPatSetting(__int64 AUCode)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetPatSetting)
     RC = FOnGetPatSetting(AUCode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::GetPrivVar(__int64 APrivCode)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetPrivVar)
     RC = FOnGetPrivVar(APrivCode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::GetOtvodInf(__int64 AOtvodCode)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetOtvodInf)
     RC = FOnGetOtvodInf(AOtvodCode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICCardDM::DocPreview(UnicodeString AData)
 {
  if (FOnHTMLPreview)
   FOnHTMLPreview(AData);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsICCardDM::PlanPrivInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData * ATmplData,
  bool ASaveTmpl)
 {
  bool RC = false;
  TdsCardPrivEditForm * Dlg = new TdsCardPrivEditForm(this, true, AUCode, this, FCardVacNode->GetRoot(), APatBirthday,
    FCardVacDataSrc, ATmplData, true, ASaveTmpl);
  try
   {
    RC = (Dlg->ShowModal() == mrOk);
   }
  __finally
   {
    if (FCardVacDataSrc->DataSet->State == dsInsert)
     FCardVacDataSrc->DataSet->Cancel();
    delete Dlg;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsICCardDM::PlanTestInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData * ATmplData,
  bool ASaveTmpl)
 {
  bool RC = false;
  TdsCardTestEditForm * Dlg = new TdsCardTestEditForm(this, true, AUCode, this, FCardTestNode->GetRoot(), APatBirthday,
    FCardTestDataSrc, ATmplData, true, ASaveTmpl);
  try
   {
    RC = (Dlg->ShowModal() == mrOk);
   }
  __finally
   {
    if (FCardVacDataSrc->DataSet->State == dsInsert)
     FCardVacDataSrc->DataSet->Cancel();
    delete Dlg;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsICCardDM::PlanCheckInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData * ATmplData,
  bool ASaveTmpl)
 {
  bool RC = false;
  /*
   TdsCardTestEditForm *Dlg = new TdsCardTestEditForm(this, true, "102F", (__int64)FPlanDataSrc->DataSet->CurRow->Value["317D"], FCardDM, FCardTestNode->GetRoot(), FPatBirthday, FCardTestDataSrc, FTemplateData, true);
   try
   {
   if(Dlg->ShowModal() == mrOk)
   {
   CanRefresh = true;
   CanCancel  = false;
   curCD = Dlg->InsertedRecId.ToIntDef(0);
   }
   else
   CanRefresh = CanReopen;
   }
   __finally
   {
   delete Dlg;
   }
   */
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsICCardDM::PlanCancelInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData * ATmplData,
  bool ASaveTmpl)
 {
  bool RC = false;
  TdsCardOtvodEditForm * Dlg = new TdsCardOtvodEditForm(this, true, "3030", AUCode, this, FCardCancelNode->GetRoot(),
    APatBirthday, FCardCancelDataSrc, ATmplData, ASaveTmpl);
  // TdsCardOtvodEditForm(TComponent * Owner, bool AIsAppend, UnicodeString  AClassId, __int64 AUCode, TdsICCardDM * ADM, TTagNode * ARoot,           TDate APatBirthday, TkabCustomDataSource * ADataSrc, TdsRegTemplateData * ATmplData, bool AOneCol)
  try
   {
    RC = (Dlg->ShowModal() == mrOk);
   }
  __finally
   {
    if (FCardVacDataSrc->DataSet->State == dsInsert)
     FCardVacDataSrc->DataSet->Cancel();
    delete Dlg;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsICCardDM::InfInPlan(TTagNode * AComRoot, TTagNode * APersRoot, UnicodeString AInf)
 {
  int RC = 0;
  try
   {
    if (AComRoot)
     RC = (int)(bool)((int)AComRoot->GetChildByAV("vpr", "infref", AInf));
    if (APersRoot && !RC)
     RC = (int)(bool)((int)APersRoot->GetChildByAV("vpr", "infref", AInf));
    if (RC)
     RC = 1;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICCardDM::GetPlanVacVar(UnicodeString AUCode, TDate ADate)
 {
  if (FOnGetPlanVacVar)
  return FOnGetPlanVacVar(AUCode, ADate);
  else
  return "";
 }
// ---------------------------------------------------------------------------

