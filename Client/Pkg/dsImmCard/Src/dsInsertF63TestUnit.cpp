﻿// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsInsertF63TestUnit.h"
#include "msgdef.h"

// ---------------------------------------------------------------------------
#define _TestDate   0
#define _Test       1
#define _Ser        2
#define _ReakType   3
#define _Reakval    4
#define _TestId     5
#define _ReakTypeId 6
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxBarEditItem"
#pragma link "cxClasses"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxDropDownEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLabel"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma link "cxCalendar"
#pragma link "cxContainer"
#pragma link "cxEdit"
#pragma link "cxProgressBar"
#pragma link "dxBarExtItems"
#pragma resource "*.dfm"
TdsInsertF63TestForm * dsInsertF63TestForm;
// ---------------------------------------------------------------------------
__fastcall TdsInsertF63TestForm::TdsInsertF63TestForm(TComponent * Owner, TdsICCardDM * ADM, __int64 AUCode,
 TDateTime ABirthDay) : TForm(Owner)
 {
  FDM       = ADM;
  FUCode    = AUCode;
  FBirthDay = ABirthDay;
  FDM->GetTestList(((TcxComboBoxProperties *)TestCol->Properties)->Items);

  FInsTestExecutor = new TInsertCardItem(FDM->RegDef->GetTagByUID("102f"),
   FDM->XMLList->GetXML("001D3500-00005882-DC28"));
  FInsTestExecutor->OnGetValById = FDM->OnGetValById;
  FInsTestExecutor->OnInsertData = FDM->OnInsertData;
  FInsTestExecutor->InLPU        = 1;
  FInsTestExecutor->LPU          = FDM->LPUCode;
  FInsTestExecutor->Preload();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::CreateList()
 {
  TestTL->Clear();
  TDateTime TestDate = 0;
  if (PeriodCB->ItemIndex == 0)
   TestDate = IncMonth(FBirthDay, 6);
  else
   TestDate = IncYear(FBirthDay, 1);
  TcxTreeListNode * TestNode;
  while (TestDate < Date())
   {
    TestNode                     = TestTL->Root->AddChild();
    TestNode->Values[_TestDate]  = TestDate;
    TestNode->Texts[_Test]       = "Манту";
    TestNode->Texts[_Ser]        = "";
    TestNode->Texts[_ReakType]   = "Папула";
    TestNode->Texts[_Reakval]    = "";
    TestNode->Texts[_TestId]     = "1";
    TestNode->Texts[_ReakTypeId] = "2";

    if (PeriodCB->ItemIndex == 0)
     TestDate = IncMonth(TestDate, 6);
    else
     TestDate = IncYear(TestDate, 1);
   }
  if (Visible)
   {
    TestTL->SetFocus();
    if (TestTL->Root->Count)
     {
      TestTL->Root->Items[0]->Focused = true;
      ReacValCol->Focused             = true;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::actRestoreExecute(TObject * Sender)
 {
  CreateList();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::actDeleteExecute(TObject * Sender)
 {
  TcxTreeListNode * delNode = NULL;
  try
   {
    if (TestTL->SelectionCount)
     {
      delNode = TestTL->Selections[0];

      if (delNode)
       delNode->Delete();
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::actApplyExecute(TObject * Sender)
 {
  int MIBPCode, TestCode, CheckCode, ReacId;
  TDate FExecDate;
  TcxTreeListNode * FCurRow;
  UnicodeString FInfCode, FRecID, FRC;
  TkabCustomDataSetRow * RecData;
  if (TestTL->IsEditing)
   TestTL->HideEdit();
  try
   {
    if (CheckInput())
     {
      PBar->Position        = 0;
      PBar->Properties->Max = TestTL->Root->Count * 2;
      for (int i = 0; i < TestTL->Root->Count; i++)
       {
        PBar->Position++ ;
        Application->ProcessMessages();

        FCurRow = TestTL->Root->Items[i];

        FExecDate = StrToDate(FCurRow->Texts[_TestDate]);
        ReacId    = FCurRow->Texts[_ReakTypeId].ToIntDef(0);
        FInsTestExecutor->NewRec(FBirthDay);

        TestCode = FCurRow->Texts[_TestId].ToIntDef(0);
        FInfCode = FInsTestExecutor->GetTestInf(IntToStr(TestCode));

        RecData                = FInsTestExecutor->Data;
        RecData->Value["1031"] = FExecDate;            // date name='Дата выполнения' uid='1031'
        RecData->Value["1032"] = TestCode;             // choiceDB name='Проба' uid='1032'
        RecData->Value["1200"] = FInfCode;             // choiceDB name='Инфекция' uid='1200'
        RecData->Value["1033"] = FCurRow->Texts[_Ser]; // text name='Серия препарата' uid='1033'
        RecData->Value["1034"] = 0;                    // choice name='Повод' uid='1034' >> План' value='0'
        if (ReacId)                                    // задан формат реакции
         {
          RecData->Value["103B"] = 1;      // binary name='Реакция проверена' uid='103B'
          RecData->Value["10A7"] = ReacId; // choiceDB name='Формат реакции' uid='10A7'
          // if (ReakValEnabled[ReacId])      // есть значение реакции
          RecData->Value["103F"] = FCurRow->Texts[4]; // digit name='Значение' uid='103F'
         }
        RecData->Value["1093"] = FInsTestExecutor->GetSchLineByVacVar("Пр.1", TestCode);
        // FCurRow->Texts[plVacType /* 2 */]; // extedit name='Схема' uid='1093'
        FInsTestExecutor->Execute(FUCode, FRecID, FRC);

        PBar->Position++ ;
        Application->ProcessMessages();
       }
     }
   }
  __finally
   {
    delete FInsTestExecutor;
    PBar->Position = 0;
   }
  ModalResult = mrOk;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::PeriodCBChange(TObject * Sender)
 {
  CreateList();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::ReacValColPropertiesValidate(TObject * Sender, Variant & DisplayValue,
 TCaption & ErrorText, bool & Error)
 {
  Error = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::FormShow(TObject * Sender)
 {
  CreateList();
 }
// ---------------------------------------------------------------------------
TcxTreeListNode * __fastcall TdsInsertF63TestForm::GetSelNode()
 {
  TcxTreeListNode * RC = NULL;
  try
   {
    if (TestTL->SelectionCount)
     {
      RC = TestTL->Selections[0];
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::TestColPropertiesEditValueChanged(TObject * Sender)
 {
  TcxTreeListNode * FCurRow = GetSelNode();
  FCurRow->Texts[_ReakType]   = "";
  FCurRow->Texts[_Reakval]    = "";
  FCurRow->Texts[_TestId]     = "";
  FCurRow->Texts[_ReakTypeId] = "";
  if (FCurRow && Sender)
   {
    TcxComboBox * FCB = (TcxComboBox *)Sender;
    if (FCB->ItemIndex != -1)
     FCurRow->Values[5] = (int)(FCB->Properties->Items->Objects[FCB->ItemIndex]);
   }
  ButtonEnabled();
 }
// ---------------------------------------------------------------------------

void __fastcall TdsInsertF63TestForm::SerEDPropertiesValidate(TObject * Sender, Variant & DisplayValue,
 TCaption & ErrorText, bool & Error)
 {
  Error = false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsInsertF63TestForm::CheckInput()
 {
  bool RC = TestTL->Count;
  TcxTreeListNode * tmp = GetSelNode();
  int idx = -1;
  if (tmp)
   idx = tmp->Index;
  UnicodeString FValue;
  try
   {
    TcxTreeListNode * ndCh;
    for (int i = 0; (i < TestTL->Count) && RC; i++)
     {
      ndCh = TestTL->Root->Items[i];

      FValue = GetTLValue(ndCh, _TestDate);
      RC &= FValue.Trim().Length();
      if (RC)
       {
        RC &= GetTLValue(ndCh, _Test).Length();
        if (RC)
         {
          RC = GetTLValue(ndCh, _ReakType).Length();
          if (RC)
           RC &= FInsTestExecutor->CheckInput(ndCh->Texts[_TestId], ndCh->Texts[_ReakTypeId],
           GetTLValue(ndCh, _Reakval), true);
         }
       }
      Application->ProcessMessages();
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::ReacTypeColGetEditingProperties(TcxTreeListColumn * Sender,
 TcxTreeListNode * ANode, TcxCustomEditProperties *& EditProperties)
 {
  TcxTreeListNode * FCurRow = GetSelNode();
  if (FCurRow)
   {
    int TestCode = FCurRow->Texts[_TestId].ToIntDef(0);
    FInsTestExecutor->FillReacVar(((TcxCustomComboBoxProperties *)EditProperties)->Items, TestCode, 2);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::ReacTypeColPropertiesEditValueChanged(TObject * Sender)
 {
  TcxTreeListNode * FCurRow = GetSelNode();
  FCurRow->Texts[_ReakTypeId] = "";
  if (FCurRow && Sender)
   {
    TcxComboBox * FCB = (TcxComboBox *)Sender;
    if (FCB->ItemIndex != -1)
     {
      FCurRow->Values[_ReakTypeId] = (int)(FCB->Properties->Items->Objects[FCB->ItemIndex]);
     }
   }
  ButtonEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::ButtonEnabled()
 {
  actApply->Enabled = CheckInput();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::VacTypeColPropertiesValidate(TObject * Sender, Variant & DisplayValue,
 TCaption & ErrorText, bool & Error)
 {
  Error = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::BirthDayColPropertiesEditValueChanged(TObject * Sender)
 {
  TcxTreeListNode * tmp = GetSelNode();
  if (tmp)
   {
    TDate execDate;

    UnicodeString FValue;
    FValue = GetTLValue(tmp, _TestDate);

    // if (TestTL->IsEditing)
    // FValue = VarToStr(TestTL->InplaceEditor->EditingValue);
    // else
    // FValue = tmp->Texts[0];

    if (TryStrToDate(FValue, execDate))
     {
      if (execDate > Date())
       {
        _MSG_ERR("Дата выполнения не может быть больше текущей даты.", "Ошибка");
        TestTL->CancelEdit();
       }
     }
   }
  ButtonEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::TestTLEditing(TcxCustomTreeList * Sender, TcxTreeListColumn * AColumn,
 bool & Allow)
 {
  Allow = false;
  TcxTreeListNode * FCurRow = GetSelNode();
  if (FCurRow && AColumn)
   {
    if (AColumn->Position->ColIndex < 3)
     {
      Allow = true;
     }
    else
     {
      Allow = FCurRow->Texts[_TestId].ToIntDef(0);
      if (AColumn->Position->ColIndex == _Reakval && Allow)
       Allow = FInsTestExecutor->ReakCanVal(FCurRow->Texts[_ReakTypeId].ToIntDef(0));
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsInsertF63TestForm::TestColPropertiesChange(TObject *Sender)
{
  ButtonEnabled();
}
//---------------------------------------------------------------------------

