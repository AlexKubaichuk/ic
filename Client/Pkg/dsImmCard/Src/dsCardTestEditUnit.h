﻿//---------------------------------------------------------------------------
#ifndef dsCardTestEditUnitH
#define dsCardTestEditUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
//---------------------------------------------------------------------------
#include "KabCustomDS.h"
#include "dsICCardDMUnit.h"
//---------------------------------------------------------------------------
class PACKAGE TdsCardTestEditForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *CompPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
 TcxComboBox *TypeCB;
 TLabel *TypeLab;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
 void __fastcall TypeCBPropertiesChange(TObject *Sender);
private:	// User declarations
  __int64 FUCode, FTestCode, FPlanCode;
  TdsRegTemplateData * FTmplData;
  bool FSaveTmpl;
  TDate FTestDate;
    TdsICCardDM *FDM;
    TkabCustomDataSource *FDataSrc;
    bool     FIsAppend, FCheckPlan;
//    TTagNode *Root;
    TTagNode *FClassDef,*FClassDesc;
    UnicodeString  FInsertedRecId;
  TDate FPatBirthday;
    TdsRegEDItem   *LastLeftCtrl;
    TdsRegEDItem   *LastCtrl;
    TcxPageControl *Group1st;
    TcxTabSheet    *gPage;

    TdsRegEDContainer *FCtrList;
    int FLastTop,FLastHeight;

    void __fastcall AlignCtrls(TList *ACtrlList);
    bool __fastcall GetInput(TTagNode *uiTag, UnicodeString &UID);
    bool __fastcall CheckInput();
    bool __fastcall FCtrlDataChange(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource);
    bool __fastcall FSetFocus(TTagNode *itxTag);
    void __fastcall CreateUnitList(TWinControl *AParent, TTagNode* ANode,int &ATop,int *ALeft,TList *GroupComp,int APIndex = 0);
    int  __fastcall SetAlign(TdsRegEDItem* AItem, int ALeft, int ATop, int AWidth, int ATabOrder);
    inline TdsRegEDItem* __fastcall RegComp(UnicodeString AUID);
    inline TdsRegEDItem* __fastcall AddRegED(UnicodeString AUID);

    bool __fastcall InThisLPUChBChange(TTagNode *ItTag, UnicodeString &Src);
  bool __fastcall LPUCBChange(TTagNode * ItTag, UnicodeString & Src);
    bool __fastcall CBChange(TTagNode *ItTag, UnicodeString &Src);

public:
    __property UnicodeString  InsertedRecId = {read=FInsertedRecId};
    __fastcall TdsCardTestEditForm(TComponent* Owner, bool AIsAppend, __int64 AUCode, TdsICCardDM *ADM, TTagNode* ARoot, TDate APatBirthday, TkabCustomDataSource *ADataSrc, TdsRegTemplateData *ATmplData, bool ACheckPlan = false, bool ASaveTmpl = false);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsCardTestEditForm *dsCardTestEditForm;
//---------------------------------------------------------------------------
#endif
