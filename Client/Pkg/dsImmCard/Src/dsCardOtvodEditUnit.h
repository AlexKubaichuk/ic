﻿//---------------------------------------------------------------------------
#ifndef dsCardOtvodEditUnitH
#define dsCardOtvodEditUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxCheckListBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
#include "KabCustomDS.h"
#include "dsICCardDMUnit.h"
//---------------------------------------------------------------------------
class PACKAGE TdsCardOtvodEditForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *CompPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
 TActionList *ActionList1;
 TAction *actApply;
 TPopupMenu *SelPM;
 TMenuItem *N1;
 TMenuItem *N2;
 TLabel *OtvodAgeLab;
 TPanel *InfPanel;
 TPanel *Panel1;
 TPanel *Panel2;
 TcxComboBox *InfTypeCB;
 TcxCheckListBox *InfChLB;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
 void __fastcall InfTypeCBPropertiesChange(TObject *Sender);
private:	// User declarations
  __int64 FUCode;
    TdsICCardDM *FDM;
    TkabCustomDataSource *FDataSrc;
    TStringList *FEditInfList;
    bool     FIsAppend, FOneCol;
//    TTagNode *Root;
    TTagNode *FClassDef,*FClassDesc;
    int      ColWidth, SubLevel;
    UnicodeString  FInsertedRecId;
  TDate FPatBirthday;
    TdsRegEDItem   *LastLeftCtrl;
    TdsRegEDItem   *LastCtrl;
    TcxPageControl *Group1st;
    TcxTabSheet    *gPage;

    TdsRegEDContainer *FCtrList;
    int FLastTop,FLastHeight;

  void __fastcall LoadInfList();
  bool __fastcall FTypeDateChange(TTagNode *ItTag, UnicodeString &Src);
  void __fastcall SetInfList();
    void __fastcall AlignCtrls(TList *ACtrlList);
    bool __fastcall GetInput(TTagNode *uiTag, UnicodeString &AValid);
    bool __fastcall CheckInput();
    bool __fastcall FCtrlDataChange(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource);
    bool __fastcall FSetFocus(TTagNode *itxTag);
    void __fastcall CreateUnitList(TWinControl *AParent, TTagNode* ANode,int &ATop,int *ALeft,TList *GroupComp,int APIndex = 0);
    int  __fastcall SetAlign(TdsRegEDItem* AItem, int ALeft, int ATop, int AWidth, int ATabOrder);
    inline TdsRegEDItem* __fastcall RegComp(UnicodeString AUID);
    inline TdsRegEDItem* __fastcall AddRegED(UnicodeString AUID);
public:
    __property UnicodeString  InsertedRecId = {read=FInsertedRecId};
    __fastcall TdsCardOtvodEditForm(TComponent* Owner, bool AIsAppend, UnicodeString AClassId, __int64 AUCode, TdsICCardDM *ADM, TTagNode* ARoot, TDate APatBirthday, TkabCustomDataSource *ADataSrc, TdsRegTemplateData *ATmplData, bool AOneCol = true);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsCardOtvodEditForm *dsCardOtvodEditForm;
//---------------------------------------------------------------------------
#endif
