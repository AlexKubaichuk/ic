﻿//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsCardEditClientUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma resource "*.dfm"
TdsCardEditClientForm *dsCardEditClientForm;
//---------------------------------------------------------------------------
const int LeftMargin = 2;
const int LevelCorrection = 5;
__fastcall TdsCardEditClientForm::TdsCardEditClientForm(TComponent* Owner, bool AIsAppend, UnicodeString AClassId, __int64 AUCode, TdsICCardDM *ADM, TTagNode* ARoot, TkabCustomDataSource *ADataSrc, TdsRegTemplateData *ATmplData, bool AOneCol)
    : TForm(Owner)
{
  FUCode = AUCode;
  FDM = ADM;
  FDataSrc = ADataSrc;
  FOneCol = AOneCol;
  FInsertedRecId = "";

  OkBtn->Caption = FMT(icsRegClassifEditApplyBtnCaption);
  CancelBtn->Caption = FMT(icsRegClassifEditCancelBtnCaption);
  FIsAppend = AIsAppend;
  if (FIsAppend)
   FDataSrc->DataSet->Insert();
  else
   FDataSrc->DataSet->Edit();

  FClassDef = ARoot->GetTagByUID(AClassId);
  FClassDesc = FClassDef->GetChildByName("description");

  FCtrList                  = new TdsRegEDContainer(this,CompPanel);
  FCtrList->UseHash         = true;
  FCtrList->DefXML          = ARoot;
  FCtrList->ReqColor        = FDM->RequiredColor;
  FCtrList->StyleController = FDM->StyleController;
  FCtrList->TemplateData    = ATmplData;
  FCtrList->DataPath        = FDM->DataPath;

  FCtrList->DataProvider = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider*)FCtrList->DataProvider)->DefXML = ARoot;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  ((TdsRegEDkabDSDataProvider*)FCtrList->DataProvider)->DataSource    = FDataSrc;
  FCtrList->DataProvider->OnGetCompValue       = FDM->OnGetCompValue;

  FCtrList->OnDataChange         = FCtrlDataChange;
  FCtrList->OnGetExtBtnGetBitmap = FDM->OnGetExtBtnGetBitmap;
  FCtrList->OnGetFormat          = FDM->OnGetFormat;
  FCtrList->OnExtBtnClick        = FDM->OnExtBtnClick;
  FCtrList->OnKeyDown            = FormKeyDown;


  ColWidth = FDM->ULEditWidth/2+FOneCol*50;

  int FGroupCount = 0;
  bool FGroupOnly = true;

  TTagNode *itNode = FClassDef->GetFirstChild();
  while (itNode)
   {
     if (!itNode->CmpName("description"))
      {
        if (itNode->CmpName("group"))
         FGroupCount++;
        else
         FGroupOnly = false;
      }
     itNode = itNode->GetNext();
   }


  FLastTop = 0;
  FLastHeight = 0;
  int xTop,xLeft;

  LastLeftCtrl = NULL;
  int MaxH = 0;
  TList *CntrlList = new TList;

  TTagNode *FDefNode = NULL;

  if (FGroupOnly)
   FDefNode = FClassDesc->GetNext();
  else
   FDefNode = FClassDef;
  SubLevel = 0;
  xTop = 2; xLeft = LeftMargin;
  LastCtrl = NULL;
  CreateUnitList(CompPanel, FDefNode, &xTop, &xLeft, CntrlList);
  if (LastLeftCtrl)
   {
    if ((LastLeftCtrl->Top+LastLeftCtrl->Height) <= xTop)
     MaxH = xTop;
    else
     MaxH = LastLeftCtrl->Top+LastLeftCtrl->Height;
   }
  else
   MaxH = xTop;
  ClientHeight = MaxH+50;
  Width = ColWidth+(!FOneCol)*ColWidth+(Width-CompPanel->Width)+LeftMargin*2;
  FCtrList->SetOnCtrlDataChange(0);
  FClassDef->GetFirstChild();
  FClassDef->Iterate(FSetFocus);
  delete CntrlList; CntrlList = NULL;
  if (FIsAppend)
   {
     bool valid;
     if (FDM->OnGetDefValues)
      FDM->OnGetDefValues(-1,FClassDef,FCtrList,valid);
     Caption = FClassDef->GetChildByName("description")->AV["insertlabel"];
   }
  else
   Caption = FClassDef->GetFirstChild()->AV["editlabel"];
}
//---------------------------------------------------------------------------
bool __fastcall TdsCardEditClientForm::FSetFocus(TTagNode *itxTag)
{
  TControl *tmpCtrl;
  if (itxTag->GetAttrByName("uid"))
   {
     if (FCtrList->GetEDControl(itxTag->AV["uid"]))
      {
        tmpCtrl = FCtrList->EditItems[itxTag->AV["uid"]]->GetFirstEnabledControl();
        if (tmpCtrl)
         {
           ActiveControl = (TWinControl*)tmpCtrl;
           return true;
         }
      }
   }
  return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsCardEditClientForm::CreateUnitList(TWinControl *AParent, TTagNode* ANode, int *ATop, int *ALeft, TList *GroupComp, int APIndex)
{
  TTagNode *itNode = ANode->GetFirstChild();
  while (itNode)
   {
     if (!itNode->CmpName("description"))
      {
        TdsRegEDItem *uCtrl = FCtrList->AddEditItem(itNode, FIsAppend, AParent);
        uCtrl->Tag = APIndex;
        if (itNode->CmpAV("isedit","0")&& !itNode->CmpAV("depend",""))
         {
           uCtrl->Visible = false;
         }
        else
         {
           GroupComp->Add(uCtrl);

           uCtrl->Top = *ATop;
           uCtrl->Left = *ALeft;
           TTagNode *FParentNode = uCtrl->DefNode->GetParent();
           bool ParentFirstLvl = FParentNode->CmpName("class") || (FParentNode->CmpName("group") &&  FParentNode->GetParent()->CmpName("class"));
           if (ParentFirstLvl)
            { // элемент подчинён группе 1-го уровня или классу, если нет групп
              if (!LastLeftCtrl)
               { // последний левый контрол отсутствует
                 LastLeftCtrl = uCtrl;
                 if (!FOneCol) *ALeft = ColWidth+LeftMargin;        // сдвигаем левую границу вправо
               }
              else
               { //перенос право-верх, первого контрола
                 if (!FOneCol && ((LastLeftCtrl->Top+LastLeftCtrl->Height) <= *ATop))
                  {
                    if ((!LastCtrl)||LastCtrl&&(LastCtrl->Left < ColWidth))
                     {
                       *ATop = LastLeftCtrl->Top;
                        uCtrl->Top = *ATop;
                     }
                  }
               }
            }
           uCtrl->Width = ColWidth-LeftMargin-(uCtrl->DefNode->Level-SubLevel)*2*LeftMargin;//LevelCorrection;  // ширина контрола "левая граница" - 15
           if (uCtrl->DefNode->CmpName("group"))
            {
              int xTop, xLeft; xTop = 13; xLeft = LeftMargin;
              TList *GrCtrl = new TList;
              try
               {
                 // формируем список контролов группы
                 CreateUnitList(uCtrl, uCtrl->DefNode, &xTop, &xLeft, GrCtrl, APIndex);
                 uCtrl->Height = xTop+5;
                 *ATop = *ATop+3;   //????
                 AlignCtrls(GrCtrl);
               }
              __finally
               {
                 delete GrCtrl;
               }
            }
           *ATop = *ATop + uCtrl->Height+1;
           if (ParentFirstLvl)
            { // элемент подчинён группе 1-го уровня или классу, если нет групп
              if (LastLeftCtrl && ((uCtrl->Top+uCtrl->Height) > (LastLeftCtrl->Top+LastLeftCtrl->Height)))
               {
                 if (!FOneCol && ((uCtrl->Top+uCtrl->Height) > (LastLeftCtrl->Top+LastLeftCtrl->Height+15)))
                  {
                    uCtrl->Top = LastLeftCtrl->Top+LastLeftCtrl->Height+2;
                    uCtrl->Left = 2;
                    LastLeftCtrl = uCtrl;
                    *ALeft = ColWidth+LeftMargin;
                    *ATop = uCtrl->Top;
                  }
                 else
                  {
                    LastLeftCtrl->Constraints->MaxHeight = uCtrl->Top+uCtrl->Height - LastLeftCtrl->Top;
                    LastLeftCtrl->Height = LastLeftCtrl->Constraints->MaxHeight;
                    *ATop = *ATop+3;
                  }
               }
            }
           LastCtrl = uCtrl;
         }
      }
     itNode = itNode->GetNext();
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsCardEditClientForm::AlignCtrls(TList *ACtrlList)
{
  int LabR,EDL;
  int xLabR,xEDL;
  LabR = 0; EDL = 9999;
  TdsRegEDItem *tmpED;
  for (int i = 0; i < ACtrlList->Count; i++)
   {
     // определяем max правую границу меток контролов
     // и min левую самих контролов
     tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
     tmpED->GetEDLeft(&xLabR,&xEDL);
     if (xLabR != -1) LabR = max(xLabR,LabR);
     if (xEDL != -1)  EDL  = min(xEDL,EDL);
   }
  if ((LabR+2) < EDL)
   {
     // max правая граница меток контролов
     // и min левая самих контролов не пересекаются
     // выравниваем контролы по min левой границе самих контролов
     for (int i = 0; i < ACtrlList->Count; i++)
      {
        tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
        tmpED->SetEDLeft(EDL);
      }
   }
  else
   {
     // иначе выравниваем по "max правая граница меток контролов" + 2
     for (int i = 0; i < ACtrlList->Count; i++)
      {
        tmpED = ((TdsRegEDItem*)ACtrlList->Items[i]);
        tmpED->SetEDLeft(LabR+2);
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsCardEditClientForm::FormKeyDown(TObject *Sender, WORD &Key,  TShiftState Shift)
{
  if (Key == VK_RETURN)
   {ActiveControl = OkBtn; OkBtnClick(this);}
}
//---------------------------------------------------------------------------
void __fastcall TdsCardEditClientForm::OkBtnClick(TObject *Sender)
{
  TWinControl *tmpCtrl = ActiveControl;
  ActiveControl = OkBtn;
  ActiveControl = tmpCtrl;
  if (CheckInput())
   { // запрос на проверку данных
     try
      {
        UnicodeString PRC = FDataSrc->DataSet->Post(FInsertedRecId, true);
        if (!SameText(PRC,"ok"))
          _MSG_ERR(PRC,"Ошибка");
        else
         ModalResult = mrOk;
      }
     catch (EkabCustomDataSetError &E)
      {
        _MSG_ERR(E.Message,"Ошибка");
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TdsCardEditClientForm::CheckInput()
{
  bool RC = false;
  try
   {
     if (FIsAppend)
      FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
     else
      FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
     UnicodeString xValid = "";
     FClassDef->Iterate(GetInput,xValid);
     if (!xValid.Length())
      {
        RC = true;
        if (FDM->OnExtValidate)
         {
           if (FIsAppend)
            FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
           else
            FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCardEditClientForm::GetInput(TTagNode *itTag, UnicodeString &UID)
{
   if (itTag)
    {
      if (FCtrList->GetEDControl(itTag->AV["uid"]))
       if (!FCtrList->EditItems[itTag->AV["uid"]]->CheckReqValue())
        {
           UID = "no";
           return true;
        }
    }
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsCardEditClientForm::FormDestroy(TObject *Sender)
{
   FCtrList->ClearOnCtrlDataChange(0);
   delete FCtrList->DataProvider;
   delete FCtrList;
}
//---------------------------------------------------------------------------
bool __fastcall TdsCardEditClientForm::FCtrlDataChange(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource)
{
  bool RC = false;
  if (FDM->OnCtrlDataChange)
   {
     RC = FDM->OnCtrlDataChange(ItTag, Src, ASource, FIsAppend, FCtrList, UIDInt(FClassDef->AV["uid"]));
   }
  return RC;
}
//---------------------------------------------------------------------------

