//---------------------------------------------------------------------------
#ifndef dsICCardTypeDefH
#define dsICCardTypeDefH
//---------------------------------------------------------------------------
#include "dsICCommonTypeDef.h"
//---------------------------------------------------------------------------
typedef bool __fastcall (__closure *TdsICCardGetUnitDataEvent)(__int64 ACode, TkabCustomDataSetRow *& AData);
typedef UnicodeString __fastcall (__closure *TdsGetUnitSettingEvent)(__int64 ACode);
typedef void __fastcall (__closure *TdsSetUnitSettingEvent)(__int64 APatCode, System::UnicodeString AData);
typedef void __fastcall (__closure *TdsHTMLPreviewEvent)(System::UnicodeString AData);

typedef UnicodeString __fastcall (__closure *TdsCreateUnitPlanEvent)(__int64 ACode);
typedef UnicodeString __fastcall (__closure *TdsGetPlanVacVarEvent)(UnicodeString AUCode, TDate ADate);
//---------------------------------------------------------------------------
#endif

