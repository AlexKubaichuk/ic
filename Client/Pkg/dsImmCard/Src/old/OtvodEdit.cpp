//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//#define lastComp ((TWinControl*)CList->Objects[CList->Count-1])
#define RegComp(a) FCtrList->EditItems[a]
//#define SetLTop   {tmpItem->Left = 2;   tmpItem->Top = xTop+3; tmpItem->Anchors.Clear(); tmpItem->Anchors << akLeft << akRight << akTop;}
#define AddRegED(a) FCtrList->AddEditItem(AClsNode->GetTagByUID(a), !AIsEdit)
#define DSInt(a)    DS->FN(a)->AsInteger
#define DSStr(a)    DS->FN(a)->AsString
#define DSDate(a)   DS->FN(a)->AsDateTime
#define DSDateF(a)  DS->FN(a)->AsDateTime.FormatString("dd.mm.yyyy")

#include "OtvodEdit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma resource "*.dfm"
TOtvodEditForm *OtvodEditForm;
//---------------------------------------------------------------------------
__fastcall TOtvodEditForm::TOtvodEditForm(TComponent* Owner, TTagNode *AClsNode,
      TDataSource *ASource, long UCode, bool AIsEdit)
        : TForm(Owner)
{
   TRegEDItem *tmpItem;
   SetCODE = 0;
   isEdit = AIsEdit;
   UCODE = UCode;
   DS = (TpFIBDataSet*)ASource->DataSet;
   if (AIsEdit) DS->Edit();
   else         DS->Insert();

   FCtrList = new TRegEDContainer(this,CompPanel);
   FCtrList->DefXML = DM->xmlImmCardDef;
   FCtrList->ReqColor   = DM->ReqColor;
   FCtrList->TablePref  = "";
   FCtrList->DataSource = ASource;
   FCtrList->FIBTemplateQuery = DM->quFree;
   int FTabOrder = 0;
   int xTop = 5;
   tmpItem = AddRegED("1043"); xTop += SetAlign(tmpItem, true, xTop, 300, FTabOrder++); //   <choice name='�����' uid='1043' required='yes' inlist='list' default='0'>
   InfChLB->Top = xTop+3;
   tmpItem = AddRegED("1083"); tmpItem->Visible = false;                      //   <choiceBD name='���������� ��' uid='1083' ref='003A' required='yes' inlist='list'/>
   xTop += InfChLB->Height+1; InfChLB->TabOrder = FTabOrder++;
   SelAllBtn->Top   = xTop+3;
   DeSelAllBtn->Top = xTop+3;
   xTop += 32;
   SelAllBtn->TabOrder = FTabOrder++;  DeSelAllBtn->TabOrder = FTabOrder++;
   tmpItem = AddRegED("105F"); xTop += SetAlign(tmpItem, true, xTop, 300, FTabOrder++); //   <date name='������ ��������' uid='105F' required='yes' inlist='list'/>
   tmpItem = AddRegED("1065"); xTop += SetAlign(tmpItem, true, xTop, 300, FTabOrder++); //   <date name='��������� ��������' uid='1065' required='yes' inlist='list'>
   tmpItem = AddRegED("1066"); xTop += SetAlign(tmpItem, true, xTop, 300, FTabOrder++); //   <choice name='�������' uid='1066' required='yes' inlist='list_ext' default='0'>
   tmpItem = AddRegED("10A0"); xTop += SetAlign(tmpItem, true, xTop, 300, FTabOrder++); //   <binary name='������������������ �� ���-10' uid='10A0' required='yes' inlist='list_ext' default='check'>
   tmpItem = AddRegED("1082"); xTop += SetAlign(tmpItem, true, xTop, 300, FTabOrder++); //   <choiceTree name='�����������' uid='1082' ref='2000' required='yes' inlist='list_ext' dlgtitle='���-10' linecount='2'>
   RegComp("1082")->TreeClasses = DM->ImmCard->TreeFormList;
   tmpItem = AddRegED("10A1"); xTop += SetAlign(tmpItem, true, xTop, 300, FTabOrder++); //   <text name='�������� �����������' uid='10A1' required='yes' inlist='list_ext' length='255'>
   DM->qtExec("Select CODE, R003C From CLASS_003A order by R003C");
   while (!DM->quFree->Eof)
    {
      InfChLB->Items->AddObject(quFNStr("R003C"),(TObject*)quFNInt("CODE"));
      DM->quFree->Next();
    }
   DM->trFree->Commit();
   if (AIsEdit)
    {
      oldType = DSInt("R1043");
      oldFromDate = DSDate("R105F");
      if (oldType == 0)
       { // ���������
         oldToDate = DSDate("R1065");
         DM->qtExec("Select R1083 From CARD_1030 Where UCODE="+DSStr("UCODE")+" and R1043="+DSStr("R1043")+" and R105F='"+DSDateF("R105F")+"' and R1065='"+DSDateF("R1065")+"'" );
       }
      else
       { // ����������
         oldToDate = TDateTime(0);
         DM->qtExec("Select R1083 From CARD_1030 Where UCODE="+DSStr("UCODE")+" and R1043="+DSStr("R1043")+" and R105F='"+DSDateF("R105F")+"'" );
       }
      while (!DM->quFree->Eof)
       {
         InfChLB->Checked[InfChLB->Items->IndexOfObject((TObject*)quFNInt("R1083"))] = true;
         DM->quFree->Next();
       }
      DM->trFree->Commit();
      Caption = DM->xmlImmCardDef->GetTagByUID("1030")->GetChildByName("description")->AV["editlabel"];
    }
   else
    Caption = DM->xmlImmCardDef->GetTagByUID("1030")->GetChildByName("description")->AV["insertlabel"];
   InfChLB->Color = DM->ReqColor;
   InfChLB->HeaderBackgroundColor = DM->ReqColor;
}
//---------------------------------------------------------------------------
void __fastcall TOtvodEditForm::OkBtnClick(TObject *Sender)
{
   if (!CheckInput()) return;
   ActiveControl = OkBtn;
   if (DSInt("R1043") == 0)
    { // ���������
      DM->qtExec("Select R1083 From CARD_1030 Where UCODE="+IntToStr(UCODE)+" and R1043=0 and R105F='"+DSDateF("R105F")+"' and R1065='"+DSDateF("R1065")+"'" );
      if (DM->quFree->RecordCount > 0)
       {
         if (!(isEdit&&(DSDate("R105F") == oldFromDate)&&(DSDateF("R1065") == oldToDate)))
          {
            ShowMessage(("��������� ����� � '"+DSDateF("R105F")+"' �� '"+DSDateF("R1065")+"' ��� ����������, ������� ������ ����.").c_str());
            return;
          }
       }
    }
   else
    { // ����������
      DM->qtExec("Select R1083 From CARD_1030 Where UCODE="+IntToStr(UCODE)+" and R1043=1 and R105F='"+DSDateF("R105F")+"'");
      if (DM->quFree->RecordCount > 0)
       {
         if (!(isEdit&&(DSDate("R105F") == oldFromDate)&&(DSDateF("R1065") == oldToDate)))
          {
            ShowMessage(("���������� ����� � '"+DSDateF("R105F")+"' ��� ����������, ������� ������ ����.").c_str());
            return;
          }
       }
    }
   TStringList *tmp = new TStringList;
   TList *FDeleteCodes = new TList;
   try
    {
      tmp->Add(DSStr("R1043")); tmp->Add(DSDateF("R105F"));
      if (DSInt("R1043") != 0)  tmp->Add("");
      else                      tmp->Add(DSDateF("R1065"));
      tmp->Add(DSStr("R1066")); tmp->Add(DSStr("R10A0")); tmp->Add(DSStr("R1082"));
      if (DSInt("R10A0") == 0)  tmp->Add(DSStr("R10A1"));
      else                      tmp->Add("");
      for (int i = 0; i < tmp->Count; i++)
       if (!tmp->Strings[i].Length()) tmp->Strings[i] = "NULL";
      DSCancel();
      if (isEdit)
       { // ���� ������� :)(�������������), ��������� ����� �������
         if (oldType == 0)
          { // ���������
            DM->qtExec("Select CODE From CARD_1030 Where UCODE="+IntToStr(UCODE)+" and R1043=0 and R105F='"+oldFromDate.FormatString("dd.mm.yyyy")+"' and R1065='"+oldToDate.FormatString("dd.mm.yyyy")+"'",false);
            while (!DM->quFree->Eof)
             {
               if (FDeleteCodes->IndexOf((void*)DM->quFree->FN("CODE")->AsInteger) == -1)
                FDeleteCodes->Add((void*)DM->quFree->FN("CODE")->AsInteger);
               DM->quFree->Next();
             }
            DM->qtExec("Delete From CARD_1030 Where UCODE="+IntToStr(UCODE)+" and R1043=0 and R105F='"+oldFromDate.FormatString("dd.mm.yyyy")+"' and R1065='"+oldToDate.FormatString("dd.mm.yyyy")+"'",true);
          }
         else
          { // ����������
            oldToDate = TDateTime(0);
            DM->qtExec("Select CODE From CARD_1030 Where UCODE="+IntToStr(UCODE)+" and R1043=1 and R105F='"+oldFromDate.FormatString("dd.mm.yyyy")+"'",false);
            while (!DM->quFree->Eof)
             {
               if (FDeleteCodes->IndexOf((void*)DM->quFree->FN("CODE")->AsInteger) == -1)
                FDeleteCodes->Add((void*)DM->quFree->FN("CODE")->AsInteger);
               DM->quFree->Next();
             }
            DM->qtExec("Delete From CARD_1030 Where UCODE="+IntToStr(UCODE)+" and R1043=1 and R105F='"+oldFromDate.FormatString("dd.mm.yyyy")+"'",true);
          }
       }
      UnicodeString FSQL,tmpSQL;
      FSQL =  "Insert into CARD_1030 (UCODE,CODE,R1043, R1083, R105F, R1065, R1066, R10A0, R1082, R10A1)";
      FSQL += " values ("+IntToStr(UCODE);
      FSQL += ", _MCODE_, "+tmp->Strings[0];
      FSQL += ", _INFCODE_, '"+tmp->Strings[1];
      if (tmp->Strings[2].UpperCase() == "NULL")
       {
         FSQL += "', "+tmp->Strings[2];
       }
      else
       {
         FSQL += "', '"+tmp->Strings[2]+"'";
       }
      FSQL += ", "+tmp->Strings[3];
      FSQL += ", "+tmp->Strings[4];
      FSQL += ", "+tmp->Strings[5];
      if (tmp->Strings[6].UpperCase() == "NULL")
       {
         FSQL += ", "+tmp->Strings[6];
       }
      else
       {
         FSQL += ", '"+tmp->Strings[6]+"'";
       }
      FSQL += ")";
      TReplaceFlags repFlg;
      repFlg << rfReplaceAll;
//      Variant DDD[1];
//      DDD[0] = Variant((long)UCODE);

      for (int i = 0; i < InfChLB->Items->Count; i++)
       {
         if (InfChLB->Checked[i])
          {
            DM->quFree->Close();
            if (!DM->trFree->Active)   DM->trFree->StartTransaction();
   //         DM->quFree->ExecProcedure("MCODE_CARD_1030",DDD,0);
            DM->quFree->ExecProcedure("MCODE_CARD_1030");
            SetCODE = quFNInt("MCODE")+1;
            tmpSQL = StringReplace(FSQL,"_MCODE_",IntToStr(quFNInt("MCODE")+1),repFlg);
            tmpSQL = StringReplace(tmpSQL,"_INFCODE_",IntToStr((int)InfChLB->Items->Objects[i]),repFlg);
            DM->trFree->Commit();
            DM->qtExec(tmpSQL,true);
          }
       }
      DM->RegCompSQLAfterInsert("CARD_1030", IntToStr(UCODE), "1030");
    }
   __finally
    {
      delete tmp;
      delete FDeleteCodes;
    }
   ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TOtvodEditForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   DSCancel();
   if (FCtrList) delete FCtrList;
   Action = caFree;
}
//---------------------------------------------------------------------------
void __fastcall TOtvodEditForm::DSCancel()
{
   if (DS->State != dsBrowse) DS->Cancel();
}
//---------------------------------------------------------------------------
bool __fastcall TOtvodEditForm::CheckInput()
{
  TDateTime PBirthDay;
  DM->qtExec("Select R0031 From casebook1 Where CODE="+IntToStr(UCODE));
  if (DM->quFree->RecordCount)
   PBirthDay = quFNDate("R0031");
  else
   {
	 MessageBox(Handle,L"������ ����������� ���� �������� ��������.",L"������",0);
	 return false;
   }
  UnicodeString xValid = "";
  int OtvodType;
  TDateTime begDate, endDate;
  DM->xmlImmCardDef->GetTagByUID("1030")->Iterate(GetInput,xValid);
  if (!xValid.Length())
   {
	 OtvodType = RegComp("1043")->GetValue("").ToIntDef(-1);
	 if (OtvodType == -1)
	  {
		 MessageBox(Handle,L"������ ����������� ���� ������.",L"������",0);
         return false;
      }
     else if (OtvodType == 0)
      { // ���������
        if (!TryStrToDate(RegComp("105F")->GetValue("").c_str(), begDate))
         {
		   MessageBox(Handle,L"������� ���������� ���� ������ �������� ������",L"������",0);
		   return false;
		 }
		if (!TryStrToDate(RegComp("1065")->GetValue("").c_str(), endDate))
		 {
		   MessageBox(Handle,L"������� ���������� ���� ��������� �������� ������",L"������",0);
		   return false;
		 }
		if (begDate < PBirthDay)
		 {
           MessageBox(Handle,L"���� ������ �������� ������ �� ����� ���� ������ ���� �������� ��������.",L"������",0);
           return false;
         }
        if (endDate < PBirthDay)
         {
		   MessageBox(Handle,L"���� ��������� �������� ������ �� ����� ���� ������ ���� �������� ��������.",L"������",0);
		   return false;
		 }
		if (endDate < begDate)
		 {
		   MessageBox(Handle,L"���� ��������� �������� ������ �� ����� ���� ������ ���� ������ �������� ������.",L"������",0);
		   return false;
		 }
	  }
	 else if (OtvodType == 1)
	  { // ����������
		if (!TryStrToDate(RegComp("105F")->GetValue("").c_str(), begDate))
		 {
		   MessageBox(Handle,L"������� ���������� ���� ������ �������� ������",L"������",0);
		   return false;
		 }
		if (begDate < PBirthDay)
		 {
		   MessageBox(Handle,L"���� ������ �������� ������ �� ����� ���� ������ ���� �������� ��������.",L"������",0);
		   return false;
		 }
	  }
	 else
	  {
		 MessageBox(Handle,L"������ ����������� ���� ������.",L"������",0);
		 return false;
	  }
	 return true;
   }
  else
   return false;
}
//---------------------------------------------------------------------------
bool __fastcall TOtvodEditForm::GetInput(TTagNode *itTag, UnicodeString &UID)
{
   if (itTag->CmpAV("uid","1083"))
    {
      bool IsSel = false;
      for (int i = 0; i < InfChLB->Items->Count; i++) IsSel |= InfChLB->Checked[i];
      if (!IsSel)
       {
         ShowMessage("���������� ������� ������� ���� ��������");
         UID = "no";
         return true;
       }
    }
   else
    {
      if (FCtrList->GetEDControl(itTag->AV["uid"]))
       if (!RegComp(itTag->AV["uid"])->CheckReqValue())
        {
           UID = "no";
           return true;
        }
    }
   return false;
}
//---------------------------------------------------------------------------
int  __fastcall TOtvodEditForm::SetAlign(TRegEDItem *AItem, bool ALeft, int ATop, int AWidth, int ATabOrder)
{
  if (ALeft) AItem->Left = 2;
  else       AItem->Left = 232;
  AItem->Width = AWidth;
  AItem->Top = ATop;
  AItem->Anchors.Clear();
  AItem->Anchors << akLeft << akRight << akTop;
  AItem->TabOrder = ATabOrder;
  return AItem->Height+4;
}
//---------------------------------------------------------------------------
void __fastcall TOtvodEditForm::actSelAllExecute(TObject *Sender)
{
   for (int i = 0; i < InfChLB->Items->Count; i++)
    {
      InfChLB->Checked[i] = true;
    }
}
//---------------------------------------------------------------------------
void __fastcall TOtvodEditForm::actResetAllExecute(TObject *Sender)
{
   for (int i = 0; i < InfChLB->Items->Count; i++)
    {
      InfChLB->Checked[i] = false;
    }
}
//---------------------------------------------------------------------------

