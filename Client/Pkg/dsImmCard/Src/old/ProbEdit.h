//---------------------------------------------------------------------------

#ifndef ProbEditH
#define ProbEditH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
#include "DMF.h"
#include "RegEd.h"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
/*
#include <ExtCtrls.hpp>
#include "ICSDrawGrid.h"
#include <ValEdit.hpp>
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
*/
//---------------------------------------------------------------------------
class TProbEditForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *CompPanel;
        TPanel *BtnPanel;
        TBevel *Bevel1;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TcxComboBox *TypeCB;
        TLabel *TypeLab;
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall OkBtnClick(TObject *Sender);
        bool __fastcall UpdatePlan(TDate AProbeDate, long AProbCode, int AProbePlProbeCode, int AResult, int APlan, bool AInsert);
        int  __fastcall CheckProbe(int AProbCode, TDate APrivDate, long *APlVacCode);
        bool __fastcall CheckInput();
        int __fastcall GetPlanMCode();
        long __fastcall GetMCode();
        bool __fastcall CheckInPlan(TDate AVacDate, long *AVacCode, int *AResult, int *APlan, bool *AInsert, int *ACheckPlPriv);
private:	// User declarations
        bool __fastcall InThisLPUChBChange(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource);
        int  __fastcall SetAlign(TRegEDItem *AItem, int ALeft, int ATop, int AWidth, int ATabOrder);
        bool __fastcall CBChange(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource);
        bool __fastcall GetCardFields(TTagNode *ANode, UnicodeString &AFields);
        bool isEdit;
        TDate PlanBeg;
        TDate PlanEnd;
        TDate PlanMonth;
        int FUCode, FProbCode, FPlProbCode;
        TpFIBDataSet *DS;
        UnicodeString sProbDate, sProbCode, FCurrSch;
//        TStringList *CList;
        TRegEDContainer *FCtrList;
        int FLastTop,FLastHeight;
public:		// User declarations
      long SetCODE;
        __fastcall TProbEditForm(TComponent* Owner, TTagNode *AClsNode,
      TDataSource *ASource, long UCode, bool AIsEdit, long AProbCode = 0, long APlProbCode = -1);
};
//---------------------------------------------------------------------------
extern PACKAGE TProbEditForm *ProbEditForm;
//---------------------------------------------------------------------------
#endif                          
