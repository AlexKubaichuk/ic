object OtvodEditForm: TOtvodEditForm
  Left = 748
  Top = 211
  BorderStyle = bsToolWindow
  Caption = 'OtvodEditForm'
  ClientHeight = 417
  ClientWidth = 311
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object CompPanel: TPanel
    Left = 0
    Top = 0
    Width = 311
    Height = 380
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      311
      380)
    object InfChLB: TCheckListBox
      Left = 3
      Top = 82
      Width = 303
      Height = 120
      BevelInner = bvLowered
      BevelOuter = bvNone
      BevelKind = bkTile
      BorderStyle = bsNone
      Color = clInfoBk
      ItemHeight = 13
      PopupMenu = SelPM
      TabOrder = 0
    end
    object SelAllBtn: TcxButton
      Left = 108
      Top = 208
      Width = 98
      Height = 25
      Action = actSelAll
      Anchors = [akRight]
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DeSelAllBtn: TcxButton
      Left = 209
      Top = 208
      Width = 98
      Height = 25
      Action = actResetAll
      Anchors = [akRight]
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      TabOrder = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object BtnPanel: TPanel
    Left = 0
    Top = 380
    Width = 311
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      311
      37)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 311
      Height = 5
      Align = alTop
      Shape = bsTopLine
      ExplicitWidth = 312
    end
    object OkBtn: TcxButton
      Left = 137
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = OkBtnClick
    end
    object CancelBtn: TcxButton
      Left = 225
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      ModalResult = 2
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object ActionList1: TActionList
    Left = 30
    Top = 14
    object actSelAll: TAction
      Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1105
      Hint = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1105
      ShortCut = 16449
      OnExecute = actSelAllExecute
    end
    object actResetAll: TAction
      Caption = #1057#1085#1103#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1080#1077
      Hint = #1057#1085#1103#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1080#1077
      ShortCut = 16474
      OnExecute = actResetAllExecute
    end
    object actApply: TAction
      Caption = 'actApply'
    end
  end
  object SelPM: TPopupMenu
    Left = 106
    Top = 40
    object N1: TMenuItem
      Action = actSelAll
    end
    object N2: TMenuItem
      Action = actResetAll
    end
  end
end
