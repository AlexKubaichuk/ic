//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//#define lastComp ((TWinControl*)CList->Objects[CList->Count-1])
#define RegComp(a) FCtrList->EditItems[a]
//#define SetLTop   {lastComp->Left = 2;   lastComp->Top = xTop+3; lastComp->Anchors.Clear(); lastComp->Anchors << akLeft << akRight << akTop;}
#define AddRegED(a) FCtrList->AddEditItem(AClsNode->GetTagByUID(a), !AIsEdit)
#define DSInt(a)    DS->FN(a)->AsInteger
#define DSStr(a)    DS->FN(a)->AsString
#define DSDate(a)   DS->FN(a)->AsDateTime
#define DSDateF(a)  DS->FN(a)->AsDateTime.FormatString("dd.mm.yyyy")
#define _EDLEFT  2
#define _EDLEFT2 232

#include "dsRegEDFunc.h"

#include "PrivEdit.h"
#include "Plan.h"     // +++
#include "DKClasses.h"
#include "msgdef.h"
#include "riChoiceBD.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxCheckBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxMaskEdit"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxClasses"
#pragma link "cxLookAndFeels"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma resource "*.dfm"
TPrivEditForm *PrivEditForm;
/*
//      <binary   name='��������� � ������ ���' uid='101F' required='yes' inlist='list_ext' default='check'/>
//      <date     name='���� ����������'        uid='1024' required='yes' inlist='list' default='CurrentDate'/>
//      <binary   name='������� ��������'       uid='10AE' required='yes' inlist='list_ext' default='check'/>
//      <choiceBD name='�������'                uid='1020' required='yes' ref='0035' inlist='list' depend='1075.002D'/>
//      <text     name='���'                    uid='1021' required='yes' inlist='list' cast='up' fixedvar='yes' length='10'/>
//      <choiceBD name='��������'               uid='1075' required='yes' ref='003A' inlist='list' depend='1020.002D'/>
//      <digit    name='����'                   uid='1022' inlist='list_ext' digits='4' decimal='2' min='0' max='9' default='006A'/>
//      <text     name='�����'                  uid='1023' inlist='list_ext' length='10'/>
//      <binary   name='������� ���������'      uid='1025' required='yes'/>
//      <binary   name='���� ����� �������'     uid='1026' required='yes'>
//      <binary   name='���� ������� �������'   uid='1028' required='yes'>
//      <choiceBD name='������ �������'         uid='10A8' ref='0038' inlist='list_ext' depend='1020.0039'>
//      <digit    name='�������� �������'       uid='1027' ref='006B' inlist='list_ext' depend='1020.0035' digits='5' min='0' max='9999'>
//      <extedit  name='�����'                  uid='1092' required='yes' inlist='list_ext' isedit='no' is3D='no'/>
*/
#define _InThisLPU_ "101F"
#define _INFCODE_   "1075"
#define _IsTur_     "10AE"
#define _VacDATE_   "1024"
#define _VacCODE_   "1020"
#define _VacTYPE_   "1021"
#define _Doza_      "1022"
#define _SER_       "1023"
#define _RCheck_    "1025"
#define _RComm_     "1026"
#define _RLoc_      "1028"
#define _RType_     "10A8"
#define _RVal_      "1027"

//---------------------------------------------------------------------------
__fastcall TPrivEditForm::TPrivEditForm(TComponent* Owner, TTagNode *AClsNode,
      TDataSource *ASource, __int64 UCode, bool AIsEdit, long AVacCode, long APrivCode, bool AIsTur)
        : TForm(Owner)
{
   FTabDown = false;
   TRegEDItem *tmpItem;
   FVacCode = AVacCode;
   FPrivCode = APrivCode;
   FUCode = UCode;
   SetCODE = 0;
   isEdit = AIsEdit;
//!!!   OldInfList = new TStringList;
   DS = (TpFIBDataSet*)ASource->DataSet;

   UnicodeString rfVal  = DS->FieldByName("R10A8")->AsString;
   if (AIsEdit) DS->Edit();
   else         DS->Insert();
   int xTop = 35;
   int FOrder = 1;

   FCtrList = new TRegEDContainer(this,CompPanel);
   FCtrList->DefXML = DM->xmlImmCardDef;
   FCtrList->ReqColor   = DM->ReqColor;
   FCtrList->TablePref  = "";
   FCtrList->DataSource = ASource;
   FCtrList->FIBTemplateQuery = DM->quFree;
   DM->qtExec("Select r0031 From "+DM->RegComp->GetUnitTabName()+" Where code="+IntToStr(FUCode), false);
   FPatBirthday = DM->quFree->FN("r0031")->AsDateTime;
   if (DM->quFree->Transaction->Active) DM->quFree->Transaction->Commit();
   //   ������ �� ��������
   FInvadeClassCB(fltInfCB->Properties->Items, 0, true ,DM->xmlRegDef->GetTagByUID("003a"), false, (TDataSet*)DM->quFree, "","", DM->xmlRegDef->GetTagByUID("003a"));

   tmpItem = AddRegED(_VacDATE_); xTop += SetAlign(tmpItem, _EDLEFT, xTop, 200, FOrder++);
   // '���� ����������'        uid='1024' required='yes' inlist='list' default='CurrentDate'/>
   tmpItem->OnDataChange = PriDateChange;
   // '������� ��������'       uid='10AE' required='yes' inlist='list_ext' default='check'/>
   AddRegED(_IsTur_);
   // '�������'                uid='1020' required='yes' ref='0035' inlist='list' depend='1075.002D'/>
   tmpItem = AddRegED(_VacCODE_); xTop += SetAlign(tmpItem, _EDLEFT, xTop, 460, FOrder++);
   ((TWinControl*)tmpItem->GetControl())->Width = 410;
   tmpItem->SetEDLeft(42);
   tmpItem->OnDataChange = CBChange;
   // '���'                    uid='1021' required='yes' inlist='list' cast='up' fixedvar='yes' length='10'/>
   InfPanel->Top = xTop+3;  NewVariantGrid->TabOrder = FOrder++; xTop += InfPanel->Height+5;
   // '��������'               uid='1075' required='yes' ref='003A' inlist='list' depend='1020.002D'/>
   tmpItem = AddRegED(_INFCODE_); tmpItem->Visible = false;
   // '����'                   uid='1022' inlist='list_ext' digits='4' decimal='2' min='0' max='9' default='006A'/>
   tmpItem = AddRegED(_Doza_); SetAlign(tmpItem, _EDLEFT, xTop, 228, FOrder++);
   // '�����'                  uid='1023' inlist='list_ext' length='10'/>
   tmpItem = AddRegED(_SER_); xTop += SetAlign(tmpItem, _EDLEFT2, xTop, 225, FOrder++);
   // '������� ���������'      uid='1025' required='yes'/>
   tmpItem = AddRegED(_RCheck_); xTop += SetAlign(tmpItem, _EDLEFT, xTop, 228, FOrder++);
   // '���� ����� �������'     uid='1026' required='yes'>
   tmpItem = AddRegED(_RComm_); SetAlign(tmpItem, _EDLEFT, xTop, 228, FOrder++);
   // '���� ������� �������'   uid='1028' required='yes'>
   tmpItem = AddRegED(_RLoc_); xTop += SetAlign(tmpItem, _EDLEFT2, xTop, 225, FOrder++);
   // '������ �������'         uid='10A8' ref='0038' inlist='list_ext' depend='1020.0039'>
   tmpItem = AddRegED(_RType_); SetAlign(tmpItem, _EDLEFT, xTop, 250, FOrder++);
   // '�������� �������'       uid='1027' ref='006B' inlist='list_ext' depend='1020.0035' digits='5' min='0' max='9999'>
   tmpItem = AddRegED(_RVal_); xTop += SetAlign(tmpItem, _EDLEFT2, xTop, 205, FOrder++);
   tmpItem->Left = 252;
   AdmBl->Top = xTop;
   xTop+=7;
   // '���' uid='101F' required='yes' inlist='list_ext' default='check'/>
   tmpItem = AddRegED("2010"); SetAlign(tmpItem, _EDLEFT, xTop, 250, FOrder++);
   tmpItem->SetEDLeft(72);
   // '������������� ���' uid='101F' required='yes' inlist='list_ext' default='check'/>
   tmpItem = AddRegED("200E"); xTop += SetAlign(tmpItem, 200, xTop, 250, FOrder++);
   // '��������� � ������ ���' uid='101F' required='yes' inlist='list_ext' default='check'/>
   tmpItem = AddRegED(_InThisLPU_); SetAlign(tmpItem, _EDLEFT, xTop, 200, FOrder++); //xTop +=
   RegComp("2010")->OnDataChange = InThisLPUChBChange;
   RegComp("200E")->OnDataChange = InThisLPUChBChange;
   // ������� ��������'       uid='10AE' required='yes' inlist='list_ext' default='check'/>
   tmpItem = RegComp(_IsTur_); xTop += SetAlign(tmpItem, 205, xTop, 245, FOrder++);//_EDLEFT, xTop, 228,
   // ����
   tmpItem = AddRegED("10B0"); SetAlign(tmpItem, 7, xTop, 250, FOrder++);
   tmpItem->SetLabel(tmpItem->DefNode->AV["name"]);
   ((TWinControl*)tmpItem->GetControl())->Width = 160;
   tmpItem->SetEDLeft(30);
   // ���. ������
   tmpItem = AddRegED("2012"); xTop += SetAlign(tmpItem, 205, xTop, 245, FOrder++);
   tmpItem->SetLabel(tmpItem->DefNode->AV["name"]);
   ((TWinControl*)tmpItem->GetControl())->Width = 175;
   tmpItem->SetEDLeft(68);

   // �������� ��������������
   tmpItem = AddRegED("10B1"); xTop += SetAlign(tmpItem, _EDLEFT, xTop, 400, FOrder++);
   ((TWinControl*)tmpItem->GetControl())->Width = 287;
   tmpItem->SetEDLeft(160);
   // ��� ����������
   tmpItem = AddRegED("2002"); xTop += SetAlign(tmpItem, _EDLEFT, xTop, 400, FOrder++);
   ((TWinControl*)tmpItem->GetControl())->Width = 287;
   tmpItem->SetEDLeft(160);

   ClientHeight = xTop+BtnPanel->Height+10;
   SchLab->Color = LtColor(clBtnFace,32);
   SchValLab->Color = LtColor(clBtnFace,32);
   TControl *tmpCtrl;
   if (AIsEdit)
    {
      DM->qtExec("Select PCD.R1075, PCD.R1092, PCD.R1021 From CARD_1003 PCD Where PCD.UCODE="+DSStr("UCODE")+" and PCD.R1020="+DSStr("R1020")+" and PCD.R1024='"+DSDateF("R1024")+"'");
      TStringList *tmpPrivList = NULL;
      try
       {
         tmpPrivList = new TStringList;
         UnicodeString tmpSch = "";
         // inf_code:vactype^scheme
         if (DM->quFree->RecordCount)
          tmpSch = _GUI(quFNStr("R1092"));
         while (!DM->quFree->Eof)
          {
            if (tmpSch != _GUI(quFNStr("R1092"))) tmpSch = "";
            tmpPrivList->Add((quFNStr("R1075")+":"+quFNStr("R1021")+"^"+quFNStr("R1092")).c_str());
            DM->quFree->Next();
          }
         if (!tmpSch.Length())
          { // ����� ������ ��� ������� ��� ����� ��������
            ExtSchChB->Checked = true;
          }
         else
          { // ����� ����������
            ExtSchChB->Checked = false;
          }
         SetVacTypes(tmpPrivList);
       }
      __finally
       {
         if (tmpPrivList) delete tmpPrivList;
       }
      if (DM->trFree->Active) DM->trFree->Commit();
      spriDate = RegComp(_VacDATE_)->GetValue("");
      spriVac  = RegComp(_VacCODE_)->GetValue("0035.");
      spriTur  = RegComp(_IsTur_)->GetValue("");
      RegComp(_RType_)->SetValue(rfVal);
      Caption = DM->xmlImmCardDef->GetTagByUID("1003")->GetChildByName("description")->AV["editlabel"];
      TStringList *tmpL = RegComp(_INFCODE_)->GetStrings("003A");
      for (int i = 0; i < tmpL->Count; i++)
       OldInfList->AddObject(tmpL->Strings[i],((TStringList*)tmpL->Objects[i])->Objects[0]);
    }
   else
    {
//      RegComp(_VacDATE_)->SetValue(TDateTime::CurrentDate().FormatString("dd.mm.yyyy"));  // '���� ����������' default='CurrentDate'/>
      Caption = DM->xmlImmCardDef->GetTagByUID("1003")->GetChildByName("description")->AV["insertlabel"];
    }
   if (!AIsEdit)
    {
      if (DM->LPUParam.LPUCode != -1)
       FCtrList->EditItems["2010"]->SetValue(IntToStr(DM->LPUParam.LPUCode),"");
      if (DM->LPUParam.LPUPartCode != -1)
       FCtrList->EditItems["200E"]->SetValue(IntToStr(DM->LPUParam.LPUPartCode),"");
    }
   if (FVacCode > 0)
    { // ������� ���������� �� �����
//      RegComp(_IsTur_)->SetEnable(false); /*Del+++*/                      // '������� ��������'       uid='10AE' required='yes' inlist='list_ext' default='check'/>
      RegComp(_IsTur_)->SetValue(IntToStr((int)AIsTur),false);  // '������� ��������'       uid='10AE' required='yes' inlist='list_ext' default='check'/>
      RegComp(_VacCODE_)->SetEnable(false); /*Del+++*/                      // '�������'                uid='1020' required='yes' ref='0035' inlist='list' depend='1075.002D'/>
      try
       {
         ((TriChoiceBD*)RegComp(_VacCODE_))->SetIsTmpl(true);
         RegComp(_VacCODE_)->SetValue(IntToStr(FVacCode),false);     // '�������'                uid='1020' required='yes' ref='0035' inlist='list' depend='1075.002D'/>
       }
      __finally
       {
         ((TriChoiceBD*)RegComp(_VacCODE_))->SetIsTmpl(false);
       }

      TStringList *tmpPrivList = NULL;
      TStringList *tmpVType = NULL;
      TStringList *tmpVSch = NULL;
      try
       {
         UnicodeString FSQL =  "Select VACTYPE, R108B From CARD_1084 Where UCODE="+IntToStr(FUCode) + " and CODE="+IntToStr(FPrivCode);
         tmpPrivList = new TStringList;
         tmpVType = new TStringList;
         tmpVSch  = new TStringList;
         UnicodeString tmpSch = "";

         tmpVType->Delimiter = '#';
         tmpVSch->Delimiter = ';';
         if (DM->qtExec(FSQL,false) && DM->quFree->RecordCount)
          {
            tmpVSch->DelimitedText = quFNStr("R108B");
            tmpVType->DelimitedText = quFNStr("VACTYPE");
            for (int i = 0; i < tmpVType->Count; i++)
             {
               tmpSch = tmpVType->Strings[i].Trim();
               if (tmpSch.Length())
                {
                  tmpVType->Strings[i] = _UID(tmpSch).UpperCase();
                  tmpVType->Objects[i] = (TObject*)(_GUI(tmpSch).ToIntDef(-1));
                }
               else
                {
                  tmpVType->Delete(i); i--;
                }
             }
            for (int i = 0; i < tmpVSch->Count; i++)
             {
               tmpSch = tmpVSch->Strings[i].Trim();
               if (tmpSch.Length())
                {
                  tmpVSch->Strings[i] = _UID(tmpSch).UpperCase();
                  tmpVSch->Objects[i] = (TObject*)(_GUI(tmpSch).ToIntDef(-1));
                }
               else
                {
                  tmpVSch->Delete(i); i--;
                }
             }

            tmpSch = "";
            if (tmpVSch->Count)
             tmpSch = tmpVSch->Strings[0];
            // inf_code:vactype^scheme
            int idx;
            for (int i = 0; i < tmpVSch->Count; i++)
             {
               if (tmpSch != tmpVSch->Strings[i]) tmpSch = "";
               idx = tmpVType->IndexOfObject(tmpVSch->Objects[i]);
               if (idx != -1)
                {
                  tmpPrivList->Add((IntToStr((int)tmpVSch->Objects[i])+":"+tmpVType->Strings[idx]+"^"+tmpVSch->Strings[i]).c_str());
                }
               else
                _MSG_ERR("������ ����������� ���� ���������� � �����.","������");
             }
            if (!tmpSch.Length())
             { // ����� ������ ��� ������� ��� ����� ��������
               ExtSchChB->Checked = true;
             }
            else
             { // ����� ����������
               TTagNode *fLine  = DM->xmlVacDef->GetTagByUID(tmpSch);
               if (fLine)
                {
                  fLine = fLine->GetParent("schema");
                  if (fLine)
                   tmpSch = fLine->AV["uid"];
                }
               UnicodeString cSch = "";
               bool FTur = (RegComp(_IsTur_)->GetValue("") == "1");
               if (FTur)
                DM->qtExec("Select CAA.R10AC as VSCH From CARD_10AA CAA join CLASS_002D CL2D on (CL2D.R003E=CAA.R10AB and CL2D.R003B="+IntToStr(FVacCode)+" and UCODE="+IntToStr(FUCode)+")",false);
               else
                DM->qtExec("Select C76.R1078 as VSCH  From CARD_1076 C76 join CLASS_002D CL2D on (CL2D.R003E=C76.R1077 and CL2D.R003B="+IntToStr(FVacCode)+" and UCODE="+IntToStr(FUCode)+")",false);
               if (DM->quFree->RecordCount)
                cSch = _GUI(quFNStr("VSCH")).UpperCase();
               while (!DM->quFree->Eof)
                {
                  if (cSch != _GUI(quFNStr("VSCH")).UpperCase()) cSch = "";
                  DM->quFree->Next();
                }
               if (tmpSch.UpperCase() != cSch.UpperCase())
                ExtSchChB->Checked = true;
               else
                ExtSchChB->Checked = false;
             }
            SetVacTypes(tmpPrivList);
          }
       }
      __finally
       {
         if (tmpPrivList) delete tmpPrivList;
         if (tmpVType)    delete tmpVType;
         if (tmpVSch)     delete tmpVSch;
       }
    }
   if (FCtrList->GetEDControl(_VacDATE_))
    {
      tmpItem = FCtrList->EditItems[_VacDATE_];
//      tmpItem->OnDataChange = CBChange;
      tmpCtrl = tmpItem->GetFirstEnabledControl();
      if (tmpCtrl)
       ActiveControl = (TWinControl*)tmpCtrl;
    }
   fltInfCB->Enabled = RegComp(_VacCODE_)->GetFirstEnabledControl();
}
//---------------------------------------------------------------------------
int  __fastcall TPrivEditForm::SetAlign(TRegEDItem *AItem, int ALeft, int ATop, int AWidth, int ATabOrder)
{
  AItem->Left = ALeft;
  AItem->Width = AWidth;
  AItem->Top = ATop;
  AItem->Anchors.Clear();
  AItem->Anchors << akLeft << akRight << akTop;
  AItem->TabOrder = ATabOrder;
  return AItem->Height+4;
}
//---------------------------------------------------------------------------
void __fastcall TPrivEditForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   if (DS->State != dsBrowse)
    {
      RegComp(_IsTur_)->OnDataChange = NULL;
      RegComp(_VacDATE_)->OnDataChange = NULL;
      RegComp(_VacCODE_)->OnDataChange = NULL;
      DS->Cancel();
    }
   delete OldInfList;
   delete FCtrList;
//   delete privVariant;
   Action = caFree;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TPrivEditForm::PriDozeVal(TDateTime APriDate, TRegEDItem *AVacItem)
{
  UnicodeString RC = "";
  UnicodeString tmp;
  TStringList *dList = new TStringList;
  try
   {
     tmp = RegComp(_VacCODE_)->GetValue("0035.006A").Trim();
     if (tmp.Length())
      dList->Add(tmp+":"+RegComp(_VacCODE_)->GetValue("0035.0125")+";"+RegComp(_VacCODE_)->GetValue("0035.0126"));
     tmp = RegComp(_VacCODE_)->GetValue("0035.0127").Trim();
     if (tmp.Length())
      dList->Add(tmp+":"+RegComp(_VacCODE_)->GetValue("0035.0128")+";"+RegComp(_VacCODE_)->GetValue("0035.0129"));
     tmp = RegComp(_VacCODE_)->GetValue("0035.012A").Trim();
     if (tmp.Length())
      dList->Add(tmp+":"+RegComp(_VacCODE_)->GetValue("0035.012B")+";"+RegComp(_VacCODE_)->GetValue("0035.012C"));
     tmp = RegComp(_VacCODE_)->GetValue("0035.012D").Trim();
     if (tmp.Length())
      dList->Add(tmp+":"+RegComp(_VacCODE_)->GetValue("0035.012E")+";"+RegComp(_VacCODE_)->GetValue("0035.012F"));

     Word FPriAgeYear, FPriAgeMonth, FPriAgeDay;
     Word FAgeYearFrom, FPriAgeMonthFrom, FPriAgeDayFrom;
     Word FPriAgeYearTo, FPriAgeMonthTo, FPriAgeDayTo;
     DateDiff(APriDate, FPatBirthday, FPriAgeDay, FPriAgeMonth, FPriAgeYear);
     bool InDiapazon;
     for (int i = 0; i < dList->Count; i++)
      {
        tmp = GetPart2(dList->Strings[i],':');
        InDiapazon = true;
        if (DecodeAgeStr(GetPart1(tmp,';'), FAgeYearFrom, FPriAgeMonthFrom, FPriAgeDayFrom))
         {
           InDiapazon &= (AgeCmp(FPriAgeYear, FPriAgeMonth, FPriAgeDay, FAgeYearFrom, FPriAgeMonthFrom, FPriAgeDayFrom) != -1);
         }
        if (DecodeAgeStr(GetPart2(tmp,';'), FPriAgeYearTo, FPriAgeMonthTo, FPriAgeDayTo))
         {
           InDiapazon &= (AgeCmp(FPriAgeYear, FPriAgeMonth, FPriAgeDay, FPriAgeYearTo, FPriAgeMonthTo, FPriAgeDayTo) == -1);
         }
        if (InDiapazon)
         {
           RC = GetPart1(dList->Strings[i],':').Trim();
           break;
         }
      }
   }
  __finally
   {
     delete dList;
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TPrivEditForm::PriDateChange(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource)
{
  bool RC = false;
  try
   {
     UnicodeString priDate = RegComp(_VacDATE_)->GetValue("");
     TDate FVacDate;
     if (TryStrToDate(priDate.c_str(), FVacDate))
      {
        RegComp(_Doza_)->SetValue(PriDozeVal(FVacDate, RegComp(_VacCODE_)));
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TPrivEditForm::CBChange(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource)
{
  bool RC = false;
  try
   {
     TcxTreeListNode *tmpNode;
     SchValLab->Caption = "";
	 NewVariantGrid->Clear();
     InfNodes.clear();
     InfList = RegComp(_INFCODE_)->GetStrings("003A");
     bool FTur = (RegComp(_IsTur_)->GetValue("") == "1");
     UnicodeString vacCode = RegComp(_VacCODE_)->GetValue("0035.");
     if (vacCode.Length())
      {
        UnicodeString vacSch = "";
        UnicodeString vacSchTur = "";
        DM->qtExec("Select C76.R1077, C76.R1078 From CARD_1076 C76 join CLASS_002D CL2D on (CL2D.R003E=C76.R1077 and CL2D.R003B="+vacCode+" and UCODE="+IntToStr(FUCode)+")",false);
        vacSch = _GUI(quFNStr("R1078"));

        DM->qtExec("Select CAA.R10AB, CAA.R10AC From CARD_10AA CAA join CLASS_002D CL2D on (CL2D.R003E=CAA.R10AB and CL2D.R003B="+vacCode+" and UCODE="+IntToStr(FUCode)+")",false);
        vacSchTur = _GUI(quFNStr("R10AC"));

        if (isEdit)
         {
           if (DSStr("R1092").UpperCase()!= "_NULL_")
            vacSch = _GUI(DSStr("R1092"));
         }
        else
         {
           // �������� ��� ���� �� ���������
//           RegComp(_Doza_)->SetValue(RegComp(_VacCODE_)->GetValue("0035.006A"));
         }
        // �������� ��� ���� �� ���������
		UnicodeString emptyString;
		PriDateChange(NULL, emptyString, NULL);
        int tmpInfCode;
		if (InfList)
         {
           for (int i = 0; i < InfList->Count; i ++)
            {
              tmpInfCode = ((int)((TStringList*)InfList->Objects[i])->Objects[0]);
			  InfNodes[tmpInfCode] = NewVariantGrid->Root->AddChild();
              tmpNode = InfNodes[tmpInfCode];
              tmpNode->Texts[0] = InfList->Strings[i];
              tmpNode->Data = (void*)tmpInfCode;
              tmpNode->Texts[1] = "";
              tmpNode->Values[1] = "";
              tmpNode->Texts[2] = "";
            }
         }
        privVariant.clear();
        privVariantUID.clear();
        ((TcxCustomComboBoxProperties*)VacVarCol->Properties)->Items->Clear();
        FShema = NULL;
        if (FTur)
         { // �������
           if (vacSchTur.Length())
            FShema = DM->xmlVacDef->GetTagByUID(vacSchTur);
         }
        else
         {
           if (vacSch.Length())
            FShema = DM->xmlVacDef->GetTagByUID(vacSch);
         }
        if (FShema)
         {
           // ��������� �����
           if (!FShema->AV["ref"].Length())
            SchValLab->Caption = "������";
           else
            SchValLab->Caption = DM->VacList[FShema->AV["ref"]]+" (������� '"+FShema->AV["name"]+"')";
           if (!FShema->CmpAV("ref",vacCode))
            FShema = DM->xmlVacDef->GetChildByAV("schema","ref",vacCode,true);
         }
        else
          FShema = DM->xmlVacDef->GetChildByAV("schema","ref",vacCode,true);
        TTagNode *tmp = FShema;
        if (tmp)
         {
           // ������������ ��������� ����������
           TTagNodeMap SchList;
           if (ExtSchChB->Checked)
			DM->GetSchemeList((int)(NewVariantGrid->Items[0]->Data), SchList, true);
           else
			SchList[tmp->AV["uid"]] = tmp;
           UnicodeString FSchLine;
           for (TTagNodeMap::iterator i = SchList.begin(); i != SchList.end(); i++)
            {
              if (i->second->CmpAV("ref",vacCode))
               {
                 tmp = i->second->GetFirstChild();
                 while (tmp)
                  {
                    if (tmp->CmpName("line"))
                     {
                       FSchLine = tmp->AV["name"];
                       if (ExtSchChB->Checked)
                       FSchLine += "  ����� \""+i->second->AV["name"]+"\"";
                       ((TcxCustomComboBoxProperties*)VacVarCol->Properties)->Items->Add(FSchLine);
                       privVariant[FSchLine] = tmp->AV["uid"];
                       privVariantUID[tmp->AV["uid"]] = tmp->AV["name"];
                       privVariantOld[tmp->AV["uid"]] = FSchLine;
                     }
                    tmp = tmp->GetNext();
                  }
               }
            }
           if (!FTur)
            {
              ((TcxCustomComboBoxProperties*)VacVarCol->Properties)->Items->Add("���.");
              privVariant["���."] = "_NULL_";
              privVariantUID["_NULL_"] = "���.";
              privVariantOld["_NULL_"] = "���.";
            }
         }
        else
         {
           if (!FTur)
            {
              ((TcxCustomComboBoxProperties*)VacVarCol->Properties)->Items->Add("���.");
              privVariant["���."] = "_NULL_";
              privVariantUID["_NULL_"] = "���.";
              privVariantOld["_NULL_"] = "���.";
            }
         }
        // ����������� ������� � ����� �������� �������� � ���������� �����������
        UnicodeString FSQL;
        UnicodeString tmpVacVariant = "";
        UnicodeString tmpVacSch = "";
        if (FPrivCode >= 0)
         {// ������� �� �����
           FSQL =  "Select CODE, VACTYPE, R108B From CARD_1084 Where UCODE="+IntToStr(FUCode);
           FSQL += " and CODE="+IntToStr(FPrivCode);
           if (DM->qtExec(FSQL,false) && DM->quFree->RecordCount)
            {
              tmpVacSch = quFNStr("R108B");
              tmpVacVariant = quFNStr("VACTYPE");
            }
         }
        else
         {// ������� �� ����������� �����
           TDate tmpVacDate;
           if (TryStrToDate(RegComp(_VacDATE_)->GetValue("").c_str(), tmpVacDate))
            {
              long tmpPlVacCode = 0;
              if (CheckPriviv(vacCode.ToInt(), tmpVacDate, &tmpPlVacCode, tmpVacVariant, tmpVacSch) != cpaCheckPlan)
               tmpVacVariant = "";
            }
           else
             tmpVacVariant = "";
         }
        if (tmpVacVariant.Length() && ItTag)
         {
           TStringList *tmpVacTypeList = NULL;
           TStringList *tmpPrivList = NULL;
           try
            {
              TReplaceFlags repF;
              repF << rfReplaceAll;
              UnicodeString tmpSch;
              tmpVacTypeList = new TStringList;
              tmpPrivList    = new TStringList;
              tmpVacTypeList->Text = StringReplace(tmpVacSch,";","\n",repF);
              TTagNode *SchNode,*LineNode;
              TAnsiStrMap tmpSchCount;
              for (int i = 0; i < tmpVacTypeList->Count; i++)
               {
                 LineNode = DM->xmlVacDef->GetTagByUID(_UID(tmpVacTypeList->Strings[i]));
                 if (LineNode)
                  {
                    if (LineNode->CmpName("line"))
                     {
                       SchNode = LineNode->GetParent();
                       if (SchNode->CmpName("schema"))
                        {
                          tmpPrivList->Add((_GUI(tmpVacTypeList->Strings[i])+":V1^"+SchNode->AV["uid"]+"."+LineNode->AV["uid"]).c_str());
                          tmpSchCount[SchNode->AV["uid"]] = "1";
                        }
                     }
                  }
               }
              if (tmpPrivList->Count)
               {
                 if (tmpSchCount.size() > 1)
                  { // ����� ������ ��� ������� ��� ����� ��������
                    ExtSchChB->Checked = true;
                  }
                 else
                  { // ����� ����������
                    ExtSchChB->Checked = false;
                  }
                 SetVacTypes(tmpPrivList);
               }
            }
           __finally
            {
              if (tmpVacTypeList) delete tmpVacTypeList;
              if (tmpPrivList) delete tmpPrivList;
            }
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TPrivEditForm::OkBtnClick(TObject *Sender)
{
   if (!CheckInput()) return;
   ActiveControl = OkBtn;
   TReplaceFlags repF;
   repF << rfReplaceAll;
   UnicodeString priSer, priDoze, priReakVal,priReakCheck,priReakComm,priReakFormat,priReakLoc, priTur, priDoc, priFin, priDopInf,priLPU,priLPUPart,priMS;

   priLPU        = RegComp("2010")->GetValue("");
   priLPUPart    = RegComp("200E")->GetValue("");
   priDoc        = RegComp("10B0")->GetValue("00AA.");
   priMS         = RegComp("2012")->GetValue("00AA.");

   if (RegComp(_InThisLPU_)->GetValue("").ToIntDef(0))
    {
      if ((DM->LPUParam.LPUCode != -1) && (DM->LPUParam.LPUCode != priLPU.ToIntDef(0)))
       {
        FCtrList->EditItems["2010"]->SetValue(IntToStr(DM->LPUParam.LPUCode),"");
        if ((DM->LPUParam.LPUPartCode != -1) && (DM->LPUParam.LPUPartCode != priLPUPart.ToIntDef(0)))
         FCtrList->EditItems["200E"]->SetValue(IntToStr(DM->LPUParam.LPUPartCode),"");
       }
    }
   priTur        = RegComp(_IsTur_)->GetValue("");
   priDate       = RegComp(_VacDATE_)->GetValue("");
   priVac        = RegComp(_VacCODE_)->GetValue("0035.");
   priDoze       = StringReplace(RegComp(_Doza_)->GetValue(""),",",".",repF);
   priSer        = RegComp(_SER_)->GetValue("");
   priReakCheck  = RegComp(_RCheck_)->GetValue("");
   priReakComm   = RegComp(_RComm_)->GetValue("");
   priReakLoc    = RegComp(_RLoc_)->GetValue("");
   priReakFormat = RegComp(_RType_)->GetValue("0038.");
   priReakVal    = RegComp(_RVal_)->GetValue("");

   priLoc        = RegComp(_InThisLPU_)->GetValue("");
   priFin        = RegComp("10B1")->GetValue("00AB.");
   priDopInf     = RegComp("2002")->GetValue("00BE.");
   UnicodeString FSQL;
   UnicodeString FVacVariant = "";
   TDate FVacDate;
   if (!TryStrToDate(priDate.c_str(), FVacDate))
    {
      MessageBox(Handle,L"������� ���������� ���� ���������� ��������",L"������",0);
      return;
    }
   long FSetVacCode = priVac.ToInt();
   long SaveVacCode = FSetVacCode;
   int FResult = 0;
   int FPlan = 0;
   TCheckPlanAction FCheckPlPriv = cpaNotCheck;
   bool FInsert = false;

   if (!priDoze.Length())       priDoze = "0";
   if (!priReakVal.Length())    priReakVal = "NULL";
   if (!priReakFormat.Length()) priReakFormat = "NULL";
   if (!priLPU.Length())        priLPU = "NULL";
   if (!priLPUPart.Length())    priLPUPart = "NULL";
   if (!priDoc.Length())        priDoc = "NULL";
   if (!priMS.Length())         priMS = "NULL";
   if (!priFin.Length())        priFin = "NULL";
   if (!priDopInf.Length())     priDopInf = "NULL";

//   TStringList  *InfVariantList = NULL;
   bool IsComm = true;
   TList *FDeleteCodes = new TList;
   TList *FInsertCodes = new TList;
   try
    {
//      InfVariantList = new TStringList;
//      InfVariantList->Sorted = true;

      int infIdx;
      TcxTreeListNode *tmpNode;
      if (!CheckInPlan(FVacDate, &FSetVacCode, FVacVariant, &FResult, &FPlan, &FInsert, &FCheckPlPriv)) return;
      FSQL = "Select CODE From CARD_1003 Where UCODE="+IntToStr(FUCode)+" and R1024 = '"+priDate+"' and R1020 = "+priVac;
      if (isEdit)
       FSQL += " and R1024 <> '"+spriDate+"' and R1020 <> "+spriVac;
      if (DM->qtExec(FSQL,false))
       {
         if (DM->quFree->RecordCount)
          {
            if (MessageBox(Handle,("�������� ���������� (���� - \""+priDate+"\", ������� - \""+DM->VacList[priVac]+"\"), ��������?").c_str(),L"���������",MB_ICONQUESTION|MB_YESNO) != ID_YES) return;
          }
       }
      else
       return;
      if (DS->State != dsBrowse)
       {
         RegComp(_IsTur_)->OnDataChange = NULL;
         RegComp(_VacDATE_)->OnDataChange = NULL;
         RegComp(_VacCODE_)->OnDataChange = NULL;
         DS->Cancel();
       }
      if (!DM->trUpdate->Active) DM->trUpdate->StartTransaction();

      if (isEdit)
       {
         DM->qtUpdate("Select CODE From CARD_1003 Where UCODE="+IntToStr(FUCode)+" and R1024='"+spriDate+"' and R1020="+spriVac);
         while (!DM->quUpdate->Eof)
          {
            if (FDeleteCodes->IndexOf((void*)DM->quUpdate->FN("CODE")->AsInteger) == -1)
             FDeleteCodes->Add((void*)DM->quUpdate->FN("CODE")->AsInteger);
            DM->quUpdate->Next();
          }
         IsComm &= DM->qtUpdate("Delete From CARD_1003 Where UCODE="+IntToStr(FUCode)+" and R1024='"+spriDate+"' and R1020="+spriVac);
       }
      DM->qtUpdate("Select CODE From CARD_1003 Where UCODE="+IntToStr(FUCode)+" and R1024='"+priDate+"' and R1020="+priVac);
      while (!DM->quUpdate->Eof)
       {
         if (FDeleteCodes->IndexOf((void*)DM->quUpdate->FN("CODE")->AsInteger) == -1)
          FDeleteCodes->Add((void*)DM->quUpdate->FN("CODE")->AsInteger);
         DM->quUpdate->Next();
       }
      IsComm &= DM->qtUpdate("Delete From CARD_1003 Where UCODE="+IntToStr(FUCode)+" and R1024='"+priDate+"' and R1020="+priVac);

      UnicodeString FSQL = "Insert Into CARD_1003 (UCODE,CODE,R101F,R1024,R10AE,R1020,R1021,R1075,R1022,R1023,R1025,R1026,R1028,R10A8,R1027,R1092,R10AF,R2010,R200E,R10B0,R2012,R10B1,R2002) Values (";
      UnicodeString ChSQL;
      FSQL += IntToStr(FUCode)+",";
      FSQL += "_CODE_,";
      FSQL += priLoc+",'";
      FSQL += priDate+"',";
      FSQL += priTur+",";
      FSQL += priVac+",";
      FSQL += "'_VACVAR_',";
      FSQL += "_INFCODE_,";
      FSQL += priDoze+",'";
      FSQL += priSer+"',";
      FSQL += priReakCheck+",";
      FSQL += priReakComm+",";
      FSQL += priReakLoc+",";
      FSQL += priReakFormat+",";
      FSQL += priReakVal+",";
      FSQL += "'_VACSCH_',";
      FSQL += priTur+",";
      FSQL += priLPU+",";
      FSQL += priLPUPart+",";
      FSQL += priDoc+",";
      FSQL += priMS+",";
      FSQL += priFin+",";
      FSQL += priDopInf+")";

      UnicodeString SSQL, tmpSetSchLine, tmpVacVar, tmpInfCode;
      int StartCode = GetMCode();
      SetCODE = StartCode;
	  for (int i = 0; i < NewVariantGrid->Count; i++)
       {
		 tmpNode = NewVariantGrid->Items[i];
         tmpVacVar  = privVariantUID[privVariant[tmpNode->Texts[1]]];
         tmpInfCode = IntToStr((int)tmpNode->Data);
         FInsertCodes->Add((void*)(StartCode+i));
         SSQL = StringReplace(FSQL,"_CODE_",IntToStr(StartCode+i),repF);
         SSQL = StringReplace(SSQL,"_VACVAR_",tmpVacVar,repF);
         SSQL = StringReplace(SSQL,"_INFCODE_",tmpInfCode,repF);
         if (tmpVacVar == "���.")
          SSQL = StringReplace(SSQL,"_VACSCH_","_NULL_",repF);
         else
          {
            if (FShema)
             {
               TTagNode *tmp = FShema->GetTagByUID(privVariant[tmpNode->Texts[1]]);
               if (tmp)
                {
                  if (tmp->CmpName("line"))
                   {
                     tmpSetSchLine = tmp->GetParent()->AV["uid"]+"."+tmp->AV["uid"];
                     bool CanSchChange = true;
                     SSQL = StringReplace(SSQL,"_VACSCH_",tmpSetSchLine.c_str(),repF);
                     /* �������� �� ���� */
                     ChSQL = "Select Max(R1024) as MDATE From CARD_1003 Where R10AE="+priTur+" and R1092 <> '_NULL_' and UCODE="+IntToStr(FUCode)+" and R1075 = "+tmpInfCode;
                     if (DM->qtExec(ChSQL,false))
                      {
                        if (DM->quFree->RecordCount)
                         {
                           if (DM->quFree->FN("MDATE")->AsDateTime > TDate(priDate))
                            CanSchChange = false;
                         }
                      }
                     if (CanSchChange)
                      {
                        if (priTur == "1")
                         IsComm &= DM->qtUpdate("Update CARD_10AA Set R10C0=0, R10AC='"+tmpSetSchLine+"' Where UCODE="+IntToStr(FUCode)+" and R10AB="+tmpInfCode);
                        else
                         IsComm &= DM->qtUpdate("Update CARD_1076 Set R1078='"+tmpSetSchLine+"' Where UCODE="+IntToStr(FUCode)+" and R1077="+tmpInfCode);
                      }
                   }
                  else
                   SSQL = StringReplace(SSQL,"_VACSCH_","_NULL_",repF);
                }
               else
                SSQL = StringReplace(SSQL,"_VACSCH_","_NULL_",repF);
             }
            else
             SSQL = StringReplace(SSQL,"_VACSCH_","_NULL_",repF);
          }
         IsComm &= DM->qtUpdate(SSQL);
       }
    #ifdef IMM_CARD_UPDATE_GEN
      if (IsComm)
       {
		 IsComm &= DM->qtUpdate("SET GENERATOR GEN_CARD_1003_ID TO "+IntToStr(StartCode+NewVariantGrid->Count));
       }
    #endif
      if (FCheckPlPriv != cpaNotCheck)
       IsComm &= UpdatePlan(FVacDate, SaveVacCode, FSetVacCode, FVacVariant, FResult, FPlan, FInsert);
      else
       {
         if (isEdit)
          IsComm &= DM->DeleteFromPlan(FUCode, TDate(spriDate), spriVac.ToInt(), false);
       }
      if (OldInfList->Count && isEdit)
       {
         int tmpInfCode;
         bool Exists;
         for (int i = 0; i < OldInfList->Count; i ++)
          {
            tmpInfCode = (int)OldInfList->Objects[i];
            Exists = false;
			for (int j = 0; (j < NewVariantGrid->Count) && !Exists; j++)
			 {
			   if ((int)((NewVariantGrid->Items[j])->Data) == tmpInfCode)
                Exists = true;
             }
            if (!Exists)
             {
               FVacVariant = "";
               if (spriTur == "1")
                {
                  FSQL =  "Select PRV.R1021, PRV.R1092 from CARD_1003 PRV Where ";
                  FSQL += "PRV.UCODE="+IntToStr(FUCode)+" and PRV.R1075="+IntToStr(tmpInfCode)+" and PRV.R10AE = 1 and ";
                  FSQL += "PRV.R1024 = (select Max(x.R1024) From CARD_1003 x Where ";
                  FSQL += "x.UCODE="+IntToStr(FUCode)+" and x.R1075="+IntToStr(tmpInfCode)+" and x.R10AE = 1 and x.R1092 <> '_NULL_')";
                  IsComm &= DM->qtUpdate(FSQL);
                  if (DM->quUpdate->RecordCount)
                   { // ���� ���������� ��������� ��������
                     if (!DM->quUpdate->FN("R1092")->IsNull)
                      {
                        FVacVariant = DM->quUpdate->FN("R1092")->AsString;
                        IsComm &= DM->qtUpdate("Update CARD_10AA Set R10AC = '"+FVacVariant+"' Where UCODE = "+IntToStr(FUCode)+" and R10AB="+IntToStr(tmpInfCode));
                      }
                   }
                }
               else
                {
                  FSQL =  "Select PRV.R1021, PRV.R1092 from CARD_1003 PRV Where ";
                  FSQL += "PRV.UCODE="+IntToStr(FUCode)+" and PRV.R1075="+IntToStr(tmpInfCode)+" and PRV.R10AE = 0 and ";
                  FSQL += "PRV.R1024 = (select Max(x.R1024) From CARD_1003 x Where ";
                  FSQL += "x.UCODE="+IntToStr(FUCode)+" and x.R1075="+IntToStr(tmpInfCode)+" and x.R10AE = 0 and x.R1092 <> '_NULL_')";
                  IsComm &= DM->qtUpdate(FSQL);
                  if (DM->quUpdate->RecordCount)
                   {
                     if (!DM->quUpdate->FN("R1092")->IsNull)
                      FVacVariant = DM->quUpdate->FN("R1092")->AsString;
                   }
                  if (!FVacVariant.Trim().Length())
                   { // ����������� ���������� �������� � ��������� �����,
                     // ����� ���� ��������������, ���� ��� �������� ����, ���� ��������� ����
                     // ���� � �������� ����� - ��������� �� �������� ��������
                     IsComm &= DM->qtUpdate("Select R003F From CLASS_003A Where CODE="+IntToStr(tmpInfCode));
                     if (DM->quUpdate->RecordCount)
                      {
                        if (!DM->quUpdate->FN("R003F")->IsNull)
                         FVacVariant = DM->quUpdate->FN("R003F")->AsString;
                      }
                     if (!FVacVariant.Trim().Length()) FVacVariant = "_NULL_";
                   }
                  IsComm &= DM->qtUpdate("Update CARD_1076 Set R1078 = '"+FVacVariant+"' Where UCODE = "+IntToStr(FUCode)+" and R1077="+IntToStr(tmpInfCode));
                }
             }
          }
       }
    }
   __finally
    {
      if (IsComm)
       {
         if (DM->trUpdate->Active) DM->trUpdate->Commit();
         DM->RegCompSQLAfterInsert("CARD_1003", IntToStr(FUCode), "1003");
       }
      else
       {
         if (DM->trUpdate->Active) DM->trUpdate->Rollback();
       }
      delete FDeleteCodes;
      delete FInsertCodes;
       // ����  AInsert - AVacCode - ��� �������
       // �����           AVacCode - ��� �������� � �����
       //      if (InfVariantList) delete InfVariantList;
    }
   ModalResult = mrOk;
}
//---------------------------------------------------------------------------
bool __fastcall TPrivEditForm::CheckInPlan(TDate AVacDate, long *AVacCode, UnicodeString &AVacVariant, int *AResult, int *APlan, bool *AInsert, TCheckPlanAction *ACheckPlPriv)
{
/*
                                                                Result  C��� ?  ���. ������.?   �� ����� ?
���������� �������� � "�����" ��                                2       0       X               X
���������� �������� ����� ��������� �� ������ ����������        1       1       0               X
���������� �������� ��� �����                                   4       1       1               0
���������� ������������ �������� �� �����                       3       1       1               1

Rezult = 1 ���������� �������� ����� ��������� �� ������ ����������;
           (��� ��������������(�����) ��� �� ����������� � �� ���������� ������� ���������� �������� ������������)
Rezult = 2 ���������� �������� � "�����" ��;
           (��������. �������. ������� ���������� �������� ������������ �� �������������)
Rezult = 3 ���������� ������������ �������� �� �����;
Rezult = 4 ���������� �������� ��� �����;

��� �������� �������� (�����) ������ � ����������� �������� ��������� �� ��������� �����,
� ������ � �������� �������� �������������� ����� �������� ���������� � ����������.
*/
   bool VacLoc = false;
   bool OtherOrg = false;
   TcxTreeListNode *tmpNode;
   long SaveVacCode = (*AVacCode);
   // ����������� ����/�����
   OtherOrg = (DM->qtExec("Select cb.CODE From CASEBOOK1 cb join class_0001 cl1 on (cl1.code=cb.r0025 and cb.code="+IntToStr(FUCode)+" and cl1.R00AF=0)", false) && DM->quFree->RecordCount);
   // ����������� ����� ���������� ��������
   VacLoc = (RegComp(_InThisLPU_)->GetValue("") == "1");

   UnicodeString tmpVacSch = "";
   if (FPrivCode >= 0)
    {// ������� �� �����
      (*ACheckPlPriv) = cpaCheckPlan;
      (*AVacCode) = FPrivCode;
      if (DM->qtExec("Select R1087, R108B, VACTYPE From CARD_1084 Where UCODE="+IntToStr(FUCode)+" and CODE="+IntToStr(FPrivCode),false) && DM->quFree->RecordCount)
       {
         AVacVariant = quFNStr("VACTYPE");
         tmpVacSch   = quFNStr("R108B");
         PlanMonth = quFNDate("R1087");
         DM->CalcPlanPeriod(&PlanBeg, &PlanEnd, &PlanMonth);// +++
         if (!DM->CheckInPeriod(PlanBeg, PlanEnd, AVacDate))
          {
            UnicodeString xMsg;
            xMsg =  "���� ���������� �������� ������ ���� � ��������\n��������� �������, � �� ������ ������� ����!\n\n���� ���������� ������ ����\n";
            xMsg += "� "+PlanBeg.FormatString("dd.mm.yyyy");
            xMsg += " �� "+PlanEnd.FormatString("dd.mm.yyyy");
			MessageBox(Handle,xMsg.c_str(),L"���������",MB_ICONINFORMATION);
            return false;
          }
       }
    }
   else
    { // ������� �� ����������� �����
      (*ACheckPlPriv) = CheckPriviv((*AVacCode), AVacDate, AVacCode, AVacVariant, tmpVacSch);
    }
   if ((*ACheckPlPriv) != cpaNotCheck)
    {
      TReplaceFlags repF;
      repF << rfReplaceAll;
      long PlCode, NotPlCode;
      bool OrgType;
      if ((*ACheckPlPriv) == cpaCheckPlan)
       { // �� �����
      // ��������� �������� ����������
       }
      if (!OtherOrg)
       {// ���� �����������
         bool VacTypeEq = true;
         TStringList *tmp = NULL;
         int FIdx;
         UnicodeString FtmpSch;
         try
          {
            tmp = new TStringList;
            tmp->Text = StringReplace(tmpVacSch,";","\n",repF);
			for (int i = 0; i < NewVariantGrid->Count; i++)
             {
			   tmpNode = NewVariantGrid->Items[i];
               FtmpSch = IntToStr((int)tmpNode->Data)+"."+privVariant[tmpNode->Texts[1]];
               FIdx = tmp->IndexOf(FtmpSch);
               if (FIdx == -1)
                {
                  VacTypeEq = false;
                  break;
                }
             }
          }
         __finally
          {
            if (tmp) delete tmp;
          }
         if ((*ACheckPlPriv) == cpaCheckDop)
          {// ���������� ������ �������� � ����� ����������� ������
           // ��������� � ���� ���������� ����������
            (*AInsert)             = true;
            if (VacLoc) (*AResult) = 4; // ���������� �������� ��� �����
            else        (*AResult) = 1; // ���������� �������� ����� ��������� �� ������ ����������
            (*APlan)               = 0;
            (*AVacCode) = SaveVacCode;
          }
         else if (!VacTypeEq)
          {// ���������� ������ �������� � ����� ������������,
           // ��� ����� ������ ������ ��� ����������, ����������, ��� ������
            UnicodeString xMsg,tmpSch;
            TStringList *tmpVacVariant = NULL;
            int RC = mrCancel;
            try
             {
               UnicodeString PlVacVariant;
               UnicodeString VacVariant = "";
               tmpVacVariant = new TStringList;
               tmpVacVariant->Text = StringReplace(tmpVacSch,";","\n",repF);
               TAnsiStrMap tmpPlVacSch;
               for (int i = 0; i < tmpVacVariant->Count; i++)
                {
                  PlVacVariant = tmpVacVariant->Strings[i].Trim();
                  if (PlVacVariant.Length())
                   {
                     tmpPlVacSch[_GUI(PlVacVariant)] = _UID(PlVacVariant);
                   }
                }
               PlVacVariant = "";
               TTagNode *tmpLineNode;
			   for (int i = 0; i < NewVariantGrid->Count; i++)
                {
				  tmpNode = NewVariantGrid->Items[i];
                  // �������
                  VacVariant += "\n"+tmpNode->Texts[0]+" - ";
                  tmpSch  = privVariant[tmpNode->Texts[1]];
                  tmpLineNode = DM->xmlVacDef->GetTagByUID(tmpSch);
                  if (tmpLineNode)
                   {
                     if (tmpLineNode->CmpName("line"))
                      VacVariant += tmpLineNode->AV["name"]+"  ����� \""+tmpLineNode->GetParent()->AV["name"]+"\"";
                   }
                  else if (tmpSch == "_NULL_")
                   {
                      VacVariant += "���.";
                   }
                  // �� �����
                  PlVacVariant += "\n"+tmpNode->Texts[0]+" - ";
                  tmpSch  = tmpPlVacSch[IntToStr((int)tmpNode->Data)];
                  tmpLineNode = DM->xmlVacDef->GetTagByUID(tmpSch);
                  if (tmpLineNode)
                   {
                     if (tmpLineNode->CmpName("line"))
                      PlVacVariant += tmpLineNode->AV["name"]+"  ����� \""+tmpLineNode->GetParent()->AV["name"]+"\"";
                   }
                  else if (tmpSch == "_NULL_")
                   {
                      PlVacVariant += "���.";
                   }
                }
               xMsg =  "�������� ��� ���������� ���������� �� ����������������.\n\n";
               xMsg += "�������� ��� ����������:\n"+VacVariant+"\n\n";
               xMsg += "�� ����� ��� ����������:\n"+PlVacVariant+"\n\n";
               xMsg += "�������� ���������� �������� ��������?\n\n";
               xMsg += "\"��\"          - �������� ��������, ��� ���������� �� �����,\n";
               xMsg += "\"���\"        - �������� ��������, ��� ���������� ��� �����,\n";
               xMsg += "\"������\" - �������� � ��������������.";
			   RC = MessageBox(Handle,xMsg.c_str(),L"���������",MB_ICONQUESTION|MB_YESNOCANCEL|MB_DEFBUTTON3);
             }
            __finally
             {
               if (tmpVacVariant) delete tmpVacVariant;
             }
            if (RC == mrCancel) return false;
            if (RC == mrYes)
             {// �������� �������� ��� ��������
               if (VacLoc) (*AResult) = 3; //���������� ������������ �������� �� �����
               else        (*AResult) = 1; //���������� �������� ����� ��������� �� ������ ����������
               (*AInsert)  = false;
               (*APlan)    = 1;
             }
            else
             {// ��������� � ���� ���������� ��������
               if (VacLoc) (*AResult) = 4; //���������� �������� ��� �����
               else        (*AResult) = 1; //���������� �������� ����� ��������� �� ������ ����������
               (*AVacCode) = SaveVacCode;
               (*AInsert) = true;
               (*APlan) = 0;
             }
          }
         else
          {// ���������� ������ �������� � ����� ������������,
           // ���� ���������� ���������
           // �������� �������� ��� ��������
            if (VacLoc) (*AResult) = 3; //���������� ������������ �������� �� �����
            else        (*AResult) = 1; //���������� �������� ����� ��������� �� ������ ����������
            (*AInsert)  = false;
            (*APlan)    = 1;
          }
       }
      else
       {// ����� �����������
         if (NotPlCode == -1)
          {// ���������� ������ �������� � ����� ����������� ������
           // ��������� � ���� ���������� ����������
            (*AResult) = 2; //���������� �������� � "�����" ��
            (*AInsert) = true;
            (*APlan)   = 0;
            (*AVacCode) = SaveVacCode;
          }
         else
          {// ���������� ������ �������� � ����� ������������,
            (*AResult)  = 2; //���������� �������� � "�����" ��
            (*AInsert)  = false;
            (*APlan)    = 1;
          }
       }
    }
   return true;
}
//---------------------------------------------------------------------------
bool __fastcall TPrivEditForm::UpdatePlan(TDate AVacDate, long AVacCode, int APrivVacCode, UnicodeString AVacVariant, int AResult, int APlan, bool AInsert)
{
   // ����  AInsert - APrivVacCode - ��� �������
   // �����           APrivVacCode - ��� �������� � �����
  bool RC = true;
  UnicodeString FSQL;
  RC &= DM->DeleteFromPlan(FUCode, AVacDate, AVacCode, false);
  if (isEdit)
   RC &= DM->DeleteFromPlan(FUCode, TDate(spriDate), spriVac.ToInt(), false);
  if (RC)
   {
	 if (AInsert)
	  {
		FSQL  = "Insert Into CARD_1084 (UCODE,CODE,LAST_INQDATE,R1085,R1086,R1087,R1088,R1089,R108A,R108B,R108C,R1098,R108D,VACTYPE,F_RESULT,F_PLAN) Values (";
		FSQL += IntToStr(FUCode)+",";
		FSQL += IntToStr(GetPlanMCode())+",";
		FSQL += "NULL,0,0,";
		FSQL += "'" + PlanMonth.FormatString("dd.mm.yyyy") + "',";
		FSQL += "'" + AVacDate.FormatString("dd.mm.yyyy") + "',";
		FSQL += IntToStr(APrivVacCode)+",";
		FSQL += "NULL,'',0,1,";
		FSQL += "'" + AVacDate.FormatString("dd.mm.yyyy") + "',";
		FSQL += "'"+AVacVariant+"',";
		FSQL += IntToStr(AResult);
		FSQL += ","+IntToStr(APlan)+")";
	  }
	 else
	  {
		FSQL  = "Update CARD_1084 Set R1098=1,";
		FSQL += "R108D='" + AVacDate.FormatString("dd.mm.yyyy") + "',";
		FSQL += "F_RESULT = "+IntToStr(AResult);
		FSQL += "Where UCODE="+IntToStr(FUCode)+" and CODE="+IntToStr(APrivVacCode);
	  }
	 RC &= DM->qtUpdate(FSQL);
   }
  return RC;
}
//---------------------------------------------------------------------------
TCheckPlanAction __fastcall TPrivEditForm::CheckPriviv(int AVacCode, TDate APrivDate, long *APlVacCode, UnicodeString &AVacType, UnicodeString &AVacSch)
{  // return: 0 - �� �������� � �����
   //         1 - �������� ��� ��������
   //         2 - �������� ��� ���������� ��� �����
   UnicodeString FSQL;
   bool CurDateLess;
   Word CurDay, CurMonth, CurYear, DayOP, MonthOP, YearOP;

   PlanMonth = TDate(0);
   DecodeDate(Date(), CurYear, CurMonth, CurDay);
   CurDateLess = DM->CalcPlanPeriod(&PlanBeg, &PlanEnd, &PlanMonth);// +++

   FSQL = "Select CODE, R1087 From CARD_1084 Where UCODE="+IntToStr(FUCode)+" and R1085=0 "; /* and R1086=0*/
   if (DM->qtExec(FSQL,false) && DM->quFree->RecordCount)
    { // ���� �������� ����
      PlanMonth = quFNDate("R1087");
      DM->CalcPlanPeriod(&PlanBeg, &PlanEnd, &PlanMonth);// +++
      if (!DM->CheckInPeriod(PlanBeg, PlanEnd, APrivDate)) return cpaNotCheck;
      else
       {
         // ����������� ������� � ����� �������� �������� ��������� ��������
         FSQL =  "Select CODE, VACTYPE, R108B  From CARD_1084 Where UCODE="+IntToStr(FUCode);
         FSQL += " and R1085=0 and R1086=0 and F_PLAN in (1,2,3,4,5,6) and R1089="+IntToStr(AVacCode);
         if (DM->qtExec(FSQL,false) && DM->quFree->RecordCount)
          { // ���� ���������� ��������� ��������
            (*APlVacCode) = quFNInt("CODE");
            AVacType      = quFNStr("VACTYPE");
            AVacSch       = quFNStr("R108B");
            return cpaCheckPlan;
          }
         else
            return cpaCheckDop;
       }
    }
   else
    {
      if (DM->CheckInPeriod(PlanBeg, PlanEnd, APrivDate)) return cpaCheckDop;
      else                                                return cpaNotCheck;
    }
}
//---------------------------------------------------------------------------
bool __fastcall TPrivEditForm::CheckInput()
{
   TDateTime PBirthDay;
   TDate FPriDate;
   DM->qtExec("Select R0031 From casebook1 Where CODE="+IntToStr(FUCode));
   if (DM->quFree->RecordCount)
	PBirthDay = quFNDate("R0031");
   else
    {
	  MessageBox(Handle,L"������ ����������� ���� �������� ��������.",L"������",0);
	  return false;
	}
   if (!TryStrToDate(RegComp(_VacDATE_)->GetValue("").c_str(), FPriDate))
	{
	  MessageBox(Handle,L"������� ���������� ���� ���������� ��������.",L"������",0);
	  return false;
	}
   if (FPriDate > Date())
	{
	  MessageBox(Handle,L"���� ���������� �������� �� ����� ���� ������ �������.",L"������",0);
      return false;
    }
   if (FPriDate < PBirthDay)
    {
	  MessageBox(Handle,L"���� ���������� �������� �� ����� ���� ������ ���� �������� ��������.",L"������",0);
      return false;
    }
   if (!RegComp(_VacCODE_)->CheckReqValue()) return false;
   if (!RegComp(_RType_)->CheckReqValue()) return false;
   if (!RegComp(_Doza_)->CheckReqValue()) return false;
   if (!RegComp(_RVal_)->CheckReqValue()) return false;
   for (int i = 0; i < NewVariantGrid->Count; i++)
    {
	  if (!NewVariantGrid->Items[i]->Texts[1].Length())
       {
		 MessageBox(Handle, L"��� ���������� ������ ���� �����.", L"������ �����", MB_ICONHAND);
         ActiveControl = NewVariantGrid;
         return false;
       }
    }
   return true;
}
//---------------------------------------------------------------------------
long __fastcall TPrivEditForm::GetMCode()
{
   long RC;
//   Variant DDD[1];
//   DDD[0] = Variant((long)FUCode);
   DM->quFree->Close();
   if (!DM->trFree->Active) DM->trFree->StartTransaction();
//   DM->quFree->ExecProcedure("MCODE_CARD_1003",DDD,0);
   DM->quFree->ExecProcedure("MCODE_CARD_1003");
   RC = DM->quFree->FN("MCODE")->AsInteger;
   DM->trFree->Commit();
   return RC;
}
//---------------------------------------------------------------------------
int __fastcall TPrivEditForm::GetPlanMCode()
{
   int RC;
//   Variant DDD[1];
//   DDD[0] = Variant((long)FUCode);
   DM->quFree->Close();
   if (!DM->trFree->Active) DM->trFree->StartTransaction();
//   DM->quFree->ExecProcedure("MCODE_CARD_1084",DDD,0);
   DM->quFree->ExecProcedure("MCODE_CARD_1084");
   RC = DM->quFree->FN("MCODE")->AsInteger;
   DM->trFree->Commit();
   return RC;
}
//---------------------------------------------------------------------------
void __fastcall TPrivEditForm::NewVariantGridKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
  if (Key == VK_RETURN) OkBtnClick(OkBtn);
}
//---------------------------------------------------------------------------
void __fastcall TPrivEditForm::VacVarColPropertiesCloseUp(TObject *Sender)
{
      if (NewVariantGrid->SelectionCount)
       {
         if (NewVariantGrid->InplaceEditor)
          {
            UnicodeString Val;
            TcxTreeListNode *itNode;
            itNode = NewVariantGrid->Selections[0];
            Val = UnicodeString((wchar_t*)NewVariantGrid->InplaceEditor->EditValue);
            itNode = itNode->GetNext();
            while (itNode)
             {
               itNode->Texts[1] = Val;
               itNode->Values[1] = Val;
               itNode = itNode->GetNext();
             }
          }
       }
}
//---------------------------------------------------------------------------
void __fastcall TPrivEditForm::NewVariantGridEnter(TObject *Sender)
{
   VacVarCol->Focused = true;
}
//---------------------------------------------------------------------------
void __fastcall TPrivEditForm::ExtSchChBClick(TObject *Sender)
{
  UnicodeString emptyString;
  CBChange(NULL, emptyString, NULL);
}
//---------------------------------------------------------------------------
void __fastcall TPrivEditForm::SetVacTypes(TStringList *AVacTypeList)
{
  TAnsiStrMap::iterator VRC;
  UnicodeString tmpInfCode,tmpVacType,tmpSch;
  // inf_code:vactype^scheme
  for (int i = 0; i < AVacTypeList->Count; i++)
   {
     tmpInfCode = GetPart1(AVacTypeList->Strings[i],':');
     tmpVacType = GetPart2(AVacTypeList->Strings[i],':');
     tmpSch     = GetPart2(tmpVacType,'^');
     tmpVacType = GetPart1(tmpVacType,'^');
     VRC = privVariantOld.find(_UID(tmpSch));
     if (VRC != privVariantOld.end())
      {
        InfNodes[tmpInfCode.ToIntDef(-1)]->Texts[1]  = privVariantOld[_UID(tmpSch)];
        InfNodes[tmpInfCode.ToIntDef(-1)]->Values[1] = privVariantOld[_UID(tmpSch)];
      }
   }
}
//---------------------------------------------------------------------------

void __fastcall TPrivEditForm::fltInfCBPropertiesChange(TObject *Sender)
{
  int FClCode = -2;
  if (fltInfCB->Text.Length())
   {
     if (fltInfCB->ItemIndex >= 0)
      {
        if (fltInfCB->Properties->Items->Strings[fltInfCB->ItemIndex] == fltInfCB->Text)
         {
           if (fltInfCB->ItemIndex > 0)
            FClCode = (int)fltInfCB->Properties->Items->Objects[fltInfCB->ItemIndex];
         }
        else
         fltInfCB->ItemIndex = -1;
      }
   }
  else
    fltInfCB->ItemIndex = -1;
  RegComp(_VacCODE_)->SetDependVal("003A",FClCode,"1075");  //
}
//---------------------------------------------------------------------------
void __fastcall TPrivEditForm::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if (Key == VK_TAB)
   FTabDown = true;
}
//---------------------------------------------------------------------------
void __fastcall TPrivEditForm::fltInfCBEnter(TObject *Sender)
{
   FTabDown = false;
}
//---------------------------------------------------------------------------

void __fastcall TPrivEditForm::fltInfCBKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if (Key == VK_TAB)
   {
     TControl *tmpCtrl;
     if (RegComp(_VacCODE_))
      {
        tmpCtrl = RegComp(_VacCODE_)->GetFirstEnabledControl();
        if (tmpCtrl)
         ((TWinControl*)tmpCtrl)->SetFocus();
      }
     Key = 0; 
   }
}
//---------------------------------------------------------------------------
bool __fastcall TPrivEditForm::InThisLPUChBChange(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource)
{
  if (DM->LPUParam.LPUCode != -1)
   {
     if (RegComp("2010")->GetValue("").ToIntDef(0) == DM->LPUParam.LPUCode)
      {
        if (DM->LPUParam.LPUPartCode != -1)
         {
           if (RegComp("200E")->GetValue("").ToIntDef(0) == DM->LPUParam.LPUPartCode)
            FCtrList->EditItems[_InThisLPU_]->SetValue("1","");
           else
            FCtrList->EditItems[_InThisLPU_]->SetValue("0","");
         }
        else
         {
           FCtrList->EditItems[_InThisLPU_]->SetValue("1","");
         }
      }
     else
      FCtrList->EditItems[_InThisLPU_]->SetValue("0","");
   }
  return true;
}
//---------------------------------------------------------------------------

