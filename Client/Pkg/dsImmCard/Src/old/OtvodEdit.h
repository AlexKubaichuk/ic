//---------------------------------------------------------------------------

#ifndef OtvodEditH
#define OtvodEditH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DMF.h"
#include "RegEd.h"
#include <CheckLst.hpp>
#include <ExtCtrls.hpp>
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
#include <ActnList.hpp>
#include "cxGraphics.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
#include <System.Actions.hpp>
//---------------------------------------------------------------------------
class TOtvodEditForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *CompPanel;
        TCheckListBox *InfChLB;
        TPanel *BtnPanel;
        TBevel *Bevel1;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TcxButton *SelAllBtn;
        TcxButton *DeSelAllBtn;
        TActionList *ActionList1;
        TPopupMenu *SelPM;
        TAction *actSelAll;
        TAction *actResetAll;
        TAction *actApply;
        TMenuItem *N1;
        TMenuItem *N2;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall DSCancel();
        bool __fastcall CheckInput();
        bool __fastcall GetInput(TTagNode *itTag, UnicodeString &UID);
        void __fastcall actSelAllExecute(TObject *Sender);
        void __fastcall actResetAllExecute(TObject *Sender);
private:	// User declarations
        int  __fastcall SetAlign(TRegEDItem *AItem, bool ALeft, int ATop, int AWidth, int ATabOrder);
        bool isEdit;
        int oldType;
        TDateTime oldFromDate,oldToDate;
        __int64 UCODE;
        TpFIBDataSet *DS;
//        TStringList *CList;
        TRegEDContainer *FCtrList;
        int FLastTop,FLastHeight;
public:		// User declarations
        long SetCODE;
        __fastcall TOtvodEditForm(TComponent* Owner, TTagNode *AClsNode, TDataSource *ASource, long UCode, bool AIsEdit);
};
//---------------------------------------------------------------------------
extern PACKAGE TOtvodEditForm *OtvodEditForm;
//---------------------------------------------------------------------------
#endif
