//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//#define lastComp ((TWinControl*)CList->Objects[CList->Count-1])
#define RegComp(a) FCtrList->EditItems[a]
//#define SetLTop   {tmpItem->Left = 2;   tmpItem->Top = xTop+3; tmpItem->Anchors.Clear(); tmpItem->Anchors << akLeft << akRight << akTop;}
#define AddRegED(a) FCtrList->AddEditItem(AClsNode->GetTagByUID(a), !AIsEdit)
#define DSInt(a)    DS->FN(a)->AsInteger
#define DSStr(a)    DS->FN(a)->AsString
#define DSDate(a)   DS->FN(a)->AsDateTime
#define DSDateF(a)  DS->FN(a)->AsDateTime.FormatString("dd.mm.yyyy")
#include "riChoiceBD.h"

#include "ProbEdit.h"
//---------------------------------------------------------------------------
#define EDLEFT1  2
#define EDLEFT2  232
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma resource "*.dfm"
TProbEditForm *ProbEditForm;
/*
<binary   name='��������� � ������ ���'        uid='10A5' required='yes'
<date     name='���� ����������'               uid='1031' required='yes'
<choiceBD name='�����'                         uid='1032' ref='002A' required='yes'
<text     name='����� ���������'               uid='1033' length='15'/>
<choice   name='�����'                         uid='1034' required='yes'
<binary   name='������� ���������'             uid='103B' required='yes'
<binary   name='���� �������'                  uid='1041'>
<choiceBD name='������ �������'                uid='10A7' ref='002B'/>
<digit    name='��������'                      uid='103F' ref='0064' required='yes' depend='1032.002A' digits='2' min='0' max='99'>
<extedit  name='�����'                         uid='1093' required='yes'
*/
//---------------------------------------------------------------------------
__fastcall TProbEditForm::TProbEditForm(TComponent* Owner, TTagNode *AClsNode,
      TDataSource *ASource, long UCode, bool AIsEdit, long AProbCode, long APlProbCode)
        : TForm(Owner)
{
//   FCurrSch = "";
   TRegEDItem *tmpItem;
   TRegEDItem *tmpItem2;
   FPlProbCode = APlProbCode;
   FProbCode = AProbCode;
   FUCode = UCode;
   SetCODE = 0;
   isEdit = AIsEdit;
   DS = (TpFIBDataSet*)ASource->DataSet;

   UnicodeString rfVal  = DS->FieldByName("R10A7")->AsString;
   UnicodeString rfVal2  = DS->FieldByName("R2009")->AsString;
   UnicodeString rfVal3  = DS->FieldByName("R200A")->AsString;
   UnicodeString rfVal4  = DS->FieldByName("R200B")->AsString;
   UnicodeString rfVal5  = DS->FieldByName("R200C")->AsString;
   if (AIsEdit)
    {
      DS->Edit();
    }
   else         DS->Insert();
   int xTop = 5;

   FCtrList = new TRegEDContainer(this,CompPanel);
   FCtrList->DefXML = DM->xmlImmCardDef;
   FCtrList->ReqColor   = DM->ReqColor;
   FCtrList->TablePref  = "";
   FCtrList->DataSource = ASource;
   FCtrList->FIBTemplateQuery = DM->quFree;
   TypeCB->Style->Color = DM->ReqColor;
//   if (AIsEdit)
//    {
//      if (DSStr("R1093") != "_NULL_")
//       FCurrSch = _GUI(DSStr("R1093"));
//    }
// UnicodeString FFl = "";
// AClsNode->Iterate(GetCardFields,FFl);
// FFl = "<fllist>"+FFl+"</fllist>";

// TTagNode *flNode = new TTagNode(NULL);
// flNode->AsXML = FFl;
// TTagNode *itNode = flNode->GetFirstChild();
   int FOrder = 0;
   int FReak = 1;
   // <date name='���� ����������' uid='1031' required='yes' inlist='list'/>
   tmpItem = AddRegED("1031");  SetAlign(tmpItem, EDLEFT1, xTop, 200, FOrder++);
   // <choiceBD name='�����/��������' uid='1032' ref='002A' required='yes' inlist='list'/>
   tmpItem = AddRegED("1032");  xTop += SetAlign(tmpItem, EDLEFT2, xTop, 250, FOrder++);
   tmpItem->OnDataChange = CBChange;
   TypeCB->Top = xTop; TypeCB->Left = 80; TypeCB->TabOrder = FOrder++;
   TypeLab->Top = xTop; TypeLab->Left = 7;
   TypeCB->Height+4;
   // <text name='����� ���������' uid='1033' inlist='list' cast='no' length='20'/>
   tmpItem = AddRegED("1033");  xTop += SetAlign(tmpItem, EDLEFT2, xTop, 250, FOrder++);
   // <choice name='�����' uid='1034' required='yes' inlist='list_ext' default='0'>
   tmpItem = AddRegED("1034");  xTop += SetAlign(tmpItem, EDLEFT1, xTop, 200, FOrder++);
   tmpItem->OnDataChange = CBChange;
   // <binary name='������� ���������' uid='103B' required='yes' inlist='list_ext' default='check'/>
   tmpItem = AddRegED("103B");  SetAlign(tmpItem, EDLEFT1, xTop, 300, FOrder++);
   // <binary name='���� �������' uid='1041' inlist='list_ext' invname='������� �������������'>
   tmpItem = AddRegED("1041");  xTop += SetAlign(tmpItem, EDLEFT2, xTop, 300, FOrder++);
   // <choiceBD name='������ �������' uid='10A7' ref='0038' required='yes' inlist='list' depend='1032.002B'>
   tmpItem = AddRegED("10A7");          SetAlign(tmpItem, 5, xTop, 300, FOrder++);
   tmpItem->SetEDLeft(70);      tmpItem->SetLabel("������� "+IntToStr(FReak++));
   // <digit name='��������' uid='103F' ref='008E' required='yes' inlist='list' depend='10A7.002B' digits='5' min='2' max='32767'>
   tmpItem = AddRegED("103F");  xTop += SetAlign(tmpItem, 200, xTop, 300, FOrder++);
   tmpItem->SetEDLeft(5);
   // <choiceBD name='������ ������� 2' uid='2009' ref='0038' required='yes' inlist='list_ext' depend='1032.002B'>
   tmpItem = AddRegED("2009");          SetAlign(tmpItem, 5, xTop, 300, FOrder++);
   tmpItem->SetEDLeft(70);      tmpItem->SetLabel("������� "+IntToStr(FReak++));
   // <digit name='�������� 2' uid='2004' inlist='list_ext' digits='5' min='2' max='32767'>
   tmpItem = AddRegED("2004");  xTop += SetAlign(tmpItem, 200, xTop, 300, FOrder++);
   tmpItem->SetEDLeft(5);
   // <choiceBD name='������ ������� 3' uid='200A' ref='0038' required='yes' inlist='list_ext' depend='1032.002B'>
   tmpItem = AddRegED("200A");          SetAlign(tmpItem, 5, xTop, 300, FOrder++);
   tmpItem->SetEDLeft(70);      tmpItem->SetLabel("������� "+IntToStr(FReak++));
   // <digit name='�������� 3' uid='2005' inlist='list_ext' digits='5' min='2' max='32767'>
   tmpItem = AddRegED("2005");  xTop += SetAlign(tmpItem, 200, xTop, 300, FOrder++);
   tmpItem->SetEDLeft(5);
   // <choiceBD name='������ ������� 4' uid='200B' ref='0038' required='yes' inlist='list_ext' depend='1032.002B'>
   tmpItem = AddRegED("200B");          SetAlign(tmpItem, 5, xTop, 300, FOrder++);
   tmpItem->SetEDLeft(70);      tmpItem->SetLabel("������� "+IntToStr(FReak++));
   // <digit name='�������� 4' uid='2006' inlist='list_ext' digits='5' min='2' max='32767'>
   tmpItem = AddRegED("2006");  xTop += SetAlign(tmpItem, 200, xTop, 300, FOrder++);
   tmpItem->SetEDLeft(5);
   // <choiceBD name='������ ������� 5' uid='200C' ref='0038' required='yes' inlist='list_ext' depend='1032.002B'>
   tmpItem = AddRegED("200C");          SetAlign(tmpItem, 5, xTop, 300, FOrder++);
   tmpItem->SetEDLeft(70);      tmpItem->SetLabel("������� "+IntToStr(FReak++));
   // <digit name='�������� 5' uid='2007' inlist='list_ext' digits='5' min='2' max='32767'>
   tmpItem = AddRegED("2007");  xTop += SetAlign(tmpItem, 200, xTop, 300, FOrder++);
   tmpItem->SetEDLeft(5);
   // <choiceBD name='���' uid='2011' ref='00C7' inlist='list_ext'>
   tmpItem = AddRegED("2011");  SetAlign(tmpItem, EDLEFT1, xTop, 250, FOrder++);
   tmpItem->SetEDLeft(73);
   // <choiceBD name='�������������' uid='200F' ref='000C' inlist='list_ext' depend='2011.000C'>
   tmpItem = AddRegED("200F");  xTop += SetAlign(tmpItem, 200, xTop, 250, FOrder++);
   // <binary name='��������� � ������ ���' uid='10A5' required='yes' inlist='list_ext' invname='��������� � ������ �����������' default='check'/>
   tmpItem = AddRegED("10A5");  xTop += SetAlign(tmpItem, EDLEFT1, xTop, 300, FOrder++);
   RegComp("2011")->OnDataChange = InThisLPUChBChange;
   RegComp("200F")->OnDataChange = InThisLPUChBChange;
   // <choiceBD name='����' uid='10B2' ref='00AA' inlist='list_ext'/>
   tmpItem = AddRegED("10B2");  SetAlign(tmpItem, 7, xTop, 250, FOrder++);
   tmpItem->SetLabel(tmpItem->DefNode->AV["name"]);
   tmpItem->SetEDLeft(68);
   // <choiceBD name='���. ������' uid='200D' ref='00AA' inlist='list_ext'/>
   tmpItem = AddRegED("200D");  xTop += SetAlign(tmpItem, 205, xTop, 245, FOrder++);
   tmpItem->SetLabel(tmpItem->DefNode->AV["name"]);
   // <choiceBD name='�������� ��������������' uid='10B3' ref='00AB' inlist='list_ext' linecount='2'/>
   tmpItem = AddRegED("10B3");  xTop += SetAlign(tmpItem, EDLEFT1, xTop, 300, FOrder++);
   ((TWinControl*)tmpItem->GetControl())->Width = 290;
   tmpItem->SetEDLeft(155);
   // <choiceBD name='���. ����������' uid='2003' ref='00BE' inlist='list_ext'/>
   tmpItem = AddRegED("2003");  xTop += SetAlign(tmpItem, EDLEFT1, xTop, 300, FOrder++);
   ((TWinControl*)tmpItem->GetControl())->Width = 290;
   tmpItem->SetEDLeft(155);

   Height = xTop + 60;
   if (AIsEdit)
    {
      RegComp("10A7")->SetValue(rfVal);
      RegComp("2009")->SetValue(rfVal2);
      RegComp("200A")->SetValue(rfVal3);
      RegComp("200B")->SetValue(rfVal4);
      RegComp("200C")->SetValue(rfVal5);

      Caption = DM->xmlImmCardDef->GetTagByUID("102F")->GetChildByName("description")->AV["editlabel"];
      sProbDate = RegComp("1031")->GetValue("");
      sProbCode = RegComp("1032")->GetValue("002A.");
      if (DSStr("R1093") != "_NULL_")
       {
         int FIdx = TypeCB->Properties->Items->IndexOfObject((TObject*)UIDInt(_UID(DSStr("R1093"))));
         TypeCB->ItemIndex = FIdx;
       }
    }
   else
    {
//      RegComp("1031")->SetValue(TDateTime::CurrentDate().FormatString("dd.mm.yyyy"));  // '���� ����������' default='CurrentDate'/>
      Caption = DM->xmlImmCardDef->GetTagByUID("102F")->GetChildByName("description")->AV["insertlabel"];
    }
   if (!AIsEdit)
    {
      if (DM->LPUParam.LPUCode != -1)
       FCtrList->EditItems["2011"]->SetValue(IntToStr(DM->LPUParam.LPUCode),"");
      if (DM->LPUParam.LPUPartCode != -1)
       FCtrList->EditItems["200F"]->SetValue(IntToStr(DM->LPUParam.LPUPartCode),"");
    }
   if (FPlProbCode >= 0)
    {// ������� ���������� ����� � �����
      RegComp("1032")->SetEnable(false); /*Del+++*/          // '�������'                uid='1020' required='yes' ref='0035' inlist='list' depend='1075.002D'/>
      try
       {
         ((TriChoiceBD*)RegComp("1032"))->SetIsTmpl(true);
         RegComp("1032")->SetValue(IntToStr(FProbCode),false);  // '�����'                         uid='1032' ref='002A' required='yes'
       }
      __finally
       {
         ((TriChoiceBD*)RegComp("1032"))->SetIsTmpl(false);
       }
//      RegComp("1034")->SetEnable(false); /*Del+++*/
      RegComp("1034")->SetValue("0",false);                  // '�����'                         uid='1034' required='yes'
      if (DM->qtExec("Select R108B From CARD_1084 Where UCODE="+IntToStr(FUCode)+" and CODE="+IntToStr(FPlProbCode),false) && DM->quFree->RecordCount)
       {
         int FIdx = TypeCB->Properties->Items->IndexOfObject((TObject*)UIDInt(_UID(quFNStr("R108B"))));
         TypeCB->ItemIndex = FIdx;
       }
    }
}
//---------------------------------------------------------------------------
bool __fastcall TProbEditForm::GetCardFields(TTagNode *ANode, UnicodeString &AFields)
{
   if (ANode->CmpName("text,extedit,binary,date,datetime,time,digit,choice,choiceBD,choiceTree"))
    {
      AFields += "<fl uid=\""+ANode->AV["uid"]+"\"/>";
    }
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TProbEditForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
   if (DS->State != dsBrowse) DS->Cancel();
   if (FCtrList) delete FCtrList;
   Action = caFree;
}
//---------------------------------------------------------------------------
void __fastcall TProbEditForm::OkBtnClick(TObject *Sender)
{
   UnicodeString xProb,xOldProbVal,xOldProbFormat,xProbDate;
   if (!CheckInput()) return;
   ActiveControl = OkBtn;
   xProb = RegComp("1032")->GetValue("002A.");
   xProbDate = RegComp("1031")->GetValue("");

   TDate FProbDate;
   if (!TryStrToDate(xProbDate.c_str(), FProbDate))
    {
	  MessageBox(Handle,L"������� ���������� ���� ���������� �����",L"������",0);
      return;
    }

   long FSetProbeCode = xProb.ToInt();
   long SaveProbeCode = FSetProbeCode;
   int FResult = 0;
   int FPlan = 0;
   int FCheckPlProbe = 0;
   bool FInsert = false;

   if (!CheckInPlan(FProbDate, &FSetProbeCode, &FResult, &FPlan, &FInsert, &FCheckPlProbe)) return;
   // ���������� � ����������� ����� ���������� �����
   if (TypeCB->ItemIndex >= 0)
    {
//      FCurrSch =
      TTagNode *tmpSch = DM->xmlTestDef->GetTagByUID(UIDStr((int)TypeCB->Properties->Items->Objects[TypeCB->ItemIndex]));
      if (tmpSch)
       {
         DSStr("R1093") = tmpSch->GetParent("schema")->AV["uid"]+"."+tmpSch->AV["uid"];
       }
      else
       DSStr("R1093") = "_NULL_";
    }
   else
    DSStr("R1093") = "_NULL_";
   // ���������� �����
   if (!DM->trUpdate->Active) DM->trUpdate->StartTransaction();
   try
    {
      xOldProbVal = "-1";
	  DM->qtExec("Select CODE From CARD_102F Where UCODE="+IntToStr(FUCode)+" and R1031='"+xProbDate+"' and R1032="+xProb,false);
      if (DM->quFree->RecordCount)
       {
         if (isEdit)
          {
			if (quFNInt("CODE") != DSInt("CODE"))
             {
			   MessageBox(Handle,("����� ���������� (���� - \""+xProbDate+"\", ����� - \""+DM->ProbList[xProb]+"\"), ������� ��������� � ��������� ������.").c_str(),L"���������",MB_ICONHAND);
               return;
             }
          }
         else
          {
			MessageBox(Handle,("����� ���������� (���� - \""+xProbDate+"\", ����� - \""+DM->ProbList[xProb]+"\"), ������� ��������� � ��������� ������.").c_str(),L"���������",MB_ICONHAND);
            return;
          }
       }
      if (!isEdit)
       {
         DSInt("CODE") = GetMCode();
         DSInt("UCODE") = FUCode;
       }
      // ����������� �������� ������� ���������� �����
	  DM->qtExec("Select Max(R1031) as MDATE From CARD_102F Where UCODE="+IntToStr(FUCode)+" and R1031<'"+xProbDate+"' and R1032="+xProb,false);
      if (DM->quFree->RecordCount)
       {
         if (!DM->quFree->FN("MDATE")->IsNull)
          { // ���������� ���������� �����
            DM->qtExec("Select R103F, R10A7 From CARD_102F Where UCODE="+IntToStr(FUCode)+" and R1031='"+quFNStr("MDATE")+"' and R1032="+xProb,false);
            if (DM->quFree->RecordCount)
             { // ���������� ���������� �����
               if (!DM->quFree->FN("R103F")->IsNull)
                xOldProbVal = quFNStr("R103F");
               if (!DM->quFree->FN("R10A7")->IsNull)
                xOldProbFormat = quFNStr("R10A7");
             }
          }
       }
      DSStr("R10C1") = xOldProbVal;
      DSStr("R10C2") = xOldProbFormat;
      SetCODE = DSInt("CODE");
      bool isComm = true;
      try
       {
         UnicodeString FSetSch = DSStr("R1093");
         DS->Post();
         if (FCheckPlProbe)
          isComm &= UpdatePlan(FProbDate, SaveProbeCode, FSetProbeCode, FResult, FPlan, FInsert);
         else
          {
            if (isEdit)
             isComm &= DM->DeleteFromPlan(FUCode, TDate(sProbDate), sProbCode.ToInt(), true);
          }
         if (FSetSch.Length() && (FSetSch != "_NULL_"))
          {
            TTagNode *tmp = DM->xmlTestDef->GetTagByUID(_UID(FSetSch));
            if (tmp)
             {
               UnicodeString ChSQL;
               UnicodeString FInfCode = "";
               TIntListMap::iterator RC;
               TIntMap *vti;
               RC = DM->ProbInfList.find(xProb.ToIntDef(-1));
               if (RC != DM->ProbInfList.end())
                {
                  vti = DM->ProbInfList[xProb.ToIntDef(-1)];
                  if (vti->begin() != vti->end())
                   {
                     FInfCode = IntToStr(vti->begin()->first);
                   }
                }
               if (FInfCode.Length())
                {
                  bool CanSchChange = true;
                  ChSQL = "Select Max(R1031) as MDATE From CARD_102F Where R1093 <> '_NULL_' and UCODE="+IntToStr(FUCode)+" and R1032 = "+xProb;
                  if (DM->qtExec(ChSQL,false))
                   {
                     if (DM->quFree->RecordCount)
                      {
                        if (DM->quFree->FN("MDATE")->AsDateTime > TDate(xProbDate))
                         CanSchChange = false;
                      }
                   }
                  if (CanSchChange)
                   {
                     isComm &= DM->qtUpdate("Update CARD_1076 Set R1079='"+FSetSch+"' Where UCODE="+IntToStr(FUCode)+" and R1077="+FInfCode);
                   }
                }
             }
          }
         if (isComm)
          {
            if (DS->Transaction->Active) DS->Transaction->Commit();
            if (DM->trUpdate->Active)    DM->trUpdate->Commit();
            if (isEdit)
             DM->RegCompSQLAfterEdit("CARD_102F", IntToStr(FUCode), "102F");
            else
             DM->RegCompSQLAfterInsert("CARD_102F", IntToStr(FUCode), "102F");

          }
       }
      catch(...)
       {
         DS->Cancel();
         if (DM->trUpdate->Active) DM->trUpdate->Rollback();
         if (DS->Transaction->Active) DS->Transaction->Rollback();
       }
    }
   catch(...)
    {
      DS->Cancel();
      if (DM->trUpdate->Active) DM->trUpdate->Rollback();
      if (DS->Transaction->Active) DS->Transaction->Rollback();
    }
   ModalResult = mrOk;
}
//---------------------------------------------------------------------------
long __fastcall TProbEditForm::GetMCode()
{
   long RC;
//   Variant DDD[1];
//   DDD[0] = Variant((long)FUCode);
   DM->quFree->Close();
   if (!DM->trFree->Active) DM->trFree->StartTransaction();
//   DM->quFree->ExecProcedure("MCODE_CARD_102F",DDD,0);
   DM->quFree->ExecProcedure("MCODE_CARD_102F");
   RC = DM->quFree->FN("MCODE")->AsInteger;
   DM->trFree->Commit();
   return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TProbEditForm::CheckInput()
{
   TDateTime PBirthDay;
   TDate FProbDate;
   DM->qtExec("Select R0031 From casebook1 Where CODE="+IntToStr(FUCode));
   if (DM->quFree->RecordCount)
    PBirthDay = quFNDate("R0031");
   else
    {
	  MessageBox(Handle,L"������ ����������� ���� �������� ��������.",L"������",0);
      return false;
    }
   if (!RegComp("1031")->CheckReqValue()) return false;
   if (!TryStrToDate(RegComp("1031")->GetValue("").c_str(), FProbDate))
    {
	  MessageBox(Handle,L"������� ���������� ���� ���������� �����",L"������",0);
	  return false;
	}
   if (FProbDate > Date())
	{
	  MessageBox(Handle,L"���� ���������� ����� �� ����� ���� ������ �������",L"������",0);
	  return false;
	}
   if (FProbDate < PBirthDay)
	{
	  MessageBox(Handle,L"���� ���������� ����� �� ����� ���� ������ ���� �������� ��������.",L"������",0);
	  return false;
	}
   if (TypeCB->ItemIndex < 0)
	{
	  MessageBox(Handle,L"������� ��� �����",L"������",0);
	  return false;
	}
   if (!RegComp("1032")->CheckReqValue()) return false;
   if (!RegComp("1034")->CheckReqValue()) return false;
   if (!RegComp("103F")->CheckReqValue()) return false;
   if (!RegComp("10A7")->CheckReqValue()) return false;
   return true;
}
//---------------------------------------------------------------------------
int __fastcall TProbEditForm::GetPlanMCode()
{
   int RC;
//   Variant DDD[1];
//   DDD[0] = Variant((long)FUCode);
   DM->quFree->Close();
   if (!DM->trFree->Active) DM->trFree->StartTransaction();
//   DM->quFree->ExecProcedure("MCODE_CARD_1084",DDD,0);
   DM->quFree->ExecProcedure("MCODE_CARD_1084");
   RC = DM->quFree->FN("MCODE")->AsInteger;
   DM->trFree->Commit();
   return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TProbEditForm::CheckInPlan(TDate AProbeDate, long *AProbCode, int *AResult, int *APlan, bool *AInsert, int *ACheckPlProbe)
{
/*
                                                                Result  C��� ?  ���. ������.?   �� ����� ?
���������� �������� � "�����" ��                                2       0       X               X
���������� �������� ����� ��������� �� ������ ����������        1       1       0               X
���������� �������� ��� �����                                   4       1       1               0
���������� ������������ �������� �� �����                       3       1       1               1

Rezult = 1 ���������� �������� ����� ��������� �� ������ ����������;
           (��� ��������������(�����) ��� �� ����������� � �� ���������� ������� ���������� �������� ������������)
Rezult = 2 ���������� �������� � "�����" ��;
           (��������. �������. ������� ���������� �������� ������������ �� �������������)
Rezult = 3 ���������� ������������ �������� �� �����;
Rezult = 4 ���������� �������� ��� �����;

��� �������� ��������/����� ������ � ����������� ��������/����� ��������� �� ��������� �����,
� ������ � �������� ��������/����� �������������� ����� �������� ���������� � ����������.
*/
   bool VacLoc;
   bool OtherOrg;
   long SaveProbCode = (*AProbCode);
   // ����������� ����/�����
   OtherOrg = (DM->qtExec("Select CODE From CASEBOOK1 Where CODE="+IntToStr(FUCode)+" and R0025 in (2,3)", false) && DM->quFree->RecordCount);
   // ����������� ����� ���������� ��������
   VacLoc = (RegComp("10A5")->GetValue("") == "1");

   if (FPlProbCode >= 0)
    {// ������� �� �����
      (*ACheckPlProbe) = 1;
      (*AProbCode) = FPlProbCode;
      if (DM->qtExec("Select R1087 From CARD_1084 Where UCODE="+IntToStr(FUCode)+" and CODE="+IntToStr(FPlProbCode),false) && DM->quFree->RecordCount)
       {
         PlanMonth = quFNDate("R1087");
         DM->CalcPlanPeriod(&PlanBeg, &PlanEnd, &PlanMonth); // +++
         if (!DM->CheckInPeriod(PlanBeg, PlanEnd, AProbeDate))
          {
            UnicodeString xMsg;
            xMsg =  "���� ���������� ����� ������ ���� � �������� ��������� �������!\n\n";
            xMsg += "C "+PlanBeg.FormatString("dd.mm.yyyy");
            xMsg += " �� "+PlanEnd.FormatString("dd.mm.yyyy");
			MessageBox(Handle,xMsg.c_str(),L"���������",MB_ICONINFORMATION);
            return false;
          }
       }
    }
   else
    { // ������� �� ����������� �����
      (*ACheckPlProbe) = CheckProbe((*AProbCode), AProbeDate, AProbCode);
    }
   if ((*ACheckPlProbe))
    {
      TReplaceFlags repF;
      repF << rfReplaceAll;
      long PlCode, NotPlCode;
      bool OrgType;
      if (!OtherOrg)
       {// ���� �����������
         if ((*ACheckPlProbe) == 2)
          {// ���������� ������ �������� � ����� ����������� ������
           // ��������� � ���� ���������� ����������
            (*AInsert)             = true;
            if (VacLoc) (*AResult) = 4; // ���������� �������� ��� �����
            else        (*AResult) = 1; // ���������� �������� ����� ��������� �� ������ ����������
            (*APlan)               = 0;
            (*AProbCode) = SaveProbCode;
          }
         else
          {// ���������� ������ �������� � ����� ������������,
           // ���� ���������� ���������
           // �������� �������� ��� ��������
            if (VacLoc) (*AResult) = 3; //���������� ������������ �������� �� �����
            else        (*AResult) = 1; //���������� �������� ����� ��������� �� ������ ����������
            (*AInsert)  = false;
            (*APlan)    = 1;
          }
       }
      else
       {// ����� �����������
         if (NotPlCode == -1)
          {// ���������� ������ �������� � ����� ����������� ������
           // ��������� � ���� ���������� ����������
            (*AResult) = 2; //���������� �������� � "�����" ��
            (*AInsert) = true;
            (*APlan)   = 0;
            (*AProbCode) = SaveProbCode;
          }
         else
          {// ���������� ������ �������� � ����� ������������,
            (*AResult)  = 2; //���������� �������� � "�����" ��
            (*AInsert)  = false;
            (*APlan)    = 1;
          }
       }
	}
   return true;
}
//---------------------------------------------------------------------------
bool __fastcall TProbEditForm::UpdatePlan(TDate AProbeDate, long AProbCode, int AProbePlProbeCode, int AResult, int APlan, bool AInsert)
{
   // ����  AInsert - AProbePlProbeCode - ��� �������
   // �����           AProbePlProbeCode - ��� �������� � �����
  bool RC = true;
  UnicodeString FSQL;
  RC &= DM->DeleteFromPlan(FUCode, AProbeDate, AProbCode, true);
  if (isEdit)
   RC &= DM->DeleteFromPlan(FUCode, TDate(sProbDate), sProbCode.ToInt(), true);
  if (RC)
   {
	 if (AInsert)
	  {
		FSQL  = "Insert Into CARD_1084 (UCODE,CODE,LAST_INQDATE,R1085,R1086,R1087,R1088,R1089,R108A,R108B,R108C,R1098,R108D,VACTYPE,F_RESULT,F_PLAN) Values (";
		FSQL += IntToStr(FUCode)+",";
		FSQL += IntToStr(GetPlanMCode())+",";
		FSQL += "NULL,0,1,";
		FSQL += "'" + PlanMonth.FormatString("dd.mm.yyyy") + "',";
		FSQL += "'" + AProbeDate.FormatString("dd.mm.yyyy") + "',";
		FSQL += "NULL,";
		FSQL += IntToStr(AProbePlProbeCode)+",'',0,1,";
		FSQL += "'" + AProbeDate.FormatString("dd.mm.yyyy") + "',";
		FSQL += "'',";
		FSQL += IntToStr(AResult);
		FSQL += ","+IntToStr(APlan)+")";
	  }
	 else
	  {
		FSQL  = "Update CARD_1084 Set R1098=1,";
		FSQL += "R108D='" + AProbeDate.FormatString("dd.mm.yyyy") + "',";
		FSQL += "F_RESULT = "+IntToStr(AResult);
		FSQL += "Where UCODE="+IntToStr(FUCode)+" and CODE="+IntToStr(AProbePlProbeCode);
	  }
	 RC &= DM->qtUpdate(FSQL);
   }
  return RC;
}
//---------------------------------------------------------------------------
int __fastcall TProbEditForm::CheckProbe(int AProbCode, TDate APrivDate, long *APlVacCode)
{  // return: 0 - �� �������� � �����
   //         1 - �������� ��� ��������
   //         2 - �������� ��� ���������� ��� �����
   UnicodeString FSQL,FPovod;
   bool CurDateLess;
   Word CurDay, CurMonth, CurYear, DayOP, MonthOP, YearOP;

   FPovod = RegComp("1034")->GetValue("");
   PlanMonth = TDate(0);
   DecodeDate(Date(), CurYear, CurMonth, CurDay);
   CurDateLess = DM->CalcPlanPeriod(&PlanBeg, &PlanEnd, &PlanMonth);// +++
   FSQL = "Select CODE, R1087 From CARD_1084 Where UCODE="+IntToStr(FUCode)+" and R1085=0 "; /*and R1086=1*/
   if (FPovod == "0")
    { // ����� - ����
      if (DM->qtExec(FSQL,false) && DM->quFree->RecordCount)
       { // ���� �������� ����
         PlanMonth = quFNDate("R1087");
         DM->CalcPlanPeriod(&PlanBeg, &PlanEnd, &PlanMonth); // +++
         if (!DM->CheckInPeriod(PlanBeg, PlanEnd, APrivDate)) return 0;
         else
          {
            // ����������� ������� � ����� �������� �������� ��������� ��������
            FSQL =  "Select CODE From CARD_1084 Where UCODE="+IntToStr(FUCode);
            FSQL += " and R1085=0 and R1086=1 and F_PLAN in (1,2,3,4,5,6) and R108A="+IntToStr(AProbCode);
            if (DM->qtExec(FSQL,false) && DM->quFree->RecordCount)
             { // ���� ���������� ����� ��������� ������
               (*APlVacCode) = quFNInt("CODE");
               return 1;
             }
            else
               return 2;
          }
       }
      else
       {
         if (DM->CheckInPeriod(PlanBeg, PlanEnd, APrivDate)) return 2;
         else                                                return 0;
       }
    }
   else
    { // ����� - ����. ��������� ��� ������
      if (DM->CheckInPeriod(PlanBeg, PlanEnd, APrivDate)) return 2;
      else                                                return 0;
    }
}
//---------------------------------------------------------------------------
int  __fastcall TProbEditForm::SetAlign(TRegEDItem *AItem, int ALeft, int ATop, int AWidth, int ATabOrder)
{
  AItem->Left = ALeft;
  AItem->Width = AWidth;
  AItem->Top = ATop;
  AItem->Anchors.Clear();
  AItem->Anchors << akLeft << akRight << akTop;
  AItem->TabOrder = ATabOrder;
  return AItem->Height+4;
}
//---------------------------------------------------------------------------
bool __fastcall TProbEditForm::CBChange(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource)
{
  bool RC = false;
  try
   {
     UnicodeString xProb,xOldProbVal,xOldProbFormat,xProbDate;
     xProb = RegComp("1032")->GetValue("002A.");
     TypeCB->Properties->Items->Clear();
     if (RegComp("1034")->GetValue("") != "1")
      {
        TIntListMap::iterator RC;
        TIntMap *vti;
        UnicodeString tmpSch;
        RC = DM->ProbInfList.find(xProb.ToIntDef(-1));
        if (RC != DM->ProbInfList.end())
         {
           vti = DM->ProbInfList[xProb.ToIntDef(-1)];
           if (vti->begin() != vti->end())
            {
              UnicodeString FtmpInfCode = IntToStr(vti->begin()->first);
  //            DM->qtExec("Select R1079 From CARD_1076 Where UCODE="+IntToStr(FUCode)+"and  R1077="+FtmpInfCode,false);
  //            if (!FCurrSch.Length())
  //             FCurrSch = _GUI(quFNStr("R1079"));
              TTagNode *tmp;// = DM->xmlTestDef->GetTagByUID(FCurrSch);
  //            if (tmp)
  //             {
                 TTagNodeMap SchList;
                 DM->GetSchemeList(FtmpInfCode.ToIntDef(-1), SchList, false);
                 for (TTagNodeMap::iterator i = SchList.begin(); i != SchList.end(); i++)
                  {
                    if (i->second->CmpAV("ref",xProb))
                     {
                       tmp = i->second->GetFirstChild();
                       while (tmp)
                        {
                          if (tmp->CmpName("line"))
                           {
                             TypeCB->Properties->Items->AddObject(i->second->AV["name"]+"."+tmp->AV["name"],(TObject*)UIDInt(tmp->AV["uid"]));
                           }
                          tmp = tmp->GetNext();
                        }
                     }
                  }
  //             }
            }
         }
      }
     else
      {
  //      FCurrSch = "";
        TypeCB->Properties->Items->AddObject("���.",(TObject*)0);
      }
     if (TypeCB->Properties->Items->Count == 1)
      TypeCB->ItemIndex = 0;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TProbEditForm::InThisLPUChBChange(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource)
{
  if (DM->LPUParam.LPUCode != -1)
   {
     if (RegComp("2011")->GetValue("").ToIntDef(0) == DM->LPUParam.LPUCode)
      {
        if (DM->LPUParam.LPUPartCode != -1)
         {
           if (RegComp("200F")->GetValue("").ToIntDef(0) == DM->LPUParam.LPUPartCode)
            FCtrList->EditItems["10A5"]->SetValue("1","");
           else
            FCtrList->EditItems["10A5"]->SetValue("0","");
         }
        else
         {
           FCtrList->EditItems["10A5"]->SetValue("1","");
         }
      }
     else
      FCtrList->EditItems["10A5"]->SetValue("0","");
   }
  return true;
}
//---------------------------------------------------------------------------

