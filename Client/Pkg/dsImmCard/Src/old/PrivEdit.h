//---------------------------------------------------------------------------

#ifndef PrivEditH
#define PrivEditH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
#include <Menus.hpp>
#include "cxButtons.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxMaskEdit.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
#include "DMF.h"
#include "RegEd.h"
#include "cxClasses.hpp"
#include "cxLookAndFeels.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
/*
#include <ExtCtrls.hpp>
#include "ICSDrawGrid.h"
#include <ValEdit.hpp>
#include "RXGrids.hpp"
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxDropDownEdit.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxEdit.hpp"
#include "cxMaskEdit.hpp"
*/
//---------------------------------------------------------------------------
typedef  map<int, TcxTreeListNode*> TTreeListNodeMap;
enum  TCheckPlanAction
{
  cpaNotCheck  = 0, // �� �������� � �����
  cpaCheckPlan = 1, // �������� ��� ��������
  cpaCheckDop  = 2  // �������� ��� ���������� ��� �����
};
//---------------------------------------------------------------------------
class TPrivEditForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TBevel *Bevel1;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TcxStyleRepository *cxStyleRepository1;
        TcxStyle *cxStyle1;
 TPanel *CompPanel;
 TBevel *Bevel2;
 TBevel *AdmBl;
 TLabel *Label1;
 TPanel *InfPanel;
 TcxTreeList *NewVariantGrid;
 TcxTreeListColumn *InfCol;
 TcxTreeListColumn *VacVarCol;
 TcxTreeListColumn *CurrSchCol;
 TPanel *VacSchPanel;
 TLabel *SchLab;
 TLabel *SchValLab;
 TcxCheckBox *ExtSchChB;
 TcxComboBox *fltInfCB;
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        bool __fastcall CBChange(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource);
        void __fastcall OkBtnClick(TObject *Sender);
        long __fastcall GetMCode();
        int __fastcall GetPlanMCode();
        bool __fastcall CheckInput();
        bool __fastcall CheckInPlan(TDate AVacDate, long *AVacCode, UnicodeString &AVacVariant, int *AResult, int *APlan, bool *AInsert, TCheckPlanAction *ACheckPlPriv);
        void __fastcall NewVariantGridKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall VacVarColPropertiesCloseUp(TObject *Sender);
        void __fastcall NewVariantGridEnter(TObject *Sender);
        void __fastcall ExtSchChBClick(TObject *Sender);
        void __fastcall fltInfCBPropertiesChange(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall fltInfCBEnter(TObject *Sender);
        void __fastcall fltInfCBKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
        bool isEdit;
        __int64 FUCode,FVacCode;
        TTreeListNodeMap InfNodes;
        int FPrivCode;
        TTagNode *FShema;
        TpFIBDataSet *DS;
        TDate PlanBeg;
        TDate PlanEnd;
        TDate PlanMonth;
        TAnsiStrMap privVariant;
        TAnsiStrMap privVariantUID;
        TAnsiStrMap privVariantOld;
        TStringList *CList;
        TNotifyEvent VacCh;
        TStringList *InfList;
        TStringList *OldInfList;
        UnicodeString spriTur,spriDate, priDate, priLoc, priVac, spriVac;
        bool __fastcall PriDateChange(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource);
        bool __fastcall InThisLPUChBChange(TTagNode *ItTag, UnicodeString &Src, TDataSource *ASource);
        bool __fastcall UpdatePlan(TDate AVacDate, long AVacCode, int APrivVacCode, UnicodeString AVacVariant, int AResult, int APlan, bool AInsert);
        int  __fastcall SetAlign(TRegEDItem *AItem, int ALeft, int ATop, int AWidth, int ATabOrder);
        TCheckPlanAction __fastcall CheckPriviv(int AVacCode, TDate APrivDate, long *APlVacCode, UnicodeString &AVacType, UnicodeString &AVacSch);
        void __fastcall SetVacTypes(TStringList *AVacTypeList);
        UnicodeString __fastcall PriDozeVal(TDateTime APriDate, TRegEDItem *AVacItem);
        TRegEDContainer *FCtrList;
        TRegEDContainer *FCtrList1;
        int FLastTop,FLastHeight;
        bool FTabDown;
        TDateTime  FPatBirthday;
public:		// User declarations
      long SetCODE;
        __fastcall TPrivEditForm(TComponent* Owner, TTagNode *AClsNode,
      TDataSource *ASource, __int64 UCode, bool AIsEdit, long AVacCode = 0, long APrivCode = -1, bool AIsTur = false);
};
//---------------------------------------------------------------------------
extern PACKAGE TPrivEditForm *PrivEditForm;
//---------------------------------------------------------------------------
#endif
