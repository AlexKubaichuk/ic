object ProbEditForm: TProbEditForm
  Left = 471
  Top = 270
  BorderStyle = bsToolWindow
  Caption = 'ProbEditForm'
  ClientHeight = 378
  ClientWidth = 492
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object CompPanel: TPanel
    Left = 0
    Top = 0
    Width = 492
    Height = 341
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object TypeLab: TLabel
      Left = 32
      Top = 265
      Width = 57
      Height = 13
      Caption = #1042#1080#1076' '#1087#1088#1086#1073#1099':'
    end
    object TypeCB: TcxComboBox
      Left = 95
      Top = 262
      Properties.DropDownListStyle = lsEditFixedList
      Style.Color = clWindow
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 0
      Width = 120
    end
  end
  object BtnPanel: TPanel
    Left = 0
    Top = 341
    Width = 492
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      492
      37)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 492
      Height = 5
      Align = alTop
      Shape = bsTopLine
    end
    object OkBtn: TcxButton
      Left = 318
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = OkBtnClick
    end
    object CancelBtn: TcxButton
      Left = 406
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      ModalResult = 2
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
end
