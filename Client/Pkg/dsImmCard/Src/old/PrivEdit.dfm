object PrivEditForm: TPrivEditForm
  Left = 399
  Top = 275
  BorderStyle = bsSizeToolWin
  Caption = 'PrivEditForm'
  ClientHeight = 521
  ClientWidth = 462
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 484
    Width = 462
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 475
    DesignSize = (
      462
      37)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 462
      Height = 5
      Align = alTop
      Shape = bsTopLine
      ExplicitWidth = 460
    end
    object OkBtn: TcxButton
      Left = 296
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = OkBtnClick
      ExplicitLeft = 309
    end
    object CancelBtn: TcxButton
      Left = 382
      Top = 6
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      ModalResult = 2
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ExplicitLeft = 395
    end
  end
  object CompPanel: TPanel
    Left = 0
    Top = 0
    Width = 462
    Height = 484
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 475
    object Bevel2: TBevel
      Left = 2
      Top = 26
      Width = 457
      Height = 8
      Shape = bsTopLine
    end
    object AdmBl: TBevel
      Left = 2
      Top = 328
      Width = 457
      Height = 3
      Shape = bsTopLine
    end
    object Label1: TLabel
      Left = 8
      Top = 7
      Width = 78
      Height = 13
      Caption = #1042#1072#1082#1094#1080#1085#1072#1094#1080#1103' '#1086#1090':'
    end
    object InfPanel: TPanel
      Left = 3
      Top = 41
      Width = 454
      Height = 167
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 1
      object NewVariantGrid: TcxTreeList
        Left = 2
        Top = 48
        Width = 450
        Height = 117
        Align = alClient
        Bands = <
          item
          end>
        LookAndFeel.Kind = lfStandard
        LookAndFeel.NativeStyle = True
        Navigator.Buttons.CustomButtons = <>
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.ConfirmDelete = False
        OptionsBehavior.DragCollapse = False
        OptionsBehavior.DragExpand = False
        OptionsBehavior.ExpandOnDblClick = False
        OptionsBehavior.MultiSort = False
        OptionsBehavior.ShowHourGlass = False
        OptionsCustomizing.BandCustomizing = False
        OptionsCustomizing.BandHorzSizing = False
        OptionsCustomizing.BandMoving = False
        OptionsCustomizing.BandVertSizing = False
        OptionsCustomizing.ColumnHorzSizing = False
        OptionsCustomizing.ColumnMoving = False
        OptionsCustomizing.ColumnVertSizing = False
        OptionsData.CancelOnExit = False
        OptionsData.AnsiSort = True
        OptionsData.Deleting = False
        OptionsView.ScrollBars = ssVertical
        OptionsView.ShowEditButtons = ecsbFocused
        OptionsView.Buttons = False
        OptionsView.GridLines = tlglBoth
        OptionsView.ShowRoot = False
        Styles.Background = cxStyle1
        TabOrder = 0
        OnEnter = NewVariantGridEnter
        OnKeyDown = NewVariantGridKeyDown
        object InfCol: TcxTreeListColumn
          Styles.Content = cxStyle1
          Caption.AlignHorz = taCenter
          Caption.Text = #1048#1085#1092#1077#1082#1094#1080#1103
          DataBinding.ValueType = 'String'
          Options.CellEndEllipsis = False
          Options.Sizing = False
          Options.Customizing = False
          Options.Editing = False
          Options.Focusing = False
          Options.IncSearch = False
          Options.Moving = False
          Options.ShowEditButtons = eisbNever
          Options.Sorting = False
          Options.TabStop = False
          Width = 150
          Position.ColIndex = 0
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object VacVarCol: TcxTreeListColumn
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.CaseInsensitive = False
          Properties.ClearKey = 16430
          Properties.DropDownListStyle = lsFixedList
          Properties.HideSelection = False
          Properties.ImmediatePost = True
          Properties.ImmediateUpdateText = True
          Properties.Items.Strings = (
            'V1'
            'V2'
            'V3'
            'RV1/1'
            #1044#1086#1087'.')
          Properties.OnCloseUp = VacVarColPropertiesCloseUp
          Caption.AlignHorz = taCenter
          Caption.ShowEndEllipsis = False
          Caption.Text = #1042#1080#1076' '#1074#1072#1082#1094#1080#1085#1072#1094#1080#1080
          DataBinding.ValueType = 'String'
          Options.CellEndEllipsis = False
          Options.Sizing = False
          Options.Customizing = False
          Options.Moving = False
          Options.Sorting = False
          Width = 275
          Position.ColIndex = 1
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object CurrSchCol: TcxTreeListColumn
          Styles.Content = cxStyle1
          Visible = False
          Caption.AlignHorz = taCenter
          Caption.Text = #1058#1077#1082#1091#1097#1072#1103' '#1089#1093#1077#1084#1072
          DataBinding.ValueType = 'String'
          Options.CellEndEllipsis = False
          Options.Sizing = False
          Options.Customizing = False
          Options.Editing = False
          Options.Focusing = False
          Options.IncSearch = False
          Options.Moving = False
          Options.ShowEditButtons = eisbNever
          Options.Sorting = False
          Options.TabStop = False
          Width = 20
          Position.ColIndex = 2
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
      end
      object VacSchPanel: TPanel
        Left = 2
        Top = 2
        Width = 450
        Height = 46
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object SchLab: TLabel
          Left = 2
          Top = 6
          Width = 35
          Height = 13
          Caption = #1057#1093#1077#1084#1072':'
          Color = clCream
          ParentColor = False
        end
        object SchValLab: TLabel
          Left = 41
          Top = 6
          Width = 402
          Height = 13
          AutoSize = False
          Color = clCream
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object ExtSchChB: TcxCheckBox
          Left = 2
          Top = 21
          Caption = #1056#1072#1089#1096#1080#1088#1077#1085#1085#1072#1103' '#1091#1089#1090#1072#1085#1086#1074#1082#1072' '#1074#1080#1076#1086#1074' '#1074#1072#1082#1094#1080#1085#1072#1094#1080#1081
          Style.LookAndFeel.Kind = lfStandard
          Style.LookAndFeel.NativeStyle = True
          StyleDisabled.LookAndFeel.Kind = lfStandard
          StyleDisabled.LookAndFeel.NativeStyle = True
          StyleFocused.LookAndFeel.Kind = lfStandard
          StyleFocused.LookAndFeel.NativeStyle = True
          StyleHot.LookAndFeel.Kind = lfStandard
          StyleHot.LookAndFeel.NativeStyle = True
          TabOrder = 0
          OnClick = ExtSchChBClick
          Width = 255
        end
      end
    end
    object fltInfCB: TcxComboBox
      Left = 230
      Top = 3
      Properties.DropDownListStyle = lsEditFixedList
      Properties.OnChange = fltInfCBPropertiesChange
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 0
      OnEnter = fltInfCBEnter
      OnKeyDown = fltInfCBKeyDown
      Width = 221
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 2
    Top = 30
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clBtnFace
    end
  end
end
