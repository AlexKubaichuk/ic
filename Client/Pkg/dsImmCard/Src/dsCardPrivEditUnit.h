﻿//---------------------------------------------------------------------------
#ifndef dsCardPrivEditUnitH
#define dsCardPrivEditUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
//---------------------------------------------------------------------------
#include "KabCustomDS.h"
#include "dsICCardDMUnit.h"
//---------------------------------------------------------------------------
enum  TCheckPlanAction
{
  cpaNotCheck  = 0, // не отмечать в плане
  cpaCheckPlan = 1, // отметить как плановую
  cpaCheckDop  = 2  // отметить как выполненую вне плана
};
class PACKAGE TdsCardPrivEditForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *CompPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
 TBevel *AdmBl;
 TPanel *InfPanel;
 TcxTreeList *NewVariantGrid;
 TcxTreeListColumn *InfCol;
 TcxTreeListColumn *VacVarCol;
 TcxTreeListColumn *CurrSchCol;
 TPanel *VacSchPanel;
 TLabel *SchLab;
 TLabel *SchValLab;
 TcxCheckBox *ExtSchChB;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
 void __fastcall ExtSchChBClick(TObject *Sender);
 void __fastcall VacVarColPropertiesCloseUp(TObject *Sender);

private:	// User declarations
  typedef  map<int, TcxTreeListNode*> TTreeListNodeMap;
  TdsRegTemplateData * FTmplData;
  bool FSaveTmpl;
  __int64 FPlanMIBPCode;
    TdsICCardDM *FDM;
  TStringList *FEditVacVar;
    TkabCustomDataSource *FDataSrc;
    TkabCustomDataSet    *FDataSet;
    bool     FIsAppend, FCheckPlan;
//    TTagNode *Root;
    TTagNode *FClassDef,*FClassDesc;
    int      ColWidth, SubLevel;
    UnicodeString  FInsertedRecId;
//  void __fastcall PreloadExtData(TStringList *AList);

  TTreeListNodeMap InfNodes;
  TStringList *InfList;
  TStringList *OldInfList;
  TDate       FPatBirthday;
  TDate       FPlanDate;
  __int64 FUCode;//,FVacCode;
  TAnsiStrMap privSchByVariant;
  TAnsiStrMap privVariantBySch;
  TAnsiStrMap privVariantOld;
  TTagNode *FShema;
//  __int64 FPrivCode;
  TDate PlanMonth;
//  TdsRegTemplateData *FTmplData;
    TdsRegEDItem   *LastLeftCtrl;
    TdsRegEDItem   *LastCtrl;
    TcxPageControl *Group1st;
    TcxTabSheet    *gPage;
  __int64 FPlanCode;

    TdsRegEDContainer *FCtrList;
    int FLastTop,FLastHeight;

    void __fastcall AlignCtrls(TList *ACtrlList);
    bool __fastcall GetInput(TTagNode *uiTag, UnicodeString &UID);
    bool __fastcall CheckInput();
    bool __fastcall FCtrlDataChange(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource);
    bool __fastcall InThisLPUChBChange(TTagNode *ItTag, UnicodeString &Src);
  bool __fastcall LPUCBChange(TTagNode * ItTag, UnicodeString & Src);
    bool __fastcall FSetFocus(TTagNode *itxTag);
    void __fastcall CreateUnitList(TWinControl *AParent, TTagNode* ANode,int &ATop,int *ALeft,TList *GroupComp,int APIndex = 0);
    int  __fastcall SetAlign(TdsRegEDItem* AItem, int ALeft, int ATop, int AWidth, int ATabOrder);
    inline TdsRegEDItem* __fastcall RegComp(UnicodeString AUID);
    inline TdsRegEDItem* __fastcall AddRegED(UnicodeString AUID);
    bool __fastcall PriDateChange(TTagNode *ItTag, UnicodeString &Src);
    bool __fastcall CBChange(TTagNode *ItTag, UnicodeString &Src);
    UnicodeString __fastcall PriDozeVal();
    void __fastcall SetVacTypes(TStringList *AVacTypeList);
  void __fastcall FSaveVacType();
public:
    __property UnicodeString  InsertedRecId = {read=FInsertedRecId};
    __fastcall TdsCardPrivEditForm(TComponent* Owner, bool AIsAppend, __int64 AUCode, TdsICCardDM *ADM, TTagNode* ARoot, TDate APatBirthday, TkabCustomDataSource *ADataSrc, TdsRegTemplateData *ATmplData, bool ACheckPlan = false, bool ASaveTmpl = false);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsCardPrivEditForm *dsCardPrivEditForm;
//---------------------------------------------------------------------------
#endif
