﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsCardTestEditUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "msgdef.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TdsCardTestEditForm * dsCardTestEditForm;
// ---------------------------------------------------------------------------
/*
 // <extedit  name='Код пациента'                uid='017B' ref='1000' depend='017B.017B' isedit='0' fieldtype='integer'/>
 // <date     name='Дата выполнения'             uid='1031' required='1' inlist='l' default='CurrentDate'/>
 // <extedit  name='Возраст'                     uid='10B5' inlist='l' depend='1031' isedit='0' length='9'/>
 // <choiceDB name='Проба'                       uid='1032' ref='002A' required='1' inlist='l'/>
 // <text     name='Серия препарата'             uid='1033' inlist='l' cast='no' length='20'/>
 // <choice   name='Повод'                       uid='1034' required='1' inlist='e' default='0'>
 // <binary   name='Реакция проверена'           uid='103B' required='1' inlist='e' default='check'/>
 // <binary   name='Есть реакция'                uid='1041' inlist='e' invname='Реакция отрицательная'>
 // <choiceDB name='Формат реакции'              uid='10A7' ref='0038' required='1' inlist='l' depend='1032.002B'>
 // <digit    name='Значение'                    uid='103F' ref='008E' required='1' inlist='l' depend='10A7.002B' digits='5' min='2' max='32767'>
 // <choiceDB name='Формат реакции 2'            uid='0183' ref='0038' required='1' inlist='e' depend='1032.002B'>
 // <digit    name='Значение 2'                  uid='0184' inlist='e' digits='5' min='2' max='32767'>
 // <choiceDB name='Формат реакции 3'            uid='0185' ref='0038' required='1' inlist='e' depend='1032.002B'>
 // <digit    name='Значение 3'                  uid='0186' inlist='e' digits='5' min='2' max='32767'>
 // <choiceDB name='Формат реакции 4'            uid='0187' ref='0038' required='1' inlist='e' depend='1032.002B'>
 // <digit    name='Значение 4'                  uid='0188' inlist='e' digits='5' min='2' max='32767'>
 // <choiceDB name='Формат реакции 5'            uid='0189' ref='0038' required='1' inlist='e' depend='1032.002B'>
 // <digit    name='Значение 5'                  uid='018A' inlist='e' digits='5' min='2' max='32767'>
 // <extedit  name='Схема'                       uid='1093' required='1' inlist='e' isedit='0' length='9'/>
 // <choiceDB name='ЛПУ'                         uid='018B' ref='00C7' inlist='e' linecount='2'/>
 // <binary   name='Выполнена в данном ЛПУ'      uid='10A5' required='1' inlist='e' invname='Выполнена в другой организации' default='check'/>
 // <choiceDB name='Врач'                        uid='10B2' ref='00AA' inlist='e' depend='018B.00AA;018C.00AA' linecount='2'/>
 // <choiceDB name='Мед. сестра'                 uid='018D' ref='00AA' inlist='e' depend='018B.00AA;018C.00AA' linecount='2'/>
 // <choiceDB name='Источник финансирования'     uid='10B3' ref='00AB' inlist='e' linecount='2'/>
 // <choiceDB name='Доп. информация'             uid='018E' ref='00BE' inlist='e' linecount='3'/>
 // <digit    name='Предыдущее значение реакции' uid='10C1' digits='2' min='0' max='99' isedit='0'/>
 // <digit    name='Предыдущий формат реакции'   uid='10C2' digits='2' min='1' max='99' isedit='0'/>
 */
#define _LPU_       "018B"
#define _InThisLPU_ "10A5"

// ---------------------------------------------------------------------------
#define EDLEFT1  2
#define EDLEFT2  232
// ---------------------------------------------------------------------------
const int LeftMargin = 2;
const int LevelCorrection = 5;
__fastcall TdsCardTestEditForm::TdsCardTestEditForm(TComponent * Owner, bool AIsAppend, __int64 AUCode,
 TdsICCardDM * ADM, TTagNode * ARoot, TDate APatBirthday, TkabCustomDataSource * ADataSrc,
 TdsRegTemplateData * ATmplData, bool ACheckPlan, bool ASaveTmpl) : TForm(Owner)
 {
  FSaveTmpl                      = ASaveTmpl;
  TypeCB->Style->StyleController = ADM->StyleController;
  FUCode                         = AUCode;
  FDM                            = ADM;
  FDataSrc                       = ADataSrc;
  FCheckPlan                     = ACheckPlan;
  FInsertedRecId                 = "";
  FPatBirthday                   = APatBirthday;

  FPlanCode          = 0;
  FTestCode          = 0;
  FTestDate          = TDate(0);
  FTmplData          = ATmplData;
  OkBtn->Caption     = FMT(icsRegClassifEditApplyBtnCaption);
  CancelBtn->Caption = FMT(icsRegClassifEditCancelBtnCaption);
  FIsAppend          = AIsAppend;

  if (FIsAppend || FCheckPlan)
   {
    FDataSrc->DataSet->Insert();
    /*
     if (ATmplData->IsTemplate("32AF"))
     {
     FPlanCode = (__int64)ATmplData->Value["32AF"];
     // ATmplData->Erase("32AF");
     }
     if (ATmplData->IsTemplate("1032"))
     {
     FTestCode = (__int64)ATmplData->Value["1032"];
     // ATmplData->Erase("1032");
     }
     if (ATmplData->IsTemplate("1031"))
     {
     FTestDate = TDate((double)ATmplData->Value["1031"]);
     // ATmplData->Erase("1031");
     }
     */
   }
  else
   FDataSrc->DataSet->Edit();

  FClassDef  = ARoot->GetTagByUID("102F");
  FClassDesc = FClassDef->GetChildByName("description");

  FCtrList                  = new TdsRegEDContainer(this, CompPanel);
  FCtrList->UseHash         = true;
  FCtrList->DefXML          = ARoot;
  FCtrList->ReqColor        = FDM->RequiredColor;
  FCtrList->StyleController = FDM->StyleController;
  FCtrList->TemplateData    = ATmplData;
  if (FIsAppend && FCheckPlan && FTmplData)
   FCtrList->TemplateData->NoLock = true;
  FCtrList->DataPath = FDM->DataPath;

  FCtrList->DataProvider = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DefXML = ARoot;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DataSource = FDataSrc;
  FCtrList->DataProvider->OnGetCompValue = FDM->OnGetCompValue;

  FCtrList->OnDataChange         = FCtrlDataChange;
  FCtrList->OnGetExtBtnGetBitmap = FDM->OnGetExtBtnGetBitmap;
  FCtrList->OnGetFormat          = FDM->OnGetFormat;
  FCtrList->OnExtBtnClick        = FDM->OnExtBtnClick;
  FCtrList->OnKeyDown            = FormKeyDown;

  // ColWidth = FDM->ULEditWidth/2+50;

  int FGroupCount = 0;
  bool FGroupOnly = true;

  TTagNode * itNode = FClassDef->GetFirstChild();
  while (itNode)
   {
    if (!itNode->CmpName("description"))
     {
      if (itNode->CmpName("group"))
       FGroupCount++ ;
      else
       FGroupOnly = false;
     }
    itNode = itNode->GetNext();
   }

  FLastTop = 0;
  FLastHeight = 0;
  int xTop, xLeft;

  LastLeftCtrl = NULL;
  int MaxH = 0;
  TList * CntrlList = new TList;

  TTagNode * FDefNode = NULL;

  if (FGroupOnly)
   FDefNode = FClassDesc->GetNext();
  else
   FDefNode = FClassDef;
  xTop     = 5;
  xLeft    = LeftMargin;
  LastCtrl = NULL;
  CreateUnitList(CompPanel, FDefNode, xTop, & xLeft, CntrlList);
  if (LastLeftCtrl)
   {
    if ((LastLeftCtrl->Top + LastLeftCtrl->Height) <= xTop)
     MaxH = xTop;
    else
     MaxH = LastLeftCtrl->Top + LastLeftCtrl->Height;
   }
  else
   MaxH = xTop;
  ClientHeight = MaxH + 50;
  // Width = ColWidth+(!FOneCol)*ColWidth+(Width-CompPanel->Width)+LeftMargin*2;
  // FCtrList->SetOnCtrlDataChange(0);
  FClassDef->GetFirstChild();
  // FClassDef->Iterate(FSetFocus);
  delete CntrlList;
  CntrlList = NULL;
  if (FIsAppend)
   {
    if (FTmplData)
     {
      if (FPlanCode)
       {
        RegComp("1032")->SetEnable(true);
        RegComp("1031")->SetEnable(true);
        RegComp("103B")->SetEnable(true);
        RegComp("10A7")->SetEnable(true);
        RegComp("3316")->SetValue(RegComp("1031")->GetValue(""), true);
       }

      /*
       // if (FTestCode)
       // RegComp("1032")->SetValue(IntToStr(FTestCode), true);
       // RegComp("1032")->SetEnable(true);
       if (FTmplData->IsTemplate("1032"))
       RegComp("1032")->SetValue(VarToStr(FTmplData->Value["1032"]), true);
       RegComp("1032")->SetEnable(true);

       // if ((int)FTestDate)
       // RegComp("1031")->SetValue(FTestDate.FormatString("dd.mm.yyyy"), true);
       // RegComp("1031")->SetEnable(true);
       if (FTmplData->IsTemplate("1031"))
       RegComp("1031")->SetValue(TDate((double)FTmplData->Value["1031"]).FormatString("dd.mm.yyyy"), true);
       RegComp("1031")->SetEnable(true);

       // FTmplData->Value["1031"] = RegComp("1031")->GetCompValue(""); // <date 'Дата выполнения' uid='1031'
       // FTmplData->Value["1032"] = RegComp("1032")->GetCompValue("002A."); // <choiceDB 'Проба' uid='1032' ref='002A'
       // FTmplData->Value["1033"] = RegComp("1033")->GetCompValue(""); // <text 'Серия препарата' uid='1033'
       // FTmplData->Value["1034"] = RegComp("1034")->GetCompValue(""); // <choice 'Повод' uid='1034'
       // FTmplData->Value["103B"] = RegComp("103B")->GetCompValue(""); // <binary 'Реакция проверена' uid='103B'
       // FTmplData->Value["10A7"] = RegComp("10A7")->GetCompValue("0038."); // <cDB 'Форм. реак' uid='10A7' ref='0038'
       // FTmplData->Value["103F"] = RegComp("103F")->GetCompValue(""); // <digit 'Значение' uid='103F'
       // FTmplData->Value["0184"] = RegComp("0184")->GetCompValue(""); // <digit 'Значение 2' uid='0184'
       // FTmplData->Value["0186"] = RegComp("0186")->GetCompValue(""); // <digit 'Значение 3' uid='0186'
       // FTmplData->Value["0188"] = RegComp("0188")->GetCompValue(""); // <digit 'Значение 4' uid='0188'
       // FTmplData->Value["018A"] = RegComp("018A")->GetCompValue(""); // <digit 'Значение 5' uid='018A'
       // FTmplData->Value["10A5"] = RegComp("10A5")->GetCompValue(""); // <binary 'Выполнена в данном ЛПУ' uid='10A5'
       // FTmplData->Value["018B"] = RegComp("018B")->GetCompValue("00C7."); // <choiceDB 'ЛПУ' uid='018B' ref='00C7'
       // FTmplData->Value["10B2"] = RegComp("10B2")->GetCompValue("00AA."); // <choiceDB 'Врач' uid='10B2' ref='00AA'
       // FTmplData->Value["018D"] = RegComp("018D")->GetCompValue("00AA."); // <cDB 'Мед. сестра' uid='018D' ref='00AA'
       // FTmplData->Value["10B3"] = RegComp("10B3")->GetCompValue("00AB."); // <cDB 'Ист. фин.' uid='10B3' ref='00AB'
       // FTmplData->Value["018E"] = RegComp("018E")->GetCompValue("00BE."); // <cDB 'Доп. инф.' uid='018E' ref='00BE'
       if (FTmplData->IsTemplate("103B"))
       RegComp("103B")->SetValue(FTmplData->Value["103B"], true);
       RegComp("103B")->SetEnable(true);

       if (FTmplData->IsTemplate("10A7"))
       RegComp("10A7")->SetValue(FTmplData->Value["10A7"], true);
       RegComp("10A7")->SetEnable(true);
       */
      // if (FPlanCode)
      // RegComp("32AF")->SetValue(IntToStr(FPlanCode), true);
      // RegComp("32AF")->SetEnable(true);
      if (FTmplData->IsTemplate("32AF"))
       RegComp("32AF")->SetValue(VarToStr(FTmplData->Value["32AF"]), true);
      RegComp("32AF")->SetEnable(true);
     }

    bool valid;
    if (FDM->OnGetDefValues)
     FDM->OnGetDefValues(-1, FClassDef, FCtrList, valid);
    Caption = FClassDef->GetChildByName("description")->AV["insertlabel"];

    UnicodeString _ES_ = "";
    CBChange(NULL, _ES_);
   }
  else
   {
    Caption = FClassDef->GetFirstChild()->AV["editlabel"];
    UnicodeString _ES_ = "";
    CBChange(NULL, _ES_);
   }
  // добавлено 7.10.08 Евдокимов М.В.
  // поднимаем событие на передачу списка компонентов
  /* if ( ARegComp->OnAfterClassEditCreate )
   {
   ARegComp->OnAfterClassEditCreate ( (TForm*)this, FCtrList, AClassDef, RegComp->DM->quReg->Value("CODE")->AsInteger );
   } */
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardTestEditForm::FSetFocus(TTagNode * itxTag)
 {
  TControl * tmpCtrl;
  if (itxTag->GetAttrByName("uid"))
   {
    if (FCtrList->GetEDControl(itxTag->AV["uid"]))
     {
      tmpCtrl = FCtrList->EditItems[itxTag->AV["uid"]]->GetFirstEnabledControl();
      if (tmpCtrl)
       {
        ActiveControl = (TWinControl *)tmpCtrl;
        return true;
       }
     }
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardTestEditForm::CreateUnitList(TWinControl * AParent, TTagNode * ANode, int & ATop, int * ALeft,
 TList * GroupComp, int APIndex)
 {
  // TypeCB->Style->Color = DM->ReqColor;
  // if (AIsEdit)
  // {
  // if (DSStr("R1093") != "_NULL_")
  // FCurrSch = _GUI(DSStr("R1093"));
  // }
  // UnicodeString FFl = "";
  // AClsNode->Iterate(GetCardFields,FFl);
  // FFl = "<fllist>"+FFl+"</fllist>";

  // TTagNode *flNode = new TTagNode(NULL);
  // flNode->AsXML = FFl;
  // TTagNode *itNode = flNode->GetFirstChild();

  // <extedit name='Код пациента' uid='017B' ref='1000' depend='017B.017B' isedit='0' fieldtype='integer'/>
  // <date name='Дата выполнения' uid='1031' required='1' inlist='l' default='CurrentDate'/>
  // <extedit name='Возраст' uid='10B5' inlist='l' depend='1031' isedit='0' length='9'/>
  // <choiceDB name='Проба' uid='1032' ref='002A' required='1' inlist='l'/>
  // <text name='Серия препарата' uid='1033' inlist='l' cast='no' length='20'/>
  // <choice name='Повод' uid='1034' required='1' inlist='e' default='0'>
  // <binary name='Реакция проверена' uid='103B' required='1' inlist='e' default='check'/>
  // <binary name='Есть реакция' uid='1041' inlist='e' invname='Реакция отрицательная'>
  // <choiceDB name='Формат реакции' uid='10A7' ref='0038' required='1' inlist='l' depend='1032.002B'>
  // <digit name='Значение' uid='103F' ref='008E' required='1' inlist='l' depend='10A7.002B' digits='5' min='2' max='32767'>
  // <choiceDB name='Формат реакции 2' uid='0183' ref='0038' required='1' inlist='e' depend='1032.002B'>
  // <digit name='Значение 2' uid='0184' inlist='e' digits='5' min='2' max='32767'>
  // <choiceDB name='Формат реакции 3' uid='0185' ref='0038' required='1' inlist='e' depend='1032.002B'>
  // <digit name='Значение 3' uid='0186' inlist='e' digits='5' min='2' max='32767'>
  // <choiceDB name='Формат реакции 4' uid='0187' ref='0038' required='1' inlist='e' depend='1032.002B'>
  // <digit name='Значение 4' uid='0188' inlist='e' digits='5' min='2' max='32767'>
  // <choiceDB name='Формат реакции 5' uid='0189' ref='0038' required='1' inlist='e' depend='1032.002B'>
  // <digit name='Значение 5' uid='018A' inlist='e' digits='5' min='2' max='32767'>
  // <extedit name='Схема' uid='1093' required='1' inlist='e' isedit='0' length='9'/>
  // <choiceDB name='ЛПУ' uid='018B' ref='00C7' inlist='e' linecount='2'/>
  // <binary name='Выполнена в данном ЛПУ' uid='10A5' required='1' inlist='e' invname='Выполнена в другой организации' default='check'/>
  // <choiceDB name='Врач' uid='10B2' ref='00AA' inlist='e' depend='018B.00AA;018C.00AA' linecount='2'/>
  // <choiceDB name='Мед. сестра' uid='018D' ref='00AA' inlist='e' depend='018B.00AA;018C.00AA' linecount='2'/>
  // <choiceDB name='Источник финансирования' uid='10B3' ref='00AB' inlist='e' linecount='2'/>
  // <choiceDB name='Доп. информация' uid='018E' ref='00BE' inlist='e' linecount='3'/>
  // <digit name='Предыдущее значение реакции' uid='10C1' digits='2' min='0' max='99' isedit='0'/>
  // <digit name='Предыдущий формат реакции' uid='10C2' digits='2' min='1' max='99' isedit='0'/>

  TdsRegEDItem * FED;
  int FOrder = 0;
  int FReak = 1;
  // <extedit name='Код пациента' uid='017B' ref='1000' depend='017B.017B' isedit='0' fieldtype='integer'/>
  // <extedit name='Возраст' uid='10B5' inlist='l' depend='1031' isedit='0' length='9'/>
  // <extedit name='Схема' uid='1093' required='1' inlist='e' isedit='0' length='9'/>
  // <digit name='Предыдущее значение реакции' uid='10C1' digits='2' min='0' max='99' isedit='0'/>
  // <digit name='Предыдущий формат реакции' uid='10C2' digits='2' min='1' max='99' isedit='0'/>

  // <date name='Дата выполнения' uid='1031' required='1' inlist='l' default='CurrentDate'/>
  FED          = AddRegED("32D8");
  FED->Visible = false;
  FED          = AddRegED("017B");
  FED->Visible = false;
  FED->SetValue(FUCode);
  FED          = AddRegED("32AF");
  FED->Visible = false;
  FED->SetValue(FPlanCode);
  FED          = AddRegED("1156");
  FED->Visible = false;
  FED->SetValue(FPatBirthday.FormatString("dd.mm.yyyy"));
  FED          = AddRegED("3316");
  FED->Visible = false;

  FED = AddRegED("1031");
  SetAlign(FED, EDLEFT1, ATop, 200, FOrder++);
  // <choiceDB name='Проба' uid='1032' ref='002A' required='1' inlist='l'/>
  FED = AddRegED("1032");
  ATop += SetAlign(FED, EDLEFT2, ATop, 250, FOrder++);
  FED->OnDataChange = CBChange;

  FED          = AddRegED("1200");
  FED->Visible = false; // ATop += SetAlign(FED, EDLEFT2, ATop, 250, FOrder++);

  // <extedit name='Схема' uid='1093' required='1' inlist='e' isedit='0' length='9'/>
  FED          = AddRegED("1093");
  FED->Visible = false;

  // ********** Реакция проверена uid=103B          <binary name='' required='1' inlist='e' default='check'/>
  FED = AddRegED("103B");
  ATop += SetAlign(FED, EDLEFT1, ATop, 300, FOrder++);
  // <binary name='Есть реакция' uid='1041' inlist='e' invname='Реакция отрицательная'>
  // FED = AddRegED("1041"); ATop += SetAlign(FED, EDLEFT2, ATop, 300, FOrder++);

  // <choiceDB name='Формат реакции' uid='10A7' ref='0038' required='1' inlist='l' depend='1032.002B'>
  FED = AddRegED("10A7");
  if (FED->GetControl())
   ((TcxComboBox *)FED->GetControl())->Properties->Alignment->Horz = taRightJustify; /* GetFirstEnabledControl() */
  SetAlign(FED, 5, ATop, 170, FOrder++);
  FED->SetEDLeft(50);
  FED->SetLabel("Реакция "); // +IntToStr(FReak++)
  // <digit name='Значение' uid='103F' ref='008E' required='1' inlist='l' depend='10A7.002B' digits='5' min='2' max='32767'>
  FED = AddRegED("103F"); /* ATop += */
  SetAlign(FED, 175, ATop, 80, FOrder++);
  FED->SetEDLeft(0);
  // <choiceDB name='Формат реакции 2' uid='0183' ref='0038' required='1' inlist='e' depend='1032.002B'>
  // FED = AddRegED("0183");   FED->Visible = false;//      SetAlign(FED, 5, ATop, 250, FOrder++);   FED->SetEDLeft(70); FED->SetLabel("Реакция "+IntToStr(FReak++));
  // <digit name='Значение 2' uid='0184' inlist='e' digits='5' min='2' max='32767'>
  FED = AddRegED("0184"); /* ATop += */
  SetAlign(FED, 256, ATop, 80, FOrder++);
  FED->SetEDLeft(0);
  // <choiceDB name='Формат реакции 3' uid='0185' ref='0038' required='1' inlist='e' depend='1032.002B'>
  // FED = AddRegED("0185");   FED->Visible = false;//      SetAlign(FED, 5, ATop, 250, FOrder++);   FED->SetEDLeft(70); FED->SetLabel("Реакция "+IntToStr(FReak++));
  // <digit name='Значение 3' uid='0186' inlist='e' digits='5' min='2' max='32767'>
  FED = AddRegED("0186"); /* ATop += */
  SetAlign(FED, 337, ATop, 80, FOrder++);
  FED->SetEDLeft(0);
  // <choiceDB name='Формат реакции 4' uid='0187' ref='0038' required='1' inlist='e' depend='1032.002B'>
  // FED = AddRegED("0187");   FED->Visible = false;//      SetAlign(FED, 5, ATop, 250, FOrder++);   FED->SetEDLeft(70); FED->SetLabel("Реакция "+IntToStr(FReak++));
  // <digit name='Значение 4' uid='0188' inlist='e' digits='5' min='2' max='32767'>
  FED = AddRegED("0188"); /* ATop += */
  SetAlign(FED, 418, ATop, 80, FOrder++);
  FED->SetEDLeft(0);
  // <choiceDB name='Формат реакции 5' uid='0189' ref='0038' required='1' inlist='e' depend='1032.002B'>
  // FED = AddRegED("0189");   FED->Visible = false;//     SetAlign(FED, 5, ATop, 250, FOrder++);   FED->SetEDLeft(70); FED->SetLabel("Реакция "+IntToStr(FReak++));
  // <digit name='Значение 5' uid='018A' inlist='e' digits='5' min='2' max='32767'>
  FED = AddRegED("018A");
  ATop += SetAlign(FED, 499, ATop, 80, FOrder++);
  FED->SetEDLeft(0);
  FED->Visible = false;

  TypeCB->Top      = ATop;
  TypeCB->Left     = 80;
  TypeCB->TabOrder = FOrder++ ;
  TypeLab->Top     = ATop;
  TypeLab->Left    = 7;
  TypeCB->Height + 4;
  // ATop += TypeCB->Height + 4;

  // <text name='Серия препарата' uid='1033' inlist='l' cast='no' length='20'/>
  FED = AddRegED("1033");
  ATop += SetAlign(FED, EDLEFT2, ATop, 250, FOrder++);

  // <choice name='Повод' uid='1034' required='1' inlist='e' default='0'>
  FED = AddRegED("1034");
  ATop += SetAlign(FED, EDLEFT1, ATop, 200, FOrder++);
  FED->OnDataChange = CBChange;

  // <choiceDB name='ЛПУ' uid='018B' ref='00C7' inlist='e' linecount='2'/>
  FED = AddRegED(_InThisLPU_);
  SetAlign(FED, EDLEFT1, ATop, 250, FOrder++);
  RegComp(_InThisLPU_)->OnDataChange = InThisLPUChBChange;
  // <binary name='Выполнена в данном ЛПУ' uid='10A5' required='yes' inlist='list_ext' invname='Выполнена в другой организации' default='check'/>
  FED = AddRegED(_LPU_);
  ATop += SetAlign(FED, 290, ATop, 250, FOrder++);
  // FED->SetEDLeft(73);
  RegComp(_LPU_)->OnDataChange = LPUCBChange;

  // <choiceDB name='Врач' uid='10B2' ref='00AA' inlist='e' depend='018B.00AA;018C.00AA' linecount='2'/>
  FED = AddRegED("10B2");
  SetAlign(FED, 5, ATop, 250, FOrder++);
  FED->SetEDLeft(68);
  FED->SetLabel(FED->DefNode->AV["name"]);

  // <choiceDB name='Мед. сестра' uid='018D' ref='00AA' inlist='e' depend='018B.00AA;018C.00AA' linecount='2'/>
  FED = AddRegED("018D");
  ATop += SetAlign(FED, 290, ATop, 245, FOrder++);
  FED->SetLabel(FED->DefNode->AV["name"]);
  // <choiceDB name='Источник финансирования' uid='10B3' ref='00AB' inlist='e' linecount='2'/>
  FED = AddRegED("10B3");
  ATop += SetAlign(FED, EDLEFT1, ATop, 300, FOrder++);
  ((TWinControl *)FED->GetControl())->Width = 290;
  FED->SetEDLeft(155);
  // <choiceDB name='Доп. информация' uid='018E' ref='00BE' inlist='e' linecount='3'/>
  FED = AddRegED("018E");
  ATop += SetAlign(FED, EDLEFT1, ATop, 300, FOrder++);
  ((TWinControl *)FED->GetControl())->Width = 290;
  FED->SetEDLeft(155);

  Height = ATop + 60;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardTestEditForm::AlignCtrls(TList * ACtrlList)
 {
  int LabR, EDL;
  int xLabR, xEDL;
  LabR = 0;
  EDL  = 9999;
  TdsRegEDItem * tmpED;
  for (int i = 0; i < ACtrlList->Count; i++)
   {
    // определяем max правую границу меток контролов
    // и min левую самих контролов
    tmpED = ((TdsRegEDItem *)ACtrlList->Items[i]);
    tmpED->GetEDLeft(& xLabR, & xEDL);
    if (xLabR != -1)
     LabR = max(xLabR, LabR);
    if (xEDL != -1)
     EDL = min(xEDL, EDL);
   }
  if ((LabR + 2) < EDL)
   {
    // max правая граница меток контролов
    // и min левая самих контролов не пересекаются
    // выравниваем контролы по min левой границе самих контролов
    for (int i = 0; i < ACtrlList->Count; i++)
     {
      tmpED = ((TdsRegEDItem *)ACtrlList->Items[i]);
      tmpED->SetEDLeft(EDL);
     }
   }
  else
   {
    // иначе выравниваем по "max правая граница меток контролов" + 2
    for (int i = 0; i < ACtrlList->Count; i++)
     {
      tmpED = ((TdsRegEDItem *)ACtrlList->Items[i]);
      tmpED->SetEDLeft(LabR + 2);
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardTestEditForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_RETURN)
   {
    ActiveControl = OkBtn;
    OkBtnClick(this);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardTestEditForm::OkBtnClick(TObject * Sender)
 {
  TWinControl * tmpCtrl = ActiveControl;
  ActiveControl = OkBtn;
  ActiveControl = tmpCtrl;
  if (CheckInput())
   { // запрос на проверку данных
    try
     {
      UnicodeString PRC = FDataSrc->DataSet->Post(FInsertedRecId, true);
      if (!SameText(PRC, "ok"))
       _MSG_ERR(PRC, "Ошибка");
      else
       {
        try
         {
          if (FTmplData && FSaveTmpl)
           {
            FTmplData->Value["1031"] = TDate(RegComp("1031")->GetValue("")); // <date 'Дата выполнения' uid='1031'
            FTmplData->Value["1032"] = RegComp("1032")->GetValue("002A."); // <choiceDB 'Проба' uid='1032' ref='002A'
            if (RegComp("1033")->GetValue("").Length())
             FTmplData->Value["1033"] = RegComp("1033")->GetValue(""); // <text 'Серия препарата' uid='1033'
            FTmplData->Value["1034"] = RegComp("1034")->GetCompValue(""); // <choice 'Повод' uid='1034'
            FTmplData->Value["103B"] = RegComp("103B")->GetCompValue(""); // <binary 'Реакция проверена' uid='103B'
            if (RegComp("10A7")->GetValue("0038.").Length())
             FTmplData->Value["10A7"] = RegComp("10A7")->GetValue("0038."); // <cDB 'Форм. реак' uid='10A7' ref='0038'
            if (RegComp("103F")->GetValue("").Length())
             FTmplData->Value["103F"] = RegComp("103F")->GetValue(""); // <digit 'Значение' uid='103F'
            if (RegComp("0184")->GetValue("").Length())
             FTmplData->Value["0184"] = RegComp("0184")->GetValue(""); // <digit 'Значение 2' uid='0184'
            if (RegComp("0186")->GetValue("").Length())
             FTmplData->Value["0186"] = RegComp("0186")->GetValue(""); // <digit 'Значение 3' uid='0186'
            if (RegComp("0188")->GetValue("").Length())
             FTmplData->Value["0188"] = RegComp("0188")->GetValue(""); // <digit 'Значение 4' uid='0188'
            if (RegComp("018A")->GetValue("").Length())
             FTmplData->Value["018A"] = RegComp("018A")->GetValue(""); // <digit 'Значение 5' uid='018A'
            FTmplData->Value["10A5"] = RegComp("10A5")->GetCompValue(""); // <binary 'Выполнена в данном ЛПУ' uid='10A5'
            if (RegComp("018B")->GetValue("00C7.").Length())
             FTmplData->Value["018B"] = RegComp("018B")->GetValue("00C7."); // <choiceDB 'ЛПУ' uid='018B' ref='00C7'
            if (RegComp("10B2")->GetValue("00AA.").Length())
             FTmplData->Value["10B2"] = RegComp("10B2")->GetValue("00AA."); // <choiceDB 'Врач' uid='10B2' ref='00AA'
            if (RegComp("018D")->GetValue("00AA.").Length())
             FTmplData->Value["018D"] = RegComp("018D")->GetValue("00AA.");
            // <cDB 'Мед. сестра' uid='018D' ref='00AA'
            if (RegComp("10B3")->GetValue("00AB.").Length())
             FTmplData->Value["10B3"] = RegComp("10B3")->GetValue("00AB."); // <cDB 'Ист. фин.' uid='10B3' ref='00AB'
            if (RegComp("018E")->GetValue("00BE.").Length())
             FTmplData->Value["018E"] = RegComp("018E")->GetValue("00BE."); // <cDB 'Доп. инф.' uid='018E' ref='00BE'
           }
         }
        __finally
         {
         }
        ModalResult = mrOk;
       }
     }
    catch (EkabCustomDataSetError & E)
     {
      _MSG_ERR(E.Message, "Ошибка");
     }
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardTestEditForm::CheckInput()
 {
  bool RC = false;
  TDate FProbDate;
  /*
   TDateTime PBirthDay;
   DM->qtExec("Select R0031 From casebook1 Where CODE="+IntToStr(FUCode));
   if (DM->quFree->RecordCount)
   PBirthDay = quFNDate("R0031");
   else
   {
   MessageBox(Handle,"Ошибка определения даты рождения пациента.","Ошибка",0);
   return false;
   }
   if (!RegComp("1031")->CheckReqValue()) return false;
   */
  try
   {
    if (FIsAppend)
     FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
    else
     FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
    UnicodeString xValid = "";
    FClassDef->Iterate(GetInput, xValid);
    if (!xValid.Length())
     {
      RC = false;
      if (!TryStrToDate(RegComp("1031")->GetValue("").c_str(), FProbDate))
       _MSG_ERR("Введите корректную дату выполнения пробы", "Ошибка");
      else if (FProbDate > Date())
       _MSG_ERR("Дата выполнения пробы не может быть больше текущей", "Ошибка");
      else if (FProbDate < FPatBirthday)
       _MSG_ERR("Дата выполнения пробы не может быть меньше даты рождения пациента.", "Ошибка");
      else if (TypeCB->ItemIndex < 0)
       _MSG_ERR("Укажите вид пробы.", "Ошибка");
      else
       RC = true;
      if (RC && FDM->OnExtValidate)
       {
        // RC = false;
        if (FIsAppend)
         FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
        else
         FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardTestEditForm::GetInput(TTagNode * itTag, UnicodeString & UID)
 {
  if (itTag)
   {
    if (FCtrList->GetEDControl(itTag->AV["uid"]))
     if (!FCtrList->EditItems[itTag->AV["uid"]]->CheckReqValue())
      {
       UID = "no";
       return true;
      }
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardTestEditForm::FormDestroy(TObject * Sender)
 {
  FCtrList->ClearOnCtrlDataChange(0);
  delete FCtrList->DataProvider;
  delete FCtrList;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardTestEditForm::FCtrlDataChange(TTagNode * ItTag, UnicodeString & Src,
 TkabCustomDataSource * ASource)
 {
  bool RC = false;
  if (FDM->OnCtrlDataChange)
   {
    RC = FDM->OnCtrlDataChange(ItTag, Src, ASource, FIsAppend, FCtrList, UIDInt(FClassDef->AV["uid"]));
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsCardTestEditForm::SetAlign(TdsRegEDItem * AItem, int ALeft, int ATop, int AWidth, int ATabOrder)
 {
  AItem->Left  = ALeft;
  AItem->Width = AWidth;
  AItem->Top   = ATop;
  AItem->Anchors.Clear();
  AItem->Anchors << akLeft << akRight << akTop;
  AItem->TabOrder = ATabOrder;
  return AItem->Height + 4;
 }
// ---------------------------------------------------------------------------
inline TdsRegEDItem * __fastcall TdsCardTestEditForm::RegComp(UnicodeString AUID)
 {
  return FCtrList->EditItems[AUID];
 }
// ---------------------------------------------------------------------------
inline TdsRegEDItem * __fastcall TdsCardTestEditForm::AddRegED(UnicodeString AUID)
 {
  return FCtrList->AddEditItem(FClassDef->GetTagByUID(AUID), FIsAppend);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardTestEditForm::InThisLPUChBChange(TTagNode * ItTag, UnicodeString & Src)
 {
  if (FDM->LPUCode)
   {
    RegComp(_LPU_)->OnDataChange = NULL;
    try
     {
      if (RegComp(_InThisLPU_)->GetValue("").ToIntDef(0))
       {
        RegComp(_LPU_)->SetEnable(false);
        RegComp(_LPU_)->SetValue(IntToStr(FDM->LPUCode), false);
       }
      else
       {
        RegComp(_LPU_)->SetEnable(false);
        RegComp(_LPU_)->SetEnable(true);
       }
     }
    __finally
     {
      RegComp(_LPU_)->OnDataChange = LPUCBChange;
     }
   }
  return true;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardTestEditForm::LPUCBChange(TTagNode * ItTag, UnicodeString & Src)
 {
  if (FDM->LPUCode)
   {
    RegComp(_InThisLPU_)->OnDataChange = NULL;
    try
     {
      if (RegComp(_LPU_)->GetValue("").ToIntDef(0) == FDM->LPUCode)
       RegComp(_InThisLPU_)->SetValue("1", true);
      else
       RegComp(_InThisLPU_)->SetValue("0", true);
     }
    __finally
     {
      RegComp(_InThisLPU_)->OnDataChange = InThisLPUChBChange;
     }
   }
  return true;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardTestEditForm::CBChange(TTagNode * ItTag, UnicodeString & Src)
 {
  bool RC = false;
  try
   {
    UnicodeString xProb, xOldProbVal, xOldProbFormat, xProbDate, FSch;
    xProb = RegComp("1032")->GetValue("002A.");
    FSch  = RegComp("1093")->GetValue("");
    TypeCB->Properties->Items->Clear();
    int FInfCode = -1;

    TIntListMap::iterator FRC = FDM->ProbInfList.find(xProb.ToIntDef(-1));
    if (FRC != FDM->ProbInfList.end())
     {
      if (FRC->second->size())
       FInfCode = FRC->second->begin()->first;
     }
    if (FInfCode != -1)
     RegComp("1200")->SetValue(IntToStr(FInfCode));

    int FValIdx = -1;
    if ((RegComp("1034")->GetValue("").ToIntDef(0) != 1) && (FInfCode != -1))
     {
      // DM->qtExec("Select R1079 From CARD_1076 Where UCODE="+IntToStr(FUCode)+"and  R1077="+FtmpInfCode,false);
      // if (!FCurrSch.Length())
      // FCurrSch = _GUI(quFNStr("R1079"));
      TTagNode * tmp; // = DM->xmlTestDef->GetTagByUID(FCurrSch);
      // if (tmp)
      // {
      /*
       RegComp("1093")->SetValue(FSchLineNode->GetParent("schema")->AV["uid"]+"."+FSchLineNode->AV["uid"]);
       */
      TTagNodeMap SchList;
      FDM->GetSchemeList(FInfCode, SchList, false);
      for (TTagNodeMap::iterator i = SchList.begin(); i != SchList.end(); i++)
       {
        if (i->second->CmpAV("ref", xProb))
         {
          tmp = i->second->GetFirstChild();
          while (tmp)
           {
            if (tmp->CmpName("line"))
             {
              if (!FIsAppend && (FValIdx == -1) && FSch.Length() && (FSch.UpperCase() != "_NULL_"))
               {
                if (tmp->CmpAV("uid", _UID(FSch)))
                 FValIdx = TypeCB->Properties->Items->Count;
               }
              TypeCB->Properties->Items->AddObject(i->second->AV["name"] + "." + tmp->AV["name"],
               (TObject *)UIDInt(tmp->AV["uid"]));
             }
            tmp = tmp->GetNext();
           }
         }
       }
      // }
     }
    else
     {
      // FCurrSch = "";
      TypeCB->Properties->Items->AddObject("Доп.", (TObject *)0);
     }
    if (FValIdx != -1)
     TypeCB->ItemIndex = FValIdx;
    else if (TypeCB->Properties->Items->Count == 1)
     TypeCB->ItemIndex = 0;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardTestEditForm::TypeCBPropertiesChange(TObject * Sender)
 {
  // сохраняем схему
  RegComp("1093")->SetValue("_NULL_");
  if (TypeCB->ItemIndex >= 0)
   {
    UnicodeString FSch = UIDStr((int)TypeCB->Properties->Items->Objects[TypeCB->ItemIndex]);
    TTagNode * FSchLineNode = FDM->TestSchDef->GetTagByUID(FSch);
    if (FSchLineNode)
     {
      if (FSchLineNode->CmpName("line"))
       RegComp("1093")->SetValue(FSchLineNode->GetParent("schema")->AV["uid"] + "." + FSchLineNode->AV["uid"]);
     }
   }
 }
// ---------------------------------------------------------------------------
