﻿//---------------------------------------------------------------------------
#ifndef dsICCardDMUnitH
#define dsICCardDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Datasnap.DSClientRest.hpp>
//#include <IPPeerClient.hpp>
//---------------------------------------------------------------------------
#include "dsRegED.h"
#include "dsRegConstDef.h"
//#include "ICSClassData.h"
//#include "ServerClientClasses.h"
#include "dsICCardTypeDef.h"
#include "ExtUtils.h"
//---------------------------------------------------------------------------
/*#include <Classes.hpp>
#include <DB.hpp>
#include <DBCtrls.hpp>
#include <Registry.hpp>
#include <ADODB.hpp>
#include "FIBDatabase.hpp"
#include "FIBDataSet.hpp"
#include "FIBQuery.hpp"
#include "pFIBDatabase.hpp"
#include "pFIBDataSet.hpp"
#include "pFIBQuery.hpp"
//---------------------------------------------------------------------------
#include "RegED.h"
#include "icsRegConstDef.h"
#include "ICSClassData.h"  */
//---------------------------------------------------------------------------
//namespace IcsReg
//{
//---------------------------------------------------------------------------
typedef map<int,int> TIntMap;
typedef map<int,TIntMap*> TIntListMap;
//---------------------------------------------------------------------------
class PACKAGE TdsICCardDM : public TDataModule
{
__published:	// IDE-managed Components
        void __fastcall DataModuleCreate(TObject *Sender);
        void __fastcall DataModuleDestroy(TObject *Sender);

private:
  int           FDefaultClassWidth,FULEditWidth;
  bool          FFullEdit;
  bool          FCardEditable;
  UnicodeString FClassShowExpanded;
//  UnicodeString FXMLPath;
  UnicodeString       FDataPath;


  __int64 FExtLPUCode;

  TStringList    *FExtSearchVacData;
  TColor         FRequiredColor;

  UnicodeString FRegistryKey;
  TTagNode      *FRegDefNode;
  TTagNode      *FClassDefNode;
//  TTagNode      *FVacSchDef;

  TTagNode *FCardVacNode;
  TTagNode *FCardTestNode;
  TTagNode *FCardCheckNode;
  TTagNode *FCardCancelNode;

  TkabCustomDataSource *FCardVacDataSrc;
  TkabCustomDataSource *FCardTestDataSrc;
  TkabCustomDataSource *FCardCheckDataSrc;
  TkabCustomDataSource *FCardCancelDataSrc;

  TTagNode* __fastcall FGetVacSchDef();
  TTagNode* __fastcall FGetTestSchDef();

  TAxeXMLContainer *FXMLList;

  TGetCompValue         FOnGetCompValue;
  TExtBtnGetBitmapEvent FOnGetExtBtnGetBitmap;
  TRegGetFormatEvent    FOnGetFormat;
  TExtBtnClick          FExtBtnClick;
  TExtValidateData      FOnGetDefValues;
  TExtValidateData      FOnExtValidate;
  TExtBtnClick          FOnCtrlDataChange;

  TExtEditData          FExtInsertData;
  TExtEditData          FExtEditData;

  TClassEditEvent       FOnClassAfterInsert;
  TClassEditEvent       FOnClassAfterEdit;
  TClassEditEvent       FOnClassAfterDelete;
  TdsGetPlanVacVarEvent FOnGetPlanVacVar;

  TUnitEvent            FOnOpenCard;
  TGetReportListEvent   FGetQuickFilterList;
  TGetReportListEvent   FReportList;
  TUnitEvent            FPrintReportClick;
  TcxEditStyleController *FStyleController;

  TdsGetClassXMLEvent FOnGetClassXML;
  TdsGetCountEvent    FOnGetCount;
  TdsGetIdListEvent   FOnGetIdList;
  TdsGetValByIdEvent  FOnGetValById;
  TdsGetValById10Event  FOnGetValById10;
  TdsFindEvent        FOnFind;
  TdsInsertDataEvent  FOnInsertData;
  TdsEditDataEvent    FOnEditData;
  TdsDeleteDataEvent  FOnDeleteData;
  TdsGetF63Event      FOnGetF63;
  TdsGetVacDefDozeEvent FOnGetVacDefDoze;
  TdsICCardGetUnitDataEvent FOnGetUnitData;
  TdsCreateUnitPlanEvent FOnCreatePlan;
  TdsHTMLPreviewEvent FOnHTMLPreview;
  TdsGetUnitSettingEvent FOnGetPatSetting;
  TdsSetUnitSettingEvent FOnSetPatSetting;
  TdsGetUnitSettingEvent FOnGetPrivVar;
  TdsGetUnitSettingEvent FOnGetOtvodInf;
  TdsGetListByIdEvent FOnGetListById;

  TJSONObject* __fastcall kabCDSGetListById(UnicodeString AId);
  void         __fastcall FLoadMapData(TJSONObject *AData, TAnsiStrMap &AMap/*, TAnsiStrMap &AMapCode*/);
  void         __fastcall FLoadDependMapData(TJSONObject *AData, TIntListMap &AMap, TIntListMap &AMapCode);

  void __fastcall PreloadExtData();
//  TdsICClassClient   *FAppClient;

  //    TClassHashTable *FClassHash;
  TTagNode* __fastcall FGetClassDef();
//  TTagNode* __fastcall FGetRegDefNode();

public:
//  TICSClassData *FClassData;
  __fastcall TdsICCardDM(TComponent* Owner);

  TAnsiStrMap VacList;
//    TAnsiStrMap VacListCode;
    TAnsiStrMap ProbList;
//    TAnsiStrMap ProbListCode;
    TAnsiStrMap InfList;
//    TAnsiStrMap InfListCode;
    TIntListMap InfVacList;
    TIntListMap InfProbList;
    TIntListMap VacInfList;
  TIntListMap ProbInfList;

  UnicodeString __fastcall FGetInfName(UnicodeString ACode);
  UnicodeString __fastcall FGetVacName(UnicodeString ACode);
  UnicodeString __fastcall FGetTestName(UnicodeString ACode);

  UnicodeString   DTFormats[3];
  UnicodeString __fastcall NewGUID();
  void __fastcall PreloadData();
  void __fastcall DocPreview(UnicodeString AData);

  __property TAxeXMLContainer *XMLList = {read=FXMLList, write=FXMLList};
  void __fastcall OnGetXML(UnicodeString AId, TTagNode *& ADef);

  UnicodeString __fastcall kabCDSGetClassXML(UnicodeString AId);
  __int64       __fastcall kabCDSGetCount(UnicodeString AId, UnicodeString ATTH, UnicodeString AFilterParam);
  TJSONObject*  __fastcall kabCDSGetCodes(UnicodeString AId, int AMax, UnicodeString ATTH, UnicodeString AFilterParam);
  TJSONObject*  __fastcall kabCDSGetValById(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  __fastcall kabCDSGetValById10(UnicodeString AId, UnicodeString ARecId);
  UnicodeString __fastcall kabCDSInsert(UnicodeString AId, TJSONObject* AValues, UnicodeString &ARecId);
  UnicodeString __fastcall kabCDSEdit(UnicodeString AId, TJSONObject* AValues, UnicodeString &ARecId);
  UnicodeString __fastcall kabCDSDelete(UnicodeString AId, UnicodeString ARecId);
  UnicodeString __fastcall kabCDSGetF63(UnicodeString AId);

  bool __fastcall PlanPrivInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData *ATmplData, bool ASaveTmpl);
  bool __fastcall PlanTestInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData *ATmplData, bool ASaveTmpl);
  bool __fastcall PlanCheckInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData *ATmplData, bool ASaveTmpl);
  bool __fastcall PlanCancelInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData *ATmplData, bool ASaveTmpl);

  UnicodeString __fastcall GetVacDefDoze(TDate APrivDate, TDate APatBirthday, UnicodeString AVacCode);
  UnicodeString __fastcall GetVacSch(TStringList *AInfList, UnicodeString AVacCode, __int64 AUCode, UnicodeString &AVacSch, UnicodeString &AVacSchTur);
  UnicodeString __fastcall AgeStr(TDateTime ACurDate, TDateTime ABirthDay);
  UnicodeString __fastcall SetDay(int AYear, int AMonth, int ADay );
  UnicodeString __fastcall GetSchName(UnicodeString ASchId, bool AVacSch);

  void __fastcall GetSchemeList(int AInfCode, TTagNodeMap &ASchList, bool AVacSch);
  void __fastcall SavePatSetting(__int64 AUCode, UnicodeString AData);
  UnicodeString __fastcall GetPatSetting(__int64 AUCode);
  UnicodeString __fastcall GetPrivVar(__int64 APrivCode);
  UnicodeString __fastcall GetOtvodInf(__int64 AOtvodCode);
  int __fastcall InfInPlan(TTagNode *AComRoot, TTagNode *APersRoot, UnicodeString AInf);
  UnicodeString __fastcall GetPlanVacVar(UnicodeString AUCode, TDate ADate);

  __property int DefaultClassWidth = {read=FDefaultClassWidth, write=FDefaultClassWidth};
  __property int ULEditWidth = {read=FULEditWidth, write=FULEditWidth};

  __property bool FullEdit = {read=FFullEdit, write=FFullEdit};
  __property UnicodeString RegistryKey = {read=FRegistryKey, write=FRegistryKey};
  __property TTagNode* ClassDef = {read=FGetClassDef};
  __property TTagNode* RegDef = {read=FRegDefNode, write = FRegDefNode};
  __property TTagNode* VacSchDef = {read=FGetVacSchDef};
  __property TTagNode* TestSchDef = {read=FGetTestSchDef};

  void __fastcall GetTestList(TStrings *AList);

//  __property TdsICClassClient   *Connection = {read=FAppClient, write=FAppClient};

  __property bool CardEditable = {read=FCardEditable, write=FCardEditable};
  __property UnicodeString ClassShowExpanded = {read=FClassShowExpanded, write=FClassShowExpanded};

  __property TcxEditStyleController *StyleController = {read=FStyleController,write=FStyleController};
  __property TColor RequiredColor                    = {read=FRequiredColor,  write=FRequiredColor};

  __property TGetCompValue         OnGetCompValue       = {read=FOnGetCompValue, write=FOnGetCompValue};
  __property TExtBtnGetBitmapEvent OnGetExtBtnGetBitmap = {read=FOnGetExtBtnGetBitmap, write=FOnGetExtBtnGetBitmap};
  __property TRegGetFormatEvent    OnGetFormat          = {read=FOnGetFormat, write=FOnGetFormat};
  __property TExtBtnClick          OnExtBtnClick        = {read=FExtBtnClick, write=FExtBtnClick};
  __property TExtValidateData      OnGetDefValues       = {read=FOnGetDefValues, write=FOnGetDefValues};
  __property TExtValidateData      OnExtValidate        = {read=FOnExtValidate, write=FOnExtValidate};
  __property TExtBtnClick          OnCtrlDataChange     = {read=FOnCtrlDataChange, write=FOnCtrlDataChange};

  __property TExtEditData          OnExtInsertData      = {read=FExtInsertData, write=FExtInsertData};
  __property TExtEditData          OnExtEditData        = {read=FExtEditData, write=FExtEditData};

  __property TClassEditEvent OnClassAfterInsert = {read=FOnClassAfterInsert,write=FOnClassAfterInsert};
  __property TClassEditEvent OnClassAfterEdit   = {read=FOnClassAfterEdit,write=FOnClassAfterEdit};
  __property TClassEditEvent OnClassAfterDelete = {read=FOnClassAfterDelete,write=FOnClassAfterDelete};


  __property TUnitEvent          OnOpenCard         = {read=FOnOpenCard,write=FOnOpenCard};
  __property TGetReportListEvent OnQuickFilterList  = {read=FGetQuickFilterList, write=FGetQuickFilterList};
  __property TGetReportListEvent OnReportList       = {read=FReportList, write=FReportList};
  __property TUnitEvent          OnPrintReportClick = {read=FPrintReportClick, write=FPrintReportClick};

  __property TdsGetClassXMLEvent OnGetClassXML  = {read=FOnGetClassXML,write=FOnGetClassXML};
  __property TdsGetCountEvent OnGetCount  = {read=FOnGetCount,write=FOnGetCount};
  __property TdsGetIdListEvent OnGetIdList  = {read=FOnGetIdList,write=FOnGetIdList};
  __property TdsGetValByIdEvent OnGetValById  = {read=FOnGetValById,write=FOnGetValById};
  __property TdsGetValById10Event OnGetValById10  = {read=FOnGetValById10,write=FOnGetValById10};
  __property TdsFindEvent OnFind  = {read=FOnFind,write=FOnFind};
  __property TdsInsertDataEvent OnInsertData  = {read=FOnInsertData,write=FOnInsertData};
  __property TdsEditDataEvent OnEditData  = {read=FOnEditData,write=FOnEditData};
  __property TdsDeleteDataEvent OnDeleteData  = {read=FOnDeleteData,write=FOnDeleteData};
  __property TdsGetF63Event      OnGetF63  = {read=FOnGetF63,write=FOnGetF63};
  __property TdsGetVacDefDozeEvent      OnGetVacDefDoze  = {read=FOnGetVacDefDoze,write=FOnGetVacDefDoze};

  __property TdsGetListByIdEvent      OnGetListById  = {read=FOnGetListById,write=FOnGetListById};

  __property TdsGetUnitSettingEvent      OnGetPatSetting  = {read=FOnGetPatSetting,write=FOnGetPatSetting};
  __property TdsSetUnitSettingEvent      OnSetPatSetting  = {read=FOnSetPatSetting,write=FOnSetPatSetting};

  __property TdsGetUnitSettingEvent      OnGetPrivVar     = {read=FOnGetPrivVar,write=FOnGetPrivVar};
  __property TdsGetUnitSettingEvent      OnGetOtvodInf    = {read=FOnGetOtvodInf,write=FOnGetOtvodInf};

  __property __int64 LPUCode = {read=FExtLPUCode, write=FExtLPUCode};
  __property TdsICCardGetUnitDataEvent OnGetUnitData = {read=FOnGetUnitData, write=FOnGetUnitData};
  __property TdsCreateUnitPlanEvent      OnCreatePlan = {read=FOnCreatePlan, write=FOnCreatePlan};

  __property TdsHTMLPreviewEvent      OnHTMLPreview = {read=FOnHTMLPreview, write=FOnHTMLPreview};

  __property TStringList* ExtSearchVacData = {read=FExtSearchVacData};

  __property UnicodeString InfName[UnicodeString ACode] = {read=FGetInfName};
  __property UnicodeString VacName[UnicodeString ACode] = {read=FGetVacName};
  __property UnicodeString TestName[UnicodeString ACode] = {read=FGetTestName};
//  __property UnicodeString XMLPath = {read=FXMLPath, write=FXMLPath};
  __property UnicodeString DataPath = {read=FDataPath, write=FDataPath};
  __property TdsGetPlanVacVarEvent OnGetPlanVacVar = {read=FOnGetPlanVacVar, write=FOnGetPlanVacVar};
};
//}; // end of namespace DataExchange
//using namespace IcsReg;
//---------------------------------------------------------------------------
extern PACKAGE TdsICCardDM *dsICCardDM;
//---------------------------------------------------------------------------
#endif

