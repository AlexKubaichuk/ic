﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsCardPrivEditUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "msgdef.h"
#include "dsRegEdChoiceDB.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxCheckBox"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma resource "*.dfm"
TdsCardPrivEditForm * dsCardPrivEditForm;
// ---------------------------------------------------------------------------
#define _EDLEFT  2
#define _EDLEFT2 232
#define _InThisLPU_ "101F"
#define _INFCODE_   "1075"
#define _IsTur_     "10AE"
#define _VacDATE_   "1024"
#define _VacCODE_   "1020"
#define _VacTYPE_   "1021"
#define _Doza_      "1022"
#define _SER_       "1023"
#define _RCheck_    "1025"
#define _RComm_     "1026"
#define _RLoc_      "1028"
#define _RType_     "10A8"
#define _RVal_      "1027"
#define _LPU_       "017F"
// ---------------------------------------------------------------------------
/*
 <extedit  'Код пациента'            uid='017A' ref='1000' depend='017A.017A' isedit='0' fieldtype='integer'/>
 <date     'Дата выполнения'         uid='1024' required='1' inlist='l' default='CurrentDate'/>
 <extedit  'Возраст'                 uid='10B4' inlist='l' depend='1024' isedit='0' length='9'/>
 <choiceDB 'МИБП'                    uid='1020' ref='0035' required='1' inlist='l' depend='1075.002D'/>
 <text     'Вид'                     uid='1021' required='1' inlist='l' cast='up' fixedvar='1' length='10'/>
 <choiceDB 'Вакцинация от'           uid='1075' ref='003A' required='1' inlist='l' depend='1020.002D'/>
 <digit    'Доза'                    uid='1022' inlist='e' digits='4' decimal='3' min='0' max='9' default='006A'/>
 <text     'Серия'                   uid='1023' inlist='e' cast='no' length='20'/>
 <binary   'Реакция проверена'       uid='1025' required='1' inlist='e'/>
 <binary   'Есть общая реакция'      uid='1026' required='1' inlist='e'>
 <binary   'Есть местная реакция'    uid='1028' required='1' inlist='e'>
 <choiceDB 'Формат реакции'          uid='10A8' ref='0038' required='1' inlist='e' depend='1020.0039'>
 <digit    'Значение реакции'        uid='1027' ref='008E' required='1' inlist='e' depend='10A8.0039' digits='5' min='0' max='9999'>
 <binary   'Туровая прививка'        uid='10AE' required='1' inlist='e'/>
 <extedit  'Схема'                   uid='1092' required='1' inlist='e' isedit='0' linecount='2' length='9'/>
 <choice   'Тур закрыт'              uid='10AF' default='0'>
 <choiceDB 'ЛПУ'                     uid='017F' ref='00C7' inlist='e' linecount='2'/>
 <binary   'Выполнена в данном ЛПУ'  uid='101F' required='1' inlist='e' inv'Выполнена в другой организации' default='check'/>
 <choiceDB 'Врач'                    uid='10B0' ref='00AA' inlist='e' depend='017F.00AA;0180.00AA' linecount='2'/>
 <choiceDB 'Мед. сестра'             uid='0181' ref='00AA' inlist='e' depend='017F.00AA;0180.00AA' linecount='2'/>
 <choiceDB 'Источник финансирования' uid='10B1' ref='00AB' inlist='e' linecount='2'/>
 <choiceDB 'Доп. информация'         uid='0182' ref='00BE' inlist='e' linecount='3'/>
 */
const int LeftMargin = 2;
const int LevelCorrection = 5;
__fastcall TdsCardPrivEditForm::TdsCardPrivEditForm(TComponent * Owner, bool AIsAppend, __int64 AUCode,
  TdsICCardDM * ADM, TTagNode * ARoot, TDate APatBirthday, TkabCustomDataSource * ADataSrc,
  TdsRegTemplateData * ATmplData, bool ACheckPlan, bool ASaveTmpl) : TForm(Owner)
 {
  ExtSchChB->Style->StyleController = ADM->StyleController;
  FUCode                            = AUCode;
  FSaveTmpl                         = ASaveTmpl;
  FPlanMIBPCode                     = -1;
  FDM                               = ADM;
  FDataSrc                          = ADataSrc;
  FDataSet                          = FDataSrc->DataSet;
  FCheckPlan                        = ACheckPlan;
  FInsertedRecId                    = "";
  FTmplData                         = ATmplData;
  TDate tmpVacDate = TDate(0);
  FPlanCode              = 0;
  FEditVacVar            = new TStringList;
  FEditVacVar->Delimiter = ';';
  // FPlanMIBPCode = -1;
  OkBtn->Caption     = FMT(icsRegClassifEditApplyBtnCaption);
  CancelBtn->Caption = FMT(icsRegClassifEditCancelBtnCaption);
  FIsAppend          = AIsAppend;
  UnicodeString FVacVar, FVacSch, FVacInf, FLastSch;
  FPlanDate = TDate(0);
  if (FIsAppend || FCheckPlan)
   {
    FDataSet->Insert();
   }
  else
   {
    FEditVacVar->DelimitedText = FDM->GetPrivVar((__int64)FDataSet->CurRow->Value["3183"]);
    FDataSet->Edit();
   }
  FPatBirthday = APatBirthday;
  FClassDef                 = ARoot->GetTagByUID("1003");
  FClassDesc                = FClassDef->GetChildByName("description");
  FCtrList                  = new TdsRegEDContainer(this, CompPanel);
  FCtrList->UseHash         = true;
  FCtrList->DefXML          = ARoot;
  FCtrList->ReqColor        = FDM->RequiredColor;
  FCtrList->StyleController = FDM->StyleController;
  FCtrList->TemplateData    = FTmplData;
  if (FIsAppend && FCheckPlan && FTmplData)
   FCtrList->TemplateData->NoLock = true;
  FCtrList->DataPath = FDM->DataPath;
  TdsRegEDkabDSDataProvider * FDataProvider = new TdsRegEDkabDSDataProvider;
  FCtrList->DataProvider                 = FDataProvider;
  FDataProvider->DefXML                  = ARoot;
  FCtrList->DataProvider->OnGetClassXML  = FDM->OnGetXML;
  FDataProvider->DataSource              = FDataSrc;
  FCtrList->DataProvider->OnGetCompValue = FDM->OnGetCompValue;
  FCtrList->OnDataChange                 = FCtrlDataChange;
  FCtrList->OnGetExtBtnGetBitmap         = FDM->OnGetExtBtnGetBitmap;
  FCtrList->OnGetFormat                  = FDM->OnGetFormat;
  FCtrList->OnExtBtnClick                = FDM->OnExtBtnClick;
  FCtrList->OnKeyDown                    = FormKeyDown;
  ColWidth                               = FDM->ULEditWidth / 2 + 50;
  OldInfList                             = new TStringList;
  int FGroupCount = 0;
  bool FGroupOnly = true;
  TTagNode * itNode = FClassDef->GetFirstChild();
  while (itNode)
   {
    if (!itNode->CmpName("description"))
     {
      if (itNode->CmpName("group"))
       FGroupCount++ ;
      else
       FGroupOnly = false;
     }
    itNode = itNode->GetNext();
   }
  FLastTop = 0;
  FLastHeight = 0;
  int xTop, xLeft;
  LastLeftCtrl = NULL;
  int MaxH = 0;
  SubLevel = 0;
  xTop     = 2;
  xLeft    = LeftMargin;
  LastCtrl = NULL;
  CreateUnitList(CompPanel, FClassDef, xTop, & xLeft, NULL);
  if (LastLeftCtrl)
   {
    if ((LastLeftCtrl->Top + LastLeftCtrl->Height) <= xTop)
     MaxH = xTop;
    else
     MaxH = LastLeftCtrl->Top + LastLeftCtrl->Height;
   }
  else
   MaxH = xTop;
  ClientHeight = MaxH + 50;
  // Width = ColWidth+(!FOneCol)*ColWidth+(Width-CompPanel->Width)+LeftMargin*2;
  // FCtrList->SetOnCtrlDataChange(0);
  FClassDef->GetFirstChild();
  FClassDef->Iterate(FSetFocus);
  if (FIsAppend)
   {
    if (FCheckPlan && FTmplData)
     {
      RegComp(_VacDATE_)->SetEnable(true);
      RegComp(_VacCODE_)->SetEnable(true);
      RegComp("32AE")->SetEnable(true);
      RegComp("3315")->SetValue(RegComp(_VacDATE_)->GetValue(""), true);
//      RegComp("3315")->SetEnable(false);
     }
    bool valid;
    if (FDM->OnGetDefValues)
     FDM->OnGetDefValues(-1, FClassDef, FCtrList, valid);
    Caption = FClassDef->GetChildByName("description")->AV["insertlabel"];
    RegComp(_VacCODE_)->UpdateChanges();
    if (FCheckPlan && FTmplData)
     {
      // ShowMessage("FCheckPlan && FTmplData =" + FTmplData->ToJSONString(true));
      // "1021":"3.V3;4.V3;2.V3;5.V3"
      FEditVacVar->DelimitedText = FTmplData->Value["1021"];
      if (FTmplData->IsTemplate("pldate"))
       FPlanDate = TDate(VarToStr(FTmplData->Value["pldate"]));
      if (FTmplData->IsTemplate("1020"))
       FPlanMIBPCode = (__int64)FTmplData->Value["1020"];
      if (FTmplData->IsTemplate("32AE"))
       FPlanCode = (__int64)FTmplData->Value["32AE"];
      TAnsiStrMap::iterator FRC;
      // ShowMessage("FPlanMIBPCode = " + IntToStr(FPlanMIBPCode));
      for (int i = 0; i < FEditVacVar->Count; i++)
       {
        // 3.V3;4.V3;2.V3;5.V3
        if (FEditVacVar->Strings[i].Trim().Length())
         {
          FVacVar = GetRPartB(FEditVacVar->Strings[i].UpperCase(), '.');
          FVacSch = "_NULL_";
          FRC     = privSchByVariant.find(FVacVar);
          if (FRC != privSchByVariant.end())
           FVacSch = FRC->second;
          FVacInf                 = GetLPartB(FEditVacVar->Strings[i].UpperCase(), '.');
          FLastSch                = FVacSch;
          FEditVacVar->Strings[i] = FVacInf + ":" + FVacVar + "^" + FVacSch;
         }
       }
      SetVacTypes(FEditVacVar);
      RegComp(_VacDATE_)->UpdateChanges();
      // RegComp(_Doza_)->SetValue(PriDozeVal());
     }
   }
  else
   {
    Caption = FClassDef->GetFirstChild()->AV["editlabel"];
    RegComp(_VacCODE_)->UpdateChanges();
    FLastSch           = "";
    ExtSchChB->Checked = false;
    for (int i = 0; i < FEditVacVar->Count; i++)
     {
      // V1:3^016A
      FVacVar = GetLPartB(FEditVacVar->Strings[i].UpperCase(), ':');
      FVacSch = GetRPartB(FEditVacVar->Strings[i].UpperCase(), ':');
      FVacInf = GetLPartB(FVacSch, '^').ToIntDef(0);
      FVacSch = GetRPartB(FVacSch, '^');
      if (!ExtSchChB->Checked && FLastSch.Length() && (FLastSch != FVacSch))
       ExtSchChB->Checked = true;
      FLastSch                = FVacSch;
      FEditVacVar->Strings[i] = FVacInf + ":" + FVacVar + "^" + FVacSch;
     }
    SetVacTypes(FEditVacVar);
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardPrivEditForm::FSetFocus(TTagNode * itxTag)
 {
  TControl *tmpCtrl;
  if (itxTag->GetAttrByName("uid"))
   {
     if (FCtrList->GetEDControl(itxTag->AV["uid"]))
      {
        tmpCtrl = FCtrList->EditItems[itxTag->AV["uid"]]->GetFirstEnabledControl();
        if (tmpCtrl)
         {
           ActiveControl = (TWinControl*)tmpCtrl;
           return true;
         }
      }
   }
  return false;
//
//  TControl * tmpCtrl;
//  if (itxTag->GetAttrByName("uid"))
//   {
//    TdsRegEDItem * tmpItem = FCtrList->GetEDControl(itxTag->AV["uid"]);
//    if (tmpItem)
//     {
//      tmpCtrl = tmpItem->GetFirstEnabledControl();
//      if (tmpCtrl)
//       {
//        if (!tmpItem->GetValue("").Trim().Length() || !FIsAppend)
//         {
//          ActiveControl = (TWinControl *)tmpCtrl;
//          return true;
//         }
//       }
//     }
//   }
//  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardPrivEditForm::CreateUnitList(TWinControl * AParent, TTagNode * ANode, int & ATop, int * ALeft,
  TList * GroupComp, int APIndex)
 {
  // FTabDown = false;
  TdsRegEDItem * tmpItem;
  // !!!   UnicodeString rfVal  = DS->FieldByName("R10A8")->AsString;
  // //   фильтр по инфекции
  // FInvadeClassCB(fltInfCB->Properties->Items, 0, true ,DM->xmlRegDef->GetTagByUID("003a"), false, (TDataSet*)DM->quFree, "","", DM->xmlRegDef->GetTagByUID("003a"));
  ATop = 5;
  int FOrder = 0;
  // <extedit  'Код пациента'            uid='017A' ref='1000' depend='017A.017A' isedit='0' fieldtype='integer'/>
  tmpItem          = AddRegED("32D7");
  tmpItem->Visible = false;
  tmpItem          = AddRegED("017A");
  tmpItem->Visible = false;
  tmpItem->SetValue(FUCode);
  // <extedit 'Код плана' uid='32AE' ref='1084' depend='32AE.32AE' isedit='0' fieldtype='integer'/>
  tmpItem          = AddRegED("32AE");
  tmpItem->Visible = false;
  tmpItem->SetValue(FPlanCode);
  // <extedit  'Возраст'                 uid='10B4' inlist='l' depend='1024' isedit='0' length='9'/>
  // <date 'ДР пациента' uid='1155' isedit='0' default='patbd'/>
  tmpItem          = AddRegED("1155");
  tmpItem->Visible = false;
  tmpItem->SetValue(FPatBirthday.FormatString("dd.mm.yyyy"));

  tmpItem          = AddRegED("3315");  //дата плана служебное поле
  tmpItem->Visible = false;

  // *****  <date     'Дата выполнения'         uid='1024' required='1' inlist='l' default='CurrentDate'/>
  tmpItem = AddRegED(_VacDATE_);
  ATop += SetAlign(tmpItem, _EDLEFT, ATop, 200, FOrder++);
  tmpItem->OnDataChange = PriDateChange;
  // ***** 'Туровая прививка'       uid='10AE' required='yes' inlist='list_ext' default='check'/>
  AddRegED(_IsTur_);
  // *****  <choiceDB 'МИБП'                    uid='1020' ref='0035' required='1' inlist='l' depend='1075.002D'/>
  tmpItem = AddRegED(_VacCODE_);
  // VacCB->Top = ATop; VacCB->Left = _EDLEFT;
  ATop += SetAlign(tmpItem, _EDLEFT, ATop, 390, FOrder++);
  ((TWinControl *)tmpItem->GetControl())->Width = 350;
  tmpItem->SetEDLeft(42);
  ((TriChoiceDB *)tmpItem)->ExtSearchMode = true;
  ((TriChoiceDB *)tmpItem)->SetExtSearchValues(FDM->ExtSearchVacData);
  tmpItem->OnDataChange = CBChange;
  // VacCB->Properties->Items->Assign(RegComp(_VacCODE_)->GetStrings("0035"));
  // tmpItem->Visible = false;
  // *****  <text     'Вид'                     uid='1021' required='1' inlist='l' cast='up' fixedvar='1' length='10'/>
  tmpItem            = AddRegED(_VacTYPE_);
  tmpItem->Visible   = false;
  InfPanel->TabOrder = FOrder++ ;
  InfPanel->Top      = ATop + 3; /* NewVariantGrid->TabOrder = FOrder++; */
  ATop += InfPanel->Height + 5;
  // *****  <choiceDB 'Вакцинация от'           uid='1075' ref='003A' required='1' inlist='l' depend='1020.002D'/>
  tmpItem          = AddRegED(_INFCODE_);
  tmpItem->Visible = false;
  // *****  <digit    'Доза'                    uid='1022' inlist='e' digits='4' decimal='3' min='0' max='9' default='006A'/>
  tmpItem = AddRegED(_Doza_);
  SetAlign(tmpItem, _EDLEFT, ATop, 228, FOrder++);
  // *****  <text     'Серия'                   uid='1023' inlist='e' cast='no' length='20'/>
  tmpItem = AddRegED(_SER_);
  ATop += SetAlign(tmpItem, _EDLEFT2, ATop, 225, FOrder++);
  // *****  <binary   'Реакция проверена'       uid='1025' required='1' inlist='e'/>
  tmpItem = AddRegED(_RCheck_);
  ATop += SetAlign(tmpItem, _EDLEFT, ATop, 228, FOrder++);
  // *****  <binary   'Есть общая реакция'      uid='1026' required='1' inlist='e'>
  tmpItem = AddRegED(_RComm_);
  SetAlign(tmpItem, _EDLEFT, ATop, 228, FOrder++);
  // *****  <binary   'Есть местная реакция'    uid='1028' required='1' inlist='e'>
  tmpItem = AddRegED(_RLoc_);
  ATop += SetAlign(tmpItem, _EDLEFT2, ATop, 225, FOrder++);
  // *****  <choiceDB 'Формат реакции'          uid='10A8' ref='0038' required='1' inlist='e' depend='1020.0039'>
  tmpItem = AddRegED(_RType_);
  SetAlign(tmpItem, _EDLEFT, ATop, 250, FOrder++);
  // *****  <digit    'Значение реакции'        uid='1027' ref='008E' required='1' inlist='e' depend='10A8.0039' digits='5' min='0' max='9999'>
  tmpItem = AddRegED(_RVal_);
  ATop += SetAlign(tmpItem, _EDLEFT2, ATop, 205, FOrder++);
  tmpItem->Left = 252;
  AdmBl->Top    = ATop;
  ATop += 7;
  // *****  <binary   'Туровая прививка'        uid='10AE' required='1' inlist='e'/>
  tmpItem = RegComp(_IsTur_);
  ATop += SetAlign(tmpItem, _EDLEFT, ATop, 200, FOrder++); // _EDLEFT, ATop, 228,
  // *****  <binary   'Выполнена в данном ЛПУ'  uid='101F' required='1' inlist='e' inv'Выполнена в другой организации' default='check'/>
  tmpItem = AddRegED(_InThisLPU_);
  SetAlign(tmpItem, _EDLEFT, ATop, 200, FOrder++);
  tmpItem->OnDataChange = InThisLPUChBChange;
  // *****  <choiceDB 'ЛПУ'                     uid='017F' ref='00C7' inlist='e' linecount='2'/>
  tmpItem = AddRegED(_LPU_);
  // tmpItem->Enabled = false;
  tmpItem->OnDataChange = LPUCBChange;
  ATop += SetAlign(tmpItem, 200, ATop, 200, FOrder++); // ATop +=
  tmpItem->SetEDLeft(42);
  // <extedit  'Схема'                   uid='1092' required='1' inlist='e' isedit='0' linecount='2' length='9'/>
  // <choice   'Тур закрыт'              uid='10AF' default='0'>
  // *****  <choiceDB 'Врач'                    uid='10B0' ref='00AA' inlist='e' depend='017F.00AA;0180.00AA' linecount='2'/>
  tmpItem = AddRegED("10B0");
  SetAlign(tmpItem, 7, ATop, 200, FOrder++);
  tmpItem->SetLabel(tmpItem->DefNode->AV["name"]);
  ((TWinControl *)tmpItem->GetControl())->Width = 160;
  tmpItem->SetEDLeft(30);
  // *****  <choiceDB 'Мед. сестра'             uid='0181' ref='00AA' inlist='e' depend='017F.00AA;0180.00AA' linecount='2'/>
  tmpItem = AddRegED("0181");
  ATop += SetAlign(tmpItem, 205, ATop, 245, FOrder++);
  tmpItem->SetLabel(tmpItem->DefNode->AV["name"]);
  ((TWinControl *)tmpItem->GetControl())->Width = 175;
  tmpItem->SetEDLeft(68);
  // *****  <choiceDB 'Источник финансирования' uid='10B1' ref='00AB' inlist='e' linecount='2'/>
  tmpItem = AddRegED("10B1");
  ATop += SetAlign(tmpItem, _EDLEFT, ATop, 400, FOrder++);
  ((TWinControl *)tmpItem->GetControl())->Width = 287;
  tmpItem->SetEDLeft(160);
  // *****  <choiceDB 'Доп. информация'         uid='0182' ref='00BE' inlist='e' linecount='3'/>
  tmpItem = AddRegED("0182");
  ATop += SetAlign(tmpItem, _EDLEFT, ATop, 400, FOrder++);
  ((TWinControl *)tmpItem->GetControl())->Width = 287;
  tmpItem->SetEDLeft(160);
  // ClientHeight = ATop+BtnPanel->Height+10;
  // SchLab->Color = LtColor(clBtnFace,32);
  // SchValLab->Color = LtColor(clBtnFace,32);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardPrivEditForm::AlignCtrls(TList * ACtrlList)
 {
  int LabR, EDL;
  int xLabR, xEDL;
  LabR = 0;
  EDL  = 9999;
  TdsRegEDItem * tmpED;
  for (int i = 0; i < ACtrlList->Count; i++)
   {
    // определяем max правую границу меток контролов
    // и min левую самих контролов
    tmpED = ((TdsRegEDItem *)ACtrlList->Items[i]);
    tmpED->GetEDLeft(& xLabR, & xEDL);
    if (xLabR != -1)
     LabR = max(xLabR, LabR);
    if (xEDL != -1)
     EDL = min(xEDL, EDL);
   }
  if ((LabR + 2) < EDL)
   {
    // max правая граница меток контролов
    // и min левая самих контролов не пересекаются
    // выравниваем контролы по min левой границе самих контролов
    for (int i = 0; i < ACtrlList->Count; i++)
     {
      tmpED = ((TdsRegEDItem *)ACtrlList->Items[i]);
      tmpED->SetEDLeft(EDL);
     }
   }
  else
   {
    // иначе выравниваем по "max правая граница меток контролов" + 2
    for (int i = 0; i < ACtrlList->Count; i++)
     {
      tmpED = ((TdsRegEDItem *)ACtrlList->Items[i]);
      tmpED->SetEDLeft(LabR + 2);
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardPrivEditForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_RETURN)
   {
    ActiveControl = OkBtn;
    OkBtnClick(this);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardPrivEditForm::FSaveVacType()
 {
  UnicodeString FVacVar = "";
  UnicodeString FInfVacVar;
  TcxTreeListNode * tmpNode;
  UnicodeString FSchLine;
  TTagNode * FLineNode;
  for (int i = 0; i < NewVariantGrid->Count; i++)
   {
    // вид:код_инфекции^uid_схемы
    tmpNode   = NewVariantGrid->Items[i];
    FSchLine  = privSchByVariant[tmpNode->Texts[1]];
    FLineNode = FDM->VacSchDef->GetTagByUID(FSchLine);
    if (FLineNode && FLineNode->CmpName("line"))
     {
      if (FLineNode->GetParent() && FLineNode->GetParent()->CmpName("schema"))
       {
        FInfVacVar = privVariantBySch[FSchLine] + ":" + IntToStr((int)tmpNode->Data) + "^" + FLineNode->GetParent()
          ->AV["uid"].UpperCase() + "." + FLineNode->AV["uid"].UpperCase();
       }
      else
       FInfVacVar = "Доп.:" + IntToStr((int)tmpNode->Data) + "^_NULL_";
     }
    else
     FInfVacVar = "Доп.:" + IntToStr((int)tmpNode->Data) + "^_NULL_";
    if (FVacVar.Length())
     FVacVar += ";";
    FVacVar += FInfVacVar;
   }
  RegComp(_VacTYPE_)->SetValue(FVacVar);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardPrivEditForm::OkBtnClick(TObject * Sender)
 {
  TWinControl * tmpCtrl = ActiveControl;
  ActiveControl = OkBtn;
  ActiveControl = tmpCtrl;
  bool FAllInfSet = true;
  for (int i = 0; (i < NewVariantGrid->Count) && FAllInfSet; i++)
   {
    FAllInfSet &= NewVariantGrid->Items[i]->Texts[1].Trim().Length();
   }
  if (!FAllInfSet)
   {
    _MSG_ERR("Необходимо указать вид вакцинации для всех инфекций!", "Ошибка");
   }
  else
   {
    FSaveVacType();
    if (CheckInput())
     { // запрос на проверку данных
      try
       {
        UnicodeString PRC = FDataSet->Post(FInsertedRecId, true);
        if (!SameText(PRC, "ok"))
         _MSG_ERR(PRC, "Ошибка");
        else
         {
          try
           {
            if (FTmplData && FSaveTmpl)
             {
              FTmplData->Value["1024"] = TDate(RegComp("1024")->GetValue("")); // <date 'Дата выполнения' uid='1024'
              FTmplData->Value["1020"] = RegComp("1020")->GetValue("0035."); // <choiceDB 'МИБП' uid='1020' ref='0035'
              if (RegComp("1021")->GetValue("").Length())
               FTmplData->Value["1021"] = RegComp("1021")->GetValue(""); // <text name='Вид' uid='1021'
              if (RegComp("1075")->GetValue("").Length())
               FTmplData->Value["1075"] = RegComp("1075")->GetValue("003A."); // <cDB 'Инфекция' uid='1075' ref='003A'
              if (RegComp("1022")->GetValue("").Length())
               FTmplData->Value["1022"] = RegComp("1022")->GetValue(""); // <digit name='Доза' uid='1022'
              if (RegComp("1023")->GetValue("").Length())
               FTmplData->Value["1023"] = RegComp("1023")->GetValue(""); // <text name='Серия' uid='1023'
              FTmplData->Value["1025"] = RegComp("1025")->GetCompValue(""); // <bin 'Реакция проверена' uid='1025'
              FTmplData->Value["1026"] = RegComp("1026")->GetCompValue(""); // <bin 'Есть общ р' uid='1026'
              FTmplData->Value["1028"] = RegComp("1028")->GetCompValue(""); // <bin 'Есть мест р' uid='1028'
              if (RegComp("10A8")->GetValue("0038.").Length())
               FTmplData->Value["10A8"] = RegComp("10A8")->GetValue("0038."); // <cDB 'Фор реак' uid='10A8' ref='0038'
              if (RegComp("1027")->GetValue("").Length())
               FTmplData->Value["1027"] = RegComp("1027")->GetValue(""); // <dig 'Зн реак' uid='1027' ref='008E'
              FTmplData->Value["10AE"] = RegComp("10AE")->GetCompValue(""); // <binary 'Туровая прививка' uid='10AE'
              FTmplData->Value["101F"] = RegComp("101F")->GetCompValue(""); // <bin 'Выполнена в данном ЛПУ' uid='101F'
              if (RegComp("017F")->GetValue("00C7.").Length())
               FTmplData->Value["017F"] = RegComp("017F")->GetValue("00C7."); // <choiceDB 'ЛПУ' uid='017F' ref='00C7'
              if (RegComp("10B0")->GetValue("00AA.").Length())
               FTmplData->Value["10B0"] = RegComp("10B0")->GetValue("00AA."); // <choiceDB 'Врач' uid='10B0' ref='00AA'
              if (RegComp("0181")->GetValue("00AA.").Length())
               FTmplData->Value["0181"] = RegComp("0181")->GetValue("00AA."); // <cDB 'Мед. сест' uid='0181' ref='00AA'
              if (RegComp("10B1")->GetValue("00AB.").Length())
               FTmplData->Value["10B1"] = RegComp("10B1")->GetValue("00AB."); // <cDB 'Ист фин' uid='10B1' ref='00AB'
              if (RegComp("0182")->GetValue("00BE.").Length())
               FTmplData->Value["0182"] = RegComp("0182")->GetValue("00BE."); // <cDB 'Доп. инф' uid='0182' ref='00BE'
             }
           }
          __finally
           {
           }
          ModalResult = mrOk;
         }
       }
      catch (EkabCustomDataSetError & E)
       {
        _MSG_ERR(E.Message, "Ошибка");
       }
     }
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardPrivEditForm::CheckInput()
 {
  bool RC = false;
  TDate FPriDate;
  /*
   TDateTime PBirthDay;
   DM->qtExec("Select R0031 From casebook1 Where CODE="+IntToStr(FUCode));
   if (DM->quFree->RecordCount)
   PBirthDay = quFNDate("R0031");
   else
   {
   MessageBox(Handle,"Ошибка определения даты рождения пациента.","Ошибка",0);
   return false;
   }
   */
  try
   {
    if (FIsAppend)
     FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
    else
     FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
    UnicodeString xValid = "";
    FClassDef->Iterate(GetInput, xValid);
    if (!xValid.Length())
     {
      RC = false;
      if (!TryStrToDate(RegComp(_VacDATE_)->GetValue("").c_str(), FPriDate))
       _MSG_ERR("Введите корректную дату выполнения прививки.", "Ошибка");
      else if (FPriDate > Date())
       _MSG_ERR("Дата выполнения прививки не может быть больше текущей.", "Ошибка");
      else if (FPriDate < FPatBirthday)
       _MSG_ERR("Дата выполнения прививки не может быть меньше даты рождения пациента.", "Ошибка");
      else
       RC = true;
      if (RC && FCheckPlan && (int)FPlanDate)
       {
        int PlanMon = FPlanDate.FormatString("mm").ToIntDef(0);
        int PrivMon = FPriDate.FormatString("mm").ToIntDef(0);
        if (PlanMon != PrivMon)
         {
          TDate FCurMonth = TDate(Date().FormatString("01.mm.yyyy"));
          TDate FPrevMonth = IncMonth(FCurMonth, -1);
          TDate FNextMonth = IncMonth(FCurMonth, 2);
          if ((FPriDate < FPrevMonth) || (FPriDate >= FNextMonth)) // Дата прививки не попадает в плановые месяцы
             _MSG_INF("Дата прививки не относится к планам на " + FPrevMonth.FormatString("mmmm") + ", " +
            FCurMonth.FormatString("mmmm") + ".\nПлан, из которого производится отметка - " +
            FPlanDate.FormatString("mmmm"), "Сообщение");
          else
           _MSG_INF("Внимание! Дата прививки относится к плану на " + FPriDate.FormatString("mmmm") +
            ".\nПлан, из которого производится отметка - " + FPlanDate.FormatString("mmmm"), "Сообщение");
         }
       }
      if (RC && FDM->OnExtValidate)
       {
        // RC = false;
        if (FIsAppend)
         FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
        else
         FDM->OnExtValidate(FDataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardPrivEditForm::GetInput(TTagNode * itTag, UnicodeString & UID)
 {
  if (itTag)
   {
    if (FCtrList->GetEDControl(itTag->AV["uid"]))
     if (!FCtrList->EditItems[itTag->AV["uid"]]->CheckReqValue())
      {
       UID = "no";
       return true;
      }
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardPrivEditForm::FormDestroy(TObject * Sender)
 {
  delete FEditVacVar;
  FCtrList->ClearOnCtrlDataChange(0);
  delete FCtrList->DataProvider;
  delete FCtrList;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardPrivEditForm::FCtrlDataChange(TTagNode * ItTag, UnicodeString & Src,
  TkabCustomDataSource * ASource)
 {
  bool RC = false;
  if (FDM->OnCtrlDataChange)
   {
    RC = FDM->OnCtrlDataChange(ItTag, Src, ASource, FIsAppend, FCtrList, UIDInt(FClassDef->AV["uid"]));
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardPrivEditForm::InThisLPUChBChange(TTagNode * ItTag, UnicodeString & Src)
 {
  if (FDM->LPUCode)
   {
    RegComp(_LPU_)->OnDataChange = NULL;
    try
     {
      if (RegComp(_InThisLPU_)->GetValue("").ToIntDef(0))
       {
        RegComp(_LPU_)->SetEnable(false);
        RegComp(_LPU_)->SetValue(IntToStr(FDM->LPUCode), false);
       }
      else
       {
        RegComp(_LPU_)->SetEnable(false);
        RegComp(_LPU_)->SetEnable(true);
       }
     }
    __finally
     {
      RegComp(_LPU_)->OnDataChange = LPUCBChange;
     }
   }
  return true;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardPrivEditForm::LPUCBChange(TTagNode * ItTag, UnicodeString & Src)
 {
  if (FDM->LPUCode)
   {
    RegComp(_InThisLPU_)->OnDataChange = NULL;
    try
     {
      if (RegComp(_LPU_)->GetValue("").ToIntDef(0) == FDM->LPUCode)
       RegComp(_InThisLPU_)->SetValue("1", true);
      else
       RegComp(_InThisLPU_)->SetValue("0", true);
     }
    __finally
     {
      RegComp(_InThisLPU_)->OnDataChange = InThisLPUChBChange;
     }
   }
  return true;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsCardPrivEditForm::SetAlign(TdsRegEDItem * AItem, int ALeft, int ATop, int AWidth, int ATabOrder)
 {
  AItem->Left  = ALeft;
  AItem->Width = AWidth;
  AItem->Top   = ATop;
  AItem->Anchors.Clear();
  AItem->Anchors << akLeft << akRight << akTop;
  AItem->TabOrder = ATabOrder;
  return AItem->Height + 4;
 }
// ---------------------------------------------------------------------------
inline TdsRegEDItem * __fastcall TdsCardPrivEditForm::RegComp(UnicodeString AUID)
 {
  return FCtrList->EditItems[AUID];
 }
// ---------------------------------------------------------------------------
inline TdsRegEDItem * __fastcall TdsCardPrivEditForm::AddRegED(UnicodeString AUID)
 {
  return FCtrList->AddEditItem(FClassDef->GetTagByUID(AUID), FIsAppend);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardPrivEditForm::PriDateChange(TTagNode * ItTag, UnicodeString & Src)
 {
  bool RC = false;
  try
   {
    if (FIsAppend)
     RegComp(_Doza_)->SetValue(PriDozeVal());
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsCardPrivEditForm::CBChange(TTagNode * ItTag, UnicodeString & Src)
 {
  // ShowMessage("CBChange");
  bool RC = false;
  try
   {
    TcxTreeListNode * tmpNode;
    SchValLab->Caption = "";
    NewVariantGrid->Clear();
    InfNodes.clear();
    InfList = RegComp(_INFCODE_)->GetStrings("003A");
    if (InfList->Count)
     RegComp(_INFCODE_)->SetValue(IntToStr((int)InfList->Objects[0]));
    bool FTur = (RegComp(_IsTur_)->GetValue("") == "1");
    UnicodeString vacCode = RegComp(_VacCODE_)->GetValue("0035.");
    if (vacCode.Length())
     {
      UnicodeString vacSch = "";
      UnicodeString vacSchTur = "";
      FDM->GetVacSch(InfList, vacCode, FUCode, vacSch, vacSchTur);
      if (!FIsAppend)
       {
        if (VarToStr(FDataSet->EditedRow->Value["1092"]).UpperCase() != "_NULL_")
         vacSch = _GUI(VarToStr(FDataSet->EditedRow->Value["1092"]));
       }
      else
       {
        // значение для дозы по умолчанию
        // RegComp(_Doza_)->SetValue(RegComp(_VacCODE_)->GetValue("0035.006A"));
       }
      // значение для дозы по умолчанию
      UnicodeString emptyString;
      PriDateChange(NULL, emptyString);
      int tmpInfCode;
      if (InfList)
       {
        for (int i = 0; i < InfList->Count; i++)
         {
          tmpInfCode           = (int)(InfList->Objects[i]);
          InfNodes[tmpInfCode] = NewVariantGrid->Root->AddChild();
          tmpNode              = InfNodes[tmpInfCode];
          tmpNode->Texts[0]    = InfList->Strings[i];
          tmpNode->Data        = (void *)tmpInfCode;
          tmpNode->Texts[1]    = "";
          tmpNode->Values[1]   = "";
          tmpNode->Texts[2]    = "";
         }
       }
      privSchByVariant.clear();
      privVariantBySch.clear();
      ((TcxCustomComboBoxProperties *)VacVarCol->Properties)->Items->Clear();
      FShema = NULL;
      if (FTur)
       { // туровая
        if (vacSchTur.Length())
         FShema = FDM->VacSchDef->GetTagByUID(vacSchTur);
       }
      else
       {
        if (vacSch.Length())
         {
          if (vacSch.Pos("."))
           FShema = FDM->VacSchDef->GetTagByUID(_GUI(vacSch));
          else
           FShema = FDM->VacSchDef->GetTagByUID(vacSch);
         }
       }
      if (FShema)
       {
        // отрисовка схемы
        if (!FShema->AV["ref"].Length())
         SchValLab->Caption = "Ошибка";
        else
         SchValLab->Caption = FDM->VacList[FShema->AV["ref"]] + " (Вариант '" + FShema->AV["name"] + "')";
        if (!FShema->CmpAV("ref", vacCode))
         FShema = FDM->VacSchDef->GetChildByAV("schema", "ref", vacCode, true);
       }
      else
       FShema = FDM->VacSchDef->GetChildByAV("schema", "ref", vacCode, true);
      /* if (FShema)
       {
       // отрисовка схемы
       if (!FShema->AV["ref"].Length())
       SchValLab->Caption = "Ошибка";
       else
       SchValLab->Caption = FDM->VacList[FShema->AV["ref"]]+" (Вариант '"+FShema->AV["name"]+"')";
       } */
      TTagNode * tmp = FShema;
      if (tmp)
       {
        // формирование вариантов вакцинации
        TTagNodeMap SchList;
        if (ExtSchChB->Checked)
         FDM->GetSchemeList((int)(NewVariantGrid->Items[0]->Data), SchList, true);
        else
         SchList[tmp->AV["uid"]] = tmp;
        UnicodeString FSchLine;
        for (TTagNodeMap::iterator i = SchList.begin(); i != SchList.end(); i++)
         {
          if (i->second->CmpAV("ref", vacCode))
           {
            tmp = i->second->GetFirstChild();
            while (tmp)
             {
              if (tmp->CmpName("line"))
               {
                FSchLine = tmp->AV["name"];
                if (ExtSchChB->Checked)
                 FSchLine += "  схема \"" + i->second->AV["name"] + "\"";
                ((TcxCustomComboBoxProperties *)VacVarCol->Properties)->Items->Add(FSchLine);
                privSchByVariant[FSchLine]       = tmp->AV["uid"];
                privVariantBySch[tmp->AV["uid"]] = tmp->AV["name"];
                privVariantOld[tmp->AV["uid"]]   = FSchLine;
               }
              tmp = tmp->GetNext();
             }
           }
         }
        if (!FTur)
         {
          ((TcxCustomComboBoxProperties *)VacVarCol->Properties)->Items->Add("Доп.");
          privSchByVariant["Доп."]   = "_NULL_";
          privVariantBySch["_NULL_"] = "Доп.";
          privVariantOld["_NULL_"]   = "Доп.";
         }
       }
      else
       {
        if (!FTur)
         {
          ((TcxCustomComboBoxProperties *)VacVarCol->Properties)->Items->Add("Доп.");
          privSchByVariant["Доп."]   = "_NULL_";
          privVariantBySch["_NULL_"] = "Доп.";
          privVariantOld["_NULL_"]   = "Доп.";
         }
       }
      // Определение наличия в плане плановой прививки с указанными параметрами
      UnicodeString FSQL;
      UnicodeString tmpVacVariant = "";
      UnicodeString tmpVacSch = "";
      // ShowMessage("CBChange::FTmplData =" + FTmplData->ToJSONString(true));
      // ShowMessage("CBChange::FPlanMIBPCode =" + IntToStr(FPlanMIBPCode));
      UnicodeString FVacVar, FVacSch, FVacInf, FLastSch;
      TAnsiStrMap::iterator FRC;
      if (FPlanMIBPCode != -1)
       { // отметка из плана
        FEditVacVar->DelimitedText = FTmplData->Value["1021"];
        // ShowMessage("FEditVacVar->Count = " + IntToStr(FEditVacVar->Count) + " var=" + FEditVacVar->Text);
        for (int i = 0; i < FEditVacVar->Count; i++)
         {
          // 3.V3;4.V3;2.V3;5.V3
          if (FEditVacVar->Strings[i].Trim().Length())
           {
            FVacVar = GetRPartB(FEditVacVar->Strings[i].UpperCase(), '.');
            FVacSch = "_NULL_";
            FRC     = privSchByVariant.find(FVacVar);
            if (FRC != privSchByVariant.end())
             FVacSch = FRC->second;
            FVacInf  = GetLPartB(FEditVacVar->Strings[i].UpperCase(), '.');
            FLastSch = FVacSch;
            if (!tmpVacSch.Length())
             tmpVacSch = FVacInf + "." + FVacSch;
            else
             tmpVacSch += ";" + FVacInf + "." + FVacSch;
           }
         }
        tmpVacVariant = FTmplData->Value["1021"];
       }
      else
       { // отметка из прививочной карты
        TDate tmpVacDate;
        if (TryStrToDate(RegComp(_VacDATE_)->GetValue("").c_str(), tmpVacDate))
         {
          TStringList * FPlanVacVar = new TStringList;
          FPlanVacVar->Delimiter = ';';
          try
           {
            FPlanVacVar->DelimitedText = FDM->GetPlanVacVar(IntToStr(FUCode), tmpVacDate);
            for (int i = 0; i < FPlanVacVar->Count; i++)
             {
              // 3.V3;4.V3;2.V3;5.V3
              if (FPlanVacVar->Strings[i].Trim().Length())
               {
                FVacVar = GetRPartB(FPlanVacVar->Strings[i].UpperCase(), '.');
                FVacSch = "_NULL_";
                FRC     = privSchByVariant.find(FVacVar);
                if (FRC != privSchByVariant.end())
                 FVacSch = FRC->second;
                FVacInf  = GetLPartB(FPlanVacVar->Strings[i].UpperCase(), '.');
                FLastSch = FVacSch;
                if (tmpVacSch.Length())
                 tmpVacSch += ";" + FVacInf + "." + FVacSch;
                else
                 tmpVacSch = FVacInf + "." + FVacSch;
               }
             }
            tmpVacVariant = tmpVacSch;
           }
          __finally
           {
            delete FPlanVacVar;
           }
         }
        else
         tmpVacVariant = "";
       }
      // ShowMessage(tmpVacVariant+"\n tmpVacSch= "+tmpVacSch);
      if (tmpVacVariant.Length())
       {
        TStringList * tmpVacTypeList = NULL;
        TStringList * tmpPrivList = NULL;
        try
         {
          TReplaceFlags repF;
          repF << rfReplaceAll;
          UnicodeString tmpSch;
          tmpVacTypeList       = new TStringList;
          tmpPrivList          = new TStringList;
          tmpVacTypeList->Text = StringReplace(tmpVacSch, ";", "\n", repF);
          TTagNode * SchNode, *LineNode;
          UnicodeString FSchName = "";
          bool ExtSch = false;
          // TAnsiStrMap tmpSchCount;
          for (int i = 0; i < tmpVacTypeList->Count; i++)
           {
            LineNode = FDM->VacSchDef->GetTagByUID(_UID(tmpVacTypeList->Strings[i]));
            if (LineNode)
             {
              if (LineNode->CmpName("line"))
               {
                SchNode = LineNode->GetParent();
                if (SchNode->CmpName("schema"))
                 {
                  tmpPrivList->Add((_GUI(tmpVacTypeList->Strings[i]) + ":V1^" + SchNode->AV["uid"] + "." +
                    LineNode->AV["uid"]).c_str());
                  if (!ExtSch)
                   {
                    if (!FSchName.Length())
                     FSchName = SchNode->AV["uid"].UpperCase();
                    else
                     ExtSch = FSchName != SchNode->AV["uid"].UpperCase();
                   }
                 }
               }
             }
           }
          if (tmpPrivList->Count)
           {
            ExtSchChB->Checked = ExtSch;
            SetVacTypes(tmpPrivList);
           }
         }
        __finally
         {
          if (tmpVacTypeList)
           delete tmpVacTypeList;
          if (tmpPrivList)
           delete tmpPrivList;
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsCardPrivEditForm::PriDozeVal()
 {
  UnicodeString RC = "";
  try
   {
    TDate FVacDate;
    if (TryStrToDate(RegComp(_VacDATE_)->GetValue(""), FVacDate))
     {
      RC = FDM->GetVacDefDoze(FVacDate, FPatBirthday, RegComp(_VacCODE_)->GetValue("0035."));
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardPrivEditForm::ExtSchChBClick(TObject * Sender)
 {
  UnicodeString emptyString;
  CBChange(NULL, emptyString);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardPrivEditForm::VacVarColPropertiesCloseUp(TObject * Sender)
 {
  if (NewVariantGrid->SelectionCount)
   {
    if (NewVariantGrid->InplaceEditor)
     {
      UnicodeString Val;
      TcxTreeListNode * itNode;
      itNode = NewVariantGrid->Selections[0];
      Val    = VarToStr(NewVariantGrid->InplaceEditor->EditValue);
      itNode = itNode->GetNext();
      while (itNode)
       {
        itNode->Texts[1]  = Val;
        itNode->Values[1] = Val;
        itNode            = itNode->GetNext();
       }
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsCardPrivEditForm::SetVacTypes(TStringList * AVacTypeList)
 {
  TAnsiStrMap::iterator VRC;
  TTreeListNodeMap::iterator IRC;
  UnicodeString tmpInfCode, tmpVacType, tmpSch;
  // inf_code:vactype^uid_scheme.uid_scheme_line
  for (int i = 0; i < AVacTypeList->Count; i++)
   {
    tmpInfCode = GetPart1(AVacTypeList->Strings[i], ':');
    tmpVacType = GetPart2(AVacTypeList->Strings[i], ':');
    tmpSch     = GetPart2(tmpVacType, '^');
    tmpVacType = GetPart1(tmpVacType, '^');
    VRC        = privVariantOld.find(_UID(tmpSch));
    if (VRC != privVariantOld.end())
     {
      IRC = InfNodes.find(tmpInfCode.ToIntDef(-1));
      if (IRC != InfNodes.end())
       {
        IRC->second->Texts[1]  = VRC->second; // privVariantOld[_UID(tmpSch)];
        IRC->second->Values[1] = VRC->second; // privVariantOld[_UID(tmpSch)];
       }
     }
   }
 }
// ---------------------------------------------------------------------------
