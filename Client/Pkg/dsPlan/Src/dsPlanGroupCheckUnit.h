﻿//---------------------------------------------------------------------------
#ifndef dsPlanGroupCheckUnitH
#define dsPlanGroupCheckUnitH

//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtonEdit.hpp"
#include "cxButtons.hpp"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxGroupBox.hpp"
#include "cxImage.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"
#include "dxGDIPlusClasses.hpp"
#include "dxStatusBar.hpp"
#include "cxDropDownEdit.hpp"
#include "cxCalendar.hpp"
#include "cxProgressBar.hpp"
#include "cxBarEditItem.hpp"
#include "dxBarExtItems.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "dsICPlanDMUnit.h"
#include "KabCustomDS.h"
#include "InsertCardItem.h"
#include "cxCheckBox.hpp"
//---------------------------------------------------------------------------

class PACKAGE TdsPlanGroupCheckForm : public TForm
{
__published:	// IDE-managed Components
 TdxBarManager *PlanTBM;
 TcxStyleRepository *Style;
        TdxStatusBar *StatusSB;
        TActionList *ClassAL;
        TAction *actExit;
  TcxImageList *LClassIL32;
  TPanel *CompPanel;
 TAction *actCheckPlanItem;
 TAction *actShowCard;
 TAction *actRefreshPlan;
 TdxBarLargeButton *dxBarLargeButton7;
 TdxBarLargeButton *dxBarLargeButton8;
 TTimer *ShowTimer;
 TcxStyle *masterContentStyle;
 TcxStyle *detailContentStyle;
 TcxStyle *vacStyle;
 TcxStyle *cxStyle1;
 TcxImage *ProgressPanel;
 TPanel *ProgressPanelMsg;
 TcxStyle *cxStyle2;
 TcxStyle *cxStyle3;
 TcxStyle *mibpStyle;
 TcxStyle *fioStyle;
 TcxStyle *execStyle;
 TcxStyle *doExecStyle;
 TcxStyle *infStyle;
 TcxTreeList *PlanTL;
 TcxTreeListColumn *FIOCol;
 TcxTreeListColumn *MIBPInfCol;
 TcxTreeListColumn *VacTypeCol;
 TcxTreeListColumn *PlanDateCol;
 TcxTreeListColumn *ExecDateCol;
 TcxTreeListColumn *ExecMIBPCol;
 TcxTreeListColumn *PtIdCol;
 TcxTreeListColumn *MIBPCodeCol;
 TcxTreeListColumn *PlanTypeCol;
 TcxImageList *SearchIL;
 TcxButtonEdit *SearchBE;
 TAction *actFind;
 TAction *actClearSearch;
 TTimer *SearchTimer;
 TdxBar *PlanTBMBar1;
 TcxTreeListColumn *OrgLvlCol;
 TcxStyle *otvStyle;
 TdxBarCombo *OrgLevel;
 TdxBarCombo *VacType;
 TcxTreeListColumn *ExecMIBPIdCol;
 TcxTreeListColumn *ExecReakIdCol;
 TdxBarCombo *MIBP;
 TcxTreeListColumn *ReakVal;
 TcxTreeListColumn *ReakTypeCol;
 TcxTreeListColumn *SepCol;
 TdxBarCombo *MIBPCountCB;
 TdxStatusBarContainerControl *StatusSBContainer0;
 TcxProgressBar *PrBar;
	TcxStyle *reqStyle;
 TcxTreeListColumn *BirthDayCol;
 TdxBarControlContainerItem *dxBarControlContainerItem1;
 TcxMaskEdit *SerED;
 TdxBarStatic *dxBarStatic1;
 TAction *actDelete;
 TdxBarLargeButton *dxBarLargeButton1;
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall actExitExecute(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
 void __fastcall actCheckPlanItemExecute(TObject *Sender);
 void __fastcall actRefreshPlanExecute(TObject *Sender);
 void __fastcall ShowTimerTimer(TObject *Sender);
 void __fastcall PlanGridViewDataControllerDataChanged(TObject *Sender);
 void __fastcall PlanTLIsGroupNode(TcxCustomTreeList *Sender, TcxTreeListNode *ANode,
          bool &IsGroup);
 void __fastcall PlanTLCollapsing(TcxCustomTreeList *Sender, TcxTreeListNode *ANode,
          bool &Allow);
 void __fastcall PlanTLFocusedNodeChanged(TcxCustomTreeList *Sender, TcxTreeListNode *APrevFocusedNode,
          TcxTreeListNode *AFocusedNode);
 void __fastcall actFindExecute(TObject *Sender);
 void __fastcall actClearSearchExecute(TObject *Sender);
 void __fastcall SearchBEPropertiesChange(TObject *Sender);
 void __fastcall SearchTimerTimer(TObject *Sender);
 void __fastcall OrgLevelChange(TObject *Sender);
 void __fastcall VacTypeChange(TObject *Sender);
 void __fastcall MIBPChange(TObject *Sender);
 void __fastcall ExecMIBPColGetEditingProperties(TcxTreeListColumn *Sender, TcxTreeListNode *ANode,
          TcxCustomEditProperties *&EditProperties);
 void __fastcall ReakTypeColGetEditingProperties(TcxTreeListColumn *Sender, TcxTreeListNode *ANode,
          TcxCustomEditProperties *&EditProperties);
 void __fastcall ExecMIBPColPropertiesEditValueChanged(TObject *Sender);
 void __fastcall PlanTLEditing(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          bool &Allow);
 void __fastcall MIBPCountCBChange(TObject *Sender);
 void __fastcall SerEDPropertiesValidate(TObject *Sender, Variant &DisplayValue,
          TCaption &ErrorText, bool &Error);
 void __fastcall PlanTLColumn1PropertiesValidate(TObject *Sender, Variant &DisplayValue,
          TCaption &ErrorText, bool &Error);
 void __fastcall PlanTLColumn1PropertiesChange(TObject *Sender);
 void __fastcall ReakColPropertiesEditValueChanged(TObject *Sender);
 void __fastcall ExecDateColPropertiesEditValueChanged(TObject *Sender);
 void __fastcall actDeleteExecute(TObject *Sender);


private:	// User declarations
  TdsICPlanDM *FDM;
  TkabCustomDataSet* FDS;
  TkabCustomDataSource *FPlanDataSrc;
  TStringList *FVacList;
  TStringList *FTestList;
  TStringList *FCheckList;

  UnicodeString FPlanType;
//  typedef map<__int64, TcxTreeListNode*> TPatNodes;
//  typedef map<UnicodeString, TStringList*> TReakList;
//  TReakList *ReacMap;
  TTagNode *FPlanInfNode;
  TTagNode *FPlanNode;
//  TPatNodes FPatNodes;

//  __int64 FUCode;
  TcxTabSheet *FCurPage;

  TTagNode *FSettingNode;
  TcxTreeListNode *LastFindedNode;
  TcxTreeListNode *LastNEFindedNode;
  TcxTreeListNode *LastCNEFindedNode;
  int NECur, NECount;
  int CNECur, CNECount;

  TInsertCardItem * FInsPrivExecutor;
  TInsertCardItem * FInsTestExecutor;
  TInsertCardItem * FInsCheckExecutor;

//  bool notReak;
//  typedef map<int, bool> TBinReak;
//  TBinReak MIBPBinReak;
//  TBinReak ReakValEnabled;

  TDate FPatBirthday;
        bool SaveEnInsert;
        bool SaveEnEdit;
        bool SaveEnDelete;
        bool SaveEnSetTemplate;
        bool SaveEnReSetTemplate;
        bool SaveEnRefresh;
        bool SaveEnFind;
        bool SaveEnFindNext;
        bool SaveEnViewClassParam;
        bool SaveEnViewTemplateParam;

  UnicodeString    SaveCaption;
  UnicodeString    LastF63UCode;
    __int64   FPlanCode, curCD,xpCount,xcTop;

    __int64   FPlanCommCount, FPlanCurr;
    int FLastTop,FLastHeight;

  void __fastcall SetNextValues(int AIdx, int AExtIdx=0);
  TcxTreeListNode * __fastcall GetActiveItemNode();
  int __fastcall GetActiveItemIndex();

  TkabCustomDataSource* __fastcall CreateDataSrc(TTagNode *ADefNode);
  void __fastcall FGetDataProgress(int AMax, int ACur, UnicodeString AMsg);
  void __fastcall GetPlanData();
  TcxTreeListNode* __fastcall GetSelNode();
  TkabCustomDataSetRow * __fastcall CurRow();
    void __fastcall CreateSrc();
  TJSONObject * __fastcall FDSGetValById(UnicodeString AId, UnicodeString ARecId);
  TJSONObject * __fastcall FDSGetValById10(UnicodeString AId, UnicodeString ARecId);
  int __fastcall PlanType(TcxTreeListNode * ANode);
  void __fastcall ShowProgress(bool AShow);
  void __fastcall FOnCardClose(TObject * Sender);
  void __fastcall FilterData();
  void __fastcall FillReacVar(TStrings * AItems);
  bool __fastcall CheckInput();
  void __fastcall ButtonEnabled();
public:		// User declarations
    __fastcall TdsPlanGroupCheckForm(TComponent* Owner, TdsICPlanDM *ADM, __int64 APlanCode, UnicodeString APlanCapt, UnicodeString APlanType);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsPlanGroupCheckForm *dsPlanGroupCheckForm;
//---------------------------------------------------------------------------
#endif

