﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <SysUtils.hpp>
#pragma hdrstop
#include "dsPlanEditClientUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "dsICPlanSetting.h"
#include "msgdef.h"
#include "dsPlanGroupCheckUnit.h"
// #include "pkgsetting.h"
// ---------------------------------------------------------------------------
#define  plOrgFIOID    0
#define  plMIBPInfID   1
#define  plVacTypeID   2
#define  plPlanDateID  3
#define  plAgeID       4
#define  plExecDateID  5
#define  plNotExecOrMIBPID  6
#define  plPatExecType 6
#define  plPatientID   7
#define  plMIBPCodeID  8
#define  plInfCodeID   9
// служебные поля
#define  plExecId      10
#define  plSrcId       11
#define  plSupStateId  12
#define  plTLPUId      13
#define  plNotExecType 14
#define  plIllCodeId   15
#define  plNoMIBPId    16
#define  plTypeID      17
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxImage"
#pragma link "cxImageComboBox"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxSplitter"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma link "dxGDIPlusClasses"
#pragma link "dxStatusBar"
#pragma link "Htmlview"
#pragma link "cxCalendar"
#pragma link "cxCheckComboBox"
#pragma resource "*.dfm"
TdsPlanEditClientForm * dsPlanEditClientForm;
// ---------------------------------------------------------------------------
__fastcall TdsPlanEditClientForm::TdsPlanEditClientForm(TComponent * Owner, TdsICPlanDM * ADM, __int64 APlanCode,
  UnicodeString APlanCapt, UnicodeString APlanType) : TForm(Owner)
 {
  // "com", "uch", "org"
  NECur                            = NECount = CNECur = CNECount = 0;
  FPlanType                        = APlanType.LowerCase();
  LastFindedNode                   = NULL;
  LastNEFindedNode                 = NULL;
  LastCNEFindedNode                = NULL;
  LastF63UCode                     = "";
  SearchBE->Style->StyleController = ADM->StyleController;
  Caption                          = APlanCapt;
  FDM                              = ADM;
  FPlanCode                        = APlanCode;
  SaveEnInsert                     = false;
  SaveEnEdit                       = false;
  SaveEnDelete                     = false;
  SaveEnSetTemplate                = false;
  SaveEnReSetTemplate              = false;
  SaveEnRefresh                    = false;
  SaveEnFind                       = false;
  SaveEnFindNext                   = false;
  SaveEnViewClassParam             = false;
  SaveEnViewTemplateParam          = false;
  FPlanDataSrc                     = NULL;
  FDetailDataSrc                   = NULL;
  FDS                              = NULL;
  FSettingNode                     = NULL;
  FPlanInfNode                     = NULL;
  FPlanNode                        = NULL;
  if (FDM->DefaultClassWidth)
   Width = FDM->DefaultClassWidth;
  FPlanInfNode = FDM->ClassDef->GetTagByUID("1084");
  FPlanNode    = FDM->ClassDef->GetTagByUID("3084");
  FLastTop     = 0;
  FLastHeight  = 0;
  curCD        = 0;
  CreateSrc();
 }
// ---------------------------------------------------------------------------
TkabCustomDataSource * __fastcall TdsPlanEditClientForm::CreateDataSrc(TTagNode * ADefNode)
 {
  TkabCustomDataSource * RC = NULL;
  try
   {
    RC                          = new TkabCustomDataSource(ADefNode);
    RC->DataSet->IdPref         = "plan";
    RC->DataSet->OnGetCount     = FDM->kabCDSGetCount;
    RC->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
    RC->DataSet->OnGetValById   = FDSGetValById;
    RC->DataSet->OnGetValById10 = FDSGetValById10;
    RC->DataSet->OnInsert       = FDM->kabCDSInsert;
    RC->DataSet->OnEdit         = FDM->kabCDSEdit;
    RC->DataSet->OnDelete       = FDM->kabCDSDelete;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::CreateSrc()
 {
  try
   {
    try
     {
      FPlanDataSrc   = CreateDataSrc(FPlanNode);
      FDS            = FPlanDataSrc->DataSet;
      FDetailDataSrc = CreateDataSrc(FPlanInfNode);
      FDetailDS      = FDetailDataSrc->DataSet;
      // Split->CloseSplitter();
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsErrorMsgCaptionSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegistryErrorCommDBErrorMsgCaption), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::FGetDataProgress(int AMax, int ACur, UnicodeString AMsg)
 {
  if (AMax == -1)
   {
    FPlanCurr                        = 1;
    FPlanCommCount                   = 0;
    ProgressPanelMsg->Caption        = "";
    StatusSB->Panels->Items[0]->Text = "";
   }
  else if (!(AMax + ACur))
   {
    ProgressPanelMsg->Caption        = AMsg;
    StatusSB->Panels->Items[0]->Text = AMsg;
   }
  else
   {
    StatusSB->Panels->Items[0]->Text = AMsg + " [" + IntToStr(ACur) + "/" + IntToStr(AMax) + "]";
    ProgressPanelMsg->Caption        = StatusSB->Panels->Items[0]->Text;
   }
  if (AMax && (AMax != FPlanCommCount))
   {
    ShowProgress(true);
    FPlanCommCount = AMax;
   }
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::FormDestroy(TObject * Sender)
 {
  if (FSettingNode)
   delete FSettingNode;
  if (FPlanDataSrc)
   delete FPlanDataSrc;
  if (FDetailDataSrc)
   delete FDetailDataSrc;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::RefreshPatData(TcxTreeListNode * ANode)
 {
  PlanTL->BeginUpdate();
  try
   {
    UnicodeString NotExecStr;
    UnicodeString MKBStr;
    TcxTreeListNode * tmpRow;
    TcxTreeListNode * SelNode = ANode;
    if (!SelNode)
     SelNode = GetSelNode();
    if (SelNode)
     {
      if (SelNode->Level == 3)
       SelNode = SelNode->Parent;
      if (SelNode->Level == 2)
       SelNode = SelNode->Parent;
      if (SelNode->Level == 1)
       { // пациент
        bool AllExec = true;
        int RowIdx;
        for (int mId = 0; mId < SelNode->Count; mId++)
         { // цикл по прививкам/пробам,проверкам
          tmpRow = SelNode->Items[mId];
          if (tmpRow->Data)
           {
            RowIdx = ((__int64)tmpRow->Data) - 1;
            FDS->GetDataById(VarToStr(FDS->Row[RowIdx]->Value["code"]), NULL); // .Обновляем
            tmpRow->Texts[plExecDateID] = FDS->Row[RowIdx]->Value["308D"]; // .    Дата
            tmpRow->Values[plExecId]    = FDS->Row[RowIdx]->Value["3098"];
            // 'Выполнение' uid='3098'  'Нет'='0', 'Да'='1', 'Частично'='2'
            tmpRow->Values[plSrcId] = FDS->Row[RowIdx]->Value["32FA"];
            // 'Источник плана' uid='32FA'// 'Плановая' = '1', 'Ручное добавление' = '2', 'Планировщик по календарю' = '3', 'Планировщик не по календарю' = '4', 'Планировщик как предварительная' = '5', 'Планировщик по туру' = '6', 'Планировщик по туру как предварительная' = '7'
            tmpRow->Values[plSupStateId] = FDS->Row[RowIdx]->Value["32FB"];
            // 'Статус обслуживания' uid='32FB'// 'Обслуживается по участку' = '1', 'Обслуживается по организации' = '2', 'Обслуживается по полису' = '3', 'Контролируется' = '4', 'Не обслуживается' = '6'
            tmpRow->Values[plTLPUId]      = FDS->Row[RowIdx]->Value["32FD"]; // 'Выполнена в данном ЛПУ' uid='32FD'
            tmpRow->Values[plNotExecType] = FDS->Row[RowIdx]->Value["32FC"];
            // 'Причина невыполнения' uid='32FC'// 'Без причины' = '1', 'Временно выбыл' = '2', 'Отвод' = '3', 'Отказ' = '4', 'Прочее' = '13'
            tmpRow->Values[plIllCodeId] = FDS->Row[RowIdx]->Value["32FF"]; // 'Заболевание' uid='32FF'
            tmpRow->Values[plNoMIBPId]  = FDS->Row[RowIdx]->Value["3308"]; // 'Отсутствует МИБП' uid='3308'
            NotExecStr                  = FDS->Row[RowIdx]->StrValue["3098"]; // . Выполнение
            if (((int)FDS->Row[RowIdx]->Value["3098"]) != 1)
             { // не выполнена
              if (((int)FDS->Row[RowIdx]->Value["32FC"] == 1) && ((int)FDS->Row[RowIdx]->Value["3308"]))
               { // 'Причина невыполнения' = Без причины' && Отсутствует МИБП'
                NotExecStr = FDS->Row[RowIdx]->StrValue["3308"];
               }
              else if ((int)FDS->Row[RowIdx]->Value["32FC"] > 1)
               { // 'Причина невыполнения' = {'Временно выбыл' = '2', 'Отвод' = '3', 'Отказ' = '4', 'Прочее' = '13'}
                NotExecStr = FDS->Row[RowIdx]->StrValue["32FC"];
                MKBStr     = FDM->OnGetMKBStr(VarToStr(FDS->Row[RowIdx]->Value["32FF"]));
                if (MKBStr.Length())
                 NotExecStr += "; -> " + MKBStr; // 'Заболевание'
               }
             }
            tmpRow->Texts[plNotExecOrMIBPID] = NotExecStr;
            AllExec &= (int)FDS->Row[RowIdx]->Value["3098"];
            tmpRow->Collapse(true);
           }
         }
        if (SelNode->Texts[plPatExecType] == "yes")
         { // было всё отмечено
          if (!AllExec)
           NECount++ ;
         }
        else if (SelNode->Texts[plPatExecType] == "no")
         { // было не всё отмечено
          if (AllExec)
           NECount-- ;
         }
        else
         { // был бардак :)
          if (!AllExec)
           NECount++ ;
         }
        SelNode->Texts[plPatExecType] = (AllExec) ? "yes" : "no";
        LastNEFindedNode          = NULL;
        NECur                     = 0;
        actFilterNotExec->Caption = IntToStr(NECur) + " / " + IntToStr(NECount);
        F63ShortViewer->LoadFromString(FDM->kabCDSGetF63(LastF63UCode));
       }
     }
   }
  __finally
   {
    CheckGroupVisible();
    PlanTL->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsPlanEditClientForm::GetBtnEn(UnicodeString AAttrName)
 {
  return ((bool)(FPlanInfNode->GetChildByName("description")->AV[AAttrName].Length()));
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actExitExecute(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::FormShow(TObject * Sender)
 {
  ShowTimer->Enabled = true;
 }
// ---------------------------------------------------------------------------
TcxTreeListNode * __fastcall TdsPlanEditClientForm::GetSelNode()
 {
  TcxTreeListNode * RC = NULL;
  try
   {
    if (PlanTL->SelectionCount)
     {
      RC = PlanTL->Selections[0];
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TkabCustomDataSetRow * __fastcall TdsPlanEditClientForm::CurRow()
 {
  TkabCustomDataSetRow * RC = NULL;
  TcxTreeListNode * tlNode = GetSelNode();
  try
   {
    if (tlNode && tlNode->Data)
     RC = FDS->Row[((__int64)tlNode->Data) - 1];
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actInsertPlanItemExecute(TObject * Sender)
 {
  ShowProgress(true);
  try
   {
   }
  __finally
   {
    ShowProgress(false);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actDeletePlanItemExecute(TObject * Sender)
 {
  ShowProgress(true);
  if (!actDeletePlanItem->Enabled)
   return;
  bool CanRefresh;
  TcxTreeListNode * FNode = GetSelNode();
  TcxTreeListNode * FPatNode = NULL;
  try
   {
    CanRefresh = false;
    try
     {
      UnicodeString Str = "";
      bool FFullDelete = false;
      if (CurRow())
       {
        FDS->GetDataById(VarToStr(CurRow()->Value["code"]), NULL); // .Обновляем
        if (FNode)
         {
          if (FNode->Level == 3)
           {
            if (FNode->Parent)
             FNode = FNode->Parent;
           }
         }
        if (FNode)
         {
          if (FNode->Level == 1)
           {
            FPatNode    = FNode;
            FFullDelete = true;
            Str         = FNode->Texts[plOrgFIOID];
           }
          else if (FNode->Level == 2)
           {
            if (FNode->Parent)
             {
              FPatNode = FNode->Parent;
              Str      = FNode->Texts[plMIBPInfID] + "\n" + FNode->Texts[plVacTypeID] + "\nдата плана: " +
                FNode->Texts[plPlanDateID] + "\n\n";
              Str += FNode->Parent->Texts[plOrgFIOID];
             }
           }
         }
       }
      if (Str.Length())
       {
        Str = FMT(icsRegErrorConfirmDeleteRec) + " удаление из плана\n\n" + Str + "?";
        if (MessageBox(Handle, Str.c_str(), cFMT(icsRegErrorConfirmDeleteRecMsgCaption),
          MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2) == IDYES)
         {
          if (FDM->OnDeletePlanItem)
           {
            UnicodeString DelRC = "error";
            if (FFullDelete)
             DelRC = FDM->OnDeletePlanItem(IntToStr(FPlanCode) + ".all", VarToStr(CurRow()->Value["317D"]));
            else
             DelRC = FDM->OnDeletePlanItem(IntToStr(FPlanCode) + ".item", VarToStr(CurRow()->Value["code"]));
            CanRefresh = SameText(DelRC, "ok");
            if (!CanRefresh && DelRC.Length())
             _MSG_ERR(DelRC, "Ошибка");
           }
         }
       }
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsRegErrorDeleteRecSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegErrorDeleteRec), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
    if (CanRefresh)
     {
      if (FDM->OnClassAfterDelete)
       FDM->OnClassAfterDelete(FPlanNode);
      if (FNode->Level > 1)
       {
        if (FPatNode)
         {
          if (FPatNode->Count == 1)
           FPatNode->Delete();
          else
           FNode->Delete(); // RefreshPatData(FNode);
         }
        else
         FNode->Delete(); // RefreshPatData(FNode);
       }
      else if (FPatNode)
       FPatNode->Delete();
      else
       actRefreshPlanExecute(actRefreshPlan);
     }
    ShowProgress(false);
   }
  ActiveControl = PlanTL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actCheckPlanItemExecute(TObject * Sender)
 {
  if (!actCheckPlanItem->Enabled)
   return;
  ShowProgress(true);
  TdsRegTemplateData * FTemplateData = new TdsRegTemplateData;
  try
   {
    if (CurRow())
     {
      try
       {
        __int64 FUCode = (__int64)CurRow()->Value["317D"];
        FDS->GetDataById(VarToStr(CurRow()->Value["code"]), NULL); // .Обновляем
        FPatBirthday = TDate((double)CurRow()->Value["32A5"]);
        if ((int)CurRow()->Value["3086"] == 1)
         { // Прививка
          if (FDM->OnPlanPrivInsert)
           {
            FTemplateData->Value["pldate"] = CurRow()->Value["3087"];
            FTemplateData->Value["32AE"]   = (__int64)CurRow()->Value["code"];
            FTemplateData->Value["017A"]   = CurRow()->Value["317D"];
            // <extedit name='Код пациента' uid='017A' ref='1000' depend='017A.017A' isedit='0' fieldtype='integer'/>
            FTemplateData->Value["1024"] = Date();
            // <date name='Дата выполнения' uid='1024' required='1' inlist='l' default='CurrentDate'/>
            FTemplateData->Value["1020"] = CurRow()->Value["3089"];
            // <choiceDB name='МИБП' uid='1020' ref='0035' required='1' inlist='l' depend='1075.002D'/>
            FTemplateData->Value["1021"] = CurRow()->Value["308B"];
            // <text name='Вид' uid='1021' required='1' inlist='l' cast='up' fixedvar='1' length='10'/>
            FDM->OnPlanPrivInsert(FUCode, FPatBirthday, FTemplateData, false);
           }
         }
        else if ((int)CurRow()->Value["3086"] == 2)
         { // Проба
          if (FDM->OnPlanTestInsert)
           {
            FTemplateData->Value["32AF"] = (__int64)CurRow()->Value["code"];
            FTemplateData->Value["017B"] = CurRow()->Value["317D"];
            // <extedit name='Код пациента' uid='017A' ref='1000' depend='017A.017A' isedit='0' fieldtype='integer'/>
            FTemplateData->Value["1031"] = Date();
            // <date name='Дата выполнения' uid='1024' required='1' inlist='l' default='CurrentDate'/>
            FTemplateData->Value["1032"] = CurRow()->Value["3089"];
            // <choiceDB name='МИБП' uid='1020' ref='0035' required='1' inlist='l' depend='1075.002D'/>
            FDM->OnPlanTestInsert(FUCode, FPatBirthday, FTemplateData, false);
           }
         }
        else if ((int)CurRow()->Value["3086"] == 3)
         { // Проверка
          if (FDM->OnPlanCheckInsert)
           {
            // 32B0
            // FTemplateData->Value["017B"] = CurRow()->Value["317D"]; // <extedit name='Код пациента' uid='017A' ref='1000' depend='017A.017A' isedit='0' fieldtype='integer'/>
            // FTemplateData->Value["1031"] = Date();                                      // <date name='Дата выполнения' uid='1024' required='1' inlist='l' default='CurrentDate'/>
            // FTemplateData->Value["1032"] = CurRow()->Value["3089"]; // <choiceDB name='МИБП' uid='1020' ref='0035' required='1' inlist='l' depend='1075.002D'/>
            FDM->OnPlanCheckInsert(FUCode, FPatBirthday, FTemplateData, false);
           }
         }
        RefreshPatData();
       }
      catch (Exception & E)
       {
        MessageBox(Handle, cFMT1(icsRegErrorInsertRecSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
       }
      catch (...)
       {
        MessageBox(Handle, cFMT(icsRegErrorInsertRec), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
       }
     }
   }
  __finally
   {
    delete FTemplateData;
    ShowProgress(false);
   }
  ActiveControl = PlanTL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actShowCardExecute(TObject * Sender)
 {
  ShowProgress(true);
  try
   {
    if (FDM->OnCardOpen && CurRow())
     {
      FDM->OnCardOpen((__int64)CurRow()->Value["317D"], FOnCardClose);
     }
   }
  __finally
   {
    ShowProgress(false);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::FOnCardClose(TObject * Sender)
 {
  TPatNodes::iterator FRC = FPatNodes.find((__int64)Sender);
  if (FRC != FPatNodes.end())
   RefreshPatData(FRC->second);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actPrintPlanExecute(TObject * Sender)
 {
  ShowProgress(true);
  try
   {
    if (FDM->OnPreviewDoc)
     {
      UnicodeString FPrintPlan = "<body>Ошибка печати плана</body>";
      if (FDM->OnPlanPrint)
       {
        FPrintPlan = FDM->OnPlanPrint(FPlanCode);
       }
      FDM->OnPreviewDoc(FPrintPlan, true);
     }
   }
  __finally
   {
    ShowProgress(false);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::GetPlanData()
 {
  try
   {
    FDS->GetData("{\"328F\":\"" + IntToStr(FPlanCode) + "\"}", TdsRegFetch::None, 200, FGetDataProgress);
   }
  catch (Exception & E)
   {
    _MSG_ERR(E.Message, "Ошибка получения данных");
    FDS->Clear();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actRefreshPlanExecute(TObject * Sender)
 {
  NECur               = NECount = CNECur = CNECount = 0;
  LastFindedNode      = NULL;
  LastNEFindedNode    = NULL;
  LastCNEFindedNode   = NULL;
  SpeedButton1->Down  = false;
  PlanTL->OnExpanding = NULL;
  PlanTL->BeginUpdate();
  PlanTL->Clear();
  FPatNodes.clear();
  ShowProgress(true);
  TAnsiStrMap tmpFltMap;
  try
   {
    GetPlanData();
    TcxTreeListNode * tmpOrgRow = NULL;
    TcxTreeListNode * tmpPatRow = NULL;
    TcxTreeListNode * tmpRow;
    UnicodeString NotExecStr;
    __int64 LVal = 0;
    UnicodeString FLev = "";
    UnicodeString FLevVal = "";
    UnicodeString MKBStr;
    TReplaceFlags fRfl;
    fRfl << rfReplaceAll;
    bool AllExec = true;
    for (int i = 0; i < FDS->RecordCount; i++)
     { // "com", "uch", "org"
      if (FPlanType == "org")
       {
        if (FLev != VarToStr(FDS->Row[i]->StrValue["3298"]).UpperCase())
         {
          tmpOrgRow                      = PlanTL->Root->AddChild();
          tmpOrgRow->CheckGroupType      = ncgCheckGroup;
          FLevVal                        = VarToStr(FDS->Row[i]->StrValue["3298"]);
          FLev                           = FLevVal.UpperCase();
          FLevVal                        = StringReplace(FLevVal, "$", "", fRfl).Trim();
          tmpOrgRow->Texts[plOrgFIOID]   = FLevVal;
          tmpOrgRow->Values[plPatientID] = FDS->Row[i]->Value["317D"]; // code Пациент
         }
       }
      if (!tmpOrgRow)
       tmpOrgRow = PlanTL->Root->AddChild();
      if (LVal != (__int64)FDS->Row[i]->Value["317D"])
       {
        if (tmpPatRow)
         {
          tmpPatRow->Texts[plPatExecType] = (AllExec) ? "yes" : "no";
          if (!AllExec)
           NECount++ ;
         }
        AllExec = true;
        tmpPatRow                       = tmpOrgRow->AddChild();
        tmpPatRow->CheckGroupType       = ncgCheckGroup;
        tmpPatRow->Texts[plPatExecType] = "notch";
        if (FLevVal.Length())
         tmpPatRow->Texts[plOrgFIOID] = VarToStr(FDS->Row[i]->StrValue["317D"]) + "  [ " + FLevVal + " ]";
        else
         tmpPatRow->Texts[plOrgFIOID] = VarToStr(FDS->Row[i]->StrValue["317D"]);
        tmpPatRow->Values[plPatientID]                 = FDS->Row[i]->Value["317D"]; // code Пациент
        FPatNodes[(__int64)FDS->Row[i]->Value["317D"]] = tmpPatRow;
       }
      LVal = (__int64)FDS->Row[i]->Value["317D"];
      if (!tmpPatRow)
       {
        tmpPatRow                       = tmpOrgRow->AddChild();
        tmpPatRow->Texts[plPatExecType] = "notch"; // Выполнение ->32A7 Выполнена другой вакциной
        AllExec                         = true;
       }
      tmpPatRow->Data = (TObject *)(i + 1);
      tmpRow = tmpPatRow->AddChild();
      tmpRow->AddChild();
      tmpRow->Data            = (TObject *)(i + 1);
      tmpRow->Texts[plTypeID] = FDS->Row[i]->Value["3086"];
      if (VarToStr(FDS->Row[i]->StrValue["3089"]).Trim().Length())
       {
        tmpRow->Texts[plMIBPInfID]               = FDS->Row[i]->StrValue["3089"]; // МИБП
        tmpFltMap[FDS->Row[i]->StrValue["3089"]] = "0:" + FDS->Row[i]->Value["3089"];
       }
      else
       {
        tmpRow->Texts[plMIBPInfID]               = FDS->Row[i]->StrValue["32AB"]; // Инфекция
        tmpFltMap[FDS->Row[i]->StrValue["32AB"]] = "1:" + FDS->Row[i]->Value["32AB"];
       }
      tmpRow->Texts[plVacTypeID] = FDS->Row[i]->StrValue["308B"]; // Вид
      tmpRow->Texts[plPlanDateID] = FDS->Row[i]->Value["3088"]; // Дата
      tmpRow->Texts[plAgeID]      = FDS->Row[i]->Value["32A8"]; // Возраст
      tmpRow->Texts[plExecDateID] = FDS->Row[i]->Value["308D"]; // Дата
      AllExec &= (int)FDS->Row[i]->Value["3098"];
      tmpRow->Values[plPatientID]  = FDS->Row[i]->Value["317D"]; // code Пациент
      tmpRow->Values[plMIBPCodeID] = FDS->Row[i]->Value["3089"]; // code МИБП
      tmpRow->Values[plInfCodeID]  = FDS->Row[i]->Value["32AB"]; // code Инфекция
      tmpRow->Values[plExecId]     = FDS->Row[i]->Value["3098"];
      // 'Выполнение' uid='3098'  'Нет'='0', 'Да'='1', 'Частично'='2'
      tmpRow->Values[plSrcId] = FDS->Row[i]->Value["32FA"];
      // 'Источник плана' uid='32FA'// 'Плановая' = '1', 'Ручное добавление' = '2', 'Планировщик по календарю' = '3', 'Планировщик не по календарю' = '4', 'Планировщик как предварительная' = '5', 'Планировщик по туру' = '6', 'Планировщик по туру как предварительная' = '7'
      tmpRow->Values[plSupStateId] = FDS->Row[i]->Value["32FB"];
      // 'Статус обслуживания' uid='32FB'// 'Обслуживается по участку' = '1', 'Обслуживается по организации' = '2', 'Обслуживается по полису' = '3', 'Контролируется' = '4', 'Не обслуживается' = '6'
      tmpRow->Values[plTLPUId]      = FDS->Row[i]->Value["32FD"]; // 'Выполнена в данном ЛПУ' uid='32FD'
      tmpRow->Values[plNotExecType] = FDS->Row[i]->Value["32FC"];
      // 'Причина невыполнения' uid='32FC'// 'Без причины' = '1', 'Временно выбыл' = '2', 'Отвод' = '3', 'Отказ' = '4', 'Прочее' = '13'
      tmpRow->Values[plIllCodeId] = FDS->Row[i]->Value["32FF"]; // 'Заболевание' uid='32FF'
      tmpRow->Values[plNoMIBPId]  = FDS->Row[i]->Value["3308"]; // 'Отсутствует МИБП' uid='3308'
      NotExecStr                  = FDS->Row[i]->StrValue["3098"];
      if (((int)FDS->Row[i]->Value["3098"]) != 1)
       { // не выполнена
        if (((int)FDS->Row[i]->Value["32FC"] == 1) && ((int)FDS->Row[i]->Value["3308"]))
         { // 'Причина невыполнения' = Без причины' && Отсутствует МИБП'
          NotExecStr = FDS->Row[i]->StrValue["3308"];
         }
        else if ((int)FDS->Row[i]->Value["32FC"] > 1)
         { // 'Причина невыполнения' = {'Временно выбыл' = '2', 'Отвод' = '3', 'Отказ' = '4', 'Прочее' = '13'}
          NotExecStr = FDS->Row[i]->StrValue["32FC"];
          MKBStr     = FDM->OnGetMKBStr(VarToStr(FDS->Row[i]->Value["32FF"]));
          if (MKBStr.Length())
           NotExecStr += "; -> " + MKBStr; // 'Заболевание'
         }
       }
      tmpRow->Texts[plNotExecOrMIBPID] = NotExecStr;
     }
    if (tmpPatRow)
     {
      if (tmpPatRow->Texts[plPatExecType] == "notch")
       {
        tmpPatRow->Texts[plPatExecType] = (AllExec) ? "yes" : "no";
        if (!AllExec)
         NECount++ ;
       }
     }
   }
  __finally
   {
    InfMIBPFilterChCB->Properties->Items->Clear();
    InfMIBPFilterChCB->Properties->Items->AddCheckItem("Выделить все", "Все значения");
    InfMIBPFilterChCB->States[InfMIBPFilterChCB->Properties->Items->Count - 1] = cbsChecked;
    TcxCheckComboBoxItem * fltItem;
    for (TAnsiStrMap::iterator i = tmpFltMap.begin(); i != tmpFltMap.end(); i++)
     {
      fltItem      = InfMIBPFilterChCB->Properties->Items->AddCheckItem(i->first, GetRPartB(i->second, ':'));
      fltItem->Tag = GetLPartB(i->second, ':').ToIntDef(0);
      InfMIBPFilterChCB->States[InfMIBPFilterChCB->Properties->Items->Count - 1] = cbsChecked;
      // TcxCheckBoxState = (cbsUnchecked, cbsChecked, cbsGrayed);
      // roperties->Items->AddCheckItem(i->first, GetRPartB(i->second, ':'));
     }
    actFilterNotExec->Caption = IntToStr(NECur) + " / " + IntToStr(NECount);
    Application->ProcessMessages();
    // TcxCheckComboBoxItem * chItem;
    // TcxTreeListNode * plNode;
    TcxTreeListNode * NodeL1, *NodeL2;
    int FCount = 0;
    for (int i = 0; i < PlanTL->Root->Count; i++)
     {
      NodeL1 = PlanTL->Root->Items[i];
      for (int j = 0; j < NodeL1->Count; j++)
       {
        NodeL2 = NodeL1->Items[j];
        for (int k = 0; k < NodeL2->Count; k++)
         FCount++ ;
       }
     }
    PrivCountLab->Caption = "Привикок/проб: " + IntToStr(FCount);
    PlanTL->EndUpdate();
    Application->ProcessMessages();
    PlanTL->Root->Expand(false);
    for (int i = 0; i < PlanTL->Root->Count; i++)
     {
      PlanTL->Root->Items[i]->Expand(false);
      for (int j = 0; j < PlanTL->Root->Items[i]->Count; j++)
       PlanTL->Root->Items[i]->Items[j]->Expand(false);
     }
    Application->ProcessMessages();
    PlanTL->OnExpanding = PlanTLExpanding;
    Application->ProcessMessages();
    ShowProgress(false);
    Application->ProcessMessages();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::PlanTLExpanding(TcxCustomTreeList * Sender, TcxTreeListNode * ANode,
  bool & Allow)
 {
  if (ANode->Level == 2)
   {
    try
     {
      UnicodeString FFilter;
      UnicodeString NotExecStr;
      UnicodeString MKBStr;
      FFilter = "\"318F\":\"" + VarToStr(FPlanCode) + "\"";
      FFilter += ",\"017D\":\"" + ANode->Texts[plPatientID] + "\"";
      FFilter += ",\"1086\":\"" + VarToStr(ANode->Values[plTypeID]) + "\"";
      if (ANode->Texts[plMIBPCodeID].ToIntDef(0))
       FFilter += ",\"1089\":\"" + ANode->Texts[plMIBPCodeID] + "\"";
      else
       FFilter += ",\"32A6\":\"" + ANode->Texts[plInfCodeID] + "\"";
      FFilter = "{" + FFilter + "}";
      FDetailDS->GetData(FFilter, TdsRegFetch::All, 200, FGetDataProgress);
      TcxTreeListNode * tmpRow;
      ANode->DeleteChildren();
      for (int i = 0; i < FDetailDS->RecordCount; i++)
       {
        tmpRow                      = ANode->AddChild();
        tmpRow->Data                = ANode->Data;
        tmpRow->Texts[plTypeID]     = FDetailDS->Row[i]->Value["1086"]; // . Tип
        tmpRow->Texts[plOrgFIOID]   = ANode->Texts[plOrgFIOID];
        tmpRow->Texts[plMIBPInfID]  = "    " + FDetailDS->Row[i]->StrValue["32A6"]; // . Инфекция
        tmpRow->Texts[plVacTypeID]  = FDetailDS->Row[i]->StrValue["108B"]; // .          Вид
        tmpRow->Texts[plPlanDateID] = FDetailDS->Row[i]->Value["1088"]; // .             Дата
        tmpRow->Texts[plAgeID]      = FDetailDS->Row[i]->Value["32A9"]; // .             Возраст
        tmpRow->Texts[plExecDateID] = FDetailDS->Row[i]->Value["108D"]; // .             Дата
        tmpRow->Values[plExecId] = FDetailDS->Row[i]->Value["1098"]; // 'Выполнение' uid='1098'  //'Нет'='0', 'Да'='1'
        tmpRow->Values[plSrcId] = FDetailDS->Row[i]->Value["3300"];
        // 'Источник плана' uid='3300'// 'Плановая' = '1', 'Ручное добавление' = '2', 'Планировщик по календарю' = '3', 'Планировщик не по календарю' = '4', 'Планировщик как предварительная' = '5', 'Планировщик по туру' = '6', 'Планировщик по туру как предварительная' = '7'
        tmpRow->Values[plSupStateId] = FDetailDS->Row[i]->Value["3301"];
        // 'Статус обслуживания' uid='3301'// 'Обслуживается по участку' = '1', 'Обслуживается по организации' = '2', 'Обслуживается по полису' = '3', 'Контролируется' = '4', 'Не обслуживается' = '6'
        tmpRow->Values[plTLPUId]      = FDetailDS->Row[i]->Value["3302"]; // 'Выполнена в данном ЛПУ' uid='3302'
        tmpRow->Values[plNotExecType] = FDetailDS->Row[i]->Value["3303"];
        // 'Причина невыполнения' uid='3303'// 'Без причины' = '1', 'Временно выбыл' = '5', 'Заболевание' = '0', 'Карантин' = '2', 'Отказ от прививки/пробы' = '3', 'Прививается вне поликлиники' = '6', 'Отказ от обслуживания в поликлинике' = '7', 'Обследование' = '8', 'Лечение в стационаре' = '9', 'Распоряжение администрации' = '4', 'Мать носитель Гепатита В' = '10', 'Мать носитель Гепатита С' = '11', 'R-75' = '12', 'Прочее' = '13'
        tmpRow->Values[plIllCodeId] = FDetailDS->Row[i]->Value["3304"]; // 'Заболевание' uid='3304'
        tmpRow->Values[plNoMIBPId]  = FDetailDS->Row[i]->Value["3307"]; // 'Отсутствует МИБП' uid='3307'
        NotExecStr                  = FDetailDS->Row[i]->StrValue["1098"];
        if (((int)FDetailDS->Row[i]->Value["1098"]) != 1)
         { // не выполнена
          if (((int)FDetailDS->Row[i]->Value["3303"] == 1) && ((int)FDetailDS->Row[i]->Value["3307"]))
           { // 'Причина невыполнения' = Без причины' && Отсутствует МИБП'
            NotExecStr = FDetailDS->Row[i]->StrValue["3307"];
           }
          else if ((int)FDetailDS->Row[i]->Value["3303"] != 1)
           { // 'Временно выбыл' = '5', 'Заболевание' = '0', 'Карантин' = '2', 'Отказ от прививки/пробы' = '3', 'Прививается вне поликлиники' = '6', 'Отказ от обслуживания в поликлинике' = '7', 'Обследование' = '8', 'Лечение в стационаре' = '9', 'Распоряжение администрации' = '4', 'Мать носитель Гепатита В' = '10', 'Мать носитель Гепатита С' = '11', 'R-75' = '12', 'Прочее' = '13'
            NotExecStr = FDetailDS->Row[i]->StrValue["3303"];
            MKBStr     = FDM->OnGetMKBStr(VarToStr(FDetailDS->Row[i]->Value["3304"]));
            if (MKBStr.Length())
             NotExecStr += "; -> " + MKBStr; // 'Заболевание'
           }
          tmpRow->Texts[plNotExecOrMIBPID] = NotExecStr;
         }
        else
         tmpRow->Texts[plNotExecOrMIBPID] = FDetailDS->Row[i]->StrValue["32AA"]; // .          МИБП";
       }
      Application->ProcessMessages();
     }
    __finally
     {
      ShowProgress(false);
     }
   }
  Allow = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::ShowTimerTimer(TObject * Sender)
 {
  ShowTimer->Enabled = false;
  Application->ProcessMessages();
  actRefreshPlanExecute(actRefreshPlan);
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsPlanEditClientForm::FDSGetValById(UnicodeString AId, UnicodeString ARecId)
 {
  Application->ProcessMessages();
  FGetDataProgress(FPlanCommCount, FPlanCurr++ , "");
  return FDM->kabCDSGetValById(AId, ARecId);
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsPlanEditClientForm::FDSGetValById10(UnicodeString AId, UnicodeString ARecId)
 {
  Application->ProcessMessages();
  FGetDataProgress(FPlanCommCount, FPlanCurr++ , "");
  return FDM->kabCDSGetValById10(AId, ARecId);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::PlanGridViewDataControllerDataChanged(TObject * Sender)
 {
  ShowProgress(false);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::ShowProgress(bool AShow)
 {
  if (AShow)
   {
    ProgressPanel->Width   = Width;
    ProgressPanel->Height  = ClientHeight - StatusSB->Height;
    ProgressPanel->Left    = 0;
    ProgressPanel->Top     = 0;
    ProgressPanel->Visible = true;
    ProgressPanel->BringToFront();
    ProgressPanelMsg->Left    = 3;
    ProgressPanelMsg->Top     = ProgressPanel->Height / 2 - ProgressPanelMsg->Height / 2;
    ProgressPanelMsg->Width   = ProgressPanel->Width - 6;
    ProgressPanelMsg->Visible = true;
    ProgressPanelMsg->BringToFront();
   }
  else
   {
    FPlanCurr                 = 0;
    FPlanCommCount            = 0;
    ProgressPanel->Width      = 10;
    ProgressPanel->Height     = 10;
    ProgressPanel->Visible    = false;
    ProgressPanelMsg->Visible = false;
   }
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::PlanTLIsGroupNode(TcxCustomTreeList * Sender, TcxTreeListNode * ANode,
  bool & IsGroup)
 {
  IsGroup = true;
  if (ANode && (ANode->Level > 1))
   {
    IsGroup = false;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::PlanTLCollapsing(TcxCustomTreeList * Sender, TcxTreeListNode * ANode,
  bool & Allow)
 {
  Allow = !(ANode && (ANode->Level == 1));
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::PlanTLStylesGetBandContentStyle(TcxCustomTreeList * Sender,
  TcxTreeListBand * ABand, TcxTreeListNode * ANode, TcxStyle *& AStyle)
 {
  if (ABand && ANode && (ABand->Position->ColIndex == 1))
   {
    if (ANode->Texts[plExecId].UpperCase() == "1")
     AStyle = execStyle;
    else if ((int)ANode->Values[plNotExecType] != 1)
     AStyle = otvStyle;
    else
     AStyle = doExecStyle;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::PlanTLStylesGetContentStyle(TcxCustomTreeList * Sender,
  TcxTreeListColumn * AColumn, TcxTreeListNode * ANode, TcxStyle *& AStyle)
 {
  if (AColumn && ANode && (AColumn->Position->ColIndex == 1) && (AColumn->Position->BandIndex == 0))
   {
    if (ANode->Level == 1)
     AStyle = mibpStyle; // фио
    else if (ANode->Level == 2)
     AStyle = mibpStyle; // вакцина,инфекция
    else if (ANode->Level == 3)
     AStyle = infStyle; // инфекция
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::PlanTLFocusedNodeChanged(TcxCustomTreeList * Sender,
  TcxTreeListNode * APrevFocusedNode, TcxTreeListNode * AFocusedNode)
 {
  actDeletePlanItem->Enabled = false;
  actCheckPlanItem->Enabled  = false;
  actShowCard->Enabled       = false;
  if (CurRow())
   {
    actDeletePlanItem->Enabled = FDM->PlanEditable;
    actCheckPlanItem->Enabled  = VarToStr(CurRow()->Value["3098"]).ToIntDef(0) != 1;
    actShowCard->Enabled       = true;
    TcxTreeListNode * SelNode = AFocusedNode;
    if (!SelNode)
     SelNode = GetSelNode();
    if (SelNode)
     {
      if (SelNode->Level == 3)
       SelNode = SelNode->Parent;
      if (SelNode->Level == 2)
       SelNode = SelNode->Parent;
      if (SelNode->Level == 1)
       {
        if ((LastF63UCode.UpperCase() != SelNode->Texts[plPatientID].UpperCase()))
         {
          LastF63UCode = SelNode->Texts[plPatientID];
          F63ShortViewer->LoadFromString(FDM->kabCDSGetF63(LastF63UCode));
         }
       }
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actFindExecute(TObject * Sender)
 {
  if (SearchBE->Text.Trim().Length() && (SearchBE->Text.Trim().UpperCase() != "ПОИСК"))
   {
    SearchTimer->Enabled = false;
    PlanTL->BeginUpdate();
    try
     {
      bool LastExist = LastFindedNode;
      // TcxTreeListFindMode = (tlfmNormal, tlfmLike, tlfmExact);
      // FindNodeByText(const AText: string; AColumn: TcxTreeListColumn; AStartNode: TcxTreeListNode = nil; AExpandedOnly: Boolean = False; AForward: Boolean = True; ACaseSensitive: Boolean = True; AMode: TcxTreeListFindMode = tlfmNormal; ALikeParams: TcxTreeListLikeParams = nil; AIgnoreStartNode: Boolean = False): TcxTreeListNode;
      LastFindedNode = PlanTL->FindNodeByText(SearchBE->Text.Trim() + "%", FIOCol, LastFindedNode, false, true, false,
        tlfmLike, NULL, true);
      if (LastFindedNode)
       {
        PlanTL->SetFocus();
        LastFindedNode->Focused = true;
        LastFindedNode->MakeVisible();
        PlanTL->TopVisibleNode = LastFindedNode;
       }
      else
       {
        if (LastExist)
         {
          LastFindedNode = PlanTL->FindNodeByText(SearchBE->Text.Trim(), FIOCol, LastFindedNode, false, true, false,
            tlfmNormal, NULL, true);
          if (LastFindedNode)
           {
            PlanTL->SetFocus();
            LastFindedNode->Focused = true;
            LastFindedNode->MakeVisible();
            PlanTL->TopVisibleNode = LastFindedNode;
           }
         }
        else
         _MSG_INF("\"" + SearchBE->Text.Trim() + "\" в плане не найден(а).", "Сообщение");
       }
     }
    __finally
     {
      PlanTL->EndUpdate();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actFilterExecute(TObject * Sender)
 {
  //
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actClearSearchExecute(TObject * Sender)
 {
  SearchBE->Text = "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::SearchBEPropertiesChange(TObject * Sender)
 {
  LastFindedNode = NULL;
  if (SearchBE->Text.Trim().Length())
   SearchTimer->Enabled = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::SearchTimerTimer(TObject * Sender)
 {
  SearchTimer->Enabled = false;
  actFindExecute(actFind);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::SpeedButton1Click(TObject * Sender)
 {
  ShowMessage(IntToStr((int)SpeedButton1->Down));
  // SpeedButton1->Down = !SpeedButton1->Down;
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::cxButton2Click(TObject * Sender)
 {
  ShowMessage(IntToStr(SpeedButton1->GroupIndex));
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actFilterNotExecExecute(TObject * Sender)
 {
  PlanTL->BeginUpdate();
  try
   {
    for (int i = 0; i < PlanTL->Root->Count; i++)
     {
      for (int j = 0; j < PlanTL->Root->Items[i]->Count; j++)
       {
        PlanTL->Root->Items[i]->Items[j]->Visible = (SpeedButton1->Down) ?
          (PlanTL->Root->Items[i]->Items[j]->Texts[plPatExecType] == "no") : true;
       }
     }
   }
  __finally
   {
    CheckGroupVisible();
    PlanTL->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actSelectNotExecExecute(TObject * Sender)
 {
  PlanTL->BeginUpdate();
  try
   {
    // bool LastExist = LastNEFindedNode;
    // TcxTreeListFindMode = (tlfmNormal, tlfmLike, tlfmExact);
    // FindNodeByText(const AText: string; AColumn: TcxTreeListColumn; AStartNode: TcxTreeListNode = nil; AExpandedOnly: Boolean = False; AForward: Boolean = True; ACaseSensitive: Boolean = True; AMode: TcxTreeListFindMode = tlfmNormal; ALikeParams: TcxTreeListLikeParams = nil; AIgnoreStartNode: Boolean = False): TcxTreeListNode;
    LastNEFindedNode = PlanTL->FindNodeByText("no", ExecMIBPCol, GetSelNode(), false, true, false, tlfmNormal,
      NULL, true);
    if (LastNEFindedNode)
     {
      PlanTL->SetFocus();
      LastNEFindedNode->Focused = true;
      LastNEFindedNode->MakeVisible();
      PlanTL->TopVisibleNode = LastNEFindedNode;
      NECur++ ;
     }
    else
     {
      // if (LastExist)
      // {
      // LastNEFindedNode = PlanTL->FindNodeByText("no", ExecMIBPCol, LastNEFindedNode, false, true, false, tlfmNormal, NULL, true);
      // if (LastNEFindedNode)
      // {
      // PlanTL->SetFocus();
      // LastNEFindedNode->Focused = true;
      // LastNEFindedNode->MakeVisible();
      // PlanTL->TopVisibleNode = LastNEFindedNode;
      // NECur++ ;
      // }
      // }
      // else
      NECur = 0;
      // _MSG_INF("\"" + SearchBE->Text.Trim() + "\" в плане не найден(а).", "Сообщение");
     }
   }
  __finally
   {
    CheckGroupVisible();
    PlanTL->EndUpdate();
   }
  actFilterNotExec->Caption = IntToStr(NECur) + " / " + IntToStr(NECount);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actFilterCancelNotExecExecute(TObject * Sender)
 {
  //
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actSelectCancelNotExecExecute(TObject * Sender)
 {
  PlanTL->BeginUpdate();
  try
   {
    bool LastExist = LastCNEFindedNode;
    // TcxTreeListFindMode = (tlfmNormal, tlfmLike, tlfmExact);
    // FindNodeByText(const AText: string; AColumn: TcxTreeListColumn; AStartNode: TcxTreeListNode = nil; AExpandedOnly: Boolean = False; AForward: Boolean = True; ACaseSensitive: Boolean = True; AMode: TcxTreeListFindMode = tlfmNormal; ALikeParams: TcxTreeListLikeParams = nil; AIgnoreStartNode: Boolean = False): TcxTreeListNode;
    LastCNEFindedNode = PlanTL->FindNodeByText("%]%" + SearchBE->Text.Trim() + "%", FIOCol, LastCNEFindedNode, false,
      true, false, tlfmLike, NULL, true);
    if (LastCNEFindedNode)
     {
      PlanTL->SetFocus();
      LastCNEFindedNode->Focused = true;
      LastCNEFindedNode->MakeVisible();
      PlanTL->TopVisibleNode = LastCNEFindedNode;
      CNECur++ ;
     }
    else
     {
      if (LastExist)
       {
        LastCNEFindedNode = PlanTL->FindNodeByText(SearchBE->Text.Trim(), FIOCol, LastCNEFindedNode, false, true, false,
          tlfmNormal, NULL, true);
        if (LastCNEFindedNode)
         {
          PlanTL->SetFocus();
          LastCNEFindedNode->Focused = true;
          LastCNEFindedNode->MakeVisible();
          PlanTL->TopVisibleNode = LastCNEFindedNode;
          CNECur++ ;
         }
       }
      else
       CNECur = 0;
     }
   }
  __finally
   {
    CheckGroupVisible();
    PlanTL->EndUpdate();
   }
  actFilterCancelNotExec->Caption = IntToStr(CNECur) + " / " + IntToStr(CNECount);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::actGroupCheckExecute(TObject * Sender)
 {
  TdsPlanGroupCheckForm * Dlg = new TdsPlanGroupCheckForm(this, FDM, FPlanCode, Caption, FPlanType);
  try
   {
    try
     {
      Dlg->ShowModal();
     }
    catch (...)
     {
      int s = 1;
     }
   }
  __finally
   {
    delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::InfMIBPFilterChCBPropertiesClickCheck(TObject * Sender, int ItemIndex,
  bool & AllowToggle)
 {
  InfMIBPFilter(ItemIndex);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::InfMIBPFilter(int ItemIndex)
 {
  PlanTL->BeginUpdate();
  TcxCheckComboBoxItem * chItem;
  TcxTreeListNode * plNode;
  TcxTreeListNode * NodeL1, *NodeL2;
  int FCount = 0;
  try
   {
    if (!ItemIndex)
     {
      for (int i = 1; i < InfMIBPFilterChCB->Properties->Items->Count; i++)
       InfMIBPFilterChCB->States[i] = !InfMIBPFilterChCB->States[ItemIndex];
     }
    for (int i = 0; i < PlanTL->Root->Count; i++)
     {
      NodeL1 = PlanTL->Root->Items[i];
      for (int j = 0; j < NodeL1->Count; j++)
       {
        NodeL2 = NodeL1->Items[j];
        for (int k = 0; k < NodeL2->Count; k++)
         {
          chItem = InfMIBPFilterChCB->Properties->Items->Items[ItemIndex];
          plNode = NodeL2->Items[k];
          if (!chItem->Tag)
           {
            if (!ItemIndex) // прививки
               plNode->Visible = (InfMIBPFilterChCB->States[ItemIndex] != cbsChecked);
            else
             {
              if (plNode->Texts[plMIBPCodeID].ToIntDef(0) == chItem->ShortDescription.ToIntDef(0))
               plNode->Visible = (InfMIBPFilterChCB->States[ItemIndex] != cbsChecked);
             }
            if (plNode->Visible)
             FCount++ ;
           }
          else // пробы
           {
            if (!ItemIndex)
             plNode->Visible = (InfMIBPFilterChCB->States[ItemIndex] != cbsChecked);
            else
             {
              if (plNode->Texts[plInfCodeID].ToIntDef(0) == chItem->ShortDescription.ToIntDef(0))
               plNode->Visible = (InfMIBPFilterChCB->States[ItemIndex] != cbsChecked);
             }
            if (plNode->Visible)
             FCount++ ;
           }
         }
       }
     }
   }
  __finally
   {
    CheckGroupVisible();
    PlanTL->EndUpdate();
    PrivCountLab->Caption = "Привикок/проб: " + IntToStr(FCount);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanEditClientForm::CheckGroupVisible()
 {
  TcxTreeListNode * plNode;
  TcxTreeListNode * NodeL1, *NodeL2;
  bool vis1, vis2;
  for (int i = 0; i < PlanTL->Root->Count; i++)
   {
    NodeL1 = PlanTL->Root->Items[i];
    vis1   = false;
    for (int j = 0; j < NodeL1->Count; j++)
     {
      NodeL2 = NodeL1->Items[j];
      vis2   = false;
      for (int k = 0; k < NodeL2->Count; k++)
       vis2 |= NodeL2->Items[k]->Visible;
      NodeL2->Visible = vis2;
      vis1 |= vis2;
     }
    NodeL1->Visible = vis1;
   }
 }
// ---------------------------------------------------------------------------
