﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AddPauseUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TAddPauseForm *AddPauseForm;
//---------------------------------------------------------------------------
__fastcall TAddPauseForm::TAddPauseForm(TComponent* Owner, TdsICPlanDM *ADM)
 : TForm(Owner)
{
  FDM = ADM;
}
//---------------------------------------------------------------------------
void __fastcall TAddPauseForm::Type1CBPropertiesChange(TObject *Sender)
{
  TStringList *FItemList = new TStringList;
  try
   {
     ItemsList1CB->Properties->Items->Clear();
     switch (Type1CB->ItemIndex)
     {
       case 0: {FDM->LoadInfList(FItemList); break;}
       case 1: {FDM->LoadTestList(FItemList); break;}
       case 2: {FDM->LoadCheckList(FItemList); break;}
     }

     FItemList->Sort();
     for (int i = 0; i < FItemList->Count; i++)
      {
        ItemsList1CB->Properties->Items->AddObject(FItemList->Strings[i], FItemList->Objects[i]);
      }
   }
  __finally
   {
     delete FItemList;
     CheckCtrl();
   }
}
//---------------------------------------------------------------------------
void __fastcall TAddPauseForm::Type2CBPropertiesChange(TObject *Sender)
{
  TStringList *FItemList = new TStringList;
  try
   {
     ItemsList2CB->Properties->Items->Clear();
     switch (Type2CB->ItemIndex)
     {
       case 0: {FDM->LoadInfList(FItemList); break;}
       case 1: {FDM->LoadTestList(FItemList); break;}
       case 2: {FDM->LoadCheckList(FItemList); break;}
     }

     FItemList->Sort();
     for (int i = 0; i < FItemList->Count; i++)
      {
        ItemsList2CB->Properties->Items->AddObject(FItemList->Strings[i], FItemList->Objects[i]);
      }
   }
  __finally
   {
     delete FItemList;
     CheckCtrl();
   }
}
//---------------------------------------------------------------------------
void __fastcall TAddPauseForm::CheckCtrl()
{
  ApplyBtn->Enabled = (Type1CB->ItemIndex != -1)&&
                      (ItemsList1CB->ItemIndex != -1)&&
                      (Type2CB->ItemIndex != -1)&&
                      (ItemsList2CB->ItemIndex != -1)&&
                      ValED->Text.Length();
}
//---------------------------------------------------------------------------
void __fastcall TAddPauseForm::ItemsList1CBPropertiesChange(TObject *Sender)
{
  CheckCtrl();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddPauseForm::FGetType1()
{
  UnicodeString RC = "";
  if (Type1CB->ItemIndex != -1)
   {
     switch(Type1CB->ItemIndex)
     {
       case 0: {RC = "i"; break;};
       case 1: {RC = "t"; break;};
       case 2: {RC = "c"; break;};
     }
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddPauseForm::FGetCode1()
{
  UnicodeString RC = "";
  if (ItemsList1CB->ItemIndex != -1)
   {
     RC = IntToStr((int)ItemsList1CB->Properties->Items->Objects[ItemsList1CB->ItemIndex]);
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddPauseForm::FGetType2()
{
  UnicodeString RC = "";
  if (Type2CB->ItemIndex != -1)
   {
     switch(Type2CB->ItemIndex)
     {
       case 0: {RC = "i"; break;};
       case 1: {RC = "t"; break;};
       case 2: {RC = "c"; break;};
     }
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddPauseForm::FGetCode2()
{
  UnicodeString RC = "";
  if (ItemsList2CB->ItemIndex != -1)
   {
     RC = IntToStr((int)ItemsList2CB->Properties->Items->Objects[ItemsList2CB->ItemIndex]);
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddPauseForm::FGetValue()
{
  return ValED->Text.Trim();
}
//---------------------------------------------------------------------------

