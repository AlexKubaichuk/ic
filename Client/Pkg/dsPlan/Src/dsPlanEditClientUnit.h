﻿//---------------------------------------------------------------------------
#ifndef dsPlanEditClientUnitH
#define dsPlanEditClientUnitH

//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include "cxButtonEdit.hpp"
#include "cxMaskEdit.hpp"
#include "cxCheckBox.hpp"
#include "cxDropDownEdit.hpp"
#include "cxButtons.hpp"
#include "cxGroupBox.hpp"
#include <Vcl.StdCtrls.hpp>
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxImage.hpp"
#include "cxImageComboBox.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxSplitter.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"
#include "dxGDIPlusClasses.hpp"
#include "dxStatusBar.hpp"
#include "Htmlview.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "dsICPlanDMUnit.h"
#include "KabCustomDS.h"
#include <Vcl.Buttons.hpp>
#include "cxCalendar.hpp"
#include "cxCheckComboBox.hpp"
//---------------------------------------------------------------------------
/*

#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxImage.hpp"
#include "cxImageComboBox.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxPC.hpp"
#include "cxSplitter.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxGDIPlusClasses.hpp"
#include "dxStatusBar.hpp"
#include "Htmlview.hpp"


#include "cxTextEdit.hpp"
#include "dxBar.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxmdaset.hpp"
#include "dxStatusBar.hpp"
#include "Htmlview.hpp"
//---------------------------------------------------------------------------
#include "cxImage.hpp"
#include "dxGDIPlusClasses.hpp"
#include <Vcl.StdCtrls.hpp>
#include "cxInplaceContainer.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include <Vcl.ComCtrls.hpp>
*/
class PACKAGE TdsPlanEditClientForm : public TForm
{
__published:	// IDE-managed Components
 TdxBarManager *PlanTBM;
 TcxStyleRepository *Style;
        TdxStatusBar *StatusSB;
        TActionList *ClassAL;
        TAction *actExit;
  TdxBarLargeButton *dxBarLargeButton5;
  TcxImageList *LClassIL32;
  TcxImageList *ClassIL;
 TdxBarPopupMenu *PlanPM;
  TPanel *CompPanel;
  TcxSplitter *Split;
 TdxBarLargeButton *dxBarLargeButton1;
 TdxBarLargeButton *dxBarLargeButton6;
 TAction *actInsertPlanItem;
 TAction *actDeletePlanItem;
 TAction *actCheckPlanItem;
 TAction *actShowCard;
 TAction *actPrintPlan;
 TAction *actRefreshPlan;
 TdxBarLargeButton *dxBarLargeButton7;
 TdxBarLargeButton *dxBarLargeButton8;
 TdxBarLargeButton *dxBarLargeButton11;
 TTimer *ShowTimer;
 TcxStyle *masterContentStyle;
 TcxStyle *detailContentStyle;
 TcxImageList *ItemTypeIL;
 TcxStyle *vacStyle;
 TcxStyle *cxStyle1;
 TcxImage *ProgressPanel;
 TPanel *ProgressPanelMsg;
 TcxStyle *cxStyle2;
 TcxStyle *cxStyle3;
 TcxStyle *mibpStyle;
 TcxStyle *fioStyle;
 TcxStyle *execStyle;
 TcxStyle *doExecStyle;
 TcxStyle *infStyle;
 TcxTreeList *PlanTL;
 TcxTreeListColumn *FIOCol;
 TcxTreeListColumn *MIBPInfCol;
 TcxTreeListColumn *VacTypeCol;
 TcxTreeListColumn *PlanDateCol;
 TcxTreeListColumn *AgeCol;
 TcxTreeListColumn *ExecDateCol;
 TcxTreeListColumn *ExecMIBPCol;
 TcxTreeListColumn *PtIdCol;
 TcxTreeListColumn *MIBPCodeCol;
 TcxTreeListColumn *InfCodeCol;
 TcxTreeListColumn *ResCol;
 TcxTreeListColumn *cxTreeList1Column15;
 TcxTreeListColumn *cxTreeList1Column16;
 TcxTreeListColumn *cxTreeList1Column17;
 TcxTreeListColumn *cxTreeList1Column18;
 TcxImageList *SearchIL;
 TcxButtonEdit *SearchBE;
 TAction *actFind;
 TAction *actFilter;
 TAction *actClearSearch;
 TAction *actFilterNotExec;
 TTimer *SearchTimer;
 THTMLViewer *F63ShortViewer;
 TPanel *PlanAnalizPanel;
 TcxGroupBox *cxGroupBox1;
 TcxButton *cxButton2;
 TcxGroupBox *cxGroupBox2;
 TcxButton *cxButton4;
 TSpeedButton *SpeedButton1;
 TSpeedButton *SpeedButton2;
 TdxBar *PlanTBMBar1;
 TdxBarLargeButton *dxBarLargeButton2;
 TdxBarLargeButton *dxBarLargeButton3;
 TdxBarLargeButton *dxBarLargeButton4;
 TAction *actSelectNotExec;
 TAction *actFilterCancelNotExec;
 TAction *actSelectCancelNotExec;
 TcxTreeListColumn *PlanTLColumn1;
 TcxTreeListColumn *PlanTLColumn2;
 TcxTreeListColumn *PlanTLColumn3;
 TcxStyle *otvStyle;
 TdxBarButton *dxBarButton1;
 TAction *actGroupCheck;
 TdxBarLargeButton *dxBarLargeButton9;
 TPanel *Panel1;
 TcxCheckComboBox *InfMIBPFilterChCB;
 TLabel *PrivCountLab;
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall actExitExecute(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
 void __fastcall actInsertPlanItemExecute(TObject *Sender);
 void __fastcall actDeletePlanItemExecute(TObject *Sender);
 void __fastcall actCheckPlanItemExecute(TObject *Sender);
 void __fastcall actShowCardExecute(TObject *Sender);
 void __fastcall actPrintPlanExecute(TObject *Sender);
 void __fastcall actRefreshPlanExecute(TObject *Sender);
 void __fastcall ShowTimerTimer(TObject *Sender);
 void __fastcall PlanGridViewDataControllerDataChanged(TObject *Sender);
 void __fastcall PlanTLExpanding(TcxCustomTreeList *Sender, TcxTreeListNode *ANode,
          bool &Allow);
 void __fastcall PlanTLIsGroupNode(TcxCustomTreeList *Sender, TcxTreeListNode *ANode,
          bool &IsGroup);
 void __fastcall PlanTLCollapsing(TcxCustomTreeList *Sender, TcxTreeListNode *ANode,
          bool &Allow);
 void __fastcall PlanTLStylesGetBandContentStyle(TcxCustomTreeList *Sender, TcxTreeListBand *ABand,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
 void __fastcall PlanTLStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
 void __fastcall PlanTLFocusedNodeChanged(TcxCustomTreeList *Sender, TcxTreeListNode *APrevFocusedNode,
          TcxTreeListNode *AFocusedNode);
 void __fastcall actFindExecute(TObject *Sender);
 void __fastcall actFilterExecute(TObject *Sender);
 void __fastcall actClearSearchExecute(TObject *Sender);
 void __fastcall SearchBEPropertiesChange(TObject *Sender);
 void __fastcall SearchTimerTimer(TObject *Sender);
 void __fastcall SpeedButton1Click(TObject *Sender);
 void __fastcall cxButton2Click(TObject *Sender);
 void __fastcall actFilterNotExecExecute(TObject *Sender);
 void __fastcall actSelectNotExecExecute(TObject *Sender);
 void __fastcall actFilterCancelNotExecExecute(TObject *Sender);
 void __fastcall actSelectCancelNotExecExecute(TObject *Sender);
 void __fastcall actGroupCheckExecute(TObject *Sender);
 void __fastcall InfMIBPFilterChCBPropertiesClickCheck(TObject *Sender, int ItemIndex,
          bool &AllowToggle);


private:	// User declarations
  TdsICPlanDM *FDM;
  TkabCustomDataSet* FDS;
  TkabCustomDataSet* FDetailDS;
  TkabCustomDataSource *FPlanDataSrc;
  TkabCustomDataSource * FDetailDataSrc;

  UnicodeString FPlanType;
  typedef map<__int64, TcxTreeListNode*> TPatNodes;

  TTagNode *FPlanInfNode;
  TTagNode *FPlanNode;
  TPatNodes FPatNodes;

//  __int64 FUCode;
  TcxTabSheet *FCurPage;

  TTagNode *FSettingNode;
  TcxTreeListNode *LastFindedNode;
  TcxTreeListNode *LastNEFindedNode;
  TcxTreeListNode *LastCNEFindedNode;
  int NECur, NECount;
  int CNECur, CNECount;

  TDate FPatBirthday;
        bool SaveEnInsert;
        bool SaveEnEdit;
        bool SaveEnDelete;
        bool SaveEnSetTemplate;
        bool SaveEnReSetTemplate;
        bool SaveEnRefresh;
        bool SaveEnFind;
        bool SaveEnFindNext;
        bool SaveEnViewClassParam;
        bool SaveEnViewTemplateParam;

    UnicodeString    SaveCaption;
  UnicodeString    LastF63UCode;
    __int64   FPlanCode, curCD,xpCount,xcTop;

    __int64   FPlanCommCount, FPlanCurr;
    int FLastTop,FLastHeight;

  void __fastcall InfMIBPFilter(int ItemIndex);
  TkabCustomDataSource* __fastcall CreateDataSrc(TTagNode *ADefNode);
  void __fastcall FGetDataProgress(int AMax, int ACur, UnicodeString AMsg);
  void __fastcall RefreshPatData(TcxTreeListNode * ANode = NULL);
  void __fastcall GetPlanData();
  TcxTreeListNode* __fastcall GetSelNode();
  TkabCustomDataSetRow * __fastcall CurRow();
    bool __fastcall GetBtnEn(UnicodeString AAttrName);
    void __fastcall CreateSrc();
  TJSONObject * __fastcall FDSGetValById(UnicodeString AId, UnicodeString ARecId);
  TJSONObject * __fastcall FDSGetValById10(UnicodeString AId, UnicodeString ARecId);
  void __fastcall ShowProgress(bool AShow);
  void __fastcall FOnCardClose(TObject * Sender);
  void __fastcall CheckGroupVisible();
public:		// User declarations
    __fastcall TdsPlanEditClientForm(TComponent* Owner, TdsICPlanDM *ADM, __int64 APlanCode, UnicodeString APlanCapt, UnicodeString APlanType);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsPlanEditClientForm *dsPlanEditClientForm;
//---------------------------------------------------------------------------
#endif
