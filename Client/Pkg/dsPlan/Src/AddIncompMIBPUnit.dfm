object AddIncompMIBPForm: TAddIncompMIBPForm
  Left = 0
  Top = 0
  Caption = #1053#1077#1089#1086#1074#1084#1077#1089#1090#1080#1084#1099#1077' '#1052#1048#1041#1055
  ClientHeight = 118
  ClientWidth = 341
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 77
    Width = 341
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      341
      41)
    object CancelBtn: TButton
      Left = 237
      Top = 6
      Width = 100
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 0
    end
    object ApplyBtn: TButton
      Left = 131
      Top = 6
      Width = 100
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      Enabled = False
      ModalResult = 1
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 39
    Width = 335
    Height = 30
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 18
      Height = 21
      Margins.Top = 6
      Align = alLeft
      Caption = '(2):'
      ExplicitHeight = 13
    end
    object Vac2CB: TcxComboBox
      AlignWithMargins = True
      Left = 29
      Top = 5
      Margins.Left = 5
      Margins.Top = 5
      Margins.Right = 5
      Margins.Bottom = 5
      Align = alClient
      Properties.DropDownListStyle = lsEditFixedList
      Properties.OnChange = Vac1CBPropertiesChange
      TabOrder = 0
      Width = 301
    end
  end
  object Panel2: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 335
    Height = 30
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label2: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 18
      Height = 21
      Margins.Top = 6
      Align = alLeft
      Caption = '(1):'
      ExplicitHeight = 13
    end
    object Vac1CB: TcxComboBox
      AlignWithMargins = True
      Left = 29
      Top = 5
      Margins.Left = 5
      Margins.Top = 5
      Margins.Right = 5
      Margins.Bottom = 5
      Align = alClient
      Properties.DropDownListStyle = lsEditFixedList
      Properties.OnChange = Vac1CBPropertiesChange
      TabOrder = 0
      Width = 301
    end
  end
end
