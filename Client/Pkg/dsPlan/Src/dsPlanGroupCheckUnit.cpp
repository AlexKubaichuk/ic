﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <SysUtils.hpp>
#pragma hdrstop
#include "dsPlanGroupCheckUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "dsICPlanSetting.h"
#include "msgdef.h"
// #include "pkgsetting.h"
// ---------------------------------------------------------------------------
#define  plOrgFIO    0
#define  plMIBPInf   1
#define  plVacType   2
#define  plPlanDate  3
#define  plSep       4
#define  plExecDate  5
#define  plExecMIBP  6
#define  plReakType  7
#define  plReakVal   8
// служебные поля
#define  plPatientID 9
#define  plMIBPInfID 10
#define  plTypeID      11
#define  plOrgLvl      12
#define  plExecMIBPID  13
#define  plExecReakID  14
#define  plPtBirthDay  15
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxButtons"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxGroupBox"
#pragma link "cxImage"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma link "dxGDIPlusClasses"
#pragma link "dxStatusBar"
#pragma link "cxDropDownEdit"
#pragma link "cxCalendar"
#pragma link "cxProgressBar"
#pragma link "cxBarEditItem"
#pragma link "dxBarExtItems"
#pragma link "cxCheckBox"
#pragma resource "*.dfm"
TdsPlanGroupCheckForm * dsPlanGroupCheckForm;
// ---------------------------------------------------------------------------
__fastcall TdsPlanGroupCheckForm::TdsPlanGroupCheckForm(TComponent * Owner, TdsICPlanDM * ADM, __int64 APlanCode,
  UnicodeString APlanCapt, UnicodeString APlanType) : TForm(Owner)
 {
  // "com", "uch", "org"
  // ReacMap           = new TReakList;
  NECur                            = NECount = CNECur = CNECount = 0;
  FPlanType                        = APlanType.LowerCase();
  LastFindedNode                   = NULL;
  LastNEFindedNode                 = NULL;
  LastCNEFindedNode                = NULL;
  LastF63UCode                     = "";
  SearchBE->Style->StyleController = ADM->StyleController;
  Caption                          = APlanCapt;
  FDM                              = ADM;
  FPlanCode                        = APlanCode;
  SaveEnInsert                     = false;
  SaveEnEdit                       = false;
  SaveEnDelete                     = false;
  SaveEnSetTemplate                = false;
  SaveEnReSetTemplate              = false;
  SaveEnRefresh                    = false;
  SaveEnFind                       = false;
  SaveEnFindNext                   = false;
  SaveEnViewClassParam             = false;
  SaveEnViewTemplateParam          = false;
  FPlanDataSrc                     = NULL;
  FDS                              = NULL;
  FSettingNode                     = NULL;
  if (FDM->DefaultClassWidth)
   Width = FDM->DefaultClassWidth;
  FPlanNode   = FDM->ClassDef->GetTagByUID("3084");
  FLastTop    = 0;
  FLastHeight = 0;
  curCD       = 0;
  FVacList    = new TStringList;
  FTestList   = new TStringList;
  FCheckList  = new TStringList;
  FDM->LoadVacList(FVacList, true);
  FVacList->Sort();
  FDM->LoadTestList(FTestList);
  FTestList->Sort();
  FDM->LoadCheckList(FCheckList);
  FCheckList->Sort();
  ((TcxComboBoxProperties *)ExecMIBPCol->Properties)->Items->Assign(FVacList);
  FInsPrivExecutor = new TInsertCardItem(FDM->RegDef->GetTagByUID("1003"),
    FDM->XMLList->GetXML("12063611-00008CD7-CD89"));
  FInsTestExecutor = new TInsertCardItem(FDM->RegDef->GetTagByUID("102f"),
    FDM->XMLList->GetXML("001D3500-00005882-DC28"));
  FInsCheckExecutor = new TInsertCardItem(FDM->RegDef->GetTagByUID("1100"),
    FDM->XMLList->GetXML("569E1EB2-D0CFEF0F-BE9A"));
  FInsPrivExecutor->OnGetValById = FDM->OnGetValById;
  FInsPrivExecutor->OnInsertData = FDM->OnInsertData;
  FInsPrivExecutor->InLPU        = 1;
  FInsPrivExecutor->LPU          = FDM->LPUCode;
  FInsPrivExecutor->BinReak      = true;
  FInsPrivExecutor->Preload();
  FInsTestExecutor->OnGetValById = FDM->OnGetValById;
  FInsTestExecutor->OnInsertData = FDM->OnInsertData;
  FInsTestExecutor->InLPU        = 1;
  FInsTestExecutor->LPU          = FDM->LPUCode;
  FInsTestExecutor->Preload();
  FInsCheckExecutor->OnGetValById = FDM->OnGetValById;
  FInsCheckExecutor->OnInsertData = FDM->OnInsertData;
  FInsCheckExecutor->InLPU        = 1;
  FInsCheckExecutor->LPU          = FDM->LPUCode;
  FInsCheckExecutor->Preload();
  CreateSrc();
 }
// ---------------------------------------------------------------------------
int __fastcall TdsPlanGroupCheckForm::PlanType(TcxTreeListNode * ANode)
 {
  return ANode->Texts[plTypeID].ToIntDef(0);
 }
// ---------------------------------------------------------------------------
TkabCustomDataSource * __fastcall TdsPlanGroupCheckForm::CreateDataSrc(TTagNode * ADefNode)
 {
  TkabCustomDataSource * RC = NULL;
  try
   {
    RC                          = new TkabCustomDataSource(ADefNode);
    RC->DataSet->IdPref         = "plan";
    RC->DataSet->OnGetCount     = FDM->kabCDSGetCount;
    RC->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
    RC->DataSet->OnGetValById   = FDSGetValById;
    RC->DataSet->OnGetValById10 = FDSGetValById10;
    RC->DataSet->OnInsert       = FDM->kabCDSInsert;
    RC->DataSet->OnEdit         = FDM->kabCDSEdit;
    RC->DataSet->OnDelete       = FDM->kabCDSDelete;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::CreateSrc()
 {
  try
   {
    try
     {
      FPlanDataSrc = CreateDataSrc(FPlanNode);
      FDS          = FPlanDataSrc->DataSet;
      // Split->CloseSplitter();
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsErrorMsgCaptionSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegistryErrorCommDBErrorMsgCaption), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::FGetDataProgress(int AMax, int ACur, UnicodeString AMsg)
 {
  if (AMax == -1)
   {
    FPlanCurr                        = 1;
    FPlanCommCount                   = 0;
    ProgressPanelMsg->Caption        = "";
    StatusSB->Panels->Items[0]->Text = "";
   }
  else if (!(AMax + ACur))
   {
    ProgressPanelMsg->Caption        = AMsg;
    StatusSB->Panels->Items[0]->Text = AMsg;
   }
  else
   {
    StatusSB->Panels->Items[0]->Text = AMsg + " [" + IntToStr(ACur) + "/" + IntToStr(AMax) + "]";
    ProgressPanelMsg->Caption        = StatusSB->Panels->Items[0]->Text;
   }
  if (AMax && (AMax != FPlanCommCount))
   {
    ShowProgress(true);
    FPlanCommCount = AMax;
   }
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::FormDestroy(TObject * Sender)
 {
  delete FInsPrivExecutor;
  delete FInsTestExecutor;
  delete FInsCheckExecutor;
  if (FSettingNode)
   delete FSettingNode;
  if (FPlanDataSrc)
   delete FPlanDataSrc;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::actExitExecute(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::FormShow(TObject * Sender)
 {
  ShowTimer->Enabled = true;
 }
// ---------------------------------------------------------------------------
TcxTreeListNode * __fastcall TdsPlanGroupCheckForm::GetSelNode()
 {
  TcxTreeListNode * RC = NULL;
  try
   {
    if (PlanTL->SelectionCount)
     {
      RC = PlanTL->Selections[0];
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TkabCustomDataSetRow * __fastcall TdsPlanGroupCheckForm::CurRow()
 {
  TkabCustomDataSetRow * RC = NULL;
  TcxTreeListNode * tlNode = GetSelNode();
  try
   {
    if (tlNode && tlNode->Data)
     RC = FDS->Row[((__int64)tlNode->Data) - 1];
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::actCheckPlanItemExecute(TObject * Sender)
 {
  TkabCustomDataSetRow * RecData;
  __int64 FPtId;
  int MIBPCode, TestCode, CheckCode, ReacId;
  TDate FExecDate, PtBirthDay;
  TcxTreeListNode * FCurRow;
  UnicodeString FInfCode, FRecID, FRC, VacSch;
  if (!actCheckPlanItem->Enabled)
   return;
  ShowProgress(true);
  if (PlanTL->IsEditing)
   PlanTL->HideEdit();
  try
   {
    PrBar->Position        = 0;
    PrBar->Properties->Max = PlanTL->AbsoluteCount * 2;
    for (int i = 0; i < PlanTL->Root->Count; i++)
     {
      if (PlanTL->Root->Items[i]->Visible)
       {
        PrBar->Position++ ;
        Application->ProcessMessages();
        for (int j = 0; j < PlanTL->Root->Items[i]->Count; j++)
         {
          FCurRow = PlanTL->Root->Items[i]->Items[j];
          PrBar->Position++ ;
          Application->ProcessMessages();
          if (FCurRow->Visible)
           {
            FPtId = FCurRow->Texts[plPatientID].ToIntDef(0);
            PtBirthDay = StrToDate(FCurRow->Texts[plPtBirthDay]);
            FExecDate  = StrToDate(FCurRow->Texts[plExecDate]);
            ReacId     = FCurRow->Texts[plExecReakID].ToIntDef(0);
            switch (PlanType(FCurRow))
             {
             case 1:
               {
                FInsPrivExecutor->NewRec(PtBirthDay);
                MIBPCode = FCurRow->Texts[plExecMIBPID].ToIntDef(0);
                VacSch = FInsPrivExecutor->GetSchLineByVacVar(FCurRow->Texts[plVacType], FCurRow->Texts[plExecMIBPID]);
                RecData                = FInsPrivExecutor->Data;
                RecData->Value["1024"] = FExecDate; // date name='Дата выполнения'
                RecData->Value["1020"] = MIBPCode; // choiceDB name='МИБП'
                if (VacSch.Length())
                 VacSch = FInsPrivExecutor->GetPrivVar(_UID(VacSch));
                else
                 VacSch = FInsPrivExecutor->GetPrivVar(FCurRow->Texts[plVacType]);
                RecData->Value["1021"] = VacSch;
                // text name='Вид'
                RecData->Value["1075"] = Variant::Empty(); // choiceDB name='Инфекция'
                RecData->Value["1022"] = FDM->GetVacDefDoze(FExecDate, PtBirthDay, MIBPCode); // digit name='Доза'
                RecData->Value["1023"] = SerED->Text; // text name='Серия'
                if (!FInsPrivExecutor->ReakIsBin(MIBPCode))
                 {
                  if (ReacId) // задан формат реакции
                   {
                    RecData->Value["1025"] = 1; // binary name='Реакция проверена'
                    RecData->Value["10A8"] = ReacId; // choiceDB name='Формат реакции'
                    if (FInsPrivExecutor->ReakCanVal(ReacId)) // есть значение реакции
                       RecData->Value["1027"] = FCurRow->Texts[plReakVal]; // digit name='Значение реакции'
                   }
                 }
                FInsPrivExecutor->Execute(FPtId, FRecID, FRC);
                break;
               }
             case 2:
               {
                FInsTestExecutor->NewRec(PtBirthDay);
                TestCode               = FCurRow->Texts[plExecMIBPID].ToIntDef(0);
                FInfCode               = FInsTestExecutor->GetTestInf(IntToStr(TestCode));
                RecData                = FInsTestExecutor->Data;
                RecData->Value["1031"] = FExecDate; // date name='Дата выполнения' uid='1031'
                RecData->Value["1032"] = TestCode; // choiceDB name='Проба' uid='1032'
                RecData->Value["1200"] = FInfCode; // choiceDB name='Инфекция' uid='1200'
                RecData->Value["1033"] = SerED->Text; // text name='Серия препарата' uid='1033'
                RecData->Value["1034"] = 0; // choice name='Повод' uid='1034' >> План' value='0'
                if (ReacId) // задан формат реакции
                 {
                  RecData->Value["103B"] = 1; // binary name='Реакция проверена' uid='103B'
                  RecData->Value["10A7"] = ReacId; // choiceDB name='Формат реакции' uid='10A7'
                  if (FInsTestExecutor->ReakCanVal(ReacId)) // есть значение реакции
                     RecData->Value["103F"] = FCurRow->Texts[plReakVal]; // digit name='Значение' uid='103F'
                  // RecData->Value["103F"] = AUCode; // digit name='Значение' uid='103F'
                  // RecData->Value["0184"] = AUCode; // digit name='Значение 2' uid='0184'
                  // RecData->Value["0186"] = AUCode; // digit name='Значение 3' uid='0186'
                  // RecData->Value["0188"] = AUCode; // digit name='Значение 4' uid='0188'
                  // RecData->Value["018A"] = AUCode; // digit name='Значение 5' uid='018A'
                 }
                RecData->Value["1093"] = FCurRow->Texts[plVacType]; // extedit name='Схема' uid='1093'
                FInsTestExecutor->Execute(FPtId, FRecID, FRC);
                break;
               }
             case 3:
               {
                FInsCheckExecutor->NewRec(PtBirthDay);
                CheckCode              = FCurRow->Texts[plExecMIBPID].ToIntDef(0);
                RecData                = FInsCheckExecutor->Data;
                RecData->Value["1031"] = FExecDate; // date name='Дата выполнения' uid='1031'
                RecData->Value["1032"] = CheckCode; // choiceDB name='Проба' uid='1032'
                RecData->Value["1033"] = SerED->Text; // text name='Серия препарата' uid='1033'
                RecData->Value["1034"] = 0; // choice name='Повод' uid='1034' >> План' value='0'
                if (ReacId) // задан формат реакции
                 {
                  RecData->Value["103B"] = 1; // binary name='Реакция проверена' uid='103B'
                  RecData->Value["10A7"] = ReacId; // choiceDB name='Формат реакции' uid='10A7'
                  if (FInsCheckExecutor->ReakCanVal(ReacId)) // есть значение реакции
                     RecData->Value["103F"] = FCurRow->Texts[plReakVal]; // digit name='Значение' uid='103F'
                 }
                // RecData->Value["1093"] = FCurRow->Texts[9]; // extedit name='Схема' uid='1093'
                FInsCheckExecutor->Execute(FPtId, FRecID, FRC);
                break;
               }
             }
           }
          PrBar->Position++ ;
          Application->ProcessMessages();
         }
       }
      PrBar->Position++ ;
      Application->ProcessMessages();
     }
   }
  __finally
   {
    PrBar->Position = 0;
    ShowProgress(false);
   }
  ActiveControl = PlanTL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::FOnCardClose(TObject * Sender)
 {
  // TPatNodes::iterator FRC = FPatNodes.find((__int64)Sender);
  // if (FRC != FPatNodes.end())
  // RefreshPatData(FRC->second);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::GetPlanData()
 {
  try
   {
    FDS->GetData("{\"328F\":\"" + IntToStr(FPlanCode) + "\"}", TdsRegFetch::None, 200, FGetDataProgress);
   }
  catch (Exception & E)
   {
    _MSG_ERR(E.Message, "Ошибка получения данных");
    FDS->Clear();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::actRefreshPlanExecute(TObject * Sender)
 {
  NECur               = NECount = CNECur = CNECount = 0;
  LastFindedNode      = NULL;
  LastNEFindedNode    = NULL;
  LastCNEFindedNode   = NULL;
  PlanTL->OnExpanding = NULL;
  PlanTL->BeginUpdate();
  PlanTL->Clear();
  // FPatNodes.clear();
  ShowProgress(true);
  try
   {
    GetPlanData();
    TDate FPlDate;
    TcxTreeListNode * tmpOrgRow = NULL;
    TcxTreeListNode * tmpPatRow = NULL;
    // __int64 LVal = 0;
    UnicodeString FLev = "";
    UnicodeString FLevVal = "";
    UnicodeString MKBStr;
    TReplaceFlags fRfl;
    fRfl << rfReplaceAll;
    OrgLevel->Items->Clear();
    VacType->Items->Clear();
    MIBP->Items->Clear();
    for (int i = 0; i < FDS->RecordCount; i++)
     { // "com", "uch", "org"
      if ((FDS->Row[i]->Value["3098"] != 1) && (FDS->Row[i]->Value["32FC"] == 1))
       { // на отметку попадают невыполненные без причины
        if (FPlanType == "org")
         {
          if (FLev != VarToStr(FDS->Row[i]->StrValue["3298"]).UpperCase())
           {
            tmpOrgRow = PlanTL->Root->AddChild();
            // tmpOrgRow->CheckGroupType = ncgCheckGroup;
            FLevVal = VarToStr(FDS->Row[i]->StrValue["3298"]);
            FLev    = FLevVal.UpperCase();
            FLevVal = StringReplace(FLevVal, "$", "", fRfl).Trim();
            OrgLevel->Items->Add(FLevVal);
            tmpOrgRow->Texts[plOrgFIO] = FLevVal;
           }
         }
        if (!tmpOrgRow)
         tmpOrgRow = PlanTL->Root->AddChild();
        tmpOrgRow->Values[plOrgLvl] = FLevVal;
        // пациент, прививка/проба/проверка
        TryStrToDate(VarToStr(FDS->Row[i]->Value["3088"]), FPlDate);
        if (FPlDate <= Date())
         {
          tmpPatRow       = tmpOrgRow->AddChild();
          tmpPatRow->Data = (TObject *)(i + 1);
          if (FLevVal.Length())
           tmpPatRow->Texts[plOrgFIO] = VarToStr(FDS->Row[i]->StrValue["317D"]) + "  [ " + FLevVal + " ]";
          else
           tmpPatRow->Texts[plOrgFIO] = VarToStr(FDS->Row[i]->StrValue["317D"]);
          // FPatNodes[(__int64)FDS->Row[i]->Value["317D"]] = tmpPatRow;
          // tmpPatRow->CheckGroupType = ncgCheckGroup;
          if (VarToStr(FDS->Row[i]->StrValue["3089"]).Trim().Length())
           tmpPatRow->Values[plMIBPInf] = FDS->Row[i]->StrValue["3089"]; // МИБП
          else
           tmpPatRow->Values[plMIBPInf] = FDS->Row[i]->StrValue["32AB"]; // Инфекция
          tmpPatRow->Texts[plVacType]   = FDS->Row[i]->StrValue["308B"]; // Вид
          tmpPatRow->Texts[plPlanDate]  = FDS->Row[i]->Value["3088"]; // Дата
          tmpPatRow->Values[plExecDate] = FDS->Row[i]->Value["3088"]; // Date(); //Дата
          tmpPatRow->Values[plExecMIBP] = tmpPatRow->Values[plMIBPInf];
          // служебные поля
          if (MIBP->Items->IndexOf(tmpPatRow->Texts[plMIBPInf]) == -1)
           MIBP->Items->Add(tmpPatRow->Texts[plMIBPInf]);
          if (VacType->Items->IndexOf(tmpPatRow->Texts[plVacType]) == -1)
           VacType->Items->Add(tmpPatRow->Texts[plVacType]);
          tmpPatRow->Values[plPatientID] = FDS->Row[i]->Value["317D"]; // code Пациент
          if (VarToStr(FDS->Row[i]->StrValue["3089"]).Trim().Length())
           tmpPatRow->Values[plMIBPInfID] = FDS->Row[i]->Value["3089"]; // МИБП
          else
           tmpPatRow->Values[plMIBPInfID] = FDS->Row[i]->Value["32AB"]; // Инфекция
          tmpPatRow->Texts[plTypeID]  = FDS->Row[i]->Value["3086"];
          tmpPatRow->Values[plOrgLvl] = FLevVal;
          if (VarToStr(FDS->Row[i]->StrValue["3089"]).Trim().Length())
           tmpPatRow->Values[plExecMIBPID] = FDS->Row[i]->Value["3089"]; // МИБП
          else
           tmpPatRow->Values[plExecMIBPID] = FDS->Row[i]->Value["32AB"]; // Инфекция
          tmpPatRow->Values[plPtBirthDay] = FDS->Row[i]->Value["32A5"]; // ДР пациента
         }
       }
     }
   }
  __finally
   {
    Application->ProcessMessages();
    ((TStringList *)OrgLevel->Items)->Sort();
    ((TStringList *)VacType->Items)->Sort();
    ((TStringList *)MIBP->Items)->Sort();
    PlanTL->EndUpdate();
    Application->ProcessMessages();
    PlanTL->Root->Expand(false);
    for (int i = 0; i < PlanTL->Root->Count; i++)
     {
      if (!PlanTL->Root->Items[i]->Count)
       PlanTL->Root->Items[i]->Delete();
      else
       {
        PlanTL->Root->Items[i]->Expand(false);
        for (int j = 0; j < PlanTL->Root->Items[i]->Count; j++)
         PlanTL->Root->Items[i]->Items[j]->Expand(false);
       }
     }
    Application->ProcessMessages();
    // PlanTL->OnExpanding = PlanTLExpanding;
    Application->ProcessMessages();
    ShowProgress(false);
    Application->ProcessMessages();
    if (PlanTL->Root->Count == 1)
     if (!PlanTL->Root->Items[0]->Count)
      _MSG_ERR("Отсутствуют прививки/пробы доступные для отметки выполнения.", "Ошибка");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::ShowTimerTimer(TObject * Sender)
 {
  ShowTimer->Enabled = false;
  Application->ProcessMessages();
  actRefreshPlanExecute(actRefreshPlan);
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsPlanGroupCheckForm::FDSGetValById(UnicodeString AId, UnicodeString ARecId)
 {
  Application->ProcessMessages();
  FGetDataProgress(FPlanCommCount, FPlanCurr++ , "");
  return FDM->kabCDSGetValById(AId, ARecId);
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsPlanGroupCheckForm::FDSGetValById10(UnicodeString AId, UnicodeString ARecId)
 {
  Application->ProcessMessages();
  FGetDataProgress(FPlanCommCount, FPlanCurr++ , "");
  return FDM->kabCDSGetValById10(AId, ARecId);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::PlanGridViewDataControllerDataChanged(TObject * Sender)
 {
  ShowProgress(false);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::ShowProgress(bool AShow)
 {
  if (AShow)
   {
    ProgressPanel->Width   = Width;
    ProgressPanel->Height  = ClientHeight - StatusSB->Height;
    ProgressPanel->Left    = 0;
    ProgressPanel->Top     = 0;
    ProgressPanel->Visible = true;
    ProgressPanel->BringToFront();
    ProgressPanelMsg->Left    = 3;
    ProgressPanelMsg->Top     = ProgressPanel->Height / 2 - ProgressPanelMsg->Height / 2;
    ProgressPanelMsg->Width   = ProgressPanel->Width - 6;
    ProgressPanelMsg->Visible = true;
    ProgressPanelMsg->BringToFront();
   }
  else
   {
    FPlanCurr                 = 0;
    FPlanCommCount            = 0;
    ProgressPanel->Width      = 10;
    ProgressPanel->Height     = 10;
    ProgressPanel->Visible    = false;
    ProgressPanelMsg->Visible = false;
   }
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::PlanTLIsGroupNode(TcxCustomTreeList * Sender, TcxTreeListNode * ANode,
  bool & IsGroup)
 {
  IsGroup = !(ANode && ANode->Level);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::PlanTLCollapsing(TcxCustomTreeList * Sender, TcxTreeListNode * ANode,
  bool & Allow)
 {
  Allow = !(ANode && (ANode->Level == 1));
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::PlanTLFocusedNodeChanged(TcxCustomTreeList * Sender,
  TcxTreeListNode * APrevFocusedNode, TcxTreeListNode * AFocusedNode)
 {
  // actDeletePlanItem->Enabled = false;
  // actCheckPlanItem->Enabled = false;
  actShowCard->Enabled = false;
  if (CurRow())
   {
    // actDeletePlanItem->Enabled = true;
    // actCheckPlanItem->Enabled = VarToStr(CurRow()->Value["3098"]).ToIntDef(0) != 1;
    actShowCard->Enabled = true;
    TcxTreeListNode * SelNode = AFocusedNode;
    if (!SelNode)
     SelNode = GetSelNode();
    if (SelNode)
     {
      if (SelNode->Level == 3)
       SelNode = SelNode->Parent;
      if (SelNode->Level == 2)
       SelNode = SelNode->Parent;
      if (SelNode->Level == 1)
       {
        // if ((LastF63UCode.UpperCase() != SelNode->Texts[plPatientID ].UpperCase()))
        // {
        // LastF63UCode = SelNode->Texts[plPatientID ];
        // }
       }
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::actFindExecute(TObject * Sender)
 {
  if (SearchBE->Text.Trim().Length() && (SearchBE->Text.Trim().UpperCase() != "ПОИСК"))
   {
    SearchTimer->Enabled = false;
    PlanTL->BeginUpdate();
    try
     {
      bool LastExist = LastFindedNode;
      // TcxTreeListFindMode = (tlfmNormal, tlfmLike, tlfmExact);
      // FindNodeByText(const AText: string; AColumn: TcxTreeListColumn; AStartNode: TcxTreeListNode = nil; AExpandedOnly: Boolean = False; AForward: Boolean = True; ACaseSensitive: Boolean = True; AMode: TcxTreeListFindMode = tlfmNormal; ALikeParams: TcxTreeListLikeParams = nil; AIgnoreStartNode: Boolean = False): TcxTreeListNode;
      LastFindedNode = PlanTL->FindNodeByText(SearchBE->Text.Trim() + "%", FIOCol, LastFindedNode, false, true, false,
        tlfmLike, NULL, true);
      if (LastFindedNode)
       {
        PlanTL->SetFocus();
        LastFindedNode->Focused = true;
        LastFindedNode->MakeVisible();
        PlanTL->TopVisibleNode = LastFindedNode;
       }
      else
       {
        if (LastExist)
         {
          LastFindedNode = PlanTL->FindNodeByText(SearchBE->Text.Trim(), FIOCol, LastFindedNode, false, true, false,
            tlfmNormal, NULL, true);
          if (LastFindedNode)
           {
            PlanTL->SetFocus();
            LastFindedNode->Focused = true;
            LastFindedNode->MakeVisible();
            PlanTL->TopVisibleNode = LastFindedNode;
           }
         }
        else
         _MSG_INF("\"" + SearchBE->Text.Trim() + "\" в плане не найден(а).", "Сообщение");
       }
     }
    __finally
     {
      PlanTL->EndUpdate();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::actClearSearchExecute(TObject * Sender)
 {
  SearchBE->Text = "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::SearchBEPropertiesChange(TObject * Sender)
 {
  LastFindedNode = NULL;
  if (SearchBE->Text.Trim().Length() && (SearchBE->Text.Trim().UpperCase() != "ПОИСК"))
   SearchTimer->Enabled = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::SearchTimerTimer(TObject * Sender)
 {
  SearchTimer->Enabled = false;
  actFindExecute(actFind);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::OrgLevelChange(TObject * Sender)
 {
  FilterData();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::MIBPChange(TObject * Sender)
 {
  FilterData();
  ButtonEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::VacTypeChange(TObject * Sender)
 {
  FilterData();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::FilterData()
 {
  PlanTL->BeginUpdate();
  try
   {
    bool VC, MC;
    bool OrgFilter = OrgLevel->ItemIndex != -1;
    bool MIBPFilter = MIBP->ItemIndex != -1;
    bool VTFilter = VacType->ItemIndex != -1;
    if (OrgFilter || MIBPFilter || VTFilter)
     {
      UnicodeString OrgVal = "";
      UnicodeString MIBPVal = "";
      UnicodeString VTVal = "";
      if (OrgFilter)
       OrgVal = OrgLevel->Items->Strings[OrgLevel->ItemIndex];
      if (MIBPFilter)
       MIBPVal = MIBP->Items->Strings[MIBP->ItemIndex];
      if (VTFilter)
       VTVal = VacType->Items->Strings[VacType->ItemIndex];
      for (int i = 0; i < PlanTL->Root->Count; i++)
       { // цикл по подразделениям
        PlanTL->Root->Items[i]->Visible = OrgFilter && (PlanTL->Root->Items[i]->Texts[plOrgLvl] == OrgVal) ||
          !OrgFilter;
        if (PlanTL->Root->Items[i]->Visible)
         {
          VC = false;
          for (int j = 0; j < PlanTL->Root->Items[i]->Count; j++)
           { // цикл по пациентам
            PlanTL->Root->Items[i]->Items[j]->Visible =
              (OrgFilter && (PlanTL->Root->Items[i]->Items[j]->Texts[plOrgLvl] == OrgVal) || !OrgFilter) &&
              (MIBPFilter && (PlanTL->Root->Items[i]->Items[j]->Texts[plMIBPInf] == MIBPVal) || !MIBPFilter) &&
              (VTFilter && (PlanTL->Root->Items[i]->Items[j]->Texts[plVacType] == VTVal) || !VTFilter);
            /*
             if (PlanTL->Root->Items[i]->Items[j]->Visible)
             {
             MC = false;
             for (int k = 0; k < PlanTL->Root->Items[i]->Items[j]->Count; k++)
             {//цикл по прививкам
             PlanTL->Root->Items[i]->Items[j]->Items[k]->Visible =
             (MIBPFilter && (PlanTL->Root->Items[i]->Items[j]->Items[k]->Texts[plMIBPInf] == MIBPVal) || !MIBPFilter) &&
             (VTFilter && (PlanTL->Root->Items[
             i]->Items[j]->Items[k]->Texts[plVacType] == VTVal) || !VTFilter);
             MC |= PlanTL->Root->Items[i]->Items[j]->Items[k]->Visible;
             }
             PlanTL->Root->Items[i]->Items[j]->Visible = MC;

             }
             */
            VC |= PlanTL->Root->Items[i]->Items[j]->Visible;
           }
          PlanTL->Root->Items[i]->Visible = VC;
         }
       }
     }
    else
     {
      for (int i = 0; i < PlanTL->Root->Count; i++)
       {
        for (int j = 0; j < PlanTL->Root->Items[i]->Count; j++)
         {
          PlanTL->Root->Items[i]->Items[j]->Visible = true;
         }
        PlanTL->Root->Items[i]->Visible = true;
       }
     }
   }
  __finally
   {
    PlanTL->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::FillReacVar(TStrings * AItems)
 {
  TcxTreeListNode * FCurRow = GetSelNode();
  if (FCurRow)
   {
    try
     {
      int ItemMIBPCode = FCurRow->Texts[plExecMIBPID].ToIntDef(0);
      int ItemPlType = PlanType(FCurRow);
      switch (ItemPlType)
       {
       case 1: // Caption = "Vac";
         {
          FInsPrivExecutor->FillReacVar(AItems, ItemMIBPCode, ItemPlType);
          break;
         }
       case 2: // Caption = "Test";
         {
          FInsTestExecutor->FillReacVar(AItems, ItemMIBPCode, ItemPlType);
          break;
         }
       case 3: // Caption = "Check";
         {
          FInsCheckExecutor->FillReacVar(AItems, ItemMIBPCode, ItemPlType);
          break;
         }
       }
     }
    __finally
     {
      // ReacVar->Properties->OnChange = FReacTypeChange;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::ExecMIBPColGetEditingProperties(TcxTreeListColumn * Sender,
  TcxTreeListNode * ANode, TcxCustomEditProperties *& EditProperties)
 {
  TcxTreeListNode * FCurRow = GetSelNode();
  if (FCurRow)
   {
    // ((TcxComboBoxProperties *)ExecMIBPCol->Properties)->Items->Clear();
    switch (PlanType(FCurRow))
     {
     case 1:
      Caption = "Vac";
      ((TcxComboBoxProperties *)EditProperties)->Items->Assign(FVacList);
      break;
     case 2:
      Caption = "Test";
      ((TcxComboBoxProperties *)EditProperties)->Items->Assign(FTestList);
      break;
     case 3:
      Caption = "Check";
      ((TcxComboBoxProperties *)EditProperties)->Items->Assign(FCheckList);
      break;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::ReakTypeColGetEditingProperties(TcxTreeListColumn * Sender,
  TcxTreeListNode * ANode, TcxCustomEditProperties *& EditProperties)
 {
  FillReacVar(((TcxCustomComboBoxProperties *)EditProperties)->Items);
 }
// ---------------------------------------------------------------------------
TcxTreeListNode * __fastcall TdsPlanGroupCheckForm::GetActiveItemNode()
 {
  TcxTreeListNode * RC = NULL;
  try
   {
    if (PlanTL->SelectionCount)
     {
      RC = PlanTL->Selections[0];
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsPlanGroupCheckForm::GetActiveItemIndex()
 {
  int RC = -1;
  TcxTreeListNode * tmpNode = GetActiveItemNode();
  try
   {
    if (tmpNode)
     {
      RC = tmpNode->Index;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::SetNextValues(int AIdx, int AExtIdx)
 {
  TcxTreeListNode * tmp = GetActiveItemNode();
  if (tmp)
   {
    int Idx = tmp->Index;
    UnicodeString FValue = "";
    UnicodeString FExtValue = "";
    if (PlanTL->IsEditing)
     {
      FValue = VarToStr(PlanTL->InplaceEditor->EditingValue);
      if (AExtIdx)
       {
        TcxComboBox * CB = (TcxComboBox *)PlanTL->InplaceEditor;
        int ValIdx = CB->ItemIndex;
        if (ValIdx != -1)
         FExtValue = IntToStr((int)CB->Properties->Items->Objects[ValIdx]);
       }
     }
    else
     {
      FValue = tmp->Texts[AIdx];
     }
    if (AExtIdx)
     tmp->Texts[AExtIdx] = FExtValue;
    PlanTL->BeginUpdate();
    try
     {
      TcxTreeListNode * ptmp = tmp->Parent;
      for (int i = Idx + 1; i < ptmp->Count; i++)
       {
        if (ptmp->Items[i]->Visible)
         ptmp->Items[i]->Texts[AIdx] = FValue;
        if (AExtIdx && ptmp->Items[i]->Visible)
         ptmp->Items[i]->Texts[AExtIdx] = FExtValue;
        Application->ProcessMessages();
       }
     }
    __finally
     {
      PlanTL->EndUpdate();
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::ExecMIBPColPropertiesEditValueChanged(TObject * Sender)
 {
  TcxTreeListNode * FCurRow = GetSelNode();
  FCurRow->Values[plExecMIBPID] = 0;
  if (FCurRow && Sender)
   {
    TcxComboBox * FCB = (TcxComboBox *)Sender;
    if (FCB->ItemIndex != -1)
     FCurRow->Values[plExecMIBPID] = (int)(FCB->Properties->Items->Objects[FCB->ItemIndex]);
   }
  SetNextValues(plExecMIBP, plExecMIBPID);
  ButtonEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::ReakColPropertiesEditValueChanged(TObject * Sender)
 {
  TcxTreeListNode * FCurRow = GetSelNode();
  FCurRow->Values[plExecReakID] = 0;
  if (FCurRow && Sender)
   {
    TcxComboBox * FCB = (TcxComboBox *)Sender;
    if (FCB->ItemIndex != -1)
     {
      FCurRow->Values[plExecReakID] = (int)(FCB->Properties->Items->Objects[FCB->ItemIndex]);
     }
   }
  SetNextValues(plReakType, plExecReakID);
  ButtonEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::PlanTLEditing(TcxCustomTreeList * Sender, TcxTreeListColumn * AColumn,
  bool & Allow)
 {
  Allow = false;
  TcxTreeListNode * FCurRow = GetSelNode();
  if (FCurRow && AColumn)
   {
    if (FCurRow->Level && AColumn->Position->BandIndex == 1)
     {
      if (AColumn->Position->ColIndex < 2)
       {
        Allow = true;
       }
      else if (AColumn->Position->ColIndex >= 2)
       {
        TInsertCardItem * FInsExecutor;
        switch (PlanType(FCurRow))
         {
         case 1:
          FInsExecutor = FInsPrivExecutor;
          Allow = !FInsExecutor->ReakIsBin(FCurRow->Texts[plExecMIBPID].ToIntDef(0));
          break;
         case 2:
          FInsExecutor = FInsTestExecutor;
          Allow = true;
          break;
         case 3:
          FInsExecutor = FInsCheckExecutor;
          Allow = true;
          break;
         }
        if (AColumn->Position->ColIndex == 3 && Allow)
         Allow = FInsExecutor->ReakCanVal(FCurRow->Texts[plExecReakID].ToIntDef(0));
       }
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::MIBPCountCBChange(TObject * Sender)
 {
  if (PlanTL->IsEditing && PlanTL->InplaceEditor)
   PlanTL->CancelEdit();
  FVacList->Clear();
  FDM->LoadVacList(FVacList, (MIBPCountCB->ItemIndex != 1));
  FVacList->Sort();
  ((TcxComboBoxProperties *)ExecMIBPCol->Properties)->Items->Assign(FVacList);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::SerEDPropertiesValidate(TObject * Sender, Variant & DisplayValue,
  TCaption & ErrorText, bool & Error)
 {
  Error = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::PlanTLColumn1PropertiesValidate(TObject * Sender, Variant & DisplayValue,
  TCaption & ErrorText, bool & Error)
 {
  Error = false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsPlanGroupCheckForm::CheckInput()
 {
  bool RC = MIBP->ItemIndex != -1;
  TInsertCardItem * FInsExecutor;
  UnicodeString FValue;
  int ActIdx = GetActiveItemIndex();
  try
   {
    if (RC)
     {
      TcxTreeListNode * ndCh;
      for (int i = 0; (i < PlanTL->Count) && RC; i++)
       {
        for (int j = 0; (j < PlanTL->Root->Items[i]->Count) && RC; j++)
         {
          ndCh = PlanTL->Root->Items[i]->Items[j];
          if (ndCh->Visible)
           {
            switch (PlanType(ndCh))
             {
             case 1:
              FInsExecutor = FInsPrivExecutor;
              break;
             case 2:
              FInsExecutor = FInsTestExecutor;
              break;
             case 3:
              FInsExecutor = FInsCheckExecutor;
              break;
             }
            RC &= GetTLValue(ndCh, plExecDate).Trim().Length();
            if (RC)
             {
              FValue = (GetTLValue(ndCh, plReakType).Length()) ? ndCh->Texts[plExecReakID] : UnicodeString("0");
              RC &= FInsExecutor->CheckInput(ndCh->Texts[plExecMIBPID], FValue, GetTLValue(ndCh, plReakVal),
                (PlanType(ndCh) == 2));
             }
           }
          Application->ProcessMessages();
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::ButtonEnabled()
 {
  actCheckPlanItem->Enabled = CheckInput();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::PlanTLColumn1PropertiesChange(TObject * Sender)
 {
  ButtonEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::ExecDateColPropertiesEditValueChanged(TObject * Sender)
 {
  TcxTreeListNode * tmp = GetActiveItemNode();
  if (tmp)
   {
    TDate planDate;
    TDate execDate;
    UnicodeString FValue;
    if (PlanTL->IsEditing)
     FValue = VarToStr(PlanTL->InplaceEditor->EditingValue);
    else
     FValue = tmp->Texts[plExecDate];
    if (TryStrToDate(tmp->Texts[plPlanDate], planDate) && TryStrToDate(FValue, execDate))
     {
      if (execDate < planDate)
       {
        _MSG_ERR("Дата выполнения должна быть больше или равна дате плана", "Ошибка");
        PlanTL->CancelEdit();
        tmp->Texts[plExecDate] = "";
       }
      if (execDate > Date())
       {
        _MSG_ERR("Дата выполнения не может быть больше текущей даты.", "Ошибка");
        PlanTL->CancelEdit();
        tmp->Texts[plExecDate] = "";
       }
     }
   }
  ButtonEnabled();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanGroupCheckForm::actDeleteExecute(TObject * Sender)
 {
  TcxTreeListNode * tmp = GetActiveItemNode();
  if (tmp)
   {
    TcxTreeListNode * pNode = tmp->Parent;
    tmp->Delete();
    if (!pNode->Count)
     pNode->Delete();
   }
 }
// ---------------------------------------------------------------------------
