﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsICPlanSetting.h"
#include "msgdef.h"
#include "AddPlanInfUnit.h"
#include "AddIncompMIBPUnit.h"
#include "AddPauseUnit.h"
#include "AddPreTestUnit.h"
//#include "dsCardSchListUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxGroupBox"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxPC"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma link "dxSkinscxPCPainter"
#pragma link "cxButtons"
#pragma link "cxCheckBox"
#pragma link "cxClasses"
#pragma link "cxButtonEdit"
#pragma resource "*.dfm"
TPlanSettingForm * PlanSettingForm;
//---------------------------------------------------------------------------
__fastcall TPlanSettingForm::TPlanSettingForm(TComponent * Owner, TdsICPlanDM * ADM)
    : TForm(Owner)
 {
  UchDefPlanFormatCB->Style->StyleController = ADM->StyleController;
  OrgDefPlanFormatCB->Style->StyleController = ADM->StyleController;
  FDM                                        = ADM;
  FPlanOptDef                                = new TTagNode;
  FGetReportList(false);
  GetPlanOpt();
  SettingPC->ActivePageIndex = 0;
  SchPC->ActivePageIndex     = 0;
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::FGetReportList(bool ASet)
 {
  PlanFormatsTV->Clear();
  if (FDM->OnGetReportList)
   {
    TStringList * tmpRepList = new TStringList;
    try
     {
      if (ASet)
       tmpRepList->Add("set");
      else
       tmpRepList->Add("add");
      FDM->OnGetReportList("planreport:Список список форматов печати плана", tmpRepList);

      UnicodeString FCaption, FGUI;
      FRepList.clear();
      UchDefPlanFormatCB->Properties->Items->Clear();
      OrgDefPlanFormatCB->Properties->Items->Clear();
      TcxTreeListNode * tmpItem;

      int FOrgPFIdx = -1;
      int FUchPFIdx = -1;
      for (int i = 0; i < tmpRepList->Count; i++)
       {
        FCaption       = GetRPartB(tmpRepList->Strings[i], '=');
        FGUI           = GetLPartB(tmpRepList->Strings[i], '=');
        FRepList[FGUI] = FCaption;
        UchDefPlanFormatCB->Properties->Items->Add(FCaption);
        OrgDefPlanFormatCB->Properties->Items->Add(FCaption);
        FUchDefRep[UchDefPlanFormatCB->Properties->Items->Count - 1] = FGUI.UpperCase();
        FOrgDefRep[OrgDefPlanFormatCB->Properties->Items->Count - 1] = FGUI.UpperCase();

        if (FGUI.UpperCase() == FDM->AppOpt->Vals["uchdefplanreport"].AsString.UpperCase())
         FUchPFIdx = UchDefPlanFormatCB->Properties->Items->Count - 1;
        if (FGUI.UpperCase() == FDM->AppOpt->Vals["orgdefplanreport"].AsString.UpperCase())
         FOrgPFIdx = OrgDefPlanFormatCB->Properties->Items->Count - 1;
        tmpItem           = PlanFormatsTV->Add();
        tmpItem->Texts[0] = FCaption;
        tmpItem->Texts[1] = FGUI;
       }
      UchDefPlanFormatCB->ItemIndex = FUchPFIdx;
      OrgDefPlanFormatCB->ItemIndex = FOrgPFIdx;
     }
    __finally
     {
      delete tmpRepList;
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::PlanFormatListEditBtnClick(TObject * Sender)
 {
  FGetReportList(true);
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::SaveDefSch(TTagNode * ANode)
 {
  /*
   <infdef>
   <pr infref="1" v="1" rv="11" ib="1" tb="1" vschdef="0006.0455" tschdef="0001.004D"/>
   </infdef>
   */
  TTagNode * itNode = ANode->GetFirstChild();
  while (itNode)
   {
    for (int i = 0; i < PrivSchDefTV->Root->Count; i++)
     {
      if (PrivSchDefTV->Root->Items[i]->Values[2] == itNode->AV["infref"].ToIntDef(0))
       {
        itNode->AV["vschdef"] = PrivSchDefTV->Root->Items[i]->Texts[3];
        i                     = PrivSchDefTV->Root->Count + 1;
       }
     }
    for (int i = 0; i < TestSchDefTV->Root->Count; i++)
     {
      if (TestSchDefTV->Root->Items[i]->Values[2] == itNode->AV["infref"].ToIntDef(0))
       {
        itNode->AV["tschdef"] = TestSchDefTV->Root->Items[i]->Texts[3];
        i                     = TestSchDefTV->Root->Count + 1;
       }
     }
    for (int i = 0; i < CheckSchDefTV->Root->Count; i++)
     {
      if (CheckSchDefTV->Root->Items[i]->Values[2] == itNode->AV["infref"].ToIntDef(0))
       {
        itNode->AV["cschdef"] = CheckSchDefTV->Root->Items[i]->Texts[3];
        i                     = CheckSchDefTV->Root->Count + 1;
       }
     }
    int setVRV = 2;
    for (int i = 0; (i < PriorTV->Root->Count) && setVRV; i++)
     {
      if (PriorTV->Root->Items[i]->Texts[2].ToIntDef(0) == itNode->AV["infref"].ToIntDef(0))
       {
        if (PriorTV->Root->Items[i]->Texts[3] == "v")
         itNode->AV["v"] = IntToStr(i + 1);
        else
         itNode->AV["rv"] = IntToStr(i + 1);
        setVRV-- ;
       }
     }
    itNode = itNode->GetNext();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::SaveVacPlan(TTagNode * ANode)
 {
  /*
   <vacplan>
   <vpr infref="14" objtype="l" objref="0" vac="1" test="0" check="0" />
   <vpr infref="15" objtype="l" objref="0" vac="1" test="0" check="0" />
   </vacplan>
   */
  TTagNode * infNode;
  ANode->DeleteChild();
  for (int i = 0; i < NacPlanTV->Root->Count; i++)
   {
    infNode                = ANode->AddChild("vpr");
    infNode->AV["infref"]  = NacPlanTV->Root->Items[i]->Texts[4];
    infNode->AV["objtype"] = "l";
    infNode->AV["objref"]  = "0";
    infNode->AV["vac"]     = IntToStr((int)NacPlanTV->Root->Items[i]->Values[1]);
    infNode->AV["test"]    = IntToStr((int)NacPlanTV->Root->Items[i]->Values[2]);
    infNode->AV["check"]   = IntToStr((int)NacPlanTV->Root->Items[i]->Values[3]);
   }
  for (int i = 0; i < EpidPlanTV->Root->Count; i++)
   {
    infNode                = ANode->AddChild("vpr");
    infNode->AV["infref"]  = EpidPlanTV->Root->Items[i]->Texts[5];
    infNode->AV["objtype"] = EpidPlanTV->Root->Items[i]->Texts[6];
    infNode->AV["objref"]  = EpidPlanTV->Root->Items[i]->Texts[7];
    infNode->AV["vac"]     = IntToStr((int)EpidPlanTV->Root->Items[i]->Values[2]);
    infNode->AV["test"]    = IntToStr((int)EpidPlanTV->Root->Items[i]->Values[3]);
    infNode->AV["check"]   = IntToStr((int)EpidPlanTV->Root->Items[i]->Values[4]);
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::SaveSetPause(TTagNode * ANode, UnicodeString  ANodeName, UnicodeString  AValue)
 {
  TTagNode * FPauseDef = ANode->GetChildByName(ANodeName, true);
  if (!FPauseDef)
   FPauseDef = ANode->GetChildByName("def", true)->AddChild(ANodeName);
  FPauseDef->AV["val"] = AValue;
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::SavePause(TTagNode * ANode)
 {
  /*
   <pause>
   <def>
   <prii val="30" />
   <prit val="30" />
   <pric val="0" />
   <prti val="3" />
   <prtt val="0" />
   <prtc val="0" />
   <prci val="0" />
   <prct val="0" />
   <prcc val="0" />
   </def>
   <content>
   <pr objtype1="i" objref1="1" objtype2="t" objref2="1" val="360" />
   </content>
   </pause>
   */
  TTagNode * FContent = ANode->GetChildByName("content", true);
  TTagNode * FPause;
  TcxTreeListNode * pNode;
  FContent->DeleteChild();
  for (int i = 0; i < PauseTV->Root->Count; i++)
   {
    pNode = PauseTV->Root->Items[i];
    if (!pNode->Texts[5].Length() && !pNode->Texts[7].Length() && !(int)pNode->Values[3])
     SaveSetPause(ANode, "pr" + pNode->Texts[4] + pNode->Texts[6], pNode->Texts[2]);
    else
     {
      FPause                 = FContent->AddChild("pr");
      FPause->AV["objtype1"] = pNode->Texts[4];
      FPause->AV["objref1"]  = pNode->Texts[5];
      FPause->AV["objtype2"] = pNode->Texts[6];
      FPause->AV["objref2"]  = pNode->Texts[7];
      FPause->AV["val"]      = pNode->Texts[2];

     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::SaveIncompMIBP(TTagNode * ANode)
 {
  /*
   <incompMIBP>
   <imr objref1="1" objref2="-1"/>
   <imr objref1="2" objref2="-1"/>
   </incompMIBP>
   */
  TTagNode * infNode;
  ANode->DeleteChild();
  for (int i = 0; i < IncompTV->Root->Count; i++)
   {
    infNode                = ANode->AddChild("imr");
    infNode->AV["objref1"] = IncompTV->Root->Items[i]->Texts[2];
    infNode->AV["objref2"] = IncompTV->Root->Items[i]->Texts[3];
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::SavePreTest(TTagNode * ANode)
 {
  /*
   <incompMIBP>
   <imr objref1="1" objref2="-1"/>
   <imr objref1="2" objref2="-1"/>
   </incompMIBP>
   */
  TTagNode * infNode;
  ANode->DeleteChild();
  for (int i = 0; i < PreTestTL->Root->Count; i++)
   {
    infNode                 = ANode->AddChild("ppt");
    infNode->AV["inf"]      = PreTestTL->Root->Items[i]->Texts[8];
    infNode->AV["test"]     = PreTestTL->Root->Items[i]->Texts[9];
    infNode->AV["plantype"] = PreTestTL->Root->Items[i]->Texts[10];
    infNode->AV["rv"]       = PreTestTL->Root->Items[i]->Texts[3];
    infNode->AV["actuals"]  = PreTestTL->Root->Items[i]->Texts[4];
    infNode->AV["pause"]    = PreTestTL->Root->Items[i]->Texts[5];
    infNode->AV["minage"]   = PreTestTL->Root->Items[i]->Texts[6];
    infNode->AV["maxage"]   = PreTestTL->Root->Items[i]->Texts[7];
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::ApplyBtnClick(TObject * Sender)
 {
  if (CheckInput())
   {
    SaveDefSch(FPlanOptDef->GetChildByName("infdef", true));
    SaveVacPlan(FPlanOptDef->GetChildByName("vacplan", true));
    SavePause(FPlanOptDef->GetChildByName("pause", true));
    SaveIncompMIBP(FPlanOptDef->GetChildByName("incompMIBP", true));
    SavePreTest(FPlanOptDef->GetChildByName("pretest", true));
    /*
     <brigexec>
     <be inf="1" vac="1" test="1" check="1"/>
     </brigexec>
     <plan_print_formats>
     <ppf ref="1032032A-0000BEC0-E49C" name="План (организация)"/>
     <ppf ref="10320938-0000BEC0-0B7C" name="План (участок)"/>
     </plan_print_formats>
     */
    SetPlanOpt();

    TIntStrMap::iterator FRC = FUchDefRep.find(UchDefPlanFormatCB->ItemIndex);
    if (FRC != FUchDefRep.end())
     FDM->AppOpt->Vals["uchdefplanreport"] = FRC->second;

    FRC = FOrgDefRep.find(OrgDefPlanFormatCB->ItemIndex);
    if (FRC != FOrgDefRep.end())
     FDM->AppOpt->Vals["orgdefplanreport"] = FRC->second;

    ModalResult = mrOk;
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TPlanSettingForm::CheckInput()
 {
  bool RC = false;
  try
   {
    if (UchDefPlanFormatCB->ItemIndex == -1)
     {
      _MSG_ERR("Необходимо указать формат плана по умолчанию для участка", "Ошибка");
     }
    else if (OrgDefPlanFormatCB->ItemIndex == -1)
     {
      _MSG_ERR("Необходимо указать формат плана по умолчанию для организации", "Ошибка");
     }
    else
     RC = true;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actSchVacExecute(TObject * Sender)
 {
  FDM->ShowVacSch();
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actSchTestExecute(TObject * Sender)
 {
  FDM->ShowTestSch();
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actSchCheckExecute(TObject * Sender)
 {
  FDM->ShowCheckSch();
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::GetPlanOpt()
 {
  if (FDM->OnGetPlanOpts)
   {
    UnicodeString FOptsDef = "";
    if (FDM->OnGetPlanOpts(FOptsDef))
     {
      try
       {
        FPlanOptDef->AsXML = FOptsDef;
        FillCommPlan(FPlanOptDef->GetChildByName("vacplan", true));
        FillDefSch(FPlanOptDef->GetChildByName("infdef", true));
        FillPause(FPlanOptDef->GetChildByName("pause", true));
        FillVacPrior(FPlanOptDef->GetChildByName("infdef", true));
        FillIncomMIBP(FPlanOptDef->GetChildByName("incompMIBP", true));
        FillPreTest(FPlanOptDef->GetChildByName("pretest", true));
       }
      catch (Exception & E)
       {
        _MSG_ERR(E.Message, "Ошибка");
       }
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::SetPlanOpt()
 {
  if (FDM->OnSetPlanOpts)
   {
    try
     {
      FDM->OnSetPlanOpts(FPlanOptDef->AsXML);
     }
    catch (Exception & E)
     {
      _MSG_ERR(E.Message, "Ошибка");
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::FillCommPlan(TTagNode * APlanNode)
 {
  if (APlanNode)
   {
    NacPlanTV->Clear();
    EpidPlanTV->Clear();
    TcxTreeListNode * infNode;
    TTagNode * itInf = APlanNode->GetFirstChild();
    UnicodeString objName;
    while (itInf)
     {
      //«l» - ЛПУ
      //«o» - Организация
      //«u» - Участок
      //«a» - Адрес
      //«p» - Профессия
      if (itInf->CmpAV("objtype", "l") && itInf->CmpAV("objref", "0"))
       {
        infNode            = NacPlanTV->Root->AddChild();
        infNode->Texts[0]  = FDM->InfName[itInf->AV["infref"]];
        infNode->Values[1] = itInf->AV["vac"].ToIntDef(0);
        infNode->Values[2] = itInf->AV["test"].ToIntDef(0);
        infNode->Values[3] = itInf->AV["check"].ToIntDef(0);
        infNode->Texts[4]  = itInf->AV["infref"];
       }
      else if (!itInf->CmpAV("objref", "0"))
       {
        objName = "";
        if (itInf->CmpAV("objtype", "l"))
         objName = itInf->AV["objref"];
        else if (itInf->CmpAV("objtype", "o"))
         {
          objName = itInf->AV["objref"];
          if (FDM->OnGetOrgStr)
           objName = FDM->OnGetOrgStr(objName);
         }
        else if (itInf->CmpAV("objtype", "u"))
         {
          objName = itInf->AV["objref"];
          if (FDM->OnGetUchStr)
           objName = FDM->OnGetUchStr(objName);
         }
        else if (itInf->CmpAV("objtype", "a"))
         {
          objName = itInf->AV["objref"];
          if (FDM->OnGetAddrStr)
           objName = FDM->OnGetAddrStr(objName);
         }
        else if (itInf->CmpAV("objtype", "p"))
         objName = itInf->AV["objref"];
        if (objName.Length())
         {
          infNode           = EpidPlanTV->Root->AddChild();
          infNode->Texts[0] = FDM->InfName[itInf->AV["infref"]];
          infNode->Texts[1] = objName;

          infNode->Values[2] = itInf->AV["vac"].ToIntDef(0);
          infNode->Values[3] = itInf->AV["test"].ToIntDef(0);
          infNode->Values[4] = itInf->AV["check"].ToIntDef(0);

          infNode->Texts[5] = itInf->AV["infref"];
          infNode->Texts[6] = itInf->AV["objtype"];
          infNode->Texts[7] = itInf->AV["objref"];
         }
       }
      itInf = itInf->GetNext();
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::FillDefSch(TTagNode * APlanNode)
 {
  if (APlanNode)
   {
    PrivSchDefTV->Clear();
    TestSchDefTV->Clear();
    CheckSchDefTV->Clear();
    TcxTreeListNode * infPrivNode;
    TcxTreeListNode * infTestNode;
    TcxTreeListNode * infCheckNode;
    TTagNode * itInf = APlanNode->GetFirstChild();
    while (itInf)
     {
      infPrivNode  = PrivSchDefTV->Root->AddChild();
      infTestNode  = TestSchDefTV->Root->AddChild();
      infCheckNode = CheckSchDefTV->Root->AddChild();

      infPrivNode->Texts[0]  = FDM->InfName[itInf->AV["infref"]];
      infTestNode->Texts[0]  = infPrivNode->Texts[0];
      infCheckNode->Texts[0] = infPrivNode->Texts[0];
      if (!itInf->CmpAV("vschdef", "_NULL_"))
       infPrivNode->Texts[1] = FDM->GetSchName(itInf->AV["vschdef"], true);
      if (!itInf->CmpAV("tschdef", "_NULL_"))
       infTestNode->Texts[1] = FDM->GetSchName(itInf->AV["tschdef"], false);
      infCheckNode->Texts[1]  = "";
      infPrivNode->Values[2]  = itInf->AV["infref"].ToIntDef(0);
      infTestNode->Values[2]  = itInf->AV["infref"].ToIntDef(0);
      infCheckNode->Values[2] = itInf->AV["infref"].ToIntDef(0);
      infPrivNode->Texts[3]   = itInf->AV["vschdef"];
      infTestNode->Texts[3]   = itInf->AV["tschdef"];
      infCheckNode->Texts[3]  = "_NULL_";
      itInf                   = itInf->GetNext();
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::FillPause(TTagNode * APlanNode)
 {
  if (APlanNode)
   {
    PauseTV->Clear();
    TTagNode * FDefVals = APlanNode->GetChildByName("def");
    TTagNode * FContent = APlanNode->GetChildByName("content");
    if (FDefVals && FContent)
     {
      TcxTreeListNode * PauseNode;
      //заполняем значения по умолчанию
      TTagNode * itPause = FDefVals->GetFirstChild();
      while (itPause)
       {
        PauseNode           = PauseTV->Root->AddChild();
        PauseNode->Texts[5] = "";
        PauseNode->Texts[7] = "";
        if (itPause->CmpName("prii"))
         {
          PauseNode->Texts[0] = "Прививка";
          PauseNode->Texts[1] = "Прививка";
          PauseNode->Texts[4] = "i";
          PauseNode->Texts[6] = "i";
         }
        if (itPause->CmpName("prit"))
         {
          PauseNode->Texts[0] = "Прививка";
          PauseNode->Texts[1] = "Проба";
          PauseNode->Texts[4] = "i";
          PauseNode->Texts[6] = "t";
         }
        if (itPause->CmpName("pric"))
         {
          PauseNode->Texts[0] = "Прививка";
          PauseNode->Texts[1] = "Проверка";
          PauseNode->Texts[4] = "i";
          PauseNode->Texts[6] = "c";
         }
        if (itPause->CmpName("prti"))
         {
          PauseNode->Texts[0] = "Проба";
          PauseNode->Texts[1] = "Прививка";
          PauseNode->Texts[4] = "t";
          PauseNode->Texts[6] = "i";
         }
        if (itPause->CmpName("prtt"))
         {
          PauseNode->Texts[0] = "Проба";
          PauseNode->Texts[1] = "Проба";
          PauseNode->Texts[4] = "t";
          PauseNode->Texts[6] = "t";
         }
        if (itPause->CmpName("prtc"))
         {
          PauseNode->Texts[0] = "Проба";
          PauseNode->Texts[1] = "Проверка";
          PauseNode->Texts[4] = "t";
          PauseNode->Texts[6] = "c";
         }
        if (itPause->CmpName("prci"))
         {
          PauseNode->Texts[0] = "Проверка";
          PauseNode->Texts[1] = "Прививка";
          PauseNode->Texts[4] = "c";
          PauseNode->Texts[6] = "i";
         }
        if (itPause->CmpName("prct"))
         {
          PauseNode->Texts[0] = "Проверка";
          PauseNode->Texts[1] = "Проба";
          PauseNode->Texts[4] = "c";
          PauseNode->Texts[6] = "t";
         }
        if (itPause->CmpName("prcc"))
         {
          PauseNode->Texts[0] = "Проверка";
          PauseNode->Texts[1] = "Проверка";
          PauseNode->Texts[4] = "c";
          PauseNode->Texts[6] = "c";
         }
        PauseNode->Values[2] = itPause->AV["val"].ToIntDef(0);
        PauseNode->Values[3] = 0;
        itPause              = itPause->GetNext();
       }

      itPause = FContent->GetFirstChild();
      while (itPause)
       {
        PauseNode = PauseTV->Root->AddChild();

        if (itPause->CmpAV("objtype1", "i"))
         PauseNode->Texts[0] = "Прививка {" + FDM->InfName[itPause->AV["objref1"]] + "}";
        else if (itPause->CmpAV("objtype1", "t"))
         PauseNode->Texts[0] = "Проба {" + FDM->TestName[itPause->AV["objref1"]] + "}";
        else if (itPause->CmpAV("objtype1", "c"))
         PauseNode->Texts[0] = "Проверка {" + FDM->CheckName[itPause->AV["objref1"]] + "}";

        if (itPause->CmpAV("objtype2", "i"))
         PauseNode->Texts[1] = "Прививка {" + FDM->InfName[itPause->AV["objref2"]] + "}";
        else if (itPause->CmpAV("objtype2", "t"))
         PauseNode->Texts[1] = "Проба {" + FDM->TestName[itPause->AV["objref2"]] + "}";
        else if (itPause->CmpAV("objtype2", "c"))
         PauseNode->Texts[1] = "Проверка {" + FDM->CheckName[itPause->AV["objref2"]] + "}";

        PauseNode->Values[2] = itPause->AV["val"].ToIntDef(0);
        PauseNode->Values[3] = 1;
        PauseNode->Texts[4]  = itPause->AV["objtype1"];
        PauseNode->Texts[5]  = itPause->AV["objref1"];
        PauseNode->Texts[6]  = itPause->AV["objtype2"];
        PauseNode->Texts[7]  = itPause->AV["objref2"];
        itPause              = itPause->GetNext();
       }
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::FillVacPrior(TTagNode * APlanNode)
 {
  if (APlanNode)
   {
    typedef map<int, UnicodeString>TPriorMap;
    TPriorMap FPrior = TPriorMap();
    TcxTreeListNode * infNode;
    TTagNode * infDef;

    PriorTV->Clear();
    TTagNode * itInf = APlanNode->GetFirstChild();
    while (itInf)
     {
      FPrior[itInf->AV["v"].ToIntDef(0)]  = itInf->AV["infref"] + ":v";
      FPrior[itInf->AV["rv"].ToIntDef(0)] = itInf->AV["infref"] + ":rv";
      itInf                               = itInf->GetNext();
     }
    int FNpp = 1;
    for (TPriorMap::iterator i = FPrior.begin(); i != FPrior.end(); i++)
     {
      infDef = APlanNode->GetChildByAV("pr", "infref", GetLPartB(i->second, ':'));
      if (infDef)
       {
        infNode           = PriorTV->Root->AddChild();
        infNode->Texts[0] = IntToStr(FNpp++);
        if (GetRPartB(i->second, ':') == "v")
         {
          infNode->Texts[1] = FDM->InfName[infDef->AV["infref"]] + " {Вакцинация}";
          infNode->Texts[3] = "v";
         }
        else
         {
          infNode->Texts[1] = FDM->InfName[infDef->AV["infref"]] + " {Ревакцинация}";
          infNode->Texts[3] = "rv";
         }
        infNode->Texts[2] = i->second;
       }
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::FillIncomMIBP(TTagNode * APlanNode)
 {
  if (APlanNode)
   {
    IncompTV->Clear();
    TcxTreeListNode * MIBPNode;
    TTagNode * itMIBP = APlanNode->GetFirstChild();
    while (itMIBP)
     {
      MIBPNode           = IncompTV->Root->AddChild();
      MIBPNode->Texts[0] = FDM->VacName[itMIBP->AV["objref1"]];
      if (itMIBP->CmpAV("objref2", "-1"))
       MIBPNode->Texts[1] = "Все";
      else
       MIBPNode->Texts[1] = FDM->VacName[itMIBP->AV["objref2"]];
      MIBPNode->Texts[2] = itMIBP->AV["objref1"];
      MIBPNode->Texts[3] = itMIBP->AV["objref2"];
      itMIBP             = itMIBP->GetNext();
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::FillPreTest(TTagNode * APlanNode)
 {
  if (APlanNode)
   {
    PreTestTL->Clear();
    TcxTreeListNode * PreTestNode;
    TTagNode * itPreTest = APlanNode->GetFirstChild();
    while (itPreTest)
     {
      //<ppt inf='1' test='1' rv='1' actuals='12' plantype='1' pause='3'  minage=''  maxage=''/>
      PreTestNode           = PreTestTL->Root->AddChild();
      PreTestNode->Texts[0] = FDM->InfName[itPreTest->AV["inf"]];
      PreTestNode->Texts[1] = FDM->TestName[itPreTest->AV["test"]];
      PreTestNode->Texts[2] = PreTestPlanTypeStr(itPreTest->AV["plantype"]);
      PreTestNode->Texts[3] = itPreTest->AV["rv"];
      PreTestNode->Texts[4] = itPreTest->AV["actuals"];
      PreTestNode->Texts[5] = itPreTest->AV["pause"];
      PreTestNode->Texts[6] = itPreTest->AV["minage"];
      PreTestNode->Texts[7] = itPreTest->AV["maxage"];
      PreTestNode->Texts[8] = itPreTest->AV["inf"];
      PreTestNode->Texts[9] = itPreTest->AV["test"];
      PreTestNode->Texts[10] = itPreTest->AV["plantype"];

      itPreTest             = itPreTest->GetNext();
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::FormDestroy(TObject * Sender)
 {
  delete FPlanOptDef;
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actSetVacSchExecute(TObject * Sender)
 {
  UnicodeString FVal = PrivSchDefTV->Selections[0]->Texts[3];
  TTagNodeMap FSchList;
  FDM->GetSchemeList((int)PrivSchDefTV->Selections[0]->Values[2], FSchList, TSchemeObjType::Vac);
  if (GetSchData(&FSchList, GetVacName, FVal))
   {
    PrivSchDefTV->HideEdit();
    PrivSchDefTV->Selections[0]->Texts[3] = FVal;
    if (FVal != "_NULL_")
     PrivSchDefTV->Selections[0]->Texts[1] = FDM->GetSchName(FVal, true);
    else
     PrivSchDefTV->Selections[0]->Texts[1] = "";
    Application->ProcessMessages();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actSetTestSchExecute(TObject * Sender)
 {
  UnicodeString FVal = TestSchDefTV->Selections[0]->Texts[3];
  TTagNodeMap FSchList;
  FDM->GetSchemeList((int)TestSchDefTV->Selections[0]->Values[2], FSchList, TSchemeObjType::Test);
  if (GetSchData(&FSchList, GetTestName, FVal))
   {
    TestSchDefTV->HideEdit();
    TestSchDefTV->Selections[0]->Texts[3] = FVal;
    if (FVal != "_NULL_")
     TestSchDefTV->Selections[0]->Texts[1] = FDM->GetSchName(FVal, false);
    else
     TestSchDefTV->Selections[0]->Texts[1] = "";
    Application->ProcessMessages();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actSetCheckSchExecute(TObject * Sender)
 {
  UnicodeString FVal = CheckSchDefTV->Selections[0]->Texts[3];
  TTagNodeMap FSchList;
  FDM->GetSchemeList((int)CheckSchDefTV->Selections[0]->Values[2], FSchList, TSchemeObjType::Check);
  if (GetSchData(&FSchList, GetCheckName, FVal))
   {
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actAddPlanInfExecute(TObject * Sender)
 {
  TAddPlanInfForm * Dlg = new TAddPlanInfForm(this, FDM);
  try
   {
    Dlg->ShowModal();
    if (Dlg->ModalResult == mrOk)
     {
      TcxTreeListNode * infNode;
      bool FRes = false;
      for (int i = 0; (i < NacPlanTV->Root->Count) && !FRes; i++)
       FRes = (NacPlanTV->Root->Items[i]->Texts[4] == Dlg->Inf);
      if (FRes)
       _MSG_INF("Инфекция не добавлена в эпид. календарь, т.к. присутствует в национальном календаре.", "Сообщение");
      else
       {
        for (int i = 0; (i < EpidPlanTV->Root->Count) && !FRes; i++)
         FRes = (EpidPlanTV->Root->Items[i]->Texts[5] == Dlg->Inf) &&
             (EpidPlanTV->Root->Items[i]->Texts[6] == Dlg->Type) &&
             (EpidPlanTV->Root->Items[i]->Texts[7] == Dlg->Code);

        if (FRes)
         _MSG_INF("Инфекция не добавлена, т.к. уже присутствует в эпид. календаре.", "Сообщение");
        else
         {
          infNode           = EpidPlanTV->Root->AddChild();
          infNode->Texts[0] = FDM->InfName[Dlg->Inf];
          infNode->Texts[1] = Dlg->StrVal;

          infNode->Values[2] = 1;
          infNode->Values[3] = 0;
          infNode->Values[4] = 0;

          infNode->Texts[5] = Dlg->Inf;
          infNode->Texts[6] = Dlg->Type;
          infNode->Texts[7] = Dlg->Code;
         }
       }
     }
   }
  __finally
   {
    delete Dlg;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actDelPlanInfExecute(TObject * Sender)
 {
  if (EpidPlanTV->SelectionCount)
   {
    EpidPlanTV->Selections[0]->Delete();
    Application->ProcessMessages();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actAddPauseExecute(TObject * Sender)
 {
  TAddPauseForm * Dlg = new TAddPauseForm(this, FDM);
  try
   {
    Dlg->ShowModal();
    if (Dlg->ModalResult == mrOk)
     {
      TcxTreeListNode * PauseNode;
      PauseNode = PauseTV->Root->AddChild();
      if (Dlg->Type1 == "i")
       PauseNode->Texts[0] = "Прививка {" + FDM->InfName[Dlg->Code1] + "}";
      else if (Dlg->Type1 == "t")
       PauseNode->Texts[0] = "Проба {" + FDM->TestName[Dlg->Code1] + "}";
      else if (Dlg->Type1 == "c")
       PauseNode->Texts[0] = "Проверка {" + FDM->CheckName[Dlg->Code1] + "}";

      if (Dlg->Type2 == "i")
       PauseNode->Texts[1] = "Прививка {" + FDM->InfName[Dlg->Code2] + "}";
      else if (Dlg->Type2 == "t")
       PauseNode->Texts[1] = "Проба {" + FDM->TestName[Dlg->Code2] + "}";
      else if (Dlg->Type2 == "c")
       PauseNode->Texts[1] = "Проверка {" + FDM->CheckName[Dlg->Code2] + "}";

      PauseNode->Values[2] = Dlg->Value.ToIntDef(0);
      PauseNode->Values[3] = 1;
      PauseNode->Texts[4]  = Dlg->Type1;
      PauseNode->Texts[5]  = Dlg->Code1;
      PauseNode->Texts[6]  = Dlg->Type2;
      PauseNode->Texts[7]  = Dlg->Code2;
     }
   }
  __finally
   {
    delete Dlg;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actDelPauseExecute(TObject * Sender)
 {
  if (PauseTV->SelectionCount)
   {
    PauseTV->Selections[0]->Delete();
    Application->ProcessMessages();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actUpPriorExecute(TObject * Sender)
 {
  if (PriorTV->SelectionCount)
   {
    if (PriorTV->Selections[0]->getPrevSibling())
     PriorTV->Selections[0]->MoveTo(PriorTV->Selections[0]->getPrevSibling(), tlamInsert);
    PriorTV->Selections[0]->MakeVisible();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actDownPriorExecute(TObject * Sender)
 {
  if (PriorTV->SelectionCount)
   {
    if (PriorTV->Selections[0]->getNextSibling())
     {
      if (PriorTV->Selections[0]->getNextSibling()->getNextSibling())
       PriorTV->Selections[0]->MoveTo(PriorTV->Selections[0]->getNextSibling()->getNextSibling(), tlamInsert);
      else
       PriorTV->Selections[0]->MoveTo(PriorTV->Selections[0]->getNextSibling(), tlamAdd);
     }
    PriorTV->Selections[0]->MakeVisible();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actAddIncomMIBPExecute(TObject * Sender)
 {
  TAddIncompMIBPForm * Dlg = new TAddIncompMIBPForm(this, FDM);
  try
   {
    Dlg->ShowModal();
    if (Dlg->ModalResult == mrOk)
     {
      TcxTreeListNode * MIBPNode;
      MIBPNode           = IncompTV->Root->AddChild();
      MIBPNode->Texts[0] = FDM->VacName[Dlg->Code1];
      MIBPNode->Texts[1] = FDM->VacName[Dlg->Code2];
      MIBPNode->Texts[2] = Dlg->Code1;
      MIBPNode->Texts[3] = Dlg->Code2;
     }
   }
  __finally
   {
    delete Dlg;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actDelIncomMIBPExecute(TObject * Sender)
 {
  if (IncompTV->SelectionCount)
   {
    IncompTV->Selections[0]->Delete();
    Application->ProcessMessages();
   }
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TPlanSettingForm::GetVacName(UnicodeString  Code)
 {
  return FDM->VacName[Code];
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TPlanSettingForm::GetTestName(UnicodeString  Code)
 {
  return FDM->TestName[Code];
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TPlanSettingForm::GetCheckName(UnicodeString  Code)
 {
  return FDM->CheckName[Code];
 }
//---------------------------------------------------------------------------
bool __fastcall TPlanSettingForm::GetSchData(TTagNodeMap * ASchList, TGetItemNameEvent AGetItemName, UnicodeString & AVal)
 {
  bool RC = false;
  TCommSchListForm * Dlg = new TCommSchListForm(this, ASchList, AGetItemName, AVal, false);
  try
   {
    Dlg->ShowModal();
    if (Dlg->ModalResult == mrOk)
     {
      AVal = Dlg->Schema;
      RC   = true;
     }
   }
  __finally
   {
    delete Dlg;
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::PauseTVStylesGetContentStyle(TcxCustomTreeList * Sender,
    TcxTreeListColumn * AColumn, TcxTreeListNode * ANode, TcxStyle *& AStyle)
 {
  if (ANode && AColumn)
   if (!ANode->Texts[3].ToIntDef(0) && (AColumn->Position->ColIndex < 2))
    AStyle = SchDefBkg;
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::PauseTVFocusedNodeChanged(TcxCustomTreeList * Sender,
    TcxTreeListNode * APrevFocusedNode, TcxTreeListNode * AFocusedNode)
 {
  if (AFocusedNode)
   actDelPause->Enabled = AFocusedNode->Texts[3].ToIntDef(0);
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TPlanSettingForm::PreTestPlanTypeStr(UnicodeString ACode)
 {
  UnicodeString RC = "0";
  try
   {
     switch (ACode.ToIntDef(0))
     {
       case 0:
       RC = "Не планировать";
       break;
       case 1:
       RC = "Планировать предвакцинальную пробу";
       break;
       case 2:
       RC = "Планировать поствакцинальную пробу";
       break;
     }
   }
  __finally
   {
   }
  return RC;
  if (PreTestTL->SelectionCount)
   {
    PreTestTL->Selections[0]->Delete();
    Application->ProcessMessages();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actAddPreTestExecute(TObject * Sender)
 {
  TAddPreTestForm * Dlg = new TAddPreTestForm(this, FDM);
  try
   {
    Dlg->ShowModal();
    if (Dlg->ModalResult == mrOk)
     {
      TcxTreeListNode * PreTestNode;
      PreTestNode           = PreTestTL->Root->AddChild();
      PreTestNode->Texts[0] = FDM->InfName[Dlg->Inf];
      PreTestNode->Texts[1] = FDM->TestName[Dlg->Test];
      PreTestNode->Texts[2] = PreTestPlanTypeStr(Dlg->Plantype);
      PreTestNode->Texts[3] = Dlg->Reak;
      PreTestNode->Texts[4] = Dlg->Actuals;
      PreTestNode->Texts[5] = Dlg->Pause;
      PreTestNode->Texts[6] = Dlg->MinAge;
      PreTestNode->Texts[7] = Dlg->MaxAge;
      PreTestNode->Texts[8] = Dlg->Inf;
      PreTestNode->Texts[9] = Dlg->Test;
      PreTestNode->Texts[10] = Dlg->Plantype;
     }
   }
  __finally
   {
    delete Dlg;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TPlanSettingForm::actDelPreTestExecute(TObject * Sender)
 {
  if (PreTestTL->SelectionCount)
   {
    PreTestTL->Selections[0]->Delete();
    Application->ProcessMessages();
   }
 }
//---------------------------------------------------------------------------

