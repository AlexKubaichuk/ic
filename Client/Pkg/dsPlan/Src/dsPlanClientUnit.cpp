﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <SysUtils.hpp>
#pragma hdrstop
#include "dsPlanClientUnit.h"
#include "dsICPlanCreatePlanUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "dsICPlanSetting.h"
#include "msgdef.h"
#include "JSONUtils.h"
// #include "pkgsetting.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxData"
#pragma link "cxDataStorage"
#pragma link "cxEdit"
#pragma link "cxFilter"
#pragma link "cxGraphics"
#pragma link "cxGrid"
#pragma link "cxGridCustomTableView"
#pragma link "cxGridCustomView"
#pragma link "cxGridLevel"
#pragma link "cxGridTableView"
#pragma link "cxHint"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxNavigator"
#pragma link "cxPC"
#pragma link "cxScrollBar"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxCustomHint"
#pragma link "dxScreenTip"
#pragma link "dxStatusBar"
#pragma link "Htmlview"
#pragma link "dxBar"
#pragma link "dxBarExtItems"
#pragma link "cxMaskEdit"
#pragma link "cxLabel"
#pragma link "cxCheckBox"
#pragma link "cxImageComboBox"
#pragma link "cxDropDownEdit"
#pragma link "cxCheckComboBox"
#pragma resource "*.dfm"
TdsPlanClientForm * dsPlanClientForm;
// ---------------------------------------------------------------------------
#define  plSortCol 0
#define  plName      1
#define  plPrev      2
#define  plCur       3
#define  plNext      4
#define  plPrevState 5
#define  plCurState  6
#define  plNextState 7
#define  plPrevCode  8
#define  plCurCode   9
#define  plNextCode  10
#define  plType      11
#define  plLPUCode     12
// #define  plLPUPartCode 13
#define  plPrevFullCount  14
#define  plCurFullCount   15
#define  plNextFullCount  16
#define  plPrevEmptyCount 17
#define  plCurEmptyCount  18
#define  plNextEmptyCount 19
// ---------------------------------------------------------------------------
__fastcall TdsPlanClientForm::TdsPlanClientForm(TComponent * Owner, TdsICPlanDM * ADM) : TForm(Owner)
 {
  FindED->Style->StyleController = ADM->StyleController;
  FDM                            = ADM;
  actPlanSetting->Enabled        = FDM->PlanEditable;
  FPlanListDataSrc               = NULL;
  // SaveCaption = AUnitStr+" - ";
  _UID_        = "3188";
  FPlanListDef = NULL;
  // PlanListTL->OptionsView->Headers = false;
  if (FDM->DefaultClassWidth)
   Width = FDM->DefaultClassWidth;
  FPlanListDef = FDM->ClassDef->GetTagByUID("3188");
  FShowHidden  = !actShowHidden->Checked;
  CreateSrc();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::CreateSrc()
 {
  // PrivView->BeginUpdate();
  TStringList * FListData = new TStringList;
  TStringList * FMainListData = new TStringList;
  TStringList * FControlListData = new TStringList;
  try
   {
    try
     {
      FPlanListDataSrc                          = new TkabCustomDataSource(FPlanListDef);
      FPlanListDataSrc->DataSet->IdPref         = "plan";
      FPlanListDataSrc->DataSet->OnGetCount     = FDM->kabCDSGetCount;
      FPlanListDataSrc->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
      FPlanListDataSrc->DataSet->OnGetValById   = FDM->kabCDSGetValById;
      FPlanListDataSrc->DataSet->OnGetValById10 = FDM->kabCDSGetValById10;
      FPlanListDataSrc->DataSet->OnInsert       = FDM->kabCDSInsert;
      FPlanListDataSrc->DataSet->OnEdit         = FDM->kabCDSEdit;
      FPlanListDataSrc->DataSet->OnDelete       = FDM->kabCDSDelete;
      TcxTreeListNode * unitNode;
      FOrgUchMap.clear();
      PlanListTL->Clear();
      BrigPlanListTL->Clear();
      UnicodeString FClStr, FClCode, FLPUCode, FUchCode;
      // общий план
      unitNode                   = PlanListTL->Root->AddChild();
      unitNode->Data             = 0;
      unitNode->Texts[plName]    = "Все участки, организации";
      unitNode->Texts[plType]    = "com";
      unitNode->Texts[plLPUCode] = FLPUCode;
      // бригадный план
      unitNode                   = BrigPlanListTL->Root->AddChild();
      unitNode->Data             = 0;
      unitNode->Texts[plName]    = "Все организации";
      unitNode->Texts[plType]    = "com";
      unitNode->Texts[plLPUCode] = FLPUCode;
      // unitNode->Texts[plLPUPartCode] = FOtdelCode;
      // FOrgUchMap["uch_"+FUchCode] = unitNode;
      FDM->GetStrClassData
        ("select distinct (cast(uch.R00F6 as varchar(10))||'.'||cast(uch.code as varchar(10)) ) as ps1,('Уч: '||uch.R000F||case when otd.R000D is null then '' else  ', '|| otd.R000D end) as ps2 from class_000E uch left join class_000c otd on (otd.code=uch.R00F7) where R00F6=" +
        IntToStr(FDM->LPUCode) + " Order by Upper(otd.R000D), Upper(uch.R000F)", FListData);
      for (int i = 0; i < FListData->Count; i++)
       {
        FClStr  = GetLPartB(FListData->Strings[i], '^');
        FClCode = GetRPartB(FListData->Strings[i], '^');
        // <LPUCode>.<UchCode>
        FLPUCode = GetLPartB(FClCode, '.');
        FUchCode = GetRPartB(FClCode, '.');
        // общий план
        unitNode                   = PlanListTL->Root->AddChild();
        unitNode->Data             = (void *)FUchCode.ToIntDef(0);
        unitNode->Texts[plName]    = FClStr;
        unitNode->Texts[plType]    = "uch";
        unitNode->Texts[plLPUCode] = FLPUCode;
        // unitNode->Texts[plLPUPartCode] = FOtdelCode;
        FOrgUchMap["uch_" + FUchCode] = unitNode;
       }
      FDM->GetClassData("Select R32F4 as ps1, R32F4 as ps2 From class_32F2", FMainListData);
      FDM->GetClassData("Select R32F5 as ps1, R32F5 as ps2 From class_32F3", FControlListData);
      FDM->GetClassData("Select code as ps1, R0008 as ps2 From class_0001 order by Upper(R0008)", FListData);
      bool cont;
      for (int i = 0; i < FListData->Count; i++)
       {
        cont = true;
        if (FMainListData->Count || FControlListData->Count)
         {
          cont = (FMainListData->IndexOfObject(FListData->Objects[i]) != -1) ||
            (FControlListData->IndexOfObject(FListData->Objects[i]) != -1);
         }
        if (cont)
         {
          // общий план
          unitNode                = PlanListTL->Root->AddChild();
          unitNode->Data          = FListData->Objects[i];
          unitNode->Texts[plName] = FListData->Strings[i];
          unitNode->Texts[plType] = "org";
          FOrgUchMap["org_" + IntToStr((__int64)FListData->Objects[i])] = unitNode;
          // бригадный план
          unitNode                = BrigPlanListTL->Root->AddChild();
          unitNode->Data          = FListData->Objects[i];
          unitNode->Texts[plName] = FListData->Strings[i];
          unitNode->Texts[plType] = "org";
          FBrigOrgMap["org_" + IntToStr((__int64)FListData->Objects[i])] = unitNode;
         }
       }
      FCurMonth = TDate(Date().FormatString("01.mm.yyyy"));
      FPrevMonth                      = IncMonth(FCurMonth, -1);
      FNextMonth                      = IncMonth(FCurMonth, 1);
      PrevMonthCol->Caption->Text     = FPrevMonth.FormatString("mmmm");
      CurMonthCol->Caption->Text      = FCurMonth.FormatString("mmmm");
      NextMonthCol->Caption->Text     = FNextMonth.FormatString("mmmm");
      BrigPrevMonthCol->Caption->Text = FPrevMonth.FormatString("mmmm");
      BrigCurMonthCol->Caption->Text  = FCurMonth.FormatString("mmmm");
      BrigNextMonthCol->Caption->Text = FNextMonth.FormatString("mmmm");
      // actComCreateCurPlan->Hint  = "Создать/обновить планы на "+FCurMonth.FormatString("mmmm");
      // actComCreateNextPlan->Hint = "Создать/обновить планы на "+FNextMonth.FormatString("mmmm");
      // actComPrevPrintPlan->Hint  = "Печатать планы на "+FPrevMonth.FormatString("mmmm");
      // actComCurPrintPlan->Hint   = "Печатать планы на "+FCurMonth.FormatString("mmmm");
      // actComNextPrintPlan->Hint  = "Печатать планы на "+FNextMonth.FormatString("mmmm");
      // ****************************************
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsErrorMsgCaptionSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegistryErrorCommDBErrorMsgCaption), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
    delete FListData;
    delete FMainListData;
    delete FControlListData;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::FGetDataProgress(int AMax, int ACur, UnicodeString AMsg)
 {
  if (AMax == -1)
   StatusSB->Panels->Items[2]->Text = "";
  else if (!(AMax + ACur))
   StatusSB->Panels->Items[2]->Text = AMsg;
  else
   StatusSB->Panels->Items[2]->Text = AMsg + " [" + IntToStr(ACur) + "/" + IntToStr(AMax) + "]";
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::FormDestroy(TObject * Sender)
 {
  // if (&FPlans != NULL)
  // {
  // for (TPlanMap::iterator i = FPlans.begin(); i != FPlans.end(); i++)
  // {
  // ShowMessage("1");
  // if (i->second)
  // delete i->second;
  // i->second = NULL;
  // }
  // ShowMessage("2");
  // FPlans.clear();
  // }
  if (FPlanListDataSrc)
   delete FPlanListDataSrc;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::GetData()
 {
  TJSONObject * FPlanParam;
  SortCol->SortOrder     = Dxcore::soNone;
  BrigSortCol->SortOrder = Dxcore::soNone;
  try
   {
    TcxTreeListNode * itPl;
    // общий план
    for (int i = 0; i < PlanListTL->AbsoluteCount; i++)
     {
      itPl                          = PlanListTL->AbsoluteItems[i];
      itPl->Texts[plPrev]           = "";
      itPl->Texts[plPrevState]      = "";
      itPl->Texts[plPrevCode]       = "";
      itPl->Texts[plCur]            = "";
      itPl->Texts[plCurState]       = "";
      itPl->Texts[plCurCode]        = "";
      itPl->Texts[plNext]           = "";
      itPl->Texts[plNextState]      = "";
      itPl->Texts[plNextCode]       = "";
      itPl->Texts[plPrevFullCount]  = "0";
      itPl->Texts[plCurFullCount]   = "0";
      itPl->Texts[plNextFullCount]  = "0";
      itPl->Texts[plPrevEmptyCount] = "0";
      itPl->Texts[plCurEmptyCount]  = "0";
      itPl->Texts[plNextEmptyCount] = "0";
      if (itPl->Texts[plType] == "com")
       itPl->Texts[plSortCol] = "3";
      else
       itPl->Texts[plSortCol] = "0";
     }
    // бригадный план
    for (int i = 0; i < BrigPlanListTL->AbsoluteCount; i++)
     {
      itPl                          = BrigPlanListTL->AbsoluteItems[i];
      itPl->Texts[plPrev]           = "";
      itPl->Texts[plPrevState]      = "";
      itPl->Texts[plPrevCode]       = "";
      itPl->Texts[plCur]            = "";
      itPl->Texts[plCurState]       = "";
      itPl->Texts[plCurCode]        = "";
      itPl->Texts[plNext]           = "";
      itPl->Texts[plNextState]      = "";
      itPl->Texts[plNextCode]       = "";
      itPl->Texts[plPrevFullCount]  = "0";
      itPl->Texts[plCurFullCount]   = "0";
      itPl->Texts[plNextFullCount]  = "0";
      itPl->Texts[plPrevEmptyCount] = "0";
      itPl->Texts[plCurEmptyCount]  = "0";
      itPl->Texts[plNextEmptyCount] = "0";
      if (itPl->Texts[plType] == "com")
       itPl->Texts[plSortCol] = "3";
      else
       itPl->Texts[plSortCol] = "0";
     }
    /*
     unitNode->Texts[plType]    = "com";
     unitNode->Texts[plType]    = "uch";
     unitNode->Texts[plType] = "org";
     */
    TOrgUchMap::iterator FRC;
    int FPlanType;
    bool FBrigPlan;
    // предыдущий месяц
    FPlanListDataSrc->DataSet->Clear();
    FPlanListDataSrc->DataChanged();
    FPlanListDataSrc->DataSet->GetData("{\"318A\":\"'" + FPrevMonth.FormatString("01.mm.yyyy") + "'\"}",
      TdsRegFetch::All, 20, FGetDataProgress);
    FPlanListDataSrc->DataChanged();
    for (int i = 0; i < FPlanListDataSrc->DataSet->RecordCount; i++)
     {
      FPlanParam = (TJSONObject *)TJSONObject::ParseJSONValue(FPlanListDataSrc->DataSet->Row[i]->Value["318C"]);
      FPlanType  = JSONToInt(FPlanParam, "type", 0);
      FBrigPlan  = false;
      switch (FPlanType)
       {
       case 0: // <choicevalue name='Участок' value='0'/>
         {
          FRC = FOrgUchMap.find("uch_" + JSONToString(FPlanParam, "R00B3", ""));
          break;
         }
       case 1: // <choicevalue name='Организация (стандартный)' value='1'/>
       case 2: // <choicevalue name='Организация (полный)' value='2'/>
         {
          FRC = FOrgUchMap.find("org_" + JSONToString(FPlanParam, "R0025", ""));
          break;
         }
       case 3: // <choicevalue name='Организация (для бригады)' value='3'/>
       case 4: // <choicevalue name='Организация (для бригады, по инфекциям)' value='4'/>
         {
          FRC       = FBrigOrgMap.find("org_" + JSONToString(FPlanParam, "R0025", ""));
          FBrigPlan = true;
          break;
         }
       case 5: // <choicevalue name='Фильтр' value='5'/>
         {
         }
       }
      if (FBrigPlan)
       {
        if (FRC != FBrigOrgMap.end())
         {
          if (FRC->second)
           {
            if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318D"])
             FRC->second->Texts[plPrev] = FPlanListDataSrc->DataSet->Row[i]->Value["318D"];
            FRC->second->Texts[plPrevState] = FPlanListDataSrc->DataSet->Row[i]->Value["318E"];
            FRC->second->Texts[plPrevCode]  = FPlanListDataSrc->DataSet->Row[i]->Value["code"];
            if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318E"] == 1)
             FRC->second->Texts[plPrevEmptyCount] = "1";
            else if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318E"] == 2)
             {
              FRC->second->Texts[plPrevFullCount] = "1";
              if (FRC->second->Texts[plType] == "uch")
               FRC->second->Texts[plSortCol] = "2";
              else if (FRC->second->Texts[plType] == "org")
               FRC->second->Texts[plSortCol] = "1";
             }
           }
         }
       }
      else
       {
        if (FRC != FOrgUchMap.end())
         {
          if (FRC->second)
           {
            if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318D"])
             FRC->second->Texts[plPrev] = FPlanListDataSrc->DataSet->Row[i]->Value["318D"];
            FRC->second->Texts[plPrevState] = FPlanListDataSrc->DataSet->Row[i]->Value["318E"];
            FRC->second->Texts[plPrevCode]  = FPlanListDataSrc->DataSet->Row[i]->Value["code"];
            if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318E"] == 1)
             FRC->second->Texts[plPrevEmptyCount] = "1";
            else if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318E"] == 2)
             {
              FRC->second->Texts[plPrevFullCount] = "1";
              if (FRC->second->Texts[plType] == "uch")
               FRC->second->Texts[plSortCol] = "2";
              else if (FRC->second->Texts[plType] == "org")
               FRC->second->Texts[plSortCol] = "1";
             }
           }
         }
       }
     }
    // текущий месяц
    FPlanListDataSrc->DataSet->Clear();
    FPlanListDataSrc->DataChanged();
    FPlanListDataSrc->DataSet->GetData("{\"318A\":\"'" + FCurMonth.FormatString("01.mm.yyyy") + "'\"}",
      TdsRegFetch::All, 20, FGetDataProgress);
    FPlanListDataSrc->DataChanged();
    for (int i = 0; i < FPlanListDataSrc->DataSet->RecordCount; i++)
     {
      FPlanParam = (TJSONObject *)TJSONObject::ParseJSONValue(FPlanListDataSrc->DataSet->Row[i]->Value["318C"]);
      FPlanType  = JSONToInt(FPlanParam, "type", 0);
      FBrigPlan  = false;
      switch (FPlanType)
       {
       case 0: // <choicevalue name='Участок' value='0'/>
         {
          FRC = FOrgUchMap.find("uch_" + JSONToString(FPlanParam, "R00B3", ""));
          break;
         }
       case 1: // <choicevalue name='Организация (стандартный)' value='1'/>
       case 2: // <choicevalue name='Организация (полный)' value='2'/>
         {
          FRC = FOrgUchMap.find("org_" + JSONToString(FPlanParam, "R0025", ""));
          break;
         }
       case 3: // <choicevalue name='Организация (для бригады)' value='3'/>
       case 4: // <choicevalue name='Организация (для бригады, по инфекциям)' value='4'/>
         {
          FRC       = FBrigOrgMap.find("org_" + JSONToString(FPlanParam, "R0025", ""));
          FBrigPlan = true;
          break;
         }
       case 5: // <choicevalue name='Фильтр' value='5'/>
         {
         }
       }
      if (FBrigPlan)
       {
        if (FRC != FBrigOrgMap.end())
         {
          if (FRC->second)
           {
            if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318D"])
             FRC->second->Texts[plCur] = FPlanListDataSrc->DataSet->Row[i]->Value["318D"];
            FRC->second->Texts[plCurState] = FPlanListDataSrc->DataSet->Row[i]->Value["318E"];
            FRC->second->Texts[plCurCode]  = FPlanListDataSrc->DataSet->Row[i]->Value["code"];
            if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318E"] == 1)
             FRC->second->Texts[plCurEmptyCount] = "1";
            else if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318E"] == 2)
             {
              FRC->second->Texts[plCurFullCount] = "1";
              if (FRC->second->Texts[plType] == "uch")
               FRC->second->Texts[plSortCol] = "2";
              else if (FRC->second->Texts[plType] == "org")
               FRC->second->Texts[plSortCol] = "1";
             }
           }
         }
       }
      else
       {
        if (FRC != FOrgUchMap.end())
         {
          if (FRC->second)
           {
            if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318D"])
             FRC->second->Texts[plCur] = FPlanListDataSrc->DataSet->Row[i]->Value["318D"];
            FRC->second->Texts[plCurState] = FPlanListDataSrc->DataSet->Row[i]->Value["318E"];
            FRC->second->Texts[plCurCode]  = FPlanListDataSrc->DataSet->Row[i]->Value["code"];
            if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318E"] == 1)
             FRC->second->Texts[plCurEmptyCount] = "1";
            else if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318E"] == 2)
             {
              FRC->second->Texts[plCurFullCount] = "1";
              if (FRC->second->Texts[plType] == "uch")
               FRC->second->Texts[plSortCol] = "2";
              else if (FRC->second->Texts[plType] == "org")
               FRC->second->Texts[plSortCol] = "1";
             }
           }
         }
       }
     }
    // следующий месяц
    FPlanListDataSrc->DataSet->Clear();
    FPlanListDataSrc->DataChanged();
    FPlanListDataSrc->DataSet->GetData("{\"318A\":\"'" + FNextMonth.FormatString("01.mm.yyyy") + "'\"}",
      TdsRegFetch::All, 20, FGetDataProgress);
    FPlanListDataSrc->DataChanged();
    for (int i = 0; i < FPlanListDataSrc->DataSet->RecordCount; i++)
     {
      FPlanParam = (TJSONObject *)TJSONObject::ParseJSONValue(FPlanListDataSrc->DataSet->Row[i]->Value["318C"]);
      FPlanType  = JSONToInt(FPlanParam, "type", 0);
      FBrigPlan  = false;
      switch (FPlanType)
       {
       case 0: // <choicevalue name='Участок' value='0'/>
         {
          FRC = FOrgUchMap.find("uch_" + JSONToString(FPlanParam, "R00B3", ""));
          break;
         }
       case 1: // <choicevalue name='Организация (стандартный)' value='1'/>
       case 2: // <choicevalue name='Организация (полный)' value='2'/>
         {
          FRC = FOrgUchMap.find("org_" + JSONToString(FPlanParam, "R0025", ""));
          break;
         }
       case 3: // <choicevalue name='Организация (для бригады)' value='3'/>
       case 4: // <choicevalue name='Организация (для бригады, по инфекциям)' value='4'/>
         {
          FRC       = FBrigOrgMap.find("org_" + JSONToString(FPlanParam, "R0025", ""));
          FBrigPlan = true;
          break;
         }
       case 5: // <choicevalue name='Фильтр' value='5'/>
         {
         }
       }
      if (FBrigPlan)
       {
        if (FRC != FBrigOrgMap.end())
         {
          if (FRC->second)
           {
            if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318D"])
             FRC->second->Texts[plNext] = FPlanListDataSrc->DataSet->Row[i]->Value["318D"];
            FRC->second->Texts[plNextState] = FPlanListDataSrc->DataSet->Row[i]->Value["318E"];
            FRC->second->Texts[plNextCode]  = FPlanListDataSrc->DataSet->Row[i]->Value["code"];
            if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318E"] == 1)
             FRC->second->Texts[plNextEmptyCount] = "1";
            else if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318E"] == 2)
             {
              FRC->second->Texts[plNextFullCount] = "1";
              if (FRC->second->Texts[plType] == "uch")
               FRC->second->Texts[plSortCol] = "2";
              else if (FRC->second->Texts[plType] == "org")
               FRC->second->Texts[plSortCol] = "1";
             }
           }
         }
       }
      else
       {
        if (FRC != FOrgUchMap.end())
         {
          if (FRC->second)
           {
            if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318D"])
             FRC->second->Texts[plNext] = FPlanListDataSrc->DataSet->Row[i]->Value["318D"];
            FRC->second->Texts[plNextState] = FPlanListDataSrc->DataSet->Row[i]->Value["318E"];
            FRC->second->Texts[plNextCode]  = FPlanListDataSrc->DataSet->Row[i]->Value["code"];
            if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318E"] == 1)
             FRC->second->Texts[plNextEmptyCount] = "1";
            else if ((int)FPlanListDataSrc->DataSet->Row[i]->Value["318E"] == 2)
             {
              FRC->second->Texts[plNextFullCount] = "1";
              if (FRC->second->Texts[plType] == "uch")
               FRC->second->Texts[plSortCol] = "2";
              else if (FRC->second->Texts[plType] == "org")
               FRC->second->Texts[plSortCol] = "1";
             }
           }
         }
       }
     }
   }
  __finally
   {
    SortCol->SortOrder     = soDescending;
    BrigSortCol->SortOrder = soDescending;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::actRefreshPlanListExecute(TObject * Sender)
 {
  PlanListTL->BeginUpdate();
  BrigPlanListTL->BeginUpdate();
  try
   {
    if (PlanListTL->InplaceEditor)
     PlanListTL->Root->EndEdit(true);
    if (BrigPlanListTL->InplaceEditor)
     BrigPlanListTL->Root->EndEdit(true);
    GetData();
    Application->ProcessMessages();
    // StatusSB->Panels->Items[1]->Text = IntToStr(FPlanListDataSrc->DataSet->RecordCount);
   }
  __finally
   {
    PlanListTL->EndUpdate();
    BrigPlanListTL->EndUpdate();
   }
  Application->ProcessMessages();
  if (PlanTypePC->ActivePage == ComTS)
   {
    ActiveControl = PlanListTL;
    UpdateHintAndActions(GetActivePlanNode(), PlanListTL->FocusedColumn);
   }
  else
   {
    ActiveControl = BrigPlanListTL;
    UpdateHintAndActions(GetActivePlanNode(), BrigPlanListTL->FocusedColumn);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::actExitExecute(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::FormShow(TObject * Sender)
 {
  ShowTimer->Enabled = true;
  Application->ProcessMessages();
  // ComPlanTL->Root->Items[0]->Focused = true;
  PlanTypePC->ActivePage       = ComTS;
  PlanListTL->FocusedColumn    = CurMonthCol;
  PlanListTL->TopNode->Focused = true;
  PlanListTL->TopNode->MakeVisible();
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
TcxTreeListNode * __fastcall TdsPlanClientForm::GetActivePlanNode()
 {
  TcxTreeListNode * RC = NULL;
  try
   {
    if (PlanTypePC->ActivePage == ComTS)
     {
      if (PlanListTL->SelectionCount)
       RC = PlanListTL->Selections[0];
     }
    else
     {
      if (BrigPlanListTL->SelectionCount)
       RC = BrigPlanListTL->Selections[0];
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
// TTagNode * __fastcall TdsPlanClientForm::GetSettingSelNode(TcxTreeList * ASrc)
// {
// TTagNode * RC = NULL;
// try
// {
// if (ASrc->SelectionCount)
// {
// if (ASrc->Selections[0]->Data)
// RC = (TTagNode *)ASrc->Selections[0]->Data;
// }
// }
// __finally
// {
// }
// return RC;
// }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::ShowTimerTimer(TObject * Sender)
 {
  ShowTimer->Enabled = false;
  Application->ProcessMessages();
  actRefreshPlanListExecute(actRefreshPlanList);
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::FPlanPeriodDisplayText(TcxCustomGridTableItem * Sender,
  TcxCustomGridRecord * ARecord, UnicodeString & AText)
 {
  if (AText.Length())
   AText = TDate(AText).FormatString("mmmm yyyy");
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::PlanListTLStylesGetContentStyle(TcxCustomTreeList * Sender,
  TcxTreeListColumn * AColumn, TcxTreeListNode * ANode, TcxStyle *& AStyle)
 {
  try
   {
    if (AColumn && ANode && (AColumn->Position->BandIndex == 1))
     {
      if (ANode->Texts[AColumn->Position->ColIndex + 5].ToIntDef(-1) != -1)
       {
        if (AColumn->Focused && ANode->Focused)
         {
          AStyle = selStyle;
         }
        else
         {
          if (ANode->Texts[AColumn->Position->ColIndex + 5].ToIntDef(0) == 2)
           AStyle = planStyle;
          else if (ANode->Texts[AColumn->Position->ColIndex + 5].ToIntDef(0) == 1)
           AStyle = emptyPlanStyle;
         }
       }
     }
   }
  catch (Exception & E)
   {
    ShowMessage(UnicodeString(__FUNC__) + " >> " + E.Message);
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsPlanClientForm::FCreatePlan(TDate ADefPlanMonth, int ADefType, int ADefSubType,
  __int64 ADefUchOrgCode, UnicodeString ADopCode)
 {
  bool RC = false;
  try
   {
    try
     {
      TdsCreatePlanForm * Dlg = new TdsCreatePlanForm(this, FDM, FPlanListDef->GetRoot(), FPlanListDataSrc,
        ADefPlanMonth, ADefType, ADefSubType, ADefUchOrgCode, ADopCode);
      try
       {
        if (Dlg->ShowModal() == mrOk)
         {
          // Dlg->GetPlanType()
          // RC = 0 // участок
          // RC = 1 // организация стандартный план (без прививок и проб выполняемых бригадно)
          // RC = 2 // организация полный план (входят все прививки и пробы)
          // RC = 3 // организация план для бригадного выполнения (общий)
          // RC = 4// организация план для бригадного выполнения (отдельно для каждой инфекции)
          // RC = 5 // фильтр
          int lpType = Dlg->GetPlanType();
          if ((lpType == 3) || (lpType == 4))
           PlanTypePC->ActivePage = BrigTS;
          else
           PlanTypePC->ActivePage = ComTS;
          Application->ProcessMessages();
          RC = true;
          TcxTreeListNode * NewPlNode = PlanListNodeByCode(Dlg->InsertedRecId.ToIntDef(0)); // !!! что-то странное
          if (NewPlNode)
           NewPlNode->Focused = true;
         }
       }
      __finally
       {
        delete Dlg;
       }
     }
    catch (Exception & E)
     {
      MessageBox(Handle, cFMT1(icsRegErrorInsertRecSys, E.Message), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
    catch (...)
     {
      MessageBox(Handle, cFMT(icsRegErrorInsertRec), cFMT(icsErrorMsgCaption), MB_ICONSTOP);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::FDeletePlan(__int64 APlanCode, UnicodeString APlanName)
 {
  UnicodeString Str = FMT(icsRegErrorConfirmDeleteRec) + " " + FPlanListDef->GetChildByName("description")
    ->AV["deletelabel"].LowerCase() + " плана на \n\n" + APlanName + "?";
  if (_MSG_QUE(Str, cFMT(icsRegErrorConfirmDeleteRecMsgCaption)) == IDYES)
   { // удаляем
    // ShowMessage(IntToStr(APlanCode));
    TPlanMap::iterator FRC = FPlans.find(APlanCode);
    if (FRC != FPlans.end())
     {
      // ShowMessage("план найден");
      delete FRC->second;
      FRC->second = NULL;
      FPlans.erase(APlanCode);
     }
    UnicodeString RC = FDM->DeletePlan(IntToStr(APlanCode));
    if (RC.UpperCase() == "OK")
     actRefreshPlanListExecute(actRefreshPlanList);
    else
     _MSG_ERR(RC, "Ошибка удаления плана");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::FOpenPlan(__int64 APlanCode, UnicodeString APlanName, UnicodeString APlanType)
 {
  TdsPlanEditClientForm * Dlg;
  TPlanMap::iterator FRC = FPlans.find(APlanCode);
  if (FRC == FPlans.end())
   {
    Dlg               = new TdsPlanEditClientForm(this, FDM, APlanCode, APlanName, APlanType);
    FPlans[APlanCode] = Dlg;
   }
  else
   Dlg = FRC->second;
  Dlg->Show();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::FComPrintPlan(TDate APlanMonth)
 {
  ShowMessage("Скоро будет...(версии через 2-3 :) )");
  /*
   if (FDM->OnPreviewDoc)
   {
   UnicodeString FPrintPlan = "<body>Ошибка печати плана</body>";
   if (FDM->OnPlanPrint)
   {
   FPrintPlan = FDM->OnPlanPrint(__int64 APlanCode);
   }
   FDM->OnPreviewDoc(FPrintPlan, true);
   }
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::FPrintPlan(int AIdx)
 {
  if (FDM->OnPreviewDoc)
   {
    TcxTreeListNode * FPlNode = GetActivePlanNode();
    if (FPlNode)
     { // выбрана строка с участком/организацией
      if (FPlNode->Texts[AIdx].ToIntDef(-1) != -1)
       { // план существует
        __int64 FPlanCode = (__int64)FPlNode->Texts[AIdx].ToIntDef(0);
        UnicodeString FPrintPlan = "<body>Ошибка печати плана</body>";
        if (FDM->OnPlanPrint)
         {
          FPrintPlan = FDM->OnPlanPrint(FPlanCode);
         }
        FDM->OnPreviewDoc(FPrintPlan, true);
       }
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::actPlanSettingExecute(TObject * Sender)
 {
  TPlanSettingForm * Dlg = new TPlanSettingForm(this, FDM);
  try
   {
    Dlg->ShowModal();
    if (Dlg->ModalResult == mrOk)
     {
     }
   }
  __finally
   {
    delete Dlg;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::FindEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_RETURN)
   {
    FindPlanItem();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::FindPlanItem()
 {
  /*
   PlanListTL->BeginUpdate();
   BrigPlanListTL->BeginUpdate();
   TStringList * FSList = new TStringList;
   try
   {
   TOrgUchMap::iterator i;
   int SPos;
   UnicodeString FSearchText = FindED->Text.Trim();
   if (FSearchText == FindED->Properties->Nullstring)
   FSearchText = "";
   //if (FSearchText.UpperCase() == FindED->Properties->Nullstring.UpperCase())
   //FSearchText = "";
   if (FSearchText.Length())
   {
   TReplaceFlags fRfl;
   fRfl << rfReplaceAll;
   FSList->Text = StringReplace(FSearchText.UpperCase(), " ", "\n", fRfl);

   //основной план
   for (i = FOrgUchMap.begin(); i != FOrgUchMap.end(); i++)
   {
   i->second->Visible = true;
   for (int j = 0; (j < FSList->Count) && i->second->Visible; j++)
   {
   SPos = i->second->Texts[plName].UpperCase().Pos(FSList->Strings[j]);
   i->second->Visible &= SPos;
   }
   }
   //бригадный план
   for (i = FBrigOrgMap.begin(); i != FBrigOrgMap.end(); i++)
   {
   i->second->Visible = true;
   for (int j = 0; (j < FSList->Count) && i->second->Visible; j++)
   {
   SPos = i->second->Texts[plName].UpperCase().Pos(FSList->Strings[j]);
   i->second->Visible &= SPos;
   }
   }
   }
   else
   {
   //основной план
   for (i = FOrgUchMap.begin(); i != FOrgUchMap.end(); i++)
   i->second->Visible = true;
   //бригадный план
   for (i = FBrigOrgMap.begin(); i != FOrgUchMap.end(); i++)
   i->second->Visible = true;
   }
   }
   __finally
   {
   delete FSList;
   PlanListTL->EndUpdate();
   BrigPlanListTL->EndUpdate();
   }
   */
 }
// ---------------------------------------------------------------------------
TcxTreeListNode * __fastcall TdsPlanClientForm::PlanListNodeByCode(__int64 ACode)
 {
  TcxTreeListNode * RC = NULL;
  TcxTreeListNode * itPl;
  try
   {
    TcxTreeList * FSearchTL = NULL;
    if (PlanTypePC->ActivePage == ComTS)
     FSearchTL = PlanListTL;
    else if (PlanTypePC->ActivePage == BrigTS)
     FSearchTL = BrigPlanListTL;
    if (FSearchTL)
     {
      for (int i = 0; (i < FSearchTL->AbsoluteCount) && !RC; i++)
       {
        itPl = FSearchTL->AbsoluteItems[i];
        if ((itPl->Texts[plPrevCode].ToIntDef(-1) == ACode) || (itPl->Texts[plCurCode].ToIntDef(-1) == ACode) ||
          (itPl->Texts[plNextCode].ToIntDef(-1) == ACode))
         RC = itPl;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::SearchTimerTimer(TObject * Sender)
 {
  SearchTimer->Enabled = false;
  FindPlanItem();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::FindEDPropertiesChange(TObject * Sender)
 {
  UnicodeString FSearchText = FindED->Text.Trim();
  // if (FSearchText.UpperCase() == FindED->Properties->Nullstring.UpperCase())
  // FSearchText = "";
  if (!FSearchText.Length())
   {
    FindPlanItem();
   }
  else
   {
    SearchTimer->Enabled = false;
    SearchTimer->Enabled = true;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::FindEDPropertiesButtonClick(TObject * Sender, int AButtonIndex)
 {
  if (!AButtonIndex)
   FindPlanItem();
  else
   {
    FindED->Text = "";
    FindPlanItem();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::PlanListTLStylesGetColumnFooterStyle(TcxCustomTreeList * Sender,
  TcxTreeListColumn * AColumn, TcxTreeListNode * ANode, TcxTreeListSummaryItem * AFooterItem, TcxStyle *& AStyle)
 {
  TcxTreeListColumn * CalcCol;
  if (AFooterItem)
   {
    CalcCol = AFooterItem->CalculatedColumn;
    if ((CalcCol == PrevFullCount) || (CalcCol == CurFullCount) || (CalcCol == NextFullCount))
     AStyle = planStyle;
    else if ((CalcCol == PrevEmptyCount) || (CalcCol == CurEmptyCount) || (CalcCol == NextEmptyCount))
     AStyle = emptyPlanStyle;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::BrigPlanListTLStylesGetColumnFooterStyle(TcxCustomTreeList * Sender,
  TcxTreeListColumn * AColumn, TcxTreeListNode * ANode, TcxTreeListSummaryItem * AFooterItem, TcxStyle *& AStyle)
 {
  try
   {
    TcxTreeListColumn * CalcCol;
    if (AFooterItem)
     {
      CalcCol = AFooterItem->CalculatedColumn;
      if ((CalcCol == BrigPrevFullCount) || (CalcCol == BrigCurFullCount) || (CalcCol == BrigNextFullCount))
       AStyle = planStyle;
      else if ((CalcCol == BrigPrevEmptyCount) || (CalcCol == BrigCurEmptyCount) || (CalcCol == BrigNextEmptyCount))
       AStyle = emptyPlanStyle;
     }
   }
  catch (Exception & E)
   {
    ShowMessage(UnicodeString(__FUNC__) + " >> " + E.Message);
   }
 }
// ---------------------------------------------------------------------------
int __fastcall TdsPlanClientForm::GetNotPlanedCount(TcxTreeListColumn * ABaseColumn, TcxTreeListColumn * AColumn)
 {
  int RC = 0;
  try
   {
    if (ABaseColumn && AColumn)
     {
      int AllCount, PlanCount, EmptyPlanCount;
      AllCount = PlanCount = EmptyPlanCount = 0;
      Variant tmp;
      tmp = ABaseColumn->TreeList->Summary->FooterSummaryValues[ABaseColumn->Summary->FooterSummaryItems->Items[0]];
      if (!tmp.IsEmpty() && !tmp.IsNull())
       AllCount = (int)tmp;
      tmp = ABaseColumn->TreeList->Summary->FooterSummaryValues[AColumn->Summary->FooterSummaryItems->Items[0]];
      if (!tmp.IsEmpty() && !tmp.IsNull())
       PlanCount = (int)tmp;
      tmp = ABaseColumn->TreeList->Summary->FooterSummaryValues[AColumn->Summary->FooterSummaryItems->Items[1]];
      if (!tmp.IsEmpty() && !tmp.IsNull())
       EmptyPlanCount = (int)tmp;
      RC = AllCount - PlanCount - EmptyPlanCount - 1;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
// сводные данные для стандартных планов
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::OrgUchNameColTcxTreeListColumnSummaryFooterSummaryItems0GetText
  (TcxTreeListSummaryItem * Sender, const Variant & AValue, UnicodeString & AText)
 {
  try
   {
    if (AText.Length())
     AText = "Всего записей: " + IntToStr(AText.ToIntDef(1) - 1);
   }
  catch (Exception & E)
   {
    ShowMessage(UnicodeString(__FUNC__) + " >> " + E.Message);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::PrevMonthGetNotPlanedCount(TcxTreeListSummaryItem * Sender, const Variant & AValue,
  UnicodeString & AText)
 {
  AText = IntToStr(GetNotPlanedCount(OrgUchNameCol, PrevMonthCol));
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::CurMonthGetNotPlanedCount(TcxTreeListSummaryItem * Sender, const Variant & AValue,
  UnicodeString & AText)
 {
  AText = IntToStr(GetNotPlanedCount(OrgUchNameCol, CurMonthCol));
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::NextMonthGetNotPlanedCount(TcxTreeListSummaryItem * Sender, const Variant & AValue,
  UnicodeString & AText)
 {
  AText = IntToStr(GetNotPlanedCount(OrgUchNameCol, NextMonthCol));
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::GetTextForEmptySummary(TcxTreeListSummaryItem * Sender, const Variant & AValue,
  UnicodeString & AText)
 {
  try
   {
    if (!AText.Length())
     AText = "0";
   }
  catch (Exception & E)
   {
    ShowMessage(UnicodeString(__FUNC__) + " >> " + E.Message);
   }
 }
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// сводные данные для бригадных планов
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::BrigPrevMonthGetNotPlanedCount(TcxTreeListSummaryItem * Sender,
  const Variant & AValue, UnicodeString & AText)
 {
  try
   {
    AText = IntToStr(GetNotPlanedCount(BrigOrgNameCol, BrigPrevMonthCol));
   }
  catch (Exception & E)
   {
    ShowMessage(UnicodeString(__FUNC__) + " >> " + E.Message);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::BrigCurMonthGetNotPlanedCount(TcxTreeListSummaryItem * Sender,
  const Variant & AValue, UnicodeString & AText)
 {
  try
   {
    AText = IntToStr(GetNotPlanedCount(BrigOrgNameCol, BrigCurMonthCol));
   }
  catch (Exception & E)
   {
    ShowMessage(UnicodeString(__FUNC__) + " >> " + E.Message);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::BrigNextMonthGetNotPlanedCount(TcxTreeListSummaryItem * Sender,
  const Variant & AValue, UnicodeString & AText)
 {
  try
   {
    AText = IntToStr(GetNotPlanedCount(BrigOrgNameCol, BrigNextMonthCol));
   }
  catch (Exception & E)
   {
    ShowMessage(UnicodeString(__FUNC__) + " >> " + E.Message);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::FindEDExit(TObject * Sender)
 {
  if (!FindED->Text.Trim().Length())
   FindED->Clear();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::PlanListTLFocusedNodeChanged(TcxCustomTreeList * Sender,
  TcxTreeListNode * APrevFocusedNode, TcxTreeListNode * AFocusedNode)
 {
  UpdateHintAndActions(AFocusedNode, PlanListTL->FocusedColumn);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::PlanListTLFocusedColumnChanged(TcxCustomTreeList * Sender,
  TcxTreeListColumn * APrevFocusedColumn, TcxTreeListColumn * AFocusedColumn)
 {
  UpdateHintAndActions(GetActivePlanNode(), AFocusedColumn);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::BrigPlanListTLFocusedNodeChanged(TcxCustomTreeList * Sender,
  TcxTreeListNode * APrevFocusedNode, TcxTreeListNode * AFocusedNode)
 {
  // ShowMessage(UnicodeString(__FUNC__) + " 1");
  UpdateHintAndActions(AFocusedNode, BrigPlanListTL->FocusedColumn);
  // ShowMessage(UnicodeString(__FUNC__) + " 2");
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::BrigPlanListTLFocusedColumnChanged(TcxCustomTreeList * Sender,
  TcxTreeListColumn * APrevFocusedColumn, TcxTreeListColumn * AFocusedColumn)
 {
  // ShowMessage(UnicodeString(__FUNC__) + " 1");
  UpdateHintAndActions(GetActivePlanNode(), AFocusedColumn);
  // ShowMessage(UnicodeString(__FUNC__) + " 2");
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::UpdateHintAndActions(TcxTreeListNode * AFocusedNode,
  TcxTreeListColumn * AFocusedColumn)
 {
  try
   {
    if (AFocusedColumn)
     {
      if (AFocusedNode)
       {
        AFocusedNode->MakeVisible();
        TDate FMonth = GetSelMonth(AFocusedColumn);
        if ((double)FMonth)
         {
          if (AFocusedColumn->Position->ColIndex)
           {
            actCreatePlan->Hint = "Создать/обновить план на " + FMonth.FormatString("mmmm") + "\nдля\n" +
              AFocusedNode->Texts[plName];
            actCreatePlan->Enabled = FDM->PlanEditable;
           }
          else
           {
            actCreatePlan->Hint    = "";
            actCreatePlan->Enabled = false;
           }
          actOpenPlan->Hint = "Открыть план на " + FMonth.FormatString("mmmm") + "\nдля\n" +
            AFocusedNode->Texts[plName];
          actDeletePlan->Hint = "Удалить план на " + FMonth.FormatString("mmmm") + "\nдля\n" +
            AFocusedNode->Texts[plName];
          actPrintPlan->Hint = "Печатать план на " + FMonth.FormatString("mmmm") + "\nдля\n" +
            AFocusedNode->Texts[plName];
          Caption                = "План на " + FMonth.FormatString("mmmm") + " [" + AFocusedNode->Texts[plName] + "]";
          actOpenPlan->Enabled   = (AFocusedNode->Texts[GetSelCodeIdx()].ToIntDef(-1) != -1);
          actDeletePlan->Enabled = (AFocusedNode->Texts[GetSelCodeIdx()].ToIntDef(-1) != -1) && FDM->PlanEditable;
          actPrintPlan->Enabled  = (AFocusedNode->Texts[GetSelCodeIdx()].ToIntDef(-1) != -1);
         }
        else
         {
          actCreatePlan->Hint    = "Создать/обновить план на ???\nдля\n" + AFocusedNode->Texts[plName];
          actOpenPlan->Hint      = "Открыть план на ???\nдля\n" + AFocusedNode->Texts[plName];
          actDeletePlan->Hint    = "Удалить план на ???\nдля\n" + AFocusedNode->Texts[plName];
          actPrintPlan->Hint     = "Печатать план на ???\nдля\n" + AFocusedNode->Texts[plName];
          Caption                = "Список планов";
          actOpenPlan->Enabled   = false;
          actDeletePlan->Enabled = false;
          actPrintPlan->Enabled  = false;
         }
       }
     }
   }
  catch (Exception & E)
   {
    ShowMessage(">>> " + E.Message);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::actCreatePlanExecute(TObject * Sender)
 {
  if (!actCreatePlan->Enabled)
   return;
  try
   {
    TcxTreeListNode * FCurNode = GetActivePlanNode();
    if (FCurNode)
     {
      if (FCurNode->Texts[plType] == "com")
       {
        if (FCreatePlan(GetSelMonth(), 0, 0, 0))
         {
          actRefreshPlanListExecute(actRefreshPlanList);
         }
       }
      else if (FCurNode->Data)
       {
        UnicodeString FDopCode = "";
        int FPlType = 1;
        int FPlDopType = 0;
        if (FCurNode->Texts[plType] == "org")
         FPlType = 2;
        else
         {
          FDopCode = FCurNode->Texts[plLPUCode] + ".";
         }
        if (PlanTypePC->ActivePage == BrigTS)
         FPlDopType = 2;
        if (FCreatePlan(GetSelMonth(), FPlType, FPlDopType, (__int64)FCurNode->Data, FDopCode))
         actRefreshPlanListExecute(actRefreshPlanList);
       }
     }
   }
  __finally
   {
   }
  if (PlanTypePC->ActivePage == ComTS)
   {
    ActiveControl = PlanListTL;
    UpdateHintAndActions(GetActivePlanNode(), PlanListTL->FocusedColumn);
   }
  else
   {
    ActiveControl = BrigPlanListTL;
    UpdateHintAndActions(GetActivePlanNode(), BrigPlanListTL->FocusedColumn);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::actDeletePlanExecute(TObject * Sender)
 {
  if (!actDeletePlan->Enabled)
   return;
  try
   {
    TcxTreeListNode * FCurNode = GetActivePlanNode();
    if (FCurNode)
     {
      if (FCurNode->Texts[plType] != "com")
       {
        FDeletePlan((__int64)FCurNode->Texts[GetSelCodeIdx()].ToIntDef(0),
          GetSelMonth().FormatString("mmmm") + "\nдля\n" + FCurNode->Texts[plName]);
       }
     }
   }
  __finally
   {
   }
  if (PlanTypePC->ActivePage == ComTS)
   ActiveControl = PlanListTL;
  else
   ActiveControl = BrigPlanListTL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::actOpenPlanExecute(TObject * Sender)
 {
  if (!actOpenPlan->Enabled)
   return;
  try
   {
    TcxTreeListNode * FCurNode = GetActivePlanNode();
    if (FCurNode)
     {
      if (FCurNode->Texts[plType] == "com")
       {
       }
      else
       {
        FOpenPlan((__int64)FCurNode->Texts[GetSelCodeIdx()].ToIntDef(0), FCurNode->Texts[plName],
          FCurNode->Texts[plType]);
       }
     }
   }
  __finally
   {
   }
  if (PlanTypePC->ActivePage == ComTS)
   ActiveControl = PlanListTL;
  else
   ActiveControl = BrigPlanListTL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::actPrintPlanExecute(TObject * Sender)
 {
  if (!actPrintPlan->Enabled)
   return;
  try
   {
    TcxTreeListNode * FCurNode = GetActivePlanNode();
    if (FCurNode)
     {
      if (FCurNode->Texts[plType] == "com")
       {
        FComPrintPlan(GetSelMonth());
       }
      else
       {
        FPrintPlan(GetSelCodeIdx());
       }
     }
   }
  __finally
   {
   }
  if (PlanTypePC->ActivePage == ComTS)
   ActiveControl = PlanListTL;
  else
   ActiveControl = BrigPlanListTL;
 }
// ---------------------------------------------------------------------------
TDate __fastcall TdsPlanClientForm::GetSelMonth(TcxTreeListColumn * ASelCol)
 {
  TDate RC = TDate(0);
  try
   {
    TcxTreeListColumn * FCol = ASelCol;
    if (!FCol)
     {
      if (PlanTypePC->ActivePage == ComTS)
       FCol = PlanListTL->FocusedColumn;
      else
       FCol = BrigPlanListTL->FocusedColumn;
     }
    if (FCol)
     {
      if (FCol->Position->BandIndex == 1)
       {
        switch (FCol->Position->ColIndex)
         {
         case 0:
           {
            RC = FPrevMonth;
            break;
           }
         case 1:
           {
            RC = FCurMonth;
            break;
           }
         case 2:
           {
            RC = FNextMonth;
            break;
           }
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsPlanClientForm::GetSelCodeIdx()
 {
  int RC = 0;
  try
   {
    if (PlanTypePC->ActivePage == ComTS)
     {
      if (PlanListTL->FocusedColumn)
       {
        if (PlanListTL->FocusedColumn->Position->BandIndex == 1)
         {
          switch (PlanListTL->FocusedColumn->Position->ColIndex)
           {
           case 0:
             {
              RC = plPrevCode;
              break;
             }
           case 1:
             {
              RC = plCurCode;
              break;
             }
           case 2:
             {
              RC = plNextCode;
              break;
             }
           }
         }
       }
     }
    else
     {
      if (BrigPlanListTL->FocusedColumn)
       {
        if (BrigPlanListTL->FocusedColumn->Position->BandIndex == 1)
         {
          switch (BrigPlanListTL->FocusedColumn->Position->ColIndex)
           {
           case 0:
             {
              RC = plPrevCode;
              break;
             }
           case 1:
             {
              RC = plCurCode;
              break;
             }
           case 2:
             {
              RC = plNextCode;
              break;
             }
           }
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsPlanClientForm::PlanListTLDblClick(TObject * Sender)
 {
  if (actOpenPlan->Enabled)
   actOpenPlanExecute(actOpenPlan);
  else if (actCreatePlan->Enabled)
   actCreatePlanExecute(actCreatePlan);
 }
// ---------------------------------------------------------------------------
