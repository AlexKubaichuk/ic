﻿//---------------------------------------------------------------------------
#ifndef dsPlanClientUnitH
#define dsPlanClientUnitH

//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtonEdit.hpp"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxData.hpp"
#include "cxDataStorage.hpp"
#include "cxEdit.hpp"
#include "cxFilter.hpp"
#include "cxGraphics.hpp"
#include "cxGrid.hpp"
#include "cxGridCustomTableView.hpp"
#include "cxGridCustomView.hpp"
#include "cxGridLevel.hpp"
#include "cxGridTableView.hpp"
#include "cxHint.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxNavigator.hpp"
#include "cxPC.hpp"
#include "cxScrollBar.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxCustomHint.hpp"
#include "dxScreenTip.hpp"
#include "dxStatusBar.hpp"
#include "Htmlview.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "dsICPlanDMUnit.h"
#include "KabCustomDS.h"
#include "dsPlanEditClientUnit.h"
#include "dxBar.hpp"
#include "dxBarExtItems.hpp"
#include "cxMaskEdit.hpp"
#include "cxLabel.hpp"
#include "cxCheckBox.hpp"
#include "cxImageComboBox.hpp"
#include "cxDropDownEdit.hpp"
#include "cxCheckComboBox.hpp"
//---------------------------------------------------------------------------
/*
//---------------------------------------------------------------------------
//#include "dsICCardDMUnit.h"
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#include "cxInplaceContainer.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "cxButtonEdit.hpp"
#include "cxHint.hpp"
#include "dxCustomHint.hpp"
#include "dxScreenTip.hpp"
#include "cxScrollBar.hpp"
#include <Vcl.StdCtrls.hpp>
#include "cxMaskEdit.hpp"
//---------------------------------------------------------------------------
*/
typedef map<__int64, TdsPlanEditClientForm*> TPlanMap;
typedef map<UnicodeString, TcxTreeListNode*> TOrgUchMap;
//---------------------------------------------------------------------------
class PACKAGE TdsPlanClientForm : public TForm
{
__published:	// IDE-managed Components
 TPopupMenu *PlanListPM;
        TMenuItem *InsertItem;
        TMenuItem *DeleteItem;
 TcxStyleRepository *Style;
        TdxStatusBar *StatusSB;
        TActionList *ClassAL;
 TAction *actPlanSetting;
 TAction *actRefreshPlanList;
        TAction *actExit;
  TcxImageList *LClassIL32;
  TcxImageList *ClassIL;
 TTimer *ShowTimer;
 TAction *actShowHidden;
 TMenuItem *N1;
 TMenuItem *N6;
 TMenuItem *N7;
 TMenuItem *N2;
 TcxStyle *planStyle;
 TcxStyle *emptyPlanStyle;
 TcxHintStyleController *cxHintStyleController1;
 TcxStyle *defContentStyle;
 TcxStyle *headStyle;
 TcxStyle *nameColStyle;
 TTimer *SearchTimer;
 TdxBarManager *MainBM;
 TdxBar *dxBarManager1Bar1;
 TdxBarLargeButton *dxBarLargeButton1;
 TdxBarLargeButton *dxBarLargeButton2;
 TdxBarLargeButton *dxBarLargeButton3;
 TcxStyle *cxStyle1;
 TdxBarControlContainerItem *dxBarControlContainerItem1;
 TdxBarStatic *dxBarStatic1;
 TcxButtonEdit *FindED;
 TcxStyle *notPlanedStyle;
 TcxStyle *cxStyle2;
 TcxStyle *cxStyle3;
 TdxBarButton *dxBarButton1;
 TdxBarLargeButton *dxBarLargeButton4;
 TdxBarLargeButton *dxBarLargeButton5;
 TdxBarLargeButton *dxBarLargeButton6;
 TdxBarLargeButton *dxBarLargeButton7;
 TAction *actCreatePlan;
 TAction *actOpenPlan;
 TAction *actDeletePlan;
 TAction *actPrintPlan;
 TcxPageControl *PlanTypePC;
 TcxTabSheet *ComTS;
 TcxTabSheet *BrigTS;
 TcxTreeList *PlanListTL;
 TcxTreeListColumn *OrgUchNameCol;
 TcxTreeListColumn *PrevMonthCol;
 TcxTreeListColumn *CurMonthCol;
 TcxTreeListColumn *NextMonthCol;
 TcxTreeListColumn *PrevMonthStateCol;
 TcxTreeListColumn *CurMonthStateCol;
 TcxTreeListColumn *NextMonthStateCol;
 TcxTreeListColumn *PrevMonthPlanCodeCol;
 TcxTreeListColumn *CurMonthPlanCodeCol;
 TcxTreeListColumn *NextMonthPlanCodeCol;
 TcxTreeListColumn *ItemTypeCol;
 TcxTreeListColumn *LPUCodeCol;
 TcxTreeListColumn *LPUPartCodeCol;
 TcxTreeListColumn *PrevFullCount;
 TcxTreeListColumn *CurFullCount;
 TcxTreeListColumn *NextFullCount;
 TcxTreeListColumn *PrevEmptyCount;
 TcxTreeListColumn *CurEmptyCount;
 TcxTreeListColumn *NextEmptyCount;
 TcxTreeListColumn *SortCol;
 TcxTreeList *BrigPlanListTL;
 TcxTreeListColumn *BrigSortCol;
 TcxTreeListColumn *BrigOrgNameCol;
 TcxTreeListColumn *BrigPrevMonthCol;
 TcxTreeListColumn *BrigCurMonthCol;
 TcxTreeListColumn *BrigNextMonthCol;
 TcxTreeListColumn *cxTreeListColumn6;
 TcxTreeListColumn *cxTreeListColumn7;
 TcxTreeListColumn *cxTreeListColumn8;
 TcxTreeListColumn *cxTreeListColumn9;
 TcxTreeListColumn *cxTreeListColumn10;
 TcxTreeListColumn *cxTreeListColumn11;
 TcxTreeListColumn *cxTreeListColumn12;
 TcxTreeListColumn *cxTreeListColumn13;
 TcxTreeListColumn *cxTreeListColumn14;
 TcxTreeListColumn *BrigPrevFullCount;
 TcxTreeListColumn *BrigCurFullCount;
 TcxTreeListColumn *BrigNextFullCount;
 TcxTreeListColumn *BrigPrevEmptyCount;
 TcxTreeListColumn *BrigCurEmptyCount;
 TcxTreeListColumn *BrigNextEmptyCount;
 TcxStyle *selStyle;
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall actRefreshPlanListExecute(TObject *Sender);
        void __fastcall actExitExecute(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
 void __fastcall ShowTimerTimer(TObject *Sender);
 void __fastcall PlanListTLStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
 void __fastcall actPlanSettingExecute(TObject *Sender);
 void __fastcall PlanListTLFocusedNodeChanged(TcxCustomTreeList *Sender, TcxTreeListNode *APrevFocusedNode,
          TcxTreeListNode *AFocusedNode);
 void __fastcall FindEDKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
 void __fastcall SearchTimerTimer(TObject *Sender);
 void __fastcall FindEDPropertiesChange(TObject *Sender);
 void __fastcall FindEDPropertiesButtonClick(TObject *Sender, int AButtonIndex);
 void __fastcall PlanListTLStylesGetColumnFooterStyle(TcxCustomTreeList *Sender,
          TcxTreeListColumn *AColumn, TcxTreeListNode *ANode, TcxTreeListSummaryItem *AFooterItem,
          TcxStyle *&AStyle);
 void __fastcall FindEDExit(TObject *Sender);
 void __fastcall PlanListTLFocusedColumnChanged(TcxCustomTreeList *Sender, TcxTreeListColumn *APrevFocusedColumn,
          TcxTreeListColumn *AFocusedColumn);
 void __fastcall actCreatePlanExecute(TObject *Sender);
 void __fastcall actDeletePlanExecute(TObject *Sender);
 void __fastcall actOpenPlanExecute(TObject *Sender);
 void __fastcall actPrintPlanExecute(TObject *Sender);
 void __fastcall OrgUchNameColTcxTreeListColumnSummaryFooterSummaryItems0GetText(TcxTreeListSummaryItem *Sender,
          const Variant &AValue, UnicodeString &AText);

 void __fastcall GetTextForEmptySummary(TcxTreeListSummaryItem *Sender,
          const Variant &AValue, UnicodeString &AText);

 void __fastcall PrevMonthGetNotPlanedCount(TcxTreeListSummaryItem *Sender, const Variant &AValue, UnicodeString &AText);
 void __fastcall CurMonthGetNotPlanedCount(TcxTreeListSummaryItem *Sender, const Variant &AValue, UnicodeString &AText);
 void __fastcall NextMonthGetNotPlanedCount(TcxTreeListSummaryItem *Sender, const Variant &AValue, UnicodeString &AText);

 void __fastcall BrigPrevMonthGetNotPlanedCount(TcxTreeListSummaryItem * Sender, const Variant & AValue, UnicodeString & AText);
 void __fastcall BrigCurMonthGetNotPlanedCount(TcxTreeListSummaryItem * Sender, const Variant & AValue, UnicodeString & AText);
 void __fastcall BrigNextMonthGetNotPlanedCount(TcxTreeListSummaryItem * Sender, const Variant & AValue, UnicodeString & AText);
 void __fastcall BrigPlanListTLFocusedColumnChanged(TcxCustomTreeList *Sender, TcxTreeListColumn *APrevFocusedColumn,
          TcxTreeListColumn *AFocusedColumn);
 void __fastcall BrigPlanListTLFocusedNodeChanged(TcxCustomTreeList *Sender, TcxTreeListNode *APrevFocusedNode,
          TcxTreeListNode *AFocusedNode);
 void __fastcall BrigPlanListTLStylesGetColumnFooterStyle(TcxCustomTreeList *Sender,
          TcxTreeListColumn *AColumn, TcxTreeListNode *ANode, TcxTreeListSummaryItem *AFooterItem,
          TcxStyle *&AStyle);
 void __fastcall PlanListTLDblClick(TObject *Sender);

private:	// User declarations
  TdsICPlanDM *FDM;

  TkabCustomDataSource *FPlanListDataSrc;
  TPlanMap FPlans;
  TOrgUchMap FOrgUchMap;
  TOrgUchMap FBrigOrgMap;

  TTagNode *FPlanListDef;

  UnicodeString _UID_;
  void __fastcall FGetDataProgress(int AMax, int ACur, UnicodeString AMsg);
  void __fastcall GetData();
  TcxTabSheet *FCurPage;

  TDate FCurMonth, FPrevMonth, FNextMonth;

    UnicodeString    SaveCaption;
    void __fastcall CreateSrc();

//  TTagNode*        __fastcall GetSettingSelNode(TcxTreeList *ASrc);
  TcxTreeListNode* __fastcall GetActivePlanNode();
  void             __fastcall FPlanPeriodDisplayText(TcxCustomGridTableItem *Sender, TcxCustomGridRecord *ARecord, UnicodeString &AText);

  bool __fastcall FCreatePlan(TDate ADefPlanMonth, int ADefType = 0, int ADefSubType = 0, __int64 ADefUchOrgCode = 0, UnicodeString ADopCode = "");
  void __fastcall FOpenPlan(__int64 APlanCode, UnicodeString APlanName, UnicodeString APlanType);
  void __fastcall FDeletePlan(__int64 APlanCode, UnicodeString APlanName);
  void __fastcall FComPrintPlan(TDate APlanMonth);
  void __fastcall FPrintPlan(int AIdx);
  TcxTreeListNode* __fastcall PlanListNodeByCode(__int64 ACode);
  int __fastcall GetNotPlanedCount(TcxTreeListColumn * ABaseColumn, TcxTreeListColumn * AColumn);

  void __fastcall UpdateHintAndActions(TcxTreeListNode *AFocusedNode, TcxTreeListColumn *AFocusedColumn);
  TDate __fastcall GetSelMonth(TcxTreeListColumn *ASelCol = NULL);
  int __fastcall GetSelCodeIdx();
  void __fastcall FindPlanItem();
    bool    FShowHidden;

public:		// User declarations
  __fastcall TdsPlanClientForm(TComponent* Owner, TdsICPlanDM *ADM);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsPlanClientForm *dsPlanClientForm;
//---------------------------------------------------------------------------
#endif
