﻿//---------------------------------------------------------------------------

#ifndef AddPlanInfUnitH
#define AddPlanInfUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "cxButtonEdit.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
#include "dsICPlanDMUnit.h"
#include "dsRegTemplate.h"
//---------------------------------------------------------------------------
class TAddPlanInfForm : public TForm
{
__published:	// IDE-managed Components
 TPanel *BtnPanel;
 TButton *CancelBtn;
 TButton *ApplyBtn;
 TcxComboBox *InfCB;
 TcxComboBox *ItemTypeCB;
 TLabel *Label1;
 TLabel *Label2;
 TPanel *CompPanel;
 void __fastcall ItemTypeCBPropertiesChange(TObject *Sender);
 void __fastcall FormDestroy(TObject *Sender);
 void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
 void __fastcall ApplyBtnClick(TObject *Sender);
private:	// User declarations
  TdsICPlanDM *FDM;

  UnicodeString FType, FCode, FStrVal, FInf;

  TTagNode *FTempXML;
  TTagNode *FRootNode;
  TTagNode *TmplNode;
  TdsRegTemplate *FTempl;

  bool __fastcall CheckInput();
  bool __fastcall GetInput(TTagNode *itTag, UnicodeString &UID);
public:		// User declarations
 __fastcall TAddPlanInfForm(TComponent* Owner, TdsICPlanDM *ADM);
 __property UnicodeString Type = {read=FType};
 __property UnicodeString Code = {read=FCode};
 __property UnicodeString StrVal = {read=FStrVal};
 __property UnicodeString Inf  = {read=FInf};
};
//---------------------------------------------------------------------------
extern PACKAGE TAddPlanInfForm *AddPlanInfForm;
//---------------------------------------------------------------------------
#endif
