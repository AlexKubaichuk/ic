﻿//---------------------------------------------------------------------------

#ifndef AddIncompMIBPUnitH
#define AddIncompMIBPUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
#include "dsICPlanDMUnit.h"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
class TAddIncompMIBPForm : public TForm
{
__published:	// IDE-managed Components
 TPanel *BtnPanel;
 TButton *CancelBtn;
 TButton *ApplyBtn;
 TPanel *Panel1;
 TLabel *Label1;
 TPanel *Panel2;
 TLabel *Label2;
 TcxComboBox *Vac2CB;
 TcxComboBox *Vac1CB;
 void __fastcall Vac1CBPropertiesChange(TObject *Sender);
private:	// User declarations
  TdsICPlanDM *FDM;
  UnicodeString __fastcall FGetCode1();
  UnicodeString __fastcall FGetCode2();
public:		// User declarations
 __fastcall TAddIncompMIBPForm(TComponent* Owner, TdsICPlanDM *ADM);
  __property UnicodeString Code1 = {read=FGetCode1};
  __property UnicodeString Code2 = {read=FGetCode2};
};
//---------------------------------------------------------------------------
extern PACKAGE TAddIncompMIBPForm *AddIncompMIBPForm;
//---------------------------------------------------------------------------
#endif
