﻿//---------------------------------------------------------------------------

#ifndef AddPreTestUnitH
#define AddPreTestUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
#include "dsICPlanDMUnit.h"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
class TAddPreTestForm : public TForm
{
__published:	// IDE-managed Components
 TPanel *BtnPanel;
 TButton *CancelBtn;
 TButton *ApplyBtn;
 TPanel *Panel2;
 TcxComboBox *InfCB;
 TPanel *Panel3;
 TLabel *Label3;
 TcxMaskEdit *ReakED;
 TLabel *Label2;
 TPanel *Panel6;
 TLabel *Label6;
 TcxMaskEdit *MaxAgeED;
 TPanel *Panel7;
 TLabel *Label7;
 TcxMaskEdit *MinAgeED;
 TPanel *Panel8;
 TLabel *Label8;
 TcxMaskEdit *PauseED;
 TPanel *Panel9;
 TLabel *Label9;
 TcxMaskEdit *ActualED;
 TPanel *Panel10;
 TLabel *Label10;
 TcxComboBox *PreTestType;
 TPanel *Panel11;
 TLabel *Label11;
 TcxComboBox *TestCB;
 void __fastcall MinAgeEDPropertiesValidate(TObject *Sender, Variant &DisplayValue,
          TCaption &ErrorText, bool &Error);
 void __fastcall InfCBPropertiesChange(TObject *Sender);
private:	// User declarations
  TdsICPlanDM *FDM;
  void __fastcall CheckCtrl();

  UnicodeString __fastcall FGetInf();
  UnicodeString __fastcall FGetTest();
  UnicodeString __fastcall FGetReak();
  UnicodeString __fastcall FGetActuals();
  UnicodeString __fastcall FGetPlantype();
  UnicodeString __fastcall FGetPause();
  UnicodeString __fastcall FGetMinAge();
  UnicodeString __fastcall FGetMaxAge();
public:		// User declarations
  __fastcall TAddPreTestForm(TComponent* Owner, TdsICPlanDM *ADM);

  __property UnicodeString Inf = {read = FGetInf};
  __property UnicodeString Test = {read = FGetTest};
  __property UnicodeString Reak = {read = FGetReak};
  __property UnicodeString Actuals = {read = FGetActuals};
  __property UnicodeString Plantype = {read = FGetPlantype};
  __property UnicodeString Pause = {read = FGetPause};
  __property UnicodeString MinAge = {read = FGetMinAge};
  __property UnicodeString MaxAge = {read = FGetMaxAge};

};
//---------------------------------------------------------------------------
extern PACKAGE TAddPreTestForm *AddPreTestForm;
//---------------------------------------------------------------------------
#endif
