﻿//---------------------------------------------------------------------------
#ifndef dsICPlanDMUnitH
#define dsICPlanDMUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Datasnap.DSClientRest.hpp>
//#include <IPPeerClient.hpp>
//---------------------------------------------------------------------------
#include "dsRegED.h"
#include "dsRegConstDef.h"
#include "dsICPlanTypeDef.h"
#include "ExtUtils.h"
#include "AppOptions.h"
#include "dsICSchTypeDef.h"
//---------------------------------------------------------------------------
//namespace IcsReg
//{
//---------------------------------------------------------------------------
typedef bool __fastcall (__closure *TdsPlanCardInsertDataEvent)(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData *ATmplData, bool ASaveTmpl);
typedef UnicodeString __fastcall (__closure *TdsPlanDeletePlanDataEvent)(UnicodeString AId, UnicodeString ARecId);
typedef UnicodeString __fastcall (__closure *TdsPlanGetExtDataEvent)(UnicodeString ASrc);
//---------------------------------------------------------------------------
typedef map<int,int> TIntMap;
typedef map<int,TIntMap*> TIntListMap;
//---------------------------------------------------------------------------
class PACKAGE TdsICPlanDM : public TDataModule
{
__published:	// IDE-managed Components
        void __fastcall DataModuleCreate(TObject *Sender);

private:
  int           FDefaultClassWidth,FULEditWidth;
  bool          FFullEdit;
  bool          FPlanEditable;
  UnicodeString FClassShowExpanded;


  __int64 FExtLPUCode;

  TColor         FRequiredColor;
//  UnicodeString FXMLPath;
  UnicodeString FDataPath;

  UnicodeString FRegistryKey;
  TTagNode      *FRegDefNode;
  TTagNode      *FClassDefNode;
//  TTagNode      *FVacSchDef;

  TTagNode* __fastcall FGetVacSchDef();
  TTagNode* __fastcall FGetTestSchDef();
  TTagNode* __fastcall FGetCheckSchDef();

  TAxeXMLContainer *FXMLList;

  TGetCompValue         FOnGetCompValue;
  TExtBtnGetBitmapEvent FOnGetExtBtnGetBitmap;
  TRegGetFormatEvent    FOnGetFormat;
  TExtBtnClick          FExtBtnClick;
  TExtValidateData      FOnGetDefValues;
  TExtValidateData      FOnExtValidate;
  TExtBtnClick          FOnCtrlDataChange;

  TExtEditData          FExtInsertData;
  TExtEditData          FExtEditData;

  TClassEditEvent       FOnClassAfterInsert;
  TClassEditEvent       FOnClassAfterEdit;
  TClassEditEvent       FOnClassAfterDelete;

  TUnitEvent            FOnOpenPlan;
  TGetReportListEvent   FGetQuickFilterList;
  TGetReportListEvent   FReportList;
  TUnitEvent            FPrintReportClick;
  TcxEditStyleController *FStyleController;

  TdsGetClassXMLEvent FOnGetClassXML;
  TdsGetCountEvent    FOnGetCount;
  TdsGetIdListEvent   FOnGetIdList;
  TdsGetValByIdEvent  FOnGetValById;
  TdsGetValById10Event  FOnGetValById10;
  TdsFindEvent        FOnFind;
  TdsInsertDataEvent  FOnInsertData;
  TdsEditDataEvent    FOnEditData;
  TdsDeleteDataEvent  FOnDeleteData;
//  TdsCreatePlanEvent  FOnCreatePlan;
  TdsDeletePlanEvent  FOnDeletePlan;
  TdsStartCreatePlanEvent FOnStartCreatePlan;
  TdsStopCreatePlanEvent FOnStopCreatePlan;
  TdsICPlanCardOpenEvent FOnCardOpen;
  TdsPreviewDocumentEvent FOnPreviewDoc;
  TNotifyEvent FOnPrePostTestList;
  TdsGetF63Event      FOnGetF63;
  TdsPlanDeletePlanDataEvent FOnDeletePlanItem;

  TdsCheckCreatePlanProgressEvent FOnCheckCreatePlanProgress;
  TdsGetPlanPrintFormatsEvent FOnGetPlanPrintFormats;
  TdsICPlanPrintEvent         FOnPlanPrint;
  TdsICPlanGetUnitDataEvent FOnGetUnitData;
  TPlanTemplateDefEVent   FOnGetPlanTemplateDef;
  TGetReportListEvent    FOnGetReportList;

  TdsPlanCardInsertDataEvent FOnPlanPrivInsert;
  TdsPlanCardInsertDataEvent FOnPlanTestInsert;
  TdsPlanCardInsertDataEvent FOnPlanCheckInsert;
  TdsShowSchEvent  FOnShowVacSch;
  TdsShowSchEvent  FOnShowTestSch;
  TdsShowSchEvent  FOnShowCheckSch;
  TOnGetPlanOptsEvent FOnGetPlanOpts;
  TOnSetPlanOptsEvent FOnSetPlanOpts;
  TdsGetListByIdEvent FOnGetListById;
  TdsGetVacDefDozeEvent FOnGetVacDefDoze;

  TdsPlanGetExtDataEvent FOnGetAddrStr;
  TdsPlanGetExtDataEvent FOnGetMKBStr;
  TdsPlanGetExtDataEvent FOnGetOrgStr;
  TdsPlanGetExtDataEvent FOnGetUchStr;

  //    TClassHashTable *FClassHash;
  TTagNode* __fastcall FGetClassDef();

  TJSONObject* __fastcall kabCDSGetListById(UnicodeString AId);
  void __fastcall FLoadMapData(TJSONObject *AData, TAnsiStrMap &AMap/*, TAnsiStrMap &AMapCode*/);
  void __fastcall FLoadDependMapData(TJSONObject *AData, TIntListMap &AMap, TIntListMap &AMapCode);

public:
//  TICSClassData *FClassData;
  __fastcall TdsICPlanDM(TComponent* Owner);

  TAnsiStrMap VacList;
//    TAnsiStrMap VacListCode;
    TAnsiStrMap ProbList;
  TAnsiStrMap CheckList;
//    TAnsiStrMap ProbListCode;
    TAnsiStrMap InfList;
//    TAnsiStrMap InfListCode;
    TIntListMap InfVacList;
    TIntListMap InfProbList;
    TIntListMap VacInfList;
  TIntListMap ProbInfList;
  TAppOptions *FAppOpt;

  UnicodeString __fastcall FGetInfName(UnicodeString ACode);
  UnicodeString __fastcall FGetVacName(UnicodeString ACode);
  UnicodeString __fastcall FGetTestName(UnicodeString ACode);
  UnicodeString __fastcall FGetCheckName(UnicodeString ACode);

  UnicodeString   DTFormats[3];
  TObject      *UnitList;
  TObject      *UnitPlan;
  TTagNode     *SelRegUnit;
  UnicodeString __fastcall NewGUID();
  void __fastcall PreloadData();

  __property TAxeXMLContainer *XMLList = {read=FXMLList, write=FXMLList};
//  void __fastcall OnGetXML(UnicodeString AId, TTagNode *& ADef);

  UnicodeString __fastcall kabCDSGetClassXML(UnicodeString AId);
  __int64       __fastcall kabCDSGetCount(UnicodeString AId, UnicodeString ATTH, UnicodeString AFilterParam);
  TJSONObject*  __fastcall kabCDSGetCodes(UnicodeString AId, int AMax, UnicodeString ATTH, UnicodeString AFilterParam);
  TJSONObject*  __fastcall kabCDSGetValById(UnicodeString AId, UnicodeString ARecId);
  TJSONObject*  __fastcall kabCDSGetValById10(UnicodeString AId, UnicodeString ARecId);
  UnicodeString __fastcall kabCDSInsert(UnicodeString AId, TJSONObject* AValues, UnicodeString &ARecId);
  UnicodeString __fastcall kabCDSEdit(UnicodeString AId, TJSONObject* AValues, UnicodeString &ARecId);
  UnicodeString __fastcall kabCDSDelete(UnicodeString AId, UnicodeString ARecId);
  UnicodeString __fastcall kabCDSGetF63(UnicodeString AId);

  void __fastcall ShowVacSch();
  void __fastcall ShowTestSch();
  void __fastcall ShowCheckSch();
  void __fastcall GetClassData(UnicodeString ASQL, TStrings *ARetData);
  void __fastcall GetStrClassData(UnicodeString ASQL, TStrings *ARetData);
  void __fastcall LoadInfList(TStringList *AInfList);
  void __fastcall LoadVacList(TStringList *AVacList, bool AFull = true);
  void __fastcall LoadTestList(TStringList *ATestList);
  void __fastcall LoadCheckList(TStringList *ACheckList);

  UnicodeString __fastcall GetVacSch(TStringList *AInfList, UnicodeString AVacCode, __int64 AUCode, UnicodeString &AVacSch, UnicodeString &AVacSchTur);
  UnicodeString __fastcall AgeStr(TDateTime ACurDate, TDateTime ABirthDay);
  UnicodeString __fastcall SetDay(int AYear, int AMonth, int ADay );
  UnicodeString __fastcall GetSchName(UnicodeString ASchId, bool AVacSch);
//  UnicodeString __fastcall CreatePlan(TJSONObject *AVal, UnicodeString &ARecId);
  UnicodeString __fastcall DeletePlan(UnicodeString APlanCode);
  UnicodeString __fastcall StartCreatePlan(TJSONObject *AVal);
  UnicodeString __fastcall StopCreatePlan();
  UnicodeString __fastcall CheckCreatePlanProgress(UnicodeString &ARecId);
  TJSONObject*  __fastcall GetPlanPrintFormats();

  void __fastcall GetSchemeList(int AInfCode, TTagNodeMap &ASchList, TSchemeObjType ASchType);
  void __fastcall SavePatSetting(__int64 AUCode, UnicodeString AData);
  UnicodeString __fastcall GetPlanTemplateDef(int AType);
  UnicodeString __fastcall GetVacDefDoze(TDate APrivDate, TDate APatBirthday, UnicodeString AVacCode);
//  UnicodeString __fastcall GetPatSetting(__int64 AUCode);
//  UnicodeString __fastcall GetPrivVar(__int64 APrivCode);
//  UnicodeString __fastcall GetOtvodInf(__int64 AOtvodCode);

  __property int DefaultClassWidth = {read=FDefaultClassWidth, write=FDefaultClassWidth};
  __property int ULEditWidth = {read=FULEditWidth, write=FULEditWidth};

  __property bool FullEdit = {read=FFullEdit, write=FFullEdit};
  __property UnicodeString RegistryKey = {read=FRegistryKey, write=FRegistryKey};
  __property TTagNode* ClassDef = {read=FGetClassDef};
  __property TTagNode* RegDef = {read=FRegDefNode, write = FRegDefNode};
  __property TTagNode* VacSchDef = {read=FGetVacSchDef};
  __property TTagNode* TestSchDef = {read=FGetTestSchDef};
  __property TTagNode* CheckSchDef = {read=FGetCheckSchDef};

//  __property TdsICClassClient   *Connection = {read=FAppClient, write=FAppClient};

  __property bool PlanEditable = {read=FPlanEditable, write=FPlanEditable};
  __property UnicodeString ClassShowExpanded = {read=FClassShowExpanded, write=FClassShowExpanded};

  __property TcxEditStyleController *StyleController = {read=FStyleController,write=FStyleController};
  __property TColor RequiredColor                    = {read=FRequiredColor,  write=FRequiredColor};

  __property TGetCompValue         OnGetCompValue       = {read=FOnGetCompValue, write=FOnGetCompValue};
  __property TExtBtnGetBitmapEvent OnGetExtBtnGetBitmap = {read=FOnGetExtBtnGetBitmap, write=FOnGetExtBtnGetBitmap};
  __property TRegGetFormatEvent    OnGetFormat          = {read=FOnGetFormat, write=FOnGetFormat};
  __property TExtBtnClick          OnExtBtnClick        = {read=FExtBtnClick, write=FExtBtnClick};
  __property TExtValidateData      OnGetDefValues       = {read=FOnGetDefValues, write=FOnGetDefValues};
  __property TExtValidateData      OnExtValidate        = {read=FOnExtValidate, write=FOnExtValidate};
  __property TExtBtnClick          OnCtrlDataChange     = {read=FOnCtrlDataChange, write=FOnCtrlDataChange};

  __property TExtEditData          OnExtInsertData      = {read=FExtInsertData, write=FExtInsertData};
  __property TExtEditData          OnExtEditData        = {read=FExtEditData, write=FExtEditData};

  __property TdsPlanDeletePlanDataEvent OnDeletePlanItem        = {read=FOnDeletePlanItem, write=FOnDeletePlanItem};

  __property TClassEditEvent OnClassAfterInsert = {read=FOnClassAfterInsert,write=FOnClassAfterInsert};
  __property TClassEditEvent OnClassAfterEdit   = {read=FOnClassAfterEdit,write=FOnClassAfterEdit};
  __property TClassEditEvent OnClassAfterDelete = {read=FOnClassAfterDelete,write=FOnClassAfterDelete};


  __property TUnitEvent          OnOpenPlan         = {read=FOnOpenPlan,write=FOnOpenPlan};
  __property TGetReportListEvent OnQuickFilterList  = {read=FGetQuickFilterList, write=FGetQuickFilterList};
  __property TGetReportListEvent OnReportList       = {read=FReportList, write=FReportList};
  __property TUnitEvent          OnPrintReportClick = {read=FPrintReportClick, write=FPrintReportClick};

  __property TdsGetClassXMLEvent OnGetClassXML  = {read=FOnGetClassXML,write=FOnGetClassXML};
  __property TdsGetCountEvent OnGetCount  = {read=FOnGetCount,write=FOnGetCount};
  __property TdsGetIdListEvent OnGetIdList  = {read=FOnGetIdList,write=FOnGetIdList};
  __property TdsGetValByIdEvent OnGetValById  = {read=FOnGetValById,write=FOnGetValById};
  __property TdsGetValById10Event OnGetValById10  = {read=FOnGetValById10,write=FOnGetValById10};
  __property TdsFindEvent OnFind  = {read=FOnFind,write=FOnFind};
  __property TdsInsertDataEvent OnInsertData  = {read=FOnInsertData,write=FOnInsertData};
  __property TdsEditDataEvent OnEditData  = {read=FOnEditData,write=FOnEditData};
  __property TdsDeleteDataEvent OnDeleteData  = {read=FOnDeleteData,write=FOnDeleteData};

//  __property TdsCreatePlanEvent  OnCreatePlan  = {read=FOnCreatePlan,write=FOnCreatePlan};
  __property TdsDeletePlanEvent  OnDeletePlan  = {read=FOnDeletePlan,write=FOnDeletePlan};
  __property TdsStartCreatePlanEvent OnStartCreatePlan  = {read=FOnStartCreatePlan,write=FOnStartCreatePlan};
  __property TdsStopCreatePlanEvent OnStopCreatePlan  = {read=FOnStopCreatePlan,write=FOnStopCreatePlan};

  __property TdsICPlanCardOpenEvent OnCardOpen  = {read=FOnCardOpen,write=FOnCardOpen};
  __property TdsPreviewDocumentEvent OnPreviewDoc  = {read=FOnPreviewDoc,write=FOnPreviewDoc};

  __property TdsCheckCreatePlanProgressEvent OnCheckCreatePlanProgress  = {read=FOnCheckCreatePlanProgress,write=FOnCheckCreatePlanProgress};

//  __property TdsGetUnitSettingEvent      OnGetPatSetting  = {read=FOnGetPatSetting,write=FOnGetPatSetting};
//  __property TdsGetUnitSettingEvent      OnGetPrivVar     = {read=FOnGetPrivVar,write=FOnGetPrivVar};
//  __property TdsGetUnitSettingEvent      OnGetOtvodInf    = {read=FOnGetOtvodInf,write=FOnGetOtvodInf};

  __property __int64 LPUCode = {read=FExtLPUCode, write=FExtLPUCode};
  __property TdsICPlanGetUnitDataEvent OnGetUnitData = {read=FOnGetUnitData, write=FOnGetUnitData};
  __property TdsGetPlanPrintFormatsEvent OnGetPlanPrintFormats = {read=FOnGetPlanPrintFormats, write=FOnGetPlanPrintFormats};
  __property TdsICPlanPrintEvent OnPlanPrint = {read=FOnPlanPrint, write=FOnPlanPrint};


  __property UnicodeString InfName[UnicodeString ACode] = {read=FGetInfName};
  __property UnicodeString VacName[UnicodeString ACode] = {read=FGetVacName};
  __property UnicodeString TestName[UnicodeString ACode] = {read=FGetTestName};
  __property UnicodeString CheckName[UnicodeString ACode] = {read=FGetCheckName};

  __property  TPlanTemplateDefEVent OnGetPlanTemplateDef = {read=FOnGetPlanTemplateDef, write=FOnGetPlanTemplateDef};

  __property TAppOptions *AppOpt = {read=FAppOpt, write=FAppOpt};
  __property TGetReportListEvent OnGetReportList = {read=FOnGetReportList, write=FOnGetReportList};

  __property TdsPlanCardInsertDataEvent OnPlanPrivInsert = {read=FOnPlanPrivInsert, write=FOnPlanPrivInsert};
  __property TdsPlanCardInsertDataEvent OnPlanTestInsert = {read=FOnPlanTestInsert, write=FOnPlanTestInsert};
  __property TdsPlanCardInsertDataEvent OnPlanCheckInsert = {read=FOnPlanCheckInsert, write=FOnPlanCheckInsert};

  __property TdsShowSchEvent  OnShowVacSch = {read=FOnShowVacSch, write=FOnShowVacSch};
  __property TdsShowSchEvent  OnShowTestSch = {read=FOnShowTestSch, write=FOnShowTestSch};
  __property TdsShowSchEvent  OnShowCheckSch = {read=FOnShowCheckSch, write=FOnShowCheckSch};
  __property TNotifyEvent OnPrePostTestList = {read=FOnPrePostTestList, write=FOnPrePostTestList};

//  __property UnicodeString XMLPath = {read=FXMLPath, write=FXMLPath};
  __property UnicodeString DataPath = {read=FDataPath, write=FDataPath};
  __property TOnGetPlanOptsEvent OnGetPlanOpts = {read=FOnGetPlanOpts, write=FOnGetPlanOpts};
  __property TOnSetPlanOptsEvent OnSetPlanOpts = {read=FOnSetPlanOpts, write=FOnSetPlanOpts};
  __property TdsGetListByIdEvent      OnGetListById  = {read=FOnGetListById,write=FOnGetListById};

  __property TdsPlanGetExtDataEvent   OnGetAddrStr  = {read=FOnGetAddrStr,write=FOnGetAddrStr};
  __property TdsPlanGetExtDataEvent   OnGetMKBStr  = {read=FOnGetMKBStr,write=FOnGetMKBStr};
  __property TdsPlanGetExtDataEvent   OnGetOrgStr  = {read=FOnGetOrgStr,write=FOnGetOrgStr};
  __property TdsPlanGetExtDataEvent   OnGetUchStr  = {read=FOnGetUchStr,write=FOnGetUchStr};
  __property TdsGetF63Event           OnGetF63  = {read=FOnGetF63,write=FOnGetF63};
  __property TdsGetVacDefDozeEvent    OnGetVacDefDoze  = {read=FOnGetVacDefDoze,write=FOnGetVacDefDoze};
};
//}; // end of namespace DataExchange
//using namespace IcsReg;
//---------------------------------------------------------------------------
extern PACKAGE TdsICPlanDM *dsICPlanDM;
//---------------------------------------------------------------------------
#endif

