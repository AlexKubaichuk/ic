object AddPauseForm: TAddPauseForm
  Left = 0
  Top = 0
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1072#1091#1079#1091
  ClientHeight = 155
  ClientWidth = 367
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 114
    Width = 367
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      367
      41)
    object CancelBtn: TButton
      Left = 263
      Top = 6
      Width = 100
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 0
    end
    object ApplyBtn: TButton
      Left = 157
      Top = 6
      Width = 100
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      Enabled = False
      ModalResult = 1
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 361
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 18
      Height = 18
      Margins.Top = 6
      Align = alLeft
      Caption = '(1):'
      ExplicitHeight = 13
    end
    object Type1CB: TcxComboBox
      AlignWithMargins = True
      Left = 27
      Top = 3
      Align = alLeft
      Properties.DropDownListStyle = lsEditFixedList
      Properties.Items.Strings = (
        #1055#1088#1080#1074#1080#1074#1082#1072' '#1086#1090
        #1055#1088#1086#1073#1072
        #1055#1088#1086#1074#1077#1088#1082#1072)
      Properties.OnChange = Type1CBPropertiesChange
      TabOrder = 0
      Width = 111
    end
    object ItemsList1CB: TcxComboBox
      AlignWithMargins = True
      Left = 144
      Top = 3
      Align = alClient
      Properties.DropDownListStyle = lsEditFixedList
      Properties.OnChange = ItemsList1CBPropertiesChange
      TabOrder = 1
      Width = 214
    end
  end
  object Panel2: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 36
    Width = 361
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label2: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 18
      Height = 18
      Margins.Top = 6
      Align = alLeft
      Caption = '(2):'
      ExplicitHeight = 13
    end
    object Type2CB: TcxComboBox
      AlignWithMargins = True
      Left = 27
      Top = 3
      Align = alLeft
      Properties.DropDownListStyle = lsEditFixedList
      Properties.Items.Strings = (
        #1055#1088#1080#1074#1080#1074#1082#1072' '#1086#1090
        #1055#1088#1086#1073#1072
        #1055#1088#1086#1074#1077#1088#1082#1072)
      Properties.OnChange = Type2CBPropertiesChange
      TabOrder = 0
      Width = 111
    end
    object ItemsList2CB: TcxComboBox
      AlignWithMargins = True
      Left = 144
      Top = 3
      Align = alClient
      Properties.DropDownListStyle = lsEditFixedList
      Properties.OnChange = ItemsList1CBPropertiesChange
      TabOrder = 1
      Width = 214
    end
  end
  object Panel3: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 69
    Width = 361
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object Label3: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 70
      Height = 18
      Margins.Top = 6
      Align = alLeft
      Caption = #1055#1072#1091#1079#1072' ('#1076#1085#1077#1081'):'
      ExplicitHeight = 13
    end
    object ValED: TcxMaskEdit
      AlignWithMargins = True
      Left = 79
      Top = 3
      Align = alClient
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d{1,3}'
      Properties.OnChange = ItemsList1CBPropertiesChange
      TabOrder = 0
      Width = 279
    end
  end
end
