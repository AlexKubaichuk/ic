object IPSPlanSettingForm: TIPSPlanSettingForm
  Left = 0
  Top = 0
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1089#1080#1089#1090#1077#1084#1099' '#1087#1083#1072#1085#1080#1088#1086#1074#1072#1085#1080#1103
  ClientHeight = 502
  ClientWidth = 774
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 461
    Width = 774
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      774
      41)
    object CancelBtn: TButton
      Left = 670
      Top = 6
      Width = 100
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 0
    end
    object ApplyBtn: TButton
      Left = 564
      Top = 6
      Width = 100
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      TabOrder = 1
      OnClick = ApplyBtnClick
    end
  end
  object SettingPC: TcxPageControl
    Left = 0
    Top = 0
    Width = 774
    Height = 461
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = SchTS
    Properties.CustomButtons.Buttons = <>
    Properties.Images = SettingIL
    Properties.Options = [pcoAlwaysShowGoDialogButton, pcoFixedTabWidthWhenRotated, pcoGradient, pcoGradientClientArea, pcoRedrawOnResize, pcoUsePageColorForTab]
    Properties.Rotate = True
    Properties.TabHeight = 30
    Properties.TabPosition = tpLeft
    Properties.TabWidth = 170
    ClientRectBottom = 457
    ClientRectLeft = 176
    ClientRectRight = 770
    ClientRectTop = 4
    object cxTabSheet1: TcxTabSheet
      Caption = #1050#1072#1083#1077#1085#1076#1072#1088#1100' '#1087#1088#1080#1074#1080#1074#1086#1082
      ImageIndex = 0
      object cxGroupBox6: TcxGroupBox
        AlignWithMargins = True
        Left = 3
        Top = 3
        Align = alClient
        Caption = #1053#1072#1094#1080#1086#1085#1072#1083#1100#1085#1099#1081' '#1082#1072#1083#1077#1085#1076#1072#1088#1100
        TabOrder = 0
        Height = 447
        Width = 588
        object NacPlanTV: TcxTreeList
          AlignWithMargins = True
          Left = 5
          Top = 21
          Width = 578
          Height = 383
          Align = alClient
          Bands = <
            item
            end>
          Navigator.Buttons.CustomButtons = <>
          OptionsCustomizing.BandCustomizing = False
          OptionsCustomizing.BandHorzSizing = False
          OptionsCustomizing.BandVertSizing = False
          OptionsCustomizing.ColumnCustomizing = False
          OptionsView.Buttons = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GridLineColor = clSilver
          OptionsView.GridLines = tlglBoth
          OptionsView.ShowRoot = False
          TabOrder = 0
          Data = {
            00000500370300000F00000044617461436F6E74726F6C6C6572310500000012
            000000546378537472696E6756616C7565547970651200000054637853747269
            6E6756616C75655479706512000000546378537472696E6756616C7565547970
            6512000000546378537472696E6756616C756554797065120000005463785374
            72696E6756616C7565547970650D000000445855464D5400000200000035043D
            0401010101445855464D5400000400000035043D0435043D0401010101445855
            464D540000030000003A0435043D0401010101445855464D540000060000003A
            0435043D043A0435043D0401010101445855464D540000060000003A0435043D
            043A0435043D0401010101445855464D540000060000003A0435043D043A0435
            043D0401010101445855464D540000060000003A0435043D043A0435043D0401
            010101445855464D540000060000003A0435043D043A0435043D040101010144
            5855464D540000090000003A0435043D043A0435043D043A0435043D04010101
            01445855464D540000040000003D04350435043D0401010101445855464D5400
            000300000043043A04350401010101445855464D5400000300000043043A0435
            0401010101445855464D54000003000000460443043A04010101010D00000000
            00000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF01000000080800
            00000000000000FFFFFFFFFFFFFFFFFFFFFFFF02000000080800000000000000
            00FFFFFFFFFFFFFFFFFFFFFFFF0300000008080000000000000000FFFFFFFFFF
            FFFFFFFFFFFFFF0400000008080000000000000000FFFFFFFFFFFFFFFFFFFFFF
            FF0500000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0600000008
            080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0700000008080000000000
            000000FFFFFFFFFFFFFFFFFFFFFFFF0800000008080000000000000000FFFFFF
            FFFFFFFFFFFFFFFFFF0900000008080000000000000000FFFFFFFFFFFFFFFFFF
            FFFFFF0A00000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0B0000
            0008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0C0000000808000000
            0000000000FFFFFFFFFFFFFFFFFFFFFFFF1A080D000000}
          object cxTreeListColumn1: TcxTreeListColumn
            Caption.Text = #1048#1085#1092#1077#1082#1094#1080#1103
            DataBinding.ValueType = 'String'
            Options.Editing = False
            Options.Focusing = False
            Width = 256
            Position.ColIndex = 0
            Position.RowIndex = 0
            Position.BandIndex = 0
            SortOrder = soAscending
            SortIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeListColumn2: TcxTreeListColumn
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssInactive
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Caption.AlignHorz = taCenter
            Caption.Text = #1055#1088#1080#1074#1080#1074#1082#1072
            DataBinding.ValueType = 'String'
            Position.ColIndex = 1
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object NacPlanTVColumn1: TcxTreeListColumn
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssInactive
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Caption.AlignHorz = taCenter
            Caption.Text = #1055#1088#1086#1073#1072
            DataBinding.ValueType = 'String'
            Position.ColIndex = 2
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object NacPlanTVColumn2: TcxTreeListColumn
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssInactive
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Caption.AlignHorz = taCenter
            Caption.Text = #1055#1088#1086#1074#1077#1088#1082#1072
            DataBinding.ValueType = 'String'
            Position.ColIndex = 3
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object NacPlanTVColumn3: TcxTreeListColumn
            Visible = False
            DataBinding.ValueType = 'String'
            Position.ColIndex = 4
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
        end
        object Panel2: TPanel
          Left = 2
          Top = 407
          Width = 584
          Height = 38
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            584
            38)
          object Button1: TButton
            Left = 109
            Top = 3
            Width = 100
            Height = 25
            Action = actDelPlanInf
            Anchors = [akLeft, akBottom]
            Cancel = True
            TabOrder = 0
          end
          object Button2: TButton
            Left = 3
            Top = 3
            Width = 100
            Height = 25
            Action = actAddPlanInf
            Anchors = [akLeft, akBottom]
            TabOrder = 1
          end
        end
      end
    end
    object SchTS: TcxTabSheet
      Caption = #1057#1093#1077#1084#1099
      ImageIndex = 5
      object TestSch: TcxButton
        AlignWithMargins = True
        Left = 3
        Top = 364
        Width = 588
        Height = 40
        Align = alBottom
        Action = actSchTest
        OptionsImage.Margin = 5
        OptionsImage.Spacing = 10
        SpeedButtonOptions.Transparent = True
        TabOrder = 0
      end
      object CheckSch: TcxButton
        AlignWithMargins = True
        Left = 3
        Top = 410
        Width = 588
        Height = 40
        Align = alBottom
        Action = actSchCheck
        OptionsImage.Margin = 5
        OptionsImage.Spacing = 10
        SpeedButtonOptions.Transparent = True
        TabOrder = 1
      end
      object VacSch: TcxButton
        AlignWithMargins = True
        Left = 3
        Top = 318
        Width = 588
        Height = 40
        Align = alBottom
        Action = actSchVac
        OptionsImage.Margin = 5
        OptionsImage.Spacing = 10
        SpeedButtonOptions.Transparent = True
        TabOrder = 2
      end
      object cxGroupBox4: TcxGroupBox
        AlignWithMargins = True
        Left = 3
        Top = 3
        Align = alClient
        Caption = #1057#1093#1077#1084#1099' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
        TabOrder = 3
        Height = 309
        Width = 588
        object SchPC: TcxPageControl
          Left = 2
          Top = 18
          Width = 584
          Height = 289
          Align = alClient
          TabOrder = 0
          Properties.ActivePage = SchPrivTS
          Properties.CustomButtons.Buttons = <>
          ClientRectBottom = 285
          ClientRectLeft = 4
          ClientRectRight = 580
          ClientRectTop = 24
          object SchPrivTS: TcxTabSheet
            Caption = #1055#1088#1080#1074#1080#1074#1082#1080
            ImageIndex = 0
            object PrivSchDefTV: TcxTreeList
              AlignWithMargins = True
              Left = 3
              Top = 3
              Width = 570
              Height = 255
              Align = alClient
              Bands = <
                item
                end>
              Navigator.Buttons.CustomButtons = <>
              OptionsCustomizing.BandCustomizing = False
              OptionsCustomizing.BandHorzSizing = False
              OptionsCustomizing.BandVertSizing = False
              OptionsCustomizing.ColumnCustomizing = False
              OptionsView.Buttons = False
              OptionsView.ColumnAutoWidth = True
              OptionsView.GridLineColor = clSilver
              OptionsView.GridLines = tlglBoth
              OptionsView.ShowRoot = False
              TabOrder = 0
              object cxTreeListColumn5: TcxTreeListColumn
                Caption.Text = #1048#1085#1092#1077#1082#1094#1080#1103
                DataBinding.ValueType = 'String'
                Options.Editing = False
                Options.Focusing = False
                Width = 160
                Position.ColIndex = 0
                Position.RowIndex = 0
                Position.BandIndex = 0
                SortOrder = soAscending
                SortIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object cxTreeListColumn6: TcxTreeListColumn
                PropertiesClassName = 'TcxButtonEditProperties'
                Properties.Buttons = <
                  item
                    Action = actSetVacSch
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.ViewStyle = vsHideCursor
                Caption.AlignHorz = taCenter
                Caption.Text = #1057#1093#1077#1084#1072
                DataBinding.ValueType = 'String'
                Width = 409
                Position.ColIndex = 1
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object PrivSchDefTVColumn1: TcxTreeListColumn
                Visible = False
                DataBinding.ValueType = 'String'
                Position.ColIndex = 2
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object PrivSchDefTVColumn2: TcxTreeListColumn
                Visible = False
                DataBinding.ValueType = 'String'
                Position.ColIndex = 3
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
            end
          end
          object SchTestTS: TcxTabSheet
            Caption = #1055#1088#1086#1073#1099
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object TestSchDefTV: TcxTreeList
              AlignWithMargins = True
              Left = 3
              Top = 3
              Width = 570
              Height = 255
              Align = alClient
              Bands = <
                item
                end>
              Navigator.Buttons.CustomButtons = <>
              OptionsCustomizing.BandCustomizing = False
              OptionsCustomizing.BandHorzSizing = False
              OptionsCustomizing.BandVertSizing = False
              OptionsCustomizing.ColumnCustomizing = False
              OptionsView.Buttons = False
              OptionsView.ColumnAutoWidth = True
              OptionsView.GridLineColor = clSilver
              OptionsView.GridLines = tlglBoth
              OptionsView.ShowRoot = False
              TabOrder = 0
              object cxTreeListColumn11: TcxTreeListColumn
                Caption.Text = #1048#1085#1092#1077#1082#1094#1080#1103
                DataBinding.ValueType = 'String'
                Options.Editing = False
                Options.Focusing = False
                Width = 160
                Position.ColIndex = 0
                Position.RowIndex = 0
                Position.BandIndex = 0
                SortOrder = soAscending
                SortIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object cxTreeListColumn15: TcxTreeListColumn
                PropertiesClassName = 'TcxButtonEditProperties'
                Properties.Buttons = <
                  item
                    Action = actSetTestSch
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.ViewStyle = vsHideCursor
                Caption.AlignHorz = taCenter
                Caption.Text = #1057#1093#1077#1084#1072
                DataBinding.ValueType = 'String'
                Width = 408
                Position.ColIndex = 1
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object TestSchDefTVColumn1: TcxTreeListColumn
                Visible = False
                DataBinding.ValueType = 'String'
                Position.ColIndex = 2
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object TestSchDefTVColumn2: TcxTreeListColumn
                Visible = False
                DataBinding.ValueType = 'String'
                Position.ColIndex = 3
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
            end
          end
          object SchCheckTS: TcxTabSheet
            Caption = #1055#1088#1086#1074#1077#1088#1082#1080
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object CheckSchDefTV: TcxTreeList
              AlignWithMargins = True
              Left = 3
              Top = 3
              Width = 570
              Height = 255
              Align = alClient
              Bands = <
                item
                end>
              Navigator.Buttons.CustomButtons = <>
              OptionsCustomizing.BandCustomizing = False
              OptionsCustomizing.BandHorzSizing = False
              OptionsCustomizing.BandVertSizing = False
              OptionsCustomizing.ColumnCustomizing = False
              OptionsView.Buttons = False
              OptionsView.ColumnAutoWidth = True
              OptionsView.GridLineColor = clSilver
              OptionsView.GridLines = tlglBoth
              OptionsView.ShowRoot = False
              TabOrder = 0
              object cxTreeListColumn22: TcxTreeListColumn
                Caption.Text = #1048#1085#1092#1077#1082#1094#1080#1103
                DataBinding.ValueType = 'String'
                Options.Editing = False
                Options.Focusing = False
                Width = 160
                Position.ColIndex = 0
                Position.RowIndex = 0
                Position.BandIndex = 0
                SortOrder = soAscending
                SortIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object cxTreeListColumn23: TcxTreeListColumn
                PropertiesClassName = 'TcxButtonEditProperties'
                Properties.Buttons = <
                  item
                    Action = actSetCheckSch
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.ViewStyle = vsHideCursor
                Caption.AlignHorz = taCenter
                Caption.Text = #1057#1093#1077#1084#1072
                DataBinding.ValueType = 'String'
                Width = 408
                Position.ColIndex = 1
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object CheckSchDefTVColumn1: TcxTreeListColumn
                Visible = False
                DataBinding.ValueType = 'String'
                Position.ColIndex = 2
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object CheckSchDefTVColumn2: TcxTreeListColumn
                Visible = False
                DataBinding.ValueType = 'String'
                Position.ColIndex = 3
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
            end
          end
        end
      end
    end
    object PlanPrintFormatTS: TcxTabSheet
      Caption = #1060#1086#1088#1084#1072#1090#1099' '#1087#1077#1095#1072#1090#1080' '#1087#1083#1072#1085#1072
      ImageIndex = 7
      object cxGroupBox2: TcxGroupBox
        AlignWithMargins = True
        Left = 3
        Top = 393
        Align = alBottom
        Caption = #1060#1086#1088#1084#1072#1090' '#1087#1083#1072#1085#1072' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102' '#1076#1083#1103' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
        TabOrder = 0
        Height = 57
        Width = 588
        object OrgDefPlanFormatCB: TcxComboBox
          AlignWithMargins = True
          Left = 5
          Top = 31
          Align = alBottom
          Properties.DropDownListStyle = lsEditFixedList
          TabOrder = 0
          Width = 578
        end
      end
      object cxGroupBox1: TcxGroupBox
        AlignWithMargins = True
        Left = 3
        Top = 3
        Align = alClient
        Caption = #1057#1087#1080#1089#1086#1082' '#1092#1086#1088#1084#1072#1090#1086#1074' '#1087#1083#1072#1085#1072
        TabOrder = 1
        Height = 328
        Width = 588
        object PlanFormatsTV: TcxTreeList
          AlignWithMargins = True
          Left = 5
          Top = 21
          Width = 578
          Height = 271
          Align = alClient
          Bands = <
            item
            end>
          Navigator.Buttons.CustomButtons = <>
          OptionsCustomizing.BandCustomizing = False
          OptionsCustomizing.BandHorzSizing = False
          OptionsCustomizing.BandVertSizing = False
          OptionsCustomizing.ColumnCustomizing = False
          OptionsData.Editing = False
          OptionsSelection.CellSelect = False
          OptionsView.Buttons = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GridLineColor = clSilver
          OptionsView.GridLines = tlglHorz
          OptionsView.ShowRoot = False
          TabOrder = 0
          object cxTreeList1Column1: TcxTreeListColumn
            Caption.Text = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
            DataBinding.ValueType = 'String'
            Width = 256
            Position.ColIndex = 0
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object PlanFormatsTVColumn2: TcxTreeListColumn
            Visible = False
            DataBinding.ValueType = 'String'
            Position.ColIndex = 1
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
        end
        object PlanFormatListEditBtn: TButton
          AlignWithMargins = True
          Left = 5
          Top = 298
          Width = 578
          Height = 25
          Align = alBottom
          Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1089#1087#1080#1089#1086#1082' '#1092#1086#1088#1084#1072#1090#1086#1074' '#1087#1083#1072#1085#1072
          TabOrder = 1
          OnClick = PlanFormatListEditBtnClick
        end
      end
      object cxGroupBox3: TcxGroupBox
        AlignWithMargins = True
        Left = 3
        Top = 337
        Align = alBottom
        Caption = #1060#1086#1088#1084#1072#1090' '#1087#1083#1072#1085#1072' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102' '#1076#1083#1103' '#1091#1095#1072#1089#1090#1082#1072
        TabOrder = 2
        Height = 50
        Width = 588
        object UchDefPlanFormatCB: TcxComboBox
          AlignWithMargins = True
          Left = 5
          Top = 24
          Align = alBottom
          Properties.DropDownListStyle = lsEditFixedList
          TabOrder = 0
          Width = 578
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #1055#1072#1091#1079#1099
      ImageIndex = 1
      object PauseTV: TcxTreeList
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 588
        Height = 409
        Align = alClient
        Bands = <
          item
          end>
        Navigator.Buttons.CustomButtons = <>
        OptionsCustomizing.BandCustomizing = False
        OptionsCustomizing.BandHorzSizing = False
        OptionsCustomizing.BandVertSizing = False
        OptionsCustomizing.ColumnCustomizing = False
        OptionsView.Buttons = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GridLineColor = clSilver
        OptionsView.GridLines = tlglBoth
        OptionsView.ShowRoot = False
        Styles.OnGetContentStyle = PauseTVStylesGetContentStyle
        TabOrder = 0
        OnFocusedNodeChanged = PauseTVFocusedNodeChanged
        Data = {
          00000500A00300000F00000044617461436F6E74726F6C6C6572310800000012
          000000546378537472696E6756616C7565547970651200000054637853747269
          6E6756616C75655479706512000000546378537472696E6756616C7565547970
          6512000000546378537472696E6756616C756554797065120000005463785374
          72696E6756616C75655479706512000000546378537472696E6756616C756554
          79706512000000546378537472696E6756616C75655479706512000000546378
          537472696E6756616C7565547970650D000000445855464D5400000300000046
          0443043A0401010101010101445855464D5400000300000043043A0435040101
          0101010101445855464D5400000300000043043A043504010101010101014458
          55464D540000040000003D04350435043D0401010101010101445855464D5400
          000200000035043D0401010101010101445855464D5400000400000035043D04
          35043D0401010101010101445855464D540000030000003A0435043D04010101
          01010101445855464D540000060000003A0435043D043A0435043D0401010101
          010101445855464D540000060000003A0435043D043A0435043D040101010101
          0101445855464D540000060000003A0435043D043A0435043D04010101010101
          01445855464D540000060000003A0435043D043A0435043D0401010101010101
          445855464D540000090000003A0435043D043A0435043D043A0435043D040101
          0101010101445855464D540000060000003A0435043D043A0435043D04010101
          010101010D0000000000000008080000000000000000FFFFFFFFFFFFFFFFFFFF
          FFFF0100000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF02000000
          08080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF03000000080800000000
          00000000FFFFFFFFFFFFFFFFFFFFFFFF0400000008080000000000000000FFFF
          FFFFFFFFFFFFFFFFFFFF0500000008080000000000000000FFFFFFFFFFFFFFFF
          FFFFFFFF0600000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0700
          000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0800000008080000
          000000000000FFFFFFFFFFFFFFFFFFFFFFFF0900000008080000000000000000
          FFFFFFFFFFFFFFFFFFFFFFFF0A00000008080000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFF0B00000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF
          0C00000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF1A080D000000}
        object cxTreeListColumn7: TcxTreeListColumn
          Caption.Text = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' (1)'
          DataBinding.ValueType = 'String'
          Options.Editing = False
          Options.Focusing = False
          Width = 244
          Position.ColIndex = 0
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxTreeListColumn8: TcxTreeListColumn
          Caption.Text = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' (2)'
          DataBinding.ValueType = 'String'
          Options.Editing = False
          Options.Focusing = False
          Width = 192
          Position.ColIndex = 1
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxTreeList4Column1: TcxTreeListColumn
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.MaskKind = emkRegExprEx
          Properties.EditMask = '\d{1,3}'
          Caption.AlignHorz = taCenter
          Caption.Text = #1047#1085#1072#1095#1077#1085#1080#1077' ('#1076#1085'.)'
          DataBinding.ValueType = 'String'
          Width = 150
          Position.ColIndex = 2
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object PauseTVColumn1: TcxTreeListColumn
          Visible = False
          DataBinding.ValueType = 'String'
          Position.ColIndex = 3
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object PauseTVColumn2: TcxTreeListColumn
          Visible = False
          DataBinding.ValueType = 'String'
          Position.ColIndex = 4
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object PauseTVColumn3: TcxTreeListColumn
          Visible = False
          DataBinding.ValueType = 'String'
          Position.ColIndex = 5
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object PauseTVColumn4: TcxTreeListColumn
          Visible = False
          DataBinding.ValueType = 'String'
          Position.ColIndex = 6
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object PauseTVColumn5: TcxTreeListColumn
          Visible = False
          DataBinding.ValueType = 'String'
          Position.ColIndex = 7
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 415
        Width = 594
        Height = 38
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        DesignSize = (
          594
          38)
        object Button3: TButton
          Left = 109
          Top = 3
          Width = 100
          Height = 25
          Action = actDelPause
          Anchors = [akLeft, akBottom]
          Cancel = True
          TabOrder = 0
        end
        object Button4: TButton
          Left = 3
          Top = 3
          Width = 100
          Height = 25
          Action = actAddPause
          Anchors = [akLeft, akBottom]
          TabOrder = 1
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = #1055#1088#1080#1086#1088#1080#1090#1077#1090#1099' '#1074#1072#1082#1094#1080#1085#1072#1094#1080#1081
      ImageIndex = 2
      object PriorTV: TcxTreeList
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 510
        Height = 447
        Align = alClient
        Bands = <
          item
          end>
        Navigator.Buttons.CustomButtons = <>
        OptionsCustomizing.BandCustomizing = False
        OptionsCustomizing.BandHorzSizing = False
        OptionsCustomizing.BandVertSizing = False
        OptionsCustomizing.ColumnCustomizing = False
        OptionsData.Editing = False
        OptionsSelection.CellSelect = False
        OptionsView.Buttons = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GridLineColor = clSilver
        OptionsView.GridLines = tlglBoth
        OptionsView.ShowRoot = False
        Styles.Inactive = cxStyle1
        Styles.Selection = cxStyle1
        TabOrder = 0
        Data = {
          00000500140300000F00000044617461436F6E74726F6C6C6572310400000012
          000000546378537472696E6756616C7565547970651200000054637853747269
          6E6756616C75655479706512000000546378537472696E6756616C7565547970
          6512000000546378537472696E6756616C7565547970650D000000445855464D
          5400010003000000460443043A040101445855464D540001000300000043043A
          0435040101445855464D540001000300000043043A0435040101445855464D54
          000100040000003D04350435043D040101445855464D54000100020000003504
          3D040101445855464D540001000400000035043D0435043D040101445855464D
          54000100030000003A0435043D040101445855464D54000100060000003A0435
          043D043A0435043D040101445855464D54000100060000003A0435043D043A04
          35043D040101445855464D54000100060000003A0435043D043A0435043D0401
          01445855464D54000100060000003A0435043D043A0435043D04010144585546
          4D54000100090000003A0435043D043A0435043D043A0435043D040101445855
          464D54000100060000003A0435043D043A0435043D0401010D00000000000000
          08080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF01000000080800000000
          00000000FFFFFFFFFFFFFFFFFFFFFFFF0200000008080000000000000000FFFF
          FFFFFFFFFFFFFFFFFFFF0300000008080000000000000000FFFFFFFFFFFFFFFF
          FFFFFFFF0400000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0500
          000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0600000008080000
          000000000000FFFFFFFFFFFFFFFFFFFFFFFF0700000008080000000000000000
          FFFFFFFFFFFFFFFFFFFFFFFF0800000008080000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFF0900000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF
          0A00000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0B0000000808
          0000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0C0000000808000000000000
          0000FFFFFFFFFFFFFFFFFFFFFFFF1A080D000000}
        object PriorTVColumn1: TcxTreeListColumn
          Caption.Text = #8470' '#1087'/'#1087
          DataBinding.ValueType = 'String'
          Options.Editing = False
          Width = 41
          Position.ColIndex = 0
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxTreeListColumn9: TcxTreeListColumn
          Caption.Text = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' (1)'
          DataBinding.ValueType = 'String'
          Options.Editing = False
          Options.Sorting = False
          Width = 467
          Position.ColIndex = 1
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxTreeListColumn10: TcxTreeListColumn
          Visible = False
          Caption.Text = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' (2)'
          DataBinding.ValueType = 'String'
          Width = 155
          Position.ColIndex = 2
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object PriorTVColumn2: TcxTreeListColumn
          Visible = False
          DataBinding.ValueType = 'String'
          Position.ColIndex = 3
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
      end
      object Panel3: TPanel
        Left = 516
        Top = 0
        Width = 78
        Height = 453
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        DesignSize = (
          78
          453)
        object Button5: TButton
          Left = 3
          Top = 20
          Width = 71
          Height = 142
          Action = actUpPrior
          Cancel = True
          ImageAlignment = iaTop
          ImageMargins.Top = 5
          Images = SettingIL
          TabOrder = 0
        end
        object Button6: TButton
          Left = 3
          Top = 168
          Width = 71
          Height = 142
          Action = actDownPrior
          Anchors = [akLeft, akBottom]
          ImageAlignment = iaBottom
          ImageMargins.Bottom = 5
          Images = SettingIL
          TabOrder = 1
        end
      end
    end
    object cxTabSheet4: TcxTabSheet
      Caption = #1057#1086#1074#1084#1077#1089#1090#1080#1084#1086#1089#1090#1100' '#1052#1048#1041#1055
      ImageIndex = 3
      object cxGroupBox7: TcxGroupBox
        AlignWithMargins = True
        Left = 3
        Top = 3
        Align = alClient
        Caption = #1057#1087#1080#1089#1086#1082' '#1085#1077#1089#1086#1074#1084#1077#1089#1090#1080#1084#1099#1093' '#1052#1048#1041#1055
        TabOrder = 0
        Height = 447
        Width = 588
        object IncompTV: TcxTreeList
          AlignWithMargins = True
          Left = 5
          Top = 21
          Width = 578
          Height = 383
          Align = alClient
          Bands = <
            item
            end>
          Navigator.Buttons.CustomButtons = <>
          OptionsCustomizing.BandCustomizing = False
          OptionsCustomizing.BandHorzSizing = False
          OptionsCustomizing.BandVertSizing = False
          OptionsCustomizing.ColumnCustomizing = False
          OptionsData.Editing = False
          OptionsSelection.CellSelect = False
          OptionsView.Buttons = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GridLineColor = clSilver
          OptionsView.GridLines = tlglBoth
          OptionsView.ShowRoot = False
          TabOrder = 0
          object cxTreeListColumn17: TcxTreeListColumn
            Caption.Text = #1052#1048#1041#1055' (1)'
            DataBinding.ValueType = 'String'
            Width = 276
            Position.ColIndex = 0
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeListColumn18: TcxTreeListColumn
            Caption.Text = #1052#1048#1041#1055' (2)'
            DataBinding.ValueType = 'String'
            Width = 300
            Position.ColIndex = 1
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList7Column1: TcxTreeListColumn
            Visible = False
            DataBinding.ValueType = 'String'
            Position.ColIndex = 2
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object IncompTVColumn1: TcxTreeListColumn
            Visible = False
            DataBinding.ValueType = 'String'
            Position.ColIndex = 3
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
        end
        object Panel4: TPanel
          Left = 2
          Top = 407
          Width = 584
          Height = 38
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            584
            38)
          object Button7: TButton
            Left = 109
            Top = 3
            Width = 100
            Height = 25
            Action = actDelIncomMIBP
            Anchors = [akLeft, akBottom]
            Cancel = True
            TabOrder = 0
          end
          object Button8: TButton
            Left = 3
            Top = 3
            Width = 100
            Height = 25
            Action = actAddIncomMIBP
            Anchors = [akLeft, akBottom]
            TabOrder = 1
          end
        end
      end
    end
    object CommSettingTS: TcxTabSheet
      Caption = #1055#1088#1077#1076'/'#1087#1086#1089#1090#1074#1072#1082#1094#1080#1085#1072#1083#1100#1085#1099#1077' '#1087#1088#1086#1073#1099
      ImageIndex = 4
      object PreTestGB: TcxGroupBox
        AlignWithMargins = True
        Left = 3
        Top = 3
        Align = alClient
        Caption = '['#1055#1088#1077#1076'/'#1087#1086#1089#1090']'#1074#1072#1082#1094#1080#1085#1072#1083#1100#1085#1099#1077' '#1087#1088#1086#1073#1099
        TabOrder = 0
        Height = 447
        Width = 588
        object Panel5: TPanel
          Left = 2
          Top = 407
          Width = 584
          Height = 38
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          DesignSize = (
            584
            38)
          object Button9: TButton
            Left = 109
            Top = 2
            Width = 100
            Height = 25
            Action = actDelPreTest
            Anchors = [akLeft, akBottom]
            Cancel = True
            TabOrder = 0
          end
          object Button10: TButton
            Left = 3
            Top = 2
            Width = 100
            Height = 25
            Action = actAddPreTest
            Anchors = [akLeft, akBottom]
            TabOrder = 1
          end
        end
        object PreTestTL: TcxTreeList
          AlignWithMargins = True
          Left = 5
          Top = 21
          Width = 578
          Height = 383
          Align = alClient
          Bands = <
            item
            end>
          Navigator.Buttons.CustomButtons = <>
          OptionsCustomizing.BandCustomizing = False
          OptionsCustomizing.BandHorzSizing = False
          OptionsCustomizing.BandVertSizing = False
          OptionsCustomizing.ColumnCustomizing = False
          OptionsView.Buttons = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GridLineColor = clSilver
          OptionsView.GridLines = tlglBoth
          OptionsView.ShowRoot = False
          TabOrder = 1
          Data = {
            00000500090400000F00000044617461436F6E74726F6C6C6572310B00000012
            000000546378537472696E6756616C7565547970651200000054637853747269
            6E6756616C75655479706512000000546378537472696E6756616C7565547970
            6512000000546378537472696E6756616C756554797065120000005463785374
            72696E6756616C75655479706512000000546378537472696E6756616C756554
            79706512000000546378537472696E6756616C75655479706512000000546378
            537472696E6756616C75655479706512000000546378537472696E6756616C75
            655479706512000000546378537472696E6756616C7565547970651200000054
            6378537472696E6756616C7565547970650D000000445855464D540000020000
            0035043D0401010101010101010101445855464D5400000400000035043D0435
            043D0401010101010101010101445855464D540000030000003A0435043D0401
            010101010101010101445855464D540000060000003A0435043D043A0435043D
            0401010101010101010101445855464D540000060000003A0435043D043A0435
            043D0401010101010101010101445855464D540000060000003A0435043D043A
            0435043D0401010101010101010101445855464D540000060000003A0435043D
            043A0435043D0401010101010101010101445855464D540000060000003A0435
            043D043A0435043D0401010101010101010101445855464D540000090000003A
            0435043D043A0435043D043A0435043D0401010101010101010101445855464D
            540000040000003D04350435043D0401010101010101010101445855464D5400
            000300000043043A04350401010101010101010101445855464D540000030000
            0043043A04350401010101010101010101445855464D54000003000000460443
            043A04010101010101010101010D0000000000000008080000000000000000FF
            FFFFFFFFFFFFFFFFFFFFFF0100000008080000000000000000FFFFFFFFFFFFFF
            FFFFFFFFFF0200000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF03
            00000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF04000000080800
            00000000000000FFFFFFFFFFFFFFFFFFFFFFFF05000000080800000000000000
            00FFFFFFFFFFFFFFFFFFFFFFFF0600000008080000000000000000FFFFFFFFFF
            FFFFFFFFFFFFFF0700000008080000000000000000FFFFFFFFFFFFFFFFFFFFFF
            FF0800000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0900000008
            080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0A00000008080000000000
            000000FFFFFFFFFFFFFFFFFFFFFFFF0B00000008080000000000000000FFFFFF
            FFFFFFFFFFFFFFFFFF0C00000008080000000000000000FFFFFFFFFFFFFFFFFF
            FFFFFF1A080D000000}
          object cxTreeListColumn14: TcxTreeListColumn
            Caption.Text = #1048#1085#1092#1077#1082#1094#1080#1103
            DataBinding.ValueType = 'String'
            Options.Editing = False
            Options.Focusing = False
            Width = 98
            Position.ColIndex = 0
            Position.RowIndex = 0
            Position.BandIndex = 0
            SortOrder = soAscending
            SortIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeListColumn16: TcxTreeListColumn
            Caption.Text = #1055#1088#1086#1073#1072
            DataBinding.ValueType = 'String'
            Options.Editing = False
            Options.Focusing = False
            Width = 91
            Position.ColIndex = 1
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column2: TcxTreeListColumn
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.Items.Strings = (
              #1053#1077' '#1087#1083#1072#1085#1080#1088#1086#1074#1072#1090#1100
              #1055#1083#1072#1085#1080#1088#1086#1074#1072#1090#1100' '#1087#1088#1077#1076#1074#1072#1082#1094#1080#1085#1072#1083#1100#1085#1091#1102' '#1087#1088#1086#1073#1091
              #1055#1083#1072#1085#1080#1088#1086#1074#1072#1090#1100' '#1087#1086#1089#1090#1074#1072#1082#1094#1080#1085#1072#1083#1100#1085#1091#1102' '#1087#1088#1086#1073#1091)
            Caption.Text = #1055#1083#1072#1085#1080#1088#1086#1074#1072#1090#1100
            DataBinding.ValueType = 'String'
            Options.Editing = False
            Width = 109
            Position.ColIndex = 2
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column3: TcxTreeListColumn
            Caption.Text = #1056#1077#1072#1082#1094#1080#1103
            DataBinding.ValueType = 'String'
            Width = 43
            Position.ColIndex = 3
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column4: TcxTreeListColumn
            Caption.Text = #1040#1082#1090#1091#1072#1083#1100#1085#1086#1089#1090#1100
            DataBinding.ValueType = 'String'
            Width = 66
            Position.ColIndex = 4
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column5: TcxTreeListColumn
            Caption.Text = #1055#1072#1091#1079#1072
            DataBinding.ValueType = 'String'
            Width = 49
            Position.ColIndex = 5
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column6: TcxTreeListColumn
            Caption.Text = #1052#1080#1085'. '#1074#1086#1079#1088#1072#1089#1090
            DataBinding.ValueType = 'String'
            Width = 57
            Position.ColIndex = 6
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column7: TcxTreeListColumn
            Caption.Text = #1052#1072#1082#1089'. '#1074#1086#1079#1088#1072#1089#1090
            DataBinding.ValueType = 'String'
            Width = 63
            Position.ColIndex = 7
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column8: TcxTreeListColumn
            Visible = False
            DataBinding.ValueType = 'String'
            Position.ColIndex = 8
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object cxTreeList1Column9: TcxTreeListColumn
            Visible = False
            DataBinding.ValueType = 'String'
            Position.ColIndex = 9
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
          object PreTestTLColumn1: TcxTreeListColumn
            Visible = False
            DataBinding.ValueType = 'String'
            Position.ColIndex = 10
            Position.RowIndex = 0
            Position.BandIndex = 0
            Summary.FooterSummaryItems = <>
            Summary.GroupFooterSummaryItems = <>
          end
        end
      end
    end
  end
  object SettingAL: TActionList
    Images = SettingIL
    Left = 36
    Top = 295
    object actPredPostVacTest: TAction
      Caption = #1055#1088#1077#1076'/'#1087#1086#1089#1090#1074#1072#1082#1094#1080#1085#1072#1083#1100#1085#1099#1077' '#1087#1088#1086#1073#1099
      ImageIndex = 4
    end
    object actSetVacSch: TAction
      Caption = 'actSetSch'
      OnExecute = actSetVacSchExecute
    end
    object actSetTestSch: TAction
      Caption = 'actSetTestSch'
      OnExecute = actSetTestSchExecute
    end
    object actSetCheckSch: TAction
      Caption = 'actSetCheckSch'
      OnExecute = actSetCheckSchExecute
    end
    object actSchVac: TAction
      Caption = #1057#1093#1077#1084#1099' '#1087#1088#1080#1084#1077#1085#1077#1085#1080#1103' '#1074#1072#1082#1094#1080#1085
      ImageIndex = 5
      OnExecute = actSchVacExecute
    end
    object actSchTest: TAction
      Caption = #1057#1093#1077#1084#1099' '#1087#1088#1080#1084#1077#1085#1077#1085#1080#1103' '#1087#1088#1086#1073
      ImageIndex = 5
      OnExecute = actSchTestExecute
    end
    object actSchCheck: TAction
      Caption = #1057#1093#1077#1084#1099' '#1087#1088#1080#1084#1077#1085#1077#1085#1080#1103' '#1087#1088#1086#1074#1077#1088#1086#1082
      ImageIndex = 5
      OnExecute = actSchCheckExecute
    end
    object actAddPlanInf: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = actAddPlanInfExecute
    end
    object actDelPlanInf: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = actDelPlanInfExecute
    end
    object actAddPause: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = actAddPauseExecute
    end
    object actDelPause: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Enabled = False
      OnExecute = actDelPauseExecute
    end
    object actUpPrior: TAction
      Caption = #1042#1074#1077#1088#1093
      ImageIndex = 8
      OnExecute = actUpPriorExecute
    end
    object actDownPrior: TAction
      Caption = #1042#1085#1080#1079
      ImageIndex = 9
      OnExecute = actDownPriorExecute
    end
    object actAddIncomMIBP: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = actAddIncomMIBPExecute
    end
    object actDelIncomMIBP: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = actDelIncomMIBPExecute
    end
    object actAddPreTest: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = actAddPreTestExecute
    end
    object actDelPreTest: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = actDelPreTestExecute
    end
  end
  object SettingIL: TcxImageList
    Height = 32
    Width = 32
    FormatVersion = 1
    DesignInfo = 23527455
    ImageInfo = <
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0001000000020000000400000005000000050000000500000005000000050000
          0005000000060000000600000006000000060000000600000006000000060000
          0006000000060000000600000006000000060000000600000006000000070000
          0007000000070000000600000005000000030000000100000001000000010000
          0003000000090000000F00000013000000140000001400000014000000150000
          0015000000150000001600000016000000160000001600000017000000170000
          0017000000180000001800000018000000180000001900000019000000190000
          001A0000001A00000019000000140000000D0000000500000001000000020000
          00092F211D57956B5DEAA47565FFA47565FFA37463FFA17363FFA17262FFA071
          62FFA17161FFA07161FFA07060FF9F7060FF9E6F60FF9E6E5FFF9E6E5EFF9D6D
          5EFF9D6D5EFF9C6C5DFF9C6C5CFF9C6C5CFF9B6B5CFF9C6B5BFF9B6A5BFF9B6A
          5BFF9B695BFF9A695AFF8C5F52EB2B1D195E0000000C00000003000000030000
          000D9F7464F6DCC4B0FFE1CCB8FFE1CCB8FFE1CCB7FFE1CCB8FFE1C9B7FFE1CC
          B7FFDEC9B4FFE1C9B7FFDEC9B4FFDEC9B4FFDEC9B4FFDDC7B1FFDDC7B1FFDEC9
          B4FFDEC8B4FFDDC7B1FFDDC7B1FFDDC7B1FFDDC7B1FFDDC7B1FFDDC7B1FFDDC7
          B1FFDDC7B1FFDDC7B1FFD7BEA9FF946556F70000001400000005000000040000
          0010AA7B6CFFCCA390FFCA9E8BFFC99E8CFFC99D8AFFC99C8AFFC89B89FFC79B
          88FFC69887FFC69787FFC59785FFC49684FFC49683FFC49482FFC49382FFC393
          80FFC29280FFC1917EFFC1907DFFC08E7CFFC08E7BFFBF8C7AFFBE8C7AFFBE8B
          79FFBD8B79FFBD8A76FFC08F7CFF9D6C5CFF0000001800000006000000040000
          0010B08272FFEFE3D8FFFCFDF5FFFCFDF5FFFCFDF5FFFCFCF4FFFCFCF4FFFBFC
          F3FFFCFBF3FFFBFBF3FFFBFAF2FFFAFAF1FFFAF9F1FFFBF9F1FFFAF9F0FFF9F8
          EFFFF9F8EFFFF9F7EEFFF9F6EEFFF8F6EDFFF8F5ECFFF7F5ECFFF7F4EBFFF8F3
          EBFFF8F3EAFFF6F2EAFFE6D4C9FFA37262FF0000001800000006000000040000
          000FB18474FFF6F0E9FFF1E7DFFFF2E7DFFFF1E7DEFFF1E7DEFFF1E7DEFFF1E6
          DDFFF1E6DEFFF0E6DEFFF0E6DDFFF1E6DDFFF0E6DDFFF0E6DCFFF0E6DCFFF0E6
          DCFFF0E6DBFFEFE6DBFFF0E5DBFFF0E6DBFFF0E5DAFFF0E5DAFFEFE5DAFFEFE4
          DAFFEFE5DAFFEFE3D9FFF2E8E0FFA47363FF0000001700000006000000040000
          000EB28575FFF6EFE9FFF4ECE5FFF5ECE4FFF4EBE4FFF5EBE3FFF4EBE3FFF4EB
          E3FFF4EBE2FFF4EBE2FFF4EAE1FFF3EAE2FFF4EAE2FFF4EAE1FFF3EAE1FFF3EA
          E0FFF4EAE0FFF3EAE0FFF3EAE0FFF2EADFFFF3E9DFFFF3E9DFFFF3E9DFFFF2E9
          DFFFF3E9DEFFF2E8DEFFF2E8DFFFA57463FF0000001700000006000000030000
          000EB38676FFF8F2ECFFF6EEE7FFF6EFE7FFF6EEE6FFF6EEE6FFF6EEE6FFF5EE
          E6FFF5EEE5FFF5EEE5FFF5EDE4FFF5EDE4FFF5ECE4FFF4EDE3FFF5ECE3FFF4EC
          E3FFF5ECE3FFF4EBE2FFF4EBE3FFF4ECE2FFF4EBE2FFF4EBE1FFF4EBE1FFF4EA
          E1FFF4EAE1FFF3E9E0FFF4EBE2FFA67565FF0000001600000006000000030000
          000DB58878FFF9F4EEFFF7EFE8FFF6F0E8FFC19B8BFFC09A8AFFF1E6DEFFC099
          89FFBF9788FFF1E5DDFFBF9687FFBE9585FFF0E4DCFFBC9584FFBC9483FFEFE3
          DAFFBB9182FFBA9182FFEFE3D9FF5867DBFF5865DAFFE5DEE2FF5664DAFF5664
          DAFFF4EAE1FFF4EAE2FFF5EDE4FFA67566FF0000001500000005000000030000
          000CB68A7AFFFAF5EFFFF7F0E9FFF7F1E9FFC39E8DFFC29C8CFFF1E7DFFFC09B
          8AFFC09A8AFFF1E6DEFFBF9889FFBF9988FFF1E5DDFFBE9587FFBE9585FFF0E5
          DCFFBB9584FFBB9482FFEFE4DBFF5A69DCFF5A69DCFFE5DFE2FF5967DBFF5866
          DAFFF4EBE3FFF4ECE2FFF5EDE5FFA87767FF0000001400000005000000030000
          000CB88C7CFFF9F5F0FFF8F1EAFFF7F0EAFFF2E8E1FFF2E9E0FFF6EFE8FFF2EB
          E4FFEFE8E1FFEEE7E0FFEEE7DFFFEDE5DEFFECE6DEFFECE6DEFFEFE7E1FFF3EC
          E4FFF0E4DCFFF0E4DCFFEFE4DBFFE7E0E3FFE6E0E4FFE6DFE3FFE5DFE2FFE5DF
          E3FFF5ECE3FFF5ECE3FFF6EEE6FFA87869FF0000001400000005000000030000
          000BB88E7EFFFAF6F1FFF7F2EBFFF8F1EBFFC6A192FFC5A091FFF3EDE6FFC6A1
          92FFC5A091FFF3EDE6FFC6A192FFC5A091FFF3EDE6FFBF9789FFBF9788FFEFE8
          E1FFBF9789FFBF9788FFF1E5DDFF6070DFFF5F6FDFFFE7E0E4FF5E6DDEFF5D6C
          DDFFF5EDE4FFF5ECE4FFF6EEE6FFA97969FF0000001300000005000000030000
          000ABA8F80FFFAF7F2FFF8F2ECFFF8F3ECFFC7A495FFC7A393FFF3EDE6FFC7A4
          95FFC7A393FFF3EDE6FFC7A495FFC7A393FFF3EDE6FFC09A8BFFC09A8AFFECE6
          DFFFC09A8BFFC09A8AFFF1E6DEFF6273E0FF6173E0FFE7E1E6FF6071DFFF6070
          DEFFF5EDE5FFF5EDE5FFF6EFE8FFAA7A6BFF0000001200000005000000020000
          000ABB9181FFFAF7F2FFF8F3EDFFF8F3EDFFF4EBE4FFF3EBE4FFF3EDE7FFF4EB
          E4FFF3EBE4FFF3EDE7FFF4EBE4FFF3EBE4FFF3EDE7FFF1E7E0FFF1E7DFFFEDE7
          E0FFF1E7E0FFF1E7DFFFF1E8DFFFE7E4E7FFE8E3E6FFE7E3E6FFE7E2E6FFE7E2
          E6FFF6EEE6FFF5EEE6FFF7F0E8FFAC7D6CFF0000001200000004000000020000
          0009BD9282FFFBF8F4FFF9F4EEFFF9F4EEFFC9A89AFFC9A899FFF5EFE9FFC9A8
          9AFFC9A899FFF5EFE9FFC9A89AFFC9A899FFF5EFE9FFC49F8EFFC39E8EFFEEE8
          E1FFC49F8EFFC39E8EFFF2E8DFFF687AE3FF6879E3FFE8E4E8FF6678E1FF6577
          E1FFF6EEE7FFF6EFE7FFF7F1E9FFAD7E6EFF0000001100000004000000020000
          0008BE9484FFFBF8F4FFF9F5F0FFFAF5EFFFCBAA9BFFCAAA9BFFF5F0EAFFCBAA
          9BFFCAAA9BFFF5F0EAFFCBAA9BFFCAAA9BFFF5F0EAFFC5A091FFC4A090FFEFE9
          E4FFC5A091FFC4A090FFF2E8E1FF6B7EE4FF6B7EE4FFE8E5E9FF697BE3FF687A
          E2FFF6EFE8FFF6EFE8FFF7F1EAFFAE7F6FFF0000001000000004000000020000
          0008BF9685FFFCF8F5FFFAF5F1FFFAF5F0FFF6EEE8FFF6EEE7FFF6F1ECFFF6EE
          E8FFF6EEE7FFF6F1ECFFF6EEE8FFF6EEE7FFF6F1ECFFF3EAE2FFF3E9E2FFF1EB
          E6FFF3EAE2FFF3E9E2FFF3E9E2FFEAE5EAFFEAE5EAFFEAE5EAFFE9E5E9FFE9E5
          E9FFF7F0E9FFF7F0E8FFF8F1EBFFAF8170FF0000000F00000004000000020000
          0007C09888FFFCFAF6FFFAF7F2FFFAF6F1FFCEAFA0FFCEAEA0FFF8F3EEFFCEAF
          A0FFCEAEA0FFF8F3EEFFCEAFA0FFCEAEA0FFF8F3EEFFC9A596FFC7A595FFF2ED
          E7FFC9A596FFC7A595FFF3EAE3FF7085E7FF6F84E6FFEAE6EBFF6E82E6FF6D81
          E5FFF7F0EAFFF7F0E9FFF8F3ECFFB08272FF0000000F00000004000000020000
          0006C19989FFFCFAF7FFFAF7F2FFFAF6F2FFD0B1A3FFD0B1A1FFF9F5F0FFD0B1
          A3FFD0B1A1FFF9F5F0FFD0B1A3FFD0B1A1FFF9F5F0FFCAA89AFFC9A699FFF6F1
          EBFFCAA89AFFC9A699FFF3EBE5FF7388E8FF7287E7FFEBE8EBFF7185E7FF7184
          E6FFF8F2EBFFF7F1EBFFF8F3EEFFB18474FF0000000E00000004000000010000
          0006C39B8AFFFCFAF8FFFBF8F3FFFBF8F3FFFBF7F3FFFAF7F2FFFBF6F2FFFAF5
          F1FFF8F4EFFFF8F4EFFFF7F3EEFFF7F3EDFFF6F2ECFFF5F1EBFFF7F2EDFFF8F3
          EEFFFAF4EFFFF9F4EEFFF9F4EEFFF9F4EEFFF8F3EDFFF8F3EDFFF8F3ECFFF8F2
          ECFFF8F2ECFFF8F2ECFFF9F4EEFFB28576FF0000000D00000003000000010000
          0005C39C8CFFFCFAF9FFFBF8F4FFFBF8F4FFFBF8F4FFFBF8F4FFFBF7F4FFFAF7
          F3FFFAF7F2FFFBF7F2FFFAF6F2FFFAF6F1FFFAF6F1FFFAF5F1FFFAF5F0FFFAF5
          F0FFFAF5F0FFFAF5EFFFF9F5EFFFF9F4EEFFF9F4EFFFF9F4EEFFF9F4EEFFF9F3
          EDFFF8F3EDFFF8F3EDFFF9F4EFFFB48778FF0000000D00000003000000010000
          0005C49D8DFFFDFCFBFFFDFBF8FFFDFBF8FFFCFAF8FFFDFAF7FFFDFBF7FFFDF9
          F7FFFCFAF7FFFCFAF6FFFBF9F6FFFCF9F6FFFCF8F5FFFBF9F5FFFCF9F5FFFBF8
          F5FFFBF8F5FFFBF8F4FFFBF8F5FFFBF8F4FFFBF8F3FFFBF8F3FFFBF7F3FFFBF7
          F2FFFBF6F2FFFBF6F2FFFBF8F4FFB58979FF0000000C00000003000000010000
          0004444BBBFF9EADF0FF6B81E6FF6B80E6FF6A7FE5FF687CE4FF677BE4FF677A
          E3FF6578E2FF6476E2FF6375E1FF6274E1FF6172E0FF6070DFFF5E6FDFFF5D6D
          DEFF5C6BDEFF5B69DDFF5968DCFF5967DBFF5765DAFF5663DAFF5561DAFF5460
          D8FF525FD8FF515DD7FF6872DEFF3336A9FF0000000B00000003000000010000
          0004474EBDFFA0AFF1FF6D83E8FF6C82E7FF6B80E6FF8093EAFF8799EBFF7E91
          E9FF677AE4FF6679E3FF6577E3FF6376E1FF6274E1FF6173E0FF6171E0FF5F6F
          DFFF5E6EDEFF5D6CDDFF5B6BDDFF5B69DCFF717DE1FF7783E4FF6E7BE1FF5563
          D9FF5460D9FF5360D8FF6B76DFFF3639ACFF0000000B00000003000000010000
          00034951C0FFA0B2F2FF6F85E8FF6E84E7FF5F73D7FF3E3D7BFF29234EFF2B2B
          61FF596DD1FF677BE4FF667AE4FF6578E2FF6476E2FF6375E2FF6273E1FF6172
          E0FF6070DFFF5E6FDEFF5E6DDEFF505ECFFF3C3879FF29234EFF28275EFF4B57
          C8FF5663DAFF5562D9FF6D78E0FF383CAEFF0000000A00000003000000010000
          00034C54C3FFA3B4F2FF778DEAFF7086E8FF5B6FD3FF5D558CFFB7A194FF3F37
          6CFF5667CCFF697DE5FF687CE4FF677AE4FF6679E3FF6478E3FF6375E1FF6274
          E1FF6273E0FF6071DFFF5F6FDFFF4C5BC9FF5D558CFFB7A194FF3F376CFF4753
          C1FF5866DBFF5663DAFF6F7BE1FF3A3FB0FF0000000900000002000000000000
          00024B53B6ED8E9DE7FFA4B5F2FFA3B3F2FF8D9DE1FF645B91FFAA9284FF473F
          74FF8795D9FF9DADF0FF9CACF0FF9BAAEFFF9BA9EEFF9AA7EEFF98A6EEFF97A5
          EDFF96A4EDFF94A2ECFF93A1ECFF7D8AD9FF645B91FFAA9284FF473F74FF7681
          D2FF8C97E9FF8C97E8FF7680DBFF3C3FA5EE0000000600000002000000000000
          0001141631413F479ECA4F58C7FF4F58C7FF4048B4FF6A6399FFFFFEF3FF5048
          7FFF3D44ADFF4B54C2FF4A52C2FF4A52C1FF4951C0FF4950C0FF484FBFFF474F
          BEFF464EBEFF464DBDFF464CBCFF383DA9FF6A6399FFFFFEF3FF50487FFF3539
          A3FF4248B8FF4147B7FF32368FCC10112D450000000400000001000000000000
          000000000001000000010000000200000002000000088A7368FFDACEC5FF735A
          50FF0000000D0000000300000003000000040000000400000004000000040000
          00040000000500000005000000050000000B8A7368FFDACEC5FF735A50FF0000
          0010000000060000000600000005000000030000000100000000000000000000
          000000000000000000000000000000000000000000026C5B54C090796FFF6655
          4EC2000000060000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000036C5B54C090796FFF66554EC20000
          0006000000010000000100000001000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000002000000030000
          0003000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000200000003000000030000
          0001000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000010000000100000001000000010000000000000000000000010000
          0002000000040000000600000006000000060000000600000006000000060000
          0006000000060000000700000007000000070000000700000007000000070000
          0007000000070000000700000007000000070000000700000007000000070000
          0007000000070000000700000006000000040000000200000001000000020000
          00070000000F0000001500000018000000190000001900000019000000190000
          001900000019000000190000001A0000001A0000001A0000001A0000001A0000
          001A0000001A0000001A0000001B0000001B0000001B0000001B0000001B0000
          001B0000001B0000001B00000018000000110000000800000002000000040000
          000F7B5A4FC0AB7E6EFFAB7E6EFFAB7E6EFFAA7D6EFFAB7D6DFFAA7D6DFFAA7D
          6CFFAB7C6CFFAA7C6CFFAA7C6CFFAA7C6CFFAA7B6CFFAA7B6CFFA97B6BFFA97B
          6BFFA97B6BFFA97B6BFFA97B6AFFA97B6BFFA97A6AFFA97A6AFFA97A6AFFA879
          6AFFA97A6AFFA87969FFA87969FF78574CC10000001100000004000000050000
          0014AD8070FFFAF5F1FFFAF5F1FFFAF5F1FFF9F4F1FFF9F4F1FFF9F4F0FFF9F4
          F0FFF9F4F0FFF9F3F0FFF9F3F0FFF9F3F0FFF8F3EFFFF9F3EFFFF9F3EFFFF9F3
          EFFFF9F2EEFFF9F2EEFFF9F2EEFFF8F2EEFFF9F1EEFFF8F1EDFFF8F1EDFFF8F1
          ECFFF8F1ECFFF8F1EDFFF8F1ECFFA97A6AFF0000001700000006000000060000
          0016AE8172FFFAF5F2FFF6ECE7FFF5ECE6FFF5ECE6FFF5ECE6FFF5ECE6FFF6EC
          E6FFF5ECE6FFF5ECE6FFF5ECE6FFF5ECE5FFF5ECE5FFF5ECE6FFF5EBE5FFF5EC
          E5FFF5ECE5FFF5ECE5FFF5ECE5FFF4EBE5FFF4EBE4FFF4EBE5FFF5EBE4FFF4EB
          E4FFF5EBE4FFF4EBE4FFF8F2EDFFAA7C6BFF0000001900000007000000050000
          0016AF8373FFFAF7F4FFF5EEE8FFF6EDE7FFF5EDE7FFF5EDE7FFF6EDE7FFF6ED
          E7FFF6ECE7FFF5EDE7FFF5ECE7FFF6ECE6FFF5ECE7FFF5ECE6FFF5ECE6FFF5EC
          E6FFF5ECE6FFF5ECE5FFF5ECE6FFF5ECE5FFF5ECE5FFF5ECE5FFF5ECE5FFF5EB
          E5FFF4EBE5FFF4EBE4FFF8F2EEFFAA7D6DFF0000001900000006000000050000
          0015B18576FFFBF7F4FFF6EEE8FFF6EDE8FFF6EDE8FFF6EEE8FFF5EDE8FFF6ED
          E7FFF6EDE8FFF5EDE7FFF5EDE7FFF5EDE7FFF6EDE6FFF5EDE6FFF5EDE6FFF5EC
          E7FFF5ECE6FFF5ECE7FFF5ECE6FFF5EDE6FFF5ECE6FFF5ECE6FFF5ECE5FFF5EC
          E6FFF5ECE6FFF5ECE5FFF9F3EFFFAB7E6EFF0000001900000006000000050000
          0014B28877FFFBF8F5FFF6EFE9FFF6EEE9FFF7EEE8FFF7EEE9FFF6EEE9FFF6EE
          E9FFF6EEE8FFF6EDE8FFF6EEE8FFF6EEE8FFF6EEE7FFF6EEE8FFF5EDE8FFF5ED
          E7FFF6EDE7FFF6EDE7FFF5EDE7FFF5ECE7FFF6EDE7FFF6ECE6FFF5EDE6FFF6EC
          E7FFF5ECE7FFF5ECE6FFF9F4F1FFAD7F71FF0000001800000006000000050000
          0014B4897AFFB58B7CFFB58B7BFFB58A7BFFB48A7BFFB48A7BFFB48A7BFFB48A
          7AFFB3897AFFB38979FFB38979FFB38879FFB38879FFB38878FFB28778FFB287
          77FFB28677FFB18777FFB18677FFB18677FFB18676FFB08676FFB08576FFB085
          75FFB08475FFB08475FFB08475FFAE8272FF0000001700000006000000050000
          0013B58B7CFFFCF9F7FFF7EFEBFFF7EFEAFFF7F0EBFFF7F0EAFFF6F0EAFFF7EF
          EAFFF6EFEAFFF6EFE9FFF7EFEAFFF7EFE9FFF7EFE9FFF7EFE9FFF7EEEAFFF6EF
          E9FFF6EEE9FFF6EEE9FFF6EEE8FFF6EEE8FFF6EEE8FFF6EDE8FFF6EDE8FFF6EE
          E8FFF6EEE8FFF6EDE7FFFAF6F2FFAF8374FF0000001700000006000000050000
          0012B78E7FFFFCFAF8FFF7F0EBFFF8F0ECFFF8F0EBFFCFC3BDFFF7F0EBFFF7F0
          EBFFF7EFEBFFF8F0EBFFF7EFEBFFF7EFEAFFF7EFEAFFF7EFEAFFF7EFEAFFF7EF
          EAFF328265FF328264FF328265FF328264FF328164FF328265FF328265FF3282
          65FF328264FFF6EEE8FFFBF7F4FFB18676FF0000001600000006000000040000
          0012B99081FFFDFAF9FFF8F1EDFFF8F1ECFFA7958FFF573B33FFA6958EFFF8F1
          ECFFF8F1ECFFF8F0ECFFF7F0ECFFF7F0EBFFF7F0EBFFF7EFEBFFF7F0EBFFF8EF
          EAFF35896CFF77D7BAFF51CAA5FF4EC9A2FF4DC7A0FF4BC69FFF48C49DFF5DCC
          A9FF35896CFFF7EEE9FFFBF7F5FFB38878FF0000001500000005000000040000
          0011BA9384FFFDFBFAFFF8F2EEFFAA9790FF5B3F37FF5A3F36FF593E35FFA896
          90FFF8F1EDFFF8F1EDFFF7F0ECFFF7F1ECFFF8F0ECFFF8F1ECFFF8F0ECFFF7F0
          ECFF399273FF8DE2CBFF62D3B3FF60D2B0FF5DD0AFFF5ACFACFF58CEAAFF75D7
          BAFF399273FFF7EFEAFFFBF8F6FFB48A7AFF0000001500000005000000040000
          0010BB9485FFFDFBFAFFF9F2EFFFF9F2EEFFF9F2EEFF5D4238FFF9F2EEFFF8F1
          EEFFF8F2EDFFF8F1EEFFF9F1EDFFF8F1EDFFF8F1EDFFF8F1EDFFF8F1EDFFF8F1
          EDFF3D9A7CFFA2EAD9FF9FE9D7FF9CE8D5FF98E6D3FF95E5D0FF90E3CDFF8DE2
          CCFF3C9A7BFFF7F0EBFFFCF9F7FFB58C7CFF0000001400000005000000040000
          0010BD9788FFFDFCFBFFF9F3F0FFF9F3F0FFF9F3EFFF60453AFFF9F3EFFFF8F3
          EFFFF9F2EFFFF9F2EEFFF9F3EEFFF8F3EEFFF8F2EEFFF9F2EEFFF8F2EEFFF8F2
          EEFF40A283FF40A283FF40A283FF40A283FF40A283FF40A283FF40A282FF40A2
          83FF40A283FFF8F0ECFFFCF9F7FFB78F7FFF0000001300000005000000040000
          000FBE998AFFFEFCFCFFF9F4F0FFF9F4F1FFF9F3F0FF65493EFFF9F3F0FFF9F3
          EFFFF9F3EFFFF9F3F0FFF9F3EFFFF9F3EFFFF9F3EFFFF9F2EFFFF9F2EFFFF9F2
          EEFFF9F2EEFFF9F2EEFFF8F2EEFFF8F2EEFFF8F2EEFFF9F1EDFFF8F2EDFFF9F1
          EDFFF8F1EEFFF8F1EDFFFCFAF8FFB89181FF0000001300000005000000040000
          000EC19B8DFFFEFDFCFFF9F5F2FFFAF4F1FFF9F5F1FF684D40FFFAF4F1FFFAF4
          F1FFF9F4F0FFF9F4F1FFB3815AFFAF7A52FFAE7852FFAD7751FFAC7750FFAC75
          4EFFAB754EFFAA744DFFAA734CFFA9724BFFA8714AFFA87049FFF9F2EEFFF9F2
          EFFFF9F2EEFFF8F2EEFFFDFBF9FFBB9383FF0000001200000005000000030000
          000EC29D8FFFFEFDFDFFFAF5F3FFFAF5F3FFFAF5F2FF6C5144FFFAF4F2FFFAF5
          F2FFFAF5F2FFF9F5F1FFBA8B64FFE7CBA0FFE2BF8EFFE1BF8EFFE1BE8DFFE1BE
          8CFFE1BE8CFFE1BE8CFFE0BE8BFFE0BC8BFFE2C191FFAD7750FFF9F3EFFFF9F3
          EFFFF9F3EFFFF9F2EFFFFDFBFAFFBC9586FF0000001100000004000000030000
          000DC4A092FFFEFDFDFFFBF6F4FFB7A79FFF715648FF715547FF705446FFB5A5
          9DFFFAF6F3FFFAF5F2FFC0946DFFEAD4ADFFE3C594FFE3C393FFE2C393FFE2C3
          93FFE2C393FFE2C291FFE2C191FFE2C191FFE6CA9FFFB17D57FFF9F4F0FFF9F4
          F0FFF9F3F0FFFAF3F0FFFDFBFBFFBD9888FF0000001100000004000000030000
          000DC5A293FFFFFEFDFFFBF7F5FFFBF6F4FFB9A9A0FF75594AFFB8A8A0FFFBF6
          F3FFFBF5F3FFFBF5F3FFC59B74FFEEDBB9FFEDDAB7FFEDD9B6FFEDD9B4FFECD7
          B3FFEBD6B2FFEBD6B0FFEBD5AEFFEAD4ADFFEAD2ABFFB5855EFFFAF5F1FFFAF4
          F1FFF9F4F1FFFAF4F1FFFDFCFBFFBF9A8BFF0000001000000004000000030000
          000CC6A395FFFEFEFEFFFCF7F6FFFBF7F5FFFCF7F5FFDCD3CDFFFBF6F5FFFBF7
          F5FFFBF7F5FFFBF7F4FFC9A17BFFC8A17AFFC8A17AFFC8A079FFC8A079FFC89F
          78FFC89F78FFC79F78FFC79F77FFC79E77FFC69D76FFC69D76FFFAF5F2FFFAF5
          F2FFFAF5F2FFFAF5F2FFFEFDFCFFC19C8DFF0000000F00000004000000030000
          000BC7A598FFFFFEFEFFFCF8F6FFFCF7F6FFFCF8F6FFFCF7F6FFFCF8F6FFFCF7
          F5FFFCF7F5FFFBF7F5FFFBF7F5FFFCF7F5FFFCF7F5FFFBF7F4FFFBF6F4FFFBF7
          F4FFFBF6F4FFFBF7F4FFFAF6F4FFFAF6F4FFFBF6F4FFFBF6F3FFFAF6F3FFFBF5
          F3FFFAF6F3FFFBF5F3FFFEFDFCFFC29E8FFF0000000F00000004000000030000
          000BC9A799FFC9A89AFFC9A89AFFC9A79AFFC9A899FFC9A899FFC8A899FFC9A7
          99FFC9A799FFC9A799FFC8A698FFC8A698FFC8A697FFC8A698FFC8A698FFC8A5
          97FFC7A597FFC7A596FFC7A596FFC6A496FFC6A495FFC7A495FFC6A395FFC6A3
          94FFC6A395FFC6A394FFC5A294FFC4A092FF0000000E00000004000000020000
          000ACAA99AFFFFFFFEFFFDF9F8FFFCF9F7FFFCF9F8FFFCF9F8FFFCF8F7FFFCF9
          F7FFFCF9F8FFFCF8F7FFFBF9F7FFFBF9F7FFFCF8F7FFFCF8F6FFFCF8F6FFFBF7
          F7FFFBF8F6FFFBF7F6FFFBF7F6FFFBF7F5FFFCF7F5FFFBF7F6FFFBF7F5FFFBF7
          F5FFFBF7F5FFFBF6F5FFFEFEFDFFC5A293FF0000000D00000003000000020000
          0009CBAA9CFFFFFFFFFFFDF9F9FFFCF9F9FFFDF9F8FFFDFAF9FFFDFAF8FFFCF9
          F8FFFCF9F8FFFCF9F8FFFDF9F7FFFCF9F7FFFCF9F8FFFDF9F7FFFCF9F7FFFCF9
          F7FFFCF8F7FFFCF9F6FFFCF8F7FFFBF8F6FFFBF8F6FFFBF8F6FFFBF8F6FFFCF8
          F5FFFBF7F6FFFCF7F6FFFEFEFDFFC7A495FF0000000D00000003000000020000
          0009CCAC9DFFFFFFFFFFFDFAF9FFFDFAF9FFFDFAF9FFFDFAF9FFFDFAF9FFFCFA
          F8FFFDFAF9FFFDF9F9FFFCFAF9FFFCF9F9FFFDF9F8FFFDF9F8FFFCF9F8FFFCF9
          F8FFFCF9F8FFFCF9F8FFFCF9F8FFFCF9F8FFFCF9F7FFFCF9F7FFFCF8F6FFFCF9
          F7FFFBF8F6FFFCF8F7FFFEFEFEFFC8A698FF0000000C00000003000000020000
          0008CDAD9FFFFFFFFFFFFEFBFAFFFDFBFAFFFDFAFAFFFDFAFAFFFDFAFAFFFDFA
          F9FFFDFAF9FFFDFAF9FFFDFAF9FFFDFAF9FFFDFAF9FFFDF9F9FFFCFAF9FFFDF9
          F9FFFCF9F9FFFDFAF8FFFCFAF8FFFDF9F8FFFDFAF8FFFCF9F8FFFDF9F8FFFCF8
          F7FFFCF9F8FFFCF9F7FFFFFEFEFFC9A79AFF0000000B00000003000000020000
          0007CDAD9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFEFFFFFE
          FEFFFFFFFEFFFFFEFEFFFFFEFEFFCAA99AFF0000000A00000003000000010000
          0005988076C0CDAD9FFFCDAD9FFFCDAD9FFFCDAD9FFFCDAD9FFFCDAD9FFFCDAD
          9FFFCDAD9FFFCDAD9FFFCDAD9FFFCDAD9EFFCDAD9FFFCDAC9FFFCDAC9EFFCDAC
          9FFFCDAC9FFFCCAC9EFFCCAB9EFFCCAB9DFFCCAC9DFFCCAC9DFFCCAB9DFFCBAB
          9DFFCBAB9DFFCCAB9CFFCBAB9CFF977F74C10000000600000002000000000000
          0002000000040000000600000007000000070000000700000007000000070000
          0008000000080000000800000008000000080000000800000008000000080000
          0008000000080000000900000009000000090000000900000009000000090000
          0009000000090000000900000008000000060000000300000001000000000000
          0000000000010000000100000002000000020000000200000002000000020000
          0002000000020000000200000002000000020000000200000002000000020000
          0002000000020000000200000002000000020000000200000002000000020000
          0002000000020000000200000002000000010000000100000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000005D422781000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000005D422781B8824DFF5D4227810000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000093683ECCB8824DFFB8824DFFB882
          4DFFB8824DFFB8824DFFAC7948EE000000000000000000000000000000000000
          00000000000000000000000000005D422781B8824DFFB8824DFFB8824DFF5D42
          2781000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007B5733AAB8824DFFB8824DFFB882
          4DFFB8824DFFB8824DFFAC7948EE000000000000000000000000000000000000
          000000000000000000005D422781B8824DFFB8824DFFB8824DFFB8824DFFB882
          4DFF5D4227810000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000251A0F33B8824DFFAC7948EE0C09
          0511000000000000000000000000000000000000000000000000000000000000
          0000000000005D422781B8824DFFB8824DFFB8824DFFB8824DFFB8824DFFB882
          4DFFB8824DFF5D42278100000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000875F38BBB8824DFF563D
          2477000000000000000000000000000000000000000000000000000000000000
          00005D422781B8824DFFB8824DFFB8824DFFB8824DFFB8824DFFB8824DFFB882
          4DFFB8824DFFB8824DFF5D422781000000000000000000000000000000000000
          000000000000000000000000000000000000000000003D2B1A55B8824DFF9F71
          43DD000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000009F7143DDB882
          4DFF563D24770000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000563D2477B882
          4DFF93683ECC0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000009F71
          43DDB8824DFF3D2B1A5500000000000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000006245
          2988B8824DFF7B5733AA00000000000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000C09
          0511AC7948EEB8824DFF251A0F33000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000093683ECCB8824DFFB8824DFFB882
          4DFFB8824DFFB8824DFF6E4E2E99000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000093683ECCB8824DFFB8824DFFB882
          4DFFB8824DFFB8824DFF93683ECC000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000008B4281BBBE5AB0FF72366A99000000000000
          00000000000072366A99BE5AB0FF8B4281BB0000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000592A5277BE5AB0FFA54E99DD000000000000
          000000000000A54E99DDBE5AB0FF592A52770000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000033182F44BE5AB0FFBE5AB0FF65305E886530
          5E8865305E88BE5AB0FFBE5AB0FF33182F440000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000B154A4EEBE5AB0FFBE5AB0FFBE5A
          B0FFBE5AB0FFBE5AB0FFB154A4EE000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008B4281BBBE5AB0FF72366A990000
          000072366A99BE5AB0FF8B4281BB000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000592A5277BE5AB0FFA54E99DD0000
          0000A54E99DDBE5AB0FF592A5277000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000190C1722BE5AB0FFBE5AB0FF592A
          5277BE5AB0FFBE5AB0FF33182F44000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000A54E99DDBE5AB0FFB154
          A4EEBE5AB0FFA54E99DD00000000000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000072366A99BE5AB0FFBE5A
          B0FFBE5AB0FF72366A9900000000000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000004C244666BE5AB0FFBE5A
          B0FFBE5AB0FF4C24466600000000000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000190C1722BE5AB0FFBE5A
          B0FFBE5AB0FF190C172200000000000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000A54E99DDBE5A
          B0FFA54E99DD0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000B8824DFFB8824DFFB8824DFF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000010000000200000004000000060000000600000007000000070000
          0007000000070000000700000007000000070000000600000004000000020000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000002000000070000000F000000160000001800000019000000190000
          001A0000001A0000001A0000001A0000001A0000001700000010000000070000
          0002000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000040000000F74473BC0A26252FFA16251FFA16150FFA16150FFA062
          4FFFA0624FFF9F614EFF9F604DFF9F604DFF9E604CFF724337C1000000100000
          0004000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00010000000500000014A46754FFFCF9F7FFFAF3EEFFFAF1EDFFF9F1ECFFF8F0
          EAFFF7EEE8FFF6EDE7FFF8EAE4FFF6E9E3FFF5E8E1FFA0604FFF000000160000
          0006000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00010000000500000016A86A58FFFDFAF8FFF4E6DBFFF3E5DBFFF3E5DBFFF4E5
          DAFFF3E5DAFFF3E2D8FFF3E2D7FFF1E2D7FFF7ECE4FFA26352FF000000180000
          0006000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00010000000500000015AA6E5BFFFDFBF9FFF5E8DEFFF4E8DEFFF4E6DDFFF4E6
          DBFFF4E5DDFFF4E5DAFFF4E5DAFFF4E3DAFFF7EDE6FFA46755FF000000180000
          0006000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00010000000500000015AD725FFFFEFCFBFFF5EAE3FFF5EAE3FFF5E8DFFFF5E8
          DEFFF4E8DEFFF4E8DDFFF4E6DDFFF4E6DBFFF8EFE9FFA96B58FF000000170000
          0006000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000500000014B07764FFFEFCFBFFF7EDE6FFF7EEE5FFF7EDE5FFF7E9
          E2FFF5E9DFFFF5E8DFFFF5E9DFFFF4E8DEFFF9F1ECFFAA715BFF000000160000
          0006000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000500000013B47B66FFFEFDFCFFF8F0E9FFF8F0E8FFF8EEE8FFF7ED
          E6FFF7EDE6FFF5EDE3FFF5EAE3FFF5E9DFFFFAF3EEFFAD735FFF000000150000
          0005000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000400000012B77F6DFFFEFDFDFFF7F0EDFFF8F1EAFFF8F0E9FFF8F0
          E8FFF8F0E8FFF7EEE6FFF7EEE5FFF7EDE6FFFBF6F2FFB17663FF000000150000
          0005000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000400000011BB8472FFFEFEFDFFF8F1EEFFF8F1EEFFFBF1EDFFFBF1
          EDFFFBEEEAFFF8F0E9FFF8F0E8FFF7F0E8FFFCF7F4FFB37B68FF000000140000
          0005000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000400000010BB8775FFFEFEFEFFFBF4F1FFFBF3F0FFFBF3F0FFF8F1
          EEFFF8F1EEFFFBF1EDFFFBF1EAFFF8F1E9FFFCF9F6FFB7806DFF000000130000
          0005000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000400000010C18B78FFFFFEFEFFFCF5F3FFFCF5F3FFFBF4F3FFFBF4
          F1FFFBF3F0FFF8F3F0FFF8F3EEFFFBF3EDFFFCFAF7FFBA8472FF000000120000
          0005000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000040000000FC18D7BFFFFFFFEFFFCF7F5FFFCF7F4FFFCF7F4FFFCF5
          F3FFFBF4F3FFF8F4F1FFF8F3F1FFFBF3F0FFFDFBFAFFBD8675FF000000110000
          0004000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000030000000CC3907FFFFFFFFEFFFFFFFFFFFFFFFEFFFFFEFEFFFEFE
          FEFFFEFEFEFFFEFEFEFFFEFDFDFFFEFDFDFFFEFDFDFFC08B79FF0000000F0000
          0004000000010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000200000009936D5FC2C69381FFC49380FFC49380FFC49180FFC390
          7FFFC3907FFFC3907EFFC38F7EFFC28F7EFFC28E7DFF8F695DC30000000C0000
          0005000000040000000400000004000000040000000400000004000000040000
          0004000000050000000400000003000000010000000000000000000000000000
          0000000000020000000700000010000000170000001A0000001B0000001B0000
          001C0000001C0000001C0000001D0000001C0000001A000000140000000C0000
          000C0000000E0000000F00000010000000100000001000000011000000110000
          0011000000110000000F0000000B000000050000000100000000000000000000
          00000000000200000008896154BCBF8775FFBE8774FFBE8673FFBE8673FFBD87
          72FFBD8772FFBC8671FFBC8570FFBC8570FFBB856FFF865D50BE000000128257
          4ABDB67967FFB57966FFB57865FFB57865FFB47964FFB47964FFB37863FFB377
          62FFB37762FFB27761FF7F5346BD0000000B0000000300000000000000000000
          0000000000020000000AC18C77FFFEFCFBFFFCF9F7FFFCF8F6FFFCF7F5FFFCF7
          F4FFFBF6F3FFFBF6F2FFFBF5F1FFFAF4F0FFFAF4EFFFBD8572FF00000018B87F
          69FFFDFBFAFFFBF8F5FFFBF7F4FFFBF6F3FFFBF6F2FFFAF5F0FFF9F5F0FFFAF3
          EEFFF9F3EDFFF9F2EDFFB47764FF0000000E0000000400000000000000000000
          0000000000030000000AC38F7CFFFEFCFCFFF9F2ECFFF9F1ECFFF9F1ECFFF9F1
          ECFFF9F1ECFFF9F0EBFFF9F0EAFFF8F0EAFFFBF5F1FFBF8875FF00000019BB82
          6EFFFDFCFBFFF7F0E8FFF7EFE8FFF7EFE8FFF7EFE8FFF7EFE8FFF7EEE7FFF7EE
          E6FFF6EEE6FFFAF4EEFFB67A67FF0000000F0000000400000000000000000000
          0000000000020000000AC5927FFFFEFDFCFFFAF3EEFFF9F3EEFFF9F2EDFFF9F2
          ECFFF9F1EDFFF9F1ECFFF9F1ECFFF9F0ECFFFBF5F2FFC18C78FF00000018BD85
          71FFFEFCFCFFFAF1EBFFF7F1EBFFF7F0E9FFF7F0E8FFF7EFE9FFF7EFE8FFF7EF
          E8FFF7EEE8FFFAF4F0FFB87F6AFF0000000F0000000400000000000000000000
          00000000000200000009C79584FFFEFDFDFFFAF5F0FFFAF5F0FFFAF3EFFFFAF3
          EEFFF9F3EEFFF9F3EDFFF9F2EDFFF9F2ECFFFBF7F4FFC4907BFF00000017BF88
          76FFFEFDFCFFFAF3EEFFFAF3EEFFFAF1EDFFFAF1EBFFF7F1EBFFF7F1E9FFF7F0
          E9FFF7F0E8FFFAF5F1FFBC836DFF0000000E0000000400000000000000000000
          00000000000200000008C99A89FFFEFEFDFFFBF5F2FFFBF6F1FFFBF5F1FFFBF4
          F0FFFAF4EFFFFAF3EFFFFAF4EFFFF9F3EEFFFCF8F5FFC5947FFF00000015C18E
          7BFFFEFDFDFFFBF3F0FFFBF4EFFFFBF3EFFFFBF2EEFFFAF2EDFFFAF1EDFFFAF2
          EDFFF7F1EBFFFAF7F4FFBD8771FF0000000D0000000300000000000000000000
          00000000000200000008CD9E8BFFFEFEFDFFFCF7F4FFFCF7F3FFFCF6F3FFFBF5
          F2FFFBF5F2FFFAF5F0FFFAF5F0FFFAF4EFFFFCF9F7FFC79684FF00000014C692
          7EFFFEFEFDFFFCF5F2FFFCF5F1FFFCF4F1FFFBF3F0FFFBF3F0FFFAF3EEFFFAF3
          EEFFFAF2EDFFFCF8F6FFBF8976FF0000000C0000000300000000000000000000
          00000000000200000007CFA191FFFEFEFEFFFBF7F5FFFCF8F5FFFCF7F4FFFCF7
          F3FFFCF7F3FFFBF6F2FFFBF6F1FFFBF5F2FFFDFAF8FFCA9988FF00000013C895
          84FFFEFEFDFFFBF5F3FFFCF6F3FFFCF5F2FFFCF5F1FFFCF5F1FFFBF4F0FFFBF4
          EFFFFBF3F0FFFDF9F7FFC38C7AFF0000000C0000000300000000000000000000
          00000000000200000006D1A695FFFEFEFEFFFCF8F6FFFCF8F6FFFDF8F5FFFDF8
          F5FFFDF6F5FFFCF7F4FFFCF7F3FFFBF7F3FFFEFBF9FFCC9E8DFF00000011CA9B
          88FFFEFEFEFFFCF6F4FFFCF6F4FFFDF6F3FFFDF6F3FFFDF4F3FFFCF5F2FFFCF5
          F1FFFBF5F1FFFEFAF8FFC59280FF0000000B0000000300000000000000000000
          00000000000100000006D1A998FFFFFEFEFFFDF9F8FFFDF9F7FFFDF9F7FFFCF8
          F6FFFCF8F6FFFDF8F5FFFDF8F5FFFCF8F4FFFEFCFAFFCFA291FF00000010CA9E
          8BFFFFFEFEFFFDF7F6FFFDF7F5FFFDF7F5FFFCF6F4FFFCF6F4FFFDF6F3FFFDF6
          F3FFFCF6F2FFFEFBFAFFC89684FF0000000A0000000300000000000000000000
          00000000000100000005D6AC9BFFFFFFFEFFFDFAF9FFFDFAF9FFFDF9F9FFFDF9
          F8FFFDF9F7FFFCF9F7FFFCF9F6FFFDF9F5FFFEFDFBFFD1A695FF0000000ED0A1
          8FFFFFFFFEFFFDFAF7FFFDFAF7FFFDF7F7FFFDF7F6FFFDF7F5FFFCF7F5FFFCF7
          F4FFFDF7F3FFFEFCFAFFCA9B88FF000000090000000200000000000000000000
          00000000000100000004D6AE9EFFFFFFFFFFFDFBFAFFFDFBF9FFFDFBF9FFFDFA
          F9FFFDF9F9FFFCF9F8FFFCF9F8FFFDF9F7FFFEFDFCFFD3A898FF0000000DD0A3
          92FFFFFFFFFFFDFBFAFFFDFBF7FFFDFBF7FFFDFAF7FFFDF7F7FFFCF7F6FFFCF7
          F6FFFDF7F5FFFEFCFCFFCD9D8BFF000000090000000200000000000000000000
          00000000000100000003D8B1A1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFE
          FEFFFFFEFEFFFFFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFD5AC9CFF0000000AD2A7
          95FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFEFFFFFEFEFFFFFEFEFFFEFE
          FEFFFEFEFEFFFEFEFDFFCFA190FF000000070000000200000000000000000000
          00000000000000000002A18579BED9B3A3FFD9B3A2FFD9B3A2FFD9B2A2FFD8B1
          A1FFD8B1A1FFD8B1A0FFD8B0A0FFD7B0A0FFD7AF9FFF9E8176BF000000079D7D
          70BFD4A997FFD4A996FFD4A996FFD4A896FFD2A795FFD2A795FFD2A794FFD2A6
          94FFD1A694FFD1A493FF9A786DC0000000050000000100000000000000000000
          0000000000000000000100000002000000030000000300000003000000040000
          0004000000040000000400000004000000040000000400000003000000020000
          0003000000040000000500000005000000050000000600000006000000060000
          0006000000060000000600000004000000020000000100000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000131B3B462637778D354DA9C7405DCBF0405DCBF0354D
          A9C72637778D131B3B4600000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000090C1B202536778C415ECEF34463D8FF4463D8FF4463D8FF4463D8FF4463
          D8FF4463D8FF415ECEF32536778C090C1B200000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000090C
          1B202B3F8AA34463D8FF3C58BFE22638798F131C3D480608121506081215131C
          3D482638798F3C58BFE24463D8FF2B3F8AA3090C1B2000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002536
          778C4463D8FF354DA8C6151F434F000000000000000000000000000000000000
          000000000000151F434F354DA8C64463D8FF2536778C00000000000000000000
          000000000000848484FF848484FF848484FF848484FF848484FF848484FF8484
          84FF848484FF848484FF848484FF848484FF848484FF575757A9131B3B46415E
          CEF33C58BFE2151F434F00000000000000000000000000000000000000000000
          00000000000000000000151F434F3C58BFE2415ECEF3131B3B46000000000000
          000000000000848484FF00000000000000000000000000000000848484FF0000
          0000000000000000000000000000848484FF00000000000000002637778D4463
          D8FF2638798F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000002638798F4463D8FF2637778D000000000000
          000000000000848484FF00000000000000000000000000000000848484FF0000
          0000000000000000000000000000848484FF0000000000000000354DA9C74463
          D8FF131C3D480000000000000000000000000000000000000000000000000000
          0000000000000000000000000000131C3D484463D8FF354DA9C7000000000000
          000000000000848484FF00000000000000000000000000000000848484FF0000
          0000000000000000000000000000848484FF0000000000000000405DCBF04463
          D8FF0608121500000000000000000000000000000000848484FF848484FF8484
          84FF848484FF848484FF00000000060812154463D8FF405DCBF0000000000000
          000000000000848484FF00000000000000000000000000000000848484FF0000
          0000000000000000000000000000848484FF0000000000000000405DCBF04463
          D8FF0608121500000000000000000000000000000000848484FF848484FF8484
          84FF848484FF848484FF00000000060812154463D8FF405DCBF0000000000000
          000000000000848484FF848484FF848484FF848484FF848484FF848484FF8484
          84FF848484FF848484FF848484FF848484FF848484FF1A1A1A32354DA9C74463
          D8FF131C3D4800000000000000000000000000000000848484FF848484FF0000
          0000000000000000000000000000131C3D484463D8FF354DA9C7000000000000
          000000000000848484FF00000000000000000000000000000000848484FF0000
          0000000000000000000000000000848484FF00000000000000002637778D4463
          D8FF2638798F00000000000000000000000000000000848484FF848484FF0000
          00000000000000000000000000002638798F4463D8FF2637778D000000000000
          000000000000848484FF00000000000000000000000000000000848484FF0000
          0000000000000000000000000000848484FF0000000000000000131B3B46415E
          CEF33C58BFE2151F434F000000000000000000000000848484FF848484FF0000
          00000000000000000000151F434F3C58BFE2415ECEF3131B3B46000000000000
          000000000000848484FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002536
          778C4463D8FF354DA8C6151F434F0000000000000000848484FF848484FF0000
          000000000000151F434F354DA8C64463D8FF2536778C00000000000000000000
          000000000000848484FF0000000000000000000000004463D8FF4463D8FF4463
          D8FF4463D8FF4463D8FF4463D8FF4463D8FF4463D8FF0000000000000000090C
          1B202B3F8AA34463D8FF3C58BFE22638798F131C3D480608121506081215131C
          3D482638798F3C58BFE24463D8FF2B3F8AA3090C1B2000000000000000000000
          000000000000848484FF848484FF848484FF000000004463D8FF4463D8FF4463
          D8FF4463D8FF4463D8FF4463D8FF4463D8FF4463D8FF00000000848484FF5656
          56A6161A293B2536778C415ECEF34463D8FF4463D8FF4463D8FF4463D8FF4463
          D8FF4463D8FF415ECEF32536778C090C1B200000000000000000000000000000
          000000000000848484FF0000000000000000000000004463D8FF4463D8FF0000
          00000000000000000000000000004463D8FF4463D8FF00000000000000000000
          0000606060BA00000000131B3B462637778D354DA9C7405DCBF0405DCBF0354D
          A9C72637778D131B3B461A1A1A33000000000000000000000000000000000000
          000000000000848484FF0000000000000000000000004463D8FF4463D8FF0000
          00000000000000000000000000004463D8FF4463D8FF00000000000000000000
          0000848484FF000000000000000000000000000000000707070E000000000000
          000000000000000000007F7F7FF5000000000000000000000000000000000000
          000000000000848484FF0000000000000000000000004463D8FF4463D8FF0000
          00000000000000000000000000004463D8FF4463D8FF00000000000000000000
          0000848484FF00000000000000000000000000000000848484FF000000000000
          00000000000000000000848484FF000000000000000000000000000000000000
          000000000000848484FF0000000000000000000000004463D8FF4463D8FF0000
          00000000000000000000000000004463D8FF4463D8FF00000000000000000000
          0000848484FF00000000000000000000000000000000848484FF000000000000
          00000000000000000000848484FF000000000000000000000000000000000000
          000000000000848484FF848484FF848484FF000000004463D8FF4463D8FF4463
          D8FF4463D8FF4463D8FF4463D8FF4463D8FF4463D8FF00000000848484FF8484
          84FF848484FF848484FF848484FF848484FF848484FF848484FF848484FF8484
          84FF848484FF848484FF848484FF000000000000000000000000000000000000
          000000000000848484FF0000000000000000000000004463D8FF4463D8FF4463
          D8FF4463D8FF4463D8FF4463D8FF4463D8FF4463D8FF00000000000000000000
          0000848484FF00000000000000000000000000000000848484FF000000000000
          00000000000000000000848484FF000000000000000000000000000000000000
          000000000000848484FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000848484FF00000000000000000000000000000000848484FF000000000000
          00000000000000000000848484FF000000000000000000000000000000000000
          000000000000848484FF00000000000000000000000000000000848484FF0000
          0000000000000000000000000000848484FF0000000000000000000000000000
          0000848484FF00000000000000000000000000000000848484FF000000000000
          00000000000000000000848484FF000000000000000000000000000000000000
          000000000000848484FF00000000000000000000000000000000848484FF0000
          0000000000000000000000000000848484FF0000000000000000000000000000
          0000848484FF00000000000000000000000000000000848484FF000000000000
          00000000000000000000848484FF000000000000000000000000000000000000
          000000000000848484FF848484FF848484FF848484FF848484FF848484FF8484
          84FF848484FF848484FF848484FF848484FF848484FF848484FF848484FF8484
          84FF848484FF848484FF848484FF848484FF848484FF848484FF848484FF8484
          84FF848484FF848484FF848484FF000000000000000000000000000000000000
          000000000000848484FF848484FF848484FF848484FF848484FF848484FF8484
          84FF848484FF848484FF848484FF848484FF848484FF848484FF848484FF8484
          84FF848484FF848484FF848484FF848484FF848484FF848484FF848484FF8484
          84FF848484FF848484FF848484FF000000000000000000000000000000000000
          000000000000848484FF848484FF848484FF0000000000000000000000000000
          0000848484FF848484FF848484FF848484FF848484FF848484FF848484FF8484
          84FF848484FF848484FF848484FF848484FF0000000000000000000000000000
          0000848484FF848484FF848484FF000000000000000000000000000000000000
          000000000000848484FF848484FF848484FF00000000848484FF848484FF0000
          0000848484FF848484FF848484FF848484FF848484FF848484FF848484FF8484
          84FF848484FF848484FF848484FF848484FF00000000848484FF848484FF0000
          0000848484FF848484FF848484FF000000000000000000000000000000000000
          00000000000000000000000000000000000000000000848484FF848484FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000848484FF848484FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000848484FF848484FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000848484FF848484FF0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0001000000010000000100000001000000010000000100000001000000010000
          0001000000000000000100000001000000010000000100000001000000010000
          0001000000010000000100000000000000010000000100000001000000010000
          0001000000010000000100000001000000010000000000000000000000020000
          0004000000080000000A0000000B0000000B0000000B0000000A000000080000
          00060000000400000005000000080000000A0000000B0000000B0000000B0000
          000A00000008000000060000000400000005000000080000000A0000000B0000
          000B0000000B0000000A00000008000000050000000200000001000000060000
          00110000001D00000024000000260000002700000027000000260000001F0000
          00140000000D000000130000001D000000240000002600000027000000270000
          00260000001F000000140000000D000000130000001D00000024000000260000
          002700000027000000260000001F0000001300000007000000020000000F0B21
          1863216A4BED267453FF257353FF247251FF237152FF237051FF1F6448ED0A1F
          16680000001F0B211865216A4BED267453FF257353FF247251FF237152FF2370
          51FF1F6448ED0A1F16680000001F0B211865216A4BED267453FF257353FF2472
          51FF237152FF237051FF1F6448ED0A1F16660000001100000004000000162672
          55E939B58AFF2CC18EFF2CC18EFF2CC08DFF2CC08DFF2BC08DFF35B186FF2670
          53EA0000002E277355EA39B58AFF2CC18EFF2CC18EFF2CC08DFF2CC08DFF2BC0
          8DFF35B186FF267053EA0000002E277355EA39B58AFF2CC18EFF2CC18EFF2CC0
          8DFF2CC08DFF2BC08DFF35B287FF277254EA0000001A00000006000000172D87
          66FF46CFA5FF30C593FF2FC493FF2FC492FF2EC492FF2EC392FF40C99DFF2E84
          64FF000000302D8766FF46CFA5FF30C593FF2FC493FF2FC492FF2EC492FF2EC3
          92FF40C99DFF2E8464FF000000302D8766FF46CFA5FF30C593FF2FC493FF2FC4
          92FF2EC492FF2EC392FF40CA9EFF2F8766FF0000001C0000000700000015318F
          6EFF4ED4ACFF32C898FF32C898FF32C897FF31C797FF31C797FF47CFA5FF308C
          6AFF0000002C318F6EFF4ED4ACFF32C898FF32C898FF32C897FF31C797FF31C7
          97FF47CFA5FF308C6AFF0000002C318F6EFF4ED4ACFF32C898FF32C898FF32C8
          97FF31C797FF31C797FF47D0A6FF318F6CFF0000001900000007000000113598
          74FF55D8B3FF36CC9DFF36CC9DFF35CB9CFF35CC9DFF35CB9CFF4DD5ADFF3496
          72FF00000026359874FF55D8B3FF36CC9DFF36CC9DFF35CB9CFF35CC9DFF35CB
          9CFF4DD5ADFF349672FF00000026359874FF55D8B3FF36CC9DFF36CC9DFF35CB
          9CFF35CC9DFF35CB9CFF4DD5ADFF359874FF00000016000000060000000E38A1
          7DFF6AE0C1FF60DDBCFF68E0C0FF6FE2C5FF67DFC0FF60DDBBFF64DEBBFF379F
          7CFF0000001F38A17DFF6AE0C1FF60DDBCFF68E0C0FF6FE2C5FF67DFC0FF60DD
          BBFF64DEBBFF379F7CFF0000001F38A17DFF6AE0C1FF60DDBCFF68E0C0FF6FE2
          C5FF67DFC0FF60DDBBFF64DEBBFF38A17DFF00000012000000050000000A3CA9
          84FFA2F0DFFF8AECD5FF82EAD2FF82EAD2FF82EAD2FF88EBD4FF9DEFDDFF3CA7
          84FF000000173CA984FFA2F0DFFF8AECD5FF82EAD2FF82EAD2FF82EAD2FF88EB
          D4FF9DEFDDFF3CA784FF000000173CA984FFA2F0DFFF8AECD5FF82EAD2FF82EA
          D2FF82EAD2FF88EBD4FF9DEFDDFF3CA885FF0000000E0000000300000006389F
          7EE894E2CFFFB1F4E7FFB0F4E7FFB0F4E7FFAEF4E7FFAEF4E6FF90E2CEFF3A9F
          7EE90000000E389F7EE894E2CFFFB1F4E7FFB0F4E7FFB0F4E7FFAEF4E7FFAEF4
          E6FF90E2CEFF3A9F7EE90000000E389F7EE894E2CFFFB1F4E7FFB0F4E7FFB0F4
          E7FFAEF4E7FFAEF4E6FF90E2CEFF3AA07FE9000000080000000200000002102D
          2444338F73CB42B791FF42B691FF42B791FF41B791FF42B791FF338F73CC102D
          244600000006102D2445338F73CB42B791FF42B691FF42B791FF41B791FF42B7
          91FF338F73CC102D244600000006102D2445338F73CB42B791FF42B691FF42B7
          91FF41B791FF42B791FF338F73CC102D24460000000400000001000000000000
          0001000000020000000300000004000000040000000400000004000000040000
          0002000000010000000100000002000000030000000400000004000000040000
          0004000000040000000200000001000000010000000200000003000000040000
          0004000000040000000400000004000000020000000100000000000000000000
          00000000000000000000000000006D5045FF0000000100000001000000000000
          0000000000000000000000000000000000000000000061453BFF000000010000
          000100000000000000000000000000000000000000000000000000000000573B
          33FF000000010000000100000000000000000000000000000000000000000000
          0000000000000000000000000000715448FF0000000000000000000000000000
          0000000000000000000000000000000000000000000064493EFF000000000000
          000000000000000000000000000000000000000000000000000000000000593F
          35FF000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000005F473DCF735749FF725648FF705448FF6F53
          46FF6E5246FF6D5145FF6C5043FF6A4E42FF694D42FF684C41FF674B3FFF664A
          3FFF65483EFF63483DFF62463CFF61453CFF60443AFF5F4439FF5E4238FF4B35
          2DCF000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000001000000010000000100000001000000016C5043FF000000010000
          0001000000010000000100000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          000100000004000000070000000800000009000000096B5044FF000000090000
          0009000000080000000700000004000000010000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          00050000000F000000190000001F000000210000002200000023000000230000
          0023000000210000001B00000011000000060000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000030000
          000E33201260A46A3BECB57340FFB57340FFB47341FFB47340FFB47240FFB473
          40FFB37340FFA4683AED322012630000000F0000000300000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000050000
          0015A36A3CE9CD9B67FFD7AB76FFD8AC76FFD9AC78FFD7AD78FFD8AC77FFD7AC
          77FFD7AC77FFCD9B67FFA2683BEA000000180000000600000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000050000
          0017B87847FFD9B07DFFD5A871FFD6AA73FFD7AA74FFD7AB75FFD7AB76FFD7AB
          76FFD7AA74FFDAB07CFFB77744FF0000001B0000000600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000060000
          0016BA7B49FFDCB584FFD8AB76FFD8AC78FFD9AE79FFD9AF7AFFD9AF7AFFD9AF
          7AFFD9AE7AFFDDB585FFB87947FF0000001A0000000700000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000050000
          0014BC7E4CFFE0BB8CFFD9AE7AFFDAB07CFFDBB17EFFDCB380FFDCB380FFDBB3
          7FFFDBB17EFFE0BC8EFFBA7C4AFF000000180000000600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000040000
          0012BD804EFFE2C196FFDAB27EFFDCB381FFDDB583FFDDB784FFDEB785FFDDB7
          85FFDDB583FFE2C298FFBC7E4DFF000000160000000600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000040000
          0010BF8351FFE5C69DFFE1BD8DFFE5C699FFE8CB9FFFEACFA4FFE9CCA1FFE7C9
          9CFFE3C192FFE6C8A0FFBE814FFF000000140000000500000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000030000
          000DC38958FFF2DFBFFFEED7AEFFEFD8AFFFEFDAB1FFF0DBB2FFF0DAB2FFF0DB
          B2FFF0DAB1FFF2E0C1FFC18757FF000000110000000400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          000BCD9D6FFFF8EDD2FFF5E5C1FFF4E3BDFFF5E4BEFFF5E4BFFFF5E5BFFFF5E5
          BFFFF6E6C4FFF9EDD3FFCD9C6EFF0000000E0000000300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000020000
          0007BE956CE9F0DEC2FFFCF3DDFFFCF3DDFFFBF3DEFFFBF4DEFFFCF4DDFFFBF4
          DDFFFBF3DDFFF0DFC2FFBE956CE90000000A0000000200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0003352A1F46A88562CCD5A97DFFD5A97DFFD5A97DFFD5A97CFFD5A97DFFD5A9
          7CFFD4A97DFFA78563CD352A1F47000000050000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000030000000500000006000000070000000700000007000000080000
          0008000000070000000600000004000000010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000100000001000000010000000100000001000000010000
          0002000000010000000100000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000100000002000000030000000500000006000000060000
          0006000000060000000600000006000000060000000600000006000000060000
          0006000000060000000600000006000000070000000700000007000000070000
          0006000000040000000200000001000000000000000000000000000000000000
          00000000000000000002000000060000000D0000001400000016000000170000
          0017000000170000001800000018000000180000001800000018000000180000
          00190000001900000019000000190000001A0000001A0000001A000000190000
          0017000000100000000700000002000000000000000000000000000000000000
          000000000001000000030000000E7C5B50BFAD7F6FFFAC7F6FFFAC7F6FFFAB7E
          6EFFAB7E6EFFAB7D6DFFAB7D6DFFAA7D6DFFAB7C6CFFAA7C6CFFAA7B6BFFA97B
          6BFFAA7B6BFFA97A6BFFA97A6BFFA97A6AFFA87A6AFFA87A69FFA8796AFFA879
          69FF78574CC10000001000000004000000010000000000000000000000000000
          0000000000010000000500000012AE8171FFFFFFFFFFFBF8F5FFFAF7F5FFFAF7
          F5FFFAF6F5FFFAF6F4FFFAF6F3FFF9F6F3FFF9F5F3FFF9F5F2FFF9F5F2FFF9F4
          F1FFF9F4F1FFF8F4F1FFF8F3F0FFF8F3F0FFF8F3EFFFF8F2EFFFF7F2EFFFF7F2
          EEFFA87A6AFF0000001600000006000000010000000000000000000000000000
          0000000000010000000500000014AF8273FFFFFFFFFFF5EDE9FFF5EDE8FFF4ED
          E8FFF5EDE7FFF4EDE7FFF5EDE8FFF5ECE8FFF4EDE7FFF4EDE8FFF4EDE7FFF4EC
          E8FFF4ECE8FFF4ECE7FFF4ECE8FFF4ECE7FFF4ECE7FFF4EDE7FFF4ECE7FFF8F2
          EFFFA97B6AFF0000001800000006000000010000000000000000000000000000
          0000000000000000000500000014B08474FFFFFFFFFFF4EDE9FFF5EDE9FFF5EE
          E9FFF5EEE9FFF5EDE9FFF4EDE8FFF5EEE8FFF4EDE8FFF4ECE9FFF4EDE8FFF5ED
          E8FFF4ECE8FFF4EDE8FFF4EDE8FFF4EDE7FFF4ECE8FFF4EDE8FFF4ECE7FFF8F3
          EFFFAA7C6BFF0000001800000006000000010000000000000000000000000000
          0000000000000000000500000013B18676FFFFFFFFFFF6EEE9FFF5EEE9FFF5EE
          EAFFF5EDE9FFF5EEE9FFF5EDE9FFF5EEE9FFF5EEE8FFF5EDE9FFF5EDE8FFF5EE
          E8FFF5EDE9FFF4EDE8FFF5EDE9FFF4EDE8FFF5EDE9FFF5EDE8FFF5EDE8FFF8F4
          F1FFAA7D6DFF0000001800000006000000010000000000000000000000000000
          0000000000000000000500000013B38778FFFFFFFFFFF6EFEAFFF6EEEAFFF5EF
          EAFFF6EFEAFFF6EFE9FFF5EEEAFFF5EEE9FFF5EEEAFFF6EEE9FFF5EEE9FFF5EE
          E9FFF5EEE9FFF5EEE9FFF5EDE9FFF5EEE8FFF5EDE8FFF5EDE9FFF4EDE9FFF9F4
          F1FFAB7D6DFF0000001700000006000000010000000000000000000000000000
          0000000000000000000500000012B38979FFFFFFFFFFF6EFEAFFF6EFEAFFF6EF
          EBFFF5EFEBFFF6EFEAFFF6EFEAFFF5EFEAFFF5EEEAFFF6EFEAFFF5EEEAFFF5EE
          E9FFF6EFEAFFF5EEEAFFF6EFE9FFF5EEE9FFF6EEE9FFF5EEE9FFF5EDE9FFF9F5
          F2FFAD7F70FF0000001700000006000000010000000000000000000000000000
          0000000000000000000400000012B48A7BFFFFFFFFFFF7F0ECFFF7F0ECFFF6EF
          EBFFF6EFEBFFF6EFECFFF6EFEBFFF6F0EBFFF6F0EBFFF6EFEAFFF5EFEBFFF6EF
          EBFFF5EFEAFFF6EFEBFFF5EEEAFFF5EEEBFFF5EEEAFFF6EFEAFFF6EEEAFFF9F6
          F3FFAE8070FF0000001600000006000000010000000000000000000000000000
          0000000000000000000400000011B68D7DFFFFFFFFFFF6F0EDFFF6F1ECFFF7F1
          ECFFF7F1EDFFF6F0ECFFF6F0EBFFF6F0ECFFF6F0EBFFF6F0ECFFF6EFECFFF6F0
          ECFFE7E2EAFFF6F0ECFFF6EFEBFFF6EFEBFFF6EFEAFFF6EFEBFFF5EFEBFFFAF7
          F4FFAE8272FF0000001600000005000000010000000000000000000000000000
          000000000000000000040000000FB88E7FFFFFFFFFFFF7F1EDFFF7F2EDFFF7F1
          EDFFF7F1EDFFF7F1EDFFF6F1ECFFF7F0ECFFF7F1EDFFF6F0EDFFF7F1EDFFD8D3
          E6FF0F0C9FFFD8D3E4FFF6F0EDFFF7F0EBFFF6EFEBFFF7F0ECFFF6F0EBFFFAF7
          F5FFAF8374FF0000001400000005000000010000000000000000000000000000
          000000000000000000030000000BC9A89BFFFFFFFFFFFAF5F2FFF9F5F2FFF9F5
          F2FFFAF5F2FFF7F1EFFFF7F1EEFFF7F1EDFFF7F1EDFFF8F1EEFFD9D5E7FF1210
          A2FF05042A5B110DA1FFD8D4E6FFF9F4F2FFF8F5F1FFF9F5F1FFF9F4F1FFFCFA
          F8FFC39F91FF0000000E00000004000000010000000000000000000000000000
          00000000000000000001000000052325B1FF2223B0FF2022AFFF2021AEFF1E1F
          ADFF1E1EACFFDBD7E9FFF7F2F0FFF7F2EEFFF8F2EFFFDAD6E7FF1616A6FF0303
          17330000000D030217311310A2FF110EA1FF110DA0FF100CA0FF0F0B9EFF0E0A
          9EFF0D099DFF0000000700000002000000010000000000000000000000000000
          0000000000000000000000000001000000030000000300000004000000040000
          00050505192D1F21AEFFDBD8EAFFF8F4F1FFDBD7E9FF1B1BA9FF0404182D0000
          0005000000030000000400000005000000050000000500000005000000050000
          0004000000030000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000030505192B2224B0FFC1BFE2FF1F21ADFF0404182B000000030000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000100000002050619282427B3FF0505192900000002000000010000
          000102020C140000000100000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000020000000300000003000000040000
          000400000003000000020000000103030D140000000200000002000000040505
          192A2123B0FF0405192A00000005000000040000000400000004000000040000
          0004000000030000000100000000000000000000000000000000000000000000
          0000000000000000000100000004000000090000000C0000000D0000000E0000
          000D0000000B000000060000000200000001000000020000000606061A2F272A
          B5FFBCBBDDFF2327B2FF05051933000000100000001000000011000000110000
          00100000000C0000000600000001000000000000000000000000000000000000
          00000000000000000002000000093740C3FF353EC3FF343DC2FF333CC1FF323B
          C0FF3139BFFF07081B3100000007000000040000000706071A322B30B9FFD8D5
          E6FFF1ECE9FFD6D3E5FF272AB5FF2529B4FF2527B2FF2326B1FF2124B0FF2023
          AFFF1F21AEFF0000000C00000003000000000000000000000000000000000000
          000000000000000000030000000BD1B4A7FFFFFFFFFFF9F8F7FFF9F8F6FFF8F6
          F5FFDCDCEEFF343CC2FF07081B330000000C07081B332F36BDFFD9D7EAFFF2EF
          ECFFF1ECEAFFF0ECE9FFF3EFEEFFF3EFEDFFF2EFECFFF2EFECFFF2EDECFFFBFA
          F9FFCAAB9EFF0000000F00000004000000000000000000000000000000000000
          000000000000000000030000000BC5A091FFFFFFFFFFF8F6F5FFF8F5F5FFF7F5
          F4FFF6F4F2FFDDDCEEFF363FC4FF0E103355353DC1FFDBDAEBFFF4F0EFFFF3EE
          ECFFF2EEECFFF1EDEBFFF1ECEAFFF0EBE8FFEFEAE8FFEFEAE7FFEEE9E6FFFAF9
          F8FFBC9585FF0000001000000004000000000000000000000000000000000000
          000000000000000000030000000BC6A293FFFFFFFFFFF9F8F7FFF9F7F6FFF9F6
          F5FFF8F6F5FFF7F5F4FFDEDEEFFF3942C6FFDDDCEEFFF5F1F0FFF5F1EFFFF4F0
          EEFFF4F0EEFFF3EEEDFFF2EEECFFF2EDEBFFF1ECEAFFF1ECE9FFEFEBE8FFFBFA
          F9FFBD9787FF0000000F00000004000000000000000000000000000000000000
          000000000000000000030000000AC6A394FFFFFFFFFFFBFAF9FFFBF8F8FFFAF8
          F7FFF9F7F6FFF9F6F5FFF8F6F5FFEDEBF4FFF7F5F3FFF6F4F2FFF6F3F1FFF5F2
          F0FFF5F1EFFFF4F0EEFFF4EFEEFFF3EFEDFFF2EEECFFF2EDEBFFF1EDEAFFFCFB
          FAFFBE9889FF0000000F00000004000000000000000000000000000000000000
          000000000000000000020000000AC8A595FFFFFFFFFFFCFBFAFFFCFAFAFFFBF9
          F8FFFAF9F7FFFAF8F7FFF9F8F6FFF9F6F5FFF8F5F5FFF8F5F4FFF7F4F3FFF6F4
          F2FFF5F3F2FFF5F2F0FFF5F1EFFFF4F0EFFFF4EFEEFFF3EEEDFFF2EEEDFFFCFC
          FBFFC09A8BFF0000000E00000004000000000000000000000000000000000000
          0000000000000000000200000009C9A697FFFFFFFFFFFCFCFCFFFCFCFCFFFCFC
          FAFFFCFAFAFFFCF9F9FFFBF9F8FFFAF8F7FFF9F7F7FFF9F6F5FFF8F5F5FFF8F5
          F5FFF7F5F3FFF6F4F3FFF6F3F1FFF5F2F1FFF5F1F0FFF4F1EEFFF3EFEEFFFDFC
          FCFFC19C8DFF0000000E00000003000000000000000000000000000000000000
          0000000000000000000200000009C9A899FFFFFFFFFFFDFCFDFFFDFCFCFFFCFC
          FCFFFCFCFBFFFCFBFBFFFCFBF9FFFBFAF9FFFAF9F8FFFAF8F8FFFAF8F6FFF9F6
          F5FFF8F5F5FFF8F5F4FFF7F5F4FFF7F4F3FFF5F3F1FFF5F3F0FFF5F1EFFFFDFD
          FCFFC39E8FFF0000000D00000003000000000000000000000000000000000000
          0000000000000000000200000008CBAA9AFFFFFFFFFFFEFEFEFFFDFDFDFFFDFD
          FCFFFDFCFCFFFCFCFCFFFCFCFCFFFCFBFBFFFCFBFAFFFCFAF9FFFBF9F8FFFAF8
          F8FFFAF8F6FFF9F6F5FFF8F5F5FFF8F5F5FFF7F5F4FFF6F4F3FFF6F3F2FFFEFD
          FDFFC39F90FF0000000D00000003000000000000000000000000000000000000
          0000000000000000000200000008CBAA9BFFFFFFFFFFFFFFFFFFFFFFFEFFFEFE
          FEFFFEFDFEFFFEFDFDFFFDFCFCFFFCFCFCFFFCFCFCFFFCFBFBFFFCFBFBFFFCFA
          F9FFFBFAF9FFFBF8F7FFFAF7F7FFF9F7F6FFF8F6F5FFF7F5F5FFF7F5F4FFFEFE
          FDFFC5A292FF0000000C00000003000000000000000000000000000000000000
          0000000000000000000200000006CCAB9DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFC6A394FF0000000A00000003000000000000000000000000000000000000
          0000000000000000000100000004987F75C0CDAC9DFFCCAC9DFFCDAC9DFFCCAB
          9CFFCCAB9CFFCBAB9BFFCCAA9BFFCBA99BFFCAAA9AFFCAA99AFFCAA89AFFCAA8
          99FFCAA899FFC9A798FFC9A797FFC9A798FFC9A698FFC8A697FFC7A696FFC7A5
          95FF947A6FC10000000700000002000000000000000000000000000000000000
          0000000000000000000000000002000000040000000600000007000000070000
          0007000000080000000800000008000000080000000800000008000000090000
          0009000000090000000900000009000000090000000A0000000A0000000A0000
          0009000000060000000300000001000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000003000000040000
          0005000000050000000500000005000000050000000500000006000000060000
          0006000000060000000600000005000000040000000200000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000100000001000000010000000100000002000000060000000B000000100000
          0012000000130000001300000014000000140000001500000015000000150000
          00160000001600000016000000140000000F0000000700000003000000010000
          0001000000010000000100000000000000000000000000000000000000010000
          0002000000050000000500000006000000080000000F7B5A4DC4A57867FFA577
          67FFA57767FFA47666FFA47665FFA37565FFA37565FFA37564FFA37363FFA273
          63FFA27262FFA17262FFA17262FF775448C6000000140000000A000000080000
          0006000000060000000300000001000000010000000000000000000000040000
          000A0000001100000015000000160000001B00000025A97C6BFFF8F0EDFFF7F0
          EDFFF7F0EDFFF7F0ECFFF7F0EBFFF6EEE8FFF6EEE8FFF5EDE8FFF5EDE8FFF5EC
          E7FFF6EDE7FFF5ECE7FFF5ECE6FFA37364FF0000003400000028000000230000
          001F000000170000000D000000050000000100000000000000000000000A2D1F
          1B598D6454EA9B6C5CFF9B6B5CFF976958FF8E6151FFAC8070FFF9F3EFFFDFAE
          75FFDEAE75FFDEAE76FFDEAE74FFDCA76CFFDAA164FFDAA063FFD9A062FFD89F
          61FFD99E5FFFD89D5FFFF4E9E2FFA57767FF724538FF794A3CFF7E4E3FFF8352
          42FF7A4D3FEC2618135F0000000D0000000300000001000000000000000F996F
          60EFC39F8DFFCBAB9AFFDCC5B4FFD8C0B0FFCEB6A4FF996C5AFFF9F5F2FFE1B3
          7CFFE5BC87FFE5BD88FFE5BB86FFE4B881FFDFAE73FFDFAD72FFDFAC70FFDFAB
          6EFFDEAA6CFFD89E5FFFF4EAE3FFA87A69FFBEA28DFFC8A995FFD0B39FFFC4A1
          8FFFC09B89FF936758F00000001500000005000000010000000000000011A67A
          69FFC8A797FFCDAF9EFFDFCABBFFDAC3B4FFDFCEC2FFA87B6BFFFAF6F3FFE2B7
          82FFE7C18EFFE8C08FFFE7C08DFFE7BE8AFFE2B379FFDFAF74FFDFAE72FFDFAD
          70FFDEAC6FFFD99F61FFF4EBE5FFA97D6DFFD0BAABFFC7A692FFD0B4A0FFC3A2
          90FFC5A493FF9F6F5FFF0000001800000007000000010000000000000011A87B
          6BFFCBAB9BFFD0B3A4FFE3D0C3FFDFCABBFFA37D6CFFAB7F6FFFFAF7F5FFE5BD
          88FFE8C493FFE9C596FFE8C493FFE8C392FFE5BA84FFE0B076FFE0B074FFE0AE
          72FFDFAD70FFD9A062FFF5ECE7FFAD8071FF9E7766FFCEB3A0FFD7BEADFFC8A8
          97FFC9A898FFA07160FF0000001900000006000000010000000000000010AA7E
          6DFFCEB0A1FFD3B8AAFFE7D7CCFFE5D3C6FF966C5BFF7A4835FF7A4836FF7A48
          35FF7A4835FF7A4835FF7A4835FF7A4835FF7A4835FF7A4835FF7A4835FF7A48
          36FF7A4836FF7A4835FF7A4835FF7A4835FF966C5BFFE5D3C7FFE7D7CBFFD3B8
          AAFFCEAFA1FFA17261FF000000180000000600000001000000000000000FAC80
          6FFFD1B6A7FFD6BEB1FFEBDDD5FFE8D9CEFFE8D9CFFFE8D9CFFFE8DACEFFE8D9
          CEFFE8D9CEFFE8D9CFFFE8D9CEFFE8DACFFFE8D9CFFFE8D9CFFFE8DACEFFE8D9
          CFFFE8DACFFFE8D9CEFFE8D9CEFFE8D9CFFFE8D9CEFFE8DACEFFEBDED5FFD6BE
          B1FFD1B6A8FFA37463FF000000170000000600000001000000000000000EAE82
          72FFD4BBAFFFDAC4B9FFEFE5DDFFECE0D7FFECE0D7FFECE0D7FFECE0D8FFECE0
          D7FFECE0D8FFECE0D7FFECE0D7FFECE0D7FFECE0D7FFECE0D7FFECE0D7FFECE0
          D7FFECE0D7FFECE0D7FFECE0D7FFECE0D7FFECE0D7FFECE0D7FFEFE5DDFFDAC4
          B9FFD4BBAFFFA47665FF000000160000000500000001000000000000000DAF84
          74FFD8C1B7FFDECBC1FFF4EBE6FFF0E7E0FFF1E8E2FFF5EFEAFFF6F2EDFFF7F3
          EEFFF7F3EFFFF7F3EFFFF7F3EEFFF7F3EEFFF7F3EFFFF7F3EEFFF7F3EFFFF7F3
          EFFFF7F3EFFFF7F2EFFFF6F1EEFFF5EFE9FFF1E8E2FFF0E6DFFFF3EBE6FFDECB
          C0FFD8C1B7FFA67867FF000000150000000500000001000000000000000CB289
          78FFDCC7BEFFE1D0C8FFF6F1ECFFF5EEEAFFE8E2DEFF8C7972FF654D45FF553C
          32FF553A32FF553B32FF543A31FF543A32FF543931FF523930FF523830FF5238
          30FF52372FFF51372EFF614941FF88766FFFE7E1DDFFF5EEE9FFF6F1EDFFE1D0
          C8FFDCC7BDFFA87C6BFF000000140000000500000001000000000000000BB68D
          7DFFDFCCC4FFE4D5CEFFF9F5F2FFE0D8D4FF674E44FF674A40FF6B4E44FF6D4F
          45FF6E4F45FF6E4F45FF6D4F45FF6D4F45FF6D4F45FF6E4F45FF6D4F45FF6D4F
          45FF6D4F45FF6D4F45FF6C4D43FF65483EFF614840FFDFD7D3FFF9F5F2FFE4D5
          CEFFDFCCC4FFAC8070FF000000130000000500000000000000000000000AB992
          81FFE2D1CBFFE8DAD4FFFBF9F7FF907F78FF6B4F45FF715449FF715349FF7153
          49FF705349FF715449FF71534AFF715349FF71544AFF715349FF715349FF7154
          49FF705349FF715449FF70534AFF715349FF684D42FF8B7973FFFBF9F7FFE8DA
          D4FFE2D1CBFFAF8575FF0000001100000004000000000000000000000009BC97
          87FFE5D6CFFFEBDFD9FFFDFCFAFF6C554AFF896B5FFF8D6F63FF917265FF9677
          69FF957668FF97796AFF9C7E6FFF9E7F70FF9E7F70FF9E7F70FF9E7F70FF9E7F
          70FF9D7D6EFF967869FF917367FF8E7264FF85695CFF765D51FFEBEAE8FFDED3
          CDFFDECFC9FFB28A7AFF0000001000000004000000000000000000000007B592
          83F1E5D6D0FFECE1DDFFFEFDFCFF6E5649FFB19280FFB29381FFB29381FFB393
          81FFB29381FFB29381FFB29381FFB29381FFB29381FFB29281FFB39281FFB192
          80FFB0917FFFA88A7AFF68725BFF356145FF135936FF135937FF135937FF3C6F
          54FF768D7CFF9E7D6FF50000001300000005000000000000000000000005846C
          61AFDDCAC2FFEEE4E1FFFEFEFEFF7F6759FFC4A490FF715349FF705349FF7152
          49FF705348FF705248FF705248FF705248FF705248FF6E5147FF6E5046FF6C4F
          45FF5D4C3FFF28563AFF176E47FF1F9D6AFF2CB785FF32C38FFF2CB784FF1D9B
          68FF166E47FF1E5A3CF704110B4F0000000A0000000100000000000000031E19
          172DBFA296F0E1D0C8FFF9F5F3FF866E5FFFC8A994FFA97C6BFFF2EAE5FFEEE2
          DBFFEDE2DAFFEDE1DAFFEDE1DBFFECE0D9FFECE0D9FFEDE0D9FFEADCD5FFCDC8
          BEFF256948FF1E8D60FF26BC84FF27BE86FF3FC897FF8FEAD2FF77DFBFFF24BB
          83FF22B87EFF1B8E5FFF165F3DF6030E093B0000000500000000000000010000
          000315121020856F67ABB59789E58E786AFF937D71FFAD8272FFF4ECE8FFEFE4
          DEFFEFE3DEFFEFE4DDFFEEE3DDFFEEE2DDFFEEE3DCFFEDE1DBFFE5DAD3FF5284
          69FF239A6CFF2DC38DFF2EC38DFF2DC38DFF53BE99FF1B643CFF378260FF35C3
          90FF26BD86FF25BB83FF1E9968FF124D33CF0001011300000002000000000000
          00010000000200000004000000050000000A00000015B18876FFF4EFEBFFF1E7
          E1FFF1E6E1FFF0E6E0FFF0E5E1FFEFE5E0FFF0E5DFFFEDE2DBFFA2B4A4FF2B83
          5EFF33C793FF34C894FF35C795FF35C794FF299169FF115A35FF125934FF2CB8
          85FF2DC28DFF2AC08AFF27BE86FF2B845EFF0A2C1D7C00000007000000000000
          0000000000000000000100000001000000040000000EB68D7EFFF6F1EEFFF2E9
          E4FFF2E9E5FFF2E9E3FFF1E8E3FFF2E8E3FFF1E7E2FFEBE2DDFF588F74FF3CAF
          85FF3BCC9AFF3CCB9CFF3CCC9CFF3CCC9CFF38C090FF2E8C67FF35A178FF36C8
          96FF34C692FF30C38FFF2CC28DFF30A87BFF155739CD0000000B000000000000
          0000000000000000000000000000000000030000000CBB9484FFF9F4F0FFF4EC
          E8FFF3EBE8FFF3EBE7FFF3EBE7FFF3EBE6FFF3EAE5FFEDE4E0FF368460FF44C5
          9BFF42D0A2FF45D1A3FF45D2A4FF45D2A3FF44D0A3FF74CBB0FF7DD3BAFF4AD1
          A6FF39CB9AFF36C996FF32C792FF30BD8AFF1D744EF80000000E000000000000
          0000000000000000000000000000000000030000000BBF998AFFF9F5F3FFF6EF
          EBFFF6EEEBFFF5EEEBFFF5EEEAFFF5EDE9FFF5EDE9FFEFE7E4FF2B835DFF59D4
          AEFF4BD5A9FF4CD6ABFF4ED6ABFF55D9B0FF53D8AEFF24734FFF23724CFF8CDF
          C9FF65DDB7FF3DCD9CFF3AC99AFF44CC9CFF207E56FF01040317000000000000
          00000000000000000000000000000000000200000009C3A08FFFFAF7F5FFF7F1
          EEFFF7F1EEFFF6F0EDFFF7F0EDFFF7F0ECFFF6F0ECFFF1EAE6FF3F926FFF75D8
          B9FF52D9AFFF55DAB2FF56DBB2FF93E7D2FF98EBD7FF399976FF186840FF1F6E
          47FF6DBFA3FF43D1A3FF3FCE9FFF69D5B0FF23815AF70000000C000000000000
          00000000000000000000000000000000000200000008C7A495FFFBF8F7FFF8F4
          F1FFF9F3F1FFF9F3F0FFF8F2F0FFF7F2EFFFF8F3EFFFF4EFECFF67AA8DFF88D1
          B8FF5CDCB7FF5DDEB8FF5EDFBAFF23734DFF21754CFF75D4B7FF20714AFF1B6C
          43FF1D6C45FF49D4A9FF45D2A4FF83D0B6FF1D6C4CCA00000008000000000000
          00000000000000000000000000000000000200000007CAA99AFFFDFAF8FFFAF6
          F4FFFAF6F4FFF9F6F4FFF9F5F2FFF9F5F2FFF9F5F2FFF8F4F0FFB6D3C4FF4AA7
          84FF98EED6FF65E1BDFF67E2BFFF2D8260FF196942FF50A384FF20734BFF1866
          3FFF267C58FF4FD7ADFF87E7CBFF50AC89FF0F37276C00000004000000000000
          00000000000000000000000000000000000100000005CDAD9FFFFDFBFBFFFDFB
          FAFFFDFBFAFFFDFBF9FFFDFBFAFFFCFAF9FFFCFAF9FFFCF9F8FFF9F6F5FF5DAE
          8EFF87CFB6FFA7F3DFFF71E7C7FF67DAB9FF338C6AFF1A6542FF1A6442FF328F
          6CFF59D5AFFF98EDD5FF8CD3BAFF247E5BD60102010B00000001000000000000
          000000000000000000000000000000000001000000039A8378BFCFB0A2FFCEB0
          A1FFCEB0A1FFCEAFA1FFCEB0A1FFCEAFA1FFCEAFA1FFCDAEA0FFCCAD9EFFBDAA
          99FF3FA077FF7AC9AEFFC3F8EBFFA0F2DDFF8AEDD4FF75E8CAFF85EACFFF98EF
          D7FFBFF7E8FF80CDB2FF2C926BEC0616102D0000000200000000000000000000
          0000000000000000000000000000000000000000000100000002000000040000
          0004000000050000000500000005000000060000000600000007000000070000
          0008040F0B22247B5BC449B28BFF8CD6BEFFB0EAD8FFCEFAEFFFB0EAD8FF8ED7
          BFFF4EB38EFF25825FCF06130E25000000020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0001000000010000000100000001000000010000000100000001000000010000
          000200000002000000040E2F234C1F6D50A72A8B65D431A87CFF2A8D67D72173
          55B1103527560000000400000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000200000002000000020000
          0002000000010000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000010000000200000004000000050000000600000007000000070000
          0006000000050000000400000002000000010000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000010000
          0003000000060000000B0000001000000015000000180000001A0000001A0000
          001800000016000000110000000C000000070000000400000001000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000003000000070000
          000E0000001707140E4D123324951B4F37D11B523AD9226547FF226547FF1C52
          3BD91A4F37D11134249607140F4F000000190000001000000008000000040000
          0001000000010000000000000000000000000000000000000000000000000000
          000000000000000000000000000100000001000000040000000B000000150B22
          176B1D573FE1237351FF248B63FF269F72FF27A477FF28B37FFF28B280FF27A5
          77FF27A072FF248B64FF247351FF1E583FE20C22186D000000180000000D0000
          0005000000010000000100000000000000000000000000000000000000000000
          0000000000000000000100000001000000050000000D040C09361A5039CF247A
          56FF27A374FF29B480FF28B481FF29B381FF29B381FF29B480FF29B482FF29B4
          80FF29B380FF29B381FF29B480FF27A374FF257B57FF1B5039D1040C093A0000
          0010000000060000000100000001000000000000000000000000000000000000
          00000000000000000001000000050000000E091A1255216347F026966BFF29B4
          82FF29B482FF29B482FF29B583FF29B583FF29B583FF29B583FF2AB583FF2AB5
          83FF29B483FF29B582FF29B482FF29B482FF29B482FF26976CFF226348F1091A
          135A000000120000000600000001000000000000000000000000000000000000
          000000000000000000040000000C091A1253236D4EFA27A174FF29B482FF2AB4
          82FF29B584FF2AB584FF2AB584FF2BB583FF2BB584FF2BB685FF2BB684FF2BB6
          85FF2BB685FF2BB583FF2BB684FF2BB684FF2AB583FF2AB482FF27A174FF236E
          4FFA091B12590000001000000005000000010000000000000000000000000000
          00000000000200000009050F0B38236648EE2BA77AFF2BB684FF2AB584FF2BB6
          84FF2BB685FF2BB686FF2BB785FF2BB786FF2BB786FF2BB786FF2BB786FF2CB8
          86FF2CB786FF2BB786FF2BB786FF2BB685FF2BB684FF2BB685FF2CB684FF2CA8
          7AFF21654AEF050F0B3E0000000C000000030000000100000000000000000000
          000100000005000000101B523BCB2B9970FF2DB685FF2BB685FF2BB785FF2BB7
          86FF2CB887FF2CB887FF2DB888FF3CC093FF41C497FF41C398FF41C398FF42C4
          98FF3CC193FF2DB888FF2DB888FF2CB887FF2BB786FF2BB786FF2BB786FF2DB7
          86FF2B9A71FF1C543CD000000016000000070000000100000000000000000000
          00020000000A0B231A652B7F5CFF31B98AFF2BB685FF2BB786FF2CB887FF2DB8
          87FF2EB989FF2EB989FF2EBA89FF1F9063FF137F51FF137F52FF137F51FF137E
          51FF209165FF2EB989FF2EB98AFF2EB989FF2DB889FF2DB987FF2CB787FF2BB7
          86FF31B98BFF2B805EFF0C241A6B0000000E0000000300000001000000010000
          00040000000F1E5C43DD2FA87DFF2DB888FF2CB887FF2DB888FF2EB989FF2EBA
          89FF2FBA8AFF2FBC8AFF30BC8BFF127D50FFFEFDFCFFFEFDFCFFFEFDFCFFFEFD
          FCFF127C51FF31BC8BFF30BC8BFF2FBC89FF2FBA8AFF2FB988FF2EB989FF2DB8
          88FF2EB989FF30A97DFF1F5E44DF000000160000000700000001000000020000
          0007091B144E2E8261FF34BC8CFF2DB888FF2DB988FF2EB989FF30BA8BFF30BC
          8BFF31BD8CFF31BD8CFF32BE8DFF117B50FFF4EAE4FFF5EAE3FFF4EAE4FFF4E9
          E3FF117A4CFF32BE8DFF31BD8CFF31BD8CFF31BD8CFF30BC8BFF2FBC89FF2EBA
          89FF2EB989FF34BC8DFF2E8362FF0A1D15570000000B00000002000000020000
          0009133C2A92379D78FF32BC8BFF2FBA89FF2FBC89FF30BC8BFF31BD8CFF32BD
          8DFF33BE8EFF33BE8FFF34BF8FFF13794DFFF5ECE5FFF5EBE5FFF5ECE5FFF5EB
          E5FF13794DFF34BF90FF34BF8FFF33BE8FFF33BE8EFF32BD8EFF31BD8CFF31BC
          8CFF2FBA8BFF33BD8CFF389E79FF143C2C980000000E00000003000000020000
          000B1D553FC63AAE86FF32BB8CFF30BA8BFF30BC8BFF32BE8DFF43C69BFF48C9
          9EFF49C99EFF4ACA9FFF4ACBA0FF12794CFFF6EDE7FFF6EDE7FFF6ECE7FFF6ED
          E7FF11784CFF4BCBA0FF4BCB9FFF4ACA9FFF49C99EFF45C79BFF32BE8DFF32BE
          8DFF30BC8BFF32BC8EFF3BAF87FF1D5740CA0000001100000004000000030000
          000C236C4EEC3BBB8EFF31BD8CFF32BC8DFF33BE8EFF33BE8FFF188155FF1170
          48FF127448FF12754AFF13754AFF12764AFFF6EFEAFFF7EFEAFFF7EEE9FFF7EF
          EAFF12754BFF12754AFF117349FF117147FF117045FF187F53FF34C08FFF33BE
          8FFF32BE8DFF32BE8DFF3CBC8FFF236F50ED0000001300000005000000030000
          000C267655FA40C396FF32BD8CFF33BE8EFF34BF8FFF36C091FF31B587FF3182
          5DFFE7E7E4FFFEFDFDFFFEFEFDFFFEFEFDFFF8F2EEFFF7F2EEFFF8F1ECFFF7F1
          EDFFFEFEFDFFFEFEFDFFFEFDFDFFE8E8E5FF337F5BFF32B487FF36C191FF36C0
          91FF34BF8FFF32BE8DFF41C497FF267857FA0000001300000005000000030000
          000B267856FA49C79CFF32BF8DFF34BF8FFF35C190FF37C193FF38C395FF2AA3
          77FF5E9376FFF1E8E4FFF8F3EEFFF8F3EFFFF9F3F0FFF8F4F0FFF8F4F0FFF8F3
          EFFFF8F2EFFFF8F0EBFFF1E7E2FF5F9074FF2A9F75FF39C496FF38C395FF37C2
          93FF35C091FF35BF8EFF4BC89EFF277A59FA0000001200000005000000020000
          000A247252EC52C59FFF36C092FF35C190FF37C193FF3AC69AFF3ECEA4FF3FD4
          ABFF218B64FF9DB3A1FFF6F1EEFFF9F5F2FFF9F6F3FFF9F6F3FFF9F5F2FFF9F5
          F2FFF9F4F0FFF6F0ECFF9FB1A1FF1F855FFF40D0A5FF3DCCA0FF3AC69AFF39C2
          95FF37C293FF38C293FF53C7A0FF257456ED0000001100000004000000020000
          00081E5F46C556BF9DFF3AC394FF39C597FF40D0A7FF41D5ADFF42D6AEFF43D7
          AFFF40D2ABFF217552FFCFD2CAFFFAF7F4FFFAF7F5FFFAF8F5FFFAF7F5FFFAF7
          F4FFF9F6F2FFD0D2CAFF21704EFF40CDA4FF41D3A9FF40D2A7FF40D0A5FF3ECB
          A1FF3AC497FF3CC497FF58C19FFF1F6047C80000000E00000003000000010000
          00061544328F56B394FF46D0A7FF43D5ADFF44D6AFFF45D8B0FF45D8B1FF46DA
          B3FF47DAB3FF3CBF99FF3F785CFFEFEAE7FFFBF9F8FFFBFAF8FFFBF9F7FFFBF8
          F6FFEEE9E5FF43765CFF39B993FF47D6AEFF46D5ACFF44D4AAFF42D2A8FF42D1
          A6FF40CDA4FF45CCA3FF56B496FF164533940000000B00000003000000010000
          00040A2018483EA081FF60E1C1FF48D7B2FF48DAB3FF4ADAB4FF4BDAB5FF4CDB
          B5FF4CDCB8FF4DDDB8FF2D9A76FF7A9986FFF6F5F2FFFCFBF9FFFCFAF9FFF7F4
          F2FF7C9886FF2C9571FF4BDAB4FF4AD9B2FF49D7B1FF47D5ADFF46D4ACFF44D2
          A9FF43D0A7FF5FDBB9FF3E9E7DFF0A20184E0000000700000002000000000000
          0002000000072C7F62DA69D6BCFF52DCB8FF4EDBB6FF4EDCB7FF50DDB9FF50DE
          BBFF51DFBAFF52DEBBFF51DDBAFF217655FFB4C0B6FFFBFBFAFFFAFAF9FFB7C0
          B7FF207150FF50DBB6FF4FDCB7FF4EDBB5FF4DD9B3FF4BD8B2FF49D6AFFF47D4
          ACFF4BD5AEFF6BD1B6FF287A5DDC0000000E0000000400000001000000000000
          0001000000041132285B45AB8CFF6FE7CAFF53DEBAFF53DEBBFF55DFBDFF55DF
          BDFF56E0BEFF56E1BEFF57E2BFFF4ED1AFFF2B6749FFE2E1DDFFE2E1DDFF2D66
          49FF4CCDAAFF55DFBCFF54DDBAFF52DDB9FF51DBB7FF4EDAB4FF4ED8B3FF4CD6
          AFFF6DE1C4FF44A687FF10302561000000080000000200000000000000000000
          000000000002000000062A785FC669D0B7FF67E6C7FF59E0BEFF5AE1C0FF5BE1
          C1FF5BE2C1FF5CE4C3FF5DE4C3FF5CE4C3FF43B392FF537A64FF547B65FF41B0
          8FFF5AE2C0FF59E2BFFF59E0BDFF57DFBCFF55DDB9FF54DBB8FF51DAB5FF62DF
          BEFF6BCDB1FF267359C80000000C000000040000000100000000000000000000
          0000000000010000000307151028389879EC7BE1CBFF6CE6C9FF60E3C4FF60E3
          C4FF61E4C5FF62E5C5FF61E6C6FF61E6C6FF61E6C6FF2E8666FF2C8364FF60E5
          C5FF60E5C4FF5EE3C2FF5CE2C0FF5BE1BEFF59DEBDFF57DDBAFF66E0C1FF7EDD
          C7FF349474ED07140F2D00000006000000020000000000000000000000000000
          00000000000000000001000000040F28214641A889F980E1CCFF7BECD4FF66E5
          C7FF66E6C8FF67E7C8FF67E7C9FF67E8CAFF67E7C9FF67E7C9FF67E7C9FF66E7
          C8FF65E6C7FF63E5C5FF61E3C3FF5FE2C2FF5EE0BFFF77E7CEFF80DEC8FF3DA4
          83FA0D271F4A0000000700000002000000000000000000000000000000000000
          000000000000000000000000000100000004102A21453DA081EE75D7BFFF91F3
          E0FF78EBD1FF6CE8CBFF6CE9CCFF6DE9CCFF6DE9CCFF6CEACDFF6BE9CCFF6AE9
          CBFF69E7C9FF68E6C7FF66E5C6FF72E7CCFF90F0DBFF73D4BBFF389D7CEE0E29
          204A000000070000000200000001000000000000000000000000000000000000
          000000000000000000000000000000000001000000030814102333876DC857C1
          A3FF89E6D3FF9BF6E6FF8AF1DDFF7FEDD6FF7BEDD5FF72EAD0FF71EACFFF7AEC
          D3FF7CECD3FF89EFDBFF9BF4E3FF89E6D1FF55C0A1FF2F8669CA071410280000
          0006000000020000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000002000000051638
          2E563A9A7CDA51BF9EFF73D6BDFF8CE8D4FF92ECDAFFA5F7E9FFA5F7E9FF93EC
          DAFF8BE8D4FF71D5BBFF4FBD9CFF379779DA153A2E5B00000007000000040000
          0002000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000010000
          0003000000040D201A32235B4A82368F73C539977ACE46BA96FC46BC98FC3796
          7ACF358F74C6225B4A840C201A34000000060000000400000002000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000010000000200000003000000040000000500000005000000050000
          0005000000040000000400000003000000020000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000010000000100000001000000010000
          0001000000010000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000010000000200000004000000050000000600000007000000070000
          0006000000050000000400000002000000010000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000010000
          0003000000060000000B0000001000000015000000180000001A0000001A0000
          001800000016000000110000000C000000070000000400000001000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000010000000100000003000000070000
          000E0000001707140E4D123324951B4F37D11B523AD9226547FF226547FF1C52
          3BD91A4F37D11134249607140F4F000000190000001000000008000000040000
          0001000000010000000000000000000000000000000000000000000000000000
          000000000000000000000000000100000001000000040000000B000000150B22
          176B1D573FE1237351FF248B63FF269F72FF27A477FF28B37FFF28B280FF27A5
          77FF27A072FF248B64FF247351FF1E583FE20C22186D000000180000000D0000
          0005000000010000000100000000000000000000000000000000000000000000
          0000000000000000000100000001000000050000000D040C09361A5039CF247A
          56FF27A374FF29B480FF28B481FF29B381FF29B381FF29B480FF29B482FF29B4
          80FF29B380FF29B381FF29B480FF27A374FF257B57FF1B5039D1040C093A0000
          0010000000060000000100000001000000000000000000000000000000000000
          00000000000000000001000000050000000E091A1255216347F026966BFF29B4
          82FF29B482FF29B482FF29B583FF29B583FF29B583FF29B583FF2AB583FF2AB5
          83FF29B483FF29B582FF29B482FF29B482FF29B482FF26976CFF226348F1091A
          135A000000120000000600000001000000000000000000000000000000000000
          000000000000000000040000000C091A1253236D4EFA27A174FF29B482FF2AB4
          82FF29B584FF2AB584FF2AB584FF2BB583FF2BB584FF2BB685FF2BB684FF2BB6
          85FF2BB685FF2BB583FF2BB684FF2BB684FF2AB583FF2AB482FF27A174FF236E
          4FFA091B12590000001000000005000000010000000000000000000000000000
          00000000000200000009050F0B38236648EE2BA77AFF2BB684FF2AB584FF2BB6
          84FF2BB685FF2BB686FF2BB785FF2BB786FF2BB786FF3EC196FF3EC296FF2CB8
          86FF2CB786FF2BB786FF2BB786FF2BB685FF2BB684FF2BB685FF2CB684FF2CA8
          7AFF21654AEF050F0B3E0000000C000000030000000100000000000000000000
          000100000005000000101B523BCB2B9970FF2DB685FF2BB685FF2BB785FF2BB7
          86FF2CB887FF2CB887FF2DB888FF2DB887FF37BE90FF289067FF278F67FF38BE
          8FFF2DB987FF2DB888FF2DB888FF2CB887FF2BB786FF2BB786FF2BB786FF2DB7
          86FF2B9A71FF1C543CD000000016000000070000000100000000000000000000
          00020000000A0B231A652B7F5CFF31B98AFF2BB685FF2BB786FF2CB887FF2DB8
          87FF2EB989FF2EB989FF2EBA89FF33BC8BFF39AA83FF5B977CFF5D997EFF39A9
          81FF33BC8CFF2EB989FF2EB98AFF2EB989FF2DB889FF2DB987FF2CB787FF2BB7
          86FF31B98BFF2B805EFF0C241A6B0000000E0000000300000001000000010000
          00040000000F1E5C43DD2FA87DFF2DB888FF2CB887FF2DB888FF2EB989FF2EBA
          89FF2FBA8AFF2FBC8AFF30BC8BFF41BD93FF2F7C59FFF0F4F1FFF0F4F1FF327D
          5CFF41BA92FF31BC8BFF30BC8BFF2FBC89FF2FBA8AFF2FB988FF2EB989FF2DB8
          88FF2EB989FF30A97DFF1F5E44DF000000160000000700000001000000020000
          0007091B144E2E8261FF34BC8CFF2DB888FF2DB988FF2EB989FF30BA8BFF30BC
          8BFF31BD8CFF31BD8CFF41C598FF227F5AFFC2D7CCFFFBF7F5FFFBF7F5FFC5D9
          CEFF217D57FF41C599FF31BD8CFF31BD8CFF31BD8CFF30BC8BFF2FBC89FF2EBA
          89FF2EB989FF34BC8DFF2E8362FF0A1D15570000000B00000002000000020000
          0009133C2A92379D78FF32BC8BFF2FBA89FF2FBC89FF30BC8BFF31BD8CFF32BD
          8DFF33BE8EFF3AC294FF329871FF86AF9BFFFDFBFAFFF6ECE6FFF6EDE6FFFDFB
          FAFF88B09DFF319770FF3CC395FF33BE8FFF33BE8EFF32BD8EFF31BD8CFF31BC
          8CFF2FBA8BFF33BD8CFF389E79FF143C2C980000000E00000003000000020000
          000B1D553FC63AAE86FF32BB8CFF30BA8BFF30BC8BFF32BE8DFF33BE8FFF33BF
          8EFF36C091FF42B48DFF448364FFFBFBFAFFF8F1ECFFF6EDE7FFF6ECE7FFF8F1
          ECFFFBFBFAFF478467FF42B38CFF37C193FF34BF90FF34BF8FFF32BE8DFF32BE
          8DFF30BC8BFF32BC8EFF3BAF87FF1D5740CA0000001100000004000000030000
          000C236C4EEC3BBB8EFF31BD8CFF32BC8DFF33BE8EFF33BE8FFF34C08FFF36C1
          92FF48C69CFF227350FFDFE8E4FFFBF6F4FFF6EFEAFFF7EFEAFFF7EEE9FFF7EF
          EAFFFAF6F3FFE2EAE6FF247350FF49C59DFF36C192FF35C091FF34C08FFF33BE
          8FFF32BE8DFF32BE8DFF3CBC8FFF236F50ED0000001300000005000000030000
          000C267655FA40C396FF32BD8CFF33BE8EFF34BF8FFF36C091FF36C192FF43C8
          9CFF28845FFFACC5B9FFFDFBF9FFF7F0EBFFF8F2EEFFF7F2EEFFF8F1ECFFF7F1
          EDFFF7EFEBFFFCFAF9FFAFC7BBFF27835EFF44C89EFF38C293FF36C191FF36C0
          91FF34BF8FFF32BE8DFF41C497FF267857FA0000001300000005000000030000
          000B267856FA49C79CFF32BF8DFF34BF8FFF35C190FF37C193FF3DC598FF3DA6
          81FF65967EFFFEFDFCFFF9F4F0FFF8F3EFFFF9F3F0FFF8F4F0FFF8F4F0FFF8F3
          EFFFF8F2EFFFF9F1EDFFFEFDFCFF689781FF3DA480FF3EC69AFF38C395FF37C2
          93FF35C091FF35BF8EFF4BC89EFF277A59FA0000001200000005000000020000
          000A247252EC52C59FFF36C092FF35C190FF37C193FF3AC69AFF4DC5A1FF3071
          54FFE1DEDBFFE7DED9FFE6DCD7FFE7DDD8FFF9F6F3FFF9F6F3FFF9F5F2FFF9F5
          F2FFE7DCD6FFE6DBD6FFE7DDD8FFE1DEDBFF337253FF4CC29DFF3AC69AFF39C2
          95FF37C293FF38C293FF53C7A0FF257456ED0000001100000004000000020000
          00081E5F46C556BF9DFF3AC394FF39C597FF40D0A7FF41D5ADFF1C7E57FF1167
          41FF116A43FF116B44FF116B44FF126C44FFFAF7F5FFFAF8F5FFFAF7F5FFFAF7
          F4FF116B44FF116B43FF116943FF116941FF116740FF1B7B54FF40D0A5FF3ECB
          A1FF3AC497FF3CC497FF58C19FFF1F6047C80000000E00000003000000010000
          00061544328F56B394FF46D0A7FF43D5ADFF44D6AFFF45D8B0FF45D8B1FF46DA
          B3FF47DAB3FF48DBB5FF4ADAB5FF116942FFFBF9F8FFFBFAF8FFFBF9F7FFFBF8
          F6FF116942FF48D8B2FF47D7B0FF47D6AEFF46D5ACFF44D4AAFF42D2A8FF42D1
          A6FF40CDA4FF45CCA3FF56B496FF164533940000000B00000003000000010000
          00040A2018483EA081FF60E1C1FF48D7B2FF48DAB3FF4ADAB4FF4BDAB5FF4CDB
          B5FF4CDCB8FF4DDDB8FF4DDDB8FF116640FFFBFBF8FFFCFBF9FFFCFAF9FFFBF9
          F8FF11663FFF4CDBB5FF4BDAB4FF4AD9B2FF49D7B1FF47D5ADFF46D4ACFF44D2
          A9FF43D0A7FF5FDBB9FF3E9E7DFF0A20184E0000000700000002000000000000
          0002000000072C7F62DA69D6BCFF52DCB8FF4EDBB6FF4EDCB7FF50DDB9FF50DE
          BBFF51DFBAFF52DEBBFF52DFBCFF11653DFFFBFBFAFFFCFCFBFFFBFBFAFFFBF9
          F7FF11653DFF51DDB8FF4FDCB7FF4EDBB5FF4DD9B3FF4BD8B2FF49D6AFFF47D4
          ACFF4BD5AEFF6BD1B6FF287A5DDC0000000E0000000400000001000000000000
          0001000000041132285B45AB8CFF6FE7CAFF53DEBAFF53DEBBFF55DFBDFF55DF
          BDFF56E0BEFF56E1BEFF57E2BFFF10623BFFE9E2DFFFEAE3E0FFE9E2DFFFE9E0
          DDFF11613DFF55DFBCFF54DDBAFF52DDB9FF51DBB7FF4EDAB4FF4ED8B3FF4CD6
          AFFF6DE1C4FF44A687FF10302561000000080000000200000000000000000000
          000000000002000000062A785FC669D0B7FF67E6C7FF59E0BEFF5AE1C0FF5BE1
          C1FF5BE2C1FF5CE4C3FF5DE4C3FF25825EFF125F39FF12613AFF0F613AFF1160
          3BFF268460FF59E2BFFF59E0BDFF57DFBCFF55DDB9FF54DBB8FF51DAB5FF62DF
          BEFF6BCDB1FF267359C80000000C000000040000000100000000000000000000
          0000000000010000000307151028389879EC7BE1CBFF6CE6C9FF60E3C4FF60E3
          C4FF61E4C5FF62E5C5FF61E6C6FF61E6C6FF61E6C6FF62E6C6FF61E5C6FF60E5
          C5FF60E5C4FF5EE3C2FF5CE2C0FF5BE1BEFF59DEBDFF57DDBAFF66E0C1FF7EDD
          C7FF349474ED07140F2D00000006000000020000000000000000000000000000
          00000000000000000001000000040F28214641A889F980E1CCFF7BECD4FF66E5
          C7FF66E6C8FF67E7C8FF67E7C9FF67E8CAFF67E7C9FF67E7C9FF67E7C9FF66E7
          C8FF65E6C7FF63E5C5FF61E3C3FF5FE2C2FF5EE0BFFF77E7CEFF80DEC8FF3DA4
          83FA0D271F4A0000000700000002000000000000000000000000000000000000
          000000000000000000000000000100000004102A21453DA081EE75D7BFFF91F3
          E0FF78EBD1FF6CE8CBFF6CE9CCFF6DE9CCFF6DE9CCFF6CEACDFF6BE9CCFF6AE9
          CBFF69E7C9FF68E6C7FF66E5C6FF72E7CCFF90F0DBFF73D4BBFF389D7CEE0E29
          204A000000070000000200000001000000000000000000000000000000000000
          000000000000000000000000000000000001000000030814102333876DC857C1
          A3FF89E6D3FF9BF6E6FF8AF1DDFF7FEDD6FF7BEDD5FF72EAD0FF71EACFFF7AEC
          D3FF7CECD3FF89EFDBFF9BF4E3FF89E6D1FF55C0A1FF2F8669CA071410280000
          0006000000020000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000002000000051638
          2E563A9A7CDA51BF9EFF73D6BDFF8CE8D4FF92ECDAFFA5F7E9FFA5F7E9FF93EC
          DAFF8BE8D4FF71D5BBFF4FBD9CFF379779DA153A2E5B00000007000000040000
          0002000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000010000
          0003000000040D201A32235B4A82368F73C539977ACE46BA96FC46BC98FC3796
          7ACF358F74C6225B4A840C201A34000000060000000400000002000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0001000000010000000200000003000000040000000500000005000000050000
          0005000000040000000400000003000000020000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000010000000100000001000000010000
          0001000000010000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end>
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 31
    Top = 3
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = clHighlight
      TextColor = clHighlightText
    end
    object SchDefBkg: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
  end
end
