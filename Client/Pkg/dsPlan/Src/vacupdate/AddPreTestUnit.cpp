﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AddPreTestUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TAddPreTestForm * AddPreTestForm;
//---------------------------------------------------------------------------
__fastcall TAddPreTestForm::TAddPreTestForm(TComponent * Owner, TdsICPlanDM * ADM)
    : TForm(Owner)
 {
  FDM = ADM;
  InfCB->Properties->Items->Clear();
  TestCB->Properties->Items->Clear();
  TStringList * FItemList = new TStringList;
  try
   {
    FDM->LoadInfList(FItemList);
    FItemList->Sort();
    for (int i = 0; i < FItemList->Count; i++)
     {
      InfCB->Properties->Items->AddObject(FItemList->Strings[i], FItemList->Objects[i]);
     }

    FItemList->Clear();
    FDM->LoadTestList(FItemList);
    FItemList->Sort();
    for (int i = 0; i < FItemList->Count; i++)
     {
      TestCB->Properties->Items->AddObject(FItemList->Strings[i], FItemList->Objects[i]);
     }
   }
  __finally
   {
    delete FItemList;
    CheckCtrl();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TAddPreTestForm::CheckCtrl()
 {
  ApplyBtn->Enabled = (InfCB->ItemIndex != -1) &&
      (TestCB->ItemIndex != -1) &&
      (PreTestType->ItemIndex != -1) &&
      ReakED->Text.Trim().Length() &&
      ActualED->Text.Trim().Length() &&
      PauseED->Text.Trim().Length();
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddPreTestForm::FGetInf()
 {
  UnicodeString RC = "-1";
  if (InfCB->ItemIndex != -1)
   {
    RC = IntToStr((int)InfCB->Properties->Items->Objects[InfCB->ItemIndex]);
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddPreTestForm::FGetTest()
 {
  UnicodeString RC = "-1";
  if (TestCB->ItemIndex != -1)
   {
    RC = IntToStr((int)TestCB->Properties->Items->Objects[TestCB->ItemIndex]);
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddPreTestForm::FGetPlantype()
 {
  UnicodeString RC = "0";
  if (PreTestType->ItemIndex != -1)
   {
    RC = IntToStr(PreTestType->ItemIndex);
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddPreTestForm::FGetReak()
 {
  return ReakED->Text.Trim();
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddPreTestForm::FGetActuals()
 {
  return ActualED->Text.Trim();
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddPreTestForm::FGetPause()
 {
  return PauseED->Text.Trim();
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddPreTestForm::FGetMinAge()
 {
  return MinAgeED->Text.Trim();
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddPreTestForm::FGetMaxAge()
 {
  return MaxAgeED->Text.Trim();
 }
//---------------------------------------------------------------------------
void __fastcall TAddPreTestForm::MinAgeEDPropertiesValidate(TObject * Sender, Variant & DisplayValue,
    TCaption & ErrorText, bool & Error)
 {
  if (Error)
   {
    UnicodeString tmpVal = VarToStr(DisplayValue).Trim();
    Error = false;
    if (!tmpVal.LastDelimiter("/"))
     {//введён год
      DisplayValue = Variant(tmpVal + "/0/0");
     }
    else
     {
      if (tmpVal.Pos("/") == tmpVal.LastDelimiter("/"))
       {//введён год+"/"+месяцев?
        if (tmpVal.LastDelimiter("/") == tmpVal.Length())
         {//введён год+"/"
          DisplayValue = Variant(tmpVal + "0/0");
         }
        else
         {//введён год+"/"+месяцев
          DisplayValue = Variant(tmpVal + "/0");
         }
       }
      else
       {
        if (tmpVal.LastDelimiter("/") == tmpVal.Length())
         {//введён год+"/"+месяцев+"/"
          DisplayValue = Variant(tmpVal + "0");
         }
        else
         {//странно, но всё должно быть нормально
          Error     = true;
          ErrorText = "Введено некорректное значение возраста.";
         }
       }
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TAddPreTestForm::InfCBPropertiesChange(TObject * Sender)
 {
  CheckCtrl();
 }
//---------------------------------------------------------------------------
