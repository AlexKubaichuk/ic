//---------------------------------------------------------------------------
#ifndef dsICPlanTypeDefH
#define dsICPlanTypeDefH
//---------------------------------------------------------------------------
#include "dsICCommonTypeDef.h"
//---------------------------------------------------------------------------

typedef bool __fastcall (__closure *TdsICPlanGetUnitDataEvent)(__int64 ACode, TkabCustomDataSetRow *& AData);
typedef UnicodeString __fastcall (__closure *TdsCreatePlanEvent)(TJSONObject* ADefValues, UnicodeString &ARecId);
typedef UnicodeString __fastcall (__closure *TdsDeletePlanEvent)(UnicodeString ARecId);
typedef UnicodeString __fastcall (__closure *TdsStartCreatePlanEvent)(TJSONObject* ADefValues);
typedef UnicodeString __fastcall (__closure *TdsStopCreatePlanEvent)(void);
typedef UnicodeString __fastcall (__closure *TdsCheckCreatePlanProgressEvent)(UnicodeString &ARecId);
typedef void __fastcall (__closure *TdsShowSchEvent)(void);
typedef void __fastcall (__closure *TdsPreviewDocumentEvent)(UnicodeString ARecId, bool AByCode);
typedef void __fastcall (__closure *TdsGetPlanPrintFormatsEvent)(TJSONObject *& AData );
typedef UnicodeString __fastcall (__closure *TdsICPlanPrintEvent)(__int64 ARecId);
typedef bool __fastcall (__closure *TdsICPlanCardOpenEvent)(__int64 ARecId, TNotifyEvent AProc);
typedef UnicodeString __fastcall (__closure *TPlanTemplateDefEVent)(int AType);
typedef void __fastcall (__closure *TOnSetPlanOptsEvent)(UnicodeString ARC);
//---------------------------------------------------------------------------
#endif

