﻿//---------------------------------------------------------------------------

#ifndef AddPauseUnitH
#define AddPauseUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
#include "dsICPlanDMUnit.h"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
class TAddPauseForm : public TForm
{
__published:	// IDE-managed Components
 TPanel *BtnPanel;
 TButton *CancelBtn;
 TButton *ApplyBtn;
 TPanel *Panel1;
 TPanel *Panel2;
 TcxComboBox *Type1CB;
 TcxComboBox *ItemsList1CB;
 TcxComboBox *Type2CB;
 TcxComboBox *ItemsList2CB;
 TLabel *Label1;
 TLabel *Label2;
 TPanel *Panel3;
 TLabel *Label3;
 TcxMaskEdit *ValED;
 void __fastcall Type1CBPropertiesChange(TObject *Sender);
 void __fastcall Type2CBPropertiesChange(TObject *Sender);
 void __fastcall ItemsList1CBPropertiesChange(TObject *Sender);
private:	// User declarations
  TdsICPlanDM *FDM;
  void __fastcall CheckCtrl();
  UnicodeString __fastcall FGetType1();
  UnicodeString __fastcall FGetCode1();
  UnicodeString __fastcall FGetType2();
  UnicodeString __fastcall FGetCode2();
  UnicodeString __fastcall FGetValue();
public:		// User declarations
 __fastcall TAddPauseForm(TComponent* Owner, TdsICPlanDM *ADM);
  __property UnicodeString Type1 = {read = FGetType1};
  __property UnicodeString Code1 = {read = FGetCode1};
  __property UnicodeString Type2 = {read = FGetType2};
  __property UnicodeString Code2 = {read = FGetCode2};
  __property UnicodeString Value = {read = FGetValue};

};
//---------------------------------------------------------------------------
extern PACKAGE TAddPauseForm *AddPauseForm;
//---------------------------------------------------------------------------
#endif
