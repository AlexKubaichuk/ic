﻿//---------------------------------------------------------------------------

#ifndef dsIPSPlanSettingH
#define dsIPSPlanSettingH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxGroupBox.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxPC.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinscxPCPainter.hpp"
#include <Vcl.ExtCtrls.hpp>
#include "dsICPlanDMUnit.h"
#include "cxButtons.hpp"
#include <Vcl.Menus.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ImgList.hpp>
#include "cxCheckBox.hpp"
#include "cxClasses.hpp"
#include "cxButtonEdit.hpp"
//---------------------------------------------------------------------------
#include "dsCommSchListUnit.h"
//---------------------------------------------------------------------------
class TIPSPlanSettingForm : public TForm
{
__published:	// IDE-managed Components
 TPanel *BtnPanel;
 TcxPageControl *SettingPC;
 TcxTabSheet *PlanPrintFormatTS;
 TcxGroupBox *cxGroupBox2;
 TcxGroupBox *cxGroupBox1;
 TcxGroupBox *cxGroupBox3;
 TcxTreeList *PlanFormatsTV;
 TcxComboBox *UchDefPlanFormatCB;
 TcxComboBox *OrgDefPlanFormatCB;
 TButton *PlanFormatListEditBtn;
 TButton *CancelBtn;
 TButton *ApplyBtn;
 TcxTreeListColumn *cxTreeList1Column1;
 TcxTreeListColumn *PlanFormatsTVColumn2;
 TcxTabSheet *SchTS;
 TcxButton *TestSch;
 TcxButton *CheckSch;
 TcxButton *VacSch;
 TcxTabSheet *CommSettingTS;
 TActionList *SettingAL;
 TcxImageList *SettingIL;
 TAction *actSchVac;
 TAction *actSchTest;
 TAction *actSchCheck;
 TAction *actPredPostVacTest;
 TcxTabSheet *cxTabSheet1;
 TcxTabSheet *cxTabSheet2;
 TcxTabSheet *cxTabSheet3;
 TcxTabSheet *cxTabSheet4;
 TcxGroupBox *cxGroupBox4;
 TcxTreeList *PauseTV;
 TcxTreeListColumn *cxTreeListColumn7;
 TcxTreeListColumn *cxTreeListColumn8;
 TPanel *Panel1;
 TButton *Button3;
 TButton *Button4;
 TcxTreeListColumn *cxTreeList4Column1;
 TcxGroupBox *cxGroupBox6;
 TcxTreeList *NacPlanTV;
 TcxTreeListColumn *cxTreeListColumn1;
 TcxTreeListColumn *cxTreeListColumn2;
 TcxTreeList *PriorTV;
 TcxTreeListColumn *cxTreeListColumn9;
 TcxTreeListColumn *cxTreeListColumn10;
 TcxGroupBox *cxGroupBox7;
 TcxTreeList *IncompTV;
 TcxTreeListColumn *cxTreeListColumn17;
 TcxTreeListColumn *cxTreeListColumn18;
 TcxTreeListColumn *NacPlanTVColumn1;
 TcxTreeListColumn *NacPlanTVColumn2;
 TcxTreeListColumn *PauseTVColumn1;
 TPanel *Panel3;
 TButton *Button5;
 TButton *Button6;
 TAction *actUpPrior;
 TAction *actDownPrior;
 TcxPageControl *SchPC;
 TcxTabSheet *SchPrivTS;
 TcxTabSheet *SchTestTS;
 TcxTabSheet *SchCheckTS;
 TcxTreeList *PrivSchDefTV;
 TcxTreeListColumn *cxTreeListColumn5;
 TcxTreeListColumn *cxTreeListColumn6;
 TcxTreeList *TestSchDefTV;
 TcxTreeListColumn *cxTreeListColumn11;
 TcxTreeListColumn *cxTreeListColumn15;
 TcxTreeList *CheckSchDefTV;
 TcxTreeListColumn *cxTreeListColumn22;
 TcxTreeListColumn *cxTreeListColumn23;
 TcxStyleRepository *cxStyleRepository1;
 TcxStyle *cxStyle1;
 TcxTreeListColumn *cxTreeList7Column1;
 TAction *actAddPlanInf;
 TAction *actDelPlanInf;
 TAction *actAddPause;
 TAction *actDelPause;
 TAction *actSetVacSch;
 TAction *actSetTestSch;
 TAction *actSetCheckSch;
 TcxTreeListColumn *PrivSchDefTVColumn1;
 TcxTreeListColumn *TestSchDefTVColumn1;
 TcxTreeListColumn *CheckSchDefTVColumn1;
 TcxTreeListColumn *PrivSchDefTVColumn2;
 TcxTreeListColumn *TestSchDefTVColumn2;
 TcxTreeListColumn *CheckSchDefTVColumn2;
 TPanel *Panel4;
 TButton *Button7;
 TButton *Button8;
 TAction *actAddIncomMIBP;
 TAction *actDelIncomMIBP;
 TcxTreeListColumn *NacPlanTVColumn3;
 TcxStyle *SchDefBkg;
 TcxTreeListColumn *PriorTVColumn1;
 TcxTreeListColumn *IncompTVColumn1;
 TcxTreeListColumn *PauseTVColumn2;
 TcxTreeListColumn *PauseTVColumn3;
 TcxTreeListColumn *PauseTVColumn4;
 TcxTreeListColumn *PauseTVColumn5;
 TcxTreeListColumn *PriorTVColumn2;
 TcxGroupBox *PreTestGB;
 TPanel *Panel5;
 TButton *Button9;
 TButton *Button10;
 TcxTreeList *PreTestTL;
 TcxTreeListColumn *cxTreeListColumn14;
 TcxTreeListColumn *cxTreeListColumn16;
 TcxTreeListColumn *cxTreeList1Column2;
 TcxTreeListColumn *cxTreeList1Column3;
 TcxTreeListColumn *cxTreeList1Column4;
 TcxTreeListColumn *cxTreeList1Column5;
 TcxTreeListColumn *cxTreeList1Column6;
 TcxTreeListColumn *cxTreeList1Column7;
 TcxTreeListColumn *cxTreeList1Column8;
 TcxTreeListColumn *cxTreeList1Column9;
 TAction *actAddPreTest;
 TAction *actDelPreTest;
 TcxTreeListColumn *PreTestTLColumn1;
 TPanel *Panel2;
 TButton *Button1;
 TButton *Button2;
 void __fastcall PlanFormatListEditBtnClick(TObject *Sender);
 void __fastcall ApplyBtnClick(TObject *Sender);
 void __fastcall actSchVacExecute(TObject *Sender);
 void __fastcall actSchTestExecute(TObject *Sender);
 void __fastcall actSchCheckExecute(TObject *Sender);
 void __fastcall FormDestroy(TObject *Sender);
 void __fastcall actUpPriorExecute(TObject *Sender);
 void __fastcall actDownPriorExecute(TObject *Sender);
 void __fastcall actAddPlanInfExecute(TObject *Sender);
 void __fastcall actDelPlanInfExecute(TObject *Sender);
 void __fastcall actAddPauseExecute(TObject *Sender);
 void __fastcall actDelPauseExecute(TObject *Sender);
 void __fastcall actSetVacSchExecute(TObject *Sender);
 void __fastcall actSetTestSchExecute(TObject *Sender);
 void __fastcall actSetCheckSchExecute(TObject *Sender);
 void __fastcall actAddIncomMIBPExecute(TObject *Sender);
 void __fastcall actDelIncomMIBPExecute(TObject *Sender);
 void __fastcall PauseTVStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
 void __fastcall PauseTVFocusedNodeChanged(TcxCustomTreeList *Sender, TcxTreeListNode *APrevFocusedNode,
          TcxTreeListNode *AFocusedNode);
 void __fastcall actAddPreTestExecute(TObject *Sender);
 void __fastcall actDelPreTestExecute(TObject *Sender);

private:	// User declarations
  TdsICPlanDM *FDM;
  TTagNode *FPlanOptDef;
  void __fastcall FGetReportList(bool ASet);
  TAnsiStrMap FRepList;
  typedef map<int,UnicodeString> TIntStrMap;
  TIntStrMap FUchDefRep;
  TIntStrMap FOrgDefRep;
  void __fastcall GetPlanOpt();
  void __fastcall SetPlanOpt();
  bool __fastcall CheckInput();
  void __fastcall SaveDefSch(TTagNode *ANode);
  void __fastcall SaveVacPlan(TTagNode *ANode);
  void __fastcall SaveSetPause(TTagNode *ANode, UnicodeString ANodeName, UnicodeString AValue);
  void __fastcall SavePause(TTagNode *ANode);
  void __fastcall SaveIncompMIBP(TTagNode *ANode);
  void __fastcall SavePreTest(TTagNode * ANode);

  void __fastcall FillCommPlan(TTagNode *APlanNode);
  void __fastcall FillDefSch(TTagNode *APlanNode);
  void __fastcall FillPause(TTagNode *APlanNode);
  void __fastcall FillVacPrior(TTagNode *APlanNode);
  void __fastcall FillIncomMIBP(TTagNode *APlanNode);
  void __fastcall FillPreTest(TTagNode * APlanNode);

  UnicodeString __fastcall PreTestPlanTypeStr(UnicodeString ACode);
  bool __fastcall GetSchData(TTagNodeMap *ASchList, TGetItemNameEvent AGetItemName, UnicodeString &AVal);
  UnicodeString __fastcall GetVacName(UnicodeString Code);
  UnicodeString __fastcall GetTestName(UnicodeString Code);
  UnicodeString __fastcall GetCheckName(UnicodeString Code);
public:		// User declarations
 __fastcall TIPSPlanSettingForm(TComponent* Owner, TdsICPlanDM *ADM);
};
//---------------------------------------------------------------------------
extern PACKAGE TIPSPlanSettingForm *IPSPlanSettingForm;
//---------------------------------------------------------------------------
#endif
