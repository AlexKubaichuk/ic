﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsCommSchListUnit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxCustomData"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma resource "*.dfm"
TCommSchListForm * CommSchListForm;
// ---------------------------------------------------------------------------
__fastcall TCommSchListForm::TCommSchListForm(TComponent * Owner, TTagNodeMap * ASchList,
  TGetItemNameEvent AGetItemName, UnicodeString AVal, bool ASelectSchOnly) : TForm(Owner)
 {
  FSelectSchOnly = ASelectSchOnly;
  TcxTreeListNode * itSchTreeNode;
  TcxTreeListNode * itLineTreeNode;
  TcxTreeListNode * FCurSel = NULL;
  TTagNode * itLine;
  FOnGetItemName = AGetItemName;
  SchemaTL->Clear();
  itSchTreeNode           = SchemaTL->Root->AddChild();
  itSchTreeNode->Data     = (void *)NULL;
  itSchTreeNode->Texts[0] = " Не определено";
  if (AVal == "_NULL_")
   FCurSel = itSchTreeNode;
  for (TTagNodeMap::iterator i = ASchList->begin(); i != ASchList->end(); i++)
   {
    itSchTreeNode           = SchemaTL->Root->AddChild();
    itSchTreeNode->Data     = (void *)(i->second);
    itSchTreeNode->Texts[0] = GetItemName(i->second->AV["ref"]);
    itSchTreeNode->Texts[0] = itSchTreeNode->Texts[0] + " схема '" + i->second->AV["name"] + "'";
    itLine                  = i->second->GetFirstChild();
    while (itLine)
     {
      if (itLine->CmpName("line"))
       {
        itLineTreeNode           = itSchTreeNode->AddChild();
        itLineTreeNode->Data     = (void *)itLine;
        itLineTreeNode->Texts[0] = ((itLine->AV["name"][1] == 'V') ? " " : "") + itLine->AV["name"];
        if (!FSelectSchOnly)
         {
          if (AVal.UpperCase() == (i->second->AV["uid"] + "." + itLine->AV["uid"]).UpperCase())
           FCurSel = itLineTreeNode;
         }
       }
      else if (itLine->CmpName("begline"))
       {
        itLineTreeNode           = itSchTreeNode->AddChild();
        itLineTreeNode->Data     = (void *)itLine;
        itLineTreeNode->Texts[0] = "Начало схемы";
        if (!FSelectSchOnly)
         {
          if (AVal == i->second->AV["uid"] + "." + itLine->AV["uid"])
           FCurSel = itLineTreeNode;
         }
       }
      itLine = itLine->GetNext();
     }
    if (FSelectSchOnly)
     {
      if (AVal.UpperCase() == i->second->AV["uid"].UpperCase())
       FCurSel = itSchTreeNode;
     }
   }
  if (FCurSel)
   {
    FCurSel->Focused = true;
    FCurSel->MakeVisible();
   }
  else
   SchemaTL->Items[0]->Focused = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TCommSchListForm::OkBtnClick(TObject * Sender)
 {
  if (SchemaTL->SelectionCount)
   {
    TTagNode * tmpNode;
    if (!SchemaTL->Selections[0]->Data)
     {
      FSchCodeValue = "_NULL_";
      FSchNameValue = "-";
     }
    else
     {
      if (FSelectSchOnly)
       {
        if (SchemaTL->Selections[0]->Level)
         return;
        tmpNode       = (TTagNode *)SchemaTL->Selections[0]->Data;
        FSchCodeValue = tmpNode->AV["uid"];
        FSchNameValue = SchemaTL->Selections[0]->Texts[0];
       }
      else
       {
        if (!SchemaTL->Selections[0]->Level)
         return;
        tmpNode       = (TTagNode *)SchemaTL->Selections[0]->Data;
        FSchCodeValue = tmpNode->GetParent()->AV["uid"] + "." + tmpNode->AV["uid"];
        if (tmpNode->CmpName("line"))
         FSchNameValue = "{" + tmpNode->GetParent()->AV["name"] + "." + tmpNode->AV["name"] + "} " +
           GetItemName(tmpNode->GetParent()->AV["ref"]);
        else if (tmpNode->CmpName("begline"))
         FSchNameValue = "{" + tmpNode->GetParent()->AV["name"] + ".Начало схемы} " +
           GetItemName(tmpNode->GetParent()->AV["ref"]);
       }
     }
    ModalResult = mrOk;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TCommSchListForm::SchemaTLStylesGetContentStyle(TcxCustomTreeList * Sender, TcxTreeListColumn * AItem,
  TcxTreeListNode * ANode, TcxStyle *& AStyle)
 {
  if (ANode)
   {
    if (!FSelectSchOnly)
     {
      if ((ANode->Level == 1) || !ANode->Data)
       AStyle = BoldStyle;
      else
       AStyle = NoBoldStyle;
     }
    else
     {
      if ((ANode->Level != 1) || !ANode->Data)
       AStyle = BoldStyle;
      else
       AStyle = NoBoldStyle;
     }
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TCommSchListForm::GetItemName(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetItemName)
     RC = FOnGetItemName(ACode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
