﻿//---------------------------------------------------------------------------
#ifndef dsCommSchListUnitH
#define dsCommSchListUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxClasses.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
//---------------------------------------------------------------------------
typedef UnicodeString __fastcall (__closure *TGetItemNameEvent)(UnicodeString ACode);
class PACKAGE TCommSchListForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TcxTreeList *SchemaTL;
        TcxTreeListColumn *SchCol;
 TcxStyleRepository *Style;
        TcxStyle *BoldStyle;
        TcxStyle *NoBoldStyle;
        TcxStyle *cxStyle1;
        void __fastcall OkBtnClick(TObject *Sender);
  void __fastcall SchemaTLStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AItem, TcxTreeListNode *ANode, TcxStyle *&AStyle);
private:	// User declarations
//    TStringList *xValues;
  UnicodeString FSchCodeValue, FSchNameValue;
    bool FSelectSchOnly, FVac;
    TGetItemNameEvent  FOnGetItemName;
    UnicodeString __fastcall GetItemName(UnicodeString ACode);
public:		// User declarations
  __property UnicodeString Schema = {read = FSchCodeValue};
  __property UnicodeString SchemaName = {read = FSchNameValue};
    __fastcall TCommSchListForm(TComponent* Owner, TTagNodeMap *ASchList, TGetItemNameEvent AGetItemName, UnicodeString AVal, bool ASelectSchOnly = false);
};
//---------------------------------------------------------------------------
extern PACKAGE TCommSchListForm *CommSchListForm;
//---------------------------------------------------------------------------
#endif
