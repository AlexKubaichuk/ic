﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AddIncompMIBPUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TAddIncompMIBPForm *AddIncompMIBPForm;
//---------------------------------------------------------------------------
__fastcall TAddIncompMIBPForm::TAddIncompMIBPForm(TComponent* Owner, TdsICPlanDM *ADM)
 : TForm(Owner)
{
  FDM = ADM;
  TStringList *FVacList = new TStringList;
  try
   {
     Vac1CB->Properties->Items->Clear();
     Vac2CB->Properties->Items->Clear();
     FDM->LoadVacList(FVacList);
     Vac2CB->Properties->Items->AddObject("Все", 0);
     FVacList->Sort();
     for (int i = 0; i < FVacList->Count; i++)
      {
        Vac1CB->Properties->Items->AddObject(FVacList->Strings[i], FVacList->Objects[i]);
        Vac2CB->Properties->Items->AddObject(FVacList->Strings[i], FVacList->Objects[i]);
      }
   }
  __finally
   {
     delete FVacList;
   }
}
//---------------------------------------------------------------------------
void __fastcall TAddIncompMIBPForm::Vac1CBPropertiesChange(TObject *Sender)
{
  ApplyBtn->Enabled = (Vac1CB->ItemIndex != -1)&&(Vac2CB->ItemIndex != -1);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddIncompMIBPForm::FGetCode1()
{
  UnicodeString RC = "";
  if (Vac1CB->ItemIndex != -1)
   {
     RC = IntToStr((int)Vac1CB->Properties->Items->Objects[Vac1CB->ItemIndex]);
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TAddIncompMIBPForm::FGetCode2()
{
  UnicodeString RC = "";
  if (Vac2CB->ItemIndex != -1)
   {
     RC = IntToStr((int)Vac2CB->Properties->Items->Objects[Vac2CB->ItemIndex]);
   }
  return RC;
}
//---------------------------------------------------------------------------

