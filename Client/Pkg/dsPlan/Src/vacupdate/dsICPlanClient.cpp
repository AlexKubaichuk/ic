﻿//---------------------------------------------------------------------------

#include <basepch.h>

#pragma hdrstop


//#include "DxLocale.h"

#include "dsICPlanClient.h"
#include "dsICPlanDMUnit.h"
//#include "ICSRegList.h"
//#include "ICSRegSelList.h"
//#include "Reg_DMF.h"
//#include "ICSRegProgress.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TdsICPlanClient *)
{
  new TdsICPlanClient(NULL);
}
//---------------------------------------------------------------------------
__fastcall TdsICPlanClient::TdsICPlanClient(TComponent* Owner)
        : TComponent(Owner)
{
  FUseClassDisControls = false;
  FUseReportSetting = false;
  FDBUser = "";
  FDBPassword ="";
  FDBCharset = "";
  RegistryKey = "Software\\ICS Ltd.\\ClassifSize";
  FUnitListCreated = NULL;
  FBuyCaption = "";
  FUnitListDeleted = NULL;
//  FOnCtrlDataChange = NULL;
//  FPlanForm = NULL;

  FRefreshList = false;
  FClassTreeColor = clBtnFace;
  FDTFormat = "dd.mm.yyyy";
  FDM = new TdsICPlanDM(this);
  FActive = true;
  FClassEnabled = true;

}
//---------------------------------------------------------------------------
__fastcall TdsICPlanClient::~TdsICPlanClient()
{
  Active = false;
  delete FDM;
//  if (FPlanForm) delete FPlanForm;
//  FPlanForm = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetFullEdit(bool AFullEdit)
{
  FFullEdit = AFullEdit;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetActive(bool AActive)
{
 FActive = AActive;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetClassEnabled(bool AClassEnabled)
{
/*
  FClassEnabled = AClassEnabled;
  if (FDM)
   if (FDM->UnitList) ((TICSRegListForm*)FDM->UnitList)->CheckAccess();
*/
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetListEditEnabled(bool AListEditEnabled)
{
/*
  FListEditEnabled = AListEditEnabled;
  if (FDM)
   if (FDM->UnitList) ((TICSRegListForm*)FDM->UnitList)->CheckAccess();
*/
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOpenPlanEnabled(bool AOpenPlanEnabled)
{
/*
  FOpenPlanEnabled = AOpenPlanEnabled;
  if (FDM)
   if (FDM->UnitList) ((TICSRegListForm*)FDM->UnitList)->CheckAccess();
*/
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetUListTop(int ATop)
{
  FUListTop = ATop;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetUListLeft(int ALeft)
{
  FUListLeft = ALeft;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetDBName(UnicodeString  ADBName)
{
  FDBName = ADBName;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetXMLName(UnicodeString  AXMLName)
{
  FXMLName = AXMLName;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::ShowPlan()
{
  if (!FActive) return;
  if (!FClassEnabled) return;
//  if (!FPlanForm)
//    FPlanForm = new TdsPlanClientForm(Application->Owner, FDM);
//  FPlanForm->Show();
}
//---------------------------------------------------------------------------
void  __fastcall TdsICPlanClient::FSetInqComp(TControl *AInqComp)
{
  FInqComp  = AInqComp;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::GetTreeChoice(TList *AList)
{
  tmpChTree = new TList;
  FDM->RegDef->Iterate(GetTreeCh);
  AList->Assign(tmpChTree,laCopy,NULL);
  if (tmpChTree) delete tmpChTree;
  tmpChTree = NULL;
}
//---------------------------------------------------------------------------
bool __fastcall TdsICPlanClient::GetTreeCh(TTagNode *itTag)
{
   if (itTag->CmpName("choiceTree"))
    tmpChTree->Add(itTag);
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetInit(int APersent, UnicodeString AMess)
{
/*
  if (APersent == 0) ((TICSRegProgressForm*)PrF)->SetCaption(AMess);
  ((TICSRegProgressForm*)PrF)->SetPercent(APersent);
*/
}
//---------------------------------------------------------------------------
/*
TdxBar*  __fastcall TdsICPlanClient::GetListExtTB(void)
{
  if (FDM->UnitList) return ((TICSRegListForm*)FDM->UnitList)->GetExtTB();
  else              return NULL;
}
//---------------------------------------------------------------------------
*/
void __fastcall TdsICPlanClient::FSetDTFormat(UnicodeString  AParam)
{
  FDTFormat = AParam;
}
//---------------------------------------------------------------------------
bool __fastcall TdsICPlanClient::FGetPlanEditable()
{
  bool RC = false;
  try
   {
     if (FDM)
      RC = FDM->PlanEditable;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetPlanEditable(bool AVal)
{
  if (FDM)
    FDM->PlanEditable = AVal;
}
//---------------------------------------------------------------------------
UnicodeString  __fastcall TdsICPlanClient::FGetClassShowExpanded()
{
  UnicodeString RC = "";
  try
   {
     if (FDM)
      RC = FDM->ClassShowExpanded;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetClassShowExpanded(UnicodeString AVal)
{
  if (FDM)
    FDM->ClassShowExpanded = AVal;
}
//---------------------------------------------------------------------------
TExtEditData __fastcall TdsICPlanClient::FGetExtInsertData()
{

  if (FDM)
   return FDM->OnExtInsertData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetExtInsertData(TExtEditData AValue)
{
  if (FDM)
   FDM->OnExtInsertData = AValue;
}
//---------------------------------------------------------------------------
TExtEditData __fastcall TdsICPlanClient::FGetExtEditData()
{

  if (FDM)
   return FDM->OnExtEditData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetExtEditData(TExtEditData AValue)
{
  if (FDM)
   FDM->OnExtEditData = AValue;
}
//---------------------------------------------------------------------------
TClassEditEvent __fastcall TdsICPlanClient::FGetClassAfterInsert()
{
  if (FDM)
   return FDM->OnClassAfterInsert;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetClassAfterInsert(TClassEditEvent AValue)
{
  if (FDM)
   FDM->OnClassAfterInsert = AValue;
}
//---------------------------------------------------------------------------
TClassEditEvent __fastcall TdsICPlanClient::FGetClassAfterEdit()
{
  if (FDM)
   return FDM->OnClassAfterEdit;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetClassAfterEdit(TClassEditEvent AValue)
{
  if (FDM)
   FDM->OnClassAfterEdit = AValue;
}
//---------------------------------------------------------------------------
TClassEditEvent __fastcall TdsICPlanClient::FGetClassAfterDelete()
{
  if (FDM)
   return FDM->OnClassAfterDelete;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetClassAfterDelete(TClassEditEvent AValue)
{
  if (FDM)
   FDM->OnClassAfterDelete = AValue;
}
//---------------------------------------------------------------------------
TUnitEvent __fastcall TdsICPlanClient::FGetOnOpenPlan()
{
  if (FDM)
   return FDM->OnOpenPlan;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnOpenPlan(TUnitEvent AValue)
{
  if (FDM)
   FDM->OnOpenPlan = AValue;
}
//---------------------------------------------------------------------------
TGetReportListEvent __fastcall TdsICPlanClient::FGetQuickFilterList()
{
  if (FDM)
   return FDM->OnQuickFilterList;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetQuickFilterList(TGetReportListEvent AValue)
{
  if (FDM)
   FDM->OnQuickFilterList = AValue;
}
//---------------------------------------------------------------------------
TGetReportListEvent __fastcall TdsICPlanClient::FGetReportList()
{
  if (FDM)
   return FDM->OnReportList;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetReportList(TGetReportListEvent AValue)
{
  if (FDM)
   FDM->OnReportList = AValue;
}
//---------------------------------------------------------------------------
TUnitEvent __fastcall TdsICPlanClient::FGetPrintReportClick()
{
  if (FDM)
   return FDM->OnPrintReportClick;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetPrintReportClick(TUnitEvent AValue)
{
  if (FDM)
   FDM->OnPrintReportClick = AValue;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TdsICPlanClient::FGetRegDef()
{
  if (FDM)
   return FDM->RegDef;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetRegDef(TTagNode* AValue)
{
  if (FDM)
   FDM->RegDef = AValue;
}
//---------------------------------------------------------------------------
TExtBtnClick __fastcall TdsICPlanClient::FGetExtBtnClick()
{
  if (FDM)
   return FDM->OnExtBtnClick;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetExtBtnClick(TExtBtnClick AValue)
{
  if (FDM)
   FDM->OnExtBtnClick = AValue;
}
//---------------------------------------------------------------------------
TcxEditStyleController* __fastcall TdsICPlanClient::FGetStyleController()
{
  if (FDM)
   return FDM->StyleController;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetStyleController(TcxEditStyleController* AValue)
{
  if (FDM)
   FDM->StyleController = AValue;
}
//---------------------------------------------------------------------------
TdsGetClassXMLEvent __fastcall TdsICPlanClient::FGetOnGetClassXML()
{
  if (FDM)
   return FDM->OnGetClassXML;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetClassXML(TdsGetClassXMLEvent AValue)
{
  if (FDM)
   FDM->OnGetClassXML = AValue;
}
//---------------------------------------------------------------------------
TdsGetCountEvent __fastcall TdsICPlanClient::FGetOnGetCount()
{
  if (FDM)
   return FDM->OnGetCount;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetCount(TdsGetCountEvent AValue)
{
  if (FDM)
   FDM->OnGetCount = AValue;
}
//---------------------------------------------------------------------------
TdsGetIdListEvent __fastcall TdsICPlanClient::FGetOnGetIdList()
{
  if (FDM)
   return FDM->OnGetIdList;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetIdList(TdsGetIdListEvent AValue)
{
  if (FDM)
   FDM->OnGetIdList = AValue;
}
//---------------------------------------------------------------------------
TdsGetValByIdEvent __fastcall TdsICPlanClient::FGetOnGetValById()
{
  if (FDM)
   return FDM->OnGetValById;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetValById(TdsGetValByIdEvent AValue)
{
  if (FDM)
   FDM->OnGetValById = AValue;
}
//---------------------------------------------------------------------------
TdsGetValById10Event __fastcall TdsICPlanClient::FGetOnGetValById10()
{
  if (FDM)
   return FDM->OnGetValById10;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetValById10(TdsGetValById10Event AValue)
{
  if (FDM)
   FDM->OnGetValById10 = AValue;
}
//---------------------------------------------------------------------------
TdsFindEvent __fastcall TdsICPlanClient::FGetOnFind()
{
  if (FDM)
   return FDM->OnFind;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnFind(TdsFindEvent AValue)
{
  if (FDM)
   FDM->OnFind = AValue;
}
//---------------------------------------------------------------------------
TdsInsertDataEvent __fastcall TdsICPlanClient::FGetOnInsertData()
{
  if (FDM)
   return FDM->OnInsertData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnInsertData(TdsInsertDataEvent AValue)
{
  if (FDM)
   FDM->OnInsertData = AValue;
}
//---------------------------------------------------------------------------
TdsEditDataEvent __fastcall TdsICPlanClient::FGetOnEditData()
{
  if (FDM)
   return FDM->OnEditData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnEditData(TdsEditDataEvent AValue)
{
  if (FDM)
   FDM->OnEditData = AValue;
}
//---------------------------------------------------------------------------
TdsDeleteDataEvent __fastcall TdsICPlanClient::FGetOnDeleteData()
{
  if (FDM)
   return FDM->OnDeleteData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnDeleteData(TdsDeleteDataEvent AValue)
{
  if (FDM)
   FDM->OnDeleteData = AValue;
}
//---------------------------------------------------------------------------
TAxeXMLContainer* __fastcall TdsICPlanClient::FGetXMLList()
{
  if (FDM)
   return FDM->XMLList;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetXMLList(TAxeXMLContainer *AVal)
{
  if (FDM)
   FDM->XMLList = AVal;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::PreloadData()
{
  if (FDM)
    FDM->PreloadData();
}
//---------------------------------------------------------------------------
TdsICPlanGetUnitDataEvent __fastcall TdsICPlanClient::FGetOnGetUnitData()
{
  if (FDM)
   return FDM->OnGetUnitData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetUnitData(TdsICPlanGetUnitDataEvent AValue)
{
  if (FDM)
   FDM->OnGetUnitData = AValue;
}
//---------------------------------------------------------------------------
TdsDeletePlanEvent __fastcall TdsICPlanClient::FGetOnDeletePlan()
{
  if (FDM)
   return FDM->OnDeletePlan;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnDeletePlan(TdsDeletePlanEvent AValue)
{
  if (FDM)
   FDM->OnDeletePlan = AValue;
}
//---------------------------------------------------------------------------
TdsStartCreatePlanEvent __fastcall TdsICPlanClient::FGetOnStartCreatePlan()
{
  if (FDM)
   return FDM->OnStartCreatePlan;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnStartCreatePlan(TdsStartCreatePlanEvent AValue)
{
  if (FDM)
   FDM->OnStartCreatePlan = AValue;
}
//---------------------------------------------------------------------------
TdsStopCreatePlanEvent __fastcall TdsICPlanClient::FGetOnStopCreatePlan()
{
  if (FDM)
   return FDM->OnStopCreatePlan;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnStopCreatePlan(TdsStopCreatePlanEvent AValue)
{
  if (FDM)
   FDM->OnStopCreatePlan = AValue;
}
//---------------------------------------------------------------------------
TdsICPlanCardOpenEvent __fastcall TdsICPlanClient::FGetOnCardOpen()
{
  if (FDM)
   return FDM->OnCardOpen;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnCardOpen(TdsICPlanCardOpenEvent AValue)
{
  if (FDM)
   FDM->OnCardOpen = AValue;
}
//---------------------------------------------------------------------------
TdsPreviewDocumentEvent __fastcall TdsICPlanClient::FGetOnPreviewDoc()
{
  if (FDM)
   return FDM->OnPreviewDoc;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnPreviewDoc(TdsPreviewDocumentEvent AValue)
{
  if (FDM)
   FDM->OnPreviewDoc = AValue;
}
//---------------------------------------------------------------------------
TdsCheckCreatePlanProgressEvent __fastcall TdsICPlanClient::FGetOnCheckCreatePlanProgress()
{
  if (FDM)
   return FDM->OnCheckCreatePlanProgress;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnCheckCreatePlanProgress(TdsCheckCreatePlanProgressEvent AValue)
{
  if (FDM)
   FDM->OnCheckCreatePlanProgress = AValue;
}
//---------------------------------------------------------------------------
TdsGetPlanPrintFormatsEvent __fastcall TdsICPlanClient::FGetOnGetPlanPrintFormats()
{
  if (FDM)
   return FDM->OnGetPlanPrintFormats;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetPlanPrintFormats(TdsGetPlanPrintFormatsEvent AValue)
{
  if (FDM)
   FDM->OnGetPlanPrintFormats = AValue;
}
//---------------------------------------------------------------------------
TdsICPlanPrintEvent __fastcall TdsICPlanClient::FGetOnPlanPrint()
{
  if (FDM)
   return FDM->OnPlanPrint;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnPlanPrint(TdsICPlanPrintEvent AValue)
{
  if (FDM)
   FDM->OnPlanPrint = AValue;
}
//---------------------------------------------------------------------------
__int64 __fastcall TdsICPlanClient::FGetLPUCode()
{
  if (FDM)
   return FDM->LPUCode;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetLPUCode(__int64 AValue)
{
  if (FDM)
   FDM->LPUCode = AValue;
}
//---------------------------------------------------------------------------
TPlanTemplateDefEVent __fastcall TdsICPlanClient::FGetOnGetPlanTemplateDef()
{
  if (FDM)
   return FDM->OnGetPlanTemplateDef;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetPlanTemplateDef(TPlanTemplateDefEVent AValue)
{
  if (FDM)
   FDM->OnGetPlanTemplateDef = AValue;
}
//---------------------------------------------------------------------------
TAppOptions* __fastcall TdsICPlanClient::FGetAppOpt()
{
  if (FDM)
   return FDM->AppOpt;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetAppOpt(TAppOptions* AValue)
{
  if (FDM)
   FDM->AppOpt = AValue;
}
//---------------------------------------------------------------------------
TGetReportListEvent __fastcall TdsICPlanClient::FGetOnGetReportList()
{
  if (FDM)
   return FDM->OnGetReportList;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetReportList(TGetReportListEvent AValue)
{
  if (FDM)
   FDM->OnGetReportList = AValue;
}
//---------------------------------------------------------------------------
TdsPlanCardInsertDataEvent __fastcall TdsICPlanClient::FGetOnPlanTestInsert()
{
  if (FDM)
   return FDM->OnPlanTestInsert;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnPlanTestInsert(TdsPlanCardInsertDataEvent AValue)
{
  if (FDM)
   FDM->OnPlanTestInsert = AValue;
}
//---------------------------------------------------------------------------
TdsPlanCardInsertDataEvent __fastcall TdsICPlanClient::FGetOnPlanCheckInsert()
{
  if (FDM)
   return FDM->OnPlanCheckInsert;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnPlanCheckInsert(TdsPlanCardInsertDataEvent AValue)
{
  if (FDM)
   FDM->OnPlanCheckInsert = AValue;
}
//---------------------------------------------------------------------------
TdsShowSchEvent __fastcall TdsICPlanClient::FGetOnShowVacSch()
{
  if (FDM)
   return FDM->OnShowVacSch;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnShowVacSch(TdsShowSchEvent AValue)
{
  if (FDM)
   FDM->OnShowVacSch = AValue;
}
//---------------------------------------------------------------------------
TdsShowSchEvent __fastcall TdsICPlanClient::FGetOnShowTestSch()
{
  if (FDM)
   return FDM->OnShowTestSch;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnShowTestSch(TdsShowSchEvent AValue)
{
  if (FDM)
   FDM->OnShowTestSch = AValue;
}
//---------------------------------------------------------------------------
TdsShowSchEvent __fastcall TdsICPlanClient::FGetOnShowCheckSch()
{
  if (FDM)
   return FDM->OnShowCheckSch;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnShowCheckSch(TdsShowSchEvent AValue)
{
  if (FDM)
   FDM->OnShowCheckSch = AValue;
}
//---------------------------------------------------------------------------
//UnicodeString __fastcall TdsICPlanClient::FGetXMLPath()
//{
//  if (FDM)
//   return FDM->XMLPath;
//  else
//   return "";
//}
////---------------------------------------------------------------------------
//void __fastcall TdsICPlanClient::FSetXMLPath(UnicodeString AVal)
//{
//  if (FDM)
//   FDM->XMLPath = AVal;
//}
////---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanClient::FGetDataPath()
{
  if (FDM)
   return FDM->DataPath;
  else
   return "";
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetDataPath(UnicodeString AVal)
{
  if (FDM)
   FDM->DataPath = AVal;
}
//---------------------------------------------------------------------------
TNotifyEvent __fastcall TdsICPlanClient::FGetOnPrePostTestList()
{
  if (FDM)
   return FDM->OnPrePostTestList;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSeOnPrePostTestList(TNotifyEvent AValue)
{
  if (FDM)
   FDM->OnPrePostTestList = AValue;
}
//---------------------------------------------------------------------------
TOnGetPlanOptsEvent __fastcall TdsICPlanClient::FGetOnGetPlanOpts()
{
  if (FDM)
   return FDM->OnGetPlanOpts;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetPlanOpts(TOnGetPlanOptsEvent AValue)
{
  if (FDM)
   FDM->OnGetPlanOpts = AValue;
}
//---------------------------------------------------------------------------
TOnSetPlanOptsEvent __fastcall TdsICPlanClient::FGetOnSetPlanOpts()
{
  if (FDM)
   return FDM->OnSetPlanOpts;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnSetPlanOpts(TOnSetPlanOptsEvent AValue)
{
  if (FDM)
   FDM->OnSetPlanOpts = AValue;
}
//---------------------------------------------------------------------------
TdsGetListByIdEvent __fastcall TdsICPlanClient::FGetOnGetListById()
{
  if (FDM)
   return FDM->OnGetListById;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetListById(TdsGetListByIdEvent AVal)
{
  if (FDM)
   FDM->OnGetListById = AVal;
}
//---------------------------------------------------------------------------
TColor __fastcall TdsICPlanClient::FGetReqColor()
{
  if (FDM)
   return FDM->RequiredColor;
  else
   return clInfoBk;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetReqColor(TColor AValue)
{
  if (FDM)
   FDM->RequiredColor = AValue;
}
//---------------------------------------------------------------------------
TExtValidateData __fastcall TdsICPlanClient::FGetOnExtValidate()
{
  if (FDM)
   return FDM->OnExtValidate;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnExtValidate(TExtValidateData AVal)
{
  if (FDM)
   FDM->OnExtValidate = AVal;
}
//---------------------------------------------------------------------------
TExtValidateData __fastcall TdsICPlanClient::FGetOnGetDefValues()
{
  if (FDM)
   return FDM->OnGetDefValues;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetDefValues(TExtValidateData AVal)
{
  if (FDM)
   FDM->OnGetDefValues = AVal;
}
//---------------------------------------------------------------------------
TdsPlanGetExtDataEvent __fastcall TdsICPlanClient::FGetOnGetAddrStr()
{
  if (FDM)
   return FDM->OnGetAddrStr;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetMKBStr(TdsPlanGetExtDataEvent AVal)
{
  if (FDM)
   FDM->OnGetMKBStr = AVal;
}
//---------------------------------------------------------------------------
TdsPlanGetExtDataEvent __fastcall TdsICPlanClient::FGetOnGetMKBStr()
{
  if (FDM)
   return FDM->OnGetMKBStr;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetAddrStr(TdsPlanGetExtDataEvent AVal)
{
  if (FDM)
   FDM->OnGetAddrStr = AVal;
}
//---------------------------------------------------------------------------
TdsPlanGetExtDataEvent __fastcall TdsICPlanClient::FGetOnGetOrgStr()
{
  if (FDM)
   return FDM->OnGetOrgStr;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetOrgStr(TdsPlanGetExtDataEvent AVal)
{
  if (FDM)
   FDM->OnGetOrgStr = AVal;
}
//---------------------------------------------------------------------------
TdsPlanGetExtDataEvent __fastcall TdsICPlanClient::FGetOnGetUchStr()
{
  if (FDM)
   return FDM->OnGetUchStr;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetUchStr(TdsPlanGetExtDataEvent AVal)
{
  if (FDM)
   FDM->OnGetUchStr = AVal;
}
//---------------------------------------------------------------------------
TdsGetF63Event __fastcall TdsICPlanClient::FGetOnGetF63()
{
  if (FDM)
   return FDM->OnGetF63;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetF63(TdsGetF63Event AValue)
{
  if (FDM)
   FDM->OnGetF63 = AValue;
}
//---------------------------------------------------------------------------
TdsGetVacDefDozeEvent __fastcall TdsICPlanClient::FGetOnGetVacDefDoze()
{
  if (FDM)
   return FDM->OnGetVacDefDoze;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsICPlanClient::FSetOnGetVacDefDoze(TdsGetVacDefDozeEvent AValue)
{
  if (FDM)
   FDM->OnGetVacDefDoze = AValue;
}
//---------------------------------------------------------------------------

