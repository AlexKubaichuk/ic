﻿//---------------------------------------------------------------------------
#ifndef dsICPlanCreatePlanUnitH
#define dsICPlanCreatePlanUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Controls.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxGroupBox.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxRadioGroup.hpp"
#include "cxSpinEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
#include "KabCustomDS.h"
#include "dsICPlanDMUnit.h"
#include "dsRegTemplate.h"
//---------------------------------------------------------------------------
/*
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.CheckLst.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "cxCheckListBox.hpp"
#include "cxSpinEdit.hpp"
#include "cxGroupBox.hpp"
#include "cxRadioGroup.hpp"
#include "cxSplitter.hpp"
#include <Vcl.Forms.hpp>
*/
class PACKAGE TdsCreatePlanForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
 TActionList *PlanListCreateAL;
 TAction *actSelAll;
 TAction *actResetAll;
 TAction *actApply;
 TcxGroupBox *PlanParamGB;
 TPanel *CompPanel;
 TPanel *PlanPrintPanel;
 TLabel *Label2;
 TcxComboBox *PlanPrintFormatCB;
 TcxGroupBox *BrigPlanOptGB;
 TLabel *BrigPlanPeriodLab1;
 TLabel *BrigPlanPeriodLab2;
 TcxSpinEdit *BrigPlanPeriod;
 TLabel *MsgLab;
 TcxRadioGroup *PlanKindRG;
 TcxGroupBox *PlanTypeGB;
 TcxComboBox *OrgPlanTypeCB;
 TPanel *Panel1;
 TcxComboBox *SupportTypeCB;
 TLabel *SupportTypeLab;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
 void __fastcall PlanKindRGPropertiesChange(TObject *Sender);
 void __fastcall CancelBtnClick(TObject *Sender);
 void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
 void __fastcall OrgPlanTypeCBPropertiesChange(TObject *Sender);
private:	// User declarations
  typedef map<int, UnicodeString> TIntStrMap;
  bool FIsSubSel, IsAll, FCh;
  TTagNode *FTempXML;
  TTagNode *FRootNode;
  TdsICPlanDM *FDM;
  TTagNode *TmplNode;
  TdsRegTemplate *FTempl;
  TIntStrMap  FPlanPrintFormats;
  int FDefUchPFIdx, FDefOrgPFIdx;
  TDate FPlpanMonth;
  __int64 FDefUchOrgCode;

  UnicodeString FDefLPUCode;
    TkabCustomDataSource *FDataSrc;
    TStringList *FEditInfList;
    bool     FIsAppend, FOneCol;
//    TTagNode *Root;
//    TTagNode *FClassDef,*FClassDesc;
    int      ColWidth, SubLevel;
    UnicodeString  FInsertedRecId;
  TDate FPatBirthday;
    TdsRegEDItem   *LastLeftCtrl;
    TdsRegEDItem   *LastCtrl;
    TcxPageControl *Group1st;
    TcxTabSheet    *gPage;

    TdsRegEDContainer *FCtrList;
    int FLastTop,FLastHeight;

  int __fastcall GetSupportType();
  bool __fastcall GetFl(TTagNode *itTag, UnicodeString &UIDs);
  void __fastcall PlanKindChange();

    bool __fastcall GetInput(TTagNode *uiTag, UnicodeString &UID);
    bool __fastcall CheckInput();
//    bool __fastcall FCtrlDataChange(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource);
public:
    __property UnicodeString  InsertedRecId = {read=FInsertedRecId};
    __fastcall TdsCreatePlanForm(TComponent* Owner, TdsICPlanDM *ADM, TTagNode* ARoot, TkabCustomDataSource *ADataSrc, TDate ADefPlanMonth, int ADefType = 0, int ADefSubType = 0, __int64 ADefUchOrgCode = 0, UnicodeString ADefDopCode = "");

  int __fastcall GetPlanType();
  TStringList *ValuesList;
  TTagNode *Result;
};
//---------------------------------------------------------------------------
extern PACKAGE TdsCreatePlanForm *dsCreatePlanForm;
//---------------------------------------------------------------------------
#endif
