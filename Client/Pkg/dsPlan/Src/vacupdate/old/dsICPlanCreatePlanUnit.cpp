﻿//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsICPlanCreatePlanUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxGroupBox"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxRadioGroup"
#pragma link "cxSpinEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
#define _Type_       "3043"
#define _Inf_        "3083"
#define _Prich_      "3066"
#define _MKB_        "3082"
#define _BegDate_    "305F"
#define _EndDate_    "3065"
#define _PrivCancel_ "3110"
#define _TestCancel_ "3111"
//---------------------------------------------------------------------------
#define _EDLEFT  2
#define _EDLEFT2 232

TdsCreatePlanForm * dsCreatePlanForm;
//---------------------------------------------------------------------------
__fastcall TdsCreatePlanForm::TdsCreatePlanForm(TComponent * Owner, TdsICPlanDM * ADM, TTagNode * ARoot, TkabCustomDataSource * ADataSrc, TDate ADefPlanMonth, int ADefType, int ADefSubType,
    __int64 ADefUchOrgCode, UnicodeString  ADefDopCode)
    : TForm(Owner)
 {
  PlanKindRG->Style->StyleController = ADM->StyleController;

  PlanParamGB->Style->StyleController       = ADM->StyleController;
  PlanPrintFormatCB->Style->StyleController = ADM->StyleController;
  BrigPlanOptGB->Style->StyleController     = ADM->StyleController;
  BrigPlanPeriod->Style->StyleController    = ADM->StyleController;
  //FDefUchOrgCode = "";
  FPlpanMonth = ADefPlanMonth;
  FDefLPUCode = "";
  if (ADefDopCode.Length())
   {
    FDefLPUCode = GetLPartB(ADefDopCode, '.');
   }
  FDefUchOrgCode = ADefUchOrgCode;
  //
  FDM = ADM;
  //FXMLList = AXMLList;

  FIsSubSel    = false;
  IsAll        = false;
  Result       = new TTagNode;
  Result->Name = "ieres";
  FTempl       = NULL;
  FTempXML     = new TTagNode(NULL);
  TmplNode     = new TTagNode(NULL);
  FRootNode    = FDM->XMLList->GetXML("40381E23-92155860-4448");
  TmplNode->Assign(FRootNode, true);

  //UnicodeString FPlPrCode, FPlPrStr;
  //TJSONPairEnumerator *itPair = FDM->GetPlanPrintFormats()->GetEnumerator();
  PlanPrintFormatCB->Properties->Items->Clear();
  if (FDM->OnGetReportList)
   {
    TStringList * tmpRepList = new TStringList;
    try
     {
      tmpRepList->Add("add");
      FDM->OnGetReportList("planreport: ", tmpRepList);

      UnicodeString uchdefplanreport = FDM->AppOpt->Vals["uchdefplanreport"].AsString.UpperCase();
      UnicodeString orgdefplanreport = FDM->AppOpt->Vals["orgdefplanreport"].AsString.UpperCase();

      UnicodeString FCaption, FGUI;
      for (int i = 0; i < tmpRepList->Count; i++)
       {
        FCaption = GetRPartB(tmpRepList->Strings[i], '=');
        FGUI     = GetLPartB(tmpRepList->Strings[i], '=');
        PlanPrintFormatCB->Properties->Items->Add(FCaption);
        FPlanPrintFormats[PlanPrintFormatCB->Properties->Items->Count - 1] = FGUI;

        if (uchdefplanreport == FGUI.UpperCase())
         FDefUchPFIdx = PlanPrintFormatCB->Properties->Items->Count - 1;
        if (orgdefplanreport == FGUI.UpperCase())
         FDefOrgPFIdx = PlanPrintFormatCB->Properties->Items->Count - 1;
       }
     }
    __finally
     {
      delete tmpRepList;
     }
   }
  PlanKindRG->ItemIndex = -1;
  if (ADefType && FDefUchOrgCode)
   {//конкретно планируем %)_
    PlanKindRG->ItemIndex = ADefType - 1;
    if (PlanKindRG->ItemIndex)
     {//организация
      OrgPlanTypeCB->ItemIndex = ADefSubType;
     }
    else
     {//участок

     }
   }
  else
   PlanKindRG->ItemIndex = 0;

  /*
   while (itPair->MoveNext())
   {
   FPlPrCode = ((TJSONString*)itPair->Current->JsonString)->Value();
   FPlPrStr  = ((TJSONString*)itPair->Current->JsonValue)->Value();
   }
   */
 }
//---------------------------------------------------------------------------
void __fastcall TdsCreatePlanForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_RETURN)
   {
    ActiveControl = OkBtn;
    OkBtnClick(this);
   }
 }
//---------------------------------------------------------------------------
int __fastcall TdsCreatePlanForm::GetPlanType()
 {
  int RC = -1;
  try
   {
    switch (PlanKindRG->ItemIndex)
     {
     case 0: //участок
       {
        RC = 0;
        break;
       }
     case 1: //организация
       {
        switch (OrgPlanTypeCB->ItemIndex)
         {
         case 0: //стандартный план (без прививок и проб выполняемых бригадно)
           {
            RC = 1;
            break;
           }
         case 1: //полный план (входят все прививки и пробы)
           {
            RC = 2;
            break;
           }
         case 2: //план для бригадного выполнения (общий)
           {
            RC = 3;
            break;
           }
          //case 3: // план для бригадного выполнения (отдельно для каждой инфекции)
          //{
          //RC = 4;
          //break;
          //}
         }
        break;
       }
     case 2: //фильтр
       {
        RC = 5;
        break;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
int __fastcall TdsCreatePlanForm::GetSupportType()
 {
  int RC = -1;
  try
   {
    switch (PlanKindRG->ItemIndex)
     {
     case 0: //участок
       {
        RC = SupportTypeCB->ItemIndex;
        break;
       }
     case 1: //организация
       {
        RC = SupportTypeCB->ItemIndex;
        break;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCreatePlanForm::OkBtnClick(TObject * Sender)
 {
  TWinControl * tmpCtrl = ActiveControl;
  ActiveControl = OkBtn;
  ActiveControl = tmpCtrl;
  if (CheckInput())
   {//запрос на проверку данных
    PlanKindRG->Enabled = false;
    try
     {
      TJSONObject * TmplVal = FTempl->GetJSONValue();
      TmplVal->AddPair("type", GetPlanType());
      TmplVal->AddPair("supporttype", GetSupportType());
      TmplVal->AddPair("planmonth", FPlpanMonth);
      TmplVal->AddPair("planperiod", BrigPlanPeriod->Value);
      TmplVal->AddPair("planprintformat", FPlanPrintFormats[PlanPrintFormatCB->ItemIndex]);

      UnicodeString PRC = FDM->StartCreatePlan(TmplVal);
      if (!SameText(PRC, "ok"))
       _MSG_ERR(PRC, "Ошибка");
      else
       {
        FCh = true;
        bool FCont = true;
        while (FCont)
         {
          if (!FCh)
           {
            FCont = !SameText(FDM->StopCreatePlan(), "ok");
           }
          if (FCont)
           {
            PRC = FDM->CheckCreatePlanProgress(FInsertedRecId);

            if (SameText(PRC, "ok"))
             {
              FCh         = false;
              ModalResult = mrOk;
             }
            else if (SameText(PRC, "error"))
             {
              FCh = false;
              _MSG_ERR(PRC, "Ошибка");
             }
            else
             {
              MsgLab->Caption = PRC;
              Application->ProcessMessages();
              Sleep(300);
             }
           }
         }
       }
     }
    catch (EkabCustomDataSetError & E)
     {
      _MSG_ERR(E.Message, "Ошибка");
     }
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCreatePlanForm::GetFl(TTagNode * itTag, UnicodeString & UIDs)
 {
  if (itTag->CmpName("fl"))
   UIDs += itTag->AV["ref"] + "\n";
  return false;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCreatePlanForm::CheckInput()
 {
  bool RC = false;
  try
   {
    if (FTempXML)
     {
      UnicodeString xValid = "";
      FTempXML->Iterate(GetInput, xValid);
      if (!xValid.Length())
       {
        if (PlanPrintFormatCB->ItemIndex != -1)
         RC = true;
        else
         _MSG_ERR("Необходимо указать формат печати плана.", "Ошибка");

        /*if (FDM->OnExtValidate)
         {
         if (FIsAppend)
         FDM->OnExtValidate(-1, FClassDef, FCtrList, RC);
         else
         FDM->OnExtValidate(FDataSrc->DataSet->EditedRow->Value["CODE"], FClassDef, FCtrList, RC);
         }*/
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsCreatePlanForm::GetInput(TTagNode * itTag, UnicodeString & UID)
 {
  if (itTag && FTempl)
   {
    if (FTempl->CtrList->GetTemplateControl(itTag->AV["ref"]))
     {
      if (!FTempl->CtrList->TemplateItems[itTag->AV["ref"]]->CheckReqValue())
       {
        UID = "no";
        return true;
       }
     }
   }
  return false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCreatePlanForm::FormDestroy(TObject * Sender)
 {
  delete FTempl;
  delete FTempXML;
  delete TmplNode;
 }
//---------------------------------------------------------------------------
void __fastcall TdsCreatePlanForm::PlanKindRGPropertiesChange(TObject * Sender)
 {
  OrgPlanTypeCB->Properties->Items->Clear();
  SupportTypeCB->Properties->Items->Clear();

  switch (PlanKindRG->ItemIndex)
   {
   case 0: //участок
     {
      OrgPlanTypeCB->Properties->Items->Add("полный план (входят все прививки и пробы)");
      OrgPlanTypeCB->ItemIndex = 0;
      OrgPlanTypeCB->Enabled   = false;

      SupportTypeLab->Enabled = true;
      SupportTypeCB->Enabled  = true;
      SupportTypeCB->Properties->Items->Add("Обслуживаемые по участку + по полису");
      SupportTypeCB->Properties->Items->Add("Обслуживаемые по участку");
      SupportTypeCB->Properties->Items->Add("Обслуживаемые по полису");
      SupportTypeCB->ItemIndex = 0;
      //SupportTypeCB->Enabled = false;
      break;
     }
   case 1: //организация
     {
      OrgPlanTypeCB->Properties->Items->Add("стандартный план (без прививок и проб выполняемых бригадно)");
      OrgPlanTypeCB->Properties->Items->Add("полный план (входят все прививки и пробы)");
      OrgPlanTypeCB->Properties->Items->Add("план для бригадного выполнения (общий)");
      //OrgPlanTypeCB->Properties->Items->Add("{резерв}план для бригадного выполнения (отдельно для каждой инфекции)");
      OrgPlanTypeCB->ItemIndex = 0;
      OrgPlanTypeCB->Enabled   = true;

      SupportTypeLab->Enabled = true;
      SupportTypeCB->Enabled  = true;
      SupportTypeCB->Properties->Items->Add("Обслуживаемые по организации");
      SupportTypeCB->Properties->Items->Add("Обслуживаемые по организации + контролируемые");
      SupportTypeCB->Properties->Items->Add("Контролируемые");
      SupportTypeCB->ItemIndex = 0;
      break;
     }
   case 2: //фильтр
     {
      OrgPlanTypeCB->ItemIndex = -1;
      OrgPlanTypeCB->Enabled   = false;
      SupportTypeCB->ItemIndex = -1;
      SupportTypeCB->Enabled   = false;
      SupportTypeLab->Enabled  = false;
      break;
     }
   }
  PlanKindChange();
 }
//---------------------------------------------------------------------------
void __fastcall TdsCreatePlanForm::OrgPlanTypeCBPropertiesChange(TObject * Sender)
 {
  PlanKindChange();
 }
//---------------------------------------------------------------------------
void __fastcall TdsCreatePlanForm::PlanKindChange()
 {
  actApply->Enabled = true;
  UnicodeString tmp;
  CompPanel->Visible = false;
  if (FTempl)
   delete FTempl;
  FTempl                 = NULL;
  BrigPlanOptGB->Visible = false;
  BrigPlanOptGB->Enabled = false;
  try
   {
    int FPlanType = GetPlanType();
    if (FPlanType == 0)
     PlanPrintFormatCB->ItemIndex = FDefUchPFIdx;
    else if ((FPlanType > 0) && (FPlanType < 5))
     PlanPrintFormatCB->ItemIndex = FDefOrgPFIdx;

    if (FPlanType != 5)
     {
      FTempl                  = new TdsRegTemplate(TmplNode);
      FTempl->StyleController = FDM->StyleController;
      FTempl->DataPath        = FDM->DataPath;
      tmp                     = FDM->GetPlanTemplateDef(GetPlanType());
     }
    if ((FPlanType == 3) || (FPlanType == 4))
     {
      BrigPlanOptGB->Visible      = true;
      BrigPlanOptGB->Enabled      = true;
      BrigPlanPeriod->Enabled     = true;
      BrigPlanPeriodLab1->Enabled = true;
      BrigPlanPeriodLab2->Enabled = true;
     }
   }
  __finally
   {
    if (FTempl)
     {
      FTempl->TmplPanel                  = CompPanel;
      FTempl->DataProvider->OnGetValById = FDM->OnGetValById;
      FTempl->OnExtBtnClick              = FDM->OnExtBtnClick;
      FTempXML->AsXML                    = tmp;

      FTempl->DataPath = FDM->DataPath;
      FTempl->CreateTemplate(FTempXML->AsXML);
      FTempl->OnKeyDown = FormKeyDown;

      TTagNode * FLPUNode = FTempXML->GetChildByAV("fl", "ref", "00F1");
      if (FLPUNode)
       {
        if (FDefLPUCode.Length())
         {
          FTempl->CtrList->TemplateItems["00F1"]->SetValue(FDefLPUCode);
          FTempl->CtrList->TemplateItems["00F1"]->SetEnable(true);
          //FTempl->CtrList->TemplateItems["00F1"]->UpdateChanges();
         }
        else if (FDM->LPUCode)
         {
          FTempl->CtrList->TemplateItems["00F1"]->SetValue(IntToStr(FDM->LPUCode));
          FTempl->CtrList->TemplateItems["00F1"]->SetEnable(true);
          //FTempl->CtrList->TemplateItems["00F1"]->UpdateChanges();
         }
       }
      /*FLPUNode = FTempXML->GetChildByAV("fl", "ref", "00F9");
       if (FLPUNode)
       {
       if (FDefLPUPartCode.Length())
       {
       FTempl->CtrList->TemplateItems["00F9"]->SetValue(FDefLPUPartCode);
       FTempl->CtrList->TemplateItems["00F9"]->SetEnable(true);
       }
       else if (FDM->LPUPartCode != -1)
       {
       FTempl->CtrList->TemplateItems["00F9"]->SetValue(IntToStr((int)FDM->LPUPartCode));
       FTempl->CtrList->TemplateItems["00F9"]->SetEnable(true);
       }
       }
       */
      FLPUNode = FTempXML->GetChildByAV("fl", "ref", "00B3");
      if (FLPUNode)
       {
        if (FDefUchOrgCode)
         {
          FTempl->CtrList->TemplateItems["00B3"]->SetValue(IntToStr(FDefUchOrgCode));
          FTempl->CtrList->TemplateItems["00B3"]->SetEnable(true);
         }
       }
      FLPUNode = FTempXML->GetChildByAV("fl", "ref", "0025");
      if (FLPUNode)
       {
        if (FDefUchOrgCode)
         {
          FTempl->CtrList->TemplateItems["0025"]->SetValue("findbycode:" + IntToStr(FDefUchOrgCode));
          FTempl->CtrList->TemplateItems["0025"]->SetEnable(true);
         }
       }
     }
    CompPanel->Visible = true;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsCreatePlanForm::CancelBtnClick(TObject * Sender)
 {
  FCh = false;
  Application->ProcessMessages();
 }
//---------------------------------------------------------------------------
void __fastcall TdsCreatePlanForm::FormCloseQuery(TObject * Sender, bool & CanClose)
 {
  FCh = false;
  Application->ProcessMessages();
  CanClose = true;
 }
//---------------------------------------------------------------------------
