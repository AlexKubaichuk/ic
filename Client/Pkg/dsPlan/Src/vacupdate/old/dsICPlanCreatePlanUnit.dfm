object dsCreatePlanForm: TdsCreatePlanForm
  Left = 369
  Top = 357
  HelpContext = 1011
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1057#1086#1079#1076#1072#1085#1080#1077' '#1087#1083#1072#1085#1072
  ClientHeight = 443
  ClientWidth = 524
  Color = clBtnFace
  Constraints.MinHeight = 110
  Constraints.MinWidth = 298
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = [fsBold]
  OldCreateOrder = True
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 383
    Width = 524
    Height = 60
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      524
      60)
    object MsgLab: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 5
      Width = 518
      Height = 13
      Margins.Top = 5
      Margins.Bottom = 5
      Align = alTop
      AutoSize = False
      Color = clCream
      EllipsisPosition = epEndEllipsis
      ParentColor = False
      Transparent = False
      ExplicitLeft = 6
      ExplicitTop = 6
      ExplicitWidth = 605
    end
    object OkBtn: TcxButton
      Left = 354
      Top = 27
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = OkBtnClick
    end
    object CancelBtn: TcxButton
      Left = 440
      Top = 28
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = CancelBtnClick
    end
  end
  object PlanParamGB: TcxGroupBox
    Left = 0
    Top = 111
    Align = alClient
    Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1087#1083#1072#1085#1072
    PanelStyle.OfficeBackgroundKind = pobkStyleColor
    ParentBackground = False
    ParentColor = False
    Style.BorderColor = clBlue
    Style.BorderStyle = ebsUltraFlat
    Style.Color = clWhite
    Style.LookAndFeel.Kind = lfUltraFlat
    Style.LookAndFeel.NativeStyle = True
    Style.LookAndFeel.SkinName = ''
    Style.TextColor = clBlue
    Style.TransparentBorder = True
    StyleDisabled.BorderStyle = ebsUltraFlat
    StyleDisabled.LookAndFeel.Kind = lfUltraFlat
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.SkinName = ''
    StyleFocused.LookAndFeel.Kind = lfUltraFlat
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.SkinName = ''
    StyleHot.LookAndFeel.Kind = lfUltraFlat
    StyleHot.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.SkinName = ''
    TabOrder = 1
    Height = 224
    Width = 524
    object CompPanel: TPanel
      Left = 2
      Top = 18
      Width = 520
      Height = 175
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 5
      UseDockManager = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object PlanPrintPanel: TPanel
      Left = 2
      Top = 193
      Width = 520
      Height = 29
      Align = alBottom
      BevelOuter = bvNone
      BorderWidth = 3
      TabOrder = 1
      object Label2: TLabel
        AlignWithMargins = True
        Left = 6
        Top = 6
        Width = 104
        Height = 17
        Align = alLeft
        AutoSize = False
        Caption = #1060#1086#1088#1084#1072#1090' '#1087#1077#1095#1072#1090#1080':'
        ExplicitLeft = 3
        ExplicitTop = 3
      end
      object PlanPrintFormatCB: TcxComboBox
        Left = 113
        Top = 3
        Align = alClient
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          #1060#1086#1088#1084#1072#1090' '#1087#1083#1072#1085#1072'   1'
          #1060#1086#1088#1084#1072#1090' '#1087#1083#1072#1085#1072'  2'
          '. . .'
          #1060#1086#1088#1084#1072#1090' '#1087#1083#1072#1085#1072'  '#1099' n'
          '')
        Style.Color = clInfoBk
        TabOrder = 0
        OnKeyDown = FormKeyDown
        Width = 404
      end
    end
  end
  object BrigPlanOptGB: TcxGroupBox
    Left = 0
    Top = 335
    Align = alBottom
    Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1087#1083#1072#1085#1072' '#1076#1083#1103' '#1073#1088#1080#1075#1072#1076#1085#1086#1075#1086' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103
    Enabled = False
    ParentBackground = False
    ParentColor = False
    Style.Color = clBtnFace
    Style.TextColor = clBlue
    StyleDisabled.BorderStyle = ebsUltraFlat
    StyleDisabled.TextColor = clBtnShadow
    StyleDisabled.TextStyle = [fsBold]
    TabOrder = 2
    Visible = False
    Height = 48
    Width = 524
    object BrigPlanPeriodLab1: TLabel
      Left = 6
      Top = 23
      Width = 145
      Height = 13
      Caption = #1060#1086#1088#1084#1080#1088#1086#1074#1072#1080#1090#1100' '#1087#1083#1072#1085' '#1085#1072' '
    end
    object BrigPlanPeriodLab2: TLabel
      Left = 217
      Top = 23
      Width = 214
      Height = 13
      Caption = #1084#1077#1089#1103#1094'('#1086#1074') '#1074#1087#1077#1088#1105#1076' '#1086#1090' '#1090#1077#1082#1091#1097#1077#1081' '#1076#1072#1090#1099'.'
      Enabled = False
    end
    object BrigPlanPeriod: TcxSpinEdit
      Left = 157
      Top = 17
      ParentFont = False
      Properties.MaxValue = 12.000000000000000000
      Properties.MinValue = 1.000000000000000000
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 0
      Value = 1
      OnKeyDown = FormKeyDown
      Width = 54
    end
  end
  object PlanTypeGB: TcxGroupBox
    Left = 0
    Top = 0
    Align = alTop
    Caption = #1058#1080#1087' '#1087#1083#1072#1085#1072
    PanelStyle.OfficeBackgroundKind = pobkStyleColor
    ParentBackground = False
    ParentColor = False
    Style.BorderColor = clBlue
    Style.BorderStyle = ebsUltraFlat
    Style.Color = clWhite
    Style.LookAndFeel.Kind = lfUltraFlat
    Style.LookAndFeel.NativeStyle = True
    Style.LookAndFeel.SkinName = ''
    Style.TextColor = clBlue
    Style.TransparentBorder = True
    StyleDisabled.BorderStyle = ebsUltraFlat
    StyleDisabled.LookAndFeel.Kind = lfUltraFlat
    StyleDisabled.LookAndFeel.NativeStyle = True
    StyleDisabled.LookAndFeel.SkinName = ''
    StyleFocused.LookAndFeel.Kind = lfUltraFlat
    StyleFocused.LookAndFeel.NativeStyle = True
    StyleFocused.LookAndFeel.SkinName = ''
    StyleHot.LookAndFeel.Kind = lfUltraFlat
    StyleHot.LookAndFeel.NativeStyle = True
    StyleHot.LookAndFeel.SkinName = ''
    TabOrder = 3
    Height = 111
    Width = 524
    object PlanKindRG: TcxRadioGroup
      AlignWithMargins = True
      Left = 5
      Top = 21
      Cursor = crHandPoint
      Align = alTop
      Alignment = alTopCenter
      ParentBackground = False
      ParentFont = False
      Properties.Columns = 3
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1059#1095#1072#1089#1090#1086#1082
          Value = 0
        end
        item
          Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
          Value = 1
        end
        item
          Caption = #1055#1086' '#1092#1080#1083#1100#1090#1088#1091
          Value = '5'
        end>
      Properties.WordWrap = True
      Properties.OnChange = PlanKindRGPropertiesChange
      Style.BorderStyle = ebsNone
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 0
      Height = 27
      Width = 514
    end
    object OrgPlanTypeCB: TcxComboBox
      AlignWithMargins = True
      Left = 5
      Top = 54
      Align = alTop
      Properties.DropDownListStyle = lsFixedList
      Properties.Items.Strings = (
        #1087#1086#1083#1085#1099#1081' '#1087#1083#1072#1085' ('#1074#1093#1086#1076#1103#1090' '#1074#1089#1077' '#1087#1088#1080#1074#1080#1074#1082#1080' '#1080' '#1087#1088#1086#1073#1099')'
        #1089#1090#1072#1085#1076#1072#1088#1090#1085#1099#1081' '#1087#1083#1072#1085' ('#1073#1077#1079' '#1087#1088#1080#1074#1080#1074#1086#1082' '#1080' '#1087#1088#1086#1073' '#1074#1099#1087#1086#1083#1085#1103#1077#1084#1099#1093' '#1073#1088#1080#1075#1072#1076#1085#1086')'
        #1087#1086#1083#1085#1099#1081' '#1087#1083#1072#1085' ('#1074#1093#1086#1076#1103#1090' '#1074#1089#1077' '#1087#1088#1080#1074#1080#1074#1082#1080' '#1080' '#1087#1088#1086#1073#1099') '
        #1087#1083#1072#1085' '#1076#1083#1103' '#1073#1088#1080#1075#1072#1076#1085#1086#1075#1086' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103' ('#1086#1073#1097#1080#1081') '
        #1087#1083#1072#1085' '#1076#1083#1103' '#1073#1088#1080#1075#1072#1076#1085#1086#1075#1086' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103' ('#1086#1090#1076#1077#1083#1100#1085#1086' '#1076#1083#1103' '#1082#1072#1078#1076#1086#1081' '#1080#1085#1092#1077#1082#1094#1080#1080')')
      Properties.OnChange = OrgPlanTypeCBPropertiesChange
      Style.Color = clInfoBk
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 1
      Width = 514
    end
    object Panel1: TPanel
      Left = 2
      Top = 78
      Width = 520
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object SupportTypeLab: TLabel
        Left = 5
        Top = 7
        Width = 134
        Height = 13
        Caption = #1057#1090#1072#1090#1091#1089' '#1086#1073#1089#1083#1091#1078#1080#1074#1072#1085#1080#1103':'
      end
      object SupportTypeCB: TcxComboBox
        AlignWithMargins = True
        Left = 145
        Top = 3
        Properties.DropDownListStyle = lsFixedList
        Properties.OnChange = OrgPlanTypeCBPropertiesChange
        Style.Color = clInfoBk
        Style.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 0
        Width = 372
      end
    end
  end
  object PlanListCreateAL: TActionList
    Left = 314
    Top = 122
    object actSelAll: TAction
      Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1105
      Enabled = False
      Hint = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1105
      ShortCut = 16449
    end
    object actResetAll: TAction
      Caption = #1057#1085#1103#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1080#1077
      Enabled = False
      Hint = #1057#1085#1103#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1080#1077
      ShortCut = 16474
    end
    object actApply: TAction
      Caption = 'actApply'
      Enabled = False
    end
  end
end
