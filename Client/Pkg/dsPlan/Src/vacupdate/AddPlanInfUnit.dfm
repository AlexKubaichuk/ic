object AddPlanInfForm: TAddPlanInfForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1101#1087#1080#1076'. '#1087#1083#1072#1085
  ClientHeight = 283
  ClientWidth = 431
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 425
    Height = 13
    Align = alTop
    Caption = #1055#1083#1072#1085#1080#1088#1086#1074#1072#1090#1100':'
    ExplicitWidth = 71
  end
  object Label2: TLabel
    AlignWithMargins = True
    Left = 3
    Top = 49
    Width = 425
    Height = 13
    Align = alTop
    Caption = #1044#1083#1103':'
    ExplicitWidth = 24
  end
  object BtnPanel: TPanel
    Left = 0
    Top = 242
    Width = 431
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      431
      41)
    object CancelBtn: TButton
      Left = 327
      Top = 6
      Width = 100
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 0
    end
    object ApplyBtn: TButton
      Left = 221
      Top = 6
      Width = 100
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      TabOrder = 1
      OnClick = ApplyBtnClick
    end
  end
  object InfCB: TcxComboBox
    AlignWithMargins = True
    Left = 3
    Top = 22
    Align = alTop
    Properties.DropDownListStyle = lsEditFixedList
    TabOrder = 1
    Width = 425
  end
  object ItemTypeCB: TcxComboBox
    AlignWithMargins = True
    Left = 3
    Top = 68
    Align = alTop
    Properties.DropDownListStyle = lsEditFixedList
    Properties.Items.Strings = (
      #1051#1055#1059
      #1059#1095#1072#1089#1090#1086#1082
      #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
      #1040#1076#1088#1077#1089)
    Properties.OnChange = ItemTypeCBPropertiesChange
    TabOrder = 2
    Width = 425
  end
  object CompPanel: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 95
    Width = 425
    Height = 144
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
  end
end
