﻿// ---------------------------------------------------------------------------
#include <vcl.h>
// #include <stdio.h>
// #include <clipbrd.hpp>
#pragma hdrstop

#include "dsICPlanDMUnit.h"
#include "icsDateUtil.hpp"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
// kabCDSGetValById("reg.s3 - ps1 ps2:ps3;
// kabCDSGetValById("reg.s4 - ps1 ps2:ps3#ps4
TdsICPlanDM * dsICPlanDM;
// ---------------------------------------------------------------------------
__fastcall TdsICPlanDM::TdsICPlanDM(TComponent * Owner) : TDataModule(Owner)
 {
  FClassDefNode = NULL;
  // FRegDefNode = NULL;
  FPlanEditable     = true;
  FFullEdit          = true;
  FClassShowExpanded = "";
  FRequiredColor     = clInfoBk;
  FULEditWidth       = 700;
//  FXMLPath           = icsPrePath(GetEnvironmentVariable("APPDATA") + "\\"+PRODFOLDER+"\\xml");
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICPlanDM::DataModuleCreate(TObject * Sender)
 {
  UnitList     = NULL;
  SelRegUnit   = NULL;
  DTFormats[0] = FMT(icsDTFormatsD);
  DTFormats[1] = FMT(icsDTFormatsDT);
  DTFormats[2] = FMT(icsDTFormatsT);
  FExtLPUCode  = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICPlanDM::GetClassData(UnicodeString ASQL, TStrings * ARetData)
 {
  TJSONObject * RetData;
  UnicodeString FClCode, FClStr;
  TJSONPairEnumerator * itPair;
  ARetData->Clear();
  RetData = kabCDSGetValById("reg.s2." + ASQL, "0");
  itPair  = RetData->GetEnumerator();
  while (itPair->MoveNext())
   {
    FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
    FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
    ARetData->AddObject(FClStr, (TObject *)FClCode.ToIntDef(0));
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICPlanDM::GetStrClassData(UnicodeString ASQL, TStrings * ARetData)
 {
  TJSONObject * RetData;
  UnicodeString FClCode, FClStr;
  TJSONPairEnumerator * itPair;
  ARetData->Clear();
  RetData = kabCDSGetValById("reg.s2." + ASQL, "0");
  itPair  = RetData->GetEnumerator();
  while (itPair->MoveNext())
   {
    FClCode = ((TJSONString *)itPair->Current->JsonString)->Value();
    FClStr  = ((TJSONString *)itPair->Current->JsonValue)->Value();
    ARetData->Add(FClStr + "^" + FClCode);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICPlanDM::FLoadMapData(TJSONObject * AData, TAnsiStrMap & AMap /* , TAnsiStrMap &AMapCode */)
 {
  TJSONPairEnumerator * itPair;
  UnicodeString FClCode, FClStr;

  itPair = AData->GetEnumerator();
  while (itPair->MoveNext())
   {
    FClCode       = ((TJSONString *)itPair->Current->JsonString)->Value();
    FClStr        = ((TJSONString *)itPair->Current->JsonValue)->Value();
    AMap[FClCode] = FClStr;
    // AMapCode[FClStr] = FClCode;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICPlanDM::FLoadDependMapData(TJSONObject * AData, TIntListMap & AMap, TIntListMap & AMapCode)
 {
  TJSONPairEnumerator * itPair;
  int FIntClCode, FIntClVal;
  TIntMap * vti;

  itPair = AData->GetEnumerator();
  while (itPair->MoveNext())
   {
    FIntClCode = ((TJSONString *)itPair->Current->JsonString)->Value().ToIntDef(-1);
    FIntClVal  = ((TJSONString *)itPair->Current->JsonValue)->Value().ToIntDef(-1);

    if (AMap.find(FIntClCode) == AMap.end())
     AMap[FIntClCode] = new TIntMap;
    vti = AMap[FIntClCode];
    (*vti)[FIntClVal] = 1;

    if (AMapCode.find(FIntClVal) == AMapCode.end())
     AMapCode[FIntClVal] = new TIntMap;
    vti = AMapCode[FIntClVal];
    (*vti)[FIntClCode] = 1;
   }
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsICPlanDM::kabCDSGetListById(UnicodeString AId)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetListById)
     FOnGetListById(AId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICPlanDM::PreloadData()
 {
  // ***** Список Инфекций ******
  FLoadMapData(kabCDSGetListById("reg.s2.Select CODE as ps1, R003C as ps2 From CLASS_003A"), InfList);
  // ***** Список МИБП ******
  FLoadMapData(kabCDSGetListById("reg.s2.Select CODE as ps1, R0040 as ps2 From CLASS_0035"), VacList);
  // ***** Список Проб ******
  FLoadMapData(kabCDSGetListById("reg.s2.Select CODE as ps1, R002C as ps2 From CLASS_002A"), ProbList);
  // ***** Список проверок ******
  FLoadMapData(kabCDSGetListById("reg.s2.Select CODE as ps1, R013F as ps2 From CLASS_013E"), CheckList);
  // ***** Инфекции по пробам и пробы по инфекциям ******
  FLoadDependMapData(kabCDSGetListById("reg.s2.Select R007B as ps1, R007A as ps2 From CLASS_0079"), InfProbList,
   ProbInfList);
  // ***** Инфекции по вакцинам и вакцины по инфекциям ******
  FLoadDependMapData(kabCDSGetListById("reg.s2.Select R003E as ps1, R003B as ps2 From CLASS_002D"), InfVacList,
   VacInfList);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::NewGUID()
 {
  return icsNewGUID().UpperCase();
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsICPlanDM::FGetClassDef()
 {
  TTagNode * RC = FClassDefNode;
  try
   {
    if (!RC)
     {
      FClassDefNode = RegDef->GetChildByName("classes");
      RC            = FClassDefNode;
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TdsICPlanDM::kabCDSGetCount(UnicodeString AId, UnicodeString ATTH, UnicodeString AFilterParam)
 {
  __int64 RC = 0;
  try
   {
    if (FOnGetCount)
     RC = FOnGetCount(AId, AFilterParam);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsICPlanDM::kabCDSGetCodes(UnicodeString AId, int AMax, UnicodeString ATTH,
 UnicodeString AFilterParam)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetIdList)
     FOnGetIdList(AId, AMax, AFilterParam, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsICPlanDM::kabCDSGetValById(UnicodeString AId, UnicodeString ARecId)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetValById)
     FOnGetValById(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsICPlanDM::kabCDSGetValById10(UnicodeString AId, UnicodeString ARecId)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetValById10)
     FOnGetValById10(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::kabCDSInsert(UnicodeString AId, TJSONObject * AValues, UnicodeString & ARecId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnInsertData)
     FOnInsertData(AId, AValues, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::kabCDSEdit(UnicodeString AId, TJSONObject * AValues, UnicodeString & ARecId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnEditData)
     FOnEditData(AId, AValues, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::kabCDSDelete(UnicodeString AId, UnicodeString ARecId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnDeleteData)
     FOnDeleteData(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::kabCDSGetClassXML(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetClassXML)
     FOnGetClassXML(AId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::kabCDSGetF63(UnicodeString AId)
 {
  UnicodeString RC = "";
  try
   {
    if (FOnGetF63)
     FOnGetF63(AId, RC);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
//void __fastcall TdsICPlanDM::OnGetXML(UnicodeString AId, TTagNode *& ARC)
// {
//  try
//   {
//    if (!FXMLList->XMLExist(AId))
//     {
//      TTagNode * tmp = new TTagNode;
//      try
//       {
//        UnicodeString FFN = icsPrePath(FXMLPath + "\\" + AId + ".xml");
//        if (!FileExists(FFN))
//         {
//          ForceDirectories(FXMLPath);
//          tmp->AsXML = kabCDSGetClassXML(AId);
//          tmp->SaveToXMLFile(FFN, "");
//         }
//        FXMLList->Load(FFN, AId);
//       }
//      __finally
//       {
//        delete tmp;
//       }
//     }
//    ARC = FXMLList->GetXML(AId);
//   }
//  __finally
//   {
//   }
// }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::GetVacSch(TStringList * AInfList, UnicodeString AVacCode, __int64 AUCode,
 UnicodeString & AVacSch, UnicodeString & AVacSchTur)
 {
  /*
   UnicodeString RC = "";
   TTagNode *FSettingNode = new TTagNode;
   try
   {
   AVacSch = "";
   AVacSchTur = "";
   FSettingNode->AsXML = GetPatSetting(AUCode);
   TTagNode *FSchList = FSettingNode->GetChildByName("sch", true);
   UnicodeString FCurrSch, FCurrTurSch;
   TTagNode *FSchNode;
   if (FSchList)
   {
   FCurrSch = "";
   FCurrTurSch = "";
   for (int i = 0; i < AInfList->Count; i++)
   {
   FSchNode = FSchList->GetChildByAV("spr","infref",IntToStr((int)AInfList->Objects[i]));
   if (FSchNode)
   {
   if (!FCurrSch.Length())
   {
   FCurrSch = FSchNode->AV["vac"].UpperCase();
   AVacSch  = FCurrSch;
   }
   else
   {
   if (FCurrSch != FSchNode->AV["vac"].UpperCase())
   {
   FCurrSch = FSchNode->AV["vac"].UpperCase();
   AVacSch += ", "+FCurrSch;
   }
   }
   if (!FCurrTurSch.Length())
   {
   FCurrTurSch = FSchNode->AV["tvac"].UpperCase();
   AVacSchTur  = FCurrTurSch;
   }
   else
   {
   if (FCurrTurSch != FSchNode->AV["tvac"].UpperCase())
   {
   FCurrTurSch = FSchNode->AV["tvac"].UpperCase();
   AVacSchTur += ", "+FCurrTurSch;
   }
   }
   }
   }
   }
   }
   __finally
   {
   delete FSettingNode;
   }
   return RC;
   */
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsICPlanDM::FGetVacSchDef()
 {
  TTagNode * RC = NULL;
  try
   {
    RC = FXMLList->GetXML("12063611-00008CD7-CD89");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsICPlanDM::FGetTestSchDef()
 {
  TTagNode * RC = NULL;
  try
   {
    RC = FXMLList->GetXML("001D3500-00005882-DC28");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsICPlanDM::FGetCheckSchDef()
 {
  TTagNode * RC = NULL;
  try
   {
    RC = FXMLList->GetXML("569E1EB2-D0CFEF0F-BE9A");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICPlanDM::GetSchemeList(int AInfCode, TTagNodeMap & ASchList, TSchemeObjType ASchType)
 {
  TIntListMap::iterator RC = NULL;
  TIntListMap::iterator EndSL = NULL;
  TTagNode * itSch = NULL;
  switch (ASchType)
   {
   case TSchemeObjType::Vac:
     {
      itSch = VacSchDef->GetChildByName("schema", true);
      RC    = InfVacList.find(AInfCode);
      EndSL = InfVacList.end();
      break;
     }
   case TSchemeObjType::Test:
     {
      itSch = TestSchDef->GetChildByName("schema", true);
      RC    = InfProbList.find(AInfCode);
      EndSL = InfProbList.end();
      break;
     }
   case TSchemeObjType::Check:
     {
      itSch = CheckSchDef->GetChildByName("schema", true);
      // TIntListMap::iterator RC = ((AVacSch)?InfVacList.find(AInfCode):InfProbList.find(AInfCode));
      // EndSL = InfProbList.end();
      break;
     }
   }

  if (RC != EndSL)
   {
    TIntMap vti = *(RC->second);
    UnicodeString FVacCodes = "";
    for (TIntMap::iterator i = vti.begin(); i != vti.end(); i++)
     {
      if (!FVacCodes.Length())
       FVacCodes = IntToStr(i->first);
      else
       FVacCodes += "," + IntToStr(i->first);
     }
    while (itSch)
     {
      if (itSch->CmpAV("ref", FVacCodes))
       ASchList[itSch->AV["uid"]] = itSch;
      itSch = itSch->GetNext();
     }
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::FGetInfName(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    TAnsiStrMap::iterator FRC = InfList.find(ACode);
    if (FRC != InfList.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::FGetVacName(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    TAnsiStrMap::iterator FRC = VacList.find(ACode);
    if (FRC != VacList.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::FGetTestName(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    TAnsiStrMap::iterator FRC = ProbList.find(ACode);
    if (FRC != ProbList.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::FGetCheckName(UnicodeString ACode)
 {
  UnicodeString RC = "";
  try
   {
    TAnsiStrMap::iterator FRC = CheckList.find(ACode);
    if (FRC != CheckList.end())
     RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::GetSchName(UnicodeString ASchId, bool AVacSch)
 {
  UnicodeString RC = "ошибка";
  try
   {
    if ((ASchId.UpperCase() == "_NULL_") || !ASchId.Length())
     {
      RC = "";
     }
    else
     {
      TTagNode * FSch = (AVacSch) ? VacSchDef->GetTagByUID(_GUI(ASchId)) : TestSchDef->GetTagByUID(_GUI(ASchId));
      TTagNode * FSchLine = (AVacSch) ? VacSchDef->GetTagByUID(_UID(ASchId)) : TestSchDef->GetTagByUID(_UID(ASchId));
      if (FSch && FSchLine)
       {
        if (FSch->CmpName("schema") && FSchLine->CmpName("begline, line, endline") && FSchLine->GetParent("schema")
         ->CmpAV("uid", FSch->AV["uid"]))
         { // _GUI(ASchId) указывает на схему, _UID(ASchId) указывает на строку схемы _GUI(ASchId) - остальное косяки
          if (FSchLine->CmpName("begline"))
           RC = "Начало схемы";
          else if (FSchLine->CmpName("endline"))
           RC = "Конец схемы";
          else
           RC = FSchLine->AV["name"];

          RC = "{" + FSch->AV["name"] + "." + RC + "} " + ((AVacSch) ? VacName[FSch->AV["ref"]] :
           TestName[FSch->AV["ref"]]);
         }
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::AgeStr(TDateTime ACurDate, TDateTime ABirthDay)
 {
  UnicodeString RC = "";
  try
   {
    Word FYear, FMonth, FDay;
    DateDiff(ACurDate, ABirthDay, FDay, FMonth, FYear);
    RC = SetDay(FYear, FMonth, FDay);
   }
  __finally
   {
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::SetDay(int AYear, int AMonth, int ADay)
 {
  UnicodeString RC = "";
  try
   {
    UnicodeString sm = IntToStr(AYear);
    sm = sm.SubString(sm.Length(), 1);
    if (((sm == "1") || (sm == "2") || (sm == "3") || (sm == "4")) && !((AYear < 15) && (AYear > 10)))
     sm = "г. ";
    else
     sm = "л. ";
    if (AYear)
     RC += IntToStr(AYear) + sm;
    if (AMonth)
     RC += IntToStr(AMonth) + "м. ";
    if (ADay)
     RC += IntToStr(ADay) + "д.";
    if (!(AYear + AMonth + ADay))
     RC += "0д.";
   }
  __finally
   {
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICPlanDM::SavePatSetting(__int64 AUCode, UnicodeString AData)
 {
 }
// ---------------------------------------------------------------------------
/*
 UnicodeString __fastcall TdsICPlanDM::CreatePlan(TJSONObject *AVal, UnicodeString &ARecId)
 {
 UnicodeString RC = "ошибка, неопределён обработчик OnCreatePlan";
 try
 {
 if (FOnCreatePlan)
 RC = FOnCreatePlan(AVal, ARecId);
 }
 __finally
 {
 }
 return RC;
 }
 */
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::StartCreatePlan(TJSONObject * AVal)
 {
  UnicodeString RC = "ошибка, неопределён обработчик OnStartCreatePlan";
  try
   {
    if (FOnStartCreatePlan)
     RC = FOnStartCreatePlan(AVal);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::StopCreatePlan()
 {
  UnicodeString RC = "ошибка, неопределён обработчик OnStopCreatePlan";
  try
   {
    if (FOnStopCreatePlan)
     RC = FOnStopCreatePlan();
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::CheckCreatePlanProgress(UnicodeString & ARecId)
 {
  UnicodeString RC = "ошибка, неопределён обработчик OnCheckCreatePlanProgress";
  try
   {
    if (FOnCheckCreatePlanProgress)
     RC = FOnCheckCreatePlanProgress(ARecId);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsICPlanDM::GetPlanPrintFormats()
 {
  TJSONObject * RC; // = new TJSONObject;
  try
   {
    if (FOnGetPlanPrintFormats)
     FOnGetPlanPrintFormats(RC);
    // {}
    // if (FOnCreatePlan)
    // RC = FOnCreatePlan(AVal, ARecId);
    // RC->AddPair("11111111","Формат печати плана 1");
    // RC->AddPair("22222222","Формат печати плана 2");
    // RC->AddPair("33333333","Формат печати плана 3");
    // RC->AddPair("44444444","Формат печати плана n");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::GetPlanTemplateDef(int AType)
 {

  UnicodeString RC = "";
  try
   {
    if (FOnGetPlanTemplateDef)
     RC = FOnGetPlanTemplateDef(AType);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
/* UnicodeString __fastcall TdsICPlanDM::GetPatSetting(__int64 AUCode)
 {
 UnicodeString RC = "";
 try
 {
 if (FOnGetPatSetting)
 RC = FOnGetPatSetting(AUCode);
 }
 __finally
 {
 }
 return RC;
 }
 //---------------------------------------------------------------------------
 */
/*
 UnicodeString __fastcall TdsICPlanDM::GetPrivVar(__int64 APrivCode)
 {
 UnicodeString RC = "";
 try
 {
 if (FOnGetPrivVar)
 RC = FOnGetPrivVar(APrivCode);
 }
 __finally
 {
 }
 return RC;
 }
 //---------------------------------------------------------------------------
 */
/*
 UnicodeString __fastcall TdsICPlanDM::GetOtvodInf(__int64 AOtvodCode)
 {
 UnicodeString RC = "";
 try
 {
 if (FOnGetOtvodInf)
 RC = FOnGetOtvodInf(AOtvodCode);
 }
 __finally
 {
 }
 return RC;
 }
 //---------------------------------------------------------------------------
 */
void __fastcall TdsICPlanDM::ShowVacSch()
 {
  try
   {
    if (FOnShowVacSch)
     FOnShowVacSch();
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICPlanDM::ShowTestSch()
 {
  try
   {
    if (FOnShowTestSch)
     FOnShowTestSch();
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICPlanDM::ShowCheckSch()
 {
  try
   {
    if (FOnShowCheckSch)
     FOnShowCheckSch();
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::DeletePlan(UnicodeString APlanCode)
 {
  UnicodeString RC = "Отсутствует обработчик OnDeletePlan";
  try
   {
    if (FOnDeletePlan)
     RC = FOnDeletePlan(APlanCode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICPlanDM::LoadInfList(TStringList * AInfList)
 {
  for (TAnsiStrMap::iterator i = InfList.begin(); i != InfList.end(); i++)
   {
    AInfList->AddObject(i->second, (TObject *)i->first.ToIntDef(0));
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICPlanDM::LoadVacList(TStringList * AVacList, bool AFull)
 {
  TStringList * StorMIBP = new TStringList;
  try
   {
    int idx;
    if (!AFull)
     GetClassData("Select R1147 as ps1, R1148 AS ps2 From CLASS_1122", StorMIBP);
    for (TAnsiStrMap::iterator i = VacList.begin(); i != VacList.end(); i++)
     {
      if (!AFull)
       {
        idx = StorMIBP->IndexOfObject((TObject *)i->first.ToIntDef(0));
        if (idx != -1)
         if ((int)StorMIBP->Objects[idx])
          AVacList->AddObject(i->second, (TObject *)i->first.ToIntDef(0));
       }
      else
       AVacList->AddObject(i->second, (TObject *)i->first.ToIntDef(0));
     }
   }
  __finally
   {
    delete StorMIBP;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICPlanDM::LoadTestList(TStringList * ATestList)
 {
  for (TAnsiStrMap::iterator i = ProbList.begin(); i != ProbList.end(); i++)
   {
    ATestList->AddObject(i->second, (TObject *)i->first.ToIntDef(0));
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsICPlanDM::LoadCheckList(TStringList * ACheckList)
 {
  for (TAnsiStrMap::iterator i = CheckList.begin(); i != CheckList.end(); i++)
   {
    ACheckList->AddObject(i->second, (TObject *)i->first.ToIntDef(0));
   }
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsICPlanDM::GetVacDefDoze(TDate APrivDate, TDate APatBirthday, UnicodeString AVacCode)
 {
  UnicodeString RC = "0,0";
  try
   {
    if (FOnGetVacDefDoze)
     RC = FOnGetVacDefDoze(APrivDate, APatBirthday, AVacCode);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
