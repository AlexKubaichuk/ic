﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AddPlanInfUnit.h"
#include "msgdef.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"
TAddPlanInfForm *AddPlanInfForm;
//---------------------------------------------------------------------------
__fastcall TAddPlanInfForm::TAddPlanInfForm(TComponent* Owner, TdsICPlanDM *ADM, bool AShowTo)
 : TForm(Owner)
{
  FDM = ADM;
  InfCB->Clear();
  FType = "";
  FCode = "";
  FStrVal = "";
  FInf = "";
  TStringList *FInfList = new TStringList;
  try
   {
     FDM->LoadInfList(FInfList);
     FInfList->Sort();
     for (int i = 0; i < FInfList->Count; i++)
      InfCB->Properties->Items->AddObject(FInfList->Strings[i], FInfList->Objects[i]);

     FRootNode = new TTagNode;
     FTempXML  = new TTagNode;
     FRootNode->Assign(FDM->XMLList->GetXML("40381E23-92155860-4448"),true);
     Label2->Visible = AShowTo;
     ItemTypeCB->Visible = AShowTo;
   }
  __finally
   {
     delete FInfList;
   }
}
//---------------------------------------------------------------------------
void __fastcall TAddPlanInfForm::FormDestroy(TObject *Sender)
{
  delete FTempl;
  delete FTempXML;
  delete FRootNode;
}
//---------------------------------------------------------------------------
void __fastcall TAddPlanInfForm::ItemTypeCBPropertiesChange(TObject *Sender)
{
//  actApply->Enabled = true;

  UnicodeString tmp;
  CompPanel->Visible = false;
  if (FTempl) delete FTempl;
  FTempl = NULL;

  try
   {
     if (ItemTypeCB->ItemIndex > 0)
      {
        FTempl = new TdsRegTemplate(FRootNode);
        FTempl->StyleController = FDM->StyleController;
        FTempl->DataPath        = FDM->DataPath;

        FTempl->TmplPanel = CompPanel;
        FTempl->DataProvider->OnGetValById  = FDM->OnGetValById;
        FTempl->OnExtBtnClick = FDM->OnExtBtnClick;

        tmp = "<root>";

        if (ItemTypeCB->ItemIndex == 1)
         tmp += "<fl ref='00F1' en='0'/> <fl ref='00B3' req='1'/>";
        else if (ItemTypeCB->ItemIndex == 2)
         tmp += "<fl ref='011B'/><fl ref='0026'/><fl ref='0027'/><fl ref='0028'/><fl ref='0111'/><fl ref='0112'/><fl ref='0025' req='1'/>";
        else if (ItemTypeCB->ItemIndex == 3)
         tmp += "<fl ref='0032'/><fl ref='00FF'/><fl ref='0100'/><fl ref='0033'/><fl ref='0136' req='1'/>";

        tmp += "</root>";



        FTempXML->AsXML = tmp;
        FTempl->DataPath = FDM->DataPath;
        FTempl->CreateTemplate(FTempXML->AsXML);
        FTempl->OnKeyDown = FormKeyDown;
        TTagNode *FLPUNode = FTempXML->GetChildByAV("fl", "ref", "00F1");
        if (FLPUNode)
         {
           if (FDM->LPUCode)
            {
              FTempl->CtrList->TemplateItems["00F1"]->SetValue(IntToStr(FDM->LPUCode));
              FTempl->CtrList->TemplateItems["00F1"]->SetEnable(true);
            }
         }

//              FTempl->CtrList->TemplateItems["00F1"]->SetValue(IntToStr(FDM->LPUCode));

      }
   }
  __finally
   {
     CompPanel->Visible = true;
   }
}
//---------------------------------------------------------------------------
void __fastcall TAddPlanInfForm::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
  if (Key == VK_RETURN)
   {ActiveControl = ApplyBtn; ApplyBtnClick(this);}
}
//---------------------------------------------------------------------------
void __fastcall TAddPlanInfForm::ApplyBtnClick(TObject *Sender)
{
  TWinControl *tmpCtrl = ActiveControl;
  ActiveControl = ApplyBtn;
  ActiveControl = tmpCtrl;
  if (CheckInput())
   { // запрос на проверку данных
     try
      {
        FInf = IntToStr((int)InfCB->Properties->Items->Objects[InfCB->ItemIndex]);

        if      (ItemTypeCB->ItemIndex == 0)
         { // ЛПУ
           FType = "l";
           FCode = IntToStr(FDM->LPUCode);
           FStrVal = "ЛПУ";
         }
        else if (ItemTypeCB->ItemIndex == 1)
         { // Участок
           FType = "u";
           FCode = FTempl->CtrList->TemplateItems["00B3"]->GetValue("");
           FStrVal = "Уч: "+FTempl->CtrList->TemplateItems["00B3"]->GetValueStr();
         }
        else if (ItemTypeCB->ItemIndex == 2)
         { // Организация
           FType = "o";
           FCode = FTempl->CtrList->TemplateItems["0025"]->GetValue("");
           FStrVal = FTempl->CtrList->TemplateItems["0025"]->GetValueStr();
         }
        else if (ItemTypeCB->ItemIndex == 3)
         { // Адрес
           FType = "a";
           FCode = FTempl->CtrList->TemplateItems["0136"]->GetValue("");
           FStrVal = FTempl->CtrList->TemplateItems["0136"]->GetValueStr();
         }
        ModalResult = mrOk;
      }
     catch (EkabCustomDataSetError &E)
      {
        _MSG_ERR(E.Message,"Ошибка");
      }
   }
}
//---------------------------------------------------------------------------
bool __fastcall TAddPlanInfForm::CheckInput()
{
  bool RC = false;
  try
   {
     if (FTempXML)
      {
        UnicodeString xValid = "";
        FTempXML->Iterate(GetInput,xValid);
        if (!xValid.Length())
         {
           if (InfCB->ItemIndex == -1)
            _MSG_ERR("Необходимо указать инфекцию.","Ошибка");
           else if (ItemTypeCB->Visible && (ItemTypeCB->ItemIndex == -1))
            _MSG_ERR("Необходимо указать тип подразделения, для которого будет выполняться планирование.","Ошибка");
           else
            RC = true;
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TAddPlanInfForm::GetInput(TTagNode *itTag, UnicodeString &UID)
{
  if (itTag && FTempl)
   {
     if (FTempl->CtrList->GetTemplateControl(itTag->AV["ref"]))
      {
        if (!FTempl->CtrList->TemplateItems[itTag->AV["ref"]]->CheckReqValue())
         {
            UID = "no";
            return true;
         }
      }
   }
  return false;
}
//---------------------------------------------------------------------------

