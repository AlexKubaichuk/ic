object AddPreTestForm: TAddPreTestForm
  Left = 0
  Top = 0
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1088#1077#1076'/'#1087#1086#1089#1090#1074#1072#1082#1094#1080#1085#1072#1083#1100#1085#1091#1102' '#1087#1088#1086#1073#1091
  ClientHeight = 305
  ClientWidth = 370
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 264
    Width = 370
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      370
      41)
    object CancelBtn: TButton
      Left = 266
      Top = 6
      Width = 100
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 0
    end
    object ApplyBtn: TButton
      Left = 160
      Top = 6
      Width = 100
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      Enabled = False
      ModalResult = 1
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 364
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label2: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 100
      Height = 18
      Margins.Top = 6
      Align = alLeft
      Caption = #1048#1085#1092#1077#1082#1094#1080#1103':'
      Constraints.MinWidth = 100
      ExplicitHeight = 13
    end
    object InfCB: TcxComboBox
      AlignWithMargins = True
      Left = 109
      Top = 3
      Align = alClient
      Constraints.MaxWidth = 252
      Properties.DropDownListStyle = lsEditFixedList
      Properties.OnChange = InfCBPropertiesChange
      TabOrder = 0
      Width = 252
    end
  end
  object Panel3: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 102
    Width = 364
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label3: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 150
      Height = 18
      Margins.Top = 6
      Align = alLeft
      Caption = #1047#1085#1072#1095#1077#1085#1080#1077' '#1088#1077#1072#1082#1094#1080#1080':'
      Constraints.MinWidth = 150
      ExplicitHeight = 13
    end
    object ReakED: TcxMaskEdit
      AlignWithMargins = True
      Left = 159
      Top = 3
      Align = alClient
      Constraints.MaxWidth = 202
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d{1,3}'
      Properties.OnChange = InfCBPropertiesChange
      TabOrder = 0
      Width = 202
    end
  end
  object Panel6: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 234
    Width = 364
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object Label6: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 150
      Height = 18
      Margins.Top = 6
      Align = alLeft
      Caption = #1052#1072#1082#1089'. '#1074#1086#1079#1088#1072#1089#1090':'
      Constraints.MinWidth = 150
      ExplicitHeight = 13
    end
    object MaxAgeED: TcxMaskEdit
      AlignWithMargins = True
      Left = 159
      Top = 3
      Align = alClient
      Constraints.MaxWidth = 202
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d?\d?\d\/(0?\d|1?[0-1])\/([0-2]?\d|3?[0-1])'
      Properties.OnChange = InfCBPropertiesChange
      Properties.OnValidate = MinAgeEDPropertiesValidate
      TabOrder = 0
      Width = 202
    end
  end
  object Panel7: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 201
    Width = 364
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object Label7: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 150
      Height = 18
      Margins.Top = 6
      Align = alLeft
      Caption = #1052#1080#1085'. '#1074#1086#1079#1088#1072#1089#1090':'
      Constraints.MinWidth = 150
      ExplicitHeight = 13
    end
    object MinAgeED: TcxMaskEdit
      AlignWithMargins = True
      Left = 159
      Top = 3
      Align = alClient
      Constraints.MaxWidth = 202
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d?\d?\d\/(0?\d|1?[0-1])\/([0-2]?\d|3?[0-1])'
      Properties.OnChange = InfCBPropertiesChange
      Properties.OnValidate = MinAgeEDPropertiesValidate
      TabOrder = 0
      Width = 202
    end
  end
  object Panel8: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 168
    Width = 364
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    object Label8: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 150
      Height = 18
      Margins.Top = 6
      Align = alLeft
      Caption = #1055#1072#1091#1079#1072' ('#1076#1085#1077#1081'):'
      Constraints.MinWidth = 150
      ExplicitHeight = 13
    end
    object PauseED: TcxMaskEdit
      AlignWithMargins = True
      Left = 159
      Top = 3
      Align = alClient
      Constraints.MaxWidth = 202
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d{1,3}'
      Properties.OnChange = InfCBPropertiesChange
      TabOrder = 0
      Width = 202
    end
  end
  object Panel9: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 135
    Width = 364
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 6
    object Label9: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 150
      Height = 18
      Margins.Top = 6
      Align = alLeft
      Caption = #1040#1082#1090#1091#1072#1083#1100#1085#1086#1089#1090#1100' '#1079#1085#1072#1095#1077#1085#1080#1103':'
      Constraints.MinWidth = 150
      ExplicitHeight = 13
    end
    object ActualED: TcxMaskEdit
      AlignWithMargins = True
      Left = 159
      Top = 3
      Align = alClient
      Constraints.MaxWidth = 202
      Properties.MaskKind = emkRegExprEx
      Properties.EditMask = '\d{1,3}'
      Properties.OnChange = InfCBPropertiesChange
      TabOrder = 0
      Width = 202
    end
  end
  object Panel10: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 69
    Width = 364
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 7
    object Label10: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 100
      Height = 18
      Margins.Top = 6
      Align = alLeft
      Caption = #1055#1083#1072#1085#1080#1088#1086#1074#1072#1090#1100':'
      Constraints.MinWidth = 100
      ExplicitHeight = 13
    end
    object PreTestType: TcxComboBox
      AlignWithMargins = True
      Left = 109
      Top = 3
      Align = alClient
      Constraints.MaxWidth = 252
      Properties.DropDownListStyle = lsEditFixedList
      Properties.Items.Strings = (
        #1053#1077' '#1087#1083#1072#1085#1080#1088#1086#1074#1072#1090#1100
        #1055#1083#1072#1085#1080#1088#1086#1074#1072#1090#1100' '#1087#1088#1077#1076#1074#1072#1082#1094#1080#1085#1072#1083#1100#1085#1091#1102' '#1087#1088#1086#1073#1091
        #1055#1083#1072#1085#1080#1088#1086#1074#1072#1090#1100' '#1087#1086#1089#1090#1074#1072#1082#1094#1080#1085#1072#1083#1100#1085#1091#1102' '#1087#1088#1086#1073#1091)
      Properties.OnChange = InfCBPropertiesChange
      TabOrder = 0
      Width = 252
    end
  end
  object Panel11: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 36
    Width = 364
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 8
    object Label11: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 6
      Width = 100
      Height = 18
      Margins.Top = 6
      Align = alLeft
      Caption = #1055#1088#1086#1073#1072':'
      Constraints.MinWidth = 100
      ExplicitHeight = 13
    end
    object TestCB: TcxComboBox
      AlignWithMargins = True
      Left = 109
      Top = 3
      Align = alClient
      Constraints.MaxWidth = 252
      Properties.DropDownListStyle = lsEditFixedList
      Properties.OnChange = InfCBPropertiesChange
      TabOrder = 0
      Width = 252
    end
  end
end
