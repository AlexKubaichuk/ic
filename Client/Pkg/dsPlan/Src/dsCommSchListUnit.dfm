object CommSchListForm: TCommSchListForm
  Left = 470
  Top = 208
  HelpContext = 4008
  ActiveControl = SchemaTL
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1042#1099#1073#1086#1088' '#1089#1093#1077#1084#1099
  ClientHeight = 418
  ClientWidth = 698
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 320
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 381
    Width = 698
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      698
      37)
    object OkBtn: TcxButton
      Left = 530
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = OkBtnClick
    end
    object CancelBtn: TcxButton
      Left = 614
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object SchemaTL: TcxTreeList
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 692
    Height = 375
    BorderStyle = cxcbsNone
    Align = alClient
    Bands = <
      item
      end>
    Navigator.Buttons.CustomButtons = <>
    OptionsBehavior.AlwaysShowEditor = True
    OptionsBehavior.ConfirmDelete = False
    OptionsBehavior.DragCollapse = False
    OptionsBehavior.DragExpand = False
    OptionsBehavior.MultiSort = False
    OptionsBehavior.ShowHourGlass = False
    OptionsCustomizing.BandCustomizing = False
    OptionsCustomizing.BandHorzSizing = False
    OptionsCustomizing.BandMoving = False
    OptionsCustomizing.BandVertSizing = False
    OptionsCustomizing.ColumnCustomizing = False
    OptionsCustomizing.ColumnHorzSizing = False
    OptionsCustomizing.ColumnMoving = False
    OptionsCustomizing.ColumnVertSizing = False
    OptionsData.CancelOnExit = False
    OptionsData.AnsiSort = True
    OptionsData.Deleting = False
    OptionsView.CellAutoHeight = True
    OptionsView.ScrollBars = ssVertical
    OptionsView.ShowEditButtons = ecsbFocused
    OptionsView.ColumnAutoWidth = True
    Styles.OnGetContentStyle = SchemaTLStylesGetContentStyle
    TabOrder = 1
    Data = {
      00000500740100000F00000044617461436F6E74726F6C6C6572310100000012
      000000546378537472696E6756616C75655479706509000000445855464D5401
      445855464D5401445855464D5401445855464D5401445855464D540144585546
      4D5401445855464D5401445855464D5401445855464D54010300000000000000
      1200030000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF010000000800
      0000000000000000FFFFFFFFFFFFFFFFFFFFFFFF020000000800000000000000
      0000FFFFFFFFFFFFFFFFFFFFFFFF0300000008000000000000000000FFFFFFFF
      FFFFFFFFFFFFFFFF040000001200030000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFF0500000008000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF
      0600000008000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF070000000800
      0000000000000000FFFFFFFFFFFFFFFFFFFFFFFF080000000800000000000000
      0000FFFFFFFFFFFFFFFFFFFFFFFF1A0803000000}
    object SchCol: TcxTreeListColumn
      Caption.AlignHorz = taCenter
      Caption.Text = #1057#1093#1077#1084#1072
      DataBinding.ValueType = 'String'
      Options.CellEndEllipsis = False
      Options.Customizing = False
      Options.Editing = False
      Options.Focusing = False
      Options.IncSearch = False
      Options.Moving = False
      Options.ShowEditButtons = eisbNever
      Options.TabStop = False
      Width = 291
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object Style: TcxStyleRepository
    Left = 37
    Top = 14
    PixelsPerInch = 96
    object BoldStyle: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object NoBoldStyle: TcxStyle
    end
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clBtnFace
    end
  end
end
