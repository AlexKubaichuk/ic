//---------------------------------------------------------------------------

#ifndef dsSchEditClientH
#define dsSchEditClientH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
//---------------------------------------------------------------------------
#include "dxBar.hpp"
//#include "ServerClientClasses.h"
#include "XMLContainer.h"
//#include "dsRegEDFunc.h"
//#include "dsRegExtFilterSetTypes.h"
//#include "dsICCardTypeDef.h"
#include "dsSchEditClientUnit.h"
#include "dsICSchTypeDef.h"
//---------------------------------------------------------------------------
class TdsSchEditDM;
//---------------------------------------------------------------------------
class PACKAGE TdsSchEditClient : public TComponent
{
typedef map<TSchemeObjType, TdsSchEditForm*> TSchEditMap;
private:
        TImageList      *FImages;
        TImageList      *FLargeImages;
        TImageList      *FSelImages;
        TImageList      *FSelLargeImages;
        TImageList      *FHotImages;
        TImageList      *FDisabledImages;
        UnicodeString   FFindCaption,FInqCompName,FBuyCaption;
        UnicodeString   FDBUser,FDBPassword,FDBCharset,FDTFormat;
        bool            FActive;
        bool            FADO;
        bool            FFetchAll;
        bool            FRefreshList;
        bool            FCallEvtForInvisebleExtEdit;
        TSchEditMap     FSchEditMap;

        UnicodeString   FCInsertShortCut;
        UnicodeString   FCEditShortCut;
        UnicodeString   FCDeleteShortCut;
        bool            FCInsert;
        bool            FCEdit;
        bool            FCDelete;
        bool            FUseDisableControls;
        bool            FUseRefresh;
        bool            FUseReportSetting;
        bool            FUseQuickFilterSetting;

        bool            FUseTemplate;
        bool            FShowUniqueRec;
        bool            FUseFilter;
        bool            FUseClassDisControls;
        bool            FUseProgressBar;
        bool            FUseUnitGUID;
        bool            FUseClsGUID;
        bool            FUseAfterScroll;
        bool            FUseSCInfPanel;
        bool            FUseLargeImages;
        bool            FExtShortCutInfPanel;
        bool            FClassEnabled;
        bool            FListEditEnabled;
        bool            FOpenCardEnabled;
        bool            FFullEdit,FFindPanel;
        int             FULEditWidth;
        int             FUListTop;
        int             FUListLeft;
        int             FDefaultListWidth;
        int             FDefaultClassWidth;
        TControl        *FInqComp;
        TList           *tmpChTree;
        UnicodeString   FInsCapt;
        UnicodeString   FEditCapt;
        UnicodeString   FDelCapt;
        UnicodeString   FExtCapt;
        UnicodeString   FRegKey;

        UnicodeString   FClKeyName;
        UnicodeString   FDBName;
        UnicodeString   FULCaption;
        UnicodeString   FXMLName;
        TNotifyEvent    FUnitListCreated;
        TNotifyEvent    FUnitListDeleted;
//!!!        TExtLoadXML     FLoadExtXML;

//        TExtBtnClick    FGetExtWhere;
//        TExtBtnClick    FOnCtrlDataChange;
//        TGetExtData     FGetExtData;
//        TClassCallBack  FOnInit;
//        TClassCallBack  FDesOnInit;
//        TRegistryDemoMessageEvent FDemoMsg;
//        TExtEditData    FExtInsertData;
//        TExtEditData    FExtEditData;
//        TExtEditData    FExtEditExtData;
//        TExtEditData    FExtDeleteData;
//        TUnitEvent      FOnAfterInsert;
//        TClassEditEvent FOnClassAfterInsert;
//        TClassEditEvent FOnClassAfterEdit;
//        TClassEditEvent FOnClassAfterDelete;
//        TExtEditDependFieldEvent FExtEditGetDependField;
        TColor          FClassTreeColor;
//        TModifUnitEvent FOnBeforeInsert;
//        TClassEditCreate FClassEditCreate; // ��������� ����������� �.�. 7.10.08
//        TUnitEvent      FOnAfterEdit;
//        TModifUnitEvent FOnBeforeEdit;
//        TUnitEvent      FOnAfterDelete;
//        TModifUnitEvent FOnBeforeDelete;
//        TInqUpdate      FOnInqUpdate;
        TNotifyEvent    FOnListClose;
        TNotifyEvent    FOnBeforeRefresh;
//        TExtValidateData FOnExtValidate;
//        TExtValidateData FOnGetDefValues;
//        TGetCompValue   FGetCompValue;
//        TRegEditButtonClickEvent  FOnRegEditButtonClick;
//        TExtBtnGetBitmapEvent FExtBtnGetBitmap;

//        TRegistryDataEditEvent FSQLAfterInsertEvt;
//        TRegistryDataEditEvent FSQLBeforeEditEvt;
//        TRegistryDataEditEvent FSQLAfterEditEvt;
//        TRegistryDataEditEvent FSQLAfterDeleteEvt;

        TStartDragEvent  FOnUnitStartDrag;
        TDragOverEvent   FOnUnitDragOver;
        TDragDropEvent   FOnUnitDragDrop;
        TEndDragEvent    FOnUnitEndDrag;

//        TExtFilterSetEvent FOnExtFilterSet;


//        TUnitEvent       FQuickFilterClick;

//        TcxGridGetCellStyleEvent FListContentStyleEvent;
//        TRegGetOrderEvent        FOnRegGetOrderEvent;
//        TRegListGetColWidthEvent FOnGetListColumnWidth;

//        TRegGetFormatEvent  FOnGetFormat;

//        TExtEditData __fastcall FGetExtInsertData();
//        void __fastcall FSetExtInsertData(TExtEditData AValue);

//        TExtEditData __fastcall FGetExtEditData();
//        void __fastcall FSetExtEditData(TExtEditData AValue);

//        TClassEditEvent __fastcall FGetClassAfterInsert();
//        void __fastcall FSetClassAfterInsert(TClassEditEvent AValue);

//        TClassEditEvent __fastcall FGetClassAfterEdit();
//        void __fastcall FSetClassAfterEdit(TClassEditEvent AValue);

//        TClassEditEvent __fastcall FGetClassAfterDelete();
//        void __fastcall FSetClassAfterDelete(TClassEditEvent AValue);

//        TUnitEvent __fastcall FGetOnOpenCard();
//        void __fastcall FSetOnOpenCard(TUnitEvent AValue);

//        TGetReportListEvent __fastcall FGetQuickFilterList();
//        void __fastcall FSetQuickFilterList(TGetReportListEvent AValue);

//        TGetReportListEvent __fastcall FGetReportList();
//        void __fastcall FSetReportList(TGetReportListEvent AValue);

//        TUnitEvent __fastcall FGetPrintReportClick();
//        void __fastcall FSetPrintReportClick(TUnitEvent AValue);

//        TdsICClassClient* __fastcall FGetConnection();
//        void __fastcall FSetConnection(TdsICClassClient* AValue);

//        TExtBtnClick __fastcall FGetExtBtnClick();
//        void __fastcall FSetExtBtnClick(TExtBtnClick AValue);

        bool __fastcall FGetNativeStyle();
        void __fastcall FSetNativeStyle(bool AValue);

        TcxLookAndFeelKind __fastcall FGetLFKind();
        void __fastcall FSetLFKind(TcxLookAndFeelKind AValue);

        TcxEditStyleController* __fastcall FGetStyleController();
        void __fastcall FSetStyleController(TcxEditStyleController*AValue);

        TdsGetClassXMLEvent __fastcall FGetOnGetClassXML();
        void __fastcall FSetOnGetClassXML(TdsGetClassXMLEvent AValue);

//        TdsGetCountEvent __fastcall FGetOnGetCount();
//        void __fastcall FSetOnGetCount(TdsGetCountEvent AValue);

//        TdsGetIdListEvent __fastcall FGetOnGetIdList();
//        void __fastcall FSetOnGetIdList(TdsGetIdListEvent AValue);

        TdsGetValByIdEvent __fastcall FGetOnGetValById();
        void __fastcall FSetOnGetValById(TdsGetValByIdEvent AValue);

        TdsGetValById10Event __fastcall FGetOnGetValById10();
        void __fastcall FSetOnGetValById10(TdsGetValById10Event AValue);

//        TdsFindEvent __fastcall FGetOnFind();
//        void __fastcall FSetOnFind(TdsFindEvent AValue);

//        TdsInsertDataEvent __fastcall FGetOnInsertData();
//        void __fastcall FSetOnInsertData(TdsInsertDataEvent AValue);

//        TdsEditDataEvent __fastcall FGetOnEditData();
//        void __fastcall FSetOnEditData(TdsEditDataEvent AValue);

//        TdsDeleteDataEvent __fastcall FGetOnDeleteData();
//        void __fastcall FSetOnDeleteData(TdsDeleteDataEvent AValue);

        __int64 __fastcall FGetLPUCode();
        void __fastcall FSetLPUCode(__int64 AValue);

//        TExtValidateData __fastcall FGetOnGetDefValues();
//        void __fastcall FSetOnGetDefValues(TExtValidateData AValue);

        TOnGetPlanOptsEvent __fastcall FGetOnGetPlanOpts();
        void __fastcall FSetOnGetPlanOpts(TOnGetPlanOptsEvent AValue);

        TICSNomenklator* __fastcall FGetNom();
        void __fastcall FSetNom(TICSNomenklator* AValue);

        TdsNomCallDefProcEvent __fastcall FGetOnCallDefProc();
        void __fastcall FSetOnCallDefProc(TdsNomCallDefProcEvent AValue);

  UnicodeString __fastcall FGetXMLPath();
  void __fastcall FSetXMLPath(UnicodeString AVal);

  TOnSaveSchemeEvent __fastcall FGetOnSaveXML();
  void __fastcall FSetOnSaveXML(TOnSaveSchemeEvent AVal);

  TdsGetListByIdEvent __fastcall FGetOnGetListById();
  void __fastcall FSetOnGetListById(TdsGetListByIdEvent AVal);

  TColor __fastcall FGetReqColor();
  void __fastcall FSetReqColor(TColor AVal);

        void __fastcall FSetActive(bool AActive);
        void __fastcall FSetClassEnabled(bool AClassEnabled);
        void __fastcall FSetListEditEnabled(bool AListEditEnabled);
        void __fastcall FSetOpenCardEnabled(bool AOpenCardEnabled);
        void __fastcall FSetFullEdit(bool AFullEdit);
        void __fastcall FSetUListTop(int ATop);
        void __fastcall FSetUListLeft(int ALeft);
        void __fastcall FSetDBName(UnicodeString  ADBName);
        void __fastcall FSetXMLName(UnicodeString  AXMLName);
        void __fastcall FSetInqComp(TControl *AInqComp);
        void __fastcall FSetInit(int APersent, UnicodeString AMess);
        void __fastcall FSetDTFormat(UnicodeString  AParam);
  TAxeXMLContainer* __fastcall FGetXMLList();
  void __fastcall FSetXMLList(TAxeXMLContainer *AVal);
protected:
        TObject   *PrF;
        TdsSchEditDM     *FDM;


        bool           __fastcall FGetClassEditable();
        void           __fastcall FSetClassEditable(bool AVal);
        UnicodeString  __fastcall FGetClassShowExpanded();
        void           __fastcall FSetClassShowExpanded(UnicodeString AVal);

public:
        TAnsiStrMap  HelpContextList;
  void __fastcall ShowSch(TSchemeObjType AType);
//        void       __fastcall GetSelectedUnits(TStrings *ADest);

//        bool       __fastcall GetSelected(__int64 *ACode, UnicodeString &AUnitStr);
//        TdxBar*    __fastcall GetListExtTB(void);

//  bool __fastcall PlanPrivInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData *ATmplData);
//  bool __fastcall PlanTestInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData *ATmplData);
//  bool __fastcall PlanCheckInsert(__int64 AUCode, TDate APatBirthday, TdsRegTemplateData *ATmplData);

        __fastcall TdsSchEditClient(TComponent* Owner);
        __fastcall ~TdsSchEditClient();
__published:

//        __property TdsICClassClient   *Connection = {read=FGetConnection, write=FSetConnection};
//        __property TRegGetFormatEvent  OnGetFormat  = {read=FOnGetFormat, write=FOnGetFormat};

        __property bool NativeStyle                        = {read=FGetNativeStyle,    write=FSetNativeStyle};
        __property TcxLookAndFeelKind LFKind               = {read=FGetLFKind,         write=FSetLFKind};
        __property TcxEditStyleController *StyleController = {read=FGetStyleController,write=FSetStyleController};

//        __property TUnitEvent OnPrintReportClick    = {read=FGetPrintReportClick, write=FSetPrintReportClick};
//        __property TGetReportListEvent OnReportList    = {read=FGetReportList, write=FSetReportList};

//        __property TUnitEvent OnQuickFilterClick    = {read=FQuickFilterClick, write=FQuickFilterClick};
//        __property TGetReportListEvent OnQuickFilterList    = {read=FGetQuickFilterList, write=FSetQuickFilterList};

        __property bool UseReportSetting  = {read=FUseReportSetting, write=FUseReportSetting};
        __property bool UseQuickFilterSetting  = {read=FUseQuickFilterSetting, write=FUseQuickFilterSetting};

        __property bool UseDisableControls  = {read=FUseDisableControls, write=FUseDisableControls};
        __property bool UseUnitGUID  = {read=FUseUnitGUID, write=FUseUnitGUID};
        __property bool UseClsGUID  = {read=FUseClsGUID, write=FUseClsGUID};

        __property bool CallEvtForInvisebleExtEdit  = {read=FCallEvtForInvisebleExtEdit, write=FCallEvtForInvisebleExtEdit};

//        __property TRegEditButtonClickEvent  OnRegEditButtonClick  = {read=FOnRegEditButtonClick, write=FOnRegEditButtonClick};
//        __property TExtBtnGetBitmapEvent  OnGetExtBtnGetBitmap  = {read=FExtBtnGetBitmap, write=FExtBtnGetBitmap};

        __property int  DefaultListWidth  = {read=FDefaultListWidth, write=FDefaultListWidth};
        __property int  DefaultClassWidth  = {read=FDefaultClassWidth, write=FDefaultClassWidth};

//        __property TGetExtData OnGetExtData    = {read=FGetExtData, write=FGetExtData};
//        __property TExtBtnClick OnExtBtnClick  = {read=FGetExtBtnClick, write=FSetExtBtnClick};

//        __property TRegistryDemoMessageEvent OnDemoMessage  = {read=FDemoMsg, write=FDemoMsg};
//        __property TExtBtnClick OnGetExtWhere = {read=FGetExtWhere, write=FGetExtWhere};
//        __property TGetCompValue OnGetCompValue = {read=FGetCompValue, write=FGetCompValue};

//        __property TExtBtnClick OnCtrlDataChange    = {read=FOnCtrlDataChange, write=FOnCtrlDataChange};

//        __property TExtEditData OnExtInsertData   = {read=FGetExtInsertData, write=FSetExtInsertData};
//        __property TExtEditData OnExtEditData     = {read=FGetExtEditData, write=FSetExtEditData};

//        __property TExtEditData OnExtEditExtData     = {read=FExtEditExtData, write=FExtEditExtData};
//        __property TExtEditData OnExtDeleteData   = {read=FExtDeleteData, write=FExtDeleteData};

//        __property TExtFilterSetEvent OnExtFilterSet   = {read=FOnExtFilterSet, write=FOnExtFilterSet};


        __property TStartDragEvent  OnUnitStartDrag = {read=FOnUnitStartDrag, write=FOnUnitStartDrag};
        __property TDragOverEvent   OnUnitDragOver = {read=FOnUnitDragOver, write=FOnUnitDragOver};
        __property TDragDropEvent   OnUnitDragDrop = {read=FOnUnitDragDrop, write=FOnUnitDragDrop};
        __property TEndDragEvent    OnUnitEndDrag = {read=FOnUnitEndDrag, write=FOnUnitEndDrag};

        // ��������� 7.10.08 ��������� �.�.
        // ������� ������������ �� �������� ����� �������������� ��������������
        // � �������� ������ ����������� ����� -->
//        __property TClassEditCreate OnAfterClassEditCreate = {read=FClassEditCreate, write=FClassEditCreate};
        // <--

//!!!        __property TExtLoadXML OnLoadXML   = {read=FLoadExtXML, write=FLoadExtXML};


        __property bool UseTemplate        = {read=FUseTemplate,write=FUseTemplate};
        __property bool UseFilter        = {read=FUseFilter,write=FUseFilter};
        __property bool UseProgressBar  = {read=FUseProgressBar,write=FUseProgressBar};

        __property bool FindPanel        = {read=FFindPanel,write=FFindPanel};
        __property bool ShortCutInfPanel = {read=FUseSCInfPanel,write=FUseSCInfPanel};
        __property bool ExtShortCutInfPanel = {read=FExtShortCutInfPanel,write=FExtShortCutInfPanel};

        __property bool InsertBtnEnabled   = {read=FCInsert,write=FCInsert};
        __property bool EditBtnEnabled     = {read=FCEdit,write=FCEdit};
        __property bool DeleteBtnEnabled   = {read=FCDelete,write=FCDelete};

        __property UnicodeString InsertShortCut  = {read=FCInsertShortCut,write=FCInsertShortCut};
        __property UnicodeString EditShortCut    = {read=FCEditShortCut,write=FCEditShortCut};
        __property UnicodeString DeleteShortCut  = {read=FCDeleteShortCut,write=FCDeleteShortCut};

        __property bool FullEdit        = {read=FFullEdit,write=FSetFullEdit};
        __property bool ClassEditable   = {read=FGetClassEditable,write=FSetClassEditable};
        __property bool RefreshList     = {read=FRefreshList,write=FRefreshList};

        __property bool UseClassDisControls   = {read=FUseClassDisControls,write=FUseClassDisControls};
        __property bool ShowUniqueRec         = {read=FShowUniqueRec,write=FShowUniqueRec};

        __property bool ClassEnabled    = {read=FClassEnabled,write=FSetClassEnabled};
        __property bool ListEditEnabled = {read=FListEditEnabled,write=FSetListEditEnabled};
        __property bool OpenCardEnabled = {read=FOpenCardEnabled,write=FSetOpenCardEnabled};
        __property bool ADOSource       = {read=FADO,write=FADO};
        __property bool Active          = {read=FActive,write=FSetActive};
//        __property bool LargeIcons      = {read=FLargeIcons,write=FLargeIcons};
        __property bool FetchAll        = {read=FFetchAll,write=FFetchAll};
        __property bool UseAfterScroll  = {read=FUseAfterScroll,write=FUseAfterScroll};
        __property bool UseRefresh  = {read=FUseRefresh,write=FUseRefresh};
        __property bool UseLargeImages  = {read=FUseLargeImages,write=FUseLargeImages};

//        __property TcxGridGetCellStyleEvent OnGetListContentStyle = {read=FListContentStyleEvent,write=FListContentStyleEvent};
//        __property TRegGetOrderEvent        OnGetOrder = {read=FOnRegGetOrderEvent, write=FOnRegGetOrderEvent};
//        __property TRegListGetColWidthEvent OnGetListColumnWidth = {read=FOnGetListColumnWidth, write=FOnGetListColumnWidth};

        __property int ULEditWidth      = {read=FULEditWidth,write=FULEditWidth};
        __property int UnitListTop      = {read=FUListTop,write=FSetUListTop, default=0};
        __property int UnitListLeft     = {read=FUListLeft,write=FSetUListLeft, default=0};

//        __property TClassEditEvent OnClassAfterInsert = {read=FGetClassAfterInsert,write=FSetClassAfterInsert};
//        __property TClassEditEvent OnClassAfterEdit   = {read=FGetClassAfterEdit,write=FSetClassAfterEdit};
//        __property TClassEditEvent OnClassAfterDelete = {read=FGetClassAfterDelete,write=FSetClassAfterDelete};

        __property UnicodeString FindCaption    = {read=FFindCaption,write=FFindCaption};

        __property UnicodeString DBName    = {read=FDBName,write=FSetDBName};
        __property UnicodeString PrefixCaption = {read=FULCaption,write=FULCaption};
        __property UnicodeString XML_AV1_Name  = {read=FXMLName,write=FSetXMLName};
//        __property TClassCallBack OnInit    = {read=FOnInit,write=FOnInit};

        __property UnicodeString RegistryKey = {read=FRegKey,write=FRegKey};
        __property UnicodeString ClassShowExpanded = {read=FGetClassShowExpanded,write=FSetClassShowExpanded};
        __property UnicodeString InsertBtnCaption = {read=FInsCapt,write=FInsCapt};
        __property UnicodeString EditBtnCaption   = {read=FEditCapt,write=FEditCapt};
        __property UnicodeString DeleteBtnCaption = {read=FDelCapt,write=FDelCapt};
        __property UnicodeString ExtBtnCaption    = {read=FExtCapt,write=FExtCapt};
        __property UnicodeString DTFormat         = {read=FDTFormat,write=FSetDTFormat};

//        __property TModifUnitEvent OnBeforeInsert = {read=FOnBeforeInsert,write=FOnBeforeInsert};
//        __property TModifUnitEvent OnBeforeEdit   = {read=FOnBeforeEdit,write=FOnBeforeEdit};
//        __property TModifUnitEvent OnBeforeDelete = {read=FOnBeforeDelete,write=FOnBeforeDelete};

        __property TColor RequiredColor  = {read=FGetReqColor,write=FSetReqColor};
        __property TColor ClassTreeColor = {read=FClassTreeColor,write=FClassTreeColor};

//        __property TExtEditDependFieldEvent OnExtEditGetDependField = {read=FExtEditGetDependField,write=FExtEditGetDependField};

//        __property TExtValidateData OnExtValidate = {read=FOnExtValidate,write=FOnExtValidate};
//        __property TExtValidateData OnGetDefValues = {read=FGetOnGetDefValues,write=FSetOnGetDefValues};


//        __property TUnitEvent OnAfterInsert = {read=FOnAfterInsert,write=FOnAfterInsert};
//        __property TUnitEvent OnAfterEdit   = {read=FOnAfterEdit,write=FOnAfterEdit};
//        __property TUnitEvent OnAfterDelete = {read=FOnAfterDelete,write=FOnAfterDelete};

//        __property TUnitEvent OnOpenCard        = {read=FGetOnOpenCard,write=FSetOnOpenCard};
//        __property TInqUpdate OnGetInqData      = {read=FOnInqUpdate,write=FOnInqUpdate};
        __property TNotifyEvent OnUnitListClose = {read=FOnListClose,write=FOnListClose};
        __property TNotifyEvent OnBeforeRefresh = {read=FOnBeforeRefresh,write=FOnBeforeRefresh};

        __property TNotifyEvent AfterUnitListCreate = {read=FUnitListCreated,write=FUnitListCreated};
        __property TNotifyEvent BeforeUnitListDeleted = {read=FUnitListDeleted,write=FUnitListDeleted};

        __property TControl* InqComponent       = {read=FInqComp,write=FSetInqComp};
        __property UnicodeString InqComponentName  = {read=FInqCompName,write=FInqCompName};

        __property UnicodeString BuyCaption  = {read=FBuyCaption,write=FBuyCaption};

        __property UnicodeString DBUser  = {read=FDBUser,write=FDBUser};
        __property UnicodeString DBPassword  = {read=FDBPassword,write=FDBPassword};
        __property UnicodeString DBCharset  = {read=FDBCharset,write=FDBCharset};

        __property TImageList* Images  = {read=FImages,write=FImages};
        __property TImageList* LargeImages  = {read=FLargeImages,write=FLargeImages};
        __property TImageList* SelImages  = {read=FSelImages,write=FSelImages};
        __property TImageList* SelLargeImages  = {read=FSelLargeImages,write=FSelLargeImages};

        __property TImageList* HotImages  = {read=FHotImages,write=FHotImages};
        __property TImageList* DisabledImages  = {read=FDisabledImages,write=FDisabledImages};

        __property TdsGetClassXMLEvent OnGetClassXML  = {read=FGetOnGetClassXML,write=FSetOnGetClassXML};
//        __property TdsGetCountEvent OnGetCount  = {read=FGetOnGetCount,write=FSetOnGetCount};
//        __property TdsGetIdListEvent OnGetIdList  = {read=FGetOnGetIdList,write=FSetOnGetIdList};
        __property TdsGetValByIdEvent OnGetValById  = {read=FGetOnGetValById,write=FSetOnGetValById};
        __property TdsGetValById10Event OnGetValById10  = {read=FGetOnGetValById10,write=FSetOnGetValById10};
//        __property TdsFindEvent OnFind  = {read=FGetOnFind,write=FSetOnFind};
//        __property TdsInsertDataEvent OnInsertData  = {read=FGetOnInsertData,write=FSetOnInsertData};
//        __property TdsEditDataEvent OnEditData  = {read=FGetOnEditData,write=FSetOnEditData};
//        __property TdsDeleteDataEvent OnDeleteData  = {read=FGetOnDeleteData,write=FSetOnDeleteData};

  __property TAxeXMLContainer *XMLList = {read=FGetXMLList, write=FSetXMLList};
  void __fastcall PreloadData();
  __property __int64 LPUCode = {read=FGetLPUCode, write=FSetLPUCode};
  __property TOnGetPlanOptsEvent OnGetPlanOpts = {read=FGetOnGetPlanOpts, write=FSetOnGetPlanOpts};
  __property TICSNomenklator*  Nom                = {read=FGetNom, write=FSetNom};
  __property TdsNomCallDefProcEvent OnCallDefProc = {read=FGetOnCallDefProc, write=FSetOnCallDefProc};
  __property UnicodeString       XMLPath = {read=FGetXMLPath, write=FSetXMLPath};
  __property TOnSaveSchemeEvent OnSaveXML = {read=FGetOnSaveXML, write=FSetOnSaveXML};
  __property TdsGetListByIdEvent      OnGetListById  = {read=FGetOnGetListById,write=FSetOnGetListById};
};
//---------------------------------------------------------------------------
#endif

