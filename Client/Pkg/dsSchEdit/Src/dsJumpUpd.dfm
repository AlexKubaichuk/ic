object fmJumpUpd: TfmJumpUpd
  Left = 517
  Top = 304
  BorderStyle = bsDialog
  BorderWidth = 5
  Caption = 'fmJumpUpd'
  ClientHeight = 243
  ClientWidth = 409
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gbClient: TGroupBox
    Left = 0
    Top = 0
    Width = 409
    Height = 212
    Align = alClient
    TabOrder = 0
    ExplicitHeight = 184
    object pTop: TPanel
      Left = 2
      Top = 15
      Width = 405
      Height = 37
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        405
        37)
      object lbActualTimeTitle: TLabel
        Left = 8
        Top = 4
        Width = 199
        Height = 13
        Caption = #1042#1088#1077#1084#1103' '#1076#1077#1081#1089#1090#1074#1080#1103' '#1087#1088#1080#1074#1080#1074#1082#1080' ('#1075#1075#1075'/'#1084#1084'/'#1076#1076'):'
      end
      object Bevel: TBevel
        Left = 8
        Top = 29
        Width = 389
        Height = 2
        Anchors = [akLeft, akTop, akRight]
      end
      object lbFilterTitle: TLabel
        Left = 3
        Top = 3
        Width = 125
        Height = 13
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1092#1080#1083#1100#1090#1088#1072':'
      end
      object edActualTime: TEdit
        Left = 216
        Top = 0
        Width = 73
        Height = 21
        Color = clInfoBk
        TabOrder = 0
      end
      object cbFilter: TcxComboBox
        Left = 134
        Top = 0
        Properties.DropDownListStyle = lsFixedList
        TabOrder = 1
        Width = 228
      end
      object btnConditionListEdit: TcxButton
        Left = 362
        Top = 0
        Width = 21
        Height = 21
        OptionsImage.Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF000000FF00B4B4FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF000000FF000000FF000000FF00B4B4FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF000000FF000000FF000000FF000000FF00B4B4FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF000000FF000000FF000000FF00B4B4FF000000FF000000FF00B4B4FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
          FF000000FF000000FF00B4B4FF0000000000FF00FF000000FF000000FF00B4B4
          FF00000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF000000
          00000000FF00B4B4FF0000FFFF0000000000FF00FF00FF00FF000000FF00B4B4
          FF0000FFFF0000FFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF000000
          000000000000000000000000000000000000FF00FF00FF00FF00000000000000
          FF00B4B4FF000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00FF000000
          00000000FF00B4B4FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0000000000FF00FF00FF00FF0000000000FF00
          FF00FF00FF000000FF00B4B4FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF000000FF00B4B4FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0000000000000000000000000000000000000000000000
          0000FF00FF00FF00FF00FF00FF000000FF00B4B4FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF000000000000FFFF0000FFFF0000FFFF0000FFFF000000
          0000FF00FF00FF00FF00FF00FF00FF00FF000000FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0000000000000000000000000000000000000000000000
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        TabOrder = 2
        OnClick = btnConditionListEditClick
      end
    end
    object pRefs: TPanel
      Left = 2
      Top = 52
      Width = 405
      Height = 158
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitHeight = 130
      object lbRefListTitle: TLabel
        Left = 8
        Top = 0
        Width = 54
        Height = 13
        Caption = #1055#1077#1088#1077#1093#1086#1076#1099':'
      end
      object vlRefList: TValueListEditor
        Left = 8
        Top = 16
        Width = 306
        Height = 138
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goRowSelect, goThumbTracking]
        PopupMenu = pmRefList
        TabOrder = 0
        TitleCaptions.Strings = (
          #1042#1072#1082#1094#1080#1085#1072#1094#1080#1103' '#1086#1090
          #1055#1077#1088#1077#1093#1086#1076)
        OnDblClick = vlDblClick
        OnDrawCell = vlDrawCell
        OnKeyDown = vlKeyDown
        OnMouseDown = vlMouseDown
        OnMouseWheelDown = vlMouseWheelDown
        OnMouseWheelUp = vlMouseWheelUp
        ColWidths = (
          141
          159)
      end
      object bEdit: TcxButton
        Left = 322
        Top = 16
        Width = 75
        Height = 21
        Action = actEditRef
        TabOrder = 1
      end
    end
  end
  object pBottom: TPanel
    Left = 0
    Top = 212
    Width = 409
    Height = 31
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 184
    object pCtrls: TPanel
      Left = 177
      Top = 0
      Width = 232
      Height = 31
      Align = alRight
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 0
      object bHelp: TcxButton
        Left = 157
        Top = 6
        Width = 75
        Height = 25
        Hint = #1048#1079#1084#1077#1085#1080#1090#1100' '#1089#1089#1099#1083#1082#1091
        Caption = '&'#1055#1086#1084#1086#1097#1100
        TabOrder = 0
        OnClick = bHelpClick
      end
      object bCancel: TcxButton
        Left = 79
        Top = 6
        Width = 75
        Height = 25
        Hint = #1048#1079#1084#1077#1085#1080#1090#1100' '#1089#1089#1099#1083#1082#1091
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
        OnClick = bCancelClick
      end
      object bApply: TcxButton
        Left = 0
        Top = 6
        Width = 75
        Height = 25
        Hint = #1048#1079#1084#1077#1085#1080#1090#1100' '#1089#1089#1099#1083#1082#1091
        Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
        TabOrder = 2
        OnClick = bApplyClick
      end
    end
  end
  object ActionList: TActionList
    Left = 48
    Top = 136
    object actEditRef: TAction
      Caption = '&'#1048#1079#1084#1077#1085#1080#1090#1100
      HelpType = htContext
      Hint = #1048#1079#1084#1077#1085#1080#1090#1100
      OnExecute = actEditRefExecute
    end
  end
  object pmRefList: TPopupMenu
    Left = 16
    Top = 136
    object N1: TMenuItem
      Action = actEditRef
      Default = True
    end
  end
end
