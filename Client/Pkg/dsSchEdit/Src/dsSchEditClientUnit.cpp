//---------------------------------------------------------------------------
#include <vcl.h>
#include <SysUtils.hpp>
#pragma hdrstop

#include "dsSchEditClientUnit.h"
#include "dsSchUpd.h"
#include "dsCopiedSchName.h"
#include "dsPrivivUpd.h"
#include "dsSchCondList.h"
#include "dsAgeMIBPList.h"

#include "msgdef.h"
//#include "dsCardEditClientUnit.h"
//#include "dsCardSchListUnit.h"

//#include "pkgsetting.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

#pragma link "cxClasses"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxGraphics"
#pragma link "cxImageComboBox"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxPC"
#pragma link "cxSplitter"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxBarExtItems"
#pragma link "dxRibbon"
#pragma link "dxRibbonCustomizationForm"
#pragma link "dxRibbonSkins"
#pragma link "dxStatusBar"
#pragma link "Htmlview"
#pragma link "cxCheckBox"
#pragma link "cxContainer"
#pragma link "cxEdit"
#pragma link "cxMaskEdit"
#pragma resource "*.dfm"
TdsSchEditForm * dsSchEditForm;
//---------------------------------------------------------------------------
__fastcall TdsSchEditForm::TdsSchEditForm(TComponent * Owner, TdsSchEditDM * ADM, TSchemeObjType AType)
    : TForm(Owner),
    FIMM_S(NULL),
    XMLModified(false),
    FIMM_SModified(false)
 {
  FDM                   = ADM;
  FSchemeObjType        = AType;
  actMIBPByAge->Enabled = false;
  actMIBPByAge->Visible = false;
  switch (FSchemeObjType)
   {
   case TSchemeObjType::Vac:
    FSchGUI = "12063611-00008CD7-CD89";
    MIBPCol->Caption->Text = "�������� / �������"; /*ItemsGB-> */
    actMIBPByAge->Enabled  = true;
    actMIBPByAge->Visible  = true;
    break;
   case TSchemeObjType::Test:
    FSchGUI = "001D3500-00005882-DC28";
    MIBPCol->Caption->Text = "�������� / �����"; /*ItemsGB-> */
    break;
   case TSchemeObjType::Check:
    FSchGUI = "569E1EB2-D0CFEF0F-BE9A";
    MIBPCol->Caption->Text = "�������� / ��������"; /*ItemsGB-> */
    break;
   }
  FNmklGUI = "0E291426-00005882-2493";
  FCurVacCode = -1;
  FCurVacName = "";
  FCurInfCode = -1;
  FCurInfName = "";
  OpenEditor();
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::FormDestroy(TObject * Sender)
 {
  //if (quClass()->Active) quClass()->Close();
  //if (quClass()->Transaction->Active) quClass()->Transaction->Rollback();
 }
//---------------------------------------------------------------------------
bool __fastcall TdsSchEditForm::FSetEdit(TTagNode * itTag, UnicodeString & UID)
 {
  if (itTag->GetAttrByName("isedit"))
   itTag->AV["isedit"] = "1";
  return false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::FGetDataProgress(int AMax, int ACur, UnicodeString  AMsg)
 {
  if (AMax == -1)
   StatusSB->Panels->Items[1]->Text = "";
  else if (!(AMax + ACur))
   StatusSB->Panels->Items[1]->Text = AMsg;
  else
   StatusSB->Panels->Items[1]->Text = AMsg + " [" + IntToStr(ACur) + "/" + IntToStr(AMax) + "]";
  Application->ProcessMessages();
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::SchSettingTVStylesGetContentStyle(TcxCustomTreeList * Sender,
    TcxTreeListColumn * AColumn, TcxTreeListNode * ANode, TcxStyle *& AStyle)
 {
  if (AColumn && ANode)
   {
    if (SameText(ANode->Texts[0], csEndLineLabel) || SameText(ANode->Texts[0], csBeginLineLabel))
     AStyle = HeadStyle;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::actAddSchemeExecute(TObject * Sender)
 {
  if (CanEdit())
   {
    UnicodeString FSchName = "";
    if (ShowModalSchUpd(this, fumADD, FCurVacCode, FSchName) == mrOk)
     AddScheme(FSchName, FCurVacCode);
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::actEditSchemeExecute(TObject * Sender)
 {
  if (CanEdit())
   {
    UnicodeString FSchName = FCurSchName;
    if (ShowModalSchUpd(this, fumEDIT, FCurVacCode, FSchName) == mrOk)
     EditScheme(FSchName, FCurVacCode);
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::actCopySchemeExecute(TObject * Sender)
 {
  if (CanEdit())
   {
    TTagNode * ActiveSchemeNode = GetActiveSchemeNode();
    if (ActiveSchemeNode)
     {
      //����������� ����� ��� ����������� ��� ����������� �����
      UnicodeString CopiedSchName = ShowModalCopiedSchName(
          this,
          ActiveSchemeNode->AV["name"] + "_1"
          );
      if (CopiedSchName != "")
       {
        TTagNode * CopiedScheme = ActiveSchemeNode->GetParent()->AddChild();
        TAnsiStrMap UpdateUIDTable = CopiedScheme->AssignEx(
            ActiveSchemeNode,
            true
            );
        CopiedScheme->AV["name"] = CopiedSchName;
        CopiedScheme->_Iterate(UpdateInternalRefs, & UpdateUIDTable);
        XMLModified = true;
        AddScheme(CopiedSchName, GetCurVakId(), CopiedScheme);
       }
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::actDeleteSchemeExecute(TObject * Sender)
 {
  if (CanEdit())
   {
    if (_MSG_QUE("�� ������� � ���, ��� ������ ������� ��������� �����?",
        "������������� ��������") == IDYES)
     {
      switch (CanDeleteCurScheme())
       {
       case crONLY_ONE_SCHEME:
        switch (FSchemeObjType)
         {
         case TSchemeObjType::Vac:
          _MSG_ERR("������ ������� ������������ ����� ��� ����.", "������");
          break;
         case TSchemeObjType::Test:
          _MSG_ERR("������ ������� ������������ ����� ��� �����.", "������");
          break;
         case TSchemeObjType::Check:
          _MSG_ERR("������ ������� ������������ ����� ��� ��������.", "������");
          break;
         }
        break;
       case crCUR_SCHEME_IS_START:
        _MSG_ERR("������ ������� ��������� �����.", "������");
        break;
       case crCUR_SCHEME_LINES_REFS_EXIST:
        _MSG_ERR("��������� ����� ������ �������, ��������� �� ��� ������� ������.", "������");
        break;
       case crCUR_SCHEME_LINES_DBREFS_EXIST:
        switch (FSchemeObjType)
         {
         case TSchemeObjType::Vac:
          _MSG_ERR("��������� ����� ������ �������, ��������� ������� �������� ��������������� �����.", "������");
          break;
         case TSchemeObjType::Test:
          _MSG_ERR("��������� ����� ������ �������, ��������� ������� ����� ��������������� �����.", "������");
          break;
         case TSchemeObjType::Check:
          _MSG_ERR("��������� ����� ������ �������, ��������� ��������� �������� ��������������� �����.", "������");
          break;
         }

        break;
       case crCAN_DELETE_SCHEME:
        DeleteActiveScheme();
        break;
       }
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::actAddPrivivExecute(TObject * Sender)
 {
  if (CanEdit())
   {
    TStrings * PrivivTypes = new TStringList;
    if (PrivivTypes)
     {
      bool ValidTypesRes;
      switch (FSchemeObjType)
       {
       case TSchemeObjType::Vac:
        ValidTypesRes = GetValidPrivivTypes(PrivivTypes);
        break;
       case TSchemeObjType::Test:
        ValidTypesRes = GetValidProbTypes(PrivivTypes);
        break;
       case TSchemeObjType::Check:
        ValidTypesRes = GetValidProbTypes(PrivivTypes); //!!!
        break;
       }

      if (ValidTypesRes)
       ShowModalPrivivUpd(this, FDM, fumADD, pmLine, CurrSchIsMonoVac(), PrivivTypes);
      else
       switch (FSchemeObjType)
        {
        case TSchemeObjType::Vac:
         _MSG_ERR("��� ��������� ����� ������ ������ ��������� ���� ��������.", "������");
         break;
        case TSchemeObjType::Test:
         _MSG_ERR("��� ��������� ����� ������ ������ ��������� ���� ����.", "������");
         break;
        case TSchemeObjType::Check:
         _MSG_ERR("��� ��������� ����� ������ ������ ��������� ���� ��������.", "������");
         break;
        }
      delete PrivivTypes;
     }
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TdsSchEditForm::CurrSchIsMonoVac()
 {
  bool RC = false;
  TStringList * VacInfs = new TStringList;
  try
   {
    if (FSchemeObjType == TSchemeObjType::Vac)
     {
      TTagNode * SchemeNode = GetActiveSchemeNode();
      if (SchemeNode)
       {
        FDM->FillVacInfList(VacInfs, SchemeNode->AV["ref"].ToIntDef(0));
        RC = (VacInfs->Count == 1);
       }
     }
   }
  __finally
   {
    delete VacInfs;
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::actEditPrivivExecute(TObject * Sender)
 {
  TTagNode * FNode = GetCurSchLineData();
  if (FNode)
   {
    if (!FNode->CmpName("begline"))
     {
      TPrivivUpdMode AMode = (FNode->CmpName("line")) ? pmLine : pmEndLine;
      ShowModalPrivivUpd(this, FDM, fumEDIT, AMode, CurrSchIsMonoVac());
     }
   }
 }
//---------------------------------------------------------------------------
TcxTreeListNode * __fastcall TdsSchEditForm::GetActiveSch()
 {
  TcxTreeListNode * RC = NULL;
  try
   {
    if (SchListTL->SelectionCount)
     RC = SchListTL->Selections[0];
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
TcxTreeListNode * __fastcall TdsSchEditForm::GetCurSchLine()
 {
  TcxTreeListNode * RC = NULL;
  try
   {
    if (SchTV->SelectionCount)
     RC = SchTV->Selections[0];
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
TTagNode * __fastcall TdsSchEditForm::GetCurSchLineData()
 {
  TTagNode * RC = NULL;
  try
   {
    TcxTreeListNode * FNode = GetCurSchLine();
    if (FNode)
     RC = (TTagNode *)FNode->Data;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::actDeletePrivivExecute(TObject * Sender)
 {
  if (CanEdit())
   {
    UnicodeString Msg;
    switch (FSchemeObjType)
     {
     case TSchemeObjType::Vac:
      Msg = "�� ������� � ���, ��� ������ ������� ��������� ��� ��������?";
      break;
     case TSchemeObjType::Test:
      Msg = "�� ������� � ���, ��� ������ ������� ��������� ��� �����?";
      break;
     case TSchemeObjType::Check:
      Msg = "�� ������� � ���, ��� ������ ������� ��������� ��� ��������?";
      break;
     }
    if (GetCurSchLine()/*SchTV->ItemIndex != -1*/)
     if (_MSG_QUE(Msg, "������������� ��������") == IDYES)
      switch (CanDeleteActivePriviv())
       {
       case crEND_LINE:
        _MSG_ERR("������ ������� ��������� �����.", "������");
        break;
       case crNOT_GR_END_PRIVIV:
        switch (FSchemeObjType)
         {
         case TSchemeObjType::Vac:
          _MSG_ERR("������� ����� ������ ��������� ��� �������� ������ \"V\" �\\��� ������ \"RV\".", "������");
          break;
         case TSchemeObjType::Test:
          _MSG_ERR("������� ����� ������ ��������� ��� �����.", "������");
          break;
         case TSchemeObjType::Check:
          _MSG_ERR("������� ����� ������ ��������� ��� ��������.", "������");
          break;
         }
        break;
       case crCUR_PRIVIV_REFS_EXIST:
        switch (FSchemeObjType)
         {
         case TSchemeObjType::Vac:
          _MSG_ERR("��������� ��� �������� ������ �������, ��������� �� ���� ������� ������.", "������");
          break;
         case TSchemeObjType::Test:
          _MSG_ERR("��������� ��� ����� ������ �������, ��������� �� ���� ������� ������.", "������");
          break;
         case TSchemeObjType::Check:
          _MSG_ERR("��������� ��� �������� ������ �������, ��������� �� ���� ������� ������.", "������");
          break;
         }
        break;
       case crCUR_PRIVIV_DBREFS_EXIST:
        switch (FSchemeObjType)
         {
         case TSchemeObjType::Vac:
          _MSG_ERR("��������� ��� �������� ������ �������, ��������� ������� �������� ������ ����.", "������");
          break;
         case TSchemeObjType::Test:
          _MSG_ERR("��������� ��� ����� ������ �������, ��������� ������� ����� ������ ����.", "������");
          break;
         case TSchemeObjType::Check:
          _MSG_ERR("��������� ��� �������� ������ �������, ��������� ��������� �������� ������ ����.", "������");
          break;
         }
        break;
       case crCAN_DELETE_PRIVIV:
        DeleteActivePriviv();
        break;
       }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::actApplyUpdatesExecute(TObject * Sender)
 {
  ApplyUpdates();
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::actCancelUpdatesExecute(TObject * Sender)
 {
  if (
      _MSG_QUE(
      "�� ������� � ���, ��� ������ �������� ��������� ���������?",
      "������������� ������ ���������"
      ) == IDYES
      )
   CancelUpdates();
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::actCloseExecute(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::actCondListEditExecute(TObject * Sender)
 {
  TCondListForm * Dlg = new TCondListForm(this, FDM, IMM_S, 0);
  try
   {
    Dlg->ShowModal();
    if (Dlg->UpdateList)
     UpdateFilterList();
   }
  __finally
   {
    delete Dlg;
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TdsSchEditForm::CanEdit()
 {
  bool bResult = false;
  switch (FSchemeObjType)
   {
   case TSchemeObjType::Vac:
    //#ifdef IMM_PLAN_VAC_SCH_EDIT
    bResult = true;
    //#endif
    break;
   case TSchemeObjType::Test:
    //#ifdef IMM_PLAN_PROB_SCH_EDIT
    bResult = true;
    //#endif
    break;
   case TSchemeObjType::Check:
    //#ifdef IMM_PLAN_PROB_SCH_EDIT
    bResult = true;
    //#endif
    break;
   }
  return bResult;
 }
//---------------------------------------------------------------------------
//���������� TagNode, ��������������� ������� �����
TTagNode * __fastcall TdsSchEditForm::GetActiveSchemeNode()
 {
  CheckActive(__FUNC__);
  TTagNode * Result = NULL;
  try
   {
    TcxTreeListNode * FNode = GetActiveSch();
    if (FNode)
     Result = (TTagNode *)FNode->Data;
   }
  __finally
   {
   }
  /*
   if ( SchLB->ItemIndex >= 0 )
   Result = (TTagNode*)SchLB->Items->Objects[SchLB->ItemIndex];
   */
  return Result;
 }
//---------------------------------------------------------------------------
/*
 * ���������� ���������� ������ � ��������� xml-��������� ( ������������ ��� ����������� ����� )
 * ���� � �������� ����� ���� ���������� ������ ( �.�. ������ �� ������ ���� �� �����), �� �
 * ������������� ����� ��� ������ ��� �� ������ ���� �����������, � �� ��������� �� ������
 * �����-���������, ��� ��� ���� �� ��� ���������� ������� ������
 *
 *
 * ���������:
 *   [in]  ItTag - ������� ���� ���������
 *   [out] Src1  - ������� UID'�� ( ������� <������ UID> -> <����� UID> )
 *         Src2  - �� ������������
 *         Src3  - �� ������������
 *         Src4  - �� ������������
 *
 * ������������ ��������:
 *   true  - ���������� ���������� �����
 *   false - ���������� ���������� �����
 *
 */
bool __fastcall TdsSchEditForm::UpdateInternalRefs(TTagNode * ItTag, void * Src1, void * Src2, void * Src3, void * Src4)
 {
  if (!ItTag || !Src1)
   return true;

  TUpdateUIDTable * UpdateUIDTable = (TUpdateUIDTable *)Src1;

  UnicodeString NewRef, OldRef;
  TAttr * UIDAttr;
  if (ItTag->CmpName("refer"))
   {
    UIDAttr = ItTag->GetAttrByName("ref");
    if (UIDAttr)
     {
      OldRef = UIDAttr->Value;
      NewRef = (*UpdateUIDTable)[OldRef];
      if (Trim(NewRef) != "")
       UIDAttr->Value = NewRef;
     }
   }
  return false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::SetXMLModified(bool AValue)
 {
  FXMLModified                     = AValue;
  actApplyUpdates->Enabled         = AValue;
  actCancelUpdates->Enabled        = AValue;
  StatusSB->Panels->Items[2]->Text = (AValue) ? "��������" : "";
 }
//---------------------------------------------------------------------------
TcxTreeListNode * __fastcall TdsSchEditForm::FAddSchData(UnicodeString  AStr, TTagNode * AData)
 {
  TcxTreeListNode * FNode = NULL;
  try
   {
    FNode            = SchListTL->Root->AddChild();
    FNode->Values[0] = (int)IsStartScheme(AData->AV["uid"]);
    FNode->Texts[1]  = AStr;
    FNode->Data      = AData;
   }
  __finally
   {
   }
  return FNode;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::FUpdateSchData(UnicodeString  AStr)
 {
  TcxTreeListNode * FSchNode = GetActiveSch();
  if (FSchNode)
   {
    FSchNode->Texts[1] = AStr;
    SchListTL->SetFocus();
    FSchNode->Focused = true;
   }
 }
//---------------------------------------------------------------------------
//���������� �����
bool __fastcall TdsSchEditForm::AddScheme(UnicodeString  SchemeName, int nVakId, TTagNode * SchemeNode)
 {
  CheckActive(__FUNC__);
  bool bResult = false;

  //���� �� ������ TagNode ��� ����������� �����, �� �� ���������
  if (!SchemeNode)
   {
    SchemeNode = NewScheme(SchemeName, FCurVacCode, FIMM_S);
    if (SchemeNode)
     XMLModified = true;
   }

  //���������� �������� � ������ ����
  TcxTreeListNode * FNewSchNode = NULL;
  if (SchemeNode)
   {
    FNewSchNode = FAddSchData(SchemeNode->AV["name"], SchemeNode);
    //SchLB->AddItem(SchemeNode->AV["name"], (TObject*)SchemeNode);
    bResult = true;
   }
  if (FNewSchNode)
   {
    SchListTL->SetFocus();
    //ShowMessage(FNewSchNode->Texts[1]);
    FNewSchNode->Focused = true;
   }
  //���������� �������� � ������ �������
  if (bResult)
   {
    UpdateSchemeActions();
    UpdateSchemeStatusPanel();
   }
  return bResult;
 }
//��������� ���������� �����
//��������� ���������� �����
void __fastcall TdsSchEditForm::EditScheme(UnicodeString  SchemeName, int nVakId)
 {
  CheckActive(__FUNC__);
  //���� nVakId ��������� � ID ������� �������, �� ������ �������� ������������ �����
  if (nVakId == FCurVacCode)
   {
    SetActiveSchemeAttrVal("name", SchemeName);
    FUpdateSchData(SchemeName); //SchLB->EditItem( SchemeName ); ���������� ����� � ������
   }
 }
//---------------------------------------------------------------------------
//�������� ������� ����� ( bModifyXML ����������, ���� �� ������� ��������������� ��� � xml'� )
void __fastcall TdsSchEditForm::DeleteActiveScheme(bool bModifyXML)
 {
  CheckActive(__FUNC__);
  bool bCanDelete = false;

  if (bModifyXML)
   {
    TTagNode * ActiveSchemeNode = GetActiveSchemeNode();
    if (ActiveSchemeNode)
     {
      delete ActiveSchemeNode;
      bCanDelete  = true;
      XMLModified = true;
     }
   }
  else
   bCanDelete = true;

  if (bCanDelete)
   {
    if (GetActiveSch())
     GetActiveSch()->Delete();
    if (bModifyXML)
     {
      UpdateSchemeActions();
      UpdateSchemeStatusPanel();
     }
   }
 }
//---------------------------------------------------------------------------
//���������� ������� ����� � ��������� ������ � ������ ��������� �������
bool __fastcall TdsSchEditForm::SchemeNameExists(UnicodeString  SchemeName, int nVakId)
 {
  CheckActive(__FUNC__);
  bool Result = false;

  if (nVakId == -1)
   nVakId = GetCurVakId();

  TTagNode * CurNode = FIMM_S->GetChildByName("schemalist");
  if (CurNode)
   {
    CurNode = CurNode->GetFirstChild();
    while (CurNode)
     {
      int nCurNodeRef = StrToInt(CurNode->AV["ref"]);
      if (nCurNodeRef == nVakId)
       if (CurNode->AV["name"] == SchemeName)
        {
         Result = true;
         break;
        }
      CurNode = CurNode->GetNext();
     }
   }
  return Result;
 }
//---------------------------------------------------------------------------
int __fastcall TdsSchEditForm::GetCurVakId()
 {
  CheckActive(__FUNC__);
  return FCurVacCode;
 }
//---------------------------------------------------------------------------
//����������, ����� �� ������� ������� �����
TCanDelSchRes __fastcall TdsSchEditForm::CanDeleteCurScheme()
 {
  CheckActive(__FUNC__);
  TTagNode * ActiveSchemeNode = GetActiveSchemeNode();
  if (ActiveSchemeNode)
   {
    if (SchListTL->Root->Count == 1/*SchLB->Items->Count == 1*/)
     return crONLY_ONE_SCHEME;
    if (IfCurSchemeIsStart())
     return crCUR_SCHEME_IS_START;

    THexUIDs HexUIDs;
    TTagNode * CurSchemeLine = ActiveSchemeNode->GetFirstChild();
    while (CurSchemeLine)
     {
      HexUIDs.insert(CurSchemeLine->AV["uid"]);
      CurSchemeLine = CurSchemeLine->GetNext();
     }
    bool bRefFound = false;
    UnicodeString ActiveSchemeUID = ActiveSchemeNode->AV["uid"];
    TagNode_IterateExcl(FIMM_S, & ActiveSchemeUID, IfCurSchemeLinesRefsExist, & HexUIDs, & bRefFound);
    if (bRefFound)
     return crCUR_SCHEME_LINES_REFS_EXIST;

    CurSchemeLine = ActiveSchemeNode->GetFirstChild();
    while (CurSchemeLine)
     {
      if (CurSchemeLine->CmpName("line,endline"))
       if (SchemeLineDBRefsExist(CurSchemeLine))
        return crCUR_SCHEME_LINES_DBREFS_EXIST;
      CurSchemeLine = CurSchemeLine->GetNext();
     }

    return crCAN_DELETE_SCHEME;

   }
  return crNO_ACTIVE_SCHEME;
 }
//---------------------------------------------------------------------------
//��������� ������ ���������� ����� ��� ����������� ��������. � ������ �������
//������ ������ ������ �������� SchTV, ����� �������� ���������� ��������
//����� ��������. ���� ���� ������ ����� -2 - ��, �������� ������ ���� ���������
//����� ���������� �����
//
//������ -2? ��� ������ GetObject() ������� -1 �������� ����� ������, � ������
//���������� ����� ����������� �������� � ��������� ������ ��� �������� � ������
bool __fastcall TdsSchEditForm::GetValidPrivivTypes(TStrings * strings)
 {
  if (!strings)
   return false;

  strings->Clear();

  UnicodeString CurPrivivType;
  int nMaxVNo = 0, nMaxRVMajorNo = 0, nMaxRVMinorNo = 0;
  int nMaxVNoInd = -2, nMaxRVMajorNoInd = -2;
  int nVNo, nRVMajorNo, nRVMinorNo;
  for (int i = 0; i < SchTV->Root->Count; i++)
   {
    CurPrivivType = SchTV->Root->Items[i]->Texts[0];
    if (CurPrivivType[1] == 'V')//����������
     {
      if (CurPrivivType.Length() >= 2)
       {
        nVNo = StrToIntDef(CurPrivivType.SubString(2, CurPrivivType.Length() - 1), -1);
        if (nVNo > nMaxVNo)
         {
          nMaxVNo    = nVNo;
          nMaxVNoInd = i;
         }
       }
     }
    else if (CurPrivivType[1] == 'R')//������������
     {
      if (CurPrivivType.Length() >= 5)
       {
        nRVMajorNo = StrToIntDef(CurPrivivType.SubString(3, CurPrivivType.Pos("/") - 3), -1);
        if (nRVMajorNo >= nMaxRVMajorNo)
         {
          nMaxRVMajorNo    = nRVMajorNo;
          nMaxRVMajorNoInd = i;
         }
       }
     }
   }

  if (nMaxVNo < 9)
   strings->AddObject((UnicodeString)"V" + IntToStr(nMaxVNo + 1), (TObject *)nMaxVNoInd);

  if (nMaxRVMajorNo <= 99)
   {
    if (nMaxRVMajorNoInd > -1)
     {
      UnicodeString MaxRVCaption = SchTV->Root->Items[nMaxRVMajorNoInd]->Texts[0];
      if (MaxRVCaption.Length() >= 5)
       {
        int SlashInd = MaxRVCaption.Pos("/");
        nMaxRVMinorNo = StrToIntDef(MaxRVCaption.SubString(SlashInd + 1, MaxRVCaption.Length() - SlashInd + 1), 0);
       }
     }
    if ((nMaxRVMinorNo < 9) && (nMaxRVMajorNo >= 1))
     strings->AddObject((UnicodeString)"RV" + IntToStr(nMaxRVMajorNo) + "/" + IntToStr(nMaxRVMinorNo + 1), (TObject *)nMaxRVMajorNoInd);

    if ((nMaxRVMajorNoInd <= -1) && (nMaxVNoInd > -1))
     nMaxRVMajorNoInd = nMaxVNoInd;
    if (nMaxRVMajorNo < 99)
     strings->AddObject((UnicodeString)"RV" + IntToStr(nMaxRVMajorNo + 1) + "/1", (TObject *)nMaxRVMajorNoInd);

   }

  return (bool)strings->Count;
 }
//---------------------------------------------------------------------------
//��������� ������ ���������� ����� ��� ����������� �����. ���������� �����������
//������, �� ����������� ����, ��� ��� ���� ����������� ��� "��.i", i = 1..99
bool __fastcall TdsSchEditForm::GetValidProbTypes(TStrings * strings)
 {
  if (!strings)
   return false;

  strings->Clear();

  UnicodeString CurProbType;
  int nMaxPNo = 0;
  int nMaxPNoInd = -2;
  int nPNo;
  for (int i = 0; i < SchTV->Root->Count; i++)
   {
    CurProbType = SchTV->Root->Items[i]->Texts[0];
    if (CurProbType[1] == '�')//�����
     {
      if (CurProbType.Length() >= 4)
       {
        nPNo = StrToIntDef(CurProbType.SubString(4, CurProbType.Length() - 3), -1);
        if (nPNo > nMaxPNo)
         {
          nMaxPNo    = nPNo;
          nMaxPNoInd = i;
         }
       }
     }
   }

  if (nMaxPNo < 99)
   strings->AddObject((UnicodeString)"��." + IntToStr(nMaxPNo + 1), (TObject *)nMaxPNoInd);

  return (bool)strings->Count;
 }
//---------------------------------------------------------------------------
//����������, ����� �� ������� ������� ��������
TCanDelPrivRes __fastcall TdsSchEditForm::CanDeleteActivePriviv()
 {
  CheckActive(__FUNC__);
  if (GetCurSchLine())
   {
    if (GetCurSchLine()->Texts[0] == csEndLineLabel)
     return crEND_LINE;
    //!!!    if ( SchTV->ItemIndex != GetMaxPrivivItemIndex ( GetCurSchLine()->Texts[0][1] ) )
    //return crNOT_GR_END_PRIVIV;

    THexUIDs HexUIDs;
    TTagNode * CurPriviv = GetCurSchLineData();
    UnicodeString CurPrivivUID = CurPriviv->AV["uid"];
    HexUIDs.insert(CurPrivivUID);

    bool bRefFound = false;
    TagNode_IterateExcl(FIMM_S, & CurPrivivUID, IfCurSchemeLinesRefsExist, & HexUIDs, & bRefFound);
    if (bRefFound)
     return crCUR_PRIVIV_REFS_EXIST;

    if (SchemeLineDBRefsExist(CurPriviv))
     return crCUR_PRIVIV_DBREFS_EXIST;

    return crCAN_DELETE_PRIVIV;
   }
  return crNO_ACTIVE_PRIVIV;
 }
//---------------------------------------------------------------------------
//���������� ��������
//nAfterInsertInd - ������ �������� � ListView, ����� �������� ���������� ��������
//����� �������, ��������������� ��������. ���� nAfterInsertInd == -2, �� ��������
//���������� �������� ����� ���������� �����
bool __fastcall TdsSchEditForm::AddPriviv(TTagNode * LineNode, int nAfterInsertInd)
 {
  CheckActive(__FUNC__);
  bool bResult = false;
  if (nAfterInsertInd == -2)
   nAfterInsertInd = -1;
  TTagNode * ActiveSchemeNode = GetActiveSchemeNode();
  TcxTreeListNode * NewListItem = NULL;
  if (ActiveSchemeNode)
   {
    //����������� ���� TagNode, ����� ��� ����� �������� ���������� �������� ����� ��������
    TTagNode * PrevLineNode;
    bool bInsertBefore;
    if (nAfterInsertInd == -1)
     {
      PrevLineNode = ActiveSchemeNode->GetChildByName("endline");

      NewListItem   = SchTV->Root->InsertChild(SchTV->Root->GetLastChild());
      bInsertBefore = true;
     }
    else
     {
      PrevLineNode = (TTagNode *)SchTV->Root->Items[nAfterInsertInd]->Data;
      if ((nAfterInsertInd + 1) < SchTV->Root->Count)
       NewListItem = SchTV->Root->InsertChild(SchTV->Root->Items[nAfterInsertInd + 1]);
      else
       NewListItem = SchTV->Root->AddChild();
      bInsertBefore = false;
     }

    //������� TagNode
    TTagNode * NewLineNode = ActiveSchemeNode->InsertChild(PrevLineNode, "line", bInsertBefore);
    NewLineNode->Assign(LineNode, true);

    //������� �������� ListView
    AddLineInfo(NewLineNode, NewListItem);
    XMLModified = true;
    bResult     = true;
    if (NewListItem)
     {
      NewListItem->Focused = true;
      SchTV->SetFocus();
     }

    //���������� �������� � ������ �������
    UpdatePrivivActions();
    UpdatePrivivStatusPanel();
   }
  return bResult;
 }
//---------------------------------------------------------------------------
//��������� ���������� ��������
bool __fastcall TdsSchEditForm::EditPriviv(TTagNode * LineNode)
 {
  CheckActive(__FUNC__);
  TTagNode * CurLineNode = GetCurSchLineData();
  if (CurLineNode)
   {
    CurLineNode->Assign(LineNode, true);
    EditLineInfo(CurLineNode, GetCurSchLine());
    XMLModified = true;
    return true;
   }
  return false;
 }
//---------------------------------------------------------------------------
//�������� ������� ��������
void __fastcall TdsSchEditForm::DeleteActivePriviv()
 {
  CheckActive(__FUNC__);
  if (GetCurSchLine())
   {
    int NewItemIndex = GetCurSchLine()->Index - 1;
    TTagNode * LineNode = GetCurSchLineData();
    delete LineNode;
    GetCurSchLine()->Delete();
    XMLModified                               = true;
    SchTV->Root->Items[NewItemIndex]->Focused = true;
    UpdatePrivivActions();
    UpdatePrivivStatusPanel();
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TdsSchEditForm::FProgressProc(TkabProgressType AType, int AVal, UnicodeString AMsg)
 {
  if (AType == TkabProgressType::CommPerc)
   StatusSB->Panels->Items[4]->Text = IntToStr(AVal) + "%";
  else if (AType == TkabProgressType::CommComplite)
   StatusSB->Panels->Items[4]->Text = "";
  return true;
 }
//---------------------------------------------------------------------------
/*
 * ���������� ��������� � xml-���������
 *
 *
 * ������������ ��������:
 *   ������� ���������� ���������
 *
 */
bool __fastcall TdsSchEditForm::ApplyUpdates()
 {
  CheckActive(__FUNC__);
  bool fResult = false;
  if (XMLModified)
   {
    if (!FDM->OnSaveXML)
     throw ESchListError(__FUNC__, "�� ���������� ���������� ������� OnSaveXML.");

    if (FValidateOnSave)
     if (ValidateXML(FSchemeObjType, FIMM_S, FDM))
      XMLModified = true;

    TTagNode * NewChangeNode = FIMM_S->GetChildByName("passport")->AddChild("changeshistory");
    NewChangeNode->AV["timestamp"] = Date().FormatString("yyyymmddhhnnss");

    try
     {
      SetBookmark();
      CloseEditor();
      if (FDM->OnSaveXML(FIMM_S, FSchemeObjType, FProgressProc))
       {
        XMLModified    = false;
        FIMM_SModified = true;
        FDM->XMLList->GetXML(FSchGUI)->Assign(FIMM_S, true);
        fResult = true;
       }
      OpenEditor();
     }
    __finally
     {
      ClearBookmark();
     }
   }
  return fResult;
 }
//---------------------------------------------------------------------------
/*
 * ������ ���������, ��������� � xml-��������
 *
 *
 * ������������ ��������:
 *   ������� ������ ���������
 *
 */
bool __fastcall TdsSchEditForm::CancelUpdates()
 {
  CheckActive(__FUNC__);
  bool fResult = false;
  if (XMLModified)
   {
    try
     {
      SetBookmark();
      FIMM_S->Assign(FDM->XMLList->GetXML(FSchGUI), true);
      CloseEditor();
      OpenEditor();
      XMLModified = false;
      fResult     = true;
     }
    __finally
     {
      ClearBookmark();
     }
   }
  return fResult;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::SetIMM_S(TTagNode * AValue)
 {
  if (!AValue)
   throw DKClasses::ENullArgument(__FUNC__, "AValue");
  CloseEditor();
  FIMM_S = AValue;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::UpdateFilterList()
 {
  CheckActive(__FUNC__);
  FilterList->Clear();
  TTagNode * itSch = IMM_S->GetChildByName("conditionlist");
  if (itSch)
   {
    itSch = itSch->GetFirstChild();
    while (itSch)
     {
      FilterList->AddObject(itSch->AV["name"], (TObject *)UIDInt(itSch->AV["uid"]));
      itSch = itSch->GetNext();
     }
   }
  XMLModified = true;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::CheckActive(const UnicodeString MethodName) const
 {
  if (!FEditorActive)
   throw ESchListError(MethodName, "�������� �� �������.");
 }
//---------------------------------------------------------------------------
TTagNode * __fastcall TdsSchEditForm::NewScheme(UnicodeString  ASchemeName, int AVacId, TTagNode * AXML)
 {
  if (!AXML)
   throw DKClasses::ENullArgument(__FUNC__, "AXML");
  TTagNode * SchemeNode = AXML->GetChildByName("schemalist")->AddChild("schema");
  SchemeNode->AV["uid"]  = AXML->NewUID();
  SchemeNode->AV["name"] = ASchemeName;
  SchemeNode->AV["ref"]  = IntToStr(AVacId);
  TTagNode * BegLineNode = SchemeNode->AddChild("begline");
  BegLineNode->AV["uid"] = AXML->NewUID();
  TTagNode * EndLineNode = SchemeNode->AddChild("endline");
  EndLineNode->AV["uid"] = AXML->NewUID();
  return SchemeNode;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::UpdateSchemeActions()
 {
  CheckActive(__FUNC__);
  bool bAddActEn = (bool)(FCurVacCode != -1);
  bool bActEn = (bool)(SchListTL->Root->Count/*SchLB->Items->Count*/);

  actAddScheme->Enabled    = bAddActEn;
  actEditScheme->Enabled   = bActEn;
  actCopyScheme->Enabled   = bActEn;
  actDeleteScheme->Enabled = bActEn;
 }

//---------------------------------------------------------------------------
//���������� enabled ��� ��������, ���������� ��� ������ ����� ���� (��������)
void __fastcall TdsSchEditForm::UpdatePrivivActions()
 {
  CheckActive(__FUNC__);
  bool bAddActEn = (bool)GetActiveSch()/*(SchLB->ItemIndex >= 0)*/;
  bool bActEn = (bool)SchTV->Root->Count;

  actAddPriviv->Enabled    = bAddActEn;
  actEditPriviv->Enabled   = bActEn;
  actDeletePriviv->Enabled = bActEn;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditForm::GetCaptionText()
 {
  UnicodeString RC = "";
  try
   {
    if (FCurSchLineName.Length())
     RC += FCurSchLineName + ".";
    if (FCurSchName.Length())
     RC += FCurSchName;
    if (FCurInfName.Length())
     RC += " [ " + FCurInfName + " ] ";
    if (FCurVacName.Length())
     RC += " { " + FCurVacName + " } ";
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
//���������� ���������� � ������ �� ������ �������
void __fastcall TdsSchEditForm::UpdateSchemeStatusPanel()
 {
  CheckActive(__FUNC__);
  int nCurVakSchCount = SchListTL->Root->Count;
  int nTotalSchCount = (FIMM_S) ? FIMM_S->GetChildByName("schemalist")->Count : 0;

  StatusSB->Panels->Items[0]->Text =
      IntToStr(nCurVakSchCount) + " �� " + IntToStr(nTotalSchCount) + " " +
      ICUtils::GetWordForNumber(nCurVakSchCount, WFNScheme);
  /*StatusSB->Panels->Items[0]->Text =
   IntToStr(nCurVakSchCount) + " " +
   GetWordForNumber(nCurVakSchCount, WFNScheme);*/
  StatusSB->Panels->Items[4]->Text = GetCaptionText();
  Caption                          = StatusSB->Panels->Items[4]->Text;

 }
//---------------------------------------------------------------------------
//���������� ���������� � ��������� �� ������ �������
void __fastcall TdsSchEditForm::UpdatePrivivStatusPanel()
 {
  CheckActive(__FUNC__);
  UnicodeString * WordForNumber;
  int nPrivivCount = 0;
  switch (FSchemeObjType)
   {
   case TSchemeObjType::Vac:
    WordForNumber = const_cast<UnicodeString *>(WFNPrivivka);
    break;
   case TSchemeObjType::Test:
    WordForNumber = const_cast<UnicodeString *>(WFNTest);
    break;
   case TSchemeObjType::Check:
    WordForNumber = const_cast<UnicodeString *>(WFNCheck);
    break;
   }
  if (SchTV->Root->Count > 2)
   nPrivivCount = SchTV->Root->Count - 2;
  StatusSB->Panels->Items[1]->Text =
      IntToStr(nPrivivCount) + " " +
      ICUtils::GetWordForNumber(nPrivivCount, WordForNumber);
  StatusSB->Panels->Items[4]->Text = GetCaptionText();
  Caption                          = StatusSB->Panels->Items[4]->Text;
 }
//---------------------------------------------------------------------------
//��������� �������� �������� TagNode, ���������������� ������� �����
void __fastcall TdsSchEditForm::SetActiveSchemeAttrVal(UnicodeString  AttrName, UnicodeString  AttrVal)
 {
  CheckActive(__FUNC__);
  TTagNode * ActiveSchemeNode = GetActiveSchemeNode();
  if (ActiveSchemeNode)
   {
    ActiveSchemeNode->AV[AttrName] = AttrVal;
    XMLModified                    = true;
   }
 }
//---------------------------------------------------------------------------
//��������� �������� �������� TagNode, ���������������� ������� �����
UnicodeString __fastcall TdsSchEditForm::GetActiveSchemeAttrVal(UnicodeString  AttrName)
 {
  CheckActive(__FUNC__);
  UnicodeString Result = "";
  TTagNode * ActiveSchemeNode = GetActiveSchemeNode();
  if (ActiveSchemeNode)
   Result = ActiveSchemeNode->AV[AttrName];
  return Result;
 }
//---------------------------------------------------------------------------
//����������, �������� �� ������� ����� ���������
bool __fastcall TdsSchEditForm::IfCurSchemeIsStart()
 {
  CheckActive(__FUNC__);
  bool Result = false;
  TTagNode * SchemeNode = GetActiveSchemeNode();
  if (SchemeNode)
   Result = IsStartScheme(SchemeNode->AV["uid"]);
  return Result;
 }
//---------------------------------------------------------------------------
//����������, �������� �� ����� � ��������� UID'�� ���������
bool __fastcall TdsSchEditForm::IsStartScheme(UnicodeString UID)
 {
  CheckActive(__FUNC__);
  bool Result = false;
  try
   {
    TTagNode * FInfDef = FDM->PlanOptions->GetChildByName("infdef", true);
    if (FInfDef)
     {
      UnicodeString FFLName = "";
      switch (FSchemeObjType)
       {
       case TSchemeObjType::Vac:
        FFLName = "vschdef";
        break;
       case TSchemeObjType::Test:
        FFLName = "tschdef";
        break;
       case TSchemeObjType::Check:
        FFLName = "���_���������";
        break; //!!!
       }

      TTagNode * itNode = FInfDef->GetFirstChild();
      while (itNode && !Result)
       {
        if (FFLName.Length() && !itNode->CmpAV(FFLName, "_NULL_"))
         Result = (_GUI(itNode->AV[FFLName]).UpperCase() == UID.UpperCase());
        itNode = itNode->GetNext();
       }
     }
   }
  __finally
   {
   }
  return Result;
 }
//---------------------------------------------------------------------------
/*
 * ����������� ������� ������ �� ������ ������� ����� �� ������� �����
 * ������ ����
 *
 *
 * ���������:
 *   [in]  ItTag - ������� ���� xml-������
 *   [in]  Src1  - ��������� UID'�� �����, ������ �� ������� ���������� �����
 *   [out] Src2  - ������� ��������� ������ ( ����� ������������ ��� ���������� ������ ������ )
 *         Src3  - �� ������������
 *         Src4  - �� ������������
 *
 * ������������ ��������:
 *   true  - ���������� ��������� �����
 *   false - ���������� ���������� �����
 *
 */

bool __fastcall TdsSchEditForm::IfCurSchemeLinesRefsExist(TTagNode * ItTag, void * Src1, void * Src2, void * Src3, void * Src4)
 {
  if (!ItTag || !Src1 || !Src2)
   return true;

  bool & bRefFound = *((bool *)Src2);
  THexUIDs * HexUID = (THexUIDs *)Src1;
  TAttr * RefAttr = ItTag->GetAttrByName("ref");

  bRefFound = false;
  if (RefAttr)
   if (HexUID->find(RefAttr->Value) != HexUID->end())
    {
     bRefFound = true;
     return true;
    }
  return false;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsSchEditForm::SchemeLineDBRefsExist(TTagNode * SchemeLine)
 {
  return false;
  /*
   CheckActive(__FUNC__);
   if ( !SchemeLine )
   throw DKClasses::ENullArgument(__FUNC__, "SchemeLine");
   if ( !SchemeLine->CmpName( "line,endline" ) )
   throw DKClasses::EInvalidArgument(__FUNC__, "SchemeLine", "�������� ������ ��������������� ������ �����.");
   bool fResult = false;
   TpFIBQuery* q;
   switch(FSchemeObjType)
   {
   case TSchemeObjType::Vac: q = FSDM->fqPrivivCount; break;
   case TSchemeObjType::Test   : q = FSDM->fqProbCount;   break;
   case TSchemeObjType::Check:   q = FSDM->fqProbCount;   break;
   }
   if ( q )
   {
   q->ParamByName( "pSchemeLine" )->AsString =
   SchemeLine->GetParent()->AV["uid"] +
   "." +
   SchemeLine->AV["uid"];
   START_FIBQUERY(q)
   fResult = q->FN("Count")->AsInteger;
   STOP_FIBQUERY(q)
   }
   return fResult;
   */
 }
//---------------------------------------------------------------------------
//���������� TagNode, ��������������� ������� ��������
//TTagNode* __fastcall TdsSchEditForm::GetActivePrivivNode()
//{
/*
 CheckActive(__FUNC__);
 TTagNode *Result = NULL;
 if ( SchTV->ItemIndex >= 0 )
 Result = (TTagNode*)SchTV->Items->Item[ SchTV->ItemIndex ]->Data;
 return Result;
 */
//}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditForm::PausePeriodText(TTagNode * ANode)
 {
  UnicodeString RC = XMLStrDurationToStr(ANode->AV["min_pause"]);
  try
   {
    UnicodeString pvt = ANode->AV["min_pause_vactype"].Trim();
    if (pvt.Length())
     {
      if (pvt.Pos(":"))
       {
        int idx = GetLPartB(pvt, ':').ToIntDef(0);
        pvt = GetRPartB(pvt, ':');
        switch (idx)
         {
         case 0: //�� ��������� ����� / �������� / ��������
          RC += " �� ��������� ����� / �������� / ��������";
          break;
         case 1: //�� ���������� ��������
          RC += " �� ��������� ��������";
          break;
         case 2: //�� ���������� �����
          RC += " �� ��������� �����";
          break;
         case 3: //�� ���������� ��������
          RC += " �� ��������� ��������";
          break;
         case 4: //�� ��������:
          RC += " �� ��������: " + pvt;
          break;
         case 5: //�� �����:
          RC += " �� �����: " + pvt;
          break;
         case 6: //�� ��������:
          RC += " �� ��������: " + pvt;
          break;
         }
       }
      else
       RC += " �� " + pvt;
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
//����������� TagNode �� ListItem � ������ ����������
bool __fastcall TdsSchEditForm::AddLineInfo(TTagNode * ASrcNode, TcxTreeListNode * ADestNode)
 {
  if ((!ASrcNode) || (!ADestNode))
   return false;

  //ListItem->Caption    = (ASrcNode->CmpName("line")) ? ASrcNode->AV["name"] : csEndLineLabel;
  ADestNode->Texts[0] = (ASrcNode->CmpName("line")) ? ASrcNode->AV["name"] : ((ASrcNode->CmpName("endline")) ? csEndLineLabel : csBeginLineLabel);
  ADestNode->Data     = ASrcNode;
  //ListItem->ImageIndex = iiNone;

  ADestNode->Texts[1] = PausePeriodText(ASrcNode);
  ADestNode->Texts[2] = XMLStrDurationToStr(ASrcNode->AV["min_age"]);
  ADestNode->Texts[3] = GetSeasonDiap(ASrcNode->AV["season_beg"], ASrcNode->AV["season_end"]);

  UpdateLineJumpsInfo(ASrcNode, ADestNode);
  return true;
 }
//---------------------------------------------------------------------------
//����������� TagNode �� ListItem � ������ ��������������
bool __fastcall TdsSchEditForm::EditLineInfo(TTagNode * ASrcNode, TcxTreeListNode * ADestNode)
 {
  if ((!ASrcNode) || (!ADestNode))
   return false;

  ADestNode->Texts[1] = PausePeriodText(ASrcNode);
  ADestNode->Texts[2] = XMLStrDurationToStr(ASrcNode->AV["min_age"]);
  ADestNode->Texts[3] = GetSeasonDiap(ASrcNode->AV["season_beg"], ASrcNode->AV["season_end"]);
  UpdateLineJumpsInfo(ASrcNode, ADestNode);
  return true;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsSchEditForm::ValidateXML(TSchemeObjType ASchemeObjType, TTagNode * AXML, TdsSchEditDM * ADM)
 {
  bool fModified = false;
  try
   {
    fModified |= ValidateSchemeExistence(ASchemeObjType, AXML, ADM);
    fModified |= ValidateSchemeJumps(ASchemeObjType, AXML, ADM);
   }
  __finally
   {
   }
  return fModified;
 }
//---------------------------------------------------------------------------
/*
 * ��������� xml-��������� �� ������� ������� ���� ��� ������ �������
 *
 *
 * ���������:
 *   [in]     ASDM - ������ ������
 *   [in\out] AXML - xml-��������
 *
 * ������������ ��������:
 *   ������� ����������� xml-���������
 *
 */
bool __fastcall TdsSchEditForm::ValidateSchemeExistence(TSchemeObjType ASchemeObjType, TTagNode * AXML, TdsSchEditDM * ADM)
 {
  bool fModified = false;
  TStringList * tmpVacList = new TStringList;
  try
   {
    //ADM->FillVacList(tmpVacList, 0);
    switch (ASchemeObjType)
     {
     case TSchemeObjType::Vac:
      ADM->FillVacList(tmpVacList, 0);
      break;
     case TSchemeObjType::Test:
      ADM->FillTestList(tmpVacList, 0);
      break;
     case TSchemeObjType::Check:
      ADM->FillCheckList(tmpVacList, 0);
      break;
     }
    int nVacId;
    for (int i = 0; i < tmpVacList->Count; i++)
     {
      nVacId = (int)tmpVacList->Objects[i];
      TTagNode * SchemeListNode = AXML->GetChildByName("schemalist");
      TTagNode * SchemeNode = SchemeListNode->GetChildByAV("schema", "ref", IntToStr(nVacId));
      if (!SchemeNode)
       fModified = (SchemeNode = NewScheme("1", nVacId, AXML));
     }
   }
  __finally
   {
    delete tmpVacList;
   }
  return fModified;
 }
//---------------------------------------------------------------------------
/*
 * ��������� xml-��������� �� ������� ���������� ���������
 *
 *
 * ���������:
 *   [in]     ASDM - ������ ������
 *   [in\out] AXML - xml-��������
 *
 * ������������ ��������:
 *   ������� ����������� xml-���������
 *
 */

bool __fastcall TdsSchEditForm::ValidateSchemeJumps(TSchemeObjType ASchemeObjType, TTagNode * AXML, TdsSchEditDM * ADM)
 {
  bool fModified = false;
  TStringList * VacInfs = NULL;
  try
   {
    VacInfs = new TStringList;
    TTagNode * SchemeNode = AXML->GetChildByName("schemalist")->GetFirstChild();
    int nVacId;
    while (SchemeNode)
     {
      nVacId = SchemeNode->AV["ref"].ToInt();
      //ASDM->fqInfeks->ParamByName("PVakCode")->AsInteger = nVacId;
      //FillStrings(ASDM->fqInfeks, VacInfs, InfekchiyaName, TablePK, false);
      //ADM->FillVacInfList(VacInfs, nVacId);
      switch (ASchemeObjType)
       {
       case TSchemeObjType::Vac:
        ADM->FillVacInfList(VacInfs, nVacId);
        break;
       case TSchemeObjType::Test:
        ADM->FillTestInfList(VacInfs, nVacId);
        break;
       case TSchemeObjType::Check:
        ADM->FillCheckInfList(VacInfs, nVacId);
        break;
       }

      TTagNode * LineNode = SchemeNode->GetFirstChild();
      while (LineNode)
       {
        TTagNode * JumpNode = LineNode->GetFirstChild();
        while (JumpNode)
         {
          TTagNode * RefList = (JumpNode->CmpName("jump"))
              ? JumpNode->GetChildByName("reflist")
              : JumpNode;
          //�������� ������ �� ���������, �� ��������������� �������
          TTagNode * Refer = RefList->GetFirstChild();
          while (Refer)
           {
            int nInfId = Refer->AV["infref"].ToInt();
            if (VacInfs->IndexOfObject(reinterpret_cast<TObject *>(nInfId)) == -1)
             {
              TTagNode * NodeToDelete = Refer;
              Refer = Refer->GetNext();
              delete NodeToDelete;
              fModified = true;
             }
            else
             Refer = Refer->GetNext();
           }
          //���������� ����������� ������ �� ��������� ����������� �� ���������
          for (int i = 0; i < VacInfs->Count; i++)
           {
            int nInfId = reinterpret_cast<int>(VacInfs->Objects[i]);
            if (!RefList->GetChildByAV("refer", "infref", IntToStr(nInfId)))
             {
              TTagNode * NewRefer = RefList->AddChild("refer");
              NewRefer->AV["infref"] = IntToStr(nInfId);
              NewRefer->AV["ref"]    = EmptyRef;
              fModified              = true;
             }
           }
          JumpNode = JumpNode->GetNext();
         }
        LineNode = LineNode->GetNext();
       }
      SchemeNode = SchemeNode->GetNext();
     }
   }
  __finally
   {
    if (VacInfs)
     delete VacInfs;
   }
  return fModified;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::SetBookmark()
 {
  CheckActive(__FUNC__);
  if (!FEditorBookmark)
   FEditorBookmark = new TEditorBookmark;
  else
   FEditorBookmark->Clear();

  //if (InfCB->ItemIndex != -1)
  //if (InfCB->Properties->Items->Strings[InfCB->ItemIndex].Length())
  //FEditorBookmark->nInfId = reinterpret_cast<int>(InfCB->Properties->Items->Objects[InfCB->ItemIndex]);
  //if (ItemsCB->ItemIndex != -1)
  //FEditorBookmark->nVacId = reinterpret_cast<int>(ItemsCB->Properties->Items->Objects[ItemsCB->ItemIndex]);
  //if (GetActiveSch()/*SchLB->ItemIndex != -1*/)
  //FEditorBookmark->sSchUID = GetActiveSchemeNode()/*dynamic_cast<TTagNode*>(
  //SchLB->Items->Objects[SchLB->ItemIndex]
  //)*/->AV["uid"];
  //if (GetCurSchLineData()/*SchTV->ItemIndex != -1*/)
  //FEditorBookmark->sSchLineUID = GetCurSchLineData()/*reinterpret_cast<TTagNode*>(
  //SchTV->Items->Item[SchTV->ItemIndex]->Data
  //)*/->AV["uid"];
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::ClearBookmark()
 {
  __DELETE_OBJ(FEditorBookmark)
 }
//---------------------------------------------------------------------------
bool __fastcall TdsSchEditForm::OpenEditor()
 {
  bool fResult = false;
  if (!FEditorActive)
   {
    fResult = true;
    if (!FNmklGUI.Length())
     throw ESchListError(__FUNC__, "�� ������ ��� ������������.");
    if (!FIMM_S)
     FIMM_S = new TTagNode;

    FIMM_S->Assign(FDM->XMLList->GetXML(FSchGUI), true);

    if (fResult)
     {
      if (!FIMM_S)
       throw ESchListError(__FUNC__, "�� ���������� xml-��������.");
      try
       {
        SchListTL->Enabled = true;
        SchTV->Enabled     = true;
        ItemTL->Enabled    = true;
        FEditorActive      = true;
        FFilterList        = new TStringList;
        //�������� ������ ������� ��� ����
        TTagNode * itSch = FIMM_S->GetChildByName("conditionlist");
        if (itSch)
         {
          itSch = itSch->GetFirstChild();
          while (itSch)
           {
            FFilterList->AddObject(itSch->AV["name"], (TObject *)UIDInt(itSch->AV["uid"]));
            itSch = itSch->GetNext();
           }
         }

        FillItemList();

        XMLModified = false;
       }
      catch (...)
       {
        CloseEditor();
        throw;
       }
     }
   }
  return fResult;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsSchEditForm::FillItemList()
 {
  bool RC = false;
  TcxTreeListNode * FInfNode, *FVacNode;
  TStringList * FInfList = new TStringList;
  TStringList * FItemsList = new TStringList;
  ItemTL->BeginUpdate();
  try
   {
    ItemTL->Clear();
    if (GroupByInf->Checked)
     {//������������ �� ���������
      int FInfCode;
      FDM->FillInfList(FInfList);
      for (int i = 0; i < FInfList->Count; i++)
       {
        FInfCode = (int)FInfList->Objects[i];
        switch (FSchemeObjType)
         {
         case TSchemeObjType::Vac:
          FDM->FillVacList(FItemsList, FInfCode);
          break;
         case TSchemeObjType::Test:
          FDM->FillTestList(FItemsList, FInfCode);
          break;
         case TSchemeObjType::Check:
          FDM->FillCheckList(FItemsList, FInfCode);
          break;
         }
        if (FItemsList->Count)
         {
          FInfNode           = ItemTL->Root->AddChild();
          FInfNode->Texts[0] = FInfList->Strings[i];
          FInfNode->Data     = FInfList->Objects[i];

          for (int j = 0; j < FItemsList->Count; j++)
           {
            FVacNode           = FInfNode->AddChild();
            FVacNode->Texts[0] = FItemsList->Strings[j];
            FVacNode->Data     = FItemsList->Objects[j];
           }
         }
       }
      if (FSchemeObjType == TSchemeObjType::Check)
       {
        FDM->FillCheckList(FItemsList, 0);
        if (FItemsList->Count)
         {
          FInfNode           = ItemTL->Root->AddChild();
          FInfNode->Texts[0] = "��� ��������";
          FInfNode->Data     = 0;

          for (int j = 0; j < FItemsList->Count; j++)
           {
            FVacNode           = FInfNode->AddChild();
            FVacNode->Texts[0] = FItemsList->Strings[j];
            FVacNode->Data     = FItemsList->Objects[j];
           }
         }
       }
     }
    else
     {//��� ����������� �� ���������
      switch (FSchemeObjType)
       {
       case TSchemeObjType::Vac:
        FDM->FillVacList(FItemsList, 0);
        break;
       case TSchemeObjType::Test:
        FDM->FillTestList(FItemsList, 0);
        break;
       case TSchemeObjType::Check:
        FDM->FillCheckList(FItemsList, 0);
        break;
       }
      for (int j = 0; j < FItemsList->Count; j++)
       {
        FVacNode           = ItemTL->Root->AddChild();
        FVacNode->Texts[0] = FItemsList->Strings[j];
        FVacNode->Data     = FItemsList->Objects[j];
       }
     }
   }
  __finally
   {
    delete FInfList;
    delete FItemsList;
    ItemTL->EndUpdate();
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsSchEditForm::CloseEditor()
 {
  bool fResult = false;
  if (FEditorActive)
   {
    fResult = true;
    try
     {
      //FSDM->CloseDataSets();
      //InfCB->Properties->Items->Clear();
      //ItemsCB->Properties->Items->Clear();
      SchListTL->Clear();
      SchListTL->Enabled = false;
      SchTV->Clear();
      SchTV->Enabled  = false;
      ItemTL->Enabled = false;
      if (FFilterList)
       {
        delete FFilterList;
        FFilterList = NULL;
       }
      //delete FIMM_S; FIMM_S = NULL;
     }
    __finally
     {
      UpdateSchemeActions();
      UpdateSchemeStatusPanel();
      UpdatePrivivActions();
      UpdatePrivivStatusPanel();
      XMLModified   = false;
      FEditorActive = false;
     }
   }
  return fResult;
 }
//---------------------------------------------------------------------------
//���������� ������ ����� ������� �����
void __fastcall TdsSchEditForm::UpdatePrivivList(TTagNode * SchemeNode)
 {
  CheckActive(__FUNC__);
  SchTV->Clear();
  int nSelPrivivInd = 0;
  if (SchemeNode)
   {
    TTagNode * LineNode = SchemeNode->GetFirstChild();
    int nInd = 0;
    while (LineNode)
     {
      if (FEditorBookmark && FEditorBookmark->sSchLineUID.Length())
       if (LineNode->AV["uid"] == FEditorBookmark->sSchLineUID)
        nSelPrivivInd = nInd;
      AddLineInfo(LineNode, SchTV->Root->AddChild());
      LineNode = LineNode->GetNext();
      nInd++ ;
     }

    TcxTreeListNode * FSchLineNode = GetCurSchLine();
    if (FSchLineNode)
     {
      FSchLineNode->Focused = true;
      SchTV->SetFocus();
     }

    UpdatePrivivActions();
    UpdatePrivivStatusPanel();
   }
 }
//---------------------------------------------------------------------------
//����������� � ListItem ������� � ������� ���������
bool __fastcall TdsSchEditForm::UpdateLineJumpsInfo(TTagNode * ASrcNode, TcxTreeListNode * ADestNode)
 {
  if ((!ASrcNode) || (!ADestNode))
   return false;
  ADestNode->Values[4] = (ASrcNode->GetChildByName("srokjump")) ? 1 : 0;
  ADestNode->Values[5] = (ASrcNode->GetChildByName("agejump")) ? 1 : 0;
  ADestNode->Values[6] = (ASrcNode->GetChildByName("jump")) ? 1 : 0;
  ADestNode->Values[7] = (ASrcNode->GetChildByName("endjump")) ? 1 : 0;
  return true;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::SetNmklGUI(UnicodeString AValue)
 {
  CloseEditor();
  FNmklGUI = AValue;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditForm::GetCurVakName()
 {
  CheckActive(__FUNC__);
  return FCurVacName;
 }
//---------------------------------------------------------------------------
//��������� ������ �������� ��� ��������� �������
bool __fastcall TdsSchEditForm::GetVakInfekList(TStrings * strings, int nVakId)
 {
  CheckActive(__FUNC__);
  bool RC = false;
  try
   {
    if (strings)
     {
      switch (FSchemeObjType)
       {
       case TSchemeObjType::Vac:
        RC = (bool)FDM->FillVacInfList(strings, nVakId);
        break;
       case TSchemeObjType::Test:
        RC = (bool)FDM->FillTestInfList(strings, nVakId);
        break;
       case TSchemeObjType::Check:
        RC = (bool)FDM->FillCheckInfList(strings, nVakId);
        break;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
//��������� ������ ������ ��� ��������� ��������
bool __fastcall TdsSchEditForm::GetInfekVakList(TStrings * strings, int nInfekId)
 {
  CheckActive(__FUNC__);
  bool RC = false;
  try
   {
    if (strings)
     {
      switch (FSchemeObjType)
       {
       case TSchemeObjType::Vac:
        RC = FDM->FillVacList(strings, nInfekId);
        break;
       case TSchemeObjType::Test:
        RC = FDM->FillTestList(strings, nInfekId);
        break;
       case TSchemeObjType::Check:
        RC = FDM->FillCheckList(strings, nInfekId);
        break;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditForm::GetInfekName(int nInfekId)
 {
  CheckActive(__FUNC__);
  return FDM->InfName[IntToStr(nInfekId)];
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditForm::GetFilterNameByID(int nId)
 {
  CheckActive(__FUNC__);
  UnicodeString Result = "����������� ������";
  int idx = FFilterList->IndexOfObject((TObject *)nId);
  if (idx != -1)
   Result = FFilterList->Strings[idx];
  return Result;
 }

//---------------------------------------------------------------------------
//��������� ������������ �������� � ���� <��� �������> (<��� �����>.<��� ��������>)

UnicodeString __fastcall TdsSchEditForm::GetPrivivFullName(
    TTagNode * AIMM_S,
    UnicodeString  AVacName,
    UnicodeString  APrivivUID
    )
 {
  if (!AIMM_S)
   throw DKClasses::ENullArgument(
      __FUNC__,
      "AIMM_S"
       );
  if (!AIMM_S->CmpName("imm-s"))
   throw DKClasses::EMethodError(
      __FUNC__,
      "��� xml-��������� �� ������� ������ ���� imm-s."
       );
  UnicodeString sPrivivFullName;
  if (APrivivUID == EmptyRef)
   sPrivivFullName = csEndPlanLabel;
  else
   {
    TTagNode * LineNode = AIMM_S->GetTagByUID(APrivivUID);
    if (LineNode)
     {
      UnicodeString sSchName = LineNode->GetParent()->AV["name"];
      UnicodeString sPrivivType = (LineNode->GetAttrByName("name"))
          ? LineNode->AV["name"]
          : csEndLineLabel.LowerCase();
      sPrivivFullName = Format(UnicodeString("%s (%s.%s)"), OPENARRAY(TVarRec, (AVacName, sSchName, sPrivivType)));
     }
   }
  return sPrivivFullName;
 }

//---------------------------------------------------------------------------
//�������� ������������ �������� � ���� <��� �������> (<��� �����>.<��� ��������>)
UnicodeString __fastcall TdsSchEditForm::GetPrivivFullName(UnicodeString  APrivivUID)
 {
  CheckActive(__FUNC__);

  UnicodeString sVacName;
  TTagNode * LineNode = FIMM_S->GetTagByUID(APrivivUID);
  if (LineNode)
   sVacName = GetVakName(LineNode->GetParent()->AV["ref"].ToInt());

  return GetPrivivFullName(FIMM_S, sVacName, APrivivUID);
 }
//---------------------------------------------------------------------------
//��������� ������ �������� ��� ������� �������
bool __fastcall TdsSchEditForm::GetCurVakInfekList(TStrings * strings)
 {
  CheckActive(__FUNC__);
  if (FCurVacCode == -1)
   return false;

  return GetVakInfekList(strings, FCurVacCode);
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditForm::GetVakName(int nVakId)
 {
  CheckActive(__FUNC__);
  UnicodeString RC = "� � � � � �";
  try
   {
    switch (FSchemeObjType)
     {
     case TSchemeObjType::Vac:
      RC = FDM->VacName[IntToStr(nVakId)];
      break;
     case TSchemeObjType::Test:
      RC = FDM->TestName[IntToStr(nVakId)];
      break;
     case TSchemeObjType::Check:
      RC = FDM->CheckName[IntToStr(nVakId)];
      break;
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
//��������� ������ ���� ��� ��������� ������
bool __fastcall TdsSchEditForm::GetSchList(TStrings * strings, const TIntSet VakIds, TGetSchListOptions GetOptions)
 {
  CheckActive(__FUNC__);
  if (!strings)
   return false;

  strings->Clear();

  TTagNode * CurNode = FIMM_S->GetChildByName("schemalist")->GetFirstChild();
  int nCurNodeRef;
  UnicodeString CurSchName;
  UnicodeString * CurFullUID;
  while (CurNode)
   {
    nCurNodeRef = StrToInt(CurNode->AV["ref"]);

    if (VakIds.find(nCurNodeRef) != VakIds.end())
     {
      CurSchName = CurNode->AV["name"];
      if (GetOptions.Contains(slVakNames))
       CurSchName = CurSchName + " (" + GetVakName(nCurNodeRef) + ")";

      CurFullUID = NULL;
      if (GetOptions.Contains(slSchFullUIDs))
       {
        CurFullUID = new UnicodeString;
        (*CurFullUID) = CurNode->AV["uid"] + "." + CurNode->GetFirstChild()->AV["uid"];
       }
      strings->AddObject(CurSchName, (TObject *)CurFullUID);
     }
    CurNode = CurNode->GetNext();
   }
  return (bool)strings->Count;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::SchListTLSelectionChanged(TObject * Sender)
 {
  SchListTLClick(SchListTL);
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::SchListTLClick(TObject * Sender)
 {
  //���������� ������ ����� ��� ��������� �����
  FCurSchName = GetActiveSchemeAttrVal("name");
  UpdatePrivivList(GetActiveSchemeNode());
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::SchListTLEnter(TObject * Sender)
 {
  SchListRBar->Active = true;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::SchTVEnter(TObject * Sender)
 {
  SchRBar->Active = true;
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::ItemTLStylesGetContentStyle(TcxCustomTreeList * Sender,
    TcxTreeListColumn * AColumn, TcxTreeListNode * ANode, TcxStyle *& AStyle)

 {
  if (AColumn && ANode)
   {
    if (!GroupByInf->Checked || (GroupByInf->Checked && ANode->Level))
     AStyle = bStyle;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::ItemTLFocusedNodeChanged(TcxCustomTreeList * Sender,
    TcxTreeListNode * APrevFocusedNode, TcxTreeListNode * AFocusedNode)

 {
  FCurInfCode = FCurVacCode = -1;
  FCurInfName = FCurVacName = "";
  if (AFocusedNode && AFocusedNode->Data)
   {
    SchListTL->Clear();
    TTagNode * SelNode = NULL;
    if (!GroupByInf->Checked || (GroupByInf->Checked && AFocusedNode->Level))
     {//������� �����
      FCurVacCode = (int)AFocusedNode->Data;
      FCurVacName = AFocusedNode->Texts[0];
      if (GroupByInf->Checked)
       {
        FCurInfCode = (int)AFocusedNode->Parent->Data;
        FCurInfName = AFocusedNode->Parent->Texts[0];
       }
      else
       {
        FCurInfCode = 0;
        FCurInfName = "��� ����������� �� ���������";
       }

      //���������� ������ ���� ��� ��������� �������
      SchListTL->BeginUpdate();
      TTagNode * CurNode = FIMM_S->GetChildByName("schemalist");
      if (CurNode)
       {
        CurNode = CurNode->GetFirstChild();
        while (CurNode)
         {
          int nCurNodeRef = StrToInt(CurNode->AV["ref"]);
          if (nCurNodeRef == FCurVacCode)
           {
            if (!SelNode && FEditorBookmark && FEditorBookmark->sSchUID.Length())
             if (CurNode->AV["uid"] == FEditorBookmark->sSchUID)
              SelNode = CurNode;
            FAddSchData(CurNode->AV["name"], CurNode);
           }
          CurNode = CurNode->GetNext();
         }
       }
      SchListTL->EndUpdate();
     }
    else
     {//������� ��������
      FCurInfCode = (int)AFocusedNode->Data;
      FCurInfName = AFocusedNode->Texts[0];
     }
    UpdateSchemeActions();
    UpdateSchemeStatusPanel();
    /*
     if (SelNode)
     SchLB->ItemIndex = SchLB->Items->IndexOfObject(
     dynamic_cast<TTagNode*>(SelNode)
     );
     else
     SchLB->ItemIndex = 0;
     */

    if (SchListTL->Root->Count)
     SchListTL->Root->Items[0]->Focused = true;
    SchListTLClick(SchListTL);
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::GroupByInfPropertiesChange(TObject * Sender)
 {
  FillItemList();
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::actMIBPByAgeExecute(TObject * Sender)
 {
  TAgeMIBPForm * Dlg = new TAgeMIBPForm(this, FDM, IMM_S, 0);
  try
   {
    Dlg->ShowModal();
    if (Dlg->ModalResult == mrOk)
     XMLModified = true;
   }
  __finally
   {
    delete Dlg;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsSchEditForm::FormCloseQuery(TObject * Sender, bool & CanClose)
 {
  if (XMLModified && _MSG_QUE("��������� ��������� ���������?", "�������������") == IDYES)
   ApplyUpdates();
  CanClose = true;
 }
//---------------------------------------------------------------------------
