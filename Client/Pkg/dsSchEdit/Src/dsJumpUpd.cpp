/*
  ����        - JumpUpd.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ �������������� ��������� �� ����� ����.
                ���� ����������
  ����������� - ������� �.�., �������� �.�.
  ����        - 13.04.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DKUtils.h"
#include "DKMBUtils.h"

#include "dsJumpUpd.h"

#include "dsRefLine.h"
#include "dsSchCondList.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"

namespace Dsjumpupd
{

//###########################################################################
//##                                                                       ##
//##                              Globals                                  ##
//##                                                                       ##
//###########################################################################

//���������� � ������
typedef struct tagREFER_INFO
{
  int        nInfekID;          //ID ��������
  UnicodeString asRef;             //UID �������� ��� ��������
}REFER_INFO, *PREFER_INFO;

//---------------------------------------------------------------------------

//�������� ��� ��������������� ������ (��� ���� asRef ��������� REFER_INFO)
const UnicodeString csNullRef = "????";

} // end of namespace JumpUpd

//---------------------------------------------------------------------------

/*
 * ����� ����� � ��������� ������
 *
 *
 * ���������:
 *   [in]  Owner     - �������� ( fmPrivivUpd )
 *   [in]  AFUM      - ����� ������ �����
 *   [in]  AJumpKind - ��� ��������
 *
 * ������������ ��������:
 *   ModalResult �����
 *
 */

int Dsjumpupd::ShowModalJumpUpd( TComponent* Owner, TdsSchEditDM* ADM, TFormUpdateMode AFUM, TJumpKind AJumpKind )
{
  int nModalResult = mrNone;
  TfmJumpUpd *dlg = new TfmJumpUpd(Owner, ADM, AFUM, AJumpKind);
  if (dlg)
  {
    nModalResult = dlg->ShowModal();
    delete dlg;
  }
  return nModalResult;
}

//###########################################################################
//##                                                                       ##
//##                            TfmJumpUpd                                 ##
//##                                                                       ##
//###########################################################################

__fastcall TfmJumpUpd::TfmJumpUpd(TComponent* Owner, TdsSchEditDM* ADM, TFormUpdateMode AFUM, TJumpKind AJumpKind)
        : TForm(Owner)
{
  FDM = ADM;
  if ( AJumpKind == jkUnknown )
    throw DKClasses::EInvalidArgument(__FUNC__, "AJumpKind");

  FOwnFM    = (TfmPrivivUpd*)Owner;
  FTopOwnFM = FOwnFM->OwnFM;
  FFUM      = AFUM;
  FJumpKind = AJumpKind;
}

//---------------------------------------------------------------------------

__fastcall TfmJumpUpd::~TfmJumpUpd()
{
  for( int i = 0; i < vlRefList->Strings->Count; i++ )
    if ( vlRefList->Strings->Objects[i] )
      delete ((PREFER_INFO)vlRefList->Strings->Objects[i]);
}

//---------------------------------------------------------------------------

void __fastcall TfmJumpUpd::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif

  cbFilter->Style->Color = clInfoBk;//AppOptions->ReqEdBkColor;
  edActualTime->Color    = clInfoBk;//AppOptions->ReqEdBkColor;

  //����������� ������� ��������� �����
  switch ( FFUM )
  {
    case fumADD :
      Caption = "����������";
      switch ( FJumpKind )
      {
        case jkSrok : Caption = Caption + " �������� �� �����";     break;
        case jkAge  : Caption = Caption + " �������� �� ��������";  break;
        case jkCond : Caption = Caption + " �������� �� �������";   break;
        case jkEnd  : Caption = Caption + " ������������ ��������"; break;
      }
    break;
    case fumEDIT:
      Caption = "���������";
      switch ( FJumpKind )
      {
        case jkSrok : Caption = Caption + " �������� � �������� �� �����";    break;
        case jkAge  : Caption = Caption + " �������� � �������� �� ��������"; break;
        case jkCond : Caption = Caption + " �������� � �������� �� �������";  break;
        case jkEnd  : Caption = Caption + " �������� � ����������� ��������"; break;
      }
    break;
  }

  //��������� �������� ���� ����� � ����������� �� ���� �������������� ��������
  int nDelta;
  switch ( FJumpKind )
  {
    case jkSrok :
      lbFilterTitle->Visible = false;
      cbFilter->Visible      = false;
      btnConditionListEdit->Visible = false;
      switch (FTopOwnFM->SchemeObjType)
      {
        case TSchemeObjType::Vac  : lbActualTimeTitle->Caption = "����� �������� �������� (���/��/��):"; break;
        case TSchemeObjType::Test : lbActualTimeTitle->Caption = "����� �������� ����� (���/��/��):"; break;
        case TSchemeObjType::Check: lbActualTimeTitle->Caption = "����� �������� �������� (���/��/��):"; break;
      }
      edActualTime->Left = lbActualTimeTitle->Left +
                           lbActualTimeTitle->Canvas->TextWidth( lbActualTimeTitle->Caption ) + 5;
    break;
    case jkAge :
      lbFilterTitle->Visible = false;
      cbFilter->Visible      = false;
      btnConditionListEdit->Visible = false;
      lbActualTimeTitle->Caption = "������� (���/��/��):";
      edActualTime->Left = lbActualTimeTitle->Left +
                           lbActualTimeTitle->Canvas->TextWidth( lbActualTimeTitle->Caption ) + 5;
    break;
    case jkCond :
      lbActualTimeTitle->Visible = false;
      edActualTime     ->Visible = false;
      cbFilter->Properties->Items->Clear();
      cbFilter->Properties->Items->Assign(FTopOwnFM->FilterList);
    break;
    case jkEnd  :
      Height = Height - pTop->Height;
      pTop->Visible = false;
    break;
  }

  //���������� ������ ��������
  UnicodeString sRefVal, sRefLabel;
  switch (FFUM)
  {
    case fumADD:
      sRefVal   = EmptyRef;
      sRefLabel = csEndPlanLabel;
    break;
    case fumEDIT:
      sRefVal   = csNullRef;
      sRefLabel = csNullRefLabel;
    break;
  }
  FOwnFM->GetPrivivInfekList( vlRefList->Strings );
  for( int i = 0; i < vlRefList->Strings->Count; i++ )
  {
    vlRefList->Strings->Strings[i] = vlRefList->Strings->Strings[i] + "=" + sRefLabel;
    int nInfekId = (int)vlRefList->Strings->Objects[i];
    vlRefList->Strings->Objects[i] = (TObject*)new REFER_INFO;
    ((PREFER_INFO)vlRefList->Strings->Objects[i])->nInfekID = nInfekId;
    ((PREFER_INFO)vlRefList->Strings->Objects[i])->asRef    = sRefVal;
  }

  //���������� ����� ������� ��� ��������������
  int Idx = -1;
  if ( FFUM == fumEDIT )
  {
    TTagNode *JumpNode = (TTagNode*)FOwnFM->Jumps->Selected->Data;
    TTagNode *RefListNode;
    switch ( FJumpKind )
    {
      case jkSrok :
        edActualTime->Text = JumpNode->AV["actualtime"];
        RefListNode = JumpNode;
      break;
      case jkAge :
        edActualTime->Text = JumpNode->AV["max_age"];
        RefListNode = JumpNode;
      break;
      case jkCond :
        Idx = cbFilter->Properties->Items->IndexOfObject((TObject*)(UIDInt(JumpNode->AV["ref"])));
        cbFilter->ItemIndex = Idx;
        cbFilter->Update();
        RefListNode = JumpNode->GetChildByName("reflist");
      break;
      case jkEnd  :
        RefListNode = JumpNode;
      break;
    }
    TTagNode *RefNode = RefListNode->GetFirstChild();
    while( RefNode )
    {
      int nStrInd = GetRefListStrInd(StrToInt(RefNode->AV["infref"]));
      UnicodeString sRefVal = RefNode->AV["ref"];
      vlRefList->Cells[1][nStrInd+1] = FTopOwnFM->GetPrivivFullName(RefNode->AV["ref"]);
      ((PREFER_INFO)vlRefList->Strings->Objects[nStrInd])->asRef = sRefVal;
      RefNode = RefNode->GetNext();
    }
  }

  UpdateActions();
}

//---------------------------------------------------------------------------

bool __fastcall TfmJumpUpd::AreInputsCorrect()
{
  UnicodeString ExAgeString;
  switch ( FJumpKind )
  {
    case jkSrok:
      if ( !IsCorrectAgeString( edActualTime->Text, ExAgeString, false ) )
      {
        switch (FTopOwnFM->SchemeObjType)
        {
          case TSchemeObjType::Vac:
			MB_SHOW_ERROR_CPT( L"����� �������� ���� �������� ������ ���� ������� � ������� \"���/��/��\".\n"
							   L"���\t - ���������� ��� ( 0 - 999 )\n"
							   L"��\t - ���������� ������� ( 0 - 12 )\n"
							   L"��\t - ���������� ���� ( 0 - 31 ).",
							   L"������ �����");
		  break;
		  case TSchemeObjType::Test:
			MB_SHOW_ERROR_CPT( L"����� �������� ���� ����� ������ ���� ������� � ������� \"���/��/��\".\n"
							   L"���\t - ���������� ��� ( 0 - 999 )\n"
							   L"��\t - ���������� ������� ( 0 - 12 )\n"
							   L"��\t - ���������� ���� ( 0 - 31 ).",
                               L"������ �����");
          break;
        }
        return false;
      }
      edActualTime->Text = ExAgeString;
    break;

    case jkAge:
      if ( !IsCorrectAgeString( edActualTime->Text, ExAgeString, false ) )
      {
		MB_SHOW_ERROR_CPT( L"������� ������ ���� ������ � ������� \"���/��/��\".\n"
						   L"���\t - ���������� ��� ( 0 - 999 )\n"
						   L"��\t - ���������� ������� ( 0 - 12 )\n"
						   L"��\t - ���������� ���� ( 0 - 31 ).",
						   L"������ �����");
		return false;
	  }
	  edActualTime->Text = ExAgeString;
	break;

	case jkCond:
	  if ( cbFilter->ItemIndex == -1 )
	  {
        MB_SHOW_ERROR_CPT(L"���������� ������� ������ �� ������.", L"������ �����");
        return false;
      }
    break;
  }

  int nNullRefCount = 0;
  for ( int i = 0; i < vlRefList->Strings->Count; i++ )
    if ( ((PREFER_INFO)vlRefList->Strings->Objects[i])->asRef == csNullRef )
      nNullRefCount++;

  if (nNullRefCount)
  {
	MB_SHOW_ERROR_CPT(L"���������� ������� �������� ��� ���� ����������.", L"������ �����");
	return false;
  }

  return true;
}

//---------------------------------------------------------------------------

void __fastcall TfmJumpUpd::UpdateActions()
{
  actEditRef ->Enabled = (bool)vlRefList->Strings->Count;
}

//---------------------------------------------------------------------------

//���������� ����� ������ vlRefList'� �� ���� ��������

int __fastcall TfmJumpUpd::GetRefListStrInd( int nInfekInd )
{
  int Result = -1;
  for ( int i = 0; i < vlRefList->Strings->Count; i++ )
    if ( ((PREFER_INFO)vlRefList->Strings->Objects[i])->nInfekID == nInfekInd )
    {
      Result = i;
      break;
    }
  return Result;
}

//---------------------------------------------------------------------------
//---------------------- ��������� ���������� ������ ValueListEditor'�� -----
//---------------------- � ������ goRowSelect                           -----
//---------------------------------------------------------------------------

/*
  ������������ ����
    vl->Row = <������ ����� ������>;
  ��� ������� ����� goRowSelect �� �������� :( ( �� ���������� ������).
  ������ ������� �� ������������ ����������� ������� CustomGrid'� �� WheelUp, WheelDown � KeyDown
  ( �.�. �������� ������ ��������� �� �������� ���� ��������� ��� ������� ������ �� ���������,
    ������ ���� Home � ������������� ��������� ).
*/

void __fastcall TfmJumpUpd::SetVLEditorSelection( TValueListEditor *vl, int nTop, int nBottom, int nLeft, int nRight )
{
  //��������� �������� ������ ValueListEditor'�
  TGridRect GridRect;
  GridRect.Top    = nTop;
  GridRect.Bottom = nBottom;
  GridRect.Left   = nLeft;
  GridRect.Right  = nRight;
  vl->Selection = GridRect;

  //������ ����� �������� ������ �������
  TRect CellRect    = vl->CellRect( vl->Col, vl->Row );
  if ( ( HEIGHTOF_TRECT(CellRect) != vl->RowHeights[ vl->Row ] ) ||
       ( WIDTHOF_TRECT (CellRect) != vl->ColWidths [ vl->Col ] ) )
  {
    if ( vl->TopRow < vl->Row )
      vl->TopRow = vl->Row - vl->VisibleRowCount + 1;
    else
      vl->TopRow = vl->Row;
  }
}

//---------------------------------------------------------------------------

int __fastcall TfmJumpUpd::MoveVLEditorSelectionPrev( TValueListEditor *vl )
{
  if ( vl->Row > 1 )
    SetVLEditorSelection( vl, vl->Row - 1, vl->Row - 1, 1, 0 );
  return vl->Row;
}

//---------------------------------------------------------------------------

int __fastcall TfmJumpUpd::MoveVLEditorSelectionNext( TValueListEditor *vl )
{
  if ( vl->Row < vl->RowCount - 1 )
    SetVLEditorSelection( vl, vl->Row + 1, vl->Row + 1, 1, 0 );
  return vl->Row;
}

//---------------------------------------------------------------------------

int __fastcall TfmJumpUpd::MoveVLEditorSelectionFirst( TValueListEditor *vl )
{
  if ( vl->RowCount > 1 )
    SetVLEditorSelection( vl, 1, 1, 1, 0 );
  return vl->Row;
}

//---------------------------------------------------------------------------

int __fastcall TfmJumpUpd::MoveVLEditorSelectionLast( TValueListEditor *vl )
{
  if ( vl->RowCount > 1 )
    SetVLEditorSelection( vl, vl->RowCount - 1, vl->RowCount - 1, 1, 0 );
  return vl->Row;
}

//---------------------------------------------------------------------------

int  __fastcall TfmJumpUpd::MoveVLEditorSelectionPgUp ( TValueListEditor *vl )
{
  if ( vl->RowCount > 1 )
    SetVLEditorSelection( vl, vl->TopRow, vl->TopRow, 1, 0 );
  return vl->Row;
}

//---------------------------------------------------------------------------

int  __fastcall TfmJumpUpd::MoveVLEditorSelectionPgDwn( TValueListEditor *vl )
{
  if ( vl->RowCount > 1 )
  {
    int nNewRow = vl->TopRow + vl->VisibleRowCount - 1;
    SetVLEditorSelection( vl, nNewRow, nNewRow, 1, 0 );
  }
  return vl->Row;
}

//---------------------------------------------------------------------------
//---------------------- ������ �  ValueListEditor'��� ----------------------
//---------------------------------------------------------------------------

void  __fastcall TfmJumpUpd::SetVLEditorEnable( TValueListEditor *vl, bool bEnable )
{
  if ( vl->Enabled != bEnable )
  {
    vl->Enabled = bEnable;
    if ( bEnable )
    {
      vl->Color       = clWindow;
      vl->Font->Color = clWindowText;
    }
    else
    {
      vl->Color       = clBtnFace;
      vl->Font->Color = clGrayText;
    }
  }
}

//***************************************************************************
//********************** ����������� �������*********************************
//***************************************************************************

void __fastcall TfmJumpUpd::bApplyClick(TObject *Sender)
{
  if ( !AreInputsCorrect() )
    return;

  TTreeNodes *TreeNodes = FOwnFM->Jumps->Items;
  TTagNode *LineNode = (TTagNode*)TreeNodes->Item[0]->Data;
  TTreeNode *TreeNode = NULL;
  TTagNode *JumpNode = NULL;
  TTagNode *RefListNode = NULL;
  TTagNode *TmpNode;
  TTreeNode *TmpTreeNode;

  //�������������� ��������������� ����� TagNode � TreeNode
  TreeNodes->BeginUpdate();
  switch ( FFUM )
  {
    case fumADD :
      switch( FJumpKind )
      {
        case jkSrok:
          TmpNode = LineNode->GetFirstChild();
          if ( TmpNode )
            JumpNode = LineNode->InsertChild(TmpNode, "srokjump", true);
          else
            JumpNode = LineNode->AddChild("srokjump");
          JumpNode->AV["actualtime"] = edActualTime->Text;
          RefListNode = JumpNode;
          TreeNode = TreeNodes->AddChildFirst( TreeNodes->Item[0], "");
        break;

        case jkAge:
          TmpNode = LineNode->GetFirstChild();
          if ( TmpNode )
          {
            if ( TmpNode->CmpName("srokjump") )
              JumpNode = LineNode->InsertChild(TmpNode, "agejump", false);
            else
              JumpNode = LineNode->InsertChild(TmpNode, "agejump", true);
          }
          else
            JumpNode = LineNode->AddChild("agejump");
          JumpNode->AV["max_age"] = edActualTime->Text;
          RefListNode = JumpNode;
          TmpTreeNode = TreeNodes->Item[0]->getFirstChild();
          if ( TmpTreeNode )
          {
            if ( ((TTagNode*)TmpTreeNode->Data)->CmpName("srokjump") )
            {
              TmpTreeNode = TmpTreeNode->getNextSibling();
              if ( TmpTreeNode )
                TreeNode = TreeNodes->Insert( TmpTreeNode, "");
              else
                TreeNode = TreeNodes->AddChild( TreeNodes->Item[0], "");
            }
            else
              TreeNode = TreeNodes->AddChildFirst( TreeNodes->Item[0], "");
          }
          else
            TreeNode = TreeNodes->AddChildFirst( TreeNodes->Item[0], "");
        break;

        case jkCond:
          TmpNode = LineNode->GetLastChild();
          if ( TmpNode )
          {
            if ( TmpNode->CmpName("endjump") )
              JumpNode = LineNode->InsertChild(TmpNode, "jump", true);
            else
              JumpNode = LineNode->AddChild("jump");
          }
          else
            JumpNode = LineNode->AddChild("jump");
          JumpNode->AV["ref"] = UIDStr((int)cbFilter->Properties->Items->Objects[cbFilter->ItemIndex]);
          RefListNode = JumpNode->AddChild("reflist");
          TmpTreeNode = TreeNodes->Item[0]->GetLastChild();
          if ( TmpTreeNode )
          {
            if ( ((TTagNode*)TmpTreeNode->Data)->CmpName("endjump") )
              TreeNode = TreeNodes->Insert( TmpTreeNode, "");
            else
              TreeNode = TreeNodes->AddChild( TreeNodes->Item[0], "");
          }
          else
            TreeNode = TreeNodes->AddChild( TreeNodes->Item[0], "");
        break;

        case jkEnd :
          RefListNode = JumpNode = LineNode->AddChild("endjump");
          TreeNode = TreeNodes->AddChild( TreeNodes->Item[0], "");
        break;
      }
      FOwnFM->Jumps->Select(TreeNode);
    break;
    case fumEDIT:
      RefListNode = JumpNode = (TTagNode*)FOwnFM->Jumps->Selected->Data;
      switch( FJumpKind )
      {
        case jkSrok:
          JumpNode->AV["actualtime"] = edActualTime->Text;
        break;
        case jkAge:
          JumpNode->AV["max_age"] = edActualTime->Text;
        break;
        case jkCond:
          JumpNode->AV["ref"] = UIDStr((int)cbFilter->Properties->Items->Objects[cbFilter->ItemIndex]);
          RefListNode = JumpNode->GetChildByName( "reflist" );
        break;
        case jkEnd :
        break;
      }
      RefListNode->DeleteChild();
      FOwnFM->Jumps->Selected->DeleteChildren();
    break;
  }

  //���������� ������
  for ( int i = 0; i < vlRefList->Strings->Count; i++ )
  {
    PREFER_INFO pReferInfo = (PREFER_INFO)vlRefList->Strings->Objects[i];
    if ( pReferInfo->asRef != csNullRef )
    {
      TTagNode *NewNode = RefListNode->AddChild("refer");
      NewNode->AV["infref"] = IntToStr(pReferInfo->nInfekID);
      NewNode->AV["ref"]    = pReferInfo->asRef;
    }
  }

  //����������� TagNode �� TreeNode
  CopyTagNodeToTree(JumpNode,  FOwnFM->Jumps->Selected, FOwnFM->JumpsCopyItem);
//  JumpNode->OnCopyItem = FOwnFM->JumpsCopyItem;
//  JumpNode->ToTree( FOwnFM->Jumps->Selected );
//  JumpNode->OnCopyItem = NULL;
  FOwnFM->KillRefListNodes(FOwnFM->Jumps->Selected);
  FOwnFM->ReferTreeNodeSort(FOwnFM->Jumps->Selected);
  FOwnFM->Jumps->Selected->Expand(true);
  TreeNodes->EndUpdate();

  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TfmJumpUpd::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TfmJumpUpd::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}

//---------------------------------------------------------------------------

void __fastcall TfmJumpUpd::actEditRefExecute(TObject *Sender)
{
  PREFER_INFO pReferInfo = (PREFER_INFO)vlRefList->Strings->Objects[vlRefList->Row - 1];
  UnicodeString NewRef = ShowModalRefLine(
    FTopOwnFM,
    pReferInfo->nInfekID,
    vlRefList->Cells[0][vlRefList->Row],
    pReferInfo->asRef
  );
  if ( NewRef != "" )
  {
    pReferInfo->asRef = NewRef;
    vlRefList->Cells[1][vlRefList->Row] = FTopOwnFM->GetPrivivFullName(NewRef);
  }
}

//---------------------- ��������� ���������� ������ ValueListEditor'�� -----
//---------------------- � ������ goRowSelect + ������� ������          -----
//---------------------- ������ ����� ����� �������� ������             -----
//---------------------------------------------------------------------------

void __fastcall TfmJumpUpd::vlMouseWheelDown(TObject *Sender,
      TShiftState Shift, TPoint &MousePos, bool &Handled)
{
  MoveVLEditorSelectionNext( (TValueListEditor*)Sender );
  Handled = true;
}

//---------------------------------------------------------------------------

void __fastcall TfmJumpUpd::vlMouseWheelUp(TObject *Sender,
      TShiftState Shift, TPoint &MousePos, bool &Handled)
{
  MoveVLEditorSelectionPrev( (TValueListEditor*)Sender );
  Handled = true;
}

//---------------------------------------------------------------------------

void __fastcall TfmJumpUpd::vlKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  TValueListEditor *vlSender = (TValueListEditor*)Sender;
  switch ( Key )
  {
    case VK_LEFT   :
    case VK_UP     :
    case VK_NUMPAD4:
    case VK_NUMPAD8:
      MoveVLEditorSelectionPrev( vlSender );
    break;
    case VK_RIGHT  :
    case VK_DOWN   :
    case VK_NUMPAD6:
    case VK_NUMPAD2:
      MoveVLEditorSelectionNext( vlSender );
    break;
    case VK_HOME   :
    case VK_NUMPAD7:
      MoveVLEditorSelectionFirst( vlSender );
    break;
    case VK_END    :
    case VK_NUMPAD1:
      MoveVLEditorSelectionLast( vlSender );
    break;
    case VK_PRIOR  :
    case VK_NUMPAD9:
      MoveVLEditorSelectionPgUp( vlSender );
    break;
    case VK_NEXT   :
    case VK_NUMPAD3:
      MoveVLEditorSelectionPgDwn( vlSender );
    break;
    case VK_RETURN :
      if ( Shift.Contains(ssShift) )
        vlDblClick(Sender);
    break;
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmJumpUpd::vlMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  if ( Button == mbRight )
  {
    TValueListEditor *vlSender = (TValueListEditor*)Sender;
    int ACol, ARow;
    vlSender->MouseToCell( X, Y, ACol, ARow );
    SetVLEditorSelection( vlSender, ARow, ARow, 1, 0 );
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmJumpUpd::vlDblClick(TObject *Sender)
{
  TValueListEditor *vlSender = (TValueListEditor*)Sender;
  if (vlSender->Name == "vlRefList")
    actEditRef->Execute();
}

//---------------------------------------------------------------------------
//---------------------- ��������� ����� ValueListEditor'�� -----------------
//---------------------------------------------------------------------------

void __fastcall TfmJumpUpd::vlDrawCell(TObject *Sender, int ACol,
      int ARow, TRect &Rect, TGridDrawState State)
{
  TValueListEditor *vlSender = (TValueListEditor*)Sender;
  if ( !State.Contains(gdFixed) )
  {
    TColor clOldFontColor = vlSender->Canvas->Font ->Color;
    if ( vlSender->Enabled )
    {
      if ( vlSender->Strings->Count )
      {
        bool bNoneRef = ( vlSender->Cells[ACol][ARow] == csNullRefLabel );
        if ( State.Contains(gdSelected) )
        {
          if ( vlSender->Focused() )
          {
            vlSender->Canvas->Brush->Color = clHighlight;
            vlSender->Canvas->Font ->Color = ( bNoneRef ) ? clYellow : clHighlightText;
          }
          else
          {
            vlSender->Canvas->Brush->Color = clBtnFace;
            vlSender->Canvas->Font ->Color = ( bNoneRef ) ? clRed : clWindowText;
          }
        } //if ( State.Contains(gdSelected) )
        else if ( bNoneRef )
          vlSender->Canvas->Font ->Color = clRed;
      } //if ( vlSender->Strings->Count )
      else
      {
        vlSender->Canvas->Brush->Color = vlSender->Color;
        vlSender->Canvas->Font ->Color = vlSender->Font->Color;
      }
    } //if ( vlSender->Enabled )
    else
    {
      vlSender->Canvas->Brush->Color = vlSender->Color;
      vlSender->Canvas->Font ->Color = vlSender->Font->Color;
    }
    vlSender->Canvas->FillRect(Rect);
    vlSender->Canvas->TextRect(Rect, Rect.Left+2, Rect.Top+2, vlSender->Cells[ACol][ARow] );
    vlSender->Canvas->Font ->Color = clOldFontColor;
  } //  if ( !State.Contains(gdFixed) )
}

//---------------------------------------------------------------------------

void __fastcall TfmJumpUpd::btnConditionListEditClick(TObject *Sender)
{
  int Idx = 0;
  if (cbFilter->ItemIndex != -1)
   Idx = (int)cbFilter->Properties->Items->Objects[cbFilter->ItemIndex];

  TCondListForm *Dlg = new TCondListForm(FTopOwnFM, FDM, FTopOwnFM->IMM_S, Idx);
  try
  {
    Dlg->ShowModal();
    if (Dlg->UpdateList)
    {
      FTopOwnFM->UpdateFilterList();
      cbFilter->Properties->Items->Assign(FTopOwnFM->FilterList);
    }
  }
 __finally
  {
    delete Dlg;
  }
}

//---------------------------------------------------------------------------
