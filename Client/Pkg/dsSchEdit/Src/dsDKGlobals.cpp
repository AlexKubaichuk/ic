/*
  ����        - DKGlobals.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ����������.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 12.03.2004
*/
//---------------------------------------------------------------------------

#pragma hdrstop

#include "DKFileUtils.h"

#include "dsDKGlobals.h"
#include "ICSDocGlobals.h"
#include "icsDateUtil.hpp"
//#include "RxDateUtil.hpp"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//###########################################################################
//##                                                                       ##
//##                             ������ ����                               ##
//##                                                                       ##
//###########################################################################

const int Dkglobals::DKCodeVersionMajor   = 3;
const int Dkglobals::DKCodeVersionMinor   = 0;
const int Dkglobals::DKCodeVersionRelease = 2;
const int Dkglobals::DKCodeVersionBuild   = 0;
const TVersionState Dkglobals::DKCodeVersionState = vsRelease;
const int Dkglobals::DKCodeVersionStateNumber = 0;

//---------------------------------------------------------------------------

UnicodeString __fastcall Dkglobals::DKCodeVersion()
{
  UnicodeString sVersion = Format(
    "%d.%d.%d.%d",
    ARRAYOFCONST((
      (int)DKCodeVersionMajor,
      (int)DKCodeVersionMinor,
      (int)DKCodeVersionRelease,
      (int)DKCodeVersionBuild
    ))
  );
  #pragma warn -ccc
  #pragma warn -rch
  if (DKCodeVersionState != vsRelease)
  {
    sVersion += ".";
    switch (DKCodeVersionState)
    {
      case vsAlpha: sVersion += "alpha"; break;
      case vsBeta : sVersion += "beta";  break;
      case vsRC   : sVersion += "rc";    break;
    }
    if (DKCodeVersionStateNumber)
      sVersion += "." + IntToStr(DKCodeVersionStateNumber);
  }
  #pragma warn .rch
  #pragma warn .ccc
  return sVersion;
}

//###########################################################################
//##                                                                       ##
//##                             ���������                                 ##
//##                                                                       ##
//###########################################################################

const UnicodeString Dkglobals::TablePK = "CODE";


const UnicodeString Dkglobals::RusShortMonthNames[] = { "���", "���", "���", "���", "���", "���", "���", "���", "���", "���", "���", "���" };
const UnicodeString Dkglobals::RusLongMonthNames[]  = { "������", "�������", "����", "������", "���", "����", "����", "������", "��������", "�������", "������", "�������" };
const int Dkglobals::RusMonthCount = 12;
const UnicodeString Dkglobals::WFNScheme[]   = { "����"    , "�����"   , "�����"    };
const UnicodeString Dkglobals::WFNPrivivka[] = { "��������", "��������", "��������" };
const UnicodeString Dkglobals::WFNTest[]     = { "����"    , "�����"   , "�����"    };
const UnicodeString Dkglobals::WFNCheck[]    = { "��������", "��������", "��������"    };

//###########################################################################
//##                                                                       ##
//##                             ����������                                ##
//##                                                                       ##
//###########################################################################

//TICAppOptions* Dkglobals::AppOptions;

//###########################################################################
//##                                                                       ##
//##                              �������                                  ##
//##                                                                       ##
//###########################################################################

/*int __fastcall Dkglobals::FocusListViewItem( TListView *lv, int nNewItemIndex )
{
  if ( !lv->Items->Count )
    return -1;

  if      ( nNewItemIndex < 0 )
    nNewItemIndex = 0;
  else if ( nNewItemIndex >= lv->Items->Count )
    nNewItemIndex = lv->Items->Count - 1;

  lv->ItemIndex   = nNewItemIndex;
  lv->ItemFocused = lv->Items->Item[lv->ItemIndex];
  return lv->ItemIndex;
}  */

//---------------------------------------------------------------------------
/*
TListItem* __fastcall Dkglobals::AddListViewItem( TListView *lv, UnicodeString Caption, TObject *Data, int ImageIndex )
{
  TListItem *NewListItem = lv->Items->Add();
  if ( NewListItem )
  {
    NewListItem->Caption    = Caption;
    NewListItem->Data       = Data;
    NewListItem->ImageIndex = ImageIndex;
  }
  return NewListItem;
}      */

//---------------------------------------------------------------------------

UnicodeString __fastcall Dkglobals::DurationToStr(const int AYears, const int AMonths, const int ADays)
{
  UnicodeString sStr;

  if (AYears)
  {
    if (sStr.Length())
      sStr += " ";
    sStr += IntToStr(AYears) + GetWordForNumber(AYears, WFNYear)[1] + ".";
  }
  if (AMonths)
  {
    if (sStr.Length())
      sStr += " ";
    sStr += IntToStr(AMonths) + "�.";
  }
  if (ADays)
  {
    if (sStr.Length())
      sStr += " ";
    sStr += IntToStr(ADays) + "�.";
  }

  if (!sStr.Length())
    sStr = "0�.";
  return sStr;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall Dkglobals::XMLStrDurationToStr(UnicodeString AValue)
{
  AValue = AValue.Trim();
  UnicodeString sStr;
  if (AValue.Length())
  {
    int nYears, nMonths, nDays;
    XMLStrToDuration(AValue, &nYears, &nMonths, &nDays);
    sStr = DurationToStr(nYears, nMonths, nDays);
  }
  return sStr;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall Dkglobals::DateDurationToStr(TDate ADate1, TDate ADate2)
{
  Word wYears, wMonths, wDays;
  DateDiff(ADate1, ADate2, wDays, wMonths, wYears);
  return DurationToStr(wYears, wMonths, wDays);
}

//---------------------------------------------------------------------------

UnicodeString __fastcall Dkglobals::GetSeasonDiap ( UnicodeString SeasonBeg, UnicodeString SeasonEnd )
{
  UnicodeString Result = "";
  int nSeasonBeg = StrToIntDef( SeasonBeg, -1 );
  int nSeasonEnd = StrToIntDef( SeasonEnd, -1 );
  if ( ( nSeasonBeg != -1 ) && ( nSeasonEnd != -1 ) )
  {
    Result = RusShortMonthNames[nSeasonBeg] + ".";
    if ( nSeasonBeg != nSeasonEnd )
      Result += " - " + RusShortMonthNames[nSeasonEnd] + ".";
  }
  return Result;
}

//---------------------------------------------------------------------------

bool __fastcall Dkglobals::TagNode_IterateExcl(
  TTagNode   *CurNode,
  UnicodeString *ExclNodeUID,
  TIterate1  ItItem,
  void       *Src1,
  void       *Src2,
  void       *Src3,
  void       *Src4
)
{
 if ( ( !CurNode ) || ( !ExclNodeUID ) )
   return true;

 TAttr *UIDAttr = CurNode->GetAttrByName("uid");
 if ( UIDAttr )
   if ( UIDAttr->Value == (*ExclNodeUID) )
     return false;

 if ( ItItem(CurNode, Src1, Src2, Src3, Src4) )
   return true;

 CurNode = CurNode->GetFirstChild();
 while ( CurNode )
 {
   if ( TagNode_IterateExcl( CurNode, ExclNodeUID, ItItem, Src1, Src2, Src3, Src4 ) )
     return true;
   CurNode = CurNode->GetNext();
 }
 return false;
}

//---------------------------------------------------------------------------
bool __fastcall Dkglobals::IsNumberStr ( UnicodeString Str, UnicodeString *ExStr, int* Val )
{
  bool bResult;
  if ( ExStr ) (*ExStr) = "";
  if ( Val )   (*Val)   = 0;

  try
  {
    int temp = StrToInt ( Str );
    if ( ExStr ) (*ExStr) = IntToStr( temp );
    if ( Val )   (*Val)   = temp;
    bResult = true;
  }
  catch (EConvertError&)
  {
    bResult = false;
  }
  return bResult;
}
//---------------------------------------------------------------------------

bool __fastcall Dkglobals::IsCorrectAgeString ( UnicodeString AgeString, UnicodeString &ExAgeString, bool bCanBeEmpty )
{
  bool bSectionsOk = false;
  ExAgeString = "";
  AgeString = Trim( AgeString );

  if ( !AgeString.Length() )
    return bCanBeEmpty;

  wchar_t *szAgeString = new wchar_t[ AgeString.Length() + 1 ];
  if ( szAgeString )
  {
    szAgeString[0] = '\0';
    wcscpy(szAgeString, AgeString.c_str() );

    UnicodeString Sections[3];
    wchar_t *p;
    int i = -1;

    p = wcstok( szAgeString, L"/" );
    while(p)
    {
      i++;
      if ( i >= 3 )
        break;
      Sections[i] = p;
      p = wcstok( NULL, L"/" );
    }
    delete [] szAgeString;
    if ( i == 2 )
    {
      UnicodeString ExStr;
      int SectionVal;
      int nMaxSectionLen;
      int nMinSectionLen = 1;
      bSectionsOk = true;
      for ( int j = 0; j < 3; j++ )
      {
        nMaxSectionLen = ( j ) ? 2 : 3;
        if ( ( Sections[j].Length() < nMinSectionLen ) ||
             ( Sections[j].Length() > nMaxSectionLen ) ||
             ( !IsNumberStr ( Sections[j], &ExStr, &SectionVal ) ) )
        {
          bSectionsOk = false;
          break;
        }
        if ( ( ( j == 1 ) && ( SectionVal > 12 ) ) ||
             ( ( j == 2 ) && ( SectionVal > 31 ) ) )
        {
          bSectionsOk = false;
          break;
        }

        if ( ExAgeString != "" )
          ExAgeString += "/";
        ExAgeString += ExStr;
      }
    }
  }

  return bSectionsOk;
}

//---------------------------------------------------------------------------
/*
int __fastcall Dkglobals::DKCorrectDay( Word Year, Word Month, Word Day )
{
  Word DaysCount = DaysInAMonth(Year,Month);
  if ( Day > DaysCount )
    Day = DaysCount;
  return Day;
}
  */
//---------------------------------------------------------------------------
/*
bool __fastcall Dkglobals::DKIsDateInPeriod( TDate Date, TDate PeriodBeg, TDate PeriodEnd )
{
  return ( ( ( CompareDate( Date, PeriodEnd ) == LessThanValue    ) ||
             ( CompareDate( Date, PeriodEnd ) == EqualsValue      ) ) &&
           ( ( CompareDate( Date, PeriodBeg ) == GreaterThanValue ) ||
             ( CompareDate( Date, PeriodBeg ) == EqualsValue      ) ) );
}
*/
//---------------------------------------------------------------------------
/*
int __fastcall Dkglobals::AgeCmp( Word Years1, Word Months1, Word Days1, Word Years2, Word Months2, Word Days2 )
{
  int Result;
  if      ( Years1  < Years2  ) Result = -1;
  else if ( Years1  > Years2  ) Result = 1;
  else if ( Months1 < Months2 ) Result = -1;
  else if ( Months1 > Months2 ) Result = 1;
  else if ( Days1   < Days2   ) Result = -1;
  else if ( Days1   > Days2   ) Result = 1;
  else                          Result = 0;
  return Result;
}
*/
//###########################################################################
//##                                                                       ##
//##                            TICAppOptions                                ##
//##                                                                       ##
//###########################################################################

//const UnicodeString TICAppOptions::csProgRegKeyBase       = "SOFTWARE\\ICS Ltd.\\IC\\V6";
//const UnicodeString TICAppOptions::csCurrentVersionRegKey = "CurrentVersion";
//const UnicodeString TICAppOptions::csUserSettingRegKey    = "UserSetting";
//const UnicodeString TICAppOptions::csTemplateDir          = "Template";

//const UnicodeString TICAppOptions::csMSWordDotFileName   = "ICS_Word.dot";
//const UnicodeString TICAppOptions::csMSTmpDocFileName    = "report_z.doc";
//const UnicodeString TICAppOptions::csMSTmpDocTmpFileName = "~$port_z.doc";

//---------------------------------------------------------------------------
/*
__fastcall TICAppOptions::TICAppOptions(bool fLoad) :
  TObject(),
  FDefRootPath(ExcludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)))),
  FDefReqEdBkColor(clInfoBk),
  FDefReqEdBkAltColor(static_cast<TColor>(0x0090FFFF))
{
  FRootPath     = FDefRootPath;
  FReqEdBkColor = FDefReqEdBkColor;
  UpdatePaths();
  if (fLoad)
    Load();
}
//---------------------------------------------------------------------------
__fastcall TICAppOptions::~TICAppOptions()
{
}
//---------------------------------------------------------------------------
/*
void __fastcall TICAppOptions::UpdatePaths()
{
  FTemplatePath        = AppendFileName(FRootPath, csTemplateDir);
  FMSWordDotFileName   = AppendFileName(FTemplatePath, csMSWordDotFileName);
  FMSTmpDocFileName    = AppendFileName(FRootPath, csMSTmpDocFileName);
  FMSTmpDocTmpFileName = AppendFileName(FRootPath, csMSTmpDocTmpFileName);
}

//---------------------------------------------------------------------------

void __fastcall TICAppOptions::Load()
{
  TRegistry *Registry = new TRegistry(KEY_READ);
  try
  {
    Registry->RootKey = HKEY_LOCAL_MACHINE;
    #ifdef DEVELOP_MODE
      FRootPath = FDefRootPath + "\\..\\..";
    #else
      if (Registry->OpenKey(csProgRegKeyBase + "\\" + csCurrentVersionRegKey, false))
      {
        FRootPath = Registry->ReadString("AppPath").Trim();
        if (!FRootPath.Length())
          FRootPath = FDefRootPath;
        Registry->CloseKey();
      }
    #endif
    UpdatePaths();
    if (Registry->OpenKey(csProgRegKeyBase + "\\" + csUserSettingRegKey, false))
    {
      if (Registry->ReadString("ReqColor").Trim() == "1")
        FReqEdBkColor = FDefReqEdBkAltColor;
      else
        FReqEdBkColor = FDefReqEdBkColor;
      Registry->CloseKey();
    }
  }
  __finally
  {
    delete Registry;
  }
}
 */
//###########################################################################
//##                                                                       ##
//##                    �������������/����������� ������                   ##
//##                                                                       ##
//###########################################################################
/*
class TModuleStartup
{
public:
    TModuleStartup();
    ~TModuleStartup();
};

static TModuleStartup ModuleStartup;

//---------------------------------------------------------------------------

TModuleStartup::TModuleStartup()
{
  AppOptions = new TICAppOptions;
}

//---------------------------------------------------------------------------

TModuleStartup::~TModuleStartup()
{
  if (AppOptions) delete AppOptions;
}
*/
//---------------------------------------------------------------------------

