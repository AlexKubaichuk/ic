/*
  ����        - CopiedSchName.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ��� ����� ������������ ����������� ��� ����������� �����.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 12.04.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  26.02.2007
    [*] ������ � ���������� ���������� �������������� ����� ���������� ����������
        Dkglobals::AppOptions

  12.04.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef dsCopiedSchNameH
#define dsCopiedSchNameH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������
//---------------------------------------------------------------------------

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtonEdit.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
#include "dsSchEditClientUnit.h"
//---------------------------------------------------------------------------

/*
#include <Mask.hpp>

*/
namespace CopiedSchName
{

//###########################################################################
//##                                                                       ##
//##                               Globals                                 ##
//##                                                                       ##
//###########################################################################

extern UnicodeString ShowModalCopiedSchName( TComponent* Owner, UnicodeString AAutoSchName );

//###########################################################################
//##                                                                       ##
//##                           TfmCopiedSchName                            ##
//##                                                                       ##
//###########################################################################

// ������ ��� ����� ������������ ����������� ��� ����������� �����

class TfmCopiedSchName : public TForm
{
__published:    // IDE-managed Components
        TGroupBox *gbClient;
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
        TLabel *Label1;
 TcxButtonEdit *edName;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bOkClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
 void __fastcall edNamePropertiesButtonClick(TObject *Sender, int AButtonIndex);

private:        // User declarations
        TdsSchEditForm* FSchListFM;         //����� "������ ����"
        UnicodeString  FSchName;           //��������� ��� �����
        UnicodeString  FAutoSchName;       //������������ �� ��������� ������������� ����������� ��� �����

        bool __fastcall AreInputsCorrect();

public:         // User declarations
        __fastcall TfmCopiedSchName(TComponent* Owner, UnicodeString AAutoSchName);

        __property UnicodeString SchName = {read=FSchName};
};

//---------------------------------------------------------------------------

} // end of namespace CopiedSchName
using namespace CopiedSchName;

#endif
