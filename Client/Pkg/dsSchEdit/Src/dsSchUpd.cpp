/*
  ����        - SchUpd.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ �������������� ����� ���������� �������/�����.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 13.03.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DKMBUtils.h"

#include "dsSchUpd.h"

#include "dsVakSchList.h"
#include "msgdef.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxButtonEdit"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                               Globals                                 ##
//##                                                                       ##
//###########################################################################

/*
 * ����� ����� � ��������� ������
 *
 *
 * ���������:
 *   [in]  Owner   - �������� ( fmSchList )
 *   [in]  FUM     - ����� ������ �����
 *
 * ������������ ��������:
 *   ModalResult �����
 *
 */

int SchUpd::ShowModalSchUpd( TComponent* Owner, TFormUpdateMode FUM, __int64 AVacCode, UnicodeString &ASchName )
{
  int nModalResult = mrNone;
  TSchUpdForm *dlg = new TSchUpdForm(Owner, FUM, AVacCode, ASchName);
  if (dlg)
  {
    nModalResult = dlg->ShowModal();
    ASchName = dlg->edName->Text;
    delete dlg;
  }
  return nModalResult;
}

//###########################################################################
//##                                                                       ##
//##                              TfmSchUpd                                ##
//##                                                                       ##
//###########################################################################

__fastcall TSchUpdForm::TSchUpdForm(TComponent* Owner, TFormUpdateMode FUM, __int64 AVacCode, UnicodeString ASchName)
        : TForm(Owner)
{
  FFormUpdateMode = FUM;
  FOwnFM = (TdsSchEditForm*)Owner;
  FVacCode = AVacCode;
//  FInitVakItemIndex = -1;
  FInitSchemeName = ASchName;
}

//---------------------------------------------------------------------------

void __fastcall TSchUpdForm::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif

//  edName->Color = AppOptions->ReqEdBkColor;
//  cbVak->Color  = AppOptions->ReqEdBkColor;

  //���������� �������� � ����������� �� ������ ������ ����� "������ ����"
//  switch (FOwnFM->SchemeObjType)
//  {
//    case TSchemeObjType::Vac   : lbVakTitle->Caption = "����:";  break;
//    case TSchemeObjType::Test  : lbVakTitle->Caption = "�����:"; break;
//    case TSchemeObjType::Check : lbVakTitle->Caption = "��������:"; break;
//  }

//  int nCurVacId = -1;
  switch ( FFormUpdateMode )
  {
    case fumADD :
      Caption = "���������� �����";
    break;
    case fumEDIT:
      Caption = "��������� ���������� �����";
      edName->Text = FInitSchemeName;//FOwnFM->GetActiveSchemeAttrVal( "name" );
//      FInitSchemeName = edName->Text;
    break;
  }
//  if (nCurVacId != -1)
//    cbVak->ItemIndex = cbVak->Items->IndexOfObject(reinterpret_cast<TObject*>(nCurVacId));
//  FInitVakItemIndex = cbVak->ItemIndex;
}

//---------------------------------------------------------------------------

bool __fastcall TSchUpdForm::AreInputsCorrect()
{
  if ( edName->Text == "" )
  {
//    MB_SHOW_ERROR_CPT(L"���� \"������������\" ������ ���� ���������.", L"������ �����");
    _MSG_ERR("���� \"������������\" ������ ���� ���������.", L"������ �����");
    return false;
  }

  UnicodeString VakName;
  if ( ( FFormUpdateMode == fumADD ) ||
       ( (FFormUpdateMode == fumEDIT) && WasSchemeAttrsChanged() ) )
    if ( FOwnFM->SchemeNameExists( edName->Text, FVacCode ) )
    {
      _MSG_ERR("����� \""+edName->Text+"\" ��� ����.","������ �����");
//       cxGetResourceString("gg");
//      MB_SHOW_ERROR_CPT( Format( UnicodeString("����� \"%s\" ��� ����."), &TVarRec(edName->Text), 0 ).c_str(), L"������ �����");
      return false;
    }

  return true;
}

//---------------------------------------------------------------------------

bool __fastcall TSchUpdForm::WasSchemeAttrsChanged()
{
  bool Result;

  Result = ( AnsiUpperCase( FInitSchemeName ) != AnsiUpperCase( edName->Text ) );
  return Result;
}

//---------------------------------------------------------------------------

void __fastcall TSchUpdForm::bApplyClick(TObject *Sender)
{
  if ( !AreInputsCorrect() )
    return;

//  switch (FFormUpdateMode)
//  {
//    case fumADD  :
//      FOwnFM->AddScheme ( edName->Text, FVacCode);
//    break;
//    case fumEDIT :
//      FOwnFM->EditScheme( edName->Text, FVacCode);
//    break;
//  }
  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TSchUpdForm::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TSchUpdForm::edNamePropertiesButtonClick(TObject *Sender, int AButtonIndex)

{
  ShowModalVakSchList( FOwnFM, FVacCode, FOwnFM->GetVakName(FVacCode), edName->Text );
}
//---------------------------------------------------------------------------

