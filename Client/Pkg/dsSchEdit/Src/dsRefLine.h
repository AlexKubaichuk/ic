/*
  ����        - RefLine.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ������ �� ������ �����.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 14.04.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  01.12.2005 - kab
    [*] ����� "��������" � ���������� �������� �� "���������� ��"

  14.04.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef dsRefLineH
#define dsRefLineH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>

#include "dsJumpUpd.h"

namespace Dsrefline
{

//###########################################################################
//##                                                                       ##
//##                                Globals                                ##
//##                                                                       ##
//###########################################################################

extern UnicodeString ShowModalRefLine(TComponent* Owner, int AInfekId, UnicodeString AInfekName, UnicodeString sRefLine = "");

//###########################################################################
//##                                                                       ##
//##                               TfmRefLine                              ##
//##                                                                       ##
//###########################################################################

// ������ ������ ������ �� ������ �����

class TfmRefLine : public TForm
{
__published:    // IDE-managed Components
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
        TTreeView *tv;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bOkClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall tvChange(TObject *Sender, TTreeNode *Node);
        void __fastcall tvCustomDrawItem(TCustomTreeView *Sender,
          TTreeNode *Node, TCustomDrawState State, bool &DefaultDraw);


private:        // User declarations
        TdsSchEditForm* FOwnFM;     //����� "������ ����"
        UnicodeString  FRefLine;   //UID ��������� ������ �����
        int         FInfekId;   //ID ��������, �� ������� ������ ��������� �������, �����
                                //� ������� ����� ���� ������� ������ ����� ���� ������� ��� ���������
                                //��������
        UnicodeString  FInfekName; //������������ �������� --^


protected:
        bool __fastcall IsRefObj(TTreeNode *Node);
        bool __fastcall AreInputsCorrect();
        void __fastcall TagNodeToTree(TTagNode *TagNode, TTreeNode *TreeNode);


public:         // User declarations
        __fastcall TfmRefLine(int AInfekId, UnicodeString AInfekName, TComponent* Owner);
        virtual __fastcall ~TfmRefLine();

        __property UnicodeString RefLine = {read = FRefLine, write = FRefLine};
};

//---------------------------------------------------------------------------

} // end of namespace RefLine
using namespace Dsrefline;

#endif
