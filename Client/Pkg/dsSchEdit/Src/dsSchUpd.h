/*
  ����        - SchUpd.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ �������������� ����� ���������� �������/�����.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 13.03.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  26.02.2007
    [*] ������ � ���������� ���������� �������������� ����� ���������� ����������
        Dkglobals::AppOptions

  13.03.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef dsSchUpdH
#define dsSchUpdH

//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtonEdit.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
#include "dsDKGlobals.h"
#include "dsSchEditClientUnit.h"
//---------------------------------------------------------------------------

//#include "dsDKCfg.h"      // ��������� �������� ����������

/*
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Mask.hpp>

//#include "DKUtils.h"
//#include "SchList.h"
#include "cxButtonEdit.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
*/
namespace SchUpd
{

//###########################################################################
//##                                                                       ##
//##                               Globals                                 ##
//##                                                                       ##
//###########################################################################

extern int ShowModalSchUpd( TComponent* Owner, TFormUpdateMode FUM, __int64 AVacCode, UnicodeString &ASchName);

//###########################################################################
//##                                                                       ##
//##                              TfmSchUpd                                ##
//##                                                                       ##
//###########################################################################

// ������ �������������� ����� ���������� �������/�����

class TSchUpdForm : public TForm
{
__published:    // IDE-managed Components
        TPanel *Panel1;
        TPanel *Panel2;
        TPanel *Panel3;
        TButton *bApply;
        TButton *bCancel;
        TButton *bHelp;
        TGroupBox *GroupBox1;
        TLabel *Label1;
 TcxButtonEdit *edName;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bApplyClick(TObject *Sender);
 void __fastcall edNamePropertiesButtonClick(TObject *Sender, int AButtonIndex);


private:        // User declarations
        TFormUpdateMode FFormUpdateMode;        //����� ������ �����
        TdsSchEditForm*     FOwnFM;                 //����� "������ ����"
//        int             FInitVakItemIndex;      //��������� �������� ItemIndex'� �������
        UnicodeString      FInitSchemeName;        //��������� ������������ �����
        __int64         FVacCode;

        bool __fastcall AreInputsCorrect();
        bool __fastcall WasSchemeAttrsChanged();

public:         // User declarations
        __fastcall TSchUpdForm(TComponent* Owner, TFormUpdateMode FUM, __int64 AVacCode, UnicodeString ASchName);
};

//---------------------------------------------------------------------------

} // end of namespace SchUpd
using namespace SchUpd;

#endif
