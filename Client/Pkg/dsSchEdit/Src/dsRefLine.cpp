/*
  ����        - RefLine.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ������ �� ������ �����.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 14.04.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsRefLine.h"
#include "DKClasses.h"
#include "dsSchEditClientUnit.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"

namespace Dsrefline
{

//###########################################################################
//##                                                                       ##
//##                               TVI_DATA                                ##
//##                                                                       ##
//###########################################################################

// ��� ������, ��������������� � ����� TreeView
enum TTVIDataType
{
  dtEndPlan,   // ������� ��������� ������������
  dtVakId,     // ��� �������/�����
  dtTagNode    // ���
};

//---------------------------------------------------------------------------

// ������, ��������������� � ����� TreeView
class TVI_DATA
{
private:
        TTVIDataType FType;     // ��� ������
        TTagNode*    FTagNode;  // ���
        int          FVakId;    // ��� �������/�����
        /*
          Type == dtEndPlan  ==> TagNode == NULL,  VakId == -1
          Type == dtVakId    ==> TagNode == NULL,  VakId == <���>
          Type == dtTagNode  ==> TagNode == <���>, VakId == -1
        */


protected:
        void __fastcall SetType(TTVIDataType AValue);
        void __fastcall SetTagNode(TTagNode* AValue);
        void __fastcall SetVakId(int AValue);


public:
        __fastcall TVI_DATA();

        __property TTVIDataType Type = {read = FType, write = SetType};
        __property TTagNode*    TagNode = {read = FTagNode, write = SetTagNode};
        __property int          VakId = {read = FVakId, write = SetVakId};
};

//---------------------------------------------------------------------------

} // end of namespace RefLine

//###########################################################################
//##                                                                       ##
//##                                Globals                                ##
//##                                                                       ##
//###########################################################################

/*
 * ����� ����� � ��������� ������
 *
 *
 * ���������:
 *   [in]  Owner      - �������� ( fmJumpUpd )
 *   [in]  AInfekId   - Id ��������, �� ������� ������ ��������� �������, ����� �� ����� ������ � ������
 *   [in]  AInfekName - ������������ ��������
 *
 * ������������ ��������:
 *   UID ��������� ������ ����� ��� ��������
 *
 */

UnicodeString Dsrefline::ShowModalRefLine(TComponent* Owner, int AInfekId, UnicodeString AInfekName, UnicodeString sRefLine)
{
  TfmRefLine *dlg = NULL;
  try
  {
    dlg = new TfmRefLine(AInfekId, AInfekName, Owner);
    dlg->RefLine = sRefLine;
    if ( dlg->ShowModal() == mrOk )
      sRefLine = dlg->RefLine;
  }
  __finally
  {
    delete dlg;
  }
  return sRefLine;
}

//###########################################################################
//##                                                                       ##
//##                               TVI_DATA                                ##
//##                                                                       ##
//###########################################################################

__fastcall TVI_DATA::TVI_DATA():
  FType(dtEndPlan),
  FTagNode(NULL),
  FVakId(-1)
{
}

//---------------------------------------------------------------------------

void __fastcall TVI_DATA::SetType(TTVIDataType AValue)
{
  if (FType != AValue)
  {
    FType = AValue;
    switch(FType)
    {
      case dtEndPlan:
        FTagNode = NULL;
        FVakId = -1;
      break;
      case dtVakId  :
        FTagNode = NULL;
      break;
      case dtTagNode:
        FVakId = -1;
      break;
    }
  }
}

//---------------------------------------------------------------------------

void __fastcall TVI_DATA::SetTagNode(TTagNode* AValue)
{
  if ((FTagNode != AValue) && (FType == dtTagNode))
    FTagNode = AValue;
}

//---------------------------------------------------------------------------

void __fastcall TVI_DATA::SetVakId(int AValue)
{
  if ((FVakId != AValue) && (FType == dtVakId))
    FVakId = AValue;
}

//###########################################################################
//##                                                                       ##
//##                               TfmRefLine                              ##
//##                                                                       ##
//###########################################################################

__fastcall TfmRefLine::TfmRefLine(int AInfekId, UnicodeString AInfekName, TComponent* Owner)
        : TForm(Owner)
{
  if (!Owner)
    throw DKClasses::ENullArgument(__FUNC__, "Owner");
  if (!Owner->InheritsFrom(__classid(TdsSchEditForm)))
    throw DKClasses::EInvalidArgument(__FUNC__, "Owner", "�������� ����� ������ ���� �������� TfmSchList.");

  FOwnFM     = (TdsSchEditForm*)Owner;
  FRefLine   = "";
  FInfekId   = AInfekId;
  FInfekName = AInfekName;
}

//---------------------------------------------------------------------------

__fastcall TfmRefLine::~TfmRefLine()
{
  for (int i = 0; i < tv->Items->Count; i++)
    if (tv->Items->Item[i]->Data)
      delete reinterpret_cast<TVI_DATA*>(tv->Items->Item[i]->Data);
}

//---------------------------------------------------------------------------

void __fastcall TfmRefLine::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif

  Caption = Caption + " (���������� �� \"" + FInfekName + "\")";
  TVI_DATA *pData = new TVI_DATA;
  tv->Items->AddObject(NULL, csEndPlanLabel, pData);

  TTreeNode  *RootNode;
  switch(FOwnFM->SchemeObjType)
  {
    case TSchemeObjType::Vac:
      RootNode = tv->Items->AddObject( NULL, "����� ����������", NULL );
    break;
    case TSchemeObjType::Test:
      RootNode = tv->Items->AddObject( NULL, "����� ����", NULL );
    break;
    case TSchemeObjType::Check:
      RootNode = tv->Items->AddObject( NULL, "����� ��������", NULL );
    break;
  }

  TStrings   *sl       = new TStringList;

  //��������� � ������ ������, ������� ����� ������ �� �������� � ����� FInfekId
  FOwnFM->GetInfekVakList( sl, FInfekId );
  for ( int i = 0; i < sl->Count; i++ )
  {
    TVI_DATA *pData = new TVI_DATA;
    pData->Type  = dtVakId;
    pData->VakId = reinterpret_cast<int>(sl->Objects[i]);
    tv->Items->AddChildObject( RootNode, sl->Strings[i], (void*)pData );
  }

  delete sl;

  //��������� � ������ ���� ��� ������ ���������� �������
  TTagNode* CurNode = FOwnFM->IMM_S->GetChildByName("schemalist")->GetFirstChild();
  while ( CurNode )
  {
    int nCurNodeRef = StrToInt(CurNode->AV["ref"]);
    for ( int i = 0; i < RootNode->Count; i++ )
      if ( reinterpret_cast<TVI_DATA*>(RootNode->Item[i]->Data)->VakId == nCurNodeRef )
      {
        TVI_DATA *pData = new TVI_DATA;
        pData->Type    = dtTagNode;
        pData->TagNode = CurNode;
        tv->Items->AddChildObject(RootNode->Item[i], CurNode->AV["name"], pData);
        break;
      }
    CurNode = CurNode->GetNext();
  }

  //�������� ������, ��� ������� ��� �� ����� �����, �� ������
  TTreeNode *TreeNode = RootNode->getFirstChild();
  while ( TreeNode )
  {
    TTreeNode *NextNode = TreeNode->getNextSibling();
    if ( !TreeNode->Count )
      TreeNode->Delete();
    TreeNode = NextNode;
  }

  //���������� �� �������� ���� ��� ������ ������� � ������
  for ( int i = 0; i < RootNode->Count; i++ )
    RootNode->Item[i]->AlphaSort();

  //��������� ����� ����
  for ( int i = 0; i < RootNode->Count; i++ )
    for ( int j = 0; j < RootNode->Item[i]->Count; j++ )
    {
      TTagNode *LineNode = reinterpret_cast<TVI_DATA*>(RootNode->Item[i]->Item[j]->Data)->TagNode;
      CopyTagNodeToTree(LineNode,  RootNode->Item[i]->Item[j], TagNodeToTree);
//      LineNode->OnCopyItem = TagNodeToTree;
//    LineNode->ToTree( RootNode->Item[i]->Item[j] );
//    LineNode->OnCopyItem = NULL;
    }

  //�������� �����, ��������������� ���� "reflist"
  for ( int i = 0; i < tv->Items->Count; i++ )
  {
    TVI_DATA* pData = reinterpret_cast<TVI_DATA*>(tv->Items->Item[i]->Data);
    if (pData && (pData->Type == dtTagNode))
      if ( pData->TagNode->CmpName("reflist") )
        tv->Items->Item[i]->Delete();
  }

  //��������� �������� ����
  TTreeNode* SelNode = NULL;
  if (FRefLine.Length())
    for ( int i = 0; i < tv->Items->Count; i++ )
    {
      TVI_DATA* pData = reinterpret_cast<TVI_DATA*>(tv->Items->Item[i]->Data);
      if (
        pData &&
        (
          (
            (pData->Type == dtEndPlan) &&
            (FRefLine == EmptyRef)
          ) ||
          (
            (pData->Type == dtTagNode) &&
            pData->TagNode->GetAttrByName("uid") &&
            pData->TagNode->CmpAV("uid", FRefLine)
          )
        )
      )
      {
        SelNode = tv->Items->Item[i];
        break;
      }
    }
  if (!SelNode)
    SelNode = RootNode;
  tv->Select(SelNode);
  tv->Selected->Expand(false);
}

//---------------------------------------------------------------------------

bool __fastcall TfmRefLine::AreInputsCorrect()
{
        //---- ����� ���������� ��������� ���� �������� ----

  return true;
}

//---------------------------------------------------------------------------

//����������� TagNode �� TreeNode

void __fastcall TfmRefLine::TagNodeToTree(TTagNode *TagNode, TTreeNode *TreeNode)
{
  TVI_DATA *pData = new TVI_DATA;
  pData->Type    = dtTagNode;
  pData->TagNode = TagNode;
  TreeNode->Data = pData;
  if      ( TagNode->CmpName("schema") )
  {
    TreeNode->Text = TagNode->AV["name"];
  }

  else if ( TagNode->CmpName("line") )
  {
    UnicodeString tmp, text;
    text = TagNode->AV["name"] + "  ( ���. ����� - " + XMLStrDurationToStr(TagNode->AV["min_pause"]);
    tmp = XMLStrDurationToStr(TagNode->AV["min_age"]);
    if ( tmp != "" )
      text += ", ���. ������� - " + tmp;
    tmp = GetSeasonDiap(TagNode->AV["season_beg"], TagNode->AV["season_end"]);
    if ( tmp != "" )
      text += ", ���������� - " + tmp;
    text += " )";
    TreeNode->Text = text;
  }

  if      ( TagNode->CmpName("begline") )
  {
    TreeNode->Text = csBeginLineLabel;
  }
  else if      ( TagNode->CmpName("endline") )
  {
    TreeNode->Text = csEndLineLabel;
  }

  else if ( TagNode->CmpName("srokjump") )
  {
    TreeNode->Text = "������� �� �����  (" + XMLStrDurationToStr(TagNode->AV["actualtime"]) + " )";
  }

  else if ( TagNode->CmpName("agejump") )
  {
    TreeNode->Text = "������� �� �������� (" + XMLStrDurationToStr(TagNode->AV["max_age"]) + ")";
  }

  else if ( TagNode->CmpName("jump"    ) )
  {
    TreeNode->Text = "������� �� ������� (" + FOwnFM->GetFilterNameByID(UIDInt(TagNode->AV["ref"])) +")" ;
  }

  else if ( TagNode->CmpName("endjump" ) )
  {
    TreeNode->Text = "����������� �������";
  }

  else if ( TagNode->CmpName("refer" ) )
  {
    UnicodeString VakName        = FOwnFM->GetInfekName(StrToInt(TagNode->AV["infref"]));
    UnicodeString PrivivFullName = FOwnFM->GetPrivivFullName(TagNode->AV["ref"]);
    TreeNode->Text = VakName + " -> " + PrivivFullName;

    if ( TagNode->GetParent()->CmpName("reflist") )
      TreeNode->MoveTo( TreeNode->Parent->Parent, naAddChild );

  }
}

//---------------------------------------------------------------------------

void __fastcall TfmRefLine::bOkClick(TObject *Sender)
{
  if ( !AreInputsCorrect() )
    return;

  TVI_DATA* pData = reinterpret_cast<TVI_DATA*>(tv->Selected->Data);
  if (pData->Type == dtEndPlan)
    FRefLine = EmptyRef;
  else if (pData->Type == dtTagNode)
    FRefLine = pData->TagNode->AV["uid"];

  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TfmRefLine::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TfmRefLine::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}

//---------------------------------------------------------------------------

bool __fastcall TfmRefLine::IsRefObj(TTreeNode *Node)
{
  bool fResult = false;
  TVI_DATA* pData = reinterpret_cast<TVI_DATA*>(Node->Data);
  if (pData)
  {
    if (pData->Type == dtEndPlan)
      fResult = true;
    else if (pData->Type == dtTagNode)
      fResult = pData->TagNode->CmpName("line,endline");
  }
  return fResult;
}

//---------------------------------------------------------------------------

void __fastcall TfmRefLine::tvChange(TObject *Sender, TTreeNode *Node)
{
  bOk->Enabled = IsRefObj(Node);
}

//---------------------------------------------------------------------------

void __fastcall TfmRefLine::tvCustomDrawItem(TCustomTreeView *Sender,
      TTreeNode *Node, TCustomDrawState State, bool &DefaultDraw)
{
  if (IsRefObj(Node))
    Sender->Canvas->Font->Style = TFontStyles() << fsBold;
  DefaultDraw = true;
}

//---------------------------------------------------------------------------

