object fmVakSchList: TfmVakSchList
  Left = 483
  Top = 232
  BorderStyle = bsDialog
  BorderWidth = 5
  Caption = #1057#1087#1080#1089#1086#1082' '#1089#1093#1077#1084' '#1074#1072#1082#1094#1080#1085#1099
  ClientHeight = 311
  ClientWidth = 324
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pBottom: TPanel
    Left = 0
    Top = 277
    Width = 324
    Height = 34
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 1
    object pCtrls: TPanel
      Left = 169
      Top = 0
      Width = 155
      Height = 34
      Align = alRight
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 0
      object bOk: TButton
        Left = 0
        Top = 6
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1047#1072#1082#1088#1099#1090#1100
        Default = True
        TabOrder = 0
        OnClick = bOkClick
      end
      object bHelp: TButton
        Left = 80
        Top = 6
        Width = 75
        Height = 25
        Caption = '&'#1055#1086#1084#1086#1097#1100
        TabOrder = 1
        OnClick = bHelpClick
      end
    end
  end
  object lbxScheme: TListBox
    Left = 0
    Top = 0
    Width = 324
    Height = 277
    Align = alClient
    ItemHeight = 13
    Sorted = True
    TabOrder = 0
  end
end
