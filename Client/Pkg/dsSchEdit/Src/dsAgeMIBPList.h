/*
 ����        - SchCondList.h
 ������      - ���������� ������������.
 ��������� "���������� ������������"
 ��������    - ������ �������������� ������ ������� ��� ����.
 ������������ ����
 ����������� - ������� �.�., �������� �.�.
 ����        - 26.02.2007
 */
//---------------------------------------------------------------------------
/*
 ������� ���������:

 26.02.2007
 �������������� �������
 */
//---------------------------------------------------------------------------

#ifndef dsAgeMIBPListH
#define dsAgeMIBPListH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������

#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>

#include "cxClasses.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxGraphics.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"

/*
 #include <Classes.hpp>
 #include <Controls.hpp>
 #include <StdCtrls.hpp>
 #include <Forms.hpp>
 #include "cxButtons.hpp"
 #include "cxContainer.hpp"
 #include "cxControls.hpp"
 #include "cxListBox.hpp"
 #include "cxLookAndFeelPainters.hpp"
 #include <ActnList.hpp>
 #include <ExtCtrls.hpp>
 #include <Menus.hpp>
 #include "XMLContainer.h"
 //#include "ICSDocFilter.h"
 #include "FilterEditEx.h"
 #include "cxEdit.hpp"
 #include "cxGraphics.hpp"
 #include "cxLookAndFeels.hpp"
 #include "dxSkinBlue.hpp"
 #include "dxSkinsCore.hpp"
 #include <System.Actions.hpp>
 #include "dsSchEditClientUnit.h"
 #include "cxClasses.hpp"
 #include "dxBar.hpp"
 #include "dxSkinsdxBarPainter.hpp"
 #include <Vcl.ImgList.hpp>

 #include "cxCustomData.hpp"
 #include "cxImageComboBox.hpp"
 #include "cxInplaceContainer.hpp"
 #include "cxStyles.hpp"
 #include "cxTextEdit.hpp"
 #include "cxTL.hpp"
 #include "cxTLdxBarBuiltInMenu.hpp"
 #include "dxSkinOffice2010Blue.hpp"
 #include "dxSkinValentine.hpp"
 #include "dxSkinXmas2008Blue.hpp"
 */
//---------------------------------------------------------------------------
#include "dsSchEditDMUnit.h"
#include "cxMaskEdit.hpp"
//---------------------------------------------------------------------------
//namespace SchList
//{
//class TdsSchEditForm;
//}

namespace SchCondList
 {

 //###########################################################################
 //##                                                                       ##
 //##                             TCondListForm                             ##
 //##                                                                       ##
 //###########################################################################

 //������ �������������� ������ ������� ��� ����

 class TAgeMIBPForm : public TForm
  {
  __published: //IDE-managed Components
   TActionList *       MainAL;
   TAction *           actApply;
   TcxImageList *      LMainIL;
   TdxBarManager *     ClassTBM;
   TdxBar *            ClassTBMBar1;
   TdxBarLargeButton * dxBarLargeButton2;
   TPopupMenu *        PMenu;
   TMenuItem *         InsertItem;
   TMenuItem *         ModifyItem;
   TMenuItem *         DeleteItem;
   TcxTreeList *       MIBPList;
   TcxTreeListColumn * CondNameCol;
   TcxTreeListColumn * CondListColumn1;
   TcxTreeListColumn * CondListColumn2;
   void __fastcall actApplyExecute(TObject * Sender);
 void __fastcall CondListColumn1PropertiesValidate(TObject *Sender, Variant &DisplayValue,
          TCaption &ErrorText, bool &Error);

  private: //User declarations
   TdsSchEditDM * FDM;
   TTagNode *     FIMMS;

  public: //User declarations
   __fastcall TAgeMIBPForm(TComponent * Owner, TdsSchEditDM * ADM, TTagNode * AImm_S, int ASchUID);
  };

 //---------------------------------------------------------------------------

 }//end of namespace SchCondList
using namespace SchCondList;

#endif
