/*
  ����        - SchCondList.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ �������������� ������ ������� ��� ����.
                ���� ����������
  ����������� - ������� �.�., �������� �.�.
  ����        - 26.02.2007
*/     
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsSchCondList.h"
#include "DKMBUtils.h"
#include "msgdef.h"
//#include "DKGlobals\DKFilterEdit.h"
//#include "DMF.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxListBox"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma link "cxClasses"
#pragma link "dxBar"
#pragma link "dxSkinsdxBarPainter"
#pragma link "cxCustomData"
#pragma link "cxImageComboBox"
#pragma link "cxInplaceContainer"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxSkinOffice2010Blue"
#pragma link "dxSkinValentine"
#pragma link "dxSkinXmas2008Blue"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                             TCondListForm                             ##
//##                                                                       ##
//###########################################################################

__fastcall TCondListForm::TCondListForm(TComponent* Owner, TdsSchEditDM* ADM, TTagNode *AImm_S, int ASchUID)
        : TForm(Owner)
{
//  if (!Owner->InheritsFrom(__classid(TdsSchEditForm)))
//    throw DKClasses::EInvalidArgument(__FUNC__, "Owner", "�������� ����� ������ ���� �������� TdsSchEditForm.");
  FDM = ADM;
  FUpdate = false;
  FIMMS = AImm_S;
  FEdit = false;
  TTagNode *CondNode = FIMMS->GetChildByName("conditionlist");
  FIMMS->SetReferNames("jump.ref");
  if (CondNode)
   {
     CondNode = CondNode->GetFirstChild();
     CondList->Clear();
     TcxTreeListNode *FNode;
     while (CondNode)
      {
        FNode = CondList->Root->AddChild();
        FNode->Texts[0] = CondNode->AV["name"];
        FNode->Data = CondNode;
        if (UIDInt(CondNode->AV["uid"]) == ASchUID)
         FNode->Focused = true;
//        CondList->Items->AddObject(CondNode->AV["name"],(TObject*)CondNode);
        CondNode = CondNode->GetNext();
      }
   }
  UpdateActions();
}

//---------------------------------------------------------------------------
bool __fastcall TCondListForm::FilterEditCanClose(TFilterEditExForm *Sender, UnicodeString ACondName)
{
  bool RC = true;
  try
   {
    //  int nIdx = CondList->Items->IndexOf(ACondName.Trim());
     TcxTreeListNode *FSearchNode = CondList->FindNodeByText(ACondName, CondNameCol,NULL, false,true, false);
     if (FSearchNode)
      {
        if (FEdit)
          RC = (FSearchNode == GetActiveCond());
        else
          RC = false;
        if (!RC)
         {
           MB_SHOW_ERROR(("������� \"" + ACondName + "\" ��� ����������.").c_str());
         }
      }
   }
  __finally
   {
   }
  return RC;
}

//---------------------------------------------------------------------------
UnicodeString __fastcall TCondListForm::FGetMsgTxt(TFilterEditMsgType AType, UnicodeString ADefMsg)
{
  UnicodeString RC = ADefMsg;
  try
   {
     switch (AType)
      {
        case femtCaption:        {RC = "�������� �������"; break;}
        case femtRootNameF:      {RC = "�������"; break;}
        case femtNewFileName:    {RC = "����� �������"; break;}
        case femtCheckOk:        {RC = "������� ���������"; break;}
        case femtCheckNone:      {RC = "������� �����������"; break;}
        case femtCheckEmpty:     {RC = "������� ������"; break;}
        case femtSelectCondName: {RC = "����� �������"; break;}
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TCondListForm::actInsertExecute(TObject *Sender)
{
  ActiveControl  = CondList;
  TFilterEditExForm *Dlg = new TFilterEditExForm(this);
  TTagNode *__tmpFlt = NULL;
  try
   {
/*
     FDataSrc->DataSet->Insert();

     EditDlg->OnCallDefProc = FDM->OnCallDefProc;
     EditDlg->EditType = fetFilter;

     EditDlg->Nom             = FDM->Nom;
     EditDlg->XMLContainer    = FDM->XMLList;

     EditDlg->CanKonkr = false;
     EditDlg->New();
     EditDlg->OnCanClose = FCanClose;
     EditDlg->ShowModal();
*/


      __tmpFlt = new TTagNode(NULL);

     Dlg->OnCallDefProc = FDM->OnCallDefProc;

//     Dlg->NomDef = DM->ICSDocEx->DM->ICSNom;
     Dlg->EditType = fetFilter;

     Dlg->Nom              = FDM->Nom;
     Dlg->XMLContainer     = FDM->XMLList;
//     Dlg->DTD              = DM->ICSDocEx->DM->FltDTD;
     Dlg->DomainNumber     = 1;
     Dlg->OnCanClose       = FilterEditCanClose;
     Dlg->OnGetMessageText = FGetMsgTxt;

     Dlg->CanKonkr = true;
     Dlg->New();
     Dlg->ShowModal();

     if (Dlg->ModalResult == mrOk)
      {
        __tmpFlt->Assign(Dlg->Filter,true);
        TTagNode *ndCond = FIMMS->GetChildByName("conditionlist");
        if (!ndCond)
          ndCond = FIMMS->AddChild("conditionlist");
        ndCond = ndCond->AddChild();

        UnicodeString sCondName  = __tmpFlt->GetChildByName("passport")->AV["mainname"].Trim();
        TTagNode *ndCondRules = __tmpFlt->GetChildByName("condrules");
        ndCondRules->AV["name"] = sCondName;
        ndCondRules->AV["uid"]  = FIMMS->NewUID();

        ndCond->AssignEx(ndCondRules, true);

        TcxTreeListNode *FNode;
        FNode = CondList->Root->AddChild();
        FNode->Texts[0] = sCondName;
        FNode->Data = ndCond;

//        CondList->Items->AddObject(sCondName, (TObject*)ndCond);
        FUpdate = true;
      }
   }
  __finally
   {
     delete Dlg;
     if (__tmpFlt) delete __tmpFlt;
   }
  UpdateActions();
}

//---------------------------------------------------------------------------
void __fastcall TCondListForm::actEditExecute(TObject *Sender)
{
  FEdit = true;
  try
  {
    ActiveControl  = CondList;
    TTagNode *ndCond = GetActiveCondData();
    if (ndCond)
     {
       TTagNode *tmpCond = new TTagNode;
       try
       {
         tmpCond->Name = "region";
         tmpCond->AddChild(
           "passport",
           "GUI=00000000-00000000-0000,"
           "autor=����������,version=1,"
           "release=0,"
           "timestamp=" + TDateTime::CurrentDateTime().FormatString("yyyymmddhhnnss") + ","
           "mainname=" + ndCond->AV["name"]
         );
         tmpCond->AddChild()->Assign(ndCond, true);
         TFilterEditExForm *Dlg = new TFilterEditExForm(this);
         TTagNode *__tmpFlt = NULL;
         try
         {
              __tmpFlt = new TTagNode(NULL);
   //           Dlg->NomDef = DM->ICSDocEx->DM->ICSNom;
              Dlg->OnCallDefProc = FDM->OnCallDefProc;
              Dlg->EditType = fetFilter;

              Dlg->Nom              = FDM->Nom;
              Dlg->XMLContainer     = FDM->XMLList;
              Dlg->OnCanClose       = FilterEditCanClose;
              Dlg->OnGetMessageText = FGetMsgTxt;
              Dlg->DomainNumber     = 1;

              Dlg->CanKonkr = true;
              Dlg->Filter = tmpCond;
              Dlg->ShowModal();

           if (Dlg->ModalResult == mrOk)
           {
             __tmpFlt->Assign(Dlg->Filter,true);
             UnicodeString sCondName  = __tmpFlt->GetChildByName("passport")->AV["mainname"].Trim();
             TTagNode *ndCondRules = __tmpFlt->GetChildByName("condrules");
             ndCondRules->AV["name"] = sCondName;
             ndCondRules->AV["uid"]  = ndCond->AV["uid"];
             GetActiveCond()->Texts[0] = sCondName;
             ndCond->AssignEx(ndCondRules, true);
             FUpdate = true;
           }
         }
         __finally
         {
           delete Dlg;
           if (__tmpFlt) delete __tmpFlt;
         }
    }
    __finally
    {
      delete tmpCond;
    }
     }
  }
  __finally
  {
    FEdit = false;
  }
  UpdateActions();
}
//---------------------------------------------------------------------------
void __fastcall TCondListForm::actDeleteExecute(TObject *Sender)
{
  ActiveControl = CondList;
  TcxTreeListNode *FNode = GetActiveCond();
  if (FNode)
  {
    TTagNode *FCondDataNode = GetActiveCondData();
    TTagNode *SearchNode = FIMMS->GetChildByAV("jump", "ref", FCondDataNode->AV["uid"], true);
    if (SearchNode)
     _MSG_ERR("�������� ����������, ���������� �����, ������������ ������ �������!", "������");
    else
     {
      UnicodeString sMsg = "�� ������� � ���, ��� ������ ������� ������� \"" + GetActiveCond()->Texts[0] + "\"?";
      if (_MSG_QUE(sMsg, "������������� ��������") == ID_YES)
      {
        FNode->Delete();
        delete FCondDataNode;
        FUpdate = true;
      }
    }
  }
  UpdateActions();
}
//---------------------------------------------------------------------------
void __fastcall TCondListForm::UpdateActions()
{
  if (GetActiveCond())
  {
    actEdit->Enabled   = true;
    actDelete->Enabled = true;
  }
  else
  {
    actEdit->Enabled   = false;
    actDelete->Enabled = false;
  }
}
//---------------------------------------------------------------------------
TcxTreeListNode* __fastcall TCondListForm::GetActiveCond()
{
  TcxTreeListNode* RC = NULL;
  try
   {
     if (CondList->SelectionCount)
      RC =  CondList->Selections[0];
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TCondListForm::GetActiveCondData()
{
  TTagNode* RC = NULL;
  try
   {
     TcxTreeListNode* FNode = GetActiveCond();
     if (FNode)
      RC = (TTagNode*)FNode->Data;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

