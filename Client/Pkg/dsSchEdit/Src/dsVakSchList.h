/*
  ����        - VakSchList.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ��� ��������� ������ ���� ��������� �������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 07.04.2004
*/
//---------------------------------------------------------------------------

#ifndef dsVakSchListH
#define dsVakSchListH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>

#include "dsSchEditClientUnit.h"

namespace VakSchList
{

//###########################################################################
//##                                                                       ##
//##                               Globals                                 ##
//##                                                                       ##
//###########################################################################

extern int ShowModalVakSchList( TComponent* Owner, int AVakId, UnicodeString AVakName, UnicodeString ASchName = "" );

//###########################################################################
//##                                                                       ##
//##                             TfmVakSchList                             ##
//##                                                                       ##
//###########################################################################

// ������ ��� ��������� ������ ���� ��������� �������

class TfmVakSchList : public TForm
{
__published:    // IDE-managed Components
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bHelp;
        TListBox *lbxScheme;
        void __fastcall bOkClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
private:        // User declarations
        TdsSchEditForm *FSchListFM;         //����� "������ ����"
        int         FVakId;             //ID �������
        UnicodeString  FVakName;           //��� �������
        UnicodeString  FSchName;           //��� ������� �����
public:         // User declarations
        __fastcall TfmVakSchList(int AVakId, UnicodeString AVakName, TComponent* Owner, UnicodeString ASchName = "" );
};

//---------------------------------------------------------------------------

} // end of namespace VakSchList
using namespace VakSchList;

#endif
