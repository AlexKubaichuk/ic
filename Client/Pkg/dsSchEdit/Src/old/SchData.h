/*
  ����        - SchData.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ��� ��������� ���� ����������/����
                ������������ ����
  ����������� - ������� �.�.
  ����        - 13.03.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  28.04.2006
    [+] ����� GetFilterName

  01.12.2005
    [+] ������� fqPrivivCount � fqProbCount

  13.03.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef SchDataH
#define SchDataH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "FIBDatabase.hpp"
#include "FIBDataSet.hpp"
#include "pFIBDatabase.hpp"
#include "pFIBDataSet.hpp"
#include <DB.hpp>
#include "FIBQuery.hpp"
#include "pFIBQuery.hpp"
#include <XMLContainer.h>
#include "DKDBUtils.h"

#include "DKGlobals.h"

#ifdef DEVELOP_MODE
  #include "dmodule.h"
#endif

namespace SchData
{

//###########################################################################
//##                                                                       ##
//##                               Globals                                 ##
//##                                                                       ##
//###########################################################################

// ��� �������� ������� ��� �����
enum TSchemeObjType
{
  sotVaccine,   // �������
  sotTest       // �����
};

//###########################################################################
//##                                                                       ##
//##                                TSDM                                   ##
//##                                                                       ##
//###########################################################################

// ������ ������ ��� ��������� ���� ����������/����

class TSDM : public TDataModule
{
__published:    // IDE-managed Components
        void __fastcall DataModuleCreate(TObject *Sender);
        void __fastcall DataModuleDestroy(TObject *Sender);


private:        // User declarations
        TpFIBDatabase* FDB;                      // ������� ��������� "���� ������"
        TSchemeObjType FSchemeObjType;           // ��� �������� ������� ��� �����
        UnicodeString     FStartSchFN;              // ��� ���� ������� "��������", ����������� UID ��������� ����� ��� �����
        UnicodeString     FSchOwnerTN;              // ��� ������� "�������" ��� "�����"
        UnicodeString     FSchOwnerNameFN;          // ��� ���� "������������" ������� "�������" ��� "�����"
        UnicodeString     FSchOwnerInfekTN;         // ��� ������� "������� ��� ��������" ��� "�������� -> �����"
        UnicodeString     FSchOwnerInfekInfekFN;    // ��� ���� "��������" ������� "������� ��� ��������" ��� "�������� -> �����"
        UnicodeString     FSchOwnerInfekSchOwnerFN; // ��� ���� "�������" ������� "������� ��� ��������" ��� ���� "�����" ������� "�������� -> �����"


public:         // User declarations
        __fastcall TSDM(TComponent* Owner, TpFIBDatabase *ADB, TSchemeObjType ASchemeObjType);

        __property TSchemeObjType SchemeObjType = {read = FSchemeObjType};
        // ��� ���� ������� "��������", ����������� UID ��������� ����� ��� �����
        __property UnicodeString StartSchFN = {read = FStartSchFN};
        // ��� ������� "�������" ��� "�����"
        __property UnicodeString SchOwnerTN = {read = FSchOwnerTN};
        // ��� ���� "������������" ������� "�������" ��� "�����"
        __property UnicodeString SchOwnerNameFN = {read = FSchOwnerNameFN};
        // ��� ������� "������� ��� ��������" ��� "�������� -> �����"
        __property UnicodeString SchOwnerInfekTN = {read = FSchOwnerInfekTN};
        // ��� ���� "��������" ������� "������� ��� ��������" ��� "�������� -> �����"
        __property UnicodeString SchOwnerInfekInfekFN = {read = FSchOwnerInfekInfekFN};
        // ��� ���� "�������" ������� "������� ��� ��������" ��� ���� "�����" ������� "�������� -> �����"
        __property UnicodeString SchOwnerInfekSchOwnerFN = {read = FSchOwnerInfekSchOwnerFN};

        void __fastcall OpenDataSets();
        void __fastcall CloseDataSets();        
        bool __fastcall UpdatesCommit();
        bool __fastcall UpdatesCommitRetaining();
        bool __fastcall UpdatesRollback();
        bool __fastcall UpdatesRollbackRetaining();

        UnicodeString __fastcall GetVakName(int nVakId);
        UnicodeString __fastcall GetInfekName(int nInfekId);
        UnicodeString __fastcall GetFilterName(const int ACHType, const int AId);
};

//---------------------------------------------------------------------------

} // end of namespace SchData
using namespace SchData;

#ifdef OLD_CODE_SUPPORT
  #include "DKOld\SchDataOld.h"
#endif

#endif
