object SchCpbltFM: TSchCpbltFM
  Left = 423
  Top = 353
  BorderStyle = bsDialog
  BorderWidth = 5
  Caption = #1057#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1077' '#1074#1080#1076#1086#1074' '#1074#1072#1082#1094#1080#1085#1072#1094#1080#1080
  ClientHeight = 206
  ClientWidth = 362
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pBottom: TPanel
    Left = 0
    Top = 172
    Width = 362
    Height = 34
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 1
    object pCtrls: TPanel
      Left = 127
      Top = 0
      Width = 235
      Height = 34
      Align = alRight
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 0
      object bOk: TButton
        Left = 0
        Top = 6
        Width = 75
        Height = 25
        Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
        Default = True
        TabOrder = 0
        OnClick = bOkClick
      end
      object bCancel: TButton
        Left = 80
        Top = 6
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
        OnClick = bCancelClick
      end
      object bHelp: TButton
        Left = 160
        Top = 6
        Width = 75
        Height = 25
        Caption = '&'#1055#1086#1084#1086#1097#1100
        TabOrder = 2
        OnClick = bHelpClick
      end
    end
  end
  object Grid: TRxDrawGrid
    Left = 0
    Top = 0
    Width = 362
    Height = 172
    Align = alClient
    ColCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goColSizing, goEditing]
    TabOrder = 0
    OnDrawCell = GridDrawCell
    OnGetEditText = GridGetEditText
    OnGetEditStyle = GridGetEditStyle
    OnEditButtonClick = GridEditButtonClick
  end
end
