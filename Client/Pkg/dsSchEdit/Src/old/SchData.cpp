/*
  ����        - SchData.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ��� ��������� ���� ����������/����
                ���� ����������
  ����������� - ������� �.�.
  ����        - 13.03.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SchData.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "FIBDatabase"
#pragma link "FIBDataSet"
#pragma link "pFIBDatabase"
#pragma link "pFIBDataSet"
#pragma link "FIBQuery"
#pragma link "pFIBQuery"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                                TSDM                                   ##
//##                                                                       ##
//###########################################################################

__fastcall TSDM::TSDM(TComponent* Owner, TpFIBDatabase *ADB, TSchemeObjType ASchemeObjType)
        : TDataModule(Owner)
{
  if (!ADB)
    throw DKClasses::ENullArgument(__FUNC__, "ADB");
  FDB = ADB;
  FSchemeObjType = ASchemeObjType;
}

//---------------------------------------------------------------------------

void __fastcall TSDM::DataModuleCreate(TObject *Sender)
{
  //��������� ������ �� ������� ��������� "���� ������" ��� ���� �������
  //������, �������� � ���������� ������
  SetDMFIBComponentsDB( this, FDB );

  switch(FSchemeObjType)
  {
    case sotVaccine:
      FStartSchFN              = InfekchiyaStartSch;
      FSchOwnerTN              = VakchinaTableName;
      FSchOwnerNameFN          = VakchinaName;
      FSchOwnerInfekTN         = VakInfekTableName;
      FSchOwnerInfekInfekFN    = VakInfekInfek;
      FSchOwnerInfekSchOwnerFN = VakInfekVak;
    break;
    case sotTest:
      FStartSchFN              = InfekchiyaStartProba;
      FSchOwnerTN              = ProbTableName;
      FSchOwnerNameFN          = ProbName;
      FSchOwnerInfekTN         = ProbInfekTableName;
      FSchOwnerInfekInfekFN    = ProbInfekInfek;
      FSchOwnerInfekSchOwnerFN = ProbInfekProb;
    break;
  }

  #ifdef OLD_CODE_SUPPORT
    //������������� ���������� ����������
    switch(FSchemeObjType)
    {
      case sotVaccine:
        ::StartSchFN              = InfekchiyaStartSch;
        ::SchOwnerTN              = VakchinaTableName;
        ::SchOwnerNameFN          = VakchinaName;
        ::SchOwnerInfekTN         = VakInfekTableName;
        ::SchOwnerInfekInfekFN    = VakInfekInfek;
        ::SchOwnerInfekSchOwnerFN = VakInfekVak;
      break;
      case sotTest:
        ::StartSchFN              = InfekchiyaStartProba;
        ::SchOwnerTN              = ProbTableName;
        ::SchOwnerNameFN          = ProbName;
        ::SchOwnerInfekTN         = ProbInfekTableName;
        ::SchOwnerInfekInfekFN    = ProbInfekInfek;
        ::SchOwnerInfekSchOwnerFN = ProbInfekProb;
      break;
    }
  #endif

  //********** ����� ������ fdstVakchina ************************************
  //********** �������� ������ **********************************************
  /*
    SELECT *
    FROM   �������
    ORDER BY ������������
  */
  fdstVakchina->SQLs->SelectSQL->Text = Format(
    "SELECT * "
    "FROM %s "
    "ORDER BY %s",
    OPENARRAY(TVarRec, (FSchOwnerTN, FSchOwnerNameFN))
  );
  fdstVakchina->AutoUpdateOptions->UpdateTableName = FSchOwnerTN;
  fdstVakchina->AutoUpdateOptions->AutoReWriteSqls = true;
  fdstVakchina->AutoUpdateOptions->KeyFields       = TablePK;

  //********** ����� ������ fdstInfekchiya **********************************
  //********** ������������ ��� �������������� ��������� ���� � ���� ********
  /*
    SELECT *
    FROM   ��������
    ORDER BY ������������
  */
  fdstInfekchiya->SQLs->SelectSQL->Text = Format(
    "SELECT * "
    "FROM %s "
    "ORDER BY %s",
    OPENARRAY(TVarRec, (InfekchiyaTableName, InfekchiyaName))
  );
  fdstInfekchiya->AutoUpdateOptions->UpdateTableName = InfekchiyaTableName;
  fdstInfekchiya->AutoUpdateOptions->AutoReWriteSqls = true;
  fdstInfekchiya->AutoUpdateOptions->KeyFields       = TablePK;

  //********** ������ fqVaks ************************************************
  //********** ������������ ��������� ������ ������          ****************
  //********** ��� ��������� �������� ( �������� PInfekCode) ****************

  /*
    SELECT �������.CODE, �������.������������
    FROM   �������, ������� ��� ��������
    WHERE  ������� ��� ��������.�������� = ?��� �������� AND
           �������.CODE = ������� ��� ��������.�������
    ORDER BY �������.������������
  */
  fqVaks->SQL->Text = Format(
    "SELECT %s.%s, %s.%s "
    "FROM   %s, %s "
    "WHERE  %s.%s = ?PInfekCode AND "
    "       %s.%s = %s.%s "
    "ORDER BY %s.%s",
    OPENARRAY(
      TVarRec,
      (
        FSchOwnerTN, TablePK, FSchOwnerTN, FSchOwnerNameFN,
        FSchOwnerTN, FSchOwnerInfekTN,
        FSchOwnerInfekTN, FSchOwnerInfekInfekFN,
        FSchOwnerTN, TablePK, FSchOwnerInfekTN, FSchOwnerInfekSchOwnerFN,
        FSchOwnerTN, FSchOwnerNameFN
      )
    )
  );

  //********** ������ fqInfeks***********************************************
  //********** ������������ ��������� ������ ��������     *******************
  //********** ��� ��������� ������� ( �������� PVakCode) *******************

  /*
    SELECT ��������.CODE, ��������.������������
    FROM   ��������, ������� ��� ��������
    WHERE  ������� ��� ��������.������� = ?��� �������   AND
           ��������.CODE = ������� ��� ��������.��������
    ORDER BY ��������.������������
  */
  fqInfeks->SQL->Text = Format(
    "SELECT %s.%s, %s.%s "
    "FROM   %s, %s "
    "WHERE  %s.%s = ?PVakCode AND "
    "       %s.%s = %s.%s "
    "ORDER BY %s.%s",
    OPENARRAY(
      TVarRec,
      (
        InfekchiyaTableName, TablePK, InfekchiyaTableName, InfekchiyaName,
        InfekchiyaTableName, FSchOwnerInfekTN,
        FSchOwnerInfekTN   , FSchOwnerInfekSchOwnerFN,
        InfekchiyaTableName, TablePK, FSchOwnerInfekTN, FSchOwnerInfekInfekFN,
        InfekchiyaTableName, InfekchiyaName
      )
    )
  );
}

//---------------------------------------------------------------------------

void __fastcall TSDM::DataModuleDestroy(TObject *Sender)
{
  UpdatesRollback();
}

//---------------------------------------------------------------------------

void __fastcall TSDM::OpenDataSets()
{
  CloseOpenFIBDataSet(fdstInfekchiya);
  CloseOpenFIBDataSet(fdstVakchina);
}

//---------------------------------------------------------------------------

void __fastcall TSDM::CloseDataSets()
{
  fdstInfekchiya->Close();
  fdstVakchina->Close();
  if ( !ftrSch->InTransaction )
    ftrSch->Commit();
}

//---------------------------------------------------------------------------

bool __fastcall TSDM::UpdatesCommit()
{
  return SaveFIBTransactionUpdates( ftrSch, true, false );
}

//---------------------------------------------------------------------------

bool __fastcall TSDM::UpdatesCommitRetaining()
{
  return SaveFIBTransactionUpdates( ftrSch, true, true );
}

//---------------------------------------------------------------------------

bool __fastcall TSDM::UpdatesRollback()
{
  return SaveFIBTransactionUpdates( ftrSch, false, false );
}

//---------------------------------------------------------------------------

bool __fastcall TSDM::UpdatesRollbackRetaining()
{
  return SaveFIBTransactionUpdates( ftrSch, false, true );
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TSDM::GetVakName(int nVakId)
{
  UnicodeString Result = "";
  if ( fdstVakchina->Active )
    if ( fdstVakchina->Locate( TablePK, nVakId, NULL_SRCH_OPTS) )
      Result = fdstVakchina->FBN(FSchOwnerNameFN)->AsString;
  return Result;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TSDM::GetInfekName(int nInfekId)
{
  UnicodeString Result = "";
  if ( fdstInfekchiya->Active )
    if ( fdstInfekchiya->Locate( TablePK, nInfekId, NULL_SRCH_OPTS) )
      Result = fdstInfekchiya->FBN(InfekchiyaName)->AsString;
  return Result;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TSDM::GetFilterName(const int ACHType, const int AId)
{
  UnicodeString sName = "����������� ������";
  GetFilterNameQ->ParamByName("pCHType")->AsInteger = ACHType;
  GetFilterNameQ->ParamByName("pId")->AsInteger     = AId;
  START_FIBQUERY(GetFilterNameQ);
  if (!GetFilterNameQ->FN("NAME")->IsNull)
    sName = GetFilterNameQ->FN("NAME")->AsString;
  STOP_FIBQUERY(GetFilterNameQ);
  return sName;
}

//---------------------------------------------------------------------------

