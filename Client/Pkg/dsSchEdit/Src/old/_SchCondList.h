/*
  ����        - SchCondList.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ �������������� ������ ������� ��� ����.
                ������������ ����
  ����������� - ������� �.�., �������� �.�.
  ����        - 26.02.2007
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  26.02.2007
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef SchCondListH
#define SchCondListH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxListBox.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <ActnList.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include "XMLContainer.h"
//#include "ICSDocFilter.h"
#include "FilterEditEx.h"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
#include <System.Actions.hpp>

namespace SchList
{
class TfmSchList;
}

namespace SchCondList
{

//###########################################################################
//##                                                                       ##
//##                             TCondListForm                             ##
//##                                                                       ##
//###########################################################################

// ������ �������������� ������ ������� ��� ����

class TCondListForm : public TForm
{
__published:    // IDE-managed Components
    TPanel *BtnPanel;
    TcxListBox *CondList;
    TcxButton *InsertBtn;
    TcxButton *EditBtn;
    TcxButton *DeleteBtn;
    TcxButton *CancelBtn;
    TActionList *ActionList1;
    TAction *actInsert;
    TAction *actEdit;
    TAction *actDelete;
    TPopupMenu *PopupMenu1;
    TMenuItem *N1;
    TMenuItem *N2;
    TMenuItem *N3;
    void __fastcall actInsertExecute(TObject *Sender);
    void __fastcall actEditExecute(TObject *Sender);
    void __fastcall actDeleteExecute(TObject *Sender);


private:        // User declarations
    SchList::TfmSchList* FOwnFm;
    TTagNode*   FIMMS;
    bool        FEdit;
    bool        FUpdate;


protected:
    void __fastcall UpdateActions();
    bool __fastcall FilterEditCanClose(TObject *Sender, UnicodeString ACondName);
    UnicodeString __fastcall FGetMsgTxt(TFilterEditMsgType AType, UnicodeString ADefMsg);


public:         // User declarations
    __fastcall TCondListForm(TComponent* Owner,TTagNode *AImm_S);
    __property bool UpdateList = {read=FUpdate};
};

//---------------------------------------------------------------------------

} // end of namespace SchCondList
using namespace SchCondList;

#endif
