/*
  ����        - SchCpbltUpd.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ���� ����������.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 26.04.2006
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SchCpblt.h"
#include "SchCpbltUpd.h"
#include "DKClasses.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"

namespace SchCpbltUpd
{

//###########################################################################
//##                                                                       ##
//##                               TVI_DATA                                ##
//##                                                                       ##
//###########################################################################

// ��� ������, ��������������� � ����� TreeView
enum TTVIDataType
{
  dtUnSet,   // ������� "�� �����������"
  dtTagNode  // ���
};

//---------------------------------------------------------------------------

// ������, ��������������� � ����� TreeView
class TVI_DATA
{
private:
        TTVIDataType FType;     // ��� ������
        TTagNode*    FTagNode;  // ���
        /*
          Type == dtUnSet   ==> TagNode == NULL
          Type == dtTagNode ==> TagNode == <���>
        */


protected:
        void __fastcall SetType(TTVIDataType AValue);
        void __fastcall SetTagNode(TTagNode* AValue);


public:
        __fastcall TVI_DATA();

        __property TTVIDataType Type = {read = FType, write = SetType};
        __property TTagNode*    TagNode = {read = FTagNode, write = SetTagNode};
};

//---------------------------------------------------------------------------

} // end of namespace SchCpbltUpd

//###########################################################################
//##                                                                       ##
//##                               TVI_DATA                                ##
//##                                                                       ##
//###########################################################################

__fastcall TVI_DATA::TVI_DATA():
  FType(dtUnSet),
  FTagNode(NULL)
{
}

//---------------------------------------------------------------------------

void __fastcall TVI_DATA::SetType(TTVIDataType AValue)
{
  if (FType != AValue)
  {
    FType = AValue;
    if (FType == dtUnSet)
      FTagNode = NULL;
  }
}

//---------------------------------------------------------------------------

void __fastcall TVI_DATA::SetTagNode(TTagNode* AValue)
{
  if ((FTagNode != AValue) && (FType == dtTagNode))
    FTagNode = AValue;
}

//###########################################################################
//##                                                                       ##
//##                             TSchCpbltUpdFM                            ##
//##                                                                       ##
//###########################################################################

__fastcall TSchCpbltUpdFM::TSchCpbltUpdFM(TComponent* Owner, UnicodeString ASchemeUID)
        : TForm(Owner)
{
  FSchCpbltEditor = NULL;
  FSchemeUID      = ASchemeUID;
}

//---------------------------------------------------------------------------

__fastcall TSchCpbltUpdFM::~TSchCpbltUpdFM()
{
  for (int i = 0; i < tv->Items->Count; i++)
    if (tv->Items->Item[i]->Data)
      delete reinterpret_cast<TVI_DATA*>(tv->Items->Item[i]->Data);
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltUpdFM::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif

  if (!FSchCpbltEditor)
    throw DKClasses::EMethodError(
      __FUNC__,
      "�� ����������� ������ �� SchemeCompatibilityEditor."
    );

  TVI_DATA *pData = new TVI_DATA;
  tv->Items->AddObject(NULL, csNullRefLabel, pData);

  TTreeNode *RootNode = tv->Items->AddObject(NULL, "���� ����������", NULL);

  //��������� ����� ����
  TTagNode *LineNode = FSchCpbltEditor->VacSchXML->GetTagByUID(FSchemeUID)->GetFirstChild();
  while(LineNode)
  {
    LineNode->OnCopyItem = TagNodeToTree;
    LineNode->ToTree(tv->Items->AddChildObject(RootNode, "", NULL));
    LineNode->OnCopyItem = NULL;
    LineNode = LineNode->GetNext();
  }

  //�������� �����, ��������������� ���� "reflist"
  for ( int i = 0; i < tv->Items->Count; i++ )
  {
    TVI_DATA* pData = reinterpret_cast<TVI_DATA*>(tv->Items->Item[i]->Data);
    if (pData && (pData->Type == dtTagNode))
      if ( pData->TagNode->CmpName("reflist") )
        tv->Items->Item[i]->Delete();
  }

  //��������� �������� ����
  TTreeNode* SelNode = NULL;
  if (FLineUID.Length())
    for ( int i = 0; i < tv->Items->Count; i++ )
    {
      TVI_DATA* pData = reinterpret_cast<TVI_DATA*>(tv->Items->Item[i]->Data);
      if (
        pData &&
        (
          (
            (pData->Type == dtUnSet) &&
            (FLineUID == csReqFldNULL)
          ) ||
          (
            (pData->Type == dtTagNode) &&
            pData->TagNode->GetAttrByName("uid") &&
            pData->TagNode->CmpAV("uid", FLineUID)
          )
        )
      )
      {
        SelNode = tv->Items->Item[i];
        break;
      }
    }
  if (!SelNode)
    SelNode = RootNode;
  tv->Select(SelNode);
  tv->Selected->Expand(false);
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltUpdFM::SetSchemeCompatibilityEditor(
  SchList::TSchemeCompatibilityEditor* ASchCpbltEditor
)
{
  FSchCpbltEditor = ASchCpbltEditor;
}

//---------------------------------------------------------------------------

bool __fastcall TSchCpbltUpdFM::AreInputsCorrect()
{
  return true;
}

//---------------------------------------------------------------------------

// ����������� TagNode �� TreeNode

void __fastcall TSchCpbltUpdFM::TagNodeToTree(TTagNode *TagNode, TTreeNode *TreeNode)
{
  TVI_DATA *pData = new TVI_DATA;
  pData->Type    = dtTagNode;
  pData->TagNode = TagNode;
  TreeNode->Data = pData;
  if ( TagNode->CmpName("line") )
  {
    UnicodeString tmp, text;
    text = TagNode->AV["name"] + "  ( ���. ����� - " + XMLStrDurationToStr(TagNode->AV["min_pause"]);
    tmp = XMLStrDurationToStr(TagNode->AV["min_age"]);
    if ( tmp != "" )
      text += ", ���. ������� - " + tmp;
    tmp = GetSeasonDiap(TagNode->AV["season_beg"], TagNode->AV["season_end"]);
    if ( tmp != "" )
      text += ", ���������� - " + tmp;
    text += " )";
    TreeNode->Text = text;
  }
  if      ( TagNode->CmpName("endline") )
  {
    TreeNode->Text = csEndLineLabel;
  }
  else if ( TagNode->CmpName("srokjump") )
  {
    TreeNode->Text = "������� �� �����  (" + XMLStrDurationToStr(TagNode->AV["actualtime"]) + " )";
  }
  else if ( TagNode->CmpName("agejump") )
  {
    TreeNode->Text = "������� �� �������� (" + XMLStrDurationToStr(TagNode->AV["max_age"]) + ")";
  }
  else if ( TagNode->CmpName("jump"    ) )
  {
    TreeNode->Text =
      "������� �� ������� (" +
      FSchCpbltEditor->DM->GetFilterName(1, TagNode->AV["ref"].ToInt()) +
      ")";
  }
  else if ( TagNode->CmpName("endjump" ) )
  {
    TreeNode->Text = "����������� �������";
  }
  else if ( TagNode->CmpName("refer" ) )
  {
    UnicodeString sInfecName = FSchCpbltEditor->DM->GetInfekName(TagNode->AV["infref"].ToInt());
    UnicodeString sVacName;
    TTagNode *LineNode = FSchCpbltEditor->VacSchXML->GetTagByUID(TagNode->AV["ref"]);
    if (LineNode)
      sVacName = FSchCpbltEditor->DM->GetVakName(LineNode->GetParent()->AV["ref"].ToInt());
    UnicodeString sPrivivFullName = TfmSchList::GetPrivivFullName(
      FSchCpbltEditor->VacSchXML,
      sVacName,
      TagNode->AV["ref"]
    );
    TreeNode->Text = sInfecName + " -> " + sPrivivFullName;
    if ( TagNode->GetParent()->CmpName("reflist") )
      TreeNode->MoveTo( TreeNode->Parent->Parent, naAddChild );
  }
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltUpdFM::bOkClick(TObject *Sender)
{
  if (!AreInputsCorrect())
    return;

  TVI_DATA* pData = reinterpret_cast<TVI_DATA*>(tv->Selected->Data);
  if (pData->Type == dtUnSet)
  {
    FSchemeUID = csReqFldNULL;
    FLineUID   = csReqFldNULL;
    FVacType   = csReqFldNULL;
  }
  else if (pData->Type == dtTagNode)
  {
    FSchemeUID = pData->TagNode->GetParent()->AV["uid"];
    FLineUID   = pData->TagNode->AV["uid"];
    if (pData->TagNode->CmpName("line"))
      FVacType = pData->TagNode->AV["name"];
    else
      FVacType = csEndLineLabel;
  }

  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltUpdFM::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltUpdFM::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}

//---------------------------------------------------------------------------

bool __fastcall TSchCpbltUpdFM::IsRefObj(TTreeNode *Node)
{
  bool fResult = false;
  TVI_DATA* pData = reinterpret_cast<TVI_DATA*>(Node->Data);
  if (pData)
  {
    if (pData->Type == dtUnSet)
      fResult = true;
    else if (pData->Type == dtTagNode)
      fResult = pData->TagNode->CmpName("line,endline");
  }
  return fResult;
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltUpdFM::tvChange(TObject *Sender, TTreeNode *Node)
{
  bOk->Enabled = IsRefObj(Node);
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltUpdFM::tvCustomDrawItem(TCustomTreeView *Sender,
      TTreeNode *Node, TCustomDrawState State, bool &DefaultDraw)
{
  if (IsRefObj(Node))
    Sender->Canvas->Font->Style = TFontStyles() << fsBold;
  DefaultDraw = true;
}

//---------------------------------------------------------------------------

