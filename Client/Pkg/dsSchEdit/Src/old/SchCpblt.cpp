/*
  ����        - SchCpblt.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ �������������� ������������ ����� ����������.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 25.04.2006
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SchCpblt.h"
#include "SchCpbltUpd.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "AKSGRXGRIDS"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                             TSchCpbltFM                               ##
//##                                                                       ##
//###########################################################################

__fastcall TSchCpbltFM::TSchCpbltFM(TComponent* Owner)
        : TForm(Owner)
{
  FSchCpbltEditor = NULL;
  FGridNativeWndProc = Grid->WindowProc;
  Grid->WindowProc  = GridWndProc;
}

//---------------------------------------------------------------------------

__fastcall TSchCpbltFM::~TSchCpbltFM()
{
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltFM::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif

  if (!FSchCpbltEditor)
    throw DKClasses::EMethodError(
      __FUNC__,
      "�� ����������� ������ �� SchemeCompatibilityEditor."
    );

  Grid->RowCount = FSchCpbltEditor->ItemCount + 1;
  Grid->DefaultColWidth = GetGridDataColumnsTotalWidth(Grid) / 2;
}

//---------------------------------------------------------------------------

bool __fastcall TSchCpbltFM::AreInputsCorrect()
{
  return true;
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltFM::SetSchemeCompatibilityEditor(
  SchList::TSchemeCompatibilityEditor* ASchCpbltEditor
)
{
  FSchCpbltEditor = ASchCpbltEditor;
}

//---------------------------------------------------------------------------

int __fastcall TSchCpbltFM::GetGridDataColumnsTotalWidth(TDrawGrid* AGrid)
{
  // ��� ������� ����������� ���������� ������� � ������� � �����������,
  // ��������� ������ ������� �������� �������������� ������ ������ ���
  // ������-����������� ����� ���������
  int nColumnsWidth = AGrid->Width -                     // ������ DBGrid'�
                      GetSystemMetrics( SM_CXVSCROLL ) - // ������ ������������ ������ ���������
                      AGrid->ColCount;                   // ���������� �������

  if (AGrid->BorderStyle == bsSingle)                    // ���� ������� �����
  {
    nColumnsWidth -= 2;
    if (AGrid->Ctl3D)                                    //���� ������� ��������� ����
      nColumnsWidth -= 2;
  }

  return nColumnsWidth;
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltFM::GridWndProc(Messages::TMessage &Message)
{
  if (Message.Msg == WM_CTLCOLORSTATIC)
  {
    UnicodeString sWndTxt;
    int nWndTxtLen = GetWindowTextLength((HWND)Message.LParam);
    wchar_t *szWndTxt = new wchar_t[nWndTxtLen + 1];
    szWndTxt[0] = '\0';

    GetWindowText((HWND)Message.LParam, szWndTxt, nWndTxtLen + 1);
    sWndTxt = szWndTxt;
    delete [] szWndTxt;

    if (sWndTxt == csNullRefLabel)
    {
      SetTextColor((HDC)Message.WParam, ColorToRGB(clRed));
      SetBkColor((HDC)Message.WParam, ColorToRGB(Grid->Color));
      Message.Result = (int)GetCurrentObject(
        (HDC)Message.WParam,
        OBJ_BRUSH
      );
    }
    else
      FGridNativeWndProc(Message);
  }
  else
    FGridNativeWndProc(Message);
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltFM::bOkClick(TObject *Sender)
{
  if (!CheckDKFilterControls(Sender))   //��� ����������� �� ������ DKComp,
    return;                             //������������ TDKControlFilter

  if (!AreInputsCorrect())
    return;

  FSchCpbltEditor->Apply();
  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltFM::bCancelClick(TObject *Sender)
{
  FSchCpbltEditor->Cancel();
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltFM::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltFM::GridGetEditStyle(TObject *Sender, int ACol,
      int ARow, TEditStyle &Style)
{
  Style = esEllipsis;
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltFM::GridGetEditText(TObject *Sender, int ACol,
      int ARow, UnicodeString &Value)
{
  switch (ACol)
  {
    case 0:
      if (FSchCpbltEditor->ItemVacType[ARow - 1] == csReqFldNULL)
        Value = csNullRefLabel;
      else
        Value = FSchCpbltEditor->ItemVacType[ARow - 1];
    break;

    case 1:
      if (FSchCpbltEditor->ItemNewSchemeInfo[ARow - 1].sVacType == csReqFldNULL)
        Value = csNullRefLabel;
      else
		Value = FSchCpbltEditor->ItemNewSchemeInfo[ARow - 1].sVacType;
    break;
  }
  TRxDrawGrid *SenderGrid = dynamic_cast<TRxDrawGrid*>(Sender);
  if (SenderGrid->InplaceEditor)
    SendMessage(
      SenderGrid->InplaceEditor->Handle,
      EM_SETREADONLY,
      TRUE,
      0
    );
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltFM::GridDrawCell(TObject *Sender, int ACol,
      int ARow, TRect &Rect, TGridDrawState State)
{
  TRxDrawGrid *SenderGrid = dynamic_cast<TRxDrawGrid*>(Sender);
  UnicodeString sText;
  SenderGrid->Canvas->Font->Color = clWindowText;
  if (ARow)
  {
    switch (ACol)
    {
      case 0:
        if (FSchCpbltEditor->ItemVacType[ARow - 1] == csReqFldNULL)
          sText = csNullRefLabel;
        else
          sText = FSchCpbltEditor->ItemVacType[ARow - 1];
        Rect.Left += 3;
      break;

      case 1:
        if (FSchCpbltEditor->ItemNewSchemeInfo[ARow - 1].sVacType == csReqFldNULL)
        {
          sText = csNullRefLabel;
          SenderGrid->Canvas->Font->Color = clRed;
        }
        else
          sText = FSchCpbltEditor->ItemNewSchemeInfo[ARow - 1].sVacType;
        Rect.Left += 2;
      break;
    }
  }
  else
  {
    switch (ACol)
    {
      case 0: sText = "������� ��� ����������"; break;
      case 1: sText = "����� ��� ����������";   break;
    }
    SenderGrid->Canvas->Font->Style = TFontStyles() << fsBold;
    Rect.Left += 3;
  }
  ::DrawText(
    SenderGrid->Canvas->Handle,
    sText.c_str(),
    sText.Length(),
    &Rect,
    DT_NOCLIP | DT_LEFT | DT_VCENTER | DT_SINGLELINE
  );
}

//---------------------------------------------------------------------------

void __fastcall TSchCpbltFM::GridEditButtonClick(TObject *Sender)
{
  TRxDrawGrid *SenderGrid = dynamic_cast<TRxDrawGrid*>(Sender);
  TSchCpbltUpdFM *SchCpbltUpdFM = NULL;
  try
  {
    SchCpbltUpdFM = new TSchCpbltUpdFM(this, FSchCpbltEditor->NewSchemeUID);
    SchCpbltUpdFM->SetSchemeCompatibilityEditor(FSchCpbltEditor);
    SchCpbltUpdFM->LineUID = FSchCpbltEditor->ItemNewSchemeInfo[SenderGrid->Row - 1].sLineUID;
    if (SchCpbltUpdFM->ShowModal() == mrOk)
    {
      TUpdateSchemesMapCreator::TSchemeInfo SchInfo;
      SchInfo.sSchemeUID = SchCpbltUpdFM->SchemeUID;
      SchInfo.sLineUID   = SchCpbltUpdFM->LineUID;
      SchInfo.sVacType   = SchCpbltUpdFM->VacType;
      FSchCpbltEditor->ItemNewSchemeInfo[SenderGrid->Row - 1] = SchInfo;
      SenderGrid->EditorMode = false;
      SenderGrid->EditorMode = true;
    }
  }
  __finally
  {
    if (SchCpbltUpdFM) delete SchCpbltUpdFM;
  }
}

//---------------------------------------------------------------------------

