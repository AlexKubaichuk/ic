/*
  ����        - word_dm.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ��� ������������ �������� ���������� � ������� MS Word.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 28.06.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <algorithm>
#include "DKClasses.h"
#include "DKDBUtils.h"
#include "DKFileUtils.h"
#include "DKUtils.h"
#include "RxVclUtils.hpp"
#include "DKMBUtils.h"
#include "Plan.h"

#include "word_dm.h"
#include "Preview.h"

#include "DKGlobals.h"
#include "DMF.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "FIBDatabase"
#pragma link "FIBQuery"
#pragma link "pFIBDatabase"
#pragma link "pFIBQuery"
#pragma link "pFIBStoredProc"
#pragma link "FIBDataSet"
#pragma link "pFIBDataSet"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                                Globals                                ##
//##                                                                       ##
//###########################################################################

namespace word_dm
{

// ����� ������-��������
const UnicodeString csTemplateName[] = {"Plan.xml", "F63.xml"};

const UnicodeString csPlanPeriodActualOtvodSQLCond =
  " ( "
    "( "
      "O.R1043 = 1 AND "              //���������� �����
      "O.R105F <= ?pDatePE "
    ") OR "
    "( "
      "O.R1043 = 0 AND "           //��������� �����
      "O.R105F <= ?pDatePE AND "
      "O.R1065 >= ?pDatePB "
    ") "
   ") ";

} //end of namespace word_dm

//###########################################################################
//##                                                                       ##
//##                                 Twdm                                  ##
//##                                                                       ##
//###########################################################################

/*
 * �����������
 *
 *
 * ���������:
 *   ADB           - ������ �� ������� ��������� "���� ������"
 *   ADocType      - ��� ��������� ���������
 *   pInitData     - ��������� �� ������ ��� ������������� ( ������� �� ���� ��������� ��������� ):
 *                   ADocType == dtPlan ==> ��������� �� ��������� PLAN_DOC_INIT_DATA
 *                   ADocType == dtF63  ==> ��������� �� ��������� F63_DOC_INIT_DATA
 */

__fastcall Twdm::Twdm(TComponent* Owner, TpFIBDatabase *ADB, TDocType ADocType, void *pInitData )
        : TDataModule(Owner)
{
  if ( !ADB )
    throw DKClasses::ENullArgument(__FUNC__, "ADB");
  if ( !pInitData )
    throw DKClasses::ENullArgument(__FUNC__, "pInitData");

  FInitData  = pInitData;
  FDB        = ADB;
  FDocType   = ADocType;
  FDoc       = NULL;
  FDocCreate = false;
  FDocName   = "";
  FWDWait    = NULL;
  FPreviewCaption = "";

  //��������� ������ �� ������� ��������� "���� ������" ��� ���� �������
  //������, �������� � ���������� ������
  SetDMFIBComponentsDB( this, FDB );
}

//---------------------------------------------------------------------------

__fastcall Twdm::~Twdm()
{
  DestroyDoc();
  DestroyWaitForm();
}

//---------------------------------------------------------------------------

/*
 * ����� �������� ��������� ��������� MS Word
 *
 *
 * ������������ ��������:
 *   true  - Ok
 *   false - ������ �������� ��������� ���������
 */

bool __fastcall Twdm::CreateDoc()
{
  if (!FWDWait)
    throw DKClasses::EMethodError(__FUNC__, "�� ������� ����� ��� ����������� ���������.");
  try
  {
    FWDWait->InitCurProgress(100);

    if (FDoc)
      DestroyDoc();

    FWDWait->IncProgress(10, 20);
    FDoc = new TXMLDoc();
    FDocCreate = true;

    FWDWait->IncProgress(10, 20);

    FWDWait->IncProgress(10, 20);

    FWDWait->IncProgress(10, 20);
    FDoc->Open(AppendFileName(AppOptions->TemplatePath, csTemplateName[FDocType]));
    FWDWait->IncProgress(10, 20);
    return true;
   }
  catch (...)
   {
     DestroyDoc();
     MB_SHOW_ERROR(L"������ ������������ ��������� ���������.");
     return false;
   }
}

//---------------------------------------------------------------------------

//�����, ����������� ��������������� �������� ��������� ��������� � ��������� ������

void __fastcall Twdm::PreviewDoc()
{
  FWDWait->InitCurProgress( 100 );
  FWDWait->InitTotalProgress( 100 );
  TPreviewForm *pf = new TPreviewForm(this);
  TStringList *DDD = new TStringList;
  FDoc->AsStrings(DDD);
//   DDD->SaveToFile("c:\\__ddd.htm");
  pf->Preview(DDD,FDoc->TopMargin,FDoc->LeftMargin,FDoc->RightMargin,FDoc->BottomMargin,(TPrinterOrientation)FDoc->Orientation,(FDoc->Orientation)?pzHeight:pzWidth);
  delete DDD;
  delete pf;
//  FDoc->Preview(FDocName, FPreviewCaption,true,0,true);
  DestroyDoc();
}

//---------------------------------------------------------------------------

//�����, �������� �������-��������� MS Word
// (���� ������� �� �����, ��������� ������ ������ TXMLDoc)

void __fastcall Twdm::DestroyDoc()
{
  try
  {
    if( FDoc )
    {
//      FDoc->Close();
      DELETE_OBJ(FDoc);
    }
  }
  catch(...) {;}
  FDocCreate = false;
}

//---------------------------------------------------------------------------

/*
 * ����� ������ �����, ������������ ������� �������� ��������� ���������
 * (�� ��������� �����)
 *
 *
 * ������������ ��������:
 *   true  - Ok
 *   false - ������ �������� �����
 *
 */

bool __fastcall Twdm::ShowWaitForm()
{
  bool bResult = false;

  if( FWDWait )
  {
    FWDWait->Close();
    DELETE_OBJ(FWDWait);
  }
  FWDWait = new TfmOpProgress(this, "������������ ��������� ���������...");
  if (FWDWait)
  {
    FWDWait->SetTotalLabel( "������������ ���������" );
    FWDWait->Show();
    bResult = true;
  }

  return bResult;
}

//---------------------------------------------------------------------------

//����� �������� � �������� �����, ������������ ������� �������� ��������� ���������

void __fastcall Twdm::DestroyWaitForm()
{
  if( FWDWait )
  {
    FWDWait->Close();
    DELETE_OBJ(FWDWait);
  }
}

//---------------------------------------------------------------------------

//����� ���������� �����, ������������ ������� �������� ��������� ���������

void __fastcall Twdm::UpdateWaitForm()
{
  if( FWDWait )
    FWDWait->Update();
}

//---------------------------------------------------------------------------

//����� ���������� ��������� MS Word �������

void __fastcall Twdm::FillDoc(UnicodeString ATitle)
{
  if ( !FDoc )
    throw DKClasses::EMethodError(__FUNC__, "�� ������ ������ ���������.");

  ftrWD->StartTransaction();

  switch ( FDocType )
  {
    case dtPlan : FillDoc1(ATitle); break;
    case dtF63  : FillDoc2(ATitle); break;
  }

  CloseFIBTransaction( ftrWD, NULL );

//  FDoc->Save();
//  FDoc->Close();
}

//---------------------------------------------------------------------------

//���������� ��������� "���� ���������������� �������� � ����"

void __fastcall Twdm::FillDoc1(UnicodeString ATitle)
{
  //*************************************************************************
  // �������������
  //*************************************************************************
  PPLAN_DOC_INIT_DATA pINIT_DATA = reinterpret_cast<PPLAN_DOC_INIT_DATA>(FInitData);
  TXMLTab *CurTab;        // ������� �������
  UnicodeString SQLSelect, SQLFrom, SQLWhere, SQLOrderBy, SQLGroupBy;

  //����������� ���������� ����������� �������� � ����
  int nPlanItemCount;

  SQLSelect = "SELECT COUNT(Distinct cb.code) ";
  SQLFrom   = "FROM   CASEBOOK1 CB INNER JOIN CARD_1084 PL ON (CB.CODE = PL.UCODE AND PL.F_PLAN <> 0) ";
  if (pINIT_DATA->FilterDopFrom != "")
    SQLFrom += pINIT_DATA->FilterDopFrom;
  SQLWhere = "WHERE PL.R1087 = ?pPlanMonth";
  if ( pINIT_DATA->FilterWhere != "" )
    SQLWhere += " AND ( " +
                pINIT_DATA->FilterWhere +
                " ) ";
  SQLOrderBy = "";
  fqTemp->SQL->Text = SQLSelect + SQLFrom + SQLWhere + SQLOrderBy;
  FIBQ_DATEP(fqTemp, "pPlanMonth") = pINIT_DATA->PlanMonth;
  fqTemp->ExecQuery();
  nPlanItemCount = fqTemp->Fields[0]->AsInteger;
  fqTemp->Close();

  FWDWait->InitCurProgress(nPlanItemCount + 1 + 3);
  FWDWait->IncProgress(90);

  //*************************************************************************
  // ����� ���������
  //*************************************************************************
  CurTab = FDoc->GetTab(1);
  // ������������ ���
  CurTab->SetText(1, 1, GetLPUFullName());
  FWDWait->IncProgress(90);
  // ���� ������������ ���������
  CurTab->SetText(2, 1, Date().FormatString("dd.mm.yyyy"));
  FWDWait->IncProgress(90);
  // �������� �����
  CurTab->SetText(3, 2, pINIT_DATA->PlanMonth.FormatString(" mmmm yyyy �."));
  // ����������
  CurTab->SetText(4, 2, ATitle);
  FWDWait->IncProgress(90);

  //*************************************************************************
  // ����
  //*************************************************************************
  CurTab = FDoc->GetTab(3);
  if (nPlanItemCount > 1)
    CurTab->AppendRow(nPlanItemCount - 1);

  SQLSelect = "SELECT CB.CODE, ( CB.R001F || ' ' || CB.R0020 || ' ' || CB.R002F ) AS FIO, "
              "CB.R0031 AS BirthDay, PL.R1086 AS IsProb, PL.R1088 AS PlanDate, PL.R1089 AS VakId, "
              "PL.R108A AS ProbId, PL.R108D AS VypDate, V.R0040 AS VName, P.R002C AS PName, PL.R108B AS KindPrep, PL.VACTYPE, PL.F_PLAN ";
  SQLFrom   = "FROM   ( ( CASEBOOK1 CB INNER JOIN CARD_1084 PL ON (CB.CODE = PL.UCODE AND PL.F_PLAN <> 0) ) "
                       "LEFT JOIN "
                       "CLASS_0035 V ON PL.R1089 = V.CODE ) "
                     "LEFT JOIN CLASS_002A P ON PL.R108A = P.CODE ";
  if (pINIT_DATA->FilterDopFrom != "")
    SQLFrom += pINIT_DATA->FilterDopFrom;
  SQLWhere = "WHERE PL.R1087 = ?pPlanMonth ";
  if (pINIT_DATA->FilterWhere != "")
    SQLWhere += " AND ( " + pINIT_DATA->FilterWhere + " ) ";
  SQLOrderBy = " ORDER BY CB.R001F, CB.R0020, CB.R002F, CB.R0031, PL.R1088, V.R0040, P.R002C";
  fqQuery->SQL->Text = SQLSelect + SQLFrom + SQLWhere + SQLOrderBy;
  FIBQ_DATEP(fqQuery, "pPlanMonth") = pINIT_DATA->PlanMonth;

  int nRowInd = 1, nPtNo = 1;
  int nPrevPtId = -1;
  UnicodeString sType, sName, sKind, sPlanDate, sDate;
  const UnicodeString csExtraPlanItemViewText = "���.";
  fqQuery->ExecQuery();
  while (!fqQuery->Eof)
  {
    if (nPrevPtId != fqQuery->FN("CODE")->AsInteger)
    {
      if (sName.Length() && sPlanDate.Length())
      {
        CurTab->SetText(nRowInd, 4, sType);
        CurTab->SetText(nRowInd, 5, sName);
        CurTab->SetText(nRowInd, 6, sKind);
        CurTab->SetText(nRowInd, 7, sPlanDate);
        CurTab->SetText(nRowInd, 8, sDate);
      }
      nRowInd++;
      sType = "";
      sName = "";
      sKind = "";
      sPlanDate = "";
      sDate = "";
      CurTab->SetText(nRowInd, 1, IntToStr(nPtNo++));
      CurTab->SetText(nRowInd, 2, fqQuery->FN("FIO")->AsString);
      CurTab->SetText(nRowInd, 3, fqQuery->FN("BirthDay")->AsString);
    }
    nPrevPtId = fqQuery->FN("CODE")->AsInteger;

    switch (fqQuery->FN("F_PLAN")->AsInteger)
    {
      case pisManual:         AppendLine(sType, "�");  break;
      case pisPlanerClndr:    AppendLine(sType, "�");  break;
      case pisPlanerOffClndr: AppendLine(sType, "�");  break;
      case pisPlanerPrev:     AppendLine(sType, "�");  break;
      case pisPlanerRnd:      AppendLine(sType, "�");  break;
      case pisPlanerRndPrev:  AppendLine(sType, "��"); break;
      default:                AppendLine(sType, " ");  break;
    }
    if (fqQuery->FN("IsProb")->AsInteger)
      AppendLine(sName, fqQuery->FN("PName")->AsString);
    else
      AppendLine(sName, fqQuery->FN("VName")->AsString);
    AppendLine(sPlanDate, fqQuery->FN("PlanDate")->AsString);
    if (fqQuery->FN("VypDate")->IsNull)
      AppendLine(sDate, " ");  // ��� ����������� ������ ���� ������, ����� ���������
    else
      AppendLine(sDate, fqQuery->FN("VypDate")->AsString);

    if (!fqQuery->FN("KindPrep")->AsString.Length())  // �������������� ��������/�����
    {
      AppendLine(sKind, csExtraPlanItemViewText);
    }
    else
    {
      list<UnicodeString> slKinds;
      TStringList *SL = new TStringList;
      try
      {
        SL->Text = StringReplace(
          fqQuery->FN("VACTYPE")->AsString,
          "#",
          "\n",
          TReplaceFlags() << rfReplaceAll
        );
        for (int i = 0; i < SL->Count; i++)
        {
          UnicodeString sCurStr = SL->Strings[i].Trim();
          if (sCurStr.Length())
          {
            int nPos = sCurStr.Pos('.');
//            int nCurInfId = sCurStr.SubString(1, nPos - 1).Trim().ToInt();
            UnicodeString sCurKind = sCurStr.SubString(nPos + 1, sCurStr.Length() - nPos).Trim();
            if (sCurKind == "D")
              sCurKind = csExtraPlanItemViewText;
            slKinds.push_back(sCurKind);
          }
        }
      }
      __finally
      {
        delete SL;
      }
      if (slKinds.size())
      {
        // ���� ���� ���������� ��� ���� �������� ���������
        #pragma warn -csu
        if (slKinds.size() == std::count(slKinds.begin(), slKinds.end(), *(slKinds.begin())))
        #pragma warn .csu
          AppendLine(sKind, *(slKinds.begin()));
        else
        {
          UnicodeString sTmp;
          for (list<UnicodeString>::const_iterator i = slKinds.begin(); i != slKinds.end(); i++)
          {
            if (sTmp.Length())
              sTmp += ", ";
            sTmp += *i;
          }
          AppendLine(sKind, sTmp);
        }
      }
      else
        AppendLine(sKind, " ");
    }

    fqQuery->Next();
    FWDWait->IncProgress(90);
  }
  if (sName.Length() && sPlanDate.Length())
  {
    CurTab->SetText(nRowInd, 4, sType);
    CurTab->SetText(nRowInd, 5, sName);
    CurTab->SetText(nRowInd, 6, sKind);
    CurTab->SetText(nRowInd, 7, sPlanDate);
    CurTab->SetText(nRowInd, 8, sDate);
  }
  fqQuery->Close();
}

//---------------------------------------------------------------------------

//���������� ������-����������� ����� ��������� ���������

void __fastcall Twdm::AppendTableRowSeparator( TXMLTab *ATab )
{
  ATab->AppendRow();
  int nLastRow = ATab->RowCount();
//  ATab->SetText( nLastRow, 1, "\n" );
  for ( int i = 1; i <= ATab->ColCount(); i++ )
  {
    ATab->LeftBorder  ( nLastRow, i )->SetVisible(false);
    ATab->RightBorder ( nLastRow, i )->SetVisible(false);
    ATab->BottomBorder( nLastRow, i )->SetVisible(false);
    ATab->SetText( nLastRow, i, "\n" );
  }
}

//---------------------------------------------------------------------------

//���������� ��������� "����������� ����� �������� ( ����� �63 )"

void __fastcall Twdm::FillDoc2(UnicodeString APlDef)
{
  TTagNode *PlDef = NULL;
  try
   {
     PlDef = new TTagNode(NULL);
     if (APlDef.Trim().Length())
      PlDef->AsXML = APlDef;
     const UnicodeString csprTrueTxt     = "���";
     const UnicodeString csprFalseTxt    = "����";

     const UnicodeString csTrueTxt     = "�������������";
     const UnicodeString csFalseTxt    = "�������������";
     const UnicodeString csNotCheckTxt = "�� ���������";

     //*************************************************************************
     // �������������
     //*************************************************************************
     PF63_DOC_INIT_DATA pINIT_DATA = reinterpret_cast<PF63_DOC_INIT_DATA>(FInitData);
     int nTabInd = 1;      // ������ ������� �������
     TXMLTab *CurTab;        // ������� �������
     TDate PtBirthDay;     // ���� �������� ��������

     // ����������� ���������� ��������
     int nPrivivCount = 0;
     if (pINIT_DATA->bPrivivListCreate)
     {
	   fqQuery->SQL->Text = Format(UnicodeString(
		 "SELECT COUNT(*) "
		 "FROM CARD_%s1003 P "
         "WHERE P.UCODE = ?pPtId "),
         ARRAYOFCONST((pINIT_DATA->TablesPrefix))
       );
       FIBQ_INTP(fqQuery, "pPtId") = pINIT_DATA->nPtId;
       fqQuery->ExecQuery();
       nPrivivCount = fqQuery->Fields[0]->AsInteger;
       fqQuery->Close();
     }

     // ����������� ���������� ����
     int nProbCount = 0;
     if (pINIT_DATA->bProbListCreate)
     {
	   fqQuery->SQL->Text = Format(UnicodeString(
		 "SELECT COUNT(*) "
		 "FROM CARD_%s102F P "
		 "WHERE P.UCODE = ?pPtId "),
		 ARRAYOFCONST((pINIT_DATA->TablesPrefix))
       );
       FIBQ_INTP(fqQuery, "pPtId") = pINIT_DATA->nPtId;
       fqQuery->ExecQuery();
       nProbCount = fqQuery->Fields[0]->AsInteger;
       fqQuery->Close();
     }

     // ����������� �������� ��������� �������
     TDate DatePB;
     TDate DatePE;
     CalculateCurrentPlanPeriod(&DatePB, &DatePE, NULL);

     // ����������� ���������� ����������
     int nOtvodCount = 0;
     if (pINIT_DATA->bProbListCreate)
     {

       UnicodeString SQLText = Format(
         "SELECT COUNT(*) "
         "FROM CARD_%s1030 O "
         "WHERE O.UCODE = ?pPtId AND " + csPlanPeriodActualOtvodSQLCond +
         "GROUP BY O.R105F, O.R1065, O.R1043",
         ARRAYOFCONST((pINIT_DATA->TablesPrefix))
       );
       fqQuery->SQL->Text = SQLText;
       FIBQ_INTP (fqQuery, "pPtId"  ) = pINIT_DATA->nPtId;
       FIBQ_DATEP(fqQuery, "pDatePB") = DatePB;
       FIBQ_DATEP(fqQuery, "pDatePE") = DatePE;
       fqQuery->ExecQuery();
       while( !fqQuery->Eof )
       {
         nOtvodCount++;
         fqQuery->Next();
       }
       fqQuery->Close();
     }

     // ����������� ���������� ����������� �������� � ����
     int nPlanItemCount = 0;
     if (pINIT_DATA->bPlanCreate)
      nPlanItemCount = PlDef->Count;

     FWDWait->InitCurProgress(nPrivivCount + nProbCount + nOtvodCount + nPlanItemCount + 1 + 5);
     FWDWait->IncProgress(90);

     //*************************************************************************
     // ����� ���������
     //*************************************************************************
     CurTab = FDoc->GetTab(nTabInd++);
     // ������������ ���
     CurTab->SetText(1, 1, GetLPUFullName());
     FWDWait->IncProgress(90);
     // ���� ������������ ���������
     CurTab->SetText(2, 1, Date().FormatString("dd.mm.yyyy"));
     FWDWait->IncProgress(90);

     //*************************************************************************
     // �������� � ��������
     //*************************************************************************

	 fqQuery->SQL->Text = Format(UnicodeString(
	   "SELECT (R001F || ' ' || R0020 || ' ' || R002F) AS FIO, R0031 AS BirthDay, "
	   "       R0022 AS IsFormalAddr, R0024 AS NonFormalAddr, R0032 AS FormalAddrTown, "
	   "       R00FF AS FormalAddrStreet,"
	   "       R0100 AS FormalAddrHouse,"
	   "       R0033 AS KNum, R0122 as Ncard "
	   "FROM %sCASEBOOK1 "
       "WHERE CODE = ?pPtId"),
       ARRAYOFCONST((pINIT_DATA->TablesPrefix))
     );
     FIBQ_INTP(fqQuery, "pPtId") = pINIT_DATA->nPtId;
     fqQuery->ExecQuery();

     // ������
     if (fqQuery->FN("Ncard")->AsString.Length())
      CurTab->SetText(3, 1, "����������� ����� � "+fqQuery->FN("Ncard")->AsString);

     CurTab = FDoc->GetTab(nTabInd++);
     // ���
     CurTab->SetText(1, 2, fqQuery->FN("FIO")->AsString);
     FWDWait->IncProgress(90);
     // ���� ��������
     CurTab->SetText(2, 2, fqQuery->FN("BirthDay")->AsString);
     FWDWait->IncProgress(90);
     PtBirthDay = fqQuery->FN("BirthDay")->AsDate;
     // �����
     if (fqQuery->FN("IsFormalAddr")->AsInteger)
     {
       // ��������������� �����
       UnicodeString FormalAddr;
       fspSP->StoredProcName = Format(UnicodeString("CLASSDATA_%s0019"), ARRAYOFCONST((pINIT_DATA->TablesPrefix)));
       fspSP->ParamByName("CL_CODE")->AsInteger = fqQuery->FN("FormalAddrTown")->AsInteger;
       fspSP->ExecProc();
       FormalAddr = fspSP->ParamByName("STR_CLASS")->AsString;
       fspSP->Close();
       if (!fqQuery->FN("FormalAddrStreet")->IsNull)
        {
          fspSP->StoredProcName = Format(UnicodeString("CLASSDATA_%s0018"), ARRAYOFCONST((pINIT_DATA->TablesPrefix)));
          fspSP->ParamByName("CL_CODE")->AsInteger = fqQuery->FN("FormalAddrStreet")->AsInteger;
          fspSP->ExecProc();
          FormalAddr = fspSP->ParamByName("STR_CLASS")->AsString;
          fspSP->Close();
        }
       if (!fqQuery->FN("FormalAddrHouse")->IsNull && fqQuery->FN("FormalAddrHouse")->AsString.Trim().Length())
        FormalAddr += " �. " + fqQuery->FN("FormalAddrHouse")->AsString;
       if (!fqQuery->FN("KNum")->IsNull && fqQuery->FN("KNum")->AsString.Trim().Length())
        FormalAddr += " ��. " + fqQuery->FN("KNum")->AsString;
       CurTab->SetText(3, 2, FormalAddr);
     }
     else
       // ����������������� �����
       CurTab->SetText(3, 2, fqQuery->FN("NonFormalAddr")->AsString);
     FWDWait->IncProgress(90);
     fqQuery->Close();

     //*************************************************************************
     // ������ ��������
     //*************************************************************************
     if (pINIT_DATA->bPrivivListCreate && nPrivivCount)
     {
       CurTab = FDoc->GetTab(nTabInd++);

       // ��� ������ ��������
       if (nPrivivCount > 1)
         CurTab->AppendRow(nPrivivCount - 1);
	   fqQuery->SQL->Text = Format(UnicodeString(
		 "SELECT P.R1024 AS PDate, P.R1021 AS PType, P.R10AE AS IsRound, P.R1020 AS VakId, "
		 "       P.R1075 AS InfId, P.R1023 AS Serial, "
		 "       P.R1022 AS Doza, P.R1025 AS IsDone, P.R1026 AS IsCommon, "
		 "       P.R1028 AS IsLoc, "
		 "       P.R1027 AS LocVal, V.R0040 AS VName, F.R008E As Fmt, I.R003C AS IName "
		 "FROM ( "
		 "       ( "
		 "         CARD_%s1003 P INNER JOIN "
		 "         CLASS_%s0035 V ON "
		 "         P.R1020 = V.CODE "
		 "       ) INNER JOIN "
		 "       CLASS_%s003A I ON "
		 "       P.R1075 = I.CODE "
		 "     ) LEFT JOIN "
		 "     CLASS_%s0038 F ON "
		 "     P.R10A8 = F.CODE "
		 "WHERE P.UCODE = ?pPtId "
         "ORDER BY P.R1075, P.R1024"),
         ARRAYOFCONST((
           pINIT_DATA->TablesPrefix,
           pINIT_DATA->TablesPrefix,
           pINIT_DATA->TablesPrefix,
           pINIT_DATA->TablesPrefix
         ))
       );
       FIBQ_INTP(fqQuery, "pPtId") = pINIT_DATA->nPtId;
       fqQuery->ExecQuery();
       int nRowInd = 4;
       int nPrevInfId = -1;
       while (!fqQuery->Eof)
       {
         // ��������
         if (fqQuery->FN("InfId")->AsInteger != nPrevInfId)
         {
           nPrevInfId = fqQuery->FN("InfId")->AsInteger;
           CurTab->AddRow(nRowInd);
           CurTab->MergeCol(nRowInd, 1, 9);
           CurTab->SetHAlign(nRowInd, 1, 1);   // ������������ �� ������
           CurTab->SetBold(nRowInd, 1, true);  // bold
           CurTab->SetText(nRowInd, 1, fqQuery->FN("IName")->AsString);
           nRowInd++;
         }
         // ����
         CurTab->SetText(nRowInd, 1, fqQuery->FN("PDate")->AsString);
         // �������
         CurTab->SetText(nRowInd, 2, DateDurationToStr(PtBirthDay, fqQuery->FN("PDate")->AsDateTime));
         // ����
         CurTab->SetText(nRowInd, 3, fqQuery->FN("VName")->AsString);
         // ���
         CurTab->SetText(nRowInd, 4, fqQuery->FN("PType")->AsString);
         // ���
         if (fqQuery->FN("IsRound")->AsInteger)
           CurTab->SetText(nRowInd, 5, "+");

         // �����
         CurTab->SetText(nRowInd, 6, fqQuery->FN("Serial")->AsString);
         // ����
         CurTab->SetText(nRowInd, 7, fqQuery->FN("Doza")->AsString);

         UnicodeString CommonR, LocR;
         // ���� ������� ���������
         if (fqQuery->FN("IsDone")->AsInteger)
         {
           // ���� ����� �������

           CommonR = (fqQuery->FN("IsCommon")->AsInteger) ? csprTrueTxt : csprFalseTxt;
           // ���� ��� �������� ������� �������
           if (fqQuery->FN("LocVal")->IsNull || !fqQuery->FN("LocVal")->AsInteger)
             // ���� ������� �������
             LocR = (fqQuery->FN("IsLoc")->AsInteger) ? csprTrueTxt : csprFalseTxt;
           else
             // ���� �������� ������� �������
             LocR =  fqQuery->FN("Fmt")->AsString + " " +
                     fqQuery->FN("LocVal")->AsString;
         }
         else
         {
           CommonR = "";
           LocR    = "";
         }
         // ����� �������
         CurTab->SetText(nRowInd, 8, CommonR);
         // ������� �������
         CurTab->SetText(nRowInd, 9, LocR);
         fqQuery->Next();
         nRowInd++;
         FWDWait->IncProgress(90);
       }
       fqQuery->Close();
       // ������-����������� ����� ��������� ���������
       AppendTableRowSeparator(CurTab);
     }
     else
       FDoc->DeleteTab(nTabInd);

     //*************************************************************************
     // ������ ����
     //*************************************************************************
     if (pINIT_DATA->bProbListCreate && nProbCount)
     {
       CurTab = FDoc->GetTab(nTabInd++);

       // ��� ������ �����
       if (nProbCount > 1)
         CurTab->AppendRow(nProbCount - 1);
	   fqQuery->SQL->Text = Format(UnicodeString(
		 "SELECT P.R1031 AS PDate, P.R1032 AS ProbId, "
		 "       P.R103B AS IsDone, P.R1041 AS IsReac, P.R103F AS ReacVal, "
		 "       Pr.R002C AS PName, F.R008E As Fmt "
		 "FROM ( "
		 "       CARD_%s102F P INNER JOIN "
		 "       CLASS_%s002A Pr ON "
		 "       P.R1032 = Pr.CODE "
		 "     ) LEFT JOIN "
		 "     CLASS_%s0038 F ON "
		 "     P.R10A7 = F.CODE "
		 "WHERE P.UCODE = ?pPtId "
         "ORDER BY P.R1031"),
         ARRAYOFCONST((
           pINIT_DATA->TablesPrefix,
           pINIT_DATA->TablesPrefix,
           pINIT_DATA->TablesPrefix
         ))
       );
       FIBQ_INTP(fqQuery, "pPtId") = pINIT_DATA->nPtId;
       fqQuery->ExecQuery();
       int nRowInd = 3;
       while (!fqQuery->Eof)
       {
         // ����
         CurTab->SetText(nRowInd, 1, fqQuery->FN("PDate")->AsString);
         // �������
         CurTab->SetText(nRowInd, 2, DateDurationToStr(PtBirthDay, fqQuery->FN("PDate")->AsDateTime));
         // �����
         CurTab->SetText(nRowInd, 3, fqQuery->FN("PName")->AsString);

         // �������
         UnicodeString Reac;
         // ���� ������� ���������
         if (fqQuery->FN("IsDone")->AsInteger)
         {
           // ���� ��� �������� �������
           if (fqQuery->FN("ReacVal")->IsNull)
             // ���� �������
             Reac = (fqQuery->FN("IsReac")->AsInteger) ? csTrueTxt : csFalseTxt;
           else
             // ���� �������� �������
             Reac = fqQuery->FN("Fmt")->AsString + " " +
                    fqQuery->FN("ReacVal")->AsString;
         }
         else
           Reac = csNotCheckTxt;
         CurTab->SetText(nRowInd, 4, Reac);
         fqQuery->Next();
         nRowInd++;
         FWDWait->IncProgress(90);
       }
       fqQuery->Close();
       // ������-����������� ����� ��������� ���������
       AppendTableRowSeparator(CurTab);
     }
     else
       FDoc->DeleteTab(nTabInd);

     //*************************************************************************
     // ������ ����������
     //*************************************************************************
     if (pINIT_DATA->bOtvodListCreate && nOtvodCount)
     {
       CurTab = FDoc->GetTab(nTabInd++);

       // ���������� ������� ������ �������
       map<int,UnicodeString> OtvodReasonMap;
       TTagNode *ndChoice = pINIT_DATA->XMLContainer->GetXML(pINIT_DATA->ImmCardGUI )->GetTagByUID("1066");
       if (ndChoice)
       {
         TTagNode *ndChoiceValue = ndChoice->GetFirstChild();
         while(ndChoiceValue)
         {
           OtvodReasonMap[StrToInt(ndChoiceValue->AV["value"])] = ndChoiceValue->AV["name"];
           ndChoiceValue = ndChoiceValue->GetNext();
         }
       }

       // ��� ������� ������������ ���������
       if (nOtvodCount > 1)
         CurTab->AppendRow(nOtvodCount - 1);
       fqQuery->SQL->Text = Format(
         "SELECT O.R105F AS BegDate, O.R1065 AS EndDate, O.R1043 AS OType, "
                "O.R1083 AS InfId, "
                "O.R1066 AS Reason, O.R10A0 AS MKBClassified, "
                "O.R1082 AS MKBId, O.R10A1 AS ZabDesc, "
                "I.R003C AS InfName, MKB.R008A AS MKBCode, MKB.R0089 AS MKBName "
         "FROM ( CARD_%s1030 O LEFT JOIN CLASS_%s003A I ON O.R1083 = I.CODE ) "
              "LEFT JOIN CLASS_%s0052 MKB ON O.R1082 = MKB.CODE "
         "WHERE O.UCODE = ?pPtId AND " + csPlanPeriodActualOtvodSQLCond +
         "ORDER BY O.R105F, O.R1065, O.R1043, I.R003C",
         ARRAYOFCONST((pINIT_DATA->TablesPrefix, pINIT_DATA->TablesPrefix, pINIT_DATA->TablesPrefix))
       );
       FIBQ_INTP (fqQuery, "pPtId"  ) = pINIT_DATA->nPtId;
       FIBQ_DATEP(fqQuery, "pDatePB") = DatePB;
       FIBQ_DATEP(fqQuery, "pDatePE") = DatePE;
       fqQuery->ExecQuery();
       int nRowInd = 3;
       int nRecNo = 0;
       TDate PrevOtvodBegDate;
       TDate PrevOtvodEndDate;
       int nPrevOtvodType = -1;
       UnicodeString sBegDate, sEndDate, sInfText, sReason;
       while (!fqQuery->Eof)
       {
         // ���� ����������� ������� �� ����� �����
         if (
           (
             (
               (fqQuery->FN("OType")->AsInteger == 1) &&  // ���������� �����
               (
                 (PrevOtvodBegDate != fqQuery->FN("BegDate")->AsDate  ) ||
                 (nPrevOtvodType   != fqQuery->FN("OType")->AsInteger )
               )
             ) ||
             (
               (fqQuery->FN("OType")->AsInteger == 0) &&  // ��������� �����
               (
                 (PrevOtvodBegDate != fqQuery->FN("BegDate")->AsDate  ) ||
                 (PrevOtvodEndDate != fqQuery->FN("BegDate")->AsDate  ) ||
                 (nPrevOtvodType   != fqQuery->FN("OType")->AsInteger )
               )
             )
           ) &&
           (sBegDate.Length())
         )
         {
           // ��
           CurTab->SetText(nRowInd, 1, sBegDate);
           // ��
           CurTab->SetText(nRowInd, 2, sEndDate);
           // ��������� ��������
           CurTab->SetText(nRowInd, 3, sInfText);
           // ������� ���������
           CurTab->SetText(nRowInd, 4, sReason );
           sBegDate = "";
           sEndDate = "";
           sInfText = "";
           sReason  = "";
           nRowInd++;
           FWDWait->IncProgress(90);
         }

         // ��
         if (!sBegDate.Length())
           sBegDate = fqQuery->FN("BegDate")->AsString;
         // ��
         if (!sEndDate.Length() && !fqQuery->FN("EndDate")->IsNull)
           sEndDate = fqQuery->FN("EndDate")->AsString;
         // ��������� ��������
         if (sInfText.Length())
           sInfText += "\n";
         sInfText += fqQuery->FN("InfName")->AsString;
         // ������� ���������
         if (!sReason.Length())
           if (fqQuery->FN("Reason")->AsInteger == 0)            // �����������
           {
             if (fqQuery->FN("MKBClassified")->AsInteger)        // ���������������� �� ���
               sReason = "(" +
                         fqQuery->FN("MKBCode")->AsString +
                         ") " +
                         fqQuery->FN("MKBName")->AsString;
             else
               sReason = fqQuery->FN("ZabDesc")->AsString;
           }
           else
             sReason = OtvodReasonMap[fqQuery->FN("Reason")->AsInteger];
         PrevOtvodBegDate = fqQuery->FN("BegDate")->AsDate;
         if (fqQuery->FN("OType")->AsInteger == 0) // ��������� �����
           PrevOtvodEndDate = fqQuery->FN("BegDate")->AsDate;
         nPrevOtvodType = fqQuery->FN("OType")->AsInteger;
         nRecNo++;
         fqQuery->Next();
       }
       fqQuery->Close();
       if (sBegDate.Length())
       {
         // ��
         CurTab->SetText(nRowInd, 1, sBegDate);
         // ��
         CurTab->SetText(nRowInd, 2, sEndDate);
         // ��������� ��������
         CurTab->SetText(nRowInd, 3, sInfText);
         // ������� ���������
         CurTab->SetText(nRowInd, 4, sReason);
         FWDWait->IncProgress(90);
       }

       // ������-����������� ����� ��������� ���������
       AppendTableRowSeparator(CurTab);
     }
     else
       FDoc->DeleteTab(nTabInd);

     //*************************************************************************
     // ����
     //*************************************************************************
     if (pINIT_DATA->bPlanCreate && nPlanItemCount)
     {
       CurTab = FDoc->GetTab(nTabInd++);
       //��� ������� �������� �����
       if (nPlanItemCount > 1)
         CurTab->AppendRow(nPlanItemCount-1);

       TTagNode *itNode = PlDef->GetFirstChild();
       TTagNode *itNode1;
       int nRowInd = 3;
       while (itNode)
       {
         // ���� �����
         CurTab->SetText(nRowInd, 1, itNode->AV["date"]);

         // ������������ ����/�����
         CurTab->SetText(nRowInd, 2, itNode->AV["name"]);

         // ����
         if (itNode->Count)
          {
            UnicodeString FVType = itNode->AV["vactype"]+"$br$";
            itNode1 = itNode->GetFirstChild();
            while (itNode1)
             {
               FVType += itNode1->AV["vactype"]+"$br$";
               itNode1 = itNode1->GetNext();
             }
            CurTab->SetText(nRowInd, 3, FVType);
          }
         else
          CurTab->SetText(nRowInd, 3, itNode->AV["vactype"]);

         itNode = itNode->GetNext();
         nRowInd++;
         FWDWait->IncProgress(90);
       }
       fqQuery->Close();
       // ������-����������� ����� ��������� ���������
//       AppendTableRowSeparator(CurTab);
     }
     else
       FDoc->DeleteTab(nTabInd);
   }
  __finally
   {
     if (PlDef) delete PlDef;
   }
}
//---------------------------------------------------------------------------

UnicodeString __fastcall Twdm::GetLPUFullName()
{
  UnicodeString sLPUFullName;
  qGetLPUFullName->ExecQuery();
  sLPUFullName = qGetLPUFullName->FN("FULLNAME")->AsString;
  qGetLPUFullName->Close();
  return sLPUFullName;
}

//---------------------------------------------------------------------------
