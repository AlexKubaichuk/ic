/*
  ����        - LogView.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ����������� �������.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 10.11.2005
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "LogView.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "dxBar"
#pragma link "cxClasses"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma link "dxSkinsdxBarPainter"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                               Globals                                 ##
//##                                                                       ##
//###########################################################################

void __fastcall LogView::ViewLog (
  TComponent* AOwner,
  UnicodeString  ACaption,
  TStringList* AText
)
{
  TfmLogView *dlg = NULL;
  try
  {
    dlg = new TfmLogView( AOwner );
    dlg->Caption = ACaption;
    dlg->Memo->Lines->Assign( AText );
    dlg->ShowModal();
  }
  __finally
  {
    if ( dlg ) delete dlg;
  }
}

//###########################################################################
//##                                                                       ##
//##                              TfmLogView                               ##
//##                                                                       ##
//###########################################################################

__fastcall TfmLogView::TfmLogView(TComponent* Owner)
        : TForm(Owner)
{
}

//---------------------------------------------------------------------------

void __fastcall TfmLogView::actSaveAsExecute(TObject *Sender)
{
  if ( SaveDialog->Execute() )
    Memo->Lines->SaveToFile( SaveDialog->FileName );
}

//---------------------------------------------------------------------------

void __fastcall TfmLogView::actCloseExecute(TObject *Sender)
{
  Close();
}

//---------------------------------------------------------------------------

