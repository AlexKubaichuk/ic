/*
  ����        - DKGlobals.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ����������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 12.03.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  02.03.2007
    [+] ������� DurationToStr
    [+] ������� XMLStrDurationToStr
    [+] ������� DateDurationToStr
    [+] ������� AppendLine
    [*] ����� TICAppOptions
      [*] �������� AppPath ������������� � RootPath
      [+] �������� TemplatePath
      [+] �������� MSWordDotFileName
      [+] �������� MSTmpDocFileName
      [+] �������� MSTmpDocTmpFileName

  26.02.2007
    [+] ���������� ���������� AppOptions
    [+] ������������� � ����������� ������

  04.05.2006
    [+] ��������� DKCodeVersionState
    [+] ��������� DKCodeVersionStateNumber
    [*] ������� DKCodeVersion()

  23.03.2006
    [*] �����������

  11.11.2005 -  kab
    [+] ���� ��������� ������ ������������ TPlanErrMsg

  08.11.2005
    [+] ��������� DKCodeVersionMajor
    [+] ��������� DKCodeVersionMinor
    [+] ��������� DKCodeVersionRelease
    [+] ��������� DKCodeVersionBuild
    [+] ������� DKCodeVersion()
*/
//---------------------------------------------------------------------------

#ifndef DKGlobalsH
#define DKGlobalsH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������

#include <string.h>
#include <stdarg.h>
#include <System.hpp>
#include <SysUtils.hpp>
#include <Classes.hpp>
#include <XMLContainer.h>
#include "fib.hpp"
#include <DateUtils.hpp>
#include "DKUtils.h"
#include "DKClasses.h"
#include "DKRusMB.h"

namespace Dkglobals
{

//###########################################################################
//##                                                                       ##
//##                             ������ ����                               ##
//##                                                                       ##
//###########################################################################

enum TVersionState { vsAlpha, vsBeta, vsRC, vsRelease };
extern const int DKCodeVersionMajor;
extern const int DKCodeVersionMinor;
extern const int DKCodeVersionRelease;
extern const int DKCodeVersionBuild;
extern const TVersionState DKCodeVersionState;
extern const int DKCodeVersionStateNumber;

//---------------------------------------------------------------------------

extern UnicodeString __fastcall DKCodeVersion();

//###########################################################################
//##                                                                       ##
//##                              �������                                  ##
//##                                                                       ##
//###########################################################################

#define GET_EXTREF_FIRST(ext_ref)       _GUI(ext_ref)
#define GET_EXTREF_SECOND(ext_ref)      _UID(ext_ref)

//---------------------------------------------------------------------------

//����� ������� � �������� ���������� � ���������� �� ������ (� ������� "���������")
// ���������:
//  msg     - ���������
//  cpt     - ��������� �������
//  dopinfo - �������������� ����������

#define RMB_SHOW_ERRORQUESTIONEX_CPT(msg,cpt,dopinfo) (RusModalMessageBox( \
                                                          (msg), \
                                                          (cpt), \
                                                          mtError, \
                                                          TMsgDlgButtons() << mbYes << mbNo, \
                                                          mbNo, \
                                                          mbNo, \
                                                          true, \
                                                          (dopinfo) ))

//����� ������� � �������� ���������� � ���������� �� ������
// ���������:
//  msg     - ���������
//  cpt     - ��������� �������

#define RMB_SHOW_ERRORQUESTION_CPT(msg,cpt) (RusModalMessageBox( \
                                                          (msg), \
                                                          (cpt), \
                                                          mtError, \
                                                          TMsgDlgButtons() << mbYes << mbNo, \
                                                          mbNo, \
                                                          mbNo, \
                                                          false, \
                                                          NULL ))

//###########################################################################
//##                                                                       ##
//##                                ����                                   ##
//##                                                                       ##
//###########################################################################

// ��� ��������
enum TJumpKind
{
  jkUnknown,    // �����������
  jkSrok,       // �� �����
  jkAge,        // �� ��������
  jkCond,       // �� �������
  jkEnd         // �� ���������
};

// ��� �����������
enum TKontType
{
  ktNone = 0,
  ktUch  = 1,        // �� �������
  ktOrg  = 2,        // �� �����������
  ktFlt  = 3         // �� �������
};

// ��� ������� ������������
enum TPlanPeriodType
{
  ptNone     = -1,
  ptCur      = 0,       // �������
  ptPrev     = 1,       // ����������
  ptPrevPrev = 2,       // ����. ����������
  ptNext                // ��������� ( �������� ��� ����������� ������� ������������ )
};

// ��� ������ ���� TfmPlan::FDopID
enum TDopIDKind 
{
  dkUnknown = 0,
  dkUch     = 1,        // ���� TfmPlan::FDopID �������� ��� �������
  dkOrg     = 2,        // ���� TfmPlan::FDopID �������� ��� �����������
  dkFltVak  = 3,        // ���� TfmPlan::FDopID �������� ��� �������
  dkFltProb = 4         // ���� TfmPlan::FDopID �������� ��� �����
};

// ��� ������������ �� �������
enum TFltPlanKind 
{
  fpNone,               //������������ ������������� �� �� �������
  fpDop,                //�������������
  fpCal                 //�� ���������
};

// ��� �������� ������������
enum TPlanItemKind 
{
  pkVakchina = 0,       // �������
  pkProba    = 1,       // �����
  pkUnknown
};

// ��� ��������� �� ������ ������������
enum TPlanErrMsg 
{
  pmWarning = 1,        // ��������������
  pmError = 2           // ������
};

//###########################################################################
//##                                                                       ##
//##                             ���������                                 ##
//##                                                                       ##
//###########################################################################

extern const UnicodeString TablePK;                // ��� ����, ����������� ��������� ������

/*��������*/
extern const UnicodeString InfekchiyaTableName;    // �������� ������� "��������"
extern const UnicodeString InfekchiyaName;         // �������� ���� "������������"
extern const UnicodeString InfekchiyaVPriority;    // �������� ���� "��������� ����������"
extern const UnicodeString InfekchiyaRVPriority;   // �������� ���� "��������� ������������"
extern const UnicodeString InfekchiyaStartSch;     // �������� ���� "����� ���������� �� ���������"
extern const UnicodeString InfekchiyaStartProba;   // �������� ���� "����� ����� �� ���������"

/*������� ��� ��������*/
extern const UnicodeString VakInfekTableName;      // �������� ������� "������� ��� ��������"
extern const UnicodeString VakInfekInfek;          // �������� ���� "��������"
extern const UnicodeString VakInfekVak;            // �������� ���� "�������"

/*�������� -> �����*/
extern const UnicodeString ProbInfekTableName;     // �������� ������� "�������� -> �����"
extern const UnicodeString ProbInfekInfek;         // �������� ���� "��������"
extern const UnicodeString ProbInfekProb;          // �������� ���� "�����"

/*�������*/
extern const UnicodeString VakchinaTableName;      // �������� ������� "�������"
extern const UnicodeString VakchinaName;           // �������� ���� "������������"

/*�����*/
extern const UnicodeString ProbTableName;  // �������� ������� "�����"
extern const UnicodeString ProbName;       // �������� ���� "������������"

/*������ ���������*/
extern const UnicodeString RegListTableName;       // �������� ������� "��������"
extern const UnicodeString RegListLName;           // �������� ���� "�������"
extern const UnicodeString RegListName;            // �������� ���� "���"
extern const UnicodeString RegListSName;           // �������� ���� "��������"

/*����*/
extern const UnicodeString PlanPeriodType; // �������� ���� "��� ������� �����"
extern const UnicodeString PlanPeriod;     // �������� ���� "������ ������������"
extern const UnicodeString PlanPlanDate;   // �������� ���� "���� ������������ ����������"
extern const UnicodeString PlanVakId;      // �������� ���� "��� �������"
extern const UnicodeString PlanProbId;     // �������� ���� "��� �����"
extern const UnicodeString PlanKind_Prep;  // �������� ���� "���/��������"
extern const UnicodeString PlanPriority;   // �������� ���� "��������� ����������"
extern const UnicodeString PlanVypDate;    // �������� ���� "���� ��������� ����������"

//�����
extern const UnicodeString SchemeVakSch;   // �������� ���� "����� ������"
extern const UnicodeString SchemeProbSch;  // �������� ���� "����� ����"

//���������� NOT NULL ����, ���� �������� ��� ���� ������������
extern const UnicodeString csReqFldNULL;

extern const UnicodeString RusShortMonthNames[];
extern const UnicodeString RusLongMonthNames[];
extern const int RusMonthCount;
extern const UnicodeString WFNScheme[];
extern const UnicodeString WFNPrivivka[];
extern const UnicodeString WFNProba[];

//###########################################################################
//##                                                                       ##
//##                              �������                                  ##
//##                                                                       ##
//###########################################################################

// ����� �������� TListView (lv) � �������� nNewItemIndex
// ����� - ������� ������������� ������
extern int __fastcall FocusListViewItem(TListView *lv, int nNewItemIndex);

// ���������� ������ �������� � TListView
extern TListItem* __fastcall AddListViewItem(TListView *lv, UnicodeString Caption, TObject *Data, int ImageIndex = -1);

// ��������� ������������ ������ ��� ������������ �������
extern UnicodeString __fastcall DurationToStr(const int AYears, const int AMonths, const int ADays);

// ��������� ������������ ������ ��� ������������ �������, ���������� � xml-�������
extern UnicodeString __fastcall XMLStrDurationToStr(UnicodeString AValue);

// ��������� ������������ ������ ��� ������������ �������, ���������� ����� ������
extern UnicodeString __fastcall DateDurationToStr(TDate ADate1, TDate ADate2);

// ��������� ��������� ����������
extern UnicodeString __fastcall GetSeasonDiap (UnicodeString SeasonBeg, UnicodeString SeasonEnd);

/*
 * ����� ��������� ����� TTagNode � ����������� �� ������ ������ �����������
 * � ��������� UID'�� �����
 *
 *
 * ���������:
 *   [in/out] CurNode     - ������ ��������� ��� ������
 *   [in]     ExclNodeUID - UID ����� ��������� ��� ���������� ��� �� ������ ������
 *   [in]     ItItem      - ��������� �� �������, ������� ���������� ������� ��� ���������
 *                          ������� ����
 *   [in/out] Src1        - �������� ��� ������� ItItem
 *   [in/out] Src2        - --//--
 *   [in/out] Src3        - --//--
 *   [in/out] Src4        - --//--
 *
 * ������������ ��������:
 *   false - ����� ������ ��������� �������� (������� ItItem ��� ������� ���� ������� false )
 *   true  - ����� ������ �������� �� ��������� (������� ItItem ��� ������-�� ���� ������� true )
 *
 */
extern bool __fastcall TagNode_IterateExcl(
  TTagNode   *CurNode,
  UnicodeString *ExclNodeUID,
  TIterate1  ItItem,
  void       *Src1 = NULL,
  void       *Src2 = NULL,
  void       *Src3 = NULL,
  void       *Src4 = NULL
);

// ������ ������������ ��� ������ ������ � ��
extern int __fastcall AskOnFIBError(EFIBError* E, UnicodeString QuestionMsg);

// ����������, �������� �� ������ Str ������, ���� �� �� � Val ��������� ��� �����,
// � � ExStr - ��� �� ����� �� � ���� ������ � ���������� ����������� ������ � ���������
extern bool __fastcall IsNumberStr (UnicodeString Str,  UnicodeString *ExStr = NULL, int* Val = NULL);

/*
 * ���������� ������������ ���������� ������ ������� "���/��/��"
 *
 *
 * ���������:
 *   [in]  AgeString   - ����������� ������
 *   [out] ExAgeString - ������ AgeString � ���������� ��������� � ����������� ������
 *   [in]  bCanBeEmpty - ����������, ����� �� AgeString ���� ������ �������
 *
 * ������������ ��������:
 *   true  - Ok
 *   false - �������������� ������ ���������� �������
 *
 */
extern bool __fastcall IsCorrectAgeString (UnicodeString AgeString, UnicodeString &ExAgeString, bool bCanBeEmpty = true);

extern int __fastcall DKCorrectDay(Word Year, Word Month, Word Day);
extern bool __fastcall DKIsDateInPeriod(TDate Date, TDate PeriodBeg, TDate PeriodEnd);
extern int __fastcall AgeCmp(Word Years1, Word Months1, Word Days1, Word Years2, Word Months2, Word Days2);

inline void __fastcall AppendLine(UnicodeString &AStr, UnicodeString ALine)
{
  if (AStr.Length())
    AStr += "<br/>";
  AStr += ALine;
}

//###########################################################################
//##                                                                       ##
//##                            TICAppOptions                                ##
//##                                                                       ##
//###########################################################################

class TICAppOptions : public TObject
{
private:
    UnicodeString FRootPath;           // ���� � �������� ��
    UnicodeString FTemplatePath;
    UnicodeString FMSWordDotFileName;
    UnicodeString FMSTmpDocFileName;
    UnicodeString FMSTmpDocTmpFileName;
    TColor     FReqEdBkColor;       // ���� ���� ��� ��� ������������ ����� �����

    const UnicodeString FDefRootPath;
    const TColor FDefReqEdBkColor;
    const TColor FDefReqEdBkAltColor;

    inline void __fastcall UpdatePaths();


public:
    __fastcall TICAppOptions(bool fLoad = true);
    virtual __fastcall ~TICAppOptions();

    // ������
    static const UnicodeString csProgRegKeyBase;
    static const UnicodeString csCurrentVersionRegKey;
    static const UnicodeString csUserSettingRegKey;
    static const UnicodeString csTemplateDir;

    // ��� ������� � MS Word
    static const UnicodeString csMSWordDotFileName;
    static const UnicodeString csMSTmpDocFileName;
    static const UnicodeString csMSTmpDocTmpFileName;

    // ���� � �������� ��
    __property UnicodeString RootPath = {read = FRootPath};
    __property UnicodeString TemplatePath = {read = FTemplatePath};
    __property UnicodeString MSWordDotFileName = {read = FMSWordDotFileName};
    __property UnicodeString MSTmpDocFileName = {read = FMSTmpDocFileName};
    __property UnicodeString MSTmpDocTmpFileName = {read = FMSTmpDocTmpFileName};

    // ���� ���� ��� ��� ������������ ����� �����
    __property TColor     ReqEdBkColor = {read = FReqEdBkColor};

    void __fastcall Load();
};

//###########################################################################
//##                                                                       ##
//##                             ����������                                ##
//##                                                                       ##
//###########################################################################

extern TICAppOptions* AppOptions;         // ��������� ����������

//---------------------------------------------------------------------------

}
using namespace Dkglobals;

#ifdef OLD_CODE_SUPPORT
  #include "DKOld\DKGlobalsOld.h"
#endif

#endif
