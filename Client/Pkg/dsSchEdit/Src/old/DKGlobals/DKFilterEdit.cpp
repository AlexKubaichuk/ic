/*
  ����        - DKFilterEdit.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ �������������� �������.
                ���� ����������
  ����������� - ������� �.�., �������� �.�.
  ����        - 27.02.2007
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DKFilterEdit.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "ICSDocFilter"
#pragma link "TB2Item"
#pragma link "ICSDocBaseEdit"
#pragma link "cxButtons"
#pragma link "cxLookAndFeelPainters"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                             TFilterEditFm                             ##
//##                                                                       ##
//###########################################################################

__fastcall TDKFilterEditFm::TDKFilterEditFm(TComponent* Owner, TTagNode *AFilter) :
  TForm(Owner),
  FFlt(AFilter)
{
}

//---------------------------------------------------------------------------

void __fastcall TDKFilterEditFm::OkBtnClick(TObject *Sender)
{
  if (FOnCanClose)
  {
    if (FOnCanClose(this, DocFilter))
      ModalResult = mrOk;
  }
  else
    ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TDKFilterEditFm::FormShow(TObject *Sender)
{
  DocFilter->LoadNmkl();
  if (FFlt)
    DocFilter->Load(FFlt);
  else
    DocFilter->New(utOR);
  ActiveControl = DocFilter;
}

//---------------------------------------------------------------------------

