/*
  ����        - word_dm.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ��� ������������ �������� ���������� � ������� MS Word.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 28.06.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  26.02.2007
    [*] ������ � ���������� ���������� �������������� ����� ���������� ����������
        Dkglobals::AppOptions
    [!] ���������� �������� � ����� � ��������� �� ����������
    [*] �������� "����������� �����":
      [!] ����� ��� ��������������� �������������� ��������/�����
          ������������ ������ �������� � ������� ����, ��� ��� ��������������
      [+] ������� "���" � ������ �������� - ������� ������� ��������
      [+] ���������� �� ������������ ���� /����� � ������ ��������� �������� � ����
    [*] �������� "���� ���������������� �������� � ����":
      [+] ������� "���" � "���" - ��� � ��� ����������/�����
      [+] ���������� �� ������������ ���� /�����
    [*] ����������� ������������ ����������
    [*] kab
    [*] �����������

  03.05.2006
    [-] ����� KillWinWord()

  18.03.2006
    [*] kab

  10.02.2006
    [!] ��� ���������� ������� � ����������� � ����������� ����� �������� 

  08.02.2006 - kab
    [*] ��� ������������ ���� ���������������� �������� � ���� ��� ����� ��������
        � ����� ���������� � ���� ������, �������� �� ������� �������� ����� '\n'

  30.11.2005
    [*] ��������� �������������� "������ �������" ��� �������� � ����

  14.07.2005
    1. ����� ������� ��������, ��������� � ���, ��� ������ ������ MS Office'� �� �������
       �������� ������ � ������� � ������������� �����, ������� ��� ������������ ���������
       "���� ���������������� �������� � ����" ������ ����������� ����� ���������������
       ������� �������� ����������

  21.02.2005
    1. �������� ����� KillWinWord() ����� ��������� ��������� ���������

  22.12.2004
    1. ���������� ����� ��� ������������ ��������� � ��������� ������������ ( � ����������� ��������)
    2. ��������� ���� � ������� ���������� �� ���������� �������� � ������

  29.11.2004
    1. ���������� ������ ����� � ���������

  25.11.2004
    1. ��������� ������������ ��������� "����������� ����� �������� ( ����� �63 )"

  28.06.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef word_dmH
#define word_dmH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Clipbrd.hpp>
#include <ComObj.hpp>
#include <DB.hpp>
#include "FIBDatabase.hpp"
#include "FIBQuery.hpp"
#include "pFIBDatabase.hpp"
#include "pFIBQuery.hpp"
#include "pFIBStoredProc.hpp"
#include "FIBDataSet.hpp"
#include "pFIBDataSet.hpp"
#include "DKProgress.h"
#include "IcsXMLDoc.h"

#include "DKGlobals.h"

namespace word_dm
{

//###########################################################################
//##                                                                       ##
//##                                Globals                                ##
//##                                                                       ##
//###########################################################################

// ��� ��������� ���������

enum TDocType
{
  dtPlan = 0,           //���� ���������������� �������� � ����
  dtF63                 //����������� ����� �������� ( ����� �63 )
};

//---------------------------------------------------------------------------

// ������ ��� ������������� ��� ������������ ��������� "���� ���������������� �������� � ����"

typedef struct tagPLAN_DOC_INIT_DATA
{
  UnicodeString FilterWhere;       //������� ������ ����������� (WHERE �����)
  UnicodeString FilterDopFrom;     //���������� � ����� FROM ��� ������ �����������
  TDate      PlanMonth;         //����� ������������
  tagPLAN_DOC_INIT_DATA() : FilterWhere(""),
                            FilterDopFrom(""),    
                            PlanMonth(0) {};
}PLAN_DOC_INIT_DATA, *PPLAN_DOC_INIT_DATA;

//---------------------------------------------------------------------------

// ������ ��� ������������� ��� ������������ ��������� "����������� ����� �������� (����� �63)"

typedef struct tagF63_DOC_INIT_DATA
{
  int   nPtId;                  //��� ��������, ��� �������� ����������� ����������� �����
  bool  bPrivivListCreate;      //������� ������������� �������� � �������� ������ ��������� ��������
  bool  bProbListCreate;        //������� ������������� �������� � �������� ������ ��������� ����
  bool  bOtvodListCreate;       //������� ������������� �������� � �������� ������ ����������
  bool  bPlanCreate;            //������� ������������� �������� � �������� ���� �� ��������� �����
                                //( ��. PlanMonth )
  TDate PlanMonth;              //�������� �����
  TAxeXMLContainer *XMLContainer;       //xml-��������� � ����������� ��������� ��������������� ����� �
                                        //xml'��� �� ������� ���������� � ����
  UnicodeString ImmCardGUI;                //GUI ��������������� ����� � xml-����������
  UnicodeString SchVakGUI;                 //GUI xml'� �� ������� ����������
  UnicodeString SchProbGUI;                //GUI xml'� �� ������� ����
  UnicodeString TablesPrefix;      //������� ��� ������������ ������ �� � ����:
                                //  -- ��� ������ ��������� <pfx><������������ �������>, ��������: ACASEBOOK1
                                //  -- ��� ��������������� CLASS_<pfx><uid>, ��������: CLASS_A0035
                                //  -- ��� ����� CARD_<pfx><uid>, ��������: CARD_A1003
                                //  -- ��� �������� �������� CLASSDATA_<pfx><uid>, ��������: CLASSDATA_A0019                                
  tagF63_DOC_INIT_DATA() : nPtId(-1),
                           bPrivivListCreate(false),
                           bProbListCreate(false),
                           bOtvodListCreate(false),
                           bPlanCreate(false),
                           PlanMonth(0),
                           XMLContainer(NULL),
                           ImmCardGUI(""),
                           SchVakGUI(""),
                           SchProbGUI(""),
                           TablesPrefix("") {};
}F63_DOC_INIT_DATA, *PF63_DOC_INIT_DATA;

//###########################################################################
//##                                                                       ##
//##                                 Twdm                                  ##
//##                                                                       ##
//###########################################################################

// ������ ������ ��� ������������ �������� ����������

class Twdm : public TDataModule
{
__published:    // IDE-managed Components
    TpFIBTransaction *ftrWD;
    TpFIBQuery *fqTemp;
    TpFIBQuery *fqQuery;
    TpFIBStoredProc *fspSP;
    TpFIBQuery *qGetLPUFullName;

private:        // User declarations
    void*           FInitData;              //������ ��� �������������
    TpFIBDatabase*  FDB;                    //������ �� ������� ��������� "���� ������"
    TDocType        FDocType;               //��� ������������ ��������� ��������� MS Word
    bool            FDocCreate;             //������� �������� ��������� MS Word
    UnicodeString      FDocName;               //��� ��������� MS Word
    TXMLDoc*          FDoc;                   //�������� MS Word
    TfmOpProgress*  FWDWait;                //����� ��� ����������� �������� ������������
                                            //��������� ���������
    UnicodeString      FPreviewCaption;        //��������� ����� ���������������� ���������
                                            //��������� MS Word

    void __fastcall AppendTableRowSeparator( TXMLTab *ATab );

    void __fastcall FillDoc1(UnicodeString ATitle);
    void __fastcall FillDoc2(UnicodeString APlDef);


public:         // User declarations
    __fastcall Twdm(TComponent* Owner, TpFIBDatabase *ADB, TDocType ADocType, void *pInitData );
    inline __fastcall ~Twdm();

    __property TDocType       DocType   = { read = FDocType   };
    __property TfmOpProgress* WDWait    = { read = FWDWait    };
    __property bool           DocCreate = { read = FDocCreate };
    __property UnicodeString     PreviewCaption = { read = FPreviewCaption, write = FPreviewCaption };

    bool __fastcall CreateDoc ();
    void __fastcall PreviewDoc();
    void __fastcall DestroyDoc();
    bool __fastcall ShowWaitForm();
    void __fastcall UpdateWaitForm();
    void __fastcall DestroyWaitForm();
    void __fastcall FillDoc(UnicodeString ATitle = "");
    UnicodeString __fastcall GetLPUFullName();
};

//---------------------------------------------------------------------------

} //end of namespace word_dm
using namespace word_dm;

#endif
