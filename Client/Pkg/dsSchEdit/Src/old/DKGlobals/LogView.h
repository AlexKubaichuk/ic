/*
  ����        - LogView.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ����������� �������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 10.11.2005
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  08.11.2005
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef LogViewH
#define LogViewH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ActnList.hpp>
#include <ImgList.hpp>
#include <Dialogs.hpp>
#include "dxBar.hpp"
#include "cxClasses.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinsdxBarPainter.hpp"
#include <System.Actions.hpp>

namespace LogView
{

//###########################################################################
//##                                                                       ##
//##                              TfmLogView                               ##
//##                                                                       ##
//###########################################################################

class TfmLogView : public TForm
{
__published:    // IDE-managed Components
    TMemo *Memo;
    TActionList *ActionList;
    TAction *actSaveAs;
    TAction *actClose;
    TImageList *ImageList;
    TSaveDialog *SaveDialog;
    TdxBarManager *dxBarManager;
    TdxBarButton *dxBarButton1;
    TdxBarButton *dxBarButton2;
    void __fastcall actCloseExecute(TObject *Sender);
    void __fastcall actSaveAsExecute(TObject *Sender);
private:        // User declarations
public:         // User declarations
    __fastcall TfmLogView(TComponent* Owner);
};

//###########################################################################
//##                                                                       ##
//##                               Globals                                 ##
//##                                                                       ##
//###########################################################################

extern void __fastcall ViewLog (
  TComponent* AOwner,
  UnicodeString  ACaption,
  TStringList* AText
);

//---------------------------------------------------------------------------

} // end of namespace LogView
using namespace LogView;

#endif
