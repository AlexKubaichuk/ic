object wdm: Twdm
  OldCreateOrder = False
  Height = 210
  Width = 243
  object ftrWD: TpFIBTransaction
    Left = 8
    Top = 8
  end
  object fqTemp: TpFIBQuery
    Transaction = ftrWD
    Left = 144
    Top = 8
    qoTrimCharFields = True
  end
  object fqQuery: TpFIBQuery
    Transaction = ftrWD
    Left = 80
    Top = 8
    qoTrimCharFields = True
  end
  object fspSP: TpFIBStoredProc
    Transaction = ftrWD
    Left = 80
    Top = 64
    qoTrimCharFields = True
  end
  object qGetLPUFullName: TpFIBQuery
    Transaction = ftrWD
    SQL.Strings = (
      'select FULLNAME'
      'from SUBORDORG'
      'where SUBORD  = 2'
      '')
    Left = 144
    Top = 64
  end
end
