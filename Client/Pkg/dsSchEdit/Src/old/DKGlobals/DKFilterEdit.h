/*
  ����        - DKFilterEdit.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ �������������� �������.
                ������������ ����
  ����������� - ������� �.�., �������� �.�.
  ����        - 27.02.2007
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  13.03.2007
    [*] ������ ������������ � DKFilterEdit
    [*] TFilterEditFm ������������ � TDKFilterEditFm
     
  27.02.2007
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef DKFilterEditH
#define DKFilterEditH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������

#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <StdCtrls.hpp>
#include <Mask.hpp>
//#include "ICSDocFilter.h"
#include "TB2Item.hpp"
#include "ICSDocBaseEdit.h"
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>

namespace DKFilterEdit
{

//###########################################################################
//##                                                                       ##
//##                             TFilterEditFm                             ##
//##                                                                       ##
//###########################################################################

typedef bool __fastcall (__closure *TOnCanCloseEvent)(TObject *Sender, TICSDocFilter *ADocFilter);

//---------------------------------------------------------------------------

class TDKFilterEditFm : public TForm
{
__published:    // IDE-managed Components
    TICSDocFilter *DocFilter;
    TPanel *BtnPanel;
    TBevel *Bevel1;
    TcxButton *OkBtn;
    TcxButton *CancelBtn;
    void __fastcall OkBtnClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);


private:        // User declarations
    TTagNode*        FFlt;
    TOnCanCloseEvent FOnCanClose;


public:         // User declarations
    __fastcall TDKFilterEditFm(TComponent *Owner, TTagNode *AFilter);

    __property TOnCanCloseEvent OnCanClose = {read = FOnCanClose, write = FOnCanClose};    
};

//---------------------------------------------------------------------------

} // end of namespace DKFilterEdit
using namespace DKFilterEdit;

#endif
