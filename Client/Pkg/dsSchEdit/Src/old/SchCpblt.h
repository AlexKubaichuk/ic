/*
  ����        - SchCpblt.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ �������������� ������������ ����� ����������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 25.04.2006
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  25.04.2006
    �������������� �������.
*/
//---------------------------------------------------------------------------

#ifndef SchCpbltH
#define SchCpbltH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>

#include "SchList.h"
#include "AKSGRXGRIDS.hpp"

//---------------------------------------------------------------------------

namespace SchCpblt
{

//###########################################################################
//##                                                                       ##
//##                             TSchCpbltFM                               ##
//##                                                                       ##
//###########################################################################

// ������ �������������� ������������ ����� ����������

class TSchCpbltFM : public TForm
{
__published:    // IDE-managed Components
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
        TRxDrawGrid *Grid;
		void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bOkClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall GridGetEditStyle(TObject *Sender, int ACol,
          int ARow, TEditStyle &Style);
        void __fastcall GridGetEditText(TObject *Sender, int ACol,
          int ARow, UnicodeString &Value);
        void __fastcall GridDrawCell(TObject *Sender, int ACol, int ARow,
          TRect &Rect, TGridDrawState State);
        void __fastcall GridEditButtonClick(TObject *Sender);


private:        // User declarations
        SchList::TSchemeCompatibilityEditor* FSchCpbltEditor;
        TWndMethod        FGridNativeWndProc;


protected:
        bool __fastcall AreInputsCorrect();
        int __fastcall GetGridDataColumnsTotalWidth(TDrawGrid* AGrid);
        void __fastcall GridWndProc(Messages::TMessage &Message);


public:         // User declarations
        __fastcall TSchCpbltFM(TComponent* Owner);
        __fastcall ~TSchCpbltFM();

        // ��������� ��������� ������������ ����� ����������
        void __fastcall SetSchemeCompatibilityEditor(SchList::TSchemeCompatibilityEditor* ASchCpbltEditor);
};

//---------------------------------------------------------------------------

} //end of namespace SchCpblt
using namespace SchCpblt;

#endif
