/*
  ����        - SchList.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - �������� ���� ���������� ������/����.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 12.03.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SchList.h"

#include <string.h>
#include <algorithm>

#include "DKRusMB.h"
#include "DKDBUtils.h"
#include "DKMBUtils.h"
//#include "DKGlobals.h"
#include "DKSTL.h"

#include "SchUpd.h"
//#include "PrivivUpd.h"
//#include "CopiedSchName.h"
//#include "SchCpblt.h"
//#include "SchCondList.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "DKListBox"
#pragma link "dxBar"
#pragma link "cxControls"
#pragma link "cxGraphics"
#pragma link "dxStatusBar"
#pragma link "cxClasses"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma link "dxSkinsdxBarPainter"
#pragma link "dxSkinsdxStatusBarPainter"
#pragma link "cxClasses"
#pragma link "cxControls"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "DKListBox"
#pragma link "dxBar"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma link "dxSkinsdxBarPainter"
#pragma link "dxSkinsdxStatusBarPainter"
#pragma link "dxStatusBar"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                               Globals                                 ##
//##                                                                       ##
//###########################################################################

const UnicodeString SchList::csNullRefLabel = "�� �����������";
const UnicodeString SchList::csEndLineLabel = "���������";
const UnicodeString SchList::csEndPlanLabel = "��������� ������������";

//###########################################################################
//##                                                                       ##
//##                            TfmSchList                                 ##
//##                                                                       ##
//###########################################################################

// ASchemeObjType - ��� �������� ������� ��� �����

__fastcall TfmSchList::TfmSchList(
  TComponent*  Owner,
  TSchemeObjType ASchemeObjType
) :
  TForm(Owner),
  FSchemeObjType (ASchemeObjType),
  FDB            (NULL),
  FFilterList    (NULL),
  FIMM_S         (NULL),
  XMLModified    (false),
  FIMM_SModified (false),
  FEditorActive  (false),
  FEditorBookmark(NULL),
  FValidateOnSave(true),
  FSDM           (NULL),
  FOnLoadXML     (NULL),
  FOnSaveXML     (NULL)
{
  wcsncpy( MB_ProgramTitle, Application->Title.c_str(), 255);
  MB_ProgramTitle[255 - 1] = '\0';
  RMB_ProgramTitle = Application->Title;
}

//---------------------------------------------------------------------------

__fastcall TfmSchList::~TfmSchList()
{
  if (FSDM) delete FSDM;
  ClearBookmark();
}

//---------------------------------------------------------------------------

void __fastcall TfmSchList::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    actHelp->Visible = false;
    HelpMIt->Visible = ivNever; 	
	dxBarManager->BarByComponentName("Help")->Visible = false;
  #endif

  if (!CanEdit())
  {
    actAddScheme->Visible    = false;
    actEditScheme->Visible   = false;
    actCopyScheme->Visible   = false;
    actDeleteScheme->Visible = false;
    actAddPriviv->Visible    = false;
    actDeletePriviv->Visible = false;
  }

  //���������� �������� � ����������� �� ������
  switch (FSchemeObjType)
  {
    case sotVaccine:
      lbVakTitle->Caption = "����:";
      Caption = Caption + " ����";
    break;
    case sotTest:
      lbVakTitle->Caption = "�����:";
      Caption = Caption + " ����";
    break;
  }
  OpenEditor();
}

//---------------------------------------------------------------------------

void __fastcall TfmSchList::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  if ( FEditorActive && XMLModified )
	switch ( MB_COMMON( L"��������� ��������� ���������?",
                        L"������������� ���������� ���������",
						MB_ICONQUESTION | MB_YESNOCANCEL | MB_DEFBUTTON3 ) )
    {
      case IDYES   :
        if (!ApplyUpdates())
          Action = caNone;
      break;

      case IDNO    :
      break;

      case IDCANCEL:
        Action = caNone;
      break;
    }
  if (Action != caNone)
    CloseEditor();
}

//---------------------------------------------------------------------------

void __fastcall TfmSchList::SetXMLModified( bool AValue )
{
  FXMLModified = AValue;
  actApplyUpdates  ->Enabled = AValue;
  actCancelUpdates ->Enabled = AValue;
  dxStatusBar->Panels->Items[2]->Text = (AValue) ? "��������" : "";
}

//---------------------------------------------------------------------------

void __fastcall TfmSchList::SetDB(TpFIBDatabase* AValue)
{
  if (!AValue)
    throw DKClasses::ENullArgument(__FUNC__, "AValue");
  CloseEditor();
  FDB = AValue;
  if (FSDM) delete FSDM;
  FSDM = new TSDM(this, FDB, FSchemeObjType);
}

//---------------------------------------------------------------------------

void __fastcall TfmSchList::SetXMLContainer(TAxeXMLContainer *AValue)
{
  if (!AValue)
    throw DKClasses::ENullArgument(__FUNC__, "AValue");
  CloseEditor();
  FXMLContainer = AValue;
}

//---------------------------------------------------------------------------

void __fastcall TfmSchList::SetNmklGUI(UnicodeString AValue)
{
  CloseEditor();
  FNmklGUI = AValue;
}

//---------------------------------------------------------------------------

void __fastcall TfmSchList::SetIMM_S( TTagNode* AValue )
{
  if (!AValue)
    throw DKClasses::ENullArgument(__FUNC__, "AValue");
  CloseEditor();
  FIMM_S = AValue;
}

//---------------------------------------------------------------------------

//���������� enabled ��� ��������, ���������� ��� ������ ����

void __fastcall TfmSchList::UpdateSchemeActions()
{
  CheckActive(__FUNC__);
  bool bAddActEn = (bool)(cbVak->ItemIndex >= 0);
  bool bActEn    = (bool)(lbxScheme->Items->Count);

  actAddScheme     ->Enabled = bAddActEn;
  actEditScheme    ->Enabled = bActEn;
  actCopyScheme    ->Enabled = bActEn;
  actDeleteScheme  ->Enabled = bActEn;
}

//---------------------------------------------------------------------------

//���������� enabled ��� ��������, ���������� ��� ������ ����� ���� (��������)

void __fastcall TfmSchList::UpdatePrivivActions()
{
  CheckActive(__FUNC__);
  bool bAddActEn = (bool)(lbxScheme->ItemIndex >= 0);
  bool bActEn    = (bool)lvPrivivList->Items->Count;

  actAddPriviv   ->Enabled = bAddActEn;
  actEditPriviv  ->Enabled = bActEn;
  actDeletePriviv->Enabled = bActEn;
}

//---------------------------------------------------------------------------

//���������� ���������� � ������ �� ������ �������

void __fastcall TfmSchList::UpdateSchemeStatusPanel()
{
  CheckActive(__FUNC__);
  int nCurVakSchCount = lbxScheme->Items->Count;
//  int nTotalSchCount  = FIMM_S->GetChildByName("schemalist")->Count;

/*  dxStatusBar->Panels->Items[0]->Text =
    IntToStr(nCurVakSchCount) + " �� " + IntToStr(nTotalSchCount) + " " +
    GetWordForNumber(nCurVakSchCount, WFNScheme);*/
  dxStatusBar->Panels->Items[0]->Text =
    IntToStr(nCurVakSchCount) + " " +
    GetWordForNumber(nCurVakSchCount, WFNScheme);
}

//---------------------------------------------------------------------------

//���������� ���������� � ��������� �� ������ �������

void __fastcall TfmSchList::UpdatePrivivStatusPanel()
{
  CheckActive(__FUNC__);
  UnicodeString *WordForNumber;
  int nPrivivCount = 0;
  switch (FSchemeObjType)
  {
    case sotVaccine: WordForNumber = const_cast<UnicodeString*>(WFNPrivivka); break;
    case sotTest   : WordForNumber = const_cast<UnicodeString*>(WFNProba);    break;
  }
  if ( lvPrivivList->Items->Count > 1 )
    nPrivivCount = lvPrivivList->Items->Count - 1;
  dxStatusBar->Panels->Items[1]->Text =
    IntToStr(nPrivivCount) + " " +
    GetWordForNumber(nPrivivCount, WordForNumber);
}

//---------------------------------------------------------------------------

/*
 * ����������� ������� ������ �� ������ ������� ����� �� ������� �����
 * ������ ����
 *
 *
 * ���������:
 *   [in]  ItTag - ������� ���� xml-������
 *   [in]  Src1  - ��������� UID'�� �����, ������ �� ������� ���������� �����
 *   [out] Src2  - ������� ��������� ������ ( ����� ������������ ��� ���������� ������ ������ )
 *         Src3  - �� ������������
 *         Src4  - �� ������������
 *
 * ������������ ��������:
 *   true  - ���������� ��������� �����
 *   false - ���������� ���������� �����
 *
 */

bool __fastcall TfmSchList::IfCurSchemeLinesRefsExist(TTagNode *ItTag, void *Src1, void *Src2, void *Src3, void *Src4 )
{
  if (!ItTag || !Src1 || !Src2)
    return true;

  bool     &bRefFound = *((bool*)Src2);
  THexUIDs *HexUID    = (THexUIDs*)Src1;
  TAttr    *RefAttr   = ItTag->GetAttrByName("ref");

  bRefFound = false;
  if (RefAttr)
    if (HexUID->find(RefAttr->Value) != HexUID->end())
    {
      bRefFound = true;
      return true;
    }
  return false;
}

//---------------------------------------------------------------------------

bool __fastcall TfmSchList::SchemeLineDBRefsExist( TTagNode* SchemeLine )
{
  CheckActive(__FUNC__);
  if ( !SchemeLine )
    throw DKClasses::ENullArgument(__FUNC__, "SchemeLine");
  if ( !SchemeLine->CmpName( "line,endline" ) )
    throw DKClasses::EInvalidArgument(__FUNC__, "SchemeLine", "�������� ������ ��������������� ������ �����.");
  bool fResult = false;
  TpFIBQuery* q;
  switch(FSchemeObjType)
  {
    case sotVaccine: q = FSDM->fqPrivivCount; break;
    case sotTest   : q = FSDM->fqProbCount;   break;
  }
  if ( q )
  {
    q->ParamByName( "pSchemeLine" )->AsString =
      SchemeLine->GetParent()->AV["uid"] +
      "." +
      SchemeLine->AV["uid"];
    START_FIBQUERY(q)
    fResult = q->FN("Count")->AsInteger;
    STOP_FIBQUERY(q)
  }
  return fResult;
}

//---------------------------------------------------------------------------

//����������, ����� �� ������� ������� �����

TCanDelSchRes __fastcall TfmSchList::CanDeleteCurScheme()
{
  CheckActive(__FUNC__);
  TTagNode*  ActiveSchemeNode = GetActiveSchemeNode();
  if ( ActiveSchemeNode )
  {
    if ( lbxScheme->Items->Count == 1 )
      return crONLY_ONE_SCHEME;
    if ( IfCurSchemeIsStart() )
      return crCUR_SCHEME_IS_START;

    THexUIDs HexUIDs;
    TTagNode *CurSchemeLine = ActiveSchemeNode->GetFirstChild();
    while ( CurSchemeLine )
    {
      HexUIDs.insert(CurSchemeLine->AV["uid"]);
      CurSchemeLine = CurSchemeLine->GetNext();
    }
    bool bRefFound = false;
    UnicodeString ActiveSchemeUID = ActiveSchemeNode->AV["uid"];
    TagNode_IterateExcl( FIMM_S,
                         &ActiveSchemeUID,
                         IfCurSchemeLinesRefsExist,
                         &HexUIDs,
                         &bRefFound );
    if (bRefFound)
      return crCUR_SCHEME_LINES_REFS_EXIST;

    CurSchemeLine = ActiveSchemeNode->GetFirstChild();
    while ( CurSchemeLine )
    {
      if ( CurSchemeLine->CmpName( "line,endline" ) )
        if ( SchemeLineDBRefsExist(CurSchemeLine) )
          return crCUR_SCHEME_LINES_DBREFS_EXIST;
      CurSchemeLine = CurSchemeLine->GetNext();
    }

    return crCAN_DELETE_SCHEME;

  }
  return crNO_ACTIVE_SCHEME;
}

//---------------------------------------------------------------------------

//����������� TagNode �� ListItem � ������ ����������

bool __fastcall TfmSchList::AddLineInfo( TTagNode *LineNode, TListItem *ListItem )
{
  if ((!LineNode) || (!ListItem))
    return false;

  ListItem->Caption    = (LineNode->CmpName("line")) ? LineNode->AV["name"] : csEndLineLabel;
  ListItem->Data       = LineNode;
  ListItem->ImageIndex = iiNone;

  ListItem->SubItems->Add(XMLStrDurationToStr(LineNode->AV["min_pause"]));
  ListItem->SubItems->Add(XMLStrDurationToStr(LineNode->AV["min_age"]));
  ListItem->SubItems->Add(GetSeasonDiap(LineNode->AV["season_beg"], LineNode->AV["season_end"]));
  ListItem->SubItems->Add("");
  ListItem->SubItems->Add("");
  ListItem->SubItems->Add("");
  ListItem->SubItems->Add("");
  UpdateLineJumpsInfo(LineNode, ListItem);
  return true;
}

//---------------------------------------------------------------------------

//����������� TagNode �� ListItem � ������ ��������������

bool __fastcall TfmSchList::EditLineInfo( TTagNode *LineNode, TListItem *ListItem )
{
  if ((!LineNode) || (!ListItem))
    return false;

  ListItem->SubItems->Strings[0] = XMLStrDurationToStr(LineNode->AV["min_pause"]);
  ListItem->SubItems->Strings[1] = XMLStrDurationToStr(LineNode->AV["min_age"]);
  ListItem->SubItems->Strings[2] = GetSeasonDiap(
    LineNode->AV["season_beg"],
    LineNode->AV["season_end"]
  );
  UpdateLineJumpsInfo(LineNode, ListItem);
  return true;
}

//---------------------------------------------------------------------------

//����������� � ListItem ������� � ������� ���������

bool __fastcall TfmSchList::UpdateLineJumpsInfo( TTagNode *LineNode, TListItem *ListItem )
{
  if ((!LineNode) || (!ListItem))
    return false;
  ListItem->SubItemImages[3] = ( LineNode->GetChildByName("srokjump") ) ? iiJumpExists : iiNone;
  ListItem->SubItemImages[4] = ( LineNode->GetChildByName("agejump" ) ) ? iiJumpExists : iiNone;
  ListItem->SubItemImages[5] = ( LineNode->GetChildByName("jump"    ) ) ? iiJumpExists : iiNone;
  ListItem->SubItemImages[6] = ( LineNode->GetChildByName("endjump" ) ) ? iiJumpExists : iiNone;
  return true;
}

//---------------------------------------------------------------------------

//����������, ����� �� ������� ������� ��������

TCanDelPrivRes __fastcall TfmSchList::CanDeleteActivePriviv()
{
  CheckActive(__FUNC__);
  if ( lvPrivivList->ItemIndex != -1 )
  {
    if ( GetCurPrivivItem()->Caption == csEndLineLabel )
      return crEND_LINE;
    if ( lvPrivivList->ItemIndex != GetMaxPrivivItemIndex ( GetCurPrivivItem()->Caption[1] ) )
      return crNOT_GR_END_PRIVIV;

    THexUIDs HexUIDs;
    TTagNode *CurPriviv = (TTagNode*)lvPrivivList->Items->Item[lvPrivivList->ItemIndex]->Data;
    UnicodeString CurPrivivUID = CurPriviv->AV["uid"];
    HexUIDs.insert( CurPrivivUID );

    bool bRefFound = false;
    TagNode_IterateExcl( FIMM_S,
                         &CurPrivivUID,
                         IfCurSchemeLinesRefsExist,
                         &HexUIDs,
                         &bRefFound );
    if (bRefFound)
      return crCUR_PRIVIV_REFS_EXIST;

    if ( SchemeLineDBRefsExist(CurPriviv) )
      return crCUR_PRIVIV_DBREFS_EXIST;

    return crCAN_DELETE_PRIVIV;
  }
  return crNO_ACTIVE_PRIVIV;
}

//---------------------------------------------------------------------------

//���������� ������ ����� ������� �����

void __fastcall TfmSchList::UpdatePrivivList( TTagNode *SchemeNode )
{
  CheckActive(__FUNC__);
  lvPrivivList->Clear();
  int nSelPrivivInd = 0;
  if ( SchemeNode )
  {
    TTagNode *LineNode = SchemeNode->GetFirstChild();
    int nInd = 0;
    while ( LineNode )
    {
      if (FEditorBookmark && FEditorBookmark->sSchLineUID.Length())
        if (LineNode->AV["uid"] == FEditorBookmark->sSchLineUID)
          nSelPrivivInd = nInd;
      AddLineInfo( LineNode, lvPrivivList->Items->Add() );
      LineNode = LineNode->GetNext();
      nInd++;
    }
  }
//!!!  FocusListViewItem(lvPrivivList, nSelPrivivInd);

  UpdatePrivivActions();
  UpdatePrivivStatusPanel();
}

//---------------------------------------------------------------------------

//���������� TagNode, ��������������� ������� �����

TTagNode* __fastcall TfmSchList::GetActiveSchemeNode()
{
  CheckActive(__FUNC__);
  TTagNode *Result = NULL;
  if ( lbxScheme->ItemIndex >= 0 )
    Result = (TTagNode*)lbxScheme->Items->Objects[lbxScheme->ItemIndex];
  return Result;
}

//---------------------------------------------------------------------------

/*
 * ���������� ���������� ������ � ��������� xml-��������� ( ������������ ��� ����������� ����� )
 * ���� � �������� ����� ���� ���������� ������ ( �.�. ������ �� ������ ���� �� �����), �� �
 * ������������� ����� ��� ������ ��� �� ������ ���� �����������, � �� ��������� �� ������
 * �����-���������, ��� ��� ���� �� ��� ���������� ������� ������
 *
 *
 * ���������:
 *   [in]  ItTag - ������� ���� ���������
 *   [out] Src1  - ������� UID'�� ( ������� <������ UID> -> <����� UID> )
 *         Src2  - �� ������������
 *         Src3  - �� ������������
 *         Src4  - �� ������������
 *
 * ������������ ��������:
 *   true  - ���������� ���������� �����
 *   false - ���������� ���������� �����
 *
 */

bool __fastcall TfmSchList::UpdateInternalRefs (TTagNode *ItTag, void *Src1, void *Src2, void *Src3, void *Src4)
{
  if (!ItTag || !Src1)
    return true;

  TUpdateUIDTable *UpdateUIDTable = (TUpdateUIDTable*)Src1;

  UnicodeString NewRef, OldRef;
  TAttr *UIDAttr;
  if (ItTag->CmpName("refer"))
  {
    UIDAttr = ItTag->GetAttrByName("ref");
    if (UIDAttr)
    {
      OldRef = UIDAttr->Value;
      NewRef = (*UpdateUIDTable)[OldRef];
      if (Trim(NewRef) != "")
        UIDAttr->Value = NewRef;
    }
  }
  return false;
}

//---------------------------------------------------------------------------

//��������� ������ ���������� ����� ��� ����������� ��������. � ������ �������
//������ ������ ������ �������� lvPrivivList, ����� �������� ���������� ��������
//����� ��������. ���� ���� ������ ����� -2 - ��, �������� ������ ���� ���������
//����� ���������� �����
//
//������ -2? ��� ������ GetObject() ������� -1 �������� ����� ������, � ������
//���������� ����� ����������� �������� � ��������� ������ ��� �������� � ������

bool __fastcall TfmSchList::GetValidPrivivTypes ( TStrings *strings )
{
  if ( !strings ) return false;

  strings->Clear();

  UnicodeString CurPrivivType;
  int nMaxVNo = 0, nMaxRVMajorNo = 0, nMaxRVMinorNo = 0;
  int nMaxVNoInd = -2, nMaxRVMajorNoInd = -2;
  int nVNo, nRVMajorNo, nRVMinorNo;
  for ( int i = 0; i < lvPrivivList->Items->Count; i++ )
  {
    CurPrivivType = lvPrivivList->Items->Item[i]->Caption;
    if      ( CurPrivivType[1] == 'V' )    //����������
    {
      if ( CurPrivivType.Length() >= 2 )
      {
        nVNo = StrToIntDef( CurPrivivType.SubString( 2, CurPrivivType.Length() - 1 ), -1 );
        if ( nVNo > nMaxVNo )
        {
          nMaxVNo = nVNo;
          nMaxVNoInd = i;
        }
      }
    }
    else if ( CurPrivivType[1] == 'R' )    //������������
    {
      if ( CurPrivivType.Length() >= 5 )
      {
        nRVMajorNo = StrToIntDef( CurPrivivType.SubString( 3, CurPrivivType.Pos("/") - 3 ), -1 );
        if ( nRVMajorNo >= nMaxRVMajorNo )
        {
          nMaxRVMajorNo = nRVMajorNo;
          nMaxRVMajorNoInd = i;
        }
      }
    }
  }

  if ( nMaxVNo < 9 )
    strings->AddObject( (UnicodeString)"V" + IntToStr(nMaxVNo+1), (TObject*)nMaxVNoInd );

  if ( nMaxRVMajorNo <= 99 )
  {
    if ( nMaxRVMajorNoInd > -1 )
    {
      UnicodeString MaxRVCaption = lvPrivivList->Items->Item[nMaxRVMajorNoInd]->Caption;
      if ( MaxRVCaption.Length() >= 5 )
      {
        int SlashInd = MaxRVCaption.Pos("/");
        nMaxRVMinorNo = StrToIntDef( MaxRVCaption.SubString( SlashInd + 1, MaxRVCaption.Length() - SlashInd + 1 ), 0 );
      }
    }
    if ( ( nMaxRVMinorNo < 9 ) && ( nMaxRVMajorNo >= 1 ) )
      strings->AddObject( (UnicodeString)"RV" + IntToStr(nMaxRVMajorNo) + "/" + IntToStr(nMaxRVMinorNo + 1), (TObject*)nMaxRVMajorNoInd );

    if ( ( nMaxRVMajorNoInd <= -1 ) && ( nMaxVNoInd > -1 ) )
      nMaxRVMajorNoInd = nMaxVNoInd;
    if ( nMaxRVMajorNo < 99 )
      strings->AddObject( (UnicodeString)"RV" + IntToStr(nMaxRVMajorNo+1) + "/1", (TObject*)nMaxRVMajorNoInd );

  }

  return (bool)strings->Count;
}

//---------------------------------------------------------------------------

//��������� ������ ���������� ����� ��� ����������� �����. ���������� �����������
//������, �� ����������� ����, ��� ��� ���� ����������� ��� "��.i", i = 1..99

bool __fastcall TfmSchList::GetValidProbTypes ( TStrings *strings )
{
  if ( !strings ) return false;

  strings->Clear();

  UnicodeString CurProbType;
  int nMaxPNo = 0;
  int nMaxPNoInd = -2;
  int nPNo;
  for ( int i = 0; i < lvPrivivList->Items->Count; i++ )
  {
    CurProbType = lvPrivivList->Items->Item[i]->Caption;
    if      ( CurProbType[1] == '�' )    //�����
    {
      if ( CurProbType.Length() >= 4 )
      {
        nPNo = StrToIntDef( CurProbType.SubString( 4, CurProbType.Length() - 3 ), -1 );
        if ( nPNo > nMaxPNo )
        {
          nMaxPNo = nPNo;
          nMaxPNoInd = i;
        }
      }
    }
  }

  if ( nMaxPNo < 99 )
    strings->AddObject( (UnicodeString)"��." + IntToStr(nMaxPNo+1), (TObject*)nMaxPNoInd );

  return (bool)strings->Count;
}

//---------------------------------------------------------------------------

/*
 * ���������� ��������� � xml-���������
 *
 *
 * ������������ ��������:
 *   ������� ���������� ���������
 *
 */

bool __fastcall TfmSchList::ApplyUpdates()
{
  CheckActive(__FUNC__);
  bool fResult = false;
  if (XMLModified)
  {
    if (!FOnSaveXML)
      throw ESchListError(__FUNC__, "�� ���������� ���������� ������� OnSaveXML.");

    if (FValidateOnSave)
      if (ValidateXML(FDB, FSchemeObjType, FIMM_S))
        XMLModified = true;

    TTagNode *NewChangeNode = FIMM_S->GetChildByName("passport")->AddChild("changeshistory");
    NewChangeNode->AV["timestamp"] = Date().FormatString("yyyymmddhhnnss");

    try
    {
      SetBookmark();
      CloseEditor();
      if (FOnSaveXML(this, FSchemeObjType, FIMM_S))
      {
        XMLModified    = false;
        FIMM_SModified = true;
        fResult        = true;
      }
      OpenEditor();
    }
    __finally
    {
      ClearBookmark();
    }
  }
  return fResult;
}

//---------------------------------------------------------------------------

/*
 * ������ ���������, ��������� � xml-��������
 *
 *
 * ������������ ��������:
 *   ������� ������ ���������
 *
 */

bool __fastcall TfmSchList::CancelUpdates()
{
  CheckActive(__FUNC__);
  bool fResult = false;
  if (XMLModified)
  {
    if (!FOnLoadXML)
      throw ESchListError(__FUNC__, "�� ���������� ���������� ������� OnLoadXML.");

    try
    {
      SetBookmark();
      if (FOnLoadXML(this, FSchemeObjType, FIMM_S))
      {
        CloseEditor();
        OpenEditor();
      }
      XMLModified = false;
      fResult     = true;
    }
    __finally
    {
      ClearBookmark();
    }
  }
  return fResult;
}

//---------------------------------------------------------------------------

void __fastcall TfmSchList::CheckActive(const UnicodeString MethodName) const
{
  if (!FEditorActive)
    throw ESchListError(MethodName, "�������� �� �������.");
}

//---------------------------------------------------------------------------

bool __fastcall TfmSchList::OpenEditor()
{
  bool fResult = false;
  if (!FEditorActive)
  {
    fResult = true;
    if (!FDB)
      throw ESchListError(__FUNC__, "�� ���������� ������� ��������� \"���� ������\".");
    if (!FSDM)
      throw ESchListError(__FUNC__, "�� ������ ������ ������.");
    if (!FXMLContainer)
      throw ESchListError(__FUNC__, "�� ���������� XML-��������� � ����������� �������������.");
    if (!FNmklGUI.Length())
      throw ESchListError(__FUNC__, "�� ������ ��� ������������.");
    if (!FIMM_S)
    {
      if (!FOnLoadXML)
        throw ESchListError(__FUNC__, "�� ���������� ���������� ������� OnLoadXML.");
      if (!FOnLoadXML(this, FSchemeObjType, FIMM_S))
        fResult = false;
    }

    if (fResult)
    {
      if (!FIMM_S)
        throw ESchListError(__FUNC__, "�� ���������� xml-��������.");
      try
      {
        FEditorActive = true;
        FFilterList = new TStringList;
        // �������� ������ ������� ��� ����
        TTagNode *itSch = FIMM_S->GetChildByName("conditionlist");
        if (itSch)
        {
          itSch = itSch->GetFirstChild();
          while (itSch)
          {
            FFilterList->AddObject(itSch->AV["name"],(TObject*)UIDInt(itSch->AV["uid"]));
            itSch = itSch->GetNext();
          }
        }
        InfekCBx->Clear();

        //���������� ������� �������� � ������
        FSDM->OpenDataSets();
        InfekCBx->Items->Append("");
        FillStringsFromFIBDataSet(FSDM->fdstInfekchiya, InfekCBx->Items, InfekchiyaName, TablePK, true);

        if ( InfekCBx->Items->Count )
          if (FEditorBookmark && (FEditorBookmark->nInfId != -1))
            InfekCBx->ItemIndex = InfekCBx->Items->IndexOfObject(
              reinterpret_cast<TObject*>(FEditorBookmark->nInfId)
            );
          else
            InfekCBx->ItemIndex = 0;

        InfekCBx->OnSelect(InfekCBx);
        XMLModified = false;
      }
      catch(...)
      {
        CloseEditor();
        throw;
      }
    }
  }
  return fResult;
}

//---------------------------------------------------------------------------

bool __fastcall TfmSchList::CloseEditor()
{
  bool fResult = false;
  if (FEditorActive)
  {
    fResult = true;
    try
    {
      FSDM->CloseDataSets();
      InfekCBx->Clear();
      cbVak->Clear();
      lbxScheme->Clear();
      lvPrivivList->Clear();
      if (FFilterList) 
      {
        delete FFilterList;
        FFilterList = NULL;
      }
    }
    __finally
    {
      UpdateSchemeActions();
      UpdateSchemeStatusPanel();
      UpdatePrivivActions();
      UpdatePrivivStatusPanel();
      XMLModified = false;
      FEditorActive = false;
    }
  }
  return fResult;
}

//---------------------------------------------------------------------------

void __fastcall TfmSchList::SetBookmark()
{
  CheckActive(__FUNC__);
  if (!FEditorBookmark)
    FEditorBookmark = new TEditorBookmark;
  else
    FEditorBookmark->Clear();
  if (InfekCBx->ItemIndex != -1)
    if (InfekCBx->Items->Strings[InfekCBx->ItemIndex].Length())
      FEditorBookmark->nInfId = reinterpret_cast<int>(InfekCBx->Items->Objects[InfekCBx->ItemIndex]);
  if (cbVak->ItemIndex != -1)
    FEditorBookmark->nVacId = reinterpret_cast<int>(cbVak->Items->Objects[cbVak->ItemIndex]);
  if (lbxScheme->ItemIndex != -1)
    FEditorBookmark->sSchUID = dynamic_cast<TTagNode*>(
      lbxScheme->Items->Objects[lbxScheme->ItemIndex]
    )->AV["uid"];
  if (lvPrivivList->ItemIndex != -1)
    FEditorBookmark->sSchLineUID = reinterpret_cast<TTagNode*>(
      lvPrivivList->Items->Item[lvPrivivList->ItemIndex]->Data
    )->AV["uid"];
}

//---------------------------------------------------------------------------

void __fastcall TfmSchList::ClearBookmark()
{
  __DELETE_OBJ(FEditorBookmark)
}

//---------------------------------------------------------------------------

TTagNode* __fastcall TfmSchList::NewScheme(UnicodeString ASchemeName, int AVacId, TTagNode *AXML)
{
  if (!AXML)
    throw DKClasses::ENullArgument(__FUNC__, "AXML");
  TTagNode *SchemeNode = AXML->GetChildByName("schemalist")->AddChild("schema");
  SchemeNode->AV["uid"]  = AXML->NewUID();
  SchemeNode->AV["name"] = ASchemeName;
  SchemeNode->AV["ref"]  = IntToStr(AVacId);
  TTagNode *EndLineNode = SchemeNode->AddChild("endline");
  EndLineNode->AV["uid"] = AXML->NewUID();
  return SchemeNode;
}

//---------------------------------------------------------------------------

/*
 * ��������� xml-��������� �� ������� ������� ���� ��� ������ �������
 *
 *
 * ���������:
 *   [in]     ASDM - ������ ������
 *   [in\out] AXML - xml-��������
 *
 * ������������ ��������:
 *   ������� ����������� xml-���������
 *
 */

bool __fastcall TfmSchList::ValidateSchemeExistence(TSDM* ASDM, TTagNode *AXML)
{
  bool fModified = false;
  ASDM->fdstVakchina->First();
  while (!ASDM->fdstVakchina->Eof)
  {
    int nVacId = ASDM->fdstVakchina->FieldByName(TablePK)->AsInteger;
    TTagNode* SchemeListNode = AXML->GetChildByName("schemalist");
    TTagNode* SchemeNode = SchemeListNode->GetChildByAV(
      "schema",
      "ref",
      IntToStr(nVacId)
    );
    if (!SchemeNode)
      fModified = (SchemeNode = NewScheme("1", nVacId, AXML));
    ASDM->fdstVakchina->Next();
  }
  return fModified;
}

//---------------------------------------------------------------------------

/*
 * ��������� xml-��������� �� ������� ���������� ���������
 *
 *
 * ���������:
 *   [in]     ASDM - ������ ������
 *   [in\out] AXML - xml-��������
 *
 * ������������ ��������:
 *   ������� ����������� xml-���������
 *
 */

bool __fastcall TfmSchList::ValidateSchemeJumps(TSDM* ASDM, TTagNode *AXML)
{
  bool fModified = false;

  TStringList *VacInfs = NULL;
  try
  {
    VacInfs = new TStringList;
    TTagNode* SchemeNode = AXML->GetChildByName("schemalist")->GetFirstChild();
    while(SchemeNode)
    {
      int nVacId = SchemeNode->AV["ref"].ToInt();
      ASDM->fqInfeks->ParamByName("PVakCode")->AsInteger = nVacId;
      FillStrings(ASDM->fqInfeks, VacInfs, InfekchiyaName, TablePK, false);

      TTagNode *LineNode = SchemeNode->GetFirstChild();
      while(LineNode)
      {
        TTagNode *JumpNode = LineNode->GetFirstChild();
        while(JumpNode)
        {
          TTagNode *RefList = (JumpNode->CmpName("jump"))
                              ? JumpNode->GetChildByName("reflist")
                              : JumpNode;
          // �������� ������ �� ���������, �� ��������������� �������
          TTagNode *Refer = RefList->GetFirstChild();
          while(Refer)
          {
            int nInfId = Refer->AV["infref"].ToInt();
            if (VacInfs->IndexOfObject(reinterpret_cast<TObject*>(nInfId)) == -1)
            {
              TTagNode *NodeToDelete = Refer;
              Refer = Refer->GetNext();
              delete NodeToDelete;
              fModified = true;
            }
            else
              Refer = Refer->GetNext();
          }
          // ���������� ����������� ������ �� ��������� ����������� �� ���������
          for (int i = 0; i < VacInfs->Count; i++)
          {
            int nInfId = reinterpret_cast<int>(VacInfs->Objects[i]);
            if (!RefList->GetChildByAV("refer", "infref", IntToStr(nInfId)))
            {
              TTagNode *NewRefer = RefList->AddChild("refer");
              NewRefer->AV["infref"] = IntToStr(nInfId);
              NewRefer->AV["ref"]    = EmptyRef;
              fModified = true;
            }
          }
          JumpNode = JumpNode->GetNext();
        }
        LineNode = LineNode->GetNext();
      }
      SchemeNode = SchemeNode->GetNext();
    }
  }
  __finally
  {
    if (VacInfs) delete VacInfs;
  }
  return fModified;
}

//---------------------------------------------------------------------------

bool __fastcall TfmSchList::CanEdit()
{
  bool bResult = false;
  switch (FSchemeObjType)
  {
    case sotVaccine:
      #ifdef IMM_PLAN_VAC_SCH_EDIT
        bResult = true;
      #endif
    break;
    case sotTest:
      #ifdef IMM_PLAN_PROB_SCH_EDIT
        bResult = true;
      #endif
    break;
  }
  return bResult;
}

//---------------------------------------------------------------------------

bool __fastcall TfmSchList::ValidateXML(TpFIBDatabase *ADB, TSchemeObjType ASchemeObjType, TTagNode *AXML)
{
  bool fModified = false;
  TSDM* SDM = NULL;
  try
  {
    SDM = new TSDM(NULL, ADB, ASchemeObjType);
    SDM->OpenDataSets();
    fModified |= ValidateSchemeExistence(SDM, AXML);
    fModified |= ValidateSchemeJumps(SDM, AXML);
    SDM->CloseDataSets();
  }
  __finally
  {
    if (SDM) delete SDM;
  }
  return fModified;
}

//---------------------------------------------------------------------------

//���������� �����

bool __fastcall TfmSchList::AddScheme( UnicodeString SchemeName, int nVakId, TTagNode *SchemeNode )
{
  CheckActive(__FUNC__);
  bool bResult  = false;

  //����� ������� � ID nVakId � ������� � ���������, ���� ����� ����������� �� ��� ������� �������
  int nVakItemIndex = cbVak->Items->IndexOfObject( (TObject*)nVakId );
  if ( nVakItemIndex != cbVak->ItemIndex )
  {
    cbVak->ItemIndex = nVakItemIndex;
    cbVak->OnSelect(cbVak);
  }

  //���� �� ������ TagNode ��� ����������� �����, �� �� ���������
  if ( !SchemeNode )
  {
    SchemeNode = NewScheme(SchemeName, nVakId, FIMM_S);
    if (SchemeNode)
      XMLModified = true;
  }

  //���������� �������� � ������ ����
  if ( SchemeNode )
  {
    lbxScheme->AddItem(SchemeNode->AV["name"], (TObject*)SchemeNode);
    bResult     = true;
  }

  //���������� �������� � ������ �������
  if (bResult)
  {
    UpdateSchemeActions();
    UpdateSchemeStatusPanel();
  }
  return bResult;
}

//---------------------------------------------------------------------------

//��������� ���������� �����

void __fastcall TfmSchList::EditScheme( UnicodeString SchemeName, int nVakId )
{
  CheckActive(__FUNC__);
  //���� nVakId ��������� � ID ������� �������, �� ������ �������� ������������ �����
  if ( nVakId == (int)cbVak->Items->Objects[cbVak->ItemIndex]  )
  {
    SetActiveSchemeAttrVal( "name", SchemeName );
    lbxScheme->EditItem( SchemeName );
  }

  //����� ����������� ������� ����� �� ������ ������� ����� �������� � � �������
  // � ���������� � ������� � ��������� ID'��
  else
  {
    TTagNode *ActiveSchemeNode = GetActiveSchemeNode();
    if ( ActiveSchemeNode )
    {
      SetActiveSchemeAttrVal( "ref" , "-1" );
      SetActiveSchemeAttrVal( "name", SchemeName       );

      DeleteActiveScheme( false );
      AddScheme( SchemeName, nVakId, ActiveSchemeNode );
      SetActiveSchemeAttrVal( "ref" , IntToStr(nVakId) );
    }
  }
}

//---------------------------------------------------------------------------

//�������� ������� ����� ( bModifyXML ����������, ���� �� ������� ��������������� ��� � xml'� )

void __fastcall TfmSchList::DeleteActiveScheme( bool bModifyXML )
{
  CheckActive(__FUNC__);
  bool bCanDelete = false;

  if ( bModifyXML )
  {
    TTagNode*  ActiveSchemeNode = GetActiveSchemeNode();
    if ( ActiveSchemeNode )
    {
      delete ActiveSchemeNode;
      bCanDelete  = true;
      XMLModified = true;
    }
  }
  else
    bCanDelete = true;

  if ( bCanDelete )
  {
    lbxScheme->DeleteSelected();
    if ( bModifyXML )
    {
      UpdateSchemeActions();
      UpdateSchemeStatusPanel();
    }
  }
}

//---------------------------------------------------------------------------

//���������� ������� ����� � ��������� ������ � ������ ��������� �������

bool __fastcall TfmSchList::SchemeNameExists( UnicodeString SchemeName, int nVakId )
{
  CheckActive(__FUNC__);
  bool Result = false;

  if ( nVakId == -1 )
    nVakId = GetCurVakId();

  TTagNode* CurNode = FIMM_S->GetChildByName("schemalist");
  if ( CurNode )
  {
    CurNode = CurNode->GetFirstChild();
    while ( CurNode )
    {
      int nCurNodeRef = StrToInt(CurNode->AV["ref"]);
      if ( nCurNodeRef == nVakId )
        if(CurNode->AV["name"] == SchemeName)
        {
          Result = true;
          break;
        }
      CurNode = CurNode->GetNext();
    }
  }
  return Result;
}

//---------------------------------------------------------------------------

//���������� ��������
// nAfterInsertInd - ������ �������� � ListView, ����� �������� ���������� ��������
// ����� �������, ��������������� ��������. ���� nAfterInsertInd == -2, �� ��������
// ���������� �������� ����� ���������� �����

bool __fastcall TfmSchList::AddPriviv( TTagNode *LineNode, int nAfterInsertInd )
{
  CheckActive(__FUNC__);
  bool bResult = false;
  if ( nAfterInsertInd == -2 )
    nAfterInsertInd = -1;
  TTagNode *ActiveSchemeNode = GetActiveSchemeNode();
  if ( ActiveSchemeNode )
  {
    //����������� ���� TagNode, ����� ��� ����� �������� ���������� �������� ����� ��������
    TTagNode *PrevLineNode;
    bool bInsertBefore;
    if ( nAfterInsertInd == -1 )
    {
      PrevLineNode = ActiveSchemeNode->GetChildByName("endline");
      bInsertBefore  = true;
    }
    else
    {
      PrevLineNode = (TTagNode*)lvPrivivList->Items->Item[nAfterInsertInd]->Data;
      bInsertBefore  = false;
    }

    //������� TagNode
    TTagNode *NewLineNode = ActiveSchemeNode->InsertChild( PrevLineNode, "line", bInsertBefore );
    NewLineNode->Assign( LineNode, true );

    //������� �������� ListView
    TListItem *NewListItem = lvPrivivList->Items->Insert( nAfterInsertInd + 1);
    AddLineInfo( NewLineNode, NewListItem );
    XMLModified = true;
    bResult = true;
//!!!    FocusListViewItem( lvPrivivList, NewListItem->Index );

    //���������� �������� � ������ �������
    UpdatePrivivActions();
    UpdatePrivivStatusPanel();
  }
  return bResult;
}

//---------------------------------------------------------------------------

//��������� ���������� ��������

bool __fastcall TfmSchList::EditPriviv( TTagNode *LineNode )
{
  CheckActive(__FUNC__);
  TTagNode *CurLineNode = GetActivePrivivNode();
  if ( CurLineNode )
  {
    CurLineNode->Assign(LineNode, true);
    EditLineInfo( CurLineNode, lvPrivivList->Items->Item[lvPrivivList->ItemIndex] );
    XMLModified = true;
    return true;
  }
  return false;
}

//---------------------------------------------------------------------------

//�������� ������� ��������

void __fastcall TfmSchList::DeleteActivePriviv()
{
  CheckActive(__FUNC__);
  if ( lvPrivivList->ItemIndex != -1 )
  {
    int NewItemIndex = lvPrivivList->ItemIndex - 1;
    TTagNode *LineNode = (TTagNode*)lvPrivivList->Items->Item[lvPrivivList->ItemIndex]->Data;
    delete LineNode;
    lvPrivivList->DeleteSelected();
    XMLModified = true;
//!!!    FocusListViewItem( lvPrivivList, NewItemIndex );
    UpdatePrivivActions();
    UpdatePrivivStatusPanel();
  }
}

//---------------------------------------------------------------------------

void __fastcall TfmSchList::UpdateFilterList()
{
  CheckActive(__FUNC__);
  FilterList->Clear();
  TTagNode *itSch = IMM_S->GetChildByName("conditionlist");
  if (itSch)
  {
    itSch = itSch->GetFirstChild();
    while (itSch)
    {
      FilterList->AddObject(itSch->AV["name"],(TObject*)UIDInt(itSch->AV["uid"]));
      itSch = itSch->GetNext();
    }
  }
  XMLModified = true;
}

//---------------------------------------------------------------------------

/*
 * ��������� ������ ������, ��������������� �� ������� (���������) ��������
 *
 *
 * ���������:
 *   [out] Strings - ������ ������
 *
 * ������������ ��������:
 *   -- ��� ������� (���������) �������
 *   -- -1 - ������� �� �������
 *
 */

int __fastcall TfmSchList::FillVacListForCurInf(TStrings *Strings)
{
  CheckActive(__FUNC__);
  if (!Strings)
    throw DKClasses::ENullArgument(__FUNC__, "Strings");

  int nCurVacId = -1;
  Strings->Clear();
  if ( InfekCBx->ItemIndex != -1 )
  {
    if (InfekCBx->Items->Strings[InfekCBx->ItemIndex].Length())
    {
      FSDM->fqVaks->ParamByName("PInfekCode")->AsInteger = (int)InfekCBx->Items->Objects[InfekCBx->ItemIndex];
      FillStrings(FSDM->fqVaks, Strings, FSDM->SchOwnerNameFN, TablePK, false);
    }
    else
      FillStringsFromFIBDataSet(FSDM->fdstVakchina, Strings, FSDM->SchOwnerNameFN, TablePK);
  }
  if (cbVak->ItemIndex != -1)
    nCurVacId = reinterpret_cast<int>(cbVak->Items->Objects[cbVak->ItemIndex]);
  return nCurVacId;
}

//---------------------------------------------------------------------------

/*
 * ��������� ������ ������, �� ������� ����� ���������� ������� (���������) �����
 *
 *
 * ���������:
 *   [out] Strings - ������ ������
 *
 * ������������ ��������:
 *   -- ��� ������� (���������) �������
 *   -- -1 - ������� �� �������
 *
 */

int __fastcall TfmSchList::FillVacListForCurScheme(TStrings *Strings)
{
  CheckActive(__FUNC__);
  if (!Strings)
    throw DKClasses::ENullArgument(__FUNC__, "Strings");

  int nCurVacId = -1;
  Strings->Clear();

  if (cbVak->ItemIndex != -1)
    nCurVacId = reinterpret_cast<int>(cbVak->Items->Objects[cbVak->ItemIndex]);

  TTagNode *SchemeNode = NULL;
  if ((nCurVacId != -1) && (lbxScheme->ItemIndex != -1))
    SchemeNode = dynamic_cast<TTagNode*>(lbxScheme->Items->Objects[lbxScheme->ItemIndex]);
  if (SchemeNode)
  {
    bool fJumpExists = false;
    TTagNode *LineNode = SchemeNode->GetFirstChild();
    while(LineNode && !fJumpExists)
    {
      fJumpExists = LineNode->GetFirstChild();
      LineNode = LineNode->GetNext();
    }
    if (fJumpExists)
    {
      TStringList *sl = NULL;
      try
      {
        sl = new TStringList;
        if (GetVakInfekList(sl, nCurVacId))
        {
          UnicodeString sExists;
          for (int i = 0; i < sl->Count; i++)
          {
            if (sExists.Length())
              sExists += " and ";
            sExists +=
              "exists ("
              "  select * "
              "  from " + FSDM->SchOwnerInfekTN + " y "
              "  where y." + FSDM->SchOwnerInfekInfekFN + " = " + IntToStr((int)sl->Objects[i]) + " and "
              "        x." + FSDM->SchOwnerInfekSchOwnerFN + " = y." + FSDM->SchOwnerInfekSchOwnerFN +
              ")";
          }
          FSDM->Query->SQL->Text =
            "select distinct x." + FSDM->SchOwnerInfekSchOwnerFN + ", z." + FSDM->SchOwnerNameFN + " "
            "from " + FSDM->SchOwnerInfekTN + " x inner join " + FSDM->SchOwnerTN + " z "
            "on x." + FSDM->SchOwnerInfekSchOwnerFN + " = z.CODE "
            "where " + sExists;
          FillStrings(FSDM->Query, Strings, FSDM->SchOwnerNameFN, FSDM->SchOwnerInfekSchOwnerFN, false);
        }
      }
      __finally
      {
        if (sl) delete sl;
      }
    }
    else
      FillVacListForCurInf(Strings);
  }
  return nCurVacId;
}

//---------------------------------------------------------------------------

//���������� TagNode, ��������������� ������� ��������

TTagNode* __fastcall TfmSchList::GetActivePrivivNode()
{
  CheckActive(__FUNC__);
  TTagNode *Result = NULL;
  if ( lvPrivivList->ItemIndex >= 0 )
    Result = (TTagNode*)lvPrivivList->Items->Item[ lvPrivivList->ItemIndex ]->Data;
  return Result;
}

//---------------------------------------------------------------------------

//��������� �������� �������� TagNode, ���������������� ������� �����

UnicodeString __fastcall TfmSchList::GetActiveSchemeAttrVal( UnicodeString AttrName )
{
  CheckActive(__FUNC__);
  UnicodeString Result = "";
  TTagNode *ActiveSchemeNode = GetActiveSchemeNode();
  if ( ActiveSchemeNode )
    Result = ActiveSchemeNode->AV[AttrName];
  return Result;
}

//---------------------------------------------------------------------------

//��������� �������� �������� TagNode, ���������������� ������� �����

void __fastcall TfmSchList::SetActiveSchemeAttrVal( UnicodeString AttrName, UnicodeString AttrVal )
{
  CheckActive(__FUNC__);
  TTagNode *ActiveSchemeNode = GetActiveSchemeNode();
  if ( ActiveSchemeNode )
  {
    ActiveSchemeNode->AV[AttrName] = AttrVal;
    XMLModified = true;
  }
}

//---------------------------------------------------------------------------

//��������� �������� �������� TagNode, ���������������� ������� ��������

UnicodeString __fastcall TfmSchList::GetActivePrivivAttrVal( UnicodeString AttrName )
{
  CheckActive(__FUNC__);
  UnicodeString Result = "";
  if ( lvPrivivList->ItemIndex != -1 )
  {
    TTagNode *LineNode = (TTagNode*)lvPrivivList->Items->Item[ lvPrivivList->ItemIndex ]->Data;
    Result = LineNode->AV[AttrName];
  }
  return Result;
}

//---------------------------------------------------------------------------

//��������� �������� �������� TagNode, ���������������� ������� ��������

void __fastcall TfmSchList::SetActivePrivivAttrVal( UnicodeString AttrName, UnicodeString AttrVal )
{
  CheckActive(__FUNC__);
  if ( lvPrivivList->ItemIndex != -1 )
  {
    TTagNode *LineNode = (TTagNode*)lvPrivivList->Items->Item[ lvPrivivList->ItemIndex ]->Data;
    LineNode->AV[AttrName] = AttrVal;
  }
}

//---------------------------------------------------------------------------

//��������� ������ ���� ��� ��������� ��������

bool __fastcall TfmSchList::GetInfekSchList( TStrings *strings, int nInfekId )
{
  CheckActive(__FUNC__);
  if ( !strings ) return false;

  FSDM->fqVaks->ParamByName("PInfekCode")->AsInteger = nInfekId;
  ExecFIBQuery(FSDM->fqVaks);
  TIntSet VakIds;
  while( !FSDM->fqVaks->Eof )
  {
    VakIds.insert( FSDM->fqVaks->FN(TablePK)->AsInteger );
    FSDM->fqVaks->Next();
  }
  CloseFIBQuery(FSDM->fqVaks);

  return GetSchList( strings, VakIds, TGetSchListOptions() << slVakNames << slSchFullUIDs );
}

//---------------------------------------------------------------------------

//��������� ������ �������� ��� ��������� �������

bool __fastcall TfmSchList::GetVakInfekList( TStrings *strings, int nVakId )
{
  CheckActive(__FUNC__);
  if ( !strings ) return false;

  strings->Clear();

  FSDM->fqInfeks->ParamByName("PVakCode")->AsInteger = nVakId;
  ExecFIBQuery(FSDM->fqInfeks);
  while( !FSDM->fqInfeks->Eof )
  {
    strings->AddObject( FSDM->fqInfeks->FN(InfekchiyaName)->AsString,
                        (TObject*)FSDM->fqInfeks->FN(TablePK)->AsInteger );
    FSDM->fqInfeks->Next();
  }
  CloseFIBQuery(FSDM->fqInfeks);

  return (bool)strings->Count;
}

//---------------------------------------------------------------------------

//��������� ������ �������� ��� ������� �������

bool __fastcall TfmSchList::GetCurVakInfekList( TStrings *strings )
{
  CheckActive(__FUNC__);
  if ( cbVak->ItemIndex < 0 ) return false;

  return GetVakInfekList( strings, (int)cbVak->Items->Objects[cbVak->ItemIndex] );
}

//---------------------------------------------------------------------------

//��������� ������ ������ ��� ��������� ��������

bool __fastcall TfmSchList::GetInfekVakList( TStrings *strings, int nInfekId )
{
  CheckActive(__FUNC__);
  if ( !strings ) return false;

  strings->Clear();

  FSDM->fqVaks->ParamByName("PInfekCode")->AsInteger = nInfekId;
  ExecFIBQuery(FSDM->fqVaks);
  while( !FSDM->fqVaks->Eof )
  {
    strings->AddObject( FSDM->fqVaks->FN(FSDM->SchOwnerNameFN)->AsString,
                        (TObject*)FSDM->fqVaks->FN(TablePK)->AsInteger );
    FSDM->fqVaks->Next();
  }
  CloseFIBQuery(FSDM->fqVaks);
  return (bool)strings->Count;
}

//---------------------------------------------------------------------------

//��������� Max ItemIndex ��� ���������� ���� �������� ( "R" ��� "V" )

int __fastcall TfmSchList::GetMaxPrivivItemIndex ( char PrivivType )
{
  CheckActive(__FUNC__);
  int nResult = -1;
  for ( int i = 0; i < lvPrivivList->Items->Count; i++ )
    if ( lvPrivivList->Items->Item[i]->Caption[1] == PrivivType )
      if ( i > nResult )
        nResult = i;
  return nResult;
}

//---------------------------------------------------------------------------

//����������, �������� �� ������� ����� ���������

bool __fastcall TfmSchList::IfCurSchemeIsStart()
{
  CheckActive(__FUNC__);
  bool Result = false;
  TTagNode *SchemeNode = GetActiveSchemeNode();
  if ( SchemeNode )
    Result = IsStartScheme(SchemeNode->AV["uid"]);
  return Result;
}

//---------------------------------------------------------------------------

//����������, �������� �� ����� � ��������� UID'�� ���������

bool __fastcall TfmSchList::IsStartScheme( UnicodeString UID )
{
  CheckActive(__FUNC__);
  bool Result = false;
  if ( FSDM->fdstInfekchiya->Active )
    Result = FSDM->fdstInfekchiya->Locate( FSDM->StartSchFN, UID, FULL_SRCH_OPTS );
  return Result;
}

//---------------------------------------------------------------------------

//��������� ������ ���� ��� ��������� ������

bool __fastcall TfmSchList::GetSchList( TStrings *strings, const TIntSet VakIds, TGetSchListOptions GetOptions )
{
  CheckActive(__FUNC__);
  if (!strings)
    return false;

  strings->Clear();

  TTagNode* CurNode = FIMM_S->GetChildByName("schemalist")->GetFirstChild();
  int nCurNodeRef;
  UnicodeString CurSchName;
  UnicodeString *CurFullUID;
  while ( CurNode )
  {
    nCurNodeRef = StrToInt(CurNode->AV["ref"]);

    if ( VakIds.find( nCurNodeRef ) != VakIds.end() )
    {
      CurSchName = CurNode->AV["name"];
      if ( GetOptions.Contains(slVakNames) )
        CurSchName = CurSchName + " (" + GetVakName(nCurNodeRef) + ")";

      CurFullUID = NULL;
      if ( GetOptions.Contains(slSchFullUIDs) )
      {
        CurFullUID = new UnicodeString;
        (*CurFullUID) = CurNode->AV["uid"] + "." + CurNode->GetFirstChild()->AV["uid"];
      }
      strings->AddObject( CurSchName, (TObject*)CurFullUID );
    }
    CurNode = CurNode->GetNext();
  }
  return (bool)strings->Count;

}

//---------------------------------------------------------------------------

//��������� ����� ����� �� � UID'�

UnicodeString __fastcall TfmSchList::GetSchemeNameByUID( UnicodeString UID, bool bVakName )
{
  CheckActive(__FUNC__);
  UnicodeString Result = "";
  TTagNode *SchemNode = FIMM_S->GetTagByUID(UID);
  if( SchemNode )
  {
    Result = SchemNode->AV["name"];
    if (bVakName)
      Result += " (" + GetVakName(StrToInt(SchemNode->AV["ref"])) + ")";
  }
  return Result;
}

//---------------------------------------------------------------------------

//��������� ������������ �������� � ���� <��� �������> (<��� �����>.<��� ��������>)

UnicodeString __fastcall TfmSchList::GetPrivivFullName(
  TTagNode *AIMM_S,
  UnicodeString AVacName,
  UnicodeString APrivivUID
)
{
  if (!AIMM_S)
    throw DKClasses::ENullArgument(
      __FUNC__,
      "AIMM_S"
    );
  if (!AIMM_S->CmpName("imm-s"))
    throw DKClasses::EMethodError(
      __FUNC__,
      "��� xml-��������� �� ������� ������ ���� imm-s."
    );
  UnicodeString sPrivivFullName;
  if (APrivivUID == EmptyRef)
    sPrivivFullName = csEndPlanLabel;
  else
  {
    TTagNode *LineNode = AIMM_S->GetTagByUID(APrivivUID);
    if (LineNode)
    {
      UnicodeString sSchName    = LineNode->GetParent()->AV["name"];
      UnicodeString sPrivivType = (LineNode->GetAttrByName("name"))
                               ? LineNode->AV["name"]
                               : csEndLineLabel.LowerCase();
	  sPrivivFullName = Format(UnicodeString(
		"%s (%s.%s)"),
        OPENARRAY(TVarRec, (AVacName, sSchName, sPrivivType))
	  );
    }
  }
  return sPrivivFullName;
}

//---------------------------------------------------------------------------

//�������� ������������ �������� � ���� <��� �������> (<��� �����>.<��� ��������>)

UnicodeString __fastcall TfmSchList::GetPrivivFullName(UnicodeString APrivivUID)
{
  CheckActive(__FUNC__);

  UnicodeString sVacName;
  TTagNode *LineNode = FIMM_S->GetTagByUID(APrivivUID);
  if (LineNode)
    sVacName = GetVakName(LineNode->GetParent()->AV["ref"].ToInt());

  return GetPrivivFullName(
    FIMM_S,
    sVacName,
    APrivivUID
  );
}

//---------------------------------------------------------------------------

//��������� ������������ ������� �� ��� ����

UnicodeString __fastcall TfmSchList::GetFilterNameByID( int nId )
{
  CheckActive(__FUNC__);
  UnicodeString Result = "����������� ������";
  int idx = FFilterList->IndexOfObject((TObject*)nId);
  if (idx != -1)
    Result = FFilterList->Strings[idx];
  return Result;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TfmSchList::GetCurVakName()
{
  CheckActive(__FUNC__);
  return cbVak->Text;
}

//---------------------------------------------------------------------------
UnicodeString __fastcall TfmSchList::GetVakName( int nVakId )
{
  CheckActive(__FUNC__);
  return FSDM->GetVakName(nVakId);
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TfmSchList::GetInfekName( int nInfekId )
{
  CheckActive(__FUNC__);
  return FSDM->GetInfekName(nInfekId);
}
//---------------------------------------------------------------------------
int __fastcall TfmSchList::GetCurVakId()
{
  CheckActive(__FUNC__);
  if ( cbVak->ItemIndex >= 0 )
    return (int)cbVak->Items->Objects[cbVak->ItemIndex];
  else
    return -1;
}
//---------------------------------------------------------------------------
int __fastcall TfmSchList::GetCurSchemePrivivCount()
{
  return lvPrivivList->Items->Count;
}
//---------------------------------------------------------------------------
int __fastcall TfmSchList::GetCurPrivivInd()
{
  return lvPrivivList->ItemIndex;
}
//---------------------------------------------------------------------------
TListItem* __fastcall TfmSchList::GetCurPrivivItem()
{
  return lvPrivivList->Items->Item[GetCurPrivivInd()];
}
//---------------------------------------------------------------------------
void __fastcall TfmSchList::lvPrivivListKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  ExecuteGridAction( Key,
                     Shift,
                     actAddPriviv,
                     actEditPriviv,
                     actDeletePriviv,
                     0,
                     TShiftState(),
                     NULL );
}

//---------------------------------------------------------------------------
void __fastcall TfmSchList::lvPrivivListDblClick(TObject *Sender)
{
  POINT MousePos;
  GetCursorPos(&MousePos);
  TPoint p = lvPrivivList->ScreenToClient( TPoint( MousePos.x, MousePos.y ) );
  lvPrivivListMouseDown( NULL, mbLeft, TShiftState() , p.x, p.y );

  ExecuteGridAction( VK_RETURN,
                     TShiftState() << ssShift,
                     actAddPriviv,
                     actEditPriviv,
                     actDeletePriviv,
                     0,
                     TShiftState(),
                     NULL );
}

//---------------------------------------------------------------------------
void __fastcall TfmSchList::lvPrivivListMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  TListItem *item = lvPrivivList->GetItemAt( 0, Y );
//!!!  if ( item )
//!!!    FocusListViewItem( lvPrivivList, item->Index );
//!!!  else if ( lvPrivivList->Items->Count )
//!!!    FocusListViewItem( lvPrivivList, lvPrivivList->Items->Count - 1 );
}

//---------------------------------------------------------------------------
void __fastcall TfmSchList::actCloseExecute(TObject *Sender)
{
  Close();
}

//---------------------------------------------------------------------------
void __fastcall TfmSchList::lbxSchemeDrawListItemImage(TObject *Sender,
      int ListItemIndex, int &ItemImageIndex)
{
  TTagNode *SchemeNode = (TTagNode*)((TCustomListBox*)Sender)->Items->Objects[ListItemIndex];

  try
  {
    //��������� �������� ��� ��������� ������� ����� �� ��������� ������ � ������ ����
    if (IsStartScheme(SchemeNode->AV["uid"]))
      ItemImageIndex = 0;
  }
  catch (EFIBInterBaseError &E) {;}
}

//---------------------------------------------------------------------------

//��������� �������� lvPrivivList

//###########################################################################
//##                                                                       ##
//##                          TIMMSchemeEditor                             ##
//##                                                                       ##
//###########################################################################

// ASchemeObjType - ��� �������� ������� ��� �����

__fastcall TIMMSchemeEditor::TIMMSchemeEditor(
  TComponent *Owner,
  TSchemeObjType ASchemeObjType
) : TObject()
{
  FSchListFM = new TfmSchList(Owner, ASchemeObjType);
}

//---------------------------------------------------------------------------

__fastcall TIMMSchemeEditor::~TIMMSchemeEditor()
{
  delete FSchListFM;
}

//---------------------------------------------------------------------------

TSchemeObjType __fastcall TIMMSchemeEditor::GetSchemeObjType()
{
  return FSchListFM->SchemeObjType;
}

//---------------------------------------------------------------------------

TpFIBDatabase* __fastcall TIMMSchemeEditor::GetDB()
{
  return FSchListFM->DB;
}

//---------------------------------------------------------------------------

void __fastcall TIMMSchemeEditor::SetDB(TpFIBDatabase* AValue)
{
  FSchListFM->DB = AValue;
}

//---------------------------------------------------------------------------

TAxeXMLContainer* __fastcall TIMMSchemeEditor::GetXMLContainer()
{
  return FSchListFM->XMLContainer;
}

//---------------------------------------------------------------------------

void __fastcall TIMMSchemeEditor::SetXMLContainer(TAxeXMLContainer *AValue)
{
  FSchListFM->XMLContainer = AValue;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TIMMSchemeEditor::GetNmklGUI()
{
  return FSchListFM->NmklGUI;
}

//---------------------------------------------------------------------------

void __fastcall TIMMSchemeEditor::SetNmklGUI(UnicodeString AValue)
{
  FSchListFM->NmklGUI = AValue;
}

//---------------------------------------------------------------------------

TStrings* __fastcall TIMMSchemeEditor::GetFilterList()
{
  return FSchListFM->FilterList;
}

//---------------------------------------------------------------------------

TTagNode* __fastcall TIMMSchemeEditor::GetIMM_S()
{
  return FSchListFM->IMM_S;
}

//---------------------------------------------------------------------------

void __fastcall TIMMSchemeEditor::SetIMM_S(TTagNode* AValue)
{
  FSchListFM->IMM_S = AValue;
}

//---------------------------------------------------------------------------

bool __fastcall TIMMSchemeEditor::GetIMM_SModified()
{
  return FSchListFM->IMM_SModified;
}

//---------------------------------------------------------------------------

bool __fastcall TIMMSchemeEditor::GetValidateOnSave()
{
  return FSchListFM->ValidateOnSave;
}

//---------------------------------------------------------------------------

void __fastcall TIMMSchemeEditor::SetValidateOnSave(bool AValue)
{
  FSchListFM->ValidateOnSave = AValue;
}

//---------------------------------------------------------------------------

TOnLoadXML __fastcall TIMMSchemeEditor::GetOnLoadXML()
{
  return FSchListFM->OnLoadXML;
}

//---------------------------------------------------------------------------

void __fastcall TIMMSchemeEditor::SetOnLoadXML(TOnLoadXML AValue)
{
  FSchListFM->OnLoadXML = AValue;
}

//---------------------------------------------------------------------------

TOnSaveXML __fastcall TIMMSchemeEditor::GetOnSaveXML()
{
  return FSchListFM->OnSaveXML;
}

//---------------------------------------------------------------------------

void __fastcall TIMMSchemeEditor::SetOnSaveXML(TOnSaveXML AValue)
{
  FSchListFM->OnSaveXML = AValue;
}

//---------------------------------------------------------------------------

/*
 * ����������� ���������� ���������
 *
 *
 * ������������ ��������:
 *   ModalResult ����� TfmSchList
 *
 */

int __fastcall TIMMSchemeEditor::ShowEditor()
{
  return FSchListFM->ShowModal();
}

//---------------------------------------------------------------------------

// ���������� ����������� ������ ��������������

bool __fastcall TIMMSchemeEditor::CanEdit()
{
  return FSchListFM->CanEdit();
}

//---------------------------------------------------------------------------

/*
 * ��������� xml-��������� �� �������:
 *   -- ������� ���� ��� ������ �������
 *   -- ���������� ���������
 *
 * ���������:
 *   [in]     ADB            - ������� ���� ������
 *   [in]     ASchemeObjType - ��� �������� ������� ��� �����
 *   [in\out] AXML           - xml-��������
 *
 * ������������ ��������:
 *   ������� ����������� xml-���������
 *
 */

bool __fastcall TIMMSchemeEditor::ValidateXML(
  TpFIBDatabase *ADB,
  TSchemeObjType ASchemeObjType,
  TTagNode *AXML
)
{
  return TfmSchList::ValidateXML(ADB, ASchemeObjType, AXML);
}

//###########################################################################
//##                                                                       ##
//##                       TUpdateSchemesMapCreator                        ##
//##                                                                       ##
//###########################################################################

bool TUpdateSchemesMapCreator::SchemeInfoListLessVacType::operator() (
  TSchemeInfo x,
  TSchemeInfo y
)
{
  if      (x.sVacType == csReqFldNULL)
    return true;
  else if (y.sVacType == csReqFldNULL)
    return false;
  else
    return x.sVacType < y.sVacType;
};

//---------------------------------------------------------------------------

void TUpdateSchemesMapCreator::TSchemeInfo::Clear()
{
  sSchemeUID = csReqFldNULL;
  sLineUID   = csReqFldNULL;
  sVacType   = csReqFldNULL;
}

//---------------------------------------------------------------------------

void TUpdateSchemesMapCreator::TSchemeCpblt::Clear()
{
  sVacType = "";
  PSchemeInfoList.clear();
  NewSchemeInfo.Clear();
}

//---------------------------------------------------------------------------

__fastcall TUpdateSchemesMapCreator::TUpdateSchemesMapCreator() :
  TObject()
{
  FContingent   = NULL;
  FInfecList    = NULL;
  FDB           = NULL;
  FDM           = NULL;
  FVacSchXML    = NULL;
  FCreationMode = false;
  FTerminated   = false;
  FSchInfoList  = NULL; 
  FSchCpbltList = NULL;
  FOnCreationBegin    = NULL;
  FOnProgressChange   = NULL;
  FOnCreationComplete = NULL;
}

//---------------------------------------------------------------------------

__fastcall TUpdateSchemesMapCreator::~TUpdateSchemesMapCreator()
{
  if (FDM) delete FDM;
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::SetContingent(TStringList* AValue)
{
  CheckCreationMode(__FUNC__, false);
  FContingent = AValue;
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::SetInfecList(TInfecList* AValue)
{
  CheckCreationMode(__FUNC__, false);
  FInfecList = AValue;
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::SetNewSchemeUID(UnicodeString AValue)
{
  CheckCreationMode(__FUNC__, false);
  FNewSchemeUID = AValue;
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::SetDB(TpFIBDatabase* AValue)
{
  CheckCreationMode(__FUNC__, false);
  if (FDB != AValue)
  {
    if (FDM) delete FDM;
    FDB = AValue;
    if (FDB)
      FDM = new TUpdSchMapDM(NULL, FDB);
    else
      FDM = NULL;
  }
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::SetVacSchXML(TTagNode* AValue)
{
  CheckCreationMode(__FUNC__, false);
  FVacSchXML = AValue;
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::SetOptions(TOptions AValue)
{
  CheckCreationMode(__FUNC__, false);
  FOptions = AValue;
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::ChangeStage(TCreationStage AStage, int AMaxProgress)
{
  if (FOnCreationBegin)
    FOnCreationBegin(this, AStage, AMaxProgress);
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::IncProgress(int ADelta)
{
  if (FOnProgressChange)
    FOnProgressChange(this, ADelta);
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::CheckInputs(UnicodeString AMethodName)
{
  if (!FContingent)
    throw DKClasses::EMethodError(
      AMethodName,
      "�� ����������� �������� Contingent."
    );
  if (!FInfecList)
    throw DKClasses::EMethodError(
      AMethodName,
      "�� ����������� �������� InfecList."
    );
  if (!FNewSchemeUID.Length())
    throw DKClasses::EMethodError(
      AMethodName,
      "�� ����������� �������� NewSchemeUID."
    );
  if (!FDB || !FDM)
    throw DKClasses::EMethodError(
      AMethodName,
      "�� ����������� �������� DB."
    );
  if (!FVacSchXML)
    throw DKClasses::EMethodError(
      AMethodName,
      "�� ����������� �������� VacSchXML."
    );
  if (!FVacSchXML->CmpName("imm-s"))
    throw DKClasses::EMethodError(
      AMethodName,
      "��� xml-��������� �� ������� ���������� ������ ���� imm-s."
    );
  if (!FVacSchXML->GetTagByUID(FNewSchemeUID))
    throw DKClasses::EMethodError(
      AMethodName,
      "����� ���������� �� ��������� UID \"" + FNewSchemeUID + "\" �� �������."
    );
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::CheckCreationMode(UnicodeString AMethodName, bool AReqModeValue)
{
  if (FCreationMode != AReqModeValue)
    if (FCreationMode)
      throw DKClasses::EMethodError(
        AMethodName,
        "������ ��������� � ������ ������������ �������."
      );
    else
      throw DKClasses::EMethodError(
        AMethodName,
        "������ �� ��������� � ������ ������������ �������."
      );
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::CheckBuffers(UnicodeString AMethodName)
{
  if (!FSchInfoList)
    throw DKClasses::EMethodError(AMethodName, "�� ������ ������ FSchInfoList");
  if (!FSchCpbltList)
    throw DKClasses::EMethodError(AMethodName, "�� ������ ������ FSchCpbltList");
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::DoInitStage(int *pASchDataSize)
{
  *pASchDataSize = 0;

  ChangeStage(csInit, 1);

  // ���������� � ������� ������ �� ������� "�����"
  UnicodeString sContingent, sInfecList;
  for (int i = 0; i < FContingent->Count; i++)
  {
    if (sContingent.Length())
      sContingent += ", ";
    sContingent += IntToStr(reinterpret_cast<int>(FContingent->Objects[i]));
  }
  for (TInfecList::iterator i = FInfecList->begin(); i != FInfecList->end(); i++)
  {
    if (sInfecList.Length())
      sInfecList += ", ";
    sInfecList += IntToStr(*i);
  }
  FDM->SetGetSchDataSQL(sContingent, sInfecList);

  // ����������� ���������� �������������� ������� �� ������� "�����"
  try
  {
    START_FIBQUERY(FDM->GetSchDataSizeQ);
    *pASchDataSize = FDM->GetSchDataSizeQ->FN("VacSchemeCount")->AsInteger;
  }
  __finally
  {
    STOP_FIBQUERY(FDM->GetSchDataSizeQ);
  }
  IncProgress();
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::DoGetDataStage(const int ASchDataSize)
{
  CheckBuffers(__FUNC__);
  FSchInfoList->clear();
  FSchCpbltList->clear();

  ChangeStage(csGetData, 2*ASchDataSize);

  // ������� ������ �� ������� "�����" � SchInfoList
  try
  {
    START_FIBQUERY(FDM->GetSchDataQ);
    while(!FDM->GetSchDataQ->Eof)
    {
      TSchemeInfo SchInfo;
      if (FDM->GetSchDataQ->FN("VacScheme")->AsString != csReqFldNULL)
      {
        SchInfo.sSchemeUID = _GUI(FDM->GetSchDataQ->FN("VacScheme")->AsString);
        SchInfo.sLineUID   = _UID(FDM->GetSchDataQ->FN("VacScheme")->AsString);
        TTagNode *LineNode = FVacSchXML->GetTagByUID(SchInfo.sLineUID);
        SchInfo.sVacType = csEndLineLabel;
        if (LineNode)
         if (LineNode->CmpName("line"))
          SchInfo.sVacType = LineNode->AV["name"];
      }
      FSchInfoList->push_back(SchInfo);
      FDM->GetSchDataQ->Next();
      IncProgress();
      if (FTerminated)
        break;
    }
  }
  __finally
  {
    STOP_FIBQUERY(FDM->GetSchDataQ);
  }

  if (!FTerminated && FSchInfoList->size())
  {
    // ����������� ������ SchInfoList �� ���� ����������
    FSchInfoList->sort(SchemeInfoListLessVacType());

    UnicodeString sPrevVacType;
    TSchemeCpblt SchCpblt;
    for (TSchemeInfoList::iterator i = FSchInfoList->begin(); i != FSchInfoList->end(); i++)
    {
      if (!sPrevVacType.Length())
        SchCpblt.sVacType = (*i).sVacType;
      if (sPrevVacType.Length() && (sPrevVacType != (*i).sVacType))
      {
        FSchCpbltList->push_back(SchCpblt);
        SchCpblt.Clear();
        SchCpblt.sVacType = (*i).sVacType;
      }
      SchCpblt.PSchemeInfoList.push_back(&(*i));
      sPrevVacType = (*i).sVacType;
      IncProgress();
      if (FTerminated)
        break;
    }
    FSchCpbltList->push_back(SchCpblt);
  }
}

//---------------------------------------------------------------------------

bool __fastcall TUpdateSchemesMapCreator::DoSetCompatibilityStage()
{
  CheckBuffers(__FUNC__);

  ChangeStage(csSetCompatibility, FSchCpbltList->size());

  // ����������� ������������ ����� ������ ���� � ����� ����� ����
  for (TSchemeCpbltList::iterator i = FSchCpbltList->begin(); i != FSchCpbltList->end(); i++)
  {
    TTagNode *NewSchemeLine;
    if      ((*i).sVacType == csReqFldNULL)
      NewSchemeLine = FVacSchXML->GetTagByUID(FNewSchemeUID)->GetFirstChild();
    else if ((*i).sVacType == csEndLineLabel)
      NewSchemeLine = FVacSchXML->GetTagByUID(FNewSchemeUID)->GetChildByName("endline");
    else
      NewSchemeLine = FVacSchXML->GetTagByUID(FNewSchemeUID)->GetChildByAV(
        "line",
        "name",
        (*i).sVacType
      );
    if (NewSchemeLine)
    {
      (*i).NewSchemeInfo.sSchemeUID = FNewSchemeUID;
      (*i).NewSchemeInfo.sLineUID   = NewSchemeLine->AV["uid"];
      if (NewSchemeLine->CmpName("line"))
        (*i).NewSchemeInfo.sVacType = NewSchemeLine->AV["name"];
      else
        (*i).NewSchemeInfo.sVacType = csEndLineLabel;
    }
    IncProgress();
    if (FTerminated)
      break;
  }
  
  bool fRes = false;
  if (!FTerminated)
  {
    // ��������� ������������ ����� ������ ���� � ����� ����� ���� ��� ��� ��
    // ���, ��� ������� �� ���������� ����� ������������ ���������� �� �������
    TSchemeCompatibilityEditor *SchCpbltEditor = NULL;
    try
    {
      SchCpbltEditor = new TSchemeCompatibilityEditor;
      SchCpbltEditor->NewSchemeUID = FNewSchemeUID;
      SchCpbltEditor->DB           = FDB;
      SchCpbltEditor->VacSchXML    = FVacSchXML;
      SchCpbltEditor->EditAll      = FOptions.Contains(oSchCpbltEditAll);
      SchCpbltEditor->SetSchCpbltList(FSchCpbltList);
      fRes = SchCpbltEditor->Edit();
    }
    __finally
    {
      if (SchCpbltEditor) delete SchCpbltEditor;
    }
  }
  return fRes;
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::DoCreateMapStage(TUpdateSchemesMap *pAUpdateSchemesMap)
{
  CheckBuffers(__FUNC__);

  ChangeStage(csCreateMap, FSchCpbltList->size());

  // ���������� �������
  for (TSchemeCpbltList::iterator i = FSchCpbltList->begin(); i != FSchCpbltList->end(); i++)
  {
    UnicodeString sNewSchLineID;
    if ((*i).NewSchemeInfo.sSchemeUID == csReqFldNULL)
      sNewSchLineID = csReqFldNULL;
    else
      sNewSchLineID = (*i).NewSchemeInfo.sSchemeUID + "." + (*i).NewSchemeInfo.sLineUID;
    for (TPSchemeInfoList::iterator j = (*i).PSchemeInfoList.begin(); j != (*i).PSchemeInfoList.end(); j++)
    {
      UnicodeString sOldSchLineID;
      if ((*j)->sSchemeUID == csReqFldNULL)
        sOldSchLineID = csReqFldNULL;
      else
        sOldSchLineID = (*j)->sSchemeUID + "." + (*j)->sLineUID;
      if (sOldSchLineID != sNewSchLineID)
        (*pAUpdateSchemesMap)[sNewSchLineID].push_back(sOldSchLineID);
    }
    IncProgress();
    if (FTerminated)
      break;
  }
}

//---------------------------------------------------------------------------

TUpdateSchemesMapCreator::TCreationResult __fastcall TUpdateSchemesMapCreator::DoCreateUpdateSchemesMap(TUpdateSchemesMap *pAUpdateSchemesMap)
{
  TCreationResult nResult = csOk;

  int nSchDataSize;
  DoInitStage(&nSchDataSize);

  if (!FTerminated)
  {
    try
    {
      FSchInfoList  = new TSchemeInfoList;
      FSchCpbltList = new TSchemeCpbltList;
      DoGetDataStage(nSchDataSize);
      if (!FTerminated && FSchCpbltList->size())
      {
        bool fStageRes = DoSetCompatibilityStage();
        if (!FTerminated)
        {
          if (fStageRes)
            DoCreateMapStage(pAUpdateSchemesMap);
          else
            nResult = csCanceled;
        }
      }
    }
    __finally
    {
      if (FSchInfoList)
      {
        delete FSchInfoList;
        FSchInfoList = NULL;
      }
      if (FSchCpbltList)
      {
        delete FSchCpbltList;
        FSchCpbltList = NULL;
      }
    }
  }

  if (FOnCreationComplete)
    FOnCreationComplete(this);

  if (FTerminated && (nResult != csCanceled))
    return csTerminated;
  else
    return nResult;
}

//---------------------------------------------------------------------------

/*
 * ���������� ������� ��� ���������� ������� ���� ���������� ��������� ���������
 * ��� ��������� ��������
 *
 *
 * ���������:
 *   [out] pAUpdateSchemesMap - ������� ID ����� � ������ ����� ����
 *                              (��. �������� TUpdateSchemesMap)
 *
 * ������������ ��������:
 *   ��. �������� TCreationResult
 *
 */

TUpdateSchemesMapCreator::TCreationResult __fastcall TUpdateSchemesMapCreator::CreateUpdateSchemesMap(TUpdateSchemesMap *pAUpdateSchemesMap)
{
  CheckCreationMode(__FUNC__, false);
  CheckInputs(__FUNC__);
  if (!pAUpdateSchemesMap)
    throw DKClasses::ENullArgument(__FUNC__, "AUpdateSchemesMap");

  TCreationResult nResult;
  try
  {
    FCreationMode = true;
    pAUpdateSchemesMap->clear();
    nResult = DoCreateUpdateSchemesMap(pAUpdateSchemesMap);
  }
  __finally
  {
    FCreationMode = false;
    FTerminated   = false;
  }
  return nResult;
}

//---------------------------------------------------------------------------

void __fastcall TUpdateSchemesMapCreator::Terminate()
{
  FTerminated = true;
}

//###########################################################################
//##                                                                       ##
//##                       TSchemeCompatibilityEditor                      ##
//##                                                                       ##
//###########################################################################

UnicodeString  TSchemeCompatibilityEditor::SelectSchCpbltNewSchVacType::operator()(
  const TUpdateSchemesMapCreator::TSchemeCpblt &x
) const
{
  return x.NewSchemeInfo.sVacType;
}

//---------------------------------------------------------------------------

__fastcall TSchemeCompatibilityEditor::TSchemeCompatibilityEditor() : TObject()
{
  FDB              = NULL;
  FDM              = NULL;
  FVacSchXML       = NULL;
  FSchCpbltList    = NULL;
  FSchCpbltListBuf = NULL;  
  FSchCpbltArr     = NULL;
  FEditAll         = false;
  FEditMode        = false;
}

//---------------------------------------------------------------------------

__fastcall TSchemeCompatibilityEditor::~TSchemeCompatibilityEditor()
{
  if (FDM) delete FDM;
  if (FSchCpbltListBuf) delete FSchCpbltListBuf;
  if (FSchCpbltArr) delete FSchCpbltArr;
}

//---------------------------------------------------------------------------

void __fastcall TSchemeCompatibilityEditor::SetNewSchemeUID(UnicodeString AValue)
{
  CheckEditMode(__FUNC__, false);
  FNewSchemeUID = AValue;
}

//---------------------------------------------------------------------------

void __fastcall TSchemeCompatibilityEditor::SetDB(TpFIBDatabase* AValue)
{
  CheckEditMode(__FUNC__, false);
  if (FDB != AValue)
  {
    if (FDM) delete FDM;
    FDB = AValue;
    if (FDB)
      FDM = new TSDM(NULL, FDB, sotVaccine);
    else
      FDM = NULL;
  }
}

//---------------------------------------------------------------------------

void __fastcall TSchemeCompatibilityEditor::SetVacSchXML(TTagNode* AValue)
{
  CheckEditMode(__FUNC__, false);
  FVacSchXML = AValue;
}

//---------------------------------------------------------------------------

void __fastcall TSchemeCompatibilityEditor::SetEditAll(bool AValue)
{
  CheckEditMode(__FUNC__, false);
  FEditAll = AValue;
}

//---------------------------------------------------------------------------

int __fastcall TSchemeCompatibilityEditor::GetItemCount()
{
  return (FSchCpbltArr) ? FSchCpbltArr->size() : 0;
}

//---------------------------------------------------------------------------

UnicodeString __fastcall TSchemeCompatibilityEditor::GetItemVacType(int AIndex)
{
  CheckArr(__FUNC__);
  return (*FSchCpbltArr)[AIndex]->sVacType;
}

//---------------------------------------------------------------------------

TUpdateSchemesMapCreator::TSchemeInfo __fastcall TSchemeCompatibilityEditor::GetItemNewSchemeInfo(int AIndex)
{
  CheckArr(__FUNC__);
  return (*FSchCpbltArr)[AIndex]->NewSchemeInfo;
}

//---------------------------------------------------------------------------

void __fastcall TSchemeCompatibilityEditor::SetItemNewSchemeInfo(int AIndex, TUpdateSchemesMapCreator::TSchemeInfo AValue)
{
  CheckArr(__FUNC__);
  (*FSchCpbltArr)[AIndex]->NewSchemeInfo = AValue;
}

//---------------------------------------------------------------------------

void __fastcall TSchemeCompatibilityEditor::CheckArr(const UnicodeString AMethodName)
{
  if (!FSchCpbltArr)
    throw DKClasses::EMethodError(
      AMethodName,
      "��� ������ ��� ��������������."
    );
}

//---------------------------------------------------------------------------

void __fastcall TSchemeCompatibilityEditor::CheckInputs(const UnicodeString AMethodName)
{
  if (!FNewSchemeUID.Length())
    throw DKClasses::EMethodError(
      AMethodName,
      "�� ����������� �������� NewSchemeUID."
    );
  if (!FDB || !FDM)
    throw DKClasses::EMethodError(
      AMethodName,
      "�� ����������� �������� DB."
    );
  if (!FVacSchXML)
    throw DKClasses::EMethodError(
      AMethodName,
      "�� ����������� �������� VacSchXML."
    );
  if (!FVacSchXML->CmpName("imm-s"))
    throw DKClasses::EMethodError(
      AMethodName,
      "��� xml-��������� �� ������� ���������� ������ ���� imm-s."
    );
  if (!FVacSchXML->GetTagByUID(FNewSchemeUID))
    throw DKClasses::EMethodError(
      AMethodName,
      "����� ���������� �� ��������� UID \"" + FNewSchemeUID + "\" �� �������."
    );
  if (!FSchCpbltList)
    throw DKClasses::EMethodError(
      AMethodName,
      "�� ����������� �������� SchCpbltList."
    );
}

//---------------------------------------------------------------------------

void __fastcall TSchemeCompatibilityEditor::CheckEditMode(const UnicodeString AMethodName, bool AReqModeValue)
{
  if (FEditMode != AReqModeValue)
    if (FEditMode)
      throw DKClasses::EMethodError(
        AMethodName,
        "�������� ��������� � ������ ��������������."
      );
    else
      throw DKClasses::EMethodError(
        AMethodName,
        "�������� �� ��������� � ������ ��������������."
      );
}

//---------------------------------------------------------------------------

void __fastcall TSchemeCompatibilityEditor::SetSchCpbltList(
  TUpdateSchemesMapCreator::TSchemeCpbltList* AValue
)
{
  CheckEditMode(__FUNC__, false);
  FSchCpbltList = AValue;
  if (FSchCpbltListBuf)
  {
    delete FSchCpbltListBuf;
    FSchCpbltListBuf = NULL;
  }
  if (FSchCpbltArr)
  {
    delete FSchCpbltArr;
    FSchCpbltArr = NULL;
  }
  if (FSchCpbltList)
  {
    FSchCpbltListBuf = new TUpdateSchemesMapCreator::TSchemeCpbltList;
    *FSchCpbltListBuf = *FSchCpbltList;
    int nArrSize;
    if (FEditAll)
      nArrSize = FSchCpbltListBuf->size();
    else
	  nArrSize = std::count_if(
        FSchCpbltListBuf->begin(),
        FSchCpbltListBuf->end(),
		DKSTL::compose1(
          bind2nd(
            equal_to<UnicodeString>(),
            csReqFldNULL
          ),
          SelectSchCpbltNewSchVacType()
        )
      );
    if (nArrSize)
    {
      FSchCpbltArr = new TSchCpbltArr(nArrSize);
      int j = 0;
      for (
        TUpdateSchemesMapCreator::TSchemeCpbltList::iterator i = FSchCpbltListBuf->begin();
        i != FSchCpbltListBuf->end();
        i++
      )
      {
        bool fAppend;
        if (FEditAll)
          fAppend = true;
        else
          fAppend = ((*i).NewSchemeInfo.sVacType == csReqFldNULL);
        if (fAppend)
          (*FSchCpbltArr)[j++] = &(*i);
      }
    }
  }
}

//---------------------------------------------------------------------------

bool __fastcall TSchemeCompatibilityEditor::Edit()
{
  CheckEditMode(__FUNC__, false);
  CheckInputs(__FUNC__);

  bool fCpbltSet = true;
  if (FSchCpbltArr)
  {
    TSchCpbltFM *SchCpbltFM = NULL;
    try
    {
      FEditMode = true;
      FDM->OpenDataSets();
      SchCpbltFM = new TSchCpbltFM(NULL);
      SchCpbltFM->SetSchemeCompatibilityEditor(this);
      fCpbltSet = (SchCpbltFM->ShowModal() == mrOk);
    }
    __finally
    {
      if (SchCpbltFM) delete SchCpbltFM;
      FDM->CloseDataSets();
      FEditMode = false;
    }
  }
  return fCpbltSet;
}

//---------------------------------------------------------------------------

void __fastcall TSchemeCompatibilityEditor::Cancel()
{
  CheckEditMode(__FUNC__, true);
  *FSchCpbltListBuf = *FSchCpbltList;
}

//---------------------------------------------------------------------------

void __fastcall TSchemeCompatibilityEditor::Apply()
{
  CheckEditMode(__FUNC__, true);
  *FSchCpbltList = *FSchCpbltListBuf;
}

//---------------------------------------------------------------------------

