/*
  ����        - SchUpd.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ �������������� ����� ���������� �������/�����.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 13.03.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DKMBUtils.h"

#include "SchUpd.h"

#include "VakSchList.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "ToolEdit"
#pragma link "cxButtonEdit"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                               Globals                                 ##
//##                                                                       ##
//###########################################################################

/*
 * ����� ����� � ��������� ������
 *
 *
 * ���������:
 *   [in]  Owner   - �������� ( fmSchList )
 *   [in]  FUM     - ����� ������ �����
 *
 * ������������ ��������:
 *   ModalResult �����
 *
 */

int SchUpd::ShowModalSchUpd( TComponent* Owner, TFormUpdateMode FUM )
{
  int nModalResult = mrNone;
  TfmSchUpd *dlg = new TfmSchUpd(Owner, FUM);
  if (dlg)
  {
    nModalResult = dlg->ShowModal();
    delete dlg;
  }
  return nModalResult;
}

//###########################################################################
//##                                                                       ##
//##                              TfmSchUpd                                ##
//##                                                                       ##
//###########################################################################

__fastcall TfmSchUpd::TfmSchUpd(TComponent* Owner, TFormUpdateMode FUM)
        : TForm(Owner)
{
  FFormUpdateMode = FUM;
  FOwnFM = (TfmSchList*)Owner;
  FInitVakItemIndex = -1;
  FInitSchemeName = "";
}

//---------------------------------------------------------------------------

void __fastcall TfmSchUpd::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif

  edName->Color = AppOptions->ReqEdBkColor;
  cbVak->Color  = AppOptions->ReqEdBkColor;

  //���������� �������� � ����������� �� ������ ������ ����� "������ ����"
  switch (FOwnFM->SchemeObjType)
  {
    case sotVaccine: lbVakTitle->Caption = "����:";  break;
    case sotTest   : lbVakTitle->Caption = "�����:"; break;
  }

  int nCurVacId = -1;
  switch ( FFormUpdateMode )
  {
    case fumADD :
      Caption = "���������� �����";
      nCurVacId = FOwnFM->FillVacListForCurInf(cbVak->Items);
    break;
    case fumEDIT:
      Caption = "��������� ���������� �����";
      nCurVacId = FOwnFM->FillVacListForCurScheme(cbVak->Items);
      edName->Text = FOwnFM->GetActiveSchemeAttrVal( "name" );
      FInitSchemeName = edName->Text;
    break;
  }
  if (nCurVacId != -1)
    cbVak->ItemIndex = cbVak->Items->IndexOfObject(reinterpret_cast<TObject*>(nCurVacId));
  FInitVakItemIndex = cbVak->ItemIndex;
}

//---------------------------------------------------------------------------

bool __fastcall TfmSchUpd::AreInputsCorrect()
{
  if ( edName->Text == "" )
  {
	MB_SHOW_ERROR_CPT(L"���� \"������������\" ������ ���� ���������.", L"������ �����");
    return false;
  }

  if ( cbVak->ItemIndex == -1 )
  {
    switch(FOwnFM->SchemeObjType)
    {
      case sotVaccine:
		MB_SHOW_ERROR_CPT(L"���������� ������� ���� �� ������.", L"������ �����");
      break;
      case sotTest:
		MB_SHOW_ERROR_CPT(L"���������� ������� ����� �� ������.", L"������ �����");
      break;
    }
    return false;
  }

  UnicodeString VakName;
  if ( ( FFormUpdateMode == fumADD ) ||
       ( (FFormUpdateMode == fumEDIT) && WasSchemeAttrsChanged() ) )
    if ( FOwnFM->SchemeNameExists( edName->Text, (int)cbVak->Items->Objects[cbVak->ItemIndex] ) )
    {
	  MB_SHOW_ERROR_CPT( Format( UnicodeString("����� \"%s\" ��� ����."), &TVarRec(edName->Text), 0 ).c_str(), L"������ �����");
      return false;
    }

  return true;
}

//---------------------------------------------------------------------------

bool __fastcall TfmSchUpd::WasSchemeAttrsChanged()
{
  bool Result;

  Result = ( AnsiUpperCase( FInitSchemeName ) != AnsiUpperCase( edName->Text ) );
  Result |= ( cbVak->ItemIndex != FInitVakItemIndex );
  return Result;
}

//---------------------------------------------------------------------------

void __fastcall TfmSchUpd::bApplyClick(TObject *Sender)
{
  if ( !AreInputsCorrect() )
    return;

  switch (FFormUpdateMode)
  {
    case fumADD  :
      FOwnFM->AddScheme ( edName->Text, (int)cbVak->Items->Objects[cbVak->ItemIndex] );
    break;
    case fumEDIT :
      FOwnFM->EditScheme( edName->Text, (int)cbVak->Items->Objects[cbVak->ItemIndex] );
    break;
  }
  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TfmSchUpd::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}

//---------------------------------------------------------------------------

void __fastcall TfmSchUpd::cbVakSelect(TObject *Sender)
{
  #ifndef CAN_CHANGE_START_SCH_VAK
    if ( ( FFormUpdateMode == fumEDIT ) && ( FOwnFM->IfCurSchemeIsStart() ) )
    {
	  MB_SHOW_ERROR_CPT(L"��� ��������� ����� ������ �������� �����������.", L"������ �����");
      cbVak->ItemIndex = FInitVakItemIndex;
    }
  #endif
}

//---------------------------------------------------------------------------
void __fastcall TfmSchUpd::edNamePropertiesButtonClick(TObject *Sender, int AButtonIndex)

{
  if ( cbVak->ItemIndex == -1 )
    switch(FOwnFM->SchemeObjType)
    {
      case sotVaccine:
		MB_SHOW_WARNING(L"��� ��������� ������ ������������ ���� ���������� ������� �����.");
      break;
	  case sotTest:
        MB_SHOW_WARNING(L"��� ��������� ������ ������������ ���� ���������� ������� �����.");
      break;
    }
  else
    ShowModalVakSchList( FOwnFM, (int)cbVak->Items->Objects[cbVak->ItemIndex], cbVak->Text, edName->Text );
}
//---------------------------------------------------------------------------

