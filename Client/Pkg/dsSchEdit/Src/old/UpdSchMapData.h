/*
  ����        - UpdSchMapData.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ��� ��������� TSchemeCompatibilityEditor.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 25.04.2006
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  25.04.2006
    �������������� �������.
*/
//---------------------------------------------------------------------------

#ifndef UpdSchMapDataH
#define UpdSchMapDataH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "FIBDatabase.hpp"
#include "pFIBDatabase.hpp"
#include "FIBQuery.hpp"
#include "pFIBQuery.hpp"

namespace UpdSchMapData
{

//###########################################################################
//##                                                                       ##
//##                            TUpdSchMapDM                               ##
//##                                                                       ##
//###########################################################################

class TUpdSchMapDM : public TDataModule
{
__published:    // IDE-managed Components
        TpFIBTransaction *MainTR;
        TpFIBQuery *GetSchDataQ;
        TpFIBQuery *GetSchDataSizeQ;
        void __fastcall DataModuleCreate(TObject *Sender);


private:        // User declarations
        TpFIBDatabase* FDB;                      // ������� ���� ������


public:         // User declarations
        __fastcall TUpdSchMapDM(TComponent* Owner, TpFIBDatabase *ADB);

        void __fastcall SetGetSchDataSQL(
          const UnicodeString AContingent,
          const UnicodeString AInfecList
        );
};

//---------------------------------------------------------------------------

} // end of namespace UpdSchMapData
using namespace UpdSchMapData;

#endif
