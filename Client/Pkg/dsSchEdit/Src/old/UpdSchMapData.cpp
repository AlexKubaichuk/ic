/*
  ����        - UpdSchMapData.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ��� ��������� TSchemeCompatibilityEditor.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 25.04.2006
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UpdSchMapData.h"

#include "DKDBUtils.h"
#include "DKClasses.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "FIBQuery"
#pragma link "pFIBQuery"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                            TUpdSchMapDM                               ##
//##                                                                       ##
//###########################################################################

__fastcall TUpdSchMapDM::TUpdSchMapDM(TComponent* Owner, TpFIBDatabase *ADB)
        : TDataModule(Owner)
{
  if (!ADB)
    throw DKClasses::ENullArgument(this, "TUpdSchMapDM", "ADB");
  FDB = ADB;
}

//---------------------------------------------------------------------------

void __fastcall TUpdSchMapDM::DataModuleCreate(TObject *Sender)
{
  SetDMFIBComponentsDB(this, FDB);
}

//---------------------------------------------------------------------------

// AContingent - ������ ID ��������� (����� �������)
// AInfecList  - ������ ID ��������  (����� �������)

void __fastcall TUpdSchMapDM::SetGetSchDataSQL(
  const UnicodeString AContingent,
  const UnicodeString AInfecList
)
{
  GetSchDataQ->SQL->Text =
    "select distinct R1078 as VacScheme "
    "from CARD_1076 "
    "where UCODE in (" + AContingent + ") and "
    "      R1077 in (" + AInfecList  + ")";
  GetSchDataSizeQ->SQL->Text =
    "select count (distinct R1078) as VacSchemeCount "
    "from CARD_1076 "
    "where UCODE in (" + AContingent + ") and "
    "      R1077 in (" + AInfecList  + ")";
}

//---------------------------------------------------------------------------
