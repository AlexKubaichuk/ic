/*
  ����        - SchCpbltUpd.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ���� ����������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 26.04.2006
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  26.04.2006
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef SchCpbltUpdH
#define SchCpbltUpdH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>

#include "SchList.h"

//---------------------------------------------------------------------------

namespace SchCpbltUpd
{

//###########################################################################
//##                                                                       ##
//##                            TSchCpbltUpdFM                             ##
//##                                                                       ##
//###########################################################################

// ������ ������ ���� ����������

class TSchCpbltUpdFM : public TForm
{
__published:    // IDE-managed Components
        TPanel *pBottom;
        TPanel *pCtrls;
        TButton *bOk;
        TButton *bCancel;
        TButton *bHelp;
        TTreeView *tv;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bOkClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall tvChange(TObject *Sender, TTreeNode *Node);
        void __fastcall tvCustomDrawItem(TCustomTreeView *Sender,
          TTreeNode *Node, TCustomDrawState State, bool &DefaultDraw);


private:        // User declarations
        SchList::TSchemeCompatibilityEditor* FSchCpbltEditor;
        UnicodeString FSchemeUID;
        UnicodeString FLineUID;
        UnicodeString FVacType;


protected:
        bool __fastcall IsRefObj(TTreeNode *Node);
        bool __fastcall AreInputsCorrect();
        void __fastcall TagNodeToTree(TTagNode *TagNode, TTreeNode *TreeNode);


public:         // User declarations
        __fastcall TSchCpbltUpdFM(TComponent* Owner, UnicodeString ASchemeUID);
        virtual __fastcall ~TSchCpbltUpdFM();

        // UID �����
        __property UnicodeString SchemeUID = {read = FSchemeUID};
        // UID ������ �����
        __property UnicodeString LineUID = {read = FLineUID, write = FLineUID};
        // ��� ����������
        __property UnicodeString VacType = {read = FVacType};

        // ��������� ��������� ������������ ����� ����������
        void __fastcall SetSchemeCompatibilityEditor(SchList::TSchemeCompatibilityEditor* ASchCpbltEditor);
};

//---------------------------------------------------------------------------

} // end of namespace SchCpbltUpd
using namespace SchCpbltUpd;

#endif
