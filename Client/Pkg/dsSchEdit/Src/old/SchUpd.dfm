object fmSchUpd: TfmSchUpd
  Left = 398
  Top = 250
  BorderStyle = bsDialog
  BorderWidth = 5
  Caption = 'fmSchUpd'
  ClientHeight = 105
  ClientWidth = 304
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 304
    Height = 71
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 304
      Height = 71
      Align = alClient
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 17
        Width = 79
        Height = 13
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077':'
      end
      object lbVakTitle: TLabel
        Left = 8
        Top = 46
        Width = 35
        Height = 13
        Caption = #1052#1048#1041#1055':'
      end
      object cbVak: TComboBox
        Left = 96
        Top = 42
        Width = 200
        Height = 21
        Color = clInfoBk
        TabOrder = 0
        OnSelect = cbVakSelect
      end
      object edName: TcxButtonEdit
        Left = 93
        Top = 15
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = edNamePropertiesButtonClick
        TabOrder = 1
        Text = 'edName'
        Width = 203
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 71
    Width = 304
    Height = 34
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 69
      Top = 0
      Width = 235
      Height = 34
      Align = alRight
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 0
      object bApply: TButton
        Left = 0
        Top = 6
        Width = 75
        Height = 25
        Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
        Default = True
        TabOrder = 0
        OnClick = bApplyClick
      end
      object bCancel: TButton
        Left = 80
        Top = 6
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
        OnClick = bCancelClick
      end
      object bHelp: TButton
        Left = 160
        Top = 6
        Width = 75
        Height = 25
        Caption = #1055#1086#1084#1086#1097#1100
        TabOrder = 2
      end
    end
  end
end
