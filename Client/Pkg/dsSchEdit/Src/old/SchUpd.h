/*
  ����        - SchUpd.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ �������������� ����� ���������� �������/�����.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 13.03.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  26.02.2007
    [*] ������ � ���������� ���������� �������������� ����� ���������� ����������
        Dkglobals::AppOptions

  13.03.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef SchUpdH
#define SchUpdH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "ToolEdit.hpp"
#include <Mask.hpp>

#include "DKGlobals.h"
#include "SchList.h"
#include "cxButtonEdit.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"

namespace SchUpd
{

//###########################################################################
//##                                                                       ##
//##                               Globals                                 ##
//##                                                                       ##
//###########################################################################

extern int ShowModalSchUpd( TComponent* Owner, TFormUpdateMode FUM );

//###########################################################################
//##                                                                       ##
//##                              TfmSchUpd                                ##
//##                                                                       ##
//###########################################################################

// ������ �������������� ����� ���������� �������/�����

class TfmSchUpd : public TForm
{
__published:    // IDE-managed Components
        TPanel *Panel1;
        TPanel *Panel2;
        TPanel *Panel3;
        TButton *bApply;
        TButton *bCancel;
        TButton *bHelp;
        TGroupBox *GroupBox1;
        TLabel *Label1;
        TLabel *lbVakTitle;
        TComboBox *cbVak;
 TcxButtonEdit *edName;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bApplyClick(TObject *Sender);
        void __fastcall cbVakSelect(TObject *Sender);
 void __fastcall edNamePropertiesButtonClick(TObject *Sender, int AButtonIndex);


private:        // User declarations
        TFormUpdateMode FFormUpdateMode;        //����� ������ �����
        TfmSchList*     FOwnFM;                 //����� "������ ����"
        int             FInitVakItemIndex;      //��������� �������� ItemIndex'� �������
        UnicodeString      FInitSchemeName;        //��������� ������������ �����

        bool __fastcall AreInputsCorrect();
        bool __fastcall WasSchemeAttrsChanged();

public:         // User declarations
        __fastcall TfmSchUpd(TComponent* Owner, TFormUpdateMode FUM);
};

//---------------------------------------------------------------------------

} // end of namespace SchUpd
using namespace SchUpd;

#endif
