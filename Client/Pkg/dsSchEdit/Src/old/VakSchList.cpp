/*
  ����        - VakSchList.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ��� ��������� ������ ���� ��������� �������.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 07.04.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DKUtils.h"

#include "VakSchList.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                               Globals                                 ##
//##                                                                       ##
//###########################################################################

/*
 * ����� ����� � ��������� ������
 *
 *
 * ���������:
 *   [in]  Owner    - �������� ( fmSchList )
 *   [in]  AVakId   - ID �������
 *   [in]  AVakName - ������������ �������
 *   [in]  ASchName - ������������ ������� �����
 *
 * ������������ ��������:
 *   ModalResult �����
 *
 */

int VakSchList::ShowModalVakSchList( TComponent* Owner, int AVakId, UnicodeString AVakName, UnicodeString ASchName )
{
  int nModalResult = mrNone;
  TfmVakSchList *dlg = new TfmVakSchList(AVakId, AVakName, Owner, ASchName);
  if (dlg)
  {
    nModalResult = dlg->ShowModal();
    delete dlg;
  }
  return nModalResult;
}

//###########################################################################
//##                                                                       ##
//##                             TfmVakSchList                             ##
//##                                                                       ##
//###########################################################################

//Owner - ����������� ������ ���� ����� fmSchList

__fastcall TfmVakSchList::TfmVakSchList(int AVakId, UnicodeString AVakName, TComponent* Owner, UnicodeString ASchName )
        : TForm(Owner)
{
  FSchListFM = (TdsSchEditForm*)Owner;
  FVakId     = AVakId;
  FVakName   = AVakName;
  FSchName   = ASchName;
}

//---------------------------------------------------------------------------

void __fastcall TfmVakSchList::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif

  //���������� �������� � ����������� �� ������ ������ ����� "������ ����"
  switch (FSchListFM->SchemeObjType)
  {
    case sotVaccine: Caption = "������ ���� ����";  break;
    case sotTest   : Caption = "������ ���� �����"; break;
  }

  Caption = Caption + " \"" + FVakName + "\"";
  TIntSet VakIds;
  VakIds.insert(FVakId);
  FSchListFM->GetSchList( lbxScheme->Items, VakIds, TGetSchListOptions() );
  UpdateListBoxLogicalWidth( lbxScheme );
  lbxScheme->ItemIndex = lbxScheme->Items->IndexOf(FSchName);
}

//---------------------------------------------------------------------------

void __fastcall TfmVakSchList::bOkClick(TObject *Sender)
{
  ModalResult = mrOk;
}

//---------------------------------------------------------------------------

void __fastcall TfmVakSchList::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}

//---------------------------------------------------------------------------


