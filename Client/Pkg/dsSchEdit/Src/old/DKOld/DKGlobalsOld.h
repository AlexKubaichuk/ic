/*
  ����        - DKGlobalsOld.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ����������.
                ������������ ����.
                �������� ��� �������� �������������!!!
  ����������� - ������� �.�.
  ����        - 10.03.2006
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  02.03.2007
    [+] ������� GetViewAge
    [+] ������� DecodeAgeStr

  10.03.2006
    �������������� �������
*/
//---------------------------------------------------------------------------

#if !defined(DKGlobalsH)
#error Do not include this file directly.  Include 'DKGlobals.h'.
#endif

#ifndef DKGlobalsOldH
#define DKGlobalsOldH

//---------------------------------------------------------------------------

#include <SysUtils.hpp>

namespace Dkglobals
{

//---------------------------------------------------------------------------

#define IS_DIGIT(ch)    ( ( (ch) >= '0' ) && ( (ch) <= '9' ) )

// ������ ����
#define DK_CODE_VERSION         DKCodeVersion().c_str()

#define REQ_FIELD_NULL_VALUE    csReqFldNULL

// ������
#define PROG_REG_KEY_BASE       TAppOptions::csProgRegKeyBase.c_str()
#define CURRENT_VERSION_REG_KEY ("\\" + TAppOptions::csCurrentVersionRegKey).c_str()
#define LPUPARAM_REG_KEY        "\\LPUParam"
#define LPUPARAM_FullName       "FullName"
#define TEMPLATES_DIR           TAppOptions::csTemplateDir.c_str()

// ��� ������� � MS Word
#define DOT_FILE_NAME           TAppOptions::csMSWordDotFileName.c_str()
#define TMP_DOC_FILE_NAME       TAppOptions::csMSTmpDocFileName.c_str()
#define RES_DOC_FILE_NAME       TAppOptions::csMSTmpDocTmpFileName.c_str()

//---------------------------------------------------------------------------

extern UnicodeString GetViewAge(UnicodeString Age);
extern void DecodeAgeStr(UnicodeString AgeStr, Word &Years, Word &Months, Word &Days);

} // end of namespace Dkglobals

#endif
