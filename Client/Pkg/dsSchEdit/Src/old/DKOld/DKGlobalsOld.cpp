/*
  ����        - DKGlobalsOld.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ����������.
                ���� ����������.
                �������� ��� �������� �������������!!!
  ����������� - ������� �.�.
  ����        - 10.03.2006
*/
//---------------------------------------------------------------------------

#pragma hdrstop

#ifndef DKGlobalsH
  #define DKGlobalsH
  #include "DKGlobalsOld.h"
  #undef DKGlobalsH
#else
  #include "DKGlobalsOld.h"
#endif

#include "DKGlobals.h"
#include "ICSDocGlobals.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------

UnicodeString Dkglobals::GetViewAge(UnicodeString Age)
{
  return XMLStrDurationToStr(Age);
}

//---------------------------------------------------------------------------

void Dkglobals::DecodeAgeStr(UnicodeString AgeStr, Word &Years, Word &Months, Word &Days)
{
  int nYears, nMonths, nDays;
  XMLStrToDuration(AgeStr, &nYears, &nMonths, &nDays);
  Years  = nYears;
  Months = nMonths;
  Days   = nDays;
}

//---------------------------------------------------------------------------

