/*
  ����        - SchDataOld.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ��� ��������� ���� ����������/����.
                ������������ ����.
                �������� ��� �������� �������������!!!
  ����������� - ������� �.�.
  ����        - 10.03.2006
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  10.03.2006
    �������������� �������
*/
//---------------------------------------------------------------------------

#if !defined(SchDataH)
#error Do not include this file directly.  Include 'SchData.h'.
#endif

#ifndef SchDataOldH
#define SchDataOldH

//---------------------------------------------------------------------------

#include <SysUtils.hpp>

namespace SchData
{

//---------------------------------------------------------------------------

extern UnicodeString StartSchFN;                   //��� ���� ������� "��������", ����������� UID ��������� ����� ��� �����
extern UnicodeString SchOwnerTN;                   //��� ������� "�������" ��� "�����"
extern UnicodeString SchOwnerNameFN;               //��� ���� "������������" ������� "�������" ��� "�����"
extern UnicodeString SchOwnerInfekTN;              //��� ������� "������� ��� ��������" ��� "�������� -> �����"
extern UnicodeString SchOwnerInfekInfekFN;         //��� ���� "��������" ������� "������� ��� ��������" ��� "�������� -> �����"
extern UnicodeString SchOwnerInfekSchOwnerFN;      //��� ���� "�������" ������� "������� ��� ��������" ��� ���� "�����" ������� "�������� -> �����"

//---------------------------------------------------------------------------

} // end of namespace SchData

#endif
