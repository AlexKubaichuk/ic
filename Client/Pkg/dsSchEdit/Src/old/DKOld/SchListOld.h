/*
  ����        - SchListOld.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - �������� ����.
                ������������ ����.
                �������� ��� �������� �������������!!!
  ����������� - ������� �.�.
  ����        - 10.03.2006
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  10.03.2006
    �������������� �������
*/
//---------------------------------------------------------------------------

#if !defined(SchListH)
#error Do not include this file directly.  Include 'SchList.h'.
#endif

#ifndef SchListOldH
#define SchListOldH

//---------------------------------------------------------------------------

#include <SysUtils.hpp>
#include <map.h>
#include "AxeUtil.h"

namespace SchList
{

//---------------------------------------------------------------------------

#define IMMS_DTD_FILE_NAME      "imm-s.dtd"
#define IMMS_XML_FILE_TITLE     "<!DOCTYPE imm-s SYSTEM \'"IMMS_DTD_FILE_NAME"\'>"

#define NULL_REF_LABEL  csNullRefLabel
#define END_LINE_LABEL  csEndLineLabel

#define XMLUIDAsInt(xml_uid) StrToInt( (AnsiString)"0x" + xml_uid )
#define XMLUIDAsHex(xml_uid) IntToHex( xml_uid, 4 )

#define IND_NONE_IMG            ((int)iiNone)
#define IND_SROKJUMP_IMG        ((int)iiSrokJump)
#define IND_AGEJUMP_IMG         ((int)iiAgeJump)
#define IND_JUMP_IMG            ((int)iiJump)
#define IND_ENDJUMP_IMG         ((int)iiEndJump)
#define IND_JUMPEXISTS_IMG      ((int)iiJumpExists)
#define IND_SUBITEM_BASE        cnSubItemBase

typedef map<const AnsiString, TTagNode*>        TUIDTable;

//����� ������ �����
enum TSchListMode {
  smVak,        //����� ����������
  smProb        //����� ����
};

//---------------------------------------------------------------------------

} // end of namespace SchList

#endif
