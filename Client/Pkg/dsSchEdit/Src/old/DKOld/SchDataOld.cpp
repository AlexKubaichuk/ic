/*
  ����        - SchDataOld.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ������ ��� ��������� ���� ����������/����.
                ���� ����������.
                �������� ��� �������� �������������!!!
  ����������� - ������� �.�.
  ����        - 13.03.2004
*/
//---------------------------------------------------------------------------

#pragma hdrstop

#ifndef SchDataH
  #define SchDataH
  #include "SchDataOld.h"
  #undef SchDataH
#else
  #include "SchDataOld.h"
#endif

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------

UnicodeString SchData::StartSchFN;
UnicodeString SchData::SchOwnerTN;
UnicodeString SchData::SchOwnerNameFN;
UnicodeString SchData::SchOwnerInfekTN;
UnicodeString SchData::SchOwnerInfekInfekFN;
UnicodeString SchData::SchOwnerInfekSchOwnerFN;

//---------------------------------------------------------------------------
