/*
  ����        - SchList.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - �������� ���� ���������� ������/����.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 12.03.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  26.02.2007
    [+] �������� ExtNOM_DLL, XMLContainer � NmklGUI
    [*] ������ � ���������� ���������� �������������� ����� ���������� ����������
        Dkglobals::AppOptions
    [*] kab
    [*] Toolbar2000 ������� �� TdxBar
    [*] StatusBar ������� �� TdxStatusBar
    [*] ����� ���������� ���� ������ �� ������������
    [+] �������� ExtNOM_DLL, XMLContainer � NmklGUI

  04.05.2006
    [!] ��� ������ � �������� � TUpdateSchemesMapCreator

  23.04.2006 - 28.04.2006
    [+] ���������� ������ ����/���� � ��������
    [*] ����� "��������" �������� �� "���������� ��"
    [+] ����������� ����� TfmSchList::GetPrivivFullName
    [+] ����� TUpdateSchemesMapCreator
    [+] ��������������� ����� TSchemeCompatibilityEditor

  18.03.2006
    [+] ���������� ������ ������ �� ���������
    [+] �����-�������� TIMMSchemeEditor
    [*] ��������� uid �������������� ���������� TTagNode
    [*] �������������� TTagNode ������ xml-�����
    [-] ������� ShowSchList � ShowModalSchList, ������ ��� ������� ������������
        �����-�������� TIMMSchemeEditor
    [*] �����������

  01.12.2005
    [+] ������ �� �������� ������ ����� ��� �����, ���� ������� ��������
        ��� ����� ��������������� �����

  01.12.2005 - kab
    [!] ��������� ������� ������� ������������ ��������

  28.02.2005
    1. ��������� ��������� �������� ����� pgm_setting.h

  18.02.2005
    1. ������ ���� ��� ������� ��� ����� ��� ����, �� ������
       �������� ��������, ������ ��������� ������ ������
    2. ��������� ����, ��������� � ���������� ��� ��������� �����
       ��� ������� ����� ����

  12.03.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

//***************************************************************************
//********************** ���������� *****************************************
//***************************************************************************

/*
  ���������� ����� � ����������� � ���� �������� ������ �� �������������, ���
  �������� ������������ ��� ������������ ���� ����������. ������� ������������
  ���� ��� ���� ����������, ������� ������� ������ � ������ ���������� ������ ��
  � �� ����� ( ����������� ����� ���������������� ��������� ��� ���� � ����
  ��������� ).
  ������� ��� ������ ��������� � ������ "�����" (sotTest) ������� "�������"
  ���������� �������� "�����", ��� ������� ���� ��������� � ������ ���������
  ���������� ���������� ��������� � � ������������� ������ �� � �� �����.
*/

//---------------------------------------------------------------------------

#ifndef SchListH
#define SchListH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������
#include "pgmsetting.h"

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ActnList.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Math.hpp>
#include <Buttons.hpp>
#include <Graphics.hpp>
#include <jpeg.hpp>
#include "pFIBDatabase.hpp"
#include <limits.h>
#include "cxClasses.hpp"
#include "cxControls.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "DKListBox.h"
#include "dxBar.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinsdxBarPainter.hpp"
#include "dxSkinsdxStatusBarPainter.hpp"
#include "dxStatusBar.hpp"
#include <System.Actions.hpp>
#include <set>
#include <map>
#include <list>
#include <vector>
//#include <function>
#include "XMLContainer.h"

#include "DKListBox.h"
#include "DKUtils.h"
#include "SchData.h"
#include "UpdSchMapData.h"
#include "dxBar.hpp"
#include "cxControls.hpp"
#include "cxGraphics.hpp"
#include "dxStatusBar.hpp"
#include "cxClasses.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinsdxBarPainter.hpp"
#include "dxSkinsdxStatusBarPainter.hpp"
#include <System.Actions.hpp>

//---------------------------------------------------------------------------

namespace SchList
{

//###########################################################################
//##                                                                       ##
//##                               Globals                                 ##
//##                                                                       ##
//###########################################################################

const int cnSubItemBase = 100;  //�������� ��� ������� ��������, �������������� ���
                                //SubItem'�� ������ ��������

enum TImgInd
{
  iiNone       = -1,    //��� ��������
  iiSrokJump   = 0,     //������ �������� ��� �������� �� �����
  iiAgeJump    = 1,     //������ �������� ��� �������� �� ��������
  iiJump       = 2,     //������ �������� ��� �������� �� �������
  iiEndJump    = 3,     //������ �������� ��� ������������ ��������
  iiJumpExists = 4      //������ �������� ��� ������� ������� ��������
};

typedef map<UnicodeString, UnicodeString>             TUpdateUIDTable;
typedef set <UnicodeString>                        THexUIDs;
typedef set <int>                               TIntSet;

//����� ��� ������ TfmSchList::GetSchList - �������� ������ ���� ��� ��������� ������
enum TGetSchListOption {
  slVakNames,           //������� � ������� ����� ������������ ����� ������������ ��������������� �������
  slSchFullUIDs         //������������� �� ������� ������, ��������������� ����� ������ ����
                        // "<UID �����>.<UID ������ ������ �����>"
};

typedef Set<TGetSchListOption, slVakNames, slSchFullUIDs>  TGetSchListOptions;

//---------------------------------------------------------------------------

//��������� ������ "����� �� ������� ������� ����� ?"
enum TCanDelSchRes {
  crNO_ACTIVE_SCHEME,                   //��� �������� �����
  crCAN_DELETE_SCHEME,                  //����� �������
  crONLY_ONE_SCHEME,                    //������ ������� ������������ �����
  crCUR_SCHEME_IS_START,                //������ ������� ��������� �����
  crCUR_SCHEME_LINES_REFS_EXIST,        //�� ������ ����� ������� ������
  crCUR_SCHEME_LINES_DBREFS_EXIST       //�� ������ ����� ������� ������ � ��
};

//---------------------------------------------------------------------------

//��������� ������ "����� �� ������� ������� �������� ?"
enum TCanDelPrivRes {
  crNO_ACTIVE_PRIVIV,                   //��� �������� ��������
  crCAN_DELETE_PRIVIV,                  //����� �������
  crNOT_GR_END_PRIVIV,                  //�������� �������� �� �������� ��������� � ������ "V" ��� "RV"
  crEND_LINE,                           //������ ������� ��������� �����
  crCUR_PRIVIV_REFS_EXIST,              //�� ������ ������� ������
  crCUR_PRIVIV_DBREFS_EXIST             //�� ������ ������� ������ � ��
};

//---------------------------------------------------------------------------

extern const UnicodeString csNullRefLabel;
extern const UnicodeString csEndLineLabel;
extern const UnicodeString csEndPlanLabel;

//###########################################################################
//##                                                                       ##
//##                            TfmSchList                                 ##
//##                                                                       ##
//###########################################################################

/*
 * ��������� ��� ������������� �������� xml-���������
 *
 *
 * ���������:
 *   [in]     ASchemeObjType - ��� �������� ������� ��� �����
 *   [in\out] XML            - xml-��������
 *
 * ������������ ��������:
 *   true  - �������� ���������
 *   false - �������� �� ���������
 *
 */
typedef bool __fastcall (__closure *TOnLoadXML)(TObject *Sender, TSchemeObjType ASchemeObjType, TTagNode *&XML);

/*
 * ��������� ��� ������������� ���������� xml-���������
 *
 *
 * ���������:
 *   [in]     ASchemeObjType - ��� �������� ������� ��� �����
 *   [in\out] XML            - xml-��������
 *
 * ������������ ��������:
 *   true  - ���������� ���������
 *   false - ���������� �� ���������
 *
 */
typedef bool __fastcall (__closure *TOnSaveXML)(TObject *Sender, TSchemeObjType ASchemeObjType, TTagNode *&XML);

//---------------------------------------------------------------------------

// ������ ���� ���������� ������/����

class TfmSchList : public TForm
{
__published:    // IDE-managed Components
    TActionList *ActionList;
    TPanel *pMain;
    TImageList *ilSchPriviv;
    TAction *actAddScheme;
    TAction *actEditScheme;
    TAction *actCopyScheme;
    TAction *actDeleteScheme;
    TAction *actAddPriviv;
    TAction *actEditPriviv;
    TAction *actDeletePriviv;
    TImageList *ilEdit;
    TAction *actApplyUpdates;
    TAction *actCancelUpdates;
    TAction *actClose;
    TAction *actHelp;
    TSplitter *Splitter1;
    TPanel *Panel8;
    TPanel *Panel7;
    TPanel *Panel1;
    TPanel *Panel2;
    TComboBox *cbVak;
    TLabel *lbVakTitle;
    TPanel *Panel3;
    TLabel *Label2;
    TImageList *ilLBX;
    TDKListBox *lbxScheme;
    TListView *lvPrivivList;
    TLabel *InfekLB;
    TComboBox *InfekCBx;
    TAction *actCondListEdit;
    TdxBarManager *dxBarManager;
    TdxBarSubItem *dxBarSubItem1;
    TdxBarSubItem *dxBarSubItem2;
    TdxBarSubItem *dxBarSubItem3;
    TdxBarSubItem *HelpMIt;
    TdxBarButton *dxBarButton1;
    TdxBarButton *dxBarButton2;
    TdxBarButton *dxBarButton3;
    TdxBarButton *dxBarButton4;
    TdxBarButton *dxBarButton5;
    TdxBarButton *dxBarButton6;
    TdxBarButton *dxBarButton7;
    TdxBarButton *dxBarButton8;
    TdxBarButton *dxBarButton9;
    TdxBarButton *dxBarButton10;
    TdxBarButton *dxBarButton11;
    TdxBarButton *dxBarButton12;
    TdxBarButton *dxBarButton13;
    TdxBarButton *dxBarButton14;
    TdxBarButton *dxBarButton15;
    TdxBarButton *dxBarButton16;
    TdxBarButton *dxBarButton17;
    TdxBarButton *dxBarButton18;
    TdxBarButton *dxBarButton19;
    TdxBarButton *dxBarButton20;
    TdxBarButton *dxBarButton21;
    TdxBarButton *dxBarButton22;
    TdxBarButton *dxBarButton23;
    TPopupMenu *SchemePMn;
    TPopupMenu *PrivivPMn;
    TMenuItem *N1;
    TMenuItem *N2;
    TMenuItem *N3;
    TMenuItem *N4;
    TMenuItem *N5;
    TMenuItem *N6;
    TMenuItem *N7;
    TdxStatusBar *dxStatusBar;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall lvPrivivListKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift);
    void __fastcall lvPrivivListDblClick(TObject *Sender);
    void __fastcall actCloseExecute(TObject *Sender);
    void __fastcall lvPrivivListMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y);
    void __fastcall lbxSchemeDrawListItemImage(TObject *Sender,
      int ListItemIndex, int &ItemImageIndex);


private:        // User declarations
    // ��������, ������������ ������������ ���������
    struct TEditorBookmark
    {
      int nInfId;                   // Id ��������
      int nVacId;                   // Id �������
      UnicodeString sSchUID;           // UID �����
      UnicodeString sSchLineUID;       // UID ������ �����
      __fastcall TEditorBookmark() : nInfId(-1), nVacId(-1) {}
      void __fastcall Clear()
      {
        nInfId      = -1;
        nVacId      = -1;
        sSchUID     = "";
        sSchLineUID = "";
      }
    };
    TSchemeObjType     FSchemeObjType;      // ��� �������� ������� ��� �����
    TStrings*          FFilterList;         // ������ �������� ��� ��������� �� �������
    TpFIBDatabase*     FDB;                 // ������� ���� ������
    TAxeXMLContainer*  FXMLContainer;       // XML-��������� � ����������� �������������
    UnicodeString         FNmklGUI;            // ��� ������������
    TSDM*              FSDM;                // ������ ������
    TTagNode*          FIMM_S;              // ������������� xml-��������
    bool               FXMLModified;        // ������� ������� ������������� ��������� � xml-���������
    bool               FIMM_SModified;      // ������� ������� ����������� ��������� � xml-���������
    bool               FEditorActive;       // ������� ���������� ������
    TEditorBookmark*   FEditorBookmark;     // ��������, ������������ ������������ ���������
    bool               FValidateOnSave;     // ������� ��������� xml-��������� ��� ����������
    TOnLoadXML         FOnLoadXML;          // ��������� ��� ������������� �������� xml-���������
    TOnSaveXML         FOnSaveXML;          // ��������� ��� ������������� ���������� xml-���������


protected:
    // ������� ������� ������������� ��������� � xml-���������
    __property bool XMLModified = {read = FXMLModified, write = SetXMLModified };

    void __fastcall SetXMLModified(bool AValue);
    void __fastcall SetDB(TpFIBDatabase *AValue);
    void __fastcall SetXMLContainer(TAxeXMLContainer *AValue);
    void __fastcall SetNmklGUI(UnicodeString AValue);
    void __fastcall SetIMM_S(TTagNode* AValue);

    void __fastcall UpdateSchemeActions();
    void __fastcall UpdatePrivivActions();
    void __fastcall UpdateSchemeStatusPanel();
    void __fastcall UpdatePrivivStatusPanel();
    bool __fastcall IfCurSchemeLinesRefsExist(TTagNode *ItTag, void *Src1 = NULL,void *Src2 = NULL,void *Src3 = NULL, void *Src4 = NULL);
    bool __fastcall SchemeLineDBRefsExist( TTagNode* SchemeLine );
    TCanDelSchRes __fastcall CanDeleteCurScheme();
    bool __fastcall AddLineInfo( TTagNode *LineNode, TListItem *ListItem );
    bool __fastcall EditLineInfo( TTagNode *LineNode, TListItem *ListItem );
    bool __fastcall UpdateLineJumpsInfo( TTagNode *LineNode, TListItem *ListItem );
    TCanDelPrivRes __fastcall CanDeleteActivePriviv();
    void __fastcall UpdatePrivivList( TTagNode *SchemeNode );
    TTagNode* __fastcall GetActiveSchemeNode();
    bool __fastcall UpdateInternalRefs (TTagNode *ItTag, void *Src1 = NULL,void *Src2 = NULL,void *Src3 = NULL, void *Src4 = NULL);
    bool __fastcall GetValidPrivivTypes ( TStrings *strings );
    bool __fastcall GetValidProbTypes ( TStrings *strings );
    bool __fastcall ApplyUpdates();
    bool __fastcall CancelUpdates();
    inline void __fastcall CheckActive(const UnicodeString MethodName) const;
    bool __fastcall OpenEditor();
    bool __fastcall CloseEditor();
    void __fastcall SetBookmark();
    void __fastcall ClearBookmark();
    static TTagNode* __fastcall NewScheme(UnicodeString ASchemeName, int AVakId, TTagNode *AXML);
    static bool __fastcall ValidateSchemeExistence(TSDM* ASDM, TTagNode *AXML);
    static bool __fastcall ValidateSchemeJumps(TSDM* ASDM, TTagNode *AXML);


public:         // User declarations
    __fastcall TfmSchList(TComponent *Owner, TSchemeObjType ASchemeObjType);
    virtual __fastcall ~TfmSchList();

    Variant            ICSNom;              // ���������� ����������� nom_dll.dll
    // ��� �������� ������� ��� �����
    __property TSchemeObjType SchemeObjType = {read = FSchemeObjType};
    // ������� ���� ������
    __property TpFIBDatabase*    DB = {read = FDB, write = SetDB};
    // ���������� ����������� nom_dll.dll
    // XML-��������� � ����������� �������������
    __property TAxeXMLContainer* XMLContainer = {read = FXMLContainer, write = SetXMLContainer};
    // ��� ������������
    __property UnicodeString        NmklGUI = {read = FNmklGUI, write = SetNmklGUI};

    // ������ �������� ��� ��������� �� �������
    // -- ������ ������ ������ ����� ��� "<������������ �������>=<��� �������>"
    //    <��� �������> - ���������� �����
    // -- ��������� �� �������� ������ ������ �� ��������������
    __property TStrings*      FilterList = {read = FFilterList};
    // ������������� xml-��������
    __property TTagNode*      IMM_S = {read = FIMM_S, write = SetIMM_S};
    // ������� ������� ����������� ��������� � xml-���������
    __property bool           IMM_SModified = {read = FIMM_SModified};
    // ������� ��������� xml-��������� ��� ����������
    __property bool           ValidateOnSave = {read = FValidateOnSave, write = FValidateOnSave};
    // ��������� ��� ������������� �������� xml-���������
    __property TOnLoadXML     OnLoadXML = {read = FOnLoadXML, write = FOnLoadXML};
    // ��������� ��� ������������� ���������� xml-���������
    __property TOnSaveXML     OnSaveXML = {read = FOnSaveXML, write = FOnSaveXML};

    bool __fastcall CanEdit();
   static bool __fastcall ValidateXML(TpFIBDatabase *ADB, TSchemeObjType ASchemeObjType, TTagNode *AXML);
    bool __fastcall AddScheme ( UnicodeString SchemeName, int nVakId, TTagNode *SchemeNode = NULL );
    void __fastcall EditScheme( UnicodeString SchemeName, int nVakId );
    void __fastcall DeleteActiveScheme( bool bModifyXML = true );
    bool __fastcall SchemeNameExists( UnicodeString SchemeName, int nVakId = -1 );
    bool __fastcall AddPriviv( TTagNode *LineNode, int nAfterInsertInd );
    bool __fastcall EditPriviv( TTagNode *LineNode );
    void __fastcall DeleteActivePriviv();
    void __fastcall UpdateFilterList();    
    int __fastcall FillVacListForCurInf(TStrings *Strings);
    int __fastcall FillVacListForCurScheme(TStrings *Strings);
    TTagNode* __fastcall GetActivePrivivNode();
    UnicodeString __fastcall GetActiveSchemeAttrVal( UnicodeString AttrName );
    void __fastcall SetActiveSchemeAttrVal( UnicodeString AttrName, UnicodeString AttrVal );
    UnicodeString __fastcall GetActivePrivivAttrVal( UnicodeString AttrName );
    void __fastcall SetActivePrivivAttrVal( UnicodeString AttrName, UnicodeString AttrVal );
    bool __fastcall GetInfekSchList( TStrings *strings, int nInfekId );
    bool __fastcall GetVakInfekList( TStrings *strings, int nVakId );
    bool __fastcall GetCurVakInfekList( TStrings *strings );
    bool __fastcall GetInfekVakList( TStrings *strings, int nInfekId );
    int __fastcall GetMaxPrivivItemIndex ( char PrivivType );
    bool __fastcall IfCurSchemeIsStart();
    bool __fastcall IsStartScheme( UnicodeString UID );
    bool __fastcall GetSchList( TStrings *strings, const TIntSet VakIds, TGetSchListOptions GetOptions );
    UnicodeString __fastcall GetSchemeNameByUID( UnicodeString UID, bool bVakName = false );
    static UnicodeString __fastcall GetPrivivFullName(
      TTagNode *AIMM_S,
      UnicodeString AVacName,
      UnicodeString APrivivUID
    );
    UnicodeString __fastcall GetPrivivFullName(UnicodeString APrivivUID);
    UnicodeString __fastcall GetFilterNameByID( int nId );

    UnicodeString __fastcall GetCurVakName();
    UnicodeString __fastcall GetVakName( int nVakId );
    UnicodeString __fastcall GetInfekName( int nInfekId );
    int __fastcall GetCurVakId();

    int __fastcall GetCurSchemePrivivCount();
    int __fastcall GetCurPrivivInd();
    TListItem* __fastcall GetCurPrivivItem();
};

//###########################################################################
//##                                                                       ##
//##                             ESchListError                             ##
//##                                                                       ##
//###########################################################################

// ������������ ������, ����������� ��� ������������ ������ � TfmSchList

class ESchListError : public DKClasses::EMethodError
{
        typedef DKClasses::EMethodError inherited;

public:
        #pragma warn -inl
        /* Exception.Create */
        __fastcall ESchListError(
          const UnicodeString MethodName,
          const UnicodeString Description
        ) : DKClasses::EMethodError(MethodName, Description) {}
        /* TObject.Destroy */
        inline __fastcall virtual ~ESchListError(void) {}
        #pragma warn .inl
};

//###########################################################################
//##                                                                       ##
//##                          TIMMSchemeEditor                             ##
//##                                                                       ##
//###########################################################################

// �������� ���� (�������� ��� TfmSchList)

class TIMMSchemeEditor : public TObject
{
        typedef TObject inherited;

private:        // User declarations
        TfmSchList* FSchListFM;
        Variant         FICSNom;


protected:
        TSchemeObjType __fastcall GetSchemeObjType();
        TpFIBDatabase* __fastcall GetDB();
        void __fastcall SetDB(TpFIBDatabase* AValue);
        TAxeXMLContainer* __fastcall GetXMLContainer();
        void __fastcall SetXMLContainer(TAxeXMLContainer *AValue);
        UnicodeString __fastcall GetNmklGUI();
        void __fastcall SetNmklGUI(UnicodeString AValue);
        TStrings* __fastcall GetFilterList();
        TTagNode* __fastcall GetIMM_S();
        void __fastcall SetIMM_S(TTagNode* AValue);
        bool __fastcall GetIMM_SModified();
        bool __fastcall GetValidateOnSave();
        void __fastcall SetValidateOnSave(bool AValue);

        TOnLoadXML __fastcall GetOnLoadXML();
        void __fastcall SetOnLoadXML(TOnLoadXML AValue);
        TOnSaveXML __fastcall GetOnSaveXML();
        void __fastcall SetOnSaveXML(TOnSaveXML AValue);

        void __fastcall SetICSNom(Variant         AICSNom)
         {
           FICSNom = AICSNom;
           FSchListFM->ICSNom = AICSNom;
         };

public:         // User declarations
        __fastcall TIMMSchemeEditor(TComponent *Owner, TSchemeObjType ASchemeObjType);
        virtual __fastcall ~TIMMSchemeEditor();

        // ��� �������� ������� ��� �����
        __property TSchemeObjType SchemeObjType = {read = GetSchemeObjType};
        // ������� ���� ������
        __property TpFIBDatabase* DB = {read = GetDB, write = SetDB};
        // ���������� ����������� nom_dll.dll
        // XML-��������� � ����������� �������������
        __property Variant ICSNom = {read = FICSNom, write = SetICSNom};
        __property TAxeXMLContainer* XMLContainer = {read = GetXMLContainer, write = SetXMLContainer};
        // ��� ������������
        __property UnicodeString        NmklGUI = {read = GetNmklGUI, write = SetNmklGUI};
        // ������ �������� ��� ��������� �� �������
        // -- ������ ������ ������ ����� ��� "<������������ �������>=<��� �������>"
        //    <��� �������> - ���������� �����
        // -- ��������� �� �������� ������ ������ �� ��������������
        __property TStrings*      FilterList = {read = GetFilterList};
        // xml-�������� ���� imm-s
        __property TTagNode*      IMM_S = {read = GetIMM_S, write = SetIMM_S};
        // ������� ������� ����������� ��������� � xml-���������
        __property bool           IMM_SModified = {read = GetIMM_SModified};
        // ������� ��������� xml-��������� ��� ����������
        __property bool           ValidateOnSave = {read = GetValidateOnSave, write = SetValidateOnSave};
        // ��������� ��� ������������� �������� xml-���������
        __property TOnLoadXML     OnLoadXML = {read = GetOnLoadXML, write = SetOnLoadXML};
        // ��������� ��� ������������� ���������� xml-���������
        __property TOnSaveXML     OnSaveXML = {read = GetOnSaveXML, write = SetOnSaveXML};

        // ����������� ���������� ��������� (��������� ��. cpp-����)
        int  __fastcall ShowEditor();
        // ���������� ����������� ������ �������������� (��������� ��. cpp-����)
        bool __fastcall CanEdit();
        // ��������� xml-��������� (��������� ��. cpp-����)
        static bool __fastcall ValidateXML(
          TpFIBDatabase *ADB,
          TSchemeObjType ASchemeObjType,
          TTagNode *AXML
        );
};

//###########################################################################
//##                                                                       ##
//##                       TUpdateSchemesMapCreator                        ##
//##                                                                       ##
//###########################################################################

// ������������ ������������ ������� ��� ���������� ������� ���� ����������
// ��������� ��������� ��� ��������� ��������

class TUpdateSchemesMapCreator : public TObject
{
        typedef TObject inherited;

public:
        typedef list<UnicodeString> TOldSchemesList;

        // key  - ID ����� ������ �����,
        // data - ������ ID ������ ����� �����,
        // ������ ID ������ �����: <UID �����>.<UID ������> | _NULL_
        typedef map<const UnicodeString, TOldSchemesList> TUpdateSchemesMap;

        // data - ID ��������
        typedef list<int> TInfecList;

        struct TSchemeInfo
        {
          UnicodeString sSchemeUID;        // UID �����
          UnicodeString sLineUID;          // UID ������ �����
          UnicodeString sVacType;          // ��� ����������

          TSchemeInfo() :
            sSchemeUID(csReqFldNULL),
            sLineUID  (csReqFldNULL),
            sVacType  (csReqFldNULL) {}

          void Clear();
        };
        typedef list<TSchemeInfo*> TPSchemeInfoList;

        struct TSchemeCpblt
        {
          UnicodeString       sVacType;            // ��� ����������
          TPSchemeInfoList PSchemeInfoList;     // ������ TSchemeInfo, ���������������
                                                // ������� ���� � ����� ����������
                                                // sVacType
          TSchemeInfo      NewSchemeInfo;       // �������� � ����� ������ �����

          void Clear();
        };
        typedef list<TSchemeCpblt> TSchemeCpbltList;

        enum TOption
        {
          oSchCpbltEditAll      // ���������� � ������� "������������ ����� ����������"
                                // �������� � ���� ����� ����������, ����� � �������
                                // ������������ �������� ������ � ��� �����, ������������
                                // ��� ������� ������� �� ����
        };

        // ���������
        typedef Set<TOption, oSchCpbltEditAll, oSchCpbltEditAll> TOptions;

        enum TCreationStage     // ���� ������������
        {
          csInit,               // �������������
          csGetData,            // ������� ������
          csSetCompatibility,   // ����������� ������������ ����� ������ ���� � ����� ����� ����
          csCreateMap           // ���������� �������
        };
        // ��������� ����� ������� ���������� ����� ������������ �������
        // AStage       - ���� ������������
        // AMaxProgress - max �������� ��� ���������� ���������
        typedef void __fastcall (__closure *TOnCreationBegin)(TObject* Sender, TCreationStage AStage, int AMaxProgress);
        // ��������� ��� ������������� �������� ��������� ��������� ����������
        // �������� ����� ������������ �������
        // ADelta - ��������, �� ������� ���������� ��������� ������� ������� ����������
        typedef void __fastcall (__closure *TOnProgressChange)(TObject* Sender, int ADelta);

        enum TCreationResult    // ��������� ������������
        {
          csOk,                 // Ok
          csCanceled,           // ������������ �������� �������������
                                // (� ������� "������������ ����� ����������")
          csTerminated          // ������������ �������� (������� Terminate)
        };


private:
        typedef list<TSchemeInfo> TSchemeInfoList;
        
        TStringList*      FContingent;
        TInfecList*       FInfecList;
        UnicodeString        FNewSchemeUID;
        TpFIBDatabase*    FDB;
        TUpdSchMapDM*     FDM;
        TTagNode*         FVacSchXML;
        TOptions          FOptions;
        bool              FCreationMode;
        bool              FTerminated;
        TSchemeInfoList*  FSchInfoList;
        TSchemeCpbltList* FSchCpbltList;

        TOnCreationBegin  FOnCreationBegin;
        TOnProgressChange FOnProgressChange;
        TNotifyEvent      FOnCreationComplete;


protected:
        struct SchemeInfoListLessVacType : public binary_function<
          TSchemeInfo,
          TSchemeInfo,
          bool
        >
        {
          bool operator() (TSchemeInfo x, TSchemeInfo y);
        };


protected:
        void __fastcall SetContingent(TStringList* AValue);
        void __fastcall SetInfecList(TInfecList* AValue);
        void __fastcall SetNewSchemeUID(UnicodeString AValue);
        void __fastcall SetDB(TpFIBDatabase* AValue);
        void __fastcall SetVacSchXML(TTagNode* AValue);
        void __fastcall SetOptions(TOptions AValue);
        inline void __fastcall ChangeStage(TCreationStage AStage, int AMaxProgress);
        inline void __fastcall IncProgress(int ADelta = 1);
        void __fastcall CheckInputs(UnicodeString AMethodName);
        void __fastcall CheckCreationMode(UnicodeString AMethodName, bool AReqModeValue);
        void __fastcall CheckBuffers(UnicodeString AMethodName);
        void __fastcall DoInitStage(int *pASchDataSize);
        void __fastcall DoGetDataStage(const int ASchDataSize);
        bool __fastcall DoSetCompatibilityStage();
        void __fastcall DoCreateMapStage(TUpdateSchemesMap *pAUpdateSchemesMap);
        TCreationResult __fastcall DoCreateUpdateSchemesMap(TUpdateSchemesMap *pAUpdateSchemesMap);


public:
        __fastcall TUpdateSchemesMapCreator();
        virtual __fastcall ~TUpdateSchemesMapCreator();

        // ���������� ���������
        //   String - �������� � ��������
        //   Object - ID ��������
        __property TStringList* Contingent = {read = FContingent, write = SetContingent};
        // ������ ID �������� (��. �������� TInfecList)
        __property TInfecList* InfecList = {read = FInfecList, write = SetInfecList};
        // UID ����� ������ �����
        __property UnicodeString NewSchemeUID = {read = FNewSchemeUID, write = SetNewSchemeUID};
        // ������� ���� ������
        __property TpFIBDatabase* DB = {read = FDB, write = SetDB};
        // xml-�������� �� ������� ���������� (���� imm-s)
        __property TTagNode* VacSchXML = {read = FVacSchXML, write = SetVacSchXML};
        // ���������
        __property TOptions Options = {read = FOptions, write = SetOptions};
        // ��. �������� TOnCreationBegin
        __property TOnCreationBegin OnCreationBegin = {read = FOnCreationBegin, write = FOnCreationBegin};
        // ��. �������� TOnProgressChange
        __property TOnProgressChange OnProgressChange = {read = FOnProgressChange, write = FOnProgressChange};
        // ��������� ��� ���������� ������������ �������
        __property TNotifyEvent OnCreationComplete = {read = FOnCreationComplete, write = FOnCreationComplete};

        // ��. cpp-����
        TCreationResult __fastcall CreateUpdateSchemesMap(TUpdateSchemesMap *pAUpdateSchemesMap);
        // ���������� ������������
        void __fastcall Terminate();
};

//###########################################################################
//##                                                                       ##
//##                       TSchemeCompatibilityEditor                      ##
//##                                                                       ##
//###########################################################################

// �������� ������������ ����� ����������

class TSchemeCompatibilityEditor : public TObject
{
        typedef TObject inherited;

private:
        typedef vector<TUpdateSchemesMapCreator::TSchemeCpblt*> TSchCpbltArr;

        UnicodeString     FNewSchemeUID;
        TpFIBDatabase* FDB;
        TSDM*          FDM;
        TTagNode*      FVacSchXML;
        TUpdateSchemesMapCreator::TSchemeCpbltList* FSchCpbltList;
        TUpdateSchemesMapCreator::TSchemeCpbltList* FSchCpbltListBuf;
        TSchCpbltArr*                               FSchCpbltArr;
        bool           FEditAll;
        bool           FEditMode;


protected:
        struct SelectSchCpbltNewSchVacType : public unary_function<
          TUpdateSchemesMapCreator::TSchemeCpblt,
          UnicodeString
        >
        {
          UnicodeString  operator()(const TUpdateSchemesMapCreator::TSchemeCpblt &x) const;
        };


protected:
        void __fastcall SetNewSchemeUID(UnicodeString AValue);
        void __fastcall SetDB(TpFIBDatabase* AValue);
        void __fastcall SetVacSchXML(TTagNode* AValue);
        void __fastcall SetEditAll(bool AValue);
        int __fastcall GetItemCount();
        UnicodeString __fastcall GetItemVacType(int AIndex);
        TUpdateSchemesMapCreator::TSchemeInfo __fastcall GetItemNewSchemeInfo(int AIndex);
        void __fastcall SetItemNewSchemeInfo(int AIndex, TUpdateSchemesMapCreator::TSchemeInfo AValue);
        void __fastcall CheckArr(const UnicodeString AMethodName);
        void __fastcall CheckInputs(const UnicodeString AMethodName);
        void __fastcall CheckEditMode(const UnicodeString AMethodName, bool AReqModeValue);


public:
        __fastcall TSchemeCompatibilityEditor();
        virtual __fastcall ~TSchemeCompatibilityEditor();

        // UID ����� ������ �����
        __property UnicodeString NewSchemeUID = {read = FNewSchemeUID, write = SetNewSchemeUID};
        // ������� ���� ������
        __property TpFIBDatabase* DB = {read = FDB, write = SetDB};
        // xml-�������� �� ������� ���������� (���� imm-s)
        __property TTagNode* VacSchXML = {read = FVacSchXML, write = SetVacSchXML};
        // ������ ������
        __property TSDM* DM = {read = FDM};
        // ������� ����������� � ������� "������������ ����� ����������"
        // �������� � ���� ����� ����������, ����� � �������
        // ������������ �������� ������ � ��� �����, ������������
        // ��� ������� ������� �� ����
        __property bool EditAll = {read = FEditAll, write = SetEditAll};

        // ��������� ������ ������������ ����� ����������
        void __fastcall SetSchCpbltList(TUpdateSchemesMapCreator::TSchemeCpbltList* AValue);

        // ���������� ��������� � ������ ������������ ����� ����������
        __property int __fastcall ItemCount = {read = GetItemCount};
        // ��� ���������� ��� �������� ������ ������������ ����� ���������� �
        // �������� AIndex
        __property UnicodeString ItemVacType[int AIndex] = {read = GetItemVacType};
        // �������� � ����� ���� ���������� ��� �������� ������ ������������
        // ����� ���������� � �������� AIndex
        __property TUpdateSchemesMapCreator::TSchemeInfo ItemNewSchemeInfo[int AIndex] = {
          read = GetItemNewSchemeInfo,
          write = SetItemNewSchemeInfo
        };

        // ��������������
        bool __fastcall Edit();
        // ����� ���������
        void __fastcall Cancel();
        // ���������� ���������
        void __fastcall Apply();
};

//---------------------------------------------------------------------------

} // end of namespace SchList
using namespace SchList;

#ifdef OLD_CODE_SUPPORT
  #include "DKOld\SchListOld.h"
#endif

#endif
