//---------------------------------------------------------------------------
#include <vcl.h>
//#include <stdio.h>
//#include <clipbrd.hpp>
#pragma hdrstop

#include "dsSchEditDMUnit.h"
#include "icsDateUtil.hpp"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//kabCDSGetValById("reg.s3 - ps1 ps2:ps3;
//kabCDSGetValById("reg.s4 - ps1 ps2:ps3#ps4
TdsSchEditDM *dsSchEditDM;
//---------------------------------------------------------------------------
const UnicodeString csNullRefLabel   = "�� �����������";
const UnicodeString csBeginLineLabel = "������";
const UnicodeString csEndLineLabel   = "���������";
const UnicodeString csEndPlanLabel   = "��������� ������������";
//---------------------------------------------------------------------------
__fastcall TdsSchEditDM::TdsSchEditDM(TComponent* Owner)
    : TDataModule(Owner)
{
  FClassDefNode = NULL;
//  FRegDefNode = NULL;
  FClassEditable = true;
  FFullEdit = true;
  FClassShowExpanded = "";
  FNativeStyle = true;
  FRequiredColor = clInfoBk;
//  FLFKind = lfStandard;
  FULEditWidth = 700;
  FExtSearchVacData = new TStringList;
  FPlanOpts = new TTagNode;
  FPlanOpts->AsXML = "<pls/>";
  FXMLPath = icsPrePath(GetEnvironmentVariable("APPDATA")+"\\ic\\xml");
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditDM::DataModuleCreate(TObject *Sender)
{
  FExtLPUCode = -1;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditDM::FLoadMapData(TJSONObject *AData, TAnsiStrMap &AMap, TAnsiStrMap &AMapCode)
{
  TJSONPairEnumerator *itPair;
  UnicodeString FClCode, FClStr;

  itPair = AData->GetEnumerator();
  while (itPair->MoveNext())
   {
     FClCode =((TJSONString*)itPair->Current->JsonString)->Value();
     FClStr  =((TJSONString*)itPair->Current->JsonValue)->Value();
     AMap[FClCode] = FClStr;
     AMapCode[FClStr] = FClCode;
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditDM::FLoadDependMapData(TJSONObject *AData, TIntListMap &AMap, TIntListMap &AMapCode)
{
  TJSONPairEnumerator *itPair;
  int FIntClCode, FIntClVal;
  TIntMap *vti;

  itPair = AData->GetEnumerator();
  while (itPair->MoveNext())
   {
     FIntClCode =((TJSONString*)itPair->Current->JsonString)->Value().ToIntDef(-1);
     FIntClVal  =((TJSONString*)itPair->Current->JsonValue)->Value().ToIntDef(-1);

     if (AMap.find(FIntClCode) == AMap.end())
      AMap[FIntClCode] = new TIntMap;
     vti = AMap[FIntClCode];
     (*vti)[FIntClVal] = 1;

     if (AMapCode.find(FIntClVal) == AMapCode.end())
      AMapCode[FIntClVal] = new TIntMap;
     vti = AMapCode[FIntClVal];
     (*vti)[FIntClCode] = 1;
   }
}
//---------------------------------------------------------------------------
TJSONObject* __fastcall TdsSchEditDM::kabCDSGetListById(UnicodeString AId)
{
  TJSONObject* RC = NULL;
  try
   {
     if (FOnGetListById)
      FOnGetListById(AId, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditDM::PreloadData()
{
  TJSONObject *RetData;
  UnicodeString FClCode, FClStr;
  TJSONPairEnumerator *itPair;

  int FIntClCode, FIntClVal;

  TIntMap *vti;
  TIntListMap::iterator RC;

/*
        DM->qtExec("Select CODE, R003C From CLASS_003A");
        while (!DM->quFree->Eof)
         {
           DM->quFree->Next();
         }
*/
  // ***** ������ �������� ******
//  RetData = kabCDSGetValById("reg.s2.Select CODE as ps1, R003C as ps2 From CLASS_003A", "0");
  FLoadMapData(kabCDSGetListById("reg.s2.Select CODE as ps1, R003C as ps2 From CLASS_003A"), InfList, InfListCode);
  /*
  itPair = RetData->GetEnumerator();
  while (itPair->MoveNext())
   {
     FClCode =((TJSONString*)itPair->Current->JsonString)->Value();
     FClStr  =((TJSONString*)itPair->Current->JsonValue)->Value();
     InfList[FClCode] = FClStr;
     InfListCode[FClStr] = FClCode;
   }
  */

  // ***** ������ ���� ******
//  RetData = kabCDSGetValById("reg.s2.Select CODE as ps1, R0040 as ps2 From CLASS_0035", "0");
//  FLoadMapData(RetData, VacList, VacListCode);
  FLoadMapData(kabCDSGetListById("reg.s2.Select CODE as ps1, R0040 as ps2 From CLASS_0035"), VacList, VacListCode);
/*
  itPair = RetData->GetEnumerator();
  while (itPair->MoveNext())
   {
     FClCode =((TJSONString*)itPair->Current->JsonString)->Value();
     FClStr  =((TJSONString*)itPair->Current->JsonValue)->Value();
     VacList[FClCode] = FClStr;
     VacListCode[FClStr] = FClCode;
   }
*/
  // ***** ������ ���� ******
//  RetData = kabCDSGetValById("reg.s2.Select CODE as ps1, R002C as ps2 From CLASS_002A", "0");
//  FLoadMapData(RetData, TestList, TestListCode);
  FLoadMapData(kabCDSGetListById("reg.s2.Select CODE as ps1, R002C as ps2 From CLASS_002A"), TestList, TestListCode);
/*
  itPair = RetData->GetEnumerator();
  while (itPair->MoveNext())
   {
     FClCode =((TJSONString*)itPair->Current->JsonString)->Value();
     FClStr  =((TJSONString*)itPair->Current->JsonValue)->Value();
     TestList[FClCode] = FClStr;
     TestListCode[FClStr] = FClCode;
   }
*/
  // ***** ������ �������� ******
//  RetData = kabCDSGetValById("reg.s2.Select CODE as ps1, R013F as ps2 From CLASS_013E", "0");
//  FLoadMapData(RetData, CheckList, CheckListCode);
  FLoadMapData(kabCDSGetListById("reg.s2.Select CODE as ps1, R013F as ps2 From CLASS_013E"), CheckList, CheckListCode);
/*
  itPair = RetData->GetEnumerator();
  while (itPair->MoveNext())
   {
     FClCode =((TJSONString*)itPair->Current->JsonString)->Value();
     FClStr  =((TJSONString*)itPair->Current->JsonValue)->Value();
     CheckList[FClCode] = FClStr;
   }
*/
  // ***** �������� �� �������� � ������� �� ��������� ******
//  RetData = kabCDSGetValById("reg.s2.Select R003E as ps1, R003B as ps2 From CLASS_002D", "0");
  FLoadDependMapData(kabCDSGetListById("reg.s2.Select R003E as ps1, R003B as ps2 From CLASS_002D"), InfVacList, VacInfList);
/*
  itPair = RetData->GetEnumerator();
  while (itPair->MoveNext())
   {
     FIntClCode =((TJSONString*)itPair->Current->JsonString)->Value().ToIntDef(-1);
     FIntClVal  =((TJSONString*)itPair->Current->JsonValue)->Value().ToIntDef(-1);

     if (InfVacList.find(FIntClCode) == InfVacList.end())
      InfVacList[FIntClCode] = new TIntMap;
     vti = InfVacList[FIntClCode];
     (*vti)[FIntClVal] = 1;

     if (VacInfList.find(FIntClVal) == VacInfList.end())
      VacInfList[FIntClVal] = new TIntMap;
     vti = VacInfList[FIntClVal];
     (*vti)[FIntClCode] = 1;
   }
*/
  // ***** �������� �� ������ � ����� �� ��������� ******
//  RetData = kabCDSGetValById("reg.s2.Select R007B as ps1, R007A as ps2 From CLASS_0079", "0");
//  FLoadDependMapData(RetData, InfTestList, TestInfList);
  FLoadDependMapData(kabCDSGetListById("reg.s2.Select R007B as ps1, R007A as ps2 From CLASS_0079"), InfTestList, TestInfList);
/*
  itPair = RetData->GetEnumerator();
  while (itPair->MoveNext())
   {
     FIntClCode =((TJSONString*)itPair->Current->JsonString)->Value().ToIntDef(-1);
     FIntClVal  =((TJSONString*)itPair->Current->JsonValue)->Value().ToIntDef(-1);

     if (InfTestList.find(FIntClCode) == InfTestList.end())
      InfTestList[FIntClCode] = new TIntMap;
     vti = InfTestList[FIntClCode];
     (*vti)[FIntClVal] = 1;

     if (TestInfList.find(FIntClVal) == TestInfList.end())
      TestInfList[FIntClVal] = new TIntMap;
     vti = TestInfList[FIntClVal];
     (*vti)[FIntClCode] = 1;
   }
*/
  // ***** �������� �� ��������� � �������� �� ��������� ******
//  RetData = kabCDSGetValById("reg.s2.Select R0140 as ps1, CODE as ps2 From CLASS_013E", "0");
//  FLoadDependMapData(RetData, InfCheckList, CheckInfList);
  FLoadDependMapData(kabCDSGetListById("reg.s2.Select R0140 as ps1, CODE as ps2 From CLASS_013E"), InfCheckList, CheckInfList);

  GetPlanOpts();
//  PreloadExtData();
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditDM::DataModuleDestroy(TObject *Sender)
{
  delete FPlanOpts; FPlanOpts = NULL;
  delete FExtSearchVacData;
/*
  delete FClassData;
  if (REG_DB->Connected) REG_DB->Close();
*/
}
//---------------------------------------------------------------------------
#include <rpc.h>
UnicodeString __fastcall TdsSchEditDM::NewGUID()
{
  UnicodeString RC = "";
  try
   {
     UUID FGUI;
     if (UuidCreate(&FGUI) == RPC_S_OK)
      {
        unsigned short * strGUID[1];
        if (UuidToString(&FGUI,strGUID) == RPC_S_OK)
         RC = UnicodeString(AnsiString((char*)(strGUID[0])));
      }
   }
  __finally
   {
   }
  if (RC.Trim().Length())
   return RC.Trim().UpperCase();
  else
   throw Exception("������ ������������ GUID");
}
//---------------------------------------------------------------------------
//TTagNode* __fastcall TdsRegDM::FGetRegDefNode()
//{
//  TTagNode *RC = FRegDefNode;
//  return RC;
//}
//---------------------------------------------------------------------------
/*
TTagNode* __fastcall TdsSchEditDM::FGetClassDef()
{
}
*/
//---------------------------------------------------------------------------
__int64 __fastcall TdsSchEditDM::kabCDSGetCount(UnicodeString AId, UnicodeString ATTH, UnicodeString AFilterParam)
{
  __int64 RC = 0;
  try
   {
//     if (FOnGetCount)
//      RC = FOnGetCount(AId, AFilterParam);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TJSONObject* __fastcall TdsSchEditDM::kabCDSGetCodes(UnicodeString AId, int AMax, UnicodeString ATTH, UnicodeString AFilterParam)
{
  TJSONObject* RC = NULL;
  try
   {
//     if (FOnGetIdList)
//      FOnGetIdList(AId, AMax, AFilterParam, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TJSONObject* __fastcall TdsSchEditDM::kabCDSGetValById(UnicodeString AId, UnicodeString ARecId)
{
  TJSONObject* RC = NULL;
  try
   {
     if (FOnGetValById)
      FOnGetValById(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TJSONObject* __fastcall TdsSchEditDM::kabCDSGetValById10(UnicodeString AId, UnicodeString ARecId)
{
  TJSONObject* RC = NULL;
  try
   {
     if (FOnGetValById10)
      FOnGetValById10(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditDM::kabCDSInsert(UnicodeString AId, TJSONObject* AValues, UnicodeString &ARecId)
{
  UnicodeString RC = "";
  try
   {
//     if (FOnInsertData)
//      FOnInsertData(AId, AValues, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditDM::kabCDSEdit(UnicodeString AId, TJSONObject* AValues, UnicodeString &ARecId)
{
  UnicodeString RC = "";
  try
   {
//     if (FOnEditData)
//      FOnEditData(AId, AValues, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditDM::kabCDSDelete(UnicodeString AId, UnicodeString ARecId)
{
  UnicodeString RC = "";
  try
   {
//     if (FOnDeleteData)
//      FOnDeleteData(AId, ARecId, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditDM::kabCDSGetClassXML(UnicodeString AId)
{
  UnicodeString RC = "";
  try
   {
     if (FOnGetClassXML)
       FOnGetClassXML(AId, RC);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditDM::kabCDSGetF63(UnicodeString AId)
{
  UnicodeString RC = "";
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditDM::OnGetXML(UnicodeString AId, TTagNode *& ARC)
{
  try
   {
     if (!FXMLList->XMLExist(AId))
      {
        TTagNode *tmp = new TTagNode;
        try
         {
           UnicodeString FFN = icsPrePath(FXMLPath+"\\"+AId+".xml");
           if (!FileExists(FFN))
            {
              ForceDirectories(FXMLPath);
              tmp->AsXML = kabCDSGetClassXML(AId);
              tmp->SaveToXMLFile(FFN,"");
            }
           FXMLList->Load(FFN,AId);
         }
        __finally
         {
           delete tmp;
         }
      }
     ARC = FXMLList->GetXML(AId);
   }
  __finally
   {
   }
}
//---------------------------------------------------------------------------
/*
UnicodeString __fastcall TdsSchEditDM::GetVacSch(TStringList *AInfList, UnicodeString AVacCode, __int64 AUCode, UnicodeString &AVacSch, UnicodeString &AVacSchTur)
{
  UnicodeString RC = "";
  TTagNode *FSettingNode = new TTagNode;
  try
   {
     AVacSch = "";
     AVacSchTur = "";
     FSettingNode->AsXML = GetPatSetting(AUCode);
     TTagNode *FSchList = FSettingNode->GetChildByName("sch", true);
     UnicodeString FCurrSch, FCurrTurSch;
     TTagNode *FSchNode;
     if (FSchList)
      {
        FCurrSch = "";
        FCurrTurSch = "";
        for (int i = 0; i < AInfList->Count; i++)
         {
           FSchNode = FSchList->GetChildByAV("spr","infref",IntToStr((int)AInfList->Objects[i]));
           if (FSchNode)
            {
              if (!FCurrSch.Length())
               {
                 FCurrSch = FSchNode->AV["vac"].UpperCase();
                 AVacSch  = FCurrSch;
               }
              else
               {
                 if (FCurrSch != FSchNode->AV["vac"].UpperCase())
                  {
                    FCurrSch = FSchNode->AV["vac"].UpperCase();
                    AVacSch += ", "+FCurrSch;
                  }
               }
              if (!FCurrTurSch.Length())
               {
                 FCurrTurSch = FSchNode->AV["tvac"].UpperCase();
                 AVacSchTur  = FCurrTurSch;
               }
              else
               {
                 if (FCurrTurSch != FSchNode->AV["tvac"].UpperCase())
                  {
                    FCurrTurSch = FSchNode->AV["tvac"].UpperCase();
                    AVacSchTur += ", "+FCurrTurSch;
                  }
               }
            }
         }
      }
   }
  __finally
   {
     delete FSettingNode;
   }
  return RC;
}
*/
//---------------------------------------------------------------------------
TTagNode* __fastcall TdsSchEditDM::FGetVacSchDef()
{
  TTagNode* RC = NULL;
  try
   {
     RC = FXMLList->GetXML("12063611-00008CD7-CD89");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TdsSchEditDM::FGetTestSchDef()
{
  TTagNode* RC = NULL;
  try
   {
     RC = FXMLList->GetXML("001D3500-00005882-DC28");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditDM::GetSchemeList(int AInfCode, TTagNodeMap &ASchList, bool AVacSch)
{
  TIntListMap::iterator RC = ((AVacSch)?InfVacList.find(AInfCode):InfTestList.find(AInfCode));
  if (RC != ((AVacSch)?InfVacList.end():InfTestList.end()))
   {
     TIntMap vti = *(RC->second);
     UnicodeString FVacCodes = "";
     for (TIntMap::iterator i = vti.begin(); i != vti.end(); i++ )
      {
        if (!FVacCodes.Length())
         FVacCodes = IntToStr(i->first);
        else
         FVacCodes += ","+IntToStr(i->first);
      }
     TTagNode *itSch = ((AVacSch)? VacSchDef->GetChildByName("schema",true) : TestSchDef->GetChildByName("schema",true));
     while (itSch)
      {
        if (itSch->CmpAV("ref",FVacCodes))
         ASchList[itSch->AV["uid"]] = itSch;
        itSch = itSch->GetNext();
      }
   }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditDM::FGetInfName(UnicodeString ACode)
{
  UnicodeString RC = "";
  try
   {
     TAnsiStrMap::iterator FRC = InfList.find(ACode);
     if (FRC != InfList.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditDM::FGetVacName(UnicodeString ACode)
{
  UnicodeString RC = "";
  try
   {
     TAnsiStrMap::iterator FRC = VacList.find(ACode);
     if (FRC != VacList.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditDM::FGetTestName(UnicodeString ACode)
{
  UnicodeString RC = "";
  try
   {
     TAnsiStrMap::iterator FRC = TestList.find(ACode);
     if (FRC != TestList.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditDM::FGetCheckName(UnicodeString ACode)
{
  UnicodeString RC = "";
  try
   {
     TAnsiStrMap::iterator FRC = CheckList.find(ACode);
     if (FRC != CheckList.end())
      RC = FRC->second;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditDM::GetSchName(UnicodeString ASchId, bool AVacSch)
{
  UnicodeString RC = "������";
  try
   {
     if ((ASchId.UpperCase() == "_NULL_") || !ASchId.Length())
      {
        RC = "";
      }
     else
      {
        TTagNode *FSch = (AVacSch)? VacSchDef->GetTagByUID(_GUI(ASchId)) : TestSchDef->GetTagByUID(_GUI(ASchId));
        TTagNode *FSchLine = (AVacSch)? VacSchDef->GetTagByUID(_UID(ASchId)) : TestSchDef->GetTagByUID(_UID(ASchId));
        if (FSch && FSchLine)
         {
           if (FSch->CmpName("schema") && FSchLine->CmpName("begline, line, endline") && FSchLine->GetParent("schema")->CmpAV("uid", FSch->AV["uid"]))
            {// _GUI(ASchId) ��������� �� �����, _UID(ASchId) ��������� �� ������ ����� _GUI(ASchId) - ��������� ������
              if (FSchLine->CmpName("begline"))
               RC = "������ �����";
              else if (FSchLine->CmpName("endline"))
               RC = "����� �����";
              else
               RC = FSchLine->AV["name"];

              RC = "{"+FSch->AV["name"]+"."+RC+"} " + ((AVacSch)? VacName[FSch->AV["ref"]]:TestName[FSch->AV["ref"]]);
            }
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditDM::AgeStr(TDateTime ACurDate, TDateTime ABirthDay)
{
  UnicodeString RC = "";
  try
   {
     Word FYear,FMonth,FDay;
     DateDiff(ACurDate, ABirthDay, FDay, FMonth, FYear);
     RC = SetDay(FYear,FMonth,FDay);
   }
  __finally
   {
   }
  return RC.Trim();
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditDM::SetDay(int AYear, int AMonth, int ADay )
{
  UnicodeString RC = "";
  try
   {
     UnicodeString sm = IntToStr(AYear);
     sm = sm.SubString(sm.Length(),1);
     if (((sm == "1")||(sm == "2")||(sm == "3")||(sm == "4"))&&!((AYear<15)&&(AYear>10))) sm = "�. ";
     else                                                    sm = "�. ";
     if (AYear)   RC += IntToStr(AYear)+sm;
     if (AMonth)  RC += IntToStr(AMonth)+"�. ";
     if (ADay)    RC += IntToStr(ADay)+"�.";
     if (!(AYear+AMonth+ADay))    RC += "0�.";
   }
  __finally
   {
   }
  return RC.Trim();
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditDM::DocPreview(UnicodeString AData)
{
}
//---------------------------------------------------------------------------
int __fastcall TdsSchEditDM::FillInfList(TStrings *AList)
{
  AList->Clear();
  for (TAnsiStrMap::iterator i = InfListCode.begin(); i != InfListCode.end(); i++)
    AList->AddObject(i->first, (TObject*)i->second.ToIntDef(0));
  return AList->Count;
}
//---------------------------------------------------------------------------
int __fastcall TdsSchEditDM::FillVacList(TStrings *AList, int AInf)
{
  AList->Clear();
  if (AInf)
   {
     TIntListMap::iterator FRC = InfVacList.find(AInf);
     if (FRC != InfVacList.end())
      {
        for (TAnsiStrMap::iterator i = VacListCode.begin(); i != VacListCode.end(); i++)
         {
           if (FRC->second->find(i->second.ToIntDef(0)) != FRC->second->end())
            AList->AddObject(i->first, (TObject*)i->second.ToIntDef(0));
         }
      }
   }
  else
   {
     for (TAnsiStrMap::iterator i = VacListCode.begin(); i != VacListCode.end(); i++)
       AList->AddObject(i->first, (TObject*)i->second.ToIntDef(0));
   }
  return AList->Count;
}
//---------------------------------------------------------------------------
int __fastcall TdsSchEditDM::FillTestList(TStrings *AList, int AInf)
{
  AList->Clear();
  if (AInf)
   {
     TIntListMap::iterator FRC = InfTestList.find(AInf);
     if (FRC != InfTestList.end())
      {
        for (TAnsiStrMap::iterator i = TestListCode.begin(); i != TestListCode.end(); i++)
         {
           if (FRC->second->find(i->second.ToIntDef(0)) != FRC->second->end())
            AList->AddObject(i->first, (TObject*)i->second.ToIntDef(0));
         }
      }
   }
  else
   {
     for (TAnsiStrMap::iterator i = TestListCode.begin(); i != TestListCode.end(); i++)
       AList->AddObject(i->first, (TObject*)i->second.ToIntDef(0));
   }
  return AList->Count;
}
//---------------------------------------------------------------------------
int __fastcall TdsSchEditDM::FillCheckList(TStrings *AList, int AInf)
{
  AList->Clear();
  if (AInf)
   {
   }
  else
   {
     for (TAnsiStrMap::iterator i = CheckListCode.begin(); i != CheckListCode.end(); i++)
       AList->AddObject(i->first, (TObject*)i->second.ToIntDef(0));
   }
  return AList->Count;
}
//---------------------------------------------------------------------------
int __fastcall TdsSchEditDM::FillVacInfList(TStrings *AList, int AVac)
{
  AList->Clear();
  if (AVac)
   {
     TIntListMap::iterator FRC = VacInfList.find(AVac);
     if (FRC != VacInfList.end())
      {
        for (TAnsiStrMap::iterator i = InfListCode.begin(); i != InfListCode.end(); i++)
         {
           if (FRC->second->find(i->second.ToIntDef(0)) != FRC->second->end())
            AList->AddObject(i->first, (TObject*)i->second.ToIntDef(0));
         }
      }
   }
  return AList->Count;
}
//---------------------------------------------------------------------------
int __fastcall TdsSchEditDM::FillTestInfList(TStrings *AList, int ATest)
{
  AList->Clear();
  if (ATest)
   {
     TIntListMap::iterator FRC = TestInfList.find(ATest);
     if (FRC != TestInfList.end())
      {
        for (TAnsiStrMap::iterator i = InfListCode.begin(); i != InfListCode.end(); i++)
         {
           if (FRC->second->find(i->second.ToIntDef(0)) != FRC->second->end())
            AList->AddObject(i->first, (TObject*)i->second.ToIntDef(0));
         }
      }
   }
  return AList->Count;
}
//---------------------------------------------------------------------------
int __fastcall TdsSchEditDM::FillCheckInfList(TStrings *AList, int ACheck)
{
  AList->Clear();
  if (ACheck)
   {
     TIntListMap::iterator FRC = CheckInfList.find(ACheck);
     if (FRC != CheckInfList.end())
      {
        for (TAnsiStrMap::iterator i = InfListCode.begin(); i != InfListCode.end(); i++)
         {
           if (FRC->second->find(i->second.ToIntDef(0)) != FRC->second->end())
            AList->AddObject(i->first, (TObject*)i->second.ToIntDef(0));
         }
      }
   }
  return AList->Count;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditDM::GetPlanOpts()
{
  if (FOnGetPlanOpts)
   {
     UnicodeString FOptsDef = "";
     if (FOnGetPlanOpts(FOptsDef))
      {
        try
         {
           FPlanOpts->AsXML = FOptsDef;
         }
        catch(Exception &E)
         {
           FPlanOpts->AsXML = "<pls/>";
         }
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall FToTree(TTagNode* ASrc,  TTreeNodes *ADestNodes, TTreeNode *ADest, TOnCopyItem AOnCopy)
{
  TTreeNode *FDest;
//  void*     xData;
//  UnicodeString xName;
  TTagNode *itNode = ASrc->GetFirstChild();
  while (itNode)
   {
//     xName = itNode->Name;
//     xData = NULL;
     FDest = ADestNodes->AddChildObject(ADest, itNode->Name, NULL);
     if (AOnCopy) AOnCopy(itNode, FDest);
     FToTree(itNode, ADestNodes, FDest, AOnCopy);
     itNode = itNode->GetNext();
   }
}
//---------------------------------------------------------------------------
void __fastcall CopyTagNodeToTree(TTagNode* ASrc,  TTreeNode *ADest, TOnCopyItem AOnCopy)
{
  TTreeNodes *FDestNodes = ((TTreeView*)ADest->TreeView)->Items;
  ADest->Text = ASrc->Name;
  ADest->Data = NULL;
  if (AOnCopy) AOnCopy(ASrc, ADest);
  FToTree(ASrc, FDestNodes, ADest, AOnCopy);
}
//---------------------------------------------------------------------------

