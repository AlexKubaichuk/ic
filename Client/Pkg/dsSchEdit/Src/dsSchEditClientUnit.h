//---------------------------------------------------------------------------
#ifndef dsSchEditClientUnitH
#define dsSchEditClientUnitH

//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include "cxClasses.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxGraphics.hpp"
#include "cxImageComboBox.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxPC.hpp"
#include "cxSplitter.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxBarExtItems.hpp"
#include "dxRibbon.hpp"
#include "dxRibbonCustomizationForm.hpp"
#include "dxRibbonSkins.hpp"
#include "dxStatusBar.hpp"
#include "Htmlview.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "dsSchEditDMUnit.h"
#include "DKClasses.h"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxEdit.hpp"
#include "cxMaskEdit.hpp"
//---------------------------------------------------------------------------
//###########################################################################
//##                                                                       ##
//##                             ESchListError                             ##
//##                                                                       ##
//###########################################################################

// ������������ ������, ����������� ��� ������������ ������ � TfmSchList

class ESchListError : public DKClasses::EMethodError
{
        typedef DKClasses::EMethodError inherited;

public:
        #pragma warn -inl
        /* Exception.Create */
        __fastcall ESchListError(
          const UnicodeString MethodName,
          const UnicodeString Description
        ) : DKClasses::EMethodError(MethodName, Description) {}
        /* TObject.Destroy */
        inline __fastcall virtual ~ESchListError(void) {}
        #pragma warn .inl
};
//---------------------------------------------------------------------------
class PACKAGE TdsSchEditForm : public TForm
{
__published:	// IDE-managed Components
        TdxBarManager *ClassTBM;
 TcxStyleRepository *Style;
        TcxStyle *BGStyle;
        TdxStatusBar *StatusSB;
        TcxStyle *cxStyle2;
  TdxBarLargeButton *dxBarLargeButton2;
  TdxBarLargeButton *dxBarLargeButton3;
  TdxBarLargeButton *dxBarLargeButton4;
  TdxBarLargeButton *dxBarLargeButton5;
  TdxBarLargeButton *dxBarLargeButton7;
  TdxBarLargeButton *dxBarLargeButton8;
  TdxBarLargeButton *dxBarLargeButton9;
  TdxBarLargeButton *dxBarLargeButton10;
  TcxImageList *LClassIL32;
  TcxImageList *ClassIL;
  TdxBarPopupMenu *QuickFilterMenu;
 TcxImageList *CondState;
  TcxPageControl *CardViewPC;
  TcxTabSheet *F63TS;
  TcxTabSheet *ComTS;
  TPanel *CompPanel;
  TcxSplitter *Split;
  TPanel *Panel1;
  THTMLViewer *F63Viewer;
  TcxStyle *cxStyle1;
 TcxStyle *cxStyle3;
 TPanel *Panel2;
 TcxStyle *cxStyle4;
 TcxStyle *cxStyle5;
 TcxStyle *FixedColStyle;
 TcxStyle *cxStyle7;
 TcxStyle *HeadStyle;
 TcxStyle *PlanStyle;
 TcxTreeList *SchTV;
 TActionList *ActionList;
 TAction *actAddScheme;
 TAction *actEditScheme;
 TAction *actCopyScheme;
 TAction *actDeleteScheme;
 TAction *actAddPriviv;
 TAction *actEditPriviv;
 TAction *actDeletePriviv;
 TAction *actApplyUpdates;
 TAction *actCancelUpdates;
 TAction *actClose;
 TAction *actCondListEdit;
 TcxTreeListColumn *SchSettingTVColumn1;
 TcxTreeListColumn *SchSettingTVColumn2;
 TcxTreeListColumn *SchSettingTVColumn3;
 TcxTreeListColumn *SchSettingTVColumn4;
 TcxTreeListColumn *SchSettingTVColumn5;
 TcxTreeListColumn *SchSettingTVColumn6;
 TcxTreeListColumn *SchSettingTVColumn7;
 TcxTreeListColumn *SchSettingTVColumn8;
 TdxBarLargeButton *dxBarLargeButton6;
 TdxBarLargeButton *dxBarLargeButton11;
 TdxBarLargeButton *dxBarLargeButton12;
 TPanel *RightPanel;
 TAction *actInfCBClear;
 TAction *actItemsCBClear;
 TdxBarLargeButton *dxBarLargeButton13;
 TdxBarPopupMenu *SchListPM;
 TdxBarPopupMenu *SchPM;
 TdxBarLargeButton *dxBarLargeButton1;
 TdxRibbon *RBar;
 TdxRibbonTab *SchListRBar;
 TdxRibbonTab *SchRBar;
 TdxBar *SchListBar;
 TdxBar *SchBar;
 TdxBarControlContainerItem *dxBarControlContainerItem1;
 TdxBarControlContainerItem *dxBarControlContainerItem2;
 TcxStyle *bStyle;
 TcxTreeList *SchListTL;
 TcxTreeListColumn *cxTreeList1Column1;
 TcxTreeListColumn *cxTreeList1Column2;
 TcxSplitter *cxSplitter1;
 TcxTreeList *ItemTL;
 TcxTreeListColumn *MIBPCol;
 TcxCheckBox *GroupByInf;
 TAction *actMIBPByAge;
 TdxBarButton *dxBarButton1;
        void __fastcall FormDestroy(TObject *Sender);
 void __fastcall SchSettingTVStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
 void __fastcall actAddSchemeExecute(TObject *Sender);
 void __fastcall actEditSchemeExecute(TObject *Sender);
 void __fastcall actCopySchemeExecute(TObject *Sender);
 void __fastcall actDeleteSchemeExecute(TObject *Sender);
 void __fastcall actAddPrivivExecute(TObject *Sender);
 void __fastcall actEditPrivivExecute(TObject *Sender);
 void __fastcall actDeletePrivivExecute(TObject *Sender);
 void __fastcall actApplyUpdatesExecute(TObject *Sender);
 void __fastcall actCancelUpdatesExecute(TObject *Sender);
 void __fastcall actCloseExecute(TObject *Sender);
 void __fastcall actCondListEditExecute(TObject *Sender);
 void __fastcall SchListTLSelectionChanged(TObject *Sender);
 void __fastcall SchListTLClick(TObject *Sender);
 void __fastcall SchListTLEnter(TObject *Sender);
 void __fastcall SchTVEnter(TObject *Sender);
 void __fastcall ItemTLFocusedNodeChanged(TcxCustomTreeList *Sender, TcxTreeListNode *APrevFocusedNode,
          TcxTreeListNode *AFocusedNode);
 void __fastcall ItemTLStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
 void __fastcall GroupByInfPropertiesChange(TObject *Sender);
 void __fastcall actMIBPByAgeExecute(TObject *Sender);
 void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);

private:	// User declarations
  struct TEditorBookmark
  {
    int nInfId;                   // Id ��������
    int nVacId;                   // Id �������
    UnicodeString sSchUID;           // UID �����
    UnicodeString sSchLineUID;       // UID ������ �����
    __fastcall TEditorBookmark() : nInfId(-1), nVacId(-1) {}
    void __fastcall Clear()
    {
      nInfId      = -1;
      nVacId      = -1;
      sSchUID     = "";
      sSchLineUID = "";
    }
  };
  TdsSchEditDM *FDM;
  UnicodeString FSchGUI;
  __int64            FCurInfCode, FCurVacCode;
  UnicodeString      FCurInfName, FCurVacName, FCurSchName, FCurSchLineName;

  TStrings*          FFilterList;         // ������ �������� ��� ��������� �� �������
  TSchemeObjType     FSchemeObjType;      // ��� �������� ������� ��� �����

  UnicodeString      FNmklGUI;            // ��� ������������
  bool               FXMLModified;        // ������� ������� ������������� ��������� � xml-���������
  TTagNode*          FIMM_S;              // ������������� xml-��������
  bool               FEditorActive;       // ������� ���������� ������
  bool               FIMM_SModified;      // ������� ������� ����������� ��������� � xml-���������
  TEditorBookmark*   FEditorBookmark;     // ��������, ������������ ������������ ���������

  bool               FValidateOnSave;     // ������� ��������� xml-��������� ��� ����������
  TOnLoadXML         FOnLoadXML;          // ��������� ��� ������������� �������� xml-���������
//  TOnSaveXML         FOnSaveXML;          // ��������� ��� ������������� ���������� xml-���������

  void __fastcall SetIMM_S(TTagNode* AValue);

  TTagNode* __fastcall GetActiveSchemeNode();
  bool __fastcall UpdateInternalRefs (TTagNode *ItTag, void *Src1, void *Src2, void *Src3, void *Src4);
  void __fastcall SetNmklGUI(UnicodeString AValue);

  //  TkabCustomDataSource *FDataSrc;
//  TkabCustomDataSource *FPrivDataSrc;
  TTagNode *FPrivNode;
  __int64 FUCode;
  UnicodeString _UID_;

  TcxTreeListNode* __fastcall FAddSchData(UnicodeString AStr, TTagNode *AData);
  void             __fastcall FUpdateSchData(UnicodeString AStr);
  TcxTreeListNode* __fastcall GetActiveSch();

  TcxTreeListNode* __fastcall GetCurSchLine();

  void __fastcall FGetDataProgress(int AMax, int ACur, UnicodeString AMsg);

  //  bool __fastcall CanEdit();

    bool __fastcall FSetEdit(TTagNode *itTag, UnicodeString &UID);
//    TRegistry_ICS* RegComp;
//    TRegIniFile  *RegIni;
    UnicodeString    SaveCaption;
    TcxTreeListNode *FRootNode;

  UnicodeString __fastcall GetCaptionText();
  bool __fastcall CurrSchIsMonoVac();
  bool __fastcall FillItemList();

    //    void __fastcall SetColumns(TcxGridCardView *AView, UnicodeString AColDef);
protected:
    // ������� ������� ������������� ��������� � xml-���������
    __property bool XMLModified = {read = FXMLModified, write = SetXMLModified };

  void __fastcall SetXMLModified( bool AValue );

  TCanDelSchRes __fastcall CanDeleteCurScheme();
  bool __fastcall GetValidPrivivTypes ( TStrings *strings );
  bool __fastcall GetValidProbTypes ( TStrings *strings );
  TCanDelPrivRes __fastcall CanDeleteActivePriviv();

  void __fastcall DeleteActivePriviv();
  bool __fastcall ApplyUpdates();
  bool __fastcall CancelUpdates();
  inline void __fastcall CheckActive(const UnicodeString MethodName) const;
  static TTagNode* __fastcall NewScheme(UnicodeString ASchemeName, int AVakId, TTagNode *AXML);
  void __fastcall UpdateSchemeActions();
  void __fastcall UpdatePrivivActions();
  void __fastcall UpdateSchemeStatusPanel();
  void __fastcall UpdatePrivivStatusPanel();
  bool __fastcall IfCurSchemeLinesRefsExist(TTagNode *ItTag, void *Src1 = NULL,void *Src2 = NULL,void *Src3 = NULL, void *Src4 = NULL);
  bool __fastcall SchemeLineDBRefsExist( TTagNode* SchemeLine );
  bool __fastcall EditLineInfo(TTagNode *ASrcNode, TcxTreeListNode *ADestNode);
  bool __fastcall AddLineInfo(TTagNode *ASrcNode, TcxTreeListNode *ADestNode);
  bool __fastcall UpdateLineJumpsInfo(TTagNode *ASrcNode, TcxTreeListNode *ADestNode);
  void __fastcall SetBookmark();
  void __fastcall ClearBookmark();
  bool __fastcall OpenEditor();
  bool __fastcall CloseEditor();
  void __fastcall UpdatePrivivList( TTagNode *SchemeNode );
  bool __fastcall FProgressProc(TkabProgressType AType, int AVal, UnicodeString AMsg);
  UnicodeString __fastcall PausePeriodText(TTagNode * ANode);

public:		// User declarations
    __fastcall TdsSchEditForm(TComponent* Owner, TdsSchEditDM *ADM, TSchemeObjType AType);

  TTagNode* __fastcall GetCurSchLineData();
  bool __fastcall AddScheme ( UnicodeString SchemeName, int nVakId, TTagNode *SchemeNode = NULL );
  void __fastcall EditScheme( UnicodeString SchemeName, int nVakId );
  bool __fastcall AddPriviv( TTagNode *LineNode, int nAfterInsertInd );
  bool __fastcall EditPriviv( TTagNode *LineNode );

  bool __fastcall GetSchList( TStrings *strings, const TIntSet VakIds, TGetSchListOptions GetOptions );
  bool __fastcall GetInfekVakList( TStrings *strings, int nInfekId );
  void __fastcall DeleteActiveScheme( bool bModifyXML = true );
  bool __fastcall SchemeNameExists( UnicodeString SchemeName, int nVakId = -1 );
  void __fastcall UpdateFilterList();

  void __fastcall SetActiveSchemeAttrVal( UnicodeString AttrName, UnicodeString AttrVal );
  UnicodeString __fastcall GetActiveSchemeAttrVal( UnicodeString AttrName );

  UnicodeString __fastcall GetCurVakName();
  bool __fastcall GetVakInfekList( TStrings *strings, int nVakId );
  bool __fastcall IfCurSchemeIsStart();
  bool __fastcall IsStartScheme( UnicodeString UID );
//  TTagNode* __fastcall GetActivePrivivNode();
  UnicodeString __fastcall GetInfekName(int nInfekId);
  bool __fastcall CanEdit();
  UnicodeString __fastcall GetFilterNameByID( int nId );
  static UnicodeString __fastcall GetPrivivFullName(
    TTagNode *AIMM_S,
    UnicodeString AVacName,
    UnicodeString APrivivUID
  );
  UnicodeString __fastcall GetPrivivFullName(UnicodeString APrivivUID);
  UnicodeString __fastcall GetVakName( int nVakId );
  bool __fastcall GetCurVakInfekList( TStrings *strings );
  static bool __fastcall ValidateXML(TSchemeObjType ASchemeObjType, TTagNode *AXML, TdsSchEditDM *ADM);
  static bool __fastcall ValidateSchemeExistence(TSchemeObjType ASchemeObjType, TTagNode *AXML, TdsSchEditDM *ADM);
  static bool __fastcall ValidateSchemeJumps(TSchemeObjType ASchemeObjType, TTagNode *AXML, TdsSchEditDM *ADM);


  int __fastcall GetCurVakId();

  // ������ �������� ��� ��������� �� �������
  // -- ������ ������ ������ ����� ��� "<������������ �������>=<��� �������>"
  //    <��� �������> - ���������� �����
  // -- ��������� �� �������� ������ ������ �� ��������������
  __property TStrings*      FilterList = {read = FFilterList};

  // ��� �������� ������� ��� �����
  __property TSchemeObjType SchemeObjType = {read = FSchemeObjType};

  // ������������� xml-��������
  __property TTagNode*      IMM_S = {read = FIMM_S, write = SetIMM_S};
  // ������� ������� ����������� ��������� � xml-���������
  __property bool           IMM_SModified = {read = FIMM_SModified};

  // ��� ������������
  __property UnicodeString        NmklGUI = {read = FNmklGUI, write = SetNmklGUI};

  // ��������� ��� ������������� �������� xml-���������
  __property TOnLoadXML     OnLoadXML = {read = FOnLoadXML, write = FOnLoadXML};
  // ��������� ��� ������������� ���������� xml-���������
//  __property TOnSaveXML     OnSaveXML = {read = FOnSaveXML, write = FOnSaveXML};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsSchEditForm *dsSchEditForm;
//---------------------------------------------------------------------------
#endif
