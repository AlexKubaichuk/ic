object fmRefLine: TfmRefLine
  Left = 486
  Top = 260
  BorderIcons = [biSystemMenu, biMaximize]
  BorderWidth = 5
  Caption = #1059#1089#1090#1072#1085#1086#1074#1082#1072' '#1087#1077#1088#1077#1093#1086#1076#1072
  ClientHeight = 303
  ClientWidth = 424
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001001010100000000000280100001600000028000000100000002000
    00000100040000000000C0000000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    000000000000000000000000000000000000000000000B00000000000000000E
    00BFBFB00000000E0BFBF0000000000E0FBFBFBF0000000E0BFBF0000000000E
    0FBFBFBFBFB0000E0BF000000000000000BFB000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000FFFF
    0000FFFF00001FFF0000041F0000000F0000000F000000070000000100000000
    000000010000003F0000FC7F0000FFFF0000FFFF0000FFFF0000FFFF0000}
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pBottom: TPanel
    Left = 0
    Top = 269
    Width = 424
    Height = 34
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 1
    object pCtrls: TPanel
      Left = 189
      Top = 0
      Width = 235
      Height = 34
      Align = alRight
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 0
      object bOk: TButton
        Left = 0
        Top = 6
        Width = 75
        Height = 25
        Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
        Default = True
        TabOrder = 0
        OnClick = bOkClick
      end
      object bCancel: TButton
        Left = 80
        Top = 6
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
        OnClick = bCancelClick
      end
      object bHelp: TButton
        Left = 160
        Top = 6
        Width = 75
        Height = 25
        Caption = '&'#1055#1086#1084#1086#1097#1100
        TabOrder = 2
        OnClick = bHelpClick
      end
    end
  end
  object tv: TTreeView
    Left = 0
    Top = 0
    Width = 424
    Height = 269
    Align = alClient
    AutoExpand = True
    HideSelection = False
    Indent = 19
    ReadOnly = True
    TabOrder = 0
    OnChange = tvChange
    OnCustomDrawItem = tvCustomDrawItem
  end
end
