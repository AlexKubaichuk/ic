/*
  ����        - CopiedSchName.cpp
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ ��� ����� ������������ ����������� ��� ����������� �����.
                ���� ����������
  ����������� - ������� �.�.
  ����        - 12.04.2004
*/
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DKMBUtils.h"

#include "dsCopiedSchName.h"

#include "dsVakSchList.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxButtonEdit"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                               Globals                                 ##
//##                                                                       ##
//###########################################################################

/*
 * ����� ����� � ��������� ������
 *
 *
 * ���������:
 *   [in]  Owner        - �������� ����� ( ����������� ������ ���� ����� fmSchList )
 *   [in]  AAutoSchName - ������������ �� ��������� ������������� ����������� ��� �����
 *
 * ������������ ��������:
 *   ��������� ��� �����
 *
 */

UnicodeString CopiedSchName::ShowModalCopiedSchName( TComponent* Owner, UnicodeString AAutoSchName )
{
  UnicodeString CopiedSchName = "";
  TfmCopiedSchName *dlg = new TfmCopiedSchName(Owner, AAutoSchName);
  if (dlg)
  {
    if ( dlg->ShowModal() == mrOk )
      CopiedSchName = dlg->SchName;
    delete dlg;
  }
  return CopiedSchName;
}

//###########################################################################
//##                                                                       ##
//##                           TfmCopiedSchName                            ##
//##                                                                       ##
//###########################################################################

//Owner - ����������� ������ ���� ����� fmSchList

__fastcall TfmCopiedSchName::TfmCopiedSchName(TComponent* Owner, UnicodeString AAutoSchName)
        : TForm(Owner)
{
  FSchListFM   = (TdsSchEditForm*)Owner;
  FSchName     = "";
  FAutoSchName = AAutoSchName;
}

//---------------------------------------------------------------------------

void __fastcall TfmCopiedSchName::FormShow(TObject *Sender)
{
  #ifdef HIDE_HELP_BUTTONS
    bHelp->Visible = false;
  #endif
  edName->Text = FAutoSchName;
//  edName->Color = AppOptions->ReqEdBkColor;
}

//---------------------------------------------------------------------------
bool __fastcall TfmCopiedSchName::AreInputsCorrect()
{
  if ( edName->Text == "" )
  {
	MB_SHOW_ERROR_CPT(L"���� \"������������\" ������ ���� ���������.", L"������ �����");
    return false;
  }
  if ( FSchListFM->SchemeNameExists( edName->Text ) )
  {
	MB_SHOW_ERROR_CPT( Format( UnicodeString("����� \"%s\" ��� ����."), &TVarRec(edName->Text), 0 ).c_str(), L"������ �����");
    return false;
  }
  return true;
}
//---------------------------------------------------------------------------
void __fastcall TfmCopiedSchName::bOkClick(TObject *Sender)
{
  edName->Text = Trim( edName->Text );
  if ( !AreInputsCorrect() )
    return;

  FSchName = edName->Text;
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TfmCopiedSchName::bCancelClick(TObject *Sender)
{
  ModalResult = mrCancel;
}
//---------------------------------------------------------------------------
void __fastcall TfmCopiedSchName::bHelpClick(TObject *Sender)
{
  Application->HelpContext( HelpContext );
}
//---------------------------------------------------------------------------
void __fastcall TfmCopiedSchName::edNamePropertiesButtonClick(TObject *Sender, int AButtonIndex)
{
  ShowModalVakSchList( FSchListFM, FSchListFM->GetCurVakId(), FSchListFM->GetCurVakName(), edName->Text );
}
//---------------------------------------------------------------------------

