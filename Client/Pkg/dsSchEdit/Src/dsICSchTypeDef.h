//---------------------------------------------------------------------------
#ifndef dsICSchTypeDefH
#define dsICSchTypeDefH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include "XMLContainer.h"
//---------------------------------------------------------------------------
enum class TSchemeObjType {None,Vac,Test,Check} ;
//---------------------------------------------------------------------------
typedef void __fastcall (__closure *TOnCopyItem)(TTagNode *TagNode, TTreeNode *TreeNode);
typedef bool __fastcall (__closure *TOnGetPlanOptsEvent)(UnicodeString &ARC);
typedef bool __fastcall (__closure *TOnSaveSchemeEvent)(TTagNode *TagNode, TSchemeObjType AType, TkabProgress AProgress);
//---------------------------------------------------------------------------
#endif

