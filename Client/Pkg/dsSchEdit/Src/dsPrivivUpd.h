/*
  ����        - PrivivUpd.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ �������������� �������� ��������.
                ������������ ����
  ����������� - ������� �.�.
  ����        - 13.03.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  26.02.2007
    [*] ������ � ���������� ���������� �������������� ����� ���������� ����������
        Dkglobals::AppOptions

  03.04.2006
    [!] ��� ��������� ��������� �������� ��� ��������
    [+] ���������� ��������� ��� �������� �� ������������� ��������

  18.03.2006
    [+] ����������� ��������� ��������
    [*] �����������

  28.02.2005
    1. ��������� ��������� �������� ����� pgm_setting.h

  18.02.2005
    1. ���� ������ �� ��������� ����������� ����� ��� ������� ���� ��������/�����

  13.03.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef dsPrivivUpdH
#define dsPrivivUpdH

//---------------------------------------------------------------------------

#include "DKCfg.h"   // ��������� �������� ����������
//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxGroupBox.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "dxBar.hpp"
//---------------------------------------------------------------------------
#include "dsSchEditClientUnit.h"
#include "dsDKGlobals.h"

/*
#include <Vcl.ActnList.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxGroupBox.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
#include <Vcl.ActnCtrls.hpp>
#include <Vcl.ActnMan.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.PlatformDefaultStyleActnCtrls.hpp>
#include <Vcl.ToolWin.hpp>
#include "cxClasses.hpp"
#include "dxBar.hpp"
//---------------------------------------------------------------------------
*/
namespace Dsprivivupd
{

//###########################################################################
//##                                                                       ##
//##                              Globals                                  ##
//##                                                                       ##
//###########################################################################

//��� �������������� ������ �����
enum TPrivivUpdMode {
  pmLine,       //������ �����
  pmEndLine     //��������� �����
};

//---------------------------------------------------------------------------

extern int ShowModalPrivivUpd(
  TComponent* Owner, TdsSchEditDM* ADM,
  TFormUpdateMode FUM,
  TPrivivUpdMode AMode,
  bool AISMonoVac = false,
  TStrings *strings = NULL
);

//###########################################################################
//##                                                                       ##
//##                            TfmPrivivUpd                               ##
//##                                                                       ##
//###########################################################################

// ������ �������������� �������� ��������

class TfmPrivivUpd : public TForm
{
__published:    // IDE-managed Components
 TPanel *BtnPanel;
    TPanel *Panel3;
    TButton *bApply;
    TButton *bCancel;
    TButton *bHelp;
    TActionList *ActionList;
    TAction *actAddSrokJump;
    TAction *actEditJump;
    TAction *actDeleteJump;
    TAction *actAddJump;
    TAction *actAddEndJump;
    TAction *actAddAgeJump;
    TPopupMenu *pmAddJump_Menu;
    TMenuItem *N5;
    TMenuItem *N6;
    TMenuItem *N7;
    TMenuItem *N8;
    TMenuItem *N9;
    TMenuItem *N10;
    TMenuItem *N11;
 TcxGroupBox *CommonGB;
 TLabel *lbPrivivTypeTitle;
 TLabel *lbMinPause;
 TLabel *lbMinAge;
 TLabel *Label2;
 TLabel *Label1;
 TComboBox *cbPrivivType;
 TcxGroupBox *SeasonGB;
 TLabel *lbSeasonBegTitle;
 TLabel *lbSeasonEndTitle;
 TComboBox *cbSeasonBeg;
 TComboBox *cbSeasonEnd;
 TcxGroupBox *JumpGB;
 TPanel *pJumps;
 TTreeView *tvJump;
 TcxMaskEdit *edMinPause;
 TcxMaskEdit *edMinAge;
 TcxImageList *LJumpIL;
 TdxBarManager *dxBarManager1;
 TdxBar *dxBarManager1Bar1;
 TdxBarDockControl *dxBarDockControl1;
 TdxBarLargeButton *dxBarLargeButton1;
 TdxBarLargeButton *dxBarLargeButton2;
 TdxBarLargeButton *dxBarLargeButton3;
 TdxBarPopupMenu *AddJumpPM;
 TdxBarButton *dxBarButton1;
 TdxBarButton *dxBarButton2;
 TdxBarButton *dxBarButton3;
 TdxBarButton *dxBarButton4;
 TcxImageList *JumpIL;
 TcxGroupBox *PlanLinkGB;
 TComboBox *PlanLinkCB;
 TdxBarSubItem *dxBarSubItem1;
 TcxMaskEdit *MinPauseVacTypeME;
 TLabel *Label4;
 TComboBox *PauseTypeCB;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall actEditJumpExecute(TObject *Sender);
    void __fastcall actDeleteJumpExecute(TObject *Sender);
    void __fastcall bCancelClick(TObject *Sender);
    void __fastcall tvJumpKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift);
    void __fastcall bApplyClick(TObject *Sender);
    void __fastcall tvJumpChange(TObject *Sender, TTreeNode *Node);
    void __fastcall tvJumpContextPopup(TObject *Sender,
      TPoint &MousePos, bool &Handled);
    void __fastcall actAddSrokJumpExecute(TObject *Sender);
    void __fastcall actAddEndJumpExecute(TObject *Sender);
    void __fastcall actAddJumpExecute(TObject *Sender);
    void __fastcall tvJumpCustomDrawItem(TCustomTreeView *Sender,
      TTreeNode *Node, TCustomDrawState State, bool &DefaultDraw);
    void __fastcall actAddAgeJumpExecute(TObject *Sender);
 void __fastcall edMinPausePropertiesChange(TObject *Sender);
 void __fastcall edMinPausePropertiesValidate(TObject *Sender, Variant &DisplayValue,
          TCaption &ErrorText, bool &Error);
 void __fastcall edMinAgePropertiesValidate(TObject *Sender, Variant &DisplayValue,
          TCaption &ErrorText, bool &Error);
 void __fastcall PauseTypeCBChange(TObject *Sender);


private:        // User declarations
    TFormUpdateMode  FFormUpdateMode;               //����� ������ �����
    TPrivivUpdMode   FMode;                         //��� �������������� ������ �����
    TdsSchEditDM*    FDM;
    TdsSchEditForm*  FOwnFM;                        //��������
    bool             FCanChangeMinPause;            //������� ������������ ��������� ���. �����
    const UnicodeString FMinPauseInitVal;              //��������� �������� ���. �����
    TTagNode*        FLine;                         //������ �����
    TNotifyEvent     FOldMinPauseChangeHandler;
    bool FISMonoVac;
    void DisableMinPauseOnChange()
    {
      FOldMinPauseChangeHandler = edMinPause->Properties->OnChange;
      edMinPause->Properties->OnChange = NULL;
    }

    void EnableMinPauseOnChange()
    {
      edMinPause->Properties->OnChange = FOldMinPauseChangeHandler;
    }

    bool __fastcall AreInputsCorrect();
    void __fastcall UpdateJumpActions();
    void __fastcall DrawButton(TTreeView *tv, TRect* ARect, TTreeNode* Node);
    void __fastcall DrawImage(TTreeView *tv, TRect* NodeRect, int ImageIndex);
  void __fastcall CheckFormat(Variant &DisplayValue, TCaption &ErrorText, bool &Error, UnicodeString AMsg);


public:         // User declarations
    __fastcall TfmPrivivUpd(TComponent* Owner, TdsSchEditDM* ADM, TFormUpdateMode FUM, TPrivivUpdMode AMode, TStrings *strings = NULL );
    virtual __fastcall ~TfmPrivivUpd();

    __property TdsSchEditForm* OwnFM = { read = FOwnFM };
    __property TTreeView* Jumps  = { read = tvJump };

    __property bool ISMonoVac  = { read = FISMonoVac, write=FISMonoVac};
    bool __fastcall GetPrivivInfekList(TStrings *strings);
    void __fastcall JumpsCopyItem(TTagNode *TagNode, TTreeNode *TreeNode);
    void __fastcall KillRefListNodes(TTreeNode *Root = NULL);
    void __fastcall ReferTreeNodeSort(TTreeNode *Root);
};

//---------------------------------------------------------------------------

} // end of namespace Dsprivivupd
using namespace Dsprivivupd;

#endif
