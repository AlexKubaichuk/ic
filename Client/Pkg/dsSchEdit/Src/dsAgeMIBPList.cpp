/*
 ����        - SchCondList.cpp
 ������      - ���������� ������������.
 ��������� "���������� ������������"
 ��������    - ������ �������������� ������ ������� ��� ����.
 ���� ����������
 ����������� - ������� �.�., �������� �.�.
 ����        - 26.02.2007
 */
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsAgeMIBPList.h"
#include "DKMBUtils.h"
#include "msgdef.h"
//#include "DKGlobals\DKFilterEdit.h"
//#include "DMF.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "cxClasses"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxGraphics"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma link "cxMaskEdit"
#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                             TCondListForm                             ##
//##                                                                       ##
//###########################################################################

__fastcall TAgeMIBPForm::TAgeMIBPForm(TComponent * Owner, TdsSchEditDM * ADM, TTagNode * AImm_S, int ASchUID)
    : TForm(Owner)
 {
  //if (!Owner->InheritsFrom(__classid(TdsSchEditForm)))
  //throw DKClasses::EInvalidArgument(__FUNC__, "Owner", "�������� ����� ������ ���� �������� TdsSchEditForm.");
  FDM   = ADM;
  FIMMS = AImm_S;
  TTagNode * AgeMIBPList = FIMMS->GetChildByName("mibpparams", true);
  TTagNode * AgeMIBParam;
  TStringList * FItemsList = new TStringList;
  MIBPList->BeginUpdate();
  MIBPList->Clear();
  try
   {
    TcxTreeListNode * FNode;
    FDM->FillVacList(FItemsList, 0);
    for (int i = 0; i < FItemsList->Count; i++)
     {
      FNode           = MIBPList->Root->AddChild();
      FNode->Texts[0] = FItemsList->Strings[i];
      FNode->Data     = FItemsList->Objects[i];
      if (AgeMIBPList)
       {
        AgeMIBParam = AgeMIBPList->GetChildByAV("m", "ref", IntToStr((int)FItemsList->Objects[i]));
        if (AgeMIBParam)
         {
          FNode->Texts[1] = AgeMIBParam->AV["f"];
          FNode->Texts[2] = AgeMIBParam->AV["t"];
         }
       }
     }
   }
  __finally
   {
    delete FItemsList;
    MIBPList->EndUpdate();
   }
 }

//---------------------------------------------------------------------------
void __fastcall TAgeMIBPForm::actApplyExecute(TObject * Sender)
 {
  bool ValExisit = false;
  for (int i = 0; (i < MIBPList->Root->Count) && !ValExisit; i++)
   ValExisit = MIBPList->Root->Items[i]->Texts[1].Trim().Length() || MIBPList->Root->Items[i]->Texts[2].Trim().Length();
  TTagNode * AgeMIBPList = FIMMS->GetChildByName("mibpparams", true);
  if (ValExisit)
   {
    if (!AgeMIBPList)
     AgeMIBPList = FIMMS->AddChild("mibpparams");

    TTagNode * AgeMIBParam;
    for (int i = 0; i < MIBPList->Root->Count; i++)
     {
      AgeMIBParam = AgeMIBPList->GetChildByAV("m", "ref", IntToStr((int)MIBPList->Root->Items[i]->Data));
      if (MIBPList->Root->Items[i]->Texts[1].Trim().Length() || MIBPList->Root->Items[i]->Texts[2].Trim().Length())
       {
        if (!AgeMIBParam)
         {
          AgeMIBParam            = AgeMIBPList->AddChild("m");
          AgeMIBParam->AV["ref"] = IntToStr((int)MIBPList->Root->Items[i]->Data);
         }

         AgeMIBParam->AV["f"] = MIBPList->Root->Items[i]->Texts[1].Trim();
         AgeMIBParam->AV["t"] = MIBPList->Root->Items[i]->Texts[2].Trim();
       }
      else if (AgeMIBParam)
       delete AgeMIBParam;
     }
   }
  else if (AgeMIBPList)
   delete AgeMIBPList;

  ModalResult = mrOk;
 }
//---------------------------------------------------------------------------
void __fastcall TAgeMIBPForm::CondListColumn1PropertiesValidate(TObject * Sender, Variant & DisplayValue,
    TCaption & ErrorText, bool & Error)
 {
  if (Error)
   {
    UnicodeString Val = VarToStr(DisplayValue);
    if (Val.Length())
     {
      UnicodeString v1;
      UnicodeString v2;

      v1 = GetLPartB(Val, '/');
      if (v1.Length())
       {
        Val = GetRPartB(Val, '/');
        v2  = GetLPartB(Val, '/');
        if (v2.Length())
         DisplayValue = v1 + "/" + v2 + "/0";
        else if (Val.Length())
         DisplayValue = v1 + "/" + Val + "/0";
        else
         DisplayValue = v1 + "/0/0";
       }
      else
       DisplayValue = Val + "/0/0";
     }
    Error = false;
   }
 }
//---------------------------------------------------------------------------
