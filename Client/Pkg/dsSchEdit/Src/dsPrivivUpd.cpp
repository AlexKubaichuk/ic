/*
 ����        - PrivivUpd.cpp
 ������      - ���������� ������������.
 ��������� "���������� ������������"
 ��������    - ������ �������������� �������� ��������.
 ���� ����������
 ����������� - ������� �.�.
 ����        - 13.03.2004
 */
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DKMBUtils.h"

#include "dsPrivivUpd.h"

#include "dsJumpUpd.h"
#include "dsRefLine.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxGroupBox"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "dxBar"

#pragma resource "*.dfm"

//###########################################################################
//##                                                                       ##
//##                              Globals                                  ##
//##                                                                       ##
//###########################################################################

/*
 * ����� ����� � ��������� ������
 *
 *
 * ���������:
 *   [in]  Owner   - �������� ( fmSchList )
 *   [in]  FUM     - ����� ������ �����
 *   [in]  AMode   - ��� �������������� ������ �����
 *   [in]  strings - ���������� ���� �������� (��� �������� ������ �����)
 *
 * ������������ ��������:
 *   ModalResult �����
 *
 */

int Dsprivivupd::ShowModalPrivivUpd(
    TComponent * Owner,
    TdsSchEditDM * ADM,
    TFormUpdateMode FUM,
    TPrivivUpdMode AMode,
    bool AISMonoVac,
    TStrings * strings
    )
 {
  int nModalResult = mrNone;
  TfmPrivivUpd * dlg = new TfmPrivivUpd(Owner, ADM, FUM, AMode, strings);
  if (dlg)
   {
    dlg->ISMonoVac = AISMonoVac;
    nModalResult   = dlg->ShowModal();
    delete dlg;
   }
  return nModalResult;
 }

//---------------------------------------------------------------------------

namespace Dsprivivupd
 {

 int CALLBACK ReferTreeNodeCompare(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
  {
   int fCompareRes = 0;
   TTreeNode * Node1 = reinterpret_cast<TTreeNode *>(lParam1);
   TTreeNode * Node2 = reinterpret_cast<TTreeNode *>(lParam2);
   TdsSchEditForm * SchListFM = reinterpret_cast<TdsSchEditForm *>(lParamSort);
   if (Node1->Data && Node2->Data && SchListFM)
    {
     TTagNode * TagNode1 = reinterpret_cast<TTagNode *>(Node1->Data);
     TTagNode * TagNode2 = reinterpret_cast<TTagNode *>(Node2->Data);
     if (TagNode1->CmpName("refer") && TagNode2->CmpName("refer"))
      {
       UnicodeString Vak1Name = SchListFM->GetInfekName(TagNode1->AV["infref"].ToInt());
       UnicodeString Vak2Name = SchListFM->GetInfekName(TagNode2->AV["infref"].ToInt());
       if (Vak1Name < Vak2Name)
        fCompareRes = -1;
       else if (Vak1Name > Vak2Name)
        fCompareRes = 1;
      }
    }
   return fCompareRes;
  }

 }//end of namespace PrivivUpd

//###########################################################################
//##                                                                       ##
//##                            TfmPrivivUpd                               ##
//##                                                                       ##
//###########################################################################

__fastcall TfmPrivivUpd::TfmPrivivUpd(TComponent * Owner, TdsSchEditDM * ADM, TFormUpdateMode FUM, TPrivivUpdMode AMode, TStrings * strings)
    : TForm(Owner), FMinPauseInitVal("0/0/0")
 {
  FDM             = ADM;
  FFormUpdateMode = FUM;
  FMode           = AMode;
  if ((FFormUpdateMode == fumADD) && (FMode == pmEndLine))
   throw DKClasses::EMethodError(__FUNC__, "������ ��������� ��������� �����.");
  FOwnFM                    = (TdsSchEditForm *)Owner;
  FCanChangeMinPause        = true;
  FOldMinPauseChangeHandler = NULL;
  FLine                     = new TTagNode(NULL);
  FLine->Name               = "line";

  cbPrivivType->Clear();
  if (strings)
   cbPrivivType->Items->AddStrings(strings);
 }

//---------------------------------------------------------------------------

__fastcall TfmPrivivUpd::~TfmPrivivUpd()
 {
  delete FLine;
 }

//---------------------------------------------------------------------------

void __fastcall TfmPrivivUpd::FormShow(TObject * Sender)
 {
#ifdef HIDE_HELP_BUTTONS
  bHelp->Visible = false;
#endif

  cbPrivivType->Color      = clInfoBk; //AppOptions->ReqEdBkColor;
  edMinPause->Style->Color = clInfoBk; //AppOptions->ReqEdBkColor;

  if (!FOwnFM->CanEdit())
   bApply->Visible = false;

  //������������� control'�� ��� ����������
  cbSeasonBeg->Items->Add("");
  cbSeasonEnd->Items->Add("");
  for (int i = 0; i < RusMonthCount; i++)
   {
    cbSeasonBeg->Items->Add(TDate("01." + IntToStr(i + 1) + ".2000").FormatString("mmmm")/*RusLongMonthNames[i]*/);
    cbSeasonEnd->Items->Add(RusLongMonthNames[i]);
   }

  //����������� �������� ���� ����� � ����������� �� ������
  switch (FFormUpdateMode)
   {
   case fumADD:
    switch (FOwnFM->SchemeObjType)
     {
     case TSchemeObjType::Vac:
      Caption = "���������� �������� ���� ��������";
      PlanLinkGB->Visible = FISMonoVac;
      break;
     case TSchemeObjType::Test:
      Caption = "���������� �������� ���� �����";
      PlanLinkGB->Visible = false;
      break;
     case TSchemeObjType::Check:
      Caption = "���������� �������� ���� ��������";
      PlanLinkGB->Visible = false;
      break;
     }
    //lbPrivivType->Visible = false;
    if (cbPrivivType->Items->Count)
     cbPrivivType->ItemIndex = 0;
    edMinPause->Text       = FMinPauseInitVal;
    cbSeasonBeg->ItemIndex = 0;
    cbSeasonEnd->ItemIndex = 0;
    //if ( FOwnFM->GetCurSchemePrivivCount() < 2 )
    //FCanChangeMinPause = false;
    break;
   case fumEDIT:
    int nSeasonBeg;
    int nSeasonEnd;
    FLine->Assign(FOwnFM->GetCurSchLineData(), true);
    switch (FMode)
     {
     case pmLine:
       {
        switch (FOwnFM->SchemeObjType)
         {
         case TSchemeObjType::Vac:
          Caption = "��������� �������� ���� ��������";
          PlanLinkGB->Visible = FISMonoVac;
          //�� ���������� ��������
          PauseTypeCB->ItemIndex = 1;
          break;
         case TSchemeObjType::Test:
          Caption = "��������� �������� ���� �����";
          PlanLinkGB->Visible = false;
          //�� ���������� �����
          PauseTypeCB->ItemIndex = 2;
          break;
         case TSchemeObjType::Check:
          Caption = "��������� �������� ���� ��������";
          PlanLinkGB->Visible = false;
          //�� ���������� ��������
          PauseTypeCB->ItemIndex = 3;
          break;
         }

        if (PlanLinkGB->Visible)
         PlanLinkCB->ItemIndex = FLine->AV["link"].ToIntDef(-1);
        cbPrivivType->Items->Add(FLine->AV["name"]);
        cbPrivivType->ItemIndex = 0;
        cbPrivivType->Enabled   = false;
        edMinPause->Text        = FLine->AV["min_pause"];

        UnicodeString pvt = FLine->AV["min_pause_vactype"].Trim();
        if (pvt.Length())
         {
          if (pvt.Pos(":"))
           {
            PauseTypeCB->ItemIndex  = GetLPartB(pvt, ':').ToInt();
            MinPauseVacTypeME->Text = GetRPartB(pvt, ':');
           }
          else
           MinPauseVacTypeME->Text = pvt;
          MinPauseVacTypeME->Enabled = true;
         }

        edMinAge->Text = FLine->AV["min_age"];
        nSeasonBeg = StrToIntDef(FLine->AV["season_beg"], -1);
        nSeasonEnd = StrToIntDef(FLine->AV["season_end"], -1);
        if ((nSeasonBeg != -1) && (nSeasonEnd != -1))
         {
          cbSeasonBeg->ItemIndex = nSeasonBeg + 1;
          cbSeasonEnd->ItemIndex = nSeasonEnd + 1;
         }
        else
         {
          cbSeasonBeg->ItemIndex = 0;
          cbSeasonEnd->ItemIndex = 0;
         }
        break;
       }
     case pmEndLine:
      Caption = "��������� ��������� �� ��������� �����";
      //Height = Height - pInputs->Height;
      SeasonGB->Visible   = false;
      CommonGB->Visible   = false;
      PlanLinkGB->Visible = false;
      //pInputs->Visible = false;
      actAddSrokJump->Enabled = false;
      break;
     }
    break;
   }

  tvJump->Items->BeginUpdate();
  TTreeNode * RootNode = tvJump->Items->AddObject(NULL, "", NULL);

  //���������� ������ � ����������
  CopyTagNodeToTree(FLine, RootNode, JumpsCopyItem);

  KillRefListNodes();

  TTreeNode * JumpTreeNode = RootNode->getFirstChild();
  while (JumpTreeNode)
   {
    ReferTreeNodeSort(JumpTreeNode);
    JumpTreeNode = JumpTreeNode->getNextSibling();
   }
  tvJump->Select(RootNode);
  RootNode->Expand(true);
  tvJump->Items->EndUpdate();

  UpdateJumpActions();
 }

//---------------------------------------------------------------------------

//�������� �����, ��������������� ���� "reflist"

void __fastcall TfmPrivivUpd::KillRefListNodes(TTreeNode * Root)
 {
  if (Root)
   for (int i = 0; i < Root->Count; i++)
    {
     TTagNode * TagNode = (TTagNode *)Root->Item[i]->Data;
     if (TagNode)
      if (TagNode->CmpName("reflist"))
       Root->Item[i]->Delete();
    }
  else
   for (int i = 0; i < tvJump->Items->Count; i++)
    {
     TTagNode * TagNode = (TTagNode *)tvJump->Items->Item[i]->Data;
     if (TagNode)
      if (TagNode->CmpName("reflist"))
       tvJump->Items->Item[i]->Delete();
    }
 }

//---------------------------------------------------------------------------

//����������� TagNode �� TreeNode

void __fastcall TfmPrivivUpd::JumpsCopyItem(TTagNode * TagNode, TTreeNode * TreeNode)
 {
  TreeNode->Data = TagNode;

  if (TagNode->CmpName("line,endline"))
   {
    TreeNode->Text = "��������";
   }

  else if (TagNode->CmpName("srokjump"))
   {
    TreeNode->Text = "�� ����� (" + XMLStrDurationToStr(TagNode->AV["actualtime"]) + ")";
   }

  else if (TagNode->CmpName("agejump"))
   {
    TreeNode->Text = "�� �������� (" + XMLStrDurationToStr(TagNode->AV["max_age"]) + ")";
   }

  else if (TagNode->CmpName("jump"))
   {
    TreeNode->Text = "�� ������� (" + FOwnFM->GetFilterNameByID(UIDInt(TagNode->AV["ref"])) + ")";
   }

  else if (TagNode->CmpName("endjump"))
   {
    TreeNode->Text = "�����������";
   }

  else if (TagNode->CmpName("refer"))
   {
    UnicodeString VakName = FOwnFM->GetInfekName(StrToInt(TagNode->AV["infref"]));
    UnicodeString PrivivFullName = FOwnFM->GetPrivivFullName(TagNode->AV["ref"]);
    TreeNode->Text = VakName + " -> " + PrivivFullName;

    //����������� ������ �� ������� ���� ( �.�. � ����������� ����, ��������������� ���� "reflist",
    //����� ������� )
    if (
        TreeNode->Parent &&
        TreeNode->Parent->Data &&
        reinterpret_cast<TTagNode *>(TreeNode->Parent->Data)->CmpName("reflist")
        )
     TreeNode->MoveTo(TreeNode->Parent->Parent, naAddChild);
   }
 }

//---------------------------------------------------------------------------

void __fastcall TfmPrivivUpd::ReferTreeNodeSort(TTreeNode * Root)
 {
  Root->CustomSort(ReferTreeNodeCompare, reinterpret_cast<int>(FOwnFM), false);
 }

//---------------------------------------------------------------------------

bool __fastcall TfmPrivivUpd::AreInputsCorrect()
 {
  if (FMode == pmEndLine)
   return true;

  if (FFormUpdateMode == fumADD)
   if (cbPrivivType->ItemIndex == -1)
    {
     switch (FOwnFM->SchemeObjType)
      {
      case TSchemeObjType::Vac:
       MB_SHOW_ERROR_CPT(L"���������� ������� ��� �������� �� ������.", L"������ �����");
       break;
      case TSchemeObjType::Test:
       MB_SHOW_ERROR_CPT(L"���������� ������� ��� ����� �� ������.", L"������ �����");
       break;
      case TSchemeObjType::Check:
       MB_SHOW_ERROR_CPT(L"���������� ������� ��� �������� �� ������.", L"������ �����");
       break;
      }
     return false;
    }

  UnicodeString ExAgeString;

  if (!IsCorrectAgeString(edMinPause->Text, ExAgeString, false))
   {
    MB_SHOW_ERROR_CPT(L"����������� ����� ������ ���� ������� � ������� \"���/��/��\".\n"
        L"���\t - ���������� ��� ( 0 - 999 )\n"
        L"��\t - ���������� ������� ( 0 - 12 )\n"
        L"��\t - ���������� ���� ( 0 - 31 ).",
        L"������ �����");
    return false;
   }
  edMinPause->Text = ExAgeString;

  if (!IsCorrectAgeString(edMinAge->Text, ExAgeString))
   {
    MB_SHOW_ERROR_CPT(L"����������� ������� ������ ���� ������ � ������� \"���/��/��\".\n"
        L"���\t - ���������� ��� ( 0 - 999 )\n"
        L"��\t - ���������� ������� ( 0 - 12 )\n"
        L"��\t - ���������� ���� ( 0 - 31 ).",
        L"������ �����");
    return false;
   }
  edMinAge->Text = ExAgeString;

  if (!(((cbSeasonBeg->ItemIndex > 0) && (cbSeasonEnd->ItemIndex > 0)) ||
      ((cbSeasonBeg->ItemIndex == 0) && (cbSeasonEnd->ItemIndex == 0) &&
      (cbSeasonBeg->Text == "") && (cbSeasonEnd->Text == ""))))
   {
    MB_SHOW_ERROR_CPT(L"��� ���������� ������ ���� ������� ����������� � ������������ ������� ���������. ��������� ������ ���� ������� �� ������.", L"������ �����");
    return false;
   }

  return true;
 }

//---------------------------------------------------------------------------

bool __fastcall TfmPrivivUpd::GetPrivivInfekList(TStrings * strings)
 {
  return FOwnFM->GetCurVakInfekList(strings);
 }

//---------------------------------------------------------------------------

void __fastcall TfmPrivivUpd::UpdateJumpActions()
 {
  if (FMode == pmLine)
   actAddSrokJump->Enabled = (bool) !FLine->GetChildByName("srokjump");
  actAddAgeJump->Enabled = (bool) !FLine->GetChildByName("agejump");
  actAddEndJump->Enabled = (bool) !FLine->GetChildByName("endjump");
 }

//---------------------------------------------------------------------------

void __fastcall TfmPrivivUpd::actEditJumpExecute(TObject * Sender)
 {
  TTagNode * TagNode = reinterpret_cast<TTagNode *>(tvJump->Selected->Data);
  if (TagNode)
   {
    if (TagNode->CmpName("refer"))
     {
      UnicodeString sNewRef = ShowModalRefLine(
          FOwnFM,
          TagNode->AV["infref"].ToInt(),
          FOwnFM->GetInfekName(TagNode->AV["infref"].ToInt()),
          TagNode->AV["ref"]
          );
      if (sNewRef.Length())
       {
        TagNode->AV["ref"] = sNewRef;
        //����������� TagNode �� TreeNode
        CopyTagNodeToTree(TagNode, tvJump->Selected, JumpsCopyItem);
        /*
         TagNode->OnCopyItem = JumpsCopyItem;
         TagNode->ToTree( tvJump->Selected );
         TagNode->OnCopyItem = NULL;
         */
       }
     }
    else
     {
      TJumpKind JumpKind = jkUnknown;
      if (TagNode->CmpName("srokjump"))
       JumpKind = jkSrok;
      else if (TagNode->CmpName("agejump"))
       JumpKind = jkAge;
      else if (TagNode->CmpName("jump"))
       JumpKind = jkCond;
      else if (TagNode->CmpName("endjump"))
       JumpKind = jkEnd;
      if (ShowModalJumpUpd(this, FDM, fumEDIT, JumpKind) == mrOk)
       UpdateJumpActions();
     }
   }
 }

//---------------------------------------------------------------------------

void __fastcall TfmPrivivUpd::actDeleteJumpExecute(TObject * Sender)
 {
  if (MB_SHOW_QUESTION_CPT(L"�� ������� � ���, ��� ������ ������� ��������� �������?",
      L"������������� ��������") == IDYES)
   {
    delete((TTagNode *)tvJump->Selected->Data);
    tvJump->Selected->Delete();
    UpdateJumpActions();
   }
 }

//---------------------------------------------------------------------------

void __fastcall TfmPrivivUpd::bCancelClick(TObject * Sender)
 {
  ModalResult = mrCancel;
 }

//---------------------------------------------------------------------------

void __fastcall TfmPrivivUpd::tvJumpKeyDown(TObject * Sender, WORD & Key,
    TShiftState Shift)
 {
  ExecuteGridAction(Key,
      Shift,
      actAddJump,
      actEditJump,
      actDeleteJump,
      0,
      TShiftState(),
      NULL);
 }
//---------------------------------------------------------------------------

void __fastcall TfmPrivivUpd::bApplyClick(TObject * Sender)
 {
  if (!FOwnFM->CanEdit())
   return;

  if (!AreInputsCorrect())
   return;

  switch (FFormUpdateMode)
   {
   case fumADD:
    FLine->AV["uid"] = FOwnFM->IMM_S->NewUID();
    FLine->AV["name"]              = cbPrivivType->Items->Strings[cbPrivivType->ItemIndex];
    FLine->AV["min_pause"]         = edMinPause->Text;
    FLine->AV["min_pause_vactype"] = IntToStr(PauseTypeCB->ItemIndex) + ":" + MinPauseVacTypeME->Text;

    FLine->AV["min_age"] = edMinAge->Text;

    if (PlanLinkGB->Visible)
     {
      FLine->AV["link"] = "0";
      if (PlanLinkCB->ItemIndex != -1)
       FLine->AV["link"] = IntToStr(PlanLinkCB->ItemIndex);
     }

    if (cbSeasonBeg->ItemIndex > 0)
     {
      FLine->AV["season_beg"] = IntToStr(cbSeasonBeg->ItemIndex - 1);
      FLine->AV["season_end"] = IntToStr(cbSeasonEnd->ItemIndex - 1);
     }
    else
     {
      FLine->AV["season_beg"] = "";
      FLine->AV["season_end"] = "";
     }
    FOwnFM->AddPriviv(FLine, (int)cbPrivivType->Items->Objects[cbPrivivType->ItemIndex]);
    break;
   case fumEDIT:
    switch (FMode)
     {
     case pmLine:
      FLine->AV["min_pause"] = edMinPause->Text;
      FLine->AV["min_pause_vactype"] = IntToStr(PauseTypeCB->ItemIndex) + ":" + MinPauseVacTypeME->Text;
      FLine->AV["min_age"]           = edMinAge->Text;

      FLine->AV["link"] = "0";
      if (PlanLinkCB->ItemIndex != -1)
       FLine->AV["link"] = IntToStr(PlanLinkCB->ItemIndex);

      if (cbSeasonBeg->ItemIndex > 0)
       {
        FLine->AV["season_beg"] = IntToStr(cbSeasonBeg->ItemIndex - 1);
        FLine->AV["season_end"] = IntToStr(cbSeasonEnd->ItemIndex - 1);
       }
      else
       {
        FLine->AV["season_beg"] = "";
        FLine->AV["season_end"] = "";
       }
      break;
     case pmEndLine:
      break;
     }
    FOwnFM->EditPriviv(FLine);
    break;
   }
  ModalResult = mrOk;
 }

//---------------------------------------------------------------------------

void __fastcall TfmPrivivUpd::tvJumpChange(TObject * Sender,
    TTreeNode * Node)
 {
  TTagNode * TagNode = reinterpret_cast<TTagNode *>(Node->Data);
  bool bModifEn = (Node->Level == 1);
  actEditJump->Enabled   = bModifEn || (TagNode && TagNode->CmpName("refer"));
  actDeleteJump->Enabled = bModifEn;
 }

//---------------------------------------------------------------------------

void __fastcall TfmPrivivUpd::tvJumpContextPopup(TObject * Sender,
    TPoint & MousePos, bool & Handled)
 {
  //������� ������������� ��� ������� ������ ������ ����
  if (!((MousePos.x == -1) && (MousePos.y == -1)))
   {
    TTreeNode * SelectedNode = tvJump->Selected;
    SelectedNode->Selected = true;
    SelectedNode->Focused  = true;
   }
 }

//---------------------------------------------------------------------------
void __fastcall TfmPrivivUpd::actAddSrokJumpExecute(TObject * Sender)
 {
  if (ShowModalJumpUpd(this, FDM, fumADD, jkSrok) == mrOk)
   UpdateJumpActions();
 }

//---------------------------------------------------------------------------

void __fastcall TfmPrivivUpd::actAddAgeJumpExecute(TObject * Sender)
 {
  if (ShowModalJumpUpd(this, FDM, fumADD, jkAge) == mrOk)
   UpdateJumpActions();
 }

//---------------------------------------------------------------------------

void __fastcall TfmPrivivUpd::actAddEndJumpExecute(TObject * Sender)
 {
  if (ShowModalJumpUpd(this, FDM, fumADD, jkEnd) == mrOk)
   UpdateJumpActions();
 }

//---------------------------------------------------------------------------

void __fastcall TfmPrivivUpd::actAddJumpExecute(TObject * Sender)
 {
  ShowModalJumpUpd(this, FDM, fumADD, jkCond);
 }

//---------------------------------------------------------------------------
void __fastcall TfmPrivivUpd::tvJumpCustomDrawItem(TCustomTreeView * Sender,
    TTreeNode * Node, TCustomDrawState State, bool & DefaultDraw)
 {
  TTagNode * TagNode = (TTagNode *)Node->Data;
  if (TagNode)
   if (TagNode->CmpName("srokjump,agejump,jump,endjump"))
    Sender->Canvas->Font->Style = TFontStyles() << fsBold;
  DefaultDraw = true;
 }
//---------------------------------------------------------------------------
void __fastcall TfmPrivivUpd::edMinPausePropertiesChange(TObject * Sender)
 {
  if (!FCanChangeMinPause)
   {
    switch (FOwnFM->SchemeObjType)
     {
     case TSchemeObjType::Vac:
      MB_SHOW_ERROR_CPT(L"������ �������� ����������� ����� ��� ������� ���� ��������.", L"������ �����");
      break;
     case TSchemeObjType::Test:
      MB_SHOW_ERROR_CPT(L"������ �������� ����������� ����� ��� ������� ���� �����.", L"������ �����");
      break;
     case TSchemeObjType::Check:
      MB_SHOW_ERROR_CPT(L"������ �������� ����������� ����� ��� ������� ���� �������� .", L"������ �����");
      break;
     }
    DisableMinPauseOnChange();
    edMinPause->Text = FMinPauseInitVal;
    EnableMinPauseOnChange();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TfmPrivivUpd::CheckFormat(Variant & DisplayValue, TCaption & ErrorText, bool & Error, UnicodeString  AMsg)
 {
  if (Error)
   {
    UnicodeString tmpVal = VarToStr(DisplayValue).Trim();
    Error = false;
    if (!tmpVal.LastDelimiter("/"))
     {//����� ���
      DisplayValue = Variant(tmpVal + "/0/0");
     }
    else
     {
      if (tmpVal.Pos("/") == tmpVal.LastDelimiter("/"))
       {//����� ���+"/"+�������?
        if (tmpVal.LastDelimiter("/") == tmpVal.Length())
         {//����� ���+"/"
          DisplayValue = Variant(tmpVal + "0/0");
         }
        else
         {//����� ���+"/"+�������
          DisplayValue = Variant(tmpVal + "/0");
         }
       }
      else
       {
        if (tmpVal.LastDelimiter("/") == tmpVal.Length())
         {//����� ���+"/"+�������+"/"
          DisplayValue = Variant(tmpVal + "0");
         }
        else
         {//�������, �� �� ������ ���� ���������
          Error     = true;
          ErrorText = AMsg;
         }
       }
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TfmPrivivUpd::edMinPausePropertiesValidate(TObject * Sender, Variant & DisplayValue,
    TCaption & ErrorText, bool & Error)
 {
  CheckFormat(DisplayValue, ErrorText, Error, "������� ������������ �������� ���. �����.");
 }
//---------------------------------------------------------------------------
void __fastcall TfmPrivivUpd::edMinAgePropertiesValidate(TObject * Sender, Variant & DisplayValue,
    TCaption & ErrorText, bool & Error)
 {
  CheckFormat(DisplayValue, ErrorText, Error, "������� ������������ �������� ���. ��������.");
 }
//---------------------------------------------------------------------------
void __fastcall TfmPrivivUpd::PauseTypeCBChange(TObject * Sender)
 {
  if (PauseTypeCB->ItemIndex == -1)
   {
    switch (FOwnFM->SchemeObjType)
     {
     case TSchemeObjType::Vac:
      PauseTypeCB->ItemIndex = 1;
      break;
     case TSchemeObjType::Test:
      PauseTypeCB->ItemIndex = 2;
      break;
     case TSchemeObjType::Check:
      PauseTypeCB->ItemIndex = 3;
      break;
     }
   }
  else
   {
    //�� ��������� �������� / ����� / ��������
    //�� ��������� ��������
    //�� ��������� �����
    //�� ��������� ��������
    //�� ��������:
    //�� �����:
    //�� ��������:
    if (PauseTypeCB->ItemIndex > 3)
     MinPauseVacTypeME->Enabled = true;
    else
     {
      MinPauseVacTypeME->Enabled = false;
      MinPauseVacTypeME->Text    = "";
     }
   }
 }
//---------------------------------------------------------------------------
