/*
  ����        - JumpUpd.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ �������������� ��������� �� ����� ����.
                ������������ ����
  ����������� - ������� �.�., �������� �.�.
  ����        - 13.04.2004
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  26.02.2007
    [*] ������ � ���������� ���������� �������������� ����� ���������� ����������
        Dkglobals::AppOptions
    [*] kab

  03.04.2006
    [+] ��������� ���������� ��������� ��� �������� �� ������������� ��������

  18.03.2006
    [+] ��������� �������� �� ��������� ������������
    [*] ����������

  01.12.2005 - kab
    [*] ����� "��������" � ���������� �������� �� "���������� ��"

  13.04.2004
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef dsJumpUpdH
#define dsJumpUpdH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������
//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.ValEdit.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
#include "dsDKGlobals.h"
#include "dsPrivivUpd.h"
//---------------------------------------------------------------------------
/*
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.ValEdit.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
*/
namespace Dsjumpupd
{

//###########################################################################
//##                                                                       ##
//##                              Globals                                  ##
//##                                                                       ##
//###########################################################################

extern int ShowModalJumpUpd( TComponent* Owner, TdsSchEditDM* ADM, TFormUpdateMode AFUM, TJumpKind AJumpKind );

//###########################################################################
//##                                                                       ##
//##                            TfmJumpUpd                                 ##
//##                                                                       ##
//###########################################################################

// ������ �������������� ��������� �� ����� ����
class TfmJumpUpd : public TForm
{
__published:    // IDE-managed Components
        TGroupBox *gbClient;
        TPanel *pBottom;
        TPanel *pCtrls;
        TPanel *pTop;
        TPanel *pRefs;
        TBevel *Bevel;
        TLabel *lbActualTimeTitle;
        TEdit *edActualTime;
        TValueListEditor *vlRefList;
        TLabel *lbRefListTitle;
        TActionList *ActionList;
        TAction *actEditRef;
        TLabel *lbFilterTitle;
        TcxComboBox *cbFilter;
        TcxButton *btnConditionListEdit;
        TcxButton *bEdit;
        TcxButton *bHelp;
        TcxButton *bCancel;
        TcxButton *bApply;
    TPopupMenu *pmRefList;
    TMenuItem *N1;
        void __fastcall bCancelClick(TObject *Sender);
        void __fastcall bApplyClick(TObject *Sender);
        void __fastcall bHelpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall actEditRefExecute(TObject *Sender);
        void __fastcall vlMouseWheelDown(TObject *Sender,
          TShiftState Shift, TPoint &MousePos, bool &Handled);
        void __fastcall vlMouseWheelUp(TObject *Sender,
          TShiftState Shift, TPoint &MousePos, bool &Handled);
        void __fastcall vlKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall vlDrawCell(TObject *Sender, int ACol,
          int ARow, TRect &Rect, TGridDrawState State);
        void __fastcall vlMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall vlDblClick(TObject *Sender);
        void __fastcall btnConditionListEditClick(TObject *Sender);


private:        // User declarations
  TdsSchEditDM* FDM;

        TfmPrivivUpd*   FOwnFM;         //��������
        TdsSchEditForm*     FTopOwnFM;      //����� "������ ����"
        TFormUpdateMode FFUM;           //������ ������ ����� - ���������� ��� ����������� ������ ��������
        TJumpKind       FJumpKind;      //��� ��������

        bool __fastcall AreInputsCorrect();
        void __fastcall UpdateActions();
        int  __fastcall GetRefListStrInd( int nInfekInd );
        void __fastcall SetVLEditorSelection( TValueListEditor *vl, int nTop, int nBottom, int nLeft, int nRight );
        int  __fastcall MoveVLEditorSelectionPrev ( TValueListEditor *vl );
        int  __fastcall MoveVLEditorSelectionNext ( TValueListEditor *vl );
        int  __fastcall MoveVLEditorSelectionFirst( TValueListEditor *vl );
        int  __fastcall MoveVLEditorSelectionLast ( TValueListEditor *vl );
        int  __fastcall MoveVLEditorSelectionPgUp ( TValueListEditor *vl );
        int  __fastcall MoveVLEditorSelectionPgDwn( TValueListEditor *vl );

        void __fastcall SetVLEditorEnable( TValueListEditor *vl, bool bEnable );
        void __fastcall DisableVLEditor( TValueListEditor *vl ) { SetVLEditorEnable( vl, false ); }
        void __fastcall EnableVLEditor ( TValueListEditor *vl ) { SetVLEditorEnable( vl, true  ); }


public:         // User declarations
        __fastcall TfmJumpUpd(TComponent* Owner, TdsSchEditDM* ADM, TFormUpdateMode AFUM, TJumpKind AJumpKind);
        virtual __fastcall ~TfmJumpUpd();

        __property TfmPrivivUpd* OwnFM = { read = FOwnFM };
};

//---------------------------------------------------------------------------

} // end of namespace JumpUpd
using namespace Dsjumpupd;

#endif
