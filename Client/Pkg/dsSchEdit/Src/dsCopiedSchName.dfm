object fmCopiedSchName: TfmCopiedSchName
  Left = 512
  Top = 315
  BorderStyle = bsDialog
  BorderWidth = 5
  Caption = #1042#1074#1086#1076' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1103' '#1089#1093#1077#1084#1099
  ClientHeight = 95
  ClientWidth = 297
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gbClient: TGroupBox
    Left = 0
    Top = 0
    Width = 297
    Height = 61
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 186
      Height = 13
      Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1089#1086#1079#1076#1072#1074#1072#1077#1084#1086#1081' '#1089#1093#1077#1084#1099':'
    end
    object edName: TcxButtonEdit
      Left = 3
      Top = 34
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = edNamePropertiesButtonClick
      TabOrder = 0
      Text = 'edName'
      Width = 291
    end
  end
  object pBottom: TPanel
    Left = 0
    Top = 61
    Width = 297
    Height = 34
    Align = alBottom
    AutoSize = True
    BevelOuter = bvNone
    TabOrder = 1
    object pCtrls: TPanel
      Left = 62
      Top = 0
      Width = 235
      Height = 34
      Align = alRight
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 0
      object bOk: TButton
        Left = 0
        Top = 6
        Width = 75
        Height = 25
        Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
        Default = True
        TabOrder = 0
        OnClick = bOkClick
      end
      object bCancel: TButton
        Left = 80
        Top = 6
        Width = 75
        Height = 25
        Cancel = True
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
        OnClick = bCancelClick
      end
      object bHelp: TButton
        Left = 160
        Top = 6
        Width = 75
        Height = 25
        Caption = '&'#1055#1086#1084#1086#1097#1100
        TabOrder = 2
        OnClick = bHelpClick
      end
    end
  end
end
