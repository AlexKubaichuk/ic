/*
  ����        - SchCondList.h
  ������      - ���������� ������������.
                ��������� "���������� ������������"
  ��������    - ������ �������������� ������ ������� ��� ����.
                ������������ ����
  ����������� - ������� �.�., �������� �.�.
  ����        - 26.02.2007
*/
//---------------------------------------------------------------------------
/*
  ������� ���������:

  26.02.2007
    �������������� �������
*/
//---------------------------------------------------------------------------

#ifndef dsSchCondListH
#define dsSchCondListH

//---------------------------------------------------------------------------

#include "DKCfg.h"      // ��������� �������� ����������

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxListBox.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <ActnList.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include "XMLContainer.h"
//#include "ICSDocFilter.h"
#include "FilterEditEx.h"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
#include <System.Actions.hpp>
#include "dsSchEditClientUnit.h"
#include "cxClasses.hpp"
#include "dxBar.hpp"
#include "dxSkinsdxBarPainter.hpp"
#include <Vcl.ImgList.hpp>

#include "dsSchEditDMUnit.h"
#include "cxCustomData.hpp"
#include "cxImageComboBox.hpp"
#include "cxInplaceContainer.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxSkinOffice2010Blue.hpp"
#include "dxSkinValentine.hpp"
#include "dxSkinXmas2008Blue.hpp"

//namespace SchList
//{
//class TdsSchEditForm;
//}

namespace SchCondList
{

//###########################################################################
//##                                                                       ##
//##                             TCondListForm                             ##
//##                                                                       ##
//###########################################################################

// ������ �������������� ������ ������� ��� ����

class TCondListForm : public TForm
{
__published:    // IDE-managed Components
 TActionList *MainAL;
    TAction *actInsert;
    TAction *actEdit;
    TAction *actDelete;
 TcxImageList *LMainIL;
 TdxBarManager *ClassTBM;
 TdxBar *ClassTBMBar1;
 TdxBarLargeButton *dxBarLargeButton1;
 TdxBarLargeButton *dxBarLargeButton2;
 TdxBarLargeButton *dxBarLargeButton3;
 TdxBarLargeButton *dxBarLargeButton4;
 TPopupMenu *PMenu;
 TMenuItem *InsertItem;
 TMenuItem *ModifyItem;
 TMenuItem *DeleteItem;
 TcxTreeList *CondList;
 TcxTreeListColumn *CondNameCol;
    void __fastcall actInsertExecute(TObject *Sender);
    void __fastcall actEditExecute(TObject *Sender);
    void __fastcall actDeleteExecute(TObject *Sender);


private:        // User declarations
    TdsSchEditDM* FDM;
    TTagNode*   FIMMS;
    bool        FEdit;
    bool        FUpdate;
  TcxTreeListNode* __fastcall GetActiveCond();
  TTagNode* __fastcall GetActiveCondData();


protected:
    void __fastcall UpdateActions();
    bool __fastcall FilterEditCanClose(TFilterEditExForm *Sender, UnicodeString ACondName);
    UnicodeString __fastcall FGetMsgTxt(TFilterEditMsgType AType, UnicodeString ADefMsg);


public:         // User declarations
    __fastcall TCondListForm(TComponent* Owner, TdsSchEditDM* ADM, TTagNode *AImm_S, int ASchUID);
    __property bool UpdateList = {read=FUpdate};
};

//---------------------------------------------------------------------------

} // end of namespace SchCondList
using namespace SchCondList;

#endif
