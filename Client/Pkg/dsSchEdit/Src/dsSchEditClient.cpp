//---------------------------------------------------------------------------

#include <basepch.h>

#pragma hdrstop

#include "dsSchEditClient.h"

//#include "DxLocale.h"

#include "dsSchEditDMUnit.h"
//#include "ICSRegList.h"
//#include "ICSRegSelList.h"
//#include "Reg_DMF.h"
//#include "ICSRegProgress.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//

static inline void ValidCtrCheck(TdsSchEditClient *)
{
  new TdsSchEditClient(NULL);
}
//---------------------------------------------------------------------------
__fastcall TdsSchEditClient::TdsSchEditClient(TComponent* Owner)
        : TComponent(Owner)
{
  FUseClassDisControls = false;
  FUseReportSetting = false;
  FDBUser     = "";
  FDBPassword = "";
  FDBCharset  = "";
  RegistryKey = "Software\\ICS Ltd.\\ClassifSize";
  FUnitListCreated = NULL;
  FBuyCaption = "";
//  FLargeIcons = false;
//  FFetchAll = true;
  FUnitListDeleted = NULL;
//  FOnExtFilterSet = NULL;
//  FOnCtrlDataChange = NULL;

  FRefreshList = false;
  FClassTreeColor = clBtnFace;
  FDTFormat = "dd.mm.yyyy";
  FDM = new TdsSchEditDM(this);
  FActive = true;
  FClassEnabled = true;

}
//---------------------------------------------------------------------------
__fastcall TdsSchEditClient::~TdsSchEditClient()
{
  for (TSchEditMap::iterator i = FSchEditMap.begin(); i != FSchEditMap.end(); i++)
   delete i->second;
  FSchEditMap.clear();
  Active = false;
  delete FDM;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetFullEdit(bool AFullEdit)
{
  FFullEdit = AFullEdit;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetActive(bool AActive)
{
/*
*/
 FActive = AActive;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetClassEnabled(bool AClassEnabled)
{
/*
  FClassEnabled = AClassEnabled;
  if (FDM)
   if (FDM->UnitList) ((TICSRegListForm*)FDM->UnitList)->CheckAccess();
*/
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetListEditEnabled(bool AListEditEnabled)
{
/*
  FListEditEnabled = AListEditEnabled;
  if (FDM)
   if (FDM->UnitList) ((TICSRegListForm*)FDM->UnitList)->CheckAccess();
*/
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOpenCardEnabled(bool AOpenCardEnabled)
{
/*
  FOpenCardEnabled = AOpenCardEnabled;
  if (FDM)
   if (FDM->UnitList) ((TICSRegListForm*)FDM->UnitList)->CheckAccess();
*/
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetUListTop(int ATop)
{
  FUListTop = ATop;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetUListLeft(int ALeft)
{
  FUListLeft = ALeft;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetDBName(UnicodeString  ADBName)
{
  FDBName = ADBName;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetXMLName(UnicodeString  AXMLName)
{
  FXMLName = AXMLName;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::ShowSch(TSchemeObjType AType)
{
  if (!FActive) return;
  if (!FClassEnabled) return;
  TSchEditMap::iterator FRC = FSchEditMap.find(AType);
  if (FRC == FSchEditMap.end())
   {
     FSchEditMap[AType] = new TdsSchEditForm(Application->Owner, FDM, AType);
   }
  else
   {
     delete FRC->second;
     FSchEditMap[AType] = new TdsSchEditForm(Application->Owner, FDM, AType);
   }
//  FSchEditMap[AUCode]->SetActiveCard("1003");
  FSchEditMap[AType]->Show();
}
//---------------------------------------------------------------------------
void  __fastcall TdsSchEditClient::FSetInqComp(TControl *AInqComp)
{
  FInqComp  = AInqComp;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetInit(int APersent, UnicodeString AMess)
{
/*
  if (APersent == 0) ((TICSRegProgressForm*)PrF)->SetCaption(AMess);
  ((TICSRegProgressForm*)PrF)->SetPercent(APersent);
*/
}
//---------------------------------------------------------------------------
/*
TdxBar*  __fastcall TdsSchEditClient::GetListExtTB(void)
{
  if (FDM->UnitList) return ((TICSRegListForm*)FDM->UnitList)->GetExtTB();
  else              return NULL;
}
//---------------------------------------------------------------------------
*/
void __fastcall TdsSchEditClient::FSetDTFormat(UnicodeString  AParam)
{
  FDTFormat = AParam;
}
//---------------------------------------------------------------------------
bool __fastcall TdsSchEditClient::FGetClassEditable()
{
  bool RC = false;
  try
   {
     if (FDM)
      RC = FDM->ClassEditable;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetClassEditable(bool AVal)
{
  if (FDM)
    FDM->ClassEditable = AVal;
}
//---------------------------------------------------------------------------
UnicodeString  __fastcall TdsSchEditClient::FGetClassShowExpanded()
{
  UnicodeString RC = "";
  try
   {
     if (FDM)
      RC = FDM->ClassShowExpanded;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetClassShowExpanded(UnicodeString AVal)
{
  if (FDM)
    FDM->ClassShowExpanded = AVal;
}
//---------------------------------------------------------------------------
/*TExtEditData __fastcall TdsSchEditClient::FGetExtInsertData()
{

  if (FDM)
   return FDM->OnExtInsertData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetExtInsertData(TExtEditData AValue)
{
  if (FDM)
   FDM->OnExtInsertData = AValue;
}  */
//---------------------------------------------------------------------------
/*TExtEditData __fastcall TdsSchEditClient::FGetExtEditData()
{

  if (FDM)
   return FDM->OnExtEditData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetExtEditData(TExtEditData AValue)
{
  if (FDM)
   FDM->OnExtEditData = AValue;
}*/
//---------------------------------------------------------------------------
/*TClassEditEvent __fastcall TdsSchEditClient::FGetClassAfterInsert()
{
  if (FDM)
   return FDM->OnClassAfterInsert;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetClassAfterInsert(TClassEditEvent AValue)
{
  if (FDM)
   FDM->OnClassAfterInsert = AValue;
}*/
//---------------------------------------------------------------------------
/*TClassEditEvent __fastcall TdsSchEditClient::FGetClassAfterEdit()
{
  if (FDM)
   return FDM->OnClassAfterEdit;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetClassAfterEdit(TClassEditEvent AValue)
{
  if (FDM)
   FDM->OnClassAfterEdit = AValue;
}*/
//---------------------------------------------------------------------------
/*TClassEditEvent __fastcall TdsSchEditClient::FGetClassAfterDelete()
{
  if (FDM)
   return FDM->OnClassAfterDelete;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetClassAfterDelete(TClassEditEvent AValue)
{
  if (FDM)
   FDM->OnClassAfterDelete = AValue;
}*/
//---------------------------------------------------------------------------
/*TUnitEvent __fastcall TdsSchEditClient::FGetOnOpenCard()
{
  if (FDM)
   return FDM->OnOpenCard;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnOpenCard(TUnitEvent AValue)
{
  if (FDM)
   FDM->OnOpenCard = AValue;
}*/
//---------------------------------------------------------------------------
/*TGetReportListEvent __fastcall TdsSchEditClient::FGetQuickFilterList()
{
  if (FDM)
   return FDM->OnQuickFilterList;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetQuickFilterList(TGetReportListEvent AValue)
{
  if (FDM)
   FDM->OnQuickFilterList = AValue;
} */
//---------------------------------------------------------------------------
/*TGetReportListEvent __fastcall TdsSchEditClient::FGetReportList()
{
  if (FDM)
   return FDM->OnReportList;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetReportList(TGetReportListEvent AValue)
{
  if (FDM)
   FDM->OnReportList = AValue;
}  */
//---------------------------------------------------------------------------
/*TUnitEvent __fastcall TdsSchEditClient::FGetPrintReportClick()
{
  if (FDM)
   return FDM->OnPrintReportClick;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetPrintReportClick(TUnitEvent AValue)
{
  if (FDM)
   FDM->OnPrintReportClick = AValue;
} */
//---------------------------------------------------------------------------
//TdsICClassClient* __fastcall TdsSchEditClient::FGetConnection()
//{
//  if (FDM)
//   return FDM->Connection;
//  else
//   return NULL;
//}
//---------------------------------------------------------------------------
//void __fastcall TdsSchEditClient::FSetConnection(TdsICClassClient* AValue)
//{
//  if (FDM)
//   FDM->Connection = AValue;
//}
//---------------------------------------------------------------------------
/*TExtBtnClick __fastcall TdsSchEditClient::FGetExtBtnClick()
{
  if (FDM)
   return FDM->OnExtBtnClick;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetExtBtnClick(TExtBtnClick AValue)
{
  if (FDM)
   FDM->OnExtBtnClick = AValue;
} */
//---------------------------------------------------------------------------
bool __fastcall TdsSchEditClient::FGetNativeStyle()
{
  if (FDM)
   return FDM->NativeStyle;
  else
   return true;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetNativeStyle(bool AValue)
{
  if (FDM)
   FDM->NativeStyle = AValue;
}
//---------------------------------------------------------------------------
TcxLookAndFeelKind __fastcall TdsSchEditClient::FGetLFKind()
{
//  if (FDM)
//   return FDM->LFKind;
//  else
   return lfStandard;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetLFKind(TcxLookAndFeelKind AValue)
{
//  if (FDM)
//   FDM->LFKind = AValue;
}
//---------------------------------------------------------------------------
TcxEditStyleController* __fastcall TdsSchEditClient::FGetStyleController()
{
//  if (FDM)
//   return FDM->StyleController;
//  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetStyleController(TcxEditStyleController* AValue)
{
//  if (FDM)
//   FDM->StyleController = AValue;
}
//---------------------------------------------------------------------------
TdsGetClassXMLEvent __fastcall TdsSchEditClient::FGetOnGetClassXML()
{
  if (FDM)
   return FDM->OnGetClassXML;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnGetClassXML(TdsGetClassXMLEvent AValue)
{
  if (FDM)
   FDM->OnGetClassXML = AValue;
}
//---------------------------------------------------------------------------
/*TdsGetCountEvent __fastcall TdsSchEditClient::FGetOnGetCount()
{
  if (FDM)
   return FDM->OnGetCount;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnGetCount(TdsGetCountEvent AValue)
{
  if (FDM)
   FDM->OnGetCount = AValue;
}*/
//---------------------------------------------------------------------------
/*TdsGetIdListEvent __fastcall TdsSchEditClient::FGetOnGetIdList()
{
  if (FDM)
   return FDM->OnGetIdList;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnGetIdList(TdsGetIdListEvent AValue)
{
  if (FDM)
   FDM->OnGetIdList = AValue;
}*/
//---------------------------------------------------------------------------
TdsGetValByIdEvent __fastcall TdsSchEditClient::FGetOnGetValById()
{
  if (FDM)
   return FDM->OnGetValById;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnGetValById(TdsGetValByIdEvent AValue)
{
  if (FDM)
   FDM->OnGetValById = AValue;
}
//---------------------------------------------------------------------------
TdsGetValById10Event __fastcall TdsSchEditClient::FGetOnGetValById10()
{
  if (FDM)
   return FDM->OnGetValById10;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnGetValById10(TdsGetValById10Event AValue)
{
  if (FDM)
   FDM->OnGetValById10 = AValue;
}
//---------------------------------------------------------------------------
/*TdsFindEvent __fastcall TdsSchEditClient::FGetOnFind()
{
  if (FDM)
   return FDM->OnFind;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnFind(TdsFindEvent AValue)
{
  if (FDM)
   FDM->OnFind = AValue;
} */
//---------------------------------------------------------------------------
/*TdsInsertDataEvent __fastcall TdsSchEditClient::FGetOnInsertData()
{
  if (FDM)
   return FDM->OnInsertData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnInsertData(TdsInsertDataEvent AValue)
{
  if (FDM)
   FDM->OnInsertData = AValue;
}*/
//---------------------------------------------------------------------------
/*TdsEditDataEvent __fastcall TdsSchEditClient::FGetOnEditData()
{
  if (FDM)
   return FDM->OnEditData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnEditData(TdsEditDataEvent AValue)
{
  if (FDM)
   FDM->OnEditData = AValue;
}*/
//---------------------------------------------------------------------------
/*TdsDeleteDataEvent __fastcall TdsSchEditClient::FGetOnDeleteData()
{
  if (FDM)
   return FDM->OnDeleteData;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnDeleteData(TdsDeleteDataEvent AValue)
{
  if (FDM)
   FDM->OnDeleteData = AValue;
}*/
//---------------------------------------------------------------------------
TAxeXMLContainer* __fastcall TdsSchEditClient::FGetXMLList()
{
  if (FDM)
   return FDM->XMLList;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetXMLList(TAxeXMLContainer *AVal)
{
  if (FDM)
   FDM->XMLList = AVal;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::PreloadData()
{
  if (FDM)
    FDM->PreloadData();
}
//---------------------------------------------------------------------------
__int64 __fastcall TdsSchEditClient::FGetLPUCode()
{
  if (FDM)
   return FDM->LPUCode;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetLPUCode(__int64 AValue)
{
  if (FDM)
   FDM->LPUCode = AValue;
}
//---------------------------------------------------------------------------
/*TExtValidateData __fastcall TdsSchEditClient::FGetOnGetDefValues()
{
  if (FDM)
   return FDM->OnGetDefValues;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnGetDefValues(TExtValidateData AValue)
{
  if (FDM)
   FDM->OnGetDefValues = AValue;
}*/
//---------------------------------------------------------------------------
TOnGetPlanOptsEvent __fastcall TdsSchEditClient::FGetOnGetPlanOpts()
{
  if (FDM)
   return FDM->OnGetPlanOpts;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnGetPlanOpts(TOnGetPlanOptsEvent AValue)
{
  if (FDM)
   FDM->OnGetPlanOpts = AValue;
}
//---------------------------------------------------------------------------
TICSNomenklator* __fastcall TdsSchEditClient::FGetNom()
{
  if (FDM)
   return FDM->Nom;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetNom(TICSNomenklator* AValue)
{
  if (FDM)
   FDM->Nom = AValue;
}
//---------------------------------------------------------------------------
TdsNomCallDefProcEvent __fastcall TdsSchEditClient::FGetOnCallDefProc()
{
  if (FDM)
   return FDM->OnCallDefProc;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnCallDefProc(TdsNomCallDefProcEvent AValue)
{
  if (FDM)
   FDM->OnCallDefProc = AValue;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsSchEditClient::FGetXMLPath()
{
  if (FDM)
   return FDM->XMLPath;
  else
   return "";
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetXMLPath(UnicodeString AVal)
{
  if (FDM)
   FDM->XMLPath = AVal;
}
//---------------------------------------------------------------------------
TOnSaveSchemeEvent __fastcall TdsSchEditClient::FGetOnSaveXML()
{
  if (FDM)
   return FDM->OnSaveXML;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnSaveXML(TOnSaveSchemeEvent AVal)
{
  if (FDM)
   FDM->OnSaveXML = AVal;
}
//---------------------------------------------------------------------------
TdsGetListByIdEvent __fastcall TdsSchEditClient::FGetOnGetListById()
{
  if (FDM)
   return FDM->OnGetListById;
  else
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetOnGetListById(TdsGetListByIdEvent AVal)
{
  if (FDM)
   FDM->OnGetListById = AVal;
}
//---------------------------------------------------------------------------
TColor __fastcall TdsSchEditClient::FGetReqColor()
{
  if (FDM)
   return FDM->RequiredColor;
  else
   return clInfoBk;
}
//---------------------------------------------------------------------------
void __fastcall TdsSchEditClient::FSetReqColor(TColor AVal)
{
  if (FDM)
   FDM->RequiredColor = AVal;
}
//---------------------------------------------------------------------------

