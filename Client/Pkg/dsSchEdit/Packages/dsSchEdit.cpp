//---------------------------------------------------------------------------

#include <System.hpp>
#pragma hdrstop
USEFORM("..\Src\dsSchEditClientUnit.cpp", dsSchEditForm);
USEFORM("..\Src\dsVakSchList.cpp", fmVakSchList);
USEFORM("..\Src\dsSchUpd.cpp", SchUpdForm);
USEFORM("..\Src\dsSchEditDMUnit.cpp", dsSchEditDM); /* TDataModule: File Type */
USEFORM("..\Src\dsRefLine.cpp", fmRefLine);
USEFORM("..\Src\dsCopiedSchName.cpp", fmCopiedSchName);
USEFORM("..\Src\dsPrivivUpd.cpp", fmPrivivUpd);
USEFORM("..\Src\dsJumpUpd.cpp", fmJumpUpd);
USEFORM("..\Src\dsSchCondList.cpp", CondListForm);
USEFORM("..\Src\dsAgeMIBPList.cpp", AgeMIBPForm);
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Package source.
//---------------------------------------------------------------------------


#pragma argsused
extern "C" int _libmain(unsigned long reason)
{
 return 1;
}
//---------------------------------------------------------------------------
