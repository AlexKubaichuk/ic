//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DocOrgEdit.h"
#include "pkgsetting.h"

#ifndef _CONFSETTTING
 #error _CONFSETTTING using are not defined
#endif

#include "msgdef.h"
#include "icsDocExConstDef.h"
#include "ExtFunc.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxButtons"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxDropDownEdit"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxMaskEdit"
#pragma link "cxTextEdit"
#pragma link "cxCheckBox"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma resource "*.dfm"
TDocOrgEditForm *DocOrgEditForm;
//---------------------------------------------------------------------------
__fastcall TDocOrgEditForm::TDocOrgEditForm(TComponent* Owner, TDocExDM *ADM, bool AIsEdit, bool AIsSet, bool AUseLPUClass)
        : TForm(Owner)
{
  if (!AUseLPUClass)
   {
     LPUClassPanel->Visible = false;
     Height -= 90;
   }
  FExtLPUCode = -1;
  FExtLPUPartCode = -1;
  FDM = ADM;
  IsSet = AIsSet;
  if (IsSet) IsEdit = true;
  else       IsEdit = AIsEdit;
  SubordCB->Properties->Items->Clear();
  if (FDM->UseKLADRAddr)
   {
     FDM->KLADR->DomVisible = true;
   }
  else
   {
     AddrED->Properties->ViewStyle = vsNormal;
     AddrED->Properties->Buttons->Clear();
     AddrED->Properties->ReadOnly = false;
     AddrValLab->Visible = false;
     CommonPanel->Height = CommonPanel->Height - 60;
     Height = Height-60;
   }

  SubordCB->Properties->Items->Add(FMT(icsDocExDocOrgEditSubordCBItem1));
  SubordCB->Properties->Items->Add(FMT(icsDocExDocOrgEditSubordCBItem2));
  SubordCB->Properties->Items->Add(FMT(icsDocExDocOrgEditSubordCBItem3));
  if (IsSet)
   {
     SubordLab->Visible = false;
     SubordCB->Visible = false;
     Caption = FMT(icsDocExDocOrgEditSetCaption);
     if (!AUseLPUClass)
      ActiveControl = OrgCodeED;
     else
      ActiveControl = UseLPUClass;
   }
  else
   {
     if (IsEdit)
      {
        if (FDM->quOrgSUBORD->AsInteger == 3)
         SubordCB->ItemIndex = 2;
        else
         SubordCB->ItemIndex = FDM->quOrgSUBORD->AsInteger;
      }
     Caption = UnicodeString((AIsEdit)?cFMT(icsDocExDocOrgEditEditCaption) : cFMT(icsDocExDocOrgEditInsertCaption));
     if (!AUseLPUClass)
      ActiveControl = SubordCB;
     else
      ActiveControl = UseLPUClass;
   }
  if (IsEdit)
   {
     OrgCodeED->Text  = FDM->quOrgCODE->AsString;
     NameED->Text     = FDM->quOrgNAME->AsString;
     FullNameED->Text = FDM->quOrgFULLNAME->AsString;
     AddrED->Text     = FDM->quOrgADDR->AsString;

     if (FDM->UseKLADRAddr)
      AddrValLab->Caption = FDM->KLADR->codeToText(AddrED->Text);

     EMailED->Text    = FDM->quOrgEMAIL->AsString;
     PhoneED->Text    = FDM->quOrgPHONE->AsString;
     OwnerFIOED->Text = FDM->quOrgOWNERFIO->AsString;
     SaveCode = FDM->quOrgCODE->AsString;
   }
  else
   {
     OrgCodeED->Text = NewGUID();
     SaveCode = "";
   }

  SubordLab->Caption = FMT(icsDocExDocOrgEditSubordLabCaption);
  OrgCodeLab->Caption = FMT(icsDocExDocOrgEditOrgCodeLabCaption);
  NameLab->Caption = FMT(icsDocExDocOrgEditNameLabCaption);
  AddrLab->Caption = FMT(icsDocExDocOrgEditAddrLabCaption);
  EMailLab->Caption = FMT(icsDocExDocOrgEditEMailLabCaption);
  PhoneLab->Caption = FMT(icsDocExDocOrgEditPhoneLabCaption);
  FullNameLab->Caption = FMT(icsDocExDocOrgEditFullNameLabCaption);
  OkBtn->Caption = FMT(icsDocExDocOrgEditOkBtnCaption);
  CancelBtn->Caption = FMT(icsDocExDocOrgEditCancelBtnCaption);

}
//---------------------------------------------------------------------------
bool __fastcall TDocOrgEditForm::CheckInput()
{
   try
    {
      if (UseLPUClass->Checked && !(LPUCB->ItemIndex >= 0))
       {
         _MSG_ERR(FMT(icsDocExDocOrgEditChLPUErrMsg),FMT(icsDocExCommonInputErrorCaption));
         ActiveControl = LPUCB;
         return false;
       }
      if (!IsSet&&(SubordCB->ItemIndex < 0))
       {
         _MSG_ERR(FMT(icsDocExDocOrgEditChISubordErrMsg),FMT(icsDocExCommonInputErrorCaption));
         ActiveControl = SubordCB;
         return false;
       }
      if (!OrgCodeED->Text.Trim().Length())
       {
         _MSG_ERR(FMT(icsDocExDocOrgEditChIOrgCodeErrMsg),FMT(icsDocExCommonInputErrorCaption));
         ActiveControl = OrgCodeED;
         return false;
       }
      else if (
               (OrgCodeED->Text == "000.0000000.000") ||
               ( ((!IsSet&&(SubordCB->ItemIndex == 0))||IsSet) &&
                  (
                    (OrgCodeED->Text == "812.2741381.000") ||
                    (OrgCodeED->Text == "812.1107704.000") ||
                    (OrgCodeED->Text == "812.7107704.000") ||
                    (OrgCodeED->Text == "812.7177704.000") ||
                    (OrgCodeED->Text == "812.7170425.000") ||
                    (OrgCodeED->Text == "812.7100425.000") ||
                    (OrgCodeED->Text == "812.1100425.000") ||
                    (OrgCodeED->Text == "812.7171319.000")
                  )
               )
              )
       {
         _MSG_ERR(FMT(icsDocExDocOrgEditChIOrgCodeResErrMsg),FMT(icsDocExCommonInputErrorCaption));
         ActiveControl = OrgCodeED;
         return false;
       }
      if (!NameED->Text.Trim().Length())
       {
         _MSG_ERR(FMT(icsDocExDocOrgEditChIOrgShortNameErrMsg),FMT(icsDocExCommonInputErrorCaption));
         ActiveControl = NameED;
         return false;
       }
      if (!FullNameED->Text.Trim().Length())
       {
         _MSG_ERR(FMT(icsDocExDocOrgEditChIOrgFullNameErrMsg),FMT(icsDocExCommonInputErrorCaption));
         ActiveControl = FullNameED;
         return false;
       }
      if (!AddrED->Text.Trim().Length())
       {
         _MSG_ERR(FMT(icsDocExDocOrgEditChIAddrErrMsg),FMT(icsDocExCommonInputErrorCaption));
         ActiveControl = AddrED;
         return false;
       }
      if (!IsEdit || (IsEdit&&(SaveCode.Trim() != OrgCodeED->Text.Trim())))
       {
         FDM->qtExecDoc("Select CODE From SUBORDORG Where CODE='"+OrgCodeED->Text+"'",false);
         if (FDM->quFreeDoc->RecordCount)
          {
            _MSG_ERR(FMT(icsDocExDocOrgEditChIOrgExistErrMsg),FMT(icsDocExCommonInputErrorCaption));
            ActiveControl = OrgCodeED;
            return false;
          }
       }
    }
   __finally
    {
      if (FDM->trFreeDoc->Active) FDM->trFreeDoc->Commit();
    }
   return true;
}
//---------------------------------------------------------------------------
void __fastcall TDocOrgEditForm::OkBtnClick(TObject *Sender)
{
   if (!CheckInput()) return;

   FDM->quOrg->FN("CODE")->AsString     = OrgCodeED->Text.Trim();
   FDM->quOrg->FN("NAME")->AsString     = NameED->Text;
   FDM->quOrg->FN("FULLNAME")->AsString = FullNameED->Text;
   FDM->quOrg->FN("ADDR")->AsString     = AddrED->Text;
   if (IsSet)
    {
      if (SaveCode != OrgCodeED->Text.Trim())
       {
         FDM->qtExecDoc("Update SPECIFIKATORS Set F_OWNER ='"+OrgCodeED->Text.Trim()+"' Where F_OWNER='"+SaveCode+"'",true);
         FDM->qtExecDoc("Update DOCUMENTS Set F_OWNER ='"+OrgCodeED->Text.Trim()+"' Where F_OWNER='"+SaveCode+"'",true);
         FDM->qtExecDoc("Update DOCUMENTS Set F_SPECOWNER ='"+OrgCodeED->Text.Trim()+"' Where F_SPECOWNER='"+SaveCode+"'",true);
         TStringList *tmpDocList = NULL;
         try
          {
            tmpDocList = new TStringList;
            // �������� ���������� - �����
            tmpDocList->Clear();
            FDM->qtExecDoc("Select GUI, SPECTEXT From  SPECIFIKATORS Where F_OWNER ='"+OrgCodeED->Text.Trim()+"'",false);
            while (!FDM->quFreeDoc->Eof)
             {
               try
                {
                  tmpDocList->AddObject(FDM->quFreeDoc->FN("GUI")->AsString,(TObject*) new TTagNode(NULL));
                  ((TTagNode*)tmpDocList->Objects[tmpDocList->Count-1])->AsXML = FDM->quFreeDoc->FN("SPECTEXT")->AsString;
                }
               catch(...)
                {
                }
               FDM->quFreeDoc->Next();
             }
            if (!FDM->trFreeDoc->Active) FDM->trFreeDoc->StartTransaction();
            FDM->quFreeDoc->Close();
            FDM->quFreeDoc->SQL->Clear();
            FDM->quFreeDoc->SQL->Text = "Update SPECIFIKATORS Set SPECTEXT = ?SPECTEXT Where GUI = ?GUI";
            FDM->quFreeDoc->Prepare();
            for (int i = 0; i < tmpDocList->Count; i++)
             {
               SetSpecOwner(((TTagNode*)tmpDocList->Objects[i]), OrgCodeED->Text.Trim());
               FDM->quFreeDoc->ParamByName("SPECTEXT")->AsString = ((TTagNode*)tmpDocList->Objects[i])->AsXML;
               FDM->quFreeDoc->ParamByName("GUI")->AsString = tmpDocList->Strings[i];
               FDM->quFreeDoc->ExecQuery();
             }
            FDM->trFreeDoc->Commit();
            for (int i = 0; i < tmpDocList->Count; i++)
             {
               delete (TTagNode*)tmpDocList->Objects[i];
             }
            // ��������� - �����
            tmpDocList->Clear();
            FDM->qtExecDoc("Select GUI, DOCTEXT From  DOCUMENTS Where F_OWNER ='"+OrgCodeED->Text.Trim()+"'",false);
            while (!FDM->quFreeDoc->Eof)
             {
               try
                {
                  tmpDocList->AddObject(FDM->quFreeDoc->FN("GUI")->AsString,(TObject*) new TTagNode(NULL));
                  TStringStream *FZipDoc = new TStringStream;
                  try
                   {
                     FZipDoc->WriteData(FDM->quFreeDoc->FN("DOCTEXT")->AsString.BytesOf(),FDM->quFreeDoc->FN("DOCTEXT")->AsString.Length());
                     ((TTagNode*)tmpDocList->Objects[tmpDocList->Count-1])->SetZIPXML(FZipDoc);
                   }
                  __finally
                   {
                     delete FZipDoc;
                   }
                }
               catch(...)
                {
                }
               FDM->quFreeDoc->Next();
             }
            if (!FDM->trFreeDoc->Active) FDM->trFreeDoc->StartTransaction();
            FDM->quFreeDoc->Close();
            FDM->quFreeDoc->SQL->Clear();
            FDM->quFreeDoc->SQL->Text = "Update DOCUMENTS Set DOCTEXT = ?DOCTEXT Where GUI = ?GUI";
            FDM->quFreeDoc->Prepare();
            for (int i = 0; i < tmpDocList->Count; i++)
             {
               SetSpecOwner(((TTagNode*)tmpDocList->Objects[i]), OrgCodeED->Text.Trim());
               TStringStream *FZipDoc = new TStringStream;
               try
                {
                  ((TTagNode*)tmpDocList->Objects[i])->GetZIPXML(FZipDoc);
                  FDM->quFreeDoc->ParamByName("DOCTEXT")->AsString = FZipDoc->DataString;      //  DOCTEXT
                }
               __finally
                {
                  delete FZipDoc;
                }
               FDM->quFreeDoc->ParamByName("GUI")->AsString = tmpDocList->Strings[i];
               FDM->quFreeDoc->ExecQuery();
             }
            FDM->trFreeDoc->Commit();
            for (int i = 0; i < tmpDocList->Count; i++)
             {
               delete (TTagNode*)tmpDocList->Objects[i];
             }
          }
         __finally
          {
            if (tmpDocList) delete tmpDocList;
          }
       }
      FDM->quOrg->FN("SUBORD")->AsInteger = 2;
    }
   else
    {
      if (SubordCB->ItemIndex < 2)
       FDM->quOrg->FN("SUBORD")->AsInteger = SubordCB->ItemIndex;
      else
       FDM->quOrg->FN("SUBORD")->AsInteger = 3;
    }
   FDM->quOrg->FN("OKPO")->AsString     = "0";
   FDM->quOrg->FN("SOATO")->AsString    = "0";
   FDM->quOrg->FN("OKVED")->AsString    = "0";
   FDM->quOrg->FN("OKATO")->AsString    = "0";
   FDM->quOrg->FN("EMAIL")->AsString    = EMailED->Text;
   FDM->quOrg->FN("PHONE")->AsString    = PhoneED->Text;
   FDM->quOrg->FN("OwnerFIO")->AsString = OwnerFIOED->Text;

   if (OnSetLPUData && OnGetFillLPUData)
    {
      TTagNode *FLPUData = new TTagNode(NULL);
      try
       {
         GetLPUData(FLPUData);
         if (LPUCB->ItemIndex >=0)
          {
            OnSetLPUData(ltMain, (int)LPUCB->Properties->Items->Objects[LPUCB->ItemIndex], FLPUData);
            if (LPUPartCB->ItemIndex >=0)
             OnSetLPUData(ltDep, (int)LPUPartCB->Properties->Items->Objects[LPUPartCB->ItemIndex], FLPUData);
            else
             OnSetLPUData(ltDep, -1, FLPUData);
          }
         else
          OnSetLPUData(ltMain, -1, FLPUData);
       }
      __finally
       {
         delete FLPUData;
       }
    }
   ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TDocOrgEditForm::OrgCodeEDPropertiesValidate(
      TObject *Sender, Variant &DisplayValue, TCaption &ErrorText,
      bool &Error)
{
   Error =  false;
}
//---------------------------------------------------------------------------
void __fastcall TDocOrgEditForm::SetLPUData(TTagNode *AData)
{
  if (AData->GetChildByName("Code"))
   OrgCodeED->Text = AData->GetChildByName("Code")->AV["PCDATA"];
  if (AData->GetChildByName("FullName"))
   FullNameED->Text = AData->GetChildByName("FullName")->AV["PCDATA"];
  if (AData->GetChildByName("Name"))
   NameED->Text = AData->GetChildByName("Name")->AV["PCDATA"];
  if (AData->GetChildByName("OwnerFIO"))
   OwnerFIOED->Text = AData->GetChildByName("OwnerFIO")->AV["PCDATA"];
  if (AData->GetChildByName("Phone"))
   PhoneED->Text = AData->GetChildByName("Phone")->AV["PCDATA"];
  if (AData->GetChildByName("EMail"))
   EMailED->Text = AData->GetChildByName("EMail")->AV["PCDATA"];
  if (AData->GetChildByName("Addr"))
   AddrED->Text = AData->GetChildByName("Addr")->AV["PCDATA"];

  if (FDM->UseKLADRAddr)
   {
     if (AData->GetChildByName("AddrStr"))
      AddrValLab->Caption = AData->GetChildByName("AddrStr")->AV["PCDATA"];
   }
}
//---------------------------------------------------------------------------
void __fastcall TDocOrgEditForm::GetLPUData(TTagNode *AData)
{
  AData->Name = "LPUData";
  AData->DeleteChild();
  AData->AddChild("Code")->AV["PCDATA"]     = OrgCodeED->Text;
  AData->AddChild("FullName")->AV["PCDATA"] = FullNameED->Text;
  AData->AddChild("Name")->AV["PCDATA"]     = NameED->Text;
  AData->AddChild("OwnerFIO")->AV["PCDATA"] = OwnerFIOED->Text;
  AData->AddChild("Phone")->AV["PCDATA"]    = PhoneED->Text;
  AData->AddChild("EMail")->AV["PCDATA"]    = EMailED->Text;
  AData->AddChild("Addr")->AV["PCDATA"]    = AddrED->Text;
  if (FDM->UseKLADRAddr)
   AData->AddChild("AddrStr")->AV["PCDATA"] = AddrValLab->Caption;
}
//---------------------------------------------------------------------------
void __fastcall TDocOrgEditForm::LPUCBPropertiesChange(TObject *Sender)
{
  if (OnGetFillLPUData)
   {
     if (LPUCB->ItemIndex >= 0)
      {
        long FLPUCode = (long)LPUCB->Properties->Items->Objects[LPUCB->ItemIndex];
        LPUPartCB->ItemIndex = -1;
        LPUPartCB->Properties->Items->Clear();
        OnGetFillLPUData(ltDep, LPUPartCB->Properties->Items, FLPUCode);
        if (OnGetLPUData)
         {
           TTagNode *FLPUData = new TTagNode(NULL);
           GetLPUData(FLPUData);
           try
            {
              OnGetLPUData(ltMain, FLPUCode, FLPUData);
              SetLPUData(FLPUData);
            }
           __finally
            {
              delete FLPUData;
            }
         }
      }
     else
      {
        LPUPartCB->ItemIndex = -1;
        LPUPartCB->Properties->Items->Clear();
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TDocOrgEditForm::LPUPartCBPropertiesChange(TObject *Sender)
{
  if (OnGetFillLPUData && (LPUPartCB->ItemIndex >= 0))
   {
     if (OnGetLPUData)
      {
        TTagNode *FLPUData = new TTagNode(NULL);
        GetLPUData(FLPUData);
        try
         {
           OnGetLPUData(ltDep, (long)LPUPartCB->Properties->Items->Objects[LPUPartCB->ItemIndex], FLPUData);
           SetLPUData(FLPUData);
         }
        __finally
         {
           delete FLPUData;
         }
      }
   }
}
//---------------------------------------------------------------------------
#include <rpc.h>
UnicodeString __fastcall TDocOrgEditForm::NewGUID()
{
/*  UnicodeString RC = "";
  try
   {
     UUID FGUI;
     if (UuidCreate(&FGUI) == RPC_S_OK)
      {
        unsigned char * strGUID[1];
        if (UuidToString(&FGUI,strGUID) == RPC_S_OK)
         RC = UnicodeString((char*)(strGUID[0]));
      }
   }
  __finally
   {
   }
  if (RC.Trim().Length())
   return RC.Trim().UpperCase();
  else
   throw Exception("������ ������������ GUID");  */
}
//---------------------------------------------------------------------------
void __fastcall TDocOrgEditForm::UseLPUClassPropertiesChange(
      TObject *Sender)
{
  LPULab->Enabled     = UseLPUClass->Checked && (OnGetFillLPUData != NULL);
  LPUCB->Enabled      = UseLPUClass->Checked && (OnGetFillLPUData != NULL);
  LPUPartLab->Enabled = UseLPUClass->Checked && (OnGetFillLPUData != NULL);
  LPUPartCB->Enabled  = UseLPUClass->Checked && (OnGetFillLPUData != NULL);

  OrgCodeLab->Enabled  = !UseLPUClass->Checked;
  OrgCodeED->Enabled   = !UseLPUClass->Checked;
  NameLab->Enabled     = !UseLPUClass->Checked;
  NameED->Enabled      = !UseLPUClass->Checked;
  FullNameLab->Enabled = !UseLPUClass->Checked;
  FullNameED->Enabled  = !UseLPUClass->Checked;
  AddrLab->Enabled     = !UseLPUClass->Checked;
  AddrED->Enabled      = !UseLPUClass->Checked;
  AddrValLab->Enabled  = !UseLPUClass->Checked;
  FIOLab->Enabled      = !UseLPUClass->Checked;
  OwnerFIOED->Enabled  = !UseLPUClass->Checked;
  PhoneLab->Enabled    = !UseLPUClass->Checked;
  PhoneED->Enabled     = !UseLPUClass->Checked;
  EMailLab->Enabled    = !UseLPUClass->Checked;
  EMailED->Enabled     = !UseLPUClass->Checked;

  if (!UseLPUClass->Checked)
   {
     LPUCB->ItemIndex = -1;
     LPUPartCB->ItemIndex = -1;
     LPUPartCB->Properties->Items->Clear();
   }
}
//---------------------------------------------------------------------------
void __fastcall TDocOrgEditForm::FormShow(TObject *Sender)
{
  if (OnGetFillLPUData)
   {
     OnGetFillLPUData(ltMain, LPUCB->Properties->Items, -1);
     if (UseLPUClass->Checked)
      {
        if (FExtLPUCode != -1)
         {
           int idx = LPUCB->Properties->Items->IndexOfObject((TObject*)FExtLPUCode);
           if (idx != -1)
            {
              LPUCB->ItemIndex = idx;
              if (FExtLPUPartCode != -1)
               {
                 idx = LPUPartCB->Properties->Items->IndexOfObject((TObject*)FExtLPUPartCode);
                 if (idx != -1)
                  {
                    LPUPartCB->ItemIndex = idx;
                  }
               }
            }
         }
      }
   }
  else
   {
     UseLPUClass->Checked = false;
     UseLPUClass->Enabled = false;
   }
}
//---------------------------------------------------------------------------



