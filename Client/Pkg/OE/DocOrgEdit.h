//---------------------------------------------------------------------------
#ifndef DocOrgEditH
#define DocOrgEditH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <StdCtrls.hpp>
#include "cxButtonEdit.hpp"
#include "cxButtons.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
//---------------------------------------------------------------------------
#include "DocExDFM.h"
#include "cxCheckBox.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
//---------------------------------------------------------------------------

/*
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Mask.hpp>
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "cxButtonEdit.hpp"
*/
//---------------------------------------------------------------------------
class PACKAGE TDocOrgEditForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TBevel *Bevel1;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TPanel *LPUClassPanel;
        TLabel *LPULab;
        TcxComboBox *LPUCB;
        TcxComboBox *LPUPartCB;
        TLabel *LPUPartLab;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall OrgCodeEDPropertiesValidate(TObject *Sender,
          Variant &DisplayValue, TCaption &ErrorText, bool &Error);
        void __fastcall LPUCBPropertiesChange(TObject *Sender);
        void __fastcall LPUPartCBPropertiesChange(TObject *Sender);
        void __fastcall UseLPUClassPropertiesChange(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
        bool __fastcall CheckInput();
        bool IsEdit,IsSet;
        UnicodeString SaveCode;
        TDocExDM *FDM;
        long FExtLPUCode,FExtLPUPartCode;
        TFillLPUData    FOnGetFillLPUData;
        TSetGetLPUData  FOnSetLPUData;
        TSetGetLPUData  FOnGetLPUData;
        UnicodeString __fastcall NewGUID();
        void __fastcall SetLPUData(TTagNode *AData);
        void __fastcall GetLPUData(TTagNode *AData);
public:		// User declarations
        __fastcall TDocOrgEditForm(TComponent* Owner, TDocExDM *ADM, bool AIsEdit, bool AIsSet = false, bool AUseLPUClass = true);

        __property TFillLPUData  OnGetFillLPUData = {read=FOnGetFillLPUData, write=FOnGetFillLPUData};
        __property TSetGetLPUData  OnSetLPUData = {read=FOnSetLPUData, write=FOnSetLPUData};
        __property TSetGetLPUData  OnGetLPUData = {read=FOnGetLPUData, write=FOnGetLPUData};

        __property long LPUCode = {read=FExtLPUCode, write=FExtLPUCode};
        __property long LPUPartCode = {read=FExtLPUPartCode, write=FExtLPUPartCode};
};
//---------------------------------------------------------------------------
extern PACKAGE TDocOrgEditForm *DocOrgEditForm;
//---------------------------------------------------------------------------
#endif
