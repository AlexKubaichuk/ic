object DocOrgEditForm: TDocOrgEditForm
  Left = 615
  Top = 149
  BorderStyle = bsSingle
  Caption = 'DocOrgEditForm'
  ClientHeight = 143
  ClientWidth = 503
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 106
    Width = 503
    Height = 37
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 461
    ExplicitWidth = 364
    DesignSize = (
      503
      37)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 503
      Height = 5
      Align = alTop
      Shape = bsTopLine
      ExplicitWidth = 364
    end
    object OkBtn: TcxButton
      Left = 329
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Default = True
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = OkBtnClick
      ExplicitLeft = 190
    end
    object CancelBtn: TcxButton
      Left = 417
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      LookAndFeel.Kind = lfStandard
      LookAndFeel.NativeStyle = True
      ModalResult = 2
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ExplicitLeft = 278
    end
  end
  object LPUClassPanel: TPanel
    Left = 0
    Top = 0
    Width = 503
    Height = 90
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 364
    object LPULab: TLabel
      Left = 15
      Top = 37
      Width = 27
      Height = 13
      Caption = #1051#1055#1059':'
      FocusControl = LPUCB
    end
    object LPUPartLab: TLabel
      Left = 14
      Top = 64
      Width = 110
      Height = 13
      Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077' '#1051#1055#1059':'
    end
    object LPUCB: TcxComboBox
      Left = 133
      Top = 33
      Properties.DropDownListStyle = lsEditFixedList
      Properties.OnChange = LPUCBPropertiesChange
      Style.Color = clInfoBk
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 0
      Width = 364
    end
    object LPUPartCB: TcxComboBox
      Left = 133
      Top = 60
      Properties.DropDownListStyle = lsEditFixedList
      Properties.OnChange = LPUPartCBPropertiesChange
      Style.Color = clWindow
      Style.LookAndFeel.Kind = lfStandard
      Style.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.Kind = lfStandard
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfStandard
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfStandard
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 1
      Width = 364
    end
  end
end
