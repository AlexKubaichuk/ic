//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "dsClassifClientUnit.h"
#include "dsClassifEditClientUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "dsRegTemplateUnit.h"
//#include "fmtdef.h"
#include "msgdef.h"
//#include "pkgsetting.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxData"
#pragma link "cxDataStorage"
#pragma link "cxEdit"
#pragma link "cxFilter"
#pragma link "cxGraphics"
#pragma link "cxGrid"
#pragma link "cxGridCustomTableView"
#pragma link "cxGridCustomView"
#pragma link "cxGridLevel"
#pragma link "cxGridTableView"
#pragma link "cxImage"
#pragma link "cxInplaceContainer"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxNavigator"
#pragma link "cxPC"
#pragma link "cxSplitter"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "cxTL"
#pragma link "cxTLdxBarBuiltInMenu"
#pragma link "dxBar"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxGDIPlusClasses"
#pragma link "dxStatusBar"
#pragma resource "*.dfm"
TdsClassifClientForm * dsClassifClientForm;
//---------------------------------------------------------------------------
__fastcall TdsClassifClientForm::TdsClassifClientForm(TComponent * Owner, TdsRegDM * ADM, TdsRegClassType AShowType,
    UnicodeString AGroups, UnicodeString AClsId) : TForm(Owner)
 {
  FDM                     = ADM;
  SaveEnInsert            = false;
  SaveEnEdit              = false;
  SaveEnDelete            = false;
  SaveEnSetTemplate       = false;
  SaveEnReSetTemplate     = false;
  SaveEnRefresh           = false;
  SaveEnFindNext          = false;
  SaveEnViewClassParam    = false;
  SaveEnViewTemplateParam = false;
  FDataSrc                = NULL;
  FFilter                 = "";
  FSearchIdx              = 0;
  FSearchModify           = false;
  FSearchAndFilter        = false;
  FSearchList             = new TStringList;
  /*actInsert->Caption = FMT(icsRegClassifInsertCaption);
   actInsert->Hint = FMT(icsRegClassifInsertHint);
   if (FMT(icsRegClassifInsertShortCut).Length())
   actInsert->ShortCut = TextToShortCut(FMT(icsRegClassifInsertShortCut));

   actEdit->Caption = FMT(icsRegClassifEditCaption);
   actEdit->Hint = FMT(icsRegClassifEditHint);
   if (FMT(icsRegClassifEditShortCut).Length())
   actEdit->ShortCut = TextToShortCut(FMT(icsRegClassifEditShortCut));

   actDelete->Caption = FMT(icsRegClassifDeleteCaption);
   actDelete->Hint = FMT(icsRegClassifDeleteHint);
   if (FMT(icsRegClassifDeleteShortCut).Length())
   actDelete->ShortCut = TextToShortCut(FMT(icsRegClassifDeleteShortCut));

   //  actSetTemplate->Caption = FMT(icsRegClassifSetTemplateCaption);
   //  actSetTemplate->Hint = FMT(icsRegClassifSetTemplateHint);
   //  if (FMT(icsRegClassifSetTemplateShortCut).Length())
   //   actSetTemplate->ShortCut = TextToShortCut(FMT(icsRegClassifSetTemplateShortCut));

   //  actReSetTemplate->Caption = FMT(icsRegClassifReSetTemplateCaption);
   //  actReSetTemplate->Hint = FMT(icsRegClassifReSetTemplateHint);
   //  if (FMT(icsRegClassifReSetTemplateShortCut).Length())
   //   actReSetTemplate->ShortCut = TextToShortCut(FMT(icsRegClassifReSetTemplateShortCut));

   actRefresh->Caption = FMT(icsRegClassifRefreshCaption);
   actRefresh->Hint = FMT(icsRegClassifRefreshHint);
   if (FMT(icsRegClassifRefreshShortCut).Length())
   actRefresh->ShortCut = TextToShortCut(FMT(icsRegClassifRefreshShortCut));

   actFind->Caption = FMT(icsRegClassifFindCaption);
   actFind->Hint = FMT(icsRegClassifFindHint);
   if (FMT(icsRegClassifFindShortCut).Length())
   actFind->ShortCut = TextToShortCut(FMT(icsRegClassifFindShortCut));

   actFindNext->Caption = FMT(icsRegClassifFindNextCaption);
   actFindNext->Hint = FMT(icsRegClassifFindNextHint);
   if (FMT(icsRegClassifFindNextShortCut).Length())
   actFindNext->ShortCut = TextToShortCut(FMT(icsRegClassifFindNextShortCut));

   //  actViewClassParam->Caption = FMT(icsRegClassifViewClassParamCaption);
   //  actViewClassParam->Hint = FMT(icsRegClassifViewClassParamHint);
   //  if (FMT(icsRegClassifViewClassParamShortCut).Length())
   //   actViewClassParam->ShortCut = TextToShortCut(FMT(icsRegClassifViewClassParamShortCut));

   //  actViewTemplateParam->Caption = FMT(icsRegClassifViewTemplateParamCaption);
   //  actViewTemplateParam->Hint = FMT(icsRegClassifViewTemplateParamHint);
   //  if (FMT(icsRegClassifViewTemplateParamShortCut).Length())
   //   actViewTemplateParam->ShortCut = TextToShortCut(FMT(icsRegClassifViewTemplateParamShortCut));*/
  /*StatusSB->Panels->Items[0]->Text = FMT(icsCommonRecCount);*/
  ViewParamPC->ActivePage = ClassParamTS;
  UnicodeString tmpStr = AGroups.UpperCase().Trim();
  if (tmpStr.Length())
   {
    TReplaceFlags rFlag;
    rFlag << rfReplaceAll;
    tmpStr = StringReplace(tmpStr, ",", "\n", rFlag);
    TStringList * DDD = new TStringList;
    DDD->Text = tmpStr;
    for (int i = 0; i < DDD->Count; i++)
     {
      FGroups[DDD->Strings[i]] = "1";
     }
    delete DDD;
   }
  else
   {
    FGroups["0"] = "1";
   }
  //BGStyle->Color = clInactiveBorder;
  FShowType = AShowType;
  if (FDM->DefaultClassWidth)
   Width = FDM->DefaultClassWidth;
  RootClass        = FDM->ClassDef;
  FCtrList         = new TdsRegEDContainer(this, ExtListPC);
  FCtrList->DefXML = RootClass;
  //FCtrList->FIBTemplateQuery = FDM->xquFree;
  //FCtrList->ClassData        = FDM->FClassData;
  FCtrList->DataProvider = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DefXML = RootClass;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  FCtrList->DataPath                    = FDM->DataPath;
  FLastTop                              = 0;
  FLastHeight                           = 0;
  //SaveCaption = FDM->PrefixCaption.Trim();
  Caption = SaveCaption;
  CreateTV(RootClass);
  curCD = 0;
  //ClassTBM->Images = FDM->Images;
  //ClassTBM->LargeImages = FDM->LargeImages;
  //ClassTBM->HotImages = FDM->HotImages;
  //ClassTBM->DisabledImages = FDM->DisabledImages;
  //ClassTBM->LargeIcons = FDM->UseLargeImages;
  //ClassAL->Images = FDM->Images;
  //PMenu->Images = FDM->Images;
  int AllItemsCount = 0;
  for (int i = 0; i < ClassTBM->Bars->Count; i++)
   {
    AllItemsCount += ClassTBM->Bars->Items[i]->ItemLinks->Count;
   }
  int FCalcWidth = 0;
  //if (FDM->UseLargeImages)
  //FCalcWidth = (AllItemsCount+1) * ClassTBM->LargeImages->Width;
  //else
  //FCalcWidth = (AllItemsCount+1) * ClassTBM->Images->Width;
  //if (FCalcWidth > Width) Width = FCalcWidth;
  if (ClassTL->Count == 0)
   {
    actTemplate->Enabled         = false;
    actSetFilter->Enabled        = false;
    actReSetFilter->Enabled      = false;
    actInsert->Enabled           = false;
    actEdit->Enabled             = false;
    actDelete->Enabled           = false;
    actRefresh->Enabled          = false;
    actFind->Enabled             = false;
    actFindAndFilter->Enabled    = false;
    actClearSearchField->Enabled = false;
    Caption                      = SaveCaption;
    //if (FDM->UseClassDisControls)
    //quClass()->DisableControls();
    DisableAfterScroll();
    //quClass()->Fields->Clear();
    //quClass()->Close();
    FCtrList->ClearLabItems();
    return;
   }
  if (!AClsId.Length())
   {
    actGetClassListExecute(actGetClassList);
   }
  else
   {
    FSelectedNode = RootClass->GetTagByUID(AClsId);
    if (FSelectedNode)
     {
      actGetClassList->Enabled = false;
      actGetClassList->Visible = false;
      curCD                    = 0;
      FSelectedNodeDescr       = FSelectedNode->GetChildByName("description");
      Caption                  = SaveCaption + " " + FSelectedNode->AV["name"];
      ChangeSrc();
      //SetActiveCtrl();
     }
    else
     actGetClassListExecute(actGetClassList);
   }
  if (FDM->FullEdit)
   {
    RootClass->Iterate(FSetEdit);
   }
  //SetActiveCtrl();
  //if (FDM->FetchAll)
  //quClass()->Options << poFetchAll;
  //else
  //quClass()->Options >> poFetchAll;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsClassifClientForm::FSetEdit(TTagNode * itTag)
 {
  if (itTag->GetAttrByName("isedit"))
   {
    if (itTag->CmpName("extedit"))
     {
      if (!(itTag->CmpAV("isedit", "0") && !itTag->CmpAV("depend", "")))
       itTag->AV["isedit"] = "1";
     }
    else
     itTag->AV["isedit"] = "1";
   }
  return false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::SetActiveClass(UnicodeString AClsId)
 {
  if (AClsId.Length())
   {
    TTagNode * tmp;
    for (int i = 0; i < ClassTL->Count; i++)
     {
      if (ClassTL->Items[i]->Level == 1)
       {
        tmp = ((TTagNode *)ClassTL->Items[i]->Data);
        if (tmp)
         {
          if (tmp->CmpAV("uid", AClsId))
           {
            ClassTL->Items[i]->Focused = true;
            curCD                      = 0;
            FSelectedNode              = (TTagNode *)ClassTL->Items[i]->Data;
            FSelectedNodeDescr         = FSelectedNode->GetChildByName("description");
            ChangeSrc();
            SetActiveCtrl();
           }
          break;
         }
       }
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::ChangeSrc()
 {
  CheckCtrlState(false);
  Application->ProcessMessages();
  try
   {
    try
     {
      Caption = SaveCaption + " " + _CLASSNODE_()->AV["name"];
      FCtrList->ClearLabItems();
      ClassGridView->DataController->CustomDataSource = NULL;
      if (FDataSrc)
       delete FDataSrc;
      FDataSrc                          = new TkabCustomDataSource(_CLASSNODE_());
      FDataSrc->DataSet->OnGetCount     = FDM->kabCDSGetCount;
      FDataSrc->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
      FDataSrc->DataSet->OnGetValById   = FDM->kabCDSGetValById;
      FDataSrc->DataSet->OnGetValById10 = FDM->kabCDSGetValById10;
      FDataSrc->DataSet->OnInsert       = FDM->kabCDSInsert;
      FDataSrc->DataSet->OnEdit         = FDM->kabCDSEdit;
      FDataSrc->DataSet->OnDelete       = FDM->kabCDSDelete;
      ClassGridView->BeginUpdate();
      try
       {
        FGetData();
        //FDataSrc->DataSet->GetData("", FDM->ClassFetchAll, FDM->ClassFetchRecCount, FGetDataProgress);
        FDataSrc->CreateColumns(ClassGridView);
        ClassGridView->DataController->CustomDataSource = FDataSrc;
        FDataSrc->DataChanged();
       }
      __finally
       {
        ClassGridView->EndUpdate();
       }
      if (curCD)
       {
        TLocateOptions opt;
        //if(!quClass()->Locate("CODE",Variant(curCD),opt))
        //quClass()->First();
       }
      //else
      //quClass()->First();
      ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DataSource = FDataSrc;
      xcTop = 1;
      int FPC = ExtListPC->PageCount - 1;
      for (int i = FPC; i >= 0; i--)
       {
        FCurPage              = ExtListPC->Pages[i];
        FCurPage->PageControl = NULL;
        delete FCurPage;
       }
      _CLASSNODE_()->Iterate(CreateListExt);
      if (!FCtrList->LabCount)
       Split->CloseSplitter();
      else
       Split->OpenSplitter();
      StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
      //CorrectCount(FDataSrc->DataSet->RecordCount);
      //if (SetTmpl->Visible)
      //StatusSB->Panels->Items[3]->Text = CorrectCount(FDataSrc->DataSet->RecordCount);
      //else
      //StatusSB->Panels->Items[3]->Text = "";
     }
    catch (Exception & E)
     {
      _MSG_ERR(FMT1(icsErrorMsgCaptionSys, E.Message), FMT(icsErrorMsgCaption));
     }
    catch (...)
     {
      _MSG_ERR(FMT(icsRegistryErrorCommDBErrorMsgCaption), FMT(icsErrorMsgCaption));
     }
   }
  __finally
   {
    CheckCtrlState(true);
    Application->ProcessMessages();
    //if (quClass()->Active)
    //{
    //if (FDM->UseAfterScroll)
    //quClass()->AfterScroll  = ListAfterScroll;
    //else
    //quClass()->OnEndScroll  = ListAfterScroll;
    //ListAfterScroll(quClass());
    //}
    //if (FDM->UseClassDisControls)
    //quClass()->EnableControls();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::FGetDataProgress(int AMax, int ACur, UnicodeString  AMsg)
 {
  if (AMax == -1)
   StatusSB->Panels->Items[2]->Text = "";
  else if (!(AMax + ACur))
   StatusSB->Panels->Items[2]->Text = AMsg;
  else
   StatusSB->Panels->Items[2]->Text = AMsg + " [" + IntToStr(ACur) + "/" + IntToStr(AMax) + "]";
  Application->ProcessMessages();
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_INSERT && actInsert->Enabled)
   actInsert->OnExecute(this);
  if (Key == VK_RETURN)
   {
    if (actEdit->Enabled && Shift.Contains(ssShift))
     actEdit->OnExecute(this);
    else if (ActiveControl == ClassTL)
     actClassSelectExecute(actClassSelect);
   }
  if (Key == VK_DELETE && actDelete->Enabled)
   actDelete->OnExecute(this);
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::CreateTV(TTagNode * AClassRoot)
 {
  ClassTL->Clear();
  TTagNode * itNode = AClassRoot->GetFirstChild();
  while (itNode)
   {
    UnicodeString Group = itNode->AV["group"];
    if (!Group.Length())
     Group = "0";
    if (Group.Length())
     {
      if (FGroups[Group] == "1")
       {
        FRootNode           = ClassTL->Root->AddChild();
        FRootNode->Texts[0] = itNode->AV["name"];
        FRootNode->Data     = (void *)UIDInt(itNode->AV["uid"]);
        itNode->ReIterateGroup(CreateClassTV);
       }
     }
    itNode = itNode->GetNext();
   }
 }
//---------------------------------------------------------------------------
bool __fastcall TdsClassifClientForm::CreateClassTV(TTagNode * itTag)
 {
  TcxTreeListNode * FNode;
  if (itTag->CmpName("class"))
   {
    UnicodeString InList = itTag->GetAVDef("inlist", "n").LowerCase();
    if (!FDM->FullEdit)
     {
      switch (FShowType)
       {
       case ctAll:
        break;
       case ctList:
         {
          if (InList != "l")
           return false;
          break;
         }
       case ctNone:
         {
          if (InList != "n")
           return false;
          break;
         }
       }
     }
    FNode = FRootNode->AddChild();
    FNode->Texts[0] = itTag->AV["name"];
    FNode->Data     = itTag;
   }
  return false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::FormDestroy(TObject * Sender)
 {
  delete FSearchList;
  //ViewParamPC->ActivePage = ClassParamTS;
  if (FCtrList)
   {
    if (FCtrList->DataProvider)
     delete FCtrList->DataProvider;
    delete FCtrList;
   }
  FCtrList = NULL;
  //if (quClass()->Active) quClass()->Close();
  //if (quClass()->Transaction->Active) quClass()->Transaction->Rollback();
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::ListAfterScroll(TDataSet * DataSet)
 {
  FCtrList->LabDataChange();
 }
//---------------------------------------------------------------------------
bool __fastcall TdsClassifClientForm::CreateListExt(TTagNode * itTag)
 {
  if ((itTag->CmpName("group") && itTag->GetParent()->CmpName("unit")) || itTag->CmpName("class"))
   {
    //TcxTabSheet *Page;
    //ExtPanelList->Add((void*) new TPanel(this));
    //CurExtList = (TPanel*)ExtPanelList->Items[ExtPanelList->Count-1];
    FCurPage              = new TcxTabSheet(this);
    FCurPage->Caption     = itTag->AV["name"];
    FCurPage->PageControl = ExtListPC;
    FCurPage->Color       = clBtnFace;
    FCurPage->BorderWidth = 3;
    FCurPage->ImageIndex  = FCurPage->TabIndex;
    xcTop                 = 0;
   }
  TdsRegLabItem * tmpItem;
  if (!itTag->CmpName("class,description,actuals"))
   {
    if (itTag->AV["inlist"].LowerCase() == "e")
     {
      tmpItem      = FCtrList->AddLabItem(itTag, FCurPage->Width, FCurPage);
      tmpItem->Top = xcTop;
      xcTop += tmpItem->Height;
      //if (itTag->CmpName("extedit"))
      //tmpItem->OnGetExtData = FDM->OnGetExtData;
     }
   }
  return false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::actInsertExecute(TObject * Sender)
 {
  /*if (FDM->OnRegEditButtonClick)
   {
   if (!FDM->OnRegEditButtonClick(bcInsert))  return;
   }*/
  //if (!quClass()->Active)              return;
  if (!actInsert->Enabled)
   return;
  //if (!quClass()->Transaction->Active) return;
  TdsClassEditClientForm * Dlg = NULL;
  TTagNode * EditNode = NULL;
  bool CanRefresh, CanCancel, CanCont;
  try
   {
    CanRefresh = true;
    CanCancel  = true;
    DisableAfterScroll();
    EditNode = new TTagNode(NULL);
    EditNode->Assign(_CLASSNODE_(), true);
    try
     {
      CanCont = true;
      bool CanReopen = false;
      if (FDM->OnExtInsertData)
       {
        __int64 FCode;
        FDM->OnExtInsertData(EditNode, FDataSrc, 0, CanCont, CanReopen, FCode);
        if (CanReopen)
         curCD = FCode;
       }
      if (CanCont)
       {
        //FDataSrc->DataSet->Insert();
        //quClass()->Value("CODE")->AsInteger = FSaveCode;
        Dlg = new TdsClassEditClientForm(this, true, EditNode, FDM, _CLASSNODE_()->GetRoot(), FDataSrc,
            FCtrList->TemplateData, FDM->ClsSingleEditCol);
        if (Dlg)
         {
          if (Dlg->ShowModal() == mrOk)
           {
            CanRefresh = true;
            CanCancel  = false;
            curCD      = Dlg->InsertedRecId.ToIntDef(0);
           }
          else
           CanRefresh = CanReopen;
         }
       }
      else
       {
        CanRefresh = CanReopen;
        CanCancel  = false;
       }
     }
    catch (Exception & E)
     {
      _MSG_ERR(FMT1(icsRegErrorInsertRecSys, E.Message), FMT(icsErrorMsgCaption));
     }
    catch (...)
     {
      _MSG_ERR(FMT(icsRegErrorInsertRec), FMT(icsErrorMsgCaption));
     }
   }
  __finally
   {
    if (Dlg)
     delete Dlg;
    if (EditNode)
     delete EditNode;
    if (CanCancel)
     {
      //if (quClass()->State != dsBrowse)
      FDataSrc->DataSet->Cancel();
     }
    if (CanRefresh)
     {
      if (FDM->OnClassAfterInsert)
       FDM->OnClassAfterInsert(_CLASSNODE_());
      RefreshById(IntToStr(curCD));
     }
    EnableAfterScroll();
   }
  SetActiveCtrl();
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::actEditExecute(TObject * Sender)
 {
  //if (FDM->SQLBeforeEdit)
  //FDM->SQLBeforeEdit("CLASS_"+FDM->TablePrefix+_CLASSNODE_()->AV["uid"], quClass()->Value("CODE")->AsString,_CLASSNODE_()->AV["uid"]);
  //if (FDM->OnRegEditButtonClick)
  //{
  //if (!FDM->OnRegEditButtonClick(bcEdit)) return;
  //}
  //if (!quClass()->Active) return;
  if (!actEdit->Enabled)
   return;
  //if (!quClass()->Transaction->Active) return;
  //if (quClass()->RecordCount < 1) return;
  TTagNode * EditNode = NULL;
  TdsClassEditClientForm * Dlg = NULL;
  bool CanRefresh, CanCancel, CanCont;
  try
   {
    CanRefresh = true;
    CanCancel  = true;
    DisableAfterScroll();
    EditNode = new TTagNode(NULL);
    EditNode->Assign(_CLASSNODE_(), true);
    try
     {
      CanCont = true;
      bool CanReopen = false;
      if (FDM->OnExtEditData)
       {
        __int64 FCode;
        FDM->OnExtEditData(EditNode, FDataSrc, FDataSrc->DataSet->CurRow->Value["CODE"], CanCont, CanReopen, FCode);
        if (CanReopen)
         curCD = FCode;
       }
      if (CanCont)
       {
        Dlg = new TdsClassEditClientForm(this, false, EditNode, FDM, _CLASSNODE_()->GetRoot(), FDataSrc,
            FCtrList->TemplateData, FDM->ClsSingleEditCol);
        if (Dlg)
         {
          if (Dlg->ShowModal() == mrOk)
           {
            //curCD = quClass()->Value("CODE")->AsInteger;
            CanRefresh = true;
            CanCancel  = false;
            curCD      = Dlg->InsertedRecId.ToIntDef(0);
           }
          else
           CanRefresh = CanReopen;
         }
       }
      else
       {
        CanRefresh = CanReopen;
        CanCancel  = false;
       }
     }
    catch (Exception & E)
     {
      _MSG_ERR(FMT1(icsRegErrorEditRecSys, E.Message), FMT(icsErrorMsgCaption));
     }
    catch (...)
     {
      _MSG_ERR(FMT(icsRegErrorEditRec), FMT(icsErrorMsgCaption));
     }
   }
  __finally
   {
    if (Dlg)
     delete Dlg;
    if (EditNode)
     delete EditNode;
    if (CanCancel)
     {
      /*if (quClass()->State != dsBrowse)*/
      FDataSrc->DataSet->Cancel();
     }
    if (CanRefresh)
     {
      if (FDM->OnClassAfterEdit)
       FDM->OnClassAfterEdit(_CLASSNODE_());
      RefreshById(IntToStr(curCD));
     }
    EnableAfterScroll();
   }
  SetActiveCtrl();
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::actDeleteExecute(TObject * Sender)
 {
  /*
   if (FDM->OnRegEditButtonClick)
   {
   if (!FDM->OnRegEditButtonClick(bcDelete)) return;
   }

   __int64 xfCode;
   TTagNode* EditNode = NULL;
   if (!quClass()->Active) return;
   */
  if (!actDelete->Enabled)
   return;
  /*
   if (!quClass()->Transaction->Active) return;
   if(quClass()->RecordCount < 1) return;
   */
  bool CanRefresh, CanCancel, CanCont;
  try
   {
    CanRefresh = true;
    CanCancel  = true;
    DisableAfterScroll();
    try
     {
      //xfCode = (__int64)quClass()->Value("CODE")->AsInteger;
      CanCont = true;
      /*
       if (FDM->OnExtDeleteData)
       {
       bool CanReopen = false;
       __int64 FCode;
       FDM->OnExtDeleteData(EditNode,FDM->DM->srcReg,xfCode,CanCont,CanReopen,FCode);
       if (CanReopen)
       {
       curCD = FCode;
       if (quClass()->Transaction->Active)
       quClass()->Transaction->Commit();
       ChangeSrc();
       if (FDM->OnClassAfterDelete) FDM->OnClassAfterDelete(_CLASSNODE_());

       }
       }
       */
      if (CanCont)
       {
        //curCD = quClass()->Value("CODE")->AsInteger;
        int FRecIdx = FDataSrc->CurrentRecIdx;
        UnicodeString Str = "";
        UnicodeString FFlId;
        if (FRecIdx >= 0)
         {
          if (_CLASSDESCR_()->AV["confirmlabel"].Trim().Length())
           {//���� confirmlabel;
            TStringList * FConfMsgList = new TStringList;
            try
             {
              FConfMsgList->Delimiter     = '#';
              FConfMsgList->QuoteChar     = '~';
              FConfMsgList->DelimitedText = _CLASSDESCR_()->AV["confirmlabel"].Trim();
              for (int i = 0; i < FConfMsgList->Count; i++)
               {
                if (FConfMsgList->Strings[i].Length())
                 {
                  if (FConfMsgList->Strings[i][1] == '@')
                   {
                    FFlId = FConfMsgList->Strings[i].SubString(2, 4);
                    //if (_CLASSDESCR_()->GetTagByUID(FFlId)->CmpName("binary,choice,choiceDB,extedit,choiceTree"))
                    Str += " " + FDataSrc->DataSet->CurRow->StrValue[FFlId];
                    //else
                    //Str += " "+FDataSrc->DataSet->CurRow->Value[FFlId] ;
                   }
                  else
                   {
                    Str += " " + FConfMsgList->Strings[i];
                   }
                 }
               }
             }
            __finally
             {
              delete FConfMsgList;
             }
           }
          else
           {
            TTagNode * itFl = FDataSrc->DataSet->ListDefNode->GetFirstChild();
            while (itFl)
             {
              if (Str.Length())
               Str += ", ";
              //if (_CLASSDESCR_()->GetTagByUID(itFl->AV["uid"])->CmpName("binary,choice,choiceDB,extedit,choiceTree"))
              Str += FDataSrc->DataSet->CurRow->StrValue[itFl->AV["uid"]];
              //else
              //Str += FDataSrc->DataSet->CurRow->Value[itFl->AV["uid"]];
              itFl = itFl->GetNext();
             }
            Str = FMT(icsRegErrorConfirmDeleteRec) + " " + _CLASSDESCR_()->AV["deletelabel"].LowerCase() + "\n\n\" " +
                Str + "\"\n";
           }
         }
        if (_MSG_QUE(Str, FMT(icsRegErrorConfirmDeleteRecMsgCaption)) == IDYES)
         {
          FDataSrc->DataSet->Delete();
          CanRefresh = true;
          //if (quClass()->Active && quClass()->RecordCount)
          //curCD = quClass()->Value("CODE")->AsInteger;
          CanCancel = false;
         }
        else
         {
          CanRefresh = false;
          CanCancel  = false;
         }
       }
      else
       {
        CanRefresh = false;
        CanCancel  = false;
       }
     }
    catch (Exception & E)
     {
      _MSG_ERR(FMT1(icsRegErrorDeleteRecSys, E.Message), FMT(icsErrorMsgCaption));
     }
    catch (...)
     {
      _MSG_ERR(FMT(icsRegErrorDeleteRec), FMT(icsErrorMsgCaption));
     }
   }
  __finally
   {
    if (CanCancel)
     {
      FDataSrc->DataSet->Cancel();
     }
    if (CanRefresh)
     {
      if (FDM->OnClassAfterDelete)
       FDM->OnClassAfterDelete(_CLASSNODE_());
      actRefreshExecute(actRefresh);
     }
    EnableAfterScroll();
   }
  SetActiveCtrl();
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::SetFiltered(bool AVal)
 {
  //SetTmpl->Visible = AVal;
  //ResetTmpl->Visible = !AVal;
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::FSetFilterExecute(UnicodeString AFilterGUI)
 {
  if (!actSetFilter->Checked)
   {
    if (FDM->OnExtFilterSet)
     {
      bool FCanSet = false;
      UnicodeString FTabSel = "";
      FFilter = FDM->OnExtFilterSet(_CLASSNODE_(), FCanSet, FFilter);
      if (FCanSet)
       {
        actSetFilter->Checked = true;
        SetFiltered(true);
        //ListReopen();
       }
     }
   }
  else
   {
    actSetFilter->Checked = false;
    SetFiltered(false);
    //ListReopen();
   }
  SetActiveCtrl();
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::actSetFilterExecute(TObject * Sender)
 {
  FSetFilterExecute();
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::actReSetFilterExecute(TObject * Sender)
 {
  SetFiltered(false);
  FFilter               = "";
  actSetFilter->Checked = false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::FGetData()
 {
  //TdsRegListType { ltFull = 0, ltFilter = 1, ltSearchFilter = 2};
  //FDataSrc->DataSet->GetData("", FDM->ClassFetchAll, FDM->ClassFetchRecCount, FGetDataProgress);
  if (FDM->ClassListOpenType == TdsRegListType::ltFull)
   {
    if (actSetFilter->Checked)
     {
      if (!FFilter.Trim().Length())
       ClearData();
      else
       FDataSrc->DataSet->GetData(FFilter, FDM->FetchAll, FDM->FetchRecCount, FGetDataProgress);
     }
    else
     FDataSrc->DataSet->GetData("", FDM->FetchAll, FDM->FetchRecCount, FGetDataProgress);
   }
  else if (FDM->ClassListOpenType == TdsRegListType::ltFilter)
   {
    if (actSetFilter->Checked)
     {
      if (!FFilter.Trim().Length())
       ClearData();
      else
       FDataSrc->DataSet->GetData(FFilter, FDM->FetchAll, FDM->FetchRecCount, FGetDataProgress);
     }
    else
     ClearData();
   }
  else if (FDM->ClassListOpenType == TdsRegListType::ltSearchFilter)
   ClearData();
  //FDataSrc->DataSet->GetData(NULL, FDM->ClassFetchAll, FDM->ClassFetchRecCount, FGetDataProgress);
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::ClearData()
 {
  FDataSrc->DataSet->Clear();
  StatusSB->Panels->Items[1]->Text = "";
  StatusSB->Panels->Items[2]->Text = "";
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::ClearTemplate()
 {
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::actRefreshExecute(TObject * Sender)
 {
  ClassGridView->BeginUpdate();
  CheckCtrlState(false);
  try
   {
    FGetData();
    //FDataSrc->DataSet->GetData(NULL, FDM->ClassFetchAll, FDM->ClassFetchRecCount, FGetDataProgress);
    FDataSrc->DataChanged();
    StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
   }
  __finally
   {
    CheckCtrlState(true);
    ClassGridView->EndUpdate();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::RefreshById(UnicodeString  ACode)
 {
  ClassGridView->BeginUpdate();
  CheckCtrlState(false);
  try
   {
    FDataSrc->DataSet->GetDataById(ACode, FGetDataProgress);
    FDataSrc->DataChanged();
    StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
   }
  __finally
   {
    ClassGridView->EndUpdate();
    CheckCtrlState(true);
    ClassGridView->DataController->FocusedRecordIndex = FDataSrc->DataSet->GetRowIdxByCode(ACode);
   }
 }
//---------------------------------------------------------------------------
TcxGridColumn * __fastcall TdsClassifClientForm::ClassCol(int AIdx)
 {
  return ClassGridView->Columns[AIdx];
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::DisableAfterScroll()
 {
  /*
   if (FDM->UseAfterScroll)
   quClass()->AfterScroll = NULL;
   else
   quClass()->OnEndScroll = NULL;
   */
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::EnableAfterScroll()
 {
  /*
   if (quClass()->Active)
   {
   if (FDM->UseAfterScroll)
   quClass()->AfterScroll = ListAfterScroll;
   else
   quClass()->OnEndScroll = ListAfterScroll;
   }
   */
 }
//---------------------------------------------------------------------------
TTagNode * __fastcall TdsClassifClientForm::_CLASSNODE_()
 {
  return FSelectedNode;
 }
//---------------------------------------------------------------------------
TTagNode * __fastcall TdsClassifClientForm::_CLASSDESCR_()
 {
  return FSelectedNodeDescr;
 }
//---------------------------------------------------------------------------
bool __fastcall TdsClassifClientForm::GetBtnEn(UnicodeString AAttrName)
 {
  return ((bool)(_CLASSDESCR_()->AV[AAttrName].Length()));
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::actGetClassListExecute(TObject * Sender)
 {
  if (ViewParamPC->ActivePage == ClassListTS)
   {
    CheckCtrlState(true);
    if (_CLASSNODE_())
     {
      Caption = SaveCaption + " " + _CLASSNODE_()->AV["name"];
     }
    ViewParamPC->ActivePage = ClassParamTS;
   }
  else
   {
    Caption = "����� �����������";
    CheckCtrlState(false, true);
    SearchBE->Visible = false;
    SearchBE->Enabled = false;
    Split->OpenSplitter();
    ViewParamPC->ActivePage = ClassListTS;
    bool FSelected = false;
    if (!_CLASSNODE_())
     {
      TTagNode * tmpNode = new TTagNode(NULL);
      try
       {
        tmpNode->Name = "tmp";
        for (int i = 0; i < ClassTL->Root->Count; i++)
         {
          tmpNode->AV["uid"] = UIDStr((int)ClassTL->Root->Items[i]->Data);
          if (tmpNode->CmpAV("uid", FDM->ClassShowExpanded))
           {
            if (!FSelected)
             {
              ClassTL->Root->Items[i]->Focused = true;
              FSelected                        = true;
             }
            ClassTL->Root->Items[i]->Expand(true);
           }
         }
       }
      __finally
       {
        if (tmpNode)
         delete tmpNode;
       }
     }
    ActiveControl = ClassTL;
    //SetActiveCtrl();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::actClassSelectExecute(TObject * Sender)
 {
  if (ClassTL->SelectionCount)
   {
    if (ClassTL->Selections[0]->Level)
     {
      SearchBE->Visible  = true;
      SearchBE->Enabled  = true;
      curCD              = 0;
      FSelectedNode      = (TTagNode *)ClassTL->Selections[0]->Data;
      FSelectedNodeDescr = FSelectedNode->GetChildByName("description");
      CheckCtrlState(true);
      ViewParamPC->ActivePage = ClassParamTS;
      //ClearTemplate();
      Application->ProcessMessages();
      ChangeSrc();
      if (FDM->ClassListOpenType == TdsRegListType::ltFull)
       actRefreshExecute(actRefresh);
      else if (FDM->ClassListOpenType == TdsRegListType::ltFilter)
       FSetFilterExecute();
      Application->ProcessMessages();
      SetActiveCtrl();
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::actExitExecute(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::actTemplateExecute(TObject * Sender)
 {
  if (!actTemplate->Checked)
   {
    UnicodeString tmpl = GetTemplateFields(_CLASSDESCR_());
    if (tmpl.Length())
     {
      TdsRegTemplateForm * TmpDlg = NULL;
      try
       {
        TmpDlg = new TdsRegTemplateForm(this, FDM, tmpl, FCtrList->TemplateData);
        if (TmpDlg)
         {
          TmpDlg->ShowModal();
          if (TmpDlg->ModalResult == mrOk)
           {
            if (!TmpDlg->IsEmpty())
             {
              FCtrList->TemplateData = TmpDlg->TemplateData;
              actTemplate->Checked   = true;
              FCtrList->LabDataChange();
             }
           }
         }
       }
      __finally
       {
        if (TmpDlg)
         delete TmpDlg;
       }
     }
   }
  else
   {
    actTemplate->Checked = false;
    try
     {
      FCtrList->TemplateData->Clear();
      FCtrList->LabDataChange();
     }
    __finally
     {
     }
   }
  SetActiveCtrl();
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::ClassTLClick(TObject * Sender)
 {
  if (ClassTL->HitTest->HitAtNode && !ClassTL->HitTest->HitAtIndent)
   actClassSelectExecute(actClassSelect);
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::ClassGridViewFocusedRecordChanged(TcxCustomGridTableView * Sender,
    TcxCustomGridRecord * APrevFocusedRecord, TcxCustomGridRecord * AFocusedRecord, bool ANewItemRecordFocusingChanged)
 {
  if (IsRecSelected())
   FDataSrc->DataSet->GetDataById(VarToStr(FDataSrc->DataSet->CurRow->Value["CODE"]), FGetDataProgress);
  FCtrList->LabDataChange();
  actEdit->Enabled   = IsRecSelected() && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("editlabel"));
  actDelete->Enabled = IsRecSelected() && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("deletelabel"));
 }
//---------------------------------------------------------------------------
bool __fastcall TdsClassifClientForm::IsRecSelected()
 {
  return ClassGridView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1);
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::CheckCtrlState(bool AEnable, bool AClassSelect)
 {
  if (AEnable)
   {
    ProgressPanel->Visible = false;
    ProgressPanel->Width   = 0;
    ProgressPanel->Height  = 0;
   }
  else
   {
    if (!AClassSelect)
     {
      ProgressPanel->Left    = 0;
      ProgressPanel->Top     = 0;
      ProgressPanel->Width   = ClientWidth;
      ProgressPanel->Height  = ClientHeight - 25;
      ProgressPanel->Visible = true;
      ProgressPanel->BringToFront();
     }
   }
  if (_CLASSNODE_())
   {
    actInsert->Enabled = AEnable && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("insertlabel"));
    if (!AEnable)
     {
      actEdit->Enabled   = false;
      actDelete->Enabled = false;
     }
    else if (ClassGridView->DataController->CustomDataSource && FDataSrc)
     {
      if (FDataSrc->DataSet->RecordCount && (FDataSrc->CurrentRecIdx != -1))
       {
        actEdit->Enabled   = AEnable && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("editlabel"));
        actDelete->Enabled = AEnable && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("deletelabel"));
       }
      else
       {
        actEdit->Enabled   = false;
        actDelete->Enabled = false;
       }
     }
    actRefresh->Enabled = AEnable;
    actTemplate->Enabled         = AEnable;
    actSetFilter->Enabled        = AEnable;
    actReSetFilter->Enabled      = AEnable;
    actFind->Enabled             = AEnable;
    actFindAndFilter->Enabled    = AEnable;
    actClearSearchField->Enabled = AEnable;
   }
  else
   {
    actInsert->Enabled           = false;
    actEdit->Enabled             = false;
    actDelete->Enabled           = false;
    actRefresh->Enabled          = false;
    actTemplate->Enabled         = false;
    actSetFilter->Enabled        = false;
    actReSetFilter->Enabled      = false;
    actFind->Enabled             = false;
    actFindAndFilter->Enabled    = false;
    actClearSearchField->Enabled = false;
   }
  /*

   actTemplate->Enabled          = AEnable && actTemplate->Visible && actInsert->Enabled;//&&actEdit->Enabled&&actDelete->Enabled;
   actSetFilter->Enabled         = AEnable && actSetFilter->Visible;
   actReSetFilter->Enabled       = AEnable && actReSetFilter->Visible;
   actFilterSetting->Enabled     = AEnable && actFilterSetting->Visible;

   actOpen->Enabled              = AEnable && actOpen->Visible;
   actExt->Enabled               = AEnable && actExt->Visible;
   actSelAll->Enabled            = AEnable && actSelAll->Visible;

   PrintBI->Enabled              = AEnable;
   actPrintListSetting->Enabled  = AEnable && actPrintListSetting->Visible;
   SearchBE->Enabled             = AEnable;
   //  StatusSB->Enabled             = AEnable;
   //  ClassGrid->Enabled            = AEnable;
   */
  /*
   if (_CLASSDESCR_()->GetChildByName("maskfields") == NULL)
   {
   actTemplate->Enabled = false;
   actSetFilter->Enabled = false;
   actReSetFilter->Enabled = false;
   }
   else
   {
   actTemplate->Enabled = actInsert->Enabled;
   actSetFilter->Enabled = true;
   actReSetFilter->Enabled = true;
   }
   */
  /*
   actInsert->Enabled = FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("insertlabel"));
   if (FDataSrc->DataSet->RecordCount && (FDataSrc->CurrentRecIdx != -1))
   {
   actEdit->Enabled   = FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("editlabel"));
   actDelete->Enabled = FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("deletelabel"));
   }
   else
   {
   actEdit->Enabled = false;
   actDelete->Enabled = false;
   }
   */
  /*actInsert->Enabled            = FDM->ClassEditable && (SaveEnInsert&&_CLASSNODE_());
   actEdit->Enabled              = FDM->ClassEditable && (SaveEnEdit&&_CLASSNODE_());
   actDelete->Enabled            = FDM->ClassEditable && (SaveEnDelete&&_CLASSNODE_());
   actTemplate->Enabled          = SaveEnSetTemplate&&_CLASSNODE_()&&actInsert->Enabled;
   actSetFilter->Enabled         = SaveEnSetTemplate&&_CLASSNODE_();
   actReSetFilter->Enabled       = SaveEnReSetTemplate&&_CLASSNODE_();
   actRefresh->Enabled           = SaveEnRefresh&&_CLASSNODE_();
   actFindNext->Enabled          = SaveEnFindNext&&_CLASSNODE_();
   */
  /*actInsert->Enabled            = AEnable && FDM->ClassEditable && (SaveEnInsert&&_CLASSNODE_());
   actEdit->Enabled              = AEnable && FDM->ClassEditable && (SaveEnEdit&&_CLASSNODE_());
   actDelete->Enabled            = AEnable && FDM->ClassEditable && (SaveEnDelete&&_CLASSNODE_());
   actRefresh->Enabled           = AEnable && SaveEnRefresh&&_CLASSNODE_();
   actFind->Enabled              = AEnable && SaveEnFind&&_CLASSNODE_();
   actTemplate->Enabled          = AEnable && SaveEnSetTemplate&&_CLASSNODE_()&&actInsert->Enabled&&actEdit->Enabled&&actDelete->Enabled;
   actSetFilter->Enabled         = AEnable && SaveEnSetTemplate&&_CLASSNODE_();
   actReSetFilter->Enabled       = AEnable && SaveEnReSetTemplate&&_CLASSNODE_();
   actOpen->Enabled              = AEnable && SaveEnReSetTemplate&&_CLASSNODE_();
   actSelAll->Enabled            = AEnable && SaveEnReSetTemplate&&_CLASSNODE_();
   actFilterSetting->Enabled     = AEnable && SaveEnReSetTemplate&&_CLASSNODE_();
   actPrintListSetting->Enabled  = AEnable && SaveEnReSetTemplate&&_CLASSNODE_();
   Application->ProcessMessages();*/
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::actFindExecute(TObject * Sender)
 {
  if (SearchBE->Text.Trim().Length() && (SearchBE->Text.Trim().UpperCase() != "�����"))
   {
    ClassGridView->BeginUpdate();
    int FRecIdx, FColIdx;
    bool FExist = true;
    bool FContinueSearch = true;
    if (FSearchModify)
     {
      FFetchAllRec();
      FSearchList->Delimiter     = ' ';
      FSearchList->DelimitedText = SearchBE->Text.Trim();
      FSearchModify              = false;
     }
    try
     {
      for (FRecIdx = FSearchIdx; (FRecIdx < ClassGridView->DataController->RecordCount) && FContinueSearch; FRecIdx++)
       {
        FExist = true;
        for (FColIdx = 0; (FColIdx < ClassGridView->DataController->ItemCount) && (FColIdx < FSearchList->Count)
            && FExist; FColIdx++)
         {
          FExist &= AnsiContainsText(ClassGridView->DataController->DisplayTexts[FRecIdx][FColIdx],
              FSearchList->Strings[FColIdx]);
         }
        if (FExist)
         {
          FSearchIdx      = FRecIdx + 1;
          FContinueSearch = false;
          ClassGridView->Controller->ClearSelection();
          ClassGridView->DataController->FocusedRecordIndex = FRecIdx;
          if (ClassGridView->Controller->FocusedRecord)
           {
            ClassGridView->Controller->FocusedRecord->Selected = true;
            ClassGridView->Controller->FocusedRecord->MakeVisible();
           }
         }
       }
      if (FRecIdx >= ClassGridView->DataController->RecordCount)
       FSearchIdx = 0;
     }
    __finally
     {
      ClassGridView->EndUpdate();
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::actFindAndFilterExecute(TObject * Sender)
 {
  if (FDM->OnFind && SearchBE->Text.Trim().Length() && !FSearchAndFilter &&
      (SearchBE->Text.Trim().UpperCase() != "�����"))
   {
    TJSONObject * FRC;
    if (FDM->OnFind("reg." + _CLASSNODE_()->AV["uid"], SearchBE->Text.Trim(), FRC))
     {
      FDataSrc->DataSet->FilterByFind((TJSONArray *)FRC, FDM->ClassFetchAll, FDM->ClassFetchRecCount, FGetDataProgress);
      FDataSrc->DataChanged();
      FSearchAndFilter = true;
     }
   }
  //SearchBEPropertiesButtonClick(SearchBE, 1);
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::actClearSearchFieldExecute(TObject * Sender)
 {
  if (SearchBE->Text.Trim().Length())
   {
    SearchBE->Clear();
    actRefreshExecute(actRefresh);
   }
  //SearchBEPropertiesButtonClick(SearchBE, 2);
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::SearchBEPropertiesChange(TObject * Sender)
 {
  FSearchIdx       = 0;
  FSearchModify    = true;
  FSearchAndFilter = false;
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::ClassGridViewColumnHeaderClick(TcxGridTableView * Sender, TcxGridColumn * AColumn)
 {
  FFetchAllRec(); //FDataSrc->DataSet->FetchAll(FDM->FetchRecCount, FGetDataProgress);
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::FFetchAllRec()
 {
  if (!FDataSrc->DataSet->IsAllFetched())
   {
    CheckCtrlState(false);
    try
     {
      FDataSrc->DataSet->FetchAll(FDM->ClassFetchRecCount, FGetDataProgress);
     }
    __finally
     {
      CheckCtrlState(true);
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::FormActivate(TObject * Sender)
 {
  if (SearchBE->Enabled)
   {
    SearchBE->Clear();
    SearchBE->SetFocus();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TdsClassifClientForm::SetActiveCtrl()
 {
  if (Visible)
   {
    if (SearchBE->Enabled && SearchBE->Visible)
     SearchBE->SetFocus();
    else
     ClassGrid->SetFocus();
   }
 }
//---------------------------------------------------------------------------
