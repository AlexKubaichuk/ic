//---------------------------------------------------------------------------
#ifndef dsRegTreeClassH
#define dsRegTreeClassH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtonEdit.hpp"
#include "cxButtons.hpp"
#include "cxCheckBox.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxProgressBar.hpp"
#include "cxTextEdit.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
#include "dxSkinsdxStatusBarPainter.hpp"
#include "dxStatusBar.hpp"
#include "cxCustomData.hpp"
#include "cxInplaceContainer.hpp"
#include "cxStyles.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "cxClasses.hpp"
//---------------------------------------------------------------------------
class PACKAGE TdsRegTreeClassForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TLabel *FindLab;
        TPanel *TmplPanel;
        TcxCheckBox *SelGroupChB;
        TcxButtonEdit *QSearchED;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        TdxStatusBar *StatusBar;
        TdxStatusBarContainerControl *dxStatusBar1Container0;
        TcxProgressBar *PrBar;
 TcxTreeList *ClassTree;
 TcxTreeListColumn *ExtCodeCol;
 TcxTreeListColumn *TextCol;
 TcxTreeListColumn *CodeCol;
 TcxTreeListColumn *CanSelectCol;
 TcxStyleRepository *FormStyle;
 TcxStyle *oddStyle;
 TcxStyle *bStyle;
 TcxStyle *cxStyle1;
 TTimer *SearchTimer;
 TcxTreeListColumn *NameCol;
        void __fastcall BtnOkClick(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall QSearchEDPropertiesButtonClick(TObject *Sender,
          int AButtonIndex);
 void __fastcall ClassTreeStylesGetContentStyle(TcxCustomTreeList *Sender, TcxTreeListColumn *AColumn,
          TcxTreeListNode *ANode, TcxStyle *&AStyle);
 void __fastcall ClassTreeFocusedNodeChanged(TcxCustomTreeList *Sender, TcxTreeListNode *APrevFocusedNode,
          TcxTreeListNode *AFocusedNode);
 void __fastcall QSearchEDPropertiesChange(TObject *Sender);
 void __fastcall SearchTimerTimer(TObject *Sender);
private:	// User declarations
    TcxTreeListNode *FFirstSN;
    UnicodeString FclText, FclExtCode;

    __int64       FclCode, FclCodeLast;
    bool          FclSelectGroup;

    void __fastcall TagNodeToTreeNode(TcxTreeListNode *ADest, TTagNode *ASrc);
    bool __fastcall FGetTemplateMode();
    void __fastcall FSetTemplateMode(bool AMode);
    void __fastcall FSetClCode(__int64 AVal);
    void __fastcall FSearchText(UnicodeString AText, TcxTreeListColumn *AColumn);
    TcxTreeListNode* __fastcall GetLastGroupNode(TcxTreeListNode* ARoot);

public:
       __fastcall TdsRegTreeClassForm(TComponent* Owner, TTagNode *ADef);

    __property __int64       clCode     = {read = FclCode, write=FSetClCode};
    __property __int64       clCodeLast = {read = FclCodeLast};
    __property UnicodeString clText     = {read = FclText};
    __property UnicodeString clExtCode  = {read = FclExtCode};

    __property bool TemplateMode        = {read=FGetTemplateMode, write=FSetTemplateMode};
    __property bool clSelectGroup       = {read = FclSelectGroup};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsRegTreeClassForm *dsRegTreeClassForm;
//---------------------------------------------------------------------------
#endif
