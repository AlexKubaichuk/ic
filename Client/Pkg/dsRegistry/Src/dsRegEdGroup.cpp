﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop
#include "dsRegEdGroup.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
// ---------------------------------------------------------------------------
__fastcall TriGroup::TriGroup(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner, bool IsApp) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner, IsApp)
 {
  FLab          = new TLabel(this);
  FLab->Parent  = this;
  FLab->Visible = false;
  ED            = new TcxGroupBox(this);
  ED->Parent    = this;
  // ED->Transparent = true;
  ED->Style->StyleController = FCtrlOwner->StyleController;
  ED->Style->Font->Style     = TFontStyles() << fsBold;
  ED->CaptionBkColor         = (TColor)0x00C08000;
  // ED->Style->TextColor = (TColor)0x00C08000;
  ED->Caption = " " + FNode->AV["name"] + " ";
  ED->Align   = alClient;
  CMaxH       = 500;
  CMinH       = 22;
  Width       = 135;
  // if (FNode->GetParent()->CmpName("group"))  Width = 130;
  // else                                       Width = 135;
 }
// ---------------------------------------------------------------------------
TcxGroupBox * __fastcall TriGroup::FGetCastED()
 {
  return (TcxGroupBox *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriGroup::FSetCastED(TcxGroupBox * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriGroup::SetOnDependValues()
 {
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
__fastcall TriGroup::~TriGroup()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
 }
// ---------------------------------------------------------------------------
void __fastcall TriGroup::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign = true;
  // ED->Left = ALeft;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriGroup::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  *AEDLeft = -1;
  *ALabR   = -1;
  // else                                            *ALabR = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TriGroup::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
 }
// ---------------------------------------------------------------------------
bool __fastcall TriGroup::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return true;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriGroup::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString xVal = "";
  return xVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriGroup::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriGroup::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled   = AEnable;
  Enabled     = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock());
  ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock());
 }
// ---------------------------------------------------------------------------
void __fastcall TriGroup::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
 }
// ---------------------------------------------------------------------------
void __fastcall TriGroup::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ACaption.Length())
   ED->Caption = ACaption + ":";
  else
   ED->Caption = ""; // FNode->AV["name"]+":";
  // ED->Update();
  // if (ACaption.Length()) Caption = ACaption+":";
  // else               Caption = "";//FNode->AV["name"]+":";
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TriGroup::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  /*
   SetEnList->Add(ANode);
   DataChange(this);
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TriGroup::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  /*
   FDataChange(Sender);
   if (!TmpCtrl) return;
   // запись значений при изменении значения контрола кроме choiceDB ----------------------------------
   Variant xVal; xVal.Empty();
   */
 }
// ---------------------------------------------------------------------------
void __fastcall TriGroup::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  /*
   if (FGetDataChange)
   {
   UnicodeString Src = "";
   FGetDataChange(FNode,Src);
   }
   UnicodeString xRef,xUID,FUID;
   FUID = _UID_.UpperCase();
   // отработка enabled для зависимых контролов -------------------------------------------------------
   if (SetEnList->Count > 0)
   {
   TTagNode *xNode;
   for (int i = 0; i < SetEnList->Count; i++)
   {
   xNode = ((TTagNode*)SetEnList->Items[i]);
   FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
   }
   }
   */
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriGroup::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return NULL;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriGroup::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriGroup::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return "";
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriGroup::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " group has no value");
 }
// ---------------------------------------------------------------------------
bool __fastcall TriGroup::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return true;
 }
// ---------------------------------------------------------------------------
