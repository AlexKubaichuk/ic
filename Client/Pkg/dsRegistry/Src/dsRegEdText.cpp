﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop
#include "dsRegEdText.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// #define ED         ((TcxDBMaskEdit*)ED)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED_CB       ((TcxComboBox*)ED)
// ---------------------------------------------------------------------------
__fastcall TriText::TriText(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner, bool IsApp) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner, IsApp)
 {
  TRect xSize = Rect(0, 0, 120, 23); // default позиция и размер для "ED"
  FLab                       = new TLabel(this);
  FLab->Parent               = this;
  FLab->Transparent          = true;
  FLab->Top                  = 2;
  FLab->Left                 = 4;
  FLab->Caption              = FNode->AV["name"] + ":";
  FOnGetFormat               = ACtrlOwner->OnGetFormat;
  ED                         = new TcxMaskEdit(this);
  ED->Parent                 = this;
  ED->Properties->OnChange   = FDataChange;
  ED->Style->StyleController = FCtrlOwner->StyleController;
  if (Required)
   ED->Style->Color = FReqColor;
  int xW = 5 * FNode->AV["length"].ToInt();
  xSize                     = Rect(0, 0, xW, 23);
  ED->Properties->MaxLength = FNode->AV["length"].ToInt();
  ED->CMinW                 = 60;
  if (!FNode->CmpAV("cast", "0"))
   ED->OnExit = FFieldCast;
  if (FNode->AV["format"].Length())
   {
    ED->Properties->MaskKind = emkRegExprEx;
    if (FOnGetFormat)
     ED->Properties->EditMask = FOnGetFormat(FNode->AV["format"]);
    else
     ED->Properties->EditMask = FNode->AV["format"];
   }
  if (IsAppend)
   {
    if (FCtrlOwner->IsTemplate(_UID_))
     {
      FCtrlOwner->SetCtrlData(FNode, FCtrlOwner->TemplateData->Value[_UID_], FCtrlOwner->TemplateData->Value[_UID_]);
      ED->Text = FCtrlOwner->GetCtrlData(FNode, _ES_);
     }
   }
  else
   ED->Text = FCtrlOwner->GetCtrlData(FNode, _ES_);
  ED->SelLength = 0;
  ED->SelStart  = 0;
  ED->Enabled   = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock());
  ED->Constraints->MinWidth = 60;
  ED->Parent = this;
  ED->Width  = xSize.Right + 10;
  if (ED->Width < 60)
   ED->Width = 60;
  ED->Top    = xSize.Top;
  ED->Height = xSize.Bottom;
  if (FLab->Width > (AParent->Width - 2))
   {
    FLab->Font->Size = FLab->Font->Size - 1;
    FLab->Update();
    if (FLab->Width > (AParent->Width - 2))
     FLab->Width = AParent->Width - 2;
   }
  ED->Left = FLab->Left + FLab->Width + 5;
  if ((ED->Left + ED->Width) > (AParent->Width - 2))
   {
    TwoLine   = true;
    FLab->Top = 0;
    ED->Left  = FLab->Left + 5;
    ED->Top   = FLab->Height;
    CMaxH     = 36;
    CMinH     = 36;
    Height    = CMaxH;
    if ((ED->Left + ED->Width) > (AParent->Width - 2))
     ED->Width = AParent->Width - 4 - ED->Left;
    if (FNode->CmpName("text"))
     ED->Width = AParent->Width - 12 - ED->Left;
   }
  FLab->FocusControl = ED;
  if (Parent->ClassNameIs("TriText"))
   {
    Anchors.Clear();
    Anchors << akLeft << akRight << akTop;
   }
  ED->OnKeyDown = FEDKeyDown;
 }
// ---------------------------------------------------------------------------
TcxMaskEdit * __fastcall TriText::FGetCastED()
 {
  return (TcxMaskEdit *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::FSetCastED(TcxMaskEdit * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::SetOnDependValues()
 {
  if (FNode->AV["depend"].Length() == 9 && EDCtrlExists(_GUI(FNode->AV["depend"])))
   GetEDCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(this);
  else if (FNode->AV["ref"].Length() == 4 && EDCtrlExists(FNode->AV["ref"]))
   GetEDCtrl(FNode->AV["ref"])->SetOnLab(this);
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::SetFixedVar(UnicodeString AVals)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  /*
   if (isFV)
   {
   UnicodeString tmp = tED_CB->Text;
   tED_CB->Properties->Items->Text = AVals;
   int ind = tED_CB->Properties->Items->IndexOf(tmp);
   if (ind != -1)
   {
   tED_CB->ItemIndex = ind;
   tED_CB->Text = tmp;
   }
   else
   tED_CB->Text = tmp;
   }
   */
 }
// ---------------------------------------------------------------------------
__fastcall TriText::~TriText()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Properties->OnChange = NULL;
  delete ED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign = true;
  if (!TwoLine)
   ED->Left = ALeft;
  Width = ED->Left + ED->Width + 4;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!TwoLine)
   * AEDLeft = ED->Left;
  else
   * AEDLeft = -1;
  if (!TwoLine)
   * ALabR = FLab->Left + FLab->Width;
  else
   * ALabR = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!IsAlign)
   {
    if (TwoLine)
     {
      ED->Left  = 2;
      ED->Width = Width - 4;
     }
    else
     ED->Left = Width - ED->Width - 2;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::FFieldCast(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString FVal;
  if (ED->Text.Trim().Length())
   {
    FVal     = FieldCast(FNode, ED->Text.Trim());
    ED->Text = FVal;
    FCtrlOwner->SetCtrlData(FNode, FVal, FVal);
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriText::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(ED);
  return !FCtrlOwner->IsTemplate(_UID_);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriText::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString xVal = "";
  if (ED->Enabled && ED->Text.Length())
   xVal = ED->Text.Trim();
  return xVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled = AEnable;
  bool tmpClearVal = isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock());
  FLab->Enabled = isEnabled && tmpClearVal;
  ED->Enabled   = isEnabled && tmpClearVal;
  if (!isEnabled && tmpClearVal)
   ED->Text = "";
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnable(AEnable);
  ED->Text = AVal;
  FCtrlOwner->SetCtrlData(FNode, AVal, AVal); // if (SrcDSEditable()) irquSetAsStr(SrcDS(),FFlName,AVal);
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::FEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if ((Key == VK_DELETE) && Shift.Contains(ssCtrl) && ED->Enabled)
   {
    ED->Text = "";
    FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), ""); // irquSetAsNull(SrcDS(),FFlName);
   }
  if (Key && FCtrlOwner->OnKeyDown)
   FCtrlOwner->OnKeyDown(Sender, Key, Shift);
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ACaption.Length())
   FLab->Caption = ACaption + ":";
  else
   FLab->Caption = ""; // FNode->AV["name"]+":";
  FLab->Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnList->Add(ANode);
  // ED->Properties->OnChange = DataChange;
  DataChange(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriText::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  // запись значений при изменении значения контрола кроме choiceDB ----------------------------------
  if (ED->Enabled)
   {
    if (ED->Text.Trim().Length())
     FCtrlOwner->SetCtrlData(FNode, ED->Text, ED->Text);
    else if (isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock()))
     FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), "");
   }
  else if (isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock()))
   FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), "");
  // отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  if (FGetDataChange)
   {
    UnicodeString Src = FCtrlOwner->GetCtrlData(FNode, _ES_);
    FGetDataChange(FNode, Src);
   }
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriText::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TControl *)ED;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriText::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (ED->Enabled && isEdit) ? ((TControl *)ED) : NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriText::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return ED->Text;
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriText::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (GetValue("").Trim().Length())
   return GetValue("");
  else
   return Variant("NULL");
 }
// ---------------------------------------------------------------------------
bool __fastcall TriText::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  bool RC = true;
  if (Required)
   {
    try
     {
      if (ED->Enabled)
       {
        if (!ED->Text.Length())
         RC = false;
       }
     }
    __finally
     {
      if (!RC)
       {
        if (APC)
         APC->ActivePageIndex = this->Tag;
        if (!ASilent)
         {
          UnicodeString ErrMessage;
          if (FLab)
           ErrMessage = FLab->Caption;
          else
           ErrMessage = FNode->AV["name"];
          ErrMessage = FMT1(icsRegItemErrorReqField, ErrMessage);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
        if (ED->Visible && Visible)
         ED->SetFocus();
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
