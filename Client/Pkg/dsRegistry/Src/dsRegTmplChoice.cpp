﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop
#include "dsRegTmplChoice.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
// ---------------------------------------------------------------------------
__fastcall TriChoiceTmpl::TriChoiceTmpl(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner)
 {
  FClearBtn           = new TcxButton(this);
  FClearBtn->Parent   = this;
  FClearBtn->OnClick  = FClearClick;
  FClearBtn->ShowHint = true;
  FClearBtn->Hint     = "Очистить поле \"" + FNode->AV["name"] + "\"";
  FClearBtn->Width    = 20;
  FClearBtn->Height   = 20;
  FClearBtn->OptionsImage->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\regclear.bmp"));
  FClearBtn->OptionsImage->NumGlyphs          = 4;
  FClearBtn->PaintStyle                       = bpsGlyph;
  FClearBtn->SpeedButtonOptions->CanBeFocused = false;
  FClearBtn->SpeedButtonOptions->Flat         = true;
  FClearBtn->TabStop                          = false;
  FClearBtn->Anchors.Clear();
  FClearBtn->Anchors << akRight;
  FLab                              = new TLabel(this);
  FLab->Parent                      = this;
  FLab->Transparent                 = true;
  FLab->Top                         = 2;
  FLab->Left                        = 4;
  FLab->Caption                     = FNode->AV["name"] + ":";
  ED                                = new TcxComboBox(this);
  ED->Properties->DropDownListStyle = lsEditFixedList;
  ED->Style->StyleController        = FCtrlOwner->StyleController;
  if (Required)
   ED->Style->Color = FReqColor;
  ED->Parent = this;
  ED->Properties->Items->Clear();
  TTagNode * itxNode = FNode->GetFirstChild();
  int MaxW = 16;
  while (itxNode)
   {
    if (itxNode->CmpName("choicevalue"))
     {
      ED->Properties->Items->AddObject(itxNode->AV["name"].c_str(), (TObject *)itxNode->AV["value"].ToInt());
      if (MaxW < ED->Canvas->TextWidth(itxNode->AV["name"]))
       MaxW = ED->Canvas->TextWidth(itxNode->AV["name"]);
     }
    itxNode = itxNode->GetNext();
   }
  if (!FCtrlOwner->TemplateField && FCtrlOwner->IsTemplate(_UID_))
   {
    __int64 FIdx = FCtrlOwner->TemplateData->Value[_UID_];
    int xind = ED->Properties->Items->IndexOfObject((TObject *)FIdx);
    if (xind != -1)
     {
      ED->ItemIndex = -1;
      ED->ItemIndex = xind;
      ED->Text      = "empty";
     }
   }
  ED->Enabled = isEnabled && isEdit && (!isTemplate || FCtrlOwner->TemplateNoLock());
  ED->Properties->OnChange  = FDataChange;
  ED->Constraints->MinWidth = 60;
  ED->Parent                = this;
  ED->Width                 = MaxW + GetSystemMetrics(SM_CXVSCROLL) + GetSystemMetrics(SM_CXBORDER) * 2 + 10;
  if (ED->Width < 60)
   ED->Width = 60;
  ED->Top    = 0;
  ED->Height = 21;
  Height     = 25;
  if (FLab->Width > (AParent->Width - 2))
   {
    FLab->Font->Size = FLab->Font->Size - 1;
    FLab->Update();
    if (FLab->Width > (AParent->Width - 2))
     FLab->Width = AParent->Width - 2;
   }
  ED->Left = FLab->Left + FLab->Width + 5;
  if ((ED->Left + ED->Width) > (AParent->Width - 2 - _TMPL_CLEAR_WIDTH_))
   {
    TwoLine   = true;
    FLab->Top = 0;
    ED->Left  = FLab->Left + 5;
    ED->Top   = FLab->Height;
    CMaxH     = 38;
    CMinH     = 38;
    Height    = CMaxH;
    if ((ED->Left + ED->Width) > (AParent->Width - 2 - _TMPL_CLEAR_WIDTH_))
     ED->Width = AParent->Width - 4 - ED->Left - _TMPL_CLEAR_WIDTH_;
   }
  FLab->FocusControl = ED;
  FClearBtn->Top = ED->Top + ED->Height / 2 - 10;
  ED->OnKeyDown  = FEDKeyDown;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::Clear()
 {
  FClearClick(FClearBtn);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::FClearClick(TObject * ASender)
 {
  ED->ItemIndex                          = -1;
  FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
  DataChange(ED);
 }
// ---------------------------------------------------------------------------
TcxComboBox * __fastcall TriChoiceTmpl::FGetCastED()
 {
  return (TcxComboBox *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::FSetCastED(TcxComboBox * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetOnDependValues()
 {
  if (FNode->AV["depend"].Length() == 9 && EDCtrlExists(_GUI(FNode->AV["depend"])))
   GetEDCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(this);
  else if (FNode->AV["ref"].Length() == 4 && EDCtrlExists(FNode->AV["ref"]))
   GetEDCtrl(FNode->AV["ref"])->SetOnLab(this);
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
__fastcall TriChoiceTmpl::~TriChoiceTmpl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Properties->OnChange = NULL;
  delete ED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign = true;
  if (!TwoLine)
   {
    if ((ALeft + ED->Width) < Width - 6)
     ED->Left = ALeft;
   }
  Width = ED->Left + ED->Width;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (TwoLine)
   {
    *AEDLeft = -1;
    *ALabR   = -1;
   }
  else
   {
    *AEDLeft = ED->Left;
    *ALabR   = FLab->Left + FLab->Width;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!IsAlign)
   {
    if (TwoLine)
     {
      ED->Left  = 2;
      ED->Width = Width - 4 - _TMPL_CLEAR_WIDTH_;
     }
    else
     {
      ED->Left = Width - ED->Width - 2 - _TMPL_CLEAR_WIDTH_;
     }
    FClearBtn->Left = ED->Left + ED->Width + 2;
    ED->Properties->OnChange = NULL;
    int FSaveIndex = ED->ItemIndex;
    ED->ItemIndex            = -1;
    ED->ItemIndex            = FSaveIndex;
    ED->Properties->OnChange = DataChange;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriChoiceTmpl::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(ED);
  return !FCtrlOwner->IsTemplate(_UID_);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTmpl::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString xVal = "";
  if (ED->Enabled)
   {
    if (ED->ItemIndex >= 0)
     xVal += ED->Properties->Items->Strings[ED->ItemIndex];
   }
  return xVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled          = AEnable;
  FLab->Enabled      = isEnabled;
  ED->Enabled        = isEnabled;
  FClearBtn->Enabled = ED->Enabled;
  if (!isEnabled)
   {
    FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
    ED->ItemIndex                          = -1;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetEnableEx(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled          = AEnable;
  FLab->Enabled      = isEnabled;
  ED->Enabled        = isEnabled;
  FClearBtn->Enabled = ED->Enabled;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnable(AEnable);
  int xind = ED->Properties->Items->IndexOfObject((TObject *)AVal.ToIntDef(0));
  if (xind != -1)
   {
    ED->ItemIndex = -1;
    ED->ItemIndex = xind;
    ED->Text      = AVal;
    // if (SrcDSEditable())
    // {
    FCtrlOwner->TemplateData->Value[_UID_] = AVal;
    // irquSetAsStr(SrcDS(),FFlName,AVal);
    // }
   }
  else
   ED->ItemIndex = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::FEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if ((Key == VK_DELETE) && Shift.Contains(ssCtrl) && ED->Enabled)
   {
    FClearClick(FClearBtn);
    Key = 0;
   }
  if (Key && FCtrlOwner->OnKeyDown && !ED->DroppedDown)
   FCtrlOwner->OnKeyDown(Sender, Key, Shift);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ACaption.Length())
   FLab->Caption = ACaption + ":";
  else
   FLab->Caption = ""; // FNode->AV["name"]+":";
  FLab->Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetOnLab(TdsRegEDItem * AEDItem)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FLabList[AEDItem->DefNode->AV["uid"]] = AEDItem;
  DataChange(this);
  /*
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   SetLabList->Add(ANode);
   //  ED->Properties->OnChange = DataChange;
   DataChange(this); */
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnList->Add(ANode);
  // ED->Properties->OnChange = DataChange;
  DataChange(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceTmpl::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ED->Enabled)
   {
    if (ED->ItemIndex >= 0)
     FCtrlOwner->TemplateData->Value[_UID_] = (int)ED->Properties->Items->Objects[ED->ItemIndex];
    else if (isEdit && !isTemplate)
     FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
   }
  else if (isEdit && !isTemplate)
   FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
  // присвоение меток --------------------------------------------------------------------------------
  if (FLabList.size()) // Получение текста для Label возможно от choice или choiceDB
   {
    int ItmIndex;
    ItmIndex = ED->ItemIndex;
    if (ItmIndex >= 0)
     {
      for (TRegEDMap::iterator i = FLabList.begin(); i != FLabList.end(); i++)
       {
        // xRef = ((TTagNode*)SetLabList->Items[i])->AV["ref"].UpperCase(); // ref == uid.choice
        if (FNode->CmpAV("uid", i->second->DefNode->AV["ref"])) // ref == uid.choice
           i->second->SetLabel(ED->Properties->Items->Strings[ED->ItemIndex]);
       }
     }
   }
  // отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  if (FGetDataChange)
   {
    UnicodeString Src = FCtrlOwner->GetCtrlData(FNode, _ES_);
    FGetDataChange(FNode, Src);
   }
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriChoiceTmpl::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TControl *)ED;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriChoiceTmpl::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (ED->Enabled && isEdit) ? ((TControl *)ED) : NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceTmpl::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ED->ItemIndex >= 0)
   return IntToStr(((int)ED->Properties->Items->Objects[ED->ItemIndex]));
  return "";
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriChoiceTmpl::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  Variant RC = Variant::Empty();
  try
   {
    if (FCtrlOwner->DataProvider->OnGetCompValue)
     {
      if (ED->ItemIndex >= 0)
       RC = Variant((int)(int)ED->Properties->Items->Objects[ED->ItemIndex]);
      FCtrlOwner->DataProvider->OnGetCompValue(FNode, RC);
     }
    else
     {
      if (ED->ItemIndex >= 0)
       RC = Variant((int)(int)ED->Properties->Items->Objects[ED->ItemIndex]);
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TriChoiceTmpl::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  bool RC = true;
  if (Required)
   {
    try
     {
      if (ED->Enabled)
       if (ED->ItemIndex < 0)
        RC = false;
     }
    __finally
     {
      if (!RC)
       {
        if (APC)
         APC->ActivePageIndex = this->Tag;
        if (!ASilent)
         {
          UnicodeString ErrMessage;
          if (FLab)
           ErrMessage = FLab->Caption;
          else
           ErrMessage = FNode->AV["name"];
          ErrMessage = FMT1(icsRegItemErrorReqField, ErrMessage);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
        if (ED->Visible && Visible)
         ED->SetFocus();
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
