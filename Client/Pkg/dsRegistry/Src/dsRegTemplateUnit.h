//---------------------------------------------------------------------------
#ifndef dsRegTemplateUnitH
#define dsRegTemplateUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"
//---------------------------------------------------------------------------
#include "dsRegDMUnit.h"
#include "dsRegTemplate.h"
#include "dsRegTemplateData.h"
#include "cxClasses.hpp"
/*#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Mask.hpp>
#include <StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxLookAndFeelPainters.hpp"
#include <Menus.hpp>
//---------------------------------------------------------------------------
#include "cxGraphics.hpp"
#include "cxLookAndFeels.hpp"
#include "dxSkinBlue.hpp"
#include "dxSkinsCore.hpp"     */
//---------------------------------------------------------------------------
class PACKAGE TdsRegTemplateForm : public TForm
{
__published:	// IDE-managed Components
 TPanel *CompPanel;
 TPanel *Panel1;
 TcxButton *OkBtn;
 TcxButton *CancelBtn;
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall CancelBtnClick(TObject *Sender);
 void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
private:
  TdsRegTemplate *FTempl;
  TdsRegDM       *FDM;
  TdsRegTemplateData* __fastcall FGetTemplateData();
//        TICSTemplate *Tmpl;
//        TTagNode *FRoot;
//        TTagNode *FUnitNode;
//        TStringList *FTreeFormList;
//        bool  __fastcall FOnTreeBtnClick(TTagNode *ItTag, bool &AIsGroup, UnicodeString &ACodeBeg, UnicodeString &ACodeEnd, UnicodeString &AExtCode, UnicodeString &AText, bool isClicked);
public:		// User declarations
//        UnicodeString  __fastcall GetWhere(UnicodeString ATabAlias = "");
        UnicodeString  __fastcall GetSelectedText(bool CanField = false);
        void  __fastcall Clear();
        bool  __fastcall IsEmpty();

  __fastcall TdsRegTemplateForm(TComponent* Owner, TdsRegDM* ADM, UnicodeString ATemplXML, TdsRegTemplateData *ATmplData);
  __property TdsRegTemplateData *TemplateData = {read=FGetTemplateData};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsRegTemplateForm *dsRegTemplateForm;
//---------------------------------------------------------------------------
#endif
