// ---------------------------------------------------------------------------
#include <basepch.h>
#pragma hdrstop
#include "dsRegClient.h"
// #include "DxLocale.h"
#include "dsRegDMUnit.h"
#include "dsClassifClientUnit.h"
#include "dsUnitListClientUnit.h"
#include "dsUnitListExClientUnit.h"
#include "dsSelUnitListClientUnit.h"
// #include "ICSRegList.h"
// #include "ICSRegSelList.h"
// #include "Reg_DMF.h"
// #include "ICSRegProgress.h"
#pragma package(smart_init)
// ---------------------------------------------------------------------------
// ValidCtrCheck is used to assure that the components created do not have
// any pure virtual functions.
//
static inline void ValidCtrCheck(TdsRegClient *)
 {
  new TdsRegClient(NULL);
 }
// ---------------------------------------------------------------------------
__fastcall TdsRegClient::TdsRegClient(TComponent * Owner) : TComponent(Owner)
 {
  // FUseClassDisControls = false;
  // FUseReportSetting = false;
  FDBUser     = "";
  FDBPassword = "";
  FDBCharset  = "";
  RegistryKey = "Software\\ICS Ltd.\\ClassifSize";
  // FUnitListCreated = NULL;
  FBuyCaption = "";
  // FLargeIcons = false;
  // FFetchAll = true;
  FUnitListDeleted = NULL;
  // FOnExtFilterClear = NULL;
  // FOnExtFilterGetText = NULL;
  // FRefreshList = false;
  FUpperFunc       = "Upper";
  FDTFormat        = "dd.mm.yyyy";
  FDM              = new TdsRegDM(this);
  FActive          = true;
  FClassEnabled    = true;
  FUnitListEnabled = true;
 }
// ---------------------------------------------------------------------------
__fastcall TdsRegClient::~TdsRegClient()
 {
  Active = false;
  if (FDM->UnitList)
   delete FDM->UnitList;
  FDM->UnitList = NULL;
  delete FDM;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegClient::FGetFullEdit()
 {
  if (FDM)
   return FDM->FullEdit;
  else
   return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetFullEdit(bool AFullEdit)
 {
  if (FDM)
   FDM->FullEdit = AFullEdit;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetActive(bool AActive)
 {
  FActive = AActive;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetClassEnabled(bool AClassEnabled)
 {
  FClassEnabled = AClassEnabled;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetUnitListEnabled(bool AUnitListEnabled)
 {
  FUnitListEnabled = AUnitListEnabled;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegClient::FGetOpenCardEnabled()
 {
  bool RC = false;
  try
   {
    if (FDM)
     RC = FDM->OpenCardEnabled;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOpenCardEnabled(bool AOpenCardEnabled)
 {
  if (FDM)
   FDM->OpenCardEnabled = AOpenCardEnabled;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetUListTop(int ATop)
 {
  FUListTop = ATop;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetUListLeft(int ALeft)
 {
  FUListLeft = ALeft;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetDBName(UnicodeString ADBName)
 {
  FDBName = ADBName;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetXMLName(UnicodeString AXMLName)
 {
  FXMLName = AXMLName;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::ShowClasses(UnicodeString AClassID, TdsRegClassType AShowType, UnicodeString AGroups)
 {
  if (!FActive)
   return;
  if (!FClassEnabled)
   return;
  TdsClassifClientForm * ClassDlg = NULL;
  try
   {
    ClassDlg = new TdsClassifClientForm(Application->Owner, FDM, AShowType, AGroups, -1);
    ClassDlg->SetActiveClass(AClassID);
    ClassDlg->ShowModal();
   }
  __finally
   {
    if (ClassDlg)
     delete ClassDlg;
    ClassDlg = NULL;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::ShowClasses(TdsRegClassType AShowType, UnicodeString AGroups, UnicodeString AClassId)
 {
  if (!FActive)
   return;
  if (!FClassEnabled)
   return;
  TdsClassifClientForm * ClassDlg = NULL;
  try
   {
    ClassDlg = new TdsClassifClientForm(Application->Owner, FDM, AShowType, AGroups, AClassId);
    ClassDlg->ShowModal();
   }
  __finally
   {
    if (ClassDlg)
     delete ClassDlg;
    ClassDlg = NULL;
   }
 }
// ---------------------------------------------------------------------------
TActionList * __fastcall TdsRegClient::FGetUnitListActionList()
 {
  TActionList * RC = NULL;
  if (FActive && FDM)
   {
    if (FDM->UnitList)
     {
      RC = ((TdsUnitListExClientForm *)FDM->UnitList)->ActionList;
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TForm* __fastcall TdsRegClient::CreateUnitList()
 {
  if (FActive && FDM && FUnitListEnabled)
   {
    if (!FDM->UnitList)
     {
      FDM->UnitList = new TdsUnitListExClientForm(Application->Owner, FDM);
      ((TdsUnitListExClientForm *)FDM->UnitList)->ImageList = FULImageList;
      FDM->UnitList->Top  = FUListTop;
      FDM->UnitList->Left = FUListLeft;
     }
   }
  return FDM->UnitList;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::ShowUnitList(bool isModal, bool AExtended)
 {
  if (!FActive)
   return;
  if (!FDM)
   return;
  if (!FUnitListEnabled)
   return;
  if (!FDM->UnitList)
   {
    // if (FUnitListCreated) FUnitListCreated((TdsUnitListClientForm*)FDM->UnitList);
    if (AExtended)
     {
      FDM->UnitList = new TdsUnitListExClientForm(Application->Owner, FDM);
      ((TdsUnitListExClientForm *)FDM->UnitList)->ImageList = FULImageList;
     }
    else
     FDM->UnitList = new TdsUnitListClientForm(Application->Owner, FDM);
    // TICSRegListForm(Application->Owner, UnitListTop, UnitListLeft,this);
    FDM->UnitList->Top  = FUListTop;
    FDM->UnitList->Left = FUListLeft;
   }
  if (isModal)
   FDM->UnitList->ShowModal();
  else
   FDM->UnitList->Show();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::ShowSelUnitList(UnicodeString ATmpl, TStringList * ACodeList)
 {
  if (!FActive)
   return;
  if (!FDM)
   return;
  TdsSelUnitListClientForm * Dlg = new TdsSelUnitListClientForm(Application->Owner, FDM);
  try
   {
    ACodeList->Text = "";
    Dlg->Template   = ATmpl;
    Dlg->Top        = FUListTop;
    Dlg->Left       = FUListLeft;
    Dlg->ShowModal();
    if (Dlg->ModalResult == mrOk)
     {
      ACodeList->Text = Dlg->Codes->Text;
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::RefreshUnitList()
 {
  /*
   if (!FActive) return;
   if (!FDM->UnitList) return;
   if (!((TForm*)FDM->UnitList)->Visible) return;
   ((TICSRegListForm*)FDM->UnitList)->ListReopen();
   */ }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetInqComp(TControl * AInqComp)
 {
  FInqComp = AInqComp;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegClient::IsListShowing()
 {
  if (!FActive)
   return false;
  if (FDM->UnitList)
   {
    return FDM->UnitList->Showing;
   }
  return false;
 }
// ---------------------------------------------------------------------------
/*
 bool  __fastcall TdsRegClient::GetSelected(__int64 *ACode, UnicodeString &AUnitStr)
 {
 if (!IsListShowing()) return false;
 if (FDM->UnitList)
 {
 __int64 XXCode = ((TICSRegListForm*)FDM->UnitList)->GetSelCode();
 if (XXCode == -1) return false;
 *ACode   = XXCode;
 AUnitStr = ((TICSRegListForm*)FDM->UnitList)->GetSelStr();
 return true;
 }
 return false;
 }
 */
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::GetSelectedUnitData(TkabCustomDataSetRow *& AData)
 {
  AData = NULL;
  if (FDM->UnitList)
   {
    ((TdsUnitListClientForm *)FDM->UnitList)->GetSelectedUnitData(AData);
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegClient::GetUnitData(__int64 ACode, TkabCustomDataSetRow *& AData)
 {
  bool RC = false;
  AData = NULL;
  try
   {
    if (FDM)
     RC = FDM->GetUnitData(ACode, AData);
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::GetSelectedUnits(TStrings * ADest)
 {
  /*
   if (!IsListShowing()) return;
   if (FDM->UnitList)
   ((TICSRegListForm*)FDM->UnitList)->GetSelectedUnits(ADest);
   */ }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::GetTreeChoice(TList * AList)
 {
  tmpChTree = new TList;
  FDM->RegDef->Iterate(GetTreeCh);
  AList->Assign(tmpChTree, laCopy, NULL);
  if (tmpChTree)
   delete tmpChTree;
  tmpChTree = NULL;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegClient::GetTreeCh(TTagNode * itTag)
 {
  if (itTag->CmpName("choiceTree"))
   tmpChTree->Add(itTag);
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetInit(int APersent, UnicodeString AMess)
 {
  /*
   if (APersent == 0) ((TICSRegProgressForm*)PrF)->SetCaption(AMess);
   ((TICSRegProgressForm*)PrF)->SetPercent(APersent);
   */ }
// ---------------------------------------------------------------------------
/*
 TdxBar*  __fastcall TdsRegClient::GetListExtTB(void)
 {
 if (FDM->UnitList) return ((TICSRegListForm*)FDM->UnitList)->GetExtTB();
 else              return NULL;
 }
 */
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetUpperFunc(UnicodeString AParam)
 {
  FUpperFunc = AParam;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetDTFormat(UnicodeString AParam)
 {
  FDTFormat = AParam;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegClient::FGetClassEditable()
 {
  bool RC = false;
  try
   {
    if (FDM)
     RC = FDM->ClassEditable;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetClassEditable(bool AVal)
 {
  if (FDM)
   FDM->ClassEditable = AVal;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegClient::FGetUnitListEditable()
 {
  bool RC = false;
  try
   {
    if (FDM)
     RC = FDM->UnitListEditable;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetUnitListEditable(bool AVal)
 {
  if (FDM)
   FDM->UnitListEditable = AVal;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegClient::FGetClassShowExpanded()
 {
  UnicodeString RC = "";
  try
   {
    if (FDM)
     RC = FDM->ClassShowExpanded;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetClassShowExpanded(UnicodeString AVal)
 {
  if (FDM)
   FDM->ClassShowExpanded = AVal;
 }
// ---------------------------------------------------------------------------
TExtEditData __fastcall TdsRegClient::FGetExtInsertData()
 {
  if (FDM)
   return FDM->OnExtInsertData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetExtInsertData(TExtEditData AValue)
 {
  if (FDM)
   FDM->OnExtInsertData = AValue;
 }
// ---------------------------------------------------------------------------
TExtEditData __fastcall TdsRegClient::FGetExtEditData()
 {
  if (FDM)
   return FDM->OnExtEditData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetExtEditData(TExtEditData AValue)
 {
  if (FDM)
   FDM->OnExtEditData = AValue;
 }
// ---------------------------------------------------------------------------
TdsExportData __fastcall TdsRegClient::FGetOnExportData()
 {
  if (FDM)
   return FDM->OnExportData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnExportData(TdsExportData AValue)
 {
  if (FDM)
   FDM->OnExportData = AValue;
 }
// ---------------------------------------------------------------------------
TdsExportData __fastcall TdsRegClient::FGetOnInsPriv()
 {
  if (FDM)
   return FDM->OnInsPriv;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnInsPriv(TdsExportData AValue)
 {
  if (FDM)
   FDM->OnInsPriv = AValue;
 }
// ---------------------------------------------------------------------------
TdsInsEditData __fastcall TdsRegClient::FGetOnInsPTCData()
 {
  if (FDM)
   return FDM->OnInsPTCData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnInsPTCData(TdsInsEditData AValue)
 {
  if (FDM)
   FDM->OnInsPTCData = AValue;
 }
// ---------------------------------------------------------------------------
TdsInsEditData __fastcall TdsRegClient::FGetOnEditPTCData()
 {
  if (FDM)
   return FDM->OnEditPTCData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnEditPTCData(TdsInsEditData AValue)
 {
  if (FDM)
   FDM->OnEditPTCData = AValue;
 }
// ---------------------------------------------------------------------------
TdsImportData __fastcall TdsRegClient::FGetOnImportData()
 {
  if (FDM)
   return FDM->OnImportData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnImportData(TdsImportData AValue)
 {
  if (FDM)
   FDM->OnImportData = AValue;
 }
// ---------------------------------------------------------------------------
TClassEditEvent __fastcall TdsRegClient::FGetClassAfterInsert()
 {
  if (FDM)
   return FDM->OnClassAfterInsert;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetClassAfterInsert(TClassEditEvent AValue)
 {
  if (FDM)
   FDM->OnClassAfterInsert = AValue;
 }
// ---------------------------------------------------------------------------
TClassEditEvent __fastcall TdsRegClient::FGetClassAfterEdit()
 {
  if (FDM)
   return FDM->OnClassAfterEdit;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetClassAfterEdit(TClassEditEvent AValue)
 {
  if (FDM)
   FDM->OnClassAfterEdit = AValue;
 }
// ---------------------------------------------------------------------------
TClassEditEvent __fastcall TdsRegClient::FGetClassAfterDelete()
 {
  if (FDM)
   return FDM->OnClassAfterDelete;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetClassAfterDelete(TClassEditEvent AValue)
 {
  if (FDM)
   FDM->OnClassAfterDelete = AValue;
 }
// ---------------------------------------------------------------------------
TUnitEvent __fastcall TdsRegClient::FGetOnOpenCard()
 {
  if (FDM)
   return FDM->OnOpenCard;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnOpenCard(TUnitEvent AValue)
 {
  if (FDM)
   FDM->OnOpenCard = AValue;
 }
// ---------------------------------------------------------------------------
TUnitEvent __fastcall TdsRegClient::FGetOnPrintF63()
 {
  if (FDM)
   return FDM->OnPrintF63;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnPrintF63(TUnitEvent AValue)
 {
  if (FDM)
   FDM->OnPrintF63 = AValue;
 }
// ---------------------------------------------------------------------------
TUnitEvent __fastcall TdsRegClient::FGetOnShowPrePlan()
 {
  if (FDM)
   return FDM->OnShowPrePlan;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnShowPrePlan(TUnitEvent AValue)
 {
  if (FDM)
   FDM->OnShowPrePlan = AValue;
 }
// ---------------------------------------------------------------------------
TGetReportListEvent __fastcall TdsRegClient::FGetOnGetQuickFilterList()
 {
  if (FDM)
   return FDM->OnGetQuickFilterList;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnGetQuickFilterList(TGetReportListEvent AValue)
 {
  if (FDM)
   FDM->OnGetQuickFilterList = AValue;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegClient::FGetUseQuickFilterSetting()
 {
  if (FDM)
   return FDM->UseQuickFilterSetting;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetUseQuickFilterSetting(bool AValue)
 {
  if (FDM)
   FDM->UseQuickFilterSetting = AValue;
 }
// ---------------------------------------------------------------------------
TGetReportListEvent __fastcall TdsRegClient::FGetOnGetReportList()
 {
  if (FDM)
   return FDM->OnGetReportList;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnGetReportList(TGetReportListEvent AValue)
 {
  if (FDM)
   FDM->OnGetReportList = AValue;
 }
// ---------------------------------------------------------------------------
TdsRegReportClickEvent __fastcall TdsRegClient::FGetPrintReportClick()
 {
  if (FDM)
   return FDM->OnPrintReportClick;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetPrintReportClick(TdsRegReportClickEvent AValue)
 {
  if (FDM)
   FDM->OnPrintReportClick = AValue;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegClient::FGetUseReportSetting()
 {
  if (FDM)
   return FDM->UseReportSetting;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetUseReportSetting(bool AValue)
 {
  if (FDM)
   FDM->UseReportSetting = AValue;
 }
// ---------------------------------------------------------------------------
// TdsICClassClient* __fastcall TdsRegClient::FGetConnection()
// {
// if (FDM)
// return FDM->Connection;
// else
// return NULL;
// }
// ---------------------------------------------------------------------------
// void __fastcall TdsRegClient::FSetConnection(TdsICClassClient* AValue)
// {
// if (FDM)
// FDM->Connection = AValue;
// }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsRegClient::FGetRegDef()
 {
  if (FDM)
   return FDM->RegDef;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetRegDef(TTagNode * AValue)
 {
  if (FDM)
   FDM->RegDef = AValue;
 }
// ---------------------------------------------------------------------------
TExtBtnClick __fastcall TdsRegClient::FGetExtBtnClick()
 {
  if (FDM)
   return FDM->OnExtBtnClick;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetExtBtnClick(TExtBtnClick AValue)
 {
  if (FDM)
   FDM->OnExtBtnClick = AValue;
 }
// ---------------------------------------------------------------------------
TcxEditStyleController * __fastcall TdsRegClient::FGetStyleController()
 {
  if (FDM)
   return FDM->StyleController;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetStyleController(TcxEditStyleController * AValue)
 {
  if (FDM)
   FDM->StyleController = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetClassXMLEvent __fastcall TdsRegClient::FGetOnGetClassXML()
 {
  if (FDM)
   return FDM->OnGetClassXML;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnGetClassXML(TdsGetClassXMLEvent AValue)
 {
  if (FDM)
   FDM->OnGetClassXML = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetCountEvent __fastcall TdsRegClient::FGetOnGetCount()
 {
  if (FDM)
   return FDM->OnGetCount;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnGetCount(TdsGetCountEvent AValue)
 {
  if (FDM)
   FDM->OnGetCount = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetIdListEvent __fastcall TdsRegClient::FGetOnGetIdList()
 {
  if (FDM)
   return FDM->OnGetIdList;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnGetIdList(TdsGetIdListEvent AValue)
 {
  if (FDM)
   FDM->OnGetIdList = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetValByIdEvent __fastcall TdsRegClient::FGetOnGetValById()
 {
  if (FDM)
   return FDM->OnGetValById;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnGetValById10(TdsGetValById10Event AValue)
 {
  if (FDM)
   FDM->OnGetValById10 = AValue;
 }
// ---------------------------------------------------------------------------
TdsGetValById10Event __fastcall TdsRegClient::FGetOnGetValById10()
 {
  if (FDM)
   return FDM->OnGetValById10;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnGetValById(TdsGetValByIdEvent AValue)
 {
  if (FDM)
   FDM->OnGetValById = AValue;
 }
// ---------------------------------------------------------------------------
TdsFindEvent __fastcall TdsRegClient::FGetOnFind()
 {
  if (FDM)
   return FDM->OnFind;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnFind(TdsFindEvent AValue)
 {
  if (FDM)
   FDM->OnFind = AValue;
 }
// ---------------------------------------------------------------------------
TdsInsertDataEvent __fastcall TdsRegClient::FGetOnInsertData()
 {
  if (FDM)
   return FDM->OnInsertData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnInsertData(TdsInsertDataEvent AValue)
 {
  if (FDM)
   FDM->OnInsertData = AValue;
 }
// ---------------------------------------------------------------------------
TdsEditDataEvent __fastcall TdsRegClient::FGetOnEditData()
 {
  if (FDM)
   return FDM->OnEditData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnEditData(TdsEditDataEvent AValue)
 {
  if (FDM)
   FDM->OnEditData = AValue;
 }
// ---------------------------------------------------------------------------
TdsDeleteDataEvent __fastcall TdsRegClient::FGetOnDeleteData()
 {
  if (FDM)
   return FDM->OnDeleteData;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnDeleteData(TdsDeleteDataEvent AValue)
 {
  if (FDM)
   FDM->OnDeleteData = AValue;
 }
// ---------------------------------------------------------------------------
TExtCountEvent __fastcall TdsRegClient::FGetExtCount()
 {
  if (FDM)
   return FDM->OnExtCount;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetExtCount(TExtCountEvent AValue)
 {
  if (FDM)
   FDM->OnExtCount = AValue;
 }
// ---------------------------------------------------------------------------
TdsRegFetch __fastcall TdsRegClient::FGetFetchAll()
 {
  if (FDM)
   return FDM->FetchAll;
  else
   return TdsRegFetch::None;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetFetchAll(TdsRegFetch AValue)
 {
  if (FDM)
   FDM->FetchAll = AValue;
 }
// ---------------------------------------------------------------------------
TdsRegFetch __fastcall TdsRegClient::FGetClassFetchAll()
 {
  if (FDM)
   return FDM->ClassFetchAll;
  else
   return TdsRegFetch::None;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetClassFetchAll(TdsRegFetch AValue)
 {
  if (FDM)
   FDM->ClassFetchAll = AValue;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsRegClient::FGetFetchRecCount()
 {
  if (FDM)
   return FDM->FetchRecCount;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetFetchRecCount(int AValue)
 {
  if (FDM)
   FDM->FetchRecCount = AValue;
 }
// ---------------------------------------------------------------------------
int __fastcall TdsRegClient::FGetClassFetchRecCount()
 {
  if (FDM)
   return FDM->ClassFetchRecCount;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetClassFetchRecCount(int AValue)
 {
  if (FDM)
   FDM->ClassFetchRecCount = AValue;
 }
// ---------------------------------------------------------------------------
TdsRegExtFilterSetEvent __fastcall TdsRegClient::FGetOnExtFilterSet()
 {
  if (FDM)
   return FDM->OnExtFilterSet;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnExtFilterSet(TdsRegExtFilterSetEvent AValue)
 {
  if (FDM)
   FDM->OnExtFilterSet = AValue;
 }
// ---------------------------------------------------------------------------
TAxeXMLContainer * __fastcall TdsRegClient::FGetXMLList()
 {
  if (FDM)
   return FDM->XMLList;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetXMLList(TAxeXMLContainer * AVal)
 {
  if (FDM)
   FDM->XMLList = AVal;
 }
// ---------------------------------------------------------------------------
TExtValidateData __fastcall TdsRegClient::FGetOnGetDefValues()
 {
  if (FDM)
   return FDM->OnGetDefValues;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnGetDefValues(TExtValidateData AVal)
 {
  if (FDM)
   FDM->OnGetDefValues = AVal;
 }
// ---------------------------------------------------------------------------
TExtValidateData __fastcall TdsRegClient::FGetOnExtValidate()
 {
  if (FDM)
   return FDM->OnExtValidate;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnExtValidate(TExtValidateData AVal)
 {
  if (FDM)
   FDM->OnExtValidate = AVal;
 }
// ---------------------------------------------------------------------------
TdsRegListType __fastcall TdsRegClient::FGetUnitListOpenType()
 {
  if (FDM)
   return FDM->UnitListOpenType;
  else
   return TdsRegListType::ltFull;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetUnitListOpenType(TdsRegListType AVal)
 {
  if (FDM)
   FDM->UnitListOpenType = AVal;
 }
// ---------------------------------------------------------------------------
TdsRegListType __fastcall TdsRegClient::FGetClassListOpenType()
 {
  if (FDM)
   return FDM->ClassListOpenType;
  else
   return TdsRegListType::ltFull;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetClassListOpenType(TdsRegListType AVal)
 {
  if (FDM)
   FDM->ClassListOpenType = AVal;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegClient::FGetXMLPath()
 {
  if (FDM)
   return FDM->XMLPath;
  else
   return "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetXMLPath(UnicodeString AVal)
 {
  if (FDM)
   FDM->XMLPath = AVal;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegClient::FGetDataPath()
 {
  if (FDM)
   return FDM->DataPath;
  else
   return "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetDataPath(UnicodeString AVal)
 {
  if (FDM)
   FDM->DataPath = AVal;
 }
// ---------------------------------------------------------------------------
TExtBtnClick __fastcall TdsRegClient::FGetOnCtrlDataChange()
 {
  if (FDM)
   return FDM->OnCtrlDataChange;
  else
   return NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetOnCtrlDataChange(TExtBtnClick AVal)
 {
  if (FDM)
   FDM->OnCtrlDataChange = AVal;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegClient::FGetSingleEditCol()
 {
  bool RC = true;
  try
   {
    if (FDM)
     RC = FDM->SingleEditCol;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetSingleEditCol(bool AVal)
 {
  if (FDM)
   FDM->SingleEditCol = AVal;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegClient::FGetClsSingleEditCol()
 {
  bool RC = true;
  try
   {
    if (FDM)
     RC = FDM->ClsSingleEditCol;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetClsSingleEditCol(bool AVal)
 {
  if (FDM)
   FDM->ClsSingleEditCol = AVal;
 }
// ---------------------------------------------------------------------------
TColor __fastcall TdsRegClient::FGetReqColor()
 {
  TColor RC = clInfoBk;
  try
   {
    if (FDM)
     RC = FDM->RequiredColor;
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegClient::FSetReqColor(TColor AVal)
 {
  if (FDM)
   FDM->RequiredColor = AVal;
 }
// ---------------------------------------------------------------------------
