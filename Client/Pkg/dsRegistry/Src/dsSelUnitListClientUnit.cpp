// ---------------------------------------------------------------------------
#include <vcl.h>
#include <SysUtils.hpp>
#pragma hdrstop
#include "dsSelUnitListClientUnit.h"
#include "dsClassifEditClientUnit.h"
#include "dsRegEDkabDSDataProvider.h"
#include "dsRegTemplateUnit.h"
#include "msgdef.h"
// #include "pkgsetting.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#pragma link "cxClasses"
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxCustomData"
#pragma link "cxData"
#pragma link "cxDataStorage"
#pragma link "cxEdit"
#pragma link "cxFilter"
#pragma link "cxGraphics"
#pragma link "cxGrid"
#pragma link "cxGridCustomTableView"
#pragma link "cxGridCustomView"
#pragma link "cxGridLevel"
#pragma link "cxGridTableView"
#pragma link "cxImage"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxMaskEdit"
#pragma link "cxNavigator"
#pragma link "cxPC"
#pragma link "cxSplitter"
#pragma link "cxStyles"
#pragma link "cxTextEdit"
#pragma link "dxBar"
#pragma link "dxBarBuiltInMenu"
#pragma link "dxGDIPlusClasses"
#pragma link "dxStatusBar"
#pragma link "dxBarExtItems"
#pragma link "cxProgressBar"
#pragma link "cxMemo"
#pragma link "cxRichEdit"
#pragma resource "*.dfm"
TdsSelUnitListClientForm * dsSelUnitListClientForm;
// ---------------------------------------------------------------------------
__fastcall TdsSelUnitListClientForm::TdsSelUnitListClientForm(TComponent * Owner, TdsRegDM * ADM) : TForm(Owner)
 {
  FCodes                                = new TStringList;
  FDM                                   = ADM;
  SearchBE->Style->StyleController      = FDM->StyleController;
  ProgressPanel->Style->StyleController = FDM->StyleController;
  SaveEnInsert                          = false;
  SaveEnEdit                            = false;
  SaveEnDelete                          = false;
  SaveEnSetTemplate                     = false;
  SaveEnReSetTemplate                   = false;
  SaveEnRefresh                         = false;
  SaveEnFind                            = false;
  SaveEnFindNext                        = false;
  SaveEnViewClassParam                  = false;
  SaveEnViewTemplateParam               = false;
  FDataSrc                              = NULL;
  FFilter                               = "";
  FSearchIdx                            = 0;
  FSearchModify                         = false;
  // FSearchAndFilter = false;
  FSearchList = new TStringList;
  FProgress   = NULL;
  FSearchCodes.clear();
  FFilterMode   = true;
  FindFilterSrc = false;
  if (FDM->OnExtCount)
   CountED->Visible = true;
  else
   CountED->Visible = false;
  if (FDM->DefaultClassWidth)
   Width = FDM->DefaultClassWidth;
  RootClass = FDM->RegDef->GetChildByAV("classform", "group", "unit", true);
  if (RootClass)
   {
    RootClass = RootClass->GetFirstChild();
    UnitDesc  = RootClass->GetChildByName("description");
   }
  FCtrList = new TdsRegEDContainer(this, ExtListPC);
  FCtrList->DefXML       = RootClass;
  FCtrList->DataProvider = new TdsRegEDkabDSDataProvider;
  ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DefXML = RootClass;
  FCtrList->DataProvider->OnGetClassXML = FDM->OnGetXML;
  FCtrList->DataPath                    = FDM->DataPath;
  FLastTop                              = 0;
  FLastHeight                           = 0;
  Caption                               = SaveCaption;
  curCD                                 = 0;
  int AllItemsCount = 0;
  for (int i = 0; i < ClassTBM->Bars->Count; i++)
   {
    AllItemsCount += ClassTBM->Bars->Items[i]->ItemLinks->Count;
   }
  if (FDM->FullEdit)
   {
    // RootClass->Iterate(FSetEdit);
   }
  CheckCtrlVisible();
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSelUnitListClientForm::FSetEdit(TTagNode * itTag)
 {
  if (itTag->GetAttrByName("isedit"))
   itTag->AV["isedit"] = "1";
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::SetActiveClass(UnicodeString AClsId)
 {
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::ChangeSrc()
 {
  CheckCtrlState(false);
  try
   {
    try
     {
      Caption = SaveCaption + " " + _CLASSNODE_()->AV["name"];
      FCtrList->ClearLabItems();
      ClassGridView->DataController->CustomDataSource = NULL;
      if (FDataSrc)
       delete FDataSrc;
      FDataSrc                          = new TkabCustomDataSource(_CLASSNODE_());
      FDataSrc->DataSet->OnGetCount     = FDM->kabCDSGetCount;
      FDataSrc->DataSet->OnGetIdList    = FDM->kabCDSGetCodes;
      FDataSrc->DataSet->OnGetValById   = FDM->kabCDSGetValById;
      FDataSrc->DataSet->OnGetValById10 = FDM->kabCDSGetValById10;
      // FDataSrc->DataSet->OnInsert       = FDM->kabCDSInsert;
      // FDataSrc->DataSet->OnEdit = FDM->kabCDSEdit;
      // FDataSrc->DataSet->OnDelete       = FDM->kabCDSDelete;
      ClassGridView->BeginUpdate();
      try
       {
        FDataSrc->CreateColumns(ClassGridView);
        ClassGridView->DataController->CustomDataSource = FDataSrc;
        FDataSrc->DataChanged();
       }
      __finally
       {
        ClassGridView->EndUpdate();
       }
      ((TdsRegEDkabDSDataProvider *)FCtrList->DataProvider)->DataSource = FDataSrc;
      xcTop = 1;
      int FPC = ExtListPC->PageCount - 1;
      for (int i = FPC; i >= 0; i--)
       {
        FCurPage              = ExtListPC->Pages[i];
        FCurPage->PageControl = NULL;
        delete FCurPage;
       }
      _CLASSNODE_()->Iterate(CreateListExt);
      StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
      // CorrectCount(FDataSrc->DataSet->RecordCount);
     }
    catch (Exception & E)
     {
      _MSG_ERR(FMT1(icsErrorMsgCaptionSys, E.Message), FMT(icsErrorMsgCaption));
     }
    catch (...)
     {
      _MSG_ERR(FMT(icsRegistryErrorCommDBErrorMsgCaption), FMT(icsErrorMsgCaption));
     }
   }
  __finally
   {
    CheckCtrlState(true);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::FGetDataProgress(int AMax, int ACur, UnicodeString AMsg)
 {
  if (AMax == -1)
   StatusSB->Panels->Items[2]->Text = "";
  else if (!(AMax + ACur))
   StatusSB->Panels->Items[2]->Text = AMsg;
  else
   StatusSB->Panels->Items[2]->Text = AMsg + " [" + IntToStr(ACur) + "/" + IntToStr(AMax) + "]";
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::FormKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_RETURN)
   {
    if (actEdit->Enabled && Shift.Contains(ssShift))
     actEdit->OnExecute(this);
    else
     {
      if (SearchBE->IsFocused)
       {
        if (!FDataSrc->DataSet->RecordCount) // ���� ��� �������, �� ���������� ���������, ����� ����
         {
          actFindAndFilterExecute(actFindAndFilter);
          FFind(true);
         }
        else
         { // ���� �������� ����, �� ���������� ������ ���� ���� ������ ���������� ����������
          if (FFilterMode)
           {
            actFindAndFilterExecute(actFindAndFilter);
            FFind(true);
           }
          else
           actFindExecute(actFind);
         }
       }
     }
   }
  else if (Key == VK_DELETE && actDelete->Enabled)
   actDelete->OnExecute(this);
  else if (((Key == VK_UP) || (Key == VK_DOWN)) && FDataSrc->DataSet->RecordCount && SearchBE->IsFocused)
   {
    if (Key == VK_UP)
     ClassGridView->DataController->GotoPrev();
    else
     ClassGridView->DataController->GotoNext();
    if (FDataSrc->DataSet->CurRow)
     SetFocusedRec(FDataSrc->DataSet->GetRowIdxByCode(FDataSrc->DataSet->CurRow->Value["CODE"]));
    else
     SetFocusedRec(0);
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSelUnitListClientForm::CreateClassTV(TTagNode * itTag)
 {
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::FormDestroy(TObject * Sender)
 {
  delete FCodes;
  delete FSearchList;
  if (FProgress)
   delete FProgress;
  if (FCtrList)
   {
    if (FCtrList->DataProvider)
     delete FCtrList->DataProvider;
    delete FCtrList;
   }
  FCtrList = NULL;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::ListAfterScroll(TDataSet * DataSet)
 {
  FCtrList->LabDataChange();
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSelUnitListClientForm::CreateListExt(TTagNode * itTag)
 {
  if (itTag->CmpName("group") && itTag->GetParent()->CmpName("class"))
   {
    // TcxTabSheet *Page;
    // ExtPanelList->Add((void*) new TPanel(this));
    // CurExtList = (TPanel*)ExtPanelList->Items[ExtPanelList->Count-1];
    FCurPage              = new TcxTabSheet(this);
    FCurPage->Caption     = itTag->AV["name"];
    FCurPage->PageControl = ExtListPC;
    FCurPage->BorderWidth = 3;
    FCurPage->ImageIndex  = FCurPage->TabIndex;
    xcTop                 = 0;
   }
  TdsRegLabItem * tmpItem;
  if (!itTag->CmpName("class,description,actuals"))
   {
    if (itTag->CmpAV("inlist", "e"))
     {
      tmpItem      = FCtrList->AddLabItem(itTag, FCurPage->Width, FCurPage);
      tmpItem->Top = xcTop;
      xcTop += tmpItem->Height;
      // if (itTag->CmpName("extedit"))
      // tmpItem->OnGetExtData = FDM->OnGetExtData;
     }
   }
  return false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::actEditExecute(TObject * Sender)
 {
  if (actEdit->Enabled && IsRecSelected())
   {
    TTagNode * EditNode = NULL;
    TdsClassEditClientForm * Dlg = NULL;
    bool CanRefresh, CanCancel, CanCont;
    try
     {
      CanRefresh = true;
      CanCancel  = true;
      EditNode   = new TTagNode(NULL);
      EditNode->Assign(_CLASSNODE_(), true);
      try
       {
        CanCont = true;
        bool CanReopen = false;
        if (CanCont)
         {
          Dlg = new TdsClassEditClientForm(this, false, EditNode, FDM, _CLASSNODE_()->GetRoot(), FDataSrc,
           FCtrList->TemplateData, FDM->SingleEditCol);
          if (Dlg)
           {
            if (Dlg->ShowModal() == mrOk)
             {
              CanRefresh = true;
              CanCancel  = false;
              curCD      = Dlg->InsertedRecId.ToIntDef(0);
             }
            else
             CanRefresh = CanReopen;
           }
         }
        else
         {
          CanRefresh = CanReopen;
          CanCancel  = false;
         }
       }
      catch (Exception & E)
       {
        _MSG_ERR(FMT1(icsRegErrorEditRecSys, E.Message), FMT(icsErrorMsgCaption));
       }
      catch (...)
       {
        _MSG_ERR(FMT(icsRegErrorEditRec), FMT(icsErrorMsgCaption));
       }
     }
    __finally
     {
      if (Dlg)
       delete Dlg;
      if (EditNode)
       delete EditNode;
      if (CanCancel)
       {
        FDataSrc->DataSet->Cancel();
       }
      if (CanRefresh)
       {
        if (FDM->OnClassAfterEdit)
         FDM->OnClassAfterEdit(_CLASSNODE_());
        RefreshById(IntToStr(curCD));
       }
     }
    SearchBE->Clear();
    FFilterMode = true;
    SetActiveCtrl();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::actDeleteExecute(TObject * Sender)
 {
  if (actDelete->Enabled && IsRecSelected())
   {
    bool CanRefresh, CanCancel;
    try
     {
      CanRefresh = true;
      CanCancel  = true;
      try
       {
        /*
         The GetSelectedRowIndex function returns the row index of a selected row. Row indexes define the order in which rows (data records and grouping rows) appear in the data controller. See the TcxCustomDataController topic for information on row indexes.

         The Index parameter addresses a particular selected row by its position within the collection of all selected rows.
         Index can vary from 0 to GetSelectedCount � 1.

         GetSelectedRowIndex returns valid row indexes for selected rows only in provider, unbound and default bound modes.
         */
        int FSelCount = ClassGridView->DataController->GetSelectedCount();
        if (FSelCount > 1)
         { // �������� ��������� �����
          for (int i = FSelCount - 1; i >= 0; i--)
           {
            FDataSrc->DataSet->Delete(i);
           }
          CanRefresh = true;
          CanCancel = false;
         }
        else
         { // �������� ���� ������

          if (FDataSrc->CurrentRecIdx >= 0)
           {
            FDataSrc->DataSet->Delete();
            CanRefresh = true;
            CanCancel  = false;
           }
         }
       }
      catch (Exception & E)
       {
        _MSG_ERR(FMT1(icsRegErrorDeleteRecSys, E.Message), FMT(icsErrorMsgCaption));
       }
      catch (...)
       {
        _MSG_ERR(FMT(icsRegErrorDeleteRec), FMT(icsErrorMsgCaption));
       }
     }
    __finally
     {
      if (CanCancel)
       FDataSrc->DataSet->Cancel();
      if (CanRefresh)
       actRefreshExecute(actRefresh);
     }
    SearchBE->Clear();
    FFilterMode = true;
    SetActiveCtrl();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::FSetFilterExecute(UnicodeString AFilterGUI)
 {
  if (!actSetFilter->Checked)
   {
    if (FDM->OnExtFilterSet)
     {
      bool FCanSet = false;
      UnicodeString FTabSel = "";
      if (AFilterGUI.Length())
       FFilter = FDM->OnExtFilterSet(RootClass, FCanSet, AFilterGUI);
      else
       FFilter = FDM->OnExtFilterSet(RootClass, FCanSet, FFilter);
      if (FCanSet)
       {
        actSetFilter->Checked = true;
        actRefreshExecute(actRefresh);
       }
     }
   }
  else
   {
    actSetFilter->Checked = false;
    actRefreshExecute(actRefresh);
   }
  SetActiveCtrl();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::actSetFilterExecute(TObject * Sender)
 {
  FSetFilterExecute();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::actReSetFilterExecute(TObject * Sender)
 {
  FFilter               = "";
  actSetFilter->Checked = false;
  if (FDM->OnExtFilterSet)
   {
    bool tmp = false;
    FDM->OnExtFilterSet(RootClass, tmp, "clear");
   }
  actRefreshExecute(actRefresh);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::FGetData()
 {
  // TdsRegListType { ltFull = 0, ltFilter = 1, ltSearchFilter = 2};
  if (actSetFilter->Checked)
   {
    if (!FFilter.Trim().Length())
     ClearData();
    else
     FDataSrc->DataSet->GetData(FFilter, FDM->FetchAll, FDM->FetchRecCount, FGetDataProgress);
   }
  else
   {
    // if (FDM->UnitListOpenType == TdsRegListType::ltFull)
    // FDataSrc->DataSet->GetData("", FDM->FetchAll, FDM->FetchRecCount, FGetDataProgress);
    // else
    ClearData();
   }
  FindFilterSrc = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::ClearData()
 {
  FDataSrc->DataSet->Clear();
  StatusSB->Panels->Items[1]->Text = "";
  StatusSB->Panels->Items[2]->Text = "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::actRefreshExecute(TObject * Sender)
 {
  CheckCtrlState(false);
  ClassGridView->BeginUpdate();
  try
   {
    if (!FindFilterSrc)
     FGetData();
    FDataSrc->DataChanged();
    GetCommCount();
    StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
   }
  __finally
   {
    CheckCtrlState(true);
    ClassGridView->EndUpdate();
   }
  SearchBE->Clear();
  FFilterMode = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::RefreshById(UnicodeString ACode)
 {
  CheckCtrlState(false);
  ClassGridView->BeginUpdate();
  try
   {
    FDataSrc->DataSet->GetDataById(ACode, FGetDataProgress);
    FDataSrc->DataChanged();
    GetCommCount();
    StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
   }
  __finally
   {
    ClassGridView->EndUpdate();
    CheckCtrlState(true);
    SetFocusedRec(FDataSrc->DataSet->GetRowIdxByCode(ACode));
   }
 }
// ---------------------------------------------------------------------------
TcxGridColumn * __fastcall TdsSelUnitListClientForm::ClassCol(int AIdx)
 {
  return ClassGridView->Columns[AIdx];
 }
// ---------------------------------------------------------------------------
TTagNode * __fastcall TdsSelUnitListClientForm::_CLASSNODE_()
 {
  return RootClass;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSelUnitListClientForm::GetBtnEn(UnicodeString AAttrName)
 {
  return ((bool)(_CLASSNODE_()->GetChildByName("description")->AV[AAttrName].Length()));
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::actExitExecute(TObject * Sender)
 {
  ModalResult = mrCancel;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::FormShow(TObject * Sender)
 {
  ShowTimer->Enabled = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::ClassGridViewFocusedRecordChanged(TcxCustomGridTableView * Sender,
 TcxCustomGridRecord * APrevFocusedRecord, TcxCustomGridRecord * AFocusedRecord, bool ANewItemRecordFocusingChanged)
 {
  if (IsRecSelected())
   FDataSrc->DataSet->GetDataById(VarToStr(FDataSrc->DataSet->CurRow->Value["CODE"]), FGetDataProgress);
  FCtrList->LabDataChange();
  actEdit->Enabled   = IsRecSelected() && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("editlabel"));
  actDelete->Enabled = IsRecSelected() && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("deletelabel"));
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSelUnitListClientForm::IsRecSelected()
 {
  return ClassGridView->DataController->CustomDataSource && (FDataSrc->CurrentRecIdx != -1);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsSelUnitListClientForm::GetUnitStr()
 {
  TTagNode * itNode = RootClass->GetChildByName("fields", true)->GetFirstChild();
  UnicodeString RC = "";
  if (IsRecSelected())
   {
    while (itNode)
     {
      RC += VarToWideStrDef(FDataSrc->DataSet->CurRow->Value[itNode->AV["ref"]], "") + " ";
      itNode = itNode->GetNext();
     }
   }
  return RC.Trim();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::FGetQuickFilterList(bool ASet)
 {
  for (int i = QuickFilterMenu->ItemLinks->Count - 1; i > 1; i--)
   {
    delete(TdxBarButton *)QuickFilterMenu->ItemLinks->Items[i]->Item;
   }
  if (FDM->OnExtFilterSet && FDM->OnGetQuickFilterList)
   {
    TStringList * FQFList = NULL;
    try
     {
      FQFList = new TStringList;
      if (ASet)
       FQFList->Add("set");
      else
       FQFList->Add("add");
      FDM->OnGetQuickFilterList("filter:������ �������� ��� ������������", FQFList);
      UnicodeString FCaption, FGUI;
      FQuickFilterList.clear();
      TdxBarButton * ExtBtn;
      for (int i = 0; i < FQFList->Count; i++)
       {
        FCaption = GetRPartB(FQFList->Strings[i], '=');
        FGUI     = GetLPartB(FQFList->Strings[i], '=');
        ExtBtn   = new TdxBarButton(NULL);
        QuickFilterMenu->ItemLinks->Add()->Item = ExtBtn;
        ExtBtn->Caption               = FCaption;
        ExtBtn->ImageIndex            = -1;
        ExtBtn->Tag                   = i + 1;
        FQuickFilterList[ExtBtn->Tag] = FGUI;
        ExtBtn->OnClick               = QuickFilterItemClick;
       }
     }
    __finally
     {
      if (FQFList)
       delete FQFList;
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::QuickFilterItemClick(TObject * Sender)
 {
  UnicodeString FQFGUI;
  if (FDM->OnExtFilterSet)
   {
    FQFGUI = FQuickFilterList[((TdxBarButton *)Sender)->Tag];
    if (FQFGUI.Trim().Length())
     {
      FSetFilterExecute(FQFGUI);
     }
   }
  SetActiveCtrl();
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSelUnitListClientForm::CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg)
 {
  Application->ProcessMessages();
  switch (AType)
   {
   case TkabProgressType::InitCommon:
     {
      SetFocus();
      PrBar->Properties->Max           = AVal;
      StatusSB->Panels->Items[2]->Text = AMsg;
      break;
     }
   case TkabProgressType::StageInc:
     {
      // MsgLab->Caption = AMsg;
      StatusSB->Panels->Items[2]->Text = AMsg;
      break;
     }
   case TkabProgressType::CommInc:
     {
      Caption                          = SaveCaption + " {" + FloatToStrF(AVal / 10.0, ffFixed, 2, 1) + "%}";
      StatusSB->Panels->Items[2]->Text = AMsg;
      PrBar->Position                  = AVal;
      break;
     }
   case TkabProgressType::CommComplite:
     {
      // MsgLab->Caption = "";
      // PrBar->Position = 0;
      StatusSB->Panels->Items[2]->Text = "";
      Caption                          = SaveCaption + " " + _CLASSNODE_()->AV["name"];
      break;
     }
   }
  Application->ProcessMessages();
  return InProgress;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::ShowTimerTimer(TObject * Sender)
 {
  ShowTimer->Enabled = false;
  Application->ProcessMessages();
  ChangeSrc();
  // if (FDM->UnitListOpenType == TdsRegListType::ltFull)
  // actRefreshExecute(actRefresh);
  // else if (FDM->UnitListOpenType == TdsRegListType::ltFilter)
  // FSetFilterExecute();
  // else
  GetCommCount();
  Application->ProcessMessages();
  FGetQuickFilterList(false);
  Application->ProcessMessages();
  SetActiveCtrl();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::CheckCtrlState(bool AEnable)
 {
  InProgress = !AEnable;
  if (AEnable)
   {
    // if (FProgress) delete FProgress;
    // FProgress = NULL;
    PrTimer->Enabled       = false;
    PrBar->Visible         = false;
    ProgressPanel->Visible = false;
    ProgressPanel->Width   = 0;
    ProgressPanel->Height  = 0;
   }
  else
   {
    PrTimer->Enabled       = true;
    PrBar->Visible         = true;
    ProgressPanel->Left    = 0;
    ProgressPanel->Top     = 0;
    ProgressPanel->Width   = ClientWidth;
    ProgressPanel->Height  = ClientHeight - StatusSB->Height - 5;
    ProgressPanel->Visible = true;
    ProgressPanel->BringToFront();
   }
  Application->ProcessMessages();
  if (!AEnable)
   {
    actEdit->Enabled   = false;
    actDelete->Enabled = false;
   }
  else if (ClassGridView->DataController->CustomDataSource && FDataSrc)
   {
    if (FDataSrc->DataSet->RecordCount && (FDataSrc->CurrentRecIdx != -1))
     {
      actEdit->Enabled   = AEnable && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("editlabel"));
      actDelete->Enabled = AEnable && FDM->ClassEditable && (FDM->FullEdit | GetBtnEn("deletelabel"));
     }
    else
     {
      actEdit->Enabled   = false;
      actDelete->Enabled = false;
     }
   }
  Application->ProcessMessages();
  actRefresh->Enabled     = AEnable;
  actSetFilter->Enabled   = AEnable && actSetFilter->Visible;
  actReSetFilter->Enabled = AEnable && actReSetFilter->Visible;
  SearchBE->Enabled       = AEnable;
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::CheckCtrlVisible()
 {
  if (!_CLASSNODE_()->GetChildByName("description")->GetChildByName("maskfields"))
   {
    actSetFilter->Visible   = false;
    actReSetFilter->Visible = false;
   }
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::GetSelectedUnitData(TkabCustomDataSetRow *& AData)
 {
  AData = NULL;
  if (FDataSrc->DataSet->RecordCount && (FDataSrc->CurrentRecIdx != -1))
   {
    AData = new TkabCustomDataSetRow(FDataSrc->DataSet->DefNode, FDataSrc->DataSet->CurRow);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::actFindExecute(TObject * Sender)
 {
  FFind(false);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::FFind(bool ASilent)
 {
  if (SearchBE->Text.Trim().Length() && (SearchBE->Text.Trim().UpperCase() != "�����"))
   {
    CheckCtrlState(false);
    ClassGridView->BeginUpdate();
    try
     {
      if (FSearchModify)
       {
        FSearchCodes.clear();
        TJSONObject * FRC;
        UnicodeString tmpValue;
        if (FDM->OnFind("reg.1000", "findbyfield:" + SearchBE->Text.Trim(), FRC))
         {
          for (int i = 0; i < ((TJSONArray *)FRC)->Count; i++)
           {
            tmpValue = ((TJSONString *)((TJSONArray *)FRC)->Items[i])->Value();
            if (FDataSrc->DataSet->GetRowIdxByCode(tmpValue) != -1)
             FSearchCodes.push_back(tmpValue);
           }
         }
        FSearchModify = false;
       }
      if (FSearchCodes.size())
       {
        SetFocusedRec(FDataSrc->DataSet->GetRowIdxByCode(FSearchCodes.front()));
        FSearchCodes.push_back(FSearchCodes.front());
        FSearchCodes.pop_front();
       }
      else if (!ASilent)
       _MSG_INF("����� ���������, �� �� ������ ����� :(", "���������");
     }
    __finally
     {
      ClassGridView->EndUpdate();
      CheckCtrlState(true);
     }
    SetActiveCtrl();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::actFindAndFilterExecute(TObject * Sender)
 {
  if (FDM->OnFind && SearchBE->Text.Trim().Length() /* && !FSearchAndFilter */ &&
   (SearchBE->Text.Trim().UpperCase() != "�����"))
   {
    CheckCtrlState(false);
    ClassGridView->BeginUpdate();
    try
     {
      actSetFilter->Checked = false;
      TJSONObject * FRC;
      if (FDM->OnFind("reg.1000", SearchBE->Text.Trim(), FRC))
       {
        FDataSrc->DataSet->FilterByFind((TJSONArray *)FRC, FDM->FetchAll, FDM->FetchRecCount, FGetDataProgress);
        FindFilterSrc = true;
        FDataSrc->DataChanged();
        StatusSB->Panels->Items[1]->Text = IntToStr(FDataSrc->DataSet->RecordCount);
        GetCommCount();
       }
     }
    __finally
     {
      CheckCtrlState(true);
      ClassGridView->EndUpdate();
      if (FDataSrc->DataSet->RecordCount)
       SetFocusedRec(0);
     }
    SetActiveCtrl();
    FFilterMode = false;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::actClearSearchFieldExecute(TObject * Sender)
 {
  if (SearchBE->Text.Trim().Length())
   {
    SearchBE->Clear();
    FFilterMode = true;
    actRefreshExecute(actRefresh);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::SearchBEPropertiesChange(TObject * Sender)
 {
  FSearchIdx    = 0;
  FSearchModify = true;
  // FSearchAndFilter = false;
  if (!SearchBE->Text.Trim().Length())
   FFilterMode = true;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::FFetchAllRec()
 {
  if (!FDataSrc->DataSet->IsAllFetched())
   {
    CheckCtrlState(false);
    try
     {
      FDataSrc->DataSet->FetchAll(FDM->FetchRecCount, FGetDataProgress);
     }
    __finally
     {
      CheckCtrlState(true);
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::FormActivate(TObject * Sender)
 {
  if (SearchBE->Enabled)
   {
    SearchBE->Clear();
    FFilterMode = true;
    SearchBE->SetFocus();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::PrTimerTimer(TObject * Sender)
 {
  Application->ProcessMessages();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::SetInProgress(bool AVal)
 {
  // ApplyBtn->Caption = (AVal)? "��������":"��������� � ��";
  FInProgress = AVal;
  // PrBar->Visible = AVal;
  // if (AVal)  PrBar->Position = 0;
  // else       StatusBar->Panels->Items[1]->Text = "";
  // SetEnabled(!AVal);
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::WndProc(Messages::TMessage & Message)
 {
  switch (Message.Msg)
   {
   case WM_CLOSE:
    if (CanProgress())
     {
      Message.Result = 0;
      return;
     }
    else
     inherited::WndProc(Message);
    break;
   }
  inherited::WndProc(Message);
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsSelUnitListClientForm::CanProgress()
 {
  bool RC = false;
  if (InProgress)
   {
    if (_MSG_QUE("��������?", "���������") == ID_YES)
     {
      InProgress = false;
      RC         = true;
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::SetActiveCtrl()
 {
  if (SearchBE->Enabled)
   SearchBE->SetFocus();
  else
   ClassGrid->SetFocus();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::SetFocusedRec(__int64 ARowIdx)
 {
  ClassGridView->Controller->ClearSelection();
  ClassGridView->DataController->FocusedRecordIndex = ARowIdx;
  if (ClassGridView->Controller->FocusedRecord)
   {
    ClassGridView->Controller->FocusedRecord->Selected = true;
    ClassGridView->Controller->FocusedRecord->MakeVisible();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::GetCommCount()
 {
  if (FDM->OnExtCount)
   {
    UnicodeString RC = "";
    FDM->OnExtCount(RC);
    CountED->Lines->Text                  = RC;
    CountED->Properties->VisibleLineCount = CountED->Lines->Count + 1;
   }
  else
   {
    CountED->Lines->Text                  = "";
    CountED->Properties->VisibleLineCount = 0;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::ApplyMIClick(TObject * Sender)
 {
  ApplyMI->DropDown();
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::actApplyExecute(TObject * Sender)
 {
  CheckCtrlState(false);
  ClassGridView->BeginUpdate();
  try
   {
    FCodes->Clear();
    UnicodeString Str;
    for (int i = 0; i < FDataSrc->DataSet->RecordCount; i++)
     {
      FGetDataProgress(FDataSrc->DataSet->RecordCount, i + 1, "��������� ������");
      FDataSrc->DataSet->GetDataById(FDataSrc->DataSet->Row[i]->Value["code"], NULL);
      Str = VarToStr(FDataSrc->DataSet->Row[i]->Value["code"]);
      Str += "##" + VarToStr(FDataSrc->DataSet->Row[i]->Value["001f"]);
      Str += "##" + VarToStr(FDataSrc->DataSet->Row[i]->Value["0020"]);
      Str += "##" + VarToStr(FDataSrc->DataSet->Row[i]->Value["002f"]);
      Str += "##" + VarToStr(FDataSrc->DataSet->Row[i]->Value["0031"]);
      Str += "##" + FDataSrc->DataSet->Row[i]->StrValue["0136"];
      Str += "##" + FDataSrc->DataSet->Row[i]->StrValue["0025"];
      FCodes->Add(Str);
     }

    if (FCodes->Count)
     ModalResult = mrOk;
    else
     ModalResult = mrCancel;
   }
  __finally
   {
    CheckCtrlState(true);
    ClassGridView->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TdsSelUnitListClientForm::actApplySelectedExecute(TObject * Sender)
 {
  CheckCtrlState(false);
  ClassGridView->BeginUpdate();
  try
   {
    FCodes->Clear();
    UnicodeString Str;
    int FSelCount = ClassGridView->DataController->GetSelectedCount();
    int SelIdx;
    if (FSelCount >= 1)
     { // �������� ��������� 1+ �����
      for (int i = 0; i < FSelCount; i++)
       {
        FGetDataProgress(FDataSrc->DataSet->RecordCount, i + 1, "��������� ������");
        SelIdx = ClassGridView->DataController->GetSelectedRowIndex(i);
        FDataSrc->DataSet->GetDataById(FDataSrc->DataSet->Row[SelIdx]->Value["code"], NULL);
        Str = VarToStr(FDataSrc->DataSet->Row[SelIdx]->Value["code"]);
        Str += "##" + VarToStr(FDataSrc->DataSet->Row[SelIdx]->Value["001f"]);
        Str += "##" + VarToStr(FDataSrc->DataSet->Row[SelIdx]->Value["0020"]);
        Str += "##" + VarToStr(FDataSrc->DataSet->Row[SelIdx]->Value["002f"]);
        Str += "##" + VarToStr(FDataSrc->DataSet->Row[SelIdx]->Value["0031"]);
        Str += "##" + FDataSrc->DataSet->Row[SelIdx]->StrValue["0136"];
        Str += "##" + FDataSrc->DataSet->Row[SelIdx]->StrValue["0025"];
        FCodes->Add(Str);
       }
     }

    if (FCodes->Count)
     ModalResult = mrOk;
    else
     ModalResult = mrCancel;
   }
  __finally
   {
    CheckCtrlState(true);
    ClassGridView->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
