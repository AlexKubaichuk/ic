// ---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
// ---------------------------------------------------------------------------
#include "dsRegTemplate.h"
#include "dsRegTemplateSeparator.h"
// #include "Reg_DMF.h"
#include "msgdef.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// ---------------------------------------------------------------------------
/*
 <!ELEMENT root (fl|gr|separator)+>            //������
 <!ATTLIST root ref CDATA #IMPLIED>  // ref - uid �������������� ��� ��������
 // ����������� ������
 // ���� ref = NULL - ������ �����������
 // ��� ������� ����� �� �������� ������������

 <!ELEMENT gr (fl|gr|separator)+>              // ������
 <!ATTLIST gr ref CDATA #REQUIRED>   // ������ ��� �������� ����������� ����� ��
 // �������������� �� ������� ��������� �������
 // � uid-�� = ref, ������ ������� ����� ���� ������
 // ���� "choiceDB" - ������ ������ �� �������������

 <!ELEMENT fl EMPTY>                 // ����
 <!ATTLIST fl ref CDATA #REQUIRED    // �������� ���� �� �������� �������� ����������
 def CDATA #IMPLIED     // ��������� ����� ref - uid ��������
 show (0|1) "1"         // ���������� ��������� �������� � ???
 req (0|1) "0"          // �������������� ������� ��������
 "0" - �� �����������
 "1" - �����������
 "2" - �������������� ������������ ��������� ��������
 en  (0|1) "1">         // def - �������� �� ���������
 // en - ����������� ��� ��������������

 <!ELEMENT separator EMPTY>          // �����������

 AFields -  XML - �������� ����� �������
 ACompPanel - ������ ��� ���������� ��������� �������
 AquFree -  ��������� �� FIBQuery ��� ������� � ��
 ATreePath - ������ ���� � ����� �������� ���
 ATree - ��������� �� TTagNode - � ����������� ���������
 */
// ---------------------------------------------------------------------------
__fastcall EdsRegTemplateError::EdsRegTemplateError(UnicodeString Msg) : Sysutils::Exception(Msg.c_str())
 {
 };
// ---------------------------------------------------------------------------
__fastcall TdsRegTemplate::TdsRegTemplate(TTagNode * ARegDef)
 {
  FReqColor        = clInfoBk;
  FIsTemplateField = false;
  FRegDef          = ARegDef;
  FCompPanel       = NULL;
  FFltFl           = new TTagNode(NULL);
  FTmplFieldList   = new TStringList;
  FSepList         = new TObjectList;
  FDataProvider    = new TdsRegEDTemplateDataProvider;
  FStyleController = NULL;
  FMaxHeight       = Screen->Height / 2;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTemplate::CreateTemplate(UnicodeString AFields, TdsRegTemplateData * ATemplateData,
  int AFixHeight)
 {
  if (!AFields.Length())
   throw Exception("�� ����� ������ ����� �������.");
  FFltFl->AsXML     = AFields;
  FCopyTemplateData = ATemplateData;
  FHeight           = 0;
  FWidth            = 0;
  FLastTop          = 0;
  FLastHeight       = 0;
  if (!FCompPanel)
   throw Exception("�� ����� ��������� ��� �������.");
  FTmplFieldList->Clear();
  FSepList->Clear();
  xTop                      = 2;
  MaxW                      = 20;
  FCtrList                  = new TdsRegEDContainer(FCompPanel->Owner, FCompPanel);
  FCtrList->DefXML          = FRegDef;
  FCtrList->ReqColor        = FReqColor;
  FCtrList->TemplateField   = FIsTemplateField;
  FCtrList->DataProvider    = FDataProvider;
  FCtrList->TemplateData    = ATemplateData;
  FCtrList->OnExtBtnClick   = FOnExtBtnClick;
  FCtrList->StyleController = FStyleController;
  FCtrList->DataPath        = FDataPath;
  // FDataProvider->DefXML = FRegDef;
  MS = false;
  int FlCount = FFltFl->GetCount(true, "fl,separator");
  if (AFixHeight)
   FMaxHeight = AFixHeight;
  else
   FMaxHeight = Screen->Height / 2;
  if ((FlCount + 2) * 21 > FMaxHeight)
   MS = true;
  FlCounter = 0;
  FNextCol  = false;
  FFltFl->Iterate(CreateTmpl);
  MaxW = max(MaxW, FCompPanel->Width - 5);
  for (int i = 0; i < FTmplFieldList->Count; i++)
   {
    FCtrList->TemplateItems[FTmplFieldList->Strings[i]]->Width = MaxW - 10;
    FCtrList->TemplateItems[FTmplFieldList->Strings[i]]->UpdateChanges();
   }
  Application->ProcessMessages();
  for (int i = 0; i < FSepList->Count; i++)
   {
    ((TdsRegTEmplateSeparator *)FSepList->Items[i])->Width = MaxW - 10;
   }
  if (FTmplFieldList->Count)
   {
    FHeight = FLastTop + FLastHeight + 3;
    if (MS && FNextCol)
     FWidth = MaxW * 2 + 10;
    else
     FWidth = MaxW + 5;
    FCompPanel->Height = FHeight;
    FCompPanel->Width  = FWidth;
    FFltFl->Iterate(FSetDefaultValue);
   }
  else
   {
    FHeight = 0;
    FWidth  = 0;
   }
 }
// ---------------------------------------------------------------------------
__fastcall TdsRegTemplate::~TdsRegTemplate()
 {
  delete FCtrList;
  delete FDataProvider;
  delete FTmplFieldList;
  delete FSepList;
  delete FFltFl;
 }
// ---------------------------------------------------------------------------
TWinControl * __fastcall TdsRegTemplate::GetFirstEnabledControl()
 {
  TControl * tmpCtrl;
  TdsRegEDItem * tmpItem;
  TTagNode * itNode = FFltFl->GetFirstChild();
  while (itNode)
   {
    if (FCtrList->GetTemplateControl(itNode->AV["ref"]))
     {
      tmpItem = FCtrList->TemplateItems[itNode->AV["ref"]];
      tmpCtrl = tmpItem->GetFirstEnabledControl();
      if (tmpCtrl)
       {
        return (TWinControl *)tmpCtrl;
       }
     }
    itNode = itNode->GetNext();
   }
  return NULL;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::CreateTmpl(TTagNode * itxTag)
 {
  TdsRegEDItem * tmpItem;
  TTagNode * FNode;
  FlCounter++ ;
  if (itxTag->CmpName("fl"))
   {
    FNode = FRegDef->GetTagByUID(itxTag->AV["ref"]);
    if (FNode)
     {
      if (FNode->CmpAV("isedit", "0") && !FNode->CmpAV("depend", ""))
       {
        tmpItem = FCtrList->AddTemplateItem(FNode);
        tmpItem->SetEnableEx(itxTag->AV["en"].ToIntDef(1));
        tmpItem->SetIsVal(true);
        FTmplFieldList->Add(FNode->AV["uid"]);
        tmpItem->Visible = false;
       }
      else
       {
        tmpItem = FCtrList->AddTemplateItem(FNode);
        tmpItem->SetEnableEx(itxTag->AV["en"].ToIntDef(1));
        tmpItem->SetIsVal(true);
        if (MS && FNextCol)
         tmpItem->Left = FCompPanel->Width + 2;
        else
         tmpItem->Left = 2;
        tmpItem->Top = xTop;
        xTop += 2 + tmpItem->Height;
        MaxW = max(tmpItem->Width, MaxW);
        if (FLastTop < xTop)
         FLastTop = xTop;
        FLastHeight = tmpItem->Height;
        FTmplFieldList->Add(FNode->AV["uid"]);
       }
      tmpItem->Required = itxTag->AV["req"].ToIntDef(0);
     }
    else
     throw EdsRegTemplateError(FMT1(icsTemplateRefError, itxTag->AV["ref"]));
   }
  else if (itxTag->CmpName("separator"))
   {
    TdsRegTEmplateSeparator * tmpSep = new TdsRegTEmplateSeparator(FCompPanel, FCompPanel->Width,
    itxTag->AV["comment"]);
    FSepList->Add(tmpSep);
    xTop += 3;
    tmpSep->Top = xTop;
    xTop += 12;
    if (MS && FNextCol)
     tmpSep->Left = FCompPanel->Width + 2;
    else
     tmpSep->Left = 3;
    if (FLastTop < xTop)
     FLastTop = xTop;
    FLastHeight = 4;
    if (MS && !FNextCol)
     {
      if ((FlCounter) * 21 > FMaxHeight)
       {
        xTop     = 0;
        FNextCol = true;
       }
     }
   }
  return false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::FSetDefaultValue(TTagNode * itxTag)
 {
  if (itxTag->CmpName("fl"))
   {
    // FCtrList->TemplateItems[itxTag->AV["ref"]]->DataChange();
    UnicodeString FUID = itxTag->AV["ref"];
    UnicodeString FDef = itxTag->AV["def"];
    bool FEn = itxTag->AV["en"].ToIntDef(1);
    if (FCopyTemplateData)
     {
      if (FCopyTemplateData->IsTemplate(FUID))
       {
        FCtrList->TemplateItems[FUID]->SetValue(VarToStr(FCopyTemplateData->Value[FUID]), FEn);
       }
      else if (FDef.Length())
       {
        FCtrList->TemplateItems[FUID]->SetValue(FDef, FEn);
       }
     }
    else if (FDef.Length())
     {
      FCtrList->TemplateItems[FUID]->SetValue(FDef, FEn);
     }
   }
  return false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::FPrepare()
 { // ������ ��������� � xml
  bool IsEmpty = true;
  try
   {
    for (int i = 0; i < FTmplFieldList->Count; i++)
     {
      // FCtrList->TemplateItems[FTmplFieldList->Strings[i]]->UpdateChanges();
      IsEmpty &= !FCtrList->IsTemplate(FTmplFieldList->Strings[i]);
     }
   }
  __finally
   {
   }
  return IsEmpty;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::CheckInput(bool ASilent)
 {
  bool RC = false;
  try
   {
    FSilent = ASilent;
    UnicodeString DDD = "1";
    FFltFl->Iterate(FCheckInput, DDD);
    if (DDD.Length())
     {
      if (FFltFl->CmpAV("blank", "1"))
       RC = true;
      else
       {
        if (!IsEmpty())
         RC = true;
        else
         {
          if (!ASilent)
           _MSG_ERRA(FMT(icsTemplateReqEmpty), FMT(icsErrorMsgCaption));
         }
       }
     }
   }
  __finally
   {
    FSilent = false;
   }
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::FCheckInput(TTagNode * itxTag, UnicodeString & tmp)
 {
  bool RC = false;
  try
   {
    if (itxTag->CmpName("fl") && itxTag->AV["req"].ToIntDef(0))
     RC = !FCtrList->TemplateItems[itxTag->AV["ref"]]->CheckReqValue(NULL, FSilent);
    if (RC)
     tmp = "";
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTemplate::Clear()
 {
  for (int i = 0; i < FTmplFieldList->Count; i++)
   FCtrList->TemplateData->Value[FTmplFieldList->Strings[i]] = Variant::Empty();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplate::GetValue(UnicodeString AUID)
 {
  return FGetValue(AUID.UpperCase(), false);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplate::GetName(UnicodeString AUID)
 {
  TTagNode * xTag;
  xTag = FRegDef->GetTagByUID(AUID);
  if (xTag)
   {
    return xTag->AV["name"] + "=" + FCtrList->TemplateItems[xTag->AV["uid"]]->GetValueStr();
   }
  else
   return "";
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTemplate::FSetWidth(int AWidth)
 {
  MaxW = AWidth - 5;
  for (int i = 0; i < FSepList->Count; i++)
   {
    ((TdsRegTEmplateSeparator *)FSepList->Items[i])->Width = MaxW + 10;
   }
  for (int i = 0; i < FTmplFieldList->Count; i++)
   {
    FCtrList->TemplateItems[FTmplFieldList->Strings[i]]->Width = MaxW - 10;
    FCtrList->TemplateItems[FTmplFieldList->Strings[i]]->Update(); // UpdateChanges();
   }
  Application->ProcessMessages();
  FWidth            = AWidth;
  FCompPanel->Width = AWidth;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::IsEmpty()
 {
  return FPrepare();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplate::GetSelectedTextValues(bool CanFieldName)
 {
  UnicodeString RC = "";
  if (CanFieldName)
   FFltFl->Iterate(FGetSelectedTextFieldValues, RC);
  else
   FFltFl->Iterate(FGetSelectedTextValues, RC);
  return RC;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::FGetSelectedTextValues(TTagNode * itxTag, UnicodeString & Values)
 {
  if (itxTag->CmpName("fl"))
   {
    TTagNode * xTag;
    xTag = FRegDef->GetTagByUID(itxTag->AV["ref"]);
    if (xTag)
     {
      if (itxTag->AV["show"].ToIntDef(1))
       {
        if (FGetValue(itxTag->AV["ref"], true).Length())
         Values += FCtrList->TemplateItems[xTag->AV["uid"]]->GetValueStr(true) + "\n";
       }
     }
   }
  return false;
 }
// ---------------------------------------------------------------------------
bool __fastcall TdsRegTemplate::FGetSelectedTextFieldValues(TTagNode * itxTag, UnicodeString & Values)
 {
  if (itxTag->CmpName("fl"))
   {
    TTagNode * xTag;
    xTag = FRegDef->GetTagByUID(itxTag->AV["ref"]);
    if (xTag)
     {
      if (itxTag->AV["show"].ToIntDef(1))
       {
        if (FGetValue(itxTag->AV["ref"], true).Length())
         Values += xTag->AV["name"] + "#" + FCtrList->TemplateItems[xTag->AV["uid"]]->GetValueStr(true) + "\n";
       }
     }
   }
  return false;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegTemplate::FGetValue(UnicodeString AUID, bool ABinaryNames, UnicodeString ADefVal)
 {
  UnicodeString RC = ADefVal;
  try
   {
    TTagNode * xTag;
    xTag = FRegDef->GetTagByUID(AUID);
    Variant iVal = FCtrList->TemplateData->Value[xTag->AV["uid"]];
    if (!iVal.IsEmpty())
     {
      if (xTag->CmpName("binary"))
       {
        if (ABinaryNames)
         RC = ((int)iVal) ? xTag->AV["name"] : xTag->AV["invname"];
        else
         RC = UnicodeString(((int)iVal) ? "1" : "0");
       }
      else if (xTag->CmpName("text,choice,choiceDB,extedit,digit,choiceTree"))
       RC = VarToStr(iVal);
      else if (xTag->CmpName("date"))
       RC = TDateTime(VarToStr(iVal)).FormatString("dd.mm.yyyy");
     }
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
Variant __fastcall TdsRegTemplate::FGetVarValue(UnicodeString AUID)
 {
  Variant RC = Variant::Empty();
  try
   {
    TTagNode * xTag;
    xTag = FRegDef->GetTagByUID(AUID);
    if (xTag)
     RC = FCtrList->TemplateData->Value[xTag->AV["uid"]];
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TJSONObject * __fastcall TdsRegTemplate::GetJSONValue()
 {
  TJSONObject * RC = new TJSONObject;
  try
   {
    for (int i = 0; i < FTmplFieldList->Count; i++)
     RC->AddPair(FTmplFieldList->Strings[i].UpperCase(), FGetVarValue(FTmplFieldList->Strings[i]));
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TdsRegTemplate::FSetOnKeyDown(TKeyEvent AValue)
 {
  FOnKeyDown          = AValue;
  FCtrList->OnKeyDown = AValue;
 }
// ---------------------------------------------------------------------------
TdsRegTemplateData * __fastcall TdsRegTemplate::GetTemplateData()
 {
  return FCtrList->TemplateData;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall GetTemplateFields(TTagNode * ADescription)
 {
  TTagNode * FTmpl = ADescription->GetChildByName("maskfields");
  UnicodeString tmpl = "";
  if (FTmpl)
   {
    FTmpl = FTmpl->GetFirstChild();
    if (FTmpl->CmpName("field"))
     { // old template set
      TTagNode * tmp = new TTagNode(NULL);
      tmp->Name      = "root";
      tmp->AV["ref"] = "";
      while (FTmpl)
       {
        if (FTmpl->CmpName("field"))
         tmp->AddChild("fl", "ref=" + FTmpl->AV["ref"]);
        else
         tmp->AddChild("separator");
        FTmpl = FTmpl->GetNext();
       }
      tmpl = tmp->AsXML;
      delete tmp;
     }
    else
     tmpl = FTmpl->AsXML;
   }
  return tmpl;
 }
// ---------------------------------------------------------------------------
