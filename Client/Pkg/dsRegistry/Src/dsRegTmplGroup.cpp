﻿//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "dsRegTmplGroup.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
//---------------------------------------------------------------------------
__fastcall TriGroupTmpl::TriGroupTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode,
                                      TdsRegEDContainer *ACtrlOwner)
: TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner)
{
  FLab = new TLabel(this);
  FLab->Parent = this;
  FLab->Visible = false;

  ED = new TcxGroupBox(this);
  ED->Parent = this;
//  ED->Transparent = true;
  ED->Style->StyleController = FCtrlOwner->StyleController;
  ED->Style->Font->Style = TFontStyles() << fsBold;
  ED->CaptionBkColor = (TColor)0x00C08000;
//  ED->Style->TextColor = (TColor)0x00C08000;

  ED->Caption = " "+FNode->AV["name"]+" ";
  ED->Align  = alClient;

  CMaxH = 500;
  CMinH = 22;
  Width = 135;

//  if (FNode->GetParent()->CmpName("group"))  Width = 130;
//  else                                       Width = 135;
}
//---------------------------------------------------------------------------
TcxGroupBox* __fastcall TriGroupTmpl::FGetCastED()
{
  return (TcxGroupBox*)FED;
}
//---------------------------------------------------------------------------
void __fastcall TriGroupTmpl::FSetCastED(TcxGroupBox *AVal)
{
  FED = AVal;
}
//---------------------------------------------------------------------------
void __fastcall TriGroupTmpl::SetOnDependValues()
{
  FSetOnDependValues();
}
//---------------------------------------------------------------------------
__fastcall TriGroupTmpl::~TriGroupTmpl()
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
}
//---------------------------------------------------------------------------
void __fastcall TriGroupTmpl::SetEDLeft(int ALeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  IsAlign = true;
//  ED->Left = ALeft;
  Update();
}
//---------------------------------------------------------------------------
void __fastcall TriGroupTmpl::GetEDLeft(int *ALabR, int *AEDLeft)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  *AEDLeft = -1;
  *ALabR = -1;
//  else                                            *ALabR = -1;
}
//---------------------------------------------------------------------------
void __fastcall TriGroupTmpl::cResize(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
}
//---------------------------------------------------------------------------
bool __fastcall TriGroupTmpl::UpdateChanges()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return true;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriGroupTmpl::GetValueStr(bool ABinaryNames)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  UnicodeString xVal = "";
  return xVal;
}
//---------------------------------------------------------------------------
void __fastcall TriGroupTmpl::SetIsVal(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isVal = AEnable;
}
//---------------------------------------------------------------------------
void __fastcall TriGroupTmpl::SetEnable(bool AEnable)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  isEnabled = AEnable;
  Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
  ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate);
}
//---------------------------------------------------------------------------
void __fastcall TriGroupTmpl::SetValue(UnicodeString AVal,bool AEnable)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
}
//---------------------------------------------------------------------------
void __fastcall TriGroupTmpl::SetLabel(UnicodeString ACaption)
{
   if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   if (ACaption.Length()) ED->Caption = ACaption+":";
   else                   ED->Caption = "";//FNode->AV["name"]+":";
//   ED->Update();
//   if (ACaption.Length()) Caption = ACaption+":";
//   else               Caption = "";//FNode->AV["name"]+":";
   Application->ProcessMessages();
}
//---------------------------------------------------------------------------
void __fastcall TriGroupTmpl::SetOnEnable(TTagNode* ANode)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
/*
  SetEnList->Add(ANode);
  DataChange(this);
*/
}
//---------------------------------------------------------------------------
void __fastcall TriGroupTmpl::DataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
/*
  FDataChange(Sender);
  if (!TmpCtrl) return;
// запись значений при изменении значения контрола кроме choiceDB ----------------------------------
  Variant xVal; xVal.Empty();
*/
}
//---------------------------------------------------------------------------
void __fastcall TriGroupTmpl::FDataChange(TObject *Sender)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
/*
  if (FGetDataChange)
   {
     UnicodeString Src = "";
     FGetDataChange(FNode,Src);
   }
  UnicodeString xRef,xUID,FUID;
  FUID = FNode->AV["uid"].UpperCase();
// отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
     TTagNode *xNode;
     for (int i = 0; i < SetEnList->Count; i++)
      {
        xNode = ((TTagNode*)SetEnList->Items[i]);
        FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
      }
   }
*/
}
//---------------------------------------------------------------------------
TControl* __fastcall TriGroupTmpl::GetControl(int AIdx)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return NULL;
}
//---------------------------------------------------------------------------
TControl* __fastcall TriGroupTmpl::GetFirstEnabledControl()
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return NULL;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TriGroupTmpl::GetValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return "";
}
//---------------------------------------------------------------------------
Variant __fastcall TriGroupTmpl::GetCompValue(UnicodeString ARef)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" group has no value");
}
//---------------------------------------------------------------------------
bool __fastcall TriGroupTmpl::CheckReqValue(TcxPageControl *APC, bool ASilent)
{
  if (!this)throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
  return true;
}
//---------------------------------------------------------------------------

