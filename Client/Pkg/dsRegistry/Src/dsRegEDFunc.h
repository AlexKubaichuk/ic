//---------------------------------------------------------------------------

#ifndef dsRegEDFuncH
#define dsRegEDFuncH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <DBCtrls.hpp>
//#include "RXDBCtrl.hpp"
//#include <Registry.hpp>
#include "XMLContainer.h"

#include "cxGridDBTableView.hpp"
#include "KabCustomDS.h"
//---------------------------------------------------------------------------
//namespace IcsRegED
//{
//---------------------------------------------------------------------------
typedef enum {bcInsert,bcEdit,bcDelete} TbtType;
//---------------------------------------------------------------------------
//#include "RegDefs.h"
typedef bool __fastcall (__closure *TExtBtnClick)(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource, bool isClicked, TObject* ACtrlList, int ABtnIdx);
typedef bool __fastcall (__closure *TExtBtnGetBitmapEvent)(TTagNode *ItTag, int ABtnIdx, TObject *AStm, UnicodeString &AHint);
typedef bool __fastcall (__closure *TTreeBtnClick)(TTagNode *ItTag, bool &AIsGroup, UnicodeString &ACodeBeg, UnicodeString &ACodeEnd, UnicodeString &AExtCode, UnicodeString &AText, bool isClicked);

typedef bool __fastcall (__closure *TGetExtData)(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource);

typedef bool __fastcall (__closure *TGetCompValue)(TTagNode *ItTag, Variant &AVal);
typedef bool __fastcall (__closure *TRegEditButtonClickEvent)(TbtType AType);
typedef UnicodeString __fastcall (__closure *TRegGetOrderEvent)(TTagNode *ADefNode);
typedef UnicodeString __fastcall (__closure *TRegGetFormatEvent)(UnicodeString ADefFormat);
typedef void __fastcall (__closure *TRegListGetColWidthEvent)(TcxGridDBColumn *AColumn, int AMaxWidth);

//---------------------------------------------------------------------------
extern PACKAGE UnicodeString __fastcall FieldCast(TTagNode *ADefNode, UnicodeString AVal);
extern PACKAGE Extended __fastcall ICSStrToFloat(UnicodeString AVal);
extern PACKAGE Extended __fastcall ICSStrToFloatDef(UnicodeString AVal, Extended ADefVal);
extern PACKAGE UnicodeString __fastcall icsCalcMask(bool AIsFolat, int ADigits, int ADecimal, UnicodeString AMin);
#endif
//---------------------------------------------------------------------------

