﻿//---------------------------------------------------------------------------

#ifndef dsRegEDContainerH
#define dsRegEDContainerH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
//#include <Classes.hpp>
//#include <DB.hpp>
//#include <DBCtrls.hpp>
#include "dsRegConstDef.h"
#include "msgdef.h"

#include "dsRegEDBaseDataProvider.h"

//---------------------------------------------------------------------------
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxDropDownEdit.hpp"
#include "cxEdit.hpp"
#include "cxGraphics.hpp"
#include "cxMaskEdit.hpp"
#include "cxTextEdit.hpp"
#include "cxCheckBox.hpp"
#include "cxCalendar.hpp"
//#include "cxDBEdit.hpp"
#include "cxGroupBox.hpp"
//---------------------------------------------------------------------------
#include "dsRegTypeDef.h"
#include "XMLContainer.h"
#include "dsRegEDFunc.h"
//#include "ICSClassData.h"
#include "dsRegTreeClass.h"
#include "dsRegTemplateData.h"
#include "ExtUtils.h"
//---------------------------------------------------------------------------

#define _TMPL_CLEAR_WIDTH_ 25
//---------------------------------------------------------------------------
class PACKAGE EdsRegEDContainerError : public System::Sysutils::Exception
{
public:
     __fastcall EdsRegEDContainerError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE EdsRegEDContainerDSError : public System::Sysutils::Exception
{
public:
     __fastcall EdsRegEDContainerDSError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE ERegEDError : public System::Sysutils::Exception
{
public:
     __fastcall ERegEDError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE ERegEDTemplateError : public System::Sysutils::Exception
{
public:
     __fastcall ERegEDTemplateError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE TdsRegEDContainer;
class PACKAGE TdsRegEDItem;
typedef  map<UnicodeString, TdsRegEDItem*>  TRegEDMap;
typedef bool __fastcall (__closure *TIntGetExtData)(TTagNode *ItTag, UnicodeString &Src);
class PACKAGE TdsRegEDItem : public TPanel //TcxGroupBox
{
private:
  void __fastcall FRegEDItemInit(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode, TdsRegEDContainer *ACtrlOwner);
protected:
  UnicodeString _UID_;
  UnicodeString _ES_;
  TdsRegEDContainer     *FCtrlOwner;
  TIntGetExtData         FGetDataChange;
  TExtBtnGetBitmapEvent  FExtBtnGetBitmap;
  TRegGetFormatEvent     FOnGetFormat;

  TTagNode     *FNode;

  TLabel       *FLab;
  TWinControl  *FED;
  TWinControl  *FED2;

  bool       IsAlign, IsAppend, FRequired, TmpCtrl, isTemplate, isEnabled,isVal, isFV, isEdit, isDelegate;


//    TDataSet*    __fastcall SrcDS();
//    bool         __fastcall SrcDSEditable();

  virtual void     __fastcall GetClassCode(UnicodeString &ACode,__int64 cCODE,TTagNode *ANode);
  virtual void     __fastcall cResize(TObject *Sender);
  bool     __fastcall Condition(TTagNode *CondNode);
  void     __fastcall CalcParam(TTagNode *AFirst,Variant* AParam);
  virtual void     __fastcall FDataChange(TObject *Sender);

//  bool     __fastcall isRefCtrl(TTagNode *ANode);
//  bool     __fastcall isRefMCtrl(UnicodeString ADepend);
  TRegEDMap  FLabList;
  TRegEDMap  FDependList;
  TRegEDMap  FEnList;
//  TList      *SetLabList;
//  TList      *SetValList;
  TList      *SetEnList;
  TColor     FReqColor;
  UnicodeString tmpUID;
  int        tmpInt;
  TdsRegEDItem* __fastcall GetEDCtrl(UnicodeString AId);
  bool          __fastcall EDCtrlExists(UnicodeString AId);
//  TdsRegEDItem* __fastcall RefCtrl(TTagNode* ARef);
//  TdsRegEDItem* __fastcall RefMCtrl(UnicodeString ADepend);
  TdsRegEDItem* __fastcall labCtrl(UnicodeString AId);
  TdsRegEDItem* __fastcall depCtrl(UnicodeString AId);
  TdsRegEDItem* __fastcall FGetExtCtrl(UnicodeString AUID);
  virtual void __fastcall FSetEDRequired(bool AVal);
  void         __fastcall FSetOnDependValues();
public:
    // DB
  __fastcall TdsRegEDItem::TdsRegEDItem(TComponent *AOwner, TWinControl *AParent, TTagNode* ANode, TdsRegEDContainer *ACtrlOwner, bool IsApp);
    // Template
  __fastcall TdsRegEDItem::TdsRegEDItem(TComponent *AOwner, TWinControl *AParent, TTagNode* ANode, TdsRegEDContainer *ACtrlOwner);
  virtual __fastcall ~TdsRegEDItem();

  bool         TwoLine;
  virtual Variant      __fastcall GetCompValue(UnicodeString ARef);
  virtual void         __fastcall SetDependVal(UnicodeString ADepClCode, __int64 ACode, UnicodeString ADepUID);
  virtual void         __fastcall SetOnDependValues();
  virtual void         __fastcall SetOnLab(TdsRegEDItem *AEDItem);
  virtual void         __fastcall SetOnValDepend(TdsRegEDItem *AEDItem);
  virtual void         __fastcall DataChange(TObject *Sender);
  bool                 __fastcall getClassCode(TTagNode *itTag, UnicodeString &UID);
  virtual UnicodeString   __fastcall GetValueStr(bool ABinaryNames = false);
  virtual TStringList* __fastcall GetStrings(UnicodeString AClRef);
  virtual TControl*    __fastcall GetControl(int AIdx = 0);
  virtual TControl*    __fastcall GetFirstEnabledControl();
  virtual void         __fastcall SetEnable(bool AEnable);
  virtual void         __fastcall SetEnableEx(bool AEnable);
  virtual void         __fastcall SetIsVal(bool AEnable);
  virtual void         __fastcall SetValue(UnicodeString AVal,bool AEnable = true);
  virtual void         __fastcall SetLabel(UnicodeString ACaption);
  virtual void         __fastcall SetOnEnable(TTagNode* ANode);
  virtual void         __fastcall SetEDLeft(int ALeft);
  virtual bool         __fastcall UpdateChanges();
  virtual bool         __fastcall CheckReqValue(TcxPageControl *APC = NULL, bool ASilent = false);
  virtual void         __fastcall GetEDLeft(int *ALabR, int *AEDLeft);
  virtual UnicodeString   __fastcall GetValue(UnicodeString ARef);
  virtual UnicodeString   __fastcall GetExtValue(UnicodeString ARef);
  virtual void         __fastcall SetFixedVar(UnicodeString AVals);
  virtual void         __fastcall Clear();

  __property TTagNode*     DefNode        = {read=FNode};

  __property bool Required  = {read=FRequired, write=FSetEDRequired};

  __property TIntGetExtData OnDataChange   = {read=FGetDataChange, write=FGetDataChange};
  __property TExtBtnGetBitmapEvent  OnGetExtBtnGetBitmap  = {read=FExtBtnGetBitmap, write=FExtBtnGetBitmap};

  __property TRegGetFormatEvent  OnGetFormat  = {read=FOnGetFormat, write=FOnGetFormat};

};
//---------------------------------------------------------------------------
class PACKAGE ERegLabError : public System::Sysutils::Exception
{
public:
     __fastcall ERegLabError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE TdsRegLabItem : public TPanel
{
private:
    TdsRegEDContainer *FCtrlOwner;
    UnicodeString _UID_;
    TLabel      *FLab;
    TLabel      *FDataLab;
    TWinControl *FGroupCtrl;
    TTagNode    *FNode;
    Variant      FDataValue;


    bool __fastcall FGetData();
//    TGetExtData FGetExtData;

    UnicodeString tmpUID;
    TList  *SetLabList;
    TList  *SetEnList;
    bool   TwoLine;
    bool   isTmpVal;
    int    tmpInt;
    bool FSaveTransparent;
    TColor FSaveColor;
    TColor FSaveFontColor;

    bool         __fastcall Condition(TTagNode *CondNode);
    Variant      __fastcall GetCompValue(UnicodeString ARef);
    void         __fastcall CalcParam(TTagNode *AFirst,Variant* AParam);
    UnicodeString   __fastcall GetClValue(UnicodeString ARef);
    bool         __fastcall isRefLabCtrl(TTagNode *ANode);
    bool         __fastcall isRefMLabCtrl(TTagNode *ANode);
    TdsRegLabItem* __fastcall FGetExtCtrl(UnicodeString AUID);
    TdsRegLabItem* __fastcall RefLabCtrl(TTagNode* ARef);
    TdsRegLabItem* __fastcall RefMLabCtrl(TTagNode* ARef);
    TdsRegLabItem* __fastcall labLabCtrl(int AIdx);
    void         __fastcall FOnLabResize(TObject *Sender);
  void __fastcall DataLabClick(TObject *Sender);
  void __fastcall DataLabMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
  void __fastcall DataLabMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
public:
    void __fastcall SetOnDependValues();
    void __fastcall DataChange();
    void __fastcall SetEnable(bool AEnable);
    void __fastcall SetLabel(UnicodeString ACaption);
    void __fastcall SetOnLab(TTagNode* ANode);
    void __fastcall SetOnEnable(TTagNode* ANode);
__published:
//    __property TGetExtData OnGetExtData  = {read=FGetExtData, write=FGetExtData};

    __fastcall TdsRegLabItem::TdsRegLabItem (TComponent *AOwner, TWinControl *AParent,
                                             TTagNode* ANode, TdsRegEDContainer *ACtrlOwner,
                                             int AWidth);
    __fastcall ~TdsRegLabItem();
};
//---------------------------------------------------------------------------
typedef  map<UnicodeString, TdsRegLabItem*> TRegLabMap;
class TdsRegTreeClassForm;
//---------------------------------------------------------------------------
class PACKAGE TdsRegEDContainer : public TObject
{
private:
    typedef map <UnicodeString, TdsRegTreeClassForm*> TTreeFormMap;
    typedef map<UnicodeString, Variant> TkabChDBHashMap;
    typedef map<UnicodeString, TStringList*> TkabClassHashMap;
    TcxEditStyleController *FStyleController;
    UnicodeString       FDataPath;

    TComponent  *FOwner;
    TWinControl *FPlaceCtrl;
    TGetExtData   FCtrlDataChange;
    bool FUseHash;

    TdsRegTemplateData  *FTemplateData;

    TStringList *FEditIdList;
    TStringList *FTmplEditIdList;
    TStringList *FLabIdList;

    TRegEDMap   FEditMap;        // список RegED
    TRegEDMap   FTmplEditMap;    // список template RegED
    TRegLabMap  FLabMap;         // список RegLab
    TTreeFormMap *FTreeForm;
    TGetCompValue FGetCompValue;
    TExtBtnGetBitmapEvent  FExtBtnGetBitmap;
    TExtBtnClick  FOnExtBtnClick;
    TkabChDBHashMap  *FChDBHash;
    TkabClassHashMap *FClassHash;

    TTagNode     *FDefXML;
    TRegGetFormatEvent  FOnGetFormat;

    TColor      FReqColor;
    TKeyEvent   FOnKeyDown;

    void __fastcall FSetTemplateData(TdsRegTemplateData  *AData);

    TdsRegEDItem*  __fastcall FGetEditItems(UnicodeString AUID);
    TdsRegEDItem*  __fastcall FGetTemplateItems(UnicodeString AUID);
    TdsRegLabItem* __fastcall FGetLabItems(UnicodeString AUID);
    bool           __fastcall FIntCtrlDataChange(TTagNode *ItTag, UnicodeString &Src);

    int __fastcall FGetEditCount();
    int __fastcall FGetTemplateEditCountCount();
    int __fastcall FGetLabCount();
    TdsRegEDBaseDataProvider *FDataProvider;

    bool FIsTemplateField;
public:
  __fastcall TdsRegEDContainer(TComponent *AOwner,TWinControl *AParent);
  __fastcall ~TdsRegEDContainer();

    TdsRegEDItem*  __fastcall AddEditItem(TTagNode* ANode, bool AIsApp, TWinControl *AParent = NULL);
    TdsRegEDItem*  __fastcall AddTemplateItem(TTagNode* ANode, UnicodeString AVal = "", TWinControl *AParent = NULL);
    TdsRegLabItem* __fastcall AddLabItem(TTagNode* ANode, int AWidth, TWinControl *AParent = NULL);

    bool __fastcall FillClass(UnicodeString ARef, TStringList *AValues, __int64 ACode = -1);
    Variant __fastcall GetChDBData(UnicodeString ARef, UnicodeString RecVal);
    Variant __fastcall GetCtrlData(TTagNode *ADefNode, UnicodeString &ACaption);
    void    __fastcall SetCtrlData(TTagNode *ADefNode, Variant AValue, UnicodeString AStrValue);
    void __fastcall ReiterateClear(TTagNode *ANode, TRegEDMap AMap);
    void __fastcall ClearEditItems();
    void __fastcall ClearTemplateItems();
    void __fastcall ClearLabItems();

    void __fastcall EditDataChange();
    void __fastcall TemplateDataChange();
    void __fastcall LabDataChange();

//    TTagNode*  __fastcall GetNode(UnicodeString ARef);
    TdsRegTreeClassForm* __fastcall GetTreeFormDlg(UnicodeString ARef);
    UnicodeString        __fastcall GetTreeValue(UnicodeString ARef, UnicodeString ACode, UnicodeString &AExtCode);

    TdsRegEDItem*  __fastcall GetEDControl(UnicodeString AUID);
    TdsRegEDItem*  __fastcall GetTemplateControl(UnicodeString AUID);
    TdsRegLabItem* __fastcall GetLabControl(UnicodeString AUID);
    UnicodeString __fastcall GetEDValue(bool ATemplate, UnicodeString AUID, UnicodeString ARef = "");
    void __fastcall SetEDValue(bool ATemplate, UnicodeString AUID, UnicodeString AValue);

    void __fastcall SetOnCtrlDataChange(int AType);
    void __fastcall ClearOnCtrlDataChange(int AType);

    __property TTagNode* DefXML = {read=FDefXML, write=FDefXML};
    __property TExtBtnGetBitmapEvent  OnGetExtBtnGetBitmap  = {read=FExtBtnGetBitmap, write=FExtBtnGetBitmap};
    __property TExtBtnClick OnExtBtnClick = {read=FOnExtBtnClick, write=FOnExtBtnClick};

    __property TGetExtData   OnDataChange   = {read=FCtrlDataChange, write=FCtrlDataChange};
    __property TRegGetFormatEvent  OnGetFormat  = {read=FOnGetFormat, write=FOnGetFormat};

    __property TColor ReqColor = {read=FReqColor, write=FReqColor};

    __property int EditCount = {read=FGetEditCount};
    __property int TemplateEditCount = {read=FGetTemplateEditCountCount};
    __property int LabCount = {read=FGetLabCount};

    __property TdsRegEDItem*  EditItems[UnicodeString AUID]     = {read=FGetEditItems};
    __property TdsRegEDItem*  TemplateItems[UnicodeString AUID] = {read=FGetTemplateItems};
    __property TdsRegLabItem* LabItems[UnicodeString AUID]      = {read=FGetLabItems};

  __property TStringList *EditIdList     = {read=FEditIdList};
  __property TStringList *TmplEditIdList = {read=FTmplEditIdList};
  __property TStringList *LabIdList      = {read=FLabIdList};

    __property TcxEditStyleController *StyleController = {read=FStyleController,write=FStyleController};

    __property bool TemplateField = {read=FIsTemplateField, write=FIsTemplateField};
  __property bool UseHash = {read=FUseHash, write=FUseHash};

  __property TdsRegTemplateData  *TemplateData = {read=FTemplateData, write=FSetTemplateData};
  bool __fastcall IsTemplate(UnicodeString AId);
  bool __fastcall TemplateNoLock();

  __property TKeyEvent OnKeyDown = {read=FOnKeyDown, write=FOnKeyDown};

  TTagNode* __fastcall GetNode(UnicodeString ARef);
  __property TdsRegEDBaseDataProvider *DataProvider = {read=FDataProvider, write=FDataProvider};
  __property UnicodeString       DataPath = {read=FDataPath, write=FDataPath};
};
//---------------------------------------------------------------------------
#endif
