//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsRegEDTemplateDataProvider.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall TdsRegEDTemplateDataProvider::TdsRegEDTemplateDataProvider()
{
}
//---------------------------------------------------------------------------
__fastcall TdsRegEDTemplateDataProvider::~TdsRegEDTemplateDataProvider()
{
}
//---------------------------------------------------------------------------
bool __fastcall FillClassCB(TdsGetValByIdEvent AOnGetValById, UnicodeString ARef, TStringList *AValues, __int64 ACode)
{
  bool RC = false;
  try
   {
     TJSONObject *RetData = NULL;
     if (AOnGetValById)
      {
        int FClCode;
        UnicodeString FClStr;

        if (AOnGetValById("reg."+ARef, ACode, RetData))
         {
           TJSONPairEnumerator *itPair = RetData->GetEnumerator();
           AValues->Clear();
           while (itPair->MoveNext())
            {
              FClCode =((TJSONString*)itPair->Current->JsonString)->Value().ToIntDef(0);
              FClStr  =((TJSONString*)itPair->Current->JsonValue)->Value();
              AValues->AddObject(FClStr, (TObject*)FClCode);
            }
           RC = true;
         }
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegEDTemplateDataProvider::FillClass(UnicodeString ARef, TStringList *AValues, __int64 ACode)
{
  return FillClassCB(FOnGetValById, ARef, AValues, ACode);
/*
  bool RC = false;
  try
   {
     TJSONObject *RetData = NULL;
     if (FOnGetValById)
      {
        int FClCode;
        UnicodeString FClStr;

        if (FOnGetValById("reg."+ARef, ACode, RetData))
         {
           TJSONPairEnumerator *itPair = RetData->GetEnumerator();
           AValues->Clear();
           while (itPair->MoveNext())
            {
              FClCode =((TJSONString*)itPair->Current->JsonString)->Value().ToIntDef(0);
              FClStr  =((TJSONString*)itPair->Current->JsonValue)->Value();
              AValues->AddObject(FClStr, (TObject*)FClCode);
            }
           RC = true;
         }
      }
   }
  __finally
   {
   }
  return RC;
*/
}
//---------------------------------------------------------------------------
Variant __fastcall TdsRegEDTemplateDataProvider::GetChDBData(UnicodeString ARef, UnicodeString RecVal)
{
  Variant RC = Variant::Empty();
  try
   {
     TJSONObject *RetData = NULL;
     if (FOnGetValById)
      {
        if (FOnGetValById("reg.f."+ARef, RecVal, RetData))
          RC =((TJSONString*)RetData->Pairs[0]->JsonValue)->Value();
      }
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
Variant __fastcall TdsRegEDTemplateDataProvider::GetCtrlData(TTagNode *ADefNode, UnicodeString &ACaption)
{
  Variant RC = Variant::Empty();
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDTemplateDataProvider::SetCtrlData(TTagNode *ADefNode, Variant AValue, UnicodeString AStrValue)
{
}
//---------------------------------------------------------------------------

