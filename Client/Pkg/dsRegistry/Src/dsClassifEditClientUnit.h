//---------------------------------------------------------------------------
#ifndef dsClassifEditClientUnitH
#define dsClassifEditClientUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.StdCtrls.hpp>
#include "cxButtons.hpp"
#include "cxGraphics.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
//---------------------------------------------------------------------------
#include "dsRegDMUnit.h"
#include "KabCustomDS.h"
#include "dsRegTemplateData.h"
//---------------------------------------------------------------------------
class PACKAGE TdsClassEditClientForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *BtnPanel;
        TPanel *CompPanel;
        TcxButton *OkBtn;
        TcxButton *CancelBtn;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
        void __fastcall OkBtnClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
private:	// User declarations
    TdsRegDM *FDM;
    TkabCustomDataSource *FDataSrc;
    bool     FIsAppend, FOneCol;
//    TTagNode *Root;
    TTagNode *FClassDef,*FClassDesc;
    int      ColWidth, SubLevel;

    TdsRegEDItem   *LastLeftCtrl;
    TdsRegEDItem   *LastCtrl;
    TcxPageControl *Group1st;
    TcxTabSheet    *gPage;
    UnicodeString  FInsertedRecId;
    TdsRegEDContainer *FCtrList;
    int FLastTop,FLastHeight;

    void __fastcall AlignCtrls(TList *ACtrlList);
    bool __fastcall GetInput(TTagNode *uiTag, UnicodeString &UID);
    bool __fastcall CheckInput();
    bool __fastcall FCtrlDataChange(TTagNode *ItTag, UnicodeString &Src, TkabCustomDataSource *ASource);
    bool __fastcall FSetFocus(TTagNode *itxTag);
    void __fastcall CreateUnitList(TWinControl *AParent, TTagNode* ANode,int *ATop,int *ALeft,TList *GroupComp,int APIndex = 0);
public:
    __fastcall TdsClassEditClientForm(TComponent* Owner, bool AIsAppend, TTagNode* AClassDef, TdsRegDM *ADM, TTagNode* ARoot, TkabCustomDataSource *ADataSrc, TdsRegTemplateData *ATmplData, bool AOneCol = true);
    __property UnicodeString  InsertedRecId = {read=FInsertedRecId};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsClassEditClientForm *dsClassEditClientForm;
//---------------------------------------------------------------------------
#endif
