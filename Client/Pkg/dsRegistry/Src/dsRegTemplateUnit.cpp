//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "dsRegTemplateUnit.h"
//#include "irTreeClass.h"
//#include "Reg_DMF.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtons"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "dxSkinBlue"
#pragma link "dxSkinsCore"
#pragma link "cxClasses"
#pragma resource "*.dfm"
//#define lComp ((TWinControl*)CList->Items[CList->Count-1])
TdsRegTemplateForm *dsRegTemplateForm;
//---------------------------------------------------------------------------
__fastcall TdsRegTemplateForm::TdsRegTemplateForm(TComponent* Owner, TdsRegDM* ADM, UnicodeString ATemplXML, TdsRegTemplateData *ATmplData)
    : TForm(Owner)
{
  FDM = ADM;
//   Caption            = FMT(dsRegITemplateCaption);
//   OkBtn->Caption     = FMT(dsRegITemplateApplyBtn);
//   CancelBtn->Caption = FMT(dsRegITemplateCancelBtn);

//   FRoot = FDM->RegUnit;

  FTempl = new TdsRegTemplate(FDM->RegDef);
  FTempl->TmplPanel = CompPanel;
  FTempl->DataProvider->OnGetValById  = FDM->OnGetValById;
//  FTempl->DataProvider->OnGetCompValue  = FDM->OOnGetValById;
  FTempl->DataProvider->OnGetClassXML = FDM->OnGetXML;
  FTempl->OnExtBtnClick = FDM->OnExtBtnClick;
//  FTempl->OnTreeBtnClick = FOnTreeBtnClick;

  FTempl->StyleController = FDM->StyleController;
  FTempl->DataPath        = FDM->DataPath;
  FTempl->CreateTemplate(ATemplXML, ATmplData);
//  FTempl->OnKeyDown = FormKeyDown;


//   Tmpl->UpperFunc = ARegComp->UpperFunc;
//   FTreeFormList = ARegComp->DM->TreeClasses;
//   Tmpl->OnTreeBtnClick = FOnTreeBtnClick;
//   Tmpl->OnExtBtnClick = ARegComp->OnExtBtnClick;
//   Tmpl->OnGetExtWhere = ARegComp->OnGetExtWhere;
   Height = CompPanel->Height+(Height-ClientHeight)+40/*BtnPanel->Height*//*+BtnBevel->Height*/-5;
   Width = CompPanel->Width;
   CompPanel->Align = alClient;
   ActiveControl = FTempl->GetFirstEnabledControl();
}
//---------------------------------------------------------------------------
void __fastcall TdsRegTemplateForm::OkBtnClick(TObject *Sender)
{
   if (FTempl->CheckInput())
    {
      ModalResult = mrOk;
    }
}
//---------------------------------------------------------------------------
//UnicodeString  __fastcall TdsRegTemplateForm::GetWhere(UnicodeString ATabAlias)
//{
//   return FTempl->GetWhere(ATabAlias);
//}
//---------------------------------------------------------------------------
bool  __fastcall TdsRegTemplateForm::IsEmpty()
{
   return FTempl->IsEmpty();
}
//---------------------------------------------------------------------------
UnicodeString  __fastcall TdsRegTemplateForm::GetSelectedText(bool CanField)
{
   return FTempl->GetSelectedTextValues(CanField);
}
//---------------------------------------------------------------------------
void  __fastcall TdsRegTemplateForm::Clear()
{
   FTempl->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TdsRegTemplateForm::FormDestroy(TObject *Sender)
{
  if (FTempl) delete FTempl;
//  if (FRoot) delete FRoot;
}
//---------------------------------------------------------------------------
/*
bool __fastcall TdsRegTemplateForm::FOnTreeBtnClick(TTagNode *ItTag, bool &AIsGroup, UnicodeString &ACodeBeg, UnicodeString &ACodeEnd, UnicodeString &AExtCode, UnicodeString &AText, bool isClicked)
{
  bool RC = false;
  try
   {
     if (isClicked)
      {
        int clIndex = FTreeFormList->IndexOf(ItTag->AV["uid"]);
        if (clIndex != -1)
         {
           TirTreeClassForm *Dlg = (TirTreeClassForm*)FTreeFormList->Objects[clIndex];
           Dlg->Caption = ItTag->AV["dlgtitle"];
           Dlg->TemplateMode = false;
           Dlg->ShowModal();
           if (Dlg->ModalResult == mrOk)
            {
              RC = true;
              AIsGroup = Dlg->clSelectGroup;
              ACodeBeg = IntToStr(Dlg->clCode);
              ACodeEnd = IntToStr(Dlg->clCodeLast);
              AExtCode = Dlg->clExtCode;
              AText =    Dlg->clText;
            }
         }
        else
         {
           _MSG_ATT(FMT1(icsIErrTreeFormMsg,ItTag->AV["name"]),FMT(icsErrorMsgCaption));
         }
      }
   }
  __finally
   {
   }
  return RC;
}
*/
//---------------------------------------------------------------------------
void __fastcall TdsRegTemplateForm::CancelBtnClick(TObject *Sender)
{
   FTempl->Clear();
}
//---------------------------------------------------------------------------
TdsRegTemplateData* __fastcall TdsRegTemplateForm::FGetTemplateData()
{
  return   FTempl->CtrList->TemplateData;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegTemplateForm::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
  if (Key == VK_RETURN)
   {
     OkBtnClick(this);
   }
}
//---------------------------------------------------------------------------

