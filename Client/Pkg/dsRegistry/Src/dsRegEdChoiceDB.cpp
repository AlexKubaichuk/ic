﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop
#include "dsRegEdChoiceDB.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
// ---------------------------------------------------------------------------
__fastcall TriChoiceDB::TriChoiceDB(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner, bool IsApp) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner, IsApp)
 {
  TTagNode * defNode = FCtrlOwner->GetNode(FNode->AV["ref"]);
  bool FCorrect = false;
  FExtSearchMode = false;
  if (defNode)
   FCorrect = defNode->CmpName("class");
  if (!FCorrect)
   ERegEDError(UnicodeString(__FUNC__) + " -> Error on create object");
  FSValues                          = NULL;
  FSUpValues                        = NULL;
  FSValuesExt                       = NULL;
  FLab                              = new TLabel(Owner);
  FLab->Caption                     = "  " + FNode->AV["name"] + ":";
  FLab->Transparent                 = true;
  FLab->Name                        = "LAB_" + FNode->AV["ref"] + _UID_;
  FLab->Parent                      = this;
  FLab->Top                         = 1;
  FLab->Left                        = 0;
  ED                                = new TcxComboBox(Owner);
  ED->Properties->DropDownListStyle = lsEditFixedList;
  ED->Style->StyleController        = FCtrlOwner->StyleController;
  ED->Name                          = "CB_" + FNode->AV["ref"] + _UID_;
  ED->Text                          = "";
  ED->Top                           = 0;
  ED->Left                          = FLab->Width + 7;
  if (Required)
   ED->Style->Color = FReqColor;
  ED->Parent         = this;
  FLab->FocusControl = ED;
  CMaxH              = ED->Height + 2;
  Height             = ED->Height + 2;
  FFillClass();
  ED->ItemIndex = -1;
  ED->Text      = "";
  if (IsAppend)
   {
    if (FCtrlOwner->IsTemplate(_UID_))
     {
      try
       {
        SetChBDValue(FCtrlOwner->TemplateData->Value[_UID_], FCtrlOwner->TemplateNoLock());
        DataChange(ED);
       }
      __finally
       {
       }
     }
   }
  else
   {
    SetChBDValue(FCtrlOwner->GetCtrlData(FNode, _ES_), isEnabled);
   }
  ED->Properties->OnChange = DataChange;
  // if (cBox(0)->Width > 170) cBox(0)->Width = 170;
  // Width = xW+cBox(0)->Width+21;
  if (Parent->ClassNameIs("TriChoiceDB"))
   {
    Anchors.Clear();
    Anchors << akLeft << akRight << akTop;
   }
  ED->OnKeyDown = FEDKeyDown;
 }
// ---------------------------------------------------------------------------
TcxComboBox * __fastcall TriChoiceDB::FGetCastED()
 {
  return (TcxComboBox *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::FSetCastED(TcxComboBox * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
__int64 __fastcall TriChoiceDB::FGetClassCode()
 {
  if (ED->ItemIndex >= 0)
   return (__int64)ED->Properties->Items->Objects[ED->ItemIndex];
  else
   return -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::SetOnDependValues()
 {
  UnicodeString FDep, FDep2;
  FDep2 = FNode->AV["depend"].UpperCase();
  if (FDep2.Length() >= 9)
   {
    FDep2 += ";";
    while (FDep2.Length())
     {
      FDep = GetPart1(FDep2, ';').Trim();
      if (!FDep.Length())
       FDep = FDep2;
      if (FDep.Length() == 9 && EDCtrlExists(_GUI(FDep)))
       GetEDCtrl(_GUI(FDep))->SetOnValDepend(this);
      FDep2 = GetRPartB(FDep2, ';').Trim();
     }
   }
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
__fastcall TriChoiceDB::~TriChoiceDB()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Properties->OnChange = NULL;
  if (FSValues)
   delete FSValues;
  if (FSUpValues)
   delete FSUpValues;
  if (FSValuesExt)
   delete FSValuesExt;
  delete FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign   = true;
  ED->Left  = ALeft;
  ED->Width = Width - ED->Left - 5;
  // Width = ED->Left+ED->Width+4;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  *AEDLeft = ED->Left;
  *ALabR   = ED->Left - 2;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!IsAlign)
   {
    ED->Width = Width - ED->Left - 5;
    // ED->Left = Width - ED->Width - 2;
    TNotifyEvent FSaveOnChange = ED->Properties->OnChange;
    ED->Properties->OnChange = NULL;
    int FSaveIndex = ED->ItemIndex;
    ED->ItemIndex            = -1;
    ED->ItemIndex            = FSaveIndex;
    ED->Properties->OnChange = FSaveOnChange;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::FFillClass()
 {
  UnicodeString FDep, FDep2;
  UnicodeString FDepVals = "";
  FDep2 = FNode->AV["depend"].UpperCase();
  if (FDep2.Length() >= 9)
   {
    FDep2 += ";";
    while (FDep2.Length())
     {
      FDep = GetPart1(FDep2, ';').Trim();
      if (!FDep.Length())
       FDep = FDep2;
      if (FDep.Length() == 9 && EDCtrlExists(_GUI(FDep)))
       {
        TTagNode * DepNode = FCtrlOwner->GetNode(_GUI(FDep));
        FDepVals += _GUI(FDep) + "=" + GetEDCtrl(_GUI(FDep))->GetValue(_GUI(FDep) + "." + DepNode->AV["ref"]) + ";";
       }
      FDep2 = GetRPartB(FDep2, ';').Trim();
     }
    FCtrlOwner->FillClass("d." + _UID_ + ":" + FDepVals, (TStringList *)ED->Properties->Items, -1);
   }
  else
   FCtrlOwner->FillClass("l." + FNode->AV["ref"], (TStringList *)ED->Properties->Items);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::SetDependVal(UnicodeString ADepClCode, __int64 ACode, UnicodeString ADepUID)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FFillClass();
  if (IsAppend)
   {
    if (ED->Properties->OnChange)
     ED->Properties->OnChange(ED);
    if (ED->Properties->Items->Count > 1)
     ED->Text = "";
   }
  else
   DataChange(ED);
  // }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriChoiceDB::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  DataChange(ED);
  return !FCtrlOwner->IsTemplate(_UID_);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceDB::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString xVal = "";
  if (ED->ItemIndex >= 0)
   xVal += " " + ED->Properties->Items->Strings[ED->ItemIndex];
  return xVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled = AEnable;
  bool tmpClearVal = isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock());
  ED->Enabled   = isEnabled && tmpClearVal;
  FLab->Enabled = isEnabled && tmpClearVal;
  if (!isEnabled && tmpClearVal)
   {
    ED->ItemIndex = -1;
    ED->Properties->OnChange(ED);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  // SetEnable(AEnable);
  try
   {
    SetChBDValue(AVal, AEnable);
    __int64 idx = ED->Properties->Items->IndexOfObject((TObject *)AVal.Trim().ToIntDef(-2));
    if (idx != -1)
     {
      FCtrlOwner->SetCtrlData(FNode, AVal, ED->Properties->Items->Strings[idx]);
     }
    // ED->Properties->OnChange(ED);
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::FEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if ((Key == VK_DELETE) && Shift.Contains(ssCtrl) && ED->Enabled)
   {
    ED->ItemIndex = -1;
    ED->Text      = "";
    // FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), "");
    Key = 0;
   }
  if (Key && FCtrlOwner->OnKeyDown && !ED->DroppedDown)
   FCtrlOwner->OnKeyDown(Sender, Key, Shift);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FLab->Caption = ACaption;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::SetOnLab(TdsRegEDItem * AEDItem)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FLabList[AEDItem->DefNode->AV["uid"]] = AEDItem;
  DataChange(this);
  /*
   if (!this) throw ERegEDError("Error in "+UnicodeString(__FUNC__)+" -> object is NULL");
   SetLabList->Add(ANode);
   if (!IsAppend)
   {
   SetChBDValue(FCtrlOwner->GetCtrlData(FNode, _ES_),isEnabled);
   }
   //  if (FCtrlOwner->IsTemplate(_UID_))
   //   SetChBDValue(FCtrlOwner->_TemplateValue,false); */
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnList->Add(ANode);
  if (FNode->CmpName("choiceDB"))
   {
    if (!IsAppend)
     {
      SetChBDValue(FCtrlOwner->GetCtrlData(FNode, _ES_), isEnabled);
      ED->Properties->OnChange(ED);
     }
    if (FCtrlOwner->IsTemplate(_UID_))
     {
      if (FDependList.size())
       { // есть зависимость по данным и код для choiceDB установлен
        if (ED->ItemIndex >= 0)
         {
          for (TRegEDMap::iterator i = FDependList.begin(); i != FDependList.end(); i++)
           {
            i->second->SetDependVal(FNode->AV["ref"], _CLASSCODE_, _UID_);
           }
         }
       }
      DataChange(ED);
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::FSearchDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString FSText = ED->EditText.Trim().UpperCase();
  ED->Properties->BeginUpdate();
  try
   {
    if (FSText.Length())
     {
      TStringList * FSList = new TStringList;
      try
       {
        FSList->Delimiter     = ' ';
        FSList->DelimitedText = FSText;
        ED->Properties->Items->Clear();
        bool FExist, FExtExist;
        int FIdx;
        for (int i = 0; i < FSUpValues->Count; i++)
         {
          FExist = true;
          for (int j = 0; (j < FSList->Count) && FExist; j++)
           {
            FExtExist = FSUpValues->Strings[i].Pos(FSList->Strings[j]);
            if (!FExtExist && FSValuesExt)
             {
              FIdx = FSValuesExt->IndexOfObject(FSValues->Objects[i]);
              if (FIdx != -1)
               FExtExist = FSValuesExt->Strings[FIdx].Pos(FSList->Strings[j]);
             }
            FExist &= FExtExist;
           }
          if (FExist)
           ED->Properties->Items->AddObject(FSValues->Strings[i], FSValues->Objects[i]);
         }
       }
      __finally
       {
        delete FSList;
       }
     }
    else
     ED->Properties->Items->Assign(FSValues);
   }
  __finally
   {
    ED->Properties->EndUpdate();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::FEditValueChanged(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Properties->BeginUpdate();
  ED->Properties->Items->Assign(FSValues);
  ED->Properties->EndUpdate();
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ED->Enabled)
   {
    if (ED->ItemIndex >= 0)
     FCtrlOwner->SetCtrlData(FNode, _CLASSCODE_, "");
    else if (isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock()))
     FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), "");
   }
  else if (isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock()))
   FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), "");
  // есть зависимость по данным и код для choiceDB установлен ---------------------
  if (FDependList.size())
   { // есть зависимость по данным и код для choiceDB установлен
    if (ED->ItemIndex >= 0)
     {
      for (TRegEDMap::iterator i = FDependList.begin(); i != FDependList.end(); i++)
       {
        i->second->SetDependVal(FNode->AV["ref"], _CLASSCODE_, _UID_);
       }
     }
    else
     {
      for (TRegEDMap::iterator i = FDependList.begin(); i != FDependList.end(); i++)
       {
        i->second->SetDependVal(FNode->AV["ref"], -1, _UID_);
       }
     }
   }
  // присвоение меток ------------------------------------------------------------
  if (FLabList.size()) // Получение текста для Label возможно от choice или choiceDB
   {
    int ItmIndex;
    ItmIndex = ED->ItemIndex;
    if (ItmIndex >= 0)
     {
      for (TRegEDMap::iterator i = FLabList.begin(); i != FLabList.end(); i++)
       {
        if (ED->ItemIndex >= 0)
         i->second->SetLabel(FCtrlOwner->GetChDBData(FNode->AV["ref"] + "." + i->second->DefNode->AV["ref"],
          IntToStr(_CLASSCODE_)));
        else
         i->second->SetLabel("");
       }
     }
   }
  // отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  if (FGetDataChange)
   {
    UnicodeString Src = FCtrlOwner->GetCtrlData(FNode, _ES_);
    FGetDataChange(FNode, Src);
   }
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriChoiceDB::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TControl *)ED;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriChoiceDB::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ED->Enabled && isEdit)
   return (TControl *)ED;
  return NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceDB::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString RC = "";
  try
   {
    if (!ARef.Length() || !_UID(ARef).Length())
     {
      if (ED->ItemIndex >= 0)
       RC = IntToStr(_CLASSCODE_);
     }
    else if (FNode->CmpAV("ref", _UID(ARef)) && FNode->CmpAV("uid", _GUI(ARef)))
     {
      if (ED->ItemIndex >= 0)
       RC = IntToStr(_CLASSCODE_);
     }
    else
     RC = FCtrlOwner->GetChDBData(ARef, IntToStr(_CLASSCODE_));
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriChoiceDB::GetExtValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString RC = "";
  try
   {
    if (ED->ItemIndex >= 0)
     RC = IntToStr(_CLASSCODE_) + "\n";
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
TStringList * __fastcall TriChoiceDB::GetStrings(UnicodeString AClRef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TStringList *)ED->Properties->Items;
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriChoiceDB::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  Variant RC = Variant::Empty();
  try
   {
    if (ARef.Length())
     {
      if (FNode->CmpAV("ref", _GUI(ARef)))
       {
        if (ED->ItemIndex >= 0)
         RC = FCtrlOwner->GetChDBData(ARef, IntToStr(_CLASSCODE_));
       }
     }
    else
     _MSG_INF(FMT1(icsRegEDErrorRefObject, ARef), "");
   }
  __finally
   {
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::SetChBDValue(UnicodeString AVal, bool AEnabled)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  try
   {
    __int64 idx = ED->Properties->Items->IndexOfObject((TObject *)AVal.Trim().ToIntDef(-2));
    if (idx != -1)
     {
      ED->ItemIndex = idx;
      // ED->Properties->OnChange(ED);
      bool tmpEnabled = isEnabled && isEdit && (!IsAppend || FCtrlOwner->TemplateNoLock() /* || cEnabled(i) */);
      if (!tmpEnabled)
       {
        FLab->Enabled = false;
        ED->Enabled   = false;
       }
     }
   }
  __finally
   {
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriChoiceDB::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  bool RC = true;
  if (Required)
   {
    try
     {
      if (ED->Enabled && (ED->ItemIndex < 0))
       {
        if (!ASilent)
         {
          UnicodeString ErrMessage = FNode->AV["name"] + ": ";
          ErrMessage += FCtrlOwner->GetNode(ED->Name.SubString(4, 4).c_str())->AV["name"];
          ErrMessage = FMT1(icsRegItemErrorReqField, ErrMessage);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
        RC = false;
       }
     }
    __finally
     {
      if (!RC)
       {
        if (APC)
         APC->ActivePageIndex = this->Tag;
        if (ED->Visible && Visible)
         ED->SetFocus();
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::FSetExtSearchMode(bool AVal)
 {
  FExtSearchMode = AVal;
  if (FSValues)
   delete FSValues;
  if (FSUpValues)
   delete FSUpValues;
  if (FSValuesExt)
   delete FSValuesExt;
  FSValues    = NULL;
  FSUpValues  = NULL;
  FSValuesExt = NULL;
  if (FExtSearchMode)
   {
    FSValues = new TStringList;
    FSValues->Assign(ED->Properties->Items);
    FSUpValues                         = new TStringList;
    FSUpValues->Text                   = FSValues->Text.UpperCase();
    ED->Properties->OnChange           = FSearchDataChange;
    ED->Properties->OnEditValueChanged = FEditValueChanged;
    ED->Properties->DropDownListStyle  = lsEditList;
   }
  else
   {
    ED->Properties->DropDownListStyle  = lsEditFixedList;
    ED->Properties->OnChange           = DataChange;
    ED->Properties->OnEditValueChanged = NULL;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriChoiceDB::SetExtSearchValues(TStringList * AVal)
 {
  if (FExtSearchMode)
   {
    if (!FSValuesExt)
     FSValuesExt = new TStringList;
    FSValuesExt->Assign(AVal);
   }
 }
// ---------------------------------------------------------------------------
