//---------------------------------------------------------------------------

#ifndef dsRegClientH
#define dsRegClientH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
//---------------------------------------------------------------------------
#include "dxBar.hpp"
#include "XMLContainer.h"
#include "dsRegEDFunc.h"
#include "dsRegExtFilterSetTypes.h"
#include "dsRegTypeDef.h"
//#include "dsRegServerClientClasses.h"
//---------------------------------------------------------------------------
class TdsRegDM;
//---------------------------------------------------------------------------
class PACKAGE TdsRegClient : public TComponent
{
private:
//        TImageList      *FImages;
//        TImageList      *FLargeImages;
//        TImageList      *FSelImages;
//        TImageList      *FSelLargeImages;
//        TImageList      *FHotImages;
//        TImageList      *FDisabledImages;

  TcxImageList *FULImageList;

        TAttr           *xAtt;
        UnicodeString      FFindCaption,FInqCompName,FBuyCaption;
        UnicodeString      FDBUser,FDBPassword,FDBCharset,FUpperFunc,FDTFormat;
        bool            FActive;
        bool            FADO;
//        bool            FRefreshList;
        bool            FCallEvtForInvisebleExtEdit;

        UnicodeString      FCInsertShortCut;
        UnicodeString      FCEditShortCut;
        UnicodeString      FCDeleteShortCut;
        bool            FCInsert;
        bool            FCEdit;
        bool            FCDelete;
        bool            FUseDisableControls;
//        bool            FUseRefresh;
//        bool            FUseReportSetting;

        bool            FUseTemplate;
//        bool            FShowUniqueRec;
        bool            FUseFilter;
//        bool            FUseClassDisControls;
        bool            FUseProgressBar;
        bool            FUseUnitGUID;
        bool            FUseClsGUID;
        bool            FUseAfterScroll;
        bool            FUseSCInfPanel;
        bool            FUseLargeImages;
        bool            FExtShortCutInfPanel;

        bool            FClassEnabled;
        bool            FUnitListEnabled;

        bool            FFindPanel;
        int             FULEditWidth;
        int             FUListTop;
        int             FUListLeft;
        int             FDefaultListWidth;
        int             FDefaultClassWidth;
        TControl        *FInqComp;
        TList           *tmpChTree;
        UnicodeString      FInsCapt;
        UnicodeString      FEditCapt;
        UnicodeString      FDelCapt;
        UnicodeString      FExtCapt;
        UnicodeString      FRegKey;

        UnicodeString      FClKeyName;
        UnicodeString      FDBName;
        UnicodeString      FULCaption;
        UnicodeString      FXMLName;
//        TNotifyEvent    FUnitListCreated;
        TNotifyEvent    FUnitListDeleted;
        TExtLoadXML     FLoadExtXML;

        TExtBtnClick    FGetExtWhere;
        TGetExtData     FGetExtData;
        TClassCallBack  FOnInit;
        TClassCallBack  FDesOnInit;
        TRegistryDemoMessageEvent FDemoMsg;
        TExtEditData    FExtInsertData;
        TExtEditData    FExtEditData;

        TExtEditData    FExtDeleteData;
        TUnitEvent      FOnAfterInsert;
        TClassEditEvent FOnClassAfterInsert;
        TClassEditEvent FOnClassAfterEdit;
        TClassEditEvent FOnClassAfterDelete;
        TExtEditDependFieldEvent FExtEditGetDependField;
        TModifUnitEvent FOnBeforeInsert;
//        TClassEditCreate FClassEditCreate; // ��������� ����������� �.�. 7.10.08
        TUnitEvent      FOnAfterEdit;
        TModifUnitEvent FOnBeforeEdit;
        TUnitEvent      FOnAfterDelete;
        TModifUnitEvent FOnBeforeDelete;
        TInqUpdate      FOnInqUpdate;
        TNotifyEvent    FOnListClose;
        TNotifyEvent    FOnBeforeRefresh;
//        TExtValidateData FOnExtValidate;
//        TExtValidateData FOnGetDefValues;
        TGetCompValue   FGetCompValue;
        TRegEditButtonClickEvent  FOnRegEditButtonClick;
        TExtBtnGetBitmapEvent FExtBtnGetBitmap;

//        TRegistryDataEditEvent FSQLAfterInsertEvt;
//        TRegistryDataEditEvent FSQLBeforeEditEvt;
//        TRegistryDataEditEvent FSQLAfterEditEvt;
//        TRegistryDataEditEvent FSQLAfterDeleteEvt;

        TStartDragEvent  FOnUnitStartDrag;
        TDragOverEvent   FOnUnitDragOver;
        TDragDropEvent   FOnUnitDragDrop;
        TEndDragEvent    FOnUnitEndDrag;

//        TExtFilterClearEvent FOnExtFilterClear;
//        TExtFilterGetTextEvent FOnExtFilterGetText;


//        TUnitEvent       FQuickFilterClick;

        TcxGridGetCellStyleEvent FListContentStyleEvent;
        TRegGetOrderEvent        FOnRegGetOrderEvent;
        TRegListGetColWidthEvent FOnGetListColumnWidth;

        TRegGetFormatEvent  FOnGetFormat;

        TExtEditData __fastcall FGetExtInsertData();
        void __fastcall FSetExtInsertData(TExtEditData AValue);

        TExtEditData __fastcall FGetExtEditData();
        void __fastcall FSetExtEditData(TExtEditData AValue);

        TdsExportData __fastcall FGetOnExportData();
        void __fastcall FSetOnExportData(TdsExportData AValue);

        TdsExportData __fastcall FGetOnInsPriv();
        void __fastcall FSetOnInsPriv(TdsExportData AValue);

        TdsInsEditData __fastcall FGetOnInsPTCData();
        void __fastcall FSetOnInsPTCData(TdsInsEditData AValue);

        TdsInsEditData __fastcall FGetOnEditPTCData();
        void __fastcall FSetOnEditPTCData(TdsInsEditData AValue);

        TdsImportData __fastcall FGetOnImportData();
        void __fastcall FSetOnImportData(TdsImportData AValue);

        TClassEditEvent __fastcall FGetClassAfterInsert();
        void __fastcall FSetClassAfterInsert(TClassEditEvent AValue);

        TClassEditEvent __fastcall FGetClassAfterEdit();
        void __fastcall FSetClassAfterEdit(TClassEditEvent AValue);

        TClassEditEvent __fastcall FGetClassAfterDelete();
        void __fastcall FSetClassAfterDelete(TClassEditEvent AValue);

        TUnitEvent __fastcall FGetOnOpenCard();
        void __fastcall FSetOnOpenCard(TUnitEvent AValue);

        TUnitEvent __fastcall FGetOnPrintF63();
        void __fastcall FSetOnPrintF63(TUnitEvent AValue);

        TUnitEvent __fastcall FGetOnShowPrePlan();
        void __fastcall FSetOnShowPrePlan(TUnitEvent AValue);

        TGetReportListEvent __fastcall FGetOnGetQuickFilterList();
        void __fastcall FSetOnGetQuickFilterList(TGetReportListEvent AValue);

        bool __fastcall FGetUseQuickFilterSetting();
        void __fastcall FSetUseQuickFilterSetting(bool AValue);

        TGetReportListEvent __fastcall FGetOnGetReportList();
        void __fastcall FSetOnGetReportList(TGetReportListEvent AValue);

        TdsRegReportClickEvent __fastcall FGetPrintReportClick();
        void __fastcall FSetPrintReportClick(TdsRegReportClickEvent AValue);

        bool __fastcall FGetUseReportSetting();
        void __fastcall FSetUseReportSetting(bool AValue);

        TExtBtnClick __fastcall FGetExtBtnClick();
        void __fastcall FSetExtBtnClick(TExtBtnClick AValue);


        TcxEditStyleController* __fastcall FGetStyleController();
        void __fastcall FSetStyleController(TcxEditStyleController*AValue);

        TTagNode* __fastcall FGetRegDef();
        void __fastcall FSetRegDef(TTagNode* AValue);

        TdsGetClassXMLEvent __fastcall FGetOnGetClassXML();
        void __fastcall FSetOnGetClassXML(TdsGetClassXMLEvent AValue);

        TdsGetCountEvent __fastcall FGetOnGetCount();
        void __fastcall FSetOnGetCount(TdsGetCountEvent AValue);

        TdsGetIdListEvent __fastcall FGetOnGetIdList();
        void __fastcall FSetOnGetIdList(TdsGetIdListEvent AValue);

        TdsGetValByIdEvent __fastcall FGetOnGetValById();
        void __fastcall FSetOnGetValById(TdsGetValByIdEvent AValue);

        TdsGetValById10Event __fastcall FGetOnGetValById10();
        void __fastcall FSetOnGetValById10(TdsGetValById10Event AValue);

        TdsFindEvent __fastcall FGetOnFind();
        void __fastcall FSetOnFind(TdsFindEvent AValue);

        TdsInsertDataEvent __fastcall FGetOnInsertData();
        void __fastcall FSetOnInsertData(TdsInsertDataEvent AValue);

        TdsEditDataEvent __fastcall FGetOnEditData();
        void __fastcall FSetOnEditData(TdsEditDataEvent AValue);

        TdsDeleteDataEvent __fastcall FGetOnDeleteData();
        void __fastcall FSetOnDeleteData(TdsDeleteDataEvent AValue);

        TExtCountEvent __fastcall FGetExtCount();
        void __fastcall FSetExtCount(TExtCountEvent AValue);

        TdsRegFetch __fastcall FGetFetchAll();
        void __fastcall FSetFetchAll(TdsRegFetch AValue);

        TdsRegFetch __fastcall FGetClassFetchAll();
        void __fastcall FSetClassFetchAll(TdsRegFetch AValue);

        int __fastcall FGetFetchRecCount();
        void __fastcall FSetFetchRecCount(int AValue);

        int __fastcall FGetClassFetchRecCount();
        void __fastcall FSetClassFetchRecCount(int AValue);

        TdsRegExtFilterSetEvent __fastcall FGetOnExtFilterSet();
        void __fastcall FSetOnExtFilterSet(TdsRegExtFilterSetEvent AValue);

        TExtValidateData __fastcall FGetOnGetDefValues();
        void __fastcall FSetOnGetDefValues(TExtValidateData AVal);

        TExtValidateData __fastcall FGetOnExtValidate();
        void __fastcall FSetOnExtValidate(TExtValidateData AVal);

        TdsRegListType __fastcall FGetUnitListOpenType();
        void __fastcall FSetUnitListOpenType(TdsRegListType AVal);

        TdsRegListType __fastcall FGetClassListOpenType();
        void __fastcall FSetClassListOpenType(TdsRegListType AVal);

        UnicodeString __fastcall FGetXMLPath();
        void __fastcall FSetXMLPath(UnicodeString AVal);

        UnicodeString __fastcall FGetDataPath();
        void __fastcall FSetDataPath(UnicodeString AVal);

        TExtBtnClick __fastcall FGetOnCtrlDataChange();
        void __fastcall FSetOnCtrlDataChange(TExtBtnClick AVal);

        bool __fastcall FGetSingleEditCol();
        void __fastcall FSetSingleEditCol(bool AVal);

        bool __fastcall FGetClsSingleEditCol();
        void __fastcall FSetClsSingleEditCol(bool AVal);

        TColor __fastcall FGetReqColor();
        void __fastcall FSetReqColor(TColor AVal);

        void __fastcall FSetActive(bool AActive);

        void __fastcall FSetClassEnabled(bool AClassEnabled);
        void __fastcall FSetUnitListEnabled(bool AUnitListEnabled);

        bool __fastcall FGetOpenCardEnabled();
        void __fastcall FSetOpenCardEnabled(bool AOpenCardEnabled);

        void __fastcall FSetFullEdit(bool AFullEdit);
        bool __fastcall FGetFullEdit();

        void __fastcall FSetUListTop(int ATop);
        void __fastcall FSetUListLeft(int ALeft);
        void __fastcall FSetDBName(UnicodeString  ADBName);
        void __fastcall FSetXMLName(UnicodeString  AXMLName);
        void __fastcall FSetInqComp(TControl *AInqComp);
        void __fastcall FSetInit(int APersent, UnicodeString AMess);
        void __fastcall FSetUpperFunc(UnicodeString  AParam);
        void __fastcall FSetDTFormat(UnicodeString  AParam);
  TActionList *__fastcall FGetUnitListActionList();

  TAxeXMLContainer* __fastcall FGetXMLList();
  void __fastcall FSetXMLList(TAxeXMLContainer *AVal);

protected:
        TObject   *PrF;
//        bool __fastcall Connect(UnicodeString ADBName, UnicodeString AXMLName);
        bool __fastcall GetTreeCh(TTagNode *itTag);
        void __fastcall GetTreeChoice(TList *AList);
        TdsRegDM     *FDM;


        bool           __fastcall FGetClassEditable();
        void           __fastcall FSetClassEditable(bool AVal);

        bool           __fastcall FGetUnitListEditable();
        void           __fastcall FSetUnitListEditable(bool AVal);

        UnicodeString  __fastcall FGetClassShowExpanded();
        void           __fastcall FSetClassShowExpanded(UnicodeString AVal);

public:

        __fastcall TdsRegClient(TComponent* Owner);
        __fastcall ~TdsRegClient();

        TAnsiStrMap  HelpContextList;
        void       __fastcall ShowSelUnitList(UnicodeString ATmpl,TStringList *ACodeList);
        void       __fastcall ShowClasses(TdsRegClassType AShowType = ctList, UnicodeString AGroups = "", UnicodeString AClassId = "");
        void       __fastcall ShowClasses(UnicodeString AClassID, TdsRegClassType AShowType = ctList, UnicodeString AGroups = "");
        void       __fastcall ShowUnitList  ( bool isModal =false, bool AExtended = false);
  TForm* __fastcall CreateUnitList();

        void       __fastcall GetSelectedUnits(TStrings *ADest);
        void       __fastcall RefreshUnitList();
        bool       __fastcall IsListShowing();

//        bool       __fastcall GetSelected(__int64 *ACode, UnicodeString &AUnitStr);
//        TdxBar*    __fastcall GetListExtTB(void);
        void       __fastcall GetSelectedUnitData(TkabCustomDataSetRow *& AData);
        bool       __fastcall GetUnitData(__int64 ACode, TkabCustomDataSetRow *& AData);

__published:
//        __property TRegistryDataEditEvent SQLAfterInsert  = {read=FSQLAfterInsertEvt, write=FSQLAfterInsertEvt};
//        __property TRegistryDataEditEvent SQLBeforeEdit   = {read=FSQLBeforeEditEvt,  write=FSQLBeforeEditEvt};
//        __property TRegistryDataEditEvent SQLAfterEdit    = {read=FSQLAfterEditEvt,   write=FSQLAfterEditEvt};
//        __property TRegistryDataEditEvent SQLAfterDelete  = {read=FSQLAfterDeleteEvt, write=FSQLAfterDeleteEvt};

//        __property TdsICClassClient   *Connection = {read=FGetConnection, write=FSetConnection};
        __property TRegGetFormatEvent  OnGetFormat  = {read=FOnGetFormat, write=FOnGetFormat};

        __property TcxEditStyleController *StyleController = {read=FGetStyleController,write=FSetStyleController};

        __property TdsRegReportClickEvent OnPrintReportClick    = {read=FGetPrintReportClick, write=FSetPrintReportClick};
        __property TGetReportListEvent OnGetReportList    = {read=FGetOnGetReportList, write=FSetOnGetReportList};
        __property bool UseReportSetting  = {read=FGetUseReportSetting, write=FSetUseReportSetting};

//        __property TUnitEvent OnQuickFilterClick    = {read=FQuickFilterClick, write=FQuickFilterClick};
        __property TGetReportListEvent OnGetQuickFilterList    = {read=FGetOnGetQuickFilterList, write=FSetOnGetQuickFilterList};
        __property bool UseQuickFilterSetting  = {read=FGetUseQuickFilterSetting, write=FSetUseQuickFilterSetting};

        __property bool UseDisableControls  = {read=FUseDisableControls, write=FUseDisableControls};
        __property bool UseUnitGUID  = {read=FUseUnitGUID, write=FUseUnitGUID};
        __property bool UseClsGUID  = {read=FUseClsGUID, write=FUseClsGUID};

        __property TTagNode *RegDef = {read=FGetRegDef, write=FSetRegDef};

        __property bool CallEvtForInvisebleExtEdit  = {read=FCallEvtForInvisebleExtEdit, write=FCallEvtForInvisebleExtEdit};

        __property TRegEditButtonClickEvent  OnRegEditButtonClick  = {read=FOnRegEditButtonClick, write=FOnRegEditButtonClick};
        __property TExtBtnGetBitmapEvent  OnGetExtBtnGetBitmap  = {read=FExtBtnGetBitmap, write=FExtBtnGetBitmap};

        __property int  DefaultListWidth  = {read=FDefaultListWidth, write=FDefaultListWidth};
        __property int  DefaultClassWidth  = {read=FDefaultClassWidth, write=FDefaultClassWidth};

        __property TGetExtData OnGetExtData    = {read=FGetExtData, write=FGetExtData};
        __property TExtBtnClick OnExtBtnClick  = {read=FGetExtBtnClick, write=FSetExtBtnClick};

        __property TRegistryDemoMessageEvent OnDemoMessage  = {read=FDemoMsg, write=FDemoMsg};
        __property TExtBtnClick OnGetExtWhere = {read=FGetExtWhere, write=FGetExtWhere};
        __property TGetCompValue OnGetCompValue = {read=FGetCompValue, write=FGetCompValue};

        __property TExtBtnClick OnCtrlDataChange    = {read=FGetOnCtrlDataChange, write=FSetOnCtrlDataChange};

        __property TExtEditData OnExtInsertData   = {read=FGetExtInsertData, write=FSetExtInsertData};
        __property TExtEditData OnExtEditData     = {read=FGetExtEditData, write=FSetExtEditData};


        __property TdsExportData OnInsPriv  = {read=FGetOnInsPriv, write=FSetOnInsPriv};
        __property TdsInsEditData OnInsPTCData         = {read=FGetOnInsPTCData, write=FSetOnInsPTCData};
        __property TdsInsEditData OnEditPTCData        = {read=FGetOnEditPTCData, write=FSetOnEditPTCData};

        __property TdsExportData OnExportData  = {read=FGetOnExportData, write=FSetOnExportData};
        __property TdsImportData OnImportData  = {read=FGetOnImportData, write=FSetOnImportData};

        __property TExtEditData OnExtDeleteData   = {read=FExtDeleteData, write=FExtDeleteData};

        __property bool SingleEditCol    = {read=FGetSingleEditCol, write=FSetSingleEditCol};
        __property bool ClsSingleEditCol = {read=FGetClsSingleEditCol, write=FSetClsSingleEditCol};

  __property TdsRegExtFilterSetEvent OnExtFilterSet   = {read=FGetOnExtFilterSet, write=FSetOnExtFilterSet};
//        __property TExtFilterClearEvent OnExtFilterClear = {read=FOnExtFilterClear, write=FOnExtFilterClear};
//        __property TExtFilterGetTextEvent OnExtFilterGetText = {read=FOnExtFilterGetText, write=FOnExtFilterGetText};

        __property TStartDragEvent  OnUnitStartDrag = {read=FOnUnitStartDrag, write=FOnUnitStartDrag};
        __property TDragOverEvent   OnUnitDragOver = {read=FOnUnitDragOver, write=FOnUnitDragOver};
        __property TDragDropEvent   OnUnitDragDrop = {read=FOnUnitDragDrop, write=FOnUnitDragDrop};
        __property TEndDragEvent    OnUnitEndDrag = {read=FOnUnitEndDrag, write=FOnUnitEndDrag};

        // ��������� 7.10.08 ��������� �.�.
        // ������� ������������ �� �������� ����� �������������� ��������������
        // � �������� ������ ����������� ����� -->
//        __property TClassEditCreate OnAfterClassEditCreate = {read=FClassEditCreate, write=FClassEditCreate};
        // <--

        __property TExtLoadXML OnLoadXML   = {read=FLoadExtXML, write=FLoadExtXML};


        __property bool UseTemplate        = {read=FUseTemplate,write=FUseTemplate};
        __property bool UseFilter        = {read=FUseFilter,write=FUseFilter};
        __property bool UseProgressBar  = {read=FUseProgressBar,write=FUseProgressBar};

        __property bool FindPanel        = {read=FFindPanel,write=FFindPanel};
        __property bool ShortCutInfPanel = {read=FUseSCInfPanel,write=FUseSCInfPanel};
        __property bool ExtShortCutInfPanel = {read=FExtShortCutInfPanel,write=FExtShortCutInfPanel};

        __property bool InsertBtnEnabled   = {read=FCInsert,write=FCInsert};
        __property bool EditBtnEnabled     = {read=FCEdit,write=FCEdit};
        __property bool DeleteBtnEnabled   = {read=FCDelete,write=FCDelete};

        __property UnicodeString InsertShortCut  = {read=FCInsertShortCut,write=FCInsertShortCut};
        __property UnicodeString EditShortCut    = {read=FCEditShortCut,write=FCEditShortCut};
        __property UnicodeString DeleteShortCut  = {read=FCDeleteShortCut,write=FCDeleteShortCut};

        __property bool FullEdit        = {read=FGetFullEdit,write=FSetFullEdit};
//        __property bool RefreshList     = {read=FRefreshList,write=FRefreshList};

//        __property bool UseClassDisControls   = {read=FUseClassDisControls,write=FUseClassDisControls};
//        __property bool ShowUniqueRec         = {read=FShowUniqueRec,write=FShowUniqueRec};

  __property bool ClassEnabled    = {read=FClassEnabled,write=FSetClassEnabled};
  __property bool ClassEditable   = {read=FGetClassEditable,write=FSetClassEditable};

  __property bool UnitListEnabled    = {read=FUnitListEnabled,write=FSetUnitListEnabled};
  __property bool UnitListEditable   = {read=FGetUnitListEditable,write=FSetUnitListEditable};

  __property bool OpenCardEnabled = {read=FGetOpenCardEnabled,write=FSetOpenCardEnabled};

        __property bool ADOSource       = {read=FADO,write=FADO};
        __property bool Active          = {read=FActive,write=FSetActive};

        __property TdsRegFetch FetchAll      = {read=FGetFetchAll,write=FSetFetchAll};
        __property TdsRegFetch ClassFetchAll = {read=FGetClassFetchAll,write=FSetClassFetchAll};

        __property int FetchRecCount         = {read=FGetFetchRecCount,write=FSetFetchRecCount};
        __property int ClassFetchRecCount    = {read=FGetClassFetchRecCount,write=FSetClassFetchAll};

        __property bool UseAfterScroll  = {read=FUseAfterScroll,write=FUseAfterScroll};
//        __property bool UseRefresh  = {read=FUseRefresh,write=FUseRefresh};
        __property bool UseLargeImages  = {read=FUseLargeImages,write=FUseLargeImages};

        __property TcxGridGetCellStyleEvent OnGetListContentStyle = {read=FListContentStyleEvent,write=FListContentStyleEvent};
        __property TRegGetOrderEvent        OnGetOrder = {read=FOnRegGetOrderEvent, write=FOnRegGetOrderEvent};
        __property TRegListGetColWidthEvent OnGetListColumnWidth = {read=FOnGetListColumnWidth, write=FOnGetListColumnWidth};

        __property int ULEditWidth      = {read=FULEditWidth,write=FULEditWidth};
        __property int UnitListTop      = {read=FUListTop,write=FSetUListTop, default=0};
        __property int UnitListLeft     = {read=FUListLeft,write=FSetUListLeft, default=0};

        __property TClassEditEvent OnClassAfterInsert = {read=FGetClassAfterInsert,write=FSetClassAfterInsert};
        __property TClassEditEvent OnClassAfterEdit   = {read=FGetClassAfterEdit,write=FSetClassAfterEdit};
        __property TClassEditEvent OnClassAfterDelete = {read=FGetClassAfterDelete,write=FSetClassAfterDelete};

        __property UnicodeString FindCaption    = {read=FFindCaption,write=FFindCaption};

        __property UnicodeString DBName    = {read=FDBName,write=FSetDBName};
        __property UnicodeString PrefixCaption = {read=FULCaption,write=FULCaption};
        __property UnicodeString XML_AV1_Name  = {read=FXMLName,write=FSetXMLName};
        __property TClassCallBack OnInit    = {read=FOnInit,write=FOnInit};

        __property UnicodeString RegistryKey = {read=FRegKey,write=FRegKey};
        __property UnicodeString ClassShowExpanded = {read=FGetClassShowExpanded,write=FSetClassShowExpanded};
        __property UnicodeString InsertBtnCaption = {read=FInsCapt,write=FInsCapt};
        __property UnicodeString EditBtnCaption   = {read=FEditCapt,write=FEditCapt};
        __property UnicodeString DeleteBtnCaption = {read=FDelCapt,write=FDelCapt};
        __property UnicodeString ExtBtnCaption    = {read=FExtCapt,write=FExtCapt};
        __property UnicodeString UpperFunc        = {read=FUpperFunc,write=FSetUpperFunc};
        __property UnicodeString DTFormat         = {read=FDTFormat,write=FSetDTFormat};

        __property TModifUnitEvent OnBeforeInsert = {read=FOnBeforeInsert,write=FOnBeforeInsert};
        __property TModifUnitEvent OnBeforeEdit   = {read=FOnBeforeEdit,write=FOnBeforeEdit};
        __property TModifUnitEvent OnBeforeDelete = {read=FOnBeforeDelete,write=FOnBeforeDelete};

        __property TColor RequiredColor  = {read=FGetReqColor,write=FSetReqColor};

        __property TExtEditDependFieldEvent OnExtEditGetDependField = {read=FExtEditGetDependField,write=FExtEditGetDependField};

        __property TExtValidateData OnExtValidate = {read=FGetOnExtValidate,write=FSetOnExtValidate};
        __property TExtValidateData OnGetDefValues = {read=FGetOnGetDefValues,write=FSetOnGetDefValues};

        __property TUnitEvent OnAfterInsert = {read=FOnAfterInsert,write=FOnAfterInsert};
        __property TUnitEvent OnAfterEdit   = {read=FOnAfterEdit,write=FOnAfterEdit};
        __property TUnitEvent OnAfterDelete = {read=FOnAfterDelete,write=FOnAfterDelete};

        __property TUnitEvent OnOpenCard        = {read=FGetOnOpenCard,write=FSetOnOpenCard};
        __property TInqUpdate OnGetInqData      = {read=FOnInqUpdate,write=FOnInqUpdate};
        __property TNotifyEvent OnUnitListClose = {read=FOnListClose,write=FOnListClose};
        __property TNotifyEvent OnBeforeRefresh = {read=FOnBeforeRefresh,write=FOnBeforeRefresh};

  __property TUnitEvent OnPrintF63 = {read=FGetOnPrintF63, write=FSetOnPrintF63};
  __property TUnitEvent OnShowPrePlan = {read=FGetOnShowPrePlan, write=FSetOnShowPrePlan};

//        __property TNotifyEvent AfterUnitListCreate = {read=FUnitListCreated,write=FUnitListCreated};
        __property TNotifyEvent BeforeUnitListDeleted = {read=FUnitListDeleted,write=FUnitListDeleted};

        __property TControl* InqComponent       = {read=FInqComp,write=FSetInqComp};
        __property UnicodeString InqComponentName  = {read=FInqCompName,write=FInqCompName};

        __property UnicodeString BuyCaption  = {read=FBuyCaption,write=FBuyCaption};

        __property UnicodeString DBUser  = {read=FDBUser,write=FDBUser};
        __property UnicodeString DBPassword  = {read=FDBPassword,write=FDBPassword};
        __property UnicodeString DBCharset  = {read=FDBCharset,write=FDBCharset};

//        __property TImageList* Images  = {read=FImages,write=FImages};
//        __property TImageList* LargeImages  = {read=FLargeImages,write=FLargeImages};
//        __property TImageList* SelImages  = {read=FSelImages,write=FSelImages};
//        __property TImageList* SelLargeImages  = {read=FSelLargeImages,write=FSelLargeImages};

//        __property TImageList* HotImages  = {read=FHotImages,write=FHotImages};
//        __property TImageList* DisabledImages  = {read=FDisabledImages,write=FDisabledImages};

        __property TdsGetClassXMLEvent OnGetClassXML  = {read=FGetOnGetClassXML,write=FSetOnGetClassXML};
        __property TdsGetCountEvent OnGetCount  = {read=FGetOnGetCount,write=FSetOnGetCount};
        __property TdsGetIdListEvent OnGetIdList  = {read=FGetOnGetIdList,write=FSetOnGetIdList};
        __property TdsGetValByIdEvent OnGetValById  = {read=FGetOnGetValById,write=FSetOnGetValById};
        __property TdsGetValById10Event OnGetValById10  = {read=FGetOnGetValById10,write=FSetOnGetValById10};
        __property TdsFindEvent OnFind  = {read=FGetOnFind,write=FSetOnFind};
        __property TdsInsertDataEvent OnInsertData  = {read=FGetOnInsertData,write=FSetOnInsertData};
        __property TdsEditDataEvent OnEditData  = {read=FGetOnEditData,write=FSetOnEditData};
        __property TdsDeleteDataEvent OnDeleteData  = {read=FGetOnDeleteData,write=FSetOnDeleteData};
  __property TExtCountEvent OnExtCount = {read=FGetExtCount, write=FSetExtCount};

  __property TcxImageList *UnitListImageList = {read=FULImageList, write=FULImageList};
  __property TActionList *UnitListActionList = {read=FGetUnitListActionList};

  __property TAxeXMLContainer *XMLList = {read=FGetXMLList, write=FSetXMLList};
  __property TdsRegListType      UnitListOpenType = {read=FGetUnitListOpenType, write=FSetUnitListOpenType};
  __property TdsRegListType      ClassListOpenType = {read=FGetClassListOpenType, write=FSetClassListOpenType};
  __property UnicodeString       XMLPath = {read=FGetXMLPath, write=FSetXMLPath};
  __property UnicodeString       DataPath = {read=FGetDataPath, write=FSetDataPath};
};
//---------------------------------------------------------------------------
#endif

