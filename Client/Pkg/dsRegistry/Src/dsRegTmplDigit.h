﻿//---------------------------------------------------------------------------

#ifndef dsRegTmplDigitH
#define dsRegTmplDigitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>

//#include <DBCtrls.hpp>
//#include "RXDBCtrl.hpp"
//#include <Registry.hpp>
//#define F_DATE 1
//#define F_DATETIME 2
//#define F_TIME 3

//---------------------------------------------------------------------------
#include "dsRegEDContainer.h"
//---------------------------------------------------------------------------
class PACKAGE TriDigitTmpl: public TdsRegEDItem
{
private:
  TcxButton  *FClearBtn;
  TcxMaskEdit* FED2;
  TLabel* FFrLab;
  TLabel* FToLab;
  UnicodeString __fastcall FGetValue();
  void __fastcall FSetEDValue(UnicodeString AVal);
  bool __fastcall CheckMinMax(UnicodeString AValue);
    void     __fastcall cResize(TObject *Sender);
    void     __fastcall FDataChange(TObject *Sender);
    void     __fastcall FEditValidate(TObject *Sender, Variant &DisplayValue, TCaption &ErrorText, bool &Error);
    bool    FIsFloat;
    TcxMaskEdit* __fastcall FGetCastED();
    void __fastcall FSetCastED(TcxMaskEdit *AVal);
    __property TcxMaskEdit *ED = {read=FGetCastED,write=FSetCastED};
  void __fastcall FClearClick(TObject *ASender);
  void __fastcall FEDKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
public:
    __fastcall TriDigitTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode, TdsRegEDContainer *ACtrlOwner);
    __fastcall ~TriDigitTmpl();
    Variant  __fastcall GetCompValue(UnicodeString ARef);
    void         __fastcall SetOnDependValues();
    void         __fastcall DataChange(TObject *Sender);
    UnicodeString   __fastcall GetValueStr(bool ABinaryNames = false);
    TControl*    __fastcall GetControl(int AIdx);
    TControl*    __fastcall GetFirstEnabledControl();
    void         __fastcall SetEnable(bool AEnable);
    void         __fastcall SetEnableEx(bool AEnable);
    void         __fastcall SetIsVal(bool AEnable);
    void         __fastcall SetValue(UnicodeString AVal,bool AEnable = true);
    void         __fastcall SetLabel(UnicodeString ACaption);
    void         __fastcall SetOnEnable(TTagNode* ANode);
    void         __fastcall SetEDLeft(int ALeft);
    bool         __fastcall UpdateChanges();
    bool         __fastcall CheckReqValue(TcxPageControl *APC = NULL, bool ASilent = false);
    void         __fastcall GetEDLeft(int *ALabR, int *AEDLeft);
    UnicodeString   __fastcall GetValue(UnicodeString ARef);
  void __fastcall Clear();
};
//---------------------------------------------------------------------------
#endif
