﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop
#include "dsRegTmplDate.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// #define ED      ((TcxDBDateEdit*)ED)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
// #pragma resource     "calendar.RES"
// ---------------------------------------------------------------------------
__fastcall TriDateTmpl::TriDateTmpl(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner)
 {
  FClearBtn           = new TcxButton(this);
  FClearBtn->Parent   = this;
  FClearBtn->OnClick  = FClearClick;
  FClearBtn->ShowHint = true;
  FClearBtn->Hint     = "Очистить поле \"" + FNode->AV["name"] + "\"";
  FClearBtn->Width    = 20;
  FClearBtn->Height   = 20;
  FClearBtn->OptionsImage->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\regclear.bmp"));
  FClearBtn->OptionsImage->NumGlyphs          = 4;
  FClearBtn->PaintStyle                       = bpsGlyph;
  FClearBtn->SpeedButtonOptions->CanBeFocused = false;
  FClearBtn->SpeedButtonOptions->Flat         = true;
  FClearBtn->TabStop                          = false;
  FClearBtn->Anchors.Clear();
  FClearBtn->Anchors << akRight;
  TRect xSize = Rect(0, 0, 120, 21); // default позиция и размер для "ED"
  FLab                       = new TLabel(this);
  FLab->Parent               = this;
  FLab->Transparent          = true;
  FLab->Top                  = 2;
  FLab->Left                 = 4;
  FLab->Caption              = FNode->AV["name"] + ":";
  ED                         = new TcxDateEdit(this);
  ED->Parent                 = this;
  ED->Properties->OnChange   = FDataChange;
  ED->Style->StyleController = FCtrlOwner->StyleController;
  ED->Properties->ButtonGlyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\regcalendar.bmp"));
  // LoadFromResourceName(reinterpret_cast<int>(HInstance), "CALENDARBTN");
  xSize                     = Rect(0, 0, 85, 21);
  ED->Constraints->MaxWidth = 95;
  ED->Constraints->MinWidth = 95;
  // ED->DataBinding->DataField = FFlName; ED->DataBinding->DataSource = FSrc;
  // ED->YearDigits = dyFour;
  if (Required)
   ED->Style->Color = FReqColor;
  if (!FCtrlOwner->TemplateField && FCtrlOwner->IsTemplate(_UID_))
   {
    ED->Date = FCtrlOwner->TemplateData->Value[_UID_];
   }
  ED->Enabled = isEnabled && isEdit && (!isTemplate || FCtrlOwner->TemplateNoLock());
  ED->Parent = this;
  ED->Width  = 95;
  // if (ED->Width < 60) ED->Width = 60;
  ED->Top    = xSize.Top;
  ED->Height = xSize.Bottom;
  if (FLab->Width > (AParent->Width - 2))
   {
    FLab->Font->Size = FLab->Font->Size - 1;
    FLab->Update();
    if (FLab->Width > (AParent->Width - 2))
     FLab->Width = AParent->Width - 2;
   }
  ED->Left = FLab->Left + FLab->Width + 5;
  if ((ED->Left + ED->Width) > (AParent->Width - 2))
   {
    TwoLine   = true;
    FLab->Top = 0;
    ED->Left  = FLab->Left + 5;
    ED->Top   = FLab->Height;
    CMaxH     = 34;
    CMinH     = 34;
    Height    = CMaxH;
   }
  FLab->FocusControl = ED;
  FClearBtn->Top = ED->Top + ED->Height / 2 - 10;
  ED->OnKeyDown  = FEDKeyDown;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::Clear()
 {
  FClearClick(FClearBtn);
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::FClearClick(TObject * ASender)
 {
  ED->Date                               = Cxdateutils::NullDate;
  FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
  DataChange(ED);
 }
// ---------------------------------------------------------------------------
TcxDateEdit * __fastcall TriDateTmpl::FGetCastED()
 {
  return (TcxDateEdit *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::FSetCastED(TcxDateEdit * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetOnDependValues()
 {
  if (FNode->AV["depend"].Length() == 9 && EDCtrlExists(_GUI(FNode->AV["depend"])))
   GetEDCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(this);
  else if (FNode->AV["ref"].Length() == 4 && EDCtrlExists(FNode->AV["ref"]))
   GetEDCtrl(FNode->AV["ref"])->SetOnLab(this);
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
__fastcall TriDateTmpl::~TriDateTmpl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Properties->OnChange = NULL;
  delete ED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign = true;
  if (!TwoLine)
   ED->Left = ALeft;
  Width = ED->Left + ED->Width + 4;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!TwoLine)
   * AEDLeft = ED->Left;
  else
   * AEDLeft = -1;
  if (!TwoLine)
   * ALabR = FLab->Left + FLab->Width;
  else
   * ALabR = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!IsAlign)
   {
    if (TwoLine)
     {
      ED->Left  = 2;
      ED->Width = 95;
     }
    else
     ED->Left = Width - ED->Width - 2 - _TMPL_CLEAR_WIDTH_;
    FClearBtn->Left = ED->Left + ED->Width + 2;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriDateTmpl::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  DataChange(ED);
  return !FCtrlOwner->IsTemplate(_UID_);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriDateTmpl::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString xVal = "";
  if (ED->Enabled)
   if (((int)ED->Date != Cxdateutils::NullDate))
    xVal = ED->Date.FormatString("dd.mm.yyyy");
  return xVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled          = AEnable;
  FLab->Enabled      = isEnabled && isEdit && !isTemplate;
  ED->Enabled        = isEnabled && isEdit && !isTemplate;
  FClearBtn->Enabled = ED->Enabled;
  if (isEnabled && isEdit && !isTemplate)
   {
    // ED->Style->Color = (Required)? FReqColor : clWindow;
   }
  else
   {
    // ED->Style->Color = clBtnFace;
    if (isEdit && !isTemplate)
     ED->Date = Cxdateutils::NullDate;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetEnableEx(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled          = AEnable;
  FLab->Enabled      = isEnabled && isEdit && !isTemplate;
  ED->Enabled        = isEnabled && isEdit && !isTemplate;
  FClearBtn->Enabled = ED->Enabled;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnable(AEnable);
  TDate FDate;
  if (TryStrToDate(AVal, FDate))
   {
    ED->Date                               = FDate;
    FCtrlOwner->TemplateData->Value[_UID_] = FDate;
   }
  else
   {
    ED->Date                               = Cxdateutils::NullDate;
    FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::FEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if ((Key == VK_DELETE) && Shift.Contains(ssCtrl) && ED->Enabled)
   {
    FClearClick(FClearBtn);
    Key = 0;
   }
  if (Key && FCtrlOwner->OnKeyDown)
   FCtrlOwner->OnKeyDown(Sender, Key, Shift);
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ACaption.Length())
   FLab->Caption = ACaption + ":";
  else
   FLab->Caption = ""; // FNode->AV["name"]+":";
  FLab->Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnList->Add(ANode);
  ED->Properties->OnChange = DataChange;
  DataChange(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriDateTmpl::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ED->Enabled)
   {
    if ((int)ED->Date != Cxdateutils::NullDate)
     FCtrlOwner->TemplateData->Value[_UID_] = ED->Date;
    else if (isEdit && !isTemplate)
     FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
   }
  else if (isEdit && !isTemplate)
   FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
  // отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  if (FGetDataChange)
   {
    UnicodeString Src = FCtrlOwner->TemplateData->Value[_UID_];
    FGetDataChange(FNode, Src);
   }
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriDateTmpl::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TControl *)ED;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriDateTmpl::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (ED->Enabled && isEdit) ? ((TControl *)ED) : NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriDateTmpl::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if ((int)ED->Date != Cxdateutils::NullDate)
   return ED->Date.FormatString("dd.mm.yyyy");
  return "";
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriDateTmpl::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return Variant(ED->Date.FormatString("'dd.mm.yyyy'"));
 }
// ---------------------------------------------------------------------------
bool __fastcall TriDateTmpl::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  bool RC = true;
  if (Required)
   {
    try
     {
      if (ED->Enabled)
       if ((int)ED->Date == Cxdateutils::NullDate)
        RC = false;
     }
    __finally
     {
      if (!RC)
       {
        if (APC)
         APC->ActivePageIndex = this->Tag;
        if (!ASilent)
         {
          UnicodeString ErrMessage;
          if (FLab)
           ErrMessage = FLab->Caption;
          else
           ErrMessage = FNode->AV["name"];
          ErrMessage = FMT1(icsRegItemErrorReqField, ErrMessage);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
        if (ED->Visible && Visible)
         ED->SetFocus();
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
