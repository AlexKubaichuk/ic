﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop
#include "dsRegEdDigit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// #define ED         ((TcxDBMaskEdit*)ED)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
// ---------------------------------------------------------------------------
__fastcall TriDigit::TriDigit(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner, bool IsApp) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner, IsApp)
 {
  TRect xSize = Rect(0, 0, 120, 21); // default позиция и размер для "ED"
  FLab                            = new TLabel(this);
  FLab->Parent                    = this;
  FLab->Transparent               = true;
  FLab->Top                       = 2;
  FLab->Left                      = 4;
  FLab->Caption                   = FNode->AV["name"] + ":";
  ED                              = new TcxMaskEdit(this);
  ED->Parent                      = this;
  ED->Style->StyleController      = FCtrlOwner->StyleController;
  ED->Properties->OnChange        = FDataChange;
  FIsFloat                        = (FNode->AV["decimal"].ToIntDef(-1) != -1);
  ED->Properties->Alignment->Horz = taRightJustify;
  ED->Properties->MaskKind        = emkRegExpr;
  ED->Properties->EditMask = icsCalcMask(FIsFloat, FNode->AV["digits"].ToIntDef(0), FNode->AV["decimal"].ToIntDef(0),
    FNode->AV["min"]);
  ED->Properties->OnValidate = FEditValidate;
  if (Required)
   ED->Style->Color = FReqColor;
  if (IsAppend)
   {
    if (FCtrlOwner->IsTemplate(_UID_))
     {
      ED->EditValue = ICSStrToFloat(VarToStr(FCtrlOwner->TemplateData->Value[_UID_]));
      FCtrlOwner->SetCtrlData(FNode, FCtrlOwner->TemplateData->Value[_UID_], "");
     }
   }
  else
   {
    Variant tmpVal = FCtrlOwner->GetCtrlData(FNode, _ES_);
    if (!tmpVal.IsEmpty())
     ED->EditValue = ICSStrToFloat(VarToStr(tmpVal));
   }
  ED->Enabled = isEnabled && isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock());
  int xW = 5 * FNode->AV["digits"].ToInt();
  if (xW > 180)
   xW = 180;
  xSize                     = Rect(0, 0, xW, 21);
  ED->Constraints->MinWidth = 60;
  ED->Parent                = this;
  ED->Width                 = xSize.Right + 10;
  if (ED->Width < 60)
   ED->Width = 60;
  ED->Top    = xSize.Top;
  ED->Height = xSize.Bottom;
  if (FLab->Width > (AParent->Width - 2))
   {
    FLab->Font->Size = FLab->Font->Size - 1;
    FLab->Update();
    if (FLab->Width > (AParent->Width - 2))
     FLab->Width = AParent->Width - 2;
   }
  ED->Left = FLab->Left + FLab->Width + 5;
  if ((ED->Left + ED->Width) > (AParent->Width - 2))
   {
    TwoLine   = true;
    FLab->Top = 0;
    ED->Left  = FLab->Left + 5;
    ED->Top   = FLab->Height;
    CMaxH     = 34;
    CMinH     = 34;
    Height    = CMaxH;
    if ((ED->Left + ED->Width) > (AParent->Width - 2))
     ED->Width = AParent->Width - 4 - ED->Left;
    if (FNode->CmpName("text"))
     ED->Width = AParent->Width - 12 - ED->Left;
   }
  FLab->FocusControl = ED;
  ED->OnKeyDown = FEDKeyDown;
 }
// ---------------------------------------------------------------------------
TcxMaskEdit * __fastcall TriDigit::FGetCastED()
 {
  return (TcxMaskEdit *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigit::FSetCastED(TcxMaskEdit * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigit::SetOnDependValues()
 {
  if (FNode->AV["depend"].Length() == 9 && EDCtrlExists(_GUI(FNode->AV["depend"])))
   GetEDCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(this);
  else if (FNode->AV["ref"].Length() == 4 && EDCtrlExists(FNode->AV["ref"]))
   GetEDCtrl(FNode->AV["ref"])->SetOnLab(this);
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
__fastcall TriDigit::~TriDigit()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Properties->OnChange = NULL;
  delete ED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigit::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign = true;
  if (!TwoLine && ED && (!FNode->CmpName("binary")))
   ED->Left = ALeft;
  if (ED)
   Width = ED->Left + ED->Width + 4;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigit::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!TwoLine && ED)
   * AEDLeft = ED->Left;
  else
   * AEDLeft = -1;
  if (!TwoLine && FLab)
   * ALabR = FLab->Left + FLab->Width;
  else
   * ALabR = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigit::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!IsAlign)
   {
    if (TwoLine)
     {
      ED->Left  = 2;
      ED->Width = Width - 4;
     }
    else
     {
      ED->Left = Width - ED->Width - 2;
     }
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriDigit::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  DataChange(ED);
  return !FCtrlOwner->IsTemplate(_UID_);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriDigit::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString xVal = "";
  if (ED)
   {
    if (ED->Enabled)
     if (ED->Text.Length())
      xVal = ED->Text;
   }
  return xVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigit::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigit::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled = AEnable;
  bool tmpClearVal = isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock());
  FLab->Enabled = isEnabled && tmpClearVal; // isEdit && !isDelegate && (!IsAppend || !isTemplate);
  ED->Enabled   = isEnabled && tmpClearVal; // isEdit && !isDelegate && (!IsAppend || !isTemplate);
  if (!isEnabled && tmpClearVal)
   {
    ED->Text = "";
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigit::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnable(AEnable);
  if (!isFV)
   {
    ED->EditValue = ICSStrToFloat(AVal);
    FCtrlOwner->SetCtrlData(FNode, ED->Text, ED->Text);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigit::FEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if ((Key == VK_DELETE) && Shift.Contains(ssCtrl) && ED->Enabled)
   {
    ED->Text = "";
    FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), ""); // irquSetAsNull(SrcDS(),FFlName);
   }
  if (Key && FCtrlOwner->OnKeyDown)
   FCtrlOwner->OnKeyDown(Sender, Key, Shift);
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigit::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ACaption.Length())
   FLab->Caption = ACaption + ":";
  else
   FLab->Caption = ""; // FNode->AV["name"]+":";
  FLab->Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigit::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnList->Add(ANode);
  ED->Properties->OnChange = DataChange;
  DataChange(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigit::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigit::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ED->Enabled)
   {
    if (ED->Text.Trim().Length())
     FCtrlOwner->SetCtrlData(FNode, ED->Text, ED->Text);
    else if (isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock()))
     FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), "");
   }
  else if (isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock()))
   FCtrlOwner->SetCtrlData(FNode, Variant::Empty(), "");
  // отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  if (FGetDataChange)
   {
    UnicodeString Src = FCtrlOwner->GetCtrlData(FNode, _ES_);
    FGetDataChange(FNode, Src);
   }
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriDigit::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TControl *)ED;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriDigit::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (ED->Enabled && isEdit) ? ((TControl *)ED) : NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriDigit::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return ED->Text;
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriDigit::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ED->Text.Length())
   return Variant(ED->Text);
  else
   return Variant::Empty();
 }
// ---------------------------------------------------------------------------
bool __fastcall TriDigit::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  bool RC = true;
  if (Required)
   {
    try
     {
      if (ED)
       {
        if (ED->Enabled)
         {
          if (!ED->Text.Length())
           RC = false;
         }
       }
     }
    __finally
     {
      if (!RC)
       {
        if (!ASilent)
         {
          UnicodeString ErrMessage;
          if (FLab)
           ErrMessage = FLab->Caption;
          else
           ErrMessage = FNode->AV["name"];
          ErrMessage = FMT1(icsRegItemErrorReqField, ErrMessage);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
       }
     }
   }
  if (RC)
   {
    if (ED)
     {
      if (ED->Enabled && ED->Text.Length())
       {
        if (!FIsFloat)
         { // целое
          __int64 FMin, FMax, FVal;
          FMin = FNode->AV["min"].ToIntDef(0);
          FMax = FNode->AV["max"].ToIntDef(0);
          FVal = ED->Text.ToIntDef(-1);
          RC   = !((FVal < FMin) || (FVal > FMax));
         }
        else
         { // вещественное  StrToFloat
          double FMin, FMax, FVal;
          FMin = ICSStrToFloatDef(FNode->AV["min"], 0);
          FMax = ICSStrToFloatDef(FNode->AV["max"], 0);
          FVal = ICSStrToFloatDef(ED->Text, -1);
          RC   = !((FVal < FMin) || (FVal > FMax));
         }
        if (!RC)
         {
          UnicodeString ErrMessage;
          if (FLab)
           ErrMessage = FLab->Caption;
          else
           ErrMessage = FNode->AV["name"];
          if (!ASilent)
           {
            ErrMessage = FMT3(icsriDigitValRange, ErrMessage, FNode->AV["min"], FNode->AV["max"]);
            _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
           }
         }
       }
     }
   }
  if (!RC)
   {
    if (APC)
     APC->ActivePageIndex = this->Tag;
    if (ED->Visible && Visible)
     ED->SetFocus();
   }
  return RC;
 }
// ---------------------------------------------------------------------------
void __fastcall TriDigit::FEditValidate(TObject * Sender, Variant & DisplayValue, TCaption & ErrorText, bool & Error)
 {
  TReplaceFlags rFlag;
  rFlag << rfReplaceAll;
  if (FIsFloat)
   { // вещественное
    if (VarToWideStrDef(DisplayValue, "").Length())
     DisplayValue =
       Variant(System::Sysutils::StringReplace(System::Sysutils::StringReplace(VarToWideStrDef(DisplayValue, ""), ".",
      FormatSettings.DecimalSeparator, rFlag), ",", FormatSettings.DecimalSeparator, rFlag));
   }
  Error = false;
 }
// ---------------------------------------------------------------------------
