﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop
#include "dsRegEdExtEdit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
// ---------------------------------------------------------------------------
__fastcall TriExtEdit::TriExtEdit(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner, bool IsApp) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner, IsApp)
 {
  // Создаём метку контрола
  FLab              = new TLabel(this);
  FLab->Parent      = this;
  FLab->Transparent = true;
  FLab->Caption     = FNode->AV["name"] + ":";
  // Создаём метку для текстового значения
  FExtLab              = new TLabel(this);
  FExtLab->Parent      = this;
  FExtLab->Transparent = true;
  FExtLab->AutoSize    = false;
  FExtLab->ParentFont  = true;
  FExtLab->WordWrap    = true;
  FExtLab->Color       = clInactiveBorder;
  FEDSetValue          = false;
  FSearchMode          = FNode->CmpAV("comment", "search");
  bool FExtBitmapSet = false;
  // Создаём контрол
  if (FSearchMode)
   {
    FLb            = new TcxListBox(this);
    FLb->Parent    = AParent->Parent;
    FLb->Visible   = false;
    FLb->ItemIndex = -1;
    FLb->Items->Clear();
    FLb->OnDblClick            = FSearchLBDblClick;
    FLb->OnKeyDown             = FSearchLBKeyDown;
    FLb->OnExit                = FSearchLBExit;
    ED                         = new TcxButtonEdit(this);
    ED->Style->StyleController = FCtrlOwner->StyleController;
    ED->Parent                 = this;
    ED->ShowHint               = true;
    ED->Properties->ClickKey   = 0; // TShortCut("Enter"/*VK_DOWN,TShiftState() << ssAlt*/); //ShortCut
    // ED->Properties->ReadOnly = true;
    // ED->Properties->ViewStyle = vsHideCursor;
    TcxEditButton * tmpBtn;
    int FBtnCount = FNode->AV["btncount"].ToIntDef(1);
    bool FSetBmp;
    UnicodeString FHint;
    TMemoryStream * BmpStm = new TMemoryStream;
    try
     {
      for (int i = 1; i < FBtnCount; i++)
       {
        tmpBtn  = ED->Properties->Buttons->Add();
        FSetBmp = false;
        FHint   = "";
        if (ACtrlOwner->OnGetExtBtnGetBitmap)
         {
          tmpBtn->Kind = bkGlyph;
          FSetBmp      = ACtrlOwner->OnGetExtBtnGetBitmap(FNode, i, tmpBtn, FHint);
          FExtBitmapSet |= FSetBmp;
         }
        tmpBtn->Hint = FHint;
        if (!FSetBmp)
         {
          tmpBtn->Kind    = bkText;
          tmpBtn->Caption = FHint;
         }
       }
     }
    __finally
     {
      delete BmpStm;
     }
    if (!FExtBitmapSet)
     {
      ED->Properties->Buttons->Items[0]->Kind = bkGlyph;
      ED->Properties->Buttons->Items[0]->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\find.bmp"));
     }
    tmpBtn = ED->Properties->Buttons->Add();
    tmpBtn->Kind = bkGlyph;
    tmpBtn->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\clear.bmp"));
    // tmpBtn->Kind = bkText;
    // tmpBtn->Caption = "X";
    // ED->Properties->OnButtonClick = ExtBtnClick;
    ED->OnKeyDown                 = FEDKeyDown;
    ED->Properties->OnButtonClick = FEDBtnClick;
    ED->Properties->OnChange      = FEDChanged;
    // ED->Text                               = //!!!codeToText(adres.AsString(),"11111110");
    FEDModify = false;
    FFindedObjects.clear();
   }
  else
   {
    ED                        = new TcxButtonEdit(this);
    ED->Parent                = this;
    ED->ShowHint              = true;
    ED->Properties->ClickKey  = ShortCut(VK_DOWN, TShiftState() << ssAlt);
    ED->Properties->ReadOnly  = true;
    ED->Properties->ViewStyle = vsHideCursor;
    TcxEditButton * tmpBtn;
    int FBtnCount = FNode->AV["btncount"].ToIntDef(1);
    bool FSetBmp;
    UnicodeString FHint;
    TMemoryStream * BmpStm = new TMemoryStream;
    try
     {
      for (int i = 1; i < FBtnCount; i++)
       {
        tmpBtn  = ED->Properties->Buttons->Add();
        FSetBmp = false;
        FHint   = "";
        if (ACtrlOwner->OnGetExtBtnGetBitmap)
         {
          tmpBtn->Kind = bkGlyph;
          FSetBmp      = ACtrlOwner->OnGetExtBtnGetBitmap(FNode, i, tmpBtn, FHint);
          FExtBitmapSet |= FSetBmp;
         }
        tmpBtn->Hint = FHint;
        if (!FSetBmp)
         {
          tmpBtn->Kind    = bkText;
          tmpBtn->Caption = FHint;
         }
       }
     }
    __finally
     {
      delete BmpStm;
     }
    if (!FExtBitmapSet)
     {
      ED->Properties->Buttons->Items[0]->Kind = bkGlyph;
      ED->Properties->Buttons->Items[0]->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\book.bmp"));
     }
    tmpBtn = ED->Properties->Buttons->Add();
    tmpBtn->Kind = bkGlyph;
    tmpBtn->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\clear.bmp"));
    // tmpBtn = ED->Properties->Buttons->Add();
    // tmpBtn->Kind = bkText;
    // tmpBtn->Caption = "X";
    ED->Properties->OnButtonClick = ExtBtnClick;
   }
  // прочие настройки
  if (Required)
   ED->Style->Color = FReqColor;
  FLab->FocusControl = ED;
  // расположение и размеры
  FLab->Top  = 2;
  FLab->Left = 2;
  if (FLab->Width > (AParent->Width - 2))
   {
    FLab->Font->Size = FLab->Font->Size - 1;
    FLab->Update();
    if (FLab->Width > (AParent->Width - 2))
     FLab->Width = AParent->Width - 2;
   }
  ED->Constraints->MinWidth = 60;
  if (FNode->AV["linecount"].ToIntDef(0) < 2)
   {
    ED->Left = FLab->Left + FLab->Width + 5;
    ED->Top  = 0;
   }
  else
   {
    ED->Left = 2;
    ED->Top  = FLab->Height + 2;
    TwoLine  = true;
   }
  ED->Width = AParent->Width - 4 - ED->Left;
  if ((ED->Left + ED->Width) > (AParent->Width - 4))
   {
    TwoLine   = true;
    ED->Left  = 4;
    ED->Top   = FLab->Height;
    ED->Width = AParent->Width - 4 - ED->Left;
   }
  FExtLab->Left = FLab->Left;
  FExtLab->Width  = Width - 4;
  FExtLab->Height = (FExtLab->Height - 2) * (FNode->AV["linecount"].ToIntDef(1) - 1);
  FExtLab->Top    = ED->Top + ED->Height;
  CMaxH           = FExtLab->Top + FExtLab->Height + 2;
  CMinH           = CMaxH;
  Height          = CMaxH;
  if (FCtrlOwner->OnExtBtnClick)
   {
    UnicodeString SrcCode = "";
    if (IsAppend)
     {
      if (FCtrlOwner->IsTemplate(_UID_))
       {
        SrcCode = VarToStr(FCtrlOwner->TemplateData->Value[_UID_]);
        if (FCtrlOwner->OnExtBtnClick(FNode, SrcCode, NULL /* FSrc */ , false, (TObject *) FCtrlOwner, 0))
         {
          FCtrlOwner->SetCtrlData(FNode, FCtrlOwner->TemplateData->Value[_UID_], "");
          FSetEditValue(SrcCode);
         }
       }
     }
    else
     {
      if (!FCtrlOwner->GetCtrlData(FNode, _ES_).IsEmpty())
       {
        SrcCode = FCtrlOwner->GetCtrlData(FNode, _ES_);
        if (FCtrlOwner->OnExtBtnClick(FNode, SrcCode, NULL /* FSrc */ , false, (TObject *) FCtrlOwner, 0))
         FSetEditValue(SrcCode);
       }
     }
   }
  if (FNode->GetAVDef("depend").Length() && EDCtrlExists(_GUI(FNode->AV["depend"])))
   {
    UnicodeString DepUID = _GUI(FNode->AV["depend"]);
    TTagNode * DepNode = FCtrlOwner->GetNode(DepUID);
    UnicodeString DepVal = GetEDCtrl(_GUI(FNode->AV["depend"]))->GetValue("");
    SetDependVal(DepNode->AV["ref"], DepVal.ToIntDef(-1), DepNode->AV["uid"]);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::FSearchLBDblClick(TObject * Sender)
 {
  if (FLb->ItemIndex != -1)
   {
    UnicodeString SrcCode = FSetEditValue(FFindedObjects[FLb->Items->Strings[FLb->ItemIndex].UpperCase()] + "=" +
      FLb->Items->Strings[FLb->ItemIndex]);
    // !!!     FSetFindedAddr(FFindedObjects[lb->Items->Strings[lb->ItemIndex].UpperCase()]);
    FFindedObjects.clear();
    FEDModify    = false;
    FLb->Visible = false;
    if (ED->Visible)
     ED->SetFocus();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::FSearchLBKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_RETURN)
   {
    FSearchLBDblClick((TcxListBox *) Sender);
    Key = 0;
   }
  else if (Key == VK_ESCAPE)
   {
    FFindedObjects.clear();
    ((TcxListBox *)Sender)->Visible = false;
    if (ED->Visible)
     ED->SetFocus();
    FEDModify = true;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::FSearchLBExit(TObject * Sender)
 {
  FLb->Visible = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::FEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_RETURN) // (Key == 'F') && Shift.Contains(ssCtrl)
   {
    if (FEDModify)
     {
      FEDBtnClick(NULL, 0);
      Key = 0;
     }
   }
  else if ((Key == VK_DELETE) && Shift.Contains(ssCtrl) && ED->Enabled)
   {
    FEDBtnClick(NULL, 1);
    Key = 0;
   }
  if (Key && FCtrlOwner->OnKeyDown)
   FCtrlOwner->OnKeyDown(Sender, Key, Shift);
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::FEDBtnClick(TObject * Sender, int AButtonIndex)
 {
  if (AButtonIndex == 1)
   { // очистка редактора
    ED->Text    = "";
    FEDModify   = false;
    FEDSetValue = false;
   }
  else
   {
    FSearchData(ED->Text.Trim(), AButtonIndex);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::FSearchData(UnicodeString AData, int AButtonIndex)
 {
  if (FCtrlOwner->OnExtBtnClick && AData.Length())
   {
    UnicodeString SrcCode = AData; // FCtrlOwner->GetCtrlData(FNode, _ES_);
    if (FCtrlOwner->OnExtBtnClick(FNode, SrcCode, NULL, true, (TObject *)FCtrlOwner, AButtonIndex))
     {
      TStringList * FValues = new TStringList;
      try
       {
        FLb->Items->Clear();
        FValues->Text = SrcCode;
        // раскидываем по контролам
        if (AData.LowerCase().Pos("findbycode"))
         {
          if (FValues->Count == 1)
           FSetEditValue(FValues->Strings[0]);
          FEDModify = false;
         }
        else
         {
          if (FValues->Count == 1)
           {
            SrcCode   = FSetEditValue(FValues->Strings[0]);
            FEDModify = false;
           }
          else if (FValues->Count > 1)
           {
            for (int i = 0; i < FValues->Count; i++)
             {
              FFindedObjects[FValues->Values[FValues->Names[i]].UpperCase()] = FValues->Names[i];
              FLb->Items->Add(FValues->Values[FValues->Names[i]]);
             }
            FLb->Left = Parent->Left + Left + ED->Left;
            FLb->Top = Parent->Top + Top + ED->Top + ED->Height + 1;
            if (FSearchMode)
             FLb->Width = ED->Width * 1.5;
            else
             FLb->Width = ED->Width;
            FLb->Visible = true;
            FLb->BringToFront();
            if (FLb->Visible)
             FLb->SetFocus();
            FLb->ItemIndex = 0;
           }
          else
           {
            FEDModify = false;
            if (ED->Visible)
             ED->SetFocus();
           }
         }
       }
      __finally
       {
        delete FValues;
       }
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::FEDChanged(TObject * Sender)
 {
  FEDModify = true;
  if (!ED->Text.Trim().Length())
   {
    FExtLab->Caption = "";
    FCtrlOwner->SetCtrlData(FNode, "", "");
    DataChange(this);
   }
 }
// ---------------------------------------------------------------------------
TcxButtonEdit * __fastcall TriExtEdit::FGetCastED()
 {
  return (TcxButtonEdit *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::FSetCastED(TcxButtonEdit * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::SetOnDependValues()
 {
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriExtEdit::FSetEditValue(UnicodeString AVal)
 {
  UnicodeString FCode = "";
  UnicodeString FText = "";
  try
   {
    int ind = AVal.Pos("=");
    if (ind)
     {
      FText = AVal.SubString(ind + 1, AVal.Length() - ind);
      FCode = AVal.SubString(1, ind - 1);
     }
    else
     FCode = AVal;
    if (FNode->AV["showtype"].ToIntDef(0) && !FSearchMode)
     { // 1 - в редакторе код
      ED->Text         = FCode;
      FExtLab->Caption = FText;
     }
    else
     { // 0 - в редакторе текст
      ED->Text         = FText;
      FExtLab->Caption = FCode;
     }
    FEDSetValue = ED->Text.Length();
    FCtrlOwner->SetCtrlData(FNode, FCode, FText);
    DataChange(this);
   }
  __finally
   {
   }
  return FCode;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::ExtBtnClick(TObject * Sender, int AButtonIndex)
 {
  UnicodeString SrcCode = "";
  if (!AButtonIndex)
   {
    if (FCtrlOwner->OnExtBtnClick)
     {
      SrcCode = FCtrlOwner->GetCtrlData(FNode, _ES_);
      if (FCtrlOwner->OnExtBtnClick(FNode, SrcCode, NULL /* FSrc */ , true, (TObject *) FCtrlOwner, AButtonIndex))
       {
        SrcCode = FSetEditValue(SrcCode);
        FCtrlOwner->SetCtrlData(FNode, SrcCode, ""); // irquSetAsStr(SrcDS(),FFlName,);
       }
     }
   }
  else
   {
    // if (SrcDSEditable())
    // {
    FEDSetValue      = false;
    ED->Text         = "";
    FExtLab->Caption = "";
    SrcCode          = "";
    FCtrlOwner->SetCtrlData(FNode, SrcCode, ""); // irquSetAsStr(SrcDS(),FFlName,SrcCode);
    // }
   }
 }
// ---------------------------------------------------------------------------
__fastcall TriExtEdit::~TriExtEdit()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Properties->OnButtonClick = NULL;
  delete FExtLab;
  delete ED;
  if (FSearchMode)
   delete FLb;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign = true;
  if (!TwoLine)
   ED->Left = ALeft;
  FExtLab->Width = ED->Left + ED->Width;
  Width          = ED->Left + ED->Width + 2;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!TwoLine && ED)
   * AEDLeft = ED->Left;
  else
   * AEDLeft = -1;
  if (!TwoLine && FLab)
   * ALabR = FLab->Left + FLab->Width;
  else
   * ALabR = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!IsAlign && ED)
   {
    FExtLab->Left  = 2;
    FExtLab->Width = Width - 4;
    if (TwoLine)
     {
      ED->Left  = 2;
      ED->Width = Width - 4;
     }
    else
     ED->Left = Width - ED->Width - 2;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriExtEdit::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return !FCtrlOwner->IsTemplate(_UID_);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriExtEdit::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString xVal = "";
  if (FNode->AV["showtype"].ToIntDef(0) && !FSearchMode)
   xVal = FExtLab->Caption;
  else
   xVal = ED->Text;
  return xVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled = AEnable;
  bool tmpClearVal = isEdit && !isDelegate && (!IsAppend || !isTemplate || FCtrlOwner->TemplateNoLock());
  FLab->Enabled    = isEnabled && tmpClearVal;
  ED->Enabled      = isEnabled && tmpClearVal;
  FExtLab->Enabled = isEnabled && tmpClearVal;
  if (!isEnabled && tmpClearVal)
   {
    FSetEditValue("");
    FCtrlOwner->SetCtrlData(FNode, "", ""); // irquSetAsStr(SrcDS(),FFlName,"");
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnable(AEnable);
  UnicodeString FVal = AVal;
  if (AVal.LowerCase().Pos("findbycode"))
   FSearchData(AVal, 0);
  else
   FCtrlOwner->SetCtrlData(FNode, FSetEditValue(AVal), "");
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ACaption.Length())
   FLab->Caption = ACaption + ":";
  else
   FLab->Caption = ""; // FNode->AV["name"]+":";
  FLab->Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnList->Add(ANode);
  DataChange(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEdit::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  // отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  if (FGetDataChange)
   {
    UnicodeString Src = FCtrlOwner->GetCtrlData(FNode, _ES_);
    FGetDataChange(FNode, Src);
   }
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriExtEdit::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TControl *)ED;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriExtEdit::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (ED->Enabled && isEdit) ? ((TControl *)ED) : NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriExtEdit::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (FNode->AV["showtype"].ToIntDef(0) && !FSearchMode)
   return ED->Text;
  else
   return FExtLab->Caption;
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriExtEdit::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (GetValue("").Trim().Length())
   return GetValue("");
  else
   return Variant("NULL");
 }
// ---------------------------------------------------------------------------
bool __fastcall TriExtEdit::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  bool RC = true;
  if (Required)
   {
    try
     {
      if (ED)
       {
        if (ED->Enabled)
         {
          if (FSearchMode)
           RC = FEDSetValue;
          else
           RC = ED->Text.Length();
         }
       }
     }
    __finally
     {
      if (!RC)
       {
        if (APC)
         APC->ActivePageIndex = this->Tag;
        if (!ASilent)
         {
          UnicodeString ErrMessage;
          if (FLab)
           ErrMessage = FLab->Caption;
          else
           ErrMessage = FNode->AV["name"];
          ErrMessage = FMT1(icsRegItemErrorReqField, ErrMessage);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
        if (ED->Visible && Visible)
         ED->SetFocus();
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
