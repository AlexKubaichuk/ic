//---------------------------------------------------------------------------

#pragma hdrstop

#include "KabCustomDS.h"
#include "JSONUtils.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall EkabCustomDataSetRowError::EkabCustomDataSetRowError(UnicodeString Msg)
    : Exception(Msg)
 {
 }
//---------------------------------------------------------------------------
TkabCustomDataSetRowField::TkabCustomDataSetRowField()
 {
  Value    = Variant::Empty();
  StrValue = "";
  Name     = "";
  Type     = "";
 };
//---------------------------------------------------------------------------
void TkabCustomDataSetRowField:: operator = (TkabCustomDataSetRowField AVal)
 {
  Value    = AVal.Value;
  StrValue = AVal.StrValue;
  Name     = AVal.Name;
  Type     = AVal.Type;
 };
//---------------------------------------------------------------------------
//###########################################################################
//#                                                                         #
//#                     TkabCustomDataSetRow                                #
//#                                                                         #
//###########################################################################
//---------------------------------------------------------------------------

__fastcall TkabCustomDataSetRow::TkabCustomDataSetRow(TTagNode * AFlDef)
 {
  FFlDef = AFlDef;
  NewRec();
 }
//---------------------------------------------------------------------------
__fastcall TkabCustomDataSetRow::TkabCustomDataSetRow(TTagNode * AFlDef, TkabCustomDataSetRow * ASrc)
 {
  FFlDef = AFlDef;
  NewRec();
  TTagNode * itFl = FFlDef->GetFirstChild();
  UnicodeString FName;
  while (itFl)
   {
	if (itFl->CmpName("code,data"))
	 FName = itFl->Name;
	else
	 FName = itFl->AV["uid"];
	FSetValue(FName, (ASrc) ? ASrc->Value[FName] : Variant::Empty());
	if (itFl->CmpName("binary,choice,choiceDB,extedit,choiceTree"))
	 FSetStrValue(FName, (ASrc) ? ASrc->StrValue[FName] : Variant::Empty());
	itFl = itFl->GetNext();
   }
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSetRow::NewRec()
 {
  FFetched     = false;
  FListFetched = false;
  FDataFields  = new TkabCustomDataSetFieldsMap;
  //FStrDataFields = new TkabCustomDataSetFieldsMap;
  TTagNode * itFl = FFlDef->GetFirstChild();
  UnicodeString FName, FType;
  TkabCustomDataSetRowField tmp;
  while (itFl)
   {
	if (itFl->CmpName("code,data"))
	 tmp.Name = itFl->Name;
	else
	 tmp.Name = itFl->AV["uid"];

	tmp.Type = itFl->Name;
	(*FDataFields)[tmp.Name.UpperCase()] = tmp;
	itFl = itFl->GetNext();
   }
 }
//---------------------------------------------------------------------------
__fastcall TkabCustomDataSetRow::~TkabCustomDataSetRow()
 {
  delete FDataFields;
  //delete FStrDataFields;
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSetRow::FSetValue(UnicodeString AName, const Variant & AVal)
 {
  try
   {
	TkabCustomDataSetFieldsMap::iterator FRC = FDataFields->find(AName.UpperCase());
	if (FRC != FDataFields->end())
	 FRC->second.Value = AVal;
	else
	 throw EkabCustomDataSetRowError("field '" + AName + "' not found.");
   }
  __finally
   {
   }
 }
//---------------------------------------------------------------------------
Variant __fastcall TkabCustomDataSetRow::FGetValue(UnicodeString AName)
 {
  Variant RC = Variant::Empty();
  try
   {
	TkabCustomDataSetFieldsMap::iterator FRC = FDataFields->find(AName.UpperCase());
	if (FRC != FDataFields->end())
	 RC = FRC->second.Value;
	else
	 throw EkabCustomDataSetRowError("field '" + AName + "' not found.");
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSetRow::FSetStrValue(UnicodeString AName, const Variant & AVal)
 {
  try
   {
	TkabCustomDataSetFieldsMap::iterator FRC = FDataFields->find(AName.UpperCase());
	if (FRC != FDataFields->end())
	 FRC->second.StrValue = AVal;
	else
	 throw EkabCustomDataSetRowError("field '" + AName + "' not found.");
   }
  __finally
   {
   }
 }
//---------------------------------------------------------------------------
Variant __fastcall TkabCustomDataSetRow::FGetStrValue(UnicodeString AName)
 {
  UnicodeString RC;
  try
   {
	TkabCustomDataSetFieldsMap::iterator FRC = FDataFields->find(AName.UpperCase());
	if (FRC != FDataFields->end())
	 {
	  RC = FRC->second.StrValue.Trim();
	  if (!RC.Length())
	   RC = FGetValue(AName);
	 }
	else
	 RC = FGetValue(AName);
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TkabCustomDataSetRow::FGetType(UnicodeString AName)
 {
  UnicodeString RC = "";
  try
   {
	TkabCustomDataSetFieldsMap::iterator FRC = FDataFields->find(AName.UpperCase());
	if (FRC != FDataFields->end())
	 RC = FRC->second.Type;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
TJSONObject * __fastcall TkabCustomDataSetRow::GetJSONValues()
 {
  TJSONObject * RC = new TJSONObject;
  try
   {
	TTagNode * itFl = FFlDef->GetFirstChild();
	UnicodeString FVal, FName;
	Variant tmpVal;
	while (itFl)
	 {
	  FName = "";
	  if (!itFl->CmpName("code,data"))
	   FName = itFl->AV["uid"];
	  else if (itFl->CmpName("code"))
	   FName = itFl->Name;
	  if (FName.Length())
	   {
		tmpVal = Value[FName];
		if (!(tmpVal.IsEmpty() || tmpVal.IsNull()))
		 {
		  if (SameText(Type[FName], "date"))
		   {
			if (!int(tmpVal))
			 RC->AddPair(FName, new TJSONNull);
			else
			 RC->AddPair(FName, tmpVal);
		   }
		  else
		   RC->AddPair(FName, tmpVal);

		 }
		else
		 RC->AddPair(FName, new TJSONNull);
	   }
	  itFl = itFl->GetNext();
	 }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------

//###########################################################################
//#                                                                         #
//#                         TkabCustomDataSet                               #
//#                                                                         #
//###########################################################################
//---------------------------------------------------------------------------

__fastcall EkabCustomDataSetError::EkabCustomDataSetError(UnicodeString Msg)
    : Exception(Msg)
 {
 }
//---------------------------------------------------------------------------
__fastcall TkabCustomDataSet::TkabCustomDataSet(TTagNode * ADefNode)
 {
  FDSIdPref = "reg";
  FDefUID   = ADefNode->AV["uid"];
  FDSId     = FDSIdPref + "." + FDefUID;

  FDefNode       = new TTagNode;
  FDefNode->Name = "fld";

  FDefListNode       = new TTagNode;
  FDefListNode->Name = "lfld";

  UnicodeString _EMPTYSTR_;
  FDefNode->AddChild("code");
  FDefNode->AddChild("data");
  ADefNode->Iterate(FSetFieldsDef, _EMPTYSTR_);

  FRows       = new TList;
  FCodeIdx    = new TkabCustomDataSetIntMap;
  FNewCodeIdx = new TkabCustomDataSetIntMap;
  FEditedRow  = NULL;
  FDSState    = dsBrowse;
  FTTH        = "";
 }
//---------------------------------------------------------------------------
bool __fastcall FCreateFieldsDef(TTagNode * ANode, void * Src1)
 {
  if (ANode->CmpName("binary,choice,choiceDB,choiceTree,text,date,extedit,digit"))
   {
    ((TTagNode *)Src1)->AddChild()->Assign(ANode, true);
   }
  return false;
 }
//---------------------------------------------------------------------------
TTagNode * __fastcall TkabCustomDataSet::CreateDefNode(TTagNode * ADefNode)
 {
  TTagNode * RC = new TTagNode;
  RC->Name = "fld";
  RC->AddChild("code");
  RC->AddChild("data");
  ADefNode->_Iterate(FCreateFieldsDef, RC);
  return RC;
 }
//---------------------------------------------------------------------------
__fastcall TkabCustomDataSet::~TkabCustomDataSet()
 {
  Clear();
  if (FEditedRow)
   delete FEditedRow;
  delete FCodeIdx;
  delete FNewCodeIdx;
  delete FRows;
  delete FDefNode;
  delete FDefListNode;
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSet::FSetDSIdPref(UnicodeString AVal)
 {
  FDSIdPref = AVal;
  FDSId     = FDSIdPref + "." + FDefUID;
 }
//---------------------------------------------------------------------------
TkabCustomDataSetRow * __fastcall TkabCustomDataSet::Add()
 {
  TkabCustomDataSetRow * RC = new TkabCustomDataSetRow(FDefNode);
  FRows->Add(RC);
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSet::Insert()
 {
  if (FEditedRow)
   delete FEditedRow;
  FEditedRow = new TkabCustomDataSetRow(FDefNode);
  FDSState   = dsInsert;
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSet::Edit()
 {
  if (FEditedRow)
   delete FEditedRow;
  if (FOnGetRowIdx)
   FEditedRow = new TkabCustomDataSetRow(FDefNode, FGetRow(FOnGetRowIdx(-1)));
  else
   FEditedRow = new TkabCustomDataSetRow(FDefNode);
  FDSState = dsEdit;
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSet::Delete(int ARecIdx)
 {
  if (FEditedRow)
   delete FEditedRow;
  FEditedRow = NULL;
  if (FOnGetRowIdx)
   {
    int FRowIdx = FOnGetRowIdx(ARecIdx);
    TkabCustomDataSetRow * FDelRow = FGetRow(FRowIdx);
    if (FDelRow)
     {
      if (FOnDelete)
       {
        UnicodeString RC = FOnDelete(FDSId, FDelRow->Value["code"]);
        if (!SameText(RC, "ok"))
         throw EkabCustomDataSetError(RC);
       }

      //FCodeIdx->erase(VarToStr(FDelRow->FN["code"]));
      FRows->Delete(FRowIdx);
      FCodeIdx->clear();
      for (int i = 0; i < FRows->Count; i++)
       (*FCodeIdx)[VarToStr(FGetRow(i)->Value["code"])] = i;
     }
   }
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TkabCustomDataSet::Post(UnicodeString & ARetCode, bool ACheck)
 {
  UnicodeString RC = "ok";
  ARetCode = "";
  try
   {
    if (FEditedRow && ((FDSState == dsInsert) || (FDSState == dsEdit)))
     {
      if (FDSState == dsInsert)
       {
        if (FOnInsert)
         RC = FOnInsert(FDSId, FEditedRow->GetJSONValues(), ARetCode);
       }
      else if (FDSState == dsEdit)
       {
        if (FOnEdit)
         RC = FOnEdit(FDSId, FEditedRow->GetJSONValues(), ARetCode);
       }
      if (SameText(RC, "ok") || (!SameText(RC, "ok") && !ACheck))
       {
        delete FEditedRow;
        FEditedRow = NULL;
        FDSState   = dsBrowse;
       }
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSet::Cancel()
 {
  if (FEditedRow)
   delete FEditedRow;
  FEditedRow = NULL;
  FDSState   = dsBrowse;
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSet::Clear()
 {
  for (int i = 0; i < FRows->Count; i++)
   {
    delete FRows->Items[i];
    FRows->Items[i] = NULL;
   }
  FRows->Clear();
  FCodeIdx->clear();
  FDSState = dsInactive;
 }
//---------------------------------------------------------------------------
TkabCustomDataSetRow * __fastcall TkabCustomDataSet::FGetCurrRow()
 {
  __int64 FRowIdx = -1;
  if (FOnGetRowIdx)
   FRowIdx = FOnGetRowIdx(-1);
  if ((FRowIdx == -1) || (FRowIdx >= FRows->Count))
   throw EkabCustomDataSetError("�� ������� ������� ������.");
  else
   return FGetRow(FRowIdx);
 }
//---------------------------------------------------------------------------
bool __fastcall TkabCustomDataSet::FSetFieldsDef(TTagNode * ANode, UnicodeString & ATmp)
 {
  if (ANode->CmpName("binary,choice,choiceDB,choiceTree,text,date,extedit,digit"))
   {
    FDefNode->AddChild()->Assign(ANode, true);
    if (ANode->CmpAV("inlist", "l") && !(ANode->CmpAV("isedit", "0") && !ANode->CmpAV("depend", "")))
     FDefListNode->AddChild()->Assign(ANode, true);
   }
  return false;
 }
//---------------------------------------------------------------------------
__int64 __fastcall TkabCustomDataSet::FExtGetCount(System::UnicodeString  AFilterParam)
 {
  __int64 RC = 0;
  try
   {
    if (FOnGetCount)
     RC = FOnGetCount(FDSId, FTTH, AFilterParam);
    else
     throw EkabCustomDataSetError("�� ����� ���������� OnGetCount.");
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TkabCustomDataSet::FExtGetIdList(System::UnicodeString  AFilterParam, TkabCDSProgressEvent AProgress)
 {
  bool RC = false;
  try
   {
    if (FOnGetIdList)
     {
      RC = FCreateRecByIdList((TJSONArray *)FOnGetIdList(FDSId, 0, FTTH, AFilterParam), AProgress);
     }
    else
     throw EkabCustomDataSetError("�� ����� ���������� OnGetIdList.");
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TkabCustomDataSet::FCreateRecByIdList(TJSONArray * AIdList, TkabCDSProgressEvent AProgress)
 {
  bool RC = false;
  try
   {
    try
     {
      TkabCustomDataSetRow * tmpRow;
      UnicodeString FCode;

      int FStp = AIdList->Count / 50;
      int Prs = 0;
      FShowProgress(AProgress, AIdList->Count, 1, "��������� ������: ");
      FNewCodeIdx->clear();
      bool FArrayType = false;
      TJSONObject * tmpFlList;
      for (int i = 0; i < AIdList->Count; i++)
       {
        tmpFlList = NULL;
        FCode     = ((TJSONString *)AIdList->Items[i])->Value();
        if (FCode.Length())
         {//������ �����
          FArrayType = true;
         }
        else
         {//������ ����� ������
          tmpFlList = ((TJSONObject *)AIdList->Items[i]);
          FCode     = JSONToString(tmpFlList, "code");
         }
        if (FCodeIdx->find(FCode) == FCodeIdx->end())
         {
          tmpRow = Add();
          (*FCodeIdx)[FCode] = FRows->Count - 1;
         }
        else
         {
          tmpRow = FGetRow((*FCodeIdx)[FCode]);
         }

        (*FNewCodeIdx)[FCode] = 1;
        tmpRow->Value["code"] = FCode;
        tmpRow->Fetched       = false;
        tmpRow->ListFetched   = false;
        if (!FArrayType && tmpFlList)
         FGetRowData(tmpRow, tmpFlList, true);
        if (Prs < i)
         {
          Prs += FStp;
          FShowProgress(AProgress, AIdList->Count, i + 1, "��������� ������: ");
         }
       }
      for (int i = FRows->Count - 1; i >= 0; i--)
       {
        if (FNewCodeIdx->find(VarToStr(FGetRow(i)->Value["code"])) == FNewCodeIdx->end())
         {//��� �� ������, �������
          FRows->Delete(i);
         }
       }
      FCodeIdx->clear();
      for (int i = 0; i < FRows->Count; i++)
       (*FCodeIdx)[VarToStr(FGetRow(i)->Value["code"])] = i;
      FShowProgress(AProgress, AIdList->Count, AIdList->Count, "��������� ������: ");
      RC = true;
     }
    __finally
     {
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
__int64 __fastcall TkabCustomDataSet::GetCount()
 {
  return FRows->Count; //FExtGetCount(NULL);
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSet::FShowProgress(TkabCDSProgressEvent AProgress, int AMax, int ACur, UnicodeString  AMsg)
 {
  if (AProgress)
   AProgress(AMax, ACur, AMsg);
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSet::FilterByFind(TJSONArray * AIdList, TdsRegFetch AFetchAll, int AFetchRecCount, TkabCDSProgressEvent AProgress)
 {
  TTime FBeg = Time();
  FShowProgress(AProgress, 0, 0, "��������� ����� �������");
  if (FOnGetValById && FCreateRecByIdList(AIdList, AProgress))
   {
    FGetRecData10(AFetchAll, AFetchRecCount, AProgress);
   }
  FShowProgress(AProgress, 0, 0, IntToStr(FRows->Count) + " ���. �������� �� " + (Time() - FBeg).FormatString("nn �. ss,zzz �."));
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSet::GetData(System::UnicodeString  AFilterParam, TdsRegFetch AFetchAll, int AFetchRecCount, TkabCDSProgressEvent AProgress)
 {
  GetData10(AFilterParam, AFetchAll, AFetchRecCount, AProgress);
 }
//---------------------------------------------------------------------------
bool __fastcall TkabCustomDataSet::IsAllFetched()
 {
  bool RC = true;
  try
   {
    for (int i = 0; (i < FRows->Count) && RC; i++)
     RC &= Row[i]->Fetched;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSet::FetchAll(int AFetchRecCount, TkabCDSProgressEvent AProgress)
 {
  TTime FBeg = Time();
  FGetRecData10(TdsRegFetch::All, AFetchRecCount, AProgress);
  FShowProgress(AProgress, 0, 0, IntToStr(FRows->Count) + " ���. �������� �� " + (Time() - FBeg).FormatString("nn �. ss,zzz �."));
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSet::GetData10(System::UnicodeString  AFilterParam, TdsRegFetch AFetchAll, int AFetchRecCount, TkabCDSProgressEvent AProgress)
 {
  TTime FBeg = Time();
  FShowProgress(AProgress, 0, 0, "��������� ���������� �������");
  if (FExtGetCount(AFilterParam))
   {
    FShowProgress(AProgress, 0, 0, "��������� ����� �������");
    if (FOnGetValById && FExtGetIdList(AFilterParam, AProgress))
     {
      FGetRecData10(AFetchAll, AFetchRecCount, AProgress);
     }
   }
  else if (FRows->Count)
   Clear();

  //FShowProgress(AProgress,FRows->Count, FRows->Count, "������������ �������: ");
  //Sleep(50);
  FShowProgress(AProgress, 0, 0, IntToStr(FRows->Count) + " ���. �������� �� " + (Time() - FBeg).FormatString("nn �. ss,zzz �."));
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSet::FGetRecData10(TdsRegFetch AFetchAll, int AFetchRecCount, TkabCDSProgressEvent AProgress)
 {
  bool FFetchAllRec = (AFetchAll != TdsRegFetch::None);
  if (FFetchAllRec && (AFetchAll == TdsRegFetch::Auto))
   FFetchAllRec = FRows->Count < 20;
  if (FFetchAllRec)
   {
    int FStp = FRows->Count / 100;
    int Prs = 0;
    int FHi;
    FShowProgress(AProgress, FRows->Count, 1, "������������ �������: ");
    for (int i = 0; i < FRows->Count; i += AFetchRecCount)
     {
      if (Prs < i)
       {
        Prs += FStp;
        FShowProgress(AProgress, FRows->Count, i + 1, "��������� ������: ");
       }
      FHi = i + (AFetchRecCount - 1);
      if (FHi >= FRows->Count)
       FHi = FRows->Count - 1;
      GetRowData10(i, FHi);
     }
   }
 }
//---------------------------------------------------------------------------
TJSONObject * __fastcall TkabCustomDataSet::GetChDBData(UnicodeString  ARef, UnicodeString  AVal)
 {
  TJSONObject * RC = NULL;
  try
   {
    if (FOnGetValById)
     {
      RC = FOnGetValById(FDSIdPref + "." + ARef, AVal);
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TkabCustomDataSet::GetDataById(UnicodeString  ACode, TkabCDSProgressEvent AProgress)
 {
  bool RC = false;
  try
   {
    TkabCustomDataSetRow * tmpRow;
    FShowProgress(AProgress, 1, 1, "��������� ������: ");
    if (FCodeIdx->find(ACode) == FCodeIdx->end())
     {
      tmpRow = Add();
      (*FCodeIdx)[ACode] = FRows->Count - 1;
     }
    else
     {
      tmpRow = FGetRow((*FCodeIdx)[ACode]);
     }
    tmpRow->Value["code"] = ACode;
    tmpRow->Fetched     = false;
    tmpRow->ListFetched = false;
    GetRowData((* FCodeIdx)[ACode], AProgress);
    FShowProgress(AProgress, 1, 1, "��������� ������: ");
    RC = true;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
bool __fastcall TkabCustomDataSet::FGetRowData(TkabCustomDataSetRow * ARow, TJSONObject * ARowData, bool AListOnly)
 {
  bool RC = false;
  try
   {
    TJSONObject * tmpFlData;
    TJSONString * tmpData;
    UnicodeString FFlId;
    UnicodeString FFlVal;
    TTagNode * itField = FDefNode->GetFirstChild();
    bool FCont;
    while (itField)
     {
      if (!itField->CmpName("code,data"))
       {
        if (AListOnly)
         FCont = itField->CmpAV("inlist", "l");
        else
         FCont = true;
        if (FCont)
         {
          FFlId     = itField->AV["uid"];
          tmpFlData = (TJSONObject *)GetJSONValue(ARowData, FFlId); //str | {code:str} | null
          if (tmpFlData)
           {
            ARow->Value[FFlId]    = Variant::Empty();
            ARow->StrValue[FFlId] = "";
            if (!tmpFlData->Null)
             {
              tmpData = (TJSONString *)tmpFlData;
              if (itField->CmpName("binary,choice,choiceDB,choiceTree,extedit"))
               {
                if (tmpFlData->Count)
                 {
                  FFlVal = JSONPairName(tmpFlData->Pairs[0]);
                  if (FFlVal.Trim().Length())
                   {
                    if (itField->CmpName("binary,choice,choiceDB"))
                     ARow->Value[FFlId] = FFlVal.ToInt();
                    else
                     ARow->Value[FFlId] = FFlVal;

                   }
                  ARow->StrValue[FFlId] = JSONPairValue(tmpFlData->Pairs[0])->Value();
                 }
               }
              else
               {
                if (tmpData->Value().Trim().Length())
                 {
                  if (itField->CmpName("date"))
                   ARow->Value[FFlId] = TDate(tmpData->Value());
                  else
                   ARow->Value[FFlId] = tmpData->Value();
                 }
               }
             }
           }
         }
       }
      itField = itField->GetNext();
     }
    ARow->Fetched = !AListOnly;
    ARow->ListFetched = true;
    RC                = true;
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSet::GetRowData(int ARowId, TkabCDSProgressEvent AProgress)
 {
  if (FOnGetValById)
   {
    if (!Row[ARowId]->Fetched)
     FGetRowData(Row[ARowId], FOnGetValById(FDSId, Row[ARowId]->Value["code"]));
   }
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSet::GetRowData10(__int64 ALo, __int64 AHi, TkabCDSProgressEvent AProgress)
 {
  if (FOnGetValById10)
   {
    //TTime FBegTime = Time();
    //TTime FfBegTime = Time();
    //TJSONArray *ARowId
    TJSONObject * RetData;
    TJSONObject * tmpPair;
    UnicodeString FFlVal = "";
    for (int i = ALo; i <= AHi; i++)
     {
      if (!Row[i]->Fetched)
       {
        if (FFlVal.Length())
         FFlVal += "," + Row[i]->Value["code"];
        else
         FFlVal = Row[i]->Value["code"];
       }
     }
    if (FFlVal.Length())
     {
      RetData = FOnGetValById10(FDSId, FFlVal);
      if (RetData)
       {
        TJSONPairEnumerator * itPair = RetData->GetEnumerator();
        TJSONObject * FRowData;
        while (itPair->MoveNext())
         {
          FRowData = (TJSONObject *)itPair->Current->JsonValue;
          FGetRowData(Row[GetRowIdxByCode(((TJSONString *)(FRowData->Values["code"]))->Value())], FRowData);
         }
       }
     }
   }
 }
//---------------------------------------------------------------------------
__int64 __fastcall TkabCustomDataSet::GetRowIdxByCode(UnicodeString  ACode)
 {
  __int64 RC = -1;
  try
   {
    if (FCodeIdx->find(ACode) != FCodeIdx->end())
     RC = (*FCodeIdx)[ACode];
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
int __fastcall TkabCustomDataSet::FGetCount()
 {
  return FRows->Count;
 }
//---------------------------------------------------------------------------
TkabCustomDataSetRow * __fastcall TkabCustomDataSet::FGetRow(int AIdx)
 {
  TkabCustomDataSetRow * RC = NULL;
  RC = ((TkabCustomDataSetRow *)FRows->Items[AIdx]);
  return RC;
 }
//---------------------------------------------------------------------------

//###########################################################################
//#                                                                         #
//#                       TkabCustomDataSource                              #
//#                                                                         #
//###########################################################################

//---------------------------------------------------------------------------
__fastcall TkabCustomDataSource::TkabCustomDataSource(TTagNode * ADefNode) : TcxCustomDataSource()
 {
  FDS              = new TkabCustomDataSet(ADefNode);
  FDS->OnGetRowIdx = FGetCurrentRecIdx;

  FGridColumns = new TColMap;
 }
//---------------------------------------------------------------------------
__fastcall TkabCustomDataSource::~TkabCustomDataSource()
 {
  FDS->OnGetRowIdx = NULL;
  delete FGridColumns;
  delete FDS;
 }
//---------------------------------------------------------------------------
int __fastcall TkabCustomDataSource::FGetCurrentRecIdx(int ARecIdx)
 {
  int RC = ARecIdx;
  try
   {
    try
     {
      if (RC == -1)
       RC = DataController->GetFocusedRecordIndex();
      else
       RC = DataController->GetSelectedRowIndex(RC);
      if (RC != -1)
       {
        if (RC < FDS->RecordCount)
         RC = (int)GetRecordHandleByIndex(RC);
        else
         RC = -1;
       }
     }
    catch (Exception & E)
     {
      throw EkabCustomDataSetError(E.Message);
     }
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TkabCustomDataSource::FGetFNByIdx(int AIdx)
 {
  UnicodeString RC = "";
  try
   {
    TColMap::iterator FRC = FGridColumns->find(AIdx);
    if (FRC != FGridColumns->end())
     RC = FRC->second;
    else
     throw EkabCustomDataSetError("field by index='" + IntToStr(AIdx) + "' not found.");
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
UnicodeString __fastcall TkabCustomDataSource::GetColIdByIdx(int AIdx)
 {
  return FGetFNByIdx(AIdx);
 }
//---------------------------------------------------------------------------
//Returns the TcxGridColumn.DataBinding property for a specified visible index of a column. This property is initialized when a grid column is created.
TcxGridItemDataBinding * __fastcall TkabCustomDataSource::GetDataBinding(int AItemIndex)
 {
  return ((TcxCustomGridTableItem *)DataController->GetItem(AItemIndex))->DataBinding;
 }
//---------------------------------------------------------------------------
//Returns TcxGridColumn.DataBinding as an item handle for a specified visible index of a column
void * __fastcall TkabCustomDataSource::GetItemHandle(int AItemIndex)
 {
  return (TcxDataItemHandle *)GetDataBinding(AItemIndex);
 }
//---------------------------------------------------------------------------
bool __fastcall TkabCustomDataSource::IsNativeCompare()
 {
  return false;
 }
//---------------------------------------------------------------------------
//Returns the number of records to display in a grid control
int __fastcall TkabCustomDataSource::GetRecordCount()
 {
  return FDS->RecordCount;
 }
//---------------------------------------------------------------------------
//Returns a specific field value from the TListEntry object. ARecordHandle identifies a record in the list. AItemHandle is an item (column) handle retrieved by the GetItemHandle method.
Variant __fastcall TkabCustomDataSource::GetValue(void * ARecordHandle, void * AItemHandle)
 {
  Variant RC = Variant::Empty();
  try
   {
    int FRecId = (int)ARecordHandle;
    int FFlIdx = int(((TcxGridItemDataBinding *)AItemHandle)->Data);
    UnicodeString FFlId = FGetFNByIdx(FFlIdx);
    TTagNode * FFlDefNode = FDS->DefNode->GetTagByUID(FFlId);
    if (FFlDefNode)
     {
      if (FFlDefNode->CmpAV("inlist", "l") && !FDS->Row[FRecId]->ListFetched || !FFlDefNode->CmpAV("inlist", "l") && !FDS->Row[FRecId]->Fetched)
       FDS->GetRowData(FRecId);

      if (FFlDefNode->CmpName("binary,choice,choiceDB,extedit,choicetree"))
       RC = FDS->Row[FRecId]->StrValue[FFlId];
      else
       RC = FDS->Row[FRecId]->Value[FFlId];
     }
    else
     RC = FDS->Row[FRecId]->Value[FFlId];
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSource::SetValue(void * ARecordHandle, void * AItemHandle, const Variant & AValue)
 {
  FDS->Row[(int)ARecordHandle]->Value[FGetFNByIdx(int(((TcxGridItemDataBinding *)AItemHandle)->Data))] = AValue;
 }
//---------------------------------------------------------------------------
//Since the TkabCustomDataSource provides the GetInfoForCompare procedure, the IsNativeCompare function should return True.
void __fastcall TkabCustomDataSource::CreateColumns(TcxGridTableView * AView, UnicodeString  AColDef)
 {
  if (AColDef.Length())
   {
    TStringList * FColDef = new TStringList;
    try
     {
      TcxGridColumn * tmpCol;
      FColDef->Text = AColDef;
      AView->ClearItems();
      FGridColumns->clear();
      UnicodeString FCode, FCapt;
      for (int i = 0; i < FColDef->Count; i++)
       {
        tmpCol                    = AView->CreateColumn();
        tmpCol->DataBinding->Data = (TObject *)i;
        FCapt                     = "";
        FCode                     = FColDef->Strings[i];
        if (FCode.Pos(":"))
         {
          FCapt = GetRPartB(FCode, ':');
          FCode = GetLPartB(FCode, ':');
         }
        tmpCol->DataBinding->ValueTypeClass = FGetValueTypeClass(FCode);
        (*FGridColumns)[i] = FCode;
        tmpCol->Caption = FCapt;
       }
     }
    __finally
     {
      delete FColDef;
     }
   }
  else if (FDS->ListDefNode)
   {
    AView->ClearItems();
    TTagNode * itNode = FDS->ListDefNode->GetFirstChild();
    TcxGridColumn * tmpCol;
    int i = 0;
    while (itNode)
     {
      tmpCol                              = AView->CreateColumn();
      tmpCol->DataBinding->Data           = (TObject *)i;
      tmpCol->Caption                     = itNode->AV["name"];
      tmpCol->DataBinding->ValueTypeClass = FGetValueTypeClass(itNode->AV["uid"]);
      (*FGridColumns)[i] = itNode->AV["uid"];
      i++ ;
      itNode = itNode->GetNext();
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSource::SetColumns(TcxGridTableView * AView, UnicodeString  AColDef)
 {
  TStringList * FColDef = new TStringList;
  try
   {
    FColDef->Text = AColDef;
    FGridColumns->clear();
    UnicodeString FCode, FCapt;
    for (int i = 0; i < AView->ColumnCount; i++)
     {
      FCapt = "";
      FCode = FColDef->Strings[i];
      if (FCode.Pos(":"))
       {
        FCapt = GetRPartB(FCode, ':');
        FCode = GetLPartB(FCode, ':');
       }
      AView->Columns[i]->DataBinding->Data = (TObject *)i;
      AView->Columns[i]->DataBinding->ValueTypeClass = FGetValueTypeClass(FCode);
      (*FGridColumns)[i] = FCode;
      AView->Columns[i]->Caption = FCapt;
     }
   }
  __finally
   {
    delete FColDef;
   }
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSource::CreateColumns(TcxGridCardView * AView, UnicodeString  AColDef)
 {
  if (AColDef.Length())
   {
    TStringList * FColDef = new TStringList;
    try
     {
      TcxGridCardViewRow * tmpCol;
      FColDef->Text = AColDef;
      AView->ClearItems();
      FGridColumns->clear();
      UnicodeString FCode, FCapt;
      for (int i = 0; i < FColDef->Count; i++)
       {
        FCapt = "";
        FCode = FColDef->Strings[i];
        if (FCode.Pos(":"))
         {
          FCapt = GetRPartB(FCode, ':');
          FCode = GetLPartB(FCode, ':');
         }
        tmpCol = AView->CreateRow();
        tmpCol->DataBinding->Data           = (TObject *)i;
        tmpCol->DataBinding->ValueTypeClass = FGetValueTypeClass(FCode);
        (*FGridColumns)[i] = FCode;
        tmpCol->Caption = FCapt;
       }
     }
    __finally
     {
      delete FColDef;
     }
   }
  else if (FDS->ListDefNode)
   {
    AView->ClearItems();
    TTagNode * itNode = FDS->ListDefNode->GetFirstChild();
    TcxGridCardViewRow * tmpCol;
    int i = 0;
    while (itNode)
     {
      tmpCol                              = AView->CreateRow();
      tmpCol->DataBinding->Data           = (TObject *)i;
      tmpCol->DataBinding->ValueTypeClass = FGetValueTypeClass(itNode->AV["uid"]);
      tmpCol->Caption                     = itNode->AV["name"];
      (*FGridColumns)[i] = itNode->AV["uid"];
      i++ ;
      itNode = itNode->GetNext();
     }
   }
 }
//---------------------------------------------------------------------------
void __fastcall TkabCustomDataSource::SetColumns(TcxGridCardView * AView, UnicodeString  AColDef)
 {
  TStringList * FColDef = new TStringList;
  try
   {
    FColDef->Text = AColDef;
    FGridColumns->clear();
    UnicodeString FCode, FCapt;
    for (int i = 0; i < AView->RowCount; i++)
     {
      FCapt = "";
      FCode = FColDef->Strings[i];
      if (FCode.Pos(":"))
       {
        FCapt = GetRPartB(FCode, ':');
        FCode = GetLPartB(FCode, ':');
       }
      AView->Rows[i]->DataBinding->Data = (TObject *)i;
      AView->Rows[i]->DataBinding->ValueTypeClass = FGetValueTypeClass(FCode);
      (*FGridColumns)[i] = FCode;
      AView->Rows[i]->Caption = FCapt;
     }
   }
  __finally
   {
    delete FColDef;
   }
 }
//---------------------------------------------------------------------------
Cxdatastorage::TcxValueTypeClass __fastcall TkabCustomDataSource::FGetValueTypeClass(UnicodeString  AColDef)
 {
  TcxValueTypeClass RC = __classid(TcxVariantValueType);
  try
   {
    TTagNode * FFlDefNode = FDS->DefNode->GetTagByUID(AColDef.Trim());
    if (FFlDefNode)
     {
      if (FFlDefNode->CmpName("date"))
       RC = __classid(TcxDateTimeValueType);
      else if (FFlDefNode->CmpName("digit"))
       RC = __classid(TcxFloatValueType);
      else if (FFlDefNode->CmpName("text,choice,choiceDB,extedit,choicetree"))
       RC = __classid(TcxStringValueType);
     }
    /*
     ftString, ftMemo, ftFmtMemo, ftFixedChar:
     Result := TcxStringValueType;
     ftInteger, ftAutoInc:
     Result := TcxIntegerValueType;
     ftBoolean:
     Result := TcxBooleanValueType;
     ftCurrency, ftFloat:
     Result := TcxFloatValueType;
     ftDate, ftTime, ftDateTime:
     Result := ;

     Result := TcxVariantValueType;
     end;
     end;
     end;
     */
   }
  __finally
   {
   }
  return RC;
 }
//---------------------------------------------------------------------------
