//---------------------------------------------------------------------------
#ifndef kabCustomDSTypeDefH
#define kabCustomDSTypeDefH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include "System.JSON.hpp"
//#include "XMLContainer.h"
//#include "dsRegExtFilterSetTypes.h"

//---------------------------------------------------------------------------
typedef __int64       __fastcall (__closure *TkabCDSGetCountEvent)  (UnicodeString AId, UnicodeString ATTH, System::UnicodeString AFilterParam);
typedef TJSONObject*  __fastcall (__closure *TkabCDSGetCodesEvent)  (UnicodeString AId, int AMax, UnicodeString ATTH, System::UnicodeString AFilterParam);
typedef TJSONObject*  __fastcall (__closure *TkabCDSGetValByIdEvent)(UnicodeString AId, UnicodeString ARecId);
typedef TJSONObject*  __fastcall (__closure *TkabCDSGetValById10Event)(UnicodeString AId, UnicodeString ARecId);
typedef void          __fastcall (__closure *TkabCDSProgressEvent)  (int AMax, int ACur, UnicodeString AMsg);

typedef UnicodeString __fastcall (__closure *TkabCDSInsertEvent)(UnicodeString AId, TJSONObject* AValues, System::UnicodeString &ARecID);
typedef UnicodeString __fastcall (__closure *TkabCDSEditEvent)  (UnicodeString AId, TJSONObject* AValues, System::UnicodeString &ARecID);
typedef UnicodeString __fastcall (__closure *TkabCDSDeleteEvent)(UnicodeString AId, UnicodeString ARecId);

//---------------------------------------------------------------------------
#endif

