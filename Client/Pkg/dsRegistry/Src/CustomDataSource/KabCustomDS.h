//---------------------------------------------------------------------------

#ifndef KabCustomDSH
#define KabCustomDSH

//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include "cxGridTableView.hpp"
#include "cxGridCardView.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "kabCustomDSTypeDef.h"
//#include "dsRegTypeDef.h"
//#include "Variant.hpp"
//#include "icsVariant.h"
enum class TdsRegFetch { None, All, Auto};
//---------------------------------------------------------------------------
class PACKAGE EkabCustomDataSetRowError : public System::Sysutils::Exception
{
public:
	 __fastcall EkabCustomDataSetRowError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
class PACKAGE EkabCustomDataSetError : public System::Sysutils::Exception
{
public:
	 __fastcall EkabCustomDataSetError(UnicodeString Msg);
};
//---------------------------------------------------------------------------
struct PACKAGE TkabCustomDataSetRowField
{
  Variant Value;
  UnicodeString StrValue;
  UnicodeString Name;
  UnicodeString Type;
  TkabCustomDataSetRowField();
  void operator = (TkabCustomDataSetRowField AVal);
};
//---------------------------------------------------------------------------
class PACKAGE TkabCustomDataSetRow : public TObject
{
private:
  typedef map<UnicodeString, TkabCustomDataSetRowField> TkabCustomDataSetFieldsMap;
  TkabCustomDataSetFieldsMap *FDataFields;
//  TkabCustomDataSetFieldsMap *FStrDataFields;
  bool FFetched;
  bool FListFetched;
  TTagNode *FFlDef;
  void __fastcall NewRec();

  void       __fastcall FSetValue(UnicodeString AName, const Variant& AVal);
  Variant    __fastcall FGetValue(UnicodeString AName);

  void       __fastcall FSetStrValue(UnicodeString AName, const Variant &AVal);
  Variant    __fastcall FGetStrValue(UnicodeString AName);

  UnicodeString __fastcall FGetType(UnicodeString AName);

public:
  __fastcall TkabCustomDataSetRow(TTagNode *AFlDef);
  __fastcall TkabCustomDataSetRow(TTagNode *AFlDef, TkabCustomDataSetRow *ASrc);
  __fastcall ~TkabCustomDataSetRow();
  TJSONObject * __fastcall GetJSONValues();

  __property bool Fetched = {read=FFetched, write=FFetched};
  __property bool ListFetched = {read=FListFetched, write=FListFetched};
  __property UnicodeString Type [UnicodeString AName]    = {read=FGetType};
  __property Variant Value[UnicodeString AName]    = {read=FGetValue, write=FSetValue};
  __property Variant StrValue[UnicodeString AName] = {read=FGetStrValue, write=FSetStrValue};
};
//---------------------------------------------------------------------------
typedef int __fastcall (__closure * TkabCDSGetRowIdx)(int ARecIdx);
//---------------------------------------------------------------------------
class PACKAGE TkabCustomDataSet : public TObject
{
private:
  typedef map <UnicodeString, int> TkabCustomDataSetIntMap;
  TTagNode *FDefNode;
  TTagNode *FDefListNode;
  TList *FRows;
  TkabCustomDataSetIntMap *FCodeIdx;
  TkabCustomDataSetIntMap *FNewCodeIdx;
  TkabCustomDataSetRow* FEditedRow;

  TDataSetState          FDSState;

  UnicodeString          FTTH;
  UnicodeString          FDSId;
  UnicodeString          FDefUID;

  UnicodeString          FDSIdPref;
  void __fastcall FSetDSIdPref(UnicodeString AVal);

  TkabCDSGetCountEvent   FOnGetCount;
  TkabCDSGetCodesEvent   FOnGetIdList;
  TkabCDSGetValByIdEvent FOnGetValById;
  TkabCDSGetValById10Event FOnGetValById10;

  TkabCDSInsertEvent     FOnInsert;
  TkabCDSEditEvent       FOnEdit;
  TkabCDSDeleteEvent     FOnDelete;
  TkabCDSGetRowIdx       FOnGetRowIdx;


  bool __fastcall FGetRowData(TkabCustomDataSetRow *ARow, TJSONObject *ARowData, bool AListOnly = false);

  bool __fastcall FSetFieldsDef(TTagNode *ANode, UnicodeString &ATmp);
//  bool __fastcall FCreateFieldsDef(TTagNode *ANode, void *Src1 = NULL);

  TkabCustomDataSetRow*  __fastcall FGetCurrRow();

  __int64 __fastcall FExtGetCount(System::UnicodeString AFilterParam);
  bool __fastcall FExtGetIdList(System::UnicodeString AFilterParam, TkabCDSProgressEvent AProgress = 0);
  bool __fastcall FCreateRecByIdList(TJSONArray *AIdList, TkabCDSProgressEvent AProgress = 0);
  void __fastcall FGetRecData10(TdsRegFetch AFetchAll, int AFetchRecCount = 20, TkabCDSProgressEvent AProgress = 0);

  int __fastcall FGetCount();
  void __fastcall FShowProgress(TkabCDSProgressEvent AProgress, int AMax, int ACur, UnicodeString AMsg);

  TkabCustomDataSetRow* __fastcall FGetRow(int AIdx);
  TkabCustomDataSetRow* __fastcall Add();

public:
  __fastcall TkabCustomDataSet(TTagNode *ADefNode);
  __fastcall ~TkabCustomDataSet();

  void __fastcall Clear();

  __property UnicodeString Id = {read=FDSId};
  __property int RecordCount = {read=FGetCount};
  __property TkabCustomDataSetRow* Row[int AIdx] = {read=FGetRow};
  TJSONObject* __fastcall GetChDBData(UnicodeString ARef, UnicodeString AVal);
  void __fastcall GetData(System::UnicodeString AFilterParam, TdsRegFetch AFetchAll = TdsRegFetch::None, int AFetchRecCount = 20, TkabCDSProgressEvent AProgress = 0);
  void __fastcall FilterByFind(TJSONArray *AIdList, TdsRegFetch AFetchAll, int AFetchRecCount = 20, TkabCDSProgressEvent AProgress = 0);
  void __fastcall GetData10(System::UnicodeString AFilterParam, TdsRegFetch AFetchAll, int AFetchRecCount, TkabCDSProgressEvent AProgress = 0);
  void __fastcall FetchAll(int AFetchRecCount = 20, TkabCDSProgressEvent AProgress = 0);
  bool __fastcall IsAllFetched();
  bool __fastcall GetDataById(UnicodeString ACode, TkabCDSProgressEvent AProgress = 0);
  void __fastcall GetRowData(int ARowId, TkabCDSProgressEvent AProgress = 0);
  void __fastcall GetRowData10(__int64 ALo, __int64 AHi, TkabCDSProgressEvent AProgress = 0);
  static TTagNode* __fastcall CreateDefNode(TTagNode *ADefNode);

  void          __fastcall Insert();
  void          __fastcall Edit();
  void          __fastcall Delete(int ARecIdx = -1);
  UnicodeString __fastcall Post(UnicodeString &ARetCode, bool ACheck = false);
  void          __fastcall Cancel();
  __int64 __fastcall GetRowIdxByCode(UnicodeString ACode);

  __int64 __fastcall GetCount();


  __property TTagNode*              DefNode     = {read=FDefNode};
  __property TTagNode*              ListDefNode = {read=FDefListNode};
  __property TkabCustomDataSetRow*  EditedRow   = {read=FEditedRow};
  __property TkabCustomDataSetRow*  CurRow      = {read=FGetCurrRow};
  __property TDataSetState          State       = {read=FDSState};

  __property UnicodeString IdPref = {read=FDSIdPref, write=FSetDSIdPref};

  __property TkabCDSGetRowIdx       OnGetRowIdx  = {read=FOnGetRowIdx,  write=FOnGetRowIdx};

  __property TkabCDSGetCountEvent   OnGetCount   = {read=FOnGetCount,   write=FOnGetCount};
  __property TkabCDSGetCodesEvent   OnGetIdList  = {read=FOnGetIdList,  write=FOnGetIdList};
  __property TkabCDSGetValByIdEvent OnGetValById = {read=FOnGetValById, write=FOnGetValById};
  __property TkabCDSGetValById10Event OnGetValById10 = {read=FOnGetValById10, write=FOnGetValById10};

  __property TkabCDSInsertEvent     OnInsert     = {read=FOnInsert,     write=FOnInsert};
  __property TkabCDSEditEvent       OnEdit       = {read=FOnEdit,       write=FOnEdit};
  __property TkabCDSDeleteEvent     OnDelete     = {read=FOnDelete,     write=FOnDelete};

};
//---------------------------------------------------------------------------
class PACKAGE TkabCustomDataSource  : public TcxCustomDataSource
{
private:
  TkabCustomDataSet *FDS;

  typedef map<int, UnicodeString> TColMap;
  TColMap   *FGridColumns;

  TcxGridItemDataBinding* __fastcall GetDataBinding(int AItemIndex);
  UnicodeString           __fastcall FGetFNByIdx(int AIdx);
  int                     __fastcall FGetCurrentRecIdx(int ARecIdx = -1);
protected:
  void*   __fastcall GetItemHandle(int AItemIndex);
  int     __fastcall GetRecordCount();
  Variant __fastcall GetValue(void * ARecordHandle, void * AItemHandle);
  void    __fastcall SetValue(void * ARecordHandle, void * AItemHandle, const Variant &AValue);
  bool    __fastcall IsNativeCompare();

//virtual void * __fastcall AppendRecord(void);
//virtual void * __fastcall InsertRecord(void * ARecordHandle);
//virtual void   __fastcall DeleteRecord(void * ARecordHandle);

//virtual void __fastcall CustomSort(void);
//virtual int __fastcall GetDefaultItemID(int AItemIndex);
//virtual bool __fastcall GetDetailHasChildren(int ARecordIndex, int ARelationIndex);
//virtual System::UnicodeString __fastcall GetDisplayText(void * ARecordHandle, void * AItemHandle);
//virtual System::Variant __fastcall GetRecordId(void * ARecordHandle);
//virtual void * __fastcall GetRecordHandle(int ARecordIndex);
//virtual bool __fastcall IsCustomSorting(void);
//virtual bool __fastcall IsMultiThreadingSupported(void);
//virtual bool __fastcall IsNativeCompareFunc(void);
//virtual bool __fastcall IsRecordIdSupported(void);
//virtual void __fastcall LoadRecordHandles(void);
//virtual int __fastcall NativeCompareFunc(void * ARecordHandle1, void * ARecordHandle2, void * AItemHandle);
//virtual void __fastcall SetRecordCount(int ARecordCount);
//__property TcxCustomDataProvider* CurrentProvider = {read=FCurrentProvider, write=FCurrentProvider};
//int __fastcall AddRecordHandle(void * ARecordHandle);

  Cxdatastorage::TcxValueTypeClass __fastcall TkabCustomDataSource::FGetValueTypeClass(UnicodeString AColDef);

public:
  __fastcall TkabCustomDataSource(TTagNode *ADefNode);
  __fastcall ~TkabCustomDataSource();

//virtual void __fastcall DataChanged(void);
//virtual void * __fastcall GetRecordHandleByIndex(int ARecordIndex);
//int __fastcall GetRecordIndexByHandle(void * ARecordHandle);
//__property TcxCustomDataController* DataController = {read=GetDataController};
//__property TcxCustomDataProvider* Provider = {read=GetProvider};

  UnicodeString __fastcall GetColIdByIdx(int AIdx);

  void __fastcall CreateColumns(TcxGridTableView *AView, UnicodeString AColDef = "");
  void __fastcall SetColumns(TcxGridTableView *AView, UnicodeString AColDef);

  void __fastcall CreateColumns(TcxGridCardView *AView, UnicodeString AColDef = "");
  void __fastcall SetColumns(TcxGridCardView *AView, UnicodeString AColDef);

  __property TkabCustomDataSet* DataSet = {read=FDS};
  __property int CurrentRecIdx = {read=FGetCurrentRecIdx};
};
//---------------------------------------------------------------------------
#endif

