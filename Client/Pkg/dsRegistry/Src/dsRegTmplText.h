﻿//---------------------------------------------------------------------------

#ifndef dsRegTmplTextH
#define dsRegTmplTextH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>

//#include <DBCtrls.hpp>
//#include "RXDBCtrl.hpp"
//#include <Registry.hpp>
//#define F_DATE 1
//#define F_DATETIME 2
//#define F_TIME 3

//---------------------------------------------------------------------------
#include "dsRegEDContainer.h"
//---------------------------------------------------------------------------
class PACKAGE TriTextTmpl: public TdsRegEDItem
{
private:
  TcxButton  *FClearBtn;
    void     __fastcall cResize(TObject *Sender);
    void     __fastcall FDataChange(TObject *Sender);
    void     __fastcall FFieldCast(TObject *Sender);
    TcxMaskEdit* __fastcall FGetCastED();
    void __fastcall FSetCastED(TcxMaskEdit *AVal);
    __property TcxMaskEdit *ED = {read=FGetCastED,write=FSetCastED};
  void __fastcall FClearClick(TObject *ASender);
  void __fastcall FEDKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
public:
    __fastcall TriTextTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode, TdsRegEDContainer *ACtrlOwner);
    __fastcall ~TriTextTmpl();
    void         __fastcall SetOnDependValues();
    void         __fastcall DataChange(TObject *Sender);
    Variant  __fastcall GetCompValue(UnicodeString ARef);
    UnicodeString   __fastcall GetValueStr(bool ABinaryNames = false);
    TControl*    __fastcall GetControl(int AIdx);
    TControl*    __fastcall GetFirstEnabledControl();
    void         __fastcall SetEnable(bool AEnable);
    void         __fastcall SetEnableEx(bool AEnable);
    void         __fastcall SetIsVal(bool AEnable);
    void         __fastcall SetValue(UnicodeString AVal,bool AEnable = true);
    void         __fastcall SetLabel(UnicodeString ACaption);
    void         __fastcall SetOnEnable(TTagNode* ANode);
    void         __fastcall SetEDLeft(int ALeft);
    bool         __fastcall UpdateChanges();
    bool         __fastcall CheckReqValue(TcxPageControl *APC = NULL, bool ASilent = false);
    void         __fastcall GetEDLeft(int *ALabR, int *AEDLeft);
    UnicodeString   __fastcall GetValue(UnicodeString ARef);
    void         __fastcall SetFixedVar(UnicodeString AVals);
  void __fastcall Clear();
};
//---------------------------------------------------------------------------
#endif
