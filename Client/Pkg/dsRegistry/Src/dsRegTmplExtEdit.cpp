﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop
#include "dsRegTmplExtEdit.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxButtonEdit"
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
// ---------------------------------------------------------------------------
__fastcall TriExtEditTmpl::TriExtEditTmpl(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner)
 {
  FClearBtn           = new TcxButton(this);
  FClearBtn->Parent   = this;
  FClearBtn->OnClick  = FClearClick;
  FClearBtn->ShowHint = true;
  FClearBtn->Hint     = "Очистить поле \"" + FNode->AV["name"] + "\"";
  FClearBtn->Width    = 20;
  FClearBtn->Height   = 20;
  FClearBtn->OptionsImage->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\regclear.bmp"));
  FClearBtn->OptionsImage->NumGlyphs          = 4;
  FClearBtn->PaintStyle                       = bpsGlyph;
  FClearBtn->SpeedButtonOptions->CanBeFocused = false;
  FClearBtn->SpeedButtonOptions->Flat         = true;
  FClearBtn->TabStop                          = false;
  FClearBtn->Anchors.Clear();
  FClearBtn->Anchors << akRight;
  // Создаём метку контрола
  FLab              = new TLabel(this);
  FLab->Parent      = this;
  FLab->Transparent = true;
  FLab->Caption     = FNode->AV["name"] + ":";
  // Создаём метку для текстового значения
  FExtLab              = new TLabel(this);
  FExtLab->Parent      = this;
  FExtLab->Transparent = true;
  FExtLab->AutoSize    = false;
  FExtLab->ParentFont  = true;
  FExtLab->WordWrap    = true;
  FExtLab->Color       = clInactiveBorder;
  FEDSetValue          = false;
  FSearchMode          = FNode->CmpAV("comment", "search");
  bool FExtBitmapSet = false;
  // Создаём контрол
  if (FSearchMode)
   {
    FLb = new TcxListBox(this);
    if (AParent->Parent)
     FLb->Parent = AParent->Parent;
    else
     FLb->Parent = AParent;
    FLb->Visible   = false;
    FLb->ItemIndex = -1;
    FLb->Items->Clear();
    FLb->OnDblClick            = FSearchLBDblClick;
    FLb->OnKeyDown             = FSearchLBKeyDown;
    FLb->OnExit                = FSearchLBExit;
    ED                         = new TcxButtonEdit(this);
    ED->Style->StyleController = FCtrlOwner->StyleController;
    ED->Parent                 = this;
    ED->ShowHint               = true;
    ED->Properties->ClickKey   = 0; // ShortCut(VK_DOWN,TShiftState() << ssAlt);
    if (Required)
     ED->Style->Color = FReqColor;
    // ED->Properties->ReadOnly = true;
    // ED->Properties->ViewStyle = vsHideCursor;
    TcxEditButton * tmpBtn;
    int FBtnCount = FNode->AV["btncount"].ToIntDef(1);
    bool FSetBmp;
    UnicodeString FHint;
    TMemoryStream * BmpStm = new TMemoryStream;
    try
     {
      for (int i = 1; i < FBtnCount; i++)
       {
        tmpBtn  = ED->Properties->Buttons->Add();
        FSetBmp = false;
        FHint   = "";
        if (ACtrlOwner->OnGetExtBtnGetBitmap)
         {
          tmpBtn->Kind = bkGlyph;
          FSetBmp      = ACtrlOwner->OnGetExtBtnGetBitmap(FNode, i, tmpBtn, FHint);
          FExtBitmapSet |= FSetBmp;
         }
        tmpBtn->Hint = FHint;
        if (!FSetBmp)
         {
          tmpBtn->Kind    = bkText;
          tmpBtn->Caption = FHint;
         }
       }
     }
    __finally
     {
      delete BmpStm;
     }
    if (!FExtBitmapSet)
     {
      ED->Properties->Buttons->Items[0]->Kind = bkGlyph;
      ED->Properties->Buttons->Items[0]->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\find.bmp"));
     }
    // tmpBtn->Kind = bkText;
    // tmpBtn->Caption = "X";
    // ED->Properties->OnButtonClick = ExtBtnClick;
    ED->OnKeyDown = FEDKeyDown;
    ED->Properties->OnButtonClick = FEDBtnClick;
    ED->Properties->OnChange      = FEDChanged;
    // ED->Text                               = //!!!codeToText(adres.AsString(),"11111110");
    FEDModify = false;
    FFindedObjects.clear();
   }
  else
   {
    ED                        = new TcxButtonEdit(this);
    ED->Parent                = this;
    ED->ShowHint              = true;
    ED->Properties->ClickKey  = ShortCut(VK_DOWN, TShiftState() << ssAlt);
    ED->Properties->ReadOnly  = true;
    ED->Properties->ViewStyle = vsHideCursor;
    TcxEditButton * tmpBtn;
    int FBtnCount = FNode->AV["btncount"].ToIntDef(1);
    bool FSetBmp;
    if (Required)
     ED->Style->Color = FReqColor;
    UnicodeString FHint;
    TMemoryStream * BmpStm = new TMemoryStream;
    try
     {
      for (int i = 1; i < FBtnCount; i++)
       {
        tmpBtn  = ED->Properties->Buttons->Add();
        FSetBmp = false;
        FHint   = "";
        if (ACtrlOwner->OnGetExtBtnGetBitmap)
         {
          tmpBtn->Kind = bkGlyph;
          FSetBmp      = ACtrlOwner->OnGetExtBtnGetBitmap(FNode, i, tmpBtn, FHint);
          FExtBitmapSet |= FSetBmp;
         }
        tmpBtn->Hint = FHint;
        if (!FSetBmp)
         {
          tmpBtn->Kind    = bkText;
          tmpBtn->Caption = FHint;
         }
       }
     }
    __finally
     {
      delete BmpStm;
     }
    if (!FExtBitmapSet)
     {
      ED->Properties->Buttons->Items[0]->Kind = bkGlyph;
      ED->Properties->Buttons->Items[0]->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\book.bmp"));
     }
    // tmpBtn = ED->Properties->Buttons->Add();
    // tmpBtn->Kind = bkText;
    // tmpBtn->Caption = "X";
    ED->Properties->OnButtonClick = ExtBtnClick;
   }
  // прочие настройки
  // if (Required) ED->Style->Color = FReqColor;
  FLab->FocusControl = ED;
  // расположение и размеры
  FLab->Top  = 2;
  FLab->Left = 2;
  if (FLab->Width > (AParent->Width - 2))
   {
    FLab->Font->Size = FLab->Font->Size - 1;
    FLab->Update();
    if (FLab->Width > (AParent->Width - 2))
     FLab->Width = AParent->Width - 2;
   }
  ED->Constraints->MinWidth = 60;
  if (FNode->AV["linecount"].ToIntDef(0) < 2)
   {
    ED->Left = FLab->Left + FLab->Width + 5;
    ED->Top  = 0;
   }
  else
   {
    ED->Left = 2;
    ED->Top  = FLab->Height + 2;
    TwoLine  = true;
   }
  ED->Width = AParent->Width - 4 - ED->Left;
  if ((ED->Left + ED->Width) > (AParent->Width - 4))
   {
    TwoLine   = true;
    ED->Left  = 4;
    ED->Top   = FLab->Height;
    ED->Width = AParent->Width - 4 - ED->Left;
   }
  FExtLab->Left = FLab->Left;
  FExtLab->Width  = Width - 4;
  FExtLab->Height = (FExtLab->Height - 2) * (FNode->AV["linecount"].ToIntDef(1) - 1);
  FExtLab->Top    = ED->Top + ED->Height;
  CMaxH           = FExtLab->Top + FExtLab->Height + 2;
  CMinH           = CMaxH;
  Height          = CMaxH;
  if (!FCtrlOwner->TemplateField && FCtrlOwner->IsTemplate(_UID_))
   {
    FSetEditValue(VarToStr(FCtrlOwner->TemplateData->Value[_UID_]));
   }
  if (FNode->GetAVDef("depend").Length() && EDCtrlExists(_GUI(FNode->AV["depend"])))
   {
    UnicodeString DepUID = _GUI(FNode->AV["depend"]);
    TTagNode * DepNode = FCtrlOwner->GetNode(DepUID);
    UnicodeString DepVal = GetEDCtrl(_GUI(FNode->AV["depend"]))->GetValue("");
    SetDependVal(DepNode->AV["ref"], DepVal.ToIntDef(-1), DepNode->AV["uid"]);
   }
  FClearBtn->Top = ED->Top + ED->Height / 2 - 10;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::FSearchLBDblClick(TObject * Sender)
 {
  if (FLb->ItemIndex != -1)
   {
    UnicodeString SrcCode = FSetEditValue(FFindedObjects[FLb->Items->Strings[FLb->ItemIndex].UpperCase()] + "=" +
      FLb->Items->Strings[FLb->ItemIndex]);
    // !!!     FSetFindedAddr(FFindedObjects[lb->Items->Strings[lb->ItemIndex].UpperCase()]);
    FFindedObjects.clear();
    FEDModify    = false;
    FLb->Visible = false;
    if (ED->Visible)
     ED->SetFocus();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::FSearchLBKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_RETURN)
   {
    FSearchLBDblClick((TcxListBox *) Sender);
    Key = 0;
   }
  else if (Key == VK_ESCAPE)
   {
    FFindedObjects.clear();
    ((TcxListBox *)Sender)->Visible = false;
    if (ED->Visible)
     ED->SetFocus();
    FEDModify = true;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::FSearchLBExit(TObject * Sender)
 {
  FLb->Visible = false;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::FEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if (Key == VK_RETURN) // (Key == 'F') && Shift.Contains(ssCtrl)
   {
    if (FEDModify)
     {
      FEDBtnClick(NULL, 0);
      Key = 0;
     }
   }
  else if ((Key == VK_DELETE) && Shift.Contains(ssCtrl) && ED->Enabled)
   {
    FClearClick(FClearBtn);
    Key = 0;
   }
  if (Key && FCtrlOwner->OnKeyDown)
   FCtrlOwner->OnKeyDown(Sender, Key, Shift);
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::FEDBtnClick(TObject * Sender, int AButtonIndex)
 {
  if (AButtonIndex == 1)
   { // очистка редактора
    FClearClick(FClearBtn);
    FEDModify = false;
   }
  else
   {
    FSearchData(ED->Text.Trim(), AButtonIndex);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::FSearchData(UnicodeString AData, int AButtonIndex)
 {
  if (FCtrlOwner->OnExtBtnClick && AData.Length())
   {
    UnicodeString SrcCode = AData;
    if (FCtrlOwner->OnExtBtnClick(FNode, SrcCode, NULL, true, (TObject *)FCtrlOwner, AButtonIndex))
     {
      TStringList * FDataList = new TStringList;
      try
       {
        FLb->Items->Clear();
        FDataList->Text = SrcCode;
        // раскидываем по контролам
        if (AData.LowerCase().Pos("findbycode"))
         {
          if (FDataList->Count == 1)
           FSetEditValue(FDataList->Strings[0]);
          FEDModify = false;
         }
        else
         {
          if (FDataList->Count == 1)
           {
            SrcCode   = FSetEditValue(FDataList->Strings[0]);
            FEDModify = false;
           }
          else if (FDataList->Count > 1)
           {
            for (int i = 0; i < FDataList->Count; i++)
             {
              FFindedObjects[FDataList->Values[FDataList->Names[i]].UpperCase()] = FDataList->Names[i];
              FLb->Items->Add(FDataList->Values[FDataList->Names[i]]);
             }
            FLb->Left = Parent->Left + Left + ED->Left;
            FLb->Top     = Parent->Top + Top + ED->Top + ED->Height + 1;
            if (FSearchMode)
             FLb->Width = ED->Width * 1.5;
            else
             FLb->Width = ED->Width;
            FLb->Visible = true;
            FLb->BringToFront();
            if (FLb->Visible)
             FLb->SetFocus();
            FLb->ItemIndex = 0;
           }
          else
           {
            FEDModify = false;
            if (ED->Visible)
             ED->SetFocus();
           }
         }
       }
      __finally
       {
        delete FDataList;
       }
     }
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::FEDChanged(TObject * Sender)
 {
  FEDModify = true;
  if (!ED->Text.Trim().Length())
   {
    FExtLab->Caption = "";
    DataChange(this);
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::Clear()
 {
  FClearClick(FClearBtn);
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::FClearClick(TObject * ASender)
 {
  FEDSetValue                            = false;
  ED->Text                               = "";
  FExtLab->Caption                       = "";
  FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
  DataChange(ED);
 }
// ---------------------------------------------------------------------------
TcxButtonEdit * __fastcall TriExtEditTmpl::FGetCastED()
 {
  return (TcxButtonEdit *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::FSetCastED(TcxButtonEdit * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetOnDependValues()
 {
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriExtEditTmpl::FSetEditValue(UnicodeString AVal)
 {
  UnicodeString FCode = "";
  UnicodeString FText = "";
  try
   {
    int ind = AVal.Pos("=");
    if (ind)
     {
      FText = AVal.SubString(ind + 1, AVal.Length() - ind);
      FCode = AVal.SubString(1, ind - 1);
     }
    else
     FCode = AVal;
    /*
     UnicodeString FCode = AVal;
     UnicodeString FText = "";
     try
     {
     int ind = FCode.Pos("=");
     if (!ind)
     {
     if (FCtrlOwner->OnExtBtnClick && FCode.Length())
     {
     if (!FCtrlOwner->OnExtBtnClick(FNode, FCode, NULL, false, (TObject*) FCtrlOwner, 0))
     FCode = "";
     ind = FCode.Pos("=");
     }
     }
     if (ind)
     {
     FText = FCode.SubString(ind+1, FCode.Length()-ind);
     FCode = FCode.SubString(1, ind-1);
     }

     */
    if (FNode->AV["showtype"].ToIntDef(0) && !FSearchMode)
     { // 1 - в редакторе код
      ED->Text         = FCode;
      FExtLab->Caption = FText;
     }
    else
     { // 0 - в редакторе текст
      ED->Text         = FText;
      FExtLab->Caption = FCode;
     }
    FEDSetValue = ED->Text.Length();
    FCtrlOwner->TemplateData->Value[_UID_] = FCode;
    DataChange(this);
   }
  __finally
   {
   }
  return FCode;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::ExtBtnClick(TObject * Sender, int AButtonIndex)
 {
  UnicodeString SrcCode = "";
  if (!AButtonIndex)
   {
    if (!FCtrlOwner->OnExtBtnClick)
     ShowMessage("не задан OnExtBtnClick");
    if (FCtrlOwner->OnExtBtnClick)
     {
      SrcCode = FCtrlOwner->GetCtrlData(FNode, _ES_);
      if (FCtrlOwner->OnExtBtnClick(FNode, SrcCode, NULL /* FSrc */ , true, (TObject *) FCtrlOwner, AButtonIndex))
       {
        SrcCode                                = FSetEditValue(SrcCode);
        FCtrlOwner->TemplateData->Value[_UID_] = SrcCode;
       }
     }
   }
  else
   {
    // if (SrcDSEditable())
    // {
    FEDSetValue                            = false;
    ED->Text                               = "";
    FExtLab->Caption                       = "";
    SrcCode                                = "";
    FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
    // }
   }
 }
// ---------------------------------------------------------------------------
__fastcall TriExtEditTmpl::~TriExtEditTmpl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Properties->OnButtonClick = NULL;
  delete FExtLab;
  delete ED;
  if (FSearchMode)
   delete FLb;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign = true;
  if (!TwoLine)
   ED->Left = ALeft;
  FExtLab->Width = ED->Left + ED->Width;
  Width          = ED->Left + ED->Width + 2;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!TwoLine && ED)
   * AEDLeft = ED->Left;
  else
   * AEDLeft = -1;
  if (!TwoLine && FLab)
   * ALabR = FLab->Left + FLab->Width;
  else
   * ALabR = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!IsAlign && ED)
   {
    FExtLab->Left  = 2;
    FExtLab->Width = Width - 4;
    if (TwoLine)
     {
      ED->Left  = 2;
      ED->Width = Width - 4 - _TMPL_CLEAR_WIDTH_;
     }
    else
     ED->Left = Width - ED->Width - 2 - _TMPL_CLEAR_WIDTH_;
    FClearBtn->Left = ED->Left + ED->Width + 2;
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriExtEditTmpl::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return !FCtrlOwner->IsTemplate(_UID_);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriExtEditTmpl::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString xVal = "";
  if (FNode->AV["showtype"].ToIntDef(0) && !FSearchMode)
   xVal = FExtLab->Caption;
  else
   xVal = ED->Text;
  return xVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled          = AEnable;
  FLab->Enabled      = isEnabled && isEdit;
  ED->Enabled        = FLab->Enabled;
  FExtLab->Enabled   = FLab->Enabled;
  FClearBtn->Enabled = ED->Enabled;
  if (isEdit && !ED->Enabled)
   {
    FSetEditValue("");
    FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetEnableEx(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled          = AEnable;
  FLab->Enabled      = isEnabled && isEdit;
  ED->Enabled        = FLab->Enabled;
  FExtLab->Enabled   = FLab->Enabled;
  FClearBtn->Enabled = ED->Enabled;
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnable(AEnable);
  if (AVal.LowerCase().Pos("findbycode"))
   FSearchData(AVal, 0);
  else
   FSetEditValue(AVal);
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ACaption.Length())
   FLab->Caption = ACaption + ":";
  else
   FLab->Caption = ""; // FNode->AV["name"]+":";
  FLab->Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnList->Add(ANode);
  DataChange(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriExtEditTmpl::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  // отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  if (FGetDataChange)
   {
    UnicodeString Src = FCtrlOwner->GetCtrlData(FNode, _ES_);
    FGetDataChange(FNode, Src);
   }
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriExtEditTmpl::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TControl *)ED;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriExtEditTmpl::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (ED->Enabled && isEdit) ? ((TControl *)ED) : NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriExtEditTmpl::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (FNode->AV["showtype"].ToIntDef(0) && !FSearchMode)
   return ED->Text;
  else
   return FExtLab->Caption;
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriExtEditTmpl::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (GetValue("").Trim().Length())
   return GetValue("");
  else
   return Variant("NULL");
 }
// ---------------------------------------------------------------------------
bool __fastcall TriExtEditTmpl::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  bool RC = true;
  if (Required)
   {
    try
     {
      if (ED)
       {
        if (ED->Enabled)
         {
          if (FSearchMode)
           RC = FEDSetValue;
          else
           RC = ED->Text.Length();
         }
       }
     }
    __finally
     {
      if (!RC)
       {
        if (APC)
         APC->ActivePageIndex = this->Tag;
        if (!ASilent)
         {
          UnicodeString ErrMessage;
          if (FLab)
           ErrMessage = FLab->Caption;
          else
           ErrMessage = FNode->AV["name"];
          ErrMessage = FMT1(icsRegItemErrorReqField, ErrMessage);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
        if (ED->Visible && Visible)
         ED->SetFocus();
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
