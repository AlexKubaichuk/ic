#ifndef dsRegConstDefHPP
#define dsRegConstDefHPP

#include "fmtdef.h"
// Common
#define icsErrorMsgCaption "������"
#define icsErrorMsgCaptionSys "������.\r\n��������� ���������:\r\n%s"
//#define   icsMessageMsgCaption "���������"
#define   icsInputErrorMsgCaption "������ �����"
//#define   icsRecCount1       "������"
//#define   icsRecCount2       "������"
//#define   icsRecCount3       "�������"
//#define   icsCommonRecCount  "�����:"
//#define   icsFieldNotFound   "���� \"%s\" � ������� \"%s\" �����������."
//#define   icsRegListCreate   "������������ ������"
//#define
#define   icsRegErrorInsertRec "������ ���������� ������."
#define   icsRegErrorInsertRecSys "������ ���������� ������, ��������� ���������:\r\n%s"
#define   icsRegErrorEditRec   "������ �������������� ������."
#define   icsRegErrorEditRecSys "������ �������������� ������, ��������� ���������:\r\n%s"
#define   icsRegErrorDeleteRec "������ �������� ������."
#define   icsRegErrorDeleteRecSys "������ �������� ������, ��������� ���������:\r\n%s"
//#define   icsRegErrorSearch    "������ ������; ��������:"
//#define   icsRegRecFound       "��� ����������! ������� ��������� � ��������� ������."
//#define
#define   icsRegErrorConfirmDeleteRec   "�������������"
#define   icsRegErrorConfirmDeleteRecMsgCaption   "������������� �������� ������"
//#define   icsRegTransportErrorNoRegDefXML "�� ������� xml-�������� ������������"
//#define   icsRegTransportErrorNoEIDataProvider "�� ����� ��������� ��������/������� ������"
//#define
//#define // DTFormats
#define   icsDTFormatsD      "dd.mm.yyyy �."
#define   icsDTFormatsDT     "dd.mm.yyyy hh:nn:ss"
#define   icsDTFormatsT      "hh:nn:ss"
//#define
//#define // RegEd
#define   icsRegEDErrorNoOwner   "�� �������� ��������� ��������."
#define   icsRegEDErrorNoRef     "����������� ������ �� ������."
#define   icsRegEDErrorRefObject "\"��������� ������ ������ -> \"%s\"."
//#define
//#define // RegItem common
#define   icsRegItemErrorReqField "�� ������� �������� !\r\n��� ������������� ���� \"%s\"."
//#define // RegItem binary
#define   icsriBinaryCheckTxt   "�������"
#define   icsriBinaryUnCheckTxt "�� �������"
//#define // RegItem ChoiceTreeTmpl
//#define   icsriChoiceTreeSubClass "{��������� ���������} "
//#define // RegItem Digit
#define   icsriDigitValRange "�������� ���� \"%s\" ������ ���� � ��������� ��: %s ��: %s."
//#define
//
//#define //  TdsRegEDContainer
#define   dsRegEDContainerErrorNoOwner         "�� ���!!!, �������� - Owner ������ ���� ��������."
#define   dsRegEDContainerErrorNoParent        "�� ���!!!, �������� - Parent ������ ���� ��������."
#define   dsRegEDContainerErrorNoTmpQuery      "��������� ��� ���������� ��������� �������� ������ ���� �����. ������ = (FIBTemplateQuery ��� ADOTemplateQuery = NULL)"
#define   dsRegEDContainerErrorTmpQueryTypeADO "��������� ��� ���������� ��������� �������� ������ ���� ���� TADODataSet."
#define   dsRegEDContainerErrorTmpQueryTypeFIB "��������� ��� ���������� ��������� �������� ������ ���� ���� TpFIBQuery."
#define   dsRegEDContainerErrorNoDataSrc       "�������� ������ ������ ���� �����. ������ = (DataSource = NULL)"
#define   dsRegEDContainerErrorNoDataDef       "�������� �������� �������� ������ ���� �����. ������ = (ANode = NULL)"
#define   dsRegEDContainerErrorNoDataDefId     "��� �������� �������� ������ ���� �����. ������ = (ANode->AV[\"uid\"] = \"\")"
//
#define   dsRegEDContainerErrorDSTypeADO       "��������� ��������� � ������ ������� � ������ ����� ADO - ����������."
#define   dsRegEDContainerErrorDSTypeFIB       "��������� ��������� � ������ ������� � ������ ����� FIB - ����������."
#define   dsRegEDContainerErrorEDNo            "�������� � ���=\"%s\" �����������."
#define   dsRegEDContainerErrorEDExists        "�������� � ���=\"%s\" ��� ����������."
#define   dsRegEDContainerErrorEDCreate        "������ �������� ��������� � ���=\"%s\"."
#define   dsRegEDContainerSysErrorEDCreate     "������ �������� ��������� � ���=\"%s\".\r\n��������� ���������:%s"
#define   dsRegEDContainerErrorTmplNo          "�������� ������� � ���=\"%s\" �����������."
#define   dsRegEDContainerErrorTmplExists      "�������� ������� � ���=\"%s\" ��� ����������."
#define   dsRegEDContainerErrorTmplCreate      "������ �������� ��������� ������� � ���=\"%s\"."
#define   dsRegEDContainerSysErrorTmplCreate   "������ �������� ��������� ������� � ���=\"%s\".\r\n��������� ���������:%s"
#define   dsRegEDContainerErrorLabNo           "��������� ����� � ���=\"%s\" �����������."
#define   dsRegEDContainerErrorLabExists       "��������� ����� � ���=\"%s\" ��� ����������."
#define   dsRegEDContainerErrorLabCreate       "������ �������� ��������� ����� � ���=\"%s\"."
#define   dsRegEDContainerSysErrorLabCreate    "������ �������� ��������� ����� � ���=\"%s\".\r\n��������� ���������:%s"
//#define
//#define // RegEDFunc
//#define   icsRegEDFuncErrorGetValues            "������ ��������� �������� ��������������."
//#define
//// RegEI
//#define   icsRegEIErrorClassesExport            "������ �������� ���������������."
//#define   icsRegEIErrorClassExport              "������ �������� ��������������."
//#define   icsRegEIErrorUnitsExport              "������ �������� ������ ������ �����."
//#define   icsRegEIErrorUnitExport               "������ �������� ������� �����."
//#define   icsRegEIErrorClassesImport            "������ ������� ���������������."
//#define   icsRegEIErrorClassImport              "������ ������� �������������� \"%s\"."
//#define   icsRegEIErrorLastInqDate              "���� ���������� ������������."
//#define   icsRegEIErrorUnitImport               "������ ������� ������ ��������."
//#define   icsRegEIErrorUnitsImport              "������ ������� ������ ������ �����."
//
//// TICSRegTransport
//#define   icsRegTransportErrorClassesExport            "������ �������� ���������������."
//#define   icsRegTransportErrorClassesExportSys         "������ �������� ���������������.\r\n��������� ���������:%s"
//#define   icsRegTransportErrorClassExport              "������ �������� ��������������."
//#define   icsRegTransportErrorClassExportSys           "������ �������� ��������������.\r\n��������� ���������:%s"
//#define   icsRegTransportErrorUnitsExport              "������ �������� ������ ������ �����."
//#define   icsRegTransportErrorUnitsExportSys           "������ �������� ������ ������ �����.\r\n��������� ���������:%s"
//#define   icsRegTransportErrorUnitExport               "������ �������� ������� �����."
//#define   icsRegTransportErrorUnitExportSys            "������ �������� ������� �����.\r\n��������� ���������:%s"
//#define   icsRegTransportErrorClassesImport            "������ ������� ���������������."
//#define   icsRegTransportErrorClassesImportSys         "������ ������� ���������������.\r\n��������� ���������:%s"
//#define   icsRegTransportErrorClassImport              "������ ������� �������������� \"%s\"."
//#define   icsRegTransportErrorClassImportSys           "������ ������� �������������� \"%s\".\r\n��������� ���������:%s"
//#define   icsRegTransportErrorLastInqDate              "���� ���������� ������������."
//#define //  icsRegTransportErrorUnitImport               "������ ������� ������ ��������."
//#define   icsRegTransportErrorUnitsImport              "������ ������� ������ ������ �����."
//#define   icsRegTransportErrorUnitsImportSys           "������ ������� ������ ������ �����.\r\n��������� ���������:%s"
//#define   icsRegTransportErrorUnitsImportData          "������ ������� ������ ������ �����.\r\n������: %s"
//#define   icsRegTransportErrorUnitsImportDataSys       "������ ������� ������ ������ �����.\r\n������: %s\r\n��������� ���������:%s"
//#define   icsRegTransportErrorGetClsKey                "������ ��������� �������� ����� ��� ��������������, ��� = %s"
//#define   icsRegTransportErrorGetClsKeySys             "������ ��������� �������� ����� ��� ��������������, ��� = %s\r\n��������� ���������:%s"
//#define
//#define
//#define // TRegXMLEIDataProvider
//#define   icsRegXMLEIDataProviderRecNotFound           "������ ��������� ��������, ������ �����������."
//#define   icsRegXMLEIDataProviderIEDataNotFound        "�� ������ �������� �������� ������ ������."
//#define   icsRegXMLEIDataProviderErrorCreateSect       "������ �������� ������. ������ \"%s\" �� ��������� � ������� \"%s\"."
//#define   icsRegXMLEIDataProviderErrorSectNotFound     "����������� ������ \"%s\"."
//
//// TRegFIBEIDataProvider
//#define   icsRegFIBEIDataProviderErrorExecProcSys      "������ ���������� �������� ��������� \"%s\".\r\n��������� ���������: %s\"
//#define   icsRegFIBEIDataProviderErrorExecProc         "������ ���������� �������� ��������� \"%s\"."
//#define   icsRegFIBEIDataProviderErrorExecSQLSys       "������ ���������� SQL �������:\r\n\"%s\".\r\n��������� ���������: %s"
//#define   icsRegFIBEIDataProviderErrorExecSQL          "������ ���������� SQL �������:\r\n\"%s\"."
//#define   icsRegFIBEIDataProviderNoQueryDef            "����������� ��������� ��� ���������� �������."
//#define   icsRegFIBEIDataProviderErrorSectNotFound     "����������� ������ \"%s\"."
//#define
//#define //  icsRegXMLEIDataProviderErrorCreateSect       "������ �������� ������. ������ \"%s\" �� ��������� � ������� \"%s\"."
//#define //  icsRegXMLEIDataProviderErrorSectNotFound     "����������� ������ \"%s\"."
//
//// ICSRegistry
//#define   icsRegistryErrorDBNameNotDef          "�� ������ ��� ��."
//#define   icsRegistryErrorNoRegDef              "�� ������ �������� ������������."
//#define   icsRegistryErrorDBConnect             "������ ����������� � �� ��� �������� �������� ������������."
#define   icsRegistryErrorCommDBErrorMsgCaption "������ ������ � ��"
//#define   icsRegistryErrorDBErrorMsgCaption     "������ ������ � ��, ���: %s"
//#define   icsRegistryErrorDBNotExist            "���� ������ \"%s\" �����������!"
//#define   icsRegistryErrorNoServer              "�� ������� \"FireBird\" server."
//#define   icsRegistryErrorLoadRegDef            "������ �������� �������� ������������."
//#define   icsRegistryErrorLoadDM                "������������. ������ �������� ������ ������.\r\n��������� ���������:%s"
//
//// ICSTemplate
//#define   icsTemplateReqField   "�� ������ �������� ��� ������������� ���� \"%s\"."
//#define   icsTemplateNoClassDef "�� ������ �������� ���������������."
//#define   icsTemplateNoFltDef   "�� ������ �������� �������."
#define   icsTemplateRefError   "��� � ���=\"%s\" �����������."
#define   icsTemplateReqEmpty   "���������� ������� �������� ��� ������� ��� ������ ���������."
//
//// TITemplateForm
//#define   icsITemplateCaption   "������������ �������"
//#define   icsITemplateApplyBtn  "���������"
//#define   icsITemplateCancelBtn "������"
//#define   icsIErrTreeFormMsg    "������ ������������ ����� ��� \"%s\"."
//
//// Reg SearchUnit
//#define   icsRegSearchEndMsg           "����� ��������.\r\n������ � ������ ������?"
//
//#define   icsRegSearchCaption           "�����"
//#define   icsRegSearchSettingBtnCaption "���������"
//#define   icsRegSearchFindBtnCaption    "�����"
//#define   icsRegSearchCancelBtnCaption  "������"
//#define   icsRegSearchNotFound          "���������� �� �������."
//
//// Reg SearchUnit Setting
//#define   icsRegSearchSettingCaption             "��������� ������"
//#define   icsRegSearchSettingFieldListLabCaption "������ ����� ��� ������:"
//#define   icsRegSearchSettingSaveBtnCaption      "���������"
//#define   icsRegSearchSettingCancelBtnCaption    "������"
//
//// RegSelList
//#define   icsRegSelListSelectAllCaption      "������� ���"
//#define   icsRegSelListSelectAllHint         "������� ��� ..."
//#define   icsRegSelListSelectAllShortCut     "Ins"
//
//#define   icsRegSelListUnSelectAllCaption    "����� ���������"
//#define   icsRegSelListUnSelectAllHint       "����� ��������� ..."
//#define   icsRegSelListUnSelectAllShortCut   "Del"
//
//#define   icsRegSelListApplyCaption          "���������"
//#define   icsRegSelListApplyHint             "��������� ..."
//#define   icsRegSelListApplyShortCut         "Shift+Enter"
//
//#define   icsRegSelListSetTemplateCaption    "���������� ������"
//#define   icsRegSelListSetTemplateHint       "���������� ������ ..."
//#define   icsRegSelListSetTemplateShortCut   "Ctrl+T"
//
//#define   icsRegSelListReSetTemplateCaption  "����� ������"
//#define   icsRegSelListReSetTemplateHint     "����� ������ ..."
//#define   icsRegSelListReSetTemplateShortCut "Ctrl+G"
//
//// RegList
//#define   icsRegListInsertCaption      "��������"
//#define   icsRegListInsertHint         "�������� ..."
//#define   icsRegListInsertShortCut     "Ins"
//
//#define   icsRegListEditCaption      "��������"
//#define   icsRegListEditHint         "�������� ..."
//#define   icsRegListEditShortCut     "Shift+Enter"
//
//#define   icsRegListDeleteCaption      "�������"
//#define   icsRegListDeleteHint         "������� ..."
//#define   icsRegListDeleteShortCut     "Del"
//
//#define   icsRegListSetTemplateCaption      "���������� ������"
//#define   icsRegListSetTemplateHint         "���������� ������ ..."
//#define   icsRegListSetTemplateShortCut     "Ctrl+T"
//
//#define   icsRegListReSetTemplateCaption      "����� ������"
//#define   icsRegListReSetTemplateHint         "����� ������ ..."
//#define   icsRegListReSetTemplateShortCut     "Ctrl+G"
//
//#define   icsRegListOpenCaption      "������� �����"
//#define   icsRegListOpenHint         "������� ����� ..."
//#define   icsRegListOpenShortCut     "Ctrl+Enter"
//
//#define   icsRegListExtCaption      "� �����"
//#define   icsRegListExtHint         "� ����� ..."
//#define   icsRegListExtShortCut     ""
//
//#define   icsRegListRefreshCaption      "��������"
//#define   icsRegListRefreshHint         "�������� ..."
//#define   icsRegListRefreshShortCut     "F5"
//
//#define   icsRegListFindCaption      "�����"
//#define   icsRegListFindHint         "����� ..."
//#define   icsRegListFindShortCut     "Ctrl+F"
//
//#define   icsRegListFindNextCaption      "����� �����"
//#define   icsRegListFindNextHint         "����� ����� ..."
//#define   icsRegListFindNextShortCut     "F3"
//
//#define   icsRegListViewUnitParamCaption      "��������� ��������"
//#define   icsRegListViewUnitParamHint         "��������� �������� ..."
//#define   icsRegListViewUnitParamShortCut     "Alt+1"
//
//#define   icsRegListViewTemplateParamCaption      "��������� �������"
//#define   icsRegListViewTemplateParamHint         "��������� ������� ..."
//#define   icsRegListViewTemplateParamShortCut     "Alt+2"
//
//#define   icsRegListPrintListBICaption  "������"
//#define   icsRegListPrintListBIHint     "������..."
//#define   icsRegListPrintListBIShortCut "Ctrl+P"
//
//// RegListEdit
//#define   icsRegListEditApplyBtnCaption "���������"
//#define   icsRegListEditCancelBtnCaption "������"
//
//// RegClassif
//#define   icsRegClassifInsertCaption      "��������"
//#define   icsRegClassifInsertHint         "�������� ..."
//#define   icsRegClassifInsertShortCut     "Ins"
//
//#define   icsRegClassifEditCaption      "��������"
//#define   icsRegClassifEditHint         "�������� ..."
//#define   icsRegClassifEditShortCut     "Shift+Enter"
//
//#define   icsRegClassifDeleteCaption      "�������"
//#define   icsRegClassifDeleteHint         "������� ..."
//#define   icsRegClassifDeleteShortCut     "Del"
//
//#define   icsRegClassifSetTemplateCaption      "���������� ������"
//#define   icsRegClassifSetTemplateHint         "���������� ������ ..."
//#define   icsRegClassifSetTemplateShortCut     "Ctrl+T"
//
//#define   icsRegClassifReSetTemplateCaption      "����� ������"
//#define   icsRegClassifReSetTemplateHint         "����� ������ ..."
//#define   icsRegClassifReSetTemplateShortCut     "Ctrl+G"
//
//#define   icsRegClassifRefreshCaption      "��������"
//#define   icsRegClassifRefreshHint         "�������� ..."
//#define   icsRegClassifRefreshShortCut     "F5"
//
//#define   icsRegClassifFindCaption      "�����"
//#define   icsRegClassifFindHint         "����� ..."
//#define   icsRegClassifFindShortCut     "Ctrl+F"
//
//#define   icsRegClassifFindNextCaption      "����� �����"
//#define   icsRegClassifFindNextHint         "����� ����� ..."
//#define   icsRegClassifFindNextShortCut     "F3"
//
//#define   icsRegClassifViewClassParamCaption      "��������� �������� ��������������"
//#define   icsRegClassifViewClassParamHint         "��������� �������� �������������� ..."
//#define   icsRegClassifViewClassParamShortCut     "Alt+1"
//
//#define   icsRegClassifViewTemplateParamCaption      "��������� �������"
//#define   icsRegClassifViewTemplateParamHint         "��������� ������� ..."
//#define   icsRegClassifViewTemplateParamShortCut     "Alt+2"
//// RegListEdit
#define   icsRegClassifEditApplyBtnCaption "���������"
#define   icsRegClassifEditCancelBtnCaption "������"
//
//// TTreeClassForm
//#define   icsRegTreeClassLoadMsg    "�������� �������������� \"%s\" ��� \"%s\""
//#define   icsRegTreeClassLoadError  "������ �������� ��������������"
//#define
//#define   icsRegTreeClassSelGroupCaption  "������� &��������� ��������"
//#define   icsRegTreeClassFindLabCaption   "�����:"
//#define   icsRegTreeClassApplyBtnCaption  "���������"
//#define   icsRegTreeClassCancelBtnCaption "������"

#endif
