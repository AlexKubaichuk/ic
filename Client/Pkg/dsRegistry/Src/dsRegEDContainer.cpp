﻿//---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop

#include "dsRegEDContainer.h"

#include "dsRegEdBinary.h"
#include "dsRegEdChoice.h"
#include "dsRegEdChoiceDB.h"
//#include "dsRegChoiceDBTmpl.h"
#include "dsRegEdChoiceTree.h"
#include "dsRegEdDate.h"
#include "dsRegEdDigit.h"
#include "dsRegEdExtEdit.h"
#include "dsRegEdText.h"
#include "dsRegEdGroup.h"

#include "dsRegTmplBinary.h"
#include "dsRegTmplChoice.h"
#include "dsRegTmplChoiceDB.h"
#include "dsRegTmplChoiceTree.h"
#include "dsRegTmplDate.h"
#include "dsRegTmplDigit.h"
#include "dsRegTmplExtEdit.h"
#include "dsRegTmplGroup.h"
#include "dsRegTmplText.h"
//#include "riBinaryTmpl.h"
//#include "riChoiceTmpl.h"
//#include "riChoiceTreeTmpl.h"
//#include "riDateTmpl.h"
//#include "riDigitTmpl.h"
//#include "riExtEditTmpl.h"
//#include "riTextTmpl.h"
//#include "riGroupTmpl.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
__fastcall EdsRegEDContainerError::EdsRegEDContainerError(UnicodeString Msg) : Sysutils::Exception(Msg.c_str()) {};
//---------------------------------------------------------------------------
__fastcall EdsRegEDContainerDSError::EdsRegEDContainerDSError(UnicodeString Msg) : Sysutils::Exception(Msg.c_str()) {};
//---------------------------------------------------------------------------
__fastcall ERegEDError::ERegEDError(UnicodeString Msg) : Sysutils::Exception(Msg.c_str()) {};
//---------------------------------------------------------------------------
__fastcall ERegEDTemplateError::ERegEDTemplateError(UnicodeString Msg) : Sysutils::Exception(Msg.c_str()) {};
//---------------------------------------------------------------------------
__fastcall ERegLabError::ERegLabError(UnicodeString Msg) : Sysutils::Exception(Msg.c_str()) {};
//---------------------------------------------------------------------------
__fastcall TdsRegEDContainer::TdsRegEDContainer(TComponent *AOwner, TWinControl *AParent)
{
  if (!AOwner)  throw EdsRegEDContainerError(FMT(dsRegEDContainerErrorNoOwner));
  FOwner = AOwner;
  if (!AParent) throw EdsRegEDContainerError(FMT(dsRegEDContainerErrorNoParent));
  FPlaceCtrl = AParent;
  FCtrlDataChange = NULL;
  FDataProvider = NULL;
  FDefXML  = NULL;
  FReqColor = clInfoBk;
//  FClassData = NULL;
  FDataPath = "";

  FUseHash = false;
  FTreeForm = new TTreeFormMap;
  FTemplateData = new TdsRegTemplateData;
  FEditIdList     = new TStringList;
  FTmplEditIdList = new TStringList;
  FLabIdList      = new TStringList;
  FChDBHash       = new  TkabChDBHashMap;
  FClassHash      = new TkabClassHashMap;
  FOnKeyDown = NULL;
}
//---------------------------------------------------------------------------
__fastcall TdsRegEDContainer::~TdsRegEDContainer()
{
  ClearEditItems();
  ClearTemplateItems();
  ClearLabItems();
  for (TTreeFormMap::iterator i = FTreeForm->begin(); i != FTreeForm->end(); i++)
   delete i->second;
  FTreeForm->clear();
  delete FTreeForm;
  delete FEditIdList;
  delete FTmplEditIdList;
  delete FLabIdList;
  FChDBHash->clear();
  delete FChDBHash;
  for (TkabClassHashMap::iterator i = FClassHash->begin(); i != FClassHash->end(); i++)
   delete i->second;
  FClassHash->clear();
  delete FClassHash;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDContainer::ReiterateClear(TTagNode *ANode, TRegEDMap AMap)
{
  TTagNode *itNode = ANode->GetLastChild();
  while (itNode)
   {
     ReiterateClear(itNode, AMap);
     itNode = itNode->GetPrev();
   }
  if (ANode->CmpName("binary,choice,choiceDB,class,choiceTree,date,digit,text,extedit,group"))
   {
     if (ANode->AV["uid"].Trim().Length())
      {
        TRegEDMap::iterator FRC = AMap.find(ANode->AV["uid"].UpperCase().Trim());
        if (FRC != AMap.end())
         {
           if (FRC->second)
            {
//              TdsRegEDItem* regItem = FRC->second;
              if (FRC->second->DefNode->CmpName("choiceDB,class"))
               {
                 delete FRC->second;
                 FRC->second = NULL;
               }
              else if (FRC->second->DefNode->CmpName("binary"))
               {
                 delete FRC->second;
                 FRC->second = NULL;
               }
              else
               {
                 FRC->second->Parent = NULL;
                 delete FRC->second;
                 FRC->second = NULL;
               }
            }
         }
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDContainer::ClearEditItems()
{
  ReiterateClear(FDefXML, FEditMap);
  FEditMap.clear();
  FEditIdList->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDContainer::ClearTemplateItems()
{
  ReiterateClear(FDefXML, FTmplEditMap);
  FTmplEditMap.clear();
  FTmplEditIdList->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDContainer::ClearLabItems()
{
  for (TRegLabMap::iterator i = FLabMap.begin(); i != FLabMap.end(); i++)
   i->second->Parent = NULL;
  for (TRegLabMap::iterator i = FLabMap.begin(); i != FLabMap.end(); i++)
   delete i->second;
  FLabMap.clear();
  FLabIdList->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDContainer::EditDataChange()
{
  for (TRegEDMap::iterator i = FEditMap.begin(); i != FEditMap.end(); i++)
   i->second->DataChange(NULL);
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDContainer::TemplateDataChange()
{
  for (TRegEDMap::iterator i = FTmplEditMap.begin(); i != FTmplEditMap.end(); i++)
   i->second->DataChange(NULL);
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDContainer::LabDataChange()
{
  for (TRegLabMap::iterator i = FLabMap.begin(); i != FLabMap.end(); i++)
   i->second->DataChange();
}
//---------------------------------------------------------------------------
TdsRegEDItem* __fastcall TdsRegEDContainer::FGetEditItems(UnicodeString AUID)
{
  TRegEDMap::iterator RC = FEditMap.find(AUID.UpperCase());
  if (RC == FEditMap.end())
   throw EdsRegEDContainerError(FMT1(dsRegEDContainerErrorEDNo,AUID).c_str());
  return FEditMap[AUID];
}
//---------------------------------------------------------------------------
TdsRegEDItem* __fastcall TdsRegEDContainer::FGetTemplateItems(UnicodeString AUID)
{
  TRegEDMap::iterator RC = FTmplEditMap.find(AUID.UpperCase());
  if (RC == FTmplEditMap.end())
   throw EdsRegEDContainerError(FMT1(dsRegEDContainerErrorTmplNo,AUID).c_str());
  return FTmplEditMap[AUID];
}
//---------------------------------------------------------------------------
TdsRegLabItem* __fastcall TdsRegEDContainer::FGetLabItems(UnicodeString AUID)
{
  TRegLabMap::iterator RC = FLabMap.find(AUID.UpperCase());
  if (RC == FLabMap.end())
   throw EdsRegEDContainerError(FMT1(dsRegEDContainerErrorLabNo,AUID).c_str());
  return FLabMap[AUID];
}
//---------------------------------------------------------------------------
int __fastcall TdsRegEDContainer::FGetEditCount()
{
  return FEditMap.size();
}
//---------------------------------------------------------------------------
int __fastcall TdsRegEDContainer::FGetTemplateEditCountCount()
{
  return FTmplEditMap.size();
}
//---------------------------------------------------------------------------
int __fastcall TdsRegEDContainer::FGetLabCount()
{
  return FLabMap.size();
}
//---------------------------------------------------------------------------
TdsRegEDItem* __fastcall TdsRegEDContainer::AddEditItem(TTagNode* ANode, bool AIsApp, TWinControl *AParent)
{
  if (!FDataProvider)
   throw ERegEDError(FMT(dsRegEDContainerErrorNoDataSrc));
  if (!ANode)
   throw ERegEDError(FMT(dsRegEDContainerErrorNoDataDef));
  if (!ANode->AV["uid"].Length())
   throw ERegEDError(FMT(dsRegEDContainerErrorNoDataDefId));

  UnicodeString FUID = ANode->AV["uid"].UpperCase();
  try
   {
     TRegEDMap::iterator RC = FEditMap.find(FUID);
     if (RC == FEditMap.end())
      {
        try
         {
           if (ANode->CmpName("binary"))
            {
              FEditMap[FUID] = new TriBinary(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this, AIsApp);
            }
           else if (ANode->CmpName("choice"))
            {
              FEditMap[FUID] = new TriChoice(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this, AIsApp);
            }
           else if (ANode->CmpName("choiceDB,class"))
            {
              FEditMap[FUID] = new TriChoiceDB(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this, AIsApp);
            }
           else if (ANode->CmpName("choiceTree"))
            {
              FEditMap[FUID] = new TriChoiceTree(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this, AIsApp);
            }
           else if (ANode->CmpName("date"))
            {
              FEditMap[FUID] = new TriDate(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this, AIsApp);
            }
           else if (ANode->CmpName("digit"))
            {
              FEditMap[FUID] = new TriDigit(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this, AIsApp);
            }
           else if (ANode->CmpName("text"))
            {
              FEditMap[FUID] = new TriText(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this, AIsApp);
            }
           else if (ANode->CmpName("extedit"))
            {
              FEditMap[FUID] = new TriExtEdit(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this, AIsApp);
            }
           else if (ANode->CmpName("group"))
            {
              FEditMap[FUID] = new TriGroup(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this, AIsApp);
            }
           FEditIdList->Add(FUID);
           FEditMap[FUID]->SetOnDependValues();
           FEditMap[FUID]->OnDataChange = FIntCtrlDataChange;
         }
        catch(Exception &E)
         {
           throw EdsRegEDContainerError(FMT2(dsRegEDContainerSysErrorEDCreate,FUID,E.Message).c_str());
         }
        catch(...)
         {
           throw EdsRegEDContainerError(FMT1(dsRegEDContainerErrorEDCreate,FUID).c_str());
         }
      }
     else
      throw EdsRegEDContainerError(FMT1(dsRegEDContainerErrorEDExists,FUID).c_str());
   }
  __finally
   {
   }
  return FEditMap[FUID];
}
//---------------------------------------------------------------------------
TdsRegEDItem* __fastcall TdsRegEDContainer::AddTemplateItem(TTagNode* ANode, UnicodeString AVal, TWinControl *AParent)
{
  if (!ANode)
   throw ERegEDError(FMT(dsRegEDContainerErrorNoDataDef));

  if (!ANode->AV["uid"].Length())
   throw ERegEDError(FMT(dsRegEDContainerErrorNoDataDefId));

  UnicodeString FUID = ANode->AV["uid"].UpperCase();
  try
   {
     TRegEDMap::iterator RC = FTmplEditMap.find(FUID);
     if (RC == FTmplEditMap.end())
      {
        try
         {
           if (ANode->CmpName("binary"))
            {
              FTmplEditMap[FUID] = new TriBinaryTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this);
            }
           else if (ANode->CmpName("choice"))
            {
              FTmplEditMap[FUID] = new TriChoiceTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this);
            }
           else if (ANode->CmpName("choiceDB"))
            {
              FTmplEditMap[FUID] = new TriChoiceDBTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this);
            }
           else if (ANode->CmpName("choiceTree"))
            {
              FTmplEditMap[FUID] = new TriChoiceTreeTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this);
            }
           else if (ANode->CmpName("date"))
            {
              FTmplEditMap[FUID] = new TriDateTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this);
            }
           else if (ANode->CmpName("digit"))
            {
              FTmplEditMap[FUID] = new TriDigitTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this);
            }
           else if (ANode->CmpName("text"))
            {
              FTmplEditMap[FUID] = new TriTextTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this);
            }
           else if (ANode->CmpName("extedit"))
            {
              FTmplEditMap[FUID] = new TriExtEditTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this);
            }
           else if (ANode->CmpName("group"))
            {
              FTmplEditMap[FUID] = new TriGroupTmpl(FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this);
            }
           FTmplEditIdList->Add(FUID);
           FTmplEditMap[FUID]->SetOnDependValues();
           if (AVal.Trim().Length() && !IsTemplate(FUID))
            FTmplEditMap[FUID]->SetValue(AVal);
           FTmplEditMap[FUID]->OnDataChange = FIntCtrlDataChange;
         }
        catch(Exception &E)
         {
           throw EdsRegEDContainerError(FMT2(dsRegEDContainerSysErrorTmplCreate,FUID,E.Message).c_str());
         }
        catch(...)
         {
           throw EdsRegEDContainerError(FMT1(dsRegEDContainerErrorTmplCreate,FUID).c_str());
         }
      }
     else
      throw EdsRegEDContainerError(FMT1(dsRegEDContainerErrorTmplExists,FUID).c_str());
   }
  __finally
   {
   }
  return FTmplEditMap[FUID];
}
//---------------------------------------------------------------------------
TdsRegLabItem* __fastcall TdsRegEDContainer::AddLabItem(TTagNode* ANode, int AWidth, TWinControl *AParent)
{
  if (!FDataProvider)
   throw ERegEDError(FMT(dsRegEDContainerErrorNoDataSrc));

  if (!ANode)
   throw ERegEDError(FMT(dsRegEDContainerErrorNoDataDef));
  if (!ANode->AV["uid"].Length())
   throw ERegEDError(FMT(dsRegEDContainerErrorNoDataDefId));

  UnicodeString FUID = ANode->AV["uid"].UpperCase();
  try
   {
     TRegLabMap::iterator RC = FLabMap.find(FUID);
     if (RC == FLabMap.end())
      {
        try
         {
           FLabMap[FUID] = new TdsRegLabItem (FOwner, (AParent)? AParent : FPlaceCtrl, ANode, this, AWidth);
           FLabIdList->Add(FUID);
           FLabMap[FUID]->SetOnDependValues();
         }
        catch(Exception &E)
         {
           throw EdsRegEDContainerError(FMT2(dsRegEDContainerSysErrorLabCreate,FUID,E.Message).c_str());
         }
        catch(...)
         {
           throw EdsRegEDContainerError(FMT1(dsRegEDContainerErrorLabCreate,FUID).c_str());
         }
      }
     else
      throw EdsRegEDContainerError(FMT1(dsRegEDContainerErrorLabExists,FUID).c_str());
   }
  __finally
   {
   }
  return FLabMap[FUID];
}
//---------------------------------------------------------------------------
TdsRegEDItem*  __fastcall TdsRegEDContainer::GetEDControl(UnicodeString AUID)
{
  TRegEDMap::iterator RC = FEditMap.find(AUID.UpperCase());
  if (RC == FEditMap.end())
   return NULL;
  else
   return FEditMap[AUID];
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegEDContainer::FillClass(UnicodeString ARef, TStringList *AValues, __int64 ACode)
{
  bool RC = false;
  try
   {
     if (FUseHash)
      {
        UnicodeString FHashKey = (ARef+"_"+IntToStr(ACode)).UpperCase();
        TkabClassHashMap::iterator FRC = FClassHash->find(FHashKey);
        if (FRC == FClassHash->end())
         {
           TStringList *FtmpList = new TStringList;
           if (!FDataProvider->FillClass(ARef, FtmpList, ACode))
            delete FtmpList;
           else
             (*FClassHash)[FHashKey] = FtmpList;
           FRC = FClassHash->find(FHashKey);
         }
        if (FRC != FClassHash->end())
         {
           AValues->Assign(FRC->second);
           RC = true;
         }
      }
     else
       RC = FDataProvider->FillClass(ARef, AValues, ACode);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
Variant __fastcall TdsRegEDContainer::GetChDBData(UnicodeString ARef, UnicodeString RecVal)
{
  Variant RC = Variant::Empty();
  try
   {
     if (FUseHash)
      {
        UnicodeString FHashKey = (ARef+"_"+RecVal).UpperCase();
        TkabChDBHashMap::iterator FRC = FChDBHash->find(FHashKey);
        if (FRC == FChDBHash->end())
         {
           (*FChDBHash)[FHashKey] = FDataProvider->GetChDBData(ARef, RecVal);
           FRC = FChDBHash->find(FHashKey);
         }
        if (FRC != FChDBHash->end())
         RC = FRC->second;
      }
     else
       RC = FDataProvider->GetChDBData(ARef, RecVal);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
Variant __fastcall TdsRegEDContainer::GetCtrlData(TTagNode *ADefNode, UnicodeString &ACaption)
{
  return FDataProvider->GetCtrlData(ADefNode, ACaption);
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDContainer::SetCtrlData(TTagNode *ADefNode, Variant AValue, UnicodeString AStrValue)
{
  FDataProvider->SetCtrlData(ADefNode, AValue, AStrValue);
}
//---------------------------------------------------------------------------
TdsRegEDItem*  __fastcall TdsRegEDContainer::GetTemplateControl(UnicodeString AUID)
{
  TRegEDMap::iterator RC = FTmplEditMap.find(AUID.UpperCase());
  if (RC == FTmplEditMap.end())
   return NULL;
  else
   return FTmplEditMap[AUID];
}
//---------------------------------------------------------------------------
TdsRegLabItem* __fastcall TdsRegEDContainer::GetLabControl(UnicodeString AUID)
{
  TRegLabMap::iterator RC = FLabMap.find(AUID.UpperCase());
  if (RC == FLabMap.end())
   return NULL;
  else
   return FLabMap[AUID];
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegEDContainer::FIntCtrlDataChange(TTagNode *ItTag, UnicodeString &Src)
{
  bool RC = false;
  try
   {
     if (FCtrlDataChange)
      RC = FCtrlDataChange(ItTag, Src, NULL/*FDataSrc*/);    //FDataProvider
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDContainer::SetOnCtrlDataChange(int AType)
{
  // AType:
  //  0 - EditMap
  //  1 - TmplEditMap
  //  3 - All
  if (FCtrlDataChange)
   {
     switch (AType)
      {
        case 0:
        {
          for (TRegEDMap::iterator i = FEditMap.begin(); i != FEditMap.end(); i++)
           i->second->OnDataChange = FIntCtrlDataChange;
          break;
        }
        case 1:
        {
          for (TRegEDMap::iterator i = FTmplEditMap.begin(); i != FTmplEditMap.end(); i++)
           i->second->OnDataChange = FIntCtrlDataChange;
          break;
        }
        case 3:
        {
          for (TRegEDMap::iterator i = FEditMap.begin(); i != FEditMap.end(); i++)
           i->second->OnDataChange = FIntCtrlDataChange;
          for (TRegEDMap::iterator i = FTmplEditMap.begin(); i != FTmplEditMap.end(); i++)
           i->second->OnDataChange = FIntCtrlDataChange;
        }
      }
   }
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDContainer::ClearOnCtrlDataChange(int AType)
{
  // AType:
  //  0 - EditMap
  //  1 - TmplEditMap
  //  2 - LabMap
  //  3 - All
  if (FCtrlDataChange)
   {
     switch (AType)
      {
        case 0:
        {
          for (TRegEDMap::iterator i = FEditMap.begin(); i != FEditMap.end(); i++)
           i->second->OnDataChange = NULL;
          break;
        }
        case 1:
        {
          for (TRegEDMap::iterator i = FTmplEditMap.begin(); i != FTmplEditMap.end(); i++)
           i->second->OnDataChange = NULL;
          break;
        }
        case 3:
        {
          for (TRegEDMap::iterator i = FEditMap.begin(); i != FEditMap.end(); i++)
           i->second->OnDataChange = NULL;
          for (TRegEDMap::iterator i = FTmplEditMap.begin(); i != FTmplEditMap.end(); i++)
           i->second->OnDataChange = NULL;
        }
      }
   }
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegEDContainer::GetEDValue(bool ATemplate, UnicodeString AUID, UnicodeString ARef)
{
  UnicodeString RC = "";
  try
   {
     if (ATemplate)
      RC = TemplateItems[AUID]->GetValue(ARef);
     else
      RC = EditItems[AUID]->GetValue(ARef);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDContainer::SetEDValue(bool ATemplate, UnicodeString AUID, UnicodeString AValue)
{
  if (ATemplate)
   TemplateItems[AUID]->SetValue(AValue);
  else
   EditItems[AUID]->SetValue(AValue);
}
//---------------------------------------------------------------------------
TdsRegTreeClassForm* __fastcall TdsRegEDContainer::GetTreeFormDlg(UnicodeString ARef)
{
  TdsRegTreeClassForm* RC = NULL;
  try
   {
     if (FDataProvider->OnGetClassXML)
      {
        TTreeFormMap::iterator FRC = FTreeForm->find(ARef.UpperCase());
        if (FRC != FTreeForm->end())
         RC = FRC->second;
        else
         {
           TTagNode *FDef;
           FDataProvider->OnGetClassXML(ARef, FDef);
           RC = new TdsRegTreeClassForm(NULL, FDef);
           (*FTreeForm)[ARef.UpperCase()] = RC;
         }
      }
     else
      throw EdsRegEDContainerError("Не задан обработчик OnGetClassXML.");
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
UnicodeString __fastcall TdsRegEDContainer::GetTreeValue(UnicodeString ARef, UnicodeString ACode, UnicodeString &AExtCode)
{
  return FDataProvider->GetTreeValue(ARef, ACode, AExtCode);
/*
  UnicodeString RC = "";
  try
   {
     if (FOnGetClassXML)
      {
        TTagNode *FDef;
        FOnGetClassXML(ARef, FDef);
        FDef = FDef->GetChildByAV("i","c",ACode,true);
        if (FDef)
         {
           RC = FDef->AV["n"];
           AExtCode = FDef->AV["e"];
         }
      }
     else
      throw EdsRegEDContainerError("Не задан обработчик OnGetClassXML.");
   }
  __finally
   {
   }
  return RC;
  */
}
//---------------------------------------------------------------------------
TTagNode* __fastcall TdsRegEDContainer::GetNode(UnicodeString ARef)
{
  if (FDefXML) return FDefXML->GetTagByUID(ARef);
  else         return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TdsRegEDContainer::FSetTemplateData(TdsRegTemplateData *AData)
{
  if (AData)
   FTemplateData->Copy(AData);
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegEDContainer::IsTemplate(UnicodeString AId)
{
  bool RC = false;
  try
   {
     if (FTemplateData)
      RC = FTemplateData->IsTemplate(AId);
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------
bool __fastcall TdsRegEDContainer::TemplateNoLock()
{
  bool RC = false;
  try
   {
     if (FTemplateData)
      RC = FTemplateData->NoLock;
   }
  __finally
   {
   }
  return RC;
}
//---------------------------------------------------------------------------

