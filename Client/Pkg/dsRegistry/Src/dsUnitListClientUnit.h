//---------------------------------------------------------------------------
#ifndef dsUnitListClientUnitH
#define dsUnitListClientUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include "cxButtonEdit.hpp"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxData.hpp"
#include "cxDataStorage.hpp"
#include "cxEdit.hpp"
#include "cxFilter.hpp"
#include "cxGraphics.hpp"
#include "cxGrid.hpp"
#include "cxGridCustomTableView.hpp"
#include "cxGridCustomView.hpp"
#include "cxGridLevel.hpp"
#include "cxGridTableView.hpp"
#include "cxImage.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxNavigator.hpp"
#include "cxPC.hpp"
#include "cxSplitter.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "dxBar.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxGDIPlusClasses.hpp"
#include "dxStatusBar.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "dsRegDMUnit.h"
#include "KabCustomDS.h"
#include "dxBarExtItems.hpp"
#include "dsProgress.h"
#include "cxProgressBar.hpp"
#include "cxMemo.hpp"
#include "cxRichEdit.hpp"
//---------------------------------------------------------------------------
class PACKAGE TdsUnitListClientForm : public TForm
{
__published:	// IDE-managed Components
        TdxBarManager *ClassTBM;
 TcxStyleRepository *Style;
        TcxStyle *BGStyle;
        TActionList *ClassAL;
        TAction *actInsert;
        TAction *actEdit;
        TAction *actDelete;
        TAction *actSetFilter;
        TAction *actReSetFilter;
        TAction *actRefresh;
        TcxStyle *cxStyle2;
        TAction *actExit;
        TAction *actTemplate;
        TPanel *CompPanel;
        TcxSplitter *cxSplitter1;
        TcxGrid *ClassGrid;
        TcxGridLevel *ClassGridLevel1;
 TdxBarLargeButton *InsertBI;
 TdxBarLargeButton *EditBI;
  TdxBarLargeButton *dxBarLargeButton4;
  TdxBarLargeButton *dxBarLargeButton5;
  TdxBarLargeButton *dxBarLargeButton8;
  TdxBarLargeButton *dxBarLargeButton9;
  TdxBarLargeButton *dxBarLargeButton10;
  TcxGridTableView *ClassGridView;
 TcxImageList *LClassIL32;
 TdxBarLargeButton *dxBarLargeButton11;
 TdxBarLargeButton *dxBarLargeButton12;
 TAction *actExport;
 TAction *actOpen;
 TAction *actSelAll;
 TAction *actFilterSetting;
 TAction *actPrintListSetting;
 TdxBar *ClassTBMBar3;
 TdxBarPopupMenu *ReportMenu;
 TdxBarPopupMenu *QuickFilterMenu;
 TdxBarLargeButton *PrintBI;
 TdxBarLargeButton *dxBarLargeButton1;
 TdxBarLargeButton *dxBarLargeButton14;
 TTimer *ShowTimer;
 TdxStatusBar *StatusSB;
 TAction *actFind;
 TAction *actFindAndFilter;
 TAction *actClearSearchField;
 TcxImage *ProgressPanel;
 TdxBarLargeButton *dxBarLargeButton6;
 TdxBarLargeButton *dxBarLargeButton7;
 TdxBarLargeButton *ServiceBI;
 TdxBarPopupMenu *ServiceMenu;
 TAction *actShowPrePlan;
 TAction *actPrintF63;
 TdxBarPopupMenu *PMenu;
 TcxStyle *cxStyle1;
 TcxButtonEdit *SearchBE;
 TcxImageList *SearchIL;
 TAction *actImport;
 TdxBarLargeButton *dxBarLargeButton15;
 TdxStatusBarContainerControl *StatusSBContainer0;
 TcxProgressBar *PrBar;
 TAction *actAddImmAct;
 TdxBarLargeButton *dxBarLargeButton16;
 TPanel *InfoPanel;
 TcxPageControl *ExtListPC;
 TcxRichEdit *CountED;
 TTimer *PrTimer;
 TAction *actPrintFullF63;
 TdxBarLargeButton *dxBarLargeButton17;
 TdxBarPopupMenu *InsertMenu;
 TAction *actInsPriv;
 TAction *actInsTest;
 TAction *actEditLastPriv;
 TAction *actEditLastTest;
 TdxBarPopupMenu *EditMenu;
 TdxBarLargeButton *dxBarLargeButton13;
 TdxBarLargeButton *dxBarLargeButton18;
 TAction *actInsCancel;
 TdxBarLargeButton *dxBarLargeButton2;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall actInsertExecute(TObject *Sender);
        void __fastcall actEditExecute(TObject *Sender);
        void __fastcall actDeleteExecute(TObject *Sender);
        void __fastcall actSetFilterExecute(TObject *Sender);
        void __fastcall actReSetFilterExecute(TObject *Sender);
        void __fastcall actRefreshExecute(TObject *Sender);
        void __fastcall actExitExecute(TObject *Sender);
        void __fastcall actTemplateExecute(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
 void __fastcall ClassGridViewFocusedRecordChanged(TcxCustomGridTableView *Sender,
          TcxCustomGridRecord *APrevFocusedRecord, TcxCustomGridRecord *AFocusedRecord,
          bool ANewItemRecordFocusingChanged);
 void __fastcall actOpenExecute(TObject *Sender);
 void __fastcall actFilterSettingExecute(TObject *Sender);
 void __fastcall actPrintListSettingExecute(TObject *Sender);
 void __fastcall ShowTimerTimer(TObject *Sender);
 void __fastcall actFindExecute(TObject *Sender);
 void __fastcall actFindAndFilterExecute(TObject *Sender);
 void __fastcall actClearSearchFieldExecute(TObject *Sender);
 void __fastcall ClassGridViewColumnHeaderClick(TcxGridTableView *Sender, TcxGridColumn *AColumn);
 void __fastcall SearchBEPropertiesChange(TObject *Sender);
 void __fastcall ClassGridViewDataControllerFilterChanged(TObject *Sender);
 void __fastcall actShowPrePlanExecute(TObject *Sender);
 void __fastcall actPrintF63Execute(TObject *Sender);
 void __fastcall ServiceBIClick(TObject *Sender);
 void __fastcall FormActivate(TObject *Sender);
 void __fastcall actImportExecute(TObject *Sender);
 void __fastcall actExportExecute(TObject *Sender);
 void __fastcall PrTimerTimer(TObject *Sender);
 void __fastcall actAddImmActExecute(TObject *Sender);
 void __fastcall actPrintFullF63Execute(TObject *Sender);
 void __fastcall actInsPrivExecute(TObject *Sender);
 void __fastcall actInsTestExecute(TObject *Sender);
 void __fastcall actEditLastPrivExecute(TObject *Sender);
 void __fastcall actEditLastTestExecute(TObject *Sender);
 void __fastcall actInsCancelExecute(TObject *Sender);



private:	// User declarations
  TdsRegDM *FDM;
  TkabCustomDataSource *FDataSrc;
  TdsProgressForm* FProgress;
  void __fastcall FGetDataProgress(int AMax, int ACur, UnicodeString AMsg);
  void __fastcall RefreshById(UnicodeString ACode);
  TcxTabSheet *FCurPage;
  typedef map<int, UnicodeString> TIntStrMap;
  TIntStrMap FReportList;
  TIntStrMap FQuickFilterList;
typedef list<UnicodeString> TSearchCodeMap;
  TSearchCodeMap FSearchCodes;
  bool FFilterMode;
  UnicodeString FFilter;
        bool SaveEnInsert;
        bool SaveEnEdit;
        bool SaveEnDelete;
        bool SaveEnSetTemplate;
        bool SaveEnReSetTemplate;
        bool SaveEnRefresh;
        bool SaveEnFind;
        bool SaveEnFindNext;
        bool SaveEnViewClassParam;
        bool SaveEnViewTemplateParam;

    void __fastcall CheckCtrlState(bool AEnable);
    void __fastcall CheckCtrlVisible();
    bool __fastcall FSetEdit(TTagNode *itTag);
    void __fastcall SetFiltered(bool AVal);
//    TRegistry_ICS* RegComp;
    TRegIniFile  *RegIni;
    UnicodeString    SaveCaption;
    TTagNode* __fastcall _CLASSNODE_();
    bool __fastcall GetBtnEn(UnicodeString AAttrName);
    void __fastcall ChangeSrc();
    bool __fastcall CreateClassTV(TTagNode *itTag);
    bool __fastcall CreateListExt(TTagNode *itTag);
    void __fastcall ListAfterScroll(TDataSet *DataSet);
    UnicodeString __fastcall GetUnitStr();
    void __fastcall FFetchAllRec();
  void __fastcall ClearData();

    TcxGridColumn* __fastcall ClassCol(int AIdx);
    __int64    curCD,xpCount,xcTop;
    int     chBDInd;
    int FSearchIdx;
    bool FSearchModify;
    bool FSearchAndFilter;
    TStringList *FSearchList;
//    TList   *CompList;
    UnicodeString   SaveClassCapt;
    UnicodeString   cCapt;
    TTagNode     *RootClass;
    TTagNode     *UnitDesc;
//    TTreeNode    *TreeClassRoot;
    TcxTreeListNode *FRootNode;
    TTagNode     *CallRootClass;
//    TTagNode     *FSelectedNode;
    TdsRegEDContainer *FCtrList;
    int FLastTop,FLastHeight;
  bool FInProgress;
  bool FindFilterSrc;
  void __fastcall SetInProgress(bool AVal);
  bool __fastcall IsRecSelected();
  void __fastcall FSetFilterExecute(UnicodeString AFilterGUI = "");
  void __fastcall FGetData();
  void __fastcall FGetQuickFilterList(bool ASet);
  void __fastcall QuickFilterItemClick(TObject *Sender);
  void __fastcall FGetReportList(bool ASet);
  void __fastcall PrintReportItemClick(TObject *Sender);
  bool __fastcall CheckProgress(TkabProgressType AType, int AVal, UnicodeString AMsg);
  bool __fastcall CanProgress();
  void __fastcall SetFocusedRec(__int64 ARowIdx);
  void __fastcall SetActiveCtrl();
  void __fastcall GetCommCount();
  void __fastcall FFind(bool ASilent);
protected:
  virtual void __fastcall WndProc(Messages::TMessage &Message);
public:		// User declarations
    __fastcall TdsUnitListClientForm(TComponent* Owner, TdsRegDM *ADM);
    void __fastcall SetActiveClass(UnicodeString AClsId);
  void __fastcall GetSelectedUnitData(TkabCustomDataSetRow *& AData);
  __property bool InProgress = {read=FInProgress, write=SetInProgress};
};
//---------------------------------------------------------------------------
extern PACKAGE TdsUnitListClientForm *dsUnitListClientForm;
//---------------------------------------------------------------------------
#endif
