//---------------------------------------------------------------------------
#ifndef dsClassifClientUnitH
#define dsClassifClientUnitH
//---------------------------------------------------------------------------
#include <System.Actions.hpp>
#include <System.Classes.hpp>
#include <System.SysUtils.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.ImgList.hpp>
#include "cxButtonEdit.hpp"
#include "cxClasses.hpp"
#include "cxContainer.hpp"
#include "cxControls.hpp"
#include "cxCustomData.hpp"
#include "cxData.hpp"
#include "cxDataStorage.hpp"
#include "cxEdit.hpp"
#include "cxFilter.hpp"
#include "cxGraphics.hpp"
#include "cxGrid.hpp"
#include "cxGridCustomTableView.hpp"
#include "cxGridCustomView.hpp"
#include "cxGridLevel.hpp"
#include "cxGridTableView.hpp"
#include "cxImage.hpp"
#include "cxInplaceContainer.hpp"
#include "cxLookAndFeelPainters.hpp"
#include "cxLookAndFeels.hpp"
#include "cxMaskEdit.hpp"
#include "cxNavigator.hpp"
#include "cxPC.hpp"
#include "cxSplitter.hpp"
#include "cxStyles.hpp"
#include "cxTextEdit.hpp"
#include "cxTL.hpp"
#include "cxTLdxBarBuiltInMenu.hpp"
#include "dxBar.hpp"
#include "dxBarBuiltInMenu.hpp"
#include "dxGDIPlusClasses.hpp"
#include "dxStatusBar.hpp"
//---------------------------------------------------------------------------
#include "XMLContainer.h"
#include "dsRegDMUnit.h"
#include "KabCustomDS.h"
//---------------------------------------------------------------------------
class PACKAGE TdsClassifClientForm : public TForm
{
__published:	// IDE-managed Components
        TPopupMenu *PMenu;
        TMenuItem *InsertItem;
        TMenuItem *ModifyItem;
        TMenuItem *DeleteItem;
        TdxBarManager *ClassTBM;
        TdxBarButton *InsertBI;
        TdxBarButton *ResetTemplateBI;
        TdxBarButton *DeleteBI;
        TdxBarButton *EditBI;
 TcxStyleRepository *Style;
        TActionList *ClassAL;
        TAction *actInsert;
        TAction *actEdit;
        TAction *actDelete;
        TAction *actSetFilter;
        TAction *actReSetFilter;
        TdxBarButton *SetFilterBI;
        TAction *actRefresh;
        TdxBarButton *RefreshBI;
        TdxBarButton *ClassParamBI;
        TdxBarButton *TemplateParamBI;
        TcxStyle *cxStyle2;
        TAction *actGetClassList;
        TdxBarButton *dxBarButton1;
        TAction *actClassSelect;
        TAction *actExit;
        TAction *actTemplate;
        TdxBarButton *SetTemplateBI;
        TPanel *CompPanel;
        TcxPageControl *ViewParamPC;
        TcxTabSheet *ClassParamTS;
        TcxTabSheet *ClassListTS;
        TcxTreeList *ClassTL;
        TcxTreeListColumn *ClassTLcxTreeListColumn1;
 TcxSplitter *Split;
        TcxGrid *ClassGrid;
        TcxGridLevel *ClassGridLevel1;
  TdxBarLargeButton *dxBarLargeButton1;
  TdxBarLargeButton *dxBarLargeButton2;
  TdxBarLargeButton *dxBarLargeButton3;
  TdxBarLargeButton *dxBarLargeButton4;
  TdxBarLargeButton *dxBarLargeButton5;
  TdxBarLargeButton *dxBarLargeButton6;
  TdxBarLargeButton *dxBarLargeButton7;
  TdxBarLargeButton *dxBarLargeButton8;
  TdxBarLargeButton *dxBarLargeButton9;
  TdxBarLargeButton *dxBarLargeButton10;
  TcxGridTableView *ClassGridView;
 TcxImageList *LClassIL32;
 TcxImageList *ClassIL;
 TdxBarPopupMenu *QuickFilterMenu;
 TcxPageControl *ExtListPC;
 TAction *actFind;
 TAction *actFindAndFilter;
 TAction *actClearSearchField;
 TcxImage *ProgressPanel;
 TdxStatusBar *StatusSB;
 TcxButtonEdit *SearchBE;
 TcxStyle *cxStyle1;
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall actInsertExecute(TObject *Sender);
        void __fastcall actEditExecute(TObject *Sender);
        void __fastcall actDeleteExecute(TObject *Sender);
        void __fastcall actSetFilterExecute(TObject *Sender);
        void __fastcall actReSetFilterExecute(TObject *Sender);
        void __fastcall actRefreshExecute(TObject *Sender);
        void __fastcall actGetClassListExecute(TObject *Sender);
        void __fastcall actClassSelectExecute(TObject *Sender);
        void __fastcall actExitExecute(TObject *Sender);
        void __fastcall actTemplateExecute(TObject *Sender);
        void __fastcall ClassTLClick(TObject *Sender);
 void __fastcall ClassGridViewFocusedRecordChanged(TcxCustomGridTableView *Sender,
          TcxCustomGridRecord *APrevFocusedRecord, TcxCustomGridRecord *AFocusedRecord,
          bool ANewItemRecordFocusingChanged);
 void __fastcall SearchBEPropertiesChange(TObject *Sender);
 void __fastcall actFindExecute(TObject *Sender);
 void __fastcall actFindAndFilterExecute(TObject *Sender);
 void __fastcall actClearSearchFieldExecute(TObject *Sender);
 void __fastcall ClassGridViewColumnHeaderClick(TcxGridTableView *Sender, TcxGridColumn *AColumn);
 void __fastcall FormActivate(TObject *Sender);


private:	// User declarations
  TdsRegDM *FDM;
  TkabCustomDataSource *FDataSrc;
  void __fastcall FGetDataProgress(int AMax, int ACur, UnicodeString AMsg);
  void __fastcall RefreshById(UnicodeString ACode);
  void __fastcall FFetchAllRec();
  void __fastcall CheckCtrlState(bool AEnable, bool AClassSelect = false);
  TcxTabSheet *FCurPage;
  UnicodeString FFilter;
  bool FSearchModify;
  TStringList *FSearchList;


        bool SaveEnInsert;
        bool SaveEnEdit;
        bool SaveEnDelete;
        bool SaveEnSetTemplate;
        bool SaveEnReSetTemplate;
        bool SaveEnRefresh;
        bool SaveEnFind;
        bool SaveEnFindNext;
        bool SaveEnViewClassParam;
        bool SaveEnViewTemplateParam;

  void __fastcall FGetData();
  void __fastcall ClearData();
    bool __fastcall FSetEdit(TTagNode *itTag);
    void __fastcall SetFiltered(bool AVal);
//    TRegistry_ICS* RegComp;
    TRegIniFile  *RegIni;
    UnicodeString    SaveCaption;
    void __fastcall DisableAfterScroll();
    void __fastcall EnableAfterScroll();
    TTagNode* __fastcall _CLASSNODE_();
    TTagNode* __fastcall _CLASSDESCR_();
    bool __fastcall GetBtnEn(UnicodeString AAttrName);
    void __fastcall ChangeSrc();
    void __fastcall CreateTV(TTagNode *AClassRoot);
    bool __fastcall CreateClassTV(TTagNode *itTag);
    bool __fastcall CreateListExt(TTagNode *itTag);
    void __fastcall ListAfterScroll(TDataSet *DataSet);
    TcxGridColumn* __fastcall ClassCol(int AIdx);
    void __fastcall ClearTemplate();
    __int64    curCD,xpCount,xcTop;
    int     chBDInd;
    int FSearchIdx;
    bool FSearchAndFilter;
//    TList   *CompList;
    UnicodeString   SaveClassCapt;
    UnicodeString   cCapt;
    TTagNode     *RootClass;
//    TTreeNode    *TreeClassRoot;
    TcxTreeListNode *FRootNode;
    TTagNode     *CallRootClass;
    TdsRegClassType FShowType;
    TAnsiStrMap  FGroups;
    TTagNode     *FSelectedNode, *FSelectedNodeDescr;
    TdsRegEDContainer *FCtrList;
    int FLastTop,FLastHeight;
  void __fastcall FSetFilterExecute(UnicodeString AFilterGUI = "");
  void __fastcall SetActiveCtrl();
  bool __fastcall IsRecSelected();
public:		// User declarations
    __fastcall TdsClassifClientForm(TComponent* Owner, TdsRegDM *ADM, TdsRegClassType AShowType, UnicodeString AGroups = "", UnicodeString  AClsId = "");
    void __fastcall SetActiveClass(UnicodeString AClsId);
};
//---------------------------------------------------------------------------
extern PACKAGE TdsClassifClientForm *dsClassifClientForm;
//---------------------------------------------------------------------------
#endif
