//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "dsProgress.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "cxContainer"
#pragma link "cxControls"
#pragma link "cxEdit"
#pragma link "cxGraphics"
#pragma link "cxLookAndFeelPainters"
#pragma link "cxLookAndFeels"
#pragma link "cxProgressBar"
#pragma resource "*.dfm"
TdsProgressForm *dsProgressForm;
//---------------------------------------------------------------------------
__fastcall TdsProgressForm::TdsProgressForm(TComponent* Owner)
 : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TdsProgressForm::ShowTimerTimer(TObject *Sender)
{
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------
TdsProgressForm* __fastcall ShowProgress(TForm *AParent, UnicodeString AText)
{
  TdsProgressForm *RC = new TdsProgressForm(AParent);
  RC->PrBar->Properties->Text = AText;
//  RC->Left = AParent->Left;
//  RC->Top = AParent->Top+(AParent->Height-AParent->ClientHeight)-6;
//  RC->Width = AParent->Width;
//  RC->Height = AParent->ClientHeight;
  RC->Show();
  return RC;
}
//---------------------------------------------------------------------------
