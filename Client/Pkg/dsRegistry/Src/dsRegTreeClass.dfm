object dsRegTreeClassForm: TdsRegTreeClassForm
  Left = 525
  Top = 232
  HelpContext = 3000
  ActiveControl = ClassTree
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = ':)'
  ClientHeight = 497
  ClientWidth = 778
  Color = clBtnFace
  Constraints.MinHeight = 350
  Constraints.MinWidth = 570
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object BtnPanel: TPanel
    Left = 0
    Top = 440
    Width = 778
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      778
      38)
    object FindLab: TLabel
      Left = 11
      Top = 11
      Width = 35
      Height = 13
      Caption = #1055#1086#1080#1089#1082':'
    end
    object QSearchED: TcxButtonEdit
      Left = 51
      Top = 7
      Anchors = [akLeft, akTop, akRight]
      Properties.Buttons = <
        item
          Default = True
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            04000000000080000000CE0E0000C40E00001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777777777777777700000777770000070F000777770F00070F000777770F
            0007000000070000000700F000000F00000700F000700F00000700F000700F00
            00077000000000000077770F00070F0007777700000700000777777000777000
            77777770F07770F0777777700077700077777777777777777777}
          Kind = bkGlyph
        end>
      Properties.OnButtonClick = QSearchEDPropertiesButtonClick
      Properties.OnChange = QSearchEDPropertiesChange
      TabOrder = 0
      Width = 533
    end
    object OkBtn: TcxButton
      Left = 609
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      TabOrder = 1
      OnClick = BtnOkClick
    end
    object CancelBtn: TcxButton
      Left = 695
      Top = 7
      Width = 80
      Height = 25
      Anchors = [akRight]
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object TmplPanel: TPanel
    Left = 0
    Top = 0
    Width = 778
    Height = 25
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 1
    object SelGroupChB: TcxCheckBox
      Left = 3
      Top = 2
      Caption = #1042#1082#1083#1102#1095#1072#1103' &'#1074#1083#1086#1078#1077#1085#1085#1099#1077' '#1101#1083#1077#1084#1077#1085#1090#1099
      TabOrder = 0
      Visible = False
      Width = 374
    end
  end
  object StatusBar: TdxStatusBar
    Left = 0
    Top = 478
    Width = 778
    Height = 19
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarContainerPanelStyle'
        PanelStyle.Container = dxStatusBar1Container0
        Bevel = dxpbNone
        Width = 250
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    object dxStatusBar1Container0: TdxStatusBarContainerControl
      Left = 0
      Top = 2
      Width = 252
      Height = 17
      object PrBar: TcxProgressBar
        Left = 0
        Top = 0
        Align = alClient
        Style.BorderStyle = ebs3D
        Style.Edges = [bLeft, bTop, bRight, bBottom]
        Style.LookAndFeel.Kind = lfStandard
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfStandard
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfStandard
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfStandard
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 0
        Visible = False
        Width = 252
      end
    end
  end
  object ClassTree: TcxTreeList
    Left = 0
    Top = 25
    Width = 778
    Height = 415
    Align = alClient
    Bands = <
      item
      end>
    Navigator.Buttons.CustomButtons = <>
    OptionsBehavior.AlwaysShowEditor = True
    OptionsBehavior.ConfirmDelete = False
    OptionsBehavior.DragCollapse = False
    OptionsBehavior.DragExpand = False
    OptionsBehavior.ExpandOnDblClick = False
    OptionsBehavior.ExpandOnIncSearch = True
    OptionsBehavior.FocusCellOnCycle = True
    OptionsBehavior.IncSearch = True
    OptionsBehavior.IncSearchItem = TextCol
    OptionsBehavior.MultiSort = False
    OptionsBehavior.ShowHourGlass = False
    OptionsBehavior.Sorting = False
    OptionsCustomizing.BandCustomizing = False
    OptionsCustomizing.BandHorzSizing = False
    OptionsCustomizing.BandMoving = False
    OptionsCustomizing.BandVertSizing = False
    OptionsCustomizing.ColumnCustomizing = False
    OptionsCustomizing.ColumnMoving = False
    OptionsCustomizing.ColumnVertSizing = False
    OptionsData.CancelOnExit = False
    OptionsData.AnsiSort = True
    OptionsData.Deleting = False
    OptionsSelection.CellSelect = False
    OptionsView.CellAutoHeight = True
    OptionsView.ShowEditButtons = ecsbFocused
    OptionsView.ColumnAutoWidth = True
    Styles.Inactive = cxStyle1
    Styles.OnGetContentStyle = ClassTreeStylesGetContentStyle
    Styles.ContentEven = oddStyle
    TabOrder = 3
    OnFocusedNodeChanged = ClassTreeFocusedNodeChanged
    object TextCol: TcxTreeListColumn
      DataBinding.ValueType = 'String'
      Width = 704
      Position.ColIndex = 1
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object NameCol: TcxTreeListColumn
      Visible = False
      DataBinding.ValueType = 'String'
      Position.ColIndex = 4
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object ExtCodeCol: TcxTreeListColumn
      Visible = False
      DataBinding.ValueType = 'String'
      Width = 58
      Position.ColIndex = 0
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object CodeCol: TcxTreeListColumn
      Visible = False
      DataBinding.ValueType = 'String'
      Position.ColIndex = 2
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
    object CanSelectCol: TcxTreeListColumn
      Visible = False
      DataBinding.ValueType = 'String'
      Position.ColIndex = 3
      Position.RowIndex = 0
      Position.BandIndex = 0
      Summary.FooterSummaryItems = <>
      Summary.GroupFooterSummaryItems = <>
    end
  end
  object FormStyle: TcxStyleRepository
    Left = 26
    Top = 42
    PixelsPerInch = 96
    object oddStyle: TcxStyle
      AssignedValues = [svColor]
      Color = 16051437
    end
    object bStyle: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svTextColor]
      Color = clHighlight
      TextColor = clBtnText
    end
  end
  object SearchTimer: TTimer
    Enabled = False
    OnTimer = SearchTimerTimer
    Left = 97
    Top = 44
  end
end
