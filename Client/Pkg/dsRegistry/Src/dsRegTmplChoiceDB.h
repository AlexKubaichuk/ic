﻿//---------------------------------------------------------------------------

#ifndef dsRegTmplChoiceDBH
#define dsRegTmplChoiceDBH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>

//#include <DBCtrls.hpp>
//#include "RXDBCtrl.hpp"
//#include <Registry.hpp>
#include "XMLContainer.h"
//#define F_DATE 1
//#define F_DATETIME 2
//#define F_TIME 3

//---------------------------------------------------------------------------
#include "dsRegEDContainer.h"
//---------------------------------------------------------------------------
class PACKAGE TriChoiceDBTmpl: public TdsRegEDItem
{
private:
  TcxButton  *FClearBtn;
    void     __fastcall cResize(TObject *Sender);
    void     __fastcall SetChBDValue(UnicodeString AVal, bool AEnabled);
    void     __fastcall FDataChange(TObject *Sender);
    bool     FExtSearchMode;
    void     __fastcall FSetExtSearchMode(bool AVal);

    void     __fastcall FFillClass();
    __int64 __fastcall FGetClassCode();
    TStringList *FSValues, *FSUpValues, *FSValuesExt;
    TcxComboBox* __fastcall FGetCastED();
    void __fastcall FSetCastED(TcxComboBox *AVal);
    __property TcxComboBox *ED = {read=FGetCastED,write=FSetCastED};
    __property __int64 _CLASSCODE_ = {read=FGetClassCode};
  void __fastcall FClearClick(TObject *ASender);
  void __fastcall FSearchDataChange(TObject *Sender);
  void __fastcall FEditValueChanged(TObject *Sender);
  void __fastcall FEDKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
public:
    __fastcall TriChoiceDBTmpl(TComponent *AOwner, TWinControl *AParent,TTagNode* ANode, TdsRegEDContainer *ACtrlOwner);
    __fastcall ~TriChoiceDBTmpl();
    void         __fastcall SetDependVal(UnicodeString ADepClCode, __int64 ACode, UnicodeString ADepUID);
    Variant  __fastcall GetCompValue(UnicodeString ARef);
    void         __fastcall SetOnDependValues();
    void         __fastcall DataChange(TObject *Sender);
//    bool         TwoLine;
    UnicodeString   __fastcall GetValueStr(bool ABinaryNames = false);
    TControl*    __fastcall GetControl(int AIdx);
    TControl*    __fastcall GetFirstEnabledControl();
    void         __fastcall SetEnable(bool AEnable);
    void         __fastcall SetEnableEx(bool AEnable);
    void         __fastcall SetIsVal(bool AEnable);
    void         __fastcall SetValue(UnicodeString AVal,bool AEnable = true);
    void         __fastcall SetLabel(UnicodeString ACaption);
    void         __fastcall SetOnLab(TdsRegEDItem *AEDItem);
    void         __fastcall SetOnEnable(TTagNode* ANode);
    void         __fastcall SetEDLeft(int ALeft);
    bool         __fastcall UpdateChanges();
    bool         __fastcall CheckReqValue(TcxPageControl *APC = NULL, bool ASilent = false);
    void         __fastcall GetEDLeft(int *ALabR, int *AEDLeft);
    UnicodeString   __fastcall GetValue(UnicodeString ARef);
    UnicodeString   __fastcall GetExtValue(UnicodeString ARef);
    TStringList* __fastcall GetStrings(UnicodeString AClRef);
  void __fastcall Clear();
  __property bool ExtSearchMode = {read=FExtSearchMode, write=FSetExtSearchMode};
  void __fastcall SetExtSearchValues(TStringList *AVal);
};
//---------------------------------------------------------------------------
#endif
