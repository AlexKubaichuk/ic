#ifndef dsRegExtFilterSetTypesH
#define dsRegExtFilterSetTypesH
//---------------------------------------------------------------------------
//enum TdsRegExtFilterSetType { efsUnit, efsUnitSel, efsClass, efsExtUnit1, efsExtUnit2, efsExtUnit3, efsExtUnit4};
typedef UnicodeString __fastcall (__closure *TdsRegExtFilterSetEvent)(TTagNode *ADefNode, bool &CanContinue, UnicodeString AFilterGUI);
#endif
