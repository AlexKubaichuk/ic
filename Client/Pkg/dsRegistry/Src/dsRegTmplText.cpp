﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <clipbrd.hpp>
#pragma hdrstop
#include "dsRegTmplText.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
// #define ED         ((TcxDBMaskEdit*)ED)
#define CMaxH        Constraints->MaxHeight
#define CMinH        Constraints->MinHeight
#define CMaxW        Constraints->MaxWidth
#define CMinW        Constraints->MinWidth
#define tED_CB      ((TcxComboBox*)ED)
// ---------------------------------------------------------------------------
__fastcall TriTextTmpl::TriTextTmpl(TComponent * AOwner, TWinControl * AParent, TTagNode * ANode,
  TdsRegEDContainer * ACtrlOwner) : TdsRegEDItem(AOwner, AParent, ANode, ACtrlOwner)
 {
  FClearBtn           = new TcxButton(this);
  FClearBtn->Parent   = this;
  FClearBtn->OnClick  = FClearClick;
  FClearBtn->ShowHint = true;
  FClearBtn->Hint     = "Очистить поле \"" + FNode->AV["name"] + "\"";
  FClearBtn->Width    = 20;
  FClearBtn->Height   = 20;
  FClearBtn->OptionsImage->Glyph->LoadFromFile(icsPrePath(FCtrlOwner->DataPath + "\\img\\regclear.bmp"));
  FClearBtn->OptionsImage->NumGlyphs          = 4;
  FClearBtn->PaintStyle                       = bpsGlyph;
  FClearBtn->SpeedButtonOptions->CanBeFocused = false;
  FClearBtn->SpeedButtonOptions->Flat         = true;
  FClearBtn->TabStop                          = false;
  FClearBtn->Anchors.Clear();
  FClearBtn->Anchors << akRight;
  TRect xSize = Rect(0, 0, 120, 23); // default позиция и размер для "ED"
  FLab                       = new TLabel(this);
  FLab->Parent               = this;
  FLab->Transparent          = true;
  FLab->Top                  = 2;
  FLab->Left                 = 4;
  FLab->Caption              = FNode->AV["name"] + ":";
  FOnGetFormat               = ACtrlOwner->OnGetFormat;
  ED                         = new TcxMaskEdit(this);
  ED->Parent                 = this;
  ED->Properties->OnChange   = FDataChange;
  ED->Style->StyleController = FCtrlOwner->StyleController;
  if (Required)
   ED->Style->Color = FReqColor;
  int xW = 5 * FNode->AV["length"].ToInt();
  xSize                     = Rect(0, 0, xW, 23);
  ED->Properties->MaxLength = FNode->AV["length"].ToInt();
  ED->CMinW                 = 60;
  if (!FNode->CmpAV("cast", "0"))
   ED->OnExit = FFieldCast;
  if (FNode->AV["format"].Length())
   {
    ED->Properties->MaskKind = emkRegExprEx;
    if (FOnGetFormat)
     ED->Properties->EditMask = FOnGetFormat(FNode->AV["format"]);
    else
     ED->Properties->EditMask = FNode->AV["format"];
   }
  if (!FCtrlOwner->TemplateField && FCtrlOwner->IsTemplate(_UID_))
   {
    ED->Text = FCtrlOwner->TemplateData->Value[_UID_];
   }
  ED->SelLength = 0;
  ED->SelStart              = 0;
  ED->Enabled               = isEnabled && isEdit && (!isTemplate || FCtrlOwner->TemplateNoLock());
  ED->Constraints->MinWidth = 60;
  ED->Parent                = this;
  ED->Width                 = xSize.Right + 10;
  if (ED->Width < 60)
   ED->Width = 60;
  ED->Top    = xSize.Top;
  ED->Height = xSize.Bottom;
  if (FLab->Width > (AParent->Width - 2))
   {
    FLab->Font->Size = FLab->Font->Size - 1;
    FLab->Update();
    if (FLab->Width > (AParent->Width - 2))
     FLab->Width = AParent->Width - 2;
   }
  ED->Left = FLab->Left + FLab->Width + 5;
  if ((ED->Left + ED->Width) > (AParent->Width - 2))
   {
    TwoLine   = true;
    FLab->Top = 0;
    ED->Left  = FLab->Left + 5;
    ED->Top   = FLab->Height;
    CMaxH     = 36;
    CMinH     = 36;
    Height    = CMaxH;
    if ((ED->Left + ED->Width) > (AParent->Width - 2))
     ED->Width = AParent->Width - 4 - ED->Left;
    if (FNode->CmpName("text"))
     ED->Width = AParent->Width - 12 - ED->Left;
   }
  FLab->FocusControl = ED;
  FClearBtn->Top = ED->Top + ED->Height / 2 - 10;
  ED->OnKeyDown  = FEDKeyDown;
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::Clear()
 {
  FClearClick(FClearBtn);
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::FClearClick(TObject * ASender)
 {
  ED->Text                               = "";
  FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
  DataChange(ED);
 }
// ---------------------------------------------------------------------------
TcxMaskEdit * __fastcall TriTextTmpl::FGetCastED()
 {
  return (TcxMaskEdit *)FED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::FSetCastED(TcxMaskEdit * AVal)
 {
  FED = AVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetOnDependValues()
 {
  if (FNode->AV["depend"].Length() == 9 && EDCtrlExists(_GUI(FNode->AV["depend"])))
   GetEDCtrl(_GUI(FNode->AV["depend"]))->SetOnLab(this);
  else if (FNode->AV["ref"].Length() == 4 && EDCtrlExists(FNode->AV["ref"]))
   GetEDCtrl(FNode->AV["ref"])->SetOnLab(this);
  FSetOnDependValues();
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetFixedVar(UnicodeString AVals)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  /*
   if (isFV)
   {
   UnicodeString tmp = tED_CB->Text;
   tED_CB->Properties->Items->Text = AVals;
   int ind = tED_CB->Properties->Items->IndexOf(tmp);
   if (ind != -1)
   {
   tED_CB->ItemIndex = ind;
   tED_CB->Text = tmp;
   }
   else
   tED_CB->Text = tmp;
   }
   */
 }
// ---------------------------------------------------------------------------
__fastcall TriTextTmpl::~TriTextTmpl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  ED->Properties->OnChange = NULL;
  delete ED;
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetEDLeft(int ALeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  IsAlign = true;
  if (!TwoLine)
   ED->Left = ALeft;
  Width = ED->Left + ED->Width + 4;
  Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::GetEDLeft(int * ALabR, int * AEDLeft)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!TwoLine)
   * AEDLeft = ED->Left;
  else
   * AEDLeft = -1;
  if (!TwoLine)
   * ALabR = FLab->Left + FLab->Width;
  else
   * ALabR = -1;
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::cResize(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (!IsAlign)
   {
    if (TwoLine)
     {
      ED->Left  = 2;
      ED->Width = Width - 4 - _TMPL_CLEAR_WIDTH_;
     }
    else
     ED->Left = Width - ED->Width - 2 - _TMPL_CLEAR_WIDTH_;
    FClearBtn->Left = ED->Left + ED->Width + 2;
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::FFieldCast(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString FVal;
  if (ED->Text.Trim().Length())
   {
    FVal     = FieldCast(FNode, ED->Text.Trim());
    ED->Text = FVal;
    if (ED->Text.Trim().Length())
     FCtrlOwner->TemplateData->Value[_UID_] = ED->Text;
    else
     FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
   }
 }
// ---------------------------------------------------------------------------
bool __fastcall TriTextTmpl::UpdateChanges()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(ED);
  return !FCtrlOwner->IsTemplate(_UID_);
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriTextTmpl::GetValueStr(bool ABinaryNames)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  UnicodeString xVal = "";
  if (ED->Enabled && ED->Text.Length())
   xVal = ED->Text.Trim();
  return xVal;
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetIsVal(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isVal = AEnable;
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetEnable(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled          = AEnable;
  FLab->Enabled      = isEnabled && isEdit && !isTemplate;
  ED->Enabled        = isEnabled && isEdit && !isTemplate;
  FClearBtn->Enabled = ED->Enabled;
  if (!isEnabled)
   {
    if (isEdit && !isTemplate)
     ED->Text = "";
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetEnableEx(bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  isEnabled          = AEnable;
  FLab->Enabled      = isEnabled && isEdit && !isTemplate;
  ED->Enabled        = isEnabled && isEdit && !isTemplate;
  FClearBtn->Enabled = ED->Enabled;
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetValue(UnicodeString AVal, bool AEnable)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnable(AEnable);
  if (AVal.Length())
   {
    ED->Text                               = AVal;
    FCtrlOwner->TemplateData->Value[_UID_] = AVal;
   }
  else
   {
    ED->Text                               = "";
    FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
   }
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::FEDKeyDown(TObject * Sender, WORD & Key, TShiftState Shift)
 {
  if ((Key == VK_DELETE) && Shift.Contains(ssCtrl) && ED->Enabled)
   {
    FClearClick(FClearBtn);
    Key = 0;
   }
  if (Key && FCtrlOwner->OnKeyDown)
   FCtrlOwner->OnKeyDown(Sender, Key, Shift);
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetLabel(UnicodeString ACaption)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (ACaption.Length())
   FLab->Caption = ACaption + ":";
  else
   FLab->Caption = ""; // FNode->AV["name"]+":";
  FLab->Update();
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::SetOnEnable(TTagNode * ANode)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  SetEnList->Add(ANode);
  // ED->Properties->OnChange = DataChange;
  DataChange(this);
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::DataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  FDataChange(Sender);
 }
// ---------------------------------------------------------------------------
void __fastcall TriTextTmpl::FDataChange(TObject * Sender)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  // запись значений при изменении значения контрола кроме choiceDB ----------------------------------
  if (ED->Enabled)
   {
    if (ED->Text.Trim().Length())
     FCtrlOwner->TemplateData->Value[_UID_] = ED->Text;
    else if (isEdit && !isTemplate)
     FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
   }
  else if (isEdit && !isTemplate)
   FCtrlOwner->TemplateData->Value[_UID_] = Variant::Empty();
  // отработка enabled для зависимых контролов -------------------------------------------------------
  if (SetEnList->Count > 0)
   {
    TTagNode * xNode;
    for (int i = 0; i < SetEnList->Count; i++)
     {
      xNode = ((TTagNode *)SetEnList->Items[i]);
      FGetExtCtrl(xNode->AV["uid"])->SetEnable(Condition(xNode->GetChildByName("actuals")));
     }
   }
  if (FGetDataChange)
   {
    UnicodeString Src = FCtrlOwner->TemplateData->Value[_UID_];
    FGetDataChange(FNode, Src);
   }
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriTextTmpl::GetControl(int AIdx)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (TControl *)ED;
 }
// ---------------------------------------------------------------------------
TControl * __fastcall TriTextTmpl::GetFirstEnabledControl()
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return (ED->Enabled && isEdit) ? ((TControl *)ED) : NULL;
 }
// ---------------------------------------------------------------------------
UnicodeString __fastcall TriTextTmpl::GetValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  return ED->Text;
 }
// ---------------------------------------------------------------------------
Variant __fastcall TriTextTmpl::GetCompValue(UnicodeString ARef)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  if (GetValue("").Trim().Length())
   return GetValue("");
  else
   return Variant("NULL");
 }
// ---------------------------------------------------------------------------
bool __fastcall TriTextTmpl::CheckReqValue(TcxPageControl * APC, bool ASilent)
 {
  if (!this)
   throw ERegEDError("Error in " + UnicodeString(__FUNC__) + " -> object is NULL");
  bool RC = true;
  if (Required)
   {
    try
     {
      if (ED->Enabled)
       {
        if (!ED->Text.Length())
         RC = false;
       }
     }
    __finally
     {
      if (!RC)
       {
        if (APC)
         APC->ActivePageIndex = this->Tag;
        if (!ASilent)
         {
          UnicodeString ErrMessage;
          if (FLab)
           ErrMessage = FLab->Caption;
          else
           ErrMessage = FNode->AV["name"];
          ErrMessage = FMT1(icsRegItemErrorReqField, ErrMessage);
          _MSG_ATT(ErrMessage, FMT(icsInputErrorMsgCaption));
         }
        if (ED->Visible && Visible)
         ED->SetFocus();
       }
     }
   }
  return RC;
 }
// ---------------------------------------------------------------------------
